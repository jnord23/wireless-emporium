<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "Home"
	
	dim reset : reset = prepInt(request.QueryString("reset"))
	if reset = 1 then
		response.Cookies("mobileBrandID") = 0
		response.Cookies("mobileModelID") = 0
	end if
%>
<!--#include virtual="/template/top.asp"-->
<%	
	sql = "select brandID, brandName from we_brands where phoneSale = 1 and other = 0 order by brandName"
	session("errorSQL") = sql
	set brandRS = oConn.execute(sql)
%>
<div id="main">
    <div class="homePageBanner"><img src="/images/mobile/banners/homePageBanner1.jpg" border="0" /></div>
    <div class="selectByBrand">
        <div class="CallToAction">SELECT A DEVICE BRAND BELOW:</div>
        <div class="ctaDownArrow"></div>
        <div class="allBrandImgs">
            <%
            do while not brandRS.EOF
                brandID = brandRS("brandID")
                brandName = brandRS("brandName")
                brandName = replace(brandName,"/Palm","")
                brandName = replace(brandName,"/Qualcomm","")
                brandName = replace(brandName," Ericsson","")
            %>
            <div class="fl brandButton" onclick="window.location='/<%=lcase(brandName)%>-phone-accessories.htm'">
                <div class="brandImg"><img src="/images/mobile/homeBrands/<%=lcase(brandName)%>.png" /></div>
                <div class="brandText"><a class="brandLink" href="/<%=lcase(brandName)%>-phone-accessories.htm"><%=brandName%></a></div>
            </div>
            <%
                brandRS.movenext
            loop
            brandRS.movefirst
            %>
            <div class="fl brandButton" onclick="window.location='/other-phone-accessories.htm'">
                <div class="brandImg"><img src="/images/mobile/homeBrands/others.png" /></div>
                <div class="brandText"><a class="brandLink" href="/other-phone-accessories.htm">Others</a></div>
            </div>
        </div>
    </div>
    <div class="selectByBrand" style="background-color:#efefef; padding:5px 0px 5px 0px;" align="center">
        <a href="/search/search.htm?#brm-search?request_type=search&search_type=trending&q=&l=trending">
            <div id="br_btnTrending"></div>
        </a>
        <a href="/search/search.htm?#brm-search?request_type=jfy">
            <div id="br_btnJfy"></div>
        </a>
    </div>
</div>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">
	var mobileBrandID = <%=prepInt(request.Cookies("mobileBrandID"))%>
	var mobileModelID = <%=prepInt(request.Cookies("mobileModelID"))%>
</script>
<script language="javascript" src="/template/js/index.js"></script>