<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "Basket"
	
	dim itemID : itemID = prepInt(request.Form("itemID"))
	dim qty : qty = prepInt(request.Form("qty"))
	dim acctID : acctID = prepInt(session("acctID"))
	dim prodAdj : prodAdj = 0
	dim removePromo : removePromo = prepInt(request.QueryString("removePromo"))
	if removePromo = 1 then session("promocode") = null
	
	if prepInt(request.Cookies("mySession")) = 0 then
		response.Cookies("mySession") = session.SessionID
		response.Cookies("mySession").expires = date + 30
		useSessionID = session.SessionID
		newID = useSessionID
	else
		useSessionID = prepInt(request.Cookies("mySession"))
		newID = 0
	end if
	mySession = useSessionID
	
	if itemID > 0 then
		if qty = 0 then
			sql = "delete from shoppingCart where itemID = " & itemID & " and sessionID = " & useSessionID
		else
			sql =	"if (select count(*) from shoppingCart where itemID = " & itemID & " and sessionID = " & useSessionID & ") > 0 " &_
						"update shoppingCart " &_
						"set qty = " & qty & " " &_
						"where itemID = " & itemID & " and sessionID = " & useSessionID & " " &_
					"else " &_
						"insert into shoppingCart (" &_
							"store,sessionID,accountID,itemID,qty,mobileOrder,ipAddress" &_
						") values(" &_
							"0," & useSessionID & "," & acctID & "," & itemID & "," & qty & ",1,'" & request.ServerVariables("REMOTE_ADDR") & "'" &_
						")"
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
		prodAdj = 1
	end if
	
	sql =	"select c.totalItems, a.qty, b.itemID, b.itemPic, b.itemDesc, b.price_our, a.customCost " &_
			"from shoppingCart a " &_
				"left join we_Items b on a.itemID = b.itemID " &_
				"outer apply (" &_
					"select sum(qty) as totalItems, sessionID " &_
					"from shoppingCart " &_
					"where sessionID = " & useSessionID & " and purchasedorderid is null " &_
					"group by sessionID" &_
				") c " &_
			"where a.sessionID = " & useSessionID & " and a.purchasedorderid is null"
'	response.write sql
	session("errorSQL") = sql
	Set rsCartItem = oConn.execute(sql)
	
	if rsCartItem.EOF then
		itemCnt = "0 Items"
		if prodAdj = 1 then
			if prepStr(request.Cookies("customerEmail")) <> "" then
				sql = "exec sp_remarketing 0," & mySession & ",'" & request.Cookies("customerEmail") & "',0"
				session("errorSQL") = sql
				oConn.execute(sql)
			end if
		end if
	else
		if rsCartItem("totalItems") <> 1 then
			itemCnt = rsCartItem("totalItems") & " Items"
		else
			itemCnt = rsCartItem("totalItems") & " Item"
		end if
		if prodAdj = 1 then
			if prepStr(request.Cookies("customerEmail")) <> "" then
				sql = "exec sp_remarketing 0," & mySession & ",'" & request.Cookies("customerEmail") & "',1"
				session("errorSQL") = sql
				oConn.execute(sql)
			end if
		end if
	end if
	
	nSubTotal = 0	'this is for promo calculation on "/cart/includes/inc_promoFunctions.asp"
	if not rsCartItem.eof then
		do while not rsCartItem.EOF
			lineQty = rsCartItem("qty")
			linePrice = rsCartItem("price_our")
			if not isnull(rsCartItem("customCost")) then linePrice = rsCartItem("customCost")
			nSubTotal = nSubTotal + (linePrice*lineQty)
			rsCartItem.movenext
		loop
		rsCartItem.movefirst
	end if
%>
<!--#include virtual="/template/promoCalc.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/template/top.asp"-->
<%
	discountTotal = prepInt(discountTotal)
	discountedCartTotal = 0
	promoCodeUsed = request.form("txtPromo")
	promoCodeIsValid = false

	if request.form("submitted") and request.form("submitted") <> 1 then
		promoCodeIsValid = true
	end if

%>
<script>
window.WEDATA.pageType = "cart";
window.WEDATA.pageData = {
	cartItems: [],
	promoCode: <%= jsStr(promoCodeUsed) %>,
	promoIsValid: <%= jsStr(LCase(promoCodeIsValid)) %>,
	couponId: <%= jsStr(couponId) %>,
	subTotal: <%= jsStr(nSubtotal) %>,
	weight: <%= jsStr(totalWeight) %>
};
</script>
<script>
	window.onload = function () {
	<%if request.form("submitted") = "1" and prepStr(sPromoCode) <> "" then%>
		var p = $('#totalBox').position();
		$('html,body').animate({ scrollTop: p.top-10 }, 'slow');
	<%end if%>
	}
</script>
<script language="javascript" src="/template/js/basket.js"></script>
<div id="qtyInCartRow">
   	<div id="qtyInCartText">You have <span id="orangeText"><%=itemCnt%></span> in your cart:</div>
</div>
<div id="productContainer">
	<div style="padding:0px 0px 5px 0px; text-align:left;"><a href="/" style="text-decoration:none; color:#333; font-weight:bold;">&lsaquo; Continue Shopping</a></div>
	<%
	if rsCartItem.EOF then
	%>
    <div id="itemBox_noItems">You Currently Have<br />No Items In Your Cart</div>
    <%
	end if

	subtotal = 0
	do while not rsCartItem.EOF
		itemID = prepInt(rsCartItem("itemID"))
		itemPic = rsCartItem("itemPic")
		itemDesc = prepStr(rsCartItem("itemDesc"))
		qty = prepInt(rsCartItem("qty"))
		itemPrice = prepInt(rsCartItem("price_our"))
	%>
    <div id="itemBox">
    	<div id="itemPic" onclick="window.location='/product.htm?itemID=<%=itemID%>'"><img src="/productPics/big/<%=itemPic%>" width="100" border="0" /></div>
        <div id="itemDetails">
        	<div id="itemDesc" onclick="window.location='/product.htm?itemID=<%=itemID%>'"><%=itemDesc%></div>
            <form name="qtyAdjustForm_<%=itemID%>" method="post" id="basicForm">
            <div id="qtyDetails">
            	<div id="qtyText">Qty.</div>
                <div id="qtyValue">
                	<input type="tel" id="qtyInput" name="qty" value="<%=qty%>" size="3" maxlength="3" />
                    <input type="hidden" name="itemID" value="<%=itemID%>" />
                </div>
                <div id="qtyButton"><input type="image" src="/images/mobile/retina/cart/we-m-button-update.png" width="69" /></div>
            </div>
            </form>
            <% if qty = 1 then %>
            <div id="itemPrice">Item Price: <%=formatCurrency(itemPrice,2)%></div>
            <% else %>
            <div id="itemPriceSmall">Item Price: <%=formatCurrency(itemPrice,2)%></div>
            <div id="itemPrice">Item Total: <%=formatCurrency((itemPrice * qty),2)%></div>
            <% end if %>
            <div id="removeRow" onclick="br_cartTracking('remove', '<%=itemid%>', '', '<%=replace(itemDesc, "'", "\'")%>', '<%=itemPrice%>'); removeItem(<%=itemID%>);">
            	<div id="removeImg" class="removeImg" data-itemid="<%=itemid%>"><img src="/images/mobile/retina/cart/we-m-button-remove.png" border="0" width="20" /></div>
                <div id="removeText">Remove Item</div>
            </div>
        </div>
    </div>
    <%
		subtotal = subtotal + (itemPrice * qty)
		rsCartItem.movenext
	loop
	discountedCartTotal = subtotal - discountTotal
	
	if prepStr(sPromoCode) = "" then
	%>
    <div id="promoTitle" class="couponRow">
    	<a href="javascript:showPromoField()" class="promoLnk">Do you have a coupon code?</a>
        <%if request.form("submitted") = "1" then%>
        <div class="clr" style="color:#f00;">The code you entered is invalid.</div>
        <%end if%>
	</div>
    <div id="promoRow" class="basketRow" style="display:none;">
    	<form name="frmPromo" method="post">
        <div class="tb" style="padding:10px;">
            <div class="tb" style="color:#000034; font-weight:bold;">Enter a coupon code below:</div>
            <div class="tb" style="padding-top:5px;">
                <div class="fl"><input type="text" name="txtPromo" value="Coupon code" class="promoField" onclick="this.value=''"></div>
                <div class="fl"><div class="promoApply" onclick="submitPromoCode()"></div></div>
            </div>
        </div>
		<input type="hidden" name="submitted" value="1" />
        </form>
    </div>
    <%end if%>
    
    <div id="totalBox">
    	<div id="subTotalRow">
        	<div id="subtotalText">Subtotal:</div>
            <div id="subtotalAmt"><%=formatCurrency(prepInt(subtotal),2)%></div>
        </div>
		<%
		if sPromoCode <> "" then
			session("promocode") = sPromoCode
		%>
		<div id="subTotalRow">
        	<div id="shipText">Discount:</div>
            <a href="/basket.htm?removePromo=1"><div class="fr" style="cursor:pointer; margin:-2px 0px 0px 2px;"><img src="/images/mobile/removeX.gif" border="0"></div></a>
            <div id="shipAmt"><%=formatcurrency(0-discountTotal,2)%></div>
        </div>
		<%end if%>        
        <div id="subTotalRow">
        	<div id="shipText">Shipping:</div>
            <div id="shipAmt">FREE</div>
        </div>
        <div id="totalRow">
        	<div id="totalText">Estimated Total:</div>
            <div id="totalAmt"><%=formatCurrency(prepInt(discountedCartTotal),2)%></div>
        </div>
    </div>
	<div class="clr" style="height:10px;"></div>
</div>
<%
	sql =	"select c.totalItems, c.totalWeight, a.qty, b.itemID, b.itemPic, b.itemDesc, b.price_our " &_
			"from shoppingCart a " &_
				"left join we_Items b on a.itemID = b.itemID " &_
				"outer apply (" &_
					"select sum(qty) as totalItems, sum(itemWeight * ia.qty) as totalWeight, sessionID " &_
					"from shoppingCart ia " &_
						"left join we_Items ib on ia.itemID = ib.itemID " &_
					"where sessionID = " & useSessionID & " and ia.purchasedorderid is null " &_
					"group by sessionID" &_
				") c " &_
			"where a.sessionID = " & useSessionID & " and a.purchasedorderid is null" 
	session("errorSQL") = sql
	Set rsTotal = oConn.execute(sql)
	
	if rsTotal.EOF or useSessionID = 0 then
		response.Redirect("/")
	else
		itemCnt = rsTotal("totalItems")
		totalWeight = rsTotal("totalWeight")
		
		itemDetails = ""
		orderAmt = 0
		do while not rsTotal.EOF
			qty = rsTotal("qty")
			itemID = rsTotal("itemID")
			itemDesc = rsTotal("itemDesc")
			itemPrice = rsTotal("price_our")
			
			itemDetails = itemDetails & qty & "##"
			itemDetails = itemDetails & itemID & "##"
			itemDetails = itemDetails & itemDesc & "##"
			itemDetails = itemDetails & itemPrice & "@@"
			orderAmt = orderAmt + (qty * itemPrice)
			rsTotal.movenext
		loop
		if len(itemDetails) > 0 then itemDetails = left(itemDetails,len(itemDetails)-2)
		itemDetailArray = split(itemDetails,"@@")
	end if
%>
<div id="checkoutOption">
<%
	actionURL = "https://m.wirelessemporium.com/checkout_step2.htm"
	if instr(request.ServerVariables("SERVER_NAME"), "mdev.") > 0 then actionURL = "/checkout_step2.htm"
%>
	<form method="post" action="<%=actionURL%>">
    	<input type="hidden" name="subtotal" value="<%=orderAmt%>" />
	    <input type="image" src="/images/mobile/retina/cart/we-m-button-checkout.png" border="0" width="282" />
    </form>
</div>
<div id="checkoutOption"><img src="/images/mobile/retina/cart/we-m-creditcard.png" width="185" border="0" /></div>
<div id="otherOptions">
	<div id="checkoutOption" style="display:none;">
    	<form name="googleForm" method="post" action="/cart/process/google/CartProcessing.htm">
            <input type="hidden" name="promo" value="">
            <input type="hidden" name="analyticsdata" value="">
            <input type="hidden" name="sWeight" value="<%=totalWeight%>">
            <input type="hidden" name="mobileOrder" value="1" />
            <%
			for i = 0 to ubound(itemDetailArray)
				singleItemArray = split(itemDetailArray(i),"##")
				qty = singleItemArray(0)
				itemID = singleItemArray(1)
			%>
            <input type="hidden" name="itemID_<%=i+1%>" value="<%=itemID%>">
            <input type="hidden" name="itemQty_<%=i+1%>" value="<%=qty%>">
            <% next %>
            <input type="image" src="/images/mobile/buttons/googleCheckout.jpg" border="0" />
        </form>
    </div>
	<div id="checkoutOption"<% if prepInt(disablePayPal) = 1 then %> style="display:none;"<% end if %>>
    	<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.htm" style="margin:0px;">
            <%
			itemLap = 0
			for i = 0 to ubound(itemDetailArray)
				singleItemArray = split(itemDetailArray(i),"##")
				qty = singleItemArray(0)
				itemID = singleItemArray(1)
				itemDesc = singleItemArray(2)
				itemPrice = singleItemArray(3)
			%>
			<input type="hidden" name="L_NUMBER<%=itemLap%>" value="<%=itemID%>">
            <input type="hidden" name="L_NAME<%=itemLap%>" value="<%=itemDesc%>">
            <input type="hidden" name="L_QTY<%=itemLap%>" value="<%=qty%>">
            <input type="hidden" name="L_AMT<%=itemLap%>" value="<%=itemPrice%>">
            <script>
            	window.WEDATA.pageData.cartItems.push({itemId: <%= jsStr(itemID) %>, qty: <%= jsStr(qty) %>, price: <%= jsStr(itemPrice) %>});
            </script>

            <%
				itemLap = itemLap + 1
			next
            
			if sPromoCode <> "" then
				itemCnt = itemCnt + 1
			%>
			<input type="hidden" name="promo" value="<%=sPromoCode%>">
			<input type="hidden" name="L_NUMBER<%=itemLap%>" value="DISCOUNT">
            <input type="hidden" name="L_NAME<%=itemLap%>" value="<%=sPromoCode%>">
            <input type="hidden" name="L_QTY<%=itemLap%>" value="1">
            <input type="hidden" name="L_AMT<%=itemLap%>" value="<%=formatnumber(0-discountTotal, 2)%>">
			<%
			end if
			%>
            
            <input type="hidden" name="numItems" value="<%=itemCnt%>">
		    <input type="hidden" name="paymentAmount" value="<%=formatnumber(discountedCartTotal, 2)%>">
		    <input type="hidden" name="ItemsAndWeight" value="<%=itemCnt%>|<%=totalWeight%>">
		    <input type="hidden" name="mobileOrder" value="1">
            <input type="image" src="/images/mobile/retina/cart/we-m-button-paypal.png" width="282" border="0" />
		</form>
    </div>
</div>
<!--
<div id="proceedButtons"><a href="/checkout_step1.htm"><img src="/images/mobile/buttons/secureCheckout.jpg" border="0" /></a></div>
<div id="proceedButtons"><a href="/"><img src="/images/mobile/buttons/continueShopping.jpg" border="0" /></a></div>
-->
<div style="height:10px;"></div>
<script>
window.WEDATA.pageData.weight = '<%=totalWeight %>';
</script>

<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">
	function removeItem(itemID) {
		eval("document.qtyAdjustForm_" + itemID + ".qty.value = 0")
		eval("document.qtyAdjustForm_" + itemID + ".submit()")
	}
</script>