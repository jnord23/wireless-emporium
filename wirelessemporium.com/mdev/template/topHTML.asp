<% if valPropBanner = 0 then %>
<div id="freeShipTop">
    <div id="freeText" style="padding:5px;">Fast, FREE Shipping! <span style="color:#FFF;">|</span> Easy 90-Day Returns</div>
</div>
<div id="logoRow">
    <div id="logoContainer">
        <div id="weLogo"><a href="/"><img src="/images/mobile/retina/home/we-m-logo.png" border="0" width="261" height="33" alt="Wireless Emporium Mobile Site" /></a></div>
        <div id="cartIcon" onclick="window.location='/basket.htm'" style="width:58px;">
            <div style="background:url(/images/mobile/retina/home/we-m-shoppingcart.png) no-repeat;background-size:20px 20px; width:20px;height:20px;position:relative;margin:0 auto;"></div>
			<div id="inCart"><%=miniTotalQuantity%></div>
        </div>
    </div>
</div>
<% else %>
<a href="/">
<div id="bannerRow">
    <div id="valPropBanner1"></div>
</div>
</a>
<% end if %>
<div id="searchBar">
    <div id="searchContainer">
        <div id="orderByPhone"><%=mobilePhoneText%>&nbsp;<a href="tel:<%=mobilePhoneNumber%>" id="csNumber"><%=mobilePhoneNumber%></a></div>
        <% if showSearch = 1 then %>
        <div id="searchButton"><a href="/search.htm"><img src="/images/mobile/retina/checkout/we-m-button-search2.png" border="0" width="44" height="33" alt="Product Search Link" /></a></div>
        <% end if %>
    </div>
</div>
<% if gpsLoc = 2 then %>
<div id="gpsBar">
    <div id="gpsMsg" onclick="navigator.geolocation.getCurrentPosition(GetLocation)"><img src="/images/mobile/buttons/useGPS.jpg" border="0" width="320" height="40" alt="Submit GPS Location" /></div>
</div>
<% end if %>
<% if pageName = "Home" or showTabs = 0 then %>
<div id="adSpot" style="display:none;"><img src="/images/mobile/mobileAd.jpg" border="0" width="320" height="120" alt="Advertisement" /></div>
<% elseif instr(pageName,"Checkout") > 0 then %>
<div id="breadCrumbs">
    <div id="breadCrumbText"><a href="/" id="breadCrumbLink">Home</a></div>
    <div id="breadCrumbBreak"><img src="/images/mobile/breadCrumbBreak.gif" border="0" width="15" height="40" alt="Breadcrumb Break" /></div>
    <div id="breadCrumbText">
        <div id="checkoutStepBox"><span style="font-weight:bold;">Checkout:</span> Step</div>
        <div id="checkoutStepImgBox">
            <% if pageName = "Checkout1" then %><img src="/images/mobile/icons/step1.gif" border="0" width="20" height="20" alt="Checkout Step 1" /><% end if %>
            <% if pageName = "Checkout2" then %><img src="/images/mobile/retina/checkout/we-m-steps-2.png" border="0" width="20" height="20" alt="Checkout Step 2" /><% end if %>
            <% if pageName = "Checkout3" then %><img src="/images/mobile/icons/step3.gif" border="0" width="20" height="20" alt="Checkout Step 3" /><% end if %>
        </div>
        <div id="checkoutStepBox">of</div>
        <div id="checkoutStepImgBox"><img src="/images/mobile/retina/checkout/we-m-steps-3.png" border="0" width="20" height="20" alt="Total Checkout Steps" /></div>
    </div>
    <div id="breadCrumbBreak"><img src="/images/mobile/breadCrumbBreak.gif" border="0" width="15" height="40" alt="Breadcrumb Break" /></div>
</div>
<% else %>
<div id="breadCrumbs">
    <div id="breadCrumbText"><a href="/" id="breadCrumbLink">Home</a></div>
    <div id="breadCrumbBreak"><img src="/images/mobile/breadCrumbBreak.gif" border="0" width="15" height="40" alt="Breadcrumb Break" /></div>
    <div id="breadCrumbText"><a href="javascript:history.go(-1)" id="breadCrumbLink">Previous Page</a></div>
    <div id="breadCrumbBreak"><img src="/images/mobile/breadCrumbBreak.gif" border="0" width="15" height="40" alt="Breadcrumb Break" /></div>
</div>
<% end if %>