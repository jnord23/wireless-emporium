	function newTypeSelected(typeID) {
		if (typeID != "") {
			chkForm()
		}
	}
	
	function newBrandSelected() {
		var brandID = document.prodSelectForm.brandID.options[document.prodSelectForm.brandID.selectedIndex].value
		if (brandID == "") {
			document.getElementById("selectBoxContainer2").innerHTML = "<select id='actualSelectBox2' name='modelID' disabled='disabled'><option value=''>Choose A Device Model</option></select>"
			document.getElementById("selectBoxContainer3").innerHTML = "<select id='actualSelectBox2' name='typeID' disabled='disabled'><option value=''>Choose A Product Type</option></select>"
		}
		else {
			ajax('/ajax/homepageSelect.htm?brandSelect=1&brandID=' + brandID,'selectBoxContainer2')
		}
		document.getElementById("selectBoxContainer3").innerHTML = "<select id='actualSelectBox2' name='typeID' disabled='disabled'><option value=''>Choose A Product Type</option></select>"
	}
	
	function newModelSelected() {
		var modelID = document.prodSelectForm.modelID.options[document.prodSelectForm.modelID.selectedIndex].value
		if (modelID == "") {
			document.getElementById("selectBoxContainer3").innerHTML = "<select id='actualSelectBox2' name='typeID' disabled='disabled'><option value=''>Choose A Product Type</option></select>"
		}
		else {
			ajax('/ajax/homepageSelect.htm?modelSelect=1&modelID=' + modelID,'selectBoxContainer3')
			scroll(100,0)
		}
	}
	
	function chkForm() {
		var brandID = document.prodSelectForm.brandID.options[document.prodSelectForm.brandID.selectedIndex].value
		var brandName = eval("document.prodSelectForm.brand_" + brandID + ".value")
		var modelID = document.prodSelectForm.modelID.options[document.prodSelectForm.modelID.selectedIndex].value
		var modelName = eval("document.prodSelectForm.model_" + modelID + ".value")
		var typeID = document.prodSelectForm.typeID.options[document.prodSelectForm.typeID.selectedIndex].value
		var typeName = eval("document.prodSelectForm.cat_" + typeID + ".value")
		
		if (brandID == "") {
			alert("You must select a brand, model and product type")
			return false
		}
		else if (modelID == "") {
			alert("You must select a model and product type")
			return false
		}
		else if (typeID == "") {
			alert("You must select a product type")
			return false
		}
		else {
			document.prodSelectForm.action = "/sb-" + brandID + "-sm-" + modelID + "-sc-" + typeID + "-" + typeName + "-" + brandName + "-" + modelName + ".htm"
			document.prodSelectForm.submit()
		}
	}
	
	if (mobileBrandID > 0) {
		for (i=0;i<document.prodSelectForm.brandID.length;i++) {
			if (document.prodSelectForm.brandID.options[i].value == mobileBrandID) {
				document.prodSelectForm.brandID.selectedIndex = i
				newBrandSelected()
				setTimeout("setModleByCookie()",500)
			}
		}
	}
	
	function setModleByCookie() {
		for (i=0;i<document.prodSelectForm.modelID.length;i++) {
			if (document.prodSelectForm.modelID.options[i].value == mobileModelID) {
				document.prodSelectForm.modelID.selectedIndex = i
				newModelSelected()
			}
		}
	}