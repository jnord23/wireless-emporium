<%
	dim sPromoCode
	if request.form("txtPromo") <> "" then
		sPromoCode = prepStr(request.form("txtPromo"))
	else
		sPromoCode = session("promocode")
	end if
	
	if sPromoCode <> "" then
		dim couponid : couponid = 0
		sql = "SELECT * FROM WE_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
		session("errorSQL") = SQL
		set rsPromo = oConn.execute(sql)
		if not rsPromo.eof then
			dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
			couponid = rsPromo("couponid")
			promoMin = rsPromo("promoMin")
			promoPercent = rsPromo("promoPercent")
			typeID = rsPromo("typeID")
			excludeBluetooth = rsPromo("excludeBluetooth")
			BOGO = rsPromo("BOGO")
			couponDesc = rsPromo("couponDesc")
			FreeItemPartNumber = rsPromo("FreeItemPartNumber")
			setValue = rsPromo("setValue")
			oneTime = rsPromo("oneTime")
		else
			promoError = "Promo Code Not Found"
			sPromoCode = ""
		end if
	end if
%>