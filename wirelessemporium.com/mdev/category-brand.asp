<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "category-brand"
	
	dim categoryID : categoryID = prepInt(request.QueryString("categoryID"))
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim curUrl : curUrl = request.ServerVariables("HTTP_X_REWRITE_URL")
	dim sortBy : sortBy = "az"
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	
	if instr(curUrl,"?") > 0 then
		curQs = mid(curUrl, instr(curUrl,"?")+1)
		sortBy = mid(curQs, instr(curQs,"=")+1)
		curUrl = left(curUrl, instr(curUrl,"?")-1)
	end if
%>
<!--#include virtual="/template/top.asp"-->
<%	
	sql = "select typeName_WE from we_types where typeID = " & categoryID
	session("errorSQL") = sql
	set catRS = oConn.execute(sql)
	
	if not catRS.EOF then
		categoryName = catRS("typeName_WE")
	end if
	
	sql = "select brandID, brandName from we_brands where phoneSale = 1 and other = 0 and brandID = " & brandID & " order by brandName"
	session("errorSQL") = sql
	set brandRS = oConn.execute(sql)
	
	if not brandRS.EOF then
		brandName = brandRS("brandName")
	end if
	
	sql = "exec sp_modelsByBrandMobile " & brandID & ",'" & sortBy & "'"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
%>
<div class="tb mCenter" style="background-color:#fff; margin-bottom:10px;">
	<div class="fl brandImgSmall"><img src="/images/mobile/homeBrands/<%=formatSEO(brandName)%>.png" border="0" /></div>
    <div class="fl catDetails">
    	<div class="curView">Currently Viewing:</div>
        <div class="curCat"><%=brandName & " " & categoryName%></div>
        <div>
        	<a href="/?reset=1" style="text-decoration:none;">
            <div class="tb" id="changeBrandBox">
                <div id="redX"></div>
                <div id="tapToChange">Tap to change category.</div>
            </div>
            </a>
        </div>
    </div>
    <div class="tb mCenter pt10">
        <select name="sortBy" class="sortBySelect" onchange="document.location='<%=curUrl%>?sortBy=' + this.value">
            <option value="">Sort By</option>
            <option value="az">A-Z</option>
            <option value="za">Z-A</option>
            <option value="no">Newest-Oldest</option>
            <option value="on">Oldest-Newest</option>
        </select>
    </div>
</div>
<div class="selectByBrand">
    <div class="CallToAction">SELECT A DEVICE BELOW:</div>
    <div class="ctaDownArrow"></div>
    <div class="allBrandImgs">
    <%
    do while not rs.EOF
		hidelive = rs("hidelive")
		international = rs("international")
		if not hidelive and not international then 
			deviceCnt = deviceCnt + 1
			useBrandName = rs("brandName")
			modelID = rs("modelID")
			modelImg = rs("modelImg")
			modelName = rs("modelName")
			showModelName = modelName
			if brandName = "other" then
				if useBrandName <> "Other" then showModelName = useBrandName & " " & modelName
			end if
			
			if fso.fileExists(server.MapPath("/productPics/models/thumbs/" & modelImg)) then
	%>
    <a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-<%=categoryID%>-<%=formatSEO(categoryName & "-" & brandName & "-" & modelName)%>.htm" style="text-decoration:none;" title="<%=modelName%>">
	<div id="singleDeviceBox_<%=deviceCnt%>" class="singleDeviceBox">
    	<div id="devicePic" style="position:relative;">
			<%if instr(useCarrierLogo, modelID & ",") > 0 then%>
				<div style="position:absolute; bottom:0px; left:-13px;"><img src="/images/brands/b_carrierLogo_<%=modelID%>.png" border="0" /></div>
			<%end if%>
        	<img src="/productPics/models/thumbs/<%=modelImg%>" border="0" height="112" />
		</div>
        <div id="deviceName"><%=showModelName%></div>
    </div>
    </a>
    <%
			end if
		end if
		rs.movenext
	loop
	%>
    </div>
</div>
<script>
window.WEDATA.pageType = 'categoryBrand';
window.WEDATA.pageData = {
	brand: '<%= brandName %>',
	brandId: '<%= brandId %>',
	category: '<%=categoryName%>',
	categoryId: '<%= categoryId%>'

};
</script>

<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">
	var mobileBrandID = <%=prepInt(request.Cookies("mobileBrandID"))%>
	var mobileModelID = <%=prepInt(request.Cookies("mobileModelID"))%>
</script>
<script language="javascript" src="/template/js/index.js"></script>