<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<%
	response.buffer = true
	Session.Timeout = 20
	
	if isnull(request.Cookies("mySession")) or len(request.Cookies("mySession")) < 1 then response.Cookies("mySession") = session.SessionID
	
	if session("adTracking") <> "" then
		buildURL = "/mobile/confirm.asp?o=" & request.querystring("o") & "&a=" & request.querystring("a") & "&d=" & request.querystring("d") & "" & replace(session("additionalQS"),"?","&")
		session("adTracking") = null
		session("additionalQS") = null
		response.Redirect(buildURL)
	end if
	
	curPage = "Confirm Order"
	userMsg = ""
	ajaxRedir = ""
	
	if isnull(session("acctID")) or len(session("acctID")) < 1 or session("acctID") = 0 then response.Redirect("/mobile/?a=")
	
	nAccountID = SQLquote(request.querystring("a"))
	orderID = SQLquote(request.querystring("o"))
	nOrderGrandTotal = SQLquote(request.querystring("d"))
	
	sql = "update we_orders set mobileSite = 1 where orderID = " & orderID
	session("errorSQL") = sql
	oConn.execute(sql)
	
	sql = "insert into we_mobileTrackingPages (sessionID,page) values('" & request.Cookies("mySession") & "','" & request.ServerVariables("URL") & "?" & request.ServerVariables("QUERY_STRING") & "')"
	session("errorSQL") = sql
	oConn.execute(sql)
	
	if len(request.Cookies("mySession")) > 0 then
		sql = "delete from shoppingCart where sessionID = " & request.Cookies("mySession") & " or accountID = " & session("acctID")
	else
		sql = "delete from shoppingCart where accountID = " & session("acctID")
	end if
	session("errorSQL") = sql
	oConn.execute(sql)
	
	sql = "update we_mobileTracking set itemsPurchased = itemsPurchased + itemsInCart, itemsInCart = 0, checkouts = checkouts + 1 where sessionID = '" & request.Cookies("mySession") & "'"
	session("errorSQL") = sql
	oConn.execute(sql)
	
	function fOpenConn
		'nothing
	end function
	function fCloseConn
		'nothing
	end function
	
	response.Cookies("mySession") = ""
	response.Cookies("mySession").expires = now
	response.Cookies("checkoutPage") = ""
	response.Cookies("checkoutPage").expires = now
	
	sql = "select c.partNumber, c.noDiscount, a.orderID, a.orderSubTotal, a.orderShippingFee, a.orderTax, a.orderGrandTotal, a.shipType, c.itemID, c.itemDesc, c.price_our, b.quantity as qty from we_orders a left join we_orderDetails b on a.orderid = b.orderid left join we_items c on b.itemID = c.itemID where a.orderID = " & orderID & " and c.itemDesc is not null"
	sql = sql & " union "
	sql = sql & "select '' as partNumber, cast(0 as bit) as noDiscount, a.orderID, a.orderSubTotal, a.orderShippingFee, a.orderTax, a.orderGrandTotal, a.shipType, c.id as itemID, c.artist + ' ' + c.designName as itemDesc, c.price_we as price_our, b.quantity as qty from we_orders a join we_orderDetails b on a.orderid = b.orderid join we_items_musicSkins c on b.itemID = c.id where a.orderID = " & orderID
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 3, 3
	
	tax = rs("orderTax")
	shipAmt = rs("orderShippingFee")
	
	sql = "select * from we_accounts where accountID = " & session("acctID")
	session("errorSQL") = sql
	Set billAddrRS = Server.CreateObject("ADODB.Recordset")
	billAddrRS.open sql, oConn, 3, 3
	
	if not billAddrRS.EOF then
		sCity = billAddrRS("sCity")
		sState = billAddrRS("sState")
	end if
	
	baseCrumb = "<a style='font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?r=1','mobileContent',1)>Main Menu&nbsp;&gt;</a>"
%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html>
<head>
	<title>Wireless Emporium Mobile Site - <%=curPage%></title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <script type="text/javascript" src="/includes/js/acCombo.js"></script>
	<script type="text/javascript" src="/includes/js/autocomplete-min.js"></script>
    <style type="text/css">
	body{font:13px/1.231 arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;margin:0;padding:0;}
	select,input,button,textarea,button{font:99% arial,helvetica,clean,sans-serif;}
	table{font-size:inherit;font:100%;}
	pre,code,kbd,samp,tt{font-family:monospace;*font-size:108%;line-height:100%;}
	.yui-skin-sam .yui-ac{position:relative;font-family:arial;font-size:100%;}
	.yui-skin-sam .yui-ac-input{position:absolute;width:100%;}
	.yui-skin-sam .yui-ac-container{position:absolute;top:1.6em;width:100%; font-size:20px;}
	.yui-skin-sam .yui-ac-content{position:absolute;width:100%;border:1px solid #808080;background:#fff;overflow:hidden;z-index:9050;}
	.yui-skin-sam .yui-ac-shadow{position:absolute;margin:.3em;width:100%;background:#000;-moz-opacity:.10;opacity:.10;filter:alpha(opacity=10);z-index:9049;}
	.yui-skin-sam .yui-ac iframe{opacity:0;filter:alpha(opacity=0);padding-right:.3em;padding-bottom:.3em;}
	.yui-skin-sam .yui-ac-content ul{margin:0;padding:0;width:100%;}
	.yui-skin-sam .yui-ac-content li{margin:0;padding:2px 5px;cursor:default;white-space:nowrap;list-style:none;zoom:1;}
	.yui-skin-sam .yui-ac-content li.yui-ac-prehighlight{background:#B3D4FF;}
	.yui-skin-sam .yui-ac-content li.yui-ac-highlight{background:#cccccc;color:#FFF;}
	#myAutoComplete {
	    width:15em; /* set width here or else widget will expand to fit its container */
    	padding-bottom:2em;
	}
	a { color:#ff6633; text-decoration:none; }
	a:hover { color:#ff6633; text-decoration:underline; }
    </style>
</head>
<body class="yui-skin-sam" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" style="width:100%; text-align:center">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr><td align="left" style="border-top:2px solid #666666; border-bottom:2px solid #666666; padding-top:0px;"><a href="/?r=1"><img src="/images/mobile/WE_logo_r2.png" border="0" /></a></td></tr>
    <tr bgcolor="#666666">
    	<td>
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td align="left" style="padding-left:10px;"><img src="/images/mobile/cart.jpg" border="0" /></td>
                    <td id="viewCart" align="left" width="100%" style="font-weight:bold; color:#FFF; padding-left:5px;">
	                    No Items
                    </td>
                    <td align="right" style="padding-right:10px; color:#FFF" nowrap="nowrap">
                    	<%
						noLogin = 1
						if noLogin = 1 then
						%>
                        1.800.305.1106
                        <%
						else
							if isnull(session("customer")) or len(session("customer")) < 1 then
						%>
                        <a href="https://m.wirelessemporium.com/mobile/login.asp?mm=1" style="color:#FFF; font-weight:bold;">Login</a>
                        <% 	else %>
                        Welcome <%=session("customer")%>&nbsp;(<a href="http://m.wirelessemporium.com/mobile/?logout=1" style="color:#FFF;">Logout</a>)
                        <% 	end if
						end if
						%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td align="left" style="background-image:url(/images/mobile/searchBG.gif); background-repeat:repeat-x">
            <form name="searchForm" method="get" action="/mobile/">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td><img src="/images/blank.gif" border="0" width="5" height="30" /></td>
                    <td valign="top" style="padding-top:5px;">
                    	<div id="myAutoComplete">
		                <input id="myInput" name="searchString" type="text" value="Find Your Product" onfocus="testContent(this.value)">
        		        <div id="myContainer"></div>
                        </div>
                    </td>
                    <td><img src="/images/blank.gif" border="0" width="10" height="30" /></td>
                    <td align="right" valign="top" style="padding-top:6px; padding-right:8px;"><input type="image" src="/images/mobile/buttons/button_search.png" align="absmiddle" border="0" alt="GO"></td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <% if userMsg <> "" then %>
    <tr bgcolor="#FF0000"><td align="center" style="font-weight:bold; color:#FFF;"><%=userMsg%></td></tr>
    <% end if %>
    <tr bgcolor="#ebebeb"><td align="left" style="color:#000000; font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%>&nbsp;Order Confirm</td></tr>
    <tr>
    	<td id="mobileContent" align="center" style="padding-left:10px; padding-right:10px;">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr><td align="left" style="font-weight:bold; font-size:18px;">Order Complete</td></tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td align="left">
                    	Thanks for your order we appreciate your business and loook forward to serving you!
                    </td>
                </tr>
                <tr><td><br /></td></tr>
                <tr><td align="left" style="font-weight:bold; font-size:18px;">Order Number: <%=orderID%></td></tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td style="font-weight:bold; font-size:18px; color:#ff6633; border-bottom:1px solid #ff6633;" align="left">Purchased Items</td>
                </tr>
                <tr>
                	<td align="center">
                    	<table border="0" cellpadding="3" cellspacing="0" width="100%">
                        	<tr bgcolor="#ff6633" style="font-weight:bold; color:#FFF">
                            	<td align="left">Item</td>
                                <td align="left">Qty</td>
                                <td align="left">Subtotal</td>
                            </tr>
                            <%
							bgColor = "#ffffff"
							itemTotal = 0
							totalRetail = 0
							lap = 0
							googleItemList = ""
							specialOffer = 0
							faceplateDiscountCnt = 0
							validProd = split("FP1|FP2|FP3|FP4|FP5|FP6","|")
							do while not rs.EOF
								lap = lap + 1
								partNumber = rs("partNumber")
								for i = 0 to ubound(validProd)
									if instr(partNumber,validProd(i)) > 0 then
										if not rs("noDiscount") then specialOffer = 1
									end if
								next
								if specialOffer = 1 then
									for i = 1 to cdbl(rs("qty"))
										faceplateDiscountCnt = faceplateDiscountCnt + 1
										if faceplateDiscountCnt = 2 then
											itemTotal = itemTotal + (rs("price_our")/2)
											faceplateDiscountCnt = 0
										else
											itemTotal = itemTotal + rs("price_our")
										end if
									next
								else
									itemTotal = itemTotal + (rs("qty") * rs("price_our"))
								end if
								totalRetail = totalRetail + (rs("price_our") * rs("qty"))
								specialOffer = 0
							%>
                            <tr bgcolor="<%=bgColor%>">
                            	<td align="left"><%=rs("itemDesc")%></td>
                                <td align="right"><%=rs("qty")%></td>
                                <td align="right"><%=rs("qty") * rs("price_our")%></td>
                            </tr>
                            <%
								googleItemList = googleItemList & "_gaq.push(['_addItem','" & orderID & "','" & rs("itemID") & "','" & rs("itemDesc") & "','','" & rs("price_our") & "','" & rs("qty") & "']); "
								rs.movenext
								if bgColor = "#ffffff" then bgColor = "#f0f0f0" else bgColor = "#ffffff"
								if billAddrRS("sState") = "CA" then tax = itemTotal * .0775
							loop
							grandTotal = itemTotal + tax + shipAmt
							%>
                            <tr>
                            	<td colspan="2" align="right" style="font-weight:bold; border-top:1px solid #999999;">Tax</td>
                                <td align="right" style="border-top:1px solid #999999;"><%=formatCurrency(tax,2)%></td>
                            </tr>
                            <tr>
                            	<td colspan="2" align="right" style="font-weight:bold;">Shipping</td>
                                <td align="right"><%=formatCurrency(shipAmt,2)%></td>
                            </tr>
                            <% if totalRetail > itemTotal then %>
                            <tr>
                            	<td colspan="2" align="right" style="font-weight:bold;">Total Before Discount</td>
                                <td align="right"><%=formatCurrency(totalRetail + tax + shipAmt,2)%></td>
                            </tr>
                            <tr>
                            	<td colspan="2" align="right" style="font-weight:bold;">Sale Discount</td>
                                <td align="right"><%=formatCurrency(totalRetail - itemTotal,2)%></td>
                            </tr>
                            <% end if %>
                            <tr>
                            	<td colspan="2" align="right" style="font-weight:bold;">Grand Total</td>
                                <td align="right"><%=formatCurrency(grandTotal,2)%></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td style="font-weight:bold; font-size:18px; color:#ff6633; border-bottom:1px solid #ff6633;" align="left">Shipping Address</td>
                </tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td>
                    	<table border="0" cellpadding="3" cellspacing="0">
                            <tr>
                                <td align="left"><%=billAddrRS("fname")%>&nbsp;<%=billAddrRS("lname")%></td>
                            </tr>
                            <tr>
                                <td align="left"><%=billAddrRS("sAddress1")%></td>
                            </tr>
                            <% if not isnull(billAddrRS("sAddress2")) and len(billAddrRS("sAddress2")) > 0 then %>
                            <tr>
                                <td align="left"><%=billAddrRS("sAddress2")%></td>
                            </tr>
                            <% end if %>
                            <tr>
                                <td align="left"><%=billAddrRS("sCity")%>,&nbsp;<%=billAddrRS("sState")%>&nbsp;<%=billAddrRS("sZip")%></td>
                            </tr>
                            <tr>
                                <td align="left"><%=billAddrRS("phone")%></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td style="font-weight:bold; font-size:18px; color:#ff6633; border-bottom:1px solid #ff6633;" align="left">Billing Address</td>
                </tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td>
                    	<table border="0" cellpadding="3" cellspacing="0">
                            <tr>
                                <td align="left"><%=billAddrRS("fname")%>&nbsp;<%=billAddrRS("lname")%></td>
                            </tr>
                            <tr>
                                <td align="left"><%=billAddrRS("bAddress1")%></td>
                            </tr>
                            <% if not isnull(billAddrRS("bAddress2")) and len(billAddrRS("bAddress2")) > 0 then %>
                            <tr>
                                <td align="left"><%=billAddrRS("bAddress2")%></td>
                            </tr>
                            <% end if %>
                            <tr>
                                <td align="left"><%=billAddrRS("bCity")%>,&nbsp;<%=billAddrRS("bState")%>&nbsp;<%=billAddrRS("bZip")%></td>
                            </tr>
                            <tr>
                                <td align="left"><%=billAddrRS("phone")%></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td align="left">
                    	You will receive an email confirmation shortly. Please contact us at 1.800.305.1106 
                        or <a href="mailto:service@wirelessemporium.com">service@wirelessemporium.com</a> if you have any questions
                    </td>
                </tr>
                <tr><td><br /></td></tr>
                <tr><td align="center"><input type="button" name="myAction" value="Return to Home Page" onclick="window.location='/?r=1'" /></td></tr>
                <tr><td><br /></td></tr>
            </table>
        </td>
    </tr>
    <tr bgcolor="#ebebeb">
    	<td>
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td nowrap="nowrap" style="padding-left:5px;"><a style="text-decoration:underline;">Contact Us</a></td>
                    <td style="color:#666; font-weight:bold; padding-left:10px; padding-right:10px;">|</td>
                    <td nowrap="nowrap"><a style="text-decoration:underline;">RSS</a></td>
                    <td style="color:#666; font-weight:bold; padding-left:10px; padding-right:10px;">|</td>
                    <td nowrap="nowrap"><a href="/?ms=full" style="text-decoration:underline;">Full Site</a></td>
                	<td align="right" width="100%"><img src="/images/mobile/we_logo_little.png" border="0" /></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div id="testZone"></div>
</body>
</html>
<%
	oConn.Close
	set oConn = nothing
%>
<script language="javascript" src="/includes/js/productKeywords.js"></script>
<script type="text/javascript">
	function testContent(val) {
		if (val == "Find Your Product") {
			document.searchForm.searchString.value = ""
		}
	}
	
	YAHOO.example.BasicLocal = function() {
		var oDS = new YAHOO.util.LocalDataSource(YAHOO.example.Data.arrayProducts);
		var oAC = new YAHOO.widget.AutoComplete("myInput", "myContainer", oDS);
		oAC.prehighlightClassName = "yui-ac-prehighlight";
		oAC.useShadow = false;
		oAC.maxResultsDisplayed = 3;
		
		return {
			oDS: oDS,
			oAC: oAC
		};
	}();
	
	function ajax(newURL,rLoc,loading) {
		if (newURL.indexOf("?") > 0) {
			newURL = newURL + '&ajaxID=' + Math.floor(Math.random()*999999)
		}
		else {
			newURL = newURL + '?ajaxID=' + Math.floor(Math.random()*999999)
		}
		
		if (loading == 1) {
			document.getElementById(rLoc).innerHTML = "<table width='100%'><tr><td align='center' style=background-image:url(/images/mobile/loading.gif);background-repeat:no-repeat;background-position:center;><img src='/images/blank.gif' border='0' width='1' height='60' /><br /><hr color='#ff6633' /><br /><img src='/images/blank.gif' border='0' width='1' height='40' /><br /><div align='center' style='font-weight:bold; font-size:18px;'>Loading Content...</div><hr color='#ff6633' /><br /><img src='/images/blank.gif' border='0' width='1' height='100' /></td></tr></table>"
		}
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						window.location = "/mobile/?r=1"
					}
					else {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				}
				else {
					//alert("Error performing action")
				}
			}
		};
		httpRequest.send(null);
	}
</script>
<!-- Google Analytics Start -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1311669-2']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_addTrans','<%=orderID%>','m.wirelessemporium.com','<%=grandTotal%>','<%=tax%>','<%=shipAmt%>','<%=sCity%>','<%=sState%>','USA']);
  <%=googleItemList%>
  _gaq.push(['_trackTrans']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Google Analytics End -->