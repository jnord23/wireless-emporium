<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<%
	response.buffer = true
	response.expires = -1
	response.ExpiresAbsolute = Now() - 1
	response.CacheControl = "no-cache"
	Session.Timeout = 20
	
	sql = "insert into we_mobileTrackingPages (sessionID,page) values('" & request.Cookies("mySession") & "','" & request.ServerVariables("URL") & "?" & request.ServerVariables("QUERY_STRING") & "')"
	session("errorSQL") = sql
	oConn.execute(sql)
	
	curPage = "Checkout"
	userMsg = ""
	ajaxRedir = ""
	
	login = SQLquote(request.Form("login"))
	
	if isnull(login) or len(login) < 1 then login = ""
	
	if len(request.Cookies("mySession")) > 0 then
		useSessionID = request.Cookies("mySession")
	else
		response.Cookies("mySession") = session.SessionID
		response.Cookies("mySession").expires = date + 30
		useSessionID = session.SessionID
	end if
	
	if isnull(session("acctID")) or len(session("acctID")) < 1 then	response.Redirect("https://m.wirelessemporium.com/mobile/login.asp?a=")
	
	function fOpenConn
		'nothing
	end function
	function fCloseConn
		'nothing
	end function
	
	sql = "select isnull(sum(qty), 0) as cartItems from shoppingCart where sessionID = " & useSessionID & " or accountID = " & session("acctID")
	session("errorSQL") = sql
	Set cartRS = Server.CreateObject("ADODB.Recordset")
	cartRS.open sql, oConn, 0, 1
	cartItems = cartRS("cartItems")
	cartRS = null
	
	sql = "select a.itemID, b.itemDesc, b.price_our, a.qty, b.itemWeight from shoppingCart a left join we_items b on a.itemID = b.itemID where a.sessionID = " & useSessionID & " or a.accountID = " & session("acctID")
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	sWeight = 0
	nProdIdCount = 0
	do while not rs.EOF
		nProdIdCount = nProdIdCount + 1
		curItemID = rs("itemID")
		curItemDesc = rs("itemDesc")
		curPrice = rs("price_our")
		curWeight = rs("itemWeight")
		curQty = rs("qty")
		if not isnull(curWeight) then
			sWeight = sWeight + (cdbl(curWeight) * curQty)
		else
			sWeight = sWeight + 1
		end if
		PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & curItemID & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & curItemDesc & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & curQty & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & curPrice & """>" & vbcrlf
		rs.movenext
	loop
	
	fname = SQLquote(request.Form("fname"))
	lname = SQLquote(request.Form("lname"))
	phone = SQLquote(request.Form("phone"))
	s_addr1 = SQLquote(request.Form("addr1"))
	s_addr2 = SQLquote(request.Form("addr2"))
	s_city = SQLquote(request.Form("city"))
	s_state = SQLquote(request.Form("state"))
	s_zip = SQLquote(request.Form("zip"))
	
	if isnull(fname) or len(fname) < 1 then fname = ""
	if isnull(lname) or len(lname) < 1 then lname = ""
	if isnull(phone) or len(phone) < 1 then phone = ""
	if isnull(s_addr1) or len(s_addr1) < 1 then s_addr1 = ""
	if isnull(s_addr2) or len(s_addr2) < 1 then s_addr2 = ""
	if isnull(s_city) or len(s_city) < 1 then s_city = ""
	if isnull(s_state) or len(s_state) < 1 then s_state = ""
	if isnull(s_zip) or len(s_zip) < 1 then s_zip = ""
	
	if fname <> "" then
		if isnumeric(s_zip) then
			for i = len(s_zip) to 4
				s_zip = "0" & s_zip
			next
		end if
		sql = "update we_accounts set fname = '" & fname & "', lname = '" & lname & "', sAddress1 = '" & s_addr1 & "', sAddress2 = '" & s_addr2 & "', sCity = '" & s_city & "', sState = '" & s_state & "', sZip = '" & s_zip & "' where accountID = " & session("acctID")
		session("errorSQL") = sql
		oConn.execute(sql)
		if instr(fname," ") > 0 then
			session("customer") = left(fname,instr(fname," "))
		else
			session("customer") = fname
		end if
	end if
	
	if isnull(session("acctID")) then
		response.Redirect("/mobile/login.asp")
	elseif session("acctID") = 0 then
		response.Redirect("/mobile/login.asp")
	end if
	
	sql = "select * from we_accounts where accountID = " & session("acctID")
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 3, 3
	
	baseCrumb = "<a style='font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?r=1','mobileContent',1)>Main Menu&nbsp;&gt;</a>"
%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html>
<head>
	<title>Wireless Emporium Mobile Site - <%=curPage%></title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <script type="text/javascript" src="/includes/js/acCombo.js"></script>
	<script type="text/javascript" src="/includes/js/autocomplete-min.js"></script>
    <style type="text/css">
	body{font:13px/1.231 arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;margin:0;padding:0;}
	select,input,button,textarea,button{font:99% arial,helvetica,clean,sans-serif;}
	table{font-size:inherit;font:100%;}
	pre,code,kbd,samp,tt{font-family:monospace;*font-size:108%;line-height:100%;}
	.yui-skin-sam .yui-ac{position:relative;font-family:arial;font-size:100%;}
	.yui-skin-sam .yui-ac-input{position:absolute;width:100%;}
	.yui-skin-sam .yui-ac-container{position:absolute;top:1.6em;width:100%; font-size:20px;}
	.yui-skin-sam .yui-ac-content{position:absolute;width:100%;border:1px solid #808080;background:#fff;overflow:hidden;z-index:9050;}
	.yui-skin-sam .yui-ac-shadow{position:absolute;margin:.3em;width:100%;background:#000;-moz-opacity:.10;opacity:.10;filter:alpha(opacity=10);z-index:9049;}
	.yui-skin-sam .yui-ac iframe{opacity:0;filter:alpha(opacity=0);padding-right:.3em;padding-bottom:.3em;}
	.yui-skin-sam .yui-ac-content ul{margin:0;padding:0;width:100%;}
	.yui-skin-sam .yui-ac-content li{margin:0;padding:2px 5px;cursor:default;white-space:nowrap;list-style:none;zoom:1;}
	.yui-skin-sam .yui-ac-content li.yui-ac-prehighlight{background:#B3D4FF;}
	.yui-skin-sam .yui-ac-content li.yui-ac-highlight{background:#cccccc;color:#FFF;}
	#myAutoComplete {
	    width:15em; /* set width here or else widget will expand to fit its container */
    	padding-bottom:2em;
	}
	a { color:#ff6633; text-decoration:none; }
	a:hover { color:#ff6633; text-decoration:underline; }
    </style>
</head>
<body class="yui-skin-sam" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" style="width:100%; text-align:center">
<% session("errorSQL") = "cartItems:" & cartItems & "<br>sWeight:" & sWeight %>
<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.asp" style="margin:0px;">
    <input type="hidden" name="numItems" value="<%=cartItems%>">
    <input type="hidden" name="paymentAmount" value="<%=session("totalSubtotal")%>">
    <input type="hidden" name="ItemsAndWeight" value="<%=cStr(cartItems) & "|" & cStr(sWeight)%>">
    <input type="hidden" name="shipZip" value="<%=rs("sZip")%>">
    <input type="hidden" name="mobileOrder" value="1">
    <%=PPhtml%>
</form>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr><td align="left" style="border-top:2px solid #666666; border-bottom:2px solid #666666; padding-top:0px;"><a href="/?r=1"><img src="/images/mobile/WE_logo_r2.png" border="0" /></a></td></tr>
    <tr bgcolor="#666666">
    	<td>
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td align="left" style="padding-left:10px;"><img src="/images/mobile/cart.jpg" border="0" /></td>
                    <td id="viewCart" align="left" width="100%" style="font-weight:bold; color:#FFF; padding-left:5px;"<% if cartItems > 0 then %> onclick="ajax('/ajax/mobileCheckout.asp','mobileContent',1)"<% end if %>>
                    	<% if cartItems > 0 then %>
                        <%=cartItems%> Item(s)
                        <% else %>
                        No Items
                        <% end if %>
                    </td>
                    <td align="right" style="padding-right:10px; color:#FFF" nowrap="nowrap">
                    	<%
						noLogin = 1
						if noLogin = 1 then
						%>
                        1.800.305.1106
                        <%
						else
							if isnull(session("customer")) or len(session("customer")) < 1 then
						%>
                        <a href="https://m.wirelessemporium.com/mobile/login.asp?mm=1" style="color:#FFF; font-weight:bold;">Login</a>
                        <% 	else %>
                        Welcome <%=session("customer")%>&nbsp;(<a href="http://m.wirelessemporium.com/mobile/?logout=1" style="color:#FFF;">Logout</a>)
                        <% 	end if
						end if
						%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td align="left" style="background-image:url(/images/mobile/searchBG.gif); background-repeat:repeat-x">
            <form name="searchForm" method="get" action="/mobile/">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td><img src="/images/blank.gif" border="0" width="5" height="30" /></td>
                    <td valign="top" style="padding-top:5px;">
                    	<div id="myAutoComplete">
		                <input id="myInput" name="searchString" type="text" value="Find Your Product" onfocus="testContent(this.value)">
        		        <div id="myContainer"></div>
                        </div>
                    </td>
                    <td><img src="/images/blank.gif" border="0" width="10" height="30" /></td>
                    <td align="right" valign="top" style="padding-top:6px; padding-right:8px;"><input type="image" src="/images/mobile/buttons/button_search.png" align="absmiddle" border="0" alt="GO"></td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <% if userMsg <> "" then %>
    <tr bgcolor="#FF0000"><td align="center" style="font-weight:bold; color:#FFF;"><%=userMsg%></td></tr>
    <% end if %>
    <tr bgcolor="#ebebeb"><td align="left" style="color:#000000; font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%>&nbsp;Shipping</td></tr>
    <tr>
    	<td align="left" style="color:#ff6633; font-size:24px; font-weight:bold; padding-bottom:5px; padding-top:10px; padding-left:5px;">Checkout Step 2 of 3</td>
    </tr>
    <tr><td style="border-bottom:1px solid #ff6633;"></td></tr>
    <tr>
    	<td>
        	<div style="font-weight:bold; font-size:18px; color:#ffffff; background-color:#ff6633; padding:2px;">Checkout with PayPal</div>
            <div style="padding-top:10px; text-align:center; width:100%"><a style="cursor:pointer;" onclick="document.PaypalForm.submit()"><img src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" border="0" /></a></div>
        </td>
    </tr>
    <tr>
    	<td>
        	<div style="font-weight:bold; font-size:18px; color:#ffffff; background-color:#ff6633; padding:2px;">Checkout with Credit Card</div>
        </td>
    </tr>
    <tr>
    	<td id="mobileContent" align="center">
        	<form name="shippingInfo" method="post" action="https://m.wirelessemporium.com/mobile/checkout.asp">
        	<table border="0" cellpadding="3" cellpadding="0">
                <tr>
                	<td style="font-weight:bold; font-size:18px; color:#ff6633; border-bottom:1px solid #ff6633;" align="left">Shipping Address</td>
                </tr>
                <tr>
                	<td align="left">
						<%=rs("fname")%>&nbsp;<%=rs("lname")%><br />
                        <%=rs("sAddress1")%><br />
                        <% if not isnull(rs("sAddress2")) and len(rs("sAddress2")) > 0 then response.Write(rs("sAddress2") & "<br />") %>
                        <%=rs("sCity")%>, <%=rs("sState")%>&nbsp;<%=rs("sZip")%><br />
                        <% if isnull(rs("sCountry")) or len(rs("sCountry")) < 1 then response.Write("USA") else response.Write(rs("sCountry")) %><br />
                        <%=rs("phone")%>
                        <br /><br />
                        <a href="#" style="color:#ff6633;" onclick="document.getElementById('editShipAddr').style.display=''">Edit Shipping Address</a>
                    </td>
                </tr>
                <tr>
                	<td id="editShipAddr" style="display:none;">
                    	<table border="0" cellpadding="3" cellspacing="0">
                            <tr><td><br /></td></tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">First Name:</td>
                                <td style="color:#F00;" align="left"><input type="text" name="fname" value="<%=rs("fname")%>" size="32" />&nbsp;*</td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">Last Name:</td>
                                <td style="color:#F00;" align="left"><input type="text" name="lname" value="<%=rs("lname")%>" size="32" />&nbsp;*</td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">Phone:</td>
                                <td style="color:#F00;" align="left"><input type="text" name="phone" value="<%=rs("phone")%>" size="32" />&nbsp;*</td>
                            </tr>
                            <tr><td><br /></td></tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">Address 1:</td>
                                <td style="color:#F00;" align="left"><input type="text" name="addr1" value="<%=rs("sAddress1")%>" size="32" />&nbsp;*</td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">Address 2:</td>
                                <td style="color:#F00;" align="left"><input type="text" name="addr2" value="<%=rs("sAddress2")%>" size="32" />&nbsp;*</td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">City:</td>
                                <td style="color:#F00;" align="left"><input type="text" name="city" value="<%=rs("sCity")%>" size="32" />&nbsp;*</td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">State:</td>
                                <td style="color:#F00;" align="left">
                                    <select name="state">
                                        <%getStates(rs("sState"))%>
                                    </select>
                                    &nbsp;*
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">Zip/Postal Code:</td>
                                <td style="color:#F00;" align="left"><input type="text" name="zip" value="<%=rs("sZip")%>" size="32" />&nbsp;*</td>
                            </tr>
                            <tr>
                            	<td colspan="2" align="right"><input type="button" name="myAction" value="Update Shipping Address" onclick="document.shippingInfo.action='https://m.wirelessemporium.com/mobile/shipping.asp';document.shippingInfo.submit()" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                	<td style="font-weight:bold; font-size:18px; color:#ff6633; border-bottom:1px solid #ff6633;" align="left">Shipping Options</td>
                </tr>
                <tr>
                	<td align="left">
	                  	<input type="hidden" name="shipcost0" value="0"><input type="radio" name="shiptype" id="rad_ship1" value="0" onClick="updateGrandTotal();"<%if shiptype = "0" or shiptype = "" then response.write " checked"%>><span style="color:#db3300;font-weight:bold;">FREE SHIPPING</span> <span id="Shipping0">USPS First Class (4-10 business days)</span><br /><br />
						<input type="hidden" name="shipcost2" value="6.99"><input type="radio" name="shiptype" id="rad_ship2" value="2" onClick="updateGrandTotal();"<%if shiptype = "2" then response.write " checked"%>><span id="Shipping2">USPS Priority Mail (2-4 business days) - $6.99</span><br /><br />
						<input type="hidden" name="shipcost3" value="19.99"><input type="radio" name="shiptype" id="rad_ship3" value="3" onClick="updateGrandTotal();"<%if shiptype = "3" then response.write " checked"%>><span id="Shipping3">USPS Express Mail (1-2 business days) - $19.99</span>
                        <%
						useZip = rs("sZip")
						on error resume next
						if useZip <> "" then
							response.Write replace(UPScode(rs("sZip"),sWeight,0,0,0,0),"<br>","<br /><br />")
						end if
						on error goto 0
						%>
                    </td>
                </tr>
                <tr><td style="border-bottom:1px solid #ff6633;"></td></tr>
                <tr>
                	<td align="left"><input type="image" src="/images/mobile/buttons/button_contcheckout.png" name="myAction" onclick="document.shippingInfo.submit()" /></td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr bgcolor="#ebebeb">
    	<td>
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td nowrap="nowrap" style="padding-left:5px;"><a style="text-decoration:underline;">Contact Us!!</a></td>
                    <td style="color:#666; font-weight:bold; padding-left:10px; padding-right:10px;">|</td>
                    <td nowrap="nowrap"><a style="text-decoration:underline;">RSS</a></td>
                    <td style="color:#666; font-weight:bold; padding-left:10px; padding-right:10px;">|</td>
                    <td nowrap="nowrap"><a href="/?ms=full" style="text-decoration:underline;">Full Site</a></td>
                	<td align="right" width="100%"><img src="/images/mobile/we_logo_little.png" border="0" /></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div id="testZone"></div>
</body>
</html>
<%
	oConn.Close
	set oConn = nothing
%>
<script language="javascript" src="/includes/js/productKeywords.js"></script>
<script type="text/javascript">
	function updateGrandTotal() {
		//nothing
	}
	
	function testContent(val) {
		if (val == "Find Your Product") {
			document.searchForm.searchString.value = ""
		}
	}
	
	YAHOO.example.BasicLocal = function() {
		var oDS = new YAHOO.util.LocalDataSource(YAHOO.example.Data.arrayProducts);
		var oAC = new YAHOO.widget.AutoComplete("myInput", "myContainer", oDS);
		oAC.prehighlightClassName = "yui-ac-prehighlight";
		oAC.useShadow = false;
		oAC.maxResultsDisplayed = 3;
		
		return {
			oDS: oDS,
			oAC: oAC
		};
	}();
	
	function ajax(newURL,rLoc,loading) {
		if (newURL.indexOf("?") > 0) {
			newURL = newURL + '&ajaxID=' + Math.floor(Math.random()*999999)
		}
		else {
			newURL = newURL + '?ajaxID=' + Math.floor(Math.random()*999999)
		}
		
		if (loading == 1) {
			document.getElementById(rLoc).innerHTML = "<table width='100%'><tr><td align='center' style=background-image:url(/images/mobile/loading.gif);background-repeat:no-repeat;background-position:center;><img src='/images/blank.gif' border='0' width='1' height='60' /><br /><hr color='#ff6633' /><br /><img src='/images/blank.gif' border='0' width='1' height='40' /><br /><div align='center' style='font-weight:bold; font-size:18px;'>Loading Content...</div><hr color='#ff6633' /><br /><img src='/images/blank.gif' border='0' width='1' height='100' /></td></tr></table>"
		}
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						window.location = "/mobile/?r=1"
					}
					else {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				}
				else {
					//alert("Error performing action")
				}
			}
		};
		httpRequest.send(null);
	}
</script>
<!-- Google Analytics Start -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1311669-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Google Analytics End -->