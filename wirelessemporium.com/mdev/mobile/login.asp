<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<%
	response.buffer = true
	response.expires = -1
	response.ExpiresAbsolute = Now() - 1
	response.CacheControl = "no-cache"
	Session.Timeout = 20
	
	if isnull(request.Cookies("mySession")) or len(request.Cookies("mySession")) < 1 then response.Cookies("mySession") = session.SessionID
	
	sql = "insert into we_mobileTrackingPages (sessionID,page) values('" & request.Cookies("mySession") & "','" & request.ServerVariables("URL") & "?" & request.ServerVariables("QUERY_STRING") & "')"
	session("errorSQL") = sql
	oConn.execute(sql)
	
	curPage = "Login"
	userMsg = ""
	ajaxRedir = ""
	
	mm = SQLquote(request.QueryString("mm"))
	login = SQLquote(request.Form("login"))
	loginPW = SQLquote(request.Form("loginPW"))
	email = SQLquote(request.Form("email"))
	pass1 = SQLquote(request.Form("pass1"))
	fname = SQLquote(request.Form("fname"))
	lname = SQLquote(request.Form("lname"))
	phone = SQLquote(request.Form("phone"))
	s_addr1 = SQLquote(request.Form("addr1"))
	s_addr2 = SQLquote(request.Form("addr2"))
	s_city = SQLquote(request.Form("city"))
	s_state = SQLquote(request.Form("state"))
	s_zip = SQLquote(request.Form("zip"))
	b_addr1 = SQLquote(request.Form("b_addr1"))
	b_addr2 = SQLquote(request.Form("b_addr2"))
	b_city = SQLquote(request.Form("b_city"))
	b_state = SQLquote(request.Form("b_state"))
	b_zip = SQLquote(request.Form("b_zip"))
	emailOpt = SQLquote(request.Form("emailOpt"))
	totalSubtotal = SQLquote(request.Form("totalSubtotal"))
	
	if isnull(mm) or len(mm) < 1 then mm = 0
	if isnull(login) or len(login) < 1 then login = ""
	if isnull(loginPW) or len(loginPW) < 1 then loginPW = ""
	if isnull(email) or len(email) < 1 then email = ""
	if isnull(pass1) or len(pass1) < 1 then pass1 = ""
	if isnull(fname) or len(fname) < 1 then fname = ""
	if isnull(lname) or len(lname) < 1 then lname = ""
	if isnull(phone) or len(phone) < 1 then phone = ""
	if isnull(s_addr1) or len(s_addr1) < 1 then s_addr1 = ""
	if isnull(s_addr2) or len(s_addr2) < 1 then s_addr2 = ""
	if isnull(s_city) or len(s_city) < 1 then s_city = ""
	if isnull(s_state) or len(s_state) < 1 then s_state = ""
	if isnull(s_zip) or len(s_zip) < 1 then s_zip = ""
	if isnull(b_addr1) or len(b_addr1) < 1 then b_addr1 = ""
	if isnull(b_addr2) or len(b_addr2) < 1 then b_addr2 = ""
	if isnull(b_city) or len(b_city) < 1 then b_city = ""
	if isnull(b_state) or len(b_state) < 1 then b_state = ""
	if isnull(b_zip) or len(b_zip) < 1 then b_zip = ""
	if isnull(emailOpt) or len(emailOpt) < 1 then emailOpt = "N" else emailOpt = "Y"
	if isnull(totalSubtotal) or len(totalSubtotal) < 1 then totalSubtotal = 0
	
	session("totalSubtotal") = totalSubtotal
	
	if len(request.Cookies("mySession")) > 0 then
		useSessionID = request.Cookies("mySession")
	else
		response.Cookies("mySession") = session.SessionID
		response.Cookies("mySession").expires = date + 30
		useSessionID = session.SessionID
	end if
	
	function fOpenConn
		'nothing
	end function
	function fCloseConn
		'nothing
	end function
	
	if login <> "" then
		sql = "select accountid, fname, lname, pword from we_accounts where email = '" & login & "'"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 3, 3
		if rs.EOF then
			userMsg = "NO ACCOUNT EXISTS WITH THAT EMAIL ADDRESS"
			ajaxRedir = "/ajax/mobileContent.asp?login=1"
		else
			if loginPW <> rs("pword") then
				userMsg = "PASSWORD DOES NOT MATCH THE EMAIL ADDRESS"
				ajaxRedir = "/ajax/mobileContent.asp?login=1&email=" & login
			else
				if instr(rs("fname")," ") > 0 then
					session("customer") = left(rs("fname"),instr(rs("fname")," "))
				else
					session("customer") = rs("fname")
				end if
				session("acctID") = rs("accountid")
				
				sql = "update we_mobileTracking set logins = logins + 1, acctID = " & rs("accountid") & " where sessionID = '" & request.Cookies("mySession") & "'"
				session("errorSQL") = sql
				oConn.execute(sql)
			end if
		end if
	elseif email <> "" then
		if len(s_state) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",s_state) > 0 then
			s_country = "CANADA"
		else
			s_country = "USA"
		end if
		if len(b_state) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",b_state) > 0 then
			b_country = "CANADA"
		else
			b_country = "USA"
		end if
		sql = "SET NOCOUNT ON; INSERT INTO WE_accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,pword,dateEntered,CustomerIP) VALUES ("
		SQL = SQL & "'" & SQLquote(fname) & "', "
		SQL = SQL & "'" & SQLquote(lname) & "', "
		SQL = SQL & "'" & SQLquote(b_addr1) & "', "
		SQL = SQL & "'" & SQLquote(b_addr2) & "', "
		SQL = SQL & "'" & SQLquote(b_city) & "', "
		SQL = SQL & "'" & SQLquote(b_state) & "', "
		SQL = SQL & "'" & SQLquote(b_zip) & "', "
		SQL = SQL & "'" & SQLquote(b_country) & "', "
		SQL = SQL & "'" & SQLquote(email) & "', "
		SQL = SQL & "'" & SQLquote(phone) & "', "
		SQL = SQL & "'" & SQLquote(s_addr1) & "', "
		SQL = SQL & "'" & SQLquote(s_addr2) & "', "
		SQL = SQL & "'" & SQLquote(s_city) & "', "
		SQL = SQL & "'" & SQLquote(s_state) & "', "
		SQL = SQL & "'" & SQLquote(s_zip) & "', "
		SQL = SQL & "'" & SQLquote(s_country) & "', "
		SQL = SQL & "'" & SQLquote(pass1) & "', "
		SQL = SQL & "'" & now & "', "
		SQL = SQL & "'" & request.ServerVariables("remote_addr") & "')"
		SQL = SQL & "SELECT @@IDENTITY AS newAccountID; "
		SQL = SQL & "SET NOCOUNT OFF;"
		session("errorSQL") = SQL
		'response.write "<p>" & SQL & "</p><p>" & emailOpt & "</p>" & vbcrlf
		set RS = oConn.execute(SQL)
		session("acctID") = cdbl(RS("newAccountID"))
		oConn.execute ("sp_ModifyEmailOpt '" & SQLquote(fname) & "','" & email & "','" & emailOpt & "'")
		
		sql = "update we_mobileTracking set registration = 1 where sessionID = '" & request.Cookies("mySession") & "'"
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	session("errorSQL") = "AccountID:" & session("acctID")
	if isnull(session("acctID")) or len(session("acctID")) < 1 then
		session("acctID") = 0
	elseif not isnumeric(session("acctID")) then
		session("acctID") = 0
	elseif session("acctID") > 0 then
		if mm = 1 then
			response.Redirect("http://m.wirelessemporium.com/mobile/")
		else
			response.Redirect("https://m.wirelessemporium.com/mobile/shipping.asp")
		end if
	end if
	
	sql = "select sum(qty) as cartItems from shoppingCart where sessionID = " & useSessionID & " or accountID = " & session("acctID")
	session("errorSQL") = sql
	Set cartRS = Server.CreateObject("ADODB.Recordset")
	cartRS.open sql, oConn, 3, 3
	cartItems = cartRS("cartItems")
	cartRS = null
	
	baseCrumb = "<a style='font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?r=1','mobileContent',1)>Main Menu&nbsp;&gt;</a>"
%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html>
<head>
	<title>Wireless Emporium Mobile Site - <%=curPage%></title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <script type="text/javascript" src="/includes/js/acCombo.js"></script>
	<script type="text/javascript" src="/includes/js/autocomplete-min.js"></script>
    <style type="text/css">
	body{font:13px/1.231 arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;margin:0;padding:0;}
	select,input,button,textarea,button{font:99% arial,helvetica,clean,sans-serif;}
	table{font-size:inherit;font:100%;}
	pre,code,kbd,samp,tt{font-family:monospace;*font-size:108%;line-height:100%;}
	.yui-skin-sam .yui-ac{position:relative;font-family:arial;font-size:100%;}
	.yui-skin-sam .yui-ac-input{position:absolute;width:100%;}
	.yui-skin-sam .yui-ac-container{position:absolute;top:1.6em;width:100%; font-size:20px;}
	.yui-skin-sam .yui-ac-content{position:absolute;width:100%;border:1px solid #808080;background:#fff;overflow:hidden;z-index:9050;}
	.yui-skin-sam .yui-ac-shadow{position:absolute;margin:.3em;width:100%;background:#000;-moz-opacity:.10;opacity:.10;filter:alpha(opacity=10);z-index:9049;}
	.yui-skin-sam .yui-ac iframe{opacity:0;filter:alpha(opacity=0);padding-right:.3em;padding-bottom:.3em;}
	.yui-skin-sam .yui-ac-content ul{margin:0;padding:0;width:100%;}
	.yui-skin-sam .yui-ac-content li{margin:0;padding:2px 5px;cursor:default;white-space:nowrap;list-style:none;zoom:1;}
	.yui-skin-sam .yui-ac-content li.yui-ac-prehighlight{background:#B3D4FF;}
	.yui-skin-sam .yui-ac-content li.yui-ac-highlight{background:#cccccc;color:#FFF;}
	#myAutoComplete {
	    width:15em; /* set width here or else widget will expand to fit its container */
    	padding-bottom:2em;
	}
	a { color:#ff6633; text-decoration:none; }
	a:hover { color:#ff6633; text-decoration:underline; }
    </style>
</head>
<body class="yui-skin-sam" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" style="width:100%; text-align:center">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr><td align="left" style="border-top:2px solid #666666; border-bottom:2px solid #666666; padding-top:0px;"><a href="/?r=1"><img src="/images/mobile/WE_logo_r2.png" border="0" /></a></td></tr>
    <tr bgcolor="#666666">
    	<td>
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td align="left" style="padding-left:10px;"><img src="/images/mobile/cart.jpg" border="0" /></td>
                    <td id="viewCart" align="left" width="100%" style="font-weight:bold; color:#FFF; padding-left:5px;"<% if cartItems > 0 then %> onclick="ajax('/ajax/mobileCheckout.asp','mobileContent',1)"<% end if %>>
                    	<% if cartItems > 0 then %>
                        <%=cartItems%> Item(s)
                        <% else %>
                        No Items
                        <% end if %>
                    </td>
                    <td align="right" style="padding-right:10px; color:#FFF" nowrap="nowrap">
                    	<%
						noLogin = 1
						if noLogin = 1 then
						%>
                        1.800.305.1106
                        <%
						else
							if isnull(session("customer")) or len(session("customer")) < 1 then
						%>
                        <a href="https://m.wirelessemporium.com/mobile/login.asp?mm=1" style="color:#FFF; font-weight:bold;">Login</a>
                        <% 	else %>
                        Welcome <%=session("customer")%>&nbsp;(<a href="http://m.wirelessemporium.com/mobile/?logout=1" style="color:#FFF;">Logout</a>)
                        <% 	end if
						end if
						%>                        
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td align="left" style="background-image:url(/images/mobile/searchBG.gif); background-repeat:repeat-x">
            <form name="searchForm" method="get" action="http://m.wirelessemporium.com/mobile/">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td><img src="/images/blank.gif" border="0" width="5" height="30" /></td>
                    <td valign="top" style="padding-top:5px;">
                    	<div id="myAutoComplete">
		                <input id="myInput" name="searchString" type="text" value="Find Your Product" onfocus="testContent(this.value)">
        		        <div id="myContainer"></div>
                        </div>
                    </td>
                    <td><img src="/images/blank.gif" border="0" width="10" height="30" /></td>
                    <td align="right" valign="top" style="padding-top:6px; padding-right:8px;"><input type="image" src="/images/mobile/buttons/button_search.png" align="absmiddle" border="0" alt="GO"></td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <% if userMsg <> "" then %>
    <tr bgcolor="#FF0000"><td align="center" style="font-weight:bold; color:#FFF;"><%=userMsg%></td></tr>
    <% end if %>
    <tr bgcolor="#ebebeb"><td align="left" style="color:#000000; font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%>&nbsp;Login</td></tr>
    <tr>
    	<td id="mobileContent" align="center" style="display:none;">
        	<table border="0" cellpadding="3" cellspacing="0">
                <tr><td><br /></td></tr>
                <tr bgcolor="#ff6633" style="color:#FFF; font-weight:bold; font-size:18px;">
                	<td align="left">Login</td>
                </tr>
                <tr>
                	<td style="border:1px solid #ff6633;">
                    	<form method="post" name="loginForm" action="https://m.wirelessemporium.com/mobile/login.asp?mm=<%=mm%>">
                        <table border="0" cellpadding="3" cellspacing="0">
                        	<tr>
                            	<td align="right" style="font-weight:bold;">Email:</td>
                                <td><input type="text" name="login" value="<%=login%>" size="23" /></td>
                            </tr>
                            <tr>
                            	<td align="right" style="font-weight:bold;">Password:</td>
                                <td><input type="password" name="loginPW" value="" size="23" /></td>
                            </tr>
                            <tr>
                            	<td colspan="2" align="right"><input type="submit" name="myAction" value="Login" /></td>
                            </tr>
                        </table>
                        </form>
                    </td>
                </tr>
                <tr><td><br /></td></tr>
                <tr bgcolor="#ff6633" style="color:#FFF; font-weight:bold; font-size:18px;">
                	<td align="left">Register New Account</td>
                </tr>
                <tr><td align="right" style="border:1px solid #ff6633;"><input type="button" name="myAction" value="Register Account" onclick="document.getElementById('mobileContent').style.display='none';document.getElementById('regForm').style.display=''" /></td></tr>
                <tr><td><br /></td></tr>
            </table>
        </td>
    	<td align="center" id="regForm">
        	<form name="newAcct" method="post" action="https://m.wirelessemporium.com/mobile/login.asp" onsubmit="return(chkForm())">
        	<table border="0" cellpadding="3" cellspacing="0" style="padding-top:10px;">
            	<!--<tr>
                	<td colspan="2" style="font-weight:bold; font-size:18px; color:#ff6633; border-bottom:1px solid #ff6633;" align="left">Account Registration (<a onclick="document.getElementById('mobileContent').style.display='';document.getElementById('regForm').style.display='none'">Already have an account? Click here to login</a>)</td>
                </tr>-->
                <tr>
                	<td align="right" style="font-weight:bold;">Email Address:</td>
                    <td style="color:#F00;" align="left"><input type="text" name="email" value="" size="32" onchange="getCustomerData(this.value)" />&nbsp;*</td>
                </tr>
                <!--
                <tr>
                	<td align="right" style="font-weight:bold;">Password:</td>
                    <td style="color:#F00;" align="left"><input type="password" name="pass1" value="" size="32" />&nbsp;*</td>
                </tr>
                <tr>
                	<td align="right" style="font-weight:bold;">Confirm Password:</td>
                    <td style="color:#F00;" align="left"><input type="password" name="pass2" value="" size="32" />&nbsp;*</td>
                </tr>
                -->
                <tr><td><br /></td></tr>
                <tr>
                	<td align="right" style="font-weight:bold;">First Name:</td>
                    <td style="color:#F00;" align="left"><input type="text" name="fname" value="" size="32" />&nbsp;*</td>
                </tr>
                <tr>
                	<td align="right" style="font-weight:bold;">Last Name:</td>
                    <td style="color:#F00;" align="left"><input type="text" name="lname" value="" size="32" />&nbsp;*</td>
                </tr>
                <tr>
                	<td align="right" style="font-weight:bold;">Phone:</td>
                    <td style="color:#F00;" align="left"><input type="text" name="phone" value="" size="32" />&nbsp;*</td>
                </tr>
                <tr><td><br /></td></tr>
                <tr><td colspan="2" align="left" style="font-weight:bold; font-size:18px; color:#ff6633; border-bottom:1px solid #ff6633;">Shipping Address</td></tr>
                <tr>
                	<td colspan="2" align="left">
                    	Please input the delivery address where you would like your order Shipped To.<br />
						ALL ORDERS are shipped exclusively via the US Postal Service. Please be as accurate as possible to ensure the timeliness of delivery.<br />
						This would be the same address where you receive your mail.
                    </td>
                </tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td align="right" style="font-weight:bold;">Address 1:</td>
                    <td style="color:#F00;" align="left"><input type="text" name="addr1" value="" size="32" />&nbsp;*</td>
                </tr>
                <tr>
                	<td align="right" style="font-weight:bold;">Address 2:</td>
                    <td style="color:#F00;" align="left"><input type="text" name="addr2" value="" size="32" />&nbsp;*</td>
                </tr>
                <tr>
                	<td align="right" style="font-weight:bold;">City:</td>
                    <td style="color:#F00;" align="left"><input type="text" name="city" value="" size="32" />&nbsp;*</td>
                </tr>
                <tr>
                	<td align="right" style="font-weight:bold;">State:</td>
                    <td style="color:#F00;" align="left">
                    	<select name="state">
							<%getStates(sstate)%>
                        </select>
                        &nbsp;*
                    </td>
                </tr>
                <tr>
                	<td align="right" style="font-weight:bold;">Zip/Postal Code:</td>
                    <td style="color:#F00;" align="left"><input type="text" name="zip" value="" size="32" />&nbsp;*</td>
                </tr>
                <tr><td><br /></td></tr>
                <tr><td colspan="2" align="left" style="font-weight:bold; font-size:18px; color:#ff6633; border-bottom:1px solid #ff6633;">Billing Address</td></tr>
                <tr>
                	<td colspan="2" align="left"><input type="checkbox" name="sameAddr" value="1" onclick="copyShipping(this.checked)" />&nbsp;Check if Billing Address is the same as Shipping Address</td>
                </tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td align="right" style="font-weight:bold;">Address 1:</td>
                    <td style="color:#F00;" align="left"><input type="text" name="b_addr1" value="" size="32" />&nbsp;*</td>
                </tr>
                <tr>
                	<td align="right" style="font-weight:bold;">Address 2:</td>
                    <td style="color:#F00;" align="left"><input type="text" name="b_addr2" value="" size="32" />&nbsp;*</td>
                </tr>
                <tr>
                	<td align="right" style="font-weight:bold;">City:</td>
                    <td style="color:#F00;" align="left"><input type="text" name="b_city" value="" size="32" />&nbsp;*</td>
                </tr>
                <tr>
                	<td align="right" style="font-weight:bold;">State:</td>
                    <td style="color:#F00;" align="left">
                    	<select name="b_state">
							<%getStates(sstate)%>
                        </select>&nbsp;*
                    </td>
                </tr>
                <tr>
                	<td align="right" style="font-weight:bold;">Zip/Postal Code:</td>
                    <td style="color:#F00;" align="left"><input type="text" name="b_zip" value="" size="32" />&nbsp;*</td>
                </tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td colspan="2" align="left">
						<input type="checkbox" name="emailOpt" value="1" checked="checked" /> I would like to be included in the Wireless Emporium Exclusive VIP mailing list and be notified of periodic coupon code offers from Wireless Emporium via email.
                        <br /><br />
						(Wireless Emporium, Inc. abides by the most stringent privacy policies in the industry and will NEVER share or sell your email address or contact information with 3rd parties.)
                    </td>
                </tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td colspan="2" align="center">
                    	<input type="submit" name="myAction" value="Continue Checkout" />
                        <input type="hidden" name="parentAcctID" value="" />
                    </td>
                </tr>
                <tr><td><br /></td></tr>
            </table>
            </form>
        </td>
    </tr>
    <tr bgcolor="#ebebeb">
    	<td>
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td nowrap="nowrap" style="padding-left:5px;"><a style="text-decoration:underline;">Contact Us</a></td>
                    <td style="color:#666; font-weight:bold; padding-left:10px; padding-right:10px;">|</td>
                    <td nowrap="nowrap"><a style="text-decoration:underline;">RSS</a></td>
                    <td style="color:#666; font-weight:bold; padding-left:10px; padding-right:10px;">|</td>
                    <td nowrap="nowrap"><a href="/?ms=full" style="text-decoration:underline;">Full Site</a></td>
                	<td align="right" width="100%"><img src="/images/mobile/we_logo_little.png" border="0" /></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div id="testZone"></div>
<div id="dumpZone" style="display:none;"></div>
</body>
</html>
<%
	oConn.Close
	set oConn = nothing
%>
<script language="javascript" src="/includes/js/productKeywords.js"></script>
<script type="text/javascript">
	document.newAcct.email.value = ""
	
	function getCustomerData(email) {
		ajax('/ajax/customerData.asp?email=' + email,'dumpZone')
		setTimeout("displayData(document.getElementById('dumpZone').innerHTML)",500)
	}
	function displayData(dataReturn) {
		if (dataReturn != "no data") {
			window.location = '/mobile/login.asp'
		}
	}
	
	function chkForm() {
		if (document.newAcct.email.value == "") { alert("Please enter your email address");document.newAcct.email.focus();return false }
		//else if (document.newAcct.pass1.value == "") { alert("Please enter a password");document.newAcct.pass1.focus();return false }
		//else if (document.newAcct.pass2.value == "") { alert("Please confirm your password");document.newAcct.pass2.focus();return false }
		//else if (document.newAcct.pass2.value != document.newAcct.pass1.value) { alert("Your confirmed password does not match your password");document.newAcct.pass2.select();return false }
		else if (document.newAcct.fname.value == "") { alert("Please enter your first name");document.newAcct.fname.focus();return false }
		else if (document.newAcct.lname.value == "") { alert("Please enter your last name");document.newAcct.lname.focus();return false }
		else if (document.newAcct.phone.value == "") { alert("Please enter your phone number");document.newAcct.phone.focus();return false }
		else if (document.newAcct.addr1.value == "") { alert("Please enter your shipping address");document.newAcct.addr1.focus();return false }
		else if (document.newAcct.city.value == "") { alert("Please enter your shipping city");document.newAcct.city.focus();return false }
		else if (document.newAcct.state.value == "") { alert("Please enter your shipping state");document.newAcct.state.focus();return false }
		else if (document.newAcct.zip.value == "") { alert("Please enter your shipping zip/postal code");document.newAcct.zip.focus();return false }
		else if (document.newAcct.b_addr1.value == "") { alert("Please enter your billing address");document.newAcct.b_addr1.focus();return false }
		else if (document.newAcct.b_city.value == "") { alert("Please enter your billing city");document.newAcct.b_city.focus();return false }
		else if (document.newAcct.b_state.value == "") { alert("Please enter your billing state");document.newAcct.b_state.focus();return false }
		else if (document.newAcct.b_zip.value == "") { alert("Please enter your billing zip/postal code");document.newAcct.b_zip.focus();return false }
		else { return true }
	}
	
	function copyShipping(val) {
		if (val == true) {
			document.newAcct.b_addr1.value = document.newAcct.addr1.value
			document.newAcct.b_addr2.value = document.newAcct.addr2.value
			document.newAcct.b_city.value = document.newAcct.city.value
			document.newAcct.b_state.selectedIndex = document.newAcct.state.selectedIndex
			document.newAcct.b_zip.value = document.newAcct.zip.value
		}
		else {
			document.newAcct.b_addr1.value = ""
			document.newAcct.b_addr2.value = ""
			document.newAcct.b_city.value = ""
			document.newAcct.b_state.selectedIndex = 0
			document.newAcct.b_zip.value = ""
		}
	}
	
	function testContent(val) {
		if (val == "Find Your Product") {
			document.searchForm.searchString.value = ""
		}
	}
	
	YAHOO.example.BasicLocal = function() {
		var oDS = new YAHOO.util.LocalDataSource(YAHOO.example.Data.arrayProducts);
		var oAC = new YAHOO.widget.AutoComplete("myInput", "myContainer", oDS);
		oAC.prehighlightClassName = "yui-ac-prehighlight";
		oAC.useShadow = false;
		oAC.maxResultsDisplayed = 3;
		
		return {
			oDS: oDS,
			oAC: oAC
		};
	}();
	
	function ajax(newURL,rLoc,loading) {
		if (newURL.indexOf("?") > 0) {
			newURL = newURL + '&ajaxID=' + Math.floor(Math.random()*999999)
		}
		else {
			newURL = newURL + '?ajaxID=' + Math.floor(Math.random()*999999)
		}
		
		if (loading == 1) {
			document.getElementById(rLoc).innerHTML = "<table width='100%'><tr><td align='center' style=background-image:url(/images/mobile/loading.gif);background-repeat:no-repeat;background-position:center;><img src='/images/blank.gif' border='0' width='1' height='60' /><br /><hr color='#ff6633' /><br /><img src='/images/blank.gif' border='0' width='1' height='40' /><br /><div align='center' style='font-weight:bold; font-size:18px;'>Loading Content...</div><hr color='#ff6633' /><br /><img src='/images/blank.gif' border='0' width='1' height='100' /></td></tr></table>"
		}
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						window.location = "/mobile/?r=1"
					}
					else {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				}
				else {
					//alert("Error performing action")
				}
			}
		};
		httpRequest.send(null);
	}
</script>
<!-- Google Analytics Start -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1311669-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Google Analytics End -->