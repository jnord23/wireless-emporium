<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<%
	dim pageName : pageName = "Checkout3"
	dim useSessionID : useSessionID = prepInt(request.Cookies("mySession"))
	dim csID : csID = prepInt(request.QueryString("csID"))
	dim securePage : securePage = 1
	if instr(request.ServerVariables("SERVER_NAME"), "mdev.") > 0 then securePage = 0	
	
	if csID > 0 then useSessionID = csID
	mySession = useSessionID
	
	sql =	"update shoppingCart set step3 = 1 where sessionID = " & useSessionID
	session("errorSQL") = sql
	oConn.execute(sql)
	
	sql =	"select c.totalItems, c.totalWeight, a.qty, b.itemID, b.itemPic, b.itemDesc, b.price_our " &_
			"from shoppingCart a " &_
				"left join we_Items b on a.itemID = b.itemID " &_
				"outer apply (" &_
					"select sum(qty) as totalItems, sum(itemWeight * ia.qty) as totalWeight, sessionID " &_
					"from shoppingCart ia " &_
						"left join we_Items ib on ia.itemID = ib.itemID " &_
					"where sessionID = " & useSessionID & " and ia.purchasedorderid is null " &_
					"group by sessionID" &_
				") c " &_
			"where a.sessionID = " & useSessionID & " and a.purchasedorderid is null"
	session("errorSQL") = sql
	Set rsItems = oConn.execute(sql)
	
	dim email : email = prepStr(request.Form("email"))
	dim fname : fname = prepStr(request.Form("fname"))
	dim lname : lname = prepStr(request.Form("lname"))
	dim phone : phone = prepStr(request.Form("phone"))
	
	dim sAddress1 : sAddress1 = prepStr(request.Form("sAddress1"))
	dim sAddress2 : sAddress2 = prepStr(request.Form("sAddress2"))
	dim sCity : sCity = prepStr(request.Form("sCity"))
	dim sState : sState = prepStr(request.Form("sState"))
	dim sZip : sZip = prepStr(request.Form("sZip"))
	
	dim bAddress1 : bAddress1 = prepStr(request.Form("bAddress1"))
	dim bAddress2 : bAddress2 = prepStr(request.Form("bAddress2"))
	dim bCity : bCity = prepStr(request.Form("bCity"))
	dim bState : bState = prepStr(request.Form("bState"))
	dim bZip : bZip = prepStr(request.Form("bZip"))
	
	dim tax : tax = prepInt(request.Form("tax"))
	dim shipType : shipType = prepInt(request.Form("shipType"))
	dim shippingAmt : shippingAmt = prepInt(request.Form("shippingAmt"))
	dim subtotal : subtotal = prepInt(request.Form("subtotal"))
	dim nSubTotal : nSubTotal = prepInt(request.Form("subtotal"))	'for inc_promoFunctions.asp
	dim grandTotal : grandTotal = prepInt(request.Form("grandTotal"))	
	
	if sAddress1 = "" then response.Redirect("/checkout_step2.asp")
	if sCity = "" then response.Redirect("/checkout_step2.asp")
	if sState = "" then response.Redirect("/checkout_step2.asp")
	if sZip = "" then response.Redirect("/checkout_step2.asp")
	if subtotal = 0 then response.Redirect("/checkout_step2.asp")
	if grandTotal = 0 then response.Redirect("/checkout_step2.asp")
	
	if bAddress1 = "" then bAddress1 = sAddress1
	if bAddress2 = "" then bAddress2 = sAddress2
	if bCity = "" then bCity = sCity
	if bState = "" then bState = sState
	if bZip = "" then bZip = sZip
	
	if rsItems.EOF or useSessionID = 0 then
		response.Redirect("/")
	else
		itemCnt = rsItems("totalItems")
		totalWeight = rsItems("totalWeight")
	end if
%>
<!--#include virtual="/template/promoCalc.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/template/top.asp"-->
<%
	discountTotal = prepInt(discountTotal)
	discountedCartTotal = orderAmt - discountTotal
%>
<script>
window.WEDATA.pageType = 'checkout3';
window.WEDATA.pageData = {
	cartItems: []
}
</script>
<div id="secureRow">
   	<div id="secureImg"><img src="/images/mobile/retina/checkout/we-m-icon-lock.png" height="23" /></div>
    <div id="secureText">Credit Card Information</div>
</div>
<form name="checkoutForm" method="post" action="/framework/processing.htm" onsubmit="return doSubmit();">
<div id="entryFormContainer">
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">Cardholder Name:</div>
        </div>
        <div id="entryField"><input id="dataEntryInput" type="text" name="cc_cardOwner" value="" /></div>
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">Credit Card #:</div>
        </div>
        <div id="entryField"><input id="dataEntryInput" type="tel" name="cardNum" value="" size="16" maxlength="16" /></div>
    </div>
    <div id="ccInstructions">
    	No spaces/dashes<br />
        Example: 1234123412341234
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">Security Code:</div>
        </div>
        <div id="entryFieldSmall">
        	<div id="securityCode1"><input id="dataEntryInputSmall" type="tel" name="secCode" value="" size="4" maxlength="4" /></div>
            <div id="securityCode2">What's This?</div>
        </div>
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">Expiration Date:</div>
        </div>
        <div id="entryFieldSmall">
        	<div id="exp1">
            	<select name="cc_month" id="dataEntryInputSmall">
                	<option value=""></option>
					<%
					for i = 1 to 12
					%>
                    <option value="<%=i%>"><%=i%></option>
                    <%
					next
					%>
                </select>
            </div>
            <div id="exp2">
            	<select name="cc_year" id="dataEntryInputSmall">
                	<option value=""></option>
					<%
					for i = year(now) to year(now)+10
					%>
                    <option value="<%=right(i,2)%>"><%=i%></option>
                    <%
					next
					%>
                </select>
            </div>
        </div>
    </div>
</div>
<div id="secureRow">
   	<div id="secureImg"><img src="/images/mobile/retina/checkout/we-m-icon-lock.png" height="23" /></div>
    <div id="secureText">Billing Address</div>
</div>
<div id="orderReviewContainer">
	<div id="orderReviewBox">
    	<div id="orderReviewRow">
        	<div class="orderReviewText"><%=fname & " " & lname%></div>
        </div>
        <div id="orderReviewRow">
        	<div class="orderReviewText"><%=bAddress1%></div>
        </div>
        <% if bAddress2 <> "" then %>
        <div id="orderReviewRow">
        	<div class="orderReviewText"><%=bAddress2%></div>
        </div>
        <% end if %>
        <div id="orderReviewRow">
        	<div class="orderReviewText"><%=bCity & ", " & bState & " " & bZip%></div>
        </div>
        <div id="orderReviewRow" onclick="javascript:history.go(-1)">
        	<div id="changeAddress1"><img src="/images/mobile/retina/cart/we-m-button-remove.png" width="20" border="0" /></div>
            <div id="changeAddress2">Change Billing Address</div>
        </div>
    </div>
</div>
<div id="secureRow">
   	<div id="secureImg"><img src="/images/mobile/retina/checkout/we-m-icon-lock.png" height="23" /></div>
    <div id="secureText">Order Review</div>
</div>
<div id="orderReviewContainer">
	<%
	prodLap = 1
    do while not rsItems.EOF
		itemID = prepInt(rsItems("itemID"))
		itemPic = rsItems("itemPic")
		itemDesc = prepStr(rsItems("itemDesc"))
		qty = prepInt(rsItems("qty"))
		itemPrice = prepInt(rsItems("price_our"))
	%>
    <div id="itemBox">
    	<div id="itemPic" onclick="window.location='/product.htm?itemID=<%=itemID%>'"><img src="/productPics/big/<%=itemPic%>" width="100" border="0" /></div>
        <div id="itemDetails">
        	<div id="itemDesc" onclick="window.location='/product.htm?itemID=<%=itemID%>'"><%=itemDesc%></div>
            <div id="qtyDetails">
            	<div id="qtyText">Qty.</div>
                <div id="qtyValue"><%=qty%></div>
            </div>
            <% if qty = 1 then %>
            <div id="itemPrice">Item Price: <%=formatCurrency(itemPrice,2)%></div>
            <% else %>
            <div id="itemPriceSmall">Item Price: <%=formatCurrency(itemPrice,2)%></div>
            <div id="itemPrice">Item Total: <%=formatCurrency((itemPrice * qty),2)%></div>
            <% end if %>
        </div>
    </div>
    <input type="hidden" name="ssl_item_number_<%=prodLap%>" value="<%=itemID%>">
    <input type="hidden" name="ssl_item_qty_<%=prodLap%>" value="<%=qty%>">
    <input type="hidden" name="ssl_musicSkins_<%=prodLap%>" value="0">
    <script>
        window.WEDATA.pageData.cartItems.push({itemId: <%= jsStr(itemId) %>, qty: <%= jsStr(qty) %>, price: <%= jsStr(itemPrice) %>});
    </script>
    <%
		rsItems.movenext
		prodLap = prodLap + 1
	loop
	%>
	<div id="orderReviewBox">
    	<div id="orderReviewRow">
        	<div class="orderReviewText">Subtotal:</div>
            <div class="orderReviewValue"><%=formatCurrency(subtotal,2)%></div>
        </div>
        <div id="orderReviewRow">
        	<div id="taxTotal" class="orderReviewText">Tax:</div>
            <div id="taxValue" class="orderReviewValue">
				<%=formatCurrency(tax,2)%>
            </div>
        </div>
		<%
		if sPromoCode <> "" then
			session("promocode") = sPromoCode
		%>
        <div id="orderReviewRow">
        	<div class="orderReviewText">Discount:</div>
            <div class="orderReviewValueRed"><%=formatcurrency(0-discountTotal,2)%></div>
        </div>
        <input type="hidden" name="sPromoCode" value="<%=sPromoCode%>">
        <input type="hidden" name="nPromo" value="<%=couponid%>">
        <input type="hidden" name="discountTotal" value="<%=discountTotal%>">
		<%end if%>
        <div id="orderReviewRow">
        	<div class="orderReviewText">Shipping:</div>
            <div id="shippingValue" class="orderReviewValueRed">
            	<% if shippingAmt = 0 then shippingAmt = "FREE" %>
				<%=shippingAmt%>
            </div>
        </div>
        <div id="orderReviewRow2">
        	<div id="orderReviewTextWhite">Grand Total:</div>
            <div id="grandTotalValue" class="orderReviewValueOrange">
				<%=formatCurrency(grandTotal,2)%>
            </div>
        </div>
    </div>
</div>
<div id="continueCheckout">
	<div id="imgLoading" style="display:none;"><img src="/images/preloading.gif" border="0" /> Please wait..</div>
	<input id="imgSubmit" type="image" src="/images/mobile/retina/payment/we-m-button-submit.png" width="282" border="0" />
    <input type="hidden" name="email" value="<%=email%>" />
    <input type="hidden" name="fname" value="<%=fname%>" />
    <input type="hidden" name="lname" value="<%=lname%>" />
    <input type="hidden" name="phoneNumber" value="<%=phone%>" />
    
    <input type="hidden" name="bAddress1" value="<%=bAddress1%>" />
    <input type="hidden" name="bAddress2" value="<%=bAddress2%>" />
    <input type="hidden" name="bCity" value="<%=bCity%>" />
    <input type="hidden" name="bState" value="<%=bState%>" />
    <input type="hidden" name="bZip" value="<%=bZip%>" />
    
    <input type="hidden" name="sAddress1" value="<%=sAddress1%>" />
    <input type="hidden" name="sAddress2" value="<%=sAddress2%>" />
    <input type="hidden" name="sCity" value="<%=sCity%>" />
    <input type="hidden" name="sState" value="<%=sState%>" />
    <input type="hidden" name="sZip" value="<%=sZip%>" />
    
    <input type="hidden" name="shippingTotal" value="<%=shippingAmt%>" />
    <input type="hidden" name="shiptype" value="<%=shipType%>" />
    <input type="hidden" name="shiptypeID" value="<%=shipType%>" />    
    <input type="hidden" name="subTotal" value="<%=subtotal%>" />
    <input type="hidden" name="grandTotal" value="<%=grandTotal%>" />
    <input type="hidden" name="ssl_ProdIdCount" value="<%=itemCnt%>" />
    <input type="hidden" name="nShippingID_mobile" value="<%=shipType%>" />
    <input type="hidden" name="nCATax" value="<%=tax%>" />
    <input type="hidden" name="mobile" value="1" />
</div>
<div id="secureIcons"><img src="/images/mobile/retina/home/we-m-footer-seals.png" width="300" border="0" /></div>
</form>
<div style="height:15px;"></div>
<div id="emailReturn"></div>
<script>
window.WEDATA.pageData.discountTotal = <%= jsStr(discountTotal)%>;
window.WEDATA.pageData.subTotal = <%= jsStr(subTotal) %>;
window.WEDATA.pageData.grandTotal = <%= jsStr(grandTotal) %>;
window.WEDATA.pageData.shipping = <%= jsStr(shippingAmt) %>;
window.WEDATA.pageData.amount = <%= jsStr(grandTotal) %>;
window.WEDATA.pageData.tax = <%= jsStr(tax)%>;


</script>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">
	function doSubmit() {
		if (verifyForm()) {
			document.getElementById('imgLoading').style.display = '';
			document.getElementById('imgSubmit').style.display = 'none';
			return true;
		}
		return false;
	}
	
	function verifyForm() {
		if (document.checkoutForm.cc_cardOwner.value == "") {
			alert("You must enter the cardholder's name");
			document.checkoutForm.cc_cardOwner.focus();
			return false;
		}
		else if (document.checkoutForm.cardNum.value == "") {
			alert("You must enter the credit card number");
			document.checkoutForm.cardNum.focus();
			return false;
		}
		else if (document.checkoutForm.secCode.value == "") {
			alert("You must enter the security code");
			document.checkoutForm.secCode.focus();
			return false;
		}
		else if (document.checkoutForm.cc_month.value == "") {
			alert("You must enter the expiration month");
			document.checkoutForm.cc_month.focus();
			return false;
		}
		else if (document.checkoutForm.cc_year.value == "") {
			alert("You must enter the expiration year");
			document.checkoutForm.cc_year.focus();
			return false;
		}
		else {
			return true;
		}
	}
</script>