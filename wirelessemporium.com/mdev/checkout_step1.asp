<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<%
	dim pageName : pageName = "Checkout1"
	dim useSessionID : useSessionID = prepInt(request.Cookies("mySession"))
	dim securePage : securePage = 1
	
	sql =	"select c.totalItems, c.totalWeight, a.qty, b.itemID, b.itemPic, b.itemDesc, b.price_our " &_
			"from shoppingCart a " &_
				"left join we_Items b on a.itemID = b.itemID " &_
				"outer apply (" &_
					"select sum(qty) as totalItems, sum(itemWeight * ia.qty) as totalWeight, sessionID " &_
					"from shoppingCart ia " &_
						"left join we_Items ib on ia.itemID = ib.itemID " &_
					"where sessionID = " & useSessionID & " " &_
					"group by sessionID" &_
				") c " &_
			"where a.sessionID = " & useSessionID
	session("errorSQL") = sql
	Set rs = oConn.execute(sql)
	
	if rs.EOF or useSessionID = 0 then
		response.Redirect("/")
	else
		itemCnt = rs("totalItems")
		totalWeight = rs("totalWeight")
		
		itemDetails = ""
		orderAmt = 0
		do while not rs.EOF
			qty = rs("qty")
			itemID = rs("itemID")
			itemDesc = rs("itemDesc")
			itemPrice = rs("price_our")
			
			itemDetails = itemDetails & qty & "##"
			itemDetails = itemDetails & itemID & "##"
			itemDetails = itemDetails & itemDesc & "##"
			itemDetails = itemDetails & itemPrice & "@@"
			orderAmt = orderAmt + (qty * itemPrice)
			rs.movenext
		loop
		if len(itemDetails) > 0 then itemDetails = left(itemDetails,len(itemDetails)-2)
		itemDetailArray = split(itemDetails,"@@")
	end if
%>
<!--#include virtual="/template/top.asp"-->
<div id="secureRow">
   	<div id="secureImg"><img src="/images/mobile/icons/secureIcon.gif" /></div>
    <div id="secureText">Checkout Method</div>
</div>
<div id="checkoutOption">
	<form method="post" action="/checkout_step2.htm">
    	<input type="hidden" name="subtotal" value="<%=orderAmt%>" />
	    <input type="image" src="/images/mobile/buttons/creditCardCheckout.jpg" border="0" />
    </form>
</div>
<div id="checkoutOption"><img src="/images/mobile/buttons/creditCards.jpg" border="0" /></div>
<div id="otherOptions">
	<div id="checkoutOption" style="display:none;">
    	<form name="googleForm" method="post" action="/cart/process/google/CartProcessing.htm">
            <input type="hidden" name="promo" value="">
            <input type="hidden" name="analyticsdata" value="">
            <input type="hidden" name="sWeight" value="<%=totalWeight%>">
            <input type="hidden" name="mobileOrder" value="1" />
            <%
			for i = 0 to ubound(itemDetailArray)
				singleItemArray = split(itemDetailArray(i),"##")
				qty = singleItemArray(0)
				itemID = singleItemArray(1)
			%>
            <input type="hidden" name="itemID_<%=i+1%>" value="<%=itemID%>">
            <input type="hidden" name="itemQty_<%=i+1%>" value="<%=qty%>">
            <% next %>
            <input type="image" src="/images/mobile/buttons/googleCheckout.jpg" border="0" />
        </form>
    </div>
	<div id="checkoutOption">
    	<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.htm" style="margin:0px;">
            <%
			itemLap = 0
			for i = 0 to ubound(itemDetailArray)
				singleItemArray = split(itemDetailArray(i),"##")
				qty = singleItemArray(0)
				itemID = singleItemArray(1)
				itemDesc = singleItemArray(2)
				itemPrice = singleItemArray(3)
			%>
			<input type="hidden" name="L_NUMBER<%=itemLap%>" value="<%=itemID%>">
            <input type="hidden" name="L_NAME<%=itemLap%>" value="<%=itemDesc%>">
            <input type="hidden" name="L_QTY<%=itemLap%>" value="<%=qty%>">
            <input type="hidden" name="L_AMT<%=itemLap%>" value="<%=itemPrice%>">
            <%
				itemLap = itemLap + 1
			next
			%>
            <input type="hidden" name="numItems" value="<%=itemCnt%>">
		    <input type="hidden" name="paymentAmount" value="<%=orderAmt%>">
		    <input type="hidden" name="ItemsAndWeight" value="<%=itemCnt%>|<%=totalWeight%>">
		    <input type="hidden" name="mobileOrder" value="1">
            <input type="image" src="/images/mobile/buttons/payPalCheckout.jpg" border="0" />
		</form>
    </div>
</div>
<script>
window.WEDATA.pageType = 'checkout1';
</script>
<!--#include virtual="/template/bottom.asp"-->