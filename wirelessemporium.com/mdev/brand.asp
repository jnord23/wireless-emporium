<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	dim brandName : brandName = prepStr(request.QueryString("brand"))
	dim pageName : pageName = "brands"
	
	dim curUrl : curUrl = request.ServerVariables("HTTP_X_REWRITE_URL")
	dim sortBy : sortBy = "az"
	if instr(curUrl,"?") > 0 then
		curQs = mid(curUrl, instr(curUrl,"?")+1)
		sortBy = mid(curQs, instr(curQs,"=")+1)
		sortBy = prepStr(sortBy)
		curUrl = left(curUrl, instr(curUrl,"?")-1)
	end if
	
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	dim sql,rs
	
	if brandName = "hp" then brandName = "HP/Palm"
	if brandName = "kyocera" then brandName = "Kyocera/Qualcomm"
	if brandName = "sony" then brandName = "Sony Ericsson"
	sql = "exec sp_brandIdByBrandName '" & brandName & "'"
	session("errorSQL") = sql
	set brandRS = oConn.execute(sql)
	
	if not brandRS.EOF then brandID = brandRS("brandID")
	
	sql = "exec sp_modelsByBrandMobile " & brandID & ",'" & sortBy & "'"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
%>
<!--#include virtual="/template/top.asp"-->
<div id="main">
    <div id="curBrandDetails">
        <div class="tb mCenter ptb10" id="selectDevice">SELECT YOUR DEVICE BELOW:</div>
        <!--<div id="brandLogo">Brand Picture</div>-->
        <a href="/?reset=1" style="text-decoration:none;">
        <div class="tb" id="changeBrandBox">
            <div id="redX"></div>
            <div id="tapToChange">Tap to change brand.</div>
        </div>
        </a>
        <div class="tb mCenter pt10">
            <select name="sortBy" class="sortBySelect" onchange="document.location='<%=curUrl%>?sortBy=' + this.value">
                <option value="">Sort By</option>
                <option value="az">A-Z</option>
                <option value="za">Z-A</option>
                <option value="no">Newest-Oldest</option>
                <option value="on">Oldest-Newest</option>
            </select>
        </div>
    </div>
    <div id="mainDiviceBox">
        <%
        deviceCnt = 0
        if rs.EOF then
    %>
        <div id="noDevices">No Devices Found</div>
    <%
        end if
    
        useCarrierLogo = "1301,1259,1258,"
        do while not rs.EOF
            hidelive = rs("hidelive")
            international = rs("international")
            if not hidelive and not international then 
                useBrandName = rs("brandName")
                modelID = rs("modelID")
                modelImg = rs("modelImg")
                modelName = rs("modelName")
                showModelName = modelName
                if brandName = "other" then
                    if useBrandName <> "Other" then showModelName = useBrandName & " " & modelName
                end if
                
                if fso.fileExists(server.MapPath("/productPics/models/thumbs/" & modelImg)) then
                    deviceCnt = deviceCnt + 1
        %>
        <a href="/<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>-accessories.htm" style="text-decoration:none;" title="<%=modelName%>">
        <div id="singleDeviceBox_<%=deviceCnt%>" class="singleDeviceBox"<% if deviceCnt > 20 then %> style="display:none;"<% end if %>>
            <div id="devicePic" style="position:relative;">
                <%if instr(useCarrierLogo, modelID & ",") > 0 then%>
                    <div style="position:absolute; bottom:0px; left:-13px;"><img src="/images/brands/b_carrierLogo_<%=modelID%>.png" border="0" /></div>
                <%end if%>
                <tempimg src="/productPics/models/thumbs/<%=modelImg%>" border="0" height="112" />
            </div>
            <div id="deviceName"><%=showModelName%></div>
        </div>
        </a>
        <%
                end if
            end if
            rs.movenext
        loop
        %>
    </div>
</div>
<script>
    window.WEDATA.pageType = 'brand';
    window.WEDATA.pageData = {
        brand: <%= jsStr(brandName) %>,
        brandId: <%= jsStr(brandId) %>
    }
</script>

<!--#include virtual="/template/bottom.asp"-->
<script language="javascript" src="/template/js/brand.js"></script>