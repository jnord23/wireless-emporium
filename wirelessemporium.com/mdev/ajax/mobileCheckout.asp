<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<%	
	if isnull(request.Cookies("mySession")) or len(request.Cookies("mySession")) < 1 then response.Cookies("mySession") = session.SessionID
	
	dim thisUser, pageTitle, header
	thisUser = Request.Cookies("admin")("username")
	
	sql = "insert into we_mobileTrackingPages (sessionID,page) values('" & request.Cookies("mySession") & "','" & request.ServerVariables("URL") & "?" & request.ServerVariables("QUERY_STRING") & "')"
	session("errorSQL") = sql
	oConn.execute(sql)
	
	sql = "insert into _terry(msg) values('a')"
	oConn.execute(sql)

	itemID = prepStr(request.QueryString("itemID"))
	newQty = prepStr(request.QueryString("newQty"))
	viewCart = prepStr(request.QueryString("viewCart"))
	
	if isnull(itemID) or len(itemID) < 1 then itemID = 0
	if isnull(newQty) or len(newQty) < 1 then newQty = 0
	if isnull(viewCart) or len(viewCart) < 1 then viewCart = 0
	
	if session("acctID") = 0 then acctID = -1 else acctID = session("acctID")
	
	if isnull(request.Cookies("mySession")) or len(request.Cookies("mySession")) < 1 then
		response.Cookies("mySession") = session.SessionID
		response.Cookies("mySession").expires = date + 30
		useSessionID = session.SessionID
	else
		useSessionID = request.Cookies("mySession")
	end if
	
	if itemID > 0 then
		if newQty = 0 then
			sql = "delete from shoppingCart where itemID = " & itemID & " and (sessionID = " & useSessionID & " or accountID = " & acctID & ")"
		else
			sql = "update shoppingCart set qty = " & newQty & " where itemID = " & itemID & " and (sessionID = " & useSessionID & " or accountID = " & acctID & ")"
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
	elseif viewCart = 1 then
		sql = "select sum(qty) as cartItems from shoppingCart where sessionID = " & useSessionID & " or accountID = " & acctID
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
		if rs("cartItems") > 0 then
			response.Write(rs("cartItems") & " Item(s)")
		else
			response.Write("No Items")
		end if
		call CloseConn(oConn)
		response.End()
	end if
	
	sql = "select b.partNumber, b.noDiscount, a.qty, b.itemID, b.itemPic, b.itemDesc, b.price_retail, b.price_our from shoppingCart a join we_items b on a.itemID = b.itemID where a.sessionID = " & useSessionID & " or a.accountID = " & acctID
	sql = sql & " union "
	sql = sql & "select '' as partNumber, cast(0 as bit) as noDiscount, a.qty, b.id as itemID, b.image + '##' + b.defaultImg as itemPic, b.artist + ' ' + b.designName as itemDesc, b.msrp as price_retail, b.price_we as price_our from shoppingCart a join we_items_musicSkins b on a.itemID = b.id where a.sessionID = " & useSessionID & " or a.accountID = -1"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	if rs.EOF then 
		call CloseConn(oConn)
		response.End()
	end if
	
	itemCnt = 0
	totalSubtotal = 0
	specialOffer = 0
	faceplateDiscountCnt = 0
	validProd = split("FP1|FP2|FP3|FP4|FP5|FP6","|")
	do while not rs.EOF
		partNumber = rs("partNumber")
		for i = 0 to ubound(validProd)
			if instr(partNumber,validProd(i)) > 0 then
				if not rs("noDiscount") then specialOffer = 1
			end if
		next
		itemCnt = itemCnt + rs("qty")
		if specialOffer = 1 then
			for i = 1 to cdbl(rs("qty"))
				faceplateDiscountCnt = faceplateDiscountCnt + 1
				if faceplateDiscountCnt = 2 then
					totalSubtotal = totalSubtotal + (rs("price_our")/2)
					faceplateDiscountCnt = 0
				else
					totalSubtotal = totalSubtotal + rs("price_our")
				end if
			next
		else
			totalSubtotal = totalSubtotal + (rs("qty") * rs("price_our"))
		end if
		specialOffer = 0
		rs.movenext
	loop
	rs.movefirst
	
	sql = "update we_mobileTracking set itemsInCart = " & itemCnt & " where sessionID = '" & request.Cookies("mySession") & "'"
	session("errorSQL") = sql
	oConn.execute(sql)
	
	baseCrumb = "<a style='font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?r=1','mobileContent',1)>Main Menu&nbsp;&gt;</a>"
%>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
	<tr bgcolor="#ebebeb"><td align="left" style="color:#000000; font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%>&nbsp;Your Cart</td></tr>
    <tr>
    	<td align="left" style="color:#ff6633; font-size:24px; font-weight:bold; padding-bottom:5px; padding-top:10px; padding-left:5px;">Checkout Step 1 of 3</td>
    </tr>
    <tr><td style="border-bottom:1px solid #ff6633;"></td></tr>
    <tr>
    	<td align="left" style="color:#ff6633; font-size:24px; font-weight:bold; padding-bottom:5px; padding-top:10px; padding-left:5px;">Your Cart</td>
    </tr>
    <tr>
    	<td align="left" style="font-weight:bold; padding-bottom:10px; padding-left:5px;"><%=itemCnt%> Item(s) Subtotal: <%=formatCurrency(totalSubtotal,2)%></td>
    </tr>
    <%
	totalSavings = 0
	do while not rs.EOF
	%>
    <tr bgcolor="#f0f0f0">
    	<td>
        	<table border="0" cellpadding="0" cellspacing="0" style="border-top:1px solid #cccccc; border-bottom:1px solid #cccccc;" width="100%">
            	<tr>
                	<td valign="top" rowspan="2" style="padding-left:20px; padding-top:10px; padding-right:20px; padding-bottom:10px;">
                    	<%
						if instr(rs("itemPic"),"##") > 0 then
							set fso = CreateObject("Scripting.FileSystemObject")
							imgArray = split(rs("itemPic"),"##")
							if isnull(imgArray(0)) or imgArray(0) = "" then
								useImg = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & imgArray(1)
							else
								if fso.fileExists(server.MapPath("/productpics/musicSkins/musicSkinsSmall/" & imgArray(0))) then
									useImg = "/productpics/musicSkins/musicSkinsSmall/" & imgArray(0)
								else
									useImg = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & imgArray(1)
								end if
							end if
						%>
                        <img src="<%=useImg%>" border="0" style="border:1px solid #cccccc;" />
                        <% else %>
                        <img src="/productPics/thumb/<%=rs("itemPic")%>" border="0" style="border:1px solid #cccccc;" />
                        <% end if %>
                    </td>
                    <td align="left" style="padding-top:10px; padding-right:5px;"><%=rs("itemDesc")%></td>
                </tr>
                <tr>
                	<td valign="bottom" style="padding-bottom:10px;" width="100%">
                    	<table border="0" cellpadding="0" cellspacing="0">
                        	<tr>
                                <td align="left" style="color:#999; padding-right:20px;">Retail Price</td>
                                <td align="left" style="color:#999; text-decoration:line-through;"><%=formatCurrency(rs("price_retail"),2)%></td>
                            </tr>
                            <tr>
                                <td align="left" style="font-weight:bold; padding-right:20px;">Your Price</td>
                                <td align="left"><%=formatCurrency(rs("price_our"),2)%></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                	<td colspan="2" align="left">
                    	<form name="item<%=rs("itemID")%>_form" onsubmit="alert('Click update to change qty');return false">
                    	<table border="0" cellpadding="0" cellspacing="0">
                        	<tr>
                            	<td style="font-weight:bold; padding-left:20px;">Quantity:</td>
                                <td style="padding-left:5px;"><input type="text" name="qty" value="<%=rs("qty")%>" size="3" /></td>
                                <td style="padding-left:20px;"><a href="#" onclick="ajax('/ajax/mobileCheckout.asp?itemID=<%=rs("itemID")%>&newQty=' + document.item<%=rs("itemID")%>_form.qty.value,'mobileContent',1);ajax('/ajax/mobileCheckout.asp?viewCart=1','viewCart',0)" style="color:#ff6633; text-decoration:underline;">Update</a></td>
                                <td style="padding-left:40px;"><a href="#" onclick="ajax('/ajax/mobileCheckout.asp?itemID=<%=rs("itemID")%>&newQty=0','mobileContent',1);ajax('/ajax/mobileCheckout.asp?viewCart=1','viewCart',0)" style="color:#666666; text-decoration:underline;">Remove</a></td>
                            </tr>
                        </table>
                        </form>
                    </td>
                </tr>
                <tr><td><br /></td></tr>
            </table>
        </td>
    </tr>
    <tr bgcolor="#ffffff"><td style="padding-top:3px;"></td></tr>
    <%
		totalSavings = totalSavings + (rs("price_retail") - rs("price_our"))
		totalRetail = totalRetail + (rs("price_retail") * rs("qty"))
		rs.movenext
	loop
	%>
    <tr>
    	<td align="right" style="padding-right:10px;">
        	<table border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<td align="right" style="padding-right:40px;">Shipping:</td>
                    <td align="right">FREE</td>
                </tr>
                <tr>
                	<td align="right" style="padding-right:40px;">Savings:</td>
                    <td align="right"><%=formatCurrency(totalRetail - totalSubtotal,2)%></td>
                </tr>
                <tr>
                	<td align="right" style="padding-right:40px; font-weight:bold;">Item Total:</td>
                    <td align="right" style="font-weight:bold;"><%=formatCurrency(totalSubtotal,2)%></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td style="padding-top:20px; padding-bottom:10px; padding-left:10px; padding-right:10px;">
        	<form name="checkoutForm" method="post" action="https://m.wirelessemporium.com/mobile/login.asp">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td align="left"><a href="#" onclick="window.location='/mobile/'"><img src="/images/mobile/buttons/button_contshopping.png" border="0" /></a></td>
                    <td align="right">
                    	<a style="cursor:pointer;" onclick="document.checkoutForm.submit()"><img src="/images/mobile/buttons/button_checkout.png" border="0" /></a>
                        <input type="hidden" name="totalSubtotal" value="<%=totalSubtotal%>" />
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr><td><br /></td></tr>
</table>
<%
	sql = "insert into _terry(msg) values('b')"
	oConn.execute(sql)

	call CloseConn(oConn)
%>
<!-- Google Analytics Start -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1311669-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Google Analytics End -->