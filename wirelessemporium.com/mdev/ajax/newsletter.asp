<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_GetMySession.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
dim fso : set fso = CreateObject("Scripting.FileSystemObject")

if prepInt(mySession) = 0 then
	mySession = session.SessionID
	response.cookies("mySession") = mySession
	response.cookies("mySession").expires = now+30
end if

dim semail
semail = prepStr(request.QueryString("semail"))
dim newsletterMsg : newsletterMsg = ""

campaign_id = "welcome1"
promo_code = "WELCOME15"
curYear = year(date)
expDate = date+7

bSubscribed = false

if semail <> "" then
	if semail = "closewidget" then
		response.cookies("welcomeCouponUsed") = mySession
		response.cookies("welcomeCouponUsed").expires = now+30
		response.write "do not want"
	elseif instr(trim(semail), "@") > 0 and instr(trim(semail), " ") = 0 and instr(trim(semail), ".@") = 0 then
		if isValidEmail(semail) then
			response.Cookies("customerEmail") = semail
			response.Cookies("customerEmail").expires = now + 60
			
			sql = "exec sp_emailSub 0,'" & semail & "',''"
			session("errorSQL") = sql
			set emailRS = oConn.execute(sql)
			
			if emailRS("returnValue") = "duplicate" then
				newsletterTitle = "Welcome Back!"
				newsletterMsg = "Thanks for your interest in our email newsletter. Our records indicate that you are already a member. Unfortunately, this promotion is for new subscribers. But you will receive future exclusive offers. Add us to your safe sender's list so you don't miss any of your special promotions." & _
								"<br /><br />" &_
								"Sincerely,<br />" &_                                                    
								"WirelessEmporium.com"
			else
				dim cdo_to, cdo_from, cdo_subject, cdo_body
				set curHTML = fso.OpenTextFile(server.MapPath("/images/email/blast/template/we-welcome-email.htm"),1,false,0)
				cdo_body = curHTML.readall
				cdo_body = replace(cdo_body,"{{campaign_id}}",campaign_id)
				cdo_body = replace(cdo_body,"{{promo_code}}",promo_code)
				cdo_body = replace(cdo_body,"{{expDate}}",expDate)
				cdo_body = replace(cdo_body,"{{curYear}}",curYear)				
				
				strProducts = ""
				sql = "sp_topSellingProducts 0, 20, 10, 'UNI', 1"
				set rsRecommend = oConn.execute(sql)
				nCnt = 0
				do until rsRecommend.eof
					nCnt = nCnt + 1
					rsItemid = rsRecommend("itemid")
					rsItemDesc = rsRecommend("itemdesc")
					rsItempic = "http://www.wirelessemporium.com/productpics/thumb/" & rsRecommend("itempic")
					rsPrice = formatcurrency(rsRecommend("price_our"), 2)
					productLink = "http://www.wirelessemporium.com/p-" & rsItemid & "-" & formatSEO(rsItemDesc) & "?utm_source=streamsend&utm_medium=email2&utm_campaign=" & campaign_id & "&utm_promocode=" & promo_code
					strProducts = 	strProducts & 	"  <table width=""180"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf & _
													"	  <tr>" & vbcrlf & _
													"		<td style=""font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;"" width=""180"" align=""center"" valign=""top""><a href=""" & productLink & """ target=""_blank""><img style=""display: block; border: 2px solid #999999;"" src=""" & rsItempic & """ width=""75"" height=""75"" /></a><br />" & vbcrlf & _
													"		  <a style=""color: #333333; text-decoration: none;"" href=""" & productLink & """>" & rsItemDesc & "</a><br />" & vbcrlf & _
													"		  <br />" & vbcrlf & _
													"		  <a style=""color: #333333; font-size: 14px; text-decoration: none;"" href=""" & productLink & """ target=""_blank"">" & rsPrice & "</a><br />" & vbcrlf & _
													"		  <table width=""160"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf & _
													"			<tr>" & vbcrlf & _
													"			  <td style=""background-color: #ff6600; border-radius: 17px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;"" width=""160"" height=""34"" align=""center"" valign=""middle""><a style=""color: #ffffff; text-decoration: none;"" href=""" & productLink & """ target=""_blank"">SHOP NOW &raquo;</a></td>" & vbcrlf & _
													"			</tr>" & vbcrlf & _
													"		  </table>" & vbcrlf & _
													"		</td>" & vbcrlf & _
													"	  </tr>" & vbcrlf & _
													"	</table>" & vbcrlf & _
													"	<br />"
					if nCnt >= 2 then exit do
					rsRecommend.movenext
				loop
				cdo_body = replace(cdo_body,"{{product_recommend}}",strProducts)
				
				if semail = "jnord@test.com" then semail = "jon@wirelessemporium.com"
				cdo_to = semail
				cdo_from = "Wireless Emporium<sales@wirelessemporium.com>"
				cdo_subject = "Save 15% Off Your Order - Welcome To Wireless Emporium"
				
				CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
				
				newsletterTitle = "Welcome To The Club!"
				newsletterMsg = "Thanks for signing up. Check your inbox for your offer. And be sure to add us to your safe sender list so you don't miss any of our emails.  You're now connected to our latest news, special offers, and quality accessories made for just your phone."
			end if
			response.cookies("welcomeCouponUsed") = mySession
			response.cookies("welcomeCouponUsed").expires = now+30
			bSubscribed = true
		else
			newsletterMsg = "Please enter a valid email address and try again. (" & semail & ")"
		end if
	else
		newsletterMsg = "Please make sure you enter a valid email address and try again. (" & semail & ")"
	end if
else
	newsletterMsg = "Email error, please double check that you have entered a valid email. (" & semail & ")"
end if

call CloseConn(oConn)

%>
    <div onclick="closeEmailWidget('overlay2');<%if bSubscribed then%>showHide(null, 'floating15');<%end if%>" class="btn-close"></div>
    <div class="modal-content">
        <p class="discount-title"><%=newsletterTitle%></p>
        <p><%=newsletterMsg%></p>
        <p style="text-align:center;">
            <input type="button" class="modal-continue" value="CONTINUE SHOPPING" onclick="closeEmailWidget('overlay2'); <%if bSubscribed then%>showHide(null, 'floating15');<%end if%>" />
        </p>
    </div>
<%

Function isValidEmail(emailAddress)
	'Declare variables 
	Dim ValidEmail, emailParts, iLoopCounter, emailChar, acceptableChars, last2Chars 
	ValidEmail = True 'set the default result to True
	'acceptableChars are the characters that we will allow in our email
	acceptableChars="abcdefghijklmnopqrstuvwxyz.-_@"
	'use the Split function to create an array with the @ as the separator
	'so if your email was test@tester.com the email would be split into an array
	'with the first array element holding "test" and the second "tester.com"
	emailParts = Split(emailAddress, "@")
	'check to make sure that there is only 1 @ and that there are 2 parts
	'remember arrays are zero based
	'Using the UBound function will return the highest element in the array
	'So if it's a valid email the UBound function will return 1, i.e. 0 start
	If UBound(emailParts) <> 1 Then
	ValidEmail = false
	Else 
	'Check the length of each part of the email address
	'first part can be just one character, 2nd part must be atleast 4
	If Len(emailParts(0))<1 OR Len(emailParts(1))<4 Then
	ValidEmail = false
	End If
	'check first character on the left part isn't a "." using Left function
	If Left(emailParts(0), 1)="." Then
	ValidEmail = false
	End If 
	'check that there is a . in the second part of the email address - .com
	If InStr(emailParts(1), ".") <= 0 Then
	ValidEmail = false 
	End If 
	'We know there's a . now make sure there are 2 characters after the . for valid email
	last2Chars=Right(emailParts(1),2)
	If InStr(last2chars,".") Then
	ValidEmail = false
	End If
	'check that there shouldn't be a _ in the second part of the email address 
	If InStr(emailParts(1), "_") >0 Then
	ValidEmail = false 
	End If 
	End If
	'loop through each character of email
	For iLoopCounter = 1 to Len(emailAddress)
	'Use Lcase & Mid functions, Mid function used to return each individual character
	'in the email, and then Lcase converts it into lowercase
	emailChar = Lcase(Mid(emailAddress, iLoopCounter, 1)) 
	'Check if the emailAddress characters are acceptable
	If InStr(acceptableChars, emailChar) = 0 and Not IsNumeric(emailChar) Then
	ValidEmail = false
	End if
	Next
	'check if there is 2 . in a row
	If InStr(emailAddress, "..") > 0 Then
	ValidEmail=false
	End If
	'check if there is @. in a row
	If InStr(emailAddress, "@.") > 0 Then
	ValidEmail=false
	End If 
	isValidEmail=ValidEmail
End function
%>