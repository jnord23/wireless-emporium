<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<%	
set fs = CreateObject("Scripting.FileSystemObject")

itemid			=	prepInt(request.QueryString("itemid"))
screenHeight	=	prepInt(request.QueryString("screenH"))
useImg			=	prepStr(request.QueryString("useImg"))
shipping		=	prepInt(request.QueryString("shipping"))
newSize 		=	800
if useImg <> "" then useImg = prepStr(request.QueryString("useImg"))

if screenHeight <= 900 and screenHeight <> 0 then
	newSize = screenHeight - 100
end if

if shipping = 1 then
%>
<div id="id_zoomimage" style="display:inline-block; width:400px; padding:3px; border:3px solid #666; background-color:#FFF; margin-left:auto; margin-right:auto; margin-top:30px;">
	<div style="float:left; width:100%; position:relative;">
        <div style="position:absolute; top:0; right:0;"><a href="javascript:closeFloatingBox();"><img src="/images/icon-x.png" border="0" /></a></div>    
    </div>
    <div style="float:left; text-align:center; width:100%; padding-bottom:15px;">
		<div style="font-size:22px; padding:15px 0px; text-align:center; border-bottom:1px solid #CCC; width:100%;">Shipping Options &amp; Rates</div>
        <div style="padding-left:15px;">
	        <div style="font-size:14px; font-weight:bold; padding:5px 0px; text-align:left;">Your Shipping Location</div>
            <div style="font-size:12px; padding:5px 0px; text-align:left;">
            	To get a shipping price for your location, please enter your zip/postal code.
            </div>
            <div style="font-size:14px; font-weight:bold; padding:10px 0px; text-align:center;">
            	Zip/Postal Code:
            </div>
            <div style="margin:0px auto; width:160px;">
            	<form name="myZip">
                <div style="float:left;"><input type="text" name="zip" value="" size="10" style="border:1px solid #CCC; border-radius:5px; padding:5px;" /></div>
                <div style="float:left;"><input type="button" name="myAction" value="GO" class="shippingRateGoButton" onclick="getNewShipping()" /></div>
                </form>
            </div>
        </div>
    </div>
    <div style="float:left; border-top:1px dashed #ccc; padding:10px; width:370px;">
    	<div style="float:left; text-align:left;">
        	<div style="font-weight:bold; font-size:14px;">USPS First Class</div>
            <div style="font-size:12px;">Up to 4-10 Business Days</div>
        </div>
        <div style="float:right; font-size:16px; font-weight:bold; color:#ff6600;">FREE!</div>
    </div>
    <div style="float:left; border-top:1px dashed #ccc; padding:10px; width:370px;">
    	<div style="float:left; text-align:left;">
        	<div style="font-weight:bold; font-size:14px;">USPS Priority</div>
            <div style="font-size:12px;">Up to 2-4 Business Days</div>
        </div>
        <div style="float:right; font-size:16px; font-weight:bold; color:#000;">$6.99</div>
    </div>
    <div style="float:left; border-top:1px dashed #ccc; padding:10px; width:370px;">
    	<div style="float:left; text-align:left;">
        	<div style="font-weight:bold; font-size:14px;">USPS Express</div>
            <div style="font-size:12px;">Up to 1-2 Business Days</div>
        </div>
        <div style="float:right; font-size:16px; font-weight:bold; color:#000;">$24.99</div>
    </div>
    <div id="myShippingOptions"></div>
</div>
<%
	call CloseConn(oConn)
	response.End()
end if

sql = "select itempic from we_items where itemid = '" & itemid & "'"
set rs = oConn.execute(sql)

%>
<div id="id_zoomimage" style="display:inline-block; width:<%=newSize%>px; padding:3px; border:3px solid #666; background-color:#FFF; margin-left:auto; margin-right:auto; margin-top:30px;">
<%
if rs.eof and useImg = "" then
%>
	<div style="float:left; padding:50px 0px 50px 0px;">No zoom image available</div>
	<div style="float:right; padding:50px 0px 50px 0px;"><a href="javascript:closeFloatingBox();"><img src="/images/icon-x.png" border="0" /></a></div>
<%
else
%>
	<div style="float:left; width:100%; position:relative;">
        <div style="position:absolute; top:0; right:0;"><a href="javascript:closeFloatingBox();"><img src="/images/icon-x.png" border="0" /></a></div>    
    </div>
	<div style="float:left; text-align:center; width:100%;">
    <%
	if useImg <> "" then
		if rs.EOF then
	%>
    	<div style="text-align:center;"><img id="id_img_zoom" src="<%=useImg%>" border="0" /></div>
    <%
		else
			itempic = rs("itempic")
			defaultPath = "/productpics/big/zoom/" & itempic
			if fs.FileExists(Server.MapPath(defaultPath)) and instr(useImg,itempic) > 0 then
	%>
    	<div style="text-align:center;"><img id="id_img_zoom" src="<%=defaultPath%>" border="0" width="<%=newSize%>" height="<%=newSize%>" /></div>
    <%
			else
				testImg = mid(useImg,instr(useImg,".com/")+4)
				session("errorSQL") = replace(testImg,"/AltViews/","/AltViews/zoom/")
				if fs.FileExists(Server.MapPath(replace(testImg,"/AltViews/","/AltViews/zoom/"))) then
					testImg = replace(testImg,"/AltViews/","/AltViews/zoom/")
				end if
	%>
    	<div style="text-align:center;"><img id="id_img_zoom" src="<%=testImg%>" border="0" /></div>
    <%
			end if
		end if
	else
		itempic = rs("itempic")
		defaultPath = "/productpics/big/zoom/" & itempic
		if fs.FileExists(Server.MapPath(defaultPath)) then
	%>
    	<div style="text-align:center;"><img id="id_img_zoom" src="<%=defaultPath%>" border="0" width="<%=newSize%>" height="<%=newSize%>" /></div>
    <%
		else
			defaultPath = "/productpics/big/" & itempic
			if fs.FileExists(Server.MapPath(defaultPath)) then
	%>
    	<div style="text-align:center;"><img id="id_img_zoom" src="<%=defaultPath%>" border="0" /></div>
    <%
			end if
		end if
	end if
	%>
    </div>
	<div style="float:left; text-align:center;">
    <%
	bigPath = ""
	if fs.FileExists(server.mappath("/productpics/big/zoom/" & itempic)) then
		bigPath = "/productpics/big/zoom/" & itempic
	elseif fs.FileExists(server.mappath("/productpics/big/" & itempic)) then
		bigPath = "/productpics/big/" & itempic
	end if
	
	if bigPath <> "" then
	%>
    <div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onClick="fnSwapZoomImage('<%=bigPath%>');"><img id="id_altimg_zoom" src="<%=bigPath%>" width="40" height="40" border="0" /></div>
    <%
	end if

	noAltImage = true
	for iCount = 0 to 7
		path = "/productpics/AltViews/zoom/" & replace(itempic,".jpg","-" & iCount & ".jpg")
		if fs.FileExists(Server.MapPath(path)) then
			noAltImage = false
		%>
        <div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onClick="fnSwapZoomImage('<%=path%>');"><img id="id_altimg_zoom<%=iCount%>" src="<%=path%>" width="40" height="40" border="0" /></div>
        <%
		end if
	next
	
	if noAltImage then
	for iCount = 0 to 7
		path = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".jpg")
		if fs.FileExists(Server.MapPath(path)) then
			noAltImage = false
		%>
        <div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onClick="fnSwapZoomImage('<%=path%>');"><img id="id_altimg_zoom<%=iCount%>" src="<%=path%>" width="40" height="40" border="0" /></div>
        <%
		end if
	next		
	end if
	%>
    </div>
<%
end if

call CloseConn(oConn)
%>
</div>