dim oConn
set oConn = CreateObject("ADODB.Connection")
oConn.ConnectionString = "DSN=WEDB01;Uid=wireless_user;Pwd=c3llph0n3;"
oConn.Open

dim fs, file, path, nFileIdx, fileBaseName, folderName, nRows
nRows = 0
nFileIdx = 1
folderName = "C:\inetpub\wwwroot\wirelessemporium.com\mobile"
fileBaseName = "sitemap"
set fs = CreateObject("Scripting.FileSystemObject")

path = folderName & "\" & fileBaseName & nFileIdx & ".xml"
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if
set file = fs.CreateTextFile(path)
file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
file.WriteLine "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">"
file.WriteLine "<url>"
file.WriteLine vbtab & "<loc>http://m.wirelessemporium.com/</loc>"
file.WriteLine vbtab & "<priority>0.90</priority>"
file.WriteLine "</url>"

dim temp(100)
temp(1) = "/apple-cell-phone-accessories.htm"
temp(2) = "/blackberry-cell-phone-accessories.htm"
temp(3) = "/casio-cell-phone-accessories.htm"
temp(4) = "/hp-cell-phone-accessories.htm"
temp(5) = "/htc-cell-phone-accessories.htm"
temp(6) = "/huawei-cell-phone-accessories.htm"
temp(7) = "/kyocera-cell-phone-accessories.htm"
temp(8) = "/lg-cell-phone-accessories.htm"
temp(9) = "/motorola-cell-phone-accessories.htm"
temp(10) = "/nokia-cell-phone-accessories.htm"
temp(11) = "/pantech-cell-phone-accessories.htm"
temp(12) = "/samsung-cell-phone-accessories.htm"
temp(13) = "/sony-cell-phone-accessories.htm"
temp(14) = "/zte-cell-phone-accessories.htm"
temp(15) = "/other-cell-phone-accessories.htm"

dim a, URL
for a = 1 to 15
	URL = temp(a)
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://m.wirelessemporium.com" & URL & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"
next




'============================================== BRAND-MODEL
dim strBrandModelSQL, objRsBrandModel
strBrandModelSQL = 	"select	distinct a.modelid, b.brandid, b.brandname, isnull(a.modelname, '') modelname, replace(a.linkName, '.htm', '') linkName" & vbcrlf & _
					"from 	we_models a with (nolock) join we_brands b with (nolock) " & vbcrlf & _
					"	on 	a.brandid = b.brandid join we_items c with (nolock)" & vbcrlf & _
					"	on	a.modelid = c.modelid" & vbcrlf & _
					"where 	a.modelname is not null and a.hidelive = 0" & vbcrlf & _
					"	and a.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
					"	and (a.modelname not like 'all model%' or a.modelname not like 'universal%')" & vbcrlf & _
					"	and	c.hidelive = 0 and c.inv_qty <> 0 and c.price_our > 0" & vbcrlf
					
set objRsBrandModel	= oConn.execute(strBrandModelSQL)

do until objRsBrandModel.EOF
	if not isnull(objRsBrandModel("linkName")) then
		url = "accessories-for-" & formatSEO(objRsBrandModel("linkName")) & ".asp"
	end if

	call fWriteFile(url)
	objRsBrandModel.movenext
loop
set objRsBrandModel = nothing
'============================================== BRAND-MODEL END	


'============================================== category-brand START
sql	=	"select	distinct a.typeid, a.typeName, c.brandid, c.brandname" & vbcrlf & _
		"from	v_subtypematrix a join we_items b" & vbcrlf & _
		"	on	a.subtypeid = b.subtypeid join we_brands c" & vbcrlf & _
		"	on	b.brandid = c.brandid" & vbcrlf & _
		"where	a.typeid not in (8,16)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0 " & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct a.typeid, a.typeName, c.brandid, c.brandname" & vbcrlf & _
		"from	v_subtypematrix a join we_items b" & vbcrlf & _
		"	on	a.subtypeid = b.subtypeid join we_subRelatedItems t" & vbcrlf & _
		"	on	b.itemid = t.itemid join we_brands c" & vbcrlf & _
		"	on	t.brandid = c.brandid" & vbcrlf & _
		"where	a.typeid not in (8,16)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0" & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct a.typeid, a.typeName, c.brandid, c.brandname" & vbcrlf & _
		"from	v_subtypematrix a cross join we_brands c" & vbcrlf & _
		"where	a.typeid = 5" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"order by 1, 3" & vbcrlf
Set RS = CreateObject("ADODB.Recordset")
set RS = oConn.execute(sql)

do until RS.EOF
	if RS("typeid") = 15 then
		url = "sb-" & RS("brandid") & "-sm-0-sc-" & RS("typeid")  & "-" & formatSEO(RS("brandname")) & "-" & formatSEO(RS("typename")) & ".asp"
	else
		if RS("brandid") = 17 then
			url = "sc-" & RS("typeid") & "-sb-" & RS("brandid") & "-" & formatSEO(RS("brandname")) & "-iphone-" & formatSEO(RS("typename")) & ".asp"
			call fWriteFile(url)
			url = "sc-" & RS("typeid") & "-sb-" & RS("brandid") & "-" & formatSEO(RS("brandname")) & "-ipod-ipad-" & formatSEO(RS("typename")) & ".asp"
		else
			url = "sc-" & RS("typeid") & "-sb-" & RS("brandid") & "-" & formatSEO(RS("brandname")) & "-" & formatSEO(RS("typename")) & ".asp"
		end if
	end if
	call fWriteFile(url)
	RS.movenext
loop
set RS = nothing
'============================================== category-brand END


'============================================== brand-category-brand 
sql	=	"select	distinct a.typeid, a.typeName, c.brandid, c.brandname, m.modelid, m.modelname" & vbcrlf & _
		"from	v_subtypematrix a join we_items b" & vbcrlf & _
		"	on	a.subtypeid = b.subtypeid join we_brands c" & vbcrlf & _
		"	on	b.brandid = c.brandid join we_models m" & vbcrlf & _
		"	on	b.modelid = m.modelid" & vbcrlf & _
		"where	a.typeid not in (5,8,16)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0 " & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct a.typeid, a.typeName, c.brandid, c.brandname, m.modelid, m.modelname" & vbcrlf & _
		"from	v_subtypematrix a join we_subRelatedItems t" & vbcrlf & _
		"	on	a.subtypeid = t.subtypeid join we_brands c" & vbcrlf & _
		"	on	t.brandid = c.brandid join we_items b" & vbcrlf & _
		"	on	t.itemid = b.itemid join we_models m" & vbcrlf & _
		"	on	t.modelid = m.modelid" & vbcrlf & _
		"where	a.typeid not in (5,8,16)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0" & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct 5 typeid, 'Handsfree Bluetooth' typename, c.brandid, c.brandname, m.modelid, m.modelname" & vbcrlf & _
		"from	we_brands c join we_models m" & vbcrlf & _
		"	on	c.brandid = m.brandid" & vbcrlf & _
		"where	m.handsfreetypes is not null" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"order by 1, 3" & vbcrlf
		
Set RS = CreateObject("ADODB.Recordset")	
set RS = oConn.execute(sql)	
do until RS.EOF
	url = "sb-" & RS("brandID") & "-sm-" & RS("modelID") & "-sc-" & RS("typeID") & "-" & formatSEO(RS("typeName")) & "-" & formatSEO(RS("brandName")) & "-" & formatSEO(RS("modelName")) & ".asp"		
	
	call fWriteFile(url)
	RS.movenext
loop
set RS = nothing
'============================================== brand-category-brand END



'============================================== product page START
dim pSQL, objRsProduct
pSQL	=	"select	a.itemid, a.itemdesc " & vbcrlf & _
			"from	we_items a join we_models b " & vbcrlf & _
			"	on	a.modelid = b.modelid " & vbcrlf & _			
			"where	a.hidelive = 0 and a.inv_qty <> 0  " & vbcrlf & _
			"	and a.price_our > 0 " & vbcrlf & _
			"	and b.hidelive = 0 " & vbcrlf & _			
			"order by 1"
			
set objRsProduct = oConn.execute(pSQL)

do until objRsProduct.EOF
	url = "p-" & objRsProduct("itemID") & "-" & formatSEO(objRsProduct("itemDesc")) & ".htm"

	call fWriteFile(url)

	objRsProduct.movenext
loop
set objRsProduct = nothing
'============================================== product page END

file.WriteLine "</urlset>"
file.close()
set file = nothing


'============================================== index site map START
path = folderName & "\" & fileBaseName & ".xml"
'If the file already exists, delete it.
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if
set file = fs.CreateTextFile(path)
file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
file.WriteLine "<sitemapindex xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"">"

for i=1 to nFileIdx
	file.WriteLine "<sitemap>"


	file.WriteLine vbtab & "<loc>http://m.wirelessemporium.com/" & fileBaseName & i & ".xml</loc>"
	file.WriteLine vbtab & "<lastmod>" & RFC3339(now) & "</lastmod>"
	file.WriteLine "</sitemap>"
next

file.WriteLine "</sitemapindex>"
file.close()
set file = nothing
'============================================== index site map END

		

sub fWriteFile(pStrUrl)
	nRows = nRows + 1
	
	if nRows > 45000 then
		nFileIdx = nFileIdx + 1
		nRows = 0
		file.WriteLine "</urlset>"
		file.close()
		set file = nothing
		
		path = folderName & "\" & fileBaseName & nFileIdx & ".xml"
		if fs.FileExists(path) then
			fs.DeleteFile(path)
		end if
		set file = fs.CreateTextFile(path)
		file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
		file.WriteLine "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">"
	end if
	
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://m.wirelessemporium.com/" & pStrUrl & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"	
end sub

function RFC3339(myDate)
	dim myDays, myMonth, myYear, myHours, myMonths, mySeconds
	myDate = dateAdd("h",8,myDate)
	myDays = zeroPad(day(myDate),2)
	myMonth = zeroPad(month(myDate),2)
	myYear = year(myDate)
	myHours = zeroPad(hour(myDate),2)
	myMinutes = zeroPad(minute(myDate),2)
	mySeconds = zeroPad(second(myDate),2)
	RFC3339 = myYear & "-" & myMonth & "-" & myDays & "T" & myHours & ":" & myMinutes & ":" & mySeconds & "Z"
end function
 
function zeroPad(m,t)
	zeroPad = String(t-Len(m),"0") & m
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
end function