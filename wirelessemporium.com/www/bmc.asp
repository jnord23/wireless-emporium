<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
dim noAsp : noAsp = true
response.buffer = true
dim fsThumb : set fsThumb = CreateObject("Scripting.FileSystemObject")
dim basePageName : basePageName = "bmc.asp"
dim pageTitle : pageTitle = "bmc.asp"
dim pageName : pageName = "bmc"
dim picLap : picLap = 0
dim curPage : curPage = request.QueryString("curPage")
dim isMasterPage : isMasterPage = 0
dim productListingPage : productListingPage = 1
dim googleAds : googleAds = 1
dim useCustomCase : useCustomCase = false
dim isTablet : isTablet = false
dim brandID : brandID = prepInt(request.querystring("brandID"))
dim modelID : modelID = prepInt(request.querystring("modelID"))
dim categoryID : categoryID = prepInt(request.querystring("categoryID"))
dim usePages : usePages = 0
dim leftGoogleAd : leftGoogleAd = 1
extraWidgets = "no"
numProducts = 0

if curPageNum = 0 then curPageNum = 1
'if showAll <> "" then
	productsPerPage = 2000
'else
'	productsPerPage = 40
'end if

sql = "exec sp_productColorOptions"
session("errorSQL") = sql
arrColors = getDbRows(sql)

sql = "exec sp_categoryData 0, '" & categoryID & "'"
session("errorSQL") = sql
set catRS = oConn.execute(sql)
if not catRS.eof then
	parentCatName = catRS("parentTypeName")
	parentCatNameSEO = catRS("parentTypeNameSEO")	
	categoryName = catRS("typename")
	categoryNameSEO = catRS("typeNameSEO")
	parentTypeID = prepInt(catRS("typeid"))
	subCatTopText = catRS("subCatTopText")
	strSubTypeID = catRS("subtypeid")
end if
catRS = null

sql = "exec sp_modelDetailsByID " & modelID
session("errorSQL") = sql
set nameRS = oConn.execute(sql)
if not nameRS.eof then
	modelName = nameRS("modelname")
	modelImg = nameRS("modelimg")
	brandName = nameRS("brandName")
	isTablet = nameRS("isTablet")
	excludePouches = nameRS("excludePouches")
	includeNFL = nameRS("includeNFL")
	includeExtraItem = nameRS("includeExtraItem")
	HandsfreeTypes = ""
	if nameRS("HandsfreeTypes") <> "" then
		HandsfreeTypes = left(nameRS("HandsfreeTypes"),len(nameRS("HandsfreeTypes"))-1)	
	end if
end if
nameRS = null

sql = "exec sp_bmcdProducts '" & modelID & "', '" & categoryID & "', '" & strSort & "', 0, 0, '" & brandID & "', '" & categoryID & "'"
session("errorSQL") = sql
Set RS = oConn.execute(sql)

if RS.eof then
	noProducts = 1
	if modelid > 0 then
		sql = "exec sp_brandModelNamesByModelID " & modelID
		session("errorSQL") = sql
		Set RS = oConn.execute(sql)
		
		if RS.eof then
			if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
				response.Write("staging alert: Model cannot be found")
				response.End()
			else
				PermanentRedirect("/?ec=bmc9002")
			end if
		end if
		
		brandName = RS("brandName")
		modelName = RS("modelName")
		modelImg = RS("modelImg")
		isTablet = RS("isTablet")
	end if
	RS = null
else
	do until rs.eof
		numProducts = numProducts + 1
		rs.movenext
	loop
	rs.movefirst
end if

dim strBreadcrumb : strBreadcrumb = brandName & " " & modelName & " " & nameSEO(categoryName)

'todo: need to consolidate Metatags with we_XXXText
dim strH1: strH1 = ""
dim strH2: strH2 = ""
dim strAltText: strAltText = ""

dim strTypeToken : strTypeToken = "Cell Phone"
if isTablet then strTypeToken = "Tablet"

modelLink = "/" & formatSEO(brandName & " " & modelName) & "-accessories"

if isTablet then
	modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " " & strTypeToken & " Accessories</a>"	
else
	if brandid = 17 and phoneOnly = 1 then
		brandCrumb 	= "<a class=""breadcrumb"" href=""/iphone-accessories"">Apple iPhone Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " Accessories</a>"	
	elseif brandid 	= 17 and phoneOnly = 0 then	
		brandCrumb 	= "<a class=""breadcrumb"" href=""/ipod-ipad-accessories"">Apple iPod/iPad Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " Accessories</a>"
	else
		brandCrumb 	= "<a class=""breadcrumb"" href=""/" & formatSEO(brandName) & "-phone-accessories"">" & brandName & " " & strTypeToken & " Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " " & strTypeToken & " Accessories</a>"	
	end if
end if

if h1 <> "" then
	strH1 = h1
elseif strH1 = "" then
	strH1 = brandName & "&nbsp;" & modelName & "&nbsp;" & nameSEO(categoryName)
end if

if strSort = "pop" then usePopImg = "button-most-pop2.jpg" else usePopImg = "button-most-pop1.jpg"
if strSort = "new" then useNewImg = "button-new2.jpg" else useNewImg = "button-new1.jpg"
if strSort = "low" then useLowImg = "button-lowest2.jpg" else useLowImg = "button-lowest1.jpg"
if strSort = "high" then useHighImg = "button-highest2.jpg" else useHighImg = "button-highest1.jpg"

session("otherAdminOptions") = ", <a style='color:#ffffff;' href='/admin/db_update_models.asp?ModelID=" & modelID & "&BrandID=" & brandID & "&searchType=Model&submitType=Edit&submitModel=Search'>Edit " & modelName & "</a>"
%>
<!--#include virtual="/includes/template/top.asp"-->
<link href="/includes/css/bmcd.css" rel="stylesheet" type="text/css">
<style>
	.bmcProductBox { float:left; height:275px; width:186px; margin-top:10px; padding:10px 0px 5px 0px; text-align:center;}
</style>
<div class="breadcrumbFinal" id="id_breadCrumb">
	<a class="breadcrumb" href="/">HOME</a>&nbsp;&rsaquo;&nbsp;<%=brandCrumb%>&nbsp;&rsaquo;&nbsp;<%=modelCrumb%>&nbsp;&rsaquo;&nbsp;<%=strBreadcrumb%>
</div>
<div><img src="/images/brandmodelheaders/header_<%=brandID%>.jpg" width="745" height="30" border="0" alt="<%=brandName & " " & modelName%> Accessories"></div>
<div class="divContainer">
	<div class="ffl" style="padding:10px 0px 15px 0px; border-bottom:1px solid #ccc;">
    	<div class="fl" style="text-align:center; width:120px;"><img src="/productpics/models/<%=modelImg%>" border="0" alt="<%=strH1%>" width="70" /></div>
    	<div class="fl" style="text-align:left; width:408px;">
        	<h1 style="font-weight:bold; font-size:24px;"><%=strH1%></h1>
            <div class="ffl"><%=seTopText%></div>
        </div>
    	<div class="fl" style="text-align:right; width:220px;"><img src="/images/bmc/cat_header_<%=categoryid%>.png" border="0" alt="<%=strH1%>" /></div>
    </div>
</div>
<div class="divContainer" style="padding:10px 0px 0px 0px;">
	<div class="fl" style="color:#333; font-size:14px; padding:7px 0px 0px 10px;">Currently Displaying <span style="color:#ff6600; font-weight:bold;"><%=numProducts%></span> Products</div>
	<div class="fr" style="background-image:url(/images/bmc/dropdown-full.png); width:200px; height:30px; overflow:hidden;">
        <form name="frmSearch" method="post">
            <select name="cbItem" style="border:0px; background-color:transparent; -webkit-appearance: none; margin:7px 0px 0px 5px; width:215px;" onchange="onJumpTo(this.value);">
            <%
            sql = "exec sp_relatedSubtypeCats " & parentTypeID & ", " & modelid
            session("errorSQL") = sql
            arrJumpTo = getDbRows(sql)
            if not isnull(arrJumpTo) then
                for nRows=0 to ubound(arrJumpTo,2)
                    %>
                    <option value="<%=arrJumpTo(0,nRows)%>" title="<%=arrJumpTo(1,nRows)%>"><%=arrJumpTo(1,nRows)%></option>
                    <%
                next
            end if
            %>
            </select>
        </form>    
    </div>
	<div class="fr" style="color:#333; font-size:16px; padding:7px 10px 0px 0px;">Jump To:</div>
</div>
<div class="divContainer">
    <%
	curSubTypeID = -1
	a = 0
	if noProducts = 0 then
		do until rs.eof
			DoNotDisplay = 0
			curAlwaysInStock = rs("alwaysInStock")
			curItemID = rs("itemID")
			partnumber = rs("partnumber")
			if isnull(curAlwaysInStock) then curAlwaysInStock = false
			if left(partnumber,3) = "WCD" then DoNotDisplay = 1

			itemPic = RS("itemPic")
			itemImgPath = Server.MapPath("/productpics/thumb") & "\" & RS("itemPic")
			itemImgPath2 = Server.MapPath("/productpics/thumb") & "\" & RS("itemPic")
			useImgPath = "/productpics/thumb/" & RS("itemPic")
			
			if not fsThumb.FileExists(itemImgPath) then DoNotDisplay = 1
			if DoNotDisplay = 0 then
				a = a + 1
				picLap = picLap + 1
				altText = brandName & " " & modelName & " " & singularSEO(categoryName) & " - " & RS("itemDesc")
				if categoryid = "14" and modelid = "0" then altText = "Cell Phone Charms & Bling : " & altText
				
				if len(RS("itemDesc")) >= 73 then
					itemDescEtc = left(RS("itemDesc"),70) & "..."
				else
					itemDescEtc = RS("itemDesc")
				end if
				itemDescEtc = replace(itemDescEtc, "/", " / ")
				productLink = "/p-" & RS("itemid") &"-"& formatSEO(RS("itemDesc"))
				colorid = rs("colorid")				
				onSale = RS("ActiveItemValueType") <> "OriginalPrice"
				customize = rs("customize")
				allNames = allNames & formatSEO(itemDescEtc) & ","
				allPrices = allPrices + prepInt(RS("price_Our"))
				availColors = rs("availColors")
				if instr(availColors,",") > 0 then 
					numAvailColors = ubound(split(availColors, ",")) + 1
				else 
					numAvailColors = 1
				end if
				
				if rs("subtypeid") <> curSubTypeID then
				%>
                <div id="subtype_header_<%=rs("subtypeid")%>" class="ffl" style="border-top:1px solid #ccc; margin-top:10px; padding:20px 0px 10px 0px;">
                	<div class="fl" style="padding:0px 10px 0px 0px;"><img src="/images/bmc/subcat_header_<%=rs("subtypeid")%>.png" border="0" alt="<%=strH1%>" /></div>
                	<div class="fl" style="font-weight:bold; font-size:18px; color:#333; padding-top:8px;"><%=brandName & " " & modelName & " " & rs("subtypename")%></div>
                	<div class="fr" style="cursor:pointer; color:#06C;" onclick="location.href='#id_breadCrumb'"><img src="/images/bmc/back-to-top-arrow.png" border="0" alt="Back To Top" /> &nbsp; Back to top</div>
				</div>
                <%
				end if
				%>
				<div class="bmcProductBox" id="itemListID_<%=picLap%>">
					<a href="<%=productLink%>" title="<%=altText%>">
					<div class="bmc_productPic" title="<%=altText%>">
						<div id="id_productImage_<%=curItemID%>" style="position:relative;">
							<% if onSale then %><div onclick="window.location='<%=productLink%>'" class="product-bmc-onSale clickable"></div><% end if %>
							<img src="<%=useImgPath%>" border="0" width="100" height="100" title="<%=altText%>" class="clickable">
							<% if customize then %>
							<div style="position:absolute; right:0px; top:0px; cursor:pointer;" onclick="window.location='<%=productLink%>'"><img src="/images/icons/customize-sm.png" border="0" /></div>
							<% end if %>
						</div>
					</div>
					</a>
					<% if RS("inv_qty") = 0 and not curAlwaysInStock then %>
					<div class="product-bmc-soldOut" align="center">
						<img src="/images/sold_out.png" width="85">
					</div>
					<% else %>
					<div class="product-bmc-details" align="center">
						<div id="id_productLink_<%=curItemID%>" class="product-bmc-desc">
							<a class="cellphone-link" href="<%=productLink%>" title="<%=altText%>" alt="<%=altText%>"><%=itemDescEtc%></a>
						</div>
						<% if numAvailColors > 1 then %>
						<div style="width:100%; padding-top:5px; height:32px; clear:both; overflow:hidden;">
							<div align="center" style="font-weight:bold;">Available Colors:</div>
							<div align="center"><%=generateColorPicker(arrColors, curItemID, availColors, colorid, numAvailColors, onSale)%></div>
							<div style="display:none;" id="preloading_deck_<%=curItemID%>"></div>                            
						</div>
						<% end if %>                                                    
					</div>
					<div style="float:left; width:100%; padding-top:5px;" align="center" onclick="window.location='<%=productLink%>'">
						<span class="boldText">Our&nbsp;Price:&nbsp;</span>
						<span class="pricing-orange2"><%=formatCurrency(RS("price_Our"))%></span><br>
						<span class="pricing-gray2">List&nbsp;Price:&nbsp;<s><%=formatCurrency(prepInt(RS("price_Retail")))%></s></span>
					</div>
					<div id="mvt_bmcd_fresshipping" style="float:left; width:100%; padding:5px 0px 5px 0px;" align="center" onclick="window.location='<%=productLink%>'">
						<img src="/images/bmc/we-freeshipping1.jpg" border="0" />
					</div>
					<% end if %>
				</div>
			<%
			end if
			curSubTypeID = rs("subtypeid")
			rs.movenext
		loop
	else
	%>
	<div class="divContainer" style="font-weight:bold; font-size:18px; color:#F00; padding:20px 0px 20px 0px; text-align:center; border-top:1px solid #ccc;">
		No Products Currently Available
	</div>    
    <%
	end if
	%>
</div>
<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
    <%
    sql = "exec sp_getCatsByModel " & modelID & ", 0"
	session("errorSQL") = sql
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	a = 0

	if not RS.eof then
	%>
    <!-- other accessories start -->
    <tr>
    	<td style="padding-top:50px;">
            <div style="background-color:#ebebeb; padding:10px; border:1px solid #ccc;">
                <div style="font-size:15px; font-weight:bold; color:#494949;" title="OTHER <%=ucase(brandName) & " " & ucase(modelName)%> ACCESSORIES" align="center">
                	<h3 class="brandModel">OTHER <%=ucase(brandName) & " " & ucase(modelName)%> ACCESSORIES</h3>
				</div>
            </div>
        </td>
    </tr>
    <tr>
    	<td>
            <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                <tr>
                	<td>
						<%
                        dim handsFree : handsFree = 0
						do until RS.eof
							thisType = replace(RS("typeName"), "/", " / ")
							useTypeID = rs("typeid")
							if useTypeID = 5 then handsFree = 1
							if useTypeID = 16 then useTypeID = 15
							strLink = replace(replace(replace(replace(replace(replace(rs("urlStructure"), "{brandid}", brandid), "{modelid}", modelid), "{typeid}", useTypeID), "{brandname}", formatSEO(brandName)), "{modelname}", formatSEO(modelName)), "{typename}", formatSEO(thisType))
							strLink = replace(strLink,".asp","")
							altText = modelName & " " & thisType
						%>
						<div id="catMainBox">
							<div id="catImg"><a href="<%=strLink%>" title="<%=altText%>"><img src="/images/categories/we_cat_<%=useTypeID%>.jpg" width="165" border="0" alt="<%=altText%>" title="<%=altText%>" /></a></div>
							<div id="catDetails"><a href="<%=strLink%>" title="<%=altText%>" class="cellphone3-link"><%=altText%></a></div>
						</div>
						<%
							RS.movenext
						loop
						
						if handsFree = 0 then
							thisType = "Handsfree Bluetooth"
							useTypeID = 5
							strLink = replace(replace(replace(replace(replace(replace("sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}", "{brandid}", brandid), "{modelid}", modelid), "{typeid}", useTypeID), "{brandname}", formatSEO(brandName)), "{modelname}", formatSEO(modelName)), "{typename}", formatSEO(thisType))
							altText = modelName & " " & thisType
						%>
						<div id="catMainBox">
							<div id="catImg"><a href="<%=strLink%>" title="<%=altText%>"><img src="/images/categories/we_cat_<%=useTypeID%>.jpg" width="165" border="0" alt="<%=altText%>" title="<%=altText%>" /></a></div>
							<div id="catDetails"><a href="<%=strLink%>" title="<%=altText%>" class="cellphone3-link"><%=altText%></a></div>
						</div>
						<%
						end if
                        %>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!-- other accessories end -->
	<%
	end if
	%>
    <tr><td align="center" class="bottomText"><%=seBottomText%></td></tr>    
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<script language="javascript">var isMasterPage = 0</script>
<!--#include virtual="/includes/template/bottom.asp"-->
<%
	useURL = request.ServerVariables("HTTP_X_REWRITE_URL")
	if instr(useURL,"?") > 0 then useUrl = left(useURL,instr(useURL,"?")-1)
%>
<script>
	function preloading(pItemID, selectedColorID)
	{
		ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmcd&uType=pre&itemid=' + pItemID + '&selectedColorID=' + selectedColorID, 'preloading_deck_' + pItemID);		
	}
	
	function changeProduct(pItemID, selectedColorID, numColors, selectedIDX, onSale)
	{
		curObj = null;
		for(i=0; i < numColors; i++) 
		{
			curObj = 'div_colorOuterBox_' + pItemID + '_' + i;
			if (document.getElementById(curObj) != null) document.getElementById(curObj).className = 'colorBoxUnselected';
		}
		curObj = 'div_colorOuterBox_' + pItemID + '_' + selectedIDX;
		document.getElementById(curObj).className = 'colorBoxSelected';

		ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmcd&uType=img&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&onsale=' + onSale + '&adminOption=<%=prepInt(session("adminID"))%>', 'id_productImage_'+pItemID);
		ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmcd&uType=link&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&adminOption=<%=prepInt(session("adminID"))%>', 'id_productLink_'+pItemID);
		ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmcd&uType=price&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&adminOption=<%=prepInt(session("adminID"))%>', 'id_productPricing_'+pItemID);
	}
	
	function fnSort(sortOption)
	{
		var useURL = window.location + ''
		if (useURL.indexOf("?") > 0) {
			useURL = useURL.substring(0,useURL.indexOf("?"))
		}
		window.location = useURL + '?strSort=' + sortOption
		return false;
	}
	
	function onJumpTo(subtypeid) {
		location.href = "#subtype_header_" + subtypeid;
	}
</script>