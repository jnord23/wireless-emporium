<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
set fsThumb = CreateObject("Scripting.FileSystemObject")
response.buffer = true
dim basePageName : basePageName = "brand-model-category-details.asp"
dim pageTitle : pageTitle = "brand-model-category-details.asp"
curPageNum = prepInt(request.QueryString("page"))
if isnull(curPageNum) or len(curPageNum) < 1 then curPageNum = 1
showAll = prepStr(request.QueryString("show"))
dim picLap : picLap = 0
dim curPage : curPage = request.QueryString("curPage")

isMasterPage = 1

pageName = "bmcd"

strSort = prepStr(request("strSort"))

dim productListingPage : productListingPage = 1
googleAds = 1
if showAll <> "" then
	productsPerPage = 2000
else
	productsPerPage = 40
end if

dim modelid, categoryid, isTablet, parentTypeID, useCustomCase
useCustomCase = false
isTablet = false

brandID = prepInt(request.querystring("brandID"))
modelID = prepInt(request.querystring("modelID"))
categoryID = prepInt(request.querystring("categoryID"))
musicSkins = prepInt(request.querystring("musicSkins"))
musicSkinGenre = prepStr(request.querystring("musicSkinGenre"))
musicSkinArtistID = prepInt(request.querystring("musicSkinArtist"))

if instr(musicSkins,",") > 0 then
	musicSkinsArray = split(musicSkins,",")
	musicSkins = musicSkinsArray(0)
end if
if instr(musicSkinGenre,",") > 0 then
	musicSkinGenreArray = split(musicSkinGenre,",")
	musicSkinGenre = musicSkinGenreArray(0)
end if
session("errorSQL2") = musicSkins

if musicSkinGenre = "r-b" then musicSkinGenre = "R&B"
if musicSkinGenre = "tv-movies" then musicSkinGenre = "TV/Movies"
if musicSkinGenre = "screen-protectors" then musicSkinGenre = "Screen Protectors"
if len(musicSkinGenre) > 0 then
	musicSkinGenre = ucase(left(musicSkinGenre,1)) & right(musicSkinGenre,len(musicSkinGenre)-1)
end if
if instr(REWRITE_URL, "apple-iphone") > 0 then
	phoneOnly = 1
else
	phoneOnly = 0
end if

usePages = 1
leftGoogleAd = 1

strSubTypeID = ""
sqlCustomCase = ""
if prepInt(categoryid) = 2000 then
	sql = "SELECT 'Custom Cases' typename, subtypeid, typeid, isnull(subCatTopText, '') subCatTopText, 'Custom Cases' parentTypeNameSEO, 'Custom Cases' parentTypeName, 'Custom Cases' typeNameSEO from v_subTypeMatrix WHERE typeid = '3'"
	useCustomCase = true
	sqlCustomCase = " and exd.customize = 1 "
elseif prepInt(categoryid) > 999 then
	sql = "SELECT subTypeName typename, subtypeid, typeid, isnull(subCatTopText, '') subCatTopText, typeNameSEO_WE parentTypeNameSEO, typename parentTypeName, subTypeNameSEO_WE typeNameSEO from v_subTypeMatrix WHERE subtypeid = '" & categoryid & "'"
	sqlCustomCase = ""
else
	sql = "SELECT typename, subtypeid, typeid, isnull(subCatTopText, '') subCatTopText, typeNameSEO_WE parentTypeNameSEO, typename parentTypeName, subTypeNameSEO_WE typeNameSEO from v_subTypeMatrix WHERE typeid = '" & categoryid & "'"
	sqlCustomCase = ""
end if
session("errorSQL") = sql
set catRS = oConn.execute(sql)
if not catRS.eof then
	parentCatName = catRS("parentTypeName")
	parentCatNameSEO = catRS("parentTypeNameSEO")	
	categoryName = catRS("typename")
	categoryNameSEO = catRS("typeNameSEO")
	parentTypeID = catRS("typeid")
	subCatTopText = catRS("subCatTopText")
	do until catRS.eof
		strSubTypeID = strSubTypeID & catRS("subtypeid") & ","
		catRS.movenext
	loop
	strSubTypeID = left(strSubTypeID, len(strSubTypeID) - 1)
end if
if strSubTypeID = "" then strSubTypeID = "9999999" end if
catRS = null

brandID = prepInt(brandID)
modelID = prepInt(modelID)
parentTypeID = prepInt(parentTypeID)

if (brandID = 0 or modelID = 0) and (parentTypeID <> 8 and parentTypeID <> 15) then call PermanentRedirect("/?ec=bmc9001") end if

sql = "select a.modelname, a.modelimg, b.brandname, a.isTablet, isnull(a.handsfreetypes, '') handsfreetypes, a.excludePouches, a.includeNFL, a.includeExtraItem from we_models a join we_brands b on a.brandid = b.brandid where a.modelid = '" & modelid & "'"
session("errorSQL") = sql
set nameRS = oConn.execute(sql)
if not nameRS.eof then
	modelName = nameRS("modelname")
	modelImg = nameRS("modelimg")
	brandName = nameRS("brandName")
	isTablet = nameRS("isTablet")
	excludePouches = nameRS("excludePouches")
	includeNFL = nameRS("includeNFL")
	includeExtraItem = nameRS("includeExtraItem")
	HandsfreeTypes = ""
	if nameRS("HandsfreeTypes") <> "" then
		HandsfreeTypes = left(nameRS("HandsfreeTypes"),len(nameRS("HandsfreeTypes"))-1)	
	end if
end if
nameRS = null

if modelid = 0 then
	sql = "select brandname from we_brands where brandid = '" & brandid & "'"
	set nameRS = oConn.execute(sql)
	if not nameRS.eof then
		brandName = nameRS("brandName")
	end if
end if

sql = "select id, color, colorCodes, borderColor from xproductcolors order by 1"
session("errorSQL") = sql
arrColors = getDbRows(sql)

'response.write "{parentTypeID:" & parentTypeID & "}"
if parentTypeID = 5 then
	sql	=	"select	pnd.alwaysInStock, c.flag1, c.designType, c.itemDesc " & vbCRLF & _
			"	,	c.itemID, c.itemPic, c.typeID, c.price_our, c.price_retail, (select count(*) from we_reviews rv where itemID in (select itemID from we_Items where partNumber = c.partNumber) and approved = 1) as reviews, (select top 1 inv_qty from we_items where partNumber = c.partNumber order by master desc, inv_qty desc) as inv_qty" & vbCRLF & _
			"	,	c.vendor, c.subtypeID, c.itemKit_NEW, c.hotDeal, isnull( tv.[ActiveItemValueType], 'OriginalPrice') as [ActiveItemValueType], c.numberOfSales, c.partnumber, 0 related" & vbcrlf & _
			"	,	x.master, v.colorSlaves, c.colorid, isnull(v.cnt, 0) numAvailColors, exd.customize" & vbCRLF & _
			"	,	convert(varchar(1000), substring((	select (',' + convert(varchar(100), i.colorid)) from we_items i where i.partnumber in (select slave from xproductcolormatrix where master = x.master) and i.inv_qty <> 0 and i.hidelive = 0 and	(select top 1 inv_qty from we_items where partNumber = i.partNumber order by master desc, inv_qty desc) > 0 order by i.colorid for xml path('')), 2, 1000)) availColors" & vbCRLF & _
			"from	we_items c with (nolock) left outer join v_itemOnSale tv" & vbcrlf & _
			"	on	c.itemid = tv.itemid left outer join v_colorMatrix v" & vbcrlf & _
			"	on	c.partnumber = v.master2 and v.hideLive = 0 left outer join xproductcolormatrix x" & vbcrlf & _
			"	on	c.partnumber = x.slave and x.hideLive = 0" & vbcrlf & _
			"	left join we_pnDetails pnd on c.PartNumber = pnd.partnumber" & vbcrlf & _
			"	left join we_ItemsExtendedData exd on c.PartNumber = exd.partnumber" & vbcrlf & _
			"where	c.subtypeid in (" & strSubTypeID & ")" & vbcrlf & _
			"	and c.price_our > 0 and c.hidelive = 0 and c.ghost = 0" & vbcrlf & _
			"	and	c.handsfreetype in ('" & replace(HandsfreeTypes,",","','") & "')" & vbcrlf & _
			"	and	((v.master is not null and x.slave is not null) or (v.master is null and x.slave is null))" & vbcrlf
elseif parentTypeID = 8 or parentTypeID = 15 then
	sql = 	"	select	pnd.alwaysInStock, c.flag1, c.designType, c.itemDesc" & vbCRLF & _
			"		,	c.itemID, c.itemPic, c.typeID, c.price_our, c.price_retail, (select count(*) from we_reviews rv where itemID in (select itemID from we_Items where partNumber = c.partNumber) and approved = 1) as reviews, (select top 1 inv_qty from we_items where partNumber = c.partNumber order by master desc, inv_qty desc) as inv_qty" & vbCRLF & _
			"		,	c.vendor, c.subtypeID, c.itemKit_NEW, c.hotDeal, isnull( tv.[ActiveItemValueType], 'OriginalPrice') as [ActiveItemValueType], c.numberOfSales, c.partnumber, 0 related" & vbcrlf & _
			"		,	x.master, v.colorSlaves, c.colorid, isnull(v.cnt, 0) numAvailColors, exd.customize" & vbCRLF & _
			"		,	convert(varchar(1000), substring((	select (',' + convert(varchar(100), i.colorid)) from we_items i where i.partnumber in (select slave from xproductcolormatrix where master = x.master) and i.inv_qty <> 0 and i.hidelive = 0 and	(select top 1 inv_qty from we_items where partNumber = i.partNumber order by master desc, inv_qty desc) > 0 order by i.colorid for xml path('')), 2, 1000)) availColors" & vbCRLF & _
			"	from	we_items c with (nolock) left outer join v_itemOnSale tv" & vbCRLF & _
			"		on	c.itemid = tv.itemid left outer join v_colorMatrix v" & vbcrlf & _
			"	on	c.partnumber = v.master2 and v.hideLive = 0 left outer join xproductcolormatrix x" & vbcrlf & _
			"	on	c.partnumber = x.slave and x.hideLive = 0" & vbcrlf & _
			"	left join we_pnDetails pnd on c.PartNumber = pnd.partnumber" & vbcrlf & _
			"	left join we_ItemsExtendedData exd on c.PartNumber = exd.partnumber" & vbcrlf & _
			"	where	c.subtypeid in (" & strSubTypeID & ")" & vbcrlf & _
			"		and	((v.master is not null and x.slave is not null) or (v.master is null and x.slave is null))" & vbcrlf
	if modelid > 0 then
		sql	=	sql & "			and	c.modelid = '" & modelID & "'" & vbCRLF
	end if
	if brandid > 0 then
		sql	=	sql & "			and	(c.brandid = '" & brandID & "' or c.brandid=12)" & vbCRLF
	end if
	sql	=	sql & 	"		and	c.price_our > 0 and c.hidelive = 0 and c.ghost = 0" & vbCRLF
elseif musicSkins = 1 then
	Set Jpeg = Server.CreateObject("Persits.Jpeg")
	Jpeg.Quality = 100
	Jpeg.Interpolation = 1
	sql = 	"	select 	cast(1 as bit) as alwaysInStock, 0 as reviews, 100 as inv_qty, null as ItemKit_NEW, a.musicSkinsID, a.defaultImg, a.id as itemID, a.artist + ' ' + a.designName as itemDesc" & vbcrlf & _
			"		, 	a.image as itemPic, a.price_we as price_our, a.msrp as price_retail, a.genre" & vbcrlf & _
			"		, 	e.artist, 'OriginalPrice' as [ActiveItemValueType], 1 numberOfSales, a.musicSkinsID partnumber, 0 related" & vbcrlf & _
			"		, 	null master, null colorSlaves, null colorid, 0 numAvailColors, 0 customize" & vbcrlf & _
			"		,	'' availColors" & vbCRLF & _			
			"	from 	we_items_musicSkins a left outer join we_items_musicskins_artist e" & vbcrlf & _
			"		on 	a.artist = e.artist" & vbcrlf & _
			"	where 	a.skip = 0 " & vbcrlf & _
			"		and a.deleteItem = 0 " & vbcrlf & _
			"		and (a.artist <> '' and a.artist is not null) " & vbcrlf & _
			"		and (a.designname <> '' and a.designname is not null) " & vbcrlf & _
			"		and a.deleteItem = 0 " & vbcrlf & _
			"		and a.modelID = '" & modelID & "' " & vbcrlf & _
			"		and a.genre like '%" & musicSkinGenre & "%'" & vbcrlf
	if "0" <> musicSkinArtistID and len(musicSkinArtistID) > 0 then
		sql = sql & "		and e.id = '" & musicSkinArtistID & "'" & vbcrlf
	end if	
else
	sql = 	"	select	pnd.alwaysInStock, c.flag1, c.designType, c.itemDesc" & vbCRLF & _
			"		,	c.itemID, c.itemPic, c.typeID, c.price_our, c.price_retail, (select count(*) from we_reviews rv where itemID in (select itemID from we_Items where partNumber = c.partNumber) and approved = 1) as reviews, (select top 1 inv_qty from we_items where partNumber = c.partNumber order by master desc, inv_qty desc) as inv_qty" & vbCRLF & _
			"		,	c.vendor, c.subtypeID, c.itemKit_NEW, c.hotDeal, isnull( tv.[ActiveItemValueType], 'OriginalPrice') as [ActiveItemValueType], c.numberOfSales, c.partnumber, 0 related" & vbcrlf & _
			"		,	x.master, v.colorSlaves, c.colorid, isnull(v.cnt, 0) numAvailColors, exd.customize" & vbCRLF & _
			"		,	convert(varchar(1000), substring((	select (',' + convert(varchar(100), i.colorid)) from we_items i where i.partnumber in (select slave from xproductcolormatrix where master = x.master) and i.modelid = '" & modelID & "' and i.inv_qty <> 0 and i.hidelive = 0 and (select top 1 inv_qty from we_items where partNumber = i.partNumber order by master desc, inv_qty desc) > 0 order by i.colorid for xml path('')), 2, 1000)) availColors" & vbCRLF & _
			"	from	we_items c with (nolock) left outer join v_itemOnSale tv" & vbCRLF & _
			"		on	c.itemid = tv.itemid left outer join v_colorMatrix v" & vbcrlf & _
			"		on	c.partnumber = v.master2 and v.hideLive = 0 left outer join xproductcolormatrix x" & vbcrlf & _
			"		on	c.partnumber = x.slave and x.hideLive = 0" & vbcrlf & _
			"		left join we_pnDetails pnd on c.PartNumber = pnd.partnumber" & vbcrlf & _
			"		left join we_ItemsExtendedData exd on c.PartNumber = exd.partnumber" & vbcrlf & _
			"	where	c.modelid = '" & modelID & "' and c.subtypeid in (" & strSubTypeID & ")" & vbCRLF & _
			"		and	c.price_our > 0 and c.hidelive = 0 and c.ghost = 0" & vbCRLF & _
			"		and	((v.master is not null and x.slave is not null) or (v.master is null and x.slave is null))" & sqlCustomCase & vbcrlf & _
			"	union" & vbCRLF & _
			"	select	pnd.alwaysInStock, c.flag1, c.designType, c.itemDesc" & vbCRLF & _
			"		,	c.itemID, c.itemPic, c.typeID, c.price_our, c.price_retail, (select count(*) from we_reviews rv where itemID in (select itemID from we_Items where partNumber = c.partNumber) and approved = 1) as reviews, (select top 1 inv_qty from we_items where partNumber = c.partNumber order by master desc, inv_qty desc) as inv_qty" & vbCRLF & _
			"		,	c.vendor, c.subtypeID, c.itemKit_NEW, c.hotDeal, isnull( tv.[ActiveItemValueType], 'OriginalPrice') as [ActiveItemValueType], c.numberOfSales, c.partnumber, 1 related" & vbcrlf & _
			"		,	x.master, v.colorSlaves, c.colorid, isnull(v.cnt, 0) numAvailColors, exd.customize" & vbCRLF & _
			"		,	convert(varchar(1000), substring((	select (',' + convert(varchar(100), i.colorid)) from we_items i where i.partnumber in (select slave from xproductcolormatrix where master = x.master) and i.inv_qty <> 0 and i.hidelive = 0 and	(select top 1 inv_qty from we_items where partNumber = i.partNumber order by master desc, inv_qty desc) > 0 order by i.colorid for xml path('')), 2, 1000)) availColors" & vbCRLF & _
			"	from	we_items c with (nolock) join we_subRelatedItems r" & vbCRLF & _
			"		on	c.itemid = r.itemid left outer join v_itemOnSale tv" & vbCRLF & _
			"		on	c.itemid = tv.itemid left outer join v_colorMatrix v" & vbcrlf & _
			"		on	c.partnumber = v.master2 and v.hideLive = 0 left outer join xproductcolormatrix x" & vbcrlf & _
			"		on	c.partnumber = x.slave and x.hideLive = 0" & vbcrlf & _
			"		left join we_pnDetails pnd on c.PartNumber = pnd.partnumber" & vbcrlf & _
			"		left join we_ItemsExtendedData exd on c.PartNumber = exd.partnumber" & vbcrlf & _
			"	where	r.modelid = '" & modelID & "' and r.subtypeid in (" & strSubTypeID & ")" & vbCRLF & _
			"		and	c.price_our > 0 and c.hidelive = 0 and c.ghost = 0" & vbcrlf & _
			"		and	((v.master is not null and x.slave is not null) or (v.master is null and x.slave is null))" & sqlCustomCase & vbcrlf
	if categoryid = 1330 then
		sql = sql & "	union" & vbCRLF & _ 
					"	select	pnd.alwaysInStock, c.flag1, c.designType, c.itemDesc" & vbCRLF & _ 
					"		,	c.itemID, c.itemPic, c.typeID, c.price_our, c.price_retail, c.inv_qty" & vbCRLF & _ 
					"		,	c.vendor, c.subtypeID, c.itemKit_NEW, c.hotDeal, isnull( tv.[ActiveItemValueType], 'OriginalPrice') as [ActiveItemValueType], c.numberOfSales, c.partnumber, 0 related" & vbcrlf & _
					"		,	null master, null colorSlaves, null colorid, 0 numAvailColors, 0 as customize, '' availColors" & vbCRLF & _
					"	from	v_univAnt c left outer join v_itemonSale tv " & vbCRLF & _ 	
					"		on	c.itemid = tv.itemid" & vbCRLF &_
					"		left join we_pnDetails pnd on c.PartNumber = pnd.partnumber" & vbcrlf
	end if	
end if

if musicSkins = 0 then
	select case strSort
		case "pop"
			sql = sql & " order by numberOfSales desc, flag1 desc, c.itemID desc"
		case "new"
			sql = sql & " order by c.itemID desc"
		case "low"
			sql = sql & " order by price_our, flag1 desc, c.itemID desc"		
		case "high"
			sql = sql & " order by price_our desc, flag1 desc, c.itemID desc"
		case else
			sql = sql & " order by flag1 desc, c.itemID desc"
	end select
else
	sql = sql & "	order by a.artist, a.genre"	
end if
session("errorSQL") = sql
'response.Write(request.ServerVariables("URL") & "?" & request.ServerVariables("QUERY_STRING") & "<br>")
'response.write "<pre>" & replace(sql,vbcrlf,"<br />") & "</pre>"
'response.write "<pre>" & sql & "</pre>"
'response.end
Set RS = Server.CreateObject("ADODB.Recordset")
RS.open sql, oConn, 0, 1

if parentTypeID = 8 then
	brandName = ""
	modelName = ""
	modelImg = "mini-phones.jpg"
	excludePouches = false
	includeNFL = false
	includeExtraItem = null
elseif parentTypeID = 15 then
	modelName = ""
	modelImg = "mini-phones.jpg"
	excludePouches = false
	includeNFL = false
	includeExtraItem = null
elseif musicSkins = 1 then
	categoryName = "Music Skins"
end if

noProducts = 0
if RS.eof then
	
	noProducts = 1
	
	if modelid > 0 then
		sql = "select b.brandName, a.modelName, a.modelImg, a.isTablet, '" & categoryName & "' as typeName from we_models a left join we_brands b on a.brandID = b.brandID where a.modelID = '" & modelID & "'"
		session("errorSQL") = sql
		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.open sql, oConn, 0, 1
		
		if RS.eof then 
			PermanentRedirect("/?ec=bmc9002")
		end if
		
		brandName = RS("brandName")
		modelName = RS("modelName")
		modelImg = RS("modelImg")
		isTablet = RS("isTablet")
	end if
else
	if musicSkins = 1 then musicSkinArtistName = RS("artist") end if
	
	productCount = 0
	if not RS.eof then
		lap = -1
		do while not rs.EOF
			lap = lap + 1
			'check for zero quantity, reviews and no product image
			if (RS("inv_qty") <= 0 and RS("reviews") < 2) or (not fsThumb.FileExists(Server.MapPath("/productpics/thumb") & "\" & RS("itemPic")) and musicSkins <> 1) then
				'no product count
			else
				productCount = productCount + 1
			end if
			RS.movenext
		loop		
		RS.movefirst
	end if

	if curPageNum > totalPages then curPageNum = totalPages

	if curPageNum > 1 then
		loopPage = 1
		pagesSkipped = 0
		productsSkipped = 0
		do while cdbl(loopPage) < cdbl(curPageNum)
			pagesSkipped = pagesSkipped + 1
			for i = 1 to 20000
				if productsSkipped >= productsPerPage then exit for
				'check for zero quantity, reviews and no product image
				if RS.EOF then
					exit for
				else
					if (RS("inv_qty") <= 0 and RS("reviews") < 2) or (not fsThumb.FileExists(Server.MapPath("/productpics/thumb") & "\" & RS("itemPic")) and musicSkins <> 1) then
						if rs.eof then exit for
						RS.movenext
					else
						if rs.eof then exit for
						RS.movenext
						productsSkipped = productsSkipped + 1
					end if
				end if
			next
			productsSkipped = 0
			loopPage = loopPage + 1
		loop
	end if	
end if

dim SEtitle, SEdescription, SEkeywords, h1, topText, bottomText
if parentTypeID = "8" then
	topText = "GET NOTICED every time you raise your phone to your ear! Bling your cell phone with popular <a class=""topText-link"" href=""/p-7679-pink-and-clear-rhinestone-crystals--300-count-peel--n--stick-.asp"" title=""Cell Phone Bling"">Cell Phone Bling</a> Kits & <a class=""topText-link"" href=""http://www.wirelessemporium.com/p-18634-crystal-charms---heart-in-heart.asp"" title=""Cell Phone Charms"">Cell Phone Charms</a> AT WHOLESALE COST! Cell phone bling kits have grown in popularity and are used by cell phone users and famous stars across the world. They are a quick and easy way to personalize your cell phone. Best of all, when you buy your bling kit at WirelessEmporium.com, you know you're getting the BEST DEAL ONLINE - GUARANTEED!</p>"
	topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">We offer a wide range of cell phone charms like crystal charms & phone pets charms. Apart from cell phone charms & bling, we offer a complete line of accessories for your cell phone at WHOLESALE PRICES! Buy Phone Pets Charms like the pig, black cat, mouse or yellow, pink, blue <a class=""topText-link"" href=""/p-7676-purple-and-clear-rhinestone-crystals--300-count-peel--n--stick-.asp"" title=""Crystal Cell Phone Charms"">crystal cell phone charms</a> at wholesale prices!"
elseif parentTypeID = "14" then
	SEtitle = "Cell Phone Charms: Stylish Cell Phone Bling Kits at Wireless Emporium"
	SEdescription = "Buy Fashionable Cell Phone Charms at Wireless Emporium. We are the biggest store for Cell Phone Bling Kits & Jewels at Wholesale Prices and Free Shipping!"
	SEkeywords = "cell phone charms, cell phone bling, cute cell phone charms, cell phone charms wholesale, cell phone jewels, flashing cell phone charms"
	topText = "Let your phone shine with cell phone charms! Choose from dozens of cute and cuddly, bright and shiny charms for your cell phone. WirelessEmporium.com is the biggest cell phone accessory store online. We have a huge cell phone bling & charm collection including many other <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Cell Phone Accessories and Charms"">cell phone accessories</a> for all cell phone brands including Nokia, Samsung, Motorola, LG, Apple & many more.</p>"
	topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">Buy Cellphone Blings & Charms to accessorize your phone at WirelessEmporium.com! Bling your phone with a wide array of bling kits and bling accessories. Tag your phone with a cute, colorful personalized charm. Pay prices that you simply won't believe and get FREE SHIPPING to boot!"
end if

dim modelLink
if modelID = 940 then
	modelLink = "/ipod-ipad-accessories.asp"
else
	modelLink = "/T-" & modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
end if

if SEtitle = "" or SEdescription = "" or SEkeywords = "" then
	SQL = "SELECT title, description, keywords FROM we_MetaTags WHERE brandID = '" & brandID & "' AND modelid = '" & modelid & "' AND typeID = '" & categoryid & "'"
	if musicSkinGenre <> "" then 
		SQL = SQL & " and genre = '" & musicSkinGenre & "'"
		h1 = brandName & " " & modelName & " " & musicSkinGenre & " " & nameSEO(categoryName)
		strBreadcrumb = brandName & " " & modelName & " " & musicSkinGenre & " " & nameSEO(categoryName)
	end if
	
	Set RSmeta = Server.CreateObject("ADODB.Recordset")
	RSmeta.open sql, oConn, 0, 1

	if not RSmeta.eof then
		SEtitle = RSmeta("title")
		SEdescription = RSmeta("description")
		SEkeywords = RSmeta("keywords")
	else
		SEtitle = brandName & " " & modelName & " " & nameSEO(categoryName) & " - " & brandName & " " & modelName & " Cell Phone Accessories"
		SEdescription = brandName & " " & modelName & " " & nameSEO(categoryName) & " - WirelessEmporium is the largest store online for " & brandName & " " & modelName & " " & nameSEO(categoryName) & " & other " & brandName & " " & modelName & " Cell Phone Accessories at the lowest prices."
		SEkeywords = brandName & " " & modelName & " Accessories, " & brandName & " " & modelName & " Cell Phone Accessories, " & brandName & " " & modelName & " " & nameSEO(categoryName) & ", " & brandName & " " & modelName & " Cell Phone " & nameSEO(categoryName)
	end if
	RSmeta.close
	set RSmeta = nothing	
end if

if modelid = "987" or modelid = "988" or modelid = "1009" or modelid = "1015" then
	if SEtitle = "" then SEtitle = "Samsung " & modelName & " " & nameSEO(categoryName) & " - Buy Exclusive Samsung " & modelName & " Accessories"
	if SEdescription = "" then SEdescription = "Buy Quality Samsung " & modelName & " " & nameSEO(categoryName) & " - WirelessEmporium carries your favorite Samsung " & modelName & " " & singularSEO(categoryName) & " at discounted rates and Free Shipping!"
	if SEkeywords = "" then SEkeywords = "samsung " & modelName & " " & nameSEO(categoryName) & ", samsung " & modelName & " " & singularSEO(categoryName) & ", " & modelName & " " & singularSEO(categoryName) & ", samsung accessories, samsung " & modelName & " cell phone " & nameSEO(categoryName) & ", " & nameSEO(categoryName) & " for samsung " & modelName
	if h1 = "" then h1 = "Samsung " & modelName & " " & nameSEO(categoryName)
	select case categoryid
		case "6"
			topText = "Choose from a wide array of our quality Samsung " & modelName & " holsters and belt clips to safely and easily secure your Samsung " & modelName & " phone. We offer Samsung " & modelName & " belt holsters at prices you won't find anywhere else! Protect your Samsung " & modelName & " cell phone with our low priced Samsung " & modelName & " cell phone holsters! They clip easily to belts, purses, pockets, and protect your phone through your active lifestyle. An adjustable belt clip that rotates and locks in six positions makes it easy for you to adjust your phone the way you need it. Our Samsung " & modelName & " cell phone belt clips are stylish, too! Thousands of customers trust us to offer high-quality Samsung " & modelName & " holsters at the lowest possible prices, and we're dedicated to satisfying the needs of all our customers! Browse through our Samsung " & modelName & " cell phone belt clip selection today.</p>"
			topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">Since 2001, Wireless Emporium has been offering wholesale <a class=""topText-link"" href=""http://www.wirelessemporium.com"" title=""Cell Phone Accessories"">cell phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-" & modelid & "-cell-accessories-samsung-" & formatSEO(modelName) & ".asp"" title=""Samsung " & modelName & " Accessories"">Samsung " & modelName & " accessories</a> like holsters & belt clips for your Samsung " & modelName & " phone at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
		case "7"
			topText = "Choose from a wide array of our quality Samsung " & modelName & " leather cases to safely and easily secure your Samsung " & modelName & " phone. We offer prices you won't find anywhere else! Protect your Samsung " & modelName & " cell with our low priced Samsung " & modelName & " cases! They clip easily to belts, purses, pockets, and protect your phone through your active lifestyle. Our Samsung leather cases are stylish, too! Browse through our <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-" & modelid & "-cell-accessories-samsung-" & formatSEO(modelName) & ".asp"" title=""Samsung " & modelName & " Accessories"">Samsung " & modelName & " Accessories</a> selection today. Buy a quality Samsung " & modelName & " cell phone leather case today and we'll ship it to your home or business for free! Wireless Emporium guarantees your satisfaction on all of our quality Samsung leather cases. As the leading name in wholesale <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Cell Phone Accessories"">cell phone accessories</a>, we know a thing or two about Samsung " & modelName & " cases.</p>"
			topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">See for yourself why countless customers trust us to offer the best Samsung " & modelName & " leather cases for Samsung " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day! All of the quality <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-cases.asp"" title=""Cell Phone Cases"">cell phone cases</a> we offer are guaranteed to have passed tough quality tests and our customer service is sure to please. So don't wait! Buy your Samsung " & modelName & " cell phone case from Wireless Emporium today!"
		case "13"
			topText = "WirelessEmporium is the largest store online for Samsung " & modelName & " Data Cable & Memory & other <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-" & modelid & "-cell-accessories-samsung-" & formatSEO(modelName) & ".asp"" title=""Samsung " & modelName & " Accessories"">Samsung " & modelName & " Accessories</a> at the lowest prices.</p>"
			topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">The best in Samsung " & modelName & " Data Cable & Memory at prices up to 80% off retail. Couple this with our WirelessEmporium.com Happy-Shopper 30-day Guarantee and fast FREE SHIPPING, and you realize there's no better place to find your Samsung " & modelName & " Data Cable & Memory!"
		case "17"
			topText = "WirelessEmporium is the largest store online for Samsung " & modelName & " Body Guards/Skins & other Samsung " & modelName & " Cell Phone Accessories at the lowest prices.</p>"
			topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">The best in Samsung " & modelName & " Body Guards/Skins at prices up to 80% off retail. Couple this with our WirelessEmporium.com Happy-Shopper 30-day Guarantee and fast FREE SHIPPING, and you realize there's no better place to find your Samsung " & modelName & " Body Guards/Skins!"
	end select
else
	if categoryid = "1" then
		topText = "Wireless Emporium has everything you need for your " & brandName & " " & modelName & " phone, including a wide selection of <a class=""topText-link"" href=""/sc-1-sb-" & brandID & "-" & brandName & "-batteries.asp"" title=""" & brandName & " Cell Phone Batteries"">batteries for " & brandName & " phones</a>. If you need a spare battery because your current " & brandName & " " & modelName & " phone is burning through battery power too fast, or you need to replace on old battery that isn't holding it's charge anymore, we have a " & brandName & " " & modelName & " <a class=""topText-link"" href=""cell-phone-batteries.asp"" title=""Cell Phone Batteries"">battery</a> that will fit your needs guaranteed."
		topText = topText & "Every " & brandName & " " & modelName & " cell phone battery we sell is quality-tested and guaranteed for flawless performance."
		bottomText = "Don't pay outrageous retail prices for the same " & brandName & " " & modelName & " cell phone batteries you can find here at Wireless Emporium. We provide discount " & brandName & " " & modelName & " battery priced at up to 65% off retail! All cell phone batteries are guaranteed to perform or your money back. Since 2001, Wireless Emporium has been offering <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Discount " & brandName & " " & modelName & " Accessories"">discount cell phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best cell phone batteries for " & brandName & " " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
	elseif categoryid = "2" then
		topText = "Wireless Emporium is the online leader in <a class=""topText-link"" href=""/sc-2-sb-" & brandID & "-" & brandName & "-chargers.asp"" title=""" & brandName & " Cell Phone Batteries"">" & brandName & " cell phone chargers</a>. Because of that, we guarantee to have the widest selection of car chargers and travel chargers for your " & brandName & " " & modelName & " phone. If you are constantly on the go and need to keep your " & brandName & " " & modelName & " powered up or you just need a spare <a class=""topText-link"" href=""/cell-phone-chargers.asp"" title=""Cell Phone Chargers"">cell phone charger</a> for your home or office, Wireless Emporium is the place to be for the essential accessories you need."
		bottomText = "We provide thousands of businesses and consumers discount <a class=""topText-link"" href=""" & modelLink & """ title=""" & brandName & " " & modelName & " Accessory"">" & brandName & " " & modelName & " accessory</a> solutions that give your phone flawless performance time after time at prices up to 50% OFF retail we even ship for FREE! We can help you find the " & brandName & " " & modelName & " charger you need to extend the talk and standby times for your " & brandName & " " & modelName & " mobile phone. All Wireless Emporium " & brandName & " " & modelName & " chargers are guaranteed to perform or your money back. "
		bottomText = bottomText & "Look through our selection of " & brandName & " " & modelName & " chargers today. See for yourself why countless customers trust us to offer the best " & brandName & " " & modelName & " cell phone chargers for " & brandName & " " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
	elseif categoryid = "3" then
		topText = "Go bold or go sleek with your " & brandName & " " & modelName & " by choosing from hundreds of styles of <a class=""topText-link"" href=""http://www.wirelessemporium.com/sc-3-sb-" & brandID & "-" & formatSEO(brandName) & "-" & formatSEO(categoryName) & ".asp"" title=""" & brandName & " " & modelName & " Faceplates"">" & brandName & " cell phone faceplates</a> and screen protectors at Wireless Emporium. These " & brandName & " " & modelName & " faceplates are easy to assemble and our " & brandName & " " & modelName & " screen protectors are as simple as a sticker to install."
		topText = topText & "Because we offer our cell phone faceplates and screen protectors at discount prices, you can buy multiple " & brandName & " " & modelName & " faceplates and change them as often as your mood! Choose the color or design that best suits your personality from our wide selection of " & brandName & " " & modelName & " <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a> and cell phone faceplates below and protect your phone with style! " & brandName & " " & modelName & " faceplates and covers are an essential addition to your mobile device, as it is one of the most effective ways to both protect the look of your phone and enhance it, all while retaining total functionality."
		bottomText = "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">Since 2001, Wireless Emporium has been offering high-quality cell <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Discount Phone Accessories"">discount phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best cell phone faceplates and covers for " & brandName & " " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
	elseif categoryid = "6" then
		topText = "Choose from a wide array of our quality " & brandName & " " & modelName & " cell phone holsters and belt clips to safely and easily secure your " & brandName & " " & modelName & " phone. We offer " & brandName & " " & modelName & " belt holsters at prices you won't find anywhere else! Protect your " & brandName & " " & modelName & " cell phone with our low priced " & brandName & " " & modelName & " holsters! They clip easily to belts, purses, pockets, and protect your phone through your active lifestyle. An adjustable belt clip that rotates and locks in six positions makes it easy for you to adjust your phone the way you need it. Our " & brandName & " " & modelName & " <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-holsters-belt-clips-holders.asp"" title=""" & brandName & " " & modelName & " Cell Phone Belt Clips"">cell phone belt clips</a> are stylish, too! "
		topText = topText & "Thousands of customers trust us to offer high-quality holsters at the lowest possible prices, and we're dedicated to satisfying the needs of all our customers! Browse through our " & brandName & " " & modelName & " cell phone belt clip selection today."
		bottomText = "Since 2001, Wireless Emporium has been offering <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Wholesale " & brandName & " " & modelName & " Cell Phone Accessories"">wholesale cell phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best <a class=""topText-link"" href=""" & modelLink & """ title=""" & brandName & " " & modelName & " Accessories"">" & brandName & " " & modelName & " accessories</a> like holsters & belt clips for your " & brandName & " " & modelName & " phone at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
	elseif categoryid = "7" then
		topText = "Choose from a wide array of our quality " & brandName & " " & modelName & " leather cases to safely and easily secure your " & brandName & " " & modelName & " phone. We offer prices you won't find anywhere else! Protect your " & brandName & " " & modelName & " cell with our low priced " & brandName & " " & modelName & " leather cases! They clip easily to belts, purses, pockets, and protect your phone through your active lifestyle. Our " & brandName & " " & modelName & " leather cases are stylish, too! Browse through our <a class=""topText-link"" href=""" & modelLink & """ title=""" & brandName & " " & modelName & " Accessories"">" & brandName & " " & modelName & " Accessories</a> cases selection today. "
		topText = topText & "Buy a quality " & brandName & " " & modelName & " cell phone leather cases today and we'll ship it to your home or business for free! Wireless Emporium guarantees your satisfaction on all of our quality <a class=""topText-link"" href=""http://www.wirelessemporium.com/sc-3-sb-" & brandID & "-" & formatSEO(brandName) & "-" & formatSEO(categoryName) & ".asp"" title=""" & brandName & " " & modelName & " Leather Case"">" & brandName & " leather cases</a>. As the leading name in <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Wholesale " & brandName & " " & modelName & " Cell Phone Accessories"">wholesale cell phone accessories</a>, we know a thing or two about " & brandName & " " & modelName & " cases."
		bottomText = "See for yourself why countless customers trust us to offer the best " & brandName & " " & modelName & " cases for " & brandName & " " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day! All of the <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-cases.asp"" title=""Quality " & brandName & " " & modelName & " Cases"">quality cell phone cases</a> we offer are guaranteed to have passed tough quality tests and our customer service is sure to please. So don't wait! Buy your " & brandName & " " & modelName & " cell phone case from Wireless Emporium today!"
	end if
end if

if topText = "" then
	topText = "WirelessEmporium is the largest store online for " & brandName & " " & modelName & " Cell Phone " & nameSEO(categoryName) & " & other " & brandName & " " & modelName & " Cell Phone Accessories at the lowest prices."
	topText = topText & "<br /><br />The best in " & brandName & " " & modelName & " " & nameSEO(categoryName) & " at prices up to 80% off retail. Couple this with our WirelessEmporium.com Happy-Shopper 30-day Guarantee and fast FREE SHIPPING, and you realize there's no better place to find your " & brandName & " " & modelName & " " & nameSEO(categoryName) & "!"
end if

dim strBreadcrumb : strBreadcrumb = brandName & " " & modelName & " " & nameSEO(categoryName)

'todo: need to consolidate Metatags with we_XXXText
dim strH1: strH1 = EMPTY_STRING
dim strH2: strH2 = EMPTY_STRING
dim strAltText: strAltText = EMPTY_STRING

'set input values to extract page meta-values
dim dicReplaceAttribute: set dicReplaceAttribute = CreateObject("Scripting.Dictionary")
dicReplaceAttribute( "BrandName") = brandName
dicReplaceAttribute( "ModelName") = modelName
dicReplaceAttribute( "CategoryName") = categoryName

'set known product attributes to replace out template placeholders
dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
dicSeoAttributeInput( "BrandId") = brandID
dicSeoAttributeInput( "ModelId") = modelID
dicSeoAttributeInput( "CategoryId") = parentTypeID
dicSeoAttributeInput( "SubCategoryId") = categoryID

'get content event text (initially installed to load pixels) [knguyen/20110520]
'todo: deploy to other wrappers as needed
dim dicContentEventText: set dicContentEventText = GetContentEventText( dicSeoAttributeInput)

call LookupSeoAttributes()

set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
set dicSeoAttributeInput = nothing

dim strTypeToken : strTypeToken = "Cell Phone"
if isTablet then strTypeToken = "Tablet"

if isTablet then
	modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " " & strTypeToken & " Accessories</a>"	
else
	if brandid = 17 and phoneOnly = 1 then
		brandCrumb 	= "<a class=""breadcrumb"" href=""/iphone-accessories.asp"">Apple iPhone Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " Accessories</a>"	
	elseif brandid 	= 17 and phoneOnly = 0 then
		topText = replace(replace(topText, "apple cell phone", brandName & " " & modelName), strTypeToken, "")
		SEtitle = replace(replace(SEtitle, "apple cell phone", brandName & " " & modelName), strTypeToken, "")
		SEdescription = replace(replace(SEdescription, "apple cell phone", brandName & " " & modelName), strTypeToken, "")
		SEkeywords = replace(replace(SEkeywords, "apple cell phone", brandName & " " & modelName), strTypeToken, "")
	
		brandCrumb 	= "<a class=""breadcrumb"" href=""/ipod-ipad-accessories.asp"">Apple iPod/iPad Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " Accessories</a>"
	else
		brandCrumb 	= "<a class=""breadcrumb"" href=""/" & brandSEO(brandID) & """>" & brandName & " " & strTypeToken & " Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " " & strTypeToken & " Accessories</a>"	
	end if
end if

if parentTypeID = 8 then
	select case categoryid
		case 1350, 1370		'stylers pen, memory cards
			strH1 = nameSEO(categoryName)
			strBreadcrumb = nameSEO(categoryName)
		case else
			strH1 = nameSEO(categoryNameSEO)
			strBreadcrumb = nameSEO(categoryNameSEO)
	end select
elseif categoryid = 1010 then
	strH1 = "Tablet PCs & eReaders"
	strBreadcrumb = "Tablet PCs & eReaders"	
elseif h1 <> "" then
	strH1 = h1
elseif strH1 = "" then
	strH1 = brandName & "&nbsp;" & modelName & "&nbsp;" & nameSEO(categoryName)
end if

session("otherAdminOptions") = ", <a style='color:#ffffff;' href='/admin/db_update_models.asp?ModelID=" & modelID & "&BrandID=" & brandID & "&searchType=Model&submitType=Edit&submitModel=Search'>Edit " & modelName & "</a>"
%>
<!--#include virtual="/includes/template/top.asp"-->
<link href="/includes/css/mvt/bmcd/mvt_leftHeaderImg2.css" rel="stylesheet" type="text/css"> <!-- 2 -->
<link href="/includes/css/mvt/bmcd/mvt_rightHeaderImg2.css" rel="stylesheet" type="text/css"> <!-- 2 -->
<% extraWidgets = "no" %>
<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <%
			if isTablet then
			%>
	            <a class="breadcrumb" href="/tablet-ereader-accessories.asp">Tablet Accessories</a>&nbsp;&gt;&nbsp;<%=modelCrumb%>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
            <%
			else
				if parentTypeID = 15 or parentTypeID = 8 then
				%>
                <a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;
                <a class="breadcrumb" href="/<%=formatSEO(parentCatNameSEO)%>.asp"><%=parentCatName%></a>&nbsp;&gt;&nbsp;
				<%=strBreadcrumb%>
                <%
				else
				%>
				<a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;<%=brandCrumb%>&nbsp;&gt;&nbsp;<%=modelCrumb%>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
                <%
				end if
			end if
			%>
        </td>
    </tr>
    <tr>
        <td width="100%" align="center" valign="top">
            <img src="/images/brandmodelheaders/header_<%=brandID%>.jpg" width="745" height="30" border="0" alt="<%=brandName & " " & modelName%> Accessories">
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" valign="top" style="padding:10px 0px 10px 0px;" title="<%=strH1%>">
        	<table cellspacing="0" cellpadding="0" border="0" width="100%">
            	<tr>
                	<!--
                	<td id="mvt_bmcd_modelpic" width="100px" align="center" valign="top">
	                    <img src="/productpics/models/<%=modelImg%>" width="80" border="0" alt="<%=strH1%>" title="<%=strH1%>">
                    </td>
                    -->
                    <% if parentTypeID = 15 or parentTypeID = 8 then %>
                	<td style="font-weight:bold; font-size:24px; padding-left:20px;" width="*" align="left" valign="top" class="boldText"><%=strH1%></td>
                    <% else %>
                	<td style="padding:0px 0px 0px 20px;" align="left" width="*" valign="center">
                        <div style="width:100%; border-bottom:1px solid #ccc; font-size:24px; font-weight:bold;"><%=brandName & " " & modelName%></div>
                        <div style="width:100%; font-size:26px; font-weight:bold;"><%=categoryName%></div>                    
					</td>
                    <% end if %>
                    <!--
                	<td id="mvt_bmcd_catHeader" width="220px" align="right" valign="top">
	                    <img src="/images/cats/we_header_<%=parentTypeID%>.jpg" border="0" alt="<%=strH1%>" title="<%=strH1%>" />
                    </td>
                    -->
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
        	<div style="background-color:#ebebeb; padding:10px; border:1px solid #ccc; height:20px;">
            	<div style="float:left;"><img src="/images/gray-arrow.jpg" border="0" /></div>
                <div style="float:right;">
                    <div class="graybox-inner" style="display:inline;">Jump to:</div>
                    <form name="frmSearch" method="post" style="display:inline;">
                        <select name="cbItem" onChange="if(this.value != ''){window.location=this.value;}">
                        <%
                        sql	=	"select	distinct v.subtypeid, v.subtypename, v.subTypeOrderNum" & vbcrlf & _
                                "from	we_items a join v_subtypematrix v" & vbcrlf & _
                                "	on	a.subtypeid = v.subtypeid" & vbcrlf & _
                                "where	v.typeid = '" & parentTypeID & "'" & vbcrlf & _
                                "	and a.price_our > 0 and a.hidelive = 0 and a.ghost = 0" & vbcrlf
                        if parentTypeID <> 5 and parentTypeID <> 8 and parentTypeID <> 15 then
                            sql = sql &	"	and	a.modelid = '" & modelid & "'" & vbcrlf
                        end if
                        sql = sql & "union" & vbcrlf & _
                                    "select	distinct v.subtypeid, v.subtypename, v.subTypeOrderNum" & vbcrlf & _
                                    "from	we_items a join v_subtypematrix v" & vbcrlf & _
                                    "	on	a.subtypeid = v.subtypeid join we_subRelatedItems s" & vbcrlf & _
                                    "	on	a.itemid = s.itemid" & vbcrlf & _
                                    "where	v.typeid = '" & parentTypeID & "'" & vbcrlf & _
                                    "	and a.price_our > 0 and a.hidelive = 0 and a.ghost = 0" & vbcrlf
                        if parentTypeID <> 5 and parentTypeID <> 8 and parentTypeID <> 15 then
                            sql = sql &	"	and	s.modelid = '" & modelid & "'" & vbcrlf
                        end if
                        if parentTypeID = 18 then
                            sql = sql & "union" & vbcrlf & _
                                        "select	distinct subtypeid, subtypename, subTypeOrderNum" & vbcrlf & _
                                        "from	v_subtypematrix where subtypeid = '1270'" & vbcrlf
                        end if								
                        sql = sql &	"order by v.subTypeOrderNum" & vbcrlf
                        session("errorSQL") = sql
                        arrJumpTo = getDbRows(sql)
                        if not isnull(arrJumpTo) then
                            for nRows=0 to ubound(arrJumpTo,2)
                                strText = replace(replace(replace(replace(arrJumpTo(1,nRows), "-Covers", ""), " Hard Covers", ""), " Covers", ""), " Cases", "")
                                if cstr(categoryid) = cstr(arrJumpTo(0,nRows)) then
                                %>
                                <option value="/sb-<%=brandid%>-sm-<%=modelid%>-scd-<%=arrJumpTo(0,nRows)%>-<%=formatSEO(arrJumpTo(1,nRows))%>-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>.asp" title="<%=arrJumpTo(1,nRows)%>" selected="selected"><%=strText%></option>
                                <%
                                else
                                %>
                                <option value="/sb-<%=brandid%>-sm-<%=modelid%>-scd-<%=arrJumpTo(0,nRows)%>-<%=formatSEO(arrJumpTo(1,nRows))%>-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>.asp" title="<%=arrJumpTo(1,nRows)%>"><%=strText%></option>
                                <%
                                end if
                            next
                        end if
                        %>
                        </select>
                    </form>
                </div>
            </div>
        </td>
    </tr>
	<tr>
    	<td style="font-size:11px; color:#494949; border-bottom:1px solid #ccc; padding:3px;" align="left">
			<%
			if prepStr(SeTopText) <> "" then subCatTopText = SeTopText
			%>
			<%=subCatTopText%>
        </td>
    </tr>
	<tr><td style="font-size:1px;border-bottom:1px solid #ccc;" align="left">&nbsp;</td></tr>
<%
if strSort = "pop" then usePopImg = "button-most-pop2.jpg" else usePopImg = "button-most-pop1.jpg"
if strSort = "new" then useNewImg = "button-new2.jpg" else useNewImg = "button-new1.jpg"
if strSort = "low" then useLowImg = "button-lowest2.jpg" else useLowImg = "button-lowest1.jpg"
if strSort = "high" then useHighImg = "button-highest2.jpg" else useHighImg = "button-highest1.jpg"
%>
    <tr>
    	<td width="100%" style="padding:10px 0px 10px 0px;">
        	<div style="float:left; color:#494949; font-weight:bold; padding:4px 0px 0px 10px;">Sort By:</div>
			<div style="float:left; padding-left:20px;">
                <form name="frmSort" method="post" action="javascript:void(0);">
                    <input type="image" style="margin-right:3px;" value="Sort By Most Popular" src="/images/<%=usePopImg%>" onmouseover="this.src='/images/button-most-pop2.jpg'" onmouseout="this.src='/images/button-most-pop1.jpg'" onclick="return fnSort('pop');" border="0" />
                    <input type="image" style="margin-right:3px;" value="Sort By Newest" src="/images/<%=useNewImg%>" onmouseover="this.src='/images/button-new2.jpg'" onmouseout="this.src='/images/button-new1.jpg'" onclick="return fnSort('new');" border="0" />
                    <input type="image" style="margin-right:3px;" value="Sort By Lowest Price" src="/images/<%=useLowImg%>" onmouseover="this.src='/images/button-lowest2.jpg'" onmouseout="this.src='/images/button-lowest1.jpg'" onclick="return fnSort('low');" border="0" />
                    <input type="image" style="margin-right:3px;" value="Sort By Highest Price" src="/images/<%=useHighImg%>" onmouseover="this.src='/images/button-highest2.jpg'" onmouseout="this.src='/images/button-highest1.jpg'" onclick="return fnSort('high');" border="0" />
                </form>
			</div>
            <div style="float:right;" id="upperPagination"></div>
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" valign="top">
            <div style="border-bottom:1px solid #CCC; border-top:1px solid #CCC; padding:5px 0px 5px 0px; float:left; width:100%;">
				<%
                if musicSkins = 1 then
                    musicSkinsLink = "ms-"
                    musicSkinsLinkTail = "-music-skins-for-" & formatSEO(brandName) & "-" & formatSEO(modelName)
                    genreArray = split(session("curGenres"),",")
					
					sql = "select distinct genre from we_items_musicskins where modelID = " & modelID & " and deleteItem = 0 and skip = 0 order by genre"
                    session("errorSQL") = sql
                    Set objRsGenre = oConn.execute(sql)
					
					genreList = ""
					do while not objRsGenre.EOF
						genreList = genreList & objRsGenre("genre") & ","
						objRsGenre.movenext
					loop
					genreArray = split(genreList,",")
					genreList = ""
					for i = 0 to ubound(genreArray)
						if prepStr(genreArray(i)) <> "" then
							if instr(genreList,prepStr(genreArray(i))) < 1 then genreList = genreList & prepStr(genreArray(i)) & ","
						end if
					next
                    
                    dim arrMusicArtist, objRsArtist, sqlArtist
                    sqlArtist = " select distinct id, artist from we_items_musicskins_artist where genre like '%" & musicSkinGenre & "%' order by 2"
                    session("errorSQL") = sqlArtist
                    Set objRsArtist = oConn.execute(sqlArtist)
                %>
				<form name="frmMusicSkin">
                <div style="float:left; font-weight:bold; font-size:12px; padding-right:10px; padding-top:3px;">Select Music Skins Genre:</div>
                <div style="float:left; padding-right:20px; border-right:1px dotted #CCC; margin-right:20px;">
                	<select name="musicGenre" style="width:120px;" onchange="bmc('g',this.value)">
                        <option value="">Select Genre</option>
                        <%
						genreArray = split(genreList,",")
						
						for i = 0 to ubound(genreArray)
							if prepStr(genreArray(i)) <> "" then
						%>
                        <option value="<%=genreArray(i)%>"><%=genreArray(i)%></option>
                        <%
							end if
						next
						%>
                    </select>
                </div>
                <%
					if not objRsArtist.eof then
				%>
                <div style="float:left; font-weight:bold; font-size:12px; padding-right:10px; padding-top:3px;">Select Music Skins Artist:</div>
                <div style="float:left;">
                	<select name="musicArtist" onchange="bmc('a',this.value)">
                        <option value="" style="width:130px;">Select Artist</option>
                        <% 
                        do until objRsArtist.eof 
                        %>
                        <option value="<%=objRsArtist("id") & "-" & formatSEO(objRsArtist("artist"))%>" <%if cint(objRsArtist("id")) = cint(musicSkinArtistID) then %>selected<% end if%>><%=objRsArtist("artist")%></option>
                        <% 
                            objRsArtist.movenext
                        loop 
                        %>
                        <option value="0-all" <%if 0 = cint(musicSkinArtistID) then %>selected<% end if%>>Show All</option>
                    </select>
                </div>
                <%
					end if
					set objRsArtist = nothing
				%>
                </form>
				<%
                else
                    musicSkinsLink = ""
                    musicSkinsLinkTail = ""
                end if
				%>
            </div>
        </td>
	</tr>
    <tr>
    	<td id="allProducts">
        	<div id="noProducts" class="alertBoxHidden">
                No products match your current filter settings<br />
                Please try to adjust you filter to see more products
            </div>
			<%
            if categoryid = 7 and (design = "" or not isNumeric(design)) then 'cannot find example that executes this condition since design always defaults to 0 [knguyen/20110602]
                dim boxHeader(6), imgSrc(6)
                if excludePouches = false then
                    boxHeader(1) = "MLB OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                    boxHeader(2) = "NCAA OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                    boxHeader(3) = "DISNEY OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                    boxHeader(4) = "BLING UNIVERSAL CELL PHONE POUCHES"
                    imgSrc(1) = "LC-MLB-thumb.jpg"
                    imgSrc(2) = "LC-NCAA-Thumb.jpg"
                    imgSrc(3) = "LC-Disney-Thumb.jpg"
                    imgSrc(4) = "LC-Blingpouch-Thumb.jpg"
                    startCount = 1
                    endCount = 4
                elseif includeNFL = true then
                    boxHeader(5) = "NFL OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                    imgSrc(5) = "LC-NFL-Thumb.jpg"
                    startCount = 5
                    endCount = 6
                    boxHeader(6) = "NBA OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                    imgSrc(6) = "LC-NBA-Thumb.jpg"
                end if
                if startCount > 0 and endCount > 0 then
                    for aCount = startCount to endCount
                        SQL = "SELECT TOP 1 price_Retail, price_Our FROM we_items WHERE hidelive=0 AND inv_qty > 0 AND Sports = '" & aCount & "'"
                        session("errorSQL") = SQL	
                        'response.write "<pre>" & sql & "</pre>"
                        set	RSextra = oConn.execute(SQL)
                        tempCount = 0
                        if not RSextra.eof then
                            tempCount = tempCount + 1
                            altText = boxHeader(aCount)
            %>
            <div style="width:177px; padding-bottom:5px;">
                <div style="float:left;"><a href="/pouches-<%=aCount & "-" & formatSEO(boxHeader(aCount))%>.asp"><img src="/productpics/thumb/<%=imgSrc(aCount)%>" border="0" alt="<%=altText%>"></a></div>
            </div>
            <div style="width:177px; padding-bottom:5px;">
                <div style="float:left;"><a class="product-description5" href="/pouches-<%=aCount & "-" & formatSEO(boxHeader(aCount))%>.asp" title="<%=altText%>"><%=boxHeader(aCount)%></a></div>
            </div>
            <div style="width:177px;">
                <div style="float:left;">
                    <span class="boldText">Our&nbsp;Price:&nbsp;</span>
                    <span class="pricing-orange2"><%=formatCurrency(RSextra("price_Our"))%>&nbsp;</span><br />
                    <span class="pricing-gray2">List Price: <s><%=formatCurrency(RSextra("price_Retail"))%></s></span><br />
                    <a href="/pouches-<%=aCount & "-" & formatSEO(boxHeader(aCount))%>.asp" class="product-description3"><img src="/images/link_arrow.gif" border="0" align="abs-middle"><u><i>Click Here For Our Complete Lineup</i></u></a>
                </div>
            </div>
            <%
                        end if
                        RSextra.close
                        set RSextra = nothing
                    next
                end if
            elseif categoryid = 2 then	'chargers, i.e. http://www.wirelessemporium.com/sb-20-sm-653-sc-2-chargers-htc-t-mobile-g1-google-phone.asp [knguyen/20110602]
                if not isNull(includeExtraItem) and includeExtraItem > 0 then
                    SQL = 	"	SELECT 	i.itemID, i.itemDesc, i.itemPic, i.price_Retail, i.price_Our, isnull(i.inv_qty, 0) as inv_qty, isnull( tv.[ActiveItemValueType], 'OriginalPrice') as [ActiveItemValueType] " & vbcrlf & _
                            "	FROM 	we_items i with(nolock)  left outer join v_itemOnSale tv" & vbcrlf & _
                            "		on	tv.itemid = i.itemid" & vbcrlf & _
                            "	where	i.itemid = '" & includeExtraItem & "'"

                    session("errorSQL") = SQL
                    set RSextra = oConn.execute(SQL)
                    do until RSextra.eof
                        altText = singularSEO(categoryName) & " for " & brandName & " " & modelName & " - " & RSextra("itemDesc")
                        if len(RSextra("itemDesc")) >= 85 then
                            itemDescEtc = left(RSextra("itemDesc"),80) & "..."
                        else
                            itemDescEtc = RSextra("itemDesc")
                        end if
                        
                        call ImageHtmlTagWrapper( _
                            "<img src=""/productpics/thumb/" & RSextra("itemPic") & """ border=""0"" alt=""" & altText & """ />",  _
                            "/p-" & RSextra("itemid") & "-" & formatSEO(RSextra("itemDesc")) & ".asp", _
                            (RSextra("ActiveItemValueType")<>"OriginalPrice"))
            %>
            <div style="width:177px; padding-bottom:5px;">
                <div style="float:left;"><a class="product-description5" href="/p-<%=RSextra("itemid") & "-" & formatSEO(RSextra("itemDesc"))%>.asp" title="<%=altText%>"><%=itemDescEtc%></a></div>
            </div>
            <div style="width:177px; padding-bottom:5px;">
                <div style="float:left;">
                    <span class="boldText">Our Price: </span>
                    <span class="pricing-orange2"><%=formatCurrency(RSextra("price_Our"))%></span><br />
                    <span class="pricing-gray2">List Price: <s><%=formatCurrency(RSextra("price_Retail"))%></s></span>
                </div>
            </div>
            <%
                        RSextra.movenext
                    loop
                    RSextra.close
                    set RSextra = nothing
                end if
            end if
			%>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
            	<tr>
            <%
            dim DoNotDisplay, RSkit
            noImageCount = 0
            curID = 0
            visCnt = 0
            useClass = "bmc_productBox"
			a = 0
            if noProducts = 0 then
                do until RS.eof '--mainstream bmc listings
					DoNotDisplay = 0
					curInvQty = prepInt(RS("inv_qty"))
					curAlwaysInStock = RS("alwaysInStock")
					if isNull(curAlwaysInStock) then curAlwaysInStock = false
                    if curInvQty = 0 and not curAlwaysInStock then
                        DoNotDisplay = 1
                        'if RS("reviews") > 1 then
                            'DoNotDisplay = 0
                        'end if
                    end if
'					response.write "RS(""alwaysInStock""):" & RS("alwaysInStock") & "<br>"
'					response.write "DoNotDisplay:" & DoNotDisplay
                    curItemID = rs("itemID")
                    partnumber = rs("partnumber")
                    if left(partnumber,3) = "WCD" then DoNotDisplay = 1
                    isRelated = rs("related")
                    if not isNull(RS("ItemKit_NEW")) then
                        sql = 	"	select	(select top 1 inv_qty from we_items where partNumber = a.partNumber order by master desc, inv_qty desc) as inv_qty " & vbcrlf & _
                                "	from	we_items a " & vbcrlf & _
                                "	where	a.itemid in ("& RS("ItemKit_NEW") & ")" & vbcrlf
                        session("errorSQL") = SQL
                        set RSkit = oConn.execute(SQL)
                        do until RSkit.eof
                            if RSkit("inv_qty") < 1 then DoNotDisplay = 1
                            RSkit.movenext
                        loop
                        RSkit.close
                        set RSkit = nothing
                    end if

                    itemPic = RS("itemPic")
                    if musicSkins = 1 then
                        itemImgPath = Server.MapPath("/productpics/musicSkins/musicSkinsLarge") & "\" & RS("itemPic")
                        itemImgPath2 = Server.MapPath("/productpics/musicSkins/musicSkinsSmall") & "\" & RS("itemPic")
                        useImgPath = "/productpics/musicSkins/musicSkinsSmall/" & RS("itemPic")
                    else
                        itemImgPath = Server.MapPath("/productpics/thumb") & "\" & RS("itemPic")
                        itemImgPath2 = Server.MapPath("/productpics/thumb") & "\" & RS("itemPic")
                        useImgPath = "/productpics/thumb/" & RS("itemPic")
                    end if
                    
                    if not fsThumb.FileExists(itemImgPath) then
                        noImageCount = noImageCount + 1
                        if musicSkins = 1 then
                            if isnull(rs("defaultImg")) then
                                itemPic = "imagena.jpg"
                                useImgPath = "/productpics/thumb/imagena.jpg"
                            else
                                skipRest = 0
                                if fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & rs("defaultImg"))) then
                                    setDefault = rs("defaultImg")
                                elseif fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & replace(rs("defaultImg")," ","-"))) then
                                    setDefault = replace(rs("defaultImg")," ","-")
                                else
                                    setDefault = rs("defaultImg")
                                end if
                                if skipRest = 0 then
                                    chkPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & setDefault)
                                    if not fsThumb.FileExists(chkPath) then
                                        musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)
                                        if fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)) then
                                            musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)
                                        elseif fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & replace(setDefault," ","-"))) then
                                            musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & replace(setDefault," ","-"))
                                        else
                                            itemPic = "imagena.jpg"
                                            useImgPath = "/productpics/thumb/imagena.jpg"
                                            skipRest = 1
                                        end if
                                        if skipRest = 0 then
                                            session("errorSQL") = musicSkinsDefaultPath
                                            Jpeg.Open musicSkinsDefaultPath
                                            Jpeg.Width = 100
                                            Jpeg.Height = Jpeg.OriginalHeight * 100 / Jpeg.OriginalWidth
                                            Jpeg.Save chkPath
                                        end if
                                    end if
                                    if skipRest = 0 then
                                        itemPic = setDefault
                                        useImgPath = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & setDefault
                                    end if
                                end if
                            end if
                        else
                            DoNotDisplay = 1
                            itemPic = "imagena.jpg"
                            useImgPath = "/productpics/thumb/imagena.jpg"
                        end if
                    elseif musicSkins = 1 and not fsThumb.FileExists(itemImgPath2) then
                        Jpeg.Open itemImgPath
                        if Jpeg.OriginalHeight > Jpeg.OriginalWidth then
                            Jpeg.Height = 100
                            Jpeg.Width = Jpeg.OriginalWidth * 100 / Jpeg.OriginalHeight
                        else
                            Jpeg.Width = 100
                            Jpeg.Height = Jpeg.OriginalHeight * 100 / Jpeg.OriginalWidth
                        end if
                        Jpeg.Save itemImgPath2
                    end if
                    
                    if DoNotDisplay = 0 then
						a = a + 1
                        altText = brandName & " " & modelName & " " & singularSEO(categoryName) & " - " & RS("itemDesc")
                        if categoryid = "14" and modelid = "0" then altText = "Cell Phone Charms & Bling : " & altText
                        if len(RS("itemDesc")) >= 73 then
                            itemDescEtc = left(RS("itemDesc"),70) & "..."
                        else
                            itemDescEtc = RS("itemDesc")
                        end if
                        itemDescEtc = replace(itemDescEtc, "/", " / ")
                        
                        productLink = "/p-" & musicSkinsLink & RS("itemid") &"-"& formatSEO(RS("itemDesc")) & musicSkinsLinkTail &".asp"
                        colorSlaves = rs("colorSlaves")
                        colorid = rs("colorid")
                        numAvailColors = cint(rs("numAvailColors"))
                        
                        picLap = picLap + 1
                        onSale = RS("ActiveItemValueType") <> "OriginalPrice"
                        'response.write "ActiveItemValueType: " & RS("ActiveItemValueType")
                        'response.write "onSale: " & onSale
                        customize = rs("customize")
						allNames = allNames & formatSEO(itemDescEtc) & ","
						allPrices = allPrices + prepInt(RS("price_Our"))
						
						availColors = rs("availColors")
						
						if (picLap mod 4) = 1 then 
							a = 0
							response.write "</tr><tr>"
						end if
                    %>
                    <td id="itemListID_<%=picLap%>" class="<%=useClass%>" valign="top">
                    <!--<div id="itemListID_<%=picLap%>" class="<%=useClass%>">-->
                        <div class="bmc_productPic" id="picDiv_<%=picLap%>" title="<%=altText%>">
                        	<div id="id_productImage_<%=curItemID%>" style="position:relative;">
								<% if onSale then %><div onclick="window.location='<%=productLink%>'" class="product-bmc-onSale clickable"></div><% end if %>
                                <img src="<%=useImgPath%>" border="0" width="100" height="100" title="<%=altText%>" onclick="window.location='<%=productLink%>'" class="clickable">
                                <%
                                if not isnull(session("adminID")) and len(session("adminID")) > 0 and session("adminNav") then
                                    response.write "<br /><span style=""color:red; font-size:10px;"">" & partnumber & "</span><br /><span style=""font-size:10px;"">" & curItemID & "</span>"
                                    if isRelated = 1 then response.write " (<span style=""color:blue; font-size:10px;"">Related</span>)"
                                    if RS("alwaysInStock") then response.Write("<span style=""color:blue; font-size:10px;"">(Always In Stock)</span>")
                                end if
                                %>
                                <% if customize then %>
                                <div style="position:absolute; right:0px; top:0px; cursor:pointer;" onclick="window.location='<%=productLink%>'"><img src="/images/icons/customize-sm.png" border="0" /></div>
                                <% end if %>
                            </div>
                        </div>
                        <% if RS("inv_qty") = 0 and not RS("alwaysInStock") then %>
                        <div class="product-bmc-soldOut" align="center">
                            <img src="/images/sold_out.png" width="85">
                        </div>
                        <% else %>
                        <div class="product-bmc-details" align="center">
                            <div id="id_productLink_<%=curItemID%>" class="product-bmc-desc">
                                <a class="cellphone-link" href="<%=productLink%>" title="<%=altText%>" alt="<%=altText%>"><%=itemDescEtc%></a>
                            </div>
                        	<% if numAvailColors > 1 then %>
                            <div style="width:100%; padding-top:5px; height:32px; clear:both; overflow:hidden;">
                                <div align="center" style="font-weight:bold;">Available Colors:</div>
                                <div align="center"><%=generateColorPicker(arrColors, curItemID, availColors, colorid, numAvailColors, onSale)%></div>
                                <div style="display:none;" id="preloading_deck_<%=curItemID%>"></div>                            
                            </div>
                        	<% end if %>                                                    
                        </div>
                        <div style="float:left; width:100%; padding-top:5px;" align="center">
                            <span class="boldText">Our&nbsp;Price:&nbsp;</span>
                            <span class="pricing-orange2"><%=formatCurrency(RS("price_Our"))%></span><br>
                            <span class="pricing-gray2">List&nbsp;Price:&nbsp;<s><%=formatCurrency(prepInt(RS("price_Retail")))%></s></span>
                        </div>
                        <div id="mvt_bmcd_fresshipping" style="float:left; width:100%; padding:5px 0px 5px 0px;" align="center">
                            <img src="/images/bmc/we-freeshipping1.jpg" border="0" />
                        </div>
                        <% end if %>
                    <!--</div>-->
                    </td>
                    <%
						'if visCnt = productsPerPage then useClass = "bmc_productBox2"
					end if
					
					if musicSkins = 1 then
						itemDesc = rs("itemDesc")
						do while itemDesc = rs("itemDesc") or (isnull(itemDesc) and isnull(rs("itemDesc")))
							RS.movenext
							if rs.EOF then exit do
						loop
					else
						do while curItemID = rs("itemID")
							RS.movenext
							if rs.EOF then exit do
						loop
					end if
                loop
				if a = 3 then
					response.write "<td>&nbsp;</td>"
				elseif a = 2 then
					response.write "<td>&nbsp;</td><td>&nbsp;</td>"
				elseif a = 1 or a = 0 then
					response.write "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>"
				end if
            end if
			%>
	            </tr>
			</table>
            <%
            if totalPages = 0 then totalPages = 1
			if curPageNum = 0 then curPageNum = 1
			hiddenCount = picLap - 40
            hiddenPages = round(hiddenCount / productsPerPage)
            if (hiddenCount / productsPerPage) > round(hiddenCount / productsPerPage) then hiddenPages = hiddenPages + 1
            totalPages = curPageNum + hiddenPages
            if instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"?") > 0 then
                usePageURL = left(request.ServerVariables("HTTP_X_REWRITE_URL"),instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"?")-1)
            else
                usePageURL = request.ServerVariables("HTTP_X_REWRITE_URL")
            end if
            if picLap = 0 then	'no products available
                if categoryid = 1010 then
            %>
            <div>
                No tablets currently available. 
                <br /><a href="/tablet-ereader-accessories.asp" style="text-decoration:underline;" title="Tablets EReaders Accessories">Click HERE</a> to view tablet and e-reader accessories.
            </div>
                <% else %>
            <div style="font-weight:bold; font-size:18px; color:#F00; padding:20px 0px 20px 0px;">
                No Products Currently Available
            </div>
                <%
                end if
            end if
            %>
            <div style="clear:both;"></div>
            <div style="float:left; width:500px;">
                <%
                if not isnull(arrJumpTo) then
                %>
                <img src="/images/viewother.jpg" border="0" style="float:left; margin-right:5px;" />
                <form name="frmSearch2" method="post" style="float:left; padding-top:6px;">
                    <select name="cbItem" style="font-size:12px;" onChange="if(this.value != ''){window.location=this.value;}">
                    <%
                    for nRows=0 to ubound(arrJumpTo,2)
                        strText = replace(replace(replace(replace(arrJumpTo(1,nRows), "-Covers", ""), " Hard Covers", ""), " Covers", ""), " Cases", "")
                        if cstr(categoryid) = cstr(arrJumpTo(0,nRows)) then
                        %>
                        <option value="/sb-<%=brandid%>-sm-<%=modelid%>-scd-<%=arrJumpTo(0,nRows)%>-<%=formatSEO(arrJumpTo(1,nRows))%>-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>.asp" title="<%=arrJumpTo(1,nRows)%>" selected="selected"><%=strText%></option>
                        <%
                        else
                        %>
                        <option value="/sb-<%=brandid%>-sm-<%=modelid%>-scd-<%=arrJumpTo(0,nRows)%>-<%=formatSEO(arrJumpTo(1,nRows))%>-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>.asp" title="<%=arrJumpTo(1,nRows)%>"><%=strText%></option>
                        <%
                        end if
                    next
                    %>
                    </select>
                </form>                            
                <%
                end if
                %>
            </div>
            <div id="lowerPagination" style="float:right;">
                <%
				if totalPages > 1 then
				%>
                <div style="float:right;"><a class="inactivePageNum" onclick="showPage(0)">Show All Products</a></div>
                <% end if %>
                <div style="float:right;">
                    <div style="padding:2px 5px 0px 5px; float:left; font-size:11px;">View Page:</div>
                    <%
                    for x = 1 to totalPages
                        if x = cdbl(curPageNum) then
                            pageNumClass = "activePageNum"
                        else
                            pageNumClass = "inactivePageNum"
                        end if
                    %>
                    <div id="pageLinkBox_<%=x%>" class="paginationLink1"><a id="pageLink_<%=x%>" class="<%=pageNumClass%>" onclick="showPage(<%=x%>)"><%=x%></a></div>
                    <%
                    next
                    %>
                </div>
            </div>
        </td>
    </tr>
    <%
    sql	=	"select	distinct a.modelid, a.modelname, a.modelimg, a.temp, c.brandid, c.brandname" & vbcrlf & _
            "	,	a.isTablet, v.typeid, v.typeName, v.orderNum" & vbcrlf & _
            "	,	'sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}.asp' urlStructure	" & vbcrlf & _
            "from	we_models a join we_brands c " & vbcrlf & _
            "	on	a.brandid = c.brandid join we_items b " & vbcrlf & _
            "	on	a.modelid = b.modelid join we_types d " & vbcrlf & _
            "	on	b.typeid = d.typeid join v_subtypeMatrix v" & vbcrlf & _
            "	on	b.subtypeid = v.subtypeid" & vbcrlf & _
            "where	a.modelid = '" & modelid & "' " & vbcrlf & _
            "	and d.typeid not in (5) and b.hidelive = 0 and b.inv_qty <> 0 and b.price_our > 0 " & vbcrlf & _
            "union" & vbcrlf & _
            "select	distinct a.modelid, a.modelname, a.modelimg, a.temp, c.brandid, c.brandname" & vbcrlf & _
            "	,	a.isTablet, v.typeID, v.typeName, v.orderNum	" & vbcrlf & _
            "	,	'sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}.asp' urlStructure	" & vbcrlf & _
            "from	we_models a join we_brands c" & vbcrlf & _
            "	on	a.brandid = c.brandid join v_typeMatrix v" & vbcrlf & _
            "	on	v.typeID in (5, 8)" & vbcrlf & _
            "where	a.modelid = '" & modelid & "'" & vbcrlf & _
            "order by v.orderNum desc" & vbcrlf
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	a = 0

	if not RS.eof then
	%>
    <!-- other accessories start -->
    <tr>
    	<td style="padding-top:50px;">
            <div style="background-color:#ebebeb; padding:10px; border:1px solid #ccc;">
                <div style="font-size:15px; font-weight:bold; color:#494949;" title="OTHER <%=ucase(brandName) & " " & ucase(modelName)%> ACCESSORIES" align="center">
                	<h3 class="brandModel">OTHER <%=ucase(brandName) & " " & ucase(modelName)%> ACCESSORIES</h3>
				</div>
            </div>
        </td>
    </tr>
    <tr>
    	<td>
            <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                <tr>
					<%
                    brandName = replace(brandName, "/", " / ")
                    modelName = replace(modelName, "/", " / ")					
                    do until RS.eof
                        thisType = replace(RS("typeName"), "/", " / ")
                        'sb-{brandid}-sm-{modelid}-scd-{typeid}-{typename}-{brandname}-{modelname}.asp
                        strLink = replace(replace(replace(replace(replace(replace(rs("urlStructure"), "{brandid}", brandid), "{modelid}", modelid), "{typeid}", rs("typeid")), "{brandname}", formatSEO(brandName)), "{modelname}", formatSEO(modelName)), "{typename}", formatSEO(thisType))
                        altText = modelName & " " & thisType
					%>
                    <td align="center" valign="top" width="165" style="padding:15px 5px 30px 5px;">
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr><td align="center"><a href="<%=strLink%>" title="<%=altText%>"><img src="/images/categories/we_cat_<%=rs("typeid")%>.jpg" width="165" border="0" alt="<%=altText%>" title="<%=altText%>" /></a></td></tr>
                            <tr><td align="center" style="padding-top:10px;"><a href="<%=strLink%>" title="<%=altText%>" class="cellphone3-link"><%=altText%></a></td></tr>                            
                        </table>
                    </td>
					<%
                        RS.movenext
                        a = a + 1
                        if a = 4 then
                            response.write "</tr><tr>" & vbcrlf
                            a = 0
                        end if
                    loop    
                    %>
                </tr>
            </table>
        </td>
    </tr>
    <!-- other accessories end -->
	<%
	end if
	%>
	<tr><td style="padding-top:20px;"><h1 class="brandModelCat2"><%=strH1%></h1></td></tr>    
    <tr><td align="center" class="topText"><%=topText%></td></tr>
    <tr><td align="center" class="bottomText"><%=bottomText%></td></tr>    
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<script language="javascript">var isMasterPage = 1</script>
<!--#include virtual="/includes/template/bottom.asp"-->
<%
	useURL = request.ServerVariables("HTTP_X_REWRITE_URL")
	if instr(useURL,"?") > 0 then useUrl = left(useURL,instr(useURL,"?")-1)
%>
<script language="javascript">
	window.onload = function() {
		if(typeof dynamicLoad == 'function'){
			dynamicLoad();
		}
	}
	var useFilter = ""
	var useFilter2 = ""
	var curPage = 1
	var totalPages = <%=prepInt(totalPages)%>
	var totalItems = <%=prepInt(picLap)%>
	<% if picLap > 40 then %>
	var numToShow = 40
	<% else %>
	var numToShow = <%=prepInt(picLap)%>
	<% end if %>
	var perPage = 40
	if (totalPages == 0) { totalPages = 1 }
</script>
<script language="javascript" src="/includes/js/filterProducts.js"></script>
<% if musicSkins = 1 then %>
<script language="javascript">
	function bmc(type, val) {
		var useVal = val.replace(" ","-");
		
		if ("" != useVal)
		{
			if ("a" == type) window.location = '/sb-<%=brandID%>-sm-<%=modelID%>-scd-<%=categoryID%>-music-skins-<%=formatSEO(musicSkinGenre)%>-artist-' + useVal + '.asp';
			else if("g" == type) window.location = '/sb-<%=brandID%>-sm-<%=modelID%>-scd-<%=categoryID%>-music-skins-' + useVal + '.asp';		
		}
	}
</script>
<% end if %>
<script>
	function preloading(pItemID, selectedColorID)
	{
		ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmcd&uType=pre&itemid=' + pItemID + '&selectedColorID=' + selectedColorID, 'preloading_deck_' + pItemID);		
	}
	
	function changeProduct(pItemID, selectedColorID, numColors, selectedIDX, onSale)
	{
		curObj = null;
		for(i=0; i < numColors; i++) 
		{
			curObj = 'div_colorOuterBox_' + pItemID + '_' + i;
			if (document.getElementById(curObj) != null) document.getElementById(curObj).className = 'colorBoxUnselected';
		}
		curObj = 'div_colorOuterBox_' + pItemID + '_' + selectedIDX;
		document.getElementById(curObj).className = 'colorBoxSelected';

		ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmcd&uType=img&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&onsale=' + onSale + '&adminOption=<%=prepInt(session("adminID"))%>', 'id_productImage_'+pItemID);
		ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmcd&uType=link&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&adminOption=<%=prepInt(session("adminID"))%>', 'id_productLink_'+pItemID);
		ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmcd&uType=price&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&adminOption=<%=prepInt(session("adminID"))%>', 'id_productPricing_'+pItemID);
	}
	
	function fnSort(sortOption)
	{
		var useURL = window.location + ''
		if (useURL.indexOf("?") > 0) {
			useURL = useURL.substring(0,useURL.indexOf("?"))
		}
		window.location = useURL + '?strSort=' + sortOption
		return false;
	}
</script>