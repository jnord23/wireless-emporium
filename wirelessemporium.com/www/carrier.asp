<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
response.buffer = true
dim carrierid
carrierid = request.querystring("carrierid")
if carrierid = "" or not isNumeric(carrierid) then
	call PermanentRedirect("/")
end if

call fOpenConn()
dim RS3, RS4
dim carrierName, carrierCode, carrierImg
SQL = "SELECT * FROM we_Carriers WHERE id = '" & carrierid & "'"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
else
	carrierName = RS("carrierName")
	carrierCode = RS("carrierCode")
	carrierImg = RS("carrierImg")
end if

SQL = "SELECT DISTINCT brandID, brandName, brandImg, brandCode FROM"
SQL = SQL & " (SELECT A.brandID, A.carrierCode, C.brandName, C.brandImg, C.brandCode, C.listOrder"
SQL = SQL & " FROM we_Models A INNER JOIN we_Brands C ON A.brandID=C.brandID WHERE a.isTablet = 0 and A.carrierCode LIKE '%" & carrierCode & "%') AS derivedtbl_1"
SQL = SQL & " ORDER BY brandName"
'session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
end if

dim SEtitle, SEdescription, SEkeywords, topText
dim ProviderID, bgcolor, h1color, bordercolor, spacerHeight, spacerWidth, link1, link2, link3, bannerText

sql	=	"select	seTitle, seDescription, seKeywords, bannerText, spacerHeight, spacerWidth, banner_bordercolor, banner_bgcolor, h1color, h1Tag, h2Tag, breadCrumb" & vbcrlf & _
		"from	we_carrierText" & vbcrlf & _
		"where	carrierCode = '" & carrierCode & "'" & vbcrlf
'response.write "<pre>" & sql & "</pre>"		
session("errorSQL") = sql		
set objRsSEO = Server.CreateObject("ADODB.Recordset")
objRsSEO.open sql, oConn, 0, 1

if not objRsSEO.eof then
'	SEtitle			=	objRsSEO("seTitle")
'	SEdescription	=	objRsSEO("seDescription")
'	SEkeywords		=	objRsSEO("seKeywords")
	bannerText		=	objRsSEO("bannerText")
	spacerHeight	=	objRsSEO("spacerHeight")
	spacerWidth		=	objRsSEO("spacerWidth")
	bordercolor		=	objRsSEO("banner_bordercolor")
	bgcolor			=	objRsSEO("banner_bgcolor")
	h1Tag			=	objRsSEO("h1Tag")
	h1color			=	objRsSEO("h1color")
	h2Tag			=	objRsSEO("h2Tag")
	strBreadcrumb	=	objRsSEO("breadCrumb")
end if


dim strBreadcrumb
if strBreadcrumb = "" then strBreadcrumb = carrierName & " Cell Phone Accessories"
%>
<!--#include virtual="/includes/template/top.asp"-->
<script>
window.WEDATA.pageType = "carrier";
window.WEDATA.pageData = {
    carrier: <%= jsStr(carrierName) %>,
    carrierId: <%= jsStr(carrierId) %>
};
</script>

<!--#include virtual="/includes/asp/inc_GetElementText.asp"-->
<style>
	h1 { color:<%=h1color%>; font-family:Arial,Helvetica,sans-serif; font-size:22px; font-weight:bold; display:inline; }
</style>

<div style="width:100%;">
    <div style="width:100%; float:left; ">
		<a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>    
    </div>
    <div style="width:100%; height:119px; float:left; background:url(/productpics/carriers/<%=carrierCode%>/banner.jpg) no-repeat; background-size:748px 119px;">
		<img src="/images/spacer.gif" width="<%=spacerWidth%>" height="<%=spacerHeight%>"><h1><%=h1Tag%></h1>
    </div>
    <div style="width:744px; float:left; border-left:2px solid <%=bordercolor%>; border-right:2px solid <%=bordercolor%>;">
        <div style="width:100%; float:left; padding-bottom:5px;">
			<img src="/images/carriers/hline_dot2.jpg" width="740" height="8" border="0">
		</div>
        <div style="width:100%; float:left;">
            <div class="left">
                <img src="/images/carriers/header.jpg" width="300" height="45" border="0" alt="Find Accessories for these <%=carrierName%> phones by Brand">
            </div>
            <div class="right" style="padding-right:10px;">
                <img src="/images/carriers/header_topseller.jpg" width="176" height="45" border="0" alt="Top Seller">
            </div>
        </div>
        <div style="width:100%; float:left; padding-top:5px;">
			<img src="/images/carriers/hline_dot2.jpg" width="740" height="8" border="0">
		</div>
        <div style="width:100%; float:left;">
            <div class="left" style="width:550px;">
            <%
			set fs = CreateObject("Scripting.FileSystemObject")			
            SQL = "SELECT DISTINCT A.brandName, A.brandID FROM we_brands A INNER JOIN we_models B ON A.brandid = B.brandid WHERE b.isTablet = 0 and B.carrierCode LIKE '%" & carrierCode & ",%' ORDER BY A.brandName"
            set RS3 = oConn.execute(SQL)
            others = 0
            do until RS3.eof
                if RS3("brandID") = 15 then
                    others = 1
                else
					if fs.FileExists(server.MapPath("/images/cats/we_cat_brand_" & RS3("brandid") & ".jpg")) then
                %>
                <div class="left">
                    <!--<a href="/car-<%=carrierID%>-b-<%=RS3("brandID")%>-<%=formatSEO(carrierName)%>-<%=formatSEO(RS3("brandName"))%>-phone-accessories">-->
                    <a href="<%=brandSEO(RS3("brandID"))%>">
                        <img src="/images/cats/we_cat_brand_<%=RS3("brandid")%>.jpg" border="0" vspace="5" hspace="5" title="<%=optCarrierName(carrierName)%>&nbsp;<%=optBrandName(RS3("brandName"))%> Accessories" alt="<%=optCarrierName(carrierName)%>&nbsp;<%=optBrandName(RS3("brandName"))%>&nbsp;Accessories">
                    </a>
                </div>
                <%
					end if
                end if
                RS3.movenext
            loop
            
            if others = 1 then
            %>
                <div class="left">
                    <!--<a href="/car-<%=carrierID%>-b-15-<%=formatSEO(carrierName)%>-other-phone-accessories">-->
                    <a href="<%=brandSEO(15)%>">
                        <img src="/images/cats/we_cat_brand_15.jpg" border="0" vspace="5" hspace="5" alt="<%=carrierName%>&nbsp;Other Phone Accessories">
                    </a>
                </div>
            <%
            end if
            RS3.close
            set RS3 = nothing
            %>
            
            </div>
            <div class="right" style="padding-right:10px;">
                <a href="http://www.wirelessemporium.com/p-21321-aliph-jawbone-prime-3-wireless-bluetooth-headset--black-">
                    <img src="/images/carriers/handsfree1.jpg" width="176" height="97" border="0" alt="Aliph Jawbone">
                </a><br><br>
                <a href="http://www.wirelessemporium.com/p-19485-window-mount-cell-phone-pda-gps-holder">
                    <img src="/images/carriers/windowmount1.jpg" width="176" height="97" border="0" alt="Window Mount">
                </a><br><br>
                <a href="http://www.wirelessemporium.com/p-24155-universal-7-in-1-retractable-cell-phone-car-charger">
                    <img src="/images/carriers/charger1.jpg" width="176" height="97" border="0" alt="Universal Charger">
                </a>
            </div>
        </div>
		<div style="width:100%; float:left;" >
        	<p style="padding:5px;" class="carrierText">
	            <%=SeBottomText%>
            </p>
		</div>
    </div>
	<div style="width:100%; float:left;">
	    <img src="/productpics/carriers/<%=carrierCode%>/bottom.jpg" width="748" height="29" border="0">
    </div>
</div>
<div class="clear"></div>
<table border="0" cellpadding="10" cellspacing="0" width="100%" align="center">
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->