<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
call fOpenConn()
SQL = "SELECT DISTINCT A.*, C.*, D.* FROM ((we_models A INNER JOIN we_items B ON A.modelID = B.modelID)"
SQL = SQL & " INNER JOIN we_brands C ON A.brandid = C.brandid)"
SQL = SQL & " INNER JOIN we_types D ON B.typeid = D.typeid"
SQL = SQL & " WHERE B.modelid = 940"
SQL = SQL & " AND B.hideLive = 0"
SQL = SQL & " AND B.inv_qty <> 0"
SQL = SQL & " ORDER BY A.temp DESC"
'session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3

dim brandName, brandID, modelName, modelImg
brandName = RS("brandName")
brandID = RS("brandID")
modelName = RS("modelName")
modelImg = RS("modelImg")

dim SEtitle, SEdescription, SEkeywords, topText, bottomText, RSmeta
SEtitle = "iPad Accessories: iPad Headset, iPad Screen Protector, iPad 3G Car Charger"
SEdescription = "Get the best deals on iPad accessories from Wireless Emporium - the #1 store online for cell phone accessories like iPad headsets, screen protectors, car chargers and much more."
SEkeywords = "iPad accessories, iPad 3G accessories, iPad screen protector, iPad headset, iPad 3g car charger, iPad charger, iPad 3g charger"

h1 = "We Have the iPad 3G Accessories You Need!"
altText = "Apple iPad Accessories: iPad TYPES"
topText = "If you're looking for iPad accessories, you've come to the right place! Wireless Emporium is the leading name in <a class=""topText-link"" href=""http://www.wirelessemporium.com"" title=""cell phone accessories"">cell phone accessories</a>, and we have the latest iPad accessories available at discounted prices. "
topText = topText & "<br><br>With a plethora of iPad cases, iPad covers, and other iPad accessories to choose from, you'll find what you need at affordable prices. Choose from dozens of iPad cases and iPad covers that will help protect your precious iPad from unwanted damage. Pick up an iPad headset today for hands-free and hassle-free calls while you drive and work. "
topText = topText & "With wired and wireless varieties available, it's easy to find an iPad headset that suits your needs and your budget. We also carry the latest in iPad 3G accessories, so don't hesitate to pick up an iPad 3G charger, iPad 3G car charger, iPad 3G screen protector, and anything else you've been looking for. "
topText = topText & "Buy a replacement or backup iPad 3G charger or iPad 3G car charger to ensure that your iPad 3G will be charged wherever you go. Crystal clear iPad 3G screen protectors protect your phone's screen without detracting from the screen's resolution."
bottomText = "Since 2001, Wireless Emporium has been offering high-quality cell phone accessories to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best cell phone accessories at the best value. Shipping and superior customer service is free with every order and offered every day!"

dim strBreadcrumb
strBreadcrumb = brandName & " " & modelName & " Cell Phone Accessories"
%>
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="/brand.asp?brandid=<%=brandID%>"><%=brandName%> Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <h1>iPad Accessories</h1>
        </td>
    </tr>
    <tr>
        <td width="100%" height="200" align="left" valign="top" style="background-image: url('/images/WE_ipad_banner.jpg'); background-repeat: no-repeat; background-position: center top;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                    <td align="center" valign="top" width="300" class="topText">&nbsp;
                        
                    </td>
                    <td align="center" valign="top" width="500" class="topText">
                        <img src="/images/spacer.gif" width="1" height="40" border="0">
                        <div align="justify"><%=topText%></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%">
            <img src="/images/spacer.gif" width="1" height="10" border="0">
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="800">
                <tr>
                    <%
                    response.write "<td align=""center"" valign=""top"" width=""200""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""119""><tr><td align=""center"" valign=""middle""><img src=""/images/spacer.gif"" width=""1"" height=""6"" border=""0""></td></tr>" & vbcrlf
                    response.write "<tr><td align=""center"" valign=""middle""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""100""><tr><td align=""center"" valign=""bottom""><a class=""brand-cat"" href=""hf-sm-940-" & formatSEO(brandName) & "-" & formatSEO(modelName) & "-bluetooth-headsets-headphones.asp"" title=""" & replace(altText,"TYPES","Headsets/Bluetooth") & """>Handsfree</a></td></tr>" & vbcrlf
                    response.write "<tr><td ""center"" valign=""middle"" height=""150""><a href=""hf-sm-940-" & formatSEO(brandName) & "-" & formatSEO(modelName) & "-bluetooth-headsets-headphones.asp""><img src=""/images/categories/" & brandID & "_5.gif"" width=""172"" height=""106"" border=""0"" alt=""" & replace(altText,"TYPES","Headsets/Bluetooth") & """></a></td></tr></table></td></tr>" & vbcrlf
                    response.write "<tr><td align=""center"" valign=""middle""><img src=""/images/spacer.gif"" width=""1"" height=""6"" border=""0""></td></tr></table></td>" & vbcrlf
                    a = 1
                    do until RS.eof
                        response.write "<td align=""center"" valign=""top"" width=""200""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""119""><tr><td align=""center"" valign=""middle""><img src=""/images/spacer.gif"" width=""1"" height=""6"" border=""0""></td></tr>" & vbcrlf
                        response.write "<tr><td align=""center"" valign=""middle""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""100""><tr><td align=""center"" valign=""bottom""><a class=""brand-cat"" href=""/sb-" & brandID & "-sm-940-sc-" & RS("typeid") & "-" & formatSEO(RS("typename")) & "-" & formatSEO(brandname) & "-" & formatSEO(modelName) & "-" & ".asp"" title=""" & replace(altText,"TYPES",nameSEO(RS("typeName"))) & """>" & nameSEO(RS("typeName")) & "</a></td></tr>" & vbcrlf
                        response.write "<tr><td align=""center"" valign=""middle"" height=""150""><a href=""/sb-" & brandID & "-sm-940-sc-" & RS("typeid") & "-" & formatSEO(RS("typename")) & "-" & formatSEO(brandname) & "-" & formatSEO(modelName) & ".asp""><img src=""/images/categories/" & brandID & "_" & RS("typeid") & ".gif"" width=""172"" height=""106"" border=""0"" alt=""" & replace(altText,"TYPES",nameSEO(RS("typeName"))) & """></a></td></tr></table></td></tr>" & vbcrlf
                        response.write "<tr><td align=""center"" valign=""middle""><img src=""/images/spacer.gif"" width=""1"" height=""6"" border=""0""></td></tr></table></td>" & vbcrlf
                        RS.movenext
                        a = a + 1
                        if a = 4 then
                            response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""4"">&nbsp;</td></tr><tr>" & vbcrlf
                            a = 0
                        end if
                    loop
                    if a = 1 then
                        response.write "<td width=""200"">&nbsp;</td><td width=""200"">&nbsp;</td><td width=""200"">&nbsp;</td>" & vbcrlf
                    elseif a = 2 then
                        response.write "<td width=""200"">&nbsp;</td><td width=""200"">&nbsp;</td>" & vbcrlf
                    elseif a = 3 then
                        response.write "<td width=""200"">&nbsp;</td>" & vbcrlf
                    end if
                    %>
                </tr>
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">&nbsp;</td>
    </tr>
    <!--#include virtual="/includes/asp/inc_banners.asp"-->
    <tr>
        <td align="left" valign="top">&nbsp;</td>
    </tr>
    <tr>
        <td align="center">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" class="topText">
                        <p style="margin-top:0px;margin-bottom:6px;"><%=bottomText%></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->