<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/Framework/Data/ProductDetail/Base.asp"-->
<!--#include virtual="/includes/asp/inc_Compatibility.asp"-->
<%
response.buffer = true
dim itemid, itemDesc, productPage, modelID, URL, curSite, isTablet
dim pageName : pageName = "product.asp"
dim basePageName : basePageName = "product.asp"
googleAds = 1
noCommentBox = true
productPage = 1
pageTitle = "product"
isTablet = false

itemid = prepInt(request.querystring("itemid"))
musicSkins = prepInt(request.QueryString("musicSkins"))

dim varient : varient = prepInt(request.QueryString("varient"))

response.Cookies("saveItemID") = itemID

dim jpeg : set jpeg = Server.CreateObject("Persits.Jpeg")
dim fs : set fs = CreateObject("Scripting.FileSystemObject")

dim OutOfStock
OutOfStock = 0

dim SEtitle, SEdescription, SEkeywords

dim strTypename, strBrandname, strModelname, strItemname
call fOpenConn()
SQL = "SELECT c.itemdesc, b.brandname, m.modelID, m.modelname, t.typename FROM ((we_items c"
SQL = SQL & " left join we_brands b ON b.brandid = c.brandid)"
SQL = SQL & " left join we_models m ON m.modelid = c.modelid)"
SQL = SQL & " left join we_types t ON t.typeid = c.typeid"
SQL = SQL & " WHERE c.itemid = '" & itemID & "'"
set RS = oConn.execute(SQL)
if not RS.eof then
	strTypename = nameSEO(RS("typename"))
	strBrandname = RS("brandname")
	modelID = RS("modelID")
	strModelname = RS("modelname")
	strItemname = insertDetails(RS("itemdesc"))
else
	if musicSkins = 1 then
		strItemname = insertDetails(itemDesc)
		strTypename = "music skins"
		strBrandname = brandName
		strModelname = modelName
		
		SEtitle = insertDetails(itemDesc) & " " & strBrandname & " " & strModelname & " Music Skin"
		SEdescription = "Buy the " & insertDetails(itemDesc) & " Music Skin for the " & strBrandname & " " & strModelname & " at Wireless Emporium with Free Shipping!"
		SEkeywords = insertDetails(itemDesc) & ", " & insertDetails(itemDesc) & " music skin, " & strBrandname & " " & strModelname & " skins, " & strModelname & " skins, cell phone skin, phone skins"
	else
		strTypename = "Misc. Gear"
		strBrandname = "ALL Cell Phone Brands"
		strModelname = ""
		SQL = "SELECT itemdesc FROM we_items WHERE itemid = '" & itemID & "'"
		dim RS2
		set RS2 = oConn.execute(SQL)
		if not RS2.eof then strItemname = insertDetails(RS2("itemdesc"))
		RS2.close
		set RS2 = nothing
	end if
end if
RS.close
set RS = nothing


'========================================================= color picker ========================
sql = "select id, color, colorCodes, borderColor from xproductcolors order by 1"
session("errorSQL") = sql
arrColors = getDbRows(sql)

dim colorSlaves : colorSlaves = ""
dim numAvailColors : numAvailColors = cint(0)
dim colorid : colorid = 0
sql = 	"select	distinct x.master, v.colorSlaves, v.cnt numAvailColors, b.colorid curColorID" & vbcrlf & _
		"	,	convert(varchar(2000), substring((	select	(',' + convert(varchar(100), i.colorid)) " & vbcrlf & _
		"											from	we_items i " & vbcrlf & _
		"											where	i.partnumber in (select slave from xproductcolormatrix where master = x.master) " & vbcrlf & _
		"												and i.modelid = b.modelid and i.inv_qty <> 0 and i.hidelive = 0" & vbcrlf & _
		"												and	(select top 1 inv_qty from we_items where partNumber = i.partNumber order by master desc, inv_qty desc) > 0" & vbcrlf & _
		"											order by i.colorid " & vbcrlf & _
		"											for xml path('') " & vbcrlf & _
		"											), 2, 1000)) availColors" & vbcrlf & _
		"from	xproductcolormatrix x join xproductcolormatrix a " & vbcrlf & _
		"	on	x.master = a.master join we_items b" & vbcrlf & _
		"	on	a.slave = b.partnumber join v_slaveColorMatrix v" & vbcrlf & _
		"	on	x.master = v.master" & vbcrlf & _
		"where	b.itemid = '" & itemid & "'" & vbcrlf & _
		"	and	x.hideLive = 0"
'response.write "<pre>" & sql & "</pre>"
session("errorSQL") = sql
set rs = oConn.execute(sql)
if not rs.eof then
'	colorSlaves		=	rs("colorSlaves")
	numAvailColors	=	cint(rs("numAvailColors"))
	colorid			=	rs("curColorID")
	colorSlaves		=	rs("availColors")
end if
'//========================================================= color picker ========================


if SEtitle = "" then SEtitle = "" & strItemname & " - Cell Phone " & strTypename & " for " & strBrandname & " " & strModelname & ""
if SEkeywords = "" then SEkeywords = "Discount " & strTypename & " for " & strBrandname & " " & strModelname & ", cheap cell phone accessories, " & strItemname & ""
if SEdescription = "" then SEdescription = "" & strItemname & " - Discount " & strTypename & " for " & strBrandname & " " & strModelname & " and Wireless Phone Accessories at Wireless Emporium."

dim modelLink
if modelID = 940 then
	modelLink =  "http://www.wirelessemporium.com/apple-ipad-accessories.asp"
else
	modelLink = "http://www.wirelessemporium.com/T-" & modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
end if

dim cart
session("otherAdminOptions") = ", <a style='color:#ffffff;' href='/admin/db_update_models.asp?ModelID=" & modelID & "&BrandID=" & brandID & "&searchType=Model&submitType=Edit&submitModel=Search'>Edit " & modelName & "</a>, <a style='color:#ffffff;' href='/admin/db_update_items.asp?ItemID=" & itemID & "'>Edit This Product</a>"
if len(itemDesc) > 0 then
	displayItemDesc = insertDetails(itemDesc)
	itemDesc = replace(replace(itemDesc,"(",""),")","")
end if


'todo: need to consolidate Metatags with we_XXXText
dim strH1: strH1 = displayItemDesc
dim strH2: strH2 = EMPTY_STRING
dim strAltText: strAltText = EMPTY_STRING
dim strBreadcrumb: strBreadcrumb = strItemname
if typeID = 16 then 
	if brandName <> "Other" then strBreadcrumb = brandName &" "& modelName else strBreadcrumb = modelName
end if

'strBreadcrumb = brandName & " " & modelName & " " & singularSEO(categoryName)

'set input values to extract page meta-values
dim dicReplaceAttribute: set dicReplaceAttribute = CreateObject("Scripting.Dictionary")
dicReplaceAttribute( "BrandName") = brandName
dicReplaceAttribute( "ProductName") = itemDesc

'set known product attributes to replace out template placeholders
dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")

'used for product rating and product review [knguyen/20110610]
dim objSqlExecReview: set objSqlExecReview = GetUserRatingStat( ItemId)
dim strAvgRating: strAvgRating = objSqlExecReview.Item( "AvgRating")
dim dblAvgRating: dblAvgRating = cdbl( strAvgRating)
dim strRatingCount: strRatingCount = objSqlExecReview.Item( "RatingCount")
dim strRatingLevel:
set objSqlExecReview = nothing

SEtitle = insertDetails(SEtitle)
SEdescription = insertDetails(SEdescription)

if musicSkins = 1 then 'hack in original pricing as constant .. currently no blow-out support for this table [knguyen/20110602]
	SQL = "select '' itemDimensions, 1000 masterInvQty, 'False' alwaysInStock, '' as itemLongDetail, 0 as HandsfreeType, null as ItemKit_NEW, 'musicSkins' as vendor, '' as UPCCode, 'new' as Condition, 1 as NoDiscount, c.defaultImg, c.sampleImg, c.musicSkinsID as partNumber, c.brandID, 20 as typeID, c.image as itemPic, 100 as inv_qty, c.id as itemID, 'false' as hideLive, c.price_we as price_our, c.msrp as price_retail, c.brand + ' ' + c.model + ' ' + c.artist + ' ' + c.designName + ' Music Skins' as itemDesc, b.modelName, b.modelImg, c.brand as brandName, 'music skins' as typeName, 'OriginalPrice' as [ActiveItemValueType], c.price_we as [OriginalPrice], b.isTablet, 0 seasonal, 0 showAnimation, 0 customize from we_items_musicSkins c left join we_models b on c.modelID = b.modelID where c.id = " & itemID
else
	if modelid <> "0" then
		SQL = "SELECT c.*, (select top 1 inv_qty from we_items where partNumber = c.partNumber and master = 1 order by inv_qty desc) as masterInvQty, d.alwaysInStock, m.modelName, m.modelImg, b.brandName, t.typeName, isnull( tv.[ActiveItemValueType], 'OriginalPrice') as [ActiveItemValueType], tv.[OriginalPrice], m.isTablet, e.showAnimation, e.customize FROM ((we_Items c left join we_models m ON c.modelID=m.modelID)"
		SQL = SQL & " left join we_brands b ON c.brandID=b.brandID)"
		SQL = SQL & " left join we_types t ON c.typeID=t.typeID"
		SQL = SQL & " left join we_pnDetails d on c.partNumber = d.partNumber"
	else
		SQL =	"SELECT c.*, (" &_
					"select top 1 inv_qty " &_
					"from we_items " &_
					"where partNumber = c.partNumber and master = 1 " &_
					"order by inv_qty desc" &_
				") as masterInvQty, d.alwaysInStock, 'UNIVERSAL' AS modelName, 'UNIVERSAL.JPG' AS modelImg, 0 AS brandID, 'UNIVERSAL' AS brandName, t.typeName, isnull( tv.[ActiveItemValueType], 'OriginalPrice') as [ActiveItemValueType], tv.[OriginalPrice], cast(0 as bit) isTablet, e.showAnimation, e.customize " &_
				"FROM we_items c " &_
					"left join we_types t ON c.typeid = t.typeid " &_
					"left join we_pnDetails d on c.partNumber = d.partNumber"
	end if
	SQL = SQL & " " & "left join we_ItemsExtendedData e on c.PartNumber = e.partNumber"
	SQL = SQL & " " & strSqlSaleLeftJoin
	SQL = SQL & " WHERE c.itemID='" & itemid & "'"
	SQL = SQL & " AND c.HideLive = 0"
	SQL = SQL & " AND c.price_Our > 0"
end if
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 1

if not RS.eof then
	dim NoDiscount, partnumber, brandID, typeID, itempic, price_retail, price_Our, KIT, HandsfreeType, seasonal
	dim modelName, modelImg, brandName, categoryName, SquaretradeItemCondition, UPCCode, vendor, strActiveItemValueType, blnIsNotOriginalPrice, strOriginalPrice
	if musicSkins = 1 then
		defaultImg = rs("defaultImg")
		sampleImg = rs("sampleImg")
	end if
	itemDimentions = RS("itemDimensions")
	seasonal = RS("seasonal")
	NoDiscount = RS("NoDiscount")
	partnumber = RS("partnumber")
	if left(partnumber,3) = "WCD" then response.Redirect("/")
	brandID = RS("brandID")
	typeID = RS("typeID")
	itemDesc = insertDetails(RS("itemDesc"))
	itempic = RS("itempic")
	price_retail = RS("price_retail")
	price_Our = RS("price_Our")
	KIT = RS("ItemKit_NEW")
	HandsfreeType = RS("HandsfreeType")
	masterInvQty = RS("masterInvQty")
	alwaysInStock = prepStr(RS("alwaysInStock"))
	modelName = RS("modelName")
	modelImg = RS("modelImg")
	brandName = RS("brandName")
	categoryName = RS("typeName")
	SquaretradeItemCondition = RS("Condition")
	UPCCode = RS("UPCCode")
	vendor = RS("vendor")
	strActiveItemValueType = RS("ActiveItemValueType"): blnIsNotOriginalPrice = (strActiveItemValueType<>"OriginalPrice")
	strOriginalPrice = RS("OriginalPrice")
	arrayStr = ""
	isTablet = RS("isTablet")
	showAnimation = RS("showAnimation")
	customize = rs("customize")
	if not isnull(KIT) and len(KIT) > 0 then
		SQL = "SELECT itemID, isnull((select inv_qty from we_items where partNumber = a.partNumber and master = 1 and hideLive = 0), 0) as inv_qty FROM we_items a WHERE itemID IN (" & KIT & ")"
		session("errorSQL") = SQL
		set RS2 = oConn.execute(sql)
		if RS2.eof then
			OutOfStock = 1
		end if
		RS2.close
		set RS2 = nothing
	elseif masterInvQty <= 0 then
		OutOfStock = 1
	end if
	dim strItemLongDetail, COMPATIBILITY, download_URL, download_TEXT, PackageContents
	dim BULLET1, BULLET2, BULLET3, BULLET4, BULLET5, BULLET6, BULLET7, BULLET8, BULLET9, BULLET10
	strItemLongDetail = RS("itemLongDetail")
	if not isNull(strItemLongDetail) then
		strItemLongDetail = replace(strItemLongDetail,"Wireless Emporium","WirelessEmporium.com")
		'strItemLongDetail = replace(strItemLongDetail," FREE!"," less!")
		strItemLongDetail = replace(strItemLongDetail,chr(34),"''")
		strItemLongDetail = replace(strItemLongDetail,"<a href='/downloads/","<a href='/downloads/")
		strItemLongDetail = replace(strItemLongDetail,vbcrlf," ")
	end if
	if musicSkins = 1 then
		BULLET1 = ""
		BULLET2 = ""
		BULLET3 = ""
		BULLET4 = ""
		BULLET5 = ""
		BULLET6 = ""
		BULLET7 = ""
		BULLET8 = ""
		BULLET9 = ""
		BULLET10 = ""
		download_URL = ""
		download_TEXT = ""
		PackageContents = ""
		COMPATIBILITY = ""
	else
		if not isNull(RS("BULLET1")) and RS("BULLET1") <> "" then BULLET1 = RS("BULLET1")
		if not isNull(RS("BULLET2")) and RS("BULLET2") <> "" then BULLET2 = RS("BULLET2")
		if not isNull(RS("BULLET3")) and RS("BULLET3") <> "" then BULLET3 = RS("BULLET3")
		if not isNull(RS("BULLET4")) and RS("BULLET4") <> "" then BULLET4 = RS("BULLET4")
		if not isNull(RS("BULLET5")) and RS("BULLET5") <> "" then BULLET5 = RS("BULLET5")
		if not isNull(RS("BULLET6")) and RS("BULLET6") <> "" then BULLET6 = RS("BULLET6")
		if not isNull(RS("BULLET7")) and RS("BULLET7") <> "" then BULLET7 = RS("BULLET7")
		if not isNull(RS("BULLET8")) and RS("BULLET8") <> "" then BULLET8 = RS("BULLET8")
		if not isNull(RS("BULLET9")) and RS("BULLET9") <> "" then BULLET9 = RS("BULLET9")
		if not isNull(RS("BULLET10")) and RS("BULLET10") <> "" then BULLET10 = RS("BULLET10")
		
		download_URL = RS("download_URL")
		download_TEXT = RS("download_TEXT")
		PackageContents = RS("PackageContents")
		if isnull(RS("COMPATIBILITY")) or len(RS("COMPATIBILITY")) < 1 then
			COMPATIBILITY = CompatibilityList(PartNumber,"display")
		else
			COMPATIBILITY = RS("COMPATIBILITY")
		end if
	end if
else
	session("errorCode") = "p-101"
	call PermanentRedirect("/")
end if

sql = 	"select	q.id qid, q.userName qName, q.content qContent, q.contentType qContentType, convert(varchar(10), q.postDate, 20) qPostDate" & vbcrlf & _
		"	,	a.id aid, a.userName aName, a.content aContent, a.contentType aContentType, convert(varchar(10), a.postDate, 20) aPostDate" & vbcrlf & _
		"	,	r.id rid, r.userName rName, r.content rContent, r.contentType rContentType, convert(varchar(10), r.postDate, 20) rPostDate" & vbcrlf & _
		"from	dbo.ugc q left outer join dbo.ugc a" & vbcrlf & _
		"	on	q.id = a.parentID and a.contentType = 2 left outer join dbo.ugc r" & vbcrlf & _
		"	on	a.id = r.parentID and r.contentType = 3" & vbcrlf & _
		"where	q.partnumber = 	'" & partnumber & "'" & vbcrlf & _
		"	and	not (q.ContentType = 1 and a.id is null)" & vbcrlf & _
		"order by q.contentType, q.id, a.id, r.id"
session("errorSQL") = sql
arrUGC = getDbRows(sql)
%>
<!--#include virtual="/includes/template/top_product.asp"-->
<%
on error resume next
for formIndex = 1 to 3
	recentItem = Request.Cookies("RecentItem" & formIndex)("id")
	if inStr(recentItem,"/") > 0 then recentItem = left(recentItem,inStr(recentItem,"/")-1)
	if recentItem <> "" and isNumeric(recentItem) then response.Cookies("RecentItem" & formIndex)("id") = recentItem
next
if Request.Cookies("RecentItem1")("id") = "" OR (Request.Cookies("RecentItem1")("id") = itemid) then
	Response.Cookies("RecentItem1")("id") = itemid
	Response.Cookies("RecentItem1")("date") = now
	Response.Cookies("RecentItem1").Expires = DateAdd("m", 1, now)
elseif Request.Cookies("RecentItem2")("id") = "" OR (Request.Cookies("RecentItem2")("id") = itemid) then
	Response.Cookies("RecentItem2")("id") = itemid
	Response.Cookies("RecentItem2")("date") = now
	Response.Cookies("RecentItem2").Expires = DateAdd("m", 1, now)
elseif Request.Cookies("RecentItem3")("id") = "" OR (Request.Cookies("RecentItem3")("id") = itemid) then
	Response.Cookies("RecentItem3")("id") = itemid
	Response.Cookies("RecentItem3")("date") = now
	Response.Cookies("RecentItem3").Expires = DateAdd("m", 1, now)
else
	dim formIndex, thisleastRecentIndex, thisleastRecentDate
	thisleastRecentIndex = 1
	thisleastRecentDate = Request.Cookies("RecentItem1")("date")
	for formIndex = 1 to 3 step 1
		if thisleastRecentDate > Request.Cookies("RecentItem" & formIndex)("date") then
			thisLeastRecentDate = Request.Cookies("RecentItem" & formIndex)("date")
			thisLeastRecentIndex = formIndex
		end if
	next
	Response.Cookies("RecentItem" & thisLeastRecentIndex)("id") = itemid
	Response.Cookies("RecentItem" & thisLeastRecentIndex)("date") = now
	Response.Cookies("RecentItem" & thisLeastRecentIndex).Expires = DateAdd("m", 1, now)
end if
on error goto 0

sql = "select AVG(rating) as avgRating, COUNT(*) as reviewCnt from we_Reviews where approved = 1 and itemID in (select itemID from we_Items where PartNumber = '" & partNumber & "')"
session("errorSQL") = sql
set reviewRS = oConn.execute(sql)

if reviewRS.EOF then
	dblAvgRating = 0
	strRatingCount = 0
else
	dblAvgRating = reviewRS("avgRating")
	strRatingCount = reviewRS("reviewCnt")
end if

dim bFinalOutofStock : bFinalOutofStock = false
if OutOfStock = 1 and (alwaysInStock = "" or alwaysInStock = "False") then
	bFinalOutofStock = true
end if

specialOffer = 0
validProd = split("FP1|FP2|FP3|FP4|FP5|FP6","|")
for i = 0 to ubound(validProd)
	if instr(partNumber,validProd(i)) > 0 then specialOffer = 1
next
if NoDiscount then specialOffer = 0

featureList = BULLET1 & "##" & BULLET2 & "##" & BULLET3 & "##" & BULLET4 & "##" & BULLET5 & "##" & BULLET6 & "##" & BULLET7 & "##" & BULLET8 & "##" & BULLET9 & "##" & BULLET10
do while instr(featureList,"####") > 0
	featureList = replace(featureList,"####","##")
loop
bFeatureList = false
%>
<link href="/includes/css/product/accessory.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_imageLeftRight2.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_otherStyles1.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_mayLike1.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_features2.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_buyNowBox3.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_limitedSupply2.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#buyNow1 { display:none; } /* OG layout */
</style>
<link href="/includes/css/iv_leftNav1.css" rel="stylesheet" type="text/css">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <% if typeid = 16 then %>
    <tr id="bannerShippingSmall"><td width="100%" align="center"><div class="freeShippingBanner" title="free express shipping"></div></td></tr>
    <%else%>
    <tr id="bannerShippingSmall"><td width="100%" align="center"><img src="/images/we-free-sameday-shipping-4.jpg" border="0" alt="free same day shipping" /></td></tr>
    <% end if %>
    <tr>
        <td>
            <script type="text/javascript"><!--
            var request_uri = document.location.href;
            var referrer = document.referrer;
            var nxt_cid = 'e05d3bcff4a5f60f1ce09f376d1f8cf8';
            var nxt_mode = 'f';
            var nxt_num_products = '5';
            var nxt_heading_font_family = '';
            var nxt_heading_font_size = '';
            var nxt_item_font_family = '';
            var nxt_item_font_size = '';
            var nxt_heading = 'More WirelessEmporium.com Search Results for "[KEYWORDS]"';
            var nxt_image_height = '90';
            var nxt_image_width = '90';
            var nxt_thumbnail_background_color = '#ffffff';
            var nxt_heading_background_color = '';
            var nxt_heading_text_color = '';
            var nxt_item_text_color = '';
            var nxt_border_color = '';
            var nxt_main_background_color = '';
            var nxt_tracking_elements = 'utm_source=nextopia&utm_medium=search&utm_campaign=nextLpOptimizer';

            var nxt_affiliate_tracking = 'nt101';
            var nxt_display_conditions = 'only_from';
            var nxt_display_orientation = 'horizontal';
            var nxt_display_template = '<a href="[URL]"><img src="[IMAGE]" border="0"/></a><br /><a href="[URL]">[NAME]</a><br />[PRICE]<br />';
            var nxt_auth_key = '92a4728b1f881d658c23072efcc698f3';
            
            //document.write(unescape("%3Cscript src='") + ('https:' == document.location.protocol ? 'https:' : 'http:') + "//p.nextopia.net/nxt_personalization.js' type='text/javascript'" + unescape("%3E%3C/script%3E"));
            
            //--></script>
        </td>
    </tr>
    <tr>
        <td class="breadcrumbFinal">
            <%if typeID = 16 then%>
            <a class="breadcrumb" href="/unlocked-cell-phones.asp">Unlocked Cell Phones</a>&nbsp;&#183;&nbsp;<a class="breadcrumb" href="/cp-sb-<%=brandID & "-" & formatSEO(brandName)%>-unlocked-cell-phones.asp"><%=brandName%> Unlocked Cell Phones</a>
            <%else%>
				<%if musicSkins = 1 then%>
					<a class="breadcrumb" href="/">Cell Phone Accessories</a>&nbsp;&#183;&nbsp;<a class="breadcrumb" href="/<%=brandSEO(brandID)%>"><%=brandName%> Accessories</a>&nbsp;&#183;&nbsp;<a class="breadcrumb" href="<%=modelLink%>"><%=brandName & " " & modelName%> Accessories</a>&nbsp;&#183;&nbsp;<a class="breadcrumb" href="/sb-<%=brandID & "-sm-" & modelID & "-sc-" & typeID & "-" & formatSEO(categoryName) & "-genre"%>.asp"><%=brandName & " " & modelName & " " & nameSEO(categoryName)%></a>
				<%elseif isTablet then%>	
					<a class="breadcrumb" href="/tablet-ereader-accessories.asp">Tablet Accessories</a>&nbsp;&#183;&nbsp;<a class="breadcrumb" href="<%=modelLink%>"><%=brandName & " " & modelName%> Accessories</a>&nbsp;&#183;&nbsp;<a class="breadcrumb" href="/sb-<%=brandID & "-sm-" & modelID & "-sc-" & typeID & "-" & formatSEO(categoryName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp"><%=brandName & " " & modelName & " " & nameSEO(categoryName)%></a>
				<%else%>	                
					<a class="breadcrumb" href="/">Cell Phone Accessories</a>&nbsp;&#183;&nbsp;<a class="breadcrumb" href="/<%=brandSEO(brandID)%>"><%=brandName%> Accessories</a>&nbsp;&#183;&nbsp;<a class="breadcrumb" href="<%=modelLink%>"><%=brandName & " " & modelName%> Accessories</a>&nbsp;&#183;&nbsp;<a class="breadcrumb" href="/sb-<%=brandID & "-sm-" & modelID & "-sc-" & typeID & "-" & formatSEO(categoryName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp"><%=brandName & " " & modelName & " " & nameSEO(categoryName)%></a>
				<%end if%>
            <%end if%>
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                    <td align="left" valign="top" width="100%">
                    	<!-- rich snippets mark up -->
                        <div itemscope itemtype="http://data-vocabulary.org/Product">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td width="100%" align="left" valign="top" style="padding-top:5px; border-top:3px solid #CCC;">
                                    <span itemprop="name"><h1 class="product" id="id_h1"><%=itemDesc%></h1></span>
                                </td>
                            </tr>
                            <tr><td><br /></td></tr>
                            <tr>
                                <td width="100%" align="left" valign="top">
                                    <div id="productPicBox" style="width:300px; position:relative;">
                                    	<%	
										dim fsImg, itemImgAbsolutePath, itemImgPath, itemSamplePath, itemImgFullPath, objItemImgFull, hasFlashFile
										sampleImg = ""
										itemImgPath = ""
										itemSamplePath = ""
										itemImgFullPath = ""
										itemImgAbsolutePath = ""
										objItemImgFull = ""
										strZoom = ""
										hasFlashFile = false

										if fs.fileExists(server.MapPath("/productpics/big/zoom/" & itempic)) then
											strZoom = "onclick=""showFloatingZoomImage();"""
										end if
																				
										session("errorSQL") = "itempic:" & itempic
										if itempic <> "" then
											flashImgPath = "/productpics/swf/" & replace(itempic, ".jpg", ".swf")
											if fs.FileExists(Server.MapPath(flashImgPath)) then 
												hasFlashFile = true
											end if
										end if
										
										altText = brandName & "&nbsp;" & modelName & "&nbsp;" & singularSEO(categoryName) & "&nbsp;-&nbsp;" & itemDesc
										
										set fsImg = CreateObject("Scripting.FileSystemObject")
										if musicSkins = 1 then
											itemImgPath = "/productpics/musicSkins/musicSkinsLarge"
											itemImgFullPath = itemImgPath & "/" & itemPic
											itemImgAbsolutePath = Server.MapPath(itemImgFullPath)
										else
											itemImgPath = "/productpics/big"
											itemImgFullPath = itemImgPath & "/" & itemPic
											itemImgAbsolutePath = Server.MapPath(itemImgFullPath)
										end if
										
										if not fsImg.FileExists(itemImgAbsolutePath) then
											itemPic = "imagena.jpg"
											if musicSkins = 1 then
												itemImgPath = "/productpics/musicSkins/musicSkinsDefault"
												itemImgFullPath = itemImgPath & "/" & defaultImg
												if fsImg.FileExists(Server.MapPath(itemImgFullPath)) then
													itemSamplePath = "/productpics/musicSkins/musicSkinsSample"
													if fsImg.FileExists(Server.MapPath(itemSamplePath & "/" & replace(defaultImg,"artwork-thumbnail.jpg","sample-devices.jpg"))) then
														sampleImg = replace(defaultImg,"artwork-thumbnail.jpg","sample-devices.jpg")
													end if
													
													objItemImgFull = "<img itemprop=""image"" src=""http://www.wirelessemporium.com/" & itemImgFullPath & """ border=""0"" title=""" & replace(insertDetails(altText), "'", "") & """ id=""imgLarge"">"
										%>
											<div id="imgLarge-location" style="height:300px; cursor:pointer;" <%=strZoom%>><%=objItemImgFull%></div>
										<%
												else
													objItemImgFull = 	"<img itemprop=""image"" src=""http://www.wirelessemporium.com/" & itemImgFullPath & """ width=""300"" border=""0"" title=""" & replace(insertDetails(altText), "'", "") & """ id=""imgLarge"">"
										%>
											<div id="imgLarge-location" style="height:300px; cursor:pointer;" <%=strZoom%>><%=objItemImgFull%></div>
										<%
												end if
											else ' non-music skins, itemImgAbsolutePath doesn't exist
											%>
											<div id="imgLarge-location" style="height:300px; cursor:pointer;" <%=strZoom%>>
											<%
												objItemImgFull = "<div style=""width:300px; display:block; position: relative; z-index:1;"">"
												objItemImgFull = objItemImgFull & "<div style=""display:block; position:absolute; top:0px; left:0px; z-index:2;""><img itemprop=""image"" src=""http://www.wirelessemporium.com/productpics/big/" & itempic & """ width=""300"" height=""300"" border=""0"" title=""" & replace(insertDetails(altText), "'", "") & """ id=""imgLarge""></div>"
												if blnIsNotOriginalPrice then
													objItemImgFull = objItemImgFull & "<div style=""display:block; position: absolute; top:0px; right:0px; z-index:3;""><img src=""/images/LargeStar.png"" alt=""Sale Price"" title=""Sale Price"" border=""0"" /></div>"
												end if
												
												if hasFlashFile then
'													objItemImgFull = objItemImgFull & "<div style=""cursor:pointer; display:block; position: absolute; top:250px; right:-70px; z-index:3;""><img id=""id_view360"" onClick=""playFlash();"" src=""/images/product/360/360-clicktoview-button.png"" alt=""360 image"" title=""360 image"" border=""0"" /></div>"
												end if
												objItemImgFull = objItemImgFull & "</div>"
												
												response.write objItemImgFull
											%>
											</div>
											<%
											end if
										else 'itemImgAbsolutePath exists
										%>
											<div id="imgLarge-location" style="height:300px; cursor:pointer;" <%=strZoom%>>
												<% 
													objItemImgFull = "<div style=""width:300px; display:block; position: relative; z-index:2;"">"
													if musicSkins = 1 then 
														objItemImgFull = "<img itemprop=""image"" src=""http://www.wirelessemporium.com/" & itemImgFullPath & """ width=""300"" border=""0"" alt=""" & replace(altText, "'", "") & """ id=""imgLarge"">"
													else
														objItemImgFull = objItemImgFull & "<div style=""display:block; position:absolute; top:0px; left:0px; z-index:3;""><img itemprop=""image"" src=""http://www.wirelessemporium.com/productpics/big/" & itempic & """ width=""300"" height=""300"" border=""0"" title=""" & replace(insertDetails(altText), "'", "") & """ id=""imgLarge""></div>"
														if blnIsNotOriginalPrice then
															objItemImgFull = objItemImgFull & "<div style=""display:block; position:absolute; top:0px; right:0px; z-index:4;""><img src=""/images/LargeStar.png"" alt=""Sale Price"" title=""Sale Price"" border=""0"" /></div>"
														end if
														if customize then
															objItemImgFull = objItemImgFull & "<div style='position:absolute; right:0px; top:0px; z-index:5;'><a href='/custom/?aspid=" & itemID & "'><img src='/images/icons/customize.png' border='0' /></a></div>"
														end if
														
														if hasFlashFile then
'															objItemImgFull = objItemImgFull & "<div style=""cursor:pointer; display:block; position:absolute; top:250px; right:-70px; z-index:3;""><img id=""id_view360"" onClick=""playFlash();"" src=""/images/product/360/360-clicktoview-button.png"" alt=""360 image"" title=""360 image"" border=""0"" /></div>"
														end if
													end if 
													objItemImgFull = objItemImgFull & "</div>"

													response.write objItemImgFull
												%>
											</div>
										<%
										end if
										
										if customize then
										%>
                                        	<div><a href="/custom/?aspid=<%=itemID%>"><img src="/images/icons/customize-cta.jpg" border="0" /></a></div>
                                        <%
										end if
										
                                        if fs.fileExists(server.MapPath("/productpics/big/zoom/" & itempic)) then
                                        %>
                                        <div align="center"><a href="javascript:showFloatingZoomImage();"><img src="/images/product/Click-to-Zoom.jpg" border="0" /></a></div>
                                        <%
                                        end if

										if numAvailColors > 1 then
										%>
										<div align="center" style="padding-top:10px;">
											<div style="display:inline-block; font-style:italic;" align="center">CURRENT COLOR:&nbsp;</div>
											<%=generateColorPickerText(arrColors, colorSlaves, colorid, "font-weight:bold; text-align:left;")%>
										</div>
										<div align="center" style="padding-bottom:10px;"><%=generateColorPicker(arrColors, itemid, colorSlaves, colorid, numAvailColors, blnIsNotOriginalPrice)%></div>
										<div style="display:none;" id="preloading_deck"></div>
										<%
										end if
										%>
										<div id="imgLarge-location-pool" style="display:none;"><%=replace(objItemImgFull, "itemprop=""image""", "")%></div>
                                        <div id="imgAlt-location">
										<%
                                        dim iCount, path, src, a, strAltImage
                                        a = 0
                                        for iCount = 0 to 2
                                            path = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".jpg")
											if fs.FileExists(Server.MapPath(path)) then
                                                a = a + 1
                                                src = replace(path,".jpg","_thumb.jpg")
                                                'on error resume next
                                                set f1 = fs.getFile(server.MapPath(path))
                                                if fs.FileExists(Server.MapPath(src)) then
                                                    set f2 = fs.getFile(server.MapPath(src))
                                                else
                                                    set f2 = fs.getFile(server.MapPath(path))
                                                end if
                                                if fs.FileExists(Server.MapPath(src)) and f1.DateLastModified < f2.DateLastModified then
                                                    strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('" & path & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
                                                else
                                                '	response.write path & "<br><br>"
                                                '	response.write "fs.FileExists(Server.MapPath(path)):" & fs.FileExists(Server.MapPath(path)) & "<br><br>"
                                                '	response.write Server.MapPath(path) & "<br><br>"
                                                    session("errorSQL") = "filePath:" & Server.MapPath(path)
													if fs.FileExists(Server.MapPath(path)) then
														jpeg.Open Server.MapPath(path)
    	                                                jpeg.Height = 40
        	                                            jpeg.Width = 40
            	                                        jpeg.Save Server.MapPath(src)
                	                                    strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('" & path & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
                    	                                response.Write("<!-- make new alt image:" & a & " -->")
													end if
                                                end if
                                                'if err.number <> 0 then
                                                '	response.write "err.description: <b>" & err.description & "</b><br>"
                                                '	response.end
                                                'end if
                                            end if
                                            path = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".gif")
                                            if fs.FileExists(Server.MapPath(path)) then
                                                a = a + 1
                                                src = path
                                                strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('" & src & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
                                            end if
                                            path = "/productpics/AltViews/" & replace(itempic,".gif","-" & iCount & ".jpg")
                                            if fs.FileExists(Server.MapPath(path)) then
                                                a = a + 1
                                                src = path
                                                strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('" & src & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
                                            end if
                                            path = "/productpics/AltViews/" & replace(itempic,".gif","-" & iCount & ".gif")
                                            if fs.FileExists(Server.MapPath(path)) then
                                                a = a + 1
                                                src = path
                                                strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('" & src & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
                                            end if
                                        next
                                        if typeID = 17 then
                                            a = a + 1
                                            if vendor = "DS" then
                                                strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/decal_pic5.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/decal_pic5.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
                                            else
                                                strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/gg_atl_image1.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/gg_atl_image1.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
                                            end if
                                        end if
                                        'decal skins new alt image by Terry (11/9/11)
                                        if vendor = "DS" then
                                            strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/decalskin-mg.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/decalskin-mg.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
                                        end if
                                        if musicSkins = 1 then
'																samplePath = ""
'																if sampleImg <> "" then
'																	samplePath = itemSamplePath & "/" & sampleImg
'																end if
                                            a = a + 1
                                            strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/ms_altview1.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/ms_altview1.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
                                            a = a + 1
                                            strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/ms_altview2.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/ms_altview2.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
                                        end if
                                        if strAltImage <> "" then
											%>
                                            <p class="altImgText">ROLLOVER ANY IMAGE TO VIEW FULL-SIZE</p>
											<%
											if isnull(showAnimation) then showAnimation = true end if
											'if lCase(left(partnumber,3)) = "fp2" or lCase(left(partnumber,3)) = "fp3" then
                                            if (lCase(left(partnumber,3)) = "fp2" or lCase(left(partnumber,3)) = "fp3") and showAnimation then
                                                a = a + 1
                                                strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/FP-SNAP-ON-ANIM.gif');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb2"" src=""/productpics/AltViews/FP-SNAP-ON-ANIM_icon.gif"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
                                                strAltImage = strAltImage & "<script language=""javascript"">" & vbcrlf
                                                strAltImage = strAltImage & "if (document.images) {" & vbcrlf
                                                strAltImage = strAltImage & "newLargeImg = new Image();" & vbcrlf
                                                strAltImage = strAltImage & "newLargeImg.src = '/productpics/AltViews/FP-SNAP-ON-ANIM.gif'"
                                                strAltImage = strAltImage & "}" & vbcrlf
                                                strAltImage = strAltImage & "</script>" & vbcrlf
                                            end if
                                            response.write strAltImage
                                        else
                                            response.write "&nbsp;"
                                        end if
                                        %>
                                        </div>
                                        <div style="padding-top:10px;" id="otherStyles"></div>
                                        <%
										if typeID = 16 and SquaretradeItemCondition = "0" then
											%>
											<div style="padding-top:5px;">
												<script type="text/javascript" src="/Squaretrade/Squaretrade3/quoteWidget-custom.js"></script>
												<span id="squaretradewidget"></span>
												<script type="text/javascript">
													st_widget.create({itemPrice: '<%=price_Our%>', itemCondition : 'New', itemDescription : '<%=insertDetails(itemDesc)%>'});
												</script>
											</div>
											<%
										end if
										%>
                                    </div>
                                    <div id="fullBuyNowBox" style="float:right; width:430px;">
                                    	<form name="frmSubmit" action="/cart/item_add.asp" method="post" style="margin:0px; padding:0px;">
                                        <div id="buyNow2" style="float:left; width:420px; border:1px solid #CCC; background-color:#ebebeb; padding:5px; border-radius:15px;">
                                            <div style="height:50px;">
                                                <% if bFinalOutofStock or instr(partnumber,"MLD-") > 0 or instr(partnumber,"MS-") > 0 or instr(partnumber,"HYP-") > 0 then %>
                                                <div style="float:left; width:100px; height:40px; border-right:2px groove #fff; font-weight:bold; font-size:10px; text-align:left;"><img src="/images/product/bullet.gif" width="13" height="14" border="0" align="absmiddle">&nbsp;Ships within<br />24-48 hours</div>
                                                <% else %>
                                                <div style="float:left; width:100px; height:40px; border-right:2px groove #fff; font-weight:bold; font-size:12px; text-align:left; padding-top:10px;">
                                                	<div style="float:left;"><img src="/images/product/bullet.gif" width="13" height="14" border="0" align="absmiddle"></div>
                                                    <div id="inStock" style="float:left; padding-left:10px;">In Stock</div>
                                                    <div id="limitedStock" style="float:left; color:#C00; font-size:12px;">Limited Supply<br />Order Today</div>
                                                </div>
                                                <% end if %>
                                                <div style="float:left; padding-top:3px;"><!--#include virtual="/includes/asp/inc_ProductRating2.asp"--></div>
                                            </div>
                                            <div style="border-top:2px groove #fff; height:85px;">
                                                <div id="qtyBox" style="float:left; border-right:2px groove #fff; padding-top:10px;">
                                                    <% if not bFinalOutofStock then %>
                                                    <div style="width:100px; text-align:center;">Quantity</div>
                                                    <div style="width:100px; text-align:center; padding-top:5px;"><input type="text" name="qty" value="1" size="5" /></div>
                                                    <% end if %>
                                                    <div id="satBadge" style="width:100px; text-align:center; padding-top:50px;"><img src="/images/product/seal_90day.png" width="95" height="95" border="0" alt="90 Day Satisfaction Guarantee"></div>
                                                </div>
                                                <div id="priceDetails" style="float:left; padding-top:10px; height:75px;">
                                                    <div style="height:20px; border-bottom:1px solid #CCC; width:300px; margin-left:10px;">
                                                        <div style="float:left; font-size:13px; font-weight:bold;">Regular Price:</div>
                                                        <div style="float:right; text-decoration:line-through; font-size:12px;"><%=formatCurrency(prepInt(price_Retail))%></div>
                                                    </div>
                                                    <% if prepInt(price_Retail) > 0 then %>
                                                    <div style="height:20px; border-bottom:1px solid #CCC; width:300px; margin-left:10px; padding-top:5px;">
                                                        <div style="float:left; font-size:13px; font-weight:bold;">You Save:</div>
                                                        <div style="float:right; font-size:12px;">(<%=formatPercent((prepInt(price_Retail) - price_Our) / prepInt(price_Retail),0)%> OFF!) <%=formatCurrency(prepInt(price_Retail) - price_Our)%></div>
                                                    </div>
                                                    <% end if %>
                                                    <div style="height:20px; border-bottom:1px solid #CCC; width:300px; margin-left:10px; padding-top:5px;">
                                                        <div style="float:left; font-size:13px; font-weight:bold;">Shipping:</div>
                                                        <div style="float:right; font-size:12px; color:#FF6600; font-weight:bold;">FREE</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="height:100px; padding-top:5px;">
                                                <div id="confedenceBox" style="float:left; width:200px;">
                                                    <div><img src="/images/icons/header-shop-with-con.jpg" border="0" width="199" height="29" /></div>
                                                    <div style="background-color:#FFF; border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc; border-bottom-left-radius:10px; border-bottom-right-radius:10px; width:197px; height:75px;">
                                                        <div style="float:left; width:100%; height:12px; padding-top:5px;">
                                                            <div style="float:left; height:12px; padding:5px 10px 0px 5px;"><img src="/images/icons/orange-arrow-white-bg.jpg" width="8" height="8" /></div>
                                                            <div style="float:left; width:170px; height:12px; font-weight:bold;"><% if typeID = 16 then xDayNoHassleReturns = "30" else xDayNoHassleReturns = "90" end if %><%=xDayNoHassleReturns%>s-Day No Hassle-Returns</div>
                                                        </div>
                                                        <div style="float:left; width:100%; height:12px; padding-top:5px;">
                                                            <div style="float:left; height:12px; padding:5px 10px 0px 5px;"><img src="/images/icons/orange-arrow-white-bg.jpg" width="8" height="8" /></div>
                                                            <div style="float:left; width:170px; height:10px; font-weight:bold;">Everyday Low Price</div>
                                                        </div>
                                                        <div style="float:left; width:100%; height:12px; padding-top:5px;">
                                                            <div style="float:left; height:12px; padding:5px 10px 0px 5px;"><img src="/images/icons/orange-arrow-white-bg.jpg" width="8" height="8" /></div>
                                                            <div style="float:left; width:170px; height:10px; font-weight:bold;">Free Shipping On All Purchases</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <% if not bFinalOutofStock then %>
                                                <div id="addToCart" style="float:right; width:215px;">
                                                    <div style="float:right; width:100%; padding-right:10px;">
                                                        <div style="float:right; font-size:24px; font-weight:bold; color:#f00;"><%=formatCurrency(price_Our)%></div>
                                                        <div style="float:right; font-size:24px; font-weight:bold; padding-right:10px;">Our Price:</div>
                                                    </div>
                                                    <div style="float:right; width:100%; text-align:right; padding:5px 10px 0px 0px;">
                                                        <input type="image" src="/images/product/pp_buybutton.png" onclick="return addToCart();" width="162" height="36" border="0" alt="Add to Cart">
                                                    </div>
                                                </div>
                                                <% else %>
                                                <div id="addToCart" style="float:right; text-align:center; font-weight:bold; font-size:18px; color:#F00; width:230px;">THIS ITEM IS CURRENTLY<br />OUT OF STOCK</div>
                                                <% end if %>
                                                <div id="trustLogos" style="float:right; width:300px; padding-right:10px; margin-top:10px;"><img src="/images/icons/trustLogos.jpg" border="0" /></div>
                                            </div>
                                            <div id="featureBox1" style="float:left; width:100%; border-top:2px groove #fff; margin:10px 0px 5px 0px;">
                                                <div style="float:left; width:100%; padding:10px 0px 10px 5px;"><img src="/images/icons/Features.jpg" border="0" width="85" height="14" /></div>
                                                <%
                                                featureListArray = split(featureList,"##")
												
                                                for i = 0 to ubound(featureListArray)
                                                %>
                                                <div style="float:left; margin:5px 0px 0px 5px; width:100%;">
                                                <%
													if prepStr(featureListArray(i)) <> "" then
                                                %>
                                                    <div style="float:left; width:12px; padding-top:3px;"><img src="/images/icons/featurePoint.gif" border="0" /></div>
                                                    <div style="float:left; width:400px; text-align:left; font-size:12px; padding-left:5px;"><%=featureListArray(i)%></div>
                                                <%
                                                    end if
                                                %>
                                                </div>
                                                <%
                                                next
												
												if customize then
												%>
                                                <div style="float:left; padding:5px 0px 0px 5px; width:100%; border-top:1px solid #333; margin-top:5px;">
                                                    <div style="float:left; width:400px; text-align:left; font-size:12px; padding-left:5px; font-weight:bold;">Customizing this product? Please allow up to 5-7 business days for production of custom cases prior to shipping time.</div>
                                                </div>
                                                <%
												end if
                                                %>
                                            </div>
                                        </div>
                                        	<input type="hidden" name="musicSkins" value="<%=musicSkins%>" />
                                            <input type="hidden" name="prodid" value="<%=itemID%>">
                                        </form>
                                        <div id="featureBox3" style="float:left; width:420px; margin-top:10px;">
                                        	<div style="background:url(/images/product/grey_header_bar.jpg) repeat-x; width:100%; float:left;">
                                                <div style="float:left;"><img src="/images/product/header-feat.jpg" border="0" /></div>
                                                <div style="float:right;"><img src="/images/product/grey_header_right.jpg" border="0" /></div>
                                            </div>
                                            <div style="float:left; width:100%; padding:10px 0px 10px 0px; border-left:1px solid #CCC; border-right:1px solid #CCC; border-bottom:1px solid #CCC; border-bottom-left-radius:15px; border-bottom-right-radius:15px; margin-bottom:10px;">
                                                <%
                                                if musicSkins = 1 then
                                                %>
                                                <ul>
                                                    <li>Thin material wraps around your <%=brandName%>&nbsp;<%=modelName%> without adding any bulk</li>
                                                    <li>Provides basic layer of protection from scratches and dust</li>
                                                    <li>Premium grade vinyl material and high gloss finish for a stunning look</li>
                                                    <li>Custom cut and non-permanent</li>
                                                    <li>NOT REUSABLE</li>
                                                </ul>
                                                <%
                                                else																	
                                                    featureListArray = split(featureList,"##")
                                                    
                                                    for i = 0 to ubound(featureListArray)
                                                %>
                                                <div style="float:left; margin:5px 0px 0px 20px; width:100%;">
                                                <%
                                                        if prepStr(featureListArray(i)) <> "" then
                                                %>
                                                    <div style="float:left; width:12px; font-size:16px; padding-top:3px;"><img src="/images/icons/featurePoint.gif" border="0" /></div>
                                                    <div style="float:left; width:340px; text-align:left; font-size:12px; padding-left:5px;"><%=featureListArray(i)%></div>
                                                <%
                                                        end if
                                                %>
                                                </div>
                                                <%
                                                    next
                                                end if
                                                %>
                                            </div>
                                        </div>
                                        <% if not isDropship(partnumber) then %>
                                        <div style="float:left; width:300px; margin:10px 0px 0px 60px;"><div name="sr_productDetailDiv"></div></div>
                                        <% end if %>
                                        <div id="otherStyles3" style="float:left; width:420px;"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <!--Tab-->
							<tr>
                                <td valign="top">
                                    <table width="100%" class="smlText" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td width="100%" align="left" valign="top">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr id="newDetails">
                                                    	<td style="padding:10px;">
                                                        	<%
															if len(replace(featureList,"#","")) > 0 then
																bFeatureList = true
															%>
                                                            <div id="featureBox2" style="float:left;">
                                                                <div style="background:url(/images/product/grey_header_bar.jpg) repeat-x; width:100%; float:left;">
                                                                    <div style="float:left;"><img src="/images/product/header-feat.jpg" border="0" /></div>
                                                                    <div style="float:right;"><img src="/images/product/grey_header_right.jpg" border="0" /></div>
                                                                    <div style="float:right; padding-top:4px;">
                                                                        <!-- Pin it -->
                                                                        <script type="text/javascript">
                                                                            (function() {
                                                                                window.PinIt = window.PinIt || { loaded:false };
                                                                                if (window.PinIt.loaded) return;
                                                                                window.PinIt.loaded = true;
                                                                                function async_load(){
                                                                                    var s = document.createElement("script");
                                                                                    s.type = "text/javascript";
                                                                                    s.async = true;
                                                                                    if (window.location.protocol == "https:")
                                                                                        s.src = "https://assets.pinterest.com/js/pinit.js";
                                                                                    else
                                                                                        s.src = "http://assets.pinterest.com/js/pinit.js";
                                                                                    var x = document.getElementsByTagName("script")[0];
                                                                                    x.parentNode.insertBefore(s, x);
                                                                                }
                                                                                if (window.attachEvent)
                                                                                    window.attachEvent("onload", async_load);
                                                                                else
                                                                                    window.addEventListener("load", async_load, false);
                                                                            })();
                                                                        </script>
                                                                        <a target="_blank" href="http://pinterest.com/pin/create/button/?url=<%=canonicalURL%>&media=http://www.wirelessemporium.com<%=itemImgFullPath%>" class="pin-it-button" count-layout="none">
                                                                            <img src="/images/product/pinit_pre.jpg" border="0" width="42" height="20" alt="Pinterest" />
                                                                        </a>
                                                                    </div>
                                                                    <div style="float:right; padding:4px 10px 0px 0px;">
                                                                        <div style="position:relative;" onmouseover="document.getElementById('id_500_email').style.display=''" onmouseout="document.getElementById('id_500_email').style.display='none'">
                                                                            <div id="id_500_email" style="display:none; position:absolute; bottom:20px; right:-100px;" onmouseover="document.getElementById('id_500_email').style.display=''" onmouseout="document.getElementById('id_500_email').style.display='none'">
                                                                                <a href="#" onclick="show500Help(); return false;"><img src="/images/500/WE-BUCKS-PDP-HOVER-EMAIL.png" border="0" /></a>
                                                                            </div>
                                                                            <script type="text/javascript">
                                                                                try {
                                                                                    var fullTitle = document.title;
                                                                                    var ffImageUrl = "http://www.wirelessemporium.com<%=itemImgFullPath%>";
                                                                                    var ffProductName = "<%=insertDetails(itemDesc)%>";
                                                                                    var ffProducturl = "<%=canonicalURL%>";
                                                                                    var ffMessage = "Check out this product " + ffProductName + " on Wireless Emporium";
                                                                                }
                                                                                catch (e) {}
                                                                            </script>
                                                                            <script type="text/javascript">
                                                                                _ffLoyalty.displayWidget("buBY2zrrDT", {
                                                                                    message: ffMessage,
                                                                                    url: ffProducturl,
                                                                                    image_url: ffImageUrl,
                                                                                    title: fullTitle,
                                                                                    description: ffProductName
                                                                            });
                                                                            </script>
                                                                        </div>
                                                                    </div>
                                                                    <div style="float:right; padding:4px 10px 0px 0px;">
                                                                        <div style="position:relative;" onmouseover="document.getElementById('id_500_twitter').style.display=''" onmouseout="document.getElementById('id_500_twitter').style.display='none'">
                                                                            <div id="id_500_twitter" style="display:none; position:absolute; bottom:20px; right:-100px;" onmouseover="document.getElementById('id_500_twitter').style.display=''" onmouseout="document.getElementById('id_500_twitter').style.display='none'">
                                                                                <a href="#" onclick="show500Help(); return false;"><img src="/images/500/WE-BUCKS-PDP-HOVER-TWITTER.png" border="0" /></a>
                                                                            </div>                                                                
                                                                            <script type="text/javascript">
                                                                                try {
                                                                                    var tProductName = "<%=insertDetails(itemDesc)%>";
                                                                                    if (tProductName.length >= 70) tProductName = tProductName.substring(0,67) + '...'
                                                                                    var tMessage = "Check out this product [" + tProductName + "] on Wireless Emporium";
                                                                                    var tProducturl = "<%=canonicalURL%>";
                                                                                }
                                                                                catch (e) {}
                                                                            </script>
                                                                            <script type="text/javascript">
                                                                                _ffLoyalty.displayWidget("TuBYAs1fyg", {
                                                                                    message: tMessage, url: tProducturl
                                                                                });
                                                                            </script>
                                                                        </div>
                                                                    </div>
                                                                    <div style="float:right; padding:0px 10px 0px 0px;"><img src="/images/Earn-WE-BUCKS.png" border="0" /></div>
                                                                </div>
                                                                <div style="float:left; width:100%; padding:10px 0px 10px 0px; border-left:1px solid #CCC; border-right:1px solid #CCC; border-bottom:1px solid #CCC; border-bottom-left-radius:15px; border-bottom-right-radius:15px; margin-bottom:10px;">
                                                                    <%
                                                                    if musicSkins = 1 then
                                                                    %>
                                                                    <ul>
                                                                        <li>Thin material wraps around your <%=brandName%>&nbsp;<%=modelName%> without adding any bulk</li>
                                                                        <li>Provides basic layer of protection from scratches and dust</li>
                                                                        <li>Premium grade vinyl material and high gloss finish for a stunning look</li>
                                                                        <li>Custom cut and non-permanent</li>
                                                                        <li>NOT REUSABLE</li>
                                                                    </ul>
                                                                    <%
                                                                    else																	
                                                                        featureListArray = split(featureList,"##")
                                                                        
                                                                        for i = 0 to ubound(featureListArray)
                                                                    %>
                                                                    <div style="float:left; margin-left:20px; width:100%;">
                                                                    <%
                                                                            if prepStr(featureListArray(i)) <> "" then
                                                                    %>
                                                                        <div style="float:left; width:12px; font-size:16px; padding-top:3px;"><img src="/images/icons/featurePoint.gif" border="0" /></div>
                                                                        <div style="float:left; width:340px; text-align:left; font-size:12px; padding-left:5px;"><%=featureListArray(i)%></div>
                                                                    <%
                                                                                i = i + 1
                                                                                if prepStr(featureListArray(i)) <> "" then 
                                                                    %>
                                                                        <div style="float:left; width:12px; font-size:16px; padding-top:3px;"><img src="/images/icons/featurePoint.gif" border="0" /></div>
                                                                        <div style="float:left; width:340px; text-align:left; font-size:12px; padding-left:5px;"><%=featureListArray(i)%></div>
                                                                    <%
                                                                                end if
                                                                            end if
                                                                    %>
                                                                    </div>
                                                                    <%
                                                                        next
                                                                    end if
                                                                    %>
                                                                </div>
                                                            </div>
                                                            <% end if %>
                                                            <div style="background:url(/images/product/grey_header_bar.jpg) repeat-x; width:100%; float:left;">
                                                                <div style="float:left;"><img src="/images/product/header-desc.jpg" border="0" /></div>
                                                                <div style="float:right;"><img src="/images/product/grey_header_right.jpg" border="0" /></div>
                                                                <div id="descTags" style="float:right;">
                                                                	<div style="float:right;padding-top:4px;">
                                                                        <g:plusone size="medium" annotation="none"></g:plusone>
                                                                        <script type="text/javascript">
                                                                          (function() {
                                                                            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                                                            po.src = 'https://apis.google.com/js/plusone.js';
                                                                            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                                                                          })();
                                                                        </script>
                                                                    </div>
                                                                    <div style="float:right; padding:4px 10px 0px 0px;">
                                                                        <!-- Pin it -->
                                                                        <script type="text/javascript">
                                                                            (function() {
                                                                                window.PinIt = window.PinIt || { loaded:false };
                                                                                if (window.PinIt.loaded) return;
                                                                                window.PinIt.loaded = true;
                                                                                function async_load(){
                                                                                    var s = document.createElement("script");
                                                                                    s.type = "text/javascript";
                                                                                    s.async = true;
                                                                                    if (window.location.protocol == "https:")
                                                                                        s.src = "https://assets.pinterest.com/js/pinit.js";
                                                                                    else
                                                                                        s.src = "http://assets.pinterest.com/js/pinit.js";
                                                                                    var x = document.getElementsByTagName("script")[0];
                                                                                    x.parentNode.insertBefore(s, x);
                                                                                }
                                                                                if (window.attachEvent)
                                                                                    window.attachEvent("onload", async_load);
                                                                                else
                                                                                    window.addEventListener("load", async_load, false);
                                                                            })();
                                                                        </script>
                                                                        <a target="_blank" href="http://pinterest.com/pin/create/button/?url=<%=canonicalURL%>&media=http://www.wirelessemporium.com<%=itemImgFullPath%>" class="pin-it-button" count-layout="none">
                                                                            <img src="/images/product/pinit_pre.jpg" border="0" width="42" height="20" alt="Pinterest" />
                                                                        </a>
                                                                    </div>
                                                                    <div style="float:right; padding:4px 10px 0px 0px;">
                                                                        <div style="position:relative;" onmouseover="document.getElementById('id_500_email2').style.display=''" onmouseout="document.getElementById('id_500_email2').style.display='none'">
                                                                            <div id="id_500_email2" style="display:none; position:absolute; bottom:20px; right:-100px;" onmouseover="document.getElementById('id_500_email2').style.display=''" onmouseout="document.getElementById('id_500_email2').style.display='none'">
                                                                                <a href="#" onclick="show500Help(); return false;"><img src="/images/500/WE-BUCKS-PDP-HOVER-EMAIL.png" border="0" /></a>
                                                                            </div>
                                                                            <script type="text/javascript">
                                                                                try {
                                                                                    var fullTitle = document.title;
                                                                                    var ffImageUrl = "http://www.wirelessemporium.com<%=itemImgFullPath%>";
                                                                                    var ffProductName = "<%=insertDetails(itemDesc)%>";
                                                                                    var ffProducturl = "<%=canonicalURL%>";
                                                                                    var ffMessage = "Check out this product " + ffProductName + " on Wireless Emporium";
                                                                                }
                                                                                catch (e) {}
                                                                            </script>
                                                                            <script type="text/javascript">
                                                                                _ffLoyalty.displayWidget("buBY2zrrDT", {
                                                                                    message: ffMessage,
                                                                                    url: ffProducturl,
                                                                                    image_url: ffImageUrl,
                                                                                    title: fullTitle,
                                                                                    description: ffProductName
                                                                            });
                                                                            </script>
                                                                        </div>
                                                                    </div>
                                                                    <div style="float:right; padding:4px 10px 0px 0px;">
                                                                        <div style="position:relative;" onmouseover="document.getElementById('id_500_twitter2').style.display=''" onmouseout="document.getElementById('id_500_twitter2').style.display='none'">
                                                                            <div id="id_500_twitter2" style="display:none; position:absolute; bottom:20px; right:-100px;" onmouseover="document.getElementById('id_500_twitter2').style.display=''" onmouseout="document.getElementById('id_500_twitter2').style.display='none'">
                                                                                <a href="#" onclick="show500Help(); return false;"><img src="/images/500/WE-BUCKS-PDP-HOVER-TWITTER.png" border="0" /></a>
                                                                            </div>                                                                
                                                                            <script type="text/javascript">
                                                                                try {
                                                                                    var tProductName = "<%=insertDetails(itemDesc)%>";
                                                                                    if (tProductName.length >= 70) tProductName = tProductName.substring(0,67) + '...'
                                                                                    var tMessage = "Check out this product [" + tProductName + "] on Wireless Emporium";
                                                                                    var tProducturl = "<%=canonicalURL%>";
                                                                                }
                                                                                catch (e) {}
                                                                            </script>
                                                                            <script type="text/javascript">
                                                                                _ffLoyalty.displayWidget("TuBYAs1fyg", {
                                                                                    message: tMessage, url: tProducturl
                                                                                });
                                                                            </script>
                                                                        </div>
                                                                    </div>
                                                                    <div style="float:right; padding:0px 10px 0px 0px;"><img src="/images/Earn-WE-BUCKS.png" border="0" /></div>
                                                                </div>
                                                            </div>
                                                            <div style="float:left; width:100%; padding:10px 0px 0px 0px; border-left:1px solid #CCC; border-right:1px solid #CCC; border-bottom:1px solid #CCC; border-bottom-left-radius:15px; border-bottom-right-radius:15px; margin-bottom:10px;">
                                                            	<div id="prodDescription" style="width:700px; text-align:left; padding:0px 10px 0px 10px; overflow:auto; font-size:14px;">
																<%
																if musicSkins = 1 then
																	if fsImg.FileExists(Server.MapPath(itemSamplePath & "/" & sampleImg)) then
																		session("errorSQL") = "openPic:" & Server.MapPath(itemSamplePath & "/" & sampleImg)
																		jpeg.Open Server.MapPath(itemSamplePath & "/" & sampleImg)
																		if jpeg.OriginalWidth > 300 then
																			jpeg.Width = 300
																			jpeg.Height = jpeg.OriginalHeight * 300 / jpeg.OriginalWidth
																			jpeg.Save Server.MapPath(itemSamplePath & "/" & sampleImg)
																		end if
																%>
																<div style="float:left; margin:0px 20px 15px 20px;"><img src="<%=itemSamplePath & "/" & sampleImg%>" border="0" /></div>
																<%
																	end if
																%>
																<%=brandName%>&nbsp;<%=modelName%> Music Skin <%=insertDetails(itemDesc)%> - There is no better way to personalize your phone than with 
																a Music Skin. Music skins are a thin, "sticker-like" covering made from a patented 3M material that wraps around your 
																entire phone, helping to prevent scratches and dust from marring the look of your phone. Easy to apply and non-permanent, 
																Music Skins are custom cut to fit your phone and add virtually no bulk at all to your phone. Even with your phone wrapped 
																in a Music Skin you can use it in conjunction with any case, holster or charging dock.
																<br /><br />
																<ul>
																	<li>Thin material wraps around your <%=brandName%>&nbsp;<%=modelName%> without adding any bulk</li>
																	<li>Provides basic layer of protection from scratches and dust</li>
																	<li>Premium grade vinyl material and high gloss finish for a stunning look</li>
																	<li>Custom cut and non-permanent</li>
																	<li>NOT REUSABLE</li>
																</ul>
																<br /><br />
																<img src="/images/musicskin/detail.jpg" border="0" width="380" />
																<img src="/images/musicskin/detail2.jpg" border="0" width="380" />
																<br /><br />
																YouTube Video = <a href="http://www.youtube.com/watch?v=Ri3LhsUcqZs" target="_blank">http://www.youtube.com/watch?v=Ri3LhsUcqZs</a>
																<br /><br />
																<iframe title="YouTube video player" width="640" height="390" src="http://www.youtube.com/embed/Ri3LhsUcqZs" frameborder="0" allowfullscreen></iframe>
																<br /><br />
																<%
																else
																	if instr(strItemLongDetail,"Buy this quality") > 0 then
																		useLongDesc = left(strItemLongDetail,instr(strItemLongDetail,"Buy this quality")-1)
																	elseif instr(strItemLongDetail,"Remember - WirelessEmporium.com") > 0 then
																		useLongDesc = left(strItemLongDetail,instr(strItemLongDetail,"Remember - WirelessEmporium.com")-1)
																	else
																		useLongDesc = strItemLongDetail
																	end if
																	response.write "<span itemprop=""description""><p style=""margin-top:0px;"">" & insertDetails(useLongDesc) & "</p></span>" & vbcrlf
																end if
																if typeID = 16 then
																	response.write "<div style=""font-size:14px; font-weight:bold;"">PHONE FEATURES:</div>" & vbcrlf
																	if BULLET1 <> "" or BULLET2 <> "" or BULLET3 <> "" or BULLET4 <> "" or BULLET5 <> "" or BULLET6 <> "" or BULLET7 <> "" or BULLET8 <> "" or BULLET9 <> "" or BULLET10 <> "" then
																		response.write "<ul style=""margin-top:0px;"">" & vbcrlf
																		if BULLET1 <> "" then response.write "<li>" & insertDetails(BULLET1) & "</li>" & vbcrlf
																		if BULLET2 <> "" then response.write "<li>" & insertDetails(BULLET2) & "</li>" & vbcrlf
																		if BULLET3 <> "" then response.write "<li>" & insertDetails(BULLET3) & "</li>" & vbcrlf
																		if BULLET4 <> "" then response.write "<li>" & insertDetails(BULLET4) & "</li>" & vbcrlf
																		if BULLET5 <> "" then response.write "<li>" & insertDetails(BULLET5) & "</li>" & vbcrlf
																		if BULLET6 <> "" then response.write "<li>" & insertDetails(BULLET6) & "</li>" & vbcrlf
																		if BULLET7 <> "" then response.write "<li>" & insertDetails(BULLET7) & "</li>" & vbcrlf
																		if BULLET8 <> "" then response.write "<li>" & insertDetails(BULLET8) & "</li>" & vbcrlf
																		if BULLET9 <> "" then response.write "<li>" & insertDetails(BULLET9) & "</li>" & vbcrlf
																		if BULLET10 <> "" then response.write "<li>" & insertDetails(BULLET10) & "</li>" & vbcrlf
																		response.write "</ul>" & vbcrlf
																	end if
																	if COMPATIBILITY <> "" then
																		response.write "<p><div style=""font-size:14px; font-weight:bold;"">COMPATIBILITY:</div>" & COMPATIBILITY & "</p>" & vbcrlf
																	end if
																	%>
																	<p><div style="font-size:14px; font-weight:bold;"> WHAT IS INCLUDED: </div><%=PackageContents%></p>
																	<p><em>Wireless Emporium offers direct replacement and unlocked Cell/PDA/Smart Phones to you WITHOUT ANY CONTRACTS! Order your replacement or unlocked Cell/PDA/Smart Phones today and we'll ship it out to your home or business for FREE!</em></p>
																	<p style="font-size:14px; font-weight:bold;"> Click Here: ** <a href="javascript:Open_Popup('/phone_return_policy.asp','PhoneReturnPolicy','width=550,height=650,scrollbars=no');" style="font-size:14px;font-weight:bold;color:#CC3300;text-decoration:none;">Cell Phone Return Policy</a> **</p>
																	<%
																else
																	if typeID = 1 or typeID = 2 or typeID = 6 or typeID = 7 then
																		'response.write "<h5><i>All factory-direct Wireless Emporium Cell Phone " & nameSEO(categoryName) & " are manufactured to the highest ISO 9001:2000 certified quality standards.</i></h5>" & vbcrlf 'Removed for Penguin update
																	end if
																end if
																if not isnull(itemDimentions) and itemDimentions <> "" then
																	response.write "<p style=""margin-top:0px;"">Product Dimensions: <span style=""font-size:13px; font-weight:bold;"">" & itemDimentions & "</span></p>" & vbcrlf
																end if
																
																if typeID = 18 then
																%>
                                                                <div style="">
                                                                	<iframe width='640' height='360' src='http://www.youtube.com/embed/N0JZfqNk0Vk?rel=0&showinfo=1&modestbranding=1&wmode=opaque' frameborder='0' allowfullscreen></iframe>
                                                                </div>
                                                                <div style="padding:15px 0px 15px 0px; cursor:pointer;" onclick="showScreenProtectorInfo();">
                                                                	<img src="/images/product/New-button-screenprotector-infographic.png" border="0" width="710" />
																</div>
																<%
																end if
																if typeID = 1 then
																%>
                                                                <div style="">
                                                                	<iframe width='640' height='360' src='http://www.youtube.com/embed/R7e5niCL0kE?rel=0&showinfo=1&modestbranding=1&wmode=opaque' frameborder='0' allowfullscreen></iframe>
                                                                </div>
																<%
																end if
																%>
                                                                <%
																if customize and instr(strItemLongDetail,"/productTabs/custom_1.jpg") < 1 then
																%>
                                                                <div align=center>
                                                                <a href=/custom/?aspid=<%=itemID%>><img src="/images/productTabs/custom_1.jpg" border="0"></a>
                                                                </div>
                                                                <br />
                                                                Customize your device with your own distinctive flair! An amazing gift for any <%=brandName & " " & modelName%> owner, WirelessEmporium's cases can be fully personalized with any pictures and text. Transform this impact-resistant case into a work of art with a design that is vibrantly printed in full color using advanced ECO-UV inks. 100% satisfaction guaranteed!
                                                                <br /><br />
                                                                <div align=center>
                                                                <img src="/images/productTabs/pdp-hori-div.png">
                                                                <br /><br />
                                                                <iframe width=640 height=360 src=http://www.youtube.com/embed/epi2_d2PhUA?feature=player_detailpage frameborder=0 allowfullscreen></iframe>
                                                                <br /><br />
                                                                <img src="/images/productTabs/pdp-hori-div.png">
                                                                <br /><br />
                                                                <img src="/images/productTabs/custom-pdp-graphic.png">
                                                                </div>
                                                                <% end if %>
                                                                <%
																response.write "<p style=""margin-top:0px;"">Part&nbsp;Number:&nbsp;<b>" & partnumber & "</b></p>" & vbcrlf
																response.write "<p style=""margin-top:0px;"">Item&nbsp;ID:&nbsp;<b>" & itemid & "</b></p>" & vbcrlf
																if download_URL <> "" then response.write "<p><a href=""" & download_URL & """ target=""_blank"">" & download_TEXT & "</a></p>" & vbcrlf																										
																%>
                                                                </div>
                                                            </div>
                                                            <div style="background:url(/images/product/grey_header_bar.jpg) repeat-x; width:100%; float:left;">
                                                                <div style="float:left;"><img src="/images/product/header-comp.jpg" border="0" /></div>
                                                                <div style="float:right;"><img src="/images/product/grey_header_right.jpg" border="0" /></div>
                                                            </div>
                                                            <div style="float:left; width:100%; height:200px; overflow:auto; border-left:1px solid #CCC; border-right:1px solid #CCC; border-bottom:1px solid #CCC; border-bottom-left-radius:15px; border-bottom-right-radius:15px; margin-bottom:10px;">
                                                            	<div style="float:left; width:650px; padding-left:10px;">
																	<%
                                                                    if COMPATIBILITY <> "" then 
                                                                        response.write "<p><b>COMPATIBILITY:</b>&nbsp;&nbsp;" & COMPATIBILITY & "</p>" & vbcrlf
                                                                    else
                                                                        response.write "<p><b>COMPATIBILITY:</b>&nbsp;&nbsp;" & modelName & "</p>" & vbcrlf	
                                                                    end if
                                                                    %>
                                                                </div>
                                                            </div>
                                                            <div id="youMayLike" style="float:left;">
                                                                <div style="background:url(/images/product/grey_header_bar.jpg) repeat-x; width:100%; float:left;">
                                                                    <div style="float:left;"><img src="/images/icons/header-you-may-also-like.jpg" border="0" /></div>
                                                                    <div style="float:right;"><img src="/images/product/grey_header_right.jpg" border="0" /></div>
                                                                </div>
                                                                <div style="float:left; width:100%; height:60px; overflow:auto; border-left:1px solid #CCC; border-right:1px solid #CCC; border-bottom:1px solid #CCC; border-bottom-left-radius:15px; border-bottom-right-radius:15px;">
                                                                    <div id="otherStyles2" style="float:left; width:650px; padding:5px 0px 0px 10px;"></div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="21%" align="left" valign="top">&nbsp;</td>
                                        </tr>
                                        <tr><td align="center"><!--#include virtual="/includes/asp/inc_ProductReview.asp"--></td>
										<tr><td style="padding: 10px 0px 10px;" align="center"><!--#include virtual="/includes/asp/inc_qna.asp"--></td>
                                    </table>
                                </td>
                            </tr>
                            <!--
							<tr>
                            	<td>
                                	<%
'									response.Write(" select"& strSqlTopN &VbCrLf&_
'		" 	Nickname, " &VbCrLf&_
'		" 	ReviewTitle, " &VbCrLf&_
'		" 	Review, " &VbCrLf&_
'		" 	Rating," &VbCrLf&_
'		" 	DateTimeEntd" &VbCrLf&_
'		" from "& dicTableReview( GetSiteId()) &" with(nolock)" &VbCrLf&_
'		ReviewCondition( strItemId) &VbCrLf&_
'		strSortOrderClause)
									%>
                                </td>
                            </tr>
                            -->                            
                            <%
                            dim strItemCheck
                            strItemCheck = itemID
                            %>
                            <!--#include virtual="/includes/asp/inc_CrossSells_layout.asp"-->
                            <tr>
                                <td align="left" valign="top">&nbsp;</td>
                            </tr>
                            <!--#include virtual="/includes/asp/inc_recent_items.asp"-->
                            <tr>
                                <td align="left" valign="top">&nbsp;</td>
                            </tr>
                            <%if typeID = 116 then%>
                            <tr>
                                <td width="748">
                                    ProductWiki:<br>
                                    <%
                                    'pwAPI.MPN = itemid			'MPN of the product
                                    pwAPI.UPC = UPCCode			'UPC of the product
                                    pwAPI.canonicalURL = "http://www.wirelessemporium.com/p-" & itemid & "-" & formatSEO(insertDetails(itemDesc)) & ".asp"		'The canonical URL of the SKU
                                    pwAPI.title = insertDetails(itemDesc)		'Title of the SKU
                                    pwAPI.price = price_Our		'(Optional) Price of the SKU
                                    pwAPI.render()				'render the output to the page
                                    %>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">&nbsp;</td>
                            </tr>
                            <%end if%>
                        </table>
                        </div>
                        <!-- // rich snippets mark up -->
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!--
    <tr>
        <td align="center" style="padding:10px 0px 10px 0px;">
            <!-- begin ad tag -->
            <script type="text/javascript">
            //<![CDATA[
//            ord=Math.random()*10000000000000000;
//            document.write(unescape("%3Cscript src='" + (location.protocol=="https:" ? "https://" : "http://") + "ad.doubleclick.net/adj/tmn.wirelessemporium/;sz=728x90;ord=" + ord + "?' type='text/javascript'%3E%3C/script%3E"));
            //]]>
            </script>
            <!--<noscript><a href="http://ad.doubleclick.net/jump/tmn.wirelessemporium/;sz=728x90;ord=123456789?" target="_blank" ><img src="http://ad.doubleclick.net/ad/tmn.wirelessemporium/;sz=728x90;ord=123456789?" border="0" alt="" /></a></noscript>-->
            <!-- end ad tag -->
        </td>
    </tr>
    <%
	recaptcha_public_key	= "6Ld2C74SAAAAAG_CoH_enQM1_uKABFbiftwRnxyW" ' your public key
	recaptcha_private_key	= "6Ld2C74SAAAAADoSV8ZEMd2SBztaSvrXBYcs59rQ" ' your private key	
	%>
    <%''' **************** Start Comment Card ********************* %>
    <tr>
        <td width="100%">
            <form id="frmComment"  name="frmComment">
            <div style="float:left; width:748px;">
                <div style="float:left; width:160px; height:200px; vertical-align:top;"><img src="/images/icons/WE_comment_image.png" alt="" border="0" width="160" /></div>
                <div style="float:left; width:546px; height:320px; background-color:#ebebeb; border:1px solid #999; padding:5px 20px 0px 20px;">
                    <div style="float:left; width:546px;">
                        <span style="font-size:22px; font-weight:bold;">SUGGESTION BOX</span><br/>
                        We work hard to make sure our site offers the best shopping experience possible. If you have any 
                        suggestions on how we can make things even better - whatever it is, please drop us a note!
                    </div>
                    <div style="float:left; width:546px; margin-top:10px;">
                        <div style="float:left; width:134px; text-align:right; font-weight:bold;">Email (optional):</div>
                        <div style="float:left; width:380px; text-align:left; padding-left:10px;"><input type="text" name="email" value="" size="30" /></div>
                    </div>
                    <div style="float:left; width:546px; margin-top:10px;">
                        <div style="float:left; width:134px; text-align:right; font-weight:bold;">Phone (optional):</div>
                        <div style="float:left; width:380px; text-align:left; padding-left:10px;"><input type="text" name="phone" value="" size="30" /></div>
                    </div>
                    <div style="float:left; width:546px; margin-top:10px;" align="left"><TEXTAREA style="width:540px; height:50px;" id="textarea1" name="textarea1"></TEXTAREA></div>
			        <div style="float:left; margin-top:5px; width:440px; background-color:#fff;"><div id="div_recaptcha"></div></div>                    
                    <div style="float:right; margin:45px 0px 0px 5px;"><img src="/images/buttons/WE_submit.png" alt="" style="cursor:pointer;" onclick="return checkall();" border="0" width="100" height="26" /></div>
                </div>
            </div>
            </form>
        </td>
    </tr>
    <%''' **************** End Comment Card ********************* %>
    <!--
    <%if typeID <> 16 then%>
    <tr>
        <td class="breadcrumbFinal">
            <a class="breadcrumb" href="/">Cell Phone Accessories</a>&nbsp;&#183;&nbsp;<a class="breadcrumb" href="/<%=categorySEO(typeID)%>">Cell Phone <%=nameSEO(categoryName)%></a>&nbsp;&#183;&nbsp;<a class="breadcrumb" href="/sc-<%=typeID & "-sb-" & brandID & "-" & formatSEO(brandName) & "-" & formatSEO(categoryName)%>.asp"><%=brandName & " Cell Phone " & nameSEO(categoryName)%></a>&nbsp;&#183;&nbsp;<a class="breadcrumb" href="/sc-<%=typeID & "-sb-" & brandID & "-sm-" & modelID & "-" & formatSEO(categoryName) & "-for-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp"><%=brandName & " " & modelName & " " & nameSEO(categoryName)%></a>&nbsp;&#183;&nbsp;<%=insertDetails(itemDesc)%>
        </td>
    </tr>
    <%end if%>
    -->
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>

<%
if hasFlashFile then
%>
	<!--#include virtual="/includes/asp/inc_360view.asp"-->
<% 
end if
%>

<!--#include virtual="/includes/template/bottom_product.asp"-->
<script>
/***********************************************
* Scrollable content Script- ?Dynamic Drive (www.dynamicdrive.com)
* This notice must stay intact for use
* Visit http://www.dynamicdrive.com/ for full source code
***********************************************/

var nsstyle='display:""'
if (document.layers)
var scrolldoc=document.scroll1.document.scroll2
function up(){
if (!document.layers) return
if (scrolldoc.top<0)
scrolldoc.top+=10
temp2=setTimeout("up()",50)
}
function down(){
if (!document.layers) return
if (scrolldoc.top-150>=scrolldoc.document.height*-1)
scrolldoc.top-=10
temp=setTimeout("down()",50)
}

function clearup(){
if (window.temp2)
clearInterval(temp2)
}

function cleardown(){
if (window.temp)
clearInterval(temp)
}
</script>

<%
function StaffReviews(PartNumber)
	if PartNumber = "BT1-BLU-00N9-01" then
		StaffReviews = "Can You Hear Me Now?<br><br>"
		StaffReviews = StaffReviews & "If you've been shopping WirelessEmporium.com for a Bluetooth headset, you've probably seen our most affordable offering�the Bluedio N9. At just under $20 (shipped for free, of course!) you've got to wonder�is it any good?<br><br>"
		StaffReviews = StaffReviews & "This Bluetooth headset comes with just the essentials�the headset itself, a plastic over-the-ear hook, a wall charger, and a user manual. I charged the headset for a couple of hours then paired it with no problems; hold the multi-function button down until the LED is solid blue and enter ""0000"" into the phone.<br><br>"
		StaffReviews = StaffReviews & "<b>Functionality</b> of the device is good. The multi-function button allows the user to power the device on and off, answer/end calls, activate voice dialing (dependent on your phone's functionality), re-dial, and call waiting. There are no dedicated volume buttons so you'd have to adjust this on your handheld.<br><br>"
		StaffReviews = StaffReviews & "The <b>look and feel</b> of the headset is decent. It's not the fanciest looking Bluetooth on the market, but it is lightweight and comfortable to wear. The headset was very secure in my ear even without the ear hook; I could run and shake my head violently without the headset falling out.<br><br>"
		StaffReviews = StaffReviews & "The issue of <b>call quality</b> is split down the middle. On the receiving end, speech comes in loud and clear. However, outgoing voice was not so impressive. I had to raise my voice for the caller to hear me, and when I spoke quickly, I sounded mumbled. Road noise wasn't a major issue when I used the headset in my car and neither was regular street noise. Overall, call quality is pedestrian.<br><br>"
		StaffReviews = StaffReviews & "The Bluedio N9 suffers the same pitfall as other budget Bluetooth headsets�mediocre outgoing call quality. If you're on a budget and are set on getting a Bluetooth, the N9 doesn't perform any worse than headsets that cost $20 more. But if you're tight on money and don't want to jeopardize call quality, get a wired headset (we carry these for nearly all phone models). They are much cheaper, don't require charging, and have reliable call quality.  Remember: hands-free laws don't require that you use a Bluetooth, just a hands-free device.<br><br>"
	elseif left(PartNumber,3) = "FP2" or left(PartNumber,3) = "FP3" or left(PartNumber,3) = "LC5" then
		StaffReviews = "Protecting Your Phone<br><br>"
		StaffReviews = StaffReviews & "Let's face it: your phone is going to see some hard times. You have it with you all day. It gets picked up, put down, thrown in bags and pockets, and occasionally dropped. Most phone owners want to preserve the condition of their phones, and there's no better way to do that than with a case. I've tested three protective cases to give you the lowdown on what to expect from a snap-on faceplate, snap-on rubberized case, and silicone case.<br><br>"
		StaffReviews = StaffReviews & "The <b>snap-on faceplate</b> really is easy to install. Pressing down firmly at a few points around the case created a very secure closure. This case does add some bulk to the phone which could be a plus or negative, depending on how you look at it. There were cutouts for everything I may need access to including the volume rocker, charging port, speaker, camera, and audio jack. My only complaint is that the case impedes text input on my particular phone model; it is difficult to hit the outer keys of my QWERTY keyboard because of the raised edges of the case. However, this is overcome with practice and by re-training my thumbs.<br><br>"
		StaffReviews = StaffReviews & "The <b>rubberized snap-on case</b> is much like the faceplate: same installation method, same basic structure with the necessary cutouts, and similar bulk added to the phone. I didn't have quite the same issue with text input as I did with the faceplate; this case is slightly thinner. The main difference, of course, is the rubberized feel, which I definitely refer over the plastic feel. The biggest negative for me, however, was that the rubberized case made it a bit difficult to slide in out of my pocket. This won't be an issue for people who wear looser fitting pants or carry their phone in a carrying pouch or purse.<br><br>"
		StaffReviews = StaffReviews & "Both snap-on cases may seem tricky to remove but the instructions are simple: find two spots on either side of the case and pull away from each other. Depending on the setup of your case, it might require two pulls at different points. This does require some force but don't worry�your case won't break. Remember, these are ISO-certified products so quality and durability is guaranteed.<br><br>"
		StaffReviews = StaffReviews & "The <b>silicone case</b> is quite different from the first two cases. It easily slips onto the phone and the additional weight is hardly noticeable. There are cutouts for anything that requires them: speaker, camera, LED indicator, audio jack, and charging port. Silicone actually covers side buttons like the volume rocker, which is great for keeping dust and lint out of those crevices. The silicone doesn't creep up on the keyboard like the snap-on cases, a big plus. However, this case is even harder to slide in and out of my pocket, and that sadly makes it a deal breaker.<br><br>"
		StaffReviews = StaffReviews & "The final verdict? Though I like the feel of the rubberized case and the protection of the silicone case, it's crucial for me to be able to easily slide my phone in and out of my pocket. The winner: the snap-on faceplate. Having dozens of choices between colors and designs is a big plus, too. I strongly recommend complementing your protective case with a <a href=""http://www.wirelessemporium.com/p-10848-universal-pda---cell-phone-screen-protector.asp"">screen protector</a> for full protection."
	elseif left(PartNumber,3) = "CC1" or left(PartNumber,3) = "CC2" or left(PartNumber,3) = "CC3" or left(PartNumber,3) = "CC4" then
		StaffReviews = "Which to Choose For On-the-Go Charging?<br><br>"
		StaffReviews = StaffReviews & "We here at Wireless Emporium offer nothing but quality cell phone accessories, so buying a product that will last is guaranteed; the real challenge is choosing which is right for you.  A car charger is essential to any heavy cell phone user on the go, but which is best�the standard, heavy-duty, retractable, or OEM? We've tested the products so you can make an informed purchase.<br><br>"
		StaffReviews = StaffReviews & "The <b>standard car charger</b> proved to be quite impressive. With only fifteen minutes of charging time, it replenished <b>20%</b> of my phone's battery. As the most affordable car charger, this is definitely a good buy.<br><br>"
		StaffReviews = StaffReviews & "The <b>heavy-duty car charger</b> performed the best out of the four, replenishing <b>25%</b> of my phone's battery in just fifteen minutes. It also feels very solid and has the longest and thickest cord of the four. We don't call it heavy-duty for nothing!<br><br>"
		StaffReviews = StaffReviews & "The <b>retractable car charger</b> added <b>10%</b> to my battery's power bar after fifteen minutes. It accomplished the least, sure, but it outdoes the others in another way. With just the press of a button, the cord winds up into the charger, making it easy to unplug and stow away in my glove compartment or middle console.<br><br>"
		StaffReviews = StaffReviews & "The <b>OEM car charger</b> managed to restore <b>15%</b> of my phone's battery in the same fifteen-minute window. Cord length and thickness falls right between the standard and heavy-duty car chargers.<br><br>"
		StaffReviews = StaffReviews & "Of course, it all boils down to what you need and personal preference. Is a quick charge all you're looking for or do you want to keep your car as wire-free as possible? Stick with a brand name or a more efficient non-OEM model? Personally, I use the heavy-duty car charger because I appreciate the durable build. I also keep an <a href=""http://wirelessemporium.com/p-18147-ac-power-supply-quick-charger-adapter.asp"">AC/DC adapter</a> handy so I get the most out of my car charger, plugging it into a wall when I need to. Whichever car charger you choose, Wireless Emporium will back it for a full year and ship it to you for free, so you've got nothing to lose!"
	else
		StaffReviews = ""
	end if
end function
%>
<script type="text/javascript" src="/includes/js/dropdown.js"></script>
<script type="text/javascript" src="/includes/js/tabcontent.js"></script>
<script type="text/javascript" src="/includes/js/recaptcha_ajax.js"></script>
<script language="javascript">
	function closeFloatingBox() {
		if (document.getElementById('prodDescription') != null) document.getElementById('prodDescription').style.display = '';

		//------------------ change it to original -------------------//
		document.getElementById("popBox").style.position = "fixed";
		document.getElementById("popCover").onclick = function () {}
		document.getElementById("popBox").onclick = function () {}
		//------------------------------------------------------------//
				
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";

		document.getElementById("popCover").innerHTML = "";
		document.getElementById("popBox").innerHTML = "";
	}

	function showScreenProtectorInfo() {
		viewPortW = $(window).width();
		window.scrollTo(0, 0);
		if (document.getElementById('prodDescription') != null) document.getElementById('prodDescription').style.display = 'none';	
		ajax("/ajax/ajaxScreenProtectorInfographic.asp?screenW="+viewPortW,'popBox');
		document.getElementById("popBox").style.position = "absolute";
		document.getElementById("popCover").onclick = function () { closeFloatingBox() }
		document.getElementById("popBox").onclick = function () { closeFloatingBox() }
		
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}

	function showFloatingZoomImage() {
		viewPortH = $(window).height(); //get browser's viewport height.
		itemid = document.frmSubmit.prodid.value;
		ajax("/ajax/ajaxZoomImage.asp?itemid=" + itemid + "&screenH="+viewPortH,'popBox');
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	
	function show500Help() {
		ajax("/ajax/ajax500.asp",'popBox');
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}	
	
	function fnSwapZoomImage(imgSrc) {
		document.getElementById('id_img_zoom').src = imgSrc;
	}

	function showRecaptcha() { 
		 Recaptcha.create("<%=recaptcha_public_key%>", 'div_recaptcha', {theme: "clean"});
	}

	(function () {
		setTimeout("showRecaptcha()",500)
	}());


	<% if numAvailColors > 1 then %>
	setTimeout("ajax('/ajax/ajaxSwapProductColor.asp?uPage=p&uType=pre&itemid=<%=itemid%>', 'preloading_deck')",500)

	function preloading(pItemID, selectedColorID){}
	function changeProduct(pItemID, selectedColorID, numColors, selectedIDX, onSale)
	{
		for(i=0; i < numColors; i++) 
		{
			if (document.getElementById('div_colorOuterBox_' + pItemID + '_' + i) != null) document.getElementById('div_colorOuterBox_' + pItemID + '_' + i).className = 'colorBoxUnselected';
			if (document.getElementById('id_colorText_' + i) != null) document.getElementById('id_colorText_' + i).style.display = 'none';
		}
		document.getElementById('div_colorOuterBox_' + pItemID + '_' + selectedIDX).className = 'colorBoxSelected';
		document.getElementById('id_colorText_' + selectedIDX).style.display = 'inline-block';

		if (document.getElementById('prodDetails1') != null) 
			document.getElementById('prodDetails1').innerHTML = document.getElementById('new_logdesc_' + selectedIDX).innerHTML;
			
		if ((document.getElementById('prodDescription') != null) && (document.getElementById('new_logdesc_' + selectedIDX) != null)) 
			document.getElementById('prodDescription').innerHTML = document.getElementById('new_logdesc_' + selectedIDX).innerHTML;

		document.getElementById('imgLarge-location-pool').innerHTML = '';
		ajax('/ajax/ajaxSwapProductColor.asp?uPage=p&uType=img&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&onsale=' + onSale + '&hasflash=<%=hasFlashFile%>', 'imgLarge-location-pool');
		setTimeout('checkUpdate()', 50);

		ajax('/ajax/ajaxSwapProductColor.asp?uPage=p&uType=altimg&itemid=' + pItemID + '&selectedColorID=' + selectedColorID, 'imgAlt-location');
		ajax('/ajax/ajaxSwapProductColor.asp?uPage=p&uType=title&itemid=' + pItemID + '&selectedColorID=' + selectedColorID, 'id_h1');
	}
	
	function checkUpdate()
	{
		if ("" == document.getElementById('imgLarge-location-pool').innerHTML) {
			setTimeout('checkUpdate()', 50);
		}
		else 
		{
			document.getElementById('imgLarge-location').innerHTML = document.getElementById('imgLarge-location-pool').innerHTML;
			document.frmSubmit.prodid.value = document.getElementById('new_itemid').innerHTML;
		}
	}
	<% end if %>

	function fnPreviewImage(imgSrc) {
		var objToDisplay = '<img src="' + imgSrc + '" border="0" width="300" />';
		document.getElementById('imgLarge-location').innerHTML = objToDisplay;
	}
	function fnDefaultImage() {
		document.getElementById('imgLarge-location').innerHTML = document.getElementById('imgLarge-location-pool').innerHTML;
	}
	
	var commentLoop = 0
	var commentReturn
	var commentValue = '';
	
	function changeProdTab(num) {
		for (i=0;i<=5;i++) {
			if (document.getElementById("prodTabLink" + i) != null) {
				document.getElementById("prodDetails" + i).style.display = 'none';
				document.getElementById("prodTabLink" + i).className = '';				
			}
		}
		document.getElementById("prodDetails" + num).style.display = '';
		document.getElementById("prodTabLink" + num).className = 'selected';
	}
	
	function Open_Popup(theURL,winName,features) {
		window.open(theURL,winName,features);
	}
	
	<% ''' *********** For Comments Validations -- After validation it will send the request to server using with ajax%>
	function checkall() {	 
	  var GreForm = this.document.frmComment;
		
		if (GreForm.textarea1.value=="") {
			alert("Enter Your Comments");
			GreForm.textarea1.focus();
			return false;
		}
		
		ajax('/ajax/saveCommentCard.asp?itemID=<%=itemID%>&email=' + document.frmComment.email.value + '&phone=' + document.frmComment.phone.value + '&comment=' + document.frmComment.textarea1.value + '&re_challenge=' + Recaptcha.get_challenge() + '&re_response=' + Recaptcha.get_response() + '&re_privatekey=<%=recaptcha_private_key%>','dumpZone');
		commentValue = GreForm.textarea1.value;
		GreForm.textarea1.value = "";
		
		setTimeout("chkCommentReturn()",500)
		
		return true;
	}
	
	function chkCommentReturn() {
		commentLoop++
		if (commentLoop < 5) {
			if (document.getElementById("dumpZone").innerHTML != "") {
				if (document.getElementById("dumpZone").innerHTML == "wrong recaptcha") {
					document.getElementById('textarea1').value = commentValue;
					alert('You have entered the wrong secret words.');
				}
				else if (document.getElementById("dumpZone").innerHTML == "commentGood") {
					alert("Comment saved")
				}
				else {
					alert("Error saving comment\nPlease resubmit\n" + document.getElementById("dumpZone").innerHTML)
					document.getElementById('textarea1').value = commentValue
				}
			}
		}
		else {
			alert("Error saving comment\nPlease resubmit")
			document.getElementById('textarea1').value = commentValue
		}
	}
	<% if typeID = 16 then %>
	setTimeout("checkReview()",2000)
	function checkReview() {
		var testVal = document.getElementById("expertReview2").innerHTML
		var testLen = testVal.length - 10
		for (i=0; i<testLen; i++) {
			if (testVal.substring(i,i+9) == "stars_new") {
				document.getElementById("expertReview1").style.display = ''
				document.getElementById("expertReview2").style.display = ''
			}
		}
	}
	<% end if %>
	setTimeout("otherStyle()",3000)
	
	function otherStyle() {
		ajax('/ajax/ajaxProduct.asp?partNumber=<%=partNumber%>&modelID=<%=modelID%>&itemID=<%=itemID%>&typeID=<%=typeID%>&HandsfreeType=<%=HandsfreeType%>','otherStyles')
		setTimeout("cleanOtherStyle()",2000)
	}
	
	function cleanOtherStyle() {
		var curStyleCode = document.getElementById('otherStyles').innerHTML
		document.getElementById('otherStyles3').innerHTML = curStyleCode
		curStyleCode = curStyleCode.replace('OTHER STYLES TO CHOOSE FROM:','')
		document.getElementById('otherStyles2').innerHTML = curStyleCode
	}
</script>
