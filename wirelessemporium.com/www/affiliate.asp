<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;Affiliate Alliance Program
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle" width="100%">
            <h1>Affiliate Alliance Program</h1>
        </td>
    </tr>
    <tr>
        <td>
            <p>
                <div align="justify">
                    The Wireless Emporium Affiliate Alliance program is an exclusive, industry-leading affiliate program designed with generous commission rates and
                    courteous, responsive service to assist in maximizing revenue for qualified affiliate publishers. Since 2001, Wireless Emporium has provided 1000's of
                    customers quality cell phone accessories including chargers, batteries, faceplates, cases, data cables and much more from the leading cell phone brands
                    including Blackberry, Motorola, Nokia, Samsung, LG and more all at cost-plus pricing. All products are manufacturer-direct and come with a 100%
                    satisfaction guarantee. In addition to easy, secure online ordering, rapid fulfillment, and fast FREE 1st class shipping, Wireless Emporium is able to
                    offer the best total value in wireless accessories online.
                </div>
            </p>
            <p>
                <div align="justify">
                    As an affiliate partner, take advantage of a generous commission structure designed to produce an efficient revenue stream that can be easily
                    maximized with higher volume. Couple that with a superior site-wide close rate and all the advertising material at your disposal and you have a powerful,
                    profitable partnership that is sure to be a leading revenue generator for your site!
                </div>
            </p>
            <p>
                <div align="justify">
                    Example transaction:
                    <ol style="margin-top:0px;">
                        <li>Place a Wireless Emporium ad on your site</li>
                        <li>Deliver traffic to corresponding page on WirelessEmporium.com</li>
                        <li>Let us close the sale for you with our industry-leading site-wide close rate! (ex: $100 order)</li>
                        <li>Receive your commission!</li>
                    </ol>
                </div>
            </p>
            <p>
                <div align="justify">
                    Additional bonuses and customized commission tiers are available based on affiliate performance and volume. As an Affiliate Alliance partner you can
                    expect the highest level of service from our experienced marketing team, ready to help you optimize your ads for more sales. Leverage the power of
                    the Wireless Emporium brand as a part of your affiliate arsenal or even as a stand-alone affiliate partner site. Join us today!
                </div>
            </p>
            <p>
                PepperJAM Network Click Here:<br>
                <a href="https://www.pepperjamnetwork.com/affiliate/registration.php?refid=228" target="_blank"><img src="/images/aboutus/signup.gif" width="100" height="20" border="0"></a>
            </p>
            <p>
                Commission Junction Click Here:<br>
                <a href="https://signup.cj.com/member/brandedPublisherSignUp.do?air_refmerchantid=1629329" target="_blank"><img src="/images/aboutus/signup.gif" width="100" height="20" border="0"></a>
            </p>
            <p>
                Google Affiliate Network Click Here:<br>
                <a href="https://www.connectcommerce.com/secure/partner_app.html?ccmid=21000000000268833" target="_blank"><img src="/images/aboutus/signup.gif" width="100" height="20" border="0"></a>
            </p>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->