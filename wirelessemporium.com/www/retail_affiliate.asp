<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;Affiliate Retailer Program
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle" width="100%">
            <h1>Affiliate Retailer Program</h1>
        </td>
    </tr>
    <tr>
        <td valign="top" align="center">
            <img src="/images/retailer_banner.jpg" width="765" height="105" hspace="10" vspace="2">
            <script language="javascript">
            <!-- Begin
            function CheckValidNEW(strFieldName, strMsg) {
                if (strFieldName == "" || strFieldName == null || strFieldName == "NULL" || strFieldName == " ") {
                    if (bValid) {
                        alert(strMsg);
                        bValid = false;
                    }
                }
            }
            function CheckSubmit() {
                var f = document.frmRetailAffiliate;
                bValid = true;
                CheckValidNEW(f.mt_first.value, "Your First Name is a required field!");
                CheckValidNEW(f.mt_last.value, "Your Last Name is a required field!");
                CheckValidNEW(f.mt_company.value, "Your Company Name is a required field!");
                CheckValidNEW(f.mt_title.value, "Your Title is a required field!");
                CheckValidNEW(f.mt_phone.value, "Your Phone Number is a required field!");
                CheckValidNEW(f.mt_address.value, "Your Address is a required field!");
                CheckValidNEW(f.mt_city.value, "Your City is a required field!");
                sString = f.mt_state.options[f.mt_state.selectedIndex].value;
                CheckValidNEW(sString, "Your State is a required field!");
                CheckValidNEW(f.mt_zip.value, "Your Zip Code is a required field!");
                if (bValid) {
                    f.action = "retail_affiliate.asp";
                    f.submit();
                }
            }
            //  End -->
            </script>
        </td>
    </tr>
    <tr>
        <td class="smlText">
            <h3 align="center"><b>Wireless Emporium Affiliate Retailer Program</b></h3>
            <%
            if request.form("submitted") <> "" then
                SQL = "INSERT INTO we_retail_affiliate (firstname,lastname,company,title,phone,fax,email,address,address2,city,state,zip,how,dateTimeEntd) VALUES ("
                SQL = SQL & "'" & SQLquote(request.form("mt_first")) & "',"
                SQL = SQL & "'" & SQLquote(request.form("mt_last")) & "',"
                SQL = SQL & "'" & SQLquote(request.form("mt_company")) & "',"
                SQL = SQL & "'" & SQLquote(request.form("mt_title")) & "',"
                SQL = SQL & "'" & SQLquote(request.form("mt_phone")) & "',"
                SQL = SQL & "'" & SQLquote(request.form("mt_fax")) & "',"
                SQL = SQL & "'" & SQLquote(request.form("mt_email")) & "',"
                SQL = SQL & "'" & SQLquote(request.form("mt_address")) & "',"
                SQL = SQL & "'" & SQLquote(request.form("mt_address2")) & "',"
                SQL = SQL & "'" & SQLquote(request.form("mt_city")) & "',"
                SQL = SQL & "'" & SQLquote(request.form("mt_state")) & "',"
                SQL = SQL & "'" & SQLquote(request.form("mt_zip")) & "',"
                SQL = SQL & "'" & SQLquote(request.form("mt_how")) & "',"
                SQL = SQL & "'" & now & "')"
                'response.write "<p>" & SQL & "</p>" & vbcrlf
                call fOpenConn()
                session("errorSQL") = SQL
                oConn.execute SQL
                call fCloseConn()
                
                strFrom = "sales@wirelessemporium.com"
                strTo = "ruben@wirelessemporium.com"
                strSubject = "New Retail Affiliate application."
                strBody = "<html><head>" & vbcrlf
                strBody = strBody & "<p>First Name: " & request.form("mt_first") & "</p>" & vbcrlf
                strBody = strBody & "<p>Last Name: " & request.form("mt_last") & "</p>" & vbcrlf
                strBody = strBody & "<p>Company: " & request.form("mt_company") & "</p>" & vbcrlf
                strBody = strBody & "<p>Title: " & request.form("mt_title") & "</p>" & vbcrlf
                strBody = strBody & "<p>Phone: " & request.form("mt_phone") & "</p>" & vbcrlf
                strBody = strBody & "<p>Fax: " & request.form("mt_fax") & "</p>" & vbcrlf
                strBody = strBody & "<p>Email: " & request.form("mt_email") & "</p>" & vbcrlf
                strBody = strBody & "<p>Address: " & request.form("mt_address") & "</p>" & vbcrlf
                strBody = strBody & "<p>Address 2: " & request.form("mt_address2") & "</p>" & vbcrlf
                strBody = strBody & "<p>City: " & request.form("mt_city") & "</p>" & vbcrlf
                strBody = strBody & "<p>State: " & request.form("mt_state") & "</p>" & vbcrlf
                strBody = strBody & "<p>Zip: " & request.form("mt_zip") & "</p>" & vbcrlf
                strBody = strBody & "<p>How Did You Hear About Us: " & request.form("mt_how") & "</p>" & vbcrlf
                strBody = strBody & "<p>Date/Time Entered: " & now & "</p>" & vbcrlf
                strBody = strBody & "</body></html>"
                CDOSend strTo,strFrom,strSubject,strBody
                
                response.write "<p>Thank you for taking the time to sign up for our Affiliate program.</p>" & vbcrlf
                response.write "<p>Within the next 24 hours your account representative will contact you by telephone to verify your account application.</p>" & vbcrlf
                response.write "<p>Once your account is confirmed you will receive your unique store code and information on how orders are shipped to your customers.</p>" & vbcrlf
                'response.write "<p>If you have any questions please contact us at 1-888-725-7575.</p>" & vbcrlf
            else
                %>
                <p align="justify">
                    <b><i>What is it?</i></b>
                    <br>
                    The <b>Wireless Emporium Affiliate Retailer Program</b> is an exclusive, innovative
                    program designed to <u>help retail store owners of all sizes leverage the depth of
                    product offering of the <i>#1 Online Retailer of wireless accessories online</i></u>.
                    We offer generous commission rates and courteous, responsive service to assist
                    in maximizing revenue for our Affiliate Retailer partners.
                </p>
                <p align="justify">
                    <b><i>How Does it Work?</i></b>
                    <br>
                    As a store owner, it is difficult to carry accessory items for each and every
                    phone that your customers may inquire about. This is where Wireless Emporium as
                    your fulfillment partner will help you service your customer on the spot! Don't
                    turn customers away to your competitors � <u>let Wireless Emporium help YOU close
                    each and every accessory sale</u>! We offer a vast array of accessories for
                    virtually ALL cell phones! <b>This is the easiest money you can generate for
                    your business</b>!
                </p>
                <p align="justify">
                    <i><u>Example Transaction:</u></i>
                    <ol style="margin-top:0px;">
                        <li>A customer enters your store looking for an accessory you do not stock</li>
                        <li>Place an order on <a href="http://www.WirelessEmporium.com">www.WirelessEmporium.com</a> for the accessories needed using your unique store code</li>
                        <li>Provide customer with delivery information. Orders can be shipped directly to your customer's home or your store location</li>
                        <li>A Wireless Emporium Account Manager keeps track of sales and commissions (<b><i>starting at 25% of GROSS sales</i></b>!)</li>
                        <li>Receive a check every month based on your sales!</li>
                    </ol>
                </p>
                <p align="justify">
                    <b><i>How Do I Join/Next Steps?</i></b>
                    <ol style="margin-top:0px;">
                        <li>Fill out and Submit the form below.</li>
                        <li>You <span style="color:red;">MUST</span> fill out all of the fields in red.</li>
                        <li>A Wireless Emporium Account Manager will contact you within 1 business day and provide details and issue you a unique store code.</li>
                        <li>Start earning commission with your referrals!</li>
                    </ol>
                </p>
                
                <p>&nbsp;</p>
                
                <form action="/retail_affiliate.asp" name="frmRetailAffiliate" method="post">
                    <table border="0" cellpadding="0" cellspacing="0" width="500" class="smlText">
                        <tr>
                            <td><p align="right" style="text-align:right;"><b><span style="color:red;">First Name:&nbsp;&nbsp;</span></b></p></td>
                            <td><p><input type="text" size="40" name="mt_first" maxlength="40"></p></td>
                        </tr>
                        <tr>
                            <td><p align="right" style="text-align:right;"><b><span style="color:red;">Last Name:&nbsp;&nbsp;</span></b></p></td>
                            <td><p><input type="text" size="40" name="mt_last" maxlength="40"></p></td>
                        </tr>
                        <tr>
                            <td><p align="right" style="text-align:right;"><b><span style="color:red;">Company/Store Name:&nbsp;&nbsp;</span></b></p></td>
                            <td><p><input type="text" size="40" name="mt_company" maxlength="40"></p></td>
                        </tr>
                        <tr>
                            <td><p align="right" style="text-align:right;"><b><span style="color:red;">Title:&nbsp;&nbsp;</span></b></p></td>
                            <td><p><input type="text" size="40" name="mt_title" maxlength="40"></p></td>
                        </tr>
                        <tr>
                            <td><p align="right" style="text-align:right;"><b><span style="color:red;">Phone:&nbsp;&nbsp;</span></b></p></td>
                            <td><p><input type="text" size="15" name="mt_phone" maxlength="20"></p></td>
                        </tr>
                        <tr>
                            <td><p align="right" style="text-align:right;"><b>Fax:&nbsp;&nbsp;</b></p></td>
                            <td><p><input type="text" size="15" name="mt_fax" maxlength="20"></p></td>
                        </tr>
                        <tr>
                            <td><p align="right" style="text-align:right;"><b>E-Mail Address:&nbsp;&nbsp;</b></p></td>
                            <td><p><input type="text" size="40" name="mt_email" maxlength="90"></p></td>
                        </tr>
                        <tr>
                            <td><p align="right" style="text-align:right;"><b><span style="color:red;">Address:&nbsp;&nbsp;</span></b></p></td>
                            <td><p><input type="text" size="40" name="mt_address" maxlength="90"></p></td>
                        </tr>
                        <tr>
                            <td><p align="right" style="text-align:right;"><b>Address 2:&nbsp;&nbsp;</b></p></td>
                            <td><p><input type="text" size="40" name="mt_address2" maxlength="90"></p></td>
                        </tr>
                        <tr>
                            <td><p align="right" style="text-align:right;"><b><span style="color:red;">City:&nbsp;&nbsp;</span></b></p></td>
                            <td><p><input type="text" size="40" name="mt_city" maxlength="40"></p></td>
                        </tr>
                        <tr>
                            <td><p align="right" style="text-align:right;"><b><span style="color:red;">State/Province:&nbsp;&nbsp;</span></b></p></td>
                            <td>
                                <p>
                                    <select name="mt_state">
                                        <%getStates("")%>
                                    </select>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td><p align="right" style="text-align:right;"><b><span style="color:red;">Zip:&nbsp;&nbsp;</span></b></p></td>
                            <td><p><input type="text" size="10" name="mt_zip" maxlength="10"></p></td>
                        </tr>
                        <tr>
                            <td><p align="right" style="text-align:right;"><b>How did you hear about us?&nbsp;&nbsp;</b></p></td>
                            <td><p><input type="text" size="40" name="mt_how" maxlength="200"></p></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <p>
                                    <input type="hidden" name="submitted" value="1">
                                    <input type="button" name="frmSubmit" value="Submit" onClick="CheckSubmit();">
                                </p>
                            </td>
                        </tr>
                    </table>
                </form>
                <%
            end if
            %>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->