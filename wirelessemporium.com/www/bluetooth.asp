<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
call permanentredirect("/bluetooth-headsets-handsfree.asp")

response.buffer = true
set fso = CreateObject("Scripting.FileSystemObject")

dim productListingPage
productListingPage = 1

call fOpenConn()
SQL = "SELECT A.itemID, A.itemDesc, A.itemPic, A.price_Our, A.price_Retail, A.ItemKit_NEW, B.modelName, B.modelImg, C.brandID, C.brandName"
SQL = SQL & " FROM we_items A"
SQL = SQL & " INNER JOIN we_models B ON A.modelID = B.modelID"
SQL = SQL & " INNER JOIN we_brands C ON A.brandid = C.brandid"
SQL = SQL & " WHERE (A.bluetooth = 1 OR HandsfreeType = 2)"
SQL = SQL & " AND A.hideLive = 0"
SQL = SQL & " AND A.inv_qty <> 0"
SQL = SQL & " AND A.price_Our > 0 "

dim nSorting
nSorting = request.form("nSorting")
select case nSorting
	case "1" : SQL = SQL & "ORDER BY A.numberofsales DESC"
	case "2" : SQL = SQL & "ORDER BY A.price_Our DESC"
	case "3" : SQL = SQL & "ORDER BY A.price_Our"
	case "4" : SQL = SQL & "ORDER BY A.brandid, A.DatetimeEntd DESC"
	case "5" : SQL = SQL & "ORDER BY A.brandid, A.DateTimeEntd"
	case "6" : SQL = SQL & "ORDER BY C.brandname"
	case "7" : SQL = SQL & "ORDER BY C.brandname DESC"
	case else : SQL = SQL & "ORDER BY A.numberofsales DESC"
end select
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
end if

dim brandName, brandID, modelName, modelImg
brandName = RS("brandName")
brandID = RS("brandID")
modelName = RS("modelName")
modelImg = RS("modelImg")

dim SEtitle, SEdescription, SEkeywords, topText
SEtitle = ""
SEdescription = ""
SEkeywords = ""
topText = "Free yourselves from tangled cords! With a Bluetooth product in your hands, you can sync, send, talk, dance, and more ?all wirelessly. Bluetooth wireless technology has expanded immensely. Whether you are working, playing, or moving, Bluetooth technology in your devices can make life easier for you. When you want the latest and greatest in Bluetooth accessories, you'll simply find no better value than at Wireless Emporium. Shop our Bluetooth accessories below!</p>"

dim strBreadcrumb
strBreadcrumb = "Bluetooth Accessories"
%>
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle" width="100%">
            <h1>Bluetooth Headsets</h1>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%">
            <table border="0" cellspacing="7" cellpadding="0">
                <tr>
                    <td align="center" valign="top" width="200">
                        <p class="topText">
                            <img src="/images/cats/cat-bluetooth.jpg" border="0" alt="Bluetooth Headsets">
                        </p>
                    </td>
                    <td align="center" valign="top" width="600">
                        <table border="0" cellspacing="0" cellpadding="5" width="541" class="cate-pro-border">
                            <tr>
                                <td>
                                    <p class="topText"><%=topText%></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">
            <table width="426" border="0" cellspacing="0" cellpadding="0">
                <form name="sortForm" method="post" action="/bluetooth.asp">
                <tr>
                    <td width="426" align="right" valign="middle">
                        Sort By:
                        <select name="nSorting" onChange="this.form.submit();" style="background-color:#e6f5fd;">
                            <option value="1"<%if nSorting = "1" or nSorting = "" then response.write " selected"%>>Best Selling</option>
                            <option value="2"<%if nSorting = "2" then response.write " selected"%>>Price: High to Low</option>
                            <option value="3"<%if nSorting = "3" then response.write " selected"%>>Price: Low to High</option>
                            <option value="4"<%if nSorting = "4" then response.write " selected"%>>Product: Newest to Oldest</option>
                            <option value="5"<%if nSorting = "5" then response.write " selected"%>>Product: Oldest to Newest</option>
                            <option value="6"<%if nSorting = "6" then response.write " selected"%>>Brand Name: A - Z</option>
                            <option value="7"<%if nSorting = "7" then response.write " selected"%>>Brand Name: Z - A</option>
                        </select>
                    </td>
                </tr>
                </form>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="798">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td align="center" valign="top" colspan="7"><img src="/images/line-hori.jpg" width="798" height="5" border="0"></td>
                            </tr>
                            <tr>
                                <%
                                dim altText
                                a = 0
                                dim DoNotDisplay, RSkit
                                do until RS.eof
                                    DoNotDisplay = 0
                                    if not isNull(RS("ItemKit_NEW")) then
                                        SQL = "SELECT inv_qty FROM we_Items WHERE itemID IN (" & RS("ItemKit_NEW") & ")"
                                        set RSkit = Server.CreateObject("ADODB.recordset")
                                        RSkit.open SQL, oConn, 0, 1
                                        do until RSkit.eof
                                            if RSkit("inv_qty") < 1 then DoNotDisplay = 1
                                            RSkit.movenext
                                        loop
                                        RSkit.close
                                        set RSkit = nothing
                                    end if
                                    '#######################
                                    if not fso.fileExists(server.MapPath("/productpics/thumb/" & RS("itemPic"))) then DoNotDisplay = 1
                                    if DoNotDisplay = 0 then
                                        altText = brandName & " " & modelName & " Bluetooth Headset ?" & RS("itemDesc")
                                        if categoryid = "14" and modelid = "0" then altText = "Cell Phone Charms & Bling : " & altText
                                        response.write "<td align=""center"" valign=""top"" width=""192"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""192"" height=""100%"">" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""middle"" width=""192"" height=""154""><a href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp""><img src=""/productpics/thumb/" & RS("itemPic") & """ border=""0"" alt=""" & altText & """></a></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""top""><a class=""product-description4"" href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"" title=""" & altText & """>" & RS("itemDesc") & "</a></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""top"" height=""8""><img src=""/images/spacer.gif"" width=""1"" height=""8""></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""bottom""><span class=""boldText"">Our&nbsp;Price:&nbsp;</span><span class=""pricing-orange2"">" & formatCurrency(RS("price_Our")) & "</span><br><span class=""pricing-gray2"">List&nbsp;Price:&nbsp;<s>" & formatCurrency(RS("price_Retail")) & "</s></span><br><span class=""pricing-gray2"">You&nbsp;Save:&nbsp;" & formatCurrency(RS("price_Retail") - RS("price_Our")) & "</span></td></tr>" & vbcrlf
                                        response.write "</table></td>" & vbcrlf
                                        a = a + 1
                                        if a = 4 then
                                            response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""798"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
                                            a = 0
                                        else
                                            response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                                        end if
                                    end if
                                    '################################
                                    'if DoNotDisplay = 0 then
                                        'altText = brandName & " " & modelName & " Bluetooth Headset ?" & RS("itemDesc")
                                        'if categoryid = "14" and modelid = "0" then altText = "Cell Phone Charms & Bling : " & altText
                                        'response.write "<td align=""center"" valign=""top"" width=""192"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""192"" height=""100%"">" & vbcrlf
                                        'response.write "<tr><td align=""center"" valign=""middle"" width=""192"" height=""154""><a href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp""><img src=""/productpics/thumb/" & RS("itemPic") & """ border=""0"" alt=""" & altText & """></a></td></tr>" & vbcrlf
                                        'response.write "<tr><td align=""left"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
                                        'response.write "<tr><td align=""left"" valign=""top""><a class=""product-description2"" href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"" title=""" & altText & """>" & RS("itemDesc") & "</a></td></tr>" & vbcrlf
                                        'response.write "<tr><td align=""right"" valign=""top"" height=""8""><img src=""/images/spacer.gif"" width=""1"" height=""8""></td></tr>" & vbcrlf
                                        'response.write "<form action=""http://www.wirelessemporium.com/cart/item_add.asp"" method=""post""><tr><td align=""right"" valign=""bottom"">" & vbcrlf
                                        'response.write "<span class=""pricing-gray"">List&nbsp;Price:&nbsp;<s>" & formatCurrency(RS("price_Retail")) & "</s></span><br><span class=""pricing-black"">You&nbsp;Save:&nbsp;" & formatCurrency(RS("price_Retail") - RS("price_Our")) & "</span><br><input type=""hidden"" name=""prodid"" value=""" & RS("itemid") & """><input type=""hidden"" name=""qty"" value=""1""><input type=""image"" src=""/images/button_buynow_4.jpg"" width=""94"" height=""24"" border=""0"" alt=""Buy Now"" align=""absbottom""><span class=""pricing-red"">&nbsp;&nbsp;&nbsp;" & formatCurrency(RS("price_Our")) & vbcrlf
                                        'response.write "</td></tr></form>" & vbcrlf
                                        'response.write "</table></td>" & vbcrlf
                                        'a = a + 1
                                        'if a = 4 then
                                            'response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""798"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
                                            'a = 0
                                        'else
                                            'response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                                        'end if
                                    'end if
                                    RS.movenext
                                loop
                                if a = 1 then
                                    response.write "<td width=""192"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""192"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""192"">&nbsp;</td>" & vbcrlf
                                elseif a = 2 then
                                    response.write "<td width=""192"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""192"">&nbsp;</td>" & vbcrlf
                                elseif a = 3 then
                                    response.write "<td width=""192"">&nbsp;</td>" & vbcrlf
                                end if
                                %>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->