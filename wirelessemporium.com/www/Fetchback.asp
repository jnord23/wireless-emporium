<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td valign="top">
            <img src="/images/10off09_fetchback_WE.jpg" width="806" height="80" border="0" alt="Thanks for visitng us again! You may use the coupon code below to enjoy an instant 10% Off + FREE SHIPPING on your next order!">
        </td>
    </tr>
    <tr>
        <td> 
            <table border="0" width="600" cellspacing="0" cellpadding="0" align="center">
                <tr>
                    <td align="center" valign="top" bgcolor="#FFFFFF">
                        <p>&nbsp;</p>
                        <p align="center" class="normaltext">
                            Thanks for visiting WirelessEmporium.com! Feel free to use the following Exclusive one-time coupon code at checkout
                            to obtain an additional 10% Off your entire order + FREE 1st Class Shipping:
                        </p>
                        <p align="center" class="normaltext">
                            <b>WEFETCHBACK</b>
                        </p>
                        <p align="center" class="smltext">
                            <i>* No minimum purchase required. Offer applies only to cell phone accessory items and does not include ringtones, cell phones or downloads.</i>
                        </p>
                        <br>
                        <p align="center">
                            <a href="http://www.wirelessemporium.com/" style="font-weight: bold; font-size: 14pt; color: #f86a11; font-family: Verdana, Arial, Helvetica, sans-serif">Start Shopping Now!</a>
                        </p>
                        <p>&nbsp;</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->