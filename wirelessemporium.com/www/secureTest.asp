<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td>HTTP_URL:</td>
        <td><%=request.ServerVariables("HTTP_URL")%></td>
    </tr>
    <tr>
        <td>HTTP_HOST:</td>
        <td><%=request.ServerVariables("HTTP_HOST")%></td>
    </tr>
    <tr>
        <td>HTTPS_SERVER_ISSUER:</td>
        <td><%=request.ServerVariables("HTTPS_SERVER_ISSUER")%></td>
    </tr>
    <tr>
        <td>PATH_INFO:</td>
        <td><%=request.ServerVariables("PATH_INFO")%></td>
    </tr>
    <tr>
        <td>ALL HTTP:</td>
        <td><%=request.ServerVariables("ALL_HTTP")%></td>
    </tr>
    <tr>
        <td>SERVER_PROTOCOL:</td>
        <td><%=request.ServerVariables("SERVER_PROTOCOL")%></td>
    </tr>
    <tr>
        <td>HTTPS Issuer:</td>
        <td><%=request.ServerVariables("HTTPS_SERVER_ISSUER")%></td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->
