<%
	noMobileRedirect = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_OnSale.asp"-->
<%
if date > cdate("11/30/2011") then
	call PermanentRedirect("/")
end if

response.buffer = true
dim productListingPage
productListingPage = 1

call fOpenConn()
SQL = 	"	select	a.typeid, a.itemid, a.itemdesc, a.itempic, a.price_retail, a.price_our, a.inv_qty, a.itemkit_new, b.originalprice, b.discountpercent, c.orderNum" & vbcrlf & _
		"	from	we_items a join itemvalue b" & vbcrlf & _
		"		on	a.itemid = b.itemid left outer join temp_seasonal_order c" & vbcrlf & _
		"		on	a.itemid = c.itemid" & vbcrlf & _
		"	where	a.hidelive = 0" & vbcrlf & _
		"		and	a.inv_qty <> 0" & vbcrlf & _
		"		and	a.price_our > 0" & vbcrlf & _
		"		and	a.seasonal = 1" & vbcrlf & _
		"		and	b.siteid = 0" & vbcrlf & _
		"		and	a.itemid in (98535,98534,98533,98532,98531,98530,98529,98528,88433,95788,58022,83118,55486,69465,69471,78205)" & vbcrlf & _
		"	order by c.orderNum, a.typeid desc, a.itemid " & vbcrlf
		
session("errorSQL") = sql				
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

'if RS.eof then
'	call fCloseConn()
'	call PermanentRedirect("/")
'end if
%>
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="middle" width="100%">
        	<% if date > cdate("11/27/2011") then %>
	            <img src="/images/seasonal/WE_seasonalPicks_monday.jpg" border="0" alt="Cyber Monday Specials - Get More. Spend Less." title="Cyber Monday Specials - Get More. Spend Less." />
            <% else %>
	            <img src="/images/seasonal/WE_seasonalPicks_friday.jpg" border="0" alt="Black Friday Specials - Get More. Spend Less." title="Black Friday Specials - Get More. Spend Less." />
            <% end if %>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="748">
                <tr>
                    <td>&nbsp;</td>
                </tr>
				<tr>
                    <td align="center" valign="top">
                        <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <%
                                dim altText, brandName, modelName, categoryName
                                dim DoNotDisplay, RSkit
								do until RS.eof
									altText = RS("itemDesc")
									%>
									<td align="center" valign="top" width="172" height="100%">
                                    	<table border="0" cellspacing="0" cellpadding="0" width="172" height="100%">
                                            <tr>
                                            	<td align="center" valign="middle" width="172" height="154">
                                                <%
												call ImageHtmlTagWrapper( _
												"<img src=""/productpics/thumb/" & RS("itemPic") & """ border=""0"" alt=""" & altText & """ />",  _
												"/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp", _
												true)
												%>
                                                </td>
											</tr>
                                            <tr><td align="center" valign="top" height="5"><img src="/images/spacer.gif" width="1" height="5"></td></tr>
                                            <tr><td align="center" valign="top" height="55"><a class="cellphone-link" href="/p-<%=RS("itemid")%>-<%=formatSEO(RS("itemDesc"))%>.asp" title="<%=altText%>"><%=RS("itemDesc")%></a></td></tr>
                                            <tr><td align="center" valign="top" height="8"><img src="/images/spacer.gif" width="1" height="8"></td></tr>
                                            <tr><td align="center" valign="bottom">
                                            	<span class="boldText">Sale&nbsp;Price:&nbsp;</span><span class="pricing-orange2"><%=formatCurrency(RS("price_Our"))%></span><br>
                                                <span class="pricing-gray2">Our&nbsp;Price:&nbsp;<s><%=formatCurrency(RS("originalprice"))%></s></span><br>
                                                <span class="pricing-gray2">List&nbsp;Price:&nbsp;<s><%=formatCurrency(RS("price_Retail"))%></s></span><br>
                                                <span class="pricing-gray2">You&nbsp;Save:&nbsp;<%=formatCurrency(RS("price_Retail") - RS("price_Our"))%></span></td></tr>
										</table>
									</td>
                                    <%
									a = a + 1
									if a = 4 then
										response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""748"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
										a = 0
									else
										response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""280"" border=""0""></td>" & vbcrlf
									end if
									RS.movenext
                                loop
                                if a = 1 then
                                    response.write "<td width=""172"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""172"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""172"">&nbsp;</td>" & vbcrlf
                                elseif a = 2 then
                                    response.write "<td width=""172"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""172"">&nbsp;</td>" & vbcrlf
                                elseif a = 3 then
                                    response.write "<td width=""172"">&nbsp;</td>" & vbcrlf
                                end if
                                %>
                            </tr>
                        </table>
                    </td>
                </tr>                
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->