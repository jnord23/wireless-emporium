<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
dim noAsp : noAsp = true
response.buffer = true
dim pageName : pageName = "brand-model"
dim basePageName : basePageName = "Brand-Model"
dim curPage : curPage = prepStr(request.QueryString("curPage"))
dim modelid, brandName, modelName, modelName2, brandModel
modelid = prepInt(request.querystring("modelid"))

testURL = request.QueryString("brandModel") & "-accessories"
if instr(testURL, "--") > 0 then
	testURL = replace(testURL, "--", "-")
	if instr(request.QueryString, "temp=1.1&") > 0 then
		testURL = testURL & "?" & mid(request.QueryString, instr(request.QueryString, "temp=1.1&")+9)
	end if
	PermanentRedirect("/" & testURL)
elseif left(testURL, 6) = "other-" then
'	response.write mid(testURL, 7)
	PermanentRedirect("/" & mid(testURL, 7))
end if

if instr(request.QueryString("brandModel"),"--") > 0 then
	brandModel = prepStr(replace(request.QueryString("brandModel"),"--","-"))
else
	brandModel = prepStr(request.QueryString("brandModel"))
end if

if brandModel <> "" then
	if brandModel = "apple-iphone-1" then brandModel = "apple-iphone"
	curURL = curPage
	curURL = replace(replace(replace(replace(curURL,"-accessories",""),"-"," "),".asp",""), "'", "''")
	
	brandModel = replace(brandModel,"plussign","+")
	useBrandModel = replace(brandModel,"-"," ")
	sql = "exec sp_modelIdByName 0,'" & useBrandModel & "'"
'	response.write "1sql:" & sql & "<br>"
	session("errorSQL") = sql
	set modelIdRS = oConn.execute(sql)
	
	if modelIdRS.eof then 
		sql = "exec sp_modelIdByName 1,'" & useBrandModel & "'"
'		response.write "2sql:" & sql & "<br>"		
		session("errorSQL") = sql
		set modelIdRS = oConn.execute(sql)	
	end if
	
	if modelIdRS.EOF then
		'===== added by Terry on 2/18/2014 to fix other minor brands linking
		if modelIdRS.EOF then
			sql = "exec sp_modelIdByName 4,'" & useBrandModel & "'"
'			response.write "3sql:" & sql & "<br>"
			session("errorSQL") = sql
			set modelIdRS = oConn.execute(sql)
		end if
		'=====

		chopLap = 1
		do while modelIdRS.EOF
			session("errorSQL") = "useBrandModel:" & useBrandModel
			removedWord = replace(mid(useBrandModel,instrrev(useBrandModel," "))," ","")
			useBrandModel = left(useBrandModel,instrrev(useBrandModel," ")-1)
			if removedWord <> "" then
				sql = "exec sp_modelIdByName 2,'" & useBrandModel & "'"
'				response.write "4sql:" & sql & "<br>"
			else
				sql = "exec sp_modelIdByName 1,'" & useBrandModel & "'"
'				response.write "5sql:" & sql & "<br>"
			end if
			session("errorSQL") = sql
			set modelIdRS = oConn.execute(sql)
			
			if instrrev(useBrandModel," ") < 1 then exit do
		loop

		if modelIdRS.EOF then
			sql = "exec sp_modelIdByName 3,'" & removedWord & "'"
'			response.write "5sql:" & sql & "<br>"
			session("errorSQL") = sql
			set modelIdRS = oConn.execute(sql)
		end if
		
		if modelIdRS.EOF then
			if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
				response.Write(sql & "<br><br>")
				response.Write("bounce to home1 (MP)")
				response.End()
			else
				PermanentRedirect("/")
			end if
		else
			modelid = modelIdRS("modelID")
		end if
	else
		modelid = modelIdRS("modelID")
	end if
else
	if modelid = 0 then
		if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
			response.Write("bounce to home2 (MP)")
			response.End()
		else
			PermanentRedirect("/")
		end if
	else
		sql = "exec sp_brandModelNamesByModelID " & modelID
		session("errorSQL") = sql
		set modelRS = oConn.execute(sql)
		
		useLink = formatSEO(modelRS("brandName")) & "-" & formatSEO(modelRS("modelName"))
		PermanentRedirect("/" & useLink & "-accessories")
	end if
end if
saveModelID = modelid

response.Cookies("saveBrandID") = 0
response.Cookies("saveModelID") = modelID
response.Cookies("saveItemID") = 0

leftGoogleAd = 1
googleAds = 1
isTablet = false

dim brandID, modelImg
dim SEtitle, SEdescription, SEkeywords, topText, bottomText

call fOpenConn()

'updated sp on 12/20/2012 JN
sql	= "exec sp_getCatsByModel " & modelID & ", 0"
session("errorSQL") = SQL
set RS = oConn.execute(sql)

if not RS.eof then
	brandName = RS("brandName")
	brandID = prepInt(RS("brandID"))
	modelName = RS("modelName")
	modelImg = RS("modelImg")
	isTablet = RS("isTablet")
	if brandID = 17 then useBrandImg = 17 else useBrandImg = 20
else
	sql	=	"exec sp_basicModelDetails " & modelid
	session("errorSQL") = SQL
	set objRsName = oConn.execute(sql)
	
	if not objRsName.eof then
		brandName = objRsName("brandName")
		brandID = objRsName("brandID")
		modelName = objRsName("modelName")
		modelImg = objRsName("modelImg")
		isTablet = objRsName("isTablet")
	end if
	
	objRsName = null
end if

session("otherAdminOptions") = ", <a style='color:#ffffff;' href='/admin/db_update_models.asp?ModelID=" & modelID & "&BrandID=" & brandID & "&searchType=Model&submitType=Edit&submitModel=Search'>Edit " & modelName & "</a>"

'todo: need to consolidate Metatags with we_XXXText
dim strH1: strH1 = brandName & " " & modelName & " Accessories"
dim strH2: strH2 = EMPTY_STRING
dim strAltText: strAltText = altText & " Accessories"
dim strBreadcrumb : strBreadcrumb = brandName & " " & modelName & " Accessories"
dim strBrandBreadCrumb

if brandID = 17 and instr(lcase(modelName), "iphone") > 0 then
	strBrandBreadCrumb = brandName & " iPhone Accessories"
elseif brandID = 17 and instr(lcase(modelName), "iphone") <= 0 then
	strBrandBreadCrumb = brandName & " iPod/iPad Accessories"
else
	strBrandBreadCrumb = brandName & " Cell Phone Accessories"
end if

if isTablet then
	strTypeToken = "Tablet"
else
	strTypeToken = "Cell Phone"
end if
%>
<!--#include virtual="/includes/template/top.asp"-->
<!-- super sweet stuff -->
<link href="/includes/css/mvt_accessoryFinder1.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_banner1.css" rel="stylesheet" type="text/css">
<link href="/includes/css/brandModel.css" rel="stylesheet" type="text/css">
<div class="breadcrumbFinal">
	<% if isTablet then %>
		<a class="breadcrumb" href="/tablet-ereader-accessories">Tablet Accessories</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
	<% else %>
		<a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;
        <%if brandID = 17 and instr(lcase(modelName), "iphone") > 0 then%>
			<a class="breadcrumb" href="/<%=formatSEO(brandName)%>-iphone-accessories"><%=strBrandBreadCrumb%></a>&nbsp;&gt;&nbsp;
		<%elseif brandID = 17 and instr(lcase(modelName), "iphone") <= 0 then%>
			<a class="breadcrumb" href="/<%=formatSEO(brandName)%>-ipod-ipad-accessories"><%=strBrandBreadCrumb%></a>&nbsp;&gt;&nbsp;
        <%else%>
			<a class="breadcrumb" href="/<%=formatSEO(brandName)%>-phone-accessories"><%=strBrandBreadCrumb%></a>&nbsp;&gt;&nbsp;
        <%end if%>
		<%=strBreadcrumb%>
	<% end if %>
</div>
<div><img src="/images/brandmodelheaders/header_<%=brandID%>.jpg" width="745" height="30" border="0" alt="<%=strBreadCrumb_brand & " " & modelName%> Accessories"></div>
<div id="modelDetails">
	<div id="modelDetailImg"><img src="/productPics/models/<%=modelImg%>" width="80" border="0" alt="<%=strAltText%>"></div>
    <div id="modelDetailText">
    	<h1 class="brandModel"><%=seH1%></h1>
        <div id="CustomerContent"><%=SeTopText%></div>
    </div>
</div>
<% if RS.eof then %>
<div id="nothingAvailable">The product you are searching for is unavailable at this time</div>
<% else %>
<div id="availableCategories">
	<%
	brandName = replace(brandName, "/", " / ")
	modelName = replace(modelName, "/", " / ")
	
	sql	=	"exec sp_customFaceplatesAvailable " & modelID
	session("errorSQL") = sql
	set rsCustom = oConn.execute(sql)
	
	if rsCustom("cnt") > 0 then
		strLink = "/sb-" & brandid & "-sm-" & modelid & "-scd-2000-custom-covers-faceplates-" & formatSEO(brandName) & "-" & formatSEO(modelName)
		altText = modelName & " Custom Cases"
		%>
    <div id="catMainBox">
    	<div id="catImg"><a href="<%=strLink%>" title="<%=altText%>"><img src="/images/WE-category-buttons-custom.jpg" width="165" border="0" alt="<%=altText%>" title="<%=altText%>" /></a></div>
        <div id="catDetails"><a href="<%=strLink%>" title="<%=altText%>" class="cellphone3-link"><%=altText%></a></div>
    </div>
		<%
	end if
	
	dim handsFree : handsFree = 0
	do until RS.eof
		thisType = replace(RS("typeName"), "/", " / ")
		useTypeID = rs("typeid")
		if useTypeID = 5 then handsFree = 1
		if useTypeID = 16 then useTypeID = 15
		curLink = replace(rs("urlStructure"),".asp","")
		strLink = replace(replace(replace(replace(replace(replace(curLink, "{brandid}", brandid), "{modelid}", modelid), "{typeid}", useTypeID), "{brandname}", formatSEO(brandName)), "{modelname}", formatSEO(modelName)), "{typename}", formatSEO(thisType))
		altText = modelName & " " & thisType
%>
	<div id="catMainBox">
    	<div id="catImg"><a href="<%=strLink%>" title="<%=altText%>"><img src="/images/categories/we_cat_<%=useTypeID%>.jpg" width="165" border="0" alt="<%=altText%>" title="<%=altText%>" /></a></div>
        <div id="catDetails"><a href="<%=strLink%>" title="<%=altText%>" class="cellphone3-link"><%=altText%></a></div>
    </div>
<%
		RS.movenext
	loop
	
	if handsFree = 0 then
		thisType = "Handsfree Bluetooth"
		useTypeID = 5
		strLink = replace(replace(replace(replace(replace(replace("sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}", "{brandid}", brandid), "{modelid}", modelid), "{typeid}", useTypeID), "{brandname}", formatSEO(brandName)), "{modelname}", formatSEO(modelName)), "{typename}", formatSEO(thisType))
		altText = modelName & " " & thisType
%>
	<div id="catMainBox">
    	<div id="catImg"><a href="<%=strLink%>" title="<%=altText%>"><img src="/images/categories/we_cat_<%=useTypeID%>.jpg" width="165" border="0" alt="<%=altText%>" title="<%=altText%>" /></a></div>
        <div id="catDetails"><a href="<%=strLink%>" title="<%=altText%>" class="cellphone3-link"><%=altText%></a></div>
    </div>
<%
	end if
	%>
</div>
<% end if %>
<% if not isTablet then %>
<div class="graybox-middle" id="relatedModelsHeader">
    <div class="graybox-inner" title="RELATED <%=ucase(brandName)%><%=" " & ucase(strTypeToken)%> MODELS" align="center">
        <h3 class="brandModel">RELATED <%=ucase(brandName)%><%=" " & ucase(strTypeToken)%> MODELS</h3>
    </div>
</div>
<div id="allRelatedModels">
	<%
	dim strRelatedSQL
	strRelatedSQL = "exec sp_relatedPhoneModels " & brandID & ", " & modelID                                
	set RS2 = oConn.execute(strRelatedSQL)
						
	do until RS2.eof
		modelID = RS2("modelID")
		modelName = RS2("modelName")
		modelImg = RS2("modelImg")

		if brandid = "17" then
			altTag = modelName & " Accessories"
		else
			altTag = brandName & " " & modelName & " Accessories"
		end if
		%>
        <div id="relatedModelBox">
        	<%if modelID = 940 then%>
                <a href="/apple-ipad-accessories" title="<%=altTag%>"><img src="/productPics/models/<%=modelImg%>" border="0" alt="<%=replace(altTag,"Accessories","accessories")%>"></a><br /><br />
                <a class="cellphone-link" href="/apple-ipad-accessories" title="<%=altTag%>"><%=replace(modelName,"/"," / ") & "&nbsp;Accessories"%></a>
            <%else%>
                <a href="/<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>-accessories" title="<%=altTag%>"><img src="/productPics/models/<%=modelImg%>" border="0" alt="<%=replace(altTag,"Accessories","accessories")%>"></a><br /><br />
                <a class="cellphone-link" href="/<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>-accessories" title="<%=altTag%>"><%=replace(modelName,"/"," / ") & "&nbsp;Accessories"%></a>
            <%end if%>
        </div>
		<%
		RS2.movenext
	loop
	RS2.close
	set RS2 = nothing
	%>
</div>
<% end if %>
<% if saveModelID = 1267 or saveModelID = 968 or saveModelID = 1120 then 'iPhone 4s, iPhone 4, iPhone 4S %>
<div style="text-align:center;margin-top:2.0em;">
	<iframe width="468" height="263" src="http://www.youtube.com/embed/nvNXP5iNBHo?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;wmode=opaque" frameborder="0" allowfullscreen></iframe>
</div>
<%
end if
%>
<div style="width:100%; display:table; padding-top:20px; margin:20px auto;">
	<%
	turnToItemID = "M" & saveModelID
	%>
    <!--#include virtual="/includes/asp/inc_turnto.asp"-->
</div>
<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<div><img src="/images/brandmodelheaders/hline_bottom.jpg" width="740" height="8" border="0"></div>
<% if SeBottomText <> "" then %>
<div style="padding:5px 0px; margin:10px 0px;"><%=SeBottomText%></div>
<% end if %>
<script>
window.WEDATA.pageType = 'brandModel';
window.WEDATA.pageData = {
	brand: <%= jsStr(brandName) %>,
	brandId: <%= jsStr(brandId) %>,
	model: <%= jsStr(modelName) %>,
	modelId: <%= jsStr(modelId) %>
}
</script>
<!--#include virtual="/includes/template/bottom.asp"-->
<script language="javascript">
	ajax('/ajax/accessoryFinder.asp?hzBox=1&brandid=<%=brandID%>&selectModelid=<%=saveModelID%>', 'cbModelBox2')
	ajax('/ajax/accessoryFinder.asp?hzBox=1&modelid=<%=saveModelID%>', 'cbCatBox2')
	
	<% if saveModelID = 1412 then %>
	setTimeout("getImages()",2000)
	function getImages() {
		document.getElementById("iphone5Pic1").innerHTML = document.getElementById("iphone5Pic1").innerHTML.replace("imgtemp","img")
		document.getElementById("iphone5Pic2").innerHTML = document.getElementById("iphone5Pic2").innerHTML.replace("imgtemp","img")
		document.getElementById("iphone5Pic3").innerHTML = document.getElementById("iphone5Pic3").innerHTML.replace("imgtemp","img")
	}
	<% end if %>
</script>