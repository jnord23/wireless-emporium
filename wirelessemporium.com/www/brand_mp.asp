<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
	response.buffer = true

	pageName = "brandNew2"
	brandid = prepInt(request.querystring("brandid"))
	brand = prepStr(request.QueryString("brand"))
	phoneOnly = ds(request.QueryString("phoneOnly"))
	showIntPhones = ds(request.Form("showIntPhones"))
	cbModel = ds(request("cbModel"))
	cbSort = ds(request("cbSort"))
	cbCarrier = ds(request("cbCarrier"))
	viewPage = ds(request.form("viewPage"))
	maxModels = ds(request.form("maxModels"))
	if not isnull(session("qSort")) then
		qSort = session("qSort")
		session("qSort") = null
	end if
	stitchImgPath = "/images/stitch/brands/"
	
	if brand <> "" then
		if brand = "hp-palm" then brand = "hp/palm"
		if brand = "sony-ericsson" then brand = "Sony Ericsson"
		if brand = "other-brand" then brand = "other"
		sql = "select brandID from we_brands where brandName like '" & brand & "%'"
		session("errorSQL") = sql
		set brandRS = oConn.execute(sql)
		
		if brandRS.EOF then
			if instr(request.ServerVariables("SCRIPT_NAME"),"staging") > 0 then
				response.Write("can't find the brand")
				response.End()
			else
				response.Redirect("/")
			end if
		end if
		brandID = brandRS("brandID")
	end if
	
	response.Cookies("saveBrandID") = brandID
	response.Cookies("saveModelID") = 0
	response.Cookies("saveItemID") = 0
	
	noSort = 0
	if brandid = 0 then call PermanentRedirect("/")
	if phoneOnly = "" or not isNumeric(phoneOnly) then phoneOnly = 0
	if isnull(qSort) or len(qSort) < 1 then qSort = ""
	if qSort <> "" then cbSort = qSort
	if isnull(showIntPhones) or len(showIntPhones) < 1 then showIntPhones = 0
	if isnull(cbSort) or len(cbSort) < 1 then
		noSort = 1
		cbSort = "AZ"
	end if
	if isnull(viewPage) or len(viewPage) < 1 then viewPage = 1
	if isnull(maxModels) or len(maxModels) < 1 then maxModels = 40

	leftGoogleAd = 1
	displayIntLink = 0
	displayHideIntLink = 0
	googleAds = 1
	lap = 0
	sqlOrder = " order by orderNum desc, oldModel, international, modelname"
	if viewPage = 1 then showPopPhones = 1 else showPopPhones = 0
	
	maxModels = 4000
	showPopPhones = 1
	
	'Normal Page Start
	set fso = CreateObject("Scripting.FileSystemObject")
	Set Jpeg = Server.CreateObject("Persits.Jpeg")
	Jpeg.Quality = 100
	Jpeg.Interpolation = 1
	
	if brandID = 15 then
		sql = "exec sp_otherBrandIDs"
		session("errorSQL") = sql
		set brandRS = oConn.execute(sql)
		
		useBrandID = brandID
		do while not brandRS.EOF
			useBrandID = useBrandID & "," & brandRS("brandID")
			brandRS.movenext
		loop
	else
		useBrandID = brandID
	end if
	
	if brandID = 17 and phoneOnly = 1 then
		brandName = "iPhone"
		cellPhoneTitle = "Cell Phone"
	elseif brandID = 17 and phoneOnly = 0 then
		brandName = "iPod/iPad"
		phoneTitle = "Product"
		cellPhoneTitle = ""
	else
		cellPhoneTitle = "Cell Phone"
	end if
	
	if cbCarrier <> "" then
		fileExt = "_" & cbCarrier
		showPopPhones = 0
	end if
	
	if cbSort <> "" then
		if cbSort = "AZ" and noSort = 0 then
			fileExt = fileExt & "_" & cbSort & "S"
		else
			fileExt = fileExt & "_" & cbSort
		end if
		if noSort = 0 then showPopPhones = 0 else showPopPhones = 1
	end if
	
	sql = "exec sp_modelsByBrand '" & useBrandID & "','" & cbSort & "','" & cbCarrier & "'," & phoneOnly
	session("errorSQL") = SQL
	'response.write "<pre>" & sql & "</pre>"
	arrRS = getDbRows(sql)

	if isnull(arrRS) then response.redirect("/") end if
	
	otherPhones = arrRS(9,0)
	linkName = arrRS(10,0)
	arrPopPhones = filter2DRS(arrRS, 3, true)
	arrOldPhones = filter2DRS(arrRS, 4, true)
	arrIntPhones = filter2DRS(arrRS, 5, true)
	arrDisplayPhones = filter2DRS(arrRS, 8, true)
	
	if brandid = 15 then 
		arrSortedRS = arrRS
	else
		arrSortedRS = sort2DRS(arrRS, 1)
	end if
	
	if ubound(arrPopPhones,2) >= 0 then showPopPhones = 1 else showPopPhones = 0 end if
	if ubound(arrOldPhones,2) >= 0 then showOldPhones = 1 else showOldPhones = 0 end if
	if ubound(arrIntPhones,2) >= 0 then showIntPhones = 1 else showIntPhones = 0 end if
	
	if brandName = "" then brandName = arrDisplayPhones(6,0) end if
	
	phoneList = ""
	if not isnull(arrRS) then
		for i=0 to ubound(arrRS,2)
			if phoneList = "" then
				phoneList = """" & arrRS(1,i) & """"
			else
				phoneList = phoneList & ", """ & arrRS(1,i) & """"
			end if
		next
	end if
	
	dim strH2: strH2=""
	phoneTitle = "Phone"

	if showPopPhones = 1 then
		popItemImgPath = formatSEO(brandName) & "_pop.jpg"
	end if
	
	if maxModels > 40 then
		if brandID = 17 and phoneOnly = 1 then
			itemImgPath = "applePhones_all" & fileExt & ".jpg"
		else
			itemImgPath = formatSEO(brandName) & "_all" & fileExt & ".jpg"
			oldItemImgPath = formatSEO(brandName) & "_old" & fileExt & ".jpg"
			intItemImgPath = formatSEO(brandName) & "_int" & fileExt & ".jpg"
		end if
	else
		itemImgPath = formatSEO(brandName) & "_p" & viewPage & fileExt & ".jpg"
	end if

	if bStaging then popItemImgPath = "staging_" & popItemImgPath
	if bStaging then itemImgPath = "staging_" & itemImgPath
	if bStaging then oldItemImgPath = "staging_" & oldItemImgPath
	if bStaging then intItemImgPath = "staging_" & intItemImgPath
	
	if brandID = 15 then brandName = "Other"
	
	dim strBreadcrumb
	if brandID = 17 and phoneOnly = 0 then
		strBreadcrumb = "iPod/iPad Accessories"
	elseif brandID = 17 and phoneOnly = 1 then
		strBreadcrumb = "iPhone Accessories"	
	else
		strBreadcrumb = brandName & " Accessories"
	end if

	'legacy rule-based logic for H1
	if brandName = "Apple" then
		if phoneOnly = 0 then strH1="iPod/iPad Accessories" else strH1="iPhone Accessories" end if
	else
		strH1=brandName & " Accessories"
	end if

	'legacy rule-based logic for H2
	if brandName = "Apple" then
		if phoneOnly = 0 then strH1="iPod/iPad Accessories" else strH1="iPhone Accessories" end if
	else
		strH1=brandName & " Accessories"
	end if

	if showPopPhones = 1 then
		imgStitchPath = Server.MapPath(stitchImgPath & popItemImgPath)
		call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrPopPhones, 2)	
	end if

	imgStitchPath = Server.MapPath(stitchImgPath & itemImgPath)
	call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrDisplayPhones, 2)
	
	session("breadcrumb_model") = ""
	if not fso.fileExists(server.MapPath("/images/brandheaders/we_brand_header_" & brandid & "_small.jpg")) then
		if fso.fileExists(server.MapPath("/images/brandheaders/we_brand_header_" & brandid & ".jpg")) then
			Jpeg.Open server.MapPath("/images/brandheaders/we_brand_header_" & brandid & ".jpg")
			Jpeg.Width = 100
        	Jpeg.Height = Jpeg.OriginalHeight * 100 / Jpeg.OriginalWidth
			Jpeg.Save server.MapPath("/images/brandheaders/we_brand_header_" & brandid & "_small.jpg")
		end if
	end if
%>
<!--#include virtual="/includes/template/top_brand.asp"-->
<script>
window.WEDATA.pageType = 'brand';
window.WEDATA.pageData = {
	brand: <%= jsStr(brandName) %>,
	brandId: <%= jsStr(brandId) %>
};
</script>

<link href="/includes/css/brand.css" rel="stylesheet" type="text/css">
<div><a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%></div>
<div id="h1BrandLogo">
	<div id="brandH1">
        <div><h1 class="brand"><%=seH1%></h1></div>
        <div>
            <h2 class="brand"><%=strH2%></h2>
            <div id="CustomerContent"><%=SeTopText%></div>
        </div>
    </div>
    <div id="brandLogo"><img src="/images/brandheaders/we_brand_header_<%=brandid%>_small.jpg" border="0" /></div>
</div>
<div style="clear:both;">&nbsp;</div>
<form name="topSearchForm" method="post">
<div id="selectModelBox">
    <div id="selectModelBoxPart1"><img src="/images/brands/sort-by-background-left.jpg" border="0" /></div>
    <div id="selectModelBoxPart2">
        <select name="cbModel" style="width:275px;" onChange="if(this.value != ''){window.location=this.value;}">
            <option value="">Search By Phone Name</option>
            <%
            for nRows=0 to ubound(arrSortedRS, 2)
                jumptoLink = "/" & formatSEO(arrSortedRS(6,nRows)) & "-" & formatSEO(arrSortedRS(1,nRows)) & "-accessories"
				jumptoLink = replace(jumptoLink, "t-mobile-sidekick-", "sidekick-")
                %>
                <option value="<%=jumptoLink%>"><%=arrSortedRS(6,nRows) & " " & arrSortedRS(1,nRows)%></option>                                                                        
                <%
            next
            %>
        </select>
    </div>
    <div id="selectModelBoxPart3">-&nbsp;OR&nbsp;-</div>
    <div id="selectModelBoxPart4">
        <div id="selectModelBoxPart4_inner1">
            <input type="text" name="txtSearch" value="Search" onclick="searchHelpSelect()" onkeyup="searchHelp(this.value)" onblur="searchHelpHide()" />
            <div id="searchHelpBox" style="z-index:100;"></div>
        </div>
    </div>
    <div id="selectModelBoxPart5"><img src="/images/brands/sort-by-background-right.jpg" border="0" onclick="jumpto();return false;" /></div>
</div>
</form>
<% if showPopPhones = 1 then %>
<div id="topModelsHeaderBox">
	<div id="topModelsHeader">TOP <%=ucase(replace(brandName, "/", " / ") & " " & cellPhoneTitle)%> MODELS</div>
    <div><%=drawStitchModelBoxByArray(arrPopPhones, stitchImgPath & popItemImgPath)%></div>
</div>
<% end if %>
<div id="standardModelsHeaderBox">
	<div id="standardModelsHeader">
		<% if showPopPhones = 1 and brandid <> 15 then %>
        OTHER <%=ucase(replace(brandName, "/", " / ") & " " & cellPhoneTitle)%> MODELS
        <% else %>
        <%=ucase(replace(brandName, "/", " / ") & " " & cellPhoneTitle)%> MODELS
        <% end if %>
    </div>
    <div><%=drawStitchModelBoxByArray(arrDisplayPhones, stitchImgPath & itemImgPath)%></div>
</div>
<% if otherPhones > 0 then %>
<div id="viewOtherModelsBox" onclick="showOtherModels()" title="CLICK TO VIEW MORE <%=ucase(brandName)%> MODELS">
	<div id="viewOtherModelsBox_inner1"><img src="/images/brands/bottom-header-left.jpg" border="0" style="cursor:pointer;" /></div>
    <div id="viewOtherModelsBox_inner2">CLICK TO VIEW MORE <%=ucase(brandName)%> MODELS</div>
    <div id="viewOtherModelsBox_inner3"><img id="id_bottom_arrow_old" src="/images/brands/down-arrow.jpg" border="0" /></div>
    <div id="viewOtherModelsBox_inner4"><img src="/images/brands/bottom-header-right.jpg" border="0" style="cursor:pointer;" /></div>
</div>
<div id="otherModelsCell"></div>
<% end if %>
<div>
	<div style="padding:15px 0px 15px 0px;"><%=SeBottomText%></div>
	<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
		<!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
    </table>
</div>
<script language="javascript">var isMasterPage = 0</script>
<!--#include virtual="/includes/template/bottom_brand.asp"-->
<script>
	//onFinder2('b',document.frmFinder2.cbBrand);
	
	function jumpto()
	{
		modelname = document.topSearchForm.txtSearch.value;
		var x=document.topSearchForm.cbModel;
		for (i=0;i<x.length;i++)
			if (x.options[i].text == modelname) window.location = x.options[i].value;
	}

	function toggleModels(which, idArrow)
	{
		toggle(which);
		
		if (document.getElementById(which).style.display == '') 
			document.getElementById(idArrow).src = "/images/brands/up-arrow.jpg"
		else 
			document.getElementById(idArrow).src = "/images/brands/down-arrow.jpg"
	}
	
	function showOtherModels() {
		if (document.getElementById("otherModelsCell").innerHTML == "") {
			document.getElementById("otherModelsCell").innerHTML = "<div style='width=100%;text-align=center;'>Loading Phones...<br />Please Wait</div>"
			ajax('/ajax/otherModels.asp?brandID=<%=brandID%>','otherModelsCell')
		}
		else {
			document.getElementById("otherModelsCell").innerHTML = ""
		}
	}
</script>