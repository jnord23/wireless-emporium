<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Faceplate Sale!</title>
<link rel="stylesheet" href="/includes/css/style.css" type="text/css">
</head>
<body>

<table cellpadding="0" cellspacing="0" border="0" align="center" width="88%">
	<tr>
		<td width="100%" align="center" class="normaltext">
			<br><br><br>
			<h1>Wireless Emporium Faceplate Sale!</h1>
			<p align="justify" class="normaltext">
			For a limited time, when you purchase one faceplate at regular price you can get a second 
            faceplate of equal or lesser value for 50% off.  Choose from thousands of designs for your 
            second faceplate, excluding OEM products.  OEM products include, but aren't limited to Ed 
            Hardy, Naztech, Body Glove, Seidio, Incipio, and CaseMate.
			</p>
			<p align="center"><a href="javascript:window.close();">CLOSE</a></p>
		</td>
	</tr>
</table>

</body>
</html>
