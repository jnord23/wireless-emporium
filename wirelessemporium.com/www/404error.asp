<%
pageTitle = "File not Found ?Error 404"

'Soft 404
Response.Clear
Response.Status = "200 OK"

%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<table border="0" cellspacing="0" cellpadding="0" width="746">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal"><img src="/images/spacer.gif" width="1" height="10"></td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <p>
                Sorry, but the page you are looking for no longer exists.
            </p>
            <p>
                At WirelessEmporium.com, we are continually updating our vast inventory, so the product you are searching for may be unavailable at this time, or its name may have changed.
            </p>
            <p>
                Please use our convenient browse or search features to quickly find the product you need.
            </p>
            <p>
                If you still cannot find what you are looking for, please feel free to e-mail us at
                <a href="mailto:service@WirelessEmporium.com">service@WirelessEmporium.com</a>
            </p>
        </td>
    </tr>
</table>
<p>&nbsp;</p>
<%
Const lngMaxFormBytes = 200

Dim objASPError, blnErrorWritten, strServername, strServerIP, strRemoteIP
Dim strMethod, lngPos, datNow, strQueryString, strURL
Dim strBody

Set objASPError = Server.GetLastError

Dim MissingFile
MissingFile = replace(Request.ServerVariables("QUERY_STRING"),"404;","")

strBody = "<br><br>" & vbcrlf
strBody = strBody & "<ul>" & vbcrlf
strBody = strBody & "<li>MISSING FILE:<br>" & vbcrlf
strBody = strBody & MissingFile & vbcrlf
strBody = strBody & "<br><br></li>" & vbcrlf
strBody = strBody & "<li>Browser Type:<br>" & vbcrlf
strBody = strBody & Server.HTMLEncode(Request.ServerVariables("HTTP_USER_AGENT")) & vbcrlf
strBody = strBody & "<br><br></li>" & vbcrlf
strBody = strBody & "<li>Page:<br>" & vbcrlf

strMethod = Request.ServerVariables("REQUEST_METHOD")
strBody = strBody & strMethod & " " & vbcrlf
If strMethod = "POST" Then strBody = strBody & Request.TotalBytes & " bytes to " & vbcrlf
strBody = strBody & Request.ServerVariables("SCRIPT_NAME") & vbcrlf
strBody = strBody & "</li>" & vbcrlf
If strMethod = "POST" Then
	strBody = strBody & "<p><li>POST Data:<br>" & vbcrlf
	' On Error in case Request.BinaryRead was executed in the page that triggered the error.
	On Error Resume Next
	If Request.TotalBytes > lngMaxFormBytes Then
		strBody = strBody & Server.HTMLEncode(Left(Request.Form, lngMaxFormBytes)) & " . . ." & vbcrlf
	Else
		strBody = strBody & Server.HTMLEncode(Request.Form) & vbcrlf
	End If
	On Error Goto 0
	strBody = strBody & "</li>" & vbcrlf
End If

strBody = strBody & "<br><br></li>" & vbcrlf
strBody = strBody & "<li>Time:<br>" & vbcrlf

datNow = Now()
strBody = strBody & Server.HTMLEncode(FormatDateTime(datNow, 1) & ", " & FormatDateTime(datNow, 3))

SERVER_NAME = Request.ServerVariables("SERVER_NAME")
SCRIPT_NAME = Request.ServerVariables("SCRIPT_NAME")
QUERY_STRING = Request.ServerVariables("QUERY_STRING")

strBody = strBody & "<br><br></li>" & vbcrlf
strBody = strBody & "</ul>" & vbcrlf
strBody = strBody & "<p>REMOTE_ADDR: " & Request.ServerVariables("REMOTE_ADDR") & "</p>" & vbcrlf
strBody = strBody & "<p>SERVER_NAME: " & Request.ServerVariables("SERVER_NAME") & "</p>" & vbcrlf
strBody = strBody & "<p>SCRIPT_NAME: " & Request.ServerVariables("SCRIPT_NAME") & "</p>" & vbcrlf
strBody = strBody & "<p>&nbsp;</p>" & vbcrlf

strBody = strBody & "<p>LINK:<br><a href='http://" & SERVER_NAME & SCRIPT_NAME & "?" & QUERY_STRING & "'>" & SERVER_NAME & SCRIPT_NAME & "?" & QUERY_STRING & "</a></p>" & vbcrlf
strBody = strBody & "<p>&nbsp;</p>" & vbcrlf
strBody = strBody & "<p>HTTP_REFERRER:<br>" & Request.ServerVariables("HTTP_REFERER") & "</p>" & vbcrlf
strBody = strBody & "<p>&nbsp;</p>" & vbcrlf

dim cdo_to, cdo_from, cdo_subject, cdo_body
cdo_to = "webmaster@wirelessemporium.com,jonathan@wirelessemporium.com,terry@wirelessemporium.com"
cdo_from = "service@wirelessemporium.com"
cdo_subject = "404 Problem"
cdo_body = strBody
'CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
%>
<!--#include virtual="/includes/template/bottom.asp"-->