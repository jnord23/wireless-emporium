<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top_index.asp"-->
<%
noCommentBox = true
%>
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;<h1 class="breadcrumbFinal">About Us</h1>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%" style="padding-top:15px;">
			<img src="/images/aboutus/Top-Group-Pic-1.png" border="0" />
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%">
			<img src="/images/aboutus/About-WirelessEmporium-Header.png" border="0" />
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%" style="padding-top:10px;">
			<table border="0" cellspacing="0" cellpadding="0">			
            	<tr>
                	<td align="center" valign="top" width="248">
                    	<img src="/images/aboutus/Co-Founder-Image-Bar.png" border="0" />
                    </td>
                    <td align="left" valign="top" width="500" style="padding-left:20px;">
                    	<div style="padding-top:5px;"><img src="/images/aboutus/WirelessEmporium-Story-Header.png" border="0" /></div>
                    	<div style="padding-top:5px; width:450px; text-align:left;">
                        	The Wireless Emporium Family is fast approaching our 10-year anniversary. That's right; we're proudly reaching a full decade of delivering the best prices, selection and service to our great customers. We started off as a kiosk at the Main Place Mall in Santa Ana, CA; and have grown to be the undisputed leader of quality cell phone accessories at discounted prices--a testament to our philosophy of working with, and gaining the support of our loyal clients and fans. For that, we thank you from the bottom of our hearts.
							We have thousands of products to choose from, and our team is constantly scouring the globe to make sure that there's something to fit every lifestyle. Enjoy convenient shopping, unbeatable savings, and top of the line selections at just a mouse click away. Choose from popular brands like Motorola, Apple, BlackBerry, LG, Samsung, HTC, and many others. We're sure to have the accessories you need. Wireless Emporium is your one-stop-shop for cell phone covers, cases, replacement batteries, chargers, and Bluetooth headsets precision-fitted to match all your phone needs.
							Along with the best brand name cell phone accessories, Wireless Emporium provides in-depth customer service to shed light on any questions or issues. We also offer a 100% satisfaction guarantee and free shipping on all our items!
                            <br /><br />
							<span style="color:#ff7d01; font-weight:bold;"><i>"Wireless Emporium is simply the best for all your wireless needs. I wouldn't use any one else for my cell phone accessories."</i></span>
                            <br /><br />
                            - Terry (La Habra, CA)
						</div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%" style="padding-top:15px;">
			<img src="/images/aboutus/Hori-Bar-Divider.png" border="0" />
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%" style="padding-top:20px;">
			<table border="0" cellspacing="0" cellpadding="0">			
            	<tr>
                	<td align="center" valign="middle" width="350">
                    	<img src="/images/aboutus/Left-Group-Images-Bar.png" border="0" />
                    </td>
                    <td align="center" valign="middle" width="398">
                    	<div style="padding-top:15px;"><img src="/images/aboutus/Satisfaction-Mini-Header.png" border="0" /></div>
                    	<div style="padding-top:3px; width:375px; text-align:left; font-size:11px;">Wireless Emporium thrives off of your satisfaction. Our company wouldn't exist today if our customers weren't happy with the products and services we provide. Our skilled Customer Happiness Team is available to help with any questions about our products, transactions, or even for a casual hello. We know how it feels to be frustrated with products that don't look like they were described, or receiving a broken order in the mail, which is why we offer a 100% satisfaction guarantee policy. Give us a call and return any product from us for ANY reason and we'll take care of the issue as fast as we can.</div>
                    	<div style="padding-top:15px;"><img src="/images/aboutus/Get-Excited-Mini-Header.png" border="0" /></div>
                    	<div style="padding-top:3px; width:375px; text-align:left; font-size:11px;">One of our main goals is to deliver your order as quick as possible. We offer flexible shipping plans for all of our products along with our already-amazing FREE SHIPPING through 1st-Class Mail. We send out most of our products the same day as your purchase or within 24 hours, so that you can have your new accessories sooner. For those who need their products right away, we also offer USPS Priority Mail (2-3 days) or USPS Express as upgrades. We'll ship anything you purchase from us and the more you buy, the cheaper it gets. It's easy.</div>
                    	<div style="padding-top:15px;"><img src="/images/aboutus/Growing-Mini-Header.png" border="0" /></div>
                    	<div style="padding-top:3px; width:375px; text-align:left; font-size:11px;">Our company is constantly growing and evolving. In order for us to keep up with our consumers' needs, we are always reviewing our company's structure both internally and externally, making sure that our site is clear, easy to navigate, and our product selection is always abundant and diverse. This is important for us, we want you to have the best online experience for your cell phone and wireless accessory needs. With that, security has also been a priority since day one. We guarantee that our customer's privacy is completely safe from third parties and is only used by us for fulfilling orders and sending only the best offers available your way.</div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%" style="padding-top:25px;">
			<img src="/images/aboutus/Thanks-For-Visiting-Header.png" border="0" />
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%" style="padding-top:5px;">
        	<div style="width:700px; text-align:left;">
				So what are you waiting for? As the Leader in innovative wireless accessories online, we're confident that with every purchase you make at Wireless Emporium, you will receive the <span style="font-size:14px; font-weight:bold;">ABSOLUTE BEST PRODUCT</span>, at the <span style="font-size:14px; font-weight:bold;">BEST POSSIBLE PRICE</span>, with first-class <span style="font-size:14px; font-weight:bold;">CUSTOMER SERVICE</span>. It is our goal to make every Wireless Emporium customer a customer for life! Thank you for visiting our virtual showroom and please do not hesitate to <a href="/contactus" style="color:#ff7d01; text-decoration:underline;">contact us</a> should you have any questions, comments or suggestions about our site and our service.
            </div>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%" style="padding-top:10px;">
			<table border="0" cellspacing="0" cellpadding="0">			
            	<tr>
                	<td align="right" valign="middle" width="280">
                    	<div style="text-align:left; font-size:14px; font-weight:bold; width:250px; paddinf-left:30px;">
                        	<span style="font-size:20px;">Wireless Emporium, INC.</span>
                            <br />
                           	1410 N. Batavia St.<br />
                            Orange, CA 92867
                            <br /><br />
                            <span style="color:#fe6700;">
                            Contact:<br />
                            800-305-1106<br />
                            </span>
                            M-F: 8:00 am - 5:00 pm PST<br />
                            <a href="/contactus" style="color:#ff7d01; text-decoration:underline;">service@wirelessemporium.com</a>
                        </div>
                    </td>
                    <td align="center" valign="top" width="443">
                    	<div style="height:100%;width:1px;height:207px;background:#3D3D3C;float:left;border-left:1px solid #A3A3A3;border-right:1px solid #A3A3A3; display:none;"></div>
						<iframe width="420" height="236" src="http://www.youtube.com/embed/i4L0aj54Czs?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;wmode=opaque" frameborder="0" allowfullscreen></iframe>
                        <%'<img src="/images/aboutus/WE-Bottom-Building-Image.png" border="0">%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%" style="padding-top:15px; color:#666;">
            * Certain restrictions apply. For details on our shipping 
            policy <a href="http://www.wirelessemporium.com/shipping" title="Shipping Information" class="bottomText-link">click 
            here</a>.<br>
            ** Certain restrictions apply. For details on our return 
            policy <a href="http://www.wirelessemporium.com/store" title="Store Policy" class="bottomText-link">click 
            here</a>.
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%" style="padding-top:35px;">
			<img src="/images/aboutus/Feedback-Header.png" border="0" />
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%" style="padding-top:25px;">
			<table border="0" cellspacing="0" cellpadding="0">			
            	<tr>
                	<td align="center" valign="top" width="440">
	                    <!--#include virtual="/includes/asp/commentBoxIndex.asp"-->  
                    </td>
				</tr>
			</table>
        </td>
    </tr>    
</table>
<!--#include virtual="/includes/template/bottom.asp"-->