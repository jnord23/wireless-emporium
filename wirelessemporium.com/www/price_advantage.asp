<HTML>
<HEAD>
<TITLE>Buy Cell Phone Accessories from WirelessEmporium � Cell phone Covers, Faceplates, Charms, Chargers, Batteries - Deep Discounts and Free Shipping</TITLE>
<META NAME="Keywords" CONTENT="Panasonic cell phone accessory, Panasonic mobile  phone accessories, Panasonic battery pack, Panasonic battery charger, Panasonic phone battery, Panasonic phone part, Panasonic headset, mobile phone accessories, mobile HANDSFREE, faceplates for cell phones">
<META NAME="Description" CONTENT="Wireless Emporium offers discount accessories for your Panasonic cell phones including cases, batteries, chargers, antenna, keypads, bluetooth headsets and more. Buy Online!">
<META NAME="Robots" CONTENT="index,follow">
<META NAME="GOOGLEBOT" CONTENT="INDEX, FOLLOW">
<!-- This site contains information about: cell phone accessories,cheapest cell phone accessories,cell phone faceplate,cell phone covers,cell phone battery,cellular battery,cell phone charger,discount cell phone chargers,hands free kit,hands-free -->
<link rel="stylesheet" href="/includes/css/style.css" type="text/css">
</HEAD>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" link="#666666" vlink="#666666" alink="#FF6600">
		
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" summary="cell phone accessories">
	<tr>
		<td align="center" valign="middle">
			<table width="100%" height="100%" border="0" cellpadding="15" cellspacing="0" summary="Wireless Emporium Price Advantage">
				<tr>
					<td valign="top" bgcolor="#FFFFFF" class="normalText">
						<b>How is Wireless Emporium able to offer these prices?</b>
						<p>We utilize our substantial buying power purchasing in bulk directly from the manufacturers and
						<b>selling direct</b> to the customer.
						By eliminating the "middle-man" we are able to pass
						on considerable savings to you, our valued customer.
						The majority of the products we carry are shipped without the "fancy"
						packaging that is priced into the <b>same products</b> you find at your typical
						cell phone store or mass-market retailer. In addition, our merchandising
						staff is constantly shopping the competition ensuring that our pricing
						is in line with and in most cases better than all of our competitors.
						Factor in our rapid FREE SHIPPING and you receive the <b>best total value</b>
						and <b>the best wireless accessory shopping experience</b> on the Web - this is what Wireless Emporium is all about!
						</p>
					</td>
				</tr>
				<tr>
					<td align="right"><a href="javascript:window.close()" class="normalText">close</a></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</body>
</html>
