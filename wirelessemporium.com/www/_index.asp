<!--#include virtual="/includes/asp/inc_BasePage.asp"--><%
response.buffer = true
if request.ServerVariables("HTTP_X_REWRITE_URL") = "/index.asp" then PermanentRedirect "/"

dim pageTitle
pageTitle = "Home"
dim SEtitle, SEdescription, SEkeywords
SEtitle = "Cell Phone Accessories ?Cell Phone Covers Cases Batteries | iPhone BlackBerry Motorola Samsung LG"
SEdescription = "The #1 Name in Cell Phone Accessories Online - Wireless Emporium. Find all cell phone accessories like cases, covers, chargers and batteries for popular brands like iPhone, BlackBerry, Motorola, HTC, Samsung and LG."
SEkeywords = "cell phone accessories, cheap cell phone accessories, cellphone accessories, discount cell phone accessories, mobile phone accessories, mobile accessories, cellular accessories, cell phone covers, cell phone chargers, cell phone cases, cell phone batteries, iphone accessories, iphone cases, iphone covers, iphone batteries, iphone chargers, blackberry accessories, blackberry chargers, blackberry cases, blackberry batteries, Motorola accessories, Motorola chargers, Motorola cases, Motorola covers, LG covers, LG accessories, LG cases, LG batteries, Samsung accessories, Samsung cases, Samsung covers, Samsung batteries"

dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
dim dicContentEventText: set dicContentEventText = GetContentEventText( dicSeoAttributeInput)
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="padding-top:10px;">
    <tr>
        <td width="100%" align="left" valign="top" style="padding-bottom:10px;">
            <span id="headerTxt"><div id="orangeStar" style="float:left;"></div>Wireless Emporium - Your Trusted Source for <h1 style="margin:0px; display:inline;">Cell Phone Accessories</h1> and More!</span>
            <div align="justify" class="xtrasmlIndexText">
                <strong>Wireless Emporium</strong> is the Leader in Cell Phone Accessories.
                We offer 1000s of accessories for all brands - 
                <a href="/blackberry-cell-phone-accessories.asp" class="xtrasmlIndexLink">BlackBerry Accessories</a>,
                <a href="/iphone-accessories.asp" class="xtrasmlIndexLink">iPhone Accessories</a>,
                <a href="/motorola-cell-phone-accessories.asp" class="xtrasmlIndexLink">Motorola Accessories</a>,																 
                <a href="/lg-cell-phone-accessories.asp" class="xtrasmlIndexLink">LG Accessories</a>, 
                <a href="/samsung-cell-phone-accessories.asp" class="xtrasmlIndexLink">Samsung Accessories</a>, 
                <a href="/htc-cell-phone-accessories.asp" class="xtrasmlIndexLink">HTC Accessories</a> 
                &amp; more. All products, from 
                <a href="/cell-phone-covers-faceplates-screen-protectors.asp" class="xtrasmlIndexLink">cell phone covers</a>,
                <a href="/cell-phone-chargers.asp" class="xtrasmlIndexLink">cell phone chargers</a>, 
                <a href="/cell-phone-batteries.asp" class="xtrasmlIndexLink">cell phone batteries</a> to
                <a href="/bluetooth-headsets-handsfree.asp" class="xtrasmlIndexLink">Bluetooth headsets</a>,
                come with a 100% satisfaction guarantee. Discount Accessories + FREE SHIPPING =
                <strong><font color="#000000">The Best</font> <font color="#404040">TOTAL</font> <font color="#FF0000">Price</font> <font color="#000000">Online, Every Day!</font></strong>
            </div>
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                    <td align="center" valign="top">
                        <div id="simplegallery1"></div>
                    </td>
                    <td align="center" valign="top" style="padding-left:5px;">
                    	<div style="width:255px; height:250px;">
                        	<!--<div style="padding-bottom:10px;" title="Protect Your Smartphone with Great Designs"><a href="/music-skins.asp"><div id="miniBanner1"></div></a></div>-->
                            <div style="padding-bottom:10px;" title="WirelessEmporium Quality Assurance"><a href="/aboutus.asp" title="WirelessEmporium Quality Assurance"><img src="/images/mainBanners/WE-QA-Mini-Banner.jpg" border="0" alt="WirelessEmporium Quality Assurance" /></a></div>
                            <div title="Cellphone Plans"><a href="http://www.cellstores.com/mobile/?r=wirelessemporium2" target="_blank"><div id="miniBanner2"></div></a></div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td style="padding-top:5px;">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                    <td align="left" valign="top" colspan="4"><div id="shopByCat"></div></td>
                </tr>
                <%
				categoryData = "/unlocked-cell-phones.asp#UNLOCKED & REPLACEMENT PHONES#unlockedphones.jpg#Unlocked & Replacement Cell Phones#"
				categoryData = categoryData & "/cell-phone-chargers.asp#CHARGERS#chargers.jpg#Cell Phone Chargers#"
				categoryData = categoryData & "/cell-phone-cases.asp#CASES/POUCHES#cases.jpg#Cell Phone Cases#"
				categoryData = categoryData & "/cell-phone-covers-faceplates-screen-protectors.asp#FACEPLATES#faceplates.jpg#Cell Phone Faceplates#"
				
				categoryData = categoryData & "/music-skins.asp#MUSIC SKINS#musicSkins.jpg#Cell Phone Music Skins#"
				categoryData = categoryData & "/decal-skin.asp#DECAL SKINS#decalSkin.jpg#Cell Phone Decal Skins#"
				categoryData = categoryData & "/bluetooth.asp#BLUETOOTH#bluetooth.jpg#Cell Phone Bluetooth#"
				categoryData = categoryData & "/bluetooth-headsets-handsfree.asp#HANDS-FREE/SPEAKERS#handsfree.jpg#Cell Phone Handsfree#"
				
				categoryData = categoryData & "/cell-phone-batteries.asp#BATTERIES#batteries.jpg#Cell Phone Batteries#"
				categoryData = categoryData & "/cell-phone-holsters-belt-clips-holders.asp#HOLSTERS/HOLDERS#holsters.jpg#Cell Phone Holsters &amp; Holders#"
				categoryData = categoryData & "/cell-phone-data-cables-memory-cards.asp#DATA/MEMORY#datacables.jpg#Cell Phone Data Cables#"
				categoryData = categoryData & "/bundle-packs.asp#BUNDLE PACKS#bundlePacks.jpg#Cell Phone Accessory Bundle Packs#"
				
				categoryData = categoryData & "/screen-protectors.asp#SCREEN PROTECTORS#screenprotectors.jpg#Cell Phone Screen Protectors#"
				categoryData = categoryData & "/category.asp?categoryid=17#INVISIBLE GADGET GUARDS#fullbodyprotectors.jpg#Cell Phone Full Body Protectors#"
				categoryData = categoryData & "/cell-phone-charms-bling-kits.asp#CHARMS#charms.jpg#Cell Phone Charms#"
				categoryData = categoryData & "/cell-phone-antennas.asp#ANTENNAS/PARTS#antennas.jpg#Cell Phone Antennas"				
				
				categoryArray = split(categoryData,"#")
				
				arrayItem = 0
				catNum = 0
				for i = 1 to 4
				%>
                <tr>
                	<%
					for x = 1 to 4
						catNum = catNum + 1
					%>
                    <td align="center" valign="top" style="padding-left:10px; border-left:1px solid #D8D8D8;<% if x = 4 then %> border-right:1px solid #D8D8D8;<% end if %> border-bottom:1px solid #D8D8D8;">
                        <a href="<%=categoryArray(arrayItem)%>" class="category" title="<%=categoryArray(arrayItem+3)%>"><%=categoryArray(arrayItem+1)%></a><br />
                        <a href="<%=categoryArray(arrayItem)%>"><div id="catImg<%=catNum%>" title="<%=categoryArray(arrayItem+3)%>"></div></a>
                    </td>
                    <%
						arrayItem = arrayItem + 4
					next
					%>
                </tr>
                <% next %>
            </table>
        </td>
    </tr>
    <%
	SQL = "SELECT TOP 4 B.itemID, B.WE_URL, B.itemDesc, B.itemPic, B.price_Retail, B.price_Our, B.HandsfreeType, C.typeName FROM we_recommends A INNER JOIN we_items B ON A.itemID=B.itemID INNER JOIN we_types C ON B.typeID=C.typeID WHERE B.inv_qty <> 0 AND B.hidelive = 0 ORDER BY A.SortOrder"
	session("errorSQL") = SQL
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	%>
    <tr>
    	<td style="padding-top:10px;">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                    <td align="left" valign="top" colspan="4"><div id="featuredProds"></div></td>
                </tr>
                <tr>
                    <%
					a = 0
					do until RS.eof
						a = a + 1
						if RS("HandsfreeType") = 2 then
							singularTypeName = "Bluetooth"
						else
							singularTypeName = RS("typeName")
						end if
					%>
					<td width="25%" align="center" valign="top" style="border-left:1px solid #D8D8D8; border-bottom:1px solid #D8D8D8; padding:10px 0px 10px 0px;<% if a = 4 then %> border-right:1px solid #D8D8D8;<% end if %>">
						<div style="width:185px;">
                        <div style="border:1px solid #D8D8D8; height:120px; width:120px; padding-top:10px;"><a href="/products/<%=RS("WE_URL")%>.asp"><img src="/productpics/thumb/<%=RS("itemPic")%>" width="100" height="100" border="0" alt="<%=RS("itemDesc")%>" /></a></div>
						<p style="line-height:14px;margin-top:4px;"><a class="product-description4" href="/products/<%=RS("WE_URL")%>.asp" title="<%=RS("itemDesc")%>"><%=RS("itemDesc")%></a></p>
						<span class="pricing-gray">List&nbsp;Price:&nbsp;<%=formatCurrency(RS("price_Retail"),2)%></span><br />
						<span class="pricing-orange">Our&nbsp;Price:&nbsp;<%=formatCurrency(RS("price_Our"),2)%></span><br />
						<a class="pricing-gray" href="/products/<%=RS("WE_URL")%>.asp">more&nbsp;detail&nbsp;&gt;&nbsp;&gt;</a>
                        </div>
					</td>
					<%
						RS.movenext
					loop
					RS.close
					set RS = nothing
					%>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
        	<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td align="left" valign="top" colspan="5"><div id="topSelling"></div></td>
                </tr>
                <tr>
                    <%
'					session("breadcrumb_brand") = ""
					
					SQL = "select top 10 a.brandid, a.modelid, a.modelname, a.modelimg, b.brandName, b.brandimg, c.store, c.sumqty from we_models a join we_brands b on a.brandid = b.brandid join temp_topmodels c on a.modelid = c.modelid where c.store = 4 order by c.sumqty desc"  
					session("errorSQL") = SQL
					set RS = server.createobject("ADODB.recordset")
					RS.open SQL, oConn, 0, 1
					dim a, modelLink, altTag
					a = 0
  
					do until RS.eof
						a = a + 1
						modelID = RS("modelID")
						modelName = RS("modelName")
						brandName = RS("brandName")
						modelImg = RS("modelImg")
						
						if RS("modelID") = 940 then
							modelLink = "/apple-ipad-accessories.asp"
						else
							modelLink = "/T-" & RS("modelID") & "-cell-accessories-" & RS("brandName") & "-" & RS("modelName") & ".asp"
						end if
						altTag = brandName & " " & modelName & " Accessories"
					%>
                    <td align="center" valign="top" style="padding:10px 0px 10px 0px;<% if a = 1 or a = 6 then %> border-left:1px solid #D8D8D8;<% end if %><% if a = 5 or a = 10 then %> border-right:1px solid #D8D8D8;<% end if %><% if a > 5 then %> border-bottom:1px solid #D8D8D8;<% end if %>">
                    	<div style="width:155px;">
                        <table border="0" cellpadding="0" cellspacing="0">
                        	<tr>
                            	<td valign="top" style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</td>
                                <td align="left"><a class="TopModelLink" href="<%=modelLink%>" title="<%=RS("brandName") & " " & RS("modelName")%> Accessories"><%=RS("brandName") & " " & replace(RS("modelName"),"/"," / ")%> Accessories</a></td>
                            </tr>
                        </table>
                        </div>
                    </td>
					<%
						if a = 5 then response.Write("</tr><tr>")
						RS.movenext
					loop
					RS.close
					set RS = nothing
					%>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td style="padding:10px 0px 10px 0px;">
        	<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td align="center" valign="top" width="800"><div id="blogHeader"></div></td>
                </tr>
                <%
                SQL = "SELECT TOP 3 A.*, B.nickname, C.b_categoryName FROM b_articles A LEFT JOIN b_user B ON A.b_userid = B.userid LEFT JOIN b_category C ON A.b_categoryid = C.b_categoryid WHERE A.store = 0 AND A.b_parentID = 0 AND A.b_categoryid <> 2 AND (A.b_articlestatus = 1 OR A.b_articlestatus = 3) ORDER BY A.b_firstPostTime DESC"
				session("errorSQL") = SQL
                set RS = Server.CreateObject("ADODB.Recordset")
                RS.open SQL, oConn, 0, 1
                do until RS.eof
                    dim b_firstPostTime, PostMonthName, PostDay, PostYear, b_articleid, b_articleTitle, b_articleContent, nextSpace
                    b_articleid = RS("b_articleid")
					b_articleTitle = RS("b_articleTitle")
					b_articleContent = replace(RS("b_articleContent"),"<br />","")
					b_firstPostTime = dateValue(RS("b_firstPostTime"))
                    PostMonthName = MonthName(month(b_firstPostTime))
                    PostDay = day(b_firstPostTime)
                    PostYear = year(b_firstPostTime)
					lap = 0
					do while instr(b_articleContent,"<") > 0
						lap = lap + 1
						b_articleContent = left(b_articleContent,instr(b_articleContent,"<")-1) & mid(b_articleContent,instr(b_articleContent,">")+1)
					loop
					if len(b_articleContent) > 100 then
    	                nextSpace = inStr(right(b_articleContent,len(b_articleContent) - 100)," ")
        	            b_articleContent = left(b_articleContent,100 + nextSpace) & "..."
					end if
                %>
                <tr>
                    <td width="100%" align="center" valign="top" colspan="3" style="padding-top:10px;">
                        <table bgcolor="#EBEBEB" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td width="100%" align="center" valign="top" colspan="3"><div id="blogPostTop"></div></td>
                            </tr>
                            <tr>
                                <td rowspan="3" align="left" valign="top"><span style="color:#6699CC; font-size:24px; padding-left:7px;">&nbsp;&bull;&nbsp;</span></td>
                                <td align="left" valign="top" class="smlText"><a href="/blog/article-<%=b_articleid & "-" & replace(b_articleTitle,"?","")%>.html" class="blog-link"><%=b_articleTitle%></a>&nbsp;&nbsp;&nbsp;&nbsp;Posted on <%=PostMonthName & " " & PostDay & ", " & PostYear%></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" class="normalText"><%=b_articleContent%></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><a class="blog-full" href="/blog/article-<%=b_articleid & "-" & replace(b_articleTitle,"?","")%>.html">Read the full post &gt;&gt;</a></td>
                            </tr>
                            <tr>
                                <td width="100%" align="center" valign="top" colspan="3"><div id="blogPostBottom"></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%
                    RS.movenext
                loop
                RS.close
                set RS = nothing
                %>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="border-top:1px solid #D8D8D8; padding-top:10px;">
            <div align="justify" class="xtrasmlIndexText">
                These days, phones act as our social hubs, providing us with the ability to connect with friends and family through multiple media outlets. 
                Wireless Emporium knows how important it is to personalize and protect your phone, which is why we offer the largest assortment of cell phone 
                accessories anywhere. With our phones doing so much more than just making calls, they tend to use up battery life quickly. Make sure your 
                phone has the juice to get you through your busy day. Optimize the performance of your phone with our huge selection of cell phone batteries, 
                cell phone chargers, and cell phone holsters. You can also make a statement and show off your personal style. We have thousands of 
                <a href="/cell-phone-covers-faceplates-screen-protectors.asp" class="TopModelLink">cell phone covers</a>, 
                <a href="/cell-phone-cases.asp" class="TopModelLink">cell phone cases</a> 
                and cell phone charms to add a little color to your handheld device. Our full line of cheap cell phone accessories 
                aren't just priced well below retail prices, everything we sell is made from high quality materials and is guaranteed to meet the most 
                stringent of industry standards. Regardless of what brand or model phone you have, Wireless Emporium has the largest selection of accessories 
                to fit your needs. Whether it be a car charger for your Blackberry, a new phone case for your iPhone or a phone cover to protect that new 
                Samsung phone, we have it. We are a leading resource for phone accessories and 
                <a href="/unlocked-cell-phones.asp" class="TopModelLink">unlocked cell phones</a> 
                for every major brand of phone in the market, like HTC, LG, Nokia, Motorola and more. Shop with peace of mind, knowing that all of our 
                products are backed by our 100% satisfaction guarantee and come with free shipping.  Our commitment to customer service is the reason why we 
                have cultivated relationships with so many of our customers, and it's why you should choose Wireless Emporium for all of your cellular needs.
            </div>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom_index.asp"-->