<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;Store/Return Policy
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle" width="100%">
            <h1>Store/Return Policy</h1>
        </td>
    </tr>
    <tr>
        <td class="normaltext">
            <p align="justify">
                <strong>PRODUCT AVAILABILITY:</strong>
                <br>
                Wireless Emporium is committed to offering 
                the lowest prices and most accurate product information 
                available to our customers. Due to our low prices and the 
                rapid turnover of many high-demand accessory items presented 
                on our site, and the limited inventory we have available 
                on-hand, the listing of a particular item or accessory does 
                not constitute a guarantee or promise of availability.
            </p>
            <p align="justify">
                If a product you have ordered is not available 
                for shipment, we will do our best to contact you immediately 
                to provide status on that particular item. Anticipated delivery 
                dates are dependent upon our suppliers and other factors 
                beyond our control, and are subject to change.
            </p>
        </td>
    </tr>
    <tr>
        <td class="normaltext">
            <img src="/images/aboutus/paymethods.gif" width="96" height="110" border="0" align="left">
            <p align="justify">
                <strong>PAYMENT METHODS:</strong>
                <br>
                Wireless Emporium accepts payment via the following authorized
                credit cards: American Express, Discover, MasterCard, Visa, and
                Mastercard/Visa Check Cards. We also accept 3rd party payment
                systems including PayPal, Google Checkout, and eBillme. We do
                not accept cash, personal checks, CODs, money orders, cashier's
                checks, or direct wire transfers. Please be advised that we
                require EXACT U.S. billing addresses in order to fulfill all orders.
                Credit cards with international billing addresses cannot be accepted.
                All prices quoted on the WirelessEmporium.com site are quoted in U.S.
                currency and all orders must be transacted in the same.
                <br><br>
                For large volume orders and orders placed via open account status,
                acceptable forms of payment may also include checks, money orders,
                or cashier's checks denominated in U.S. dollars and drawn on a U.S.
                bank. Terms of payment for large volume orders are determined on a
                case-by-case basis and are subject to change without notice.
            </p>
            <img src="/images/aboutus/refund.gif" width="96" height="62" border="0" align="left">
            <p align="justify">
                <strong>OUR EZ NO-HASSLE &amp; RETURN POLICY:</strong>
                <br>
                Wireless Emporium is happy to offer our exclusive <strong>EZ NO-HASSLE 
                RETURN POLICY -</strong> If you are not happy with a cell phone accessory, 
                you may return the item for 
                REPLACEMENT OR REFUND <strong>within ninety (90) days of receipt</strong>.
                Contact us at:
                <a href="mailto:returns@wirelessemporium.com" title="mailto:returns@wirelessemporium.com">returns@wirelessemporium.com</a>
            </p>
            <p>
                All returns must be accompanied by a Wireless Emporium RMA number. To obtain an RMA number, please
                send an e-mail to:
                <a href="mailto:returns@wirelessemporium.com" title="mailto:returns@wirelessemporium.com">returns@wirelessemporium.com</a>
            </p>
            <p>
                Be sure to include the following information:
                <ul style="margin-top:0px;">
                    <li>Order Number</li>
                    <li>Date of original purchase</li>
                    <li>Reason for request for RMA</li>
                    <li>Your e-mail address</li>
                </ul>
            </p>
            <p>
                <strong>No returns, refunds or exchanges will be processed without an RMA number.</strong>
            </p>
            <p align="justify">
                <strong>NON-DEFECTIVE BLUETOOTH AND WIRED HEADSET RETURNS:</strong>
                <br>
                NON-DEFECTIVE BLUETOOTH AND WIRED HEADSET RETURNS MAY BE SUBJECT TO A 15% RE-STOCKING FEE
                and such returns will be for store credit or refund at the customer's
                request. Refunds are only available for order within 90 days of original
                order date. Please contact us at
                <a href="mailto:returns@wirelessemporium.com">returns@wirelessemporium.com</a>
                if you have questions about which products are returnable and which
                products may be subject to a restocking fee.
                <br><br>
                * PLEASE NOTE: Due to sanitary issues with Bluetooth headsets and wired headsets Wireless Emporium
                Inc. does not accept returns on headset products which have been opened.
                <br><br>
                Customers must contact the headset manufacturer for all defective/warranty
                issues on headsets that have had the packaging opened. For your convenience,
                you can contact us at
                <a href="mailto:returns@wirelessemporium.com">returns@wirelessemporium.com</a>
                for the support phone numbers and email addresses of the various manufacturers.
            </p>
            <p align="justify">
                <strong>CELL PHONE RETURN POLICY:</strong>
                <br>
                All return requests must be made within <strong>7 days</strong> of receiving the item.
                All returned merchandise must be unregistered, in complete original manufacturer's packaging,
                same condition as sold, with all literature, accessories, instructions, blank warranty cards and documentation.
                All Phones must be returned in "Like-New Condition", show no signs of use and must have less than
                <strong>25 minutes</strong> in total cumulative talk time.
                <br><br>
                If an item is defective it may only be exchanged with an equivalent product if we are unable
                to supply the same item or if the item cannot be repaired. Any return shipping cost is the
                responsibility of the customer. All shipping and handling fees are non-refundable.
                This includes all refused and unaccepted packages.
                <br><br>
                All NON-DEFECTIVE cell phone returns will be subject to a 20% restocking fee.
                <br><br>
                For refunds allow 3-4 business days for the refund to post to your account from the time the refund is issued.
                <br><br>
                All returns must be accompanied by an RMA number. To obtain an RMA number, please send an e-mail to:
                <a href="mailto:returns@wirelessemporium.com">returns@wirelessemporium.com</a>
            </p>
            <p align="justify">
                <strong>WARRANTY POLICY FOR CELL PHONE ACCESSORIES:</strong>
                <br>
                Wireless Emporium stands behind the quality 
                of our products with an iron-clad <strong>100% Quality Assurance 
                Guarantee</strong>. All products are warranted for 1 Yr. 
                unless specified otherwise. For warranty replacements, simply 
                ship the item back to Wireless Emporium at the address above, 
                with a note stating the specific problem(s). Warranty replacements 
                take approximately 1-2 weeks to process from the date of 
                receipt. If Wireless Emporium is out-of-stock on the item, 
                Wireless Emporium will replace it with an equivalent product 
                or issue an in-store credit. Warranty does not cover abuse, 
                neglect, or improper installation of product, or acts of 
                nature. PLEASE NOTE: Installation of any of our products 
                are at your own risk. Wireless Emporium is not liable for 
                any damages caused by our products. Damage(s) due to what 
                Wireless Emporium deems customer negligence is not covered 
                under this warranty. For cell phone warranties, please contact the manufacturer.
            </p>
            <p align="justify">
                <strong>CUSTOM & FEATURED ARTIST DESIGN RETURN POLICY:</strong>
                <br>
                Since each item is crafted uniquely for you, customized products may not be returned or exchanged.
            </p>
            <p align="justify">
                <strong>SAFE SHOPPING GUARANTEE:</strong>
                <br>
                The Wireless Emporium Safe Shopping Guarantee 
                protects you while you shop at WirelessEmporium.com, so 
                that you never have to worry about credit card safety. We 
                guarantee that every transaction you make at WirelessEmporium.com 
                will be safe. This means you pay nothing if unauthorized 
                charges are made to your card as a result of shopping at 
                WirelessEmporium.com. In the event of unauthorized use of 
                your credit card, you must notify your credit card provider 
                in accordance with its reporting rules and procedures.
            </p>
            <p align="justify">
                Safe Technology: Our Secure Sockets Layer (SSL) software 
                is the industry standard and among the best software available 
                today for secure online commerce transactions. It encrypts 
                all of your personal information, including credit card 
                number, name, and address, so that it cannot be read as 
                the information travels over the Internet.
            </p>
            <p align="justify">
                In addition to our encryption standards, your Wireless Emporium 
                account information is password protected so that you and 
                only you have access to this personal information. Please 
                do not disclose this password to anyone. Also, remember 
                to close your browser window when your transaction is complete 
                as this will keep your account safe if you use your computer 
                in a public place like an office or library. Wireless Emporium 
                cannot be held responsible if you disclose your account 
                password to other individuals.
            </p>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->