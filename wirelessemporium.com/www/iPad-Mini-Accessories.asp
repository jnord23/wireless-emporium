<%
	response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<%
	'call PermanentRedirect("/accessories-for-apple-ipad-mini.asp")
	'sql = ""
	'session("errorSQL") = sql
	'set	RS = oConn.execute(SQL)
%>

<style>
body {
    /*font-family: "Lucida Grande", Arial, Helvetica, sans-serif;*/
}

#divRumors h2, #divRumors h3, #divRumors dt {
    color:slategray;
}



#divRumors dt, #divRumors dd {
    margin-bottom: 1.0em;
	/*margin-left: 1.5em;*/
}

#divRumors dt {
    font-weight: bold;
	font-size:1.25em;
}



#divRumors dl dd dt {
    color: #000;
    margin-top: 1.0em;
    font-weight: inherit;
    font-style: italic;
}

.divPhotoR {
	margin-top:2.5em;
	margin-left:2.0em;
	margin-bottom:1.5em;
	min-width:1px;
	min-height:1px;
	float:right;
}

.firstPhoto {
	padding-top:0.0em;
}

.nonFirstPhoto {
	margin-top:0.5em;
}

#divRumors .italic {
    font-style: italic;
}

#divRumors .empty {
	display:none;
}

#divRumors img {
	border:none;
}
#divBreadCrumbs {
	margin-bottom:1.0em;
}
</style>

<div id="divRumors">
<div id="divBreadCrumbs" class="breadcrumbFinal">
    Tablet Accessories&nbsp;&gt;&nbsp;
    Apple iPad Mini Accessories&nbsp;&gt;&nbsp;
    Apple iPad Mini Rumors
</div>
<div id="divMainImg" class="divPhotoR">
	<img src="/images/ipad-mini.jpg" width="375" height="411" border="0" alt="iPad Mini" title="iPad Mini">
</div>
    <h1 class="brandModel">
        iPad Mini Rumor Roundup
    </h1>

    <div class="divPhotoR firstPhoto"></div>

    <p>Hot off the heels of the launch of the new iPad, Apple is set to launch another edgy product: The iPad Mini. 
    In anticipation of the latest Apple invention, the tech industry has been abuzz with rumors on everything 
from the release date to specific product specifications.</p>
    
    <p>Regardless of what surprise features might be revealed about the iPad Mini, Wireless Emporium is fully prepared and ready with a slew of products guaranteed to be compatible with the latest Apple iPad. With Wireless Emporium's fast shipping and 90-day return hassle-free return policy, you can order all the latest accessories for your iPad Mini with the greatest of confidence.</p>
    
    <dl>
        <dt class="italic">Name:</dt>
        <dd>
            <p>First came the iPad, then came the iPad 2, then came the…new iPad. More than a few consumers were confused 
            as to Apple's decision to brand the latest iPad as the new iPad as opposed to the chronological iPad 3, 
            which is why experts are assuming that Apple will go the safe route and call their latest product the iPad Mini.</p>
        </dd>
  <dt class="italic">Launch Date:</dt>
        <dd>
            <p>While initial reports speculated that the iPad Mini would launch concurrently with the iPhone 5 on September 12, 
            current rumors indicate that Apple will release both products at separate events. This is most likely because the 
            business-savvy Apple wants to maximize publicity for both products and having separate launch dates will capitalize 
            on media attention for the iPhone 5 and the iPad Mini.</p>
            
            <p>Major online sources such as <a href="http://allthingsd.com/20120825/confirmed-new-ipad-mini-will-debut-in-october-after-latest-iphones-september-bow/" target="_blank">AllThingsD</a> 
            expect that the iPad Mini will launch sometime in October. To top it all off, word on the street is that the 
            iPad Mini product presentation will be held on October 5, the one year anniversary of Steve Jobs' death.</p>
      </dd>
  <dt class="italic">Price:</dt>
    <dd>
    	<p>Early projections have priced the iPad Mini at around $279-299 in order to remain competitive in a tight market. 
        Experts predict that in order to entice buyers to opt for the iPad Mini as opposed to competitor items such as the 
        Google Nexus 7 and Amazon Kindle Fire, both priced at a mere $199, Apple will have to market their new item for under $300.</p>
    </dd>
    <dt class="italic">Form:</dt>
    <dd>
    	<p>According to AppleInsider, Apple's smaller iPad will be the first Apple device to sport a superthin coating of 
        indium tin oxide (ITO) from Taiwan-based Efun Technology. The coating is designed to reduce interference and 
        crossed signals and could improve connectivity capabilities.</p>
    
   	  <p><a href="http://www.tech.sc/ipad-mini-release-date/" target="_blank">Tech Source</a> is predicting that the 
        iPad Mini will sport a 7.85-inch display. Keeping consistent with Jobs' vision for the iPad series, 
      the new iPad Mini will maintain a 4:3 display aspect ratio. </p>
        
   	  <p><a href="http://www.ibtimes.com/articles/377774/20120827/apple-ipad-mini-release-date-rumors-launch.htm" target="_blank">International Business Times</a>
        expects that the iPad Mini will feature a Retina Display, front and rear cameras (iSight and FaceTime), and 
        a smaller dock connector like the one expected to be seen on the  iPhone 5. One of the more exciting rumors 
        surrounding the iPad Mini is that is said to feature an indium gallium zinc oxide (IGZO) display which translates 
      into a brighter and crisper screen than the market's current LCD standards.</p>        
    </dd>
</dl>
</div>

<!--#include virtual="/includes/template/bottom.asp"-->
