<%
	response.buffer = true
	noLeftNav = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<script type="text/javascript" src="/includes/js/account.js"></script>

<% 'Styles must come first so they can overridden when necessary %>
<link rel="stylesheet" type="text/css" href="/includes/css/account_loginForms.css" />

<!--#include virtual="/includes/template/top.asp"-->

<div id="divRegisterWrapper">

  <div class="aLeft halve">
    	
        <h1>Your Account</h1>
    
	    <div id="divMyAccountIcon"></div>
    
    </div>
    
    <div class="aRight halve">
    
    	<div id="divCreateANewAccount"><a href="<%=urlRelLogin%>">Sign In</a></div>
    
    </div>
    
    <div class="hr bow"></div>
    
    <div id="divGenericContainer">
    
		<!--#include virtual="/includes/account/account_leftTower.asp"-->
    
        <div id="divSignUpContainer">
            
            <div id="divSignUp">
        
        <h2>Sorry</h2>
        
        <p>We encountered a problem logging you in.  If you wish, you may try again.</p>
        
        <p>You may still browse our vast collection of cell phone accessories for your device.</p>
        
            </div>
                
        </div>
            
        <div class="divMarginBottom"></div>

	</div>
  
</div>

<div id="divSpaceAtBottom"></div>













    

<!--#include virtual="/includes/template/bottom.asp"-->
