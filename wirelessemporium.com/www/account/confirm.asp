<%
	response.buffer = true
	noLeftNav = 1
	mobileAccess = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
	pageTitle = ""
	email = prepStr(request.QueryString("email"))
	if email <> "" and isEmail(email) then
		sql = "update " & getAccountsTable & " " &_
		"set isActive = 1 " &_
		"where email = '" & email & "' and isMaster = 1"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		cSuccess = true
	else
		cSuccess = false
	end if

	use500 = true
%>
<!--#include virtual="/includes/template/top.asp"-->
<script type="text/javascript" src="/includes/js/account.js"></script>

<% 'Styles must come first so they can overridden when necessary %>
<link rel="stylesheet" type="text/css" href="/includes/css/account_loginForms.css" />
<script>_ffLoyalty.initialize("k4BSyfSvY3t2Hhx");</script>

<div id="divRegisterWrapper">

  <div class="aLeft halve">
    	
        <h1>Your Account</h1>
    
	    <div id="divMyAccountIcon"></div>
    
    </div>
    
    <div class="aRight halve">
    
    	<div id="divCreateANewAccount"><a href="<%=urlRelLogin%>">Sign In</a></div>
    
    </div>
    
    <div class="hr bow"></div>
    
    <div id="divGenericContainer">
    
		<!--#include virtual="/includes/account/account_leftTower.asp"-->
    
        <div id="divSignUpContainer">
            
            <div id="divSignUp">
        
            <% if cSuccess then %>
                <h2>Registration Confirmed</h2>
                <p class="pInstructions">Thank you for confirming your account.  You may now <a href="<%=urlRelLogin%>">Sign In</a>.</p>
                <!--<img src="<%=useHttp%>://loyalty.500friends.com/api/enroll.gif?uuid=<%=WE_500_ACCID%>&email=<%=email%>" style="position: absolute; left: -10px; visibility: hidden;" />-->
                <img src="<%=useHttp%>://loyalty.500friends.com/api/enroll.gif?uuid=<%="k4BSyfSvY3t2Hhx"%>&email=<%=email%>" style="position: absolute; left: -10px; visibility: hidden;" />
            <% else %>
                <h2>Confirmation Failed</h2>
                <p class="pInstructions">An error occurred while confirming your account.</p>
                <p class="pInstructions">Please be sure that you have copied the entire link into your browser.</p>
            <% end if %>
        
            </div>
                
        </div>
            
        <div class="divMarginBottom"></div>
        
	</div>
  
</div>

<div id="divSpaceAtBottom"></div>



<!--#include virtual="/includes/template/bottom.asp"-->
