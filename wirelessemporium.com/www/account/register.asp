<%
	response.buffer = true
	noleftnav = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<% 'Styles must come first so they can overridden when necessary %>
<% pageTitle = "Register" %>
<!--#include virtual="/includes/template/top.asp"-->
<script type="text/javascript" src="/includes/js/account.js"></script>
<link rel="stylesheet" type="text/css" href="/includes/css/account_loginForms.css" />
<%
	if prepStr(request.form("submitButton")) = "Register" then

		'Clean Data				
		firstName = prepStr(request.form("fName"))
		lastName = prepStr(request.form("lName"))
		email = prepStr(request.form("email"))
		password = prepStr(request.form("password"))
		passwordConfirm = prepStr(request.form("passwordConfirm"))
		terms = prepStr(request.form("terms"))
		
		'Validate Data		
		isValid = true 'This flag is true until it's not
		if firstName = "" then
			%><style>#rqdFName{display:inline;}</style><%
			isValid = false
		end if
		if lastName = "" then
			%><style>#rqdLName{display:inline;}</style><%
			isValid = false
		end if
		if email = "" then
			%><style>#rqdEmail{display:inline;}</style><%
			isValid = false
		end if
		if accountEmailExists(email) then
			%><style>#pActiveEmailAlreadyExists{display:inline;}</style><%
			%><style>#pForgottenPassword{display:inline;}</style><%
			isValid = false
		end if
		if not isEmail(email) then
			%><style>#valEmail{display:inline;}</style><%
			isValid = false
		end if
		if password = "" then
			%><style>#rqdPassword{display:inline;}</style><%
			isValid = false
		end if
		if not terms = "true" then
			%><style>#rqdTerms{display:inline;}</style><%
			isValid = false
		end if


		
		if isValid then
		
			'Insert new record into database
			sql = "INSERT INTO " & getAccountsTable & " " &_
			"(fname, lname, pword, email, dateEntered, isMaster) " &_
			"VALUES ('" & firstName & "', '" & lastName & "', '" & password & "', '" & email & "', '" & now & "', '1')"
			session("errorSQL") = sql
			oConn.execute sql
	
			'Build Confirmation Link
			cLink = "http://" & request.ServerVariables("HTTP_HOST") & "/account/confirm.asp?email=" & email
			
			'Send confirmation email
			strFrom = "support@wirelessemporium.com"
			strSubject = "Wireless Emporium Registration: Confirmation Required"
			strBody = "To complete your account registration, please visit the following link in your browser:" & vbcrlf & vbcrlf & "<a href=""" & cLink & """ target=""_blank"">" & cLink & "</a>"  & vbcrlf & vbcrlf
			strTo = email
			weEmail strTo,strFrom,strSubject,strBody
			
			'Redirect to register_done.asp
			Response.Redirect ("/account/register_done.asp")
			
		end if
		
		'The script will complete and the form will be re-presented to the user
		
		passType = "password"

		
	else
		
		'This is a fresh load and we will pre-populate the form with ghost text
		firstName = "First Name"
		lastName = "Last Name"
		email = "Email Address"
		password = "Password"
		passwordConfirm = "Confirm Password"
		
		passType = "text"
	
	end if	
	

	'sql = ""
	'session("errorSQL") = sql
	'set rs = oConn.execute(sql)
%>

<% 'if viewToPresent = "Form" then %>
<script type="text/javascript">
function chkField(inputField, errorMessage) {
	var f = document.getElementById(inputField);
	//var f = document.getElementsByName(inputField).item(0).value;//Works
	//alert(f);
	var e = document.getElementById(errorMessage);
	//alert(e);
	if(f.value == '') {
		e.style.display = 'inline';
		//alert('value is blank');
	}else{
		e.style.display = 'none';
		//alert('value NOT blank');
	};
	if(inputField == 'email'){//hide email invalid message if changed
		var eVal = document.getElementById('valEmail');
		if(f.value != ''){
			eVal.style.display = 'none';
		}
	}
	if(inputField == 'password' || inputField == 'passwordConfirm'){//Match password fields
		var p1 = document.getElementById('password');
		var p2 = document.getElementById('passwordConfirm');
		var eVal = document.getElementById('valPassword');
		if(p1.value == p2.value){
			eVal.style.display = 'none';
		}else{
			eVal.style.display = 'inline';
		}
	}
	//Check for ghost values
	var fv = f.value;
	if (fv == 'Email Address' || fv == 'Password' || fv == 'First Name' || fv == 'Last Name' || fv == 'Confirm Password') {
		e.style.display = 'inline';
	}

};
function chkBox(inputBox, errorMessage) {
	var b = document.getElementById(inputBox);
	//alert(b);
	//alert(b.checked ? 'true' : 'false');
	//alert(b.checked);
	var e = document.getElementById(errorMessage);
	if (b.checked){
		e.style.display = 'none';
	}else{
		e.style.display = 'inline';
	}
};
function chkForm() {
	chkField('fName','rqdFName');	
	chkField('lName','rqdLName');
	chkField('email','rqdEmail');
	chkField('password','rqdPassword');
	chkField('passwordConfirm','valPassword');
	chkBox('terms','rqdTerms');
	
	var e1 = document.getElementById('rqdFName').style.display;
	var e2 = document.getElementById('rqdLName').style.display;
	var e3 = document.getElementById('rqdEmail').style.display;
	var e4 = document.getElementById('valEmail').style.display;
	var e5 = document.getElementById('rqdPassword').style.display;
	var e6 = document.getElementById('valPassword').style.display;
	var e7 = document.getElementById('rqdTerms').style.display;

	if (e1 == 'inline' || e2 == 'inline' || e3 == 'inline' || e4 == 'inline' || e5 == 'inline' || e6 == 'inline' || e7 == 'inline') {
		//alert('Please complete the entire form and try again.');
		return false;
	}else{
		return true;
	}
}
</script>



<div id="divRegisterWrapper">

  <div class="aLeft halve">
    	
        <h1>Your Account</h1>
    
	    <div id="divMyAccountIcon"></div>
    
    </div>
    
    <div class="aRight halve">
    
    	<div id="divCreateANewAccount"><a href="<%=urlRelLogin%>">Sign In</a></div>
    
    </div>
    
    <div class="hr bow"></div>
    
    <div id="divGenericContainer">
    
		<!--#include virtual="/includes/account/account_leftTower.asp"-->
            
        <div id="divSignUpContainer">
            
            <div id="divSignUp">
        
                <h2>Sign Up</h2>
                
                <p class="pInstructions">
                    Simply fill out the forms below, or <a href="<%=urlRelLogin%>">Sign In <span>&gt;&gt;<span></a>
                </p>
                
                <p class="errorMessage hide pInstructions" id="pActiveEmailAlreadyExists">An existing account already exists for that email address.</p>
    
                <p class="errorMessage hide pInstructions" id="pForgottenPassword">Did you <a href="<%=urlRelForgotPassword%>">forget your password</a>?</p>
                
                <form name="RegForm" action="" method="post" onsubmit="return chkForm()">
                    <div class="formRow">
                        <input type="text" id="fName" name="fName" value="<%=firstName%>" onblur="chkField('fName','rqdFName');replaceGhost(this);" onfocus="removeGhost(this)" /><span class="errorMessage" id="rqdFName"> * Required</span>
                    </div>
                    
                    <div class="formRow">
                        <input type="text" id="lName" name="lName" value="<%=lastName%>" onblur="chkField('lName','rqdLName');replaceGhost(this)" onfocus="removeGhost(this)" /><span class="errorMessage" id="rqdLName"> * Required</span>
                    </div>
                    
                    <div class="formRow">
                        <input type="text" id="email" name="email" value="<%=email%>" onblur="chkField('email','rqdEmail');replaceGhost(this);" onfocus="removeGhost(this)" /><span class="errorMessage" id="rqdEmail"> * Required</span><span class="errorMessage" id="valEmail"> * Invalid</span>
                    </div>
                    
                    <div class="formRow">
                        <input type="<%=passType%>" id="password" name="password" value="<%=password%>" onblur="chkField('password','rqdPassword');replaceGhost(this);passOnblur(this);" onfocus="passOnfocus(this);" /><span class="errorMessage" id="rqdPassword"> * Required</span>
                    </div>
                    
                    <div class="formRow">
                        <input type="<%=passType%>" id="passwordConfirm" name="passwordConfirm" value="<%=passwordConfirm%>" onblur="chkField('passwordConfirm','valPassword');replaceGhost(this);passOnblur(this);" onfocus="passOnfocus(this);" /><span class="errorMessage" id="valPassword"> * Must Match</span>
                    </div>
                    
                    <div class="formRowExcception" id="divTerms">
                        <div id="checkBoxTermsSurrogate" onclick="checkToggle(this,'terms');setTimeout(function(){chkBox('terms','rqdTerms')}, 500);" class="unchecked"></div>
                        <div id="divAgreementText">I have read and agreed to the <a href="http://www.wirelessemporium.com/termsofuse.asp">Terms of Service</a> <br />and <a href="http://www.wirelessemporium.com/privacy.asp">Privacy Policy</a>.</div><span class="errorMessage" id="rqdTerms"> * Required</span><input tabindex="-1" type="checkbox" id="terms" name="terms" value="true" /><script type="text/javascript">document.getElementById('terms').style.display = 'none';</script>
                    </div>
                    
                    <div class="formRowException">
                        <input type="submit" id="createAccountButton" class="btnSubmit" name="submitButton" value="Register" />
                    </div>
                    
                </form>
        
            </div>
                
        </div>
            
        <div class="divMarginBottom"></div>
        
	</div>

</div>
   
<div id="divSpaceAtBottom"></div>

<!--#include virtual="/includes/template/bottom.asp"-->
