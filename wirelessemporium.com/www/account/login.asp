<%
	Response.Expires = -1
	Response.Expiresabsolute = Now() - 1 
	Response.AddHeader "pragma","no-cache" 
	Response.AddHeader "cache-control","private" 
	Response.CacheControl = "no-cache"
	noleftnav = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<% pageTitle = "Login" %>
<!--#include virtual="/includes/template/top.asp"-->
<script type="text/javascript" src="/includes/js/account.js"></script>


<% 'Styles must come first so they can overridden when necessary %>
<link rel="stylesheet" type="text/css" href="/includes/css/account_loginForms.css" />

<%
	if isLoggedIn then
'		response.redirect urlRelMyAccount
		response.redirect "/account/account_details.asp"
	end if
	
	if prepStr(request.form("submitButton")) = "Login" then

		'Clean Data				
		inputEmail = prepStr(request.form("inputEmail"))
		'if inputEmail = "Email Address" then inputEmail = ""
		inputPassword = prepStr(request.form("inputPassword"))
		'if inputPassword = "Password" then inputEmail = ""
		
		'Validate Data
		isValid = true
		if inputEmail = "" then
			%><style>#rqdEmail{display:inline;}</style><%
			isValid = false
		end if
		if not isEmail(inputEmail) then
			%><style>#valEmail{display:inline;}</style><%
			isValid = false
		end if
		if inputPassword = "" then
			%><style>#rqdPassword{display:inline;}</style><%
			isValid = false
		end if
		
		if isValid then
		
			'Validate Credentials
			
			if validateUser(inputEmail, inputPassword) then
				'Redirect to custom home page
'				Response.Redirect urlRelMyAccount
				response.redirect "/account/account_details.asp"
			else
				'Present with another error
				%><style>#pLoginFailed{display:inline;}</style><%
			end if
			
			
		end if
		
		'The script will complete and the form will be re-presented to the user

		passType = "password"

	else
		
		'This is a fresh load and we will pre-populate the form with ghost text
		inputEmail = "Email Address"
		inputPassword = "Password"
	
		passType = "text"
	
	end if	
	
	'sql = ""
	'session("errorSQL") = sql
	'set rs = oConn.execute(sql)
%>

<script type="text/javascript">
function chkField(inputField, errorMessage) {
	//alert("test")
	//alert(document.LoginForm_B)
	//var f = eval("document.LoginForm_B."  + inputField + ".value");//Works
	var f = document.getElementsByName(inputField).item(0).value;//Works
	//alert(f)
	var e = document.getElementById(errorMessage);
	if(f == '') {
		e.style.display = 'inline';
		//alert('value is blank');
	}else{
		e.style.display = 'none';
		//alert('value NOT blank');
	};
	if(inputField == 'inputEmail'){
		var eVal = document.getElementById('valEmail');
		if(f.value != ''){
			eVal.style.display = 'none';
		}
	}
};
function chkForm() {
	chkField('inputEmail','rqdEmail');
	chkField('inputPassword','rqdPassword');
	
	var e3 = document.getElementById('rqdEmail').style.display;
	var e4 = document.getElementById('valEmail').style.display;
	var e5 = document.getElementById('rqdPassword').style.display;


	if (e3 == 'inline' || e4 == 'inline' || e5 == 'inline') {
		//alert('Please complete the entire form and try again.');
		return false;
	}else{
		return true;
	}
}
</script>

<div id="divRegisterWrapper">

  <div class="aLeft halve">
    	
        <h1>Your Account</h1>
    
	    <div id="divMyAccountIcon"></div>
    
    </div>
    
    <div class="aRight halve">
    
    	<div id="divCreateANewAccount"><a href="/account/register.asp">Create a New Account</a></div>
    
    </div>
    
    <div class="hr bow"></div>
    
    <div id="divGenericContainer">
    
		<!--#include virtual="/includes/account/account_leftTower.asp"-->
            
        <div id="divSignUpContainer">
            
            <div id="divSignUp">
        
                <h2>Sign In</h2>
                
                <p class="pInstructions" style="font-size:12px; padding-right:5px;">
					Welcome back! To access your account, please enter your email address and password. If you do not have an account or are already registered under our WEbucks Loyalty Program, you will have to re-register with our
					new login system to gain access to your account. Don't worry-it's easy and simple to do! <a href="<%=urlRelRegister%>">Create an Account <span>&gt;&gt;</span></a>
                </p>
    
                <p class="errorMessage hide pInstructions" id="pLoginFailed">Your email and password could not be verified.  Please try again.</p>
                
                <form name="LoginForm_B" action="" method="post" onsubmit="return chkForm()">
                    <div class="formRow">
                        <input type="text" id="inputEmail" name="inputEmail" value="<%=inputEmail%>" onblur="chkField('inputEmail','rqdEmail');replaceGhost(this);" onfocus="removeGhost(this)" /><span class="errorMessage" id="rqdEmail"> * Required</span><span class="errorMessage" id="valEmail"> * Invalid</span>
                    </div>
                    
                    <div class="formRow">
                        <input type="<%=passType%>" id="inputPassword" name="inputPassword" value="<%=inputPassword%>" onblur="chkField('inputPassword','rqdPassword');replaceGhost(this);passOnblur(this);" onfocus="passOnfocus(this);" /><span class="errorMessage" id="rqdPassword"> * Required</span>
                    </div>
                    
                    <div class="formRowException">
                        <div class="relContainer">
                            <div id="divSignInButton"><input type="submit" id="signInButton" class="btnSubmit" name="submitButton" value="Login" /></div>
                        </div>
                        <div id="divPword"><a href="<%=urlRelForgotPassword%>">Forgot Password?</a></div>
                    </div>
                    
                </form>
        
            </div>
                
        </div>
            
        <div class="divMarginBottom"></div>

	</div>
  
</div>

<div id="divSpaceAtBottom"></div>

<%   
'    <h1>Login</h1>
'    
'    <p>To login to your account, please complete the form below:</p>    
'
'	[ErrorMessage]
'
'    <div id="theForm">
'    	<form name="LoginForm_B" action="" method="post" onsubmit="return chkForm()">
'        <table width="" border="0" cellspacing="15" cellpadding="0">
'          <tr>
'            <td width="100" align="right"><label for="inputEmail">Email Address</label></td>
'            <td>[Email]</td>
'          </tr>
'          <tr>
'            <td align="right"><label for="inputPassword">Password</label></td>
'            <td>[Password]</td>
'          </tr>
'          <tr>
'            <td align="right">&nbsp;</td>
'            <td>[Login]</td>
'          </tr>
'          <tr>
'            <td align="right">&nbsp;</td>
'            <td>&nbsp;</td>
'          </tr>
'        </table>
'      </form>
'    <p>Forgot your password?  Click <a href="/account/password_forgot.asp">here</a>.</p>
'    <p>If you do not have an account, please <a href="/account/register.asp">register</a>.</p>    
'   </div>
'    <div style="float:left; width:56px; height:250px;"><img src="/images/cart/or_bar_250.jpg" border="0" /></div>
'    <div id="frmFacebook" style="float:left; width:325px; padding-top:80px; text-align:center;">
'    	<a href="/account/fb/fb.asp"><img src="/images/img_fb_login.jpg" border="0" /></a>
'    </div>
%>
    
<!--#include virtual="/includes/template/bottom.asp"-->
