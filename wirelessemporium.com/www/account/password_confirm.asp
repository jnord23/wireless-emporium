<%
	response.buffer = true
	noLeftNav = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<% pageTitle = "" %>
<!--#include virtual="/includes/template/top.asp"-->
<script type="text/javascript" src="/includes/js/account.js"></script>

<% 'Styles must come first so they can overridden when necessary %>
<link rel="stylesheet" type="text/css" href="/includes/css/account_loginForms.css" />
<%		
	'Prep input
	email = prepStr(request.QueryString("email"))
	
	'Is this a postback?
	if request.form("submitButton") = "Reset" then
		
		newPassword = prepStr(request.form("password"))
		newPasswordConfirm = prepStr(request.form("passwordConfirm"))
		email = prepStr(request.form("email"))
		
		'Validate Data		
		isValid = true 'This flag is true until it's not
		if newPassword = "" then
			%><style>#rqdPassword{display:inline;}</style><%
			isValid = false
		end if
		if newPassword <> newPasswordConfirm then
			%><style>#valPassword{display:inline;}</style><%
			isValid = false
		end if

		if isValid then
		
			sql = "update " & getAccountsTable & " set pword = '" & newPassword & "' " &_
			"where email = '" & email & "' " &_
			"and isMaster = 1"
			oConn.Execute(sql)
			
			'response.write "email: " & email
			'response.end
			
			'Build  Link
			cLink = "http://" & Request.ServerVariables("HTTP_HOST") & "/contact-us.html"
			
			'Send email
			strFrom = "support@wirelessemporium.com"
			strSubject = "Password Change: Confirmation"
			strBody = "Your Wireless Emporium account password has been reset.  If you did not initiate this action, please contact our support:" & vbcrlf & vbcrlf & "<a href=""" & cLink & """ target=""_blank"">" & cLink & "</a>"  & vbcrlf & vbcrlf
			strTo = email
			weEmail strTo,strFrom,strSubject,strBody
			
			accountLogIn(email)
			response.redirect "/account/password_confirm_done.asp"
		
		end if
		
	else
		
		'Check if request is valid
		if email <> "" and isEmail(email) then
		
			if accountEmailExists(email) then
				'response.write "email exists"
				'Accounts exists and reset can continue
				cSuccess = true
			else
				cSuccess = false
			end if

			'This is a fresh load and we will pre-populate the form with ghost text
			password = "Password"
			passwordConfirm = "Confirm Password"
			
			passType = "text"
			
		'If not valid
		else
			
			'response.write "not valid email"
			'Display error message
			cSuccess = false
			
		end if
	
	end if
%>
<script type="text/javascript">
function chkField(inputField, errorMessage) {
	var f = document.getElementById(inputField);
	//var f = document.getElementsByName(inputField).item(0).value;//Works
	var e = document.getElementById(errorMessage);
	if(f.value == '') {
		e.style.display = 'inline';
		//alert('value is blank');
	}else{
		e.style.display = 'none';
		//alert('value NOT blank');
	};
	if(inputField == 'email'){//hide email invalid message if changed
		var eVal = document.getElementById('valEmail');
		if(f.value != ''){
			eVal.style.display = 'none';
		}
	}
	if(inputField == 'password' || inputField == 'passwordConfirm'){//Match password fields
		var p1 = document.getElementById('password');
		var p2 = document.getElementById('passwordConfirm');
		var eVal = document.getElementById('valPassword');
		if(p1.value == p2.value){
			eVal.style.display = 'none';
		}else{
			eVal.style.display = 'inline';
		}
	}
	
};
function chkForm() {
	chkField('email','rqdEmail');
	chkField('password','rqdPassword');
	chkField('passwordConfirm','valPassword');
	
	var e3 = document.getElementById('rqdEmail').style.display;
	var e4 = document.getElementById('valEmail').style.display;
	var e5 = document.getElementById('rqdPassword').style.display;
	var e6 = document.getElementById('valPassword').style.display;


	if (e3 == 'inline' || e4 == 'inline' || e5 == 'inline' || e6 == 'inline') {
		//alert('Please complete the entire form and try again.');
		return false;
	}else{
		return true;
	}
}
</script>

<div id="divRegisterWrapper">

  <div class="aLeft halve">
    	
        <h1>Your Account</h1>
    
	    <div id="divMyAccountIcon"></div>
    
    </div>
    
    <div class="aRight halve">
    
    	<div id="divCreateANewAccount"><a href="<%=urlRelLogin%>">Sign In</a></div>
    
    </div>
    
    <div class="hr bow"></div>
    
    <div id="divGenericContainer">
    
		<!--#include virtual="/includes/account/account_leftTower.asp"-->

        <div id="divSignUpContainer">
            
            <div id="divSignUp">
    
            <% if cSuccess then %>
    
                <h2>Reset Your Password</h2>
                
                <p class="pInstructions">Complete the form below to reset your password.</p>
    
                <form name="ResetForm" action="" method="post" onsubmit="return chkForm()">
                    <div class="formRow">
                        <span onclick="document.getElementById('password').focus();"><input type="hidden" id="email" name="email" value="<%=email%>" /><input type="text" id="emailDisabled" name="emailDisabled" value="<%=email%>" onblur="chkField('emailDisabled','rqdEmail')" disabled="disabled" /></span><span class="errorMessage" id="rqdEmail"> * Required</span><span class="errorMessage" id="valEmail"> * Invalid</span>
                    </div>
                    
                    <div class="formRow">
                        <input type="<%=passType%>" id="password" name="password" value="<%=password%>" onblur="chkField('password','rqdPassword');replaceGhost(this);passOnblur(this);" onfocus="passOnfocus(this);" /><span class="errorMessage" id="rqdPassword"> * Required</span>
                    </div>
                    
                    <div class="formRow">
                        <input type="<%=passType%>" id="passwordConfirm" name="passwordConfirm" value="<%=passwordConfirm%>" onblur="chkField('passwordConfirm','valPassword');replaceGhost(this);passOnblur(this);" onfocus="passOnfocus(this);" /><span class="errorMessage" id="valPassword"> * Must Match</span>
                    </div>
                    
                    <div class="formRowExcception" id="divTerms">
                        <input type="submit" id="resetConfirmButton" class="btnSubmit" name="submitButton" value="Reset">
                    </div>
                    
                    
                </form>
        
            <% else %>
                <h2>Confirmation Failed</h2>
                <p class="pInstructions">An error occurred while confirming your account.</p>
                <p class="pInstructions">Please be sure that you have copied the entire link into your browser.</p>
            <% end if %>
            
            </div>
                
        </div>
            
        <div class="divMarginBottom"></div>
	
    </div>
  
</div>

<div id="divSpaceAtBottom"></div>

<%
'    <div id="divConfirmationWrapper">
'
'	<% if cSuccess then $>
'        <h1>Reset Your Password</h1>
'        
'    <div id="theForm">
'    	<form name="ResetForm" action="" method="post" onsubmit="return chkForm()">
'            	
'<table width="700" border="0" cellspacing="15" cellpadding="0">
'  <tr>
'    <td width="100" align="right"><label for="email">Email</label></td>
'    <td>[Email]</td>
'  </tr>
'  <tr>
'    <td align="right"><label for="password">New Password</label></td>
'    <td>[Pword]</td>
'  </tr>
'  <tr>
'    <td align="right"><label for="passwordConfirm">Confirm  Password</label></td>
'    <td>[PConfirm]</td>
'  </tr>
'  <tr>
'    <td align="right">&nbsp;</td>
'    <td>[Submit]</td>
'  </tr>
'  <tr>
'    <td align="right">&nbsp;</td>
'    <td>&nbsp;</td>
'  </tr>
'</table>
'
'
'      </form>
'    </div>
'                
'    <% else $>
'        <h1>Confirmation Failed</h1>
'        <p>An error occurred while confirming your account.</p>
'        <p>Please be sure that you have copied the entire link into your browser.</p>
'    <% end if $>
'
'
'
'    </div>
%>    
    
    
    
<!--#include virtual="/includes/template/bottom.asp"-->
