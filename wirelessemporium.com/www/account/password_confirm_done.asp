<%
	response.buffer = true
	noLeftNav = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
	pageTitle = ""
	
	'sql = ""
	'session("errorSQL") = sql
	'set rs = oConn.execute(sql)
%>
<!--#include virtual="/includes/template/top.asp"-->
<script type="text/javascript" src="/includes/js/account.js"></script>

<% 'Styles must come first so they can overridden when necessary %>
<link rel="stylesheet" type="text/css" href="/includes/css/account_loginForms.css" />

<div id="divRegisterWrapper">

	<div class="aLeft halve">
    	
        <h1>Your Account</h1>
    
	    <div id="divMyAccountIcon"></div>
    
    </div>
    
    <div class="aRight halve">
    
    	<div id="divCreateANewAccount"><a href="<%=urlRelLogin%>">Sign In</a></div>
    
    </div>
    
    <div class="hr bow"></div>
    
    <div id="divGenericContainer">
    
		<!--#include virtual="/includes/account/account_leftTower.asp"-->
    
        <div id="divSignUpContainer">
            
            <div id="divSignUp">
    
                <h2>Password Reset</h2>
                
                <p class="pInstructions">Your password has been reset.</p>
                <p class="pInstructions">Proceed to <a href="/account/account_details.asp">My Account</a>.</p>
            
            </div>
                
        </div>

        <div class="divMarginBottom"></div>

	</div>
    
</div>

<div id="divSpaceAtBottom"></div>
















<%
'
'<div id="divRegistrationWrapper">
'
'	<h1>Password Reset</h1>
'    
'	<p>Your password has been reset.</p>
'    
'    <p>Proceed to <a href="/account/account_details.asp">My Account</a>.</p>
'    
'</div>
%>

<!--#include virtual="/includes/template/bottom.asp"-->
