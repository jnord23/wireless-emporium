<%
	Response.Expires = -1
	Response.Expiresabsolute = Now() - 1 
	Response.AddHeader "pragma","no-cache" 
	Response.AddHeader "cache-control","private" 
	Response.CacheControl = "no-cache"
	
	use500 = true
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<% pageTitle = "My Account" %>
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/framework/utility/md5hash.asp"-->
<!--#include virtual="/framework/utility/json/json2.asp"-->
<%
	uuid = WE_500_ACCID
	userEmail = prepStr(session("accountEmail"))
	if userEmail = "" then response.redirect "/account/login"
	secKey = WE_500_SECKEY
	strSig = secKey & "email" & userEmail & "uuid" & uuid
	sig = MD5(strSig)
	authtoken = ""

	useURL = "https://loyalty.500friends.com/data/customer/auth_token?uuid=" & uuid & "&email=" & userEmail & "&sig=" & sig
'	response.write "useURL:" & useURL & "<br><br><br>"
	set XMLHTTP = server.CreateObject("Msxml2.ServerXMLHTTP.6.0")
	XMLHTTP.Open "GET", useURL, False
	XMLHTTP.Send

'	response.write "response text returned from 500 server:" & XMLHTTP.ResponseText & "<br><br><br>"
	set o = JSON.parse(join(array(XMLHTTP.ResponseText)))
	if o.success then authtoken = o.data.auth_token
%>
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="center" valign="top" width="100%">
			<iframe id="ff_member_iframe" style="width:100%; height:auto; border:0;" frameBorder="0"></iframe>
        </td>
    </tr>
</table>
<script type="text/javascript">
	_ffLoyalty.loadIframe({ email: '<%=userEmail%>', auth_token: '<%=authtoken%>', auto_resize: true  });
</script>


<!--#include virtual="/includes/template/bottom.asp"-->
