<%
	response.buffer = true
	noLeftNav = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<% pageTitle = "Forgot Password" %>
<!--#include virtual="/includes/template/top.asp"-->
<script type="text/javascript" src="/includes/js/account.js"></script>

<% 'Styles must come first so they can overridden when necessary %>
<link rel="stylesheet" type="text/css" href="/includes/css/account_loginForms.css" />
<style type="text/css">
#divForgotWrapper {
/*	padding:3.0em;*/
	margin:20px 0px 0px 20px;
}
#divForgotWrapper a {
	font-size:inherit;
}
.errorMessage {
	color:red;
	display:none;
	font-size:smaller;
}
.hide {
	display:none;
}
</style>

<%
	if isLoggedIn then
'		response.redirect urlRelMyAccount
		response.redirect "/account/account_details.asp"
	end if
	
	if prepStr(request.form("submitButton")) = "Submit" then

		'Clean Data				
		inputEmail = prepStr(request.form("inputEmail"))
		'if inputEmail = "Email Address" then inputEmail = ""
		
		
		'Validate Data
		isValid = true
		if inputEmail = "" then
			%><style>#rqdEmail{display:inline;}</style><%
			isValid = false
		end if
		if not isEmail(inputEmail) then
			%><style>#valEmail{display:inline;}</style><%
			isValid = false
		end if
		
		if isValid then
		
			'Validate Credentials

			if accountEmailExists(inputEmail) then
				'Build Confirmation Link
				cLink = "http://" & Request.ServerVariables("HTTP_HOST") & "/account/password_confirm.asp?email=" & inputEmail
				
				'Send email
				strFrom = "support@wirelessemporium.com"
				strSubject = "Forgot Password: Action Required"
				strBody = "To reset your account password, please visit the following link in your browser:" & vbcrlf & vbcrlf & "<a href=""" & cLink & """ target=""_blank"">" & cLink & "</a>"  & vbcrlf & vbcrlf
				strTo = inputEmail
				weEmail strTo,strFrom,strSubject,strBody
				
				Response.Redirect ("/account/password_done.asp")
			else
				'Present with another error
				%><style>#pForgotFailed{display:inline;}</style><%
			end if
			
			
		end if
		
		'The script will complete and the form will be re-presented to the user
		
	else
		
		'This is a fresh load and we will pre-populate the form with ghost text
		inputEmail = "Email Address"
	
	end if	
	
	'sql = ""
	'session("errorSQL") = sql
	'set rs = oConn.execute(sql)
%>

<script type="text/javascript">
function chkField(inputField, errorMessage) {
	//alert("test")
	//alert(document.ForgotPasswordForm)
	//var f = eval("document.ForgotPasswordForm."  + inputField + ".value");//Works
	var f = document.getElementsByName(inputField).item(0).value;//Works
	//alert(f)
	var e = document.getElementById(errorMessage);
	if(f == '') {
		e.style.display = 'inline';
		//alert('value is blank');
	}else{
		e.style.display = 'none';
		//alert('value NOT blank');
	};
	if(inputField == 'inputEmail'){
		var eVal = document.getElementById('valEmail');
		if(f.value != ''){
			eVal.style.display = 'none';
		}
	}
};
function chkForm() {
	chkField('inputEmail','rqdEmail');
	
	var e3 = document.getElementById('rqdEmail').style.display;
	var e4 = document.getElementById('valEmail').style.display;


	if (e3 == 'inline' || e4 == 'inline') {
		//alert('Please complete the entire form and try again.');
		return false;
	}else{
		return true;
	}
}
</script>

<div id="divRegisterWrapper">

  <div class="aLeft halve">
    	
        <h1>Your Account</h1>
    
	    <div id="divMyAccountIcon"></div>
    
    </div>
    
    <div class="aRight halve">
    
    	<div id="divCreateANewAccount"><a href="javascript:void(0);" style="cursor:default">Forgot Your Password?</a></div>
    
    </div>
    
    <div class="hr bow"></div>
    
    <div id="divGenericContainer">
    
		<!--#include virtual="/includes/account/account_leftTower.asp"-->
            
        <div id="divSignUpContainer">
            
            <div id="divSignUp">
        
                <h2>Reset Your Password</h2>
                
                <p class="pInstructions">
                    Enter your email address below (<em>example: name@domain.com</em>)
                </p>
               
                <p class="errorMessage hide pInstructions" id="pForgotFailed">No account found under that email address.  Please try again.</p>
                
                <form name="ForgotPasswordForm" action="" method="post" onsubmit="return chkForm()">
                    <div class="formRow">
                        <input type="text" id="inputEmail" name="inputEmail" value="<%=inputEmail%>" onblur="chkField('inputEmail','rqdEmail');replaceGhost(this);" onfocus="removeGhost(this)" /><span class="errorMessage" id="rqdEmail"> * Required</span><span class="errorMessage" id="valEmail"> * Invalid</span>
                    </div>
                    
                    <div class="formRowException">
                        <input type="submit" id="resetButton" class="btnSubmit" name="submitButton" value="Submit" />
                    </div>
                    
                </form>
        
            </div>
                
        </div>
            
        <div class="divMarginBottom"></div>
        
	</div>
  
</div>

<div id="divSpaceAtBottom"></div>












<%
'    <h1>Forgot Password</h1>
'    
'    <p>If you have forgotten your password, enter your email below to initiate a password reset on your account:</p>    
'
'	
'
'    <div id="theForm">
'    	<form name="ForgotPasswordForm" action="" method="post" onsubmit="return chkForm()">
'            	
'<table width="700" border="0" cellspacing="15" cellpadding="0">
'  <tr>
'    <td width="100" align="right"><label for="inputEmail">Email</label></td>
'    <td>[Email]</td>
'  </tr>
'  <tr>
'    <td align="right">&nbsp;</td>
'    <td>[Submit]</td>
'  </tr>
'  <tr>
'    <td align="right">&nbsp;</td>
'    <td>&nbsp;</td>
'  </tr>
'</table>
'
'      </form>
'      
'    <p>If you do not have an account, please <a href="<%=urlRelRegister$>">register</a>.</p>    
'
'    </div>
%>

<!--#include virtual="/includes/template/bottom.asp"-->
