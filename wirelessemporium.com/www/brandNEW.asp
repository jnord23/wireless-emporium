<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_functions.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
pageName = "brandNew"
response.buffer = true
dim brandid
brandid = request.querystring("brandid")
phoneOnly = request.QueryString("phoneOnly")
if brandid = "" or not isNumeric(brandid) then
	call PermanentRedirect("/")
end if
if phoneOnly = "" or not isNumeric(phoneOnly) then
	phoneOnly = 0
end if

leftGoogleAd = 1
displayIntLink = 0
googleAds = 1

showIntPhones = request.Form("showIntPhones")
if isnull(showIntPhones) or len(showIntPhones) < 1 then showIntPhones = 0

call fOpenConn()
'SQL = "SELECT DISTINCT A.*, C.* FROM (we_models A INNER JOIN we_items B ON A.modelID = B.modelID)"
'SQL = SQL & " INNER JOIN we_brands C ON A.brandid = C.brandid"
'SQL = SQL & " WHERE B.brandid = '" & brandid & "'"
'SQL = SQL & " AND B.hideLive = 0 AND patindex('%All %',A.modelname) = 0"
'SQL = SQL & " AND B.inv_qty <> 0"

'sql = "select a.modelID, a.modelName, a.modelImg, a.international, b.brandName, b.brandImg from we_models a left join we_brands b on a.brandID = b.brandID where a.modelName is not null and a.hideLive = 0 and a.brandID = " & brandid & ""
sql	=	"select	a.modelid, a.modelname, a.modelimg, a.international, b.brandname, b.brandimg" & vbcrlf & _
		"from	we_models a with (nolock) join we_brands b with (nolock)" & vbcrlf & _
		"	on	a.brandid = b.brandid " & vbcrlf & _
		"where	a.modelname is not null and a.hidelive = 0 and b.brandid = '" & brandid & "' " & vbcrlf & _
		"	and	a.modelname not like 'all model%' " & vbcrlf

if brandID = 17 and phoneOnly = 1 then
SQL = SQL & " and a.modelName not like '%iPod%' and a.modelName not like '%iPad%'"
elseif brandID = 17 and phoneOnly = 0 then
SQL = SQL & " and a.modelName not like '%iPhone%'"
end if
dim nSorting
nSorting = request.form("nSorting")
select case nSorting
	case "2" : SQL = SQL & " ORDER BY international, modelName"
	'case "3" : SQL = SQL & "ORDER BY A.ReleaseYear DESC, A.ReleaseQuarter DESC, A.modelName"
	case else : SQL = SQL & " ORDER BY international, ReleaseYear DESC, ReleaseQuarter DESC, modelName"
	'case else : SQL = SQL & "ORDER BY A.temp DESC"
end select
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
'response.write "<pre>" & SQL & "</pre>"
'RS.open SQL, oConn, 3, 3
RS.open SQL, oConn, 3, 1

if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
end if

dim brandName, brandImg
brandName = RS("brandName")
brandImg = RS("brandImg")

dim SEtitle, SEdescription, SEkeywords, topText, bottomText, brandStaticText, RStypes
select case brandid
	case "1"
		SEtitle = "Buy Discount Audiovox Cell Phone Accessories Mobile Chargers, Batteries, Antenna, Holsters, Faceplates, Cases"
		SEdescription = "The best place to buy the latest Audiovox accessories for your phones, including cases, batteries, chargers, antennas, faceplates and more. Buy Online!"
		SEkeywords = "audiovox cell phone accessory, audiovox mobile phone accessories, audiovox cell phone battery, audiovox charger, audiovox leather case, audiovox cell phone, audiovox cellular phone accessory, cell phone parts, wireless accessories, wireless phone accessories, bluetooth cell phone headsets, cell phone antennas, cell phone belt clips, cell phone gear, cell phone keypads, cellphone keypads"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your Audiovox cell phone. Choose from 1000's of quality Audiovox accessories including leather cases, pouches, batteries, <a class=""bottomText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""bottomText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""bottomText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality Audiovox wireless accessories online!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">With our discount <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"">Audiovox cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Audiovox mobile phone!"
		brandStaticText = "Shop at Wireless Emporium for your favorite UTStarcom and Audiovox cell phone accessories and join the countless others who call Wireless Emporium their top choice for batteries, cell phone holsters, chargers, leather cases, faceplates, data cables, hands-free headsets, and more. Shop for Pantech TXT8010 cell phone accessories, SMT5600 cell phone accessories, CDM-7075 cell phone accessories, Sprint V1600 cell phone accessories, CDM-9500 cell phone accessories, PM 8920 cell phone accessories, and cell phone accessories for many other UTStarcom and Audiovox models all in one convenient store. Each cell phone accessory we carry is backed by a 100% customer satisfaction guarantee and every order is shipped free of charge, so order from Wireless Emporium today!"
	case "2"
		SEtitle = "Buy Exclusive Sony Ericsson Accessories at Wireless Emporium"
		SEdescription = "Wireless Emporium carries the Latest Sony Ericsson Accessories all at affordable prices. Buy Quality Sony Ericsson Phone Accessories with Free Shipping now!"
		SEkeywords = "sony ericsson accessories, sony ericsson phone accessories, sony ericsson cell phone cases, sony ericsson cell phone covers"
		titleText = "Sony Ericsson Cell Phone Batteries & Covers"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your Sony Ericsson cell phone. Choose from 1000's of quality Sony Ericsson accessories including leather cases, pouches, batteries, <a class=""bottomText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""bottomText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""bottomText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality Sony Ericsson wireless accessories online!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">With our discount <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"">Sony Ericsson cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Sony Ericsson mobile phone!"
		brandStaticText = "Tired of running from place to place just to get all the cell phone accessories you need for your Sony Ericsson phone? Wireless Emporium makes shopping easy and convenient with an online store providing everything you need for your cell phone. We offer high-quality manufacturer-direct cell phone accessories to fit a number of Sony Ericsson cell phones. Shop for Sony Ericsson Equinox TM717 cell phone accessories, Sony Ericsson Z500a cell phone accessories, Sony Ericsson W980 cell phone accessories, Sony Ericsson W760 cell phone accessories, Sony Ericsson W380a cell phone accessories, Sony Ericsson G502 cell phone accessories, and cell phone accessories for many other Sony Ericsson cell phones. Each order is shipped free of extra charges and every item is guaranteed to provide 100% customer satisfaction, so you can shop at Wireless Emporium with confidence!"
	case "3"
		SEtitle = "Buy Discount Kyocera Qualcomm Cell Phone Accessories, Mobile Chargers, Keypads, Batteries, Antenna, Holsters, Faceplates, Cases, Covers"
		SEdescription = "Make your phone your own personal statement with our Kyocera / Qualcomm cell phone accessories including covers, keypads, chargers, batteries, antenna, holsters, faceplates, cases and so much more."
		SEkeywords = "kyocera cell phone accessory, kyocera mobile phone accessories, kyocera battery, kyocera cell phone battery, kyocera phone cover, kyocera cell phone cover, kyocera faceplates, kyocera cell phone faceplates, mobile phone headsets, mobile phone chargers, mobile phone cases, mobile phone batteries, mobile phone accessories, mobile handsfree, faceplates for cell phones"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your Kyocera Qualcomm cell phone. Choose from 1000's of quality Kyocera Qualcomm accessories including leather cases, pouches, batteries, <a class=""bottomText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""bottomText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""bottomText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality Kyocera Qualcomm wireless accessories online!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">With our discount <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"">Kyocera Qualcomm cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Kyocera Qualcomm mobile phone!"
		brandStaticText = "Wireless Emporium carries a wide selection of cell phone accessories for many brands, including the popular Kyocera lines. We carry cell phone accessories for models such as Kyocera Slider SE447, Kyocera QCP-3035, the 2300 series, Kyocera Rave KE, Kyocera Phantom KE, Kyocera KX12, Kyocera Koi KX2, Kyocera Mako S4000, Kyocera Domino S1310, Kyocera Candid KX16, and many more. Each Kyocera cell phone accessory is backed by a 100% customer satisfaction guarantee, so you know that you will be purchasing only the best manufacturer-direct Kyocera cell phone accessories. Whether you are shopping for cell phone accessories for your Kyocera K404 or Kyocera 5135 cell phone accessories, Wireless Emporium is here to help you meet your cell phone needs."
	case "4"
		SEtitle = "LG Accessories: Premium LG Cell Phone Accessories at Wireless Emporium"
		SEdescription = "Wireless Emporium carries LG Cell Phone accessories at never before seen prices. We carry LG Phone accessories for all the latest models - all with Free Shipping!"
		SEkeywords = "lg cell phone accessories, lg accessories, lg phone accessories, lg covers, lg cases"
		titleText = "LG Batteries, Faceplates, Cases & Covers"
		topText = "Wireless Emporium offers the largest selection of factory-direct LG cell phone accessories for your LG brand cell phone. Choose from 1000's of quality LG cell phone accessories including leather cases, pouches, LG cell phone batteries, LG cell phone cases, LG cell phone covers, <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/p-4532-replacement-lithium-ion-battery-for-lg-vx3200.asp"" title=""LG Battery"">LG batteries</a>, the best <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"" title=""Bluetooth Headset"">Bluetooth headsets</a>, and much more. Get great deals on <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/p-17330-lg-shine-cu720-premium-vertical-leather-pouch.asp"" title=""LG Shine Case"">LG Shine Cases</a>, LG Voyager Covers, LG Shine phone covers and many other LG cell phone accessories. Wireless Emporium is also the largest seller of LG <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/cell-phone-cases.asp"" title=""Cell Phone Cases"">cell phone cases</a>, LG <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a> and cell phone faceplates to personalize and accentuate your phone. No one sells more quality LG wireless accessories online!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Wireless Emporium is itself the epitome of quality and reliability, and its top class cell phone accessories like LG voyager covers are just a simple example of the credibility of its products."
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">With our discount LG cell phone accessories, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your LG mobile phone!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Check out our wide range of world class LG accessories at Wireless Emporium like <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/T-595-cell-accessories-lg-env2-vx9100.asp"" title=""LG enV2 Accessories"">LG enV2 Accessories</a>, <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/T-756-cell-accessories-lg-env3-vx9200.asp"" title=""LG enV3 Accessories"">LG enV3 accessories</a>, <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/T-715-cell-accessories-lg-xenon-gr500.asp"" title=""LG Xenon Accessories"">LG Xenon accessories</a> and many more!"
		bottomText = "Wireless Emporium is the Number One Online Resource for cell phone accessories and the largest seller of cell phone cases, cell phone covers, <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/cell-phone-chargers.asp"" title=""Cell Phone Chargers"">cell phone chargers</a>, cell phone batteries and other accessories to personalize, optimize and protect your phone."
		brandStaticText = "Wireless Emporium carries all of the LG cell phone accessories you need to outfit your LG cell phone. Choose from thousands of cell phone accessories, such as LG Ally VS740 cell phone accessories, LG Bliss UX700 cell phone accessories, LG Dare VX9700 cell phone accessories,, LG dLite GD570 cell phone accessories, LG enV 2 VX9100 cell phone accessories, LG Neon GT365 cell phone accessories, LG Remarq LN240 cell phone accessories, LG Vu Plus cell phone accessories, or many others, Wireless Emporium stocks the cell phone accessories you require to maximize the performance of your LG phone. Place your order today and Wireless Emporium will even ship it out to you free of charge!"
	case "5"
		SEtitle = "Motorola Accessories: Motorola Droid Cell Phone Accessories, Chargers, Cases & Batteries"
		SEdescription = "Find the best deals on Motorola cell phone accessories including Motorola Droid accessories, chargers, cases, covers and Motorola cell phone batteries and Motorola faceplates at Wireless Emporium � the #1 cell phone accessories store online"
		SEkeywords = "Motorola accessories, Motorola cell phone accessories, Motorola batteries, Motorola Droid cell phone accessories, Motorola cases, Motorola cell phone covers, Motorola cell phone chargers, Motorola faceplates"
		titleText = "Discount Motorola Cell Phone Chargers, Batteries &amp; Other Motorola Cell Phone Accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories "
		topText = topText & "for your Motorola brand cell phone. Choose from 1000's of quality Motorola accessories including "
		topText = topText & "Motorola cell phone cases, leather cases, pouches, Motorola batteries, Motorola faceplates, "
		topText = topText & "<a class=""bottomText-link"" href=""http://www.wirelessemporium.com/products/motorola-v220-antennas-antenna-stub-for-motorola-v220.asp"" title=""Motorola Antenna"">Motorola antenna</a>, "
		topText = topText & "Bluetooth headsets, all kinds of chargers and much more. "
		topText = topText & "Wireless Emporium is also the largest seller of cell phone covers, "
		topText = topText & "<a class=""bottomText-link"" href=""http://www.wirelessemporium.com/cell-phone-charms-bling-kits.asp"" title=""Cell Phone Charms"">cell phone charms</a> and "
		topText = topText & "cell phone faceplates to personalize and accentuate your phone. No one sells more quality HTC wireless accessories online!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Buy <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/T-500-accessories-motorola-w385.asp"" title=""Motorola w385 Accessories"">Motorola w385 accessories</a>, "
		topText = topText & "Motorola V3 Razr accessories and accessories for many other models right here � at Wireless Emporium. "
		topText = topText & "With our discount <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"" title=""Motorola Accessories"">Motorola accessories</a>, "
		topText = topText & "rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Motorola mobile phone!"
		brandStaticText = "Motorola is perhaps best known for the popular Droid A855 and Droid X, but Wireless Emporium makes sure that you can find cell phone accessories for any cell phone you may need. We carry Motorola RAZR V9 cell phone accessories, Morotola ROKR E1 cell phone accessories, Motorola KRZR K1 cell phone accessories, Motorola Evoke QA4 cell phone accessories, Motorola 1296 cell phone accessories, Motorola Backflip MB300 cell phone accessories, and many more. Not only do we carry the quality cell phone accessories you need, but we are so sure you will love them that we are offering a 100% customer satisfaction guarantee on each product you order. With a selection and a guarantee like that, go ahead and treat yourself to new cell phone accessories. You deserve it!"
	case "6"
		SEtitle = "Discount Nextel Cell Phone Accessories, Mobile Batteries, Cases, Antenna, Holsters, Faceplates, Hands Free & Headsets, Chargers"
		SEdescription = "If you're looking for quality, Nextel cell phone accessories, checkout our huge selection factory-direct Nextel accessories. Includes batteries, cases, faceplates, chargers & many more."
		SEkeywords = "nextel cell phone accessory, nextel mobile phone accessories, nextel battery, nextel cell phone battery, nextel phone charger, case i870 leather nextel, nextel faceplates, mobile phone headsets, mobile phone chargers, mobile phone cases, mobile phone batteries, mobile phone accessories, mobile handsfree, faceplates for cell phones, discount cell phone accessories, cellular phone headsets, cellular phone accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your Nextel cell phone. Choose from 1000's of quality Nextel accessories including leather cases, pouches, batteries, <a class=""bottomText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""bottomText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""bottomText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality Nextel wireless accessories online!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">With our discount <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"">Nextel cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Nextel mobile phone!"
		brandStaticText = "Nextel has always been known for its quality phones � but a phone is only as good as the cell phone accessories that support it! That's why Wireless Emporium is offering the widest range of Nextel cell phone accessories for the best prices online. Whether you are shopping for Nextel i850 cell phone accessories, Nextel i1000 cell phone accessories, Nextel i570 cell phone accessories, Nextel i2000 cell phone accessories, Nextel i90c cell phone accessories, Nextel i88s cell phone accessories, or any others, Wireless Emporium is sure to have the Nextel cell phone accessories you are looking for!"
	case "7"
		SEtitle = "Nokia Cell Phone Accessories with Free Shipping at Wireless Emporium!"
		SEdescription = "Wireless Emporium carries the latest Nokia Accessories at affordable prices. Buy Nokia Cell Phone Accessories like Covers, Cases for all Nokia phones with Free Shipping!"
		SEkeywords = "nokia accessories, nokia cell phone accessories, nokia phone chargers, nokia covers, nokia cases"
		titleText = "Nokia Cases, Covers, Batteries & Chargers"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories "
		topText = topText & "for your Nokia brand cell phone. Choose from 1000's of quality Nokia accessories including "
		topText = topText & "leather cases, pouches, batteries, Bluetooth headsets, "
		topText = topText & "<a class=""bottomText-link"" href=""http://www.wirelessemporium.com/cell-phone-batteries.asp3"" title=""Nokia Cables"">Nokia Cables</a>, all kinds of "
		topText = topText & "<a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"" title=""Nokia Phone Chargers"">Nokia phone chargers</a> and much more. "
		topText = topText & "Wireless Emporium is also the largest seller of "
		topText = topText & "cell phone covers, cell phone faceplates &amp; "
		topText = topText & "<a class=""bottomText-link"" href=""http://www.wirelessemporium.com/cell-phone-charms-bling-kits.asp"" title=""Cell Phone Bling"">cell phone bling</a> kits "
		topText = topText & "to personalize and accentuate your phone. No one sells more quality Nokia wireless accessories online!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">With our discount <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"" title=""Nokia Cell Phone Accessories"">Nokia cell phone accessories</a>, "
		topText = topText & "rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Nokia mobile phone!"
		brandStaticText = "Looking for quality cell phone accessories for your Nokia cell phone, but don't know where to start? Wireless Emporium is here to help! We offer an extensive array of high quality cell phone accessories for most popular Nokia cell phone models. Shop for faceplates, batteries, chargers, and many more for your favorite Nokia E73 Mode, Nokia Mirage 2605, Nokia 2760, Nokia 3600, Nokia 5230 Nuron, Nokia 6555, Nokia Surge 6790, Nokia Twist 7705, Nokia N-Gage, Nokia 8290, or any of the other dozens of popular Nokia models available. Each of our quality Nokia cell phone accessories are backed by a 100% customer satisfaction guarantee, so rest assured knowing that you will be ordering premium Nokia cell phone accessories from Wireless Emporium."
	case "8"
		SEtitle = "Discount Panasonic Cell Phone Accessories, Mobile Chargers, Batteries, Antenna, Holsters, Faceplates, Covers, Keypads"
		SEdescription = "Wireless Emporium offers latest Panasonic accessories for your cell phones, including cases, batteries, chargers, antenna, keypads, faceplates and more. Buy Online."
		SEkeywords = "panasonic cell phone accessory, panasonic mobile phone accessories, panasonic battery pack, panasonic battery charger, panasonic phone battery, panasonic phone part, panasonic headset, mobile phone accessories, mobile handsfree, faceplates for cell phones"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your Panasonic cell phone. Choose from 1000's of quality Panasonic accessories including leather cases, pouches, batteries, <a class=""bottomText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""bottomText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""bottomText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality Panasonic wireless accessories online!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">With our discount <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"">Panasonic cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Panasonic mobile phone!"
	case "9"
		SEtitle = "Samsung Accessories: Quality Samsung Phone Accessories"
		SEdescription = "Looking for Samsung Cell Phone Accessories for your phone? Then look no further than Wireless Emporium. We carry the latest Samsung Phone Accessories all with Free Shipping!"
		SEkeywords = "samsung accessories, samsung phone accessories, samsung covers, samsung cell phone cases, samsung phone covers"
		titleText = "Samsung Cases, Covers & Batteries"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your Samsung brand cell phone. Choose from 1000's of quality Samsung accessories including Samsung Behold accessories, <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/p-5396-900-mah-extended-li-ion-battery-for-samsung-c225.asp"" title=""Samsung Phone Battery"">Samsung phone batteries</a>, <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/sc-7-sb-9-samsung-cases-pouches-skins.asp"" title=""Samsung Eternity Leather Cases"">Samsung eternity leather cases</a>, pouches, batteries, Bluetooth headsets, all kinds of chargers and much more. Wireless Emporium is also the largest seller of Samsung <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a> and Samsung cell phone faceplates to personalize and accentuate your phone. No one sells more quality Samsung wireless accessories online!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Wireless Emporium is itself the epitome of quality and reliability, and its top class cell phone accessories like Samsung Behold Accessories are just a simple example of the credibility of its products."
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">With our discount <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"" title=""Samsung Cell Phone Accessories"">Samsung cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Samsung mobile phone!"
		bottomText = "Wireless Emporium is the Number One Online Resource for cell phone accessories and the largest seller of cell phone cases, cell phone covers, cell phone chargers, <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/cell-phone-batteries.asp"" title=""Cell Phone Batteries"">cell phone batteries</a> and other accessories to personalize, optimize and protect your phone."
		brandStaticText = "Purchase all of your high-quality Samsung cell phone accessories from Wireless Emporium, where we carry cell phone accessories for popular Samsung models such as Acclaim R880, Captivate i897, Comeback SGH-T559, Exclaim SPH-M550, Galaxy I7500, Freeform SCH-R350, Gravity SGH-T459, Instinct SPH-M800, Intrepid SPH-i350, Messager Touch SCH-r630, OmniaPro 7330, Reclaim SPH-M560, and many other popular Samsung models. Whether you need holsters, batteries, leather cases, chargers, or any other replacement cell phone accessories, Wireless Emporium is the place to go! Each item we carry is backed by a 100% customer satisfaction guarantee, so you know you will be receiving premium cell phone accessories for your Samsung cell phone. Order your cell phone accessories today and we will even ship them free of charge!"
	case "10"
		SEtitle = "Sanyo Cell Phone Accessories: Sanyo Chargers, Batteries, Covers & Faceplates"
		SEdescription = "Find all Sanyo phone accessories including Sanyo phone batteries, covers and faceplates, leather cases for Sanyo cell phones, Sanyo phone chargers and phone clips at Wireless Emporium."
		SEkeywords = "sanyo cell phone accessories, sanyo mobile phone accessories, sanyo phone accessories, sanyo phone batteries, sanyo phone chargers, sanyo phone covers, sanyo phone cases, sanyo phone clips, sanyo accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"" title=""cell phone accessories"">cell phone accessories</a> for your Sanyo cell phone. Choose from 1000's of quality Sanyo accessories including leather cases, pouches, batteries, <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/hf-sm-923-sanyo-incognito-scp-6760-bluetooth-headsets-headphones.asp"" title=""Sanyo Bluetooth headsets"">Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""bottomText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a> and cell phone <a class=""bottomText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">faceplates</a> to personalize and accentuate your phone. No one sells more quality Sanyo wireless accessories online!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">With our discount <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"">Sanyo cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Sanyo mobile phone!"
		brandStaticText = "At Wireless Emporium, we offer an extensive selection of high-quality manufacturer-direct cell phone accessories for your favorite Sanyo cell phone. Browse through our stocks of Sanyo SCP-2700 cell phone accessories, Sanyo SCP-5000 cell phone accessories, Sanyo Pro-700 cell phone accessories, Sanyo Katana DLX cell phone accessories, Sanyo SCP-7000 cell phone accessories, Sanyo Katana Eclipse cell phone accessories, Sanyo SCP-6400 cell phone accessories, and cell phone accessories for many other popular Samsung models. We carry everything you need for your Samsung phone, no matter the model. Each item is guaranteed 100% customer satisfaction and each order is shipped with no extra charges. What more could you ask for?"
	case "11"
		SEtitle = "Siemens Cell Phone Accessories: Siemens Phone Chargers, Batteries & Leather Cases"
		SEdescription = "Buy all Siemens cell phone accessories including Siemens phone batteries, Siemens cell phone leather cases and Siemens cell phone chargers at Wireless Emporium."
		SEkeywords = "siemens cell phone accessories, siemens mobile phone accessories, siemens phone accessories, siemens phone batteries, siemens phone chargers, siemens phone cases, siemens accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"" title=""cell phone accessories""> cell phone accessories</a> for your Siemens cell phone. Choose from 1000's of quality Siemens accessories including leather cases, pouches, batteries, <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/hf-sm-154-siemens-a56-bluetooth-headsets-headphones.asp"" title=""Siemens A56 Bluetooth headsets"">Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""bottomText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a> and <a class=""bottomText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality Siemens wireless accessories online!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">With our discount <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"">Siemens cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Siemens mobile phone!"
		brandStaticText = "Siemens, a global powerhouse in electronics and electrical engineering, has created a number of high-quality cell phones for business and personal use. Wireless Emporium knows that you want nothing but the best cell phone accessories for your favorite Siemens cell phone. That's why we are offering a massive selection of high-quality manufacturer-direct Siemens cell phone accessories for most Siemens models, including Siemens A56 cell phone accessories, Siemens C61 cell phone accessories, Siemens S46 cell phone accessories, Siemens M56 cell phone accessories, Siemens S66 cell phone accessories, and many others. Shop for chargers, batteries, faceplates, leather cases, and many more items listed at prices below retail. Each Siemens cell phone accessory is backed by a 100% customer satisfaction guarantee and shipped to you free of charge. You won't find a better deal anywhere else on the internet!"
	case "14"
		SEtitle = "BlackBerry Accessories: Buy Exclusive BlackBerry Phone Covers, Cases at WirelessEmporium"
		SEdescription = "Wireless Emporium carries BlackBerry Accessories for all the popular models. Get premium BlackBerry Covers & Cases and all other accessories at affordable prices!"
		SEkeywords = "blackberry accessories, blackberry phone accessories, blackberry covers, blackberry cases, blackberry batteries, blackberry chargers"
		titleText = "BlackBerry Cases, Covers & Batteries"
		topText = "Wireless Emporium offers the largest selection of factory-direct <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"" title=""cell phone accessories""> cell phone accessories</a> for your BlackBerry cell phone. Choose from 1000's of quality BlackBerry accessories including leather cases, pouches, batteries, <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/hf-sm-759-blackberry-tour-9630-bluetooth-headsets-headphones.asp"" title=""BlackBerry Bluetooth headsets"">bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""bottomText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a> and <a class=""bottomText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality BlackBerry wireless accessories online!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">With our discount <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"">BlackBerry cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your BlackBerry mobile phone!"
		brandStaticText = "BlackBerry has been the leading name in cell phone brands for years, specializing in high-powered phones for business and pleasure. Wireless Emporium carries a wide range of cell phone accessories for BlackBerry-brand phones, including cell phone accessories for the BlackBerry Bold 9000, BlackBerry Bold 9650, BlackBerry Curve 8330, BlackBerry Curve 8530, BlackBerry Pearl 8100, BlackBerry Pearl Flip 8230, BlackBerry Storm 9500, BlackBerry Storm2 9550, BlackBerry Torch 9800, and the BlackBerry Tour 9630. Wireless Emporium backs every BlackBerry cell phone accessory with a 100% customer satisfaction guarantee and offers free shipping on every order. Whether you are shopping for BlackBerry Storm 9500 cell phone accessories, BlackBerry Pearl 3G cell phone accessories, BlackBerry 7200 cell phone accessories, or many more, Wireless Emporium has what you need."
	case "15"
		brandStaticText = "Only at Wireless Emporium will you find thousands of high-quality manufacturer-direct cell phone accessories for a wide range of brands and models. Shop from the convenience of your own home all day, every day for the faceplates, cell phone holsters, leather cases, batteries, chargers, hands-free and Bluetooth headsets, antennas, replacement parts, and many more. We carry an extensive selection of Huawei T-Mobile Tap U7519 cell phone accessories, Garmin Nuvifone G60 cell phone accessories, HP iPAQ Glisten cell phone accessories, PCD Verizon CDM8975 cell phone accessories, Cricket CAPTR CalComp A200 cell phone accessories, Casio Exilim C721 cell phone accessories, and many more. No matter what cell phone model you have, Wireless Emporium has the premium cell phone accessories you need for affordable prices. Order your favorite cell phone accessories today and we'll even ship them to you at no extra cost!"
	case "16"
		SEtitle = "Premium Palm Accessories with Free Shipping at Wireless Emporium"
		SEdescription = "Looking for Palm Accessories? Then look no further than Wireless Emporium. We carry all the latest Palm phone accessories at Discounted Prices and Free Shipping!"
		SEkeywords = "palm accessories, palm phone accessories, palm covers, palm cases, palm cell phone accessories"
		titleText = "Discount Palm Cases, Covers & Batteries"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your Palmone cell phone. Choose from 1000's of quality Palmone accessories including leather cases, pouches, batteries, <a class=""bottomText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""bottomText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""bottomText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality Palmone wireless accessories online!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">With our discount <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"">Palmone cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Palmone mobile phone!"
		brandStaticText = "Only at Wireless Emporium will you find a large selection of high-quality Palm cell phone accessories for your favorite Palm cell phone. Whether you are shopping for Palm Pixi cell phone accessories, Palm Pre cell phone accessories, Palm Centro cell phone accessories, Palm Treo Pro cell phone accessories, Palm Treo 600 cell phone accessories, Palm Treo 680 cell phone accessories, or many other cell phone accessories, you can be sure to find what you are looking for at Wireless Emporium. Place your order today and we will ship your items free of extra charge!"
	case "17"
		if phoneOnly = 0 then
			SEtitle = "iPod and iPad Accessories: Premium iPod/iPad Covers & Cases at Wireless Emporium"
			SEdescription = "Looking for premium Apple iPod/iPad accessories? Wireless Emporium carries exclusive iPod/iPad accessories � all with Free Shipping!"
			SEkeywords = "iPod/iPad accessories, ipod accessories, ipod touch accessories, ipod nano accessories, ipad accessories"
			titleText = "iPod/iPad Accessories, iPod/iPad Cases & Covers"
'			topText = "Apple iPod/iPad covers and iPod/iPad skins are just a few of the accessories Wireless Emporium specializes in. As the leading name in <a class=""bottomText-link"" href=""http://www.wirelessemporium.com"" title=""iPod/iPad accessories"">accessories</a>, we make it our business to offer a variety of iPod/iPad faceplates, iPod/iPad covers, and iPod/iPad skins at heavily discounted prices. There's no better way to personalize your iPod/iPad than with a fashionable and durable iPod/iPad faceplate. Easily swap out quality iPod/iPad faceplates daily to match your mood! Buy your iPod/iPad cover today and invest in the first line of defense for your beloved iPod/iPad. If you want a lightweight solution to protecting your iPod/iPad, check out our myriad of iPod/iPad skins. We offer a colorful array of iPod/iPad skins made of silicone that is custom-cut to fit your iPod/iPad. "
			topText = "Since its introduction in 2001, the iPod has become the way people listen to their portable music.  Over the years, many iterations of the iPod have emerged, including the iPod Nano and iPod Touch.  Today, Apple�s portable MP3 player is still the innovator in portable music players despite increased competition from other manufacturers.  Whether you have a first generation iPod or the latest if it�s <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/T-1012-cell-accessories-apple-ipod-touch-4.asp"" title=""iPod Accessories"">iPod Touch 4 accessories</a> that you�re looking for - it is important to protect your device from everyday wear and tear.  Wireless Emporium is the leading supplier of iPod accessories, including screen protectors and iPod cases.  And since it is your personal music player, it doesn�t hurt to personalize the look of your iPod with a designer faceplate.  Whatever it is that you are looking for, Wireless Emporium has an extensive line of Apple iPod accessories that range from the practical to the personal, all backed by a 100% satisfaction guarantee."

'			bottomText = "Though thin and lightweight, these iPod/iPad skins are durable and designed to protect your product from regular wear and tear. Aside from iPod/iPad cases and iPod/iPad covers, we've got other prime iPod/iPad accessories including iPod/iPad headsets and iPod/iPad leather cases. Choose an iPod/iPad headset from the dozens of wired and wireless iPod/iPad headsets we offer at everyday low prices. Other iPod/iPad accessories including iPod/iPad leather pouches are also offered at discounted prices."
'			bottomText = bottomText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Since 2001, Wireless Emporium has been offering high-quality cell phone accessories to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best cell phone accessories at the best value. Shipping and superior customer service is free with every order and offered every day!"
			bottomText = "The world waited with baited breath as Apple unveiled its latest gadget, the iPad in early 2010.  An entirely new segment of the wireless community was created, and since a slew of would be competitors entered the arena.  The iPad remains the leader however, and Wireless Emporium remains the leader in <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/apple-ipad-accessories.asp"" title=""iPad Accessories"">iPad accessories</a>. With a brilliant 10 inch display it is important to use a screen protector to keep it from getting scratches or dinged.  An <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/sb-17-sm-940-sc-7-cases-pouches-skins-apple-ipad.asp"" title=""iPad Accessories"">iPad case</a> or an <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/sb-17-sm-940-sc-3-faceplates-screen-protectors-apple-ipad.asp"" title=""Apple iPad Accessories"">iPad cover</a> will go a long way towards assuring that the sleek design remains unblemished and looking like new.  Transport your iPad in style while protecting it, and make sure that your battery never runs out on you, allowing you to optimize the capabilities of this amazing internet device designed to make your life easier.  And to give you that added peace of mind, know that all of our iPad accessories are backed by our 100% customer satisfaction guarantee."
'			brandStaticText = "Wireless Emporium knows that you will accept nothing but the best in brand name goods. That is why we offer you an extensive selection of Apple accessories for the iPad, iPhone 4, iPhone 3G, iPhone, iPod Touch, iPod Video, iPod Photo, iPod Nano, iPod 3G, iPod Mini, and iPod Shuffle. Choose from a selection of skins, chargers, batteries, faceplates, travelling cases, and many more. Each of our Apple iPad accessories, Apple iPhone 4 accessories, Apple iPhone 3G accessories, Apple iPhone accessories, Apple iPod Touch accessories, Apple iPod Video accessories, Apple iPod Photo accessories, Apple iPod Nano accessories, Apple iPod 3G accessories, Apple iPod Mini accessories, and Apple iPod Shuffle accessories are manufacturer-direct and guaranteed 100% customer satisfaction, so order your Apple accessories today!"
		else
			SEtitle = "iPhone 4 Accessories: Premium iPhone Covers & Cases at Wireless Emporium"
			SEdescription = "Looking for premium Apple iPhone accessories? Wireless Emporium carries exclusive iPhone accessories including those for the iPhone 4 � all with Free Shipping!"
			SEkeywords = "iphone accessories, iphone 4 accessories, ipod accessories, ipod touch accessories, ipod nano accessories, ipad accessories"
			titleText = "iPhone 4 Accessories, iPhone Cases & Covers"
			topText = "Apple iPhone covers and iPhone skins are just a few of the cell phone accessories Wireless Emporium specializes in. As the leading name in <a class=""bottomText-link"" href=""http://www.wirelessemporium.com"" title=""cell phone accessories"">cell phone accessories</a>, we make it our business to offer a variety of iPhone faceplates, iPhone covers, and iPhone skins at heavily discounted prices. There's no better way to personalize your iPhone than with a fashionable and durable iPhone faceplate. Easily swap out quality iPhone faceplates daily to match your mood! Buy your iPhone cover today and invest in the first line of defense for your beloved iPhone. If you want a lightweight solution to protecting your iPhone, check out our myriad of iPhone skins. We offer a colorful array of iPhone skins made of silicone that is custom-cut to fit your iPhone. "
			bottomText = "Though thin and lightweight, these iPhone skins are durable and designed to protect your phone from regular wear and tear. Aside from iPhone cases and iPhone covers, we've got other prime iPhone accessories including iPhone headsets and iPhone leather cases. Choose an iPhone headset from the dozens of wired and wireless iPhone headsets we offer at everyday low prices. Other iPhone accessories including iPhone leather pouches are also offered at discounted prices."
			bottomText = bottomText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Since 2001, Wireless Emporium has been offering high-quality cell phone accessories to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best cell phone accessories at the best value. Shipping and superior customer service is free with every order and offered every day!"
			brandStaticText = "Wireless Emporium knows that you will accept nothing but the best in brand name goods. That is why we offer you an extensive selection of Apple accessories for the iPad, iPhone 4, iPhone 3G, iPhone, iPod Touch, iPod Video, iPod Photo, iPod Nano, iPod 3G, iPod Mini, and iPod Shuffle. Choose from a selection of skins, chargers, batteries, faceplates, travelling cases, and many more. Each of our Apple iPad accessories, Apple iPhone 4 accessories, Apple iPhone 3G accessories, Apple iPhone accessories, Apple iPod Touch accessories, Apple iPod Video accessories, Apple iPod Photo accessories, Apple iPod Nano accessories, Apple iPod 3G accessories, Apple iPod Mini accessories, and Apple iPod Shuffle accessories are manufacturer-direct and guaranteed 100% customer satisfaction, so order your Apple accessories today!"
		end if
	case "18"
		SEtitle = "Pantech Accessories: Pantech Duo & Matrix Accessories | Matrix Cases"
		SEdescription = "Buy Pantech Cell Phone Accessories - Pantech Duo Accessories and Pantech Matrix Accessories like cases and much more at discounted prices only at Wireless Emporium."
		SEkeywords = "pantech accessories, pantech cell phone accessories, pantech duo, pantech matrix accessories, pantech matrix cases, pantech data cable, pantech matrix accessories"
		titleText = "Discount Pantech Cell Phone Chargers, Batteries &amp; Other Pantech Accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your Pantech brand cell phone. Choose from 1000s of quality Pantech accessories including leather cases, pouches, batteries, Bluetooth headsets, Pantech Matrix accessories like <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/p-22435-large-neoprene-pouch-for-pantech-matrix-c740.asp"" title=""Pantech Matrix Cases"">Pantech Matrix Cases</a>, all kinds of chargers like <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/p-22425-pantech-matrix-c740-car-charger.asp"" title=""Pantech Matrix Car Charger"">Pantech Matrix car charger</a>, Pantech Duo accessories and much more. Wireless Emporium is also the largest seller of <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a>, cell phone faceplates &amp; <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/cell-phone-charms-bling-kits.asp"" title=""Cell Phone Charms"">cell phone charms</a> to personalize and accentuate your phone. Wireless Emporium is itself the epitome of quality and reliability, and its top class cell phone accessories like Pantech Matrix and Pantech Duo accessories are just a simple example of the credibility of its products. No one sells more quality Pantech wireless accessories online! With our discount Pantech cell phone accessories, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Pantech mobile phone!"
		bottomText = "Wireless Emporium is the Number One Online Resource for cell phone accessories and the largest seller of cell phone cases, cell phone covers, cell phone chargers, <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/cell-phone-batteries.asp"" title=""Cell Phone Batteries"">cell phone batteries</a> and other accessories to personalize, optimize and protect your phone."
		brandStaticText = "There are many shops where you can purchase cell phone accessories for your Pantech cell phone, but only one place offers a huge selection of high-quality manufacturer-direct Pantech cell phone accessories for a number of popular Pantech models. Shop for Pantech Helio Hero PN-8300 cell phone accessories, Pantech Jest TXT-8040 cell phone accessories, Pantech Impact P7000 cell phone accessories, Pantech Matrix Pro C820 cell phone accessories, Pantech Breeze C520 cell phone accessories, or many others from our extensive stock. Each of our premium Pantech cell phone accessories are backed by a 100% customer satisfaction guarantee, and every order is shipped ASAP with no extra charge. Make your Pantech purchase from Wireless Emporium today!"
	case "19"
		SEtitle = "Sidekick LX Accessories, Sidekick 2008 Battery & Sidekick Faceplates"
		SEdescription = "Buy Sidekick cell phone accessories like Sidekick batteries, cases, faceplates and much more at discounted prices only at Wireless Emporium � the #1 cell phone accessory store online."
		SEkeywords = "sidekick LX accessories, sidekick 2008 accessories, sidekick faceplates, sidekick slide cases, sidekick 2008 battery"
		titleText = "Discount Sidekick Cell Phone Chargers, Batteries &amp; Other Sidekick Accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories "
		topText = topText & "for your Sidekick brand cell phone. Choose from 1000's of quality Sidekick accessories including leather cases, pouches, "
		topText = topText & "batteries, Bluetooth headsets, all kinds of chargers and much more. "
		topText = topText & "Wireless Emporium is also the largest seller of cell phone covers, cell phone faceplates &amp; "
		topText = topText & "<a class=""bottomText-link"" href=""http://www.wirelessemporium.com/cell-phone-charms-bling-kits.asp"" title=""Cell Phone Charms"">cell phone charms</a> "
		topText = topText & "to personalize and accentuate your phone. "
		topText = topText & "<a class=""bottomText-link"" href=""http://www.wirelessemporium.com/T-619-accessories-t-mobile-sidekick-2008.asp"" title=""Sidekick 2008 Accessories"">Sidekick 2008 accessories</a>, "
		topText = topText & "<a class=""bottomText-link"" href=""http://www.wirelessemporium.com/T-534-accessories-t-mobile-sidekick-lx.asp"" title=""Sidekick LX Accessories"">Sidekick LX accessories</a> like "
		topText = topText & "<a class=""bottomText-link"" href=""http://www.wirelessemporium.com/p-18406-sidekick-lx-silicone-case--hot-pink-.asp"" title=""Sidekick LX Cases"">Sidekick LX cases</a> "
		topText = topText & "&amp; much more at extremely discounted prices. "
		topText = topText & "No one sells more quality Pantech wireless accessories online!"
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">With our discount Sidekick cell phone accessories, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Sidekick mobile phone!"
		brandStaticText = "Own a Sidekick and need affordable, high-quality cell phone accessories for it? You've come to the right place! At Wireless Emporium, we offer hundreds of manufacturer-direct cell phone accessories for most models of Sidekick phones, including Sidekick 2008. Sidekick Slide, Sidekick LX 2009, Sidekick LX, Sidekick iD, Sidekick 3, and Sidekick 2. Shop from our extensive selection of leather cases, faceplates, chargers, batteries, hands-free headsets, data cables, and many other items to fir your Sidekick cell phone needs. Every Sidekick cell phone accessory we carry is backed by a 100% customer satisfaction guarantee. Shop from the convenience of your own home and we will ship your order to you ASAP, free of charge!"
	case "20"
		SEtitle = "HTC Accessories: Buy HTC Phone Accessories at Wireless Emporium"
		SEdescription = "Get Great Deals on HTC Accessories only at Wireless Emporium! We carry Accessories for HTC Phones like the Incredible, Touch Pro 2 � all at discount prices."
		SEkeywords = "htc accessories, htc phone accessories, htc incredible accessories, htc touch pro 2 accessories, htc droid accessories, htc cell phone accessories"
		titleText = "HTC Incredible Accessories, HTC Touch Pro 2 Accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your HTC brand cell phone. Choose from 1000s of quality HTC accessories including HTC Touch Pro accessories, leather cases, pouches, batteries, Bluetooth headsets, all kinds of chargers and much more. Wireless Emporium is also the largest seller of HTC cell phone covers, HTC <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/cell-phone-charms-bling-kits.asp"" title=""Cell Phone Charms"">cell phone charms</a> &amp; HTC cell phone faceplates to personalize and accentuate your phone. No one sells more quality HTC wireless accessories online! Wireless Emporium is itself the epitome of quality and reliability, and its top class cell phone accessories like HTC Touch Pro Accessories are just a simple example of the credibility of its products. Buy the best <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/"" title=""Bluetooth Headset"">Bluetooth headset</a>, <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/T-537-cell-accessories-htc-touch-verizon-xv6900.asp"" title=""HTC Touch Case"">HTC touch case</a>, <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/p-17655-oem-htc-stereo-hands-free-headset-emc220.asp"" title=""HTC Headset"">HTC headset</a> and many other HTC Diamond accessories &amp; HTC touch accessories. With our discount HTC cell phone accessories, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your HTC mobile phone!"
		bottomText = "Wireless Emporium is the Number One Online Resource for cell phone accessories and the largest seller of cell phone cases, <a class=""bottomText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a>, cell phone chargers, cell phone batteries and other accessories to personalize, optimize and protect your phone."
		brandStaticText = "HTC cell phones, well known for their cutting-edge technology, are some of the most popular phones on the market. Wireless Emporium offers a wide range of cell phone accessories for your favorite HTC phone. Shop from hundreds of products including HTC Aria cell phone accessories, HTC Droid Incredible cell phone accessories, HTC Google Nexus One/Passion cell phone accessories, HTC EVO 4G cell phone accessories, HTC Hero cell phone accessories, HTC T-Mobile myTouch 3G cell phone accessories, HTC Pure cell phone accessories, and many more! Choose from a selection of faceplates, chargers, batteries, USB cables, and many other items to support your HTC cell phone, and Wireless Emporium will even ship your order to you free of charge."
end select

if SEtitle = "" then SEtitle = "Cell Phone Accessories for " & brandName & ": Discount Accessories from Wireless Emporium, " & brandName & " cell phone Accessories, " & brandName & " Mobile phone Accessories, " & brandName & " Wireless phone Accessories"
if SEdescription = "" then SEdescription = "Discount Accessories for " & brandName & "�and " & brandName & "�Wireless Phone Accessories at Wireless Emporium. Your source for wholesale " & brandName & "�cell phone Accessories, wholesale " & brandName & "�cellular Accessories, discount " & brandName & "�cell phone Accessories and cheap " & brandName & "�cell phone Accessories"
if SEkeywords = "" then SEkeywords = "Discount Accessories for " & brandName & "," & brandName & " Accessories," & brandName & " cell phone Accessories," & brandName & " cell phone Accessories," & brandName & " discount cellular Accessories," & brandName & " mobile phone Accessories," & brandName & " wireless Accessories," & brandName & "�cellular telephone Accessories," & brandName & "�wholesale cellular Accessories," & brandName & "�cellular Accessories," & brandName & "�wireless phone Accessories," & brandName & "�wholesale cell phone Accessories," & brandName & "�cellular phone Accessories,wholesale " & brandName & "�wireless Accessories,wholesale " & brandName & "�cellular phone Accessories,discount " & brandName & "�cell phone Accessories,cool " & brandName & "�cell phone Accessories"
if titleText = "" then titleText = "Discount " & brandName & " Cell Phone Chargers, Batteries &amp; Other " & brandName & " Accessories"

dim strBreadcrumb
if brandID = 17 and phoneOnly = 0 then
	strBreadcrumb = brandName & " iPod/iPad Accessories"
	session("breadcrumb_brand") = brandName & " iPod/iPad "
else
	strBreadcrumb = brandName & " Cell Phone Accessories"
	session("breadcrumb_brand") = brandName
end if

session("breadcrumb_model") = ""
%>

<!--#include virtual="/includes/template/top.asp"-->
<script>
window.WEDATA.pageType = 'brand';
window.WEDATA.pageData = {
	brand: <%= jsStr(brandName) %>,
	brandId: <%= jsStr(brandId) %>
};
</script>

					<%=TABLE_START%>
                    <tr>
                    <td align="left" valign="top" width="800">
						<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td align="left" valign="top" width="100%" class="breadcrumbFinal">
									<a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
								</td>
							</tr>
							<tr>
								<td width="100%" height="65" align="left" style="background-image: url('/images/brandheaders/WE_headers_<%=brandID%>.jpg'); background-repeat: no-repeat; background-position: center top;">
									<%
									dim h1color
									if brandID = 6 then
										h1color = "000000"
									else
										h1color = "FFFFFF"
									end if
									%>
									<style>
										h1 {
											font-size: 18pt;
											color: #<%=h1color%>;
											font-weight: normal;
											font-family: Tahoma, Trebuchet MS, Arial, Helvetica, sans-serif;
											margin-left:11px;
											margin-bottom:7px;
										}
									</style>
									<h1>
										<%
										if brandName = "Apple" then
											if phoneOnly = 0 then
												response.write "iPod/iPad Accessories"
											else
												response.write "iPhone Accessories"
											end if
										else
											response.write brandName & " Accessories"
										end if
										%>
									</h1>
								</td>
							</tr>
							<tr>
								<td width="100%" align="left" valign="top">
									<h2>
										<%=titleText%>
									</h2>
								</td>
							</tr>
							<tr>
								<td width="100%" align="center" valign="top">
									<img src="/images/hline_dot.jpg" width="800" height="8" border="0">
								</td>
							</tr>
							<tr>
								<td width="100%" align="center" valign="top">
									<img src="/images/spacer.gif" width="1" height="10" border="0">
								</td>
							</tr>
							<%
								a = 0
								dim RS2, modelID, modelName, modelImg, altTag
								'SQL = "SELECT TOP 5 A.*, B.modelName, B.modelImg FROM temp_TopModels A INNER JOIN we_models B ON A.modelID = B.modelID WHERE b.international = 0 and A.brandID = '" & brandid & "' AND A.store = 0 ORDER BY A.SumQty DESC"
								'sql = "select top 5 a.modelID, a.modelName, a.modelImg, sum(b.numberOfSales) as qtySold from we_models a left join we_items b on a.modelID = b.modelID and b.typeID <> 16 and b.hideLive = 0 where a.brandID = " & brandid & " and a.international = 0 group by a.modelID, a.modelName, a.modelImg order by qtySold desc"
								if brandID = 17 then
									sql = "select top 5 a.modelID, b.modelName, b.modelImg, a.orderNum from we_popPhones a join we_models b on a.modelID = b.modelID where a.storeID = 0 and a.brandID = " & brandID & " and b.isPhone = " & phoneOnly & " order by a.orderNum"
								else
									sql = "select top 5 a.modelID, b.modelName, b.modelImg, a.orderNum from we_popPhones a join we_models b on a.modelID = b.modelID where a.storeID = 0 and a.brandID = " & brandID & " order by a.orderNum"
								end if
								
'								response.write sql
								
'								set RS2 = Server.CreateObject("ADODB.Recordset")
								session("errorSQL") = SQL
								set RS2 = oConn.execute(SQL)
								if not RS2.eof then
							%>
							<tr>
								<td align="center" valign="top">
									<h3>Popular <%=brandName%> Cell Phone Models</h3>
									<table border="0" cellspacing="10" cellpadding="0" width="100%">
										<tr>
											<%
											dim strPopularModels
											strPopularModels = ""
											lap = 0
											do until RS2.eof
												lap = lap + 1
												modelID = RS2("modelID")
												modelName = RS2("modelName")
												modelImg = RS2("modelImg")
												orderNum = rs2("orderNum")
												if brandid = "17" then
													altTag = modelName & " Accessories"
												else
													altTag = brandName & " " & modelName & " Accessories"
												end if
												strPopularModels = strPopularModels & modelID & ","
												RS2.movenext
												%>
												<td width="160" align="center" valign="top">
													<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td width="100%" height="150" align="center" valign="top">
                                                            	<table border="0" cellpadding="0" cellspacing="0" align="center">
                                                                	<tr>
                                                                    	<% if (session("adminID") = 54 or session("adminID") = 45) and lap > 1 then %>
                                                                        <td><a href="/admin/adjPopPhones.asp?upOrderNum=<%=modelID%>&brandID=<%=brandID%>&curOrderNum=<%=orderNum%>&phoneOnly=<%=phoneOnly%>" style="font-size:24px; font-family:Arial; font-weight:bold; text-decoration:none;">&lt;</a></td>
                                                                        <% end if %>
                                                                    	<td>
																			<%if modelID = 940 then%>
                                                                                <a href="/apple-ipad-accessories.asp" title="<%=altTag%>"><img src="/productPics/models/<%=modelImg%>" border="0" alt="<%=replace(altTag,"Accessories","accessories")%>"></a><br />																			
                                                                                <a class="cellphone-font" href="/apple-ipad-accessories.asp" title="<%=altTag%>"><%=replace(modelName,"/"," / ") & "&nbsp;Accessories"%></a>
                                                                            <%else%>
                                                                                <a href="/T-<%=modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp" title="<%=altTag%>"><img src="/productPics/models/<%=modelImg%>" border="0" alt="<%=replace(altTag,"Accessories","accessories")%>"></a><br />																			
                                                                                <a class="cellphone-font" href="/T-<%=modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp" title="<%=altTag%>"><%=replace(modelName,"/"," / ") & "&nbsp;Accessories"%></a>
                                                                            <%end if%>
                                                                        </td>
                                                                        <% if (session("adminID") = 54 or session("adminID") = 45) and not rs2.EOF then %>
                                                                        <td><a href="/admin/adjPopPhones.asp?downOrderNum=<%=modelID%>&brandID=<%=brandID%>&curOrderNum=<%=orderNum%>" style="font-size:24px; font-family:Arial; font-weight:bold; text-decoration:none;">&gt;</a></td>
                                                                        <% end if %>
                                                                    </tr>
                                                                </table>
															</td>
														</tr>
                                                        <% if session("adminID") = 54 or session("adminID") = 45 then %>
                                                        <tr>
                                                            <td align="center" id="popOp_<%=modelID%>">
                                                                <a href="/admin/adjPopPhones.asp?removeModelID=<%=modelID%>&brandID=<%=brandID%>&curOrderNum=<%=orderNum%>&phoneOnly=<%=phoneOnly%>">Remove From<br />Popular Phones</a>
                                                            </td>
                                                        </tr>
                                                        <% end if %>
														<tr>
															<td colspan="2" align="center" valign="middle"><img src="/images/spacer.gif" width="1" height="6"></td>
														</tr>
													</table>
												</td>
												<%
											loop
											RS2.close
											set RS2 = nothing
											%>
										</tr>
									</table>
								</td>
							</tr>
							<%
							end if
							%>
							<tr>
								<td width="100%" align="center" valign="top">
									<img src="/images/spacer.gif" width="1" height="10" border="0">
								</td>
							</tr>
							<tr>
								<td align="left" valign="top" width="100%">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td width="500" align="left" valign="top">
												<form id="formModel" name="formModel">
													<% if brandID = 17 and phoneOnly = 0 then %>
                                                    Please Select Your Product&nbsp;&nbsp;&nbsp;
													<% else %>
                                                    Please Select Your Phone Model&nbsp;&nbsp;&nbsp;
                                                    <% end if %>
													<select name="selectModel" onChange="if(this.value != ''){window.location=this.value;}">
														<option value="">Select from this list or click on an image below</option>
														<%
														do until RS.eof
															if RS("modelID") = 940 then
																response.write "<option value=""/apple-ipad-accessories.asp"">" & RS("modelName") & "</option>" & vbcrlf
															else
																response.write "<option value=""/T-" & RS("modelID") & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(RS("modelName")) & ".asp"">" & RS("modelName") & "</option>" & vbcrlf
															end if
															RS.movenext
														loop
														RS.movefirst
														%>
													</select>
												</form>
											</td>
											<td width="300" align="right" valign="top">
												<form id="formSort" name="formSort" method="post" action="<%=brandSEO(brandid)%>">
												Sort By&nbsp;&nbsp;&nbsp;
													<select name="nSorting" id="nSorting" onChange="this.form.submit();">
														<!--<option value="1"<%if nSorting <> "2" and nSorting <> "3" then response.write " selected"%>>Popularity</option>-->
														<option value="2"<%if nSorting = "2" then response.write " selected"%>>Alphabet</option>
														<option value="3"<%if nSorting <> "2" then response.write " selected"%>>Release Date</option>
													</select>
												</form>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center" valign="top" width="100%">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<%
											a = 0
											intPhones = 0
											do until RS.eof
												if rs("international") and intPhones = 0 then
											%>
                                        </tr>
                                        <tr>
                                        	<td id="intPhones" align="left" style="font-size:18px; font-weight:bold;" colspan="5" nowrap="nowrap">
                                            	<form id="formInt" name="formInt" method="post" action="<%=brandSEO(brandid)%>">
													International Phones (<a href="#" onclick="formInt.submit()">Hide International Phones</a>)
                                                    <input type="hidden" name="showIntPhones" value="0" />
                                                </form>
                                            </td>
                                        </tr>
                                        <tr>
                                        	<td align="center" valign="top" colspan="5"><br><img src="/images/grayline_800.jpg" width="800" height="2" border="0"><br><br></td>
                                        </tr>
                                        <tr>
                                        	<%
													intPhones = 1
												end if
												modelID = RS("modelID")
												modelName = RS("modelName")
												modelImg = RS("modelImg")
												if brandID = "17" then
													altText = modelName
												elseif brandID = "4" then
													altText = "LG Accessories: LG " & modelName & " Accessories"
												elseif brandID = "18" then
													altText = "Pantech Accessories: Pantech " & modelName & " Accessories"
												elseif brandID = "9" then
													altText = "Samsung Cell Phone Accessories: Samsung " & modelName & " Accessories"
												elseif brandID = "20" then
													altText = "HTC Accessories: HTC " & modelName & " Accessories"
												else
													altText = brandName & " " & modelName
												end if
												altTextComp = altText & " Cell Phone Accessories"
												if brandID = 17 and phoneOnly = 0 then altTextComp = replace(altTextComp,"Cell Phone ","")
												if inStr(strPopularModels,cStr(modelID) & ",") = 0 then
													%>
													<td width="160" align="center" valign="top">
														<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
															<tr>
																<td align="center" valign="top">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
																		<tr>
																			<td align="center" valign="middle">
																				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
																					<tr>
																						<td align="left" valign="bottom" height="18" colspan="2"><img src="/images/spacer.gif" width="10" height="1"></td>
																					</tr>
																					<tr>
																						<td width="100%" height="150" align="center" valign="top">
																							<%if modelID = 940 then%>
																								<a href="/apple-ipad-accessories.asp" title="<%=altText%> Accessories"><img src="/productPics/models/<%=modelImg%>" border="0" alt="<%=altTextComp%>"></a><br />																							
																								<a class="cellphone-font" href="/apple-ipad-accessories.asp" title="<%=altText%> Accessories"><%=replace(modelName,"/"," / ")%></a>
																							<%else%>
																								<a href="/T-<%=modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp" title="<%=altText%> Accessories"><img src="/productPics/models/<%=modelImg%>" border="0" alt="<%=altTextComp%>"></a><br />																							
																								<a class="cellphone-font" href="/T-<%=modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp" title="<%=altText%> Accessories"><%=replace(modelName,"/"," / ") & "&nbsp;Accessories"%></a>
																							<%end if%>
																						</td>
																					</tr>
                                                                                    <% if session("adminID") = 54 or session("adminID") = 45 then %>
                                                                                    <tr>
                                                                                    	<td align="center" id="popOp_<%=modelID%>">
                                                                                        	<a href="/admin/adjPopPhones.asp?addModelID=<%=modelID%>&brandID=<%=brandID%>&phoneOnly=<%=phoneOnly%>">Popular Phone</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <% end if %>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td align="center" valign="middle"><img src="/images/spacer.gif" width="1" height="6"></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
													<%
													a = a + 1
													if a = 5 then
														response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""5""><br><img src=""/images/grayline_800.jpg"" width=""800"" height=""2"" border=""0""><br><br></td></tr><tr>" & vbcrlf
														a = 0
													end if
												end if
												RS.movenext
												if not rs.EOF then
													if showIntPhones = 0 and rs("international") then
														displayIntLink = 1
														do while not rs.EOF
															rs.movenext
														loop
													elseif rs("international") and a > 0 and intPhones = 0 then
														response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""5""><br><img src=""/images/grayline_800.jpg"" width=""800"" height=""2"" border=""0""><br><br></td></tr><tr>" & vbcrlf
														a = 0
													end if
												end if
											loop
											if a = 1 then
												response.write "<td width=""160"">&nbsp;</td><td width=""160"">&nbsp;</td><td width=""160"">&nbsp;</td><td width=""160"">&nbsp;</td>" & vbcrlf
											elseif a = 2 then
												response.write "<td width=""160"">&nbsp;</td><td width=""160"">&nbsp;</td><td width=""160"">&nbsp;</td>" & vbcrlf
											elseif a = 3 then
												response.write "<td width=""160"">&nbsp;</td><td width=""160"">&nbsp;</td>" & vbcrlf
											elseif a = 4 then
												response.write "<td width=""160"">&nbsp;</td>" & vbcrlf
											end if
											%>
										</tr>
									</table>
								</td>
							</tr>
                            <% if displayIntLink = 1 then %>
                            <tr>
                                <td align="center" valign="top" colspan="5"><br><img src="/images/grayline_800.jpg" width="800" height="2" border="0"><br><br></td>
                            </tr>
                            <tr>
                            	<td align="left">
                                	<form id="formInt" name="formInt" method="post" action="<%=brandSEO(brandid)%>#intPhones">
                                        <a href="#" onclick="formInt.submit()" style="font-size:18px; font-weight:bold;">Show International Phones</a>
                                    	<input type="hidden" name="showIntPhones" value="1" />
                                    </form>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" colspan="5"><br><img src="/images/grayline_800.jpg" width="800" height="2" border="0"></td>
                            </tr>
                            <% end if %>
							<!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
							<tr>
								<td align="left" valign="top" width="100%">
									<!--#include virtual="/includes/asp/inc_SEOtext_brand.asp"-->
									<p><%=brandText%></p>
								</td>
							</tr>
							<!--#include virtual="/includes/asp/inc_SEOtext_store.asp"-->
							<tr>
								<td align="center">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="left" valign="top" class="bottomText">
												<div align="justify">
													<p style="margin-top:0px;margin-bottom:6px;"><%=topText%></p>
													<p><%=brandStaticText%></p>
													<%if bottomText <> "" then%>
														<p><%=bottomText%></p>
													<%end if%>
													<p><%=storeText%></p>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="left" valign="top">&nbsp;</td>
							</tr>
						</table>
					</td>
                    </tr>
                    <%=TABLE_END%>

<%
call fCloseConn()
%>

<!--#include virtual="/includes/template/bottom.asp"-->
