<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/Framework/Data/ProductDetail/Base.asp"-->
<!--#include virtual="/includes/asp/inc_Compatibility.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
mvtGaq = "WE PDP-A"
response.buffer = true
dim itemid, itemDesc, productPage, modelID, URL, curSite, isTablet
dim pageName : pageName = "product.asp"
dim basePageName : basePageName = "product.asp"
googleAds = 1
noCommentBox = true
productPage = 1
'generic tag is not working, commented
'pageTitle = "product"		
pageTitle = "product.asp"
isTablet = false
use500 = true

gblShipoutDate = "Order today and this item will ship out within 24 hours."
shipoutCutoff = cdate(Date & " 12:00:00 PM")
if now < shipoutCutoff then 
	nextMin = datediff("n", now, shipoutCutoff)
	if cint(nextMin/60) = 0 then
		gblShipoutDate = "Order within the next <b>" & cint(nextMin mod 60) & " min</b> and this item will ship out today."
	else
		gblShipoutDate = "Order within the next <b>" & cint(nextMin/60) & " hrs</b> and <b>" & cint(nextMin mod 60) & " min</b> and your item ships out today."
	end if
end if

itemid = prepInt(request.querystring("itemid"))
musicSkins = prepInt(request.QueryString("musicSkins"))

response.Cookies("saveItemID") = itemID

dim jpeg : set jpeg = Server.CreateObject("Persits.Jpeg")
dim fs : set fs = CreateObject("Scripting.FileSystemObject")

dim OutOfStock
OutOfStock = 0

dim strTypename, strBrandname, strModelname, strItemname
call fOpenConn()

sql = "exec sp_recommendedItems 0," & itemID
set recommendRS = oConn.execute(sql)

maxIndex = 3
for formIndex = 1 to maxIndex
	recentItem = Request.Cookies("RecentItem" & formIndex)("id")
	if inStr(recentItem,"/") > 0 then recentItem = left(recentItem,inStr(recentItem,"/")-1)
	if recentItem <> "" and isNumeric(recentItem) then Execute("thisRecentItem" & formIndex & "id" & "=" & recentItem)
next
if thisRecentItem1ID <> "" then conditionalQuery = conditionalQuery & " ItemID = " & thisRecentItem1ID
if thisRecentItem2ID <> "" then
	if conditionalQuery <> "" then conditionalQuery = conditionalQuery & " OR "
	conditionalQuery = conditionalQuery &  " ItemID = " & thisRecentItem2ID
end if
if thisRecentItem3ID <> "" then
if conditionalQuery <> "" then conditionalQuery = conditionalQuery & " OR "
	conditionalQuery = conditionalQuery & " ItemID = " & thisRecentItem3ID
end if
if thisRecentItem1ID <> "" or thisRecentItem2ID <> "" or thisRecentItem3ID <> "" then
	showRecent = 1
	if conditionalQuery = "" then conditionalQuery = "a.partNumber not like 'WCD-%'" else conditionalQuery = conditionalQuery & " and a.partNumber not like 'WCD-%'"
	sql = "SELECT c.brandName, d.modelName, A.itemID, A.itemDesc, A.itemPic, A.price_retail, A.modelID, A.price_Our, A.HandsfreeType, B.typeName FROM we_items A"
	sql = sql & " INNER JOIN we_types B ON A.typeID=B.typeID left join we_brands c on a.brandID = c.brandID left join we_models d on a.modelID = d.modelID"
	sql = sql & " WHERE " & conditionalQuery & " ORDER BY A.numberOfSales DESC"
	set recentRS = oConn.execute(sql)
else
	showRecent = 0
end if

SQL = "exec sp_getBrandModelByItemID " & itemID
set RS = oConn.execute(SQL)
if not RS.eof then
	strTypename = nameSEO(RS("typename"))
	strBrandname = RS("brandname")
	modelID = RS("modelID")
	strModelname = RS("modelname")
	strItemname = insertDetails(RS("itemdesc"))
else
	if musicSkins = 1 then
		strItemname = insertDetails(itemDesc)
		strTypename = "music skins"
		strBrandname = brandName
		strModelname = modelName
		
		SEtitle = insertDetails(itemDesc) & " " & strBrandname & " " & strModelname & " Music Skin"
		SEdescription = "Buy the " & insertDetails(itemDesc) & " Music Skin for the " & strBrandname & " " & strModelname & " at Wireless Emporium with Free Shipping!"
		SEkeywords = insertDetails(itemDesc) & ", " & insertDetails(itemDesc) & " music skin, " & strBrandname & " " & strModelname & " skins, " & strModelname & " skins, cell phone skin, phone skins"
	else
		strTypename = "Misc. Gear"
		strBrandname = "ALL Cell Phone Brands"
		strModelname = ""
		SQL = "exec sp_itemDescByItemID " & itemID
		dim RS2
		set RS2 = oConn.execute(SQL)
		if not RS2.eof then strItemname = insertDetails(RS2("itemdesc"))
		RS2.close
		set RS2 = nothing
	end if
end if
RS.close
set RS = nothing

'========================================================= color picker ========================
sql = "exec sp_productColorOptions"
session("errorSQL") = sql
arrColors = getDbRows(sql)

dim colorSlaves : colorSlaves = ""
dim numAvailColors : numAvailColors = cint(0)
dim colorid : colorid = 0
if musicSkins = 0 then
	SQL = "exec sp_getAvailColors " & itemID
	session("errorSQL") = SQL
	set RS = oConn.execute(sql)
	
	if not rs.eof then
		colorid			=	rs("curColorId")
		colorSlaves		=	rs("availColors")
		if instr(colorSlaves,",") > 0 then 
			numAvailColors = ubound(split(colorSlaves, ",")) + 1
		end if
	end if
end if


dim modelLink
if modelID = 940 then
	modelLink =  "http://www.wirelessemporium.com/apple-ipad-accessories.asp"
else
	modelLink = "http://www.wirelessemporium.com/T-" & modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
end if

dim cart
session("otherAdminOptions") = ", <a style='color:#ffffff;' href='/admin/db_update_models.asp?ModelID=" & modelID & "&BrandID=" & brandID & "&searchType=Model&submitType=Edit&submitModel=Search'>Edit " & modelName & "</a>, <a style='color:#ffffff;' href='/admin/db_update_items.asp?ItemID=" & itemID & "'>Edit This Product</a>"
if len(itemDesc) > 0 then
	displayItemDesc = insertDetails(itemDesc)
	itemDesc = replace(replace(itemDesc,"(",""),")","")
end if


'todo: need to consolidate Metatags with we_XXXText
dim strH1: strH1 = displayItemDesc
dim strH2: strH2 = EMPTY_STRING
dim strAltText: strAltText = EMPTY_STRING
dim strBreadcrumb: strBreadcrumb = strItemname
if typeID = 16 then 
	if brandName <> "Other" then strBreadcrumb = brandName &" "& modelName else strBreadcrumb = modelName
end if

'used for product rating and product review [knguyen/20110610]
dim objSqlExecReview: set objSqlExecReview = GetUserRatingStat( ItemId)
dim strAvgRating: strAvgRating = objSqlExecReview.Item( "AvgRating")
dim dblAvgRating: dblAvgRating = cdbl( strAvgRating)
dim strRatingCount: strRatingCount = objSqlExecReview.Item( "RatingCount")
dim strRatingLevel:
set objSqlExecReview = nothing

if musicSkins = 1 then 'hack in original pricing as constant .. currently no blow-out support for this table [knguyen/20110602]
	SQL = "exec sp_pullMusicSkinsDetailsByItemID " & itemID
else
	SQL = "exec sp_pullProductDetailsByItemID " & itemID
end if
session("errorSQL") = SQL
set RS = oConn.execute(sql)

if rs.EOF then
	session("errorCode") = "p-101"
	if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
		sql = "select modelID, partNumber from we_Items where hideLive = 0 and ghost = 0 and itemID = " & itemID
		session("errorSQL") = sql
		set modelDetailsRS = oConn.execute(sql)
		
		if modelDetailsRS.EOF then
			response.Write("Can't find this item in the siteReady table")
			response.End()
		else
			useModelID = prepInt(modelDetailsRS("modelID"))
			partNumber = prepStr(modelDetailsRS("partNumber"))
			
			if left(partNumber,4) = "WCD-" then
				response.Write("This is an illegal value, no WCD allowed here")
				response.End()
			else
				sql = "exec sp_createProductListByModelID " & useModelID
				session("errorSQL") = sql
				oConn.execute(sql)
				
				response.Redirect(request.ServerVariables("HTTP_X_REWRITE_URL"))
			end if
		end if
	else
		sql = "select modelID, partNumber from we_Items where hideLive = 0 and ghost = 0 and itemID = " & itemID
		session("errorSQL") = sql
		set modelDetailsRS = oConn.execute(sql)
		
		if modelDetailsRS.EOF then
			call PermanentRedirect("/")
		else
			useModelID = prepInt(modelDetailsRS("modelID"))
			partNumber = prepStr(modelDetailsRS("partNumber"))
			
			if left(partNumber,4) = "WCD-" then
				call PermanentRedirect("/")
			else			
				sql = "exec sp_createProductListByModelID " & useModelID
				session("errorSQL") = sql
				oConn.execute(sql)
				
				if musicSkins = 1 then 'hack in original pricing as constant .. currently no blow-out support for this table [knguyen/20110602]
					SQL = "exec sp_pullMusicSkinsDetailsByItemID " & itemID
				else
					SQL = "exec sp_pullProductDetailsByItemID " & itemID
				end if
				session("errorSQL") = SQL
				set RS = oConn.execute(sql)
				
				if RS.EOF then call PermanentRedirect("/")
			end if
		end if
	end if
end if

if not RS.eof then
	dim NoDiscount, partnumber, brandID, typeID, itempic, price_retail, price_Our, KIT, HandsfreeType, seasonal
	dim modelName, modelImg, brandName, categoryName, SquaretradeItemCondition, UPCCode, vendor, strActiveItemValueType, blnIsNotOriginalPrice, strOriginalPrice
	dim BULLET1, BULLET2, BULLET3, BULLET4, BULLET5, BULLET6, BULLET7, BULLET8, BULLET9, BULLET10
	dim strItemLongDetail, COMPATIBILITY, download_URL, download_TEXT, PackageContents
	brandID = RS("brandID")
	typeID = RS("typeID")
	subTypeID = rs("subtypeID")
	vendor = RS("vendor")
	partnumber = RS("partnumber")
	if left(partnumber,3) = "WCD" then response.Redirect("/")
	itemDesc = insertDetails(RS("itemDesc"))
	itempic = RS("itempic")
	price_retail = RS("price_retail")
	price_Our = RS("price_Our")
	masterInvQty = RS("inv_qty")
	HandsfreeType = RS("HandsfreeType")
	KIT = RS("ItemKit_NEW")
	strItemLongDetail = RS("itemLongDetail")
	if not isNull(strItemLongDetail) then
		strItemLongDetail = replace(strItemLongDetail,"Wireless Emporium","WirelessEmporium.com")
		'strItemLongDetail = replace(strItemLongDetail," FREE!"," less!")
		strItemLongDetail = replace(strItemLongDetail,chr(34),"''")
		strItemLongDetail = replace(strItemLongDetail,"<a href='/downloads/","<a href='/downloads/")
		strItemLongDetail = replace(strItemLongDetail,vbcrlf," ")
	end if
	if musicSkins = 1 then
		BULLET1 = ""
		BULLET2 = ""
		BULLET3 = ""
		BULLET4 = ""
		BULLET5 = ""
		BULLET6 = ""
		BULLET7 = ""
		BULLET8 = ""
		BULLET9 = ""
		BULLET10 = ""
		download_URL = ""
		download_TEXT = ""
		PackageContents = ""
		COMPATIBILITY = ""
	else
		if not isNull(RS("BULLET1")) and RS("BULLET1") <> "" then BULLET1 = RS("BULLET1")
		if not isNull(RS("BULLET2")) and RS("BULLET2") <> "" then BULLET2 = RS("BULLET2")
		if not isNull(RS("BULLET3")) and RS("BULLET3") <> "" then BULLET3 = RS("BULLET3")
		if not isNull(RS("BULLET4")) and RS("BULLET4") <> "" then BULLET4 = RS("BULLET4")
		if not isNull(RS("BULLET5")) and RS("BULLET5") <> "" then BULLET5 = RS("BULLET5")
		if not isNull(RS("BULLET6")) and RS("BULLET6") <> "" then BULLET6 = RS("BULLET6")
		if not isNull(RS("BULLET7")) and RS("BULLET7") <> "" then BULLET7 = RS("BULLET7")
		if not isNull(RS("BULLET8")) and RS("BULLET8") <> "" then BULLET8 = RS("BULLET8")
		if not isNull(RS("BULLET9")) and RS("BULLET9") <> "" then BULLET9 = RS("BULLET9")
		if not isNull(RS("BULLET10")) and RS("BULLET10") <> "" then BULLET10 = RS("BULLET10")
		
		if prepStr(RS("COMPATIBILITY")) = "" then
			COMPATIBILITY = CompatibilityList(PartNumber,"display")
		else
			COMPATIBILITY = RS("COMPATIBILITY")
		end if
	end if
	alwaysInStock = prepStr(RS("alwaysInStock"))
	customize = rs("customize")
	modelName = RS("modelName")
	modelImg = RS("modelImg")
	brandName = RS("brandName")
	itemDimentions = RS("itemDimensions")
	seasonal = RS("seasonal")
	NoDiscount = RS("NoDiscount")
	SquaretradeItemCondition = RS("Condition")
	UPCCode = RS("UPCCode")
	isTablet = RS("isTablet")
	showAnimation = RS("showAnimation")
	categoryName = RS("typeName")
	strActiveItemValueType = RS("ActiveItemValueType"): blnIsNotOriginalPrice = (strActiveItemValueType<>"OriginalPrice")
	strOriginalPrice = RS("OriginalPrice")
	if musicSkins = 1 then
		defaultImg = rs("defaultImg")
		sampleImg = rs("sampleImg")
	else
		download_TEXT = RS("download_TEXT")
		download_URL = RS("download_URL")
		PackageContents = RS("PackageContents")
	end if
	if not isnull(KIT) and len(KIT) > 0 then
		SQL = "exec sp_pullKitItems '" & KIT & "'"
		session("errorSQL") = SQL
		set RS2 = oConn.execute(sql)
		if RS2.eof then
			OutOfStock = 1
		else
			lowAmt = 100
			do while not RS2.EOF
				if cdbl(RS2("inv_qty")) < lowAmt then lowAmt = cdbl(RS2("inv_qty"))
				if RS2("inv_qty") < 1 then OutOfStock = 1
				RS2.movenext
			loop
		end if
		RS2.close
		set RS2 = nothing
	elseif masterInvQty <= 0 then
		OutOfStock = 1
	end if
end if


'=========================================================================================
if musicSkins <> 1 then
	Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
		oParam.CompareMode = vbTextCompare
		oParam.Add "x_itemID", itemid
		oParam.Add "x_itemDesc", itemDesc
	'call redirectURL("p", "", request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
end if
'=========================================================================================

sql = 	"exec sp_pullQandAByPartNumber '" & partnumber & "'"
session("errorSQL") = sql
arrUGC = getDbRows(sql)

arrVideo = null
sql = "exec sp_getVideosByItemID " & itemid
session("errorSQL") = sql
arrVideo = getDbRows(sql)

noIndex = 1
%>
<!--#include virtual="/includes/template/top_product.asp"-->
<%
on error resume next
for formIndex = 1 to 3
	recentItem = Request.Cookies("RecentItem" & formIndex)("id")
	if inStr(recentItem,"/") > 0 then recentItem = left(recentItem,inStr(recentItem,"/")-1)
	if recentItem <> "" and isNumeric(recentItem) then response.Cookies("RecentItem" & formIndex)("id") = recentItem
next
if Request.Cookies("RecentItem1")("id") = "" OR (Request.Cookies("RecentItem1")("id") = itemid) then
	Response.Cookies("RecentItem1")("id") = itemid
	Response.Cookies("RecentItem1")("date") = now
	Response.Cookies("RecentItem1").Expires = DateAdd("m", 1, now)
elseif Request.Cookies("RecentItem2")("id") = "" OR (Request.Cookies("RecentItem2")("id") = itemid) then
	Response.Cookies("RecentItem2")("id") = itemid
	Response.Cookies("RecentItem2")("date") = now
	Response.Cookies("RecentItem2").Expires = DateAdd("m", 1, now)
elseif Request.Cookies("RecentItem3")("id") = "" OR (Request.Cookies("RecentItem3")("id") = itemid) then
	Response.Cookies("RecentItem3")("id") = itemid
	Response.Cookies("RecentItem3")("date") = now
	Response.Cookies("RecentItem3").Expires = DateAdd("m", 1, now)
else
	dim formIndex, thisleastRecentIndex, thisleastRecentDate
	thisleastRecentIndex = 1
	thisleastRecentDate = Request.Cookies("RecentItem1")("date")
	for formIndex = 1 to 3 step 1
		if thisleastRecentDate > Request.Cookies("RecentItem" & formIndex)("date") then
			thisLeastRecentDate = Request.Cookies("RecentItem" & formIndex)("date")
			thisLeastRecentIndex = formIndex
		end if
	next
	Response.Cookies("RecentItem" & thisLeastRecentIndex)("id") = itemid
	Response.Cookies("RecentItem" & thisLeastRecentIndex)("date") = now
	Response.Cookies("RecentItem" & thisLeastRecentIndex).Expires = DateAdd("m", 1, now)
end if
on error goto 0

'MG 2013.03.13 Task 1634 -- Display review accross simliar products.  
sql = "exec sp_pullProductReviewsByItemID " & itemid  
'sql = "exec sp_pullProductReviewsByPartNumber '" & partNumber & "'"

session("errorSQL") = sql
set reviewRS = oConn.execute(sql)

if reviewRS.EOF then
	dblAvgRating = 0
	strRatingCount = 0
else
	dblAvgRating = reviewRS("avgRating")
	strRatingCount = reviewRS("reviewCnt")
end if

dim bFinalOutofStock : bFinalOutofStock = false
if OutOfStock = 1 and (alwaysInStock = "" or alwaysInStock = "False") then
	bFinalOutofStock = true
end if

specialOffer = 0
validProd = split("FP1|FP2|FP3|FP4|FP5|FP6","|")
for i = 0 to ubound(validProd)
	if instr(partNumber,validProd(i)) > 0 then specialOffer = 1
next
if NoDiscount then specialOffer = 0

featureList = BULLET1 & "##" & BULLET2 & "##" & BULLET3 & "##" & BULLET4 & "##" & BULLET5 & "##" & BULLET6 & "##" & BULLET7 & "##" & BULLET8 & "##" & BULLET9 & "##" & BULLET10
do while instr(featureList,"####") > 0
	featureList = replace(featureList,"####","##")
loop
featureListArray = split(featureList,"##")

dim itemImgAbsolutePath, itemImgPath, itemSamplePath, itemImgFullPath, objItemImgFull, hasFlashFile

sampleImg = ""
itemImgPath = ""
itemSamplePath = ""
itemImgFullPath = ""
itemImgAbsolutePath = ""
objItemImgFull = ""
strAltImage = ""
strZoom = ""
objZoom = ""
hasFlashFile = false

session("errorSQL") = "itempic:" & itempic
if itempic <> "" then
	flashImgPath = "/productpics/swf/" & replace(itempic, ".jpg", ".swf")
	if fs.FileExists(Server.MapPath(flashImgPath)) then hasFlashFile = true
end if

altText = brandName & "&nbsp;" & modelName & "&nbsp;" & singularSEO(categoryName) & "&nbsp;-&nbsp;" & itemDesc
strItemCheck = itemID


'if fs.fileExists(server.MapPath("/productpics/big/zoom/" & itempic)) then 
	strZoom = "onclick=""showFloatingZoomImage();"""
	objZoom = "<a href=""javascript:showFloatingZoomImage();""><img src=""/images/product/Click-to-Zoom.jpg"" border=""0"" /></a>"
'end if

if musicSkins = 1 then
	itemImgPath = "/productpics/musicSkins/musicSkinsLarge"
	itemImgFullPath = itemImgPath & "/" & itemPic
	itemImgAbsolutePath = Server.MapPath(itemImgFullPath)
else
	itemImgPath = "/productpics/big"
	itemImgFullPath = itemImgPath & "/" & itemPic
	itemImgAbsolutePath = Server.MapPath(itemImgFullPath)
end if

if not fs.FileExists(itemImgAbsolutePath) then
	itemPic = "imagena.jpg"
	if musicSkins = 1 then
		itemImgPath = "/productpics/musicSkins/musicSkinsDefault"
		itemImgFullPath = itemImgPath & "/" & defaultImg
		if fs.FileExists(Server.MapPath(itemImgFullPath)) then
			itemSamplePath = "/productpics/musicSkins/musicSkinsSample"
			if fs.FileExists(Server.MapPath(itemSamplePath & "/" & replace(defaultImg,"artwork-thumbnail.jpg","sample-devices.jpg"))) then
				sampleImg = replace(defaultImg,"artwork-thumbnail.jpg","sample-devices.jpg")
			end if
			
			objItemImgFull = "<img " & strZoom & " id=""imgLarge"" itemprop=""image"" src=""http://www.wirelessemporium.com" & itemImgFullPath & """ border=""0"" title=""" & replace(insertDetails(altText), "'", "") & """>"
		else
			objItemImgFull =  "<img " & strZoom & " id=""imgLarge"" itemprop=""image"" src=""http://www.wirelessemporium.com" & itemImgFullPath & """ width=""300"" border=""0"" title=""" & replace(insertDetails(altText), "'", "") & """>"
		end if
	else ' non-music skins, itemImgAbsolutePath doesn't exist
		objItemImgFull = "<div style=""width:300px; display:block; position: relative; z-index:1;"">"
		objItemImgFull = objItemImgFull & "<div style=""display:block; position:absolute; top:0px; left:0px; z-index:2;""><img " & strZoom & " id=""imgLarge"" itemprop=""image"" src=""http://www.wirelessemporium.com/productpics/big/" & itempic & """ width=""300"" height=""300"" border=""0"" title=""" & replace(insertDetails(altText), "'", "") & """></div>"
		
		if blnIsNotOriginalPrice then objItemImgFull = objItemImgFull & "<div style=""display:block; position: absolute; top:0px; right:0px; z-index:3;""><img id=""imgStar"" src=""/images/LargeStar.png"" alt=""Sale Price"" title=""Sale Price"" border=""0"" /></div>"
		if hasFlashFile then  objItemImgFull = objItemImgFull & "<div style=""cursor:pointer; position: absolute; width:78px; height:80px; bottom:0px; right:0px; z-index:5;""><img id=""id_view360"" onClick=""playFlash();"" src=""/images/product/360/360-clicktoview-button.png"" alt=""360 image"" title=""360 image"" border=""0"" /></div>"
		
		objItemImgFull = objItemImgFull & "</div>"
	end if
else 'itemImgAbsolutePath exists
	objItemImgFull = "<div style=""width:300px; display:block; position: relative; z-index:2;"">"
	if musicSkins = 1 then 
		objItemImgFull = "<img " & strZoom & " id=""imgLarge"" itemprop=""image"" src=""http://www.wirelessemporium.com" & itemImgFullPath & """ width=""300"" border=""0"" alt=""" & replace(altText, "'", "") & """>"
	else
		objItemImgFull = objItemImgFull & "<div style=""display:block; position:absolute; top:0px; left:0px; z-index:3;""><img " & strZoom & " id=""imgLarge"" itemprop=""image"" src=""http://www.wirelessemporium.com/productpics/big/" & itempic & """ width=""300"" height=""300"" border=""0"" title=""" & replace(insertDetails(altText), "'", "") & """></div>"
		if blnIsNotOriginalPrice then objItemImgFull = objItemImgFull & "<div style=""display:block; position:absolute; top:0px; right:0px; z-index:4;""><img id=""imgStar"" src=""/images/LargeStar.png"" alt=""Sale Price"" title=""Sale Price"" border=""0"" /></div>"
		if hasFlashFile then  objItemImgFull = objItemImgFull & "<div style=""cursor:pointer; position: absolute; width:78px; height:80px; top:200px; right:0px; z-index:5;""><img id=""id_view360"" onClick=""playFlash();"" src=""/images/product/360/360-clicktoview-button.png"" alt=""360 image"" title=""360 image"" border=""0"" /></div>"
		
		if customize then
			objItemImgFull = objItemImgFull & "<div style='position:absolute; right:0px; top:0px; z-index:5;'><a href='/custom/?aspid=" & itemID & "'><img src='/images/icons/customize.png' border='0' /></a></div>"
			objCustomize = "<a href=""/custom/?aspid=" & itemID & """><img src=""/images/icons/customize-cta.jpg"" border=""0"" /></a>"
		end if
	end if 
	objItemImgFull = objItemImgFull & "</div>"
end if

a = 0
for iCount = 0 to 7
	path = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".jpg")
	if fs.FileExists(Server.MapPath(path)) then
		a = a + 1
		src = replace(path,".jpg","_thumb.jpg")
		set f1 = fs.getFile(server.MapPath(path))
		if fs.FileExists(Server.MapPath(src)) then
			set f2 = fs.getFile(server.MapPath(src))
		else
			set f2 = fs.getFile(server.MapPath(path))
		end if
		if fs.FileExists(Server.MapPath(src)) and f1.DateLastModified < f2.DateLastModified then
			strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('" & path & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
		else
			session("errorSQL") = "filePath:" & Server.MapPath(path)
			if fs.FileExists(Server.MapPath(path)) then
				jpeg.Open Server.MapPath(path)
				jpeg.Height = 40
				jpeg.Width = 40
				jpeg.Save Server.MapPath(src)
				strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('" & path & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
				response.Write("<!-- make new alt image:" & a & " -->")
			end if
		end if
	end if
	path = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".gif")
	if fs.FileExists(Server.MapPath(path)) then
		a = a + 1
		src = path
		strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('" & src & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
	end if
	path = "/productpics/AltViews/" & replace(itempic,".gif","-" & iCount & ".jpg")
	if fs.FileExists(Server.MapPath(path)) then
		a = a + 1
		src = path
		strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('" & src & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
	end if
	path = "/productpics/AltViews/" & replace(itempic,".gif","-" & iCount & ".gif")
	if fs.FileExists(Server.MapPath(path)) then
		a = a + 1
		src = path
		strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('" & src & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
	end if
next
if typeID = 17 then
	a = a + 1
	if vendor = "DS" then
		strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/decal_pic5.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/decal_pic5.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
	else
		strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/gg_atl_image1.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/gg_atl_image1.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
	end if
end if
if vendor = "DS" then
	strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/decalskin-mg.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/decalskin-mg.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
end if
if musicSkins = 1 then
	a = a + 1
	strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/ms_altview1.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/ms_altview1.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
	a = a + 1
	strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/ms_altview2.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/ms_altview2.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
end if
if isnull(showAnimation) then showAnimation = true
if (lCase(left(partnumber,3)) = "fp2" or lCase(left(partnumber,3)) = "fp3") and showAnimation then
	a = a + 1
	strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/FP-SNAP-ON-ANIM.gif');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/FP-SNAP-ON-ANIM_icon.gif"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
	strAltImage = strAltImage & "<script language=""javascript"">" & vbcrlf
	strAltImage = strAltImage & "if (document.images) {" & vbcrlf
	strAltImage = strAltImage & "newLargeImg = new Image();" & vbcrlf
	strAltImage = strAltImage & "newLargeImg.src = '/productpics/AltViews/FP-SNAP-ON-ANIM.gif'"
	strAltImage = strAltImage & "}" & vbcrlf
	strAltImage = strAltImage & "</script>" & vbcrlf
end if

if strAltImage <> "" then strAltImageText = "<p class=""altImgText"">ROLLOVER ANY IMAGE TO VIEW FULL-SIZE</p>"

if not isnull(arrVideo) then
	for nRow = 0 to ubound(arrVideo,2)
		tVideoURL = arrVideo(0, nRow)
		tVideoTitle = arrVideo(1, nRow)
'		if len(tVideoTitle) > 40 then tVideoTitle = left(tVideoTitle, 37) & "..."

		if nRow = 0 then 
			strVideo = strVideo & "<a class=""linkVideo"" href=""" & tVideoURL & "?rel=0&amp;wmode=transparent"" title=""" & tVideoTitle & """ id=""id_linkVideo""><div class=""fl videoLink""></div></a>" & vbcrlf
		else
			strVideo = strVideo & "<a class=""linkVideo"" href=""" & tVideoURL & "?rel=0&amp;wmode=transparent"" title=""" & tVideoTitle & """ style=""display:none;""><div class=""fl videoLink""></div></a>" & vbcrlf
		end if
	next
end if

brandID = prepInt(brandID)
modelID = prepInt(modelID)

if not isnull(arrVideo) then
%>
	<!--#include virtual="/includes/asp/inc_videoLightBox.asp"-->
<%
end if

if hasFlashFile then
%>
	<!--#include virtual="/includes/asp/inc_360view.asp"-->
<%
end if
%>
<link href="/includes/css/product/productBase2.css" rel="stylesheet" type="text/css">
<!--#include virtual="/includes/template/productHTML2.asp"-->
<!--#include virtual="/includes/template/bottom_product.asp"-->
<script type="text/javascript" src="/includes/js/dropdown.js"></script>
<script type="text/javascript" src="/includes/js/tabcontent.js"></script>
<script type="text/javascript" src="/includes/js/recaptcha_ajax.js"></script>
<script language="javascript">
	var numAvailColors = <%=numAvailColors%>;
	var partNumber = '<%=partNumber%>';
	var modelID = <%=modelID%>;
	var itemID = <%=itemID%>;
	var typeID = <%=typeID%>;
	var handsFreeType = '<%=handsFreeType%>';
	var recaptcha_public_key = '<%=recaptcha_public_key%>';
	var recaptcha_private_key = '<%=recaptcha_private_key%>';
	var hasFlashFile = '<%=hasFlashFile%>';
	var testAB = 'A';
	var musicSkins = <%=prepInt(musicSkins)%>;
	var recScrollVal = 0;
	var maxRecScroll = <%=recProdCnt*150%>;
	var rvScrollVal = 0;
	var maxRvScroll = <%=rvProdCnt*150%>;
</script>
<script language="javascript" src="/includes/js/productBase2.js?v=01182013"></script>