<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%if request.querystring("show") = "text" then%>
<html>
<head>
<title>Wireless Emporium Shipping Information</title>
<link rel="stylesheet" href="/includes/css/style.css" type="text/css">
</head>
<body>
<%else%>
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<%end if%>
<table border="0" align="center" cellspacing="0" cellpadding="0" width="95%">
    <%if request.querystring("show") <> "text" then%>
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;Shipping Information
        </td>
    </tr>
    <%end if%>
    <tr>
        <td align="left" valign="middle" width="100%">
            <h1>Shipping Information</h1>
        </td>
    </tr>
    <% if month(date) = 11 or month(date) = 12 then %>
    <tr><td align="center" style="font-weight:bold; font-size:14px;">Holiday Shipping Policy</td></tr>
    <tr>
        <td class="normaltext">
            <p align="justify">
                Wireless Emporium offers multiple shipping options to ensure timely delivery of your item for the Holidays. To help
				save our customers more money during this Holiday season, Wireless Emporium offers FREE SHIPPING ON ALL
				ORDERS. Please be advised, that such policy pertains to our standard shipping option via United States Postal
				Service (USPS) 1st Class mail.
                <br><br>
                Due to the nature of first class mail delivery, our customers who choose the free shipping option are STRONGLY
                ADVISED per the USPS to order AT LEAST NINE (9) SHIPPING DAYS BEFORE DECEMBER 24TH, 2012
                (or Tuesday, December 11th, 2012) to ensure timely delivery before Christmas day. This is primarily due to
                unpredictability of Holiday mail volume and delivery. If it is beyond that date, and pre-Christmas day delivery is critical,
                it is strongly suggested that customers opt for either our low-cost FLAT-RATE Priority shipping option (approx. 3
                business days), or our FLAT-RATE Express shipping option (1-2 business days).
                <ul>
                	<li>
                    	The last suggested pre-Christmas ordering date for FLAT-RATE Priority shipping (approx. 3 business days) is AT 
                        LEAST FOUR (4) SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Tuesday, December 17th, 2013).
                    </li>
                	<li>
                    	The last suggested pre-Christmas ordering date for FLAT-RATE Express shipping (1-2 business days) is AT LEAST 
                        THREE (3) SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Wednesday, December 18th, 2013).
                    </li>
                </ul>
                <strong>UPS Holiday Shipping Policy</strong>
                <br><br>
                For an additional fee, Wireless Emporium orders can be shipped via UPS. Orders shipped via UPS will be
                shipped within 1-2 business days and will be charged based on product weight and ship-to location. Tracking
                numbers & package tracking will be available online at UPS.com. All UPS shipments are insured up to $100.
                <br><br>
                Orders placed via UPS shipping need to be submitted by the following dates to ensure delivery in time for Christmas.
                <ul>
                	<li>The last suggested pre-Christmas ordering date for UPS Ground (approx. 6 business days) is AT LEAST SIX (6) SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Friday, December 13th,2013).</li>
                	<li>The last suggested pre-Christmas ordering date for UPS 3 Day Select (approx. 3 business days) is AT LEAST FIVE (5) SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Monday, December 16th,2013).</li>
                	<li>The last suggested pre-Christmas ordering date for UPS 2nd Day Air (approx. 2 business days) is AT LEAST THREE (3) SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Wednesday, December 18th, 2013).</li>
                </ul>
                PLEASE BE ADVISED: Regardless of shipping option, Wireless Emporium does not under any circumstance
                expressly warrant or guarantee pre-Christmas delivery. Wireless Emporium does guarantee timely processing
                and fulfillment of all IN-STOCK items upon placement of order (usually within 24-48 hours). Upon fulfillment out
                of our warehouse, it is the obligation of the United States Postal Service for timely delivery of shipment in which
                the above suggested delivery times apply. It is the responsibility of the customer to acknowledge and adhere to
                the above policy before placing their order.
                <br><br>
                <strong>CUSTOM DESIGN PRODUCTS:</strong>
                <br><br>
                Due to the nature of custom cases, our customers who choose this style of cases are advised to place their order on
				or before the cut off days listed below for each of the shipping methods we offer:
                <ul>
                	<li>
                    	The last suggested pre-Christmas ordering date is AT LEAST ELEVEN (11) SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Friday, December 6th, 2013).
                    </li>
                </ul>
                <br><br>
                <strong>SHIPPING TO CANADA:</strong>
                <br><br>
                Packages shipped to Canada are shipped with the United States Postal Service. We offer two shipment methods for orders 
                being shipped to Canada. We order USPS First Class International and USPS International Priority Mail.
                <ul>
                	<li>
                    	The last suggested pre-Christmas ordering date for USPS First Class International Mail (approx. 10 business days) 
                        is AT LEAST TEN (10) SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Monday, December 9th, 2013).
                    </li>
                    <li>
                    	The last suggested pre-Christmas ordering date for USPS International Priority Mail (approx. 5 business days) is 
                        AT LEAST SIX (6) SHIPPING DAYS BEFORE DECEMBER 24TH, 2013 (or Friday, December 13th, 2013).
                    </li>
                </ul>
                ARRIVAL TIMES: Please use this formula to determine when your order will arrive: Shipping Time + Shipping
                Method = Total Delivery Time. Shipment time (the time between ordering a product and that product leaving our
                warehouse) is indicated in each product description on our web site. All in-stock items are usually shipped out of our
                warehouse in Southern California within 1-2 business days upon receipt of order, unless otherwise specified. Orders
                shipped via USPS First Class Mail will arrive anywhere from 4-10 business days after the order has shipped from
                our warehouse(s) and depending on SHIP TO location within the continental United States. Estimated time of arrival
                for orders placed with USPS Priority Mail are anywhere from 2-4 business days after the order has shipped from our
                warehouse(s) and depending on SHIP TO location within the continental Unites States, or unless specified otherwise.
                Estimated time of arrival for orders placed with USPS Express is approximately 1-2 business days after the order
                has shipped from our warehouse(s) and depending on SHIP TO location within the continental Unites States,
                or unless specified otherwise. Please note: our warehouse does not ship on weekends and we do not offer
                Saturday, Sunday or holiday delivery. Please allow an additional 1-2 business days for late Friday, weekend and
                holiday orders.
                <br><br>
                PLEASE BE ADVISED: Regardless of shipping option, Wireless Emporium does not under any circumstance
                expressly warrant or guarantee published delivery times. Wireless Emporium does guarantee timely processing
                and fulfillment of all IN-STOCK items upon placement of order (usually within 1-2 business days). Upon fulfillment out
                of our warehouse(s), it is the obligation of the United States Postal Service for timely delivery of shipment in which the
                above suggested delivery times apply. It is the responsibility of the customer to acknowledge and adhere to the above
                policy before placing their order. The SHIP TO address MUST be a valid deliverable address for the United States
                Postal Service (must be able to receive first class mail). Please provide as complete information as possible to ensure
                delivery accuracy.
                <br><br>
                POLICY: Wireless Emporium offers shipping via USPS first class mail at no charge to our customers, regardless of
                size or weight. Customers may upgrade to USPS Priority Mail which will be charged accordingly at a FLAT-RATE,
                regardless of size or weight. Customers may also choose to upgrade to USPS Express Mail which will be charged
                accordingly at a FLAT-RATE, regardless of size or weight. All orders shipped via USPS Express Mail will require
                signature upon delivery. At this time, Wireless Emporium ONLY ships to the United States, US Protectorates and
                Canada. Additional charges may apply for large-quantity distribution orders and orders that require special delivery
                instructions. In the event such orders are placed, a representative from Wireless Emporium shall immediately contact
                the customer to notify customer of such charges prior to fulfillment. This policy is valid for all orders placed through
                Wireless Emporium within the continental United States of America.
                <br><br>
                SHIPMENT CONFIRMATION/TRACKING: At this time Wireless Emporium does not offer real-time shipping status
                and/or shipping confirmation. Upon shipment of your order you will receive notification that your order has been sent
                in accordance with your selected shipping option. Your credit card is charged when your shipment has been sent.
                <br><br>
                * UPS SHIPPING: For an additional fee, Wireless Emporium orders can be shipped via UPS. Orders shipped via UPS
                will be shipped within 1-2 business days and will be charged based on product weight and ship-to location. Tracking
                numbers & package tracking will be available online at UPS.com. All UPS shipments are insured up to $100.
                <br><br>
                ALL ORDERS ARE SUBJECT TO FURTHER VERIFICATION. Please be advised, if your order requires additional
                information for processing verification, you may receive one or more of the following: (i) an e-mail requesting
                additional information; (ii) a phone call from a Wireless Emporium representative; (iii) or a call from your bank to verify
                your purchase.
            </p>
        </td>
    </tr>
    <% else %>
    <tr>
        <td>
            <p align="justify">
                <strong>CUSTOM DESIGN PRODUCTS:</strong>
                <br>
                Please allow up to <%=gblCustomShippingDate%> business days for production of custom cases prior to shipping time.
            </p>
            <p align="justify">
                <strong>ARRIVAL TIMES:</strong>
                <strong>Please use this formula to determine when your order will arrive: 
                Shipping Time + Shipping Method = Total Delivery Time.</strong> 
                Shipment time (the time between ordering a product and that 
                product leaving our warehouse) is indicated in each product 
                description on our web site. All in-stock items are usually 
                shipped out of our warehouse in Southern California within 
                1-2 business days upon receipt of order, unless otherwise 
                specified. Orders shipped via USPS First Class Mail will 
                arrive anywhere from 4-10 business days <strong>after the 
                order has shipped from our warehouse(s)</strong> and depending 
                on SHIP TO location within the continental United States. 
                Estimated time of arrival for orders placed with USPS Priority 
                Mail are anywhere from 2-4 business days after the order 
                has shipped from our warehouse(s) and depending on SHIP 
                TO location within the continental Unites States, or unless 
                specified otherwise. Estimated time of arrival for orders 
                placed with USPS Express is approximately <strong>1-2 business 
                days after the order has shipped from our warehouse(s)</strong> 
                and depending on SHIP TO location within the continental 
                Unites States, or unless specified otherwise.
                <strong>Please note: our warehouse does not ship on weekends and 
                we do not offer Saturday, Sunday or holiday delivery.</strong>
                Please allow an additional 1-2 business days for late Friday,
                weekend and holiday orders.
            </p>
            <p align="justify">
                <strong>PLEASE BE ADVISED:</strong>
                Regardless of shipping option, Wireless Emporium <strong>does not under 
                any circumstance expressly warrant or guarantee published 
                delivery times</strong>. Wireless Emporium does guarantee 
                timely processing and fulfillment of all IN-STOCK items 
                upon placement of order (usually within 1-2 business days). 
                Upon fulfillment out of our warehouse(s), it is the obligation 
                of the United States Postal Service for timely delivery 
                of shipment in which the above suggested delivery times 
                apply. It is the responsibility of the customer to acknowledge 
                and adhere to the above policy before placing their order. 
                The SHIP TO address MUST be a valid deliverable address 
                for the United States Postal Service (must be able to receive 
                first class mail). Please provide as complete information 
                as possible to ensure delivery accuracy.
            </p>
            <p align="justify">
                <strong>POLICY:</strong>
                Wireless Emporium offers shipping via USPS first class mail at no charge to 
                our customers, regardless of size or weight. Customers may 
                upgrade to USPS Priority Mail which will be charged accordingly 
                at a FLAT-RATE, regardless of size or weight. Customers 
                may also choose to upgrade to USPS Express Mail which will 
                be charged accordingly at a FLAT-RATE, regardless of size 
                or weight.<strong> All orders shipped via USPS Express Mail 
                will require signature upon delivery</strong>. At this time, 
                Wireless Emporium ONLY ships to the United States, 
                US Protectorates and Canada. Additional 
                charges may apply for large-quantity distribution orders 
                and orders that require special delivery instructions. In 
                the event such orders are placed, a representative from 
                Wireless Emporium shall immediately contact the customer 
                to notify customer of such charges prior to fulfillment. 
                This policy is valid for all orders placed through Wireless 
                Emporium within the continental United States of America.
            </p>
            <p align="justify">
                <strong>SHIPMENT CONFIRMATION/TRACKING:</strong> 
                At this time Wireless Emporium does not offer real-time 
                shipping status and/or shipping confirmation. Upon shipment 
                of your order you will receive notification that your order 
                has been sent in accordance with your selected shipping 
                option. Your credit card is charged when your shipment has 
                been sent.
                <br><br>
                <strong>* UPS SHIPPING:</strong>
                For an additional fee, Wireless Emporium orders can be shipped via UPS.
                Orders shipped via UPS will be shipped within 1-2 business days and will
                be charged based on product weight and ship-to location. Tracking numbers
                &amp; package tracking will be available online at UPS.com.
                All UPS shipments are insured up to $100.
            </p>
            <p align="justify">
                <strong>ALL ORDERS ARE SUBJECT TO FURTHER VERIFICATION.</strong>
                Please be advised, if your order requires additional information for processing verification, 
                you may receive one or more of the following: (i) an e-mail 
                requesting additional information; (ii) a phone call from 
                a Wireless Emporium representative; (iii) or a call from 
                your bank to verify your purchase.<br>
            </p>
        </td>
    </tr>
    <% end if %>
</table>
<%if request.querystring("show") <> "text" then%>
<!--#include virtual="/includes/template/bottom.asp"-->
<%else%>
<form><p align="center"><input type="button" value="Close Window" onClick="window.close();"></p></form>
</body>
</html>
<%end if%>
