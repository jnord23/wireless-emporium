<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_301Redirect.asp"-->
<%
	response.buffer = true

	set fs = CreateObject("Scripting.FileSystemObject")
	
	pageName = "brandNew2"
	brandid = 17
	phoneOnly = 1
	dim basePageName : basePageName = "iPhone Accessories"
	dim pageTitle : pageTitle = "iPhone Accessories"
	
	response.Cookies("saveBrandID") = brandID
	response.Cookies("saveModelID") = 0
	response.Cookies("saveItemID") = 0
	
	leftGoogleAd = 1
	googleAds = 1
	lap = 0
	strIPhoneModels = "493,613,968,1120,1267,1412"
	
	'=========================================================================================
'	Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
'		oParam.CompareMode = vbTextCompare
'		oParam.Add "x_brandID", brandID
'		oParam.Add "x_phoneOnly", phoneOnly
'	call redirectURL("b", "", request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
	'=========================================================================================
	
	sql	=	"select	a.linkName, a.modelid, a.modelname" & vbcrlf & _
			"	,	case when a.modelid = 1120 then 'iphone_4_verizon__.jpg' " & vbcrlf & _
			"			when a.modelid = 968 then 'iphone_4_att__.jpg' " & vbcrlf & _
			"			else a.modelimg end modelimg" & vbcrlf & _			
			"	,	a.topModel, a.oldModel, a.international, b.brandname, b.brandimg " & vbcrlf & _
			"	,	cast(case when a.topModel <> 1 and a.oldModel <> 1 and a.international <> 1 then 1 else 0 end as bit) displayModel" & vbcrlf & _
			"	,	case when a.modelid = 1120 then 800 else modelid end orderNum" & vbcrlf & _
			"from 	we_models a with (nolock) join we_brands b with (nolock) " & vbcrlf & _
			"	on 	a.brandid = b.brandid " & vbcrlf & _
			"where 	a.hidelive = 0" & vbcrlf & _
			"	and	a.modelid in (" & strIPhoneModels & ")" & vbcrlf & _
			"order by orderNum desc" & vbcrlf
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1	

	if rs.eof then response.redirect("/") end if
	
	SEtitle = "iPhone 4S Accessories: iPhone Accessories, Covers, Cases & Batteries"
	SEdescription = "Looking for iPhone 4S accessories? Wireless Emporium carries the best iPhone Accessories like Covers, Cases, Batteries, Chargers & more all with Free Shipping!"
	SEkeywords = "iphone accessories, iphone 4S accessories, iphone 3g accessories, iphone 3gs accessories, iphone covers, iphone cases"
	strH1 = "iPhone Accessories"
	strH2 = "iPhone cases and covers"
	
	session("breadcrumb_model") = ""
%>
<!--#include virtual="/includes/template/top_brand.asp"-->
<script src="/includes/js/jquery-1.6.2.min.js"></script>    
<link rel="stylesheet" type="text/css" href="/includes/css/slides.css" />    
<script src="/includes/js/slides/slides.min.jquery.js"></script>
<script>
	window.WEDATA.pageType = 'brand';
	window.WEDATA.pageData = {
		brand: 'Apple',
		brandId: <%= jsStr(brandId) %>
	};

	$(function(){
		$('#iphone-latest').slides({
			preload: true,
			preloadImage: '/images/preloading.gif',
			generatePagination: false,
			prev: 'iphone-arrow-left',
			next: 'iphone-arrow-right'
		});

		$('#iphone-topselling').slides({
			preload: true,
			preloadImage: '/images/preloading.gif',
			generatePagination: false,
			prev: 'iphone-arrow-left',
			next: 'iphone-arrow-right'
		});		
	});
</script>   
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
	<tr><td class="breadcrumbFinal"><a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;iPhone Accessories</td></tr>
    <tr>
    	<td width="100%" style="padding:15px 0px 15px 5px;">
			<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
            	<tr>
                	<td width="456" align="left" valign="top"><img src="/images/brands/iphone/iphone_logo.png" border="0" width="263" title="iPhone accessories, iPhone cases and covers" alt="iPhone accessories, iPhone cases and covers" /></td>
                    <td rowspan="3" width="287" valign="middle"><img src="/images/brands/iphone/iphone_hero.jpg" border="0" width="287" title="iPhone accessories, iPhone cases and covers" alt="iPhone accessories, iPhone cases and covers" /></td>
                </tr>
            	<tr>
                	<td width="100%" align="left" valign="top" style="padding:10px 0px 10px 0px;">
                    	<div style="font-size:20px; color:#343434; font-weight:bold;">
                        	<h1 style="font-size:20px; color:#343434; font-weight:bold;"><%=strH1%></h1>, 
                            <h2 style="font-size:20px; color:#343434; font-weight:bold;"><%=strH2%></h2>.
						</div>
                    	<div style="font-size:13px; color:#666666; line-height:150%;">
	                        Get top of the line accessories for your Apple iPhone. We offer quality cell phone accessories, such as premium leather cases and spare batteries, for the lowest prices online.
                        </div>
                    </td>
                </tr>
            	<tr>
                	<td width="100%" align="left" valign="top" style="padding-top:10px;"><img src="/images/brands/iphone/select_button.png" border="0" width="265" title="Select your iPhone below" alt="Select your iPhone below" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td width="100%">
			<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
            	<tr>
                <%
				a = 0
				do until rs.eof
					a = a + 1
					linkName = rs("linkName")
					modelid = rs("modelid")
					modelimg = rs("modelimg")
					brandName = rs("brandName")
					modelName = rs("modelName")
					altTag = replace(rs("modelName"),"/"," / ") & " accessories"
					styleBorder = ""
					if a < 3 then styleBorder = "border-right:1px solid #ccc;"
					
'					if isnull(linkName) then
						useLink = formatSEO(brandName) & "-" & formatSEO(modelName)
'					else
'						useLink = formatSEO(linkName)
'					end if
					%>
                    <td width="139" align="center" valign="top" style="padding:5px; border-bottom:1px solid #ccc; <%=styleBorder%>">
                    	<div><a href="/accessories-for-<%=useLink%>.asp" title="<%=altTag%>"><img src="/productPics/models/thumbs/<%=modelimg%>" border="0" alt="<%=altTag%>" title="<%=altTag%>" /></a></div>
                        <div style="padding-top:10px;">
                        	<a href="/accessories-for-<%=useLink%>.asp" title="<%=altTag%>">
                            <%if fs.FileExists(server.MapPath("/images/brands/iphone/btn_" & modelid & ".png")) then%>
	                            <img src="/images/brands/iphone/btn_<%=modelid%>.png" border="0" alt="<%=altTag%>" title="<%=altTag%>" />
                            <%else%>
                            	<span style="font-size:16px; font-weight:bold; color:#333;"><%=modelName & " Accessories"%></span>
                            <%end if%>
							</a>
						</div>
					</td>
                    <%
					if a = 3 then 
						response.write "</tr><tr>"
						a = 0
					end if
					rs.movenext
				loop
				%>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <%
	'==== get 3 latest items per each models, 12 total
'	sql = 	"select	top 12 b.modelid, b.modelName, i.typeid, i.itemid, i.itempic, i.itemdesc, i.price_retail, i.price_our" & vbcrlf & _
'			"from	we_models b with (nolock) cross apply" & vbcrlf & _
'			"		(	select	top 3 a.typeid, max(a.itemid) itemid" & vbcrlf & _
'			"			from	we_items a with (nolock)" & vbcrlf & _
'			"			where	b.modelid = a.modelid" & vbcrlf & _
'			"				and	a.hidelive = 0" & vbcrlf & _
'			"				and	a.inv_qty <> 0" & vbcrlf & _
'			"			group by a.typeid" & vbcrlf & _
'			"			order by itemid desc	) x join we_items i" & vbcrlf & _
'			"	on	x.itemid = i.itemid" & vbcrlf & _
'			"where	b.modelid in (" & strIPhoneModels & ")" & vbcrlf & _
'			"order by b.modelid desc, i.itemid desc" & vbcrlf
	sql	=	"select	top 12 x.typeid, x.itemid, x.itempic, x.itemdesc, x.price_retail, x.price_our" & vbcrlf & _
			"from	we_types b with (nolock) cross apply" & vbcrlf & _
			"		(	select	top 1 a.typeid, a.itemid, a.itempic, a.itemdesc, a.price_retail, a.price_our" & vbcrlf & _
			"			from	we_items a with (nolock)" & vbcrlf & _
			"			where	a.typeid = b.typeid" & vbcrlf & _
			"				and	a.hidelive = 0" & vbcrlf & _
			"				and	a.inv_qty <> 0" & vbcrlf & _
			"				and	a.partNumber not like 'WCD-%'" & vbcrlf & _
			"				and	a.modelid in (" & strIPhoneModels & ")" & vbcrlf & _
			"				and	a.itempic is not null and a.itempic <> ''" & vbcrlf & _
			"			order by a.itemid desc	) x" & vbcrlf & _
			"order by 1, 2 desc" & vbcrlf
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1	
	%>
    <tr>
    	<td width="100%" class="iphone-header-box" align="left">
			&nbsp;&nbsp;<h2 style="font-size:18px; font-weight:bold; color:#fff;">Latest iPhone Accessories</h2>
		</td>
	</tr>
    <tr>
        <td align="left" width="100%">
            <div style="width:748px; height:230px; position:relative;">
                <div id="iphone-latest">
                    <div class="iphone-arrow-left" style="margin-top:50px;"></div>
                    <div style="float:left; width:678px; height:220px; padding:5px;">                        
                        <div class="slides_container">
                            <div class="slide">
							<%
							a = 0
							do until rs.eof
								a = a + 1
								itemid = rs("itemid")
								altText = rs("itemdesc")
								itemdesc = rs("itemDesc")
								if len(itemdesc) > 63 then itemdesc = left(itemdesc, 60) & "..."
								price_retail = formatcurrency(rs("price_retail"),2)
								price_our = formatcurrency(rs("price_our"),2)
								productLink = "/p-" & itemid & "-" & formatSEO(itemdesc) & ".asp"
								itempic = rs("itempic")
								%>
                                <div class="item" align="center">
                                    <div style="width:149px; padding-top:2px;" align="center">
                                    	<a href="<%=productLink%>" title="<%=altText%>"><div style="background: url(/productpics/thumb/<%=itemPic%>) 0px -0px no-repeat; width:100px; height:100px;" title="<%=altText%>"></div></a>
                                    </div>
                                    <div style="width:129px; padding:5px 10px 0px 10px;" align="left"><a href="<%=productLink%>" class="cellphone-link" title="<%=altText%>"><h3 style="font-size:12px;"><%=itemdesc%></h3></a></div>
                                    <div style="width:129px; padding:5px 10px 0px 10px;" align="left" class="index-price-retail">Was <del><%=price_retail%></del></div>
                                    <div style="width:129px; padding:0px 10px 0px 10px;" align="left" class="index-price-our">Our Price <%=price_our%></div>
                                    <div style="width:129px; padding:0px 10px 0px 10px;" align="left"><b>Save <%=formatPercent((price_Retail - price_Our) / price_Retail,0)%></b></div>
                                </div>
                                <%
								if ((a mod 4) = 0) and (a < 12) then
									response.write "</div>" & vbcrlf & "<div class=""slide"">"
								end if
								rs.movenext
							loop
								%>
                            </div>
                        </div>
                    </div>
                    <div class="iphone-arrow-right" style="margin-top:50px;"></div>
                </div>
            </div>
        
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <%
'	sql = 	"select	top 12 b.modelid, b.modelName, x.itemid, x.itemdesc, x.price_retail, x.price_our, x.itempic, x.numberOfSales" & vbcrlf & _
'			"from	we_models b with (nolock) cross apply" & vbcrlf & _
'			"		(	select	top 3 a.itemid, a.itemdesc, a.price_retail, a.price_our, a.itempic, a.numberOfSales" & vbcrlf & _
'			"			from	we_items a with (nolock)" & vbcrlf & _
'			"			where	b.modelid = a.modelid" & vbcrlf & _
'			"				and	a.hidelive = 0" & vbcrlf & _
'			"				and	a.inv_qty <> 0" & vbcrlf & _
'			"			order by a.numberOfSales desc	) x" & vbcrlf & _
'			"where	b.modelid in (" & strIPhoneModels & ")" & vbcrlf & _
'			"order by b.modelid desc, x.numberOfSales desc" & vbcrlf
	sql	=	"select	top 12 x.itemid, x.itemdesc, x.price_retail, x.price_our, x.itempic, x.numberOfSales" & vbcrlf & _
			"from	we_types b with (nolock) cross apply" & vbcrlf & _
			"		(	select	top 1 a.itemid, a.itemdesc, a.price_retail, a.price_our, a.itempic, a.numberOfSales" & vbcrlf & _
			"			from	we_items a with (nolock)" & vbcrlf & _
			"			where	b.typeid = a.typeid" & vbcrlf & _
			"				and	a.hidelive = 0" & vbcrlf & _
			"				and	a.inv_qty <> 0" & vbcrlf & _
			"				and	a.modelid in (" & strIPhoneModels & ")" & vbcrlf & _
			"			order by a.numberOfSales desc	) x" & vbcrlf & _
			"order by x.numberOfSales desc" & vbcrlf
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1	
	%>
    <tr>
    	<td width="100%" class="iphone-header-box" style="font-size:18px; font-weight:bold; color:#fff;" align="left">
			&nbsp;&nbsp;<h2 style="font-size:18px; font-weight:bold; color:#fff;">Top Selling iPhone Accessories</h2>
		</td>
	</tr>
    <tr>
        <td align="left" width="100%">
            <div style="width:748px; height:230px; position:relative;">
                <div id="iphone-topselling">
                    <div class="iphone-arrow-left" style="margin-top:50px;"></div>
                    <div style="float:left; width:678px; height:220px; padding:5px;">                        
                        <div class="slides_container">
                            <div class="slide">
							<%
							a = 0
							do until rs.eof
								a = a + 1							
								itemid = rs("itemid")
								altText = rs("itemdesc")
								itemdesc = rs("itemDesc")
								if len(itemdesc) > 63 then itemdesc = left(itemdesc, 60) & "..."								
								price_retail = formatcurrency(rs("price_retail"),2)
								price_our = formatcurrency(rs("price_our"),2)
								productLink = "/p-" & itemid & "-" & formatSEO(itemdesc) & ".asp"
								itempic = rs("itempic")
								%>
                                <div class="item" align="center">
                                    <div style="width:149px; padding-top:2px;" align="center">
                                    	<a href="<%=productLink%>" title="<%=altText%>"><div style="background: url(/productpics/thumb/<%=itemPic%>) 0px -0px no-repeat; width:100px; height:100px;" title="<%=altText%>"></div></a>
                                    </div>
                                    <div style="width:129px; padding:5px 10px 0px 10px;" align="left"><a href="<%=productLink%>" class="cellphone-link" title="<%=altText%>"><h3 style="font-size:12px;"><%=itemdesc%></h3></a></div>
                                    <div style="width:129px; padding:5px 10px 0px 10px;" align="left" class="index-price-retail">Was <del><%=price_retail%></del></div>
                                    <div style="width:129px; padding:0px 10px 0px 10px;" align="left" class="index-price-our">Our Price <%=price_our%></div>
                                    <div style="width:129px; padding:0px 10px 0px 10px;" align="left"><b>Save <%=formatPercent((price_Retail - price_Our) / price_Retail,0)%></b></div>
                                </div>
                                <%
								if ((a mod 4) = 0) and (a < 12) then
									response.write "</div><div class=""slide"">"
								end if
								rs.movenext
							loop
								%>
                            </div>
                        </div>
                    </div>
                    <div class="iphone-arrow-right" style="margin-top:50px;"></div>
                </div>
            </div>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <%
	'==== get 3 latest items per each models, 12 total
	sql = 	"select	top 4 app_link, app_genre, app_img, app_desc" & vbcrlf & _
			"from	we_apps" & vbcrlf & _
			"order by id" & vbcrlf
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1	
	%>    
    <tr>
    	<td width="100%" class="iphone-header-box" style="font-size:18px; font-weight:bold; color:#fff;" align="left">
			&nbsp;&nbsp;<h2 style="font-size:18px; font-weight:bold; color:#fff;">Essential iPhone Apps</h2>
		</td>
	</tr>
    <tr>
        <td align="left">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                <tr>
				<%
                do until rs.eof
                    %>
                    <td align="left" style="padding:5px;">
                    	<div style="float:left;">
                        	<a target="_blank" href="<%=rs("app_link")%>" title="iPhone Apps: <%=rs("app_desc")%>">
                        		<img src="/images/brands/iphone/<%=rs("app_img")%>" border="0" alt="iPhone Apps: <%=rs("app_desc")%>" title="iPhone Apps: <%=rs("app_desc")%>" />
                            </a>
						</div>
                        <div style="float:left; padding-left:3px;">
                        	<span style="font-size:13px; font-weight:bold;"><%=rs("app_desc")%></span><br />
                            <span style="color:#868686;"><%=rs("app_genre")%></span><br />
                            <a target="_blank" href="<%=rs("app_link")%>" title="iPhone Apps: <%=rs("app_desc")%>" style="color:#1D7EA9;">Download the app</a>
                        </div>
                    </td>
                    <%
                    rs.movenext
                loop
                %>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <%
	sql = 	"select	a.modelid, a.modelname, c.brandid, c.brandName, b.spec_model_img, b.size, b.weight, b.cellular_wireless, b.camera" & vbcrlf & _
			"from	we_models a join we_iphone_specs b" & vbcrlf & _
			"	on	a.modelid = b.modelid join we_brands c" & vbcrlf & _
			"	on	a.brandid = c.brandid" & vbcrlf & _
			"order by b.modelid desc" & vbcrlf
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	%>
    <tr>
    	<td width="100%" class="iphone-header-box" style="font-size:18px; font-weight:bold; color:#fff;" align="left">
			&nbsp;&nbsp;<h2 style="font-size:18px; font-weight:bold; color:#fff;">iPhone Specs Comparison</h2>
		</td>
	</tr>
    <tr>
        <td align="left">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
			<%
            do until rs.eof
				modelid = rs("modelid")
				brandName = rs("brandName")
				modelName = rs("modelName")
				modelImg = rs("spec_model_img")
				altTag = replace(modelName,"/"," / ") & " accessories"
                %>
                <tr>
                    <td align="left" style="padding:3px; border-bottom:1px solid #ccc;">
			            <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                        	<tr>
                            	<td style="border-right:1px solid #ccc; padding:5px; width:200px;" align="center" valign="middle">
									<a href="/T-<%=modelid%>-cell-accessories-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>.asp" title="<%=altTag%>">
                                    	<img src="/images/brands/iphone/<%=modelImg%>" border="0" alt="<%=altTag%>" title="<%=altTag%>" />
                                    </a>
                                </td>
                            	<td style="border-right:1px solid #ccc; padding:5px; width:165px;" align="left" valign="top">
                                	<div style="font-weight:bold; width:100%;" align="center">Size and Weight:</div>
                                    <div style="color:#666666; padding-top:7px; line-height:150%;">
                                    	<%=rs("size")%><br />
                                    	<%=rs("weight")%>
                                    </div>
                                </td>
                            	<td style="border-right:1px solid #ccc; padding:5px; width:180px;" align="left" valign="top">
                                	<div style="font-weight:bold; width:100%;" align="center">Cellular and Wireless:</div>
                                    <div style="color:#666666; padding-top:7px; line-height:150%;">
                                    	<%=rs("cellular_wireless")%>
                                    </div>
                                </td>
                            	<td style="padding:5px; width:180px;" align="left" valign="top">
                                	<div style="font-weight:bold; width:100%;" align="center">Camera & Display:</div>
                                    <div style="color:#666666; padding-top:7px;">
                                    	<%=rs("camera")%>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
				<%
                rs.movenext
            loop
            %>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
    	<td width="100%" class="iphone-header-box" style="font-size:18px; font-weight:bold; color:#fff;" align="left">
			&nbsp;&nbsp;<h3 style="font-size:18px; font-weight:bold; color:#fff;">iPhone Accessories | About:</h3>
		</td>
	</tr>
    <tr>
    	<td width="100%" align="left">
            <table border="0" align="center" cellspacing="0" cellpadding="5" width="100%">
                <tr>
                    <td align="left" width="50%" valign="top" style="padding:5px;">
                        <div style="text-align:justify;">
							From its retina display to the beautiful glass backing, the Apple iPhone is a piece of modern art so edgy, it's been featured as an exhibit in museums. So protect and optimize your iPhone with premium accessories, designed specifically to perfectly complement your Apple device.
                        </div>
                        <br /><br />
						<h3 style="color:#000; font-weight:bold;">Where is the best place to buy Apple iPhone accessories?</h3><br />
                        <div style="text-align:justify;">
							Wireless Emporium is the leading online retailer for all your cellular needs. We offer the widest selection of cell phone accessories for the Apple iPhone, ranging from car chargers to glitzy cases. 
                            All our products are guaranteed to work with your device just as well-if not better than?�the accessories that you would purchase at a high premium price at the Apple store. 
                            What differentiates Wireless Emporium from our competitors is our vast selection of products, heavily-slashed prices and dedicated customer service.
                        </div>
						<br /><br />
						<h3 style="color:#000; font-weight:bold;">Will my iPhone be compatible with these products?</h3><br />
                        <div style="text-align:justify;">
							Absolutely! Our team has rigorously tested all our products to work with every type of Apple phone device. 
                            Whether you have a Verizon iPhone or a 3GS, our selection is guaranteed to work with your specific iPhone.
                        </div>
                    </td>
                    <td align="left" width="50%" valign="top" style="padding:5px;">
						<h3 style="color:#000; font-weight:bold;">Do you sell replacement iPhone batteries?</h3><br />
                        <div style="text-align:justify;">
							Though we try to be as full-service as we can, unfortunately we do not currently offer iPhone battery swapping services or screen repair solutions. 
                            We do offer external battery packs to give your phone that extra boost when you're on the go, and great quality iPhone chargers for that quick charge when you see the red bar.
                        </div>
						<br /><br />                    
						<h3 style="color:#000; font-weight:bold;">What's the best protection for my iPhone?</h3><br />
                        <div style="text-align:justify;">
                            We believe that your iPhone should express your style and personality, which is why we carry a wide selection of phone cases. 
                            Whether you're looking to show off your fashionable side with a bling rhinestone case or if you're interested in the most extensive protection with our heavy-duty covers, 
                            we guarantee that we have just the right case for your iPhone!
                        </div>
                        <br /><br />
						<h3 style="color:#000; font-weight:bold;">What's the difference between the iPhone 4 and the iPhone 4S?</h3><br />
                        <div style="text-align:justify;">
							On the basis of form-factor, the iPhone 4s completely utilizes the same body-type as the iPhone 4 -- but that's where the difference ends. 
                            The 4S is a complete upgrade from its predecessor, boasting a more powerful processor, super-crisp screen, ultra-sensitive camera and more internal memory.
                        </div>
                    </td>
				</tr>
			</table>
		</td>
	</tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<!--#include virtual="/includes/template/bottom_brand.asp"-->
