<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<%
response.buffer = true
dim URL, new_URL
new_URL = ""
URL = request.servervariables("HTTP_X_REWRITE_URL")
URL = replace(URL,"?utm_source=","&utm_source=")
if inStr(URL,"sysobjects") > 0 or inStr(URL,"char(124)") > 0 or inStr(URL,"wireless_user") > 0 then
	server.transfer("404errorFile.asp")
	response.end
end if

if inStr(URL,"amp;") > 0 then
	URL = replace(URL,"amp;","&")
	call PermanentRedirect( URL)
end if

session("errorSQL") = URL

URL = replace(URL,request.servervariables("QUERY_STRING"),"")
URL = replace(replace(replace(replace(replace(replace(URL,".html",""),".asp",""),"/",""),"?",""),chr(34),""),"'","")
				
dim typeID, brandID, modelID
typeID = request.querystring("typeID")
if inStr(typeID,"?") > 0 then typeID = left(typeID,inStr(typeID,"?")-1)
brandID = request.querystring("brandID")
if inStr(brandID,"?") > 0 then brandID = left(brandID,inStr(brandID,"?")-1)
modelID = request.querystring("modelID")
if inStr(modelID,"?") > 0 then modelID = left(modelID,inStr(modelID,"?")-1)

if instr(typeID,"%26") > 0 or instr(typeID,"&") > 0 then
	if instr(typeID,"%26") > 0 then qsData = split(typeID,"%26")
	if instr(typeID,"&") > 0 then qsData = split(typeID,"&")
	for i = 0 to ubound(qsData)
		if i = 0 then
			typeID = cdbl(qsData(i))
		elseif instr(qsData(i),"brandID=") > 0 then
			brandID = cdbl(replace(qsData(i),"brandID=",""))
		elseif instr(qsData(i),"modelID=") > 0 then
			modelID = cdbl(replace(qsData(i),"modelID=",""))
		end if
	next
end if

call fOpenConn()
if typeID <> "" or brandID <> "" or modelID <> "" then
	dim strTypeName, brandName, modelName
	if typeID <> "" then
		SQL = "SELECT typeName FROM WE_types WHERE typeID = '" & typeID & "'"
		session("errorSQL") = SQL
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then strTypeName = RS("TypeName")
		if brandID <> "" then
			SQL = "SELECT brandName FROM WE_brands WHERE brandID = '" & brandID & "'"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			if not RS.eof then
				brandName = RS("brandName")
				if modelID <> "" then
					SQL = "SELECT modelName FROM WE_models WHERE modelID = '" & modelID & "'"
					set RS = Server.CreateObject("ADODB.Recordset")
					RS.open SQL, oConn, 3, 3
					if not RS.eof then modelName = RS("modelName")
					if typeID = 5 then
						new_URL = "hf-sm-" & modelID & "-" & formatSEO(strTypeName) & "-for-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
					else
						new_URL = "sc-" & typeID & "-sb-" & brandID & "-sm-" & modelID & "-" & formatSEO(strTypeName) & "-for-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
					end if
				else
					new_URL = "sc-" & typeID & "-sb-" & brandID & "-" & formatSEO(brandName) & "-" & formatSEO(strTypeName) & ".asp"
				end if
			else
				call PermanentRedirect("/")
			end if
		end if
	elseif brandID <> "" then
		if modelID <> "" then
			if modelID = "940" then
				new_URL = "apple-ipad-accessories.asp"
			else
				SQL = "SELECT A.modelName, B.brandName FROM WE_models A INNER JOIN WE_brands B ON A.brandID = B.brandID WHERE A.modelID = '" & modelID & "'"
				set RS = Server.CreateObject("ADODB.Recordset")
				RS.open SQL, oConn, 3, 3
				if not RS.eof then
					modelName = RS("modelName")
					brandName = RS("brandName")
					new_URL = "T-" & modelID & "-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
				else
					call PermanentRedirect("/")
				end if
			end if
		end if
	end if
else
	SQL = "SELECT * FROM WE_redirect WHERE old_url = '" & URL & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then new_URL = "/" & RS("new_url") & ".asp"
end if
call fCloseConn()

'response.write "<p>new_URL = <a href=""" & new_URL & """>" & new_URL & "</a></p>" & vbcrlf
'response.end

if new_URL <> "" then
	call PermanentRedirect(new_URL)
else
	call PermanentRedirect("/")
end if
%>
