var st_widget = {};


/**
 * ENTER THE VALUE FOR THE PATH TO THE DIRECTORY WHERE YOUR SQUARETRADE SWF FILES ARE HOSTED IN THE VALUE OF THE BELOW STRING
 * If you are going to use an absolute path that such as "http://mydomain.com/mydir" or "https://mydomain.com/mydir",
 * please use "//mydomain.com/mydir" instead as it will use whatever protocol the current page is using an thus avoid security popups
 */
st_widget.resource_dir = "/Squaretrade/Squaretrade1";


if ( st_widget.resource_dir.lastIndexOf("/") != st_widget.resource_dir.length-1)
{
    st_widget.resource_dir = st_widget.resource_dir + "/";
}

st_widget.banID = "squaretradewidget";
st_widget.wizID = "squaretradewizard";
st_widget.minFlashVersion = "8";
st_widget._debug = false;
st_widget.defaults = {};
st_widget.defaults.widgetType = "quote";
st_widget.defaults.bannerStyle = "wide";
st_widget.defaults.buyerCountry = "USA";

st_widget.banners = {};

// bannerStyle = wide
st_widget.banners.wide = {};
st_widget.banners.wide.width = 165;
st_widget.banners.wide.height = 65;
st_widget.banners.wide.back = "#FFFFFF";
st_widget.banners.wide.swf = st_widget.resource_dir + "quoteBanner_165x65.swf";

st_widget.wizards = {};

// widgetType = quote
st_widget.wizards.quote = {};
st_widget.wizards.quote.width = 266;
st_widget.wizards.quote.widthPx = "266px";
st_widget.wizards.quote.height = 451;
st_widget.wizards.quote.heightPx = "451px";
st_widget.wizards.quote.back = "#FFFFFF";
st_widget.wizards.quote.swf = st_widget.resource_dir + "quoteWizardLinksLess.swf";


st_widget.debug = function(s)
{
    if ( st_widget._debug == true)
        alert(s);
}

/**
 * Creates the banner
 */
st_widget.create = function(args)
{
    st_widget.el = document.getElementById(st_widget.banID);

    // debug
    if ( args.debug != undefined && args.debug == true)
        st_widget._debug = true;

    // environment
    if ( args.env != undefined )
        st_widget._env = args.env;

    var params = {};

    // populate defaults
    st_widget.mapProperties(st_widget.defaults, params);

    // populate passed in params
    st_widget.mapProperties(args, params);

    // itemCondition
    if ( params.itemCondition != "New" && params.itemCondition != "Used" && params.itemCondition != "Refurbished")
        params.itemCondition = st_widget.defaults.itemCondition;

    params.resource_dir = st_widget.resource_dir;

    if (st_widget.debug)
    {
        var debugString = "";
        for (attr in params)
        {
            debugString += attr + " = " + params[attr] + "\n";
        }
        st_widget.debug(debugString);
    }

    st_widget.fillProperties(params, st_widget.el);


    var detectFlash = false;

    // set widget props
    st_widget.el.widgetProps = st_widget.wizards[st_widget.el.widgetType];

    // set banner props
    st_widget.el.bannerProps = st_widget.banners[st_widget.el.bannerStyle];

    st_widget.debug("Banner URL: " + st_widget.getBannerURL());
    st_widget.debug("Wizard URL: " + st_widget.getWizardURL());

    var swf = new SWFObject(st_widget.getBannerURL(), st_widget.banID + "_swf",
            st_widget.el.bannerProps.width, st_widget.el.bannerProps.height,
            st_widget.minFlashVersion, st_widget.el.bannerProps.back);

    st_widget.addCommonFlashVars(swf);
    swf.addParam("wmode", "transparent");
    swf.write(st_widget.banID);

    window[st_widget.banID + "_swf"] = document.getElementById(st_widget.banID + "_swf");

}

st_widget.fillProperties = function(from, to)
{
    st_widget._mapProperties({from: from, to: to, overwrite: false});
}

st_widget.mapProperties = function(from, to)
{
    st_widget._mapProperties({from: from, to: to, overwrite: true});
}

st_widget._mapProperties = function(args)
{
    for (var p in args.from)
    {
        if (args.to[p] == undefined || args.overwrite)
            args.to[p] = args.from[p];
    }
}

/**
 * Sets common flash vars and params to the SWFObject swfObj
 * @param swfObj the SWFObject to add the vars and params to
 */
st_widget.addCommonFlashVars = function(swfObj)
{
    swfObj.skipDetect = "false";

    swfObj.addVariable("resource_dir", st_widget.resource_dir);

    if ( st_widget._env != undefined )
        swfObj.addVariable("env", st_widget._env);

    for (attr in st_widget.el)
    {
        value = st_widget.el[attr];
        if (typeof(value) == 'string')
            value = escape(value);
        else if (typeof(value) != 'number')
            continue;
        swfObj.addVariable(attr, value);
    }

    swfObj.addVariable("debug", st_widget._debug);

    swfObj.addParam("allowScriptAccess", "always");
}

/**
 * Shows the wizard. Creates the wizard if it has not already been created
 * @param warrantyPrice the price of the warranty calculated by the banner
 */
st_widget.showWizard = function(warrantyPrice, warrantyTerm, warrantyURL, learnMoreURL)
{
    // create wizard if it is not already there
    if ( st_widget.el.wizard == undefined)
    {
        st_widget.createWizard(warrantyPrice, warrantyTerm, warrantyURL, learnMoreURL);
    }

    var coords = st_widget.getWizardCoords();
    st_widget.el.wizard.style.top = coords.top + "px";
    st_widget.el.wizard.style.left = coords.left + "px";
    st_widget.el.wizard.style.display = 'block';
}

/**
 * Hides the wizard if it has been created
 */
st_widget.hideWizard = function()
{
    if ( st_widget.el.wizard != undefined)
        st_widget.el.wizard.style.display = 'none';
}


/**
 * Gets the top and left coords for the wizard so that it is centered on the page
 * @return an object with members top and left the correspond to the top and
 * left coordinate values of the wizard, respectively
 */
st_widget.getWizardCoords = function()
{
    // get scroll amounts
    var scrolledX, scrolledY;
    if (self.pageYOffset)
    {
        scrolledX = self.pageXOffset;
        scrolledY = self.pageYOffset;
    }
    else if (document.documentElement && document.documentElement.scrollTop)
    {
        scrolledX = document.documentElement.scrollLeft;
        scrolledY = document.documentElement.scrollTop;
    }
    else if (document.body)
    {
        scrolledX = document.body.scrollLeft;
        scrolledY = document.body.scrollTop;
    }

    // Determine the coordinates of the center of the page
    var centerX, centerY;
    if (self.innerHeight)
    {
        centerX = self.innerWidth;
        centerY = self.innerHeight;
    }
    else if (document.documentElement && document.documentElement.clientHeight)
    {
        centerX = document.documentElement.clientWidth;
        centerY = document.documentElement.clientHeight;
    }
    else if (document.body)
    {
        centerX = document.body.clientWidth;
        centerY = document.body.clientHeight;
    }

    var coords = {};
    coords.left = scrolledX + (centerX - st_widget.el.widgetProps.width) / 2;
    coords.top = scrolledY + (centerY - st_widget.el.widgetProps.height) / 2;
    return coords;
}

/**
 * Creates an instance of the warranty wizard
 * @param warrantyPrice the price of the warranty calculated by the banner
 */
st_widget.createWizard = function(warrantyPrice, warrantyTerm, warrantyURL, learnMoreURL)
{
    st_widget.el.wizard = document.createElement("div")
    st_widget.el.wizard.style.width = st_widget.el.widgetProps.widthPx;
    st_widget.el.wizard.style.height = st_widget.el.widgetProps.heightPx;
    st_widget.el.wizard.style.position = "absolute";
    st_widget.el.wizard.style.display = "none";
    st_widget.el.wizard.id = st_widget.wizID;
    st_widget.el.wizard.style.zIndex = 100;
    document.body.appendChild(st_widget.el.wizard);

    var docOnClickOld = document.onclick;
    document.onclick = function(e)
    {
        // event is null in case of IE
        if ( e == null)
            e = window.event;

        // get element that triggered event
        var el = (e.srcElement != null) ? e.srcElement : e.target;

        // call previousely attached handler functions
        if(typeof docOnClickOld == 'function')
        {
            try
            {
                docOnClickOld.apply(this, arguments);
            }
            catch(ignore){}
        }

        // don't hide if triggered by click on banner or wizard
        if ( el.id != st_widget.banID && el.id != st_widget.wizID && el.id != st_widget.banID + "_swf" && el.id != st_widget.wizID + "_swf" )
            st_widget.hideWizard();
    }

    st_widget.debug("Wizard URL: " + st_widget.getWizardURL());
    var swf = new SWFObject(st_widget.getWizardURL(), st_widget.wizID + "_swf",
            st_widget.el.widgetProps.width, st_widget.el.widgetProps.height,
            st_widget.minFlashVersion, st_widget.el.widgetProps.back);

    st_widget.addCommonFlashVars(swf);

    st_widget.debug("Warranty Price: " + warrantyPrice + "\nWarranty Term: " + warrantyTerm + "\nWarranty URL: " + warrantyURL + "\nLearn More URL: " + learnMoreURL);
    if ( warrantyPrice != undefined)
    {
        swf.addVariable("warrantyPrice", warrantyPrice);
    }
    if ( warrantyTerm != undefined)
    {
        swf.addVariable("warrantyTerm", warrantyTerm);
    }
    if ( warrantyURL != undefined)
    {
        swf.addVariable("warrantyURL", escape(warrantyURL));
    }
    if ( learnMoreURL != undefined)
    {
        swf.addVariable("learnMoreURL", escape(learnMoreURL));
    }
    swf.addParam('wmode', 'transparent');
    swf.write(st_widget.wizID);

    window[st_widget.wizID + "_swf"] = document.getElementById(st_widget.wizID + "_swf");
}

/**
 * gets the banner url with the proper url params appended
 */
st_widget.getBannerURL = function()
{
    return st_widget.el.bannerProps.swf;
}

/**
 * gets the wizard url with the proper url params appended
 */
st_widget.getWizardURL = function()
{
    return st_widget.el.widgetProps.swf;
}


st_widget.openWin = function (file,w,h,scr,menu,stat,tool,name)
{
	st_widget.popup = window.open(file,name,"scrollbars="+scr+",resizable=yes,menubar="+menu+",status="+stat+",toolbar="+tool+",width="+w+",height="+h+",screenx=0,screeny=0,top=0,left=0");
    if( !st_widget.popup )
        return false;
    else st_widget.popup.focus();
    return true;
}


/**
 * SWFObject v1.5: Flash Player detection and embed - http://blog.deconcept.com/swfobject/
 *
 * SWFObject is (c) 2007 Geoff Stearns and is released under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 *
 */
if(typeof deconcept=="undefined"){var deconcept=new Object();}if(typeof deconcept.util=="undefined"){deconcept.util=new Object();}if(typeof deconcept.SWFObjectUtil=="undefined"){deconcept.SWFObjectUtil=new Object();}deconcept.SWFObject=function(_1,id,w,h,_5,c,_7,_8,_9,_a){if(!document.getElementById){return;}this.DETECT_KEY=_a?_a:"detectflash";this.skipDetect=deconcept.util.getRequestParameter(this.DETECT_KEY);this.params=new Object();this.variables=new Object();this.attributes=new Array();if(_1){this.setAttribute("swf",_1);}if(id){this.setAttribute("id",id);}if(w){this.setAttribute("width",w);}if(h){this.setAttribute("height",h);}if(_5){this.setAttribute("version",new deconcept.PlayerVersion(_5.toString().split(".")));}this.installedVer=deconcept.SWFObjectUtil.getPlayerVersion();if(!window.opera&&document.all&&this.installedVer.major>7){deconcept.SWFObject.doPrepUnload=true;}if(c){this.addParam("bgcolor",c);}var q=_7?_7:"high";this.addParam("quality",q);this.setAttribute("useExpressInstall",false);this.setAttribute("doExpressInstall",false);var _c=(_8)?_8:window.location;this.setAttribute("xiRedirectUrl",_c);this.setAttribute("redirectUrl","");if(_9){this.setAttribute("redirectUrl",_9);}};deconcept.SWFObject.prototype={useExpressInstall:function(_d){this.xiSWFPath=!_d?"expressinstall.swf":_d;this.setAttribute("useExpressInstall",true);},setAttribute:function(_e,_f){this.attributes[_e]=_f;},getAttribute:function(_10){return this.attributes[_10];},addParam:function(_11,_12){this.params[_11]=_12;},getParams:function(){return this.params;},addVariable:function(_13,_14){this.variables[_13]=_14;},getVariable:function(_15){return this.variables[_15];},getVariables:function(){return this.variables;},getVariablePairs:function(){var _16=new Array();var key;var _18=this.getVariables();for(key in _18){_16[_16.length]=key+"="+_18[key];}return _16;},getSWFHTML:function(){var _19="";if(navigator.plugins&&navigator.mimeTypes&&navigator.mimeTypes.length){if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","PlugIn");this.setAttribute("swf",this.xiSWFPath);}_19="<embed type=\"application/x-shockwave-flash\" src=\""+this.getAttribute("swf")+"\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\" style=\""+this.getAttribute("style")+"\"";_19+=" id=\""+this.getAttribute("id")+"\" name=\""+this.getAttribute("id")+"\" ";var _1a=this.getParams();for(var key in _1a){_19+=[key]+"=\""+_1a[key]+"\" ";}var _1c=this.getVariablePairs().join("&");if(_1c.length>0){_19+="flashvars=\""+_1c+"\"";}_19+="/>";}else{if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","ActiveX");this.setAttribute("swf",this.xiSWFPath);}_19="<object id=\""+this.getAttribute("id")+"\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\" style=\""+this.getAttribute("style")+"\">";_19+="<param name=\"movie\" value=\""+this.getAttribute("swf")+"\" />";var _1d=this.getParams();for(var key in _1d){_19+="<param name=\""+key+"\" value=\""+_1d[key]+"\" />";}var _1f=this.getVariablePairs().join("&");if(_1f.length>0){_19+="<param name=\"flashvars\" value=\""+_1f+"\" />";}_19+="</object>";}return _19;},write:function(_20){if(this.getAttribute("useExpressInstall")){var _21=new deconcept.PlayerVersion([6,0,65]);if(this.installedVer.versionIsValid(_21)&&!this.installedVer.versionIsValid(this.getAttribute("version"))){this.setAttribute("doExpressInstall",true);this.addVariable("MMredirectURL",escape(this.getAttribute("xiRedirectUrl")));document.title=document.title.slice(0,47)+" - Flash Player Installation";this.addVariable("MMdoctitle",document.title);}}if(this.skipDetect||this.getAttribute("doExpressInstall")||this.installedVer.versionIsValid(this.getAttribute("version"))){var n=(typeof _20=="string")?document.getElementById(_20):_20;n.innerHTML=this.getSWFHTML();return true;}else{if(this.getAttribute("redirectUrl")!=""){document.location.replace(this.getAttribute("redirectUrl"));}}return false;}};deconcept.SWFObjectUtil.getPlayerVersion=function(){var _23=new deconcept.PlayerVersion([0,0,0]);if(navigator.plugins&&navigator.mimeTypes.length){var x=navigator.plugins["Shockwave Flash"];if(x&&x.description){_23=new deconcept.PlayerVersion(x.description.replace(/([a-zA-Z]|\s)+/,"").replace(/(\s+r|\s+b[0-9]+)/,".").split("."));}}else{if(navigator.userAgent&&navigator.userAgent.indexOf("Windows CE")>=0){var axo=1;var _26=3;while(axo){try{_26++;axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash."+_26);_23=new deconcept.PlayerVersion([_26,0,0]);}catch(e){axo=null;}}}else{try{var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");}catch(e){try{var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");_23=new deconcept.PlayerVersion([6,0,21]);axo.AllowScriptAccess="always";}catch(e){if(_23.major==6){return _23;}}try{axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash");}catch(e){}}if(axo!=null){_23=new deconcept.PlayerVersion(axo.GetVariable("$version").split(" ")[1].split(","));}}}return _23;};deconcept.PlayerVersion=function(_29){this.major=_29[0]!=null?parseInt(_29[0]):0;this.minor=_29[1]!=null?parseInt(_29[1]):0;this.rev=_29[2]!=null?parseInt(_29[2]):0;};deconcept.PlayerVersion.prototype.versionIsValid=function(fv){if(this.major<fv.major){return false;}if(this.major>fv.major){return true;}if(this.minor<fv.minor){return false;}if(this.minor>fv.minor){return true;}if(this.rev<fv.rev){return false;}return true;};deconcept.util={getRequestParameter:function(_2b){var q=document.location.search||document.location.hash;if(_2b==null){return q;}if(q){var _2d=q.substring(1).split("&");for(var i=0;i<_2d.length;i++){if(_2d[i].substring(0,_2d[i].indexOf("="))==_2b){return _2d[i].substring((_2d[i].indexOf("=")+1));}}}return "";}};deconcept.SWFObjectUtil.cleanupSWFs=function(){var _2f=document.getElementsByTagName("OBJECT");for(var i=_2f.length-1;i>=0;i--){_2f[i].style.display="none";for(var x in _2f[i]){if(typeof _2f[i][x]=="function"){_2f[i][x]=function(){};}}}};if(deconcept.SWFObject.doPrepUnload){if(!deconcept.unloadSet){deconcept.SWFObjectUtil.prepUnload=function(){__flash_unloadHandler=function(){};__flash_savedUnloadHandler=function(){};window.attachEvent("onunload",deconcept.SWFObjectUtil.cleanupSWFs);};window.attachEvent("onbeforeunload",deconcept.SWFObjectUtil.prepUnload);deconcept.unloadSet=true;}}if(!document.getElementById&&document.all){document.getElementById=function(id){return document.all[id];};}var getQueryParamValue=deconcept.util.getRequestParameter;var FlashObject=deconcept.SWFObject;var SWFObject=deconcept.SWFObject;
