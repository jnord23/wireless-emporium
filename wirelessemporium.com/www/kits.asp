<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_functions.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
response.buffer = true
dim productListingPage
productListingPage = 1

call fOpenConn()
SQL = "SELECT A.*, B.modelName, B.modelImg, B.excludePouches, B.includeNFL, B.includeExtraItem, C.brandID, C.brandName, D.typeName"
SQL = SQL & " FROM ((we_items A"
SQL = SQL & " INNER JOIN we_models B ON A.modelID = B.modelID)"
SQL = SQL & " INNER JOIN we_brands C ON A.brandid = C.brandid)"
SQL = SQL & " INNER JOIN we_types D ON A.typeid = D.typeid"
SQL = SQL & " WHERE A.ItemKit_NEW IS NOT NULL AND ItemKit_NEW <> ''"
SQL = SQL & " AND A.hideLive = 0"
SQL = SQL & " AND A.inv_qty <> 0"
SQL = SQL & " AND A.price_Our > 0"
SQL = SQL & " ORDER BY A.flag1 DESC"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3

if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
end if

dim brandName, brandID, categoryName, modelName, modelImg, excludePouches, includeNFL, includeExtraItem
brandName = RS("brandName")
brandID = RS("brandID")
categoryName = RS("typeName")
modelName = RS("modelName")
modelImg = RS("modelImg")

dim SEtitle, SEdescription, SEkeywords, topText
SQL = "SELECT title, description, keywords FROM we_MetaTags WHERE brandID = '" & brandID & "' AND modelid = '" & modelid & "' AND typeID = '" & categoryid & "'"
dim RSmeta
set RSmeta = Server.CreateObject("ADODB.Recordset")
RSmeta.open SQL, oConn, 3, 3
if not RSmeta.eof then
	SEtitle = RSmeta("title")
	SEdescription = RSmeta("description")
	SEkeywords = RSmeta("keywords")
end if

if isNull(SEtitle) or SEtitle = "" then SEtitle = brandName & " " & modelName & " " & nameSEO(categoryName) & " � " & brandName & " " & modelName & " Cell Phone Accessories"
if isNull(SEdescription) or SEdescription = "" then SEdescription = brandName & " " & modelName & " " & nameSEO(categoryName) & " - WirelessEmporium is the largest store online for " & brandName & " " & modelName & " " & nameSEO(categoryName) & " & other " & brandName & " " & modelName & " Cell Phone Accessories at the lowest prices."
if isNull(SEkeywords) or SEkeywords = "" then SEkeywords = brandName & " " & modelName & " Accessories, " & brandName & " " & modelName & " Cell Phone Accessories, " & brandName & " " & modelName & " " & nameSEO(categoryName) & ", " & brandName & " " & modelName & " Cell Phone " & nameSEO(categoryName)
if topText = "" then
	topText = "WirelessEmporium is the largest store online for " & brandName & " " & modelName & " Cell Phone " & nameSEO(categoryName) & " & other " & brandName & " " & modelName & " Cell Phone Accessories at the lowest prices.</p>"
	topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">The best in " & brandName & " " & modelName & " " & nameSEO(categoryName) & " at prices up to 80% off retail. Couple this with our WirelessEmporium.com Happy-Shopper 30-day Guarantee and fast FREE SHIPPING, and you realize there's no better place to find your " & brandName & " " & modelName & " " & nameSEO(categoryName) & "!"
end if

dim strBreadcrumb
strBreadcrumb = brandName & " " & modelName & " " & nameSEO(categoryName)
%>
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="/<%=brandSEO(brandID)%>"><%=brandName%> Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="<%=modelLink%>"><%=brandName & " " & modelName%> Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle" width="100%">
            <h1>KITS for All Phone Models</h1>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%">
            <table border="0" cellspacing="7" cellpadding="0">
                <tr>
                    <td align="center" valign="top" width="170">
                        <p class="topText">
                            <img src="/productpics/models/<%=modelImg%>" border="0" alt="<%=brandName & " " & modelName & " " & categoryName%>">
                        </p>
                    </td>
                    <td align="left" valign="top" width="630">
                        <p class="topText" style="margin-top:0px;margin-bottom:6px;"><%=topText%></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="798">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td align="center" valign="top" colspan="7"><img src="/images/line-hori.jpg" width="798" height="5" border="0"></td>
                            </tr>
                            <tr>
                                <%
                                dim altText, aCount, startCount, endCount, RSextra
                                a = 0
                                dim DoNotDisplay, RSkit
                                do until RS.eof
                                    DoNotDisplay = 0
                                    if not isNull(RS("ItemKit_NEW")) then
                                        SQL = "SELECT inv_qty FROM we_Items WHERE itemID IN (" & RS("ItemKit_NEW") & ")"
                                        set RSkit = Server.CreateObject("ADODB.recordset")
                                        RSkit.open SQL, oConn, 3, 3
                                        do until RSkit.eof
                                            if RSkit("inv_qty") < 1 then DoNotDisplay = 1
                                            RSkit.movenext
                                        loop
                                        RSkit.close
                                        set RSkit = nothing
                                    end if
                                    if DoNotDisplay = 0 then
                                        altText = brandName & " " & modelName & " " & singularSEO(categoryName) & " � " & RS("itemDesc")
                                        if categoryid = "14" and modelid = "0" then altText = "Cell Phone Charms & Bling : " & altText
                                        response.write "<td align=""center"" valign=""top"" width=""192"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""192"" height=""100%"">" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""middle"" width=""192"" height=""154""><a href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp""><img src=""/productpics/thumb/" & RS("itemPic") & """ border=""0"" alt=""" & altText & """></a></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""top""><a class=""product-description4"" href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"" title=""" & altText & """>" & RS("itemDesc") & "</a></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""top"" height=""8""><img src=""/images/spacer.gif"" width=""1"" height=""8""></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""bottom""><span class=""boldText"">Our&nbsp;Price:&nbsp;</span><span class=""pricing-orange2"">" & formatCurrency(RS("price_Our")) & "</span><br><span class=""pricing-gray2"">List&nbsp;Price:&nbsp;<s>" & formatCurrency(RS("price_Retail")) & "</s></span><br><span class=""pricing-gray2"">You&nbsp;Save:&nbsp;" & formatCurrency(RS("price_Retail") - RS("price_Our")) & "</span></td></tr>" & vbcrlf
                                        response.write "</table></td>" & vbcrlf
                                        a = a + 1
                                        if a = 4 then
                                            response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""798"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
                                            a = 0
                                        else
                                            response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                                        end if
                                    end if
                                    RS.movenext
                                loop
                                if a = 1 then
                                    response.write "<td width=""192"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""192"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""192"">&nbsp;</td>" & vbcrlf
                                elseif a = 2 then
                                    response.write "<td width=""192"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""192"">&nbsp;</td>" & vbcrlf
                                elseif a = 3 then
                                    response.write "<td width=""192"">&nbsp;</td>" & vbcrlf
                                end if
                                %>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="/<%=categorySEO(categoryID)%>">Cell Phone <%=nameSEO(categoryName)%></a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="/sc-<%=categoryID & "-sb-" & brandID & "-" & formatSEO(brandName) & "-" & formatSEO(categoryName)%>.asp"><%=brandName & " Cell Phone " & nameSEO(categoryName)%></a>&nbsp;&gt;&nbsp;Cell Phone <%=nameSEO(categoryName) & " for " & brandName & " " & modelName%>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
    <tr>
        <td align="center">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" class="bottomText">
                        <div align="justify">
                            <%if bottomText <> "" then%>
                                <p style="margin-top:0px;margin-bottom:6px;"><%=bottomText%></p>
                            <%end if%>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->