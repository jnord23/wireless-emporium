<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
use500 = true
%>
<!--#include virtual="/includes/template/top.asp"-->
<style>
	.reward-text					{font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;}
	.reward-box-heading				{font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; width:128px; height:25px; float:left; font-size:18px; color:#5c5c5c; font-weight:bold; padding:5px 8px 0px 8px; cursor:pointer;}
	.reward-box-heading-selected	{font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; width:128px; height:25px; float:left; font-size:18px; color:#ef6527; font-weight:bold; padding:5px 8px 0px 8px; cursor:pointer;}
	.reward-box1					{font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; width:108px; height:60px; float:left; padding:110px 10px 5px 10px; margin:10px 0px 0px 15px;}
</style>
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;Member Program
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%" style="padding-top:5px;">
        	<img src="/images/500/WEBUCKS-TOP-BANNER.jpg" border="0" width="748" alt="Wireless Emporium Rewards Program" title="Wireless Emporium Rewards Program" />
		</td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%">
			<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
            	<tr>
                	<td style="width:448px; padding:10px; border:2px solid #ccc; border-radius:5px;" align="center" valign="top">
                    	<div class="reward-box-heading-selected" id="id_webucks_0" style="margin-left:5px;" onclick="toggleHeader(0);">EARN WEbucks</div>
                        <div class="reward-box-heading" id="id_webucks_1" style="border-left:1px solid #ccc; border-right:1px solid #ccc;" onclick="toggleHeader(1);">EARN BADGES</div>
                        <div class="reward-box-heading" id="id_webucks_2" onclick="toggleHeader(2);">GET REWARDS</div>
                        <div style="width:430px; height:40px;"></div>
                        <div id="webucks_0">
                            <div class="reward-box1" style="background:url(/images/500/enroll.jpg) no-repeat;" title="Enroll">Enroll<br /><span style="color:#1c5292; font-weight:bold;">+10 WEbucks</span></div>
                            <div class="reward-box1" style="background:url(/images/500/Purchase.jpg) no-repeat;" title="Purchase">Purchase<br /><span style="color:#1c5292; font-weight:bold;">+1 WEbuck per dollar</span></div>
                            <div class="reward-box1" style="background:url(/images/500/Signupforemail.jpg) no-repeat;" title="Signup For Email">Sign up for email<br /><span style="color:#1c5292; font-weight:bold;">+30 WEbucks</span></div>
                            <div class="reward-box1" style="background:url(/images/500/Connectyourfacebookaccount.jpg) no-repeat;" title="Connect Your Facebook Account">Connect your Facebook account<br /><span style="color:#1c5292; font-weight:bold;">+10 WEbucks</span></div>
                            <div class="reward-box1" style="background:url(/images/500/Connectyourtwitteraccount.jpg) no-repeat;" title="Connect Your Twitter Account">Connect your Twitter account<br /><span style="color:#1c5292; font-weight:bold;">+10 WEbucks</span></div>
                            <div class="reward-box1" style="background:url(/images/500/Tweetaproduct.jpg) no-repeat;" title="Tweet A Product">Tweet a product<br /><span style="color:#1c5292; font-weight:bold;">+10 WEbucks</span></div>
                            <div class="reward-box1" style="background:url(/images/500/Followusontwitter.jpg) no-repeat;" title="Follow Us On Twitter">Follow us on Twitter<br /><span style="color:#1c5292; font-weight:bold;">+20 WEbucks</span></div>
                            <div class="reward-box1" style="background:url(/images/500/Emailaproduct.jpg) no-repeat;" title="Email A Product">Email a product<br /><span style="color:#1c5292; font-weight:bold;">+10 WEbucks</span></div>
                            <div class="reward-box1" style="background:url(/images/500/Writeareview.jpg) no-repeat;" title="Write A Review">Write a review<br /><span style="color:#1c5292; font-weight:bold;">+25 WEbucks</span></div>
                            <div class="reward-box1" style="background:url(/images/500/ReferAFriendToPurchase.jpg) no-repeat;" title="Refer A Friend To Purchase">Refer a friend to purchase<br /><span style="color:#1c5292; font-weight:bold;">+50 WEbucks</span></div>
                            <div class="reward-box1">&nbsp;</div>
                            <div class="reward-box1" style="margin-bottom:20px;">&nbsp;</div>
                            <!--<div class="reward-box1" style="background:url(/images/500/Referafriendtosignupforemail.jpg) no-repeat;" title="Refer A Friend To Signup For Email">Refer a friend to sign up for email<br /><span style="color:#1c5292; font-weight:bold;">+50 WEbucks</span></div>-->
                            <!--<div class="reward-box1" style="background:url(/images/500/Openanemail.jpg) no-repeat;" title="Open An Email">Open an email<br /><span style="color:#1c5292; font-weight:bold;">+5 WEbucks</span></div>-->
                        </div>
                        <div id="webucks_1" style="display:none;">
							<div style="margin-top:20px; width:100%; border-top:1px solid #666666; border-bottom:1px solid #999999;"></div>
							<div style="width:100%; margin-top:15px; font-size:13px;" align="left"><span style="font-weight:bold; font-size:13px;">SOCIAL STAR:</span> Unlock this series of badges when you refer 1, 3, 5 and 10 friends!</div>
							<div style="width:100%; margin-top:15px;" align="left">
                            	<img src="/images/500/badges/WE_SOCIAL_1.jpg" border="0" style="margin-left:15px;"/>
                            	<img src="/images/500/badges/WE_SOCIAL_2.jpg" border="0" style="margin-left:15px;"/>
                            	<img src="/images/500/badges/WE_SOCIAL_3.jpg" border="0" style="margin-left:15px;"/>
                            	<img src="/images/500/badges/WE_SOCIAL_4.jpg" border="0" style="margin-left:15px;"/>
                            </div>
							<div style="margin-top:20px; width:100%; border-top:1px solid #666666; border-bottom:1px solid #999999;"></div>
							<div style="width:100%; margin-top:15px; font-size:13px;" align="left"><span style="font-weight:bold; font-size:13px;">SAVVY SHOPPER:</span> Unlock this series of badges on your 2nd, 4th, 8th and 10th order!</div>
							<div style="width:100%; margin-top:15px;" align="left">
                            	<img src="/images/500/badges/SHOPPER_1.jpg" border="0" style="margin-left:15px;"/>
                            	<img src="/images/500/badges/SHOPPER-2.jpg" border="0" style="margin-left:15px;"/>
                            	<img src="/images/500/badges/SHOPPER-3.jpg" border="0" style="margin-left:15px;"/>
                            	<img src="/images/500/badges/SHOPPER-4.jpg" border="0" style="margin-left:15px;"/>                            
                            </div>
							<div style="margin-top:20px; width:100%; border-top:1px solid #666666; border-bottom:1px solid #999999;"></div>
							<div style="width:100%; margin-top:15px; font-size:13px;" align="left"><span style="font-weight:bold; font-size:13px;">EXPLORER:</span> Unlock this series of badges when you complete 5, 10, 20 and 40 actions!</div>
							<div style="width:100%; margin-top:15px;" align="left">
                            	<img src="/images/500/badges/EXPLORER-1.jpg" border="0" height="89" style="margin-left:15px;"/>
                            	<img src="/images/500/badges/EXPLORER-2.jpg" border="0" style="margin-left:15px;"/>
                            	<img src="/images/500/badges/EXPLORER-3.jpg" border="0" style="margin-left:15px;"/>
                            	<img src="/images/500/badges/EXPLORER-4.jpg" border="0" style="margin-left:15px;"/>                            
                            </div>
							<div style="margin-top:20px; width:100%; border-top:1px solid #666666; border-bottom:1px solid #999999;"></div>
							<div style="width:100%; margin-top:15px; font-size:13px;" align="left"><span style="font-weight:bold; font-size:13px;">VIP:</span> Achieve the coveted VIP badges when you reach 1000 WEbucks and access exclusive rewards and perks.</div>
							<div style="width:100%; margin-top:15px;" align="center">
								<img src="/images/500/badges/VIP.jpg" border="0" />
                            </div>
                        </div>
                        <div id="webucks_2" style="display:none;">
                        	<div style="width:100%; margin-top:15px; border:2px solid #ccc; border-radius:10px;" align="center">
                                <div style="width:100%;"><img src="/images/500/we_500_reward.jpg" border="0" /></div>
                            </div>
                        </div>
                        <!-- copied from dash board -->
                        <div>
                        	<div style="color:#666; padding:20px 0px 0px 20px;" align="left">
                                Now you can earn points for being social by joining our Wireless Emporium Rewards Program. It is very easy to enroll, earn and redeem points for special coupons and gift certificates that will help you save.<br /><br />
								There are many ways to earn points, such as by purchasing a product, emailing a product to a friend, following us on Twitter, referring a friend that purchases, writing a product review, asking and answering questions about a product, tweeting and much more.<br />
                                <a id="id_readMore" href="#" onclick="readMore();return false;" style="color:#ef6527; font-weight:bold; text-decoration:underline;">[Read More]</a>
                            </div>
                            <div class="help_content" id="id_helpcontent" style="display:none; padding-top:20px;">
								<style type="text/css">
                                  .help_content	{text-align:left; font-family:arial; font-size: 12px; padding: 10px; line-height: 18px; color: #666;}
                                  .reward-table{ font-family:arial; font-size: 12px; padding: 10px; line-height: 18px; color: #666; margin: 10px 0px; border-collapse: collapse; background-color: #f2f2f2; }
                                  .reward-td{ padding: 8px; border: 1px solid #ccc; `}
                                  .reward-td strong{ color: #666;}*/
                                  strong{ color: #333;}
                                  hr.reward { margin: 15px 0px !important;}
                                  a.question{ color: #333; text-decoration:underline;}
                                  a.back_to_top{ font-size: 11px;  text-decoration:underline;}
                                  div.answer{ padding: 5px 0px; }
                                </style>
                                <strong style="font-size:14px">WE Bucks Loyalty Program FAQs</strong>
                                  <hr>
                                  <a class="question" href="#1">What is WE Bucks Loyalty Program?</a><br />
                                  <a class="question" href="#2">Who is eligible to join?</a><br />
                                  <a class="question" href="#3">What does it cost?</a><br />
                                  <a class="question" href="#4">How do I enroll?</a><br />
                                  <a class="question" href="#5">How do I earn WEBucks?</a><br />
                                  <a class="question" href="#6">Is there a limit to the number of WEBucks I can earn?</a><br />
                                  <a class="question" href="#7">Do my WEBucks expire?</a><br />
                                  <a class="question" href="#8">What can I redeem my WEBucks for?</a><br />
                                  <a class="question" href="#9">How can I view my WEBucks balance?</a><br />
                                  <a class="question" href="#10">How do I redeem WEBucks?</a><br />
                                  <a class="question" href="#11">Am I able to use WEBucks at checkout?</a><br />
                                  <a class="question" href="#12">Is there a place where I can report a problem or make a suggestion?</a><br />
                                  <hr>
                                  <a name="1"></a>
                                  <strong>What is WE Bucks Loyalty Program?</strong>
                                  <div class="answer">
                                    Our loyalty program allows you to earn points for sharing, purchasing online, reviewing and much more. The points can be redeemed for specific rewards such as coupons and gift cards.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="2"></a>
                                  <strong>Who is eligible to join?</strong>
                                  <div class="answer">
                                    All customers are eligible to enroll in the loyalty program.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="3"></a>
                                  <strong>What does it cost?</strong>
                                  <div class="answer">
                                    Enrollment in the program is completely free.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="4"></a>
                                  <strong>How do I enroll?</strong>
                                  <div class="answer">
                                    To enroll, visit our <a href="/member_program">Rewards Page</a> and click on the "Enroll Now" button. You will also have an opportunity to enroll after completing a purchase.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="5"></a>
                                  <strong>How do I earn WEBucks?</strong>
                                  <div class="answer">
                                    You can earn WEbucks in a variety of different ways. Each time you place an online order you accumulate 1 point for each dollar spent on our website. You can also earn points for being social and sharing products with friends. To see the many different ways that points can be earned, check out the Earn WEbucks tab on the Member Page.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="6"></a>
                                  <strong>Is there a limit to the number of WEBucks I can earn?</strong>
                                  <div class="answer">
                                    While some tasks can only be completed once, others, such as tweeting, can be done many times. There are certain limitations on the number of points that can be earn for these actions but don't worry because that limit is reset at the start of each week.
                                    <table style="width:380px" class="reward-table">
                                      <tr>
                                        <td class="reward-td"></td>
                                        <td class="reward-td"><strong>Total # of points that can be earned</strong></td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>Tweet a product</strong></td>
                                        <td class="reward-td">50 points per week</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>Write a review</strong></td>
                                        <td class="reward-td">150 points per week</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>Open an email</strong></td>
                                        <td class="reward-td">25 points per week</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>Send an email</strong></td>
                                        <td class="reward-td">30 points per week</td>
                                      </tr>
                                    </table>
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="7"></a>
                                  <strong>Do my WEBucks expire?</strong>
                                  <div class="answer">
                                    We think that when you earn WEbucks, you should be able to keep them as long as you'd like. That's why, no matter how long you take to redeem them, your WEbucks will be safe and sound in a the world's most secure secret vault. Well maybe not, but they'll be in your account for when you're ready to use them.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="8"></a>
                                  <strong>What can I redeem my WEBucks for?</strong>
                                  <div class="answer">
                                    You can redeem WEbucks for a variety of rewards from exclusive products to discounts and gift cards.
                                    <table style="width:380px" class="reward-table">
                                      <tr>
                                        <td class="reward-td"><strong>50 WEBucks</strong></td>
                                        <td class="reward-td">10% Off Coupon</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>100 WEBucks</strong></td>
                                        <td class="reward-td">15% Off Coupon</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>150 WEBucks</strong></td>
                                        <td class="reward-td">$5 Gift Card</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>300 WEBucks</strong></td>
                                        <td class="reward-td">20% Off Coupon</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>500 WEBucks</strong></td>
                                        <td class="reward-td">25% Off Coupon</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>1000 WEBucks</strong></td>
                                        <td class="reward-td">30% Off Coupon and Free Faceplate*</td>
                                      </tr>
                                    </table>
                                *Free Faceplate reward can be redeemed for any non-OEM faceplate valued at $11.99 or under.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="9"></a>
                                  <strong>How can I view my WEBucks balance?</strong>
                                  <div class="answer">
                                    To view your balance and program activity, login to the member application found here: <a href="/account/account_details">Activity</a> and click on the "Activity" tab. Here you will find a detailed list of all completed activities as well as point totals for each of these activities.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="10"></a>
                                  <strong>How do I redeem WEBucks?</strong>
                                  <div class="answer">
                                    To redeem your points for rewards, login to the member application found here: <a href="/account/login">Rewards</a> and click on the "Rewards" tab. Here you will find a list of all program rewards. There will be a "Redeem" button below any reward that you have enough WEbucks to claim. Click on "Redeem" and instructions for redemption will be provided.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="11"></a>
                                  <strong>Am I able to use WEBucks at checkout?</strong>
                                  <div class="answer">
                                    Upon redeeming a reward, you will be provided with a unique coupon code that can be applied at checkout. For the $5 gift card reward, the entire value of the card must be used at once and cannot be carried over to another purchase.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="12"></a>
                                  <strong>Is there a place where I can report a problem or make a suggestion?</strong>
                                  <div class="answer">
                                    Please contact our Customer Happiness department with any questions. They can be reached at 1-800-305-1106.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
  


                            </div>
                        </div>
                        <!--// copied from dash board -->
                    </td>
                	<td style="width:270px; padding:0px 0px 0px 10px;" align="center" valign="top">
                    	<div style="width:250px; padding:10px; background-color:#484848; border-radius:5px;" align="left">
                        	<div style="width:250px;">
                            	<div class="reward-text" style="color:#fff; font-size:18px; font-weight:bold;" align="left">Members</div>
                            	<div class="reward-text" style="color:#fff; font-size:11px; padding-bottom:10px;" align="left">Check your point balance, redeem a reward or just learn more about the benefits of the program.</div>
                            </div>
                        	<div style="width:250px; padding-bottom:10px;" align="center">
                            	<a href="/account/account_details" style="text-decoration:none;"><img src="/images/500/WEBUCKS-CHECKYOURBALANCE-BUTTON.jpg" border="0" /></a>
                            </div>
                        	<div style="width:250px; border-top:2px solid #454545; border-bottom:1px solid #767373;"></div>
                        	<div style="width:250px;">
                            	<div class="reward-text" style="color:#fff; font-size:18px; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-weight:bold; padding-top:10px;" align="left">Non-Members</div>
                            	<div class="reward-text" style="color:#fff; font-size:11px; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; padding-bottom:10px;" align="left">Just hit the Enroll Now button to start earning points!</div>
                            </div>
                        	<div style="width:250px;" align="center">
                            	<a href="/account/register" style="text-decoration:none;"><img src="/images/500/WEBUCKS-ENROLL-NOW-BUTTON.jpg" border="0" /></a>
                            </div>
                        </div>
                        <div style="width:270px; margin-top:15px;">
                        	<div><img src="/images/500/WE-BUCKS-TOP-MEMBERS.jpg" border="0" /> </div>
                            <div>            
					            <script type="text/javascript">_ffLoyalty.displayLeaderboard("lOiq7KfD1T", {});</script>
							</div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
	</tr>
</table>
<script>
	function readMore() {
		toggle('id_helpcontent');
	}
	
	function toggleHeader(selected) {
		for (i=0; i<=2; i++) {
			document.getElementById('id_webucks_'+i).className = 'reward-box-heading';
			document.getElementById('webucks_'+i).style.display = 'none';
		}
		
		document.getElementById('id_webucks_'+selected).className = 'reward-box-heading-selected';
		document.getElementById('webucks_'+selected).style.display = '';
	}
</script>
<!--#include virtual="/includes/template/bottom.asp"-->
