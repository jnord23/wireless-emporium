<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
response.buffer = true
dim productListingPage
productListingPage = 1

dim pouchid
pouchid = request.querystring("pouchid")
if pouchid = "" or not isNumeric(pouchid) then
	call PermanentRedirect("/")
end if

call fOpenConn()
SQL = "SELECT * FROM we_items WHERE Sports = '" & pouchid & "'"
SQL = SQL & " AND hideLive = 0"
SQL = SQL & " AND inv_qty <> 0"
SQL = SQL & " ORDER BY flag1 DESC"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
end if

select case pouchid
	case 1
		modelImg = "LC-MLB-Header.jpg"
		H1 = "MLB OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
		topText = "Officially licensed Major League Baseball cell phone pouches are here! Choose your favorite Major League Baseball team from our wide selection and carry your cell phone with pride. These premium cases offer superior protection for your phone while delivering a boost to team spirit!"
	case 2
		modelImg = "LC-NCAA-Header.jpg"
		H1 = "NCAA OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
		topText = "Officially licensed NCAA cell phone pouches are here! Choose your favorite NCAA team from our wide selection and carry your cell phone with pride. These premium cases offer superior protection for your phone while delivering a boost to team spirit!"
	case 3
		modelImg = "LC-Disney-Header.jpg"
		H1 = "DISNEY OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
		topText = "Officially licensed Disney pouches are here! Now you can wear your favorite Disney characters proudly and fashionably. These top-quality pouches are made exclusively by MOBO, and come in different colors, styles, and characters. Protect your phone in a fun, creative way with these premium cell phone pouches."
	case 4
		modelImg = "LC-Blingpouch-Header.jpg"
		H1 = "BLING OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
		topText = "Check out our line of sparkly Bling cell phone pouches. You can keep your phone protected while showing off your style. These high quality pouches fit most phones, and they come in many colors and styles to ensure that you can find one that's just right for you. Buy with confidence that you're getting the best value anywhere!"
	case 5
		modelImg = "LC-NFL-Header.jpg"
		H1 = "NFL OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
		topText = "Officially licensed NFL cell phone pouches are here! Choose your favorite NFL team from our wide selection and carry your cell phone with pride. These premium cases offer superior protection for your phone while delivering a boost to team spirit!"
	case 6
		modelImg = "LC-NBA-Header.jpg"
		H1 = "NBA OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
		topText = "Officially licensed NBA cell phone pouches are here! Choose your favorite NBA team from our wide selection and carry your cell phone with pride. These premium cases offer superior protection for your phone while delivering a boost to team spirit!"
end select

dim SEtitle, SEdescription, SEkeywords
SEtitle = H1
SEdescription = topText
SEkeywords = ""
%>
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="/cell-phone-cases.asp">Cell Phone Leather Cases</a>&nbsp;&gt;&nbsp;<%=H1%>
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle" width="100%">
            <h1><%=H1%></h1>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%">
            <table border="0" cellspacing="7" cellpadding="0">
                <tr>
                    <td align="center" valign="top" width="200">
                        <p class="topText">
                            <img src="/images/brandhome/<%=modelImg%>" border="0" width="180" height="109" alt="<%=H1%>">
                        </p>
                    </td>
                    <td align="center" valign="top" width="600">
                        <table border="0" cellspacing="0" cellpadding="5" width="541" class="cate-pro-border">
                            <tr>
                                <td>
                                    <p class="topText"><%=topText%></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="798">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td align="center" valign="top" colspan="7"><img src="/images/line-hori.jpg" width="798" height="5" border="0"></td>
                            </tr>
                            <tr>
                                <%
                                a = 0
                                dim altText, DoNotDisplay, RSkit
                                do until RS.eof
                                    DoNotDisplay = 0
                                    if not isNull(RS("ItemKit_NEW")) then
                                        SQL = "SELECT inv_qty FROM we_Items WHERE itemID IN (" & RS("ItemKit_NEW") & ")"
                                        set RSkit = Server.CreateObject("ADODB.recordset")
                                        RSkit.open SQL, oConn, 0, 1
                                        do until RSkit.eof
                                            if RSkit("inv_qty") < 1 then DoNotDisplay = 1
                                            RSkit.movenext
                                        loop
                                        RSkit.close
                                        set RSkit = nothing
                                    end if
                                    if DoNotDisplay = 0 then
                                        altText = brandName & " " & modelName & " " & singularSEO(categoryName) & " ?" & RS("itemDesc")
                                        if categoryid = "14" and pouchid = "0" then altText = "Cell Phone Charms & Bling : " & altText
                                        response.write "<td align=""center"" valign=""top"" width=""192"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""192"" height=""100%"">" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""middle"" width=""192"" height=""154""><a href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp""><img src=""/productpics/thumb/" & RS("itemPic") & """ border=""0"" alt=""" & altText & """></a></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""top""><a class=""product-description4"" href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"" title=""" & altText & """>" & RS("itemDesc") & "</a></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""top"" height=""8""><img src=""/images/spacer.gif"" width=""1"" height=""8""></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""bottom""><span class=""boldText"">Our&nbsp;Price:&nbsp;</span><span class=""pricing-orange2"">" & formatCurrency(RS("price_Our")) & "</span><br><span class=""pricing-gray2"">List&nbsp;Price:&nbsp;<s>" & formatCurrency(RS("price_Retail")) & "</s></span><br><span class=""pricing-gray2"">You&nbsp;Save:&nbsp;" & formatCurrency(RS("price_Retail") - RS("price_Our")) & "</span></td></tr>" & vbcrlf
                                        response.write "</table></td>" & vbcrlf
                                        a = a + 1
                                        if a = 4 then
                                            response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""798"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
                                            a = 0
                                        else
                                            response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                                        end if
                                    end if
                                    RS.movenext
                                loop
                                if a = 1 then
                                    response.write "<td width=""192"">&nbsp;</td><td width=""192"">&nbsp;</td><td width=""192"">&nbsp;</td>" & vbcrlf
                                elseif a = 2 then
                                    response.write "<td width=""192"">&nbsp;</td><td width=""192"">&nbsp;</td>" & vbcrlf
                                elseif a = 3 then
                                    response.write "<td width=""192"">&nbsp;</td>" & vbcrlf
                                end if
                                %>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->