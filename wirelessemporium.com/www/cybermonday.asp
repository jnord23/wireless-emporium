<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_OnSale.asp"-->
<%

if date > cdate("11/30/2011") then
	call PermanentRedirect("/")
end if

response.buffer = true
dim productListingPage
productListingPage = 1

call fOpenConn()
sql	=	"select	id, promodesc, promolink, imgPath, promoPartNumber, a.itemid, a.brandid, a.modelid, a.subtypeid, a.promoPrice" & vbcrlf & _
		"	,	isnull((select top 1 inv_qty from we_items where partnumber = a.promoPartNumber order by master desc, inv_qty desc), 0) inv_qty" & vbcrlf & _
		"from	we_cybermonday a" & vbcrlf
		


fromDate	=	now

hourDiff	=	24 - hour(now) - 1
hourDiff = right("00" & cstr(hourDiff), 2)

MinDiff		=	60 - minute(now)
if MinDiff = 60 then MinDiff = 0
MinDiff		=	right("00" & cstr(MinDiff), 2)

SecDiff		=	60 - second(now)
if SecDiff = 60 then SecDiff = 0
SecDiff		=	right("00" & cstr(SecDiff), 2)

'response.write hourDiff & "<br>"
'response.write MinDiff & "<br>"
'response.write SecDiff & "<br>"
	
session("errorSQL") = sql				
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

'if RS.eof then
'	call fCloseConn()
'	call PermanentRedirect("/")
'end if

noMobileRedirect = 1
%>
<!--#include virtual="/includes/template/top.asp"-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="/includes/js/cybermonday/jquery.countdown.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
      $(function(){
        $('#counter').countdown({
          image: '/images/cybermonday/digits.png',
          startTime: '00:<%=hourDiff%>:<%=MinDiff%>:<%=SecDiff%>'
        });
      });
    </script>
    <style type="text/css">
      br { clear: both; }
      .cntSeparator {
        font-size: 54px;
        margin: 10px 7px;
        color: #000;
      }
      .desc { margin: 7px 3px; }
      .desc div {
        float: left;
        font-family: Arial;
        width: 70px;
        margin-right: 65px;
        font-size: 13px;
        font-weight: bold;
        color: #000;
      }
    </style>
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="middle" width="100%">
            <img src="/images/cybermonday/DoorBuster-Banner-v2.jpg" border="0" alt="DOORBUSTER DEALS - Save Up To 80% OFF RETAIL" title="DOORBUSTER DEALS - Save Up To 80% OFF RETAIL" width="748" />
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%" style="padding-top:10px;">
			<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">        	
            	<tr>
                	<td align="left" valign="top" style="padding-right:10px;"><img src="/images/cybermonday/HurrySaleEnds-MidBanner.jpg" border="0" /></td>
                    <td align="right" valign="top">
                        <div id="counter"></div>
                        <div class="desc">
                            <div>Days</div>
                            <div>Hours</div>
                            <div>Minutes</div>
                            <div style="margin-right:0px;">Seconds</div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="748">
                <tr>
                    <td style="height:20px;">&nbsp;</td>
                </tr>
                <tr>
                	<td><img src="/images/line-hori.jpg" width="748" height="5" border="0"></td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <%
								do until rs.eof
									promoText = rs("promoDesc")
									strLink = rs("promoLink")
									invQty = rs("inv_qty")
									salePrice = rs("promoPrice")
									%>
									<td align="center" valign="top" width="172" height="100%">
                                    	<table border="0" cellspacing="0" cellpadding="0" width="172" height="100%">
                                            <tr><td align="center" valign="middle" width="172" height="154"><a href="<%=strLink%>" title="<%=promoText%>"><img src="<%=rs("imgPath")%>" border="0" alt="<%=promoText%>" title="<%=promoText%>" /></a></td></tr>
                                            <tr><td align="center" valign="top" height="5"><img src="/images/spacer.gif" width="1" height="5"></td></tr>
										<%
										if invQty > 0 then
										%>
											<tr><td align="center" valign="top" height="35"><a class="cellphone-link" href="<%=strLink%>" title="<%=promoText%>" style="font-style:italic;">Only <%=round(invQty/2, 0)%> Left!</a></td></tr>                                        
                                        <%
										else
										%>
											<tr>
                                            	<td align="center" valign="top" height="35">
                                                    <a class="cellphone-link" href="<%=strLink%>" title="<%=promoText%>" style="font-size:15px;">
                                                    	<b>SALE PRICE:&nbsp;</b><span style="color:#900; font-weight:bold;"><%=formatcurrency(salePrice)%></span>
                                                    </a>
												</td>
											</tr>
                                        <%
										end if
										%>
                                            <tr><td align="center" valign="top" height="8"><img src="/images/spacer.gif" width="1" height="8"></td></tr>
										</table>
									</td>
                                    <%
									a = a + 1
									if a = 4 then
										response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""748"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
										a = 0
									else
										response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""230"" border=""0""></td>" & vbcrlf
									end if
									RS.movenext
                                loop
                                if a = 1 then
                                    response.write "<td width=""172"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""172"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""172"">&nbsp;</td>" & vbcrlf
                                elseif a = 2 then
                                    response.write "<td width=""172"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""172"">&nbsp;</td>" & vbcrlf
                                elseif a = 3 then
                                    response.write "<td width=""172"">&nbsp;</td>" & vbcrlf
                                end if
                                %>
                            </tr>
                        </table>
                    </td>
                </tr>                
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->