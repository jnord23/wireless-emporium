<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_switchChars.asp"-->

<%
ID = request.querystring("releaseNum")
if not isNumeric(cStr(ID)) then
	call PermanentRedirect("/press.asp")
end if

call fOpenConn()
SQL = "SELECT * FROM we_PressReleases WHERE ID='" & ID & "'"
Set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

Set Pdf = Server.CreateObject("Persits.Pdf")
Set Doc = Pdf.CreateDocument
Set Page = Doc.Pages.Add
Set Param = Pdf.CreateParam
Set Font = Doc.Fonts("Times-Roman")

' add logo image
Set Image = Doc.OpenImage(Server.MapPath("/images/press/WElogo.gif"))
Param("x") = 30
Param("y") = 680
Param("ScaleX") = .5
Param("ScaleY") = .5
Page.Canvas.DrawImage Image, Param

' add headlines & body text
strText = "<center><p><b>" & switchChars(RS("Headline")) & "</b>"
if not isNull(RS("Headline2")) and trim(RS("Headline2")) <> "" then strText = strText & "<br><i>" & switchChars(RS("Headline2")) & "</i>"
strText = strText & "</p></center>"
strText = strText & "<div align='justify'><p><b>Orange, CA</b>&nbsp;&#150;&nbsp;" & formatDateTime(RS("ReleaseDate"),1) & "&nbsp;&#150;&nbsp;"
strText = strText & switchChars(RS("BodyText")) & "</p>"
strText = strText & "<p><b>About Wireless Emporium, Inc.</b><br>"
strText = strText & "Established in 2001, Wireless Emporium, Inc. is a recognized leader in the cell phone accessories and unlocked cell phones market supplying over 30,000 manufacturer-direct products to consumers, businesses, education and government institutions through a portfolio of leading E-Commerce web sites."
strText = strText & " Their manufacturer-direct product line includes <a href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, chargers, batteries, cases and faceplates, screen protectors, bluetooth headsets, data connectivity products and unlocked cell phones at discount prices."
strText = strText & " The company backs every order with a 100% customer satisfaction guarantee, extended manufacturer warranties and free first class shipping, policies which have set them apart from other online retailers and helped earn over 1 million loyal customers in the US and Canada.</p>"
strText = strText & "<p><b>Contact:</b> Media Relations</p></div>"
strText = strText & "<center><p>###</p></center>"
Param("x") = 60
Param("y") = 650
Param("width") = 492
Param("size") = 11
Param("HTML") = true
Page.Canvas.DrawText strText, Param, Font

filename = "WE_Press_Release_" & replace(RS("releasedate"),"/","-") & ".pdf"
Doc.SaveHttp "attachment;filename=" & filename
call CloseConn()
%>
