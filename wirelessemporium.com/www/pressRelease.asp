<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_functions.asp"-->
<table border="0" cellpadding="0" cellspacing="0" width="80%" align="center">
	<tr>
		<td>
			<p><img src="/images/press/WElogo.gif" alt="Cell Phone Accessories" hspace="10" vspace="10" border="0"></p>
			<div align="justify">
			<%
			call fOpenConn()
			releaseNum = request.querystring("releaseNum")
			if not isNumeric(releaseNum) then
				call PermanentRedirect("/")
			end if
			SQL = "SELECT * FROM we_PressReleases WHERE ID='" & releaseNum & "'"
			Set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			if RS.eof then
				%>
				<p align="center">Press Release Not Found!</p>
				<%
			else
				%>
				<p align="center">
					<strong><%=RS("Headline")%></strong>
					<%
					if not isNull(RS("Headline2")) and trim(RS("Headline2")) <> "" then response.write "<br><em>" & RS("Headline2") & "</em>"
					%>
				</p>
				<p>
					<strong>Orange, CA</strong>&nbsp;&#150;&nbsp;<%=formatDateTime(RS("ReleaseDate"),1)%>&nbsp;&#150;&nbsp;<%=replace(RS("BodyText"),vbCrLf,"<br>")%>
				</p>
				<p>
					<strong>About Wireless Emporium, Inc.</strong>
					<br>
					Established in 2001, Wireless Emporium, Inc. is a recognized leader in the cell phone accessories and unlocked cell phones market supplying over 30,000 manufacturer-direct products to consumers, businesses, education and government institutions through a portfolio of leading E-Commerce web sites.
					Their manufacturer-direct product line includes <a href="http://www.wirelessemporium.com/phone-covers-faceplates">cell phone covers</a>, chargers, batteries, cases and faceplates, screen protectors, bluetooth headsets, data connectivity products and unlocked cell phones at discount prices.
					The company backs every order with a 100% customer satisfaction guarantee, extended manufacturer warranties and free first class shipping, policies which have set them apart from other online retailers and helped earn over 1 million loyal customers in the US and Canada.
				</p>
				<p>
					<strong>Contact:</strong>
					<br>
					Media Relations<br>Wireless Emporium, Inc.
				</p>
				<p align="center">###</p>
				<%
			end if
			call fCloseConn()
			%>
			<p align="center"><a href="javascript:window.close();">Close Window</a></p>
			</div>
		</td>
	</tr>
</table>

</body>
</html>
