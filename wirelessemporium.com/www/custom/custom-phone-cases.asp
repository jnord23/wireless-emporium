<%

	pageName = "customphonecase"
	
	response.buffer = true
	noleftnav = 1
	nocommentbox = true
	mobileAccess = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<%
	'sql = ""
	'session("errorSQL") = sql
	'set	RS = oConn.execute(SQL)
%>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,800' rel='stylesheet' type='text/css'>
<link href="/includes/css/tt.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
    var turnToConfig = {
      siteKey: "W1a86usaEcbMZXlsite",
      setupType: "overlay"
    };
    (function() {
        var tt = document.createElement('script'); tt.type = 'text/javascript'; tt.async = true;
        tt.src = document.location.protocol + "//static.www.turnto.com/traServer3/trajs/" + turnToConfig.siteKey + "/tra.js";
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tt, s);
    })();
</script>

<style>
#cw {
	/*Prevent the borders from blowing out*/
	width:958px;
}
#cw #header, #cw #first, #cw #second, #cw #third, #cw #fourth {
	position:relative;
	/*border:1px solid green;*/
}
#cw h2, #cw h3 {
	font-weight:800;
	font-family: 'Open Sans', sans-serif;
	/*font-size:25px;*/
	color:#333333;
	text-transform:uppercase;
	display:block;
	text-align:center;
	white-space:nowrap;
}
#cw h2 {
	font-size:2.5em;
	line-height:1.5em;
	margin-top:0.0em;
	margin-bottom:0.0em;
}
#cw h3 {
	font-size:1.2em;
	line-height:1.2em;
}
#cw p {
	margin-left:auto;
	margin-right:auto;
	line-height:1.75em;
	color:#666666;
	font-family:Verdana, Geneva, sans-serif;
}
#cw #header {
	height:110px;
}
#cw #headerBanner {
	position:absolute;
	left:13px;
	top:5px;
	width:530px;
	height:175px;
}
#cw #headerBanner h1 {
	font-size:4.0em;
	font-family: 'Open Sans', sans-serif;
	font-weight:300;
	padding-left:0;
	margin-bottom:-0.125;
}
#cw #headerBanner .h1Bold {
	font-weight:800;
	font-family: 'Open Sans', sans-serif;
	color:#333333;
	text-transform:uppercase;
}
#cw #headerBanner #hrHeader {
	height:1px;
	width:475px;
	background:#CCCCCC;
}
#cw #headerBanner p {
	/*font-size:1.4em;*/
	font-size:16px;
	font-family: 'Open Sans', sans-serif;
	font-weight:300;
	margin-top:0em;
	padding-left:0.25em;
	padding-right:0em;
}
#cw #tool {
	background:url(/images/customCase/header-tool-small.png) no-repeat;
	width:211px;
	height:133px;
	right:0px;
	top:-4px;
	position:absolute;
	z-index:20;
}
#cw #arrow {
	background:url(/images/customCase/header-arrow-small.png) no-repeat;
	width:28px;
	height:28px;
	top:43px;
	right:190px;
	position:absolute;
}
#cw #photo {
	background:url(/images/customCase/header-photograph-small.png) no-repeat;
	width:111px;
	height:80px;
	top:22px;
	right:236px;
	position:absolute;
}
#cw #price {
	background:url(/images/customCase/custom-case-pricing.png) no-repeat;
	width:82px;
	height:82px;
	top:0px;
	right:374px;
	position:absolute;
}
#cw #grad {
	background:url(/images/customCase/header-1px-gradient.png) repeat-x;
	width:100%;
	height:65px;
	top:45px;
	position:absolute;
}
#cw #first {
	background:url(/images/customCase/splash-bg.jpg) no-repeat;
	height:357px;
}
#cw #hero {
	background:url(/images/customCase/splash-hero-small.png) no-repeat;
	width:431px;
	height:230px;
	top:16px;
	left:0px;
	position:absolute;
}
#cw #socialWrapper {
	width:296px;
	height:299px;
	left:96px;
	top:18px;
	position:absolute;
	z-index:30;
}
#cw #social {
	width:100%;
	height:100%;
	position:relative;
}
.sq {
	width:25%;
	height:64px;
	/*background:#969696;*/
	bottom:0px;
	position:absolute;
	text-align:center;
	display:table-cell;
}
.sq p {
	/*vertical-align:bottom;
	line-height:0.5em;*/
}
#sq1 {
}
	#sq1 p {
	/*padding-top:1px;*/
	}
#sq2 {
	left:25%;
}
#sq3 {
	left:50%;
}
#sq4 {
	left:75%;
}
	#sq4 p {
	padding-top:0.25em;
	}
#cw #heroCopyWrapper {
	width:434px;
	height:299px;
	right:50px;
	top:18px;
	position:absolute;
}
#cw #heroCopy {
	width:100%;
	height:100%;
	position:relative;
}
#cw #heroCopy p {
	width:420px;
	margin-top:0.75em;
	line-height:2.25em;
}
#cw .pill {
	width:212px;
	height:50px;
	position:absolute;
	bottom:0px;
	color:#FFF;
	line-height:50px;
	text-align:center;
	text-transform:uppercase;
	font-weight:800;
	font-family: 'Open Sans', sans-serif;
	font-size:13px;
	
	border-radius: 4px;
	-moz-border-radius: 4px;
	-khtml-border-radius: 4px;
	-webkit-border-radius: 4px;
	
	overflow:hidden;
	cursor:pointer;
	
	-webkit-box-shadow: 0px 1px 4px 0px darkgray; /* Saf3-4, iOS 4.0.2 - 4.2, Android 2.3+ */
	box-shadow: 0px 1px 4px 0px darkgray; /* Opera 10.5, IE9, FF4+, Chrome 6+, iOS 5 */
}
#cw .pill a {
	color:white;
	text-decoration:none;
}
#cw #redPill {
	left:0px;
}
#cw #redPill .pillGrad {
	background: #ed5c6b; /* Old browsers */
	background: -moz-linear-gradient(top, #ed5c6b 0%, #cc2328 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ed5c6b), color-stop(100%,#cc2328)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #ed5c6b 0%,#cc2328 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #ed5c6b 0%,#cc2328 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #ed5c6b 0%,#cc2328 100%); /* IE10+ */
	background: linear-gradient(to bottom, #ed5c6b 0%,#cc2328 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ed5c6b', endColorstr='#cc2328',GradientType=0 ); /* IE6-9 */
	
	/*Repeat for WebKit*/
	border-radius: 4px;
	-moz-border-radius: 4px;
	-khtml-border-radius: 4px;
	-webkit-border-radius: 4px;
}
#cw #blackPill {
	left:222px;
}
#cw #blackPill .pillGrad {
	background: #484848; /* Old browsers */
	background: -moz-linear-gradient(top, #484848 0%, #1b1b1b 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#484848), color-stop(100%,#1b1b1b)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #484848 0%,#1b1b1b 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #484848 0%,#1b1b1b 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #484848 0%,#1b1b1b 100%); /* IE10+ */
	background: linear-gradient(to bottom, #484848 0%,#1b1b1b 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#484848', endColorstr='#1b1b1b',GradientType=0 ); /* IE6-9 */
	
	/*Repeat for WebKit*/
	border-radius: 4px;
	-moz-border-radius: 4px;
	-khtml-border-radius: 4px;
	-webkit-border-radius: 4px;
}
.separator {
	background:url(/images/customCase/hori-div.png) no-repeat;
	width:972px;
	height:38px;
}
#cw #second {
	height:528px;
}
#cw #second h2 {
	margin-top:0.0em;
	margin-bottom:1.25em;
}
.colBase {
	position:absolute;
	width:100%;
	height:10px;
	bottom:0px;
}
.colBase div {
	background:#EBEBEB;
	width:300px;
	height:10px;
	margin-left:auto;
	margin-right:auto;	
}
.triCol {
	float:left;
	width:33%;
	height:385px;
	position:relative;
}
#triColMiddle {
	border-left:1px solid #EBEBEB;
	border-right:1px solid #EBEBEB;
}
.whyImgX {
	width:300px;
	height:155px;
	margin-left:auto;
	margin-right:auto;
}
#whyImg1 {
	background:url(/images/customCase/design-hero-1.jpg) no-repeat;
}
#whyImg2 {
	background:url(/images/customCase/design-hero-2.jpg) no-repeat;
}
#whyImg3 {
	background:url(/images/customCase/design-hero-3.jpg) no-repeat;
}
.triCol h3 {
	margin-top:0.75em;
	margin-bottom:1.5em;
}
.triCol p {
	width:260px;
}
#third {
	height:450px;
}
#cw #third h2 {
	margin-top:0.0em;
	margin-bottom:1.25em;
}
#leftHalf {
	float:left;
	width:59%;
}
#leftHalf div {
	width:480px;
	margin-left:auto;
	margin-right:auto;
}
#rightHalf {
	float:left;
	width:39%;
}
#rightHalf div {
	width:325px;
	margin-right:10%;
}
#third div {

}
#third h3 {
	white-space:normal;
	text-align:left;
	font-size:1.5em;
	line-height:1.5em;
	margin-top:3.0em;
}
#third p {
	width:335px;
}
#fourth {
	height:389px;
}
#fourth h2 {
	margin-bottom:1.0em;
}
#fourth .quarter {
	float:left;
	/*width:242px;*/
	width:24%;
}
#fourth .giX {
	width:220px;
	height:231px;
	margin-left:auto;
	margin-right:auto;
	padding-top:10px;
}
#fourth .giX div {
	width:110px;
	height:205px;
	margin-left:auto;
	margin-right:auto;
}
#fourth #gi1 {
	background:url(/images/customCase/inspiration-bg.jpg) no-repeat;
}
#fourth #gi2 {
	background:url(/images/customCase/inspiration-bg.jpg) no-repeat;
}
#fourth #gi3 {
	background:url(/images/customCase/inspiration-bg.jpg) no-repeat;
}
#fourth #gi4 {
	background:url(/images/customCase/inspiration-bg.jpg) no-repeat;
}
#fourth #gi1 div {
	background:url(/images/customCase/faceplate-inspiration-1.png) no-repeat;
}
#fourth #gi2 div {
	background:url(/images/customCase/faceplate-inspiration-2.png) no-repeat;
}
#fourth #gi3 div {
	background:url(/images/customCase/faceplate-inspiration-3.png) no-repeat;
}
#fourth #gi4 div {
	background:url(/images/customCase/faceplate-inspiration-4.png) no-repeat;
}
#fourth p.author, #fourth p.createAction {
	text-align:center;
	font-size:1.0em;
	line-height:1.0em;
}
#fourth .author a, #fourth .createAction a {
	font-weight:bold;
	color:#329BCB;
	font-style:italic;
	text-decoration:none;
}
#closeButton {
	text-transform:uppercase;
	position:absolute;
	right:-20px;
	top:-20px;
	width:34px;
	height:34px;
	background:#2C2C2C;
	text-align:center;
	border-radius:20px;
	font:bold 16px/34px Verdana, Geneva, sans-serif;
	border:3px solid #FFF;
	cursor:pointer;
	color:#FFF;
	z-index:100;
	-webkit-box-shadow: -2px 2px 4px 0px #000; /* Saf3-4, iOS 4.0.2 - 4.2, Android 2.3+ */
	box-shadow: -2px 2px 4px 0px #000; /* Opera 10.5, IE9, FF4+, Chrome 6+, iOS 5 */
}
</style>
<div id="cw">
	<div id="header">
    	<div id="grad"></div>
		<div id="tool"></div>
        <div id="arrow"></div>
        <div id="photo"></div>
        <div id="price"></div>
        <div id="headerBanner">
            <h1>Customize Your <span class="h1Bold">Case</span></h1>
            <div class="hr" id="hrHeader"></div>
            <p>High-Quality Images Printed Directly On Your Cover - Not A Sticker!</p>
        </div>
    </div>
    <div id="first">
    	<div id="hero"></div>
        <div id="socialWrapper">
			<div id="social">
                <div class="sq" id="sq1">
                    <p>
                        <div class="g-plus" data-action="share" data-annotation="vertical-bubble" data-height="60"></div>
                    </p>
                </div>
                <div class="sq" id="sq2">
                    <p>
                        <div id="fb-root"></div>
                        <div class="fb-like" data-href="http://www.wirelessemporium.com/custom-phone-cases" data-send="false" data-layout="box_count" data-width="50" data-show-faces="false" data-font="verdana"></div>
                    </p>
                </div>
                <div class="sq" id="sq3">
                    <p>
                        <a href="https://twitter.com/share" class="twitter-share-button" data-text="Customize your own phone case at" data-via="WirelessEmp" data-count="vertical">Tweet</a>
                    </p>
                </div>
                <div class="sq" id="sq4">
                    <p>
	                    <a href="http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.wirelessemporium.com%2Fcustom-phone-cases&media=http%3A%2F%2Fwww.wirelessemporium.com%2Fimages%2FcustomCase%2Fpin-it-custom-case.jpg&description=Customize%20Your%20Phone%20Cover%20at%20WirelessEmporium.com" class="pin-it-button" count-layout="vertical"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>
                    </p>
                </div>
            </div>
        </div>
        <div id="heroCopyWrapper">
			<div id="heroCopy">
		        <h2>Your Cover. Your Design.</h2>
                <p>Completely customize your cell phone case with our new, easy to use application. Upload your own designs and photos, add witty text and choose from a variety of different background colors. Once you're fully satisfied with your custom creation, we'll transfer your design directly onto a precision-molded case using advanced UV printing capabilities. The end result? Stylishly unique mobile protection at a great price and unmatched quality.</p>
                <div id="redPill" class="pill" onclick="window.location.href='<% if request.ServerVariables("HTTP_HOST") = "staging.wirelessemporium.com" then response.write "http://www.wirelessemporium.com" %>/custom/'"><div class="pillGrad"><a href="<% if request.ServerVariables("HTTP_HOST") = "staging.wirelessemporium.com" then response.write "http://www.wirelessemporium.com" %>/custom/">Customize Your Cover</a></div></div>
                <div id="blackPill" class="pill" onclick="showCustomCaseInfo()"><div class="pillGrad">Take An In-Depth Look</div></div>
			</div>
        </div>
    </div>
    <div class="separator"></div>
    <div id="second">
    	<h2>Why Design Your Own?</h2>
        <div class="triCol">
        	<div class="whyImgX" id="whyImg1"></div>
            <h3>Fully Customizable Protection</h3>
            <p>Delivers the normal impact resistance you have come to expect from a Wireless Emporium product, but with the added option of having your own custom design printed directly onto the surface. Support your favorite sports team or wow your family and friends with your stunning artwork.</p>
            <div class="colBase" align="center"><a href="/custom" title="get started now"><img src="/images/custom/getstarted.png" border="0" alt="get started now" /></a></div>
        </div>
        <div class="triCol" id="triColMiddle">
        	<div class="whyImgX" id="whyImg2"></div>
            <h3>State of the Art Technology</h3>
            <p>You're only limited by your imagination! Unparalleled UV printing capabilities allow us to produce crisp and stunning full-color images on a vast array of cell phone cases. Wireless Emporium supports images in both JPEG and PNG formats.</p>
            <div class="colBase" align="center"><a href="/custom" title="get started now"><img src="/images/custom/getstarted.png" border="0" alt="get started now" /></a></div>
        </div>
        <div class="triCol">
        	<div class="whyImgX" id="whyImg3"></div>
            <h3>Fast Turnaround, Free Shipping</h3>
            <p>We'll process your custom case within <%=gblCustomShippingDate%> business days from the time your order is placed. Combined with our fast, FREE shipping and industry-leading warranty policy, it's a no-brainer for anyone looking for the ultimate in custom cell phone protection.</p>
            <div class="colBase" align="center"><a href="/custom" title="get started now"><img src="/images/custom/getstarted.png" border="0" alt="get started now" /></a></div>
        </div>
    </div>
    <div class="separator"></div>
    <div id="third">
    	<h2>Featured Video</h2>
        <div id="leftHalf">
        	<div>
        		<iframe width="480" height="270" src="http://www.youtube.com/embed/epi2_d2PhUA?rel=0&showinfo=0&modestbranding=1&wmode=opaque" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
		<div id="rightHalf">
        	<div>
                <h3>Custom Smartphone Protection Explained</h3>
                <p>Do you want to see our amazing printer in action? Are you curious as to how a printer can print directly onto a phone case? Check out the video on the left to see the entire process from start to finish and get more insight in this amazing 3D printing process.</p>
        	</div>
			<div align="center"><a href="/custom/" title="get started now"><img src="/images/custom/getstarted.png" border="0" alt="get started now" /></a></div>            
        </div>        
    </div>
    <div class="separator"></div>
    <div id="fourth">
    	<h2>Get Inspired: Select Creations</h2>
        <div class="quarter">
			<div class="giX" id="gi1">
            	<div></div>
            </div>
            <p class="author">Design by <strong>Robert S.</strong></p>
            <p class="createAction">Like this? <em><a href="/custom/">Create your own!</a></em></p>
			<div align="center"><a href="/custom/" title="get started now"><img src="/images/custom/getstarted.png" border="0" alt="get started now" /></a></div>
        </div>
        <div class="quarter">
			<div class="giX" id="gi2">
            	<div></div>
            </div>
            <p class="author">Design by <strong>Shayla R.</strong></p>
            <p class="createAction">Like this? <em><a href="/custom/">Create your own!</a></em></p>
			<div align="center"><a href="/custom/" title="get started now"><img src="/images/custom/getstarted.png" border="0" alt="get started now" /></a></div>            
        </div>
        <div class="quarter">
			<div class="giX" id="gi3">
            	<div></div>
            </div>
            <p class="author">Design by <strong>Jonathan N.</strong></p>
            <p class="createAction">Like this? <em><a href="/custom/">Create your own!</a></em></p>
			<div align="center"><a href="/custom/" title="get started now"><img src="/images/custom/getstarted.png" border="0" alt="get started now" /></a></div>            
        </div>
        <div class="quarter">
			<div class="giX" id="gi4">
            	<div></div>
            </div>
            <p class="author">Design by <strong>Terry K.</strong></p>
            <p class="createAction">Like this? <em><a href="/custom/">Create your own!</a></em></p>
			<div align="center"><a href="/custom/" title="get started now"><img src="/images/custom/getstarted.png" border="0" alt="get started now" /></a></div>            
        </div>
    </div>

    <div style="clear:both; height:40px;"></div>	
    <div class="separator"></div>
    <div id="fourth" style="height:50px;"><h2>HAVE QUESTIONS? GET ANSWERS</h2></div>
</div>    
<div>
	<div style="width:100%;">
    	<table border="0" width="100%" cellpadding="0" cellspacing="0">
        	<tr>
            	<td align="center">
                    <span class="TurnToItemTeaser" style="font-size:12px !important;"></span>
                    <script type="text/javascript">
                        var TurnToItemSku = "146579";
                        document.write(unescape("%3Cscript src='" + document.location.protocol + "//static.www.turnto.com/traServer3/itemjs/" + turnToConfig.siteKey + "/" + TurnToItemSku + "' type='text/javascript'%3E%3C/script%3E"));
                    </script>
            	</td>
			</tr>
        </table>
		<%
        sql = 	"select	q.id qid, q.userName qName, q.content qContent, q.contentType qContentType, convert(varchar(10), q.postDate, 20) qPostDate" & vbcrlf & _
                "	,	a.id aid, a.userName aName, a.content aContent, a.contentType aContentType, convert(varchar(10), a.postDate, 20) aPostDate" & vbcrlf & _
                "	,	r.id rid, r.userName rName, r.content rContent, r.contentType rContentType, convert(varchar(10), r.postDate, 20) rPostDate" & vbcrlf & _
                "from	dbo.ugc q left outer join dbo.ugc a" & vbcrlf & _
                "	on	q.id = a.parentID and a.contentType = 2 left outer join dbo.ugc r" & vbcrlf & _
                "	on	a.id = r.parentID and r.contentType = 3" & vbcrlf & _
                "where	q.partnumber like 'WCD-CUSTOM-DESIGN'" & vbcrlf & _
                "	and	not (q.ContentType = 1 and a.id is null)" & vbcrlf & _
                "order by q.contentType, q.id, a.id, r.id"
        session("errorSQL") = sql
        arrUGC = getDbRows(sql)
    
		if not isnull(arrUGC) then
		%>
		<div style="width:100%; overflow:auto; height:300px;">
		<%
			numQuestions = cint(0)
			numAnswers = cint(0)
			numAns = cint(0)
			nQuestions = cint(getNumOfContent(3, 1, 0, arrUGC))
			nComments = cint(getNumOfContent(3, 4, 0, arrUGC))
			curQID = cint(-1)
			if nQuestions > 0 then
			%>
			<div style="float:left; width:100%; padding:10px 0px 0px 0px; border-top:1px solid #ccc;" class="boldText" align="left">
				<% if nQuestions > 1 then %>
					<%=formatnumber(nQuestions,0)%> QUESTIONS FROM THE COMMUNITY        
				<% else %>
					<%=formatnumber(nQuestions,0)%> QUESTION FROM THE COMMUNITY
				<% end if %>
			</div>    
			<%
			end if
			for i=0 to ubound(arrUGC, 2)
				qid = cint(arrUGC(0,i))
				qName = arrUGC(1,i)
				qContent = arrUGC(2,i)
				qContentType = arrUGC(3,i)
				qPostDate = arrUGC(4,i)
				aid = arrUGC(5,i)
				aName = arrUGC(6,i)
				aContent = arrUGC(7,i)
				aContentType = arrUGC(8,i)
				aPostDate = arrUGC(9,i)
				rid = arrUGC(10,i)
				rName = arrUGC(11,i)
				rContent = arrUGC(12,i)
				rContentType = arrUGC(13,i)
				rPostDate = arrUGC(14,i)
				
				if qContentType	<> "4" then
					if not isnull(qid) and instr(ugcID, "|" & qid & "|") <= 0 then
						numQuestions = numQuestions + 1
						ugcID = ugcID & qid & "|"
					%>
						<div style="float:left; width:100%; margin-top:10px;">
							<div style="float:left; width:766px; padding:15px 0px 5px 20px; font-weight:bold; border-top:1px solid #ccc;" class="qna-normalText">
								<div style="float:left; width:20px;" class="qna-normalText"><%=formatnumber(numQuestions,0)%>.)</div>
								<div style="float:left; width:736px; text-align:left; padding-left:10px;"><%=qContent%></div>
								<div style="float:left; width:736px; color:#666; padding:5px 0px 0px 30px; text-align:left;" class="smlText">Asked by <%=printUserName(qName)%>&nbsp;&nbsp;|&nbsp;&nbsp;<%=qPostDate%></div>
							</div>
							<div style="float:left; width:130px; padding:15px 0px 5px 0px; color:#3699D4; border-top:1px solid #ccc;" align="right" class="smlText">
								<%
								numAnswers = cint(formatnumber(getNumOfContent(0, qid, 5, arrUGC),0))
								if numAnswers > 1 then 
									response.write numAnswers & " Answers<br />"
									response.write "<a id=""" & qid & "_id_viewAns"" href=""javascript:void(0)"" onclick=""toggleAnswer(" & qid & "," & numAnswers & ");"" style=""color:#3699D4;"">View all answers</a>"
								else
									response.write numAnswers & " Answer"
								end if
								%>
							</div>
						</div>
					<%
					end if
		
					if not isnull(aid) and instr(ugcID, "|" & aid & "|") <= 0 then
						if qid <> curQID then
							curQID = qid
							numAns = 0
						end if
						numAns = numAns + 1
						ugcID = ugcID & aid & "|"
						
						curAnswerStyle = ""				
						if numAns > 1 then
							curAnswerStyle = "display:none;"
						%>
						<div id="ans_<%=qid%>_<%=numAns%>" style="float:left; width:866px; padding:5px 5px 5px 65px; font-size:11px; <%=curAnswerStyle%>" class="qna-normalText2">
							<span style="color:#FF6600; font-weight:bold;" class="normalText">Answer</span>&nbsp;&nbsp;<%=aContent%>&nbsp;&nbsp;&nbsp;
						</div>
						<%
						else
						%>
						<div style="float:left; width:866px; padding:5px 5px 5px 65px; font-size:11px;" class="qna-normalText2">
							<span style="color:#FF6600; font-weight:bold;" class="normalText">Answer</span>&nbsp;&nbsp;<%=aContent%>&nbsp;&nbsp;&nbsp;
						</div>
						<%
						end if
					end if
				
					if not isnull(rid) and instr(ugcID, "|" & rid & "|") <= 0 then
						ugcID = ugcID & rid & "|"
						if numAns > 1 then
						%>
						<div id="rpy_<%=qid%>_<%=numAns%>" style="float:left; width:846px; padding:5px 5px 5px 85px; font-size:11px; <%=curAnswerStyle%>" class="qna-normalText2">
							<span style="color:#3699D4; font-weight:bold;" class="normalText">Reply</span>&nbsp;&nbsp;<%=rContent%>&nbsp;&nbsp;&nbsp;
						</div>
						<%
						else
						%>
						<div style="float:left; width:846px; padding:5px 5px 5px 85px; font-size:11px;" class="qna-normalText2">
							<span style="color:#3699D4; font-weight:bold;" class="normalText">Reply</span>&nbsp;&nbsp;<%=rContent%>&nbsp;&nbsp;&nbsp;
						</div>
						<%
						end if
					end if
				end if
			next
		
			if nComments > 0 then
				if nQuestions > 0 then
				%>
				<div style="float:left; width:100%; height:40px;"></div>
				<%
				end if
			%>
			<div style="float:left; width:100%; padding:10px 0px 10px 0px; border-top:1px solid #ccc; border-bottom:1px solid #ccc;" class="boldText" align="left">
				<% if nComments > 1 then %>
					<%=formatnumber(nComments,0)%> COMMENTS FROM THE COMMUNITY        
				<% else %>
					<%=formatnumber(nComments,0)%> COMMENT FROM THE COMMUNITY
				<% end if %>
			</div>    
			<%
			end if
			for i=0 to ubound(arrUGC, 2)
				qid = arrUGC(0,i)
				qName = arrUGC(1,i)
				qContent = arrUGC(2,i)
				qContentType = arrUGC(3,i)
				qPostDate = arrUGC(4,i)
				aid = arrUGC(5,i)
				aName = arrUGC(6,i)
				aContent = arrUGC(7,i)
				aContentType = arrUGC(8,i)
				aPostDate = arrUGC(9,i)
				rid = arrUGC(10,i)
				rName = arrUGC(11,i)
				rContent = arrUGC(12,i)
				rContentType = arrUGC(13,i)
				rPostDate = arrUGC(14,i)
		
				if qContentType	= "4" then
					if not isnull(qid) and instr(ugcID, "|" & qid & "|") <= 0 then
						numComments = numComments + 1
						ugcID = ugcID & qid & "|"
					%>
						<div style="float:left; width:100%; padding:15px 0px 5px 0px; font-weight:bold;" class="qna-normalText">
							<div style="float:left; width:20px;" class="qna-normalText"><%=formatnumber(numComments,0)%>.)</div>
							<div style="float:left; width:866px; text-align:left; padding-left:10px;"><%=qContent%></div>
							<div style="float:left; width:866px; color:#666; padding-left:30px;" class="smlText">Commented by <%=printUserName(qName)%>&nbsp;&nbsp;|&nbsp;&nbsp;<%=qPostDate%></div>
						</div>
					<%
					end if
				end if
			next
			%>
		</div>
		<%
		end if
		%>
	</div>
</div>
<script>
	function toggleAnswer(questionID, numAnswers)
	{
		for (i=0; i<=numAnswers; i++)
		{
			if (document.getElementById("ans_" + questionID + "_" + i) != null) 
			{
				toggle("ans_" + questionID + "_" + i);
			}
		}
		if (document.getElementById(questionID + "_id_viewAns").innerHTML == "View all answers")
			document.getElementById(questionID + "_id_viewAns").innerHTML = "Hide answers";
		else
			document.getElementById(questionID + "_id_viewAns").innerHTML = "View all answers";
	}
</script>

<script type="text/javascript">
	function showCustomCaseInfo() {
		viewPortW = $(window).width();
		window.scrollTo(0, 0);
		if (document.getElementById('prodDescription') != null) document.getElementById('prodDescription').style.display = 'none';	
		ajax("/ajax/ajaxCustomCaseInfographic.asp?screenW="+viewPortW,'popBox');
		document.getElementById("popBox").style.position = "absolute";
		document.getElementById("popCover").onclick = function () { closeFloatingBox() }
		document.getElementById("popBox").onclick = function () { closeFloatingBox() }
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	function closeFloatingBox() {
		if (document.getElementById('prodDescription') != null) document.getElementById('prodDescription').style.display = '';
		//------------------ change it to original -------------------//
		document.getElementById("popBox").style.position = "fixed";
		document.getElementById("popCover").onclick = function () {}
		document.getElementById("popBox").onclick = function () {}
		//------------------------------------------------------------//
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
		document.getElementById("popCover").innerHTML = "";
		document.getElementById("popBox").innerHTML = "";
	}	
</script>
<% 'Google Button %>
<script type="text/javascript">
  (function() {
	var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	po.src = 'https://apis.google.com/js/plusone.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<% 'Facebook Button %>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>                    
<% 'Twitter Button %>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>                    
<% 'Pinterest Button %>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
<%
function getNumOfContent(searchIdx, searchVal, dupCheckIdx, byref arr)
	ret = 0
	strDupCheck = "|"
	if isarray(arr) then
		for k=0 to ubound(arr,2)
			if not isnull(searchVal) and not isnull(arr(dupCheckIdx,k)) then
				if cstr(searchVal) = cstr(arr(searchIdx,k)) and instr(strDupCheck, "|" & arr(dupCheckIdx,k) & "|") <= 0 then
					ret = ret + 1
				end if
				strDupCheck = strDupCheck & arr(dupCheckIdx,k) & "|"
			end if
		next
	end if
	getNumOfContent = ret
end function

function printUserName(userName)
	if trim(userName) = "" then
		printUserName = "Anonymous"
	else
		printUserName = userName
	end if
end function
%>
<!--#include virtual="/includes/template/bottom.asp"-->
