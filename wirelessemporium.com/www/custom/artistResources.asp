<%
	response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<%
	'sql = ""
	'session("errorSQL") = sql
	'set	RS = oConn.execute(SQL)
%>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,800' rel='stylesheet' type='text/css'>
<style>
#divGetCode {
	padding:1.0em;
	width:250px;
	height:125px;
	overflow:auto;
	border:1px solid #666666;
	background:lightgray;
}
#divContainDemo {
	width:150px;
	height:200px;
	float:left;
	padding-left:25px;
	margin-right:0.0em;
	padding-top:25px;
}
#divContainGetCode {
	float:left;
}
#divBlogBadgeInstructions {
	font-family:Verdana, Geneva, sans-serif;
}
#divBlogBadgeInstructions h1 {
	font-size:2.0em;
	font-weight:800;
	font-family: 'Open Sans', sans-serif;
}
#divBlogBadgeInstructions h2 {
	font-size:1.5em;
	font-weight:300;
	font-family: 'Open Sans', sans-serif;
}
#divContainGetCode ul li {
	list-style:none;
}<br />
</style>
<div id="divBlogBadgeInstructions">

	<h1>Blog Badge Instructions</h1>
    
    <p>We're glad to welcome you to the Wireless Emporium Featured Artist Program. Show off your prestigious position as one of our Featured Artist with this cool badge!</p>
    
    <p>Here are some easy instructions:</p>
    
    <h2>Adding to Blogger:</h2>
    
    <ol>
    	<li>Go to Layout (Design in the old interface).</li>
    	<li>Click "Add A Gadget" and select the HTML/Javascript gadget.</li>
    	<li>Copy the code and paste it inside the content box.</li>
    </ol>
    
    <h2>Adding to WordPress:</h2>
    
    <ol>
    	<li>Go to Dashboard > Appearance > Widgets  > Available Widgets</li>
    	<li>Drag the Text widget into a sidebar.</li>
    	<li>Paste in the code.</li>
    	<li>Save.</li>
    </ol>

	<div id="divFloatHack"><h2>Get Your Code:</h2></div>
	
    <div id="divContainDemo">
        <a href="http://www.wirelessemporium.com/" target="_blank"><img alt="Custom Phone Cases - Wireless Emporium" src="http://www.wirelessemporium.com/custom-case-badge.jpg" /></a>
    </div>

    <div id="divContainGetCode">
        <ul>
            <li>
            <textarea id="divGetCode" onclick="this.select();">
                &lt;a href=&quot;http://www.wirelessemporium.com/&quot; target=&quot;_blank&quot;&gt;&lt;img alt=&quot;Custom Phone Cases - Wireless Emporium&quot; src=&quot;http://www.wirelessemporium.com/custom-case-badge.jpg&quot; /&gt;&lt;/a&gt;
            </textarea>
            </li>
        </ul>
    </div>
</div>
<!--#include virtual="/includes/template/bottom.asp"-->
