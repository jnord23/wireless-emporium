<?php
//NOREFRESH==========================================
$docRoot = $_SERVER['DOCUMENT_ROOT']; ///home/nrsparx/public_html 
$docRoot .= "/custom/";
$host = $_SERVER['HTTP_HOST'];
$sitepath = "http://www.wirelessemporium.com/custom/";
$imageMagickPath  = "convert";
$config['server'] = 'localhost'; 
$config['database'] ='weDB';
$config['user'] ='root';
$config['pass'] ='bears1986';


 
//Basic===========================
define('__BASEURL__', $sitepath); 
define('__BASE_DIR_FRONT__', $docRoot); 
define('__BASE_DIR_ADMIN__', $docRoot.'admin/'); 
define('AMFPHPPATH', __BASE_DIR_ADMIN__."amfphp/services/");
define("ABSOLUTEPATH" , $docRoot);
define("SITEURL" , $sitepath);
define("ABSOLUTEPATHADMIN" , $docRoot."admin/");
define("IMAGEMAGICKPATH" , $imageMagickPath);
define('BASEUPLOADFILE',$docRoot."files/");
define('BASEUPLOADFILEPATH',$sitepath."files/");

define('IMAGEMAGICPATH', $imageMagickPath);
define('SITE_URL', $sitepath);
define('SEARCHTEXT', "Search");
define('DEFAULTDATEFORMAT', "d-m-Y");


// AMFPHP==========================================
 define('CONFIGSERVER', $config['server']);
 define('CONFIGDATABASE', $config['database']);
 define('CONFIGUSER', $config['user']);
 define('CONFIGPASS', $config['pass']);
 define('CONFIGIMAGEMAGICPATH', $imageMagickPath);
 define("ABSOLUTEPATH" , $docRoot);


//Site Logo=============================
define('__SITELOGOORIGINAL__',BASEUPLOADFILE."logo/original/");
define('__SITELOGOORIGINALPATH__',BASEUPLOADFILEPATH."logo/original/");
define('__SITELOGOTHUMB__',BASEUPLOADFILE."logo/thumb/");
define('__SITELOGOTHUMBPATH__',BASEUPLOADFILEPATH."logo/thumb/");
define('__SITELOGOLAREGE__',BASEUPLOADFILE."logo/large/");
define('__SITELOGOLAREGEPATH__',BASEUPLOADFILEPATH."logo/large/");

// FONT====================================
$pathFontTTF = "files/font/ttf";

define('__FONTORIGINAL__',BASEUPLOADFILE."font/original");
define('__FONTORIGINALPATH__',BASEUPLOADFILEPATH."font/original/");
define('__FONTTTF__',BASEUPLOADFILE."font/ttf/");
define('__FONTTTFPATH__',BASEUPLOADFILE."font/ttf/");
define('__FONTPATH__',BASEUPLOADFILEPATH."font/original/");


// Raw Product==========================================
define('__RAWPRODUCTORIGINAL__',BASEUPLOADFILE."product/original/");
define('__RAWPRODUCTORIGINALPATH__',BASEUPLOADFILEPATH."product/original/");
define('__RAWPRODUCTTHUMB__',BASEUPLOADFILE."product/thumb/");
define('__RAWPRODUCTTHUMBPATH__',BASEUPLOADFILEPATH."product/thumb/");
define('__RAWPRODUCTLARGE__',BASEUPLOADFILE."product/large/");
define('__RAWPRODUCTLARGEPATH__',BASEUPLOADFILEPATH."product/large/");

// Main Product==========================================
define('__MAINPRODUCTORIGINAL__',BASEUPLOADFILE."mainproduct/original/");
define('__MAINPRODUCTORIGINALPATH__',BASEUPLOADFILEPATH."mainproduct/original/");
define('__MAINPRODUCTTHUMB__',BASEUPLOADFILE."mainproduct/thumb/");
define('__MAINPRODUCTTHUMBPATH__',BASEUPLOADFILEPATH."mainproduct/thumb/");
define('__MAINPRODUCTLARGE__',BASEUPLOADFILE."mainproduct/large/");
define('__MAINPRODUCTLARGEPATH__',BASEUPLOADFILEPATH."mainproduct/large/");
define('__MAINPRODUCTEXTLARGE__',BASEUPLOADFILE."mainproduct/extLarge/");
define('__MAINPRODUCTEXTLARGEPATH__',BASEUPLOADFILEPATH."mainproduct/extLarge/");

// Flag==============================
define('__FLAGORIGINAL__',BASEUPLOADFILE."flag/original/");
define('__FLAGORIGINALPATH__',BASEUPLOADFILEPATH."flag/original/");
define('__FLAGTHUMB__',BASEUPLOADFILE."flag/thumb/");
define('__FLAGTHUMBPATH__',BASEUPLOADFILEPATH."flag/thumb/");

//Category ====================================

define('__CATEGORYORIGINAL__',BASEUPLOADFILE."category/original/");
define('__CATEGORYORIGINALPATH__',BASEUPLOADFILEPATH."category/original/");
define('__CATEGORYTHUMB__',BASEUPLOADFILE."category/thumb/");
define('__CATEGORYTHUMBPATH__',BASEUPLOADFILEPATH."category/thumb/");
define('__CATEGORYLARGE__',BASEUPLOADFILE."category/large/");
define('__CATEGORYLARGEPATH__',BASEUPLOADFILEPATH."category/large/");

//Design Category ====================================
define('__DESIGNCATORIGINAL__',BASEUPLOADFILE."designcategory/original/");
define('__DESIGNCATORIGINALPATH__',BASEUPLOADFILEPATH."designcategory/original/");
define('__DESIGNCATTHUMB__',BASEUPLOADFILE."designcategory/thumb/");
define('__DESIGNCATTHUMBPATH__',BASEUPLOADFILEPATH."designcategory/thumb/");
define('__DESIGNCATLARGE__',BASEUPLOADFILE."designcategory/large/");
define('__DESIGNCATLARGEPATH__',BASEUPLOADFILEPATH."designcategory/large/");

//Font Category ====================================

define('__FONTCATORIGINAL__',BASEUPLOADFILE."fontcategory/original/");
define('__FONTCATORIGINALPATH__',BASEUPLOADFILEPATH."fontcategory/original/");
define('__FONTCATTHUMB__',BASEUPLOADFILE."fontcategory/thumb/");
define('__FONTCATTHUMBPATH__',BASEUPLOADFILEPATH."fontcategory/thumb/");
define('__FONTCATLARGE__',BASEUPLOADFILE."fontcategory/large/");
define('__FONTCATLARGEPATH__',BASEUPLOADFILEPATH."fontcategory/large/");

//Design =================================================
define('__DESIGNORIGINAL__',BASEUPLOADFILE."design/original/");
define('__DESIGNORIGINALPATH__',BASEUPLOADFILEPATH."design/original/");
define('__DESIGNTHUMB__',BASEUPLOADFILE."design/thumb/");
define('__DESIGNTHUMBPATH__',BASEUPLOADFILEPATH."design/thumb/");
define('__DESIGNLARGE__',BASEUPLOADFILE."design/large/");
define('__DESIGNLARGEPATH__',BASEUPLOADFILEPATH."design/large/");

//No Image============================================
define('__NOIMAGEPATH__',BASEUPLOADFILEPATH."noimage/noimage.jpeg");


//---------Language------------
define('__DIR_LANGUAGES__', __BASE_DIR_FRONT__. 'languages/');
//=========XML Directory===============================
define('__DIR_XML__', __BASE_DIR_FRONT__. 'toolxml/');

//=========PDF  Directory===============================
define('__DIR_PDF__', BASEUPLOADFILE. 'FinalProduct/');
define('__DIR_PDFPATH__', BASEUPLOADFILEPATH. 'FinalProduct/');
//Tool Redirected Page===================
define('__TOOLUSERREDIRECTEDPAGE__',SITE_URL."product.php");
define('__TOOLADMINREDIRECTEDPAGE__',SITE_URL."admin/assign-main-product.php");
define('TOOLPAGE', "index.php");




?>
