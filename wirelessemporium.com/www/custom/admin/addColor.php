<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageColor.php","add_record");
/*---Basic for Each Page Ends----*/

$colorObj = new Color();

if(isset($_POST['submit'])) {

	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $colorObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $colorObj->getTotalRow($rst);	
	
	if($num){
		$langIdArr = array();		
		while($line = $colorObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('colorName_'.$value,$_POST['colorName_'.$value], 'req', 'Please enter Color Name.');			
		}
		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		foreach($langIdArr as $key=>$value) {
			$arr_error['colorName_'.$value]=$obj->fnGetErr($arr_error['colorName_'.$value]);
			if($arr_error['colorName_'.$value]) 
				$errorArr = 1;
		}
		
		if($_POST['colorCode'] != ''){
			if($colorObj->isColorExist($_POST['colorCode'],'')) { 
				$arr_error['colorCode'] = "Color Code already exist. ";
				if($arr_error['colorCode']) 
					$errorArr = 1;
			}else{
				$arr_error['colorCode'] = "";
			}				
		}else{
			$arr_error['colorCode'] = "";
		}
			
		
		if($errorArr == 0 && isset($_POST['submit']) && empty($arr_error['colorCode'])){
			$_POST = postwithoutspace($_POST);
			$colorObj->addRecord($_POST);
		}
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->

<!--				Color Picker (START)		-->

<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<script type="text/javascript">
	function hrefBack1(){
		window.location='manageColor.php';
	}
</script>
<!--				Color Picker (END)			-->

</head>
<body>
<? include('includes/header.php'); ?>
<div id="nav-under-bg"><!-- --></div>
		<form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
		<div class="main-body-div-new">
          <div class="main-body-div-header">Add Color</div>
		  <!-- left position -->
        
            <div class="main-body-div4" id="mainDiv">
              <div class="add-main-body-left-new" >
                <ul>
				<li class="add-main-body-left-new-text" style="clear:both; width:500px;padding-bottom:5px;" ><span class="small_error_message">
				<? $genObj = new GeneralFunctions(); ?>
                    <?=$_SESSION['SESS_MSG']?>
                  </span></li>
				  <li class="lable">Color Code <span class="spancolor">*</span></li>
                   <li><input type="text" name="colorCode" id="m__color__code" maxlength="7" value="<?=$_POST[colorCode] ?>" />&nbsp;&nbsp;<img alt="col_code" src="colorpicker/color.gif" onClick="showColorPicker(this,document.forms[0].colorCode)" border="0" style="cursor:pointer;">
				   <p style="padding-left:160px;color:#FF0000;" ><?=$arr_error['colorCode'] ?></p></li>				  
                  <li class="lable">Color Name <span class="spancolor">*</span></li>
                  
                    <?	
						echo $genObj->getLanguageTextBox('colorName','m__Color_Name',$arr_error); //1->type,2->name,3->id
					?>
					
				   
               </ul>
              </div>
              <div class="main-body-sub">
                <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
                &nbsp;
                <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
              </div>
            </div>
</div>
	</form>
	
<? unset($_SESSION['SESS_MSG']); ?>