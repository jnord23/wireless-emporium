<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageProducts.php","edit_record");
$xmfInfoObj = new XmlInfo();
$xmfInfoObj->setPanel();
/*---Basic for Each Page Ends----*/
$prodObj = new Products(base64_decode($_GET['id']));
$genObj= new GeneralFunctions;
$pid = base64_decode($_GET['id']);
//Result & Values============================================================
$result = $prodObj->getResult(base64_decode($_GET['id']));
$price=stripslashes($_POST[productPrice]?$_POST[productPrice]:$result->productPrice);
$weight=stripslashes($_POST[productWeight]?$_POST[productWeight]:$result->productWeight);
$qty=stripslashes($_POST[quantity]?$_POST[quantity]:$result->quantity);
$img=__RAWPRODUCTTHUMBPATH__.$prodObj->getImageName($id=base64_decode($_GET['id']));
$orientation = $_POST[orientation]?$_POST[orientation]:$result->orientation;
//echo "<pre>"; print_r($result); echo"</pre>";exit;	

// Update =========================================================================
if(isset($_POST['update'])) {	
	require_once('validation_class.php');
	$obj = new validationclass();	
	$rst = $prodObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $prodObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $prodObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}	
		
		//Add Error=====================================
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('productName_'.$value,$_POST['productName_'.$value], 'req', 'Please Enter Product Name.');			
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('productDesc_'.$value,$_POST['productDesc_'.$value], 'req', 'Please enter the Product Desciption.');
		}
		$obj->fnAdd('aspId',$_POST['aspId'], 'req', 'Please Enter Product aspId.');		
		$obj->fnAdd('productPrice',$_POST['productPrice'], 'req', 'Please Enter Product Price.');
		$obj->fnAdd('productWeight',$_POST['productWeight'], 'req', 'Please Enter Product Weight.');
		
		//Validate=================================
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1; 

		//Get Error========================================				
		foreach($langIdArr as $key=>$value) {
			$arr_error['productName_'.$value]=$obj->fnGetErr($arr_error['productName_'.$value]);	
		}
		foreach($langIdArr as $key=>$value) {
			$arr_error['productDesc_'.$value]=$obj->fnGetErr($arr_error['productDesc_'.$value]);	
		}
		$arr_error[aspId]=$obj->fnGetErr($arr_error[aspId]);
		$arr_error[productPrice]=$obj->fnGetErr($arr_error[productPrice]);
		$arr_error[productWeight]=$obj->fnGetErr($arr_error[productWeight]);
		
		#------check ASP id Exists----------

		if($prodObj->isProductAspIdExist($_POST['aspId'],$pid)){ 
			$arr_error['aspId'] = "ASP Id already taken or used.";
			$str_validate=0;				
		}

		
		
		//Check Product Name Exists====================================
		foreach($langIdArr as $key=>$value) {
			if($prodObj->isProductNameExist($_POST['productName_'.$value],$value,base64_decode($_GET['id']))){ 
				$arr_error['productName_'.$value] = "Product already exist. ";
				$str_validate=0;
				
			}
		}
		
		//Check Upload File Extension============================================
		foreach($_FILES as $key=>$fiesArr){
			if($_FILES[$key]['name']){
				$filename = stripslashes($_FILES[$key]['name']);
				$extension = strtolower(findexts($filename));
				if(!$prodObj->checkExtensions($extension)){
					$arr_error[$key] = "Upload Only ".$prodObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'") ." image extension.";
					$str_validate=0;	
				}
			}
		}
		//Edit Record if Validate Pass: $str_validate is 1=============================
		if($str_validate){
			$_POST = postwithoutspace($_POST);	
			$prodObj->editRecord($_POST);
		}
	}	
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>
<script language="javascript" src="js/ajax.js"></script>
<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->
<script type="text/javascript">
function hrefBack1(){
window.location.href='manageProducts.php';
}

</script>
</head>
<body>
	<? include('includes/header.php'); ?>
	<div id="nav-under-bg">
	<!-- -->
	</div>
	
	<div class="main-body-div-new">
	<div class="main-body-div-header">Edit Product</div>
	<!-- left position -->
	<div class="main-body-div4" id="mainDiv">
		<div align="center"><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
		
		<form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data" >
			<input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">
			<input type="hidden" name="page" value="<?=$_GET['page']?>">
			
			<!--============================================================= -->
			<div class="add-main-body-left-new" >
				<ul>
					<li class="add-main-body-left-new-text" style="clear:both; width:500px;" >
					<span class="small_error_message"><?=$_SESSION['SESS_MSG']?></span>
					</li>
					<!--Category : Device ==================== -->
					<li class="lable">Category <span class="spancolor">*</span></li>
					<li class="lable2">
					<select name="cat" id="cat" onchange="getSubCatByCatId(this.value);">
					<?=$genObj->getCategoryList($result->categoryId);?>
					</select>
					</li>
					
					<!-- Sub Category ======================== -->
					<li class="lable">Sub Category <span class="spancolor">*</span></li>
					<li class="lable2" >
					<select name="subCat" id="subCat" >
					<?					
					echo $genObj->subCategoryList($result->categoryId,$result->subCatId);
					?>
					</select>
					<span id="subCatSPAN"></span>
					</li>
					<!-- ASP ID  ================================= -->			
					<li class="lable">Product ASP ID <span class="spancolor">*</span></li>
					<li>
					<input type="text" name="aspId" id="aspId" class="wel" value="<?=stripslashes($result->aspId)?>" maxlength="8" />
					<p style="padding-left:150px;"><?=$arr_error[aspId]?></p>
					</li>
					<!-- Product Name================= -->
					<li class="lable">Product Name <span class="spancolor">*</span></li>
					<?php 
					//1->type,2->name,3->id,4->tablename,5->tableid
					echo 
					$genObj->getLanguageEditTextBox('productName','m__productName',TBL_PRODUCT_DESCRIPTION,base64_decode($_GET['id']),"productId",$arr_error); 
					?>
					<!-- Product Price======================= -->						
					<li class="lable">Product Price <span class="spancolor">*</span></li>
					<li>
					<input type="text" name="productPrice" id="m__Product_Price" class="wel" value="<?=$price?>"  maxlength="8" onkeyup="return isNum12(this.value);" />
					<p style="padding-left:150px;" id="price" ><?=$arr_error[productPrice]?></p>	
					</li>
					<!-- Product Weight========================= -->
					<li class="lable">Product Weight <span class="spancolor">*</span></li>
					<li>
					<input type="text" name="productWeight" id="m__Product_Weight" class="wel" value="<?=$weight?>"  maxlength="8" onkeyup="return isNum123(this.value);" />
					<p style="padding-left:150px;" id="weight" ><?=$arr_error[productWeight]?></p>
					</li>
					
					<!-- Quantity==================== -->
					<li class="lable">Product Quantity </li>
					<li>
					<input type="text" name="quantity" class="wel" value="<?=$qty?>"  />
					<p style="padding-left:150px;" id="price" ><?=$arr_error[quantity]?></p>
					</li>
					
					<!-- Description========================== -->
					<li class="lable">Product Description <span class="spancolor">*</span></li>
					<?
					echo $genObj->getLanguageEditTextarea('productDesc','m__Product_Description',TBL_PRODUCT_DESCRIPTION,base64_decode($_GET['id']),"productId",$arr_error); 
					?>
					<li>&nbsp;</li>	
				
				
					<!--Product Image================== -->					
					<li class="lable" >Product image <span class="spancolor">*</span></li>
					<li>					
					<img src="<?=$img?>" width="50" height="50" />
					<input type="file" id="product_image" name="productImage" size="40">
					<p  style="padding-left:0px;"><?=$arr_error["productImage"]?></p>
					</li>
					<!-- Product Orientation======================= 
					<li class="lable" >Product Orientation  <span class="spancolor">*</span></li>
					<li class="lable2">					
					<select name="orientation" id="m__product_orientation">
					<?=$genObj->getOrientationList($orientation)?>
					</select>					
					</li>	-->
				
				</ul>
			</div>
			<!--===================================================== -->
			
			
			<div style="clear:both; float:left;">
				<div id="divProduct" style="height:15px;text-align:center;font-weight:bold; color:#FF0000;"></div>
				<!-- Div Id Call for Ajax Function-->
				<fieldset style="margin:0px 0px 0px 20px ; font-weight:normal; width:630px;">
					<legend style="color:#000000;"><b>Product Image Detail</b></legend>
					<?php 
					$viewResult = $prodObj->getViewImageUploadEdit(base64_decode($_GET['id']),$arr_error);	
					?>
					<div style=" margin-left:50px;">
					<table border="0">
					<?php	
					foreach($viewResult as $value){
					extract($value);
					?>
					<tr id="productView<?=$id;?>">
					<td width="100" valign="top"><?=$view?></td>
					<td id="productImg<?php echo $id;?>"><?=$html?></td>
					<td valign="top"><?=$delImage?></td>
					</tr>
					<?php  }?>
					</table>
					</div>
				
				</fieldset>
			</div>
			
			<!-- Update Button========================== -->
			<div class="main-body-sub">
				<input type="submit" name="update" class="main-body-sub-submit" style="cursor:pointer;" value="Update" />
				&nbsp;
				<input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
			</div>
	
		</form>  
	</div>
	</div>
	<? unset($_SESSION['SESS_MSG']); ?>
</body>
</html>
<!-- Script Section===================================== -->
<script type="text/javascript" >
function checkAllcheckbox(fid,sid){
if(document.getElementById(fid+"menuCheck").checked == true){
for (var i = 0; i < sid; i++) {
document.getElementById(fid+"mCheck"+i).checked = true;	
}
}else{
for (var i = 0; i < sid; i++) {
document.getElementById(fid+"mCheck"+i).checked = false;	
}
}
}
function selectAllBox(){
var aid = document.getElementById("atid").value;
var aaid = document.getElementById("atvid").value;
var spaid = aid.split(",");
var sppaid = aaid.split(",");
if(document.getElementById("selectval").checked == true){
for(k=0;k<spaid.length;k++){
document.getElementById(spaid[k]+"menuCheck").checked = true;
for(l=0;l<sppaid[k];l++){
document.getElementById(spaid[k]+"mCheck"+l).checked = true;
}
}
}else{
for(i=0;i<sppaid.length;i++){
document.getElementById(spaid[i]+"menuCheck").checked = false;
for(l=0;l<sppaid[i];l++){
document.getElementById(spaid[i]+"mCheck"+l).checked = false;
}
}
}
}
</script>

