<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageProducts.php","add_record");
$xmfInfoObj = new XmlInfo();
$xmfInfoObj->setPanel();
/*---Basic for Each Page Ends----*/
$prodObj = new Products();
$genObj= new GeneralFunctions;



if(isset($_POST['submit'])) {    
	//echo "<pre>"; print_r($_POST); echo "</pre>";exit;
	require_once('validation_class.php');
	$obj = new validationclass();	
	$rst = $prodObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $prodObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $prodObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		
		$obj->fnAdd('aspId',$_POST['aspId'], 'req', 'Please Enter Product aspId.');
			
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('productName_'.$value,$_POST['productName_'.$value], 'req', 'Please Enter Product Name.');			
		}		
		$obj->fnAdd('productPrice',$_POST['productPrice'], 'req', 'Please Enter Product Price.');		
		$obj->fnAdd('productWeight',$_POST['productWeight'], 'req', 'Please Enter Product Weight.');		
		
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('productDesc_'.$value,$_POST['productDesc_'.$value], 'req', 'Please enter the Product Desciption.');
		}
		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		$arr_error[aspId]=$obj->fnGetErr($arr_error[aspId]);
		foreach($langIdArr as $key=>$value) {
			$arr_error['productName_'.$value]=$obj->fnGetErr($arr_error['productName_'.$value]);	
		}
		foreach($langIdArr as $key=>$value) {
			$arr_error['productDesc_'.$value]=$obj->fnGetErr($arr_error['productDesc_'.$value]);	
		}
		$arr_error[productPrice]=$obj->fnGetErr($arr_error[productPrice]);		
		$arr_error[productWeight]=$obj->fnGetErr($arr_error[productWeight]);		
		
		#------check ASP id Exists----------

		if($prodObj->isProductAspIdExist($_POST['aspId'],'')){ 
			$arr_error['aspId'] = "ASP Id already taken or used.";
			$str_validate=0;				
		}

		
		// Check Product Name Exist============================
		foreach($langIdArr as $key=>$value) {
			if($prodObj->isProductNameExist($_POST['productName_'.$value],$value)){ 
				$arr_error['productName_'.$value] = "Product already exist. ";
				$str_validate=0;				
			}
		}		
		// Check Image Extension==================================
		foreach($_FILES as $key=>$fiesArr){		
			if($_FILES[$key]['name']){
				$filename = stripslashes($_FILES[$key]['name']);
				$extension = findexts($filename);
				$extension = strtolower($extension);
		
				if(!$prodObj->checkExtensions($extension)){ 
					$arr_error[$key] = "Upload Only ".$prodObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'") ." image extension.";
					$str_validate=0;			
				}
			}
		}		
		//Submit if validation pass : $str_validate is 1=============================================
		if($str_validate){
			$_POST = postwithoutspace($_POST);
			$prodObj->addRecord($_POST,$_FILES);
		}
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>
<script language="javascript" src="js/ajax.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->

<script type="text/javascript">
function hrefBack1(){
	window.location = 'manageProducts.php';
}
</script>

</head>
<body>
	<? include('includes/header.php'); ?>
	<div id="nav-under-bg"><!-- --></div>
	<form name="frmUser" id="frmUser" method="post" enctype="multipart/form-data" onsubmit="javascript: return validateFrm(this);">		
		<div class="main-body-div-new">
			<div class="main-body-div-header">Add Product</div>
			<!-- left position -->
			<div class="main-body-div4" id="mainDiv">
				<div class="add-main-body-left-new" >
					<ul>
						<li class="add-main-body-left-new-text" style="clear:both; width:500px;padding-bottom:5px;" >
						<span class="small_error_message"><?=$_SESSION['SESS_MSG']?></span>
						</li>			
						<!-- Device:Category =========================-->
						<li class="lable">Category <span class="spancolor">*</span></li>
						<li class="lable2" >
						<select name="cat" id="cat" onchange="getSubCatByCatId(this.value);">
						<?=$genObj->getCategoryList();?>
						</select>
						</li>					
						
						<!-- Company : SubCategory =====================-->
						
						<li class="lable">Sub Category <span class="spancolor">*</span></li>
						<li class="lable2" >
						<select name="subCat" id="subCat" >
						<?	
						$genObj = new GeneralFunctions();
						$firstCat=$genObj->getFirstCat();
						echo $genObj->subCategoryList($firstCat);
						?>
						</select>
						<span id="subCatSPAN"></span>
						</li>  					
						
						<!-- ASP ID  ================================= -->			
						<li class="lable">Product ASP ID <span class="spancolor">*</span></li>
						<li>
						<input type="text" name="aspId" class="wel" value="<?=stripslashes($_POST['aspId'])?>" maxlength="8" />
						<p style="padding-left:150px;"><?=$arr_error[aspId]?></p>
						</li>
						<!-- Product Name ================================= -->			
						<li class="lable">Product Name <span class="spancolor">*</span></li>
						<?
						echo
						$genObj->getLanguageTextBox('productName','m__productName',$arr_error);
						//1->type,2->name,3->id
						?>
						<!-- Product Price ================================= -->
						<li class="lable">Product Price <span class="spancolor">*</span></li>
						<li  >
						<input type="text" name="productPrice" id="m__Product_Price" class="wel" value="<?=stripslashes($_POST['productPrice'])?>" maxlength="8" onkeyup="return isNum12(this.value);" />
						<p style="padding-left:150px;" id="price"><?=$arr_error[productPrice]?></p>
						</li>	
						<!-- Product Weight ================================= -->
						<li class="lable">Product Weight <span class="spancolor">*</span></li>
						<li>
						<input type="text" name="productWeight" id="m__Product_Weight" class="wel" value="<?=stripslashes($_POST['productWeight'])?>" maxlength="8" onkeyup="return isNum123(this.value);"   />
						<p style="padding-left:150px;" id="weight"><?=$arr_error[productWeight]?></p>
						</li>
						<!-- Product Quantity ================================= -->
						<li class="lable">Product Quantity </li>
						<li>
						<input type="text" name="quantity" id="m__quantity" class="wel" value="<?=stripslashes($_POST[quantity])?>"  />
						<p style="padding-left:150px;" id="price"><?=$arr_error[quantity]?></p>
						</li>	
						<!-- Product Description ================================= -->
						<li class="lable">Product Description <span class="spancolor">*</span></li>
						<?
						echo $genObj->getLanguageTextarea('productDesc','m__Product_Description',$arr_error); //1->type,2->name,3->id
						?>											
						<br />
						<!-- Product Image ================================= -->			
						<li class="lable" >Product image  <span class="spancolor">*</span></li>
						<li>
						<input type="file" id="m__product_image" name="productImage">
						<p  style="padding-left:150px;"><?= $arr_error[productImage];?></p>
						</li>
						<!-- Product Orientation ================================= 
						<li class="lable" >Product Orientation<span class="spancolor">*</span></li>
						<li style="margin-top:10px;">
						<select name="orientation" id="m__product_orientation">
						<?=$genObj->getOrientationList()?>
						</select>
						</li>	-->	
                                                
                                                <!-- Product View Image ================================= -->	
						<li class="lable" >Product view image <span class="spancolor">*</span></li>
						<li>
						<?php $viewResult = $prodObj->getViewImageUpload(); ?>
						<table border="0">
						<?php 
						$i = 1;
						foreach($viewResult as $value){
						$sel = ($i == 1)?"checked":"";
						extract($value);
						?>
						<tr>
						<td><?php echo $view; ?></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						
						<tr>
						<td>&nbsp;</td>
						<td>Outline Image</td>
						<td>
						<div>
						<input type="file" id="<?php echo $imgWithBorderId;?>" name="<?php echo $imgWithBorderName;?>">
						<p  style="padding-left:0px;">
						<?=$arr_error["prodImageWithBorder".$id.""]?>
						</p>
						</div>
						</td>
						</tr>
						
						<tr>
						<td>&nbsp;</td>
						<td>Background Image</td>
						<td>
						<div>
						<input type="file" id="<?php echo $imgWithoutBorderId;?>" name="<?php echo $imgWithoutBorderName;?>">
						<p  style="padding-left:0px;">
						<?=$arr_error["prodImageWithoutBorder".$id.""]?></p>
						</div>
						</td>						
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td>Production Image</td>
						<td>
						<div>
						<input type="file" id="<?php echo $productionImgName;?>" name="<?php echo $productionImgName;?>">
						<p  style="padding-left:0px;">
						<?=$arr_error["productionImg".$id.""]?></p>
						</div>
						</td>						
						</tr>
						
						<tr>
						<td>&nbsp;</td>
						<td>Make this Default&nbsp;</td>
						<td><input type="radio" name="isDefault" value="<?php echo $id;?>" <?php echo $sel;?> ></td>
						</tr>
                                                
<!--                                                <td>&nbsp;</td>
                                                <td>Wall Sticker</td>
                                                <td>
                                                <div>
                                                <input type="checkbox" id="wall_<?=$id?>" name="wall_<?=$id?>" onclick=showonoff(<?=$id?>);>							
                                                </div>
                                                </td>
						</tr>-->
                                                    
                                                <tr id="print_<?=$id?>" style="display: none;">
                                                <td>&nbsp;</td>
                                                <td>Print Size</td>
                                                <td>
                                                <div>
                                                <input type="text" size="3" name="<?=$printWidth;?>" id="<?=$printWidthId;?>"> X <input type="text" size="3" name="<?=$printHeight;?>" id="<?=$printHeightId;?>">in Inches							
                                                </div>
                                                </td>
						</tr>
                                                    
						<?php $i++; }?>
						</table>
						</li>
					</ul>
				</div>
				<div class="main-body-sub">
					<input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
					&nbsp;
					<input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
				</div>
			</div>
		</div>
	</form>
</body>
</html>
<? unset($_SESSION['SESS_MSG']); ?>

<!-- Script Section ================================================ -->

<script type="text/javascript" >
    function showonoff(id){
        if(document.getElementById("wall_"+id).checked == true){
            document.getElementById("print_"+id).style.display = '';	 
        }else{
            document.getElementById("print_"+id).style.display = 'none';	 
        }
    }
    function checkAllcheckbox(fid,sid){
        if(document.getElementById(fid+"menuCheck").checked == true){
            for (var i = 0; i < sid; i++) {
                document.getElementById(fid+"mCheck"+i).checked = true;	
            }
        }else{
            for (var i = 0; i < sid; i++) {
                document.getElementById(fid+"mCheck"+i).checked = false;	
            }
        }
    }
    
    function selectAllBox(){
        var aid = document.getElementById("atid").value;
        var aaid = document.getElementById("atvid").value;
        var spaid = aid.split(",");
        var sppaid = aaid.split(",");
        //alert(sppaid.length);
        if(document.getElementById("selectval").checked == true){
            for(k=0;k<spaid.length;k++){
                document.getElementById(spaid[k]+"menuCheck").checked = true;
                for(l=0;l<sppaid[k];l++){
                    document.getElementById(spaid[k]+"mCheck"+l).checked = true;
                }
            }
        }else{
            for(i=0;i<sppaid.length;i++){
                document.getElementById(spaid[i]+"menuCheck").checked = false;
                for(l=0;l<sppaid[i];l++){
                    document.getElementById(spaid[i]+"mCheck"+l).checked = false;
                }
            }
        }
    }
</script>
