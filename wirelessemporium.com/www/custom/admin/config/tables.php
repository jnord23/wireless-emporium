<?php
// Prefix============
define('PREFIX', 'sd_'); 
// BASIC TABLES===============================
define('TBL_ADMINLOGIN', PREFIX.'adminlogin'); 
define('TBL_ADMINPERMISSION', PREFIX.'adminpermission');
define('TBL_ADMINLEVEL', PREFIX.'adminlevel');
define('TBL_MENU', PREFIX.'menu'); 
define('TBL_SYSTEMCONFIG', PREFIX.'system_config'); 
define('TBL_LOGDETAIL', PREFIX.'logdetail');
define('TBL_COMMITLOG', PREFIX.'commitlog');
define('TBL_SESSIONDETAIL', PREFIX.'sessiondetail');
define('TBL_LANGUAGE', PREFIX.'language'); 
define('TBL_LANGATTRIBUTE', PREFIX.'languageattribute'); 


// Categoris=================================
define('TBL_CATEGORY', PREFIX.'category'); 
define('TBL_CATEGORY_DESCRIPTION', PREFIX.'categorydesc'); 
define('TBL_DESIGNCATEGORY', PREFIX.'design_category'); 
define('TBL_DESIGNCATEGORY_DESCRIPTION', PREFIX.'design_categorydesc'); 
define('TBL_FONTCATEGORY', PREFIX.'font_category'); 
define('TBL_FONTCATEGORY_DESCRIPTION', PREFIX.'font_categorydesc'); 

// Font===========================================
define('TBL_FONT', PREFIX.'font');
define('TBL_FONTDESC', PREFIX.'fontdesc');

// RAW PRODUCT================================================
define('TBL_PRODUCT', PREFIX.'product');
define('TBL_PRODUCT_DESCRIPTION', PREFIX.'product_description');
define('TBL_PRODUCT_VIEW', PREFIX.'product_view');
define('TBL_PRODUCT_COLOR_DESCRIPTION', PREFIX.'product_color_desc');
define('TBL_PRODUCT_COLOR', PREFIX.'product_color');
define('TBL_PRODUCT_ATTRIBUTE', PREFIX.'product_attribute');
define('TBL_PRODUCT_QUANTITY', PREFIX.'product_minmax');

//Assign Main Product======================================
define('TBL_ADMIN_DESIGNPRODUCT', PREFIX.'admin_designproduct'); 
define('TBL_ADMIN_PRODUCT_VIEW', PREFIX.'admin_product_view'); 


//Main Product===============================================
define('TBL_MAINPRODUCT', PREFIX.'mainproduct');
define('TBL_MAIN_PRODUCT_DESCRIPTION', PREFIX.'main_product_description');
define('TBL_MAIN_PRODUCT_VIEW', PREFIX.'main_product_view');

//define('TBL_MAIN_PRODUCT_ATTRIBUTE', PREFIX.'main_product_attribute');
//define('TBL_MAIN_PRODUCT_COLOR', PREFIX.'main_product_color');
//define('TBL_MAIN_PRODUCT_COLOR_DESCRIPTION', PREFIX.'main_product_color_desc');

// Attributes==================
define('TBL_ATTRIBUTE', PREFIX.'attribute');
define('TBL_ATTRIBUTE_DESCRIPTION', PREFIX.'attributedesc'); 
define('TBL_ATTRIBUTE_VALUES', PREFIX.'attribute_values');
define('TBL_ATTRIBUTE_VALUES_DESCRIPTION', PREFIX.'attributevaluesdesc');
// View========================
define('TBL_VIEW', PREFIX.'view');
define('TBL_VIEWDESC', PREFIX.'viewdesc');

//Size=========================
define('TBL_SIZE', PREFIX.'size');
define('TBL_SIZEDESC', PREFIX.'sizedesc');
define('TBL_SIZEGROUP', PREFIX.'size_group');
define('TBL_SIZEGROUPDESC', PREFIX.'sizegroupdesc');
define('TBL_SIZEPRICE', PREFIX.'size_price');
define('TBL_SIZEATTRIBUTE', PREFIX.'size_attribute');
define('TBL_MESURMENT', PREFIX.'mesurment');


// Color==================
define('TBL_COLOR', PREFIX.'color');
define('TBL_COLORDESC', PREFIX.'colordesc');

// Tool===========
define('TBL_TOOL', PREFIX.'tool');
define('TBL_TOOLDESC', PREFIX.'tooldesc');

// Currency================
define('TBL_CURRENCY_DETAIL', PREFIX.'currency_detail');
define('TBL_CURRENCY', PREFIX.'currency');
// Currency================
define('TBL_ORIENTATION', PREFIX.'orientation');
define('TBL_ORIENTATION_DESC', PREFIX.'orientation_desc');

//Design======================================
define('TBL_DESIGN', PREFIX.'design');
define('TBL_DESIGN_DESC', PREFIX.'design_desc'); 

//Upload===========================================
define('TBL_UPLOAD', PREFIX.'upload'); 

//Basket==================================
define('TBL_BASKET', PREFIX.'basket');



















//===========old=========================================================
/*
define('TBL_LANGATTRIBUTE', PREFIX.'languageattribute'); 
define('TBL_LANGATTRIBUTE_NEW', PREFIX.'languageattribute_New'); 

 
define('TBL_MAILTYPE', PREFIX.'mail_type'); 
define('TBL_MAILSETTING', PREFIX.'mail_setting'); 


define('TBL_NEWSLETTER', PREFIX.'newsletter');
define('TBL_NEWSLETTERTBL_USERTYPE', PREFIX.'newsletterusertype');
define('TBL_NEWSLETTERSUBSCRIBER', PREFIX.'newslettersubscriber');
define('TBL_NEWSLETTERTEMPLATE', PREFIX.'newsletter_template');  
define('TBL_BACKUP', PREFIX.'backup'); 
define('TBL_BANNER', PREFIX.'banner'); 
define('TBL_BANNER_DESCRIPTION', PREFIX.'bannerdesc'); 
define('TBL_BANNER_HISTORY', PREFIX.'banner_history'); 
define('TBL_USER', PREFIX.'user');
define('TBL_GIFTCERTIFICATE', PREFIX.'giftcertificate');
define('TBL_GIFTCERTIFICATEDESC', PREFIX.'giftcertificatedesc');
define('TBL_GIFT', PREFIX.'gift');
define('TBL_METADATA', PREFIX.'metadata');
define('TBL_METADATADESC', PREFIX.'metadatadesc');

define('TBL_STATICPAGETYPE', PREFIX.'staticpagetype');
define('TBL_STATICPAGESETTING', PREFIX.'staticpage_setting');
define('TBL_COUPON', PREFIX.'coupon'); 
define('TBL_COUPON_DESCRIPTION', PREFIX.'coupondesc');
define('TBL_COUPON_TO_PRODUCT', PREFIX.'coupon_to_product'); 
define('TBL_COUPON_TO_CUSTOMER', PREFIX.'coupon_to_customer');
define('TBL_CONTACTUSTYPE', PREFIX.'contactustype');
define('TBL_CONTACTUS', PREFIX.'contactus');
//define('TBL_SIZE', PREFIX.'product_size');
define('TBL_ZONE', PREFIX.'zone');
define('TBL_ZONE_DESCRIPTION', PREFIX.'zonedesc'); 
define('TBL_COUNTRY', PREFIX.'country');
define('TBL_COUNTRY_DESCRIPTION', PREFIX.'countrydesc'); 
define('TBL_STATE', PREFIX.'state');
define('TBL_STATE_DESCRIPTION', PREFIX.'statedesc');
define('TBL_COUNTY', PREFIX.'county');
define('TBL_COUNTY_DESCRIPTION', PREFIX.'countydesc'); 
define('TBL_USERTITLE', PREFIX.'usertitle');

define('TBL_ATTRIBUTE', PREFIX.'attribute');
define('TBL_ATTRIBUTE_DESCRIPTION', PREFIX.'attributedesc'); 
define('TBL_ATTRIBUTE_VALUES', PREFIX.'attribute_values');
define('TBL_ATTRIBUTE_VALUES_DESCRIPTION', PREFIX.'attributevaluesdesc');
define('TBL_SIGNUPFIELDS', PREFIX.'registrationfields');
define('TBL_SIGNUPFIELDS_DESCRIPTION', PREFIX.'registrationfieldsdesc'); 
define('TBL_BILLINGFIELDS', PREFIX.'billingfields');
define('TBL_BILLINGFIELDS_DESCRIPTION', PREFIX.'billingfieldsdesc'); 
define('TBL_TESTIMONIALFIELDS', PREFIX.'testimonialfields');
define('TBL_TESTIMONIALFIELDS_DESCRIPTION', PREFIX.'testimonialfieldsdesc'); 
define('TBL_TESTIMONIALS_DESC', PREFIX.'testimonialdesc');
define('TBL_TESTIMONIALS', PREFIX.'testimonials');
define('TBL_EMAILSCHEDULE', PREFIX.'email_schedule');
define('TBL_FORUMTOPIC', PREFIX.'forum_topic');
define('TBL_FORUMTOPICDESC', PREFIX.'forum_topicdesc');
define('TBL_FORUMTHREAD', PREFIX.'forum_thread');
define('TBL_FORUMTHREADDESC', PREFIX.'forum_threaddesc');
define('TBL_FORUMPOST', PREFIX.'forum_post');
define('TBL_FORUMPOSTDESC', PREFIX.'forum_postdesc');
define('TBL_COUNTYDESC', PREFIX.'countydesc');
define('TBL_ORDER', PREFIX.'orders');
define('TBL_ORDERDETAIL', PREFIX.'orderdetail');
define('TBL_ORDERDETAIL_ATTRIBUTES', PREFIX.'orderdetail_attributes');


define('TBL_LANGUAGE_FONT', PREFIX.'languagefont');
define('TBL_LANGUAGE_FONTDESC', PREFIX.'languagefontdesc');

define('TBL_VIEW', PREFIX.'view');
define('TBL_VIEWDESC', PREFIX.'viewdesc');


define('TBL_TITLE', PREFIX.'titlename');



define('TBL_MAINSIZEPRICE', PREFIX.'mainsize_price');
define('TBL_MAINSIZEATTRIBUTE', PREFIX.'mainsize_attribute');
define('TBL_MAINMESURMENT', PREFIX.'mainmesurment');

define('TBL_DESIGNDETAIL', PREFIX.'design_detail');
define('TBL_DESIGNDETAILDESC', PREFIX.'design_detail_desc');


define('TBL_SIZEATTRIBUTEVALUE', PREFIX.'size_attribute_value');

define('TBL_DESIGN', PREFIX.'design');
//define('TBL_DESIGNDETAIL', PREFIX.'design_detail'); 
define('TBL_USERIMAGE', PREFIX.'userimage');
define('TBL_ORDERSTATUS', PREFIX.'orderstatus');
define('TBL_ORDERSTATUSDESC', PREFIX.'orderstatusdesc');
define('TBL_DESIGNCOUNT', PREFIX.'designcount');
define('TBL_SAVEASSIGNHOTDESIGN', PREFIX.'save_assign_hot_design');
define('TBL_UPLOADPDF', PREFIX.'pdf');

define('TBL_SHIPPING_METHOD', PREFIX.'shipping_methods');
define('TBL_SHIPPING_METHOD_DESC', PREFIX.'shipping_methods_desc');

define('TBL_COUNTRY_SHIPPING', PREFIX.'country_shipping');


//// use for save Admin create design****************************
define('TBL_ADMIN_DESIGNPRODUCT', PREFIX.'admin_designproduct'); 
//define('TBL_ADMIN_DESIGNPRODUCT_DESC', PREFIX.'admin_designproduct_desc');
//define('TBL_ADMIN_PRODUCT_COLOR', PREFIX.'admin_product_color'); 
//define('TBL_ADMIN_PRODUCT_COLOR_DESC', PREFIX.'admin_product_color_desc'); 
define('TBL_ADMIN_PRODUCT_VIEW', PREFIX.'admin_product_view'); 
define('TBL_HOTDESIGN_DETAIL', PREFIX.'hotdesign_detail');
define('TBL_BASKET', PREFIX.'basket');


define('TBL_MAIN_PRODUCT_QUANTITY', PREFIX.'main_product_minmax');



/////////  Shipping Range ///////////

define('TBL_SHIPPING', PREFIX.'shipping');
define('TBL_SHIPPING_DESC', PREFIX.'shipping_desc');
define('TBL_RANGE_QUANTITY', PREFIX.'range_minmax');
define('TBL_COUNTRY_SHIPPING_PRICE', PREFIX.'countryshippingprice');


///////////  Clip Art //////////

define('TBL_CLIPARTDETAIL', PREFIX.'clipArtDetail');

/////////// Brand //////////////////

define('TBL_BRAND', PREFIX.'brand');
///////////// BULK ORDER /////////////////////////
define('TBL_BULKORDER', PREFIX.'bulk_order');
define('TBL_BULKORDERTYPE', PREFIX.'bulkordertype');
define('TBL_BULKORDER_PRODUCT', PREFIX.'bulk_order_product');
///////////////Hear about us ///////////////
define('TBL_HEARABOUTUS', PREFIX.'hearaboutus');

*/


//******************
?>