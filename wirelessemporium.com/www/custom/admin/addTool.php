<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageTool.php","add_record");
/*---Basic for Each Page Ends----*/
$toolObj = new Tool();
$genObj = new GeneralFunctions();

if(isset($_POST['submit'])) {
        $_POST = postwithoutspace($_POST);//this to stop sql injection
        require_once('validation_class.php');
        $obj = new validationclass();
        $errorArr = 0;
        $rst = $toolObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
        $num = $toolObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $toolObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		
                //Add Error==============================
		$obj->fnAdd('VariableName',$_POST['VariableName'], 'req', 'Please Enter Variable.');
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('toolName_'.$value,$_POST['toolName_'.$value], 'req', 'Please enter Name.');			
		}
		
                //Validate=============================
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
               
                //Get Error=======================
		$arr_error['VariableName']=$obj->fnGetErr($arr_error[VariableName]);
		foreach($langIdArr as $key=>$value) {
			$arr_error['toolName_'.$value]=$obj->fnGetErr($arr_error['toolName_'.$value]);                       
		}
		
                //Check if tool variable exists=======================                      
                if($toolObj->isToolVariableExists($_POST['VariableName'])){
                    $arr_error['VariableName']="Tool Variable Already Exists";
                    $str_validate=0;
                }
                              
		if($str_validate){			
			$toolObj->addRecord($_POST);
		}
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<script type="text/javascript">
	function hrefBack1(){
		window.location='manageTool.php';
	}
</script>

<!-- New Drop Down menu -->
</head>
<body>
    <? include('includes/header.php'); ?>
    <div id="nav-under-bg"><!-- --></div>
    <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
        <div class="main-body-div-new">
            <div class="main-body-div-header">Add Tool</div>      
            <div class="main-body-div4" id="mainDiv">
                <div class="add-main-body-left-new" >
                    <ul>
                        <li class="add-main-body-left-new-text" style="clear:both; width:500px; padding-bottom:5px;" >
                            <span class="small_error_message"><?=$_SESSION['SESS_MSG']?></span>
                        </li>	
                        <!-- Variable  --->
                        <li class="lable">Variable <span class="spancolor">*</span></li>
                        <li class="lable2">
                            <input type="text" name="VariableName" value="" id="m__Variable" />
                            <p style="padding-left:150px;"><?=$arr_error[VariableName]?></p>
                        </li>


                         <!-- Name  --->
                        <li class="lable">Name <span class="spancolor">*</span></li>
                        <?=$genObj->getLanguageTextarea('toolName','m__Name',$arr_error);?> 
                    </ul>
                </div>
                <div class="main-body-sub">
                    <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
                    &nbsp;
                    <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
<? unset($_SESSION['SESS_MSG']); ?>