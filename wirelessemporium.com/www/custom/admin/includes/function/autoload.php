<?php
function __autoload($class_name) {	
     $file =  'includes/classes/'.$class_name . '.php';
	//echo  $file;
	if(file_exists($file)) 
	require_once($file);
	else
	die(" $file File Not Found");
}



function redirect($page)
{	 
	if(!headers_sent())
		header("location:$page");
	else
		echo "<script>window.location.href='$page'</script>";
}


function unsets($post,$unset_value){
	if(!is_array($unset_value)){$temp=$unset_value;$unset_value=array($temp);}
	if(!empty($post) && !empty($unset_value)){
		foreach($unset_value as $val){
			if(isset($post[$val])){
				unset($post[$val]);
			}
		}
	}
	return $post;
}



function postwithoutspace($post)
{
   foreach($post as $key=>$value)
	{
	  if(is_array($value)){
	    //print_r($value);
	  	foreach($value as $key1 => $value1){
			$value1= htmlspecialchars(mysql_real_escape_string(trim($value1)));
	  		$post[$key1]= $value1;	
		}	  
	  }else{
	  	$value= htmlspecialchars(mysql_real_escape_string(trim($value)));
	  	$post[$key]= $value;
	  }
	}
	return  $post;
}
	
	
function displayWithStripslashes($post)
{
   foreach($post as $key=>$value)
	{
	  $value=stripslashes(trim($value));
	  $post[$key]= $value;
	}
	return  $post;
}
		
	
function datediff($date1,$date2,$format='d'){
    $difference = abs(strtotime($date2) - strtotime($date1));
		switch (strtolower($format)){
			case 'd':
			$days = round((($difference/60)/60)/24,0);
			break;
			case 'm':
			$days = round(((($difference/60)/60)/24)/30,0);
			break;
			case 'y':
			$days = round(((($difference/60)/60)/24)/365,0);
			break;
		}
	return $days;
}

 
function msgSuccessFail($type,$msg){
	if($type == 'fail'){
		$preTable = "<table class='Error-Msg' align='center' cellpadding='0' cellspacing='0'><tr><td width='410px' align='center' valign='middle' height='15'><img src='images/error.png' width='16' border='0' height='16'>&nbsp;&nbsp;&nbsp;$msg</td></tr></table>";
	} elseif($type == 'success') {
		$preTable = "<table class='Success-Msg' align='center' cellpadding='0' cellspacing='0'><tr><td width='410px' align='center' height='15'><img src='images/done.gif' width='16' border='0' height='16'>&nbsp;&nbsp;&nbsp;$msg</td></tr></table>";
	}
	return $preTable;
}


function orderBy($pageurl,$orderby,$displayTitle){
	if($_GET[order] == 'ASC') { 
	$order = "DESC";}
	else{ $order = "ASC";}
	if(($_GET[orderby] == $orderby)&&($_GET[order] == 'ASC')){
	$img = "<img src='images/orderup.png' border='0' />";
	}
	elseif(($_GET[orderby] == $orderby)&&($_GET[order] == 'DESC')){
	$img = "<img src='images/orderdown.png'  border='0' />";
	}else{
	$img = "";
	}
	$explodedlink = explode("?",$pageurl);
	if($explodedlink[1]){
	$display = "<a href='$pageurl&orderby=$orderby&order=$order&page=$_GET[page]&limit=$_GET[limit]' class='order'>$displayTitle </a> $img";
	}else{
	$display = "<a href='$pageurl?orderby=$orderby&order=$order&page=$_GET[page]&limit=$_GET[limit]' class='order'>$displayTitle</a> $img";
	}
	echo $display; 
}

function findexts($filename) {
  $filename = strtolower($filename) ;
  $exts = @split("[/\\.]", $filename) ;
  $n = count($exts)-1;
  $exts = $exts[$n];
  return $exts;
}

function getPageName() {	
	$pageName   = basename($_SERVER['PHP_SELF']);

	if ($pageName=='')
		$pageName = "index.php";
	return $pageName;	
}

function getImagePath($mainImage,$mainWidth,$mainHeight,$noImage,$noImageWidth,$noImageHeight) {
    
	$imageDetail = array();
	if(file_exists($mainImage)) {
		array_push($imageDetail,$mainImage);
		array_push($imageDetail,$mainWidth);
		array_push($imageDetail,$mainHeight);						
	} elseif(!file_exists($mainImage)) {
		array_push($imageDetail,$noImage);
		array_push($imageDetail,$noImageWidth);
		array_push($imageDetail,$noImageHeight);							
	}	
	return $imageDetail;
}

function _image($mainImageAbsolutePath,$mainImagePath,$noimagePath) {	
	if(file_exists($mainImageAbsolutePath)) {
		$image=$mainImagePath;					
	}else{
		$image=$noimagePath;							
	}	
	return $image;
}
//Unlink ===============================
function _unlink($filePath){
	if(file_exists($filePath)){
		unlink($filePath);
	}
}
//Unlink file arr===========================
function _unlinkArr($filePathArr){
	foreach($filePathArr as $key=>$val){
		if(file_exists($val)){
			unlink($val);
		}
	}
}

//Start : Create Directory
        function createDirectory($dirName){    
            if(!is_dir($dirName)) {
                @mkdir($dirName);
                @chmod($dirName,0777);
            }
        } 

if($_SESSION[DEFAULTCURRENCYID] == ''){
	$_SESSION[DEFAULTCURRENCYID]=1;
}



?>