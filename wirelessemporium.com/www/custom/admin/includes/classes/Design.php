<?php
session_start();
class Design extends MySqlDriver{
    var $vars;
    function __construct($id = NULL) {
        $this->obj = new MySqlDriver;
	$this->id = $id;       
    }
//Start : Show Detail in Grid===========================================================================================
    function valDetail() {
		$cond = " AND D.isDeleted = '0' AND DD.langId = '".$_SESSION['DEFAULTLANGUAGE']."' AND DCD.langId='".$_SESSION['DEFAULTLANGUAGE']."'";
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){			
			$searchtxt = addslashes($_REQUEST['searchtxt']);
			$cond .= " AND (DD.designName LIKE '%$searchtxt%')";
			stripslashes($_REQUEST['searchtxt']);	
		}
                
                if($_REQUEST['cid']){
                    $cond.=" AND D.catId='".$_REQUEST['cid']."'";
                }
                $query="SELECT D.*,DD.designName,DCD.categoryName from ".TBL_DESIGN." as D INNER JOIN ".TBL_DESIGN_DESC." AS DD ON (D.id=DD.designId) inner join ".TBL_DESIGNCATEGORY_DESCRIPTION." AS DCD ON (DCD.catId=D.catId) WHERE 1  $cond";
                $sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {			
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------                       
                        $orderby = $_GET[orderby]? $_GET[orderby]:"D.addDate";                        
                        $order = $_GET[order]? $_GET[order]:"DESC";   
                        $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
                       
                        //$query .=  " LIMIT ".$offset.", ". $recordsPerPage;
									
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			if($row > 0) {			
				$i = 1;			
				$genTable .= '<div class="column" id="column1">';		
				while($line = $this->getResultObject($rst)) {
					$currentOrder	.=	$line->id.",";
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					$status=($line->status)?"Active":"Inactive";						
					
					$genTable .= '<div class="'.$highlight.'" id="'.$line->id.'">
								 <ul>
								 	<li style="width:20px;">&nbsp;&nbsp;<input name="chk[]" value="'.$line->id.'" type="checkbox" style="margin-left:-10px;"></li>
									<li style="width:60px;">'.$i.'</li>
									<li style="width:150px;">'.stripslashes($this->fetchValue(TBL_DESIGNCATEGORY_DESCRIPTION,'categoryName',"catId='".$line->catId."' and langId='".$_SESSION['DEFAULTLANGUAGE']."'")).'</li>
									<li style="width:140px;">'.stripslashes($line->designName).'</li>
									<li style="width:140px;"><img src="'.__DESIGNTHUMBPATH__.''.$line->designImage.'"></li>';
									
					$genTable .= '<li style="width:70px;">';
					if($menuObj->checkEditPermission()) 							
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'design\')">'.$status.'</div>';
									
																											
					$genTable .= '</li>
					<li style="width:40px;"><a rel="shadowbox;width=705;height=625" title="'.$productName.'" href="viewDesign.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></li>
					<li style="width:40px;">';

									
					if($menuObj->checkEditPermission())				
						$genTable .= '<a href="editDesign.php?id='.base64_encode($line->id).'&page='.$page.'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a>';
						
					$genTable .= '</li><li>';
				
					if($menuObj->checkDeletePermission())
						$genTable .= "<a href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=design&type=delete&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					
							
					$genTable .= '</li></ul></div>';
				
		
					$i++;	
				}
				$genTable .= '</div>';
				$genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" /></div>';
		
				switch($recordsPerPage)
				{
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='10' $sel1>10</option>
					<option value='20' $sel2>20</option>
					<option value='30' $sel3>30</option> 
					<option value='".$totalrecords."' $sel4>All</option>  
					  </select> Records Per Page
				</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}

//Start : Check Design Name Existance================================================================
    function isDesignNameExist($designName,$langId,$id=NULL){
        $designName = addslashes(trim($designName));
        $query="Select D.isDeleted,DD.* from ".TBL_DESIGN." as D inner join ".TBL_DESIGN_DESC." as DD on (D.id=DD.designId) where D.id!='".$id."' and DD.langId='".$langId."' and DD.designName='".$designName."' and D.isDeleted='0'";
        $rst = $this->executeQry($query);
        $row = $this->getTotalRow($rst);
        return ($row >0)? "1":"0";
    }
//Start : Add Record=========================================================================
    function addRecord(){
        //echo "<pre>"; print_r($_POST); print_r($_FILES); echo"</pre>";exit;
	$_SESSION['SESS_MSG'] = "";
       
	if($_SESSION['SESS_MSG'] == ""){		
             //Upload Image =========================================
            if($_FILES['designImage']['name']){
                    $filename = stripslashes($_FILES['designImage']['name']);
                    $extension = strtolower(findexts($filename));                
                    $image_name = date("Ymdhis").time().rand().'.'.$extension;
                    $target    = __DESIGNORIGINAL__.$image_name;               
                    if($this->checkExtensions($extension,"DESIGNS_EXTENSION")) {                          
                            $filestatus = 	move_uploaded_file($_FILES['designImage']['tmp_name'], $target);                           
                            @chmod($target, 0777);
                            if($filestatus){                                
                                if(strtolower($extension)=='eps'){                                  
                                    //create png from eps
                                    $eps=$image_name;
                                    $image_name  = str_replace('.eps' , '.png' , $image_name);
                                    $imgSource = $target;
                                    $pngTargetPath=__DESIGNORIGINAL__.$image_name;
                                    //Convert .AI File Into .PNG FILE
                                    exec(IMAGEMAGICPATH." -density 300 -channel RGBA -colorspace RGB -background none -fill none -dither None $imgSource $pngTargetPath");
                                    $imgSource=$pngTargetPath;
                                }else{                                   
                                     $imgSource=$target;
                                     $eps="";
                                }	 
                                // get image ratio ...........................
                                list($width,$height)=getimagesize($imgSource);
                                $largeThumbW = 1000;
                                $largeThumbH = 1000;
                                if($width>$largeThumbW && $height>$largeThumbH){   
                                    $largetImageSize = $largeThumbW.'x'.$largeThumbH;
                                }else if($width<$largeThumbW && $height>$largeThumbH){   
                                    $largetImageSize = 'x'.$largeThumbH;
                                }else if($width>$largeThumbW && $height<$largeThumbH){   
                                    $largetImageSize = $largeThumbW.'x';
                                }else{
                                   $largetImageSize = $width.'x'.$height; 
                                }
                                $thumb = __DESIGNTHUMB__.$image_name;
                                $large = __DESIGNLARGE__.$image_name;
                                @chmod(__DESIGNTHUMB__,0777);
                                @chmod(__DESIGNLARGE__,0777); 
                                $thumbSize = $this->findSize('DESIGNS_THUMB_WIDTH','DESIGNS_THUMB_HEIGHT',100,100);                               
                                exec(IMAGEMAGICPATH." $imgSource -thumbnail $thumbSize $thumb");
                                exec(IMAGEMAGICPATH." $imgSource -thumbnail $largetImageSize $large");
                            }
                    }
            }            
			
            $query = "insert into ".TBL_DESIGN." set catId = '".$_POST[designCategory]."', designImage = '".$image_name."',eps='".$eps."', status = '1', addDate = '".date('Y-m-d H:i:s')."', addBy = '".$_SESSION['ADMIN_ID']."'";
           
            $sql = $this->executeQry($query);
            $this->inserted_id = mysql_insert_id();
            
           
            //Update Design Desc Table=========================================				
            $rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
            $num = $this->getTotalRow($rst);           
            if($num){			
                    while($line = $this->getResultObject($rst)) {					
                            $designName = 'designName_'.$line->id;      
                            $query = "insert into ".TBL_DESIGN_DESC." set designId = '".$this->inserted_id."', langId ='".$line->id."', designName = '".$_POST[$designName]."'";	
                            if($this->executeQry($query)) 
                                    $this->logSuccessFail('1',$query);		
                            else 	
                                    $this->logSuccessFail('0',$query);
                    }	
            }
            $_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been added successfully");
	}
	header('Location:manageDesign.php');exit;	
    }

//Start : Get Result===================================================================
    function getResult($id) {
        $sql = $this->executeQry("select * from ".TBL_DESIGN." where id = '$id'");
        $num = $this->getTotalRow($sql);
        if($num > 0) {
                $line = $this->getResultRow($sql);
                $line['designImageThumb'] =__DESIGNTHUMBPATH__.$line['designImage'];	
        return $line;		
        } else {
                redirect("manageDesign.php");
        }	
    }
//Start:Edit Record==============================================================================    
    function editRecord(){	
        //echo"<pre>";print_r($_POST);print_r($_FILES);echo"</pre>";exit;	
	$_SESSION['SESS_MSG'] = "";		
            if($_SESSION['SESS_MSG'] == ""){               
                if($_FILES['designImage']['name']){
                        $filename = stripslashes($_FILES['designImage']['name']);
                        $extension = strtolower(findexts($filename));                               
                        $imagename = date("Ymdhis").time().rand().'.'.$extension;
                        $target    = __DESIGNORIGINAL__.$imagename;                               
                        if($this->checkExtensions($extension,"DESIGNS_EXTENSION")) {
                            $filestatus = move_uploaded_file($_FILES['designImage']['tmp_name'], $target);
                            @chmod($target, 0777);
                            if($filestatus){
                                if(strtolower($extension)=='eps'){
                                    //create png from eps                                   
                                    $eps=$imagename;
                                    $imagename  = str_replace('.eps' , '.png' , $imagename);
                                    $imgSource = $target;
                                    $pngTargetPath=__DESIGNORIGINAL__.$imagename;
                                    //Convert .AI File Into .PNG FILE
                                    $IMQry=IMAGEMAGICPATH." -density 300 -channel RGBA -colorspace RGB -background none -fill none -dither None $imgSource $pngTargetPath";
                                    //echo $IMQry;exit;
                                    exec($IMQry);
                                    $imgSource=$pngTargetPath;
                                    $preEPS=$this->fetchValue(TBL_DESIGN,'eps',"id='".$this->id."'");  
                                }else{
                                     $imgSource=$target;
                                }
                                 // get image ratio ...........................
                                list($width,$height)=getimagesize($imgSource);
                                $largeThumbW = 1000;
                                $largeThumbH = 1000;
                                if($width>$largeThumbW && $height>$largeThumbH){   
                                    $largetImageSize = $largeThumbW.'x'.$largeThumbH;
                                }else if($width<$largeThumbW && $height>$largeThumbH){   
                                    $largetImageSize = 'x'.$largeThumbH;
                                }else if($width>$largeThumbW && $height<$largeThumbH){   
                                    $largetImageSize = $largeThumbW.'x';
                                }else{
                                   $largetImageSize = $width.'x'.$height; 
                                }
                                
                                $thumb    = __DESIGNTHUMB__.$imagename;
                                $large	= __DESIGNLARGE__.$imagename;
                                @chmod(__DESIGNTHUMB__,0777);
                                @chmod(__DESIGNLARGE__,0777);                           
                                $thumbSize = $this->findSize('DESIGNS_THUMB_WIDTH','DESIGNS_THUMB_HEIGHT',100,100);                                
                                exec(IMAGEMAGICPATH." $imgSource -thumbnail $thumbSize $thumb");
                                exec(IMAGEMAGICPATH." $imgSource -thumbnail $largetImageSize $large");
                            }
                        }
                        //echo $filestatus;exit;
                        $preImg=$this->fetchValue(TBL_DESIGN,'designImage',"id='".$this->id."'");  
                        @unlink(__DESIGNORIGINAL__.$preEPS);
                        @unlink(__DESIGNORIGINAL__.$preImg);
                        @unlink(__DESIGNTHUMB__.$preImg);
                        @unlink(__DESIGNLARGE__.$preImg);
                        $query = "update ".TBL_DESIGN." set designImage='".$imagename."',eps='".$eps."', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '".$this->id."'";
                        $sql = $this->executeQry($query);
                }     
		   
                $query = "update ".TBL_DESIGN." set catId = '".$_POST[designCategory]."', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '".$this->id."'";
                $sql = $this->executeQry($query);
			
		//Update Desgin Desc==================================================				
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);                
		if($num){			
                    while($line = $this->getResultObject($rst)) {					
			$designName = 'designName_'.$line->id;	
                        $sql = $this->selectQry(TBL_DESIGN_DESC,'1 and designId = "'.$_POST[id].'" and langId = "'.$line->id.'"','','');
			$numrows = $this->getTotalRow($sql);
                        if($numrows == 0){
                            //$query = "insert into ".TBL_DESIGN_DESC." set designName = '".addslashes($_POST[$designName])."' where designId = '".$this->id."' AND langId = '".$line->id."'";	
                            $query = "insert into ".TBL_DESIGN_DESC." set designId = '$post[id]', langId = '".$line->id."', designName = '".$_POST[$designName]."'";

                            if($this->executeQry($query)) 
                                    $this->logSuccessFail('1',$query);		
                            else 	
                                    $this->logSuccessFail('0',$query);
                        }else{
                            $query = "update ".TBL_DESIGN_DESC." set designName = '".$_POST[$designName]."' where designId = '".$this->id."' AND langId = '".$line->id."'";	
                            if($this->executeQry($query)) 
                                    $this->logSuccessFail('1',$query);		
                            else 	
                                    $this->logSuccessFail('0',$query);
                        }
			
                    }	
		}
		
		$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been updated successfully");
            }
	header('Location:manageDesign.php');exit;	
    }
        
//Start : Chagne Value Status===================================================================    
    function changeValueStatus($get) {
        $status=$this->fetchValue(TBL_DESIGN,"status","1 and id = '$get[id]'");		
        if($status==1) {
                $stat= 0;
                $status="Inactive";
        } else 	{
                $stat= 1;
                $status="Active";
        }	
        $query = "update ".TBL_DESIGN." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
        if($this->executeQry($query)) 
                $this->logSuccessFail('1',$query);		
        else 	
                $this->logSuccessFail('0',$query);
        echo $status;		
    }
        
        
//Start : Delete Value===================================================================  
    function deleteValue($get) {		
        $this->executeQry("update ".TBL_DESIGN." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'");
        $_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageDesign.php?page=$get[page]&limit=$get[limit]';</script>";
    }

// Start : Multiple Action============================================================================       
    function deleteAllValues($post){
            if(($post[action] == '')){
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
                    echo "<script language=javascript>window.location.href='manageDesign.php?page=$post[page]&limit=$post[limit]';</script>";
                    exit;
            }	
            //Delete selected========================================
            if($post[action] == 'deleteselected'){
            $delres = $post[chk];
                $numrec = count($delres);
                if($numrec>0){
                    foreach($delres as $key => $val){                               	
                            $this->executeQry("update ".TBL_DESIGN." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$val'");
                    }
                    $_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
                }else{
                    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
                }
            }
            //Enable Selected===============================
            if($post[action] == 'enableall'){
                $delres = $post[chk];
                $numrec = count($delres);
                if($numrec>0){
                    foreach($delres as $key => $val){                       	
                            $sql="update ".TBL_DESIGN." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
                            $this->executeQry($sql);
                    }
                    $_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
                }else{
                    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
                }
            }
            //Disable Selected=============================================
            if($post[action] == 'disableall'){
                    $delres = $post[chk];
                    $numrec = count($delres);
                    if($numrec>0){
                        foreach($delres as $key => $val){                                
                                $sql="update ".TBL_DESIGN." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
                                mysql_query($sql);
                        }
                        $_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
                    }else{
                        $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
                    }
            }
            echo "<script language=javascript>window.location.href='manageDesign.php?cid=$post[cid]&page=$post[page]';</script>";
	}
        
        
        
        
       
        
        //////////////////////// old functions============== unused=================================================================
        /*
	function getCategoryListEdit($id) {           
		$genTable = '';		
		$sql="SELECT catId,categoryName FROM ".TBL_DESIGNCATEGORY_DESCRIPTION." AS TBL1,".TBL_DESIGNCATEGORY." AS TBL2  WHERE TBL1.catId=TBL2.parent_id AND TBL2.status='1' AND TBL2.isDeleted='0' AND TBL1.langId='".$_SESSION['DEFAULTLANGUAGE']."' ORDER BY TBL2.addDate";
                $query = $this->executeQry($sql);
		$num = $this->getTotalRow($query);
	//	$psel = ($catId == '0')?"selected":"";
		if($num > 0) {
			$genTable = '<select name="designCategory" id="m__design" style=" width:210px" >'
			.'<option value="">Please Select Design</option>';
			while($line = $this->getResultObject($query)) {
			
				
			     $select = ($id == $line->catId)?'selected="selected"':'';
				//$select=($line->catId == $id)?"selected":"";
			//	$sel = ($line->CatId == ($_POST['designCategory']?$_POST['designCategory']:$this->catId)) ? "selected":"";
				$genTable .= '<option value="'.$line->catId.'" '.$select.' >'.$line->categoryName.'</option>';
			//	$genTable .= $this->subCategory($line->id,1);
			}
		}
		$genTable .= '</select>';
		return $genTable;			
	}
        */
        
	
	/*
	function subCategory($pid,$dashes) {
		$sql = "select TBL1.id, TBL2.designLayoutName from ".TBL_DESIGNLAYOUT." as TBL1 INNER JOIN ".TBL_DESIGNLAYOUTDESC." as TBL2 ON (TBL1.id = TBL2.catId) where 1 and TBL1.parent_id = '".$pid."' AND TBL1.status = '1' AND TBL1.isDeleted = '0' AND TBL2.langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by TBL1.id";
		$dash = str_repeat('&nbsp;&nbsp;',$dashes);
		$query = $this->executeQry($sql);
		$num = $this->getTotalRow($query);
		if($num > 0) {
			while($line = $this->getResultObject($query)) {
				$sel = ($line->id == ($_POST['designCategory']?$_POST['designCategory']:$this->catId)) ? "selected":"";
				$genTable .= '<option value="'.$line->id.'" "'.$sel.'" >'.$dash.$line->designLayoutName.'</option>';
				$genTable .= $this->subCategory($line->id,($dashes + 1));
			}	
			return $genTable;
		} else {
			return false;
		}		
	}*/
	
/*	function getSubCategoryDesign($get){
		
		$sql = "SELECT tbl1.id, tbl2.designLayoutName FROM ".TBL_DESIGNLAYOUT." AS tbl1 INNER JOIN ".TBL_DESIGNLAYOUTDESC." AS tbl2 ON (tbl1.id = tbl2.catId) WHERE 1=1 AND tbl2.langId = '".$_SESSION['DEFAULTLANGUAGE']."' AND tbl1.parent_id != '0' AND tbl1.path like '%-".$get['catId']."-%'";
		$query = $this->executeQry($sql);
		$num = $this->getTotalRow($query);
		$tbl = '';
		$tbl = '<select name="designSubCategory" id="m__design_category">'
				.'<option value="">Please select design category</option>';
		if($num > 0){
			while($line = $this->getResultObject($query)){
				$sel = ($line->id == $_POST['designSubCategory']) ? "selected":"";
				$tbl .= '<option value="'.$line->id.'" '.$sel.'>'.$line->designLayoutName.'</option>';
			}
		}
		$tbl .= '</select>';
	echo $tbl; 	
	}*/
	
	/*
	function getSubCategoryDesignPOST($designId){
		 $sql = "SELECT tbl1.id, tbl2.designLayoutName FROM ".TBL_DESIGNLAYOUT." AS tbl1 INNER JOIN ".TBL_DESIGNLAYOUTDESC." AS tbl2 ON (tbl1.id = tbl2.catId) WHERE 1=1 AND tbl2.langId = '".$_SESSION['DEFAULTLANGUAGE']."' AND tbl1.parent_id != '0' AND tbl1.path like '%-".$designId."-%'";
		$query = $this->executeQry($sql);
		$num = $this->getTotalRow($query);
		$tbl = '';
		$tbl = '<select name="designSubCategory" id="m__design_category">'
				.'<option value="">Please select design category</option>';
		if($num > 0){
			while($line = $this->getResultObject($query)){
				$sel = ($line->id == $_POST['designSubCategory']) ? "selected":"";
				$tbl .= '<option value="'.stripslashes($line->id).'" '.$sel.'>'.stripslashes($line->categoryName).'</option>';
			}
		}
		$tbl .= '</select>';
	return $tbl; 	
	}
	*/
	
	
	
	
	
		
	
}
?>
