<?php 

session_start();

class MainProducts extends MySqlDriver{//Start Class
    
    //Start : Constructor====================================
    function __construct() {
        $this->obj = new MySqlDriver; 
    }


    //Start : Val Detail===================================================================
    function valDetail() { 
            $type=$_GET[type]?$_GET[type]:'A';
            $cond = "1  and MP.isDeleted = '0' and MPD.langId = '".$_SESSION['DEFAULTLANGUAGE']."' and MP.userType='".$type."' ";

            if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
                $searchtxt = addslashes($_REQUEST['searchtxt']);
                $cond .= " AND (MPD.productName LIKE '%$searchtxt%')  AND (MP.isCustomizable = '".$_REQUEST['custom']."') ";
            }
           
            $query="select MP.*, MPD.productName from ".TBL_MAINPRODUCT." as MP inner join ".TBL_MAIN_PRODUCT_DESCRIPTION." as MPD on (MP.id=MPD.productId) where $cond";
            $sql = $this->executeQry($query);
            $num = $this->getTotalRow($sql);
            $menuObj = new Menu();
            $page =  $_REQUEST['page']?$_REQUEST['page']:1;
            if($num > 0) {	
            //-------------------------Paging------------------------------------------------

            $paging = $this->paging($query); 
            $this->setLimit($_GET[limit]); 
            $recordsPerPage = $this->getLimit(); 
            $offset = $this->getOffset($_GET["page"]); 
            $this->setStyle("redheading"); 
            $this->setActiveStyle("smallheading"); 
            $this->setButtonStyle("boldcolor");
            $currQueryString = $this->getQueryString();
            $this->setParameter($currQueryString);
            $totalrecords = $this->numrows;
            $currpage = $this->getPage();
            $totalpage = $this->getNoOfPages();
            $pagenumbers = $this->getPageNo();	
            //-------------------------Paging------------------------------------------------

            $orderby = $_GET[orderby]? $_GET[orderby]:"MPD.productName";
            $orderby = $_GET[orderby]? $_GET[orderby]:"MP.addDate";
            $order = $_GET[order]? $_GET[order]:"ASC";   
            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
            $rst = $this->executeQry($query); 
            $row = $this->getTotalRow($rst);
            if($row > 0) {	
                $i = 1;	
                $genTable .= '<div class="column" id="column1">';
                while($line = $this->getResultObject($rst)) {
                    $currentOrder.=	$line->id.",";
                    $highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
                    $div_id = "status".$line->id;
                    $status=($line->status)?"Active":"Inactive";
                    //$productName = $this->fetchValue(TBL_MAIN_PRODUCT_DESCRIPTION,"productName","1 and productId = '".$line->id."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'");

                    $genTable .= '<div class="'.$highlight.'" id="'.$line->id.'">';                    
                    $genTable .= '<ul>';
                    $genTable .= '<li style="width:45px;">&nbsp;&nbsp;<input name="chk[]" value="'.$line->id.'" type="checkbox"></li>';
                    $genTable .= '<li style="width:40px;">'.$i.'</li>';

                    //Category Name===========================
                    $catIdArr=explode("-",$line->categoryId);
                    $catId=$catIdArr[1];
                    $categoryName .= stripslashes($this->fetchValue(TBL_CATEGORY_DESCRIPTION,'categoryName',"catId='".$catId."' and langId='".$_SESSION['DEFAULTLANGUAGE']."'"));
                    $genTable .= '<li style="width:100px;">'.$categoryName.'</li>';

                    //subCategory Name=================================
                    $subCatName=stripslashes($this->fetchValue(TBL_CATEGORY_DESCRIPTION,'categoryName',"catId='".$line->subCatId."' and langId='".$_SESSION['DEFAULTLANGUAGE']."'"));
                    $genTable .= '<li style="width:100px;">'.$subCatName.'</li>';


                    //Product Name=====================
                    $productName=stripslashes($line->productName);
                    $genTable .= '<li style="width:160px;">'.$productName.'</li>';

                    //Product Image==============================================
                    $img=$this->fetchValue(TBL_MAIN_PRODUCT_VIEW,'imageWithBorder',"product_id='".$line->id."' and isDefault='1'");                    
                    $genTable .= '
                    <li style="width:90px;">
                    <a href="'.__MAINPRODUCTEXTLARGEPATH__.$img.'" rel="shadowbox"><img src="'.__MAINPRODUCTTHUMBPATH__.$img.'" border="0"></a>
                    <a href="'.SITE_URL.TOOLPAGE.'?designId='.base64_encode($line->id).'">Customize</a>
                    </li>';

                    //Status Section================================
                    $genTable .= '<li style="width:70px;">';
                    if($menuObj->checkEditPermission()){
                        $genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'mainproduct\')">'.$status.'</div>';
                    }
                    $genTable .= '</li>';

                    //View Section======================
                    $genTable .= '<li style="width:50px;"><a rel="shadowbox;width=705;height=325" title="'.$productName.'" href="viewMainProduct.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></li>';

                    //Edit Section========================
                    $genTable1 .= '<li style="width:50px;">';
                    if($menuObj->checkEditPermission()){
                        $genTable1 .= '<a href="editMainProduct.php?id='.base64_encode($line->id).'&page='.$page.'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a>';

                    }
                    $genTable1 .= '</li>';
                    //Delete Section==============================
                    $genTable .= '<li style="width:30px;">';
                    if($menuObj->checkDeletePermission()){
                        $genTable .= "<a href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=mainproduct&type=delete&userType=".$type."&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
                    }
                    $genTable .= '</li>';
                    $genTable .= '<li style="width:50px;">';
                    $genTable .= '<span id="PDF_'.$line->id.'"><a href="javascript:void(0);" onclick="downloadProduct(\''.$line->id.'\');">Generate</a></span>';
                     $genTable .= '</li>';
                        $genTable .= '</ul>';                   
                    $genTable .= '</div>';
                    $i++;	
                }
                $genTable .= '</div>';
                $genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" /></div>';
                switch($recordsPerPage){
                    case 10:
                    $sel1 = "selected='selected'";
                    break;
                    case 20:
                    $sel2 = "selected='selected'";
                    break;
                    case 30:
                    $sel3 = "selected='selected'";
                    break;
                    case $this->numrows:
                    $sel4 = "selected='selected'";
                    break;
                }

                $currQueryString = $this->getQueryString();
                $limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
                $genTable.="
                <div style='overflow:hidden; margin:0px 0px 0px 50px;'>
                <table border='0' width='88%' height='50'>
                <tr>
                <td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
                Display 
                <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
                <option value='10' $sel1>10</option>
                <option value='20' $sel2>20</option>
                <option value='30' $sel3>30</option> 
                <option value='".$totalrecords."' $sel4>All</option>
                </select> Records Per Page
                </td>
                <td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td>
                <td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td>
                <td width='0' align='right'>".$pagenumbers."</td>
                </tr></table></div>";	

            }	
        } else {
            $genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
        }
        return $genTable;
    }
    /************Type List***********************/
    function getTypeList($type){
        $selA=($type=="A")?"selected='selected'":"";
        $selU=($type=="U")?"selected='selected'":"";
        $tbl.='<option value="A" '.$selA.'>Admin</option>';
        $tbl.='<option value="U" '.$selU.' >User</option>';
        return $tbl;
    }
    
    //Start : Change Status==============================================
        function changeValueStatus($get) {
        $status=$this->fetchValue(TBL_MAINPRODUCT,"status","1 and id = '$get[id]'");
        if($status==1) {
            $stat= 0;
            $status="Inactive";
        } else 	{
            $stat= 1;
            $status="Active";
        }
        $query = "update ".TBL_MAINPRODUCT." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
        if($this->executeQry($query)) 
            $this->logSuccessFail('1',$query);
        else 
            $this->logSuccessFail('0',$query);
        echo $status;		

        }

	
        //Delete Single Value=================================================
        function deleteValue($get) {
            $this->executeQry("update ".TBL_MAINPRODUCT." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'");
            $_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
            echo "<script language=javascript>window.location.href='manageMainProducts.php?type=$_GET[userType]&page=$get[page]&limit=$get[limit]';</script>";exit;

        }

		
//Start : Get Result By ID=====================================================
        function getResult($id) {
            $sql = $this->executeQry("select * from ".TBL_MAINPRODUCT." where id = '$id'");
            $num = $this->getTotalRow($sql);
            if($num > 0) {
                return $line = $this->getResultObject($sql);	
            } else {
                redirect("manageMainProducts.php");
            }	
        }


//Start : Delete Muliple Records==================================================
function deleteAllValues($post){
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
                    echo "<script language=javascript>window.location.href='manageMainProducts.php?type=$post[usertype]&page=$post[page]&limit=$post[limit]';</script>";exit;
		}
                //Delete Selected===========================
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
                                    $this->executeQry("update ".TBL_MAINPRODUCT." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$val'");
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
                //Enable selected===============================================
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_MAINPRODUCT." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
                // Disableed  selected===============================================
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
                                    $sql="update ".TBL_MAINPRODUCT." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
                                    mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}

		}

		echo "<script language=javascript>window.location.href='manageMainProducts.php?type=$post[usertype]&page=$post[page]';</script>";exit;
	}	
        
         //********* Download Designed Product PDF**************************************************//
	function getProductDownload($pid){          
	    $outPut='';
	    $query="Select MPD.productName,MPD.productDesc, MPV.imageWithBorder from ".TBL_MAIN_PRODUCT_DESCRIPTION." as MPD INNER JOIN ".TBL_MAIN_PRODUCT_VIEW." as MPV ON(MPD.productId=MPV.product_id) where MPD.productId='".$pid."'";
	    $rst=$this->executeQry($query);
	    $row=$this->getResultObject($rst);
            
            
            // Create Main Prodcut Detail XML File======================================
	    $outPut .= '<?xml version="1.0" encoding="utf-8"?>'."\n";           
	    $outPut .= '<products>'."\n";
	    $outPut .= '<product id="'.$pid.'" name="'.$row->productName.'" source="'.__MAINPRODUCTLARGEPATH__.$row->imageWithBorder.'"  >'."\n";	    
	    $outPut .= '<link>'."\n";
             
	    $outPut .= '<a href="'.__BASEURL__.TOOLPAGE.'?designId='.base64_encode($pid).'">Edit</a>'."\n";
	    $outPut .= '</link>'."\n";
	    $outPut .= '</product>'."\n";
	    $outPut .= '</products>';
            
	    
	    $dirFileBak = __BASE_DIR_FRONT__."files/FinalProduct/".$pid;
            
	    $this->createDirectory($dirFileBak);	    
	    $fp=fopen($dirFileBak."/".$pid.".xml","w");
	    $f=fwrite($fp,$outPut);
	    fclose($fp);	    
	    
            //Copy Main Product Original Image========================
	    $img=__MAINPRODUCTORIGINAL__.$row->imageWithBorder;            
	    copy($img,$dirFileBak."/".$pid.".png");	    
	   
	    /********* Generate PDF*******************/
	    require_once("includes/classes/pdf.php");
	    list($pWidth,$pHeight)=getimagesize($img);
	    $pWidth=(($pWidth/300)*72);
	    $pHeight=(($pHeight/300)*72);
	    
	    $pdfFileName = $pid.".pdf";
	    $size=array($pWidth,$pHeight);
	    $pdf = new PDF("P", "pt", $size);
            $pdf->AddPage();
            
            $pdf->RotatedImage($img, 0,0, $pWidth, $pHeight, 0);
            
            $pdfPath=$dirFileBak."/".$pdfFileName;
            $pdf->Output($pdfPath);
            unset($pdf);
	    
	    
	    require_once('includes/phpzip.inc.php');
	    $zipObj = new PHPZip();
	    $zipfilename =__DIR_PDF__.$pid.".zip";
            $zipfilePathname =__DIR_PDFPATH__.$pid.".zip";
	    if($zipObj->myZip($dirFileBak, $zipfilename)){
		echo "<a href='$zipfilePathname'>Download</a>"; 
	    }	    
	    
	}
	
        /********* Create Directory************************/
	function createDirectory($dirName){
		if(!is_dir($dirName)){
		    mkdir($dirName);
		    @chmod($dirName,0777);
		}
	}
		

	
//============old functions =================	=============================================================

	function addRecord($post,$file) {



		$sequence  =  0;			

		if(count($post[categoryId]) > 0) {

			$allCatId = "-".implode('-',$post[categoryId])."-";

		}	

	if($_SESSION['SESS_MSG'] == "") {

	  	if($file['productImage']['name']){

  			$filename = stripslashes($file['productImage']['name']);

			$extension = findexts($filename);

			$extension = strtolower($extension);

	

			$image_name = date("Ymdhis").time().rand().'.'.$extension;

			 $target   = __MAINPRODUCTORIGINAL1__.$image_name;

			if($this->checkExtensions($extension)) {	

				$filestatus = 	move_uploaded_file($file['productImage']['tmp_name'], $target);

				@chmod($target, 0777);

				if($filestatus){

					$imgSource = $target;	 

					$LargeImage = __MAINPRODUCTLARGE1__.$image_name;

					$ThumbImage = __MAINPRODUCTTHUMB1__.$image_name;

					

					$fileSizeLarge = $this->findSize('CATEGORY_THUMB_WIDTH','CATEGORY_THUMB_HEIGHT',200,200);

					$fileSizeThumb = $this->findSize('CATEGORY_THUMB_WIDTH','CATEGORY_THUMB_HEIGHT',100,100);

					exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSizeThumb $ThumbImage");

					exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSizeLarge $LargeImage");

					

					@chmod($LargeImage,0777);

					@chmod($ThumbImage,0777);

				} else {

					$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"There is some error to upload flag.!!!");

				}			

			} else {

				$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");

			} 	

		}

		 $query = "insert into ".TBL_MAINPRODUCT." set categoryId = '-".$post[device]."-',companyId='".$post[deviceCompany]."', productPrice = '$post[productPrice]',imageName='".$image_name."' ,orientation='$post[orientation]',quantity = '$post[quantity]', status = '1', addDate = '".date('Y-m-d H:i:s')."', addedBy = '".$_SESSION['ADMIN_ID']."', sequence  = '".$sequence."' , userType = 'A'";

		$sql = $this->executeQry($query);

		$inserted_id = mysql_insert_id();

		/////////////   ADD PRODUCT DECS - START   ////////////////////////////

				

			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		

			$num = $this->getTotalRow($rst);

			if($num){			

				while($line = $this->getResultObject($rst)) {					

					$productName = 'productName_'.$line->id;

					$productDesc = 'productDesc_'.$line->id;

					

					$query = "insert into ".TBL_MAIN_PRODUCT_DESCRIPTION." set productId = '".$inserted_id."', langId = '".$line->id."', productName = '".addslashes($post[$productName])."', productDesc = '".mysql_real_escape_string($post[$productDesc])."'";	

					if($this->executeQry($query)) 

						$this->logSuccessFail('1',$query);		

					else 	

						$this->logSuccessFail('0',$query);

				}	

			}

		$this->addProductView($inserted_id);

	$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been added successfully");

		  }

			$_id = base64_encode($inserted_id);								

	  	header('Location:addMainProduct.php');

		exit;	

		} 	

	

		

	function addProductView($inserted_id){

		

		$rst = $this->selectQry(TBL_VIEW,"status='1' order by id asc","","");		

		$num = $this->getTotalRow($rst);

		if($num > 0){

			

			while($line = $this->getResultObject($rst)) {

				$image_name_withBorder = "";

				$image_name_withoutBorder = "";

			 if($_FILES["prodImageWithBorder".$line->id]['name']){

				if($_FILES["prodImageWithBorder".$line->id]['name']){

					$filename = stripslashes($_FILES["prodImageWithBorder".$line->id]['name']);

					$extension = findexts($filename);

					$extension = strtolower($extension);

					$image_name_withBorder = date("Ymdhis").time().rand().'.'.$extension;

					$target    = __MAINPRODUCTORIGINAL1__.$image_name_withBorder;

					$targetThumb = __MAINPRODUCTTHUMB1__.$image_name_withBorder;

					$targetLarge = __MAINPRODUCTLARGE1__.$image_name_withBorder;

					

					if($this->checkExtensions($extension)) {	

						$filestatus_withBorder = move_uploaded_file($_FILES["prodImageWithBorder".$line->id]['tmp_name'], $target);

						$img_withBorderSizeArr = getimagesize($target);

						@chmod($target, 0777);

						if($filestatus_withBorder){

							$thumbH = "100";

							$thumbW = "100";

							$query = ' '.$target.' -resize '.$thumbW.'x'.$thumbH.'! '.$targetThumb.'';

							exec(IMAGEMAGICPATH.$query);

							

							$largeH = "400";

							$largeW = "262";

							$query = ' '.$target.' -resize '.$largeW.'x'.$largeH.'! '.$targetLarge.'';

							exec(IMAGEMAGICPATH.$query);

							

						}

					}

				}

			  }	

			 if($_FILES["prodImageWithoutBorder".$line->id]['name']){

				if($_FILES["prodImageWithoutBorder".$line->id]['name']){

					$filename = stripslashes($_FILES["prodImageWithoutBorder".$line->id]['name']);

					$extension = findexts($filename);

					$extension = strtolower($extension);

					

					$image_name_withoutBorder = date("Ymdhis").time().rand().'.'.$extension;

					$target    = __MAINPRODUCTORIGINAL1__.$image_name_withoutBorder;

					$targetThumb = __MAINPRODUCTTHUMB1__.$image_name_withoutBorder;

					$targetLarge = __MAINPRODUCTORIGINAL1__.$image_name_withoutBorder;

					

					if($this->checkExtensions($extension)) {	

						$filestatus_withoutBorder = move_uploaded_file($_FILES["prodImageWithoutBorder".$line->id]['tmp_name'], $target);

						$img_wihtoutBorderSizeArr = getimagesize($target);

						@chmod($target, 0777);

						if($filestatus_withoutBorder){

							$thumbH = "100";

							$thumbW = "100";

							$query = ' '.$target.' -resize '.$thumbW.'x'.$thumbH.'! '.$targetThumb.'';

							exec(IMAGEMAGICPATH.$query);

							

							$largeH = "400";

							$largeW = "262";

							$query = ' '.$target.' -resize '.$largeW.'x'.$largeH.'! '.$targetLarge.'';

							exec(IMAGEMAGICPATH.$query);

							

						}

					} 			

				}

			  }	

				

			  if($_FILES["prodImageWithBorder".$line->id]['name'] || $_FILES["prodImageWithoutBorder".$line->id]['name']){

				if($filestatus_withBorder && $filestatus_withoutBorder){

					$isDefault = ($line->id == $_POST['isDefault'])? "1":"0";

					

					$query = "insert into ".TBL_MAIN_PRODUCT_VIEW." set product_id = '".$inserted_id."', view_id = '".$line->id."', imageWithBorder = '".$image_name_withBorder."', imageWithoutBorder = '".$image_name_withoutBorder."', widthWithBorder = '".$img_withBorderSizeArr[0]."', 	heightWithBorder = '".$img_withBorderSizeArr[1]."', widthWithoutBorder = '".$img_wihtoutBorderSizeArr[0]."', heightWithoutBorder = '".$img_wihtoutBorderSizeArr[1]."', isDefault = '".$isDefault."'";

					

						

					

					if($this->executeQry($query)) 

						$this->logSuccessFail('1',$query);		

					else 	

						$this->logSuccessFail('0',$query);

				}else{

						$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");

					}

			  }

			

			}

		}

	}



		function editRecord($post,$file) {

	

	//print_r($file);exit;

		$_SESSION['SESS_MSG'] = "";		

		if($_SESSION['SESS_MSG'] == "") {

		

			/// by. brijesh start ////////////////////////

		 if($_FILES["productImage"]['name']){

		 	$filename = stripslashes($_FILES["productImage"]['name']);

			$extension = findexts($filename);

			$extension = strtolower($extension);

			$productImage = date("Ymdhis").time().rand().'.'.$extension;

			 $target= __MAINPRODUCTORIGINAL1__.$productImage;

			 $targetThumb = __MAINPRODUCTTHUMB1__.$productImage;

			 $targetLarge = __MAINPRODUCTLARGE1__.$productImage;

			

			if($this->checkExtensions($extension)) {

			

				$productImageStatus = move_uploaded_file($_FILES["productImage"]['tmp_name'], $target);

				$imgsizearr = getimagesize($target);

				@chmod($target, 0777);

				if($productImageStatus){

						$thumbH = "84";

						$thumbW = "77";

						$query = ' '.$target.' -resize '.$thumbW.'x'.$thumbH.'! '.$targetThumb.'';

						exec(IMAGEMAGICPATH.$query);

							

						$largeH = "400";

						$largeW = "262";

						$query = ' '.$target.' -resize '.$largeW.'x'.$largeH.'! '.$targetLarge.'';

						exec(IMAGEMAGICPATH.$query);

						

						$productImageName=$this->getImageName($id=base64_decode($post[id]));	

						@unlink(__MAINPRODUCTORIGINAL1__.$productImageName);

						@unlink(__MAINPRODUCTLARGE1__.$productImageName);

						@unlink(__MAINPRODUCTTHUMB1__.$productImageName);

				}

				else{

					$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");

					}

			  }

			  $query = "UPDATE ".TBL_MAINPRODUCT." set imageName='".$productImage."', modDate = '".date('Y-m-d H:i:s')."', modBy  = '".$_SESSION['ADMIN_ID']."' WHERE id = '$post[id]'";

			  $sql = $this->executeQry($query);

		   	}

				/// by. brijesh end////////////////////////

			

			 $query = "UPDATE ".TBL_MAINPRODUCT." set categoryId = '-".$post[device]."-', companyId = '$post[deviceCompany]', productPrice = '$post[productPrice]', 	quantity = '$post[quantity]', modDate = '".date('Y-m-d H:i:s')."', modBy  = '".$_SESSION['ADMIN_ID']."' WHERE id = '$post[id]'";

			$sql = $this->executeQry($query);

			$this->updated_id = $this->id;

			

			/////////////   ADD PRODUCT DECS - START   ////////////////////////////

				

			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		

			$num = $this->getTotalRow($rst);

			if($num){			

				while($line = $this->getResultObject($rst)) {					

					$productName = 'productName_'.$line->id;

					$productDesc = 'productDesc_'.$line->id;

					

					$query = "UPDATE ".TBL_MAIN_PRODUCT_DESCRIPTION." set productName = '".addslashes($post[$productName])."', productDesc = '".mysql_real_escape_string($post[$productDesc])."' WHERE productId = '$post[id]'";	

					if($this->executeQry($query)) 

						$this->logSuccessFail('1',$query);		

					else 	

						$this->logSuccessFail('0',$query);

				}	

			}

			

		/////////////   ADD PRODUCT DECS - END   ////////////////////////////

		

		/////////////   ADD PRODUCT VIEW - START   ////////////////////////////

			$this->updateProductView($post[id]);

		/////////////   ADD PRODUCT VIEW - END   ////////////////////////////

			

	echo 	$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been update successfully");

	

		} 

		 $_id = base64_encode($this->updated_id );

		 header('Location:manageMainProducts.php');

		 exit;

}



	function updateProductView($id){

		/*echo "<pre>";

		print_r($_FILES);

		echo "</pre>";

		exit;*/

		foreach($_FILES as $key=>$files){

			  if($_FILES[$key]['name']){

			  	$fileArr = explode("_",$key);

				$viewId = $fileArr[1]; 

				$filename = stripslashes($_FILES[$key]['name']);

				$extension = findexts($filename);

				$extension = strtolower($extension);

				$image_name = date("Ymdhis").time().rand().'.'.$extension;

				$target    = __MAINPRODUCTORIGINAL1__.$image_name;

				$targetThumb = __MAINPRODUCTTHUMB1__.$image_name;

				$targetLarge = __MAINPRODUCTLARGE1__.$image_name;

						

				if($this->checkExtensions($extension)) {	

					$filestatus = 	move_uploaded_file($_FILES[$key]['tmp_name'], $target);

					$imgSizeArr = getimagesize($target);

					@chmod($target, 0777);

					if($filestatus){

							$thumbH = "100";

							$thumbW = "100";

							$query = ' '.$target.' -resize '.$thumbW.'x'.$thumbH.'! '.$targetThumb.'';

							exec(IMAGEMAGICPATH.$query);

							

							$largeH = "400";

							$largeW = "262";

							$query = ' '.$target.' -resize '.$largeW.'x'.$largeH.'! '.$targetLarge.'';

							exec(IMAGEMAGICPATH.$query);

						}

				}

				if($filestatus){

					if($key == "prodImageWithBorder_".$viewId){

						$columnName = "imageWithBorder";

						$widthCol = "widthWithBorder";

						$hightCol = "heightWithBorder";

					}elseif($key == "prodImageWithoutBorder_".$viewId){

						$columnName = "imageWithoutBorder";

						$widthCol = "widthWithoutBorder";

						$hightCol = "heightWithoutBorder";

					}

					

					$resu = $this->executeQry("SELECT id FROM ".TBL_MAIN_PRODUCT_VIEW." WHERE product_id = '".$id."' AND view_id='".$viewId."' ");

					

					$numView = $this->getTotalRow($resu);

					

					if($numView > 0){

						$imgeName = $this->fetchValue(TBL_MAIN_PRODUCT_VIEW,"$columnName","product_id = '".$id."' AND view_id='".$viewId."'");

						@unlink(__MAINPRODUCTORIGINAL1__.$imgeName);

						@unlink(__MAINPRODUCTLARGE1__.$imgeName);

						@unlink(__MAINPRODUCTTHUMB1__.$imgeName);

						$query = "update ".TBL_MAIN_PRODUCT_VIEW." set ".$columnName."='".$image_name."', ".$widthCol." = '".$imgSizeArr[0]."', ".$hightCol." = '".$imgSizeArr[1]."' WHERE product_id = '".$id."' AND view_id='".$viewId."' ";

						}

					else

						$query = "INSERT INTO ".TBL_MAIN_PRODUCT_VIEW." set ".$columnName."='".$image_name."', ".$widthCol." = '".$imgSizeArr[0]."', ".$hightCol." = '".$imgSizeArr[1]."', product_id = '".$id."', view_id='".$viewId."' ";

						

					if($this->executeQry($query)) 

						$this->logSuccessFail('1',$query);		

					else 	

						$this->logSuccessFail('0',$query);

				}else{

						$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");

					}

					

			  }

		  }

		  

		///////////////////  UPDATE ISDEFAULT        ////////////////////

		$queryPView = "SELECT * FROM ".TBL_MAIN_PRODUCT_VIEW." WHERE product_id = '".$id."' ";

		$sqlPView = $this->executeQry($queryPView);

		$numPView = $this->getTotalRow($sqlPView);

		if($numPView > 0){

			while($linePView = $this->getResultObject($sqlPView)) {

				$isDefault = ($_POST['isDefault'] == $linePView->view_id)?"1":"0";

				$query = "update ".TBL_MAIN_PRODUCT_VIEW." set isDefault ='".$isDefault."' WHERE product_id = '".$id."' AND view_id='".$linePView->view_id."' ";

				

				if($this->executeQry($query)) 

					$this->logSuccessFail('1',$query);		

				else 	

					$this->logSuccessFail('0',$query);

			}

		

		}

		///////////////////  UPDATE ISDEFAULT        ////////////////////

  }



	

	
	

	function getAttributeValueId($id,$sendvalue) {

		$sql = $this->executeQry("select * from ".TBL_MAIN_PRODUCT_ATTRIBUTE." where productId = '$id'");

		$num = $this->getTotalRow($sql);

		if($num > 0) {

			 while($line = $this->getResultObject($sql)){

			  $line1[]=$line->attributevalueId;

			  $line2[]=$line->attributeId;

			  $pricekey = "price".$line->attributevalueId;

			  $price[$pricekey]=$line->price;

			 }

		} 

		if($sendvalue == 'AttributeValueId')

		return $line1;

		elseif($sendvalue == 'attributeId')

		return $line2;

		elseif($sendvalue == 'price')

		return $price;

	}		

	

	function isProductNameExist($productname,$langId,$id=''){

		$catname = addslashes(trim($productname));

		$rst = $this->selectQry(TBL_MAIN_PRODUCT_DESCRIPTION,"langId = '".$langId."' and productName='".$catname."' AND productId!='".$id."'  ","","");

		$row = $this->getTotalRow($rst);

		$line = $this->getResultObject($rst);

		if($this->fetchValue(TBL_MAINPRODUCT,'isDeleted',"id='".$line->productId."'")){

		    if($id)

			$rstFind = $this->executeQry("update ".TBL_MAINPRODUCT." set isDeleted='1' where id='".$id."'");

			$rstFind = $this->executeQry("update ".TBL_MAINPRODUCT." set isDeleted='0' where id='".$line->productId."'");

			echo "<script>window.location.href='manageMainProducts.php';</script>";

			exit;

		}else{

			return $row;

		}

	}

	

	function getAttribute(){

		$query = "SELECT ea.* , eat.* FROM ".TBL_ATTRIBUTE." AS ea LEFT JOIN ".TBL_ATTRIBUTE_DESCRIPTION." AS eat ON ea.id = eat.attributeId WHERE ea.status = '1' AND ea.isDeleted = '0' AND eat.langId = '".$_SESSION['DEFAULTLANGUAGE']."'  and ea.showAttribute = '0' order by eat.attributeName";

		$sql = $this->executeQry($query);

		$num = $this->getTotalRow($sql);

	    $j=0;

		if($num > 0){

		    echo '<font style="padding-left:0px;" id="selectval12" >';

			echo '<input type="checkbox" name="selectval" id="selectval" onclick="selectAllBox();" />Select All';

			echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Price</font>';

		   while($line = $this->getResultObject($sql)) {

				$res = "SELECT av.* , avs.* FROM ".TBL_ATTRIBUTE_VALUES." AS av LEFT JOIN ".TBL_ATTRIBUTE_VALUES_DESCRIPTION." AS avs ON av.id = avs.attributeValuesId WHERE av.attributeId = '".$line->id."' AND  av.status = '1' AND av.isDeleted = '0' AND avs.langId = '".$_SESSION['DEFAULTLANGUAGE']."'order by avs.attributeValuesName";

				$resul = $this->executeQry($res);

				$row = $this->getTotalRow($resul);

				echo '<table width="50%" border="0" align="center"   >';

				echo '<font style="padding-left:145px; width:auto;">';

				echo '<input type="checkbox"  name="attribute[]" value="'.$line->id.'" id="'.$line->id.'menuCheck" onclick="checkAllcheckbox('.$line->id.','.$row.');" />'.stripslashes($line->attributeName);

				echo '</font>';

				echo '<br>';

			    $i=0;

				if($row > 0){

					while($result = $this->getResultObject($resul)) {

					    echo '<tr><td width="30%" align="left" >';

						echo '<font style="padding-left:20px;">';

						echo '<input type="checkbox"  name="attributevalue[]" value="'.$result->id.'" id="'.$line->id.'mCheck'.$i.'" />'.stripslashes(stripslashes($result->attributeValuesName));

						echo '</font>';

						echo '</td><td>';

						echo '<font style="padding-left:20px; width:auto;">';

						echo '<input type="text" name="price'.$result->id.'" value="'.$result->price.'" size="6" maxlength="6"  />';

						echo '</font>';

						echo '</td></tr>';

						$i++;

				    }

				    $j++;

			    }

				echo '</table>';

		    }

		 }

	}	

	

	function getAttributeEdit($attributeValueId,$attributeId,$price){

	  

		$query = "SELECT ea.* , eat.* FROM ".TBL_ATTRIBUTE." AS ea LEFT JOIN ".TBL_ATTRIBUTE_DESCRIPTION." AS eat ON ea.id = eat.attributeId WHERE ea.status = '1' and ea.showAttribute = '0' AND ea.isDeleted = '0' AND eat.langId = '".$_SESSION['DEFAULTLANGUAGE']."'order by eat.attributeName";

		$sql = $this->executeQry($query);

		$num = $this->getTotalRow($sql);

		$j=0;

		if($num > 0){

			echo '<font >';

			echo '<input type="checkbox" name="selectval" id="selectval" onclick="selectAllBox();" />Select All';

			echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Price</font>';

		    while($line = $this->getResultObject($sql)) {

				$res = "SELECT av.* , avs.* FROM ".TBL_ATTRIBUTE_VALUES." AS av LEFT JOIN ".TBL_ATTRIBUTE_VALUES_DESCRIPTION." AS avs ON av.id = avs.attributeValuesId WHERE av.attributeId = '".$line->id."' AND  av.status = '1' AND av.isDeleted = '0' AND avs.langId = '".$_SESSION['DEFAULTLANGUAGE']."'order by avs.attributeValuesName";

				$resul = $this->executeQry($res);

				$row = $this->getTotalRow($resul);

				if(count($attributeId) > 0 )

			      foreach($attributeId as $selectedId){

					 if($selectedId == $line->id){

						$checked="checked";

						break;

				     }else{

						$checked=""; 

					 }

				 }

				 echo '<table width="50%" border="0" align="center" style="padding-left:20px;"   >';

				 echo '<font style="padding-left:145px;">';

				 echo '<input type="checkbox"  name="attribute[]" '.$checked.' value="'.$line->id.'" id="'.$line->id.'menuCheck" onclick="checkAllcheckbox('.$line->id.','.$row.');" />'.$line->attributeName;

				 echo '</font>';

			     $i=0;

				 if($row > 0){

				      while($result = $this->getResultObject($resul)) {

					   echo '<font style="padding-left:20px;">';

					    if(count($attributeValueId) > 0 )

						   foreach($attributeValueId as $attributeselectedId){

							 if($attributeselectedId == $result->id){

								  $checked="checked";

								  break;

							 }else{

								  $checked="";

							 }

						   }

						   echo '<tr><td width="30%" align="left" >';

						   echo '<input type="checkbox"  name="attributevalue[]" '.$checked.' value="'.$result->id.'" id="'.$line->id.'mCheck'.$i.'" />'.stripslashes(stripslashes($result->attributeValuesName));

						   echo '</font>';

						   echo '</td><td>';

						   echo '<font  width:auto;">';

						   echo '<input type="text" name="price'.$result->id.'" value="'.$price['price'.$result->id].'" size="6" maxlength="6" onkeyup="return isNum13(this.value,'.$result->id.');"   />';

						   echo '<p style="padding-left:150px;" id="price'.$result->id.'">';

						   echo '</p>';

						   echo '</font>';

						   echo '</td></tr>';

						   $i++;

					     }

					     $j++;

				       }

				       echo '</table>';

			      }

		     }

	   }

	

    function getAttributeAllId(){

	  

	    $query = "SELECT ea.* , eat.* FROM ".TBL_ATTRIBUTE." AS ea LEFT JOIN ".TBL_ATTRIBUTE_DESCRIPTION." AS eat ON ea.id = eat.attributeId WHERE ea.status = '1' AND ea.isDeleted = '0' AND eat.langId = '".$_SESSION['DEFAULTLANGUAGE']."'order by eat.attributeName";

		$sql = $this->executeQry($query);

		$num = $this->getTotalRow($sql);

		if($num > 0){

		    while($line = $this->getResultObject($sql)) {

			  $line1[] = $line->id;

			}

	    }

		

		//print_r($line1);

		if(count($line1) > 0 ){

		 $line2 = implode(",",$line1);

		}

		return $line2;

	}

	

	function getAllAttributeValueId(){

	  

	    $query = "SELECT ea.* , eat.* FROM ".TBL_ATTRIBUTE." AS ea LEFT JOIN ".TBL_ATTRIBUTE_DESCRIPTION." AS eat ON ea.id = eat.attributeId WHERE ea.status = '1' AND ea.isDeleted = '0' AND eat.langId = '".$_SESSION['DEFAULTLANGUAGE']."'order by eat.attributeName";

		$sql = $this->executeQry($query);

		$num = $this->getTotalRow($sql);

		   if($num > 0){

		      while($line = $this->getResultObject($sql)){

				$res = "SELECT av.* , avs.* FROM ".TBL_ATTRIBUTE_VALUES." AS av LEFT JOIN ".TBL_ATTRIBUTE_VALUES_DESCRIPTION." AS avs ON av.id = avs.attributeValuesId WHERE av.attributeId = '".$line->id."' AND  av.status = '1' AND av.isDeleted = '0' AND avs.langId = '".$_SESSION['DEFAULTLANGUAGE']."'order by avs.attributeValuesName";

				$resul = $this->executeQry($res);

				$row = $this->getTotalRow($resul);

				if($row > 0){

					$result2[]=$row;

				}

		      }		

	       }

		   if(count($result2) > 0 ){

			$result3 = implode(",",$result2);

		   }

		   return $result3;

	}		

	

	

	function sortProductsOrder($get){

/*	   echo "<pre>";

	     print_r($get);

	   echo "<pre>";*/

	  $sortedArr 	=   explode("=",$get['url']);

	  $sortedIdArr	=	$sortedArr[1];

	  $Sorted_Order_Arr = explode(",",$sortedIdArr);

	  $Current_Order_Arr = explode(",",substr_replace($get['Current_Order'],"",-1));

	  	  

	  $i = 0;

	  $sorted_key_arr	=	array();	 

	  foreach($Sorted_Order_Arr as $Sorted_Order_Val){

	     $current_id	=	$Current_Order_Arr[$i];

		 $sql_string	=	"SELECT sequence FROM ".TBL_MAINPRODUCT." WHERE id = '$current_id'";

         $query 	 	=	mysql_query($sql_string);

		 if($line = mysql_fetch_object($query)){

		    $sorted_key_arr[$Sorted_Order_Val] = $line->sequence;

		 }

		 $i++;

       }

	 

	  foreach($sorted_key_arr as $key=>$val){  

	        $sql_string		=	"UPDATE ".TBL_MAINPRODUCT." SET sequence = '$val' WHERE id = '$key'";

		   	mysql_query($sql_string);   

	   }



	   $cond = "1 and ".TBL_MAINPRODUCT.".id = ".TBL_MAIN_PRODUCT_DESCRIPTION.".productId and ".TBL_MAINPRODUCT.".isDeleted = '0' and ".TBL_MAIN_PRODUCT_DESCRIPTION.".langId = '".$_SESSION['DEFAULTLANGUAGE']."'";

	   $cond .= " group by ".TBL_MAINPRODUCT.".categoryId, ".TBL_MAINPRODUCT.".sequence";

	   $query = "select ".TBL_MAINPRODUCT.".*,".TBL_MAIN_PRODUCT_DESCRIPTION.".productName from ".TBL_MAINPRODUCT." , ".TBL_MAIN_PRODUCT_DESCRIPTION." where $cond ";

	   $orderby = $_GET[orderby]? $_GET[orderby]:TBL_MAINPRODUCT.".sequence";

	   $order = $_GET[order]? $_GET[order]:"ASC";   

       //$query .=  " ORDER BY $orderby $order ";

			$paging = $this->paging($query); 

			$this->setLimit($get['limit']); 

			$recordsPerPage = $this->getLimit(); 

			$offset = $this->getOffset($get['page']); 

			$this->setStyle("redheading"); 

			$this->setActiveStyle("smallheading"); 

			$this->setButtonStyle("boldcolor");

			$currQueryString = $this->getQueryString();

   			$this->setParameter($currQueryString);

			$totalrecords = $this->numrows;

			$currpage = $this->getPage();

			$totalpage = $this->getNoOfPages();

			$pagenumbers = $this->getPageNo();	



           $query .=  " LIMIT ".$offset.", ". $recordsPerPage;

            $rst = $this->executeQry($query);  



		  while($line = mysql_fetch_object($rst)){

		     $currentOrder .= $line->id.",";

		  }

	   	  ?><input type="hidden" name="currentOrder" id="currentOrder" value="<?=$currentOrder?>" /><?php

	}

	

	

	

	function getColorViewImageUpload() {

			

		$genTable = '<li><input type="hidden" name="colorCode" value="#000000">

						 <input type="hidden" name="colorTitle" value="White"></li>';

				

		$genTable .= '</br>';		

		

		$sql2 = $this->executeQry("select ".TBL_VIEW.".*,".TBL_VIEWDESC.".viewName from ".TBL_VIEW.", ".TBL_VIEWDESC." where 1 and ".TBL_VIEW.".status  = '1' and ".TBL_VIEW.".id = ".TBL_VIEWDESC.".viewId and ".TBL_VIEWDESC.".langId = '1' order by ".TBL_VIEW.".id asc");

		$num2 = $this->getTotalRow($sql2);

		$genTable .= '<li><input type="hidden" name="totalProductImages" value="'.$num2.'"></li>';

		$j = 1;

		while($line2 = $this->getResultObject($sql2)) {

			if($j == 1) {				

				$genTable .= '<li>';

				$genTable .= '<input type="hidden" name="viewId'.$line2->id.'" value="'.$line2->id.'">';

				$genTable .= $line2->viewName . " &nbsp;&nbsp;<input type='file' name='prodImage".$line2->id."'>";

				if($_GET['id'] == '')

				$genTable .= "<li class='lable'></li><li>Print Size&nbsp;&nbsp;<input type='text' name='printWidth".$line2->id."' size='3'> X  <input type='text' name='printHeight".$line2->id."' size='3' >  </li>";



				$sel = "";	

				if($j == 1) { $sel = "checked=checked"; }	else { $sel = ""; }

				$genTable .= "<li class='lable'></li><li>Make this Default&nbsp;&nbsp;<input type='radio' name='isDefault' value='".$line2->id."' $sel></li>";

				

				

				$genTable .= '</li>';

			} else {

				$genTable .= '<li class="lable"></li><li>';

				

				$genTable .= '<input type="hidden" name="viewId'.$line2->id.'" value="'.$line2->id.'">';

				$genTable .= $line2->viewName . " &nbsp;&nbsp;<input type='file' name='prodImage".$line2->id."'>";

				if($_GET['id'] == '')

				$genTable .= "<li class='lable'></li><li>Print Size&nbsp;&nbsp;<input type='text' name='printWidth".$line2->id."' size='3'> X  <input type='text' name='printHeight".$line2->id."' size='3' >  </li>";



				$sel = "";	

				if($i == 1) { $sel = "checked=checked"; }	else { $sel = ""; }

				$genTable .= "<li class='lable'></li><li>Make this Default&nbsp;&nbsp;<input type='radio' name='isDefault' value='".$line2->id."' $sel></li>";

				

				$genTable .= '</li>';

			}	

			$genTable .= '<li>&nbsp;</li>';

			$j++;

		}

		return $genTable;

	

	}

	

	function getProductColors($pid){

		$color = array();

		$sql = "SELECT colorCode,tbl1.id,colorTitle  FROM  ".TBL_MAIN_PRODUCT_COLOR." AS tbl1 INNER JOIN  ".TBL_MAIN_PRODUCT_COLOR_DESCRIPTION." AS tbl2 ON (tbl1.id = tbl2.product_color_id) WHERE product_id ='$pid' AND   	

langId='".$_SESSION['DEFAULTLANGUAGE']."' ORDER BY colorTitle DESC";

		$rst = $this->executeQry($sql);

		while($row = $this->getResultObject($rst)){

			$color[] = $row;

		}

		return $color;	

	}

	

	

	function getColorViewImageUploadEdit($id,$colorId,$ii) {

		$sql2 = $this->executeQry("select ".TBL_VIEW.".*,".TBL_VIEWDESC.".viewName from ".TBL_VIEW.", ".TBL_VIEWDESC." where 1 and ".TBL_VIEW.".status  = '1' and ".TBL_VIEW.".id = ".TBL_VIEWDESC.".viewId and ".TBL_VIEWDESC.".langId = '1' order by ".TBL_VIEW.".id asc");

		$num2 = $this->getTotalRow($sql2);

	//	$genTable .= '<li><input type="hidden" name="totalProductImages" value="'.$num2.'"></li>';

		

		$j = 1;

		while($line2 = $this->getResultObject($sql2)) {

		//echo $ii++;

		

				$sqlView  = $this->executeQry("select * from ".TBL_MAIN_PRODUCT_VIEW." where 1 and product_id = '$id' and view_id = '".$line2->id."' AND color_id='$colorId'" ); 		

				$lineView = $this->getResultObject($sqlView);

		

			$genTable .= ' <div class="admin-product-sub">

            	<ul>';

				$genTable .= "<li id='imgdiv".$lineView->id."' style='hight:100px;'>";

					if($lineView->imageName){

					$genTable .= '<img src="'.__MAINPRODUCTTHUMB__.trim($lineView->imageName) .'" border="0" /><i><a href="javascript:;" onClick="deleteMainProdImg('.$id.','.$colorId.','.$lineView->id.')"><img src="images/close_small.png" alt="Close" title="Close" border="0" /></a></i>';

					 }else{

					$genTable .= '<img src="images/productnotfound.jpg" border="0" width="100" height="100"/>'; 

					 }

				$genTable .= '</li>';

				$genTable .= '<li class="front">'.$line2->viewName.'</li>';

				if($this->fetchValue(TBL_MAINPRODUCT,'isCustomizable',"id='".$id."'") != 1)

                    $genTable .= '<li><input type="file" name="prodImage'.$line2->id.'" value="" /></li>';

				if($ii == 0 && $this->fetchValue(TBL_MAINPRODUCT,'isCustomizable',"id='".$id."'") != 1) {

					$genTable .= "

						<li class='front'>Print Size</li><li><input type='text' name='printWidth".$line2->id."' size='3' value='".$lineView->printWidth."'><li><li class='front-size'>X</li>";	

					$genTable .= " <li><input type='text' name='printHeight".$line2->id."' size='3' value='".$lineView->printHeight."'></li>";

					}

				

				$genTable .= '<input type="hidden" name="viewId'.$line2->id.'" value="'.$line2->id.'">';							

				//$checked = "";	

				if($lineView->isDefault == '1') { $checked = 'checked="checked"'; }	else { $checked = ""; }

				$genTable .= "<li class='default'>Make this Default<span><input type='radio' name='isDefault$colorId' value='".$line2->id."' $checked></span></li>";

				$genTable .= '<li style="clear:both;"></li><li></li>

                </ul>

            </div>';	

			$j++;

			} 

			$genTable .= '<div align="center"><input type="button" name="" value="" class="classsubmit" onClick="imageFormSubmit(this.form);" /></div>';

			

		return $genTable;

	

	}

	

	

	function deleteMainColorImage($get){

	$_GET[productId] = base64_decode($_GET[productId]);

		$sqlView  = $this->executeQry("DELETE from ".TBL_MAIN_PRODUCT_VIEW." where 1 and product_id = '$_GET[productId]' AND color_id='$_GET[colorId]'" );

		

		$sqlView  = $this->executeQry("DELETE from ".TBL_MAIN_PRODUCT_COLOR." where 1 and id='$_GET[colorId]'" );

		$sqlView  = $this->executeQry("DELETE from ".TBL_MAIN_PRODUCT_COLOR_DESCRIPTION." where 1 and product_color_id='$_GET[colorId]'" );

	}

	

	function deleteMainImage($get){

		$sqlView  = $this->executeQry("select * from ".TBL_MAIN_PRODUCT_VIEW." where 1 and id = '$_GET[viewid]'" ); 		

		$lineView = $this->getResultObject($sqlView);	

		$sqlView  = $this->executeQry("DELETE from ".TBL_MAIN_PRODUCT_VIEW." where 1 and id = '$_GET[viewid]'" );

		echo '<img src="images/productnotfound.jpg" border="0" width="100" height="100"/>';

	}

	

	

	function updateMainColorProduct($post,$file){

		

		//print_r($post);

		///exit;

		

		$productId = $post['productId'];

		$colorId = $post['colorId'];

		

		$sql2 = $this->executeQry("select ".TBL_VIEW.".*,".TBL_VIEWDESC.".viewName from ".TBL_VIEW.", ".TBL_VIEWDESC." where 1 and ".TBL_VIEW.".status  = '1' and ".TBL_VIEW.".id = ".TBL_VIEWDESC.".viewId and ".TBL_VIEWDESC.".langId = '1' order by ".TBL_VIEW.".id asc");

		$num2 = $this->getTotalRow($sql2);

		$j = 1;

			while($line2 = $this->getResultObject($sql2)) {

			//$printWidth = $post['printWidth'.$line2->id];

			//$printHeight = $post['printHeight'.$line2->id];

			$printWidth = $post['printWidth'.$line2->id];

			$printHeight = $post['printHeight'.$line2->id];

			$makeDefaultId = $post['isDefault'.$colorId];

			$imageName = "prodImage".$line2->id;

			

		//	print_r($file);	

		$sql = "SELECT id,imageName FROM ".TBL_MAIN_PRODUCT_VIEW." WHERE product_id='$productId' AND color_id='$colorId' AND   	

view_id='".$line2->id."'";	

				$rst = $this->executeQry($sql);

				$num = $this->getTotalRow($rst);

				$line = $this->getResultObject($rst);

				if($num){

						///// update image and other info	

						$query2 = "UPDATE ".TBL_MAIN_PRODUCT_VIEW." set  printWidth = '".$printWidth."', printHeight = '".$printHeight."'";	

							if($makeDefaultId == $line2->id) {

							$query3 = "UPDATE ".TBL_MAIN_PRODUCT_VIEW." set  isDefault = '0' where product_id='".$productId."' AND color_id='".$colorId."' and view_id != '".$makeDefaultId."'";	

								$this->executeQry($query3);	

								$query2 .= ", isDefault = '1'";

							}

							

							if($file[$imageName]['name']){

							$filename = stripslashes($file[$imageName]['name']);

							$extension = findexts($filename);

							$extension = strtolower($extension);

					

							$image_name = date("Ymdhis").time().rand().'.'.$extension;

							$target    = __MAINPRODUCTPATH__.$image_name;

							if($this->checkExtensions($extension)) {	

								$filestatus = 	move_uploaded_file($file[$imageName]['tmp_name'], $target);

								chmod($target, 0777);	

								if($filestatus){

									$imgSource = $target;	 

									$LargeImage = __MAINPRODUCTLARGE1__.$image_name;

									$ThumbImage = __MAINPRODUCTTHUMB__.$image_name;

									chmod(__MAINPRODUCTLARGE1__,0777);

									chmod(__MAINPRODUCTTHUMB__,0777);

									$fileSize = $this->findSize('PRODUCT_THUMB_WIDTH','PRODUCT_THUMB_HEIGHT',100,100);

									exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSize $ThumbImage");

									

									$fileSize2 = $this->findSize('PRODUCT_LARGE_WIDTH','PRODUCT_LARGE_HEIGHT',250,250);

									exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSize2 $LargeImage");

									

									$query2 .= ", imageName = '$image_name'";

									

									}

								}

							}

						$query2 .= " WHERE  id='".$line->id."'";			

						$this->executeQry($query2);

						$query22 = "UPDATE ".TBL_MAIN_PRODUCT_VIEW." set  printWidth = '".$printWidth."', printHeight = '".$printHeight."' where product_id = '".$productId."' and view_id = '".$line2->id."'";

						$this->executeQry($query22);

						

						$query2222 = "UPDATE ".TBL_MAIN_PRODUCT_COLOR_DESCRIPTION." set  colorTitle = '".$post['colorName']."' where langId = '".$_SESSION['DEFAULTLANGUAGE']."' and product_color_id = '".$post['colorId']."'";

						$this->executeQry($query2222);

						

						$query222 = "UPDATE ".TBL_MAIN_PRODUCT_COLOR." set  colorCode = '".$post['colorCode']."' where id = '".$post['colorId']."' ";

						$this->executeQry($query222);

														

				}else{

						//// if view id not exists/////

					if($file[$imageName]['name']){

					$filename = stripslashes($file[$imageName]['name']);

					$extension = findexts($filename);

					$extension = strtolower($extension);

			

					$image_name = date("Ymdhis").time().rand().'.'.$extension;

					$target    = __MAINPRODUCTPATH__.$image_name;

					if($this->checkExtensions($extension)) {	

						$filestatus = 	move_uploaded_file($file[$imageName]['tmp_name'], $target);

						chmod($target, 0777);	

						if($filestatus){

							$imgSource = $target;	 

							$LargeImage = __MAINPRODUCTLARGE1__.$image_name;

							$ThumbImage = __MAINPRODUCTTHUMB__.$image_name;

							chmod(__MAINPRODUCTLARGE1__,0777);

							chmod(__MAINPRODUCTTHUMB__,0777);

							$fileSize = $this->findSize('PRODUCT_THUMB_WIDTH','PRODUCT_THUMB_HEIGHT',100,100);

							exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSize $ThumbImage");

							

							$fileSize2 = $this->findSize('PRODUCT_LARGE_WIDTH','PRODUCT_LARGE_HEIGHT',250,250);

							exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSize2 $LargeImage");

							

							$query2 = "insert into ".TBL_MAIN_PRODUCT_VIEW." set product_id = '$productId', color_id = '".$colorId."', view_id = '".$line2->id."', imageName = '".addslashes(trim($image_name))."', printWidth = '".$printWidth."', printHeight = '".$printHight."'";	

							if($makeDefaultId == $line2->id) {

								$query2 .= ", isDefault = '1'";

							}else{

								$query2 .= ", isDefault = '0'";

							}

							$this->executeQry($query2);							

						}								

					}

				}

			}

			

		}//while

	$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been updated successfully!!!");

	header("Location:editMainProduct.php?id=".base64_encode($productId));

	echo "<script language=javascript>window.location.href='editMainProduct.php?id=".base64_encode($productId)."';< /script>";

	exit;

	}// function

	

	function fetchValue_Edit($valueFind , $productId , $viewId){

		$query  = "select ".$valueFind." as findValue from ".TBL_MAIN_PRODUCT_VIEW." where product_id = '".$productId."' and  view_id = '".$viewId."' limit 0 , 1 ";

		$resu = $this->executeQry($query);

		$line = $this->getResultObject($resu);

		return $line->findValue;

	}

	

	

	function addNewColorProduct($post,$file){

	

	

		$inscolor = "INSERT INTO ".TBL_MAIN_PRODUCT_COLOR." SET  product_id = '$post[id]',colorCode = '".mysql_real_escape_string($_POST['colorcode'])."',add_date = '".date('Y-m-d H:i:s')."'";

				$colorexec = $this->executeQry($inscolor);

				$inserted_color_id = mysql_insert_id(); 

		

			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		

			$num = $this->getTotalRow($rst);

			if($num){			

				while($line = $this->getResultObject($rst)) {					

					$colorName =  'colorName_'.$line->id;

					

					$colTitlesql = "insert into ".TBL_MAIN_PRODUCT_COLOR_DESCRIPTION." set product_color_id = '$inserted_color_id', langId = '".$line->id."', colorTitle = '".mysql_real_escape_string($post[$colorName])."'";

					$caltitleexec = $this->executeQry($colTitlesql);

				}	

			}		

	

	

		$sql2 = $this->executeQry("select ".TBL_VIEW.".*,".TBL_VIEWDESC.".viewName from ".TBL_VIEW.", ".TBL_VIEWDESC." where 1 and ".TBL_VIEW.".status  = '1' and ".TBL_VIEW.".id = ".TBL_VIEWDESC.".viewId and ".TBL_VIEWDESC.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ".TBL_VIEW.".id asc");



		while($line2 = $this->getResultObject($sql2)) {

			$imageName = "prodImage".$line2->id;

			$viewId = "viewId".$line2->id;	

			//echo "product_id ='".$post['id']."' and view_id = '".$line2->id."'";

			//exit;

			$printWidth = $this->fetchValue_Edit('printWidth',$post[id],$line2->id);

			$printHeight = $this->fetchValue_Edit('printHeight',$post[id],$line2->id);

			

			if($file[$imageName]['name']){

				$filename = stripslashes($file[$imageName]['name']);

				$extension = findexts($filename);

				$extension = strtolower($extension);

		

				$image_name = date("Ymdhis").time().rand().'.'.$extension;

				$target    = __MAINPRODUCTORIGINAL__.$image_name;

				if($this->checkExtensions($extension)) {	

					$filestatus = 	move_uploaded_file($file[$imageName]['tmp_name'], $target);

					chmod($target, 0777);	

					if($filestatus){

						$imgSource = $target;	 

						$LargeImage = __MAINPRODUCTLARGE1__.$image_name;

						$ThumbImage = __MAINPRODUCTTHUMB__.$image_name;

						chmod(__MAINPRODUCTORIGINAL__,0777);

						chmod(__MAINPRODUCTLARGE1__,0777);

						chmod(__MAINPRODUCTTHUMB__,0777);

						$fileSize = $this->findSize('PRODUCT_THUMB_WIDTH','PRODUCT_THUMB_HEIGHT',100,100);

						exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSize $ThumbImage");

						

						$fileSize2 = $this->findSize('PRODUCT_LARGE_WIDTH','PRODUCT_LARGE_HEIGHT',250,250);

						exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSize2 $LargeImage");

						

						$query2 = "insert into ".TBL_MAIN_PRODUCT_VIEW." set product_id = '$post[id]', color_id = '".$inserted_color_id."', view_id = '".$post[$viewId]."', imageName = '".addslashes($image_name)."', printWidth = '".$printWidth."', printHeight = '".$printHeight."'";	

						if($post[isDefault] == $line2->id) {

							$query2 .= ", isDefault = '1'";

						}else{

							$query2 .= ", isDefault = '0'";

						}

						$this->executeQry($query2);							

					}								

				}

			}						

		}

		$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been added successfully!!!");		

	echo "<script language=javascript>window.location.href='editMainProduct.php?id=".base64_encode($post[id])."';</script>";	

}

	

function getProductSize(){

	    //echo $productid;

		$query = "select ".TBL_SIZE.".*,".TBL_SIZEDESC.".sizeName from ".TBL_SIZE.", ".TBL_SIZEDESC." where 1 and ".TBL_SIZE.".status  = '1' and ".TBL_SIZE.".id = ".TBL_SIZEDESC.".Id and ".TBL_SIZEDESC.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ".TBL_SIZE.".id asc";

		$sql2 = $this->executeQry($query);

		$num2 = $this->getTotalRow($sql2);

		$table = '<div style="padding:5px;margin:1px 0px 0px 0px;list-style:none; width:auto;"> <div style="width:750px;">';

		

		while($line2 = $this->getResultObject($sql2)){

			$arrayId[] = $line2->id;

			$table .= '<div style="width:90px;float:left;margin:5px 0px 0px 20px;"><div style="width:90px;">'.$line2->sizeName . '<input type = "checkbox" name="size'.$line2->id.'" "'.$checked.'" /></div><div style="width:90px;">Price:<input type="text" name="price'.$line2->id.'" value="'.$price.'" size=4 />' ;

			if($this->fetchValue(TBL_SYSTEMCONFIG,'systemVal',"systemName='ALLOW_WEIGHT'") == 2)

				$table .= 'Weight:<input type="text" name="weight'.$line2->id.'" value="" size=4 />';

				$table .='</div></div>';

		}

	

		$table .= '<div style="clear:both;"></div>

        </div>';

		//$table .= '<div id="ttfFileUpload"><li >

                   //<br/><input type="text" name="attributeName1"  value="" id="m__Size__Detail" size = "5"/>';  

					for($i = 0; $i< $num2 ; $i++){

					    //$table .="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

						//$table .= '<input type="text" name="attributeValue_'.$arrayId[$i].'_1" size="5" />';

					}

					//$table .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>				

                  //</li>';

				 echo $table ;

	}

	function getProductArray(){

	

		$query = "select ".TBL_SIZE.".*,".TBL_SIZEDESC.".sizeName from ".TBL_SIZE.", ".TBL_SIZEDESC." where 1 and ".TBL_SIZE.".status  = '1' and ".TBL_SIZE.".id = ".TBL_SIZEDESC.".Id and ".TBL_SIZEDESC.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ".TBL_SIZE.".id asc";

		$sql2 = $this->executeQry($query);

		$num2 = $this->getTotalRow($sql2);

		while($line2 = $this->getResultObject($sql2)){

			$arrayId[] = $line2->id;

		}

		

		if(count($arrayId) > 0)

			$aary = implode(",",$arrayId);

		else

			$aary = $arrayId;

		return $aary;

	}

	

	function getProductArraySize(){

		$query = "select ".TBL_SIZE.".*,".TBL_SIZEDESC.".sizeName from ".TBL_SIZE.", ".TBL_SIZEDESC." where 1 and ".TBL_SIZE.".status  = '1' and ".TBL_SIZE.".id = ".TBL_SIZEDESC.".Id and ".TBL_SIZEDESC.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ".TBL_SIZE.".id asc";

		$sql2 = $this->executeQry($query);

		$num2 = $this->getTotalRow($sql2);

		return $num2;

	}	

	

	

	function getAttributeName($productId){

	

	

	    $query = "select ".TBL_SIZE.".*,".TBL_SIZEDESC.".sizeName from ".TBL_SIZE.", ".TBL_SIZEDESC." where 1 and ".TBL_SIZE.".status  = '1' and ".TBL_SIZE.".id = ".TBL_SIZEDESC.".Id and ".TBL_SIZEDESC.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ".TBL_SIZE.".id asc";

		$sql2 = $this->executeQry($query);

		$num2 = $this->getTotalRow($sql2);

		$table = '<div style="padding:10px;margin:5px 0px 0px 30px;">

		<div style="width:850px;">';

		if($type != 'edit'){

			while($line2 = $this->getResultObject($sql2)){

		 		$arrayId12[] = $line2->id;

		 		$price = $this->getAttributePrice($line2->id,$productId);

				$weight = $this->getAttributeweight($line2->id,$productId);

		 		if($price)

		 			$checked = "checked = 'checked'";

		 	    else

		 	    	$checked = "";

         		$table .= '<div style="width:90px;float:left;margin:5px 0px 0px 50px;"><div style="width:90px;">'.$line2->sizeName . '<input type = "checkbox" name="size'.$line2->id.'" "'.$checked.'" /></div><div style="width:90px;">Price:<input type="text" name="price'.$line2->id.'" value="'.$price.'" size=4 />' ;

				if($this->fetchValue(TBL_SYSTEMCONFIG,'systemVal',"systemName='ALLOW_WEIGHT'") == 2)

					$table .= '<br/>weight:<input type="text" name="weight'.$line2->id.'" value="'.$weight.'" size=4 />';

					$table .='</div></div>';

			}

		  }

		$table .='</div>';

		$queryValue = "SELECT * FROM ".TBL_MAINMESURMENT." WHERE pid = '".$productId."'";

		$sqlValue = $this->executeQry($queryValue);

		$num = $this->getTotalRow($sqlValue);

		if($num){

			$table .='<br/><div style="float:left; padding-top:10px;padding-top:10px;width:650px; ">';

			$j=1;

			while($line3 = $this->getResultObject($sqlValue)){

				//$table .= '<div ><input type="text" name="attributeName'.$j.'"  value="'.$line3->name.'" id="m__font__Image" size = "5"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

			    foreach($arrayId12 as $arrid){

					$query1 = "select * from ".TBL_MAINSIZEATTRIBUTE." where productId = '".$productId."' and mesurment ='".$line3->id."' and sizeId = '".$arrid."'";

					$sqlValue1 = $this->executeQry($query1);

					$line4 = $this->getResultObject($sqlValue1);

						//echo $line4->lineValue.",";

					//$table .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="attributeValue_'.$arrid.'_'.$j.'" value="'.$line4->lineValue.'" size="5" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

				}

				$j++;

			}

		}

		if($num == 0){

			//echo $this->getProductSize('','','');

			$k = $num+1;

			//$table .= '<div id="ttfFileUpload" style="padding-top:80px;"; ><input type="text" name="attributeName'.$k.'"  value="" id="m__font__Image" size = "5"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';  

			for($ii = 0; $ii< count($arrayId12); $ii++){

				//$table .="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

				//$table .= '<input type="text" name="attributeValue_'.$arrayId12[$ii].'_'.$k.'" size="5" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

			}

			//echo "</div>";

		}

		if($num != 0){ 

			$k = $num+1;

			//$table .= '<br/><div id="ttfFileUpload">

                    //<input type="text" name="attributeName'.$k.'"  value="" id="m__font__Image" size = "5"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; 

			 

			for($ii = 0; $ii< count($arrayId12); $ii++){

				//$table .="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

				//$table .= '<input type="text" name="attributeValue_'.$arrayId12[$ii].'_'.$k.'" size="5" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

			}

			//$table .='</div>';

		}

		//$table .='</div></div>';

		echo $table;

	}

	

	function getAttributePrice($id , $productid){

		$query = "select price from ".TBL_MAINSIZEPRICE." where sizeId='$id' and productId = '$productid'";

		$sql = $this->executeQry($query);

		$line2 = $this->getResultObject($sql);

		return  $line2->price;

	}

	

	function getAttributeweight($id , $productid){

	

		$query = "select weight from ".TBL_MAINSIZEPRICE." where sizeId='$id' and productId = '$productid'";

		$sql = $this->executeQry($query);

		$line2 = $this->getResultObject($sql);

		return  $line2->weight;

	

	}

	

	function findHiddenValue($productId){

		    $queryValue = "SELECT * FROM ".TBL_MAINMESURMENT." WHERE pid = '".$productId."'";

			$sqlValue = $this->executeQry($queryValue);

			$num = $this->getTotalRow($sqlValue);

			return $num;

	

	}

	

	

	function updateProductSize($post){

	   

	    

	    $sql1 = $this->executeQry("delete from ".TBL_MAINSIZEATTRIBUTE." where productId = '".$post[productId]."'");

		$sql1 = $this->executeQry("delete from ".TBL_MAINSIZEPRICE." where productId = '".$post[productId]."'");

		$sql1 = $this->executeQry("delete from ".TBL_MAINMESURMENT." where pid  = '".$post[productId]."'");

	    $query = "select ".TBL_SIZE.".*,".TBL_SIZEDESC.".sizeName from ".TBL_SIZE.", ".TBL_SIZEDESC." where 1 and ".TBL_SIZE.".status  = '1' and ".TBL_SIZE.".id = ".TBL_SIZEDESC.".Id and ".TBL_SIZEDESC.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ".TBL_SIZE.".id asc";

		$sql2 = $this->executeQry($query);

		$num2 = $this->getTotalRow($sql2);

		while($line2 = $this->getResultObject($sql2)){

			$arrayId[] = $line2->id;

		 	$sizeId[] = $line2->id;

		}

		for($kw = 1; $kw <= $post[theValue]; $kw++){

			$attributeName = "attributeName".$kw;

			if($post[$attributeName] != ''){

				$querymesurment = "insert into ".TBL_MAINMESURMENT." set name  = '".$post[$attributeName]."',  pid  = '".$post[productId]."'";

				$sql = $this->executeQry($querymesurment);

				$mesurmentId[] = mysql_insert_id();

			}

		}

		$i=0;

		$j = $arrayId[$i];

		while($arrayId[$i] ){

			$sizeid = "size".$j;

			$priceid = "price".$arrayId[$i];

			$weightid = "weight".$arrayId[$i];

		    if( $post[$priceid] != '' && $post[$sizeid]){

				if($post[$weightid])

					$weight = " , weight = '".$post[$weightid]."'";

				$query = "insert into ".TBL_MAINSIZEPRICE." set productId = '".$post[productId]."', sizeId = '$arrayId[$i]', price = '$post[$priceid]' $weight ";

				$sql = $this->executeQry($query);

			}

			for($kk = 1,$m=0; $kk <= $post[theValue]; $kk++,$m++){

				$attributeName = "attributeName".$kk;

		 		if($post[$attributeName] != '' && $post[$sizeid] ){

					$attributeValue = "attributeValue_".$j."_".$kk;

		 			$query = "insert into ".TBL_MAINSIZEATTRIBUTE." set mesurment = '".$mesurmentId[$m]."', sizeId = '".$j."',  productId  = '".$post[productId]."' , lineValue = '".$post[$attributeValue]."'";

					$sql = $this->executeQry($query);

				//$insert_id = mysql_insert_id();

				}

		    }

			$i++;

		 	$j=$arrayId[$i];

		}

		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been updated successfully!!!");

		echo "<script>window.location.href='editMainProduct.php?id=".base64_encode($post[productId])."&page=$post[page]'</script>";

	}

		function getViewImageUpload(){

	

		$genTable = '';

		$query = "select ".TBL_VIEW.".*,".TBL_VIEWDESC.".viewName from ".TBL_VIEW.", ".TBL_VIEWDESC." where 1 and ".TBL_VIEW.".status  = '1' and ".TBL_VIEW.".id = ".TBL_VIEWDESC.".viewId and ".TBL_VIEWDESC.".langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ".TBL_VIEW.".sequence asc" ;

		$sql = $this->executeQry($query);

		$num = $this->getTotalRow($sql);

		if($num > 0){

			$send = array();

			while($line = $this->getResultObject($sql)){

				

				if($line->isValidate == '1'){

					$viewName =  $line->viewName.'<span class="spancolor">*</span>';

					$imgWithBorderId = "m__image";

					$imgWithoutBorderId = "m__image";

				}

				else{

					$viewName =  $line->viewName;

					$imgWithBorderId = "image";

					$imgWithoutBorderId = "image";

				}

				$this->vars["id"] = $line->id;

				$this->vars["view"] = $viewName;

				$this->vars["isValidate"] = $line->isValidate;

				$this->vars["imgWithBorderId"] = $imgWithBorderId;

				$this->vars["imgWithoutBorderId"] = $imgWithoutBorderId;

				$this->vars["imgWithBorderName"] = "prodImageWithBorder".$line->id;

				$this->vars["imgWithoutBorderName"] = "prodImageWithoutBorder".$line->id;

				$send[] = $this->vars;

				unset($this->vars);

			}

		}

	return $send;

	}

	function getImageName($id)

	{

		$imageName=$this->fetchValue(TBL_MAINPRODUCT,"imageName","id= '".$id."'");

		return $imageName;

	}

	function getImageNameview($id)

	{

		$imageName=$this->fetchValue(TBL_MAIN_PRODUCT_VIEW,"imageWithBorder","product_id= '".$id."'");

		return $imageName;

	}

	

	function getCompanyForDeviceEdit($companyId,$deviceId){

		 $sql = "SELECT tbl1.id, tbl2.categoryName FROM ".TBL_CATEGORY." AS tbl1 INNER JOIN ".TBL_CATEGORY_DESCRIPTION." AS tbl2 ON (tbl1.id = tbl2.catId) WHERE 1=1 AND tbl2.langId = '".$_SESSION['DEFAULTLANGUAGE']."' AND tbl1.parent_id != '0' AND tbl1.path like '%$deviceId%'";

		$query = $this->executeQry($sql);

		$num = $this->getTotalRow($query);

		$tbl = '';

		if($companyId == '0')

			$psel = "selected";

		else	

			$psel = "";

			

		$tbl = '<select name="deviceCompany" id="m__Company">'

				.'<option value="" '.$psel.'>Please Select Company</option>';

		if($num > 0){

			while($line = $this->getResultObject($query)){

				$sel = ($line->id == $companyId) ? "selected":"";

				$tbl .= '<option value="'.stripslashes($line->id).'" '.$sel.'>'.stripslashes($line->categoryName).'</option>';

			}

		}

		$tbl .= '</select>';

	return $tbl; 	

	}

	

		function getViewImageUploadEdit($pid,$arr_error){

		

		$query = "select TBL1.id, TBL1.isValidate, TBL2.viewName from ".TBL_VIEW." AS TBL1 INNER JOIN ".TBL_VIEWDESC." AS TBL2 ON (TBL1.id = TBL2.viewId) where 1 and TBL1.status  = '1' and TBL2.langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by TBL1.sequence asc" ;

		

		//$query = "SELECT * FROM ".TBL_PRODUCT_VIEW." WHERE product_id = '$pid'";

		$sql = $this->executeQry($query);

		$num = $this->getTotalRow($sql);

		if($num > 0){

			$send = array();

			while($line = $this->getResultObject($sql)){

				$queryView = "SELECT * FROM ".TBL_MAIN_PRODUCT_VIEW." WHERE product_id = '$pid' AND 	view_id = '$line->id'";

				$sqlView = $this->executeQry($queryView);

				$numView = $this->getTotalRow($sqlView);

				if($numView > 0){

					if($line->isValidate == '1'){

						$viewName = $line->viewName.'<span class="spancolor">*</span>';

						$delImage = "&nbsp;";

					}else{

						$viewName = $line->viewName;

						$delImage = "<a href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){deleteProductView('".base64_decode($_GET['id'])."','".$line->id."');}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";



					}

					

					$this->vars["view"] = $viewName;

					$this->vars["delImage"] = $delImage;

					

					$html = '<table border="0">';

					while($lineView = $this->getResultObject($sqlView)){

					$sel = ($lineView->isDefault == '1')?"checked":"";

					$html .= '<tr>

									<td>With Border</td>

									<td>

									<img src="'.__MAINPRODUCTTHUMB1__.$lineView->imageWithBorder.'" width="50" height="50" />

									</td>

								</tr>

								<tr>

									<td>&nbsp;</td>

									<td>

										<div>

											<input type="file" name="prodImageWithBorder_'.$line->id.'" />

											<p  style="padding-left:0px; color:#FF0000;">'.$arr_error["prodImageWithBorder_".$line->id.""].'</p>

										</div>	

									</td>

								</tr>

								<tr>

									<td>Without Border</td>

									<td>

									<img src="'.__MAINPRODUCTTHUMB1__.$lineView->imageWithoutBorder.'" width="50" height="50" />												

									</td>

								</tr>

								<tr>

									<td>&nbsp;</td>

									<td>

										<div>

										<input type="file" name="prodImageWithoutBorder_'.$line->id.'" />

										<p  style="padding-left:0px; color:#FF0000;">'.$arr_error["prodImageWithoutBorder_".$line->id.""].'</p>

										</div>

									</td>

								</tr>

								<tr>

									<td>Make this Default</td>

									<td><input type="radio" name="isDefault" value="'.$line->id.'" '.$sel.'>

									</td>

								</tr>';

						}

						$html .= '</table>';

						$this->vars["html"] = $html;	

				}else{

					$this->vars["view"] = '<a onclick="addProduct_image('.$line->id.');" style="cursor:pointer;">Add image for '.$line->viewName.'</a>';

					

					if(isset($_FILES['prodImageWithBorder_'.$line->id.'']['name']) || isset($_FILES['prodImageWithoutBorder_'.$line->id.'']['name']))

					{

						$this->vars["html"] = $this->setproductImagePost($line->id,$arr_error);

					}else

					$this->vars["html"] = "";

				}

				/*$this->vars["imageWithBorder"] = __PRODUCTTHUMBPATH__.$line->imageWithBorder;

				$this->vars["imageWithoutBorder"] = __PRODUCTTHUMBPATH__.$line->imageWithoutBorder;*/

				$this->vars["id"] = $line->id;

				$send[] = $this->vars;

				unset($this->vars);

			}

		return $send;

		}

	}

	

	function getProductView($pid){

	

		$query = "SELECT * FROM ".TBL_MAIN_PRODUCT_VIEW." WHERE product_id = '$pid'";

		$sql = $this->executeQry($query);

		$num = $this->getTotalRow($sql);

		if($num > 0){

			$send = array();

			while($line = $this->getResultObject($sql)){

				$send[] = $line;

			}

		return $send;

		}

	}

	function setproductImagePost($viewId,$arr_error){

		

		$html = '<table border="0">';

		$html .= '<tr>

					<td>&nbsp;</td>

					<td>With Border</td>

					<td><div>

						<input type="file" id="image" name="prodImageWithBorder_'.$viewId.'">

						<p  style="padding-left:0px; color:#FF0000;">'.$arr_error["prodImageWithBorder_".$viewId.""].'</p>

						</div>

					</td>

				 </tr>

				 <tr>

					<td>&nbsp;</td>

					<td>Without Border</td>

					<td>

					  <div>

					  <input type="file" id="image" name="prodImageWithoutBorder_'.$viewId.'">

					  <p  style="padding-left:0px; color:#FF0000;">'.$arr_error["prodImageWithoutBorder_".$viewId.""].'</p>

					  </div>

					</td>

				 </tr>

				 <tr>

					<td>&nbsp;</td>

					<td>Make this Default&nbsp;</td>

					<td>

					  <input type="radio" name="isDefault" value="'.$viewId.'">

					</td>

				 </tr>';

		$html .= '</table>';

		return $html;

	}
        
        

}// End Class

?>	