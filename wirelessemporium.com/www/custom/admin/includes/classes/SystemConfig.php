<?php 
session_start();
class SystemConfig extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }

	function addConfiguration($post,$file){
		//echo "<pre>"; print_r($post); print_r($file); echo "</pre>";exit;
		$_SESSION['SESS_MSG'] = "";
		$sql = $this->executeQry("select * from ".TBL_SYSTEMCONFIG." where 1");
		$num = $this->getTotalRow($sql);
		if($num > 0) {			
			while($line = $this->getResultObject($sql)) {				
				if($line->systemName != 'SITE_LOGO' && in_array($line->systemName,array_keys($post))) {
					$query = "update ".TBL_SYSTEMCONFIG." set systemVal = '".addslashes($post[$line->systemName])."' where systemName = '".$line->systemName."'"; 
					if($this->executeQry($query)) 
				 		$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);
				}else if($line->systemName == 'SITE_LOGO') {					
					if($file['SITE_LOGO']['name']){
						$filename = stripslashes($file['SITE_LOGO']['name']);
						$extension = findexts($filename);
						$extension = strtolower($extension);
				
						$image_name = date("Ymdhis").time().rand().'.'.$extension;
						$target    = __SITELOGOORIGINAL__.$image_name;					
						if($this->checkExtensions($extension)) {	
							$prevLogo = $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'SITE_LOGO'");							
							$filestatus = move_uploaded_file($file['SITE_LOGO']['tmp_name'], $target);								
							if($filestatus){
								$imgSource = $target;	                                                               
								$thumb = __SITELOGOTHUMB__.$image_name;	
								$large = __SITELOGOLAREGE__.$image_name;								
								
								exec(IMAGEMAGICPATH." $imgSource -thumbnail 100x100 $thumb");
								exec(IMAGEMAGICPATH." $imgSource -thumbnail 250x250 $large");								
								
								@unlink(__SITELOGOORIGINAL__.$prevLogo);
								@unlink(__SITELOGOTHUMB__.$prevLogo);
								@unlink(__SITELOGOLAREGE__.$prevLogo);
							} 								
							$query = "update ".TBL_SYSTEMCONFIG." set systemVal = '".$image_name."' where systemName = 'SITE_LOGO'"; 
							if($this->executeQry($query)) 
						 		$this->logSuccessFail('1',$query);		
							else 	
								$this->logSuccessFail('0',$query);
						} else {
							$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");
						} 	
					}
				}
			}   
		}
	}	
}// End Class
?>	