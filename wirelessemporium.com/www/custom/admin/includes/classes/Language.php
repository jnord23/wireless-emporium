<?php 
session_start();
class Language extends MySqlDriver{
//Start : Constructor====================================
    function __construct() {
        $this->obj = new MySqlDriver;       
    }
// Start : Show details==============================================

	function valDetail() {
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
			$con = " AND languageName LIKE '%$searchtxt%' OR languageCode LIKE  '%$searchtxt%'";
		}
		
		$query = "select * from ".TBL_LANGUAGE." where 1=1 and isDeleted='0' $con ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			$genTable = '';
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			
			$orderby = $_GET[orderby]? $_GET[orderby]:"languageName";
			$order = $_GET[order]? $_GET[order]:"ASC";
			
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
		
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
                                        $status=($line->status)?"Active":"Inactive";					
					if ($line->isDefault==1){
						$isDefault = "checked='checked'";
						$onclickstatus = '';
						$chkbox = '';
					}else{
						$isDefault = "";
						$onclickstatus = ' onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'language\')"';
						$chkbox = '<input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox">';
					}
					$genTable .= '<div class="'.$highlight.'">
								 <ul>
								 	<li style="width:50px;">&nbsp;&nbsp;'.$chkbox.'</li>
									<li style="width:80px;">'.$i.'</li>
									<li style="width:150px;">'.$line->languageName.'</li>
									<li style="width:50px;">'.$line->languageCode.'</li>
									<li style="width:40px;"><img src="'.__FLAGTHUMBPATH__.$line->languageFlag.'"</li>
									<li style="width:80px;"><input type="checkbox" '.$isDefault.' disabled=disabled class="welcheckbox"></li>
									<li style="width:100px;">';
					if($menuObj->checkEditPermission()) {							
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" '.$onclickstatus.' >'.$status.'</div>';
					}				
																											
					$genTable .= '</li><li style="width:80px;"><a rel="shadowbox;width=705;height=325" title="'.$line->languageName.'" href="viewLanguage.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></li><li style="width:90px;">';
					if($menuObj->checkEditPermission()) {					
						$genTable .= '<a href="editLanguage.php?id='.base64_encode($line->id).'&page='.$page.'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a>';
					}	
					$genTable .= '</li><li style="width:55px;">';
				
					if($menuObj->checkDeletePermission() && (!$line->isDefault)) {					
						$genTable .= "<a href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Language  ?')){window.location.href='pass.php?action=language&type=delete&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					}
					$genTable .= '</li></ul></div>';
					$i++;	
				}
				switch($recordsPerPage)
				{
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}

//Start : Change Status============================================
    function changeValueStatus($get) {
        $status=$this->fetchValue(TBL_LANGUAGE,"status","1 and id = '$get[id]'");
        if($status==1) {
            $stat= 0;
            $status="Inactive";
        } else 	{
            $stat= 1;
            $status="Active";
        }
        $sql = "update ".TBL_LANGUAGE." set status = '$stat', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
        $rst = $this->executeQry($sql);
        if($rst){
            $this->logSuccessFail("1",$sql);
        }else{
            $this->logSuccessFail("0",$sql);
        }
        echo $status;		
    }
	
//Start : Delete Values==================================================

    function deleteValue($get) {
        $sql = "update ".TBL_LANGUAGE." set isDeleted = '1', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
        $rst = $this->executeQry($sql);
        if($rst){
            $this->logSuccessFail("1",$sql);
        }else{
            $this->logSuccessFail("0",$sql);
        }
        $_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageLanguage.php?page=$post[page]&limit=$post[limit]';</script>";
    }
//Start : Add Lanaguage===============================================================================	
	function addNewLanguage($post,$file) {
		$date = date("Y-m-d h:i:s");
		$_SESSION['SESS_MSG'] = "";
		if($file['languageFlag']['name']){
  			$filename = stripslashes($file['languageFlag']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);
	
			$image_name = date("Ymdhis").time().rand().'.'.$extension;
			$target    = __FLAGORIGINAL__.$image_name;
			$filestatus = 	move_uploaded_file($file['languageFlag']['tmp_name'], $target);
			@chmod($target, 0777);
			if($filestatus){
				$imgSource = $target;	 
				$thumb = __FLAGTHUMB__.$image_name;	
				$size = $this->findSize('LANGFLAG_THUMB_WIDTH','LANGFLAG_THUMB_HEIGHT',20,20);	
				@chmod(__FLAGTHUMB__,0777);				
				exec(IMAGEMAGICPATH." $imgSource -thumbnail $size $thumb");
				$sql = "INSERT INTO ".TBL_LANGUAGE." SET `languageName`='".$post['languageName']."', `languageCode`='".strtolower($post['languageCode'])."', `languageFlag`='$image_name', `status`='1', `addDate`='$date', addedBy = '".$_SESSION['ADMIN_ID']."'";
				$rst = $this->executeQry($sql);
				if($rst){
					$this->logSuccessFail("1",$sql);
				}else{
					$this->logSuccessFail("0",$sql);
				}				
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been added successfully.!!!");	
				header('Location:addLanguage.php?page=$post[page]');
				/*echo "<script language=javascript>window.location.href='manageLanguage.php?page=$post[page]';</script>";*/exit;	
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","There is some error to upload flag.!!!");
			}
		}else{
			$_SESSION['SESS_MSG'] =msgSuccessFail("fail","Please upload flag.!!!");
		}
	}
//Start : Edit Language===========================================================================================	
	function editLanguage($post,$file) {
		//echo "<pre>"; print_r($post);echo "</pre>";exit;
		$date = date("Y-m-d h:i:s");
		$_SESSION['SESS_MSG'] = "";
		$isdefault = (isset($post[isDefault]))?1:0;
		
		$sql = "UPDATE ".TBL_LANGUAGE." SET `languageName`='".$post['languageName']."',isDefault='$isdefault', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' ";
		if($file['languageFlag']['name']){
  			$filename = stripslashes($file['languageFlag']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);
			$image_name = date("Ymdhis").time().rand().'.'.$extension;
			$target    = __FLAGORIGINAL__.$image_name;
			$filestatus = 	move_uploaded_file($file['languageFlag']['tmp_name'], $target);
			@chmod($target, 0777);
			if($filestatus){
				$imgSource = $target;					
				$thumb = __FLAGTHUMB__.$image_name;
				$size = $this->findSize('LANGFLAG_THUMB_WIDTH','LANGFLAG_THUMB_HEIGHT',20,20);	
				@chmod(__FLAGTHUMB__,0777);				
				exec(IMAGEMAGICPATH." $imgSource -thumbnail $size $thumb");			
				$preImg = $this->fetchValue(TBL_LANGUAGE,"languageFlag","id='$post[langId]'");
				_unlink(__FLAGORIGINAL__.$preImg);
				_unlink(__FLAGTHUMB__.$preImg);
				$sql .= ", `languageFlag`='$image_name' ";
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","There is some error to upload flag.!!!");
			}
                        $_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been updated successfully");
		}
		if($isdefault){
                    $_SESSION['DEFAULTLANGUAGE']  = $post[langId];
                    $this->executeQry("update ".TBL_LANGUAGE." set isDefault='0', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id != '$post[langId]'");
                    $sql .= " ,status = '1' ";
		}	
		$sql .= "  WHERE id='$post[langId]'";	
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
                $_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been updated successfully");
		echo "<script language=javascript>window.location.href='manageLanguage.php?page=$post[page]&limit=$post[limit]';</script>";exit;
	}

//Start : Delete Multiple values=================================================================================
    function deleteAllValues($post){
        if(($post[action] == '')){
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
            echo "<script language=javascript>window.location.href='manageLanguage.php?page=$post[page]&limit=$post[limit]';</script>";
            exit;
        }		
        //Delete Selected=====================================================
        if($post[action] == 'deleteselected'){
            $delres = $post[chk];
            $numrec = count($delres);
            if($numrec>0){
                foreach($delres as $key => $val){
                    $sql = "update ".TBL_LANGUAGE." set isDeleted = '1', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$val'";
                    $rst = $this->executeQry($sql);
                    if($rst){
                        $this->logSuccessFail("1",$sql);
                    }else{
                        $this->logSuccessFail("0",$sql);
                    }
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
            }else{
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the record!!!");
            }
        }
        //Enable selected=========================================================
        if($post[action] == 'enableall'){
            $delres = $post[chk];
            $numrec = count($delres);
            if($numrec>0){
                foreach($delres as $key => $val){
                    $sql="update ".TBL_LANGUAGE." set status ='1', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
                    $rst = $this->executeQry($sql);
                    if($rst){
                        $this->logSuccessFail("1",$sql);
                    }else{
                        $this->logSuccessFail("0",$sql);
                    }
                }
                $_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
            }else{
                $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
            }
        }
        //Disable Selected================================================================
        if($post[action] == 'disableall'){
            $delres = $post[chk];
            $numrec = count($delres);
            if($numrec>0){
                foreach($delres as $key => $val){
                    $sql="update ".TBL_LANGUAGE." set status ='0', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
                    $rst = $this->executeQry($sql);
                    if($rst){
                        $this->logSuccessFail("1",$sql);
                    }else{
                        $this->logSuccessFail("0",$sql);
                    }
                }
                $_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
            }else{
                $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
            }
        }
        echo "<script language=javascript>window.location.href='manageLanguage.php?page=$post[page]';</script>";exit;
    }	
//Start : Get Result By ID==========================================================
    function getResult($id) {
        $sql_lang = $this->executeQry("select * from ".TBL_LANGUAGE." where id = '$id'");
        $num_lang = $this->getTotalRow($sql_lang);
        if($num_lang > 0) {
            return $line_lang = $this->getResultRow($sql_lang);	
        } else {
            redirect('manageLanguage.php');
        }	
    }
//Start: Fatech Attributes=====================================================================
    function fetattribute($lan){
        $sql1="select  * FROM ".TBL_LANGATTRIBUTE." where langId='$lan'";
        $rst1=$this->executeQry($sql1);
        $row1=$this->getResultRow($rst1);
        $sql="SHOW COLUMNS FROM ".TBL_LANGATTRIBUTE."";
        $rst=$this->executeQry($sql);
        echo "  <div class='main-body-div4' id='mainDiv'><form name='languageform' action='' method='post'>
        <div class='add-main-body-left-new'>
        <ul>
        <input type='hidden' name='langId'  value='$lan'>";
        while($row=$this->getResultRow($rst)){
            $name=$row[Field];
            if($name!='id' and  $name!='langId'){
                echo '<li class="lable" style="vertical-align:top;">'.ucwords(str_replace("_"," ",$row[Field])).'</li><li ><textarea  name="'.$row[Field].'"  cols="35" rows="2">'.stripslashes($row1[$name]).'</textarea> </li>';
            }
        }
        echo "</ul></div><div class='main-body-sub'><input type='submit' name='submit' value='  Save  ' class='main-body-sub-submit'> </div></form></div>";
    }

//Start : Add Language=============================================================
	function addlanguage(){
		$rst=$this->selectQry(TBL_LANGATTRIBUTE,"langId='$_POST[langId]' ",'','');
		$num=$this->getTotalRow($rst);
		$row=$this->getResultRow($rst);
		$sql="SHOW COLUMNS FROM ".TBL_LANGATTRIBUTE."";
		$rst=$this->executeQry($sql);
		$i=0;
		$name="";
		while($row1=$this->getResultRow($rst)){
			$_POST[$fiels]=addslashes($_POST[$fiels]);
			if($i==0 && $row1[Field]!='id'){
				$fiels=addslashes($row1[Field]);
				$name.="$row1[Field]='".$_POST[$fiels]."'"; 
				$i++;
			}elseif($i>0 && $row1[Field]!='langId'){
				$fiels=$row1[Field];
				$name.=",$row1[Field]='".addslashes($_POST[$fiels])."'"; 
			}
		}
		if($num){
			$sql="update  ".TBL_LANGATTRIBUTE." set  $name  where  langId='$_POST[langId]' and id='$row[id]'";
			$this->executeQry($sql);
		}else{
			$sql="SHOW COLUMNS FROM ".TBL_LANGATTRIBUTE ;
			$rst=$this->executeQry($sql);
			$i=0;
			$name="";
			while($row1=$this->getResultRow($rst)){
				if($i==0 ){
					$fiels=$row1[Field];
					$name.="$row1[Field]"; 
				}elseif($i>0){
					$name.=",$row1[Field]"; 
				}
			$i++;
			}
			$sql="SHOW COLUMNS FROM ".TBL_LANGATTRIBUTE;
			$rst=$this->executeQry($sql);
			$i=0;
			$name1="";
			while($row1=$this->getResultRow($rst))
			{
				$fiels=$row1[Field];
				$_POST[$fiels]=addslashes($_POST[$fiels]);
				if($i==0 )
				{
					$name1.="''"; 
				}
				else if($i>0 )
				{
					$name1.=",'$_POST[$fiels]'"; 
				}
				$i++;
			}
			$sql="INSERT INTO ".TBL_LANGATTRIBUTE." ($name) VALUES ($name1)";
			$this->executeQry($sql);
		}
		foreach ($_POST as $key => $value) {
			if($value != "" && $key != 'submit' ) {
				$key=strtoupper(str_replace("_","_",$key));
				$value=stripslashes($value);
				$define=$define."define('LANG_$key','".addslashes($value)."');\n";
			}
		}
		$rst=$this->selectQry(TBL_LANGUAGE," status='1' and id='$_POST[langId]' ",'','');
		$row=$this->getResultRow($rst);
                createDirectory(__DIR_LANGUAGES__);
		$ourFileName = __DIR_LANGUAGES__.strtolower($row[languageCode]).".inc.php";
		if (file_exists($ourFileName)) {
			@unlink ($ourFileName);
		} 
		$ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
		$somecontent="<?php\n$define?>";
		fwrite($ourFileHandle, $somecontent);
		fclose($ourFileHandle);
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Information has been save successfully!!!");
	}
	
    //Start: Choose Language==========================================
    function choselanguage($id){
        $rst=$this->selectQry(TBL_LANGUAGE," status='1' and id='$id' ",'','');
        $row=$this->getResultRow($rst);
        return  $row;
    }
	
	
    //Start: Check Language Name Existance===============================================
    function islanguageNameExit($langname,$id=''){
        $langname = trim($langname);
        $rst = $this->selectQry(TBL_LANGUAGE,"languageName='$langname' AND id!='$id' AND isDeleted='0'","","");
        $row = $this->getTotalRow($rst);
        return $row;
    }
	
    //Start : Check Language Code Existance================================================
    function islanguageCodeExit($languageCode,$id=''){
        $languageCode = trim($languageCode);
        $rst = $this->selectQry(TBL_LANGUAGE,"languageCode='$languageCode' AND id!='$id'  AND isDeleted='0' ","","");
        $row = $this->getTotalRow($rst);
        return $row;
    }

	
	
}// End Class=======================================
?>	