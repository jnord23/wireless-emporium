<?php 
@ob_start();
@session_start();
class BreadCrum extends MySqlDriver{
        //Start : Constructor=====================================
        function __construct() {
            $this->obj = new MySqlDriver;
            $this->unsetBreadCrum(); 
        }
	
        //Start : Current Page name================================
	function curPageName() {
 		return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
	}
	//Start : Get Url ==========================================
	function getUrl($id){
		$current_link = $this->fetchValue(TBL_MENU,"menuUrl","1 and menuId = '$id'");
		return $current_link;
	}
	//Start : Menu ID============================================
	function menuId(){
		$current_page = $this->curPageName();
		$current_menu_id = $this->fetchValue(TBL_MENU,"menuId","1 and menuUrl = '$current_page'");
		return $current_menu_id; 
	}
	//Start : Sub Menu Name==================================================
	function subMenuName(){
			$current_page = $this->curPageName();
			if(strstr(strtoupper($current_page), 'EDIT') != false)
				$submenu_name = '&nbsp;Edit';
			if(strstr(strtoupper($current_page), 'ADD') != false)	
				$submenu_name = '&nbsp;Add';
			
			return $submenu_name;
	}
	//Start : Unset Bread Crum=========================================
	function unsetBreadCrum(){
		$current_page = $this->curPageName();
		$query = "SELECT * FROM ".TBL_MENU." WHERE menuUrl = '$current_page'";
		$this->executeQry($query);
		if(mysql_affected_rows() > 0)
			unset($_SESSION['breadcrum']);
	}
	
	//Start : Set Bread Crum========================================
	function setBreadCrum(){
		$_SESSION['breadcrum']['maintab'] = (!isset($_SESSION['breadcrum']['maintab']))?$this->menuId():$_SESSION['breadcrum']['maintab'];
		$_SESSION['breadcrum']['subtab'] = $this->subMenuName();
	}
	
        //Start : Get Bread Crum======================================
	function getBreadCrum(){
		$current_page = $this->curPageName();
		if($current_page != 'adminArea.php'){
			$this->setBreadCrum();
			$tbl = '';
			$tbl = '<ul>';
			$tbl .= '<li><a href="adminArea.php">Home</a> &gt;&gt; </li>';
			foreach($_SESSION['breadcrum'] as $key =>$value){
				if($key == "maintab"){
					$tbl .= $this->getMainTab($value);
				}
				if($key == "subtab" && $value != ""){
					$tbl .= '<li class="breadcrum_active"> &gt;&gt;'.$value.'</li>';
				}
			}
			echo $tbl;
		}	
	}
	
        //Start : Main Tab===================================
	function getMainTab($id){
	
		$current_page = $this->curPageName();
		$current_parent_id = $this->fetchValue(TBL_MENU,"parentId","1 and menuId = '$id'");
		$parent_menu_name = $this->fetchValue(TBL_MENU,"menuName","1 and menuId = '$current_parent_id' and parentId = '0'");
		$current_menu_name = $this->fetchValue(TBL_MENU,"menuName","1 and menuId = '$id'");
		
		$query = "SELECT * FROM ".TBL_MENU." WHERE menuUrl = '$current_page'";
		$this->executeQry($query);
		if(mysql_affected_rows() > 0){
			$url = '<li class="breadcrum_active">'.$parent_menu_name.'('.$current_menu_name.')'.'</li>';
		}else{
			$url = '<li> <a href="'.$this->getUrl($id).'">'.$parent_menu_name.'('.$current_menu_name.')'.'</a></li>';
		}
		
		return $url;
	}
	
}//End Class===========================
?>	