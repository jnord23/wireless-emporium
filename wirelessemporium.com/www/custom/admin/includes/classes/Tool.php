<?php 
session_start();
class Tool extends MySqlDriver{

//Start : Constructor============================================
    function __construct() {
    $this->obj = new MySqlDriver;       
    }
 
//Start : Show Detail Grid==============================================

    function valDetail() {
        $cond = "1 and ft.id = ftd.id  and ftd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
        if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
            $searchtxt = $_REQUEST['searchtxt'];
            $cond .= " AND (ftd.toolName LIKE '%$searchtxt%' or ft.variableName LIKE '%$searchtxt%') ";
        }
        $query = "select ft.*,ftd.toolName from ".TBL_TOOL." as ft , ".TBL_TOOLDESC." as ftd where $cond ";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        $menuObj = new Menu();
        $page =  $_REQUEST['page']?$_REQUEST['page']:1;
        if($num > 0) {			
            //-------------------------Paging------------------------------------------------			
            $paging = $this->paging($query); 
            $this->setLimit($_GET[limit]); 
            $recordsPerPage = $this->getLimit(); 
            $offset = $this->getOffset($_GET["page"]); 
            $this->setStyle("redheading"); 
            $this->setActiveStyle("smallheading"); 
            $this->setButtonStyle("boldcolor");
            $currQueryString = $this->getQueryString();
            $this->setParameter($currQueryString);
            $totalrecords = $this->numrows;
            $currpage = $this->getPage();
            $totalpage = $this->getNoOfPages();
            $pagenumbers = $this->getPageNo();		
            //-------------------------Paging------------------------------------------------
            $orderby = $_GET[orderby]? $_GET[orderby]:"id";
            $order = $_GET[order]? $_GET[order]:"DESC";   
            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
            $rst = $this->executeQry($query); 
            $row = $this->getTotalRow($rst);
            if($row > 0) {			
                $i = 1;			
                while($line = $this->getResultObject($rst)) {
                    $highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
                    $div_id = "status".$line->id;
                    $status=($line->status)?"Active":"Inactive";                   

                    $genTable .= '<div class="'.$highlight.'">';
                    $genTable .= '<ul>';
                    $genTable .= '<li style="width:50px;">&nbsp;&nbsp;<input name="chk[]" value="'.$line->id.'" type="checkbox"></li>';
                    $genTable .= '<li style="width:50px;">'.$i.'</li>';

                    $genTable .= '<li style="width:180px;">'.substr($line->variableName, 0,40 ).'</li>';
                    $genTable .= '<li style="width:320px;">'.substr($line->toolName, 0,40 ).'</li>';
                    //Status ===============================
                    $genTable .= '<li style="width:70px;">';
                    if($menuObj->checkEditPermission()){				
                        $genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'Tool\')">'.$status.'</div>';
                    }
                    $genTable .= '</li>';
                    //View================================
                    $genTable .= '<li style="width:40px;"><a rel="shadowbox;width=705;height=325" title="'.$line->VariableName.'" href="viewTool.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></li>';
                    //Edit ===============================
                    $genTable .= '<li style="width:40px;">';
                    if($menuObj->checkEditPermission()){					
                        $genTable .= '<a href="editTool.php?id='.base64_encode($line->id).'&page='.$page.'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a>';
                    }
                    $genTable .= '</li>';
                    $genTable .= '<li>';
                    if($menuObj->checkDeletePermission()){ 					
                        $genTable .= "<a href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Forum Topic?')){window.location.href='pass.php?action=Tool&type=delete&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
                    }
                    $genTable .= '</li>';
                    $genTable .= '</ul>';
                    $genTable .= '</div>';
                    $i++;	
                }
                switch($recordsPerPage){
                case 10:
                $sel1 = "selected='selected'";
                break;
                case 20:
                $sel2 = "selected='selected'";
                break;
                case 30:
                $sel3 = "selected='selected'";
                break;
                case $this->numrows:
                $sel4 = "selected='selected'";
                break;
                }
                $currQueryString = $this->getQueryString();
                $limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
                $genTable.="
                <div style='overflow:hidden; margin:0px 0px 0px 50px;'>
                <table border='0' width='88%' height='50'>
                <tr>
                <td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
                Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
                <option value='10' $sel1>10</option>
                <option value='20' $sel2>20</option>
                <option value='30' $sel3>30</option> 
                <option value='".$totalrecords."' $sel4>All</option>  
                </select> Records Per Page
                </td>
                <td align='center' class='page_info'>
                <inputtype='hidden' name='page' value='".$currpage."'>
                </td>
                <td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td>
                <td width='0' align='right'>".$pagenumbers."</td>
                </tr>
                </table>
                </div>";	
            }					
        } else {
            $genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
        }	
        return $genTable;
    }
	
//Start : Change Status========================================
    function changeValueStatus($get) {
        $status=$this->fetchValue(TBL_TOOL,"status","1 and id = '$get[id]'");
        if($status==1) {
            $stat= 0;
            $status="Inactive";
        } else 	{
            $stat= 1;
            $status="Active";
        }
        $query = "update ".TBL_TOOL." set status = '$stat', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
        if($this->executeQry($query)) 
            $this->logSuccessFail('1',$query);		
        else 	
            $this->logSuccessFail('0',$query);
        echo $status;		
    }
	
	
//Start : Add Record================================================
	
    function addRecord($post) {       
        //echo "<pre>"; print_r($post); echo "</pre>";exit;
        $query = "insert into ".TBL_TOOL." set  status  = '1', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."',VariableName = '".$post[VariableName]."'";
        $res = $this->executeQry($query);
        $inserted_id = mysql_insert_id();
        if($res) 
            $this->logSuccessFail('1',$query);		
        else 	
            $this->logSuccessFail('0',$query);

        $rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
        $num = $this->getTotalRow($rst);
        if($num){			
            while($line = $this->getResultObject($rst)) {					
                $toolName = 'toolName_'.$line->id;
                $query = "insert into ".TBL_TOOLDESC." set  Id  = '$inserted_id', langId = '".$line->id."' , toolName  = '".$post[$toolName]."' ";	
                if($this->executeQry($query)) 
                    $this->logSuccessFail('1',$query);		
                else 	
                    $this->logSuccessFail('0',$query);
            }	
        }					
        $_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been added successfully.");
        header("Location:addTool.php");exit;	

    }
    
//Start : Delete Single Value=========================================	
    function deleteValue($get) {
        $result=$this->deleteRec(TBL_TOOL,"id='".$get['id']."'");	
        $result1=$this->deleteRec(TBL_TOOLDESC,"id='".$get['id']."'");
        $_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageTool.php?page=$get[page]&limit=$get[limit]';</script>";exit;
    }
	
//Strt : Delete Multiple Values======================================
    function deleteAllValues($post){
        if(($post[action] == '')){
        $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
        echo "<script language=javascript>window.location.href='manageTool.php?page=$post[page]&limit=$post[limit]';</script>";	exit;
        }	
        //Delete Selected==================================
        if($post[action] == 'deleteselected'){
            $delres = $post[chk];
            $numrec = count($delres);
            if($numrec>0){
                foreach($delres as $key => $val){
                    $result=$this->deleteRec(TBL_TOOL,"id='".$val."'");	
                    $result1=$this->deleteRec(TBL_TOOLDESC,"id='".$val."'");
                }
                $_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
            }else{
                $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
            }
        }
        //Enable Selected================================
        if($post[action] == 'enableall'){
            $delres = $post[chk];
            $numrec = count($delres);
            if($numrec>0){
                foreach($delres as $key => $val){
                    //$result=$this->deleteRec(LOANTBL_CATEGORY,"cat_id='$val'");	
                    $sql="update ".TBL_TOOL." set status ='1', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
                    mysql_query($sql);
                }
                $_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
            }else{
                $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
            }
        }
        //Disable Selected================================
        if($post[action] == 'disableall'){
            $delres = $post[chk];
            $numrec = count($delres);
            if($numrec>0){
                foreach($delres as $key => $val){
                    $sql="update ".TBL_TOOL." set status ='0', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
                    mysql_query($sql);
                }
                $_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
            }else{
                $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
            }
        }
        echo "<script language=javascript>window.location.href='manageTool.php?page=$post[page]';</script>";
    }
	
    //Start : Get Result By ID====================================
    function getResult($id) {
        $sql = $this->executeQry("select * from ".TBL_TOOL." where id = '$id'");
        $num = $this->getTotalRow($sql);
        if($num > 0) {
            return $line = $this->getResultObject($sql);	
        } else {
            redirect("manageTool.php");
        }	
    }
	
//Start : Edit Record===============================================		
    function editRecord($post) {	    
        $queryupdate = "update ".TBL_TOOL." set   VariableName = '".$post[VariableName]."' where 1 and id = '$post[id]'";
        if($this->executeQry($queryupdate)) 
            $this->logSuccessFail('1',$queryupdate);	
        else 	
        $this->logSuccessFail('0',$queryupdate);				
        $rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
        $num = $this->getTotalRow($rst);
        if($num){			
            while($line = $this->getResultObject($rst)) {					
                $toolName = 'toolName_'.$line->id;
                $sql = $this->selectQry(TBL_TOOLDESC,'1 and id = "'.$post[id].'" and langId = "'.$line->id.'"','','');
                $numrows = $this->getTotalRow($sql);
                if($numrows == 0) { 
                    $query = "insert into ".TBL_TOOLDESC." set id = '$post[id]', langId = '".$line->id."', toolName = '".$post[$toolName]."' ";
                    if($this->executeQry($query)) 
                        $this->logSuccessFail('1',$query);		
                    else 	
                        $this->logSuccessFail('0',$query);	
                } else {
                    $query = "update ".TBL_TOOLDESC." set  toolName = '".$post[$toolName]."'  where 1 and id = '$post[id]' and langId = '".$line->id."'";
                    if($this->executeQry($query)) 
                        $this->logSuccessFail('1',$query);		
                    else 	
                        $this->logSuccessFail('0',$query);	
                }	
            }	
            $_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been updated successfully");
        }
        echo "<script>window.location.href='manageTool.php?page=$post[page]';</script>";exit;
    }
	
//Start : Check if tool name exists==========================================
	function isToolVariableExists($name,$id=''){            
		$name = trim($name);               
		$rst = $this->selectQry(TBL_TOOL," variableName ='$name' AND id!='$id'  ","","");
		$row = $this->getTotalRow($rst);
		return $row;
	}
	
}// End Class
?>	