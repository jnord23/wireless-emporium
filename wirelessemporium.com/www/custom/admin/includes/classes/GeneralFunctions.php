<?php
session_start();
class GeneralFunctions extends MySqlDriver {

	function __construct(){
		$obj = new MySqlDriver;  
	}
	
	
	function getSystemConfigValue($systemName){
		return $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='$systemName'");
	}
	
// Start : Get Language Text Box========================================
	function getLanguageTextBox($name,$id,$arr_error){
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){
			$genTable = "";
			$i = 0;
			while($line = $this->getResultObject($rst)) {				
				if($i == 0) {
					$genTable .= '
					<li >
					<img src="'.__FLAGTHUMBPATH__.$line->languageFlag.'" title="'.$line->languageName.'" style="margin-bottom: -5px;">  
					<input type="'.$type.'" name="'.$name.'_'.$line->id.'" id="'.$id.'_'.$line->languageCode.'" class="wel" value=""  />
					<p style="padding-left:150px;">'.$arr_error[$name."_".$line->id].'</p> 
					</li> '; 
					
				} else {
						$genTable .= '
						<li class="lable"></li>
						<li>
						<img src="'.__FLAGTHUMBPATH__.$line->languageFlag.'" title="'.$line->languageName.'" style="margin-bottom: -5px;">  
						<input type="'.$type.'" name="'.$name.'_'.$line->id.'" id="'.$id.'_'.$line->languageCode.'" class="wel" value="" /><p style="padding-left:150px;">'.$arr_error[$name."_".$line->id].'</p> 
						</li> ';
						
				}
				
				$i++;
			}	
		}
		return $genTable;
	}	


// Start : Get Language Text Box Edit=================================================================
	function getLanguageEditTextBox($name,$id,$tablename,$tableid,$tableIdName,$arr_error){
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1'   AND isDeleted='0' order by languageName asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){
			$genTable = ""; $i = 0;
			while($line = $this->getResultObject($rst)) {				
				$value = $this->fetchValue($tablename,$name,"$tableIdName=$tableid and langId = '".$line->id."'");
				if($i == 0) {
					$genTable .= '<li ><img src="'.__FLAGTHUMBPATH__.$line->languageFlag.'" title="'.$line->languageName.'" style="margin-bottom: -5px;">  <input type="'.$type.'" name="'.$name.'_'.$line->id.'" id="'.$id.'_'.$line->languageCode.'" class="wel" value="'.stripslashes($value).'" /><p style="padding-left:150px;">'.$arr_error[$name."_".$line->id].'</p> </li> '; 
				} else {
					$genTable .= '<li class="lable"></li><li ><img src="'.__FLAGTHUMBPATH__.$line->languageFlag.'" title="'.$line->languageName.'" style="margin-bottom: -5px;">  <input type="'.$type.'" name="'.$name.'_'.$line->id.'" id="'.$id.'_'.$line->languageCode.'" class="wel" value="'.stripslashes($value).'" /><p style="padding-left:150px;">'.$arr_error[$name."_".$line->id].'</p> </li> '; 
				}					
				$i++;
			}	
		}
		return $genTable;
	}	


// Start : Get Langauge View Text Box============================================================
	function getLanguageViewTextBox($name,$tablename,$tableid,$tableIdName){
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1'   AND isDeleted='0' order by languageName asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){
			$genTable = ""; $i = 0;
			while($line = $this->getResultObject($rst)) {				
				$value = $this->fetchValue($tablename,$name,"$tableIdName=$tableid and langId = '".$line->id."'");
				if($i == 0) {
					$genTable .= '<li class="lable2"><img src="'.__FLAGTHUMBPATH__.$line->languageFlag.'" title="'.$line->languageName.'" style="margin-bottom: -5px;">  '.stripslashes($value).'<br /></li> '; 
				} else {
					$genTable .= '<li class="lable"></li><li ><img src="'.__FLAGTHUMBPATH__.$line->languageFlag.'" title="'.$line->languageName.'" style="margin-bottom: -5px;">  '.stripslashes($value).' <br /></li> '; 
				}					
				$i++;
			}	
		}
		return $genTable;
	}	
// Start : Get Language Text Area=========================================================
	function getLanguageTextarea($name,$id,$arr_error){
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){
			$genTable = "";
			$i = 0;
			while($line = $this->getResultObject($rst)) {
					if($i == 0) {
						$genTable .= '<li><img src="'.__FLAGTHUMBPATH__.$line->languageFlag.'" title="'.$line->languageName.'"> <textarea name="'.$name.'_'.$line->id.'" id="'.$id.'_'.$line->languageCode.'" class="wel-textarea" rows="5" cols="40" ></textarea><p style="padding-left:150px;">'.$arr_error[$name."_".$line->id].'</p> </li> '; 
					} else {
						$genTable .= '<li class="lable"></li><li><img src="'.__FLAGTHUMBPATH__.$line->languageFlag.'" title="'.$line->languageName.'"> <textarea name="'.$name.'_'.$line->id.'" id="'.$id.'_'.$line->languageCode.'" class="wel-textarea" rows="5" cols="40" ></textarea><p style="padding-left:150px;">'.$arr_error[$name."_".$line->id].'</p> </li> '; 
					}
				$i++;
			}	
		}
		return $genTable;
	}	
// Start : Get Language Text Area Edit====================================================
	function getLanguageEditTextarea($name,$id,$tablename,$tableid,$tableIdName,$arr_error){
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1'   AND isDeleted='0' order by languageName asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){
			$genTable = ""; $i = 0;
			while($line = $this->getResultObject($rst)) {				
				$value = $this->fetchValue($tablename,$name,"$tableIdName=$tableid and langId = '".$line->id."'");
				if($i == 0) {
					$genTable .= '<li ><img src="'.__FLAGTHUMBPATH__.$line->languageFlag.'" title="'.$line->languageName.'">  <textarea name="'.$name.'_'.$line->id.'" id="'.$id.'_'.$line->languageCode.'" class="wel-textarea" rows="5" cols="40" >'.stripslashes($value).'</textarea><p style="padding-left:150px;">'.$arr_error[$name."_".$line->id].'</p> </li> '; 
				} else {
					$genTable .= '<li class="lable"></li><li ><img src="'.__FLAGTHUMBPATH__.$line->languageFlag.'" title="'.$line->languageName.'"> <textarea name="'.$name.'_'.$line->id.'" id="'.$id.'_'.$line->languageCode.'" class="wel-textarea" rows="5" cols="40" >'.stripslashes($value).'</textarea><p style="padding-left:150px;">'.$arr_error[$name."_".$line->id].'</p> </li> '; 
				}					
				$i++;
			}	
		}
		return $genTable;
	}	
// Start : Get Lanaguage View Text Area======================================
        function getLanguageViewTextarea($name,$tablename,$tableid,$tableIdName){
            $rst = $this->selectQry(TBL_LANGUAGE,"status='1'   AND isDeleted='0' order by languageName asc","","");		
            $num = $this->getTotalRow($rst);
            if($num){
                $genTable = ""; $i = 0;
                while($line = $this->getResultObject($rst)) {				
                    $value = $this->fetchValue($tablename,$name,"$tableIdName=$tableid and langId = '".$line->id."'");
                    if($i == 0) {
                        $genTable .= '<li class="lable2"><img src="'.__FLAGTHUMBPATH__.$line->languageFlag.'" title="'.$line->languageName.'" style="margin-bottom: -5px;">  '.stripslashes($value).' </li> '; 
                    } else {
                        $genTable .= '<li class="lable"></li><li ><img src="'.__FLAGTHUMBPATH__.$line->languageFlag.'" title="'.$line->languageName.'" style="margin-bottom: -5px;"> '.stripslashes($value).' </li> '; 
                    }					
                    $i++;
                }	
            }
            return $genTable;
        }	

// Start : Get Category List===============================================================
	function getCategoryList($catId='') {
		$query="select C.id,CD.categoryName from ".TBL_CATEGORY." as C INNER JOIN ".TBL_CATEGORY_DESCRIPTION." AS CD ON (C.id=CD.catId) where C.parent_id='0' and C.status='1' and C.isDeleted='0' and CD.langId='".$_SESSION['DEFAULTLANGUAGE']."' order by C.sequence ASC";	
		$rst = $this->executeQry($query);
		$num = $this->getTotalRow($rst);
		if($num > 0) {
			while($line = $this->getResultObject($rst)) {			    
				$sel=($line->id==$catId)?'selected="selected"':'';				
				$genTable .= '<option value="'.$line->id.'" '.$sel.' >'.stripslashes($line->categoryName).'</option>';				
			}	
		}
		return $genTable;			
	}
	
// Start : Get Orientation List===============================================================
	function getOrientationList($id="") {		
		$query="Select ORI.*,ORID.name from ".TBL_ORIENTATION." as ORI inner join ".TBL_ORIENTATION_DESC." as ORID on (ORI.id=ORID.orientationId) where ORI.status='1' and ORI.isDeleted='0' and ORID.langId='".$_SESSION['DEFAULTLANGUAGE']."'";		
		$rst = $this->executeQry($query);
		$num = $this->getTotalRow($rst);
		if($num > 0) {
			while($line = $this->getResultObject($rst)) {			    
				if($id){
					$sel=($id == $line->id)?'selected="selected"':'';	
				}else{
					$sel=($line->isDefault)?'selected="selected"':'';
				}				
				$genTable .= '<option value="'.$line->id.'" '.$sel.' >'.stripslashes($line->name).'</option>';				
			}	
		}
		return $genTable;			
	}


// Start : Get First Category=====================
	function getFirstCat() {
		$qry = "select a.id from ".TBL_CATEGORY." as a where 1 and a.parent_id = '0' and a.status = '1' and a.isDeleted = '0' limit 1"; 
		//echo $qry ;exit;
		$sql = $this->executeQry($qry);
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			$line = $this->getResultObject($sql);
			return  $line->id; 
		}
	}
// Start : Get Sub Cat Drop Down List====================================
	function subCategoryList($catId,$subcatId='') { 		
		$query="SELECT C.id, CD.categoryName FROM ".TBL_CATEGORY." AS C INNER JOIN ".TBL_CATEGORY_DESCRIPTION." AS CD ON(C.id=CD.catId) WHERE 1 AND C.parent_id='".$catId."' AND C.status='1' AND C.isDeleted='0' AND CD.langId='".$_SESSION['DEFAULTLANGUAGE']."'";
                
		$rst = $this->executeQry($query);
		$num = $this->getTotalRow($rst);
		if($num > 0) {
			while($line = $this->getResultObject($rst)) {
				$sel=($subcatId == $line->id)?'selected="selected"':'';			       
				$genTable .= '<option value="'.$line->id.'" '.$sel.' >'.stripslashes($line->categoryName).'</option>';
				$sel = '';
			}	
			echo $genTable;
		} else {
			echo false;
		}	
	}
//Start : Get Design Category List==================================================================================================

function getDesignCatList($id ='') {
    $query="SELECT DC.id,DCD.categoryName FROM  ".TBL_DESIGNCATEGORY." AS DC INNER JOIN ".TBL_DESIGNCATEGORY_DESCRIPTION." AS DCD ON (DC.id=DCD.catId) WHERE DC.status='1' AND DC.isDeleted='0' AND DCD.langId='".$_SESSION['DEFAULTLANGUAGE']."' AND DC.parent_id='0' order by DCD.categoryName";
    $rst = $this->executeQry($query);
    $num = $this->getTotalRow($rst);
    if($num > 0) {        
        while($line = $this->getResultObject($rst)) {
            $sel=($line->id==$id)?"selected='selected'":"";
            $genTable .= '<option value="'.$line->id.'" '.$sel.' >'.stripslashes($line->categoryName).'</option>';
        }
    }   
    return $genTable;			
}

// Start : Get Language Drop Down List============================
	function getLanguageInDropDown($langId=''){
		$preTable = "<option value=''>Please Select Language</option>";
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' AND isDeleted='0' order by languageName asc","","");
		$num = $this->getTotalRow($rst);
		if($num){
			while($row = $this->getResultObject($rst)){
			if($langId == $row->id){ $sel = "selected='selected'";}else{$sel = "";}
			$preTable.= "<option value='$row->id' $sel>".ucwords($row->languageName)."</option>"; 
			}
		}
		return $preTable;
	}	

// Start : Display Price======================
	function displayPrice($price) {
		$query = "select ".TBL_CURRENCY.".*,".TBL_CURRENCY_DETAIL.".sign from ".TBL_CURRENCY." , ".TBL_CURRENCY_DETAIL." where 1 and ".TBL_CURRENCY.".currencyDetailId = ".TBL_CURRENCY_DETAIL.".id and ".TBL_CURRENCY_DETAIL.".id = '".$_SESSION[DEFAULTCURRENCYID]."'";
		$sql = $this->executeQry($query);
		$line = $this->getResultObject($sql);
		$leftPlace = ($line->showIn==0)?$line->sign." ":"";
		$rightPlace = ($line->showIn==1)?" ".$line->sign:"";
		return $final_price = $leftPlace."".number_format(($price*$line->currencyValue),$line->decimalPlace,".",",")."".$rightPlace;
	}
	
// Start : Get Currency Drop Down List================
	function getCurrencyList($id) {
		$genTable = "";
		$sql = $this->executeQry("select * from ".TBL_CURRENCY_DETAIL." where 1 ");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			while($line = $this->getResultObject($sql)) {						
				$sel = ($id == $line->id)?'selected="selected"':'';
				$genTable .= '<option value="'.$line->id.'" '.$sel.'>'.$line->currencyName.'</option>';
			}	
		}
		return $genTable;			
	}

// Start : Get Multiple Size====================
	function getMultipleSize($sizegroupId,$sizeId){
	
		$stateId1 = explode("-",$sizeId);
		$sql = "select ft.*,ftd.sizeName from ".TBL_SIZE." as ft , ".TBL_SIZEDESC." as ftd where ft.id = ftd.id  and ftd.langId = '".$_SESSION['DEFAULTLANGUAGE']."' and ft.status = '1' and ft.flag = '1' order by ft.sequence Asc"; 
		$rst = $this->executeQry($sql);
		$preTable = "";
		$preTable .= '<select name="sizId[]" id="m__Size" class="wel" multiple="multiple" >';
		$preTable .='<option value="">Select Size</option>';
		
		while($row = $this->getResultObject($rst)){
			
			foreach($stateId1 as $stateid){
			
				if($row->id == $stateid){ $selected = 'selected="selected"'; break;}else{$selected = '';}
			}
			$preTable .= "<option value='$row->id' $selected >".ucwords($row->sizeName)."</option>";
		}
		$preTable .= '</select>';
		echo $preTable;
	}

// Start : Get Multiple Size Edit======================
	function getMultipleSizeEdit($sizegroupId,$sizeId){
	
		$flagId = substr($sizeId, 1, -1);
		$stateId1 = explode("-",$flagId);
		$flag = implode(",", $stateId1 );
		
		$sql = "select ft.*,ftd.sizeName from ".TBL_SIZE." as ft , ".TBL_SIZEDESC." as ftd where ft.id = ftd.id  and ftd.langId = '".$_SESSION['DEFAULTLANGUAGE']."' and ft.status = '1' and (ft.flag = '1' or ft.id in($flag)) order by ft.sequence Asc"; 
		$rst = $this->executeQry($sql);
		$preTable = "";
		$preTable .= '<select name="sizId[]" id="m__Size" class="wel" multiple="multiple" >';
		$preTable .='<option value="">Select Size</option>';
		
		while($row = $this->getResultObject($rst)){
			foreach($stateId1 as $stateid){
				if($row->id == $stateid){ $selected = 'selected="selected"'; break;}else{$selected = '';}
			}
			$preTable .= "<option value='$row->id' $selected >".ucwords($row->sizeName)."</option>";
		}
		$preTable .= '</select>';
		echo $preTable;
	}
	
// Start : Get Attribute List======================================
	function getAttributeList($id) {
		$genTable = "";
		$sql = $this->executeQry("select a.id,ad.attributeName from ".TBL_ATTRIBUTE." as a, ".TBL_ATTRIBUTE_DESCRIPTION." as ad where 1 and a.status = '1' and a.isDeleted = '0' and a.id = ad.attributeId and ad.langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by ad.attributeName");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			while($line = $this->getResultObject($sql)) {						
				$sel = ($id == $line->id)?'selected="selected"':'';
				$genTable .= '<option value="'.$line->id.'" '.$sel.'>'.$line->attributeName.'</option>';
			}	
		}
		return $genTable;			
	}
        
        

}// End Class



?>