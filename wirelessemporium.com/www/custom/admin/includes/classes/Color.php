<?php 
session_start();
class Color extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }

	function valDetail() {
		
		$cond = "1 and ft.id = ftd.id  and ftd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
			$cond .= " AND (ftd.colorName LIKE '%$searchtxt%' )";
		}
		$query = "select ft.*,ftd.colorName from ".TBL_COLOR." as ft , ".TBL_COLORDESC." as ftd where $cond ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {			
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET[orderby]? $_GET[orderby]:"id";
		    $order = $_GET[order]? $_GET[order]:"DESC";   
            $query .=  " ORDER BY ftd.$orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = "Inactive";
					else
						$status = "Active";
						
					if ($line->isDefault==1){
						$isDefault = "checked='checked'";
						$onclickstatus = '';
						$chkbox = '';
					}else{
						$isDefault = "";
						$onclickstatus = ' onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'Color\')"';
						$chkbox = '<input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox">';
					}	
						
						
					$genTable .= '<div class="'.$highlight.'"><ul><li style="width:50px;">&nbsp;&nbsp;'.$chkbox.'</li><li style="width:60px;">'.$i.'</li><li style="width:120px;">'.substr($line->colorName, 0,40 ).'</li><li style="width:110px;">';
					
					$genTable .= '<div  style="width:25px;background-color:'.'#'.$line->colorCode.';" >&nbsp;</div>('.'#'.$line->colorCode.')</li><li style="width:100px;"><input type="checkbox" '.$isDefault.' disabled=disabled class="welcheckbox"></li><li style="width:110px;">';
									
					if($menuObj->checkEditPermission()) 							
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'Color\')">'.$status.'</div>';
									
																											
					$genTable .= '</li>
					<li style="width:100px;"><a rel="shadowbox;width=705;height=325" title="'.$line->colorName.'" href="viewColor.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></li>
					<li style="width:75px;">';
									
					if($menuObj->checkEditPermission()) 					
						$genTable .= '<a href="editColor.php?id='.base64_encode($line->id).'&page='.$page.'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a>';
						
					$genTable .= '</li><li>';
				    
					if($menuObj->checkDeletePermission() && $line->isDefault != 1 ) 					
						$genTable .= "<a href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Forum Topic?')){window.location.href='pass.php?action=Color&type=delete&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					$genTable .= '</li></ul></div>';
					$i++;	
				}

				switch($recordsPerPage)
				{
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='10' $sel1>10</option>
					<option value='20' $sel2>20</option>
					<option value='30' $sel3>30</option> 
					<option value='".$totalrecords."' $sel4>All</option>  
					  </select> Records Per Page
				</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}
	
	/// For Add New Forum Topic
	
	function addRecord($post) {
	
		$xmlArr = array();
		$xm = 1;

			$query = "insert into ".TBL_COLOR." set  status  = '1', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."', colorCode = '".substr_replace($post['colorCode'], '', 0, 1)."'";

		$xmlArr[$xm]['query'] = addslashes($query);
		$xmlArr[$xm]['identification'] = $post['id'];
		$xmlArr[$xm]['section'] = "insert";
		$xm ++;
		
			$res = $this->executeQry($query);
			$inserted_id = mysql_insert_id();
			
			if($res) 
				$this->logSuccessFail('1',$query);		
			else 	
				$this->logSuccessFail('0',$query);
			
			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
			$num = $this->getTotalRow($rst);
			if($num){			
				while($line = $this->getResultObject($rst)) {					
					$colorName = 'colorName_'.$line->id;
					$query = "insert into ".TBL_COLORDESC." set  Id  = '$inserted_id', langId = '".$line->id."', colorName = '".addslashes($post[$colorName])."' ";	
					
		$xmlArr[$xm]['query'] = addslashes($query);
		$xmlArr[$xm]['identification'] = $post['id'];
		$xmlArr[$xm]['section'] = "insert";
		$xm ++;
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);
				}	
			}
			
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);
		$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been added successfully.");
		header("Location:addColor.php");exit;
				
	}
	
	//// check for existing topic
	
	function isColorExist($colorName,$id){
	    
		$colorName = trim(substr_replace($colorName,'', 0, 1));
		$rst = $this->selectQry(TBL_COLOR,"colorCode ='$colorName' AND id!='$id'  ","","");
		$row = $this->getTotalRow($rst);
		return $row;
	}
	
//Start : Change Status==========================================================
	function changeValueStatus($get) {	
		$xmlArr = array();
		$xm = 1;

		$status=$this->fetchValue(TBL_COLOR,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive";
		} else 	{
			$stat= 1;
			$status="Active";
		}
		$query = "update ".TBL_COLOR." set status = '$stat', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
	
		$xmlArr[$xm]['query'] = addslashes($query);
		$xmlArr[$xm]['identification'] = $get['id'];
		$xmlArr[$xm]['section'] = "update";
		$xm ++;
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);

		if($this->executeQry($query)) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);
		echo $status;		
	}
	
	function deleteAllValues($post){
		$xmlArr = array();
		$xm = 1;

		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageColor.php?page=$post[page]&limit=$post[limit]';</script>";
			exit;
		}				
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$queryFind = "select * from ".TBL_COLOR." where id='".$val."'";
					$sqlFind = $this->executeQry($queryFind);
					$lineFind = $this->getResultObject($sqlFind);
					if($lineFind->isDefault != 1){
				    	$result=$this->deleteRec(TBL_COLORDESC,"id='".$val."'");
						
						$xmlQuery="Delete FROM ".TBL_COLORDESC." WHERE id='".$val."' ";
						$xmlArr[$xm]['query'] = addslashes($xmlQuery);
						$xmlArr[$xm]['identification'] = $post['id'];
						$xmlArr[$xm]['section'] = "update";
						$xm ++;
	
						$result1=$this->deleteRec(TBL_COLOR,"id='".$val."'");
						
						$xmlQuery="Delete FROM ".TBL_COLOR." WHERE id='".$val."' ";
						$xmlArr[$xm]['query'] = addslashes($xmlQuery);
						$xmlArr[$xm]['identification'] = $post['id'];
						$xmlArr[$xm]['section'] = "update";
						$xm ++;

					}	
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_CATEGORY,"cat_id='$val'");	
					$sql="update ".TBL_COLOR." set status ='1', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$xmlArr[$xm]['query'] = addslashes($sql);
					$xmlArr[$xm]['identification'] = $post['id'];
					$xmlArr[$xm]['section'] = "update";
					$xm ++;
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_COLOR." set status ='0', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$xmlArr[$xm]['query'] = addslashes($sql);
					$xmlArr[$xm]['identification'] = $post['id'];
					$xmlArr[$xm]['section'] = "update";
					$xm ++;
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}

		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);
		echo "<script language=javascript>window.location.href='manageColor.php?page=$post[page]';</script>";
	}
	
	
	/// For Delete Single Forum Topic
	
	function deleteValue($get) {
		$xmlArr = array();
		$xm = 1;

		$result=$this->deleteRec(TBL_COLOR,"id='".$get['id']."'");
			
		$xmlQuery="DELETE FROM ".TBL_COLOR." WHERE id='".$get['id']."'";
		$xmlArr[$xm]['query'] = addslashes($xmlQuery);
		$xmlArr[$xm]['identification'] = $get['id'];
		$xmlArr[$xm]['section'] = "update";
		$xm ++;
					
		$result1=$this->deleteRec(TBL_COLORDESC,"id='".$get['id']."'");
		
		$xmlQuery="DELETE FROM ".TBL_COLORDESC." WHERE id='".$get['id']."'";
		$xmlArr[$xm]['query'] = addslashes($xmlQuery);
		$xmlArr[$xm]['identification'] = $get['id'];
		$xmlArr[$xm]['section'] = "update";
		$xm ++;

		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageColor.php?page=$get[page]&limit=$get[limit]';</script>";
	}
	
	/// Get Information About Existing color
	function getResult($id) {
		$sql = $this->executeQry("select * from ".TBL_COLOR." where id = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			return $line = $this->getResultObject($sql);	
		} else {
			redirect("manageColor.php");
		}	
	}
	
	// Edit color
		
	function editRecord($post) {		
	    $isdefault = $post[isDefault]?1:0;
	    if($isdefault){
	    	$this->executeQry("update ".TBL_COLOR." set isDefault='0', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id != '$post[id]'");			
		 	$con =", isDefault = '1',status='1'";
		}else{
		 	$con="";
		}		
		$queryUpdate = "update ".TBL_COLOR." set colorCode = '".substr_replace($post[colorCode],'', 0, 1)."' ".$con." where 1 and id = '$post[id]' ";					
                if($this->executeQry($queryUpdate)) 
                        $this->logSuccessFail('1',$queryUpdate);		
                else 	
                        $this->logSuccessFail('0',$queryUpdate);				
				
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){			
			while($line = $this->getResultObject($rst)) {					
				$colorName = 'colorName_'.$line->id;
				$sql = $this->selectQry(TBL_COLORDESC,'1 and id = "'.$post[id].'" and langId = "'.$line->id.'"','','');
				$numrows = $this->getTotalRow($sql);
				if($numrows == 0) { 
					$query = "insert into ".TBL_COLORDESC." set id = '$post[id]', langId = '".$line->id."', colorName = '".$post[$colorName]."' ";
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				} else {
					$query = "update ".TBL_COLORDESC." set colorName = '".addslashes($post[$colorName])."' where 1 and id = '$post[id]' and langId = '".$line->id."'";
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		

					else 	
						$this->logSuccessFail('0',$query);	
				}	
			}	
		}
                $_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been updated successfully");
		echo "<script>window.location.href='manageColor.php?page=$post[page]';</script>";exit;
	}
	/*
	function checkCategoryExist($cid) {
		if($cid) {
			if($cid >= 0 && is_numeric($cid)) {	
				if($cid > 0 && $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'MULTILEVEL_TBL_CATEGORY'") == 1) {
					$getCid = $this->fetchValue(TBL_CATEGORY,"id","1 and id = ".(int)$cid."");
					if(!$getCid)
						redirect('manageCategory.php');		
				} else {
					redirect('manageCategory.php');					
				}
			} else {
				redirect('manageCategory.php');
			}
		}
	}
	
	*/
	
}// End Class
?>	