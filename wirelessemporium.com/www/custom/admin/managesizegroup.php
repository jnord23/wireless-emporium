<?
/*---Basic for Each Page Starts----*/
session_start();
//print_r($_REQUEST);
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/*---Basic for Each Page Starts----*/
$viewObj = new SizeGroup();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<SCRIPT src="js/ajax.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>

<!-- New Drop Down menu Starts-->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<script language='javascript' type='text/javascript' src='js/perpage.js'></script>
<!-- New Drop Down menu Ends-->

<!--				Light Box Starts			-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--				Ligh Box Ends 	-->

</head>
<body>
<? include('includes/header.php'); ?>


<!--          *****************            DRAG AND DROP  - START        ***************   -->
<script type="text/javascript" src="dragndrop/js/jquery-ui-1.7.2.custom.min.js" ></script>
<script type="text/javascript" >
$(function(){
	$('.dragbox')
	.each(function(){
		$(this).hover(function(){ 
			$(this).find('ul').addClass('collapse');
		}, function(){
			$(this).find('ul').removeClass('collapse');
		})
		.find('h2').hover(function(){
			$(this).find('.configure').css('visibility', 'visible');
		}, function(){
			$(this).find('.configure').css('visibility', 'hidden');
		})
		.click(function(){
			$(this).siblings('.dragbox-content').toggle();
		})
		.end()
		.find('.configure').css('visibility', 'hidden');
	});
	$('.column').sortable({
		connectWith: '.column',
		handle: 'ul',
		cursor: 'move',
		placeholder: 'placeholder',
		forcePlaceholderSize: true,
		opacity: 0.4,
		stop: function(event, ui){
			$(ui.item).find('ul').click();
			var sortorder='';
			$('.column').each(function(){
				var itemorder=$(this).sortable('toArray');
				var columnId=$(this).attr('id');
				sortorder+=columnId+'='+itemorder.toString()+'&';
			});
 			current_order = document.getElementById('currentOrder').value;
 			Page = document.getElementById('page').value;
			Limit = document.getElementById('Pagelimit').value;
            //alert("urvesh");
	        sortorder = sortorder+'&Current Order='+current_order+'&page='+Page+'&limit='+Limit;
		   	//alert(sortorder);
		    sortOrderProduct(sortorder,'SizeGroup');
			/*Pass sortorder variable to server using ajax to save state*/
		}
	})
	.disableSelection();
});
</script>


<div id="nav-under-bg"><!-- --></div>
<form name="ecartFrm" method="post" action="pass.php?action=SizeGroup&type=deleteall" >
<input type="hidden" name="page" id="page" value="<?=$_GET[page] ?>" />
<input type="hidden" name="Pagelimit" id="Pagelimit" value="<?=$_GET[limit] ?>" />
<div class="main-body-div-width">
	<div class="main-body-div-header">
	<div class="main-body-header-text-top">Manage Size Group</div><span class="main-body-adduser"><b><? if($menuObj->checkAddPermission()) { ?><a href="addSizeGroup.php">Add New Group</a><? } ?></b></span>
	</div>
	
	<div id="search-main-div">
		<ul>
			<li class="selectall">
				<div id="check"> <a href="javascript:void(0)" class="buttontext" onclick='javascript:checkAllCheckboxes(document.ecartFrm);'>Select All</a></div>
 <div id="uncheck" style="display:none;"><a href="javascript:void(0)" class="buttontext" onclick='javascript:uncheckAllCheckboxes(document.ecartFrm);'>Unselect All</a></div>
			</li>
			<li class="action">Action:</li>
			<li>
				<select name="action">
                    	<option value="">Select Action</option>
						<? if(($menuObj->checkDeletePermission())){  ?>
                    	<option value="deleteselected">Delete Selected</option>
						<? } ?>
						<? if(($menuObj->checkEditPermission())){  ?>
                    	<option value="enableall">Enable Selected</option>	
                    	<option value="disableall">Disable Selected</option>	
						<? } ?>										
                  	</select>
			</li>
			<li><input name="Input" type="submit" value="Submit"  class=""/></li>
		</ul>
</div>	
<div style="margin-top:20px;"><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
	<div class="main-body-content-text-div">
							<ul style="text-align:center;">
								<li style="width:50px;">&nbsp;&nbsp;<input type="checkbox" name="checkall" onclick="javascript:checkAllCheckboxes(document.ecartFrm)" disabled="disabled"></li>
								<li style="width:60px;">SL.No</li>
								<li style="width:150px;">Name</li>
								<li style="width:120px;">Default</li>
								<li style="width:80px;">Status</li>
								<li style="width:135px;">Edit</li>
								<li style="width:55px;">Delete</li>
							</ul>
						</div>
	<?
		echo $viewObj->valDetail();
	?>
</div>
</div>
</form>
</body>
</html>



