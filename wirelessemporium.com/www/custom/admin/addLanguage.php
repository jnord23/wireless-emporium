<?
//error_reporting(E_ALL);
//ini_set("display_errors", 1); 
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageLanguage.php","add_record");
$languageObj = new Language();
require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {
    
	$obj->fnAdd('languageName',$_POST['languageName'], 'req', 'Please Enter Language Name.');
	$obj->fnAdd('languageCode', $_POST['languageCode'], 'req', "Please enter  Language Code.");
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	$arr_error[languageName]=$obj->fnGetErr($arr_error[languageName]);
	$arr_error[languageCode]=$obj->fnGetErr($arr_error[languageCode]);
	 	
	if($languageObj->islanguageNameExit($_POST['languageName']))
		 { 
		   $arr_error[languageName] = "Language already exist. ";
		 }
		
	if($languageObj->islanguageCodeExit($_POST['languageCode']))
		 { 
		   $arr_error[languageCode] = "Language Code altredy exist. ";
		 }
	
			$filename = stripslashes($_FILES['languageFlag']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);	 	 
	
	if(!$languageObj->checkExtensions($extension))
		 { 
		   $arr_error[languageFlag] = "Upload Only ".$languageObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'") ." image extension.";
		 }
	
	 
	if(empty($arr_error[languageName]) && empty($arr_error[languageCode]) && empty($arr_error[languageFlag]) && isset($_POST['submit'])){
	$_POST = postwithoutspace($_POST);
	$languageObj->addNewLanguage($_POST,$_FILES);
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<SCRIPT src="js/ajax.js" language="javascript" type="text/javascript"></SCRIPT>
<script language="javascript" src="js/requiredValidation.js"></script>
<script language="javascript" src="js/validation.js"></script>
<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<!-- Menu head -->
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<script type="text/javascript">
	function hrefBack1(){
		window.location='manageLanguage.php';
	}
</script>
</head>
<body>
<? include('includes/header.php'); ?>
<div id="nav-under-bg"><!-- --></div>

  <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
		<div class="main-body-div-new">
          <div class="main-body-div-header">Add Language</div>
		  <!-- left position -->
        
            <div class="main-body-div4" id="mainDiv">
              <div class="add-main-body-left-new">
                <ul>
                  <li class="add-main-body-left-new-text" style="clear:both; width:500px;padding-top:5px;" ><span class="small_error_message">
                    <?=$_SESSION['SESS_MSG']?>
                  </span></li>
                  <li class="lable">Language Name <span class="spancolor">*</span></li>
                  <li>
                    <input type="text" name="languageName" id="m__Language_Name" class="wel" value="<?=stripslashes($_POST[languageName])?>" /><p style="padding-left:150px;"><?=$arr_error[languageName]?></p>
                  </li>
                  <li  class="lable">Language Code <span class="spancolor">*</span></li>
                  <li>
                    <input type="text" name="languageCode" id="m__Language_Code" maxlength="3" size="3" class="wel" value="<?=stripslashes($_POST[languageCode])?>" />
					<p  style="padding-left:150px;"><?=$arr_error[languageCode]?></p>
                  </li>
				   <li  class="lable">Language Flag <span class="spancolor">*</span></li>
                  <li>
                    <input type="file" name="languageFlag" id="m__Language_Flag" class="wel" value="" />
					<p  style="padding-left:150px;"><?=$arr_error[languageFlag]?></p>					
                  </li>
                </ul>
              </div>
              <div class="main-body-sub">
                <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
                &nbsp;
                <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
              </div>
            </div>
</div>
</form>
		<div id="divTemp" style="display:none;"></div> 
<? unset($_SESSION['SESS_MSG']); ?>