// Start : Create XML HTTP OBJECT=============================
var xmlHttp;
function getobject(){
	var xmlHttp=null;
	try{
		xmlHttp=new XMLHttpRequest;
	}
		
	catch(e){
		try{
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(e){
			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP")	;
		}	
	}
	return xmlHttp;

}




// Start : Change Status ============================================
function changeStatus(divID,id,action){
	xmlhttp=getobject();
	var query="id="+id+"&action="+action;	
	document.getElementById(divID).innerHTML='<img src="images/loading_icon.gif">';
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4){			
			document.getElementById(divID).innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","pass.php?type=changestatus&"+query,true);
	xmlhttp.send(null);	

}

//Get Subcat list option by catId================
function getSubCatByCatId(id){	
	if(id > 0) {
		$("#subCatSPAN").html('<img src="images/indicator.gif">');
		var src = 'pass.php?action=product&type=subcat&id='+id;			
		$.ajax({
			url: src,				
			cache: false, 
			type:'GET',
			success: function(data, textStatus, XMLHttpRequest){						
				$("#subCat").html(data);	
				$("#subCatSPAN").html('');
			}, 	
			error:function(XMLHttpRequest, textStatus, errorThrown){alert(textStatus);
			alert(errorThrown);}
		});
	} 
	
}

//Delete Product View=======================================================
function deleteProductView(pid,vid){
	var src="pass.php?action=product&type=delProductView&productId="+pid+"&viewId="+vid;	
	$.ajax({
		url: src,				
		cache: false, 
		type:'GET',
		success: function(data, textStatus, XMLHttpRequest){								
			$("#productView"+vid).html('');
		}, 	
		error:function(XMLHttpRequest, textStatus, errorThrown){alert(textStatus);
		alert(errorThrown);}
	});
} 	

//Add Product View Image==================================================
function addProductViewImages(vid){
	var src="pass.php?action=product&type=addProductViewImages&viewId="+vid;
	$.ajax({
		url: src,				
		cache: false, 
		type:'GET',
		success: function(data, textStatus, XMLHttpRequest){								
			$("#productImg"+vid).html(data);
		}, 	
		error:function(XMLHttpRequest, textStatus, errorThrown){alert(textStatus);
		alert(errorThrown);}
	});		
}

// Download Product PDF==================================================
function downloadProduct(pid){
    $("#PDF_"+pid).html('<img src="images/loading_icon.gif">');   
    var src ="pass.php?action=mainproduct&type=downloadpdf&pid="+pid;     
    $.ajax({
        url: src,				
        cache: false, 
        type:'GET',
        success: function(data, textStatus, XMLHttpRequest){            
            $("#PDF_"+pid).html(data);
        }, 	
        error:function(XMLHttpRequest, textStatus, errorThrown){
            alert(textStatus);
            alert(errorThrown);
        }
    });	
}

// Generate PDF==================================================
function generatePDF(pid){
    $("#PDF_"+pid).html('<img src="images/loading_icon.gif">');   
    var src ="pass.php?action=pdf&type=download&pid="+pid;     
    $.ajax({
        url: src,				
        cache: false, 
        type:'GET',
        success: function(data, textStatus, XMLHttpRequest){            
            $("#PDF_"+pid).html(data);
        }, 	
        error:function(XMLHttpRequest, textStatus, errorThrown){
            alert(textStatus);
            alert(errorThrown);
        }
    });	
}


// Backup==================================================================
function takeBackup(){	
	var query="";	
        var src ="pass.php?action=backup&type=takeBackup&"+query;        
        $("#showMsg").html('<img src="images/loading_icon.gif">');   
        $.ajax({
            url: src,				
            cache: false, 
            type:'GET',
            success: function(data, textStatus, XMLHttpRequest){            
                $("#showMsg").html("<font size='3'>DATABASE BACKUP IS COMPLETED.</font><br><b>It is stored in -- "+data);
            }, 	
            error:function(XMLHttpRequest, textStatus, errorThrown){
                alert(textStatus);
                alert(errorThrown);
            }
        });	      
}

//============================================================================