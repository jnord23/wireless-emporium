<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/*---Basic for Each Page Starts----*/

$admObj = new AdminDetail();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<SCRIPT src="js/ajax.js" language="javascript" type="text/javascript"></SCRIPT>

<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<!-- Menu head -->
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>

<!--[if lte IE 6]>
<style type="text/css">
.clearfix {height: 1%;}
img {border: none;}
</style>
<![endif]-->
<!--[if gte IE 7.0]>
<style type="text/css">
.clearfix {display: inline-block;}
</style>
<![endif]-->
<!--[if gt IE 7]>
<![endif]-->
<!-- New Drop Down menu -->

</head>
<body>
<? include('includes/header.php'); ?>
<div id="nav-under-bg"><!-- --></div>
<div class="main-body-div-width">
	
	<div class="main-body-div-header">
	<div class="main-body-header-text-top">Admin Details</div><span class="main-body-adduser"><b><? if($menuObj->checkAddPermission()) { ?><a href="addAdmin.php">Add New Admin</a><? } ?></b></span>
	</div><span class="small_error_message"><?=$_SESSION['SESS_MSG']?></span>
	<?
		echo $admObj->valDetail();
	?>
</div>
</body>
</html>

<?php unset($_SESSION['SESS_MSG']); ?>

