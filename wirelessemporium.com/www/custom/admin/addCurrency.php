<?
/*---Basic for Each Page Starts----*/
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCurrency.php","add_record");
/*---Basic for Each Page Ends----*/

$currObj = new Currency();
require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {
	$obj->fnAdd('currencyValue',$_POST['currencyValue'], 'req', 'Please Enter Currency Value.');
	$obj->fnAdd("decimalPlace", $_POST["decimalPlace"], "req", "Please enter  Decimal Place.");
	$obj->fnAdd("currencyCode", $_POST["currencyCode"], "req", "Please enter Currency Code.");
	$obj->fnAdd("currencyCodeT", $_POST["currencyCodeT"], "req", "Please enter Currency Code (For Tool).");
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	$arr_error[currencyValue]=$obj->fnGetErr($arr_error[currencyValue]);
	$arr_error[decimalPlace]=$obj->fnGetErr($arr_error[decimalPlace]);
	$arr_error[currencyCode]=$obj->fnGetErr($arr_error[currencyCode]);
	$arr_error[currencyCodeT]=$obj->fnGetErr($arr_error[currencyCodeT]);

	if(!is_numeric($_POST['currencyValue']))
	{ 
		$arr_error[currencyValue] = "Currency Value is wrong. ";
	}
	if(!is_numeric($_POST['decimalPlace']))
	{ 
		$arr_error[decimalPlace] = "Decimal Place Value is wrong. ";
	}

	if(empty($arr_error[currencyValue]) && empty($arr_error[decimalPlace]) && empty($arr_error[currencyCode]) && empty($arr_error[currencyCodeT])&& isset($_POST['submit'])){
		$_POST = postwithoutspace($_POST);
		$currObj->addRecord($_POST);		
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<SCRIPT src="js/ajax.js" language="javascript" type="text/javascript"></SCRIPT>
<script language="javascript" src="js/requiredValidation.js"></script>
<script language="javascript" src="js/validation.js"></script>
<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<!-- Menu head -->
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<script type="text/javascript">
	function hrefBack1(){
		window.location='manageCurrency.php';
	}
</script>
</head>
<body>
<? include('includes/header.php'); ?>
<div id="nav-under-bg"><!-- --></div>

  <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" >
		<div class="main-body-div-new">
          <div class="main-body-div-header">Add Currency</div>
		  <!-- left position -->
        
            <div class="main-body-div4" id="mainDiv">
              <div class="add-main-body-left-new">
                <ul>
                  <li class="add-main-body-left-new-text" style="clear:both; width:500px;padding-bottom:5px;" ><span class="small_error_message">
                    <?=$_SESSION['SESS_MSG']?>
                  </span></li>
                  <li class="lable">Choose Currency <span class="spancolor">*</span></li>
                  <li class="sap"><select name="currencyDetailId" >
				  	<?	
						$genObj = new GeneralFunctions();
						echo $genObj->getCurrencyList('');
					?></select></li>
                  <li  class="lable">Currency Value <span class="spancolor">*</span></li>
                  <li>
                    <input type="text" name="currencyValue" id="m__currencyValue"  class="wel" value="<?=stripslashes($_POST[currencyValue])?>" />
					<p  style="padding-left:150px;"><?=$arr_error[currencyValue]?></p>
                  </li>
				  <li  class="lable">Symbol Show In<span class="spancolor">*</span></li>
                  <li class="sap"><input type="radio" name="showIn" value="0" checked="checked" /> Left <input type="radio" name="showIn" value="1" /> Right
                  </li>
				  <li  class="lable">Decimal Places <span class="spancolor">*</span></li>
                  <li><input type="text" name="decimalPlace" id="m__decimalPlace"  class="wel" value="<? echo ($_POST[decimalPlace] != "")?$_POST[decimalPlace]:"0" ?>" />
					<p  style="padding-left:150px;"><?=$arr_error[decimalPlace]?></p>
                  </li>		
				  <li  class="lable">Make it Default: <span class="spancolor">*</span></li>
                  <li class="sap"><input type="checkbox" name="isDefault" value="1" />
                  </li>	
				   <li  class="lable">Currency Code <span class="spancolor">*</span></li>
                  <li>
                    <input type="text" name="currencyCode" id="m__currence__code"  class="wel" value="<?=stripslashes($_POST[currencyCode])?>" />
					<p  style="padding-left:150px;"><?=$arr_error[currencyCode]?></p>
                  </li>
				   <li  class="lable">Currency Code(Tool) <span class="spancolor">*</span></li>
                  <li>
                    <input type="text" name="currencyCodeT" id="m__currence__code (For Tool)"  class="wel" value="<?=stripslashes($_POST[currencyCodeT])?>" />
					<p  style="padding-left:150px;"><?=$arr_error[currencyCodeT]?></p>
                  </li>			  
                </ul>
              </div>
              <div class="main-body-sub">
                <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
                &nbsp;
                <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
              </div>
            </div>
</div>
</form>
		<div id="divTemp" style="display:none;"></div> 
<? unset($_SESSION['SESS_MSG']); ?>