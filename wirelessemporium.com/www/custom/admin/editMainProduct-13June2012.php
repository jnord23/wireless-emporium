<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageMainProducts.php","edit_record");
/*---Basic for Each Page Ends----*/
$mainProdObj = new MainProducts();
//$mainProdObj = new Products(base64_decode($_GET['id']));
if(isset($_POST['update'])) {	
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $mainProdObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $mainProdObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $mainProdObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		$obj->fnAdd('device',$_POST['device'], 'req', 'Please Enter device.');
		$obj->fnAdd('deviceCompany',$_POST['deviceCompany'], 'req', 'Please Enter company.');
		
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('productName_'.$value,$_POST['productName_'.$value], 'req', 'Please Enter Product Name.');			
		}
		
		
		$obj->fnAdd('productPrice',$_POST['productPrice'], 'req', 'Please Enter Product Price.');
		//$obj->fnAdd('orientation',$_POST['orientation'], 'req', 'Please Enter Product Orientation.');
		
		
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('productDesc_'.$value,$_POST['productDesc_'.$value], 'req', 'Please enter the Product Desciption.');
		}
		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1; 
		
		$arr_error[device]=$obj->fnGetErr($arr_error[device]);
		$arr_error[deviceCompany]=$obj->fnGetErr($arr_error[deviceCompany]);
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['productName_'.$value]=$obj->fnGetErr($arr_error['productName_'.$value]);
			if($arr_error['productName_'.$value]) 
				$errorArr = 1;
		}
		
		/*foreach($langIdArr as $key=>$value) {
			if($mainProdObj->isProductNameExist($_POST['productName_'.$value],$value,base64_decode($_GET['id'])))
			{ 
				$arr_error['productName_'.$value] = "Product already exist. ";
				if($arr_error['productName_'.$value]) 
				$errorArr = 1;				
			}
		}*/
		foreach($langIdArr as $key=>$value) {
			$arr_error['productDesc_'.$value]=$obj->fnGetErr($arr_error['productDesc_'.$value]);
			if($arr_error['productDesc_'.$value]) 
				$errorArr = 1;
		}
		foreach($_FILES as $key=>$fiesArr){
			
			if($_FILES[$key]['name']){
				$filename = stripslashes($_FILES[$key]['name']);
				$extension = findexts($filename);
				$extension = strtolower($extension);
		
				if(!$mainProdObj->checkExtensions($extension))
				{
				
					$arr_error[$key] = "Upload Only ".$mainProdObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'") ." image extension.";
					$errorArr = 1;	
				}
			}
		}
		
		$arr_error[productPrice]=$obj->fnGetErr($arr_error[productPrice]);
		//$arr_error[orientation]=$obj->fnGetErr($arr_error[orientation]);
		
		if($errorArr == 0 && isset($_POST['update']) && empty($arr_error[device]) && empty($arr_error[deviceCompany]) && empty($arr_error[productPrice]) /*&& empty($arr_error[orientation])*/ ){
			//$_POST = postwithoutspace($_POST);	
			$mainProdObj->editRecord($_POST,$_FILES);
		}
	}	
}

$result = $mainProdObj->getResult(base64_decode($_GET['id']));
	//echo "<pre>";print_r($result);exit;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script language="javascript" src="js/requiredValidation.js"></script>
<script language="javascript" src="js/ajax.js"></script>
<!-- New Drop Down menu -->
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- New Drop Down menu -->

<!-- Color Picker (END)	-->
<script type="text/javascript">
	function hrefBack1(){
		window.location.href='manageMainProducts.php';
	}

</script>

</head>
<body>
<? include('includes/header.php'); ?>
<div id="nav-under-bg">
  <!-- -->
</div>

  <div class="main-body-div-new">
    <div class="main-body-div-header">Edit Main Product</div>
    <!-- left position -->
    <div class="main-body-div4" id="mainDiv">
			<div align="center"><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
	
	<form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">
  <input type="hidden" name="page" value="<?=$_GET['page']?>">
 	<div>
      <div class="add-main-body-left-new" style=" width:630px; float:left;">
        <ul style="float:left; width:400px;">
          <li class="add-main-body-left-new-text" style="clear:both; width:500px;" ><span class="small_error_message">
            <?=$_SESSION['SESS_MSG']?>
            </span></li>
          <li class="lable">Device <span class="spancolor">*</span></li>
          <li style="margin-top:10px;">
              <?php 
			  
					$genObj = new GeneralFunctions();
					echo $genObj->getCategoryListEdit($result->categoryId);
			   ?>
			   <p style="padding-left:150px;"><?=$arr_error[device]?></p>
          </li>
		 
		  
		  <li class="lable">Company <span class="spancolor">*</span></li>
          <li style=" margin-top:10px;" id="deviceCompanyDiv">
			<?php 
			if( isset($_POST['deviceCompany']) && $_POST['deviceCompany'] == "")
				$companyId = '0';
			else
				$companyId = ($_POST['deviceCompany'])?$_POST['deviceCompany']:$result->companyId;
			$deviceId = ($_POST['device'])?$_POST['device']:$result->categoryId;
			echo $mainProdObj->getCompanyForDeviceEdit($companyId,$deviceId);
			?>
			  <p style="padding-left:150px;"><?=$arr_error[deviceCompany]?></p>
		  </li> 
					  
		  
          <li class="lable">Product Name <span class="spancolor">*</span></li>
          <?php	$genObj = new GeneralFunctions();
				echo $genObj->getLanguageEditTextBox('productName','m__productName',TBL_MAIN_PRODUCT_DESCRIPTION,base64_decode($_GET['id']),"productId",$arr_error); //1->type,2->name,3->id,4->tablename,5->tableid
		  ?>
          <li class="lable">Product Price <span class="spancolor">*</span></li>
          <li>
            <input type="text" name="productPrice" id="m__Product_Price" class="wel" value="<?=stripslashes($_POST[productPrice]?$_POST[productPrice]:$result->productPrice)?>"  maxlength="8" onkeyup="return isNum12(this.value);" />
            <p style="padding-left:150px;" id="price" >
              <?=$arr_error[productPrice]?>
            </p>
          </li>
          <li class="lable">Product Quantity </li>
          <li>
            <input type="text" name="quantity" class="wel" value="<?=stripslashes($_POST[quantity]?$_POST[quantity]:$result->quantity)?>"  />
            <p style="padding-left:150px;" id="price" >
              <?=$arr_error[quantity]?>
            </p>
          </li>
          <li class="lable" style="float:none;">Product Description <span class="spancolor">*</span></li>
          <?	$genObj = new GeneralFunctions();
						echo $genObj->getLanguageEditTextarea('productDesc','m__Product_Description',TBL_MAIN_PRODUCT_DESCRIPTION,base64_decode($_GET['id']),"productId",$arr_error); //1->type,2->name,3->id
					?>
		<br />
		

						
			
			
			
			<?php /*?><li class="lable" >Product Orientation  <span class="spancolor">*</span></li>
		  <li style="margin-top:10px;">
		  		<?php $orientation = $_POST[orientation]?$_POST[orientation]:$result->orientation;?>
				<select name="orientation" id="m__product_orientation">
				<option value="1" <?php if($orientation == '1') echo "selected";?>>Verticale</option>
				<option value="0" <?php if($orientation == '0') echo "selected";?>>Horizontal</option>
				</select>
				<p  style="padding-left:150px;"><? echo $arr_error[orientation];?></p></li>	<?php */?>
				
        </ul>
        
        
       
           <ul style="overflow:hidden; margin-top:15px;text-align:center; ">
            <!--<li class="lable" style="float:none;" >Product image <span class="spancolor">*</span></li> -->
			<li>
				<? $productImageName=$mainProdObj->getImageNameview($id=base64_decode($_GET['id']));?>
				<img src="<?=__MAINPRODUCTSINGLLARGEPATH__.$result->singleImage ?>"  />
				<?php /*?><input type="file" id="product_image" name="productImage" size="40">
				<p  style="padding-left:0px;"><?=$arr_error["productImage"]?></p><?php */?>
			</li>
            </ul>
            
        
        
      </div>
      
     	   
           
            
	  </div>
	  
	  
      <div style="clear:both; float:left;">
        <div id="divProduct" style="height:15px;text-align:center;font-weight:bold; color:#FF0000;"></div>
        <!-- Div Id Call for Ajax Function-->
        <!-- fieldSet removed from here  -->
      </div>
	  
	  
		<div class="main-body-sub">
        <input type="submit" name="update" class="main-body-sub-submit" style="cursor:pointer;" value="Update" />
        &nbsp;
        <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
      </div>
		
		</form>  
	  
	  
      
	  
	  
    </div>
  </div>
<script type="text/javascript" >
	function checkAllcheckbox(fid,sid){
	 if(document.getElementById(fid+"menuCheck").checked == true){
		 for (var i = 0; i < sid; i++) {
			document.getElementById(fid+"mCheck"+i).checked = true;	
		 }
	 }else{
	     for (var i = 0; i < sid; i++) {
			document.getElementById(fid+"mCheck"+i).checked = false;	
		 }
	 }
	}
	function selectAllBox(){
	  var aid = document.getElementById("atid").value;
	  var aaid = document.getElementById("atvid").value;
	  var spaid = aid.split(",");
	  var sppaid = aaid.split(",");
	    if(document.getElementById("selectval").checked == true){
			for(k=0;k<spaid.length;k++){
			    document.getElementById(spaid[k]+"menuCheck").checked = true;
			    for(l=0;l<sppaid[k];l++){
			      document.getElementById(spaid[k]+"mCheck"+l).checked = true;
			    }
			 }
		 }else{
			    for(i=0;i<sppaid.length;i++){
			      document.getElementById(spaid[i]+"menuCheck").checked = false;
				  for(l=0;l<sppaid[i];l++){
			        document.getElementById(spaid[i]+"mCheck"+l).checked = false;
			      }
		        }
	    }
	}
	</script>

<? unset($_SESSION['SESS_MSG']); ?>
