<?php
ob_start();
session_start();
require_once('includes/function/autoload.php'); 
require_once('setlanguage.php');
$mpObj = new MainProduct();
$designId = $_GET["designId"]=='' ? 0 :base64_decode($_GET["designId"]);
$record=$mpObj->showDesignProduct($designId);
?>
<html>
  <head>
    <title>Product Details (<?php echo $designId ?>)</title>    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
	<script>    
		!window.jQuery && document.write('<script src="jquery-1.4.3.min.js"><\/script>');
	</script>
	<script type="text/javascript" src="js/ajax.js"></script>
	<script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.4.css" media="screen" /> 	
	<script type="text/javascript">
		window.location = "/cartTransfer?prodID=<?php echo $designId ?>"
		$(document).ready(function() {
			$("a#imageView").fancybox({
				'overlayShow'	: false,
				'transitionIn'	: 'elastic',
				'transitionOut'	: 'elastic'
			});
		});
		
	</script>
    <style>
    .main {width:600px; margin-left:300px; text-align:center;}
    .product { width:100%; margin:5px; padding:10px; border:1px solid #888; border-radius:8px;}
    .image { border:1px solid #888; padding:2px; border-radius:4px;}
    </style>
  </head>
  <body>
    <div class="main">
	<div class="product">
<!--            <button onClick="$('#basket').toggle();">show basket</button>-->
            
            <table id="basket" height="" width="100%" rules="all" cellpadding="10px" style="display:none;">
		<tr>
		    <td colspan="6" align="center"><h2>Your Basket (<?=$mpObj->showBasket(1,$designId)?>)<h2></td>
		</tr>               
                <?=$mpObj->showBasket(0,$designId);?>   
	    </table>
	    <table  height="" width="100%" rules="all" cellpadding="10px">
		<tr>
		    <td colspan="2" align="center"><h2>Your Final Product<h2></td>
		</tr>
		<tr>
		    <td>Product Name</td>
		    <td><?=$record->productName;?></td>
		</tr>
		<tr>
		    <td>Description</td>
		    <td><?=$record->productDesc;?></td>
		</tr>
		<tr>
		    <td>Product Images</td>
		    <td>
                        <a id="imageView" href="<?=__MAINPRODUCTEXTLARGEPATH__.$record->imageWithBorder?>">
                            <img class="image" src="<?__BASEURL__?>files/mainproduct/thumb/<?=$record->imageWithBorder?>" title="click to large" /></a><br>
                            <a href="index.asp?designId=<?=$designId?>">Customize</a>
                    </td>
		</tr>
		<tr>		    
		    <td  colspan="2">
                        <span id="showLoad">Go to Product > Manage Main Produts in <a href="<?=SITE_URL?>admin" target="_blank">ADMIN PANEL</a> to download pdf of this product.</span><br>
                        <a href="<?=SITE_URL?>">Click</a> here to create another product
                    </td>
                </tr>
                
<!--                <tr>		    
		    <td  colspan="2">
                        <span id="showLoad"><a href="javascript:void(0);" onclick="downloadProduct(<?=$designId?>);">Click</a> here for Download link </span><br>
                        <a href="index.php">Click</a> here to create another product
                    </td>
                </tr>-->
                
                         
                
	    </table>           
            
             
	</div>
    </div>  
  </body>
</html>