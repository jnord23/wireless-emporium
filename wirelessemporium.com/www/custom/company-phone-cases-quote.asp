<%
	pageName = "CompanyPhoneCasesQuote"
	
	response.buffer = true
	noleftnav = 1
	nocommentbox = true
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
dim strName, strCName, strPhone, strEmail
strName = prepStr(request.form("txtName"))
strCName = prepStr(request.form("txtCName"))
strPhone = prepStr(request.form("txtPhoneNumber"))
strEmail = prepStr(request.form("txtEmail"))
%>
<script>
	function quoteSubmit() {
		var name = document.frmQuote.txtName.value;
		var companyname = document.frmQuote.txtCName.value;
		var phonenumber = document.frmQuote.txtPhoneNumber.value;
		var email = document.frmQuote.txtEmail.value;
		
		if (name == '') {
			alert('Name is required!');
			return false;
		}

		if (companyname == '') {
			alert('Company Name is required!');
			return false;
		}
		
		if (phonenumber == '') {
			alert('Phone Number is required!');
			return false;
		}
		
		if (email == '') {
			alert('Email is required!');
			return false;
		}
		
		return true;
	}
</script>
<%
if strName <> "" and strCName <> "" and strPhone <> "" and strEmail <> "" then

strBody = 	"Name: " & strName & "<br>" & vbcrlf & _
			"Company Name: " & strCName & "<br>" & vbcrlf & _
			"Phone Number: " & strPhone & "<br>" & vbcrlf & _
			"Email Address: " & strEmail & "<br>"
			
set objErrMail = Server.CreateObject("CDO.Message")
with objErrMail
	.From = "sales@wirelessemporium.com"
	.To = "ruben@wirelessemporium.com,wemarketing@wirelessemporium.com,wemerch@wirelessemporium.com,wedev@wirelessemporium.com,eugene@wirelessemporium.com,tony@wirelessemporium.com"
'	.To = "terry@wirelessemporium.com,ruben@wirelessemporium.com,wemarketing@wirelessemporium.com"
	.Subject = "Custom Business/Group Quotes"
	.HTMLBody = strBody
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
	.Configuration.Fields.Item("urn:schemas:httpmail:sender") = "sales@wirelessemporium.com"
	.Configuration.Fields.Update
	.Send
end with

%>
<div style="float:left; width:958px; text-align:center; height:250px;">
	<div style="float:left; width:100%; font-weight:bold; font-size:16px; color:#333; text-align:center; padding-top:30px;">Thank you! Your request has been submitted!</div>
	<div style="float:left; width:100%; text-align:center; text-align:center; padding-top:30px;"><a href="/" style="text-decoration:underline; font-weight:bold; font-size:16px;">Continue Shopping</a></div>    
</div>
<%
else
%>
<div style="float:left; width:958px;">
	<form name="frmQuote" method="post">
	<div style="float:left; width:658px;">
    	<div style="float:left; width:100%; color:#333; font-size:24px;">Free Custom Business/Group Quotes</div>
    	<div style="float:left; width:100%; color:#666; padding-top:10px;">
        	Mar+x Myles will provide you with a free color printing quote within days. Whether you are printing postcards, presentation folders, brochures or anything else, our printing quotes will spell out all the costs. To obtain a printing quote, please fill out the form below completely.
        </div>
        <div style="float:left; width:606px; border:1px solid #b2b2b2; background-color:#f0f0f0; padding:25px; margin-top:20px;">
        	<div style="float:left; width:100%; color:#333; font-size:20px;">Request Quote</div>
        	<div style="float:left; width:100%; padding-top:20px;">
	        	<div style="float:left; width:200px;">
                	<div class="left" style="color:#444; font-weight:bold;">Name</div>
                	<div class="right"><strong>&gt;</strong>&nbsp;&nbsp;&nbsp;</div>
                </div>
	        	<div style="float:left; width:400px;">
                	<input type="text" name="txtName" style="width:400px;" />
                </div>
            </div>
        	<div style="float:left; width:100%; padding-top:10px;">
	        	<div style="float:left; width:200px;">
                	<div class="left" style="color:#444; font-weight:bold;">Company Name</div>
                	<div class="right"><strong>&gt;</strong>&nbsp;&nbsp;&nbsp;</div>
                </div>
	        	<div style="float:left; width:400px;">
                	<input type="text" name="txtCName" style="width:400px;" />
                </div>
            </div>
        	<div style="float:left; width:100%; padding-top:10px;">
	        	<div style="float:left; width:200px;">
                	<div class="left" style="color:#444; font-weight:bold;">Phone Number</div>
                	<div class="right"><strong>&gt;</strong>&nbsp;&nbsp;&nbsp;</div>
                </div>
	        	<div style="float:left; width:400px;">
                	<input type="text" name="txtPhoneNumber" style="width:400px;" />
                </div>
            </div>
        	<div style="float:left; width:100%; padding-top:10px;">
	        	<div style="float:left; width:200px;">
                	<div class="left" style="color:#444; font-weight:bold;">Email</div>
                	<div class="right"><strong>&gt;</strong>&nbsp;&nbsp;&nbsp;</div>
                </div>
	        	<div style="float:left; width:400px;">
                	<input type="text" name="txtEmail" style="width:400px;" />
                </div>
            </div>
        	<div style="float:left; width:100%; text-align:right; padding-top:10px;">
            	<input type="image" src="/images/custom/corp/button-submit.jpg" onClick="return quoteSubmit();" />
            </div>            
        </div>
    </div>
    </form>
	<div style="float:left; width:280px; margin-left:20px;">
    	<div style="float:left; width:278px; border:1px solid #ededed; border-radius:5px;">
        	<div style="float:left; width:100%; height:37px; background:url(/images/custom/corp/need_heather.gif) repeat-x; font-size:16px; color:#333; font-weight:bold; text-align:center; padding-top:20px; border-top-left-radius:5px; border-top-right-radius:5px;">NEED ASSISTANCE?</div>
            <div style="float:left; width:258px; color:#333; padding:10px; line-height:20px;">
            	We're dedicated to giving you the best service possible. If you have any questions or you encounter any problems while designing your custom faceplate, do not hesitate to contact us.
                <div style="float:left; width:100%; color:#333; padding-top:20px;">
                	<strong>&gt; Phone: </strong>1-800-305-1106
                </div>
                <div style="float:left; width:100%; padding-top:10px;">
                	<div style="float:left; display:inline-block;"><strong>&gt; Live Chat: </strong>&nbsp;&nbsp;</div>
					<script type="text/javascript">
                        var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                        var lhnid = 3410;
                        var lhnwindow = '';
                        var lhnImage = "<div style='float:left; display:inline-block; color:#56b7e2; text-decoration:underline;'>Click Here</div>";
                        document.write(unescape("%3Cscript src='/framework/utility/liveHelp.js' type='text/javascript'%3E%3C/script%3E"));
                    </script>                        
                </div>
            </div>
        </div>
        <div style="float:left; width:100%; text-align:center; padding-top:10px;">
            <img src="/images/custom/corp/6374100_s.jpg" border="0" />
        </div>
    </div>
</div>
<%
end if
%>
<!--#include virtual="/includes/template/bottom.asp"-->

