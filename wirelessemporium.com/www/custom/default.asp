<%
mobileAccess = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
	dim noLeftNav : noLeftNav = 1        
    dim designId: designId = Request.querystring("designId")
    dim aspId : aspId = Request.querystring("aspId")
    
    activeContest = 0
	bToolAvail = false
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<link rel="stylesheet" type="text/css" href="history/history.css" />
<style type="text/css">
.main {padding:0px;}
.col-main{ padding:0px;}
#flashContent{display:block;}
</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript" src="FBJSBridge.js"></script>
<script type="text/javascript">
    function popuponclick(s)
    {
        // alert("popup");
        my_window = window.open(s,"mywindow","status=1,width=800,height=600");
    }

    function closepopup()
    { 
        // alert("closeup");
        my_window.close ();
    }

    var attributes = {};

    function embedPlayer() {        
        var flashvars = {designId:"<%=prepInt(designId)%>",aspId:"<%=prepInt(aspId)%>"};
        var params = {menu: "false", wmode: "opaque"};	        
        swfobject.embedSWF("index.swf", "flashContent", "650", "870", "9.0", null, flashvars , params, {name:"flashContent"});
    }
    
    //Redirect for authorization for application loaded in an iFrame on Facebook.com 
    function redirect(id,perms,uri) { //alert(id+'=='+perms+'=='+uri);
        var params = window.location.toString().slice(window.location.toString().indexOf('?'));
        top.location = 'https://graph.facebook.com/oauth/authorize?client_id='+id+'&scope='+perms+'&redirect_uri='+uri+params;				 
    }    			
</script>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,800' rel='stylesheet' type='text/css'>
<style type="text/css">
#ccw {
	width:958px;
}
#ccw #header, #ccw #first, #ccw #second, #ccw #third, #ccw #fourth {
	position:relative;
	/*border:1px solid green;*/
}
#ccw h2, #ccw h3 {
	font-weight:800;
	font-family: 'Open Sans', sans-serif;
	/*font-size:25px;*/
	color:#333333;
	text-transform:uppercase;
	display:block;
	text-align:center;
	white-space:nowrap;
}
#ccw h2 {
	font-size:2.5em;
	line-height:1.5em;
	margin-top:0.0em;
	margin-bottom:0.0em;
}
#ccw h3 {
	font-size:1.33em;
	line-height:1.2em;
}
#ccw h4 {
	font-family:Verdana, Geneva, sans-serif;
	font-weight:bold;
	color:#666666;
	text-transform:uppercase;
	display:block;
	text-align:center;
	white-space:nowrap;
}
#ccw p {
	margin-left:auto;
	margin-right:auto;
	line-height:1.75em;
	color:#666666;
	font-family:Verdana, Geneva, sans-serif;
}
#ccw a {
	color:#3EB2D9;
	text-decoration:underline;
}
#ccw #header {
	height:110px;
	margin-bottom:20px;
}
#ccw #headerBanner {
	position:absolute;
	left:13px;
	top:5px;
	width:530px;
	height:175px;
}
#ccw #headerBanner h1 {
	font-size:4.0em;
	font-family: 'Open Sans', sans-serif;
	font-weight:300;
	padding-left:0;
	margin-bottom:-0.125;
}
#ccw #headerBanner .h1Bold {
	font-weight:800;
	font-family: 'Open Sans', sans-serif;
	color:#333333;
	text-transform:uppercase;
}
#ccw #headerBanner #hrHeader {
	height:1px;
	width:475px;
	background:#CCCCCC;
}
#ccw #headerBanner p {
	/*font-size:1.4em;*/
	font-size:16px;
	font-family: 'Open Sans', sans-serif;
	font-weight:300;
	margin-top:0em;
	padding-left:0.25em;
	padding-right:0em;
}
#ccw #tool {
	background:url(/images/customCase/header-tool-small.png) no-repeat;
	width:211px;
	height:133px;
	right:0px;
	top:-4px;
	position:absolute;
	z-index:20;
}
#ccw #arrow {
	background:url(/images/customCase/header-arrow-small.png) no-repeat;
	width:28px;
	height:28px;
	top:43px;
	right:190px;
	position:absolute;
}
#ccw #photo {
	background:url(/images/customCase/header-photograph-small.png) no-repeat;
	width:111px;
	height:80px;
	top:22px;
	right:236px;
	position:absolute;
}
#ccw #price {
	background:url(/images/customCase/custom-case-pricing.png) no-repeat;
	width:82px;
	height:82px;
	top:0px;
	right:374px;
	position:absolute;
}
#ccw #mvt_price {
	background:url(/images/custom/price.png) no-repeat;
	width:82px;
	height:82px;
	top:0px;
	right:374px;
	position:absolute;
}
#ccw #grad {
	background:url(/images/customCase/header-1px-gradient.png) repeat-x;
	width:100%;
	height:65px;
	top:45px;
	position:absolute;
}
#ccw #first {
	display:table;
	/*height:1235px;*/
	/*border:1px solid lightgreen;*/
}
#ccw #first #flashContainer {
	width:713px;
	/*height:1000px;*/
	position:relative;
	display:table;
}
#ccw #first #flashContainer #flashBorder {
	width:710px;
	height:890px;
	background:#333333;
	border-radius: 8px;
	-moz-border-radius: 8px;
	-khtml-border-radius: 8px;
	-webkit-border-radius: 8px;
	padding-top:20px;
/*	position:absolute;
	left:0px;
	top:20px;*/
}
#ccw #first #flashContainer #flashBorder #flashBorderInner {
	width:670px;
	height:870px;
	background:#FFF;
	border-radius: 8px;
	-moz-border-radius: 8px;
	-khtml-border-radius: 8px;
	-webkit-border-radius: 8px;
	margin-left:auto;
	margin-right:auto;
}
#ccw #first #flashContainer #flashBorder #flashBorderInner #flashContent {
	/*width:650px;
	height:870px;*/
	margin-left:auto;
	margin-right:auto;
}
#ccw #first #aside {
	width:240px;
	display:table;
}
#ccw #first #aside .help #a1 {
	height:286px;
}
#ccw #first #aside .help #a2 {
	height:309px;
}
#ccw #first #aside .help #a3 {
	height:225px;
}
#ccw .helpBody p {
	width:200px;
	/*border:1px solid orange;*/
}
#ccw #first #aside .help {
	/*border:1px solid purple;*/
	/*position:relative;*/
	margin-bottom:1.0em;
}
#ccw #first #aside .helpHeader {
	/*background:url(/images/customCase/title-header.png);*/
	width:237px;
	height:50px;
	/*top:0px;
	position:absolute;*/
	border:1px solid #ECECEC;
	
	border-top-left-radius: 8px;
	border-top-right-radius: 8px;
	border-bottom-left-radius: 0px;
	border-bottom-right-radius: 0px;
	
	-moz-border-radius-topleft: 8px;
	-moz-border-radius-topright: 8px;
	-moz-border-radius-bottomright: 0px;
	-moz-border-radius-bottomleft: 0px;
	
	-webkit-border-top-left-radius: 8px;
	-webkit-border-top-right-radius: 8px;
	-webkit-border-bottom-right-radius: 0px;
	-webkit-border-bottom-left-radius: 0px;
	
	background: #ffffff; /* Old browsers */
	background: -moz-linear-gradient(top,  #ffffff 1%, #ececec 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(1%,#ffffff), color-stop(100%,#ececec)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  #ffffff 1%,#ececec 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  #ffffff 1%,#ececec 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  #ffffff 1%,#ececec 100%); /* IE10+ */
	background: linear-gradient(to bottom,  #ffffff 1%,#ececec 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ececec',GradientType=0 ); /* IE6-9 */
}
#ccw #first #aside h4 {
	/*line-height:50px;*/
	padding-top:0.3em;
}
#ccw #first #aside .helpBody {
	width:237px;
	border:1px solid #ECECEC;

	border-top-left-radius: 0px;
	border-top-right-radius: 0px;
	border-bottom-left-radius: 8px;
	border-bottom-right-radius: 8px;
	
	-moz-border-radius-topleft: 0px;
	-moz-border-radius-topright: 0px;
	-moz-border-radius-bottomright: 8px;
	-moz-border-radius-bottomleft: 8px;
	
	-webkit-border-top-left-radius: 0px;
	-webkit-border-top-right-radius: 0px;
	-webkit-border-bottom-right-radius: 8px;
	-webkit-border-bottom-left-radius: 8px;

	height:100%;
	/*border:1px solid pink;*/
	padding-bottom:1.5em;
}
#ccw #first #aside #VidTut {
	width:200px;
	height:108px;
	/*background:darkgray;*/
	background:url(/images/customCase/button-video-tutorial.png) no-repeat;
	width:197px;
	height:120px;
	margin-left:auto;
	margin-right:auto;
	cursor:pointer;
}
#divViewNow {
	margin-left:auto;
	margin-right:auto;
	width:137px;
	height:54px;
	/*background:url(/images/customCase/cta-view-now.png) no-repeat;*/
	background:#FF9900;
	cursor:pointer;
	color:#FFF;

	line-height:54px;
	text-align:center;
	text-transform:uppercase;
	font-weight:800;
	font-family: 'Open Sans', sans-serif;
	font-size:14px;
	
	border-radius: 4px;
	-moz-border-radius: 4px;
	-khtml-border-radius: 4px;
	-webkit-border-radius: 4px;
		
	-webkit-box-shadow: 0px 1px 4px 0px darkgray; /* Saf3-4, iOS 4.0.2 - 4.2, Android 2.3+ */
	box-shadow: 0px 1px 4px 0px darkgray; /* Opera 10.5, IE9, FF4+, Chrome 6+, iOS 5 */

}
#ccw #divViewNow a {
	color:white;
	text-decoration:none;
}
.separator {
	background:url(/images/customCase/hori-div.png) no-repeat;
	width:972px;
	height:38px;
}
#ccw #second {
	height:500px;
}
#ccw #third {
	margin-bottom:1.25em;
}
#ccw #third h3 {
	text-align:left;
	padding-left:0.5em;
	font-size:0.75em;
}
#ccw #third li {
	font-size:0.625em;
}
#ccw #second h2 {
	margin-top:0.0em;
	margin-bottom:0.625em;
}
.colBase {
	position:absolute;
	width:100%;
	height:10px;
	bottom:0px;
}
.colBase div {
	background:#EBEBEB;
	width:300px;
	height:10px;
	margin-left:auto;
	margin-right:auto;	
}
.triCol {
	float:left;
	width:33%;
	height:427px;
	position:relative;
}
#triColMiddle {
	border-left:1px solid #EBEBEB;
	border-right:1px solid #EBEBEB;
}
.whyImgX {
	width:300px;
	height:155px;
	margin-left:auto;
	margin-right:auto;
}
#whyImg1 {
	background:url(/images/customCase/quality-hero-1.jpg) no-repeat;
}
#whyImg2 {
	background:url(/images/customCase/quality-hero-2.jpg) no-repeat;
}
#whyImg3 {
	background:url(/images/customCase/quality-hero-3.jpg) no-repeat;
}
.triCol h3 {
	margin-top:0.75em;
	margin-bottom:1.5em;
}
.triCol p {
	width:260px;
}

#getFlash { margin-left:100px; text-align:center; width:500px; }

</style>
<div id="ccw">
	<div id="header">
    	<div id="grad"></div>
		<div id="tool"></div>
        <div id="arrow"></div>
        <div id="photo"></div>
		<div id="mvt_price"></div>
		<div id="price"></div>
        <div id="headerBanner">
            <h1>Customize Your <span class="h1Bold">Case</span></h1>
            <div class="hr" id="hrHeader"></div>
            <p>High-Quality Images Printed Directly On Your Cover - Not A Sticker!</p>
        </div>
    </div>
    <div id="first">
    	<table border="0" width="100%">
        	<tr>
            	<td align="left" valign="top">
                    <div id="flashContainer">
                        <% if not bToolAvail then %>
                        <div style="font-size:24px; color:#333; width:100%; text-align:center; padding-top:100px;">
                            Custom Design Tool is currently unavailable while we are performing upgrades.<br /><br />
                            Please check back soon.
                        </div>
                        <% else %>
                        <div id="flashBorder">
                            <div id="flashBorderInner">
                                <script type="text/javascript">embedPlayer();</script>
                                <div id="fb-root"></div>
                                <div id="flashContent">
                                    <style type="text/css" media="screen"> 
                                        html, body  { height:100%; }
                                        object:focus { outline:none; }
                                        #flashContent { display:none; }
                                    </style>
                                    <link rel="stylesheet" type="text/css" href="bin-release/history/history.css" />
                                    <script type="text/javascript" src="bin-release/history/history.js"></script>  
                                    <script type="text/javascript" src="bin-release/swfobject.js"></script> 
                                    <script type="text/javascript"> 
                                        var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
                                        document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
                                                        + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
                                    </script> 
                                </div>
                            </div>
                        </div>
                        <div id="getFlash">
                            <p>To view this page ensure that Adobe Flash Player version 10.0.0 or greater is installed.</p>
                            <a href='http://www.adobe.com/go/getflashplayer'><img src='//www.adobe.com/images/shared/download_buttons/get_flash_player.gif' border="0" alt='Get Adobe Flash player' /></a>
                        </div>
                        <div id="mvt_models" style="padding:15px 0px 15px 0px;">
                            <div><img src="/images/custom/title.png" border="0" /></div>
                            <div style="padding:10px 0px 10px 0px;">
                            	<%
								sql = "exec sp_customModels"
								session("errorSQL") = sql
								set customModelsRS = oConn.execute(sql)
								
								curBrand = ""
								do while not customModelsRS.EOF
									if curBrand <> customModelsRS("brandName") then
										curBrand = customModelsRS("brandName")
								%>
                                <div class="tb" style="font-size:15px; font-weight:bold; color:#ff6600;"><%=customModelsRS("brandName")%>:</div>
                                <div class="tb" style="margin-bottom:10px;">
                                	<div class="fl"><%=customModelsRS("modelName")%></div>
                                <%
									else
								%>
                                	<div class="fl" style="margin:0px 5px 0px 2px; font-weight:bold;">,</div>
                                	<div class="fl"><%=customModelsRS("modelName")%></div>
                                <%
									end if
									customModelsRS.movenext
									if customModelsRS.EOF then
								%>
                                </div>
                                <%
									elseif curBrand <> customModelsRS("brandName") then
								%>
                                </div>
                                <%
									end if
								loop
								%>                
                            </div>
                        </div>
                        <% end if %>
                    </div>
                </td>
            	<td align="right" valign="top">
                    <div id="aside" style="text-align:left;">
                        <div class="help" id="a1">
                            <div id="mvt_click_model" style="padding-bottom:10px;">
                                <a href="#mvt_models"><img src="/images/custom/new-models-widget.png" border="0" /></a>
                            </div>
                            <% if month(date) = 11 or month(date) = 12 then %>
                            <div style="padding:10px; border:1px solid #CCC; border-radius:5px; margin-bottom:5px;">
                            	The last suggested pre-Christmas ordering date is AT LEAST ELEVEN (11) SHIPPING DAYS BEFORE 
                                DECEMBER 24TH, <%=year(date)%> (or Friday, December 6th, <%=year(date)%>). 
                            </div>
                            <% end if %>
                            <div class="helpHeader">
                                <h4>Need Assistance?</h4>
                            </div>
                            <div class="helpBody">
                                <p>We're dedicated to giving you the best service posible.  If you have any questions or you encounter 
                                any problems while designing your custom faceplace, do not hesitate to contact us.</p>
                                <p><strong>&gt; Phone:</strong> 1-800-305-1106</p>
                                <p><strong>&gt; Live Chat:</strong> <a href="javascript:void()" onclick="OpenLHNChat();return false;">Click Here</a></p>
                            </div>
                        </div>
                        <div class="help" id="a2">
                            <div class="helpHeader">
                                <h4>Video Tutorial</h4>
                            </div>
                            <div class="helpBody">
                                <p>View our simple step-by-step instructional video and learn how to design your faceplate like a pro!</p>
                                <div id="VidTut" onclick="showVidTut()"></div>
                            </div>
                        </div>
                        <div class="help" id="a3">
                            <div class="helpHeader">
                                <h4>Quick-Start Guide</h4>
                            </div>
                            <div class="helpBody">
                                <p>Check out our visual aide for a look at how simple our custom design process really is.</p>
                                <div id="divViewNow" onclick="showCustomCaseInfo()"><a href="javascript:void()">View Now</a></div>
                            </div>
                        </div>
                        <div class="help" id="a4">
                            <div class="helpHeader">
                                <h4>Please Note</h4>
                            </div>
                            <div class="helpBody">
                                <p><strong>Returns/Exchange:</strong> Since each item is crafted uniquely for you, customized products may not be returned or exchanged.</p>
                                <p><strong>Shipping:</strong> Please allow up to <%=gblCustomShippingDate%> business days for production of custom cases prior to shipping time.</p>
                                <p><strong>ShopRunner Members:</strong> ShopRunner orders still quality for FREE 2-Day shipping after your custom case is created. For example, if your case takes four days to be created, it will ship out after that four day period via 2-Day shipping.</p>
                            </div>
                        </div>
                    </div>                
                </td>
            </tr>
        </table>
    </div>
    <div class="separator"></div>
    <div id="second">
    	<h2>Custom Faceplate Design 101: An Introduction</h2>
        <div class="triCol">
        	<div class="whyImgX" id="whyImg1"></div>
            <h3>Acceptable File Types</h3>
            <p>Wireless Emporium supports images in both JPEG and PNG formats.  Both formats will produce great quality products as long as the resolution meets or exceeds our recommendations.
            With PNG, we support full transparency of your images as you design.  However, please make sure the quality of your image is high enough for printing on a product.
            </p>
            <div class="colBase"><div></div></div>
        </div>
        <div class="triCol" id="triColMiddle">
        	<div class="whyImgX" id="whyImg2"></div>
            <h3>Suggested Image Sizes</h3>
            <p>To fill the full area, we recommend that you use images that are equal to or larger than the recommended size of 750 x 1425 @ 200ppi.
            You can design your images much smaller than these recommended sizes and place them anywhere on the case.  
            If your image is bigger than these sizes, it'll turn out even better since it will be printed at an even higher resolution than the minimum.</p>
            <div class="colBase"><div></div></div>
        </div>
        <div class="triCol">
        	<div class="whyImgX" id="whyImg3"></div>
            <h3>Create Your Inspirations</h3>
            <p>Our custom configurator tool enables you to position, scale, and rotate your image, resulting in a one-of-a-kind reflection of your personality.
            By using large, high-resolution images to begin with, you're able to achieve a terrific amount detail in your final printed product.
            Our advanced ECO-UV printing process transfers the ink deep into the case's surface to ensure long-lasting vibrant designs.</p>
            <div class="colBase"><div></div></div>
        </div>
    </div>
    <% if activeContest = 1 then %>
    <div id="third">
		<!--#include virtual="/custom/includes/inc_contestRules.asp"-->
    </div>
    <% end if %>
</div>



<%
'<div style="float:left;"><img src="/images/customPrint/customize-banner1.jpg" border="0" /></div>
'<div style="padding:20px 0px 20px 150px; border:10px solid red;background:lightgray;">
'
'<script type="text/javascript">embedPlayer();<'/script>
'<div id="fb-root"></div>
'<div id="flashContent">
'    <style type="text/css" media="screen"> 
'        html, body  { height:100%; }
'        object:focus { outline:none; }
'        #flashContent { display:none; }
'    </style>
'    <link rel="stylesheet" type="text/css" href="bin-release/history/history.css" />
'    <script type="text/javascript" src="bin-release/history/history.js"><'/script>  
'    <script type="text/javascript" src="bin-release/swfobject.js"><'/script> 
'    <p>To view this page ensure that Adobe Flash Player version 10.0.0 or greater is installed. </p>
'    <script type="text/javascript"> 
'        var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
'        document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
'                        + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
'    <'/script> 
'</div>
'
'</div>
'<div style="float:left; border-top:3px groove #CCC; width:100%;">
'	<div style="float:left;"><img src="/images/customPrint/about-image.jpg" border="0" /></div>
'    <div style="float:left; width:400px; margin:35px 0px 0px 15px;">
'    	<div style="float:left; width:400px; font-weight:bold; font-size:20px; color:#333;">ABOUT OUR COVERS & FACEPLATES:</div>
'        <div style="float:left; width:400px; font-weight:bold; font-size:11px; color:#333; margin-top:15px;">What separates us from the competition?</div>
'        <div style="float:left; width:400px; font-weight:bold; font-size:11px; color:#666; margin-top:15px;">
'	        Our custom phone covers &amp; cases are not merely stickers or vinyl print outs - the designs are printed out with high quality ink with an industrial printer onto a case made specifically for your phone. For example, if you have an iPhone 4S, your hard protective case will have the design printed on it as if it came straight from the factory.
'    	    <br /><br />
'        	Wireless Emporium's custom cell phone case printing service combines the best quality, a perfect fit and rock bottom prices in a service that is truly second to none.
'        </div>
'    </div>
'</div>
'<div style="float:left; border-top:3px groove #CCC; width:100%; border-bottom:3px groove #CCC; width:100%;">
'    <div style="float:left; width:400px; margin-top:35px">
'    	<div style="float:left; width:400px; font-weight:bold; font-size:20px; color:#333;">MAKE IT YOURS:</div>
'        <div style="float:left; width:400px; font-weight:bold; font-size:11px; color:#666; margin-top:15px;">
'	        Our software allows you to let your imagination run wild. Not only can you create custom cell phone cases, but you can also personalize your other electronic devices. Imagine the envy of your friends when your iPhone 4S has a high quality, form fitting case with your favorite sports team.
'            <br /><br />
'            Through our interactive case creation software, you can upload and edit whatever image you want. Make a statement by changing the colors of your case, adding or modifying text and changing the font or size. Whatever your taste or style, you'll be sure to find an endless world of possibilities with our custom cover printing service. 
'        </div>
'    </div>
'    <div style="float:left; margin-left:15px;"><img src="/images/customPrint/make-it-yours-image.jpg" border="0" /></div>
'</div>
%>
<script type="text/javascript">
	function showCustomCaseInfo() {
		viewPortW = $(window).width();
		window.scrollTo(0, 0);
		if (document.getElementById('prodDescription') != null) document.getElementById('prodDescription').style.display = 'none';	
		ajax("/ajax/ajaxCustomCaseInfographic.asp?screenW="+viewPortW,'popBox');
		document.getElementById("popBox").style.position = "absolute";
		document.getElementById("popCover").onclick = function () { closeFloatingBox() }
		document.getElementById("popBox").onclick = function () { closeFloatingBox() }
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	function showVidTut() {
		viewPortH = $(window).height();
		window.scrollTo(0, 0);
		if (document.getElementById('prodDescription') != null) document.getElementById('prodDescription').style.display = 'none';	
		ajax("/ajax/ajaxCustomVideoTutorial.asp?screenH="+viewPortH,'popBox');
		//var e = document.getElementById('popBox');
		//e.innerHTML = '<iframe width="200" height="108" src="http://www.youtube.com/embed/GrcQ31aW4CI?rel=0&showinfo=0&wmode=opaque&modestbranding=1" frameborder="0" allowfullscreen></iframe>';
		document.getElementById("popBox").style.position = "absolute";
		document.getElementById("popCover").onclick = function () { closeFloatingBox() }
		document.getElementById("popBox").onclick = function () { closeFloatingBox() }
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	function closeFloatingBox() {
		if (document.getElementById('prodDescription') != null) document.getElementById('prodDescription').style.display = '';
		//------------------ change it to original -------------------//
		document.getElementById("popBox").style.position = "fixed";
		document.getElementById("popCover").onclick = function () {}
		document.getElementById("popBox").onclick = function () {}
		//------------------------------------------------------------//
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
		document.getElementById("popCover").innerHTML = "";
		document.getElementById("popBox").innerHTML = "";
	}	
</script>
<!--#include virtual="/includes/template/bottom_index.asp"-->
