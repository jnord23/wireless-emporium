<?php
ob_start();
session_start();
$designId = $_GET["designId"]=='' ? 0 :$_GET["designId"];
$aspId = $_GET["aspId"]=='' ? 0 :$_GET["aspId"];
?>
<html xmlns="http://www.w3.org/1999/xhtml" center="0" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="history/history.css" />
<style>.main {padding:0px;} .col-main{ padding:0px;} #flashContent{display:block;}</style>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript" src="FBJSBridge.js"></script>
<title></title>
<script type="text/javascript">
    function popuponclick(s)
    {
        // alert("popup");
        my_window = window.open(s,"mywindow","status=1,width=800,height=600");
    }

    function closepopup()
    { 
        // alert("closeup");
        my_window.close ();
    }

    var attributes = {};

    function embedPlayer() {
        var flashvars = {designId:"<?=$designId?>",aspId:"<?=$aspId?>"};
        var params = {menu: "false"};	        
        swfobject.embedSWF("index.swf", "flashContent", "650", "870", "9.0", null, flashvars , params, {name:"flashContent"});
    }
    
    //Redirect for authorization for application loaded in an iFrame on Facebook.com 
    function redirect(id,perms,uri) { //alert(id+'=='+perms+'=='+uri);
        var params = window.location.toString().slice(window.location.toString().indexOf('?'));
        top.location = 'https://graph.facebook.com/oauth/authorize?client_id='+id+'&scope='+perms+'&redirect_uri='+uri+params;				 
    }    			
</script>


</head>

<body>
<div style="margin:20px  auto; width:960px;">

<script type="text/javascript">
embedPlayer();
</script>


<div id="fb-root"></div>
<div id="flashContent">
    <style type="text/css" media="screen"> 
        html, body  { height:100%; }
        object:focus { outline:none; }
        #flashContent { display:none; }
    </style>
    <link rel="stylesheet" type="text/css" href="bin-release/history/history.css" />
    <script type="text/javascript" src="bin-release/history/history.js"></script>  
    <script type="text/javascript" src="bin-release/swfobject.js"></script> 
    <p>To view this page ensure that Adobe Flash Player version 10.0.0 or greater is installed. </p>
    <script type="text/javascript"> 
        var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
        document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
                        + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
    </script> 
</div>
</div>
</body>
</html>
