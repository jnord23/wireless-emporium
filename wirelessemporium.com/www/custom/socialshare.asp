<%
	response.buffer = true
	noleftnav = 1

%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
	socialshare_norder = prepInt(request.QueryString("itemid"))
	if socialshare_norder = "" then response.end
	sql =	"SELECT		A.itemPic, A.itemid  " & vbcrlf &_
			"FROM		we_orderdetails B left join we_items A " & vbcrlf &_
			"			ON B.itemid = A.itemid " & vbcrlf &_
			"WHERE		B.itemID = " & socialshare_norder & " " & vbcrlf &_
			"			and A.PartNumber like 'WCD-%' "
	session("errorSQL") = sql
	'response.write sql
	set	socialshare_rs = oConn.execute(SQL)
	if not socialshare_rs.eof then
		socialshare_facebook = true
		itemPic = socialshare_rs("itemPic")
		itemId = socialshare_rs("itemid")
		pageUrl = "http://" & request.ServerVariables("HTTP_HOST") & "/custom-case-" & itemId
		'response.write pageUrl
	end if

	if socialshare_facebook then 'Take first image in recordset and make available to facebook
		myCustomCaseImg = "http://www.wirelessemporium.com/productpics/big/" & socialshare_rs("itemPic")
		myCustomCaseUrl = "http://" & request.ServerVariables("HTTP_HOST") & request.ServerVariables("HTTP_X_REWRITE_URL")
		pageName = "mycustomcase" 'prints facebook opengraph tags
	end if
%>
<!--#include virtual="/includes/template/top.asp"-->


<link href='http://fonts.googleapis.com/css?family=Bitter:400italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,800' rel='stylesheet' type='text/css'>
<style>
#cw {
	/*Prevent the borders from blowing out*/
	width:958px;
}
#cw #header, #cw #first, #cw #socialShare, #cw #second, #cw #third, #cw #fourth {
	position:relative;
	/*border:1px solid green;*/
}
#cw h2, #cw h3 {
	font-weight:800;
	font-family: 'Open Sans', sans-serif;
	/*font-size:25px;*/
	color:#333333;
	text-transform:uppercase;
	display:block;
	text-align:center;
	white-space:nowrap;
}
#cw h2 {
	font-size:2.5em;
	line-height:1.5em;
	margin-top:0.0em;
	margin-bottom:0.0em;
}
#cw h3 {
	font-size:1.2em;
	line-height:1.2em;
}
#cw p {
	margin-left:auto;
	margin-right:auto;
	line-height:1.75em;
	color:#666666;
	font-family:Verdana, Geneva, sans-serif;
}
#cw #header {
	height:110px;
}
#cw #headerBanner {
	position:absolute;
	left:13px;
	top:5px;
	width:530px;
	height:175px;
}
#cw #headerBanner h1 {
	font-size:4.0em;
	font-family: 'Open Sans', sans-serif;
	font-weight:300;
	padding-left:0;
	margin-bottom:-0.125;
}
#cw #headerBanner .h1Bold {
	font-weight:800;
	font-family: 'Open Sans', sans-serif;
	color:#333333;
	text-transform:uppercase;
}
#cw #headerBanner #hrHeader {
	height:1px;
	width:475px;
	background:#CCCCCC;
}
#cw #headerBanner p {
	/*font-size:1.4em;*/
	font-size:16px;
	font-family: 'Open Sans', sans-serif;
	font-weight:300;
	margin-top:0em;
	padding-left:0.25em;
	padding-right:0em;
}
#cw #tool {
	background:url(/images/customCase/header-tool-small.png) no-repeat;
	width:211px;
	height:133px;
	right:0px;
	top:-4px;
	position:absolute;
	z-index:20;
}
#cw #arrow {
	background:url(/images/customCase/header-arrow-small.png) no-repeat;
	width:28px;
	height:28px;
	top:43px;
	right:190px;
	position:absolute;
}
#cw #photo {
	background:url(/images/customCase/header-photograph-small.png) no-repeat;
	width:111px;
	height:80px;
	top:22px;
	right:236px;
	position:absolute;
}
#cw #price {
	background:url(/images/customCase/custom-case-pricing.png) no-repeat;
	width:82px;
	height:82px;
	top:0px;
	right:374px;
	position:absolute;
}
#cw #grad {
	background:url(/images/customCase/header-1px-gradient.png) repeat-x;
	width:100%;
	height:65px;
	top:45px;
	position:absolute;
}
#cw #socialShare {
	height:2.0em;
	line-height:2.0em;
	font-family: 'Bitter', serif;
	font-style:italic;
	font-weight:400;
	font-size:18px;
	/*color:#FF6609;
	color:#ff6600;*/
	color:#FC6400;
	padding-left:35px;
}
#cw #first {
	/*background:url(/images/customCase/splash-bg.jpg) no-repeat;*/
	height:357px;
}
#cw #hero {
<%
if socialshare_facebook then
	if not socialshare_rs.eof then
		do until socialshare_rs.eof 'As designed, there will only be a single case per page
			%>
	background:url(/productpics/big/<%=itemPic%>) no-repeat 110px 0px;
	height:300px;
			<% 
			socialshare_rs.movenext
		loop
	end if
else
%>
	background:url(/images/customCase/splash-hero-small.png) no-repeat;
	height:230px;
<% end if %>
	width:431px;
	top:16px;
	left:0px;
	position:absolute;
}
#cw #socialWrapper {
	width:296px;
	height:299px;
	left:96px;
	top:18px;
	position:absolute;
	z-index:30;
}
#cw #social {
	width:100%;
	height:100%;
	position:relative;
}
.sq {
	width:75px;
	height:25%;
	/*background:#969696;*/
	left:-100px;
	position:absolute;
	text-align:center;
	display:table-cell;
}
.sq p {
	/*vertical-align:bottom;
	line-height:0.5em;*/
}
#sq1 {
}
	#sq1 p {
	/*padding-top:1px;*/
	}
#sq2 {
	top:25%;
}
#sq3 {
	top:50%;
}
#sq4 {
	top:75%;
}
	#sq4 p {
	padding-top:0.25em;
	}
#cw #heroCopyWrapper {
	width:580px;
	height:299px;
	right:50px;
	top:18px;
	position:absolute;
}
#cw h2 {
	text-align:left;
}
#cw #heroCopy {
	width:100%;
	height:100%;
	position:relative;
}
#cw #heroCopy p {
	/*width:420px;*/
	margin-top:0.75em;
	margin-left:0px;
	line-height:2.25em;
}
#cw .pill {
	width:212px;
	height:50px;
	position:absolute;
	bottom:0px;
	color:#FFF;
	line-height:50px;
	text-align:center;
	text-transform:uppercase;
	font-weight:800;
	font-family: 'Open Sans', sans-serif;
	font-size:13px;
	
	border-radius: 4px;
	-moz-border-radius: 4px;
	-khtml-border-radius: 4px;
	-webkit-border-radius: 4px;
	
	overflow:hidden;
	cursor:pointer;
	
	-webkit-box-shadow: 0px 1px 4px 0px darkgray; /* Saf3-4, iOS 4.0.2 - 4.2, Android 2.3+ */
	box-shadow: 0px 1px 4px 0px darkgray; /* Opera 10.5, IE9, FF4+, Chrome 6+, iOS 5 */
}
#cw .pill a {
	color:white;
	text-decoration:none;
}
#cw #redPill {
	left:0px;
}
#cw #redPill .pillGrad {
	background: #ed5c6b; /* Old browsers */
	background: -moz-linear-gradient(top, #ed5c6b 0%, #cc2328 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ed5c6b), color-stop(100%,#cc2328)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #ed5c6b 0%,#cc2328 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #ed5c6b 0%,#cc2328 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #ed5c6b 0%,#cc2328 100%); /* IE10+ */
	background: linear-gradient(to bottom, #ed5c6b 0%,#cc2328 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ed5c6b', endColorstr='#cc2328',GradientType=0 ); /* IE6-9 */
	
	/*Repeat for WebKit*/
	border-radius: 4px;
	-moz-border-radius: 4px;
	-khtml-border-radius: 4px;
	-webkit-border-radius: 4px;
}
#cw #blackPill {
	left:222px;
}
#cw #blackPill .pillGrad {
	background: #484848; /* Old browsers */
	background: -moz-linear-gradient(top, #484848 0%, #1b1b1b 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#484848), color-stop(100%,#1b1b1b)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #484848 0%,#1b1b1b 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #484848 0%,#1b1b1b 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #484848 0%,#1b1b1b 100%); /* IE10+ */
	background: linear-gradient(to bottom, #484848 0%,#1b1b1b 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#484848', endColorstr='#1b1b1b',GradientType=0 ); /* IE6-9 */
	
	/*Repeat for WebKit*/
	border-radius: 4px;
	-moz-border-radius: 4px;
	-khtml-border-radius: 4px;
	-webkit-border-radius: 4px;
}
.separator {
	background:url(/images/customCase/hori-div.png) no-repeat;
	width:972px;
	height:38px;
}
#cw #second {
	height:528px;
}
#cw #second h2 {
	margin-top:0.0em;
	margin-bottom:1.25em;
}
.colBase {
	position:absolute;
	width:100%;
	height:10px;
	bottom:0px;
}
.colBase div {
	background:#EBEBEB;
	width:300px;
	height:10px;
	margin-left:auto;
	margin-right:auto;	
}
.triCol {
	float:left;
	width:33%;
	height:385px;
	position:relative;
}
#triColMiddle {
	border-left:1px solid #EBEBEB;
	border-right:1px solid #EBEBEB;
}
.whyImgX {
	width:300px;
	height:155px;
	margin-left:auto;
	margin-right:auto;
}
#whyImg1 {
	background:url(/images/customCase/design-hero-1.jpg) no-repeat;
}
#whyImg2 {
	background:url(/images/customCase/design-hero-2.jpg) no-repeat;
}
#whyImg3 {
	background:url(/images/customCase/design-hero-3.jpg) no-repeat;
}
.triCol h3 {
	margin-top:0.75em;
	margin-bottom:1.5em;
}
.triCol p {
	width:260px;
}
#third {
	height:450px;
}
#cw #third h2 {
	margin-top:0.0em;
	margin-bottom:1.25em;
}
#leftHalf {
	float:left;
	width:59%;
}
#leftHalf div {
	width:480px;
	margin-left:auto;
	margin-right:auto;
}
#rightHalf {
	float:left;
	width:39%;
}
#rightHalf div {
	width:325px;
	margin-right:10%;
}
#third div {

}
#third h3 {
	white-space:normal;
	text-align:left;
	font-size:1.5em;
	line-height:1.5em;
	margin-top:3.0em;
}
#third p {
	width:335px;
}
#fourth {
	height:389px;
}
#fourth h2 {
	margin-bottom:1.0em;
}
#fourth .quarter {
	float:left;
	/*width:242px;*/
	width:25%;
}
#fourth .giX {
	width:220px;
	height:231px;
	margin-left:auto;
	margin-right:auto;
	padding-top:10px;
}
#fourth .giX div {
	width:110px;
	height:205px;
	margin-left:auto;
	margin-right:auto;
}
#fourth #gi1 {
	background:url(/images/customCase/inspiration-bg.jpg) no-repeat;
}
#fourth #gi2 {
	background:url(/images/customCase/inspiration-bg.jpg) no-repeat;
}
#fourth #gi3 {
	background:url(/images/customCase/inspiration-bg.jpg) no-repeat;
}
#fourth #gi4 {
	background:url(/images/customCase/inspiration-bg.jpg) no-repeat;
}
#fourth #gi1 div {
	background:url(/images/customCase/faceplate-inspiration-1.png) no-repeat;
}
#fourth #gi2 div {
	background:url(/images/customCase/faceplate-inspiration-2.png) no-repeat;
}
#fourth #gi3 div {
	background:url(/images/customCase/faceplate-inspiration-3.png) no-repeat;
}
#fourth #gi4 div {
	background:url(/images/customCase/faceplate-inspiration-4.png) no-repeat;
}
#fourth p.author, #fourth p.createAction {
	text-align:center;
	font-size:1.0em;
	line-height:1.0em;
}
#fourth .author a, #fourth .createAction a {
	font-weight:bold;
	color:#329BCB;
	font-style:italic;
	text-decoration:none;
}
#closeButton {
	text-transform:uppercase;
	position:absolute;
	right:-20px;
	top:-20px;
	width:34px;
	height:34px;
	background:#2C2C2C;
	text-align:center;
	border-radius:20px;
	font:bold 16px/34px Verdana, Geneva, sans-serif;
	border:3px solid #FFF;
	cursor:pointer;
	color:#FFF;
	z-index:100;
	-webkit-box-shadow: -2px 2px 4px 0px #000; /* Saf3-4, iOS 4.0.2 - 4.2, Android 2.3+ */
	box-shadow: -2px 2px 4px 0px #000; /* Opera 10.5, IE9, FF4+, Chrome 6+, iOS 5 */
}
</style>
<div id="cw">
	<div id="header">
    	<div id="grad"></div>
		<div id="tool"></div>
        <div id="arrow"></div>
        <div id="photo"></div>
        <div id="price"></div>
        <div id="headerBanner">
            <h1>Customize Your <span class="h1Bold">Case</span></h1>
            <div class="hr" id="hrHeader"></div>
            <p>High-Quality Images Printed Directly On Your Cover - Not A Sticker!</p>
        </div>
    </div>
   	<div id="socialShare">Somebody shared this with you:</div>
    <div id="first">
        <div id="hero"></div>
        <div id="socialWrapper">
			<div id="social">
                <div class="sq" id="sq1">
                    <p>
                        <div class="g-plus" data-action="share" data-annotation="vertical-bubble" data-height="60"></div>
                    </p>
                </div>
                <div class="sq" id="sq2">
                    <p>
                        <div id="fb-root"></div>
                        <div class="fb-like" data-href="<%=pageUrl%>" data-send="false" data-layout="box_count" data-width="50" data-show-faces="false" data-font="verdana"></div>
                    </p>
                </div>
                <div class="sq" id="sq3">
                    <p>
                        <a href="https://twitter.com/share" class="twitter-share-button" data-text="Check out my custom phone case " data-via="WirelessEmp" data-count="vertical">Tweet</a>
                    </p>
                </div>
                <div class="sq" id="sq4">
                    <p>
	                    <a href="http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.wirelessemporium.com%2Fcustom-case-<%=itemId%>&media=http%3A%2F%2Fwww.wirelessemporium.com%2Fimages%2FcustomCase%2Fpin-it-custom-case.jpg&description=Customize%20Your%20Phone%20Cover%20at%20WirelessEmporium.com" class="pin-it-button" count-layout="vertical"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>
                    </p>
                </div>
            </div>
        </div>
        <div id="heroCopyWrapper">
			<div id="heroCopy">
		        <h2>Check out this custom design!</h2>
                <p>Someone just shared their awesome new custom case with you! Interested in making one for yourself? At Wireless Emporium, you can add a personalized touch to your cell phone with one of our custom cases. With our innovative tool, you can upload your own photographs and designs, add witty text and also choose from a wide variety of background colors and images. Show off your personal style with a custom case today!</p>
                <div id="redPill" class="pill" onclick="window.location.href='<% if request.ServerVariables("HTTP_HOST") = "staging.wirelessemporium.com" then response.write "http://www.wirelessemporium.com" %>/custom/'"><div class="pillGrad"><a href="<% if request.ServerVariables("HTTP_HOST") = "staging.wirelessemporium.com" then response.write "http://www.wirelessemporium.com" %>/custom/">Customize Your Cover</a></div></div>
                <div id="blackPill" class="pill" onclick="showCustomCaseInfo()"><div class="pillGrad">Take An In-Depth Look</div></div>
			</div>
        </div>
    </div>
    <div class="separator"></div>
    <div id="second">
    	<h2>Why Design Your Own?</h2>
        <div class="triCol">
        	<div class="whyImgX" id="whyImg1"></div>
            <h3>Fully Customizable Protection</h3>
            <p>Delivers the normal impact resistance you have come to expect from a Wireless Emporium product, but with the added option of having your own custom design printed directly onto the surface. Support your favorite sports team or wow your family and friends with your stunning artwork.</p>
            <div class="colBase"><div></div></div>
        </div>
        <div class="triCol" id="triColMiddle">
        	<div class="whyImgX" id="whyImg2"></div>
            <h3>State of the Art Technology</h3>
            <p>You're only limited by your imagination! Unparalleled UV printing capabilities allow us to produce crisp and stunning full-color images on a vast array of cell phone cases. Wireless Emporium supports images in both JPEG and PNG formats.</p>
            <div class="colBase"><div></div></div>
        </div>
        <div class="triCol">
        	<div class="whyImgX" id="whyImg3"></div>
            <h3>Fast Turnaround, Free Shipping</h3>
            <p>We'll process your custom case within 1-2 business days from the time your order is placed. Combined with our fast, FREE shipping and industry-leading warranty policy, it's a no-brainer for anyone looking for the ultimate in custom cell phone protection.</p>
            <div class="colBase"><div></div></div>
        </div>
    </div>
    <div class="separator"></div>
    <div id="third">
    	<h2>Featured Video</h2>
        <div id="leftHalf">
        	<div>
        		<iframe width="480" height="270" src="http://www.youtube.com/embed/epi2_d2PhUA?rel=0&showinfo=0&modestbranding=1&wmode=opaque" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
		<div id="rightHalf">
        	<div>
                <h3>Custom Smartphone Protection Explained</h3>
                <p>Do you want to see our amazing printer in action? Are you curious as to how a printer can print directly onto a phone case? Check out the video on the left to see the entire process from start to finish and get more insight in this amazing 3D printing process.</p>
        	</div>
        </div>        
    </div>
    <div class="separator"></div>
    <div id="fourth">
    	<h2>Get Inspired: Select Creations</h2>
        <div class="quarter">
			<div class="giX" id="gi1">
            	<div></div>
            </div>
            <p class="author">Design by <strong>Robert S.</strong></p>
            <p class="createAction">Like this? <em><a href="/custom/">Create your own!</a></em></p>
        </div>
        <div class="quarter">
			<div class="giX" id="gi2">
            	<div></div>
            </div>
            <p class="author">Design by <strong>Shayla R.</strong></p>
            <p class="createAction">Like this? <em><a href="/custom/">Create your own!</a></em></p>
        </div>
        <div class="quarter">
			<div class="giX" id="gi3">
            	<div></div>
            </div>
            <p class="author">Design by <strong>Jonathan N.</strong></p>
            <p class="createAction">Like this? <em><a href="/custom/">Create your own!</a></em></p>
        </div>
        <div class="quarter">
			<div class="giX" id="gi4">
            	<div></div>
            </div>
            <p class="author">Design by <strong>Terry K.</strong></p>
            <p class="createAction">Like this? <em><a href="/custom/">Create your own!</a></em></p>
        </div>
    </div>
</div>
<script type="text/javascript">
	function showCustomCaseInfo() {
		viewPortW = $(window).width();
		window.scrollTo(0, 0);
		if (document.getElementById('prodDescription') != null) document.getElementById('prodDescription').style.display = 'none';	
		ajax("/ajax/ajaxCustomCaseInfographic.asp?screenW="+viewPortW,'popBox');
		document.getElementById("popBox").style.position = "absolute";
		document.getElementById("popCover").onclick = function () { closeFloatingBox() }
		document.getElementById("popBox").onclick = function () { closeFloatingBox() }
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	function closeFloatingBox() {
		if (document.getElementById('prodDescription') != null) document.getElementById('prodDescription').style.display = '';
		//------------------ change it to original -------------------//
		document.getElementById("popBox").style.position = "fixed";
		document.getElementById("popCover").onclick = function () {}
		document.getElementById("popBox").onclick = function () {}
		//------------------------------------------------------------//
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
		document.getElementById("popCover").innerHTML = "";
		document.getElementById("popBox").innerHTML = "";
	}	
</script>
<% 'Google Button %>
<script type="text/javascript">
  (function() {
	var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	po.src = 'https://apis.google.com/js/plusone.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<% 'Facebook Button %>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>                    
<% 'Twitter Button %>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>                    
<% 'Pinterest Button %>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>

<% 'Facebook Button $>
'<script>(function(d, s, id) {
'  var js, fjs = d.getElementsByTagName(s)[0];
'  if (d.getElementById(id)) return;
'  js = d.createElement(s); js.id = id;
'  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
'  fjs.parentNode.insertBefore(js, fjs);
'}(document, 'script', 'facebook-jssdk'));<$script>                 
%>
   
<!--#include virtual="/includes/template/bottom.asp"-->
