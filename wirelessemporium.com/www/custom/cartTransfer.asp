<%
response.buffer = true
mobileAccess = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<script src="/framework/UserInterface/Js/jquery/jquery-1.7.1.min.js"></script>
<%
	saveTime = time
	response.Write("Processing custom design<br>")
	response.Flush()
	dim prodID : prodID = prepInt(request.QueryString("prodID"))
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	dim jpeg : set jpeg = Server.CreateObject("Persits.Jpeg")
	Dim sConnection, objConn , objRS
	
	sConnection = "DSN=we_mySQL;Uid=sparx;Pwd=K!ll3r;"
	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open(sConnection)
	
	response.Write("Connecting...<br>")
	response.Flush()
	sql = 	"select b.dataArr, a.imageWithBorder as itemPic, c.categoryName as brandName, d.categoryName as modelName, e.productName " &_
			"from sd_main_product_view a " &_
				"left join sd_mainproduct b on b.id = a.product_id " &_
				"left join sd_categorydesc c on replace(b.categoryId,'-','') = c.catId " &_
				"left join sd_categorydesc d on b.subCatId = d.catId " &_
				"left join sd_product_description e on b.rowProductId = e.productId " &_
			"where a.product_id = " & prodID & " and c.langId = 1 and d.langId = 1"
	session("errorSQL") = sql
	set objRS = objConn.execute(sql)
	
	dataArr = objRS("dataArr")
	dataArray = split(dataArr,",")
	uploadedImages = ""
	for i = 0 to ubound(dataArray)
		'Break down the array elements and save the uploaded pictures results
		session("errorSQL") = "dataArr:" & dataArr & "<br>Currently On:" & dataArray(i)
		if instr(dataArray(i),":") > 0 then
			innerArray = split(dataArray(i),":")
			columnName = replace(innerArray(0),"""","")
			columnValue = replace(innerArray(1),"""","")
			if columnName = "imageId" then
				uploadedImages = uploadedImages & columnValue & ","
			end if
		end if
	next
	if uploadedImages <> "" then uploadedImages = left(uploadedImages,len(uploadedImages)-1)
	customImg = objRS("itemPic")
	brandName = objRS("brandName")
	modelName = objRS("modelName")
	baseProduct = objRS("productName")
	
	sql = 	"select a.brandID, a.brandCode, b.modelID, b.modelCode, c.itemID, c.price_our " &_
			"from we_brands a " &_
				"left join we_models b on b.modelName = '" & modelName & "' " &_
				"left join we_Items c on c.itemDesc = '" & baseProduct & "' " &_
			"where a.brandName = '" & brandName & "'"
	session("errorSQL") = sql
	set bmRS = oConn.execute(sql)
	
	brandID = prepInt(bmRS("brandID"))
	brandCode = prepStr(bmRS("brandCode"))
	modelID = prepInt(bmRS("modelID"))
	modelCode = prepStr(bmRS("modelCode"))
	baseItemID = prepInt(bmRS("itemID"))
	baseItemPrice = prepInt(bmRS("price_our"))
	
	sql = "select * from we_Items where itemID = " & baseItemID
	session("errorSQL") = sql
	set testRS = oConn.execute(sql)
	
	if testRS.EOF then
		response.Write("can't find case item: " & baseProduct)
		response.End()
	end if
	
	response.Write("Creating Product<br>")
	response.Flush()
	'start quick create	
	productName = "WE Custom Design"
	sufix = "01"
	templateID = "134574"
	prefix = "WCD"
	
	partNumber = prefix & "-" & baseItemID & "-" & sufix
	sql = "select top 1 replace(PartNumber,'" & prefix & "-" & baseItemID & "-','') as maxPN from we_Items where partNumber like '" & prefix & "-" & baseItemID & "-%' order by itemID desc"
	session("errorSQL") = sql
	set cntRS = oConn.execute(sql)
	
	response.Write("Similar product check<br>")
	response.Flush()
	
	if not cntRS.EOF then
		response.Write("Duplicate products found<br>")
		response.Flush()
		
		sufix = prepInt(cntRS("maxPN")) + 1
	end if
	
	response.Write("Image Transfer<br>")
	response.Flush()
	session("errorSQL") = "jpeg stuff: " & server.MapPath("/custom/files/mainproduct/extLarge/" & customImg) & " " & fso.fileExists(server.MapPath("/custom/files/mainproduct/extLarge/" & customImg))
	jpeg.Open server.MapPath("/custom/files/mainproduct/extLarge/" & customImg)
	session("errorSQL") = "open 1"
	jpeg.Save server.MapPath("/productpics/fullSized/WCD_" & customImg)
	session("errorSQL") = "save 1"
	
	jpeg.Height = 300
	jpeg.Width = jpeg.OriginalWidth * 300 / jpeg.OriginalHeight
	jpeg.Save server.MapPath("/productpics/big/WCD_" & customImg)
	
	jpeg.Open server.MapPath("/productpics/big/WCD_" & customImg)
	jpeg.Height = 100
	jpeg.Width = jpeg.OriginalWidth * 100 / jpeg.OriginalHeight
	jpeg.Save server.MapPath("/productpics/thumb/WCD_" & customImg)
	
	jpeg.Open server.MapPath("/productpics/big/WCD_" & customImg)
	jpeg.Height = 65
	jpeg.Width = jpeg.OriginalWidth * 65 / jpeg.OriginalHeight
	jpeg.Save server.MapPath("/productpics/homepage65/WCD_" & customImg)
	
	jpeg.Open server.MapPath("/productpics/big/WCD_" & customImg)
	jpeg.Height = 45
	jpeg.Width = jpeg.OriginalWidth * 45 / jpeg.OriginalHeight
	jpeg.Save server.MapPath("/productpics/icon/WCD_" & customImg)
	
	response.Write("Images created, removing temp records...<br>")
	response.Flush()
	
	session("errorSQL") = "delete slaves"
	sql = "delete from we_ItemsSlave"
	session("errorSQL") = sql
	oConn.execute(sql)
	
	response.Write("Duplicating base item<br>")
	response.Flush()
	
	sql = "SET NOCOUNT ON; insert into we_itemsSlave select [WE_URL],[brandID],[modelID],[typeID],[carrierID],[subtypeID],[vendor],[PartNumber],[itemDesc],[itemDesc_PS],[itemDesc_CA],[itemDesc_CO],[itemDesc_Google],[excludeFromCA],[ProductCatalog],[itemPic],[itemPic_CO],[itemSale],[price_Retail],[price_Our],[price_Our_BU],[price_CA],[price_CO],[price_PS],[price_ER],[price_Buy],[COGS],[numberOfSales],[inv_qty],[Seasonal],[Sports],[Bluetooth],[HandsfreeType],[ItemKit_OLD],[ItemKit_NEW],[hotDeal],[hideLive],[itemLongDetail],[itemHeader],[itemInfo],[BULLET1],[BULLET2],[BULLET3],[BULLET4],[BULLET5],[BULLET6],[BULLET7],[BULLET8],[BULLET9],[BULLET10],[COMPATIBILITY],[download_URL],[download_TEXT],[itemLongDetail_CA],[itemLongDetail_CO],[POINT1],[POINT2],[POINT3],[POINT4],[POINT5],[POINT6],[POINT7],[POINT8],[POINT9],[POINT10],[itemLongDetail_PS],[FEATURE1],[FEATURE2],[FEATURE3],[FEATURE4],[FEATURE5],[FEATURE6],[FEATURE7],[FEATURE8],[FEATURE9],[FEATURE10],[Features],[FormFactor],[BandType],[Band],[PackageContents],[TalkTime],[Standby],[Display],[Condition],[flag1],[UPCCode],[AMZN_sku],[BUY_sku],[itemDimensions],[itemWeight],[DateTimeEntd],[InvLastUpdatedBy],[InvLastUpdatedDate],[PriceLastUpdatedBy],[PriceLastUpdatedDate],[PS_videos],[Smartphone],[NoDiscount],[MobileLine_sku],[Phone_Compatibility],[DesignType],1 as master,0 as ghost,0 as lastCall,0 as topModel_PS,'' as itemLongDetail_ER,'' as itemDesc_ER,[colorID] from we_items where itemID = " & templateID & "; SELECT @@IDENTITY AS NewID;"
	session("errorSQL") = sql
	set objRS = oConn.execute(sql)
	newItemID = objRS.Fields("NewID").Value
	
	sql = "select * from we_itemsSlave where itemID = " & newItemID
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	itemDesc = insertDetails(rs("itemDesc"))
	itemDesc_PS = insertDetails(rs("itemDesc_PS"))
	itemDesc_CA = insertDetails(rs("itemDesc_CA"))
	itemDesc_CO = insertDetails(rs("itemDesc_CO"))
	itemPic_CO = insertDetails(rs("itemPic_CO"))
	tempHolding = insertDetails(rs("itemLongDetail"))
	itemLongDetail = tempHolding
	bullet1 = insertDetails(rs("bullet1"))
	bullet2 = insertDetails(rs("bullet2"))
	bullet3 = insertDetails(rs("bullet3"))
	bullet4 = insertDetails(rs("bullet4"))
	bullet5 = insertDetails(rs("bullet5"))
	bullet6 = insertDetails(rs("bullet6"))
	bullet7 = insertDetails(rs("bullet7"))
	bullet8 = insertDetails(rs("bullet8"))
	bullet9 = insertDetails(rs("bullet9"))
	bullet10 = insertDetails(rs("bullet10"))
	itemLongDetail_CA = insertDetails(rs("itemLongDetail_CA"))
	itemLongDetail_CO = insertDetails(rs("itemLongDetail_CO"))
	point1 = insertDetails(rs("point1"))
	point2 = insertDetails(rs("point2"))
	point3 = insertDetails(rs("point3"))
	point4 = insertDetails(rs("point4"))
	point5 = insertDetails(rs("point5"))
	point6 = insertDetails(rs("point6"))
	point7 = insertDetails(rs("point7"))
	point8 = insertDetails(rs("point8"))
	point9 = insertDetails(rs("point9"))
	point10 = insertDetails(rs("point10"))
	itemLongDetail_PS = insertDetails(rs("itemLongDetail_PS"))
	feature1 = insertDetails(rs("feature1"))
	feature2 = insertDetails(rs("feature2"))
	feature3 = insertDetails(rs("feature3"))
	feature4 = insertDetails(rs("feature4"))
	feature5 = insertDetails(rs("feature5"))
	feature6 = insertDetails(rs("feature6"))
	feature7 = insertDetails(rs("feature7"))
	feature8 = insertDetails(rs("feature8"))
	feature9 = insertDetails(rs("feature9"))
	feature10 = insertDetails(rs("feature10"))
	newPN = prefix & "-" & baseItemID & "-" & sufix
	
	response.Write("Update temp record<br>")
	response.Flush()
	
	sql = 	"update we_itemsSlave set vendor = 'WE', brandID = " & ds(brandID) & ", modelID = " & ds(modelID) & ", partNumber = '" & newPN & "', " &_
			"itemDesc = '" & ds(itemDesc) & "', itemDesc_PS = '" & ds(itemDesc_PS) & "', itemDesc_CA = '" & ds(itemDesc_CA) & "', itemDesc_CO = '" & ds(itemDesc_CO) & "', " &_
			"itemPic = 'WCD_" & customImg & "', itemPic_CO = 'WCD_" & customImg & "', itemLongDetail = '" & ds(itemLongDetail) & "', itemLongDetail_CA = '" & ds(itemLongDetail_CA) & "', itemLongDetail_CO = '" & ds(itemLongDetail_CO) & "', itemLongDetail_PS = '" & ds(itemLongDetail_PS) & "', " &_
			"price_our = '19.99', price_retail = '29.99', flag1 = " & prodID & ",inv_qty = 1, master = 1, hideLive = 0, ghost = 0, colorId = 0, packageContents = '" & uploadedImages & "' " &_
			"where itemID = " & newItemID
	session("errorSQL") = sql
	oConn.execute(sql)
	
	response.Write("Save new product<br>")
	response.Flush()
	
	sql = "SET NOCOUNT ON; insert into we_items select [WE_URL],[brandID],[modelID],[typeID],[carrierID],[subtypeID],[vendor],[PartNumber],[itemDesc],[itemDesc_PS],[itemDesc_CA],[itemDesc_CO],[itemDesc_Google],[excludeFromCA],[ProductCatalog],[itemPic],[itemPic_CO],[itemSale],[price_Retail],[price_Our],[price_Our_BU],[price_CA],[price_CO],[price_PS],[price_ER],[price_Buy],[COGS],[numberOfSales],[inv_qty],[Seasonal],[Sports],[Bluetooth],[HandsfreeType],[ItemKit_OLD],[ItemKit_NEW],[hotDeal],[hideLive],[itemLongDetail],[itemHeader],[itemInfo],[BULLET1],[BULLET2],[BULLET3],[BULLET4],[BULLET5],[BULLET6],[BULLET7],[BULLET8],[BULLET9],[BULLET10],[COMPATIBILITY],[download_URL],[download_TEXT],[itemLongDetail_CA],[itemLongDetail_CO],[POINT1],[POINT2],[POINT3],[POINT4],[POINT5],[POINT6],[POINT7],[POINT8],[POINT9],[POINT10],[itemLongDetail_PS],[FEATURE1],[FEATURE2],[FEATURE3],[FEATURE4],[FEATURE5],[FEATURE6],[FEATURE7],[FEATURE8],[FEATURE9],[FEATURE10],[Features],[FormFactor],[BandType],[Band],[PackageContents],[TalkTime],[Standby],[Display],[Condition],[flag1],[UPCCode],[AMZN_sku],[BUY_sku],[itemDimensions],[itemWeight],[DateTimeEntd],[InvLastUpdatedBy],[InvLastUpdatedDate],[PriceLastUpdatedBy],[PriceLastUpdatedDate],[PS_videos],[Smartphone],[NoDiscount],[MobileLine_sku],[Phone_Compatibility],[DesignType],1 as master,0 as ghost,0 as lastCall,0 as topModel_PS,'' as itemLongDetail_ER,'' as itemDesc_ER,[colorID] from we_itemsSlave where itemID = " & newItemID & "; SELECT @@IDENTITY AS NewID;"
	session("errorSQL") = sql
	set objRS = oConn.execute(sql)
	newItemID = objRS.Fields("NewID").Value
	
	sql = "delete from we_ItemsSlave"
	session("errorSQL") = sql
	oConn.execute(sql)
	'finish quick create
	response.Write("Transfering to cart (" & datediff("s",saveTime,time) & ")<br>")
	response.Flush()
%>
<style>
#divContestDialogWrapper {
	width:800px;
	height:162px;
	margin-left:auto;
	margin-right:auto;
	margin-top:325px;
	position:relative;
}
#divContestDialog, #divContestRules
{
	width:590px;
	height:172px;
	background-color:#000;
	position:absolute;
	right:0px;	
	border-radius: 8px;
	-moz-border-radius: 8px;
	-khtml-border-radius: 8px;
	padding:5px;
}
#divContestContent, #divContestRulesContent {
	background:#FFF;
	width:100%;
	height:100%;
	border-radius: 8px;
	-moz-border-radius: 8px;
	-khtml-border-radius: 8px;
	font-family:Verdana, Geneva, sans-serif;
}
#divContestInstructions {
	text-align:center;
	height:50%;
	padding-top:0.66em;
	font-size:1.875em;
	color:#666666;
	line-height:1.5em;
}
#divContestEntryArea {
	height:36px;
	width:450px;
	margin-left:auto;
	margin-right:auto;
	position:relative;
}
.buttonText {
	text-transform:uppercase;
	color:white;
	font-weight:bold;
	text-align:center;
	line-height:36px;
	font-size:1.125em;
}
#divContestEntryChooseYes, #divContestEntryChooseNo {
	height:36px;
	width:210px;
	position:absolute;
	top:0px;
	cursor:pointer;
}
#divContestEntryChooseYes {
	left:0px;
	background:#FF6600;
}
#divContestEntryChooseNo {
	right:0px;
	background:#666666;
}
#divContestEntryEmail, #divContestEntrySubmit {
	position:absolute;
	height:100%;
	display:none;
}
#divContestEntryEmail {
	left:0px;
	width:100%;
	border-radius: 8px;
	-moz-border-radius: 8px;
	-khtml-border-radius: 8px;
	
	-moz-box-shadow:    inset 1px 1px 6px #BBBBBB;
	-webkit-box-shadow: inset 1px 1px 6px #BBBBBB;
	box-shadow:         inset 1px 1px 6px #BBBBBB;


}
#divContestEntryEmail input {
	background:none;
	border:none;
	outline:none;
	height:100%;
	width:343px;
	
	padding-left:1.5em;
	color:#999999;
	font-style:italic;
	font-size:1.125em;
	
}

#divContestEntrySubmit {
	width:107px;
	right:0px;
	top:0px;
	background:#FF6600;
	cursor:pointer;
}
#divContestRulesLink {
	padding-top:1.0em;
	width:450px;
	margin-left:auto;
	margin-right:auto;
	position:relative;
	font-size:0.75em;
	color:#666666;
}
#divContestRules {
	height:350px;
	top:190px;
	display:none;
}
#divContestRulesIncludeWrapper {
	padding:1.5em;
	paddding-right:2.5em;
}
</style>
<script>
function showContestDialog() {
	document.getElementById("popBoxCustom").style.position = "absolute";
	document.getElementById("popCoverCustom").style.display = "";
	document.getElementById("popBoxCustom").style.display = "";
}

function choseSubmit() {
	var prodid = document.addItemForm.prodid.value;
	//var qty = document.addItemForm.prodid.value; //unnecessary
	var email = document.contestForm.email.value;
	//var musicSkins = document.addItemForm.musicSkins.value;//unnecessary
	if (email == getGhost()){
		alert('Please enter a valid email address');
	}else{
		var src ='/ajax/ajaxCustomCaseContest.asp';//?prodid='+prodid+'&email='+email;
		//alert(src);
		$.ajax({
			url: src,				
			cache: false, 
			data: {prodid:prodid,email:email},
			async:false,
			type:'GET',
			success: function(data, textStatus, XMLHttpRequest){            
				
			}, 	
			error:function(ts){
				alert(ts.responseStatus);
			}
		});
		document.addItemForm.submit();
	}
}
function choseYes() {
	//update display
	document.getElementById('divContestEntryChooseYes').style.display = 'none';
	document.getElementById('divContestEntryChooseNo').style.display = 'none';
	document.getElementById('divContestEntryEmail').style.display = 'block';
	//document.getElementById('email').focus();
	document.getElementById('divContestEntrySubmit').style.display = 'block';
}
function choseNo() {
	document.addItemForm.submit();
}
function getGhost() { return 'Enter your email address...' }
function remGhost(x) {
	if (x.value == getGhost()) {
		x.value = '';
	}
}
function addGhost(x) {
	if (x.value == '') {
		x.value = getGhost();
	}
}
function showRules() {
	document.getElementById('divContestRules').style.display = 'block';
}
</script>
<div style="margin-left:auto; margin-right:auto;">
	<div style="font-weight:bold; font-size:14px; text-align:center;">
    	Processing Your<br />
        WE Custom Design
        <br /><br />
        Please Wait...
    </div>
    <div style="text-align:center; width:300px;">
    	<form action="/cart/item_add" method="post" name="addItemForm">
			<input type="hidden" name="prodid" value="<%=newItemID%>" />
		    <input type="hidden" name="qty" value="1" />
		    <input type="hidden" name="musicSkins" value="0" />
		</form>
    </div>
</div>
<div id="popCoverCustom" style="height:100%; display:none; width:100%; background-color:#333; position:fixed; left:0px; top:0px; z-index:3000; opacity:0.6; filter:alpha(opacity=60);">&nbsp;</div>
<div id="popBoxCustom" style="height:2000px; display:none; width:100%; position:fixed; left:0px; top:0px; z-index:3001;margin:auto;">
    <div id="divContestDialogWrapper">
        <div id="divContestDialog">
        	<div id="divContestContent">
	        	<div id="divContestInstructions">
                	Submit your custom design for a chance to win<br />a brand-new Nexus 7 tablet from Google.
                </div>
                <div id="divContestEntryArea">
                	<form name="contestForm" action="javascript:void(0);" method="post">
                        <div id="divContestEntryChooseYes" class="buttonText" onclick="choseYes()">Submit My Design</div>
                        <div id="divContestEntryChooseNo" class="buttonText" onclick="choseNo()">No Thank You</div>
                        <div id="divContestEntryEmail"><input name="email" id="email" value="Enter your email address..." onblur="addGhost(this)" onfocus="remGhost(this)" tabindex="1" /></div>
                        <a href="javascript:choseSubmit()"><div id="divContestEntrySubmit" class="buttonText" tabindex="2">Submit!</div></a>
                    </form>
                </div>
	        	<div id="divContestRulesLink">
                    <a href="javascript:showRules()">Read the Contest Rules and Regulations</a>
                </div>
            </div>
        </div>
    <div id="divContestRules">
        <div id="divContestRulesContent">
        	<div id="divContestRulesIncludeWrapper">
            	<!--#include virtual="/custom/includes/inc_contestRules.asp"-->
            </div>
        </div>
    </div>
</div>
<!--#include virtual="/includes/template/bottom.asp"-->
<script language="javascript">	
	var src ="/custom/admin/pass.php?action=pdf&type=download&pid=<%=prodID%>";
	$.ajax({
		url: src,				
		cache: false, 
		type:'GET',
		success: function(data, textStatus, XMLHttpRequest){            
			
		}, 	
		error:function(XMLHttpRequest, textStatus, errorThrown){
			alert(textStatus);
			alert(errorThrown);
		}
	});
	
	setTimeout("document.addItemForm.submit()",1000) //just this line commented out while contest is running
	//setTimeout("showContestDialog()",1000) //Commented out now that the contest is over
</script>