<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
require_once('includes/JSON.php');
$genObj= new GeneralFunctions;

function getExtension($str)
{
	$i = strrpos($str,".");
	if (!$i)
	{
		return "";
	}
	$l = strlen($str) - $i;
	$ext = substr($str,$i+1,$l);
	return $ext;
}
	
//Variables==============================================

$ipAdd = $_SERVER['REMOTE_ADDR'];
$sessionId = $_POST['sessionId'];
$fileName	 = $_FILES['Filedata']['name'];
$file_tmp    = $_FILES['Filedata']['tmp_name'];
$file_type   = $_FILES['Filedata']['type'];
$file_size	 = $_FILES['Filedata']['size'];
$extension = strtolower(getExtension($fileName));

$tempSize=($file_size/1024);
$size=round($tempSize/1024);
if ($size <= 20) {
    //Insert into Upload Table=============================================
    $sql="INSERT INTO ".TBL_UPLOAD." SET extension='$extension', userId='0', width=0, height=0, sessionId='$sessionId', ip='$ipAdd', status='1', dateTime='".date("Y-m-d h:i:s")."'";
    $result = mysql_query($sql);
    $imageId = mysql_insert_id();


    $target ="";
    //Upload Image=====================================
    $target 		= "files/uploads/".$imageId.".".$extension;
    $targetThumb 	= "files/uploads/".$imageId."_small.".$extension;
    $targetLarge 	= "files/uploads/".$imageId."_large.".$extension;
    $res  = move_uploaded_file($file_tmp,$target);
    if($res){
        list($width, $height)=getimagesize($target);

        // get image ratio ...........................
        $largeThumbW = 1000;
        $largeThumbH = 1000;
        if($width>$largeThumbW && $height>$largeThumbH){   
        $largetImageSize = $largeThumbW.'x'.$largeThumbH;
        }else if($width<$largeThumbW && $height>$largeThumbH){   
        $largetImageSize = 'x'.$largeThumbH;
        }else if($width>$largeThumbW && $height<$largeThumbH){   
        $largetImageSize = $largeThumbW.'x';
        }else{
        $largetImageSize = $width.'x'.$height; 
        }


        //Thumb============================================
        $thumbQry=IMAGEMAGICPATH . " $target -resize 84x77 $targetThumb"; 
        exec($thumbQry);  

        //Large==============================================
        $largeQry=IMAGEMAGICPATH . " $target -resize $largetImageSize $targetLarge"; 
        exec($largeQry);


        //Update UPLOAD Table================================
        $sql="UPDATE ".TBL_UPLOAD." SET width='$width', height='$height' WHERE id=$imageId";
        $result = mysql_query($sql);

        //Create Object=======================================
        list($largeW, $largeH)=getimagesize($targetLarge);
        $obj = (object) $obj;
        $obj->id = $imageId;
        $obj->maxW = $largeW;
        $obj->maxH = $largeH;
        $obj->thumb = $targetThumb;
        $obj->url = $targetLarge;
        $obj->status = 1;
    }else{
        $obj = (object) $obj;    
        $obj->status = 0;
        $obj->errorNo = 0;
        
    }
}else{
    $obj = (object) $obj;    
    $obj->status = 0;
    $obj->errorNo = 1;
}
//Send Object======================================
$json = new JSON();
$var = $json->encode($obj);
echo $var;

//Write File for testing=============================
$f=fopen('pk.txt','a');
fwrite($f,"Ip=".$ipAdd."\n");
fwrite($f,"sessionid=".$sessionId."\n");
fwrite($f,"filename=".$fileName."\n");
fwrite($f,"filetmp=".$file_tmp."\n");
fwrite($f,"filetype=".$file_type."\n");
fwrite($f,"filesize=".$size."\n");
fwrite($f,"extension=".$extension."\n");
fclose($f);
$f=fopen('pk.txt','a');
fwrite($f,$var);
fclose($f);
?>

<?php
/*
if(obj.status == 0)
{

if(obj.errorNo == 0)//Simple upload io Error
if(obj.errorNo == 1) //Max size Error
if(obj.errorNo == 2) //Gif Error
if(obj.errorNo == 3)//Min Size Error
}

*/
?>