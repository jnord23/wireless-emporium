<?php
$generalDataObj = new GeneralFunctions();
$sitename = $generalDataObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal","systemName='SITE_NAME'");		


//-------For Language-------------------------------------

$default1 = "eng"; // default will be english...
$defLanguageId = "1";

if( isset( $_GET['lang'] ) )
{
	require_once ("languages/".$_GET['lang'] .".inc.php");	
	setcookie($sitename."_language", $_GET['lang'], time()+3600, '/'); 
}
elseif( isset($_COOKIE[$sitename."_language"]) )
{
	$_SESSION[DEFAULTLANGUAGECODE] = $_COOKIE[$sitename."_language"];	
	$_SESSION[DEFAULTLANGUAGEID] = $_COOKIE[$sitename."_languageId"];
	require_once ("languages/".$_COOKIE[$sitename."_language"] .".inc.php");
}
elseif(!$_SESSION[DEFAULTLANGUAGECODE] || !$_SESSION[DEFAULTLANGUAGEID])
{
	$_SESSION[DEFAULTLANGUAGECODE] = $default1;
	$_SESSION[DEFAULTLANGUAGEID] = $defLanguageId;
	@setcookie($sitename."_language", $default1, time()+3600, '/'); 
	@setcookie($sitename."_languageId", $defLanguageId, time()+3600, '/'); 
	require_once ("languages/".$default1 .".inc.php");	
}else{

	$_SESSION[DEFAULTLANGUAGECODE] = $default1;
	$_SESSION[DEFAULTLANGUAGEID] = $defLanguageId;
	@setcookie($sitename."_language", $default1, time()+3600, '/'); 
	@setcookie($sitename."_languageId", $defLanguageId, time()+3600, '/'); 
	require_once ("languages/".$default1 .".inc.php");	

}

//-------For Language-------------------------------------


//-------For Currency-------------------------------------
$default2 = "1"; // default will be dollar...
if( isset( $_GET['curr'] ) )
{
	setcookie($sitename."_currency", $_GET['curr'], time()+3600, '/'); 
}
elseif( isset($_COOKIE[$sitename."_currency"]) )
{
	$_SESSION[DEFAULTCURRENCYID] = $_COOKIE[$sitename."_currency"];
}
elseif(!$_SESSION[DEFAULTCURRENCYID])
{
	$_SESSION[DEFAULTCURRENCYID] = $default2;
	
}

//-------For Currency-------------------------------------

?> 