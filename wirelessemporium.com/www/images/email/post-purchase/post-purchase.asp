<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<%
'======================================================
'================ delete cdosend part =================
'======================================================

bTestMode = false

accountid = prepInt(request.QueryString("a"))
orderid = prepInt(request.QueryString("o"))

sql	=	"exec [getShipConfirmByOrderID] " & accountid & ", " & orderid

set objRsOrders = oConn.execute(sql)

strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\images\email\template\post-purchase.htm")
dup_strTemplate = strTemplate

lastOrderID 	= 	-1
curOrderID		=	-1
strOrderDetails	=	""
numItem			=	0

if objRsOrders.state = 1 then
	'recordset loop(by store)
	do until objRsOrders is nothing
		do until objRsOrders.eof
			curOrderID	=	objRsOrders("orderid")
			site_id		=	objRsOrders("site_id")
			if lastOrderID <> curOrderID then
				numItem = 0
				if strOrderDetails <> "" then
					strTemplate = swapAndSend(strTemplate, logoLink, headerText, custName, custNameAddress, topText, strShipAddress, strBillAddress, lastOrderID, lastAccountID, strOrderDetails, orderSubTotalAmt, telephone, strTelephone, site_id, shoprunnerid, trackLink, promoCode, discountAmt, taxAmt, shippingFeeAmt, shipMethod, shippedBy, strTracking, orderGrandTotalAmt, storeName, cdo_from, cdo_to, cdo_subject)
					strTemplate = dup_strTemplate	'refresh the template for an another e-mail.			
				end if

				strOrderDetails	= ""
				item_ids = ""
			end if
			
			'========================================== SET REPLACEMENT TEXTS ================================================
			lastAccountID 		=	objRsOrders("accountid")
			lastOrderID 		= 	curOrderID
			fname				=	objRsOrders("fname")
			lname				=	objRsOrders("lname")
			if isnull(fname) or len(fname) <= 0 then
				custName			=	"<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>Customer</p>"
			else
				custName			=	ucase(left(fname, 1)) & lcase(right(fname,len(fname)-1))
				custNameAddress		=	ucase(left(fname, 1)) & lcase(right(fname,len(fname)-1)) & " " & ucase(left(lname, 1)) & lcase(right(lname,len(lname)-1))
			end if
			email				=	objRsOrders("email")
			cdo_to				=	email
			
			storeName			=	objRsOrders("longDesc")
			topText				=	"Thank you for ordering from " & storeName & ".com! We would like to notify you that your order has been SHIPPED to:"			
			trackLink			=	objRsOrders("track_link")	'shipping track
			arr_track 			=	split(trackLink, "=")
			for each x in arr_track
				trackNum		=	x
			next				
			shipMethod			=	objRsOrders("shiptype")
			shippedBy			=	objRsOrders("shipped_by")
			promoCode			=	objRsOrders("promoCode")
			shoprunnerid		=	objRsOrders("shoprunnerid")
						
			orderSubTotalAmt	=	cdbl(objRsOrders("ordersubtotal"))
			orderGrandTotalAmt	=	cdbl(objRsOrders("ordergrandtotal"))
			taxAmt				=	cdbl(objRsOrders("orderTax"))
			discountAmt			=	cdbl(objRsOrders("discount"))
			shippingFeeAmt		=	cdbl(objRsOrders("ordershippingfee"))
			cdo_from			=	storeName & "<sales@" & Lcase(storeName) & ".com>"
			strOrder			=	"Order"
			if objRsOrders("reshipType") = 2 then strOrder = "Backorder"
			
			telephone			= objRsOrders("phone")
			
			strTelephone		= "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>T: " & telephone & "</p>"
			
			strBillAddress = formatAddress(objRsOrders("bAddress1"),objRsOrders("bAddress2"),objRsOrders("bCity"),objRsOrders("bState"),objRsOrders("bZip"))
			strShipAddress = formatAddress(objRsOrders("sAddress1"),objRsOrders("sAddress2"),objRsOrders("sCity"),objRsOrders("sState"),objRsOrders("sZip"))			
			
			select case site_id
				case 0
					headerText	=	strOrder & " #" & curOrderID & " Successfully Shipped from WirelessEmporium.com"
					logoLink	=	"http://www.wirelessemporium.com/images/global/we_logo_solo.gif"
				case 1
					headerText	=	"Your CellphoneAccents.com " & strOrder & " #" & curOrderID & " is on the way!"
					logoLink	=	"http://www.cellphoneaccents.com/images/logo.jpg"
				case 2
					headerText	=	"Shipment Status from CellularOutfitter.com " & strOrder & " #" & curOrderID
					logoLink	=	"http://www.cellularoutfitter.com/images/CellularOutfitter_STANDARD.jpg"
				case 3
					headerText	=	"Phonesale.com " & strOrder & " #" & curOrderID & " - Shipment Status"
					logoLink	=	"http://www.phonesale.com/images/phonesale.jpg"
				case 10
					headerText	=	"Your TabletMall.com " & strOrder & " #" & curOrderID & " - Shipment Status"
					logoLink	=	"http://www.tabletmall.com/images/logo.gif"					
			end select

			cdo_subject	=	headerText			
			'=================================================================================================================
			
			'============================================== ORDER DETAIL TABLE ===============================================
			if instr(email, "@") > 0 and len(custName) > 0 then
				numItem = numItem + 1
				itemid		=	objRsOrders("itemid")
				if item_ids <> "" then
					item_ids = item_ids & ","
				end if
				item_ids = item_ids & itemid
				
				qty			=	cdbl(objRsOrders("quantity"))
				price		=	cdbl(objRsOrders("price"))
				subtotal	=	cdbl(qty * price)
				itemDesc 	=	objRsOrders("itemdesc")
				partnumber	=	objRsOrders("partnumber")
				itempic		= objRsOrders("itempic")
				
				productLink = "http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(useItemDesc) & "?dzid=email_transactional_RECOMMENDATIONS_" & numItem & "L_" & itemID & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=wePostPurchase&utm_content=Confirmation&utm_promocode=WEPURCHASE"
					
				itemImgPath = server.MapPath("/productPics/thumb/" & itempic)
'				itemImgPath = "C:\inetpub\wwwroot\productpics\thumb\" & itempic
				
				set fsThumb = CreateObject("Scripting.FileSystemObject")
				if not fsThumb.FileExists(itemImgPath) then
					useImg = "/productPics/thumb/imagena.jpg"
					DoNotDisplay = 1
				else
					useImg = "/productPics/thumb/" & itempic
				end if
				
				strOrderDetails =   strOrderDetails &	"							<tr>" & vbcrlf & _
														"								<td style=""border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;"">" & partnumber & "</td>" & vbcrlf & _
														"								<td style=""border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;""><a href=""" & productLink & """ target=""_blank""><img src=""https://www.wirelessemporium.com" & useImg & """ height=""65""></a></td>" & vbcrlf & _
														"								<td style=""border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;"">" & formatnumber(qty,0) & "</td>" & vbcrlf & _
														"								<td style=""border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;""><a href=""" & productLink & """ style=""color:#000000;text-decoration:underline;"" target=""_blank"">" & itemDesc & "</a></td>" & vbcrlf & _
														"								<td style=""border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;"">" & formatcurrency(price) & "</td>" & vbcrlf & _
														"								<td style=""border-bottom:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;"">" & formatcurrency(subtotal) & "</td>" & vbcrlf & _
														"							</tr>" & vbcrlf
			end if
			'=================================================================================================================
			objRsOrders.movenext
		loop
		set objRsOrders = objRsOrders.nextrecordset
	loop
	
	if numItem > 0 then
		if strOrderDetails <> "" then
			strTemplate = swapAndSend(strTemplate, logoLink, headerText, custName, custNameAddress, topText, strShipAddress, strBillAddress, lastOrderID, lastAccountID, strOrderDetails, orderSubTotalAmt, telephone, strTelephone, site_id, shoprunnerid, trackLink, promoCode, discountAmt, taxAmt, shippingFeeAmt, shipMethod, shippedBy, strTracking, orderGrandTotalAmt, storeName, cdo_from, cdo_to, cdo_subject)
			strTemplate	= dup_strTemplate	'refresh the template for an another e-mail.
		end if
	end if
end if

function swapAndSend(strTemplate, logoLink, headerText, custName, custNameAddress, topText, strShipAddress, strBillAddress, lastOrderID, lastAccountID, strOrderDetails, orderSubTotalAmt, telephone, strTelephone, site_id, shoprunnerid, trackLink, promoCode, discountAmt, taxAmt, shippingFeeAmt, shipMethod, shippedBy, strTracking, orderGrandTotalAmt, storeName, cdo_from, cdo_to, cdo_subject)
	if bTestMode = true then
		cdo_to = "terry@wirelessemporium.com"
	end if

	strTemplate = replace(strTemplate, "[EMAIL]", cdo_to)
	strTemplate = replace(strTemplate, "[EMAIL_SIGNUP]", "http://www.wirelessemporium.com?dzid=email_transactional_EMAILSIGNUP_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Shipping&utm_content=Confirmation&utm_promocode=WEPURCHASE&utm_newsletter="&cdo_to)
	strTemplate = replace(strTemplate, "[LOGO IMG LINK]", logoLink)
	strTemplate = replace(strTemplate, "[HEADER TEXT]", headerText)
	strTemplate = replace(strTemplate, "[CUSTOMER NAME]", custName)
	strTemplate = replace(strTemplate, "[CUSTOMER NAME ADDRESS]", custNameAddress)
	strTemplate = replace(strTemplate, "[TOP TEXT]", topText)
	strTemplate = replace(strTemplate, "[SHIPPED TO]", strShipAddress)
	strTemplate = replace(strTemplate, "[BILLED TO]", strBillAddress)
	strTemplate = replace(strTemplate, "[ORDER ID]", lastOrderID)					
	strTemplate = replace(strTemplate, "[ORDER DETAILS]", strOrderDetails)
	strTemplate = replace(strTemplate, "[ORDER SUBTOTAL]", formatcurrency(orderSubTotalAmt))
	strTemplate = replace(strTemplate, "[DISCOUNT TOTAL]", formatCurrency(totalDiscount,2))
	strTemplate = replace(strTemplate, "[COPYRIGHT YEAR]", Year(Date))
	strTemplate = replace(strTemplate, "[EMAIL_PAGE]", "http://www.wirelessemporium.com/images/email/post-purchase/post-purchase?a=" & lastAccountID & "&o=" & lastOrderID & "&dzid=email_transactional_HEADER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Shipping&utm_content=Confirmation&utm_promocode=WEPURCHASE")
	
	if telephone <> "" then
		strTemplate = replace(strTemplate, "[TELEPHONE]", strTelephone)
	else 
		strTemplate = replace(strTemplate, "[TELEPHONE]", "")
	end if
	
	strTaxAmount = ""
	if taxAmt > 0 then
		strTaxAmount = strTaxAmount & "								<tr>" & vbcrlf
		strTaxAmount = strTaxAmount & "									<td style=""background:#cccccc;padding:5px 10px;text-align:left;font-family:Arial, Helvetica, sans-serif;font-size:14px;font-weight:bold;"">TAX:</td>" & vbcrlf
		strTaxAmount = strTaxAmount & "									<td style=""background:#cccccc;padding:5px 10px;text-align:right;font-family:Arial, Helvetica, sans-serif;font-size:14px;"">" & formatCurrency(taxAmt,2) & "</td>" & vbcrlf
		strTaxAmount = strTaxAmount & "								</tr>" & vbcrlf
	end if
	strTemplate = replace(strTemplate, "[TAX AMOUNT]", strTaxAmount)
	
	if site_id = 0 then
		if shoprunnerid <> "" then
			strTemplate = replace(strTemplate, "[SR]", "Thanks for using FREE 2-Day Shipping by ShopRunner! For returns, visit https://www.shoprunner.com/returns to print your pre-paid return label. Don't forget, invite a Friend to join.")
		else
			strTemplate = replace(strTemplate, "[SR]", "Next time get FREE 2-Day Shipping from Wireless Emporium and other great retailers with your ShopRunner membership. Visit https://www.wirelessemporium.com/shoprunner to sign up for a FREE 30-Day Trial")
	'						strTemplate = replace(strTemplate, "[SR]", "")
		end if
	else
		strTemplate = replace(strTemplate, "[SR]", "")
	end if
	
	arr_track 			=	split(trackLink, "=")
	for each x in arr_track
		trackNum		=	x
	next				
	
	if promoCode <> "" then
		strTemp = 	"            	<tr>" & vbcrlf & _
					"					<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>PROMO CODE:</b></td>" & vbcrlf & _
					"					<td width=""80%"" align=""left"" valign=""top"">" & promoCode & "</td>" & vbcrlf & _
					"				</tr>" & vbcrlf
					
		strTemplate = replace(strTemplate, "[PROMO CODE]", strTemp)
	else
		strTemplate = replace(strTemplate, "[PROMO CODE]", "")
	end if
	
	if discountAmt > 0 then			
		strTemp =	"(-" & formatcurrency(discountAmt) & ")"
		strTemplate = replace(strTemplate, "[DISCOUNT]", strTemp)
	else
		strTemplate = replace(strTemplate, "[DISCOUNT]", "")
	end if
	
	strTemplate = replace(strTemplate, "[BUY SAFE]", "")
	
	strTemplate = replace(strTemplate, "[TAX AMOUNT]", formatcurrency(taxAmt))
	strTemplate = replace(strTemplate, "[SHIPPING FEE]", formatcurrency(shippingFeeAmt))
	if shoprunnerid <> "" then
		strTemplate = replace(strTemplate, "[SHIPPING TYPE]", "ShopRunner, 2-Day Shipping - FREE")
		strTemplate = replace(strTemplate, "[TR TRACKING]", "")
	else
		if shipMethod = "First Class" then
			strTemplate = replace(strTemplate, "[SHIPPING TYPE]", shipMethod & " (3-10 Business Days)")
		else 
			strTemplate = replace(strTemplate, "[SHIPPING TYPE]", shipMethod)
		end if
		if shippedBy = "UPS-MI"	then
			strMemo = "(Tracking may take 24-48 hours to appear after your mail piece has shipped.)"
		else
			strMemo = ""
		end if
		
		strTracking = "<a href=""" & trackLink & """ style=""color:#006699;text-decoration:none;font-weight:bold;"" target=""_blank"">" & trackNum & "</a>"
		strTracking2 = "<a href=""" & trackLink & """ style=""color:#006699;text-decoration:underline;font-weight:normal;"" target=""_blank"">" & trackNum & "</a>"
		
		strTemplate = replace(strTemplate, "[TR TRACKING]", strTracking)
		strTemplate = replace(strTemplate, "[TR TRACKING2]", strTracking2)
	end if
	strTemplate = replace(strTemplate, "[GRAND TOTAL]", formatcurrency(orderGrandTotalAmt))
	strTemplate = replace(strTemplate, "[STORE NAME]", storeName)
	
	strUpSell = ""
	if item_ids then strUpSell = getCrossSellProducts(item_ids)
	strTemplate = replace(strTemplate, "[UPSELL]", strUpSell)

	response.write strTemplate
	
	swapKeyword = strTemplate
end function

function formatAddress(sAdd1,sAdd2,sCity,sState,sZip)
	dim strTemp
	strTemp = sAdd1
	if sAdd2 <> "" then 
		strTemp = strTemp & "<br>" & sAdd2
	end if
	strTemp = strTemp & "<br>" & sCity & ", " & sState & "&nbsp;" & sZip
	formatAddress = strTemp
end function

function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
end function


sub cdosend(strFrom, strTo, strSubject, strBody, strBcc)
	if instr(trim(strTo), "@") > 0 and instr(trim(strTo), " ") = 0 then
		set objEmail = CreateObject("CDO.Message")
		with objEmail
			.From = strFrom
			.To = strTo
			.Bcc = strBcc
			.Subject = strSubject
			.HTMLBody = strBody
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Update
			.Send
		end with
	end if
end sub

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")		
	else
		formatSEO = ""
	end if
	formatSEO = replace(formatSEO, "---", "-")
	formatSEO = replace(formatSEO, "--", "-")	
end function

function getCrossSellProducts(item_ids)
	sqlUpSell = "exec sp_ppUpsell 0, '" & item_ids & "', 0, 30"
	set objRsUpSell = oConn.execute(sqlUpSell)
	if not objRsUpSell.EOF then
		strUpSell	=	"<tr>" & vbcrlf & _
						"	<td style='background-color: #ffffff;' width='640'>" & vbcrlf & _
						"		<table width='640' cellspacing='0' cellpadding='0'>" & vbcrlf & _
						"			<tbody>" & vbcrlf & _
						"				<tr>"
			
		counter = 1
		do until objRsUpSell.EOF
			itemID		= objRsUpSell("itemID")
			itemPic		= objRsUpSell("itemPic")
			itemDesc	= objRsUpSell("itemDesc")
			useItemDesc = replace(itemDesc,"  "," ")
			itemImgPath = server.MapPath("/productpics/big/" & itemPic)
			'itemImgPath = "C:\inetpub\wwwroot\productpics\big\" & itempic
			price		= objRsUpSell("price")
			price_retail= objRsUpSell("price_retail")
			discountRate = 0.2
			discount	= price * discountRate
			promoPrice	= price - discount
			
			save_price	= cdbl(price_retail - promoPrice)
			save_percent= Round(save_price * 100 / price_retail)
			
			set fsThumb = CreateObject("Scripting.FileSystemObject")
			if not fsThumb.FileExists(itemImgPath) then
				useImg = "/productPics/big/imagena.jpg"
				DoNotDisplay = 1
			else
				useImg = "/productPics/big/" & itemPic
			end if
			
			if counter = 3 or counter = 6 then
				style = "padding:20px 10px 0 20px;"
			else 
				style = "padding:20px 0 0 20px;"
			end if
			
			product_link = "http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(useItemDesc) & "?dzid=email_transactional_RECOMMENDATIONS_" & counter & "L_" & itemID & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=wePostPurchase&utm_content=Confirmation&utm_promocode=WEPURCHASE"
			
			strUpSell 	= strUpSell &	"	<td width='210' style='" & style & "vertical-align:top;'>" & vbcrlf & _
											"		<table width='190' cellpadding='0' cellspacing='0'>" & vbcrlf & _
											"			<tbody>" & vbcrlf & _
											"				<tr>" & vbcrlf & _
											"					<td width='190' height='190'>" & vbcrlf & _
											"						<a href='" & product_link & "' target=""_blank""><img src='http://www.wirelessemporium.com" & useImg & "' style='display:block;' width='190' /></a>" & vbcrlf & _
											"					</td>" & vbcrlf & _
											"				</tr>" & vbcrlf & _
											"				<tr>" & vbcrlf & _
											"					<td width='190' height='60' style=""padding:7px 0px 7px 0px;"" valign=""top"">" & vbcrlf & _
											"						<a href='" & product_link & "' target=""_blank"" style=""font-size:12px; color:#333; font-family: Arial, sans-serif;"">" & itemDesc & "</a>" & vbcrlf & _
											"					</td>" & vbcrlf & _
											"				</tr>" & vbcrlf & _
											"				<tr>" & vbcrlf & _
											"					<td>" & vbcrlf & _
											"						<span style='color:#999;font-size:12px;color:#ccc;text-decoration:line-through;font-family:Arial, sans-serif;'>Retail Price: " & formatCurrency(price_retail) & "</span><br />" & vbcrlf & _
											"						<span style='color:#999;font-size:12px;color:#ccc;text-decoration:line-through;font-family:Arial, sans-serif;'>Our Price: " & formatCurrency(price) & "</span><br />" & vbcrlf & _
											"					</td>" & vbcrlf & _
											"				</tr>" & vbcrlf & _
											"				<tr>" & vbcrlf & _
											"					<td style='padding:10px 0 0 0;'>" & vbcrlf & _
											"						<span style='color:#000;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;'>Promo Price:</span> <span style='color:#ff6633;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;'>" & formatCurrency(promoPrice) & "</span><br />" & vbcrlf & _
											"						<span style='color:#000;font-size:14px;font-family:Arial, sans-serif;'>You Save:</span> <span style='margin:10px 0 0 0;color:#000;font-size:14px;font-family:Arial, sans-serif;font-weight:bold;'>" & round(save_percent) & "% (" & formatCurrency(save_price) & ")</span>" & vbcrlf & _
											"					</td>" & vbcrlf & _
											"				</tr>" & vbcrlf & _
											"				<tr>" & vbcrlf & _
											"					<td style='padding:10px 0 0 0;'>" & vbcrlf & _
											"						<table width='160' border='0' cellpadding='0' cellspacing='0'>" & vbcrlf & _
											"							<tbody>" & vbcrlf & _
											"								<tr>" & vbcrlf & _
											"									<td align='center' style='background-color:#ff6633;border-radius:16px;padding:7px 0;'>" & vbcrlf & _
											"										<a href='" & product_link & "' style='color:#fff;font-weight:bold;font-size:14px;font-family:Arial, sans-serif;text-decoration:none;' target=""_blank"">SHOP NOW &raquo;</a>" & vbcrlf & _
											"									</td>" & vbcrlf & _
											"								</tr>" & vbcrlf & _
											"							</tbody>" & vbcrlf & _
											"						</table>" & vbcrlf & _
											"					</td>" & vbcrlf & _
											"				</tr>" & vbcrlf & _
											"			</tbody>" & vbcrlf & _
											"		</table>" & vbcrlf & _
											"	</td>"
			
			if counter = 3 then
				strUpSell = strUpSell & "						</tr>" & vbcrlf & _
							"					</tbody>" & vbcrlf & _
							"				</table>" & vbcrlf & _
							"				<br />" & vbcrlf & _
							"			</td>" & vbcrlf & _
							"		</tr>" & vbcrlf & _
							"		<tr>" & vbcrlf & _
							"		  <td style='background-color: #cccccc;' width='640' height='1'></td>" & vbcrlf & _
							"		</tr>" & vbcrlf & _
							"		<tr>" & vbcrlf & _
							"			<td style='background-color: #ffffff;' width='640'>" & vbcrlf & _
							"				<table width='640' cellspacing='0' cellpadding='0'>" & vbcrlf & _
							"					<tbody>" & vbcrlf & _
							"						<tr>"
				counter = counter + 1
			elseif counter = 6 then
				exit do
			else
				counter = counter + 1
			end if
			objRsUpSell.movenext
		loop
		
		strUpSell = strUpSell & "</tr>" & vbcrlf & _
					"</table>"
	end if

	getCrossSellProducts = strUpSell
end function

Function isValidEmail(emailAddress)
	'Declare variables 
	Dim ValidEmail, emailParts, iLoopCounter, emailChar, acceptableChars, last2Chars 
	ValidEmail = True 'set the default result to True
	'acceptableChars are the characters that we will allow in our email
	acceptableChars="abcdefghijklmnopqrstuvwxyz.-_@"
	'use the Split function to create an array with the @ as the separator
	'so if your email was test@tester.com the email would be split into an array
	'with the first array element holding "test" and the second "tester.com"
	emailParts = Split(emailAddress, "@")
	'check to make sure that there is only 1 @ and that there are 2 parts
	'remember arrays are zero based
	'Using the UBound function will return the highest element in the array
	'So if it's a valid email the UBound function will return 1, i.e. 0 start
	If UBound(emailParts) <> 1 Then
	ValidEmail = false
	Else 
	'Check the length of each part of the email address
	'first part can be just one character, 2nd part must be atleast 4
	If Len(emailParts(0))<1 OR Len(emailParts(1))<4 Then
	ValidEmail = false
	End If
	'check first character on the left part isn't a "." using Left function
	If Left(emailParts(0), 1)="." Then
	ValidEmail = false
	End If 
	'check that there is a . in the second part of the email address - .com
	If InStr(emailParts(1), ".") <= 0 Then
	ValidEmail = false 
	End If 
	'We know there's a . now make sure there are 2 characters after the . for valid email
	last2Chars=Right(emailParts(1),2)
	If InStr(last2chars,".") Then
	ValidEmail = false
	End If
	'check that there shouldn't be a _ in the second part of the email address 
	If InStr(emailParts(1), "_") >0 Then
	ValidEmail = false 
	End If 
	End If
	'loop through each character of email
	For iLoopCounter = 1 to Len(emailAddress)
	'Use Lcase & Mid functions, Mid function used to return each individual character
	'in the email, and then Lcase converts it into lowercase
	emailChar = Lcase(Mid(emailAddress, iLoopCounter, 1)) 
	'Check if the emailAddress characters are acceptable
	If InStr(acceptableChars, emailChar) = 0 and Not IsNumeric(emailChar) Then
	ValidEmail = false
	End if
	Next
	'check if there is 2 . in a row
	If InStr(emailAddress, "..") > 0 Then
	ValidEmail=false
	End If
	'check if there is @. in a row
	If InStr(emailAddress, "@.") > 0 Then
	ValidEmail=false
	End If 
	isValidEmail=ValidEmail
End function
%>
