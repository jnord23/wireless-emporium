<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>15&#37; off accessories for your phone!</title>
<style type="text/css">
.ReadMsgBody {
	width: 100%;
}
.ExternalClass {
	width: 100%;
}
 @media only screen and (max-width: 479px) {
 div[class="mobile-portrait"] {
 display: block !important;
 max-height: inherit !important;
 overflow: visible !important;
 width: auto !important;
}
 div[class="desktop-version"] {
 display: none !important;
}
}
</style>
</head>
<!--#include virtual="/Framework/Utility/Static.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
set oConn = Server.CreateObject("ADODB.Connection")
oConn.Open Session("ConnectionString")

campaign_id = "welcome1"
promo_code = "WELCOME15"
curYear = year(date)
expDate = date+7
%>
<body style="margin: 0; padding: 0; width: 100%; -webkit-text-size-adjust: none;">

<!-- Containter -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background-color: #ebebeb;" width="100%" align="center"><!-- Desktop Content -->
      
      <div class="desktop-version">
        <table width="640" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 150%; mso-line-height-rule: exactly;" width="640" height="60" align="left" valign="middle">Welcome to WirelessEmporium.com. Get 15&#37; off your next order!<br />
              Having trouble viewing this email? <a style="color: #006699;" href="http://www.wirelessemporium.com/images/email/blast/template/welcome/blast/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Click here</a>.</td>
          </tr>
          <tr>
            <td style="background-color: #333333; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 13px;" width="640" height="30" align="center" valign="middle">Save <b style="color: #ff9900;">15&#37;</b> On Accesories - Use Coupon Code <b style="color: #ff9900;"><%=promo_code%></b> @ Checkout! Expires <%=expDate%></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff; font-size: 18px;" width="640" height="80" align="center" valign="middle"><a style="color: #333333; text-decoration: none;" href="http://www.wirelessemporium.com?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/we-header.gif" width="620" height="59" alt="WirelessEmporium.com - Fast, FREE Shipping | 90-Day Returns" border="0" /></a></td>
          </tr>
          <tr>
            <td style="background-color: #cccccc;" width="640" height="1"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff; font-family: Arial, Helvetica, sans-serif;" width="640" align="center"><br />
              <a style="color: #333333; text-decoration: none;" href="http://www.wirelessemporium.com/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"> <span style="font-size: 64px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;">GET 15&#37; OFF</span><br />
              <!--<span style="font-size: 24px; line-height: 175%; mso-line-height-rule: exactly;">Enter code <b><%=promo_code%></b> at checkout.</span>-->
              <span style="font-size: 24px; line-height: 30px; mso-line-height-rule: exactly;">Click on any of the links below to automatically <br />apply your 15% off discount on your next order.</span>
              </a><br />
              <br />
              <a href="http://www.wirelessemporium.com/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/hero-desktop-2x.jpg" width="310" height="120" alt="Save 15&#37; on accessories!" border="0" /></a><br />
              <br />
              <table width="300" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="background-color: #ff6600; border-radius: 22px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 18px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="300" height="44" align="center" valign="middle"><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">SHOP NOW &raquo;</a></td>
                </tr>
              </table>
              <p>&nbsp;</p></td>
          </tr>
          <tr>
            <td style="background-color: #ff9900;" width="640" height="100"><table width="640" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 100%;" width="79" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/phone-covers-faceplates?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-covers.gif" width="65" height="50" alt="Covers &amp; Skins" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/phone-covers-faceplates?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Covers &amp; Skins</a></td>
                  <td style="background-color: #ffce85;" width="1" height="100"></td>
                  <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 100%;" width="79" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/phone-pouches-carrying-cases?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-cases.gif" width="65" height="50" alt="Covers &amp; Skins" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/phone-pouches-carrying-cases?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Cases &amp; Pouches</a></td>
                  <td style="background-color: #ffce85;" width="1" height="100"></td>
                  <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 100%;" width="79" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/phone-batteries?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-batteries.gif" width="65" height="50" alt="Cell Phone Batteries" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/phone-batteries?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Cell Phone Batteries</a></td>
                  <td style="background-color: #ffce85;" width="1" height="100"></td>
                  <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 100%;" width="79" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/phone-chargers-cables?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-chargers.gif" width="65" height="50" alt="Chargers &amp; Cables" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/phone-chargers-cables?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Chargers &amp; Cables</a></td>
                  <td style="background-color: #ffce85;" width="1" height="100"></td>
                  <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 100%;" width="79" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/phone-screen-protectors-skins?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-screens.gif" width="65" height="50" alt="Screen Guards" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/phone-screen-protectors-skins?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Screen Guards</a></td>
                  <td style="background-color: #ffce85;" width="1" height="100"></td>
                  <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 100%;" width="79" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/phone-holsters-holders-mounts?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-mounts.gif" width="65" height="50" alt="Holsters &amp; Mounts" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/phone-holsters-holders-mounts?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Holsters &amp; Mounts</a></td>
                  <td style="background-color: #ffce85;" width="1" height="100"></td>
                  <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 100%;" width="79" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/bluetooth-headsets-handsfree?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-bluetooth.gif" width="65" height="50" alt="Handsfree Devices" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/bluetooth-headsets-handsfree?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Handsfree Devices</a></td>
                  <td style="background-color: #ffce85;" width="1" height="100"></td>
                  <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 100%;" width="80" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/phones-tablets?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-phones.gif" width="65" height="50" alt="Unlocked Phones" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/phones-tablets?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Unlocked Phones</a></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;" width="640"><br />
              <table width="640" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="430" valign="top"><table width="430" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="10"></td>
                        <td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px;" width="400" align="left"><p style="font-size: 18px; font-weight: bold;">Welcome to WirelessEmporium.com!</p>
                          <p style="line-height: 150%; mso-line-height-rule: exactly;">We are excited that you decided to join our exclusive email membership club. You're now set to get access to private sales, specially-crafted deals and the latest in interesting cell phone news.</p>
                          <p style="line-height: 150%; mso-line-height-rule: exactly;">We're 100&#37; dedicated to ensuring that you have the best cell phone accessory shopping experience. We offer fast, free shipping on every order along with 90-day no-hassle returns and a 1-year warranty.</p>
                          <p style="line-height: 150%; mso-line-height-rule: exactly;">We work hard to ensure we have the largest selection at the lowest possible prices. This is why we've been the #1 name in cell phone accessories since 2001.</p>
                          <p style="line-height: 150%; mso-line-height-rule: exactly;">We look forward to seeing you soon!</p>
                          <br />
                          <table width="400" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;" width="120" align="center"><a style="color: #333333; text-decoration: none;" href="http://www.wirelessemporium.com?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-value-shipping.gif" width="60" height="60" alt="Fast, FREE Shipping" border="0" /><br />
                                Fast, FREE Shipping</a></td>
                              <td width="9"></td>
                              <td style="background-color: #cccccc;" width="1"></td>
                              <td width="10"></td>
                              <td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;" width="120" align="center"><a style="color: #333333; text-decoration: none;" href="http://www.wirelessemporium.com?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-value-returns.gif" width="60" height="60" alt="90-Day Return Policy" border="0" /><br />
                                90-Day Return Policy</a></td>
                              <td width="9"></td>
                              <td style="background-color: #cccccc;" width="1"></td>
                              <td width="10"></td>
                              <td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;" width="120" align="center"><a style="color: #333333; text-decoration: none;" href="http://www.wirelessemporium.com?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-value-service.gif" width="60" height="60" alt="World Class Customer Care" border="0" /><br />
                                World Class Customer Care</a></td>
                            </tr>
                          </table></td>
                        <td width="20"></td>
                      </tr>
                    </table></td>
                  <td style="color: #333333; background-color: #ebebeb; font-family: Arial, Helvetica, sans-serif;" width="200" align="center" valign="top"><br />
                    <b style="font-size: 16px; font-weight: bold;">We Recommend:</b><br />
                    <br />
					<%
					strProducts = ""
					sql = "sp_topSellingProducts 0, 20, 10, 'UNI', 1"
					set rsRecommend = oConn.execute(sql)
					nCnt = 0
					do until rsRecommend.eof
						nCnt = nCnt + 1
						rsItemid = rsRecommend("itemid")
						rsItemDesc = rsRecommend("itemdesc")
						rsItempic = "http://www.wirelessemporium.com/productpics/thumb/" & rsRecommend("itempic")
						rsPrice = formatcurrency(rsRecommend("price_our"), 2)
						productLink = "http://www.wirelessemporium.com/p-" & rsItemid & "-" & formatSEO(rsItemDesc) & "?utm_source=streamsend&utm_medium=email2&utm_campaign=" & campaign_id & "&utm_promocode=" & promo_code
						strProducts = 	strProducts & 	"  <table width=""180"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf & _
														"	  <tr>" & vbcrlf & _
														"		<td style=""font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;"" width=""180"" align=""center"" valign=""top""><a href=""" & productLink & """ target=""_blank""><img style=""display: block; border: 2px solid #999999;"" src=""" & rsItempic & """ width=""75"" height=""75"" /></a><br />" & vbcrlf & _
														"		  <a style=""color: #333333; text-decoration: none;"" href=""" & productLink & """>" & rsItemDesc & "</a><br />" & vbcrlf & _
														"		  <br />" & vbcrlf & _
														"		  <a style=""color: #333333; font-size: 14px; text-decoration: none;"" href=""" & productLink & """ target=""_blank"">" & rsPrice & "</a><br />" & vbcrlf & _
														"		  <table width=""160"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf & _
														"			<tr>" & vbcrlf & _
														"			  <td style=""background-color: #ff6600; border-radius: 17px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;"" width=""160"" height=""34"" align=""center"" valign=""middle""><a style=""color: #ffffff; text-decoration: none;"" href=""" & productLink & """ target=""_blank"">SHOP NOW &raquo;</a></td>" & vbcrlf & _
														"			</tr>" & vbcrlf & _
														"		  </table>" & vbcrlf & _
														"		</td>" & vbcrlf & _
														"	  </tr>" & vbcrlf & _
														"	</table>" & vbcrlf & _
														"	<br />"
						if nCnt >= 2 then exit do
						rsRecommend.movenext
					loop
					response.write strProducts
					%>
				  </td>
                  <td width="10"></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;" width="640" height="20"></td>
          </tr>
        </table>
        
        <!-- Footer Table -->
        
        <table width="640" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="640" height="80" align="center" valign="middle"><img src="http://www.wirelessemporium.com/images/email/blast/welcome/images/social-zone.gif" alt="" width="149" height="23" border="0" usemap="#Map" style="display: block;" /></td>
          </tr>
          <tr>
            <td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 150%; mso-line-height-rule: exactly; margin: 0;" align="left">Offer excludes Bluetooth headsets, custom cases and select OEM products.<br />
              <br />
              This email correspondence has been sent to you as you have indicated you would like to receive special, exclusive offers from WirelessEmporium.com. If this message has been sent in error, or you would no longer like to receive any more periodic offers from WirelessEmporium.com, you may <a style="color: #006699; font-weight: bold;" href="http://www.wirelessemporium.com/unsubscribe.asp?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">unsubscribe</a>.<br />
              <br />
              <a style="color: #006699;" href="http://www.wirelessemporium.com/privacy?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Privacy Policy</a> | <a style="color: #006699;" href="http://www.wirelessemporium.com/termsofuse.asp?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Terms &amp; Conditions</a> | <a style="color: #006699;" href="http://www.wirelessemporium.com/contactus.asp?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Contact Us</a><br />
              <br />
              &copy;2002-<%=curYear%> WirelessEmporium.com<br />
              <br /></td>
          </tr>
        </table>
      </div>
      
      <!-- End Desktop Content --> 
      
      <!-- Mobile Content --> 
      
	<![if !mso]>
      
      <div class="mobile-portrait" style="display: none; max-height: 0; overflow: hidden; width: 0;">
        <table style="font-family: Arial, Helvetica, sans-serif;" width="320" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="color: #333333; font-size: 11px; line-height: 150%; mso-line-height-rule: exactly;" width="320" height="50" align="left">Welcome to WirelessEmporium - Get 15&#37; off your next order!<br />
              Can't view this email? <a style="color: #006699;" href="http://www.wirelessemporium.com/images/email/blast/template/welcome/we-welcome-email.asp?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Click here</a>.</td>
          </tr>
          <tr>
            <td style="background-color: #ff6600; font-size: 16px;" width="320" height="36" align="center"><a style="color: #ffffff; font-weight: bold; text-decoration: none;" href="http://www.wirelessemporium.com/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Get 15&#37; Off Your Next Order!</a></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;" width="320" height="58" align="center"><a href="http://www.wirelessemporium.com/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/we-logo-2x.gif" width="300" height="38" alt="WirelessEmporium.com" border="0" /></a></td>
          </tr>
          <tr>
            <td style="background-color: #cccccc;" width="320" height="1"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff; color: #333333; font-size: 11px; letter-spacing: 2px;" width="320" height="36" align="center"><a style="color: #333333; text-decoration: none;" href="http://www.wirelessemporium.com/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">FAST, FREE SHIPPING. 90-DAY RETURNS.</a></td>
          </tr>
          <tr>
            <td style="background-color: #cccccc;" width="320" height="1"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff; color: #333333;" width="320" align="center" valign="middle"><br />
              <a style="color: #333333; text-decoration: none;" href="http://www.wirelessemporium.com/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"> <span style="font-size: 44px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;">GET 15&#37; OFF</span><br />
              <!--<span style="font-size: 16px; line-height: 175%; mso-line-height-rule: exactly;">Enter code <b><%=promo_code%></b> at checkout.</span>-->
              <span style="font-size: 16px; line-height: 24px; mso-line-height-rule: exactly;"> &nbsp; Click on any of the links below to automatically apply your 15% off discount on your next order. &nbsp; </span>
              </a><br />
              <br />
              <a href="http://www.wirelessemporium.com/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/hero-2x.jpg" width="120" height="120" alt="Save 15&#37; on accessories!" border="0" /></a><br />
              <br />
              <table width="300" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="background-color: #ff6600; border-radius: 17px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="300" height="34" align="center" valign="middle"><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">SHOP NOW &raquo;</a></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;" width="320" height="20"></td>
          </tr>
          <tr>
            <td style="background-color: #cccccc;" width="320" height="1"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff; color: #333333;" width="320" height="44" align="center" valign="middle">
              <table width="320" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="60" align="center" valign="middle"><a style="color: #006699; text-decoration: none;" href="http://www.wirelessemporium.com/phone-covers-faceplates?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-cat-covers-2x.gif" width="34" height="34" alt="Covers &amp; Skins" border="0" /></a></td>
                  <td style="color: #006699; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="220" align="left" valign="middle"><a style="color: #006699; text-decoration: none;" href="http://www.wirelessemporium.com/phone-covers-faceplates?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Covers &amp; Skins</a></td>
                  <td style="color: #ff6600; font-family: Arial, Helvetica, sans-serif; font-size: 28px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="40" align="center" valign="middle"><a style="color: #ff6600; text-decoration: none;" href="http://www.wirelessemporium.com/phone-covers-faceplates?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">&rsaquo;</a></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td style="background-color: #cccccc;" width="320" height="1"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff; color: #333333;" width="320" height="44" align="center" valign="middle"><table width="320" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="60" align="center" valign="middle"><a style="color: #006699; text-decoration: none;" href="http://www.wirelessemporium.com/phone-chargers-cables?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-cat-chargers-2x.gif" width="34" height="34" alt="Chargers &amp; Cables" border="0" /></a></td>
                  <td style="color: #006699; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="220" align="left" valign="middle"><a style="color: #006699; text-decoration: none;" href="http://www.wirelessemporium.com/phone-chargers-cables?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Chargers &amp; Cables</a></td>
                  <td style="color: #ff6600; font-family: Arial, Helvetica, sans-serif; font-size: 28px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="40" align="center" valign="middle"><a style="color: #ff6600; text-decoration: none;" href="http://www.wirelessemporium.com/phone-chargers-cables?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">&rsaquo;</a></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td style="background-color: #cccccc;" width="320" height="1"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff; color: #333333;" width="320" height="44" align="center" valign="middle"><table width="320" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="60" align="center" valign="middle"><a style="text-decoration: none;" href="http://www.wirelessemporium.com/phone-pouches-carrying-cases?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-cat-cases-2x.gif" width="34" height="34" alt="Cases &amp; Pouches" border="0" /></a></td>
                  <td style="color: #006699; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="220" align="left" valign="middle"><a style="text-decoration: none;" href="http://www.wirelessemporium.com/phone-pouches-carrying-cases?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Cases &amp; Pouches</a></td>
                  <td style="color: #ff6600; font-family: Arial, Helvetica, sans-serif; font-size: 28px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="40" align="center" valign="middle"><a style="text-decoration: none;" href="http://www.wirelessemporium.com/phone-pouches-carrying-cases?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">&rsaquo;</a></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td style="background-color: #cccccc;" width="320" height="1"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff; color: #333333;" width="320" height="44" align="center" valign="middle"><table width="320" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="60" align="center" valign="middle"><a style="color: #006699; text-decoration: none;" href="http://www.wirelessemporium.com/phone-batteries?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-cat-batteries-2x.gif" width="34" height="34" alt="Cell Phone Batteries" border="0" /></a></td>
                  <td style="color: #006699; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="220" align="left" valign="middle"><a style="color: #006699; text-decoration: none;" href="http://www.wirelessemporium.com/phone-batteries?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Cell Phone Batteries</a></td>
                  <td style="color: #ff6600; font-family: Arial, Helvetica, sans-serif; font-size: 28px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="40" align="center" valign="middle"><a style="color: #ff6600; text-decoration: none;" href="http://www.wirelessemporium.com/phone-batteries?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">&rsaquo;</a></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td style="background-color: #cccccc;" width="320" height="1"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff; color: #333333;" width="320" height="44" align="center" valign="middle"><table width="320" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="60" align="center" valign="middle"><a style="color: #006699; text-decoration: none;" href="http://www.wirelessemporium.com/bluetooth-headsets-handsfree?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-cat-handsfree-2x.gif" width="34" height="34" alt="Hands-Free &amp; Bluetooth" border="0" /></a></td>
                  <td style="color: #006699; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="220" align="left" valign="middle"><a style="color: #006699; text-decoration: none;" href="http://www.wirelessemporium.com/bluetooth-headsets-handsfree?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Hands-Free &amp; Bluetooth</a></td>
                  <td style="color: #ff6600; font-family: Arial, Helvetica, sans-serif; font-size: 28px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="40" align="center" valign="middle"><a style="color: #ff6600; text-decoration: none;" href="http://www.wirelessemporium.com/bluetooth-headsets-handsfree?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">&rsaquo;</a></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td style="background-color: #cccccc;" width="320" height="1"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff; color: #333333;" width="320" height="44" align="center" valign="middle"><table width="320" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="60" align="center" valign="middle"><a style="color: #006699; text-decoration: none;" href="http://www.wirelessemporium.com/phone-screen-protectors-skins?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-cat-screens-2x.gif" width="34" height="34" alt="Screen Guards" border="0" /></a></td>
                  <td style="color: #006699; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="220" align="left" valign="middle"><a style="color: #006699; text-decoration: none;" href="http://www.wirelessemporium.com/phone-screen-protectors-skins?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Screen Guards</a></td>
                  <td style="color: #ff6600; font-family: Arial, Helvetica, sans-serif; font-size: 28px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="40" align="center" valign="middle"><a style="color: #ff6600; text-decoration: none;" href="http://www.wirelessemporium.com/phone-screen-protectors-skins?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">&rsaquo;</a></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td style="background-color: #cccccc;" width="320" height="1"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;" width="320" height="20"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;" width="320"><table width="320" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="10"></td>
                  <td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 150%; mso-line-height-rule: exactly;" width="300" align="left"><p>We are excited that you decided to join our exclusive email membership club. You're now set to get access to private sales, specially-crafted deals and the latest in interesting cell phone news.</p>
                    <p>We're 100&#37; dedicated to ensuring that you have the best cell phone accessory shopping experience. We offer fast, free shipping on every order along with 90-day no-hassle returns and a 1-year warranty.</p>
                    <p>We look forward to seeing you soon!</p></td>
                  <td width="10"></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;" width="320" height="20"></td>
          </tr>
          <tr>
            <td style="background-color: #cccccc;" width="320" height="1"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;" width="320" height="20"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;" width="320"><table width="320" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="10"></td>
                  <td style="color: #333333; font-weight: bold;" width="75" align="center" valign="top"><a href="http://www.wirelessemporium.com/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="diplay: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-15-off-2x.gif" width="75" height="75" alt="Get 15&#37; Off" border="0" /></a></td>
                  <td width="10"></td>
                  <td style="font-family: Arial, Helvetica, sans-serif;" align="left" valign="top"><a style="color: #ff6600; font-size: 16px; font-weight: bold; text-decoration: none;" href="http://www.wirelessemporium.com/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Get 15&#37; off your next order!</a><br />
                    <a style="color: #333333; font-size: 12px; line-height: 2.0; mso-line-height-rule: exactly; text-decoration: none;" href="http://www.wirelessemporium.com/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">Enter code <b><%=promo_code%></b> at checkout.</a><br />
                    <br />
                    <table width="140" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="background-color: #ff6600; border-radius: 17px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 100%; mso-line-height-rule: exactly;" width="140" height="34" align="center" valign="middle"><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank">SHOP NOW &raquo;</a></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;" width="320" height="20"></td>
          </tr>
          <tr>
            <td style="background-color: #cccccc;" width="320" height="1"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;" width="320" height="20"></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;" width="320"><table width="320" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="10"></td>
                  <td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 16px; line-height: 150%; mso-line-height-rule: exactly;" width="300" align="center"><p><b>Connect with us:</b></p>
                    <table width="300" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="75" align="center"><a href="https://www.facebook.com/WirelessEmporium?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-facebook-2x.gif" width="16" height="34" alt="Facebook" border="0" /></a></td>
                        <td width="75" align="center"><a href="https://twitter.com/wirelessemp/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-twitter-2x.gif" width="42" height="34" alt="Twitter" border="0" /></a></td>
                        <td width="75" align="center"><a href="https://plus.google.com/113217319312103488691?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-google-2x.gif" width="39" height="34" alt="Google+" border="0" /></a></td>
                        <td width="75" align="center"><a href="http://www.wirelessemporium.com/blog/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank"><img style="display: block" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-blog-2x.gif" width="34" height="34" alt="Blog" border="0" /></a></td>
                      </tr>
                    </table></td>
                  <td width="10"></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;" width="320" height="20"></td>
          </tr>
          <tr>
            <td style="background-color: #cccccc;" width="320" height="1"></td>
          </tr>
          <tr>
            <td width="320"><table width="320" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="background-color: #f7f7f7;" width="10"></td>
                  <td style="background-color: #f7f7f7; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 1.5; mso-line-height-rule: exactly; margin: 0;" width="300" align="center">
                  	<a style="color: #006699; font-weight: bold;" href="#?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><br />Unsubscribe</a><br />
                    <br />
                    <a style="color: #006699; font-weight: bold;" href="http://www.wirelessemporium.com/privacy?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Privacy Policy</a>
                    &nbsp;&nbsp;|&nbsp;&nbsp;
                    <a style="color: #006699; font-weight: bold;" href="http://www.wirelessemporium.com/termsofuse?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Terms</a>
                    &nbsp;&nbsp;|&nbsp;&nbsp;
                    <a style="color: #006699; font-weight: bold;" href="http://www.wirelessemporium.com/contactus?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Contact Us</a>
					</td>
                  <td style="background-color: #f7f7f7;" width="10"></td>
                </tr>
                <tr>
                  <td style="background-color: #f7f7f7;" width="10"></td>
                  <td style="background-color: #f7f7f7; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 1.5; mso-line-height-rule: exactly; margin: 0;" width="300" align="center">
					<br />Offer excludes Bluetooth headsets, custom cases, phones and select OEM products.<br />
					<br />
					This email correspondence has been sent to you as you have indicated you would like to receive special, exclusive offers from WirelessEmporium.com. If this message has been sent in error, or you would no longer like to receive any more periodic offers from WirelessEmporium.com, you may <a style="color: #006699; font-weight: bold;" href="http://www.wirelessemporium.com/unsubscribe.asp?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">unsubscribe</a>.<br />
				  </td>
                  <td style="background-color: #f7f7f7;" width="10"></td>
                </tr>
                <tr>
                  <td style="background-color: #f7f7f7;" width="10"></td>
                  <td style="background-color: #f7f7f7; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 1.5; mso-line-height-rule: exactly; margin: 0;" width="300" align="center">
                    <br />&copy;2002-<%=curYear%> WirelessEmporium.com<br /><br />
                  </td>
                  <td style="background-color: #f7f7f7;" width="10"></td>
				</tr>
              </table></td>
          </tr>
        </table>
      </div>
      
      <![endif]>

      <!-- End Mobile Content --></td>
  </tr>
</table>
<!-- End Container -->

<map name="Map" id="Map">
  <area shape="rect" coords="0,0,11,23" href="http://www.facebook.com/WirelessEmporium/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank" alt="Facebook" title="Facebook" />
  <area shape="rect" coords="33,0,66,23" href="http://twitter.com/#!/wirelessemp/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank" alt="Twitter" title="Twitter" />
  <area shape="rect" coords="82,0,105,23" href="https://plus.google.com/113217319312103488691?rel=author&utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank" alt="Google+" title="Google+" />
  <area shape="rect" coords="126,0,149,23" href="http://www.wirelessemporium.com/blog/?utm_source=streamsend&utm_medium=email2&utm_campaign=<%=campaign_id%>&utm_promocode=<%=promo_code%>" target="_blank" alt="Blog" title="Blog" />
</map>
</body>
</html>
