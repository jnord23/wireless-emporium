<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Save {{percentage_off}}&#37; On {{model_name}} Accessories!</title>
<style type="text/css">
.ReadMsgBody {
	width: 100%;
}
.ExternalClass {
	width: 100%;
}
</style>
</head>

<body style="margin: 0; padding: 0; width: 100%;">

<!-- Containter -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background-color: #ebebeb;" width="100%" align="center"><!-- Content -->
      
      <table style="margin: 0;" width="640" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px;" width="640" height="50" align="center" valign="middle">Having trouble viewing this email? <a style="color: #006699;" href="{{browser_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Click here</a>.</td>
        </tr>
        <tr>
          <td style="background-color: #333333; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px;" width="640" height="30" align="center" valign="middle">Save <b style="color: #ff9900;">{{percentage_off}}&#37;</b> On {{model_name}} Accesories - Use Coupon Code <b style="color: #ff9900;">{{promo_code}}</b> @ Checkout! Expires {{expiration_date}}</td>
        </tr>
        <tr>
          <td style="background-color: #ffffff;" width="640" height="80" align="center" valign="middle"><a href="http://www.wirelessemporium.com?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/template/we-header.gif" width="620" height="59" alt="WirelessEmporium.com - Fast, FREE Shipping | 90-Day Returns" border="0" /></a></td>
        </tr>
        <tr>
          <td style="background-color: #2980b9;" width="640" height="300"><table width="640" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="240" height="300"><a href="{{model_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{model_image}}" width="240" height="300" alt="{{model_name}} Accessories On Sale!" border="0" /></a></td>
                <td width="400" height="300" align="left" valign="middle"><table width="380" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style="background-color: #2c3e50;" width="380" height="240" align="center" valign="middle"><span style="color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 18px; line-height: 1.0; margin: 0; padding: 0; text-transform: uppercase;">{{model_name}} Accessories</span><br />
                        <hr style="background-color: #ffffff; border: 0; height: 2px; margin: 0; width: 340px;" />
                        <span style="color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 36px; font-weight: bold; line-height: 1.5; margin: 0; padding: 0;">{{percentage_off}}&#37; OFF SALE</span><br />
                        <span style="color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 22px; line-height: 1.0; margin: 0; padding: 0;">ENTER CODE: {{promo_code}}</span><br />
                        <br />
                        <a style="background-color: #e67e22; border: 2px solid #ffffff; color: #ffffff; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 18px; line-height: 1.0; margin: 0; padding: 15px 0; text-decoration: none; width: 260px;" href="{{model_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">SHOP THE SALE</a></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td style="background-color: #ff9900;" width="640" height="100"><table width="640" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="100" align="center" valign="top"><a href="{{bmcd_link;typeID=3}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block; margin-bottom: 5px; padding-top: 5px;" src="http://www.wirelessemporium.com/images/email/blast/template/ico-covers.gif" width="65" height="50" alt="Covers &amp; Skins" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=3}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Covers &amp; Skins</a></td>
                <td style="background-color: #ffce85;" width="1" height="100"></td>
                <td style="color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="100" align="center" valign="top"><a href="{{bmcd_link;typeID=7}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block; margin-bottom: 5px; padding-top: 5px;" src="http://www.wirelessemporium.com/images/email/blast/template/ico-cases.gif" width="65" height="50" alt="Covers &amp; Skins" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=7}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Cases &amp; Pouches</a></td>
                <td style="background-color: #ffce85;" width="1" height="100"></td>
                <td style="color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="100" align="center" valign="top"><a href="{{bmcd_link;typeID=1}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block; margin-bottom: 5px; padding-top: 5px;" src="http://www.wirelessemporium.com/images/email/blast/template/ico-batteries.gif" width="65" height="50" alt="Cell Phone Batteries" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=1}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Cell Phone Batteries</a></td>
                <td style="background-color: #ffce85;" width="1" height="100"></td>
                <td style="color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="100" align="center" valign="top"><a href="{{bmcd_link;typeID=2}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block; margin-bottom: 5px; padding-top: 5px;" src="http://www.wirelessemporium.com/images/email/blast/template/ico-chargers.gif" width="65" height="50" alt="Chargers &amp; Cables" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=2}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Chargers &amp; Cables</a></td>
                <td style="background-color: #ffce85;" width="1" height="100"></td>
                <td style="color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="100" align="center" valign="top"><a href="{{bmcd_link;typeID=18}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block; margin-bottom: 5px; padding-top: 5px;" src="http://www.wirelessemporium.com/images/email/blast/template/ico-screens.gif" width="65" height="50" alt="Screen Guards" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=18}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Screen Guards</a></td>
                <td style="background-color: #ffce85;" width="1" height="100"></td>
                <td style="color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="100" align="center" valign="top"><a href="{{bmcd_link;typeID=6}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block; margin-bottom: 5px; padding-top: 5px;" src="http://www.wirelessemporium.com/images/email/blast/template/ico-mounts.gif" width="65" height="50" alt="Holsters &amp; Mounts" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=6}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Holsters &amp; Mounts</a></td>
                <td style="background-color: #ffce85;" width="1" height="100"></td>
                <td style="color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="100" align="center" valign="top"><a href="{{bmcd_link;typeID=5}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block; margin-bottom: 5px; padding-top: 5px;" src="http://www.wirelessemporium.com/images/email/blast/template/ico-bluetooth.gif" width="65" height="50" alt="Handsfree Devices" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=5}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Handsfree Devices</a></td>
                <td style="background-color: #ffce85;" width="1" height="100"></td>
                <td style="color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="80" height="100" align="center" valign="top"><a href="http://www.wirelessemporium.com/cell-phones-tablets.asp?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block; margin-bottom: 5px; padding-top: 5px;" src="http://www.wirelessemporium.com/images/email/blast/template/ico-phones.gif" width="65" height="50" alt="Unlocked Phones" border="0" /></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/cell-phones-tablets.asp?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Unlocked Phones</a></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td style="background-color: #ffffff;" width="640"><br />
            <table width="640" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><table width="320" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10"></td>
                      <td width="100" valign="top"><a href="{{rec_item_1_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{rec_item_1_image}}" width="100" height="100" alt="{{rec_item_1_desc}}" border="0" /></a></td>
                      <td width="10"></td>
                      <td width="190" align="left" valign="top"><p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 5px;"><a style="color: #333333; text-decoration: none;" href="{{rec_item_1_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_1_desc}}</a></p>
                        <p style="color: #c0392b; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 5px;">{{rec_item_1_normal_price}} <span style="font-size: 11px;">+ {{percentage_off}}&#37; OFF</span></p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.0; margin: 0; margin-bottom: 5px; text-decoration: line-through;">Retail Price: {{rec_item_1_retail_price}}</p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 15px;">FREE SHIPPING</p>
                        <a style="background-color: #e67e22; color: #ffffff; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 18px; line-height: 1.0; margin-top: 10px; padding: 8px 0; text-align: center; text-decoration: none; width: 160px;" href="{{rec_item_1_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">SHOP NOW &raquo;</a></td>
                      <td width="10"></td>
                    </tr>
                  </table></td>
                <td><table width="320" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10"></td>
                      <td width="100" valign="top"><a href="{{rec_item_2_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{rec_item_2_image}}" width="100" height="100" alt="{{rec_item_2_desc}}" border="0" /></a></td>
                      <td width="10"></td>
                      <td width="190" align="left" valign="top"><p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 5px;"><a style="color: #333333; text-decoration: none;" href="{{rec_item_2_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_2_desc}}</a></p>
                        <p style="color: #c0392b; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 5px;">{{rec_item_2_normal_price}} <span style="font-size: 11px;">+ {{percentage_off}}&#37; OFF</span></p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.0; margin: 0; margin-bottom: 5px; text-decoration: line-through;">Retail Price: {{rec_item_2_retail_price}}</p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 15px;">FREE SHIPPING</p>
                        <a style="background-color: #e67e22; color: #ffffff; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 18px; line-height: 1.0; margin-top: 10px; padding: 8px 0; text-align: center; text-decoration: none; width: 160px;" href="{{rec_item_2_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">SHOP NOW &raquo;</a></td>
                      <td width="10"></td>
                    </tr>
                  </table></td>
              </tr>
            </table>
            <br /></td>
        </tr>
        <tr>
          <td style="background-color: #cccccc;" width="640" height="1"></td>
        </tr>
        <tr>
          <td style="background-color: #ffffff;" width="640"><br />
            <table width="640" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><table width="320" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10"></td>
                      <td width="100" valign="top"><a href="{{rec_item_3_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{rec_item_3_image}}" width="100" height="100" alt="{{rec_item_3_desc}}" border="0" /></a></td>
                      <td width="10"></td>
                      <td width="190" align="left" valign="top"><p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 5px;"><a style="color: #333333; text-decoration: none;" href="{{rec_item_3_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_3_desc}}</a></p>
                        <p style="color: #c0392b; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 5px;">{{rec_item_3_normal_price}} <span style="font-size: 11px;">+ {{percentage_off}}&#37; OFF</span></p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.0; margin: 0; margin-bottom: 5px; text-decoration: line-through;">Retail Price: {{rec_item_3_retail_price}}</p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 15px;">FREE SHIPPING</p>
                        <a style="background-color: #e67e22; color: #ffffff; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 18px; line-height: 1.0; margin-top: 10px; padding: 8px 0; text-align: center; text-decoration: none; width: 160px;" href="{{rec_item_3_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">SHOP NOW &raquo;</a></td>
                      <td width="10"></td>
                    </tr>
                  </table></td>
                <td><table width="320" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10"></td>
                      <td width="100" valign="top"><a href="{{rec_item_4_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{rec_item_4_image}}" width="100" height="100" alt="{{rec_item_4_desc}}" border="0" /></a></td>
                      <td width="10"></td>
                      <td width="190" align="left" valign="top"><p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 5px;"><a style="color: #333333; text-decoration: none;" href="{{rec_item_4_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_4_desc}}</a></p>
                        <p style="color: #c0392b; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 5px;">{{rec_item_4_normal_price}} <span style="font-size: 11px;">+ {{percentage_off}}&#37; OFF</span></p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.0; margin: 0; margin-bottom: 5px; text-decoration: line-through;">Retail Price: {{rec_item_4_retail_price}}</p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 15px;">FREE SHIPPING</p>
                        <a style="background-color: #e67e22; color: #ffffff; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 18px; line-height: 1.0; margin-top: 10px; padding: 8px 0; text-align: center; text-decoration: none; width: 160px;" href="{{rec_item_4_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">SHOP NOW &raquo;</a></td>
                      <td width="10"></td>
                    </tr>
                  </table></td>
              </tr>
            </table>
            <br /></td>
        </tr>
        <tr>
          <td style="background-color: #cccccc;" width="640" height="1"></td>
        </tr>
        <tr>
          <td style="background-color: #ffffff;" width="640"><br />
            <table width="640" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><table width="320" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10"></td>
                      <td width="100" valign="top"><a href="{{rec_item_5_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{rec_item_5_image}}" width="100" height="100" alt="{{rec_item_5_desc}}" border="0" /></a></td>
                      <td width="10"></td>
                      <td width="190" align="left" valign="top"><p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 5px;"><a style="color: #333333; text-decoration: none;" href="{{rec_item_5_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_5_desc}}</a></p>
                        <p style="color: #c0392b; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 5px;">{{rec_item_5_normal_price}} <span style="font-size: 11px;">+ {{percentage_off}}&#37; OFF</span></p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.0; margin: 0; margin-bottom: 5px; text-decoration: line-through;">Retail Price: {{rec_item_5_retail_price}}</p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 15px;">FREE SHIPPING</p>
                        <a style="background-color: #e67e22; color: #ffffff; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 18px; line-height: 1.0; margin-top: 10px; padding: 8px 0; text-align: center; text-decoration: none; width: 160px;" href="{{rec_item_5_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">SHOP NOW &raquo;</a></td>
                      <td width="10"></td>
                    </tr>
                  </table></td>
                <td><table width="320" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10"></td>
                      <td width="100" valign="top"><a href="{{rec_item_6_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{rec_item_6_image}}" width="100" height="100" alt="{{rec_item_6_desc}}" border="0" /></a></td>
                      <td width="10"></td>
                      <td width="190" align="left" valign="top"><p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 5px;"><a style="color: #333333; text-decoration: none;" href="{{rec_item_6_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_6_desc}}</a></p>
                        <p style="color: #c0392b; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 5px;">{{rec_item_6_normal_price}} <span style="font-size: 11px;">+ {{percentage_off}}&#37; OFF</span></p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.0; margin: 0; margin-bottom: 5px; text-decoration: line-through;">Retail Price: {{rec_item_6_retail_price}}</p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 15px;">FREE SHIPPING</p>
                        <a style="background-color: #e67e22; color: #ffffff; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 18px; line-height: 1.0; margin-top: 10px; padding: 8px 0; text-align: center; text-decoration: none; width: 160px;" href="{{rec_item_6_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">SHOP NOW &raquo;</a></td>
                      <td width="10"></td>
                    </tr>
                  </table></td>
              </tr>
            </table>
            <br /></td>
        </tr>
        <tr>
          <td style="background-color: #cccccc;" width="640" height="1"></td>
        </tr>
        <tr>
          <td style="background-color: #ffffff;" width="640"><br />
            <table width="640" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><table width="320" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10"></td>
                      <td width="100" valign="top"><a href="{{rec_item_7_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{rec_item_7_image}}" width="100" height="100" alt="{{rec_item_7_desc}}" border="0" /></a></td>
                      <td width="10"></td>
                      <td width="190" align="left" valign="top"><p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 5px;"><a style="color: #333333; text-decoration: none;" href="{{rec_item_7_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_7_desc}}</a></p>
                        <p style="color: #c0392b; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 5px;">{{rec_item_7_normal_price}} <span style="font-size: 11px;">+ {{percentage_off}}&#37; OFF</span></p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.0; margin: 0; margin-bottom: 5px; text-decoration: line-through;">Retail Price: {{rec_item_7_retail_price}}</p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 15px;">FREE SHIPPING</p>
                        <a style="background-color: #e67e22; color: #ffffff; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 18px; line-height: 1.0; margin-top: 10px; padding: 8px 0; text-align: center; text-decoration: none; width: 160px;" href="{{rec_item_7_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">SHOP NOW &raquo;</a></td>
                      <td width="10"></td>
                    </tr>
                  </table></td>
                <td><table width="320" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10"></td>
                      <td width="100" valign="top"><a href="{{rec_item_8_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank"><img style="display: block;" src="{{rec_item_8_image}}" width="100" height="100" alt="{{rec_item_8_desc}}" border="0" /></a></td>
                      <td width="10"></td>
                      <td width="190" align="left" valign="top"><p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 5px;"><a style="color: #333333; text-decoration: none;" href="{{rec_item_2_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">{{rec_item_8_desc}}</a></p>
                        <p style="color: #c0392b; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 5px;">{{rec_item_8_normal_price}} <span style="font-size: 11px;">+ {{percentage_off}}&#37; OFF</span></p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.0; margin: 0; margin-bottom: 5px; text-decoration: line-through;">Retail Price: {{rec_item_8_retail_price}}</p>
                        <p style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold; line-height: 1.0; margin: 0; margin-bottom: 15px;">FREE SHIPPING</p>
                        <a style="background-color: #e67e22; color: #ffffff; display: block; font-family: Verdana, Geneva, sans-serif; font-size: 18px; line-height: 1.0; margin-top: 10px; padding: 8px 0; text-align: center; text-decoration: none; width: 160px;" href="{{rec_item_8_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">SHOP NOW &raquo;</a></td>
                      <td width="10"></td>
                    </tr>
                  </table></td>
              </tr>
            </table>
            <br /></td>
        </tr>
      </table>
      
      <!-- Footer Table -->
      
      <table width="640" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center"><br />
            <img src="http://www.wirelessemporium.com/images/email/blast/template/social-zone.gif" alt="" width="149" height="23" border="0" usemap="#Map" style="display: block;" /><br /></td>
        </tr>
        <tr>
          <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 1.5; margin: 0;" align="left">Offer excludes Bluetooth headsets, custom cases and select OEM products.<br />
            <br />
            This email correspondence has been sent to you as you have indicated you would like to receive special, exclusive offers from WirelessEmporium.com. If this message has been sent in error, or you would no longer like to receive any more periodic offers from WirelessEmporium.com, you may <a style="color: #006699; font-weight: bold;" href="http://www.wirelessemporium.com/unsubscribe.asp?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">unsubscribe</a>.<br />
            <br />
            <a style="color: #006699;" href="http://www.wirelessemporium.com/privacy.asp?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Privacy Policy</a> | <a style="color: #006699;" href="http://www.wirelessemporium.com/termsofuse.asp?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Terms &amp; Conditions</a> | <a style="color: #006699;" href="http://www.wirelessemporium.com/contactus.asp?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank">Contact Us</a><br />
            <br />
            &copy;2002-2013 WirelessEmporium.com<br />
            <br /></td>
        </tr>
      </table>
      
      <!-- End Content --></td>
  </tr>
</table>
<!-- End Container -->

<map name="Map" id="Map">
  <area shape="rect" coords="0,0,11,23" href="http://www.facebook.com/WirelessEmporium/?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank" alt="Facebook" title="Facebook" />
  <area shape="rect" coords="33,0,66,23" href="http://twitter.com/#!/wirelessemp/?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank" alt="Twitter" title="Twitter" />
  <area shape="rect" coords="82,0,105,23" href="https://plus.google.com/113217319312103488691?rel=author&utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank" alt="Google+" title="Google+" />
  <area shape="rect" coords="126,0,149,23" href="http://www.wirelessemporium.com/blog/?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_id}}&utm_promocode={{promo_code}}" target="_blank" alt="Blog" title="Blog" />
</map>
</body>
</html>
