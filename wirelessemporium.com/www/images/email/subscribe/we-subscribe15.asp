<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<%

bTestMode = true

sql = "exec sp_ppUpsell 0, 102964, 0, 30"
session("errorSQL") = sql
set objRsUpSell = oConn.execute(sql) 

strUpSell = ""

if not objRsUpSell.EOF then
	strUpSell	=	"<tr>" & vbcrlf & _
					"	<td style='background-color: #ffffff;' width='640'>" & vbcrlf & _
					"		<table width='640' cellspacing='0' cellpadding='0'>" & vbcrlf & _
					"			<tbody>" & vbcrlf & _
					"				<tr>"
			
	counter = 1
	do until objRsUpSell.EOF
		itemID		= objRsUpSell("itemID")
		itemPic		= objRsUpSell("itemPic")
		itemDesc	= objRsUpSell("itemDesc")
		useItemDesc = replace(itemDesc,"  "," ")
		itemImgPath = server.MapPath("/productpics/big/" & itemPic)
		'itemImgPath = "C:\inetpub\wwwroot\productpics\big\" & itempic
		price		= objRsUpSell("price")
		price_retail= objRsUpSell("price_retail")
		discountAmt = price_retail - price
		discount = discountAmt * 100 / price_retail
		'promoPrice	= price - discount
		
		productLink = "http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".html?dzid=email_TRANS_RECS_" & counter & "L_" & itemID & "_WELCOME15&utm_source=internal&utm_medium=email&utm_campaign=Welcome1&utm_content=welcome&utm_promocode=WELCOME15"
		
		set fsThumb = CreateObject("Scripting.FileSystemObject")
		if not fsThumb.FileExists(itemImgPath) then
			useImg = "/productPics/big/imagena.jpg"
			DoNotDisplay = 1
		else
			useImg = "/productPics/big/" & itemPic
		end if
		
		if counter = 3 or counter = 6 then
			style = "padding:20px 10px 0 20px;"
		else 
			style = "padding:20px 0 0 20px;"
		end if
		
		strUpSell 	= strUpSell &	"	<td width='210' style='" & style & "vertical-align:top;'>" & vbcrlf & _
										"		<table width='190' cellpadding='0' cellspacing='0'>" & vbcrlf & _
										"			<tbody>" & vbcrlf & _
										"				<tr>" & vbcrlf & _
										"					<td width='190' height='190'>" & vbcrlf & _
										"						<a href='" & product_link & "'><img src='http://www.wirelessemporium.com" & useImg & "' style='display:block;' width='190' /></a>" & vbcrlf & _
										"					</td>" & vbcrlf & _
										"				</tr>" & vbcrlf & _
										"				<tr>" & vbcrlf & _
										"					<td width='190' height='60' style=""padding:7px 0px 7px 0px;"" valign=""top"">" & vbcrlf & _
										"						<a href='" & product_link & "' target=""_blank"" style=""font-size:12px; color:#333; font-family: Arial, sans-serif;"">" & itemDesc & "</a>" & vbcrlf & _
										"					</td>" & vbcrlf & _
										"				</tr>" & vbcrlf & _											
										"				<tr>" & vbcrlf & _
										"					<td>" & vbcrlf & _
										"						<span style='color:#999;font-size:12px;color:#ccc;text-decoration:line-through;font-family:Arial, sans-serif;'>Retail Price: " & formatCurrency(price_retail) & "</span><br />" & vbcrlf & _
										"					</td>" & vbcrlf & _
										"				</tr>" & vbcrlf & _
										"				<tr>" & vbcrlf & _
										"					<td style='padding:10px 0 0 0;'>" & vbcrlf & _
										"						<span style='color:#000;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;'>Our Price:</span> <span style='color:#ff6633;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;'>" & formatCurrency(price) & "</span><br />" & vbcrlf & _
										"						<span style='color:#000;font-size:14px;font-family:Arial, sans-serif;'>You Save:</span> <span style='margin:10px 0 0 0;color:#000;font-size:14px;font-family:Arial, sans-serif;font-weight:bold;'>" & Round(discount) & "% (" & formatCurrency(discountAmt) & ")</span>" & vbcrlf & _
										"					</td>" & vbcrlf & _
										"				</tr>" & vbcrlf & _
										"				<tr>" & vbcrlf & _
										"					<td style='padding:10px 0 0 0;'>" & vbcrlf & _
										"						<table width='160' border='0' cellpadding='0' cellspacing='0'>" & vbcrlf & _
										"							<tbody>" & vbcrlf & _
										"								<tr>" & vbcrlf & _
										"									<td align='center' style='background-color:#ff6633;border-radius:16px;padding:7px 0;'>" & vbcrlf & _
										"										<a href='" & product_link & "' style='color:#fff;font-weight:bold;font-size:14px;font-family:Arial, sans-serif;text-decoration:none;'>SHOP NOW &raquo;</a>" & vbcrlf & _
										"									</td>" & vbcrlf & _
										"								</tr>" & vbcrlf & _
										"							</tbody>" & vbcrlf & _
										"						</table>" & vbcrlf & _
										"					</td>" & vbcrlf & _
										"				</tr>" & vbcrlf & _
										"			</tbody>" & vbcrlf & _
										"		</table>" & vbcrlf & _
										"	</td>"
		
		if counter = 3 then
			strUpSell = strUpSell & "						</tr>" & vbcrlf & _
						"					</tbody>" & vbcrlf & _
						"				</table>" & vbcrlf & _
						"				<br />" & vbcrlf & _
						"			</td>" & vbcrlf & _
						"		</tr>" & vbcrlf & _
						"		<tr>" & vbcrlf & _
						"		  <td style='background-color: #cccccc;' width='640' height='1'></td>" & vbcrlf & _
						"		</tr>" & vbcrlf & _
						"		<tr>" & vbcrlf & _
						"			<td style='background-color: #ffffff;padding:0 0 25px;' width='640'>" & vbcrlf & _
						"				<table width='640' cellspacing='0' cellpadding='0'>" & vbcrlf & _
						"					<tbody>" & vbcrlf & _
						"						<tr>"
			counter = counter + 1
		elseif counter = 6 then
			exit do
		else
			counter = counter + 1
		end if
		objRsUpSell.movenext
	loop
	
	strUpSell = strUpSell & "</tr>" & vbcrlf & _
				"</table>"
end if

strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\images\email\template\we-subscribe15.htm")
strTemplate = replace(strTemplate, "[UPSELL]", strUpSell)
strTemplate = replace(strTemplate, "[EXPIRY]", DateAdd("d", 7, Date()))
response.write strTemplate

function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")		
	else
		formatSEO = ""
	end if
	formatSEO = replace(formatSEO, "---", "-")
	formatSEO = replace(formatSEO, "--", "-")	
end function

%>