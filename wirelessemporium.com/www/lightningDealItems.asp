<%
	response.buffer = true
	noLeftNav = 0
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<style>
.quarter {
	width:24%;
	display:inline-block;
}
</style>
<%
categoryID = request.QueryString("categoryID")

if categoryID <> "" then
	sql =	"select		ci.id, c.name, a.* " & vbcrlf & _
			"from		lightningDeal_CategoryItems ci left join lightningDeal_Categories c" & vbcrlf & _
			"	on		c.id = ci.categoryID left join we_Items a " & vbcrlf & _
			"	on		ci.itemID = a.itemid " & vbcrlf & _
			"where		ci.categoryID = " & categoryID & " " & vbclrf & _
			"	and		ci.isActive = 1 " & vbcrlf & _
			"order by	ci.sortOrder "
	session("errorSQL") = sql
	'sql = "select * from sys.tables where name like 'lightning%'"
	
	set rs = oConn.execute(sql)
	
	'do while not rs.eof
	'	response.write(rs("name") & "<br />")
	'	rs.movenext
	'loop
	
	'response.end
	
%>
<div>
<h1><%=rs("name")%></h1>
</div>
<% i = 1 %>
<% if not rs.eof then %>
<% do while not rs.eof %>
<%
	id = rs("id")
	itemID = rs("itemID")
	itemDesc = rs("itemDesc")
	itemPic = rs("itemPic")
	itemPartNumber = rs("partNumber")
	itemPriceOur = rs("price_Our")
	itemPriceRetail = rs("price_Retail")
%>
<div class="quarter">
                        <div class="bmc_productPic" id="picDiv_<%=i%>" title="<%=itemDesc%>">
                        	<div id="id_productImage_135708" style="position:relative;">
								
                                <img src="/productpics/thumb/<%=itemPic%>" border="0" width="100" height="100" title="<%=itemDesc%>" onclick="window.location='/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.asp'" class="clickable">
                                <br><span style="color:red; font-size:10px;"><%=itemPartNumber%></span><br><span style="font-size:10px;"><%=itemID%></span>
                            </div>
                        </div>
                        
                        <div class="product-bmc-details" align="center">
                            <div id="id_productLink_<%=itemID%>" class="product-bmc-desc">
                                <a class="cellphone-link" href="/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.asp" title="<%=itemDesc%>" alt="<%=itemDesc%>"><%=itemDesc%></a>
                            </div>
                        	                                                    
                        </div>
                        <div style="float:left; width:100%; padding-top:5px;" align="center">
                            <span class="boldText">Our&nbsp;Price:&nbsp;</span>
                            <span class="pricing-orange2">$<%=itemPriceOur%></span><br>
                            <span class="pricing-gray2">List&nbsp;Price:&nbsp;<s>$<%=itemPriceRetail%></s></span>
                        </div>
                        <div id="mvt_bmcd_fresshipping" style="float:left; width:100%; padding:5px 0px 5px 0px;" align="center">
                            <img src="/images/bmc/we-freeshipping1.jpg" border="0">
                        </div>
                        
</div>
<% i = i + 1 %>
<% rs.movenext %>
<% loop %>
<% end if %>


<% else %>
<h1>No items found.</h1>
<% end if %>
<!--#include virtual="/includes/template/bottom.asp"-->
<% fCloseConn %>
