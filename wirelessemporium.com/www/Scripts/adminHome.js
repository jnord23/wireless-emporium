﻿function TrimStrings(stringToTrim) {
    return stringToTrim.replace(/^\s+|\s+$/g, "");
}

function ajax(newURL, rLoc) {
    var httpRequest;
    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
        httpRequest = new XMLHttpRequest();
        if (httpRequest.overrideMimeType) {
            httpRequest.overrideMimeType('text/xml');
        }
    } else if (window.ActiveXObject) { // IE
        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }
    httpRequest.open('GET', newURL, true);
    httpRequest.onreadystatechange = function () {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                badRun = 0
                var rVal = httpRequest.responseText
                if (rVal == "") {
                    alert("No data available")
                }
                else if (rVal == "refresh") {
                    curLoc = window.location
                    window.location = curLoc
                }
                else if(document.getElementById(rLoc)) {
                    document.getElementById(rLoc).innerHTML = rVal
                }
                rVal = null;
            }
            else {
                document.getElementById("testZone").innerHTML = newURL
                alert("Error performing action")
            }
        }
    };
    httpRequest.send(null);
}

function ajaxPost(newURL, params, rLoc) {
    var httpRequest;
    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
        httpRequest = new XMLHttpRequest();
        if (httpRequest.overrideMimeType) {
            httpRequest.overrideMimeType('text/xml');
        }
    } else if (window.ActiveXObject) { // IE
        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }
    httpRequest.open('POST', newURL, true);
    httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    httpRequest.setRequestHeader("Content-length", params.length);
    httpRequest.send(params);

    httpRequest.onreadystatechange = function () {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                badRun = 0
                var rVal = httpRequest.responseText
                if (rVal == "") {
                    alert("No data available")
                }
                else if (rVal == "refresh") {
                    curLoc = window.location
                    window.location = curLoc
                }
                else if(document.getElementById(rLoc)) {
                    document.getElementById(rLoc).innerHTML = rVal
                }
                rVal = null;
            }
            else {
                document.getElementById("testZone").innerHTML = newURL
                alert("Error performing action")
            }
        }
    };
}




function testPass() {
    var pass1 = document.newPassForm.pass1.value
    var pass2 = document.newPassForm.pass2.value

    if (pass1 == pass2) {
        if (pass1.length < 7) {
            alert("Your new password must be at least 7 characters long")
            document.newPassForm.pass2.value = ""
            document.newPassForm.pass1.select()
            return false
        }
        else {
            return true
        }
    }
    else {
        alert("The re-entered password must match")
        document.newPassForm.pass2.value = ""
        document.newPassForm.pass2.focus()
        return false
    }
}

function menuTab(which) {

    document.getElementById("products").style.display = 'none'
    document.getElementById("productsTab").style.color = '#fff'

    document.getElementById("orders").style.display = 'none'
    document.getElementById("ordersTab").style.color = '#fff'

    document.getElementById("marketing").style.display = 'none'
    document.getElementById("marketingTab").style.color = '#fff'

    document.getElementById(which).style.display = ''
    document.getElementById(which + "Tab").style.color = '#FF0'
}
function reportMenuTab(which) {
    document.getElementById("sales").style.display = 'none'
    document.getElementById("salesTab").style.color = '#fff'
    document.getElementById("inventory").style.display = 'none'
    document.getElementById("inventoryTab").style.color = '#fff'
    document.getElementById("other").style.display = 'none'
    document.getElementById("otherTab").style.color = '#fff'
    document.getElementById(which).style.display = ''
    document.getElementById(which + "Tab").style.color = '#FF0'
}
