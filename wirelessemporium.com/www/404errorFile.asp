<%
'Causes Google to detect Crawl Errors
'Response.Clear()
'Response.Status = "404 Not Found"

'Soft 404
Response.Clear
Response.Status = "200 OK"

pageTitle = "File not Found ?Error 404"
'note: stuff below will not display [knguyen/20110517]
%>
<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
					<%=TABLE_START%>
                    <tr>
					<td valign="top" width="748" align="center">
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td align="left" valign="top" width="100%" class="breadcrumbFinal"><img src="http://www.wirelessemporium.com/images/spacer.gif" width="1" height="10"></td>
							</tr>
							<tr>
								<td align="left" valign="top">
									<h3 align="center">
										Sorry, but that product is no longer available.
									</h3>
									<p>
										At WirelessEmporium.com, we are continually updating our vast inventory, so the product you are searching for may be unavailable at this time, or its name may have changed.
									</p>
									<p>
										Please use our convenient browse or search features to quickly find the product you need.
									</p>
									<p>
										If you still cannot find what you are looking for, please feel free to e-mail us at
										<a href="mailto:service@WirelessEmporium.com">service@WirelessEmporium.com</a>
									</p>
								</td>
							</tr>
						</table>
						<p>&nbsp;</p>
					</td>
                    </tr>
                    <%=TABLE_END%>

<!--#include virtual="/includes/template/bottom.asp"-->
