<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
response.buffer = true
if request.ServerVariables("HTTP_X_REWRITE_URL") = "/index.asp" then PermanentRedirect "/"

dim pageTitle : pageTitle = "Home"
dim SEtitle, SEdescription, SEkeywords
SEtitle = "Cell Phone Accessories Cell Phone Covers Cases Batteries | iPhone BlackBerry Motorola Samsung LG"
SEdescription = "The #1 Name in Cell Phone Accessories Online - Wireless Emporium. Find all cell phone accessories like cases, covers, chargers and batteries for popular brands like iPhone, BlackBerry, Motorola, HTC, Samsung and LG."
SEkeywords = "cell phone accessories, cheap cell phone accessories, cellphone accessories, discount cell phone accessories, mobile phone accessories, mobile accessories, cellular accessories, cell phone covers, cell phone chargers, cell phone cases, cell phone batteries, iphone accessories, iphone cases, iphone covers, iphone batteries, iphone chargers, blackberry accessories, blackberry chargers, blackberry cases, blackberry batteries, Motorola accessories, Motorola chargers, Motorola cases, Motorola covers, LG covers, LG accessories, LG cases, LG batteries, Samsung accessories, Samsung cases, Samsung covers, Samsung batteries"

nItemDescMaxLength = 52
noCommentBox = true

dim arrBanners(4), holidaySale
holidaySale = 0

if date > cdate("11/24/2011") and date < cdate("11/28/2011") then holidaySale = 1	'black friday(F,SA,SU)
if date > cdate("11/27/2011") and date < cdate("12/01/2011") then holidaySale = 2	'cyber monday(M,T)
if date > cdate("11/30/2011") and date < cdate("12/13/2011") then holidaySale = 3	'holiday madness(Christmas)
if date > cdate("12/12/2011") and date < cdate("12/19/2011") then holidaySale = 4	'holiday madness(Christmas)

if holidaySale > 0 then
	bannerLength = 4
	if holidaySale = 4 then
		arrBanners(1) = "<img src='/images/mainbanners/weExpeditedShipping.jpg' border='0' width='530' height='320'  />"
	else
		arrBanners(1) = "<img src='/images/mainbanners/holidayBanner1-" & holidaySale & ".jpg' border='0' width='530' height='320'  />"
	end if
	arrBanners(2) = "<img src='/images/mainbanners/holidayBanner02.jpg' border='0' width='530' height='320' />"
	arrBanners(3) = "<a href='http://www.cellstores.com/mobile/?r=wirelessemporium2' target='_blank'><img src='/images/mainbanners/holidayBanner03.jpg' border='0' width='530' height='320' /></a>"
	arrBanners(4) = "<a href='http://www.wirelessemporium.com/sb-17-sm-1267-sc-3-covers-faceplates-apple-iphone-4s.asp'><img src='/images/mainbanners/holidayBanner04.jpg' border='0' width='530' height='320' /></a>"
else
	bannerLength = 3
	arrBanners(1) = "<img src='/images/mainbanners/banner01.jpg' border='0' width='530' height='320' />"
	arrBanners(2) = "<a href='http://www.cellstores.com/mobile/?r=wirelessemporium2' target='_blank'><img src='/images/mainbanners/banner02.jpg' border='0' width='530' height='320' /></a>"
	arrBanners(3) = "<a href='http://www.wirelessemporium.com/sb-17-sm-1267-sc-3-covers-faceplates-apple-iphone-4s.asp'><img src='/images/mainbanners/banner03.jpg' border='0' width='530' height='320' /></a>"
end if

topCats = "Covers &amp; Faceplates##Chargers &amp; Cables##Screen Protectors &amp; Skins##Pouches &amp; Carrying Cases##Batteries##Holsters, Holders &amp; Mounts##Custom Cases##Cell Phones &amp; Tablets"
topCatArray = split(topCats,"##")
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<link href="/includes/css/mvt_searchBar1.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_homeBanner1.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_largeBanners2.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_topCats2.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_miniBanners3.css" rel="stylesheet" type="text/css">
<table border="0" bordercolor="#FF0000" cellpadding="0" cellspacing="0" width="100%">
    <tr id="newCatButtons">
    	<td>
        	<!--#include virtual="/Framework/seoContent.asp"-->
            <div style="width:748px; height:30px;">
                <div id="topBar-topCategories-left" title=""></div>
                <div id="topBar-middle" style="width:325px;"></div>
                <div id="topBar-right"></div>            
            </div>
            <div style="width:726px; height:96px; border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc; background-color:#e4e4e4; padding:10px;">
            	<%
				itemCnt = 0
				for i = 0 to ubound(topCatArray)
					catName = topCatArray(i)
					itemCnt = itemCnt + 1
					if itemCnt < 4 then
						boxSpacing = 1
					else
						boxSpacing = 0
						itemCnt = 0
					end if
					
					if catName = "Cell Phones &amp; Tablets" then
						useLink = "/" & replace(formatSEO(catName),"-amp","") & ".asp"
					elseif catName = "Handsfree Bluetooth" then
						useLink = "/bluetooth-headsets-handsfree.asp"
					elseif catName = "Custom Cases" then
						useLink = "/custom-phone-cases"
					else
						useLink = "/cell-phone-" & replace(formatSEO(catName),"-amp","") & ".asp"
					end if
				%>
                <div style="float:left; text-align:center; background:url(/images/buttons/button-cat-background.jpg); width:180px; height:33px; padding-top:15px; font-weight:bold; margin:0px <%=boxSpacing%>px 5px 0px;"><a href="<%=useLink%>" style="color:#000;"><%=catName%></a></div>
                <%
				next
				%>
            </div>
        </td>
    </tr>
    <% if Application("shopRunner") = 1 then %>
    <tr>
    	<td width="100%" style="padding-top:15px;"><div name="sr_bannerDiv"></div></td>
    </tr>
    <% end if %>
	<tr>
    	<td width="100%" style="padding-top:15px;" id="featuredProducts"></td>
    </tr>
    <tr>
    	<td id="miniBanners_lower" style="padding:10px 0px 10px 0px;">
        	<div style="float:left;"><a href="/faq.asp"><img src="/images/mainbanners/set1-minibanner1.jpg" border="0" /></a></div>
            <div style="float:left; padding-left:6px;"><a href="/custom"><img src="/images/mainbanners/custom-hombanner-mini1.jpg" border="0" /></a></div>
            <%'<div style="float:left; padding-left:6px;"><a href="/sb-17-sm-1267-scd-1030-design-faceplates-covers-apple-iphone-4s.asp"><img src="/images/mainbanners/set1-minibanner2.jpg" border="0" /></a></div>%>
            <div style="float:left; padding-left:6px;"><a href="http://www.cellstores.com/mobile/?r=wirelessemporium2" target="_blank"><img src="/images/mainbanners/set1-minibanner3.jpg" border="0" /></a></div>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
	<tr>
    	<td>
			<div style="float:left; width:440px; height:750px; margin-right:15px;">
	        	<%
				'<div style="width:430px; font-size:15px; font-weight:bold; color:#494949; border:1px solid #ccc; background-color:#ebebeb; text-align:left; height:25px; padding:10px 0px 0px 10px">WIRELESS EMPORIUM</div>
                '<div style="width:420px; font-size:10px; color:#666666; padding:5px 5px 0px 5px;">
                '	<strong>Wireless Emporium</strong> is the Leader in <h1 class="xtrasmlIndexText" title="Cell Phone Accessories">Cell Phone Accessories</h1>. 
                '    We offer 1000s of accessories for all brands - 
                '    <a href="/blackberry-cell-phone-accessories.asp" class="xtrasmlIndexLink" title="BlackBerry Accessories">BlackBerry Accessories</a>, 
                '    <a href="/iphone-accessories.asp" class="xtrasmlIndexLink" title="iPhone Accessories">iPhone Accessories</a>, 
                '    <a href="/motorola-cell-phone-accessories.asp" class="xtrasmlIndexLink" title="Motorola Accessories">Motorola Accessories</a>,																 
                '    <a href="/lg-cell-phone-accessories.asp" class="xtrasmlIndexLink" title="LG Accessories">LG Accessories</a>, 
                '    <a href="/samsung-cell-phone-accessories.asp" class="xtrasmlIndexLink" title="Samsung Accessories">Samsung Accessories</a>, 
                '    <a href="/htc-cell-phone-accessories.asp" class="xtrasmlIndexLink" title="HTC Accessories">HTC Accessories</a> 
                '    &amp; more. All products, from 
                '    <a href="/cell-phone-covers-faceplates-screen-protectors.asp" class="xtrasmlIndexLink" title="cell phone covers">cell phone covers</a>, 
                '    <a href="/cell-phone-chargers.asp" class="xtrasmlIndexLink" title="cell phone chargers">cell phone chargers</a>, 
                '    <a href="/cell-phone-batteries.asp" class="xtrasmlIndexLink" title="cell phone batteries">cell phone batteries</a> to 
                '    <a href="/bluetooth-headsets-handsfree.asp" class="xtrasmlIndexLink" title="Bluetooth headsets">Bluetooth headsets</a>, 
                '    come with a 100% satisfaction guarantee. Discount Accessories + FREE SHIPPING = 
                '    The Best TOTAL Price Online, Every Day!
                '    <div id="spacer" style="height:10px;width:100%;"></div>
                '</div>
				%>
				<%
                '<div style="width:430px; margin-top:20px; font-size:15px; font-weight:bold; color:#494949; border:1px solid #ccc; background-color:#ebebeb; text-align:left; height:25px; padding:10px 0px 0px 10px">CELL PHONE ACCESSORIES</div>
                '<div style="width:420px; margin-bottom:20px; font-size:10px; color:#666666; padding:5px 5px 0px 5px;">
                '	These days, phones act as our social hubs, providing us with the ability to connect with friends and family through multiple media outlets. 
                '    Wireless Emporium knows how important it is to personalize and protect your phone, which is why we offer the largest assortment of cell phone 
                '    accessories anywhere. With our phones doing so much more than just making calls, they tend to use up battery life quickly. Make sure your 
                '    phone has the juice to get you through your busy day. Optimize the performance of your phone with our huge selection of cell phone batteries, 
                '    cell phone chargers, and cell phone holsters. You can also make a statement and show off your personal style. We have thousands of 
                '    <a href="/cell-phone-covers-faceplates-screen-protectors.asp" class="xtrasmlIndexLink" title="cell phone covers">cell phone covers</a>, 
                '    <a href="/cell-phone-cases.asp" class="xtrasmlIndexLink" title="cell phone cases">cell phone cases</a> 
                '    and cell phone charms to add a little color to your handheld device. Our full line of cheap cell phone accessories 
                '    aren't just priced well below retail prices, everything we sell is made from high quality materials and is guaranteed to meet the most 
                '    stringent of industry standards. Regardless of what brand or model phone you have, Wireless Emporium has the largest selection of accessories 
                '    to fit your needs. Whether it be a car charger for your Blackberry, a new phone case for your iPhone or a phone cover to protect that new 
                '    Samsung phone, we have it. We are a leading resource for phone accessories and 
                '    <a href="/unlocked-cell-phones.asp" class="xtrasmlIndexLink" title="unlocked cell phones">unlocked cell phones</a> 
                '    for every major brand of phone in the market, like HTC, LG, Nokia, Motorola and more. Shop with peace of mind, knowing that all of our 
                '    products are backed by our 100% satisfaction guarantee and come with free shipping.  Our commitment to customer service is the reason why we 
                '    have cultivated relationships with so many of our customers, and it's why you should choose Wireless Emporium for all of your cellular needs.
                '</div>
				%>
                <!--#include virtual="/includes/asp/commentBoxIndex.asp"-->
            </div>
            <div style="float:left; width:280px; height:730px; border:1px solid #ccc;">
            	<a href="http://www.wirelessemporium.com/blog/" target="_blank" title="Wireless Emporium Blogs"><div id="topBar-blog" title="Wireless Emporium Blogs"></div></a>
				<%
'                sql =	"	select	top 9 a.b_articleid, a.b_articleTitle, replace(dbo.fn_stripHTML(a.b_articleContent), CHAR(160), '') b_articleContent	" &_
'                        "		,	a.b_firstPostTime, b.nickname, c.b_categoryname" &_ 
'                        "	from	b_articles a left join b_user b " &_
'                        "		on	a.b_userid = b.userid left join b_category c " &_
'                        "		on	a.b_categoryid = c.b_categoryid " &_
'                        "	where	a.store = 0 and a.b_parentid = 0 and a.b_categoryid <> 2 " &_
'                        "		and (a.b_articlestatus = 1 or a.b_articlestatus = 3) " &_
'                        "	order by a.b_firstposttime desc"
				sql	=	"select	top 9 id, title, convert(varchar(1000), replace(dbo.fn_stripHTML(content), CHAR(160), '')) content, pubDate, lower(slug) slug" & vbcrlf & _
						"from	xblogposts" & vbcrlf & _
						"where	ispublished = 1" & vbcrlf & _
						"	and	isdeleted = 0" & vbcrlf & _
						"	and	site_id = 0" & vbcrlf & _
						"order by pubDate desc"
                session("errorSQL") = sql
                set rsBlog = oConn.execute(sql)
                
				lap = 0
                do until rsBlog.eof
					lap = lap + 1
                    bTitle = rsBlog("title")
                    bContent = ltrim(replace(replace(replace(rsBlog("content"), vbcr, ""), vblf, ""), vbTab, ""))
					bNameURL = rsBlog("slug")
                    pubDate = rsBlog("pubDate")
                    PostMonth = right("00" & month(pubDate), 2)
                    PostDay = right("00" & day(pubDate), 2)
                    PostYear = year(pubDate)
                    if len(bTitle) > 38 then bTitle = left(bTitle, 35) & "..."
                    if len(bContent) > 80 then bContent = left(bContent, 77) & "..."
					
					blogLink = "http://www.wirelessemporium.com/blog/post/" & PostYear & "/" & PostMonth & "/" & PostDay & "/" & bNameURL & ".aspx"
                    
                    tempBorderBottom = ""
                    if lap < 9 then tempBorderBottom = "border-bottom:1px solid #ccc;" end if							
                %>
                <div style="width:270px; padding:5px; height:60px;">
                    <div style="float:left; width:15px; padding-top:3px; height:60px; font-size:18px;" align="left" valign="top">&bull;</div>
                    <div style="float:left; width:255px; <%=tempBorderBottom%> padding:5px 0px 5px 0px; height:60px;" class="indexBlogText">
                    	<div style="float:left; width:100%;"><span style="color:#6d6d6d;">(Posted on <%=PostMonth & " " & PostDay & ", " & PostYear%>)</span></div>
                    	<div style="float:left; width:100%;"><a target="_blank" href="<%=blogLink%>" class="indexBlogLink" title="<%=bTitle%>"><%=bTitle%></a></div>
                    	<div style="float:left; width:100%;"><a target="_blank" href="<%=blogLink%>" style="font-weight:normal;" class="indexBlogLink" title="<%=bContent%>"><%=bContent%></a></div>
                    </div>
                </div>
                <%
                    rsBlog.movenext
                loop
				
				rsBlog = null
                %>
            </div>
        </td>
    </tr>
</table>
<script language="javascript">var isMasterPage = 1</script>
<!--#include virtual="/includes/template/bottom_index.asp"-->
<script language="javascript">
	var curBanner = 1
	var cycle = 1

	function nextBanner() {
		if (cycle == 1) {
			curBanner++
			if (curBanner == <%=(bannerLength+1)%>) { curBanner = 1 }

			<%
			for i=1 to bannerLength
				response.write "document.getElementById(""banner" & i & "Tab"").src = ""/images/icons/banner_off.gif""" & vbcrlf
			next			
			%>
			
			document.getElementById("banner" + curBanner + "Tab").src = "/images/icons/banner_on.gif"
			
			<%
			for i=1 to bannerLength
				response.write "if (curBanner == " & i & ") document.getElementById(""bannerCode"").innerHTML = """ & arrBanners(i) & """" & vbcrlf
			next
			%>
		}
		
		setTimeout('nextBanner()', 8000);
	}

	function setBanner(num) {
		cycle = 0
		
		<%
		for i=1 to bannerLength
			response.write "document.getElementById(""banner" & i & "Tab"").src = ""/images/icons/banner_off.gif""" & vbcrlf
		next			
		%>
		
		document.getElementById("banner" + num + "Tab").src = "/images/icons/banner_on.gif"

		<%
		for i=1 to bannerLength
			response.write "if (num == " & i & ") document.getElementById(""bannerCode"").innerHTML = """ & arrBanners(i) & """" & vbcrlf
		next
		%>
	}
	
	window.onload = function() {
		setTimeout('nextBanner()', 8000);
		dynamicLoad();
		}
</script>