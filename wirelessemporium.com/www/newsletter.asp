<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%" onclick="window.location='/'" style="cursor:pointer;">
    <tr>
        <td>
        	<div style="width:450px; margin:30px auto; font-size:16px; font-weight:bold; color:#000; cursor:pointer;">
                Thanks for joining!
                <br /><br />
                We are excited that you decided to join our exclusive email membership club. You're now set to 
                get access to private sales, handcrafted deals and coupon codes, as well as the latest in cell 
                phone news.
                <br /><br />
                Here at WirelessEmporium.com, we're 100% dedicated in ensuring that you have the best cell 
                phone accessory shopping experience. We're always working hard to ensure we have the biggest 
                selection at the lowest possible prices. That is why we have been the #1 name in cell phone 
                accessories since 2001. 
                <br /><br />
                Looking forward to seeing you soon,<br />
                The Wireless Emporium Team
            </div>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->
