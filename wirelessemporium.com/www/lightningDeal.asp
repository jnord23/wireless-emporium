<%
	response.buffer = true
	noLeftNav = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<%
	notFound = "Check back soon."
	forceRefresh = false 'force reload to correct errors, etc 'make this true for a period of time greater than or equal to the refresh period, then turn it back off
	
	projID = prepStr(request.QueryString("id"))
	if prepStr(request.Querystring("bypass")) = "1" then
		bypass = true
	else
		bypass = false
	end if

	if projID <> "" then
		sql =	"select		*" & vbcrlf &_
				"from		lightningDeal_Projects" & vbcrlf &_
				"where		id = '" & projID & "'" & vbcrlf &_
				"	--and		releaseDate <= getdate() and expirationDate > getdate()"
		session("errorSQL") = sql
		set	rs = oConn.execute(SQL)
		
		if not rs.eof then
			projName = rs("Name")
			siteID = rs("siteID")
			projEntryDate = rs("entryDate")
			projReleaseDate = rs("releaseDate")
			projExpirationDate = rs("expirationDate")
			projBannerFileName = rs("bannerFileName")
			projHeaderTextTerm1 = rs("headerTextTerm1")
			projHeaderTextDefinition1 = rs("headerTextDefinition1")
			projHeaderTextTerm2 = rs("headerTextTerm2")
			projHeaderTextDefinition2 = rs("headerTextDefinition2")
			projHeaderFrequency = rs("headerFrequency")
			projIsActive = rs("isActive")
			isReleaseLive = true
		else
			isReleaseLive = false
		end if
	else
		projName = "Demo Project"
		siteID = 0
		projEntryDate = now
		projReleaseDate = now
		projExpirationDate = dateadd("h", 1, now)
		projBannerFileName = "banner.jpg"
		projHeaderTextTerm1 = "On Sale:"
		projHeaderTextDefinition1 = "Cool Universal Gadgets & Great Gift Ideas!"
		projHeaderTextTerm2 = "Clearance Items:"
		projHeaderTextDefinition2 = "Up to <strong>80% Off!</strong>"
		projHeaderFrequency = "Extreme Deals Every 2 Hours."
		projIsActive = true
	end if
%>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="/Scripts/jquery-ui-1.8.11.min.js" type="text/javascript"></script>
<script type="text/javascript">var page = '<%=request.ServerVariables("SCRIPT_NAME")&"?"&request.ServerVariables("QUERY_STRING")%>';//Required by lightningDeal.js</script>
<script src="/includes/js/lightningDeal.js?v=1" type="text/javascript"></script>

<!--[if IE 7]> 
<link rel="stylesheet" type="text/css" href="/includes/css/lightningDeal8.css?v=1" />
<![endif]-->

<!--[if IE 8]> 
<link rel="stylesheet" type="text/css" href="/includes/css/lightningDeal8.css?v=1" />
<![endif]-->

<link rel="stylesheet" type="text/css" href="/includes/css/lightningDeal.css?v=1" />
<div id="divDealContainer">
    <div id="divUpperPortion">
        <div id="divTopBanner"><img border="0" src="/images/lightningDeal/<%=projBannerFileName%>" /></div>
        <div id="divRightColumn">
			<%
            sql	=	"exec sp_lightningActiveProds " & projID
            session("errorSQL") = sql
'			response.write "<pre>" & sql & "</pre>" : response.end
            set	rs = oConn.execute(SQL)

            if not rs.eof then
				itemID = rs("itemID")
				itemDesc = rs("itemDesc")
				itemPic = rs("itemPic")
				itemPriceOur = rs("price_Our")
				itemPriceRetail = rs("price_Retail")
				itemPriceDeal = rs("price")
				itemBullet1 = rs("bullet1")
				itemBullet2 = rs("bullet2")
				itemBullet3 = rs("bullet3")
				itemBullet4 = rs("bullet4")
				itemBullet5 = rs("bullet5")
				itemBullet6 = rs("bullet6")
				itemBullet7 = rs("bullet7")
				itemBullet8 = rs("bullet8")
				itemBullet9 = rs("bullet9")
				itemBullet10 = rs("bullet10")
				itemLongDetail = rs("itemLongDetail")
				itemReleaseDate = rs("releaseDate")
				itemExpirationDate = rs("expirationDate")
				partNumber = rs("partNumber")
				showAnimation = rs("showAnimation")
				itemQty = rs("inv_qty")
				productColor = rs("color")
				
				if now > itemReleaseDate and now < itemExpirationDate then displayMain = true
	
				if displayMain = true then
	            %>
        	    <div id="divFeaturedItemContainer">
                    <h1><%=itemDesc%></h1>
                    <div id="divPhotoContainer">
						<div id="imgLarge-location-pool" style="display:none;"><img src="/productpics/big/<%=itemPic%>" border="0" width="300" /></div>
						<div id="divMainPhoto"><img src="/productpics/big/<%=itemPic%>" border="0" width="300" /></div>
                    	<div id="divThumbnails">
							<script type="text/javascript">
                                function fnPreviewImage(imgSrc) {
                                    var objToDisplay = '<img src="' + imgSrc + '" border="0" width="300" />';
                                    document.getElementById('divMainPhoto').innerHTML = objToDisplay;
                                }
                                function fnDefaultImage() {
                                    document.getElementById('divMainPhoto').innerHTML = document.getElementById('imgLarge-location-pool').innerHTML;
                                }
                            </script>
                            <script type="text/javascript">
                            $(document).ready( function () {
                                var dateString = '<%=MonthName(Month(itemExpirationDate)) & " " & Day(itemExpirationDate) & ", " & Year(itemExpirationDate) & " " & FormatDateTime(itemExpirationDate,3)%>';
                                var now = new Date();
                                var SecDiff = Math.floor((Date.parse(dateString) - now.getTime())/1000);
                                CreateTimer("divCountdownDigits", SecDiff);
                            });
                            </script>
					<%
					''''''''''''''''' Start Thumbnails '''''''''''''''''
					set fs = CreateObject("Scripting.FileSystemObject")
					a = 0
					for iCount = 0 to 2
						path = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".jpg")
						if fs.FileExists(Server.MapPath(path)) then
							a = a + 1
							src = replace(path,".jpg","_thumb.jpg")
							set f1 = fs.getFile(server.MapPath(path))
							if fs.FileExists(Server.MapPath(src)) then
								set f2 = fs.getFile(server.MapPath(src))
							else
								set f2 = fs.getFile(server.MapPath(path))
							end if
							if fs.FileExists(Server.MapPath(src)) and f1.DateLastModified < f2.DateLastModified then
								strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('" & path & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
							else
								session("errorSQL") = "filePath:" & Server.MapPath(path)
								if fs.FileExists(Server.MapPath(path)) then
									jpeg.Open Server.MapPath(path)
									jpeg.Height = 40
									jpeg.Width = 40
									jpeg.Save Server.MapPath(src)
									strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('" & path & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
									response.Write("<!-- make new alt image:" & a & " -->")
								end if
							end if
						end if
						path = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".gif")
						if fs.FileExists(Server.MapPath(path)) then
							a = a + 1
							src = path
							strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('" & src & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
						end if
						path = "/productpics/AltViews/" & replace(itempic,".gif","-" & iCount & ".jpg")
						if fs.FileExists(Server.MapPath(path)) then
							a = a + 1
							src = path
							strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('" & src & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
						end if
						path = "/productpics/AltViews/" & replace(itempic,".gif","-" & iCount & ".gif")
						if fs.FileExists(Server.MapPath(path)) then
							a = a + 1
							src = path
							strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('" & src & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
						end if
					next
					if typeID = 17 then
						a = a + 1
						if vendor = "DS" then
							strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/decal_pic5.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/decal_pic5.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
						else
							strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/gg_atl_image1.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/gg_atl_image1.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
						end if
					end if
					if vendor = "DS" then
						strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/decalskin-mg.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/decalskin-mg.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
					end if
					if musicSkins = 1 then
						a = a + 1
						strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/ms_altview1.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/ms_altview1.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
						a = a + 1
						strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/ms_altview2.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/ms_altview2.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
					end if
					if isnull(showAnimation) then showAnimation = true
					if (lCase(left(partnumber,3)) = "fp2" or lCase(left(partnumber,3)) = "fp3") and showAnimation then
						a = a + 1
						strAltImage = strAltImage & "<a class=""lnkAltimage"" href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/FP-SNAP-ON-ANIM.gif');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/FP-SNAP-ON-ANIM_icon.gif"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
						strAltImage = strAltImage & "<script language=""javascript"">" & vbcrlf
						strAltImage = strAltImage & "if (document.images) {" & vbcrlf
						strAltImage = strAltImage & "newLargeImg = new Image();" & vbcrlf
						strAltImage = strAltImage & "newLargeImg.src = '/productpics/AltViews/FP-SNAP-ON-ANIM.gif'"
						strAltImage = strAltImage & "}" & vbcrlf
						strAltImage = strAltImage & "</script>" & vbcrlf
					end if
					
					if strAltImage <> "" then strAltImage = "<p class=""altImgText"" style=""display:none;"">ROLLOVER ANY IMAGE TO VIEW FULL-SIZE</p>" & strAltImage
					
					response.write strAltImage
					''''''''''''''''' End Thumbnails '''''''''''''''''
					%>
                        </div>
                        <% if itemQty < 1 then %>
                    	<div id="soldOutHover"><h2>Sorry, This Deal is Sold Out!</h2><div>Next deal available in: <span id="divSecondaryTimer_0" class="divSecondaryTimer"></span></div></div>
                        <% end if %>
                    </div>
                    <div id="divDescContainer">
                    	<div id="divStarRating">
						<% 
							'Star Rating / Reviews Average
							sql =	"select		sum (rating) as ratings, COUNT (*) as rCount, (sum (rating) / COUNT (*)) as avg " & vbcrlf & _
									"from		we_Reviews " & vbcrlf & _
									"where		itemID = " & itemID & " " & vbcrlf & _
									"	and		approved = 1"
							session("errorSQL") = sql
							set	st = oConn.execute(SQL)
	
							avgRatings = st("ratings")
							avgCount = st("rCount")
							avgRating = st("avg")
							if isNumeric(avgRating) then
								if (Round(avgRating, 1) - Int(Round(avgRating, 1))) >= 0.5 then halfStar = true else halfStar = false end if
							else
								halfStar = false
							end if
							
							for i = 1 to 5 
							%>
                            <span class="star <%if i <= avgRating then%>full<%else%><%if halfStar and i = (Int(avgRating) + 1) then%>half<%else%>empty<%end if%><%end if%>"></span>
							<% 
							next 
							%> 
                            <span class="star sup"><a href="javascript:showReviews();" class="<%if isnull(avgRating) then%>hide<%end if%>">Read Reviews</a></span>
                        </div>
                        <div id="divRoundedBox">
                        	<div id="divInvBlow">Inventory Blowout Price:</div>
                            <div id="divPriceBlowout"><%=formatcurrency(itemPriceDeal)%></div>
                            <div id="divFreeSameDayShipping">- Free Same Day Shipping -</div>
                            <div id="divTheNumbers">
                            	<div class="third" id="n1">
                                	<div class="title">Value:</div>
                                    <div class="price"><%=formatcurrency(itemPriceRetail)%></div>
                                </div>
                                <div class="divider"><div>&nbsp;</div><div>&nbsp;</div></div>
                            	<div class="third" id="n2">
                                	<div class="title">Discount:</div>
                                    <div class="price"><%=Round((1-(itemPriceDeal/itemPriceRetail))*100)%>%</div>
                                </div>
                                <div class="divider"><div>&nbsp;</div><div>&nbsp;</div></div>
                            	<div class="third" id="n3">
                                	<div class="title">Savings:</div>
                                    <div class="price"><%=formatcurrency(itemPriceRetail-itemPriceDeal)%></div>
                                </div>
                            </div>
                            <form name="frmSubmit" action="/cart/item_add.asp" method="post" style="margin:0px; padding:0px;">
                            <div id="divQty">
                            	Quantity <input type="text" name="qty" id="qty" value="1" maxlength="3" />
                            </div>
                            <div id="divAddToCart">
								<%if itemQty > 0 then%>
                                <!--
                                <div id="divAddToCartButton" class="addCartButtonINS clickable" onclick="document.frmSubmit.submit();"></div>
                                -->
                                <div id="divAddToCartButton" class="addCartButtonINS clickable" onclick="br_cartTracking('add', '<%=itemid%>', '<%=productColor%>', '<%=replace(itemDesc, "'", "\'")%>', '<%=itemPriceDeal%>'); popUpCartWithPromo_add(<%=itemID%>,document.frmSubmit.qty.value,<%=itemPriceDeal%>);"></div>
                                <%else%>
                                <div id="divAddToCartButton" class="addCartButtonOOS"></div>
								<%end if%>
                                <div id="divLimitedQty">
                                    <% if itemQty > 0 then%>
                                    <h2>Limited Quantity - Order Now!</h2>
                                    <div id="QtyContainer">Only <div id="varQtyRemaining" class="varQtyINS"><%=itemQty%></div> remaining at this price.</div>
                                    <% else %>
                                    <h2>Next Deal Begins Soon!</h2>
                                    <div id="QtyContainer"><span id="varQtyRemaining" class="varQtyOOS">0</span> remaining at this price.</div>
                                    <% end if %>
                                </div>
                            </div>
                            <input type="hidden" name="musicSkins" value="0" />
                    		<input type="hidden" name="prodid" value="<%=itemID%>" />
                            <input type="hidden" name="itemPrice" value="<%=itemPriceDeal%>" />
                            </form>
                        </div>
                    </div>
                </div>
                <div id="divFeaturedItemDetailsContainer">
                	<div id="divButtonShowToggle">Show Details</div>
                </div>
                <div id="divExpandedDetails">
					<div id="divExpandedDetailsContainer">
                        <div id="divProductFeatures">
                            <h2>Product Features</h2>
                            <div class="hrMini"></div>
                            <ul>
                                <%if itemBullet1 <> "" then %><li><%=itemBullet1%></li><% end if %>
                                <%if itemBullet2 <> "" then %><li><%=itemBullet2%></li><% end if %>
                                <%if itemBullet3 <> "" then %><li><%=itemBullet3%></li><% end if %>
                                <%if itemBullet4 <> "" then %><li><%=itemBullet4%></li><% end if %>
                                <%if itemBullet5 <> "" then %><li><%=itemBullet5%></li><% end if %>
                                <%if itemBullet6 <> "" then %><li><%=itemBullet6%></li><% end if %>
                                <%if itemBullet7 <> "" then %><li><%=itemBullet7%></li><% end if %>
                                <%if itemBullet8 <> "" then %><li><%=itemBullet8%></li><% end if %>
                                <%if itemBullet9 <> "" then %><li><%=itemBullet9%></li><% end if %>
                                <%if itemBullet10 <> "" then %><li><%=itemBullet10%></li><% end if %>
                            </ul>
                        </div>
                        <div id="divProductDetails">
                            <h2>Product Details</h2>
                            <div class="hrMini"></div>
                            <div id="expDesc">
                                <p><%=itemLongDetail%></p>
                            </div>
                        </div>
                        <div id="divProductReviews">
                            
                            <%
							if itemID <> "" then
								'Star Rating / Review Single
								sql =	"select		top 3 rating, DateTimeEntd, reviewTitle, review, nickname--, * " & vbcrlf & _
										"from		we_Reviews " & vbcrlf & _
										"where		itemID = " & itemID & " " & vbcrlf & _
										"	and		approved = 1 " & vbcrlf & _
										"order by	id desc "
								session("errorSQL") = sql
'								response.write "<pre>" & sql & "</pre>" : response.end
								set	st = oConn.execute(SQL)
								
								if not st.eof then
									do until st.eof
									%>
									<h2>Product Reviews</h2>
									<div class="hrMini"></div>
									<%
										reviewRating = st("rating")
										reviewDate = st("DateTimeEntd")
										reviewTitle = st("reviewTitle")
										review = st("review")
										reviewNickName = st("nickname")
									%>
									<div class="fDate"><%=MonthName(Month(reviewDate)) & " " & Day(reviewDate) & ", " & Year(reviewDate) %></div>
									<div id="divSquareStars"><% for i = 1 to 5 %><span class="squareStar <%if i <= reviewRating then%>ssFull<%else%>ssEmpty<%end if%>"></span><% next %></div>
									<div id="divReviewContainer">
										<h3><%=reviewTitle%></h3>
										<p><%=review%>
										</p>
										<p><span id="revBy">Reviewed By:</span> <%=reviewNickName%></p>
									</div>
									<%
										st.movenext
									loop
								end if
							end if
							%>
                        </div>
                    </div>
                </div>
                <% rs.movenext %>
			<% else %>
           	<h1><%=notFound%></h1>
            <% end if %>
			<% else %>        
            <h1><%=notFound%></h1>
        	<% end if %>        
        </div>
        
    	<div id="divLeftColumn">
            <%
			if not displayMain then
				if itemReleaseDate = "" then
					dt = dateadd("h",1,now)
				else
					dt = itemReleaseDate
				end if
				%>
					<script type="text/javascript">
					/*
                    $(document).ready( function () {
                        var dateString = '<%=MonthName(Month(dt)) & " " & Day(dt) & ", " & Year(dt) & " " & FormatDateTime(dt,3)%>';
                        var now = new Date();
                        var SecDiff = Math.floor((Date.parse(dateString) - now.getTime())/1000);
                        CreateTimer("divCountdownDigits", SecDiff);
                    });
					*/
                    </script>
				<%
			end if
			%>
        	<div id="divCountdownArrow">
        		<% if displayMain then %>
                <h2>Current Deal Ends In:</h2>
                <% else %>
                <h2>Next Deal Begins In:</h2>
                <% end if %>
                <div id="divCountdownDigits">0h 0m 0s</div>
           	</div>
            <div id="divPeoplePurchased">
            	<% if displayMain then %>
                <script>$(document).ready(function(){$('#divPeoplePurchased').addClass('thumbUp');});</script>
				<%
				sql =	"select		count (*) as total " & vbcrlf & _
						"from		we_orderdetails " & vbcrlf & _
						"where		itemid = " & itemID & " " & vbcrlf & _
						"--	and		price = " & itemPriceDeal & " " & vbcrlf & _
						"--	and		entryDate > '" & itemReleaseDate & "' "
'				response.write "<pre>" & sql & "</pre>" : response.end
				session("errorSQL") = sql
				set mini = Server.CreateObject("ADODB.Recordset")
				mini = oConn.execute(SQL)
				varPeoplePurchased = mini("total") + 13
				%>
                <strong><span id="varPeoplePurchased"><%=varPeoplePurchased%></span> people</strong> have purchased this deal.
                <% else %>
                <strong><span id="varPeoplePurchased" class="hide">&nbsp;</span></strong>
                <% end if %>
            </div>
            <div id="divShareThisDeal">
            	<div>Share This Deal:</div>
                <div>
                	<a href="http://twitter.com/share/" target="_blank"><div id="divTwit" style="float:left;"></div></a>
                	<a href="https://www.facebook.com/WirelessEmporium" target="_blank"><div id="divFace" style="float:left;"></div></a>
                	<a href="https://pinterest.com/wirelessemp/" target="_blank"><div id="divPint" style="float:left;"></div></a>
                </div>
            </div>

        	<% if not rs.eof then %>

            <div id="divUpcomingDeals">
            	<h2>Today's Upcoming Deals:</h2>
                <% i = 1 %>
				<% do until rs.eof or i > 2 'Only perform iteration 2 times max %>
				<% 'for i = 1 to 2 %>
                <%
					itemID = rs("itemID")
					itemDesc = rs("itemDesc")
					itemPic = rs("itemPic")
					itemPriceOur = rs("price_Our")
					itemPriceRetail = rs("price_Retail")
					itemPriceDeal = rs("price")
					itemBullet1 = rs("bullet1")
					itemBullet2 = rs("bullet2")
					itemBullet3 = rs("bullet3")
					itemBullet4 = rs("bullet4")
					itemBullet5 = rs("bullet5")
					itemBullet6 = rs("bullet6")
					itemBullet7 = rs("bullet7")
					itemBullet8 = rs("bullet8")
					itemBullet9 = rs("bullet9")
					itemBullet10 = rs("bullet10")
					itemLongDetail = rs("itemLongDetail")
					itemReleaseDate = rs("releaseDate")
					itemExpirationDate = rs("expirationDate")
					partNumber = rs("partNumber")
					showAnimation = rs("showAnimation")
					itemQty = rs("inv_qty")
				%>
            	<div class="upcomingItemContainer">
                	<div class="img" style="background:url(/productpics/thumb/<%=itemPic%>) no-repeat; width:110px; height:110px;"></div>
                    <div class="desc"><%=itemDesc%></div>
                    <div class="priceDeal"><%'=formatcurrency(itemPriceDeal)%>$?</div>
                    <div class="priceRetail"><%=formatcurrency(itemPriceRetail)%></div>
                	<div class="upcomingItemHoverContainer" id="upcomingItemHoverContainer_<%=i%>">
                    	<h2>Sorry, This Deal is Still Locked!</h2><div>Available in: <span id="divSecondaryTimer_<%=i%>" class="divSecondaryTimer"></span></div>
					</div>
                </div>
                <%
				if i = 2 then 'this is the second item in the upcoming list and it needs its own timer
					dt = itemReleaseDate
					%>
						<script type="text/javascript">
						$(document).ready( function () {
							var dateString = '<%=MonthName(Month(dt)) & " " & Day(dt) & ", " & Year(dt) & " " & FormatDateTime(dt,3)%>';
							var now = new Date();
							var SecDiff = Math.floor((Date.parse(dateString) - now.getTime())/1000);
							CreateTimer2('divSecondaryTimer_'+'<%=i%>', SecDiff);
						});
						</script>
					<%
				end if
				%>
                <% i = i + 1 %>
				<% 'next %>
                <% rs.movenext %>
                <% loop %>
            </div>
            
            <div id="divExtremeDeals">
            	<h2><%=projHeaderFrequency%></h2>
                <div>Check back often for maximum savings!</div>
            </div>
            <% else %>
            <div style="text-align:center; padding-top:200px;">
            	
            </div>
            <% end if %>
            
        </div>
        
    </div>

	<div class="hr"></div>

	<div id="divLowerPortion">

        <div class="divOtherProducts">
            <div class="divSectionTitle">
            	<h2><span><%=projHeaderTextTerm1%></span> <%=projHeaderTextDefinition1%></h2>
            </div>
            <div class="divOnSale">
                <%
				sql = 	"select		* " & vbcrlf &_
						"from		lightningDeal_Categories " & vbcrlf &_
						"where		projectID = " & projID & " and isActive = 1 " & vbcrlf &_
						"order by	sortOrder"
'				response.write "<pre>" & sql & "</pre>" : response.end
				session("errorSQL") = sql
				set	rs = oConn.execute(SQL)
				
				%>
				<% counter = 1 %>
                <% if not rs.eof then %>
                <% do while not rs.eof %>
                <%
				catID = rs("id")
				catName = rs("name")
				catPic = rs("pic")
				catBullet1 = rs("bullet1")
				catBullet2 = rs("bullet2")
				catLink = rs("link")
'				if catName = "Sherwood" then response.write "catLink:" & catLink
				if isnull(catLink) then
					catLink = "/ldc-" & catID & "-" & formatSEO(catName) & ".asp"
				end if
				'catLink = Replace(catLink, "&amp;", "n")
				'catLink = Replace(catLink, "&", "n")
				'catLink = Replace(catLink, "&", "n")
				%>
                <div class="divOnSaleSingle">
                    <div class="divPhoto clickable" onclick="window.location='<%=catLink%>';" style="background-image:url(/images/lightningDeal/<%=catPic%>);"></div>
                    <div class="divTitle clickable" onclick="window.location='<%=catLink%>';"><%=catName%></div>
                    <div class="divExclamation"><%=catBullet1%></div>
                    <div class="divStartingAt"><%=catBullet2%></div>
                    <div class="divShopNowButton clickable" onclick="window.location='<%=CatLink%>';">Shop Now</div>
                </div>
                <% if counter mod 5 = 0 then %><div class="hr hrGrid"></div><% end if %>
                <% counter = counter + 1 %>
                <% rs.movenext %>
				<% loop %>
                <% end if%>
            </div>
        </div>
        
        <div class="divOtherProducts">
            <div class="divSectionTitle">
            	<h2><span><%=projHeaderTextTerm2%></span> <%=projHeaderTextDefinition2%></h2>
            </div>
            <div class="divOnSale">
                <%
				sql = "select		a.itemID, a.itemDesc, a.itemPic, a.price_Our, a.price_Retail " & vbcrlf &_
						"from		lightningDeal_RegItems li left join we_Items a " & vbcrlf &_
						"		on	li.itemID = a.itemID " & vbcrlf &_
						"where		projectid = " & projID & " and isActive = 1 " & vbcrlf &_
						"order by	sortOrder"
'				response.write "<pre>" & sql & "</pre>" : response.end
				session("errorSQL") = sql
				set	rs = oConn.execute(SQL)
				%>
				<% counter = 1 %>
				<% if not rs.eof then %>
                <% do while not rs.eof %>
				<%
				regItemId = prepInt(rs("itemID"))
				regItemDesc = rs("itemDesc")
				regItemPic = rs("itemPic")
				regItemPriceOur = prepInt(rs("price_Our"))
				regItemPriceRetail = prepInt(rs("price_Retail"))
				%>
                <div class="divOnSaleSingle">
                    <div class="divPhoto clickable"onclick="window.location='/p-<%=regItemId%>-<%=formatSEO(regItemDesc)%>';">
                    	<img src="/productpics/big/<%=regItemPic%>" border="0" width="146" height="146" />
                    </div>
                    <div class="divTitle clickable" onclick="window.location='/p-<%=regItemId%>-<%=formatSEO(regItemDesc)%>';"><%=regItemDesc%></div>
                    <div class="divExclamation"><%=formatcurrency(regItemPriceOur)%></div>
                    <div class="divStartingAt strike">Reg. <%=formatcurrency(regItemPriceRetail)%></div>
                    <div class="divShopNowButton clickable" onclick="window.location='/p-<%=regItemId%>-<%=formatSEO(regItemDesc)%>';">View Details</div>
                </div>
                <% if counter mod 5 = 0 then %><div class="hr hrGrid"></div><% end if %>
                <% counter = counter + 1 %>
                <% rs.movenext %>
				<% loop %>
				<% end if %>
            </div>
        </div>
	
        <div class="clear"></div>

    </div>

   <div class="hide" id="forceRefresh"><%=forceRefresh%></div>
   <div class="hide" id="divRandom"><%randomize : lowNumber = 1 : highNumber = 300 : response.write Round((highNumber - lowNumber + 1) * Rnd + LowNumber)%></div>
</div>
<!--#include virtual="/includes/template/bottom.asp"-->

<% fCloseConn %>