<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
response.buffer = true
set fso = CreateObject("Scripting.FileSystemObject")

dim modelid
modelid = request.querystring("modelid")
if modelid = "" or not isNumeric(modelid) then
	call PermanentRedirect("/")
end if

dim unavailable, HandsfreeTypes, isTablet
unavailable = 0
isTablet = false

call fOpenConn()
sql	=	"select	a.modelid, a.modelname, a.modelimg, b.brandid, b.brandname, a.handsfreetypes, a.isTablet" & vbcrlf & _
		"from	we_models a with (nolock) join we_brands b with (nolock) " & vbcrlf & _
		"	on	a.brandid=b.brandid " & vbcrlf & _
		"where a.modelid = '" & modelid & "'" & vbcrlf

session("errorSQL") = SQL
'response.write "<pre>" & SQL & "</pre>"
set objRsName = oConn.execute(SQL)
if objRsName.eof then
	unavailable = 1
else
	'===========================================================================
	'======================== 		301 redirect		========================
	'===========================================================================
	link301 = "/sb-" & objRsName("brandid") & "-sm-" & objRsName("modelid") & "-sc-5-handsfree-" & formatSEO(objRsName("brandname")) & "-" & formatSEO(objRsName("modelname")) & ".asp"
	call PermanentRedirect(link301)	
	
	if isNull(objRsName("HandsfreeTypes")) then
		unavailable = 1
	else
		dim brandID, brandName, modelName, modelImg
		brandID = objRsName("brandID")
		brandName = objRsName("brandName")
		modelName = objRsName("modelName")
		modelImg = objRsName("modelImg")
		HandsfreeTypes = left(objRsName("HandsfreeTypes"),len(objRsName("HandsfreeTypes"))-1)
		isTablet = objRsName("isTablet")
		
'		SQL = "SELECT A.*, B.modelName, B.modelImg FROM we_items A INNER JOIN we_models B ON A.modelID = B.modelID" & vbcrlf
'		SQL = SQL & " WHERE A.HandsfreeType IN ('" & replace(HandsfreeTypes,",","','") & "')" & vbcrlf
'		SQL = SQL & " AND A.typeid = '5'" & vbcrlf
'		SQL = SQL & " AND A.hideLive = 0" & vbcrlf
'		SQL = SQL & " AND A.inv_qty <> 0" & vbcrlf
'		SQL = SQL & " AND A.price_Our IS NOT NULL AND A.price_Our > 0" & vbcrlf

		sql	=	"select	a.itemid, a.itemkit_new, a.itempic, a.itemdesc, a.inv_qty, a.price_our, a.price_retail" & vbcrlf & _
				"from	we_items a with (nolock)" & vbcrlf & _
				"where	a.handsfreetype in ('" & replace(HandsfreeTypes,",","','") & "')" & vbcrlf & _
				"	and a.typeid = '5'" & vbcrlf & _
				"	and a.hidelive = 0" & vbcrlf & _
				"	and a.inv_qty <> 0" & vbcrlf & _
				"	and a.price_our is not null and a.price_our > 0" & vbcrlf

		dim nSorting
		nSorting = request.form("nSorting")
		select case nSorting
			case 1 : SQL = SQL & " ORDER BY A.numberofsales DESC"
			case 2 : SQL = SQL & " ORDER BY A.price_Our DESC"
			case 3 : SQL = SQL & " ORDER BY A.price_Our"
			case 4 : SQL = SQL & " ORDER BY A.brandid, A.DatetimeEntd DESC"
			case 5 : SQL = SQL & " ORDER BY A.brandid, A.DateTimeEntd"
			case 6 : SQL = SQL & " ORDER BY A.itemDesc"
			case 7 : SQL = SQL & " ORDER BY A.itemDesc DESC"
			case else : SQL = SQL & " ORDER BY A.flag1 DESC"
		end select
		session("errorSQL") = SQL
'		response.write "<pre>" & SQL & "</pre>"
		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.open sql, oConn, 0, 1
		if RS.eof then unavailable = 1 end if
	end if
end if

objRsName.close
set objRsName = nothing

dim categoryName
categoryName = "Hands-Free Kits &amp; Bluetooth Headsets"

if unavailable = 1 then
	call fCloseConn()
	call PermanentRedirect("/")
end if

dim SEtitle, SEdescription, SEkeywords
if modelid = "987" or modelid = "988" or modelid = "1009" or modelid = "1015" then
	SEtitle = "Samsung " & modelName & " " & nameSEO(categoryName) & " ?Buy Exclusive Samsung " & modelName & " Accessories"
	SEdescription = "Buy Quality Samsung " & modelName & " " & nameSEO(categoryName) & " ?WirelessEmporium carries your favorite Samsung " & modelName & " " & singularSEO(categoryName) & " at discounted rates and Free Shipping!"
	SEkeywords = "samsung " & modelName & " " & nameSEO(categoryName) & ", samsung " & modelName & " " & singularSEO(categoryName) & ", " & modelName & " " & singularSEO(categoryName) & ", samsung accessories, samsung " & modelName & " cell phone " & nameSEO(categoryName) & ", " & nameSEO(categoryName) & " for samsung " & modelName
	h1 = "Samsung " & modelName & " " & nameSEO(categoryName)
	topText = "Wireless Emporium offers not only very competitive pricing on <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-" & modelid & "-cell-accessories-samsung-" & formatSEO(modelName) & ".asp"" title=""Samsung " & modelName & " Accessories"">Samsung " & modelName & " accessories</a> but also the widest selection of Samsung " & modelName & " phone accessories on the Internet. We pride ourselves on giving you a complete line of Samsung " & modelName & " Bluetooth Headsets & Handsfree Kits for you to accentuate and better utilize your phone. An increasing number of states are requiring that drivers use hands-free headsets while driving so they can concentrate on the road. The demand for Samsung bluetooth headsets and hands-free headsets has increased which means a better selection for you to choose from! We carry hands-free headsets for your Samsung " & modelName & " starting at under $10 and Bluetooth headsets for your Samsung " & modelName & " starting at under $20! If you're looking for high-end Bluetooth models, we've got those, too ?and they're up to 40% off retail! At Wireless Emporium, we offer only the best prices for Samsung " & modelName & " Bluetooth Headsets & Handsfree Kits because we know what's important to you. Thousands of customers trust us to offer low prices and top tier service, and our satisfaction guarantee will make you one of them!</p>"
	topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">Since 2001, Wireless Emporium has been offering <a class=""topText-link"" href=""http://www.wirelessemporium.com"" title=""Cell Phone Accessories"">cell phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best Samsung " & modelName & " Bluetooth headsets and hands-free headsets for Samsung " & modelName & " cell phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
end if

if SEtitle = "" then SEtitle = brandName & " " & modelName & " " & categoryName & " at Wholesale Prices"
if SEdescription = "" then SEdescription = "wirelessemporium.com is the largest store online for discounted " & brandName & " " & modelName & " " & categoryName & " & other " & brandName & " " & modelName & " accessories at wholesale prices."
if SEkeywords = "" then SEkeywords = brandName & " " & modelName & " Accessories, " & brandName & " " & modelName & " Cell Phone Accessories, " & brandName & " " & modelName & " " & categoryName & ", " & brandName & " " & modelName & " Cell Phone " & categoryName

dim modelLink
if modelID = 940 then
	modelLink = "/apple-ipad-accessories.asp"
else
	modelLink = "/T-" & modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
end if

if topText = "" then
	topText = "Wireless Emporium offers not only very competitive pricing on <a class=""topText-link"" href=""" & modelLink & """ title=""" & brandName & " " & modelName & " Accessories"">" & brandName & " " & modelName & " accessories</a> but also the widest selection of " & brandName & " " & modelName & " phone accessories on the Internet. We pride ourselves on giving you a complete line of " & brandName & " " & modelName & " hands-free devices for you to accentuate and better utilize your phone. An increasing number of states are requiring that drivers use hands-free headsets while driving so they can concentrate on the road. The demand for <a class=""topText-link"" href=""http://www.wirelessemporium.com/sc-5-sb-" & brandID & "-" & formatSEO(brandName) & "-headsets-hands-free-kits.asp"" title=""" & brandName & " " & modelName & " Bluetooth Headsets"">" & brandName & " bluetooth headsets</a> and hands-free headsets has increased which means a better selection for you to choose from! We carry hands-free headsets for your " & brandName & " " & modelName & " starting at under $10 and Bluetooth headsets for your " & brandName & " " & modelName & " starting at under $20! If you're looking for high-end Bluetooth models, we've got those, too ?and they're up to 40% off retail! At Wireless Emporium, we offer only the best prices for " & brandName & " " & modelName & " hands-free devices because we know what's important to you. Thousands of customers trust us to offer low prices and top tier service, and our satisfaction guarantee will make you one of them!</p>"
	topText = topText & "<p class=""topText"">Since 2001, Wireless Emporium has been offering <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Quality " & brandName & " Accessories"">quality cell phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best " & brandName & " " & modelName & " Bluetooth headsets and hands-free headsets for " & brandName & " " & modelName & " cell phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
end if

dim strBreadcrumb
strBreadcrumb = brandName & " " & modelName & " " & categoryName
%>
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal" style="padding-top:8px;">
        	<%
			if isTablet then
			%>
	            <a class="breadcrumb" href="/tablet-ereader-accessories.asp">Tablet Accessories</a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="<%=modelLink%>"><%=brandName & " " & modelName%> Accessories</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
            <%
			else
			%>
	            <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="/<%=brandSEO(brandID)%>"><%=brandName%> Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="<%=modelLink%>"><%=brandName & " " & modelName%> Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>            
            <%
			end if
			%>
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle" width="100%">
            <%
            if h1 <> "" then
                response.write "<h1>" & h1 & "</h1>"
            else
                response.write "<h1>" & brandName & " " & modelName & " " & categoryName & "</h1>"
            end if
            %>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="748">
            <table border="0" cellspacing="0" cellpadding="7">
                <tr>
                    <td width="200" align="center" valign="middle"><img src="productpics/models/<%=modelImg%>" border="0" alt="<%=brandName & " " & modelName & " " & nameSEO(categoryName)%>"></td>
                    <td width="548" valign="top" class="cate-pro-border">
                        <p class="topText"><%=topText%></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal"><img src="/images/spacer.gif" width="1" height="10" border="0"></td>
    </tr>
    <form name="sortForm" method="post" action="<%=Request.ServerVariables("HTTP_X_REWRITE_URL")%>">
    <tr>
        <td align="right" valign="top" width="748">
            <input type="hidden" name="modelid" value="<%=modelid%>">
            Sort By:
            <select name="nSorting" onChange="this.form.submit();" style="background-color:#e6f5fd;">
                <option value="0"<%if nSorting = "0" or nSorting = "" then response.write " selected"%>>Default</option>
                <option value="1"<%if nSorting = "1" then response.write " selected"%>>Best Selling</option>
                <option value="2"<%if nSorting = "2" then response.write " selected"%>>Price: High to Low</option>
                <option value="3"<%if nSorting = "3" then response.write " selected"%>>Price: Low to High</option>
                <option value="4"<%if nSorting = "4" then response.write " selected"%>>Product: Newest to Oldest</option>
                <option value="5"<%if nSorting = "5" then response.write " selected"%>>Product: Oldest to Newest</option>
                <option value="6"<%if nSorting = "6" then response.write " selected"%>>Product Name: A - Z</option>
                <option value="7"<%if nSorting = "7" then response.write " selected"%>>Product Name: Z - A</option>
            </select>
        </td>
    </tr>
    </form>
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal"><img src="/images/spacer.gif" width="1" height="10" border="0"></td>
    </tr>
    <tr>
        <td>
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="748">
                <tr>
                    <%
                    a = 0
                    dim DoNotDisplay, RSkit
                    do until RS.eof
                        DoNotDisplay = 0
                        if not isNull(RS("ItemKit_NEW")) then
                            SQL = "SELECT inv_qty FROM we_Items with (nolock) WHERE itemID IN (" & RS("ItemKit_NEW") & ")"
                            set RSkit = oConn.execute(SQL)
                            do until RSkit.eof
                                if RSkit("inv_qty") < 1 then DoNotDisplay = 1
                                RSkit.movenext
                            loop
                            RSkit.close
                            set RSkit = nothing
                        end if
                        if not fso.fileExists(server.MapPath("/productpics/thumb/" & RS("itemPic"))) then DoNotDisplay = 1
                        if DoNotDisplay = 0 then
                            dim altText
                            if modelid = "987" or modelid = "988" or modelid = "1009" or modelid = "1015" then
                                altText = RS("itemDesc")
                            else
                                altText = brandName & " " & modelName & " " & singularSEO(categoryName) & " ?" & RS("itemDesc")
                            end if
                            response.write "<td align=""center"" valign=""top"" width=""170"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""170"" height=""100%"">" & vbcrlf
                            response.write "<tr><td align=""center"" valign=""middle"" width=""170"" height=""154""><a href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp""><img src=""/productpics/thumb/" & RS("itemPic") & """ border=""0"" alt=""" & altText & """></a></td></tr>" & vbcrlf
                            response.write "<tr><td align=""center"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf

                            if 0 = RS("inv_qty") then
                                response.write "<tr><td align=""center"" valign=""top"" height=""108"" rowspan=""3""><img src=""/images/sold_out.png"" width=""85""></td></tr>" & vbcrlf
                            else
                                response.write "<tr><td align=""center"" valign=""top"" height=""50""><a class=""cellphone-link"" href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"" title=""" & altText & """>" & RS("itemDesc") & "</a></td></tr>" & vbcrlf
                                response.write "<tr><td align=""center"" valign=""top"" height=""8""><img src=""/images/spacer.gif"" width=""1"" height=""8""></td></tr>" & vbcrlf
                                response.write "<tr><td align=""center"" valign=""bottom"" height=""50""><span class=""boldText"">Our&nbsp;Price:&nbsp;</span><span class=""pricing-orange2"">" & formatCurrency(RS("price_Our")) & "</span><br><span class=""pricing-gray2"">List&nbsp;Price:&nbsp;<s>" & formatCurrency(RS("price_Retail")) & "</s></span><br><span class=""pricing-gray2"">You&nbsp;Save:&nbsp;" & formatCurrency(RS("price_Retail") - RS("price_Our")) & "</span></td></tr>" & vbcrlf
                            end if 
                                                                                            
                            response.write "</table></td>" & vbcrlf
                            a = a + 1
                            if a = 4 then
                                response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""748"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
                                a = 0
                            else
                                response.write "<td width=""10"" align=""center"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                            end if
                        end if 
                        RS.movenext
                    loop
                    if a = 1 then
                        response.write "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" & vbcrlf
                    elseif a = 2 then
                        response.write "<td>&nbsp;</td><td>&nbsp;</td>" & vbcrlf
                    elseif a = 3 then
                        response.write "<td>&nbsp;</td>" & vbcrlf
                    end if
                    %>
                </tr>
            </table>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->