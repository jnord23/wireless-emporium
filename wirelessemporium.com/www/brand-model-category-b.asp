<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
response.buffer = true

pageName = "bmc-b"
dim basePageName : basePageName = "BMC"

dim productListingPage : productListingPage = 1
googleAds = 1
leftGoogleAd = 1
nItemDescMaxLength = 85

dim modelid, categoryid, isTablet
isTablet = false

brandID = prepInt(request.querystring("brandID"))
modelID = prepInt(request.querystring("modelID"))
categoryID = prepInt(request.querystring("categoryID"))

response.Cookies("saveBrandID") = 0
response.Cookies("saveModelID") = modelID
response.Cookies("saveItemID") = 0

musicSkins = request.querystring("musicSkins")
musicSkinGenre = request.querystring("musicSkinGenre")
musicSkinArtistID = request.querystring("musicSkinArtist")

if instr(musicSkins,",") > 0 then
	musicSkinsArray = split(musicSkins,",")
	musicSkins = musicSkinsArray(0)
end if
if instr(musicSkinGenre,",") > 0 then
	musicSkinGenreArray = split(musicSkinGenre,",")
	musicSkinGenre = musicSkinGenreArray(0)
end if
session("errorSQL2") = musicSkins

if isnull(brandID) or len(brandID) < 1 then brandID = 0
if isnull(modelID) or len(modelID) < 1 then modelID = 0
if isnull(categoryID) or len(categoryID) < 1 then categoryID = 0
if isnull(musicSkins) or len(musicSkins) < 1 then musicSkins = 0
if musicSkinGenre = "r-b" then musicSkinGenre = "R&B"
if musicSkinGenre = "tv-movies" then musicSkinGenre = "TV/Movies"
if musicSkinGenre = "screen-protectors" then musicSkinGenre = "Screen Protectors"
if len(musicSkinGenre) > 0 then
	musicSkinGenre = ucase(left(musicSkinGenre,1)) & right(musicSkinGenre,len(musicSkinGenre)-1)
end if
if instr(REWRITE_URL, "apple-iphone") > 0 then
	phoneOnly = 1
else
	phoneOnly = 0
end if

if (brandID = 0 or modelID = 0) and (categoryID <> 8 and categoryid <> 15) then call PermanentRedirect("/?ec=bmc9001")
'if categoryid > 999 then
'	call PermanentRedirect(replace(REWRITE_URL, "-sc-", "-scd-"))
'end if

call fOpenConn()
sql = "select a.modelname, a.modelimg, b.brandname, a.isTablet, a.handsfreetypes, a.excludePouches, a.includeNFL, a.includeExtraItem from we_models a join we_brands b on a.brandid = b.brandid where a.modelid = '" & modelid & "'"
session("errorSQL") = sql
set nameRS = oConn.execute(sql)
if not nameRS.eof then
	modelName = nameRS("modelname")
	modelImg = nameRS("modelimg")
	brandName = nameRS("brandName")
	isTablet = nameRS("isTablet")
	excludePouches = nameRS("excludePouches")
	includeNFL = nameRS("includeNFL")
	includeExtraItem = nameRS("includeExtraItem")
	if not isnull(nameRS("HandsfreeTypes")) then HandsfreeTypes = left(nameRS("HandsfreeTypes"),len(nameRS("HandsfreeTypes"))-1)
end if
nameRS = null

if brandName = "" then
	sql = "select brandName from we_brands where brandid = '" & brandid & "'"	
	set rsTemp = oConn.execute(sql)
	if not rsTemp.eof then brandName = rsTemp("brandName")
end if
if modelImg = "" then modelImg = "mini-phones.jpg" end if

if categoryid = 3 or categoryid = 7 then
	numRowItems = 5
else
	numRowItems = 10
end if

if categoryid = 5 then
	sql	=	"select	x.itemid, x.itemdesc, x.itempic, isnull(x.price_our, 0.0) price_our, isnull(x.price_retail, 0.0) price_retail, x.flag1, x.partnumber" & vbcrlf & _
			"	,	x.subTypeOrderNum, x.typeid, x.subtypeid, x.typeName, x.typeNameSEO_WE" & vbcrlf & _
			"	,	x.typeOrderNum, x.subtypeName, x.subTypeNameSEO_WE, y.inv_qty, isnull(s.topText, '') subCatTopText, x.itempic defaultImg, 0 isMusicSkins, x.numberOfSales, 0 related" & vbcrlf & _
			"	,	x.master, x.colorSlaves, x.colorid, x.numAvailColors" & vbcrlf & _
			"from	we_subtypes s with (nolock) cross apply" & vbcrlf & _
			"		(	select	top " & numRowItems & " a.itemid, a.itemdesc, a.itempic, a.price_our, a.price_retail, a.flag1, a.partnumber" & vbcrlf & _
			"				,	v.subTypeOrderNum, v.typeid, v.subtypeid, v.typeName, v.typeNameSEO_WE" & vbcrlf & _
			"				,	v.orderNum typeOrderNum, v.subtypeName, v.subTypeNameSEO_WE, a.numberOfSales" & vbcrlf & _
			"				,	p.master, m.colorSlaves, a.colorid, isnull(m.cnt, 0) numAvailColors" & vbcrlf & _
			"			from	we_items a with (nolock) join v_subtypematrix v with (nolock)" & vbcrlf & _
			"				on	a.subtypeid = v.subtypeid left outer join v_colorMatrix m" & vbcrlf & _
			"				on	a.partnumber = m.master2 and m.hideLive = 0 left outer join xproductcolormatrix p" & vbcrlf & _
			"				on	a.partnumber = p.slave and p.hideLive = 0" & vbcrlf & _
			"			where	a.subtypeid = s.subtypeid" & vbcrlf & _
			"				and	v.typeid = '" & categoryid & "'" & vbcrlf & _
			"				and	a.handsfreetype in ('" & replace(HandsfreeTypes,",","','") & "')" & vbcrlf & _
			"				and a.price_our > 0 and a.hidelive = 0 and a.ghost = 0 --and (select top 1 inv_qty from we_Items where partNumber = a.partNumber and master = 1) > 0 " & vbcrlf & _
			"				and	(a.itempic is not null and a.itempic != '')" & vbcrlf & _
			"				and	((m.master is not null and p.slave is not null) or (m.master is null and p.slave is null))" & vbcrlf & _
			"			order by v.subTypeOrderNum, a.flag1 desc, a.itemid desc	) x join " & vbcrlf & _
			"		(	select	partnumber, isnull(max(inv_qty), 0) inv_qty" & vbcrlf & _
			"			from	we_items with (nolock)" & vbcrlf & _
			"			group by partnumber	) y	" & vbcrlf & _
			"	on	x.partnumber = y.partnumber	" & vbcrlf
elseif categoryid = 8 then
	sql = 	"select	x.itemid, x.itemdesc, x.itempic, isnull(x.price_our, 0.0) price_our, isnull(x.price_retail, 0.0) price_retail, x.flag1, x.partnumber" & vbcrlf & _
			"	,	x.subTypeOrderNum, x.typeid, x.subtypeid, x.typeName, x.typeNameSEO_WE" & vbcrlf & _
			"	,	x.typeOrderNum, x.subtypeName, x.subTypeNameSEO_WE, y.inv_qty, isnull(s.topText, '') subCatTopText, x.itempic defaultImg, 0 isMusicSkins, x.numberOfSales, 0 related" & vbcrlf & _
			"	,	x.master, x.colorSlaves, x.colorid, x.numAvailColors" & vbcrlf & _
			"from	we_subtypes s with (nolock) cross apply" & vbcrlf & _
			"		(	select	top " & numRowItems & " a.itemid, a.itemdesc, a.itempic, a.price_our, a.price_retail, a.flag1, a.partnumber" & vbcrlf & _
			"				,	v.subTypeOrderNum, v.typeid, v.subtypeid, v.typeName, v.typeNameSEO_WE" & vbcrlf & _
			"				,	v.orderNum typeOrderNum, v.subtypeName, v.subTypeNameSEO_WE, a.numberOfSales" & vbcrlf & _
			"				,	p.master, m.colorSlaves, a.colorid, isnull(m.cnt, 0) numAvailColors" & vbcrlf & _
			"			from	we_items a with (nolock) join v_subtypematrix v with (nolock)" & vbcrlf & _
			"				on	a.subtypeid = v.subtypeid left outer join v_colorMatrix m" & vbcrlf & _
			"				on	a.partnumber = m.master2 and m.hideLive = 0 left outer join xproductcolormatrix p" & vbcrlf & _
			"				on	a.partnumber = p.slave and p.hideLive = 0" & vbcrlf & _
			"			where	a.subtypeid = s.subtypeid" & vbcrlf & _
			"				and	v.typeid = '" & categoryid & "'" & vbcrlf & _
			"				and a.price_our > 0 and a.hidelive = 0 and a.ghost = 0 --and (select top 1 inv_qty from we_Items where partNumber = a.partNumber and master = 1) > 0 " & vbcrlf & _
			"				and	(a.itempic is not null and a.itempic != '')" & vbcrlf & _
			"				and	((m.master is not null and p.slave is not null) or (m.master is null and p.slave is null))" & vbcrlf & _
			"			order by v.subTypeOrderNum, a.flag1 desc, a.itemid desc	) x join " & vbcrlf & _
			"		(	select	partnumber, isnull(max(inv_qty), 0) inv_qty" & vbcrlf & _
			"			from	we_items with (nolock)" & vbcrlf & _
			"			group by partnumber	) y	" & vbcrlf & _
			"	on	x.partnumber = y.partnumber	" & vbcrlf
elseif categoryid = 15 then		
	sql = 	"select	x.itemid, x.itemdesc, x.itempic, isnull(x.price_our, 0.0) price_our, isnull(x.price_retail, 0.0) price_retail, x.flag1, x.partnumber" & vbcrlf & _
			"	,	x.subTypeOrderNum, x.typeid, x.subtypeid, x.typeName, x.typeNameSEO_WE" & vbcrlf & _
			"	,	x.typeOrderNum, x.subtypeName, x.subTypeNameSEO_WE, y.inv_qty, isnull(s.topText, '') subCatTopText, x.itempic defaultImg, 0 isMusicSkins, x.numberOfSales, x.related" & vbcrlf & _
			"	,	x.master, x.colorSlaves, x.colorid, x.numAvailColors" & vbcrlf & _
			"from	we_subtypes s with (nolock) cross apply" & vbcrlf & _
			"		(	select	top " & numRowItems & " *" & vbcrlf & _
			"			from	(	select	a.itemid, a.itemdesc, a.itempic, a.price_our, a.price_retail, a.flag1, a.partnumber" & vbcrlf & _
			"							,	v.subTypeOrderNum, v.typeid, v.subtypeid, v.typeName, v.typeNameSEO_WE" & vbcrlf & _
			"							,	v.orderNum typeOrderNum, v.subtypeName, v.subTypeNameSEO_WE, a.numberOfSales, 0 related" & vbcrlf & _
			"							,	p.master, m.colorSlaves, a.colorid, isnull(m.cnt, 0) numAvailColors" & vbcrlf & _			
			"						from	we_items a with (nolock) join v_subtypematrix v with (nolock)" & vbcrlf & _
			"							on	a.subtypeid = v.subtypeid left outer join v_colorMatrix m" & vbcrlf & _
			"							on	a.partnumber = m.master2 and m.hideLive = 0 left outer join xproductcolormatrix p" & vbcrlf & _
			"							on	a.partnumber = p.slave and p.hideLive = 0" & vbcrlf & _
			"						where	a.brandid = '" & brandid & "'" & vbcrlf & _
			"							and	v.typeid = '" & categoryid & "'" & vbcrlf & _
			"							and a.price_our > 0 and a.hidelive = 0 and a.ghost = 0 --and (select top 1 inv_qty from we_Items where partNumber = a.partNumber and master = 1) > 0 " & vbcrlf & _
			"							and	(a.itempic is not null and a.itempic != '')" & vbcrlf & _
			"							and	((m.master is not null and p.slave is not null) or (m.master is null and p.slave is null))" & vbcrlf & _
			"						union	" & vbcrlf & _
			"						select	a.itemid, a.itemdesc, a.itempic, a.price_our, a.price_retail, a.flag1, a.partnumber" & vbcrlf & _
			"							,	v.subTypeOrderNum, v.typeid, v.subtypeid, v.typeName, v.typeNameSEO_WE" & vbcrlf & _
			"							,	v.orderNum typeOrderNum, v.subtypeName, v.subTypeNameSEO_WE, a.numberOfSales, 1 related" & vbcrlf & _
			"							,	p.master, m.colorSlaves, a.colorid, isnull(m.cnt, 0) numAvailColors" & vbcrlf & _
			"						from	we_items a with (nolock) join we_subRelatedItems b" & vbcrlf & _
			"							on	a.itemid = b.itemid join v_subtypematrix v with (nolock)" & vbcrlf & _
			"							on	b.subtypeid = v.subtypeid left outer join v_colorMatrix m" & vbcrlf & _
			"							on	a.partnumber = m.master2 and m.hideLive = 0 left outer join xproductcolormatrix p" & vbcrlf & _
			"							on	a.partnumber = p.slave and p.hideLive = 0" & vbcrlf & _
			"						where	b.brandid = '" & brandid & "'" & vbcrlf & _
			"							and	v.typeid = '" & categoryid & "'" & vbcrlf & _
			"							and a.price_our > 0 and a.hidelive = 0 and a.ghost = 0 --and (select top 1 inv_qty from we_Items where partNumber = a.partNumber and master = 1) > 0" & vbcrlf & _
			"							and	(a.itempic is not null and a.itempic != '')" & vbcrlf & _
			"							and	((m.master is not null and p.slave is not null) or (m.master is null and p.slave is null))" & vbcrlf & _
			"					) a" & vbcrlf & _
			"			where	a.subtypeid = s.subtypeid" & vbcrlf & _
			"			order by a.subTypeOrderNum, a.flag1 desc, a.itemid desc	) x join " & vbcrlf & _
			"		(	select	partnumber, isnull(max(inv_qty), 0) inv_qty" & vbcrlf & _
			"			from	we_items with (nolock)" & vbcrlf & _
			"			group by partnumber	) y	" & vbcrlf & _
			"	on	x.partnumber = y.partnumber	" & vbcrlf
else
	sql = 	"select	x.itemid, x.itemdesc, x.itempic, isnull(x.price_our, 0.0) price_our, isnull(x.price_retail, 0.0) price_retail, x.flag1, x.partnumber" & vbcrlf & _
			"	,	x.subTypeOrderNum, x.typeid, x.subtypeid, x.typeName, x.typeNameSEO_WE" & vbcrlf & _
			"	,	x.typeOrderNum, x.subtypeName, x.subTypeNameSEO_WE, y.inv_qty, isnull(s.topText, '') subCatTopText, x.itempic defaultImg, 0 isMusicSkins, x.numberOfSales, x.related" & vbcrlf & _
			"	,	x.master, x.colorSlaves, x.colorid, x.numAvailColors" & vbcrlf & _
			"from	we_subtypes s with (nolock) cross apply" & vbcrlf & _
			"		(	select	top " & numRowItems & " *" & vbcrlf & _
			"			from	(	select	a.itemid, a.itemdesc, a.itempic, a.price_our, a.price_retail, a.flag1, a.partnumber" & vbcrlf & _
			"							,	v.subTypeOrderNum, v.typeid, v.subtypeid, v.typeName, v.typeNameSEO_WE" & vbcrlf & _
			"							,	v.orderNum typeOrderNum, v.subtypeName, v.subTypeNameSEO_WE, a.numberOfSales, 0 related" & vbcrlf & _
			"							,	p.master, m.colorSlaves, a.colorid, isnull(m.cnt, 0) numAvailColors" & vbcrlf & _
			"						from	we_items a with (nolock) join v_subtypematrix v with (nolock)" & vbcrlf & _
			"							on	a.subtypeid = v.subtypeid left outer join v_colorMatrix m" & vbcrlf & _
			"							on	a.partnumber = m.master2 and m.hideLive = 0 left outer join xproductcolormatrix p" & vbcrlf & _
			"							on	a.partnumber = p.slave and p.hideLive = 0" & vbcrlf & _
			"						where	a.modelid = '" & modelid & "'" & vbcrlf
	if categoryid = 3 or categoryid = 7 then
		sql = sql & "							and	v.typeid in (3,7)" & vbcrlf
	else
		sql = sql & "							and	v.typeid = '" & categoryid & "'" & vbcrlf
	end if
	sql = sql & "							and a.price_our > 0 and a.hidelive = 0 and a.ghost = 0 --and (select top 1 inv_qty from we_Items where partNumber = a.partNumber and master = 1) > 0 " & vbcrlf & _
			"								and	(a.itempic is not null and a.itempic != '')" & vbcrlf & _
			"								and	((m.master is not null and p.slave is not null) or (m.master is null and p.slave is null))" & vbcrlf & _
			"						union	" & vbcrlf & _
			"						select	a.itemid, a.itemdesc, a.itempic, a.price_our, a.price_retail, a.flag1, a.partnumber" & vbcrlf & _
			"							,	v.subTypeOrderNum, v.typeid, v.subtypeid, v.typeName, v.typeNameSEO_WE" & vbcrlf & _
			"							,	v.orderNum typeOrderNum, v.subtypeName, v.subTypeNameSEO_WE, a.numberOfSales, 1 related" & vbcrlf & _
			"							,	p.master, m.colorSlaves, a.colorid, isnull(m.cnt, 0) numAvailColors" & vbcrlf & _
			"						from	we_items a with (nolock) join we_subRelatedItems b" & vbcrlf & _
			"							on	a.itemid = b.itemid join v_subtypematrix v with (nolock)" & vbcrlf & _
			"							on	b.subtypeid = v.subtypeid left outer join v_colorMatrix m" & vbcrlf & _
			"							on	a.partnumber = m.master2 and m.hideLive = 0 left outer join xproductcolormatrix p" & vbcrlf & _
			"							on	a.partnumber = p.slave and p.hideLive = 0" & vbcrlf & _
			"						where	b.modelid = '" & modelid & "'" & vbcrlf
	if categoryid = 3 or categoryid = 7 then
		sql = sql & "							and	v.typeid in (3,7)" & vbcrlf
	else
		sql = sql & "							and	v.typeid = '" & categoryid & "'" & vbcrlf
	end if
	sql = sql & "							and a.price_our > 0 and a.hidelive = 0 and a.ghost = 0 --and (select top 1 inv_qty from we_Items where partNumber = a.partNumber and master = 1) > 0" & vbcrlf & _
			"								and	(a.itempic is not null and a.itempic != '')" & vbcrlf & _
			"								and	((m.master is not null and p.slave is not null) or (m.master is null and p.slave is null))" & vbcrlf & _
			"					) a" & vbcrlf & _
			"			where	a.subtypeid = s.subtypeid" & vbcrlf & _
			"			order by a.subTypeOrderNum, a.flag1 desc, a.itemid desc	) x join " & vbcrlf & _
			"		(	select	partnumber, isnull(max(inv_qty), 0) inv_qty" & vbcrlf & _
			"			from	we_items with (nolock)" & vbcrlf & _
			"			group by partnumber	) y	" & vbcrlf & _
			"	on	x.partnumber = y.partnumber	" & vbcrlf
	if categoryid = 18 then
		'Music Skins
		Set Jpeg = Server.CreateObject("Persits.Jpeg")
		Jpeg.Quality = 100
		Jpeg.Interpolation = 1	
		sql = sql & "	union	" & vbcrlf & _
					"	select 	top " & numRowItems & " a.id as itemID, a.artist + ' ' + a.designName as itemDesc, a.image as itemPic" & vbcrlf & _
					"		,	isnull(a.price_we, 0.0) as price_our, isnull(a.msrp, 0.0) as price_retail, 0 flag1, a.musicSkinsID partnumber" & vbcrlf & _
					"		,	v.subTypeOrderNum, v.typeid, v.subtypeid, v.typeName, v.typeNameSEO_WE" & vbcrlf & _
					"		,	v.orderNum typeOrderNum, v.subtypeName, v.subTypeNameSEO_WE, 100 inv_qty, isnull(v.topText, '') subCatTopText, a.defaultImg, 1 isMusicSkins, 1 numberOfSales, 0 related" & vbcrlf & _
					"		,	null master, null colorSlaves, null colorid, 0 numAvailColors" & vbcrlf & _					
					"	from 	we_items_musicSkins a join v_subtypematrix v" & vbcrlf & _
					"		on	v.subtypeid = 1270" & vbcrlf & _
					"	where 	a.skip = 0 " & vbcrlf & _
					"		and a.deleteItem = 0 " & vbcrlf & _
					"		and (a.artist <> '' and a.artist is not null) " & vbcrlf & _
					"		and (a.designname <> '' and a.designname is not null) " & vbcrlf & _
					"		and a.deleteItem = 0 " & vbcrlf & _
					"		and a.modelID = '" & modelid & "'" & vbcrlf
	end if
end if

if categoryid = 3 then
	sql = sql & "order by x.typeid, x.subTypeOrderNum, x.subtypeid, x.flag1 desc, x.itemid desc" & vbcrlf
elseif categoryid = 7 then
	sql = sql & "order by x.typeid desc, x.subTypeOrderNum, x.subtypeid, x.flag1 desc, x.itemid desc" & vbcrlf
else
	sql = sql & "order by x.subTypeOrderNum, x.subtypeid, x.flag1 desc, x.itemid desc" & vbcrlf
end if

'response.write "<pre>" & sql & "</pre>"
session("errorSQL") = sql
set rs = oConn.execute(sql)

noProducts = false
if rs.EOF then
	noProducts = true

	sql = "select modelname, modelimg from we_models where modelid = '" & modelid & "'"
	session("errorSQL") = SQL
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		modelName = RS("modelname")
		modelImg = RS("modelimg")
	end if
else
	categoryName = rs("typeName")
	subCategoryName = rs("subtypeName")
	subCategoryNameSEO = rs("subTypeNameSEO_WE")
	subCatTopText = rs("subCatTopText")
end if

sql = "select id, color, colorCodes, borderColor from xproductcolors order by 1"
session("errorSQL") = sql
arrColors = getDbRows(sql)

'response.write "brandid:" & brandid & "<br>"
'response.write "modelID:" & modelID & "<br>"
'response.write "categoryID:" & categoryID & "<br>"
'response.write musicSkinGenre & "<br>"
'response.write musicSkinArtistID & "<br>"	
'=========================================================================================
Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
	oParam.CompareMode = vbTextCompare
	oParam.Add "x_brandID", brandID
	oParam.Add "x_modelID", modelID
	oParam.Add "x_categoryID", categoryID
	oParam.Add "x_brandName", brandName
	oParam.Add "x_modelName", modelName
	oParam.Add "x_categoryName", categoryName	
	oParam.Add "x_musicGenreName", musicSkinGenre
	if "0" <> musicSkinArtistID and len(musicSkinArtistID) > 0 then
		oParam.Add "x_musicArtistName", "artist-" & musicSkinArtistID & "-" & musicSkinArtistName
	end if

'call redirectURL("bmc", "", request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
'=========================================================================================

dim SEtitle, SEdescription, SEkeywords, h1, topText, bottomText

dim modelLink
if modelID = 940 then
	modelLink = "/ipod-ipad-accessories.asp"
else
	modelLink = "/T-" & modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
end if

if SEtitle = "" or SEdescription = "" or SEkeywords = "" then
	SQL = "SELECT title, description, keywords FROM we_MetaTags WHERE brandID = '" & brandID & "' AND modelid = '" & modelid & "' AND typeID = '" & categoryid & "'"
	
	Set RSmeta = Server.CreateObject("ADODB.Recordset")
	RSmeta.open sql, oConn, 0, 1

	if not RSmeta.eof then
		SEtitle = RSmeta("title")
		SEdescription = RSmeta("description")
		SEkeywords = RSmeta("keywords")
	else
		SEtitle = brandName & " " & modelName & " " & nameSEO(categoryName) & " - " & brandName & " " & modelName & " Cell Phone Accessories"
		SEdescription = brandName & " " & modelName & " " & nameSEO(categoryName) & " - WirelessEmporium is the largest store online for " & brandName & " " & modelName & " " & nameSEO(categoryName) & " & other " & brandName & " " & modelName & " Cell Phone Accessories at the lowest prices."
		SEkeywords = brandName & " " & modelName & " Accessories, " & brandName & " " & modelName & " Cell Phone Accessories, " & brandName & " " & modelName & " " & nameSEO(categoryName) & ", " & brandName & " " & modelName & " Cell Phone " & nameSEO(categoryName)
	end if
	RSmeta.close
	set RSmeta = nothing	
end if

if modelid = "987" or modelid = "988" or modelid = "1009" or modelid = "1015" then
	if SEtitle = "" then SEtitle = "Samsung " & modelName & " " & nameSEO(categoryName) & " - Buy Exclusive Samsung " & modelName & " Accessories"
	if SEdescription = "" then SEdescription = "Buy Quality Samsung " & modelName & " " & nameSEO(categoryName) & " - WirelessEmporium carries your favorite Samsung " & modelName & " " & singularSEO(categoryName) & " at discounted rates and Free Shipping!"
	if SEkeywords = "" then SEkeywords = "samsung " & modelName & " " & nameSEO(categoryName) & ", samsung " & modelName & " " & singularSEO(categoryName) & ", " & modelName & " " & singularSEO(categoryName) & ", samsung accessories, samsung " & modelName & " cell phone " & nameSEO(categoryName) & ", " & nameSEO(categoryName) & " for samsung " & modelName
	if h1 = "" then h1 = "Samsung " & modelName & " " & nameSEO(categoryName)
	select case categoryid
		case "6"
			topText = "Choose from a wide array of our quality Samsung " & modelName & " holsters and belt clips to safely and easily secure your Samsung " & modelName & " phone. We offer Samsung " & modelName & " belt holsters at prices you won't find anywhere else! Protect your Samsung " & modelName & " cell phone with our low priced Samsung " & modelName & " cell phone holsters! They clip easily to belts, purses, pockets, and protect your phone through your active lifestyle. An adjustable belt clip that rotates and locks in six positions makes it easy for you to adjust your phone the way you need it. Our Samsung " & modelName & " cell phone belt clips are stylish, too! Thousands of customers trust us to offer high-quality Samsung " & modelName & " holsters at the lowest possible prices, and we're dedicated to satisfying the needs of all our customers! Browse through our Samsung " & modelName & " cell phone belt clip selection today.</p>"
			topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">Since 2001, Wireless Emporium has been offering wholesale <a class=""topText-link"" href=""http://www.wirelessemporium.com"" title=""Cell Phone Accessories"">cell phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-" & modelid & "-cell-accessories-samsung-" & formatSEO(modelName) & ".asp"" title=""Samsung " & modelName & " Accessories"">Samsung " & modelName & " accessories</a> like holsters & belt clips for your Samsung " & modelName & " phone at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
		case "7"
			topText = "Choose from a wide array of our quality Samsung " & modelName & " leather cases to safely and easily secure your Samsung " & modelName & " phone. We offer prices you won't find anywhere else! Protect your Samsung " & modelName & " cell with our low priced Samsung " & modelName & " cases! They clip easily to belts, purses, pockets, and protect your phone through your active lifestyle. Our Samsung leather cases are stylish, too! Browse through our <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-" & modelid & "-cell-accessories-samsung-" & formatSEO(modelName) & ".asp"" title=""Samsung " & modelName & " Accessories"">Samsung " & modelName & " Accessories</a> selection today. Buy a quality Samsung " & modelName & " cell phone leather case today and we'll ship it to your home or business for free! Wireless Emporium guarantees your satisfaction on all of our quality Samsung leather cases. As the leading name in wholesale <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Cell Phone Accessories"">cell phone accessories</a>, we know a thing or two about Samsung " & modelName & " cases.</p>"
			topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">See for yourself why countless customers trust us to offer the best Samsung " & modelName & " leather cases for Samsung " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day! All of the quality <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-cases.asp"" title=""Cell Phone Cases"">cell phone cases</a> we offer are guaranteed to have passed tough quality tests and our customer service is sure to please. So don't wait! Buy your Samsung " & modelName & " cell phone case from Wireless Emporium today!"
		case "13"
			topText = "WirelessEmporium is the largest store online for Samsung " & modelName & " Data Cable & Memory & other <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-" & modelid & "-cell-accessories-samsung-" & formatSEO(modelName) & ".asp"" title=""Samsung " & modelName & " Accessories"">Samsung " & modelName & " Accessories</a> at the lowest prices.</p>"
			topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">The best in Samsung " & modelName & " Data Cable & Memory at prices up to 80% off retail. Couple this with our WirelessEmporium.com Happy-Shopper 30-day Guarantee and fast FREE SHIPPING, and you realize there's no better place to find your Samsung " & modelName & " Data Cable & Memory!"
		case "17"
			topText = "WirelessEmporium is the largest store online for Samsung " & modelName & " Body Guards/Skins & other Samsung " & modelName & " Cell Phone Accessories at the lowest prices.</p>"
			topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">The best in Samsung " & modelName & " Body Guards/Skins at prices up to 80% off retail. Couple this with our WirelessEmporium.com Happy-Shopper 30-day Guarantee and fast FREE SHIPPING, and you realize there's no better place to find your Samsung " & modelName & " Body Guards/Skins!"
	end select
else
	if categoryid = "1" then
		topText = "Wireless Emporium has everything you need for your " & brandName & " " & modelName & " phone, including a wide selection of <a class=""topText-link"" href=""/sc-1-sb-" & brandID & "-" & brandName & "-batteries.asp"" title=""" & brandName & " Cell Phone Batteries"">batteries for " & brandName & " phones</a>. If you need a spare battery because your current " & brandName & " " & modelName & " phone is burning through battery power too fast, or you need to replace on old battery that isn't holding it's charge anymore, we have a " & brandName & " " & modelName & " <a class=""topText-link"" href=""cell-phone-batteries.asp"" title=""Cell Phone Batteries"">battery</a> that will fit your needs guaranteed."
		topText = topText & "Every " & brandName & " " & modelName & " cell phone battery we sell is quality-tested and guaranteed for flawless performance."
		bottomText = "Don't pay outrageous retail prices for the same " & brandName & " " & modelName & " cell phone batteries you can find here at Wireless Emporium. We provide discount " & brandName & " " & modelName & " battery priced at up to 65% off retail! All cell phone batteries are guaranteed to perform or your money back. Since 2001, Wireless Emporium has been offering <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Discount " & brandName & " " & modelName & " Accessories"">discount cell phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best cell phone batteries for " & brandName & " " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
	elseif categoryid = "2" then
		topText = "Wireless Emporium is the online leader in <a class=""topText-link"" href=""/sc-2-sb-" & brandID & "-" & brandName & "-chargers.asp"" title=""" & brandName & " Cell Phone Batteries"">" & brandName & " cell phone chargers</a>. Because of that, we guarantee to have the widest selection of car chargers and travel chargers for your " & brandName & " " & modelName & " phone. If you are constantly on the go and need to keep your " & brandName & " " & modelName & " powered up or you just need a spare <a class=""topText-link"" href=""/cell-phone-chargers.asp"" title=""Cell Phone Chargers"">cell phone charger</a> for your home or office, Wireless Emporium is the place to be for the essential accessories you need."
		bottomText = "We provide thousands of businesses and consumers discount <a class=""topText-link"" href=""" & modelLink & """ title=""" & brandName & " " & modelName & " Accessory"">" & brandName & " " & modelName & " accessory</a> solutions that give your phone flawless performance time after time at prices up to 50% OFF retail we even ship for FREE! We can help you find the " & brandName & " " & modelName & " charger you need to extend the talk and standby times for your " & brandName & " " & modelName & " mobile phone. All Wireless Emporium " & brandName & " " & modelName & " chargers are guaranteed to perform or your money back. "
		bottomText = bottomText & "Look through our selection of " & brandName & " " & modelName & " chargers today. See for yourself why countless customers trust us to offer the best " & brandName & " " & modelName & " cell phone chargers for " & brandName & " " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
	elseif categoryid = "3" then
		topText = "Go bold or go sleek with your " & brandName & " " & modelName & " by choosing from hundreds of styles of <a class=""topText-link"" href=""http://www.wirelessemporium.com/sc-3-sb-" & brandID & "-" & formatSEO(brandName) & "-" & formatSEO(categoryName) & ".asp"" title=""" & brandName & " " & modelName & " Faceplates"">" & brandName & " cell phone faceplates</a> and screen protectors at Wireless Emporium. These " & brandName & " " & modelName & " faceplates are easy to assemble and our " & brandName & " " & modelName & " screen protectors are as simple as a sticker to install."
		topText = topText & "Because we offer our cell phone faceplates and screen protectors at discount prices, you can buy multiple " & brandName & " " & modelName & " faceplates and change them as often as your mood! Choose the color or design that best suits your personality from our wide selection of " & brandName & " " & modelName & " <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a> and cell phone faceplates below and protect your phone with style! " & brandName & " " & modelName & " faceplates and covers are an essential addition to your mobile device, as it is one of the most effective ways to both protect the look of your phone and enhance it, all while retaining total functionality."
		bottomText = "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">Since 2001, Wireless Emporium has been offering high-quality cell <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Discount Phone Accessories"">discount phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best cell phone faceplates and covers for " & brandName & " " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
	elseif categoryid = "6" then
		topText = "Choose from a wide array of our quality " & brandName & " " & modelName & " cell phone holsters and belt clips to safely and easily secure your " & brandName & " " & modelName & " phone. We offer " & brandName & " " & modelName & " belt holsters at prices you won't find anywhere else! Protect your " & brandName & " " & modelName & " cell phone with our low priced " & brandName & " " & modelName & " holsters! They clip easily to belts, purses, pockets, and protect your phone through your active lifestyle. An adjustable belt clip that rotates and locks in six positions makes it easy for you to adjust your phone the way you need it. Our " & brandName & " " & modelName & " <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-holsters-belt-clips-holders.asp"" title=""" & brandName & " " & modelName & " Cell Phone Belt Clips"">cell phone belt clips</a> are stylish, too! "
		topText = topText & "Thousands of customers trust us to offer high-quality holsters at the lowest possible prices, and we're dedicated to satisfying the needs of all our customers! Browse through our " & brandName & " " & modelName & " cell phone belt clip selection today."
		bottomText = "Since 2001, Wireless Emporium has been offering <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Wholesale " & brandName & " " & modelName & " Cell Phone Accessories"">wholesale cell phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best <a class=""topText-link"" href=""" & modelLink & """ title=""" & brandName & " " & modelName & " Accessories"">" & brandName & " " & modelName & " accessories</a> like holsters & belt clips for your " & brandName & " " & modelName & " phone at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
	elseif categoryid = "7" then
		topText = "Choose from a wide array of our quality " & brandName & " " & modelName & " leather cases to safely and easily secure your " & brandName & " " & modelName & " phone. We offer prices you won't find anywhere else! Protect your " & brandName & " " & modelName & " cell with our low priced " & brandName & " " & modelName & " leather cases! They clip easily to belts, purses, pockets, and protect your phone through your active lifestyle. Our " & brandName & " " & modelName & " leather cases are stylish, too! Browse through our <a class=""topText-link"" href=""" & modelLink & """ title=""" & brandName & " " & modelName & " Accessories"">" & brandName & " " & modelName & " Accessories</a> cases selection today. "
		topText = topText & "Buy a quality " & brandName & " " & modelName & " cell phone leather cases today and we'll ship it to your home or business for free! Wireless Emporium guarantees your satisfaction on all of our quality <a class=""topText-link"" href=""http://www.wirelessemporium.com/sc-3-sb-" & brandID & "-" & formatSEO(brandName) & "-" & formatSEO(categoryName) & ".asp"" title=""" & brandName & " " & modelName & " Leather Case"">" & brandName & " leather cases</a>. As the leading name in <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Wholesale " & brandName & " " & modelName & " Cell Phone Accessories"">wholesale cell phone accessories</a>, we know a thing or two about " & brandName & " " & modelName & " cases."
		bottomText = "See for yourself why countless customers trust us to offer the best " & brandName & " " & modelName & " cases for " & brandName & " " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day! All of the <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-cases.asp"" title=""Quality " & brandName & " " & modelName & " Cases"">quality cell phone cases</a> we offer are guaranteed to have passed tough quality tests and our customer service is sure to please. So don't wait! Buy your " & brandName & " " & modelName & " cell phone case from Wireless Emporium today!"
	end if
end if


if topText = "" then
	topText = "WirelessEmporium is the largest store online for " & brandName & " " & modelName & " Cell Phone " & nameSEO(categoryName) & " & other " & brandName & " " & modelName & " Cell Phone Accessories at the lowest prices."
	topText = topText & "<br /><br />The best in " & brandName & " " & modelName & " " & nameSEO(categoryName) & " at prices up to 80% off retail. Couple this with our WirelessEmporium.com Happy-Shopper 30-day Guarantee and fast FREE SHIPPING, and you realize there's no better place to find your " & brandName & " " & modelName & " " & nameSEO(categoryName) & "!"
end if

dim strBreadcrumb
if strBreadcrumb = "" then strBreadcrumb = brandName & " " & modelName & " " & nameSEO(categoryName)


'todo: need to consolidate Metatags with we_XXXText
dim strH1: strH1 = EMPTY_STRING
dim strH2: strH2 = EMPTY_STRING
dim strAltText: strAltText = EMPTY_STRING

'set input values to extract page meta-values
dim dicReplaceAttribute: set dicReplaceAttribute = CreateObject("Scripting.Dictionary")
dicReplaceAttribute( "BrandName") = brandName
dicReplaceAttribute( "ModelName") = modelName
dicReplaceAttribute( "CategoryName") = categoryName

'set known product attributes to replace out template placeholders
dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
dicSeoAttributeInput( "BrandId") = brandID
dicSeoAttributeInput( "ModelId") = modelID
dicSeoAttributeInput( "CategoryId") = categoryID

'get content event text (initially installed to load pixels) [knguyen/20110520]
'todo: deploy to other wrappers as needed
dim dicContentEventText: set dicContentEventText = GetContentEventText( dicSeoAttributeInput)

call LookupSeoAttributes()

dim strTypeToken : strTypeToken = "Cell Phone"
if isTablet then strTypeToken = "Tablet"

if isTablet then
	modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " " & strTypeToken & " Accessories</a>"	
else
	if brandid = 17 and phoneOnly = 1 then
		brandCrumb 	= "<a class=""breadcrumb"" href=""/iphone-accessories.asp"">Apple iPhone Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " Accessories</a>"	
	elseif brandid 	= 17 and phoneOnly = 0 then
		topText = replace(replace(topText, "apple cell phone", brandName & " " & modelName), strTypeToken, "")
		SEtitle = replace(replace(SEtitle, "apple cell phone", brandName & " " & modelName), strTypeToken, "")
		SEdescription = replace(replace(SEdescription, "apple cell phone", brandName & " " & modelName), strTypeToken, "")
		SEkeywords = replace(replace(SEkeywords, "apple cell phone", brandName & " " & modelName), strTypeToken, "")
	
		brandCrumb 	= "<a class=""breadcrumb"" href=""/ipod-ipad-accessories.asp"">Apple iPod/iPad Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " Accessories</a>"
	else
		brandCrumb 	= "<a class=""breadcrumb"" href=""/" & brandSEO(brandID) & """>" & brandName & " " & strTypeToken & " Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " " & strTypeToken & " Accessories</a>"	
	end if
end if

if h1 <> "" then
	strH1 = h1
elseif strH1 = "" then
	strH1 = brandName & " " & modelName & " " & nameSEO(categoryName)
end if

session("otherAdminOptions") = ", <a style='color:#ffffff;' href='/admin/db_update_models.asp?ModelID=" & modelID & "&BrandID=" & brandID & "&searchType=Model&submitType=Edit&submitModel=Search'>Edit " & modelName & "</a>"
%>
<!--#include virtual="/includes/template/top.asp"-->
<% extraWidgets = "no" %>
<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
        	<%
			if categoryid = 8 then
			%>
			<a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;Other Accessories
            <%
			else
			%>
			<a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;<%=brandCrumb%>&nbsp;&gt;&nbsp;<%=modelCrumb%>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>            
            <%
			end if
			%>
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" valign="top" style="padding:10px 0px 10px 0px;" title="<%=strH1%>">
            <table width="748" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="150" align="center" valign="middle" style="padding-right:5px;">
						<img src="/productpics/models/<%=modelImg%>" width="80" border="0" alt="<%=strH1%>" title="<%=strH1%>">
                    </td>
                    <td width="383" align="left">
						 <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                         <%
						 if categoryid = 8 then
						 %>
                            <tr>                    
                            	<td style="height:120px; font-size:24px; font-weight:bold;" valign="middle" align="left">
	                                <%=brandName & " " & modelName & " " & categoryName%>
                                </td>
							</tr>
                         <%
						 else
						 %>
                            <tr>                    
                            	<td style="border-bottom:1px solid #ccc; height:60px; font-size:24px; font-weight:bold;" valign="bottom" align="left">
	                                <%=brandName & " " & modelName%>
                                </td>
							</tr>
                            <tr><td valign="top" align="left" style="height:60px; font-size:24px; font-weight:bold;"><%=categoryName%></td></tr>                         
                         <%
						 end if
						 %>
						</table>
                    </td>
                    <td style="padding-left:10px;">
                    <img src="/images/cats/we_header_<%=categoryid%>.jpg" border="0" alt="<%=strH1%>" title="<%=strH1%>" />
					</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
            <div style="float:left;">
            	<table width="726px" border="0" cellspacing="0" cellpadding="0" class="graybox-middle" style="border-left:1px solid #999; border-right:1px solid #999; padding:0px 10px 0px 10px;">
                	<tr>
                    	<td align="right">
	                        <div class="graybox-inner" style="display:inline;">Jump to:</div>
							<div style="display:inline;" id="jumpToOptions"></div>                      
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" valign="top">
            <%
			if not noProducts then
				lap = 0 : a = 0 : lastTypeID = 0 : lastSubTypeID = 0
				set fsThumb = CreateObject("Scripting.FileSystemObject")
				lastPic = ""
				do while not rs.EOF
					a = a + 1
					lap = lap + 1
					
					curSubTypeID = rs("subtypeid")
					subCategoryName = rs("subtypeName")
					subCategoryNameSEO = rs("subtypeNameSEO_WE")
					if lastSubCategoryName = "" then lastSubCategoryName = rs("subtypeName") end if
					itemPic = rs("itemPic")
					if lastPic <> itemPic then
						lastPic = itemPic
						musicSkins = rs("isMusicSkins")
						msDefaultImg = rs("defaultImg")
	
						'====================================================================== find appropriate product images
						if musicSkins = 1 then
							itemImgPath = Server.MapPath("/productpics/musicSkins/musicSkinsLarge") & "\" & itemPic
							itemImgPath2 = Server.MapPath("/productpics/musicSkins/musicSkinsSmall") & "\" & itemPic
							useImgPath = "/productpics/musicSkins/musicSkinsSmall/" & itemPic
						else
							itemImgPath = Server.MapPath("/productpics/thumb") & "\" & itemPic
							itemImgPath2 = Server.MapPath("/productpics/thumb") & "\" & itemPic
							useImgPath = "/productpics/thumb/" & itemPic
						end if
						
						if not fsThumb.FileExists(itemImgPath) then
							'response.Write("itemImgPath:" & itemImgPath & "<br>")
							if musicSkins = 1 then
								if isnull(msDefaultImg) then
									itemPic = "imagena.jpg"
									useImgPath = "/productpics/thumb/imagena.jpg"
								else
									skipRest = 0
									if fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & msDefaultImg)) then
										setDefault = msDefaultImg
									elseif fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & replace(msDefaultImg," ","-"))) then
										setDefault = replace(msDefaultImg," ","-")
									else
										setDefault = msDefaultImg
									end if
									if skipRest = 0 then
										chkPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & setDefault)
										if not fsThumb.FileExists(chkPath) then
											musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)
											if fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)) then
												musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)
											elseif fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & replace(setDefault," ","-"))) then
												musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & replace(setDefault," ","-"))
											else
												itemPic = "imagena.jpg"
												useImgPath = "/productpics/thumb/imagena.jpg"
												skipRest = 1
											end if
											if skipRest = 0 then
												session("errorSQL") = musicSkinsDefaultPath
												Jpeg.Open musicSkinsDefaultPath
												Jpeg.Width = 100
												Jpeg.Height = Jpeg.OriginalHeight * 100 / Jpeg.OriginalWidth
												Jpeg.Save chkPath
											end if
										end if
										if skipRest = 0 then
											itemPic = setDefault
											useImgPath = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & setDefault
										end if
									end if
								end if
							else
								itemPic = "imagena.jpg"
								useImgPath = "/productpics/thumb/imagena.jpg"
							end if
						elseif musicSkins = 1 and not fsThumb.FileExists(itemImgPath2) then
							Jpeg.Open itemImgPath
							if Jpeg.OriginalHeight > Jpeg.OriginalWidth then
								Jpeg.Height = 100
								Jpeg.Width = Jpeg.OriginalWidth * 100 / Jpeg.OriginalHeight
							else
								Jpeg.Width = 100
								Jpeg.Height = Jpeg.OriginalHeight * 100 / Jpeg.OriginalWidth
							end if
							Jpeg.Save itemImgPath2
						end if		
						'====================================================================== find appropriate product images
						if musicSkins = 1 then
							subCatLink = "/sb-" & brandid & "-sm-" & modelid & "-sc-" & curSubTypeID & "-music-skins-genre.asp"
						elseif categoryID = 8 then
							subCatLink = "/sb-0-sm-0-scd-" & curSubTypeID & "-" & formatSEO(subCategoryName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
						else
							subCatLink = "/sb-" & brandid & "-sm-" & modelid & "-scd-" & curSubTypeID & "-" & formatSEO(subCategoryName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
						end if
						selectOptions = selectOptions & subCatLink & "##" & subCategoryName & "##"
						
						useStaticImage = "1110##1130##1140##1150##1360##"
						if instr(useStaticImage, curSubTypeID & "##") > 0 then useImgPath = "/images/bmc/subcat-" & curSubTypeID & ".jpg"
						%>
						<div style="float:left; width:150px; padding:10px; margin-left:10px; border-bottom:1px solid #CCC;<% if a < 4 then %> border-right:1px dotted #CCC;<% end if %>">
							<div style="float:left; width:130px; height:100px; text-align:center;"><a href="<%=subCatLink%>" title="<%=brandName & " " & modelName & " " & subCategoryName%>"><img src="<%=useImgPath%>" border="0" height="100" /></a></div>
							<div style="float:left; width:130px; padding-top:10px; font-weight:bold; height:50px; overflow:hidden; text-align:center;"><a href="<%=subCatLink%>" class="cellphone-link" title="<%=brandName & " " & modelName & " " & subCategoryName%>"><%=subCategoryName%></a></div>
						</div>
						<%
						if a = 4 then a = 0
						do while curSubTypeID = rs("subtypeID")
							rs.movenext
							if rs.EOF then exit do
						loop
						if rs.EOF then exit do
					else
						rs.movenext
					end if
				loop
			else
				response.write "<div style=""width:700px; padding:20px;"" align=""center""><h3>No Products Matched Your Query!</h3></div>"
			end if
			%>          
        </td>
    </tr>
    <%
    sql	=	"select	distinct a.modelid, a.modelname, a.modelimg, a.temp, c.brandid, c.brandname" & vbcrlf & _
            "	,	a.isTablet, v.typeid, v.typeName, v.orderNum" & vbcrlf & _
            "	,	'sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}.asp' urlStructure	" & vbcrlf & _
            "from	we_models a join we_brands c " & vbcrlf & _
            "	on	a.brandid = c.brandid join we_items b " & vbcrlf & _
            "	on	a.modelid = b.modelid join we_types d " & vbcrlf & _
            "	on	b.typeid = d.typeid join v_subtypeMatrix v" & vbcrlf & _
            "	on	b.subtypeid = v.subtypeid" & vbcrlf & _
            "where	a.modelid = '" & modelid & "' " & vbcrlf & _
            "	and d.typeid not in (5) and b.hidelive = 0 and b.inv_qty <> 0 and b.price_our > 0 " & vbcrlf & _
            "union" & vbcrlf & _
            "select	distinct a.modelid, a.modelname, a.modelimg, a.temp, c.brandid, c.brandname" & vbcrlf & _
            "	,	a.isTablet, v.typeID, v.typeName, v.orderNum	" & vbcrlf & _
            "	,	'sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}.asp' urlStructure	" & vbcrlf & _
            "from	we_models a join we_brands c" & vbcrlf & _
            "	on	a.brandid = c.brandid join v_typeMatrix v" & vbcrlf & _
            "	on	v.typeID in (5, 8)" & vbcrlf & _
            "where	a.modelid = '" & modelid & "'" & vbcrlf & _
            "order by v.orderNum desc" & vbcrlf
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	a = 0
	if not RS.eof then
	%>
    <!-- other accessories start -->
    <tr>
    	<td style="padding-top:50px;">
            <div class="graybox-middle" style="width:726px; border-left:1px solid #999; border-right:1px solid #999; padding:0px 10px 0px 10px;">
                <div class="graybox-inner" title="OTHER <%=ucase(brandName) & " " & ucase(modelName)%> ACCESSORIES" align="center">
                	<h3 class="brandModel">OTHER <%=ucase(brandName) & " " & ucase(modelName)%> ACCESSORIES</h3>
				</div>
            </div>
        </td>
    </tr>
    <tr>
    	<td>
            <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                <tr>    
    
    <%
		brandName = replace(brandName, "/", " / ")
		modelName = replace(modelName, "/", " / ")					
		do until RS.eof
			thisType = replace(RS("typeName"), "/", " / ")
			'sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}.asp
			strLink = replace(replace(replace(replace(replace(replace(rs("urlStructure"), "{brandid}", brandid), "{modelid}", modelid), "{typeid}", rs("typeid")), "{brandname}", formatSEO(brandName)), "{modelname}", formatSEO(modelName)), "{typename}", formatSEO(thisType))
			altText = modelName & " " & thisType
			%>
					<td align="center" valign="top" width="165" style="padding:15px 5px 30px 5px;">
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
							<tr><td align="center"><a href="<%=strLink%>" title="<%=altText%>"><img src="/images/categories/we_cat_<%=rs("typeid")%>.jpg" width="165" border="0" alt="<%=altText%>" title="<%=altText%>" /></a></td></tr>
							<tr><td align="center" style="padding-top:10px;"><a href="<%=strLink%>" title="<%=altText%>" class="cellphone3-link"><%=altText%></a></td></tr>                            
						</table>
					</td>
			<%
			RS.movenext
			a = a + 1
			if a = 4 then
				response.write "</tr><tr>" & vbcrlf
				a = 0
			end if
		loop
		%>
                </tr>
            </table>
        </td>
    </tr>
    <!-- other accessories end -->
	<%
	end if
	%>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
    <tr><td align="center" class="topText"><h1 class="brandModelCat2"><%=strH1%></h1></td></tr>
    <tr><td align="center" class="topText"><%=topText%></td></tr>                            
    <tr><td align="center" class="bottomText"><%=bottomText%></td></tr>
</table>
<div id="selectOptionHolder" style="display:none;">
	<%
	if prepStr(selectOptions) <> "" then
		selectOptions = left(selectOptions,len(selectOptions)-2)
		selectOptionsArray = split(selectOptions,"##")
	%>
	<form name="frmSearch" method="post" style="display:inline;">
    <select name="cbItem" onChange="if(this.value != ''){window.location=this.value;}">
        <option value="">Choose from the following</option>
        	<%
			for i = 0 to ubound(selectOptionsArray)
				useLink = selectOptionsArray(i)
				i = i + 1
				useName = selectOptionsArray(i)
			%>
	        <option value="<%=useLink%>" title="<%=formatSEO(useName)%>"><%=useName%></option>
            <%
			next
			%>
    </select>
    <%
	end if
	%>
</form>
</div>
<script>
document.getElementById("jumpToOptions").innerHTML = document.getElementById("selectOptionHolder").innerHTML
function viewMore(subtypeid)
{
	nextLink = document.getElementById('id_link_'+subtypeid).href;
	if (nextLink != "") window.location = nextLink;
}

function preloading(pItemID, selectedColorID)
{
	ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmc&uType=pre&itemid=' + pItemID + '&selectedColorID=' + selectedColorID, 'preloading_deck_' + pItemID);		
}

function changeProduct(pItemID, selectedColorID, numColors, selectedIDX, onSale)
{
	curObj = null;
	for(i=0; i < numColors; i++) 
	{
		curObj = 'div_colorOuterBox_' + pItemID + '_' + i;
		if (document.getElementById(curObj) != null) document.getElementById(curObj).className = 'colorBoxUnselected';
	}
	curObj = 'div_colorOuterBox_' + pItemID + '_' + selectedIDX;
	document.getElementById(curObj).className = 'colorBoxSelected';
	
	ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmc&uType=img&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&onsale=' + onSale + '&adminOption=<%=prepInt(session("adminID"))%>', 'id_productImage_'+pItemID);
	ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmc&uType=link&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&itemMaxLen=<%=nItemDescMaxLength%>&adminOption=<%=prepInt(session("adminID"))%>', 'id_productLink_'+pItemID);
	ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmc&uType=price&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&adminOption=<%=prepInt(session("adminID"))%>', 'id_productPricing_'+pItemID);
}
</script>
<!--#include virtual="/includes/template/bottom.asp"-->