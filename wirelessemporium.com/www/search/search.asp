<%
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Name: search.asp  -- generic version
' Copyright: Nextopia Software Corporation 2006
' Last Modified: 2006-11-16 2:30pm
' Requires: files.asp
'			show_hide.js
'           Empty_Template.html
'           Single_Entry_Template_Grid.html
'			Single_Entry_Template_List.html
'			icon_sort_down.gif
'			icon_sort_up.gif
'			largegrid.gif
'			listview.gif
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
pageName = "Search"

dim myKeywords, HTTP_REFERER, FixedQueryString, q, qPos
HTTP_REFERER = request.servervariables("HTTP_REFERER")
'if inStr(HTTP_REFERER,"http://www.google.com/search") > 0 then
if inStr(HTTP_REFERER,"http://staging.wirelessemporium.com/search/mytest.asp") > 0 then
	'if request.querystring("ovcpn") <> "" or request.querystring("ovcrn") <> "" or request.querystring("ovtac") <> "" then
	qPos = inStr(HTTP_REFERER,"&q=")
	if qPos > 0 then
		'myKeywords = request.querystring("ovcpn") & " " & request.querystring("ovcrn") & " " & request.querystring("ovtac")
		q = mid(HTTP_REFERER,qPos,len(HTTP_REFERER))
		q = replace(q,"&q=","")
		q = left(q,inStr(q,"&"))
		myKeywords = replace(replace(q,"+"," "),"&","")
		FixedQueryString = "keywords=" & Server.URLEncode(myKeywords)
	else
		myKeywords = prepStr(request.querystring("keywords"))
		FixedQueryString = Request.QueryString
	end if
else
	myKeywords = prepStr(request.querystring("keywords"))
	FixedQueryString = Request.QueryString
end if

if lcase(prepStr(myKeywords)) = "shoprunner" then response.Redirect("/shopRunner")

dim strTitle, strBreadcrumb, SEtitle
strTitle = replace(replace(replace(replace(replace(myKeywords,"'",""),"<",""),">",""),"(",""),")","")
strBreadcrumb = strTitle
SEtitle = "Wireless Emporium Site Search for " & strTitle
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
	tempMyKeywords = myKeywords
	if not isnull(tempMyKeywords) then tempMyKeywords = trim(replace(replace(lcase(tempMyKeywords), "accessories", ""), "accessory", ""))

	sql = "select a.modelID, a.modelName, b.brandName from we_Models a join we_brands b on a.brandID = b.brandID where a.hideLive = 0 and a.modelName = '" & tempMyKeywords & "'"
	session("errorSQL") = sql
	set searchRS = oConn.execute(sql)

	if searchRS.EOF then
		sql = "select brandName, brandID from we_Brands where brandName = '" & tempMyKeywords & "'"
		session("errorSQL") = sql
		set searchRS = oConn.execute(sql)

		if searchRS.EOF then
			sql = "select a.modelID, b.brandName, a.modelName from we_Models a left join we_Brands b on a.brandID = b.brandID where a.hideLive = 0 and b.brandName + ' ' + a.modelName = '" & tempMyKeywords & "'"
			session("errorSQL") = sql
			set searchRS = oConn.execute(sql)

			if searchRS.EOF then
				sql = "select a.modelID, b.brandName, a.modelName from we_Models a left join we_Brands b on a.brandID = b.brandID where a.hideLive = 0 and b.brandName + ' ' + a.modelName like '%" & tempMyKeywords & "%' group by a.modelID, b.brandName, a.modelName"
				session("errorSQL") = sql
				set searchRS = oConn.execute(sql)

				if not searchRS.EOF then
					useModelID = searchRS("modelID")
					brandName = searchRS("brandName")
					modelName = searchRS("modelName")
					searchRS.movenext
					if searchRS.EOF then PermanentRedirect("/" & formatSEO(brandName & "-" & modelName) & "-accessories")
				end if
			else
				useModelID = searchRS("modelID")
				brandName = searchRS("brandName")
				modelName = searchRS("modelName")
				PermanentRedirect("/" & formatSEO(brandName & "-" & modelName) & "-accessories")
			end if
		else
			if searchRS("brandName") = "Apple" then
				PermanentRedirect("/iphone-accessories")
			else
				PermanentRedirect("/" & formatSEO(searchRS("brandName")) & "-phone-accessories")
			end if
		end if
	else
		useModelID = searchRS("modelID")
		modelName = searchRS("modelName")
		brandName = searchRS("brandName")
		PermanentRedirect("/" & formatSEO(brandName & "-" & modelName) & "-accessories")
	end if

	if tempMyKeywords <> "" and not isnull(tempMyKeywords) then
		sql = 	"select itemid, itemdesc from we_items where (itemID = " & prepInt(tempMyKeywords) & " or partnumber = '" & trim(replace(tempMyKeywords, "'", "''")) & "') and master = 1 and hidelive = 0 and inv_qty <> 0 and price_our > 0"
		set searchRS = oConn.execute(sql)
		if not searchRS.eof then
			PermanentRedirect("/p-" & searchRS("itemid") & "-" & formatSEO(searchRS("itemdesc")))
		end if
	end if

	noIndex = 1

%>
<!--#include virtual="/includes/template/top.asp"-->
<!--#include file="files.asp"-->
<script src="show_hide.js" language="javascript" type="text/javascript"></script>
<table border="0" bordercolor="#FF0000" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;Search Results
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle" width="100%">

<%
'Variables for the client to change
CLIENT_ID = "d9a6d3b557962a7f77019c1a8735b4d7"		' Put your client id in here
RESULTS_PER_PAGE = 20
grid_rows = 5
grid_cols = 4

'Variables for the search to use
Dim sites(3)
'sites(0) = "http://ecommerce-search.nextopia.com/return-results.php"
'sites(1) = "http://theory.nextopia.com/return-results.php"
'sites(2) = "http://ecommerce-search.nextopia.net/return-results.php"
'sites(0) = "http://ecommerce-search.nextopiasoftware.com/return-results.php"
'sites(1) = "http://ecommerce-search.nextopiasoftware.net/return-results.php"
'sites(2) = "http://ecommerce-search-dyn.nextopia.net/return-results.php"

sites(0) = "http://" & request.ServerVariables("SERVER_NAME") & "/search/return-results"
str_html = Readfile(Server.mappath("Empty_Template.html"))

'Setting the url to send XML to
'if request.querystring("ovcpn") <> "" or request.querystring("ovcrn") <> "" or request.querystring("ovtac") <> "" then
'	FixedQueryString = "keywords=" & Server.URLEncode(request.querystring("ovcpn") & " " & request.querystring("ovcrn") & " " & request.querystring("ovtac"))
'else
'	FixedQueryString = Request.QueryString
'end if

If InStr(FixedQueryString, "page") = 0 Then
	FixedQueryString = "page=1&" & FixedQueryString
End if

URLToXML = "?xml=1&client_id=" & CLIENT_ID & "&" & FixedQueryString & "&res_per_page=" & RESULTS_PER_PAGE & "&searchtype=1"

'Getting the IP and User Agent of the user performing the search
sIPAddress = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
If sIPAddress="" Then sIPAddress = Request.ServerVariables("REMOTE_ADDR")
sUserAgent = Request.ServerVariables("HTTP_USER_AGENT")

URLToXML = URLToXML & "&ip=" & Server.URLEncode(sIPAddress) & "&user_agent=" & Server.URLEncode(sUserAgent)

'Send the XML query string
connected = False
timesfailed = 0
For count = 0 To UBound(sites)-1
    FullURLToXML = sites(count) & URLToXML
    set xmlhttp = CreateObject("MSXML2.ServerXMLHTTP")
	'oServerXMLHTTPRequest.setTimeouts(resolveTimeout, connectTimeout, sendTimeout, receiveTimeout);
	xmlhttp.setTimeouts 8000, 8000, 8000, 8000

    on error resume next
'	response.write "FullURLToXML:" & FullURLToXML
    xmlhttp.open "GET", FullURLToXML, false
    xmlhttp.send ""
    status = xmlhttp.status
    if err.number = 0 or status = 200 then
		'Check if the xml fully completed, if it didn't, try again
		Set xmldom = xmlHttp.responseXML
'		response.write server.HTMLEncode(xmlHttp.responseText)
'		response.end
		Set XMLDone = xmlDOM.getElementsByTagName("xml_feed_done")
		Set XMLDone = XMLDone.Item(0)
		XMLDone = XMLQueryTime.childNodes(0).text
		If XMLDone = 1 Then
			connected = True
			count = UBound(sites)
		Else
			timesfailed = timesfailed + 1
			If timesfailed < 3 Then
				count = count - 1
			Else
				timesfailed = 0
			End if
		End if
    end if
next

'response.write "<br><br>" & server.HTMLEncode(xmlHttp.responseText)

If connected = false Then
	'Code to instead go to a local search would go here
	'Response.Redirect("" & Server.URLencode(myKeywords))
	'Response.end

	'Remove this when a proper redirect is done

	errMsg = "<TABLE width='100%'><TR><TD valign=top width='100%'>" &_
			  "<span class=""smlText"">Your search - " & myKeywords & " - did not match any products.<BR><BR>" &_
			  Sugg_String & "<BR><BR>" &_
			  "Suggestions:<BR>Make sure all words are spelled correctly.<BR>Try different keywords.<BR>Try more general keywords.<BR></span>" &_
			  "</TD></TR></TABLE>"


	str_html = Replace(str_html, "[CONTENT]", errMsg,1)
	str_html = Replace(str_html, "[MERCHANDIZING]", "",1)
	str_html = Replace(str_html, "[LOGO]", "",1)
	Response.write str_html
	Response.end
End If

Set xmldom=xmlHttp.responseXML

'Parse the XML to build the page
'Query Time
Set XMLQueryTime = xmlDOM.getElementsByTagName("query_time")
Set XMLQueryTime = XMLQueryTime.Item(0)
QueryTime = XMLQueryTime.childNodes(0).text

'Suggested Spelling
Set XMLSpell = xmlDOM.getElementsByTagName("suggested_spelling")
Set XMLSpell = XMLSpell.Item(0)
SuggestedSpelling = XMLSpell.childNodes(0).text

'Custom Synonyms
Set XMLSynonyms = xmlDOM.getElementsByTagName("synonym")
Dim SynonymsArray()
ReDim SynonymsArray(XMLSynonyms.Length)
For i = 0 To XMLSynonyms.Length-1
	Set XMLSynonym = XMLSynonyms.Item(i)
	SynonymsArray(i) = XMLSynonym.childNodes(0).text
Next


'Notices / or switch
Set XMLNotices = xmlDOM.getElementsByTagName("notices")
Set XMLNotices = XMLNotices.Item(0)
For i = 0 To XMLNotices.childNodes.Length-1
	Execute("notices_" & XMLNotices.childNodes(i).nodeName & " = " & XMLNotices.childNodes(i).text)
Next

'Since there are times the keywords will need to be replaced, we will create a regular expression object to replace keywords
Set KeywordReplaceObject = New RegExp
With KeywordReplaceObject
	.Pattern = "keywords=(.*?)&"
	.IgnoreCase = True
	.Global = True
End With

'This ScratchUrl is used for spellchecked/synonym word searches as well as user search depth as we want to wipe the refinements
ScratchUrl = "search?page=1&keywords=" & Server.URLEncode(myKeywords) & "&in_field=" & Request("in_field") &_
			"&sort_by_field=" & Request("sort_by_field")

'Create the string for spellcheck and synonyms
If SuggestedSpelling <> "" Then
	'Replace the keyword with the spellchecked word
	Curr_Url = KeywordReplaceObject.Replace(ScratchUrl, "keywords=" & SuggestedSpelling & "&")
	Sugg_String = "<A href='" & Curr_Url & "&spellcheck=y'>" & SuggestedSpelling & "</A>, "
End If
For i = 0 To UBound(SynonymsArray) - 1
	Curr_Url = KeywordReplaceObject.Replace(ScratchUrl, "keywords=" & SynonymsArray(i) & "&")
	Sugg_String = Sugg_String & "<A href='" & Curr_Url & "&synonym=y'>" & SynonymsArray(i) & "</A>, "
Next
If Sugg_String <> "" Then
	Sugg_String = Left(Sugg_String, Len(Sugg_String)-2) 'Removing the trailing comma + space
	Sugg_String = "<span class=""smlText"">Did you mean: </span>" & Sugg_String & " ? "
End if


'Searched-in Field
Set XMLSearchedIn = xmlDOM.getElementsByTagName("searched_in_field")
Set XMLSearchedIn = XMLSearchedIn.Item(0)
SearchedIn = XMLSearchedIn.childNodes(0).text

'Pagination
Set XMLPagination = xmlDOM.getElementsByTagName("pagination")
Set XMLPagination = XMLPagination.Item(0)
TotalProducts = CInt(XMLPagination.childNodes(0).text)

'Since ASP is absolutely useless we have to convert html entities manually
SearchWordToDisplay = myKeywords
SearchWordToDisplay = Replace(SearchWordToDisplay,"<","&lt;")
SearchWordToDisplay = Replace(SearchWordToDisplay,">","&gt;")
SearchWordToDisplay = Replace(SearchWordToDisplay,"(","&#40;")
SearchWordToDisplay = Replace(SearchWordToDisplay,")","&#41;")


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'If there are > 0 total products, continue, otherwise stop
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
If TotalProducts <= 0 Then
	If SearchedIn <> "" Then
		SearchedInString = " in " & SearchedIn
	End If
	Content = "<TABLE width='100%'><TR><TD valign=top width='100%'>" &_
			  "<span class=""smlText"">Your search - " & SearchWordToDisplay & SearchedInString & " - did not match any products.<BR><BR>" &_
			  Sugg_String & "<BR><BR>" &_
			  "Suggestions:<BR>Make sure all words are spelled correctly.<BR>Try different keywords.<BR>Try more general keywords.<BR></span>" &_
			  "</TD></TR></TABLE>"
Else

	'Merchandizing
	Set Merchandizing = xmlDOM.getElementsByTagName("merchandizing")
	Set Merchandizing = Merchandizing.Item(0)
	If Not (Merchandizing.childNodes(0) Is NOTHING) Then
		Merchandizing = Merchandizing.childNodes(0).text
		If InStr(Merchandizing, "link:") = 1 Then
			Response.Redirect(Replace(Merchandizing, "link:", "" ,1))
			Response.end
		End If
	Else
		Merchandizing = ""
	End if

	'The Rest of pagination
	ProductsShownMin = CInt(XMLPagination.childNodes(1).text)
	ProductsShownMax = CInt(XMLPagination.childNodes(2).text)
	CurrentPage = CInt(XMLPagination.childNodes(3).text)
	TotalPages = CInt(XMLPagination.childNodes(4).text)
	PreviousPage = CInt(XMLPagination.childNodes(5).text)
	NextPage = CInt(XMLPagination.childNodes(6).text)

	'Generate notice string
	NoticeString = ""
	If notices_or_switch = 1 Then
		NoticeString = NoticeString &_
				"<TR><TD valign=top class=""smlText"" align=left width='100%' style='border-bottom: 1px solid #c0c0c0; border-top: 1px solid #c0c0c0'>" &_
				"There were no products that contained <b>all</b> of the words you searched for. The results below contain <b>some</b> of the words." &_
				"</TD></TR>"
	End If

	'Now, for many things such as sorting or refining the search, we need to go back to page 1 with the current url + the change
	'so we'll just make a URL for that so we don't have to use regular expressions to change the page to 1 every time
	Set PageReplaceObject = New RegExp
	With PageReplaceObject
		.Pattern = "page=[0-9]+"
		.IgnoreCase = True
		.Global = True
	End With

	PageOneUrl = "search?" & PageReplaceObject.Replace(FixedQueryString, "page=1")

	'Since when changing sort by we need a query string that has no sorting, so we create a regular expresson object to make another url
	Set SortReplaceObject = New RegExp
	With SortReplaceObject
		.Pattern = "&sort_by_field=((.*?)(ASC|DESC)|)"
		.IgnoreCase = True
		.Global = True
	End With

	PageOneNoSortUrl = SortReplaceObject.Replace(PageOneUrl, "")

	'''''''''''''''''''''
	'Back to XML Parsing
	'''''''''''''''''''''

	'User Search Depth - this creates the links shown after Search: above the results, it allows a user to see what refinements they've done
	'For this we have to use the ScratchUrl since it cannot use the query string, which would have the refines in it already

	Set XMLSearchDepth = xmlDOM.getElementsByTagName("user_search_depth")
	Set XMLSearchDepth = XMLSearchDepth.Item(0)


	SearchString = "<A href='" & ScratchUrl & "'>" & SearchWordToDisplay & "</A> > "

	DepthUrl = ScratchUrl & "&refine=y"
	For Each XMLSearchDepthElement In XMLSearchDepth.childNodes
	'Child nodes(1) = key, (2) = value
		DepthUrl = DepthUrl & "&" & XMLSearchDepthElement.childNodes(1).text & "=" & XMLSearchDepthElement.childNodes(2).text
		SearchString = SearchString & "<A href='" & DepthUrl & "'>" & XMLSearchDepthElement.childNodes(1).text & ": " &_
							XMLSearchDepthElement.childNodes(2).text & "</A> > "
	Next
	SearchString = Left(SearchString, Len(SearchString)-2) 'Removing the trailing >


	'Currently Sorted by
	Set XMLSortedBy = xmlDOM.getElementsByTagName("currently_sorted_by")
	Set XMLSortedBy = XMLSortedBy.Item(0)
	SortedBy = XMLSortedBy.childNodes(0).text

	'Fields to add to the sort-by dropdown
	Set XMLSortByOptions = xmlDOM.getElementsByTagName("sort_bys")
	Set XMLSortByOptions = XMLSortByOptions.Item(0)
	Dim SortByOptionsArray()
	ReDim SortByOptionsArray(XMLSortByOptions.childNodes.Length)
	For i = 0 To XMLSortByOptions.childNodes.Length-1
		Set XMLSortByOption = XMLSortByOptions.childNodes(i)
		SortByOptionsArray(i) = XMLSortByOption.childNodes(0).text
	next

	'Refinables and Results
	'This XML is parsed within the page creation because it is easier this way based on how ASP handles arrays

	'''''''''''''''''''''''''''
	' Results Creation
	'''''''''''''''''''''''''''
		'Creating the refines
		Set XMLRefines = xmlDOM.getElementsByTagName("refinable")
		If XMLRefines.Length > 0 Then
			RefinesHTML = RefinesHTML & "<TABLE width='100%' class=""smlText""><TR><TD><b>Refine your search</b></TD></TR><TR><TD bgColor='#c0c0c0'><TABLE width='100%' bgcolor=white><TR>"
			For i = 0 To XMLRefines.Length-1
				Set XMLRefine = XMLRefines.Item(i)
				RefinesHTML = RefinesHTML & "<TD valign='top' class=""smlText""><STRONG>By " & XMLRefine.childNodes(0).text & ":</STRONG><BR>"
				Set XMLRefineValues = XMLRefine.childNodes(1).getElementsByTagName("value")
				LinkString = PageOneUrl & "&refine=y&"
				Length = XMLRefineValues.Length-1
				If Length > 4 Then
					Length = 4
				End if
				For j = 0 To Length
					RefinesHTML = RefinesHTML & "<A class='copylink' href='" & LinkString & XMLRefine.childNodes(0).text & "=" & Server.URLencode(XMLRefineValues.Item(j).childNodes(0).text) & "'>" &_
								XMLRefineValues.Item(j).childNodes(0).text & "</A> (" & XMLRefineValues.Item(j).childNodes(1).text & ")<BR>"
				Next
				RefinesHTML = RefinesHTML & "<DIV id='" & XMLRefine.childNodes(0).text & "_div' style='display:none'>"
				For j = Length+1 To XMLRefineValues.Length-1
					RefinesHTML = RefinesHTML & "<A class='copylink' href='" & LinkString & XMLRefine.childNodes(0).text & "=" & Server.URLencode(XMLRefineValues.Item(j).childNodes(0).text) & "'>" &_
								XMLRefineValues.Item(j).childNodes(0).text & "</A> (" & XMLRefineValues.Item(j).childNodes(1).text & ")<BR>"
				Next
				RefinesHTML = RefinesHTML & "</DIV>"

				'RefinesHTML = RefinesHTML & "<A class='copylink' href='" & PageOneUrl & "'>more</A></TD>"
				If XMLRefineValues.Length-1 > 4 then
					RefinesHTML = RefinesHTML & "<A id='" & XMLRefine.childNodes(0).text & "_link' class='pagelink' href='javascript:showhide(""" & XMLRefine.childNodes(0).text & """)'>more</A></TD>"
				End if
			Next
			RefinesHTML = RefinesHTML & "</TD></TABLE></TD></TR></TABLE>"
		End if

		'Do the results shown and pages output
		ResultInfoHTML =  vbCrLf & "<TABLE><TR><TD valign=top NOWRAP class=""smlText""><B>" & ProductsShownMin & " - " & ProductsShownMax & "</B> of <B>" & TotalProducts & "</B> Search Results for:</TD><TD class=""smlText"">" & SearchString & "</TD></TR></TABLE>"
		'Previous page and first pages
		If TotalPages > 1 And CurrentPage > 1 Then
			Curr_Url = PageReplaceObject.Replace(PageOneUrl, "page=" & (CurrentPage-1))
			PagesHTML = PagesHTML & "<A href='" & PageOneUrl & "'>First</A> &nbsp;&nbsp;"
			PagesHTML = PagesHTML & "<A href='" & Curr_Url & "'>Prev</A> &nbsp;&nbsp;"
		End If
		'The pages
		If TotalPages <= 10 And TotalPages > 1 Then
			For i = 1 To TotalPages
				If i = CurrentPage Then
					PagesHTML = PagesHTML & i & " &nbsp;&nbsp;"
				Else
					Curr_Url = PageReplaceObject.Replace(PageOneUrl, "page=" & i)
					PagesHTML = PagesHTML & "<A class='pagelink' href='" & Curr_Url & "'>" & i & "</A> &nbsp;&nbsp;"
				End If
			Next
		ElseIf TotalPages > 1 then
			PStart = CurrentPage - 4
			If PStart < 1 Then
				PStart = 1
			ElseIf PStart > 1 Then
				PagesHTML = PagesHTML & "... &nbsp;&nbsp;"
			End If
			For i = PStart To CurrentPage-1
				Curr_Url = PageReplaceObject.Replace(PageOneUrl, "page=" & i)
				PagesHTML = PagesHTML & "<A class='pagelink' href='" & Curr_Url & "'>" & i & "</A> &nbsp;&nbsp;"
			Next
			PagesHTML = PagesHTML & i & " &nbsp;&nbsp;"
			PEnd = CurrentPage + 4
			If PEnd > TotalPages Then
				PEnd = TotalPages
			End If
			For i = CurrentPage+1 To PEnd
				Curr_Url = PageReplaceObject.Replace(PageOneUrl, "page=" & i)
				PagesHTML = PagesHTML & "<A class='pagelink' href='" & Curr_Url & "'>" & i & "</A> &nbsp;&nbsp;"
			Next
			If PEnd < TotalPages Then
				PagesHTML = PagesHTML & "... &nbsp;&nbsp;"
			End if
		End If
		'Next and last pages
		If TotalPages > 1 And CurrentPage  < TotalPages Then
			Curr_Url = PageReplaceObject.Replace(PageOneUrl, "page=" & (CurrentPage+1))
			PagesHTML = PagesHTML & "<A href='" & Curr_Url & "'>Next</A> &nbsp;&nbsp;"
			Curr_Url = PageReplaceObject.Replace(PageOneUrl, "page=" & TotalPages)
			PagesHTML = PagesHTML & "<A href='" & Curr_Url & "'>Last</A> &nbsp;&nbsp;"
		End if

		'Doing the sort by fields
		SortByHTML = SortByHTML & "Sort by: "
		Select Case Request("sort_by_field")
			Case ""
					SortByHTML = SortByHTML & "<B>relevance</B> | " &_
						 "<A href='" & PageOneNoSortUrl & "&sort_by_field=Price:ASC'>price <IMG border=0 src='/search/images/icon_sort_up.gif'></A> | " &_
						 "<A href='" & PageOneNoSortUrl & "&sort_by_field=Price:DESC'>price <IMG border=0 src='/search/images/icon_sort_down.gif'></A>"
			Case "Price:ASC"
					SortByHTML = SortByHTML & "<A href='" & PageOneNoSortUrl & "'>relevance</A> | " &_
						 "<B>price <IMG border=0 src='/search/images/icon_sort_up.gif'></B> | " &_
						 "<A href='" & PageOneNoSortUrl & "&sort_by_field=Price:DESC'>price <IMG border=0 src='/search/images/icon_sort_down.gif'></A>"
			Case "Price:DESC"
					SortByHTML = SortByHTML & "<A href='" & PageOneNoSortUrl & "'>relevance</A> | " &_
						 "<A href='" & PageOneNoSortUrl & "&sort_by_field=Price:ASC'>price<IMG border=0 src='/search/images/icon_sort_up.gif'></A> | " &_
						 "<B>price<IMG border=0 src='/search/images/icon_sort_down.gif'></B>"
		End Select

	'And finally the results output.
		'Display changing options
		ItemTemplate = Readfile(Server.mappath("Single_Entry_Template_Grid.html"))
		Set DisplayTypeReplaceObject = New RegExp
		With DisplayTypeReplaceObject
			.Pattern = "([&]*display_type=_(.*?)_|[&]*display_type=)"
			.IgnoreCase = True
			.Global = True
		End With
		NoDispQueryString = "search?" & DisplayTypeReplaceObject.Replace(FixedQueryString, "")
		Select Case Request("display_type")
			Case ""
				DisplayChangeHTML = DisplayChangeHTML & "<B>Grid</B> <IMG border=0 src='/search/images/largegrid.gif'> " &_
						"<A href='" & NoDispQueryString & "&display_type=_List_'><B>List</B></A> <IMG border=0 src='/search/images/listview.gif'>"
				rows = grid_rows
				cols = grid_cols
			Case "_List_"
					DisplayChangeHTML = DisplayChangeHTML &	"<A href='" & NoDispQueryString & "'><B>Grid</B></a> <IMG border=0 src='/search/images/largegrid.gif'> " &_
					"<B>List</b> <IMG border=0 src='/search/images/listview.gif'>"
				rows = RESULTS_PER_PAGE
				cols = 1
				ItemTemplate = Readfile(Server.mappath("Single_Entry_Template_List.html"))
		End Select

		ProductNum = 0
		'Now we go through the results and make the output
		keywordArr = Split(myKeywords, " ")
		Set XMLProducts = xmlDOM.getElementsByTagName("results")
		Set XMLProducts = XMLProducts.Item(0)



'		response.write Server.HTMLEncode(xmlDOM.xml)
		ResultsHTML = ResultsHTML & "<TABLE width='100%' border='0' bordercolor='#0000ff'>" & vbCrLf
		For y = 1 To rows
			ResultsHTML = ResultsHTML & "<TR>"
			For x = 1 To cols
				ResultsHTML = ResultsHTML & "<TD style=""padding:25px 3px 0px 3px; width:25%;"" valign=""top"">"
				If Not (XMLProducts.childNodes(ProductNum) Is NOTHING) Then
					'Reset the values here instead of doing a number of IF statements
					ProdName = ""
					ProdUrl = ""
					ProdImg = ""
					ProdPrice = ""
					ProdDescription = ""
					Set XMLProduct = XMLProducts.childNodes(ProductNum)
					For Each ProductElement In XMLProduct.childNodes
						Select Case ProductElement.nodeName
							'Fill this case statement with the elements of a result from your XML that you want to show
							Case "Urltoproductpage"
								ProdUrl = ProductElement.text
								'ProdUrl = ProdUrl & "&id=wesearch"
							Case "Name"
								ProdName = ProductElement.text
								For Each keywordpart In keywordArr
									ProdName = Highlight(ProdName, keywordpart, "<b>", "</b>")
								next
							Case "Image"
								ProdImg = ProductElement.text
							Case "Price"
								ProdPrice = ProductElement.text
             				Case "Lineitemprice"
								Lineitemprice = ProductElement.text
							Case "Description"
								ProdDescription = ProductElement.text
								For Each keywordpart In keywordArr
									ProdDescription = Highlight(ProdDescription, keywordpart, "<b>", "</b>")
								next
							Case "Sku"
								itemID = ProductElement.text
						End Select
					Next
					'Now have it replace the tags in the ItemTemplate with the values that were just obtained
					ItemContent = Replace(ItemTemplate, "[URL]", ProdUrl)
					ItemContent = Replace(ItemContent, "[NAME]", ProdName)
					ItemContent = Replace(ItemContent, "[IMG]", Replace(ProdImg, "/big/", "/thumb/"))
					ItemContent = Replace(ItemContent, "[PRICE]", ProdPrice)
					ItemContent = Replace(ItemContent, "[LINEITEMPRICE]", Lineitemprice)
					ItemContent = Replace(ItemContent, "[DESCRIPTION]", ProdDescription)
					ItemContent = Replace(ItemContent, "[ITEMID]", itemID)

					ResultsHTML = ResultsHTML & ItemContent & vbCrLf
				End If
				ResultsHTML = ResultsHTML & "</TD>"
				ProductNum = ProductNum + 1
			Next
			ResultsHTML = ResultsHTML & "</TR>" & vbCrLf
		Next
		ResultsHTML = ResultsHTML & "</TABLE><BR>" & vbCrLf

		Content = "<table width='100%'>" &_
				"<TR><TD valign=top width='100%'>" & Sugg_String & "</TD></TR>" &_
				"<TR><TD valign=top width='100%'>" & ResultInfoHTML & "</TD></TR>" &_
				"<TR><TD valign=top width='100%'>" & RefinesHTML & "</TD></TR>"

		If NoticeString <> "" Then
			Content = Content & NoticeString
		End if
		Content = Content &_
				"<TR><TD valign=top align=right width='100%' class=""smlText"" style='border-bottom: 1px solid #c0c0c0'>" & SortByHTML & "</TD></TR>" &_
		        "<TR><TD><TABLE WIDTH=""100%""><TD valign=top align=""left"" width='50%' class=""smlText"">" & PagesHTML & "</TD>" &_
				"<TD valign=top align=""right"" width='50%' class=""smlText"">" & DisplayChangeHTML & "</TD></TR></TABLE></TD></TR>" &_
				"<TR><TD valign=top width='100%'>" & ResultsHTML & "</TD></TR>" &_
				"<TR><TD valign=top align=right width='100%' class=""smlText"" style='border-bottom: 1px solid #c0c0c0'>" & SortByHTML & "</TD></TR>" &_
		        "<TR><TD><TABLE WIDTH=""100%""><TD valign=top align=""left"" width='50%' class=""smlText"">" & PagesHTML & "</TD>" &_
				"<TD valign=top align=""right"" width='50%' class=""smlText"">" & DisplayChangeHTML & "</TD></TR></TABLE></TD></TR>" &_
				"</TABLE>"

End If	'This ends the no products found if statement

Colors = Array("nokia","samsung","appleiphone","nextel","siemens","sanyo","blackberry","pantech","sidekick")

If in_array(myKeywords,Colors) Then
  str_html = Replace(str_html, "[LOGO]", "<img src=""/search/images/brands/" & trim(LCase(myKeywords)) & ".gif"" onerror=""this.style.display = 'none';"">" ,1)
Else
  str_html = Replace(str_html, "[LOGO]", "", 1)
End If


'str_html = Replace(str_html, "[TITLE]", "Site Search for """ & myKeywords & """",1)
str_html = Replace(str_html, "[CONTENT]", Content, 1)
str_html = Replace(str_html, "[MERCHANDIZING]", Merchandizing, 1)
Response.write str_html


Function Highlight(strText, strFind, strBefore, strAfter)
	Dim nPos
	Dim nLen
	Dim nLenAll

	Set KeywordStripReplaceObject = New RegExp
	With KeywordStripReplaceObject
		.Pattern = "[\\\/\<\>'""]"
		.IgnoreCase = True
		.Global = True
	End With
	strFind = KeywordStripReplaceObject.Replace(strFind, "")

	nLen = Len(strFind)
	nLenAll = nLen + Len(strBefore) + Len(strAfter) + 1

	Highlight = strText
	If nLen > 0 And Len(Highlight) > 0 Then
		nPos = InStr(1, Highlight, strFind, 1)
		Do While nPos > 0
			Highlight = Left(Highlight, nPos - 1) & _
				strBefore & Mid(Highlight, nPos, nLen) & strAfter & _
				Mid(Highlight, nPos + nLen)

			nPos = InStr(nPos + nLenAll, Highlight, strFind, 1)
		Loop
	End If
End Function

Function in_array(element, arr)
	For i=0 To Ubound(arr)
		If Trim(arr(i)) = Trim(element) Then
			in_array = True
			Exit Function
		Else
			in_array = False
		End If
	Next
End Function
%>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<!--#include virtual="/includes/template/bottom.asp"-->