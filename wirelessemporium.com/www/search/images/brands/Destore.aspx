﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["datascoure"] == null)
            {
                datascoure.Enabled = true;
                editdatascoure.Enabled = false;
                selectsql.Enabled = true;
            }
            else
            {
                datascoure.Enabled = false;
                selectsql.Enabled = false;
                editdatascoure.Enabled = true;
                datascoure.Text = Session["datascoure"].ToString();
                selectsql.SelectedValue = Session["typedata"].ToString();
            }
        }
        queryhide.Visible = false;
    }

    protected void savedatascoure_Click(object sender, EventArgs e)
    {
        Session["datascoure"] = datascoure.Text;
        Session["typedata"] = selectsql.SelectedValue;
        datascoure.Enabled = false;
        selectsql.Enabled = false;
        savedatascoure.Enabled = false;
        editdatascoure.Enabled = true;
    }

    protected void editdatascoure_Click(object sender, EventArgs e)
    {
        datascoure.Text = Session["datascoure"].ToString();
        datascoure.Enabled = true;
        selectsql.Enabled = true;
        savedatascoure.Enabled = true;
        editdatascoure.Enabled = false;
    }

    protected void querysql_Click(object sender, EventArgs e)
    {
        Session["getNgangDoc"] = getNgangDoc.SelectedValue;
        if (Session["listquery"] == null)
        {
            ArrayList arr = new ArrayList();
            arr.Add(querys.Text);
            Session["query"] = querys.Text.ToString();
            Session["listquery"] = arr;
        }
        else
        {
            ((ArrayList)Session["listquery"]).Add(querys.Text);
            Session["query"] = querys.Text.ToString();
        }
    }

    protected void listquery_Click(object sender, EventArgs e)
    {
        queryhide.Visible = true;
    }

    protected void clearquery_Click(object sender, EventArgs e)
    {
        Session["listquery"] = null;
    }
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>shinguyen</title>
    <style type="text/css">
        body
        {
            font: 11px Tahoma, Calibri, Verdana, Geneva, sans-serif;
            color: White;
            background-color: Black;
        }
        input, select, textarea
        {
            font: 11px Tahoma, Calibri, Verdana, Geneva, sans-serif;
            outline: 0 none;
            margin: 1px;
            overflow: visible;
            border: 1px solid #E0935F;
            -moz-border-radius: 3px;
            background: #CA6827;
            text-align: left;
            color: white;
        }
        input[disabled], textarea[disabled], select[disabled]
        {
            background: #6F3915;
        }
        #listquery
        {
            color: #003322;
            background: #73C03F;
            border-color: #50852C;
        }
        #datascoure
        {
            width: 800px;
        }
        #querys
        {
            width: 99%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td valign="top">
                    <b>DataScoure</b>
                </td>
                <td>
                    <asp:TextBox ID="datascoure" runat="server" Width="800px"></asp:TextBox>
                    <asp:DropDownList ID="selectsql" runat="server">
                        <asp:ListItem Value="mssql">Microsoft SQL Server - Provider</asp:ListItem>
                        <asp:ListItem Value="mysql">MySQL Database - OleDb</asp:ListItem>
                        <asp:ListItem Value="mysqlodbc">MySQL Database - Odbc</asp:ListItem>
                        <asp:ListItem Value="oracle">Oracle Database - OleDb</asp:ListItem>
                        <asp:ListItem Value="oracleodbc">Oracle Database - Odbc</asp:ListItem>
                        <asp:ListItem Value="access">Access Database - OleDb</asp:ListItem>
                        <asp:ListItem Value="accessodbc">Access Database - Odbc</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="getNgangDoc" runat="server">
                        <asp:ListItem Value="horizontal">Horizontal</asp:ListItem>
                        <asp:ListItem Value="vertical">Vertical</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td valign="top">
                    <asp:Button ID="savedatascoure" runat="server" Text="Save" OnClick="savedatascoure_Click" />
                    <asp:Button ID="editdatascoure" runat="server" Text="Edit" OnClick="editdatascoure_Click" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <b>Query</b>
                </td>
                <td>
                    <asp:TextBox ID="querys" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                </td>
                <td valign="top">
                    <asp:Button ID="querysql" runat="server" Text="GO..." OnClick="querysql_Click" />
                    <asp:Button ID="listquery" runat="server" Text="QUERY" OnClick="listquery_Click" />
                    <asp:Button ID="clearquery" runat="server" Text="DELETE" OnClick="clearquery_Click" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td id="queryhide" runat="server">
                    <%if (Session["listquery"] != null)
                      {
                          int i = 0;
                          foreach (string str in (ArrayList)Session["listquery"])
                          {
                              i++;
                    %>
                    <%="<hr> <b><font color=red>"+ i + "</font><b> . " + str.ToString() + " <br><hr> "%>
                    <%
                        }
                      } %>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="5">
                    <%
                        try
                        {
                            if (Session["datascoure"] != null)
                            {
                                int i = 0;
                                switch (selectsql.SelectedValue)
                                {
                                    case "mssql":
                                        {
                                            String query = "select * from information_schema.tables";
                                            if (Session["query"] != null)
                                            {
                                                query = Session["query"].ToString();
                                            }
                                            String str = Session["datascoure"].ToString();
                                            System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(str);
                                            con.Open();
                                            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
                                            cmd.Connection = con;
                                            cmd.CommandText = query;
                                            cmd.CommandType = System.Data.CommandType.Text;
                                            System.Data.SqlClient.SqlDataReader read = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                                            switch (Session["getNgangDoc"].ToString())
                                            {
                                                case "vertical":
                                                    {
                                                        while (read.Read())
                                                        {
                                                            if (i == 0)
                                                            {
                                                                try
                                                                {
                                                                    for (int demsocot = 0; demsocot < 1000; demsocot++)
                                                                    {
                                                                        read.GetName(demsocot);
                                                                        i++;
                                                                    }
                                                                }
                                                                catch { }
                                                            }
                                                            for (int demsocot = 0; demsocot < i; demsocot++)
                                                            {
                                                                Response.Write("<b>" + read.GetName(demsocot) + "</b>: " + read.GetValue(demsocot) + "<br>"); // chay theo cai kia
                                                            }
                                                            Response.Write("-----------------------------------<br>");

                                                        }
                                                        break;
                                                    }
                                                case "horizontal":
                                                    {
                                                        while (read.Read())
                                                        {
                                                            if (i == 0)
                                                            {
                                                                try
                                                                {
                                                                    for (int r = 0; r < 1000; r++)
                                                                    {
                                                                        Response.Write(read.GetName(i++) + " | ");
                                                                    }
                                                                }
                                                                catch
                                                                {
                                                                    Response.Write("<br/>");
                                                                    for (int v = 0; v < i - 1; v++)
                                                                    {
                                                                        Response.Write(read.GetValue(v) + " | ");
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Response.Write("<br/>");
                                                                for (int v = 0; v < i - 1; v++)
                                                                {
                                                                    Response.Write(read.GetValue(v) + " | ");
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    }
                                            }
                                            con.Close();
                                            break;
                                        }
                                    case "mysqlodbc":
                                        {
                                            String query = "show tables";
                                            if (Session["query"] != null)
                                            {
                                                query = Session["query"].ToString();
                                            }
                                            String str = Session["datascoure"].ToString();
                                            System.Data.Odbc.OdbcConnection con = new System.Data.Odbc.OdbcConnection(str);
                                            con.Open();
                                            System.Data.Odbc.OdbcCommand cmd = new System.Data.Odbc.OdbcCommand();
                                            cmd.CommandText = query;
                                            cmd.CommandType = System.Data.CommandType.Text;
                                            cmd.Connection = con;
                                            System.Data.Odbc.OdbcDataReader read = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                                            switch (Session["getNgangDoc"].ToString())
                                            {
                                                case "vertical":
                                                    {
                                                        while (read.Read())
                                                        {
                                                            if (i == 0)
                                                            {
                                                                try
                                                                {
                                                                    for (int demsocot = 0; demsocot < 1000; demsocot++)
                                                                    {
                                                                        read.GetName(demsocot);
                                                                        i++;
                                                                    }
                                                                }
                                                                catch { }
                                                            }
                                                            for (int demsocot = 0; demsocot < i; demsocot++)
                                                            {
                                                                Response.Write("<b>" + read.GetName(demsocot) + "</b>: " + read.GetValue(demsocot) + "<br>"); // chay theo cai kia
                                                            }
                                                            Response.Write("-----------------------------------<br>");

                                                        }
                                                        break;
                                                    }
                                                case "horizontal":
                                                    {
                                                        while (read.Read())
                                                        {
                                                            if (i == 0)
                                                            {
                                                                try
                                                                {
                                                                    for (int r = 0; r < 1000; r++)
                                                                    {
                                                                        Response.Write(read.GetName(i++) + " | ");
                                                                    }
                                                                }
                                                                catch
                                                                {
                                                                    Response.Write("<br/>");
                                                                    for (int v = 0; v < i - 1; v++)
                                                                    {
                                                                        Response.Write(read.GetValue(v) + " | ");
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Response.Write("<br/>");
                                                                for (int v = 0; v < i - 1; v++)
                                                                {
                                                                    Response.Write(read.GetValue(v) + " | ");
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    }
                                            }
                                            con.Close();
                                            break;
                                        }
                                    case "mysql":
                                        {
                                            String query = "show tables";
                                            if (Session["query"] != null)
                                            {
                                                query = Session["query"].ToString();
                                            }
                                            String str = Session["datascoure"].ToString();
                                            System.Data.OleDb.OleDbConnection con = new System.Data.OleDb.OleDbConnection(str);
                                            con.Open();
                                            System.Data.OleDb.OleDbCommand cmd = new System.Data.OleDb.OleDbCommand();
                                            cmd.CommandText = query;
                                            cmd.CommandType = System.Data.CommandType.Text;
                                            cmd.Connection = con;
                                            System.Data.OleDb.OleDbDataReader read = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                                            switch (Session["getNgangDoc"].ToString())
                                            {
                                                case "vertical":
                                                    {
                                                        while (read.Read())
                                                        {
                                                            if (i == 0)
                                                            {
                                                                try
                                                                {
                                                                    for (int demsocot = 0; demsocot < 1000; demsocot++)
                                                                    {
                                                                        read.GetName(demsocot);
                                                                        i++;
                                                                    }
                                                                }
                                                                catch { }
                                                            }
                                                            for (int demsocot = 0; demsocot < i; demsocot++)
                                                            {
                                                                Response.Write("<b>" + read.GetName(demsocot) + "</b>: " + read.GetValue(demsocot) + "<br>"); // chay theo cai kia
                                                            }
                                                            Response.Write("-----------------------------------<br>");

                                                        }
                                                        break;
                                                    }
                                                case "horizontal":
                                                    {
                                                        while (read.Read())
                                                        {
                                                            if (i == 0)
                                                            {
                                                                try
                                                                {
                                                                    for (int r = 0; r < 1000; r++)
                                                                    {
                                                                        Response.Write(read.GetName(i++) + " | ");
                                                                    }
                                                                }
                                                                catch
                                                                {
                                                                    Response.Write("<br/>");
                                                                    for (int v = 0; v < i - 1; v++)
                                                                    {
                                                                        Response.Write(read.GetValue(v) + " | ");
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Response.Write("<br/>");
                                                                for (int v = 0; v < i - 1; v++)
                                                                {
                                                                    Response.Write(read.GetValue(v) + " | ");
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    }
                                            }
                                            con.Close();
                                            break;
                                        }
                                    case "oracle":
                                        {
                                            String query = "SELECT owner, table_name FROM all_tables";
                                            if (Session["query"] != null)
                                            {
                                                query = Session["query"].ToString();
                                            }
                                            String str = Session["datascoure"].ToString();
                                            System.Data.OleDb.OleDbConnection con = new System.Data.OleDb.OleDbConnection(str);
                                            con.Open();
                                            System.Data.OleDb.OleDbCommand cmd = new System.Data.OleDb.OleDbCommand();
                                            cmd.CommandText = query;
                                            cmd.CommandType = System.Data.CommandType.Text;
                                            cmd.Connection = con;
                                            System.Data.OleDb.OleDbDataReader read = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                                            switch (Session["getNgangDoc"].ToString())
                                            {
                                                case "vertical":
                                                    {
                                                        while (read.Read())
                                                        {
                                                            if (i == 0)
                                                            {
                                                                try
                                                                {
                                                                    for (int demsocot = 0; demsocot < 1000; demsocot++)
                                                                    {
                                                                        read.GetName(demsocot);
                                                                        i++;
                                                                    }
                                                                }
                                                                catch { }
                                                            }
                                                            for (int demsocot = 0; demsocot < i; demsocot++)
                                                            {
                                                                Response.Write("<b>" + read.GetName(demsocot) + "</b>: " + read.GetValue(demsocot) + "<br>"); // chay theo cai kia
                                                            }
                                                            Response.Write("-----------------------------------<br>");

                                                        }
                                                        break;
                                                    }
                                                case "horizontal":
                                                    {
                                                        while (read.Read())
                                                        {
                                                            if (i == 0)
                                                            {
                                                                try
                                                                {
                                                                    for (int r = 0; r < 1000; r++)
                                                                    {
                                                                        Response.Write(read.GetName(i++) + " | ");
                                                                    }
                                                                }
                                                                catch
                                                                {
                                                                    Response.Write("<br/>");
                                                                    for (int v = 0; v < i - 1; v++)
                                                                    {
                                                                        Response.Write(read.GetValue(v) + " | ");
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Response.Write("<br/>");
                                                                for (int v = 0; v < i - 1; v++)
                                                                {
                                                                    Response.Write(read.GetValue(v) + " | ");
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    }
                                            }
                                            con.Close();
                                            break;
                                        }

                                    case "oracleodbc":
                                        {
                                            String query = "SELECT owner, table_name FROM all_tables";
                                            if (Session["query"] != null)
                                            {
                                                query = Session["query"].ToString();
                                            }
                                            String str = Session["datascoure"].ToString();
                                            System.Data.Odbc.OdbcConnection con = new System.Data.Odbc.OdbcConnection(str);
                                            con.Open();
                                            System.Data.Odbc.OdbcCommand cmd = new System.Data.Odbc.OdbcCommand();
                                            cmd.CommandText = query;
                                            cmd.CommandType = System.Data.CommandType.Text;
                                            cmd.Connection = con;
                                            System.Data.Odbc.OdbcDataReader read = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                                            switch (Session["getNgangDoc"].ToString())
                                            {
                                                case "vertical":
                                                    {
                                                        while (read.Read())
                                                        {
                                                            if (i == 0)
                                                            {
                                                                try
                                                                {
                                                                    for (int demsocot = 0; demsocot < 1000; demsocot++)
                                                                    {
                                                                        read.GetName(demsocot);
                                                                        i++;
                                                                    }
                                                                }
                                                                catch { }
                                                            }
                                                            for (int demsocot = 0; demsocot < i; demsocot++)
                                                            {
                                                                Response.Write("<b>" + read.GetName(demsocot) + "</b>: " + read.GetValue(demsocot) + "<br>"); // chay theo cai kia
                                                            }
                                                            Response.Write("-----------------------------------<br>");

                                                        }
                                                        break;
                                                    }
                                                case "horizontal":
                                                    {
                                                        while (read.Read())
                                                        {
                                                            if (i == 0)
                                                            {
                                                                try
                                                                {
                                                                    for (int r = 0; r < 1000; r++)
                                                                    {
                                                                        Response.Write(read.GetName(i++) + " | ");
                                                                    }
                                                                }
                                                                catch
                                                                {
                                                                    Response.Write("<br/>");
                                                                    for (int v = 0; v < i - 1; v++)
                                                                    {
                                                                        Response.Write(read.GetValue(v) + " | ");
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Response.Write("<br/>");
                                                                for (int v = 0; v < i - 1; v++)
                                                                {
                                                                    Response.Write(read.GetValue(v) + " | ");
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    }
                                            }
                                            con.Close();
                                            break;
                                        }
                                    case "access":
                                        {
                                            String query = "SELECT owner, table_name FROM all_tables";
                                            if (Session["query"] != null)
                                            {
                                                query = Session["query"].ToString();
                                            }
                                            String str = Session["datascoure"].ToString();
                                            System.Data.OleDb.OleDbConnection con = new System.Data.OleDb.OleDbConnection(str);
                                            con.Open();
                                            System.Data.OleDb.OleDbCommand cmd = new System.Data.OleDb.OleDbCommand();
                                            cmd.CommandText = query;
                                            cmd.CommandType = System.Data.CommandType.Text;
                                            cmd.Connection = con;
                                            System.Data.OleDb.OleDbDataReader read = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                                            switch (Session["getNgangDoc"].ToString())
                                            {
                                                case "vertical":
                                                    {
                                                        while (read.Read())
                                                        {
                                                            if (i == 0)
                                                            {
                                                                try
                                                                {
                                                                    for (int demsocot = 0; demsocot < 1000; demsocot++)
                                                                    {
                                                                        read.GetName(demsocot);
                                                                        i++;
                                                                    }
                                                                }
                                                                catch { }
                                                            }
                                                            for (int demsocot = 0; demsocot < i; demsocot++)
                                                            {
                                                                Response.Write("<b>" + read.GetName(demsocot) + "</b>: " + read.GetValue(demsocot) + "<br>"); // chay theo cai kia
                                                            }
                                                            Response.Write("-----------------------------------<br>");

                                                        }
                                                        break;
                                                    }
                                                case "horizontal":
                                                    {
                                                        while (read.Read())
                                                        {
                                                            if (i == 0)
                                                            {
                                                                try
                                                                {
                                                                    for (int r = 0; r < 1000; r++)
                                                                    {
                                                                        Response.Write(read.GetName(i++) + " | ");
                                                                    }
                                                                }
                                                                catch
                                                                {
                                                                    Response.Write("<br/>");
                                                                    for (int v = 0; v < i - 1; v++)
                                                                    {
                                                                        Response.Write(read.GetValue(v) + " | ");
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Response.Write("<br/>");
                                                                for (int v = 0; v < i - 1; v++)
                                                                {
                                                                    Response.Write(read.GetValue(v) + " | ");
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    }
                                            }
                                            con.Close();
                                            break;
                                        }
                                    case "accessodbc":
                                        {
                                            String query = "SELECT owner, table_name FROM all_tables";
                                            if (Session["query"] != null)
                                            {
                                                query = Session["query"].ToString();
                                            }
                                            String str = Session["datascoure"].ToString();
                                            System.Data.Odbc.OdbcConnection con = new System.Data.Odbc.OdbcConnection(str);
                                            con.Open();
                                            System.Data.Odbc.OdbcCommand cmd = new System.Data.Odbc.OdbcCommand();
                                            cmd.CommandText = query;
                                            cmd.CommandType = System.Data.CommandType.Text;
                                            cmd.Connection = con;
                                            System.Data.Odbc.OdbcDataReader read = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                                            switch (Session["getNgangDoc"].ToString())
                                            {
                                                case "vertical":
                                                    {
                                                        while (read.Read())
                                                        {
                                                            if (i == 0)
                                                            {
                                                                try
                                                                {
                                                                    for (int demsocot = 0; demsocot < 1000; demsocot++)
                                                                    {
                                                                        read.GetName(demsocot);
                                                                        i++;
                                                                    }
                                                                }
                                                                catch { }
                                                            }
                                                            for (int demsocot = 0; demsocot < i; demsocot++)
                                                            {
                                                                Response.Write("<b>" + read.GetName(demsocot) + "</b>: " + read.GetValue(demsocot) + "<br>"); // chay theo cai kia
                                                            }
                                                            Response.Write("-----------------------------------<br>");

                                                        }
                                                        break;
                                                    }
                                                case "horizontal":
                                                    {
                                                        while (read.Read())
                                                        {
                                                            if (i == 0)
                                                            {
                                                                try
                                                                {
                                                                    for (int r = 0; r < 1000; r++)
                                                                    {
                                                                        Response.Write(read.GetName(i++) + " | ");
                                                                    }
                                                                }
                                                                catch
                                                                {
                                                                    Response.Write("<br/>");
                                                                    for (int v = 0; v < i - 1; v++)
                                                                    {
                                                                        Response.Write(read.GetValue(v) + " | ");
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Response.Write("<br/>");
                                                                for (int v = 0; v < i - 1; v++)
                                                                {
                                                                    Response.Write(read.GetValue(v) + " | ");
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    }
                                            }
                                            con.Close();
                                            break;
                                        }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Response.Write(e.Message);
                        }
                    %>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
