<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>November Charger Sale!</title>
<link rel="stylesheet" href="/includes/css/style.css" type="text/css">
</head>
<body>

<table cellpadding="0" cellspacing="0" border="0" align="center" width="88%">
	<tr>
		<td width="100%" align="center" class="normaltext">
			<br><br><br>
			<h1>November Charger Sale!</h1>
			<p align="justify" class="normaltext">
				From now until 11/23, save an additional 15% off our already low prices on all cell phone chargers.  No coupon code needed: just add to your cart and save!
			</p>
			<p align="center"><a href="javascript:window.close();">CLOSE</a></p>
		</td>
	</tr>
</table>

</body>
</html>
