<!--#include virtual="/includes/asp/inc_basePage.asp"-->
<!DOCTYPE html>
<html>
	<head>
		<style type="text/css">
        .ReadMsgBody {
            width: 100%;
        }
        .ExternalClass {
            width: 100%;
        }
        </style>
	</head>
    <body>
    	<table border="0" bordercolor="#CCCCCC" cellpadding="10" cellspacing="0" width="605">
        	<tr>
				<%
                sql = "sp_topSellingProducts 0, 20, 10, 'UNI', 1"
                set rsRecommend = oConn.execute(sql)
                nCnt = 0
                strProducts = ""
                do until rsRecommend.eof
                    nCnt = nCnt + 1
                    rsItemid = rsRecommend("itemid")
                    rsItemDesc = rsRecommend("itemdesc")
                    rsItempic = "http://www.wirelessemporium.com/productpics/big/" & rsRecommend("itempic")
                    bPrice = rsRecommend("price_our")
                    bRetail = rsRecommend("price_retail")
                    bPromoPrice = bPrice - (bPrice*.15)
                    bPromoSaving = bRetail - bPromoPrice
                    rsPrice = formatcurrency(bPrice, 2)
                    rsRetail = formatcurrency(bRetail, 2)
                    rsPromoPrice = formatcurrency(bPromoPrice, 2)
                    rsPromoSaving = formatcurrency(bPromoSaving, 2)
                    rsTotalPercent = formatpercent(bPromoSaving / bRetail, 0)
                    productLink = "http://www.wirelessemporium.com/p-" & rsItemid & "-" & formatSEO(rsItemDesc) & "?utm_source=responsys&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15"
                    strProducts = 	strProducts &	"<td>" & vbcrlf & _
                                                    "	<table width=""190"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf & _
                                                    "		<tbody>" & vbcrlf & _
                                                    "			<tr>" & vbcrlf & _
                                                    "				<td width=""190"" height=""190"">" & vbcrlf & _
                                                    "					<a href=""" & productLink & """><img src=""" & rsItempic & """ style=""display:block;"" width=""190"" height=""190"" /></a>" & vbcrlf & _
                                                    "				</td>" & vbcrlf & _
                                                    "			</tr>" & vbcrlf & _
                                                    "			<tr>" & vbcrlf & _
                                                    "				<td>" & vbcrlf & _
                                                    "					<span style=""color:#999;font-size:12px;color:#ccc;text-decoration:line-through;font-family:Arial, sans-serif;"">Retail Price: " & rsRetail & "</span><br />" & vbcrlf & _
                                                    "					<span style=""color:#999;font-size:12px;color:#ccc;text-decoration:line-through;font-family:Arial, sans-serif;"">Our Price: " & rsPrice & "</span><br />" & vbcrlf & _
                                                    "				</td>" & vbcrlf & _
                                                    "			</tr>" & vbcrlf & _
                                                    "			<tr>" & vbcrlf & _
                                                    "				<td style=""padding:10px 0 0 0;"">" & vbcrlf & _
                                                    "					<span style=""color:#000;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;"">Promo Price:</span> <span style=""color:#ff6633;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;"">" & rsPromoPrice & "</span><br />" & vbcrlf & _
                                                    "					<span style=""color:#000;font-size:14px;font-family:Arial, sans-serif;"">You Save:</span> <span style=""margin:10px 0 0 0;color:#000;font-size:14px;font-family:Arial, sans-serif;font-weight:bold;"">" & rsTotalPercent & " (" & rsPromoSaving & ")</span>" & vbcrlf & _
                                                    "				</td>" & vbcrlf & _
                                                    "			</tr>" & vbcrlf & _
                                                    "			<tr>" & vbcrlf & _
                                                    "				<td style=""padding:10px 0 0 0;"">" & vbcrlf & _
                                                    "					<table width=""160"" border=""0"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf & _
                                                    "						<tbody>" & vbcrlf & _
                                                    "							<tr>" & vbcrlf & _
                                                    "								<td align=""center"" style=""background-color:#ff6633;border-radius:16px;padding:7px 0;"">" & vbcrlf & _
                                                    "									<a href=""" & productLink & """ style=""color:#fff;font-weight:bold;font-size:14px;font-family:Arial, sans-serif;text-decoration:none;"">SHOP NOW &raquo;</a>" & vbcrlf & _
                                                    "								</td>" & vbcrlf & _
                                                    "							</tr>" & vbcrlf & _
                                                    "						</tbody>" & vbcrlf & _
                                                    "					</table>" & vbcrlf & _
                                                    "				</td>" & vbcrlf & _
                                                    "			</tr>" & vbcrlf & _
                                                    "		</tbody>" & vbcrlf & _
                                                    "	</table>" & vbcrlf & _
                                                    "</td>"
                    if nCnt >= 6 then exit do
					if nCnt = 3 then strProducts = strProducts & "</tr><tr>"
                    rsRecommend.movenext
                loop
                
                response.Write(strProducts)
                call closeConn(oConn)
                %>
            </tr>
        </table>
    </body>
</html>