<!--#include virtual="/includes/asp/inc_basePage.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Your Phone Accessories Are So 2013. Update Your Look And Save 20% Off!</title>
<style type="text/css">
.ReadMsgBody {
	width: 100%;
}
.ExternalClass {
	width: 100%;
}
</style>
<style type="text/css"></style></head>

<body style="margin: 0; padding: 0; width: 100%; -webkit-text-size-adjust: none; -ms-text-size-adjust: none;">

<!-- Containter -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="background-color: #ebebeb;" width="100%" align="center"><!-- Content -->

      <table style="margin: 0;" width="640" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 150%; mso-line-height-rule: exactly;" width="640" height="60" align="left" valign="middle">Welcome To WirelessEmporium.com - New Savings Inside.<br>
            Having trouble viewing this email? <a style="color: #006699;" href="http://www.wirelessemporium.com/emails/welcome/index.htm?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank">Click here</a>. | One-click <a style="color: #006699;" href="http://www.wirelessemporium.com/unsubscribe" target="_blank">unsubscribe</a>.</td>
        </tr>
        <tr>
          <td style="background-color: #333333; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 12px;" width="640" height="30" align="center" valign="middle">Thank You For Subscribing. Welcome To Wireless Emporium!</td>
        </tr>
        <tr>
          <td style="background-color: #ffffff;border-bottom:1px solid #cccccc;" width="640" height="80" align="center" valign="middle"><a href="http://www.wirelessemporium.com/?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/we-header.gif" width="620" height="59" alt="WirelessEmporium.com - Fast, FREE Shipping | 90-Day Returns" border="0"></a></td>
        </tr>
        <tr>
			<td width="640" height="256"><img src="http://www.wirelessemporium.com/emails/welcome/images/thank-you.jpg" style="display: block;" border="0" alt="Thanks for joining our club" width="640" height="256" /></td>
		</tr>
        <tr>
          <td style="background-color: #ff9900;" width="640" height="100"><table width="640" border="0" cellspacing="0" cellpadding="0">
              <tbody><tr>
                <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;" width="79" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/phone-covers-faceplates?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-covers.gif" width="65" height="50" alt="Covers &amp; Skins" border="0"></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/phone-covers-faceplates?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank">Covers &amp; Skins</a></td>
                <td style="background-color: #ffce85;" width="1" height="100"></td>
                <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;" width="79" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/phone-pouches-carrying-cases?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-cases.gif" width="65" height="50" alt="Covers &amp; Skins" border="0"></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/phone-pouches-carrying-cases?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank">Cases &amp; Pouches</a></td>
                <td style="background-color: #ffce85;" width="1" height="100"></td>
                <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;" width="79" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/phone-batteries?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-batteries.gif" width="65" height="50" alt="Cell Phone Batteries" border="0"></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/phone-batteries?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank">Cell Phone Batteries</a></td>
                <td style="background-color: #ffce85;" width="1" height="100"></td>
                <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;" width="79" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/phone-chargers-cables?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-chargers.gif" width="65" height="50" alt="Chargers &amp; Cables" border="0"></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/phone-chargers-cables?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank">Chargers &amp; Cables</a></td>
                <td style="background-color: #ffce85;" width="1" height="100"></td>
                <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;" width="79" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/phone-screen-protectors-skins?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-screens.gif" width="65" height="50" alt="Screen Guards" border="0"></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/phone-screen-protectors-skins?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank">Screen Guards</a></td>
                <td style="background-color: #ffce85;" width="1" height="100"></td>
                <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;" width="79" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/phone-holsters-holders-mounts?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-mounts.gif" width="65" height="50" alt="Holsters &amp; Mounts" border="0"></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/phone-holsters-holders-mounts?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank">Holsters &amp; Mounts</a></td>
                <td style="background-color: #ffce85;" width="1" height="100"></td>
                <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;" width="79" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/bluetooth-headsets-handsfree?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-bluetooth.gif" width="65" height="50" alt="Handsfree Devices" border="0"></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/bluetooth-headsets-handsfree?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank">Handsfree Devices</a></td>
                <td style="background-color: #ffce85;" width="1" height="100"></td>
                <td style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;" width="80" height="100" align="center" valign="middle"><a href="http://www.wirelessemporium.com/phones-tablets?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank"><img style="display: block;" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-phones.gif" width="65" height="50" alt="Unlocked Phones" border="0"></a><a style="color: #ffffff; text-decoration: none;" href="http://www.wirelessemporium.com/phones-tablets?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank">Unlocked Phones</a></td>
              </tr>
            </tbody></table></td>
        </tr>
		<tr>
			<td style="background-color: #ffffff;padding:25px;font-family:Arial, sans-serif;margin:0;font-size:16px;" width="100%" align="center">
				Improve your phone with these high-quality & highly convenient accessories:
			</td>
		</tr>
		<tr>
          <td style="background-color: #cccccc;" width="640" height="1"></td>
        </tr>
		<tr>
			<td style="background-color: #ffffff;" width="640"><br />
				<table width="640" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
                        	<%
							sql = "sp_topSellingProducts 0, 20, 10, 'UNI', 1"
							set rsRecommend = oConn.execute(sql)
							nCnt = 0
							do until rsRecommend.eof
								nCnt = nCnt + 1
								rsItemid = rsRecommend("itemid")
								rsItemDesc = rsRecommend("itemdesc")
								rsItempic = "http://www.wirelessemporium.com/productpics/big/" & rsRecommend("itempic")
								rsPrice = formatcurrency(rsRecommend("price_our"), 2)
								rsRetail = formatcurrency(rsRecommend("price_retail"), 2)
								rsPromoPrice = formatcurrency(rsRecommend("price_our") - (rsRecommend("price_our")*.15), 2)
								rsPromoSaving = formatcurrency(rsRetail - rsPromoPrice, 2)
								rsTotalPercent = formatpercent(rsPromoPrice / rsRetail, 0)
								productLink = "http://www.wirelessemporium.com/p-" & rsItemid & "-" & formatSEO(rsItemDesc) & "?utm_source=internal&utm_medium=email2&utm_campaign=" & campaign_id & "&utm_promocode=" & promo_code
								strProducts = 	strProducts & 	"<td width=""210"" style=""padding:0 0 0 20px;"">" & vbcrlf & _
																"	<table width=""190"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf & _
																"		<tbody>" & vbcrlf & _
																"			<tr>" & vbcrlf & _
																"				<td width=""190"" height=""190"">" & vbcrlf & _
																"					<a href=""" & productLink & """><img src=""" & rsItempic & """ style=""display:block;"" width=""190"" height=""190"" /></a>" & vbcrlf & _
																"				</td>" & vbcrlf & _
																"			</tr>" & vbcrlf & _
																"			<tr>" & vbcrlf & _
																"				<td>" & vbcrlf & _
																"					<span style=""color:#999;font-size:12px;color:#ccc;text-decoration:line-through;font-family:Arial, sans-serif;"">Retail Price: " & rsRetail & "</span><br />" & vbcrlf & _
																"					<span style=""color:#999;font-size:12px;color:#ccc;text-decoration:line-through;font-family:Arial, sans-serif;"">Our Price: " & rsPrice & "</span><br />" & vbcrlf & _
																"				</td>" & vbcrlf & _
																"			</tr>" & vbcrlf & _
																"			<tr>" & vbcrlf & _
																"				<td style=""padding:10px 0 0 0;"">" & vbcrlf & _
																"					<span style=""color:#000;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;"">Promo Price:</span> <span style=""color:#ff6633;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;"">" & rsPromoPrice & "</span><br />" & vbcrlf & _
																"					<span style=""color:#000;font-size:14px;font-family:Arial, sans-serif;"">You Save:</span> <span style=""margin:10px 0 0 0;color:#000;font-size:14px;font-family:Arial, sans-serif;font-weight:bold;"">" & rsTotalPercent & " (" & rsPromoSaving & ")</span>" & vbcrlf & _
																"				</td>" & vbcrlf & _
																"			</tr>" & vbcrlf & _
																"			<tr>" & vbcrlf & _
																"				<td style=""padding:10px 0 0 0;"">" & vbcrlf & _
																"					<table width=""160"" border=""0"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf & _
																"						<tbody>" & vbcrlf & _
																"							<tr>" & vbcrlf & _
																"								<td align=""center"" style=""background-color:#ff6633;border-radius:16px;padding:7px 0;"">" & vbcrlf & _
																"									<a href=""" & productLink & """ style=""color:#fff;font-weight:bold;font-size:14px;font-family:Arial, sans-serif;text-decoration:none;"">SHOP NOW &raquo;</a>" & vbcrlf & _
																"								</td>" & vbcrlf & _
																"							</tr>" & vbcrlf & _
																"						</tbody>" & vbcrlf & _
																"					</table>" & vbcrlf & _
																"				</td>" & vbcrlf & _
																"			</tr>" & vbcrlf & _
																"		</tbody>" & vbcrlf & _
																"	</table>" & vbcrlf & _
																"</td>"
								if nCnt = 3 then
									strProducts = strProducts & "				</tr>" & vbcrlf & _
																"			</tbody>" & vbcrlf & _
																"		</table>" & vbcrlf & _
																"		<br />" & vbcrlf & _
																"	</td>" & vbcrlf & _
																"</tr>" & vbcrlf & _
																"<tr>" & vbcrlf & _
																"  <td style=""background-color: #cccccc;"" width=""640"" height=""1""></td>" & vbcrlf & _
																"</tr>" & vbcrlf & _
																"<tr>" & vbcrlf & _
																"	<td style=""background-color: #ffffff;"" width=""640""><br />" & vbcrlf & _
																"		<table width=""640"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf & _
																"			<tbody>" & vbcrlf & _
																"				<tr>"
								end if
								if nCnt >= 6 then exit do
								rsRecommend.movenext
							loop
							
							response.Write(strProducts)
							%>
						</tr>
					</tbody>
				</table>
				<br />
			</td>
		</tr>
		<tr>
          <td style="background-color: #cccccc;" width="640" height="1"></td>
        </tr>
		<tr>
			<td style="background-color:#ffffff;padding:10px 20px;font-family:Arial, sans-serif;">
				<table width="600" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td style="padding:10px 10px 10px 0;" align="left">
								<table width="290" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td style="background-color:#333333;color:#fff;padding:10px;font-weight:bold;font-size:16px;" align="center">Factory-Direct Prices</td>
										</tr>
										<tr>
											<td style="background-color:#ebebeb;padding:20px;font-size:14px;line-height:20px;" align="left">You can easily save up to 72% OFF retail prices. We pass the savings along to you!</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td style="padding:10px 0 10px 10px;" align="right">
								<table width="290" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td style="background-color:#333333;color:#fff;padding:10px;font-weight:bold;font-size:16px;" align="center">Fast & Free Shipping</td>
										</tr>
										<tr>
											<td style="background-color:#ebebeb;padding:20px;font-size:14px;line-height:20px;" align="left">All orders placed before 3:00 p.m. EST are shipped same day. We get your merchandise in your hands ASAP.</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding:10px 10px 10px 0;" align="left">
								<table width="290" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td style="background-color:#333333;color:#fff;padding:10px;font-weight:bold;font-size:16px;" align="center">90-Day No-Hassle Returns</td>
										</tr>
										<tr>
											<td style="background-color:#ebebeb;padding:20px;font-size:14px;line-height:20px;" align="left">Don't like it? Don't sweat it! We offer a 90-day return window on all of our merchandise. We will make things right!</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td style="padding:10px 0 10px 10px;" align="right">
								<table width="290" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td style="background-color:#333333;color:#fff;padding:10px;font-weight:bold;font-size:16px;" align="center">1-Year Warranty</td>
										</tr>
										<tr>
											<td style="background-color:#ebebeb;padding:20px;font-size:14px;line-height:20px;" align="left">All products are backed by an iron-clad 100% Quality Assurance Guarantee. Your merchandise will be defect-free!</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding:10px 10px 10px 0;" align="left">
								<table width="290" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td style="background-color:#333333;color:#fff;padding:10px;font-weight:bold;font-size:16px;" align="center">Huge Selection of Products</td>
										</tr>
										<tr>
											<td style="background-color:#ebebeb;padding:20px;font-size:14px;line-height:20px;" align="left">From Apple to ZTE and everything in between, we carry over 250,000+ products for hundreds of devices.</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td style="padding:10px 0 10px 10px;" align="right">
								<table width="290" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td style="background-color:#333333;color:#fff;padding:10px;font-weight:bold;font-size:16px;" align="center">Sales & Discounts</td>
										</tr>
										<tr>
											<td style="background-color:#ebebeb;padding:20px;font-size:14px;line-height:20px;" align="left">We offer special discounts to our most loyal customers. Check your inbox often for exclusive deals.</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
      </tbody></table>

      <!-- Footer Table -->

      <table width="640" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <td align="center">
          	<table>
            	<tr>
                    <td width="10"></td>
                    <td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 16px; line-height: 150%; mso-line-height-rule: exactly;" width="300" align="center"><p><b>Connect with us:</b></p>
                    	<table width="300" border="0" cellspacing="0" cellpadding="0">
                    		<tr>
                    			<td width="75" align="center"><a href="https://www.facebook.com/WirelessEmporium?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank"><img style="display: block" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-facebook-2x.gif" width="16" height="34" alt="Facebook" border="0" /></a></td>
                    			<td width="75" align="center"><a href="https://twitter.com/wirelessemp/?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank"><img style="display: block" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-twitter-2x.gif" width="42" height="34" alt="Twitter" border="0" /></a></td>
                    			<td width="75" align="center"><a href="https://plus.google.com/113217319312103488691?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank"><img style="display: block" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-google-2x.gif" width="39" height="34" alt="Google+" border="0" /></a></td>
                    			<td width="75" align="center"><a href="http://www.wirelessemporium.com/blog/?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank"><img style="display: block" src="http://www.wirelessemporium.com/images/email/blast/welcome/images/ico-blog-2x.gif" width="34" height="34" alt="Blog" border="0" /></a></td>
                    		</tr>
                    	</table>
                    </td>
                    <td width="10"></td>
                </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 150%; mso-line-height-rule: exactly; margin: 0;text-align:center;" align="left">
			<br />
            <b>To place a Phone Order call 1-888-876-3137.</b><br>
			<br>
			If you have any questions, comments or concerns, please contact our customer service team at <a href="#" style="color:#006699;text-decoration:underline;">service@wirelessemporium.com</a>.<br>
			<br>
			Our customer service hours of operation are Monday through Friday 6:00 a.m. - 6:00 p.m. PST.<br>
			<br>			
			Offer excludes Bluetooth headsets, custom cases, phones and select OEM products.<br>
            <br>
            This email correspondence has been sent to you as you have indicated you would like to receive special, exclusive offers from WirelessEmporium.com. If this message has been sent in error, or you would no longer like to receive any more periodic offers from WirelessEmporium.com, you may <a style="color: #006699;" href="http://www.wirelessemporium.com/unsubscribe" target="_blank">unsubscribe</a>.<br>
            <br>
            <a style="color: #006699;" href="http://www.wirelessemporium.com/privacy?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank">Privacy Policy</a> | <a style="color: #006699;" href="http://www.wirelessemporium.com/termsofuse?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank">Terms &amp; Conditions</a> | <a style="color: #006699;" href="http://www.wirelessemporium.com/contactus?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank">Contact Us</a><br>
            <br>
            1410 N. Batavia St. | Orange, CA 92867<br>
            <br>
            &copy;2002-2014 WirelessEmporium.com<br>
            <br></td>
        </tr>
      </tbody></table>

      <!-- End Content --></td>
  </tr>
</tbody></table>
<!-- End Container -->

<map name="Map" id="Map">
  <area shape="rect" coords="0,0,11,23" href="http://www.facebook.com/WirelessEmporium/?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank" alt="Facebook" title="Facebook">
  <area shape="rect" coords="33,0,66,23" href="http://twitter.com/#!/wirelessemp/?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank" alt="Twitter" title="Twitter">
  <area shape="rect" coords="82,0,105,23" href="https://plus.google.com/113217319312103488691?rel=author&utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank" alt="Google+" title="Google+">
  <area shape="rect" coords="126,0,149,23" href="http://www.wirelessemporium.com/blog/?utm_source=internal&utm_medium=email&utm_campaign=TRANS&utm_content=welcome&utm_promocode=WELCOME15" target="_blank" alt="Blog" title="Blog">
</map>
<script type="text/javascript" async="" src="http://www.wirelessemporium.com/emails/welcome/images/_Incapsula_Resource"></script><script type="text/javascript">
//<![CDATA[
(function() {
var _analytics_scr = document.createElement('script');
_analytics_scr.type = 'text/javascript'; _analytics_scr.async = true; _analytics_scr.src = '/_Incapsula_Resource?SWJIYLWA=2977d8d74f63d7f8fedbea018b7a1d05&ns=1';
var _analytics_elem = document.getElementsByTagName('script')[0]; _analytics_elem.parentNode.insertBefore(_analytics_scr, _analytics_elem);
})();
// ]]>
</script>

</body></html>
<%
call closeConn(oConn)
%>