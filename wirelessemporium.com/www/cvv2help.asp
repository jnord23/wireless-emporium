<%
'::	Everest E-Commerce Advanced Edition
':: Copyright � 1999 - 2004 iCode, Inc. All rights reserved.
'::	Template: AMAZING
%>

<HTML>
	<HEAD>
		<TITLE>Verification Number (CVV2) Help</TITLE>
		<!-- ****** Style sheet included ****** -->
			<!--include file = style.css -->
	</HEAD>
	<BODY>
		<TABLE cellSpacing="0" cellPadding="1" width="100%" bgColor="#000000" border="0">
				<TR>
					<TD>
						<TABLE cellSpacing="1" cellPadding="1" width="100%" bgColor="#ffffff" border="0">
								<TR>
									<TD class="siteNav5TD" vAlign="top"><FONT class="contbold">What is CVV2 (or Security Code) ?</FONT></TD>
								</TR>
								<TR>
									<TD vAlign="top"><P align="Justify"><FONT class="contsmall">CVV2 is a security measure we require for all
											transactions. Since a CVV2 number is listed on your credit card, but is not
											stored anywhere, the only way to know the correct CVV2 number for your credit
											card is to physically have possession of the card itself. All VISA, MasterCard
											and American Express cards made in America in the past 5 years or so have a
											CVV2 number. </FONT></P>
									</TD>
								</TR>
								<TR>
									<TD>&nbsp;</TD>
								</TR>
								<TR>
									<TD class="siteNav5TD" vAlign="top"><FONT class="contbold">How to find your CVV2 (or
											Security Code) number ?</FONT></TD>
								</TR>
								<TR>
									<TD vAlign="top"><P align="Justify"><FONT class="contsmall">On a Visa or MasterCard, please turn your card
											over and look in the signature strip. You will find (either the entire 16-digit
											string of your card number, OR just the last 4 digits), followed by a space,
											followed by a 3-digit number. That 3-digit number is your CVV2 number. (See
											below) On American Express Cards, the CVV2 number is a 4-digit number that
											appears above the end of your card number. (See below) </FONT></P>
									</TD>
								</TR>
								<TR>
									<TD>&nbsp;</TD>
								</TR>
								<TR>
									<TD vAlign="top">
										<TABLE cellSpacing="1" cellPadding="3" border="0">
												<TR>
													<TD class="siteNav5TD" colSpan="2"><FONT class="contbold">Visa &amp; MasterCard :</FONT></TD>
												</TR>
												<TR>
													<TD><IMG src="/images/cvv2_visa.gif"></TD>
													<TD><P align="Justify"><FONT class="contsmall">This number is printed on your MasterCard &amp; Visa cards in
															the signature area of the back of the card. (it is the last 3 digits AFTER the
															credit card number in the signature area of the card). IF YOU CANNOT READ YOUR
															CVV2 NUMBER, YOU WILL HAVE TO CONTACT THE ISSUING CREDITOR. </FONT></P>
													</TD>
												</TR>
												<TR>
													<TD colSpan="2">&nbsp;</TD>
												</TR>
												<TR>
													<TD class="siteNav5TD" colSpan="2"><FONT class="contbold">American Express :</FONT></TD>
												</TR>
												<TR>
													<TD><IMG src="/images/cvv2_amex.gif"></TD>
													<TD><P align="Justify"><FONT class="contsmall">American Express cards show the CVV2 printed above and to the
															right of the imprinted card number on the front of the card. </FONT></P>
													</TD>
												</TR>
											</TABLE>
									</TD>
								</TR>
								<TR>
									<TD vAlign="top" align="middle"><FONT class="contbold"><A onClick="window.close(); return false;" href="">Close this window.</A></FONT>
									</TD>
								</TR>
							</TABLE>
					</TD>
				</TR>
			</TABLE>
	</BODY>
</HTML>
