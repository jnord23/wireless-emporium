<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<%
response.buffer = true

dim brandID, brand_URL
brandID = prepInt(request.querystring("brandID"))
brand_URL = brandSEO(brandID)
if brandID = 0 then
	call PermanentRedirect("/")
elseif brand_URL <> "" then
	call PermanentRedirect(brand_URL)
else
	call PermanentRedirect("/")
end if
%>
