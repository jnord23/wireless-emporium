<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_301Redirect.asp"-->
<%
	response.buffer = true

	set fs = CreateObject("Scripting.FileSystemObject")
	
	categoryid = 7
	brandid = 17
	phoneOnly = 1
	
	leftGoogleAd = 1
	googleAds = 1
	strIPhoneModels = "493,613,968,1120,1267,1412"
	
	if categoryid > 999 then
		sql = "SELECT subTypeName typename, subtypeid, typeid, hasUniversal from v_subTypeMatrix WHERE subtypeid = '" & categoryid & "'"
	else
		sql = "SELECT typename, typeid, subtypeid, hasUniversal from v_subTypeMatrix WHERE typeid = '" & categoryid & "'"
	end if	
	session("errorSQL") = sql
	strSubTypeID = ""
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	if rs.eof then
		call fCloseConn()
		call PermanentRedirect("/?ec=405")
	else
		categoryName = RS("typeName")
		parentTypeID = RS("typeid")
		isUniversal = RS("hasUniversal")
		do until RS.eof
			strSubTypeID = strSubTypeID & RS("subtypeid") & ","
			RS.movenext
		loop
		strSubTypeID = left(strSubTypeID, len(strSubTypeID) - 1)
	end if
	if strSubTypeID = "" then strSubTypeID = "9999999" end if
	
	'=========================================================================================
	Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
		oParam.CompareMode = vbTextCompare
		oParam.Add "x_brandID", brandid
		oParam.Add "x_categoryID", categoryid
		oParam.Add "x_brandName", "apple-iphone"
		oParam.Add "x_categoryName", categoryName	
	call redirectURL("cb", "", request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
	'=========================================================================================
	
	sql	=	"select	a.modelid, a.modelname" & vbcrlf & _
			"	,	case when a.modelid = 1120 then 'iphone_4_verizon__.jpg' " & vbcrlf & _
			"			when a.modelid = 968 then 'iphone_4_att__.jpg' " & vbcrlf & _
			"			else a.modelimg end modelimg" & vbcrlf & _			
			"	,	a.topModel, a.oldModel, a.international, b.brandname, b.brandimg " & vbcrlf & _
			"	,	cast(case when a.topModel <> 1 and a.oldModel <> 1 and a.international <> 1 then 1 else 0 end as bit) displayModel" & vbcrlf & _
			"	,	case when a.modelid = 1120 then 800 else modelid end orderNum" & vbcrlf & _
			"from 	we_models a with (nolock) join we_brands b with (nolock) " & vbcrlf & _
			"	on 	a.brandid = b.brandid " & vbcrlf & _
			"where 	a.hidelive = 0" & vbcrlf & _
			"	and	a.modelid in (" & strIPhoneModels & ")" & vbcrlf & _
			"order by orderNum desc" & vbcrlf
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1	

	if rs.eof then response.redirect("/") end if
	
	if SEtitle = "" then SEtitle = "Apple iPhone Cases: Discounted Leather Carrying Cases & Pouches"
	if SEdescription = "" then SEdescription = "Check out our wide selection of Apple iPhone Leather Carrying Cases & Pouches available at discounted prices and with free shipping at WirelessEmporium."
	if SEkeywords = "" then SEkeywords = "iphone cases, apple iphone cases, iphone leather cases, iphone pouches, iphone carrying cases"
	if strH1 = "" then strH1 = "iPhone Cases & Pouches"
	
	session("breadcrumb_model") = ""
%>
<!--#include virtual="/includes/template/top_brand.asp"-->
<script src="/includes/js/jquery-1.6.2.min.js"></script>    
<link rel="stylesheet" type="text/css" href="/includes/css/slides.css" />    
<script src="/includes/js/slides/slides.min.jquery.js"></script>
<script>
	$(function(){
		$('#iphone-latest').slides({
			preload: true,
			preloadImage: '/images/preloading.gif',
			generatePagination: false,
			prev: 'iphone-arrow-left',
			next: 'iphone-arrow-right'
		});

		$('#iphone-topselling').slides({
			preload: true,
			preloadImage: '/images/preloading.gif',
			generatePagination: false,
			prev: 'iphone-arrow-left',
			next: 'iphone-arrow-right'
		});		
	});
</script>   
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
	<tr>
	    <td align="left" valign="top" width="100%" class="breadcrumbFinal">
			<a class="breadcrumb" href="/index.asp">HOME</a>&nbsp;&gt;&nbsp;
            <a class="breadcrumb" href="/<%=categorySEO(categoryID)%>"><%=categoryName%></a>&nbsp;&gt;&nbsp;
            iPhone Cases & Pouches
        </td>    
    </tr>
    <tr>
    	<td width="100%" style="padding:15px 0px 15px 5px;">
			<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
            	<tr>
                	<td width="456" align="left" valign="top"><img src="/images/brands/iphone/iphone_logo.png" border="0" width="263" title="iPhone pouches and cases" alt="iPhone pouches and cases" /></td>
                    <td rowspan="3" width="220" valign="middle"><img src="/images/categoryBrand/iphone_cases/mainCase.jpg" border="0" width="220" title="iPhone pouches and cases" alt="iPhone pouches and cases" /></td>
                </tr>
            	<tr>
                	<td width="100%" align="left" valign="top" style="padding:10px 0px 10px 0px;">
                    	<div style="font-size:20px; color:#343434; font-weight:bold;">
                        	<h1 style="font-size:20px; color:#343434; font-weight:bold;"><%=seH1%></h1>
						</div>
                    	<div style="font-size:13px; color:#666666; line-height:150%;">
							<%
							if SeTopText <> "" then
								response.Write(SeTopText)
                            else
							%>
                            	<strong>iPhone Cases</strong> are the best way to provide your device with strong & durable protection, 
                                while updating your phone's look with a sleek new design. From bluetooth keyboard cases to designer 
                                cases for the iPhone, we have everything you need to give your iPhone the dependable, shock-proof 
                                protection you're looking for.
                            <% end if %>
                        </div>
                    </td>
                </tr>
            	<tr>
                	<td width="100%" align="left" valign="top" style="padding-top:10px;"><img src="/images/brands/iphone/select_button.png" border="0" width="265" title="Select your iPhone below" alt="Select your iPhone below" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td width="100%">
			<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
            	<tr>
                <%
				a = 0
				do until rs.eof
					a = a + 1
					modelid = rs("modelid")
					modelimg = rs("modelimg")
					brandName = rs("brandName")
					modelName = rs("modelName")
					altTag = replace(rs("modelName"),"/"," / ") & " accessories"
					styleBorder = ""
					if a < 3 then styleBorder = "border-right:1px solid #ccc;"
					%>
                    <td width="139" align="center" valign="top" style="padding:5px; border-bottom:1px solid #ccc; <%=styleBorder%>">
                    	<div><a href="/sb-<%=brandid%>-sm-<%=formatSEO(modelid)%>-sc-<%=categoryid%>-<%=formatSEO(categoryName)%>-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>.asp" title="<%=altTag%>"><img src="/productPics/models/thumbs/<%=modelimg%>" border="0" alt="<%=altTag%>" title="<%=altTag%>" /></a></div>
                        <div style="padding-top:10px;">
                        	<a href="/sb-<%=brandid%>-sm-<%=formatSEO(modelid)%>-sc-<%=categoryid%>-<%=formatSEO(categoryName)%>-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>.asp" title="<%=altTag%>">
								<%if fs.FileExists(server.MapPath("/images/brands/iphone/btn_" & modelid & ".png")) then%>
                                    <img src="/images/categoryBrand/iphone_cases/btn_<%=modelid%>.png" border="0" alt="<%=altTag%>" title="<%=altTag%>" />
                                <%else%>
                                    <span style="font-size:16px; font-weight:bold; color:#333;"><%=modelName & " Cases"%></span>
                                <%end if%>
							</a>
						</div>
					</td>
                    <%
					if a = 3 then 
						response.write "</tr><tr>"
						a = 0
					end if
					rs.movenext
				loop
				%>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <%
	sql	=	"select	top 12 x.itemid, x.itempic, x.itemdesc, x.price_retail, x.price_our" & vbcrlf & _
			"from	(" & vbcrlf & _
			"		select	partnumber, max(itemid) itemid" & vbcrlf & _
			"		from	we_items a join v_subtypeMatrix v" & vbcrlf & _
			"			on	a.subtypeid = v.subtypeid" & vbcrlf & _
			"		where	a.hidelive = 0" & vbcrlf & _
			"			and	a.inv_qty <> 0" & vbcrlf & _
			"			and	a.modelid in (" & strIPhoneModels & ")" & vbcrlf & _
			"			and	v.typeid = 7" & vbcrlf & _
			"		group by partnumber	" & vbcrlf & _
			"		) b cross apply" & vbcrlf & _
			"		(" & vbcrlf & _
			"		select	top 1 a.itemid, a.itempic, a.itemdesc, a.price_retail, a.price_our" & vbcrlf & _
			"		from	we_items a with (nolock)" & vbcrlf & _
			"		where	a.partnumber = b.partnumber" & vbcrlf & _
			"			and	a.hidelive = 0" & vbcrlf & _
			"			and	a.inv_qty <> 0" & vbcrlf & _
			"			and	a.modelid in (" & strIPhoneModels & ")" & vbcrlf & _
			"		order by a.itemid desc	) x" & vbcrlf & _
			"order by x.itemid desc" & vbcrlf
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1	
	%>
    <tr>
    	<td width="100%" class="iphone-header-box" align="left">
			&nbsp;&nbsp;<h2 style="font-size:18px; font-weight:bold; color:#fff;">Latest iPhone Cases & Pouches</h2>
		</td>
	</tr>
    <tr>
        <td align="left" width="100%">
            <div style="width:748px; height:230px; position:relative;">
                <div id="iphone-latest">
                    <div class="iphone-arrow-left" style="margin-top:50px;"></div>
                    <div style="float:left; width:678px; height:220px; padding:5px;">                        
                        <div class="slides_container">
                            <div class="slide">
							<%
							a = 0
							do until rs.eof
								a = a + 1
								itemid = rs("itemid")
								altText = rs("itemdesc")
								itemdesc = rs("itemDesc")
								price_retail = formatcurrency(rs("price_retail"),2)
								price_our = formatcurrency(rs("price_our"),2)
								productLink = "/p-" & itemid & "-" & formatSEO(itemdesc) & ".asp"
								itempic = rs("itempic")
								%>
                                <div class="item" align="center">
                                    <div style="width:149px; padding-top:2px;" align="center">
                                    	<a href="<%=productLink%>" title="<%=altText%>"><div style="background: url(/productpics/thumb/<%=itemPic%>) 0px -0px no-repeat; width:100px; height:100px;" title="<%=altText%>"></div></a>
                                    </div>
                                    <div style="width:129px; padding:5px 10px 0px 10px;" align="left"><a href="<%=productLink%>" class="cellphone-link" title="<%=altText%>"><h3 style="font-size:12px;"><%=itemdesc%></h3></a></div>
                                    <div style="width:129px; padding:5px 10px 0px 10px;" align="left" class="index-price-retail">Was <del><%=price_retail%></del></div>
                                    <div style="width:129px; padding:0px 10px 0px 10px;" align="left" class="index-price-our">Our Price <%=price_our%></div>
                                    <div style="width:129px; padding:0px 10px 0px 10px;" align="left"><b>Save <%=formatPercent((price_Retail - price_Our) / price_Retail,0)%></b></div>
                                </div>
                                <%
								if ((a mod 4) = 0) and (a < 12) then
									response.write "</div>" & vbcrlf & "<div class=""slide"">"
								end if
								rs.movenext
							loop
								%>
                            </div>
                        </div>
                    </div>
                    <div class="iphone-arrow-right" style="margin-top:50px;"></div>
                </div>
            </div>
        
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <%
	sql	=	"select	top 12 x.itemid, x.itempic, x.itemdesc, x.price_retail, x.price_our" & vbcrlf & _
			"from	we_models b cross apply" & vbcrlf & _
			"		(" & vbcrlf & _
			"		select	top 3 modelid, typeid, subtypeid, itemid, itempic, itemdesc, price_retail, price_our, numberofSales" & vbcrlf & _
			"		from	we_items with (nolock)" & vbcrlf & _
			"		where	modelid = b.modelid" & vbcrlf & _
			"			and	hidelive = 0" & vbcrlf & _
			"			and	inv_qty <> 0" & vbcrlf & _
			"			and	modelid in (493,613,968,1120,1267)" & vbcrlf & _
			"			and	subtypeid = 1030" & vbcrlf & _
			"		order by modelid desc, numberOfSales desc	) x" & vbcrlf & _
			"order by x.modelid desc, x.numberOfSales desc" & vbcrlf
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1	
	%>
    <tr>
    	<td width="100%" class="iphone-header-box" style="font-size:18px; font-weight:bold; color:#fff;" align="left">
			&nbsp;&nbsp;<h2 style="font-size:18px; font-weight:bold; color:#fff;">Top Selling iPhone Design Cases</h2>
		</td>
	</tr>
    <tr>
        <td align="left" width="100%">
            <div style="width:748px; height:230px; position:relative;">
                <div id="iphone-topselling">
                    <div class="iphone-arrow-left" style="margin-top:50px;"></div>
                    <div style="float:left; width:678px; height:220px; padding:5px;">                        
                        <div class="slides_container">
                            <div class="slide">
							<%
							a = 0
							do until rs.eof
								a = a + 1							
								itemid = rs("itemid")
								altText = rs("itemdesc")
								itemdesc = rs("itemDesc")
								price_retail = formatcurrency(rs("price_retail"),2)
								price_our = formatcurrency(rs("price_our"),2)
								productLink = "/p-" & itemid & "-" & formatSEO(itemdesc) & ".asp"
								itempic = rs("itempic")
								%>
                                <div class="item" align="center">
                                    <div style="width:149px; padding-top:2px;" align="center">
                                    	<a href="<%=productLink%>" title="<%=altText%>"><div style="background: url(/productpics/thumb/<%=itemPic%>) 0px -0px no-repeat; width:100px; height:100px;" title="<%=altText%>"></div></a>
                                    </div>
                                    <div style="width:129px; padding:5px 10px 0px 10px;" align="left"><a href="<%=productLink%>" class="cellphone-link" title="<%=altText%>"><h3 style="font-size:12px;"><%=itemdesc%></h3></a></div>
                                    <div style="width:129px; padding:5px 10px 0px 10px;" align="left" class="index-price-retail">Was <del><%=price_retail%></del></div>
                                    <div style="width:129px; padding:0px 10px 0px 10px;" align="left" class="index-price-our">Our Price <%=price_our%></div>
                                    <div style="width:129px; padding:0px 10px 0px 10px;" align="left"><b>Save <%=formatPercent((price_Retail - price_Our) / price_Retail,0)%></b></div>
                                </div>
                                <%
								if ((a mod 4) = 0) and (a < 12) then
									response.write "</div><div class=""slide"">"
								end if
								rs.movenext
							loop
								%>
                            </div>
                        </div>
                    </div>
                    <div class="iphone-arrow-right" style="margin-top:50px;"></div>
                </div>
            </div>
        </td>
    </tr>
    <!--
    <tr><td>&nbsp;</td></tr>
    <tr>
    	<td width="100%" class="iphone-header-box" style="font-size:18px; font-weight:bold; color:#fff;" align="left">
			&nbsp;&nbsp;<h2 style="font-size:18px; font-weight:bold; color:#fff;">Popular iPhone Cases</h2>
		</td>
	</tr>
    -->
    <tr><td>&nbsp;</td></tr>
    <%
	sql = 	"select	a.brandid, a.brandName, b.modelid, b.modelName, v.typeid, v.typeName, v.subtypeid, v.subTypeName" & vbcrlf & _
			"from	v_subtypeMatrix v cross join we_models b join we_brands a" & vbcrlf & _
			"	on	b.brandid = a.brandid " & vbcrlf & _
			"where	v.typeid = 3 and v.subtypeid <> 1064" & vbcrlf & _
			"	and	b.modelid in (1267, 968, 613)" & vbcrlf & _
			"order by b.modelid desc, v.subTypeOrderNum	" & vbcrlf
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	%>
    <tr>
    	<td width="100%" class="iphone-header-box" style="font-size:18px; font-weight:bold; color:#fff;" align="left">
			&nbsp;&nbsp;<h2 style="font-size:18px; font-weight:bold; color:#fff;">Shop Cases by iPhone Model</h2>
		</td>
	</tr>
    <tr>
        <td align="left">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td align="left" style="padding:3px; border-bottom:1px solid #ccc;">
                    	<%
						curModelID = -1
						a = 0
						do until rs.eof
							a = a + 1
							brandid = rs("brandid")
							brandName = rs("brandName")
							modelid = rs("modelid")
							modelName = rs("modelName")
							modelImg = "info_" & modelid & ".png"
							typeid = rs("typeid")
							parentTypeName = rs("typeName")
							subtypeid = rs("subtypeid")
							subtypeName = rs("subTypeName")
							catImg = modelid & "_" & subtypeid & ".jpg"
							modelTag = modelName & " cases"
							altTag = modelName & " " & subtypeName
							modelLink = "/sb-" & brandid & "-sm-" & modelid & "-sc-" & typeid & "-" & formatSEO(parentTypeName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
							subTypeLink = "/sb-" & brandid & "-sm-" & modelid & "-scd-" & subtypeid & "-" & formatSEO(subtypeName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
							
							if curModelID <> modelid then
							%>
							<div style="border-right:1px solid #ccc; border-bottom:1px solid #ccc; padding:5px; width:220px; float:left; height:160px;">
								<a href="<%=modelLink%>" title="<%=modelTag%>">
									<img src="/images/categoryBrand/iphone_cases/<%=modelImg%>" border="0" alt="<%=modelTag%>" title="<%=modelTag%>" />
								</a>
							</div>
							<%
								curModelID = modelid
								prodsInThisRow = 0
							end if
							
							if fs.fileExists(server.MapPath("/images/categoryBrand/iphone_cases/" & catImg)) then
								prodsInThisRow = prodsInThisRow + 1
								if prodsInThisRow < 6 then
							%>
							<div style="padding:5px 5px 5px 12px; width:85px; float:left; height:160px; border-bottom:1px solid #ccc;">
								<div>
									<a href="<%=subTypeLink%>" title="<%=altTag%>" style="padding-bottom:3px;">
										<img src="/images/categoryBrand/iphone_cases/<%=catImg%>" border="0" alt="<%=altTag%>" title="<%=altTag%>" />
									</a>
								</div>
								<div><a href="<%=subTypeLink%>" class="cellphone-link" title="<%=altTag%>"><h3 style="font-size:12px; font-weight:normal;"><%=altTag%></h3></a></div>
							</div>
							<%
								end if
							end if
							rs.movenext
						loop
						%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
    	<td width="100%" class="iphone-header-box" style="font-size:18px; font-weight:bold; color:#fff;" align="left">
			&nbsp;&nbsp;<h2 style="font-size:18px; font-weight:bold; color:#fff;">iPhone Cases Video Buyer's Guide</h2>
		</td>
	</tr>
    <tr>
    	<td width="100%" align="center" valign="top" style="padding-top:5px;">
			<iframe width="740" height="416" src="http://www.youtube.com/embed/Sn9PebdAIhI" frameborder="0" allowfullscreen></iframe>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
    	<td width="100%" class="iphone-header-box" style="font-size:18px; font-weight:bold; color:#fff;" align="left">
			&nbsp;&nbsp;<h3 style="font-size:18px; font-weight:bold; color:#fff;">Apple iPhone Cases | A Run-Down</h3>
		</td>
	</tr>
    <tr>
    	<td width="100%" align="left">
            <table border="0" align="center" cellspacing="0" cellpadding="5" width="100%">
                <tr>
                    <td colspan="2" align="left" width="50%" valign="top" style="padding:5px;">
                        <div style="text-align:justify;">
							Your great phone needs even better protection, and our <strong>iPhone Cases</strong> are here to deliver. With features like game-changing hardware and the ever-intuitive iOS operating system, 
                            keeping your Apple device protected is now more important than ever. We're ready to help with the biggest selection of rumble-ready iPhone accessories on the market. 
                            For quality protection at heavy discounts, you can count on Wireless Emporium to bring you only the best cases from across the globe. From waterproof cases to cute designer iPhone cases, 
                            you won't have a problem finding the accessories you need, and the features you want, in our selection of iPhone 3GS, 4 and 4s Cases.
                        </div>                    
                    </td>
				</tr>
                <tr>
                    <td align="left" width="50%" valign="top" style="padding:5px;">
						<h3 style="color:#000; font-weight:bold;">What are the best iPhone Cases?</h3><br />
                        <div style="text-align:justify;">
							That all depends on your preference. From silicone to design faceplates, OEM to 3rd-party cases, our excellent purchasing team has worked hard to make sure there are more than enough options to satisfy any and all your needs. 
                            Whether you're looking for rugged big names like Incipio, Case Mate, Proof and Ballistic; or searching for simple yet eye-popping self-expression through our designer leather iPhone cases, we have it all. 
                            Whatever you decide on, we're confident you'll enjoy great protection all day, and to prove it, we back all our cases with an unprecedented 1-Year Warranty.
                        </div>
						<br /><br />
						<h3 style="color:#000; font-weight:bold;">What's the difference between Verizon iPhone 4 Cases and normal iPhone 4 Cases?</h3><br />
                        <div style="text-align:justify;">
							When Apple decided to expand carriers to Verizon, small changes were made to the device. In the cosmetic sense, the volume button locations were lowered due to either changes in the internal hardware, 
                            or as a way of differentiating the CDMA from the GSM versions. Our Verizon iPhone 4 Cases are custom-made to accommodate for these differences. In terms of selection, 
                            you'll still find the best iPhone 4 cases around listed in both model pages, and our team has made sure the number of choices you have are equal.							
                        </div>
                    </td>
                    <td align="left" width="50%" valign="top" style="padding:5px;">
						<h3 style="color:#000; font-weight:bold;">I like your iPhone 4s Cases the best, will they work with my iPhone 4?</h3><br />
                        <div style="text-align:justify;">
							When Apple introduced the iPhone 4S, the Cupertino company had made it clear that the 4S was a world phone utilizing the same body-type as the original AT&amp;T iPhone 4 design. 
                            If you prefer a 4S design and own an AT&amp;T iPhone 4, most of our selections in the 4S category will be completely compatible with your device and vice-versa. 
                            Whatever you choose, we're sure you'll enjoy the lightweight, rumble-ready protection all our cases provide.
                        </div>
                        <br /><br />
						<h3 style="color:#000; font-weight:bold;">Any Cool iPhone Cases for 4, 4S and 3GS models?</h3><br />
                        <div style="text-align:justify;">
							Absolutely! Our purchasing team has scoured the globe to make sure you only get the latest and greatest accessories on the market. From quality stitched leather cases for iPhone, 
                            to cases with an integrated expansion battery, you have plenty of choices. Take a look at our Proof cases for vibrant colors and durable protection against scratches that can damage your screen. 
							If you're looking for a head-turning design, our designer faceplates are a great deal and even better choice.

                        </div>
                    </td>
				</tr>
			</table>
		</td>
	</tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<!--#include virtual="/includes/template/bottom_brand.asp"-->
