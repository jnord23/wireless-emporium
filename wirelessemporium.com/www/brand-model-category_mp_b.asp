<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
response.buffer = true
dim fsThumb : set fsThumb = CreateObject("Scripting.FileSystemObject")
dim basePageName : basePageName = "brand-model-category-details.asp"
dim pageTitle : pageTitle = "brand-model-category-details.asp"
dim curPageNum : curPageNum = prepInt(request.QueryString("page"))
dim showAll : showAll = prepStr(request.QueryString("show"))
dim picLap : picLap = 0
dim curPage : curPage = request.QueryString("curPage")
dim isMasterPage : isMasterPage = 1
dim pageName : pageName = "bmc-b"
dim strSort : strSort = prepStr(request("sort"))
dim productListingPage : productListingPage = 1
dim googleAds : googleAds = 1
dim useCustomCase : useCustomCase = false
dim isTablet : isTablet = false
dim brandID : brandID = prepInt(request.querystring("brandID"))
dim modelID : modelID = prepInt(request.querystring("modelID"))
dim categoryID : categoryID = prepInt(request.querystring("categoryID"))
dim featuredArtistID : featuredArtistID = prepInt(request.querystring("fArtistID"))
dim musicSkins : musicSkins = prepInt(request.querystring("musicSkins"))
dim musicSkinGenre : musicSkinGenre = prepStr(request.querystring("musicSkinGenre"))
dim musicSkinArtistID : musicSkinArtistID = prepInt(request.querystring("musicSkinArtist"))
dim usePages : usePages = 1
dim leftGoogleAd : leftGoogleAd = 1
dim strSubTypeID : strSubTypeID = ""
dim sqlCustomCase : sqlCustomCase = ""
dim sqlFeaturedArtist : sqlFeaturedArtist = ""
dim noProducts : noProducts = 0
dim productCount : productCount = 0
dim musicSkinsLink : musicSkinsLink = ""
dim musicSkinsLinkTail : musicSkinsLinkTail = ""


'if categoryID < 999 then
'	if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
'		response.Write("illegal category id:" & replace(curPage,"scd-","sc-"))
'		response.End()
'	else
'		'need to go to BMC page and get a proper subcategoryID
'		if instr(curPage,"scd-") > 0 then
'			call PermanentRedirect(replace(curPage,"scd-","sc-"))
'		else
'			call PermanentRedirect("/")
'		end if
'	end if
'end if

if curPageNum = 0 then curPageNum = 1
if showAll <> "" then
	productsPerPage = 2000
else
	productsPerPage = 40
end if
if instr(musicSkins,",") > 0 then
	musicSkinsArray = split(musicSkins,",")
	musicSkins = musicSkinsArray(0)
end if
if instr(musicSkinGenre,",") > 0 then
	musicSkinGenreArray = split(musicSkinGenre,",")
	musicSkinGenre = musicSkinGenreArray(0)
end if
if musicSkinGenre = "r-b" then musicSkinGenre = "R&B"
if musicSkinGenre = "tv-movies" then musicSkinGenre = "TV/Movies"
if musicSkinGenre = "screen-protectors" then musicSkinGenre = "Screen Protectors"
if len(musicSkinGenre) > 0 then
	musicSkinGenre = ucase(left(musicSkinGenre,1)) & right(musicSkinGenre,len(musicSkinGenre)-1)
end if
if instr(REWRITE_URL, "apple-iphone") > 0 then
	phoneOnly = 1
else
	phoneOnly = 0
end if

sql = "exec sp_categoryData 0," & categoryID
session("errorSQL") = sql
set catRS = oConn.execute(sql)
if not catRS.eof then
	parentCatName = catRS("parentTypeName")
	parentCatNameSEO = catRS("parentTypeNameSEO")	
	categoryName = catRS("typename")
	categoryNameSEO = catRS("typeNameSEO")
	parentTypeID = prepInt(catRS("typeid"))
	subCatTopText = catRS("subCatTopText")
	strSubTypeID = catRS("subtypeid")
	'strSubTypeID = left(strSubTypeID, len(strSubTypeID) - 1)
	
	bmcHeadline = catRS("bmcHeadline")
end if
if strSubTypeID = "" then strSubTypeID = "9999999" end if
catRS = null

if (brandID = 0 or modelID = 0) and (parentTypeID <> 8 and parentTypeID <> 15) then
	if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
		response.Write("Missing important variables")
		response.End()
	else
		call PermanentRedirect("/?ec=bmc9001")
	end if
end if

sql = "exec sp_modelDetailsByID " & modelID
session("errorSQL") = sql
set nameRS = oConn.execute(sql)
if not nameRS.eof then
	modelName = nameRS("modelname")
	modelImg = nameRS("modelimg")
	brandName = nameRS("brandName")
	isTablet = nameRS("isTablet")
	excludePouches = nameRS("excludePouches")
	includeNFL = nameRS("includeNFL")
	includeExtraItem = nameRS("includeExtraItem")
	HandsfreeTypes = ""
	if nameRS("HandsfreeTypes") <> "" then
		HandsfreeTypes = left(nameRS("HandsfreeTypes"),len(nameRS("HandsfreeTypes"))-1)	
	end if
end if
nameRS = null

if modelid = 0 then
	sql = "exec sp_brandDetailsByBrandID " & brandid
	set nameRS = oConn.execute(sql)
	if not nameRS.eof then
		brandName = nameRS("brandName")
	end if
end if
nameRS = null

sql = "exec sp_productColorOptions"
session("errorSQL") = sql
arrColors = getDbRows(sql)

if parentTypeID = 15 then
	parentTypeID = 16
end if

if categoryID = 15 then
	categoryID = 16
end if

if strSort = "" then strSort = "pop"

if parentTypeID = 5 or parentTypeID = 8 then
	sql = "exec sp_bmcdProducts " & prepInt(modelID) & ", " & prepInt(categoryID) & ", '" & strSort & "', 0, 0, 0, " & prepInt(parentTypeID)
elseif musicSkins = 1 then
	Set Jpeg = Server.CreateObject("Persits.Jpeg")
	Jpeg.Quality = 100
	Jpeg.Interpolation = 1
	musicSkinArtistID = prepInt(musicSkinArtistID)
	sql = "exec sp_bmcdMusicSkins " & prepInt(modelID) & ",'" & musicSkinGenre & "'," & prepInt(musicSkinArtistID)
else
	sql = "exec sp_bmcdProducts " & prepInt(modelID) & ", " & prepInt(categoryID) & ", '" & strSort & "', 0, " & prepInt(featuredArtistID)	& ", " & prepInt(brandID) & ", " & prepInt(parentTypeID)
end if
'response.write categoryID & "<br />"
'response.write sql & "<br />"
session("errorSQL") = sql
Set RS = oConn.execute(sql)

if parentTypeID = 8 then
	brandName = ""
	modelName = ""
	modelImg = "mini-phones.jpg"
	excludePouches = false
	includeNFL = false
	includeExtraItem = null
elseif parentTypeID = 15 then
	modelName = ""
	modelImg = "mini-phones.jpg"
	excludePouches = false
	includeNFL = false
	includeExtraItem = null
elseif musicSkins = 1 then
	categoryName = "Music Skins"
end if

if RS.eof then
	noProducts = 1
	if modelid > 0 then
		sql = "exec sp_brandModelNamesByModelID " & modelID
		session("errorSQL") = sql
		Set RS = oConn.execute(sql)
		
		if RS.eof then
			if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
				response.Write("staging alert: Model cannot be found")
				response.End()
			else
				PermanentRedirect("/?ec=bmc9002")
			end if
		end if
		
		brandName = RS("brandName")
		modelName = RS("modelName")
		modelImg = RS("modelImg")
		isTablet = RS("isTablet")
	end if
	RS = null
else
	if musicSkins = 1 then musicSkinArtistName = RS("artist") end if
	if featuredArtistID > 0 then 
		fArtistID = rs("artistID")
		fArtistName = rs("artistName")
		fArtistBio = rs("bio")
		fArtistPic = rs("artistPic")
		fArtistBanner = rs("banner1")
		fStudioName = rs("studioName")
	end if
end if

dim strBreadcrumb : strBreadcrumb = brandName & " " & modelName & " " & nameSEO(categoryName)

'todo: need to consolidate Metatags with we_XXXText
dim strH1: strH1 = ""
dim strH2: strH2 = ""
dim strAltText: strAltText = ""

dim strTypeToken : strTypeToken = "Cell Phone"
if isTablet then strTypeToken = "Tablet"

modelLink = "/" & formatSEO(brandName & " " & modelName) & "-accessories"

if isTablet then
	modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " " & strTypeToken & " Accessories</a>"	
else
	if brandid = 17 and phoneOnly = 1 then
		brandCrumb 	= "<a class=""breadcrumb"" href=""/apple-iphone-accessories"">Apple iPhone Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " Accessories</a>"	
	elseif brandid 	= 17 and phoneOnly = 0 then	
		brandCrumb 	= "<a class=""breadcrumb"" href=""/apple-ipod-ipad-accessories"">Apple iPod/iPad Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " Accessories</a>"
	else
		brandCrumb 	= "<a class=""breadcrumb"" href=""/" & formatSEO(brandName) & "-phone-accessories"">" & brandName & " " & strTypeToken & " Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " " & strTypeToken & " Accessories</a>"	
	end if
end if

if parentTypeID = 8 then
	select case categoryid
		case 1350, 1370		'stylers pen, memory cards
			strH1 = nameSEO(categoryName)
			strBreadcrumb = nameSEO(categoryName)
		case else
			strH1 = nameSEO(categoryNameSEO)
			strBreadcrumb = nameSEO(categoryNameSEO)
	end select
elseif categoryid = 1010 then
	strH1 = "Tablet PCs & eReaders"
	strBreadcrumb = "Tablet PCs & eReaders"	
elseif h1 <> "" then
	strH1 = h1
elseif strH1 = "" then
	strH1 = brandName & "&nbsp;" & modelName & "&nbsp;" & nameSEO(categoryName)
end if


if categoryID <> 1 and categoryID <> 2 and categoryID <> 3 and categoryID <> 5 and categoryID <> 7 and categoryID <> 15 and categoryID <> 18 then
	bannerID = 8
else
	bannerID = categoryID
end if

seH1 = brandName & " " & modelName & " " & categoryName 

if strSort = "pop" then usePopImg = "button-most-pop2.jpg" else usePopImg = "button-most-pop1.jpg"
if strSort = "new" then useNewImg = "button-new2.jpg" else useNewImg = "button-new1.jpg"
if strSort = "low" then useLowImg = "button-lowest2.jpg" else useLowImg = "button-lowest1.jpg"
if strSort = "high" then useHighImg = "button-highest2.jpg" else useHighImg = "button-highest1.jpg"

session("otherAdminOptions") = ", <a style='color:#ffffff;' href='/admin/db_update_models.asp?ModelID=" & modelID & "&BrandID=" & brandID & "&searchType=Model&submitType=Edit&submitModel=Search'>Edit " & modelName & "</a>"
%>
<!--#include virtual="/includes/template/top.asp"-->
<link href="/includes/css/mvt/bmcd/mvt_leftHeaderImg2.css" rel="stylesheet" type="text/css"> <!-- 2 -->
<link href="/includes/css/mvt/bmcd/mvt_rightHeaderImg2.css" rel="stylesheet" type="text/css"> <!-- 2 -->
<link href="/includes/css/bmcd.css" rel="stylesheet" type="text/css">
<link href="/includes/css/brand-model-category-b.css" rel="stylesheet" type="text/css" />
<script>
window.WEDATA.pageType = 'brandModelCategory';
window.WEDATA.pageData = {
	brand: <%= jsStr(brandName) %>,
	brandId: <%= jsStr(brandId) %>,
	model: <%= jsStr(modelName) %>,
	modelId: <%= jsStr(modelId) %>,
	category: <%=jsStr(categoryName)%>,
	categoryId: <%= jsStr(categoryId) %>,
	type: <%=jsStr(strTypeToken)%>

};
</script>

<% extraWidgets = "no" %>
<%
	if prepStr(SeTopText) <> "" then subCatTopText = SeTopText
%>
<div class="breadcrumbFinal">
	<%
	if isTablet then
	%>
	<a class="breadcrumb" href="/tablet-ereader-accessories">Tablet Accessories</a>&nbsp;&gt;&nbsp;<%=modelCrumb%>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
	<%
	else
		if parentTypeID = 15 or parentTypeID = 8 then
		%>
	<a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;
	<a class="breadcrumb" href="/<%=formatSEO(parentCatNameSEO)%>"><%=parentCatName%></a>&nbsp;&gt;&nbsp;
	<%=strBreadcrumb%>
		<%
		else
		%>
	<a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;<%=brandCrumb%>&nbsp;&gt;&nbsp;<%=modelCrumb%>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
		<%
		end if
	end if
	%>
</div>
<div><img src="/images/brandmodelheaders/header_<%=brandID%>.jpg" width="745" height="30" border="0" alt="<%=brandName & " " & modelName%> Accessories"></div>
<div class="main-banner" style="background:url(/images/bmcdb/banners/<%=bannerID%>.jpg) no-repeat;">
	<%if parentTypeID = 8 then%>
	<div class="title"><%=categoryName%></div>
	<div class="headline"><%=bmcHeadline%></div>
    <%else%>
	<div class="title"><%=brandName & " " & modelName%></div>
	<div class="subtitle"><%=categoryName%></div>
	<div class="headline"><%=bmcHeadline%></div>    
    <%end if%>
</div>
<div class="b-sort">
	<form name="frmSort" method="post">
    	<label for="sort">Sort By:</label>
        &nbsp;&nbsp;
        <!--<select name="sort" id="sort" onChange="return fnSort(this.value);">-->
		<select name="sort" onchange="return doSort()">
            <option value="pop" <% if strSort = "pop" then response.write "selected" %>>Most Popular</option>
            <option value="new" <% if strSort = "new" then response.write "selected" %>>Newest</option>
            <option value="low" <% if strSort = "low" then response.write "selected" %>>Lowest Price</option>
            <option value="high" <% if strSort = "high" then response.write "selected" %>>Highest Price</option>
        </select>
        <input type="hidden" id="subCatSelected" name="hidSubCatSelected" value="" />
    </form>
</div>
<div class="b-pagination" id="upperPagination"></div>

<% if musicSkins = 1 then %>
<div class="fl musicSkinsSelectContainer">
	<%
	musicSkinsLink = "ms-"
	musicSkinsLinkTail = "-music-skins-for-" & formatSEO(brandName) & "-" & formatSEO(modelName)
	genreArray = split(session("curGenres"),",")
	
	sql = "exec sp_musicSkinsGenresByModelID " & modelID
	session("errorSQL") = sql
	Set objRsGenre = oConn.execute(sql)
	
	genreList = ""
	do while not objRsGenre.EOF
		genreList = genreList & objRsGenre("genre") & ","
		objRsGenre.movenext
	loop
	genreArray = split(genreList,",")
	genreList = ""
	for i = 0 to ubound(genreArray)
		if prepStr(genreArray(i)) <> "" then
			if instr(genreList,prepStr(genreArray(i))) < 1 then genreList = genreList & prepStr(genreArray(i)) & ","
		end if
	next
	
	dim arrMusicArtist, objRsArtist, sqlArtist
	sqlArtist = "exec sp_musicSkinsArtistByGenre '" & musicSkinGenre & "'"
	session("errorSQL") = sqlArtist
	Set objRsArtist = oConn.execute(sqlArtist)
    %>
    <form name="frmMusicSkin">
    <div class="fl musicSkinsSelectTitle">Select Music Skins Genre:</div>
    <div class="fl musicSkinsSelectOption">
        <select name="musicGenre" class="w130" onchange="bmc('g',this.value)">
            <option value="">Select Genre</option>
            <%
            genreArray = split(genreList,",")
            
            for i = 0 to ubound(genreArray)
                if prepStr(genreArray(i)) <> "" then
            %>
            <option value="<%=formatSEO(genreArray(i))%>"><%=genreArray(i)%></option>
            <%
                end if
            next
            %>
        </select>
    </div>
    <%
    if not objRsArtist.eof then
    %>
    <div class="fl musicSkinsSelectTitle">Select Music Skins Artist:</div>
    <div class="fl">
        <select name="musicArtist" class="w130" onchange="bmc('a',this.value)">
            <option value="">Select Artist</option>
            <% 
            do until objRsArtist.eof 
            %>
            <option value="<%=objRsArtist("id") & "-" & formatSEO(objRsArtist("artist"))%>" <%if cint(objRsArtist("id")) = cint(musicSkinArtistID) then %>selected<% end if%>><%=objRsArtist("artist")%></option>
            <% 
                objRsArtist.movenext
            loop 
            %>
            <option value="0-all" <%if 0 = cint(musicSkinArtistID) then %>selected<% end if%>>Show All</option>
        </select>
    </div>
    <%
    end if
    set objRsArtist = nothing
    %>
    </form>
</div>
<% end if %>
<div id="allProducts" class="fl">
	<div id="noProducts" class="alertBoxHidden">
        No products match your current filter settings<br />
        Please try to adjust you filter to see more products
    </div>
	<%
	dim DoNotDisplay, RSkit
	noImageCount = 0
	curID = 0
	visCnt = 0
	useClass = "bmc_productBox"
	useImgTag = "img"
	a = 0
	if noProducts = 0 then
		dim arrSubTypeKey : arrSubTypeKey = 0
		dim arrSubTypeLength : arrSubTypeLength = 6
		
		do until RS.eof '--mainstream bmc listings
			DoNotDisplay = 0
			curAlwaysInStock = rs("alwaysInStock")
			curItemID = rs("itemID")
			response.Write("<!-- processItemID: " & curItemID & " -->")
			partnumber = rs("partnumber")
			if isnull(curAlwaysInStock) then curAlwaysInStock = false
			if left(partnumber,3) = "WCD" then DoNotDisplay = 1
			if not isNull(RS("ItemKit_NEW")) then
				sql = "exec sp_kitItemDetails '"& RS("ItemKit_NEW") & "'"
				session("errorSQL") = SQL
				set RSkit = oConn.execute(SQL)
				do until RSkit.eof
					if RSkit("inv_qty") < 1 then DoNotDisplay = 1
					RSkit.movenext
				loop
				RSkit.close
				set RSkit = nothing
			end if

			itemPic = RS("itemPic")
			if musicSkins = 1 then
				itemImgPath = Server.MapPath("/productpics/musicSkins/musicSkinsLarge") & "\" & RS("itemPic")
				itemImgPath2 = Server.MapPath("/productpics/musicSkins/musicSkinsSmall") & "\" & RS("itemPic")
				useImgPath = "/productpics/musicSkins/musicSkinsSmall/" & RS("itemPic")
			else
				itemImgPath = Server.MapPath("/productpics/thumb") & "\" & RS("itemPic")
				itemImgPath2 = Server.MapPath("/productpics/thumb") & "\" & RS("itemPic")
				useImgPath = "/productpics/thumb/" & RS("itemPic")
			end if
			
			if not fsThumb.FileExists(itemImgPath) then
				noImageCount = noImageCount + 1
				if musicSkins = 1 then
					if not isnull(rs("defaultImg")) then
						skipRest = 0
						if fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & rs("defaultImg"))) then
							setDefault = rs("defaultImg")
						elseif fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & replace(rs("defaultImg")," ","-"))) then
							setDefault = replace(rs("defaultImg")," ","-")
						else
							setDefault = rs("defaultImg")
						end if
						
						chkPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & setDefault)
						if not fsThumb.FileExists(chkPath) then
							musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)
							if fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)) then
								musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)
							elseif fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & replace(setDefault," ","-"))) then
								musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & replace(setDefault," ","-"))
							else
								DoNotDisplay = 1
								skipRest = 1
							end if
							if skipRest = 0 then
								session("errorSQL") = musicSkinsDefaultPath
								Jpeg.Open musicSkinsDefaultPath
								Jpeg.Width = 100
								Jpeg.Height = Jpeg.OriginalHeight * 100 / Jpeg.OriginalWidth
								Jpeg.Save chkPath
							end if
						end if
						if skipRest = 0 then
							itemPic = setDefault
							useImgPath = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & setDefault
						end if
					end if
				else
					DoNotDisplay = 1
				end if
			elseif musicSkins = 1 and not fsThumb.FileExists(itemImgPath2) then
				Jpeg.Open itemImgPath
				if Jpeg.OriginalHeight > Jpeg.OriginalWidth then
					Jpeg.Height = 100
					Jpeg.Width = Jpeg.OriginalWidth * 100 / Jpeg.OriginalHeight
				else
					Jpeg.Width = 100
					Jpeg.Height = Jpeg.OriginalHeight * 100 / Jpeg.OriginalWidth
				end if
				Jpeg.Save itemImgPath2
			end if
			
			if DoNotDisplay = 0 then
				a = a + 1
				picLap = picLap + 1
				altText = brandName & " " & modelName & " " & singularSEO(categoryName) & " - " & RS("itemDesc")
				if categoryid = "14" and modelid = "0" then altText = "Cell Phone Charms & Bling : " & altText
				
				if len(RS("itemDesc")) >= 73 then
					itemDescEtc = left(RS("itemDesc"),70) & "..."
				else
					itemDescEtc = RS("itemDesc")
				end if
				itemDescEtc = replace(itemDescEtc, "/", " / ")
				productLink = "/p-" & musicSkinsLink & RS("itemid") &"-"& formatSEO(RS("itemDesc")) & musicSkinsLinkTail
				colorid = rs("colorid")				
				onSale = RS("ActiveItemValueType") <> "OriginalPrice"
				customize = rs("customize")
				allNames = allNames & formatSEO(itemDescEtc) & ","
				allPrices = allPrices + prepInt(RS("price_Our"))
				availColors = rs("availColors")
				if instr(availColors,",") > 0 then 
					numAvailColors = ubound(split(availColors, ",")) + 1
				else 
					numAvailColors = 1
				end if
			%>
            <div class="fl <%=useClass%>" id="itemListID_<%=picLap%>">
				<div class="bmc_productPic" id="picDiv_<%=picLap%>" title="<%=altText%>" onclick="window.location='<%=productLink%>'">
					<div id="id_productImage_<%=curItemID%>" style="position:relative;">
						<% if onSale then %><div onclick="window.location='<%=productLink%>'" class="product-bmc-onSale clickable"></div><% end if %>
						<<%=useImgTag%> src="<%=useImgPath%>" border="0" width="100" height="100" title="<%=altText%>" onclick="window.location='<%=productLink%>'" class="clickable">
						<%
						'if not isnull(session("adminID")) and len(session("adminID")) > 0 and session("adminNav") then
							'response.write "<br /><span style=""color:red; font-size:10px;"">" & partnumber & "</span><br /><span style=""font-size:10px;"">" & curItemID & "</span>"
							'if RS("alwaysInStock") then response.Write("<span style=""color:blue; font-size:10px;"">(Always In Stock)</span>")
						'end if
						%>
						<% if customize then %>
						<div style="position:absolute; right:0px; top:0px; cursor:pointer;" onclick="window.location='<%=productLink%>'"><img src="/images/icons/customize-sm.png" border="0" /></div>
						<% end if %>
					</div>
				</div>
				<% if RS("inv_qty") = 0 and not curAlwaysInStock then %>
				<div class="product-bmc-soldOut" align="center">
					<img src="/images/sold_out.png" width="85">
				</div>
				<% else %>
				<div class="product-bmc-details" align="center">
					<div id="id_productLink_<%=curItemID%>" class="product-bmc-desc">
						<a class="cellphone-link" href="<%=productLink%>" title="<%=altText%>" alt="<%=altText%>"><%=itemDescEtc%></a>
					</div>
					<% if numAvailColors > 1 then %>
					<div style="width:100%; padding-top:5px; height:32px; clear:both; overflow:hidden;">
						<div align="center" style="font-weight:bold;">Available Colors:</div>
						<div align="center"><%=generateColorPicker(arrColors, curItemID, availColors, colorid, numAvailColors, onSale)%></div>
						<div style="display:none;" id="preloading_deck_<%=curItemID%>"></div>                            
					</div>
					<% end if %>                                                    
				</div>
				<div style="float:left; width:100%; padding-top:5px;" align="center" onclick="window.location='<%=productLink%>'">
					<span class="boldText">Our&nbsp;Price:&nbsp;</span>
					<span class="pricing-orange2"><%=formatCurrency(RS("price_Our"))%></span><br>
					<span class="pricing-gray2">List&nbsp;Price:&nbsp;<s><%=formatCurrency(prepInt(RS("price_Retail")))%></s></span>
				</div>
				<div id="mvt_bmcd_fresshipping" style="float:left; width:100%; padding:5px 0px 5px 0px;" align="center" onclick="window.location='<%=productLink%>'">
					<img src="/images/bmc/we-freeshipping1.jpg" border="0" />
				</div>
				<input type="hidden" name="" value="<%=RS("subtypeID")%>" id="subtypeid_<%=picLap%>" />
				<% end if %>
			</div>
			<%
				if picLap = productsPerPage then
					useClass = "bmc_productBox"
					useImgTag = "tempImg"
				end if
			end if
			
			if musicSkins = 1 then
				itemDesc = rs("itemDesc")
				do while itemDesc = rs("itemDesc") or (isnull(itemDesc) and isnull(rs("itemDesc")))
					RS.movenext
					if rs.EOF then exit do
				loop
			else
				do while curItemID = rs("itemID")
					RS.movenext
					if rs.EOF then exit do
				loop
			end if
			
			if arrSubTypeKey < arrSubTypeLength then
				arrSubTypeKey = arrSubTypeKey + 1
			else 
				arrSubTypeKey = 0
			end if
		loop
	end if
	
	if totalPages = 0 then totalPages = 1
	if curPageNum = 0 then curPageNum = 1
	hiddenCount = picLap - 40
	hiddenPages = round(hiddenCount / productsPerPage)
	if (hiddenCount / productsPerPage) > round(hiddenCount / productsPerPage) then hiddenPages = hiddenPages + 1
	totalPages = curPageNum + hiddenPages
	if instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"?") > 0 then
		usePageURL = left(request.ServerVariables("HTTP_X_REWRITE_URL"),instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"?")-1)
	else
		usePageURL = request.ServerVariables("HTTP_X_REWRITE_URL")
	end if
	if picLap = 0 then	'no products available
		if categoryid = 1010 then
	%>
	<div>
		No tablets currently available. 
		<br /><a href="/tablet-ereader-accessories" style="text-decoration:underline;" title="Tablets EReaders Accessories">Click HERE</a> to view tablet and e-reader accessories.
	</div>
		<% else %>
	<div style="font-weight:bold; font-size:18px; color:#F00; padding:20px 0px 20px 0px;">
		No Products Currently Available
	</div>
		<%
		end if
	end if
	%>
	<div style="clear:both;"></div>
	<div style="width:748px;float:left;">
		<div class="b-pagination" id="lowerPagination" style="float:right;margin:0;"></div>
	</div>
	<!--
	<div id="lowerPagination" style="float:right;">
		<%
		if totalPages > 1 then
		%>
		<div style="float:right;"><a class="inactivePageNum" onclick="showPage(0)">Show All Products</a></div>
		<% end if %>
		<div style="float:right;">
			<div style="padding:2px 5px 0px 5px; float:left; font-size:11px;">View Page:</div>
			<%
			for x = 1 to totalPages
				if x = cdbl(curPageNum) then
					pageNumClass = "activePageNum"
				else
					pageNumClass = "inactivePageNum"
				end if
			%>
			<div id="pageLinkBox_<%=x%>" class="paginationLink1"><a id="pageLink_<%=x%>" class="<%=pageNumClass%>" onclick="showPage(<%=x%>)"><%=x%></a></div>
			<%
			next
			%>
		</div>
	</div>
	-->
	<%if featuredArtistID > 0 and categoryid = 1031 then%>
	<div style="clear:both; height:40px;"></div>
	<div style="float:left; width:100%;">
		<div style="float:right; width:740px; border:1px solid #ccc; background-color:#f7f7f7; position:relative;">
			<div style="position:absolute; float:left; top:-15px; left:-10px; background:url(/images/about-artist.png) no-repeat; width:167px; height:38px;"></div>
			<div style="float:left; width:180px; text-align:center; padding:20px 0px 10px 0px;">
				<img src="/productpics/featuredArtist/<%=fArtistPic%>" border="0" alt="<%=fArtistName%>" />
			</div>
			<div style="float:left; width:540px; text-align:left; padding:10px;">
				<div style="float:left; width:540px; text-align:left; padding:10px;">
					<div style="float:left; width:390px; text-align:left;">
						<div style="float:left; width:100%; text-align:left;">
							<div style="float:left; width:100%;">
								<div style="float:left; display:inline-block; text-align:left; color:#333; font-size:24px;"><%=fArtistName%></div>
								<div style="float:left; width:50px; text-align:center;"><img src="/images/paint-icon.png" border="0" /></div>
							</div>
							<div style="float:left; width:100%; text-align:left; color:#666; font-style:italic;"><%=fStudioName%></div>
						</div>
					</div>
					<div style="float:right; width:150px; text-align:center;">
						<a href="/sb-<%=brandid%>-sm-<%=modelid%>-scd-<%=categoryid%>-<%=formatSEO(categoryName)%>-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>" style="color:#ff6600; font-weight:bold;" title="<%=brandName%> <%=modelName%> <%=categoryName%>">
							VIEW ALL ARTISTS <img src="/images/link-arrows.png" border="0" />
						</a>
					</div>
					<div style="float:left; width:100%; padding-top:10px; text-align:left; color:#666;"><%=fArtistBio%></div>
				</div>
			</div>
		</div>
	</div>
	<%end if%>
</div>
<%=tooltip%>
<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
    <%
    sql = "exec sp_getCatsByModel " & modelID & ", 0"
	session("errorSQL") = sql
	'response.write "<pre>" & sql & "</pre>"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	a = 0

	if not RS.eof then
	%>
    <!-- other accessories start -->
    <tr>
    	<td style="padding-top:50px;">
            <div style="background-color:#ebebeb; padding:10px; border:1px solid #ccc;">
                <div style="font-size:15px; font-weight:bold; color:#494949;" title="OTHER <%=ucase(brandName) & " " & ucase(modelName)%> ACCESSORIES" align="center">
                	<h3 class="brandModel">OTHER <%=ucase(brandName) & " " & ucase(modelName)%> ACCESSORIES</h3>
				</div>
            </div>
        </td>
    </tr>
    <tr>
    	<td>
            <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                <tr>
                	<td>
						<%
                        dim handsFree : handsFree = 0
						do until RS.eof
							thisType = replace(RS("typeName"), "/", " / ")
							useTypeID = rs("typeid")
							if useTypeID = 5 then handsFree = 1
							if useTypeID = 16 then useTypeID = 15
							strLink = replace(replace(replace(replace(replace(replace(rs("urlStructure"), "{brandid}", brandid), "{modelid}", modelid), "{typeid}", useTypeID), "{brandname}", formatSEO(brandName)), "{modelname}", formatSEO(modelName)), "{typename}", formatSEO(thisType))
							strLink = replace(strLink,".asp","")
							altText = modelName & " " & thisType
						%>
						<div id="catMainBox">
							<div id="catImg"><a href="<%=strLink%>" title="<%=altText%>"><img src="/images/categories/we_cat_<%=useTypeID%>.jpg" width="165" border="0" alt="<%=altText%>" title="<%=altText%>" /></a></div>
							<div id="catDetails"><a href="<%=strLink%>" title="<%=altText%>" class="cellphone3-link"><%=altText%></a></div>
						</div>
						<%
							RS.movenext
						loop
						
						if handsFree = 0 then
							thisType = "Handsfree Bluetooth"
							useTypeID = 5
							strLink = replace(replace(replace(replace(replace(replace("sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}", "{brandid}", brandid), "{modelid}", modelid), "{typeid}", useTypeID), "{brandname}", formatSEO(brandName)), "{modelname}", formatSEO(modelName)), "{typename}", formatSEO(thisType))
							altText = modelName & " " & thisType
						%>
						<div id="catMainBox">
							<div id="catImg"><a href="<%=strLink%>" title="<%=altText%>"><img src="/images/categories/we_cat_<%=useTypeID%>.jpg" width="165" border="0" alt="<%=altText%>" title="<%=altText%>" /></a></div>
							<div id="catDetails"><a href="<%=strLink%>" title="<%=altText%>" class="cellphone3-link"><%=altText%></a></div>
						</div>
						<%
						end if
                        %>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!-- other accessories end -->
    <tr>
    	<td style="padding:20px 0px 20px 0px;">
        	<%
			turnToItemID = "BMC" & brandid & modelid & categoryid
			%>
           	<!--#include virtual="/includes/asp/inc_turnto.asp"-->
        </td>
	</tr>
	<%
	end if
	%>
	<tr><td style="padding-top:20px;"><h1 class="brandModelCat2"><%=seH1%></h1></td></tr>    
    <tr><td align="center" class="bottomText"><%=seBottomText%></td></tr>    
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<script language="javascript">var isMasterPage = 1</script>
<!--#include virtual="/includes/template/bottom.asp"-->
<%
	useURL = request.ServerVariables("HTTP_X_REWRITE_URL")
	if instr(useURL,"?") > 0 then useUrl = left(useURL,instr(useURL,"?")-1)
%>
<script language="javascript">
	window.onload = function() {
		if(typeof dynamicLoad == 'function'){
			dynamicLoad();
		}
	}
	var useFilter = ""
	var useFilter2 = ""
	var curPage = 1
	var totalPages = <%=prepInt(totalPages)%>
	var totalItems = <%=prepInt(picLap)%>
	<% if parentTypeID = 16 then %>
	var selectedSubTypes = ['1020'];
	subTypeFilter(1);
	var reset_selection = 0;
	<% else %>
	var selectedSubTypes = [];
	var reset_selection = 1;
	<% end if %>
	<% if picLap > 40 then %>
	var numToShow = 40
	<% else %>
	var numToShow = <%=prepInt(picLap)%>
	<% end if %>
	var perPage = 40
	if (totalPages == 0) { totalPages = 1 }
</script>
<script language="javascript" src="/includes/js/filterProducts_bmcdb.js"></script>
<% if musicSkins = 1 then %>
<script language="javascript">
	function bmc(type, val) {
		var useVal = val.replace(" ","-");
		
		if ("" != useVal)
		{
			if ("a" == type) window.location = '/sb-<%=brandID%>-sm-<%=modelID%>-scd-<%=categoryID%>-music-skins-<%=formatSEO(musicSkinGenre)%>-artist-' + useVal;
			else if("g" == type) window.location = '/sb-<%=brandID%>-sm-<%=modelID%>-scd-<%=categoryID%>-music-skins-' + useVal;		
		}
	}
</script>
<% end if %>
<script>
	function preloading(pItemID, selectedColorID)
	{
		ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmcd&uType=pre&itemid=' + pItemID + '&selectedColorID=' + selectedColorID, 'preloading_deck_' + pItemID);		
	}
	
	function changeProduct(pItemID, selectedColorID, numColors, selectedIDX, onSale)
	{
		curObj = null;
		for(i=0; i < numColors; i++) 
		{
			curObj = 'div_colorOuterBox_' + pItemID + '_' + i;
			if (document.getElementById(curObj) != null) document.getElementById(curObj).className = 'colorBoxUnselected';
		}
		curObj = 'div_colorOuterBox_' + pItemID + '_' + selectedIDX;
		document.getElementById(curObj).className = 'colorBoxSelected';

		ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmcd&uType=img&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&onsale=' + onSale + '&adminOption=<%=prepInt(session("adminID"))%>', 'id_productImage_'+pItemID);
		ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmcd&uType=link&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&adminOption=<%=prepInt(session("adminID"))%>', 'id_productLink_'+pItemID);
		ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmcd&uType=price&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&adminOption=<%=prepInt(session("adminID"))%>', 'id_productPricing_'+pItemID);
	}
	
	function fnSort(sortOption)
	{
		var useURL = window.location + ''
		if (useURL.indexOf("?") > 0) {
			useURL = useURL.substring(0,useURL.indexOf("?"))
		}
		window.location = useURL + '?strSort=' + sortOption
		return false;
	}
	
	function doSort()
	{
		if (document.getElementsByName("subtypes[]").length > 0) {
			for(i=0; i<document.getElementsByName("subtypes[]").length;i++) {
				if(document.getElementsByName("subtypes[]")[i].checked) {
					document.getElementById('subCatSelected').value = document.getElementById('subCatSelected').value + ',' + document.getElementsByName("subtypes[]")[i].value;
				}			
			}
		}
		document.getElementById('subCatSelected').value = document.getElementById('subCatSelected').value + ',';
//		alert(document.getElementById('subCatSelected').value);
		document.frmSort.submit();
		return false;
	}
	
	$(document).ready(function(){
		$(".tooltip").mouseover(function(){
			var id = $(this).attr("id");
			var top = $(this).offset().top - 40;
			var left = $(this).offset().left + 23;
			$("#t"+id).attr("style", "position:absolute;top:"+top+"px;left:"+left+"px;z-index:1000;display:block;");
		});
		
		$(".tooltip").mouseout(function(){
			var id = $(this).attr("id");
			$("#t"+id).attr("style", "display:none;");
		});
	});
	
	//setTimeout("ajax('/ajax/updateProducts.asp?modelID=<%=modelID%>','dumpZone')",2000)
</script>