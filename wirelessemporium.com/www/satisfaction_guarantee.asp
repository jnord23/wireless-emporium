<HTML>
<HEAD>
<TITLE>Buy Cell Phone Accessories from WirelessEmporium � Cell phone Covers, Faceplates, Charms, Chargers, Batteries - Deep Discounts and Free Shipping</TITLE>
<META NAME="Keywords" CONTENT="Panasonic cell phone accessory, Panasonic mobile  phone accessories, Panasonic battery pack, Panasonic battery charger, Panasonic phone battery, Panasonic phone part, Panasonic headset, mobile phone accessories, mobile HANDSFREE, faceplates for cell phones">
<META NAME="Description" CONTENT="Wireless Emporium offers discount accessories for your Panasonic cell phones including cases, batteries, chargers, antenna, keypads, bluetooth headsets and more. Buy Online!">
<META NAME="Robots" CONTENT="index,follow">
<META NAME="GOOGLEBOT" CONTENT="INDEX, FOLLOW">
<!-- This site contains information about: cell phone accessories,cheapest cell phone accessories,cell phone faceplate,cell phone covers,cell phone battery,cellular battery,cell phone charger,discount cell phone chargers,hands free kit,hands-free -->
<link rel="stylesheet" href="/includes/css/style.css" type="text/css">
</HEAD>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" link="#666666" vlink="#666666" alink="#FF6600">
		
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" summary="cell phone accessories">
	<tr>
		<td align="center" valign="middle">
			<table width="100%" height="100%" border="0" cellpadding="15" cellspacing="0" summary="Wireless Emporium Price Advantage">
				<tr>
					<td valign="top" bgcolor="#FFFFFF" class="normalText">
						<p align="justify">
							<strong>OUR EZ NO-HASSLE &amp; RETURN POLICY:</strong>
							<br>
							Wireless Emporium is happy to offer our exclusive <strong>EZ NO-HASSLE 
							RETURN POLICY </strong>- If you are not happy with the product 
							you may return the item for 
							REPLACEMENT OR REFUND <strong>within ninety (90) days of receipt</strong>.
							Contact us at:
							<a href="mailto:returns@wirelessemporium.com" title="mailto:returns@wirelessemporium.com">returns@wirelessemporium.com</a>
						</p>
						<p>
							All returns must be accompanied by a Wireless Emporium RMA number. To obtain an RMA number, please
							send an e-mail to:
							<a href="mailto:returns@wirelessemporium.com" title="mailto:returns@wirelessemporium.com">returns@wirelessemporium.com</a>
						</p>
						<p>
							Be sure to include the following information:
							<ul style="margin-top:0px;">
								<li>Order Number</li>
								<li>Date of original purchase</li>
								<li>Reason for request for RMA</li>
								<li>Your e-mail address</li>
							</ul>
						</p>
						<p>
							<strong>No returns, refunds or exchanges will be processed without an RMA number.</strong>
						</p>
					</td>
				</tr>
				<tr>
					<td align="right"><a href="javascript:window.close()" class="normalText">close</a></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</body>
</html>
