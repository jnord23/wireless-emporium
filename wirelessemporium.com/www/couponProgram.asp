<%
	response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
	fOpenConn()
	dim couponCode : couponCode = prepStr(request.Form("couponCode"))
	if couponCode = "" then
		couponCode = prepStr(request.QueryString("couponCode"))
	end if
	dim emailAddrs : emailAddrs = prepStr(request.Form("emailAddrs"))
	dim message : message = prepStr(request.Form("message"))
	dim myEmail : myEmail =  prepStr(request.Form("myEmail"))
	dim personalMsg : personalMsg =	"Hello friends and family! I got a great deal for cell phone accessories @ WirelessEmporium.com " &_
									"and wanted to share a 10% off coupon. Input code " & couponCode & " during checkout and get 10% " &_
					                "off plus free shipping on every order.  Remember use coupon code " & couponCode & " and happy " &_
					                "shopping!"
	
	if emailAddrs <> "" then
		sql = "select fName + ' ' + lname as myName from we_accounts where personalCode = '" & couponCode & "'"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\admin\scheduled_tasks\shareAndEarn.html")
		strTemplate = replace(strTemplate,"##Personal Message##",message)
		strTemplate = replace(strTemplate,"##CODE##",couponCode)
		strTemplate = replace(strTemplate,"##MYNAME##",rs("myName"))
		emailList = split(emailAddrs,",")
		for i = 0 to ubound(emailList)
			cdo_from = myEmail
			cdo_subject = "Wireless Emporium 10% OFF Coupon"
			cdo_body = strTemplate
			cdo_to = trim(emailList(i))
			CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		next
		alertMsg = "Emails Sent"
	end if
	
	sql = "select sum(cast(ordersubtotal as money)) as spent, (select email from we_accounts where personalCode = '" & couponCode & "') as myEmail from we_coupons a left join we_orders b on a.couponid = b.couponid and b.approved = 1 and b.cancelled is null where a.promoCode = '" & couponCode & "'"
	session("errorSQL") = sql
	set	rs = oConn.execute(sql)
	
	if rs.EOF or couponCode = "" then
		session("userMsg") = "Invalid code entered<br />Please re-check your coupon code and try again"
		response.Redirect("/couponProgramLogin.asp")
	else
		amtSpent = prepInt(rs("spent"))
		amtEarned = amtSpent * .05
		myEmail = prepStr(rs("myEmail"))
	end if
%>
<form action="/couponProgram.asp" method="post" name="sendEmailForm">
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td style="font-size:12px; color:#999; border-bottom:2px solid #CCC;">
        	<div style="float:left; padding:30px 0px 0px 60px; width:500px;">
	        	<h1 style="color:#666; font-size:36px; padding:0px; margin:0px;">Affliliate point totals</h1>
    	        Once you have over $5 in cash-back rewards, chose to have a check mailed out to you or use 
                our multiplier for Wireless Emporium store credit
            </div>
            <div style="float:right; padding:10px 20px 0px 0px;"><img src="/images/icons/couponProgramHeader.jpg" border="0" /></div>
        </td>
    </tr>
    <tr>
    	<td valign="top">
        	<div style="height:70px; width:594px; background-color:#CCC; border:3px solid #666; margin-left:auto; margin-right:auto; margin-top:20px;">
            	<div style="float:left; color:#666; margin:10px 0px 0px 20px; width:90%;">
                	<div style="float:left; font-weight:bold; padding-top:4px; margin-right:10px;">Total Amount Spent:</div>
                    <div style="float:left; font-weight:bold; font-size:18px;"><%=formatCurrency(amtSpent,2)%></div>
                    <div style="float:right; font-weight:bold; font-size:18px; color:#090;"><%=formatCurrency(amtEarned,2)%></div>
                    <div style="float:right; font-weight:bold; padding-top:4px; margin-right:10px;">You are eligible to receive:</div>
                </div>
                <% if amtEarned > 5 then %>
                <div style="float:left; color:#666; margin:5px 0px 0px 20px; width:90%;">
                    <div style="float:left; font-weight:bold; color:#000; font-size:12px; margin-right:5px;">Choose redemption method:</div>
                    <div style="float:left; margin-right:5px; padding-top:3px;"><img src="/images/icons/couponProgramArrow.gif" border="0" /></div>
                    <div style="float:left; margin-right:15px;"><a href="#" style="color:#0099cd;">Send me a check!</a></div>
                    <div style="float:left; margin-right:5px; padding-top:3px;"><img src="/images/icons/couponProgramArrow.gif" border="0" /></div>
                    <div style="float:left;"><a href="#" style="color:#0099cd;">I'd like store credit (<%=formatCurrency(amtEarned*2,2)%> value)</a></div>
                </div>
                <% end if %>
            </div>
        </td>
    </tr>
    <tr>
        <td style="font-size:12px; color:#999; border-bottom:2px solid #CCC;">
        	<div style="float:left; padding:30px 0px 20px 60px; width:500px;">
	        	<h1 style="color:#666; font-size:36px; padding:0px; margin:0px;">Invite friends and earn</h1>
    	        Provide anyone and everyone with your coupon code for 10% off their entire order.<br />
                Every time your code is used, receive 5% cash back (regardless of who uses it).
            </div>
        </td>
    </tr>
    <tr>
    	<td align="center">
        	<div style="height:200px; width:594px; margin-left:auto; margin-right:auto; margin-top:20px;">
            	<div style="width:500px; text-align:left;"><img src="/images/icons/couponProgramEnterEmail.jpg" border="0" /></div>
                <div style="width:500px; border:2px solid #ccc;"><textarea rows="7" cols="59" name="emailAddrs"></textarea></div>
                <div style="font-size:11px;">Seperate email address with a comma (ex: test@abc.com,test@xyz.com)</div>
            </div>
        </td>
    </tr>
    <tr>
    	<td align="center">
        	<div style="height:200px; width:594px; margin-left:auto; margin-right:auto; margin-top:20px;">
            	<div style="width:500px; text-align:left;"><img src="/images/icons/couponProgramEnterMsg.jpg" border="0" /></div>
                <div style="width:500px; border:2px solid #ccc;">
                	<textarea rows="7" cols="59" name="message"><%=personalMsg%></textarea>
                </div>
            </div>
        </td>
    </tr>
    <tr>
    	<td align="center" style="border-bottom:2px solid #CCC;">
	       	<div>
            	<input type="image" src="/images/buttons/couponProgramInviteBttn.jpg" border="0" />
                <input type="hidden" name="couponCode" value="<%=couponCode%>" />
                <input type="hidden" name="myEmail" value="<%=myEmail%>" />
            </div>
            <div style="margin-top:10px; font-size:14px; color:#666; height:24px; width:160px; margin-left:auto; margin-right:auto;">
            	<div style="float:left; padding-bottom:8px; margin-right:5px;">Easily share on: </div>
                <div style="float:left;"><a href="#"><img src="/images/reviews/facebookIcon.jpg" border="0" /></a>&nbsp;<a href="#"><img src="/images/reviews/twitterIcon.jpg" border="0" /></a></div>
            </div>
        </td>
    </tr>
    <tr>
    	<td align="center">
        	<div style="width:500px; text-align:left; margin-top:10px;">
            	Wireless Emporium is dedicated to the privacy of everyone who visits us. Any emails sent out through the 
                WE Share &amp; Earn programs will be done as the extension of the person submitting message content and 
                email addresses. All emails submitted through the program will not be shared or saved outside of purpose 
                of tracking and program optimization. Wireless Emporium waives any liability caused by the content and 
                delivery of the message and regerral. User agrees to accept these terms upon submission.
            </div>
        </td>
    </tr>
</table>
</form>
<!--#include virtual="/includes/template/bottom.asp"-->
<%
	function readTextFile(fileName)
		Dim strContents : strContents = ""
	
		If "" <> fileName Then 
			Const ForReading = 1, ForWriting = 2, ForAppending = 3
			Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0
	
			Dim FSO, Filepath
			set FSO = createObject("Scripting.FileSystemObject")
			Filepath = fileName
	
			if FSO.FileExists(Filepath) Then
				Dim TextStream
				Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)
	
				' Read file in one hit
				strContents = TextStream.ReadAll
	
				TextStream.Close
				Set TextStream = nothing
			End If
	
			Set FSO = Nothing
		End If 
	
		readTextFile = strContents
	End Function
	
	if alertMsg = "Emails Sent" then
%>
<script language="javascript">alert("Emails have been sent\nCheck back often to see how much you have earned")</script>
<%
	end if
%>