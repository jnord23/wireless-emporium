<%
dim useHttps : useHttps = 1
'response.write session.SessionID
pageName = "checkout"
'mvtGaq = "checkout_orig_ID423"
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

refreshForLogin = request.QueryString("l")
if isnull(refreshForLogin) or len(refreshForLogin) < 1 then refreshForLogin = 0
noCommentBox = true

'Allow for logins and promo codes
if refreshForLogin = 1 then
	response.Cookies("checkoutPage") = ""
	response.Cookies("checkoutPage").expires = date - 1
end if
	
dim curSite
if bStaging then
	curSite = "http://staging"
else
	curSite = "https://www"
end if
strHttp = useHttp

dim cart
cart = 1

varientTopBanner = 2
varientTopNav = 1
varientAssurance = 0

set fs = CreateObject("Scripting.FileSystemObject")
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
'mySession = request.QueryString("passSessionID")
'mySrToken = request.QueryString("passSrToken")
mySrToken = request.Cookies("srLogin")


dim SEtitle, SEdescription, SEkeywords
SEtitle = ""
SEdescription = ""
SEkeywords = ""

dim pageTitle, strBody
pageTitle = ""
'strBody = "<body leftmargin=""0"" topmargin=""0"" onload=""changeTax(document.frmProcessOrder.sState.value);"">"
%>
<!--#include virtual="/includes/template/top_cart.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<%
dim HTTP_REFERER
HTTP_REFERER = request.servervariables("HTTP_REFERER")
'if left(HTTP_REFERER,47) <> "http://www.wirelessemporium.com/cart/basket.asp" and left(HTTP_REFERER,48) <> "https://www.wirelessemporium.com/cart/basket.asp" and left(HTTP_REFERER,50) <> "https://www.wirelessemporium.com/cart/checkout.asp" and left(HTTP_REFERER,51) <> "http://staging.wirelessemporium.com/cart/basket.asp" and left(HTTP_REFERER,53) <> "http://staging.wirelessemporium.com/cart/checkout.asp" and refreshForLogin = 0 then
'	response.write "<h3>This page will only accept forms submitted from the Wireless Emporium secure website.</h3><!-- " & request.ServerVariables("HTTP_REFERER") & " -->" & vbcrlf
'	response.end
'end if

dim nOrderId, a

dim szip, sWeight
szip = request.form("shipZip")

dim sUserId
if sUserId = "" then
	'check if the user is logging in
	if request.form("submitted") = "submitted" and request.form("loginEmail") <> "" then
		SQL = "SELECT pword,fname,lname,accountid FROM we_accounts"
		SQL = SQL & " WHERE email='" & SQLquote(request.form("loginEmail")) & "' AND pword <> '' AND pword IS NOT null"
		SQL = SQL & " ORDER BY accountid DESC"
		set RS = oConn.execute(SQL)
		if not RS.eof then
			if SQLquote(request.form("pword1")) = RS("pword") then
				myAccount = RS("accountid")
				on error resume next
				response.cookies("fname") = RS("fname")
				response.cookies("fname").expires = dateAdd("m", 1, now)
				response.cookies("myAccount") = RS("accountid")
				response.cookies("myAccount").expires = dateAdd("m", 1, now)
				on error goto 0
			end if
		end if
		RS.close
		set RS = nothing
	end if
end if

dim newEmail, email, pword1, pword2, phone, Fname, Lname, sAddress1, sAddress2, sCity, sState, bAddress1, bAddress2, bCity, bState, bZip
dim radioAddress, shiptype, PaymentType, cc_cardType, cc_cardOwner, cardNum, cc_month, cc_year, hearFrom

'check if the user is already logged in
if myAccount <> "" then
	SQL = "SELECT * FROM we_accounts WHERE accountid = '" & myAccount & "'"
	set RS = oConn.execute(SQL)
	if not RS.eof then
		email = RS("email")
		fname = RS("fname")
		lname = RS("lname")
		phone = RS("phone")
		saddress1 = RS("saddress1")
		saddress2 = RS("saddress2")
		scity = RS("scity")
		sstate = RS("sstate")
		szip = RS("szip")
		bAddress1 = RS("bAddress1")
		bAddress2 = RS("bAddress2")
		bCity = RS("bCity")
		bState = RS("bState")
		bZip = RS("bZip")
	end if
	RS.close
	set RS = nothing
	radioAddress = request.form("radioAddress")
	shiptype = request.form("shiptype")
	if request.form("szip") <> "" then szip = request.form("szip")
end if

if request.form("submitted") = "submitted" and myAccount = "" then
	newEmail = request.form("newEmail")
	pword1 = request.form("pword1")
	pword2 = request.form("pword2")
	fname = request.form("fname")
	lname = request.form("lname")
	email = request.form("email")
	phone = request.form("phone")
	saddress1 = request.form("saddress1")
	saddress2 = request.form("saddress2")
	scity = request.form("scity")
	sstate = request.form("sstate")
	szip = request.form("szip")
	bAddress1 = request.form("bAddress1")
	bAddress2 = request.form("bAddress2")
	bCity = request.form("bCity")
	bState = request.form("bState")
	bZip = request.form("bZip")
	radioAddress = request.form("radioAddress")
	shiptype = request.form("shiptype")
	PaymentType = request.form("PaymentType")
	cc_cardType = request.form("cc_cardType")
	cc_cardOwner = request.form("cc_cardOwner")
	cardNum = request.form("cardNum")
	cc_month = request.form("cc_month")
	cc_year = request.form("cc_year")
	hearFrom = request.form("hearFrom")
end if

' START basket grab
dim sItemName, sRetailPrice, sItemPrice, sItemPic, nProdIdCheck, nProdQuantity, nProdIdCount, strItemCheck, strQty, nItemTotal
dim WEhtml, WEhtml2, PPhtml, EBtemp, EBxml

nProdIdCount = 0
strItemCheck = ""
strQty = ""
discountAmt = 0

SQL = "UPDATE ShoppingCart SET DateCheckout = '" & now & "' WHERE store = 0 AND sessionID = '" & mySession & "' AND (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
oConn.execute SQL

SQL = "SELECT a.id shoppingcartID, c.brandName, d.modelName, a.musicSkins, a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, B.itemDesc, B.itemPic, '' defaultPic, B.price_Our, B.price_retail, B.Condition, B.itemWeight, B.NoDiscount FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID and a.musicSkins = 0 left join we_Brands c on b.brandID = c.brandID left join we_Models d on b.modelID = d.modelID WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
SQL = SQL & " union"
SQL = SQL & " SELECT a.id shoppingcartID, c.brandName, d.modelName, a.musicSkins, a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, 20 as typeID, B.musicSkinsID as PartNumber, isnull(c.brandName, '') + ' ' + isnull(d.modelName, '') + ' ' + isnull(B.artist, '') + ' ' + isnull(b.designName, '') as itemDesc, B.image as itemPic, B.defaultImg as defaultPic, b.price_we as price_Our, b.msrp as price_retail, 0 as Condition, 1 as itemWeight, 0 as NoDiscount FROM ShoppingCart A INNER JOIN we_items_musicSkins B ON A.itemID=B.id and a.musicSkins = 1 left join we_Brands c on b.brandID = c.brandID left join we_Models d on b.modelID = d.modelID WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
SQL = SQL & " order by partNumber desc, shoppingcartID"

set RS = Server.CreateObject("ADODB.Recordset")

RS.open SQL, oConn, 0, 1

dim itemCnt : itemCnt = 0
dim noSR : noSR = 0
activeProductCnt = 0
eligibleCnt = 0

do until RS.eof
	noDiscount = RS("NoDiscount")
	if isnumeric(noDiscount) then
		if noDiscount = 0 then noDiscount = false else noDiscount = true
	end if
	nProdIdCheck = RS("itemID")
	nProdQuantity = RS("qty")
	if isnull(rs("musicSkins")) or not rs("musicSkins") then nProdMusicSkins = 0 else nProdMusicSkins = 1
	strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
	strQty = strQty & cStr(nProdQuantity)& ","  ' QTY string for each item, this is for buySafe Use
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		useBrandName = RS("brandName")
		useModelName = RS("modelName")
		partNumber = RS("partNumber")
		sItemName = insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName)
		
		if nProdMusicSkins = 1 then
			if isnull(RS("itemPic")) then
				sItemPic = RS("defaultPic")
				musicSkinsDefaultImg = RS("defaultPic")
			else
				sItemPic = RS("itemPic")
			end if
			if instr(sItemPic,";") > 0 then
				musicSkinsDefaultImg = mid(sItemPic,instr(sItemPic,";")+1)
				sItemPic = left(sItemPic,instr(sItemPic,";")-1)
			end if		
		else
			sItemPic = RS("itemPic")
		end if
		sItemPrice = RS("price_Our")
		
		if isDropship(partNumber) or left(partNumber,3) = "WCD" then
			if left(partNumber,3) = "WCD" then noSR = 1
			srEligible = 0
		else
			if noSR = 0 then srEligible = 1
		end if

		if not isnull(rs("customCost")) then sItemPrice = rs("customCost")
		sRetailPrice = prepInt(RS("price_retail"))
		'Free Bluetooth
		if FreeBluetooth = 1 then
			if cStr(nProdIdCheck) = FreeBluetoothItemID then
				FreeBluetoothPrice = sItemPrice
			end if
		end if
		'Free Product With Phone Purchase
		if RS("typeID") = 16 then PhonePurchased = 1
		'Category Sale
		if CategorySale = 1 then
			if RS("typeID") = CategorySaleTypeID and RS("NoDiscount") = 0 then
				CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
			else
				SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
				SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_Our > 0)"
				SQL = SQL & " AND (R.typeid = '" & CategorySaleTypeID & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
				SQL = SQL & " AND I.NoDiscount = 0"
				dim RSrelated
				set RSrelated = Server.CreateObject("ADODB.Recordset")
				RSrelated.open SQL, oConn, 0, 1
				if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				RSrelated.close
				set RSrelated = nothing
			end if
		end if
		'session("errorSQL") = "nSubTotal = " & nSubTotal & " | nProdIdCheck = " & nProdIdCheck & " | sItemPrice = " & sItemPrice & " | nProdQuantity = " & nProdQuantity
		nItemTotal = nItemTotal + (sItemPrice * nProdQuantity)
		if not noDiscount and rs("typeID") <> 16 then
			discountAmt = discountAmt + (sItemPrice * nProdQuantity)
		end if
		sWeight = sWeight + (cDbl(RS("itemWeight")) * nProdQuantity)
		'WE shipping section
		if sItemPrice = 0 then showPrice = "FREE" else showPrice = formatCurrency(sItemPrice * nProdQuantity)
		productList = productList & sItemName & "##" & nProdQuantity & "##" & showPrice & "##" & srEligible & "##" & sItemPic & "##" & musicSkinsDefaultImg & "##" & nProdMusicSkins & "##" & nProdIdCheck & "@@"
		
		activeProductCnt = activeProductCnt + 1
		if srEligible = 1 then eligibleCnt = eligibleCnt + 1
		
'		WEhtml = WEhtml & "<tr><td align=""left"">" & insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName) & "</td><td align=""center"">" & nProdQuantity & "</td><td align=""right"" colspan='2'>" & showPrice & "</td></tr>" & vbcrlf
		WEhtml2 = WEhtml2 & "<input type=""hidden"" name=""itemID_" & nProdIdCount & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""itemQty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & sItemName & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & nProdQuantity & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & sItemPrice & """>" & vbcrlf

		'eBillme variables
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_number_" & nProdIdCount & """ value=""" & nProdIdCheck & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_qty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_price_" & nProdIdCount & """ value=""" & sItemPrice & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_musicSkins_" & nProdIdCount & """ value=""" & nProdMusicSkins & """>" & vbcrlf
		EBxml = EBxml & "			<itemdetail>" & vbcrlf
		EBxml = EBxml & "				<id>" & nProdIdCheck & "</id>" & vbcrlf
		EBxml = EBxml & "				<name>" & insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName) & "</name>" & vbcrlf
		EBxml = EBxml & "				<quantity>" & nProdQuantity & "</quantity>" & vbcrlf
		EBxml = EBxml & "				<itemcost>" & sItemPrice & "</itemcost>" & vbcrlf
		EBxml = EBxml & "			</itemdetail>"
		
		itemCnt = itemCnt + nProdQuantity
	end if
	RS.movenext
loop
RS.close
set RS = nothing
dim testCart : testCart = prepInt(request.QueryString("testCart"))
if nProdIdCount < 1 and testCart = 0 then
	'basket currently empty
	response.redirect("/sessionexpired.asp")
	response.end
end if
' END basket grab

'GET COUPON VALUES
dim sPromoCode
if request.form("promo") <> "" then
	sPromoCode = SQLquote(request.form("promo"))
else
	sPromoCode = session("promocode")
end if
'if sPromoCode = "" then sPromoCode = prepStr(request.QueryString("passPromocode"))

if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	SQL = "SELECT * FROM WE_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		FreeItemPartNumber = RS("FreeItemPartNumber")
		setValue = RS("setValue")
		oneTime = RS("oneTime")
	else
		sPromoCode = ""
	end if
end if

dim nSubTotal
nSubTotal = nItemTotal

dim shipcost0, shipcost2, shipcost3, shipcost4, shipcost5
shipcost0 = 0.00
if spendThisMuch = 1 and nSubTotal >= stm_total then
	shipcost2 = 0
	shiptype = 2	
else
	shipcost2 = 6.99
end if
shipcost3 = 24.99
shipcost4 = 0.00
'Canadian shipping Cost
shipcost5 = 21.99

if sWeight / 16 < 1 then
	sWeight = 1
else
	sWeight = sWeight / 16
end if

if strItemCheck <> "" then strItemCheck = left(strItemCheck,len(strItemCheck)-1)

if nProdIdCount = 0 then
	htmBasketRows = htmBasketRows & "<tr><td colspan=""7""><i>Basket is currently empty.</i></td></tr>" & vbcrlf
	EmptyBasket = true
end if
%>
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<%
discountTotal = prepInt(discountTotal)
maxDiscount = discountTotal
if discountTotal > nItemTotal then
	discountTotal = nItemTotal
end if
maxDiscount = prepInt(maxDiscount)
activeTotal = nItemTotal - discountTotal

if instr(discountTotal,".") > 0 then discountTotal = left(discountTotal,instr(discountTotal,".")+2)
if oneTime then nsubTotal = nsubTotal - discountTotal

productArray = split(productList,"@@")

dim nAmount, nCATax
nSubTotal = nItemTotal - discountTotal
if nSubTotal = nItemTotal and discountTotal > 0 then nSubTotal = nSubTotal - discountTotal
nAmount = cDbl(shipcost) + nCATax + nSubTotal
%>
<script language="javascript">
function changeValue(span,val) {
	if (document.all) {
		eval(span).innerHTML = val;
	} else if (document.getElementById) {
		document.getElementById(span).innerHTML = val;
	}
}
	
function changeTax(state) {
	//var shippingOptions = document.frmProcessOrder.shippingOptions.value;
	var nCATax = CurrencyFormatted(<%=activeTotal%> * <%=Application("taxMath")%>);
	if (state == "CA") {
		changeValue("CAtaxMsg","Additional <%=Application("taxDisplay")%>% Tax for Shipments Within CA");
		changeValue("CAtax","$" + nCATax);
		document.frmProcessOrder.nCATax.value = nCATax;
	} else {
		changeValue("CAtaxMsg","");
		changeValue("CAtax","$0.00");
		document.frmProcessOrder.nCATax.value = 0;
	}

	getNewShipping();
//	updateGrandTotal(getSelectedRadioValue(document.frmProcessOrder.shiptype), document.frmProcessOrder.shippingTotal.value);
}



function changeShippingAddress(a) {
	var b = eval('document.frmProcessOrder.sAddress1' + a);
	var c = eval('document.frmProcessOrder.sAddress2' + a);
	var d = eval('document.frmProcessOrder.sCity' + a);
	var e = eval('document.frmProcessOrder.sState' + a);
	var f = eval('document.frmProcessOrder.sZip' + a);
	var g = eval('document.frmProcessOrder.sID' + a);
	document.frmProcessOrder.sAddress1.value = b.value;
	document.frmProcessOrder.sAddress2.value = c.value;
	document.frmProcessOrder.sCity.value = d.value;
	document.frmProcessOrder.sState.value = e.value;
	document.frmProcessOrder.sZip.value = f.value;
	changeTax(e.value);
}

function CheckSubmit() {
	trackingBit('clickedPlaceMyOrder');
	
	var f = document.frmProcessOrder;
		
	bValid = true;
	<%if myAccount = "" then%>
	if (f.sameaddress.checked == 0) {
		ShipToBillPerson(f);
	}
	<% end if %>
	
	CheckValidNEW(f.email.value, "Email is a required field!");
	CheckValidNEW(f.fname.value, "Your First Name is a required field!");
	CheckValidNEW(f.lname.value, "Your Last Name is a required field!");
	CheckValidNEW(f.phoneNumber.value, "Your Phone Number is a required field!");
	CheckValidNEW(f.sAddress1.value, "Your Address (for Shipping Address) is a required field!");
	CheckValidNEW(f.sCity.value, "Your City (for Shipping Address) is a required field!");
	sString = f.sState.options[f.sState.selectedIndex].value;
	CheckValidNEW(sString, "State (for Shipping Address) is a required field!");
	CheckValidNEW(f.sZip.value, "Your Zip Code (for Shipping Address) is a required field!");
	if (f.sZip.value.length < 5) {
		alert("You must enter a valid US or Canadian postal code")
		bValid = false
	}
	if (f.sameaddress.checked == true) {
		CheckValidNEW(f.bAddress1.value, "Your Address (for Billing Address) is a required field!");
		CheckValidNEW(f.bCity.value, "Your City (for Billing Address) is a required field!");
		sString = f.bState.options[f.bState.selectedIndex].value;
		CheckValidNEW(sString, "State (for Billing Address) is a required field!");

		CheckValidNEW(f.bZip.value, "Your Zip Code (for Billing Address) is a required field!");
		if (f.bZip.value.length < 5) {
			alert("You must enter a valid US or Canadian postal code")
			bValid = false
		}
	}

/*	if (f.PaymentType[0].checked) {*/
	if (true) {
		sString = f.cc_cardType.options[f.cc_cardType.selectedIndex].value;
		CheckValidNEW(sString, "Credit Card Type is a required field!");
		CheckValidNEW(f.cc_cardOwner.value, "Your Name (as it appears on your credit card) is a required field!");
		CheckValidNEW(f.cardNum.value, "Your Credit Card Number is a required field!");
		CheckValidNEW(f.cc_month.value, "Your Credit Card Expiration Month is a required field!");
		CheckValidNEW(f.cc_year.value, "Your Credit Card Expiration Year is a required field!");
		CheckValidNEW(f.secCode.value, "Your Credit Card Security Code is a required field!");
		
		if (bValid) {
			try {
				document.frmProcessOrder.action = '/framework/processing.asp';
				<%'commented by Terry on 9/26/2012: submitted form twice for IE version 7 and 8 %>
//				document.frmProcessOrder.submit();
				document.getElementById("submitBttn1").style.display = 'none'
				document.getElementById("submitBttn2").style.display = ''
			}
			catch (e) {}
			
			
			trackingBit('validatedPlaceMyOrder');
			pausecomp(1500);<%'Allow tracking request to finish (2012-10-03)%>
			return true;
		}
	}
	else {
		if (bValid) {
			try {			
				document.frmProcessOrder.action = '/cart/process/eBillMe/eBillMe.asp';
				<%'commented by Terry on 9/26/2012: submitted form twice for IE version 7 and 8 %>				
//				document.frmProcessOrder.submit();
				document.getElementById("submitBttn1").style.display = 'none'
				document.getElementById("submitBttn2").style.display = ''
			}
			catch (e) {}
			
			trackingBit('validatedPlaceMyOrder');
			pausecomp(1500);<%'Allow tracking request to finish (2012-10-03)%>
			return true;
		}
	}

	return false;
}

function CurrencyFormatted(amount) {
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
//	caused an issue (10/30/2012)	
//	i = parseInt(i * 100);
//	i = i / 100;
	i = Math.ceil(i * 100) / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}

function pausecomp(ms) {
	ms += new Date().getTime();
	while (new Date() < ms){}
} 

</script>
<form action="/cart/checkout_d.asp?l=1" method="post" name="frmProcessOrder">
<input type="hidden" name="trackingSessionID" value="" id="trackingSessionID">
<input type="hidden" name="shippingTotal" value="">
<input type="hidden" name="shiptypeID" value="<%=shiptype%>">
<input type="hidden" name="sWeight" value="<%=sWeight%>">

<input type="hidden" name="sPromoCode" value="<%=sPromoCode%>">
<input type="hidden" name="nPromo" value="<%=couponid%>">
<input type="hidden" name="discountTotal" value="<%=discountTotal%>">

<input type="hidden" name="nCATax" value="<%=nCATax%>">
<input type="hidden" name="subTotal" value="<%=nItemTotal%>">
<input type="hidden" id="totalPrice" name="grandTotal" value="<%=nAmount%>">
<input type="hidden" name="accountid" value="">
<input type="hidden" name="parentAcctID" value="">
<style>
	a.lnk					{ text-decoration:underline; color:#006699; }
	a.edit					{ text-decoration:underline; color:#006699; font-size:14px; font-weight:bold; }
	
	.hidden					{ display:none; }
	
	.headerBlk				{ float:left; width:100%; background-color:#333; border-top-left-radius:5px; border-top-right-radius:5px; height:36px; padding:17px 0px 0px 0px; margin-top:20px; }
	.headerBlk .title		{ float:left; font-size:18px; color:#fff; margin-left:20px; }
	.headerBlk .txtRequired	{ float:right; font-size:13px; font-style:italic; color:#fff; margin-right:20px; }	
	.headerBlk .checked		{ float:right; margin-right:20px; background:url(/images/checkout/d/checkmark-large.png) no-repeat; width:28px; height:24px; }
	
	.headerGrey				{ float:left; width:100%; background-color:#ebebeb; border-top-left-radius:5px; border-top-right-radius:5px; height:36px; padding:17px 0px 0px 0px; margin-top:20px; }
	.headerGrey .title		{ float:left; font-size:18px; color:#999; margin-left:20px; }
	.headerGrey .txtRequired	{ float:right; font-size:13px; font-style:italic; color:#fff; margin-right:20px; }
	
	.headerOrange				{ float:left; width:100%; background-color:#ff6600; border-top-left-radius:5px; border-top-right-radius:5px; height:36px; padding:17px 0px 0px 0px; margin-top:20px; }
	.headerOrange .title		{ float:left; font-size:18px; color:#fff; margin-left:20px; }
	
	.btnNextOff	{ float:right; background:url(/images/checkout/d/btn-next-inactive.png) no-repeat; width:112px; height:30px; cursor:pointer; margin-top:10px; }
	.btnNextOn	{ float:right; background:url(/images/checkout/d/btn-next-active.png) no-repeat; width:112px; height:30px; cursor:pointer; margin-top:10px; }
	
	.fieldWrapperL					{ border:2px solid #ccc; width:230px; height:24px; padding-top:7px; text-align:center; background-color: #fff; position: relative; border-radius:4px; }
	.fieldWrapperL .msg				{ display:none; }
	.fieldWrapperL .field			{ width: 218px; height:19px; padding: 5px; border: 0px; background-color: #fff; color: #333; position: absolute; top: 1px; left: 1px; }
	.fieldWrapperL .warningIcon		{ display:none; }

	.fieldWrapperL-HL				{ border:2px solid #ff6600; width:230px; height:24px; padding-top:7px; text-align:center; background-color: #ffeac8; position: relative; border-radius:4px; }
	.fieldWrapperL-HL .msg			{ position: absolute; top:-19px; right:0px; font-weight:bold; color:#ff6600; }
	.fieldWrapperL-HL .field		{ width: 218px; height:19px; padding: 5px; border: 0px; background-color: #ffeac8; color: #333; position: absolute; top: 1px; left: 1px; }	
	.fieldWrapperL-HL .warningIcon	{ position: absolute; top: 7px; right: 7px; background:url(/images/checkout/d/validation-ico.png) no-repeat; width:20px; height:17px; }
	
	.fieldWrapperS					{ border:2px solid #ccc; width:105px; height:24px; padding-top:7px; text-align:center; background-color: #fff; position: relative; border-radius:4px; }
	.fieldWrapperS .msg				{ display:none; }
	.fieldWrapperS .field			{ width: 93px; height:19px; padding: 5px; border: 0px; background-color: #fff; color: #333; position: absolute; top: 1px; left: 1px; }
	.fieldWrapperS .warningIcon		{ display:none; }

	.fieldWrapperS-HL				{ border:2px solid #ff6600; width:105px; height:24px; padding-top:7px; text-align:center; background-color: #ffeac8; position: relative; border-radius:4px; }
	.fieldWrapperS-HL .msg			{ position: absolute; top:-19px; right:0px; font-weight:bold; color:#ff6600; }
	.fieldWrapperS-HL .field		{ width: 93px; height:19px; padding: 5px; border: 0px; background-color: #ffeac8; color: #333; position: absolute; top: 1px; left: 1px; }	
	.fieldWrapperS-HL .warningIcon	{ position: absolute; top: 7px; right: 7px; background:url(/images/checkout/d/validation-ico.png) no-repeat; width:20px; height:17px; }	
	
	.cc_options		{ background:url(/images/checkout/d/cc-options.png) no-repeat; width:143px; height:20px; }
	.editable		{ float:left; width:100%; background-color:#ebebeb; padding:20px 0px 20px 0px; }
	.non-editable	{ float:left; width:518px; background-color:#fff; padding:20px 0px 20px 0px; border-left:1px solid #ccc; border-bottom:1px solid #ccc; border-right:1px solid #ccc; }
											
	#leftPanel										{ float:left; width:520px; }
	#leftPanel .body 								{ float:left; width:100%; }	
	#leftPanel .body .txtLabel 						{ float:left; font-size:15px; color:#333; width:200px; margin-left:20px; padding-top:8px; font-weight:bold; }		
	#leftPanel .body .lblAddress					{ float:left; font-size:15px; color:#333; width:200px; margin-left:20px; padding-top:2px; font-weight:bold; }			
	#leftPanel .body .lblEdit						{ float:right; padding-top:8px; margin-right:20px; }
	#leftPanel .body .shippingRowSelected			{ float:left; width:498px; border:1px solid #ccc; background-color:#ffffcc; color:#333; margin:7px 10px 0px 10px; padding:3px 0px 3px 0px; }
	#leftPanel .body .shippingRowSelected .method	{ float:left; width:190px; text-align:left; padding:0px 0px 0px 10px; }	
	#leftPanel .body .shippingRowSelected .days		{ float:left; width:160px; text-align:right; padding-top:2px; }
	#leftPanel .body .shippingRowSelected .price	{ float:left; width:128px; text-align:right; font-weight:bold; padding:2px 10px 0px 0px; }	
	
	#leftPanel .body .shippingRow					{ float:left; width:498px; border:1px solid #ebebeb; background-color:#ebebeb; color:#333; margin:7px 10px 0px 10px; padding:3px 0px 3px 0px; }
	#leftPanel .body .shippingRow .method			{ float:left; width:190px; text-align:left; padding:0px 0px 0px 10px; }	
	#leftPanel .body .shippingRow .days				{ float:left; width:160px; text-align:right; padding-top:2px; }
	#leftPanel .body .shippingRow .price			{ float:left; width:128px; text-align:right; padding:2px 10px 0px 0px; }		
	#leftPanel .body .shippingRow:hover				{ float:left; width:498px; border:1px solid #ccc; background-color:#ffffee; color:#333; margin:7px 10px 0px 10px; padding:3px 0px 3px 0px; cursor:pointer; }
	#leftPanel .body .shippingRow:hover	.price		{ float:left; width:128px; text-align:right; font-weight:bold; padding:2px 10px 0px 0px; }	
	
	#rightPanel 								{ float:left; width:260px; margin-left:20px; }
	#rightPanel #detail							{ float:left; width:238px; border-left:1px solid #ccc; border-bottom:1px solid #ccc; border-right:1px solid #ccc; padding:10px; }
	#rightPanel #detail	.lineItem 				{ border-top:1px solid #ccc; margin-top:10px; padding-top:10px; float:left; width:100%; }
	#rightPanel #detail	.lineItem .qty 			{ border:1px solid #ccc; width:35px; height:22px; padding-top:2px; text-align:center; font-weight:bold; color:#333; border-radius:2px; }
	#rightPanel #detail	.lineItem .img			{ float:left; width:60px; }
	#rightPanel #detail	.lineItem .itemDetail	{ float:left; width:168px; margin-left:10px; text-align:left; font-size:12px; }
	#rightPanel #detail	.lineItem .itemDetail .price 		{ float:left; font-size:14px; font-weight:bold; color:#ff6600; }	
	#rightPanel #detail	.lineItem .itemDetail .stockIcon 	{ float:left; padding-top:2px; background:url(/images/checkout/d/checkmark-circle.png) no-repeat; width:12px; height:10px; }
	#rightPanel #detail	.lineItem .itemDetail .instock 		{ float:left; margin-left:5px; font-weight:bold; color:#333; }	
												 
	#ordertotal		{ float:left; width:100%; padding-top:15px; }
	#learnMoreBox	{ font-size:14px; float:left; width:496px; padding:10px; text-align:center; border:2px solid #ccc; margin-top:10px; color:#333; }
</style>
<script>
	var debug = 0;
	var frmFieldCheck = 0;
	var fCheck = function() {
		var $this = $(this);
		if ($this.val()	 == '') {
			if ($this.parent().hasClass('fieldWrapperL')) {
				$this.parent().attr('class', 'fieldWrapperL-HL');
			} else if ($this.parent().hasClass('fieldWrapperS')) {
				$this.parent().attr('class', 'fieldWrapperS-HL');
			}
			$this.focus();
			
			return false;
		} else {
			if ($this.parent().hasClass('fieldWrapperL-HL')) {
				$this.parent().attr('class', 'fieldWrapperL');
			} else if ($this.parent().hasClass('fieldWrapperS-HL')) {
				$this.parent().attr('class', 'fieldWrapperS');
			}
		}
		frmFieldCheck++;
	}	
	
	function getNewShipping()
	{
		debug++;
//		document.getElementById('terry').innerHTML = document.getElementById('terry').innerHTML + '<br>' + debug + 'getnewshipping<br>';
		document.getElementById('dumpZone').innerHTML = "";	
		var zipcode 			=	document.frmProcessOrder.sZip.value;
		var curShippingTotal	=	document.frmProcessOrder.shippingTotal.value;
		var curShippingType		=	document.frmProcessOrder.shiptypeID.value;
		var state 				= 	document.frmProcessOrder.sState.value;
		var international 		= 	0;
//		document.getElementById('terry').innerHTML = document.getElementById('terry').innerHTML + 'aaaa' + '<br>';									
		if (state == "AB" || state == "BC" || state == "MB" || state == "NB" || state == "NL" || state == "NS" || state == "NT" || state == "NU" || state == "ON" || state == "PE" || state == "QC" || state == "SK" || state == "YT") {
			document.frmProcessOrder.shipcost2.value = '<%=shipcost5%>';
			international = 1;
		} else {
			document.frmProcessOrder.shipcost2.value = '<%=shipcost2%>';
		}

		if (zipcode.length >= 5) {
			updateURL = '/ajax/shippingDetails_d.asp?zip=' + zipcode + '&weight=<%=sWeight%>&shiptype=' + curShippingType + '&nTotalQuantity=<%=itemCnt%>&nSubTotal=<%=nSubTotal%>&useFunction=getupsshipping&international='+international;
//			document.getElementById('terry').innerHTML = document.getElementById('terry').innerHTML + updateURL + '<br>';
			ajax(updateURL,'dumpZone');
			checkUpdate('newShipping');
		}
	}
	
	function checkUpdate(checkType)
	{
		if (checkType == "newShipping")
		{
			if ("" == document.getElementById('dumpZone').innerHTML)
			{
				setTimeout('checkUpdate(\'newShipping\')', 50);
			} else
			{
				document.getElementById('shippingDetails').innerHTML = document.getElementById('dumpZone').innerHTML;
				newShipType = getSelectedRadioValue(document.frmProcessOrder.shiptype);
	
				if (newShipType == "") 
				{
	//				document.frmProcessOrder.shiptype[0].checked = true;
					updateGrandTotal(0,<%=shipcost0%>,'checkupdate1');
				} else
				{
					updateGrandTotal(newShipType,eval('document.frmProcessOrder.shipcost' + newShipType).value,'checkupdate2');
				}
			}
		}
	}
	
	function IsEmail(email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}
	
	function getSelectedRadioValue(buttonGroup) {
		var i = getSelectedRadio(buttonGroup);
	   if (i == -1) {
		  return "";
	   } else {
		  if (buttonGroup[i]) { // Make sure the button group is an array (not just one button)
			 return buttonGroup[i].value;
		  } else { // The button group is just the one button, and it is checked
			 return buttonGroup.value;
		  }
	   }
	}
	
	function getSelectedRadio(buttonGroup) {
	   if (buttonGroup[0]) {
		  for (var i=0; i<buttonGroup.length; i++) {
			 if (buttonGroup[i].checked) {
				return i;
			 }
		  }
	   } else {
		  if (buttonGroup.checked) { return 0; } // if the one button is checked, return zero
	   }
	   return -1;
	}

	function updateGrandTotal(shipID,shipAmt,debug) {
//		document.getElementById('terry').innerHTML = document.getElementById('terry').innerHTML + 'shipID:' + shipID + ', shipAmt:' + shipAmt + ', debug:' + debug + '<br>';
		document.frmProcessOrder.shiptypeID.value = shipID;
		if (eval('document.frmProcessOrder.shipcost' + shipID) == undefined) {
			document.frmProcessOrder.shippingTotal.value = shipAmt;
		} else {
			document.frmProcessOrder.shippingTotal.value = eval('document.frmProcessOrder.shipcost' + shipID).value;
		}
	
		// shipcost is null when it is shoprunner
		if (document.getElementById("shipcost") != null) document.getElementById("shipcost").innerHTML = "$" + CurrencyFormatted(document.frmProcessOrder.shippingTotal.value);
	
		var GrandTotal = Number(document.frmProcessOrder.subTotal.value) + Number(document.frmProcessOrder.nCATax.value) + Number(document.frmProcessOrder.shippingTotal.value);
		var maxDiscount = <%=maxDiscount%>;
	
		if ((GrandTotal - maxDiscount) < 0) 
		{
			changeValue('discountAmt', "($" + CurrencyFormatted(GrandTotal) + ')');
			document.frmProcessOrder.discountTotal.value = CurrencyFormatted(GrandTotal);
			GrandTotal = 0
		}
		else 
		{
			if (maxDiscount > 0) 
			{
				changeValue('discountAmt', "($" + CurrencyFormatted(maxDiscount) + ')');
				document.frmProcessOrder.discountTotal.value = CurrencyFormatted(maxDiscount);
			}
			GrandTotal = GrandTotal - maxDiscount
		}
		
		changeValue("GrandTotal","$" + CurrencyFormatted(GrandTotal));
		document.frmProcessOrder.grandTotal.value = CurrencyFormatted(GrandTotal);
	}
	
	$(document).ready(function() {
		$('#id_email').focus();
		changeTax(document.frmProcessOrder.sState.value);
	});

	function onEmail(o) {
		if (o.value.length < 7) {
			$('#step1').find('.btnNextOn').addClass('hidden');
			$('#step1').find('.btnNextOff').removeClass('hidden');
		} else {
			$('#step1').find('.btnNextOff').addClass('hidden');
			$('#step1').find('.btnNextOn').removeClass('hidden');
		}
	}
	
	function onMethod(method) {
		if (method == "cc") {
			document.frmProcessOrder.PaymentType[0].checked = true;
		} else {
			document.frmProcessOrder.PaymentType[1].checked = true;
		}
	}
	
	function onShippingOption(id) {
		var rdo = $('#ship_'+id).find('input[type=radio]').filter(':visible:first');
		var shipcost = $('#id_shipcost'+id).val();
		document.frmProcessOrder.shiptypeID.value = rdo.val();
		rdo.attr('checked', 'checked');
		
//		$('#ship_'+id).closest(".editable").find('.shippingRowSelected').toggleClass('shippingRowSelected shippingRow');
		$('#ship_'+id).parent().find('.shippingRowSelected').toggleClass('shippingRowSelected shippingRow');
		$('#ship_'+id).toggleClass('shippingRow shippingRowSelected');
		
		updateGrandTotal(rdo.val(),shipcost,'onShippingOption');
	}
	
	function onShippingInfo(id) {
		var frm = document.frmProcessOrder;
		var nFieldVal = 0;
		
		if (frm.fname.value != '') nFieldVal++;
		if (frm.lname.value != '') nFieldVal++;
		if (frm.saddress1.value != '') nFieldVal++;		
		if (frm.sCity.value != '') nFieldVal++;		
		if (frm.sState.options[frm.sState.selectedIndex].value != '') nFieldVal++;		
		if (frm.sZip.value != '') nFieldVal++;		
		if (frm.phoneNumber.value != '') nFieldVal++;
		
		if (nFieldVal >= 7) {
			$('#'+id).find('.btnNextOff').addClass('hidden');
			$('#'+id).find('.btnNextOn').removeClass('hidden');
		} else {
			$('#'+id).find('.btnNextOn').addClass('hidden');
			$('#'+id).find('.btnNextOff').removeClass('hidden');
		}
	}
	
	function onBillingInfo(id) {
		var frm = document.frmProcessOrder;
		var nFieldVal = 0;
		
		if (frm.baddress1.value != '') nFieldVal++;		
		if (frm.bCity.value != '') nFieldVal++;		
		if (frm.bState.options[frm.bState.selectedIndex].value != '') nFieldVal++;		
		if (frm.bZip.value != '') nFieldVal++;		
		
		if (nFieldVal >= 4) {
			$('#'+id).find('.btnNextOff').addClass('hidden');
			$('#'+id).find('.btnNextOn').removeClass('hidden');
		} else {
			$('#'+id).find('.btnNextOn').addClass('hidden');
			$('#'+id).find('.btnNextOff').removeClass('hidden');
		}
	}	
	
	function toggleHeader(o, onoff) {
		if (onoff == "on") {
			$(o).find('.header').attr('class', 'header headerBlk');
		} else {
			$(o).find('.header').attr('class', 'header headerGrey');
		}
	}

	function checkHeader(o, checked) {
		if (checked) {
			$(o).find('.header').find('.txtRequired').addClass('hidden');
			$(o).find('.header').find('.checked').removeClass('hidden');		
		} else {
			$(o).find('.header').find('.txtRequired').removeClass('hidden');
			$(o).find('.header').find('.checked').addClass('hidden');		
		}
	}
	
	function onChkBilling(checked) {
		if (checked) {
			$('#step4').addClass('hidden');
		} else {
			$('#step4').removeClass('hidden');
			gotoNext(4);
		}
	}
	
	function editStep(step) {
		var oStep = $('#step'+step);

		oStep.find('.editable').removeClass('hidden');
		oStep.find('.non-editable').addClass('hidden');
		oStep.find('.btns').removeClass('hidden');
		oStep.find('input[type=text]').filter(':visible:first').focus();
		oStep.find('.btnNextOn').removeClass('hidden');
		oStep.find('.btnNextOff').addClass('hidden');

		checkHeader(oStep, false);		
	}

	function gotoNext(nextStep) {
		frmFieldCheck = 0;
		var frm = document.frmProcessOrder;

		var oCurrent = $('#step'+(nextStep-1));
		var oNext = $('#step'+nextStep);
		var oNext2 = $('#step'+(nextStep+1));
		var jump = false;
		
		switch(nextStep)
		{
		case 2:	// email -> checkout
			if (!IsEmail($('#id_email').val())) {
				$('#id_email').parent().attr('class', 'fieldWrapperL-HL');
				$('#id_email').focus();
				return false;
			}

			$('#id_email').parent().attr('class', 'fieldWrapperL');
			oCurrent.find('.non-editable').find('.txtLabel').text($('#id_email').val());		

			break;
		case 3: // checkout -> shipping info
			if (getSelectedRadioValue(frm.PaymentType) == "cc") $('#cc').removeClass('hidden');
			else $('#pp').removeClass('hidden');

			break;
		case 4: // shipping info -> billing info
			name = frm.fname.value + " " + frm.lname.value;
			saddress = frm.saddress1.value;
			sCity = frm.sCity.value;
			sState = frm.sState.options[frm.sState.selectedIndex].value;
			sZip = frm.sZip.value;
			phonenumber = frm.phoneNumber.value;
			
			oCurrent.find('input[type=text]').each(fCheck);
			oCurrent.find('select[name=sState]').each(fCheck);
			if (frmFieldCheck < 7) return false;

			oCurrent.find('.name').text(name);
			oCurrent.find('.street').text(saddress);
			oCurrent.find('.city').text(sCity + ', ' + sState + ' ' + sZip);
			oCurrent.find('.phone').text(phonenumber);
			
			// skip billing info if sameaddress checked
			if ($('#id_sameaddress').is(':checked')) {
				frm.baddress1.value = frm.saddress1.value;
				frm.bCity.value = frm.sCity.value;
				frm.bState.selectedIndex = frm.sState.selectedIndex;
				frm.bZip.value = frm.sZip.value;

//				gotoNext(5);
				oCurrent.find('.editable').addClass('hidden');
				oCurrent.find('.non-editable').removeClass('hidden');
				oCurrent.find('.btns').addClass('hidden');
				checkHeader(oCurrent, true);
						
				oNext2.find('.body').removeClass('hidden');
				oNext2.find('input[type=text]').filter(':visible:first').focus();
				toggleHeader(oNext2, "on");
				
				$('html, body').animate({
					scrollTop: oNext2.find('.header').offset().top-20
				}, 1000);
				
				return false;
			}
			
			break;
		case 5: // billing info -> shipping options
			name = frm.fname.value + " " + frm.lname.value;
			baddress = frm.baddress1.value;
			bCity = frm.bCity.value;
			bState = frm.bState.options[frm.bState.selectedIndex].value;
			bZip = frm.bZip.value;

			oCurrent.find('input[type=text]').each(fCheck);
			oCurrent.find('select').each(fCheck);
			if (frmFieldCheck < 4) return false;
			
			oCurrent.find('.name').text(name);
			oCurrent.find('.street').text(baddress);
			oCurrent.find('.city').text(bCity + ', ' + bState + ' ' + bZip);

			break;
		case 6: // shipping options -> payment
			shiptypeID = document.frmProcessOrder.shiptypeID.value;
			shipDesc = $('#ship_'+shiptypeID).find('.desc').html();
			shipDays = $('#ship_'+shiptypeID).find('.days').html();
			shipPrice = $('#ship_'+shiptypeID).find('.price').html();
			
			oCurrent.find('.non-editable').find('.desc').html(shipDesc);
			oCurrent.find('.non-editable').find('.days').html(shipDays);
			oCurrent.find('.non-editable').find('.price').html(shipPrice);
									
			$('#learnMoreBox').removeClass('hidden');
			$('#checkoutBtn').removeClass('hidden');

			break;
		case 7: // payment -> checkout

			oCurrent.find('input[type=text]').each(fCheck);
			oCurrent.find('select').each(fCheck);
			if (frmFieldCheck < 5) return false;
			
			break;
		}

		oCurrent.find('.editable').addClass('hidden');
		oCurrent.find('.non-editable').removeClass('hidden');
		oCurrent.find('.btns').addClass('hidden');
		checkHeader(oCurrent, true);
				
		oNext.find('.body').removeClass('hidden');
		oNext.find('input[type=text]').filter(':visible:first').focus();
		toggleHeader(oNext, "on");
		
		$('html, body').animate({
			scrollTop: oNext.find('.header').offset().top-20
		}, 1000);
		
		return true;
	}
</script>
<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
    	<td id="checkoutPage" width="100%">
        	<div id="terry" class="ffl">&nbsp;</div>
        	<div class="ffl">
                <div class="ffl" style="border-bottom:1px solid #ccc;">
                	<div class="fl">
	                    <div class="fl" style="font-size:30px; color:#333;">Secure Checkout</div>
                        <div class="fl" style="padding:8px 0px 0px 8px;"><img src="/images/checkout/lock-icon.png" border="0" /></div>
                    </div>
                    <div class="fr" style="padding-top:5px;">
                    	<div class="fl"><img src="/images/checkout/circle-1-off.png" border="0" /></div>
                    	<div class="fl" style="color:#ccc; font-size:16px; padding:2px 0px 0px 5px;">Cart Summary</div>
                    	<div class="fl" style="padding-left:15px;"><img src="/images/checkout/circle-2-on.png" border="0" /></div>
                    	<div class="fl" style="font-size:16px; padding:2px 0px 0px 5px; font-weight:bold;">Secure Checkout</div>
                    	<div class="fl" style="padding-left:15px;"><img src="/images/checkout/circle-3-off.png" border="0" /></div>
                    	<div class="fl" style="color:#ccc; font-size:16px; padding:2px 0px 0px 5px;">Receipt</div>
                    </div>
                </div>
            </div>
        	<div class="ffl">
            	<div id="leftPanel">
					<div id="step1">
                        <div class="header headerBlk">
                            <div class="title">EMAIL ADDRESS</div>
							<div class="txtRequired">* Required</div>
                            <div class="checked hidden"></div>
                        </div>
                        <div class="body">
                            <div class="editable">
                                <div class="ffl">
                                    <div class="txtLabel">Email Address: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperL">
                                            <div class="msg">Please enter valid email address.</div>
                                            <input id="id_email" type="text" class="field" name="email" onkeyup="onEmail(this)" onkeypress="if ((event.keyCode==13) || (event.keyCode==9)) gotoNext(2)" autocomplete="off" />
                                            <div class="warningIcon"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="non-editable hidden">
                                <div class="ffl">
                                    <div class="txtLabel"></div>
                                    <div class="lblEdit"><a href="javascript:editStep(1)" class="edit">Edit</a></div>
                                </div>
                            </div>
                            <div class="btns">
                                <div class="btnNextOff" onclick="gotoNext(2)"></div>
                                <div class="btnNextOn hidden" onclick="gotoNext(2)"></div>
                            </div>
                        </div>
                    </div>
                    
                	<div id="step2">
                        <div class="header headerGrey">
                            <div class="title">CHECKOUT METHOD</div>
                            <div class="txtRequired">* Required</div>
                            <div class="checked hidden"></div>
                        </div>
						<div class="body hidden">
                            <div class="editable">
                                <div style="float:left; width:220px; color:#333; margin-left:20px; font-weight:bold;">
                                    <div class="ffl">
                                        <div class="fl"><input type="radio" id="rdo_cc" name="PaymentType" value="cc" checked="checked" /> </div>
                                        <div class="fl" style="font-size:14px; padding-top:2px; cursor:pointer;" onclick="onMethod('cc')">Credit/Debit Card</div>
                                    </div>
                                    <div class="ffl" style="padding:10px 0px 0px 20px;">
                                        <div class="cc_options" style="cursor:pointer;" onclick="onMethod('cc')"></div>
                                    </div>
                                </div>
                                <div class="fl">
                                    <div class="clear">
                                        <div class="fl"><input type="radio" id="rdo_pp" name="PaymentType" value="pp" /> </div>
                                        <div class="fl"><img src="/images/checkout/d/btn-paypal.png" border="0" style="cursor:pointer;" onclick="onMethod('pp')" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="non-editable hidden">
                                <div id="cc" class="hidden" style="float:left; width:220px; color:#333; margin-left:20px; font-weight:bold;">
                                    <div class="ffl">
                                        <div class="fl" style="font-size:14px; padding-top:2px;">Credit/Debit Card</div>
                                    </div>
                                    <div class="ffl" style="padding:10px 0px 0px 0px;">
                                        <div class="cc_options"></div>
                                    </div>
                                </div>
                                <div id="pp" class="hidden" style="float:left; width:220px; margin-left:20px;">
	                                <img src="/images/checkout/d/btn-paypal.png" border="0" />
                                </div>
                                <div class="lblEdit"><a href="javascript:editStep(2)" class="edit">Edit</a></div>
                            </div>
                            <div class="btns">
	                            <div class="btnNextOn" onclick="gotoNext(3)"></div>
                            </div>
                        </div>
                    </div>
                    
                	<div id="step3">
                        <div class="header headerGrey">
                            <div class="title">SHIPPING INFORMATION</div>
                            <div class="txtRequired">* Required</div>
                            <div class="checked hidden"></div>
                        </div>
                        <div class="body hidden">
						    <div class="editable">
                                <div class="ffl">
                                    <div class="txtLabel">First Name: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperL">
                                            <div class="msg">This field is required.</div>
                                            <input class="field" type="text" name="fname" autocomplete="off" onkeyup="onShippingInfo('step3')" onkeypress="if (event.keyCode==13) gotoNext(4)" value="terry">
                                            <div class="warningIcon"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ffl" style="padding-top:20px;">
                                    <div class="txtLabel">Last Name: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperL">
                                            <div class="msg">This field is required.</div>
                                            <input class="field" type="text" name="lname" autocomplete="off" onkeyup="onShippingInfo('step3')" onkeypress="if (event.keyCode==13) gotoNext(4)" value="kim">
                                            <div class="warningIcon"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ffl" style="padding-top:20px;">
                                    <div class="txtLabel">Street Address: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperL">
                                            <div class="msg">This field is required.</div>
                                            <input class="field" type="text" name="saddress1" autocomplete="off" onkeyup="onShippingInfo('step3')" onkeypress="if (event.keyCode==13) gotoNext(4)" value="11701 bellflower blvd.">
                                            <input type="hidden" name="saddress2" value="">
                                            <div class="warningIcon"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ffl" style="padding-top:20px;">
                                    <div class="txtLabel">City: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperL">
                                            <div class="msg">This field is required.</div>
                                            <input class="field" type="text" name="sCity" autocomplete="off" onkeyup="onShippingInfo('step3')" onkeypress="if (event.keyCode==13) gotoNext(4)" value="downey">
                                            <div class="warningIcon"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ffl" style="padding-top:20px;">
                                    <div class="txtLabel">State: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperL">
                                            <div class="msg">This field is required.</div>
                                            <select id="id_sState" name="sState" class="field" style="width:225px; height:30px; border:0px; font-style:italic; color:#555;" onchange="onShippingInfo('step3'); changeTax(this.value);;" >
	                                            <%getStates("")%>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="ffl" style="padding-top:20px;">
                                    <div class="txtLabel">Zip Code: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperS">
                                            <div class="msg">Required.</div>
                                            <input class="field" type="text" name="sZip" autocomplete="off" onkeyup="onShippingInfo('step3'); getNewShipping();" onkeypress="if (event.keyCode==13) gotoNext(4)" value="12345">
                                            <div class="warningIcon"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ffl" style="padding-top:20px;">
                                    <div class="txtLabel">Phone Number: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperL">
                                            <div class="msg">This field is required.</div>
                                            <input class="field" type="text" name="phoneNumber" autocomplete="off" onkeyup="onShippingInfo('step3')" onkeypress="if (event.keyCode==13) gotoNext(4)" value="123-456-7890">
                                            <div class="warningIcon"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ffl" style="padding-top:20px;">
                                    <div class="txtLabel">&nbsp;</div>
                                    <div class="fl">
                                        <div class="fl"><input id="id_sameaddress" type="checkbox" name="sameaddress" value="yes" checked="checked" onclick="onChkBilling(this.checked)" /> </div>
                                        <div class="fl" style="color:#333; padding:2px 0px 0px 3px; font-size:13px;">Also use this as my billing address.</div>
                                    </div>
                                </div>
                            </div>                        
						    <div class="non-editable hidden">
                                <div class="ffl">
                                    <div class="txtLabel">
                                        <div class="name lblAddress"></div>
                                        <div class="street lblAddress"></div>
                                        <div class="city lblAddress"></div>
                                        <div class="phone lblAddress"></div>
                                    </div>
                                    <div class="lblEdit"><a href="javascript:editStep(3)" class="edit">Edit</a></div>
                                </div>
                            </div>  
							 <div class="btns">
                                <div class="btnNextOff" onclick="gotoNext(4)"></div>
                                <div class="btnNextOn hidden" onclick="gotoNext(4)"></div>
                            </div>
                        </div>
                    </div>
                    
					<div id="step4" class="hidden">
                        <div class="header headerGrey">
                            <div class="title">BILLING INFORMATION</div>
                            <div class="txtRequired">* Required</div>
                            <div class="checked hidden"></div>
                        </div>
                        <div class="body hidden">
                            <div class="editable">
                                <div class="ffl">
                                    <div class="txtLabel">Street Address: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperL">
                                            <div class="msg">This field is required.</div>
                                            <input class="field" type="text" name="baddress1" autocomplete="off" onkeyup="onBillingInfo('step4')" onkeypress="if (event.keyCode==13) gotoNext(5)" >
                                            <input type="hidden" name="baddress2" value="">
                                            <div class="warningIcon"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ffl" style="padding-top:20px;">
                                    <div class="txtLabel">City: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperL">
                                            <div class="msg">This field is required.</div>
                                            <input class="field" type="text" name="bCity" autocomplete="off" onkeyup="onBillingInfo('step4')" onkeypress="if (event.keyCode==13) gotoNext(5)" >
                                            <div class="warningIcon"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ffl" style="padding-top:20px;">
                                    <div class="txtLabel">State: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperL">
                                            <div class="msg">This field is required.</div>
                                            <select name="bState" class="field" style="width:225px; height:30px; border:0px; font-style:italic; color:#555;" onchange="onBillingInfo('step4')" >
	                                            <%getStates("")%>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="ffl" style="padding-top:20px;">
                                    <div class="txtLabel">Zip Code: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperS">
                                            <div class="msg">Required.</div>
                                            <input class="field" type="text" name="bZip" autocomplete="off" onkeyup="onBillingInfo('step4')" onkeypress="if (event.keyCode==13) gotoNext(5)" >
                                            <div class="warningIcon"></div>
                                        </div>
                                    </div>
                                </div>
							</div>
                            <div class="non-editable hidden">
                                <div class="ffl">
                                    <div class="txtLabel">
                                        <div class="name lblAddress"></div>
                                        <div class="street lblAddress"></div>
                                        <div class="city lblAddress"></div>
                                    </div>
                                    <div class="lblEdit"><a href="javascript:editStep(4)" class="edit">Edit</a></div>
                                </div>
                            </div>
							<div class="btns">
                                <div class="btnNextOff" onclick="gotoNext(5)"></div>
                                <div class="btnNextOn hidden" onclick="gotoNext(5)"></div>
                            </div>
                        </div>
                    </div>                    
                    
                	<div id="step5">
                        <div class="header headerGrey">
                            <div class="title">SHIPPING OPTIONS</div>
                            <div class="txtRequired">* Required</div>
                            <div class="checked hidden"></div>
                        </div>
                        <div class="body hidden">
						    <div class="editable">
	                            <div id="shippingDetails">
                                    <div id="ship_0" class="<%if shiptype = 0 then%>shippingRowSelected<%else%>shippingRow<%end if%>" onclick="onShippingOption(0)">
                                        <div class="method">
                                            <div class="fl"><input type="radio" name="shiptype" value="0" <%if shiptype = 0 then%>checked<%end if%> /></div>
                                            <div class="desc fl" style="padding:2px 0px 0px 2px;"><strong>FREE:</strong> USPS First Class</div>
                                        </div>
                                        <div class="days">4-10 Business Days</div>
                                        <div class="price"><%=formatcurrency(shipcost0)%></div>
                                        <input type="hidden" id="id_shipcost0" name="shipcost0" value="<%=shipcost0%>" />
                                    </div>
                                    <div id="ship_2" class="<%if shiptype = 2 then%>shippingRowSelected<%else%>shippingRow<%end if%>" onclick="onShippingOption(2)">
                                        <div class="method">
                                            <div class="fl"><input type="radio" name="shiptype" value="2" <%if shiptype = 2 then%>checked<%end if%> /></div>
                                            <div class="desc fl" style="padding:2px 0px 0px 2px;">USPS Priority Mail</div>
                                        </div>
                                        <div class="days">2-4 Business Days</div>
                                        <div class="price"><%=formatcurrency(shipcost2)%></div>
                                        <input type="hidden" id="id_shipcost2" name="shipcost2" value="<%=shipcost2%>" />
                                    </div>
                                    <div id="ship_3" class="<%if shiptype = 3 then%>shippingRowSelected<%else%>shippingRow<%end if%>" onclick="onShippingOption(3)">
                                        <div class="method">
                                            <div class="fl"><input type="radio" name="shiptype" value="3" <%if shiptype = 3 then%>checked<%end if%> /></div>
                                            <div class="desc fl" style="padding:2px 0px 0px 2px;">USPS Express Mail</div>
                                        </div>
                                        <div class="days">1-2 Business Days</div>
                                        <div class="price"><%=formatcurrency(shipcost3)%></div>
                                        <input type="hidden" id="id_shipcost3" name="shipcost3" value="<%=shipcost3%>" />
                                    </div>
                                </div>
                            </div>
                            <div class="non-editable hidden">
                                <div class="ffl">
                                    <div class="txtLabel">
                                        <div class="desc lblAddress"></div>
                                        <div class="days lblAddress"></div>
                                        <div class="price lblAddress"></div>
                                    </div>
                                    <div class="lblEdit"><a href="javascript:editStep(5)" class="edit">Edit</a></div>
                                </div>
                            </div>                         
							<div class="btns">
                                <div class="btnNextOff hidden" onclick="gotoNext(6)"></div>
                                <div class="btnNextOn" onclick="gotoNext(6)"></div>
                            </div>                            
                        </div>
                    </div>
                    
                	<div id="step6">
                        <div class="header headerGrey">
                            <div class="title">SECURE PAYMENT</div>
                            <div class="txtRequired">* Required</div>
                            <div class="checked hidden"></div>
                        </div>
                        <div class="body hidden">
						    <div class="editable">
                                <div class="ffl">
                                    <div class="txtLabel">Cardholder Name: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperL">
                                            <div class="msg">This field is required.</div>
                                            <input class="field" type="text" name="cc_cardOwner" autocomplete="off">
                                            <div class="warningIcon"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ffl" style="padding-top:20px;">
                                    <div class="txtLabel">Card Number: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperL">
                                            <div class="msg">This field is required.</div>
                                            <input class="field" type="text" maxlength="16" name="cardNum">
                                            <div class="warningIcon"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ffl" style="padding-top:10px;">
                                    <div class="txtLabel">&nbsp;</div>
                                    <div class="fl">
                                        <div class="cc_options"></div>
                                    </div>
                                </div>
                                <div class="ffl" style="padding-top:20px;">
                                    <div class="txtLabel">Expiration Date: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperS">
                                            <div class="msg">Required.</div>
                                            <select name="cc_month" class="field" style="width:100px; height:30px; border:0px; font-style:italic; color:#555;">
                                                <option value="">Month</option>
                                                <option value="01">01 (January)</option>
                                                <option value="02">02 (February)</option>
                                                <option value="03">03 (March)</option>
                                                <option value="04">04 (April)</option>
                                                <option value="05">05 (May)</option>
                                                <option value="06">06 (June)</option>
                                                <option value="07">07 (July)</option>
                                                <option value="08">08 (August)</option>
                                                <option value="09">09 (September)</option>
                                                <option value="10">10 (October)</option>
                                                <option value="11">11 (November)</option>
                                                <option value="12">12 (December)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="fl" style="padding-left:10px;">
                                        <div class="fieldWrapperS">
                                            <div class="msg">Required.</div>
                                            <select name="cc_year" class="field" style="width:100px; height:30px; border:0px; font-style:italic; color:#555;">
                                                <option value="">Year</option>
                                                <%
                                                for countYear = 0 to 14
                                                    response.write "<option value=""" & right(cStr(year(date) + countYear),2) & """>" & cStr(year(date) + countYear) & "</option>" & vbcrlf
                                                next
                                                %>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="ffl" style="padding-top:20px;">
                                    <div class="txtLabel">Security Code: <span style="color:#ff6600;">*</span></div>
                                    <div class="fl">
                                        <div class="fieldWrapperS">
                                            <div class="msg">Required.</div>
                                            <input class="field" type="text" name="secCode" maxlength="4">
                                            <div class="warningIcon"></div>
                                        </div>
                                    </div>
                                    <div class="fl" style="padding:10px 0px 0px 7px; position:relative;" onmouseover="document.getElementById('cc-tooltip').style.display=''" onmouseout="document.getElementById('cc-tooltip').style.display='none'">
                                        <a href="javascript:void(0)" class="lnk">What's this?</a>
                                        <div id="cc-tooltip" style="position:absolute; top:-20px; left:75px; background:url(/images/checkout/d/cc-tooltip.png) no-repeat; width:384px; height:70px; display:none;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="learnMoreBox" class="hidden">
                    	<div class="fl" style="padding-left:80px;"><img src="/images/checkout/d/lock-small.png" border="0" /></div>
                    	<div class="fl" style="padding-left:7px;">This is a secure 128-bit SSL encrypted payment.</div>
                    	<div class="fl" style="padding-left:7px;"><a href="" class="lnk">Learn More.</a></div>
                    </div>
                    <div id="checkoutBtn" class="ffl hidden" align="center" style="padding-top:20px;">
                        <div id="submitBttn1"><input type="image" id="purchaseBtn" onclick="return gotoNext(7)" src="/images/cart/place_order_button.jpg" width="272" height="56" alt="Place My Order"></div>
                        <div id="submitBttn2" style="font-weight:bold; font-size:18px; color:#000; display:none;">Please wait while<br />we process your order</div>
                        <input type="hidden" name="submitted" value="submitted">
                        <input type="hidden" name="PaymentType" value="cc">
                    </div>
                    
                    <div class="clear"></div>
                    <div id="step7"></div>
                </div>
                
            	<div id="rightPanel">
                	<div class="ffl" id="box_ordersummary" style="color:#333;">
                        <div class="headerOrange">
                            <div class="title">ORDER SUMMARY</div>
                        </div>
                        <div id="detail">
                            <div class="ffl" style="text-align:center; font-size:14px;">2 ITEMS IN YOUR CART</div>
        					<div id="cartItems">
                            <%
                            for i = 0 to (ubound(productArray)-1)
								lineID = i+1
                                curArray = split(productArray(i),"##")
                                activeProductCnt = activeProductCnt + 1
                                cartItemDesc = curArray(0)
                                cartItemQty = curArray(1)
                                cartItemPrice = curArray(2)
                                cartItemSR = prepInt(curArray(3))
                                cartItemImage = curArray(4)
                                cartItemMSDefaultImage = curArray(5)
                                cartItemMusicSkins = prepInt(curArray(6))
                                cartItemID = prepInt(curArray(7))
								
								ImagePath = ""
								if cartItemMusicSkins = 1 then
									if fs.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsSmall") & "\" & cartItemImage) then
										ImagePath = "/productpics/musicSkins/musicSkinsSmall/" & cartItemImage
									elseif fs.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs") & "\" & cartItemMSDefaultImage) then
										ImagePath = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & cartItemMSDefaultImage
									else
										ImagePath = "/productpics/icon/imagena.jpg"
									end if
								else
									if fs.FileExists(Server.MapPath("/productpics/icon") & "\" & cartItemImage) then
										ImagePath = "/productpics/icon/" & cartItemImage
									else
										ImagePath = "/productpics/icon/imagena.jpg"
									end if
								end if
                                %>
                                <div class="lineItem" id="item<%=lineID%>">
                                    <div class="ffl">
                                        <div class="img"><img src="<%=ImagePath%>" width="60" height="60" /></div>
                                        <div class="itemDetail">
                                            <div class="ffl">
												<%=cartItemDesc%>
												<% if cartItemSR = 1 then %>
		                                            <a onclick="sr_$.learn('Cart Product');" href="javascript:void(0); return false;"><div name="sr_catalogProductGridDiv"></div></a>
	                                            <% end if %>
											</div>
                                            <div class="ffl" style="padding-top:10px;">
                                                <div class="price">
													<% if prepInt(cartItemPrice) = 0 then %>
                                                    FREE
                                                    <% else %>
                                                    <%=formatcurrency(prepInt(cartItemPrice))%>
                                                    <% end if %>                                                
												</div>
                                                <div class="fl" style="margin-left:20px;">
                                                    <div class="stockIcon"></div>
                                                    <div class="instock">In Stock!</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ffl" style="padding-top:10px;">
                                        <div class="fl" style="color:#333; font-weight:bold; font-size:15px; padding-top:4px;">Qty:</div>
                                        <div class="fl" style="padding-left:10px;">
                                            <input type="text" name="" value="1" class="qty" />
                                        </div>
                                        <div class="fl" style="padding-top:4px;">
                                            <div class="fl" style="padding-left:20px;"><a href="" class="lnk">Update</a></div>
                                            <div class="fl" style="padding-left:7px; color:#333;">|</div>
                                            <div class="fl" style="padding-left:7px;"><a href="" class="lnk">Delete</a></div>
                                        </div>
                                    </div>
                                </div>
                            <%
                            next
                            %>
                            </div>                            
							<div id="ordertotal">
								<div class="ffl" style="padding-top:10px;">
                                    <div class="fl" style="font-weight:bold; font-size:15px;">Subtotal:</div>
                                    <div class="fr" style="font-weight:bold; font-size:15px;"><%=formatCurrency(nItemTotal)%></div>
                                </div>
								<div class="ffl" style="padding-top:10px;">
                                    <div class="fl" style="font-weight:bold; font-size:15px;">Discount:</div>
                                    <div class="fr" style="font-weight:bold; font-size:15px;">
									<%
                                    if discountTotal > 0 then
                                        nProdIdCount = nProdIdCount + 1
                                        PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""0""><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""Promo Discount""><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""1""><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & discountTotal * -1 & """>" & vbcrlf
                                    %>
                                        <span id="discountAmt" style="color:#990000;"><%=formatCurrency(discountTotal * -1)%></span>
                                    <%else%>
                                        <span id="discountAmt"><%=formatCurrency(discountTotal * -1)%></span>
                                    <%end if%>
                                    </div>
                                </div>
								<div class="ffl" style="padding-top:10px;">
                                    <div class="fl" style="font-weight:bold; font-size:15px;">Tax:</div>
                                    <div class="fr" style="font-weight:bold; font-size:15px;"><span id="CAtax">$0.00</span></div>
                                    <div class="ffr" style="font-size:11px; padding-top:2px;"><span id="CAtaxMsg"></span></div>
                                </div>
								<% if prepStr(mySrToken) = "" or eligibleCnt = 0 then %>
								<div class="ffl" style="padding-top:10px;">
                                    <div class="fl" style="font-weight:bold; font-size:15px;">Shipping:</div>
                                    <div class="fr" style="font-weight:bold; font-size:15px;"><span id="shipcost"><%=formatCurrency(shipcost)%></span></div>
                                </div>
                                <% else %>
								<div class="ffl" style="padding-top:10px;">
                                    <div style="float:right; padding:5px 20px 0px 0px; " name="sr_cartSummaryDiv"></div>
                                </div>
                                <% end if %>
								<div class="ffl" style="padding-top:25px;">
                                    <div class="fl" style="font-weight:bold; font-size:20px;">Grand Total:</div>
                                    <div id="GrandTotal" class="fr" style="font-weight:bold; font-size:20px; color:#ff6600;"><%=formatCurrency(nAmount)%></div>
                                </div>
                            </div>
                        </div>
                        <div class="fl" style="margin-top:10px; width:240px; padding:10px; color:#fff; font-size:11px; background-color:#2ecc71;">
                        	Your items will arrive as early as <strong>Nov. 13, 2013</strong> based on the selected shipping method.
                        </div>
                    </div>
                </div>
            </div>
            <div class="ffl" style="margin-top:40px; padding-top:20px; border-top:1px solid #ccc;">
				<div class="ffl">
                    <div class="fl"><img src="/images/checkout/d/trust-google.png" border="0" /></div>                
                    <div class="fl" style="padding:10px 0px 0px 20px;"><img src="/images/checkout/d/trust-mcafee.png" border="0" /></div>                
                    <div class="fl" style="padding:0px 0px 0px 20px;"><img src="/images/checkout/d/trust-norton.png" border="0" /></div>
                    <div class="fr" align="right">
                    	<div class="clear" style="color:#333; font-size:16px; font-weight:bold;">Need Help?</div>
                    	<div class="clear" style="color:#333; padding-top:10px;">1 (800) 305-1106</div>
                    	<div class="clear" style="padding-top:5px;">
                        	<div class="fr"><a class="lnk" href="">Live Chat Available</a></div>
                        	<div class="fr" style="padding-right:4px;"><a href="" class="lnk"><img src="/images/checkout/d/live-chat-ico.png" border="0" /></a></div>
						</div>
                    </div>
                </div>
				<div class="ffl" style="padding-top:10px;">
                    <div class="fl"><a href="" class="lnk">Privacy Policy</a></div>
                    <div class="fl" style="padding-left:5px; color:#333;">|</div>
                    <div class="fl" style="padding-left:5px;"><a href="" class="lnk">Terms of Use</a></div>
                    <div class="fl" style="padding-left:5px; color:#333;">|</div>
                    <div class="fl" style="padding-left:5px;"><a href="" class="lnk">90-Day Return Policy</a></div>
                </div>
				<div class="ffl" style="padding-top:7px;">
                	&copy; 2002-<%=year(date)%> Wireless Emporium, Inc. All rights reserved.
                </div>
            </div>
            
            <div class="clear" style="height:50px;"></div>
        	<div class="ffl">
			<% 
			for each i in Request.Form
              Response.Write i & ": " & request.form(i) & "<br>"
            next
            %>
            </div>
        </td>
    </tr>
</table>
<!--WE variables-->
<%=WEhtml2%>
<!--End WE variables-->
<!--eBillme variables-->
<%=EBtemp%>
<!--End eBillme variables-->
<input type="hidden" name="myBasketXML" value="<%=EBxml%>">
<input type="hidden" name="ssl_ProdIdCount" value="<%=nProdIdCount%>">
<input type="hidden" name="mySrToken" value="<%=mySrToken%>">
<input type="hidden" name="chkOptMail" value="Y">
<div id="customerInfo" style="display:none;"></div>
</form>

<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.asp">
    <input type="hidden" name="numItems" value="<%=nProdIdCount%>">
    <input type="hidden" name="paymentAmount" value="<%=nSubTotal%>">
    <input type="hidden" name="ItemsAndWeight" value="<%=cStr(nProdIdCount) & "|" & cStr(sWeight)%>">
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <%=PPhtml%>
</form>

<div style="clear:both; height:15px;"></div>
<!--#include virtual="/includes/template/bottom_cart.asp"-->

<script language="javascript">
	function getCustomerData(email) {
		trackingVar('email',email);
		ajax('/ajax/customerData.asp?email=' + email,'customerInfo')
		setTimeout("displayData(document.getElementById('customerInfo').innerHTML)",2000)
		ajax('/ajax/remarketing.asp?email=' + email + '&mySession=<%=mySession%>&siteID=0','dumpZone')
	}
	function displayData(dataReturn) {
		if (dataReturn != "no data" && dataReturn != "") {
			trackingBit('accountReturnedByEmail');
			document.getElementById("popUpBG").style.display = ''
			document.getElementById("popUpBox").style.display = ''
			var dataArray = dataReturn.split("@@")
			if (document.frmProcessOrder.fname.value == "") { document.frmProcessOrder.fname.value = dataArray[0] }
			if (document.frmProcessOrder.lname.value == "") { document.frmProcessOrder.lname.value = dataArray[1] }
			if (document.frmProcessOrder.sAddress1.value == "") { document.frmProcessOrder.sAddress1.value = dataArray[2] }
			if (document.frmProcessOrder.sAddress2.value == "") { document.frmProcessOrder.sAddress2.value = dataArray[3] }
			if (document.frmProcessOrder.sCity.value == "") { document.frmProcessOrder.sCity.value = dataArray[4] }
			if (document.frmProcessOrder.sState.selectedIndex == 0) {
				for (i=0;i<50;i++) {
					if (document.frmProcessOrder.sState.options[i].value == dataArray[5]) {
						document.frmProcessOrder.sState.selectedIndex = i
						changeTax(dataArray[5])
					}
				}
			}
			if (document.frmProcessOrder.sZip.value == "") {
				document.frmProcessOrder.sZip.value = dataArray[6]
				getNewShipping();
			}
			if (document.frmProcessOrder.phoneNumber.value == "") { document.frmProcessOrder.phoneNumber.value = dataArray[7] }
			
			if (dataArray[2] != dataArray[8] || dataArray[6] != dataArray[12]) {
				document.frmProcessOrder.sameaddress.checked = true
				showBillingInfo()
				document.frmProcessOrder.bAddress1.value = dataArray[8]
				document.frmProcessOrder.bAddress2.value = dataArray[9]
				document.frmProcessOrder.bCity.value = dataArray[10]
				for (i=0;i<50;i++) {
					if (document.frmProcessOrder.bState.options[i].value == dataArray[11]) {
						document.frmProcessOrder.bState.selectedIndex = i
					}
				}
				document.frmProcessOrder.bZip.value = dataArray[12]
			}
			document.frmProcessOrder.accountid.value = dataArray[13]
			document.frmProcessOrder.parentAcctID.value = dataArray[14]
		}
	}
	
	function newAcctInfo() {
		document.frmProcessOrder.accountid.value = ''
	}
	function newAcctInfoTrack(y) {
		document.frmProcessOrder.accountid.value = ''
		//alert('{object: ' + y.name + ',' + y.value + '}');
		if (y.name == 'phoneNumber') {
			trackingVar('phone',y.value);
		}
	}
</script>
<script language="JavaScript">
	function popUp(url, winName, h, w, l, t) {
		var args = 'height=' + h + ',width=' + w + ',left=' + l + ',top=' + t + ',toolbar=1,location=1,directories=0,status=1,menubar=1,scrollbars=1,resizable=1';
		window.open(url, winName, args);
	}
</script>