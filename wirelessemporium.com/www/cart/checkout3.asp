<%
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

dim useHttps : useHttps = 1
dim pageName : pageName = "checkout"
dim cart : cart = 1
dim scrollCart : scrollCart = 1
dim varientTopBanner : varientTopBanner = 2
dim varientTopNav : varientTopNav = 1
dim varientAssurance : varientAssurance = 0
dim mySrToken : mySrToken = request.Cookies("srLogin")
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<!--#include virtual="/includes/template/top_cart.asp"-->
<%
	sql = "exec sp_getShoppingCart @siteID = 0, @sessionID = " & mySession
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	if rs.EOF then response.Redirect("/sessionexpired")
	
	sql = "select top 3 * from we_testimonials where siteid = 0 order by newid()"
	session("errorSQL") = sql
	set testimonialRS = oConn.execute(sql)
%>
<link rel="stylesheet" type="text/css" href="/cart/includes/checkout2.css?curLoad=<%=time%>" />
<div class="tb checkoutTitleBar">
	<div class="fl checkoutTitleText">Secure Checkout</div>
    <div class="fl checkoutTitleLock"><img src="/images/checkout/icons/lock.gif" border="0" /></div>
    <div class="fl checkoutTitleInactiveBubble">1</div>
    <div class="fl checkoutTitleInactiveText">Cart Summary</div>
    <div class="fl checkoutTitleActiveBubble">2</div>
    <div class="fl checkoutTitleActiveText">Secure Checkout</div>
    <div class="fl checkoutTitleInactiveBubble">3</div>
    <div class="fl checkoutTitleInactiveText2">Receipt</div>
</div>
<div id="allFormData" class="tb checkoutContentMain">
	<div id="ccFormBlocker" class="ccBlocker"></div>
	<div id="leftContentArea" class="fl checkoutContentLeft">
    	<form name="orderForm" method="post" action="" onsubmit="return verifyFormData()">
        <div class="tb checkoutEntryBox">
        	<div class="tb checkoutEntryTitle">
            	<div class="fl checkoutEntryTitleText">Checkout Method:</div>
                <div class="fr checkoutEntryRequired">* Required</div>
            </div>
            <div class="tb checkoutEntryContent">
            	<div class="tb checkoutEntryLine">
	            	<div class="fl checkoutEntryText">*Email Address:</div>
    	            <div class="fl fieldBox">
                    	<input type="text" name="email" id="email" value="" class="checkoutEntryFormField" onchange="getCustomerData(this.value)" />
                        <div id="emailError" class="fieldBoxError"></div>
                    </div>
                </div>
                <div class="tb checkoutEntryNextLine">
                	<div class="fl payWithCC" onclick="openFields()"><img src="/images/checkout/buttons/creditCard.gif" border="0" /></div>
                    <div class="fr payWithPP" onclick="subPayPal()"><img src="/images/checkout/buttons/payPal.gif" border="0" /></div>
                </div>
            </div>
        </div>
        <div class="tb checkoutEntryBoxNext">
        	<div class="tb checkoutEntryTitle">
            	<div class="fl checkoutEntryTitleText">Shipping Information:</div>
                <div class="fr checkoutEntryRequired">* Required</div>
            </div>
            <div class="tb checkoutEntryContent">
                <div class="tb checkoutEntryLine">
	            	<div class="fl checkoutEntryText">*First Name:</div>
    	            <div class="fl fieldBox">
                    	<input type="text" name="fname" id="fname" value="First Name" class="checkoutEntryFormField bogusEntry" onfocus="clearField(this.name,'f','First Name')" onblur="clearField(this.name,'b','First Name')" />
                        <div id="fnameError" class="fieldBoxError"></div>
                    </div>
                </div>
                <div class="tb checkoutEntryNextLine">
                	<div class="fl checkoutEntryText">*Last Name:</div>
    	            <div class="fl fieldBox">
                    	<input type="text" name="lname" id="lname" value="Last Name" class="checkoutEntryFormField bogusEntry" onfocus="clearField(this.name,'f','Last Name')" onblur="clearField(this.name,'b','Last Name')" />
                        <div id="lnameError" class="fieldBoxError"></div>
                    </div>
                </div>
                <div class="tb checkoutEntryNextLine">
                	<div class="fl checkoutEntryText">*Street Address:</div>
    	            <div class="fl fieldBox">
                    	<input type="text" name="sAddress1" id="sAddress1" value="123 Aloha Ln. Apt. #1" class="checkoutEntryFormField bogusEntry" onfocus="clearField(this.name,'f','123 Aloha Ln. Apt. #1')" onblur="clearField(this.name,'b','123 Aloha Ln. Apt. #1')" />
                        <div id="sAddress1Error" class="fieldBoxError"></div>
                    </div>
                </div>
                <div class="tb checkoutEntryNextLine">
                	<div class="fl checkoutEntryText">*Zip Code:</div>
    	            <div class="fl fieldBox">
                    	<input type="text" name="sZip" id="sZip" maxlength="6" value="12345" class="checkoutEntryFormField bogusEntry" onchange="getNewShipping(); getCityState(this.value,'');" onfocus="clearField(this.name,'f','12345')" onblur="clearField(this.name,'b','12345')" />
                    	<div id="sZipError" class="fieldBoxError"></div>
                    </div>
                </div>
                <div id="shipCity" class="tb checkoutEntryNextLine" style="display:none;">
                	<div class="fl checkoutEntryText">*City:</div>
    	            <div class="fl fieldBox">
                    	<input type="text" name="sCity" id="sCity" value="" class="checkoutEntryFormField" />
                        <div id="sCityError" class="fieldBoxError"></div>
                    </div>
                </div>
                <div id="shipState" class="tb checkoutEntryNextLine" style="display:none;">
                	<div class="fl checkoutEntryText">*State:</div>
    	            <div class="fl fieldBox">
                    	<select name="sState" id="sState" class="checkoutEntryFormSelect" onChange="checkTax(this.value);">
							<%getStates(sstate)%>
                        </select>
                        <div id="sStateError" class="fieldBoxError"></div>
                    </div>
                </div>
                <div class="tb checkoutEntryNextLine">
                	<div class="fl checkoutEntryText">*Phone Number:</div>
    	            <div class="fl fieldBox">
                    	<input type="text" name="phoneNumber" id="phoneNumber" maxlength="14" value="1234567890" class="checkoutEntryFormField bogusEntry" onfocus="clearField(this.name,'f','1234567890')" onblur="clearField(this.name,'b','1234567890')" />
                        <div id="phoneNumberError" class="fieldBoxError"></div>
                    </div>
                </div>
                <div class="tb checkoutEntryNextLine billingOption">
                	<input type="checkbox" name="sameaddress" onclick="activateBilling(this.checked)" /> Check if Billing Address is different than the Shipping Address.
                </div>
            </div>
        </div>
        <div id="billingInfo" class="tb checkoutEntryBoxNext" style="display:none;">
        	<div class="tb checkoutEntryTitle">
            	<div class="fl checkoutEntryTitleText">Billing Information:</div>
                <div class="fr checkoutEntryRequired">* Required</div>
            </div>
            <div class="tb checkoutEntryContent">
                <div class="tb">
                    <div class="tb checkoutEntryLine">
                        <div class="fl checkoutEntryText">*First Name:</div>
                        <div class="fl fieldBox">
                        	<input type="text" name="bFname" id="bFname" value="First Name" class="checkoutEntryFormField bogusEntry" onfocus="clearField(this.name,'f','First Name')" onblur="clearField(this.name,'b','First Name')" />
                            <div id="bFnameError" class="fieldBoxError"></div>
                        </div>
                    </div>
                    <div class="tb checkoutEntryNextLine">
                        <div class="fl checkoutEntryText">*Last Name:</div>
                        <div class="fl fieldBox">
                        	<input type="text" name="bLname" id="bLname" value="Last Name" class="checkoutEntryFormField bogusEntry" onfocus="clearField(this.name,'f','Last Name')" onblur="clearField(this.name,'b','Last Name')" />
                            <div id="bLnameError" class="fieldBoxError"></div>
                        </div>
                    </div>
                    <div class="tb checkoutEntryNextLine">
                        <div class="fl checkoutEntryText">*Street Address:</div>
                        <div class="fl fieldBox">
                        	<input type="text" name="bAddress1" id="bAddress1" value="321 Aloha Ln. Unit B" class="checkoutEntryFormField bogusEntry" onfocus="clearField(this.name,'f','321 Aloha Ln. Unit B')" onblur="clearField(this.name,'b','321 Aloha Ln. Unit B')" />
                            <div id="bAddress1Error" class="fieldBoxError"></div>
                        </div>
                    </div>
                    <div class="tb checkoutEntryNextLine">
                        <div class="fl checkoutEntryText">*Zip Code:</div>
                        <div class="fl fieldBox">
                        	<input type="text" name="bZip" id="bZip" maxlength="6" value="12346" class="checkoutEntryFormField bogusEntry" onchange="getCityState(this.value,'bill');" onfocus="clearField(this.name,'f','12346')" onblur="clearField(this.name,'b','12346')" />
                            <div id="bZipError" class="fieldBoxError"></div>
                        </div>
                    </div>
                    <div id="billCity" class="tb checkoutEntryNextLine" style="display:none;">
                        <div class="fl checkoutEntryText">*City:</div>
                        <div class="fl fieldBox">
                            <input type="text" name="bCity" id="bCity" value="" class="checkoutEntryFormField" />
                            <div id="bCityError" class="fieldBoxError"></div>
                        </div>
                    </div>
                    <div id="billState" class="tb checkoutEntryNextLine" style="display:none;">
                        <div class="fl checkoutEntryText">*State:</div>
                        <div class="fl fieldBox">
                            <select name="bState" id="bState" class="checkoutEntryFormSelect">
                                <%getStates(sstate)%>
                            </select>
                            <div id="bStateError" class="fieldBoxError"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tb checkoutEntryBoxNext">
        	<div class="tb checkoutEntryTitle">
            	<div class="fl checkoutEntryTitleText">Shipping Method:</div>
                <div class="fr checkoutEntryRequired">* Required</div>
            </div>
            <div id="shippingOptions" class="tb checkoutEntryContent">
                <div id="ship0Box" class="tb checkoutEntryLine activeShip">
	            	<div class="fl shipRadio">
                    	<input type="radio" name="shiptype" value="0" checked="checked" onclick="setActiveShip(this.value)" />
                        <input type="hidden" name="shipcost0" value="0.00" />
                    </div>
                    <div class="fl shipTitle"><strong>FREE:</strong> USPS First Class</div>
                    <div class="fl shipTime">4-10 Business Days</div>
                    <div class="fl shipAmt">$0.00</div>
                </div>
                <div id="ship2Box" class="tb checkoutEntryNextLine inactiveShip">
	            	<div class="fl shipRadio">
                    	<input type="radio" name="shiptype" value="2" onclick="setActiveShip(this.value)" />
                        <input type="hidden" name="shipcost2" value="6.99" />
                    </div>
                    <div class="fl shipTitle">USPS Priority Mail</div>
                    <div class="fl shipTime">2-4 Business Days</div>
                    <div class="fl shipAmt">$6.99</div>
                </div>
                <div id="ship3Box" class="tb checkoutEntryNextLine inactiveShip">
	            	<div class="fl shipRadio">
                    	<input type="radio" name="shiptype" value="3" onclick="setActiveShip(this.value)" />
                        <input type="hidden" name="shipcost3" value="19.99" />
                    </div>
                    <div class="fl shipTitle">USPS Express Mail</div>
                    <div class="fl shipTime">1-2 Business Days</div>
                    <div class="fl shipAmt">$19.99</div>
                </div>
            </div>
        </div>
        <div class="tb checkoutEntryBoxNext">
        	<div class="tb checkoutEntryTitle">
            	<div class="fl checkoutEntryTitleText">Payment Information:</div>
                <div class="fr checkoutEntryRequired">* Required</div>
            </div>
            <div class="tb checkoutEntryContent">
                <div class="tb checkoutEntryLine">
	            	<div class="fl checkoutEntryText">*Name On Card:</div>
    	            <div class="fl fieldBox">
                    	<input type="text" name="cc_cardOwner" id="cc_cardOwner" value="First & Last Name" class="checkoutEntryFormField bogusEntry" onfocus="clearField(this.name,'f','First & Last Name')" onblur="clearField(this.name,'b','First/Last Name')" />
                        <div id="cc_cardOwnerError" class="fieldBoxError"></div>
                    </div>
                </div>
                <div class="tb checkoutEntryNextLine">
                	<div class="fl checkoutEntryText">*Card Number:</div>
    	            <div class="fl fieldBox">
                    	<input type="text" name="cardNum" id="cardNum" maxlength="16" value="1234123412341234" class="checkoutEntryFormField bogusEntry" onfocus="clearField(this.name,'f','1234123412341234')" onblur="clearField(this.name,'b','1234123412341234')" />
                        <div id="cardNumError" class="fieldBoxError"></div>
                    </div>
                </div>
                <div class="tb checkoutEntryNextLine ccImg"><img src="/images/checkout/icons/creditCards.gif" border="0" /></div>
                <div class="tb checkoutEntryNextLine">
                	<div class="fl checkoutEntryText">*Expiration Date:</div>
    	            <div class="fl paymentCcMonth fieldBox">
                    	<select id="cc_month" name="cc_month" class="checkoutEntryFormSelect bogusEntry" onchange="optionSelected(this.name)">
                        	<option value="">Month</option>
                            <option value="1">01 - January</option>
                            <option value="2">02 - February</option>
                            <option value="3">03 - March</option>
                            <option value="4">04 - April</option>
                            <option value="5">05 - May</option>
                            <option value="6">06 - June</option>
                            <option value="7">07 - July</option>
                            <option value="8">08 - August</option>
                            <option value="9">09 - September</option>
                            <option value="10">10 - October</option>
                            <option value="11">11 - November</option>
                            <option value="12">12 - December</option>
                        </select>
                        <div id="cc_monthError" class="fieldBoxError"></div>
                    </div>
                    <div class="fl fieldBox">
                    	<select id="cc_year" name="cc_year" class="checkoutEntryFormSelect bogusEntry" onchange="optionSelected(this.name)">
                        	<option value="">Year</option>
                            <%
							for countYear = 0 to 14
								response.write "<option value=""" & right(cStr(year(date) + countYear),2) & """>" & cStr(year(date) + countYear) & "</option>" & vbcrlf
							next
							%>
                        </select>
                        <div id="cc_yearError" class="fieldBoxError"></div>
                    </div>
                </div>
                <div class="tb checkoutEntryNextLine">
                	<div class="fl checkoutEntryText">*Security Code:</div>
    	            <div class="fl fieldBox">
                    	<input type="text" name="secCode" id="secCode" value="000" maxlength="4" class="checkoutEntryFormFieldSml bogusEntry" onfocus="clearField(this.name,'f','000')" onblur="clearField(this.name,'b','000')" />
                        <div id="secCodeError" class="fieldBoxError"></div>
                    </div>
                    <div class="fl secCodeLink">
                    	<a class="secCodeLearn" onclick="learnMore()">What's This?</a>
                    	<div id="secCodeGraphicBox" class="secCodeGraphic" style="display:none;" onmouseout="learnMore()" onclick="learnMore()"><img src="/images/checkout/icons/secCode.png" border="0" /></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tb secure128">
        	<div class="tb mCenter 128Inner">
	            <div class="fl grayLock"><img src="/images/checkout/icons/grayLock.gif" border="0" /></div>
    	        <div class="fl secure128Text">This is a secure 128-bit SSL encrypted payment.</div>
            </div>
        </div>
        <div class="fullTestimonial tb mCenter">
        	<div class="fl testimonialButton curs" onclick="testimonalChg(-1)"><img src="/images/checkout/buttons/prevTest.gif" border="0" /></div>
            <div class="fl">
				<%
                testimonialNum = 1
                do while not testimonialRS.EOF
                    testimonialText = testimonialRS("quote")
                    testimonialName = testimonialRS("attName")
                    testimonialLoc = testimonialRS("attCity") & ", " & testimonialRS("attState")
                %>
                <div id="testimonialNum_<%=testimonialNum%>" class="tb testimonialBox" <% if testimonialNum > 1 then %> style="display:none;"<% end if %>>
                    <div class="tb testText">&quot;<%=testimonialText%>&quot;</div>
                    <div class="tb testName"><%=testimonialName%></div>
                    <div class="tb testLocation"><%=testimonialLoc%></div>
                </div>
                <%
                    testimonialRS.movenext
                    testimonialNum = testimonialNum + 1
                loop
                %>
            </div>
            <div class="fl testimonialButton curs" onclick="testimonalChg(1)"><img src="/images/checkout/buttons/nextTest.gif" border="0" /></div>
        </div>
        <div id="additionalHiddenFields" style="display:none;"></div>
        <input type="submit" name="myAction" value="place order" style="display:none;" />
        <input type="hidden" name="subTotal" value="<%=subTotal%>" />
        <input type="hidden" name="nCATax" value="0.00" />
        <input type="hidden" name="shippingTotal" value="0.00" />
        <input type="hidden" name="shiptypeID" value="0">
        <input type="hidden" name="discountTotal" value="0.00" />
        <input type="hidden" name="grandTotal" value="<%=subTotal%>" />
        <input type="hidden" name="taxValue" value="<%=Application("taxMath")%>" />
        <input type="hidden" name="sWeight" value="<%=totalWeight%>" />
	    <input type="hidden" name="itemCnt" value="<%=itemCnt%>" />
        <input type="hidden" name="sPromoCode" value="<%=sPromoCode%>">
        <input type="hidden" name="cc_cardType" value="VISA" />
        <input type="hidden" name="submitted" value="submitted">
        <input type="hidden" name="PaymentType" value="cc">
        <input type="hidden" name="mySession" value="<%=mySession%>" />
        <input type="hidden" name="accountid" value="">
        <input type="hidden" name="parentAcctID" value="">
        <input type="hidden" name="ssl_ProdIdCount" value="<%=itemCnt%>">
		<input type="hidden" name="mySrToken" value="<%=mySrToken%>">
        </form>
    </div>
    <div class="fl checkoutContentRight">
    	<div id="floatingRightPane" class="tb floatingRightDetails">
            <div class="tb orderSummaryBox">
                <div class="tb orderSummaryTitle">
                    <div class="fl checkoutEntryTitleText">Contact Us:</div>
                </div>
                <div class="tb orderSummaryContent">
                	<div class="tb contactText">
                    	Have any questions or concerns regarding your order? We're here to help! 
                        Simply contact us through one of the methods below:
                    </div>
                    <div class="tb contactOption curs" onclick="OpenLHNChat()">
                    	<div class="fl"><img src="/images/checkout/icons/contactLiveChat.gif" border="0" /></div>
                        <div class="fl contactLinkText">Live Chat</div>
                    </div>
                    <div class="tb contactOption">
                    	<div class="fl"><img src="/images/checkout/icons/contactPhone.gif" border="0" /></div>
                        <div class="fl contactLinkText">1 (800) 305-1106</div>
                    </div>
                    <div class="tb contactOption curs" onclick="window.location='mailto:service@wirelessemporium.com'">
                    	<div class="fl"><img src="/images/checkout/icons/contactEmail.gif" border="0" /></div>
                        <div class="fl contactLinkText">Email Us 24/7</div>
                    </div>
                </div>
            </div>
            <div class="tb orderSummaryBoxNext">
                <div class="tb orderSummaryTitle">
                    <div class="fl checkoutEntryTitleText">Site Secured &amp; Trusted By:</div>
                </div>
                <div class="tb orderSummaryContent">
                	<div class="tb mCenter"><img src="/images/checkout/icons/trustList.jpg" border="0" /></div>
                </div>
            </div>
            <div class="tb orderSummaryBoxNext">
                <div class="tb orderSummaryTitle">
                    <div class="fl checkoutEntryTitleText">Order Summary:</div>
                    <div class="fr checkoutEntryRequired"><%=date%></div>
                </div>
                <div class="tb orderSummaryContent">
                	<%
					payPalItemID = 0
					itemCnt = 0
					subTotal = 0
					totalWeight = 0
					do while not rs.EOF
						qty = rs("qty")
						lockQty = rs("lockQty")
						musicSkins = rs("musicSkins")
						itemID = rs("itemID")
						itemDesc = rs("itemDesc")
						itemPic = rs("itemPic")
						price = rs("priceEffective")
						retail = rs("price_Retail")
						weight = rs("itemWeight")
						
						itemCnt = itemCnt + qty
						subTotal = subTotal + (qty * price)
						totalWeight = totalWeight + (qty * weight)
					%>
                    <div class="tb itemRow">
                        <div class="fl orderSummaryItemPic"><img src="/productpics/thumb/<%=itemPic%>" border="0" /></div>
                        <div class="fl orderSummaryItemDesc">
                            <div class="tb orderSummaryItemName"><%=itemDesc%></div>
                            <div class="tb orderSummaryItemPrice">
                                <div class="fl priceTitle">Retail Price:</div>
                                <div class="fl priceAmt"><%=formatCurrency(retail,2)%></div>
                            </div>
                            <% if qty > 1 then %>
                            <div class="tb orderSummaryItemPrice">
                                <div class="fl priceTitleBold">Our Price:</div>
                                <div class="fl priceAmtBoldMulti"><%=formatCurrency(price,2)%></div>
                            </div>
                            <div class="tb orderSummaryItemPrice">
                                <div class="fl priceTitleBold">Total Price:</div>
                                <div class="fl priceAmtBold"><%=formatCurrency(price*qty,2)%></div>
                            </div>
                            <% else %>
                            <div class="tb orderSummaryItemPrice">
                                <div class="fl priceTitleBold">Our Price:</div>
                                <div class="fl priceAmtBold"><%=formatCurrency(price,2)%></div>
                            </div>
                            <div class="tb orderSummaryItemPrice"></div>
                            <% end if %>
                        </div>
                        <div class="tb orderSummaryStock">
                            <div class="fl"><img src="/images/checkout/icons/inStockCheckMark.gif" border="0" /></div>
                            <div class="fl orderSummaryStockLine"><strong>In Stock:</strong> Leaves our warehouse within 24 hours.</div>
                        </div>
                        <div class="tb orderSummaryQtyLine">
                        	<form name="itemForm_<%=itemID%>" method="post" action="/cart/item_edit?chkOut=2">
                            <div class="fl qtyTitle">Qty:</div>
                            <% if lockQty then %>
                            <div class="fl"><input type="text" name="qty" value="<%=qty%>" class="qtyField" disabled="disabled" /></div>
                            <% else %>
                            <div class="fl"><input type="text" name="qty" value="<%=qty%>" class="qtyField" /></div>
                            <div class="fl osUpdateQty" onclick="updateItem(<%=itemID%>)">Update</div>
                            <div class="fl osSplitQty">|</div>
                            <div class="fl osRemoveQty" onclick="removeItem(<%=itemID%>)">Remove</div>
                            <% end if %>
                            <input type="hidden" name="prodid" value="<%=itemID%>" />
                            </form>
                        </div>
                    </div>
                    <%
						PPhtml = 	PPhtml & "<input type=""hidden"" name=""L_NUMBER" & payPalItemID & """ value=""" & itemID & """>" &_
									"<input type=""hidden"" name=""L_NAME" & payPalItemID & """ value=""" & itemDesc & """>" &_
									"<input type=""hidden"" name=""L_QTY" & payPalItemID & """ value=""" & qty & """>" &_
									"<input type=""hidden"" name=""L_AMT" & payPalItemID & """ value=""" & price & """>"
						payPalItemID = payPalItemID + 1
						ItemList = 	ItemList & "<input type=""hidden"" name=""itemID_" & payPalItemID & """ value=""" & itemID & """>" &_
									"<input type=""hidden"" name=""itemQty_" & payPalItemID & """ value=""" & qty & """>" &_
									"<input type=""hidden"" name=""ssl_item_number_" & payPalItemID & """ value=""" & itemID & """>" &_
									"<input type=""hidden"" name=""ssl_item_qty_" & payPalItemID & """ value=""" & qty & """>" &_
									"<input type=""hidden"" name=""ssl_item_price_" & payPalItemID & """ value=""" & price & """>" &_
									"<input type=""hidden"" name=""ssl_musicSkins_" & payPalItemID & """ value=""" & musicSkins & """>"
						rs.movenext
					loop
					if itemCnt = 1 then itemCntTxt = "1 item" else itemCntTxt = itemCnt & " items"
					%>
                </div>
                <div class="tb orderTotals">
                    <div class="tb orderTotalRow">
                        <div class="fl orderTotalTitle">Subtotal (<%=itemCntTxt%>):</div>
                        <div id="subTotalDiv" class="fl orderTotalAmt"><%=formatCurrency(subTotal,2)%></div>
                    </div>
                    <div class="tb orderTotalRow" style="display:none;">
                        <div class="fl orderTotalTitle">Discount:</div>
                        <div id="discountDiv" class="fl orderTotalAmtRed">($8.99)</div>
                    </div>
                    <div class="tb orderTotalRow">
                        <div class="fl orderTotalTitle">Tax:</div>
                        <div id="taxDiv" class="fl orderTotalAmt">$0.00</div>
                    </div>
                    <div class="tb orderTotalRow">
                        <div class="fl orderTotalTitle">Shipping:</div>
                        <div id="shippingDiv" class="fl orderTotalAmt">$0.00</div>
                    </div>
                </div>
                <div class="tb grandTotalRow">
                    <div class="fl grandTotalTitle">Grand Total:</div>
                    <div id="grandTotalDiv" class="fl grandTotalAmt"><%=formatCurrency(subTotal,2)%></div>
                </div>
            </div>
            <div class="tb greenShipBox">
                Your items are <strong>In Stock</strong> and will arrive as early as<br />
                <strong id="curArrivalDate"><%=date+4%></strong> based on the selected shipping method.
            </div>
            <div id="checkValue"></div>
        </div>
    </div>
</div>
<div id="submitBottom" class="tb bottomPlaceOrder"><input type="image" src="/images/checkout/buttons/placeOrder.gif" border="0" width="269" height="52" onclick="verifyFormData();" /></div>
<div class="tb bottomTrustBox">
	<div class="fl"><a href="https://www.google.com/trustedstores/verify?gl=US&hl=en-US&id=230549&sid=230550" target="_blank"><img src="/images/checkout/icons/googleTrustedStore.gif" border="0" /></a></div>
    <div class="fl"><a href="https://www.mcafeesecure.com/RatingVerify?ref=www.wirelessemporium.com" target="_blank"><img src="/images/checkout/icons/mcAfeeSecure.gif" border="0" /></a></div>
    <div class="fl"><a href="https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.WIRELESSEMPORIUM.COM&lang=en" target="_blank"><img src="/images/checkout/icons/nortonSecured.gif" border="0" /></a></div>
    <div class="fr">
    	<div class="tb btb_needHelp">Need Help?</div>
        <div class="tb btb_number">1 (800) 305-1106</div>
        <div class="tb btb_liveChat" onclick="OpenLHNChat()">
        	<div class="fl"><img src="/images/checkout/icons/liveChat.gif" border="0" /></div>
            <div class="fl liveChatLink">Live Chat Available</div>
        </div>
    </div>
    <div class="tb bottomLinks">
    	<div class="fl"><a class="secCodeLearn" onclick="window.open('/privacy');">Privacy Policy</a></div>
        <div class="fl linkBreak">|</div>
        <div class="fl"><a class="secCodeLearn" onclick="window.open('/termsofuse');">Terms of Use</a></div>
        <div class="fl linkBreak">|</div>
        <div class="fl"><a class="secCodeLearn" onclick="window.open('/store');">90-Day Return Policy</a></div>
    </div>
    <div class="tb copywriteLine">&copy;2002-<%=year(date)%> Wireless Emporium, Inc. All rights reserved.</div>
</div>
<div id="dumpZone" style="display:none;"></div>
<div id="cityStateDump" style="display:none;"></div>
<div id="customerInfo" style="display:none;"></div>
<div id="submitTop" style="display:none;"></div>
<div id="sourceHidenFields" style="display:none;"><%=ItemList%></div>
<form name="totalDetails">
	<input type="hidden" name="totalWeight" value="<%=totalWeight%>" />
    <input type="hidden" name="itemCnt" value="<%=itemCnt%>" />
    <input type="hidden" name="subTotal" value="<%=subTotal%>" />
    <input type="hidden" name="grandTotal" value="<%=subTotal%>" />
    <input type="hidden" name="arrivalDate1" value="<%=date+1%>" />
    <input type="hidden" name="arrivalDate2" value="<%=date+2%>" />
    <input type="hidden" name="arrivalDate3" value="<%=date+3%>" />
    <input type="hidden" name="arrivalDate4" value="<%=date+4%>" />
</form>
<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder">
    <input type="hidden" name="numItems" value="<%=itemCnt%>">
    <input type="hidden" name="paymentAmount" value="<%=subTotal%>">
    <input type="hidden" name="ItemsAndWeight" value="<%=itemCnt%>|<%=totalWeight%>">
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <%=PPhtml%>
</form>
<script language="javascript" src="/framework/Utility/base.js"></script>
<script language="javascript" src="/cart/includes/checkout2.js?curLoad=<%=time%>"></script>
</body>
</html>