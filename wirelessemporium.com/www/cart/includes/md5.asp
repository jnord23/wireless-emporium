<%
' Copyright Webgains Ltd 2005
' Contact webmaster@webgains.com
const BYTE_BITS = 8

on error resume next
' otherwise ASP will try to define these a second time

dim md5Inited
dim m_lOnBits()
dim m_l2Power()

on error goto 0

function md5(s)
	const WORD_BYTES = 4
	const WORD_BITS = 32
	const MOD_BITS = 512
	const CONG_BITS = 448
	const S11 = 7
    const S12 = 12
    const S13 = 17
    const S14 = 22
    const S21 = 5
    const S22 = 9
    const S23 = 14
    const S24 = 20
    const S31 = 4
    const S32 = 11
    const S33 = 16
    const S34 = 23
    const S41 = 6
    const S42 = 10
    const S43 = 15
    const S44 = 21
	
	dim sLen, numWords, bytePos, byteCount, wordCount
	dim a, b, c, d, k, AA, BB, CC, DD
	dim arrWords()
	
	if not md5Inited then
		md5Init
		md5Inited = true
	end if
	
	sLen = Len(s)
    numWords = (((sLen + ((MOD_BITS - CONG_BITS) \ BYTE_BITS)) _
    	\ (MOD_BITS \ BYTE_BITS)) + 1) * (MOD_BITS \ WORD_BITS)
    redim arrWords(numWords - 1)

    bytePos = 0
    byteCount = 0
    do until byteCount >= sLen
        wordCount = byteCount \ WORD_BYTES
        bytePos = (byteCount mod WORD_BYTES) * BYTE_BITS
        arrWords(wordCount) = arrWords(wordCount) _
        	or md5LShift(Asc(Mid(s, byteCount + 1, 1)), bytePos)
        byteCount = byteCount + 1
    loop
    
    wordCount = byteCount \ WORD_BYTES
    bytePos = (byteCount mod WORD_BYTES) * BYTE_BITS

    arrWords(wordCount) = arrWords(wordCount) or md5LShift(&H80, bytePos)

    arrWords(numWords - 2) = md5LShift(sLen, 3)
    arrWords(numWords - 1) = md5RShift(sLen, 29)
    
    a = &H67452301
    b = &HEFCDAB89
    c = &H98BADCFE
    d = &H10325476
    
    for k = 0 to UBound(arrWords) step 16
        AA = a
        BB = b
        CC = c
        DD = d
    
        md5Op a, b, c, d, arrWords(k + 0), S11, &HD76AA478, 1
        md5Op d, a, b, c, arrWords(k + 1), S12, &HE8C7B756, 1
        md5Op c, d, a, b, arrWords(k + 2), S13, &H242070DB, 1
        md5Op b, c, d, a, arrWords(k + 3), S14, &HC1BDCEEE, 1
        md5Op a, b, c, d, arrWords(k + 4), S11, &HF57C0FAF, 1
        md5Op d, a, b, c, arrWords(k + 5), S12, &H4787C62A, 1
        md5Op c, d, a, b, arrWords(k + 6), S13, &HA8304613, 1
        md5Op b, c, d, a, arrWords(k + 7), S14, &HFD469501, 1
        md5Op a, b, c, d, arrWords(k + 8), S11, &H698098D8, 1
        md5Op d, a, b, c, arrWords(k + 9), S12, &H8B44F7AF, 1
        md5Op c, d, a, b, arrWords(k + 10), S13, &HFFFF5BB1, 1
        md5Op b, c, d, a, arrWords(k + 11), S14, &H895CD7BE, 1
        md5Op a, b, c, d, arrWords(k + 12), S11, &H6B901122, 1
        md5Op d, a, b, c, arrWords(k + 13), S12, &HFD987193, 1
        md5Op c, d, a, b, arrWords(k + 14), S13, &HA679438E, 1
        md5Op b, c, d, a, arrWords(k + 15), S14, &H49B40821, 1
    
        md5Op a, b, c, d, arrWords(k + 1), S21, &HF61E2562, 2
        md5Op d, a, b, c, arrWords(k + 6), S22, &HC040B340, 2
        md5Op c, d, a, b, arrWords(k + 11), S23, &H265E5A51, 2
        md5Op b, c, d, a, arrWords(k + 0), S24, &HE9B6C7AA, 2
        md5Op a, b, c, d, arrWords(k + 5), S21, &HD62F105D, 2
        md5Op d, a, b, c, arrWords(k + 10), S22, &H2441453, 2
        md5Op c, d, a, b, arrWords(k + 15), S23, &HD8A1E681, 2
        md5Op b, c, d, a, arrWords(k + 4), S24, &HE7D3FBC8, 2
        md5Op a, b, c, d, arrWords(k + 9), S21, &H21E1CDE6, 2
        md5Op d, a, b, c, arrWords(k + 14), S22, &HC33707D6, 2
        md5Op c, d, a, b, arrWords(k + 3), S23, &HF4D50D87, 2
        md5Op b, c, d, a, arrWords(k + 8), S24, &H455A14ED, 2
        md5Op a, b, c, d, arrWords(k + 13), S21, &HA9E3E905, 2
        md5Op d, a, b, c, arrWords(k + 2), S22, &HFCEFA3F8, 2
        md5Op c, d, a, b, arrWords(k + 7), S23, &H676F02D9, 2
        md5Op b, c, d, a, arrWords(k + 12), S24, &H8D2A4C8A, 2
            
        md5Op a, b, c, d, arrWords(k + 5), S31, &HFFFA3942, 3
        md5Op d, a, b, c, arrWords(k + 8), S32, &H8771F681, 3
        md5Op c, d, a, b, arrWords(k + 11), S33, &H6D9D6122, 3
        md5Op b, c, d, a, arrWords(k + 14), S34, &HFDE5380C, 3
        md5Op a, b, c, d, arrWords(k + 1), S31, &HA4BEEA44, 3
        md5Op d, a, b, c, arrWords(k + 4), S32, &H4BDECFA9, 3
        md5Op c, d, a, b, arrWords(k + 7), S33, &HF6BB4B60, 3
        md5Op b, c, d, a, arrWords(k + 10), S34, &HBEBFBC70, 3
        md5Op a, b, c, d, arrWords(k + 13), S31, &H289B7EC6, 3
        md5Op d, a, b, c, arrWords(k + 0), S32, &HEAA127FA, 3
        md5Op c, d, a, b, arrWords(k + 3), S33, &HD4EF3085, 3
        md5Op b, c, d, a, arrWords(k + 6), S34, &H4881D05, 3
        md5Op a, b, c, d, arrWords(k + 9), S31, &HD9D4D039, 3
        md5Op d, a, b, c, arrWords(k + 12), S32, &HE6DB99E5, 3
        md5Op c, d, a, b, arrWords(k + 15), S33, &H1FA27CF8, 3
        md5Op b, c, d, a, arrWords(k + 2), S34, &HC4AC5665, 3
    
        md5Op a, b, c, d, arrWords(k + 0), S41, &HF4292244, 4
        md5Op d, a, b, c, arrWords(k + 7), S42, &H432AFF97, 4
        md5Op c, d, a, b, arrWords(k + 14), S43, &HAB9423A7, 4
        md5Op b, c, d, a, arrWords(k + 5), S44, &HFC93A039, 4
        md5Op a, b, c, d, arrWords(k + 12), S41, &H655B59C3, 4
        md5Op d, a, b, c, arrWords(k + 3), S42, &H8F0CCC92, 4
        md5Op c, d, a, b, arrWords(k + 10), S43, &HFFEFF47D, 4
        md5Op b, c, d, a, arrWords(k + 1), S44, &H85845DD1, 4
        md5Op a, b, c, d, arrWords(k + 8), S41, &H6FA87E4F, 4
        md5Op d, a, b, c, arrWords(k + 15), S42, &HFE2CE6E0, 4
        md5Op c, d, a, b, arrWords(k + 6), S43, &HA3014314, 4
        md5Op b, c, d, a, arrWords(k + 13), S44, &H4E0811A1, 4
        md5Op a, b, c, d, arrWords(k + 4), S41, &HF7537E82, 4
        md5Op d, a, b, c, arrWords(k + 11), S42, &HBD3AF235, 4
        md5Op c, d, a, b, arrWords(k + 2), S43, &H2AD7D2BB, 4
        md5Op b, c, d, a, arrWords(k + 9), S44, &HEB86D391, 4
    
        a = md5Add(a, AA)
        b = md5Add(b, BB)
        c = md5Add(c, CC)
        d = md5Add(d, DD)
    next
    
    md5 = LCase(md5WordToHex(a) & md5WordToHex(b) & md5WordToHex(c) _
    	& md5WordToHex(d))
end function

sub md5Op(a, b, c, d, x, s, ac, op)
	dim result
	
	select case op
		case 1
			result = (b and c) or ((not b) and d)
		case 2
			result = (b and d) or (c and (not d))
		case 3
			result = (b xor c xor d)
		case 4
			result = (c xor (b or (not d)))
	end select
    a = md5Add(a, md5Add(md5Add(result, x), ac))
    a = md5RotLeft(a, s)
    a = md5Add(a, b)
end sub

function md5Add(lX, lY)
	dim lX4
    dim lY4
    dim lX8
    dim lY8
    dim lResult
 
    lX8 = lX and &H80000000
    lY8 = lY and &H80000000
    lX4 = lX and &H40000000
    lY4 = lY and &H40000000
 
    lResult = (lX and &H3FFFFFFF) + (lY and &H3FFFFFFF)
 
    if lX4 and lY4 then
        lResult = lResult xor &H80000000 xor lX8 xor lY8
    elseif lX4 or lY4 then
        if lResult and &H40000000 then
            lResult = lResult xor &HC0000000 xor lX8 xor lY8
        else
            lResult = lResult xor &H40000000 xor lX8 xor lY8
        end if
    else
        lResult = lResult xor lX8 xor lY8
    end if
 
    md5Add = lResult
end function

function md5RotLeft(lValue, iShiftBits)
    md5RotLeft = md5LShift(lValue, iShiftBits) _
    	or md5RShift(lValue, (32 - iShiftBits))
end function

function md5RShift(lValue, iShiftBits)
    if iShiftBits = 0 then
        md5RShift = lValue
        exit function
    elseif iShiftBits = 31 then
        if lValue And &H80000000 then
            md5RShift = 1
        else
            md5RShift = 0
        end if
        exit function
    elseif iShiftBits < 0 or iShiftBits > 31 then
        Err.Raise 6
    end if
    
    md5RShift = (lValue and &H7FFFFFFE) \ m_l2Power(iShiftBits)

    if (lValue and &H80000000) then
        md5RShift = (md5RShift or (&H40000000 \ m_l2Power(iShiftBits - 1)))
    end if
end function

function md5LShift(lValue, iShiftBits)
    if iShiftBits = 0 then
        md5LShift = lValue
        exit function
    elseif iShiftBits = 31 then
        if lValue and 1 then
            md5LShift = &H80000000
        else
            md5LShift = 0
        end if
        exit function
    elseif iShiftBits < 0 or iShiftBits > 31 then
        Err.Raise 6
    end if

    if (lValue and m_l2Power(31 - iShiftBits)) then
        md5LShift = ((lValue And m_lOnBits(31 - (iShiftBits + 1))) * m_l2Power(iShiftBits)) Or &H80000000
    else
        md5LShift = ((lValue and m_lOnBits(31 - iShiftBits)) _
        	* m_l2Power(iShiftBits))
    end if
end function

function md5WordToHex(lValue)
    dim lByte
    dim lCount
    
    for lCount = 0 to 3
        lByte = md5RShift(lValue, lCount * BYTE_BITS) _
        	and m_lOnBits(BYTE_BITS - 1)
        md5WordToHex = md5WordToHex & Right("0" & Hex(lByte), 2)
    next
end function

sub md5Init()
	redim m_lOnBits(30)
	redim m_l2Power(30)

	m_lOnBits(0) = CLng(1)
	m_lOnBits(1) = CLng(3)
	m_lOnBits(2) = CLng(7)
	m_lOnBits(3) = CLng(15)
	m_lOnBits(4) = CLng(31)
	m_lOnBits(5) = CLng(63)
	m_lOnBits(6) = CLng(127)
	m_lOnBits(7) = CLng(255)
	m_lOnBits(8) = CLng(511)
	m_lOnBits(9) = CLng(1023)
	m_lOnBits(10) = CLng(2047)
	m_lOnBits(11) = CLng(4095)
	m_lOnBits(12) = CLng(8191)
	m_lOnBits(13) = CLng(16383)
	m_lOnBits(14) = CLng(32767)
	m_lOnBits(15) = CLng(65535)
	m_lOnBits(16) = CLng(131071)
	m_lOnBits(17) = CLng(262143)
	m_lOnBits(18) = CLng(524287)
	m_lOnBits(19) = CLng(1048575)
	m_lOnBits(20) = CLng(2097151)
	m_lOnBits(21) = CLng(4194303)
	m_lOnBits(22) = CLng(8388607)
	m_lOnBits(23) = CLng(16777215)
	m_lOnBits(24) = CLng(33554431)
	m_lOnBits(25) = CLng(67108863)
	m_lOnBits(26) = CLng(134217727)
	m_lOnBits(27) = CLng(268435455)
	m_lOnBits(28) = CLng(536870911)
	m_lOnBits(29) = CLng(1073741823)
	m_lOnBits(30) = CLng(2147483647)
    
	m_l2Power(0) = CLng(1)
	m_l2Power(1) = CLng(2)
	m_l2Power(2) = CLng(4)
	m_l2Power(3) = CLng(8)
	m_l2Power(4) = CLng(16)
	m_l2Power(5) = CLng(32)
	m_l2Power(6) = CLng(64)
	m_l2Power(7) = CLng(128)
	m_l2Power(8) = CLng(256)
	m_l2Power(9) = CLng(512)
	m_l2Power(10) = CLng(1024)
	m_l2Power(11) = CLng(2048)
	m_l2Power(12) = CLng(4096)
	m_l2Power(13) = CLng(8192)
	m_l2Power(14) = CLng(16384)
	m_l2Power(15) = CLng(32768)
	m_l2Power(16) = CLng(65536)
	m_l2Power(17) = CLng(131072)
	m_l2Power(18) = CLng(262144)
	m_l2Power(19) = CLng(524288)
	m_l2Power(20) = CLng(1048576)
	m_l2Power(21) = CLng(2097152)
	m_l2Power(22) = CLng(4194304)
	m_l2Power(23) = CLng(8388608)
	m_l2Power(24) = CLng(16777216)
	m_l2Power(25) = CLng(33554432)
	m_l2Power(26) = CLng(67108864)
	m_l2Power(27) = CLng(134217728)
	m_l2Power(28) = CLng(268435456)
	m_l2Power(29) = CLng(536870912)
	m_l2Power(30) = CLng(1073741824)
end sub

%>
