<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_Email_Validation.asp"-->
<%
dim strError, EmailAddress, CustomerName, ContactPhone, ErrorCode, AdditionalComments
strError = ""
EmailAddress = trim(request.form("EmailAddress"))
CustomerName = trim(request.form("CustomerName"))
ContactPhone = trim(request.form("ContactPhone"))
ErrorCode = request.form("ErrorCode")
AdditionalComments = trim(replace(request.form("AdditionalComments"),vbcrlf,"<br>"))

if IsValidEmail(EmailAddress) = false then strError = strError & "<li>You must provide a valid Email Address.</li>"
if CustomerName = "" then strError = strError & "<li>You must provide Your Name.</li>"
if ContactPhone = "" then strError = strError & "<li>You must provide Your Phone Number.</li>"


dim cdo_to, cdo_from, cdo_subject, cdo_body
cdo_to = "ruben@wirelessemporium.com"
'cdo_to = "webmaster@wirelessemporium.com"
cdo_from = "orderError@wirelessemporium.com"
cdo_subject = "Sent from Order Processing (payment declined)"
cdo_body = "<p>Name: " & CustomerName & "</p>" & vbcrlf
cdo_body = cdo_body & "<p>Phone: " & ContactPhone & "</p>" & vbcrlf
cdo_body = cdo_body & "<p>Email: " & EmailAddress & "</p>" & vbcrlf
cdo_body = cdo_body & "<p>Error Code Given: " & ErrorCode & "</p>" & vbcrlf
cdo_body = cdo_body & "<p>Comments: " & AdditionalComments & "</p>" & vbcrlf
cdo_body = cdo_body & "<p>Date/Time Sent: " & now & "</p>" & vbcrlf
CDOSend cdo_to,cdo_from,cdo_subject,cdo_body

%>

<!--#include virtual="/includes/template/top.asp"-->


					<%=TABLE_START%><tr><td valign="top" width="746" align="center">
						<table border="0" cellspacing="0" cellpadding="0" width="746">
							<tr>
								<td valign="top" class="top-sublink-gray" width="541">
									<h3>
										<br><br>
										<%
										if pageTitle = "ERROR!" then
											response.write pageTitle & "<ul>" & strError & "</ul>" & vbcrlf
											response.write "<a href=""javascript:history.back();"">BACK</a>" & vbcrlf
										else
											response.write pageTitle & vbcrlf
										end if
										%>
									</h3>
									<p>
										<strong>
											<br><br>
											WirelessEmporium.com
										</strong>
									</p>
								</td>
							</tr>
						</table>
					</td></tr><%=TABLE_END%>

<!--#include virtual="/includes/template/bottom.asp"-->
