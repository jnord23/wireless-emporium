<%
function getUPSShipping(sZip,sWeight,shiptype,nTotalQuantity,Paypal,Google)
	dim thisCount, myXML
	thisCount = 3
	if len(sZip) = 5 and isNumeric(sZip) then
		myXML = myXML & "<?xml version=" & chr(34) & "1.0" & chr(34) & "?>"
		myXML = myXML & "<AccessRequest xml:lang=" & chr(34) & "en-US" & chr(34) & ">"
		myXML = myXML & "	<AccessLicenseNumber>DBFAE5E2A07B0DA0</AccessLicenseNumber>"
		'myXML = myXML & "	<UserId>testUser</UserId>"
		'myXML = myXML & "	<Password>testPW</Password>"
		myXML = myXML & "	<UserId>wirelessemporium</UserId>"
		myXML = myXML & "	<Password>bears1986</Password>"
		myXML = myXML & "</AccessRequest>"
		myXML = myXML & "<?xml version=" & chr(34) & "1.0" & chr(34) & "?>"
		myXML = myXML & "<RatingServiceSelectionRequest xml:lang=" & chr(34) & "en-US" & chr(34) & ">"
		myXML = myXML & "	<Request>"
		myXML = myXML & "		<TransactionReference>"
		myXML = myXML & "			<CustomerContext>Rating and Service</CustomerContext>"
		myXML = myXML & "			<XpciVersion>1.0001</XpciVersion>"
		myXML = myXML & "		</TransactionReference>"
		myXML = myXML & "		<RequestAction>Rate</RequestAction>"
		myXML = myXML & "		<RequestOption>shop</RequestOption>"
		myXML = myXML & "	</Request>"
		myXML = myXML & "	<PickupType>"
		myXML = myXML & "		<Code>01</Code>"
		myXML = myXML & "	</PickupType>"
		myXML = myXML & "	<Shipment>"
		myXML = myXML & "		<Shipper>"
		myXML = myXML & "			<Address>"
		myXML = myXML & "				<PostalCode>92867</PostalCode>"
		myXML = myXML & "			</Address>"
		myXML = myXML & "		</Shipper>"
		myXML = myXML & "		<ShipTo>"
		myXML = myXML & "			<Address>"
		myXML = myXML & "				<PostalCode>" & sZip & "</PostalCode>"
		myXML = myXML & "			</Address>"
		myXML = myXML & "		</ShipTo>"
		myXML = myXML & "		<Service>"
		myXML = myXML & "			<Code>11</Code>"
		myXML = myXML & "		</Service>"
		myXML = myXML & "		<Package>"
		myXML = myXML & "			<PackagingType>"
		myXML = myXML & "				<Code>02</Code>"
		myXML = myXML & "				<Description>Package</Description>"
		myXML = myXML & "			</PackagingType>"
		myXML = myXML & "			<Description>Rate Shopping</Description>"
		myXML = myXML & "			<PackageWeight>"
		myXML = myXML & "				<Weight>" & sWeight & "</Weight>"
		myXML = myXML & "			</PackageWeight>"
		myXML = myXML & "		</Package>"
		myXML = myXML & "		<ShipmentServiceOptions/>"
		myXML = myXML & "	</Shipment>"
		myXML = myXML & "</RatingServiceSelectionRequest>"
		
		on error resume next
		
		dim XmlHttp
		Set XmlHttp = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
		xmlHttp.open "POST", "https://wwwcie.ups.com/ups.app/xml/Rate", False
		xmlHttp.send(myXML)
		
		dim xmlDoc, RootNode, NodeList
		Set xmlDoc = Server.CreateObject("Msxml2.DOMDocument.3.0")
		xmlDoc.async = false
		xmlDoc.loadXml xmlHttp.responseText
		Set RootNode = xmlDoc.documentElement
		Set NodeList = RootNode.selectNodes("RatedShipment")
		if NodeList.Length > 0 then
			dim Code, Price
			for a = 0 to NodeList.Length - 1
				Code = GetElementText(NodeList(a),"Service/Code")
				Price = GetElementText(NodeList(a),"RatedPackage/TotalCharges/MonetaryValue")
				Price = cDbl(Price) + 1.4
				if Google = 0 then
					if prepInt(shiptype) = 999 then
						if code = "02" or code = "03" or code = "12" then
							getUPSShipping = getUPSShipping &	"<div style='float:left; border-top:1px dashed #ccc; padding:10px; width:370px;'>" &_
														"<div style='float:left; text-align:left;'>" &_
        													"<div style='font-weight:bold; font-size:14px;'>" & ShippingType(Code) & "</div>" &_
												        "</div>" &_
												        "<div style='float:right; font-size:16px; font-weight:bold; color:#000;'>" & formatCurrency(Price,2) & "</div>" &_
												    "</div>"
						end if
					else
						select case code
							case "02"
								'shiptype 6 - UPS 2nd Day Air
								thisCount = thisCount + 1
								rowClass = "shippingRow"
								checked = ""
								if shiptype = "6" then 
									rowClass = "shippingRowSelected"
									checked = "checked"
								end if
								getUPSShipping = getUPSShipping & 	"<div id=""ship_6"" class=""" & rowClass & """ onclick=""onShippingOption(6)"">" & vbcrlf & _
																	"	<div class=""method"">" & vbcrlf & _
																	"		<div class=""fl""><input type=""radio"" name=""shiptype"" value=""6"" " & checked & " /></div>" & vbcrlf & _
																	"		<div class=""desc fl"" style=""padding:2px 0px 0px 2px;"">" & ShippingType(Code) & "</div>" & vbcrlf & _
																	"	</div>" & vbcrlf & _
																	"	<div class=""days"">2 Business Days</div>" & vbcrlf & _
																	"	<div class=""price"">" & formatCurrency(Price) & "</div>" & vbcrlf & _
																	"	<input type=""hidden"" id=""id_shipcost6"" name=""shipcost6"" value=""" & Price & """>" & vbcrlf & _
																	"</div>" & vbcrlf
							case "03"
								'shiptype 4 - UPS Ground
								thisCount = thisCount + 1
								rowClass = "shippingRow"
								checked = ""								
								if shiptype = "4" then 
									rowClass = "shippingRowSelected"
									checked = "checked"
								end if
								getUPSShipping = getUPSShipping & 	"<div id=""ship_4"" class=""" & rowClass & """ onclick=""onShippingOption(4)"">" & vbcrlf & _
																	"	<div class=""method"">" & vbcrlf & _
																	"		<div class=""fl""><input type=""radio"" name=""shiptype"" value=""4"" " & checked & " /></div>" & vbcrlf & _
																	"		<div class=""desc fl"" style=""padding:2px 0px 0px 2px;"">" & ShippingType(Code) & "</div>" & vbcrlf & _
																	"	</div>" & vbcrlf & _
																	"	<div class=""days"">3-5 Business Days</div>" & vbcrlf & _
																	"	<div class=""price"">" & formatCurrency(Price) & "</div>" & vbcrlf & _
																	"	<input type=""hidden"" id=""id_shipcost4"" name=""shipcost4"" value=""" & Price & """>" & vbcrlf & _
																	"</div>" & vbcrlf
							case "12"
								'shiptype 5 - UPS 3 Day Select
								thisCount = thisCount + 1
								rowClass = "shippingRow"
								checked = ""								
								if shiptype = "5" then 
									rowClass = "shippingRowSelected"
									checked = "checked"
								end if
								getUPSShipping = getUPSShipping & 	"<div id=""ship_5"" class=""" & rowClass & """ onclick=""onShippingOption(5)"">" & vbcrlf & _
																	"	<div class=""method"">" & vbcrlf & _
																	"		<div class=""fl""><input type=""radio"" name=""shiptype"" value=""5"" " & checked & " /></div>" & vbcrlf & _
																	"		<div class=""desc fl"" style=""padding:2px 0px 0px 2px;"">" & ShippingType(Code) & "</div>" & vbcrlf & _
																	"	</div>" & vbcrlf & _
																	"	<div class=""days"">3 Business Days</div>" & vbcrlf & _
																	"	<div class=""price"">" & formatCurrency(Price) & "</div>" & vbcrlf & _
																	"	<input type=""hidden"" id=""id_shipcost5"" name=""shipcost5"" value=""" & Price & """>" & vbcrlf & _
																	"</div>" & vbcrlf								
						end select
					end if
				else	'for Google Checkout
					select case code
						case "02"
							'shiptype 9 - UPS 2nd Day Air
							getUPSShipping = getUPSShipping & ShippingType(Code) & "|" & Price & "|"
						case "03"
							'shiptype 7 - UPS Ground
							getUPSShipping = getUPSShipping & ShippingType(Code) & "|" & Price & "|"
						case "12"
							'shiptype 8 - UPS 3 Day Select
							getUPSShipping = getUPSShipping & ShippingType(Code) & "|" & Price & "|"
					end select
				end if
			next
		end if
		
		on error goto 0
		
	end if
	getUPSShipping = getUPSShipping & "<input type=""hidden"" name=""shippingOptions"" value=" & thisCount & ">" & vbcrlf
end function

Function GetElementText(Node, Tagname)
	Dim NodeList, CurrNode, b
	Set NodeList = Node.getElementsByTagname(Tagname)
	If NodeList.Length > 0 Then
		For b = 0 To NodeList.Length - 1
			Set CurrNode = NodeList.nextNode
			GetElementText = CurrNode.text
		Next
	Else
		GetElementText = ""
	End If
	Set NodeList = Nothing
End Function

Function ShippingType(code)
	select case code
		case "01" : ShippingType = "UPS Next Day Air"
		case "02" : ShippingType = "UPS 2nd Day Air"
		case "03" : ShippingType = "UPS Ground"
		case "07" : ShippingType = "UPS Worldwide Express"
		case "08" : ShippingType = "UPS Worldwide Expedited"
		case "11" : ShippingType = "UPS Standard"
		case "12" : ShippingType = "UPS 3 Day Select"
		case "13" : ShippingType = "UPS Next Day Air Saver"
		case "14" : ShippingType = "UPS Next Day Air Early A.M."
		case "54" : ShippingType = "UPS Worldwide Express Plus"
		case "59" : ShippingType = "UPS 2nd Day Air A.M."
	end select
End Function

Function PackageType(code)
	select case code
		case "01" : PackageType = "UPS letter/ UPS Express Envelope"
		case "02" : PackageType = "Package"
		case "03" : PackageType = "UPS Tube"
		case "04" : PackageType = "UPS Pak"
		case "21" : PackageType = "UPS Express Box"
		case "24" : PackageType = "UPS 25Kg Box"
		case "25" : PackageType = "UPS 10Kg Box"
	end select
End Function
%>
