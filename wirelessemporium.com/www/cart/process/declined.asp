<%
useHttps = 1
response.buffer = true
SEtitle = "Processor Declined - WirelessEmporium.com"
SEdescription = ""
SEkeywords = ""
pageTitle = "declined"
	
dim curSite
if bStaging then
	curSite = "http://staging"
else
	curSite = "https://www"
end if
strHttp = useHttp

dim cart
cart = 1

varientTopBanner = 2
varientTopNav = 1
varientAssurance = 0

set fs = CreateObject("Scripting.FileSystemObject")
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
	' some codes
	declinedOrderID = prepInt(request.Form("declinedOrderID"))
	sql	=	"exec getDeclinedOrderDetail " & declinedOrderID
	
	set rs = oConn.execute(sql)
	if rs.eof then
		response.redirect("/sessionexpired.asp")
		response.end
	end if
	
	orderSubTotal = rs("ordersubtotal")
	orderShippingFee = rs("ordershippingfee")
	orderTax = rs("orderTax")
	orderGrandTotal = rs("ordergrandtotal")
	strShiptype = rs("shiptype")

	fname = rs("fname")
	lname = rs("lname")
	email = rs("email")
	phone = rs("phone")
	sAddress1 = rs("sAddress1")
	sAddress2 = rs("sAddress2")
	scity = rs("scity")
	sstate = rs("sstate")
	szip = rs("szip")
	sCountry = rs("sCountry")
	bAddress1 = rs("bAddress1")
	bAddress2 = rs("bAddress2")
	bcity = rs("bcity")
	bstate = rs("bstate")
	bzip = rs("bzip")
	couponid = rs("couponid")

	nShipType = 0
	if instr(strShiptype, "First Class") > 0 then
		nShipType = 0
	elseif instr(strShiptype, "USPS Priority") > 0 then
		nShipType = 2
	elseif instr(strShiptype, "USPS Express") > 0 then
		nShipType = 3
	elseif instr(strShiptype, "UPS Ground") > 0 then
	  nShipType = 4
	elseif instr(strShiptype, "UPS 3 Day Select") > 0 then
	  nShipType = 5
	elseif instr(strShiptype, "UPS 2nd Day Air") > 0 then
	  nShipType = 6
	end if

	nProdIdCount = 0
	nTotalQuantity = 0
	shippingQty = 0
	strItemCheck = ""
	strQty = ""
	FreeProductInCart = 0
	specialAmt = 0

	SQL = "SELECT a.id shoppingcartID, c.brandName, d.modelName, a.musicSkins, a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, B.itemDesc, B.itemPic, '' defaultPic, B.price_Our, B.price_retail, B.Condition, B.itemWeight, B.NoDiscount FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID and a.musicSkins = 0 left join we_Brands c on b.brandID = c.brandID left join we_Models d on b.modelID = d.modelID WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
	SQL = SQL & " union"
	SQL = SQL & " SELECT a.id shoppingcartID, c.brandName, d.modelName, a.musicSkins, a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, 20 as typeID, B.musicSkinsID as PartNumber, isnull(c.brandName, '') + ' ' + isnull(d.modelName, '') + ' ' + isnull(B.artist, '') + ' ' + isnull(b.designName, '') as itemDesc, B.image as itemPic, B.defaultImg as defaultPic, b.price_we as price_Our, b.msrp as price_retail, 0 as Condition, 1 as itemWeight, 0 as NoDiscount FROM ShoppingCart A INNER JOIN we_items_musicSkins B ON A.itemID=B.id and a.musicSkins = 1 left join we_Brands c on b.brandID = c.brandID left join we_Models d on b.modelID = d.modelID WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
	SQL = SQL & " order by partNumber desc, shoppingcartID"

	set RS = Server.CreateObject("ADODB.Recordset")

	RS.open SQL, oConn, 0, 1

	dim itemCnt : itemCnt = 0
	dim noSR : noSR = 0
	activeProductCnt = 0
	eligibleCnt = 0

	dim baseCaseList : baseCaseList = ""

	do until RS.eof
		noDiscount = RS("NoDiscount")
		if isnumeric(noDiscount) then
			if noDiscount = 0 then noDiscount = false else noDiscount = true
		end if
		nProdIdCheck = RS("itemID")
		nProdQuantity = RS("qty")
		if isnull(rs("musicSkins")) or not rs("musicSkins") then nProdMusicSkins = 0 else nProdMusicSkins = 1
		strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
		strQty = strQty & cStr(nProdQuantity)& ","  ' QTY string for each item, this is for buySafe Use
		if nProdQuantity > 0 and nProdIdCheck > 0 then
			nProdIdCount = nProdIdCount + 1
			useBrandName = RS("brandName")
			useModelName = RS("modelName")
			partNumber = RS("partNumber")
			sItemName = insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName)
			
			if nProdMusicSkins = 1 then
				if isnull(RS("itemPic")) then
					sItemPic = RS("defaultPic")
					musicSkinsDefaultImg = RS("defaultPic")
				else
					sItemPic = RS("itemPic")
				end if
				if instr(sItemPic,";") > 0 then
					musicSkinsDefaultImg = mid(sItemPic,instr(sItemPic,";")+1)
					sItemPic = left(sItemPic,instr(sItemPic,";")-1)
				end if		
			else
				sItemPic = RS("itemPic")
			end if
			sItemPrice = RS("price_Our")
			
			if isDropship(partNumber) or left(partNumber,3) = "WCD" then
				if left(partNumber,3) = "WCD" then noSR = 1
				srEligible = 0
			else
				if noSR = 0 then srEligible = 1
			end if

			if left(partNumber,3) = "WCD" then
				pnArray = split(partNumber,"-")
				baseCaseID = pnArray(1)
				if instr(baseCaseList,baseCaseID) > 0 then
					tempVar = mid(baseCaseList,instr(baseCaseList,baseCaseID))
					tempVar = replace(mid(tempVar,instr(tempVar,"@@")+2),"##","")
					curQty = prepInt(tempVar)
					baseCaseList = replace(baseCaseList,baseCaseID & "@@" & curQty,baseCaseID & "@@" & (curQty + nProdQuantity))
					curQty = curQty + nProdQuantity
				else
					baseCaseList = baseCaseList & baseCaseID & "@@" & nProdQuantity & "##"
				end if
			end if

			if not isnull(rs("customCost")) then sItemPrice = rs("customCost")
			sRetailPrice = prepInt(RS("price_retail"))
			'Free Bluetooth
			if FreeBluetooth = 1 then
				if cStr(nProdIdCheck) = FreeBluetoothItemID then
					FreeBluetoothPrice = sItemPrice
				end if
			end if
			'Free Product With Phone Purchase
			if RS("typeID") = 16 then PhonePurchased = 1
			'Category Sale
			if CategorySale = 1 then
				if RS("typeID") = CategorySaleTypeID and RS("NoDiscount") = 0 then
					CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				else
					SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
					SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_Our > 0)"
					SQL = SQL & " AND (R.typeid = '" & CategorySaleTypeID & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
					SQL = SQL & " AND I.NoDiscount = 0"
					dim RSrelated
					set RSrelated = Server.CreateObject("ADODB.Recordset")
					RSrelated.open SQL, oConn, 0, 1
					if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
					RSrelated.close
					set RSrelated = nothing
				end if
			end if
			'session("errorSQL") = "nSubTotal = " & nSubTotal & " | nProdIdCheck = " & nProdIdCheck & " | sItemPrice = " & sItemPrice & " | nProdQuantity = " & nProdQuantity
			nItemTotal = nItemTotal + (sItemPrice * nProdQuantity)
			if not noDiscount and rs("typeID") <> 16 then
				discountAmt = discountAmt + (sItemPrice * nProdQuantity)
			end if
			sWeight = sWeight + (cDbl(RS("itemWeight")) * nProdQuantity)
			'WE shipping section
			if sItemPrice = 0 then showPrice = "FREE" else showPrice = formatCurrency(sItemPrice * nProdQuantity)
			productList = productList & sItemName & "##" & nProdQuantity & "##" & showPrice & "##" & srEligible & "##" & sItemPic & "##" & musicSkinsDefaultImg & "##" & nProdMusicSkins & "##" & nProdIdCheck & "@@"
			
			activeProductCnt = activeProductCnt + 1
			if srEligible = 1 then eligibleCnt = eligibleCnt + 1
			
	'		WEhtml = WEhtml & "<tr><td align=""left"">" & insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName) & "</td><td align=""center"">" & nProdQuantity & "</td><td align=""right"" colspan='2'>" & showPrice & "</td></tr>" & vbcrlf
			WEhtml2 = WEhtml2 & "<input type=""hidden"" name=""itemID_" & nProdIdCount & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""itemQty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
			PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & sItemName & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & nProdQuantity & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & sItemPrice & """>" & vbcrlf

			'eBillme variables
			EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_number_" & nProdIdCount & """ value=""" & nProdIdCheck & """>" & vbcrlf
			EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_qty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
			EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_price_" & nProdIdCount & """ value=""" & sItemPrice & """>" & vbcrlf
			EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_musicSkins_" & nProdIdCount & """ value=""" & nProdMusicSkins & """>" & vbcrlf
			EBxml = EBxml & "			<itemdetail>" & vbcrlf
			EBxml = EBxml & "				<id>" & nProdIdCheck & "</id>" & vbcrlf
			EBxml = EBxml & "				<name>" & insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName) & "</name>" & vbcrlf
			EBxml = EBxml & "				<quantity>" & nProdQuantity & "</quantity>" & vbcrlf
			EBxml = EBxml & "				<itemcost>" & sItemPrice & "</itemcost>" & vbcrlf
			EBxml = EBxml & "			</itemdetail>"
			
			itemCnt = itemCnt + nProdQuantity
		end if
		RS.movenext
	loop
	RS.close
	set RS = nothing
	rs = null
%>
<!--#include virtual="/includes/template/top_cart.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<%
	if couponid > 0 then
		'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
		SQL = "SELECT * FROM WE_coupons WHERE couponid = '" & couponid & "'"
		set couponRS = oConn.execute(SQL)
		if not couponRS.eof then
			dim sPromoCode, promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber, setValue, oneTime
			couponid = couponRS("couponid")
			sPromoCode = couponRS("promoCode")
			promoMin = couponRS("promoMin")
			promoPercent = couponRS("promoPercent")
			typeID = couponRS("typeID")
			excludeBluetooth = couponRS("excludeBluetooth")
			BOGO = couponRS("BOGO")
			couponDesc = couponRS("couponDesc")
			FreeItemPartNumber = couponRS("FreeItemPartNumber")
			setValue = couponRS("setValue")
			oneTime = couponRS("oneTime")
		end if
	end if

	sql	= "select promoCode from WE_coupons where couponid = '" & couponid & "'"
	set rsCoupon = oConn.execute(sql)
	if not rsCoupon.eof then 
		sPromoCode = rsCoupon("promoCode")
	end if
	
	
	dim nSubTotal
	nSubTotal = nItemTotal

	dim shipcost0, shipcost2, shipcost3, shipcost4, shipcost5
	shipcost0 = "0.00"
	if spendThisMuch = 1 and nSubTotal >= stm_total then
		shipcost2 = 0
		shiptype = 2	
	else
		shipcost2 = 6.99
	end if
	shipcost3 = "24.99"
	shipcost4 = "0.00"
	'Canadian shipping Cost
	shipcost5 = "21.99"

	if sWeight / 16 < 1 then
		sWeight = 1
	else
		sWeight = sWeight / 16
	end if

	if strItemCheck <> "" then strItemCheck = left(strItemCheck,len(strItemCheck)-1)

	if nProdIdCount = 0 then
		htmBasketRows = htmBasketRows & "<tr><td colspan=""7""><i>Basket is currently empty.</i></td></tr>" & vbcrlf
		EmptyBasket = true
	end if
%>
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->
<%
	discountTotal = prepInt(discountTotal)
	maxDiscount = discountTotal
	if discountTotal > nItemTotal then
		discountTotal = nItemTotal
	end if
	maxDiscount = prepInt(maxDiscount)
	activeTotal = nItemTotal - discountTotal

	if instr(discountTotal,".") > 0 then discountTotal = left(discountTotal,instr(discountTotal,".")+2)
	if oneTime then nsubTotal = nsubTotal - discountTotal

	productArray = split(productList,"@@")

	dim nAmount, nCATax
	nSubTotal = nItemTotal - discountTotal
	if nSubTotal = nItemTotal and discountTotal > 0 then nSubTotal = nSubTotal - discountTotal
	nAmount = cDbl(shipcost) + nCATax + nSubTotal

	if discountTotal > 0 then
		nProdIdCount = nProdIdCount + 1
		PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""0""><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""Promo Discount""><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""1""><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & discountTotal * -1 & """>" & vbcrlf
	end if
%>
<table width="958" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td>
			<form id="id_frmDeclined" method="post" name="frmDeclined" >
				<input type="hidden" name="sWeight" value="<%=sWeight%>">
				<input type="hidden" name="nTotalQuantity" value="<%=nTotalQuantity%>">
				<input type="hidden" name="shippingTotal" value="<%=orderShippingFee%>">
				<input type="hidden" name="sPromoCode" value="<%=sPromoCode%>" />
				<input type="hidden" name="discountTotal" value="<%=discountTotal%>">
				<input type="hidden" name="nPromo" value="<%=couponid%>">
				<input type="hidden" name="nCATax" value="<%=orderTax%>">
				<input type="hidden" name="subTotal" value="<%=orderSubTotal%>">
				<input type="hidden" name="grandTotal" value="<%=orderGrandTotal%>">
				<input type="hidden" name="ssl_ProdIdCount" value="<%=nProdIdCount%>">
				<input type="hidden" name="PaymentType" value="CC">
				<input type="hidden" name="shiptypeID" value="<%=nShipType%>">
				<input type="hidden" name="sAddress1" value="<%=sAddress1%>">
				<input type="hidden" name="sAddress2" value="<%=sAddress2%>">
				<input type="hidden" name="sCity" value="<%=sCity%>">
				<input type="hidden" name="sState" value="<%=sState%>">
				<input type="hidden" name="sZip" value="<%=sZip%>">
				<input type="hidden" name="chkOptMail" value="Y">
				<%=WEhtml%>
				<%=EBtemp%>
				<div class="decline-content">
					<div class="head">
						<div class="title">Secure Checkout</div>
						<div class="lock"></div>
						<div class="steps">
							<div class="step1">Cart Summary</div>
							<div class="step2">Secure Checkout</div>
							<div class="step3">Receipt</div>
						</div>
					</div>
					<ul class="errors">
						<li>Payment validation failed: Processor Declined.</li>
						<li>For security reasons, you must re-enter your credit card information.</li>
					</ul>
					<div class="form-container">
						<div class="form">
							<h1><span>Step 1:</span> Enter Payment Information</h1>
							<div class="step1">
								<div class="cards">
									<div id="visa" class="visa-off"></div>
									<div id="master" class="mast-off"></div>
									<div id="amex" class="amex-off"></div>
									<div id="discover" class="disc-off"></div>
									<input type="hidden" value="" name="cc_cardType" id="card_type" />
								</div>
								<div class="fields">
									<div class="label-container"><label for="cardholder_name">Cardholder Name:</label></div>
									<div class="field-container"><input type="text" name="cc_cardOwner" id="cardholder_name" value="" /></div>
								</div>
								<div class="fields">
									<div class="label-container"><label for="card_num">Credit Card #:</label></div>
									<div class="field-container">
										<input type="text" name="cardNum" id="card_num" value="" maxlength="16" />
										<br /><i>No spaces/dashes<br />Example: 1234123412341234</i>																				
									</div>
								</div>
								<div class="fields">
									<div class="label-container"><label for="security_code">Security Code:</label></div>
									<div class="field-container"><input type="text" name="secCode" id="security_code" value="" size="4" />&nbsp;<a href="/cart/cvv2help" class="orange" target="_blank">What's this?</a></div>
								</div>
								<div class="fields">
									<div class="label-container"><label for="expiry_month">Expiration Date:</label></div>
									<div class="field-container">
										<select name="cc_month" id="expiry_month">
											<option value="01">01 (January)</option>
											<option value="02">02 (February)</option>
											<option value="03">03 (March)</option>
											<option value="04">04 (April)</option>
											<option value="05">05 (May)</option>
											<option value="06">06 (June)</option>
											<option value="07">07 (July)</option>
											<option value="08">08 (August)</option>
											<option value="09">09 (September)</option>
											<option value="10">10 (October)</option>
											<option value="11">11 (November)</option>
											<option value="12">12 (December)</option>
										</select>
										&nbsp;
										<select name="cc_year">
											 <%
												for countYear = 0 to 14
													response.write "<option value=""" & right(cStr(year(date) + countYear),2) & """>" & right(cStr(year(date) + countYear),2) & "</option>" & vbcrlf
												next
											%>
										</select>
									</div>
								</div>
								<div class="tip">Tip: You may try another credit card or pay with <a href="#" onclick="document.PaypalForm.submit()">Paypal</a></div>
							</div>
							<h1><span>Step 2:</span> Confirm Billing Information</h1>
							<div class="step2">
								<div class="billing">
									<div class="label-container">Billing Address:</div>
									<div class="content-container">[ <a href="#" id="edit">Edit</a> ]</div>
								</div>
								<div class="fields">
									<div class="label-container">Email:</div>
									<div class="field-container"><input type="text" name="email" value="<%=email%>" class="disabled" readonly /></div>
								</div>
								<div class="fields">
									<div class="label-container">First Name:</div>
									<div class="field-container"><input type="text" name="fname" value="<%=fname%>" class="disabled" readonly /></div>
								</div>
								<div class="fields">
									<div class="label-container">Last Name:</div>
									<div class="field-container"><input type="text" name="lname" value="<%=lname%>" class="disabled" readonly /></div>
								</div>
								<div class="fields">
									<div class="label-container">Address:</div>
									<div class="field-container">
										<input type="text" name="bAddress1" value="<%=bAddress1%>" class="disabled" readonly /><br />
										<input type="text" name="bAddress2" value="<%=bAddress2%>" class="disabled" readonly />
									</div>
								</div>
								<div class="fields">
									<div class="label-container">City:</div>
									<div class="field-container"><input type="text" name="bCity" value="<%=bcity%>" class="disabled" readonly /></div>
								</div>
								<div class="fields">
									<div class="label-container">State:</div>
									<div class="field-container">
										<select name="bState" class="disabled" readonly >
											<%getStates(bstate)%>
										</select>
									</div>
								</div>
								<div class="fields">
									<div class="label-container">Zip/Postal:</div>
									<div class="field-container"><input type="text" name="bZip" value="<%=bzip%>" class="disabled" readonly /></div>
								</div>
								<div class="fields">
									<div class="label-container">Phone:</div>
									<div class="field-container"><input type="text" name="phone" value="<%=phone%>" class="disabled" readonly /></div>
								</div>
							</div>
						</div>
						<div class="notes">
							<div class="note">
								<h1>Don't Worry!</h1>
								<p>Though your payment was declined, you may still see a pending charge on your bank statement. Don't worry! Your bank will remove these pending transactions from your account in the next 3-5 days.
							</div>
							<div class="note">
								<h1>Gift Card?</h1>
								<p>Trying to use a gift card? Sometimes issuers such as American Express require that you register the card in order to give it a billing address.</p>
							</div>
							<div class="note">
								<h1>Live Chat</h1>
								<p>Still having issues with your payment? Our Customer Happiness team is here to help! Feel free to call us at 1-800-305-1106 or contact us via Live Chat for assistance.</p>
							</div>
							<div class="call-us">
								<span>Call Us</span><br />
								1-800-305-1106
							</div>
							<a href="#" onclick="OpenLHNChat();return false;">
								<div class="chat-now">
									<span>Need help?</span><br />
									Chat Now
								</div>
							</a>
						</div>
						<div class="buttons-container">
							<div class="buttons">
								<div class="submit" id="submitBttn">
									<img onClick="return CheckSubmit();" src="/images/decline/place_order_button.jpg" width="272" height="56" />
								</div>
								<div class="or">- or -</div>
								<a href="#">
									<div class="paypal" onclick="document.PaypalForm.submit();"></div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</form>
		</td>
	</tr>
</table>
<div style="clear:both; height:15px;"></div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".discount-button, .floating-submit").on("click", function(){
			$.fancybox({
				'content' : $("#email-submit").html(),
				'modal' : true
			});
		});
		
		$("#expand").click(function(){
			if($(this).attr("class") == "expand") {
				$(".email-discount-container").slideDown(400, function(){
					$("#expand").attr("class", "contract");
				});
			} else {
				$(".email-discount-container").slideUp(400, function(){
					$("#expand").attr("class", "expand");
				});
			}
		});
		
		$(".floating-email").focus(function(){
			$(this).val("");
		});
		
		$("#card_num").keyup(function(){
			$("#visa").attr("class", "visa-off");
			$("#master").attr("class", "mast-off");
			$("#amex").attr("class", "amex-off");
			$("#discover").attr("class", "disc-off");
		
			var card_num = $(this).val();
			var first1 = parseInt(card_num.substring(0,1));
			var first2 = parseInt(card_num.substring(0,2));
			
			switch(true) {
				case(first1 == 4):
					$("#card_type").val("VISA");
					$("#visa").attr("class", "visa-on");
					break;
				case(first2 == 34 || first2 == 37):
					$("#card_type").val("AMEX");
					$("#amex").attr("class", "amex-on");
					break;
				case(first2 > 50 && first2 < 56):
					$("#card_type").val("MC");
					$("#master").attr("class", "mast-on");
					break;
				case(first1 == 6):
					$("#card_type").val("DISC");
					$("#discover").attr("class", "disc-on");
			}
		});
		
		$("#edit").click(function(e){
			e.preventDefault();
			$(".step2 .field-container input").each(function(){
				$(this).attr("class", "enabled");
				$(this).removeAttr("readonly");
			});
		});
	});
	
	function CheckSubmit() {
		var f = document.frmDeclined;
		bValid = true;

		CheckValidNEW(f.email.value, "Email is a required field!"); if (!bValid) { f.email.focus(); return false; }
		CheckEMailNEW(f.email.value); if (!bValid) f.email.focus();
		
		CheckValidNEW(f.fname.value, "Your First Name is a required field!"); if (!bValid) { f.fname.focus(); return false; }
		CheckValidNEW(f.lname.value, "Your Last Name is a required field!"); if (!bValid) { f.lname.focus(); return false; }
		CheckValidNEW(f.phone.value, "Your Phone Number is a required field!"); if (!bValid) { f.phone.focus(); return false; }
		
		CheckValidNEW(f.bAddress1.value, "Your Address (for Billing Address) is a required field!"); if (!bValid) { f.bAddress1.focus(); return false; }
		CheckValidNEW(f.bCity.value, "Your City (for Billing Address) is a required field!"); if (!bValid) { f.bCity.focus(); return false; }
		CheckValidNEW(f.bState.options[f.bState.selectedIndex].value, "State (for Billing Address) is a required field!"); if (!bValid) { f.bState.focus(); return false; }
		CheckValidNEW(f.bZip.value, "Your Zip Code (for Billing Address) is a required field!"); if (!bValid) { f.bZip.focus(); return false; }
	
		CheckValidNEW(f.cc_cardOwner.value, "Your Name (as it appears on your credit card) is a required field!"); if (!bValid) { f.cc_cardOwner.focus(); return false; }
		CheckValidNEW(f.cardNum.value, "Your Credit Card Number is a required field!"); if (!bValid) { f.cardNum.focus(); return false; }
		CheckValidNEW(f.cc_month.value, "Your Credit Card Expiration Month is a required field!"); if (!bValid) { f.cc_month.focus(); return false; }
		CheckValidNEW(f.cc_year.value, "Your Credit Card Expiration Year is a required field!"); if (!bValid) { f.cc_year.focus(); return false; }
		CheckValidNEW(f.secCode.value, "Your Credit Card Security Code is a required field!"); if (!bValid) { f.secCode.focus(); return false; }

		if (bValid) {
			<% if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then %>
				f.action = '/framework/processing';
			<% else %>
				f.action = 'https://www.wirelessemporium.com/framework/processing';
			<% end if %>
			document.getElementById("submitBttn").innerHTML = "Please wait while<br />we process your order"
			f.submit();
		}
		return false;
	}
</script>
<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder">
    <input type="hidden" name="numItems" value="<%=nProdIdCount%>">
    <input type="hidden" name="paymentAmount" value="<%=nSubTotal%>">
    <input type="hidden" name="ItemsAndWeight" value="<%=cStr(nProdIdCount) & "|" & cStr(sWeight)%>">
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <%=PPhtml%>
</form>
<!--#include virtual="/includes/template/bottom_cart.asp"-->
