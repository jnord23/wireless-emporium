<%
mobileOrder = request.Form("mobile")
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_GetMySession.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/framework/utility/static.asp"-->
<%
set oConn = Server.CreateObject("ADODB.Connection")
oConn.Open Session("ConnectionString")
	
mobileOrder = request.Form("mobile")
if isnull(mobileOrder) or len(mobileOrder) < 1 then
	mobileOrder = 0
else
	if request.querystring("utm_source") = "AdMob" then
		session("adTracking") = "AdMob"
		session("additionalQS") = "&utm_source=AdMob&utm_medium=" & request.querystring("utm_medium") & "&utm_campaign=" & request.querystring("utm_campaign")
	end if
end if

HTTP_REFERER = request.servervariables("HTTP_REFERER")
response.Write("<!-- " & HTTP_REFERER & " -->")
if instr(HTTP_REFERER,"wirelessemporium.com") < 1 then
	'response.write "<h3>This page will only accept forms submitted from the wirelessemporium.com secure website.</h3>" & vbcrlf
	'response.end
end if

dim sPromoCode, nPromo, nAvsStreet, nAvsZip
dim nOrderTotal, nCATax, nSubTotal, nShipType, nShipRate
dim strShiptype, nShippingID
dim sEmail, sPwd, sFname, sLname, sPhone
dim sSAddy1, sSAddy2, sSCity, sSState, sSZip, sSCountry
dim sBAddy1, sBAddy2, sBCity, sBState, sBZip, sBCountry
dim sHearFrom, sEmailOpt
dim sCardNum
dim ssl_ProdIdCount
dim sCardOwner, sCardType, sCVV2, sCC_Month, sCC_Year

ssl_ProdIdCount = request.form("ssl_ProdIdCount")

sSAddy1 = request.form("sAddress1")
sSAddy2 = request.form("sAddress2")
sSCity = request.form("sCity")
sSState = request.form("sState")
sSZip = request.form("sZip")

sameaddress = prepStr(request.Form("sameaddress"))

if sameaddress = "yes" or mobileOrder = 1 then
	sBAddy1 = request.form("bAddress1")
	sBAddy2 = request.form("bAddress2")
	sBCity = request.form("bCity")
	sBState = request.form("bState")
	sBZip = request.form("bZip")
else
	sBAddy1 = request.form("sAddress1")
	sBAddy2 = request.form("sAddress2")
	sBCity = request.form("sCity")
	sBState = request.form("sState")
	sBZip = request.form("sZip")
end if

if instr(sSZip,"-") > 0 then
	zipArray = split(sSZip,"-")
	sSZip = zipArray(0)
end if
if instr(sBZip,"-") > 0 then
	zipArray = split(sBZip,"-")
	sBZip = zipArray(0)
end if

sPromoCode = request.form("sPromoCode")
nPromo = request.form("nPromo") ' this is couponid
nSubTotal = request.form("subTotal")
discountTotal = prepInt(request.Form("discountTotal"))
useQty = request.form("nTotalQuantity")
nShipRate = prepInt(request.form("shippingTotal"))
nShippingID = request.form("sID" & request.form("radioAddress"))
sCardNum = request.form("cardNum")
nCATax = prepInt(request.form("nCATax"))
'if request.form("sstate") = "CA" then
'	nCATax = round(cDbl(nSubTotal) * .0775, 2)
'else
'	nCATax = 0
'end if
sEmailOpt = request.form("chkOptMail")
nShipType = prepInt(request.form("shiptypeID"))
sCardOwner = request.form("cc_cardOwner")
sCardType = request.form("cc_cardType")
sCVV2 = request.form("secCode")
sCC_Month = request.form("cc_month")
sCC_Year = request.form("cc_year")

sEmail = request.form("email")
sFname = request.form("fname")
sLname = request.form("lname")
sPhone = request.form("phoneNumber")

nAccountId = prepInt(request.Form("accountid"))
parentAcctID = prepInt(request.Form("parentAcctID"))

if len(sBZip) = 5 and isNumeric(sBZip) then
	sBCountry = "US"
else
	if len(sSState) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sSState) > 0 then
		sBCountry = "CANADA"
	else
		sBCountry = ""
	end if
end if

session("errorSQL") = "nSubTotal: " & nSubTotal & "<br>Qty:" & useQty
if isnull(useQty) or len(useQty) < 1 or not isnumeric(useQty) then useQty = 0
nTotalQuantity = cDbl(useQty)

nAvsStreet = sBAddy1 &" "&sBAddy2


nAvsStreet = replace(nAvsStreet, "&", "and")
nAvsZip = sBZip

session("errorSQL") = nSubTotal

if len(sSZip) = 5 and isNumeric(sSZip) then
	sSCountry = "US"
else
	if len(sSState) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sSState) > 0 then
		sSCountry = "CANADA"
	else
		sSCountry = ""
	end if
end if

if prepStr(session("sr_token")) <> "" then
	if instr(lcase(replace(sSAddy1,".","")),"po box") > 0 then
		response.Redirect("/cart/shopRunnerError.asp")
	end if
	if sSCountry <> "US" or sSState = "AE" or sSState = "AA" or sSState = "AP" or sSState = "AS" or sSState = "FM" or sSState = "GU" or sSState = "MH" or sSState = "MP" or sSState = "PW" or sSState = "VI" then
		response.Redirect("/cart/shopRunnerError.asp")
	end if
end if

select case nShipType
	case "0"
		if sSCountry <> "US" then
			strShiptype = "First Class Int'l"
			nShipRate = 0.00 + (0 * nTotalQuantity)
		else
			strShiptype = "First Class"
			nShipRate = 0.00 + (0 * nTotalQuantity)
		end if
	case "2"
		if nSubTotal > 29.99 and date < cdate("12/19/2011") then
			if sSCountry <> "US" then
				strShiptype = "USPS Priority Int'l"
				nShipRate = 0 + (0 * nTotalQuantity)
			else
				strShiptype = "USPS Priority"
				nShipRate = 0 + (0 * nTotalQuantity)
			end if
		else
			if sSCountry <> "US" then
				strShiptype = "USPS Priority Int'l"
				nShipRate = 6.99 + (0 * nTotalQuantity)
			else
				strShiptype = "USPS Priority"
				nShipRate = 6.99 + (0 * nTotalQuantity)
			end if
		end if
	case "3"
		if freeship = 1 then
			strShiptype = "USPS Express"
			nShipRate = 0 + (0 * nTotalQuantity)
		else
			strShiptype = "USPS Express"
			nShipRate = 24.99 + (0 * nTotalQuantity)
		end if
	case "4" : strShiptype = "UPS Ground"
	case "5" : strShiptype = "UPS 3 Day Select"
	case "6" : strShiptype = "UPS 2nd Day Air"
end select

if prepStr(session("sr_token")) <> "" then
	if sSZip > 80000 and sSZip < 99355 then
		nShipType = 98
		strShiptype = "ShopRunner, 2-Day Shipping - FREE"
	else
		nShipType = 99
		strShiptype = "ShopRunner, 2-Day Shipping - FREE"
	end if
end if

if ssl_ProdIdCount = "0" or ssl_ProdIdCount = "" or not isNumeric(ssl_ProdIdCount) then
	'basket currently empty
	response.redirect("http://www.wirelessemporium.com/sessionexpired.asp?ec=101&pic=" & ssl_ProdIdCount)
	response.end
else
	ssl_ProdIdCount = cDbl(ssl_ProdIdCount)
end if

if nAccountId = 0 then
	'this is an insert new account (we_Accounts)
	SQL = "SET NOCOUNT ON; "
	if parentAcctID > 0 then
		SQL = SQL & "INSERT INTO we_accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,dateEntered,CustomerIP,parentID) VALUES ("
	else
		SQL = SQL & "INSERT INTO we_accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,dateEntered,CustomerIP) VALUES ("
	end if
	SQL = SQL & "'" & ds(sFname) & "', "
	SQL = SQL & "'" & ds(sLname) & "', "
	SQL = SQL & "'" & ds(sBAddy1) & "', "
	SQL = SQL & "'" & ds(sBAddy2) & "', "
	SQL = SQL & "'" & ds(sBCity) & "', "
	SQL = SQL & "'" & ds(sBState) & "', "
	SQL = SQL & "'" & ds(sBZip) & "', "
	SQL = SQL & "'" & ds(sBCountry) & "', "
	SQL = SQL & "'" & ds(sEmail) & "', "
	SQL = SQL & "'" & ds(sPhone) & "', "
	SQL = SQL & "'" & ds(sSAddy1) & "', "
	SQL = SQL & "'" & ds(sSAddy2) & "', "
	SQL = SQL & "'" & ds(sSCity) & "', "
	SQL = SQL & "'" & ds(sSState) & "', "
	SQL = SQL & "'" & ds(sSZip) & "', "
	SQL = SQL & "'" & ds(sSCountry) & "', "
	SQL = SQL & "'" & now & "', "
	if parentAcctID > 0 then
	SQL = SQL & "'" & Request.ServerVariables("REMOTE_ADDR") & "', "
	SQL = SQL & "'" & ds(parentAcctID) & "')"
	else
	SQL = SQL & "'" & Request.ServerVariables("REMOTE_ADDR") & "')"
	end if
	SQL = SQL & "SELECT @@IDENTITY AS newAccountID; "
	SQL = SQL & "SET NOCOUNT OFF;"
	session("errorSQL") = SQL
	dim nAccountId
	set RS = oConn.execute(SQL)
	if not RS.eof then nAccountId = RS("newAccountID")
else
	sql =   "update we_accounts set fname = '" & ds(sFname) & "',lname = '" & ds(sLname) & "',bAddress1 = '" & ds(sBAddy1) & "',bAddress2 = '" & ds(sBAddy2) & "'," &_
			"bCity = '" & ds(sBCity) & "',bState = '" & ds(sBState) & "',bZip = '" & ds(sBZip) & "',bCountry = '" & ds(sBCountry) & "',email = '" & ds(sEmail) & "'," &_
			"phone = '" & ds(sPhone) & "',sAddress1 = '" & ds(sSAddy1) & "',sAddress2 = '" & ds(sSAddy2) & "',sCity = '" & ds(sSCity) & "'," &_
			"sState = '" & ds(sSState) & "',sZip = '" & ds(sSZip) & "',sCountry = '" & ds(sSCountry) & "',CustomerIP = '" & Request.ServerVariables("REMOTE_ADDR") &"' where accountID = " & nAccountId
	session("errorSQL") = sql
	oConn.execute(sql)
end if

oConn.execute ("sp_ModifyEmailOpt_PS '" & ds(sFname) & "','" & sEmail & "','" & sEmailOpt & "'")

' Add new shoppingcart
dim strItems, CellPhoneInOrder, oRsCheckType
strItems = ""
CellPhoneInOrder = 0
for a = 1 to ssl_ProdIdCount
	session("errorSQL") = "SELECT typeID FROM we_items WHERE itemID = '" & request.form("ssl_item_number_" & a) & "' (" & a & ")"
	SQL = 	"SELECT typeID FROM we_items WHERE itemID = '" & request.form("ssl_item_number_" & a) & "'" &_
			" union " &_
			"SELECT '3' as typeID FROM we_items_musicSkins WHERE id = '" & request.form("ssl_item_number_" & a) & "'"
	set oRsCheckType = oConn.execute(SQL)
	if not oRsCheckType.eof then
		if oRsCheckType("typeID") = 16 then 
			CellPhoneInOrder = 1
			Freeship = 0 'check Free overnight shipping
		end if
		strItems = strItems & request.form("ssl_item_number_" & a) & ","
	end if
	oRsCheckType.close
	set oRsCheckType = nothing
next

strItems = left(strItems,len(strItems)-1)
' end add new shoppingcart

nSubTotal = formatNumber(nSubTotal,2)
session("errorSQL") = nSubTotal & " + " & nCATax & " + " & nShipRate & " + " & discountTotal
nOrderTotal = nSubTotal + nCATax + nShipRate - discountTotal
nOrderTotal = formatNumber(nOrderTotal,2)
	
'WRITE THE ORDER IN THE DATABASE
dim nOrderID
nOrderId = fWriteBasketItemIntoDB(nAccountId,nSubTotal,nShipRate,nOrderTotal,strShiptype,nCATax,nShippingID,nPromo)
session("errorSQL") = "Order added"

dim result_message, cvv2_response, avs_response, txn_id, txn_time, approval_code, transaction_type
sCardOwner = replace(sCardOwner, Chr(34), " ")
sCardOwner = replace(sCardOwner, Chr(39), " ")
sCardNum = replace(sCardNum, Chr(34), "")
sCardNum = replace(sCardNum, Chr(39), "")

dim sExp
sExp = sCC_Month & sCC_Year
sCC_Year1 = "20" & sCC_Year

session("errorSQL") = "Process Step 1"
Set XCharge1 = Server.CreateObject("USAePayXChargeCom2.XChargeCom2")
'XCharge1.Command = authonly
XCharge1.Command = 3  '8/19/2009 change to 3
XCharge1.Sourcekey = "0UV1k13ZK7svo97086Bxr585g455ivGf"
XCharge1.Pin = "Cutler1986"
Xcharge1.ignoreDuplicate = true
'This allows fraud blocking on the customers IP address
XCharge1.IP = Request.ServerVariables("REMOTE_ADDR")

session("errorSQL") = "Process Step 2"
if StrComp(sCardNum,"4000100011112224") = 0 then
XCharge1.Testmode = true		'yyy Test mode
elseif StrComp(sCardNum,"4000100011112222") = 0 then
XCharge1.Testmode = true		'NYZ Test mode
elseif StrComp(sCardNum,"5424180279791740") = 0 then
XCharge1.Testmode = true		'negative AVS Test mode
elseif StrComp(sCardNum,"371122223332241") = 0 then
XCharge1.Testmode = true		'Test mode
elseif StrComp(sCardNum,"4111111111111111") = 0 then
XCharge1.Testmode = true		'Test mode
elseif StrComp(sCardNum,"4000200011112226") = 0 then
XCharge1.Testmode = true		'N/A Test mode
elseif StrComp(sCardNum,"") = 0 then
XCharge1.Testmode = true		'Test mode
elseif StrComp(sCardNum,"6011222233332273") = 0 then
XCharge1.Testmode = true		'Test mode
elseif StrComp(sCardNum,"4000600011112223") = 0 then
XCharge1.Testmode = true		'Test mode
else
XCharge1.Testmode = False		'production mode
end if

session("errorSQL") = "Process Step 3"
if XCharge1.Testmode = true then
	if ds(sBAddy1) <> "4040 N. Palm St." then
		response.redirect("/?testError=1")
		response.end
	end if
end if

session("errorSQL") = "Process Step 4"
XCharge1.Card = sCardNum								'Card number, no dashes, no spaces
XCharge1.Exp = sExp										'Expiration date 4 digits
XCharge1.Shipping = nShipRate							'shipping charges.
XCharge1.Discount = discountTotal						'The amount of any discounts that were applied.
XCharge1.Tax = nCATax
'XCharge1.Subtotal = nSubTotal
XCharge1.Subtotal = nSubTotal
XCharge1.Amount = nOrderTotal							'Charge amount in dollars
XCharge1.Invoice = nOrderId								'Invoice number. Must be unique.
XCharge1.OrderID = nOrderId
XCharge1.TransHolderName = sCardOwner					'Name of card holder
XCharge1.Street = sBAddy1 & " " & sBAddy2				'Street address
XCharge1.Zip = sBZip									'Zip code
XCharge1.Description = "wirelessemporium.com Online Order"		'Description of charge
XCharge1.CVV2 = sCVV2									'CVV2 code
XCharge1.Email = ds(sEmail)
' card Auth by UCAF, not sure we have purchase this service,
XCharge1.CardAuth = true

'Billing Address
XCharge1.BillFname = ds(sFname)
XCharge1.BillLNAme = ds(sLname)
XCharge1.BillStreet = ds(sBAddy1)
XCharge1.Billstreet2 = ds(sBAddy2)
XCharge1.BillCity = ds(sBCity)
XCharge1.BillState = ds(sBState)
XCharge1.BillZip = ds(sBZip)
XCharge1.BillCountry = ds(sBCountry)
XCharge1.BillPhone = ds(sPhone)

'Shipping Address
XCharge1.ShipFNAme = ds(sFname)
XCharge1.ShipLName = ds(sLname)
XCharge1.ShipStreet = sSAddy1
XCharge1.ShipStreet2 = ds(sSAddy2)
XCharge1.ShipCity = ds(sSCity)
XCharge1.ShipState = ds(sSState) 
XCharge1.ShipZip = ds(sSZip)
XCharge1.ShipCountry = ds(sSCountry)
XCharge1.ShipPhone = ds(sPhone)
' end card Auth

session("errorSQL") = "Process Step 5"
' Response.Flush
Dim nResponseStatus
if nOrderTotal = 0 then
	nResponseStatus = 0
	session("zeroOrder") = 1
else
	session("errorSQL") = "Prep for process"
	XCharge1.Process
	
	nResponseStatus = XCharge1.ResponseStatus '==== production
	'nResponseStatus = 0 '========================= testing mode
	nResponseReferenceNum = XCharge1.ResponseReferenceNum
	nAvscode = XCharge1.ResponseAVSCODE
	nCvv2code = XCharge1.ResponseCVV2CODE
end if

if nResponseStatus = 0 then      'Approved	
	SQL = "UPDATE we_orders SET transactionID = '" & nResponseReferenceNum & "', approved = 1 WHERE orderid = '" & nOrderID & "'"
	set updateRS = oConn.execute(SQL)
	
	dim thisItemID, thisQuantity
	'update Number of sales
	SQL = "SELECT A.itemID, A.quantity, B.ItemKit_NEW FROM we_orderdetails A INNER JOIN we_items B ON A.itemID=B.itemID WHERE A.orderID = '" & nOrderID & "'"
	'session("errorSQL") = SQL
	set orderdetailRS = oConn.execute(SQL)
	do until orderdetailRS.eof
		thisItemID = orderdetailRS("itemID")
		thisQuantity = orderdetailRS("quantity")
		if isNull(orderdetailRS("ItemKit_NEW")) then
			SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & thisQuantity & " WHERE itemID = '" & thisItemID & "'"
		else
			SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & thisQuantity & " WHERE itemID IN (" & orderdetailRS("ItemKit_NEW") &","&thisItemID & ")"
		end if
		session("errorSQL") = request.servervariables("PATH_INFO") & ":<br>" & SQL
		oConn.execute SQL
		orderdetailRS.movenext
	loop
	' update ShoppingCart table
	SQL = "UPDATE ShoppingCart SET purchasedOrderID = '" & nOrderID & "' WHERE store = 0 AND sessionID = '" & mySession & "' AND (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
	SQL = SQL & " AND itemID IN (" & strItems & ")"
	session("errorSQL") = SQL
	oConn.execute SQL
	' update GiftCertificates table
	'if sGiftCert <> "" then
	'	SQL = "UPDATE GiftCertificates SET UsedOnOrderNumber = '" & nOrderID & "', dateUsed = '" & now & "' WHERE GiftCertificateCode = '" & sGiftCert & "'"
	'	oConn.execute SQL
	'end if
	
	if mobileOrder = 1 then
		sql = "update we_orders set mobileSite = 1 where orderID = " & nOrderID
		session("errorSQL") = sql
		oConn.execute(sql)
	end if

	response.redirect("/cart/complete.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal & "&mobileOrder=" & mobileOrder)
	'response.redirect("/cart/complete.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal)
	response.end
else   ' reject ResponseStatus= 1,2,3,==========================
	Select Case nResponseStatus
		Case 1 : strResponseStatus = "Declined"
		Case 2 : strResponseStatus = "Verification"
		Case 3 : strResponseStatus = "Error"
	End Select
	Select Case nAvscode
		Case NYZ : AVStext = "Address Does Not Match."
		Case YNA : AVStext = "Address Match And Zip Code Not Match."
		Case NNN : AVStext = "Address Does Not Match And Zip Code Does Not Match."
		Case NYW : AVStext = "Address Does Not Match."
		Case XXU : AVStext = "Address Not Verified For Domestic Transaction."
		Case XXR : AVStext = "System Unavaliable."
		Case XXE : AVStext = "Address Verification Not Allowed for Card Type."
		Case XXG : AVStext = "Global Non-AVS Participant."
		Case YYG : AVStext = "International Zip Not Compaible."
		Case YGG : AVStext = "International Address Not Compatible."
		Case Else : AVStext = "Address Error. Declined."
	End Select
	Select Case nCvv2code
		Case N : Cvv2Text = "CVV Code Does Not Match."
		Case P : Cvv2Text = "CVV Code Does Not Process."
		Case S : Cvv2Text = "CVV Code Should Be On Card But Not So Indicated."
		Case U : Cvv2Text = "Issuer Not Certified."
		Case X : Cvv2Text = "No Response From Assocition."
		Case Else : Cvv2Text = "CVV Code Error. Declined."					
	End Select
	ErrorStr = strResponseStatus & "<br>" & AVStext & " " & Cvv2Text
	
	
	response.write "<html><head><link rel=stylesheet href=""/includes/css/styleCO.css"" type=""text/css""></head><body bgcolor=#ffffff text=#000000 leftmargin=0 topmargin=0 marginwidth='0' marginheight='0' link=#666666 vlink=#666666 alink=#ff6600>" & vbcrlf
	%>
	<br><br>
	<table cellpadding="1" cellspacing="1" width="75%" border="1" align="center" class="mc-text">
		<tr>
			<td align=middle><a href="/"><img src="/images/WElogo.gif" border="0"></a></td>
		</tr>
		<tr>
			<td>
				<strong>
					<%
					if ErrorStr = "" then
						%>
						Due to security reasons, we are unable to process your order online at this time.
						<%
					else
						response.write ErrorStr
					end if
					%>
					<br><br>
				</strong>
			</td>
		</tr>
		<tr>
			<td>
				<br>Please resubmit your order for processing.
				<br>If you continue to experience difficulties, please contact us at 1-800-818-4575 and an Associate will be happy to process your order immediately over the phone.
				<br><br>
				Thank you for shopping with wirelessemporium.com!
				<br><br>
				<a href="mailto:sales@wirelessemporium.com">sales@wirelessemporium.com</a>
			</td>
		</tr>
		<tr><td height="5" bgcolor="#CCCCCC"><img src="/images/spacer.gif" width="1" height="5" border="0"></td></tr>
		<tr>
			<td>
				<p>We apologize for the issues that you are having with this transaction. Please verify that the following information is entered correctly:</p>
				<ul>
					<li>Billing Address (your billing address must be entered exactly as it is on your credit card statement)</li>
					<li>Credit Card Number</li>
					<li>Expiration Date</li>
					<li>CVV Code (you can find this 3 digit code on the back of the card, for Amex the code can also be 4 digits and be located on the front)</li>
				</ul>
				<p>If all of this information is entered correctly please fill out the form below and we will get back to you shortly.</p>
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="mc-text">
					<form action="http://www.wirelessemporium.com/DeclineProcess.asp" method="post">
					<tr>
						<td valign="top">Customer Name on Order:</p></td>
						<td valign="top"><p><input type="text" name="CustomerName" size="80"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Email address associated with order:</p></td>
						<td valign="top"><p><input type="text" name="EmailAddress" size="80"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Contact phone number:</p></td>
						<td valign="top"><p><input type="text" name="ContactPhone" size="80"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Error code given:</p></td>
						<td valign="top"><p><input type="text" name="ErrorCode" size="80" value="<%=sRESPMSG%>"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Additional comments/concerns:</p></td>
						<td valign="top"><p><textarea name="AdditionalComments" cols="65" rows="3"></textarea></p></td>
					</tr>
					<tr>
						<td colspan="2" align="center"><p><input type="submit" name="submitted" value="Send Form"></p></td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	<%
	response.write "</body></html>" & vbcrlf
end if
'Clear the component instance from memory
set XCharge1 = Nothing
'end XCharge

'Function from inc_orderfunctions.asp
function fWriteBasketItemIntoDB(nAccountId,nSubTotal,sShipRate,nOrderTotal,strShiptype,sTax,shippingid,nPromo)
	'this function will write the basket items into the database we_orders
	if ssl_ProdIdCount = 0 then
		'basket currently empty
		response.redirect("http://www.wirelessemporium.com/sessionexpired.asp?ec=102")
		response.end
	else
		'insert into WE_Order table
		SQL = "sp_InsertOrder3 0,'" & nAccountId & "','" & FormatNumber(nSubTotal,2) & "','" & FormatNumber(sShipRate,2) & "'," & sTax & ",'" & FormatNumber(nOrderTotal,2) & "','" & replace(strShiptype, "'", "''") & "','" & shippingid & "','" & nPromo & "','" & Now & "'"
		session("errorSQL") = SQL
		nOrderId = oConn.execute(SQL).fields(0).value
		session("invoice") = nOrderId
		'initialize basket variables
		dim sSprocString
		sSprocString = ""
		for a = 1 to ssl_ProdIdCount
			nIdProd = request.form("ssl_item_number_" & a)
			nQtyProd = request.form("ssl_item_qty_" & a)
			'get string for the insertorderdetail stored proc
			sSprocString = "sp_InsertOrderDetail '" & nOrderId & "','" & nIdProd & "','" & nQtyProd & "'"
			'stored procedure that will write each orderitem detail within the orderdetail db
			oConn.execute(sSprocString)
		next
		
	end if
	sql = "update we_orders set shoprunnerID = '" & prepStr(session("sr_token")) & "', note = '" & session("specialOfferText") & "' where orderID = " & nOrderId
	session("errorSQL") = sql
	oConn.execute(sql)
	
	session("errorSQL") = "past sql: now write line"
	fWriteBasketItemIntoDB = nOrderId
end function

function XMLencode(str)
	XMLencode = replace(replace(replace(replace(replace(str,"&","&amp;"),"<","&lt;"),">","&gt;"),"'","&apos;"),chr(34),"&quot;")
end function
%>
