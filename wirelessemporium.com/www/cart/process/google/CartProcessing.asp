<%
dim useHttps : useHttps = 1
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

dim MyCart, MyItem
set MyCart = New Cart
dim mobileOrder : mobileOrder = prepInt(request.Form("mobileOrder"))
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/cart/includes/inc_makeNum.asp"-->
<!--#include virtual="/includes/asp/inc_GetMySession.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<!--#include virtual="/cart/process/google/googleglobal.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->
<%
call fOpenConn()
sql = "insert into temp_checkoutProcess (accountID,checkoutPage,sessionID) values('" & myAccount & "','Google cardProcessing.asp','" & session.SessionID & "')"
session("errorSQL") = sql
oConn.execute(sql)
call fCloseConn()

dim sItemName, sRetailPrice, sItemPrice, sItemPic, nProdIdCheck, nProdQuantity, nProdIdCount
dim sWeight, strItemCheck, WEhtml, PPhtml
sWeight = 0
strItemCheck = ""
PhonePurchased = 0
call fOpenConn()
SQL = "SELECT a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, B.itemDesc, B.itemPic, B.price_Our, B.price_retail, B.Condition, B.itemWeight, B.NoDiscount"
SQL = SQL & " FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID"
SQL = SQL & " WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"

SQL = 	"SELECT c.brandName, d.modelName, a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, B.itemDesc, B.itemPic, '' as defaultPic, B.price_Our, B.price_retail, B.Condition, B.itemWeight, B.NoDiscount FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID left join we_brands c on b.brandID = c.brandID left join we_models d on b.modelID = d.modelID WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)" &_
		" union " &_
		"SELECT c.brandName, d.modelName, a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, 3 as typeID, B.musicSkinsID as PartNumber, B.artist + ' ' + b.designName as itemDesc, B.image as itemPic, B.defaultImg as defaultPic, b.price_we as price_Our, b.msrp as price_retail, 0 as Condition, 1 as itemWeight, 0 as NoDiscount FROM ShoppingCart A INNER JOIN we_items_musicSkins B ON A.itemID=B.id and a.musicSkins = 1 left join we_brands c on b.brandID = c.brandID left join we_models d on b.modelID = d.modelID WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
response.Write(sql)
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
do until RS.eof
	nProdIdCheck = RS("itemID")
	nProdQuantity = RS("qty")
	strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		useBrandName = RS("brandName")
		useModelName = RS("modelName")
		partNumber = RS("partNumber")
		if not isnull(rs("customCost")) then
			sItemPrice = rs("customCost")
		else
			sItemPrice = RS("price_Our")
		end if
		set MyItem = New Item
		session("errorSQL") = "itemDesc:" & RS("itemDesc") & "<br>BrandName:" & useBrandName & "<br>ModelName:" & useModelName
		MyItem.Name = insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName)
		MyItem.Description = insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName)
		MyItem.Quantity = nProdQuantity
		MyItem.UnitPrice = sItemPrice
		MyItem.MerchantItemId = nProdIdCheck
		MyCart.AddItem MyItem
		set MyItem = nothing
		'Free Bluetooth
		if FreeBluetooth = 1 then
			if cStr(nProdIdCheck) = FreeBluetoothItemID then
				FreeBluetoothPrice = sItemPrice
			end if
		end if
		'Free Product With Phone Purchase
		if RS("typeID") = 16 then PhonePurchased = 1
		'Category Sale
		if CategorySale = 1 then
			if RS("typeID") = CategorySaleTypeID and RS("NoDiscount") = 0 then
				CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
			else
				SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
				SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_Our > 0)"
				SQL = SQL & " AND (R.typeid = '" & CategorySaleTypeID & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
				SQL = SQL & " AND I.NoDiscount = 0"
				dim RSrelated
				set RSrelated = Server.CreateObject("ADODB.Recordset")
				RSrelated.open SQL, oConn, 3, 3
				if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				RSrelated.close
				set RSrelated = nothing
			end if
		end if
		nItemTotal = nItemTotal + (sItemPrice * nProdQuantity)
		sWeight = sWeight + (cDbl(RS("itemWeight")) * nProdQuantity)
		
		if RS("typeID") = 16 then
			totalFetchback = totalFetchback + (RS("price_our") * .238)
		else
			totalFetchback = totalFetchback + RS("price_our")
		end if
	end if
	RS.movenext
loop
RS.close
session("orderSubTotal") = nItemTotal
set RS = nothing
call fCloseConn()

'GET COUPON VALUES
dim sPromoCode
if request.form("promo") <> "" then
	sPromoCode = SQLquote(request.form("promo"))
else
	sPromoCode = session("promo")
end if

if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	call fOpenConn()
	SQL = "SELECT * FROM WE_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		FreeItemPartNumber = RS("FreeItemPartNumber")
	else
		sPromoCode = ""
	end if
	call fCloseConn()
end if

dim nSubTotal
nSubTotal = nItemTotal

if sWeight / 16 < 1 then
	sWeight = 1
else
	sWeight = sWeight / 16
end if

if strItemCheck <> "" then strItemCheck = left(strItemCheck,len(strItemCheck)-1)

if nProdIdCount = 0 then
	response.write "<h2>Your session has expired. <a href=""/"">Click here</a> to start again.</h2>"
	response.end
end if

%>
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<%

if discountTotal > 0 then
	if instr(discountTotal,".") > 0 then discountTotal = left(discountTotal,instr(discountTotal,".")+2)
	set MyItem = New Item
	MyItem.Name = "DISCOUNT"
	MyItem.Description = sPromoCode
	MyItem.Quantity = 1
	MyItem.UnitPrice = discountTotal * -1
	MyCart.AddItem MyItem
	set MyItem = nothing
end if

if nGiftCertAmount > 0 then
	set MyItem = New Item
	MyItem.Name = "Gift Certificate"
	MyItem.Description = GiftCertificateCode
	MyItem.Quantity = 1
	MyItem.UnitPrice = nGiftCertAmount * -1
	MyCart.AddItem MyItem
	set MyItem = nothing
end if

nSubTotal = nItemTotal

' Set optional parameters
' Specify an expiration date for the order and build <shopping-cart>
dim myExpDate, myMonth, myDay
myMonth = cStr(month(date))
if month(date) < 10 then myMonth = "0" & myMonth
myDay = cStr(day(date))
if day(date) < 10 then myDay = "0" & myDay
myExpDate = cStr(year(date) + 1) & "-" & myMonth & "-" & myDay & "T23:59:59"

MyCart.CartExpiration = myExpDate
sGiftCert = request.form("gift")
MyCart.MerchantPrivateData = "<session-id>" & mySession & "</session-id><cart-id>" & sGiftCert & "</cart-id>"
if mobileOrder = 1 then
	MyCart.EditCartUrl = "http://m.wirelessemporium.com/basket.asp"
	MyCart.ContinueShoppingUrl = "http://m.wirelessemporium.com/basket.asp"
else
	MyCart.EditCartUrl = "http://www.wirelessemporium.com/cart/basket.asp"
	MyCart.ContinueShoppingUrl = "https://www.wirelessemporium.com/cart/process/google/continue.asp"
end if
MyCart.RequestBuyerPhoneNumber = True
MyCart.RequestInitialAuthDetails = True

' Add shipping methods
' Note: You cannot mix merchant-calculated-shipping methods with other shipping methods (flat-rate-shipping or pickup) in the same cart.
dim MyShipping

' Add a flat-rate-shipping method
Set MyShipping = New FlatRateShipping
MyShipping.Name = "First Class"
MyShipping.Price = 0.00
MyShipping.AllowedCountryArea = "ALL"
MyCart.AddShipping MyShipping
Set MyShipping = Nothing

' Add a flat-rate-shipping method
Set MyShipping = New FlatRateShipping
MyShipping.Name = "USPS Priority Mail (2-3 days)"
MyShipping.Price = 6.99
MyShipping.AllowedCountryArea = "ALL"
MyCart.AddShipping MyShipping
Set MyShipping = Nothing

' Add a flat-rate-shipping method
Set MyShipping = New FlatRateShipping
MyShipping.Name = "USPS Express Mail (1-2 days)"
MyShipping.Price = 24.99
MyShipping.AllowedCountryArea = "ALL"
MyCart.AddShipping MyShipping
Set MyShipping = Nothing


'*** UNCOMMENTED THESE - CHECK TO SEE IF INTERNATIONAL SHIPPING IS BEING ALLOWED IN SETTINGS RIGHT NOW ***
' Add an international flat-rate-shipping method
Set MyShipping = New FlatRateShipping
MyShipping.Name = "USPS First Class Int'l (8-12 business days)"
MyShipping.Price = 0
MyShipping.AllowedCountryArea = "CA"
MyCart.AddShipping MyShipping
Set MyShipping = Nothing

' Add an international flat-rate-shipping method
Set MyShipping = New FlatRateShipping
MyShipping.Name = "USPS Priority Int'l (3-8 business days)"
MyShipping.Price = 10.99
MyShipping.AllowedCountryArea = "CA"
MyCart.AddShipping MyShipping
Set MyShipping = Nothing
'*********************************************************************************************************


'*** NEW UPS SHIPPING ***
dim sZip, thisUPS, a
sZip = request.form("shipZip")
sWeight = request.form("sWeight")
thisUPS = UPScode(sZip,sWeight,shiptype,nTotalQuantity,0,1)
if thisUPS <> "" then
	thisUPS = left(thisUPS,len(thisUPS)-1)
	UPSarray = split(thisUPS,"|")
	for a = 0 to uBound(UPSarray)-1 step 2
		' Add a flat-rate-shipping method
		Set MyShipping = New FlatRateShipping
		MyShipping.Name = UPSarray(a)
		MyShipping.Price = UPSarray(a + 1)
		MyShipping.AllowedCountryArea = "ALL"
		MyCart.AddShipping MyShipping
		Set MyShipping = Nothing
	next
end if
'************************


' Define default tax rules
Dim MyDefaultTaxRule

Set MyDefaultTaxRule = New DefaultTaxRule
MyDefaultTaxRule.Rate = Application("taxMath")
MyDefaultTaxRule.States = Array("CA")
MyCart.AddDefaultTaxRule(MyDefaultTaxRule)
Set MyDefaultTaxRule = Nothing

Set MyDefaultTaxRule = New DefaultTaxRule
MyDefaultTaxRule.Rate = 0
MyDefaultTaxRule.CountryArea = "ALL"
MyCart.AddDefaultTaxRule(MyDefaultTaxRule)
Set MyDefaultTaxRule = Nothing

' Add parameterized-urls for tracking pixels
Dim MyURLS

' If this is an Affiliate Referral, add the appropriate tracking code
on error resume next
vendororder = Request.Cookies("vendororder")
on error goto 0
if vendororder <> "" and vendororder <> "gbase" and vendororder <> "turnto" then
	select case left(vendororder,7)
		case "adwords"
			' Add a parameterized URL for Google AdWords
			set MyURLS = New ParameterizedUrls
			MyURLS.url = server.htmlencode("https://www.googleadservices.com/pagead/conversion/1072448988/imp.gif?label=purchase&script=0")
			MyURLS.total = "value"
			MyCart.AddParameterizedUrl MyURLS
			set MyURLS = nothing
		case "fbprodu"
			if PhonePurchased = 0 then
				set MyURLS = New ParameterizedUrls
				MyURLS.url = server.htmlencode("https://pixel.fetchback.com/serve/fb/pdc?cat=&name=success&sid=1235")
				MyURLS.total = "crv"
				MyCart.AddParameterizedUrl MyURLS
				set MyURLS = nothing
			end if
	end select
else
	if PhonePurchased = 0 then
		set MyURLS = New ParameterizedUrls
		MyURLS.url = server.htmlencode("https://pixel.fetchback.com/serve/fb/pdc?cat=&name=success&sid=1235")
		MyURLS.total = "crv"
		MyCart.AddParameterizedUrl MyURLS
		set MyURLS = nothing
	end if
end if

' Display <checkout-shopping-cart> XML
'Response.Write MyCart.GetXml
'response.end

' Diagnose <checkout-shopping-cart> XML
' MyCart.DiagnoseXml

' Post Cart to Google Checkout 
MyCart.PostCartToGoogle


Set MyCart = nothing
%>
