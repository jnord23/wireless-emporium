<%
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

noCommentBox = true

dim curSite
'curSite = "http://staging"
curSite = "https://www"

dim cart
cart = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top_cart.asp"-->
<link rel="stylesheet" type="text/css" href="https://test.modasolutions.com/wupay/terms/img/default.css">
<script type="text/javascript">
	var acceptanceProgress = 0;
	var buttons = "https://content.wupaygateway.com/wupay/terms/img/buttons.htm";
	var blank = "https://content.wupaygateway.com/wupay/terms/img/blank.htm";
	
	function goBack() { window.location.href= '/cart/checkout.asp';}
	
	function acceptanceAlert() {
		if ( acceptanceProgress==3 )
			return true;
		
		alert("You must accept all Terms and Conditions");
		return false;
	}
	
	/*
	function submitContinue() {
		document.getElementById("continue").value = "Continue";
		document.form.submit();
	}

	function submitCancel() {
		document.getElementById("cancel").value = "Cancel";
		document.form.submit();
	}
	*/

	function acceptanceChangeImage(image) {
		if ( acceptanceProgress==3 ) {
			changeImages('id_submit', image);
			return true;
		}
	}

	function acceptanceCaluclator(obj) {
		if ( obj.checked==true )
			++acceptanceProgress;
		else
			--acceptanceProgress;
		if ( document.getElementById("id_submit") ) {
			changeImages('id_submit', acceptanceProgress==3 ? 'https://content.wupaygateway.com/wupay/terms/img/submit-over.png':
															'https://content.wupaygateway.com/wupay/terms/img/submit.png');
		}
		else if ( document.getElementById("buttons") ) {
			var obj = document.getElementById("buttons");
			if ( acceptanceProgress==3 ) {
				 obj.src = buttons;
			}
			else if ( obj.src!=blank ) {
				obj.src=blank;
			}
		}
	}

	function newImage(arg) {
		if (document.images) {
			rslt = new Image();
			rslt.src = arg;
			return rslt;
		}
	}

	function changeImages() {
		if (document.images && (preloadFlag == true)) {
			for (var i=0; i<changeImages.arguments.length; i+=2) {
				document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
			}
		}
	}

	var preloadFlag = false;
	function preloadImages() {
		acceptanceProgress = 0;
		if (document.images) {
			goback_over = newImage("https://content.wupaygateway.com/wupay/terms/img/goback-over.png");
			submit_over = newImage("https://content.wupaygateway.com/wupay/terms/img/submit-over.png");
			submit_down = newImage("https://content.wupaygateway.com/wupay/terms/img/submit-down.png");
			preloadFlag = true;
		}
		
	}
	
	window.onload = function() {frmEbillMe.submit();}
</script>

<%
	accountID = prepInt(request.form("accountid"))
	
	if accountID = 0 then
		sql = 	"SET NOCOUNT ON; insert into we_accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,sAddress1,sAddress2,sCity,sState,sZip,sCountry,phone,email) " &_
				"values('" & request.Form("fname") & "','" & request.Form("lname") & "','" & request.Form("bAddress1") & "','" & request.Form("bAddress2") & "','" & request.Form("bCity") & "','" & request.Form("bState") & "','" & request.Form("bZip") & "','USA','" & request.Form("sAddress1") & "','" & request.Form("sAddress2") & "','" & request.Form("sCity") & "','" & request.Form("sState") & "','" & request.Form("sZip") & "','" & request.Form("sCountry") & "','" & request.Form("phoneNumber") & "','" & request.Form("email") & "'); SELECT @@IDENTITY AS newAcctID;"
		session("errorSQL") = sql
'		response.Write(sql)
'		response.End()
		set acctRS = oConn.execute(sql)
		accountID = acctRS("newAcctID")
	end if
%>
						<form name="frmEbillMe" action="/cart/process/eBillme/eBillMe_Process.asp" method="post">
                        <!--
						<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td align="left" valign="top" width="800">
									<br>
                                    <table border="0" cellpadding="0" cellspacing="0" align="center">
                                    <tr>
                                        <td colspan="3"><img src="https://content.wupaygateway.com/wupay/terms/img/header.png"></td>
                                    </tr>
                                    <tr>
                                        <td class="leftcol"> </td>
                                        <td valign="bottom"><img src="https://content.wupaygateway.com/wupay/terms/img/tablehead.png"></td>
                                        <td class="rightcol"> </td>
                                    </tr>
                                    <tr>
                                        <td class="leftcol"> </td>
                                        <td class="content">
                                            <table border="0" cellpadding="0" cellspacing="4" width="300" align="center">
                                            <tr>
                                                <th colspan="2" align="left">Please agree to the following terms and conditions:<br><br></th>
                                            </tr>
                                            <tr>
                                                <td valign="top"><input type="checkbox" onclick="acceptanceCaluclator(this);" name="term1" value="1"></td>
                                                <td>I have online banking.</td>
                                            </tr>
                                            <tr>
                                                <td valign="top"><input type="checkbox" onclick="acceptanceCaluclator(this);" name="term2" value="1"></td>
                                                <td>I must pay WU&reg; Pay immediately using online <br>bill pay.</td>
                                            </tr>
                                            <tr>
                                                <td valign="top"><input type="checkbox" onclick="acceptanceCaluclator(this);" name="term3" value="1"></td>
                                                <td>It may take 1 to 3 business days for my bank to process my payment.</td>
                                            </tr>
                                            </table>
                                        </td>
                                        <td class="rightcol"> </td>
                                    </tr>
                                    <tr>
                                        <td class="leftcol"> </td>
                                        <td valign="top"><img src="https://content.wupaygateway.com/wupay/terms/img/tablefoot.png"></td>
                                        <td class="rightcol"> </td>
                                    </tr>
                                    <tr>
                                        <td class="leftcol" style="border-bottom: 1px solid black;"> </td>
                                        <td align="right" style="border-bottom: 1px solid black;">
                                            <a href="javascript:goBack();">
												<img name="goback" src="https://content.wupaygateway.com/wupay/terms/img/goback.png" width="163" height="67" border="0" alt="">
											</a>
                                            <input type="image" name="eSubmit" src="https://content.wupaygateway.com/wupay/terms/img/submit.png" onclick="return acceptanceAlert();" />
                                        </td>
                                        <td class="rightcol" style="border-bottom: 1px solid black;"> </td>
                                    </tr>
                                    </table>
									<img src="https://content.wupaygateway.com/wupay/index.php/accept/tracking/wirelessemporium" height="1" width="1" border="0">
								</td>
							</tr>
                            -->
							<%
                            if request.form("buysafeamount") <> "" then
                                dim buysafeamount
                                buysafeamount = formatNumber(request.form("buysafeamount"),2)
                                if cDbl(buysafeamount) > 0 then
                                    %>
                                    <tr>
                                        <td style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                                            <br>
                                            Earlier, you indicated that you wished to purchase<br>
                                            a buySAFE bond. Unfortunately, this is not<br>
                                            available when paying with eBillme. The buySAFE bond<br>
                                            amount will be removed from your order total.<br>
                                            Our apologies for this inconvenience.
                                        </td>
                                    </tr>
                                    <input type="hidden" name="buysafeamount" value="<%=buysafeamount%>">
                                    <%
                                end if
                            end if
                            %>
                            <input type="hidden" name="amount" value="<%=request.form("subTotal")%>">
                            <input type="hidden" name="nAmount" value="<%=request.form("grandTotal")%>">
                            <input type="hidden" name="nAccountId" value="<%=accountID%>">
                            <input type="hidden" name="shipping" value="<%=request.form("shippingTotal")%>">
                            <input type="hidden" name="shiptype" value="<%=request.form("shiptypeID")%>">
                            <input type="hidden" name="sPromoCode" value="<%=request.form("sPromoCode")%>">
                            <input type="hidden" name="nCouponid" value="<%=request.form("nCouponid")%>">
                            <input type="hidden" name="sGiftCert" value="<%=request.form("sGiftCert")%>">
                            <input type="hidden" name="tax" value="<%=request.form("nCATax")%>">
                            <input type="hidden" name="ShippingAddressID" value="<%=request.form("ShippingAddressID")%>">
                            <%
                            ssl_ProdIdCount = cDbl(request.form("ssl_ProdIdCount"))
                            for a = 1 to ssl_ProdIdCount
                                response.write vbtab & vbtab & vbtab & vbtab & vbtab & vbtab & vbtab & "<input type=""hidden"" name=""ssl_item_number_" & a & """ value=""" & request.form("ssl_item_number_" & a) & """>" & vbcrlf
                                response.write vbtab & vbtab & vbtab & vbtab & vbtab & vbtab & vbtab & "<input type=""hidden"" name=""ssl_item_qty_" & a & """ value=""" & request.form("ssl_item_qty_" & a) & """>" & vbcrlf
                            next
                            %>
                            <input type="hidden" name="myBasketXML" value="<%=request.form("myBasketXML")%>">
                            <input type="hidden" name="ssl_ProdIdCount" value="<%=ssl_ProdIdCount%>">
                            <input type="hidden" name="phoneNumber" value="<%=request("phoneNumber")%>">
						</form>
<!--#include virtual="/includes/template/bottom.asp"-->
