<%
dim useHttps : useHttps = 1
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

Dim cart
cart = 1
pageName = "checkout"
varientTopNav = 1
noCommentBox = true

'TEST CUSTOMER PROCESS IN CHECKOUT
on error resume next
if len(request.Cookies("checkoutPage")) > 0 then
	if request.Cookies("checkoutPage") = 2 then
		response.Cookies("checkoutPage") = 3
	end if
end if
on error goto 0

'option explicit
strBody = "<body leftmargin=""0"" topmargin=""0"" onload=""changeTax(document.frmProcessOrder.sState.value);"">"
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/cart/process/PaypalSOAP/Constants.asp"-->
<!--#include virtual="/cart/includes/inc_getStates.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->

<!--#include virtual="/includes/template/top_cart.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<%
dim token, PayerID, SoapStr
token = request.querystring("token")
PayerID = request.querystring("PayerID")
if token = "" or payerID = "" then
	response.redirect "PaypalError?ec=1004"
	response.end
end if

'BUILD CALL DATA
SoapStr = "<?xml version=""1.0"" encoding=""utf-8"" ?>" & vbcrlf
SoapStr = SoapStr & "<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" & vbcrlf
SoapStr = SoapStr & "	<soap:Header>" & vbcrlf
SoapStr = SoapStr & "		<RequesterCredentials xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
SoapStr = SoapStr & "			<Credentials xmlns=""urn:ebay:apis:eBLBaseComponents"">" & vbcrlf
SoapStr = SoapStr & "				<Username>" & API_USERNAME & "</Username>" & vbcrlf
SoapStr = SoapStr & "				<Password>" & API_PASSWORD & "</Password>" & vbcrlf
SoapStr = SoapStr & "				<Signature>" & API_SIGNATURE & "</Signature>" & vbcrlf
SoapStr = SoapStr & "			</Credentials>" & vbcrlf
SoapStr = SoapStr & "		</RequesterCredentials>" & vbcrlf
SoapStr = SoapStr & "	</soap:Header>" & vbcrlf
SoapStr = SoapStr & "	<soap:Body>" & vbcrlf
SoapStr = SoapStr & "		<GetExpressCheckoutDetailsReq xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
SoapStr = SoapStr & "			<GetExpressCheckoutDetailsRequest>" & vbcrlf
SoapStr = SoapStr & "				<Version xmlns=""urn:ebay:apis:eBLBaseComponents"">" & API_VERSION & "</Version>" & vbcrlf
SoapStr = SoapStr & "				<Token>" & token & "</Token>" & vbcrlf
SoapStr = SoapStr & "			</GetExpressCheckoutDetailsRequest>" & vbcrlf
SoapStr = SoapStr & "		</GetExpressCheckoutDetailsReq>" & vbcrlf
SoapStr = SoapStr & "	</soap:Body>" & vbcrlf
SoapStr = SoapStr & "</soap:Envelope>" & vbcrlf

'CREATE OBJECTS
dim objXMLDOC, objXMLDOM, oNode, strError
set objXMLDOC = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
set objXMLDOM = Server.CreateObject("Msxml2.DomDocument")
set oNode = Server.CreateObject("Microsoft.XMLDOM")

'SET CALL TIMEOUTS
dim lResolve, lConnect, lSend, lReceive
lResolve = 30000 'Timeout values are in milli-seconds
lConnect = 30000
lSend = 30000
lReceive = 30000
objXMLDOC.setTimeouts lResolve, lConnect, lSend, lReceive

'MAKE THE CALL
'response.write SoapStr
objXMLDOC.open "POST", API_ENDPOINT, False
objXMLDOC.setRequestHeader "Content-Type", "text/xml"
objXMLDOC.send(SoapStr)
objXMLDOM.async = false
objXMLDOM.LoadXML objXMLDOC.responseText
'response.write server.HTMLEncode(objXMLDOC.responseText)
'response.end
returnValue = objXMLDOC.responseText

'PROCESS THE CALL
if objXMLDOM.parseError.errorCode <> 0 then
	strError = strError & "<h3>Parser error found.</h3>"
else
	'BEGIN CHECK FOR ERROR FROM PAYPAL
	set oNode = objXMLDOM.getElementsByTagName("Ack")
	if (not oNode is nothing) then
		session("errorSQL") = objXMLDOC.responseText
		if oNode.item(0).text = "Failure" then
			set oNode = objXMLDOM.getElementsByTagName("ShortMessage")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
			set oNode = objXMLDOM.getElementsByTagName("LongMessage")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
			set oNode = objXMLDOM.getElementsByTagName("ErrorCode")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
		end if
	else
		strError = strError & "<h3>No data received.</h3>"
	end if
end if

if strError <> "" then
	response.write strError
	response.end
else
	'PROCESS SUCCESSFUL CALL
	dim ShipToAddressName, ShipToAddressStreet1, ShipToAddressStreet2, ShipToAddressCityName, ShipToAddressStateOrProvince, ShipToAddressPostalCode
	dim ShipToAddressCountry, ShipToAddressPhone
	set oNode = objXMLDOM.getElementsByTagName("Token")
	if (not oNode is nothing) then
		Token = oNode.item(0).text
	end if

	dim nCATax, nItemTotal
	nCATax = 0
	nItemTotal = cdbl(0)
	nDiscountTotal = cdbl(0)
	
	dim ack
	ack = Ucase(objXMLDOM.getElementsByTagName("Ack").item(0).text)
	if ack = "SUCCESS" then
		session("errorSQL") = "returnVal: " & returnValue
		paymentAmount = objXMLDOM.getElementsByTagName("PaymentDetails/ItemTotal").item(0).text
		if paymentAmount = "" then
			response.redirect "PaypalError?ec=1005"
			response.end
		end if
		
		dim nTotalQuantity, sWeight, sZip
		customdata = objXMLDOM.getElementsByTagName("GetExpressCheckoutDetailsResponseDetails/Custom").item(0).text
		if instr(customdata,",") > 0 then
			myarray = split(customdata,",")
			ItemsAndWeight = myarray(0)
		else
			ItemsAndWeight = customdata
		end if
		
		itemsArray = split(ItemsAndWeight,"|")
		for a = 0 to uBound(itemsArray)
			select case a
				case 0 : nTotalQuantity = cDbl(itemsArray(a))
				case 1 : sWeight = cDbl(itemsArray(a))
				case 2 : sZip = cStr(itemsArray(a))
			end select
		next

		set ItemDetails = objXMLDOM.documentElement.getElementsByTagName("PaymentDetailsItem")
		for basketitem = 0 to ItemDetails.Length - 1
			set ItemDetailsNodes = ItemDetails.item(basketitem).childNodes
			itemPrice = cdbl(ItemDetailsNodes(4).text)
			itemQty = cdbl(ItemDetailsNodes(2).text)
			if itemPrice >= 0 then
				nItemTotal = nItemTotal + (itemPrice*itemQty)
			else
				nDiscountTotal = nDiscountTotal + (itemPrice*itemQty)
			end if
		next
				
		dim TAXAMT, strShiptype, shiptype, nShipRate, doNotProcess, newOrderID
		
		dim shipcost0, shipcost2, shipcost3, shipcost4, shipcost5
		shipcost0 = 0
		if paymentAmount > 29.99 and date < cdate("12/19/2011") then
			shipcost2 = 0
			shiptype = 2
		else
			shipcost2 = 6.99
		end if
		shipcost3 = 24.99
		shipcost4 = 0 
		shipcost5 = 10.99
		
		dim fullName, nameArray, FIRSTNAME, LASTNAME
		fullName = objXMLDOM.getElementsByTagName("ShipToAddress/Name").item(0).text
		session("errorSQL") = fullName
		nameArray = split(fullName," ")
		FIRSTNAME = nameArray(0)
		LASTNAME = replace(fullName,FIRSTNAME & " ","")
		
		dim SHIPTOSTREET, SHIPTOSTREET2, SHIPTOCITY, sState, SHIPTOZIP, SHIPTOCOUNTRYCODE, PHONENUM, EMAIL
		SHIPTOSTREET = objXMLDOM.getElementsByTagName("ShipToAddress/Street1").item(0).text
		SHIPTOSTREET2 = objXMLDOM.getElementsByTagName("ShipToAddress/Street2").item(0).text
		SHIPTOCITY = objXMLDOM.getElementsByTagName("ShipToAddress/CityName").item(0).text
		sState = objXMLDOM.getElementsByTagName("ShipToAddress/StateOrProvince").item(0).text
		SHIPTOZIP = objXMLDOM.getElementsByTagName("ShipToAddress/PostalCode").item(0).text
		SHIPTOCOUNTRYCODE = objXMLDOM.getElementsByTagName("ShipToAddress/Country").item(0).text
		PHONENUM = objXMLDOM.getElementsByTagName("ShipToAddress/Phone").item(0).text
		EMAIL = objXMLDOM.getElementsByTagName("Payer").item(0).text
		
		dim CorrelationID
		CorrelationID = objXMLDOM.getElementsByTagName("CorrelationID").item(0).text
		
		if len(SHIPTOCOUNTRYCODE) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sState) > 0 then SHIPTOCOUNTRYCODE = "CA"
		if inStr(",US,AS,FM,GU,MH,MP,PW,PR,VI,AE,AA,AE,AE,AE,AP,","," & SHIPTOCOUNTRYCODE & ",") > 0 then
			strShiptype = ""
			doNotProcess = 0
			nShipRate = shipcost4
		else
			if SHIPTOCOUNTRYCODE = "CA" then
				strShiptype = "1"
				doNotProcess = 0
				nShipRate = shipcost0
			else
				doNotProcess = 1
			end if
		end if
		
		if sState = "CA" then
			TAXAMT = round((nItemTotal) * Application("taxMath"), 2)
		else
			TAXAMT = 0
		end if
		
		if doNotProcess = 0 then
			SQL = "SELECT * FROM we_Orders WHERE extOrderType=1 AND extOrderNumber='" & CorrelationID & "'"
			session("errorSQL") = SQL
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			if RS.eof then
				' Don't do anything if an order already exists in the WE database with this Paypal Order Number
				SQL = "SET NOCOUNT ON; "
				SQL = SQL & "INSERT INTO WE_accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,hearFrom,dateEntered) VALUES ("
				SQL = SQL & "'" & SQLquote(FIRSTNAME) & "', "
				SQL = SQL & "'" & SQLquote(LASTNAME) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOSTREET) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOSTREET2) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOCITY) & "', "
				SQL = SQL & "'" & sState & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOZIP) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOCOUNTRYCODE) & "', "
				SQL = SQL & "'" & SQLquote(EMAIL) & "', "
				SQL = SQL & "'" & SQLquote(PHONENUM) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOSTREET) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOSTREET2) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOCITY) & "', "
				SQL = SQL & "'" & sState & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOZIP) & "', "
				SQL = SQL & "'" & SQLquote(SHIPTOCOUNTRYCODE) & "', "
				SQL = SQL & "'Paypal API', "
				SQL = SQL & "'" & now & "'); "
				SQL = SQL & "SELECT @@IDENTITY AS newAccountID; "
				SQL = SQL & "SET NOCOUNT OFF;"
				session("errorSQL") = SQL
				set RS = oConn.execute(SQL)
				
				dim newAccountID
				if not RS.eof then newAccountID = RS("newAccountID")
				
				SQL = "SET NOCOUNT ON; "
				SQL = SQL & "INSERT INTO we_Orders (store,accountid,extOrderType,extOrderNumber,ordersubtotal,ordershippingfee,orderTax,ordergrandtotal,shiptype,orderdatetime,mobileSite) VALUES ("
				SQL = SQL & "'0', "
				SQL = SQL & "'" & newAccountID & "', "
				SQL = SQL & "'1', "
				SQL = SQL & "'" & CorrelationID & "', "
				SQL = SQL & "'" & formatNumber(nItemTotal,2) & "', "
				SQL = SQL & "'" & formatNumber(nShipRate,2) & "', "
				SQL = SQL & "'" & formatNumber(TAXAMT,2) & "', "
				SQL = SQL & "'" & formatNumber(paymentAmount+TAXAMT,2) & "', "
				if strShiptype = "1" then
					SQL = SQL & "'First Class Int''l', "
				else
					SQL = SQL & "'First Class', "
				end if
				SQL = SQL & "'" & now & "'," & vbcrlf
				SQL = SQL & "1);" & vbcrlf
				SQL = SQL & "SELECT @@IDENTITY AS newOrderID; "
				SQL = SQL & "SET NOCOUNT OFF;"
				session("errorSQL") = SQL
				set RS = oConn.execute(SQL)
				if not RS.eof then newOrderID = RS("newOrderID")
				
				set ItemDetails = objXMLDOM.documentElement.getElementsByTagName("PaymentDetailsItem")
				dim activeProductCnt : activeProductCnt = 0
				dim eligibleCnt : eligibleCnt = 0
				for basketitem = 0 to ItemDetails.Length - 1
					activeProductCnt = activeProductCnt + 1
					set ItemDetailsNodes = ItemDetails.item(basketitem).childNodes
					itemID = ItemDetailsNodes(1).text
					itemPrice = ItemDetailsNodes(4).text
					if itemID = "DISCOUNT" then
						couponcode = ItemDetailsNodes(0).text					
					else
						sql = "select partNumber from we_Items where itemID = " & itemID
						session("errorSQL") = sql
						set prodRS = oConn.execute(sql)
						if not prodRS.EOF then
							partNumber = prodRS("partNumber")
							if not isDropship(partNumber) then eligibleCnt = eligibleCnt + 1
						end if
						if itemID <> "" and itemID <> "1" and itemPrice >= 0 then
							SQL = "INSERT INTO we_Orderdetails (orderid,itemid,quantity,partNumber,price) VALUES ("
							SQL = SQL & "'" & newOrderID & "', "
							SQL = SQL & "'" & itemID & "', "
							SQL = SQL & "'" & ItemDetailsNodes(2).text & "','" & prepStr(partNumber) & "'," & itemPrice & ")"
							oConn.execute(SQL)
						end if
					end if					
				next
				
'				couponcode = session("promocode")
				if couponcode <> "" then
					SQL = "SELECT couponid FROM WE_coupons WHERE promoCode='" & couponcode & "'"
					set RS = Server.CreateObject("ADODB.Recordset")
					RS.open SQL, oConn, 0, 1
					if not RS.eof then
						dim couponid
						couponid = RS("couponid")
					end if
					SQL = "UPDATE we_Orders SET couponid='" & couponid & "' WHERE orderid='" & newOrderID & "'"
					oConn.execute(SQL)
				else
					SQL = "UPDATE we_Orders SET couponid=0 WHERE orderid='" & newOrderID & "'"
					oConn.execute(SQL)
				end if
			end if
			
			grandTotal = paymentAmount + TAXAMT + nShipRate
			
			if instr(lcase(request.ServerVariables("SERVER_NAME")),"m.wireless") or instr(lcase(request.ServerVariables("SERVER_NAME")),"mdev.wireless") > 0 then 
				mobileSite = 1 
				actionURL = "/cart/process/PaypalSOAP/DoExpressCheckoutPayment.htm"
			else 
				mobileSite = 0
				actionURL = "/cart/process/PaypalSOAP/DoExpressCheckoutPayment"
			end if
			%>
<form action="<%=actionURL%>" method="post" name="frmProcessOrder">
<table border="0" cellpadding="0" cellspacing="0" align="center" style="border-top:1px solid #ccc;">
    <tr>
        <td valign="top" bgcolor="#FFFFFF" align="center">
            <table width="100%" border="0" cellspacing="1" cellpadding="3" class="normalText">
                <tr>
                    <td colspan="2" align="right" bgcolor="#CCDDFF"><b>Item&nbsp;Total:</b></td>
                    <td width="56" align="right" bgcolor="#CCDDFF"><b><%=formatCurrency(nItemTotal)%></b></td>
                </tr>
                <tr>
                    <td width="27%" bgcolor="#CCDDFF"><b>Tax</b></td>
                    <td width="65%" align="right" bgcolor="#CCDDFF"><span id="CAtaxMsg"></span></td>
                    <td width="56" align="right" bgcolor="#CCDDFF">
                        <b><span id="CAtax"><%=formatCurrency(TAXAMT)%></span></b>
                        <input type="hidden" name="nCATax" value="<%=nCATax%>">
                    </td>
                </tr>
                <%if nDiscountTotal < 0 then%>
                <tr>
                    <td width="27%" bgcolor="#CCDDFF"><b>Discount</b></td>
                    <td width="65%" align="right" bgcolor="#CCDDFF"></td>
                    <td width="56" align="right" bgcolor="#CCDDFF">
                        <b><%=formatCurrency(nDiscountTotal)%></b>
                    </td>
                </tr>
                <%end if%>
                <tr>
                    <td bgcolor="#CCDDFF"><b>Shipping</b></td>
                    <td align="left" bgcolor="#CCDDFF">
						<%
						shopRunnerOrder = 0
						if prepStr(request.Cookies("srLogin")) = "" or eligibleCnt = 0 then
							if eligibleCnt > 0 then
						%>
                                <div style="width:300px; margin-bottom:10px; height:30px;"><div name="sr_shippingOptionDiv"></div></div>
                            <% end if %>
                            <% if strShiptype = "1" then %>
                                <input type="hidden" name="shipcost0" value="<%=shipcost4%>"><input type="radio" name="shiptype" id="rad_ship1" value="0" onClick="updateGrandTotal();"<%if shiptype = "0" or shiptype = "3" then response.write " checked"%>><span id="Shipping4">USPS First Class Int'l (8-12 business days) - <%=formatcurrency(shipcost4)%></span><br>
                                <input type="hidden" name="shipcost2" value="<%=shipcost5%>"><input type="radio" name="shiptype" id="rad_ship2" value="2" onClick="updateGrandTotal();"<%if shiptype = "2" then response.write " checked"%>><span id="Shipping5">USPS Priority Int'l (3-8 business days) - <%=formatcurrency(shipcost5)%></span>
                            <% else %>
                                <input type="hidden" name="shipcost0" value="<%=shipcost0%>"><input type="radio" name="shiptype" id="rad_ship1" value="0" onClick="updateGrandTotal();"<%if shiptype = "0" or shiptype = "" then response.write " checked"%>><span id="Shipping0">USPS First Class (4-10 business days) - <%=formatcurrency(shipcost0)%></span><br>
                                <input type="hidden" name="shipcost2" value="<%=shipcost2%>"><input type="radio" name="shiptype" id="rad_ship2" value="2" onClick="updateGrandTotal();"<%if shiptype = "2" then response.write " checked"%>><span id="Shipping2">USPS Priority Mail (2-4 business days) - <%=formatcurrency(shipcost2)%></span><br>
                                <input type="hidden" name="shipcost3" value="<%=shipcost3%>"><input type="radio" name="shiptype" id="rad_ship3" value="3" onClick="updateGrandTotal();"<%if shiptype = "3" then response.write " checked"%>><span id="Shipping3">USPS Express Mail (1-2 business days) - <%=formatcurrency(shipcost3)%></span>
                            <%
                            end if
                            on error resume next
                            if SHIPTOZIP <> "" then response.write UPScode(SHIPTOZIP,sWeight,shiptype,nTotalQuantity,0,0)
                            on error goto 0
						else
							shopRunnerOrder = 1
							session("sr_token") = prepStr(request.Cookies("srLogin"))
							if eligibleCnt = activeProductCnt then
							%>
                            <div name="sr_shippingOptionDiv"></div>
                            <%
							else
							%>
                            <div>ShopRunner Eligible Products</div>
                            <div name="sr_shippingOptionDiv"></div>
                            <div>Non-ShopRunner Eligible Products</div>
                            <div><input type="hidden" name="shipcost0" value="<%=shipcost0%>"><input type="radio" name="shiptype" id="rad_ship1" value="0" onClick="updateGrandTotal(this.value,'<%=shipcost0%>');"<%if shiptype = "0" or shiptype = "" then response.write " checked"%>><span style="color:#db3300;font-weight:bold;">FREE SHIPPING</span> <span id="Shipping0">USPS First Class (4-10 business days)</span></div>
                           	<% end if %>
						<% end if %>
                    </td>
                    <td width="56" align="right" valign="top" bgcolor="#CCDDFF" style="color:#db3300;font-weight:bold;">
                        <input type="hidden" name="sWeight" value="<%=sWeight%>">
                        <input type="hidden" name="nTotalQuantity" value="<%=nTotalQuantity%>">
                        <input type="hidden" name="shippingTotal" value="<%=nShipRate%>"><span id="shipcost"><%=formatCurrency(shipcost)%></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right" bgcolor="#CCDDFF"><b>Grand Total</b></td>
                    <td width="56" align="right" bgcolor="#CCDDFF">
                        <input type="hidden" name="Token" value="<%=Token%>">
                        <input type="hidden" name="PayerID" value="<%=PayerID%>">
                        <input type="hidden" name="CorrelationID" value="<%=CorrelationID%>">
                        <input type="hidden" name="itemTotal" value="<%=nItemTotal%>">
                        <input type="hidden" name="grandTotal" value="<%=grandTotal%>">
                        <input type="hidden" name="SHIPPINGAMT" value="<%=nShipRate%>">
                        <input type="hidden" name="TAXAMT" value="<%=TAXAMT%>">
                        <input type="hidden" name="intlShipping" value="<%=strShiptype%>">
                        <input type="hidden" name="INVNUM" value="<%=newOrderID%>">
                        <input type="hidden" name="sPromoCode" value="<%=couponcode%>">
                        <input type="hidden" name="paymentAmount" value="<%=paymentAmount%>">
                        <input type="hidden" name="mobileSite" value="<%=mobileSite%>" />
                        <b><span id="GrandTotal"><%=formatCurrency(grandTotal)%></span></b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <table id="tbl_step2" border="0" cellpadding="4" cellspacing="0" bgcolor="#F7F7F7" class="mc-text">
                            <tr>
                                <td style="padding-top:5px;" valign="top" colspan="2"><font style="color:#FF0000; font-weight:bold;">&nbsp;&nbsp;Shipping&nbsp;Address:</font></td>
                            </tr>
                            <tr>
                                <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;First&nbsp;Name:</strong></td>
                                <td align="left"><input type="text" name="fname" size="27" value="<%=FIRSTNAME%>"></td>
                            </tr>
                            <tr>
                                <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Last&nbsp;Name:</strong></td>
                                <td align="left"><input type="text" name="lname" size="20" value="<%=LASTNAME%>"></td>
                            </tr>
                            <tr>
                                <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Address:</strong></td>
                                <td align="left"><input type="text" name="sAddress1" size="40" value="<%=SHIPTOSTREET%>"></td>
                            </tr>
                            <tr>
                                <td align="right">&nbsp;</td>
                                <td align="left"><input type="text" name="sAddress2" size="40" value="<%=SHIPTOSTREET2%>"></td>
                            </tr>
                            <tr>
                                <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;City:</strong></td>
                                <td align="left"><input type="text" name="sCity" size="40" value="<%=SHIPTOCITY%>"></td>
                            </tr>
                            <tr>
                                <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;State:</strong></td>
                                <td align="left">
                                    <select name="sState" onChange="changeTax(this.value);">
                                        <%getStates(sState)%>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Zip/Postal:</strong></td>
                                <td align="left"><input type="hidden" name="SHIPTOCOUNTRYCODE" value="<%=SHIPTOCOUNTRYCODE%>"><input type="hidden" name="EMAIL" value="<%=EMAIL%>"><input type="input" name="sZip" value="<%=SHIPTOZIP%>"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center" bgcolor="#CCDDFF">
                        <p><input type="checkbox" name="chkOptMail" value="Y" checked>Please include me in the Wireless Emporium VIP mailing list for periodic coupon code offers from WirelessEmporium.com via email.</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <div id="submitBttn" style="font-weight:bold; font-size:18px; color:#000;"><img onClick="return CheckSubmit();" src="/images/cart/orderbutton.jpg" width="204" height="49" alt="Place My Order" style="cursor: hand;"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</form>
			<%
		else
			%>
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2">
            <h3 align="center"><br><br><br><br>We're sorry, but Wireless Emporium does not ship outside of the U.S., its territories, and Canada.</h3>
        </td>
    </tr>
</table>
			<%
		end if
	else
		%>
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2">
            <h3 align="center"><br><br><br><br>We're sorry!<br>Your Paypal payment was declined.</h3>
            <p align="center"><a href="http://www.wirelessemporium.com/cart/basket">PLEASE CLICK HERE TO START AGAIN.</a><br><br><br><br></p>
        </td>
    </tr>
</table>
		<%
	end if
end if

if err.number <> 0 then
	response.redirect "PaypalError?ec=1006"
	response.end
end if

'CLEAN UP EVERYTHING
set objXMLDOC = nothing
set objXMLDOM = nothing
set oNode = nothing
%>
<script>
window.WEDATA.pageType = 'checkout';
window.WEDATA.pageData = {
	discountTotal: '<%=ndiscountTotal%>',
	subTotal: '<%=formatNumber(nItemTotal+TAXAMT-nDiscountTotal,2) %>',
	shipping: '<%= nShipRate %>',
	amount: '<%=grandtotal%>',
	tax: '<%=TAXAMT%>',
	cartItems: []
};
</script>
<!--#include virtual="/includes/template/bottom_cart.asp"-->
<script language="javascript">
<!-- Begin
function changeValue(span,val) {
	if (document.all) {
		eval(span).innerHTML = val;
	} else if (document.getElementById) {
		document.getElementById(span).innerHTML = val;
	}
}

function changeTax(state) {
	var shippingOptions = document.frmProcessOrder.shippingOptions.value;
	var nCATax = CurrencyFormatted((document.frmProcessOrder.itemTotal.value) * <%=Application("taxMath")%>);
	if (state == "CA") {
		changeValue("CAtaxMsg","Additional <%=Application("taxDisplay")%>% Tax for Shipments Within CA");
		changeValue("CAtax","$" + nCATax);
		document.frmProcessOrder.nCATax.value = nCATax;
	} else {
		changeValue("CAtaxMsg","");
		changeValue("CAtax","$0.00");
		document.frmProcessOrder.nCATax.value = 0;
	}
	if (state == "AB" || state == "BC" || state == "MB" || state == "NB" || state == "NL" || state == "NS" || state == "NT" || state == "NU" || state == "ON" || state == "PE" || state == "QC" || state == "SK" || state == "YT") {
		changeValue("Shipping0","USPS First Class Int'l (8-12 business days)");
		document.frmProcessOrder.shipcost2.value = "<%=shipcost5%>";
		changeValue("Shipping2","USPS Priority Int'l (3-8 business days) - $<%=shipcost5%>");
		if (document.frmProcessOrder.shiptype[2].checked == true) {
			document.frmProcessOrder.shiptype[2].checked = false;
			document.frmProcessOrder.shiptype[1].checked = true;
		}
		for (var i = 2; i < shippingOptions; i++) {
			document.frmProcessOrder.shiptype[i].disabled = true;
			changeValue("Shipping" + (i+1),"");
		}
		document.frmProcessOrder.intlShipping.value = "1";
	} else {
		changeValue("Shipping0","USPS First Class (4-10 business days)");
		document.frmProcessOrder.shipcost2.value = "<%=shipcost2%>";
		changeValue("Shipping2","USPS Priority Mail (2-4 business days) - $<%=shipcost2%>");
		document.frmProcessOrder.shiptype[2].disabled = false;
		changeValue("Shipping3","USPS Express Mail (1-2 business days) - $<%=shipcost3%>");
		document.frmProcessOrder.intlShipping.value = "0";
	}
	updateGrandTotal();
}

function updateGrandTotal() {
	var a = document.frmProcessOrder.shiptype.length;
	for (var i = 0; i < a; i++) {
		if (document.frmProcessOrder.shiptype[i].checked) {
			var b = eval('document.frmProcessOrder.shipcost' + document.frmProcessOrder.shiptype[i].value);
			var shippingCost = CurrencyFormatted(b.value);
			document.frmProcessOrder.shippingTotal.value = shippingCost;
			changeValue("shipcost","$" + shippingCost);
		}
	}
	var GrandTotal = Number(document.frmProcessOrder.paymentAmount.value) + Number(document.frmProcessOrder.nCATax.value) + Number(b.value);
	changeValue("GrandTotal","$" + CurrencyFormatted(GrandTotal));
	document.frmProcessOrder.grandTotal.value = CurrencyFormatted(GrandTotal);
}

function CurrencyFormatted(amount) {
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}

function CheckSubmit() {
	var f = document.frmProcessOrder;
	bValid = true;
	
	CheckValidNEW(f.fname.value, "Your First Name is a required field!");
	CheckValidNEW(f.lname.value, "Your Last Name is a required field!");
	CheckValidNEW(f.sAddress1.value, "Your Address (for Shipping Address) is a required field!");
	CheckValidNEW(f.sCity.value, "Your City (for Shipping Address) is a required field!");
	sString = f.sState.options[f.sState.selectedIndex].value;
	CheckValidNEW(sString, "State (for Shipping Address) is a required field!");
	
	if (bValid) {
		<% if shopRunnerOrder = 1 then %>
		f.SHIPPINGAMT.value = 0.00
		<% else %>
		var a = f.shiptype.length;
		for (var i = 0; i < a; i++) {
			if (f.shiptype[i].checked) {
				var shiptype = f.shiptype[i].value;
			}
		}
		var shiprate = eval('document.frmProcessOrder.shipcost' + shiptype);
		f.SHIPPINGAMT.value = shiprate.value;
		<% end if %>
		if (f.sState.options[f.sState.selectedIndex].value == "CA") {
			f.TAXAMT.value = <%=round((nItemTotal) * Application("taxMath"), 2)%>;
		} else {
			f.TAXAMT.value = 0;
		}
		document.getElementById("submitBttn").innerHTML = "Please wait while<br />we process your order"
		f.submit();
	}
	else {
		alert("Error Submitting Form")
	}
	return false;
}

function CheckValidNEW(strFieldName, strMsg) {
	if (strFieldName == "" || strFieldName == null || strFieldName == "NULL" || strFieldName == " ") {
		if (bValid) {
			alert(strMsg);
			bValid = false;
		}
	}
}
//  End -->
</script>