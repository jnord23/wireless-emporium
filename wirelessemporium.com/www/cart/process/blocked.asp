<html>
<head>
<link rel="stylesheet" href="https://www.wirelessemporium.com/includes/css/style4.css" type="text/css">
</head>

<body bgcolor="#ffffff" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" link="#666666" vlink="#666666" alink="#ff6600">

<br><br>
<table cellspacing="1" cellpadding="1" width="75%" border="1" align="center" class="smlText">
	<tr>
		<td align="center"><a href="/index.asp"><img src="https://www.wirelessemporium.com/images/WElogo.gif" border="0" alt="WirelessEmporium.com" width="171" height="93"></a></td>
	</tr>
	<tr>
		<td align="center">
			<strong>
				<br>
				Due to security reasons, we are unable to process your order online at this time.
				<br><br>
				Please contact us at (888) 725-7575
				<br>
				and an Associate will be happy to process your order immediately over the phone.
				<br><br>
				Thank you for shopping with Wireless Emporium!
				<br><br>
				<a href="mailto:sales@wirelessemporium.com">sales@wirelessemporium.com</a>
				<br><br>
			</strong>
		</td>
	</tr>
</table>

</body>
</html>
