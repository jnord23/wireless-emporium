<%
pageName = "checkout"
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

refreshForLogin = request.QueryString("l")
if isnull(refreshForLogin) or len(refreshForLogin) < 1 then refreshForLogin = 0

'Allow for logins and promo codes
if refreshForLogin = 1 then
	response.Cookies("checkoutPage") = ""
	response.Cookies("checkoutPage").expires = date - 1
end if

dim curSite

if instr(request.servervariables("SERVER_NAME"),"staging") > 0 then
	curSite = "http://staging"
else
	curSite = "https://www"
end if

if Request.ServerVariables("SERVER_PORT_SECURE") = "1" then
	strHttp = "https"
else
	strHttp = "http"
end if

dim cart
cart = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
dim SEtitle, SEdescription, SEkeywords
SEtitle = ""
SEdescription = ""
SEkeywords = ""

dim pageTitle, strBody
pageTitle = ""
strBody = "<body leftmargin=""0"" topmargin=""0"" onload=""changeTax(document.frmProcessOrder.sState.value);"">"
%>
<div id="popUpBG" style="height:2000px; display:none; width:100%; background-color:#999; position:absolute; left:0px; top:0px; z-index:100; opacity:0.7; filter:alpha(opacity=70);">&nbsp;</div>
<div id="popUpBox" style="height:1500px; display:none; width:100%; position:absolute; left:0px; right:0px; z-index:101; text-align:center; padding-top:500px;"><a onclick="document.getElementById('popUpBG').style.display = 'none'; document.getElementById('popUpBox').style.display = 'none'" style="cursor:pointer;"><img src="/images/buttons/welcomeBack.jpg" border="0" /></a></div>
<!--#include virtual="/includes/template/top_cart.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<%
dim HTTP_REFERER
HTTP_REFERER = request.servervariables("HTTP_REFERER")
'if left(HTTP_REFERER,47) <> "http://www.wirelessemporium.com/cart/basket.asp" and left(HTTP_REFERER,48) <> "https://www.wirelessemporium.com/cart/basket.asp" and left(HTTP_REFERER,50) <> "https://www.wirelessemporium.com/cart/checkout.asp" and left(HTTP_REFERER,51) <> "http://staging.wirelessemporium.com/cart/basket.asp" and left(HTTP_REFERER,53) <> "http://staging.wirelessemporium.com/cart/checkout.asp" and refreshForLogin = 0 then
'	response.write "<h3>This page will only accept forms submitted from the Wireless Emporium secure website.</h3><!-- " & request.ServerVariables("HTTP_REFERER") & " -->" & vbcrlf
'	response.end
'end if

dim buysafeamount, WantsBondField, ShoppingCartId
if request.form("buysafeamount") = "" then
	buysafeamount = 0
	WantsBondField = "false"
else
	buysafeamount = cDbl(request.form("buysafeamount"))
	WantsBondField = request.form("WantsBondField")
end if
ShoppingCartId = request.form("buysafecartID")

dim nOrderId, a

dim shipcost0, shipcost2, shipcost3, shipcost4, shipcost5
shipcost0 = "0.00"
shipcost2 = "6.99"
shipcost3 = "19.99"
shipcost4 = "0.00"
shipcost5 = "10.99"

dim szip, sWeight
szip = request.form("shipZip")

dim sUserId
if sUserId = "" then
	'check if the user is logging in
	if request.form("submitted") = "submitted" and request.form("loginEmail") <> "" then
		SQL = "SELECT pword,fname,lname,accountid FROM we_accounts"
		SQL = SQL & " WHERE email='" & SQLquote(request.form("loginEmail")) & "' AND pword <> '' AND pword IS NOT null"
		SQL = SQL & " ORDER BY accountid DESC"
		call fOpenConn()
		set RS = oConn.execute(SQL)
		if not RS.eof then
			if SQLquote(request.form("pword1")) = RS("pword") then
				myAccount = RS("accountid")
				on error resume next
				response.cookies("fname") = RS("fname")
				response.cookies("fname").expires = dateAdd("m", 1, now)
				response.cookies("myAccount") = RS("accountid")
				response.cookies("myAccount").expires = dateAdd("m", 1, now)
				on error goto 0
			end if
		end if
		RS.close
		set RS = nothing
		call fCloseConn()
	end if
end if

dim newEmail, email, pword1, pword2, phone, Fname, Lname, sAddress1, sAddress2, sCity, sState, bAddress1, bAddress2, bCity, bState, bZip
dim radioAddress, shiptype, PaymentType, cc_cardType, cc_cardOwner, cardNum, cc_month, cc_year, hearFrom

'check if the user is already logged in
if myAccount <> "" then
	call fOpenConn()
	SQL = "SELECT * FROM we_accounts WHERE accountid = '" & myAccount & "'"
	set RS = oConn.execute(SQL)
	if not RS.eof then
		email = RS("email")
		fname = RS("fname")
		lname = RS("lname")
		phone = RS("phone")
		saddress1 = RS("saddress1")
		saddress2 = RS("saddress2")
		scity = RS("scity")
		sstate = RS("sstate")
		szip = RS("szip")
		bAddress1 = RS("bAddress1")
		bAddress2 = RS("bAddress2")
		bCity = RS("bCity")
		bState = RS("bState")
		bZip = RS("bZip")
	end if
	RS.close
	set RS = nothing
	call fCloseConn()
	radioAddress = request.form("radioAddress")
	shiptype = request.form("shiptype")
	if request.form("szip") <> "" then szip = request.form("szip")
end if

if request.form("submitted") = "submitted" and myAccount = "" then
	newEmail = request.form("newEmail")
	pword1 = request.form("pword1")
	pword2 = request.form("pword2")
	fname = request.form("fname")
	lname = request.form("lname")
	email = request.form("email")
	phone = request.form("phone")
	saddress1 = request.form("saddress1")
	saddress2 = request.form("saddress2")
	scity = request.form("scity")
	sstate = request.form("sstate")
	szip = request.form("szip")
	bAddress1 = request.form("bAddress1")
	bAddress2 = request.form("bAddress2")
	bCity = request.form("bCity")
	bState = request.form("bState")
	bZip = request.form("bZip")
	radioAddress = request.form("radioAddress")
	shiptype = request.form("shiptype")
	PaymentType = request.form("PaymentType")
	cc_cardType = request.form("cc_cardType")
	cc_cardOwner = request.form("cc_cardOwner")
	cardNum = request.form("cardNum")
	cc_month = request.form("cc_month")
	cc_year = request.form("cc_year")
	hearFrom = request.form("hearFrom")
end if

' START basket grab
dim sItemName, sRetailPrice, sItemPrice, sItemPic, nProdIdCheck, nProdQuantity, nProdIdCount, strItemCheck, strQty, nItemTotal
dim WEhtml, WEhtml2, PPhtml, EBtemp, EBxml

nProdIdCount = 0
strItemCheck = ""
strQty = ""
discountAmt = 0

call fOpenConn()
' New cart abandonment tracking added 10/29/2010 by MC
SQL = "UPDATE ShoppingCart SET DateCheckout = '" & now & "' WHERE store = 0 AND sessionID = '" & mySession & "' AND (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
oConn.execute SQL

'SQL = "SELECT a.musicSkins, a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, B.itemDesc, B.itemPic, B.price_Our, B.price_retail, B.Condition, B.itemWeight, B.NoDiscount"
'SQL = SQL & " FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID"
'SQL = SQL & " WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"

SQL = "SELECT a.musicSkins, a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, B.itemDesc, B.itemPic, B.price_Our, B.price_retail, B.Condition, B.itemWeight, B.NoDiscount FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID and a.musicSkins = 0 WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
SQL = SQL & " union"
SQL = SQL & " SELECT a.musicSkins, a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, 3 as typeID, B.musicSkinsID as PartNumber, B.artist + ' ' + b.designName as itemDesc, B.image as itemPic, b.price_we as price_Our, b.msrp as price_retail, 0 as Condition, 1 as itemWeight, 0 as NoDiscount FROM ShoppingCart A INNER JOIN we_items_musicSkins B ON A.itemID=B.id and a.musicSkins = 1 WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"

set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

dim itemCnt : itemCnt = 0

do until RS.eof
	noDiscount = RS("NoDiscount")
	if isnumeric(noDiscount) then
		if noDiscount = 0 then noDiscount = false else noDiscount = true
	end if
	nProdIdCheck = RS("itemID")
	nProdQuantity = RS("qty")
	if isnull(rs("musicSkins")) or not rs("musicSkins") then nProdMusicSkins = 0 else nProdMusicSkins = 1
	strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
	strQty = strQty & cStr(nProdQuantity)& ","  ' QTY string for each item, this is for buySafe Use
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		sItemName = RS("itemDesc")
		sItemPic = RS("itemPic")
		sItemPrice = RS("price_Our")

		if not isnull(rs("customCost")) then sItemPrice = rs("customCost")
		sRetailPrice = RS("price_retail")
		'Free Bluetooth
		if FreeBluetooth = 1 then
			if cStr(nProdIdCheck) = FreeBluetoothItemID then
				FreeBluetoothPrice = sItemPrice
			end if
		end if
		'Free Product With Phone Purchase
		if RS("typeID") = 16 then PhonePurchased = 1
		'Category Sale
		if CategorySale = 1 then
			if RS("typeID") = CategorySaleTypeID and RS("NoDiscount") = 0 then
				CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
			else
				SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
				SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_Our > 0)"
				SQL = SQL & " AND (R.typeid = '" & CategorySaleTypeID & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
				SQL = SQL & " AND I.NoDiscount = 0"
				dim RSrelated
				set RSrelated = Server.CreateObject("ADODB.Recordset")
				RSrelated.open SQL, oConn, 0, 1
				if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				RSrelated.close
				set RSrelated = nothing
			end if
		end if
		'session("errorSQL") = "nSubTotal = " & nSubTotal & " | nProdIdCheck = " & nProdIdCheck & " | sItemPrice = " & sItemPrice & " | nProdQuantity = " & nProdQuantity
		nItemTotal = nItemTotal + (sItemPrice * nProdQuantity)
		if not noDiscount and rs("typeID") <> 16 then
			discountAmt = discountAmt + (sItemPrice * nProdQuantity)
		end if
		sWeight = sWeight + (cDbl(RS("itemWeight")) * nProdQuantity)
		'WE shipping section
		if sItemPrice = 0 then showPrice = "FREE" else showPrice = formatCurrency(sItemPrice * nProdQuantity)
		productList = productList & RS("itemDesc") & "##" & nProdQuantity & "##" & showPrice & "@@"
		WEhtml = WEhtml & "<tr><td align=""left"">" & RS("itemDesc") & "</td><td align=""center"">" & nProdQuantity & "</td><td align=""right"" colspan='2'>" & showPrice & "</td></tr>" & vbcrlf
		WEhtml2 = WEhtml2 & "<input type=""hidden"" name=""itemID_" & nProdIdCount & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""itemQty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		
		PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & sItemName & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & nProdQuantity & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & sItemPrice & """>" & vbcrlf

		'eBillme variables
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_number_" & nProdIdCount & """ value=""" & nProdIdCheck & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_qty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_musicSkins_" & nProdIdCount & """ value=""" & nProdMusicSkins & """>" & vbcrlf
		EBxml = EBxml & "<itemdetails>"
		EBxml = EBxml & "<itemid>" & nProdIdCheck & "</itemid>"
		EBxml = EBxml & "<itemquantity>" & nProdQuantity & "</itemquantity>"
		EBxml = EBxml & "<itemname>" & RS("itemDesc") & "</itemname>"
		EBxml = EBxml & "<itemcost>" & sItemPrice & "</itemcost>"
		EBxml = EBxml & "<itemtax1>0.0</itemtax1><itemtax2>0.0</itemtax2>"
		EBxml = EBxml & "</itemdetails>"
		
		itemCnt = itemCnt + nProdQuantity
	end if
	RS.movenext
loop
RS.close
set RS = nothing
call fCloseConn()
if nProdIdCount < 1 then
	'basket currently empty
	response.redirect("/sessionexpired.asp")
	response.end
end if
' END basket grab

'GET COUPON VALUES
dim sPromoCode
if request.form("promo") <> "" then
	sPromoCode = SQLquote(request.form("promo"))
else
	sPromoCode = session("promocode")
end if

response.Write("<!-- sPromoCode:" & sPromoCode & " -->")

if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	call fOpenConn()
	SQL = "SELECT * FROM WE_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	response.Write("<!-- " & SQL & " -->")
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		FreeItemPartNumber = RS("FreeItemPartNumber")
		setValue = RS("setValue")
		oneTime = RS("oneTime")
		response.Write("<!-- details set (couponid:" & couponid & ") -->")
	else
		sPromoCode = ""
	end if
	call fCloseConn()
end if

dim nSubTotal
nSubTotal = nItemTotal

if sWeight / 16 < 1 then
	sWeight = 1
else
	sWeight = sWeight / 16
end if

if strItemCheck <> "" then strItemCheck = left(strItemCheck,len(strItemCheck)-1)

if nProdIdCount = 0 then
	htmBasketRows = htmBasketRows & "<tr><td colspan=""7""><i>Basket is currently empty.</i></td></tr>" & vbcrlf
	EmptyBasket = true
end if
%>
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->
<%
discountTotal = prepInt(discountTotal)
maxDiscount = discountTotal
if discountTotal > nItemTotal then
	discountTotal = nItemTotal
end if
maxDiscount = prepInt(maxDiscount)

if instr(discountTotal,".") > 0 then discountTotal = left(discountTotal,instr(discountTotal,".")+2)
if oneTime then nsubTotal = nsubTotal - discountTotal
%>
<script language="javascript">
<!-- Begin
function changeValue(span,val) {
	if (document.all) {
		eval(span).innerHTML = val;
	} else if (document.getElementById) {
		document.getElementById(span).innerHTML = val;
	}
}
	
function changeTax(state) {
	//var shippingOptions = document.frmProcessOrder.shippingOptions.value;
	var nCATax = CurrencyFormatted(<%=nItemTotal%> * 0.0775);
	if (state == "CA") {
		changeValue("CAtaxMsg","Additional 7.75% Tax for Shipments Within CA");
		changeValue("CAtax","$" + nCATax);
		document.frmProcessOrder.nCATax.value = nCATax;
	} else {
		changeValue("CAtaxMsg","");
		changeValue("CAtax","$0.00");
		document.frmProcessOrder.nCATax.value = 0;
	}
	/*
	if (state == "AB" || state == "BC" || state == "MB" || state == "NB" || state == "NL" || state == "NS" || state == "NT" || state == "NU" || state == "ON" || state == "PE" || state == "QC" || state == "SK" || state == "YT") {
		changeValue("Shipping0","USPS First Class Int'l (8-12 business days)");
		document.frmProcessOrder.shipcost2.value = "<%=shipcost5%>";
		changeValue("Shipping2","USPS Priority Int'l (3-8 business days) - $<%=shipcost5%>");
		if (document.frmProcessOrder.shiptype[2].checked == true) {
			document.frmProcessOrder.shiptype[2].checked = false;
			document.frmProcessOrder.shiptype[1].checked = true;
		}
		for (var i = 2; i < shippingOptions; i++) {
			document.frmProcessOrder.shiptype[i].disabled = true;
			changeValue("Shipping" + (i+1),"");
		}
	} else {
		changeValue("Shipping0","USPS First Class (4-10 business days)");
		alert(document.frmProcessOrder.shipcost2.value)
		document.frmProcessOrder.shipcost2.value = '<%=shipcost2%>';
		changeValue("Shipping2","USPS Priority Mail (2-4 business days) - $<%=shipcost2%>");
		document.frmProcessOrder.shiptype[2].disabled = false;
		changeValue("Shipping3","USPS Express Mail (1-2 business days) - $<%=shipcost3%>");
	}
	*/
	updateGrandTotal(document.frmProcessOrder.shiptypeID.value,document.frmProcessOrder.shippingTotal.value);
}

function updateGrandTotal(shipID,shipAmt) {
	document.getElementById("shipcost").innerHTML = "$" + CurrencyFormatted(shipAmt)
	document.frmProcessOrder.shippingTotal.value = shipAmt;
	document.frmProcessOrder.shiptypeID.value = shipID;
	//var GrandTotal = Number(document.frmProcessOrder.subTotal.value) + Number(document.frmProcessOrder.nCATax.value) + Number(b.value);
	var GrandTotal = Number(document.frmProcessOrder.subTotal.value) + Number(document.frmProcessOrder.nCATax.value) + Number(shipAmt) + Number(<%=buysafeamount%>);
	if ((GrandTotal - <%=maxDiscount%>) < 0) {
		document.getElementById("discountAmt").innerHTML = "$" + CurrencyFormatted(GrandTotal)
		GrandTotal = 0
	}
	else {
		if (<%=maxDiscount%> > 0) {
			document.getElementById("discountAmt").innerHTML = "$" + CurrencyFormatted(<%=maxDiscount%>)
		}
		GrandTotal = GrandTotal - <%=maxDiscount%>
	}
	changeValue("GrandTotal","$" + CurrencyFormatted(GrandTotal));
	document.frmProcessOrder.grandTotal.value = CurrencyFormatted(GrandTotal);
}

function changeShippingAddress(a) {
	var b = eval('document.frmProcessOrder.sAddress1' + a);
	var c = eval('document.frmProcessOrder.sAddress2' + a);
	var d = eval('document.frmProcessOrder.sCity' + a);
	var e = eval('document.frmProcessOrder.sState' + a);
	var f = eval('document.frmProcessOrder.sZip' + a);
	var g = eval('document.frmProcessOrder.sID' + a);
	document.frmProcessOrder.sAddress1.value = b.value;
	document.frmProcessOrder.sAddress2.value = c.value;
	document.frmProcessOrder.sCity.value = d.value;
	document.frmProcessOrder.sState.value = e.value;
	document.frmProcessOrder.sZip.value = f.value;
	changeTax(e.value);
}

function CheckSubmit() {
	var f = document.frmProcessOrder;
	bValid = true;
	<%if myAccount = "" then%>
	if (f.sameaddress.checked == 0) {
		ShipToBillPerson(f);
	}
	<% end if %>
	CheckValidNEW(f.email.value, "Email is a required field!");
	CheckValidNEW(f.fname.value, "Your First Name is a required field!");
	CheckValidNEW(f.lname.value, "Your Last Name is a required field!");
	CheckValidNEW(f.phoneNumber.value, "Your Phone Number is a required field!");
	CheckValidNEW(f.sAddress1.value, "Your Address (for Shipping Address) is a required field!");
	CheckValidNEW(f.sCity.value, "Your City (for Shipping Address) is a required field!");
	sString = f.sState.options[f.sState.selectedIndex].value;
	CheckValidNEW(sString, "State (for Shipping Address) is a required field!");
	CheckValidNEW(f.sZip.value, "Your Zip Code (for Shipping Address) is a required field!");
	if (f.sameaddress.checked == true) {
		CheckValidNEW(f.bAddress1.value, "Your Address (for Billing Address) is a required field!");
		CheckValidNEW(f.bCity.value, "Your City (for Billing Address) is a required field!");
		sString = f.bState.options[f.bState.selectedIndex].value;
		CheckValidNEW(sString, "State (for Billing Address) is a required field!");
		CheckValidNEW(f.bZip.value, "Your Zip Code (for Billing Address) is a required field!");
	}
	var a = f.PaymentType.length;
	for (var i = 0; i < a; i++) {
		if (f.PaymentType[i].checked) {
			var b = f.PaymentType[i].value;
		}
	}
	if (b != 'eBillMe' && b != 'PAY') {
		sString = f.cc_cardType.options[f.cc_cardType.selectedIndex].value;
		CheckValidNEW(sString, "Credit Card Type is a required field!");
		CheckValidNEW(f.cc_cardOwner.value, "Your Name (as it appears on your credit card) is a required field!");
		CheckValidNEW(f.cardNum.value, "Your Credit Card Number is a required field!");
		CheckValidNEW(f.cc_month.value, "Your Credit Card Expiration Month is a required field!");
		CheckValidNEW(f.cc_year.value, "Your Credit Card Expiration Year is a required field!");
		CheckValidNEW(f.secCode.value, "Your Credit Card Security Code is a required field!");
		}
	
	if (bValid) {
		document.frmProcessOrder.action = '/cart/process/processing2.asp';
		//document.frmProcessOrder.action = 'http://staging.wirelessemporium.com/cart/process/processing2.asp';
		document.frmProcessOrder.submit();
	}
	return false;
}

var bAddress1 = "";
var bAddress2 = "";
var bCity = "";
var bZip = "";
var bState = "";
var bStateIndex = "0";

function ShipToBillPerson(form) {
	if (form.accountid.value == '') {
		if (form.sameaddress.checked) {
			form.bAddress1.value = "";
			form.bAddress2.value = "";
			form.bCity.value = "";
			form.bZip.value = "";
			form.bState.selectedIndex = form.sState.options[0];
		}
		else {
			form.bAddress1.value = form.sAddress1.value;
			form.bAddress2.value = form.sAddress2.value;
			form.bCity.value = form.sCity.value;
			form.bZip.value = form.sZip.value;
			form.bState.selectedIndex = form.sState.selectedIndex;
		}
	}
}

function CurrencyFormatted(amount) {
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}
//  End -->
</script>
<form action="/cart/checkout.asp?l=1" method="post" name="frmProcessOrder">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="padding-bottom:20px;">
    <tr><td valign="top" bgcolor="#FFFFFF" align="center"><img src="/images/cart/we_step2.jpg" width="1000" height="66" border="0" alt="Checkout - Step 2 of 3"></td></tr>
    <tr>
        <td valign="top" bgcolor="#FFFFFF" align="center">
            <input type="hidden" name="shippingTotal" value="">
            <input type="hidden" name="shiptypeID" value="">
            <input type="hidden" name="sWeight" value="<%=sWeight%>">
            
            <input type="hidden" name="sPromoCode" value="<%=sPromoCode%>">
            <input type="hidden" name="nPromo" value="<%=couponid%>">
            
            <input type="hidden" name="nCATax" value="<%=nCATax%>">
            <input type="hidden" name="subTotal" value="<%=nItemTotal%>">
            <input type="hidden" name="grandTotal" value="<%=nAmount%>">
            <input type="hidden" name="buysafeamount" value="<%=round(buysafeamount,2)%>">
            <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
            <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
            <table width="800" border="0" cellspacing="0" cellpadding="0">
                <tr><td colspan="3"><img src="/images/cart/we_header_SecureCheckout.jpg" width="800" height="39" border="0" alt="Secure Checkout"></td></tr>
                <tr>
                    <td colspan="3"><img src="/images/cart/border_top.jpg" width="800" height="7" border="0"></td>
                </tr>
                <tr>
                    <td valign="top" width="7" background="/images/cart/border_left.jpg"><img src="/images/spacer.gif" width="7" height="5" border="0"></td>
                    <td align="center" width="781">
                        <% lineColor = "#666" %>
                        <table width="781" border="0" cellspacing="0" cellpadding="0" style="background-color:#cddeee;">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="3" class="normalText">
                                        <tr>
                                            <td align="left" width="100%" style="border-bottom:2px solid <%=lineColor%>;"><b>Item</b></td>
                                            <td align="center" style="border-bottom:2px solid <%=lineColor%>; border-left:2px solid <%=lineColor%>; border-right:2px solid <%=lineColor%>;"><div style="width:50px; font-weight:bold;">Qty</div></td>
                                            <td align="right" colspan="2" style="border-bottom:2px solid <%=lineColor%>;"><b>Cost</b></td>
                                        </tr>
                                        <%
                                        productArray = split(productList,"@@")
                                        for i = 0 to (ubound(productArray)-1)
                                            curArray = split(productArray(i),"##")
                                        %>
                                        <tr>
                                            <td align="left" style="border-bottom:2px solid <%=lineColor%>; line-height:20px;"><%=curArray(0)%></td>
                                            <td align="center" style="border-bottom:2px solid <%=lineColor%>; border-left:2px solid <%=lineColor%>; border-right:2px solid <%=lineColor%>;"><%=curArray(1)%></td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;" colspan="2"><%=curArray(2)%></td>
                                        </tr>
                                        <%
                                        next
                                        %>
                                        <tr>
                                            <td colspan="2" style="border-right:2px solid <%=lineColor%>; border-bottom:2px solid <%=lineColor%>;">&nbsp;</td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b>Subtotal:</b></td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b><%=formatCurrency(nItemTotal)%></b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="left" style="border-bottom:2px solid <%=lineColor%>; border-right:2px solid <%=lineColor%>;">&nbsp;</td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b>Discount:</b></td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><div style="font-weight:bold;" id="discountAmt"><%=formatCurrency(discountTotal * -1)%></div></td>
                                        </tr>
                                        <%
                                        if WantsBondField = "true" and buysafeamount > 0 then
                                            %>
                                            <tr>
                                                <td colspan="2" style="border-right:2px solid <%=lineColor%>;">&nbsp;</td>
                                                <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b>buySAFE&nbsp;Bond:</b></td>
                                                <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b><%=formatCurrency(buysafeamount)%></b></td>
                                            </tr>
                                            <%
                                        end if
                                        %>
                                        <tr>
                                            <td colspan="2" style="border-right:2px solid <%=lineColor%>;">&nbsp;</td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b>Tax:</b><span id="CAtaxMsg"></span></td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b><span id="CAtax">$0.00</span></b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="border-right:2px solid <%=lineColor%>;">&nbsp;</td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b>Shipping:</b></td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b><span id="shipcost"><%=formatCurrency(shipcost)%></span></b></td>
                                        </tr>
                                        <%
                                        dim nAmount, nCATax
                                        nSubTotal = nItemTotal - discountTotal
                                        if nSubTotal = nItemTotal and discountTotal > 0 then nSubTotal = nSubTotal - discountTotal
                                        nAmount = cDbl(shipcost) + nCATax + nSubTotal + buysafeamount
                                        %>
                                        <tr>
                                            <td align="left" colspan="2" style="border-right:2px solid <%=lineColor%>;">&nbsp;</td>
                                            <td align="right" nowrap="nowrap" style="color:#F00;"><b>Grand Total:</b></td>
                                            <td align="right"><b><span id="GrandTotal" style="color:#F00;"><%=formatCurrency(nAmount)%></span></b></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" width="12" background="/images/cart/border_right.jpg"><img src="/images/spacer.gif" width="12" height="5" border="0"></td>
                </tr>
                <tr>
                    <td colspan="3"><img src="/images/cart/border_bottom.jpg" width="800" height="12" border="0"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <div style="position:relative;">
                <div><img src="/images/cart/we_header_Shipping.jpg" width="800" height="39" border="0" alt="Shipping Information"></div>
                <% if myAccount = "" then %><!--<div style="position:absolute; top:14px; left:380px; font-size:14px;">Already Have An Account? - <a href="javascript:showLoginInfo();">Login Now</a></div>--><% end if %>
            </div>
        </td>
    </tr>
    <tr>
        <td align="center">
            <%if myAccount = "" then%>
            <table id="tbl_step2" width="800" height="465" border="0" cellpadding="3" cellspacing="0" class="smlText" style="background-image: url('/images/cart/bg_border_1.jpg'); background-repeat: no-repeat; background-position: center bottom; padding-left:10px;">
                <tr>
                    <td colspan="5" style="padding-top: 10px; color:#666666; font-weight:bold;" class="normalText" valign="top">
                        <div style="float:left;">&nbsp;&nbsp;Shipping&nbsp;Address:</div>
                    </td>
                </tr>
                <tr>
                    <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Email:</strong></td>
                    <td align="left" colspan="3"><input type="text" name="email" size="30" value="<%=email%>" onchange="getCustomerData(this.value)"></td>
                    <td rowspan="12" width="100%" valign="top" id="shippingDetails" align="right" style="border-left:2px solid #ff6600; padding-right:10px;">
                        <table border="0" cellpadding="0" cellspacing="0" style="padding:5px;">
                            <tr><td align="center"><img src="/images/icons/shippingHeader.jpg" border="0" /></td></tr>
                            <tr>
                                <td align="left">
                                    <input type="hidden" name="shipcost0" value="<%=shipcost0%>"><input type="radio" name="shiptype" id="rad_ship1" value="0" onClick="updateGrandTotal(this.value,'<%=shipcost0%>');"<%if shiptype = "0" or shiptype = "" then response.write " checked"%>><span style="color:#db3300;font-weight:bold;">FREE SHIPPING</span> <span id="Shipping0">USPS First Class (4-10 business days)</span><br>
                                    <input type="hidden" name="shipcost2" value="<%=shipcost2%>"><input type="radio" name="shiptype" id="rad_ship2" value="2" onClick="updateGrandTotal(this.value,'<%=shipcost2%>');"<%if shiptype = "2" then response.write " checked"%>><span id="Shipping2">USPS Priority Mail (2-4 business days) - $6.99</span><br>
                                    <input type="hidden" name="shipcost3" value="<%=shipcost3%>"><input type="radio" name="shiptype" id="rad_ship3" value="3" onClick="updateGrandTotal(this.value,'<%=shipcost3%>');"<%if shiptype = "3" then response.write " checked"%>><span id="Shipping3">USPS Express Mail (1-2 business days) - $19.99</span>
                                    <%
                                    on error resume next
                                    if sZip <> "" then response.write UPScode(sZip,sWeight,shiptype,nTotalQuantity,0,0)
                                    on error goto 0
                                    dim shipcost
                                    shipcost = request.form("shipcost" & request.form("shiptype"))
                                    %>
                                </td>
                            </tr>
                            <tr><td align="center" style="font-weight:bold; padding-top:10px;">Enter your zipcode for more shipping options</td></tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right" width="100"><strong><font color="red" size="+1">*</font>&nbsp;First&nbsp;Name:</strong></td>
                    <td align="left" width="180" colspan="3">
                        <input type="hidden" name="accountid" value="">
                        <input type="text" name="fname" size="30" value="<%=fname%>">
                    </td>
                </tr>
                <tr>
                    <td align="right" width="80"><strong><font color="red" size="+1">*</font>Last&nbsp;Name:</strong></td>
                    <td align="left" width="140" colspan="3"><input type="text" name="lname" size="30" value="<%=lname%>"></td>
                </tr>
                <tr>
                    <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Address:</strong></td>
                    <td align="left" colspan="3"><input type="text" name="sAddress1" size="30" value="<%=saddress1%>"></td>
                </tr>
                <tr>
                    <td align="right">&nbsp;</td>
                    <td align="left" colspan="3"><input type="text" name="sAddress2" size="30" value="<%=saddress2%>"></td>
                </tr>
                <tr>
                    <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;City:</strong></td>
                    <td align="left"><input type="text" name="sCity" size="20" value="<%=scity%>"></td>
                    <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Zip/Postal:</strong></td>
                    <td align="left" style="padding-right:30px;"><input type="text" name="sZip" value="<%=szip%>" size="10" onchange="ajax('/ajax/shippingDetails.asp?zip=' + this.value + '&weight=<%=sWeight%>&shiptype=<%=shiptype%>&nTotalQuantity=<%=itemCnt%>','shippingDetails')"></td>
                </tr>
                <tr>
                    <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;State:</strong></td>
                    <td align="left" colspan="3">
                        <select name="sState" onChange="changeTax(this.value);">
                            <%getStates(sstate)%>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Phone:</strong></td>
                    <td align="left" colspan="3"><input type="text" name="phoneNumber" size="20" value="<%=phone%>"></td>
                </tr>
                <!--
                <tr>
                    <td align="left" colspan="4">
                        <a style="cursor:pointer; font-size:14px; font-weight:bold; color:#ff6c00;" onclick="document.getElementById('enterExclusiveOfferCheckout').style.display='';"><img src="/images/buttons/mobileOffersButton.png" border="0" /></a>
                        <div style="position:relative;">
                            <div style="display:none; position:absolute; top:0px; left:0px; background-color:#FFF; border:2px groove #333; height:375px; overflow:auto;" id="enterExclusiveOfferCheckout"></div>
                            <div style="display:none; position:absolute; top:0px; left:0px;" id="exclusiveOfferCheckout">
                            <table border="0" cellpadding="5" cellspacing="0" style="border:1px solid #000; background-color:#FFF;" width="250">
                                <tr>
                                    <td width="100%" style="background-color:#000; color:#FFF; font-weight:bold; font-size:12px;">Get Exclusive Mobile Offers</td>
                                    <td style="background-color:#000;" align="right"><a onclick="document.getElementById('exclusiveOfferCheckout').style.display='none'" style="cursor:pointer; color:#FFF; font-size:12px; text-decoration:underline;">Close</a></td>
                                </tr>
                                <tr><td colspan="2"><img src="/images/logo_539_50.jpg" border="0" width="320" height="40" /></td></tr>
                                <tr>
                                    <td colspan="2" style="font-size:12px; font-weight:bold;">
                                        Be the first to get special offers on your favorite products, sent to your mobile phone! 
                                        <br /><br />
                                        Enter your mobile # and you can always opt-out any time!
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left:40px;"><input type="text" name="cellNumber" value="" /></td>
                                    <td style="padding-right:40px;" align="left"><input type="button" name="mySub" value="Submit" onclick="return(chkCellNumCheckout(document.frmProcessOrder.cellNumber.value))" /></td>
                                </tr>
                                <tr><td style="font-size:10px; color:#F00; padding-left:40px;">Example: (123)123-4567</td></tr>
                                <tr>
                                    <td colspan="2" style="font-size:10px;">
                                        <strong>Program Terms.</strong> We do not charge for this service, but standard or other charges may apply from your carrier. 
                                        Please check your plan to make sure. To stop receiving text messages at anytime, text STOP to 80676. For help, 
                                        text HELP to 80676 or email <a href="mailto:support@4info.net">support@4info.net</a>. "View rules and conditions 
                                        of GFD sweeps here: <a href="http://bit.ly/GFDsweeps">http://bit.ly/GFDsweeps</a>"
                                    </td>
                                </tr>
                                <tr><td style="background-color:#000;" colspan="2"><div style="height:5px;"></div></td></tr>
                            </table>
                            </div>
                        </div>
                    </td>        
                </tr>
                -->
                <tr id="row_VIPlist">
                    <td align="left" valign="top" colspan="4">
                        <div style="float:left;"><input name="chkOptMail" value="Y" checked="checked" type="checkbox"></div>
                        <div style="float:left; padding-left:10px;">Please notify me via email of exclusive periodic<br />Wireless Emporium coupon code offers*</div>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="4">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="sameaddress" type="checkbox" name="sameaddress" value="yes" OnClick="javascript:showBillingInfo();">&nbsp;&nbsp;<strong>Check if Billing Address is different than the Shipping Address.</strong>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <table id="billing_info">
                            <tr>
                                <td colspan="3" align="left" style="padding-top: 10px; color:#666666; font-weight:bold;" class="normalText" valign="top">&nbsp;&nbsp;Billing&nbsp;Address:</td>
                            </tr>
                            <tr>
                                <td align="right" style="width: 100px;"><strong><font color="red" size="+1">*</font>&nbsp;Address:</strong></td>
                                <td align="left" colspan="3" width="400"><input name="bAddress1" size="30" value="<%=bAddress1%>" type="text"></td>
                                <td rowspan="4" valign="top">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right">&nbsp;</td>
                                <td align="left" colspan="3"><input name="bAddress2" size="30" value="<%=bAddress2%>" type="text"></td>
                            </tr>
                            <tr>
                                <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;City:</strong></td>
                                <td align="left" colspan="3"><input name="bCity" size="30" value="<%=bCity%>" type="text"></td>
                            </tr>
                            <tr>
                                <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;State:</strong></td>
                                <td align="left" colspan="3">
                                    <select name="bState" onChange="changeTax(this.value);">
                                        <%getStates(bState)%>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Zip/Postal:</strong></td>
                                <td align="left" colspan="3"><input name="bZip" size="20" value="<%=bZip%>" type="text"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td colspan="5" style="font-size:10px;">* Wireless Emporium is committed to protecting your privacy. We will never share or rent your information. Please review our privacy policy <a href="/privacy.asp" target="_blank">here</a>.</td></tr>
            </table>
            <% else %>
            <table border="0" cellpadding="0" cellspacing="0" width="800" bgcolor="#FFFFFF">
                <tr>
                    <td colspan="5"><img src="/images/cart/border_top.jpg" width="800" height="7" border="0"></td>
                </tr>
                <tr>
                    <td valign="top" width="7" rowspan="4" background="/images/cart/border_left.jpg"><img src="/images/spacer.gif" width="7" height="5" border="0"></td>
                    <td valign="top" width="390">
                        <table width="390" border="0" cellpadding="4" cellspacing="0" class="normalText">
                            <tr>
                                <td align="left" width="25%"><strong>First Name:</strong></td>
                                <td align="left" width="75%">
                                    <input type="hidden" name="accountid" value="<%=myAccount%>">
                                    <input type="hidden" name="fname" value="<%=fname%>"><%=fname%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="25%"><strong>Last Name:</strong></td>
                                <td align="left" width="75%"><input type="hidden" name="lname" value="<%=lname%>"><%=lname%></td>
                            </tr>
                        </table>
                    </td>
                    <td width="1" bgcolor="#A0B0D1" rowspan="2"><font style="font-size:1px;">&nbsp;</font></td>
                    <td valign="top" width="390">
                        <table width="390" border="0" cellpadding="4" cellspacing="0" class="normalText">
                            <tr>
                                <td align="left" width="20%"><strong>Phone:</strong></td>
                                <td align="left" width="80%"><input type="hidden" name="phoneNumber" value="<%=phone%>"><%=phone%></td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"><strong>Email:</strong></td>
                                <td align="left" width="80%"><input type="hidden" name="email" value="<%=email%>"><%=email%></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" width="12" rowspan="5" background="/images/cart/border_right.jpg"><img src="/images/spacer.gif" width="12" height="5" border="0"></td>
                </tr>
                <tr><td><br></td><td><br></td></tr>
                <%
                a = 1
                call fOpenConn()
                SQL = "SELECT * FROM we_addl_shipping_addr WHERE accountid='" & myAccount & "' ORDER BY id"
                set RS = Server.CreateObject("ADODB.Recordset")
                RS.open SQL, oConn, 0, 1
                %>
                <tr>
                    <td valign="top" class="normalText" width="50%">
                        <font style="color:#FF0000;font-weight:bold;">&nbsp;&nbsp;Shipping&nbsp;Address:&nbsp;&nbsp;</font>
                        <%if not RS.eof then%>
                            <font style="color:#FF0000;font-size:11px;font-weight:normal;"><br>&nbsp;&nbsp;<em>Select&nbsp;An&nbsp;Address&nbsp;To&nbsp;Ship&nbsp;To</em></font>
                        <%end if%>
                    </td>
                    <td width="1" bgcolor="#A0B0D1" rowspan="2"><font style="font-size:1px;">&nbsp;</font></td>
                    <td valign="top" class="normalText" width="50%">
                        <font style="color:#FF0000;font-weight:bold">&nbsp;&nbsp;Billing&nbsp;Address:&nbsp;&nbsp;</font>
                        <font style="color:#FF0000;font-size:11px;font-weight:normal;"><br>&nbsp;&nbsp;<em>Must&nbsp;Match&nbsp;Credit&nbsp;Card&nbsp;Statement&nbsp;Address</em></font>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="300" border="0" cellspacing="0" cellpadding="2" class="normalText">
                            <tr>
                                <td valign="top">
                                    <table width="300" border="0" cellspacing="0" cellpadding="2" class="normalText">
                                        <%if not RS.eof then%>
                                        <tr>
                                            <td class="normalText" colspan="2">
                                                <input type="radio" name="radioAddress" value="<%=a%>" onClick="changeShippingAddress(<%=a%>);"<%if radioAddress = "" or radioAddress = cStr(a) then response.write " checked"%>>&nbsp;<b>Primary Shipping Address</b>
                                            </td>
                                        </tr>
                                        <%end if%>
                                        <tr>
                                            <td>
                                                <div align="left">
                                                    <%=sAddress1%><br>
                                                    <%
                                                    if not isNull(sAddress2) then
                                                        if trim(sAddress2) <> "" then response.write sAddress2 & "<br>"
                                                    end if
                                                    %>
                                                    <%=sCity & ",&nbsp;" & sState & "&nbsp;&nbsp;" & sZip%><br>
                                                </div>
                                            </td>
                                            <td align="right">
                                                <a href="/register/default.asp?pf=modify&orderid=<%=nOrderId%>&shiptype=&promo=">Edit</a>
                                                <input type="hidden" name="sAddress1<%=a%>" value="<%=sAddress1%>">
                                                <input type="hidden" name="sAddress2<%=a%>" value="<%=sAddress2%>">
                                                <input type="hidden" name="sCity<%=a%>" value="<%=sCity%>">
                                                <input type="hidden" name="sState<%=a%>" value="<%=sState%>">
                                                <input type="hidden" name="sZip<%=a%>" value="<%=sZip%>">
                                                <input type="hidden" name="sID<%=a%>" value="0">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <%
                            'BEGIN Additional Shipping Addresses
                            do while not RS.eof
                            a = a + 1
                            %>
                            <tr>
                                <td class="normalText">
                                    <input type="radio" name="radioAddress" value="<%=a%>" onClick="changeShippingAddress(<%=a%>);"<%if radioAddress = cStr(a) then response.write " checked"%>>&nbsp;<b>Shipping Address #<%=a%></b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="300" border="0" cellspacing="0" cellpadding="2" class="normalText">
                                        <tr>
                                            <td>
                                                <div align="left">
                                                    <%=RS("sAddress1")%><br>
                                                    <%
                                                    if not isNull(RS("sAddress2")) then
                                                        if trim(RS("sAddress2")) <> "" then response.write RS("sAddress2") & "<br>"
                                                    end if
                                                    %>
                                                    <%=RS("sCity") & ",&nbsp;" & RS("sState") & "&nbsp;&nbsp;" & RS("sZip")%><br>
                                                </div>
                                            </td>
                                            <td align="right">
                                                <a href="/register/default.asp?pf=modify&orderid=<%=nOrderId%>&shiptype=&promo=">Edit</a>
                                                <div align="right">
                                                    <input type="hidden" name="sAddress1<%=a%>" value="<%=RS("sAddress1")%>">
                                                    <input type="hidden" name="sAddress2<%=a%>" value="<%=RS("sAddress2")%>">
                                                    <input type="hidden" name="sCity<%=a%>" value="<%=RS("sCity")%>">
                                                    <input type="hidden" name="sState<%=a%>" value="<%=RS("sState")%>">
                                                    <input type="hidden" name="sZip<%=a%>" value="<%=RS("sZip")%>">
                                                    <input type="hidden" name="sID<%=a%>" value="<%=RS("id")%>">
                                                    <%
                                                    if radioAddress = cStr(a) then
                                                        sAddress1 = RS("sAddress1")
                                                        sAddress2 = RS("sAddress2")
                                                        sCity = RS("sCity")
                                                        sState = RS("sState")
                                                        sZip = RS("sZip")
                                                    end if
                                                    %>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <%
                            RS.movenext
                            loop
                            RS.close
                            set RS = nothing
                            call fCloseConn()
                            'END Additional Shipping Addresses
                            %>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="300" border="0" cellspacing="0" cellpadding="2" class="normalText">
                            <tr>
                                <td valign="top" align="left">
                                    <br>
                                    <input type="hidden" name="bAddress1" value="<%=bAddress1%>"><%=bAddress1%><br>
                                    <input type="hidden" name="bAddress2" value="<%=bAddress2%>">
                                        <%
                                        if not isNull(bAddress2) then
                                            if trim(bAddress2) <> "" then response.write bAddress2 & "<br>"
                                        end if
                                        %>
                                    <input type="hidden" name="bCity" value="<%=bCity%>"><%=bCity%>,
                                    <input type="hidden" name="bState" value="<%=bState%>"><%=bState%>&nbsp;&nbsp;
                                    <input type="hidden" name="bZip" value="<%=bZip%>"><%=bZip%>
                                    
                                    <input type="hidden" name="sAddress1" value="<%=sAddress1%>">
                                    <input type="hidden" name="sAddress2" value="<%=sAddress2%>">
                                    <input type="hidden" name="sCity" value="<%=sCity%>">
                                    <input type="hidden" name="sState" value="<%=sState%>">
                                    <input type="hidden" name="sZip" value="<%=sZip%>">
                                </td>
                                <td align="right">
                                    <a href="/register/default.asp?pf=modify&orderid=<%=nOrderId%>&shiptype=&promo=">Edit</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5"><img src="/images/cart/border_bottom.jpg" width="800" height="12" border="0"></td>
                </tr>
            </table>
            <% end if %>
        </tr>
    </tr>
    <tr>
        <td width="100%" align="center" valign="top"><img src="/images/cart/we_header_Payment.jpg" width="800" height="39" border="0" alt="Payment Information"></td>
    </tr>
    <tr>
        <td class="normalText" align="center" valign="top" width="800">
            <table width="780" height="250" border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td colspan="5"><img src="/images/cart/border_top.jpg" width="800" height="7" border="0"></td>
                </tr>
                <tr>
                    <td valign="top" width="12" style="background-image:url(/images/cart/border_left.jpg); background-repeat:repeat-y;" rowspan="2"><img src="/images/spacer.gif" width="12" height="5" border="0"></td>
                    <td width="376" align="left" valign="top" class="normalText" style="padding-top:10px;">
                        <table width="100%" border="0" cellpadding="2" cellspacing="0" align="center">
                            <tr>
                                <td valign="top" align="left" colspan="2">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                                        <tr><td valign="top" align="center"><img src="/images/cart/we_cards.gif" alt="We accept Visa, MasterCard, American Express and Discover" border="0" align="absbottom"></td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"><b>Card&nbsp;Type:&nbsp;</b></td>
                                <td>
                                    <select name="cc_cardType">
                                        <option value=""></option>
                                        <option value="VISA">VISA</option>
                                        <option value="MC">MASTER CARD</option>
                                        <option value="AMEX">AMERICAN EXPRESS</option>
                                        <option value="DISC">DISCOVER</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td align="right"><b>&nbsp;Card&nbsp;Holder&nbsp;Name:&nbsp;</b></td><td><input name="cc_cardOwner" type="text" size="23"></td></tr>
                            <tr><td align="right"><b>Credit&nbsp;Card&nbsp;#:&nbsp;</b></td><td><input name="cardNum" type="text" id="cardNum" size="23" maxlength="16"></td></tr>
                            <tr><td align="right">&nbsp;</td><td><span style="size: 7pt;">(no&nbsp;spaces/dashes<br>example:&nbsp;1234123412341234)</span></td></tr>
                            <tr><td align="right"><b>Security&nbsp;Code:&nbsp;</b></td><td><input name="secCode" type="text" id="secCode" size="4" maxlength="4">&nbsp;&nbsp;<a href="/cart/cvv2help.asp" target="_blank">(Help on Security Code)</a></td></tr>
                            <tr>
                                <td align="right"><b>Expiration&nbsp;Date:&nbsp;</b></td>
                                <td>
                                    <select name="cc_month">
                                        <option value="01">01 (January)</option>
                                        <option value="02">02 (February)</option>
                                        <option value="03">03 (March)</option>
                                        <option value="04">04 (April)</option>
                                        <option value="05">05 (May)</option>
                                        <option value="06">06 (June)</option>
                                        <option value="07">07 (July)</option>
                                        <option value="08">08 (August)</option>
                                        <option value="09">09 (September)</option>
                                        <option value="10">10 (October)</option>
                                        <option value="11">11 (November)</option>
                                        <option value="12">12 (December)</option>
                                    </select>
                                    <select name="cc_year">
                                        <%
                                        dim countYear
                                        for countYear = 0 to 14
                                            response.write "<option value=""" & right(cStr(year(date) + countYear),2) & """>" & cStr(year(date) + countYear) & "</option>" & vbcrlf
                                        next
                                        %>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="56" align="center" valign="middle" height="100%"><img src="/images/cart/or_bar_250.jpg" width="56" height="250" border="0"></td>
                    <td width="376" align="center" valign="top" height="100%">
                        <table width="100%" border="0" cellpadding="2" cellspacing="0" align="center">
                            <tr>
                                <td align="left" valign="middle"><input type="radio" name="PaymentType" value="eBillMe"></td>
                                <td align="left" valign="middle" class="smltext"><p><img src="/cart/process/eBillme/images/logo_eBillme.jpg" width="147" height="47" border="0" alt="Payment by eBillme"></p></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td align="left" valign="top" class="smltext">
                                    <div style="float:left; padding:0px 10px 0px 0px;"><img src="/images/ebillMe/10Perc_120x90.png" border="0" width="120" height="90" /></div>
                                    <div>
                                        With eBillMe you can get cash back on your order today!
                                        Simply pay cash with your online banking. eBillme allows you to pay for your order through your bank's online bill pay system. Free Buyer Protection included with every order.
                                        <br>
                                        <a href="https://www.ebillme.com/index.php/learnmore2/wirelessemporium" target="_blank">View Demo</a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" width="12" style="background-image:url(/images/cart/border_right.jpg); background-repeat:repeat-y;" rowspan="2"><img src="/images/spacer.gif" width="12" height="5" border="0"></td>
                </tr>
                <tr>
                    <td colspan="3" align="center" style="padding-top:10px; padding-bottom:20px;">
                        <input type="image" onclick="return CheckSubmit();" src="/images/cart/place_order_button.jpg" width="272" height="56" alt="Place My Order">
                        <input type="hidden" name="submitted" value="submitted">
                    </td>
                </tr>
                <tr>
                    <td colspan="5"><img src="/images/cart/border_bottom.jpg" width="800" height="12" border="0"></td>
                </tr>
            </table>
            <!--WE variables-->
			<%=WEhtml2%>
            <!--eBillme variables-->
            <%=EBtemp%>
            <input type="hidden" name="myBasketXML" value="<%=EBxml%>">
            <input type="hidden" name="ssl_ProdIdCount" value="<%=nProdIdCount%>">
        </td>
    </tr>
</table>
</form>
<!--#include virtual="/includes/template/bottom_cart.asp"-->
<form name="googleForm" method="post" action="/cart/process/google/CartProcessing.asp">
<!--<form method="post" action="/cart/process/google/CartProcessing.asp">&#150;&nbsp;or&nbsp;checkout&nbsp;with&nbsp;&#150;&nbsp;&nbsp;&nbsp;<br>-->
    <!--<input type="image" name="Checkout" alt="Google Checkout" src="https://checkout.google.com/buttons/checkout.gif?merchant_id=151073323883147&w=168&h=44&style=white&variant=text&loc=en_US" height="44" width="168">-->
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <input type="hidden" name="gift" value="<%=sGiftCert%>">
    <input type="hidden" name="analyticsdata" value="">
    <input type="hidden" name="shipZip" value="<%=sZip%>">
    <input type="hidden" name="sWeight" value="<%=cStr(sWeight)%>">
    <input type="hidden" name="buysafeamount" value="<%=buysafeamount%>">
    <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
    <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
    <%=WEhtml2%>
</form>
<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.asp">
<!--<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.asp">-->
    <input type="hidden" name="numItems" value="<%=nProdIdCount%>">
    <input type="hidden" name="paymentAmount" value="<%=nSubTotal%>">
    <input type="hidden" name="ItemsAndWeight" value="<%=cStr(nProdIdCount) & "|" & cStr(sWeight)%>">
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <input type="hidden" name="gift" value="<%=sGiftCert%>">
    <input type="hidden" name="shipZip" value="<%=sZip%>">
    <input type="hidden" name="buysafeamount" value="<%=buysafeamount%>">
    <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
    <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
    <%=PPhtml%>
    <!--<input type="image" name="submit" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif">-->
</form>
<script language="javascript">
	function enterPromo() {
		document.getElementById("promoLink").style.display = 'none'
		document.getElementById("promoCode").style.display = ''
	}
	
	function checkoutSelected(val) {
		if (val == "pp") {
			document.PaypalForm.submit()
		}
		else if (val == "cc") {
			document.getElementById("ccOrder").style.display = ''
			window.location = "#ccOrder"
		}
		else if (val == "gc") {
			document.googleForm.submit()
		}
		else if (val == "eb") {
			document.getElementById("ccOrder").style.display = ''
			window.location = "#ccOrder"
			document.frmProcessOrder.PaymentType[1].checked = true
		}
	}
	
	function getCustomerData(email) {
		ajax('/ajax/customerData.asp?email=' + email,'dumpZone')
		setTimeout("displayData(document.getElementById('dumpZone').innerHTML)",500)
	}
	function displayData(dataReturn) {
		if (dataReturn != "no data") {
			document.getElementById("popUpBG").style.display = ''
			document.getElementById("popUpBox").style.display = ''
			var dataArray = dataReturn.split("@@")
			if (document.frmProcessOrder.fname.value == "") { document.frmProcessOrder.fname.value = dataArray[0] }
			if (document.frmProcessOrder.lname.value == "") { document.frmProcessOrder.lname.value = dataArray[1] }
			if (document.frmProcessOrder.sAddress1.value == "") { document.frmProcessOrder.sAddress1.value = dataArray[2] }
			if (document.frmProcessOrder.sAddress2.value == "") { document.frmProcessOrder.sAddress2.value = dataArray[3] }
			if (document.frmProcessOrder.sCity.value == "") { document.frmProcessOrder.sCity.value = dataArray[4] }
			if (document.frmProcessOrder.sState.selectedIndex == 0) {
				for (i=0;i<50;i++) {
					if (document.frmProcessOrder.sState.options[i].value == dataArray[5]) {
						document.frmProcessOrder.sState.selectedIndex = i
						changeTax(dataArray[5])
					}
				}
			}
			if (document.frmProcessOrder.sZip.value == "") {
				document.frmProcessOrder.sZip.value = dataArray[6]
				ajax('/ajax/shippingDetails.asp?zip=' + dataArray[6] + '&weight=<%=sWeight%>&shiptype=<%=shiptype%>&nTotalQuantity=<%=itemCnt%>','shippingDetails')
			}
			if (document.frmProcessOrder.phoneNumber.value == "") { document.frmProcessOrder.phoneNumber.value = dataArray[7] }
			
			if (dataArray[2] != dataArray[8] || dataArray[6] != dataArray[12]) {
				document.frmProcessOrder.sameaddress.checked = true
				showBillingInfo()
				document.frmProcessOrder.bAddress1.value = dataArray[8]
				document.frmProcessOrder.bAddress2.value = dataArray[9]
				document.frmProcessOrder.bCity.value = dataArray[10]
				for (i=0;i<50;i++) {
					if (document.frmProcessOrder.bState.options[i].value == dataArray[11]) {
						document.frmProcessOrder.bState.selectedIndex = i
					}
				}
				document.frmProcessOrder.bZip.value = dataArray[12]
			}
			document.frmProcessOrder.accountid.value = dataArray[13]
		}
	}
</script>