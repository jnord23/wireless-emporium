<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/template/top_cart.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
	<tr>
    	<td align="center" style="padding:50px;">
        	<h3>This payment method is currently not available. Please try again later.</h3>
			<%
            If Request("requestxmlencrypted") = "yes" Then

                Dim sPlain, sKey, sBase64
                Dim myRijndael

                sKey = WE_WUPAY_SKEY
                sBase64 = Request("responsexml")
                Set myRijndael = Server.CreateObject("RijndaelCOM.RijndaelAES")
                sPlain = myRijndael.Decrypt(sBase64, sKey)

				cdo_to = "webmaster@wirelessemporium.com,terry@wirelessemporium.com,ruben@wirelessemporium.com"
'				cdo_to = "terry@wirelessemporium.com"
				cdo_from = "service@wirelessemporium.com"
				cdo_subject = "*** WUPay payment error ***"
				cdo_body = sPlain
				CDOSend cdo_to,cdo_from,cdo_subject,cdo_body				

                Set myRijndael = Nothing
            End If
            %>        
            <a href="/cart/basket.asp">Go back to the cart page</a>
        </td>
	</tr>
</table>
<!--#include virtual="/includes/template/bottom_cart.asp"-->