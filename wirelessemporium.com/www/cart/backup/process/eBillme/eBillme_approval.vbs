Dim oConn, SQL, dateStart, bodyText
Set oConn = CreateObject("ADODB.Connection")
oConn.ConnectionString = "Driver={SQL Native Client};Server=wnn510.wirelessemporium.com\WEDB01,1433;Database=wirelessemp_db;UID=wireless_user;PWD=c3llph0n3"
oConn.ConnectionTimeout = 1000		'seconds
oConn.Open

const SEB_PAYEE = "5JDv4l9BVkJPNbziOEJvow=="
const SEB_USERNAME = "pW1re@dn1n"
const SEB_PASSWORD = "1e$sEmp0"
dim seb, r, REFERENCE_ID, extOrderNumber
set seb = CreateObject("SeB.SeBImpl")
r = seb.getPaymentDetails(SEB_PAYEE, SEB_USERNAME, SEB_PASSWORD)

Set xmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
xmlDoc.async = false
xmlDoc.loadXml r

Set RootNode = xmlDoc.documentElement

Set PaymentNode = RootNode.selectNodes("collection/payment")
for a = 0 to PaymentNode.Length - 1
	OrderNumber = GetElementText(PaymentNode(a),"ordernumber")
	PaymentStatus = GetElementText(PaymentNode(a),"paymentstatus")
	bEmail = GetElementText(PaymentNode(a),"buyerdetails/email")
	if PaymentStatus = "F" then
		'make sure the order number is a real order
		SQL = "SELECT orderid FROM we_orders WHERE orderid='" & OrderNumber & "'"
		Set RS = CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 0, 1
		'if the order is real then process inventory
		if not RS.eof then
			SQL = "UPDATE we_orders SET approved=-1, emailSent='yes' WHERE orderid='" & OrderNumber & "'"
			oConn.execute SQL
			
			SQL = "SELECT a.itemid, a.quantity, b.ItemKit_NEW FROM we_orderdetails a left join we_items b on a.itemID = b.itemID WHERE a.orderid='" & OrderNumber & "'"
			Set RSinv = CreateObject("ADODB.Recordset")
			RSinv.open SQL, oConn, 0, 1
			'loop through each item in the order
			do until RSinv.eof
				dim decreaseSql, nProdIdCheck, nProdQuantity, nPartNumber, kit
				nProdIdCheck = RSinv("itemid")
				nProdQuantity = RSinv("quantity")
				kit = RSinv("ItemKit_NEW")
				if isNull(kit) then
					'grab the single items master id and master qty
					SQL = "SELECT a.PartNumber, b.itemID as masterID, b.inv_qty FROM we_items a left join we_items b on a.partNumber = b.partNumber and b.master = 1 WHERE a.itemID = '" & nProdIdCheck & "'"
				else
					'grab the master id and master qty for each item in the kit
					SQL = "SELECT a.PartNumber, b.itemID as masterID, b.inv_qty FROM we_items a left join we_items b on a.partNumber = b.partNumber and b.master = 1 WHERE a.itemID IN (" & kit & ")"
				end if
				set RS = oConn.execute(SQL)
				'loop through all items in kit, or single item if not in kit
				do while not RS.EOF
					nPartNumber = RS("PartNumber")
					masterID = RS("masterID")
					'adjust inventory
					decreaseSql = "UPDATE we_items SET inv_qty = CASE WHEN (inv_qty - " & nProdQuantity & " > 0) THEN inv_qty - " & nProdQuantity & " ELSE 0 END WHERE PartNumber = '" & nPartNumber & "' AND master = 1"
					oConn.execute(decreaseSql)
					'save inventory adjustment
					On Error Resume Next
					sql = "if not (select count(*) from we_invRecord where itemID = '" & nProdIdCheck & "' and orderID = '" & OrderNumber & "') > 0 insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & nProdIdCheck & "," & RS("inv_qty") & "," & nProdQuantity & "," & OrderNumber & ",0,'WE eBillMe Customer Order','" & now & "')"
					session("errorSQL") = sql
					oConn.execute(sql)
					On Error Goto 0
					'set number of sales for select item
					sqlQuery = "UPDATE we_items SET numberOfSales = numberOfSales + " & nProdQuantity & " WHERE itemID = '" & nProdIdCheck & "'"
					oConn.Execute(SQL)
					
					RS.movenext
				loop
				RSinv.movenext
			loop
		else
			cdo_body = "WE Order Number " & OrderNumber & " not found for eBillme order!"
			cdo_to = "webmaster@wirelessemporium.com"
			cdo_from = "service@wirelessemporium.com"
			cdo_subject = "eBillme Order Not Found!"
			CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		end if
	end if
next

set seb = nothing

Function GetElementText(Node, Tagname)
	Dim NodeList
	Set NodeList = Node.getElementsByTagname(Tagname)
	If NodeList.Length > 0 Then
		For b = 0 To NodeList.Length - 1
			Set CurrNode = NodeList.nextNode
			GetElementText = CurrNode.text
		Next
	Else
		GetElementText = ""
	End If
	Set NodeList = Nothing
End Function

'  FUNCTION FROM <!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
FUNCTION CDOSend(strTo,strFrom,strSubject,strBody)
	Dim objErrMail
	Set objErrMail = CreateObject("CDO.Message")
	With objErrMail
		.From = strFrom
		.To = strTo
		'.bcc = "webmaster@wirelessemporium.com"
		.Subject = strSubject
		.HTMLBody = CStr("" & strBody)
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Update
		.Send
	End With
	Set objErrMail = nothing
END FUNCTION
