<%
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

noCommentBox = true

dim curSite
'curSite = "http://staging"
curSite = "https://www"
'response.write request.form & "<br>"
'response.end
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<script>
	function myfunc () {
		var frm = document.getElementById("frmExample");
		frm.submit();
	}
	window.onload = myfunc;
</script>
<%
    HTTP_REFERER = request.servervariables("HTTP_REFERER")
    if instr(lcase(HTTP_REFERER), "wirelessemporium.com/cart/checkout.asp") = 0 and instr(lcase(HTTP_REFERER), "wirelessemporium.com/cart/process/ebillme/ebillme.asp") = 0 then
        response.write "<h3>This page will only accept forms submitted from the Wireless Emporium secure website.</h3>" & vbcrlf
    else	
        shipAmt = request.form("shipping")
        if isnull(shipAmt) or len(shipAmt) < 1 then shipAmt = 0
        myBasketXML = request.form("myBasketXML")
		nAccountId = request.form("nAccountId")
        nSubTotal = formatNumber(request.form("amount"),2)
        nGrandTotalOrder = formatNumber(request.form("nAmount"),2)
        sShipRate = formatNumber(shipAmt,2)
        shiptypeid = prepInt(request.form("shiptype"))
        sShipType = "First Class"
        
        sql = 	"select	top 1 b.shipdesc" & vbcrlf & _
                "from	XSiteShippingCost a join xshiptype b" & vbcrlf & _
                "	on	a.shiptypeid = b.shiptypeid" & vbcrlf & _
                "where	a.site_id = 0" & vbcrlf & _
                "	and	a.shippingid = '" & shiptypeid & "'" & vbcrlf & _
                "	and	a.international = 0" & vbcrlf & _
                "	and	a.includePhones = 0"
        session("errorSQL") = sql
        set shipRS = oConn.execute(sql)
        if not shipRS.eof then sShipType = shipRS("shipdesc")
        
        sTax = formatNumber(request.form("tax"),2)
        nBuysafeamount = formatNumber(request.form("buysafeamount"),2)
        shippingid = request.form("ShippingAddressID")
        if shippingid = "" then shippingid = "0"
        nCouponid = request.form("nCouponid")
        if nCouponid = "" then nCouponid = "0"
        sGiftCert = request.form("sGiftCert")
        
        if cDbl(nBuysafeamount) > 0 then nGrandTotalOrder = cDbl(nGrandTotalOrder) - cDbl(nBuysafeamount)
        
        SQL = "SELECT * FROM we_accounts WHERE accountid = '" & nAccountId & "'"
        set RS = Server.CreateObject("ADODB.Recordset")
        RS.open SQL, oConn, 0, 1
        if not RS.eof then
            fname = RS("fname")
            lname = RS("lname")
            bAddress1 = RS("bAddress1")
            bAddress2 = RS("bAddress2")
            bCity = RS("bCity")
            bState = RS("bState")
            bZip = RS("bZip")
            phone = prepStr(RS("phone"))
            if phone = "" then phone = request("phoneNumber")
            email = RS("email")
            if shippingid <> 0 then
                SQL2 = "SELECT * FROM we_addl_shipping_addr WHERE id = '" & shippingid & "'"
                set RS2 = Server.CreateObject("ADODB.Recordset")
                RS2.open SQL2, oConn, 3, 3
                if not RS2.eof then
                    sAddress1 = RS2("sAddress1")
                    sAddress2 = RS2("sAddress2")
                    sAddress = RS2("sAddress1")
                    if not isNull(RS2("sAddress2")) and RS2("sAddress2") <> "" then sAddress = sAddress & ", " & RS2("sAddress2")
                    sCity = RS2("sCity")
                    sState = RS2("sState")
                    sZip = RS2("sZip")
                else
                    sAddress1 = RS2("sAddress1")
                    sAddress2 = RS2("sAddress2")
                    sAddress = RS("sAddress1")
                    if not isNull(RS("sAddress2")) and RS("sAddress2") <> "" then sAddress = sAddress & ", " & RS("sAddress2")
                    sCity = RS("sCity")
                    sState = RS("sState")
                    sZip = RS("sZip")
                end if
            else
                sAddress1 = RS("sAddress1")
                sAddress2 = RS("sAddress2")
                sAddress = RS("sAddress1")
                if not isNull(RS("sAddress2")) and RS("sAddress2") <> "" then sAddress = sAddress & ", " & RS("sAddress2")
                sCity = RS("sCity")
                sState = RS("sState")
                sZip = RS("sZip")
            end if
            bCountryCode = "US"
            if instr("AB|BC|MB|NB|NL|NT|NS|NU|ON|PE|QC|SK|YT|", bState & "|") > 0 then bCountryCode = "CA"
            sCountryCode = "US"
            if instr("AB|BC|MB|NB|NL|NT|NS|NU|ON|PE|QC|SK|YT|", sState & "|") > 0 then sCountryCode = "CA"
        end if
        
        if trim(lCase(request.form("promotionalCode"))) = "ebillme2010" then
            if cDbl(nSubTotal) >= 50 then
                nGrandTotalOrder = cDbl(nGrandTotalOrder) - 5
            end if
        end if
        
        SQL = "sp_InsertOrder2 '" & nAccountId & "','" & nSubTotal & "','" & sShipRate & "','" & sTax & "','" & nGrandTotalOrder & "','" & SQLquote(sShipType) & "',null,'" & shippingid & "','" & nCouponid & "','" & now & "'"
        session("errorSQL") = SQL
        nOrderId = oConn.execute(SQL).fields(0).value
        session("invoice") = nOrderId
        
        if trim(lCase(request.form("promotionalCode"))) = "ebillme2010" then
            if cDbl(nSubTotal) >= 50 then
                SQL = "INSERT INTO we_ebillme_promotions (orderid,promoCode,GrandTotalOrder,GrandTotalDiscount,DateTimeEntd) VALUES ('" & nOrderId & "','ebillme2010','" & cDbl(nGrandTotalOrder) + 5 & "','" & nGrandTotalOrder & "','" & now & "')"
                session("errorSQL") = SQL
            end if
            oConn.execute SQL
        end if
        
        dim strItems
        strItems = ""
        for itemCount = 1 to request.form("ssl_ProdIdCount")
            strItems = strItems & request.form("ssl_item_number_" & itemCount) & ","
            detailSQL = "INSERT INTO we_orderdetails (orderid,itemid,quantity) VALUES ('" & nOrderId & "','" & request.form("ssl_item_number_" & itemCount) & "','" & request.form("ssl_item_qty_" & itemCount) & "')"
            session("errorSQL") = detailSQL
            oConn.execute detailSQL
        next
        strItems = left(strItems,len(strItems)-1)

        myXML = 	"<?xml version=""1.0"" encoding=""utf-8"" ?>" & vbcrlf & _
                    "<submitorder_request xmlns=""http://ebillme.com/ws/v3"">" & vbcrlf & _
                        "<apiversion>3.0</apiversion>" & vbcrlf & _
                        "<transtype>P</transtype>"  & vbcrlf & _
                        "<authorization>"  & vbcrlf & _
                            "<merchanttoken>" & WE_WUPAY_MERCHANT_TOKEN & "</merchanttoken>"  & vbcrlf & _
                        "</authorization>"  & vbcrlf & _
                        "<order>"  & vbcrlf & _
                            "<ordernumber>" & nOrderId & "</ordernumber>"  & vbcrlf & _
                            "<totalprice>" & nGrandTotalOrder & "</totalprice>"  & vbcrlf & _
                            "<currency>USD</currency>"  & vbcrlf & _
                            "<commandtype>_wTrans</commandtype>"  & vbcrlf & _
                            "<ipaddress>" & request.ServerVariables("REMOTE_ADDR") & "</ipaddress>"  & vbcrlf & _
                            "<sessionid>" & Session.SessionID & "</sessionid>"  & vbcrlf & _
                            "<buyerdetails>"  & vbcrlf & _
                                "<firstname>" & XMLchars(fname) & "</firstname>" & vbcrlf & _
                                "<lastname>" & XMLchars(lname) & "</lastname>" & vbcrlf & _
                                "<email>" & email & "</email>" & vbcrlf & _
                                "<address1>" & XMLchars(bAddress1) & "</address1>" & vbcrlf & _
                                "<address2>" & XMLchars(bAddress2) & "</address2>" & vbcrlf & _
                                "<city>" & XMLchars(bCity) & "</city>" & vbcrlf & _
                                "<state>" & bState & "</state>" & vbcrlf & _
                                "<country>" & bCountryCode & "</country>" & vbcrlf & _
                                "<zipcode>" & bZip & "</zipcode>" & vbcrlf & _
                                "<phone1>" & XMLchars(phone) & "</phone1>" & vbcrlf & _
                                "<merchantrating>1</merchantrating>" & vbcrlf & _
                            "</buyerdetails>"  & vbcrlf & _
                            "<shippingdetails>"  & vbcrlf & _
                                "<firstname>" & XMLchars(fname) & "</firstname>" & vbcrlf & _
                                "<lastname>" & XMLchars(lname) & "</lastname>" & vbcrlf & _
                                "<email>" & email & "</email>" & vbcrlf & _
                                "<address1>" & XMLchars(sAddress1) & "</address1>" & vbcrlf & _
                                "<address2>" & XMLchars(sAddress2) & "</address2>" & vbcrlf & _
                                "<city>" & XMLchars(sCity) & "</city>" & vbcrlf & _
                                "<state>" & sState & "</state>" & vbcrlf & _
                                "<country>" & sCountryCode & "</country>" & vbcrlf & _
                                "<zipcode>" & sZip & "</zipcode>" & vbcrlf & _
                                "<shippingmethod>STANDARD</shippingmethod>" & vbcrlf & _
                            "</shippingdetails>"  & vbcrlf & _
                            "<itemdetails>" & vbcrlf & _
                                myBasketXML & vbcrlf & _
                            "</itemdetails>" & vbcrlf & _
                        "</order>" & vbcrlf & _
                    "</submitorder_request>"
'								response.write myXML
'								response.end
        Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
        objXmlDoc.Async = False
        objXmlDoc.setProperty "SelectionLanguage", "XPath"
        
        objXmlDoc.loadXML myXML
        objXmlDoc.setProperty "SelectionNamespaces", "xmlns:ns='http://ebillme.com/ws/v3'"
        
        Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitorder_request/ns:authorization/ns:merchanttoken")
        objXmlNode.text = WE_WUPAY_MERCHANT_TOKEN
        
        Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitorder_request/ns:order/ns:ordernumber")
        objXmlNode.text = nOrderId
        
        Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitorder_request/ns:order/ns:ipaddress")
        objXmlNode.text = request.ServerVariables("REMOTE_ADDR")
        
        Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitorder_request/ns:order/ns:sessionid")
        objXmlNode.text = Session.SessionID
        
        sPlain = objXmlDoc.documentElement.xml
        
        Set objXmlDoc = Nothing
        Set objXmlNode = Nothing
        
        sKey = WE_WUPAY_SKEY
        
        Set myRijndael = Server.CreateObject("RijndaelCOM.RijndaelAES")
        sBase64 = myRijndael.Encrypt(sPlain, sKey)
        Set myRijndael = Nothing

'								Set XmlHttp = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
'								xmlHttp.open "POST", "https://wupaygateway.com/axis/SeBImpl.jws", False
'								xmlHttp.send(sBase64)
'								response.write xmlHttp.responseText
'								response.end
        %>
        <form id="frmExample" action="https://wupaygateway.com/portal/" method="post">
            <input id="form" name="form" value="frmExample" type="hidden" />
            <input name="returnUrl" value="http://www.wirelessemporium.com/cart/process/wupay/return.asp" type="hidden" />
            <input name="cancelUrl" value="http://www.wirelessemporium.com/cart/basket.asp" type="hidden" />
            <input name="errorUrl" value="http://www.wirelessemporium.com/cart/process/wupay/error.asp" type="hidden" />
            <input name="requestxmlencrypted" value="yes" type="hidden" />
            <input name="requestxml" value="<%=sBase64%>" type="hidden" />
        </form>
        <%
    end if

Function XMLchars(str)
	if not isNull(str) then
		XMLchars = replace(replace(replace(str,"&","&amp;"),chr(34),"&quot;"),"'","&apos;")
		if inStr(str,"<itemdetails>") = 0 then XMLchars = replace(replace(XMLchars,">","&gt;"),"<","&lt;")
	else
		XMLchars = str
	end if
End Function
%>
