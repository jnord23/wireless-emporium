<%
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

'TEST CUSTOMER PROCESS IN CHECKOUT
on error resume next
if len(request.Cookies("checkoutPage")) > 0 then
	if request.Cookies("checkoutPage") = 1 then
		response.Cookies("checkoutPage") = 2
	end if
end if
on error goto 0

shiptozip = request.form("shipZip")
dim mobileOrder : mobileOrder = request.Form("mobileOrder")

if not isnumeric(mobileOrder) or isnull(mobileOrder) or len(mobileOrder) < 1 then mobileOrder = 0

dim HTTP_REFERER
HTTP_REFERER = request.servervariables("HTTP_REFERER")
if instr(HTTP_REFERER,"https://m.wirelessemporium.com") < 1 and instr(HTTP_REFERER,"wirelessemporium.com/cart/") < 1  then
	response.write "<h2>This page will only accept forms submitted from the Wireless Emporium secure website.</h2>" & vbcrlf
	response.Write("<!-- " & HTTP_REFERER & " -->")
	response.end
end if
%>
<!--#include virtual="/cart/process/PaypalSOAP/Constants.asp"-->
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_GetMySession.asp"-->
<!--#include virtual="/cart/includes/inc_makeNum.asp"-->
<%
dim paymentAmount, ItemsAndWeight, itemsArray, a, numItems, sWeight, basketitem, ItemStr
paymentAmount = request.Form("paymentAmount")
ItemsAndWeight = request.form("ItemsAndWeight")
itemsArray = split(ItemsAndWeight,"|")
ShippingTotal = request.form("ShippingTotal")

for a = 0 to uBound(itemsArray)
	if a = 0 then
		numItems = cDbl(itemsArray(a))
	else
		sWeight = cDbl(itemsArray(a))
	end if
next
ItemsAndWeight = ItemsAndWeight & "|" & request.form("shipZip")

for basketitem = 0 to numItems - 1
	ItemStr = ItemStr & "						<PaymentDetailsItem>" & vbcrlf
	ItemStr = ItemStr & "							<Name>" & XMLencode(request.form("L_NAME" & basketitem)) & "</Name>" & vbcrlf
	ItemStr = ItemStr & "							<Number>" & XMLencode(request.form("L_NUMBER" & basketitem)) & "</Number>" & vbcrlf
	ItemStr = ItemStr & "							<Quantity>" & XMLencode(request.form("L_QTY" & basketitem)) & "</Quantity>" & vbcrlf
	ItemStr = ItemStr & "							<Amount currencyID=""USD"">" & XMLencode(request.form("L_AMT" & basketitem)) & "</Amount>" & vbcrlf
	ItemStr = ItemStr & "						</PaymentDetailsItem>" & vbcrlf
next

call fOpenConn()
redim itemType(numItems), itemQty(numItems), itemPrice(numItems)
for basketitem = 0 to numItems - 1
	nProdIdCheck = makeNum(request.form("L_NUMBER" & basketitem))
	nProdQuantity = makeNum(request.form("L_QTY" & basketitem))
	set RS = server.createobject("ADODB.recordset")
	if nProdIdCheck > 999999 then
		SQL = "SELECT 3 as typeID, a.price_we as price_our, b.customCost FROM we_items_musicSkins a left join shoppingCart b on a.id = b.itemID and b.store = 0 AND b.sessionID = '" & mySession & "' WHERE a.id='" & nProdIdCheck & "'"
	else
		SQL = "SELECT a.typeID, a.price_OUR, b.customCost FROM we_items a left join shoppingCart b on a.itemID = b.itemID and b.store = 0 AND b.sessionID = '" & mySession & "' WHERE a.itemID='" & nProdIdCheck & "'"
	end if
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		itemType(basketitem) = RS("typeID")
		itemQty(basketitem) = nProdQuantity
		if not isnull(rs("customCost")) then
			itemPrice(basketitem) = RS("customCost")
		else
			itemPrice(basketitem) = RS("price_OUR")
		end if
		'response.Write("paymentAmount = " & paymentAmount & " + (" & itemPrice(basketitem) & " * " & itemQty(basketitem) & ")<br>")
		'paymentAmount = paymentAmount + (itemPrice(basketitem) * itemQty(basketitem))
	end if
	RS.close
next
call fCloseConn()

'response.end

dim sPromoCode, discountTotal
if request.form("promo") <> "" then
	sPromoCode = request.form("promo")
else
	sPromoCode = session("promocode")
end if

'response.Write("discountTotal = " & paymentAmount & " - " & request.form("paymentAmount"))
'discountTotal = paymentAmount - request.form("paymentAmount")
'discountTotal = cdbl(discountTotal)

'ItemStr = ItemStr & "						<PaymentDetailsItem>" & vbcrlf
'ItemStr = ItemStr & "							<Name>" & XMLencode(sPromoCode) & "</Name>" & vbcrlf
'ItemStr = ItemStr & "							<Number>DISCOUNT</Number>" & vbcrlf
'ItemStr = ItemStr & "							<Quantity>1</Quantity>" & vbcrlf
'ItemStr = ItemStr & "							<Amount currencyID=""USD"">" & XMLencode(discountTotal * -1) & "</Amount>" & vbcrlf
'ItemStr = ItemStr & "						</PaymentDetailsItem>" & vbcrlf

'paymentAmount = paymentAmount - discountTotal
'paymentAmount = paymentAmount - discountTotal

ItemsAndWeight = ItemsAndWeight & "|" & freeship

'BUILD CALL DATA
dim SoapStr
SoapStr = "<?xml version=""1.0"" encoding=""utf-8"" ?>" & vbcrlf
SoapStr = SoapStr & "<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" & vbcrlf
SoapStr = SoapStr & "	<soap:Header>" & vbcrlf
SoapStr = SoapStr & "		<RequesterCredentials xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
SoapStr = SoapStr & "			<Credentials xmlns=""urn:ebay:apis:eBLBaseComponents"">" & vbcrlf
SoapStr = SoapStr & "				<Username>" & API_USERNAME & "</Username>" & vbcrlf
SoapStr = SoapStr & "				<Password>" & API_PASSWORD & "</Password>" & vbcrlf
SoapStr = SoapStr & "				<Signature>" & API_SIGNATURE & "</Signature>" & vbcrlf
SoapStr = SoapStr & "			</Credentials>" & vbcrlf
SoapStr = SoapStr & "		</RequesterCredentials>" & vbcrlf
SoapStr = SoapStr & "	</soap:Header>" & vbcrlf
SoapStr = SoapStr & "	<soap:Body>" & vbcrlf
SoapStr = SoapStr & "		<SetExpressCheckoutReq xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
SoapStr = SoapStr & "			<SetExpressCheckoutRequest>" & vbcrlf
SoapStr = SoapStr & "				<Version xmlns=""urn:ebay:apis:eBLBaseComponents"">53.0</Version>" & vbcrlf
SoapStr = SoapStr & "				<SetExpressCheckoutRequestDetails xmlns=""urn:ebay:apis:eBLBaseComponents"">" & vbcrlf
'SoapStr = SoapStr & "					<ReturnURL>http://staging.wirelessemporium.com/cart/process/PaypalSOAP/GetExpressCheckoutDetails.asp</ReturnURL>" & vbcrlf
if mobileOrder = 1 then
SoapStr = SoapStr & "					<ReturnURL>https://m.wirelessemporium.com/cart/process/PaypalSOAP/GetExpressCheckoutDetails_mobile.asp</ReturnURL>" & vbcrlf
SoapStr = SoapStr & "					<CancelURL>http://m.wirelessemporium.com/basket.asp</CancelURL>" & vbcrlf
elseif instr(request.ServerVariables("SERVER_NAME"),"staging.") > 0 then
SoapStr = SoapStr & "					<ReturnURL>http://staging.wirelessemporium.com/cart/process/PaypalSOAP/GetExpressCheckoutDetails.asp</ReturnURL>" & vbcrlf
SoapStr = SoapStr & "					<CancelURL>http://staging.wirelessemporium.com/cart/basket.asp</CancelURL>" & vbcrlf
else
SoapStr = SoapStr & "					<ReturnURL>https://www.wirelessemporium.com/cart/process/PaypalSOAP/GetExpressCheckoutDetails.asp</ReturnURL>" & vbcrlf
SoapStr = SoapStr & "					<CancelURL>http://www.wirelessemporium.com/cart/basket.asp</CancelURL>" & vbcrlf
end if
SoapStr = SoapStr & "					<ReqConfirmShipping>0</ReqConfirmShipping>" & vbcrlf
SoapStr = SoapStr & "					<ReqBillingAddress>0</ReqBillingAddress>" & vbcrlf
SoapStr = SoapStr & "					<NoShipping>0</NoShipping>" & vbcrlf
SoapStr = SoapStr & "					<LocaleCode>US</LocaleCode>" & vbcrlf
SoapStr = SoapStr & "					<cpp-header-border-color>FFFFFF</cpp-header-border-color>" & vbcrlf
SoapStr = SoapStr & "					<cpp-header-back-color>FFFFFF</cpp-header-back-color>" & vbcrlf
SoapStr = SoapStr & "					<cpp-payflow-color>FFFFFF</cpp-payflow-color>" & vbcrlf
SoapStr = SoapStr & "					<PaymentAction>Authorization</PaymentAction>" & vbcrlf
SoapStr = SoapStr & "					<AllowNote>0</AllowNote>" & vbcrlf
SoapStr = SoapStr & "					<PaymentDetails>" & vbcrlf
SoapStr = SoapStr & "						<OrderTotal currencyID=""USD"">" & paymentAmount & "</OrderTotal>" & vbcrlf
SoapStr = SoapStr & "						<ItemTotal currencyID=""USD"">" & paymentAmount & "</ItemTotal>" & vbcrlf
SoapStr = SoapStr & "						<Custom>" & ItemsAndWeight & "</Custom>" & vbcrlf
'SoapStr = SoapStr & "						<Custom>" & ItemsAndWeight & ","& buysafeamount& "," & buysafecartID & "," & WantsBondField & "</Custom>" & vbcrlf
SoapStr = SoapStr & ItemStr
SoapStr = SoapStr & "					</PaymentDetails>" & vbcrlf
SoapStr = SoapStr & "				</SetExpressCheckoutRequestDetails>" & vbcrlf
SoapStr = SoapStr & "			</SetExpressCheckoutRequest>" & vbcrlf
SoapStr = SoapStr & "		</SetExpressCheckoutReq>" & vbcrlf
SoapStr = SoapStr & "	</soap:Body>" & vbcrlf
SoapStr = SoapStr & "</soap:Envelope>" & vbcrlf

'CREATE OBJECTS
dim objXMLDOC, objXMLDOM, oNode, strError
set objXMLDOC = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
set objXMLDOM = Server.CreateObject("Msxml2.DomDocument")
set oNode = Server.CreateObject("Microsoft.XMLDOM")

'response.Write("<pre>" & SoapStr & "</pre>")
'response.End()

'MAKE THE CALL
objXMLDOC.open "POST", API_ENDPOINT, False
objXMLDOC.setRequestHeader "Content-Type", "text/xml"
objXMLDOC.send(SoapStr)
objXMLDOM.async = false
objXMLDOM.LoadXML objXMLDOC.responseText

'PROCESS THE CALL
if objXMLDOM.parseError.errorCode <> 0 then
	strError = strError & "<h3>Parser error found.</h3>"
else
	'BEGIN CHECK FOR ERROR FROM PAYPAL
	set oNode = objXMLDOM.getElementsByTagName("Ack")
	if (not oNode is nothing) then
		session("errorSQL") = objXMLDOC.responseText
		if oNode.item(0).text = "Failure" then
			set oNode = objXMLDOM.getElementsByTagName("ShortMessage")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
			set oNode = objXMLDOM.getElementsByTagName("LongMessage")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
			set oNode = objXMLDOM.getElementsByTagName("ErrorCode")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
		else
			'PROCESS SUCCESSFUL CALL
			dim token
			set oNode = objXMLDOM.getElementsByTagName("Token")
			if (not oNode is nothing) then token = oNode.item(0).text
		end if
	else
		strError = strError & "<h3>No data received.</h3>"
	end if
end if

'CLEAN UP EVERYTHING
set objXMLDOC = nothing
set objXMLDOM = nothing
set oNode = nothing

if strError <> "" then
	response.write strError
else
	response.redirect(PAYPAL_EC_URL & "?cmd=_express-checkout&token=" & token)
end if
response.end

function XMLencode(str)
	XMLencode = replace(replace(replace(replace(replace(str,"&","&amp;"),"<","&lt;"),">","&gt;"),"'","&apos;"),chr(34),"&quot;")
end function
%>
