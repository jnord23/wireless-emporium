<!--#include virtual="/framework/IncludeTemplate/BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_GetMySession.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
set oConn = Server.CreateObject("ADODB.Connection")
oConn.Open Session("ConnectionString")

'For each i in Request.Form
'	Response.Write i & ": " & Request.Form(i) & "<br>"
'Next
'response.end
	
dim sPromoCode, nPromo, nAvsStreet, nAvsZip, nSiteID
dim nOrderTotal, nCATax, nSubTotal, nShipType, nShipRate
dim strShiptype, nShippingID
dim sEmail, sPwd, sFname, sLname, sPhone
dim sSAddy1, sSAddy2, sSCity, sSState, sSZip, sSCountry
dim sBAddy1, sBAddy2, sBCity, sBState, sBZip, sBCountry
dim sHearFrom, sEmailOpt
dim sCardNum
dim ssl_ProdIdCount
dim ShoppingCartId
dim sCardOwner, sCardType, sCVV2, sCC_Month, sCC_Year

adminID = prepInt(request.form("adminID"))
nSiteID = prepInt(request.form("cbSite"))
sSAddy1 = request.form("sAddress1")
sSAddy2 = request.form("sAddress2")
sSCity = request.form("sCity")
sSState = request.form("sState")
sSZip = request.form("sZip")
orderNotes = request.form("txtOrderNotes")
mobileOrder = prepStr(request.form("chkMobile"))

zeroOrder = false
apiLoginID = ""
apiTransactionKey = ""
select case nSiteID
	case 0
		siteURL = "wirelessemporium.com"
		strSiteEmail = "sales@wirelessemporium.com"
		imgLogo = "/images/WElogo.gif"
		sitePhone = "(800) 818-4575"		
		strInvoiceDescription = "WirelessEmporium.com Phone Order"
			
		apiLoginID = CC_API_LOGIN_ID
		apiTransactionKey = CC_TRANSACTION_KEY
	case 1
		siteURL = "cellphoneaccents.com"
		strSiteEmail = "sales@cellphoneaccents.com"
		imgLogo = "/images/full_logo.jpg"
		sitePhone = "(888) 244-7203"
		strInvoiceDescription = "CellphoneAccents.com Phone Order"		
			
		apiLoginID = CA_CC_API_LOGIN_ID
		apiTransactionKey = CA_CC_TRANSACTION_KEY
	case 2
		siteURL = "cellularoutfitter.com"
		strSiteEmail = "sales@cellularoutfitter.com"
		imgLogo = "/images/CellularOutfitter.jpg"
		sitePhone = "(888) 725-7575"
		strInvoiceDescription = "CellularOutfitter.com Phone Order"		
			
		apiLoginID = CO_CC_API_LOGIN_ID
		apiTransactionKey = CO_CC_TRANSACTION_KEY
	case 3
		siteURL = "phonesale.com"
		strSiteEmail = "sales@phonesale.com"
		imgLogo = "/images/phonesale.jpg"
		sitePhone = "(800) 818-4575"
		strInvoiceDescription = "PhoneSale.com Phone Order"
			
		apiLoginID = PS_CC_API_LOGIN_ID
		apiTransactionKey = PS_CC_TRANSACTION_KEY
	case 10
		siteURL = "tabletmall.com"
		strSiteEmail = "sales@tabletmall.com"
		imgLogo = "/images/logo.gif"
		sitePhone = "(888) 551-6570"
		strInvoiceDescription = "TabletMall.com Phone Order"
			
		apiLoginID = TM_CC_API_LOGIN_ID
		apiTransactionKey = TM_CC_TRANSACTION_KEY
end select

chkUseDiffAddress = request.Form("chkUseDiffAddress")

if chkUseDiffAddress = "yes" then
	sBAddy1 = request.form("bAddress1")
	sBAddy2 = request.form("bAddress2")
	sBCity = request.form("bCity")
	sBState = request.form("bState")
	sBZip = request.form("bZip")
else
	sBAddy1 = request.form("sAddress1")
	sBAddy2 = request.form("sAddress2")
	sBCity = request.form("sCity")
	sBState = request.form("sState")
	sBZip = request.form("sZip")
end if

if instr(sSZip,"-") > 0 then
	zipArray = split(sSZip,"-")
	sSZip = zipArray(0)
end if
if instr(sBZip,"-") > 0 then
	zipArray = split(sBZip,"-")
	sBZip = zipArray(0)
end if

sPromoCode = request.form("sPromoCode")
nPromo = request.form("nPromo") ' this is couponid
nSubTotal = prepInt(request.form("nSubTotal"))
discountTotal = prepInt(request.Form("nDiscountTotal"))
shippingDiscountTotal = prepInt(request.Form("nShippingDiscountTotal"))
itemDiscountTotal = prepInt(request.Form("nFreeItemTotal"))
useQty = prepInt(request.form("nTotalQuantity"))
nShipRate = prepInt(request.form("nShippingTotal"))
strShiptype = request.form("strShiptype")
nShippingID = 0
sCardNum = request.form("cardNum")
nCATax = prepInt(request.form("nCATax"))
sEmailOpt = request.form("chkOptMail")
nShipType = prepInt(request.form("shiptypeID"))
ShoppingCartId = request.form("buysafecartID")
sCardOwner = request.form("cc_cardOwner")
sCardType = request.form("cc_cardType")
sCVV2 = request.form("secCode")
sCC_Month = request.form("cc_month")
sCC_Year = request.form("cc_year")

sEmail = request.form("email")
sFname = request.form("fname")
sLname = request.form("lname")
sPhone = request.form("phoneNumber")

nAccountId = request.Form("accountid")
parentAcctID = request.Form("parentAcctID")
if not isnumeric(parentAcctID) then parentAcctID = 0

if len(sBZip) = 5 and isNumeric(sBZip) then
	sBCountry = "US"
else
	if len(sSState) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sSState) > 0 then
		sBCountry = "CANADA"
	else
		sBCountry = ""
	end if
end if

nTotalQuantity = cDbl(useQty)

nAvsStreet = sBAddy1 &" "& sBAddy2
nAvsStreet = replace(nAvsStreet, "&", "and")
nAvsZip = sBZip

if len(sSZip) = 5 and isNumeric(sSZip) then
	sSCountry = "US"
else
	if len(sSState) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sSState) > 0 then
		sSCountry = "CANADA"
	else
		sSCountry = ""
	end if
end if

sql = "select * from xstore where site_id = '" & nSiteID & "'"
session("errorSQL") = sql
set rsSite = oConn.execute(sql)
if rsSIte.eof then
	response.redirect "/admin/phoneorder.asp?ec=99"
	response.end
end if

if nTotalQuantity <= 0 or not isnumeric(nTotalQuantity) then
	response.redirect "/admin/phoneorder.asp?ec=101"
	response.end
end if

select case nSiteID
	case 0 
		tblAccount = "we_accounts"
		spEmailOpt = "sp_ModifyEmailOpt"
	case 1 
		tblAccount = "ca_accounts"
		spEmailOpt = "sp_ModifyEmailOpt_CA"
	case 2 
		tblAccount = "co_accounts"
		spEmailOpt = "sp_ModifyEmailOpt_CO"
	case 3 
		tblAccount = "ps_accounts"
		spEmailOpt = "sp_ModifyEmailOpt_PS"
	case 10 
		tblAccount = "er_accounts"
		spEmailOpt = "sp_ModifyEmailOpt_ER"
end select

'response.write "nAccountId:" & nAccountId & "<br>"
'response.end

'if nAccountId = 0 then
	SQL = "SET NOCOUNT ON; "
	if nSiteID <> 1 and nSiteID <> 2 then
		SQL = SQL & "INSERT INTO " & tblAccount & " (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,dateEntered,CustomerIP,parentID) VALUES ("
	else
		SQL = SQL & "INSERT INTO " & tblAccount & " (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,dateEntered,parentID) VALUES ("
	end if
	SQL = SQL & "'" & ds(sFname) & "', "
	SQL = SQL & "'" & ds(sLname) & "', "
	SQL = SQL & "'" & ds(sBAddy1) & "', "
	SQL = SQL & "'" & ds(sBAddy2) & "', "
	SQL = SQL & "'" & ds(sBCity) & "', "
	SQL = SQL & "'" & ds(sBState) & "', "
	SQL = SQL & "'" & ds(sBZip) & "', "
	SQL = SQL & "'" & ds(sBCountry) & "', "
	SQL = SQL & "'" & ds(sEmail) & "', "
	SQL = SQL & "'" & ds(sPhone) & "', "
	SQL = SQL & "'" & ds(sSAddy1) & "', "
	SQL = SQL & "'" & ds(sSAddy2) & "', "
	SQL = SQL & "'" & ds(sSCity) & "', "
	SQL = SQL & "'" & ds(sSState) & "', "
	SQL = SQL & "'" & ds(sSZip) & "', "
	SQL = SQL & "'" & ds(sSCountry) & "', "
	SQL = SQL & "'" & now & "' "
	if nSiteID <> 1 and nSiteID <> 2 then
		SQL = SQL & ",'" & Request.ServerVariables("REMOTE_ADDR") & "', "
		SQL = SQL & "'" & ds(parentAcctID) & "')"
	else
		SQL = SQL & ",'" & ds(parentAcctID) & "')"
	end if
	SQL = SQL & "SELECT @@IDENTITY AS newAccountID; "
	SQL = SQL & "SET NOCOUNT OFF;"
	session("errorSQL") = SQL
	dim nAccountId
	set RS = oConn.execute(SQL)
	if not RS.eof then nAccountId = RS("newAccountID")
'else
'	sql =   "update " & tblAccount & " set fname = '" & ds(sFname) & "',lname = '" & ds(sLname) & "',bAddress1 = '" & ds(sBAddy1) & "',bAddress2 = '" & ds(sBAddy2) & "'," &_
'			"bCity = '" & ds(sBCity) & "',bState = '" & ds(sBState) & "',bZip = '" & ds(sBZip) & "',bCountry = '" & ds(sBCountry) & "',email = '" & ds(sEmail) & "'," &_
'			"phone = '" & ds(sPhone) & "',sAddress1 = '" & ds(sSAddy1) & "',sAddress2 = '" & ds(sSAddy2) & "',sCity = '" & ds(sSCity) & "'," &_
'			"sState = '" & ds(sSState) & "',sZip = '" & ds(sSZip) & "',sCountry = '" & ds(sSCountry) & "'"
'	if nSiteID <> 1 and nSiteID <> 2 then
'		sql = sql & ",CustomerIP = '" & Request.ServerVariables("REMOTE_ADDR") &"' "
'	end if			
'	sql = sql & " where accountID = " & nAccountId
'	session("errorSQL") = sql
'	oConn.execute(sql)
'end if

oConn.execute (spEmailOpt & " '" & ds(sFname) & "','" & sEmail & "','" & sEmailOpt & "'")

nSubTotal = formatNumber(nSubTotal,2)
session("errorSQL") = nSubTotal & " + " & nCATax & " + " & nShipRate & " + " & discountTotal
nOrderTotal = nSubTotal + nCATax + nShipRate - discountTotal
nOrderTotal = formatNumber(nOrderTotal,2)

if nOrderTotal = 0 then
	nResponseStatus = 1
	zeroOrder = true
	session("zeroOrder") = 1
end if
	
'WRITE THE ORDER IN THE DATABASE
dim nOrderID
nOrderId = fWriteBasketItemIntoDB(nAccountId,nSubTotal,nShipRate,nOrderTotal,strShiptype,nCATax,nShippingID,nPromo)

dim result_message, cvv2_response, avs_response, txn_id, txn_time, approval_code, transaction_type
sCardOwner = replace(sCardOwner, Chr(34), " ")
sCardOwner = replace(sCardOwner, Chr(39), " ")
sCardNum = replace(sCardNum, Chr(34), "")
sCardNum = replace(sCardNum, Chr(39), "")

dim sExp
sExp = sCC_Month & sCC_Year
sCC_Year1 = "20" & sCC_Year


bTestmode = false
if sCardNum = "4000100011112224" then
	bTestmode = true		'yyy Test mode
elseif sCardNum = "4000100011112222" then
	bTestmode = true		'NYZ Test mode
elseif sCardNum = "5424180279791740" then
	bTestmode = true		'negative AVS Test mode
elseif sCardNum = "371122223332241" then
	bTestmode = true		'Test mode
elseif sCardNum = "4111111111111111" then
	bTestmode = true		'Test mode
elseif sCardNum = "4000200011112226" then
	bTestmode = true		'N/A Test mode
elseif sCardNum = "6011222233332273" then
	bTestmode = true		'Test mode
elseif sCardNum = "4000600011112223" then
	bTestmode = true		'Test mode
end if

if bTestmode = true then
	if ds(sBAddy1) <> "4040 N. Palm St." then
		response.redirect("/admin/phoneOrder.asp?testError=1")
		response.end
	end if
end if

'====================================================================
post_url = CC_TRANS_POST_URL
Set post_values = CreateObject("Scripting.Dictionary")
post_values.CompareMode = vbTextCompare

post_values.Add "x_login", apiLoginID
post_values.Add "x_tran_key", apiTransactionKey

post_values.Add "x_version", "3.1"
post_values.Add "x_type", "AUTH_ONLY"
post_values.Add "x_method", "CC"
post_values.Add "x_amount", nOrderTotal
post_values.Add "x_card_num", ds(sCardNum)
post_values.Add "x_exp_date", ds(sExp)
post_values.Add "x_card_code", ds(sCVV2)
post_values.Add "x_test_request", bTestmode

post_values.Add "x_invoice_num", nOrderID
post_values.Add "x_description", strInvoiceDescription
		
'Billing Information
post_values.Add "x_first_name", ds(sFname)
post_values.Add "x_last_name", ds(sLname)
post_values.Add "x_address", ds(sBAddy1) & " " & ds(sBAddy2)
post_values.Add "x_city", ds(sBCity)
post_values.Add "x_state", ds(sBState)
post_values.Add "x_zip", ds(sBZip)
post_values.Add "x_country", ds(sBCountry)
post_values.Add "x_phone", ds(sPhone)
post_values.Add "x_email", ds(sEmail)
post_values.Add "x_customer_ip", Request.ServerVariables("REMOTE_ADDR")

'Shipping Information
post_values.Add "x_ship_to_first_name", ds(sFname)
post_values.Add "x_ship_to_last_name", ds(sLname)
post_values.Add "x_ship_to_address", ds(sSAddy1) & " " & ds(sSAddy2)
post_values.Add "x_ship_to_city", ds(sSCity)
post_values.Add "x_ship_to_state", ds(sSState)
post_values.Add "x_ship_to_zip", ds(sSZip)
post_values.Add "x_ship_to_country", ds(sSCountry)

post_values.Add "x_delim_data", true
post_values.Add "x_delim_char", "|"
post_values.Add "x_recurring_billing", false
post_values.Add "x_relay_response", false
post_values.Add "x_duplicate_window", 30
'====================================================================

post_string = ""
for each key in post_values
	post_string = post_string & key & "=" & Server.URLEncode(post_values(key)) & "&"
next
post_string = Left(post_string,Len(post_string)-1)

'oConn.execute("insert into _terry(msg) values('" & replace(post_string, "'", "''") & "')")

Dim objRequest, post_response

if not zeroOrder then
	Set objRequest = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
		objRequest.open "POST", post_url, false
		objRequest.send post_string
		post_response = objRequest.responseText
	Set objRequest = nothing

	Dim response_array
	response_array = split(post_response, post_values("x_delim_char"), -1)

	nResponseStatus = response_array(0)
	responseText = response_array(3)
	authcode = response_array(4)
	avsStreet = response_array(5)
	transactionID = response_array(6)
	sCardCodeStatus = response_array(38)
	avsText = ""
	cvvText = ""
end if

if nResponseStatus = 1 then
	SQL = "UPDATE we_orders SET approved = 1, transactionID = '" & nResponseReferenceNum & "' WHERE orderid = '" & nOrderID & "'"
	set updateRS = oConn.execute(SQL)
	
	dim thisItemID, thisQuantity
	'update Number of sales
	SQL = "SELECT A.itemID, A.quantity, B.ItemKit_NEW FROM we_orderdetails A INNER JOIN we_items B ON A.itemID=B.itemID WHERE A.orderID = '" & nOrderID & "'"
	'session("errorSQL") = SQL
	set orderdetailRS = oConn.execute(SQL)
	do until orderdetailRS.eof
		thisItemID = orderdetailRS("itemID")
		thisQuantity = orderdetailRS("quantity")
		if isNull(orderdetailRS("ItemKit_NEW")) then
			SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & thisQuantity & " WHERE itemID = '" & thisItemID & "'"
		else
			SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & thisQuantity & " WHERE itemID IN (" & orderdetailRS("ItemKit_NEW") &","&thisItemID & ")"
		end if
		session("errorSQL") = request.servervariables("PATH_INFO") & ":<br>" & SQL
		oConn.execute SQL
		orderdetailRS.movenext
	loop
	' update ShoppingCart table
	SQL = "UPDATE ShoppingCart SET purchasedOrderID = '" & nOrderID & "' where store = '" & nSiteID & "' and adminID = '" & adminID & "' and (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
	session("errorSQL") = SQL
	oConn.execute SQL
	
	select case nSiteID
		case 0 : response.redirect("https://www.wirelessemporium.com/cart/complete.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal & "&id=phoneorder")
		case 1 : response.redirect("https://www.cellphoneaccents.com/cart/complete.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal & "&id=phoneorder")
		case 2 : response.redirect("https://www.cellularoutfitter.com/cart/complete.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal & "&id=phoneorder")
		case 3 : response.redirect("https://www.phonesale.com/cart/complete.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal & "&id=phoneorder")
		case 10 : response.redirect("https://www.tabletmall.com/complete.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderTotal & "&c=" & nSubTotal & "&id=phoneorder")
	end select
	response.end
else
	select case nResponseStatus
		case 2 : strResponseStatus = "Declined"
		case 3 : strResponseStatus = "Error"
		case 4 : strResponseStatus = "Held For Review"
	end select
	select case ucase(avsStreet)
		case "A" : avsText = "Address (Street) matches, ZIP does not"
		case "B" : avsText = "Address information not provided for AVS check"
		case "E" : avsText = "AVS error"
		case "G" : avsText = "Non-U.S. Card Issuing Bank"
		case "N" : avsText = "No Match on Address (Street) or ZIP"
		case "P" : avsText = "AVS not applicable for this transaction"
		case "R" : avsText = "Retry?�System unavailable or timed out"
		case "S" : avsText = "Service not supported by issuer"
		case "U" : avsText = "Address information is unavailable"
		case "W" : avsText = "Nine digit ZIP matches, Address (Street) does not"
		case "X" : avsText = "Address (Street) and nine digit ZIP match"
		case "Y" : avsText = "Address (Street) and five digit ZIP match"
		case "Z" : avsText = "Five digit ZIP matches, Address (Street) does not"
		case else : avsText = "Error occured during process"
	end select
	select case ucase(cardCodeStatus)
		case "M" : cvvText = "Match"
		case "N" : cvvText = "No Match"
		case "P" : cvvText = "Not Processed"
		case "S" : cvvText = "Should have been present"
		case "U" : cvvText = "Issuer unable to process request"
	end select		
	ErrorStr = strResponseStatus & ": " & avsText & ". " & cvvText
	%>
	<html>
    <head>
    	<link rel=stylesheet href="/includes/css/style.css" type="text/css">
    </head>
    <body bgcolor=#ffffff text=#000000 leftmargin=0 topmargin=0 marginwidth='0' marginheight='0' link=#666666 vlink=#666666 alink=#ff6600>
	<table cellpadding="1" cellspacing="1" width="75%" border="1" align="center" class="mc-text">
		<tr>
			<td align=middle><a href="/"><img src="<%=imgLogo%>" border="0"></a></td>
		</tr>
		<tr>
			<td>
				<strong>
					<%
					if ErrorStr = "" then
						%>
						Due to security reasons, we are unable to process your order online at this time.
						<%
					else
						response.write ErrorStr
					end if
					%>
					<br><br>
				</strong>
			</td>
		</tr>
		<tr>
			<td>
				<br>Please resubmit your order for processing.
				<br>If you continue to experience difficulties, please contact us at <%=sitePhone%> and an Associate will be happy to process your order immediately over the phone.
				<br><br>
				Thank you for shopping with <%=siteURL%>!
				<br><br>
				<a href="mailto:<%=strSiteEmail%>"><%=strSiteEmail%></a>
			</td>
		</tr>
		<tr><td height="5" bgcolor="#CCCCCC"><img src="/images/spacer.gif" width="1" height="5" border="0"></td></tr>
		<tr>
			<td>
				<p>We apologize for the issues that you are having with this transaction. Please verify that the following information is entered correctly:</p>
				<ul>
					<li>Billing Address (your billing address must be entered exactly as it is on your credit card statement)</li>
					<li>Credit Card Number</li>
					<li>Expiration Date</li>
					<li>CVV Code (you can find this 3 digit code on the back of the card, for Amex the code can also be 4 digits and be located on the front)</li>
				</ul>
				<p>If all of this information is entered correctly please fill out the form below and we will get back to you shortly.</p>
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="mc-text">
					<form action="/cart/DeclineProcess.asp" method="post">
					<tr>
						<td valign="top">Customer Name on Order:</p></td>
						<td valign="top"><p><input type="text" name="CustomerName" size="80"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Email address associated with order:</p></td>
						<td valign="top"><p><input type="text" name="EmailAddress" size="80"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Contact phone number:</p></td>
						<td valign="top"><p><input type="text" name="ContactPhone" size="80"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Error code given:</p></td>
						<td valign="top"><p><input type="text" name="ErrorCode" size="80" value="<%=ErrorStr%>"></p></td>
					</tr>
					<tr>
						<td valign="top"><p>Additional comments/concerns:</p></td>
						<td valign="top"><p><textarea name="AdditionalComments" cols="65" rows="3"></textarea></p></td>
					</tr>
					<tr>
						<td colspan="2" align="center"><p><input type="submit" name="submitted" value="Send Form"></p></td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
    </body>
    </html>    
<%
end if

'Function from inc_orderfunctions.asp
function fWriteBasketItemIntoDB(nAccountId,nSubTotal,sShipRate,nOrderTotal,strShiptype,sTax,shippingid,nPromo)
	'this function will write the basket items into the database we_orders
	if nTotalQuantity <= 0 then
		'basket currently empty
		response.redirect("/admin/phoneorder.asp?ec=102")
		response.end
	else
		'insert into WE_Order table
		SQL = "sp_InsertOrder3 " & nSiteID & ",'" & nAccountId & "','" & FormatNumber(nSubTotal,2) & "','" & FormatNumber(sShipRate,2) & "'," & sTax & ",'" & FormatNumber(nOrderTotal,2) & "','" & replace(strShiptype, "'", "''") & "','" & shippingid & "','" & nPromo & "','" & Now & "'"
		session("errorSQL") = SQL
		nOrderId = oConn.execute(SQL).fields(0).value
		session("invoice") = nOrderId

		sql = 	"select	itemid, qty" & vbcrlf & _
				"from	shoppingcart" & vbcrlf & _
				"where	adminID = '" & adminID & "' and store = '" & nSiteID & "' and (purchasedorderid is null or purchasedorderid = 0)"
		session("errorSQL") = SQL
		set rsOrderDetail = oConn.execute(sql)
		if not rsOrderDetail.eof then
			do until rsOrderDetail.eof
				strSP = "sp_InsertOrderDetail '" & nOrderId & "','" & rsOrderDetail("itemid") & "','" & rsOrderDetail("qty") & "'"
				oConn.execute(strSP)
				rsOrderDetail.movenext
			loop
		end if
		sql = "update we_orderDetails set musicSkins = 1 where itemID >= 1000000 and (musicSkins is null or musicSkins = 0)"
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	sql = "update we_orders set note = '" & replace(orderNotes, "'", "''") & "' where orderID = '" & nOrderId & "'"
	session("errorSQL") = sql
	oConn.execute(sql)
	
	if mobileOrder = "Y" then
		sql = "update we_orders set mobileSite = 1 where orderID = '" & nOrderId & "'"
		session("errorSQL") = sql
		oConn.execute(sql)	
	end if

	sql	=	"insert into we_phoneorders(orderid, adminid, specialnotes, shippingDiscount, itemDiscount) values('" & nOrderId & "', '" & adminID & "', '" & replace(orderNotes, "'", "''") & "', " & shippingDiscountTotal  & ", " & itemDiscountTotal & ")"
	session("errorSQL") = sql
	oConn.execute(sql)	
	
	fWriteBasketItemIntoDB = nOrderId
end function

function XMLencode(str)
	XMLencode = replace(replace(replace(replace(replace(str,"&","&amp;"),"<","&lt;"),">","&gt;"),"'","&apos;"),chr(34),"&quot;")
end function
%>