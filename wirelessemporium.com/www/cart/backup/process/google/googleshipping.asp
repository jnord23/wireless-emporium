<%
'******************************************************************************
' Copyright (C) 2006 Google Inc.
'  
' Licensed under the Apache License, Version 2.0 (the "License");
' you may not use this file except in compliance with the License.
' You may obtain a copy of the License at
'  
'      http://www.apache.org/licenses/LICENSE-2.0
'  
' Unless required by applicable law or agreed to in writing, software
' distributed under the License is distributed on an "AS IS" BASIS,
' WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
' See the License for the specific language governing permissions and
' limitations under the License.
'******************************************************************************

'******************************************************************************
' Class FlatRateShipping is the object representation of a
'     <flat-rate-shipping>.
'******************************************************************************
Class FlatRateShipping
	Public Name
	Public Price

	Public AllowedCountryArea
	Public AllowedStates
	Public AllowedZipPatterns

	Public ExcludedCountryArea
	Public ExcludedStates
	Public ExcludedZipPatterns

	Public ShippingType

    Private Sub Class_Initialize()
		ShippingType = "flat-rate-shipping"

		AllowedCountryArea = ""
		AllowedStates = Array()
		AllowedZipPatterns = Array()
		
		ExcludedCountryArea = ""
		ExcludedStates = Array()
		ExcludedZipPatterns = Array()
    End Sub
End Class


'******************************************************************************
' Class MerchantCalculatedShipping is the object representation of a
'     <merchant-calculated-shipping>.
'******************************************************************************
Class MerchantCalculatedShipping
	Public Name
	Public Price

	Public AllowedCountryArea
	Public AllowedStates
	Public AllowedZipPatterns

	Public ExcludedCountryArea
	Public ExcludedStates
	Public ExcludedZipPatterns

	Public ShippingType

    Private Sub Class_Initialize()
		ShippingType = "merchant-calculated-shipping"

		AllowedCountryArea = ""
		AllowedStates = Array()
		AllowedZipPatterns = Array()
		
		ExcludedCountryArea = ""
		ExcludedStates = Array()
		ExcludedZipPatterns = Array()
    End Sub
End Class


'******************************************************************************
' Class Pickup is the object representation of a <pickup>.
'******************************************************************************
Class PickupShipping
	Public Name
	Public Price
	Public ShippingType

    Private Sub Class_Initialize()
		ShippingType = "pickup"
    End Sub
End Class

%>