<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<%
call fOpenConn()
sql = "select top 1 * from we_orders where store = 0 and extOrderType = 2 and ordersubtotal = '" & session("orderSubTotal") & "' order by orderID desc"
session("errorSQL") = sql
response.Write("<!-- " & sql & " -->")
set rs = Server.CreateObject("ADODB.Recordset")
rs.open sql, oConn, 0, 1

if rs.EOF then
	nOrderID = 0
	nAccountID = 0
	nOrderGrandTotal = 0
else
	nOrderID = rs("orderid")
	nAccountID = rs("accountid")
	nOrderGrandTotal = rs("ordergrandtotal")
	nOrderSubTotal = rs("ordersubtotal")
	nOrderTax = rs("orderTax")
	nOrdershippingfee = rs("ordershippingfee")
	
	nItemCount = 0
	sql = "SELECT B.price_our, B.typeID, A.quantity, B.vendor, B.PartNumber, B.ItemKit_NEW FROM we_orderdetails A INNER JOIN we_items B ON A.itemID=B.itemID WHERE A.orderID = '" & nOrderID & "'"
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	wgItems = ""
	do until RS.eof
		nItemCount = nItemCount + rs("quantity")
		if wgItems = "" then
			if rs("typeID") = 16 then
				wgItems = 7479 & "::" & rs("price_our")
			else
				wgItems = 7468 & "::" & rs("price_our")
			end if
		else
			if rs("typeID") = 16 then
				wgItems = wgItems & "|" & 7479 & "::" & rs("price_our")
			else
				wgItems = wgItems & "|" & 7468 & "::" & rs("price_our")
			end if
		end if
		RS.movenext
	loop
end if
call fCloseConn()

if isnull(nOrderID) or len(nOrderID) < 1 then nOrderID = 0
if isnull(nAccountID) or len(nAccountID) < 1 then nAccountID = 0
if isnull(nOrderGrandTotal) or len(nOrderGrandTotal) < 1 then nOrderGrandTotal = 0

if nOrderID = 0 then
	bouncePage = "http://www.wirelessemporium.com/"
else
	bouncePage = "http://www.wirelessemporium.com/confirm.asp?o=" & nOrderID & "&a=" & nAccountID & "&d=" & nOrderGrandTotal
end if
%>
<html>
	<head>
		<title>Your Transaction is Being Processed...</title>
	</head>
	<meta http-equiv="refresh" content="2;URL=<%=bouncePage%>">
	<body>
	<!--#include virtual="/cart/includes/inc_trackingCodes.asp"-->
    <table width="100%" border="0" align="center" cellpadding="20" cellspacing="0">
        <tr>
            <td align="center">
                <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                <p>
                    <strong><font color="#FF6600" size="4" face="Verdana, Arial, Helvetica, sans-serif">Your Transaction is Being Completed...</font></strong>
                </p>
                <p>
                    Please wait while your transaction is being completed<br>
                    (DO NOT HIT BACK ON YOUR BROWSER).<br>
                    <% if nOrderID > 0 then %>
                    You will soon be taken to an order receipt page confirming your order!
                    <% end if %>
                </p>
                <p>
                    <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>.... Exiting Secure Server ....</strong></font>
                </p>
            </td>
        </tr>
    </table>
    </body>
</html>
