<%
dim vendororder
on error resume next
vendororder = Request.Cookies("vendororder")
on error goto 0
if vendororder = "google" then vendororder = "adwords"

'db-driven pixel [knguyen/20110520]
'todo: consolidate all hardcoded pixels into actual db rules
'dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
'dim dicContentEventText: set dicContentEventText = GetContentEventText( dicSeoAttributeInput)
'call OutputPixel( dicContentEventText) '--sends out the db-driven pixel

call printPixel(3)

if vendororder <> "" and vendororder <> "gbase" and vendororder <> "turnto" then
	select case lcase(left(vendororder,7))
		case "webgain"
			%>
			<!--#include file="md5.asp"-->
			<%
			' Copyright Webgains Ltd 2009
			' Contact assistance@webgains.com
			
			dim wgPin
			dim wgProgramId
			dim wgValue
			dim wgEventID
			dim wgOrderReference
			dim wgString
			dim wgQueryString
			dim wgCheckString
			dim wgCheckSum
			dim wgProtocol
			dim wgVersion
			dim wgComment
			dim wgMultiple
			dim wgItems
			dim wgCustomerID
			dim wgProductID
			dim wgLang
			dim wgSLang
			dim wgSubDomain
			dim wgUri
			Dim wgVoucherCode
			
			''''''''''''''''''''''''
			' <run time variables> '
			''''''''''''''''''''''''
			
			wgValue = nOrderSubTotal ' float, value of transaction eg 5.50 (please exclude commas and currency symbols)
			wgOrderReference = Server.URLEncode(nOrderId) ' string, if your order references are NOT unique, please let us know asap via assistance@webgains.com.
			wgEventID = 7468 ' int, identifies a type of commission that you have set up on our system. To find correct numbers see  Program Setup (program settings -> commission types)). leaving as 457 will mean that all commissions are paid on default commission type.
			wgComment = Server.URLEncode("")
			wgMultiple=1
			wgItems= Server.URLEncode(wgItems)
			wgCustomerID=""

			wgProductID=""
			wgVoucherCode = Server.URLEncode("") ' string, your voucher code for transaction, if no code present please leave blank
			
			'''''''''''''''''''''''''
			' </run time variables> '
			'''''''''''''''''''''''''
			
			''''''''''''''''''''''''''
			' <configure on install> '
			''''''''''''''''''''''''''
			
			wgLang = "en_US" ' string, used to identify the human language of the transaction
			wgPin = 2353 ' pin number provided by webgains (in account under Program Setup (program settings -> technical setup))
			wgProgramID = 4734 ' int, used to identify you to webgains systems
			
			'''''''''''''''''''''''''''
			' </configure on install> '
			'''''''''''''''''''''''''''
			
			''''''''''''''''''''''
			' <not configurable> '
			''''''''''''''''''''''
			wgSLang = "asp"' string, used to identify the programming language of your online systems. Needed because url_encoding differs between platforms.
			wgVersion = "1.2"
			wgSubDomain = "track"
			wgString = "wgver=" &wgVersion &"&wgsubdomain="&wgSubDomain &"&wglang=" &wgLang &"&wgslang=" &wgSLang &"&wgprogramid=" &wgProgramID &"&wgeventid=" &wgEventID &"&wgvalue=" &wgValue &"&wgorderreference=" &wgOrderReference &"&wgcomment=" &wgComment &"&wgmultiple=" &wgMultiple &"&wgitems=" &wgItems &"&wgcustomerid=" &wgCustomerID &"&wgproductid=" &wgProductID &"&wgvouchercode=" &wgVoucherCode
			
			' join PIN and query string to make checksum
			wgCheckString = wgPin &wgString
			' make checksum
			wgCheckSum = md5(wgCheckString)
			wgQueryString = wgString &"&wgchecksum="&wgCheckSum
			wgUri = "://" & wgSubDomain &".webgains.com/transaction.html?"&wgQueryString
			
			'''''''''''''''''''''''
			' </not configurable> '
			'''''''''''''''''''''''
			%>
			<script language="javascript" type="text/javascript">
			if(location.protocol.toLowerCase() == "https:") wgProtocol="https";
			else wgProtocol="http";
			wgUri = wgProtocol + "<% Response.Write wgUri %>" + "&wgprotocol=" + wgProtocol + "&wglocation=" + location.href;
			document.write('<sc'+'ript language="JavaScript"  type="text/javascript" src="'+wgUri+'"></sc'+'ript>');
			</script><noscript>
			<% Response.Write "<img src=""https://"&wgSubDomain&".webgains.com/transaction.html?wgver=" & wgVersion & "&wgrs=1&wgsubdomain=" & wgSubDomain & "&wglang=" & wgLang & "&wgslang=" & wgSLang & "&wgprogramid=" & wgProgramID & "&wgeventid=" & wgEventID & "&wgvalue=" & wgValue & "&wgorderreference=" & wgOrderReference & "&wgcomment=" & wgComment & "&wgmultiple=" & wgMultiple & "&wgitems=" & wgItems & "&wgcustomerid=" & wgCustomerID & "&wgproductid=" & wgProductID & "&wgvouchercode=" & wgVoucherCode & "&wgchecksum=" & wgCheckSum & "&wgprotocol=https"" alt="""" width=""1"" height=""1""/>"%>
			</noscript>
			<%
		case "clickid"
			' Google Affiliate Network code added on 4/15/2010 by MC
			response.write "<img src=""https://clickserve.cc-dt.com/link/order?vid=K268833"
			response.write "&oid=" & nOrderId
			response.write "&amt=" & nOrderSubTotal
			response.write left(strGoogleSKU,len(strGoogleSKU) - 1)
			response.write left(strGoogleNum,len(strGoogleNum) - 1)
			response.write left(strGoogleQty,len(strGoogleQty) - 1)
			response.write left(strGooglePrice,len(strGooglePrice) - 1)
			response.write left(strGoogleCat,len(strGoogleCat) - 1)
			if left(vendororder,7) = "clickid" then
				response.write "&clickid=" & replace(vendororder,"clickid=","")
			end if
			response.write """ width=1 height=1>" & vbcrlf
		case "smarter"
			'Smarter code added by MC 8/11/2010
			response.write "<!-- Smarter Code for Purchase Conversion Page -->" & vbcrlf
			response.write "<script language=""JavaScript"" type=""text/javascript"">" & vbcrlf
			response.write "var sm_mer_id = 1247;" & vbcrlf
			response.write "var sm_advance = 1;" & vbcrlf
			response.write "var sm_order_id = " & nOrderId & ";" & vbcrlf
			response.write "var sm_order_amt = " & nOrderGrandTotal & ";" & vbcrlf
			response.write "var sm_units_ordered = " & nItemCount & ";" & vbcrlf
			response.write "</script>" & vbcrlf
			response.write "<script language=""JavaScript"" src=""https://sam.smarter.com/roi/js/conversion.js""></script>" & vbcrlf
		case "pricegr"
			'PriceGrabber.com CTS Item Detail Tracking
			response.write "<img src=""https://www.pricegrabber.com/conversion.php?retid=2008" & strPriceGrabber & """>" & vbcrlf
		case "become"
			'Become.com code added by MC 4/7/2010
			response.write "<script type=""text/javascript"">" & vbcrlf
			response.write "var become_merchant_id = '64D8F9D1DEF515AA';" & vbcrlf
			response.write "var become_order_num = '" & nOrderId & "';" & vbcrlf
			response.write "var become_purchased_items = new Array();" & vbcrlf
			response.write strBecome
			response.write "</script> <script type=""text/javascript"" language=""javascript"" src=""https://partner.become.com/roi-tracker2/conversion.js""></script>" & vbcrlf
			response.write "<noscript><img src=""https://partner.become.com/roi-tracker2/t.gif?merchantid=64D8F9D1DEF515AA&order_id=" & nOrderId & "&order_value=" & nOrderGrandTotal & """ style=""display:none;border:0;""/></noscript>" & vbcrlf
		case "pronto"
			'Pronto code added by MC 8/17/2010
			response.write "<script language=""Javascript"">" & vbcrlf
			response.write "var merchant_account_id='49234'" & vbcrlf
			response.write "var m_order_id='" & nOrderId & "';" & vbcrlf
			response.write "var m_order_value='" & nOrderGrandTotal & "';" & vbcrlf
			response.write "var m_category_name='" & replace(left(mycategory_name,len(mycategory_name)-2),", ",",") & "';" & vbcrlf
			response.write "var m_product_id='" & replace(left(myproduct_id,len(myproduct_id)-2),", ",",") & "';" & vbcrlf
			response.write "var m_product_name='" & replace(left(myproduct_name,len(myproduct_name)-2),", ",",") & "';" & vbcrlf
			response.write "var m_price='" & replace(left(myproduct_price,len(myproduct_price)-2),", ",",") & "';" & vbcrlf
			response.write "var m_quantity='" & replace(left(myproduct_qty,len(myproduct_qty)-2),", ",",") & "';" & vbcrlf
			response.write "</script>" & vbcrlf
			response.write "<script language=""Javascript"" src=""https://merchant.pronto.com/js/roi.js"">" & vbcrlf
			response.write "</script>" & vbcrlf
		case "nextag"
			'NexTag ROI Optimizer Data
			response.write "<script type=""text/javascript"">" & vbcrlf
			response.write "/* NexTag ROI Optimizer Data */" & vbcrlf
			response.write "var id = '189395';" & vbcrlf
			response.write "var rev = '" & nOrderGrandTotal & "';" & vbcrlf
			response.write "var order = '" & nOrderId & "';" & vbcrlf
			response.write "//var cats = '" & categories & "';" & vbcrlf
			response.write "var cats = '1';" & vbcrlf
			response.write "// var prods = '';" & vbcrlf
			response.write "var units = '" & nTotQty & "';" & vbcrlf
			response.write "</script>" & vbcrlf
			response.write "<script type=""text/javascript"" src=""https://imgsrv.nextag.com/imagefiles/includes/roitrack.js""></script>" & vbcrlf
		case "shoppin"
			'Added for DealTime/Shopping.com done by prasad 09/27.
			'Modified by MC 09/19/2007.
			response.write "<SCRIPT LANGUAGE=""JavaScript"">" & vbcrlf
			response.write "var merchant_id = '301014';" & vbcrlf
			response.write "var order_id = '"& nOrderId &"';" & vbcrlf
			response.write "var order_amt = '"& nOrderGrandTotal & "';" & vbcrlf
			response.write "var category_id = '" & left(mytypeName,len(mytypeName)-2) & "';" & vbcrlf
			response.write "var category_name = '" & left(mycategory_name,len(mycategory_name)-2) & "';" & vbcrlf
			response.write "var product_id = '" & left(myproduct_id,len(myproduct_id)-2) & "';" & vbcrlf
			response.write "var product_name = '" & left(myproduct_name,len(myproduct_name)-2) & "';" & vbcrlf
			response.write "</script>" & vbcrlf
			response.write "<script language=""JavaScript"" src=""https://stat.DealTime.com/ROI/ROI.js?mid=301014""></script>" & vbcrlf
	end select
else
	'show the Fetchback code if there is no "vendororder" cookie value - added by MC 11/1/2010
	response.write "<iframe src='https://pixel.fetchback.com/serve/fb/pdc?cat=&name=success&sid=1235&crv=" & round(totalFetchback,2) & "&oid=" & nOrderId & "' scrolling='no' width='1' height='1' marginheight='0' marginwidth='0' frameborder='0'></iframe>" & vbcrlf
end if

'TurnTo.com tracking code added 8/17/2009 by MC
response.write "<script type=""text/javascript"">" & vbcrlf
response.write "var turnToConfig = {" & vbcrlf
response.write "host: ""www.turnto.com"", " & vbcrlf
response.write "siteKey: ""W1a86usaEcbMZXlsite"", " & vbcrlf
response.write "staticHost:""static.www.turnto.com"", " & vbcrlf
response.write "floatingTeaserStyle:2, " & vbcrlf
response.write "orderConfFlowPauseSeconds:3, " & vbcrlf
response.write "postPurchaseFlow:false " & vbcrlf
response.write "};" & vbcrlf
response.write "document.write(unescape(""%3Cscript src='"" + document.location.protocol + ""//"" + turnToConfig.staticHost + ""/tra2/turntoFeed.js' type='text/javascript'%3E%3C/script%3E""));" & vbcrlf
response.write "document.write(unescape(""%3Cscript src='"" + document.location.protocol + ""//"" + turnToConfig.staticHost + ""/tra2/tra.js' type='text/javascript'%3E%3C/script%3E""));" & vbcrlf
response.write "</script>" & vbcrlf
response.write "<script type=""text/javascript"">" & vbcrlf
response.write "TurnToFeed.addFeedPurchaseOrder({orderId:'" & nOrderId & "',email:'" & sEmail & "',postalCode: '" & szip & "',firstName: '" & sFname & "', lastName: '" & sLname & "' });" & vbcrlf
response.write strTurnTo 
response.write "</script>" & vbcrlf
response.write "<script type=""text/javascript"">" & vbcrlf
response.write "TurnToFeed.sendFeed();" & vbcrlf
response.write "</script>" & vbcrlf

'response.write "<iframe src='https://pixel.fetchback.com/serve/fb/pdc?cat=&name=success&sid=1235&crv=" & round(totalFetchback,2) & "&oid=" & nOrderId & "' scrolling='no' width='1' height='1' marginheight='0' marginwidth='0' frameborder='0'></iframe>" & vbcrlf
%>
<!-- v:<%=vendororder%>-->
