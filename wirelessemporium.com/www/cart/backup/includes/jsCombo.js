function showLoginInfo() {
	document.getElementById("login_info").style.visibility = "visible";
}

function showBillingInfo() {
	checkboxVal = document.getElementById("sameaddress");
	if (checkboxVal.checked == true) {
		document.getElementById("billing_info").style.display = "block";
		document.getElementById("tbl_step2").style.backgroundImage = "url('/images/cart/bg_border_1_2.jpg')";
	}
	else {
		document.getElementById('billing_info').style.display='none';
		document.getElementById("tbl_step2").style.backgroundImage = "url('/images/cart/bg_border_1.jpg')";
	}
	ShipToBillPerson(document.frmProcessOrder);
}

function showPromoInfo() {
	document.getElementById("Promotions").style.display = "block";
}

<!--
function createCookie(name,value,days) 
{
	var expires;
	var date = new Date();
	
	if (days) 
	{
		date.setTime(date.getTime()+(days*24*60*60*1000));
		expires = "; expires="+date.toGMTString();
	}
	else 
	{
		expires = "";
	}

	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name)
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	
	for(var i=0;i < ca.length;i++) 
	{
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) 
{
	createCookie(name,"",-1);
}
//-->

<!--
function CheckValidNEW(strFieldName, strMsg) {
	if (strFieldName == "" || strFieldName == null || strFieldName == "NULL" || strFieldName == " ") {
		if (bValid) {
			alert(strMsg);
			bValid = false;
		}
	}
}

function CheckEMailNEW(addr) {
	var invalidChars = '\/\'\\ ";:?!()[]\{\}^|,';
	for (i=0; i<invalidChars.length; i++) {
		if (addr.indexOf(invalidChars.charAt(i),0) > -1) {
			if (bValid) {
				alert('E-mail address contains invalid characters');
				bValid = false;
			}
		}
	}
	for (i=0; i<addr.length; i++) {
		if (addr.charCodeAt(i)>127) {
			if (bValid) {
				alert("E-mail address contains non ascii characters.");
				bValid = false;
			}
		}
	}
	var atPos = addr.indexOf('@',0);
	if (atPos == -1) {
		if (bValid) {
			alert('E-mail address must contain an @');
			bValid = false;
		}
	}
	if (atPos == 0) {
		if (bValid) {
			alert('E-mail address must not start with @');
			bValid = false;
		}
	}
	if (addr.indexOf('@', atPos + 1) > - 1) {
		if (bValid) {
			alert('E-mail address must contain only one @');
			bValid = false;
		}
	}
	if (addr.indexOf('.', atPos) == -1) {
		if (bValid) {
			alert('E-mail address must contain a period in the domain name');
			bValid = false;
		}
	}
	if (addr.indexOf('@.',0) != -1) {
		if (bValid) {
			alert('Period must not immediately follow @ in e-mail address');
			bValid = false;
		}
	}
	if (addr.indexOf('.@',0) != -1) {
		if (bValid) {
			alert('Period must not immediately precede @ in e-mail address');
			bValid = false;
		}
	}
	if (addr.indexOf('..',0) != -1) {
		if (bValid) {
			alert('Two periods must not be adjacent in e-mail address');
			bValid = false;
		}
	}
	var suffix = addr.substring(addr.lastIndexOf('.')+1);
	if (suffix.length != 2 && suffix != 'com' && suffix != 'net' && suffix != 'org' && suffix != 'edu' && suffix != 'int' && suffix != 'mil' && suffix != 'gov' & suffix != 'arpa' && suffix != 'biz' && suffix != 'aero' && suffix != 'name' && suffix != 'coop' && suffix != 'info' && suffix != 'pro' && suffix != 'museum') {
		if (bValid) {
			alert('Invalid primary domain in e-mail address');
			bValid = false;
		}
	}
	return true;
}
//-->
