<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
	if instr(request.ServerVariables("HTTP_REFERER"),"://www.wirelessemporium.com") < 1 and instr(request.ServerVariables("HTTP_REFERER"),"://staging.wirelessemporium.com") < 1 then response.Redirect("/?ec=562")
	
	dim mySession, myAccount
	mySession = request.cookies("mySession")
	myAccount = request.cookies("myAccount")
	if mySession = "" then
		mySession = session.sessionid
		response.cookies("mySession") = mySession
		response.cookies("mySession").expires = dateAdd("m", 1, now)
	end if

	dim itemID, qty, itemPrice
	itemID = prepInt(request.form("prodid"))
	secondProductID = prepInt(request.Form("secondProductID"))
	secondProductPrice = prepInt(request.Form("secondProductPrice"))
	qty = prepInt(request.form("qty"))
	musicSkins = prepInt(request.Form("musicSkins"))
	itemPrice = ds(request.form("itemPrice"))
	
	if isnull(itemPrice) or len(itemPrice) < 1 or not isnumeric(itemPrice) then itemPrice = null

	if qty < 1 then qty = 1
	if qty > 25500 then qty = 25500
	if itemID > 0 then
		if musicSkins = 1 then
			SQL = "select cast('True' as bit) as alwaysInStock, 100 as inv_qty, b.id, b.itemID, b.qty from we_items_musicSkins a left join ShoppingCart b on a.id = b.itemID and musicSkins = 1 and sessionID = '" & mySession & "' and (purchasedOrderID IS NULL OR purchasedOrderID = 0) where a.id = " & itemID
		else
			SQL = "select c.alwaysInStock, (select top 1 inv_qty from we_items where partNumber = a.partNumber order by master desc, inv_qty desc) as inv_qty, b.id, b.itemID, b.qty from we_items a left join ShoppingCart b on a.itemID = b.itemID and musicSkins = " & musicSkins & " and sessionID = '" & mySession & "' and (purchasedOrderID IS NULL OR purchasedOrderID = 0) left join we_pnDetails c on a.partnumber = c.partnumber where a.itemID = " & itemID & " and a.hideLive = 0"
		end if
		session("errorSQL") = SQL
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 0, 1
		if not RS.eof then
			if isnull(rs("alwaysInStock")) then alwaysInStock = False else alwaysInStock = rs("alwaysInStock")
			if rs("inv_qty") > 0 or musicSkins = 1 or alwaysInStock then
				if not isnull(rs("id")) then
					if not isnull(itemPrice) then
						SQL = "UPDATE ShoppingCart SET qty = qty + " & qty & ", customCost = customCost + " & itemPrice & " WHERE id = '" & RS("id") & "'"
					else
						SQL = "UPDATE ShoppingCart SET qty = qty + " & qty & ", customCost = null WHERE id = '" & RS("id") & "'"
					end if 
					session("errorSQL") = sql
					oConn.execute SQL
					parentID = RS("id")
				else
					if isnull(itemPrice) then itemPrice = "null"
					
					SQL = "SET NOCOUNT ON; INSERT INTO ShoppingCart (store,sessionID,accountID,itemID,qty,dateEntd,customCost,musicSkins) VALUES (0,"
					SQL = SQL & "'" & mySession & "', "
					if myAccount <> "" then
						SQL = SQL & "'" & myAccount & "', "
					else
						SQL = SQL & "null, "
					end if
					SQL = SQL & "'" & itemID & "', "
					SQL = SQL & "'" & qty & "', "
					SQL = SQL & "'" & now & "', "
					SQL = SQL & itemPrice & "," & musicSkins & "); SELECT @@IDENTITY AS parentID;"		
					session("errorSQL") = sql
					set insertRS = oConn.execute(sql)
					dim newItemID : parentID = insertRS("parentID")
				end if
			else
				sql = "delete from ShoppingCart where itemID = " & itemID & " and (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
				session("userMsg") = "That part number is not currently available."
				session("errorSQL") = sql
				oConn.execute SQL
			end if
		else
			session("userMsg") = "That part number is not currently available"
		end if
		RS.close
		set RS = nothing
	end if
	
	'Add second item if required
	itemID = secondProductID
	itemPrice = secondProductPrice
	qty = 1
	if itemID > 0 then
		if musicSkins = 1 then
			SQL = "select cast('True' as bit) as alwaysInStock, 100 as inv_qty, b.id, b.itemID, b.qty from we_items_musicSkins a left join ShoppingCart b on a.id = b.itemID and musicSkins = 1 and sessionID = '" & mySession & "' and (purchasedOrderID IS NULL OR purchasedOrderID = 0) where a.id = " & itemID
		else
			SQL = "select c.alwaysInStock, (select top 1 inv_qty from we_items where partNumber = a.partNumber order by master desc, inv_qty desc) as inv_qty, b.id, b.itemID, b.qty from we_items a left join ShoppingCart b on a.itemID = b.itemID and musicSkins = " & musicSkins & " and sessionID = '" & mySession & "' and (purchasedOrderID IS NULL OR purchasedOrderID = 0) left join we_pnDetails c on a.partnumber = c.partnumber where a.itemID = " & itemID & " and a.hideLive = 0"
		end if
		session("errorSQL") = SQL
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 0, 1
		if not RS.eof then
			if isnull(rs("alwaysInStock")) then alwaysInStock = False else alwaysInStock = rs("alwaysInStock")
			if rs("inv_qty") > 0 or musicSkins = 1 or alwaysInStock then
				if not isnull(rs("id")) then
					if not isnull(itemPrice) then
						SQL = "UPDATE ShoppingCart SET qty = qty + " & qty & ", customCost = customCost + " & itemPrice & " WHERE id = '" & RS("id") & "'"
					else
						SQL = "UPDATE ShoppingCart SET qty = qty + " & qty & ", customCost = null WHERE id = '" & RS("id") & "'"
					end if 
				else
					if isnull(itemPrice) then itemPrice = "null"
					
					SQL = "INSERT INTO ShoppingCart (store,sessionID,accountID,itemID,qty,dateEntd,customCost,musicSkins,parentID) VALUES (0,"
					SQL = SQL & "'" & mySession & "', "
					if myAccount <> "" then
						SQL = SQL & "'" & myAccount & "', "
					else
						SQL = SQL & "null, "
					end if
					SQL = SQL & "'" & itemID & "', "
					SQL = SQL & "'" & qty & "', "
					SQL = SQL & "'" & now & "', "
					SQL = SQL & itemPrice & "," & musicSkins & "," & parentID & ")"		
				end if
			else
				sql = "delete from ShoppingCart where itemID = " & itemID & " and (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
				session("userMsg") = "That part number is not currently available."
			end if
			session("errorSQL") = sql
			oConn.execute SQL
		else
			session("userMsg") = "That part number is not currently available"
		end if
		RS.close
		set RS = nothing
	end if
	response.redirect "/cart/basket.asp"
%>