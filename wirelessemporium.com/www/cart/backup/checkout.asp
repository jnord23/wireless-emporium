<%
pageName = "checkout"
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

refreshForLogin = request.QueryString("l")
if isnull(refreshForLogin) or len(refreshForLogin) < 1 then refreshForLogin = 0
noCommentBox = true

'Allow for logins and promo codes
if refreshForLogin = 1 then
	response.Cookies("checkoutPage") = ""
	response.Cookies("checkoutPage").expires = date - 1
end if

dim curSite

if instr(request.servervariables("SERVER_NAME"),"staging") > 0 then
	curSite = "http://staging"
else
	curSite = "https://www"
end if

if Request.ServerVariables("SERVER_PORT_SECURE") = "1" then
	strHttp = "https"
else
	strHttp = "http"
end if

dim cart
cart = 1

varientTopBanner = 2
varientTopNav = 1
varientAssurance = 0
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
dim SEtitle, SEdescription, SEkeywords
SEtitle = ""
SEdescription = ""
SEkeywords = ""

dim pageTitle, strBody
pageTitle = ""
strBody = "<body leftmargin=""0"" topmargin=""0"" onload=""changeTax(document.frmProcessOrder.sState.value);"">"
%>
<!--#include virtual="/includes/template/top_cart.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<%
dim HTTP_REFERER
HTTP_REFERER = request.servervariables("HTTP_REFERER")
'if left(HTTP_REFERER,47) <> "http://www.wirelessemporium.com/cart/basket.asp" and left(HTTP_REFERER,48) <> "https://www.wirelessemporium.com/cart/basket.asp" and left(HTTP_REFERER,50) <> "https://www.wirelessemporium.com/cart/checkout.asp" and left(HTTP_REFERER,51) <> "http://staging.wirelessemporium.com/cart/basket.asp" and left(HTTP_REFERER,53) <> "http://staging.wirelessemporium.com/cart/checkout.asp" and refreshForLogin = 0 then
'	response.write "<h3>This page will only accept forms submitted from the Wireless Emporium secure website.</h3><!-- " & request.ServerVariables("HTTP_REFERER") & " -->" & vbcrlf
'	response.end
'end if

dim nOrderId, a

dim szip, sWeight
szip = request.form("shipZip")

dim sUserId
if sUserId = "" then
	'check if the user is logging in
	if request.form("submitted") = "submitted" and request.form("loginEmail") <> "" then
		SQL = "SELECT pword,fname,lname,accountid FROM we_accounts"
		SQL = SQL & " WHERE email='" & SQLquote(request.form("loginEmail")) & "' AND pword <> '' AND pword IS NOT null"
		SQL = SQL & " ORDER BY accountid DESC"
		call fOpenConn()
		set RS = oConn.execute(SQL)
		if not RS.eof then
			if SQLquote(request.form("pword1")) = RS("pword") then
				myAccount = RS("accountid")
				on error resume next
				response.cookies("fname") = RS("fname")
				response.cookies("fname").expires = dateAdd("m", 1, now)
				response.cookies("myAccount") = RS("accountid")
				response.cookies("myAccount").expires = dateAdd("m", 1, now)
				on error goto 0
			end if
		end if
		RS.close
		set RS = nothing
		call fCloseConn()
	end if
end if

dim newEmail, email, pword1, pword2, phone, Fname, Lname, sAddress1, sAddress2, sCity, sState, bAddress1, bAddress2, bCity, bState, bZip
dim radioAddress, shiptype, PaymentType, cc_cardType, cc_cardOwner, cardNum, cc_month, cc_year, hearFrom

'check if the user is already logged in
if myAccount <> "" then
	call fOpenConn()
	SQL = "SELECT * FROM we_accounts WHERE accountid = '" & myAccount & "'"
	set RS = oConn.execute(SQL)
	if not RS.eof then
		email = RS("email")
		fname = RS("fname")
		lname = RS("lname")
		phone = RS("phone")
		saddress1 = RS("saddress1")
		saddress2 = RS("saddress2")
		scity = RS("scity")
		sstate = RS("sstate")
		szip = RS("szip")
		bAddress1 = RS("bAddress1")
		bAddress2 = RS("bAddress2")
		bCity = RS("bCity")
		bState = RS("bState")
		bZip = RS("bZip")
	end if
	RS.close
	set RS = nothing
	call fCloseConn()
	radioAddress = request.form("radioAddress")
	shiptype = request.form("shiptype")
	if request.form("szip") <> "" then szip = request.form("szip")
end if

if request.form("submitted") = "submitted" and myAccount = "" then
	newEmail = request.form("newEmail")
	pword1 = request.form("pword1")
	pword2 = request.form("pword2")
	fname = request.form("fname")
	lname = request.form("lname")
	email = request.form("email")
	phone = request.form("phone")
	saddress1 = request.form("saddress1")
	saddress2 = request.form("saddress2")
	scity = request.form("scity")
	sstate = request.form("sstate")
	szip = request.form("szip")
	bAddress1 = request.form("bAddress1")
	bAddress2 = request.form("bAddress2")
	bCity = request.form("bCity")
	bState = request.form("bState")
	bZip = request.form("bZip")
	radioAddress = request.form("radioAddress")
	shiptype = request.form("shiptype")
	PaymentType = request.form("PaymentType")
	cc_cardType = request.form("cc_cardType")
	cc_cardOwner = request.form("cc_cardOwner")
	cardNum = request.form("cardNum")
	cc_month = request.form("cc_month")
	cc_year = request.form("cc_year")
	hearFrom = request.form("hearFrom")
end if

' START basket grab
dim sItemName, sRetailPrice, sItemPrice, sItemPic, nProdIdCheck, nProdQuantity, nProdIdCount, strItemCheck, strQty, nItemTotal
dim WEhtml, WEhtml2, PPhtml, EBtemp, EBxml

nProdIdCount = 0
strItemCheck = ""
strQty = ""
discountAmt = 0

call fOpenConn()
' New cart abandonment tracking added 10/29/2010 by MC
SQL = "UPDATE ShoppingCart SET DateCheckout = '" & now & "' WHERE store = 0 AND sessionID = '" & mySession & "' AND (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
oConn.execute SQL

'SQL = "SELECT a.musicSkins, a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, B.itemDesc, B.itemPic, B.price_Our, B.price_retail, B.Condition, B.itemWeight, B.NoDiscount"
'SQL = SQL & " FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID"
'SQL = SQL & " WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"

SQL = "SELECT c.brandName, d.modelName, a.musicSkins, a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, B.itemDesc, B.itemPic, B.price_Our, B.price_retail, B.Condition, B.itemWeight, B.NoDiscount FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID and a.musicSkins = 0 left join we_Brands c on b.brandID = c.brandID left join we_Models d on b.modelID = d.modelID WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
SQL = SQL & " union"
SQL = SQL & " SELECT c.brandName, d.modelName, a.musicSkins, a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, 3 as typeID, B.musicSkinsID as PartNumber, isnull(c.brandName, '') + ' ' + isnull(d.modelName, '') + ' ' + isnull(B.artist, '') + ' ' + isnull(b.designName, '') as itemDesc, B.image as itemPic, b.price_we as price_Our, b.msrp as price_retail, 0 as Condition, 1 as itemWeight, 0 as NoDiscount FROM ShoppingCart A INNER JOIN we_items_musicSkins B ON A.itemID=B.id and a.musicSkins = 1 left join we_Brands c on b.brandID = c.brandID left join we_Models d on b.modelID = d.modelID WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"

set RS = Server.CreateObject("ADODB.Recordset")

RS.open SQL, oConn, 0, 1

dim itemCnt : itemCnt = 0
dim noSR : noSR = 0
do until RS.eof
	noDiscount = RS("NoDiscount")
	if isnumeric(noDiscount) then
		if noDiscount = 0 then noDiscount = false else noDiscount = true
	end if
	nProdIdCheck = RS("itemID")
	nProdQuantity = RS("qty")
	if isnull(rs("musicSkins")) or not rs("musicSkins") then nProdMusicSkins = 0 else nProdMusicSkins = 1
	strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
	strQty = strQty & cStr(nProdQuantity)& ","  ' QTY string for each item, this is for buySafe Use
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		useBrandName = RS("brandName")
		useModelName = RS("modelName")
		partNumber = RS("partNumber")
		sItemName = insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName)
		sItemPic = RS("itemPic")
		sItemPrice = RS("price_Our")
		
		if isDropship(partNumber) or left(partNumber,3) = "WCD" then
			if left(partNumber,3) = "WCD" then noSR = 1
			srEligible = 0
		else
			if noSR = 0 then srEligible = 1
		end if

		if not isnull(rs("customCost")) then sItemPrice = rs("customCost")
		sRetailPrice = prepInt(RS("price_retail"))
		'Free Bluetooth
		if FreeBluetooth = 1 then
			if cStr(nProdIdCheck) = FreeBluetoothItemID then
				FreeBluetoothPrice = sItemPrice
			end if
		end if
		'Free Product With Phone Purchase
		if RS("typeID") = 16 then PhonePurchased = 1
		'Category Sale
		if CategorySale = 1 then
			if RS("typeID") = CategorySaleTypeID and RS("NoDiscount") = 0 then
				CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
			else
				SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
				SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_Our > 0)"
				SQL = SQL & " AND (R.typeid = '" & CategorySaleTypeID & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
				SQL = SQL & " AND I.NoDiscount = 0"
				dim RSrelated
				set RSrelated = Server.CreateObject("ADODB.Recordset")
				RSrelated.open SQL, oConn, 0, 1
				if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				RSrelated.close
				set RSrelated = nothing
			end if
		end if
		'session("errorSQL") = "nSubTotal = " & nSubTotal & " | nProdIdCheck = " & nProdIdCheck & " | sItemPrice = " & sItemPrice & " | nProdQuantity = " & nProdQuantity
		nItemTotal = nItemTotal + (sItemPrice * nProdQuantity)
		if not noDiscount and rs("typeID") <> 16 then
			discountAmt = discountAmt + (sItemPrice * nProdQuantity)
		end if
		sWeight = sWeight + (cDbl(RS("itemWeight")) * nProdQuantity)
		'WE shipping section
		if sItemPrice = 0 then showPrice = "FREE" else showPrice = formatCurrency(sItemPrice * nProdQuantity)
		productList = productList & insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName) & "##" & nProdQuantity & "##" & showPrice & "##" & srEligible & "@@"
		WEhtml = WEhtml & "<tr><td align=""left"">" & insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName) & "</td><td align=""center"">" & nProdQuantity & "</td><td align=""right"" colspan='2'>" & showPrice & "</td></tr>" & vbcrlf
		WEhtml2 = WEhtml2 & "<input type=""hidden"" name=""itemID_" & nProdIdCount & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""itemQty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		
		PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & sItemName & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & nProdQuantity & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & sItemPrice & """>" & vbcrlf

		'eBillme variables
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_number_" & nProdIdCount & """ value=""" & nProdIdCheck & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_qty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_musicSkins_" & nProdIdCount & """ value=""" & nProdMusicSkins & """>" & vbcrlf
		EBxml = EBxml & "			<itemdetail>" & vbcrlf
		EBxml = EBxml & "				<id>" & nProdIdCheck & "</id>" & vbcrlf
		EBxml = EBxml & "				<name>" & insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName) & "</name>" & vbcrlf
		EBxml = EBxml & "				<quantity>" & nProdQuantity & "</quantity>" & vbcrlf
		EBxml = EBxml & "				<itemcost>" & sItemPrice & "</itemcost>" & vbcrlf
		EBxml = EBxml & "			</itemdetail>"
		
		itemCnt = itemCnt + nProdQuantity
	end if
	RS.movenext
loop
RS.close
set RS = nothing
call fCloseConn()
if nProdIdCount < 1 then
	'basket currently empty
	response.redirect("/sessionexpired.asp")
	response.end
end if
' END basket grab

'GET COUPON VALUES
dim sPromoCode
if request.form("promo") <> "" then
	sPromoCode = SQLquote(request.form("promo"))
else
	sPromoCode = session("promocode")
end if

response.Write("<!-- sPromoCode:" & sPromoCode & " -->")

if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	call fOpenConn()
	SQL = "SELECT * FROM WE_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	response.Write("<!-- " & SQL & " -->")
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		FreeItemPartNumber = RS("FreeItemPartNumber")
		setValue = RS("setValue")
		oneTime = RS("oneTime")
		response.Write("<!-- details set (couponid:" & couponid & ") -->")
	else
		sPromoCode = ""
	end if
	call fCloseConn()
end if

dim nSubTotal
nSubTotal = nItemTotal

dim shipcost0, shipcost2, shipcost3, shipcost4, shipcost5
shipcost0 = "0.00"
if now > cdate("12/13/2011") and date < cdate("12/19/2011") and prepInt(nSubTotal) > 29.99 then
	shipcost2 = 0
	shiptype = 2	
else
	shipcost2 = 6.99
end if
shipcost3 = "24.99"
shipcost4 = "0.00"
shipcost5 = "10.99"

if sWeight / 16 < 1 then
	sWeight = 1
else
	sWeight = sWeight / 16
end if

if strItemCheck <> "" then strItemCheck = left(strItemCheck,len(strItemCheck)-1)

if nProdIdCount = 0 then
	htmBasketRows = htmBasketRows & "<tr><td colspan=""7""><i>Basket is currently empty.</i></td></tr>" & vbcrlf
	EmptyBasket = true
end if
%>
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->
<%
discountTotal = prepInt(discountTotal)
maxDiscount = discountTotal
if discountTotal > nItemTotal then
	discountTotal = nItemTotal
end if
maxDiscount = prepInt(maxDiscount)
activeTotal = nItemTotal - discountTotal

if instr(discountTotal,".") > 0 then discountTotal = left(discountTotal,instr(discountTotal,".")+2)
if oneTime then nsubTotal = nsubTotal - discountTotal
%>
<script language="javascript">
<!-- Begin
function changeValue(span,val) {
	if (document.all) {
		eval(span).innerHTML = val;
	} else if (document.getElementById) {
		document.getElementById(span).innerHTML = val;
	}
}
	
function changeTax(state) {
	//var shippingOptions = document.frmProcessOrder.shippingOptions.value;
	var nCATax = CurrencyFormatted(<%=activeTotal%> * 0.0775);
	if (state == "CA") {
		changeValue("CAtaxMsg","Additional 7.75% Tax for Shipments Within CA");
		changeValue("CAtax","$" + nCATax);
		document.frmProcessOrder.nCATax.value = nCATax;
	} else {
		changeValue("CAtaxMsg","");
		changeValue("CAtax","$0.00");
		document.frmProcessOrder.nCATax.value = 0;
	}
	/*
	if (state == "AB" || state == "BC" || state == "MB" || state == "NB" || state == "NL" || state == "NS" || state == "NT" || state == "NU" || state == "ON" || state == "PE" || state == "QC" || state == "SK" || state == "YT") {
		changeValue("Shipping0","USPS First Class Int'l (8-12 business days)");
		document.frmProcessOrder.shipcost2.value = "<%=shipcost5%>";
		changeValue("Shipping2","USPS Priority Int'l (3-8 business days) - $<%=shipcost5%>");
		if (document.frmProcessOrder.shiptype[2].checked == true) {
			document.frmProcessOrder.shiptype[2].checked = false;
			document.frmProcessOrder.shiptype[1].checked = true;
		}
		for (var i = 2; i < shippingOptions; i++) {
			document.frmProcessOrder.shiptype[i].disabled = true;
			changeValue("Shipping" + (i+1),"");
		}
	} else {
		changeValue("Shipping0","USPS First Class (4-10 business days)");
		alert(document.frmProcessOrder.shipcost2.value)
		document.frmProcessOrder.shipcost2.value = '<%=shipcost2%>';
		changeValue("Shipping2","USPS Priority Mail (2-4 business days) - $<%=shipcost2%>");
		document.frmProcessOrder.shiptype[2].disabled = false;
		changeValue("Shipping3","USPS Express Mail (1-2 business days) - $<%=shipcost3%>");
	}
	*/
	updateGrandTotal(document.frmProcessOrder.shiptypeID.value, document.frmProcessOrder.shippingTotal.value);
}

function updateGrandTotal(shipID,shipAmt) {
	document.getElementById("shipcost").innerHTML = "$" + CurrencyFormatted(shipAmt)
	document.frmProcessOrder.shippingTotal.value = shipAmt;
	document.frmProcessOrder.shiptypeID.value = shipID;
	//var GrandTotal = Number(document.frmProcessOrder.subTotal.value) + Number(document.frmProcessOrder.nCATax.value) + Number(b.value);
	var GrandTotal = Number(document.frmProcessOrder.subTotal.value) + Number(document.frmProcessOrder.nCATax.value) + Number(shipAmt);
	var maxDiscount = <%=maxDiscount%>;
	if ((GrandTotal - maxDiscount) < 0) 
	{
//		document.getElementById("discountAmt").innerHTML = "$" + CurrencyFormatted(GrandTotal)
//		document.getElementById("discountTotal").value = CurrencyFormatted(GrandTotal)
		changeValue('discountAmt', "$" + CurrencyFormatted(GrandTotal));
		document.frmProcessOrder.discountTotal.value = CurrencyFormatted(GrandTotal);
		GrandTotal = 0
	}
	else 
	{
		if (maxDiscount > 0) 
		{
//			document.getElementById("discountAmt").innerHTML = "$" + CurrencyFormatted(maxDiscount)
//			document.getElementById("discountTotal").value = CurrencyFormatted(maxDiscount)
			changeValue('discountAmt', "$" + CurrencyFormatted(maxDiscount));
			document.frmProcessOrder.discountTotal.value = CurrencyFormatted(maxDiscount);
		}
		GrandTotal = GrandTotal - maxDiscount
	}

	changeValue("GrandTotal","$" + CurrencyFormatted(GrandTotal));
	document.frmProcessOrder.grandTotal.value = CurrencyFormatted(GrandTotal);
}

function changeShippingAddress(a) {
	var b = eval('document.frmProcessOrder.sAddress1' + a);
	var c = eval('document.frmProcessOrder.sAddress2' + a);
	var d = eval('document.frmProcessOrder.sCity' + a);
	var e = eval('document.frmProcessOrder.sState' + a);
	var f = eval('document.frmProcessOrder.sZip' + a);
	var g = eval('document.frmProcessOrder.sID' + a);
	document.frmProcessOrder.sAddress1.value = b.value;
	document.frmProcessOrder.sAddress2.value = c.value;
	document.frmProcessOrder.sCity.value = d.value;
	document.frmProcessOrder.sState.value = e.value;
	document.frmProcessOrder.sZip.value = f.value;
	changeTax(e.value);
}

function CheckSubmit() {
	var f = document.frmProcessOrder;
	bValid = true;
	<%if myAccount = "" then%>
	if (f.sameaddress.checked == 0) {
		ShipToBillPerson(f);
	}
	<% end if %>
	CheckValidNEW(f.email.value, "Email is a required field!");
	CheckValidNEW(f.fname.value, "Your First Name is a required field!");
	CheckValidNEW(f.lname.value, "Your Last Name is a required field!");
	CheckValidNEW(f.phoneNumber.value, "Your Phone Number is a required field!");
	CheckValidNEW(f.sAddress1.value, "Your Address (for Shipping Address) is a required field!");
	CheckValidNEW(f.sCity.value, "Your City (for Shipping Address) is a required field!");
	sString = f.sState.options[f.sState.selectedIndex].value;
	CheckValidNEW(sString, "State (for Shipping Address) is a required field!");
	CheckValidNEW(f.sZip.value, "Your Zip Code (for Shipping Address) is a required field!");
	if (f.sZip.value.length < 5) {
		alert("You must enter a valid US or Canadian postal code")
		bValid = false
	}
	if (f.sameaddress.checked == true) {
		CheckValidNEW(f.bAddress1.value, "Your Address (for Billing Address) is a required field!");
		CheckValidNEW(f.bCity.value, "Your City (for Billing Address) is a required field!");
		sString = f.bState.options[f.bState.selectedIndex].value;
		CheckValidNEW(sString, "State (for Billing Address) is a required field!");
		CheckValidNEW(f.bZip.value, "Your Zip Code (for Billing Address) is a required field!");
		if (f.bZip.value.length < 5) {
			alert("You must enter a valid US or Canadian postal code")
			bValid = false
		}
	}

	var b = ""
	if (f.PaymentType.checked == true) { b = "eBillMe" }
	
	if (b != 'eBillMe' && b != 'PAY') {
		sString = f.cc_cardType.options[f.cc_cardType.selectedIndex].value;
		CheckValidNEW(sString, "Credit Card Type is a required field!");
		CheckValidNEW(f.cc_cardOwner.value, "Your Name (as it appears on your credit card) is a required field!");
		CheckValidNEW(f.cardNum.value, "Your Credit Card Number is a required field!");
		CheckValidNEW(f.cc_month.value, "Your Credit Card Expiration Month is a required field!");
		CheckValidNEW(f.cc_year.value, "Your Credit Card Expiration Year is a required field!");
		CheckValidNEW(f.secCode.value, "Your Credit Card Security Code is a required field!");
		
		if (bValid) {
			document.frmProcessOrder.action = '/framework/processing.asp';			
//			document.frmProcessOrder.action = '/cart/process/processing.asp';
			//document.frmProcessOrder.action = 'http://staging.wirelessemporium.com/cart/process/processing.asp';
			document.frmProcessOrder.submit();
			document.getElementById("submitBttn1").style.display = 'none'
			document.getElementById("submitBttn2").style.display = ''
		}
	}
	else {
		if (bValid) {
			document.frmProcessOrder.action = '/cart/process/eBillMe/eBillMe.asp';
			//document.frmProcessOrder.action = 'http://staging.wirelessemporium.com/cart/process/processing.asp';
			document.frmProcessOrder.submit();
			document.getElementById("submitBttn1").style.display = 'none'
			document.getElementById("submitBttn2").style.display = ''
		}
	}
	return false;
}

var bAddress1 = "";
var bAddress2 = "";
var bCity = "";
var bZip = "";
var bState = "";
var bStateIndex = "0";

function ShipToBillPerson(form) {
	if (form.accountid.value == '') {
		if (form.sameaddress.checked) {
			form.bAddress1.value = "";
			form.bAddress2.value = "";
			form.bCity.value = "";
			form.bZip.value = "";
			form.bState.selectedIndex = form.sState.options[0];
		}
		else {
			form.bAddress1.value = form.sAddress1.value;
			form.bAddress2.value = form.sAddress2.value;
			form.bCity.value = form.sCity.value;
			form.bZip.value = form.sZip.value;
			form.bState.selectedIndex = form.sState.selectedIndex;
		}
	}
}

function CurrencyFormatted(amount) {
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt(i * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}

function getNewShipping()
{
	document.getElementById('dumpZone').innerHTML = "";	
	zipcode 			=	document.frmProcessOrder.sZip.value;
	curShippingTotal	=	document.frmProcessOrder.shippingTotal.value;
	curShippingType		=	document.frmProcessOrder.shiptypeID.value;

	updateURL = '/ajax/shippingDetails.asp?zip=' + zipcode + '&weight=<%=sWeight%>&shiptype=' + curShippingType + '&nTotalQuantity=<%=itemCnt%>&nSubTotal=<%=nSubTotal%>';
	ajax(updateURL,'dumpZone');
	checkUpdate('newShipping');
}

function checkUpdate(checkType)
{
	if (checkType == "newShipping")
	{
		if ("" == document.getElementById('dumpZone').innerHTML)
		{
			setTimeout('checkUpdate(\'newShipping\')', 50);
		} else
		{
			document.getElementById('shippingDetails').innerHTML = document.getElementById('dumpZone').innerHTML;
			newShipType = getSelectedRadioValue(document.frmProcessOrder.shiptype);
			if (newShipType == "") 
			{
				document.frmProcessOrder.shiptype[0].checked = true;
				updateGrandTotal(0,<%=shipcost0%>);
			} else
			{
				updateGrandTotal(newShipType,eval('document.frmProcessOrder.shipcost' + newShipType).value);
			}
		}
	}
}

function getSelectedRadioValue(buttonGroup) {
    var i = getSelectedRadio(buttonGroup);
   if (i == -1) {
      return "";
   } else {
      if (buttonGroup[i]) { // Make sure the button group is an array (not just one button)
         return buttonGroup[i].value;
      } else { // The button group is just the one button, and it is checked
         return buttonGroup.value;
      }
   }
}

function getSelectedRadio(buttonGroup) {
   if (buttonGroup[0]) {
      for (var i=0; i<buttonGroup.length; i++) {
         if (buttonGroup[i].checked) {
            return i
         }
      }
   } else {
      if (buttonGroup.checked) { return 0; } // if the one button is checked, return zero
   }
   return -1;
}

//  End -->
</script>
<form action="/cart/checkout.asp?l=1" method="post" name="frmProcessOrder">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="padding-bottom:20px;">
    <tr><td valign="top" bgcolor="#FFFFFF" align="center"><img src="/images/cart/we_step2.png" border="0" alt="Checkout - Step 2 of 3"></td></tr>
    <tr>
        <td valign="top" bgcolor="#FFFFFF" align="center">
            <input type="hidden" name="shippingTotal" value="">
            <input type="hidden" name="shiptypeID" value="<%=shiptype%>">
            <input type="hidden" name="sWeight" value="<%=sWeight%>">
            
            <input type="hidden" name="sPromoCode" value="<%=sPromoCode%>">
            <input type="hidden" name="nPromo" value="<%=couponid%>">
            <input type="hidden" name="discountTotal" value="<%=discountTotal%>">
            
            <input type="hidden" name="nCATax" value="<%=nCATax%>">
            <input type="hidden" name="subTotal" value="<%=nItemTotal%>">
            <input type="hidden" name="grandTotal" value="<%=nAmount%>">
            <table width="800" border="0" cellspacing="0" cellpadding="0">
                <tr><td colspan="3"><img src="/images/cart/we_header_SecureCheckout.jpg" width="800" height="39" border="0" alt="Secure Checkout"></td></tr>
                <tr>
                    <td colspan="3"><img src="/images/cart/border_top.jpg" width="800" height="7" border="0"></td>
                </tr>
                <tr>
                    <td valign="top" width="7" background="/images/cart/border_left.jpg"><img src="/images/spacer.gif" width="7" height="5" border="0"></td>
                    <td align="center" width="781">
                        <% lineColor = "#666" %>
                        <table width="781" border="0" cellspacing="0" cellpadding="0" style="background-color:#cddeee;">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="3" class="normalText">
                                        <tr>
                                            <td align="left" width="100%" style="border-bottom:2px solid <%=lineColor%>;"><b>Item</b></td>
                                            <td align="center" style="border-bottom:2px solid <%=lineColor%>; border-left:2px solid <%=lineColor%>; border-right:2px solid <%=lineColor%>;"><div style="width:50px; font-weight:bold;">Qty</div></td>
                                            <td align="right" colspan="2" style="border-bottom:2px solid <%=lineColor%>;"><b>Cost</b></td>
                                        </tr>
                                        <%
                                        productArray = split(productList,"@@")
										dim activeProductCnt : activeProductCnt = 0
										dim eligibleCnt : eligibleCnt = 0
                                        for i = 0 to (ubound(productArray)-1)
                                            curArray = split(productArray(i),"##")
											activeProductCnt = activeProductCnt + 1
											if prepInt(curArray(3)) = 1 then eligibleCnt = eligibleCnt + 1
                                        %>
                                        <tr>
                                            <td align="left" style="border-bottom:2px solid <%=lineColor%>; line-height:20px;">
												<div style="float:left;"><%=curArray(0)%></div>
                                                <% if prepInt(curArray(3)) = 1 then %>
                                                <div style="float:left; padding-left:10px;">
	                                                <div name="sr_catalogProductGridDiv"></div>
                                                </div>
                                                <% end if %>
                                            </td>
                                            <td align="center" style="border-bottom:2px solid <%=lineColor%>; border-left:2px solid <%=lineColor%>; border-right:2px solid <%=lineColor%>;"><%=curArray(1)%></td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;" colspan="2"><%=curArray(2)%></td>
                                        </tr>
                                        <%
                                        next
                                        %>
                                        <tr>
                                            <td colspan="2" style="border-right:2px solid <%=lineColor%>; border-bottom:2px solid <%=lineColor%>;">&nbsp;</td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b>Subtotal:</b></td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b><%=formatCurrency(nItemTotal)%></b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="left" style="border-bottom:2px solid <%=lineColor%>; border-right:2px solid <%=lineColor%>;">&nbsp;</td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b>Discount:</b></td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><div style="font-weight:bold;" id="discountAmt"><%=formatCurrency(discountTotal * -1)%></div></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="border-right:2px solid <%=lineColor%>;">&nbsp;</td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b>Tax:</b><span id="CAtaxMsg"></span></td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b><span id="CAtax">$0.00</span></b></td>
                                        </tr>
                                        <% if prepStr(request.Cookies("srLogin")) = "" or eligibleCnt = 0 then %>
                                        <tr>
                                            <td colspan="2" style="border-right:2px solid <%=lineColor%>;">&nbsp;</td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;"><b>Shipping:</b></td>
                                            <td align="right" style="border-bottom:2px solid <%=lineColor%>;">
                                                <b><span id="shipcost"><%=formatCurrency(shipcost)%></span></b>
                                            </td>
                                        </tr>
                                        <% else %>
                                        <tr>
                                            <td colspan="2" style="border-right:2px solid <%=lineColor%>;">&nbsp;</td>
                                            <td colspan="2" align="right" style="border-bottom:2px solid <%=lineColor%>;"><div name="sr_cartSummaryDiv"></div></td>
                                        </tr>
                                        <% end if %>
                                        <%
                                        dim nAmount, nCATax
                                        nSubTotal = nItemTotal - discountTotal
                                        if nSubTotal = nItemTotal and discountTotal > 0 then nSubTotal = nSubTotal - discountTotal
                                        nAmount = cDbl(shipcost) + nCATax + nSubTotal
                                        %>
                                        <tr>
                                            <td align="left" colspan="2" style="border-right:2px solid <%=lineColor%>;">&nbsp;</td>
                                            <td align="right" nowrap="nowrap" style="color:#F00;"><b>Grand Total:</b></td>
                                            <td align="right"><b><span id="GrandTotal" style="color:#F00;"><%=formatCurrency(nAmount)%></span></b></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" width="12" background="/images/cart/border_right.jpg"><img src="/images/spacer.gif" width="12" height="5" border="0"></td>
                </tr>
                <tr>
                    <td colspan="3"><img src="/images/cart/border_bottom.jpg" width="800" height="12" border="0"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <div style="position:relative;">
                <div><img src="/images/cart/we_header_Shipping.jpg" width="800" height="39" border="0" alt="Shipping Information"></div>
                <% if myAccount = "" then %><!--<div style="position:absolute; top:14px; left:380px; font-size:14px;">Already Have An Account? - <a href="javascript:showLoginInfo();">Login Now</a></div>--><% end if %>
            </div>
        </td>
    </tr>
    <tr>
        <td align="center">
            <%if myAccount = "" then%>
            <table id="tbl_step2" width="800" height="465" border="0" cellpadding="3" cellspacing="0" class="smlText" style="background-image: url('/images/cart/bg_border_1.jpg'); background-repeat: no-repeat; background-position: center bottom; padding-left:10px;">
                <tr>
                    <td colspan="5" style="padding-top: 10px; color:#666666; font-weight:bold;" class="normalText" valign="top">
                        <div style="float:left;">&nbsp;&nbsp;Shipping&nbsp;Address:</div>
                    </td>
                </tr>
                <tr>
                    <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Email:</strong></td>
                    <td align="left" colspan="3"><input type="text" name="email" size="30" value="<%=email%>" onchange="getCustomerData(this.value)"></td>
                    <td rowspan="11" width="100%" valign="top" align="right" style="border-left:2px solid #ff6600; padding-right:10px;">
                        <table border="0" cellpadding="0" cellspacing="0" style="padding:5px;" width="100%">
                            <% if prepStr(request.Cookies("srLogin")) = "" or eligibleCnt = 0 then %>
                            	<% if eligibleCnt > 0 then %>
                            <tr><td align="center" style="padding-top:5px;"><div style="width:300px; margin-bottom:10px; height:30px;"><div name="sr_shippingOptionDiv"></div></div></td></tr>
                            	<% end if %>
                                <% if noSR = 1 then %>
                            <tr><td align="center" style="padding-top:5px;"><div style="width:300px; margin-bottom:10px; height:30px; font-weight:bold; border-bottom:1px solid #333; padding-bottom:10px;">Please allow up to 5-7 business days for production of custom cases prior to shipping time.</div></td></tr>
                                <% end if %>
                            <tr><td align="center"><img src="/images/icons/shippingHeader.jpg" border="0" title="Shipping Options" /></td></tr>
                            <tr>
                                <td align="left" style="font-size:12px;">
                                    <input type="hidden" name="shipcost0" value="<%=shipcost0%>"><input type="radio" name="shiptype" id="rad_ship1" value="0" onClick="updateGrandTotal(this.value,'<%=shipcost0%>');"<%if shiptype = "0" or shiptype = "" then response.write " checked"%>><span style="color:#db3300;font-weight:bold;">FREE SHIPPING</span> <span id="Shipping0">USPS First Class (4-10 business days)</span><br>
                                    <% if shipcost2 = 0 then %>
                                    <input type="hidden" name="shipcost2" value="<%=shipcost2%>"><input type="radio" name="shiptype" id="rad_ship2" value="2" onClick="updateGrandTotal(this.value,'<%=shipcost2%>');"<%if shiptype = "2" then response.write " checked"%>><span style="color:#db3300;font-weight:bold;">FREE SHIPPING</span> <span id="Shipping2">USPS Priority Mail (2-4 business days) - <%=formatcurrency(shipcost2)%></span><br>
									<% else %>
                                    <input type="hidden" name="shipcost2" value="<%=shipcost2%>"><input type="radio" name="shiptype" id="rad_ship2" value="2" onClick="updateGrandTotal(this.value,'<%=shipcost2%>');"<%if shiptype = "2" then response.write " checked"%>><span id="Shipping2">USPS Priority Mail (2-4 business days) - <%=formatcurrency(shipcost2)%></span><br>
                                    <% end if %>
                                    <input type="hidden" name="shipcost3" value="<%=shipcost3%>"><input type="radio" name="shiptype" id="rad_ship3" value="3" onClick="updateGrandTotal(this.value,'<%=shipcost3%>');"<%if shiptype = "3" then response.write " checked"%>><span id="Shipping3">USPS Express Mail (1-2 business days) - $24.99</span>
                                    <%
                                    on error resume next
									%>
                                    <div id="shippingDetails">
                                    <%
                                    if sZip <> "" then
										response.write UPScode(sZip,sWeight,shiptype,nTotalQuantity,0,0)
									else
										response.Write("<div style='font-weight:bold; padding-top:10px; width:100%; text-align:center;'>Enter your zipcode for more shipping options</div>")
									end if
									%>
                                    </div>
                                    <%
                                    on error goto 0
                                    dim shipcost
                                    shipcost = request.form("shipcost" & request.form("shiptype"))
                                    %>
                                </td>
                            </tr>
                            <%
							else
								session("sr_token") = prepStr(request.Cookies("srLogin"))
							%>
                            <tr><td align="center" style="font-weight:bold; font-size:16px; border-bottom:1px solid #CCC;">Shipping Options</td></tr>
                            	<% if eligibleCnt = activeProductCnt then %>
                            <tr><td align="center" style="padding-top:5px;"><div name="sr_shippingOptionDiv"></div></td></tr>
                                <%
								else
								%>
                            <tr>
                            	<td align="left" style="padding:5px;">
                                    <div>ShopRunner Eligible Products</div>
                                	<div name="sr_shippingOptionDiv"></div>
                                    <div>Non-ShopRunner Eligible Products</div>
                                	<div><input type="hidden" name="shipcost0" value="<%=shipcost0%>"><input type="radio" name="shiptype" id="rad_ship1" value="0" onClick="updateGrandTotal(this.value,'<%=shipcost0%>');"<%if shiptype = "0" or shiptype = "" then response.write " checked"%>><span style="color:#db3300;font-weight:bold;">FREE SHIPPING</span> <span id="Shipping0">USPS First Class (4-10 business days)</span></div>
                            	</td>
                            </tr>
                            	<% end if %>
							<% end if %>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right" width="100"><strong><font color="red" size="+1">*</font>&nbsp;First&nbsp;Name:</strong></td>
                    <td align="left" width="180" colspan="3">
                        <input type="hidden" name="accountid" value="">
                        <input type="hidden" name="parentAcctID" value="">
                        <input type="text" name="fname" size="30" value="<%=fname%>" onchange="newAcctInfo()">
                    </td>
                </tr>
                <tr>
                    <td align="right" width="80"><strong><font color="red" size="+1">*</font>Last&nbsp;Name:</strong></td>
                    <td align="left" width="140" colspan="3"><input type="text" name="lname" size="30" value="<%=lname%>" onchange="newAcctInfo()"></td>
                </tr>
                <tr>
                    <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Address:</strong></td>
                    <td align="left" colspan="3"><input type="text" name="sAddress1" size="30" value="<%=saddress1%>" onchange="newAcctInfo()"></td>
                </tr>
                <tr>
                    <td align="right">&nbsp;</td>
                    <td align="left" colspan="3"><input type="text" name="sAddress2" size="30" value="<%=saddress2%>" onchange="newAcctInfo()"></td>
                </tr>
                <tr>
                    <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;City:</strong></td>
                    <td align="left"><input type="text" name="sCity" size="20" value="<%=scity%>" onchange="newAcctInfo()"></td>
                    <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Zip/Postal:</strong></td>
                    <td align="left" style="padding-right:30px;"><input type="text" name="sZip" value="<%=szip%>" size="10" maxlength="6" onchange="newAcctInfo(); getNewShipping();"></td>
                </tr>
                <tr>
                    <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;State:</strong></td>
                    <td align="left" colspan="3">
                        <select name="sState" onChange="changeTax(this.value); newAcctInfo();">
                            <%getStates(sstate)%>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Phone:</strong></td>
                    <td align="left" colspan="3"><input type="text" name="phoneNumber" size="20" value="<%=phone%>" onchange="newAcctInfo()"></td>
                </tr>
                <!--
                <tr>
                    <td align="left" colspan="4">
                        <a style="cursor:pointer; font-size:14px; font-weight:bold; color:#ff6c00;" onclick="document.getElementById('enterExclusiveOfferCheckout').style.display='';"><img src="/images/buttons/mobileOffersButton.png" border="0" /></a>
                        <div style="position:relative;">
                            <div style="display:none; position:absolute; top:0px; left:0px; background-color:#FFF; border:2px groove #333; height:375px; overflow:auto;" id="enterExclusiveOfferCheckout"></div>
                            <div style="display:none; position:absolute; top:0px; left:0px;" id="exclusiveOfferCheckout">
                            <table border="0" cellpadding="5" cellspacing="0" style="border:1px solid #000; background-color:#FFF;" width="250">
                                <tr>
                                    <td width="100%" style="background-color:#000; color:#FFF; font-weight:bold; font-size:12px;">Get Exclusive Mobile Offers</td>
                                    <td style="background-color:#000;" align="right"><a onclick="document.getElementById('exclusiveOfferCheckout').style.display='none'" style="cursor:pointer; color:#FFF; font-size:12px; text-decoration:underline;">Close</a></td>
                                </tr>
                                <tr><td colspan="2"><img src="/images/logo_539_50.jpg" border="0" width="320" height="40" /></td></tr>
                                <tr>
                                    <td colspan="2" style="font-size:12px; font-weight:bold;">
                                        Be the first to get special offers on your favorite products, sent to your mobile phone! 
                                        <br /><br />
                                        Enter your mobile # and you can always opt-out any time!
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left:40px;"><input type="text" name="cellNumber" value="" /></td>
                                    <td style="padding-right:40px;" align="left"><input type="button" name="mySub" value="Submit" onclick="return(chkCellNumCheckout(document.frmProcessOrder.cellNumber.value))" /></td>
                                </tr>
                                <tr><td style="font-size:10px; color:#F00; padding-left:40px;">Example: (123)123-4567</td></tr>
                                <tr>
                                    <td colspan="2" style="font-size:10px;">
                                        <strong>Program Terms.</strong> We do not charge for this service, but standard or other charges may apply from your carrier. 
                                        Please check your plan to make sure. To stop receiving text messages at anytime, text STOP to 80676. For help, 
                                        text HELP to 80676 or email <a href="mailto:support@4info.net">support@4info.net</a>. "View rules and conditions 
                                        of GFD sweeps here: <a href="http://bit.ly/GFDsweeps">http://bit.ly/GFDsweeps</a>"
                                    </td>
                                </tr>
                                <tr><td style="background-color:#000;" colspan="2"><div style="height:5px;"></div></td></tr>
                            </table>
                            </div>
                        </div>
                    </td>        
                </tr>
                -->
                <tr id="row_VIPlist">
                    <td align="left" valign="top" colspan="4">
                        <div style="float:left;"><input name="chkOptMail" value="Y" checked="checked" type="checkbox"></div>
                        <div style="float:left; padding-left:10px;">Please notify me via email of exclusive periodic<br />Wireless Emporium coupon code offers*</div>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="4">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="sameaddress" type="checkbox" name="sameaddress" value="yes" OnClick="javascript:showBillingInfo();">&nbsp;&nbsp;<strong>Check if Billing Address is different than the Shipping Address.</strong>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <table id="billing_info">
                            <tr>
                                <td colspan="3" align="left" style="padding-top: 10px; color:#666666; font-weight:bold;" class="normalText" valign="top">&nbsp;&nbsp;Billing&nbsp;Address:</td>
                            </tr>
                            <tr>
                                <td align="right" style="width: 100px;"><strong><font color="red" size="+1">*</font>&nbsp;Address:</strong></td>
                                <td align="left" colspan="3" width="400"><input name="bAddress1" size="30" value="<%=bAddress1%>" type="text" onchange="newAcctInfo()"></td>
                                <td rowspan="4" valign="top">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right">&nbsp;</td>
                                <td align="left" colspan="3"><input name="bAddress2" size="30" value="<%=bAddress2%>" type="text" onchange="newAcctInfo()"></td>
                            </tr>
                            <tr>
                                <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;City:</strong></td>
                                <td align="left" colspan="3"><input name="bCity" size="30" value="<%=bCity%>" type="text" onchange="newAcctInfo()"></td>
                            </tr>
                            <tr>
                                <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;State:</strong></td>
                                <td align="left" colspan="3">
                                    <select name="bState" onChange="changeTax(this.value); newAcctInfo();">
                                        <%getStates(bState)%>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"><strong><font color="red" size="+1">*</font>&nbsp;Zip/Postal:</strong></td>
                                <% if prepStr(request.Cookies("srLogin")) = "" then %>
                                <td align="left" colspan="3"><input name="bZip" size="20" value="<%=bZip%>" type="text" onchange="newAcctInfo()"></td>
                                <% else %>
                                <td align="left" colspan="3"><input name="bZip" size="20" value="<%=bZip%>" type="text"></td>
                                <% end if %>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td colspan="5" style="font-size:10px;">* Wireless Emporium is committed to protecting your privacy. We will never share or rent your information. Please review our privacy policy <a href="/privacy.asp" target="_blank">here</a>.</td></tr>
            </table>
            <% else %>

            <table border="0" cellpadding="0" cellspacing="0" width="800" bgcolor="#FFFFFF">
                <tr>
                    <td colspan="5"><img src="/images/cart/border_top.jpg" width="800" height="7" border="0"></td>
                </tr>
                <tr>
                    <td valign="top" width="7" rowspan="4" background="/images/cart/border_left.jpg"><img src="/images/spacer.gif" width="7" height="5" border="0"></td>
                    <td valign="top" width="390">
                        <table width="390" border="0" cellpadding="4" cellspacing="0" class="normalText">
                            <tr>
                                <td align="left" width="25%"><strong>First Name:</strong></td>
                                <td align="left" width="75%">
                                    <input type="hidden" name="accountid" value="<%=myAccount%>">
                                    <input type="hidden" name="fname" value="<%=fname%>"><%=fname%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="25%"><strong>Last Name:</strong></td>
                                <td align="left" width="75%"><input type="hidden" name="lname" value="<%=lname%>"><%=lname%></td>
                            </tr>
                        </table>
                    </td>
                    <td width="1" bgcolor="#A0B0D1" rowspan="2"><font style="font-size:1px;">&nbsp;</font></td>
                    <td valign="top" width="390">
                        <table width="390" border="0" cellpadding="4" cellspacing="0" class="normalText">
                            <tr>
                                <td align="left" width="20%"><strong>Phone:</strong></td>
                                <td align="left" width="80%"><input type="hidden" name="phoneNumber" value="<%=phone%>"><%=phone%></td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"><strong>Email:</strong></td>
                                <td align="left" width="80%"><input type="hidden" name="email" value="<%=email%>"><%=email%></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" width="12" rowspan="5" background="/images/cart/border_right.jpg"><img src="/images/spacer.gif" width="12" height="5" border="0"></td>
                </tr>
                <tr><td><br></td><td><br></td></tr>
                <%
                a = 1
                call fOpenConn()
                SQL = "SELECT * FROM we_addl_shipping_addr WHERE accountid='" & myAccount & "' ORDER BY id"
                set RS = Server.CreateObject("ADODB.Recordset")
                RS.open SQL, oConn, 0, 1
                %>
                <tr>
                    <td valign="top" class="normalText" width="50%">
                        <font style="color:#FF0000;font-weight:bold;">&nbsp;&nbsp;Shipping&nbsp;Address:&nbsp;&nbsp;</font>
                        <%if not RS.eof then%>
                            <font style="color:#FF0000;font-size:11px;font-weight:normal;"><br>&nbsp;&nbsp;<em>Select&nbsp;An&nbsp;Address&nbsp;To&nbsp;Ship&nbsp;To</em></font>
                        <%end if%>
                    </td>
                    <td width="1" bgcolor="#A0B0D1" rowspan="2"><font style="font-size:1px;">&nbsp;</font></td>
                    <td valign="top" class="normalText" width="50%">
                        <font style="color:#FF0000;font-weight:bold">&nbsp;&nbsp;Billing&nbsp;Address:&nbsp;&nbsp;</font>
                        <font style="color:#FF0000;font-size:11px;font-weight:normal;"><br>&nbsp;&nbsp;<em>Must&nbsp;Match&nbsp;Credit&nbsp;Card&nbsp;Statement&nbsp;Address</em></font>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="300" border="0" cellspacing="0" cellpadding="2" class="normalText">
                            <tr>
                                <td valign="top">
                                    <table width="300" border="0" cellspacing="0" cellpadding="2" class="normalText">
                                        <%if not RS.eof then%>
                                        <tr>
                                            <td class="normalText" colspan="2">
                                                <input type="radio" name="radioAddress" value="<%=a%>" onClick="changeShippingAddress(<%=a%>);"<%if radioAddress = "" or radioAddress = cStr(a) then response.write " checked"%>>&nbsp;<b>Primary Shipping Address</b>
                                            </td>
                                        </tr>
                                        <%end if%>
                                        <tr>
                                            <td>
                                                <div align="left">
                                                    <%=sAddress1%><br>
                                                    <%
                                                    if not isNull(sAddress2) then
                                                        if trim(sAddress2) <> "" then response.write sAddress2 & "<br>"
                                                    end if
                                                    %>
                                                    <%=sCity & ",&nbsp;" & sState & "&nbsp;&nbsp;" & sZip%><br>
                                                </div>
                                            </td>
                                            <td align="right">
                                                <a href="/register/default.asp?pf=modify&orderid=<%=nOrderId%>&shiptype=&promo=">Edit</a>
                                                <input type="hidden" name="sAddress1<%=a%>" value="<%=sAddress1%>">
                                                <input type="hidden" name="sAddress2<%=a%>" value="<%=sAddress2%>">
                                                <input type="hidden" name="sCity<%=a%>" value="<%=sCity%>">
                                                <input type="hidden" name="sState<%=a%>" value="<%=sState%>">
                                                <input type="hidden" name="sZip<%=a%>" value="<%=sZip%>">
                                                <input type="hidden" name="sID<%=a%>" value="0">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <%
                            'BEGIN Additional Shipping Addresses
                            do while not RS.eof
                            a = a + 1
                            %>
                            <tr>
                                <td class="normalText">
                                    <input type="radio" name="radioAddress" value="<%=a%>" onClick="changeShippingAddress(<%=a%>);"<%if radioAddress = cStr(a) then response.write " checked"%>>&nbsp;<b>Shipping Address #<%=a%></b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="300" border="0" cellspacing="0" cellpadding="2" class="normalText">
                                        <tr>
                                            <td>
                                                <div align="left">
                                                    <%=RS("sAddress1")%><br>
                                                    <%
                                                    if not isNull(RS("sAddress2")) then
                                                        if trim(RS("sAddress2")) <> "" then response.write RS("sAddress2") & "<br>"
                                                    end if
                                                    %>
                                                    <%=RS("sCity") & ",&nbsp;" & RS("sState") & "&nbsp;&nbsp;" & RS("sZip")%><br>
                                                </div>
                                            </td>
                                            <td align="right">
                                                <a href="/register/default.asp?pf=modify&orderid=<%=nOrderId%>&shiptype=&promo=">Edit</a>
                                                <div align="right">
                                                    <input type="hidden" name="sAddress1<%=a%>" value="<%=RS("sAddress1")%>">
                                                    <input type="hidden" name="sAddress2<%=a%>" value="<%=RS("sAddress2")%>">
                                                    <input type="hidden" name="sCity<%=a%>" value="<%=RS("sCity")%>">
                                                    <input type="hidden" name="sState<%=a%>" value="<%=RS("sState")%>">
                                                    <input type="hidden" name="sZip<%=a%>" value="<%=RS("sZip")%>">
                                                    <input type="hidden" name="sID<%=a%>" value="<%=RS("id")%>">
                                                    <%
                                                    if radioAddress = cStr(a) then
                                                        sAddress1 = RS("sAddress1")
                                                        sAddress2 = RS("sAddress2")
                                                        sCity = RS("sCity")
                                                        sState = RS("sState")
                                                        sZip = RS("sZip")
                                                    end if
                                                    %>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <%
                            RS.movenext
                            loop
                            RS.close
                            set RS = nothing
                            call fCloseConn()
                            'END Additional Shipping Addresses
                            %>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="300" border="0" cellspacing="0" cellpadding="2" class="normalText">
                            <tr>
                                <td valign="top" align="left">
                                    <br>
                                    <input type="hidden" name="bAddress1" value="<%=bAddress1%>"><%=bAddress1%><br>
                                    <input type="hidden" name="bAddress2" value="<%=bAddress2%>">
                                        <%
                                        if not isNull(bAddress2) then
                                            if trim(bAddress2) <> "" then response.write bAddress2 & "<br>"
                                        end if
                                        %>
                                    <input type="hidden" name="bCity" value="<%=bCity%>"><%=bCity%>,
                                    <input type="hidden" name="bState" value="<%=bState%>"><%=bState%>&nbsp;&nbsp;
                                    <input type="hidden" name="bZip" value="<%=bZip%>"><%=bZip%>
                                    
                                    <input type="hidden" name="sAddress1" value="<%=sAddress1%>">
                                    <input type="hidden" name="sAddress2" value="<%=sAddress2%>">
                                    <input type="hidden" name="sCity" value="<%=sCity%>">
                                    <input type="hidden" name="sState" value="<%=sState%>">
                                    <input type="hidden" name="sZip" value="<%=sZip%>">
                                </td>
                                <td align="right">
                                    <a href="/register/default.asp?pf=modify&orderid=<%=nOrderId%>&shiptype=&promo=">Edit</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5"><img src="/images/cart/border_bottom.jpg" width="800" height="12" border="0"></td>
                </tr>
            </table>
            <% end if %>
        </tr>
    </tr>
    <tr>
        <td width="100%" align="center" valign="top"><img src="/images/cart/we_header_Payment.jpg" width="800" height="39" border="0" alt="Payment Information"></td>
    </tr>
    <tr>
        <td class="normalText" align="center" valign="top" width="100%">
            <table width="780" height="250" border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td colspan="5"><img src="/images/cart/border_top.jpg" width="800" height="7" border="0"></td>
                </tr>
                <tr>
                    <td valign="top" width="12" style="background-image:url(/images/cart/border_left.jpg); background-repeat:repeat-y;" rowspan="2"><img src="/images/spacer.gif" width="12" height="5" border="0"></td>
                    <td width="376" align="left" valign="top" class="normalText" style="padding-top:10px;">
                        <table width="100%" border="0" cellpadding="2" cellspacing="0" align="center">
                            <tr>
                                <td valign="top" align="left" colspan="2">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                                        <tr><td valign="top" align="center"><img src="/images/cart/we_cards.gif" alt="We accept Visa, MasterCard, American Express and Discover" border="0" align="absbottom"></td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"><b>Card&nbsp;Type:&nbsp;</b></td>
                                <td>
                                    <select name="cc_cardType">
                                        <option value=""></option>
                                        <option value="VISA">VISA</option>
                                        <option value="MC">MASTER CARD</option>
                                        <option value="AMEX">AMERICAN EXPRESS</option>
                                        <option value="DISC">DISCOVER</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td align="right"><b>&nbsp;Card&nbsp;Holder&nbsp;Name:&nbsp;</b></td><td><input name="cc_cardOwner" type="text" size="23"></td></tr>
                            <tr><td align="right"><b>Credit&nbsp;Card&nbsp;#:&nbsp;</b></td><td><input name="cardNum" type="text" id="cardNum" size="23" maxlength="16"></td></tr>
                            <tr><td align="right">&nbsp;</td><td><span style="size: 7pt;">(no&nbsp;spaces/dashes<br>example:&nbsp;1234123412341234)</span></td></tr>
                            <tr><td align="right"><b>Security&nbsp;Code:&nbsp;</b></td><td><input name="secCode" type="text" id="secCode" size="4" maxlength="4">&nbsp;&nbsp;<a href="/cart/cvv2help.asp" style="font-size:11px;" target="_blank">(Help on Security Code)</a></td></tr>
                            <tr>
                                <td align="right"><b>Expiration&nbsp;Date:&nbsp;</b></td>
                                <td>
                                    <select name="cc_month">
                                        <option value="01">01 (January)</option>
                                        <option value="02">02 (February)</option>
                                        <option value="03">03 (March)</option>
                                        <option value="04">04 (April)</option>
                                        <option value="05">05 (May)</option>
                                        <option value="06">06 (June)</option>
                                        <option value="07">07 (July)</option>
                                        <option value="08">08 (August)</option>
                                        <option value="09">09 (September)</option>
                                        <option value="10">10 (October)</option>
                                        <option value="11">11 (November)</option>
                                        <option value="12">12 (December)</option>
                                    </select>
                                    <select name="cc_year">
                                        <%
                                        dim countYear
                                        for countYear = 0 to 14
                                            response.write "<option value=""" & right(cStr(year(date) + countYear),2) & """>" & cStr(year(date) + countYear) & "</option>" & vbcrlf
                                        next
                                        %>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="56" align="center" valign="middle" height="100%"><img src="/images/cart/or_bar_250.jpg" width="56" height="250" border="0"></td>
                    <td width="376" align="center" valign="top" height="100%">
                        <table width="100%" border="0" cellpadding="2" cellspacing="0" align="center">
                            <tr>
                                <td align="left" valign="middle"><input type="radio" name="PaymentType" value="eBillMe"></td>
                                <td align="left" valign="middle" class="smltext"><p><img src="/images/WU/WU-Pay-wutag-small.jpg" border="0" width="227" height="44" /></p></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td align="left" valign="top" class="smltext">
                                    <div>
										WU&reg; Pay allows you to pay cash online. No financial information required. Simply pay using your bank's online bill pay service, or at a Western Union&reg; Agent location near you. 
                                        Just like paying a utility bill! <a href="http://www.westernunion.com/wupay/learn/how-to-use-wupay-video" target="_blank">Watch our demo</a>.
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" width="12" style="background-image:url(/images/cart/border_right.jpg); background-repeat:repeat-y;" rowspan="2"><img src="/images/spacer.gif" width="12" height="5" border="0"></td>
                </tr>
                <tr>
                    <td colspan="3" align="center" style="padding-top:10px; padding-bottom:20px;">
                        <!--<input type="image" onclick="popUp('https://one-time-offer.com/sg/gateway.aspx?v=0353A36353&p=92E20262D2BE77B7D7F706E72FC33333A383539F16D6060727&cl=F6E606','',screen.availHeight * .8, screen.availWidth * .9, screen.availWidth - (screen.availWidth * .91),screen.availHeight - (screen.availHeight * .87)); return CheckSubmit();" src="/images/cart/place_order_button.jpg" width="272" height="56" alt="Place My Order">-->
                        <div id="submitBttn1" style="font-weight:bold; font-size:18px; color:#000;"><input type="image" onclick="return CheckSubmit();" src="/images/cart/place_order_button.jpg" width="272" height="56" alt="Place My Order"></div>
                        <div id="submitBttn2" style="font-weight:bold; font-size:18px; color:#000; display:none;">Please wait while<br />we process your order</div>
                        <input type="hidden" name="submitted" value="submitted">
                    </td>
                </tr>
                <tr>
                    <td colspan="5"><img src="/images/cart/border_bottom.jpg" width="800" height="12" border="0"></td>
                </tr>
            </table>
            <!--WE variables-->
			<%=WEhtml2%>
            <!--eBillme variables-->
            <%=EBtemp%>
            <input type="hidden" name="myBasketXML" value="<%=EBxml%>">
            <input type="hidden" name="ssl_ProdIdCount" value="<%=nProdIdCount%>">
        </td>
    </tr>
</table>
<div id="customerInfo" style="display:none;"></div>
</form>
<!--#include virtual="/includes/template/bottom_cart.asp"-->
<form name="googleForm" method="post" action="/cart/process/google/CartProcessing.asp">
<!--<form method="post" action="/cart/process/google/CartProcessing.asp">&#150;&nbsp;or&nbsp;checkout&nbsp;with&nbsp;&#150;&nbsp;&nbsp;&nbsp;<br>-->
    <!--<input type="image" name="Checkout" alt="Google Checkout" src="https://checkout.google.com/buttons/checkout.gif?merchant_id=151073323883147&w=168&h=44&style=white&variant=text&loc=en_US" height="44" width="168">-->
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <input type="hidden" name="gift" value="<%=sGiftCert%>">
    <input type="hidden" name="analyticsdata" value="">
    <input type="hidden" name="shipZip" value="<%=sZip%>">
    <input type="hidden" name="sWeight" value="<%=cStr(sWeight)%>">
    <%=WEhtml2%>
</form>
<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.asp">
<!--<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.asp">-->
    <input type="hidden" name="numItems" value="<%=nProdIdCount%>">
    <input type="hidden" name="paymentAmount" value="<%=nSubTotal%>">
    <input type="hidden" name="ItemsAndWeight" value="<%=cStr(nProdIdCount) & "|" & cStr(sWeight)%>">
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <input type="hidden" name="gift" value="<%=sGiftCert%>">
    <input type="hidden" name="shipZip" value="<%=sZip%>">
    <%=PPhtml%>
    <!--<input type="image" name="submit" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif">-->
</form>
<script language="javascript">
	function enterPromo() {
		document.getElementById("promoLink").style.display = 'none'
		document.getElementById("promoCode").style.display = ''
	}
	
	function checkoutSelected(val) {
		if (val == "pp") {
			document.PaypalForm.submit()
		}
		else if (val == "cc") {
			document.getElementById("ccOrder").style.display = ''
			window.location = "#ccOrder"
		}
		else if (val == "gc") {
			document.googleForm.submit()
		}
		else if (val == "eb") {
			document.getElementById("ccOrder").style.display = ''
			window.location = "#ccOrder"
			document.frmProcessOrder.PaymentType[1].checked = true
		}
	}
	
	function getCustomerData(email) {
		ajax('/ajax/customerData.asp?email=' + email,'customerInfo')
		setTimeout("displayData(document.getElementById('customerInfo').innerHTML)",2000)
	}
	function displayData(dataReturn) {
		if (dataReturn != "no data" && dataReturn != "") {
			document.getElementById("popUpBG").style.display = ''
			document.getElementById("popUpBox").style.display = ''
			var dataArray = dataReturn.split("@@")
			if (document.frmProcessOrder.fname.value == "") { document.frmProcessOrder.fname.value = dataArray[0] }
			if (document.frmProcessOrder.lname.value == "") { document.frmProcessOrder.lname.value = dataArray[1] }
			if (document.frmProcessOrder.sAddress1.value == "") { document.frmProcessOrder.sAddress1.value = dataArray[2] }
			if (document.frmProcessOrder.sAddress2.value == "") { document.frmProcessOrder.sAddress2.value = dataArray[3] }
			if (document.frmProcessOrder.sCity.value == "") { document.frmProcessOrder.sCity.value = dataArray[4] }
			if (document.frmProcessOrder.sState.selectedIndex == 0) {
				for (i=0;i<50;i++) {
					if (document.frmProcessOrder.sState.options[i].value == dataArray[5]) {
						document.frmProcessOrder.sState.selectedIndex = i
						changeTax(dataArray[5])
					}
				}
			}
			if (document.frmProcessOrder.sZip.value == "") {
				document.frmProcessOrder.sZip.value = dataArray[6]
				getNewShipping();
			}
			if (document.frmProcessOrder.phoneNumber.value == "") { document.frmProcessOrder.phoneNumber.value = dataArray[7] }
			
			if (dataArray[2] != dataArray[8] || dataArray[6] != dataArray[12]) {
				document.frmProcessOrder.sameaddress.checked = true
				showBillingInfo()
				document.frmProcessOrder.bAddress1.value = dataArray[8]
				document.frmProcessOrder.bAddress2.value = dataArray[9]
				document.frmProcessOrder.bCity.value = dataArray[10]
				for (i=0;i<50;i++) {
					if (document.frmProcessOrder.bState.options[i].value == dataArray[11]) {
						document.frmProcessOrder.bState.selectedIndex = i
					}
				}
				document.frmProcessOrder.bZip.value = dataArray[12]
			}
			document.frmProcessOrder.accountid.value = dataArray[13]
			document.frmProcessOrder.parentAcctID.value = dataArray[14]
		}
	}
	
	function newAcctInfo() {
		document.frmProcessOrder.accountid.value = ''
	}
</script>
<script language="JavaScript">
	function popUp(url, winName, h, w, l, t) {
		var args = 'height=' + h + ',width=' + w + ',left=' + l + ',top=' + t + ',toolbar=1,location=1,directories=0,status=1,menubar=1,scrollbars=1,resizable=1';
		window.open(url, winName, args);
	}
</script>