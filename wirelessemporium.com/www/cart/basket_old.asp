<%
'option explicit
pageName = "basket"
noCommentBox = true
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"
'session.Abandon()
dim cart
dim oneTime : oneTime = False

if instr(request.ServerVariables("SERVER_NAME"),"staging.") < 1 then
	'if request.ServerVariables("HTTPS") = "off" then response.Redirect("https://www.wirelessemporium.com/cart/basket.asp")
end if

cart = 1

'dim varient : varient = prepInt(request.QueryString("varient"))

varientTopBanner = 2
varientTopNav = 1
varientAssurance = 0

set fs = CreateObject("Scripting.FileSystemObject")
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
dim SEtitle, SEdescription, SEkeywords
SEtitle = ""
SEdescription = ""
SEkeywords = ""

dim pageTitle, strBody, a, resetChkout
pageTitle = ""
strBody = ""
resetChkout = request.QueryString("r")
if isnull(resetChkout) or len(resetChkout) < 1 then resetChkout = 0

Session.Contents.Remove("nvpResArray")
Session.Contents.Remove("token")
Session.Contents.Remove("currencyCodeType")
Session.Contents.Remove("paymentAmount")
Session.Contents.Remove("PaymentType")
Session.Contents.Remove("PayerID")
%>

<!--#include virtual="/includes/template/top_cart.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->

<%
strFreeShipping = "First Class"
'session("promocode") = null
dim sPromoCode
if request.form("promo") <> "" then
	sPromoCode = SQLquote(request.form("promo"))
else
	sPromoCode = session("promocode")
end if
sPromoCode = prepStr(sPromoCode)

if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	call fOpenConn()
	sql = "exec sp_promoDetails '" & sPromoCode & "'"
	session("errorSQL") = sql
	set RS = oConn.execute(sql)
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		FreeItemPartNumber = RS("FreeItemPartNumber")
		setValue = RS("setValue")
		oneTime = RS("oneTime")
	else
		promoError = "Promo Code Not Found"
		sPromoCode = ""
	end if
	call fCloseConn()
end if

dim htmBasketRows, nItemTotal, EmptyBasket
htmBasketRows = ""
nItemTotal = 0
EmptyBasket = false

' START basket grab
dim sItemName, sRetailPrice, sItemPrice, sItemPic, nProdIdCheck, nProdQuantity, nProdIdCount, showDelBtn, nProdTotalQuantity, strItemIDList
dim sWeight, strItemCheck, strQty, FreeProductInCart
dim SquaretradeItemName, SquaretradeItemPrice, SquaretradeItemCondition
dim strMusicSkinsItemsCheck, strMusicSkinsQty
strItemIDList = ""
nProdTotalQuantity = 0
sWeight = 0
strItemCheck = ""
strQty = ""
strMusicSkinsItemsCheck = ""
strMusicSkinsQty = ""
FreeProductInCart = 0
call fOpenConn()

customSessionID = prepInt(request.QueryString("csid"))
if customSessionID > 0 then mySession = customSessionID

sql = "exec sp_itemsInBasket 0," & mySession
session("errorSQL") = SQL
set RS = oConn.execute(sql)

dim goodProducts : goodProducts = ""
dim badProducts : badProducts = ""
dim plateCnt : plateCnt = 0
do until RS.eof
	parentID = prepInt(rs("parentID"))
	if isnull(RS("alwaysInStock")) then alwaysInStock = False else alwaysInStock = RS("alwaysInStock")
	if RS("itemID") = addItem_ID and rs("customCost") = addItem_cost then
		itemAdded = 1
	end if
	
	nProdIdCheck = RS("itemID")
	nProdQuantity = RS("qty")
	nProdTotalQuantity = nProdTotalQuantity + RS("qty")
	if strItemIDList = "" then
		strItemIDList = RS("itemID")
	else
		strItemIDList = strItemIDList & "," & RS("itemID")
	end if

	if RS("musicSkins") then
		strMusicSkinsItemsCheck = strMusicSkinsItemsCheck & cStr(nProdIdCheck) & ","
		strMusicSkinsQty = strMusicSkinsQty & cStr(nProdQuantity)& "," 
	else
		strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
		strQty = strQty & cStr(nProdQuantity)& ","  			' QTY string for each item, this is for buySafe Use	
	end if
	
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		useBrandName = RS("brandName")
		useModelName = RS("modelName")
		partNumber = RS("partNumber")
		sItemName = insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName)
		musicSkinsDefaultImg = ""
		
		if RS("musicSkins") then
			if isnull(RS("itemPic")) then
				sItemPic = RS("defaultPic")
				musicSkinsDefaultImg = RS("defaultPic")
			else
				sItemPic = RS("itemPic")
			end if
			if instr(sItemPic,";") > 0 then
				musicSkinsDefaultImg = mid(sItemPic,instr(sItemPic,";")+1)
				sItemPic = left(sItemPic,instr(sItemPic,";")-1)
			end if		
		else
			sItemPic = RS("itemPic")
		end if
		
		sItemPrice = RS("price_Our")
		if not isnull(rs("customCost")) then sItemPrice = rs("customCost")
		sRetailPrice = RS("price_retail")
		'Free Bluetooth
		if FreeBluetooth = 1 then
			if cStr(nProdIdCheck) = FreeBluetoothItemID then
				FreeBluetoothPrice = sItemPrice
			end if
		end if
		'Free Product With Phone Purchase
		if RS("typeID") = 16 then
			PhonePurchased = 1
			SquaretradeItemName = sItemName
			SquaretradeItemPrice = sItemPrice
			SquaretradeItemCondition = RS("Condition")
		end if
		'Category Sale
		if CategorySale = 1 then
			if RS("typeID") = CategorySaleTypeID and RS("NoDiscount") = 0 then
				CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
			else
				sql = "exec sp_categorySale " & CategorySaleTypeID & "," & RS("modelID") & "," & nProdIdCheck
				dim RSrelated
				set RSrelated = Server.CreateObject("ADODB.Recordset")
				RSrelated.open SQL, oConn, 0, 1
				if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				RSrelated.close
				set RSrelated = nothing
			end if
		end if
		'Free Product Offer code added 2/1/2010 by MC
		if inStr(RS("PartNumber"),"scr-") > 0 then FreeProductInCart = 1
		
		'========================== screen protector sale ending in 1/15/11 by Terry
		if RS("itemID") = addItem_ID then
			showDelBtn = addItem_showDeleteBtn
			addItem_lockQty = rs("lockQty")
		end if
		
		'session("errorSQL") = "nItemTotal = " & nItemTotal & " | nProdIdCheck = " & nProdIdCheck & " | sItemPrice = " & sItemPrice & " | nProdQuantity = " & nProdQuantity
		if rs("musicSkins") then musicSkins = 1 else musicSkins = 0
		if rs("inv_qty") > 0 or alwaysInStock then
			nItemTotal = nItemTotal + (sItemPrice * nProdQuantity)
			sWeight = sWeight + (cDbl(RS("itemWeight")) * nProdQuantity)
			goodProducts = goodProducts & nProdIdCheck & "##" & sItemName & "##" & sItemPrice & "##" & cStr(nProdQuantity) & "##" & sRetailPrice & "##" & sItemPic & "##" & rs("lockQty") & "##" & showDelBtn & "##" & musicSkins & "##" & musicSkinsDefaultImg & "##" & partnumber & "##" & parentID & "@@"
			htmBasketRows = htmBasketRows & fWriteBasketRow(nProdIdCheck,sItemName,sItemPrice,cStr(nProdQuantity),sRetailPrice,sItemPic,rs("lockQty"),showDelBtn,musicSkins,musicSkinsDefaultImg)
		else
			sql = "exec sp_deleteBasketItems " & nProdIdCheck
			session("errorSQL") = sql
			oConn.execute SQL
			badProducts = badProducts & nProdIdCheck & "##" & sItemName & "##" & sItemPrice & "##" & cStr(nProdQuantity) & "##" & sRetailPrice & "##" & sItemPic & "##" & rs("lockQty") & "##" & showDelBtn & "##" & musicSkins & "##" & musicSkinsDefaultImg & "##" & partnumber & "@@"
			htmBasketRows = htmBasketRows & fWriteErrorBasketRow(nProdIdCheck,sItemName,sItemPrice,cStr(nProdQuantity),sRetailPrice,sItemPic,rs("lockQty"),showDelBtn,musicSkins,musicSkinsDefaultImg)
		end if
	end if
	
	RS.movenext
loop
RS.close
set RS = nothing

if nProdTotalQuantity = 1 then displayItemCnt = nProdTotalQuantity & " Item" else displayItemCnt = nProdTotalQuantity & " Items"

if addItem_ID > 0 then
	if nItemTotal > addItem_mustSpend then
		'freeSkin
		if itemAdded = 0 then
			if isnull(myAccount) or len(myAccount) < 1 then myAccount = 0
			sql = "exec sp_addItemToBasket 0," & mySession & "," & myAccount & "," & addItem_ID & "," & addItem_qty & "," & addItem_cost & "," & addItem_lockQty
			session("errorSQL") = sql
			oConn.execute(sql)
			
			response.Redirect("/cart/basket.asp")
		end if
	elseif itemAdded = 1 then
		sql = "exec sp_deleteMyBasket 0," & mySession
		session("errorSQL") = sql
		oConn.execute(sql)
		
		response.Redirect("/cart/basket.asp")
	end if
end if
' END basket grab
			
dim nSubTotal
nSubTotal = nItemTotal

if sWeight / 16 < 1 then
	sWeight = 1
else
	sWeight = sWeight / 16
end if

if strItemCheck <> "" then strItemCheck = left(strItemCheck,len(strItemCheck)-1)
if strMusicSkinsItemsCheck <> "" then strMusicSkinsItemsCheck = left(strMusicSkinsItemsCheck,len(strMusicSkinsItemsCheck)-1)

if nProdIdCount = 0 then
	htmBasketRows = htmBasketRows & "<tr><td colspan=""7""><i>Basket is currently empty.</i></td></tr>" & vbcrlf
	EmptyBasket = true
end if
%>
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#999999">
    <%
    if len(session("userMsg")) > 0 and not isnull(session("userMsg")) then
    %>
    <tr bgcolor="#f0f0f0"><td align="center" style="font-weight:bold; color:#C30; font-size:14px;"><%=session("userMsg")%></td></tr>
    <%
        session("userMsg") = null
    end if
    %>
    <% if resetChkout = 1 then %>
    <tr bgcolor="#f0f0f0"><td align="center" style="font-weight:bold; color:#C30; font-size:14px;">Your Checkout Process Has Been Reset - Please Try Again</td></tr>
    <% end if %>
    <tr>
        <td align="center" bgcolor="#FFFFFF">
            <div style="width:100%; float:left; margin-top:20px;">
                <div style="font-size:28px; font-weight:bold; float:left; margin-right:20px;">Your Cart Summary</div>
                <div style="font-size:18px; font-weight:bold; float:left;"><img src="/images/icons/shoppingCart.gif" border="0" /></div>
                <div style="float:right; color:#ccc; font-size:16px; padding:2px 0px 0px 5px;">Receipt</div>
                <div style="float:right; padding-left:15px;"><img src="/images/checkout/circle-3-off.png" border="0" /></div>
                <div style="float:right; color:#ccc; font-size:16px; padding:2px 0px 0px 5px;">Secure Checkout</div>
                <div style="float:right; padding-left:15px;"><img src="/images/checkout/circle-2-off.png" border="0" /></div>
                <div style="float:right; font-weight:bold; font-size:16px; padding:2px 0px 0px 5px;">Cart Summary&nbsp;<span style="color:#ff6600; font-size:11px;">(<%=displayItemCnt%>)</span></div>
                <div style="float:right;"><img src="/images/checkout/circle-1-on.png" border="0" /></div>                
            </div>
            <div style="width:100%; float:left; margin-top:20px; background-image:url(/images/structure/upperMenuBar.jpg); padding:5px 0px 5px 0px; color:#FFF;">
                <div style="float:left; font-size:16px; font-weight:bold; width:100px;">Qty.</div>
                <div style="float:left; font-size:16px; font-weight:bold; padding-left:50px; width:580px; text-align:left;">Product Description</div>
                <div style="float:right; font-size:16px; font-weight:bold; text-align:right; width:150px; padding-right:10px;">Total Price</div>
            </div>
            <%
            if discountTotal > 0 then nItemTotal = nItemTotal - discountTotal
            bgColor = "#fff"
            prodList_good = split(goodProducts,"@@")
            itemTotal = 0
            nProdIdCount = 0
			dim baseItems : baseItems = ""
			dim activeProductCnt : activeProductCnt = 0
			dim eligibleCnt : eligibleCnt = 0
			dim noShopRunner : noShopRunner = 0
			dim baseCaseList : baseCaseList = ""
            for i = 0 to (ubound(prodList_good)-1)
				activeProductCnt = activeProductCnt + 1
                curProdArray = split(prodList_good(i),"##")
				
				itemID = curProdArray(0)
				partNumber = curProdArray(10)
				usePrice = curProdArray(2)
				qty = curProdArray(3)
				
				if instr(baseItems,itemID & "@@") > 0 then
					baseArray = split(baseItems,"##")
					baseCnt = 0
					for x = 0 to ubound(baseArray)
						if prepStr(baseArray(x)) <> "" then
							detailArray = split(baseArray(x),"@@")
							if detailArray(0) = itemID then qty = qty - detailArray(1)
						end if
					next
				end if
				
				if not isDropShip(partNumber) and left(partNumber,3) <> "WCD" then eligibleCnt = eligibleCnt + 1
                'nProdIdCheck,sItemName,sItemPrice,cStr(nProdQuantity),sRetailPrice,sItemPic,rs("lockQty"),showDelBtn,musicSkins,musicSkinsDefaultImg
				dim csidLink : csidLink = ""
				if customSessionID > 0 then csidLink = "?csid=" & customSessionID
				if qty > 0 then
            %>
            <div style="float:left; background-color:<%=bgColor%>; padding-top:10px; border-left:2px solid #CCC; border-right:2px solid #CCC;">
                <div style="float:left; width:100px; text-align:center; margin-bottom:5px;">
                    <form name="frmEditDelete<%=itemID%>" action="/cart/item_edit.asp<%=csidLink%>" method="post" style="margin:0px;">
                        <input type="hidden" name="prodid" value="<%=itemID%>">
                        <input type="hidden" name="pagetype" value="edit">
                        <%
                        if curProdArray(6) then
                            'lock the qty
                        %>
                        <div>
                            <%=qty%>
                            <input type="hidden" name="qty" value="<%=qty%>" />
                        </div>
                            <%
                            if curProdArray(7) then
                                'allow the delete button
                            %>
                        <div style="padding-top:10px;"><input type="image" src="/images/buttons/cartRemoveQty.png" height="21" width="69" onClick="EditDeleteItem(this.form.name,'delete');"></div>
                        <%
                            end if
                        else
                            'full control to update qty and delete product
                        %>
                        <div><input type="text" name="qty" size="2" maxlength="3" value="<%=qty%>" class="cart-text" /></div>
                        <div style="padding-top:2px;"><input type="image" src="/images/buttons/cartUpdateQty.png" height="21" width="69" onClick="EditDeleteItem(this.form.name,'edit');"></div>
                        <div style="padding-top:2px;"><input type="image" src="/images/buttons/cartRemoveQty.png" height="21" width="69" onClick="EditDeleteItem(this.form.name,'delete');"></div>
                        <% end if %>
                    </form>
                </div>
                <div style="float:left; font-size:16px; font-weight:bold; padding-left:50px; width:643px; text-align:left; padding-bottom:10px;">
                    <%
                    if curProdArray(8) = 1 then
                        'Is a Music Skins Product
						if fs.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsSmall") & "\" & curProdArray(5)) then
							'Has a small product picture
							msImagePath = "/productpics/musicSkins/musicSkinsSmall/" & curProdArray(5)
						elseif fs.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs") & "\" & curProdArray(9)) then
							'Had a default thumb picture
							msImagePath = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & curProdArray(9)
						else
							msImagePath = "/productpics/icon/imagena.jpg"
						end if
						%>
                    <div style="float:left; border:1px solid #000; margin-right:8px; padding:5px; background-color:#FFF;"><a href="/p-ms-<%=itemID%>-<%=formatSEO(curProdArray(1))%>.asp"><img src="<%=msImagePath%>" border="0" height="45" /></a></div>
                    <div style="float:left; width:550px;"><a href="/p-ms-<%=itemID%>-<%=formatSEO(curProdArray(1))%>.asp" class="product-desc-cart"><%=curProdArray(1)%></a></div>
                    <%
                    else
                        'Is a standard WE Product
                    %>
                    <div style="float:left; border:1px solid #000; margin-right:8px; padding:5px; background-color:#FFF;">
                    	<div style="float:left;"><a href="/p-<%=itemID%>-<%=formatSEO(curProdArray(1))%>.asp"><img src="/productpics/icon/<%=curProdArray(5)%>" border="0" /></a></div>
                        <%
						if instr(partNumber,"WCD-") > 0 then
							pnArray = split(partNumber,"-")
							baseCaseID = pnArray(1)
							if instr(baseCaseList,baseCaseID) > 0 then
								tempVar = mid(baseCaseList,instr(baseCaseList,baseCaseID))
								tempVar = replace(mid(tempVar,instr(tempVar,"@@")+2),"##","")
								curQty = prepInt(tempVar)
								baseCaseList = replace(baseCaseList,baseCaseID & "@@" & curQty,baseCaseID & "@@" & (curQty + qty))
								curQty = curQty + qty
							else
								baseCaseList = baseCaseList & baseCaseID & "@@" & qty & "##"
							end if
							
							sql = "select itemPic, price_our, itemDesc from we_Items where itemID = " & baseCaseID
							session("errorSQL") = sql
							call fOpenConn() 'Has to be re-opened because connection is being closed somewhere (2012-10-01)
							set baseCaseRS = oConn.execute(sql)
							bcPic = baseCaseRS("itemPic")
							bcPrice = baseCaseRS("price_our")
							bcDesc = baseCaseRS("itemDesc")
							usePrice = usePrice + bcPrice
							baseItems = baseItems & baseCaseID & "@@" & qty & "##"
						%>
                        <div style="float:left; padding:10px 0px 0px 10px; font-weight:bold; font-size:14px;">+</div>
                        <div style="float:left; padding-left:10px;"><a href="/p-<%=baseCaseID%>-<%=formatSEO(bcDesc)%>.asp"><img src="/productpics/icon/<%=bcPic%>" border="0" /></a></div>
                        <% end if %>
                    </div>
                    <% if instr(partNumber,"WCD-") > 0 then %>
                    <div style="float:left; width:450px;">
                    	<div style="float:left; width:450px;"><a href="/p-<%=itemID%>-<%=formatSEO(curProdArray(1))%>.asp" class="product-desc-cart"><%=curProdArray(1)%></a> (<%=prepInt(curProdArray(2)) * prepInt(qty)%>)</div>
                        <div style="float:left; padding-left:15px; width:435px;"><a href="/p-<%=baseCaseID%>-<%=formatSEO(bcDesc)%>.asp" class="product-desc-cart"><%=bcDesc%></a> (<%=prepInt(qty) * prepInt(bcPrice)%>)</div>
                    </div>
                    <% else %>
                    <div style="float:left; width:550px;">
                    	<div style="float:left; width:550px;"><a href="/p-<%=itemID%>-<%=formatSEO(curProdArray(1))%>.asp" class="product-desc-cart"><%=curProdArray(1)%></a></div>
                    </div>
                    <% end if %>
                    <%
                    end if
                    %>
                </div>
                <div style="float:right; font-size:16px; width:150px; text-align:right; padding-right:9px;">
					<%=formatCurrency(usePrice * qty)%>
                </div>
            </div>
            <%
                end if
				
				if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
                itemTotal = itemTotal + (usePrice * qty)
                nProdIdCount = nProdIdCount + 1
                'google product details
                WEhtml2 = WEhtml2 & "<input type=""hidden"" name=""itemID_" & nProdIdCount & """ value=""" & itemID & """><input type=""hidden"" name=""itemQty_" & nProdIdCount & """ value=""" & curProdArray(3) & """>" & vbcrlf
                'paypal product details
                PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & itemID & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & curProdArray(1) & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & curProdArray(3) & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & curProdArray(2) & """>" & vbcrlf
            next
						
			'promo from 12/17/2012 to 12/19/2012
			if not EmptyBasket and spendThisMuch=1 then
				on error resume next
				if itemTotal < stm_total then
				%>
			<div style="float:left; background-color:<%=bgColor%>; font-size:18px; padding:30px 0px 30px 0px; text-align:center; width:952px; border-left:2px solid #CCC; border-right:2px solid #CCC;">
				Spend Just <span style="color:#f00;"><%=formatcurrency(stm_total-itemTotal)%></span> More to Get a Free Upgrade To Priority Shipping
            </div>
                <%
				else
					strFreeShipping = "Priority"
				%>
			<div style="float:left; background-color:<%=bgColor%>; font-size:18px; padding:30px 0px 30px 0px; text-align:center; width:952px; border-left:2px solid #CCC; border-right:2px solid #CCC;">
				Congratulations! Your Order Qualifies for Free Priority Shipping!
            </div>
                <%
				end if
				on error goto 0
			end if
			
            prodList_bad = split(badProducts,"@@")
			session("errorSQL") = badProducts
            for i = 0 to (ubound(prodList_bad)-1)
                curProdArray = split(prodList_bad(i),"##")
				itemID = curProdArray(0)
				partNumber = curProdArray(10)
				usePrice = curProdArray(2)
				qty = curProdArray(3)
            %>
            <div style="width:1000px; float:left; background-color:<%=bgColor%>; padding-top:10px;">
                <div style="float:left; width:100px; text-align:center; margin-bottom:5px;">&nbsp;</div>
                <div style="float:left; font-size:16px; font-weight:bold; padding-left:50px; width:680px; text-align:left;">
                    <%
                    if curProdArray(8) = 1 then
                        'Is a Music Skins Product
                        if fs.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsSmall") & "\" & sItemPic) then
                            'Has a small product picture
                    %>
                    <div style="float:left; border:1px solid #000; margin-right:10px; padding:5px; background-color:#FFF;"><a href="/p-ms-<%=curProdArray(0)%>-<%=formatSEO(curProdArray(1))%>.asp"><img src="/productpics/musicSkins/musicSkinsSmall/<%=curProdArray(5)%>" border="0" /></a></div>
                    <div style="float:left;"><a href="/p-ms-<%=curProdArray(0)%>-<%=formatSEO(curProdArray(1))%>.asp"><%=curProdArray(1)%></a></div>
                    <%
                        else
                            'Had a default thumb picture
                    %>
                    <div style="float:left; border:1px solid #000; margin-right:10px; padding:5px; background-color:#FFF;"><a href="/p-ms-<%=curProdArray(0)%>-<%=formatSEO(curProdArray(1))%>.asp"><img src="/productpics/musicSkins/musicSkinsDefault/thumbs/<%=curProdArray(9)%>" border="0" /></a></div>
                    <div style="float:left;"><a href="/p-ms-<%=curProdArray(0)%>-<%=formatSEO(curProdArray(1))%>.asp"><%=curProdArray(1)%></a></div>
                    <%
                        end if
                    else
                        'Is a standard WE Product
                    %>
                    <div style="float:left; border:1px solid #000; margin-right:8px; padding:5px; background-color:#FFF;">
                    	<div style="float:left;"><a href="/p-<%=curProdArray(0)%>-<%=formatSEO(curProdArray(1))%>.asp"><img src="/productpics/icon/<%=curProdArray(5)%>" border="0" /></a></div>
                        <%
						if instr(partNumber,"WCD-") > 0 then
							pnArray = split(partNumber,"-")
							baseCaseID = pnArray(1)
							sql = "select itemPic, price_our, itemDesc from we_Items where itemID = " & baseCaseID
							session("errorSQL") = sql
							set baseCaseRS = oConn.execute(sql)
							bcPic = baseCaseRS("itemPic")
							bcPrice = baseCaseRS("price_our")
							bcDesc = baseCaseRS("itemDesc")
							usePrice = usePrice + bcPrice
						%>
                        <div style="float:left; padding:10px 0px 0px 10px; font-weight:bold; font-size:14px;">+</div>
                        <div style="float:left; padding-left:10px;"><a href="/p-<%=baseCaseID%>-<%=formatSEO(bcDesc)%>.asp"><img src="/productpics/icon/<%=bcPic%>" border="0" /></a></div>
                        <% end if %>
                    </div>
                    <% if instr(partNumber,"WCD-") > 0 then %>
                    <div style="float:left; width:450px;">
                    	<div style="float:left; width:450px;"><a href="/p-<%=itemID%>-<%=formatSEO(curProdArray(1))%>.asp" class="product-desc-cart"><%=curProdArray(1)%></a> (<%=prepInt(curProdArray(2)) * prepInt(qty)%>)</div>
                        <div style="float:left; padding-left:15px; width:435px;"><a href="/p-<%=baseCaseID%>-<%=formatSEO(bcDesc)%>.asp" class="product-desc-cart"><%=bcDesc%></a> (<%=prepInt(qty) * prepInt(bcPrice)%>)</div>
                    </div>
                    <% else %>
                    <div style="float:left; width:550px;">
                    	<div style="float:left; width:550px;"><a href="/p-<%=curProdArray(0)%>-<%=formatSEO(curProdArray(1))%>.asp" class="product-desc-cart"><%=curProdArray(1)%></a></div>
                    </div>
                    <% end if %>
					<%
                    end if
                    %>
                </div>
                <div style="float:right; font-size:16px; text-align:left; width:150px; color:#F00;">Out of Stock</div>
            </div>
            <%
                if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
            next
            %>
            <!-- loyalty -->
            <!--
			<div style="float:left; background-color:<%=bgColor%>; width:952px; padding-top:10px; border-left:2px solid #CCC; border-right:2px solid #CCC;">
            	<div style="float:right; padding:5px 10px 0px 0px; font-size:16px;"><%=round(itemTotal,0)%> Webucks</div>
            	<div style="float:right; padding:0px 30px 10px 0px;"><img src="/images/Earn-WE-BUCKS.png" border="0" /></div>
            </div>
            -->
            <!--// loyalty -->
            <%
            finalTotal = itemTotal
            if discountTotal > itemTotal then discountTotal = itemTotal
            %>
            <div style="width:953px; float:left;">
                <% if prepInt(varientAssurance) = 0 then %>
                <div style="float:left; width:500px; height:61px; margin-top:20px; background-image:url(/images/icons/trustIcons.gif)">
                	<a href="http://www.bizrate.com/ratings_guide/cust_reviews__mid--32536.html"><div style="float:left; width:100px; height:61px;"></div></a>
                	<div style="float:left; width:100px; height:61px;"></div>
                	<a href="http://www.wirelessemporium.com/privacy.asp"><div style="float:left; width:150px; height:61px;"></div></a>
                	<a href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=13198065"><div style="float:left; width:150px; height:61px;"></div></a>
                </div>
                <% end if %>
                <div style="float:right; width:400px;">
                    <% if sPromoCode = "" then %>
                    <div style="width:100%; background-color:#FFF; float:right; padding:0px 70px 0px 0px; color:#f00; text-align:right;" id="promoError"><%=promoError%></div>
                    <div style="width:100%; background-color:#FFF; float:right; padding:10px 10px 5px 0px; color:#999; text-align:right;" id="promoLink"><a onclick="promoDisplay(1)" style="cursor:pointer; font-size:12px; font-family:Arial; color:#000;">Click to Enter Promo Code</a></div>
                    <div style="width:100%; background-color:#FFF; float:left; padding:10px 0px 5px 0px; display:none;" id="enterPromo">
                        <form style="margin:0px;" name="promoForm" method="post">
                        <div style="float:left; width:100%;">
                            <div style="float:right; font-size:16px; text-align:left; padding-top:7px; padding-left:10px;"><a onclick="document.promoForm.submit()" style="cursor:pointer;"><img src="/images/buttons/promoBttn.gif" border="0" /></a></div>
                            <div style="float:right; font-size:16px; text-align:left; padding-top:10px; padding-left:10px;"><input type="text" name="promo" value="" /></div>
                            <div style="float:right; font-size:16px; text-align:left; padding-top:10px; font-weight:bold;">Enter Promo Code:</div>
                        </div>
                        </form>
                        <div style="float:left; width:100%; font-sizteste:10px; color:#F00; text-align:right;"><div style="margin-right:60px;"><a onclick="promoDisplay(0)" style="cursor:pointer; font-family:Arial;">Cancel Promo Code</a></div></div>
                    </div>
                        <%
                        if discountTotal > 0 then
                            if instr(discountTotal,".") > 0 then discountTotal = left(discountTotal,instr(discountTotal,".")+2)
                            nProdIdCount = nProdIdCount + 1
                            PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""0""><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""Promo Discount""><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""1""><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & discountTotal * -1 & """>" & vbcrlf
                            finalTotal = itemTotal - discountTotal
                        %>
                    <div style="float:right; width:100%; background-color:#FFF; float:left; padding:10px 0px 5px 0px;">
                        <div style="float:right; font-size:16px; text-align:right; width:150px; padding:10px 10px 0px 0px; color:#CA0002;">(<%=formatCurrency(discountTotal)%>)</div>
                        <div style="float:right; font-size:16px; text-align:right; width:150px; padding-top:10px; margin-right:10px;">Discount:</div>
                    </div>
                        <% end if %>
                    <%
                    else
                        session("promocode") = sPromoCode
                        if discountTotal = 0 then
                            if isnumeric(promoPercent) then discountTotal = discountAmt * promoPercent
                            if discountTotal < 0 then discountTotal = discountTotal * -1
                        end if
                        if instr(discountTotal,".") > 0 then discountTotal = left(discountTotal,instr(discountTotal,".")+2)
                        if discountTotal > 0 then
                            nProdIdCount = nProdIdCount + 1
                            PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""0""><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""Promo Discount""><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""1""><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & discountTotal * -1 & """>" & vbcrlf
                        end if
                        finalTotal = itemTotal - discountTotal
                    %>
                    <div style="width:100%; background-color:#FFF; float:left; padding:10px 0px 5px 0px;">
                        <div style="float:right; font-size:16px; text-align:right; width:150px; padding:10px 10px 0px 0px; color:#CA0002;">(<%=formatCurrency(discountTotal)%>)</div>
                        <div style="float:right; font-size:16px; text-align:right; width:150px; padding-top:10px; margin-right:10px;">Discount: </div>
                    </div>
                    <% end if %>
                    <div style="width:100%; background-color:#FFF; float:left; padding:10px 0px 5px 0px;">
                        <div style="float:right; font-size:16px; text-align:right; width:150px; padding:10px 10px 0px 0px;"><%=formatCurrency(0)%></div>
                        <div style="float:right; font-size:16px; text-align:right; width:220px; padding:0px 10px 0px 0px; color:#ff6600;">
                            <div style="text-align:right;"><span style="font-size:24px; color:#090;">FREE</span> <%=strFreeShipping%> Shipping:</div>
                        </div>
                    </div>
                    <% if eligibleCnt > 0 and noShopRunner = 0 then %>
                    <div style="width:100%; background-color:#FFF; float:left; padding:10px 0px 5px 0px;">
                    	<div name="sr_shippingSummaryDiv"></div>
                    </div>
                    <% end if %>
                    <div style="width:100%; background-color:#FFF; float:left; padding-bottom:10px;">
                        <div style="float:right; font-size:24px; font-weight:bold; text-align:right; padding-right:10px; width:150px;"><%=formatCurrency(finalTotal)%></div>
                        <div style="float:right; font-size:24px; font-weight:bold; text-align:right; width:200px; padding-right:10px; color:#000;">Item Total:</div>
                    </div>
                </div>
            </div>
            <div style="width:953px; background-color:#FFF; float:left; margin:0px 0px 10px 0px; padding-top:10px; border-top:2px solid #ff6600;">
                <div style="float:left; font-size:20px; margin-top:15px;">Over 1,000,000 Orders Shipped!</div>
                <div style="float:left; text-align:left; font-weight:bold; margin-left:40px;">
                    <ul style="margin:0px;">
                        <li style="color:#ff6600; line-height:20px; font-size:16px;"><span style="color:#000; font-size:14px;">90-Day EZ No-Hassle Return Policy</span></li>
                        <li style="color:#ff6600; line-height:20px; font-size:16px;"><span style="color:#000; font-size:14px;">All Orders Ship Within 24 Hours</span></li>
                        <li style="color:#ff6600; line-height:20px; font-size:16px;"><span style="color:#000; font-size:14px;">Safe & Secure Checkout</span></li>
                    </ul>
                </div>

                <% if instr(request.ServerVariables("SERVER_NAME"),"staging.") > 0 then %>
                <form name="frmCheckout" method="post" action="/cart/checkout.asp">
                <div style="float:right;"><input type="image" src="/images/buttons/secureCheckout.jpg" border="0" /></div>
                <% else %>
                <form name="frmCheckout" method="post" action="https://www.wirelessemporium.com/cart/checkout.asp">
                <div style="float:right;"><input type="image" src="/images/buttons/secureCheckout.jpg" border="0" /></div>
                <% end if %>
                </form>
            </div>
            <% if oneTime then %>
            <div style="width:953px; background-color:#FFF; float:left;">
                <div style="border-bottom:2px solid #000; width:100%; position:relative; height:10px;">
                    <div style="position:absolute; top:-3px; right:40px; font-size:14px; padding:5px;">&nbsp;</div>
                </div>
            </div>
            <div style="width:953px; background-color:#FFF; float:left; margin-top:10px;">
                <div style="float:left;"><a href="/" style="color:#999; text-decoration:underline; font-size:20px; padding-bottom:20px;">Continue Shopping</a></div>
            </div>
            <% else %>
            <div style="width:953px; background-color:#FFF; float:left;">
                <div style="border-bottom:2px solid #000; width:100%; position:relative; height:10px;">
                    <div style="position:absolute; top:-3px; right:40px; font-size:14px; background-color:#FFF; padding:5px;">Or Checkout With</div>
                </div>
            </div>
            <div style="width:953px; background-color:#FFF; float:left; margin-top:10px;">
                <div style="float:left;"><a href="/" style="color:#999; text-decoration:underline; font-size:20px;">Continue Shopping</a></div>
                <div style="float:right;">
                	<% if prepStr(session("sr_token")) = "" then %>
                    <div style="margin-top:10px;"><a onclick="document.googleForm.submit()" style="cursor:pointer;"><img src="https://checkout.google.com/buttons/checkout.gif?merchant_id=151073323883147&w=168&h=44&style=white&variant=text&loc=en_US" border="0" /></a></div>
                    <% end if %>
                    <div style="margin-top:10px;"><a onclick="document.PaypalForm.submit()" style="cursor:pointer;"><img src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" border="0" /></a></div>
                </div>
            </div>
            <% end if %>
        </td>
    </tr>
</table>
<form name="googleForm" method="post" action="/cart/process/google/CartProcessing.asp">
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <input type="hidden" name="analyticsdata" value="">
    <input type="hidden" name="sWeight" value="<%=cStr(sWeight)%>">
    <%=WEhtml2%>
</form>
<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.asp">
    <input type="hidden" name="numItems" value="<%=nProdIdCount%>">
    <input type="hidden" name="paymentAmount" value="<%=formatNumber(finalTotal,2)%>">
    <input type="hidden" name="ItemsAndWeight" value="<%=cStr(nProdIdCount) & "|" & cStr(sWeight)%>">
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <%=PPhtml%>
</form>
<%
function fWriteBasketRow(nProdId,sItemName,sItemPrice,nQty,sRetailPrice,sItemPic,lockQty,showDelete,musicSkins,musicSkinsDefaultImg)
	'this will write out the row for the basket
	dim sTemp
	sTemp = "<tr>" & vbcrlf
	if musicSkins = 1 then
		if fs.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsSmall") & "\" & sItemPic) then
			sTemp = sTemp & "<td width=""74"" align=""center"" bgcolor=""#FFFFFF""><a href=""/p-ms-" & nProdId & "-" & formatSEO(sItemName) & ".asp""><img src=""/productpics/musicSkins/musicSkinsSmall/" & sItemPic & """ width=""45"" height=""45"" border=""0""></a></td>" & vbcrlf
		else
			sTemp = sTemp & "<td width=""74"" align=""center"" bgcolor=""#FFFFFF""><a href=""/p-ms-" & nProdId & "-" & formatSEO(sItemName) & ".asp""><img src=""/productpics/musicSkins/musicSkinsDefault/thumbs/" & musicSkinsDefaultImg & """ width=""45"" height=""45"" border=""0""></a></td>" & vbcrlf
		end if
		sTemp = sTemp & "<td width=""419"" align=""left"" valign=""top"" bgcolor=""#FFFFFF""><a href=""/p-ms-" & nProdId & "-" & formatSEO(sItemName) & ".asp"" class=""cart-text"">" & sItemName & "</a></td>" & vbcrlf
	else
		sTemp = sTemp & "<td width=""74"" align=""center"" bgcolor=""#FFFFFF""><a href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".asp""><img src=""/productpics/icon/" & sItemPic & """ width=""45"" height=""45"" border=""0""></a></td>" & vbcrlf
		sTemp = sTemp & "<td width=""419"" align=""left"" valign=""top"" bgcolor=""#FFFFFF""><a href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".asp"" class=""cart-text"">" & sItemName & "</a></td>" & vbcrlf
	end if
	sTemp = sTemp & "<td align=""center"" valign=""top"" bgcolor=""#FFFFFF"" class=""cart-text""><s>" & formatCurrency(sRetailPrice) & "</s></td>" & vbcrlf
	sTemp = sTemp & "<td align=""center"" valign=""top"" bgcolor=""#FFFFFF"" class=""cart-text"">" & formatCurrency(sItemPrice) & "</td>" & vbcrlf
	sTemp = sTemp & "<td align=""center"" valign=""top"" bgcolor=""#FFFFFF"" class=""cart-text""><font color=""#FF0000"">" & formatCurrency(sRetailPrice - sItemPrice) & "</font></td>" & vbcrlf
	sTemp = sTemp & "<td align=""center"" valign=""top"" bgcolor=""#FFFFFF"" class=""cart-text"">" & vbcrlf
	sTemp = sTemp & "<form name=""frmEditDelete" & nProdId & """ action=""/cart/item_edit.asp"" method=""post"">" & vbcrlf
	sTemp = sTemp & "<input type=""hidden"" name=""prodid"" value=""" & nProdId & """>" & vbcrlf
	sTemp = sTemp & "<input type=""hidden"" name=""pagetype"" value=""edit"">" & vbcrlf
	if lockQty then
		sTemp = sTemp & nQty & "<input type=""hidden"" name=""qty"" size=""2"" maxlength=""3"" value=""" & nQty & """ class=""cart-text""><br>" & vbcrlf
		if showDelete then
			sTemp = sTemp & "<input type=""image"" src=""/images/cart/btn_delete.gif"" height=""15"" width=""50"" onClick=""EditDeleteItem(this.form.name,'delete');"">" & vbcrlf
		end if 
	else
		sTemp = sTemp & "<input type=""text"" name=""qty"" size=""2"" maxlength=""3"" value=""" & nQty & """ class=""cart-text""><br>" & vbcrlf
		sTemp = sTemp & "<input type=""image"" src=""/images/cart/btn_edit.gif"" height=""16"" width=""39"" onClick=""EditDeleteItem(this.form.name,'edit');"">" & vbcrlf
		sTemp = sTemp & "<input type=""image"" src=""/images/cart/btn_delete.gif"" height=""15"" width=""50"" onClick=""EditDeleteItem(this.form.name,'delete');"">" & vbcrlf
	end if
	sTemp = sTemp & "</form>" & vbcrlf
	sTemp = sTemp & "</td>" & vbcrlf
	if sItemPrice = 0 then showPrice = "FREE" else showPrice = formatCurrency((cDbl(sItemPrice) * cDbl(nQty)))
	sTemp = sTemp & "<td align=""right"" valign=""top"" bgcolor=""#FFFFFF"" class=""cart-text"">" & showPrice & "</td>" & vbcrlf
	sTemp = sTemp & "</tr>" & vbcrlf
	fWriteBasketRow = sTemp
end function

function fWriteErrorBasketRow(nProdId,sItemName,sItemPrice,nQty,sRetailPrice,sItemPic,lockQty,showDelete,musicSkins,musicSkinsDefaultImg)
	'this will write out the row for the basket
	dim sErrorTemp
	sErrorTemp = "<tr>" & vbcrlf
	if musicSkins = 1 then
		if fs.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsSmall") & "\" & sItemPic) then
			sErrorTemp = sErrorTemp & "<td width=""74"" align=""center"" bgcolor=""#FFFFFF""><a href=""/p-ms-" & nProdId & "-" & formatSEO(sItemName) & ".asp""><img src=""/productpics/musicSkins/musicSkinsSmall/" & sItemPic & """ width=""45"" height=""45"" border=""0""></a></td>" & vbcrlf
		else
			sErrorTemp = sErrorTemp & "<td width=""74"" align=""center"" bgcolor=""#FFFFFF""><a href=""/p-ms-" & nProdId & "-" & formatSEO(sItemName) & ".asp""><img src=""/productpics/musicSkins/musicSkinsDefault/thumbs/" & musicSkinsDefaultImg & """ width=""45"" height=""45"" border=""0""></a></td>" & vbcrlf
		end if
		sErrorTemp = sErrorTemp & "<td width=""419"" align=""left"" valign=""top"" bgcolor=""#FFFFFF""><a href=""/p-ms-" & nProdId & "-" & formatSEO(sItemName) & ".asp"" class=""cart-text"">" & sItemName & "</a></td>" & vbcrlf
	else
		sErrorTemp = sErrorTemp & "<td width=""74"" align=""center"" bgcolor=""#FFFFFF""><a href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".asp""><img src=""/productpics/icon/" & sItemPic & """ width=""45"" height=""45"" border=""0""></a></td>" & vbcrlf
		sErrorTemp = sErrorTemp & "<td width=""419"" align=""left"" valign=""top"" bgcolor=""#FFFFFF""><a href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".asp"" class=""cart-text"">" & sItemName & "</a></td>" & vbcrlf
	end if
	sErrorTemp = sErrorTemp & "<td align=""center"" valign=""top"" bgcolor=""#FFFFFF"" class=""cart-text""><s>" & formatCurrency(sRetailPrice) & "</s></td>" & vbcrlf
	sErrorTemp = sErrorTemp & "<td align=""center"" valign=""top"" bgcolor=""#FFFFFF"" class=""cart-text"">" & formatCurrency(sItemPrice) & "</td>" & vbcrlf
	sErrorTemp = sErrorTemp & "<td align=""center"" valign=""top"" bgcolor=""#FFFFFF"" class=""cart-text"" colspan=""3""><font color=""#FF0000"">This item is out of stock</font></td>" & vbcrlf
	sErrorTemp = sErrorTemp & "</tr>" & vbcrlf
	fWriteErrorBasketRow = sErrorTemp
end function

if baseCaseList <> "" then
	baseCaseArray = split(baseCaseList,"##")
	for i = 0 to ubound(baseCaseArray)
		if prepStr(baseCaseArray(i)) <> "" then
			cureCaseArray = split(baseCaseArray(i),"@@")
			baseCaseID = cureCaseArray(0)
			qty = cureCaseArray(1)
			
			sql = "exec sp_qtyInBasketByItemID 0," & baseCaseID & "," & mySession
			saveSQL = sql
			session("errorSQL") = sql
			set preRS = oConn.execute(sql)
			if not preRS.EOF then preAmt = prepInt(preRS("qty")) else preAmt = 0
			preRS = null
			
			sql = "exec sp_addOrUpdateItemToBasket 0," & baseCaseID & "," & mySession & "," & qty
			session("errorSQL") = sql
			oConn.execute(sql)
			
			sql = "exec sp_qtyInBasketByItemID 0," & baseCaseID & "," & mySession
			session("errorSQL") = sql
			set postRS = oConn.execute(sql)
			if not postRS.EOF then postAmt = prepInt(postRS("qty")) else postAmt = 0
			postRS = null
			
			if postAmt <> preAmt then
				response.Redirect("/cart/basket.asp")
			end if
		end if
	next
end if
response.Write("<!-- sid:" & mySession & " -->")
%>

<!--#include virtual="/includes/template/bottom_cart.asp"-->

<script language="javascript">
function reDirect() {
	window.location = "http://<%=request.servervariables("SERVER_NAME")%>/index.asp";
}

function EditDeleteItem(nForm,thisAction) {
	var frmName = eval('document.' + nForm);
	if (isNaN(frmName.qty.value)) {
		alert("Please enter a valid number for Quantity.");
	} else {
		if (thisAction == 'delete') {
			frmName.action = "/cart/item_delete.asp<%=csidLink%>";
		} else {
			frmName.action = "/cart/item_edit.asp<%=csidLink%>";
		}
		frmName.pagetype.value = thisAction;
		frmName.submit();
	}
}

function CheckSubmit() {
	bValid = true;
	CheckValidNEW(document.CheckoutForm.shipZip.value, "Your Shipping Zip is a required field!");
	if (bValid) {
		<%
		if instr(request.servervariables("SERVER_NAME"),"staging") > 0 then
			response.write "document.CheckoutForm.action = 'http://staging.wirelessemporium.com/cart/checkout.asp';"
		else
			response.write "document.CheckoutForm.action = 'https://www.wirelessemporium.com/cart/checkout.asp';"
		end if
		'response.write "document.CheckoutForm.action = '/cart/checkout.asp';"
		%>
		document.CheckoutForm.shipZip.value = document.CheckoutForm.shipZip.value;
		document.CheckoutForm.submit();
	}
	return false;
}

function promoDisplay(num) {
	if (num == 0) {
		document.getElementById("promoError").style.display = 'none'
		document.getElementById("promoLink").style.display = ''
		document.getElementById("enterPromo").style.display = 'none'
	}
	else if (num == 1) {
		document.getElementById("promoError").style.display = 'none'
		document.getElementById("promoLink").style.display = 'none'
		document.getElementById("enterPromo").style.display = ''
	}
}
</script>
