<%
dim useHttps : useHttps = 1
'response.write session.SessionID
pageName = "checkout"
mvtGaq = "checkout_orig_ID423"
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

refreshForLogin = request.QueryString("l")
if isnull(refreshForLogin) or len(refreshForLogin) < 1 then refreshForLogin = 0
noCommentBox = true

'Allow for logins and promo codes
if refreshForLogin = 1 then
	response.Cookies("checkoutPage") = ""
	response.Cookies("checkoutPage").expires = date - 1
end if
	
dim curSite
if bStaging then
	curSite = "http://staging"
else
	curSite = "https://www"
end if
strHttp = useHttp

dim cart
cart = 1

varientTopBanner = 2
varientTopNav = 1
varientAssurance = 0

set fs = CreateObject("Scripting.FileSystemObject")
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
'mySession = request.QueryString("passSessionID")
'mySrToken = request.QueryString("passSrToken")
mySrToken = request.Cookies("srLogin")


dim SEtitle, SEdescription, SEkeywords
SEtitle = ""
SEdescription = ""
SEkeywords = ""

dim pageTitle, strBody
pageTitle = ""
strBody = "<body leftmargin=""0"" topmargin=""0"" onload=""changeTax(document.frmProcessOrder.sState.value);"">"
%>
<!--#include virtual="/includes/template/top_cart.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<%
dim HTTP_REFERER
HTTP_REFERER = request.servervariables("HTTP_REFERER")
'if left(HTTP_REFERER,47) <> "http://www.wirelessemporium.com/cart/basket.asp" and left(HTTP_REFERER,48) <> "https://www.wirelessemporium.com/cart/basket.asp" and left(HTTP_REFERER,50) <> "https://www.wirelessemporium.com/cart/checkout.asp" and left(HTTP_REFERER,51) <> "http://staging.wirelessemporium.com/cart/basket.asp" and left(HTTP_REFERER,53) <> "http://staging.wirelessemporium.com/cart/checkout.asp" and refreshForLogin = 0 then
'	response.write "<h3>This page will only accept forms submitted from the Wireless Emporium secure website.</h3><!-- " & request.ServerVariables("HTTP_REFERER") & " -->" & vbcrlf
'	response.end
'end if

dim nOrderId, a

dim szip, sWeight
szip = request.form("shipZip")

dim sUserId
if sUserId = "" then
	'check if the user is logging in
	if request.form("submitted") = "submitted" and request.form("loginEmail") <> "" then
		SQL = "SELECT pword,fname,lname,accountid FROM we_accounts"
		SQL = SQL & " WHERE email='" & SQLquote(request.form("loginEmail")) & "' AND pword <> '' AND pword IS NOT null"
		SQL = SQL & " ORDER BY accountid DESC"
		set RS = oConn.execute(SQL)
		if not RS.eof then
			if SQLquote(request.form("pword1")) = RS("pword") then
				myAccount = RS("accountid")
				on error resume next
				response.cookies("fname") = RS("fname")
				response.cookies("fname").expires = dateAdd("m", 1, now)
				response.cookies("myAccount") = RS("accountid")
				response.cookies("myAccount").expires = dateAdd("m", 1, now)
				on error goto 0
			end if
		end if
		RS.close
		set RS = nothing
	end if
end if

dim newEmail, email, pword1, pword2, phone, Fname, Lname, sAddress1, sAddress2, sCity, sState, bAddress1, bAddress2, bCity, bState, bZip
dim radioAddress, shiptype, PaymentType, cc_cardType, cc_cardOwner, cardNum, cc_month, cc_year, hearFrom

'check if the user is already logged in
if myAccount <> "" then
	SQL = "SELECT * FROM we_accounts WHERE accountid = '" & myAccount & "'"
	set RS = oConn.execute(SQL)
	if not RS.eof then
		email = RS("email")
		fname = RS("fname")
		lname = RS("lname")
		phone = RS("phone")
		saddress1 = RS("saddress1")
		saddress2 = RS("saddress2")
		scity = RS("scity")
		sstate = RS("sstate")
		szip = RS("szip")
		bAddress1 = RS("bAddress1")
		bAddress2 = RS("bAddress2")
		bCity = RS("bCity")
		bState = RS("bState")
		bZip = RS("bZip")
	end if
	RS.close
	set RS = nothing
	radioAddress = request.form("radioAddress")
	shiptype = request.form("shiptype")
	if request.form("szip") <> "" then szip = request.form("szip")
end if

if request.form("submitted") = "submitted" and myAccount = "" then
	newEmail = request.form("newEmail")
	pword1 = request.form("pword1")
	pword2 = request.form("pword2")
	fname = request.form("fname")
	lname = request.form("lname")
	email = request.form("email")
	phone = request.form("phone")
	saddress1 = request.form("saddress1")
	saddress2 = request.form("saddress2")
	scity = request.form("scity")
	sstate = request.form("sstate")
	szip = request.form("szip")
	bAddress1 = request.form("bAddress1")
	bAddress2 = request.form("bAddress2")
	bCity = request.form("bCity")
	bState = request.form("bState")
	bZip = request.form("bZip")
	radioAddress = request.form("radioAddress")
	shiptype = request.form("shiptype")
	PaymentType = request.form("PaymentType")
	cc_cardType = request.form("cc_cardType")
	cc_cardOwner = request.form("cc_cardOwner")
	cardNum = request.form("cardNum")
	cc_month = request.form("cc_month")
	cc_year = request.form("cc_year")
	hearFrom = request.form("hearFrom")
end if

' START basket grab
dim sItemName, sRetailPrice, sItemPrice, sItemPic, nProdIdCheck, nProdQuantity, nProdIdCount, strItemCheck, strQty, nItemTotal
dim WEhtml, WEhtml2, PPhtml, EBtemp, EBxml

nProdIdCount = 0
strItemCheck = ""
strQty = ""
discountAmt = 0

SQL = "UPDATE ShoppingCart SET DateCheckout = '" & now & "' WHERE store = 0 AND sessionID = '" & mySession & "' AND (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
oConn.execute SQL

SQL = "SELECT a.id shoppingcartID, c.brandName, d.modelName, a.musicSkins, a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, B.itemDesc, B.itemPic, '' defaultPic, B.price_Our, B.price_retail, B.Condition, B.itemWeight, B.NoDiscount FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID and a.musicSkins = 0 left join we_Brands c on b.brandID = c.brandID left join we_Models d on b.modelID = d.modelID WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
SQL = SQL & " union"
SQL = SQL & " SELECT a.id shoppingcartID, c.brandName, d.modelName, a.musicSkins, a.customCost, a.lockQty, A.itemID, A.qty, B.modelID, 20 as typeID, B.musicSkinsID as PartNumber, isnull(c.brandName, '') + ' ' + isnull(d.modelName, '') + ' ' + isnull(B.artist, '') + ' ' + isnull(b.designName, '') as itemDesc, B.image as itemPic, B.defaultImg as defaultPic, b.price_we as price_Our, b.msrp as price_retail, 0 as Condition, 1 as itemWeight, 0 as NoDiscount FROM ShoppingCart A INNER JOIN we_items_musicSkins B ON A.itemID=B.id and a.musicSkins = 1 left join we_Brands c on b.brandID = c.brandID left join we_Models d on b.modelID = d.modelID WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
SQL = SQL & " order by partNumber desc, shoppingcartID"

set RS = Server.CreateObject("ADODB.Recordset")

RS.open SQL, oConn, 0, 1

dim itemCnt : itemCnt = 0
dim noSR : noSR = 0
activeProductCnt = 0
eligibleCnt = 0

do until RS.eof
	noDiscount = RS("NoDiscount")
	if isnumeric(noDiscount) then
		if noDiscount = 0 then noDiscount = false else noDiscount = true
	end if
	nProdIdCheck = RS("itemID")
	nProdQuantity = RS("qty")
	if isnull(rs("musicSkins")) or not rs("musicSkins") then nProdMusicSkins = 0 else nProdMusicSkins = 1
	strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
	strQty = strQty & cStr(nProdQuantity)& ","  ' QTY string for each item, this is for buySafe Use
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		useBrandName = RS("brandName")
		useModelName = RS("modelName")
		partNumber = RS("partNumber")
		sItemName = insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName)
		
		if nProdMusicSkins = 1 then
			if isnull(RS("itemPic")) then
				sItemPic = RS("defaultPic")
				musicSkinsDefaultImg = RS("defaultPic")
			else
				sItemPic = RS("itemPic")
			end if
			if instr(sItemPic,";") > 0 then
				musicSkinsDefaultImg = mid(sItemPic,instr(sItemPic,";")+1)
				sItemPic = left(sItemPic,instr(sItemPic,";")-1)
			end if		
		else
			sItemPic = RS("itemPic")
		end if
		sItemPrice = RS("price_Our")
		
		if isDropship(partNumber) or left(partNumber,3) = "WCD" then
			if left(partNumber,3) = "WCD" then noSR = 1
			srEligible = 0
		else
			if noSR = 0 then srEligible = 1
		end if

		if not isnull(rs("customCost")) then sItemPrice = rs("customCost")
		sRetailPrice = prepInt(RS("price_retail"))
		'Free Bluetooth
		if FreeBluetooth = 1 then
			if cStr(nProdIdCheck) = FreeBluetoothItemID then
				FreeBluetoothPrice = sItemPrice
			end if
		end if
		'Free Product With Phone Purchase
		if RS("typeID") = 16 then PhonePurchased = 1
		'Category Sale
		if CategorySale = 1 then
			if RS("typeID") = CategorySaleTypeID and RS("NoDiscount") = 0 then
				CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
			else
				SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
				SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_Our > 0)"
				SQL = SQL & " AND (R.typeid = '" & CategorySaleTypeID & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
				SQL = SQL & " AND I.NoDiscount = 0"
				dim RSrelated
				set RSrelated = Server.CreateObject("ADODB.Recordset")
				RSrelated.open SQL, oConn, 0, 1
				if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				RSrelated.close
				set RSrelated = nothing
			end if
		end if
		'session("errorSQL") = "nSubTotal = " & nSubTotal & " | nProdIdCheck = " & nProdIdCheck & " | sItemPrice = " & sItemPrice & " | nProdQuantity = " & nProdQuantity
		nItemTotal = nItemTotal + (sItemPrice * nProdQuantity)
		if not noDiscount and rs("typeID") <> 16 then
			discountAmt = discountAmt + (sItemPrice * nProdQuantity)
		end if
		sWeight = sWeight + (cDbl(RS("itemWeight")) * nProdQuantity)
		'WE shipping section
		if sItemPrice = 0 then showPrice = "FREE" else showPrice = formatCurrency(sItemPrice * nProdQuantity)
		productList = productList & sItemName & "##" & nProdQuantity & "##" & showPrice & "##" & srEligible & "##" & sItemPic & "##" & musicSkinsDefaultImg & "##" & nProdMusicSkins & "@@"
		
		activeProductCnt = activeProductCnt + 1
		if srEligible = 1 then eligibleCnt = eligibleCnt + 1
		
'		WEhtml = WEhtml & "<tr><td align=""left"">" & insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName) & "</td><td align=""center"">" & nProdQuantity & "</td><td align=""right"" colspan='2'>" & showPrice & "</td></tr>" & vbcrlf
		WEhtml2 = WEhtml2 & "<input type=""hidden"" name=""itemID_" & nProdIdCount & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""itemQty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & sItemName & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & nProdQuantity & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & sItemPrice & """>" & vbcrlf

		'eBillme variables
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_number_" & nProdIdCount & """ value=""" & nProdIdCheck & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_qty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_price_" & nProdIdCount & """ value=""" & sItemPrice & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_musicSkins_" & nProdIdCount & """ value=""" & nProdMusicSkins & """>" & vbcrlf
		EBxml = EBxml & "			<itemdetail>" & vbcrlf
		EBxml = EBxml & "				<id>" & nProdIdCheck & "</id>" & vbcrlf
		EBxml = EBxml & "				<name>" & insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName) & "</name>" & vbcrlf
		EBxml = EBxml & "				<quantity>" & nProdQuantity & "</quantity>" & vbcrlf
		EBxml = EBxml & "				<itemcost>" & sItemPrice & "</itemcost>" & vbcrlf
		EBxml = EBxml & "			</itemdetail>"
		
		itemCnt = itemCnt + nProdQuantity
	end if
	RS.movenext
loop
RS.close
set RS = nothing
if nProdIdCount < 1 then
	'basket currently empty
	response.redirect("/sessionexpired.asp")
	response.end
end if
' END basket grab

'GET COUPON VALUES
dim sPromoCode
if request.form("promo") <> "" then
	sPromoCode = SQLquote(request.form("promo"))
else
	sPromoCode = session("promocode")
end if
'if sPromoCode = "" then sPromoCode = prepStr(request.QueryString("passPromocode"))

if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	SQL = "SELECT * FROM WE_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		FreeItemPartNumber = RS("FreeItemPartNumber")
		setValue = RS("setValue")
		oneTime = RS("oneTime")
	else
		sPromoCode = ""
	end if
end if

dim nSubTotal
nSubTotal = nItemTotal

dim shipcost0, shipcost2, shipcost3, shipcost4, shipcost5
shipcost0 = "0.00"
if spendThisMuch = 1 and nSubTotal >= stm_total then
	shipcost2 = 0
	shiptype = 2	
else
	shipcost2 = 6.99
end if
shipcost3 = "24.99"
shipcost4 = "0.00"
'Canadian shipping Cost
shipcost5 = "21.99"

if sWeight / 16 < 1 then
	sWeight = 1
else
	sWeight = sWeight / 16
end if

if strItemCheck <> "" then strItemCheck = left(strItemCheck,len(strItemCheck)-1)

if nProdIdCount = 0 then
	htmBasketRows = htmBasketRows & "<tr><td colspan=""7""><i>Basket is currently empty.</i></td></tr>" & vbcrlf
	EmptyBasket = true
end if
%>
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->
<%
discountTotal = prepInt(discountTotal)
maxDiscount = discountTotal
if discountTotal > nItemTotal then
	discountTotal = nItemTotal
end if
maxDiscount = prepInt(maxDiscount)
activeTotal = nItemTotal - discountTotal

if instr(discountTotal,".") > 0 then discountTotal = left(discountTotal,instr(discountTotal,".")+2)
if oneTime then nsubTotal = nsubTotal - discountTotal

productArray = split(productList,"@@")

dim nAmount, nCATax
nSubTotal = nItemTotal - discountTotal
if nSubTotal = nItemTotal and discountTotal > 0 then nSubTotal = nSubTotal - discountTotal
nAmount = cDbl(shipcost) + nCATax + nSubTotal
%>
<script language="javascript">
/*
function getStyle(oElm, strCssRule){
    var strValue = "";
    if(document.defaultView && document.defaultView.getComputedStyle){
        strValue = document.defaultView.getComputedStyle(oElm, "").getPropertyValue(strCssRule);
    }
    else if(oElm.currentStyle){
        strCssRule = strCssRule.replace(/\-(\w)/g, function (strMatch, p1){
            return p1.toUpperCase();
        });
        strValue = oElm.currentStyle[strCssRule];
    }
    return strValue;
}

function updateStepNums() {
	try {
		for (i=1; i<=3; i++) {
			o = document.getElementById("div_section"+i);
			oStep = document.getElementById("div_step"+i);
			styleName = getStyle(o, "display");
			if (styleName == "table-header-group") oStep.innerHTML = "Step 1: ";
			else if (styleName == "table-row-group") oStep.innerHTML = "Step 2: ";
			else if (styleName == "table-footer-group") oStep.innerHTML = "Step 3: ";
		}
	}
	catch (e) {}	
}
*/

function changeValue(span,val) {
	if (document.all) {
		eval(span).innerHTML = val;
	} else if (document.getElementById) {
		document.getElementById(span).innerHTML = val;
	}
}
	
function changeTax(state) {
	//var shippingOptions = document.frmProcessOrder.shippingOptions.value;
	var nCATax = CurrencyFormatted(<%=activeTotal%> * <%=Application("taxMath")%>);
	if (state == "CA") {
		changeValue("CAtaxMsg","Additional <%=Application("taxDisplay")%>% Tax for Shipments Within CA");
		changeValue("CAtax","$" + nCATax);
		document.frmProcessOrder.nCATax.value = nCATax;
	} else {
		changeValue("CAtaxMsg","");
		changeValue("CAtax","$0.00");
		document.frmProcessOrder.nCATax.value = 0;
	}

	checkInternational();
	updateGrandTotal(getSelectedRadioValue(document.frmProcessOrder.shiptype), document.frmProcessOrder.shippingTotal.value);
}

function checkInternational() {
	state = document.frmProcessOrder.sState.value;
	if (state == "AB" || state == "BC" || state == "MB" || state == "NB" || state == "NL" || state == "NS" || state == "NT" || state == "NU" || state == "ON" || state == "PE" || state == "QC" || state == "SK" || state == "YT") {
		changeValue("Shipping0","USPS First Class Int'l (8-12 business days)");
		document.frmProcessOrder.shipcost2.value = '<%=shipcost5%>';
		changeValue("Shipping2","USPS Priority Int'l (3-8 business days) - <strong>$<%=shipcost5%></strong>");
		for (k=0; k<document.frmProcessOrder.shiptype.length; k++) {
			if (k>1) {
				document.frmProcessOrder.shiptype[k].disabled = true;
				document.frmProcessOrder.shiptype[k].style.display = 'none';
				if (document.getElementById('Shipping'+(k+1)) != null) {
					document.getElementById('Shipping'+(k+1)).style.color = '#fff';
				}
				if (document.frmProcessOrder.shiptype[k].checked == true) {
					document.frmProcessOrder.shiptype[k].checked = false;
					document.frmProcessOrder.shiptype[1].checked = true;
				}
			}
		}
	} 
	else {
		changeValue("Shipping0","USPS First Class (4-10 business days)");
		document.frmProcessOrder.shipcost2.value = '<%=shipcost2%>';
		changeValue("Shipping2","USPS Priority Mail (2-4 business days) - <strong>$<%=shipcost2%></strong>");
		changeValue("Shipping3","USPS Express Mail (1-2 business days) - <strong>$<%=shipcost3%></strong>");
		for (k=0; k<document.frmProcessOrder.shiptype.length; k++) {
			if (document.frmProcessOrder.shiptype[k].disabled) {
				document.frmProcessOrder.shiptype[k].disabled = false;
				document.frmProcessOrder.shiptype[k].style.display = '';
				if (document.getElementById('Shipping'+(k+1)) != null) document.getElementById('Shipping'+(k+1)).style.color = '#444';
			}
		}
	}
}

function updateGrandTotal(shipID,shipAmt) {
	document.frmProcessOrder.shiptypeID.value = shipID;
	if (eval('document.frmProcessOrder.shipcost' + shipID) == undefined) {
		document.frmProcessOrder.shippingTotal.value = shipAmt;
	} else {
		document.frmProcessOrder.shippingTotal.value = eval('document.frmProcessOrder.shipcost' + shipID).value;
	}

	// shipcost is null when it is shoprunner
	if (document.getElementById("shipcost") != null) document.getElementById("shipcost").innerHTML = "$" + CurrencyFormatted(document.frmProcessOrder.shippingTotal.value);

	var GrandTotal = Number(document.frmProcessOrder.subTotal.value) + Number(document.frmProcessOrder.nCATax.value) + Number(document.frmProcessOrder.shippingTotal.value);
	var maxDiscount = <%=maxDiscount%>;

	if ((GrandTotal - maxDiscount) < 0) 
	{
		changeValue('discountAmt', "($" + CurrencyFormatted(GrandTotal) + ')');
		document.frmProcessOrder.discountTotal.value = CurrencyFormatted(GrandTotal);
		GrandTotal = 0
	}
	else 
	{
		if (maxDiscount > 0) 
		{
			changeValue('discountAmt', "($" + CurrencyFormatted(maxDiscount) + ')');
			document.frmProcessOrder.discountTotal.value = CurrencyFormatted(maxDiscount);
		}
		GrandTotal = GrandTotal - maxDiscount
	}
	
	changeValue("GrandTotal","$" + CurrencyFormatted(GrandTotal));
	document.frmProcessOrder.grandTotal.value = CurrencyFormatted(GrandTotal);
}

function changeShippingAddress(a) {
	var b = eval('document.frmProcessOrder.sAddress1' + a);
	var c = eval('document.frmProcessOrder.sAddress2' + a);
	var d = eval('document.frmProcessOrder.sCity' + a);
	var e = eval('document.frmProcessOrder.sState' + a);
	var f = eval('document.frmProcessOrder.sZip' + a);
	var g = eval('document.frmProcessOrder.sID' + a);
	document.frmProcessOrder.sAddress1.value = b.value;
	document.frmProcessOrder.sAddress2.value = c.value;
	document.frmProcessOrder.sCity.value = d.value;
	document.frmProcessOrder.sState.value = e.value;
	document.frmProcessOrder.sZip.value = f.value;
	changeTax(e.value);
}

function CheckSubmit() {
	trackingBit('clickedPlaceMyOrder');
	
	var f = document.frmProcessOrder;
		
	bValid = true;
	<%if myAccount = "" then%>
	if (f.sameaddress.checked == 0) {
		ShipToBillPerson(f);
	}
	<% end if %>
	
	CheckValidNEW(f.email.value, "Email is a required field!");
	CheckValidNEW(f.fname.value, "Your First Name is a required field!");
	CheckValidNEW(f.lname.value, "Your Last Name is a required field!");
	CheckValidNEW(f.phoneNumber.value, "Your Phone Number is a required field!");
	CheckValidNEW(f.sAddress1.value, "Your Address (for Shipping Address) is a required field!");
	CheckValidNEW(f.sCity.value, "Your City (for Shipping Address) is a required field!");
	sString = f.sState.options[f.sState.selectedIndex].value;
	CheckValidNEW(sString, "State (for Shipping Address) is a required field!");
	CheckValidNEW(f.sZip.value, "Your Zip Code (for Shipping Address) is a required field!");
	if (f.sZip.value.length < 5) {
		alert("You must enter a valid US or Canadian postal code")
		bValid = false
	}
	if (f.sameaddress.checked == true) {
		CheckValidNEW(f.bAddress1.value, "Your Address (for Billing Address) is a required field!");
		CheckValidNEW(f.bCity.value, "Your City (for Billing Address) is a required field!");
		sString = f.bState.options[f.bState.selectedIndex].value;
		CheckValidNEW(sString, "State (for Billing Address) is a required field!");

		CheckValidNEW(f.bZip.value, "Your Zip Code (for Billing Address) is a required field!");
		if (f.bZip.value.length < 5) {
			alert("You must enter a valid US or Canadian postal code")
			bValid = false
		}
	}

/*	if (f.PaymentType[0].checked) {*/
	if (true) {
		sString = f.cc_cardType.options[f.cc_cardType.selectedIndex].value;
		CheckValidNEW(sString, "Credit Card Type is a required field!");
		CheckValidNEW(f.cc_cardOwner.value, "Your Name (as it appears on your credit card) is a required field!");
		CheckValidNEW(f.cardNum.value, "Your Credit Card Number is a required field!");
		CheckValidNEW(f.cc_month.value, "Your Credit Card Expiration Month is a required field!");
		CheckValidNEW(f.cc_year.value, "Your Credit Card Expiration Year is a required field!");
		CheckValidNEW(f.secCode.value, "Your Credit Card Security Code is a required field!");
		
		if (bValid) {
			try {
				document.frmProcessOrder.action = '/framework/processing.asp';
				<%'commented by Terry on 9/26/2012: submitted form twice for IE version 7 and 8 %>
//				document.frmProcessOrder.submit();
				document.getElementById("submitBttn1").style.display = 'none'
				document.getElementById("submitBttn2").style.display = ''
			}
			catch (e) {}
			
			
			trackingBit('validatedPlaceMyOrder');
			pausecomp(1500);<%'Allow tracking request to finish (2012-10-03)%>
			return true;
		}
	}
	else {
		if (bValid) {
			try {			
				document.frmProcessOrder.action = '/cart/process/eBillMe/eBillMe.asp';
				<%'commented by Terry on 9/26/2012: submitted form twice for IE version 7 and 8 %>				
//				document.frmProcessOrder.submit();
				document.getElementById("submitBttn1").style.display = 'none'
				document.getElementById("submitBttn2").style.display = ''
			}
			catch (e) {}
			
			trackingBit('validatedPlaceMyOrder');
			pausecomp(1500);<%'Allow tracking request to finish (2012-10-03)%>
			return true;
		}
	}

	return false;
}

var bAddress1 = "";
var bAddress2 = "";
var bCity = "";
var bZip = "";
var bState = "";

var bStateIndex = "0";

function ShipToBillPerson(form) {
	if (form.accountid.value == '') {
		if (form.sameaddress.checked) {
			form.bAddress1.value = "";
			form.bAddress2.value = "";
			form.bCity.value = "";
			form.bZip.value = "";
			form.bState.selectedIndex = form.sState.options[0];
		}
		else {
			form.bAddress1.value = form.sAddress1.value;
			form.bAddress2.value = form.sAddress2.value;
			form.bCity.value = form.sCity.value;
			form.bZip.value = form.sZip.value;
			form.bState.selectedIndex = form.sState.selectedIndex;
		}
	}
}

function CurrencyFormatted(amount) {
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
//	caused an issue (10/30/2012)	
//	i = parseInt(i * 100);
//	i = i / 100;
	i = Math.ceil(i * 100) / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}

function getNewShipping()
{
	document.getElementById('dumpZone').innerHTML = "";	
	zipcode 			=	document.frmProcessOrder.sZip.value;
	curShippingTotal	=	document.frmProcessOrder.shippingTotal.value;
	curShippingType		=	document.frmProcessOrder.shiptypeID.value;

	updateURL = '/ajax/shippingDetails.asp?zip=' + zipcode + '&weight=<%=sWeight%>&shiptype=' + curShippingType + '&nTotalQuantity=<%=itemCnt%>&nSubTotal=<%=nSubTotal%>&useFunction=upscode2';
	ajax(updateURL,'dumpZone');
	checkUpdate('newShipping');
}

function checkUpdate(checkType)
{
	if (checkType == "newShipping")
	{
		if ("" == document.getElementById('dumpZone').innerHTML)
		{
			setTimeout('checkUpdate(\'newShipping\')', 50);
		} else
		{
			trackingBit('zipCodeDataPulled');
			document.getElementById('shippingDetails').innerHTML = document.getElementById('dumpZone').innerHTML;
			newShipType = getSelectedRadioValue(document.frmProcessOrder.shiptype);
			checkInternational();
			if (newShipType == "") 
			{
				document.frmProcessOrder.shiptype[0].checked = true;
				updateGrandTotal(0,<%=shipcost0%>);
			} else
			{
				updateGrandTotal(newShipType,eval('document.frmProcessOrder.shipcost' + newShipType).value);
			}
		}
	}
}

function getSelectedRadioValue(buttonGroup) {
    var i = getSelectedRadio(buttonGroup);
   if (i == -1) {
      return "";
   } else {
      if (buttonGroup[i]) { // Make sure the button group is an array (not just one button)
         return buttonGroup[i].value;
      } else { // The button group is just the one button, and it is checked
         return buttonGroup.value;
      }
   }
}

function getSelectedRadio(buttonGroup) {
   if (buttonGroup[0]) {
      for (var i=0; i<buttonGroup.length; i++) {
         if (buttonGroup[i].checked) {
            return i
         }
      }
   } else {
      if (buttonGroup.checked) { return 0; } // if the one button is checked, return zero
   }
   return -1;
}

function pausecomp(ms) {
	ms += new Date().getTime();
	while (new Date() < ms){}
} 

</script>
<form action="/cart/checkout.asp?l=1" method="post" name="frmProcessOrder">
<input type="hidden" name="trackingSessionID" value="" id="trackingSessionID">
<input type="hidden" name="shippingTotal" value="">
<input type="hidden" name="shiptypeID" value="<%=shiptype%>">
<input type="hidden" name="sWeight" value="<%=sWeight%>">

<input type="hidden" name="sPromoCode" value="<%=sPromoCode%>">
<input type="hidden" name="nPromo" value="<%=couponid%>">
<input type="hidden" name="discountTotal" value="<%=discountTotal%>">

<input type="hidden" name="nCATax" value="<%=nCATax%>">
<input type="hidden" name="subTotal" value="<%=nItemTotal%>">
<input type="hidden" id="totalPrice" name="grandTotal" value="<%=nAmount%>">
<input type="hidden" name="accountid" value="">
<input type="hidden" name="parentAcctID" value="">
<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
    	<td width="100%">
            <div style="float:left; width:100%; text-align:left; padding-top:10px;">
                <div style="width:100%; float:left; border-bottom:1px solid #ccc;">
                	<div style="float:left;">
	                    <div style="float:left; font-size:30px; color:#333;">Secure Checkout</div>
                        <div style="float:left; padding:8px 0px 0px 8px;"><img src="/images/checkout/lock-icon.png" border="0" /></div>
                    </div>
                    <div style="float:right; padding-top:5px;">
                    	<div style="float:left;"><img src="/images/checkout/circle-1-off.png" border="0" /></div>
                    	<div style="float:left; color:#ccc; font-size:16px; padding:2px 0px 0px 5px;">Cart Summary</div>
                    	<div style="float:left; padding-left:15px;"><img src="/images/checkout/circle-2-on.png" border="0" /></div>
                    	<div style="float:left; font-size:16px; padding:2px 0px 0px 5px; font-weight:bold;">Secure Checkout</div>
                    	<div style="float:left; padding-left:15px;"><img src="/images/checkout/circle-3-off.png" border="0" /></div>
                    	<div style="float:left; color:#ccc; font-size:16px; padding:2px 0px 0px 5px;">Receipt</div>
                    </div>
                </div>
				<!-- step1 -->
				<div style="float:left; width:100%;">
	                <div style="width:100%; float:left; padding:25px 0px 10px 0px;"><span id="div_step2" style="color:#ff6600; font-size:18px; font-weight:bold;">Step 1: </span><span style="font-size:18px; color:#333;">Enter Shipping Information</span></div>
                    <div class="cartWrapper">
                        <div style="padding:10px; display:inline-block; float:left; font-size:20px; color:#555;">Shipping Address:</div>
                        <div style="width:100%; float:left;">
                            <div class="cartShippingLeft">
                            	<div class="cartLeftRow">
                                	<div class="cartLeftText">Email:</div>
                                	<div class="cartLeftTextField"><input type="text" name="email" style="width:220px;" value="<%=email%>" onchange="getCustomerData(this.value)"></div>
                                	<div class="cartLeftStar"><img src="/images/checkout/asterisk.png" border="0" /></div>
                                </div>
                            	<div class="cartLeftRow">
                                	<div class="cartLeftText">First Name:</div>
                                	<div class="cartLeftTextField"><input type="text" name="fname" style="width:220px;" value="<%=fname%>" onchange="newAcctInfo()"></div>
                                	<div class="cartLeftStar"><img src="/images/checkout/asterisk.png" border="0" /></div>
                                </div>
                            	<div class="cartLeftRow">
                                	<div class="cartLeftText">Last Name:</div>
                                	<div class="cartLeftTextField"><input type="text" name="lname" style="width:220px;" value="<%=lname%>" onchange="newAcctInfo()"></div>
                                	<div class="cartLeftStar"><img src="/images/checkout/asterisk.png" border="0" /></div>
                                </div>
                            	<div class="cartLeftRow">
                                	<div class="cartLeftText">Address:</div>
                                	<div class="cartLeftTextField"><input type="text" name="sAddress1" style="width:220px;" value="<%=saddress1%>" onchange="newAcctInfo()"></div>
                                	<div class="cartLeftStar"><img src="/images/checkout/asterisk.png" border="0" /></div>
                                </div>
                            	<div class="cartLeftRow">
                                	<div class="cartLeftText">&nbsp;</div>
                                	<div class="cartLeftTextField"><input type="text" name="sAddress2" style="width:220px;" value="<%=saddress2%>" onchange="newAcctInfo()"></div>
                                	<div class="cartLeftStar">&nbsp;</div>
                                </div>
                            	<div class="cartLeftRow">
                                	<div class="cartLeftText">City:</div>
                                	<div class="cartLeftTextField"><input type="text" name="sCity" style="width:220px;" value="<%=scity%>" onchange="newAcctInfo()"></div>
                                	<div class="cartLeftStar"><img src="/images/checkout/asterisk.png" border="0" /></div>
                                </div>
                            	<div class="cartLeftRow">
                                	<div class="cartLeftText">State:</div>
                                	<div class="cartLeftTextField">
                                    	<select name="sState" style="width:225px;" onChange="changeTax(this.value); newAcctInfo();">
		                                    <%getStates(sstate)%>
	                        	        </select>
									</div>
                                	<div class="cartLeftStar"><img src="/images/checkout/asterisk.png" border="0" /></div>
                                </div>
                            	<div class="cartLeftRow">
                                	<div class="cartLeftText">Zip/Postal:</div>
                                	<div class="cartLeftTextField"><input type="text" name="sZip" value="<%=szip%>" style="width:220px;" maxlength="6" onchange="newAcctInfo(); getNewShipping();"></div>
                                	<div class="cartLeftStar"><img src="/images/checkout/asterisk.png" border="0" /></div>
                                </div>
                            	<div class="cartLeftRow">
                                	<div class="cartLeftText">Phone:</div>
                                	<div class="cartLeftTextField"><input type="text" name="phoneNumber" style="width:220px;" value="<%=phone%>" onchange="newAcctInfoTrack(this)"></div>
                                	<div class="cartLeftStar"><img src="/images/checkout/asterisk.png" border="0" /></div>
                                </div>
                            	<div class="cartLeftRow" style="padding-top:20px;">
                                    <div style="width:60px; text-align:center; float:left;"><input name="chkOptMail" value="Y" type="checkbox" checked="checked"></div>
                                    <div style="width:298px; padding-right:10px; text-align:left; float:left; font-size:13px; color:#444;">Please notify me via email of exclusive periodic<br />Wireless Emporium coupon code offers.</div>                                
                                </div>
                            	<div class="cartLeftRow" style="padding-top:20px;">
                                    <div style="width:60px; text-align:center; float:left;"><input id="sameaddress" type="checkbox" name="sameaddress" value="yes" OnClick="javascript:showBillingInfo();"></div>
                                    <div style="width:298px; padding-right:10px; text-align:left; float:left; font-size:13px; color:#444;">Check if Billing Address is different than the Shipping Address.</div>
                                </div>
                                <div id="billing_info" style="width:100%; float:left; padding-top:20px;">
									<div style="padding:10px 10px 10px 0px; display:inline-block; float:left; font-size:20px; color:#555;">Billing Address:</div>
                                    <div class="cartLeftRow">
                                        <div class="cartLeftText">Address:</div>
                                        <div class="cartLeftTextField"><input type="text" name="bAddress1" style="width:220px;" value="<%=bAddress1%>" onchange="newAcctInfo()"></div>
                                        <div class="cartLeftStar"><img src="/images/checkout/asterisk.png" border="0" /></div>
                                    </div>
                                    <div class="cartLeftRow">
                                        <div class="cartLeftText">&nbsp;</div>

                                        <div class="cartLeftTextField"><input type="text" name="bAddress2" style="width:220px;" value="<%=bAddress2%>" onchange="newAcctInfo()"></div>
                                        <div class="cartLeftStar">&nbsp;</div>
                                    </div>
                                    <div class="cartLeftRow">
                                        <div class="cartLeftText">City:</div>
                                        <div class="cartLeftTextField"><input type="text" name="bCity" style="width:220px;" value="<%=bCity%>" onchange="newAcctInfo()"></div>
                                        <div class="cartLeftStar"><img src="/images/checkout/asterisk.png" border="0" /></div>
                                    </div>
                                    <div class="cartLeftRow">
                                        <div class="cartLeftText">State:</div>
                                        <div class="cartLeftTextField">
                                            <select name="bState" style="width:225px;" onChange="newAcctInfo();">
                                                <%getStates(bState)%>
                                            </select>
                                        </div>
                                        <div class="cartLeftStar"><img src="/images/checkout/asterisk.png" border="0" /></div>
                                    </div>
                                    <div class="cartLeftRow">
                                        <div class="cartLeftText">Zip/Postal:</div>
                                        <div class="cartLeftTextField">
	                                        <% if prepStr(mySrToken) = "" then %>
											<input type="text" name="bZip" value="<%=bZip%>" style="width:220px;" maxlength="6" onchange="newAcctInfo();">
                                            <% else %>
											<input type="text" name="bZip" value="<%=bZip%>" style="width:220px;" maxlength="6" >
                                            <% end if %>
										</div>
                                        <div class="cartLeftStar"><img src="/images/checkout/asterisk.png" border="0" /></div>
                                    </div>
								</div>
							</div>
							<div class="cartShippingRight">
							<% 
							if prepStr(mySrToken) = "" or eligibleCnt = 0 then 
							%>
								<% if eligibleCnt > 0 then %>
                            	<div style="width:100%; float:left; text-align:center; margin-bottom:10px;">
                                	<div style="width:330px; padding-left:49px;"><div name="sr_shippingOptionDiv"></div></div>
                                </div>
								<% end if %>
								<% if noSR = 1 then %>
                            	<div style="width:100%; float:left; text-align:center;">
									<div style="width:100%; margin-bottom:10px; height:30px; font-weight:bold; border-bottom:1px solid #333; padding-bottom:10px;">Please allow up to <%=gblCustomShippingDate%> business days for production of custom cases prior to shipping time.</div>
                                </div>
								<% end if %>
                            	<div style="width:100%; float:left; text-align:center; padding-top:5px;">
	                                <img src="/images/icons/shippingHeader.jpg" border="1" title="Shipping Options" />
                                </div>
                            	<div style="width:100%; float:left; text-align:left; padding-top:15px;">
                                	<div style="width:100%; float:left; padding-bottom:5px; font-size:13px; color:#444;">
										<input type="hidden" name="shipcost0" value="<%=shipcost0%>"><input type="radio" name="shiptype" id="rad_ship1" value="0" onClick="updateGrandTotal(this.value,'<%=shipcost0%>');"<%if shiptype = "0" or shiptype = "" then response.write " checked"%>><span style="color:#ff6600;font-weight:bold;">FREE SHIPPING</span> <span id="Shipping0">USPS First Class (4-10 business days)</span>
                                    </div>
                                    <% if shipcost2 = 0 then %>
                                	<div style="width:100%; float:left; padding-bottom:5px; font-size:13px; color:#444;">
	                                    <input type="hidden" name="shipcost2" value="<%=shipcost2%>"><input type="radio" name="shiptype" id="rad_ship2" value="2" onClick="updateGrandTotal(this.value,'<%=shipcost2%>');"<%if shiptype = "2" then response.write " checked"%>><span style="color:#db3300;font-weight:bold;">FREE SHIPPING</span> <span id="Shipping2">USPS Priority Mail (2-4 business days) - <span style="font-weight:bold;"><%=formatcurrency(shipcost2)%></span></span>
									</div>                                    
                                    <% else %>
                                	<div style="width:100%; float:left; padding-bottom:5px; font-size:13px; color:#444;">
	                                    <input type="hidden" name="shipcost2" value="<%=shipcost2%>"><input type="radio" name="shiptype" id="rad_ship2" value="2" onClick="updateGrandTotal(this.value,'<%=shipcost2%>');"<%if shiptype = "2" then response.write " checked"%>><span id="Shipping2">USPS Priority Mail (2-4 business days) - <span style="font-weight:bold;"><%=formatcurrency(shipcost2)%></span></span>
									</div>
                                    <% end if %>
                                	<div style="width:100%; float:left; padding-bottom:5px; font-size:13px; color:#444;">
	                                    <input type="hidden" name="shipcost3" value="<%=shipcost3%>"><input type="radio" name="shiptype" id="rad_ship3" value="3" onClick="updateGrandTotal(this.value,'<%=shipcost3%>');"<%if shiptype = "3" then response.write " checked"%>><span id="Shipping3">USPS Express Mail (1-2 business days) - <span style="font-weight:bold;">$24.99</span></span>
									</div>
									<%
                                    on error resume next
                                    %>
                                    <div id="shippingDetails">
                                    <%
                                    if sZip <> "" then
                                        response.write UPScode2(sZip,sWeight,shiptype,nTotalQuantity,0,0)
                                    else
                                        response.Write("<div style='float:left; color:#555; font-weight:bold; padding-top:15px; width:100%; text-align:center; font-style:italic;'>Enter your zipcode for more shipping options</div>")
                                    end if
                                    %>
                                    </div>
                                    <%
                                    on error goto 0
                                    shipcost = request.form("shipcost" & request.form("shiptype"))
                                    %>
                                </div>
							<%
							else
								session("sr_token") = prepStr(mySrToken)
							%>
                            	<div style="width:100%; float:left; text-align:center;">
                                	<div style="font-weight:bold; font-size:16px; padding-bottom:5px; color:#ff6600; border-bottom:1px solid #CCC;">Shipping Options</div>
									<% if eligibleCnt = activeProductCnt then %>
									<div style="padding-top:5px;" name="sr_shippingOptionDiv"></div>
									<% else %>
                                    <div style="padding-top:10px; font-weight:bold;">ShopRunner Eligible Products</div>
                                    <div name="sr_shippingOptionDiv"></div>
                                    <div style="padding-top:10px; font-weight:bold;">Non-ShopRunner Eligible Products</div>
                                    <div align="left"><input type="hidden" name="shipcost0" value="<%=shipcost0%>"><input type="radio" name="shiptype" id="rad_ship1" value="0" onClick="updateGrandTotal(this.value,'<%=shipcost0%>');"<%if shiptype = "0" or shiptype = "" then response.write " checked"%>><span style="color:#db3300;font-weight:bold;">FREE SHIPPING</span> <span id="Shipping0">USPS First Class (4-10 business days)</span></div>
									<% end if %>
                                </div>
							<%end if%>
                            </div>
                        </div>
                        <div style="width:100%; float:left; font-size:11px; color:#555; text-align:center; padding:20px 0px 15px 0px;">
                            Wireless Emporium is committed to protecting your privacy. We will never share or rent your information. Please review our privacy policy <a target="_blank" href="/privacy.asp" style="font-weight:bold; color:#ff6600;">here</a>.
                        </div>                        
                    </div>
                </div>
                <!--// step1 -->
                <!-- step2 -->
                <div style="float:left; width:100%;">
                    <div style="width:100%; float:left; padding:25px 0px 10px 0px;"><span id="div_step1" style="color:#ff6600; font-size:18px; font-weight:bold;">Step 2: </span><span style="font-size:18px; color:#333;">Review Order</span></div>
                    <div style="width:100%; float:left;">
						<div class="cartReviewHeader">
                        	<div style="width:160px; float:left; padding-top:7px; text-align:center; font-size:16px; font-weight:bold;">Qty.</div>
                        	<div style="width:480px; float:left; padding-top:7px; text-align:left; font-size:16px; font-weight:bold;">Product Description</div>
                        	<div style="width:140px; float:left; padding:7px 20px 0px 0px; text-align:right; font-size:16px; font-weight:bold;">Total Price</div>
                        </div>
                    </div>
                    <div class="cartReviewRows">
					<%
					for i = 0 to (ubound(productArray)-1)
						curArray = split(productArray(i),"##")
						activeProductCnt = activeProductCnt + 1
						cartItemDesc = curArray(0)
						cartItemQty = curArray(1)
						cartItemPrice = curArray(2)
						cartItemSR = prepInt(curArray(3))
						cartItemImage = curArray(4)
						cartItemMSDefaultImage = curArray(5)
						cartItemMusicSkins = prepInt(curArray(6))
						%>
                    	<div class="cartReviewRow">
                        	<div style="width:160px; float:left; text-align:center;">
                            	<div class="cartReviewRowQty"><%=formatnumber(cartItemQty, 0)%></div>
                            </div>
                        	<div style="width:478px; float:left; text-align:left;">
								<%
                                ImagePath = ""
                                if cartItemMusicSkins = 1 then
                                    if fs.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsSmall") & "\" & cartItemImage) then
                                        ImagePath = "/productpics/musicSkins/musicSkinsSmall/" & cartItemImage
                                    elseif fs.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs") & "\" & cartItemMSDefaultImage) then
                                        ImagePath = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & cartItemMSDefaultImage
									else
										ImagePath = "/productpics/icon/imagena.jpg"
                                    end if
                                else
                                    if fs.FileExists(Server.MapPath("/productpics/icon") & "\" & cartItemImage) then
										ImagePath = "/productpics/icon/" & cartItemImage
									else
										ImagePath = "/productpics/icon/imagena.jpg"
									end if
                                end if
                                %>
                            	<div class="cartItemImage"><img src="<%=ImagePath%>" width="45" height="45" /></div>
                            	<div class="cartItemDesc">
                                	<div style="font-size:14px; color:#00689A; padding-bottom:3px;"><%=cartItemDesc%></div>
									<% if cartItemSR = 1 then %>
                                    <a onclick="sr_$.learn('Cart Product');" href="javascript:void(0); return false;"><div name="sr_catalogProductGridDiv"></div></a>
                                    <% end if %>
								</div>
                            </div>
                        	<div style="width:140px; float:left; padding:7px 20px 0px 0px; text-align:right; font-size:15px; font-weight:bold; color:#444;">
                            	<%=formatcurrency(prepInt(cartItemPrice))%>
                            </div>
                        </div>
					<%
					next
					%>
					</div>
                    <div class="cartReviewBottom">
                        <div class="cartReviewBottomLeft">&nbsp;</div>
                        <div class="cartReviewBottomRight">
                            <div style="float:left; padding:5px 0px 5px 5px; width:322px; border-bottom:1px solid #ccc;">
                            	<div style="float:left; width:200px; margin-right:20px; font-size:15px; text-align:right;">Subtotal:</div>
                                <div style="float:left; width:82px; margin-right:20px; color:#444; font-size:15px; font-weight:bold; text-align:right;"><%=formatCurrency(nItemTotal)%></div>
							</div>
                            <div style="float:left; padding:5px 0px 5px 5px; width:322px; border-bottom:1px solid #ccc;">
                            	<div style="float:left; width:200px; margin-right:20px; font-size:15px; text-align:right;">Discount:</div>
                                <div style="float:left; width:82px; margin-right:20px; font-size:15px; font-weight:bold; text-align:right;">
                                <%
								if discountTotal > 0 then
									nProdIdCount = nProdIdCount + 1
									PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""0""><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""Promo Discount""><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""1""><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & discountTotal * -1 & """>" & vbcrlf
								%>
	                                <span id="discountAmt" style="color:#990000;"><%=formatCurrency(discountTotal * -1)%></span>
                                <%else%>
									<span id="discountAmt" style="color:#444;"><%=formatCurrency(discountTotal * -1)%></span>
                                <%end if%>
								</div>
							</div>
                            <div style="float:left; padding:5px 0px 5px 5px; width:322px; border-bottom:1px solid #ccc;">
                            	<div style="float:left; width:200px; margin-right:20px; font-size:15px; text-align:right;">Tax:</div>
                                <div style="float:left; width:82px; margin-right:20px; color:#444; font-size:15px; font-weight:bold; text-align:right;"><span id="CAtax">$0.00</span></div>
                                <div id="CAtaxMsg" style="padding-top:2px; width:95%; float:left; font-size:11px; text-align:right; color:#666;"></div>
							</div>
							<% if prepStr(mySrToken) = "" or eligibleCnt = 0 then %>
                            <div style="float:left; padding:5px 0px 5px 5px; width:322px; border-bottom:1px solid #ccc;">
                            	<div style="float:left; width:200px; margin-right:20px; font-size:15px; text-align:right;">Shipping:</div>
                                <div style="float:left; width:82px; margin-right:20px; color:#444; font-size:15px; font-weight:bold; text-align:right;"><span id="shipcost"><%=formatCurrency(shipcost)%></span></div>
							</div>
							<% else %>
                            <div style="float:left; padding:5px 0px 5px 5px; width:322px; border-bottom:1px solid #ccc; text-align:right;">
								<div style="float:right; padding:5px 20px 0px 0px; " name="sr_cartSummaryDiv"></div>
                            </div>
                            <% end if %>
                            <div style="float:left; padding:5px 0px 5px 5px; width:322px;">
                            	<div style="float:left; width:200px; margin-right:20px; color:#ff6600; font-size:15px; font-weight:bold; text-align:right;">Grand Total:</div>
                                <div id="GrandTotal" style="float:left; width:82px; margin-right:20px; color:#ff6600; font-size:15px; font-weight:bold; text-align:right;"><%=formatCurrency(nAmount)%></div>
							</div>
                        </div>
                    </div>
                </div>
                <!--// step2 -->
                <!-- step3 -->
				<div style="float:left; width:100%;">
	                <div style="width:100%; float:left; padding:25px 0px 10px 0px;"><span id="div_step3" style="color:#ff6600; font-size:18px; font-weight:bold;">Step 3: </span><span style="font-size:18px; color:#333;">Enter Payment Information</span></div>
                    <div class="cartWrapper" style="padding:10px 0px 20px 0px; display:inline-block;">
                        <div style="padding:10px; display:inline-block; float:left;">
                        	<div style="float:left; width:30px; padding-top:5px;"><!--<input type="radio" name="PaymentType" value="cc" checked="checked" />--></div>
                            <div style="float:left;"><img src="/images/cart/we_cards.gif" border="0" /></div>
						</div>
                        <div style="width:100%; float:left;">
                        	<div class="cartPaymentLeft">
                            	<div class="cartLeftRow">
                                	<div class="cartLeftText2">Card Type:</div>
                                	<div class="cartLeftTextField2">
                                        <select style="width:185px;" name="cc_cardType">
                                            <option value=""></option>
                                            <option value="VISA">VISA</option>
                                            <option value="MC">MASTER CARD</option>
                                            <option value="AMEX">AMERICAN EXPRESS</option>
                                            <option value="DISC">DISCOVER</option>
                                        </select>
                                    </div>
                                </div>
                            	<div class="cartLeftRow">
                                	<div class="cartLeftText2">Cardholder Name:</div>
                                	<div class="cartLeftTextField2"><input name="cc_cardOwner" type="text" style="width:180px;"></div>
                                </div>
                            	<div class="cartLeftRow">
                                	<div class="cartLeftText2">Credit Card #:</div>
                                	<div class="cartLeftTextField2"><input name="cardNum" type="text" id="cardNum" style="width:180px;" maxlength="16"></div>
                                </div>
                            	<div class="cartLeftRow" style="padding-top:2px;">
                                	<div class="cartLeftText2">&nbsp;</div>
                                	<div class="cartLeftTextField2" style="color:#444; font-size:13px; font-style:italic;">
                                    	No spaces/dashes<br />Example: 1234123412341234
                                    </div>
                                </div>
                            	<div class="cartLeftRow">
                                	<div class="cartLeftText2">Security Code:</div>
                                	<div class="cartLeftTextField2">
										<input name="secCode" type="text" id="secCode" size="4" maxlength="4">&nbsp;&nbsp;<a href="/cart/cvv2help.asp" style="font-size:11px; font-weight:bold; color:#ff6600;" target="_blank">What's this?</a>
									</div>
                                </div>
                            	<div class="cartLeftRow">
                                	<div class="cartLeftText2">Expiration Date:</div>
                                	<div class="cartLeftTextField2">
                                        <select name="cc_month">
                                            <option value="01">01 (January)</option>
                                            <option value="02">02 (February)</option>
                                            <option value="03">03 (March)</option>
                                            <option value="04">04 (April)</option>
                                            <option value="05">05 (May)</option>
                                            <option value="06">06 (June)</option>
                                            <option value="07">07 (July)</option>
                                            <option value="08">08 (August)</option>
                                            <option value="09">09 (September)</option>
                                            <option value="10">10 (October)</option>
                                            <option value="11">11 (November)</option>
                                            <option value="12">12 (December)</option>
                                        </select>
                                        <select name="cc_year">
                                            <%
                                            for countYear = 0 to 14
                                                response.write "<option value=""" & right(cStr(year(date) + countYear),2) & """>" & cStr(year(date) + countYear) & "</option>" & vbcrlf
                                            next
                                            %>
                                        </select>
									</div>
                                </div>
                            </div>
                            <div class="cartPaymentMiddle" style="display:none;"><img src="/images/cart/or_bar_250.jpg" border="0" /></div>
                            <div class="cartPaymentRight" style="display:none;">
                            	<div style="width:100%; float:left;">
                                	<div style="float:left; width:30px; text-align:left; padding-top:10px;"><!--<input type="radio" name="PaymentType" value="eb" />--></div>
                                	<div style="float:left; width:319px; text-align:left; color:#555; font-size:11px;">
										<img src="/images/WU/WU-Pay-wutag-small.jpg" border="0" width="227" height="44" /><br />
										WU&reg; Pay allows you to pay cash online. No financial information required. Simply pay using your bank's online bill pay service, or at a Western Union&reg; Agent location near you. 
                                        Just like paying a utility bill! <a href="http://www.westernunion.com/wupay/learn/how-to-use-wupay-video" target="_blank" style="color:#ff6600; font-weight:bold;">Watch our demo</a>.
                                    </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--// step3 -->
				<div style="width:100%; float:left; text-align:center; padding:20px 0px 15px 0px;">
                    <div id="submitBttn1" style="font-weight:bold; font-size:18px; color:#000;"><input type="image" id="purchaseBtn" onclick="return CheckSubmit();" src="/images/cart/place_order_button.jpg" width="272" height="56" alt="Place My Order"></div>
                    <div id="submitBttn2" style="font-weight:bold; font-size:18px; color:#000; display:none;">Please wait while<br />we process your order</div>
                    <input type="hidden" name="submitted" value="submitted">
                    <input type="hidden" name="PaymentType" value="cc">
                </div>
            </div>
        </td>
    </tr>
</table>
<!--WE variables-->
<%=WEhtml2%>
<!--End WE variables-->
<!--eBillme variables-->
<%=EBtemp%>
<!--End eBillme variables-->
<input type="hidden" name="myBasketXML" value="<%=EBxml%>">
<input type="hidden" name="ssl_ProdIdCount" value="<%=nProdIdCount%>">
<input type="hidden" name="mySrToken" value="<%=mySrToken%>">
<div id="customerInfo" style="display:none;"></div>
</form>

<form name="googleForm" method="post" action="/cart/process/google/CartProcessing.asp">
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <input type="hidden" name="analyticsdata" value="">
    <input type="hidden" name="sWeight" value="<%=cStr(sWeight)%>">
    <%=WEhtml2%>
</form>

<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.asp">
    <input type="hidden" name="numItems" value="<%=nProdIdCount%>">
    <input type="hidden" name="paymentAmount" value="<%=nSubTotal%>">
    <input type="hidden" name="ItemsAndWeight" value="<%=cStr(nProdIdCount) & "|" & cStr(sWeight)%>">
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <%=PPhtml%>
</form>

<div style="clear:both; height:15px;"></div>
<!--#include virtual="/includes/template/bottom_cart.asp"-->

<script language="javascript">
	function getCustomerData(email) {
		trackingVar('email',email);
		ajax('/ajax/customerData.asp?email=' + email,'customerInfo')
		setTimeout("displayData(document.getElementById('customerInfo').innerHTML)",2000)
		ajax('/ajax/remarketing.asp?email=' + email + '&mySession=<%=mySession%>&siteID=0','dumpZone')
	}
	function displayData(dataReturn) {
		if (dataReturn != "no data" && dataReturn != "") {
			trackingBit('accountReturnedByEmail');
			document.getElementById("popUpBG").style.display = ''
			document.getElementById("popUpBox").style.display = ''
			var dataArray = dataReturn.split("@@")
			if (document.frmProcessOrder.fname.value == "") { document.frmProcessOrder.fname.value = dataArray[0] }
			if (document.frmProcessOrder.lname.value == "") { document.frmProcessOrder.lname.value = dataArray[1] }
			if (document.frmProcessOrder.sAddress1.value == "") { document.frmProcessOrder.sAddress1.value = dataArray[2] }
			if (document.frmProcessOrder.sAddress2.value == "") { document.frmProcessOrder.sAddress2.value = dataArray[3] }
			if (document.frmProcessOrder.sCity.value == "") { document.frmProcessOrder.sCity.value = dataArray[4] }
			if (document.frmProcessOrder.sState.selectedIndex == 0) {
				for (i=0;i<50;i++) {
					if (document.frmProcessOrder.sState.options[i].value == dataArray[5]) {
						document.frmProcessOrder.sState.selectedIndex = i
						changeTax(dataArray[5])
					}
				}
			}
			if (document.frmProcessOrder.sZip.value == "") {
				document.frmProcessOrder.sZip.value = dataArray[6]
				getNewShipping();
			}
			if (document.frmProcessOrder.phoneNumber.value == "") { document.frmProcessOrder.phoneNumber.value = dataArray[7] }
			
			if (dataArray[2] != dataArray[8] || dataArray[6] != dataArray[12]) {
				document.frmProcessOrder.sameaddress.checked = true
				showBillingInfo()
				document.frmProcessOrder.bAddress1.value = dataArray[8]
				document.frmProcessOrder.bAddress2.value = dataArray[9]
				document.frmProcessOrder.bCity.value = dataArray[10]
				for (i=0;i<50;i++) {
					if (document.frmProcessOrder.bState.options[i].value == dataArray[11]) {
						document.frmProcessOrder.bState.selectedIndex = i
					}
				}
				document.frmProcessOrder.bZip.value = dataArray[12]
			}
			document.frmProcessOrder.accountid.value = dataArray[13]
			document.frmProcessOrder.parentAcctID.value = dataArray[14]
		}
	}
	
	function newAcctInfo() {
		document.frmProcessOrder.accountid.value = ''
	}
	function newAcctInfoTrack(y) {
		document.frmProcessOrder.accountid.value = ''
		//alert('{object: ' + y.name + ',' + y.value + '}');
		if (y.name == 'phoneNumber') {
			trackingVar('phone',y.value);
		}
	}
</script>
<script language="JavaScript">
	function popUp(url, winName, h, w, l, t) {
		var args = 'height=' + h + ',width=' + w + ',left=' + l + ',top=' + t + ',toolbar=1,location=1,directories=0,status=1,menubar=1,scrollbars=1,resizable=1';
		window.open(url, winName, args);
	}
</script>