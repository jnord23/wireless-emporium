<%
'option explicit
pageName = "basket"
noCommentBox = true
response.buffer = true
Response.Expires = 0
Response.Expiresabsolute = Now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

'session.Abandon()
dim cart : cart = 1
dim oneTime : oneTime = False

if instr(request.ServerVariables("SERVER_NAME"),"staging.") < 1 then
	'if request.ServerVariables("HTTPS") = "off" then response.Redirect("https://www.wirelessemporium.com/cart/basket")
end if

varientTopBanner = 3
varientTopNav = 0
varientTopNavSearch = 1
varientAssurance = 0

gblShipoutDate = "Ships from our warehouse within 24 hours."
shipoutCutoff = cdate(Date & " 12:00:00 PM")
if now < shipoutCutoff then 
	nextMin = datediff("n", now, shipoutCutoff)
	if cint(nextMin/60) = 0 then
		gblShipoutDate = "Ships today if you order within the next <b>" & cint(nextMin mod 60) & " min</b>"	
	else
		gblShipoutDate = "Ships today if you order within the next <b>" & cint(nextMin/60) & " hrs</b> and <b>" & cint(nextMin mod 60) & " min</b>"
	end if
end if

if instr(request.ServerVariables("SERVER_NAME"),"staging.") > 0 then
	strCheckoutAction = "/cart/checkout"
else
	strCheckoutAction = "https://www.wirelessemporium.com/cart/checkout"
end if

set fs = CreateObject("Scripting.FileSystemObject")
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
dim SEtitle, SEdescription, SEkeywords
SEtitle = ""
SEdescription = ""
SEkeywords = ""

dim pageTitle, strBody, a, resetChkout
pageTitle = ""
strBody = ""
resetChkout = request.QueryString("r")
if isnull(resetChkout) or len(resetChkout) < 1 then resetChkout = 0

Session.Contents.Remove("nvpResArray")
Session.Contents.Remove("token")
Session.Contents.Remove("currencyCodeType")
Session.Contents.Remove("paymentAmount")
Session.Contents.Remove("PaymentType")
Session.Contents.Remove("PayerID")
%>

<!--#include virtual="/includes/template/top_cart.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/basket2/topNav2.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/basket2/dupCheckoutBtn1.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/basket2/itemValueProps1.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/basket2/instockIndicator1.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/basket2/shipOutTime1.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/basket2/bottomValueProps1.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/basket2/shopRunner1.css" />
<%
dim sPromoCode
dim promoCodeUsed
dim promoCodeIsValid

if request.form("promo") <> "" then
	sPromoCode = SQLquote(request.form("promo"))
else
'	sPromoCode = session("promocode")
	sPromoCode = request.Cookies("promocode")
end if
sPromoCode = prepStr(sPromoCode)
promoCodeUsed = sPromoCode
promoCodeIsValid = false
if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	SQL = "SELECT * FROM WE_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	session("errorSQL") = SQL
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		FreeItemPartNumber = RS("FreeItemPartNumber")
		setValue = RS("setValue")
		oneTime = RS("oneTime")
		promoCodeIsValid = true
	else
		promoError = "Promo Code Not Found"
		sPromoCode = ""
	end if
end if

dim nItemTotal, EmptyBasket
htmBasketRows = ""
nItemTotal = 0
EmptyBasket = false

' START basket grab
dim sItemName, sRetailPrice, sItemPrice, sItemPic, nProdIdCheck, nProdQuantity, nProdIdCount, showDelBtn, nProdTotalQuantity, strItemIDList
dim sWeight, strItemCheck, strQty, FreeProductInCart
dim SquaretradeItemName, SquaretradeItemPrice, SquaretradeItemCondition
dim strMusicSkinsItemsCheck, strMusicSkinsQty
strItemIDList = ""
nProdTotalQuantity = 0
sWeight = 0
strItemCheck = ""
strQty = ""
strMusicSkinsItemsCheck = ""
strMusicSkinsQty = ""
FreeProductInCart = 0

customSessionID = prepInt(request.QueryString("csid"))
if customSessionID > 0 then mySession = customSessionID

sql	=	"select	a.id, a.parentid, e.alwaysinstock, c.brandname, d.modelname" & vbcrlf & _
		"	,	(select top 1 inv_qty from we_items where partnumber = b.partnumber order by master desc, inv_qty desc) as inv_qty" & vbcrlf & _
		"	,	a.musicskins, a.customcost, a.lockqty, a.itemid, a.qty, b.modelid, b.typeid, b.partnumber" & vbcrlf & _
		"	,	b.itemdesc, b.itempic, '' as defaultpic, b.price_our, b.price_retail, b.condition, b.itemweight, b.nodiscount, b.typeid parentTypeID" & vbcrlf & _
		"from	shoppingcart a left join we_items b " & vbcrlf & _
		"	on	a.itemid=b.itemid and a.musicskins = 0 left outer join we_brands c " & vbcrlf & _
		"	on	b.brandid = c.brandid left outer join we_models d " & vbcrlf & _
		"	on	b.modelid = d.modelid left outer join we_pndetails e " & vbcrlf & _
		"	on	b.partnumber = e.partnumber" & vbcrlf & _
		"where	a.store = 0 and a.sessionid = '" & mySession & "' and (a.purchasedorderid is null or a.purchasedorderid = 0) " & vbcrlf & _
		"union " & vbcrlf & _
		"select	a.id, a.parentid, cast('true' as bit) as alwaysinstock, c.brandname, d.modelname, 100 as inv_qty" & vbcrlf & _
		"	,	a.musicskins, a.customcost, a.lockqty, a.itemid, a.qty, b.modelid, 3 as typeid, b.musicskinsid as partnumber" & vbcrlf & _
		"	,	isnull(c.brandname, '') + ' ' + isnull(d.modelname, '') + ' ' + isnull(b.artist, '') + ' ' + isnull(b.designname, '') as itemdesc" & vbcrlf & _
		"	,	b.image as itempic, b.defaultimg as defaultpic, b.price_we as price_our, b.msrp as price_retail, 0 as condition, 1 as itemweight, 0 as nodiscount, 18 parentTypeID" & vbcrlf & _
		"from	shoppingcart a join we_items_musicskins b " & vbcrlf & _
		"	on	a.itemid=b.id and a.musicskins = 1 left outer join we_brands c " & vbcrlf & _
		"	on	b.brandid = c.brandid left outer join we_models d " & vbcrlf & _
		"	on	b.modelid = d.modelid" & vbcrlf & _
		"where	a.store = 0 and a.sessionid = '" & mySession & "' and (a.purchasedorderid is null or a.purchasedorderid = 0)" & vbcrlf & _
		"order by b.partnumber desc, a.id"
'		response.write "<pre>" & sql & "</pre>"
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

dim goodProducts : goodProducts = ""
dim badProducts : badProducts = ""
dim plateCnt : plateCnt = 0
do until RS.eof
	parentID = prepInt(rs("parentID"))
	parentTypeID = prepInt(rs("parentTypeID"))
	if isnull(RS("alwaysInStock")) then alwaysInStock = False else alwaysInStock = RS("alwaysInStock")
	if RS("itemID") = addItem_ID and rs("customCost") = addItem_cost then
		itemAdded = 1
	end if
	
	nProdIdCheck = RS("itemID")
	nProdQuantity = RS("qty")
	nProdTotalQuantity = nProdTotalQuantity + RS("qty")
	if strItemIDList = "" then
		strItemIDList = RS("itemID")
	else
		strItemIDList = strItemIDList & "," & RS("itemID")
	end if

	if RS("musicSkins") then
		strMusicSkinsItemsCheck = strMusicSkinsItemsCheck & cStr(nProdIdCheck) & ","
		strMusicSkinsQty = strMusicSkinsQty & cStr(nProdQuantity)& "," 
	else
		strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
		strQty = strQty & cStr(nProdQuantity)& ","  			' QTY string for each item, this is for buySafe Use	
	end if
	
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		useBrandName = RS("brandName")
		useModelName = RS("modelName")
		partNumber = RS("partNumber")
		sItemName = insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName)
		musicSkinsDefaultImg = ""
		
		if RS("musicSkins") then
			if isnull(RS("itemPic")) then
				sItemPic = RS("defaultPic")
				musicSkinsDefaultImg = RS("defaultPic")
			else
				sItemPic = RS("itemPic")
			end if
			if instr(sItemPic,";") > 0 then
				musicSkinsDefaultImg = mid(sItemPic,instr(sItemPic,";")+1)
				sItemPic = left(sItemPic,instr(sItemPic,";")-1)
			end if		
		else
			sItemPic = RS("itemPic")
		end if
		
		sItemPrice = RS("price_Our")
		if not isnull(rs("customCost")) then sItemPrice = rs("customCost")
		sRetailPrice = RS("price_retail")
		'Free Bluetooth
		if FreeBluetooth = 1 then
			if cStr(nProdIdCheck) = FreeBluetoothItemID then
				FreeBluetoothPrice = sItemPrice
			end if
		end if
		'Free Product With Phone Purchase
		if RS("typeID") = 16 then
			PhonePurchased = 1
			SquaretradeItemName = sItemName
			SquaretradeItemPrice = sItemPrice
			SquaretradeItemCondition = RS("Condition")
		end if
		'Category Sale
		if CategorySale = 1 then
			if RS("typeID") = CategorySaleTypeID and RS("NoDiscount") = 0 then
				CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
			else
				SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
				SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_Our > 0)"
				SQL = SQL & " AND (R.typeid = '" & CategorySaleTypeID & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
				SQL = SQL & " AND I.NoDiscount = 0"
				dim RSrelated
				set RSrelated = Server.CreateObject("ADODB.Recordset")
				RSrelated.open SQL, oConn, 0, 1
				if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				RSrelated.close
				set RSrelated = nothing
			end if
		end if
		
		if inStr(RS("PartNumber"),"scr-") > 0 then FreeProductInCart = 1
		if RS("itemID") = addItem_ID then
			showDelBtn = addItem_showDeleteBtn
			addItem_lockQty = rs("lockQty")
		end if
		
		if rs("musicSkins") then musicSkins = 1 else musicSkins = 0

		if rs("inv_qty") > 0 or alwaysInStock then
			nItemTotal = nItemTotal + (prepInt(sItemPrice) * prepInt(nProdQuantity))
			sWeight = sWeight + (cDbl(RS("itemWeight")) * nProdQuantity)
			goodProducts = goodProducts & nProdIdCheck & "##" & sItemName & "##" & sItemPrice & "##" & cStr(nProdQuantity) & "##" & sRetailPrice & "##" & sItemPic & "##" & rs("lockQty") & "##" & showDelBtn & "##" & musicSkins & "##" & musicSkinsDefaultImg & "##" & partnumber & "##" & parentID & "##" & parentTypeID & "@@"
		else
			sql = "delete from ShoppingCart where itemID = " & nProdIdCheck & " and (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
			session("errorSQL") = sql
'			oConn.execute SQL
			badProducts = badProducts & nProdIdCheck & "##" & sItemName & "##" & sItemPrice & "##" & cStr(nProdQuantity) & "##" & sRetailPrice & "##" & sItemPic & "##" & rs("lockQty") & "##" & showDelBtn & "##" & musicSkins & "##" & musicSkinsDefaultImg & "##" & partnumber & "##" & parentID & "##" & parentTypeID & "@@"
		end if
	end if
	
	RS.movenext
loop
RS.close
set RS = nothing

if nProdTotalQuantity = 1 then displayItemCnt = nProdTotalQuantity & " Item" else displayItemCnt = nProdTotalQuantity & " Items"

if addItem_ID > 0 then
	if nItemTotal > addItem_mustSpend then
		'freeSkin
		if itemAdded = 0 then
			if isnull(myAccount) or len(myAccount) < 1 then myAccount = 0
			if addItem_lockQty then addItem_lockQty = 1 else addItem_lockQty = 0
			sql = "insert into ShoppingCart(store,sessionID,accountID,itemID,qty,customCost,lockQty,ipAddress) values(0," & mySession & "," & myAccount & "," & addItem_ID & "," & addItem_qty & "," & addItem_cost & "," & addItem_lockQty & ",'" & request.ServerVariables("REMOTE_ADDR") & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
			
			response.Redirect("/cart/basket")
		end if
	elseif itemAdded = 1 then
		if isnull(myAccount) or len(myAccount) < 1 then
			sql = "delete from ShoppingCart where purchasedOrderID is null and store = 0 and sessionID = " & mySession
		else
			sql = "delete from ShoppingCart where purchasedOrderID is null and store = 0 and (sessionID = " & mySession & " or accountID = " & myAccount & ")"
		end if
		response.Write("nItemTotal:" & nItemTotal & "<br>addItem_mustSpend:" & addItem_mustSpend & "<br>" & sql)
		response.End()
		session("errorSQL") = sql
		oConn.execute(sql)
		
		response.Redirect("/cart/basket")
	end if
end if
' END basket grab

dim nSubTotal
nSubTotal = nItemTotal

if sWeight / 16 < 1 then
	sWeight = 1
else
	sWeight = sWeight / 16
end if

if strItemCheck <> "" then strItemCheck = left(strItemCheck,len(strItemCheck)-1)
if strMusicSkinsItemsCheck <> "" then strMusicSkinsItemsCheck = left(strMusicSkinsItemsCheck,len(strMusicSkinsItemsCheck)-1)

if nProdIdCount = 0 then EmptyBasket = true
%>
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<script>
window.WEDATA.pageType = "cart";
window.WEDATA.pageData = {
	cartItems: [],
	promoCode: <%= jsStr(promoCodeUsed) %>,
	promoIsValid: <%= jsStr(LCase(promoCodeIsValid)) %>,
	couponId: <%= jsStr(couponId) %>,
	subTotal: <%= jsStr(nSubtotal) %>,
	weight: <%= jsStr(sWeight) %>
};
</script>

<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
    <%
    if len(session("userMsg")) > 0 and not isnull(session("userMsg")) then
    %>
    <tr bgcolor="#f0f0f0"><td align="center" style="font-weight:bold; color:#C30; font-size:14px;"><%=session("userMsg")%></td></tr>
    <%
        session("userMsg") = null
    end if
    %>
    <% if resetChkout = 1 then %>
    <tr bgcolor="#f0f0f0"><td align="center" style="font-weight:bold; color:#C30; font-size:14px;">Your Checkout Process Has Been Reset - Please Try Again</td></tr>
    <% end if %>
    <tr>
        <td align="center" bgcolor="#FFFFFF">
            <div style="width:100%; float:left; border-bottom:1px solid #ccc;">
                <div style="font-size:30px; float:left; margin-right:10px; color:#333;">Your Cart Summary</div>
                <div style="float:left; padding-top:8px;"><img src="/images/checkout/shopping-cart-icon.png" border="0" /></div>
                <div style="float:right; color:#ccc; font-size:16px; padding:2px 0px 0px 5px;">Receipt</div>
                <div style="float:right; padding-left:15px;"><img src="/images/checkout/circle-3-off.png" border="0" /></div>
                <div style="float:right; color:#ccc; font-size:16px; padding:2px 0px 0px 5px;">Secure Checkout</div>
                <div style="float:right; padding-left:15px;"><img src="/images/checkout/circle-2-off.png" border="0" /></div>
                <div style="float:right; font-weight:bold; font-size:16px; padding:2px 0px 0px 5px;">Cart Summary</div>
                <div style="float:right;"><img src="/images/checkout/circle-1-on.png" border="0" /></div>                
            </div>
            <div style="width:100%; float:left; margin:10px 0px 10px 0px;">
            	<div class="left" style="text-align:left; font-size:18px; padding-top:15px;">You have <span style="color:#ff6600; font-style:italic; font-weight:bold;"><%=displayItemCnt%></span> in your shopping cart.</div>
               	<%if not EmptyBasket then%>                
					<%if emphasizePayPal = 1 then%>
                    <div style="float:right; padding:10px 0px 0px 0px;">
                        <div style="float:right;"><a onclick="document.PaypalForm.submit()" style="cursor:pointer;"><img src="/images/checkout/paypal_cta_checkout2.png" border="0" /></a></div>
                    </div>
                    <%else%>
                    <div id="mvt_btnCheckout2" class="right" style="padding-top:5px;">
                        <form name="frmCheckout" method="post" action="<%=strCheckoutAction%>">
                            <input type="image" src="/images/checkout/cta.png" border="0" />
                        </form>
                    </div>
					<%end if%>
                <%end if%>
            </div>
            <div style="width:100%; float:left;">
                <div class="cartReviewHeader">
                    <div style="width:480px; float:left; padding:7px 0px 0px 20px; text-align:left; font-size:16px; font-weight:bold;">Description & Availability</div>
                    <div style="width:100px; float:left; padding:7px 0px 0px 0px; text-align:center; font-size:16px; font-weight:bold;">Unit Price</div>
                    <div style="width:80px; float:left; padding-top:7px; text-align:center; font-size:16px; font-weight:bold;">Qty.</div>
                    <div style="width:100px; float:left; padding:7px 20px 0px 0px; text-align:right; font-size:16px; font-weight:bold;">Total Price</div>
                </div>
            </div>
            <!-- basket rows -->
            <div style="width:798px; float:left; border-left:1px solid #ccc; border-right:1px solid #ccc;">
            <%
            if discountTotal > 0 then nItemTotal = nItemTotal - discountTotal
            bgColor = "#fff"
            prodList_good = split(goodProducts,"@@")
            itemTotal = 0
            nProdIdCount = 0
			dim baseItems : baseItems = ""
			dim activeProductCnt : activeProductCnt = 0
			dim eligibleCnt : eligibleCnt = 0
			dim noShopRunner : noShopRunner = 0
			dim baseCaseList : baseCaseList = ""
			dim csidLink
            for i = 0 to (ubound(prodList_good)-1)
				activeProductCnt = activeProductCnt + 1
                curProdArray = split(prodList_good(i),"##")
				
				itemID = curProdArray(0)
				partNumber = curProdArray(10)
				usePrice = curProdArray(2)
				qty = curProdArray(3)
				parentTypeID = prepInt(curProdArray(12))
				
				if instr(baseItems,itemID & "@@") > 0 then
					baseArray = split(baseItems,"##")
					baseCnt = 0
					for x = 0 to ubound(baseArray)
						if prepStr(baseArray(x)) <> "" then
							detailArray = split(baseArray(x),"@@")
							if detailArray(0) = itemID then qty = qty - detailArray(1)
						end if
					next
				end if
				
				if not isDropShip(partNumber) and left(partNumber,3) <> "WCD" then eligibleCnt = eligibleCnt + 1
				csidLink = ""
				if customSessionID > 0 then csidLink = "?csid=" & customSessionID
				if qty > 0 then
					prodLink = ""
                    if curProdArray(8) = 1 then
                        'Is a Music Skins Product
						if fs.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsSmall") & "\" & curProdArray(5)) then
							'Has a small product picture
							ImagePath = "/productpics/musicSkins/musicSkinsSmall/" & curProdArray(5)
						elseif fs.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs") & "\" & curProdArray(9)) then
							'Had a default thumb picture
							ImagePath = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & curProdArray(9)
						else
							ImagePath = "/productpics/icon/imagena.jpg"
						end if
						prodLink = "/p-ms-" & itemID & "-" & formatSEO(curProdArray(1))
					else
						ImagePath = "/productpics/icon/" & curProdArray(5)
						prodLink = "/p-" & itemID & "-" & formatSEO(curProdArray(1))
					end if
	            %>
				<form name="frmEditDelete<%=itemID%>" action="/cart/item_edit<%=csidLink%>" method="post" style="margin:0px;">
                <input type="hidden" name="fromPage" value="/cart/basket">
                <input type="hidden" name="prodid" value="<%=itemID%>">
                <input type="hidden" name="pagetype" value="edit">
				<div style="float:left; width:798px; padding:15px 0px 10px 0px; border-bottom:1px solid #ccc;">
                    <div style="width:500px; float:left; text-align:left;">
                    <%if instr(partNumber,"WCD-") > 0 then%>
                        <div style="float:left; width:110px; padding:10px; border:1px solid #ccc; margin:0px 10px 0px 10px; border-radius:5px;">
                        	<div style="float:left; width:45px;"><a href="<%=prodLink%>"><img src="<%=ImagePath%>" border="0" /></a></div>
							<%
                            pnArray = split(partNumber,"-")
                            baseCaseID = pnArray(1)
                            if instr(baseCaseList,baseCaseID) > 0 then
                                tempVar = mid(baseCaseList,instr(baseCaseList,baseCaseID))
                                tempVar = replace(mid(tempVar,instr(tempVar,"@@")+2),"##","")
                                curQty = prepInt(tempVar)
                                baseCaseList = replace(baseCaseList,baseCaseID & "@@" & curQty,baseCaseID & "@@" & (curQty + qty))
                                curQty = curQty + qty
                            else
                                baseCaseList = baseCaseList & baseCaseID & "@@" & qty & "##"
                            end if
                            
                            sql = "select itemPic, price_our, itemDesc from we_Items where itemID = " & baseCaseID
                            session("errorSQL") = sql
							call fOpenConn() 'Has to be re-opened because connection is being closed somewhere (2012-10-01)
                            set baseCaseRS = oConn.execute(sql)
                            bcPic = baseCaseRS("itemPic")
                            bcPrice = baseCaseRS("price_our")
                            bcDesc = baseCaseRS("itemDesc")
                            usePrice = usePrice + bcPrice
                            baseItems = baseItems & baseCaseID & "@@" & qty & "##"
                            %>
	                        <div style="float:left; width:20px; font-weight:bold; font-size:14px;">+</div>
	                        <div style="float:left; width:45px;"><a href="/p-<%=baseCaseID%>-<%=formatSEO(bcDesc)%>"><img src="/productpics/icon/<%=bcPic%>" border="0" width="45" height="45" /></a></div>
						</div>
                        <div style="float:left; width:335px; padding-right:13px;">
	                    	<div style="float:left; width:100%;"><a href="<%=prodLink%>" style="color:#026697; font-size:12px; font-weight:bold;"><%=curProdArray(1)%></a> (<%=prepInt(curProdArray(2)) * prepInt(qty)%>)</div>
	                        <div style="float:left; width:100%;"><a href="/p-<%=baseCaseID%>-<%=formatSEO(bcDesc)%>" style="color:#026697; font-size:12px; font-weight:bold;"><%=bcDesc%></a> (<%=prepInt(qty) * prepInt(bcPrice)%>)</div>
	                        <div class="mvt_valueProbs" style="float:left; width:100%; padding-top:10px;">
								<%=writeValueProps(parentTypeID)%>
							</div>
                            <div class="mvt_stockIndicator" style="float:left; width:100%; padding-top:10px;">
                                <div style="float:left; width:15px; padding-top:2px;"><img src="/images/checkout/green-checkmark.png" border="0" /></div>
                                <div style="float:left; text-align:left; font-size:12px; font-weight:bold; color:#ff6600;">This item is in stock!</div>
                            </div>
                            <div class="mvt_shipoutDate" style="float:left; font-size:12px; color:#66a844; padding:3px 0px 0px 3px;"> &nbsp; &nbsp; 
								Please allow <%=gblCustomShippingDate%> business days for production time.
							</div>
						</div>
                    <%else%>
                        <div style="float:left; width:45px; padding:10px; border:1px solid #ccc; margin:0px 10px 0px 10px; border-radius:5px;"><a href="<%=prodLink%>"><img src="<%=ImagePath%>" border="0" width="45" height="45" /></a></div>
                        <div style="float:left; width:400px; padding-right:13px;">
                        	<div style="float:left; width:100%;"><a href="<%=prodLink%>" style="color:#026697; font-size:12px; font-weight:bold;"><%=curProdArray(1)%></a></div>
                        	<div class="mvt_valueProbs" style="float:left; width:100%; padding-top:10px;"><%=writeValueProps(parentTypeID)%></div>
                            <div class="mvt_stockIndicator" style="float:left; width:100%; padding-top:10px;">
                                <div style="float:left; width:15px; padding-top:2px;"><img src="/images/checkout/green-checkmark.png" border="0" /></div>
                                <div style="float:left; text-align:left; font-size:12px; font-weight:bold; color:#ff6600;">This item is in stock!</div>
                            </div>
                            <div class="mvt_shipoutDate" style="float:left; font-size:12px; color:#66a844; padding:3px 0px 0px 3px;"> &nbsp; &nbsp; <%=gblShipoutDate%></div>
						</div>
                    <%end if%>
                    </div>
                    <div style="width:100px; float:left; text-align:center; padding:7px 00px 0px 0px; font-size:15px; font-weight:bold; color:#444;">
                        <%=formatCurrency(prepInt(usePrice))%>
                    </div>
                    <div style="float:left; width:80px; text-align:left;">
						<%if curProdArray(6) then 'lock the qty%>
		                    <div style="float:left; width:100%; text-align:center; padding-top:5px;"><%=qty%></div>
	                        <input type="hidden" name="qty" value="<%=qty%>" />
	                        <%if curProdArray(7) then 'allow the delete button%>
                                <div style="float:left; width:100%; text-align:center; padding-top:5px;">
									<img src="/images/checkout/remove-button.png" border="0" style="cursor:pointer;" onClick="br_cartTracking('remove', '<%=itemID%>', '', '<%=replace(curProdArray(1), "'", "\'")%>', ''); EditDeleteItem('frmEditDelete<%=itemID%>','delete');" />
                                </div>
                            <%end if%>
                        <%else%>
		                    <div style="float:left; width:100%; text-align:center;">
								<input type="text" name="qty" size="2" maxlength="3" value="<%=qty%>" style="background:url(/images/checkout/qty-field.png) no-repeat; width:65px; height:30px; text-align:center; border:none;" onClick="this.select();" />
	                        </div>
		                    <div style="float:left; width:100%; text-align:center; padding-top:7px;">
	                            <input type="image" src="/images/checkout/update-button.png" onClick="EditDeleteItem(this.form.name,'edit');">
	                        </div>
		                    <div style="float:left; width:100%; text-align:center; padding-top:7px;">
								<img src="/images/checkout/remove-button.png" border="0" style="cursor:pointer;" onClick="br_cartTracking('remove', '<%=itemID%>', '', '<%=replace(curProdArray(1), "'", "\'")%>', ''); EditDeleteItem('frmEditDelete<%=itemID%>','delete')" />
	                        </div>
                        <%end if%>
                    </div>
                    <div style="width:98px; float:left; padding:7px 20px 0px 0px; text-align:right; font-size:15px; font-weight:bold; color:#444;">
                        <%=formatCurrency(usePrice * qty)%>
                    </div>
                </div>
                </form>
	            <%
                end if
				
				if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
                itemTotal = itemTotal + (usePrice * qty)
                nProdIdCount = nProdIdCount + 1
                'google product details
                WEhtml2 = WEhtml2 & "<input type=""hidden"" name=""itemID_" & nProdIdCount & """ value=""" & itemID & """><input type=""hidden"" name=""itemQty_" & nProdIdCount & """ value=""" & curProdArray(3) & """>" & vbcrlf
                'paypal product details
                PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & itemID & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & curProdArray(1) & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & curProdArray(3) & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & curProdArray(2) & """>" & vbcrlf
				%>
				<script>
            		window.WEDATA.pageData.cartItems.push({
                		itemId: <%= jsStr(itemId) %>,
                		qty: <%= jsStr(curProdArray(3)) %>,
                		price: <%= jsStr(curProdArray(2)) %>
            		});
            	</script>
                <%				
            next
			%>
            </div>
            <!-- // basket rows -->
           
            <%
			if badProducts <> "" then %>
            <!-- basket rows for out of stock -->
            <div style="width:798px; float:left; border-left:1px solid #ccc; border-right:1px solid #ccc;">
            <%
            prodList_bad = split(badProducts,"@@")
			session("errorSQL") = badProducts
			for i = 0 to (ubound(prodList_bad)-1)
                curProdArray = split(prodList_bad(i),"##")
				itemID = curProdArray(0)
				partNumber = curProdArray(10)
				usePrice = curProdArray(2)
				qty = curProdArray(3)
				parentTypeID = prepInt(curProdArray(12))

				prodLink = ""
				if curProdArray(8) = 1 then
					'Is a Music Skins Product
					if fs.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsSmall") & "\" & curProdArray(5)) then
						'Has a small product picture
						ImagePath = "/productpics/musicSkins/musicSkinsSmall/" & curProdArray(5)
					elseif fs.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs") & "\" & curProdArray(9)) then
						'Had a default thumb picture
						ImagePath = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & curProdArray(9)
					else
						ImagePath = "/productpics/icon/imagena.jpg"
					end if
					prodLink = "/p-ms-" & itemID & "-" & formatSEO(curProdArray(1))
				else
					ImagePath = "/productpics/icon/" & curProdArray(5)
					prodLink = "/p-" & itemID & "-" & formatSEO(curProdArray(1))
				end if
	            %>
				<div style="float:left; width:798px; padding:15px 0px 10px 0px; border-bottom:1px solid #ccc;">
                    <div style="width:500px; float:left; text-align:left;">
                    <%if instr(partNumber,"WCD-") > 0 then%>
                        <div style="float:left; width:110px; padding:10px; border:1px solid #ccc; margin:0px 10px 0px 10px; border-radius:5px;">
                        	<div style="float:left; width:45px;"><a href="<%=prodLink%>"><img src="<%=ImagePath%>" /></a></div>
							<%
                            pnArray = split(partNumber,"-")
                            baseCaseID = pnArray(1)
                            if instr(baseCaseList,baseCaseID) > 0 then
                                tempVar = mid(baseCaseList,instr(baseCaseList,baseCaseID))
                                tempVar = replace(mid(tempVar,instr(tempVar,"@@")+2),"##","")
                                curQty = prepInt(tempVar)
                                baseCaseList = replace(baseCaseList,baseCaseID & "@@" & curQty,baseCaseID & "@@" & (curQty + qty))
                                curQty = curQty + qty
                            else
                                baseCaseList = baseCaseList & baseCaseID & "@@" & qty & "##"
                            end if
                            
                            sql = "select itemPic, price_our, itemDesc from we_Items where itemID = " & baseCaseID
                            session("errorSQL") = sql
							call fOpenConn()
                            set baseCaseRS = oConn.execute(sql)
                            bcPic = baseCaseRS("itemPic")
                            bcPrice = baseCaseRS("price_our")
                            bcDesc = baseCaseRS("itemDesc")
                            usePrice = usePrice + bcPrice
                            baseItems = baseItems & baseCaseID & "@@" & qty & "##"
                            %>
	                        <div style="float:left; width:20px; font-weight:bold; font-size:14px;">+</div>
	                        <div style="float:left; width:45px;"><a href="/p-<%=baseCaseID%>-<%=formatSEO(bcDesc)%>"><img src="/productpics/icon/<%=bcPic%>" border="0" width="45" height="45" /></a></div>
						</div>
                        <div style="float:left; width:335px; padding-right:13px;">
	                    	<div style="float:left; width:100%;"><a href="<%=prodLink%>" style="color:#026697; font-size:12px; font-weight:bold;"><%=curProdArray(1)%></a> (<%=prepInt(curProdArray(2)) * prepInt(qty)%>)</div>
	                        <div style="float:left; width:100%;"><a href="/p-<%=baseCaseID%>-<%=formatSEO(bcDesc)%>" style="color:#026697; font-size:12px; font-weight:bold;"><%=bcDesc%></a> (<%=prepInt(qty) * prepInt(bcPrice)%>)</div>
	                        <div style="float:left; width:100%; padding-top:10px;"><%=writeValueProps(parentTypeID)%></div>
						</div>
                    <%else%>
                        <div style="float:left; width:45px; padding:10px; border:1px solid #ccc; margin:0px 10px 0px 10px; border-radius:5px;"><a href="<%=prodLink%>"><img src="<%=ImagePath%>" width="45" height="45" /></a></div>
                        <div style="float:left; width:400px; padding-right:13px;">
                        	<div style="float:left; width:100%;"><a href="<%=prodLink%>" style="color:#026697; font-size:12px; font-weight:bold;"><%=curProdArray(1)%></a></div>
                        	<div style="float:left; width:100%; padding-top:10px;"><%=writeValueProps(parentTypeID)%></div>
						</div>
                    <%end if%>
                    </div>
                    <div style="width:298px; float:left; text-align:center; color:#f00; padding-top:25px; font-size:14px;">
						Temporarily Out Of Stock
                    </div>
                </div>
			<%
            next
			%>
            </div>
			<!--// basket rows for out of stock -->
            <%end if%>
            
            <%
            finalTotal = itemTotal
            if discountTotal > itemTotal then discountTotal = itemTotal
			
			if not EmptyBasket then
			%>
			<div style="float:left; width:798px; border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc;">
                <div style="float:left; width:447px; padding:10px 0px 0px 20px;">
				<%if sPromoCode = "" then%>
                    <div style="width:100%; float:left; color:#f00; text-align:left;" id="promoError"><%=promoError%></div>
                    <div style="width:100%; float:left; color:#666; text-align:left;" id="promoLink">
                    	<a onclick="promoDisplay()" style="cursor:pointer; font-size:12px; font-family:Arial; color:#999; text-decoration:underline;">Have a coupon code?</a>
					</div>
                    <div style="width:100%; float:left; display:none;" id="enterPromo">
                        <form style="margin:0px;" name="promoForm" method="post">
                            <div style="float:left;">
                                <input type="text" name="promo" style="width:100px; color:#999;" onClick="this.value=''" />
                            </div>
                            <div style="float:left; padding:2px 0px 0px 2px; ">
                                <input type="image" src="/images/checkout/orange_d_arrow.gif" />
                            </div>
                        </form>
                    </div>
                    <% 
                    if discountTotal > 0 then
                        if instr(discountTotal,".") > 0 then discountTotal = left(discountTotal,instr(discountTotal,".")+2)
                        nProdIdCount = nProdIdCount + 1
                        PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""DISCOUNT""><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""Promo Discount""><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""1""><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & discountTotal * -1 & """>" & vbcrlf
                        finalTotal = itemTotal - discountTotal
                    end if
				else
'					session("promocode") = sPromoCode
					response.Cookies("promocode") = sPromoCode
					if discountTotal = 0 then
						if isnumeric(promoPercent) then discountTotal = discountAmt * promoPercent
						if discountTotal < 0 then discountTotal = discountTotal * -1
					end if
					if instr(discountTotal,".") > 0 then discountTotal = left(discountTotal,instr(discountTotal,".")+2)
					if discountTotal > 0 then
						nProdIdCount = nProdIdCount + 1
						PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""DISCOUNT""><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & sPromoCode & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""1""><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & discountTotal * -1 & """>" & vbcrlf
					end if
					finalTotal = itemTotal - discountTotal
				end if
				%>
                </div>
                <div style="float:right; width:330px; border-left:1px solid #ccc; background-color:#f7f7f7;">
                    <div style="float:left; padding:5px 0px 5px 5px; width:325px; border-bottom:1px solid #ccc;">
                        <div style="float:left; width:200px; margin-right:20px; font-size:15px; text-align:right;">Subtotal:</div>
                        <div style="float:left; width:85px; margin-right:20px; color:#444; font-size:15px; font-weight:bold; text-align:right;"><%=formatCurrency(itemTotal)%></div>
                    </div>
                    <div style="float:left; padding:5px 0px 5px 5px; width:325px; border-bottom:1px solid #ccc;">
                        <div style="float:left; width:200px; margin-right:20px; font-size:15px; text-align:right;">Discount:</div>
                        <div style="float:left; width:85px; margin-right:20px; font-size:15px; font-weight:bold; text-align:right;">
                        <%if discountTotal > 0 then%>
                            <span id="discountAmt" style="color:#990000;"><%=formatCurrency(discountTotal * -1)%></span>
                        <%else%>
                            <span id="discountAmt" style="color:#444;"><%=formatCurrency(discountTotal * -1)%></span>
                        <%end if%>
                        </div>
                    </div>
                    <div style="float:left; padding:5px 0px 5px 5px; width:325px;">
                        <div style="float:left; width:200px; margin-right:20px; font-size:15px; text-align:right;">Shipping:</div>
                        <div style="float:left; width:85px; margin-right:20px; color:#ff6600; font-size:15px; font-weight:bold; text-align:right;">FREE!</div>
                    </div>
                </div>
            </div>
            <div style="float:left; width:100%; background-color:#333; padding:3px 0px 3px 0px;">
                <div id="siteTop-OrderNowPhone" style="margin:2px 0px 0px 10px;"></div>
                <div id="siteTop-Text" style="padding:6px 20px 0px 5px; text-decoration:none;">1-800-305-1106</div>
                <div class="left">
                    <script type="text/javascript">
                        var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                        var lhnid = 3410;
                        var lhnwindow = '';
                        var lhnImage = "<div id='siteTop-Livechat' style='margin-top:2px; '></div>";
                        document.write(unescape("%3Cscript src='/framework/utility/liveHelp.js' type='text/javascript'%3E%3C/script%3E"));
                    </script>                        
                </div>
                <div class="left">
                    <script type="text/javascript">
                        var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                        var lhnid = 3410;
                        var lhnwindow = '';
                        var lhnImage = "<div id='siteTop-Text' style='padding:6px 0px 0px 5px; font-size:12px;'>Live Chat</div>";
                        document.write(unescape("%3Cscript src='/framework/utility/liveHelp.js' type='text/javascript'%3E%3C/script%3E"));
                    </script>
                </div>
            	<div style="float:right; width:330px; padding-top:3px;">
					<div style="float:left; width:200px; margin-right:20px; color:#fff; font-size:16px; text-align:right;">Item Total:</div>
					<div id="GrandTotal" style="float:left; width:85px; margin-right:20px; color:#fff; font-size:16px; text-align:right;"><%=formatCurrency(finalTotal)%></div>
                </div>
            </div>
			<div style="float:left; width:100%; border-bottom:1px solid #ccc; padding:15px 0px 25px 0px;">
				<div id="mvt_bottomValueProps" style="float:left; border:1px solid #ccc; border-radius:5px; padding:10px 5px 10px 5px; width:330px; margin:20px 0px 0px 100px;">
                	<div style="float:left; width:100%;">
					    <div style="float:left; width:30px; text-align:center; padding-top:2px;"><img src="/images/checkout/green-checkmark.png" border="0" /></div>
                        <div style="float:left; color:#444; font-size:14px; font-weight:bold;">Over 2,000,000 Orders Shipped!</div>
                    </div>
                	<div style="float:left; width:100%; padding-top:7px;">
					    <div style="float:left;width:30px; text-align:center; padding-top:2px;"><img src="/images/checkout/green-checkmark.png" border="0" /></div>
                        <div style="float:left; color:#444; font-size:14px; font-weight:bold;">All Orders Ship Within 24 Hours</div>
                    </div>
                	<div style="float:left; width:100%; padding-top:7px;">
					    <div style="float:left; width:30px; text-align:center; padding-top:2px;"><img src="/images/checkout/green-checkmark.png" border="0" /></div>
                        <div style="float:left; color:#444; font-size:14px; font-weight:bold;">90-Day EZ No-Hassle Return Policy</div>
                    </div>
                	<div style="float:left; width:100%; padding-top:7px;">
					    <div style="float:left; width:30px; text-align:center; padding-top:2px;"><img src="/images/checkout/green-checkmark.png" border="0" /></div>
                        <div style="float:left; color:#444; font-size:14px; font-weight:bold;">Secure 128-Bit SSL Encrypted Checkout</div>
                    </div>
                </div>
                <%if emphasizePayPal = 1 then%>
            	<div style="float:right; padding:10px 0px 0px 0px;">
                    <div style="float:right;"><a onclick="document.PaypalForm.submit()" style="cursor:pointer;"><img src="/images/checkout/paypal_cta_checkout2.png" border="0" /></a></div>
					<%if not oneTime then %>
                    <div style="clear:both; color:#555; font-size:16px; font-weight:bold; text-align:center; padding:10px;">
                    	- Or checkout with -
                    </div>
                    <div style="clear:both; text-align:center;">
                        <div>
						<form name="frmCheckout" method="post" action="<%=strCheckoutAction%>">
                            <input type="image" src="/images/checkout/basket_cc_checkout.png" border="0" />
                        </form>
                        </div>
                    </div>
                    <% end if %>
                </div>
                <%else%>
            	<div style="float:right; padding:10px 0px 0px 0px;">
                    <div style="float:right;">
                        <form name="frmCheckout" method="post" action="<%=strCheckoutAction%>">
                            <input type="image" src="/images/checkout/cta.png" border="0" />
                        </form>
                    </div>
					<%if not oneTime then %>
                    <div style="clear:both; color:#444; font-size:18px; font-weight:bold; text-align:center; padding:10px;">
                    	- or -
                    </div>
                    <div style="clear:both; text-align:center;">
                        <div><a onclick="document.PaypalForm.submit()" style="cursor:pointer;"><img src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" border="0" /></a></div>
                    </div>
                    <% end if %>
                </div>
                <%end if%>
            </div>
            <%end if%>
			<div style="float:left; width:100%;">
            	<div style="float:left; padding-top:20px;">
                	<a href="/" style="font-weight:bold; color:#ff6600; font-size:13px;">&laquo; Browse more items</a>
                </div>
            	<div style="float:right;">
                    <div style="float:right; padding:18px 0px 0px 10px;">
                        <a href="/privacy" target="_blank"><img src="/images/checkout/truste2.png" border="0" /></a>
                    </div>
                    <div style="float:right; padding:5px 0px 0px 5px;" title="Click to Verify - This site chose VeriSign Trust Seal to promote trust online with consumers">
                        <a href="javascript:Open_Popup('https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.WIRELESSEMPORIUM.COM&lang=en','VeriSign','width=560,height=500,location=yes,status=yes,resizable=yes,scrollbars=yes');">
                            <div id="verisignLogo"></div>
                        </a>
                    </div>
                    <div style="float:right; padding:15px 0px 10px 0px;">
                    	<!-- START SCANALERT CODE -->
                        <a target="_blank" href="https://www.mcafeesecure.com/RatingVerify?ref=www.wirelessemporium.com"><img width="65" height="37" border="0" src="https://images.scanalert.com/meter/www.wirelessemporium.com/63.gif" alt="McAfee SECURE sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="alert('Copying Prohibited by Law - McAfee SECURE is a Trademark of McAfee, Inc.'); return false;"></a>
                        <!-- END SCANALERT CODE --> 
                    </div>
                </div>
            </div>
			<div style="float:left; width:100%; padding-top:15px; color:#333; font-size:16px; font-weight:bold; text-align:left;">
            	Have questions about your shopping cart? We're here to help. Call us toll free 1-800-305-1106.
            </div>
			<%if eligibleCnt > 0 and noShopRunner = 0 then %>
			<div id="mvt_shoprunner" style="float:left; width:100%; padding-top:20px; text-align:left;">
				<div style="float:left; text-align:left;" name="sr_shippingSummaryDiv"></div>
            </div>
			<%end if%>
			<div style="float:left; width:100%; padding-top:15px; color:#333; text-align:left;">
            	*Cart ID: <%=mySession%>
            </div>
        </td>
    </tr>
</table>
<form name="googleForm" method="post" action="/cart/process/google/CartProcessing">
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <input type="hidden" name="analyticsdata" value="">
    <input type="hidden" name="sWeight" value="<%=cStr(sWeight)%>">
    <%=WEhtml2%>
</form>
<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder">
    <input type="hidden" name="numItems" value="<%=nProdIdCount%>">
    <input type="hidden" name="paymentAmount" value="<%=formatNumber(finalTotal,2)%>">
    <input type="hidden" name="ItemsAndWeight" value="<%=cStr(nProdIdCount) & "|" & cStr(sWeight)%>">
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <%=PPhtml%>
</form>
<%
function writeValueProps(parentTypeID)
	ret = ""
	strValueProbs = getCatProps(parentTypeID)
	if strValueProbs <> "" then
		arrValueProbs = split(strValueProbs, "##")
		for nCnt=0 to ubound(arrValueProbs)
			ret = ret & "<div style=""float:left; width:100%;""><div style=""float:left; width:15px; padding-top:5px;""><img src=""/images/dot_gray.jpg"" border=""0"" /></div><div style=""float:left; width:90%; color:#666;"">" & arrValueProbs(nCnt) & "</div></div>"
		next
	end if
	writeValueProps = ret
end function

function getCatProps(parentTypeID)
	ret = ""
	select case parentTypeID
		case 1
			ret = "Replace your old, worn battery.##Long lasting & provides hours of use time.##Guaranteed to work w/ your phone."
		case 2
			ret = "Ultimate convenience & value.##Guaranteed compatible with your phone.##High quality materials ensure your purchase will last."
		case 3
			ret = "Premium protection for your phone.##The perfect stylish accessory.##Protects against falls and scratches!"
		case 5
			ret = "Premium voice technology.##Hours of talk time."
		case 6
			ret = "Superior protection and grip.##Fully-functional and adjustable."
		case 7
			ret = "Carry your phone in style.##A premium case at the perfect price.##A safe and secure fit."
		case 15, 23
			ret = "Stay connected!##The very best in cellular technology."
		case 18
			ret = "Protects against scratches!##Bubble-free protection.##Premium surface protection."
	end select
	getCatProps = ret
end function

if baseCaseList <> "" then
	baseCaseArray = split(baseCaseList,"##")
	for i = 0 to ubound(baseCaseArray)
		if prepStr(baseCaseArray(i)) <> "" then
			cureCaseArray = split(baseCaseArray(i),"@@")
			baseCaseID = cureCaseArray(0)
			qty = cureCaseArray(1)
			
			sql = "select sum(qty) as qty from shoppingCart where itemID = " & baseCaseID & " and sessionID = " & mySession
			session("errorSQL") = sql
			set preRS = oConn.execute(sql)
			if not preRS.EOF then preAmt = prepInt(preRS("qty")) else preAmt = 0
			preRS = null
			
			sql =	"if (" &_
						"select count(*) " &_
						"from shoppingCart " &_
						"where itemID = " & baseCaseID & " and sessionID = " & mySession &_
					") > 0 " &_
					"update shoppingCart set qty = " & qty & " where itemID = " & baseCaseID & " and sessionID = " & mySession & " " &_
					"else " &_
					"insert into shoppingCart (store,sessionID,itemID,qty,ipAddress) values(0," & mySession & "," & baseCaseID & "," & qty & ",'" & request.ServerVariables("REMOTE_ADDR") & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
			
			sql = "select sum(qty) as qty from shoppingCart where itemID = " & baseCaseID & " and sessionID = " & mySession
			session("errorSQL") = sql
			set postRS = oConn.execute(sql)
			if not postRS.EOF then postAmt = prepInt(postRS("qty")) else postAmt = 0
			postRS = null
			
			if postAmt <> preAmt then response.Redirect("/cart/basket")
		end if
	next
end if
response.Write("<!-- sid:" & mySession & " -->")
%>
<!--#include virtual="/includes/template/bottom_cart.asp"-->

<script language="javascript">
/*
function reDirect() {
	window.location = "http://<%=request.servervariables("SERVER_NAME")%>/index";
}
*/

function EditDeleteItem(nForm,thisAction) {
	var frmName = eval('document.' + nForm);
	if (isNaN(frmName.qty.value)) {
		alert("Please enter a valid number for Quantity.");
	} else {
		if (thisAction == 'delete') {
			frmName.action = "/cart/item_delete<%=csidLink%>";
		} else {
			frmName.action = "/cart/item_edit<%=csidLink%>";
		}
		frmName.pagetype.value = thisAction;
		frmName.submit();
	}
}

function promoDisplay() {
	document.getElementById("promoError").style.display = 'none'
	document.getElementById("enterPromo").style.display = ''
}
</script>