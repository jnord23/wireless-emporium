<%
noLeftNav = 1
noCommentBox = true
pageTitle = "File not Found ?Error 404"

'Soft 404
Response.Clear
Response.Status = "200 OK"
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
saveBrandID = prepInt(request.Cookies("saveBrandID"))
saveModelID = prepInt(request.Cookies("saveModelID"))
saveItemID = prepInt(request.Cookies("saveItemID"))

sql = "exec sp_recommendedItemsForUserCookie 0," & saveBrandID & "," & saveModelID & "," & saveItemID & ",7"
'response.write sql
'response.end
session("errorSQL") = sql
set recommendRS = oConn.execute(sql)

sql = 	"select distinct top 7 itemid, itemdesc, itempic, price_retail, price_our from we_items " & vbcrlf & _
		"where 	itemid in (	select 	top 20 itemid " & vbcrlf & _
		"					from 	shoppingcart " & vbcrlf & _
		"					where 	store = 0 " & vbcrlf & _
		"						and itemid not in (	select	cast(configValue as int) itemid " & vbcrlf & _
		"											from	we_config with (nolock)" & vbcrlf & _
		"											where	configName = 'Free ItemID') " & vbcrlf & _
		"					order by id desc )"
session("errorSQL") = sql
set recentRS = oConn.execute(sql)

%>
<link rel="stylesheet" type="text/css" href="/includes/css/product/productBase.css" />
<table border="0" cellspacing="0" cellpadding="0" width="958">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal"><img src="/images/spacer.gif" width="1" height="10"></td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <div class="top-content">
				<a href="/" title="return to homepage"><div class="page-not-found"></div></a>
				<div class="content">
					<div class="header">
						<h1>We're Sorry!</h1>
						<h2>Uh oh! Looks like you've come across a page that does not exist.</h2>
						<h2>Call <span>1 (800) 305-1106</span> to place your order today!</h2>
					</div>
					<div class="links">
						<h2>Or, here are a few links to help you get back on track:</h2>
						<div class="links-container-left">
							<a href="./apple-iphone-accessories">Apple iPhone Accessories</a><br />
							<a href="./apple-iphone-5-accessories">Apple iPhone 5 Accessories</a><br />
							<a href="./apple-iphone-5s-accessories">Apple iPhone 5S Accessories</a><br />
							<a href="./apple-iphone-5c-accessories">Apple iPhone 5C Accessories</a><br />
							<a href="./sb-17-sm-493-sc-3-covers-faceplates-apple-iphone">Apple iPhone Cases</a><br />
							<a href="./sb-17-sm-1412-sc-3-covers-faceplates-apple-iphone-5">Apple iPhone 5 Cases</a><br />
							<a href="./sb-17-sm-1628-sc-3-covers-faceplates-apple-iphone-5s">Apple iPhone 5S Cases</a><br />
						</div>
						<div class="links-container-right">
							<a href="./sb-17-sm-1612-sc-3-covers-faceplates-apple-iphone-5c">Apple iPhone 5C Cases</a><br />
							<a href="./samsung-galaxy-note-3-accessories">Samsung Galaxy Note 3 Accessories</a><br />
							<a href="./samsung-galaxy-s4-accessories">Samsung Galaxy S4 Accessories</a><br />
							<a href="./sb-9-sm-1624-sc-3-covers-faceplates-samsung-galaxy-note-3">Samsung Galaxy Note 3 Cases</a><br />
							<a href="./sb-9-sm-1523-sc-3-covers-faceplates-samsung-galaxy-s4">Samsung Galaxy S4 Cases</a><br />
							<a href="./htc-one-accessories">HTC ONE Accessories</a><br />
							<a href="./sb-20-sm-1512-sc-3-covers-faceplates-htc-one">HTC ONE Cases</a><br />
						</div>
					</div>
				</div>
			</div>
			<div class="tb titleBar">
				<div class="fl titleIconRecommend"></div>
				<div class="fl titleMainBar" style="width:878px;">We Recommend</div>
				<div class="fl titleRight"></div>
			</div>
			<div class="section">
				<div class="items">
					<%
					recProdCnt = 0
					do while not recommendRS.EOF
						recProdCnt = recProdCnt + 1
						arrItem = split(recommendRS("itemDesc"), "##")
						itemid = arrItem(0)
						itemDesc = arrItem(1)
					%>
					<div class="item">
						<div class="image"><a href="/p-<%=itemid%>-<%=formatSEO(itemDesc)%>" title="<%=itemDesc%>"><img src="/productpics/thumb/<%=recommendRS("itemPic")%>" align="center" alt="<%=recommendRS("itemDesc")%>" /></a></div>
						<div class="name"><a href="/p-<%=itemid%>-<%=formatSEO(itemDesc)%>" title="<%=itemDesc%>"><%=itemDesc%></a></div>
						<div class="prices">
							<span class="regular-price">Reg. <%=formatCurrency(recommendRS("price_retail"),2)%></span>
							<br />
							<span class="price"><%=formatCurrency(recommendRS("price_our"),2)%></span>
						</div>
					</div>
					<%
						recommendRS.movenext
					loop
					%>
				</div>
			</div>
			<div class="tb titleBar">
				<div class="fl titleIconRecent"></div>
				<div class="fl titleMainBar" style="width:878px;">Customers Have Recently Viewed</div>
				<div class="fl titleRight"></div>
			</div>
			<div class="section">
				<div class="items">
					<%
					recProdCnt = 0
					do while not recentRS.EOF
						recProdCnt = recProdCnt + 1
					%>
					<div class="item">
						<div class="image"><a href="/p-<%=recentRS("itemID")%>-<%=formatSEO(recentRS("itemDesc"))%>" title="<%=recentRS("itemDesc")%>"><img src="/productpics/thumb/<%=recentRS("itemPic")%>" align="center" alt="<%=recentRS("itemDesc")%>" /></a></div>
						<div class="name"><a href="/p-<%=recentRS("itemID")%>-<%=formatSEO(recentRS("itemDesc"))%>" title="<%=recentRS("itemDesc")%>"><%=recentRS("itemDesc")%></a></div>
						<div class="prices">
							<span class="regular-price">Reg. <%=formatCurrency(recentRS("price_retail"),2)%></span>
							<br />
							<span class="price"><%=formatCurrency(recentRS("price_our"),2)%></span>
						</div>
					</div>
					<%
						recentRS.movenext
					loop
					%>
				</div>
			</div>
			<!--
			<p>
                Sorry, but the page you are looking for no longer exists.
            </p>
            <p>
                At WirelessEmporium.com, we are continually updating our vast inventory, so the product you are searching for may be unavailable at this time, or its name may have changed.
            </p>
            <p>
                Please use our convenient browse or search features to quickly find the product you need.
            </p>
            <p>
                If you still cannot find what you are looking for, please feel free to e-mail us at
                <a href="mailto:service@WirelessEmporium.com">service@WirelessEmporium.com</a>
            </p>
			-->
        </td>
    </tr>
</table>
<p>&nbsp;</p>
<%
Const lngMaxFormBytes = 200

Dim objASPError, blnErrorWritten, strServername, strServerIP, strRemoteIP
Dim strMethod, lngPos, datNow, strQueryString, strURL
Dim strBody

Set objASPError = Server.GetLastError

Dim MissingFile
MissingFile = replace(Request.ServerVariables("QUERY_STRING"),"404;","")

strBody = "<br><br>" & vbcrlf
strBody = strBody & "<ul>" & vbcrlf
strBody = strBody & "<li>MISSING FILE:<br>" & vbcrlf
strBody = strBody & MissingFile & vbcrlf
strBody = strBody & "<br><br></li>" & vbcrlf
strBody = strBody & "<li>Browser Type:<br>" & vbcrlf
strBody = strBody & Server.HTMLEncode(Request.ServerVariables("HTTP_USER_AGENT")) & vbcrlf
strBody = strBody & "<br><br></li>" & vbcrlf
strBody = strBody & "<li>Page:<br>" & vbcrlf

strMethod = Request.ServerVariables("REQUEST_METHOD")
strBody = strBody & strMethod & " " & vbcrlf
If strMethod = "POST" Then strBody = strBody & Request.TotalBytes & " bytes to " & vbcrlf
strBody = strBody & Request.ServerVariables("SCRIPT_NAME") & vbcrlf
strBody = strBody & "</li>" & vbcrlf
If strMethod = "POST" Then
	strBody = strBody & "<p><li>POST Data:<br>" & vbcrlf
	' On Error in case Request.BinaryRead was executed in the page that triggered the error.
	On Error Resume Next
	If Request.TotalBytes > lngMaxFormBytes Then
		strBody = strBody & Server.HTMLEncode(Left(Request.Form, lngMaxFormBytes)) & " . . ." & vbcrlf
	Else
		strBody = strBody & Server.HTMLEncode(Request.Form) & vbcrlf
	End If
	On Error Goto 0
	strBody = strBody & "</li>" & vbcrlf
End If

strBody = strBody & "<br><br></li>" & vbcrlf
strBody = strBody & "<li>Time:<br>" & vbcrlf

datNow = Now()
strBody = strBody & Server.HTMLEncode(FormatDateTime(datNow, 1) & ", " & FormatDateTime(datNow, 3))

SERVER_NAME = Request.ServerVariables("SERVER_NAME")
SCRIPT_NAME = Request.ServerVariables("SCRIPT_NAME")
QUERY_STRING = Request.ServerVariables("QUERY_STRING")

strBody = strBody & "<br><br></li>" & vbcrlf
strBody = strBody & "</ul>" & vbcrlf
strBody = strBody & "<p>REMOTE_ADDR: " & Request.ServerVariables("REMOTE_ADDR") & "</p>" & vbcrlf
strBody = strBody & "<p>SERVER_NAME: " & Request.ServerVariables("SERVER_NAME") & "</p>" & vbcrlf
strBody = strBody & "<p>SCRIPT_NAME: " & Request.ServerVariables("SCRIPT_NAME") & "</p>" & vbcrlf
strBody = strBody & "<p>&nbsp;</p>" & vbcrlf

strBody = strBody & "<p>LINK:<br><a href='http://" & SERVER_NAME & SCRIPT_NAME & "?" & QUERY_STRING & "'>" & SERVER_NAME & SCRIPT_NAME & "?" & QUERY_STRING & "</a></p>" & vbcrlf
strBody = strBody & "<p>&nbsp;</p>" & vbcrlf
strBody = strBody & "<p>HTTP_REFERRER:<br>" & Request.ServerVariables("HTTP_REFERER") & "</p>" & vbcrlf
strBody = strBody & "<p>&nbsp;</p>" & vbcrlf

dim cdo_to, cdo_from, cdo_subject, cdo_body
cdo_to = "webmaster@wirelessemporium.com,jonathan@wirelessemporium.com"
cdo_from = "service@wirelessemporium.com"
cdo_subject = "404 Problem"
cdo_body = strBody
'CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
%>
<!--#include virtual="/includes/template/bottom.asp"-->