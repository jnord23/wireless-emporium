<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
response.buffer = true

dim productListingPage : productListingPage = 1
noProducts = false
googleAds = 1
leftGoogleAd = 1
nItemDescMaxLength = 65
strTopPhones = "4,5,9,14,17,20"

call fOpenConn()
sql = 	"select	c.brandOrder, a.brandid, a.brandName, c.itemid, c.itemdesc, c.itempic, c.price_our, c.price_retail, c.flag1, c.partnumber" & vbcrlf & _
		"from	we_brands a cross apply" & vbcrlf & _
		"		(	select	top 4 case when b.brandid = 17 then 1" & vbcrlf & _
		"								when b.brandid = 14 then 4" & vbcrlf & _
		"								when b.brandid = 20 then 2 " & vbcrlf & _
		"								when b.brandid = 4 then 6" & vbcrlf & _
		"								when b.brandid = 5 then 5" & vbcrlf & _
		"								when b.brandid = 9 then 3" & vbcrlf & _
		"								else b.brandid + 20" & vbcrlf & _
		"							end brandOrder" & vbcrlf & _
		"				,	b.itemid, b.itemdesc, b.itempic, b.price_our, b.price_retail, b.flag1, b.numberOfSales, b.partnumber" & vbcrlf & _
		"			from	we_items b with (nolock) join we_models m" & vbcrlf & _
		"				on	b.modelid = m.modelid" & vbcrlf & _
		"			where	b.brandid = a.brandid" & vbcrlf & _
		"				and	b.subtypeid = '1020' and b.price_our > 0 and b.hidelive = 0 and b.ghost = 0	" & vbcrlf & _
		"				and	b.typeid = 16" & vbcrlf & _
		"				and	b.brandid in (" & strTopPhones & ")" & vbcrlf & _
		"				and	(select top 1 i.inv_qty from we_items i where i.partnumber = b.partnumber and i.master = 1 order by i.inv_qty desc) > 0" & vbcrlf & _
		"			order by brandOrder, b.numberOfSales desc, b.flag1 desc, b.itemID desc) c" & vbcrlf & _
		"where	a.brandid in (" & strTopPhones & ")" & vbcrlf & _
		"order by c.brandOrder, c.numberOfSales desc, c.flag1 desc, c.itemID desc" & vbcrlf

session("errorSQL") = sql
'response.write "<pre>" & sql & "</pre>"
'response.end
arrRS = getDbRows(sql)

sql = 	"select	distinct a.brandid, a.brandName" & vbcrlf & _
		"from	we_items b with (nolock) join we_brands a" & vbcrlf & _
		"	on	b.brandid = a.brandid" & vbcrlf & _
		"where	b.subtypeid = '1020' and b.price_our > 0 and b.hidelive = 0 and b.ghost = 0	and inv_qty <> 0" & vbcrlf & _
		"	and	b.typeid = 16" & vbcrlf & _
		"	and	b.brandid not in (4,5,9,14,17,20,22)" & vbcrlf

session("errorSQL") = sql
arrRS2 = getDbRows(sql)

sql	=	"select	id, case when id=12 then carrierName + ' Phones' else carrierName end carrierName " & vbcrlf & _
		"from	we_carriers" & vbcrlf & _
		"where	id in (2,5,6,4,12)" & vbcrlf
session("errorSQL") = sql
arrRS3 = getDbRows(sql)

'=========================================================================================
'Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
'	oParam.CompareMode = vbTextCompare
'	oParam.Add "x_brandID", brandID
'	oParam.Add "x_modelID", modelID
'	oParam.Add "x_categoryID", categoryID
'	oParam.Add "x_brandName", brandName
'	oParam.Add "x_modelName", modelName
'	oParam.Add "x_categoryName", categoryName	
'	oParam.Add "x_musicGenreName", musicSkinGenre
'	if "0" <> musicSkinArtistID and len(musicSkinArtistID) > 0 then
'		oParam.Add "x_musicArtistName", "artist-" & musicSkinArtistID & "-" & musicSkinArtistName
'	end if
'call redirectURL("bmc", "", request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
'=========================================================================================

dim strH1: strH1 = "Unlocked Cell Phones"
dim strH2: strH2 = "Wireless Emporium Now Offers a Great Selection of Discount Unlocked Phones!"
bottomText = "<p>Buy cheap unlocked cell phones with no contract! Get the latest unlocked GSM cell phones and many others including the <a class=""topText-link"" href=""http://wirelessemporium.com/p-31694-blackberry-storm-9530-3g-smartphone--unlocked-.asp"" title=""BlackBerry Storm"">BlackBerry Storm</a>. Wireless Emporium, the leading name in cell phone accessories, also offers the largest selection of no contract cell phones online because we know that you may not want to be bound by multi-year contracts. Of the hundreds of phone models we offer, most are unlocked cell phones that are ready to be used ??no contract, no commitment, and no activation required.</p><p>The appeal of using the cell phone you want without being bound to long-term contracts is enough reason for many people to pay more for the cell phone they want. But when it comes to accessories for both locked and unlocked cell phones, cost isn&#39;t something you have to budge on. Wireless Emporium carries the largest selection of cell phone accessories available anywhere, so you&#39;re sure to find the cell phone covers and cell phone faceplates you&#39;ve been looking for. If cell phone cases aren&#39;t your thing, we&#39;re sure keeping your cell phone powered is. We offer a complete line of <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-batteries.asp"" title=""Cell Phone Batteries"">cell phone batteries</a> and <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-batteries.asp"" title=""Cell Phone Chargers"">cell phone chargers</a> for nearly all cell phone models!</p><p>Since 2001, <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Cell Phone Accessories"">Wireless Emporium</a> has been offering high-quality cell phone accessories to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best cell phone accessories at the best value. Shipping and superior customer service is free with every order and offered every day!</p>"

SEtitle = "Save on Unlocked Cell Phones: iPhone, Blackberry, HTC, Samsung & Motorola"
SEdescription = "Shop for Unlocked Cell Phones like the iPhone, Blackberry, HTC, Samsung, Motorola, LG & other reputed brands at WirelessEmporium ??the online Cell Phone Store."
SEkeywords = "unlocked cell phones, unlocked iphones, unlocked blackberry, unlocked htc, unlocked samsung, unlocked Motorola"

%>
<!--#include virtual="/includes/template/top.asp"-->
<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
			<a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;
			<a class="breadcrumb" href="/phones-tablets">Cell Phones & Tablets</a>&nbsp;&gt;&nbsp;
			Unlocked Cell Phones
        </td>
    </tr>
    <tr>
        <td width="100%">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
            	<tr>
                	<td align="left">
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
                        	<tr><td style="border-bottom:1px solid #ccc;" width="545" align="left"><h1 class="brand"><%=strH1%></h1></td></tr>
                            <tr><td align="left"><h2 class="brand"><%=strH2%></h2></td></tr>
                            <tr><td><img src="/images/unlockedphones/unlocked-cell-phone-top.jpg" border="0" alt="<%=strH1%>" title="<%=strH1%>" /></td></tr>
                        </table>
                    </td>
                    <td width="203px" align="right"><img src="/images/unlockedphones/unlocked-cell-phone-logo.jpg" border="0" alt="<%=strH1%>" title="<%=strH1%>" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
            <div style="float:left;">
            	<table width="726px" border="0" cellspacing="0" cellpadding="0" class="graybox-middle" style="border-left:1px solid #999; border-right:1px solid #999; padding:0px 10px 0px 10px;">
                	<tr>
                    	<td align="left" style="font-size:14px;"><b>Locked & Unlocked Cell Phones</b>
                        </td>
                    	<td align="right">
	                        <div class="graybox-inner" style="display:inline;">Show:</div>
							<form name="frmSearch" method="post" style="display:inline;">
                                <select name="cbItem" onChange="if(this.value != ''){window.location=this.value;}">
                                    <option value="">Choose from the following</option>
                                    <option value="" style="font-size:15px; font-style:italic;"> --------- Shop By Carrier --------- </option>
                                    <%
									for nRows=0 to ubound(arrRS3, 2)
									%>
                                    <option value="/car-<%=arrRS3(0,nRows)%>-phones-<%=formatSEO(arrRS3(1,nRows))%>"> &nbsp; <%=arrRS3(1,nRows)%></option>
									<%
									next
									%>
                                    <option value="" style="font-size:15px; font-style:italic;"> --------- Shop By Brand --------- </option>
                                    <%
									strBrands = ""
									for nRows=0 to ubound(arrRS, 2)
										if instr(strBrands, arrRS(2,nRows)) <= 0 then
											strBrands = strBrands & "," & arrRS(2,nRows)
									%>
                                    <option value="/sb-<%=arrRS(1,nRows)%>-sm-0-scd-1020-cell-phones-<%=formatSEO(arrRS(2,nRows))%>"> &nbsp; <%=arrRS(2,nRows)%></option>
									<%
										end if
									next
									for nRows=0 to ubound(arrRS2, 2)
									%>
                                    <option value="/sb-<%=arrRS2(0,nRows)%>-sm-0-scd-1020-cell-phones-<%=formatSEO(arrRS2(1,nRows))%>"> &nbsp; <%=arrRS2(1,nRows)%></option>
									<%
									next
									%>
                                </select>
                            </form>                        
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" valign="top">
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                	<td width="100%">
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
							<%
                            lap = 0
                            set fsThumb = CreateObject("Scripting.FileSystemObject")
							'======== unlocked cell phones only
							sql = 	"	select	top 5 b.itemid, b.itemdesc, b.itempic, b.price_our, b.price_retail, b.flag1, b.partnumber" & vbcrlf & _
									"		,	m.ReleaseYear, m.ReleaseQuarter" & vbcrlf & _
									"	from	we_items b with (nolock) join we_models m" & vbcrlf & _
									"		on	b.modelid = m.modelid" & vbcrlf & _
									"	where	b.subtypeid = '1020' and b.price_our > 0 and b.hidelive = 0 and b.ghost = 0	" & vbcrlf & _
									"		and	b.typeid = 16" & vbcrlf & _
									"		--and	b.brandid in (4,5,9,14,17,20)" & vbcrlf & _
									"		and	b.carrierid = 12" & vbcrlf & _
									"		and	b.itemdesc like '%unlocked%'" & vbcrlf & _
									"		and	( select top 1 inv_qty from we_items i where i.partnumber = b.partnumber and i.master = 1 order by inv_qty desc) > 0" & vbcrlf & _
									"	order by b.numberOfSales desc, b.flag1 desc, b.itemID desc" & vbcrlf
'							response.write "<pre>" & sql & "</pre>"
							session("errorSQL") = sql
							unlockedRS = getDbRows(sql)		
							if not isnull(unlockedRS) then
								for nRows=0 to ubound(unlockedRS,2)
									lap = lap + 1
									itemid = unlockedRS(0,nRows)
									itemdesc = replace(unlockedRS(1,nRows), "/", " / ")
									altText = itemdesc
									price_our = formatcurrency(cdbl(unlockedRS(3,nRows)))
									price_retail = formatcurrency(cdbl(unlockedRS(4,nRows)))
									brandName = "Unlocked"
				
									if len(itemdesc) > nItemDescMaxLength then
										nextSpace = inStr(right(itemdesc, len(itemdesc) - nItemDescMaxLength), " ")
										itemdesc = left(itemdesc, nItemDescMaxLength + nextSpace) & "..."
									end if
				
									'====================================================================== find appropriate product images
									itemPic = unlockedRS(2,nRows)
									itemImgPath = Server.MapPath("/productpics/thumb") & "\" & itemPic
									itemImgPath2 = Server.MapPath("/productpics/thumb") & "\" & itemPic
									useImgPath = "/productpics/thumb/" & itemPic
									
									if not fsThumb.FileExists(itemImgPath) then
										itemPic = "imagena.jpg"
										useImgPath = "/productpics/thumb/imagena.jpg"
									end if		
									'====================================================================== find appropriate product images
									
									productLink = "/p-" & itemid & "-" & formatSEO(unlockedRS(1,nRows))
									viewMoreLink = "/car-12-phones-unlocked-cell-phones"
									viewMoreAltText = " Unlocked Cell Phones"
									
									strBorder = ""
									if lap = 1 then
										strBorder = "border-left:1px solid #ccc;"
									%>
                                    <tr>
                                        <td colspan="5" style="border-bottom:1px solid #ccc; padding-top:20px;">
                                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td><a href="<%=viewMoreLink%>" title="<%=brandName & " Cell Phones"%>"><img src="/images/icons/categoryOrangeArrow.jpg" border="0" alt="<%=brandName & " Cell Phones"%>"/></a></td>
                                                    <td height="30" align="left" valign="top">
	                                                    <a href="<%=viewMoreLink%>" title="<%=brandName & " Cell Phones"%>">
                                                        <div id="subcatHeader-middle" style="width:716px;" title="<%=brandName & " Cell Phones"%>">
                                                            <div style="padding:6px 6px 0px 10px; float:left;"><h2 class="bmc"><%=ucase(brandName & " Cell Phones")%></h2></div>
                                                            <div style="padding-top:11px;"><img src="/images/icons/categoryWhiteArrow.gif" border="0" /></div>
                                                        </div>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
									<%
									elseif lap = 5 then
										strBorder = "border-right:1px solid #ccc;"
									end if
									%>
									<td style="width:144px; height:235px; padding:15px 5px 5px 5px; <%=strBorder%>" align="center" valign="top">
										<div style="float:left; width:129px; height:215px;" align="center">
											<div style="width:129px;" align="center"><a href="<%=productLink%>" title="<%=altText%>"><img src="<%=useImgPath%>" border="0" alt="<%=altText%>" title="<%=altText%>" width="100" height="100" /></a></div>
											<div style="width:129px; height:58px; padding-top:10px;" align="left"><a href="<%=productLink%>" class="cellphone-link" title="<%=altText%>"><%=itemdesc%></a></div>
											<div style="width:129px; padding-top:5px;" align="left" class="index-price-retail">Was <del><%=price_retail%></del></div>
											<div style="width:129px;" align="left" class="index-price-our">Our Price <%=price_our%></div>
											<div style="width:129px;" align="left"><b>Save <%=formatPercent((price_Retail - price_Our) / price_Retail,0)%></b></div>                                    
										</div>
										<% if lap < 5 then %>
										<div style="float:left; width:5px; height:215px;"><div id="bigborder-div"></div></div>                                    
										<% end if %>
									</td>
									<%
									
									if lap >= 5 then
										lap = 0
										response.write "</tr><tr><td colspan=""5"" style=""border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc; padding-right:15px;"" align=""right""><a href=""" & viewMoreLink & """ style=""text-decoration:none;"" title=""" & viewMoreAltText & """><img src=""/images/viewmoreproducts.jpg"" border=""0"" alt=""" & viewMoreAltText & """ title=""" & viewMoreAltText & """/></a></td></tr><tr>" & vbcrlf
									end if
								next
							end if
							%>
							</tr>                            
                            <%
							previousBrandID = 0
							lap = 0
							if not isnull(arrRS) then
								for nRows=0 to ubound(arrRS,2)
									lap = lap + 1
									itemid = arrRS(3,nRows)
									itemdesc = replace(arrRs(4,nRows), "/", " / ")
									altText = itemdesc
									price_our = formatcurrency(cdbl(arrRS(6,nRows)))
									price_retail = formatcurrency(cdbl(arrRS(7,nRows)))
									brandName = arrRS(2, nRows)
									brandid = arrRS(1, nRows)

									if len(itemdesc) > nItemDescMaxLength then
										nextSpace = inStr(right(itemdesc, len(itemdesc) - nItemDescMaxLength), " ")
										itemdesc = left(itemdesc, nItemDescMaxLength + nextSpace) & "..."
									end if
				
									'====================================================================== find appropriate product images
									itemPic = arrRS(5,nRows)
									itemImgPath = Server.MapPath("/productpics/thumb") & "\" & itemPic
									itemImgPath2 = Server.MapPath("/productpics/thumb") & "\" & itemPic
									useImgPath = "/productpics/thumb/" & itemPic
									
									if not fsThumb.FileExists(itemImgPath) then
										itemPic = "imagena.jpg"
										useImgPath = "/productpics/thumb/imagena.jpg"
									end if		
									'====================================================================== find appropriate product images
									
									productLink = "/p-" & itemid & "-" & formatSEO(arrRs(4,nRows))
									'viewMoreLink = "cp-sb-" & brandid & "-" & formatSEO(brandName) & "-unlocked-cell-phones"
									viewMoreLink = "sb-" & brandid & "-sm-0-scd-1020-cell-phones-" & formatSEO(brandName)
									viewMoreAltText = brandname & " Unlocked Cell Phones"
									
									strBorder = ""
									if previousBrandID <> brandid then
										if previousBrandID <> 0 then
											for i=4 to lap step -1
											%>
										<td style="width:144px; height:235px; padding:15px 5px 5px 5px; <%if i=lap then%>border-right:1px solid #ccc;<%end if%>" align="center" valign="top">&nbsp;</td>
                                            <%
											next
									%>
                                    </tr>
                                    <tr>
                                    	<td colspan="5" style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc; padding-right:15px;" align="right">
                                        	<a href="<%=previousViewMoreLink%>" style="text-decoration:none;" title="<%=viewMoreAltText%>"><img src="/images/viewmoreproducts.jpg" border="0" alt="<%=viewMoreAltText%>" title="<%=viewMoreAltText%>"/></a>
										</td>
									</tr>
										<%
										end if
										
										lap = 1
										%>
                                    <tr>
                                        <td colspan="5" style="border-bottom:1px solid #ccc; padding-top:20px;">
                                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td><a href="<%=viewMoreLink%>" title="<%=brandName & " Cell Phones"%>"><img src="/images/icons/categoryOrangeArrow.jpg" border="0" alt="<%=brandName & " Cell Phones"%>"/></a></td>
                                                    <td height="30" align="left" valign="top">
	                                                    <a href="<%=viewMoreLink%>" title="<%=brandName & " Cell Phones"%>">
                                                        <div id="subcatHeader-middle" style="width:716px;" title="<%=brandName & " Cell Phones"%>">
                                                            <div style="padding:6px 6px 0px 10px; float:left;"><h2 class="bmc"><%=ucase(brandName & " Cell Phones")%></h2></div>
                                                            <div style="padding-top:11px;"><img src="/images/icons/categoryWhiteArrow.gif" border="0" /></div>
                                                        </div>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:144px; height:235px; padding:15px 5px 5px 5px; border-left:1px solid #ccc;" align="center">
                                            <div style="float:left; width:129px; height:145px; padding-top:70px;" align="center"><a href="<%=viewMoreLink%>" style="text-decoration:none;" title="<%=viewMoreAltText%>"><img src="/images/unlockedphones/brand/<%=arrRS(1,nRows)%>.jpg" border="0" alt="<%=viewMoreAltText%>" title="<%=viewMoreAltText%>" /></a></div>
                                            <div style="float:left; width:5px; height:215px;"><div id="bigborder-div"></div></div>
                                        </td>
									<%
									elseif lap = 4 then
										strBorder = "border-right:1px solid #ccc;"
									end if
									%>
									<td style="width:144px; height:235px; padding:15px 5px 5px 5px; <%=strBorder%>" align="center" valign="top">
										<div style="float:left; width:129px; height:215px;" align="center">
											<div style="width:129px;" align="center"><a href="<%=productLink%>" title="<%=altText%>"><img src="<%=useImgPath%>" border="0" alt="<%=altText%>" title="<%=altText%>" width="100" height="100" /></a></div>
											<div style="width:129px; height:58px; padding-top:10px;" align="left"><a href="<%=productLink%>" class="cellphone-link" title="<%=altText%>"><%=itemdesc%></a></div>
											<div style="width:129px; padding-top:5px;" align="left" class="index-price-retail">Was <del><%=price_retail%></del></div>
											<div style="width:129px;" align="left" class="index-price-our">Our Price <%=price_our%></div>
											<div style="width:129px;" align="left"><b>Save <%=formatPercent((price_Retail - price_Our) / price_Retail,0)%></b></div>                                    
										</div>
										<% if lap < 4 then %>
										<div style="float:left; width:5px; height:215px;"><div id="bigborder-div"></div></div>                                    
										<% end if %>
									</td>
									<%
									previousBrandID = brandid
									previousViewMoreLink = viewMoreLink
								next
								%>
                                </tr>
                                <tr>
                                    <td colspan="5" style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc; padding-right:15px;" align="right">
                                        <a href="<%=viewMoreLink%>" style="text-decoration:none;" title="<%=viewMoreAltText%>"><img src="/images/viewmoreproducts.jpg" border="0" alt="<%=viewMoreAltText%>" title="<%=viewMoreAltText%>"/></a>
                                    </td>
                                </tr>
							<%
                        	end if
							
							if not isnull(arrRS2) then
								%>
                                <tr>
                                    <td colspan="5" style="border-bottom:1px solid #ccc; padding-top:20px;">
										<table border="0" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td><img src="/images/icons/categoryOrangeArrow.jpg" border="0" /></td>
                                                <td height="30" align="left" valign="top">
                                                    <div id="subcatHeader-middle" style="width:716px;" title="<%="other cell phone brands"%>">
                                                        <div style="padding:6px 6px 0px 10px; float:left;"><h2 class="bmc"><%=ucase("other cell phone brands")%></h2></div>
                                                        <div style="padding-top:11px;"><img src="/images/icons/categoryWhiteArrow.gif" border="0" /></div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>                                        
                                    </td>
                                </tr>
                                <tr>                                
                                <%
								lap = 0
								for nRows=0 to ubound(arrRS2,2)
									lap = lap + 1
									brandID = arrRS2(0,nRows)
									brandName = replace(arrRS2(1,nRows), "/", " / ")

									'viewMoreLink = "cp-sb-" & brandID & "-" & formatSEO(brandName) & "-unlocked-cell-phones"
									viewMoreLink = "sb-" & brandid & "-sm-0-scd-1020-cell-phones-" & formatSEO(brandName)
									viewMoreAltText = brandName & " Cell Phones"
									
									strBorder = ""
									if lap = 1 then
										strBorder = "border-left:1px solid #ccc;"
									elseif lap = 5 then
										strBorder = "border-right:1px solid #ccc;"
									end if
									%>
									<td style="width:149px; padding:10px; <%=strBorder%>" align="center" valign="top">
                                    	<table border="0" cellspacing="0" cellpadding="0" width="100%">
                                        	<tr>
                                            	<% if lap < 5 then %>
												<td style="border-right:1px dotted #ccc; padding-right:5px;" align="left">
                                                <% else %>
												<td style="padding-right:5px;" align="left" >
                                                <% end if %>
                                                    <a href="<%=viewMoreLink%>" style="text-decoration:none;" title="<%=viewMoreAltText%>"><img src="/images/unlockedphones/brand/<%=brandID%>.jpg" border="0" title="<%=viewMoreAltText%>" alt="<%=viewMoreAltText%>" /></a><br />
                                                    <a href="<%=viewMoreLink%>" class="cellphone2-link" title="<%=viewMoreAltText%>"><%=viewMoreAltText%></a>
                                                </td>
                                            </tr>
                                        </table>
									</td>
									<%
									
									if lap >= 5 then
										lap = 0
										response.write "</tr><tr><td colspan=""5"" style=""border-bottom:1px solid #ccc; font-size:1px"">&nbsp;</td></tr><tr>"
									end if
								next
								if lap < 5 and lap <> 0 then
									response.write "<td colspan=""" & (5-lap) & """ style=""border-right:1px solid #ccc;"">&nbsp;</td></tr></tr><tr><td colspan=""5"" style=""border-bottom:1px solid #ccc; font-size:1px"">&nbsp;</td></tr><tr>"
								end if
							end if
                            %>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
    <tr><td align="center" class="topText"><%=seTopText%></td></tr>                            
    <tr><td align="center" class="bottomText"><%=seBottomText%></td></tr>
</table>
<%
call fCloseConn()
%>
<!--#include virtual="/includes/template/bottom.asp"-->
