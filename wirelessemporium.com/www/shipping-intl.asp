<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;International Shipping
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle" width="100%">
            <h1>International Shipping</h1>
        </td>
    </tr>
    <tr>
        <td class="normaltext">
            <p>We have partnered with Bongo International Parcel and Mail Forwarding to service our international customers.</p>
            <p>Bongo International provides its customers with their very own US address (not a P.O. Box). Once you have a US address, you will be able to make purchases at our website as well as other US based online retailers. Bongo receives your purchases and logs them into their online system where you can consolidate them with other orders.</p>
            <p>This process saves international customers up to 82% off typical international shipping rates.</p>
            <strong><font face="tahoma" size="3">Step-By-Step Order Guide</font></strong>
            <br><br>
            <ol>
                <li>Shop for your items on our website and add them to your cart.</li>
                <li>Setup your Bongo International account by clicking the Bongo Checkout button.</li>
                <li>After signup, Bongo will provide you with your very own US address.</li>
                <li>Back at our website, proceed to Checkout.</li>
                <li>Enter your new US address supplied by Bongo as both your <strong>BILLING &amp; SHIPPING Address</strong>.</li>
                <li>Use the credit card that you have on file with Bongo as the payment method -- No Exceptions.</li>
                <li>Choose your shipping method and Place Order.</li>
                <li>Once completed, finish your verification process with Bongo.</li>
                <li>
                    Bongo will take over from here, so communicate directly with them for delivery of your order via
                    <a href="http://www.bongous.com/chat.php" title="Online Chat" target="_blank">online chat</a> or <a href="mailto:service@bongous.com?subject=Bongo%20Support%20" title="Email Bongo"> email</a>.
                </li>
            </ol>
            <br><br><br>
            If you have any questions, please feel free to give us a call. We'll be glad to assist you.
            <br><br>
            <strong>Click Below To Get Started!</strong>
            <br><br>
            <script>var ht = (("https:" == document.location.protocol) ? "https://" : "http://");document.write(unescape('%3Cscript type="text/javascript" src="'+ht+'bongous.com/applink/loadAppLink.php?logo=16&key1=NjIy&key2=28235cbb05eb897e12e58dc66ef07437&url='+ht+'bongous.com"%3E%3C/script%3E'));</script>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->