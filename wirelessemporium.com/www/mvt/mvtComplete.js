var mvtProjID = 0
if(typeof(Storage)!=="undefined") {
	mvtProjID = localStorage.mvtProjID
}
else {
	cookieArray = document.cookie.split(";")
	for(i=0;i<cookieArray.length;i++) {
		alert(cookieArray[i])
		selCookieArray = cookieArray[i].split("=")
		cookieName = selCookieArray[0].replace(" ","")
		cookieVal = selCookieArray[1].replace(" ","")
		if (cookieName == "mvtProjID") {
			alert("got the val:" + cookieVal)
			mvtProjID = cookieVal
		}
	}
}

if (mvtProjID > 0) {
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-35629729-1']);
	_gaq.push(['_setCustomVar',1,'mvtVarient_' + mvtProjID,eval("localStorage.mvtID_" + mvtProjID),2]);
	_gaq.push(['_trackPageview']);
	  _gaq.push(['_addTrans',
	  '<%=nOrderID%>',           // order ID - required
	  '<%=storeName%>',  // affiliation or store name
	  '<%=nOrderGrandTotal%>',          // total - required
	  '<%=nOrderTax%>',           // tax
	  '<%=nOrderShippingFee%>',              // shipping
	  '<%=nOrderCity%>',       // city
	  '<%=nOrderState%>',     // state or province
	  '<%=nOrderCountry%>'             // country
	]);
	<%=transItem%>
  
   (function() {
	  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
}