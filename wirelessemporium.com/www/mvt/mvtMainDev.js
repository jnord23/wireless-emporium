if (mvtTestPages != "") {
	var pageName = window.location.pathname
	var pageHref = window.location.href
	var qsVal
	var bodyState = 1
	var cssUpdate = 0
	var saveCombo = ""
	var saveAB = ""
	var x, p, i, w
	if (pageHref.indexOf("?") > 0) {
		qsVal = pageHref.substring(pageHref.indexOf("?")+1)
		qsArray = qsVal.split("=")
		for(var q = 0; q < qsArray.length; q++) {
			qsName = qsArray[q]
			q++
			qsVal = qsArray[q]
			if (qsName == "testMVT") {
				saveCombo = qsVal
			}
			if (qsName == "bypassAB") {
				saveAB = qsVal
			}
		}
	}
	
	var projArray = mvtTestPages.split("##")
	var typeArray = mvtTestType.split("##")
	var detailArray = mvtDetails.split("##")
	var curType
	var mvtProjID = 0
	var comboCnt
	var urlVarList = ""
	var curPage
	var comboCnt
	var useMvtID = 0
	var gaqID = "na"
	var pageMatch
	var bPageLoc
	var curUrlVar
	var subStart = 0
	var process = 0
	var varSlot = 0
	var skipGaq = 0
	var bPageFound
	
	function dynamicPageTest(testPage) {
		if (testPage.indexOf("%") > 0) {
			curPageArray = testPage.split("%")
			pageMatch = 1
			randomPageNameVars = pageName
			for(w = 0; w < curPageArray.length; w++) {
				if (pageName.indexOf(curPageArray[w]) < 0) {
					pageMatch = 0
				}
				else {
					varPlacement = randomPageNameVars.indexOf(curPageArray[w])
					curUrlVar = randomPageNameVars.substring(subStart,varPlacement)
					if (curUrlVar != "") {
						urlVarList = urlVarList + curUrlVar + "##"
					}
					randomPageNameVars = randomPageNameVars.replace(curPageArray[w],'')
					randomPageNameVars = randomPageNameVars.replace(curUrlVar,'')
				}
			}
		}
	}
	
	for(p = 0; p < projArray.length; p++) {
		bPageFound = 0
		curPage = projArray[p]
		if (curPage != "" && process == 0) {
			curType = typeArray[p]
			if (curType != 'bPage') {
				varSlot++
			}
			dynamicPageTest(curPage)
			if (curPage == pageName || pageMatch == 1) {
				innerDetailArray = detailArray[p].split("&&")
				mvtProjID = innerDetailArray[0]
				comboCnt = innerDetailArray[1]
				
				if (curType == "aPageBypass") {
					curType = "aPage"
					saveAB = 1
				}
				
				//if aPage then make sure it's not a bPage
				if (pageMatch == 1 && curType == "aPage") {
					bPageArray = eval("mvtVariations_" + mvtProjID + ".split('##')")
					for(bPageTest = 0; bPageTest < bPageArray.length; bPageTest++) {
						dynamicPageTest(bPageArray[bPageTest])
						if (pageMatch == 1) { bPageFound = 1 }
					}
				}
				if (bPageFound == 0) {
					if(typeof(Storage)!=="undefined") {
						//Save visual setting to users browser
						localStorage.mvtProjID = mvtProjID
						// ################## temp to reset the mvtID ##################
						//eval("localStorage.testMvtID_" + mvtProjID + " = undefined")
						// #############################################################
						if(eval("localStorage.testMvtID_" + mvtProjID) == "undefined" || eval("localStorage.testMvtID_" + mvtProjID) == undefined) {
							useMvtID = Math.floor(Math.random() * comboCnt)
							eval("localStorage.testMvtID_" + mvtProjID + " = " + useMvtID)
						}
						else {
							useMvtID = eval("localStorage.testMvtID_" + mvtProjID)
							if (isNaN(useMvtID) == true) {
								useMvtID = Math.floor(Math.random() * comboCnt)
								eval("localStorage.testMvtID_" + mvtProjID + " = " + useMvtID)
							}
						}
					}
					else {
						cookieArray = document.cookie.split(";")
						for(i=0;i<cookieArray.length;i++) {
							selCookieArray = cookieArray[i].split("=")
							cookieName = selCookieArray[0].replace(" ","")
							cookieVal = selCookieArray[1].replace(" ","")
							if (cookieName == "mvtID_" + mvtProjID) {
								useMvtID = cookieVal
							}
						}
					}
					if (parseInt(useMvtID) >= parseInt(comboCnt)) {
						useMvtID = Math.floor(Math.random() * comboCnt)
					}
					document.cookie = "mvtProjID = " + mvtProjID
					document.cookie = "mvtID_" + mvtProjID + " = " + useMvtID
					
					if (saveAB != "") {
						useMvtID = saveAB
					}
					
					if (curType == "aPage") {
						process = 1
						bPageArray = eval("mvtVariations_" + mvtProjID + ".split('##')")
						bPageLoc = bPageArray[useMvtID-1]
						if (useMvtID > 0) {
							//if (pageMatch == 1) {}
							w = 0
							urlVarListArray = urlVarList.split("##")
							while (bPageLoc.indexOf("%") >= 0) {
								bPageLoc = bPageLoc.replace('%',urlVarListArray[w])
								w++
							}
							skipGaq = 1
							if (bPageLoc.indexOf("?") > 0) {
								bPageLoc = bPageLoc + '&rid=' + Math.floor(Math.random()*999999)
							}
							else {
								bPageLoc = bPageLoc + '?rid=' + Math.floor(Math.random()*999999)
							}
							location.replace(bPageLoc);
							//window.location = bPageLoc
						}
						gaqID = 0
					}
					else if (curType == "bPage") {
						gaqID = useMvtID
						document.write("<META NAME='ROBOTS' CONTENT='NOINDEX, NOFOLLOW'>")
					}
					else if (curType == "MVT") {
						process = 1
						variationArray = eval("mvtVariations_" + mvtProjID + ".split('##')")
						//if (useMvtID == 0) { useMvtID = 1; }
						gaqID = variationArray[(useMvtID)]
						if (saveCombo != "") {
							gaqID = saveCombo
						}
						myCssArray = gaqID.split(",")
						myCssFileArray = eval("mvtCss_" + mvtProjID + ".split('##')")
						
						for(x=0;x<myCssArray.length;x++) {
							arrayOption = parseInt(myCssArray[x]) - 1
							fileOptionArray = myCssFileArray[x].split("&&")
							useFileName = fileOptionArray[arrayOption]
							var fileref=document.createElement("link")
							fileref.setAttribute("rel", "stylesheet")
							fileref.setAttribute("type", "text/css")
							fileref.setAttribute("href", useFileName)
							document.getElementsByTagName("head")[0].appendChild(fileref)
						}
					}
				}
			}
		}
	}
	
	if (mvtProjID == 0) { varSlot = 0 }
	
	if (skipGaq == 0) {
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-36271972-1']);
		if (gaqID != "na") {
			if (curType == "MVT") {
				_gaq.push(['_setCustomVar',1,'mvtTest_' + mvtProjID,curType + '-' + gaqID,2]);
			}
			else {
				_gaq.push(['_setCustomVar',2,'abTest_' + mvtProjID,curType,2]);
			}
		}
		_gaq.push(['_trackPageview']);
		
		(function() {
		  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	}
}

function myAlert(msg) {
	if (myIP == "173.200.82.81" || myIP == "68.5.43.43") {
		alert("WE Office Only:" + msg)
	}
}