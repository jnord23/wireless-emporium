<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/includes/asp/inc_BasePage.asp"--><%	
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%

	dim siteID : siteID = prepInt(request.QueryString("siteID"))
	dim projID : projID = prepInt(request.QueryString("projID"))
	dim projName : projName = prepStr(request.QueryString("projName"))
	dim testPage : testPage = prepStr(request.QueryString("testPage"))
	dim cancelID : cancelID = prepInt(request.QueryString("cancelID"))
	dim comboActivationID : comboActivationID = prepInt(request.QueryString("comboActivationID"))
	dim isChecked : isChecked = prepStr(request.QueryString("isChecked"))
	dim showVarients : showVarients = prepStr(request.QueryString("showVarients"))
	dim adjustStagingSetting : adjustStagingSetting = prepInt(request.QueryString("adjustStagingSetting"))
	dim stagingSite : stagingSite = prepInt(request.QueryString("stagingSite"))
	dim adjustLiveSetting : adjustLiveSetting = prepInt(request.QueryString("adjustLiveSetting"))
	dim liveSite : liveSite = prepInt(request.QueryString("liveSite"))
	dim adjustTestType : adjustTestType = prepInt(request.QueryString("adjustTestType"))
	dim testType : testType = prepStr(request.QueryString("testType"))
	dim newPageA : newPageA = prepInt(request.QueryString("newPageA"))
	dim pageName : pageName = prepStr(request.QueryString("pageName"))
	dim newObjName : newObjName = prepStr(request.QueryString("newObjName"))
	dim newObjOpts : newObjOpts = prepInt(request.QueryString("newObjOpts"))
	dim delObjID : delObjID = prepInt(request.QueryString("delObjID"))
	dim newLinkPage : newLinkPage = prepInt(request.QueryString("newLinkPage"))
	
	dim siteURL
	select case siteID
		case 0 : siteURL = "http://www.wirelessemporium.com"
		case 1 : siteURL = "http://www.cellphoneaccents.com"
		case 2 : siteURL = "http://www.cellularoutfitter.com"
		case 3 : siteURL = "http://www.phonesale.com"
		case 10 : siteURL = "http://www.tabletmall.com"
	end select
	
	if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then siteURL = replace(siteURL,"www","staging")
	
	if comboActivationID > 0 then
		if isChecked = "false" then isChecked = 0 else isChecked = 1
		sql = "update mvt_variations set active = " & isChecked & " where id = " & comboActivationID
		session("errorSQL") = sql
		oConn.execute(sql)
		response.Write("update compelte")
		call CloseConn(oConn)
		response.End()
	elseif adjustLiveSetting = 1 then
		sql = "update mvt_projects set isLive = " & liveSite & " where id = " & projID
		session("errorSQL") = sql
		oConn.execute(sql)
		response.Write("update compelte")
		call CloseConn(oConn)
		response.End()
	elseif adjustStagingSetting = 1 then
		sql = "update mvt_projects set active = " & stagingSite & " where id = " & projID
		session("errorSQL") = sql
		oConn.execute(sql)
		response.Write("update compelte")
		call CloseConn(oConn)
		response.End()
	elseif adjustTestType = 1 then
		if testType = "mvt" then
			sql = "update mvt_projects set mvt = 1, ab = 0 where id = " & projID
		elseif testType = "combo" then
			sql = "update mvt_projects set combo = 1, mvt = 0, ab = 0 where id = " & projID
		else
			sql = "update mvt_projects set mvt = 0, ab = 1 where id = " & projID
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
	elseif newPageA = 1 then
		sql = "update mvt_projects set testPage = '" & pageName & "' where id = " & projID
		session("errorSQL") = sql
		oConn.execute(sql)
		response.Write("update compelte")
		call CloseConn(oConn)
		response.End()
	elseif newLinkPage then
		sql = "update mvt_projects set linkPage = '" & pageName & "' where id = " & projID
		session("errorSQL") = sql
		oConn.execute(sql)
		response.Write("update compelte")
		call CloseConn(oConn)
		response.End()
	elseif newObjName <> "" then
		sql = "insert into mvt_objects (projectID,name,components) values(" & projID & ",'" & newObjName & "'," & newObjOpts & ")"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "delete from mvt_variations where projectID = " & projID
		session("errorSQL") = sql
		oConn.execute(sql)
	elseif delObjID > 0 then
		sql = "update mvt_objects set disabled = 1 where projectID = " & projID & " and id = " & delObjID
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "delete from mvt_variations where projectID = " & projID
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	if cancelID > 0 then
		sql = "update mvt_projects set active = 0 where id = " & cancelID
		session("errorSQL") = sql
		oConn.execute(sql)
%>
<div id="mainSelect" style="height:20px;">
    <div style="float:left;">
        <select name="siteSelect" onchange="getProjects(this.value)">
            <option value="">Select Website</option>
            <option value="0">WirelessEmporium</option>
            <option value="2">CellularOutfitter</option>
            <option value="1">CellphoneAccents</option>
            <option value="3">PhoneSale</option>
        </select>
    </div>
    <div id="projectList" style="float:left;"></div>
</div>
<div id="projectDetails" style="height:20px; margin-top:10px;"></div>
<%
	end if
	
	if projName <> "" then
		sql = "SET NOCOUNT ON; insert into mvt_projects (siteID,name,testPage) values(" & siteID & ",'" & projName & "','" & testPage & "'); SELECT @@IDENTITY AS projID;"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		projID = rs("projID")
		
		sql = "select id, name, active from mvt_projects where siteID = " & siteID
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
%>
<div id="mainSelect" style="height:20px;">
    <div style="float:left;">
        <select name="siteSelect" onchange="getProjects(this.value)">
            <option value="">Select Website</option>
            <option value="0"<% if siteID = 0 then %> selected="selected"<% end if %>>WirelessEmporium</option>
            <option value="2"<% if siteID = 2 then %> selected="selected"<% end if %>>CellularOutfitter</option>
            <option value="1"<% if siteID = 1 then %> selected="selected"<% end if %>>CellphoneAccents</option>
            <option value="3"<% if siteID = 3 then %> selected="selected"<% end if %>>PhoneSale</option>
        </select>
    </div>
    <div id="projectList" style="float:left;">
    	<select name="projectID" onchange="projSelect(<%=siteID%>,this.value)">
            <option value="">Select Project</option>
            <option value="-1">New Project</option>
            <%
            do while not rs.EOF
				if rs("active") then
            %>
            <option value="<%=rs("id")%>"><%=rs("name")%></option>
            <%
				else
			%>
            <option value="<%=rs("id")%>"><%=rs("name")%> (Inactive)</option>
            <%
				end if
                rs.movenext
            loop
            %>
        </select>
    </div>
</div>
<div id="projectDetails" style="height:20px; margin-top:10px;">
	<div style="float:left;">Test Page:</div>
	<div style="float:left; margin-left:10px;"><input type="text" name="pageA" value="<%=testPage%>" onkeyup="newPageA(<%=projID%>,this.value)" style="font-size:10px;" size="42" /></div>
    <div style="float:left; margin-left:10px;"><a href="javascript:cancelProject(<%=projID%>)" style="color:#900;">Cancel Project</a></div>
</div>
<%
		call CloseConn(oConn)
		response.End()
	end if
	
	if projID = 0 then
		sql = "select id, name, active from mvt_projects where siteID = " & siteID
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
%>
<select name="projectID" onchange="projSelect(<%=siteID%>,this.value)">
	<option value="">Select Project</option>
    <option value="-1">New Project</option>
    <%
	do while not rs.EOF
		if rs("active") then
	%>
    <option value="<%=rs("id")%>"><%=rs("name")%></option>
    <%
		else
	%>
    <option value="<%=rs("id")%>"><%=rs("name")%> (inactive)</option>
    <%
		end if
		rs.movenext
	loop
	%>
</select>
<%
	elseif projID = -1 then
%>
<form name="newProj" action="javascript:void(0);" method="post">
<div style="float:left; padding-top:2px;">Project Name:</div>
<div style="float:left; margin-left:10px;"><input type="text" name="projName" value="" style="font-size:12px;" size="24" /></div>
<div style="float:left; padding-top:2px; margin-left:20px;">Test Page:</div>
<div style="float:left; margin-left:10px;"><input type="text" name="testPage" value="" style="font-size:12px;" size="42" /></div>
<div style="float:left; margin-left:10px;"><input type="button" name="myAction" value="Create Project" style="font-size:12px;" onclick="createProject(<%=siteID%>,document.newProj.projName.value,document.newProj.testPage.value)" /></div>
</form>
<%
	elseif projID > 0 then
		sql =	"select a.mvt, a.ab, a.combo, a.testPage, a.linkPage, a.isLive, a.active, b.name, b.components, (select count(*) from mvt_variations where projectID = " & projID & ") as varients " &_
				"from mvt_projects a " &_
					"left join mvt_objects b on a.id = b.projectID and b.disabled = 0 " &_
				"where a.id = " & projID
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		mvt = rs("mvt")
		ab = rs("ab")
		combo = rs("combo")
		testPage = rs("testPage")
		linkPage = prepStr(rs("linkPage"))
		isLive = rs("isLive")
		active = rs("active")
		%>
		<div style="height:20px; background-color:#ebebeb; padding-top:5px; border-top:2px solid #666; margin-top:10px; padding-bottom:5px; border-bottom:2px solid #666; margin-bottom:10px;">
			<div style="float:left;">Test Page:</div>
			<div style="float:left; margin-left:10px; border-right:1px dotted #CCC;"><input type="text" name="pageA" value="<%=testPage%>" onkeyup="newPageA(<%=projID%>,this.value)" style="font-size:10px;" size="42" /></div>
            <% if mvt or combo then %>
            <div style="float:left; margin-left:10px;">Link Page (optional):</div>
			<div style="float:left; margin-left:10px; border-right:1px dotted #CCC;"><input type="text" name="linkPage" value="<%=linkPage%>" onkeyup="newLinkPage(<%=projID%>,this.value)" style="font-size:10px;" size="42" /></div>
            <div style="float:left; margin-left:20px;">Varients to show:</div>
            <div style="float:left; margin-left:10px; border-right:1px dotted #CCC;">
            	<select onchange="showVarients(this.value,<%=projID%>,<%=siteID%>)">
                	<option value="active">Active Only</option>
                    <option value="all"<% if showVarients = "all" then %> selected="selected"<% end if %>>All</option>
                </select>
            </div>
            <% end if %>
            <div style="float:left; margin-left:20px;">Test Type:</div>
            <div style="float:left; margin-left:10px;">
            	<select onchange="updateTestType(<%=projID%>,this.value)">
                	<option value="mvt">MVT</option>
                    <option value="ab"<% if ab then %> selected="selected"<% end if %>>AB</option>
                    <option value="combo"<% if combo then %> selected="selected"<% end if %>>Combo</option>
                </select>
            </div>
            <div style="float:left; margin-left:20px;">Staging Site:</div>
            <div style="float:left; margin-left:10px;">
            	<select onchange="updateStagingSite(<%=projID%>,this.value)">
                	<option value="0">Inactive</option>
                    <option value="1"<% if active then %> selected="selected"<% end if %>>Active</option>
                </select>
            </div>
            <div style="float:left; margin-left:20px;">Live Site:</div>
            <div style="float:left; margin-left:10px;">
            	<select onchange="updateLiveSite(<%=projID%>,this.value)">
                	<option value="0">Inactive</option>
                    <option value="1"<% if isLive then %> selected="selected"<% end if %>>Active</option>
                </select>
            </div>
		</div>
        <% if ab or combo then %>
        <div style="width:100%; margin-top:10px;">
        	<div><strong>Page A</strong>: <%=testPage%></div>
            <% if combo then %>
            <div><strong>Page B</strong>: <%=replace(testPage,".","_mp.")%></div>
            <% else %>
        	<div><strong>Page B</strong>: <%=replace(testPage,".","_new.")%></div>
            <% end if %>
        </div>
        <% end if %>
		<% if mvt or combo then %>
		<div style="height:20px; width:100%; border-bottom:1px solid #CCC; margin-top:10px;">
        	<div style="float:left; margin-left:10px; width:50px; text-align:center; border-right:1px dotted #CCC;">Row</div>
			<div style="float:left; margin-left:10px; width:50px; text-align:center; border-right:1px dotted #CCC;">ID</div>
            <div style="float:left; margin-left:10px; width:100px; text-align:center; border-right:1px dotted #CCC;">View</div>
			<%
			totalCombos = 1
			objCnt = 0
			maxCombo = ""
			do while not rs.EOF
				objCnt = objCnt + 1
				objName = prepStr(rs("name"))
				objComp = prepInt(rs("components"))
				varients = prepInt(rs("varients"))
				if objName <> "" then
					totalCombos = totalCombos * objComp
					maxCombo = maxCombo & objComp & ","
			%>
			<div style="float:left; margin-left:10px; width:150px; text-align:center; border-right:1px dotted #CCC;"><%=objName%></div>
			<%
				end if
				rs.movenext
			loop
			%>
            <div style="float:left; margin-left:10px; width:50px; text-align:center; border-right:1px dotted #CCC;">Active</div>
		</div>
        <%
		end if
		
        if varients = 0 and maxCombo <> "" then
            if right(maxCombo,1) = "," then maxCombo = left(maxCombo,len(maxCombo)-1)
            maxArray = split(maxCombo,",")
            comboList = ""
            
            availableCombos = 0
            totalLaps = 0
			do while availableCombos < (totalCombos - 1)
                totalLaps = totalLaps + 1
                curCombo = ""
                randomize
                for i = 1 to objCnt
                    curCombo = curCombo & int(rnd * maxArray(i-1)) + 1 & ","
                next
                curCombo = left(curCombo,len(curCombo)-1)
                if instr(comboList,curCombo) < 1 then comboList = comboList & curCombo & "##"
                if right(comboList,2) = "##" then
                    availableCombos = ubound(split(left(comboList,len(comboList)-2),"##"))
                else
                    availableCombos = ubound(split(comboList,"##"))
                end if
                if totalLaps > 20000 then exit do
            loop
            comboArray = split(left(comboList,len(comboList)-2),"##")
            
            For i = 0 to availableCombos 
               For j = i + 1 to availableCombos 
                  if comboArray(i) > comboArray(j) then
                      TemporalVariable = comboArray(i)
                      comboArray(i) = comboArray(j)
                      comboArray(j) = TemporalVariable
                 end if
               next 
            next  
            
            for i = 0 to availableCombos
                sql = "insert into mvt_variations (projectID,combo) values(" & projID & ",'" & comboArray(i) & "')"
                session("errorSQL")
                oConn.execute(sql)
            next
        end if
        
        if mvt or combo then
			if showVarients = "active" or showVarients = "" then showVars = " and active = 1" else showVars = ""
			sql = "select * from mvt_variations where projectID = " & projID & showVars
			session("errorSQL")
			set varientRS = oConn.execute(sql)
			
			varientCnt = 0
			bgColor = "#fff"
			do while not varientRS.EOF
				varientCnt = varientCnt + 1
				comboID = prepInt(varientRS("id"))
				curCombo = varientRS("combo")
				comboArray = split(curCombo,",")
				comboActive = varientRS("active")
			%>
			<div style="height:20px; width:100%; border-bottom:1px solid #CCC; background-color:<%=bgColor%>;">
				<div style="float:left; margin-left:10px; width:50px; text-align:center; border-right:1px dotted #CCC;"><%=varientCnt%></div>
                <div style="float:left; margin-left:10px; width:50px; text-align:center; border-right:1px dotted #CCC;"><%=comboID%></div>
				<div style="float:left; margin-left:10px; width:100px; text-align:center; border-right:1px dotted #CCC;">
                	<%
					'temporary implementation
					if instr(testPage, "product.asp") > 0 and siteID = 3 then
						addlQueryString = "&itemid=361944"
					end if
					
					if linkPage <> "" then
						usePage = linkPage
					else
						usePage = testPage
					end if
					%>
                	<a href="<%=siteURL%>/<%=usePage%>?testMVT=<%=curCombo%><%=addlQueryString%>" target="mvtTest">
                    	Link <%=curCombo%>
                    </a>
				</div>
				<% for i = 0 to ubound(comboArray) %>
				<div style="float:left; margin-left:10px; width:150px; text-align:center; border-right:1px dotted #CCC;"><%=comboArray(i)%></div>
				<% next %>
				<div style="float:left; margin-left:10px; width:50px; text-align:center; border-right:1px dotted #CCC;"><input type="checkbox" name="activeCombo" value="1"<% if comboActive then %> checked="checked"<% end if %> onclick="comboActivation(<%=comboID%>,this.checked)" /></div>
			</div>
			<%
				if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
				varientRS.movenext
				response.Flush()
			loop
			%>
			<div style="height:20px; width:100%; border-bottom:1px solid #CCC;">Total Active Varients: <%=varientCnt%></div>
			<%
			
			sql = "select id, name, components from mvt_objects where projectID = " & projID & " and disabled = 0 order by id"
			session("errorSQL") = sql
			set objRS = oConn.execute(sql)
			
			%>
            <div style="height:20px; width:300px; border-bottom:1px solid #CCC; background-color:#333; color:#FFF;">
				<div style="float:left; margin-left:10px; width:150px; text-align:center; border-right:1px dotted #CCC;">Test Objects</div>
                <div style="float:left; margin-left:10px; padding-right:10px; width:50px; text-align:center; border-right:1px dotted #CCC;">Options</div>
                <div style="float:left; margin-left:10px; width:50px; text-align:center;">Action</div>
			</div>
            <%
			do while not objRS.EOF
				objID = prepInt(objRS("id"))
				objName = prepStr(objRS("name"))
				objComps = prepInt(objRS("components"))
			%>
            <div style="height:20px; width:300px; border-bottom:1px solid #CCC; background-color:<%=bgColor%>;" id="mvtObj_<%=objID%>">
				<div style="float:left; margin-left:10px; width:150px; text-align:left; border-right:1px dotted #CCC;"><%=objName%></div>
                <div style="float:left; margin-left:10px; padding-right:10px; width:50px; text-align:center; border-right:1px dotted #CCC;"><%=objComps%></div>
                <div style="float:left; margin-left:10px; width:50px; text-align:center;"><a onclick="removeObject(<%=projID%>,<%=objID%>)" style="color:#F00; cursor:pointer;">Remove</a></div>
			</div>
            <%
				objRS.movenext
			loop
			%>
            <form name="newObjectForm" method="post" style="padding:0px; margin:0px;" action="javascript:void(0);">
            <div style="height:20px; width:300px; border-bottom:1px solid #CCC; background-color:#FFF; padding:5px 0px 5px 0px;">
				<div style="float:left; margin-left:10px; width:150px; text-align:left; border-right:1px dotted #CCC;"><input type="text" name="newObjName" value="" style="font-size:10px;" size="32" /></div>
                <div style="float:left; margin-left:10px; padding-right:10px; width:50px; text-align:center; border-right:1px dotted #CCC;"><input type="text" name="newObjOpts" value="2" style="font-size:10px;" size="2" /></div>
                <div style="float:left; margin-left:10px; width:50px; text-align:center;"><a onclick="addObject(<%=projID%>)" style="color:#090; cursor:pointer;">Add</a></div>
			</div>
            </form>
            <%
		end if
	end if
	call CloseConn(oConn)
%>