<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	dim newMobile : newMobile = prepInt(request.QueryString("newMobile"))
	
	call getDBConn(oConn)
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/cart/UPSxml2.asp"-->
<%	
	if isnumeric(request.QueryString("zip")) then
		sZip = prepInt(request.QueryString("zip"))
	else
		sZip = prepStr(request.QueryString("zip"))
	end if
	sWeight = prepStr(request.QueryString("weight"))
	shiptype = prepStr(request.QueryString("shiptype"))
	nTotalQuantity = prepInt(request.QueryString("nTotalQuantity"))
	nSubTotal = prepInt(request.QueryString("nSubTotal"))
	
	dim shipcost0, shipcost2, shipcost3
	shipcost0 = "0.00"
	shipcost2 = "6.99"
	shipcost3 = "19.99"
	dim international : international = 0
	
	if instr(sZip,"-") > 0 then
		zipArray = split(sZip,"-")
		useZip = zipArray(0)
	else
		useZip = sZip
	end if
	
	do while len(useZip) < 5
		useZip = "0" & useZip
	loop
	
	if not isnumeric(useZip) then international = 1
	
	if prepStr(session("sr_token")) = "" then
		on error resume next
		if useZip <> "" then
			response.write UPScode(useZip,sWeight,shiptype,nTotalQuantity,0,0)
		end if
		on error goto 0
	else
		response.Write("&nbsp;")
	end if	
	
	call CloseConn(oConn)
%>