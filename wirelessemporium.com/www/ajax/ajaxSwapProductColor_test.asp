<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/Framework/Utility/Static.asp"-->
<!--#include virtual="/includes/asp/inc_OnSale.asp"-->
<!--#include virtual="/includes/asp/inc_Compatibility.asp"-->
<%
call getDBConn(oConn)

selectedColorID = prepInt(request.QueryString("selectedColorID"))
itemid = prepInt(request.QueryString("itemid"))
onSale = prepInt(request.QueryString("onsale"))
adminOption = prepInt(request.QueryString("adminOption"))
hasFlashFile = prepInt(request.QueryString("hasflash"))
updatePage = prepStr(request.QueryString("uPage"))
updateType = prepStr(request.QueryString("uType"))
itemMaxLen = prepStr(request.QueryString("itemMaxLen"))
testAB = prepStr(request.QueryString("testAB"))

select case ucase(updatePage)
	case "P"
		sql = 	"select	top 1 d.itemid, d.itemdesc, d.partnumber, d.itempic, d.price_our, d.price_retail, d.colorid, d.typeid, d.vendor" & vbcrlf & _
				"from	xproductcolormatrix a join we_items b" & vbcrlf & _
				"	on	a.slave = b.partnumber join xproductcolormatrix c" & vbcrlf & _
				"	on	a.master = c.master join we_items d" & vbcrlf & _
				"	on	c.slave = d.partnumber and b.modelid = d.modelid" & vbcrlf & _
				"where	b.itemid = '" & itemid & "'" & vbcrlf & _
				"	and	d.colorid = '" & selectedColorID & "'"		
		session("errorSQL") = sql

		select case ucase(updateType)
			case "PRE"
				set fsThumb = CreateObject("Scripting.FileSystemObject")
				sql	=	"select	isnull((select top 1 inv_qty from we_items where partNumber = d.partNumber and master = 1 order by inv_qty desc), 0) as masterInvQty" & vbcrlf & _
						"	,	d.itemid, d.itemdesc, d.partnumber, d.itempic, d.price_our, d.price_retail, d.colorid, d.typeid, d.vendor" & vbcrlf & _
						"	,	convert(varchar(4000), d.itemlongdetail) itemlongdetail, d.BULLET1, d.BULLET2, d.BULLET3, d.BULLET4, d.BULLET5, d.BULLET6" & vbcrlf & _
						"	,	d.BULLET7, d.BULLET8, d.BULLET9, d.BULLET10, convert(varchar(4000), d.compatibility) compatibility, d.PackageContents, d.itemDimensions" & vbcrlf & _
						"	, 	d.download_URL, d.download_TEXT, e1.brandName, e2.modelName, e3.typename_we typename" & vbcrlf & _
						"from	xproductcolormatrix a join we_items b" & vbcrlf & _
						"	on	a.slave = b.partnumber join xproductcolormatrix c" & vbcrlf & _
						"	on	a.master = c.master join we_items d" & vbcrlf & _
						"	on	c.slave = d.partnumber and b.modelid = d.modelid left outer join we_brands e1" & vbcrlf & _
						"	on	d.brandid = e1.brandid left outer join we_models e2" & vbcrlf & _
						"	on	d.modelid = e2.modelid left outer join we_types e3" & vbcrlf & _
						"	on	d.typeid = e3.typeid" & vbcrlf & _
						"where	b.itemid = '" & itemid & "'" & vbcrlf & _
						"	and	d.hidelive = 0 and d.inv_qty <> 0" & vbcrlf & _
						"order by d.colorid"
'				response.write "<pre>" & sql & "</pre>"
				session("errorSQL") = sql
				set rsSwap = oConn.execute(sql)
				lap = -1
				do until rsSwap.eof
					masterInvQty = cint(rsSwap("masterInvQty"))
					if masterInvQty > 0 then
						lap = lap + 1
						itemid = rsSwap("itemid")
						partnumber = rsSwap("partnumber")
						itemPic = rsSwap("itemPic")
						typeid = rsSwap("typeid")
						vendor = rsSwap("vendor")
						itemImgPath = Server.MapPath("/productpics/big") & "\" & itemPic
						if not fsThumb.FileExists(itemImgPath) then itemPic = "imagena.jpg"
	
						'====================== big image
						response.write "<img src=""/productpics/big/" & itempic & """>"
						if blnIsNotOriginalPrice then response.write "<img src=""/images/LargeStar.png"" />"
						if hasFlashFile then response.write "<img src=""/images/product/360/360-clicktoview-button.png"" />"
						
						'====================== alt image
						response.write getAltImageHTML(itempic, typeid, vendor, partnumber)
						
						'====================== itemlongDetails
						set oParam = CreateObject("Scripting.Dictionary")
						oParam.CompareMode = vbTextCompare
						oParam.Add "x_longdesc", rsSwap("itemlongdetail")
						oParam.Add "x_typeid", typeid
						oParam.Add "x_bullet1", rsSwap("bullet1")
						oParam.Add "x_bullet2", rsSwap("bullet2")
						oParam.Add "x_bullet3", rsSwap("bullet3")
						oParam.Add "x_bullet4", rsSwap("bullet4")
						oParam.Add "x_bullet5", rsSwap("bullet5")
						oParam.Add "x_bullet6", rsSwap("bullet6")
						oParam.Add "x_bullet7", rsSwap("bullet7")
						oParam.Add "x_bullet8", rsSwap("bullet8")
						oParam.Add "x_bullet9", rsSwap("bullet9")
						oParam.Add "x_bullet10", rsSwap("bullet10")
						
						if isnull(rsSwap("compatibility")) or len(rsSwap("compatibility")) < 1 then
							compatibility = CompatibilityList(partnumber,"display")
						else
							compatibility = rsSwap("compatibility")
						end if
						
						oParam.Add "x_compatibility", compatibility
						oParam.Add "x_package", rsSwap("PackageContents")
						oParam.Add "x_dimensions", rsSwap("itemDimensions")
						oParam.Add "x_partnumber", partnumber
						oParam.Add "x_itemid", itemid
						oParam.Add "x_downloadUrl", rsSwap("download_URL")
						oParam.Add "x_downloadTxt", rsSwap("download_TEXT")
						oParam.Add "x_brandName", rsSwap("brandName")
						oParam.Add "x_modelName", rsSwap("modelName")
						oParam.Add "x_typeName", rsSwap("typeName")
						response.write "<div style=""display:none;"" class=""ProductDetailTabContent"" id=""new_logdesc_" & lap & """>" & getItemLongDetails(oParam) & "</div>"
					end if
					
					rsSwap.movenext
				loop
			case "IMG"
				set rsSwap = oConn.execute(sql)
				
				set fsThumb = CreateObject("Scripting.FileSystemObject")			
				childItemid = rsSwap("itemid")
				itemdesc = rsSwap("itemdesc")
				partnumber = rsSwap("partNumber")
				altText = itemdesc
				itemPic = rsSwap("itemPic")
				itemImgPath = Server.MapPath("/productpics/big") & "\" & itemPic
				itemImgFullPath = "/productpics/big/" & itemPic
				if not fsThumb.FileExists(itemImgPath) then itemImgFullPath = "/productpics/big/imagena.jpg"
				
				objItemImgFull = "<div style=""width:300px; display:block; position: relative; z-index:1;"">"
				objItemImgFull = objItemImgFull & "<div style=""display:block; position:absolute; top:0px; left:0px; z-index:2;""><img src=""" & itemImgFullPath & """ width=""300"" border=""0"" title=""" & replace(insertDetails(altText), "'", "") & """ id=""imgLarge""></div>"

				if onSale then objItemImgFull = objItemImgFull & "<div style=""display:block; position:absolute; top:0px; right:0px; z-index:3;""><img src=""/images/LargeStar.png"" alt=""Sale Price"" title=""Sale Price"" border=""0"" /></div>"
				if hasFlashFile then objItemImgFull = objItemImgFull & "<div style=""cursor:pointer; display:block; position:absolute; top:250px; right:-70px; z-index:3;""><img id=""id_view360"" onClick=""playFlash();"" src=""/images/product/360/360-clicktoview-button.png"" alt=""360 image"" title=""360 image"" border=""0"" /></div>"

				objItemImgFull = objItemImgFull & "</div>"
				objItemImgFull = objItemImgFull & "<div style=""display:none;"" id=""new_itemid"">" & childItemid & "</div>"
				objItemImgFull = objItemImgFull & "<div style=""display:none;"" id=""new_partNumber"">" & partnumber & "</div>"

				if fsThumb.fileExists(server.MapPath("/productpics/big/zoom/" & itempic)) then
					response.write "<a href=""javascript:showFloatingZoomImage();"">" & objItemImgFull & "</a>"
				else
					response.write objItemImgFull
				end if
			case "ALTIMG"
				sql =	"select	top 1 d.itemid, d.itemdesc, d.partnumber, e.itempic, d.price_our, d.price_retail, d.colorid, d.typeid, d.vendor " &_
						"from xproductcolormatrix a " &_
							"join we_items b on a.slave = b.partnumber " &_
							"join xproductcolormatrix c on a.master = c.master " &_
							"join we_items d on c.slave = d.partnumber and b.modelid = d.modelid " &_
							"join we_Items e on d.PartNumber = e.PartNumber and e.master = 1 " &_
						"where b.itemid = '" & itemid & "' and d.colorid = '" & selectedColorID & "'"
				session("errorSQL") = sql
				set rsSwap = oConn.execute(sql)
				
				partnumber = rsSwap("partnumber")
				itempic = rsSwap("itempic")
				typeid = rsSwap("typeid")
				vendor = rsSwap("vendor")
				
				if testAB = "B" then
					response.write getAltImageHTML2(itempic, typeid, vendor, partnumber)
				else
					response.write getAltImageHTML(itempic, typeid, vendor, partnumber)
				end if
			case "ALTIMG2"
				set rsSwap = oConn.execute(sql)
				
				partnumber = rsSwap("partnumber")
				itempic = rsSwap("itempic")
				typeid = rsSwap("typeid")
				vendor = rsSwap("vendor")
				
				response.write getAltImageHTML_b(itempic)
			case "TITLE"
				set rsSwap = oConn.execute(sql)
				if not rsSwap.eof then response.write rsSwap("itemdesc")
		end select				
	case "BMCD"
		sql = 	"select	top 1 d.itemid, d.itemdesc, d.partnumber, d.itempic, d.price_our, d.price_retail, d.colorid" & vbcrlf & _
				"from	xproductcolormatrix a join we_items b" & vbcrlf & _
				"	on	a.slave = b.partnumber join xproductcolormatrix c" & vbcrlf & _
				"	on	a.master = c.master join we_items d" & vbcrlf & _
				"	on	c.slave = d.partnumber and b.modelid = d.modelid" & vbcrlf & _
				"where	b.itemid = '" & itemid & "'" & vbcrlf & _
				"	and	d.colorid = '" & selectedColorID & "'"
		session("errorSQL") = sql
		set rsSwap = oConn.execute(sql)
	
		if not rsSwap.eof then
			select case ucase(updateType)
				case "PRE"
					set fsThumb = CreateObject("Scripting.FileSystemObject")			
					itemPic = rsSwap("itemPic")
					itemImgPath = Server.MapPath("/productpics/thumb") & "\" & itemPic
					useImgPath = "/productpics/thumb/" & itemPic
					if not fsThumb.FileExists(itemImgPath) then useImgPath = "/productpics/thumb/imagena.jpg"
					response.write "<img src=""" & useImgPath & """ border=""0"" />"
				case "IMG"
					set fsThumb = CreateObject("Scripting.FileSystemObject")
					childItemid = rsSwap("itemid")
					itemdesc = rsSwap("itemdesc")
					altText = itemdesc
					partnumber = rsSwap("partnumber")
					itemPic = rsSwap("itemPic")
					colorid = rsSwap("colorid")
					itemImgPath = Server.MapPath("/productpics/thumb") & "\" & itemPic
					useImgPath = "/productpics/thumb/" & itemPic
					if not fsThumb.FileExists(itemImgPath) then useImgPath = "/productpics/thumb/imagena.jpg"
									
					productLink = "/p-" & childItemid &"-"& formatSEO(itemdesc)
	
					call ImageHtmlTagWrapper( "<img id=""img_" & itemid & "_" & colorid & """ src=""" & useImgPath & """ border=""0"" width=""100"" height=""100"" title="""& altText &""" alt="""& altText &""">", productLink, onSale)
					prepInt(session("adminID"))
					if cint(adminOption) > 0 then
						response.write "<br /><span style=""color:red; font-size:10px;"">" & partnumber & "</span><br /><span style=""font-size:10px;"">" & childItemid & "</span>"
						if isRelated = 1 then response.write " (<span style=""color:blue; font-size:10px;"">Related</span>)"
					end if
									
				case "LINK"
					childItemid = rsSwap("itemid")
					itemdesc = rsSwap("itemdesc")
					productLink = "/p-" & childItemid &"-"& formatSEO(itemdesc)
					if len(itemdesc) >= 85 then
						itemDescEtc = left(itemdesc,80) & "..."
					else
						itemDescEtc = itemdesc
					end if
					itemDescEtc = replace(itemDescEtc, "/", " / ")
					response.write "<a class=""cellphone-link"" href=""" & productLink & """ title=""" & itemdesc & """ alt=""" & itemdesc & """>" & itemDescEtc & "</a>"
				case "PRICE"
					%>
						<span class="boldText">Our&nbsp;Price:&nbsp;</span>
						<span class="pricing-orange2"><%=formatCurrency(rsSwap("price_our"))%></span><br>
						<span class="pricing-gray2">List&nbsp;Price:&nbsp;<s><%=formatCurrency(rsSwap("price_retail"))%></s></span>
					<%
			end select
		end if
	case "BMC"
		sql = 	"select	top 1 d.itemid, d.itemdesc, d.partnumber, d.itempic, d.price_our, d.price_retail, d.colorid" & vbcrlf & _
				"from	xproductcolormatrix a join we_items b" & vbcrlf & _
				"	on	a.slave = b.partnumber join xproductcolormatrix c" & vbcrlf & _
				"	on	a.master = c.master join we_items d" & vbcrlf & _
				"	on	c.slave = d.partnumber and b.modelid = d.modelid" & vbcrlf & _
				"where	b.itemid = '" & itemid & "'" & vbcrlf & _
				"	and	d.colorid = '" & selectedColorID & "'"
		session("errorSQL") = sql
		set rsSwap = oConn.execute(sql)
		
		if not rsSwap.EOF then
			select case ucase(updateType)
				case "PRE"
					set fsThumb = CreateObject("Scripting.FileSystemObject")			
					itemPic = rsSwap("itemPic")
					itemImgPath = Server.MapPath("/productpics/thumb") & "\" & itemPic
					useImgPath = "/productpics/thumb/" & itemPic
					if not fsThumb.FileExists(itemImgPath) then useImgPath = "/productpics/thumb/imagena.jpg"
					response.write "<img src=""" & useImgPath & """ border=""0"" />"
				case "IMG"
					set fsThumb = CreateObject("Scripting.FileSystemObject")
					childItemid = rsSwap("itemid")
					itemdesc = rsSwap("itemdesc")
					altText = itemdesc
					partnumber = rsSwap("partnumber")
					itemPic = rsSwap("itemPic")
					colorid = rsSwap("colorid")
					itemImgPath = Server.MapPath("/productpics/thumb") & "\" & itemPic
					useImgPath = "/productpics/thumb/" & itemPic
					if not fsThumb.FileExists(itemImgPath) then useImgPath = "/productpics/thumb/imagena.jpg"
									
					productLink = "/p-" & childItemid &"-"& formatSEO(itemdesc)
	
					call ImageHtmlTagWrapper( "<img id=""img_" & itemid & "_" & colorid & """ src=""" & useImgPath & """ border=""0"" width=""100"" height=""100"" title="""& altText &""" alt="""& altText &""">", productLink, onSale)
					if cint(adminOption) > 0 then
						response.write "<br /><span style=""color:red; font-size:10px;"">" & partnumber & "</span>"
						response.write "<br /><span style=""font-size:10px;"">" & childItemid & "</span>"
						if isRelated = 1 then response.write " (<span style=""color:blue; font-size:10px;"">Related</span>)"
					end if								
				case "LINK"
					childItemid = rsSwap("itemid")
					itemdesc = rsSwap("itemdesc")
					itemDescEtc = itemdesc
					productLink = "/p-" & childItemid &"-"& formatSEO(itemdesc)
					
					if len(itemdesc) > itemMaxLen then
						nextSpace = inStr(right(itemdesc, len(itemdesc) - itemMaxLen), " ")
						itemDescEtc = left(itemdesc, itemMaxLen + nextSpace) & "..."
					end if				
					itemDescEtc = replace(itemDescEtc, "/", " / ")
					response.write "<a class=""cellphone-link"" href=""" & productLink & """ title=""" & itemdesc & """ alt=""" & itemdesc & """>" & itemDescEtc & "</a>"
				case "PRICE"
					price_retail = rsSwap("price_retail")
					price_our = rsSwap("price_our")
					%>
						<div align="left" class="index-price-retail">Was <del><%=formatcurrency(price_retail)%></del></div>
						<div align="left" class="index-price-our">Our Price <%=formatcurrency(price_our)%></div>
						<div align="left"><b>Save <%=formatPercent((price_Retail - price_Our) / price_Retail,0)%></b></div>
					<%
			end select
		end if
	case "P"
end select


call CloseConn(oConn)


function getItemLongDetails(byref pParam)
	strItemLongDetail	=	pParam.Item("x_longdesc")
	typeid 				=	pParam.Item("x_typeid")
	BULLET1				=	pParam.Item("x_bullet1")
	BULLET2				=	pParam.Item("x_bullet2")
	BULLET3				=	pParam.Item("x_bullet3")
	BULLET4				=	pParam.Item("x_bullet4")
	BULLET5				=	pParam.Item("x_bullet5")
	BULLET6				=	pParam.Item("x_bullet6")
	BULLET7				=	pParam.Item("x_bullet7")
	BULLET8				=	pParam.Item("x_bullet8")
	BULLET9				=	pParam.Item("x_bullet9")
	BULLET1				=	pParam.Item("x_bullet10")
	COMPATIBILITY		=	pParam.Item("x_compatibility")
	PackageContents 	= 	pParam.Item("x_package")
	itemDimensions		=	pParam.Item("x_dimensions")
	partnumber			=	pParam.Item("x_partnumber")
	itemid				=	pParam.Item("x_itemid")
	download_URL		=	pParam.Item("x_downloadUrl")
	download_TEXT		=	pParam.Item("x_downloadTxt")
	brandName			=	pParam.Item("x_brandName")
	modelName			=	pParam.Item("x_modelName")
	categoryName		=	pParam.Item("x_typeName")
	
	strItemLongDetail = replace(strItemLongDetail,"Wireless Emporium","WirelessEmporium.com")
	strItemLongDetail = replace(strItemLongDetail,chr(34),"''")
	strItemLongDetail = replace(replace(replace(replace(replace(strItemLongDetail,vbcrlf," "), chr(150), "-"), chr(151), "-"), chr(146), "'"), "  ", " ")
	if instr(strItemLongDetail,"Buy this quality") > 0 then
		useLongDesc = left(strItemLongDetail,instr(strItemLongDetail,"Buy this quality")-1)
	elseif instr(strItemLongDetail,"Remember - WirelessEmporium.com") > 0 then
		useLongDesc = left(strItemLongDetail,instr(strItemLongDetail,"Remember - WirelessEmporium.com")-1)
	else
		useLongDesc = strItemLongDetail
	end if
	ret	= "<p style=""margin-top:0px;"">" & insertDetails(useLongDesc) & "</p>" & vbcrlf

	if typeid = 16 then
		ret	= ret & "<div style=""font-size:14px; font-weight:bold;"">PHONE FEATURES:</div>" & vbcrlf
		if BULLET1 <> "" or BULLET2 <> "" or BULLET3 <> "" or BULLET4 <> "" or BULLET5 <> "" or BULLET6 <> "" or BULLET7 <> "" or BULLET8 <> "" or BULLET9 <> "" or BULLET10 <> "" then
			ret	= ret & "<ul style=""margin-top:0px;"">" & vbcrlf
			if BULLET1 <> "" then ret = ret & "<li>" & insertDetails(BULLET1) & "</li>" & vbcrlf
			if BULLET2 <> "" then ret = ret & "<li>" & insertDetails(BULLET2) & "</li>" & vbcrlf
			if BULLET3 <> "" then ret = ret & "<li>" & insertDetails(BULLET3) & "</li>" & vbcrlf
			if BULLET4 <> "" then ret = ret & "<li>" & insertDetails(BULLET4) & "</li>" & vbcrlf
			if BULLET5 <> "" then ret = ret & "<li>" & insertDetails(BULLET5) & "</li>" & vbcrlf
			if BULLET6 <> "" then ret = ret & "<li>" & insertDetails(BULLET6) & "</li>" & vbcrlf
			if BULLET7 <> "" then ret = ret & "<li>" & insertDetails(BULLET7) & "</li>" & vbcrlf
			if BULLET8 <> "" then ret = ret & "<li>" & insertDetails(BULLET8) & "</li>" & vbcrlf
			if BULLET9 <> "" then ret = ret & "<li>" & insertDetails(BULLET9) & "</li>" & vbcrlf
			if BULLET10 <> "" then ret = ret & "<li>" & insertDetails(BULLET10) & "</li>" & vbcrlf
			ret = ret & "</ul>" & vbcrlf
		end if
		if COMPATIBILITY <> "" then
			ret = ret & "<p><div style=""font-size:14px; font-weight:bold;"">COMPATIBILITY:</div>" & COMPATIBILITY & "</p>" & vbcrlf
		end if
		ret = ret & "<p><div style=""font-size:14px; font-weight:bold;""> WHAT IS INCLUDED: </div>" & PackageContents & "</p>" & vbcrlf
		ret = ret & "<p><em>Wireless Emporium offers direct replacement and unlocked Cell/PDA/Smart Phones to you WITHOUT ANY CONTRACTS! Order your replacement or unlocked Cell/PDA/Smart Phones today and we'll ship it out to your home or business for FREE!</em>" & vbcrlf
		ret = ret & "<p style=""font-size:14px; font-weight:bold;""> Click Here: ** <a href=""javascript:Open_Popup('/phone_return_policy','PhoneReturnPolicy','width=550,height=650,scrollbars=no');"" style=""font-size:14px;font-weight:bold;color:#CC3300;text-decoration:none;"">Cell Phone Return Policy</a> **</p>" & vbcrlf
	else
		if typeID = 1 or typeID = 2 or typeID = 6 or typeID = 7 then
			ret = ret & "<h5><i>All factory-direct Wireless Emporium Cell Phone " & categoryName & " are manufactured to the highest ISO 9001:2000 certified quality standards.</i></h5>" & vbcrlf
		end if
	end if
	
	if not isnull(itemDimentions) and itemDimentions <> "" then
		ret = ret & "<p style=""margin-top:0px;"">Product Dimensions: <span style=""font-size:13px; font-weight:bold;"">" & itemDimentions & "</span></p>" & vbcrlf
	end if
	ret = ret & "<p style=""margin-top:0px;"">Part&nbsp;Number:&nbsp;<b>" & partnumber & "</b></p>" & vbcrlf
	ret = ret & "<p style=""margin-top:0px;"">Item&nbsp;ID:&nbsp;<b>" & itemid & "</b></p>" & vbcrlf

	if download_URL <> "" then 
		ret = ret & "<p><a href=""" & download_URL & """ target=""_blank"">" & download_TEXT & "</a></p>" & vbcrlf
	end if
    
    getItemLongDetails = ret
end function

function getAltImageHTML(itempic, typeid, vendor, partnumber)
	set fsAlt = CreateObject("Scripting.FileSystemObject")			
	set jpeg = CreateObject("Persits.Jpeg")

	a = 0
	itemImgPath = "/productpics/big"
	itemImgFullPath = itemImgPath & "/" & itemPic
	
	strAltImage = "<div class=""fl altImg""><a style=""cursor:pointer;"" onclick=""fnPreviewImage('" & itemImgFullPath & "')""><img src=" & replace(itemImgFullPath,"/big/","/icon/") & " border=""0"" width=""40"" height=""40"" /></a></div>"
	for iCount = 0 to 7
		path = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".jpg")
		src = replace(path,".jpg","_thumb.jpg")
		if fsAlt.FileExists(Server.MapPath(path)) and not fsAlt.FileExists(Server.MapPath(src)) then
			jpeg.Open Server.MapPath(path)
			jpeg.Height = 40
			jpeg.Width = 40
			jpeg.Save Server.MapPath(src)
		end if
		if fsAlt.FileExists(Server.MapPath(path)) and fsAlt.FileExists(Server.MapPath(src)) then						
			strAltImage = strAltImage & "<div class=""fl altImg""><a style=""cursor:pointer;"" onclick=""fnPreviewImage('" & path & "')""><img src=""" & src & """ border=""0"" width=""40"" height=""40"" /></a></div>"
		end if
	next
	
	if typeID = 17 then
		a = a + 1
		if vendor = "DS" then
			strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/decal_pic5.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/decal_pic5.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
		else
			strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/gg_atl_image1.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/gg_atl_image1.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
		end if
	end if
	'decal skins new alt image by Terry (11/9/11)
	if vendor = "DS" then
		strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/decalskin-mg.jpg');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/decalskin-mg.jpg"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
	end if

	if strAltImage <> "" then
		if lCase(left(partnumber,3)) = "fp2" or lCase(left(partnumber,3)) = "fp3" then
			a = a + 1
			strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/FP-SNAP-ON-ANIM.gif');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb2"" src=""/productpics/AltViews/FP-SNAP-ON-ANIM_icon.gif"" width=""40"" height=""40"" class=""altImgBorder""></a>" & vbcrlf
			strAltImage = strAltImage & "<script language=""javascript"">" & vbcrlf
			strAltImage = strAltImage & "if (document.images) {" & vbcrlf
			strAltImage = strAltImage & "newLargeImg = new Image();" & vbcrlf
			strAltImage = strAltImage & "newLargeImg.src = '/productpics/AltViews/FP-SNAP-ON-ANIM.gif'"
			strAltImage = strAltImage & "}" & vbcrlf
			strAltImage = strAltImage & "</script>" & vbcrlf
		end if
	else
		strAltImage = "&nbsp;"
	end if	
	getAltImageHTML = strAltImage
end function

function getAltImageHTML_b(itempic)
	set fs = CreateObject("Scripting.FileSystemObject")
	itemImgFullPath = "/productpics/big/" & itempic
	strAltImage = "<div class='fl altImg'><a style='cursor:pointer;' onclick=fnPreviewImage('" & itemImgFullPath & "')><img src='" & replace(itemImgFullPath,"/big/","/icon/") & "' border='0' width='40' height='40' /></a></div>"

	for iCount = 0 to 7
		path = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".jpg")
		src = replace(path,".jpg","_thumb.jpg")
		if fs.FileExists(Server.MapPath(path)) and fs.FileExists(Server.MapPath(src)) then
			strAltImage = strAltImage & "<div class='fl altImg'><a style='cursor:pointer;' onclick=fnPreviewImage('" & path & "')><img src='" & src & "' border='0' width='40' height='40' /></a></div>"
		end if
	next
	getAltImageHTML_b = strAltImage
end function

function getAltImageHTML2(itempic, typeid, vendor, partnumber)
	set fsAlt = CreateObject("Scripting.FileSystemObject")			
	set jpeg = CreateObject("Persits.Jpeg")

	a = 0
	for iCount = 0 to 2
		path = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".jpg")
		if fsAlt.FileExists(Server.MapPath(path)) then
			a = a + 1
			src = replace(path,".jpg","_thumb.jpg")
			if fsAlt.FileExists(Server.MapPath(src)) then
				strAltImage = strAltImage & "<a class=""lnkAltimage"" onmousedown=""javascript:fnPreviewImage('" & path & "');""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" border=""0"" class=""altImgBorder_big""></a>" & vbcrlf
			else
				jpeg.Open Server.MapPath(path)
				jpeg.Height = 40
				jpeg.Width = 40
				jpeg.Save Server.MapPath(src)
				strAltImage = strAltImage & "<a class=""lnkAltimage"" onmousedown=""javascript:fnPreviewImage('" & path & "');"" ><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" border=""0"" class=""altImgBorder_big""></a>" & vbcrlf
			end if
		end if
		path = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".gif")
		if fsAlt.FileExists(Server.MapPath(path)) then
			a = a + 1
			src = path
			strAltImage = strAltImage & "<a class=""lnkAltimage"" onmousedown=""javascript:fnPreviewImage('" & src & "');"" ><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" border=""0"" class=""altImgBorder_big""></a>" & vbcrlf
		end if
		path = "/productpics/AltViews/" & replace(itempic,".gif","-" & iCount & ".jpg")
		if fsAlt.FileExists(Server.MapPath(path)) then
			a = a + 1
			src = path
			strAltImage = strAltImage & "<a class=""lnkAltimage"" onmousedown=""javascript:fnPreviewImage('" & src & "');"" ><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" border=""0"" class=""altImgBorder_big""></a>" & vbcrlf
		end if
		path = "/productpics/AltViews/" & replace(itempic,".gif","-" & iCount & ".gif")
		if fsAlt.FileExists(Server.MapPath(path)) then
			a = a + 1
			src = path
			strAltImage = strAltImage & "<a class=""lnkAltimage"" onmousedown=""javascript:fnPreviewImage('" & src & "');""><img id=""imgThumb" & a & """ src=""" & src & """ width=""40"" height=""40"" border=""0"" class=""altImgBorder_big""></a>" & vbcrlf
		end if
	next
	if typeID = 17 then
		a = a + 1
		if vendor = "DS" then
			strAltImage = strAltImage & "<a class=""lnkAltimage"" onmousedown=""javascript:fnPreviewImage('/productpics/AltViews/decal_pic5.jpg');""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/decal_pic5.jpg"" width=""40"" height=""40"" border=""0"" class=""altImgBorder_big""></a>" & vbcrlf
		else
			strAltImage = strAltImage & "<a class=""lnkAltimage"" onmousedown=""javascript:fnPreviewImage('/productpics/AltViews/gg_atl_image1.jpg');""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/gg_atl_image1.jpg"" width=""40"" height=""40"" border=""0"" class=""altImgBorder_big""></a>" & vbcrlf
		end if
	end if
	'decal skins new alt image by Terry (11/9/11)
	if vendor = "DS" then
		strAltImage = strAltImage & "<a class=""lnkAltimage"" onmousedown=""javascript:fnPreviewImage('/productpics/AltViews/decalskin-mg.jpg');""><img id=""imgThumb" & a & """ src=""/productpics/AltViews/decalskin-mg.jpg"" width=""40"" height=""40"" border=""0"" class=""altImgBorder_big""></a>" & vbcrlf
	end if
	
	if strAltImage <> "" then
		if lCase(left(partnumber,3)) = "fp2" or lCase(left(partnumber,3)) = "fp3" then
			a = a + 1
			strAltImage = strAltImage & "<a class=""lnkAltimage"" onmousedown=""javascript:fnPreviewImage('/productpics/AltViews/FP-SNAP-ON-ANIM.gif');""><img id=""imgThumb2"" src=""/productpics/AltViews/FP-SNAP-ON-ANIM_icon.gif"" width=""40"" height=""40"" border=""0"" class=""altImgBorder_big""></a>" & vbcrlf
			strAltImage = strAltImage & "<script language=""javascript"">" & vbcrlf
			strAltImage = strAltImage & "if (document.images) {" & vbcrlf
			strAltImage = strAltImage & "newLargeImg = new Image();" & vbcrlf
			strAltImage = strAltImage & "newLargeImg.src = '/productpics/AltViews/FP-SNAP-ON-ANIM.gif'"
			strAltImage = strAltImage & "}" & vbcrlf
			strAltImage = strAltImage & "</script>" & vbcrlf
		end if
		strAltImage = "<a class=""lnkAltimage"" onmousedown=""javascript:fnPreviewImage('/productpics/big/" & itempic & "');""><img src='/productpics/thumb/" & itempic & "' width='40' height='40' class=""altImgBorder_big"" /></a>" & strAltImage
	else
		strAltImage = "&nbsp;"
	end if	
	
	getAltImageHTML2 = strAltImage
end function
%>