<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/Framework/Utility/Static.asp"-->
<!-- METADATA TYPE="typelib" NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<%
	call getDBConn(oConn)

	itemid = prepInt(request.queryString("itemid"))
	strSort = prepStr(request.queryString("strSort"))
	canonicalURL = prepStr(request.queryString("canonicalURL"))
	
	sql = "exec getReviews 0, " & itemid & ", '" & strSort & "'"
	session("errorSQL") = sql
	set objRsReviews = oConn.execute(sql)
	
	if not objRsReviews.EOF then
		do while not objRsReviews.EOF
            reviewID = objRsReviews("id")
            reviewTitle = objRsReviews("reviewTitle")
            reviewerNickName = objRsReviews("reviewerNickName")
            reviewerLocation = objRsReviews("reviewerLocation")
            reviewerAge = objRsReviews("reviewerAge")
            reviewerGender = objRsReviews("reviewerGender")
            reviewerType = objRsReviews("reviewerType")
            starRating = objRsReviews("starRating")
            ownershipPeriod = objRsReviews("ownershipPeriod")
            entryDate = objRsReviews("entryDate2")

            isRecommend = objRsReviews("isRecommend")
            isProPrice = objRsReviews("isProPrice")
            isProDesign = objRsReviews("isProDesign")
            isProWeight = objRsReviews("isProWeight")
            isProProtection = objRsReviews("isProProtection")
            isConFit = objRsReviews("isConFit")
            isConRobust = objRsReviews("isConRobust")
            isConMaintenance = objRsReviews("isConMaintenance")
            isConWeight = objRsReviews("isConWeight")
            thumbsUpCount = objRsReviews("thumbsUpCount")
            thumbsDownCount = objRsReviews("thumbsDownCount")
            
            otherCon = objRsReviews("otherCon")
            otherPro = objRsReviews("otherPro")
            reviewBody = objRsReviews("reviewBody")
            %>
            <div class="review-item">
                <div class="review">
                    <div class="star-rating">
                        <%=getRatingAvgStarBig(starRating)%>
                        <span class="average"><%=formatnumber(starRating, 0)%></span>
                    </div>
                    <div class="title"><%=reviewTitle%></div>
                    <div class="date">Posted on <%=entryDate%></div>
                    <div class="content"><p><%=reviewBody%></p></div>
                    <div class="helpful">
                        <div class="label">Was this review helpful?</div>
                        <a href="javascript:updateThumbs(<%=reviewID%>,1,<%=thumbsUpCount%>)" class="vote-yes">
                            <div class="yes"><div class="yes-icon-active"></div></div>
                            <div class="yes-count" id="thumbsUpCount<%=reviewID%>"><%=formatnumber(thumbsUpCount, 0)%></div>
                        </a>
                        <a href="javascript:updateThumbs(<%=reviewID%>,0,<%=thumbsDownCount%>)" class="vote-no">
                            <div class="no"><div class="no-icon-inactive"></div></div>
                            <div class="no-count" id="thumbsDownCount<%=reviewID%>"><%=formatnumber(thumbsDownCount, 0)%></div>
                        </a>
                    </div>
                </div>
                <div class="reviewer">
				    <div class="profile-photo"><img src="/images/reviews/head.png" /></div>
                    <div class="username"><%=reviewerNickName%></div>
					<%if isRecommend then%>
					<div class="recommended"></div>
					<%end if%>                    
                    <div class="user-details">
                        <div class="row">
                            <label>Age: </label>
                            <%
                            if not isnull(reviewerAge) then
                                select case cint(reviewerAge)
                                    case 1 : response.write "17 & under"
                                    case 2 : response.write "18 - 25"
                                    case 3 : response.write "26 - 35"
                                    case 4 : response.write "36 - 45"
                                    case 5 : response.write "46 - 55"
                                    case 6 : response.write "56+"
                                    case else : response.write "N/A"
                                end select
                            else
                                response.write "N/A"
                            end if
                            %>
                        </div>
                        <div class="row">
                            <label>Gender: </label>
                            <%
                            if not isnull(reviewerGender) then
                                if reviewerGender = "M" then
                                    response.write "Male"
                                else
                                    response.write "Female"
                                end if
                            else
                                response.write "N/A"
                            end if
                            %>
                        </div>
                        <div class="row">
                            <label>Description: </label>
                            <%
                            if not isnull(reviewerType) then
                                select case ucase(reviewerType)
                                    case "STU" : response.write "Student"
                                    case "BO" : response.write "Business Owner"
                                    case "TE" : response.write "Technology Enthusiast"
                                    case "PA" : response.write "Phone Addict"
                                    case "CPU" : response.write "Casual Phone User"
                                end select
                            else
                                response.write "N/A"
                            end if
                            %>
                        </div>
                        <!--
                        <div class="row">
                            <label>Ownership: </label>
                            <%
                            if not isnull(ownershipPeriod) then
                                select case cint(ownershipPeriod)
                                    case 1 : response.write "2 ~ 4 Weeks"
                                    case 2 : response.write "1 ~ 2 Months"
                                    case 3 : response.write "3 ~ 12 Months"
                                    case 4 : response.write "1 year +"
                                end select
                            else
                                response.write "N/A"
                            end if
                            %>
                        </div>
                        -->
                    </div>
                    <!--
                    <div class="procons">
                        <div class="row">
                            <label>Pros: </label>
                            <%
                            strPros = ""
                            if isProPrice then strPros = "Great Price, "
                            if isProDesign then strPros = strPros & "Beautifully Designed, "
                            if isProWeight then strPros = strPros & "Functional, "
                            if isProProtection then strPros = strPros & "Convenient, "
                            if strPros <> "" then response.write left(strPros, len(strPros)-2)
                            if not isnull(otherPro) then 
                                if strPros <> "" then response.write "<br>"
                                response.write "<span style=""font-style:italic;"">""" & otherPro & """</span>"
                            end if
                            %>
                        </div>
                        <div class="row">
                            <label>Cons: </label>
                            <%
                            strCons = ""
                            if isConFit then strCons = "Poor Value, "
                            if isConRobust then strCons = strCons & "Doesn't Fit, "
                            if isConMaintenance then strCons = strCons & "Not What I Expected, "
                            if isConWeight then strCons = strCons & "Doesn't Work Well, "
                            if strCons <> "" then response.write left(strCons, len(strCons)-2)
                            if not isnull(otherCon) then 
                                if strCons <> "" then response.write "<br>"
                                response.write "<span style=""font-style:italic;"">""" & otherCon & """</span>"
                            end if
                            %>
                        </div>
                    </div>
                    -->
                </div>
                <div class="share">
                    <div class="label">Share:</div>
					<a href="http://www.facebook.com/share.php?u=<%=canonicalURL%>" onClick="return fbs_click()" target="_blank" title="Share This on Facebook" class="facebook"></a>
                    <a href="https://twitter.com/share" target="_blank" class="twitter"></a>
                </div>
            </div>
            <%
            objRsReviews.movenext
        loop
	end if
	
	call CloseConn(oConn)
	
	function getRatingAvgStarBig(rating)
		dim nRating 
		dim strRatingImg
		nRating = cdbl(rating)
		strRatingImg = 	"<span class=""empty""></span><span class=""empty""></span><span class=""empty""></span><span class=""empty""></span><span class=""empty""></span>"
		
		if nRating > 0 or nRating <=5 then
			strRatingImg = ""	
			for i=1 to 5
				if nRating => i then
					strRatingImg = strRatingImg & "<span class=""full""></span>"
				elseif nRating => ((i - 1) + .1) then
					strRatingImg = strRatingImg & "<span class=""half""></span>"				
				else
					strRatingImg = strRatingImg & "<span class=""empty""></span>"
				end if
			next		
		end if
		
		getRatingAvgStarBig = strRatingImg
	end function
%>
