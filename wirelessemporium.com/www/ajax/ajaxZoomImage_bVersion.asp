<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/Framework/Data/Const.asp"-->
<!--#include virtual="/Framework/Utility/Static.asp"-->
<%	
set fs = CreateObject("Scripting.FileSystemObject")

call getDBConn(oConn)

itemid			=	prepInt(request.QueryString("itemid"))
screenHeight	=	prepInt(request.QueryString("screenH"))
useImg			=	prepStr(request.QueryString("useImg"))
shipping		=	prepInt(request.QueryString("shipping"))
newSize 		=	800

if screenHeight <= 900 and screenHeight <> 0 then
	newSize = screenHeight - 100
end if

sql =	"select c.parentID " &_
		"from we_Items a " &_
			"join we_Items b on a.PartNumber = b.PartNumber and b.master = 1 " &_
			"left join we_Items_bVersion c on b.itemID = c.parentID " &_
		"where a.itemID = " & itemid
set rs = oConn.execute(sql)

%>
<div id="id_zoomimage" style="display:inline-block; width:<%=newSize%>px; padding:3px; border:3px solid #666; background-color:#FFF; left:50%; position:fixed; margin-left:-<%=cint(newSize/2)%>px; margin-top:30px;">
<%
if rs.eof and useImg = "" then
%>
	<div style="float:left; padding:50px 0px 50px 0px;">No zoom image available</div>
	<div style="float:right; padding:50px 0px 50px 0px;"><a href="javascript:closeFloatingBox();"><img src="/images/icon-x.png" border="0" /></a></div>
<%
else
	parentID = rs("parentID")
%>
	<div style="float:left; width:100%; position:relative;">
        <div style="position:absolute; top:0; right:0;"><a href="javascript:closeFloatingBox();"><img src="/images/icon-x.png" border="0" /></a></div>    
    </div>
	<div style="float:left; text-align:center; width:100%;">
    <%
	defaultPath = "/productpics/big/zoom/pic-" & parentID & "-0.jpg"
	if fs.FileExists(Server.MapPath(defaultPath)) then
	%>
    	<div style="text-align:center;"><img id="id_img_zoom" src="<%=defaultPath%>" border="0" width="<%=newSize%>" height="<%=newSize%>" /></div>
    <%
	else
		defaultPath = "/productpics/big/pic-" & parentID & "-0.jpg"
		if fs.FileExists(Server.MapPath(defaultPath)) then
	%>
    	<div style="text-align:center;"><img id="id_img_zoom" src="<%=defaultPath%>" border="0" /></div>
    <%
		end if
	end if
	%>
    </div>
	<div style="float:left; text-align:center;">
    <%
	bigPath = ""
	if fs.FileExists(server.mappath("/productpics/big/zoom/pic-" & parentID & "-0.jpg")) then
		bigPath = "/productpics/big/zoom/pic-" & parentID & "-0.jpg"
	elseif fs.FileExists(server.mappath("/productpics/big/pic-" & parentID & "-0.jpg")) then
		bigPath = "/productpics/big/pic-" & parentID & "-0.jpg"
	end if
	
	if bigPath <> "" then
	%>
    <div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onClick="fnSwapZoomImage('<%=bigPath%>');"><img id="id_altimg_zoom" src="<%=bigPath%>" width="40" height="40" border="0" /></div>
    <%
	end if

	'bug fix: when there are zoom alt images available, no regular alt images are not showing.
	dim arrAltImages(7)
	
	for iCount = 0 to 7
		path = "/productpics/AltViews/zoom/pic-" & parentID & "-0-" & iCount & ".jpg"
		if fs.FileExists(Server.MapPath(path)) then
			arrAltImages(iCount) = path
		end if
	next

	for iCount = 0 to 7
		path = "/productpics/AltViews/pic-" & parentID & "-0-" & iCount & ".jpg"
		if arrAltImages(iCount) = "" and fs.FileExists(Server.MapPath(path)) then
			arrAltImages(iCount) = path
		end if
	next

	for iCount = 0 to 7
		if arrAltImages(iCount) <> "" then
		%>
		<div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onClick="fnSwapZoomImage('<%=arrAltImages(iCount)%>');"><img id="id_altimg_zoom<%=iCount%>" src="<%=arrAltImages(iCount)%>" width="40" height="40" border="0" /></div>
		<%
		end if
	next
	%>
    </div>
<%
end if

call CloseConn(oConn)
%>
</div>