<!--#include virtual="/Framework/Utility/Static.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
	orderid = prepInt(request.QueryString("orderid"))
	submitted = prepStr(request.QueryString("submitted"))
	if submitted = "Y" then
		cdo_to = "status@wirelessemporium.com"
		cdo_from = prepStr(request.querystring("txtContactEmail"))
		cdo_subject = prepStr(request.QueryString("txtSubject"))
		cdo_body = cdo_body & "<p><b>Order #: </b>" & orderid & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Customer's Email: </b>" & prepStr(request.querystring("txtContactEmail")) & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Customer's Message: </b>" & prepStr(request.querystring("txtMessage")) & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>&nbsp;</p><p>" & prepStr(Request.ServerVariables("REMOTE_ADDR")) & "</p>" & vbcrlf

		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
	%>
    <div style="width:350px; padding:5px; text-align:left; margin-left:auto; margin-right:auto; margin-top:150px; position:relative; background-color:#fff; border:3px solid #ccc; border-radius:5px;">
        <div style="position:absolute; top:-12px; right:-12px; cursor:pointer;" onclick="closeContactUsPopup()"><img src="/images/orderstatus/icon-lightbox-close.png" border="0" /></div>
        <div style="float:left; width:100%; background-color:#f2f2f2;">
            <div style="float:left; padding:5px;"><img src="/images/orderstatus/icon-lightbox-header.png" border="0" /></div>
            <div style="float:left; color:#333; font-size:18px; padding-top:6px;">Your message has been received</div>
        </div>
        <%if orderid <> "" then%>
        <div style="float:left; width:100%; padding:20px 0px 20px 0px; color:#333; font-size:24px; text-align:center;">Order #<%=orderid%></div>
        <%end if%>
        <div style="float:left; width:100%; padding-top:10px;">
            <div style="float:left; color:#333; padding:10px; font-size:14px; font-weight:bold;">We are on it! The Wireless Emporium team is researching your order #<%=orderid%> now. We will contact you with your answer ASAP.</div>
        </div>
        <div style="clear:both; height:30px;"></div>
    </div>
    <%
	else
	%>
    <div style="width:350px; padding:5px; text-align:left; margin-left:auto; margin-right:auto; margin-top:150px; position:relative; background-color:#fff; border:3px solid #ccc; border-radius:5px;">
        <form name="frmContactus" method="post">
        <div style="position:absolute; top:-12px; right:-12px; cursor:pointer;" onclick="closeContactUsPopup()"><img src="/images/orderstatus/icon-lightbox-close.png" border="0" /></div>
        <div style="float:left; width:100%; background-color:#f2f2f2;">
            <div style="float:left; padding:5px;"><img src="/images/orderstatus/icon-lightbox-header.png" border="0" /></div>
			<%if orderid <> "" then%>
            <div style="float:left; color:#333; font-size:18px; padding-top:6px;">Contact us about order #<%=orderid%></div>
            <%else%>
            <div style="float:left; color:#333; font-size:18px; padding-top:6px;">Contact us</div>
            <%end if%>            
        </div>
        <div style="float:left; width:100%; padding-top:10px;">
            <div style="float:left; width:100%; color:#333; font-size:14px; font-weight:bold;">Email Address: <span style="color:#ff6600;">*</span></div>
            <div style="float:left; width:100%; padding-top:5px;">
            	<%if request.querystring("txtContactEmail") = "" then%>
                <input type="text" name="txtContactEmail" value="Enter your Email Address" onclick="if (this.value=='Enter your Email Address') this.value='';" class="contactEmail" />
                <%else%>
                <input type="text" name="txtContactEmail" value="<%=prepStr(request.querystring("txtContactEmail"))%>" onclick="if (this.value=='Enter your Email Address') this.value='';" class="contactEmail" />
                <%end if%>
            </div>
        </div>
        <div style="float:left; width:100%; padding-top:15px;">
            <div style="float:left; width:100%; color:#333; font-size:14px; font-weight:bold;">Subject: <span style="color:#ff6600;">*</span></div>
            <div style="float:left; width:100%; padding-top:5px;">
                <input type="text" name="txtSubject" value="My order #<%=orderid%> has not arrived as of <%=date%>" class="contactEmail" style="font-size:12px;" />
            </div>
        </div>
        <div style="float:left; width:100%; padding-top:15px;">
            <div style="float:left; width:100%; color:#333; font-size:14px; font-weight:bold;">Your Message: <span style="color:#ff6600;">*</span></div>
            <div style="float:left; width:100%; padding-top:5px;">
                <!--<textarea name="txtMessage" onfocus="if (this.value=='Enter your message') this.value='';" onclick="if (this.value=='Enter your message') this.value='';" class="contactContent">Enter your message</textarea>-->
				<textarea name="txtMessage" class="contactContent">My order #<%=orderid%> has not arrived. Please check on it and let me know when I can expect delivery. Thank you.</textarea>
            </div>
        </div>
        <div style="float:left; width:100%; padding-top:15px;">
            <div style="float:left; padding:5px; cursor:pointer;" class="orangeRoundBox" onclick="sendContactUs()">
                <div style="float:left; padding:5px;"><img src="/images/orderstatus/icon-lightbox-arrow.png" border="0" /></div>
                <div style="float:left; color:#fff; font-size:16px; font-weight:bold; padding-top:2px;">Submit your question!</div>            	
            </div>
        </div>
        <div style="float:left; width:100%; padding:10px 0px 15px 0px; color:#666;">FIELDS MARKED WITH * ARE REQUIRED.</div>
        <div style="clear:both;"></div>
        <input type="hidden" name="hidOrderID" value="<%=orderid%>" />
        </form>
    </div>
    <%
	end if
	
	call CloseConn(oConn)
%>
