<%
	response.buffer = true
	response.expires = -1

%>
<!--#include virtual="/Framework/Utility/Static.asp"-->
<%	

	dim thisUser, pageTitle, header
	
	call getDBConn(oConn)

	dim brandid, modelid, categoryid, hzBox
	hzBox = prepInt(request.QueryString("hzBox"))
	brandid = prepInt(request.QueryString("brandid"))
	modelid = prepInt(request.QueryString("modelid"))
	categoryid = prepInt(request.QueryString("categoryid"))
	dim selectModelid : selectModelid = prepInt(request.QueryString("selectModelid"))
	
	if hzBox = 1 then hzBox = 2 else hzBox = ""
	
	dim modelCategory : modelCategory = ""
	if brandid <> 0 then
		sql	=	"exec sp_accessoryFinder_byBrand " & brandid
		set objRs = oConn.execute(sql)
		%>
        <select name="cbModel" style="width:175px;" onchange="onFinder<%=hzBox%>('m',this);">
            <option value="">Select Phone Model</option>
        <%
		do until objRs.eof
			if objRS("menuCat") <> modelCategory then
				if modelCategory <> "" then
					response.Write("</optgroup>")
				end if
				modelCategory = objRS("menuCat")
				response.Write("<optgroup label=""" & modelCategory & """>")
			end if
		%>
        	<option value="<%=objRs("modelid")%>"<% if selectModelid = prepInt(objRs("modelid")) then %> selected="selected"<% end if %>><%=objRs("modelname")%></option>
        <%
			objRs.movenext
		loop
		%>
        </select>
        <%
	elseif modelid <> 0 then
		sql	=	"select	distinct b.typeid, b.typename" & vbcrlf & _
				"from	we_items a join v_subtypematrix b" & vbcrlf & _
				"	on	a.subtypeid = b.subtypeid" & vbcrlf & _
				"where	a.modelid = '" & modelid & "'" & vbcrlf & _
				"	and	a.hidelive = 0 and a.inv_qty <> 0" & vbcrlf & _
				"union" & vbcrlf & _
				"select	typeid, typeName" & vbcrlf & _
				"from	we_typemasking" & vbcrlf & _
				"where	typeid in (5, 8)" & vbcrlf & _
				"order by 1"
		set objRs = oConn.execute(sql)
		%>
        <select name="cbCat" style="width:175px;">
            <option value="">Select Category</option>
        <%
		do until objRs.eof
		%>
        
        	<option value="<%=objRs("typeid")%>"><%=objRs("typename")%></option>
        <%
			objRs.movenext
		loop
		%>
        </select>
        <%
	end if
	
	call CloseConn(oConn)
%>