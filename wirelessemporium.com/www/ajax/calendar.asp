<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/includes/asp/inc_dbconn.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%

	dim defaultDate : defaultDate = prepInt(request.QueryString("defaultDate"))
	dim formName : formName = prepStr(request.QueryString("formName"))
	dim curMonth : curMonth = prepInt(request.QueryString("curMonth"))
	dim curDay : curDay = prepInt(request.QueryString("curDay"))
	dim curYear : curYear = prepInt(request.QueryString("curYear"))
	dim activeDate : activeDate = prepStr(request.QueryString("activeDate"))
	dim displayBox : displayBox  = prepStr(request.QueryString("displayBox"))
	dim firstWeek : firstWeek = 1
	
	if curMonth = 0 then curMonth = month(date)
	if curDay = 0 then curDay = day(date)
	if curYear = 0 then curYear = year(date)
	
	if defaultDate <> 0 then curDay = curDay + defaultDate
	if activeDate = "" or not isDate(activeDate) then
		activeDate = date
		curDay = day(activeDate)
	else
		curDay = day(activeDate)
	end if
	
	startingDay = weekday(cdate(curMonth & "/1/" & curYear))
	
	maxDays = 28
	for i = 29 to 31
		if isDate(curMonth & "/" & i & "/" & curYear) then maxDays = i
	next
%>
<div style="float:left; width:140px; border:1px solid #000; background-color:#FFF;">
	<div style="float:left; width:100%; height:18px; padding-top:2px; background-color:#000; color:#FFF; font-size:14px; font-weight:bold;">
		<div style="float:left; width:20px; text-align:center; cursor:pointer;" onclick="ajax('/ajax/calendar.asp?curMonth=<%=curMonth-1%>&displayBox=<%=displayBox%>','<%=displayBox%>')">&lt;</div>
		<div style="float:left; width:100px; text-align:center;"><%=monthName(curMonth)%></div>
        <div style="float:left; width:20px; text-align:center; cursor:pointer;" onclick="ajax('/ajax/calendar.asp?curMonth=<%=curMonth+1%>&displayBox=<%=displayBox%>','<%=displayBox%>')">&gt;</div>
    </div>
    <div style="float:left; width:100%; height:18px; padding-top:2px; background-color:#000; color:#FFF; font-size:10px; text-align:center">
		<div style="float:left; width:20px; text-align:center;">S</div>
        <div style="float:left; width:20px; text-align:center;">M</div>
        <div style="float:left; width:20px; text-align:center;">T</div>
        <div style="float:left; width:20px; text-align:center;">W</div>
        <div style="float:left; width:20px; text-align:center;">T</div>
        <div style="float:left; width:20px; text-align:center;">F</div>
        <div style="float:left; width:20px; text-align:center;">S</div>
    </div>
    <%
	drawDay = 0
	for i = 0 to maxDays
		drawDay = i
		for x = 0 to 6
			if (x+1) < startingDay and firstWeek = 1 then
	%>
    <div style="float:left; width:19px; height:19px; padding-top:10px; border-right:1px dotted #CCC; border-bottom:1px dotted #CCC;"></div>
    <%
			else
				drawDay = drawDay + 1
				if drawDay <= maxDays then
					if drawDay = curDay then
	%>
    <div style="float:left; width:19px; height:19px; padding-top:10px; text-align:center; cursor:pointer; border-right:1px dotted #CCC; border-bottom:1px dotted #CCC; background-color:#ff4408;"><%=drawDay%></div>
    <%
					else
	%>
    <div style="float:left; width:19px; height:19px; padding-top:10px; text-align:center; cursor:pointer; border-right:1px dotted #CCC; border-bottom:1px dotted #CCC;" onclick="calendarDaySelect('<%=displayBox%>','<%=curMonth%>/<%=drawDay%>/<%=curYear%>','<%=left(displayBox,5)%>')" onmouseover="this.style.backgroundColor='#ffb69e';" onmouseout="this.style.backgroundColor='#fff';"><%=drawDay%></div>
	<%
					end if
				end if
			end if
		next
		if firstWeek = 1 then
			firstWeek = 0
			i = i + (7-startingDay)
		else
			i = i + 6
		end if
	next
	%>
</div>
<%
	call CloseConn(oConn)
%>