<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%
	call getDBConn(oConn)

	dim brandID, modelID, slave, modelSlaveID
	
	brandID = prepInt(request.QueryString("brandID"))
	modelID = prepInt(request.QueryString("modelID"))
	catID = prepInt(request.QueryString("catID"))
	slave = prepInt(request.QueryString("slave"))
	modelSlaveID = prepInt(request.QueryString("modelSlaveID"))
	itemID = prepInt(request.QueryString("itemID"))
	
	if modelID > 0 or itemID > 0 then
		if itemID > 0 then
			sql = "select itemID, itemDesc, inv_qty from we_items a where hideLive = 0 and itemID = " & itemID
		elseif catID > 0 then
			sql = "select itemID, itemDesc, inv_qty from we_items a where hideLive = 0 and typeID = " & catID & " and modelID = " & modelID & " and not exists(select * from we_items where partNumber = a.partNumber and modelID = " & modelSlaveID & ")"
		else
			sql = "select itemID, itemDesc, inv_qty from we_items a where hideLive = 0 and modelID = " & modelID & " and not exists(select * from we_items where partNumber = a.partNumber and modelID = " & modelSlaveID & ")"
		end if
		prodSQL = sql
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
		
		sql = "select typeID, typeName from we_types order by typeName"
		session("errorSQL") = sql
		Set catRS = Server.CreateObject("ADODB.Recordset")
		catRS.open sql, oConn, 0, 1
		
		if itemID = 0 then
%>
<div>
	<div style="float:left; width:140px;">Select Category:</div>
	<div style="float:left; padding-left:10px; width:450px;">
    	<select name="model" onchange="catSelect(this.value,<%=modelID%>)">
        	<option value="">Select All Categories</option>
	        <%
    	    do while not catRS.EOF
        	%>
	        <option value="<%=catRS("typeID")%>"<% if cdbl(catID) = cdbl(catRS("typeID")) then %> selected="selected"<% end if %>><%=catRS("typeName")%></option>
    	    <%
        	    catRS.movenext
	        loop
    	    %>
	    </select>
    </div>
</div>
<%
		end if
%>
<div>
	<div style="float:left; width:140px;">Select Item:</div>
	<div style="float:left; padding-left:10px; width:450px; height:200px; overflow:auto; font-size:11px;">
    	<%
		lap = 0
		bgColor = "#ffffff"
		if rs.EOF then
		%>
        <div style="font-weight:bold; color:#F00; font-size:12px; padding:5px 0px 0px 5px;">NO PRODUCTS FOUND</div>
        <%
		end if
		
		do while not rs.EOF
			lap = lap + 1
		%>
	    <div style="background-color:<%=bgColor%>; color:<% if rs("inv_qty") = 0 then %>#F00<% else %>#000<% end if %>;"><input type="checkbox" name="chkBox_<%=lap%>" value="<%=rs("itemID")%>"><%=rs("itemDesc")%></div>
		<%
            if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
			rs.movenext
        loop
        %>
    </div>
</div>
<%
	else
		sql = "select modelID, modelName from we_models where brandID = " & brandID & " order by modelName"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
		
		if slave = 0 then
			objName = "model"
			onchange = "modelSelect(this.value)"
		else
			objName = "modelSlave"
			onchange = "saveModelSlave(this.value)"
		end if
%>
<div style="width:600px;">
	<div style="float:left; width:140px;" title="<%=objName%>,<%=slave%>">Select Model:</div>
	<div style="float:left; padding-left:10px; width:450px;">
        <select name="<%=objName%>" onchange="<%=onchange%>">
        	<option value="">Select Model</option>
	        <%
    	    do while not rs.EOF
        	%>
	        <option value="<%=rs("modelID")%>"><%=rs("modelName")%></option>
    	    <%
        	    rs.movenext
	        loop
    	    %>
	    </select>
    </div>
</div>
<%
	end if
	
	call CloseConn(oConn)
%>