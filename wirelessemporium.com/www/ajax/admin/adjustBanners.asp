<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
	thisUser = Request.Cookies("admin")("username")
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	call getDBConn(oConn)

	set fs = CreateObject("Scripting.FileSystemObject")
	
	viewSite = prepStr(request.QueryString("viewSite"))
	myAction = prepStr(request.QueryString("myAction"))
	lap = prepInt(request.QueryString("lap"))
	newBanner = prepStr(request.QueryString("newBanner"))
	newVal = prepStr(request.QueryString("newVal"))
	updateText = prepInt(request.QueryString("updateText"))
	
	if viewSite = "" then viewSite = "WE"
	
	if viewSite = "CO" then
		siteFolder = "cellularoutfitter.com"
		bFolder = "mainbanner"
	elseif viewSite = "WE" then
		siteFolder = "wirelessemporium.com"
		bFolder = "mainbanners"
	end if
	
	bannerFolder = server.MapPath("/images/" & bFolder & "/")
	
	if newBanner <> "" then
		sql = "update " & lcase(viewSite) & "_mainbanners set picLoc = '" & newBanner & "' where orderNum = " & lap & " and active = 1"
		session("errorSQL") = sql
		oConn.execute(sql)
		
	%>
    <img src="https://www.<%=siteFolder%>/images/<%=bFolder%>/<%=newBanner%>" border="0" height="100" />
    <%
		response.End()
	elseif updateText = 1 then
		sql = "update " & lcase(viewSite) & "_mainbanners set " & myAction & " = '" & newVal & "' where orderNum = " & lap & " and active = 1"
		session("errorSQL") = sql
		oConn.execute(sql)
		response.Write("update complete")
		call CloseConn(oConn)
		response.End()
	end if
%>
<table border="0" cellpadding="0" cellpadding="0">
    <%
	if newBanner = "" then
		set bannerFolder = fs.GetFolder(bannerFolder)
		set fileList = bannerFolder.Files
		for each banner in fileList
			if banner.name <> "Thumbs.db" then
	%>
    <tr><td align="left" style="cursor:pointer;" onclick="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=<%=myAction%>&newBanner=<%=banner.name%>','bannerImg_<%=lap%>');cancelAction(<%=lap%>)"><img src="https://www.<%=siteFolder%>/images/<%=bFolder%>/<%=banner.name%>" border="0" height="100" /></td></tr>
    <%
			end if
		next
	end if
	%>
</table>
<%
	call CloseConn(oConn)
%>