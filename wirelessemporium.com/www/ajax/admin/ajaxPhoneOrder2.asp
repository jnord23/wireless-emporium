<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<%	
mySession = request.cookies("mySession")
if mySession = "" then
	mySession = session.sessionid
	response.cookies("mySession") = mySession
	response.cookies("mySession").expires = dateAdd("m", 1, now)
end if

set fsThumb = CreateObject("Scripting.FileSystemObject")

sZip = prepStr(request.QueryString("sZip"))
sPromoCode = prepStr(request.QueryString("sPromoCode"))
if sPromoCode = "Promo Code" then sPromoCode = ""
sState = prepStr(request.QueryString("sState"))
sZip = prepStr(request.QueryString("sZip"))
curShipID = prepInt(request.QueryString("curShipID"))
siteid = prepInt(request.QueryString("siteid"))
orderid = prepInt(request.QueryString("orderid"))
adminID = prepInt(request.QueryString("adminID"))
searchItemID = prepInt(request.QueryString("searchItemID"))
addQty = prepInt(request.QueryString("addQty"))
addItemID = prepInt(request.QueryString("addItemID"))
addPromoItemID = prepInt(request.QueryString("addPromoItemID"))
addItemPrice = prepInt(request.QueryString("addItemPrice"))
cartSessionID = prepInt(request.QueryString("cartSessionID"))
declinedOrderID = prepInt(request.QueryString("declinedOrderID"))
brandid = prepInt(request.QueryString("brandid"))
modelid = prepInt(request.QueryString("modelid"))
categoryid = prepInt(request.QueryString("categoryid"))
brandName = prepStr(request.QueryString("brandName"))
modelName = prepStr(request.QueryString("modelName"))
categoryName = prepStr(request.QueryString("categoryName"))
uType = prepStr(request.QueryString("uType"))
viewPortH = prepInt(request.QueryString("viewPortH"))
email = prepStr(request.QueryString("email"))
styleHeight = viewPortH - 100
styleMarginT = 40

select case siteid
	case 0 
		sqlPrice = "a.price_our > 0"
		itemdesc = "itemdesc"
	case 1 
		sqlPrice = "a.price_ca > 0"
		itemdesc = "itemdesc_ca"
	case 2 
		sqlPrice = "a.price_co > 0"
		itemdesc = "itemdesc_co"
	case 3 
		sqlPrice = "a.price_ps > 0"
		itemdesc = "itemdesc_ps"
	case 10 
		sqlPrice = "(a.price_our > 0 or a.price_er > 0)"
		itemdesc = "itemdesc"
end select

select case lcase(uType)
	case "getcitybyzip"
		if instr(sZip,"-") > 0 then
			zipArray = split(sZip,"-")
			useZip = zipArray(0)
		else
			useZip = sZip
		end if
		
		do while len(useZip) < 5
			useZip = "0" & useZip
		loop
		
		sql	=	"select	city, state" & vbcrlf & _
				"from	xzipcities" & vbcrlf & _
				"where	zipcode = '" & useZip & "'" & vbcrlf & _
				"order by 1"
		set rs = oConn.execute(sql)
		if rs.eof then
			response.write "no data to display"
		else
			do until rs.eof
				%>
                <div class="ffl" style="text-align:left; padding:3px 0px 3px 0px; border-bottom:1px solid #ccc;">
                    <a href="javascript:populateSCityName('<%=replace(rs("city"), "'", "\'")%>##<%=replace(rs("state"), "'", "\'")%>');"><%=rs("city")%></a>
                </div>
                <%
				rs.movenext
			loop
		end if
	case "pullorder"
		sql	=	"delete" & vbcrlf & _
				"from	shoppingcart" & vbcrlf & _
				"where	store = '" & siteid & "'" & vbcrlf & _
				"	and	adminid = '" & adminID & "'" & vbcrlf & _
				"	and (purchasedorderid is null or purchasedorderid = 0)"				
		response.write sql & "<br>"
		session("errorSQL") = sql
		oConn.execute(sql)

		sql	=	"insert into shoppingcart(store,accountid,itemid,qty,adminid,musicSkins,customcost) " & vbcrlf & _
				"select	a.store, v.accountid, b.itemid, b.quantity, '" & adminID & "', isnull(b.musicSkins, 0) musicSkins, case when b.price = 0 then 0 else null end " & vbcrlf & _
				"from	we_orders a join we_orderdetails b " & vbcrlf & _
				"	on	a.orderid = b.orderid join v_accounts v " & vbcrlf & _
				"	on	a.accountid = v.accountid and a.store = v.site_id " & vbcrlf & _
				"where	a.orderid = '" & orderid & "' "
		response.write sql & "<br>"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		response.write "added"
	case "getshipping"
		totalItemWeight = 1
		totalQTY = cint(0)
		phoneInOrder = 0

		sql = 	"select	sum(isnull(a.qty, 0)) totalQty, sum(isnull(b.itemWeight, 1)) totalItemWeight, max(isnull(case when b.typeid = 16 then 1 else 0 end, 0)) phoneInOrder" & vbcrlf & _
				"from	shoppingcart a with (nolock) join we_items b with (nolock)" & vbcrlf & _
				"	on	a.itemid = b.itemid " & vbcrlf & _
				"where	a.store = '" & siteid & "' and a.adminID = '" & adminID & "' and (a.purchasedorderid is null or a.purchasedorderid = 0) and a.customcost is null"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		if not rs.eof then 
			if not isnull(rs("totalItemWeight")) then totalItemWeight = rs("totalItemWeight")
			if not isnull(rs("totalQty")) then totalQTY = cint(rs("totalQty"))
			if not isnull(rs("phoneInOrder")) then phoneInOrder = rs("phoneInOrder")
		end if
		if totalItemWeight / 16 < 1 then
			totalItemWeight = 1
		else
			totalItemWeight = totalItemWeight / 16
		end if
				
		international = 0
		if instr("|AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT|", "|" & sState & "|") > 0 then international = 1
		if not isnumeric(useZip) then international = 1
'		shippingUPS = UPScode_all(siteid, useZip, totalItemWeight, curShipID, totalQTY, 0,0)
		sql = 	"select	site_id, shippingID, international, includePhones, hidelive, cost, isnull(addlCostPerQty, 0) addlCostPerQty, shippingDesc, isnull(x.shipdesc, 'First Class') shiptypeDesc" & vbcrlf & _
				"from	xsiteshippingcost a left outer join xshiptype x on a.shiptypeid = x.shiptypeid" & vbcrlf & _
				"where	site_id = '" & siteid & "' and international = '" & international & "' and hidelive = 0 and includePhones = '" & phoneInOrder & "' and cost is not null" & vbcrlf

		if instr(shippingUPS, "##") > 0 then
			arrUPS = split(shippingUPS,"##")
			strShippingID = ""
			for i=0 to ubound(arrUPS)-1
				arrItem = split(arrUPS(i),"@@")
				sql = sql & "union select '" & siteid & "' site_id, '" & arrItem(0) & "' shippingID, '" & international & "' international, " & phoneInOrder & " includePhones, 0 hidelive, " & arrItem(1) & " cost, 0 addlCostPerQty, '" & arrItem(2) & "' shippingDesc, '" & arrItem(2) & "' shiptypeDesc" & vbcrlf
			next
		end if
		sql = sql & "order by site_id, international, shippingid"
'		response.write "<pre>" & sql & "</pre>"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		if not rs.eof then
			%>
            <select id="id_cbShip" name="cbShip" onchange="updateGrandTotal();" class="lCbBox">
            <%
			do until rs.eof
				costPerQty = cdbl(rs("addlCostPerQty") * totalQTY)
				if totalQTY > 0 then
					shippingCost = cdbl(rs("cost")) + costPerQty
				else
					shippingCost = 0
				end if
			%>
				<option value="<%=rs("shippingID")%>" <%if curShipID = rs("shippingID") then%> selected <%end if%>>
					<%=rs("shippingDesc")%>&nbsp;-&nbsp;$<%=formatnumber(shippingCost,2)%>
				</option>
				<%
				rs.movenext
			loop
			rs.movefirst
			%>
            </select>
            <%
			do until rs.eof
				costPerQty = cdbl(rs("addlCostPerQty") * totalQTY)
				if totalQTY > 0 then
					shippingCost = cdbl(rs("cost")) + costPerQty
				else
					shippingCost = 0
				end if				
				shiptypeDesc = rs("shiptypeDesc")
				%>
                <input type="hidden" id="id_shiptype_<%=rs("shippingID")%>" value="<%=shippingCost%>" />
                <input type="hidden" id="id_shiptypedesc_<%=rs("shippingID")%>" value="<%=shiptypeDesc%>" />
                <%
				rs.movenext
			loop
		else
			%>
            <select id="id_cbShip" name="cbShip" class="mCbBox">
				<option value="-1">N/A</option>
			</select>
            <%
		end if
	case "orderhistory"
		if trim(email) <> "" then
			sql	=	"select	top 60 b.store, s.shortDesc, s.color, b.orderid, v.accountid, convert(varchar(10), b.orderdatetime, 20) orderdate" & vbcrlf & _
					"	,	isnull(o.orderdesc, 'Credit Card') orderType" & vbcrlf & _
					"	,	b.orderGrandTotal, b.shipType, case when b.approved = 1 then 'Yes' else 'No' end approved" & vbcrlf & _
					"	, 	case when b.cancelled = 1 then 'Yes' else 'No' end cancelled" & vbcrlf & _
					"from	v_accounts v with (nolock) join we_orders b with (nolock)" & vbcrlf & _
					"	on	v.accountID = b.accountID and v.site_id = b.store join xstore s" & vbcrlf & _
					"	on	b.store = s.site_id	left outer join xordertype o" & vbcrlf & _
					"	on	b.extOrderType = o.typeid" & vbcrlf & _
					"where	v.email = '" & email & "'" & vbcrlf & _
					"order by b.orderid desc"
			set rs = oConn.execute(sql)
			if rs.eof then
				response.write "<div style=""display:block; padding-top:30px; color:#666; font-weight:bold;"" align=""center"">No Order History Found</div>"
			else
				lap = 0
				do until rs.eof
					lap = lap + 1
					if (lap mod 2) = 0 then
						className = "row1"			
					else
						className = "row2"
					end if
					%>
				    <div class="<%=className%>">
                        <div class="store" style="color:<%=rs("color")%>;"><%=rs("shortDesc")%></div>
                        <div class="orderid">
                        	<div class="fl" style="cursor:pointer; text-decoration:underline;" onclick="printinvoice(<%=rs("orderid")%>,<%=rs("accountid")%>)"><%=rs("orderid")%></div>
                            <div class="btnSearch" style="margin-top:0px;" onclick="pullOrder('<%=rs("store")%>','<%=rs("orderid")%>','<%=email%>')"></div>
						</div>
                        <div class="orderdate"><%=rs("orderdate")%></div>
                        <div class="ordertotal"><%=formatcurrency(rs("orderGrandTotal"))%></div>
                        <div class="approved"><%=rs("approved")%>&nbsp;/&nbsp;<%=rs("cancelled")%></div>
                    </div>                    
					<%
					rs.movenext
				loop
			end if
		else
			response.write "<div style=""display:block; padding-top:30px; color:#666; font-weight:bold;"" align=""center"">No Order History Found</div>"
		end if
	case "customerdata"
		if trim(email) <> "" then
			sql = 	"select	top 1 a.*" & vbcrlf & _
					"from	v_accounts a join we_orders b " & vbcrlf & _
					"	on	a.accountID = b.accountID and a.site_id = b.store" & vbcrlf & _
					"where	a.email = '" & email & "'" & vbcrlf & _
					"	and	b.store = '" & siteid & "'" & vbcrlf & _
					"order by a.accountid desc" 
			session("errorSQL") = sql
			set rs = Server.CreateObject("ADODB.Recordset")
			rs.open sql, oConn, 0, 1
		
			if not rs.eof then
				response.Write(rs("fname") & "@@" & rs("lname") & "@@" & rs("sAddress1") & "@@" & rs("sAddress2") & "@@" & rs("sCity") & "@@" & rs("sState") & "@@" & rs("sZip") & "@@" & rs("phone") & "@@" & rs("bAddress1") & "@@" & rs("bAddress2") & "@@" & rs("bCity") & "@@" & rs("bState") & "@@" & rs("bZip") & "@@" & rs("accountID"))
				if isnull(rs("parentID")) then
					response.Write("@@" & rs("accountID"))
				else		
					response.Write("@@" & rs("parentID"))
				end if
			else
				sql = 	"select	top 1 a.*" & vbcrlf & _
						"from	v_accounts a join we_orders b " & vbcrlf & _
						"	on	a.accountID = b.accountID and a.site_id = b.store" & vbcrlf & _
						"where	a.email = '" & email & "'" & vbcrlf & _
						"order by b.orderid desc"
				session("errorSQL") = sql
				set rs = Server.CreateObject("ADODB.Recordset")
				rs.open sql, oConn, 0, 1
				if not rs.eof then
					response.Write(rs("fname") & "@@" & rs("lname") & "@@" & rs("sAddress1") & "@@" & rs("sAddress2") & "@@" & rs("sCity") & "@@" & rs("sState") & "@@" & rs("sZip") & "@@" & rs("phone") & "@@" & rs("bAddress1") & "@@" & rs("bAddress2") & "@@" & rs("bCity") & "@@" & rs("bState") & "@@" & rs("bZip") & "@@" & rs("accountID"))
					if isnull(rs("parentID")) then
						response.Write("@@" & rs("accountID"))
					else		
						response.Write("@@" & rs("parentID"))
					end if
				else
					response.write "No customer order history found"
				end if
			end if
		else
			response.Write "No customer order history found"
		end if
	case "grabcart"
		sql	=	"update	shoppingcart" & vbcrlf & _
				"set	adminid = '" & adminID & "'" & vbcrlf & _
				"where	store = '" & siteid & "'" & vbcrlf & _
				"	and	sessionid = '" & cartSessionID & "'" & vbcrlf & _
				"	and (purchasedorderid is null or purchasedorderid = 0)"
		session("errorSQL") = sql
		oConn.execute(sql)
		response.write "done"
	case "grabdeclinedorder"
		sql	=	"update	a" & vbcrlf & _
				"set	adminid = '" & adminID & "'" & vbcrlf & _
				"from	shoppingcart a join we_orders b" & vbcrlf & _
				"	on	a.sessionid = b.sessionid and a.store = b.store" & vbcrlf & _
				"where	a.store = '" & siteid & "'" & vbcrlf & _
				"	and	b.orderid = '" & declinedOrderID & "'" & vbcrlf & _
				"	and (a.purchasedorderid is null or a.purchasedorderid = 0)"
'		response.write sql & "<br><br>"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql	=	"select	b.email" & vbcrlf & _
				"from	we_orders a join v_accounts b" & vbcrlf & _
				"	on	a.store = b.site_id and a.accountid = b.accountid" & vbcrlf & _
				"where	a.orderid = '" & declinedOrderID & "'"
'		response.write sql & "<br><br>"
		set rs = oConn.execute(sql)
		if not rs.eof then
			response.write rs("email")
		else
			response.write "added"
		end if
	case "updateitem"
		if addQty <= 0 then
			sql	=	"delete" & vbcrlf & _
					"from	shoppingcart" & vbcrlf & _
					"where	store = '" & siteid & "'" & vbcrlf & _
					"	and	adminid = '" & adminID & "'" & vbcrlf & _
					"	and	itemid = '" & addItemID & "'" & vbcrlf & _
					"	and (purchasedorderid is null or purchasedorderid = 0)"
			session("errorSQL") = sql
			oConn.execute(sql)
			
			sql = 	"select	count(*) totalCount, isnull(sum(case when customcost = 0 then 1 else 0 end), 0) freeItemCount" & vbcrlf & _
					"from	shoppingCart " & vbcrlf & _
					"where	store = " & siteid & " and adminID = " & adminID & " and purchasedOrderID IS NULL"
			set rs = oConn.execute(sql)
			
			if rs("totalCount") = 1 and rs("freeItemCount") = 1 then
				sql = "delete from ShoppingCart where store = " & siteid & " and adminID = '" & adminID & "'"
				oConn.execute SQL
			end if
		else
			sql	=	"update	shoppingcart" & vbcrlf & _
					"set	qty = '" & addQty & "'" & vbcrlf & _
					"where	store = '" & siteid & "'" & vbcrlf & _
					"	and	adminid = '" & adminID & "'" & vbcrlf & _
					"	and	itemid = '" & addItemID & "'" & vbcrlf & _
					"	and (purchasedorderid is null or purchasedorderid = 0)"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		response.write "done"
	case "deleteitem"
		sql	=	"delete" & vbcrlf & _
				"from	shoppingcart" & vbcrlf & _
				"where	store = '" & siteid & "'" & vbcrlf & _
				"	and	adminid = '" & adminID & "'" & vbcrlf & _
				"	and	itemid = '" & addItemID & "'" & vbcrlf & _
				"	and (purchasedorderid is null or purchasedorderid = 0)"
		session("errorSQL") = sql
		oConn.execute(sql)

		sql = 	"select	count(*) totalCount, isnull(sum(case when customcost = 0 then 1 else 0 end), 0) freeItemCount" & vbcrlf & _
				"from	shoppingCart " & vbcrlf & _
				"where	store = " & siteid & " and adminID = " & adminID & " and purchasedOrderID IS NULL"
		set rs = oConn.execute(sql)
		
		if rs("totalCount") = 1 and rs("freeItemCount") = 1 then
			sql = "delete from ShoppingCart where store = " & siteid & " and adminID = '" & adminID & "'"
			oConn.execute SQL
		end if
		response.write "done"
	case "additem"
		if addItemPrice = 0 then addItemPrice = null
		if addQty < 1 then addQty = 1
		if addQty > 999 then addQty = 999
		if addItemID > 0 then
			SQL = 	"select c.alwaysInStock, (select top 1 inv_qty from we_items where partNumber = a.partNumber and master = 1) as inv_qty, b.id, b.itemID, b.qty " & vbcrlf & _
					"from we_items a with (nolock) " & vbcrlf & _
						"left join ShoppingCart b with (nolock) on a.itemID = b.itemID and adminid = '" & adminID & "' and b.lockQty = 0 and (purchasedOrderID IS NULL OR purchasedOrderID = 0) and b.store = " & siteid & vbcrlf & _
						"left join we_pnDetails c with (nolock) on a.partNumber = c.partNumber " & vbcrlf & _
					"where a.itemID = " & addItemID & " and a.hideLive = 0"
'			response.write sql & "<br><br>"
			session("errorSQL") = SQL
			set RS = oConn.execute(sql)
			if not RS.eof then
				if isnull(rs("alwaysInStock")) then alwaysInStock = False else alwaysInStock = rs("alwaysInStock")
				if rs("inv_qty") > 0 or alwaysInStock then
					if not isnull(rs("id")) then
						if not isnull(addItemPrice) then
							SQL = "UPDATE ShoppingCart SET qty = qty + " & addQty & ", customCost = " & addItemPrice & " WHERE id = '" & RS("id") & "'"
						else
							SQL = "UPDATE ShoppingCart SET qty = qty + " & addQty & ", customCost = null WHERE id = '" & RS("id") & "'"
						end if 
					else
						if isnull(addItemPrice) then addItemPrice = "null"
						
						SQL = "INSERT INTO ShoppingCart (store,sessionID,accountID,itemID,qty,dateEntd,customCost,ipAddress,adminid) VALUES (" & siteid & ","
						SQL = SQL & "null, "
						SQL = SQL & "null, "
						SQL = SQL & "'" & addItemID & "', "
						SQL = SQL & "'" & addQty & "', "
						SQL = SQL & "'" & now & "', "
						SQL = SQL & addItemPrice & ",'" & request.ServerVariables("REMOTE_ADDR") & "','" & adminID & "')"		
					end if
				else
					sql = "delete from ShoppingCart where itemID = " & addItemID & " and (purchasedOrderID IS NULL OR purchasedOrderID = 0) and	adminid = '" & adminID & "'"
				end if
'				response.write sql & "<br><br>"
				session("errorSQL") = sql
				oConn.execute SQL
				
				if instr(sql,"INSERT INTO ShoppingCart") > 0 and addPromoItemID > 0 then
					sql = "delete from ShoppingCart where store = " & siteid & " and adminID = '" & adminID & "' and customCost = 0"
					oConn.execute SQL
					
					sql =	"if (select inv_qty from we_Items where itemID = " & addPromoItemID & ") > 0 " &_
								"if (select count(*) from shoppingCart where store = " & siteid & " and adminID = '" & adminID & "' and itemID = " & addPromoItemID & " and purchasedOrderID is null) = 0 " &_
									"insert into ShoppingCart " &_
										"(store,sessionID,itemID,qty,customCost,lockQty,ipAddress,adminID) " &_
										"values " &_
										"(" & siteid & ",null," & addPromoItemID & ",1,0,1,'" & request.ServerVariables("REMOTE_ADDR") & "','" & adminID & "')"
					session("errorSQL") = sql
					oConn.execute SQL
				end if
			end if
			RS.close
			set RS = nothing
		end if
		response.write "done"
	case "cartitems"
		freeItemTotal = cdbl(0)
		sql	=	"select	a.id, c.brandname, d.modelname, a.musicskins, a.customcost customcost, a.lockqty, a.itemid, isnull(a.qty, 0) qty, b.modelid, b.typeid, b.partnumber" & vbcrlf & _
				"	, 	b." & itemdesc & " itemdesc, b.itempic, b.itempic defaultImg" & vbcrlf & _
				"	,	b.price_our, b.price_ca, b.price_co, b.price_ps, price_er price_tm, b.price_retail, b.condition, b.itemweight, b.nodiscount, t.typeid, t.typename " & vbcrlf & _
				"from	shoppingcart a join we_items b " & vbcrlf & _
				"	on	a.itemid=b.itemid left outer join we_brands c " & vbcrlf & _
				"	on	b.brandid = c.brandid left outer join we_models d " & vbcrlf & _
				"	on	b.modelid = d.modelid left outer join we_types t" & vbcrlf & _
				"	on	b.typeid = t.typeid " & vbcrlf & _
				"where	a.musicSkins = 0 and a.store = '" & siteid & "' and a.adminID = '" & adminID & "' and (a.purchasedorderid is null or a.purchasedorderid = 0)" & vbcrlf & _
				"order by id desc"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
'		response.write "<pre>" & sql & "</pre>"
		nSubTotal = cdbl(0)
		nDiscountTotal = cdbl(0)
		nTotalQty = cint(0)
		PhonePurchased = 0
		if not rs.eof then
			do until rs.eof
				select case siteid
					case 0
						productLink = "http://www.wirelessemporium.com/p-" & rs("itemid") & "-" & formatSEO(rs("itemdesc"))
						itemDesc = 	"<a href=""" & productLink & """ style=""color:#333; font-size:14px; text-decoration:none;"" target=""_blank"">" & rs("itemdesc") & "</a>"
						orig_price = cdbl(rs("price_our"))
					case 1
						productLink = "http://www.cellphoneaccents.com/p-" & rs("itemid") & "-" & formatSEO(rs("itemdesc")) & ".html"
						itemDesc = 	"<a href=""" & productLink & """ style=""color:#333; font-size:14px; text-decoration:none;"" target=""_blank"">" & rs("itemdesc") & "</a>"
						orig_price = cdbl(rs("price_ca"))
					case 2
						productLink = "http://www.cellularoutfitter.com/p-" & rs("itemid") & "-" & formatSEO(rs("itemdesc")) & ".html"
						itemDesc = 	"<a href=""" & productLink & """ style=""color:#333; font-size:14px; text-decoration:none;"" target=""_blank"">" & rs("itemdesc") & "</a>"					
						orig_price = cdbl(rs("price_co"))
					case 3
						if rs("typeid") = 16 then
							productLink = "http://www.phonesale.com/" & formatSEO(rs("brandname")) & "/cell-phones/" & formatSEO(rs("itemdesc")) & "-p-" & rs("itemid")+300001 & ".html"
						else
							productLink = "http://www.phonesale.com/accessories/" & formatSEO(rs("typename")) & "/" & formatSEO(rs("itemdesc")) & "-p-" & rs("itemid")+300001 & ".html"
						end if
						itemDesc = 	"<a href=""" & productLink & """ style=""color:#333; font-size:14px; text-decoration:none;"" target=""_blank"">" & rs("itemdesc") & "</a>"
						orig_price = cdbl(rs("price_ps"))
					case 10
						productLink = "http://www.tabletmall.com/" & formatSEO(rs("itemdesc")) & "-p-" & rs("itemid")
						itemDesc = 	"<a href=""" & productLink & """ style=""color:#333; font-size:14px; text-decoration:none;"" target=""_blank"">" & rs("itemdesc") & "</a>"
						orig_price = cdbl(getPricingTM(rs("price_tm"), rs("price_our")))
				end select
				customprice = rs("customcost")
				usePrice = orig_price
				qty = prepInt(rs("qty"))
				if not isnull(customprice) then usePrice = cdbl(customprice)
				if rs("typeid") = 16 then PhonePurchased = 1

				freeItemTotal = freeItemTotal + ((orig_price * qty) - (usePrice * qty))
				rowItemTotal = qty * usePrice
				nSubTotal = nSubTotal + rowItemTotal
				if not isnull(customprice) then 
					nTotalQty = nTotalQty + 0
				else
					nTotalQty = nTotalQty + qty
				end if
				useImagePath = getUseImage(rs("itempic"),cint(rs("musicSkins")),rs("defaultImg"))
				%>
				<div class="ffl padding-v">
					<table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
						<tr>
							<td class="itemDesc">
								<table border="0" cellpadding="5" cellspacing="0" align="left" width="100%">
									<tr>
										<td align="left" valign="top" style="width:70px;"><div class="roundBorder padding5"><img src="<%=useImagePath%>" border="0" width="60" height="60" /></div></td>
										<td align="left" valign="top"><%=itemDesc%></td>
									</tr>
								</table>
							</td>
							<td class="qty">
								<div class="fl">
                                	<input id="id_curqty_<%=rs("itemid")%>" class="xxsField" type="text" name="txtQty" value="<%=qty%>" <%if not isnull(rs("customcost")) then%>readonly="readonly"<%end if%>/>
                                    <input class="cartLineItems" type="hidden" value="<%=rs("itemid")%>" />
								</div>
								<div class="fl padding-h">
									<a class="lnk" href="javascript:updateItem('<%=rs("itemid")%>')">Update</a><br />
									<a class="lnk" href="javascript:deleteItem('<%=rs("itemid")%>')">Remove</a>
								</div>
							</td>
							<td class="subtotal">
                            	<%if usePrice = 0 then%>
                                <div style="padding:10px 0px 0px 0px;">
									FREE
								</div>
                                <%else%>
									<%=formatcurrency(rowItemTotal, 2)%>
                                <%end if%>
							</td>
						</tr>
					</table>        
				</div>
				<%
				rs.movenext
			loop
			
			dim promoMin, promoPercent, cTypeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
			if sPromoCode <> "" then
				couponid = 0
				SQL = "select * from v_coupons where site_id = '" & siteid & "' and promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
				set RS = Server.CreateObject("ADODB.Recordset")
				RS.open SQL, oConn, 0, 1
				if not RS.eof then
					couponid = RS("couponid")
					promoMin = RS("promoMin")
					promoPercent = prepInt(RS("promoPercent"))
					cTypeID = RS("typeID")
					excludeBluetooth = RS("excludeBluetooth")
					BOGO = RS("BOGO")
					couponDesc = RS("couponDesc")
					FreeItemPartNumber = RS("FreeItemPartNumber")
					setValue = RS("setValue")
					oneTime = RS("oneTime")
				else
					sPromoCode = ""
				end if
			end if
			%>
			<!--#include virtual="/includes/admin/inc_adminPromoFunctions.asp"-->        
			<input type="hidden" name="nTotalQuantity" value="<%=round(nTotalQty,0)%>" />
			<input type="hidden" name="nSubTotal" value="<%=round(nSubTotal,2)%>" />
			<input type="hidden" name="nFreeItemTotal" value="<%=round(freeItemTotal,2)%>" />            
			<input type="hidden" name="nDiscountTotal" value="<%=round(nDiscountTotal,2)%>" />
			<%
			if freeItemTotal > 0 then
			%>
			<div class="ffl padding-v">
				<table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
					<tr>
						<td class="itemDesc" style="text-align:right;"><b>Free Item Discount: </b></td>
						<td class="qty">&nbsp;</td>
						<td class="subtotal">
							<div style="color:#9a0002; padding-right:8px; font-weight:bold;"><%=formatCurrency(freeItemTotal * -1)%></div>
						</td>
					</tr>
				</table>        
			</div>
			<%	
			end if
	
			if nDiscountTotal > 0 then
			%>
			<div class="ffl padding-v">
				<table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
					<tr>
						<td class="itemDesc" style="text-align:right;"><b>Promotions: </b><%=couponDesc%></td>
						<td class="qty">&nbsp;</td>
						<td class="subtotal">
							<div style="color:#9a0002; padding-right:8px; font-weight:bold;"><%=formatCurrency(nDiscountTotal * -1)%></div>
						</td>
					</tr>
				</table>        
			</div>
			<%	
			end if
		else
		%>
        <div class="ffl" style="padding:20px 0px 20px 0px;">Cart is currently empty</div>
        <input type="hidden" name="nTotalQuantity" value="0" />
        <input type="hidden" name="nSubTotal" value="0" />
        <input type="hidden" name="nFreeItemTotal" value="0" />            
        <input type="hidden" name="nDiscountTotal" value="0" />        
        <%
		end if
		%>
        <input type="hidden" name="nPromo" value="<%=couponid%>">
        <input type="hidden" name="nCATax" value="" />
        <input type="hidden" name="nShippingTotal" value="" />
        <input type="hidden" name="nShippingDiscountTotal" value="" />
        <input type="hidden" name="shiptypeID" value="" />
        <input type="hidden" name="strShipType" value="" />
        <input type="hidden" name="nGrandTotal" value="" />
        <input type="hidden" name="couponcode" value="<%=sPromoCode%>" />
        <input type="hidden" name="coupondesc" value="<%=couponDesc%>" />
        <input type="hidden" name="adminID" value="<%=adminID%>" />
       <%
	case "strands"
		if searchItemID > 0 then
			dfilter = ""
			sql	=	"select	modelname" & vbcrlf & _
					"from	we_models with (nolock)" & vbcrlf & _
					"where	modelid in ( select modelid from we_items with (nolock) where itemid = " & searchItemID & ")" & vbcrlf & _
					"	and	modelname not like 'all model%' and modelname not like 'universal%'"
			set rs = oConn.execute(sql)
			if not rs.eof then
				dfilter = "dfilter=""modelname::" & rs("modelname") & """"
			end if
			
		%>
		<div class="w100" style="overflow:auto; height:<%=(styleHeight-160)%>px;">
        	<div class="clear">&nbsp;</div>
            <div class="tb w100 margin-v padding-v" style="font-size:18px;">Most Popular Products:</div>
            <div id="strandsResult1" class="w100 strandsRecs" tpl="CTGY-1" item="<%=searchItemID%>" <%=dfilter%>>
                <div style="width:100%; text-align:center;"><img src="//d2brc35ftammet.cloudfront.net/img/loading-grey.gif" /></div>
            </div>
        	<div class="clear">&nbsp;</div>
            <div class="tb w100 margin-v padding-v" style="font-size:18px;">Bestsellers This Week:</div>
            <div id="strandsResult2" class="w100 strandsRecs" tpl="CTGY-2" item="<%=searchItemID%>" <%=dfilter%>>
                <div style="width:100%; text-align:center;"><img src="//d2brc35ftammet.cloudfront.net/img/loading-grey.gif" /></div>
            </div>
        	<div class="clear">&nbsp;</div>
            <div class="tb w100 margin-v padding-v" style="font-size:18px;">Recommendations For This Item:</div>
            <div id="strandsResult3" class="w100 strandsRecs" tpl="CTGY-3" item="<%=searchItemID%>" <%=dfilter%>>
                <div style="width:100%; text-align:center;"><img src="//d2brc35ftammet.cloudfront.net/img/loading-grey.gif" /></div>
            </div>
        </div>
        <%
		else
			brandName = replace(replace(brandName, "[[", "("), "]]", ")")
			modelName = replace(replace(modelName, "[[", "("), "]]", ")")
			categoryName = replace(replace(categoryName, "[[", "("), "]]", ")")
			
			if categoryid = 5 then	
				sql	=	"select	isnull(handsfreetypes, '') handsfreetypes" & vbcrlf & _
						"from	we_models with (nolock)" & vbcrlf & _
						"where	modelid = " & modelid
				set rs = oConn.execute(sql)
				if not rs.eof then
					arrHandsfreeTypes = split(rs("handsfreetypes"), ",")
					for each hfType in arrHandsfreeTypes
						if strHandsfree = "" then 
							strHandsfree = "handsfreetype::" & hfType
						else
							strHandsfree = strHandsfree & ";;" & hfType
						end if
					next
				end if
				strFilter = strHandsfree
			elseif categoryid = 2 or categoryid = 4 or categoryid = 8 or categoryid = 12 or categoryid = 13 or categoryid = 14 or categoryid = 21 then
				strFilter = "brandname::" & brandName & ";;Universal_._category::" & categoryName
			elseif categoryid = 16 then
				strFilter = "brandname::" & brandName & "_._category::" & categoryName		
			else
				strFilter = "modelname::" & modelName & "_._category::" & categoryName
			end if
		end if
		%>
        <div class="w100" style="overflow:auto; height:<%=(styleHeight-160)%>px;">
        	<div class="clear">&nbsp;</div>
            <div class="tb w100 margin-v padding-v" style="font-size:18px;">Most Popular Products:</div>
            <div id="strandsResult1" class="w100 strandsRecs" tpl="CTGY-1" dfilter="<%=strFilter%>">
                <div style="width:100%; text-align:center;"><img src="//d2brc35ftammet.cloudfront.net/img/loading-grey.gif" /></div>
            </div>
        	<div class="clear">&nbsp;</div>
            <div class="tb w100 margin-v padding-v" style="font-size:18px;">Bestsellers This Week:</div>
            <div id="strandsResult2" class="w100 strandsRecs" tpl="CTGY-2" dfilter="<%=strFilter%>">
                <div style="width:100%; text-align:center;"><img src="//d2brc35ftammet.cloudfront.net/img/loading-grey.gif" /></div>
            </div>
        	<div class="clear">&nbsp;</div>
            <div class="tb w100 margin-v padding-v" style="font-size:18px;">Recommendations Just For Your Phone:</div>
            <div id="strandsResult3" class="w100 strandsRecs" tpl="CTGY-3" dfilter="modelname::<%=modelName%>">
                <div style="width:100%; text-align:center;"><img src="//d2brc35ftammet.cloudfront.net/img/loading-grey.gif" /></div>
            </div>
        </div>
        <%
	case "nextopia"
		%>
        <div class="w100" style="overflow:auto; height:<%=(styleHeight-160)%>px;">
            <div id="sideNav" style="float:left; width:200px;"></div>
            <div id="mainContent" style="float:left; width:748px; padding-left:10px;">
                <div style="padding:50px 0px 50px 250px;"><img src="//d2brc35ftammet.cloudfront.net/img/loading-grey.gif" /></div>
            </div>
        </div>
        <%
	case "onbrand"
		sql = "select modelid, modelname from we_models with (nolock) where brandid = " & brandid & " order by 2"
		set rsModel = oConn.execute(sql)
		%>
        <select id="id_cbModel" name="cbModel" class="mCbBox" onchange="doSearch(1,2)">
            <option value="0">Select Phone Model</option>
            <%
            do until rsModel.eof
            %>
            <option value="<%=rsModel("modelid")%>"><%=rsModel("modelname")%></option>
            <%
                rsModel.movenext
            loop
            %>
        </select>
        <%
	case "searchitems"
		sql = "select typeid, typename from we_types with (nolock) where typeid not in (9,15,19,20,23,24) order by 2"
		set rsType = oConn.execute(sql)
		sql = "select brandid, brandname from we_brands with (nolock) where brandid not in (12) order by 2"
		set rsBrand = oConn.execute(sql)
		sql = "select modelid, modelname from we_models with (nolock) where brandid not in (12) order by 2"
		set rsModel = oConn.execute(sql)
	%>
    <div class="phoneorderLightbox" style="height:<%=styleHeight%>px; margin-top:<%=styleMarginT%>px;">
    	<div id="errMsg" class="ffl"></div>
    	<div class="ffl">
        	<div class="fl">Item Recommendations & Search</div>
        	<div class="fr margin-h" style="cursor:pointer;" onclick="closeLightBox()">X</div>
        	<div class="fr" style="cursor:pointer;" onclick="closeLightBox()">Close</div>
        </div>
    	<div class="ffl border-b">&nbsp;</div>
        <div class="ffl margin-v padding-v">
        	<div class="fl lbl">Brand:&nbsp;</div>
        	<div class="fl" id="cbBrandBox">
            	<select id="id_cbBrand" name="cbBrand" class="mCbBox" onchange="doSearch(1,1)">
                    <option value="0">Select Phone Brand</option>
					<%
                    do until rsBrand.eof
                    %>
                    <option value="<%=rsBrand("brandid")%>"><%=rsBrand("brandname")%></option>
                    <%
                        rsBrand.movenext
                    loop
                    %>
                </select>
            </div>
        	<div class="fl lbl" style="padding-left:95px;">Model:&nbsp;</div>
        	<div class="fl" id="cbModelBox">
            	<select id="id_cbModel" name="cbModel" class="mCbBox" onchange="doSearch(1,2)">
                	<option value="0">Select Phone Model</option>
                    <%
					do until rsModel.eof
					%>
					<option value="<%=rsModel("modelid")%>"><%=rsModel("modelname")%></option>
					<%
						rsModel.movenext
					loop
					%>
                </select>
            </div>
        	<div class="fl lbl" style="padding-left:95px;">Category:&nbsp;</div>
        	<div class="fl" id="cbCategoryBox">
            	<select id="id_cbCategory" name="cbCategory" class="mCbBox" onchange="doSearch(1,3)">
                	<option value="0">Select Category</option>
                    <%
					do until rsType.eof
					%>
					<option value="<%=rsType("typeid")%>"><%=rsType("typename")%></option>
					<%
						rsType.movenext
					loop
					%>
                </select>
            </div>
        </div>
        <form name="topSearchForm" method="get" action="/admin/phoneorder2.asp">
    	<div class="ffl">
        	<div class="fl lbl">Search:&nbsp;</div>
        	<div class="fl"><input id="nxtSearchInput" style="position:relative;" type="text" class="xlField" name="search" value="Galaxy S5 Black Case" onfocus="onField(this)" onkeydown="if (event.keyCode==13) doSearch(2,1);" /></div>
            <div id="nxtBtnSearch" class="btnSearch" onclick="doSearch(2,1)"></div>
            <div class="fr">
			    <div class="fl lbl">ItemID:&nbsp;</div>
                <div class="fl"><input id="nxtItemID" type="text" class="xsField" value="ItemID" onfocus="onField(this)" onkeydown="if (event.keyCode==13) doSearch(3,document.getElementById('nxtItemID').value)" /></div>
                <div class="btnSearch" onclick="doSearch(3,document.getElementById('nxtItemID').value)"></div>
            </div>
        </div>
    	<div class="ffl border-b">&nbsp;</div>
    	<div class="ffl margin-v padding-v" id="searchResult"></div>
        </form>
	</div>
    <%
end select

call CloseConn(oConn)

function getUseImage(pItemPic, pMusicSkins, pMsDefaultImg)
	useImgPath = "/productpics/thumb/imagena.jpg"
	if pMusicSkins = 1 then
		itemImgPath = Server.MapPath("/productpics/musicSkins/musicSkinsLarge") & "\" & pItemPic
		itemImgPath2 = Server.MapPath("/productpics/musicSkins/musicSkinsSmall") & "\" & pItemPic
		useImgPath = "/productpics/musicSkins/musicSkinsSmall/" & pItemPic
	else
		itemImgPath = Server.MapPath("/productpics/thumb") & "\" & pItemPic
		itemImgPath2 = Server.MapPath("/productpics/thumb") & "\" & pItemPic
		useImgPath = "/productpics/thumb/" & pItemPic
	end if
	
	if not fsThumb.FileExists(itemImgPath) then
		if pMusicSkins = 1 then
			if isnull(pMsDefaultImg) then
				pItemPic = "imagena.jpg"
				useImgPath = "/productpics/thumb/imagena.jpg"
			else
				skipRest = 0
				if fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & pMsDefaultImg)) then
					setDefault = pMsDefaultImg
				elseif fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & replace(pMsDefaultImg," ","-"))) then
					setDefault = replace(pMsDefaultImg," ","-")
				else
					setDefault = pMsDefaultImg
				end if
				if skipRest = 0 then
					chkPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & setDefault)
					if not fsThumb.FileExists(chkPath) then
						musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)
						if fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)) then
							musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)
						elseif fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & replace(setDefault," ","-"))) then
							musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & replace(setDefault," ","-"))
						else
							pItemPic = "imagena.jpg"
							useImgPath = "/productpics/thumb/imagena.jpg"
							skipRest = 1
						end if
						if skipRest = 0 then
							session("errorSQL") = musicSkinsDefaultPath
							Jpeg.Open musicSkinsDefaultPath
							Jpeg.Width = 100
							Jpeg.Height = Jpeg.OriginalHeight * 100 / Jpeg.OriginalWidth
							Jpeg.Save chkPath
						end if
					end if
					if skipRest = 0 then
						pItemPic = setDefault
						useImgPath = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & setDefault
					end if
				end if
			end if
		else
			pItemPic = "imagena.jpg"
			useImgPath = "/productpics/thumb/imagena.jpg"
		end if
	elseif pMusicSkins = 1 and not fsThumb.FileExists(itemImgPath2) then
		Jpeg.Open itemImgPath
		if Jpeg.OriginalHeight > Jpeg.OriginalWidth then
			Jpeg.Height = 100
			Jpeg.Width = Jpeg.OriginalWidth * 100 / Jpeg.OriginalHeight
		else
			Jpeg.Width = 100
			Jpeg.Height = Jpeg.OriginalHeight * 100 / Jpeg.OriginalWidth
		end if
		Jpeg.Save itemImgPath2
	end if		
	getUseImage = useImgPath
end function
%>