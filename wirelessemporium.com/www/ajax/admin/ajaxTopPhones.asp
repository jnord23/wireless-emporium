<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	
	call getDBConn(oConn)
	
	set fso = CreateObject("Scripting.FileSystemObject")
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	dim listBy : listBy = prepStr(request.QueryString("listBy"))
	dim sDate : sDate = prepStr(request.QueryString("sDate"))
	dim eDate : eDate = prepStr(request.QueryString("eDate"))
	dim numOfPhones : numOfPhones = prepInt(request.QueryString("numOfPhones"))
	
	if numOfPhones = 0 then numOfPhones = 50
	
	if listBy = "allTime" then
		sql = 	"select top " & numOfPhones & " a.itemDesc, a.itemPic, a.cogs, b.sales from we_Items a " &_
					"left join (" &_
						"select sum(quantity) as sales, partNumber " &_
						"from we_orders ia " &_
							"left join we_orderDetails ib on ia.orderID = ib.orderID " &_
						"where ia.approved = 1 and ia.cancelled is null " &_
						"group by partNumber" &_
					") b on a.partNumber = b.partNumber" &_
				" where typeID = 16 order by b.sales desc"
	elseif listBy = "new2Old" then
		sql = 	"select top " & numOfPhones & " a.itemDesc, a.itemPic, a.cogs, b.sales, c.releaseYear, c.releaseQuarter " &_
				"from we_Items a " &_
					"left join (" &_
						"select sum(quantity) as sales, partNumber " &_
						"from we_orders ia " &_
							"left join we_orderDetails ib on ia.orderID = ib.orderID " &_
						"where ia.approved = 1 and ia.cancelled is null and orderdatetime > '" & sDate & "' and orderdatetime < '" & eDate & "' " &_
						"group by partNumber" &_
					") b on a.partNumber = b.partNumber " &_
					"left join we_models c on a.modelID = c.modelID " &_
				"where typeID = 16 order by c.releaseYear desc, c.releaseQuarter desc"
	elseif listBy = "dateRange" then
		sql = 	"select top " & numOfPhones & " a.itemDesc, a.itemPic, a.cogs, b.sales from we_Items a " &_
					"left join (" &_
						"select sum(quantity) as sales, partNumber " &_
						"from we_orders ia " &_
							"left join we_orderDetails ib on ia.orderID = ib.orderID " &_
						"where ia.approved = 1 and ia.cancelled is null and orderdatetime > '" & sDate & "' and orderdatetime < '" & eDate & "' " &_
						"group by partNumber" &_
					") b on a.partNumber = b.partNumber" &_
				" where typeID = 16 order by b.sales desc"
	end if
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	phoneCnt = 0
	do while not rs.EOF
		phoneCnt = phoneCnt + 1
		itemDesc = prepStr(rs("itemDesc"))
		itemPic = "/productpics/thumb/" & prepStr(rs("itemPic"))
		sold = prepInt(rs("sales"))
		cogs = prepInt(rs("cogs"))
		if not fso.fileExists(server.MapPath("/productpics/thumb/" & prepStr(rs("itemPic")))) then itemPic = "/productpics/thumb/imagena.jpg"
%>
<div style="width:400px; height:110px; border-bottom:1px solid #333; margin-bottom:10px; margin-left:auto; margin-right:auto;">
	<div style="float:left; width:20px; text-align:center;"><%=phoneCnt%></div>
    <div style="float:left; width:150px; text-align:center; margin-left:10px;"><img src="<%=itemPic%>" border="0" /></div>
    <div style="float:left; width:200px; text-align:left; margin-left:10px;">
		<%=itemDesc%>
        <br /><br />
        Sold: <%=sold%><br />
        Cogs: <%=formatCurrency(cogs,2)%>
    </div>
</div>
<%
		rs.movenext
	loop
	call CloseConn(oConn)
%>