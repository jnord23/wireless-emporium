<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
	thisUser = Request.Cookies("admin")("username")
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%

	call getDBConn(oConn)

	brandID = prepInt(request.QueryString("brandID"))
	
	sql = "select modelID, modelName from we_models where brandID = " & brandID & " order by modelName"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
%>
<select name="modelID">
    <option value="">-- Select Model --</option>
    <%
	do while not rs.EOF
	%>
    <option value="<%=rs("modelID")%>"><%=rs("modelName")%></option>
    <%
		rs.movenext
	loop
	%>
</select>
<%
	call CloseConn(oConn)
%>