<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	
	call getDBConn(oConn)
	dim thisUser, pageTitle, header
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	dim newItemID : newItemID = prepInt(request.QueryString("newItemID"))
	dim bpItemID : bpItemID = prepInt(request.QueryString("bpItemID"))
	dim selectOp : selectOp = prepInt(request.QueryString("selectOp"))
	
	if newItemID = 0 or bpItemID = 0 then
		response.Write("Blank itemID(s)")
		call CloseConn(oConn)
		response.End()
	end if
	
	sql = "select itemID, itemDesc, itemPic, partNumber from we_items where itemID = " & newItemID & " or itemID = " & bpItemID
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	dim itemsFound : itemsFound = 0
	dim bpPn : bpPn = ""
	dim itemData1, itemData2, curItemID, curItemDesc, curItemPic, curPartNumber
	
	itemData1 = ""
	itemData2 = ""
	if not rs.EOF then
		itemsFound = 1
		curItemID = rs("itemID")
		curItemDesc = rs("itemDesc")
		curItemPic = rs("itemPic")
		curPartNumber = rs("partNumber")
		if curItemID = bpItemID then
			bpID = 1
			bpPn = curPartNumber
			itemData1 = "##ItemData##"
		end if
		itemData1 = itemData1 & curItemID & "@@" & curItemDesc & "@@" & curItemPic & "@@" & curPartNumber
		rs.movenext
		if not rs.EOF then
			itemsFound = 2
			curItemID = rs("itemID")
			curItemDesc = rs("itemDesc")
			curItemPic = rs("itemPic")
			curPartNumber = rs("partNumber")
			if curItemID = bpItemID then
				bpID = 2
				bpPn = curPartNumber
				itemData2 = "##ItemData##"
			end if
			itemData2 = itemData2 & curItemID & "@@" & curItemDesc & "@@" & curItemPic & "@@" & curPartNumber
		end if
	end if
	
	if itemsFound < 2 then
		response.Write("Invalid itemID(s)")
		call CloseConn(oConn)
		response.End()
	else
		sql = "select b.modelID, b.modelName from we_items a left join we_models b on a.modelID = b.modelID where a.partNumber = '" & bpPn & "' order by b.modelName"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		if rs.EOF then
			response.Write("Invalid Blueprint")
			call CloseConn(oConn)
			response.End()
		else
			if bpID = 1 then
				response.Write(itemData2 & itemData1 & "##ModelResults##")
			else
				response.Write(itemData1 & itemData2 & "##ModelResults##")
			end if
			do while not rs.EOF
				%>
                <input type="checkbox" name="modelIDs" value="<%=rs("modelID")%>"<% if selectOp = 1 then %> checked="checked"<% end if %> />&nbsp;<%=rs("modelName")%><br />
                <%
				rs.movenext
			loop
		end if
	end if
	
	call CloseConn(oConn)
%>