<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%

	call getDBConn(oConn)

	dim ghostProd : ghostProd = prepInt(request.QueryString("ghostProd"))
	dim deleteProd : deleteProd = prepInt(request.QueryString("deleteProd"))
	
	if ghostProd > 0 then
		sql = "update we_items set ghost = 1, hideLive = 0 where itemID = " & ghostProd
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "insert into we_adminActions (adminID,action) values(" & prepInt(session("adminID")) & ",'Ghost item: " & ghostProd & "')"
		session("errorSQL") = sql
		oConn.execute(sql)
		response.Write("product converted to ghost")
	elseif deleteProd > 0 then
		sql = "insert into we_deletedItems select * from we_items where itemID = " & deleteProd
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "delete from ItemValue where itemID = " & deleteProd
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "delete from we_items where itemID = " & deleteProd
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "insert into we_adminActions (adminID,action) values(" & prepInt(session("adminID")) & ",'Delete item: " & deleteProd & "')"
		session("errorSQL") = sql
		oConn.execute(sql)
		response.Write("Product Deleted")
	end if
	
	call CloseConn(oConn)
%>