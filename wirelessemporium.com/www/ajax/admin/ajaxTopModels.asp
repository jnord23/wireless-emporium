<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	
	call getDBConn(oConn)
	
	set fso = CreateObject("Scripting.FileSystemObject")
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	dim listBy : listBy = prepStr(request.QueryString("listBy"))
	dim sDate : sDate = prepStr(request.QueryString("sDate"))
	dim eDate : eDate = prepStr(request.QueryString("eDate"))
	dim numOfPhones : numOfPhones = prepInt(request.QueryString("numOfPhones"))
	dim breakDownBy : breakDownBy = prepStr(request.QueryString("breakDownBy"))
	dim catID : catID = prepInt(request.QueryString("catID"))
	dim subID : subID = prepInt(request.QueryString("subID"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim website : website = prepStr(request.QueryString("website"))
	
	if website = "we" then storeID = 0
	if website = "ca" then storeID = 1
	if website = "co" then storeID = 2
	if website = "ps" then storeID = 3
	
	if breakDownBy <> "" then
		'################################
		' Model Breakdown (show categories)
		'################################
		if listBy = "dateRange" then
			sql =	"select b.typeName, b.subtypeName, b.typeID, b.subtypeID, b.sales " &_
					"from we_models a " &_
						"left join ( " &_
							"select sum(quantity) as sales, typeName, ie.subtypeName, id.typeID, ie.subtypeID, modelID " &_
							"from we_orders ia " &_
								"left join we_orderDetails ib on ia.orderid = ib.orderid " &_
								"left join we_Items ic on ib.itemID = ic.itemID " &_
								"left join we_types id on ic.typeID = id.typeID " &_
								"left join we_Subtypes ie on ic.subtypeID = ie.subtypeID " &_
							"where ia.approved = 1 and ia.cancelled is null and orderdatetime > '" & sDate & "' and orderdatetime < '" & eDate & "' " &_
							"group by typeName, ie.subtypeName, id.typeID, ie.subtypeID, modelID " &_
						") b on a.modelID = b.modelID " &_
					"where a.modelID = " & breakDownBy & " " &_
					"order by typeID, b.subtypeID"
		else
			sql =	"select b.typeName, b.subtypeName, b.typeID, b.subtypeID, b.sales " &_
					"from we_models a " &_
						"left join ( " &_
							"select sum(quantity) as sales, typeName, ie.subtypeName, id.typeID, ie.subtypeID, modelID " &_
							"from we_orders ia " &_
								"left join we_orderDetails ib on ia.orderid = ib.orderid " &_
								"left join we_Items ic on ib.itemID = ic.itemID " &_
								"left join we_types id on ic.typeID = id.typeID " &_
								"left join we_Subtypes ie on ic.subtypeID = ie.subtypeID " &_
							"where ia.approved = 1 and ia.cancelled is null " &_
							"group by typeName, ie.subtypeName, id.typeID, ie.subtypeID, modelID " &_
						") b on a.modelID = b.modelID " &_
					"where a.modelID = " & breakDownBy & " " &_
					"order by typeID, b.subtypeID"
		end if
		if website <> "" then sql = replace(sql,"ia.approved = 1","ia.approved = 1 and ia.store = " & storeID)
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		bgColor = "dae7fd"
		do while not rs.EOF
			catName = rs("typeName")
			subName = prepStr(rs("subTypeName"))
			catID = rs("typeID")
			subID =  rs("subtypeID")
			catSales = formatNumber(prepInt(rs("sales")),0)
			
			if subName <> "" then useName = catName & " - " & subName else useName = catName
%>
<div style="float:left; border-bottom:1px solid #adc1e4; padding:2px 0px 2px 5px; background-color:#<%=bgColor%>; width:100%;">
	<div style="float:left; width:75%; text-align:left; cursor:pointer;" onclick="catBreakDown('breakDown_<%=breakDownBy%>_<%=catID%>_<%=subID%>',<%=breakDownBy%>,<%=catID%>,<%=subID%>)"><%=useName%></div>
	<div style="float:left; width:25%; text-align:right;"><%=catSales%></div>
	<div id="breakDown_<%=breakDownBy%>_<%=catID%>_<%=subID%>" style="float:left; width:100%;"></div>
</div>
<%
			if bgColor = "dae7fd" then bgColor = "c5dafd" else bgColor = "dae7fd"
			rs.movenext
		loop
		call CloseConn(oConn)
		response.End()
	elseif catID > 0 then
		'################################
		' Category Breakdown (show items)
		'################################
		if listBy = "dateRange" then
			sql = 	"select top 10 a.itemID, a.itemDesc, a.partNumber, a.itemPic, b.sales, c.inv_qty, d.processOrderDate as processDate, d.orderAmt, d.vendorCode as fromVendor, d.entryDate as createDate, e.processOrderDate as orderDate, e.receivedAmt, e.vendorCode as compVendor, f.totalSold " &_
					"from we_items a " &_
						"left join ( " &_
							"select sum(quantity) as sales, itemID " &_
							"from we_orders ia " &_
								"left join we_orderDetails ib on ia.orderid = ib.orderid " &_
							"where ia.approved = 1 and ia.cancelled is null and orderdatetime > '" & sDate & "' and orderdatetime < '" & eDate & "' " &_
							"group by itemID " &_
						") b on a.itemID = b.itemID " &_
						"left join ( " &_
							"select partNumber, inv_qty " &_
							"from we_Items " &_
							"where master = 1 " &_
						") c on a.partNumber = c.partNumber " &_
						"outer apply ( " &_
							"select top 1 processOrderDate, orderAmt, vendorCode, entryDate, partNumber " &_
							"from we_vendorOrder " &_
							"where complete = 0 and partNumber = a.partNumber order by id desc " &_
						") d " &_
						"outer apply ( " &_
							"select top 1 receivedAmt, processOrderDate, partNumber, vendorCode " &_
							"from we_vendorOrder " &_
							"where complete = 1 and partNumber = a.partNumber order by id desc " &_
						") e " &_
						"left join ( " &_
							"select SUM(quantity) as totalSold, partNumber " &_
							"from we_orderdetails ia " &_
								"left join we_orders ib on ia.orderID = ib.orderID " &_
							"where ib.approved = 1 and ib.cancelled is null and orderdatetime > '" & sDate & "' and orderdatetime < '" & eDate & "' " &_
							"group by partNumber " &_
						") f on a.PartNumber = f.partNumber " &_
					"where a.modelID = " & modelID & " and a.subTypeID = " & subID & " and a.typeID = " & catID & " " &_
					"order by b.sales desc, e.processOrderDate desc, d.processOrderDate desc, d.entryDate desc"
		else
			sql =	"select top 10 a.itemID, a.itemDesc, a.partNumber, a.itemPic, b.sales, c.inv_qty, d.processOrderDate as processDate, d.orderAmt, d.vendorCode as fromVendor, d.entryDate as createDate, e.processOrderDate as orderDate, e.receivedAmt, e.vendorCode as compVendor, f.totalSold " &_
					"from we_items a " &_
						"left join ( " &_
							"select sum(quantity) as sales, itemID " &_
							"from we_orders ia " &_
								"left join we_orderDetails ib on ia.orderid = ib.orderid " &_
							"where ia.approved = 1 and ia.cancelled is null " &_
							"group by itemID " &_
						") b on a.itemID = b.itemID " &_
						"left join ( " &_
							"select partNumber, inv_qty " &_
							"from we_Items " &_
							"where master = 1 " &_
						") c on a.partNumber = c.partNumber " &_
						"outer apply ( " &_
							"select top 1 processOrderDate, orderAmt, vendorCode, entryDate, partNumber " &_
							"from we_vendorOrder " &_
							"where complete = 0 and partNumber = a.partNumber order by id desc " &_
						") d " &_
						"outer apply ( " &_
							"select top 1 receivedAmt, processOrderDate, partNumber, vendorCode " &_
							"from we_vendorOrder " &_
							"where complete = 1 and partNumber = a.partNumber order by id desc " &_
						") e " &_
						"left join ( " &_
							"select SUM(quantity) as totalSold, partNumber " &_
							"from we_orderdetails ia " &_
								"left join we_orders ib on ia.orderID = ib.orderID " &_
							"where ib.approved = 1 and ib.cancelled is null " &_
							"group by partNumber " &_
						") f on a.PartNumber = f.partNumber " &_
					"where a.modelID = " & modelID & " and a.subTypeID = " & subID & " and a.typeID = " & catID & " " &_
					"order by b.sales desc, e.processOrderDate desc, d.processOrderDate desc, d.entryDate desc"
		end if
		if website <> "" then sql = replace(sql,"ia.approved = 1","ia.approved = 1 and ia.store = " & storeID)
		if website <> "" then sql = replace(sql,"ib.approved = 1","ib.approved = 1 and ib.store = " & storeID)
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		bgColor = "#fff"
		do while not rs.EOF
			itemDesc = rs("itemDesc")
			itemPic = rs("itemPic")
			itemSales = prepInt(rs("sales"))
			itemID = prepInt(rs("itemID"))
			partNumber = rs("partNumber")
			inv_qty = rs("inv_qty")
			processDate = rs("processDate")
			orderAmt = rs("orderAmt")
			fromVendor = rs("fromVendor")
			createDate = rs("createDate")
			orderDate = rs("orderDate")
			receivedAmt = rs("receivedAmt")
			compVendor = rs("compVendor")
			totalSold = prepInt(rs("totalSold"))
			
			if itemSales > 0 then
%>
<div style="float:left; background-color:<%=bgColor%>; width:100%; border-bottom:1px solid #999;">
	<div style="float:left; width:100px;"><img src="/productpics/thumb/<%=itemPic%>" border="0" /></div>
	<div style="float:left; width:340px; text-align:left; padding-left:10px;">
    	<a href="/p-<%=itemID%>-productLookUp.asp" style="color:#00F;" target="_blank"><%=itemDesc%></a><br />
        <span style="color:#900;"><%=partNumber%></span><br />
        Sold for this Model: <%=formatNumber(itemSales,0)%><br />
        Total Sold: <%=formatNumber(totalSold,0)%><br />
        Current Inventory: <%=formatNumber(inv_qty,0)%><br />
        <%
		if isnull(processDate) and not isnull(createDate) then
			response.Write("<span style='color:#990; font-weight:bold;'>Building Order: " & orderAmt & " - " & fromVendor & " - " & dateOnly(createDate) & "</span><br />")
		elseif not isnull(processDate) then
			response.Write("<span style='color:#F00; font-weight:bold;'>On Order: " & orderAmt & " - " & fromVendor & " - " & dateOnly(processDate) & "</span><br />")
		end if
		
		if not isnull(orderDate) then response.Write("<span style='color:#090; font-weight:bold;'>Last Received: " & receivedAmt & " - " & compVendor & " - " & dateOnly(orderDate) & "</span><br />")
		%>
    </div>
</div>
<%
			end if
			do while itemID = prepInt(rs("itemID"))
				rs.movenext
				if rs.EOF then exit do
			loop
			if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
		loop
		call CloseConn(oConn)
		response.End()
	end if
	
	'################################
	' Primary level (show models)
	'################################
	if numOfPhones = 0 then numOfPhones = 50
	
	if listBy = "allTime" then
		sql = 	"select top " & numOfPhones & " a.modelID, a.modelName, a.modelImg, b.sales, c.brandName " &_
				"from we_models a " &_
				"left join (" &_
					"select sum(quantity) as sales, modelID " &_
					"from we_orders ia " &_
						"left join we_orderDetails ib on ia.orderID = ib.orderID " &_
						"left join we_Items ic on ib.itemID = ic.itemID " &_
					"where ia.approved = 1 and ia.cancelled is null " &_
					"group by modelID" &_
				") b on a.modelID = b.modelID " &_
				"join we_brands c on a.brandID = c.brandID " &_
			" order by b.sales desc"
	elseif listBy = "new2Old" then
		sql = 	"select top " & numOfPhones & " a.modelID, a.modelName, a.modelImg, b.sales, c.brandName " &_
				"from we_models a " &_
					"left join (" &_
						"select sum(quantity) as sales, modelID " &_
						"from we_orders ia " &_
							"left join we_orderDetails ib on ia.orderID = ib.orderID " &_
							"left join we_Items ic on ib.itemID = ic.itemID " &_
						"where ia.approved = 1 and ia.cancelled is null " &_
						"group by modelID" &_
					") b on a.modelID = b.modelID " &_
					"join we_brands c on a.brandID = c.brandID " &_
				"order by releaseYear desc, releaseQuarter desc"
	elseif listBy = "dateRange" then
		sql = 	"select top " & numOfPhones & " a.modelID, a.modelName, a.modelImg, b.sales, c.brandName " &_
				"from we_models a " &_
				"left join (" &_
					"select sum(quantity) as sales, modelID " &_
					"from we_orders ia " &_
						"left join we_orderDetails ib on ia.orderID = ib.orderID " &_
						"left join we_Items ic on ib.itemID = ic.itemID " &_
					"where ia.approved = 1 and ia.cancelled is null and orderdatetime > '" & sDate & "' and orderdatetime < '" & eDate & "' " &_
					"group by modelID" &_
				") b on a.modelID = b.modelID " &_
				"join we_brands c on a.brandID = c.brandID " &_
			" order by b.sales desc"
	end if
	if website <> "" then sql = replace(sql,"ia.approved = 1","ia.approved = 1 and ia.store = " & storeID)
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	set tfile = fso.CreateTextFile(server.MapPath("/admin/tempCSV/topModels.csv"))
	tfile.WriteLine("brand,model,modelID,sold")
	
	phoneCnt = 0
	do while not rs.EOF
		phoneCnt = phoneCnt + 1
		modelID = prepInt(rs("modelID"))
		modelName = prepStr(rs("modelName"))
		itemDesc = prepStr(rs("modelName"))
		itemPic = "/productpics/models/" & prepStr(rs("modelImg"))
		if not fso.fileExists(server.MapPath("/productpics/models/" & prepStr(rs("modelImg")))) then itemPic = "/productpics/thumb/imagena.jpg"
		sales = prepInt(rs("sales"))
		brandName = prepStr(rs("brandName"))
		
		tfile.WriteLine(brandName & "," & modelName & "," & modelID & "," & sales)
%>
<div style="float:left; width:100%; border-bottom:1px solid #333; margin-bottom:10px; margin-left:auto; margin-right:auto;">
	<div style="float:left; width:10%; text-align:center;"><%=phoneCnt%></div>
    <div style="float:left; width:30%; text-align:center; margin-left:10px; cursor:pointer;" onclick="expandBreakDown('breakDown_<%=formatSEO(itemDesc)%>',<%=modelID%>)"><img src="<%=itemPic%>" border="0" height="100" /></div>
    <div style="float:left; width:50%; text-align:left; margin-left:10px;"><%=itemDesc%></div>
    <div id="breakDown_<%=formatSEO(itemDesc)%>" style="float:left; width:100%;"></div>
</div>
<%
		rs.movenext
	loop
	tfile.close
	set tfile = nothing
	call CloseConn(oConn)
%>