<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
	thisUser = Request.Cookies("admin")("username")
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%

	call getDBConn(oConn)

	viewSite = prepStr(request.QueryString("viewSite"))
	
	if isnull(viewSite) or len(viewSite) < 1 then viewSite = "CO"
	
	if viewSite = "CA" or viewSite = "PS" or viewSite = "FG" then
		response.Write("<span style='color:#ff0000; font-weight:bold;'>This site is not currently supported with this application</span>")
		call CloseConn(oConn)
		response.End()
	end if
	
	sql = "select * from " & viewSite & "_mainbanners where active = 1 order by orderNum"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 3, 3
	
	if viewSite = "CO" then
%>
<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" align="center">
    <tr bgcolor="#333333" style="font-weight:bold; color:#FFF; font-size:12px;">
        <td align="left">Position</td>
        <td align="left">Image</td>
        <td align="left">Action</td>
    </tr>
    <%
    lap = 0
    do while not rs.EOF
        lap = lap + 1
    %>
    <tr>
        <td align="center">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr><td align="center"><%=lap%></td></tr>
                <tr><td align="center" nowrap="nowrap" style="font-size:10px">Default View</td></tr>
                <tr><td align="center"><input type="checkbox" name="defaultView" value="1"<% if rs("defaultBanner") then %> checked="checked"<% end if %> /></td></tr>
            </table>
        </td>
        <td>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2" align="center"><img src="http://www.cellularoutfitter.com/images/mainbanner/<%=rs("picLoc")%>" height="100" /></td>
                </tr>
                <tr>
                    <td nowrap="nowrap" align="right" style="font-weight:bold; font-size:10px;">Link:</td>
                    <td align="left"><input type="text" name="bLink" value="<%=rs("link")%>" size="42" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=link&updateText=1&newVal=' + escape(this.value),'dumpZone')" /></td>
                </tr>
                <tr>
                    <td nowrap="nowrap" align="right" style="font-weight:bold; font-size:10px;">Title:</td>
                    <td align="left"><input type="text" name="title" value="<%=rs("tabTitle")%>" size="42" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=tabTitle&updateText=1&newVal=' + escape(this.value),'dumpZone')" /></td>
                </tr>
                <tr>
                    <td nowrap="nowrap" align="right" style="font-weight:bold; font-size:10px;">Title Font:</td>
                    <td align="left"><input type="text" name="titleFont" value="<%=rs("tabTitleFont")%>" size="42" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=tabTitleFont&updateText=1&newVal=' + escape(this.value),'dumpZone')" /></td>
                </tr>
                <tr>
                    <td nowrap="nowrap" align="right" style="font-weight:bold; font-size:10px;">Subtitle:</td>
                    <td align="left"><input type="text" name="subtitle" value="<%=rs("tabSubTitle")%>" size="42" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=tabSubTitle&updateText=1&newVal=' + escape(this.value),'dumpZone')" /></td>
                </tr>
                <tr>
                    <td nowrap="nowrap" align="right" style="font-weight:bold; font-size:10px;">subtitle Font:</td>
                    <td align="left"><input type="text" name="subtitleFont" value="<%=rs("tabSubTitleFont")%>" size="42" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=tabSubTitleFont&updateText=1&newVal=' + escape(this.value),'dumpZone')" /></td>
                </tr>
                <tr><td colspan="2" align="right"><input type="button" name="SaveBttn" value="Save Text Changes" style="font-size:9px;" /></td></tr>
            </table>
        </td>
        <td align="left">
            <div>
                <div style="float:left;">
                    <select name="bannerAction_<%=lap%>" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=' + this.value,'actionArea_<%=lap%>')">
                        <option value="">Select Action</option>
                        <option value="rp">Replace</option>
                    </select>
                </div>
                <div style="float:left; width:80px; text-align:right;"><a style="cursor:pointer; color:#F00;" onclick="cancelAction(<%=lap%>)">Cancel</a></div>                            
            </div>
            <div style="width:300px;height:190px;;overflow:auto;" id="actionArea_<%=lap%>"></div>
        </td>
    </tr>
    <%
        rs.movenext
    loop
    %>
</table>
<%
	elseif viewSite = "WE" then
%>
<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" align="center">
    <tr bgcolor="#333333" style="font-weight:bold; color:#FFF; font-size:12px;">
        <td align="left">Position</td>
        <td align="left">Image</td>
        <td align="left">Action</td>
    </tr>
    <%
    lap = 0
    do while not rs.EOF
        lap = lap + 1
    %>
    <tr>
        <td align="center">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr><td align="center"><%=lap%></td></tr>
                <tr><td align="center" nowrap="nowrap" style="font-size:10px">Default View</td></tr>
                <tr><td align="center"><input type="checkbox" name="defaultView" value="1"<% if rs("defaultBanner") then %> checked="checked"<% end if %> /></td></tr>
            </table>
        </td>
        <td id="linksTitles_<%=lap%>">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2" align="center" id="bannerImg_<%=lap%>"><img src="http://www.wirelessemporium.com/images/mainbanners/<%=rs("picLoc")%>" height="100" /></td>
                </tr>
                <tr>
                    <td nowrap="nowrap" align="right" style="font-weight:bold; font-size:10px;">Link:</td>
                    <td align="left"><input type="text" name="bLink_<%=lap%>" value="<%=rs("link")%>" size="42" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=link&updateText=1&newVal=' + escape(this.value),'dumpZone')" /></td>
                </tr>
                <tr>
                    <td nowrap="nowrap" align="right" style="font-weight:bold; font-size:10px;">PopUp Location:</td>
                    <td align="left"><input type="text" name="title_<%=lap%>" value="<%=rs("popUpLoc")%>" size="42" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=popUpLoc&updateText=1&newVal=' + escape(this.value),'dumpZone')" /></td>
                </tr>
                <tr>
                    <td nowrap="nowrap" align="right" style="font-weight:bold; font-size:10px;">PopUp Title:</td>
                    <td align="left"><input type="text" name="titleFont_<%=lap%>" value="<%=rs("popUpTitle")%>" size="42" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=popUpTitle&updateText=1&newVal=' + escape(this.value),'dumpZone')" /></td>
                </tr>
                <tr><td colspan="2" align="right"><input type="button" name="SaveBttn" value="Save Text Changes" style="font-size:9px;" /></td></tr>
            </table>
        </td>
        <td align="left">
            <div>
                <div style="float:left;">
                    <select name="bannerAction_<%=lap%>" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=' + this.value,'actionArea_<%=lap%>')">
                        <option value="">Select Action</option>
                        <option value="rp">Replace</option>
                    </select>
                </div>
                <div style="float:left; width:80px; text-align:right;"><a style="cursor:pointer; color:#F00;" onclick="cancelAction(<%=lap%>)">Cancel</a></div>                            
            </div>
            <div style="width:300px;height:190px;;overflow:auto;" id="actionArea_<%=lap%>"></div>
        </td>
    </tr>
    <%
        rs.movenext
    loop
    %>
</table>
<%
	end if
	
	call CloseConn(oConn)
%>