<%
	response.buffer = true
	response.expires = -1
%><!--#include virtual="/includes/asp/inc_basepage.asp"--><%	

	dim UAID : UAID = prepInt(request.QueryString("UAID"))
	dim updateType : updateType = prepStr(request.QueryString("updateType"))
	dim isBot : isBot = prepStr(request.QueryString("isbot"))
	dim modelid : modelid = prepInt(request.QueryString("modelid"))	
	dim modelNumber : modelNumber = prepStr(request.QueryString("modelNumber"))	
	
	if updateType = "bot" then
		sql	=	"update	mobileBrowsers" & vbcrlf & _
				"set	isBot = " & isBot & vbcrlf & _
				"	,	updateDate = getdate()" & vbcrlf & _
				"	,	updateAdminUser = " & prepInt(session("adminID")) & vbcrlf & _
				"where	id = " & UAID
		oConn.execute(sql)
		response.write "updated"
	elseif updateType = "enterModel" then
	%>
    <select name="cbModel" style="width:110px;" onchange="onCbModel(<%=UAID%>,this.value,'<%=modelNumber%>')">
		<%
		sql	=	"select	a.brandid, b.modelid, a.brandname, b.modelname" & vbcrlf & _
				"from	we_brands a join we_models b" & vbcrlf & _
				"	on	a.brandid = b.brandid" & vbcrlf & _
				"where	b.modelname not like 'all model%'" & vbcrlf & _
				"order by a.brandname, b.modelname" & vbcrlf				
		set rs = oConn.execute(sql)
		brandHold = rs("brandid")
		response.write "<option value="""">Please Select --</option>"
		response.write "<optgroup label=""" & rs("brandName") & """>" & vbcrlf
		do until rs.eof
			if rs("brandid") <> brandHold then
				response.write "</optgroup>" & vbcrlf
				response.write "<optgroup label=""" & rs("brandName") & """>" & vbcrlf
			end if
			brandHold = rs("brandid")
			response.write "<option value=""" & rs("modelid") & """"
			response.write ">" & rs("modelname") & "</option>" & vbCrLf
			rs.movenext
		loop
		response.write "</optgroup>" & vbcrlf
		%>
    </select>
    <br />
	<a href="javascript:document.frm.submit()">Refresh</a>
    <%
	elseif updateType = "updateModel" then
		sql	=	"update	mobileBrowsers" & vbcrlf & _
				"set	modelid = " & modelid & vbcrlf & _
				"	,	brandid = (select brandid from we_models where modelid = " & modelid & ")" & vbcrlf & _
				"where	modelNumber = '" & modelNumber & "'"
'		response.write "<pre>" & sql & "</pre>"
		oConn.execute(sql)
		response.write "UPDATED<br>"
	end if
	
	call CloseConn(oConn)
%>