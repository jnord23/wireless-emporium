<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%

	call getDBConn(oConn)

	dim typeID : typeID = prepInt(request.QueryString("typeID"))
	dim subTypeID : subTypeID = prepInt(request.QueryString("subTypeID"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim lap : lap = prepInt(request.QueryString("lap"))
	dim dbaseID : dbaseID = prepInt(request.QueryString("dbID"))
	dim priceRetail : priceRetail = prepInt(request.QueryString("priceRetail"))
	dim priceWE : priceWE = prepInt(request.QueryString("priceWE"))
	dim cogs : cogs = prepInt(request.QueryString("cogs"))
	
	if modelID > 0 then
		sql = "select * from holding_mobileLine with(nolock) where id = " & dbaseID
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
		
		if rs.EOF then
			response.Write("Product no longer exists")
			call CloseConn(oConn)
			response.End()
		end if
		
		hld_id = rs("id")
		hld_mobileLineID = rs("mobileLineID")
		hld_cat1 = rs("cat1")
		hld_cat2 = rs("cat2")
		hld_cat3 = rs("cat3")
		hld_cat4 = rs("cat4")
		hld_name = rs("name")
		hld_description = rs("description")
		hld_shortDesc = rs("shortDesc")
		hld_weight = rs("weight")
		hld_msrp = rs("msrp")
		hld_cogs = rs("cogs")
		hld_model = rs("model")
		hld_image = rs("image")
		hld_inv_qty = rs("inv_qty")
		hld_UPC = rs("UPC")
		hld_status = rs("status")
		
		if isnull(hld_description) or len(hld_description) < 1 then hld_description = hld_name
		if isnull(hld_shortDesc) or len(hld_shortDesc) < 1 then hld_shortDesc = hld_name
		
		session("errorSQL") = "variables set"
		
		hfCode = ""
		skipProd = 0
		if hld_cat4 = "Cases" or hld_cat4 = "Pouches" or hld_cat4 = "Skins" then
'			typeID = 7
			left3 = "LEA"
		elseif hld_cat4 = "Holsters" or hld_cat4 = "Phone Mounts" or hld_cat4 = "Universal Mounts" then
'			typeID = 6
			left3 = "HOL"
		elseif hld_cat4 = "Screen Protectors" then
'			typeID = 18
			left3 = "SCR"
		elseif hld_cat4 = "Wired Headsets" then
'			typeID = 5
			left3 = "HAN"
		elseif hld_cat3 = "Batteries" or hld_cat3 = "Battery Doors" then
'			typeID = 1
			left3 = "BAT"
		elseif hld_cat3 = "Chargers" then
'			typeID = 2
			left3 = "CHA"
		elseif hld_cat1 = "Keyboards" or hld_cat4 = "Stylus" then
'			typeID = 8
			left3 = "OTH"
		elseif hld_cat4 = "USB Data Transfer" or hld_cat4 = "HDMI Cables" or hld_cat3 = "Data" or hld_cat3 = "Memory Cards" then
'			typeID = 13
			left3 = "DAT"
		elseif hld_cat3 = "Bluetooth" then
'			typeID = 5
			left3 = "BLU"
			hfCode = 1
		elseif hld_cat4 = "Lanyards" or hld_cat1 = "MISC" then
			skipProd = 1
		else
			unknownItems = unknownItems & hld_cat1 & ", " & hld_cat2 & ", " & hld_cat3 & ", " & hld_cat4 & "<br />"
			skipProd = 1
		end if
		if skipProd = 0 then
			session("errorSQL") = "do not skip"
			midSection = left(replace(replace(hld_mobileLineID,"-","")," ",""),4)
			do while len(midSection) < 4
				midSection = "0" & cstr(midSection)
			loop
			
			solidBase = 0
			
			do while solidBase = 0
				sql = "select top 1 cast(RIGHT(PartNumber,2) as int) as last2 from we_items where PartNumber like '" & left3 & "-MLD-" & midSection & "-%' and ISNUMERIC(right(PartNumber,2)) > 0 order by PartNumber desc"
				session("errorSQL") = sql
				set getLastRS = oConn.execute(sql)
				
				if getLastRS.EOF then
					last2 = "01"
					solidBase = 1
				elseif getLastRS("last2") = 99 then
					incMS = 0
					ms1 = mid(midSection,1,1)
					ms2 = mid(midSection,2,1)
					ms3 = mid(midSection,3,1)
					ms4 = mid(midSection,4,1)
					if isnumeric(ms1) and incMS = 0 then
						if prepInt(ms1) < 9 then ms1 = prepInt(ms1) + 1 : incMS = 1
					end if
					if isnumeric(ms2) and incMS = 0 then
						if prepInt(ms2) < 9 then ms2 = prepInt(ms2) + 1 : incMS = 1
					end if
					if isnumeric(ms3) and incMS = 0 then
						if prepInt(ms3) < 9 then ms3 = prepInt(ms3) + 1 : incMS = 1
					end if
					if isnumeric(ms4) and incMS = 0 then
						if prepInt(ms4) < 9 then ms4 = prepInt(ms4) + 1 : incMS = 1
					end if
					midSection = "" & ms1 & ms2 & ms3 & ms4 & ""
				else
					last2 = getLastRS("last2") + 1
					solidBase = 1
				end if
			loop
			partNumber = left3 & "-MLD-" & midSection & "-" & last2
			approvedPN = 0
			last2 = 0
			do while approvedPN = 0
				sql = "select * from we_items with(nolock) where partNumber = '" & partNumber & "'"
				session("errorSQL") = sql
				Set rs = Server.CreateObject("ADODB.Recordset")
				rs.open sql, oConn, 0, 1
				
				if rs.EOF then
					approvedPN = 1
				else
					last2 = last2 + 1
					if len(last2) < 2 then
						useLast2 = "0" & last2
					elseif len(last2) > 2 then
						useLast2 = "01"
						incMS = 0
						ms1 = mid(midSection,1,1)
						ms2 = mid(midSection,2,1)
						ms3 = mid(midSection,3,1)
						ms4 = mid(midSection,4,1)
						if isnumeric(ms1) and incMS = 0 then
							if prepInt(ms1) < 9 then ms1 = prepInt(ms1) + 1 : incMS = 1
						end if
						if isnumeric(ms2) and incMS = 0 then
							if prepInt(ms2) < 9 then ms2 = prepInt(ms2) + 1 : incMS = 1
						end if
						if isnumeric(ms3) and incMS = 0 then
							if prepInt(ms3) < 9 then ms3 = prepInt(ms3) + 1 : incMS = 1
						end if
						if isnumeric(ms4) and incMS = 0 then
							if prepInt(ms4) < 9 then ms4 = prepInt(ms4) + 1 : incMS = 1
						end if
						midSection = "" & ms1 & ms2 & ms3 & ms4 & ""
					else
						useLast2 = last2
					end if
					do while len(midSection) < 4
						midSection = "0" & cstr(midSection)
					loop
					partNumber = left3 & "-MLD-" & midSection & "-" & useLast2
				end if
			loop
			
			set jpeg = Server.CreateObject("Persits.Jpeg")
			set fso = CreateObject("Scripting.FileSystemObject")
			first2 = left(hld_image,2)

			imagePath = server.MapPath("\productpics")
						
			set XMLHTTP = server.CreateObject("MSXML2.ServerXMLHTTP")
			XMLHTTP.Open "GET", "http://www.mobileline.com/store/media/catalog/product/cache/1/image/5e06319eda06f020e43594a9c230972d/" & left(first2,1) & "/" & right(first2,1) & "/" & hld_image, False
			session("errorSQL") = "url = http://www.mobileline.com/store/media/catalog/product/cache/1/image/5e06319eda06f020e43594a9c230972d/" & left(first2,1) & "/" & right(first2,1) & "/" & hld_image
			XMLHTTP.Send
			
			If XMLHTTP.Status = "200" AND XMLHTTP.Status <> "404" then	
				set DataStream = CreateObject("ADODB.Stream")
				DataStream.Open
				DataStream.Type = 1
				DataStream.Write xmlHTTP.ResponseBody
				DataStream.Position = 0
				DataStream.SaveToFile imagePath & "\temp\" & hld_image
				DataStream.Close
				set DataStream = Nothing
			else
				set XMLHTTP = server.CreateObject("MSXML2.ServerXMLHTTP")
				XMLHTTP.Open "GET", "http://www.mobileline.com/store/media/catalog/product/cache/1/image/190x/5e06319eda06f020e43594a9c230972d/" & left(first2,1) & "/" & right(first2,1) & "/" & hld_image, False
				session("errorSQL") = "url = http://www.mobileline.com/store/media/catalog/product/cache/1/image/190x/5e06319eda06f020e43594a9c230972d/" & left(first2,1) & "/" & right(first2,1) & "/" & hld_image
				XMLHTTP.Send
				
				If XMLHTTP.Status = "200" AND XMLHTTP.Status <> "404" then	
					set DataStream = CreateObject("ADODB.Stream")
					DataStream.Open
					DataStream.Type = 1
					DataStream.Write xmlHTTP.ResponseBody
					DataStream.Position = 0
					DataStream.SaveToFile imagePath & "\temp\" & hld_image
					DataStream.Close
					set DataStream = Nothing
				else
					session("errorSQL2") = "error accessing image"
				end if
			end if
			
			Set XMLHTTP = Nothing
			
			if fso.fileExists(imagePath & "\temp\" & hld_image) then
				jpeg.Open imagePath & "\temp\" & hld_image
				if jpeg.Width >= 800 and jpeg.Height >= 800 then
					jpeg.Height = 800
					jpeg.Width = 800
					jpeg.Save imagePath & "\big\zoom\" & hld_image
				end if
			
				jpeg.Open imagePath & "\temp\" & hld_image
				jpeg.Height = 300
				jpeg.Width = 300
				jpeg.Save imagePath & "\big\" & hld_image
				
				jpeg.Open imagePath & "\temp\" & hld_image
				jpeg.Height = 100
				jpeg.Width = 100
				jpeg.Save imagePath & "\thumb\" & hld_image
				
				jpeg.Open imagePath & "\temp\" & hld_image
				jpeg.Height = 65
				jpeg.Width = 65
				jpeg.Save imagePath & "\homepage65\" & hld_image
				
				jpeg.Open imagePath & "\temp\" & hld_image
				jpeg.Height = 45
				jpeg.Width = 45
				jpeg.Save imagePath & "\icon\" & hld_image
				
				fso.deleteFile(imagePath & "\temp\" & hld_image)
			end if
			
			hld_weprice = hld_msrp - 1
			if priceRetail > 0 then hld_msrp = priceRetail
			if priceWE > 0 then hld_weprice = priceWE
			if cogs > 0 then hld_cogs = cogs
			
			sql = "SET NOCOUNT ON; insert into we_items (brandID,modelID,typeID,subtypeid,vendor,partNumber,itemDesc,itemPic,price_retail,price_our,cogs,inv_qty,itemLongDetail,UPCCode,itemWeight,mobileLine_sku,master,handsfreeType) values(" & brandID & "," & modelID & ",'" & typeID & "','" & subtypeid & "','MLD','" & partNumber & "','" & hld_name & "','" & hld_image & "','" & hld_msrp & "','" & hld_weprice & "','" & hld_cogs & "'," & hld_inv_qty & ",'" & hld_description & "','" & hld_UPC & "','" & hld_weight & "','" & hld_mobileLineID & "',1,'" & hfCode & "'); SELECT @@IDENTITY AS newItemID;"
			session("errorSQL") = sql
			set insertRS = oConn.execute(sql)
			dim newItemID : newItemID = insertRS("newItemID")
			insertRS = null
			
			response.Write("<a href='/admin/createSlave.asp?linkItemID=" & newItemID & "' target='mobileLine'>Create Slave</a>")
			response.Write("&nbsp;")
			response.Write("<a href='/admin/db_update_items.asp?ItemID=" & newItemID & "' target='mobileLine'>Edit Master</a>")
			
			sql = "delete from holding_mobileLine where id = " & dbaseID
			session("errorSQL") = sql
			oConn.execute(sql)
		else
			response.Write("Unknown Type:" & unknownItems)
		end if
		
		rs = null
		jpeg = null
	else
		sql = "select modelID, modelName from we_models where brandID = " & brandID & " order by modelName"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
%>
<select name="modelID" onchange="setModel(this.value,<%=brandID%>,<%=lap%>,<%=dbaseID%>)">
	<option value="">Select Model</option>
    <%
	do while not rs.EOF
	%>
    <option value="<%=rs("modelID")%>"><%=rs("modelName")%></option>
    <%
		rs.movenext
	loop
	%>
</select>
<%
	end if
	
	call CloseConn(oConn)
%>