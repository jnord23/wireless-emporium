<%
	response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
function FormatBool(b)
	if b = true then
		FormatBool = "<div class=""true center"" title=""True"">&#x2713;</div>"
	elseif b = false then
		FormatBool = "<div class=""false center"" title=""False"">&#x2717</div>"
	else 'null
		FormatBool = ""
	end if
end function
function FormatBoolTitle(b,m)
	if b = true then
		FormatBoolTitle = "<div class=""true center"" title=""True"">&#x2713;</div>"
	elseif b = false then
		FormatBoolTitle = "<div class=""false center"" title=""" & m & """>&#x2717</div>"
	else 'null
		FormatBoolTitle = ""
	end if
end function
function CreateOrderLink(o)
	if o = "" or isnull(o) then
		CreateOrderLink = ""
	else
		CreateOrderLink = "<a href=""/admin/view_invoice.asp?orderId=" & o & """ target=""_blank"">" & o & "</a>"
	end if
end function
%>
<%
	
	
	
	sel = "select *"
	from = "from Tracking t left join we_orders o on t.assignedOrderID = o.orderID"
	where = "where t.entryDate > DATEADD(day, -1, getdate()) and t.enteredCC = 1 and (t.accountReturnedByEmail = 1 or t.EnteredShipping = 1) and t.zipCodeDataPulled = 1 and t.clickedPlaceMyOrder = 1 and t.visitedConfirm = 0"
	if prepStr(request.QueryString("testApproved")) = "1" then
		where = "where entryDate > DATEADD(day, -1, getdate()) and processingApproved is not null"
	end if
	orderBy = "order by entryDate desc"
	groupBy = ""
	
	
	sql = sel & " " & from & " " & where & " " & orderBy & " " & groupBy
	session("errorSQL") = sql
	'response.write sql
	'response.end
	set rs = oConn.execute(sql)
	
%>
<style>
.true {
	color:green;
}
.false {
	color:red;
}
.center {
	text-align:center;
}
#tblReport td, th {
    padding: 2px;
}

#tblReport td td, #tblReport tr tr:nth-child(odd), #tblReport tr tr:nth-child(even) {
	background:none;
	color:#000;
}
.divMoreInfoContainer {
	position:relative;
}
.divMoreInfoDetails {
	position:absolute;
	right:0px;
	top:0px;
	z-index:10;
	min-width:198px;
	min-height:198px;
	border-radius: 10px;
	-moz-border-radius: 10px;
	-khtml-border-radius: 10px;
	background:#EEEEEE;
	border:1px solid #A9A9A9;
	display:none;
}
.divMoreInfoContent {
	width:100%;
	height:100%;
	padding:5px;
}
.slabel {
	font-weight:bold;
}
.rt {
	text-align:right;
}
</style>

    <p>WE visitors who reached the checkout page in the past 24 hours</p>
    
    <!--
    'Default
    <strike>Typed Credit Card = Yes</strike>
    <strike>(Account Returned by Email or Entered Shipping)</strike>
    <strike>Zip Code Data Retrieved = Yes</strike>
    <strike>Clicked Place My Order = Yes</strike>
    <strike>if they got all the way through, don't even show it on the report</strike>
    <strike>processing.asp where order id is determined</strike>
    <strike>hide date</strike>
    checkout.asp
    <strike>IE Refresh</strike>
	-->


    <!--
    <%=sql%>
	-->
    
    <table id="tblReport">
    	<thead>
            <tr>
                <th>#</th>
                <th title="sessionID">sessionID</th>
                <th title="Typed Credit Card">Credit Card</th>
                <th title="Selected Western Union Pay">WuPay</th>
                <th title="Account Returned by Email">Account Returned</th>
                <th title="Typed Shipping Street Address">Shipping</th>
                <th title="Changed Shipping Method from Default Option">Shipping Method</th>
                <th title="Typed Zip Code">Zip Code</th>
                <th title="Shipping Cost Data Successfully Retrieved">Zip Code Retrieved</th>
                <th title="Clicked the 'Place My Order' Button">Clicked Submit</th>
                <th title="Order Form Successfully Validated and Submitted to Processing">Order Submitted</th>
                <th title="Visited Processing">Visited Processing</th>
                <th title="Processing Approved or Error Message">Approved</th>
                <th title="Assigned an Order ID on Processing">Order ID</th>
                <th title="Total Dollar Amount of Order(Grand Total)">Order Amount</th>
                <th title="Visited Complete Page">Visited Complete</th>
                <th title="Visited Confirm Page">Visited Confirm</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <tr style="display:none;">
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
                <td><input type="checkbox" /></td>
            </tr>
            <% 
			cnt = 1
			if not rs.eof then
			do while not rs.eof
			
			sessionID = rs("sessionID")
			enteredCC = rs("enteredCC")
			enteredWuPay = rs("enteredWuPay")
			accountReturnedByEmail = rs("accountReturnedByEmail")
			enteredShipping = rs("enteredShipping")
			enteredShippingMethod = rs("enteredShippingMethod")
			enteredZipCode = rs("enteredZipCode")
			zipCodeDataPulled = rs("zipCodeDataPulled")
			clickedPlaceMyOrder = rs("clickedPlaceMyOrder")
			validatedPlaceMyOrder = rs("validatedPlaceMyOrder")
			visitedProcessing = rs("visitedProcessing")
			processingApproved = rs("processingApproved")
			processingErrorMessage = rs("processingErrorMessage")
			assignedOrderID = rs("assignedOrderID")
			visitedComplete = rs("visitedComplete")
			visitedConfirm = rs("visitedConfirm")
			email = rs("email")
			phone = rs("phone")
			browser = rs("browser")
			entryDate = Cstr(rs("entryDate")) ': entryDate = cStr(entryDate)
			orderGrandTotal = rs("ordergrandTotal")
			%>
            <tr>
                <td><%=cnt%></td>
                <td><a href="/cart/basket.asp?csid=<%=sessionID%>" target="_blank"><%=sessionID%></a></td>
                <td><%=FormatBool(enteredCC)%></td>
                <td><%=FormatBool(enteredWuPay)%></td>
                <td><%=FormatBool(accountReturnedByEmail)%></td>
                <td><%=FormatBool(enteredShipping)%></td>
                <td><%=FormatBool(enteredShippingMethod)%></td>
                <td><%=FormatBool(enteredZipCode)%></td>
                <td><%=FormatBool(zipCodeDataPulled)%></td>
                <td><%=FormatBool(clickedPlaceMyOrder)%></td>
                <td><%=FormatBool(validatedPlaceMyOrder)%></td>
                <td><%=FormatBool(visitedProcessing)%></td>
                <td><%=FormatBoolTitle(processingApproved,processingErrorMessage)%></td>
                <td><%=CreateOrderLink(assignedOrderID)%></td>
                <td class="rt"><%=FormatCurrency(prepInt(orderGrandTotal))%></td>
                <td><%=FormatBool(visitedComplete)%></td>
                <td><%=FormatBool(visitedConfirm)%></td>
                <td><div class="divMoreInfoContainer"><input value="More Info" type="button" onclick="document.getElementById('mi_<%=cnt%>').style.display = 'block'" />
                    	<div class="divMoreInfoDetails" id="mi_<%=cnt%>">
                        	<div class="divMoreInfoContent">
                            	<div style="text-align:right;margin-right:10px;"><input value="Close" type="button" onclick="document.getElementById('mi_<%=cnt%>').style.display = 'none'" /></div>
                                <div>
                                	<table id="tblDetail_<%=cnt%>" style="color:#000;">
                                    	<tr>
                                        	<td valign="top" class="slabel">Email:</td>
                                            <td valign="top"><%=email%></td>
                                        </tr>
                                    	<tr>
                                        	<td valign="top" class="slabel">Phone:</td>
                                            <td valign="top"><%=phone%></td>
                                        </tr>
                                    	<tr>
                                        	<td valign="top" class="slabel">Browser:</td>
                                            <td valign="top"><%=browser%></td>
                                        </tr>
                                    	<tr style="display:none;">
                                        	<td valign="top" class="slabel">Date:</td>
                                            <td valign="top"><%=entryDate%></td>
                                        </tr>
                                    </table>
                                    <%
                                    			'response.write ":)"
												'response.write(formatdatetime(entryDate, vbgeneraldate))
									%>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <%
			rs.movenext
			cnt = cnt + 1
			loop
			end if
			%>
        </tbody>
        <tfoot>
        	<tr>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            	<td>&nbsp;</td>
            </tr>
        </tfoot>
    </table>
<%
call CloseConn(oConn)
%>