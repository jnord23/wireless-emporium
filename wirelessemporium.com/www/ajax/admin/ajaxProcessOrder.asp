<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/Framework/Utility/Static.asp"-->
<%
call getDBConn(oConn)

dim dateSearch : dateSearch = prepStr(request.querystring("txtSearchDate"))
dim siteid : siteid = prepStr(request.querystring("cbSite"))
dim shiptypeid : shiptypeid = prepStr(request.querystring("cbShiptype"))
dim shiptypeid2 : shiptypeid2 = prepStr(request.querystring("cbShiptype2"))
curSortColumnName = prepStr(request.querystring("curSortColumnName"))
curSort = prepStr(request.querystring("curSort"))
dateSearchEnd = cdate(dateSearch) + 1

strSqlOrderBy = "order by " & curSortColumnName & " " & curSort & ", isRehipOrder desc, isBackOrder desc, isDropShipOrder desc"

sql	=	"select	case when a.thub_posted_to_accounting = 'R' then 1 else 0 end isRehipOrder" & vbcrlf & _
		"	,	case when a.thub_posted_to_accounting = 'B' then 1 else 0 end isBackOrder" & vbcrlf & _
		"	,	case when isnull(q.numRegularOrder, 0) = isnull(q.numVendorOrder, 0) then 1 " & vbcrlf & _
		"			when a.thub_posted_to_accounting = 'D' then 1" & vbcrlf & _
		"			else 0 " & vbcrlf & _
		"		end isDropShipOrder" & vbcrlf & _
		"	--,	case when a.processPDF like '%WHALE%' then 'WHALE' else x.shortDesc end shortDesc" & vbcrlf & _
		"	--,	case when a.processPDF like '%WHALE%' then '' " & vbcrlf & _
		"	--		else isnull(case when o.isShipMethod = 1 then o.token end," & vbcrlf & _
		"	--					case when t.shiptypeid = 1 then " & vbcrlf & _
		"	--							case when q.quantity > 1 then t.token + '-MULTI' else t.token + '-SINGLE' end" & vbcrlf & _
		"	--						else isnull(t.token, a.shiptype) " & vbcrlf & _
		"	--					end) " & vbcrlf & _
		"	--	end shipToken2" & vbcrlf & _
		"	,	'ALL' shortDesc" & vbcrlf & _
		"	,	'ALL' shipToken2" & vbcrlf & _		
		"	,	convert(varchar(10), a.thub_posted_date, 20) processed_datetime" & vbcrlf & _
		"	,	a.processPDF, a.processTXT" & vbcrlf & _
		"	,	count(*) numOrders" & vbcrlf & _
		"	,	isnull(sum(case when a.rmastatus is null and a.scandate is not null then 1 else 0 end), 0) numScan" & vbcrlf & _
		"	,	isnull(sum(case when a.cancelled is not null or cancelled = 1 then 1 else 0 end), 0) numCancel" & vbcrlf & _
		"	,	isnull(sum(case when a.rmastatus is not null then 1 else 0 end), 0) numBackorder" & vbcrlf & _
		"from 	we_orders a with (nolock) join v_accounts v" & vbcrlf & _
		"	on	a.store = v.site_id and a.accountid = v.accountid join xstore x with (nolock)" & vbcrlf & _
		"	on	x.site_id = a.store join " & vbcrlf & _
		"		(" & vbcrlf & _
		"		select	a.store, a.orderid, isnull(sum(b.quantity), 0) quantity, sum(b.quantity * isnull(c.itemWeight, 0.0)) itemWeight" & vbcrlf & _
		"			,	count(*) numRegularOrder" & vbcrlf & _
		"			,	isnull(sum(case when m.id is not null then 1 " & vbcrlf & _
		"								else" & vbcrlf & _
		"									case when c.vendor in ('CM', 'DS', 'MLD') then 1 " & vbcrlf & _
		"										when c.partnumber like '%HYP%' then 1" & vbcrlf & _
		"										else 0 end" & vbcrlf & _
		"							end), 0) numVendorOrder" & vbcrlf & _
		"		from 	we_orders a with (nolock) join we_orderdetails b with (nolock)" & vbcrlf & _
		"			on	a.orderid = b.orderid left outer join we_items c with (nolock)" & vbcrlf & _
		"			on	b.itemid = c.itemid left outer join we_items_musicskins m with (nolock)" & vbcrlf & _
		"			on	b.itemid = m.id" & vbcrlf & _
		"		where	a.thub_posted_date >= '" & dateSearch & "'" & vbcrlf & _
		"			and a.thub_posted_date < '" & dateSearchEnd & "'" & vbcrlf & _
		"			and	a.approved = 1" & vbcrlf & _
		"			and	b.itemid not in (1)" & vbcrlf & _
		"		group by a.store, a.orderid" & vbcrlf & _
		"		) q" & vbcrlf & _
		"	on	a.store = q.store and a.orderid = q.orderid left outer join XShipType t with (nolock)" & vbcrlf & _
		"	on	a.shiptype = t.shipdesc left outer join XOrderType o with (nolock)" & vbcrlf & _
		"	on	a.extOrderType = o.typeid " & vbcrlf & _
		"where	a.thub_posted_date >= '" & dateSearch & "'" & vbcrlf & _
		"	and a.thub_posted_date < '" & dateSearchEnd & "'" & vbcrlf & _
		"	and	x.active = 1" & vbcrlf
if siteid <> "" then
	sql = sql & "	and	a.store = '" & siteid & "'" & vbcrlf
end if
if shiptypeid <> "" then
	sql = sql & "	and	t.shiptypeid = '" & shiptypeid & "'" & vbcrlf
end if
if shiptypeid2 <> "" then
	if shiptypeid2 = "A" then
		sql = sql & "	and	(a.processPDF like '%AM%' or a.processPDF like '%PM%') and a.thub_posted_to_accounting not in ('R','B')" & vbcrlf
	else
		sql = sql & "	and	a.thub_posted_to_accounting = '" & shiptypeid2 & "'" & vbcrlf
	end if
end if
sql = sql & "group by	case when a.thub_posted_to_accounting = 'R' then 1 else 0 end" & vbcrlf & _
			"		,	case when a.thub_posted_to_accounting = 'B' then 1 else 0 end" & vbcrlf & _				
			"		,	case when isnull(q.numRegularOrder, 0) = isnull(q.numVendorOrder, 0) then 1 " & vbcrlf & _
			"				when a.thub_posted_to_accounting = 'D' then 1" & vbcrlf & _
			"				else 0 " & vbcrlf & _
			"			end " & vbcrlf & _
			"		--,	case when a.processPDF like '%WHALE%' then 'WHALE' else x.shortDesc end" & vbcrlf & _
			"		--,	case when a.processPDF like '%WHALE%' then '' " & vbcrlf & _
			"		--		else isnull(case when o.isShipMethod = 1 then o.token end," & vbcrlf & _
			"		--					case when t.shiptypeid = 1 then " & vbcrlf & _
			"		--							case when q.quantity > 1 then t.token + '-MULTI' else t.token + '-SINGLE' end" & vbcrlf & _
			"		--						else isnull(t.token, a.shiptype) " & vbcrlf & _
			"		--					end) " & vbcrlf & _
			"		--	end" & vbcrlf & _
			"		,	convert(varchar(10), a.thub_posted_date, 20) " & vbcrlf & _
			"		,	a.processPDF, a.processTXT" & vbcrlf & _
			strSqlOrderBy

'		response.write "<pre>" & sql & "</pre>"		
'		response.end
		
set objRsAllOrders = oConn.execute(sql)

numTotalDropShip = 0
if not objRsAllOrders.eof then
	%>
	<table width="1065" border="1" align="center" cellpadding="5" cellspacing="0" style="border-collapse:collapse;font-family: Arial, Helvetica, sans-serif; font-size:12px;">
	<%		
	do until objRsAllOrders.eof
		numOrders = objRsAllOrders("numOrders")
		numScan = objRsAllOrders("numScan")
		numCancel = objRsAllOrders("numCancel")
		numBackorder = objRsAllOrders("numBackorder")
		isDropShipper = objRsAllOrders("isDropShipOrder")
		
		processPDF = objRsAllOrders("processPDF")
		processTXT = objRsAllOrders("processTXT")
		
		if isnumeric(numOrders) then
			if numOrders = 0 then
				processRatio = 0.0
			else
				processRatio = (1.0 * (numScan+numCancel+numBackorder) / numOrders) * 100.0
			end if
		end if
		
		if isDropShipper = 1 then
			numTotalDropShip = numTotalDropShip + numOrders
		else
		%>
		<tr onMouseOver="this.className='mouseover'" onMouseOut="this.className=''">
			<td width="50"><%=objRsAllOrders("shortdesc")%></td>
			<td width="90" align="left">
				<%
				if objRsAllOrders("isRehipOrder") = 1 then
					response.write "<span style=""color:red;"">Reship</span><br />"
				elseif objRsAllOrders("isBackOrder") = 1 then
					response.write "<span style=""color:green; font-weight:bold;"">Back-Order</span><br />"
				elseif instr(processPDF, "(Single") > 0 then
					response.write "<span style=""color:#C60; font-weight:bold;"">Single</span><br />"
				elseif instr(processPDF, "AM") > 0 or instr(processPDF, "PM") > 0 then
					response.write "<span style=""color:blue;"">Additional</span><br />"
				end if
				%>
				<%=objRsAllOrders("shiptoken2")%>                        
			</td>
			<td width="80" align="left"><%=objRsAllOrders("processed_datetime")%></td>
			<td width="420" align="left">
			<%
			if processPDF <> "" then
				if numOrders = numCancel then
			%>
				- <%=processPDF%>
			<%						
				else
			%>
				- <a target="_blank" href="/admin/tempPDF/salesorders/<%=processPDF%>"><%=processPDF%></a>
			<%						
				end if
			end if
			if processTXT <> "" then
				if numOrders = numCancel then
			%>
				<br />- <%=processTXT%></a>
			<%						
				else
			%>
				<br />- <a target="_blank" href="/admin/tempTXT/salesorders/<%=processTXT%>"><%=processTXT%></a>
			<%						
				end if
			end if			
			%>
			</td>
			<td width="65" align="left">
			<%
			if processRatio >= 100 then
			%>
				<%=formatnumber(numOrders, 0)%>
			<%
			else
			%>
				<a href="#" onclick="showPop('<%=processPDF%>','<%=objRsAllOrders("shipToken2")%>');return false;"><%=formatnumber(numOrders, 0)%></a>
			<%
			end if
			%>
			</td>
			<td width="65" align="left"><%=formatnumber(numScan, 0)%></td>
			<td width="65" align="left"><%=formatnumber(numCancel, 0)%></td>
            <td width="65" align="left"><%=formatnumber(numBackorder, 0)%></td>
			<td width="65" align="left">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; font-size:12px;">
					<tr>
						<td align="left"><%=formatnumber(processRatio, 2)%>%</td>
						<td align="right">
						<%
						if processRatio >= 100 then
						%>
							<img src="/images/check.jpg" border="0" width="15"/>
						<%
						else
						%>
							<img src="/images/x.jpg" border="0" width="15"/>
						<%
						end if
						%>                                
						</td>
					</tr>
				</table>
			</td>
		</tr>                
		<%
		end if
		objRsAllOrders.movenext
	loop
	%>
		<tr onMouseOver="this.className='mouseover'" onMouseOut="this.className=''">
			<td colspan="4" align="center">Dropship Orders</td>
			<td colspan="4" align="left"><%=formatnumber(numTotalDropShip, 0)%></td>
			<td align="center">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; font-size:12px;">
					<tr>
						<td align="left"><%=formatnumber(100, 2)%>%</td>
						<td align="right"><img src="/images/check.jpg" border="0" width="15" /></td>
					</tr>
				</table>
			</td>
		</tr>                            
	</table>
	<%
	else
	%>
	No orders processed found
	<%
	end if	
	
call CloseConn(oConn)		
%>