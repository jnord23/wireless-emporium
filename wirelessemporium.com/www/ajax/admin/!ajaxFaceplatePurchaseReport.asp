<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.Open Session("ConnectionString")
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%	
	session("adminID") = prepInt(session("adminID"))
	if session("adminID") = 0 then
		session("adminID") = prepInt(request.Cookies("adminID"))
		if session("adminID") > 0 then
			sql = "insert into we_adminLogin (adminID) values(" & session("adminID") & ")"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
	end if
	adminID = session("adminID")
	
	if adminID = 0 then
		response.Write("You must login")
		response.End()
	end if
	
	dim itemsPerPage : itemsPerPage = 500
	dim blockID : blockID = prepInt(request.QueryString("blockID"))
	if blockID = 0 then blockID = 1
	
	if prepInt(session("fprDays")) = 0 then session("fprDays") = 30
	dim listDirection : listDirection = prepStr(request.QueryString("listDirection"))
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim fullBrandID : fullBrandID = prepInt(request.QueryString("fullBrandID"))
	dim sb : sb = prepStr(request.QueryString("sb"))
	dim fpType : fpType = prepStr(request.QueryString("fpType"))
	dim dsType : dsType = prepStr(request.QueryString("dsType"))
	dim itemDetails : itemDetails = prepInt(request.QueryString("itemDetails"))
	dim voCode : voCode = prepStr(request.QueryString("voCode"))
	dim voPartNumber : voPartNumber = prepStr(request.QueryString("voPartNumber"))
	dim voWePartNumber : voWePartNumber = prepStr(request.QueryString("voWePartNumber"))
	dim voQty : voQty = prepInt(request.QueryString("voQty"))
	dim voCogs : voCogs = prepInt(request.QueryString("voCogs"))
	dim justCogs : justCogs = prepInt(request.QueryString("justCogs"))
	dim dayRange : dayRange = prepInt(request.QueryString("dayRange"))
	dim fprTypeID : fprTypeID = prepInt(request.QueryString("fprTypeID"))
	dim newVendor : newVendor = prepInt(request.QueryString("newVendor"))
	dim updVendor : updVendor = prepStr(request.QueryString("updVendor"))
	dim updVPN : updVPN = prepStr(request.QueryString("updVPN"))
	dim updCogs : updCogs = prepInt(request.QueryString("updCogs"))
	dim ogVend_partNumber : ogVend_partNumber = prepStr(request.QueryString("ogVend_partNumber"))
	dim vendorCode : vendorCode = prepStr(request.QueryString("vendorCode"))
	dim partNumber : partNumber = prepStr(request.QueryString("partNumber"))
	dim cogs : cogs = prepInt(request.QueryString("cogs"))
	dim we_partNumber : we_partNumber = prepStr(request.QueryString("we_partNumber"))
	dim fullVendorCode : fullVendorCode = prepStr(request.QueryString("fullVendorCode"))
	dim cartDetails : cartDetails = prepInt(request.QueryString("cartDetails"))
	dim cartProcessed : cartProcessed = prepInt(request.QueryString("cartProcessed"))
	dim hidePN : hidePN = prepStr(request.QueryString("hidePN"))
	dim dir : dir = prepInt(request.QueryString("dir"))
	dim fullCatID : fullCatID = prepInt(request.QueryString("fullCatID"))
	dim usePN : usePN = prepStr(request.QueryString("usePN"))
	dim soloPartNumber : soloPartNumber = prepStr(request.QueryString("soloPartNumber"))
	dim quickSearch : quickSearch = prepInt(request.QueryString("quickSearch"))
	dim keywords : keywords = prepStr(request.QueryString("keywords"))
	dim vendorHistory : vendorHistory = prepStr(request.QueryString("vendorHistory"))
	dim partnumberList : partnumberList = prepStr(request.QueryString("partnumberList"))
	dim massOrderPN : massOrderPN = prepStr(request.QueryString("massOrderPN"))
	dim massOrderQty : massOrderQty = prepInt(request.QueryString("massOrderQty"))
	dim invDays : invDays = prepInt(request.QueryString("invDays"))
	dim setPurchAmt : setPurchAmt = prepInt(request.QueryString("setPurchAmt"))
	
	if fprTypeID = 0 then fprTypeID = session("fprTypeID") else session("fprTypeID") = fprTypeID
	if dayRange > 0 then session("fprDays") = dayRange
	if setPurchAmt < 0 then setPurchAmt = 0
	
	if vendorHistory <> "" then
%>
<div class="tb" style="background-color:#333; color:#FFF;">
	<div class="fl" style="width:150px; font-weight:bold; margin-right:5px; padding-left:5px;">Vendor</div>
    <div class="fl" style="width:42px; font-weight:bold; margin-right:5px;">OrdAmt</div>
    <div class="fl" style="width:80px; font-weight:bold; margin-right:5px;">OrderDate</div>
    <div class="fl" style="width:42px; font-weight:bold; margin-right:5px;">RcvAmt</div>
    <div class="fl" style="width:35px; font-weight:bold; margin-right:5px;">COGS</div>
    <div class="fl" style="width:80px; font-weight:bold; margin-right:5px;">CompleteDate</div>
    <div class="fl" style="width:85px; font-weight:bold;">Admin</div>
</div>
<%
		sql = "exec sp_purchaseHistory '" & vendorHistory & "'"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		lineColor = "#fff"
		do while not rs.EOF
			vendor = rs("vendorName")
			orderAmt = rs("orderAmt")
			entryDate = dateOnly(rs("entryDate"))
			receivedAmt = rs("receivedAmt")
			cogs = formatCurrency(rs("cogs"),2)
			completeDate = rs("completeOrderDate")
			if not isnull(completeDate) then
				completeDate = dateOnly(completeDate)
			else
				completeDate = "NA"
			end if
			adminUser = rs("weAdmin")
			if not isnull(adminUser) then
				adminUser = left(adminUser,instr(adminUser," ")+1)
			else
				adminUser = "NA"
			end if
			
			if isnull(receivedAmt) then receivedAmt = "NA" else receivedAmt = prepInt(receivedAmt)
%>
<div class="tb" style="background-color:<%=lineColor%>;">
	<div class="fl" style="width:150px; margin-right:5px; padding-left:5px;"><%=vendor%></div>
    <div class="fl" style="width:42px; text-align:right; margin-right:5px;"><%=orderAmt%></div>
    <div class="fl" style="width:80px; text-align:right; margin-right:5px;"><%=entryDate%></div>
    <div class="fl" style="width:42px; text-align:right; margin-right:5px;"><%=receivedAmt%></div>
    <div class="fl" style="width:35px; text-align:right; margin-right:5px;"><%=cogs%></div>
    <div class="fl" style="width:80px; text-align:right; margin-right:5px;"><%=completeDate%></div>
    <div class="fl" style="width:85px;"><%=adminUser%></div>
</div>
<%
			rs.movenext
			if lineColor = "#fff" then lineColor = "#ccc" else lineColor = "#fff"
		loop
		response.End()
	end if
	
	if quickSearch = 1 then
		if partnumberList <> "" then
			partnumberList = "'" & partnumberList & "'"
			partnumberList = replace(partnumberList,"bbrr","','")
			sql =	"select 2 as baseSQL, f.vendorCode, a.itemID, a.PartNumber, i.modelName, i.modelID, a.itemDesc, a.ghost, b.vendor, f.orderAmt, f.receivedAmt, f.entryDate, f.complete, f.process, g.hidden, e.totalQtySold as sales, -1 as itemSales, a.inv_qty, h.boQty, b.partNumber as vendorNumber, j.qtySoldPer30DayAvailable " &_
					"from we_Items a " &_
						"left join we_vendorNumbers b on a.PartNumber = b.we_partNumber " &_
						"left join (select itemID, SUM(quantity) as qtySold from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid where ia.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' and ib.approved = 1 and ib.cancelled is null group by itemID) d on a.itemID = d.itemID " &_
						"left join (select partNumber, SUM(quantity) as totalQtySold from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid where ia.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' and ib.approved = 1 and ib.cancelled is null group by partNumber) e on a.PartNumber = e.partNumber " &_
						"outer apply (select top 1 partNumber, orderAmt, receivedAmt, process, complete, entryDate, vendorCode from we_vendorOrder where partnumber = a.partnumber order by complete, process, entryDate desc) f " &_
						"left join we_ItemsExtendedData g on a.PartNumber = g.partNumber " &_
						"left join (select partnumber, sum(qty) as boQty from we_rma where rmaType = 8 and cancelDate is null and processDate is null group by partnumber) h on a.PartNumber = h.partnumber " &_
						"left join we_Models i on a.modelID = i.modelID " &_
						"left join we_pnDetails j on a.partNumber = j.partNumber " &_
					"where a.partnumber in (" & partnumberList & ") and a.master = 1 and a.hideLive = 0 order by i.modelName, a.PartNumber, f.entryDate desc"
		else
			sql =	"select 2 as baseSQL, f.vendorCode, a.itemID, a.PartNumber, i.modelName, i.modelID, a.itemDesc, a.ghost, b.vendor, f.orderAmt, f.receivedAmt, f.entryDate, f.complete, f.process, g.hidden, e.totalQtySold as sales, -1 as itemSales, a.inv_qty, h.boQty, b.partNumber as vendorNumber, j.qtySoldPer30DayAvailable " &_
					"from we_Items a " &_
						"left join we_vendorNumbers b on a.PartNumber = b.we_partNumber " &_
						"left join (select itemID, SUM(quantity) as qtySold from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid where ia.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' and ib.approved = 1 and ib.cancelled is null group by itemID) d on a.itemID = d.itemID " &_
						"left join (select partNumber, SUM(quantity) as totalQtySold from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid where ia.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' and ib.approved = 1 and ib.cancelled is null group by partNumber) e on a.PartNumber = e.partNumber " &_
						"outer apply (select top 1 partNumber, orderAmt, receivedAmt, process, complete, entryDate, vendorCode from we_vendorOrder where partnumber = a.partnumber order by complete, process, entryDate desc) f " &_
						"left join we_ItemsExtendedData g on a.PartNumber = g.partNumber " &_
						"left join (select partnumber, sum(qty) as boQty from we_rma where rmaType = 8 and cancelDate is null and processDate is null group by partnumber) h on a.PartNumber = h.partnumber " &_
						"left join we_Models i on a.modelID = i.modelID " &_
						"left join we_pnDetails j on a.partNumber = j.partNumber " &_
					"where a.partnumber like '%" & partNumber & "%' and a.itemDesc like '%" & keywords & "%' and a.master = 1 and a.hideLive = 0 " &_
					"order by i.modelName, a.PartNumber, f.entryDate desc"
		end if
		if invDays > 0 then sql = replace(sql,"order by","and (" & invDays & " * j.qtySoldPer30DayAvailable) > a.inv_qty order by")
		session("csvSQL") = sql
		session("errorSQL") = sql
		set itemRS = oConn.execute(sql)		
%>
<div style="width:600px; height:20px; display:block;"></div>
<table align="center" border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="600">
	<tr><td colspan="4" align="right"><a href="/admin/faceplatePurchaseReport_csv.asp" target="_blank">Create CSV</a></td></tr>
    <tr bgcolor="#333333" style="font-weight:bold; color:#FFF;">
    	<td align="center" title="topForm">#</td>
        <td align="left" style="cursor:pointer;" onclick="document.getElementById('results').innerHTML='<div style=margin-top:30px;>LOADING DATA<br>PLEASE WAIT</div>';ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullVendorCode=<%=fullVendorCode%>&fullCatID=<%=fullCatID%>&modelID=<%=modelID%>&fpType=<%=fpType%>&dsType=<%=dsType%>','results')">Item</td>
        <td align="center" style="cursor:pointer;" onclick="document.getElementById('results').innerHTML='<div style=margin-top:30px;>LOADING DATA<br>PLEASE WAIT</div>';ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullVendorCode=<%=fullVendorCode%>&fullCatID=<%=fullCatID%>&modelID=<%=modelID%>&fpType=<%=fpType%>&sb=Sold&dsType=<%=dsType%>','results')">Sold</td>
        <td align="center" style="cursor:pointer;" onclick="document.getElementById('results').innerHTML='<div style=margin-top:30px;>LOADING DATA<br>PLEASE WAIT</div>';ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullVendorCode=<%=fullVendorCode%>&fullCatID=<%=fullCatID%>&modelID=<%=modelID%>&fpType=<%=fpType%>&sb=Inv&dsType=<%=dsType%>','results')">Inv</td>
    </tr>
	<%
	rowNum = 0
	soldCnt = 0
	invCnt = 0
	bgColor = "#ffffff"
	curPartNumber = ""
	partList = ""
	session("partList") = ""
	
	do while not itemRS.EOF
		irsItemID = itemRS("itemID")
		irsItemDesc = itemRS("itemDesc")
		irsVendorCode = itemRS("vendorCode")
		irsOrderAmt = itemRS("orderAmt")
		irsReceivedAmt = itemRS("receivedAmt")
		irsEntryDate = itemRS("entryDate")
		irsComplete = itemRS("complete")
		irsProcess = itemRS("process")
		irsHidden = itemRS("hidden")
		irsGhost = itemRS("ghost")
		if irsHidden then
			if session("showHidden") = 1 then
				isHidden = 1
				irsHidden = 0
			else
				isHidden = 0
				irsHidden = 1
			end if
		else
			irsHidden = 0
			isHidden = 0
		end if
		irsPartNumber = itemRS("partNumber")
		irsSales = prepInt(itemRS("sales"))
		irsItemSales = prepInt(itemRS("itemSales"))
		irsSellThrough = round(itemRS("qtySoldPer30DayAvailable"),2)
		irsBackOrders = prepInt(itemRS("boQty"))
		irsInv_qty = prepInt(itemRS("inv_qty"))
		irsVendorNumber = itemRS("vendorNumber")
		soldCnt = soldCnt + irsSales
		invCnt = invCnt + irsInv_qty
		
		session("partList") = session("partList") & "'" & irsPartNumber & "',"
		
		if instr(partList,irsPartNumber) > 0 then
			irsHidden = 1
		else
			partList = partList & irsPartNumber & "##"
		end if
		
		if len(irsItemDesc) > 90 then useItemDesc = left(irsItemDesc,90) & "..." else useItemDesc = irsItemDesc
		
		if curPartNumber <> irsPartNumber and irsHidden = 0 then
			rowNum = rowNum + 1
			curPartNumber = irsPartNumber
			if not isnull(irsEntryDate) and instr(irsEntryDate," ") > 0 then irsEntryDate = left(irsEntryDate,instr(irsEntryDate," ")-1)
			if isnull(irsProcess) then irsProcess = true
			if not irsProcess then
				curBgColor = bgColor
				bgColor = "#009900"
			elseif irsProcess and not irsComplete then
				curBgColor = bgColor
				bgColor = "#0087d3"
			elseif isnull(irsVendorNumber) then
				curBgColor = bgColor
				bgColor = "#ff9999"
			elseif prepInt(irsBackOrders) > 0 then
				curBgColor = bgColor
				bgColor = "#999900"
			end if
	%>
	<tr bgcolor="<%=bgColor%>">
    	<td colspan="4">
        	<div class="fl numCol">
            	<%=rowNum%>
            	<% if isHidden = 1 then %><br /><span class="ft8px">Hidden</span><% end if %>
            	<% if irsGhost then %><br /><span class="ft8px">Ghost</span><% end if %>
            </div>
            <div class="fl itemCol">
            	<div class="tb itemTableRow" onclick="expandData(<%=irsItemID%>,'<%=irsPartNumber%>')">
                    <div class="tb pb5"><%=useItemDesc%></div>
                    <div class="tb"><span class="ft14px"><%=irsPartNumber%></span> (<%=irsItemID%>) VN:<%=irsVendorNumber%></div>
                    <% if not isnull(irsOrderAmt) then %>
                        <% if not irsProcess then %>
                    <div class="tb onOrder">On Order</div>
                        <% else %>
                    <div class="tb lastPurch">Last Vendor Purchase From: <%=irsVendorCode%>, Qty: <%=irsOrderAmt%>, On: <%=irsEntryDate%> (<% if irsComplete then %>Received <%=irsReceivedAmt%><% else %>Awaiting<% end if %>)</div>
                        <% end if %>
                    <% end if %>
                </div>
            </div>
            <div class="fl soldCol">
            	<div class="fr topNum" title="Sales by partnumber"><%=irsSales%></div>
            	<div title="Sales by itemID" class="fr exSalesData">(<%=irsItemSales%>)</div>
                <div title="30 Day Run Rate" id="runRate_<%=irsItemID%>" class="fr sellThrough"><%=irsSellThrough * 30%></div>
            </div>
            <div class="fl invCol">
	            <div class="fr topNum" title="Current Inventory" id="curInv_<%=irsItemID%>"><%=irsInv_qty%></div>
    	        <div title="Current Backorders" class="fr exSalesData">(<%=irsBackOrders%>)</div>
            </div>
            <div class="tb loadingBox" style="display:none;" id="item_<%=irsItemID%>">Loading Data...</div>
            <input type="hidden" id="partnumber_o<%=rowNum%>" value="<%=irsPartNumber%>" />
            <input type="hidden" id="itemID_o<%=rowNum%>" value="<%=irsItemID%>" />
        </td>
    </tr>
    <%
			if bgColor <> "#ffffff" and bgColor <> "#ebebeb" then bgColor = curBgColor
			if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
		end if
		itemRS.movenext
	loop
	
	if instr(sql,"sp_purchaseAppVendorPull") > 0 then
	%>
    <tr bgcolor="#000000">
    	<td colspan="2" align="left">
        	<% if blockID-500 > 0 then %>
        	<a href="javascript:pageItemList('<%=fullVendorCode%>',<%=fprTypeID%>,<%=blockID-itemsPerPage%>,'<%=sb%>',<%=listDirection%>);" style="color:#FFF;">Previous Page</a>
            <% end if %>
        </td>
        <td colspan="2" align="right">
        	<a href="javascript:pageItemList('<%=fullVendorCode%>',<%=fprTypeID%>,<%=blockID+itemsPerPage%>,'<%=sb%>',<%=listDirection%>);" style="color:#FFF;">Next Page</a>
        </td>
    </tr>
    <%
	end if
	%>
    <tr bgcolor="#000000">
    	<td colspan="4">
        	<div style="float:right; color:#FFF; font-weight:bold;">Sold Total: <%=formatNumber(soldCnt,0)%></div>
            <div style="float:right; color:#FFF; font-weight:bold; padding-right:20px;">Inv Total: <%=formatNumber(invCnt,0)%></div>
            <input type="hidden" id="fullResultCnt" value="<%=rowNum%>" />
        </td>
    </tr>
</table>
<%
		response.End()
	end if
	
	if hidePN <> "" then
		if dir = 1 then
			sql = "if (select count(*) from we_ItemsExtendedData where partNumber = '" & hidePN & "') > 0 update we_ItemsExtendedData set hidden = 1 where partNumber = '" & hidePN & "' else insert into we_ItemsExtendedData (partNumber,hidden) values('" & hidePN & "',1)"
			session("errorSQL") = sql
			oConn.execute(sql)
		%>
        <a class="prodLinkOption" onclick="ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?hidePN=<%=hidePN%>&dir=0&itemDetails=<%=itemDetails%>','hidePN_<%=itemDetails%>')">Unhide Product</a>
        <%
		else
			sql = "if (select count(*) from we_ItemsExtendedData where partNumber = '" & hidePN & "') > 0 update we_ItemsExtendedData set hidden = 0 where partNumber = '" & hidePN & "' else insert into we_ItemsExtendedData (partNumber) values('" & hidePN & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
		%>
        <a class="prodLinkOption" onclick="ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?hidePN=<%=hidePN%>&dir=1&itemDetails=<%=itemDetails%>','hidePN_<%=itemDetails%>')">Hide Product</a>
        <%
		end if
		response.End()
	elseif cartDetails > 0 then
		if cartProcessed > 0 then
			sql = "select a.orderID, a.vendorCode, d.name, sum(a.orderAmt) as orderAmt, sum(a.orderAmt * b.cogs) as totalCogs, sum(a.orderAmt * c.COGS) as totalGeneral, a.processOrderDate from we_vendorOrder a left join we_vendorNumbers b on a.partNumber = b.we_partNumber and a.vendorCode = b.vendor left join we_Items c on a.partNumber = c.PartNumber and c.master = 1 join we_vendors d on a.vendorCode = d.code where a.complete = 0 and a.process = 1 and adminID = " & adminID & " group by a.orderID, a.vendorCode, d.name, a.processOrderDate order by d.name"
		else
			sql = "select a.orderID, a.vendorCode, d.name, sum(a.orderAmt) as orderAmt, sum(a.orderAmt * b.cogs) as totalCogs, sum(a.orderAmt * c.COGS) as totalGeneral, a.processOrderDate from we_vendorOrder a left join we_vendorNumbers b on a.partNumber = b.we_partNumber and a.vendorCode = b.vendor left join we_Items c on a.partNumber = c.PartNumber and c.master = 1 join we_vendors d on a.vendorCode = d.code where a.complete = 0 and a.process = 0 and adminID = " & adminID & " group by a.orderID, a.vendorCode, d.name, a.processOrderDate order by d.name"
		end if
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		%>
        <table border="1" bordercolor="#000000" cellpadding="1" cellspacing="0" style="font-size:9px;">
            <tr bgcolor="#333333" style="font-weight:bold; color:#FFF;">
            	<td colspan="5" align="center" style="font-weight:bold; font-size:14px;">
                    <% if cartProcessed > 0 then %>
                    Currently Processed<br />
                    <div style="text-decoration:underline; color:#C9C; font-size:9px; cursor:pointer;" onclick="ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?cartDetails=1','floatingCart')">Show On Order</div>
                    <% else %>
                    Currently On Order<br />
                    <div style="text-decoration:underline; color:#C9C; font-size:9px; cursor:pointer;" onclick="ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?cartDetails=1&cartProcessed=1','floatingCart')">Show Processed Orders</div>
                    <% end if %>
                </td>
            </tr>
            <tr bgcolor="#333333" style="font-weight:bold; color:#FFF;">
                <td align="left">Vendor</td>
                <% if cartProcessed > 0 then %>
                <td align="left">OrderID</td>
                <% end if %>
                <td align="left" nowrap="nowrap">Itm Cnt</td>
                <td align="left" nowrap="nowrap">Approx Total</td>
                <% if cartProcessed > 0 then %>
                <td align="left" nowrap="nowrap">Date</td>
                <% end if %>
            </tr>
            <%
			if rs.EOF then
			%>
            <tr><td colspan="3" align="center"><div style="padding:10px 0px 10px 0px; font-weight:bold; color:#F00;">No Open Orders</div></td></tr>
            <%
			else
				bgColor = "#ffffff"
				do while not rs.EOF
			%>
            <tr bgcolor="<%=bgColor%>" style="cursor:pointer;" onclick="window.open('/admin/vendorOrders.asp?vendorCode=<%=rs("vendorCode")%>&processed=<%=cartProcessed%>','Vendor Order')">
            	<td align="left" title="<%=rs("orderID")%>"><%=rs("name")%></td>
                <% if cartProcessed > 0 then %>
                <td align="left"><%=rs("orderID")%></td>
                <% end if %>
                <td align="right"><%=formatNumber(rs("orderAmt"),0)%></td>
                <td align="right"><%=formatCurrency(prepInt(rs("totalGeneral")),2)%></td>
                <% if cartProcessed > 0 then %>
                <td align="right"><%=dateOnly(rs("processOrderDate"))%></td>
                <% end if %>
            </tr>
            <%
					rs.movenext
					if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
				loop
			end if
			%>
        </table>
        <%
		
		response.End()
	elseif newVendor > 0 then
		sql = "select name, code, (select partNumber from we_items where itemID = " & newVendor & ") as we_partNumber from we_vendors order by name"
		session("errorSQL") = sql
		set vendorRS = oConn.execute(sql)
		
		if vendorRS.EOF then
			response.Write("Error pulling vendor data")
			response.End()
		end if
%>
<div style="float:left; width:370px;">
	<form name="newVendorForm_<%=newVendor%>" onsubmit="return(false)">
    <div style="float:left; width:300px;">
    	<select name="vendorCode">
        	<option value="">Select Vendor</option>
            <%
			do while not vendorRS.EOF
				vendorName = vendorRS("name")
				vendorCode = vendorRS("code")
				we_partNumber = vendorRS("we_partNumber")
			%>
            <option value="<%=vendorCode%>"<% if vendorCode = updVendor then %> selected="selected"<% end if %>><%=vendorName%> (<%=vendorCode%>)</option>
            <%
				vendorRS.movenext
			loop
			%>
        </select>
    </div>
    <div style="float:left; width:200px;">Part Number: <input type="text" name="partNumber" value="<%=updVPN%>" size="15" /></div>
    <div style="float:left; width:100px;">Cogs: <input type="text" name="cogs" value="<%=updCogs%>" size="5" /></div>
    <div style="float:left; width:70px;">
    	<input type="button" name="myAction" value="Save" onclick="addVendor(<%=newVendor%>)" />
        <input type="hidden" name="we_partNumber" value="<%=we_partNumber%>" />
        <input type="hidden" name="ogVend_partNumber" value="<%=updVPN%>" />
    </div>
    </form>
</div>
<%
		response.End()
	elseif vendorCode <> "" and partNumber <> "" then
		sql = "if (select count(*) from we_vendorNumbers where vendor = '" & vendorCode & "' and (partNumber = '" & ogVend_partNumber & "' or partNumber = '" & partNumber & "')) > 0 update we_vendorNumbers set partNumber = '" & partNumber & "', cogs = " & cogs & " where vendor = '" & vendorCode & "' and partNumber = '" & ogVend_partNumber & "' else insert into we_vendorNumbers (we_partNumber,vendor,partNumber,cogs) values ('" & we_partNumber & "','" & vendorCode & "','" & partNumber & "'," & cogs & ")"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		response.Write("vendor added to system: " & sql)
		response.End()
	end if
	
	if itemDetails > 0 then
		sql = "exec sp_purchaseAppProductDetails " & itemDetails & ",'" & dateadd("d",(session("fprDays")*-1),date) & "','" & usePN & "'"
		session("errorSQL") = sql
		set itemRS = oConn.execute(sql)
		
		sql = "exec sp_purchToolVendorData " & itemDetails
		session("errorSQL") = sql
		set vendorRS = oConn.execute(sql)
		
		dim orderCnt : orderCnt = 0
		dim curPartNumber : curPartNumber = itemRS("partNumber")
		dim sale60 : sale60 = itemRS("sale60")
		dim sale90 : sale90 = itemRS("sale90")
		dim itemPic : itemPic = itemRS("itemPic")
		dim itemCogs : itemCogs = itemRS("cogs")
		dim hidden : hidden = itemRS("hidden")
		dim masterItemID : masterItemID = prepInt(itemRS("masterItemID"))
		dim lastOrderDate : lastOrderDate = itemRS("lastOrderDate")
		dim baseCode : baseCode = vendorRS("baseCode")
		dim vendor : vendor = vendorRS("vendor")
		dim notes : notes = itemRS("notes")
		partNumber = vendorRS("partNumber")
		wePartNumber = vendorRS("we_partNumber")
		if isnull(hidden) then hidden = false
		if isdate(lastOrderDate) then lastOrderDate = dateOnly(lastOrderDate) else lastOrderDate = "NA"
		dim itemAvgShip : itemAvgShip = itemRS("avgShip")
		
		dim weCnt : weCnt = 0
		dim caCnt : caCnt = 0
		dim coCnt : coCnt = 0
		dim psCnt : psCnt = 0
		dim tmCnt : tmCnt = 0
		do while not itemRS.EOF
			orderCnt = orderCnt + itemRS("totalOrders")
			if itemRS("store") = 0 then weCnt = itemRS("totalOrders")
			if itemRS("store") = 1 then caCnt = itemRS("totalOrders")
			if itemRS("store") = 2 then coCnt = itemRS("totalOrders")
			if itemRS("store") = 3 then psCnt = itemRS("totalOrders")
			if itemRS("store") = 10 then tmCnt = itemRS("totalOrders")
			itemRS.movenext
		loop
		'################ Print Item Expanded Data #####################
%>
<div style="display:table;">
    <div style="float:left; width:100px; height:100px;"><img src="/productPics/thumb/<%=itemPic%>" border="0" width="100" height="100" /></div>
    <div style="float:left; width:370px; padding-left:15px;">
        <div style="display:table; width:300px;">
        	<div style="float:left;">Sale 60: <%=sale60%></div>
            <div style="float:left; margin-left:15px;">Sale 90: <%=sale90%></div>
        </div>
        <div style="width:300px; display:block; height:15px;">Last Order Date: <%=lastOrderDate%></div>
        <div style="width:300px; display:block; height:15px;">Total Orders: <%=orderCnt%></div>
        <div style="width:300px; display:block; height:15px;">
            <div style="float:left;">WE: <%=prepInt(weCnt)%></div>
            <div style="float:left; padding-left:10px;">CO: <%=prepInt(coCnt)%></div>
            <div style="float:left; padding-left:10px;">CA: <%=prepInt(caCnt)%></div>
            <div style="float:left; padding-left:10px;">PS: <%=prepInt(psCnt)%></div>
            <div style="float:left; padding-left:10px;">TM: <%=prepInt(tmCnt)%></div>
            <div style="float:left; padding-left:10px;"><a href="/admin/productNotes.asp?partNumber=<%=curPartNumber%>" target="_blank"><img src="/images/notes<% if notes = 0 then %>_none<% end if %>.gif" border="0" /></a></div>
        </div>
        <div style="width:300px; display:table; height:15px;">Overall Item Average Ship Time: <%=formatNumber(itemAvgShip,2)%></div>
        <div style="display:table; width:500px; margin-top:5px; padding-top:5px;">
            <%
            if isnull(vendor) then
            %>
            <div style="float:left; padding-left:10px;">No Vendor Number (<%=baseCode%>)</div>
            <%
            else
                dim vLap : vLap = 0
                do while not vendorRS.EOF
                    vLap = vLap + 1
                    baseCode = vendorRS("baseCode")
                    vendor = vendorRS("vendor")
                    partNumber = vendorRS("partNumber")
                    wePartNumber = vendorRS("we_partNumber")
                    if isnull(vendorRS("cogs")) then useCogs = prepInt(itemCogs) else useCogs = prepInt(vendorRS("cogs"))
					avgShip = vendorRS("avgShip")
            %>
            <form name="vendor_<%=itemDetails%>_<%=vLap%>" action="#" method="post" style="padding:0px; margin:0px;" onsubmit="return(false)">
            <div style="height:25px; display:table;">
                <div style="float:left; cursor:pointer; width:130px; overflow:hidden;" onclick="updateVendorPN('<%=vendor%>','<%=partNumber%>',<%=useCogs%>,<%=itemDetails%>)"><%=vendor%> (<%=partNumber%>)</div>
                <div style="float:left; padding-left:5px;">QTY: <input type="text" name="qty" value="<%=setPurchAmt%>" size="1" style="font-size:10px;" /></div>
                <div style="float:left; padding-left:5px;" id="cogs_<%=itemDetails%>">COGS: <input type="text" name="cogs" value="<%=useCogs%>" size="5" style="font-size:10px;" onchange="updateCogs('<%=vendor%>','<%=partNumber%>','<%=wePartNumber%>',<%=itemDetails%>,this.value)" /></div>
                <div style="float:left; padding-left:5px;"><input type="button" name="mySub" value="Add to Order" style="font-size:10px;" onclick="addToOrder('<%=vendor%>','<%=partNumber%>','<%=wePartNumber%>',document.vendor_<%=itemDetails%>_<%=vLap%>.qty.value,<%=itemDetails%>,document.vendor_<%=itemDetails%>_<%=vLap%>.cogs.value)" /></div>
                <% if prepInt(vendorRS("orderAmt")) > 0 then %>
    	        <div style="color:#F00; height:20px; text-align:left; float:left;">CURRENTLY ON ORDER: <%=vendorRS("orderAmt")%></div>
	            <%
				else
					if isnull(avgShip) then avgShip = "Unknown" else avgShip = formatNumber(avgShip,0)
				%>
                <div style="color:#000; height:20px; text-align:left; float:left; margin-left:5px;">Ship Time: <%=avgShip%> days</div>
				<% end if %>
            </div>
            </form>
            <%
                    vendorRS.movenext
                loop
            end if
            %>
            <div style="padding-top:5px; display:table; width:300px;">
                <div style="float:left;"><a class="prodLinkOption" onclick="showNewVendor(<%=itemDetails%>)">Enter New Vendor Data</a></div>
                <div style="float:left; padding-left:10px;"><a class="prodLinkOption" onclick="window.open('/admin/db_update_price.asp?partNumber=<%=curPartNumber%>','Price Data')">Set On Sale</a></div>
                <% if hidden then %>
                <div style="float:left; padding-left:10px;" id="hidePN_<%=itemDetails%>"><a class="prodLinkOption" onclick="ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?hidePN=<%=curPartNumber%>&dir=0&itemDetails=<%=itemDetails%>','hidePN_<%=itemDetails%>')">Unhide Product</a></div>
                <% else %>
                <div style="float:left; padding-left:10px;" id="hidePN_<%=itemDetails%>"><a class="prodLinkOption" onclick="ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?hidePN=<%=curPartNumber%>&dir=1&itemDetails=<%=itemDetails%>','hidePN_<%=itemDetails%>')">Hide Product</a></div>
                <% end if %>
                <div style="float:left; padding-left:10px;"><a class="prodLinkOption" onclick="window.open('/admin/db_update_inv.asp?partNumber=<%=curPartNumber%>','Price Data')">Set HideLive/Ghost</a></div>
                <div style="float:left; padding-left:10px;"><a class="prodLinkOption" onclick="showHistory(<%=masterItemID%>,<%=itemDetails%>)">Inv History</a></div>
                <div style="float:left; padding-left:10px;"><a class="prodLinkOption" onclick="window.open('/admin/vendorPartNumbers_enter.asp?partNumber=<%=curPartNumber%>','vendor number data')">Edit Vendor Numbers</a></div>
                <div style="float:left; padding-left:10px;"><a class="prodLinkOption" onclick="showPurch('<%=curPartNumber%>','<%=itemDetails%>')">View Purchase Data</a></div>
            </div>
        </div>
    </div>
	<div style="width:100%; display:table;" id="newVendor_<%=itemDetails%>"></div>
</div>
<%
		response.End()
	elseif voCode <> "" then
		if justCogs = 1 then
			if prepInt(adminID) = 0 then
				response.Write("<div style='color:#F00;'>ERROR</div>")
			else
				sql = "insert into we_cogsHistory (vendor,partNumber,cogs,adminID) values('" & voCode & "','" & voPartNumber & "'," & voCogs & "," & adminID & ")"
				session("errorSQL") = sql
				oConn.execute(sql)
				
				sql = "update we_vendorNumbers set cogs = " & voCogs & " where vendor = '" & voCode & "' and partNumber = '" & voPartNumber & "'"
				session("errorSQL") = sql
				oConn.execute(sql)
				
				response.Write("<div style='color:#F00;'>COGS UPDATED</div>")
			end if
		else
			if prepInt(adminID) = 0 then
				response.Write("<div style='color:#F00;'>ERROR</div>")
			else
				if massOrderPN <> "" then
					sql = "select top 1 vendorPN, cogs from we_vendorOrder where partNumber = '" & massOrderPN & "' and vendorCode = '" & voCode & "' order by id desc"
					session("errorSQL") = sql
					set massOrderRS = oConn.execute(sql)
					
					if not massOrderRS.EOF then
						voPartNumber = massOrderRS("vendorPN")
						voCogs = massOrderRS("cogs")
						
						if isnull(voCogs) then
							sql = "select top 1 cogs from we_Items where partnumber = '" & massOrderPN & "' order by itemID desc"
							session("errorSQL") = sql
							set massOrderRS = oConn.execute(sql)
							
							voCogs = massOrderRS("cogs")
						end if
					else
						sql = "select partNumber, cogs from we_vendorNumbers where vendor = '" & voCode & "' and we_partNumber = '" & massOrderPN & "'"
						session("errorSQL") = sql
						set massOrderRS = oConn.execute(sql)
						
						if not massOrderRS.EOF then
							voPartNumber = massOrderRS("partNumber")
							voCogs = massOrderRS("cogs")
							
							if isnull(voCogs) then
								sql = "select top 1 cogs from we_Items where partnumber = '" & massOrderPN & "' order by itemID desc"
								session("errorSQL") = sql
								set massOrderRS = oConn.execute(sql)
								
								voCogs = massOrderRS("cogs")
							end if
						else
							response.Write("error!")
							response.End()
						end if
					end if
					if massOrderQty = 0 then
						sql = "delete from we_vendorOrder where vendorCode = '" & voCode & "' and partNumber = '" & massOrderPN & "' and adminID = " & prepInt(adminID) & " and process = 0 and complete = 0 and rowProcessed = 0"
					else
						'sql = "if (select count(*) from we_vendorOrder where partNumber = '" & massOrderPN & "' and adminID = " & prepInt(adminID) & " and vendorCode = '" & voCode & "' and (process = 0 or process = 1) and complete = 0 and rowProcessed = 0) < 1 insert into we_vendorOrder (vendorCode,partNumber,vendorPN,orderAmt,cogs,adminID) values('" & voCode & "','" & massOrderPN & "','" & voPartNumber & "'," & massOrderQty & "," & voCogs & "," & prepInt(adminID) & ") else update we_vendorOrder set orderAmt = " & massOrderQty & ", cogs = " & voCogs & " where vendorCode = '" & voCode & "' and vendorPN = '" & voPartNumber & "' and adminID = " & prepInt(adminID) & " and process = 0 and complete = 0 and rowProcessed = 0"
						sql = "if (select count(*) from we_vendorOrder where partNumber = '" & massOrderPN & "' and (process = 0 or process = 1) and complete = 0 and rowProcessed = 0) < 1 insert into we_vendorOrder (vendorCode,partNumber,vendorPN,orderAmt,cogs,adminID) values('" & voCode & "','" & massOrderPN & "','" & voPartNumber & "'," & massOrderQty & "," & voCogs & "," & prepInt(adminID) & ")"
					end if
					session("errorSQL") = sql
					oConn.execute(sql)
					
					response.Write("Vendor Order Updated")
					response.End()
				else
					sql = "insert into we_cogsHistory (vendor,partNumber,cogs,adminID) values('" & voCode & "','" & voPartNumber & "'," & voCogs & "," & adminID & ")"
					session("errorSQL") = sql
					oConn.execute(sql)
					'############## Add Qty To Vendor Order ####################
					if voQty = 0 then
						sql = "delete from we_vendorOrder where vendorCode = '" & voCode & "' and vendorPN = '" & voPartNumber & "' and adminID = " & adminID & " and process = 0 and complete = 0 and rowProcessed = 0"
					else
						sql = "if (select count(*) from we_vendorOrder where vendorCode = '" & voCode & "' and vendorPN = '" & voPartNumber & "' and adminID = " & adminID & " and process = 0 and complete = 0 and rowProcessed = 0) < 1 insert into we_vendorOrder (vendorCode,partNumber,vendorPN,orderAmt,cogs,adminID) values('" & voCode & "','" & voWePartNumber & "','" & voPartNumber & "'," & voQty & "," & voCogs & "," & prepInt(adminID) & ") else update we_vendorOrder set orderAmt = " & voQty & ", cogs = " & voCogs & " where vendorCode = '" & voCode & "' and vendorPN = '" & voPartNumber & "' and adminID = " & adminID & " and process = 0 and complete = 0 and rowProcessed = 0"
					end if
					session("errorSQL") = sql
					oConn.execute(sql)
					
					response.Write("Vendor Order Updated")
				end if
			end if
		end if
		response.End()
	end if
	
	if dsType = "" then dsType = "wh"
	if dsType = "wh" then dsVal = " and partNumber not like '%-MLD-%' and partNumber not like '%-HYP-%' and (c.vendor is null or c.vendor <> 'cm' and c.vendor <> 'ds')"
	if dsType = "ds" then dsVal = " and (partNumber like '%-MLD-%' or partNumber like '%-HYP-%' or c.vendor = 'cm' or c.vendor = 'ds')"
	
	if fullVendorCode <> "" then
		if brandID > 0 then useBrandID = brandID
		if fullBrandID > 0 then useBrandID = fullBrandID
		if fprTypeID = 0 then
			if useBrandID > 0 then
				sql = "select name as modelName, (select COUNT(*) from we_vendorNumbers a join we_Items b on a.we_partNumber = b.PartNumber and b.hideLive = 0 where a.vendor = '" & fullVendorCode & "' and b.brandID = " & useBrandID & ") as varients, (select SUM(quantity) from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID and c.brandID = " & useBrandID & " join we_vendorNumbers d on c.PartNumber = d.we_partNumber and d.vendor = '" & fullVendorCode & "' where b.approved = 1 and b.cancelled is null and b.orderdatetime > '" & date-30 & "') as qtySold, (select COUNT(*) from we_vendorNumbers a join we_Items b on a.we_partNumber = b.PartNumber and b.brandID = " & useBrandID & " and b.master = 1 and b.inv_qty > 0 where a.vendor = '" & fullVendorCode & "') as inStock from we_vendors where code = '" & fullVendorCode & "'"
			else
				sql = "select name as modelName, (select COUNT(*) from we_vendorNumbers a join we_Items b on a.we_partNumber = b.PartNumber and b.hideLive = 0 where a.vendor = '" & fullVendorCode & "') as varients, (select SUM(quantity) from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID join we_vendorNumbers d on c.PartNumber = d.we_partNumber and d.vendor = '" & fullVendorCode & "' where b.approved = 1 and b.cancelled is null and b.orderdatetime > '" & date-30 & "') as qtySold, (select COUNT(*) from we_vendorNumbers a join we_Items b on a.we_partNumber = b.PartNumber and b.master = 1 and b.inv_qty > 0 where a.vendor = '" & fullVendorCode & "') as inStock from we_vendors where code = '" & fullVendorCode & "'"
			end if
		else
			if useBrandID > 0 then
				sql = "select name as modelName, (select COUNT(*) from we_vendorNumbers a join we_items b on a.we_partNumber = b.partNumber and b.typeID = " & fprTypeID & " and b.brandID = " & useBrandID & " where a.vendor = '" & fullVendorCode & "') as varients, (select SUM(quantity) from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID and c.typeID = " & fprTypeID & " and c.brandID = " & useBrandID & " join we_vendorNumbers d on c.PartNumber = d.we_partNumber and d.vendor = '" & fullVendorCode & "' where b.approved = 1 and b.cancelled is null and b.orderdatetime > '" & date-30 & "') as qtySold, (select COUNT(*) from we_vendorNumbers a join we_Items b on a.we_partNumber = b.PartNumber and b.typeID = " & fprTypeID & " and b.brandID = " & useBrandID & " and b.master = 1 and b.inv_qty > 0 where a.vendor = '" & fullVendorCode & "') as inStock from we_vendors where code = '" & fullVendorCode & "'"
			else
				sql = "select name as modelName, (select COUNT(*) from we_vendorNumbers a join we_items b on a.we_partNumber = b.partNumber and b.typeID = " & fprTypeID & " where a.vendor = '" & fullVendorCode & "') as varients, (select SUM(quantity) from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID and c.typeID = " & fprTypeID & " join we_vendorNumbers d on c.PartNumber = d.we_partNumber and d.vendor = '" & fullVendorCode & "' where b.approved = 1 and b.cancelled is null and b.orderdatetime > '" & date-30 & "') as qtySold, (select COUNT(*) from we_vendorNumbers a join we_Items b on a.we_partNumber = b.PartNumber and b.typeID = " & fprTypeID & " and b.master = 1 and b.inv_qty > 0 where a.vendor = '" & fullVendorCode & "') as inStock from we_vendors where code = '" & fullVendorCode & "'"
			end if
		end if
	elseif brandID > 0 then
		sql = "select modelID, modelName from we_models where brandID = " & brandID & " order by modelName"
	elseif modelID > 0 then
		modelDsVal = replace(dsVal,"partNumber","ia.partNumber")
		modelDsVal = replace(modelDsVal,"c.vendor","ia.vendor")
		if vendorCode <> "" then
			if fprTypeID > 0 then
				sql = 	"select 1 as sqlBase, a.modelID as modelID, b.modelName, count(*) as varients, (" &_
							"select SUM(quantity) " &_
							"from we_orderdetails ia with (nolock) " &_
								"join we_orders ib with (nolock) on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' " &_
								"join we_Items ic with (nolock) on ia.itemid = ic.itemID and ic.hideLive = 0 and ic.typeID = " & fprTypeID & " and ic.modelID = " & modelID & " " &_
								"join we_vendorNumbers id on ic.partnumber = id.we_partNumber and id.vendor = '" & vendorCode & "'" &_
						") as qtySold, (" &_
							"select count(*) " &_
							"from we_items ia " &_
								"left join we_items ib on ia.partNumber = ib.partNumber and ib.master = 1 " &_
								"join we_vendorNumbers ic on ia.partNumber = ic.we_partNumber and ic.vendor = '" & vendorCode & "' " &_
							"where ia.modelID = " & modelID & " and ia.typeID = " & fprTypeID & " and ia.hideLive = 0 and ib.inv_qty > 0" & modelDsVal &_
						") as inStock " &_
						"from we_items a " &_
							"left join we_models b on a.modelID = b.modelID " &_
							"join we_vendorNumbers c on a.partNumber = c.we_partNumber and c.vendor = '" & vendorCode & "' " &_
						"where a.modelID = " & modelID & " and a.typeID = " & fprTypeID & " and a.hideLive = 0 " & replace(dsVal,"partNumber","a.partNumber") & " " &_
						"group by a.modelID, b.modelName, a.partNumber"
			else
				if fpType <> "" then
					sql = 	"select 2 as sqlBase, a.modelID, b.modelName as modelName, count(*) as varients, (" &_
							"select SUM(quantity) from we_orderdetails ia with (nolock) join we_orders ib with (nolock) on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' join we_Items ic with (nolock) on ia.itemid = ic.itemID and a.partNumber like '" & fpType & "%' and ic.hideLive = 0 and ic.modelID = " & modelID & " join we_vendorNumbers id on ic.partnumber = id.we_partNumber and id.vendor = '" & vendorCode & "'" &_
							") as qtySold, (" &_
							"select count(*) from we_items ia left join we_items ib on ia.partNumber = ib.partNumber and ib.master = 1 join we_vendorNumbers ic on ia.partNumber = ic.we_partNumber and ic.vendor = '" & vendorCode & "' where ia.partNumber like '" & fpType & "%' and ia.modelID = " & modelID & " and ia.hideLive = 0 and ib.inv_qty > 0" & modelDsVal &_
							") as inStock from we_items a left join we_models b on a.modelID = b.modelID join we_vendorNumbers c on a.partNumber = c.we_partNumber and c.vendor = '" & vendorCode & "' where a.partNumber like '" & fpType & "%' and a.modelID = " & modelID & " and a.hideLive = 0 " & replace(dsVal,"partNumber","a.partNumber") & " group by a.modelID, b.modelName, a.partNumber"
				else
					sql = 	"select 3 as sqlBase, a.modelID, b.modelName as modelName, count(*) as varients, (" &_
							"select SUM(quantity) from we_orderdetails ia with (nolock) join we_orders ib with (nolock) on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' join we_Items ic with (nolock) on ia.itemid = ic.itemID and ic.hideLive = 0 and ic.modelID = " & modelID & " join we_vendorNumbers id on ic.partnumber = id.we_partNumber and id.vendor = '" & vendorCode & "'" &_
							") as qtySold, (" &_
							"select count(*) from we_items ia left join we_items ib on ia.partNumber = ib.partNumber and ib.master = 1 join we_vendorNumbers ic on ia.partNumber = ic.we_partNumber and ic.vendor = '" & vendorCode & "' where ia.modelID = " & modelID & " and ia.hideLive = 0 and ib.inv_qty > 0" & modelDsVal &_
							") as inStock from we_items a left join we_models b on a.modelID = b.modelID left join we_vendorNumbers c on a.partNumber = c.we_partNumber where a.modelID = " & modelID & " and a.hideLive = 0 " & replace(dsVal,"partNumber","a.partNumber") & " and (c.vendor = '" & vendorCode & "' or a.vendor like '%" & vendorCode & "%') group by a.modelID, b.modelName"
				end if
			end if
		else
			if fprTypeID > 0 then
				sql = 	"select 4 as sqlBase, a.modelID, b.modelName, count(*) as varients, (" &_
						"select SUM(quantity) from we_orderdetails ia with (nolock) join we_orders ib with (nolock) on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' join we_Items ic with (nolock) on ia.itemid = ic.itemID and ic.hideLive = 0 and ic.typeID = " & fprTypeID & " and ic.modelID = " & modelID &_
						") as qtySold, (" &_
						"select count(*) from we_items ia left join we_items ib on ia.partNumber = ib.partNumber and ib.master = 1 where ia.modelID = " & modelID & " and ia.typeID = " & fprTypeID & " and ia.hideLive = 0 and ib.inv_qty > 0" & modelDsVal &_
						") as inStock from we_items a left join we_models b on a.modelID = b.modelID where a.modelID = " & modelID & " and a.typeID = " & fprTypeID & " and a.hideLive = 0 " & replace(dsVal,"partNumber","a.partNumber") & " group by a.modelID, b.modelName, a.partNumber"
				sql = replace(sql,"c.vendor","vendor")
			else
				if fpType <> "" then
					sql = 	"select 5 as sqlBase, a.modelID, b.modelName, count(*) as varients, (" &_
							"select SUM(quantity) from we_orderdetails ia with (nolock) join we_orders ib with (nolock) on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' join we_Items ic with (nolock) on ia.itemid = ic.itemID and a.partNumber like '" & fpType & "%' and ic.hideLive = 0 and ic.modelID = " & modelID &_
							") as qtySold, (" &_
							"select count(*) from we_items ia left join we_items ib on ia.partNumber = ib.partNumber and ib.master = 1 where ia.partNumber like '" & fpType & "%' and ia.modelID = " & modelID & " and ia.hideLive = 0 and ib.inv_qty > 0" & modelDsVal &_
							") as inStock from we_items a left join we_models b on a.modelID = b.modelID where a.partNumber like '" & fpType & "%' and a.modelID = " & modelID & " and a.hideLive = 0 " & replace(dsVal,"partNumber","a.partNumber") & " group by a.modelID, b.modelName, a.partNumber"
					sql = replace(sql,"c.vendor","vendor")
				else
					sql = 	"select 6 as sqlBase, a.modelID, b.modelName, count(*) as varients, (" &_
							"select SUM(quantity) from we_orderdetails ia with (nolock) join we_orders ib with (nolock) on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' join we_Items ic with (nolock) on ia.itemid = ic.itemID and ic.hideLive = 0 and ic.modelID = " & modelID &_
							") as qtySold, (" &_
							"select count(*) from we_items ia left join we_items ib on ia.partNumber = ib.partNumber and ib.master = 1 where ia.modelID = " & modelID & " and ia.hideLive = 0 and ib.inv_qty > 0" & modelDsVal &_
							") as inStock from we_items a left join we_models b on a.modelID = b.modelID where a.modelID = " & modelID & " and a.hideLive = 0 " & replace(replace(dsVal,"c.vendor","a.vendor"),"partNumber","a.partNumber") & " group by a.modelID, b.modelName"
				end if
			end if
		end if
	elseif fullCatID > 0 then
		sql = 	"select 71 as sqlBase, a.modelID, b.modelName, count(*) as varients, (" &_
				"select sum(quantity) from we_items ia left join we_orderDetails ib on ib.itemID in (select itemID from we_items where partNumber = ia.partNumber) left join we_orders ic on ib.orderID = ic.orderID where ic.approved = 1 and ic.cancelled is null and ib.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' and modelID = a.modelID and hideLive = 0 and typeID = " & fprTypeID &_
				") as qtySold, (" &_
				"select count(*) from we_items ia left join we_items ib on ia.partNumber = ib.partNumber and ib.master = 1 where ia.modelID = a.modelID and ia.typeID = " & fprTypeID & " and ib.inv_qty > 0 and ia.hideLive = 0" &_
				") as inStock " &_
				"from we_items a " &_
					"left join we_models b on a.modelID = b.modelID " &_
					"join we_pnDetails c on a.partNumber = c.partNumber " &_
				"where a.typeID = " & fprTypeID & " and a.hideLive = 0 and b.modelName is not null " & replace(dsVal,"partNumber","a.partNumber") & " " &_
				"group by a.modelID, b.modelName, a.partNumber order by b.modelName"
		sql = replace(sql,"c.vendor","vendor")
		if invDays > 0 then sql = replace(sql,"group by","and (" & invDays & " * c.qtySoldPer30DayAvailable) > a.inv_qty group by")
	elseif fullBrandID > 0 then
		if fprTypeID > 0 then
			sql = 	"select 72 as sqlBase, a.modelID, b.modelName, count(*) as varients, (" &_
					"select sum(quantity) from we_items ia left join we_orderDetails ib on ib.itemID in (select itemID from we_items where partNumber = ia.partNumber) left join we_orders ic on ib.orderID = ic.orderID where ic.approved = 1 and ic.cancelled is null and ib.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' and modelID = a.modelID and hideLive = 0 and typeID = " & fprTypeID &_
					") as qtySold, (" &_
					"select count(*) from we_items ia left join we_items ib on ia.partNumber = ib.partNumber and ib.master = 1 where ia.modelID = a.modelID and ia.typeID = " & fprTypeID & " and ib.inv_qty > 0 and ia.hideLive = 0" &_
					") as inStock from we_items a left join we_models b on a.modelID = b.modelID where a.brandID = " & fullBrandID & " and a.typeID = " & fprTypeID & " and a.hideLive = 0 and b.modelName is not null " & replace(dsVal,"partNumber","a.partNumber") & " group by a.modelID, b.modelName, a.partNumber order by b.modelName"
			sql = replace(sql,"c.vendor","vendor")
		else
			sql = 	"select 8 as sqlBase1, a.modelID, b.modelName, count(*) as varients, (" &_
					"select sum(quantity) from we_items ia left join we_orderDetails ib on ia.partNumber = ib.partNumber left join we_orders ic on ib.orderID = ic.orderID where ic.approved = 1 and ic.cancelled is null and ib.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' and modelID = a.modelID and hideLive = 0" &_
					") as qtySold, (" &_
					"select count(*) from we_items ia left join we_items ib on ia.partNumber = ib.partNumber and ib.master = 1 where ia.modelID = a.modelID and ib.inv_qty > 0 and ia.hideLive = 0" &_
					") as inStock " &_
					"from we_items a " &_
					"left join we_models b on a.modelID = b.modelID " &_
					"where a.brandID = " & fullBrandID & " and a.hideLive = 0 and b.modelName is not null " & replace(replace(dsVal,"c.vendor","vendor"),"partNumber","a.partNumber") & " " &_
					"group by a.modelID, b.modelName, a.partNumber order by b.modelName"
		end if
		if sb <> "" then sql = replace(sql,"order by b.modelName","order by " & sb & " desc")
		if fpType <> "" then
			'main query
			sql = replace(sql,"where a.brandID = ","where a.partNumber like '" & fpType & "%' and a.brandID = ")
			'inStock
			sql = replace(sql,"where ia.modelID = a.modelID","where ia.modelID = a.modelID and ia.partNumber like '" & fpType & "%'")
			'qtySold
			sql = replace(sql,"where ic.approved = 1","where ia.partNumber like '" & fpType & "%' and ic.approved = 1")
		end if
	end if
	session("errorSQL") = sql
	if fullCatID = 0 then set rs = oConn.execute(sql)
	
	if brandID > 0 then
		sql = "select distinct left(partNumber,3) as partNumber from we_items where typeID = " & fprTypeID & " and hideLive = 0 and left(partNumber,3) <> '' order by left(partNumber,3)"
		session("errorSQL") = sql
		set typeRS = oConn.execute(sql)
	'################ Print Expanded Search Options #####################
%>
<form name="optionForm" style="margin:0px; padding:0px;">
<div style="display:block; width:600px; height:30px;">
    <div style="float:left; width:190px; text-align:right; padding-right:10px; font-weight:bold;">Select Model:</div>
    <div style="float:left; width:400px; text-align:left;">
        <select name="modelID" onchange="prepData();ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?vendorCode=<%=session("vendorCode")%>&fullBrandID=<%=brandID%>&modelID=' + this.value + '&fpType=' + document.optionForm.fpType.value + '&dsType=' + document.optionForm.dsType.value,'results')">
            <option value="">- Select Model -</option>
            <%
            do while not rs.EOF
            %>
            <option value="<%=rs("modelID")%>"><%=rs("modelName")%></option>
            <%
                rs.movenext
            loop
            %>
        </select>
    </div>
</div>
<div style="display:block; width:600px; height:30px;">
    <div style="float:left; width:190px; text-align:right; padding-right:10px; font-weight:bold; margin-top:5px;">Select Type:</div>
    <div style="float:left; width:400px; margin-top:5px; text-align:left;">
        <select name="fpType" onchange="prepData();ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?vendorCode=<%=session("vendorCode")%>&fullBrandID=<%=brandID%>&modelID=' + document.optionForm.modelID.value + '&fpType=' + this.value + '&dsType=' + document.optionForm.dsType.value,'results')">
            <option value="">- All Types -</option>
            <% do while not typeRS.EOF %>
            <option value="<%=typeRS("partNumber")%>"><%=typeRS("partNumber")%></option>
            <%
				typeRS.movenext
			loop
			%>
        </select>
    </div>
</div>
<div style="display:block; width:600px; height:30px;">
	<div style="float:left; width:190px; text-align:right; padding-right:10px; font-weight:bold; margin-top:5px;">Dropship:</div>
	<div style="float:left; width:400px; margin-top:5px; text-align:left;">
    	<select name="dsType" onchange="prepData();ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?vendorCode=<%=session("vendorCode")%>&fullBrandID=<%=brandID%>&modelID=' + document.optionForm.modelID.value + '&fpType=' + document.optionForm.fpType.value + '&dsType=' + this.value,'results')">
        	<option value="all">- All Locations -</option>
	        <option value="wh"<% if dsType = "wh" then %> selected="selected"<% end if %>>Warehouse Only</option>
    	    <option value="ds"<% if dsType = "ds" then %> selected="selected"<% end if %>>Dropship Only</option>
	    </select>
	</div>
</div>
<div style="display:block; width:600px; height:30px;">
	<div style="float:left; width:190px; text-align:right; padding-right:10px; font-weight:bold; margin-top:5px;">Day Range:</div>
	<div style="float:left; width:400px; margin-top:5px; text-align:left;">
    	<input type="text" name="dayRange" value="<%=session("fprDays")%>" size="2" />
        <input type="button" name="myAction" value="Update" onclick="prepData();ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?vendorCode=<%=session("vendorCode")%>&fullBrandID=<%=brandID%>&modelID=' + document.optionForm.modelID.value + '&fpType=' + document.optionForm.fpType.value + '&dsType=' + document.optionForm.dsType.value + '&dayRange=' + document.optionForm.dayRange.value,'results')" />
	</div>
</div>
</form>
<%
	elseif modelID > 0 or fullBrandID > 0 or fullVendorCode <> "" then
	'################ Print Model List w/ Sales/Inventory Data #####################
%>
<table align="center" border="1" bordercolor="#000000" cellpadding="3" cellspacing="0">
	<tr bgcolor="#333333" style="font-weight:bold; color:#FFF;">
    	<td style="width:300px; cursor:pointer;" onclick="ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullBrandID=<%=fullBrandID%>','results')" valign="bottom">Model</td>
        <td style="width:90px; cursor:pointer;" onclick="ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullBrandID=<%=fullBrandID%>&sb=varients','results')" valign="bottom" align="center">Varients</td>
        <td style="width:90px; cursor:pointer;" onclick="ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullBrandID=<%=fullBrandID%>&sb=qtySold','results')" nowrap="nowrap" valign="bottom" align="center">Sold<br /><span style="font-size:10px;">(prev <%=session("fprDays")%> days)</span></td>
        <td style="width:90px; cursor:pointer;" onclick="ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullBrandID=<%=fullBrandID%>&sb=inStock','results')" nowrap="nowrap" valign="bottom" align="center">Varients<br />In Stock</td>
    </tr>
    <%
	bgColor = "#ffffff"
	curModelName = ""
	do while not rs.EOF
		modelName = prepStr(rs("modelName"))
		varients = prepInt(rs("varients"))
		qtySold = prepInt(rs("qtySold"))
		inStock = prepInt(rs("inStock"))
		if modelName <> curModelName then
			curModelName = modelName
	%>
    <tr bgcolor="<%=bgColor%>" style="cursor:pointer;" onclick="selectModel('<%=modelName%>')">
    	<td align="left"><%=modelName%></td>
    	<td align="right"><%=formatNumber(varients,0)%></td>
        <td align="right"><%=formatNumber(qtySold,0)%></td>
        <td align="right"><%=formatNumber(inStock,0)%></td>
    </tr>
    <%
		end if
		rs.movenext
		if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
	loop
	%>
</table>
<%
	end if
	
	if modelID > 0 or fullVendorCode <> "" or fullCatID > 0 then
		if fullVendorCode = "" and fullCatID = 0 then
			chartDsVal = replace(dsVal,"partNumber","ia.partNumber")
			chartDsVal = replace(chartDsVal,"vendor","ia.vendor")
			
			if vendorCode <> "" then
				if fprTypeID > 0 then
					if fpType <> "" then
						sql = "select 1 as baseSQL2, sum(quantity) as sales, CONVERT(date, entrydate, 101) as saleDate from we_orderDetails ib left join we_orders ic on ib.orderID = ic.orderID join we_Items id on ib.itemID = id.itemID and id.partNumber like '%" & fpType & "%' where ic.approved = 1 and ic.cancelled is null and ib.itemID in (select ia.itemID from we_items ia join we_vendorNumbers ib on ia.partNumber = ib.we_partNumber and ib.vendor = '" & vendorCode & "' where ia.modelID = " & modelID & " and ia.hideLive = 0 and ia.typeID = " & fprTypeID & ") and ib.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' group by CONVERT(date, entrydate, 101) order by CONVERT(date, entrydate, 101)"
					else
						sql = "select 2 as baseSQL2, sum(quantity) as sales, CONVERT(date, entrydate, 101) as saleDate from we_orderDetails ib left join we_orders ic on ib.orderID = ic.orderID where ic.approved = 1 and ic.cancelled is null and ib.itemID in (select ia.itemID from we_items ia join we_vendorNumbers ib on ia.partNumber = ib.we_partNumber and ib.vendor = '" & vendorCode & "' where ia.modelID = " & modelID & " and ia.hideLive = 0 and ia.typeID = " & fprTypeID & ") and ib.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' group by CONVERT(date, entrydate, 101) order by CONVERT(date, entrydate, 101)"
					end if
				else
					sql = "select 3 as baseSQL2, sum(quantity) as sales, CONVERT(varchar(11), ib.entrydate, 101) as saleDate from we_orderDetails ib left join we_orders ic on ib.orderID = ic.orderID left join we_Items id on ib.partNumber = id.partNumber left join we_vendorNumbers ie on ib.partNumber = ie.we_partNumber where ic.approved = 1 and ic.cancelled is null and id.modelID = " & modelID & " and id.hideLive = 0 and ib.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' and (ie.vendor = '" & vendorCode & "' or id.vendor like '%" & vendorCode & "%') group by CONVERT(varchar(11), ib.entrydate, 101) order by CONVERT(varchar(11), ib.entrydate, 101)"
				end if
			else
				if fprTypeID > 0 then
					sql = "select 41 as baseSQL2, sum(quantity) as sales, CONVERT(date, entrydate, 101) as saleDate from we_orderDetails ib left join we_orders ic on ib.orderID = ic.orderID where ic.approved = 1 and ic.cancelled is null and ib.itemID in (select itemID from we_items where modelID = " & modelID & " and hideLive = 0 and typeID = " & fprTypeID & ") and ib.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' group by CONVERT(date, entrydate, 101) order by CONVERT(date, entrydate, 101)"
				else
					sql = "select 5 as baseSQL2, sum(quantity) as sales, CONVERT(date, entrydate, 101) as saleDate from we_orderDetails ib left join we_orders ic on ib.orderID = ic.orderID where ic.approved = 1 and ic.cancelled is null and ib.itemID in (select itemID from we_items where modelID = " & modelID & " and hideLive = 0) and ib.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' group by CONVERT(date, entrydate, 101) order by CONVERT(date, entrydate, 101)"
				end if
				if fpType <> "" then
					sql = replace(sql,"where ic.approved = ","where partNumber like '%" & fpType & "%' and ic.approved = ")
				end if
			end if
			session("errorSQL") = sql
			set dailyValRS = oConn.execute(sql)
		
		'################ Print Bar Graph #####################
%>
<table align="center" border="0" cellpadding="0" cellspacing="0" style="padding-top:20px;">
	<tr>
		<%
        curDate = dateadd("d",(session("fprDays")*-1),date)
		totalSales = 0
        for i = 0 to session("fprDays")
            if dailyValRS.EOF then
		%>
        <td valign="bottom" align="center" style="font-size:10px;">0</td>
        <%
			else
				if cdate(curDate) = cdate(dailyValRS("saleDate")) then
					totalSales = totalSales + dailyValRS("sales")
        %>
    
    	<td valign="bottom" align="center" style="font-size:10px;">
        	<%=dailyValRS("sales")%><br />
            <div style="height:<%=dailyValRS("sales") * 3%>px; width:20px; background-color:#00F; padding:0px; margin:0px;" title="<%=dailyValRS("saleDate")%> (<%=weekdayname(weekday(dailyValRS("saleDate")))%>)"><!-- --></div>
        </td>
		<%
        	        dailyValRS.movenext
            	else
        %>
        <td valign="bottom" align="center" style="font-size:10px;">0.</td>
        <%
	            end if
			end if
			curDate = curDate + 1
        next
        %>
    </tr>
    <tr>
    	<%
		curDate = dateadd("d",(session("fprDays")*-1),date)
		for i = 0 to session("fprDays")
		%>
        <td align="center"><div style="margin-top:5px; text-align:left;" class="vertTxt"><%=replace(curDate,"/"&year(curDate),"")%></div></td>
        <%
			curDate = cdate(curDate) + 1
		next
		%>
    </tr>
</table>
<%
		end if
		
		if fullCatID > 0 then
'			sql = 	"select 4 as baseSQL, a.itemID, a.partNumber, e.modelName, a.modelID, a.itemDesc, a.ghost, c.vendorCode, c.orderAmt, c.receivedAmt, c.entryDate, c.complete, c.process, d.hidden, " &_
'					"(select SUM(ia.quantity) from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' join we_Items ic on ia.itemid = ic.itemID where ic.PartNumber = a.PartNumber) as sales, " &_
'					"(select SUM(quantity) from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' where ia.itemid = a.itemID) as itemSales, " &_
'					"(select top 1 inv_qty from we_Items where partNumber = a.PartNumber and master = 1 order by inv_qty desc) as inv_qty, " &_
'					"(select sum(qty) from we_rma where partnumber = a.partnumber and rmaType = 8 and cancelDate is null and processDate is null) as boQty, " &_
'					"b.partNumber as vendorNumber " &_
'					"from we_Items a left join we_vendorNumbers b on a.PartNumber = b.we_partNumber left join we_vendorOrder c on a.PartNumber = c.partNumber left join we_ItemsExtendedData d on a.partNumber = d.partNumber left join we_models e on a.modelID = e.modelID where a.partNumber not like '%-MLD-%' and a.hideLive = 0 and a.typeID = " & fprTypeID & " order by a.partNumber, c.entryDate desc"
			
			sql = 	"select a.*, b.sales, b.sales as itemSales, c.boQty, d.qtySoldPer30DayAvailable " &_
					"from ( " &_
						"select 4 as baseSQL, a.itemID, a.PartNumber, a.modelID, a.itemDesc, a.ghost, a.inv_qty, b.modelName, c.vendorCode, c.vendorCode as vendor, c.orderAmt, c.receivedAmt, c.entryDate, c.complete, c.process, d.hidden, e.partNumber as vendorNumber " &_
						"from we_Items a " &_
							"left join we_Models b on a.modelID = b.modelID " &_
							"left join we_vendorOrder c on a.PartNumber = c.partNumber " &_
							"left join we_ItemsExtendedData d on a.partNumber = d.partNumber " &_
							"left join we_vendorNumbers e on a.PartNumber = e.we_partNumber " &_
						"where a.partNumber not like '%-MLD-%' and a.hideLive = 0 and a.typeID = " & fullCatID & " and a.master = 1 " &_
					") a " &_
						"left join ( " &_
							"select ii.partnumber, SUM(ib.quantity) as sales " &_
							"from we_orders ia " &_
								"join we_orderdetails ib on ia.orderid = ib.orderid " &_
								"join we_items ii on ib.itemid = ii.itemid " &_
							"where ia.approved = 1 and ia.cancelled is null and ia.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' " &_
							"group by ii.partnumber " &_
					") b on a.partnumber = b.partnumber " &_
						"left join ( " &_
						"select SUM(qty) as boQty, partnumber " &_
						"from we_rma " &_
						"where rmaType = 8 and cancelDate is null and processDate is null " &_
						"group by partnumber " &_
					") c on a.PartNumber = c.partnumber " &_
					"join we_pnDetails d on a.partNumber = d.partNumber " &_
					"order by a.partNumber, a.entryDate desc"
			if invDays > 0 then sql = replace(sql,"order by","where (" & invDays & " * d.qtySoldPer30DayAvailable) > a.inv_qty order by")
			if sb = "Sold" then
				if isnull(session("sbSold_bs4")) then
					session("sbSold_bs4") = " desc"
				elseif session("sbSold_bs4") = " desc" then
					session("sbSold_bs4") = null
				end if
				sql = replace(sql,"order by a.partNumber, a.entryDate desc","order by 17" & session("sbSold_bs4") & ", a.partNumber, a.entryDate desc")
				session("sbInv") = null
			elseif sb = "Inv" then
				if isnull(session("sbInv")) then
					session("sbInv") = ""
				elseif session("sbInv") = " desc" then
					session("sbInv") = ""
				else
					session("sbInv") = " desc"
				end if
				sql = replace(sql,"order by a.partNumber, a.entryDate desc","order by 7" & session("sbInv") & ", a.partNumber, a.entryDate desc")
				session("sbSold_bs4") = null
			else
				session("sbInv") = null
				session("sbSold_bs4") = null
			end if
		elseif fullVendorCode <> "" then
			if brandID > 0 then useBrandID = brandID
			if fullBrandID > 0 then useBrandID = fullBrandID
			if fprTypeID = 0 then
				if useBrandID > 0 then
					sql = 	"select 1 as baseSQL, a.itemID, a.partNumber, e.modelName, a.modelID, a.itemDesc, a.ghost, c.vendorCode, c.orderAmt, c.receivedAmt, c.entryDate, c.complete, c.process, d.hidden, f.qtySoldPer30DayAvailable, " &_
							"(select SUM(ia.quantity) from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' join we_Items ic on ia.itemid = ic.itemID where ic.PartNumber = a.PartNumber) as sales, " &_
							"(select SUM(quantity) from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' where ia.itemid = a.itemID) as itemSales, " &_
							"(select top 1 inv_qty from we_Items where partNumber = a.PartNumber and master = 1 order by inv_qty desc) as inv_qty, " &_
							"(select sum(qty) from we_rma where partnumber = a.partnumber and rmaType = 8 and cancelDate is null and processDate is null) as boQty, b.partNumber as vendorNumber " &_
							"from we_Items a " &_
							"left join we_vendorNumbers b on a.PartNumber = b.we_partNumber " &_
							"left join we_vendorOrder c on a.PartNumber = c.partNumber " &_
							"left join we_ItemsExtendedData d on a.partNumber = d.partNumber " &_
							"left join we_models e on a.modelID = e.modelID " &_
							"join we_pnDetails f on a.partnumber = f.partnumber " &_
							"where a.hideLive = 0 and a.brandID = " & useBrandID & " and (b.vendor = '" & fullVendorCode & "' or a.vendor = '" & fullVendorCode & "') " &_
							"order by e.modelName, a.partNumber, c.entryDate desc"
				else
					sql =	"select 2 as baseSQL, f.vendorCode, a.itemID, a.PartNumber, i.modelName, i.modelID, a.itemDesc, a.ghost, b.vendor, f.orderAmt, f.receivedAmt, f.entryDate, f.complete, f.process, g.hidden, e.totalQtySold as sales, -1 as itemSales, a.inv_qty, h.boQty, b.partNumber as vendorNumber, j.qtySoldPer30DayAvailable " &_
							"from we_Items a " &_
								"left join we_vendorNumbers b on a.PartNumber = b.we_partNumber " &_
								"left join (select itemID, SUM(quantity) as qtySold from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid where ia.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' and ib.approved = 1 and ib.cancelled is null group by itemID) d on a.itemID = d.itemID " &_
								"left join (select partNumber, SUM(quantity) as totalQtySold from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid where ia.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' and ib.approved = 1 and ib.cancelled is null group by partNumber) e on a.PartNumber = e.partNumber " &_
								"outer apply (select top 1 partNumber, orderAmt, receivedAmt, process, complete, entryDate, vendorCode from we_vendorOrder where partnumber = a.partnumber order by complete, process, entryDate desc) f " &_
								"left join we_ItemsExtendedData g on a.PartNumber = g.partNumber " &_
								"left join (select partnumber, sum(qty) as boQty from we_rma where rmaType = 8 and cancelDate is null and processDate is null group by partnumber) h on a.PartNumber = h.partnumber " &_
								"left join we_Models i on a.modelID = i.modelID " &_
								"join we_pnDetails j on a.partnumber = j.partnumber " &_
							"where (b.vendor = '" & fullVendorCode & "' or a.vendor like '%" & fullVendorCode & "%') and a.master = 1 and a.hideLive = 0 order by i.modelName, a.PartNumber, f.entryDate desc"
					sql = "exec sp_purchaseAppVendorPull '" & fullVendorCode & "','" & sb & "'," & blockID & "," & itemsPerPage & "," & invDays & ","
				end if
			else
				if useBrandID > 0 then
					sql = 	"select 3 as baseSQL, a.itemID, a.partNumber, e.modelName, a.modelID, a.itemDesc, a.ghost, c.vendorCode, c.orderAmt, c.receivedAmt, c.entryDate, c.complete, c.process, d.hidden, f.qtySoldPer30DayAvailable,  " &_
							"(select SUM(ia.quantity) from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' join we_Items ic on ia.itemid = ic.itemID where ic.PartNumber = a.PartNumber) as sales, " &_
							"(select SUM(quantity) from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' where ia.itemid = a.itemID) as itemSales, " &_
							"(select top 1 inv_qty from we_Items where partNumber = a.PartNumber and master = 1 order by inv_qty desc) as inv_qty, " &_
							"(select sum(qty) from we_rma where partnumber = a.partnumber and rmaType = 8 and cancelDate is null and processDate is null) as boQty, b.partNumber as vendorNumber " &_
							"from we_Items a " &_
								"join we_vendorNumbers b on a.PartNumber = b.we_partNumber and b.vendor = '" & fullVendorCode & "' " &_
								"left join we_vendorOrder c on a.PartNumber = c.partNumber " &_
								"left join we_ItemsExtendedData d on a.partNumber = d.partNumber " &_
								"left join we_models e on a.modelID = e.modelID " &_
								"join we_pnDetails f on a.partnumber = f.partnumber " &_
							"where a.hideLive = 0 and a.typeID = " & fprTypeID & " and a.brandID = " & useBrandID & " " &_
							"order by e.modelName, a.partNumber, c.entryDate desc"
				else
					sql = 	"select 42 as baseSQL, a.itemID, a.partNumber, e.modelName, a.modelID, a.itemDesc, a.ghost, c.vendorCode, c.orderAmt, c.receivedAmt, c.entryDate, c.complete, c.process, d.hidden, b.vendor, f.qtySoldPer30DayAvailable, " &_
							"(select SUM(ia.quantity) from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' join we_Items ic on ia.itemid = ic.itemID where ic.PartNumber = a.PartNumber) as sales, " &_
							"(select SUM(quantity) from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' where ia.itemid = a.itemID) as itemSales, " &_
							"(select top 1 inv_qty from we_Items where partNumber = a.PartNumber and master = 1 order by inv_qty desc) as inv_qty, " &_
							"(select sum(qty) from we_rma where partnumber = a.partnumber and rmaType = 8 and cancelDate is null and processDate is null) as boQty, b.partNumber as vendorNumber " &_
							"from we_Items a " &_
								"left join we_vendorNumbers b on a.PartNumber = b.we_partNumber " &_
								"left join we_vendorOrder c on a.PartNumber = c.partNumber " &_
								"left join we_ItemsExtendedData d on a.partNumber = d.partNumber " &_
								"left join we_models e on a.modelID = e.modelID " &_
								"join we_pnDetails f on a.partnumber = f.partnumber " &_
							"where (b.vendor = '" & fullVendorCode & "' or a.vendor like '%" & fullVendorCode & "%') and a.hideLive = 0 and a.typeID = " & fprTypeID & " order by e.modelName, a.partNumber, c.entryDate desc"
				end if
			end if
			
			if sb = "Sold" then
				if listDirection = "" then
					if isnull(session("sbSold")) then
						session("sbSold") = " desc"
					elseif session("sbSold") = " desc" then
						session("sbSold") = ""
					else
						session("sbSold") = " desc"
					end if
				end if
				if instr(sql,"sp_purchaseAppVendorPull") < 1 then
					if instr(sql,"order by e.modelName, a.partNumber") > 0 then
						sql = replace(sql,"order by e.modelName, a.partNumber","order by 14" & session("sbSold") & ", e.modelName, a.partNumber")
					else
						sql = replace(sql,"order by i.modelName, a.PartNumber","order by 16" & session("sbSold") & ", i.modelName, a.PartNumber")
					end if
				else
					if session("sbSold") = " desc" then
						sql = sql & "1"
						listDirection = 1
					else
						sql = sql & "0"
						listDirection = 0
					end if
				end if
				session("sbInv") = null
			elseif sb = "Inv" then
				if listDirection = "" then
					if isnull(session("sbInv")) then
						session("sbInv") = ""
					elseif session("sbInv") = " desc" then
						session("sbInv") = ""
					else
						session("sbInv") = " desc"
					end if
				end if
				if instr(sql,"sp_purchaseAppVendorPull") < 1 then
					if instr(sql,"order by e.modelName, a.partNumber") > 0 then
						sql = replace(sql,"order by e.modelName, a.partNumber","order by 17" & session("sbInv") & ", e.modelName, a.partNumber")
					else
						sql = replace(sql,"order by i.modelName, a.PartNumber","order by 18" & session("sbInv") & ", i.modelName, a.partNumber")
					end if
				else
					if session("sbInv") = " desc" then
						sql = sql & "1"
						listDirection = 1
					else
						sql = sql & "0"
						listDirection = 2
					end if
				end if
				session("sbSold") = null
			else
				session("sbInv") = null
				session("sbSold") = null
				if instr(sql,"sp_purchaseAppVendorPull") > 0 then sql = sql & "0"
			end if
		else
			itemDsVal = replace(dsVal,"partNumber","a.partNumber")
			itemDsVal = replace(itemDsVal,"c.vendor","a.vendor")
			if vendorCode <> "" then
				if fprTypeID > 0 then
					sql = 	"select 5 as baseSQL, c.itemID as itemID, a.PartNumber, g.modelName, a.modelID, a.itemDesc, e.vendorCode, e.orderAmt, e.receivedAmt, e.entryDate, e.complete, e.process, f.hidden,  c.ghost, c.inv_qty, h.qtySoldPer30DayAvailable, " &_
							"(select SUM(ia.quantity) from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' join we_Items ic on ia.itemid = ic.itemID and ic.PartNumber = a.PartNumber) as sales, " &_
							"(select SUM(quantity) from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' where ia.itemid = a.itemID) as itemSales, " &_
							"(select sum(qty) from we_rma where partnumber = a.partnumber and rmaType = 8 and cancelDate is null and processDate is null) as boQty, b.partNumber as vendorNumber " &_
							"from we_Items a " &_
								"join we_vendorNumbers b on a.PartNumber = b.we_partNumber " &_
								"join we_Items c on a.PartNumber = c.PartNumber and c.master = 1 " &_
								"join we_vendorNumbers d on a.PartNumber = d.we_partNumber and d.vendor = '" & vendorCode & "' " &_
								"left join we_vendorOrder e on a.PartNumber = e.partNumber and e.id = (select MAX(id) from we_vendorOrder where partNumber = a.PartNumber) " &_
								"left join we_ItemsExtendedData f on a.partNumber = f.partNumber " &_
								"left join we_models g on a.modelID = g.modelID " &_
								"left join we_pnDetails h on a.partNumber = h.partNumber " &_
							"where a.modelID = " & modelID & " and a.hideLive = 0 and a.typeID = " & fprTypeID & itemDsVal & " " &_
							"order by g.modelName, a.partNumber, e.entryDate desc"
				else
					sql = 	"select 6 as baseSQL, c.itemID as itemID, a.PartNumber, g.modelName, a.modelID, a.itemDesc, e.vendorCode, e.orderAmt, e.receivedAmt, e.entryDate, e.complete, e.process, f.hidden, c.ghost, c.inv_qty, h.qtySoldPer30DayAvailable, " &_
							"(select SUM(ia.quantity) from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' join we_Items ic on ia.itemid = ic.itemID and ic.PartNumber = a.PartNumber) as sales, " &_
							"(select SUM(quantity) from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' where ia.itemid = a.itemID) as itemSales, " &_
							"(select sum(qty) from we_rma where partnumber = a.partnumber and rmaType = 8 and cancelDate is null and processDate is null) as boQty, b.partNumber as vendorNumber " &_
							"from we_Items a " &_
								"join we_Items c on a.PartNumber = c.PartNumber and c.master = 1 " &_
								"left join we_vendorNumbers d on a.PartNumber = d.we_partNumber " &_
								"left join we_vendorOrder e on a.PartNumber = e.partNumber and e.id = (select MAX(id) from we_vendorOrder where partNumber = a.PartNumber) " &_
								"left join we_ItemsExtendedData f on a.partNumber = f.partNumber " &_
								"left join we_models g on a.modelID = g.modelID " &_
								"left join we_pnDetails h on a.partNumber = h.partNumber " &_
							"where a.modelID = " & modelID & " and a.hideLive = 0 " & itemDsVal & " and (d.vendor = '" & vendorCode & "' or a.vendor like '%" & vendorCode & "%')" &_
							"order by g.modelName, a.partNumber, e.entryDate desc"
				end if
			else
				if fprTypeID > 0 then
					sql = 	"select 7 as baseSQL, b.itemID, a.partNumber, g.modelName, a.modelID, a.itemDesc, e.vendorCode, e.orderAmt, e.receivedAmt, e.entryDate, e.complete, e.process, f.hidden, b.ghost, b.inv_qty, h.qtySoldPer30DayAvailable, " &_
							"(select SUM(quantity) from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid join we_Items ic on ia.itemid = ic.itemID where ia.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' and ic.PartNumber = a.partNumber and ib.approved = 1 and ib.cancelled is null) as sales, " &_
							"(select SUM(quantity) from we_orderdetails ia join we_orders ib on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' where ia.itemid = a.itemID) as itemSales, " &_
							"(select sum(qty) from we_rma where partnumber = a.partnumber and rmaType = 8 and cancelDate is null and processDate is null) as boQty, b.partNumber as vendorNumber " &_
							"from we_items a " &_
								"left join we_items b on a.partNumber = b.partNumber and b.master = 1 " &_
								"left join we_vendorOrder e on a.PartNumber = e.partNumber and e.id = (select MAX(id) from we_vendorOrder where partNumber = a.PartNumber) " &_
								"left join we_ItemsExtendedData f on a.partNumber = f.partNumber " &_
								"left join we_models g on a.modelID = g.modelID " &_
								"left join we_pnDetails h on a.partNumber = h.partNumber " &_
							"where a.modelID = " & modelID & " and a.hideLive = 0 and a.typeID = " & fprTypeID & itemDsVal & " " &_
							"group by b.itemID, a.partNumber, g.modelName, a.modelID, a.itemDesc, e.vendorCode, e.orderAmt, e.receivedAmt, e.entryDate, e.complete, e.process, f.hidden, b.ghost, b.inv_qty, h.qtySoldPer30DayAvailable, a.itemID, b.partNumber order by g.modelName, a.partNumber, e.entryDate desc"
				else
					sql = 	"select 8 as baseSQL2, b.itemID, a.partNumber, g.modelName, a.modelID, a.itemDesc, e.vendorCode, e.orderAmt, e.receivedAmt, e.entryDate, e.complete, e.process, f.hidden, b.ghost, b.inv_qty, h.qtySoldPer30DayAvailable, " &_
							"(" &_
								"select SUM(quantity) " &_
								"from we_orderdetails ia " &_
									"join we_orders ib on ia.orderid = ib.orderid " &_
									"join we_Items ic on ia.itemid = ic.itemID " &_
								"where ia.entryDate > '" & dateadd("d",(session("fprDays")*-1),date) & "' and ic.PartNumber = a.partNumber and ib.approved = 1 and ib.cancelled is null) as sales, " &_
							"(" &_
								"select SUM(quantity) " &_
								"from we_orderdetails ia " &_
									"join we_orders ib on ia.orderid = ib.orderid and ib.approved = 1 and ib.cancelled is null and ib.orderdatetime > '" & dateadd("d",(session("fprDays")*-1),date) & "' " &_
								"where ia.itemid = a.itemID) as itemSales, " &_
							"(" &_
								"select sum(qty) " &_
								"from we_rma " &_
								"where partnumber = a.partnumber and rmaType = 8 and cancelDate is null and processDate is null) as boQty, b.partNumber as vendorNumber, b.vendor " &_
							"from we_items a " &_
								"left join we_items b on a.partNumber = b.partNumber and b.master = 1 " &_
								"left join we_vendorOrder e on a.PartNumber = e.partNumber and e.id = (" &_
									"select MAX(id) " &_
									"from we_vendorOrder " &_
									"where partNumber = a.PartNumber) " &_
								"left join we_ItemsExtendedData f on a.partNumber = f.partNumber " &_
								"left join we_models g on a.modelID = g.modelID " &_
								"left join we_pnDetails h on a.partNumber = h.partNumber " &_
							"where a.modelID = " & modelID & " and a.hideLive = 0 " & itemDsVal & " " &_
							"group by b.itemID, a.partNumber, g.modelName, a.modelID, a.itemDesc, e.vendorCode, e.orderAmt, e.receivedAmt, e.entryDate, e.complete, e.process, f.hidden, b.ghost, b.inv_qty, h.qtySoldPer30DayAvailable, a.itemID, b.partNumber, b.vendor " &_
							"order by g.modelName, a.partNumber, e.entryDate desc"
				end if
			end if
			if fpType <> "" then
				sql = replace(sql,"where a.modelID = ","where a.partNumber like '%" & fpType & "%' and a.modelID = ")
			end if
			if sb = "Sold" then
				if isnull(session("sbSold")) then
					session("sbSold") = " desc"
				elseif session("sbSold") = " desc" then
					session("sbSold") = ""
				else
					session("sbSold") = " desc"
				end if
				if instr(sql,"8 as baseSQL2") > 0 then
					sql = replace(sql,"order by g.modelName, a.partNumber","order by 16" & session("sbSold") & ", g.modelName, a.PartNumber")
				else
					sql = replace(sql,"order by g.modelName, a.partNumber","order by 13" & session("sbSold") & ", g.modelName, a.PartNumber")
				end if
				session("sbInv") = null
			elseif sb = "Inv" then
				if isnull(session("sbInv")) then
					session("sbInv") = ""
				elseif session("sbInv") = " desc" then
					session("sbInv") = ""
				else
					session("sbInv") = " desc"
				end if
				if instr(sql,"order by g.modelName, a.partNumber") > 0 then
					sql = replace(sql,"order by g.modelName, a.partNumber","order by b.inv_qty" & session("sbInv") & ", g.modelName, a.PartNumber")
				else
					sql = replace(sql,"order by i.modelName, a.partNumber","order by a.inv_qty" & session("sbInv") & ", i.modelName, a.PartNumber")
				end if
				session("sbSold") = null
			else
				session("sbInv") = null
				session("sbSold") = null
			end if
		end if
		if adminID = 54 and instr(request.ServerVariables("SERVER_NAME"),"staging.") > 0 then response.Write(sql & "<br>")
		session("csvSQL") = sql
		session("errorSQL") = sql
		set itemRS = oConn.execute(sql)
		'################ Print Item List #####################
%>
<div style="width:600px; height:20px; display:block;"></div>
<table align="center" border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="600">
	<tr><td colspan="4" align="right"><a href="/admin/faceplatePurchaseReport_csv.asp" target="_blank">Create CSV</a></td></tr>
    <tr bgcolor="#333333" style="font-weight:bold; color:#FFF;">
    	<td align="center" title="bottomForm">#</td>
        <td align="left" style="cursor:pointer;" onclick="document.getElementById('results').innerHTML='<div style=margin-top:30px;>LOADING DATA<br>PLEASE WAIT</div>';ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullVendorCode=<%=fullVendorCode%>&fullCatID=<%=fullCatID%>&modelID=<%=modelID%>&fpType=<%=fpType%>&dsType=<%=dsType%>&invDays=<%=invDays%>','results')">Item</td>
        <td align="center" style="cursor:pointer;" onclick="document.getElementById('results').innerHTML='<div style=margin-top:30px;>LOADING DATA<br>PLEASE WAIT</div>';ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullVendorCode=<%=fullVendorCode%>&fullCatID=<%=fullCatID%>&modelID=<%=modelID%>&fpType=<%=fpType%>&sb=Sold&dsType=<%=dsType%>&invDays=<%=invDays%>','results')">Sold</td>
        <td align="center" style="cursor:pointer;" onclick="document.getElementById('results').innerHTML='<div style=margin-top:30px;>LOADING DATA<br>PLEASE WAIT</div>';ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullVendorCode=<%=fullVendorCode%>&fullCatID=<%=fullCatID%>&modelID=<%=modelID%>&fpType=<%=fpType%>&sb=Inv&dsType=<%=dsType%>&invDays=<%=invDays%>','results')">Inv</td>
    </tr>
	<%
	rowNum = 0
	soldCnt = 0
	invCnt = 0
	bgColor = "#ffffff"
	curPartNumber = ""
	partList = ""
	session("partList") = ""
	
	do while not itemRS.EOF
		irsItemID = itemRS("itemID")
		irsItemDesc = itemRS("itemDesc")
		irsVendorCode = itemRS("vendorCode")
		irsOrderAmt = itemRS("orderAmt")
		irsReceivedAmt = itemRS("receivedAmt")
		irsEntryDate = itemRS("entryDate")
		irsComplete = itemRS("complete")
		irsProcess = itemRS("process")
		irsHidden = itemRS("hidden")
		irsGhost = itemRS("ghost")
		if irsHidden then
			if session("showHidden") = 1 then
				isHidden = 1
				irsHidden = 0
			else
				isHidden = 0
				irsHidden = 1
			end if
		else
			irsHidden = 0
			isHidden = 0
		end if
		irsPartNumber = itemRS("partNumber")
		irsSales = prepInt(itemRS("sales"))
		irsItemSales = prepInt(itemRS("itemSales"))
		irsSellThrough = prepInt(itemRS("qtySoldPer30DayAvailable"))
		irsBackOrders = prepInt(itemRS("boQty"))
		irsInv_qty = prepInt(itemRS("inv_qty"))
		irsVendorNumber = itemRS("vendorNumber")
		soldCnt = soldCnt + irsSales
		invCnt = invCnt + irsInv_qty
		
		session("partList") = session("partList") & "'" & irsPartNumber & "',"
		
		if instr(partList,irsPartNumber) > 0 then
			irsHidden = 1
		else
			partList = partList & irsPartNumber & "##"
		end if
		
		if len(irsItemDesc) > 90 then useItemDesc = left(irsItemDesc,90) & "..." else useItemDesc = irsItemDesc
		
		if curPartNumber <> irsPartNumber and irsHidden = 0 then
			rowNum = rowNum + 1
			curPartNumber = irsPartNumber
			if not isnull(irsEntryDate) and instr(irsEntryDate," ") > 0 then irsEntryDate = left(irsEntryDate,instr(irsEntryDate," ")-1)
			if isnull(irsProcess) then irsProcess = true
			if not irsProcess then
				curBgColor = bgColor
				bgColor = "#009900"
			elseif irsProcess and not irsComplete then
				curBgColor = bgColor
				bgColor = "#0087d3"
			elseif isnull(irsVendorNumber) then
				curBgColor = bgColor
				bgColor = "#ff9999"
			elseif prepInt(irsBackOrders) > 0 then
				curBgColor = bgColor
				bgColor = "#999900"
			end if
	%>
	<tr bgcolor="<%=bgColor%>">
    	<td colspan="4">
        	<div class="fl numCol">
            	<%=rowNum%>
            	<% if isHidden = 1 then %><br /><span class="ft8px">Hidden</span><% end if %>
            	<% if irsGhost then %><br /><span class="ft8px">Ghost</span><% end if %>
            </div>
            <div class="fl itemCol">
            	<div class="tb itemTableRow" onclick="expandData(<%=irsItemID%>,'<%=irsPartNumber%>')">
                    <div class="tb pb5"><%=useItemDesc%></div>
                    <div class="tb"><span class="ft14px"><%=irsPartNumber%></span> (<%=irsItemID%>) VN:<%=irsVendorNumber%></div>
                    <% if not isnull(irsOrderAmt) then %>
                        <% if not irsProcess then %>
                    <div class="tb onOrder">On Order</div>
                        <% else %>
                    <div class="tb lastPurch">Last Vendor Purchase From: <%=irsVendorCode%>, Qty: <%=irsOrderAmt%>, On: <%=irsEntryDate%> (<% if irsComplete then %>Received <%=irsReceivedAmt%><% else %>Awaiting<% end if %>)</div>
                        <% end if %>
                    <% end if %>
                </div>
            </div>
            <div class="fl soldCol">
            	<div class="fr topNum" title="Sales by partnumber"><%=irsSales%></div>
            	<div title="Sales by itemID" class="fr exSalesData">(<%=irsItemSales%>)</div>
                <div title="30 Day Run Rate" id="runRate_<%=irsItemID%>" class="fr sellThrough"><%=irsSellThrough * 30%></div>
            </div>
            <div class="fl invCol">
	            <div class="fr topNum" title="Current Inventory" id="curInv_<%=irsItemID%>"><%=irsInv_qty%></div>
    	        <div title="Current Backorders" class="fr exSalesData">(<%=irsBackOrders%>)</div>
            </div>
            <div class="tb loadingBox" style="display:none;" id="item_<%=irsItemID%>">Loading Data...</div>
            <input type="hidden" id="partnumber_o<%=rowNum%>" value="<%=irsPartNumber%>" />
            <input type="hidden" id="itemID_o<%=rowNum%>" value="<%=irsItemID%>" />
        </td>
    </tr>
    <%
			if bgColor <> "#ffffff" and bgColor <> "#ebebeb" then bgColor = curBgColor
			if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
		end if
		itemRS.movenext
	loop
	
	if instr(sql,"sp_purchaseAppVendorPull") > 0 then
	%>
    <tr bgcolor="#000000">
    	<td colspan="2" align="left">
        	<% if blockID-500 > 0 then %>
        	<a href="javascript:pageItemList('<%=fullVendorCode%>',<%=fprTypeID%>,<%=blockID-itemsPerPage%>,'<%=sb%>',<%=listDirection%>);" style="color:#FFF;">Previous Page</a>
            <% end if %>
        </td>
        <td colspan="2" align="right">
        	<a href="javascript:pageItemList('<%=fullVendorCode%>',<%=fprTypeID%>,<%=blockID+itemsPerPage%>,'<%=sb%>',<%=listDirection%>);" style="color:#FFF;">Next Page</a>
        </td>
    </tr>
    <%
	end if
	%>
    <tr bgcolor="#000000">
    	<td colspan="4">
        	<div style="float:right; color:#FFF; font-weight:bold;">Sold Total: <%=formatNumber(soldCnt,0)%></div>
            <div style="float:right; color:#FFF; font-weight:bold; padding-right:20px;">Inv Total: <%=formatNumber(invCnt,0)%></div>
            <input type="hidden" id="fullResultCnt" value="<%=rowNum%>" />
        </td>
    </tr>
</table>
<%
	end if
	
	oConn.Close
	set oConn = nothing
%>