<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	call getDBConn(oConn)

	dim site : site = prepStr(request.QueryString("site"))
	dim addItemID : addItemID = prepStr(request.QueryString("addItemID"))	
	dim addSortNum : addSortNum = prepStr(request.QueryString("addSortNum"))
	dim deleteID : deleteID = prepStr(request.QueryString("deleteID"))
	dim updateID : updateID = prepStr(request.QueryString("updateID"))
	dim updateSortNum : updateSortNum = prepStr(request.QueryString("updateSortNum"))	
	
	if site = "WE" then
		siteid = 0
	elseif site = "CO" then
		siteid = 2
	end if
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	if isnumeric(addItemID) and isnumeric(addSortNum) and addItemID <> "" and addSortNum <> "" then
		sql = 	"insert into we_recommends(itemid, sortOrder, site_id) " & vbcrlf & _
				"select	itemid, " & addSortNum & ", " & siteid & vbcrlf & _
				"from	we_items" & vbcrlf & _
				"where	itemid = " & addItemID
'		response.write "<pre>" & sql & "</pre>"
		oConn.execute(sql)
	elseif isnumeric(deleteID) and deleteID <> "" then
		sql = "delete from we_recommends where id = " & deleteID
'		response.write "<pre>" & sql & "</pre>"		
		oConn.execute(sql)	
	elseif isnumeric(updateID) and isnumeric(updateSortNum) and updateID <> "" and updateSortNum <> "" then
		sql = "select id from we_recommends where site_id = " & siteid & " and sortOrder = " & updateSortNum
'		response.write "<pre>" & sql & "</pre>"		
		set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
		if not rs.eof then
			sql = "update we_recommends set sortOrder = sortOrder + 1 where site_id = " & siteid & " and sortOrder >= " & updateSortNum
'			response.write "<pre>" & sql & "</pre>"			
			oConn.execute(sql)
		end if
		sql = "update we_recommends set sortOrder = " & updateSortNum & " where id = " & updateID
'		response.write "<pre>" & sql & "</pre>"		
		oConn.execute(sql)		
	end if
	
	if site = "WE" then
%>
                <div style="padding-top:10px; font-weight:bold; font-size:18px; border-bottom:1px solid #000; height:25px; width:700;">
                    <div style="float:left;">Top 5 featured products</div>
                </div>
                <div style="height:500px; overflow:scroll; border:1px solid #000; width:700; font-size:11px;" align="left">
                    <table border="0" cellpadding="5" cellspacing="0">
                <%
                sql = 	"	select	a.id, a.itemid, b.partnumber, a.sortOrder, b.itemdesc, b.itempic" & vbcrlf & _
                        "	from	we_recommends a join we_items b" & vbcrlf & _
                        "		on	a.itemid = b.itemid" & vbcrlf & _
                        "	where	a.site_id = 0" & vbcrlf & _
                        "		and	b.hidelive = 0 and b.inv_qty <> 0 and b.price_our > 0" & vbcrlf & _							
                        "	order by a.sortOrder" & vbcrlf
                session("errorSQL") = sql
                set rs = Server.CreateObject("ADODB.Recordset")
                rs.open sql, oConn, 0, 1								
                nRow = 0
                do until rs.EOF
                    nRow = nRow + 1
                %>
                        <tr class="grid_row_<%=(nRow mod 2)%>" onMouseOver="this.className='mouseover'" onMouseOut="this.className='grid_row_<%=(nRow mod 2)%>'" >

                            <td width="50" align="left"><img src="/productpics/icon/<%=rs("itempic")%>" border="0" width="45" height="45" /></td>
                            <td width="*" align="left" style="font-size:11px;"><div style="border-bottom:1px solid #ccc;">ItemID: <%=rs("itemid")%>, PartNumber: <%=rs("partnumber")%></div><div><%=rs("itemdesc")%></div></td>
                            <td width="40" align="center"><input size="3" type="text" id="txtSort_<%=rs("id")%>" name="txtSort" value="<%=rs("sortOrder")%>" /></td>
                            <td width="100" align="center">
                            	<input type="button" name="btnUpdate" value="Update Sort" onClick="return updateSort('WE','<%=rs("id")%>');" /><br />
                            	<input type="button" name="btnDelete" value="Delete" onClick="return deleteItem('WE','<%=rs("id")%>');" />
							</td>
                        </tr>
                        <input type="hidden" name="hidRecommendID" value="<%=rs("id")%>" />
                <%
                    rs.movenext
                loop
                %>
                    </table>
                </div>
                <div width="700" align="left" style="padding-top:5px;">
		            ItemID: <input size="3" type="text" name="addItemID_WE" value="" /> &nbsp;  &nbsp;  &nbsp; Sort: <input size="2" type="text" name="addSortNum_WE" value="" /> &nbsp; <input type="submit" name="btnSubmit" value="Add Item" onclick="return addItem('WE');" />
                </div>   
    <%
	elseif site = "CO" then
	%>
                <div style="padding-top:10px; font-weight:bold; font-size:18px; border-bottom:1px solid #000; height:25px; width:700;">
                    <div style="float:left;">Top 10 new arrivals</div>
                </div>
                <div style="height:500px; overflow:scroll; border:1px solid #000; width:700; font-size:11px;" align="left">
                    <table border="0" cellpadding="5" cellspacing="0">
                <%
                sql = 	"	select	a.id, a.itemid, b.partnumber, a.sortOrder, b.itemdesc_co, b.itempic_co" & vbcrlf & _
                        "	from	we_recommends a join we_items b" & vbcrlf & _
                        "		on	a.itemid = b.itemid" & vbcrlf & _
                        "	where	a.site_id = 2" & vbcrlf & _
                        "		and	b.hidelive = 0 and b.inv_qty <> 0 and b.price_co > 0" & vbcrlf & _
                        "	order by a.sortOrder" & vbcrlf
                session("errorSQL") = sql
                set rs = Server.CreateObject("ADODB.Recordset")
                rs.open sql, oConn, 0, 1
                nRow = 0
                do until rs.EOF
                    nRow = nRow + 1					
                %>
                        <tr class="grid_row_<%=(nRow mod 2)%>" onMouseOver="this.className='mouseover'" onMouseOut="this.className='grid_row_<%=(nRow mod 2)%>'" >
                            <td width="50" align="left"><img src="/productpics/co_temp/productpics/icon/<%=rs("itempic_co")%>" border="0" width="45" height="45" /></td>
                            <td width="*" align="left" style="font-size:11px;"><div style="border-bottom:1px solid #ccc;">ItemID: <%=rs("itemid")%>, PartNumber: <%=rs("partnumber")%></div><div><%=rs("itemdesc_co")%></div></td>
                            <td width="40" align="center"><input size="3" type="text" id="txtSort_<%=rs("id")%>" name="txtSort" value="<%=rs("sortOrder")%>" /></td>
                            <td width="100" align="center">
                            	<input type="button" name="btnUpdate" value="Update Sort" onClick="return updateSort('CO','<%=rs("id")%>');" /><br />
                            	<input type="button" name="btnDelete" value="Delete" onClick="return deleteItem('CO','<%=rs("id")%>');" />
							</td>
                        </tr>
                        <input type="hidden" name="hidRecommendID" value="<%=rs("id")%>" />
                <%
                    rs.movenext
                loop
                %>
                    </table>
                </div>
                <div width="700" align="left" style="padding-top:5px;">
		            ItemID: <input size="3" type="text" name="addItemID_CO" value="" /> &nbsp;  &nbsp;  &nbsp; Sort: <input size="2" type="text" name="addSortNum_CO" value="" /> &nbsp; <input type="submit" name="btnSubmit" value="Add Item" onclick="return addItem('CO');" />
                </div>    
    <%
	end if

	call CloseConn(oConn)
%>