<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/Framework/Utility/Static.asp"-->
<%
	call getDBConn(oConn)
	
	id = prepInt(request.querystring("id"))
	edittype = prepStr(request.querystring("edittype"))
	aisle = prepStr(request.querystring("aisle"))
	shelf = prepStr(request.querystring("shelf"))
	row = prepStr(request.querystring("row"))
	label = prepStr(request.querystring("label"))

	if edittype = "e" then
		sql	=	"update	warehouseItemLocation" & vbcrlf & _
				"set	aisle = '" & ucase(aisle) & "'" & vbcrlf & _
				"	,	shelf = '" & ucase(shelf) & "'" & vbcrlf & _
				"	,	row = '" & ucase(row) & "'" & vbcrlf & _
				"	,	label = '" & ucase(label) & "'" & vbcrlf & _
				"where	id = " & id
		session("errorSQL") = sql
		oConn.execute(sql)
	elseif edittype = "d" then
		sql	=	"delete from warehouseItemLocation" & vbcrlf & _
				"where	id = " & id
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	response.write "UPDATED"
	
	call CloseConn(oConn)
%>