<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/Framework/Utility/Static.asp"-->
<%	
call getDBConn(oConn)
set fsThumb = CreateObject("Scripting.FileSystemObject")

newColorID 			= prepInt(request.QueryString("colorid"))
masterPartnumber 	= prepStr(request.QueryString("masterPartnumber"))
partnumber 			= prepStr(request.QueryString("partnumber"))
uType 				= prepStr(request.QueryString("uType"))
chk_hl_we 			= prepInt(request.QueryString("chk_hl_we"))
chk_hl_co 			= prepInt(request.QueryString("chk_hl_co"))
chk_hl_ca 			= prepInt(request.QueryString("chk_hl_ca"))
chk_hl_ps 			= prepInt(request.QueryString("chk_hl_ps"))
chk_hl_tm 			= prepInt(request.QueryString("chk_hl_tm"))

sql = "select id, color, colorCodes, borderColor from xproductcolors order by 1"
session("errorSQL") = sql
arrColors = getDbRows(sql)

select case lcase(uType)
	case "current"
		if masterPartnumber <> "" then
			sql	=	"select	a.master, a.slave, a.hidelive hidelive_we, a.hidelive_co, a.hidelive_ca, a.hidelive_ps, a.hidelive_tm" & vbcrlf & _
					"	,	c.itemid, c.itemdesc, c.hidelive, c.inv_qty, c.itempic, r.id, r.color, r.colorCodes, r.borderColor, isnull(v.colorSlaves, '') colorSlaves, isnull(v.cnt, 0) cnt" & vbcrlf & _
					"	,	c.brandid, c.modelid, c.typeid, c.subtypeid" & vbcrlf & _
					"from	xproductcolormatrix a join we_items c" & vbcrlf & _
					"	on	a.slave = c.partnumber left outer join v_colorMatrix v" & vbcrlf & _
					"	on	a.master = v.master left outer join xproductcolors r" & vbcrlf & _
					"	on	c.colorid = r.id" & vbcrlf & _
					"where	a.master = '" & masterPartnumber & "'" & vbcrlf & _
					"	and	c.master = 1" & vbcrlf & _
					"order by 1, 2"
		elseif partnumber <> "" then
			sql	=	"select	a.master, a.slave, a.hidelive hidelive_we, a.hidelive_co, a.hidelive_ca, a.hidelive_ps, a.hidelive_tm" & vbcrlf & _
					"	,	c.itemid, c.itemdesc, c.hidelive, c.inv_qty, c.itempic, r.id, r.color, r.colorCodes, r.borderColor, isnull(v.colorSlaves, '') colorSlaves, isnull(v.cnt, 0) cnt" & vbcrlf & _
					"	,	c.brandid, c.modelid, c.typeid, c.subtypeid" & vbcrlf & _
					"from	xproductcolormatrix a join xproductcolormatrix b" & vbcrlf & _
					"	on	a.master = b.master join we_items c" & vbcrlf & _
					"	on	a.slave = c.partnumber left outer join v_colorMatrix v" & vbcrlf & _
					"	on	a.master = v.master left outer join xproductcolors r" & vbcrlf & _
					"	on	c.colorid = r.id" & vbcrlf & _
					"where	b.slave = '" & partnumber & "'" & vbcrlf & _
					"	and	c.master = 1" & vbcrlf & _
					"order by 1, 2"
		end if
'		response.write "<pre>" & sql & "</pre>"
		session("errorSQL") = sql
		set rsColor = oConn.execute(sql)
		lap = 0
		if not rsColor.eof then
			do until rsColor.eof
				if not rsColor("hidelive") then liveLink = "/p-" & formatSEO(rsColor("itemid")) & "-" & formatSEO(rsColor("itemdesc")) & ".asp"				
				
				rsColor.movenext
			loop
			rsColor.movefirst
			%>
            <div style="width:1000px; font-size:12px; padding:5px 0px 5px 0px;" align="right">
                <a href="<%=liveLink%>" target="_blank">How do they look like on live?</a>
            </div>
            <%
			do until rsColor.eof
				lap = lap + 1
				masterPartnumber = rsColor("master")
				slavePartnumber = rsColor("slave")
				hl_we = rsColor("hidelive_we")
				hl_co = rsColor("hidelive_co")
				hl_ca = rsColor("hidelive_ca")
				hl_ps = rsColor("hidelive_ps")
				hl_tm = rsColor("hidelive_tm")
				curItemID = rsColor("itemid")
				itemdesc = rsColor("itemdesc")
				itempic = rsColor("itempic")
				item_hidelive = rsColor("hidelive")
				inv_qty = rsColor("inv_qty")
				colorid = rsColor("id")
				colorCodes = rsColor("colorCodes")
				borderColor = rsColor("borderColor")
				colorSlaves = rsColor("colorSlaves")
				numAvailColors = rsColor("cnt")
				onSale = false
	
				itemImgPath = Server.MapPath("/productpics/thumb") & "\" & itemPic
				useImgPath = "/productpics/thumb/" & itemPic
				if not fsThumb.FileExists(itemImgPath) then useImgPath = "/productpics/thumb/imagena.jpg"
				
				if lap = 1 and colorSlaves <> "" then
				%>
					<div id="id_colorPicker" style="float:left; margin:5px 0px 0px 4px; width:180px; height:290px; padding:5px; border:1px solid #F3671C; font-size:12px;">
						<div id="id_productImage_<%=curItemID%>" style="width:150px; padding-left:30px;" align="left"><img src="<%=useImgPath%>" border="0" width="100" height="100" /></div>
						<div align="center" style="width:160px; padding:10px;"><%=generateColorPicker(arrColors, curItemID, colorSlaves, colorid, numAvailColors, onSale)%></div>
						<div align="left" style="width:180px; font-size:12px;">Color Hidelive: <br />
						WE: <input type="checkbox" name="chk_hl_we" <%if hl_we then %>checked<% end if%> onclick="updateHidelive();" />&nbsp;&nbsp;CO: <input type="checkbox" name="chk_hl_co" <%if hl_co then %>checked<% end if%> onclick="updateHidelive();" />&nbsp;&nbsp;CA: <input type="checkbox" name="chk_hl_ca" <%if hl_ca then %>checked<% end if%> onclick="updateHidelive();" /><br />
						PS: <input type="checkbox" name="chk_hl_ps" <%if hl_ps then %>checked<% end if%> onclick="updateHidelive();" />&nbsp;&nbsp;TM: <input type="checkbox" name="chk_hl_tm" <%if hl_tm then %>checked<% end if%> onclick="updateHidelive();" />
                        <input type="hidden" id="id_hidMasterPartnumber" name="hidMasterPartnumber" value="<%=masterPartnumber%>" />
						</div>
					</div>
				<%
				end if
				%>
					<div id="id_color_<%=colorid%>" style="float:left; margin:5px 0px 0px 4px; width:180px; height:290px; padding:5px; border:1px solid #ccc;">
						<div style="width:150px; padding-left:30px;" align="left"><img src="<%=useImgPath%>" border="0" width="100" height="100" /></div>
						<div style="width:180px; padding-top:10px;"><a target="_blank" href="/admin/db_update_items.asp?ItemID=<%=curItemID%>"><%=itemdesc%></a></div>
						<div style="width:180px; padding-top:5px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:12px;">
								<tr>
									<td align="left">Color Assigned:&nbsp;</td>
									<td align="left"><div style="width:20px; height:15px; font-size:1px; background-color:<%=rsColor("colorCodes")%>; border:1px solid <%=rsColor("borderColor")%>;">&nbsp;</div></td>
									<td align="left"><%=rsColor("color")%></td>
								</tr>
							</table>
						</div>
						<div style="width:180px; padding-top:5px; font-size:12px;">Partnumber:&nbsp;<%=slavePartnumber%></div>
						<div style="width:180px; padding-top:5px; font-size:12px;">Master Qty:&nbsp;<%=formatnumber(inv_qty,0)%></div>
						<div style="width:180px; padding-top:5px; font-size:12px;">hidelive:&nbsp;<input type="checkbox" name="hidelive" <%if item_hidelive then %>checked<% end if%> disabled /></div>
						<div style="width:180px; padding-top:5px; font-size:12px;">
                        	<input type="button" name="btnDeleteThis" value="Delete This" onClick="DeleteThis('<%=masterPartnumber%>','<%=slavePartnumber%>');" />
						</div>
					</div>
				<%
				rsColor.movenext
			loop
			flipSuggestionList = "NO"
			if ((lap - 3) mod 5) = 0 then 
				flipSuggestionList = "YES"
			end if
			%>
            <div id="id_new" style="float:left; margin:5px 0px 0px 4px; width:180px; height:290px; padding:5px; border:1px solid #333;">
            	<div style="width:180px; font-weight:bold;"><a href="#" onclick="document.getElementById('id_new_types').style.display=''">Add new product here</a></div>
            	<div id="id_new_types" style="width:180px; display:none;">
                <%
				sql = "select typeid, typename from we_types order by typeid"
				session("errorSQL") = sql
				arrTypes = getDbRows(sql)
				if not isnull(arrTypes) then
					%>
                    <select name="cbNew" style="width:170px;" onChange="if (this.value) searchNewItem(this.value,'<%=flipSuggestionList%>');">
                        <option value="">Select category</option>
                    <%
					for i=0 to ubound(arrTypes, 2)
						%>
                        <option value="<%=arrTypes(0,i)%>"><%=arrTypes(1,i)%></option>
                        <%
					next
					%>
                    </select>
                    <%
				end if
				%>
                </div>
            	<div id="id_new_search" style="width:180px;">&nbsp;</div>
            </div>
            <%	
		else
		%>
        <div id="id_new" style="float:left; margin:5px 0px 0px 4px; width:180px; height:290px; padding:5px; border:1px solid #333;">
            <div style="width:180px; font-weight:bold;"><a href="#" onclick="document.getElementById('id_new_types').style.display=''">Add new product here</a></div>
            <div id="id_new_types" style="width:180px; display:none;">
            <%
            sql = "select typeid, typename from we_types order by typeid"
            session("errorSQL") = sql
            arrTypes = getDbRows(sql)
            if not isnull(arrTypes) then
                %>
                <select name="cbNew" style="width:170px;" onChange="if (this.value) searchNewItem(this.value);">
                    <option value="">Select category</option>
                <%
                for i=0 to ubound(arrTypes, 2)
                    %>
                    <option value="<%=arrTypes(0,i)%>"><%=arrTypes(1,i)%></option>
                    <%
                next
                %>
                </select>
                <%
            end if
            %>
            </div>
            <div id="id_new_search" style="width:180px;">&nbsp;</div>
        </div>            
        <%	
		end if
	case "newitem"
		sql = 	"select	partnumber, hidelive, inv_qty, itemid, itemdesc, itempic, isnull(colorid, 0) colorid" & vbcrlf & _
				"from	we_items" & vbcrlf & _
				"where	partnumber = '" & partnumber & "'" & vbcrlf & _
				"	and	master = 1"
		set newRS = oConn.execute(sql)
		if not newRS.eof then
			item_hidelive = newRS("hidelive")
			inv_qty = newRS("inv_qty")
			curItemID = newRS("itemid")
			itemdesc = newRS("itemdesc")
			itempic = newRS("itempic")
			colorid = newRS("colorid")

			itemImgPath = Server.MapPath("/productpics/thumb") & "\" & itemPic
			useImgPath = "/productpics/thumb/" & itemPic
			if not fsThumb.FileExists(itemImgPath) then useImgPath = "/productpics/thumb/imagena.jpg"		
			%>
            <input type="hidden" name="hidSlavePartnumber" value="<%=partnumber%>" />
            <div style="width:150px; padding-left:30px;" align="left"><img src="<%=useImgPath%>" border="0" width="100" height="100" /></div>
            <div style="width:180px; padding-top:10px;"><a target="_blank" href="/admin/db_update_items.asp?ItemID=<%=curItemID%>"><%=itemdesc%></a></div>
            <div style="width:180px; padding-top:5px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:12px;">
                    <tr>
                        <td align="left">Color:&nbsp;</td>
                        <td align="left">
                        <select name="cbColor">
                        	<option value="">Select color</option>
                        <%
						for i=0 to ubound(arrColors, 2)
						%>
                        	<option value="<%=arrColors(0,i)%>" <%if cstr(colorid) = cstr(arrColors(0,i)) then %>selected<% end if%>><%=arrColors(1,i)%></option>
                        <%
						next
						%>
                        </select>
                        </td>
                    </tr>
                </table>
            </div>
			<div style="width:180px; padding-top:5px; font-size:12px;">Partnumber:&nbsp;<%=partnumber%></div>
            <div style="width:180px; padding-top:5px; font-size:12px;">Master Qty:&nbsp;<%=formatnumber(inv_qty,0)%></div>
            <div style="width:180px; padding-top:5px; font-size:12px;">hidelive:&nbsp;<input type="checkbox" name="hidelive" <%if item_hidelive then %>checked<% end if%> disabled /></div>
            <div style="width:180px; padding-top:5px; font-size:12px;">
            	<input type="button" name="btnAddNow" value="ADD NOW" onClick="addNewItem();" />&nbsp;
                <input type="button" name="btnCancel" value="Cancel" onClick="cancelNewItem();" />
			</div>
            <%
		end if
	case "addnewitem"
		sql = 	"select	top 1 b.itemid" & vbcrlf & _
				"from	xproductcolormatrix a join we_items b" & vbcrlf & _
				"	on	a.slave = b.partnumber" & vbcrlf & _
				"where	a.master = '" & masterPartnumber & "'" & vbcrlf & _
				"	and	b.master = 1" & vbcrlf & _
				"	and	b.colorid = '" & newColorID & "'"
		session("errorSQL") = sql
		set rsCheck = oConn.execute(sql)
		if not rsCheck.eof then
			response.write "color exists"
			response.end
		end if
		
		sql = 	"select	slave" & vbcrlf & _
				"from	xproductcolormatrix" & vbcrlf & _
				"where	master = '" & masterPartnumber & "'" & vbcrlf & _
				"	and	slave = '" & partnumber & "'"
		session("errorSQL") = sql
		set rsCheck = oConn.execute(sql)
		if not rsCheck.eof then
			response.write "partnumber exists"
			response.end
		end if
		
		if masterPartnumber <> "" then
			sql	=	"insert into xproductcolormatrix(master, slave, hidelive)" & vbcrlf & _
					"select	top 1 '" & masterPartnumber & "', '" & partnumber & "', hidelive " & vbcrlf & _
					"from	xproductcolormatrix where master = '" & masterPartnumber & "'"
			session("errorSQL") = sql
			oConn.execute(sql)		
		else
			sql = 	"select	slave" & vbcrlf & _
					"from	xproductcolormatrix" & vbcrlf & _
					"where	master = '" & partnumber & "' or slave = '" & partnumber & "'"
			session("errorSQL") = sql
			set rsCheck = oConn.execute(sql)
			if not rsCheck.eof then
				response.write "partnumber exists"
				response.end
			end if
		
			sql	=	"insert into xproductcolormatrix(master, slave)" & vbcrlf & _
					"values ('" & partnumber & "', '" & partnumber & "')"
			session("errorSQL") = sql
			oConn.execute(sql)		
		end if
		
		sql	=	"update	we_items" & vbcrlf & _
				"set	colorid = '" & newColorID & "'" & vbcrlf & _
				"where	partnumber = '" & partnumber & "'"
		session("errorSQL") = sql
		oConn.execute(sql)
				
		response.write "added"
	case "getblank"
		%>
        <div style="width:180px; font-weight:bold;"><a href="#" onclick="document.getElementById('id_new_types').style.display=''">Add new product here</a></div>
        <div id="id_new_types" style="width:180px; display:none;">
        <%
        sql = "select typeid, typename from we_types order by typeid"
        session("errorSQL") = sql
        arrTypes = getDbRows(sql)
        if not isnull(arrTypes) then
            %>
            <select name="cbNew" style="width:170px;" onChange="if (this.value) searchNewItem(this.value,'');">
                <option value="">Select category</option>
            <%
            for i=0 to ubound(arrTypes, 2)
                %>
                <option value="<%=arrTypes(0,i)%>"><%=arrTypes(1,i)%></option>
                <%
            next
            %>
            </select>
            <%
        end if
        %>
        </div>
        <div id="id_new_search" style="width:180px; display:none;">
            <input type="text" id="id_new_txt_search" name="txtNewSearch" value="Search By PartNumber" onclick="this.value='';" style="color:#666; width:175px;" />
        </div>
		<%
	case "delete"
		sql	=	"delete" & vbcrlf & _
				"from	xproductcolormatrix" & vbcrlf & _
				"where 	slave = '" & partnumber & "'"
		session("errorSQL") = sql
		oConn.execute(sql)
		response.write "deleted"
	case "updatehidelive"
		sql	=	"update	xproductcolormatrix" & vbcrlf & _
				"set	hidelive = '" & chk_hl_we & "'" & vbcrlf & _
				"	,	hidelive_co = '" & chk_hl_co & "'" & vbcrlf & _
				"	,	hidelive_ca = '" & chk_hl_ca & "'" & vbcrlf & _
				"	,	hidelive_ps = '" & chk_hl_ps & "'" & vbcrlf & _
				"	,	hidelive_tm = '" & chk_hl_tm & "'" & vbcrlf & _
				"where	master = '" & masterPartnumber & "'"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql	=	"select	brandid, modelid, typeid, subtypeid" & vbcrlf & _
				"from	we_items" & vbcrlf & _
				"where	partnumber = '" & masterPartnumber & "'"
		set rs = oConn.execute(sql)
		if not rs.eof then
			set oFolder = fsThumb.GetFolder(server.MapPath("/compressedPages"))
			set filecoll = oFolder.Files
			for each oFile in filecoll
				if instr(oFile, "category_" & rs("subtypeid")) > 0 then
					oFile.Delete
				end if
			next
		end if		
		response.write "updated"
end select

call CloseConn(oConn)
%>