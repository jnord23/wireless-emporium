<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/Framework/Utility/Static.asp"-->
<%
call getDBConn(oConn)

strSDate = prepStr(request.querystring("txtSDate"))
strEDate = prepStr(request.querystring("txtEDate"))
siteid = prepStr(request.querystring("cbSite"))
brandid = prepInt(request.querystring("cbBrand"))
modelid = prepInt(request.querystring("cbModel"))
typeid = prepInt(request.querystring("cbType"))
strGroupBy = prepStr(request.querystring("cbGroupBy"))
partnumber = prepStr(request.querystring("txtPartnumber"))
curSortColumnName = request.querystring("curSortColumnName")
curSort = prepStr(request.querystring("curSort"))

headerSort = curSortColumnName & " " & curSort

if strSDate = "" or not isdate(strSDate) then strSDate = Date
if strEDate = "" or not isdate(strEDate) then strEDate = Date

Dim cfgArrGroupBy : cfgArrGroupBy = Array("Date","Store","Brand","Model","Category","PartNumber","ItemID","Details")
if "" = strGroupBy then strGroupBy = "Date,Store,,,,,," end if
arrGroupBy = split(strGroupBy, ",")

dim strSqlSelectMain, strSqlWhere, strSqlFrom, strSqlGroupBy, strSqlOrderBy, strHeadings
strSqlSelectMain	=	""
strSqlWhere			=	""
strSqlFrom			=	""
strSqlGroupBy		=	""
strSqlOrderBy		=	""
strHeadings			=	""

For i = 0 To Cint(UBound(arrGroupBy))
	If "" <> trim(arrGroupBy(i)) Then
		Select Case ucase(trim(arrGroupBy(i)))
			Case "DATE"
				strSqlSelectMain	=	strSqlSelectMain 	& 	", convert(varchar(10), a.orderdatetime, 20) logDate"
				strSqlGroupBy		=	strSqlGroupBy		&	"convert(varchar(10), a.orderdatetime, 20), "
				strSqlOrderBy		=	strSqlOrderBy		&	"convert(varchar(10), a.orderdatetime, 20), "
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:92px; font-size:13px; font-weight:bold;"">Order Date</div>"

			Case "STORE"
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.store"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.store, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.store, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:42px; font-size:13px; font-weight:bold;"">SITE</div>"

			Case "BRAND"
				strSqlFrom			=	strSqlFrom			&	"left outer join we_brands e on c.brandid = e.brandid" & vbcrlf
				strSqlSelectMain	=	strSqlSelectMain 	& 	", isnull(e.brandName, 'Universal')"
				strSqlGroupBy		=	strSqlGroupBy		&	"isnull(e.brandName, 'Universal'), "
				strSqlOrderBy		=	strSqlOrderBy		&	"isnull(e.brandName, 'Universal'), "
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:120px; font-size:13px; font-weight:bold;"">Brand</div>"

			Case "MODEL"
				strSqlFrom			=	strSqlFrom			&	"left outer join we_models f on c.modelid = f.modelid" & vbcrlf
				strSqlSelectMain	=	strSqlSelectMain 	& 	", isnull(f.modelName, 'Universal')"
				strSqlGroupBy		=	strSqlGroupBy		&	"isnull(f.modelName, 'Universal'), "
				strSqlOrderBy		=	strSqlOrderBy		&	"isnull(f.modelName, 'Universal'), "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:252px; font-size:13px; font-weight:bold;"">Model</div>"

			Case "CATEGORY"
				strSqlFrom			=	strSqlFrom			&	"join we_types d on c.typeid = d.typeid" & vbcrlf
				strSqlSelectMain	=	strSqlSelectMain 	& 	", d.typename"
				strSqlGroupBy		=	strSqlGroupBy		&	"d.typename, "
				strSqlOrderBy		=	strSqlOrderBy		&	"d.typename, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:152px; font-size:13px; font-weight:bold;"">Category</div>"

			Case "PARTNUMBER"
				strSqlSelectMain	=	strSqlSelectMain 	& 	", c.partnumber"
				strSqlGroupBy		=	strSqlGroupBy		&	"c.partnumber, "
				strSqlOrderBy		=	strSqlOrderBy		&	"c.partnumber, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:122px; font-size:13px; font-weight:bold;"">Part Number</div>"

			Case "ITEMID"
				strSqlSelectMain	=	strSqlSelectMain 	& 	", c.itemid"
				strSqlGroupBy		=	strSqlGroupBy		&	"c.itemid, "
				strSqlOrderBy		=	strSqlOrderBy		&	"c.itemid, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:72px; font-size:13px; font-weight:bold;"">Item ID</div>"

			Case "DETAILS"
				bDetails = true			
		End Select
	End If
Next

strSqlWhere = ""
if siteid <> "" 		then strSqlWhere = strSqlWhere & "	and	a.store = " & siteid & vbcrlf
if brandid <> 0			then strSqlWhere = strSqlWhere & "	and	c.brandid = " & brandid & vbcrlf
if modelid <> 0			then strSqlWhere = strSqlWhere & "	and	c.modelid = " & modelid & vbcrlf
if typeid <> 0			then strSqlWhere = strSqlWhere & "	and	c.typeid = " & typeid & vbcrlf
if partnumber <> ""		then strSqlWhere = strSqlWhere & "	and	c.partnumber = '" & partnumber & "'" & vbcrlf

if bDetails then
	dim arrSubTotal(4,4)
	strSqlOrderBy = "order by " & headerSort
	sql	=	"select	a.store, a.orderid, convert(varchar(10), a.orderdatetime, 20) orderdatetime, c.partnumber, c.itemid, c.itemdesc, isnull(b.quantity, 0) quantity" & vbcrlf & _
			"	,	isnull(c.cogs, 0) cogs" & vbcrlf & _
			"	,	isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
			"					when a.store = 1 then c.price_ca" & vbcrlf & _
			"					when a.store = 2 then c.price_co" & vbcrlf & _
			"					when a.store = 3 then c.price_ps" & vbcrlf & _
			"					when a.store = 10 then" & vbcrlf & _
			"						case when c.price_er > 0 then c.price_er" & vbcrlf & _
			"							else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
			"						end " & vbcrlf & _
			"					else c.price_our" & vbcrlf & _
			"				end, 0) price_our" & vbcrlf & _
			"	,	isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
			"					when a.store = 1 then c.price_ca" & vbcrlf & _
			"					when a.store = 2 then c.price_co" & vbcrlf & _
			"					when a.store = 3 then c.price_ps" & vbcrlf & _
			"					when a.store = 10 then" & vbcrlf & _
			"						case when c.price_er > 0 then c.price_er" & vbcrlf & _
			"							else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
			"						end " & vbcrlf & _
			"					else c.price_our" & vbcrlf & _
			"				end, 0) * isnull(b.quantity, 0) linePriceTotal" & vbcrlf & _
			"	,	(isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
			"					when a.store = 1 then c.price_ca" & vbcrlf & _
			"					when a.store = 2 then c.price_co" & vbcrlf & _
			"					when a.store = 3 then c.price_ps" & vbcrlf & _
			"					when a.store = 10 then" & vbcrlf & _
			"						case when c.price_er > 0 then c.price_er" & vbcrlf & _
			"							else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
			"						end " & vbcrlf & _
			"					else c.price_our" & vbcrlf & _
			"				end, 0) * isnull(b.quantity, 0)) - (isnull(c.cogs, 0) * isnull(b.quantity, 0)) revenue" & vbcrlf & _
			"from	we_orders a join we_orderdetails b" & vbcrlf & _
			"	on	a.orderid = b.orderid join we_items c" & vbcrlf & _
			"	on	b.itemid = c.itemid" & vbcrlf & _
			"where	a.orderdatetime >= '" & strSDate & "'" & vbcrlf & _
			"	and	a.orderdatetime < '" & dateadd("d", 1, strEDate) & "'" & vbcrlf & _
			"	and	a.approved = 1 and (a.cancelled = 0 or a.cancelled is null)" & vbcrlf & _
			"	and	b.returned = 0" & vbcrlf & _
			strSqlWhere & vbcrlf & _
			strSqlOrderBy
'			response.write "<pre>" & sql & "</pre>"
'			response.end
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	if rs.eof then
		response.write "No data to display"
	else
		lap = 0
		priceTotal = cdbl(0)
		cogsTotal = cdbl(0)
		qtyTotal = clng(0)
		revenueTotal = cdbl(0)
		do until rs.eof
			lap = lap + 1			
			rowBackgroundColor = "#fff"
			if (lap mod 2) = 0 then rowBackgroundColor = "#eaf5fa"
			if (nRow mod 300) = 0 then response.Flush()
			
			select case rs("store")
				case 0 : site = "WE"
				case 1 : site = "CA"
				case 2 : site = "CO"
				case 3 : site = "PS"
				case 10 : site = "TM"
			end select

			qty = clng(rs("quantity"))
			price_our = cdbl(rs("price_our"))
			linePriceTotal = cdbl(rs("price_our")) * qty
			lineCogsTotal = cdbl(rs("cogs")) * qty
			revenue = cdbl(rs("revenue"))
			
			priceTotal = priceTotal + linePriceTotal
			cogsTotal = cogsTotal + lineCogsTotal
			qtyTotal = qtyTotal + qty
			revenueTotal = revenueTotal + revenue
			
			itemdesc = rs("itemdesc")
			if len(itemdesc) > 55 then itemdesc = left(itemdesc, 52) & "..."
			%>
		<div class="table-row" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='1px solid <%=rowBackgroundColor%>';this.style.backgroundColor='<%=rowBackgroundColor%>'">
			<div class="table-cell" style="width:40px;"><%=site%>&nbsp;</div>
			<div class="table-cell" style="width:90px;"><%=rs("orderdatetime")%>&nbsp;</div>
			<div class="table-cell" style="width:90px;"><%=rs("orderid")%>&nbsp;</div>
			<div class="table-cell" style="width:120px;"><%=rs("partnumber")%>&nbsp;</div>
			<div class="table-cell" style="width:60px;"><%=rs("itemid")%>&nbsp;</div>
			<div class="table-cell" style="width:320px;" title="<%=rs("itemdesc")%>"><%=itemdesc%>&nbsp;</div>
			<div class="table-cell" style="width:40px;"><%=formatnumber(qty, 0)%>&nbsp;</div>
			<div class="table-cell" style="width:70px;"><%=formatcurrency(price_our)%>&nbsp;</div>
			<div class="table-cell" style="width:80px;" align="right"><%=formatcurrency(linePriceTotal)%>&nbsp;&nbsp;</div>
			<div class="table-cell" style="width:80px;" align="right"><%=formatcurrency(lineCogsTotal)%>&nbsp;&nbsp;</div>
			<div class="table-cell" style="width:80px; color:#060;" align="right"><%=formatcurrency(revenue)%>&nbsp;&nbsp;</div>
		</div>
			<%
			if (lap mod 500) = 0 then response.flush
			rs.movenext
		loop
		%>
		<div class="table-row" style="background-color:#fff; border-top:1px solid #ccc;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='';this.style.borderTop='1px solid #ccc';this.style.backgroundColor='#fff'">
			<div class="table-cell" style="width:720px; padding-right:12px; font-weight:bold;" align="right">Total&nbsp;</div>
			<div class="table-cell" style="width:40px; font-weight:bold;"><%=formatnumber(qty, 0)%>&nbsp;</div>
			<div class="table-cell" style="width:70px; font-weight:bold;">&nbsp;</div>
			<div class="table-cell" style="width:80px; font-weight:bold;" align="right"><%=formatcurrency(priceTotal)%>&nbsp;&nbsp;</div>
			<div class="table-cell" style="width:80px; font-weight:bold;" align="right"><%=formatcurrency(cogsTotal)%>&nbsp;&nbsp;</div>
			<div class="table-cell" style="width:80px; font-weight:bold; color:#060;" align="right"><%=formatcurrency(revenueTotal)%>&nbsp;&nbsp;</div>
		</div>
		<%
	end if
else
	if "" <> strSqlGroupBy then
		strSqlGroupBy	=	"group by " & left(trim(strSqlGroupBy), len(trim(strSqlGroupBy))-1)
	end if
	strSqlOrderBy	=	"order by " & headerSort
	
	sql	=	"select	sum(isnull(b.quantity, 0)) quantity" & vbcrlf & _
			"	,	sum(isnull(b.quantity, 0) * isnull(c.cogs, 0)) cogs" & vbcrlf & _
			"	,	sum(isnull(b.quantity, 0) * isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
			"												when a.store = 1 then c.price_ca" & vbcrlf & _
			"												when a.store = 2 then c.price_co" & vbcrlf & _
			"												when a.store = 3 then c.price_ps" & vbcrlf & _
			"												when a.store = 10 then" & vbcrlf & _
			"													case when c.price_er > 0 then c.price_er" & vbcrlf & _
			"														else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
			"													end " & vbcrlf & _
			"												else c.price_our" & vbcrlf & _
			"											end, 0)) price_our" & vbcrlf & _
			"	,	sum(isnull(b.quantity, 0) * isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
			"												when a.store = 1 then c.price_ca" & vbcrlf & _
			"												when a.store = 2 then c.price_co" & vbcrlf & _
			"												when a.store = 3 then c.price_ps" & vbcrlf & _
			"												when a.store = 10 then" & vbcrlf & _
			"													case when c.price_er > 0 then c.price_er" & vbcrlf & _
			"														else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
			"													end " & vbcrlf & _
			"												else c.price_our" & vbcrlf & _
			"											end, 0)) - sum(isnull(b.quantity, 0) * isnull(c.cogs, 0)) revenue" & vbcrlf & _			
			strSqlSelectMain & vbcrlf & _
			"from	we_orders a join we_orderdetails b" & vbcrlf & _
			"	on	a.orderid = b.orderid join we_items c" & vbcrlf & _
			"	on	b.itemid = c.itemid " & vbcrlf & _
			strSqlFrom & _
			"where	a.orderdatetime >= '" & strSDate & "'" & vbcrlf & _
			"	and	a.orderdatetime < '" & dateadd("d", 1, strEDate) & "'" & vbcrlf & _
			"	and	a.approved = 1 and (a.cancelled = 0 or a.cancelled is null)" & vbcrlf & _
			"	and	b.returned = 0" & vbcrlf & _
			strSqlWhere & vbcrlf & _
			strSqlGroupBy & vbcrlf & _					
			strSqlOrderBy
'			response.write "<pre>" & sql & "</pre>"
	session("errorSQL") = sql
	arrResult = getDbRows(sql)

	qtyTotal = clng(0)
	priceTotal = cdbl(0)
	cogsTotal = cdbl(0)
	revenueTotal = cdbl(0)
	if not isnull(arrResult) then
		for nRow=0 to ubound(arrResult,2)
			rowBackgroundColor = "#eaf5fa"
			if (nRow mod 2) = 0 then rowBackgroundColor = "#fff"	
			if (nRow mod 300) = 0 then response.Flush()			
			
			qty = clng(arrResult(0,nRow))
			linePriceTotal = cdbl(arrResult(2,nRow))
			lineCogsTotal = cdbl(arrResult(1,nRow))
			revenue = cdbl(arrResult(3,nRow))
			
			qtyTotal = qtyTotal + qty								'quantity
			priceTotal = priceTotal + linePriceTotal				'priceTotal
			cogsTotal = cogsTotal + lineCogsTotal					'cogsTotal
			revenueTotal = revenueTotal + revenue
		%>
		<div class="table-row" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='1px solid <%=rowBackgroundColor%>';this.style.backgroundColor='<%=rowBackgroundColor%>'">
		<%
			nSkipColumn = 4
			totalWidth = 0
			for i=0 to ubound(arrGroupBy)
				if "" <> trim(arrGroupBy(i)) then
					select case ucase(trim(arrGroupBy(i)))
						case "DATE"
							totalWidth = totalWidth + 90
						%>
							<div class="table-cell" style="width:90px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
						<%
						Case "STORE"
							totalWidth = totalWidth + 40
							select case cint(arrResult(nSkipColumn,nRow))
								case 0 : site = "WE"
								case 1 : site = "CA"
								case 2 : site = "CO"
								case 3 : site = "PS"
								case 10 : site = "TM"
							end select
							%>
							<div class="table-cell" style="width:40px;"><%=site%>&nbsp;</div>
							<%
						Case "BRAND"
							totalWidth = totalWidth + 120
						%>
							<div class="table-cell" style="width:120px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
						<%
						Case "MODEL"
							totalWidth = totalWidth + 250
						%>
							<div class="table-cell" style="width:250px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
						<%
						Case "CATEGORY"
							totalWidth = totalWidth + 150
						%>
							<div class="table-cell" style="width:150px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
						<%
						Case "PARTNUMBER"
							totalWidth = totalWidth + 120
						%>
							<div class="table-cell" style="width:120px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
						<%
						Case "ITEMID"
							totalWidth = totalWidth + 70
						%>
							<div class="table-cell" style="width:70px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
						<%								
					end select
					nSkipColumn = nSkipColumn + 1										
				end if
			next
			%>
			<div class="table-cell" style="width:60px;"><%=formatnumber(qty, 0)%>&nbsp;</div>
			<div class="table-cell" style="width:110px;" align="right"><%=formatcurrency(linePriceTotal)%>&nbsp;&nbsp;&nbsp;</div>
			<div class="table-cell" style="width:110px;" align="right"><%=formatcurrency(lineCogsTotal)%>&nbsp;&nbsp;&nbsp;</div>
			<div class="table-cell" style="width:110px; color:#060;" align="right"><%=formatcurrency(revenue)%>&nbsp;&nbsp;&nbsp;</div>
		</div>
			<%
		next
		%>
		<div class="table-row" style="background-color:#fff; border-top:1px solid #ccc;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='';this.style.borderTop='1px solid #ccc';this.style.backgroundColor='#fff'">
			<%if totalWidth > 0 then%>
			<div class="table-cell" style="width:<%=totalWidth%>px; font-weight:bold;">Total</div>
			<%end if%>
			<div class="table-cell" style="width:60px; font-weight:bold;"><%=formatnumber(qtyTotal, 0)%>&nbsp;</div>
			<div class="table-cell" style="width:110px; font-weight:bold;" align="right"><%=formatcurrency(priceTotal)%>&nbsp;&nbsp;&nbsp;</div>
			<div class="table-cell" style="width:110px; font-weight:bold;" align="right"><%=formatcurrency(cogsTotal)%>&nbsp;&nbsp;&nbsp;</div>
			<div class="table-cell" style="width:110px;  font-weight:bold; color:#060;" align="right"><%=formatcurrency(revenueTotal)%>&nbsp;&nbsp;&nbsp;</div>
		</div>
		<%
	end if
end if

call CloseConn(oConn)
%>
