<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%

	call getDBConn(oConn)

	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	
	sql = "select modelID, modelName from we_models where brandID = " & brandID & " order by modelName"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
%>
<select id="Model" name="Model">
    <option value="">-- Select Model --</option>
    <%
	do while not rs.EOF
	%>
    <option value="<%=rs("modelID")%>"><%=rs("modelName")%></option>
    <%
		rs.movenext
	loop
	rs = null
	%>
</select>
<%
	call CloseConn(oConn)
%>