<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%	
	dim thisUser, pageTitle, header
	
	brandID = ds(request.QueryString("brandID"))
	if isnull(brandID) or len(brandID) < 1 then brandID = 0
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	sql = "select modelID, modelName from we_models where brandID = " & brandID & " and hideLive = 0 order by modelName"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	if rs.EOF then
		response.Write("No Models Available<input type='hidden' name='modelID' value='0' />")
	else
%>
<select name="modelID">
	<option value="">Select Model</option>
    <%
	do while not rs.EOF
	%>
    <option value="<%=rs("modelID")%>"><%=rs("modelName")%></option>
    <%
		rs.movenext
	loop
	%>
</select>
<%
	end if
	
	call CloseConn(oConn)
%>