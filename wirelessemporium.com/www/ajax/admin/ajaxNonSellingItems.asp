<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.Open Session("ConnectionString")

%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%

	dim brandID
	
	brandID = prepInt(request.QueryString("brandID"))
	modelID = prepInt(request.QueryString("modelID"))
	
	if brandID > 0 then
		sql = "select modelID, modelName from we_models where brandID = " & brandID & " order by modelName"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
%>
<div class="fl menuTitle">Model:</div>
<div class="fl">
    <select name="modelID">
        <option value="">Select Model</option>
        <%
        do while not rs.EOF
            curModelID = rs("modelID")
        %>
        <option value="<%=curModelID%>"<% if modelID = curModelID then %> selected="selected"<% end if %>><%=rs("modelName")%></option>
        <%
            rs.movenext
        loop
        %>
    </select>
</div>
<%
	else
%>
<div class="fl menuTitle">Model:</div>
<div class="fl">
    <select name="modelID">
        <option value="">You must select a brand first</option>
    </select>
</div>
<%
	end if
	
	oConn.Close
	set oConn = nothing
%>