<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/Framework/Utility/Static.asp"-->
<%
timeStamp = Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now)

Set objZip = Server.CreateObject("XStandard.Zip")
dateSearch = prepStr(request.querystring("dateSearch"))
datafeed = prepStr(request.querystring("datafeed"))

if datafeed <> "" then
	sourceFiles = replace(server.MapPath("\tempCSV"), "\staging\", "\www\") & "\" & datafeed & "*.txt"
	zipFileName = replace(server.MapPath("\tempCSV"), "\staging\", "\www\") & "\" & datafeed & ".zip"
	objZip.Pack sourceFiles, zipFileName
	%>
    <a href="/tempCSV/<%=datafeed%>.zip?v=<%=timeStamp%>"><%=datafeed%>.zip</a>
    <%
else
	zipDate =  replace(dateSearch, "/", "-")
	
	sourceFiles = replace(server.MapPath("\admin\tempPDF\SalesOrders"), "\staging\", "\www\") & "\*" & zipDate & "*.pdf"
	zipFileName = replace(server.MapPath("\admin\tempPDF\SalesOrders"), "\staging\", "\www\") & "\" & zipDate & ".zip"
	objZip.Pack sourceFiles, zipFileName
	%>
    <a href="/admin/tempPDF/SalesOrders/<%=zipDate%>.zip?v=<%=timeStamp%>"><%=zipDate%>.zip</a>
    <%
end if

set objZip = nothing
call CloseConn(oConn)
%>
