<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	
	
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	call getDBConn(oConn)
	
	dim templateCode : templateCode = prepStr(request.QueryString("templateCode"))
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim saveModelCode : saveModelCode = prepStr(request.QueryString("saveModelCode"))
	dim pnModel : pnModel = prepStr(request.QueryString("pnModel"))
	dim vendorCode : vendorCode = prepStr(request.QueryString("vendorCode"))
	dim vendorID : vendorID = prepStr(request.QueryString("vendorID"))
	dim cogs : cogs = prepInt(request.QueryString("cogs"))
	dim getBrandCode : getBrandCode = prepInt(request.QueryString("getBrandCode"))
	dim brandCode : brandCode = prepStr(request.QueryString("brandCode"))
	dim prodChart : prodChart = prepStr(request.QueryString("prodChart"))
	dim saveProdID : saveProdID = prepInt(request.QueryString("saveProdID"))
	dim removeProdID : removeProdID = prepInt(request.QueryString("removeProdID"))
	dim newDesign : newDesign = prepStr(request.QueryString("newDesign"))
	dim prodCode : prodCode = prepStr(request.QueryString("prodCode"))
	dim deleteDesign : deleteDesign = prepInt(request.QueryString("deleteDesign"))
	dim newPN : newPN = prepStr(request.QueryString("newPN"))
	dim prodList : prodList = prepStr(request.QueryString("prodList"))
	dim subType : subType = prepInt(request.QueryString("subType"))
	dim defaultCogs : defaultCogs = prepInt(request.QueryString("defaultCogs"))
	dim genTypeID : genTypeID = prepInt(request.QueryString("genTypeID"))
	dim genDesc : genDesc = prepStr(request.QueryString("genDesc"))
	dim updatedPN : updatedPN = prepStr(request.QueryString("updatedPN"))
	dim newItemID : newItemID = prepInt(request.QueryString("newItemID"))
	dim buildMsg : buildMsg = ""
	
	if saveProdID > 0 then
		session("prodList") = session("prodList") & saveProdID & "##"
		response.Write("prodList:" & session("prodList"))
	elseif removeProdID > 0 then
		session("prodList") = replace(session("prodList"),removeProdID & "##","")
		response.Write("prodList:" & session("prodList"))
	elseif deleteDesign > 0 then
		sql = "delete from we_itemDesc where id = " & deleteDesign
		session("errorSQL") = sql
		oConn.execute(sql)
	elseif vendorID <> "" then
		if newPN <> updatedPN then
			sql = "update we_Items set partnumber = '" & updatedPN & "' where partnumber = '" & newPN & "'"
			session("errorSQL") = sql
			oConn.execute(sql)
			newPN = updatedPN
		end if
		sql = "insert into we_vendorNumbers (we_partNumber,vendor,partNumber,cogs) values('" & newPN & "','" & vendorCode & "','" & vendorID & "'," & cogs & ")"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		if vendorCode = "DW" then
			'get the image from their site
			
		end if
	elseif newPN <> updatedPN then
		sql = "update we_Items set partnumber = '" & updatedPN & "' where partnumber = '" & newPN & "'"
		session("errorSQL") = sql
		oConn.execute(sql)
		newPN = updatedPN
		
		response.Write("partnumber updated:" & sql)
		call CloseConn(oConn)
		response.End()
	end if
	
	if brandID = 0 and getBrandCode = 0 then
		response.Write("<!-- nothing to see here -->")
		call CloseConn(oConn)
		response.End()
	end if
	
	function removeBrandModel(val)
		val = replace(val,"Apple","XXX")
		val = replace(val,"iPhone 4","YYY")
		removeBrandModel = val
	end function
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	if getBrandCode > 0 then
		sql = "select brandCode from we_brands where brandID = " & getBrandCode
		session("errorSQL") = sql
		set brandCodeRS = oConn.execute(sql)
%>
<input type="text" name="brandCode" value="<%=brandCodeRS("brandCode")%>" size="3" />
<%
	elseif prodChart <> "" then
		if newDesign <> "" then
			sql = "select max(cast(sufix as int)) as curHigh from we_itemDesc where code = '" & prodCode & "'"
			session("errorSQL") = sql
			set sufixRS = oConn.execute(sql)
			
			newSufix = prepInt(sufixRS("curHigh")) + 1
			if len(newSufix) = 1 then newSufix = "0" & newSufix
			
			sql = "insert into we_itemDesc (code,itemDesc,sufix) values('" & prodCode & "','" & newDesign & "','" & newSufix & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		
		if subType > 0 then
			sql = "select id, itemDesc, itemPic from we_itemDesc where code = '" & prodChart & "' and groupID = " & subType & " order by groupID, itemDesc"
		else
			sql = "select id, itemDesc, itemPic from we_itemDesc where code = '" & prodChart & "' order by groupID, itemDesc"
		end if
		session("errorSQL") = sql
		set itemListRS = oConn.execute(sql)
		
		if itemListRS.EOF then response.Write("No Current Designs")
		
		do while not itemListRS.EOF
			curID = itemListRS("id")
			if curDesc = itemListRS("itemDesc") then
				sql = "delete from we_itemDesc where id = " & curID
				session("errorSQL") = sql
				oConn.execute(sql)
			else
				curDesc = itemListRS("itemDesc")
				itemPic = itemListRS("itemPic")
				if not fso.fileExists(server.MapPath("/productPics/thumb/" & itemPic)) then itemPic = "imagena.jpg"
%>
<div id="designRow_<%=curID%>" style="float:left; width:100%; position:relative;" onmouseover="this.style.backgroundColor='#CCFFFF'; document.getElementById('delete_<%=curID%>').style.display=''" onmouseout="this.style.backgroundColor='#ffffff'; document.getElementById('delete_<%=curID%>').style.display='none'">
	<div style="float:left;" onmouseover="document.getElementById('blowup_<%=curID%>').style.display=''" onmouseout="document.getElementById('blowup_<%=curID%>').style.display='none'"><img src="/productPics/thumb/<%=itemPic%>" border="0" height="50" /></div>
    <div style="float:left; height:50px;"><input type="checkbox" name="prodChart_<%=curID%>" value="1" onclick="saveProdID(this.checked,<%=curID%>)" /></div>
	<div style="float:left; height:50px; padding-left:10px; font-size:10px;"><%=curDesc%></div>
    <% if prodChart <> "TPU" then %>
    <div id="delete_<%=curID%>" style="float:right; height:50px; color:#F00; font-weight:bold; font-size:18px; padding-right:10px; cursor:pointer; display:none;" onclick="deleteItem(<%=curID%>)">X</div>
    <% end if %>
    <div id="blowup_<%=curID%>" style="position:absolute; top:20px; left:55px; display:none; z-index:999;"><img src="/productPics/big/<%=itemPic%>" border="0" height="200" /></div>
</div>
<%
			end if
			itemListRS.movenext
		loop
	elseif templateCode <> "" then
		buildMsg = buildMsg & "start creating products<br>"
		pnBrand = brandCode
		dim dynamicCreate : dynamicCreate = 0
		
		session("errorSQL") = "get the length"
		session("errorSQL") = "length:" & len(templateCode)
		if len(templateCode) > 3 then
			if left(templateCode,4) = "LC7 " or left(templateCode,4) = "TPU " or left(templateCode,4) = "FPX " or left(templateCode,4) = "SCR " then listObj = 1
		else
			if templateCode = "TRI" or templateCode = "WLT" or templateCode = "SCR" then listObj = 1
		end if
		buildMsg = buildMsg & "listObj:" & listObj & "<br>"
		
		if templateCode = "FP2" then useItemID = 102959
		if templateCode = "LC7 Heavy-Duty Dual Layer" then useItemID = 233721
		if templateCode = "LC7 Perforated Armor" then useItemID = 233725
		if templateCode = "LC7 Defense Duo" then useItemID = 233728
		if templateCode = "LC7 Heavy-Duty Dual Layer w/ Kickstand" then useItemID = 233729
		if templateCode = "TPU Crystal Skin w/Stand" then useItemID = 233735
		if templateCode = "LC7 CarbonShield" then useItemID = 261550
		if templateCode = "FPX My.Carbon Armor" then useItemID = 261537
		if templateCode = "FPX Rubberized Snap-On" then useItemID = 261571
		if templateCode = "SCR Supreme Guardz" then useItemID = 261659
		if templateCode = "SCR Tempered Glass" then useItemID = 349158
		'Added on 4-15-2014
		if templateCode = "LC7 Armor Guard Hybrid Case w/Kickstand" then useItemID = 319795
		if templateCode = "FPX Protex Rubberized" then useItemID = 319796
		if templateCode = "LC7 Duo Shield Armor Case" then useItemID = 319797
		if templateCode = "LC7 Tri Shield Hybrid" then useItemID = 319798
		if templateCode = "WLT" then useItemID = 319799
		if templateCode = "TRI" then useItemID = 319800
		if templateCode = "LC7 Goospery Focus Bumper" then useItemID = 319801
		if templateCode = "FPC" then useItemID = 325734
		if templateCode = "SCR" then useItemID = 101835
		if templateCode = "LC7 3-In-1 ArmorCase w/ Kickstand" then useItemID = 342788
		if templateCode = "LC7 Textured w/ Kickstand" then useItemID = 342788
		
		if templateCode = "FP2" or prepInt(listObj) = 1 then
			buildMsg = buildMsg & "Using the list object<br>"
			dynamicCreate = 1
			if prodList = "" then
				response.Write("no products selected:" & prodList)
				call CloseConn(oConn)
				response.End()
			end if
			session("errorSQL") = "prodList:" & prodList
			prodList = left(prodList,len(prodList)-1)
			
			sql = "select id, itemDesc, sufix from we_ItemDesc where id in (" & prodList & ")"
			session("errorSQL") = sql
			set fp2RS = oConn.execute(sql)
			
			colorList = ""
			colorCodeList = ""
			templateList = ""
			lap = 0
			do while not fp2RS.EOF
				lap = lap + 1
				useLap = lap
				do while len(useLap) < 3
					useLap = "0" & useLap
				loop
				colorList = colorList & fp2RS("itemDesc") & "##"
				colorCodeList = colorCodeList & fp2RS("sufix") & "##"
				templateList = templateList & useItemID & "##"
				fp2RS.movenext
			loop
			colorList = left(colorList,len(colorList)-2)
			colorCodeList = left(colorCodeList,len(colorCodeList)-2)
			templateList = left(templateList,len(templateList)-2)
		elseif templateCode = "FP5" then
			dynamicCreate = 1
			if prodList = "" then
				response.Write("no products selected:" & prodList)
				call CloseConn(oConn)
				response.End()
			end if
			session("errorSQL") = "prodList:" & prodList
			prodList = left(prodList,len(prodList)-1)
			
			sql = "select id, itemDesc, sufix from we_ItemDesc where id in (" & prodList & ")"
			session("errorSQL") = sql
			set fp5RS = oConn.execute(sql)
			
			colorList = ""
			colorCodeList = ""
			templateList = ""
			lap = 0
			do while not fp5RS.EOF
				lap = lap + 1
				useLap = lap
				do while len(useLap) < 3
					useLap = "0" & useLap
				loop
				colorList = colorList & fp5RS("itemDesc") & "##"
				colorCodeList = colorCodeList & fp5RS("sufix") & "##"
				templateList = templateList & "117692##"
				fp5RS.movenext
			loop
			colorList = left(colorList,len(colorList)-2)
			colorCodeList = left(colorCodeList,len(colorCodeList)-2)
			templateList = left(templateList,len(templateList)-2)
		elseif templateCode = "TPU" then
			dynamicCreate = 1
			if prodList = "" then
				response.Write("no products selected:" & prodList)
				call CloseConn(oConn)
				response.End()
			end if
			session("errorSQL") = "prodList:" & prodList
			prodList = left(prodList,len(prodList)-1)
			
			sql = "select id, itemDesc, sufix, groupID from we_ItemDesc where id in (" & prodList & ")"
			session("errorSQL") = sql
			set tpuRS = oConn.execute(sql)
			
			colorList = ""
			colorCodeList = ""
			templateList = ""
			lap = 0
			do while not tpuRS.EOF
				lap = lap + 1
				useLap = lap
				do while len(useLap) < 3
					useLap = "0" & useLap
				loop
				curColor = mid(tpuRS("itemDesc"),instr(tpuRS("itemDesc"),"(")+1)
				curColor = replace(curColor,")","")
				colorList = colorList & curColor & "##"
				colorCodeList = colorCodeList & tpuRS("sufix") & "##"
				if prepInt(tpuRS("groupID")) = 3 then
					templateList = templateList & "125805##"
				else
					templateList = templateList & "119914##"
				end if
				tpuRS.movenext
			loop
			colorList = left(colorList,len(colorList)-2)
			colorCodeList = left(colorCodeList,len(colorCodeList)-2)
			templateList = left(templateList,len(templateList)-2)
		elseif templateCode = "FP3" then
			dynamicCreate = 1
			if prodList = "" then
				response.Write("no products selected:" & prodList)
				call CloseConn(oConn)
				response.End()
			end if
			session("errorSQL") = "prodList:" & prodList
			prodList = left(prodList,len(prodList)-1)
			
			sql = "select id, itemDesc, sufix, groupID from we_ItemDesc where id in (" & prodList & ")"
			session("errorSQL") = sql
			set tpuRS = oConn.execute(sql)
			
			colorList = ""
			colorCodeList = ""
			templateList = ""
			lap = 0
			do while not tpuRS.EOF
				lap = lap + 1
				useLap = lap
				do while len(useLap) < 3
					useLap = "0" & useLap
				loop
				curColor = mid(tpuRS("itemDesc"),instr(tpuRS("itemDesc"),"(")+1)
				curColor = replace(curColor,")","")
				colorList = colorList & curColor & "##"
				colorCodeList = colorCodeList & tpuRS("sufix") & "##"
				templateList = templateList & "99016##"
				tpuRS.movenext
			loop
			colorList = left(colorList,len(colorList)-2)
			colorCodeList = left(colorCodeList,len(colorCodeList)-2)
			templateList = left(templateList,len(templateList)-2)
			
			'colorList = "Hot Pink##Lime Green##Orange##Gray##White##Black##Purple##Red##Blue##Green##Yellow##Rose Pink##Cool Blue"
			'colorCodeList = "14##27##24##02##11##10##21##20##17##16##26##04##18"
			'templateList = "99016##99016##99016##99016##99016##99016##99016##99016##99016##99016##99016##99016##99016"
		elseif templateCode = "TRIAD" then
			dynamicCreate = 1
			if prodList = "" then
				response.Write("no products selected:" & prodList)
				call CloseConn(oConn)
				response.End()
			end if
			session("errorSQL") = "prodList:" & prodList
			prodList = left(prodList,len(prodList)-1)
			
			sql = "select id, itemDesc, sufix, groupID from we_ItemDesc where id in (" & prodList & ")"
			session("errorSQL") = sql
			set tpuRS = oConn.execute(sql)
			
			colorList = ""
			colorCodeList = ""
			templateList = ""
			lap = 0
			do while not tpuRS.EOF
				lap = lap + 1
				useLap = lap
				do while len(useLap) < 3
					useLap = "0" & useLap
				loop
				curColor = mid(tpuRS("itemDesc"),instr(tpuRS("itemDesc"),"(")+1)
				curColor = replace(curColor,")","")
				colorList = colorList & curColor & "##"
				colorCodeList = colorCodeList & tpuRS("sufix") & "##"
				templateList = templateList & "233084##"
				tpuRS.movenext
			loop
			colorList = left(colorList,len(colorList)-2)
			colorCodeList = left(colorCodeList,len(colorCodeList)-2)
			templateList = left(templateList,len(templateList)-2)
			
			'colorList = "White/Sky Blue##White/Purple##White/Neon Green##White/Hot Pink##White/Black##Hot Pink/White##Neon Green/White##Purple/White##Sky Blue/White##Red/White##Black/Blue##Black/Yellow##Black/White##Black/Black##Sky Blue/Hot Pink##Blue/Black##Hot Pink/Black##Neon Green/Black##Red/Black##White/Sky Blue##Yellow/Sky Blue"
			'colorCodeList = "17##21##27##14##10##30##31##32##33##34##35##36##37##38##39##40##41##42##43##44##45"
			'templateList = "233084##233084##233084##233084##233084##233084##233084##233084##233084##233084##233084##233084##233084##233084##233084##233084##233084##233084##233084##233084##233084"
		elseif templateCode = "SCR" then
			colorList = "Clear##Mirror##Anti-Glare##3-Pack"
			colorCodeList = "01##02##04##07"
			templateList = "101835##101836##101837##101838"
		elseif templateCode = "BAT" then
			colorList = "Replacement"
			colorCodeList = "01"
			templateList = "222829"
		elseif templateCode = "GEN" then			
			dynamicCreate = 1
			if prodList = "" then
				response.Write("no products selected:" & prodList)
				call CloseConn(oConn)
				response.End()
			end if
			session("errorSQL") = "prodList:" & prodList
			prodList = left(prodList,len(prodList)-1)
			
			sql = "select id, itemDesc, sufix, groupID from we_ItemDesc where id in (" & prodList & ")"
			session("errorSQL") = sql
			set tpuRS = oConn.execute(sql)
			
			colorList = ""
			colorCodeList = ""
			templateList = ""
			lap = 0
			do while not tpuRS.EOF
				lap = lap + 1
				useLap = lap
				do while len(useLap) < 3
					useLap = "0" & useLap
				loop
				curColor = mid(tpuRS("itemDesc"),instr(tpuRS("itemDesc"),"(")+1)
				curColor = replace(curColor,")","")
				colorList = colorList & curColor & "##"
				colorCodeList = colorCodeList & tpuRS("sufix") & "##"
				templateList = templateList & "224905##"
				tpuRS.movenext
			loop
			colorList = left(colorList,len(colorList)-2)
			colorCodeList = left(colorCodeList,len(colorCodeList)-2)
			templateList = left(templateList,len(templateList)-2)
		elseif templateCode = "HST" then
			colorList = "Holster1##Holster2"
			colorCodeList = "01##03"
			templateList = "103044##103046"
		elseif templateCode = "LC5" then
			dynamicCreate = 1
			if prodList = "" then
				response.Write("no products selected:" & prodList)
				call CloseConn(oConn)
				response.End()
			end if
			session("errorSQL") = "prodList:" & prodList
			prodList = left(prodList,len(prodList)-1)
			
			sql = "select id, itemDesc, sufix, groupID from we_ItemDesc where id in (" & prodList & ")"
			session("errorSQL") = sql
			set tpuRS = oConn.execute(sql)
			
			colorList = ""
			colorCodeList = ""
			templateList = ""
			lap = 0
			do while not tpuRS.EOF
				lap = lap + 1
				useLap = lap
				do while len(useLap) < 3
					useLap = "0" & useLap
				loop
				curColor = mid(tpuRS("itemDesc"),instr(tpuRS("itemDesc"),"(")+1)
				curColor = replace(curColor,")","")
				colorList = colorList & curColor & "##"
				colorCodeList = colorCodeList & tpuRS("sufix") & "##"
				templateList = templateList & "108795##"
				tpuRS.movenext
			loop
			colorList = left(colorList,len(colorList)-2)
			colorCodeList = left(colorCodeList,len(colorCodeList)-2)
			templateList = left(templateList,len(templateList)-2)
			
			'colorList = "Hot Pink##Lime Green##Orange##White##Black##Purple##Red##Blue##Trans. Smoke##Trans. Clear"
			'colorCodeList = "14##27##24##11##10##21##20##17##02##01"
			'templateList = "108795##108795##108795##108795##108795##108795##108795##108795##108795##108795##108795"
		else
			buildMsg = buildMsg & "Missing Template Instructions<br>"
		end if
		
		pnType = templateCode
		loopArray = split(colorList,"##")
		pnTailArray = split(colorCodeList,"##")
		templateArray = split(templateList,"##")
		
		sql = "select a.modelName, b.brandName from we_models a left join we_brands b on a.brandID = b.brandID where a.modelID = " & modelID
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		modelName = rs("modelName")
		brandName = rs("brandName")
		
		for quickCreateLap = 0 to ubound(loopArray)
			itemDescExists = 0
			newItemDesc = loopArray(quickCreateLap)
			pnTail = pnTailArray(quickCreateLap)
			if pnType = "TRIAD" or ogPnType = "TRIAD" then
				ogPnType = "TRIAD"
				pnType = "LC7"
				pnTail = "TR" & pnTail
			elseif pnType = "LC7 Heavy-Duty Dual Layer" or ogPnType = "LC7 Heavy-Duty Dual Layer" then
				ogPnType = "LC7 Heavy-Duty Dual Layer"
				pnType = "LC7"
				pnTail = "DL" & pnTail
			elseif pnType = "LC7 Perforated Armor" or ogPnType = "LC7 Perforated Armor" then
				ogPnType = "LC7 Perforated Armor"
				pnType = "LC7"
				pnTail = "V" & pnTail
			elseif pnType = "LC7 Defense Duo" or ogPnType = "LC7 Defense Duo" then
				ogPnType = "LC7 Defense Duo"
				pnType = "LC7"
				pnTail = "DD" & pnTail
			elseif pnType = "LC7 Heavy-Duty Dual Layer w/ Kickstand" or ogPnType = "LC7 Heavy-Duty Dual Layer w/ Kickstand" then
				ogPnType = "LC7 Heavy-Duty Dual Layer w/ Kickstand"
				pnType = "LC7"
				pnTail = "DK" & pnTail
			elseif pnType = "TPU Crystal Skin w/Stand" or ogPnType = "TPU Crystal Skin w/Stand" then
				ogPnType = "TPU Crystal Skin w/Stand"
				pnType = "TPU"
				pnTail = "CS" & pnTail
			elseif pnType = "FPX My.Carbon Armor" or ogPnType = "FPX My.Carbon Armor" then
				ogPnType = "FPX My.Carbon Armor"
				pnType = "FPX"
				pnTail = "MY" & pnTail
			elseif pnType = "LC7 CarbonShield" or ogPnType = "LC7 CarbonShield" then
				ogPnType = "LC7 CarbonShield"
				pnType = "LC7"
				pnTail = "CS" & pnTail
			elseif pnType = "FPX Rubberized Snap-On" or ogPnType = "FPX Rubberized Snap-On" then
				ogPnType = "FPX Rubberized Snap-On"
				pnType = "FPX"
			elseif pnType = "SCR Supreme Guardz" or ogPnType = "SCR Supreme Guardz" then
				ogPnType = "SCR Supreme Guardz"
				pnType = "SCR"
				pnTail = "20"
			'added 6-10-2014
			elseif pnType = "SCR Tempered Glass" or ogPnType = "SCR Tempered Glass" then
				ogPnType = "SCR Tempered Glass"
				pnType = "SCR"
				pnTail = "TG" & pnTail
			'added 4-15-2014
			elseif pnType = "LC7 Armor Guard Hybrid Case w/Kickstand" or ogPnType = "LC7 Armor Guard Hybrid Case w/Kickstand" then
				ogPnType = "LC7 Armor Guard Hybrid Case w/Kickstand"
				pnType = "LC7"
				pnTail = "AG" & pnTail
			elseif pnType = "FPX Protex Rubberized" or ogPnType = "FPX Protex Rubberized" then
				ogPnType = "FPX Protex Rubberized"
				pnType = "FPX"
				pnTail = "BC" & pnTail
			elseif pnType = "LC7 Duo Shield Armor Case" or ogPnType = "LC7 Duo Shield Armor Case" then
				ogPnType = "LC7 Duo Shield Armor Case"
				pnType = "LC7"
				pnTail = "DS" & pnTail
			elseif pnType = "LC7 Tri Shield Hybrid" or ogPnType = "LC7 Tri Shield Hybrid" then
				ogPnType = "LC7 Tri Shield Hybrid"
				pnType = "LC7"
				pnTail = "TS" & pnTail
			elseif pnType = "WLT" or ogPnType = "WLT" then
				ogPnType = "WLT"
				pnType = "WLT"
				pnTail = "FD" & pnTail
			elseif pnType = "TRI" or ogPnType = "TRI" then
				ogPnType = "TRI"
				pnType = "TRI"
				pnTail = "A" & pnTail
			elseif pnType = "SCR" or ogPnType = "SCR" then
				ogPnType = "SCR"
				pnType = "SCR"
			elseif pnType = "LC7 Goospery Focus Bumper" or ogPnType = "LC7 Goospery Focus Bumper" then
				ogPnType = "LC7 Goospery Focus Bumper"
				pnType = "LC7"
				pnTail = "FB" & pnTail
			elseif pnType = "FPC" or ogPnType = "FPC" then
				ogPnType = "FPC"
				pnType = "C" & pnType
			elseif pnType = "LC7 3-In-1 ArmorCase w/ Kickstand" or ogPnType = "LC7 3-In-1 ArmorCase w/ Kickstand" then
				ogPnType = "LC7 3-In-1 ArmorCase w/ Kickstand"
				pnType = "LC7"
				pnTail = "AC" & pnTail
			elseif pnType = "LC7 Textured w/ Kickstand" or ogPnType = "LC7 Textured w/ Kickstand" then
				ogPnType = "LC7 Textured w/ Kickstand"
				pnType = "LC7"
				pnTail = "TX" & pnTail
			end if
			partNumber = pnType & "-" & pnBrand & "-" & pnModel & "-" & pnTail
			if pnType = "GEN" then
				sql = "select count(*) as prodCnt, (select max(cast(SUBSTRING(PartNumber,14,10) as int)) as curHigh from we_items where partnumber like '" & pnType & "-" & pnBrand & "-" & pnModel & "-%' and ISNUMERIC(SUBSTRING(PartNumber,14,10)) = 1) as curMaxSufix from we_Items where partNumber = '" & partNumber & "'"
			else
				sql = "select count(*) as prodCnt, (select max(sufix) as curHigh from we_itemDesc where code = '" & pnType & "') as curMaxSufix from we_Items where partNumber = '" & partNumber & "'"
			end if
			session("errorSQL") = sql
			set cntRS = oConn.execute(sql)
			
			'if templateCode = "GEN" then 
			'	do while cntRS("prodCnt") > 0
			'		pnTail = prepInt(pnTail) + 1
			'		if len(pnTail) < 2 then pnTail = "0" & pnTail
			'		newPN = pnType & "-" & pnBrand & "-" & pnModel & "-" & pnTail
			'		
			'		sql = "select count(*) as prodCnt from we_Items where partnumber = '" & newPN & "'"
			'		session("errorSQL") = sql
			'		set cntRS = oConn.execute(sql)
			'	loop
			'end if
			
			if cntRS("prodCnt") > 0 and dynamicCreate = 1 then
				buildMsg = buildMsg & "dynamicCreate Product<br>"
				if templateCode = "GEN" then
					sql = "select itemID from we_Items where modelID = " & modelID & " and itemDesc like '%" & genDesc & "%' and itemDesc like '%" & newItemDesc & "%'"
				else
					sql = "select itemID from we_Items where modelID = " & modelID & " and itemDesc like '%" & newItemDesc & "%'"
				end if
				dim saveSQL01 : saveSQL01 = sql
				session("errorSQL") = sql
				set dupRS = oConn.execute(sql)
				
				if dupRS.EOF then
					curMaxSufix = prepInt(cntRS("curMaxSufix"))
					newSufix = curMaxSufix + 1
					
					'update sufix to first available
					sql = "update we_itemDesc set sufix = " & newSufix & " where code = '" & pnType & "' and sufix = " & pnTail
					session("errorSQL") = sql
					oConn.execute(sql)
					
					pnTail = newSufix
					
					sql = "select count(*) as prodCnt, (select max(cast(sufix as int)) as curHigh from we_itemDesc where code = '" & pnType & "') as curMaxSufix from we_Items where partNumber = '" & pnType & "-" & pnBrand & "-" & pnModel & "-" & pnTail & "'"
					session("errorSQL") = sql
					set cntRS = oConn.execute(sql)
				else
					itemDescExists = 1
				end if
			else
				if dynamicCreate = 1 then
					buildMsg = buildMsg & "dynamicCreate Not Needed<br>"
				else
					buildMsg = buildMsg & "Does not support dynamicCreate<br>"
				end if
			end if
			
			if cntRS("prodCnt") = 0 then
				sql = "delete from we_ItemsSlave"
				session("errorSQL") = sql
				oConn.execute(sql)
				
				sql = "SET NOCOUNT ON; insert into we_itemsSlave select [WE_URL],[brandID],[modelID],[typeID],[carrierID],[subtypeID],[vendor],[PartNumber],[itemDesc],[itemDesc_PS],[itemDesc_CA],[itemDesc_CO],[itemDesc_Google],[excludeFromCA],[ProductCatalog],[itemPic],[itemPic_CO],[itemSale],[price_Retail],[price_Our],[price_Our_BU],[price_CA],[price_CO],[price_PS],[price_ER],[price_Buy],[COGS],[numberOfSales],[inv_qty],[Seasonal],[Sports],[Bluetooth],[HandsfreeType],[ItemKit_OLD],[ItemKit_NEW],[hotDeal],[hideLive],[itemLongDetail],[itemHeader],[itemInfo],[BULLET1],[BULLET2],[BULLET3],[BULLET4],[BULLET5],[BULLET6],[BULLET7],[BULLET8],[BULLET9],[BULLET10],[COMPATIBILITY],[download_URL],[download_TEXT],[itemLongDetail_CA],[itemLongDetail_CO],[POINT1],[POINT2],[POINT3],[POINT4],[POINT5],[POINT6],[POINT7],[POINT8],[POINT9],[POINT10],[itemLongDetail_PS],[FEATURE1],[FEATURE2],[FEATURE3],[FEATURE4],[FEATURE5],[FEATURE6],[FEATURE7],[FEATURE8],[FEATURE9],[FEATURE10],[Features],[FormFactor],[BandType],[Band],[PackageContents],[TalkTime],[Standby],[Display],[Condition],[flag1],[UPCCode],[AMZN_sku],[BUY_sku],[itemDimensions],[itemWeight],[DateTimeEntd],[InvLastUpdatedBy],[InvLastUpdatedDate],[PriceLastUpdatedBy],[PriceLastUpdatedDate],[PS_videos],[Smartphone],[NoDiscount],[MobileLine_sku],[Phone_Compatibility],[DesignType],1 as master,0 as ghost,0 as lastCall,0 as topModel_PS,'' as itemLongDetail_ER,'' as itemDesc_ER,[colorID] from we_items where itemID = " & templateArray(quickCreateLap) & "; SELECT Scope_Identity() AS NewID;"
				session("errorSQL") = sql
				session("errorSQL2") = sql
				set objRS = oConn.execute(sql)
				newItemID = objRS.Fields("NewID").Value
				
				if templateCode = "SCR" and pnTail = "01" then
					sql = 	"update a " &_
							"set a.itemLongDetail = b.itemLongDetail, a.bullet1 = b.bullet1, a.bullet2 = b.bullet2, a.bullet3 = b.bullet3, a.bullet4 = b.bullet4, a.bullet5 = b.bullet5, a.bullet6 = b.bullet6, a.bullet7 = b.bullet7, a.bullet8 = b.bullet8, a.bullet9 = b.bullet9, a.bullet10 = b.bullet10, " &_
							"a.itemLongDetail_CO = b.itemLongDetail_CA, a.point1 = b.point1, a.point2 = b.point2, a.point3 = b.point3, a.point4 = b.point4, a.point5 = b.point5, a.point6 = b.point6, a.point7 = b.point7, a.point8 = b.point8, a.point9 = b.point9, a.point10 = b.point10, " &_
							"a.price_our = b.price_our, a.price_CO = b.price_CO, a.price_retail = b.price_retail, a.cogs = b.cogs, a.itemDesc = b.itemDesc, a.itemDesc_CO = b.itemDesc_CO " &_
							"from we_itemsSlave a " &_
								"outer apply (select * from we_Items where itemID = 101835) b " &_
							"where a.itemID = " & newItemID
					session("errorSQL") = sql
					oConn.execute(sql)
				elseif pnType = "SCR" and pnTail = "02" then
					sql = 	"update a " &_
							"set a.itemLongDetail = b.itemLongDetail, a.bullet1 = b.bullet1, a.bullet2 = b.bullet2, a.bullet3 = b.bullet3, a.bullet4 = b.bullet4, a.bullet5 = b.bullet5, a.bullet6 = b.bullet6, a.bullet7 = b.bullet7, a.bullet8 = b.bullet8, a.bullet9 = b.bullet9, a.bullet10 = b.bullet10, " &_
							"a.itemLongDetail_CO = b.itemLongDetail_CA, a.point1 = b.point1, a.point2 = b.point2, a.point3 = b.point3, a.point4 = b.point4, a.point5 = b.point5, a.point6 = b.point6, a.point7 = b.point7, a.point8 = b.point8, a.point9 = b.point9, a.point10 = b.point10, " &_
							"a.price_our = b.price_our, a.price_CO = b.price_CO, a.price_retail = b.price_retail, a.cogs = b.cogs, a.itemDesc = b.itemDesc, a.itemDesc_CO = b.itemDesc_CO " &_
							"from we_itemsSlave a " &_
								"outer apply (select * from we_Items where itemID = 101836) b " &_
							"where a.itemID = " & newItemID
					session("errorSQL") = sql
					oConn.execute(sql)
				elseif pnType = "SCR" and pnTail = "03" then
					sql = 	"update a " &_
							"set a.itemLongDetail = b.itemLongDetail, a.bullet1 = b.bullet1, a.bullet2 = b.bullet2, a.bullet3 = b.bullet3, a.bullet4 = b.bullet4, a.bullet5 = b.bullet5, a.bullet6 = b.bullet6, a.bullet7 = b.bullet7, a.bullet8 = b.bullet8, a.bullet9 = b.bullet9, a.bullet10 = b.bullet10, " &_
							"a.itemLongDetail_CO = b.itemLongDetail_CA, a.point1 = b.point1, a.point2 = b.point2, a.point3 = b.point3, a.point4 = b.point4, a.point5 = b.point5, a.point6 = b.point6, a.point7 = b.point7, a.point8 = b.point8, a.point9 = b.point9, a.point10 = b.point10, " &_
							"a.price_our = b.price_our, a.price_CO = b.price_CO, a.price_retail = b.price_retail, a.cogs = b.cogs, a.itemDesc = b.itemDesc, a.itemDesc_CO = b.itemDesc_CO " &_
							"from we_itemsSlave a " &_
								"outer apply (select * from we_Items where itemID = 101837) b " &_
							"where a.itemID = " & newItemID
					session("errorSQL") = sql
					oConn.execute(sql)
				elseif pnType = "SCR" and pnTail = "04" then
					sql = 	"update a " &_
							"set a.itemLongDetail = b.itemLongDetail, a.bullet1 = b.bullet1, a.bullet2 = b.bullet2, a.bullet3 = b.bullet3, a.bullet4 = b.bullet4, a.bullet5 = b.bullet5, a.bullet6 = b.bullet6, a.bullet7 = b.bullet7, a.bullet8 = b.bullet8, a.bullet9 = b.bullet9, a.bullet10 = b.bullet10, " &_
							"a.itemLongDetail_CO = b.itemLongDetail_CA, a.point1 = b.point1, a.point2 = b.point2, a.point3 = b.point3, a.point4 = b.point4, a.point5 = b.point5, a.point6 = b.point6, a.point7 = b.point7, a.point8 = b.point8, a.point9 = b.point9, a.point10 = b.point10, " &_
							"a.price_our = b.price_our, a.price_CO = b.price_CO, a.price_retail = b.price_retail, a.cogs = b.cogs, a.itemDesc = b.itemDesc, a.itemDesc_CO = b.itemDesc_CO " &_
							"from we_itemsSlave a " &_
								"outer apply (select * from we_Items where itemID = 101838) b " &_
							"where a.itemID = " & newItemID
					session("errorSQL") = sql
					oConn.execute(sql)
				end if
				
				sql = "select * from we_itemsSlave where itemID = " & newItemID
				session("errorSQL") = session("errorSQL") & "<br>" & sql
				set rs = oConn.execute(sql)
				
				itemDesc = insertDetails(removeBrandModel(rs("itemDesc")))
				itemDesc_PS = insertDetails(removeBrandModel(rs("itemDesc_PS")))
				itemDesc_CA = insertDetails(removeBrandModel(rs("itemDesc_CA")))
				itemDesc_CO = insertDetails(removeBrandModel(rs("itemDesc_CO")))
				itemPic = insertDetails(removeBrandModel(rs("itemPic")))
				itemPic_CO = insertDetails(removeBrandModel(rs("itemPic_CO")))
				tempHolding = rs("itemLongDetail")
				itemLongDetail = insertDetails(removeBrandModel(tempHolding))
				bullet1 = insertDetails(removeBrandModel(rs("bullet1")))
				bullet2 = insertDetails(removeBrandModel(rs("bullet2")))
				bullet3 = insertDetails(removeBrandModel(rs("bullet3")))
				bullet4 = insertDetails(removeBrandModel(rs("bullet4")))
				bullet5 = insertDetails(removeBrandModel(rs("bullet5")))
				bullet6 = insertDetails(removeBrandModel(rs("bullet6")))
				bullet7 = insertDetails(removeBrandModel(rs("bullet7")))
				bullet8 = insertDetails(removeBrandModel(rs("bullet8")))
				bullet9 = insertDetails(removeBrandModel(rs("bullet9")))
				bullet10 = insertDetails(removeBrandModel(rs("bullet10")))
				itemLongDetail_CA = insertDetails(removeBrandModel(rs("itemLongDetail_CA")))
				itemLongDetail_CO = insertDetails(removeBrandModel(rs("itemLongDetail_CO")))
				point1 = insertDetails(removeBrandModel(rs("point1")))
				point2 = insertDetails(removeBrandModel(rs("point2")))
				point3 = insertDetails(removeBrandModel(rs("point3")))
				point4 = insertDetails(removeBrandModel(rs("point4")))
				point5 = insertDetails(removeBrandModel(rs("point5")))
				point6 = insertDetails(removeBrandModel(rs("point6")))
				point7 = insertDetails(removeBrandModel(rs("point7")))
				point8 = insertDetails(removeBrandModel(rs("point8")))
				point9 = insertDetails(removeBrandModel(rs("point9")))
				point10 = insertDetails(removeBrandModel(rs("point10")))
				itemLongDetail_PS = insertDetails(removeBrandModel(rs("itemLongDetail_PS")))
				feature1 = insertDetails(removeBrandModel(rs("feature1")))
				feature2 = insertDetails(removeBrandModel(rs("feature2")))
				feature3 = insertDetails(removeBrandModel(rs("feature3")))
				feature4 = insertDetails(removeBrandModel(rs("feature4")))
				feature5 = insertDetails(removeBrandModel(rs("feature5")))
				feature6 = insertDetails(removeBrandModel(rs("feature6")))
				feature7 = insertDetails(removeBrandModel(rs("feature7")))
				feature8 = insertDetails(removeBrandModel(rs("feature8")))
				feature9 = insertDetails(removeBrandModel(rs("feature9")))
				feature10 = insertDetails(removeBrandModel(rs("feature10")))
				newPN = pnType & "-" & pnBrand & "-" & pnModel & "-" & pnTail
				useCogs = defaultCogs
				
				sql = 	"update we_itemsSlave set vendor = '" & vendorCode & "', brandID = " & ds(brandID) & ", modelID = " & ds(modelID) & ", partNumber = '" & newPN & "', " &_
						"itemDesc = '" & ds(itemDesc) & "', itemDesc_PS = '" & ds(itemDesc_PS) & "', itemDesc_CA = '" & ds(itemDesc_CA) & "', itemDesc_CO = '" & ds(itemDesc_CO) & "', " &_
						"itemPic = '', itemPic_CO = '', itemLongDetail = '" & ds(itemLongDetail) & "', itemLongDetail_CA = '" & ds(itemLongDetail_CA) & "', itemLongDetail_CO = '" & ds(itemLongDetail_CO) & "', itemLongDetail_PS = '" & ds(itemLongDetail_PS) & "', " &_
						"bullet1 = '" & ds(bullet1) & "', bullet2 = '" & ds(bullet2) & "', bullet3 = '" & ds(bullet3) & "', bullet4 = '" & ds(bullet4) & "', bullet5 = '" & ds(bullet5) & "', bullet6 = '" & ds(bullet6) & "', bullet7 = '" & ds(bullet7) & "', bullet8 = '" & ds(bullet8) & "', bullet9 = '" & ds(bullet9) & "', bullet10 = '" & ds(bullet10) & "', " &_
						"point1 = '" & ds(point1) & "', point2 = '" & ds(point2) & "', point3 = '" & ds(point3) & "', point4 = '" & ds(point4) & "', point5 = '" & ds(point5) & "', point6 = '" & ds(point6) & "', point7 = '" & ds(point7) & "', point8 = '" & ds(point8) & "', point9 = '" & ds(point9) & "', point10 = '" & ds(point10) & "', " &_
						"feature1 = '" & ds(feature1) & "', feature2 = '" & ds(feature2) & "', feature3 = '" & ds(feature3) & "', feature4 = '" & ds(feature4) & "', feature5 = '" & ds(feature5) & "', feature6 = '" & ds(feature6) & "', feature7 = '" & ds(feature7) & "', feature8 = '" & ds(feature8) & "', feature9 = '" & ds(feature9) & "', feature10 = '" & ds(feature10) & "' " &_
						"where itemID = " & newItemID
				session("errorSQL") = sql
				oConn.execute(sql)
				
				sql = "SET NOCOUNT ON; insert into we_items select [WE_URL],[brandID],[modelID],[typeID],[carrierID],[subtypeID],[vendor],[PartNumber],[itemDesc],[itemDesc_PS],[itemDesc_CA],[itemDesc_CO],[itemDesc_Google],[excludeFromCA],[ProductCatalog],[itemPic],[itemPic_CO],[itemSale],[price_Retail],[price_Our],[price_Our_BU],[price_CA],[price_CO],[price_PS],[price_ER],[price_Buy],[COGS],[numberOfSales],[inv_qty],[Seasonal],[Sports],[Bluetooth],[HandsfreeType],[ItemKit_OLD],[ItemKit_NEW],[hotDeal],[hideLive],[itemLongDetail],[itemHeader],[itemInfo],[BULLET1],[BULLET2],[BULLET3],[BULLET4],[BULLET5],[BULLET6],[BULLET7],[BULLET8],[BULLET9],[BULLET10],[COMPATIBILITY],[download_URL],[download_TEXT],[itemLongDetail_CA],[itemLongDetail_CO],[POINT1],[POINT2],[POINT3],[POINT4],[POINT5],[POINT6],[POINT7],[POINT8],[POINT9],[POINT10],[itemLongDetail_PS],[FEATURE1],[FEATURE2],[FEATURE3],[FEATURE4],[FEATURE5],[FEATURE6],[FEATURE7],[FEATURE8],[FEATURE9],[FEATURE10],[Features],[FormFactor],[BandType],[Band],[PackageContents],[TalkTime],[Standby],[Display],[Condition],[flag1],[UPCCode],[AMZN_sku],[BUY_sku],[itemDimensions],[itemWeight],[DateTimeEntd],[InvLastUpdatedBy],[InvLastUpdatedDate],[PriceLastUpdatedBy],[PriceLastUpdatedDate],[PS_videos],[Smartphone],[NoDiscount],[MobileLine_sku],[Phone_Compatibility],[DesignType],1 as master,0 as ghost,0 as lastCall,0 as topModel_PS,'' as itemLongDetail_ER,'' as itemDesc_ER,[colorID] from we_itemsSlave where itemID = " & newItemID & "; SELECT Scope_Identity() AS NewItemID;"
				session("errorSQL") = sql
				set objRS2 = oConn.execute(sql)
				createID = objRS2.Fields("NewItemID").Value
				
				if instr(genDesc,"(ZZZ)") < 1 then genDesc = genDesc & " (ZZZ)"
				if templateCode = "GEN" then
					useGenDesc = genDesc
					sql = "update we_Items set typeID = " & genTypeID & ", hideLive = 0, itemDesc = '" & insertDetails(useGenDesc) & "' where itemID = " & createID
					session("errorSQL") = sql
					oConn.execute(sql)
					itemDesc = useGenDesc
				end if
				
				sql = "delete from we_ItemsSlave"
				session("errorSQL") = sql
				oConn.execute(sql)
%>
<form name="vendorData_<%=newItemID%>" style="margin:0px; padding:0px;" onsubmit="return(false)">
<div style="float:left; width:100%; height:50px;">
    <div style="float:left; font-size:11px;" title="<%=itemDesc%>">
    	Partnumber <input type="text" name="updatedPN" value="<%=pnType%>-<%=pnBrand%>-<%=pnModel%>-<%=pnTail%>" size="17" /> Created!<br />
        <%=replace(replace(itemDesc,modelName,""),brandName,"")%>
    </div>
    <div style="float:left; font-size:11px; padding-left:10px;">VendorID:</div>
    <div style="float:left; padding-left:10px;"><input type="text" name="vendorID" value="" size="20" style="font-size:11px;" /></div>
    <div style="float:left; font-size:11px; padding-left:10px;">Cogs:</div>
    <div style="float:left; padding-left:10px;"><input type="text" name="cogs" value="<%=useCogs%>" size="5" style="font-size:11px;" /></div>
    <div style="float:left; padding-left:10px;">
    	<input type="button" name="myAction" value="Update" onclick="addVendor(<%=newItemID%>)" style="font-size:11px;" />
        <input type="hidden" name="vendorCode" value="<%=vendorCode%>" />
        <input type="hidden" name="newPN" value="<%=newPN%>" />
        <input type="hidden" name="itemID" value="<%=newItemID%>" />
    </div>
</div>
</form>
<%
			else
				if itemDescExists = 1 then
					response.Write("Item Description Already Exists (" & newItemDesc & ")<br>" & genDesc & "<br>" & saveSQL01 & "<br><br>")
				else
					response.Write("partNumber " & pnType & "-" & pnBrand & "-" & pnModel & "-" & pnTail & " Already Exists<br><br>")
				end if
			end if
		next
	elseif saveModelCode <> "" then
		sql = "update we_models set modelCode = '" & ucase(saveModelCode) & "' where modelID = " & modelID
		session("errorSQL") = sql
		oConn.execute(sql)
		
		response.Write("save complete")
	elseif modelID > 0 then
		sql = "select modelCode from we_models where modelID = " & modelID
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		modelCode = prepStr(rs("modelCode"))
%>
<input type="text" name="modelCode" value="<%=modelCode%>" size="4" maxlength="4" onchange="saveModelCode(<%=brandID%>,<%=modelID%>,this.value)" />
<%
	else
		sql = "select modelID, modelName from we_models where brandID = " & brandID & " order by modelName"
		session("errorSQL") = sql
		Set rs = oConn.execute(sql)
%>
<select name="modelID" onchange="modelSelect(<%=brandID%>,this.value)">
    <% if brandID = 9 then %>
    <option value="1674">Galaxy S5</option>
    <option value="1523">Galaxy S4</option>
    <option value="1624">Galaxy Note 3</option>
    <option value="1390">Galaxy S3</option>
    <option value="1605">Galaxy Mega 6.3</option>
    <option value="">----------------</option>
    <% elseif brandID = 17 then %>
    <option value="1628">iPhone 5s</option>
    <option value="1412">iPhone 5</option>
    <option value="1612">iPhone 5c</option>
    <option value="1643">iPad Air</option>
    <option value="1644">iPad Mini2</option>
    <option value="">----------------</option>
    <% elseif brandID = 4 then %>
    <option value="1608">G2 (AT&amp;T / T-Mobile)</option>
    <option value="1637">G2 (Sprint / Verizon)</option>
    <option value="1519">Optimus Logic L35G (Net10)/Optimus Zone VS410 (Verizon)</option>
    <option value="">----------------</option>
    <% elseif brandID = 5 then %>
    <option value="1658">Moto G</option>
    <option value="1572">Moto X</option>
    <option value="1445">Droid Razr M XT907 (Verizon)</option>
    <option value="1620">Droid Mini</option>
    <option value="1610">Droid Ultra XT-1080</option>
    <option value="">----------------</option>
    <% elseif brandID = 7 then %>
    <option value="1632">Lumia 520</option>
    <option value="1546">Lumia 521</option>
    <option value="1548">Lumia 928</option>
    <option value="1471">Lumia 920</option>
    <option value="">----------------</option>
    <% else %>
    <option value=""></option>
    <% end if %>
    <% do while not rs.EOF %>
    <option value="<%=rs("modelID")%>"><%=replace(rs("modelName"),"&","&amp;")%></option>
    <%
        rs.movenext
    loop
    %>
</select>
<%
	end if
	
	if buildMsg <> "" then
%>
<div id="buildMsgBttn" style="cursor:pointer; color:#06F; text-decoration:underline;" onclick="showBuildMsg()">View buildMsg</div>
<div id="buildMsgBox" style="display:none; font-weight:bold; color:#F00;"><%=buildMsg%></div>
<%
	end if
	
	call CloseConn(oConn)
%>