<%
	response.buffer = true
	response.expires = -1
%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	call getDBConn(oConn)

	dim siteID : siteID = prepInt(request.QueryString("siteID"))
	dim pathID : pathID = prepInt(request.QueryString("pathID"))
	dim adminID : adminID = prepInt(request.QueryString("adminID"))
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	sql	=	"select brandid, brandName from we_brands where brandid not in (12) order by brandName"
	arrBrands = getDbRows(sql)
	sql	=	"select typeid, typeName from we_types where typeid not in (4, 9) order by typeName"
	arrTypes = getDbRows(sql)
	sql	=	"select subtypeid, subtypeName, typeid, typeName from v_subtypeMatrix order by typeName, subtypeName"
	arrSubTypes = getDbRows(sql)
	sql	=	"select adminLvl, department from we_adminusers where adminid = '" & adminID & "'"
	set objRsUser = oConn.execute(sql)
	if not objRsUser.eof then	
		adminLvl = objRsUser("adminLvl")
		adminDept = objRsUser("department")
	end if

	sql	=	"select	a.id, a.site_id, a.path_id, a.typeid, a.subtypeid, a.brandid, a.modelid, a.carrierid, a.isPhone" & vbcrlf & _
			"	,	a.genre, a.validFrom, a.validTo, a.entryDate, a.isActive, a.isLive, a.eventText, a.comment" & vbcrlf & _
			"	,	a.position, a.url, a.vendorCode, b.fname" & vbcrlf & _
			"from	we_pixels a left outer join we_adminusers b" & vbcrlf & _
			"	on	a.adminID = b.adminID" & vbcrlf & _
			"where	path_id = '" & pathID & "'" & vbcrlf & _
			"	and	site_id = '" & siteID & "'" & vbcrlf & _
			"order by 1 desc" & vbcrlf
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
'	response.end
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	if rs.eof then
	%>
	<table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
        <tr><td align="center" style="padding:5px;"><h3>No pixel found</h3></td></tr>
	</table>
	<%
	else
		do until rs.eof
			pixelid = prepInt(rs("id"))
			eventText = rs("eventText")
			comment = rs("comment")
			brandid = prepInt(rs("brandid"))
			typeid = prepInt(rs("typeid"))
			subtypeid = prepInt(rs("subtypeid"))
			isPhone = rs("isPhone")
			genre = rs("genre")
			validFrom = rs("validFrom")
			validTo = rs("validTo")
			entryDate = rs("entryDate")
			isActive = rs("isActive")
			isLive = rs("isLive")
			tagPosition = prepInt(rs("position"))
			url = rs("url")
			vendorCode = prepStr(rs("vendorCode"))
			adminUser = rs("fname")
	%>
    <form name="frm<%=siteID%>_<%=pixelid%>" method="post"> 
	<table border="0" cellpadding="0" cellspacing="0" align="left" width="100%" style="padding-top:10px;">
        <tr>
            <td align="left">
                Pixel Title: <input type="text" name="txtTitle" value="<%=comment%>" size="50" /> <br />
                <textarea rows="10" cols="60" name="txtEventText"><%=eventText%></textarea>
            </td>
            <td align="left" valign="bottom">
                <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%" style="font-size:12px;">
                    <tr>
                        <td align="left">IsActive <input type="checkbox" name="chkActive" <%if isActive then %>checked<% end if%>/></td>
                        <%' if adminLvl >= 2 and adminDept = "Programming" then %>
						<td align="left">IsLive <input type="checkbox" name="chkLive" <%if isLive then %>checked<% end if%>/></td>                        
                        <%' else %>
						<!--<td align="left">IsLive <input type="checkbox" name="chkLive" <%if isLive then %>checked<% end if%> disabled="disabled" /></td>-->
                        <%' end if %>
                    </tr>
                    <tr>
                        <td align="left">Brands:</td>
                        <td align="right">
                            <select name="cbBrands" style="width:150px;">
                                <option value="0" selected>All Brands</option>
                            <%
                            for nRow=0 to ubound(arrBrands,2)
                            %>
                                <option value="<%=arrBrands(0, nRow)%>" <%if cint(arrBrands(0, nRow)) = cint(brandid) then %>selected<% end if%>><%=arrBrands(1, nRow)%></option>
                            <%
                            next
                            %>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Categories:</td>
                        <td align="right">
                            <select name="cbTypes" style="width:150px;">
                                <option value="0" selected>All Categories</option>
                            <%
                            for nRow=0 to ubound(arrTypes,2)
                            %>
                                <option value="<%=arrTypes(0, nRow)%>" <%if cint(arrTypes(0, nRow)) = cint(typeid) then %>selected<% end if%>><%=arrTypes(1, nRow)%></option>
                            <%
                            next
                            %>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Sub-Categories:</td>
                        <td align="right">
                            <select name="cbSubTypes" style="width:150px;">
                                <option value="0" selected>All Sub-Categories</option>
                            <%
                            for nRow=0 to ubound(arrSubTypes,2)
                            %>
                                <option value="<%=arrSubTypes(0, nRow)%>" <%if cint(arrSubTypes(0, nRow)) = cint(subtypeid) then %>selected<% end if%>><%=arrSubTypes(1, nRow)%></option>
                            <%
                            next
                            %>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Tag Position:</td>
                        <td align="right">
                            <select name="cbPosition" style="width:150px;">
                                <option value="1" <%if 1 = cint(tagPosition) then %>selected<% end if%>>Head</option>
                                <option value="2" <%if 2 = cint(tagPosition) then %>selected<% end if%>>Body Start</option>
                                <option value="3" <%if 3 = cint(tagPosition) then %>selected<% end if%>>Body End</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Vendor Code:</td>
                        <td align="right"><input type="text" name="txtVendorCode" value="<%=vendorCode%>" /></td>
                    </tr>
                    <tr>
                        <td align="left">Entry Date:</td>
                        <td align="left"><%=entryDate%></td>
                    </tr>
                    <tr>
                        <td align="left">Last Modifier:</td>
                        <td align="left"><%=adminUser%></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left" style="padding:5px; border-bottom:2px dotted #ccc;">
                <input type="submit" name="submitted" value="UPDATE CODE" onclick="return checkForm(document.frm<%=siteID%>_<%=pixelid%>);" />
                <input type="submit" name="submitted" value="Delete" onclick="return checkDelForm(document.frm<%=siteID%>_<%=pixelid%>);" />
            </td>
        </tr>
    </table>
	<input type="hidden" name="hidSiteID" value="<%=siteID%>" />
	<input type="hidden" name="cbPath" value="<%=pathID%>" />    
    <input type="hidden" name="hidPixelID" value="<%=pixelid%>" />
	<input type="hidden" name="hidUpdateType" value="1" />
    </form>
	<%
			rs.movenext
		loop
	end if
	
	call CloseConn(oConn)
	
	function getDbRows(pSql)
		session("errorSQL") = pSql
		Dim objRs	:	Set objRs = Server.CreateObject("ADODB.RecordSet")
		Dim tRet	:	tRet = NULL
		
		'CursorType : adOpenForwardOnly 0 , LockType adLockReadOnly 1
		objRs.Open pSql, oConn, 0, 1
	
		If objRs.EOF Then
			tRet = NULL
		Else
			tRet = objRs.GetRows()		
		End If
	
		objRs.Close : Set objRs = Nothing
	
		getDbRows = tRet
	end function	
%>