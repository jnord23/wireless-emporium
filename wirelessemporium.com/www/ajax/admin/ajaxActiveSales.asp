<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	
	call getDBConn(oConn)
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	dim incShip : incShip = prepInt(request.QueryString("incShip"))

	sql =	"select count(*) as cnt, 0 as uniqueVisits, 0 as pastUniqueVisits, store " &_
			"from we_orders a " &_
				"join v_accounts b on a.accountID = b.accountID and a.store = b.site_id " &_
			"where approved = 1 and cancelled is null and orderDateTime > '" & date & "' and email not like '%@wirelessemporium.com' and parentOrderID is null and (extOrderType is null or extOrderType < 4) " &_
			"group by store " &_
			"order by store"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	sql =	"select sum(c.price_our * b.quantity) as weTotal, sum(c.price_ca * b.quantity) as caTotal, sum(c.price_co * b.quantity) as coTotal, sum(c.price_ps * b.quantity) as psTotal, sum((c.price_our - 5) * b.quantity) as tmTotal, sum(c.cogs * b.quantity) as cogs, a.store " &_
			"from we_orders a " &_
				"join we_orderDetails b on a.orderID = b.orderID " &_
				"join we_items c on b.itemID = c.itemID " &_
				"join v_accounts d on a.accountID = d.accountID and a.store = d.site_id " &_
			"where a.approved = 1 and a.cancelled is null and a.orderdatetime > '" & date & "' and email not like '%@wirelessemporium.com' and parentOrderID is null and (extOrderType is null or extOrderType < 4) " &_
			"group by a.store"
	session("errorSQL") = sql
	set moneyRS = oConn.execute(sql)
	
	sql =	"select sum(cast(ordershippingfee as money)) as shippingTotal, store " &_
			"from we_orders a " &_
				"join v_accounts b on a.accountID = b.accountID and a.store = b.site_id " &_
			"where orderdatetime > '" & date & "' and approved = 1 and cancelled is null and email not like '%@wirelessemporium.com' and parentOrderID is null and (extOrderType is null or extOrderType < 4) " &_
			"group by store"
	session("errorSQL") = sql
	set shipRS = oConn.execute(sql)
	
	sql =	"select count(*) as cnt, store " &_
			"from we_orders a " &_
				"join v_accounts b on a.accountID = b.accountID and a.store = b.site_id " &_
			"where approved = 1 and cancelled is null and orderDateTime > '" & date-7 & "' and orderDateTime < '" & now-7 & "' and email not like '%@wirelessemporium.com' and parentOrderID is null and (extOrderType is null or extOrderType < 4) " &_
			"group by store order by store"
	session("errorSQL") = sql
	set ydaRS = oConn.execute(sql)
	
	sql =	"select sum(c.price_our * b.quantity) as weTotal, sum(c.price_ca * b.quantity) as caTotal, sum(c.price_co * b.quantity) as coTotal, sum(c.price_ps * b.quantity) as psTotal, sum((c.price_our - 5) * b.quantity) as tmTotal, sum(c.cogs * b.quantity) as cogs, a.store " &_
			"from we_orders a " &_
				"join we_orderDetails b on a.orderID = b.orderID " &_
				"join we_items c on b.itemID = c.itemID " &_
				"join v_accounts d on a.accountID = d.accountID and a.store = d.site_id " &_
			"where a.approved = 1 and a.cancelled is null and a.orderdatetime > '" & date-7 & "' and a.orderdatetime < '" & now-7 & "' and email not like '%@wirelessemporium.com' and parentOrderID is null and (extOrderType is null or extOrderType < 4) " &_
			"group by a.store"
	session("errorSQL") = sql
	set ydaMoneyRS = oConn.execute(sql)
	
	sql =	"select sum(cast(ordershippingfee as money)) as shippingTotal, store " &_
			"from we_orders a " &_
				"join v_accounts b on a.accountID = b.accountID and a.store = b.site_id " &_
			"where orderDateTime > '" & date-7 & "' and orderDateTime < '" & now-7 & "' and approved = 1 and cancelled is null and email not like '%@wirelessemporium.com' and parentOrderID is null and (extOrderType is null or extOrderType < 4) " &_
			"group by store"
	session("errorSQL") = sql
	set ydaShipRS = oConn.execute(sql)
	
	weAvgSales = 0
	caAvgSales = 0
	coAvgSales = 0
	psAvgSales = 0
	tmAvgSales = 0
	
	do while not rs.EOF
		if rs("store") = 0 then
			weAvgSales = formatNumber(rs("cnt"),0)
			weUnique = rs("uniqueVisits")
			weTotalVisits = rs("pastUniqueVisits")
		elseif rs("store") = 1 then
			caAvgSales = formatNumber(rs("cnt"),0)
			caUnique = rs("uniqueVisits")
			caTotalVisits = rs("pastUniqueVisits")
		elseif rs("store") = 2 then
			coAvgSales = formatNumber(rs("cnt"),0)
			coUnique = rs("uniqueVisits")
			coTotalVisits = rs("pastUniqueVisits")
		elseif rs("store") = 3 then
			psAvgSales = formatNumber(rs("cnt"),0)
			psUnique = rs("uniqueVisits")
			psTotalVisits = rs("pastUniqueVisits")
		elseif rs("store") = 10 then
			tmAvgSales = formatNumber(rs("cnt"),0)
			tmUnique = rs("uniqueVisits")
			tmTotalVisits = rs("pastUniqueVisits")
		end if
		rs.movenext
	loop
	
	do while not moneyRS.EOF
		if incShip = 1 then useShip = prepInt(shipRS("shippingTotal")) else useShip = 0
		if moneyRS("store") = 0 then
			if useShip > 0 then useShip = useShip - (weAvgSales * 3)
			weTotal = formatNumber(moneyRS("weTotal") + useShip,2)
			weCogs = formatNumber(moneyRS("cogs"),2)
		elseif moneyRS("store") = 1 then
			if useShip > 0 then useShip = useShip - (caAvgSales * 3)
			caTotal = formatNumber(moneyRS("caTotal") + useShip,2)
			caCogs = formatNumber(moneyRS("cogs"),2)
		elseif moneyRS("store") = 2 then
			if useShip > 0 then useShip = useShip - (coAvgSales * 3)
			coTotal = formatNumber(moneyRS("coTotal") + useShip,2)
			coCogs = formatNumber(moneyRS("cogs"),2)
		elseif moneyRS("store") = 3 then
			if useShip > 0 then useShip = useShip - (psAvgSales * 3)
			psTotal = formatNumber(moneyRS("psTotal") + useShip,2)
			psCogs = formatNumber(moneyRS("cogs"),2)
		elseif moneyRS("store") = 10 then
			if useShip > 0 then useShip = useShip - (tmAvgSales * 3)
			tmTotal = formatNumber(moneyRS("tmTotal") + useShip,2)
			tmCogs = formatNumber(moneyRS("cogs"),2)
		end if
		moneyRS.movenext
		shipRS.movenext
	loop
	
	do while not ydaRS.EOF
		if ydaRS("store") = 0 then
			weYdaSales = formatNumber(ydaRS("cnt"),0)
		elseif ydaRS("store") = 1 then
			caYdaSales = formatNumber(ydaRS("cnt"),0)
		elseif ydaRS("store") = 2 then
			coYdaSales = formatNumber(ydaRS("cnt"),0)
		elseif ydaRS("store") = 3 then
			psYdaSales = formatNumber(ydaRS("cnt"),0)
		elseif ydaRS("store") = 10 then
			tmYdaSales = formatNumber(ydaRS("cnt"),0)
		end if
		ydaRS.movenext
	loop
	
	do while not ydaMoneyRS.EOF
		if incShip = 1 then useShip = prepInt(ydaShipRS("shippingTotal")) else useShip = 0
		if ydaMoneyRS("store") = 0 then
			if useShip > 0 then useShip = useShip - (weYdaSales * 3)
			weYdaTotal = formatNumber(ydaMoneyRS("weTotal") + useShip,2)
			weYdaCogs = formatNumber(ydaMoneyRS("cogs"),2)
		elseif ydaMoneyRS("store") = 1 then
			if useShip > 0 then useShip = useShip - (caYdaSales * 3)
			caYdaTotal = formatNumber(ydaMoneyRS("caTotal") + useShip,2)
			caYdaCogs = formatNumber(ydaMoneyRS("cogs"),2)
		elseif ydaMoneyRS("store") = 2 then
			if useShip > 0 then useShip = useShip - (coYdaSales * 3)
			coYdaTotal = formatNumber(ydaMoneyRS("coTotal") + useShip,2)
			coYdaCogs = formatNumber(ydaMoneyRS("cogs"),2)
		elseif ydaMoneyRS("store") = 3 then
			if useShip > 0 then useShip = useShip - (psYdaSales * 3)
			psYdaTotal = formatNumber(ydaMoneyRS("psTotal") + useShip,2)
			psYdaCogs = formatNumber(ydaMoneyRS("cogs"),2)
		elseif ydaMoneyRS("store") = 10 then
			if useShip > 0 then useShip = useShip - (tmYdaSales * 3)
			tmYdaTotal = formatNumber(ydaMoneyRS("tmTotal") + useShip,2)
			tmYdaCogs = formatNumber(ydaMoneyRS("cogs"),2)
		end if
		ydaMoneyRS.movenext
		ydaShipRS.movenext
	loop
	
	response.Write(prepInt(weAvgSales) & "##" & formatCurrency(weTotal-weCogs,2) & "##" & formatNumber(prepInt(weUnique),0) & "##" & formatNumber(prepInt(weTotalVisits),0) & "##")
	response.Write(prepInt(coAvgSales) & "##" & formatCurrency(coTotal-coCogs,2) & "##" & coUnique & "##" & coTotalVisits & "##")
	response.Write(prepInt(caAvgSales) & "##" & formatCurrency(caTotal-caCogs,2) & "##" & caUnique & "##" & caTotalVisits & "##")
	response.Write(prepInt(psAvgSales) & "##" & formatCurrency(psTotal-psCogs,2) & "##" & psUnique & "##" & psTotalVisits & "##")
	response.Write(prepInt(tmAvgSales) & "##" & formatCurrency(tmTotal-tmCogs,2) & "##" & tmUnique & "##" & tmTotalVisits & "##")
	response.Write((cdbl(weAvgSales)+cdbl(coAvgSales)+cdbl(caAvgSales)+cdbl(psAvgSales)+cdbl(tmAvgSales)) & "##" & formatCurrency(weTotal-weCogs+coTotal-coCogs+caTotal-caCogs+psTotal-psCogs+tmTotal-tmCogs,2) & "##")
	response.Write(weYdaSales & "##")
	response.Write(coYdaSales & "##")
	response.Write(caYdaSales & "##")
	response.Write(psYdaSales & "##")
	response.Write(tmYdaSales & "##")
	response.Write(formatCurrency(weYdaTotal-weYdaCogs,2) & "##")
	response.Write(formatCurrency(coYdaTotal-coYdaCogs,2) & "##")
	response.Write(formatCurrency(caYdaTotal-caYdaCogs,2) & "##")
	response.Write(formatCurrency(psYdaTotal-psYdaCogs,2) & "##")
	response.Write(formatCurrency(tmYdaTotal-tmYdaCogs,2) & "##")
	response.Write((cdbl(weYdaSales)+cdbl(coYdaSales)+cdbl(caYdaSales)+cdbl(psYdaSales)+cdbl(tmYdaSales)) & "##")
	response.Write(formatCurrency(weYdaTotal-weYdaCogs+coYdaTotal-coYdaCogs+caYdaTotal-caYdaCogs+psYdaTotal-psYdaCogs+tmYdaTotal-tmYdaCogs,2) & "##" & now)
	
	call CloseConn(oConn)
%>