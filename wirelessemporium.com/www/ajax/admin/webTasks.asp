<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	
	call getDBConn(oConn)
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	dim taskID : taskID = prepInt(request.QueryString("taskID"))
	dim shovelReady : shovelReady = prepInt(request.QueryString("shovelReady"))
	dim oldPosition : oldPosition = prepInt(request.QueryString("oldPosition"))
	dim newPosition : newPosition = prepInt(request.QueryString("newPosition"))
	
	if newPosition > 0 then
		newPosition = newPosition - 1
		if newPosition > oldPosition then
			sql = "update we_Website_Tasks set sortValue = sortValue - 1 where sortValue > " & oldPosition & " and sortValue <= " & newPosition
		else
			sql = "update we_Website_Tasks set sortValue = sortValue + 1 where sortValue >= " & newPosition & " and sortValue < " & oldPosition
		end if
		oConn.execute(sql)
		sql = "update we_Website_Tasks set sortValue = " & newPosition & " where id = " & taskID
		msg = "Position changed! (" & taskID & "," & newPosition & ")"
	else
		if shovelReady = 1 then
			sql = "update we_Website_Tasks set shovelReady = " & shovelReady & ", sortValue = (select max(sortValue) + 1 from we_Website_Tasks where status <> 10 and deleted = 0 and shovelReady = 1) where id = " & taskID
			msg = "task set shovelReady"
		else
			sql = "update we_Website_Tasks set shovelReady = " & shovelReady & ", sortValue = null where id = " & taskID
			msg = "task removed from shovelReady"
		end if
	end if
	oConn.execute(sql)
	
	response.Write("sql:" & sql & "<br>msg:" & msg)
	
	call CloseConn(oConn)
%>