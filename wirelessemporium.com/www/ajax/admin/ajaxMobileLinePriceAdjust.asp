<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%

	call getDBConn(oConn)

	dim newPrice : newPrice = prepInt(request.QueryString("newPrice"))
	dim partNumber : partNumber = prepStr(request.QueryString("partNumber"))
	dim basePrice : basePrice = prepInt(request.QueryString("basePrice"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim catID : catID = prepInt(request.QueryString("catID"))
	dim itemID : itemID = prepInt(request.QueryString("itemID"))
	dim searchPartNumber : searchPartNumber = prepStr(request.QueryString("searchPartNumber"))
	dim category : category = prepInt(request.QueryString("category"))
	dim keyword : keyword = prepStr(request.QueryString("keyword"))
	
	if partNumber <> "" or basePrice > 0 then
		if partNumber <> "" then
			sql = "update we_items set price_our = '" & newPrice & "' where partNumber = '" & partNumber & "' and hideLive = 0 and ghost = 0"
		elseif basePrice > 0 then
			sql = "update we_items set price_our = '" & newPrice & "' where partNumber like '%-MLD-%' and price_our = '" & basePrice & "' and hideLive = 0 and ghost = 0"
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
		
		response.Write("update complete!")
	elseif (modelID > 0 and catID > 0) or itemID > 0 or searchPartNumber <> "" or category > 0 or keyword <> "" then
		if itemID > 0 then
			sql = "select price_our, itemID, itemDesc, price_retail, cogs, partNumber, inv_qty from we_items where itemID = " & itemID & " and partNumber like '%-MLD-%' and master = 1 and hideLive = 0 and ghost = 0 order by price_our"
		elseif searchPartNumber <> "" then
			sql = "select price_our, itemID, itemDesc, price_retail, cogs, partNumber, inv_qty from we_items where partNumber like '%" & searchPartNumber & "%' and partNumber like '%-MLD-%' and master = 1 and hideLive = 0 and ghost = 0 order by price_our"
		elseif category > 0 then
			sql = "select price_our, itemID, itemDesc, price_retail, cogs, partNumber, inv_qty from we_items where typeID = " & category & " and partNumber like '%-MLD-%' and master = 1 and hideLive = 0 and ghost = 0 order by price_our"
		elseif keyword <> "" then
			sql = "select price_our, itemID, itemDesc, price_retail, cogs, partNumber, inv_qty from we_items where itemDesc like '%" & keyword & "%' and partNumber like '%-MLD-%' and master = 1 and hideLive = 0 and ghost = 0 order by price_our"
		else
			sql = "select price_our, itemID, itemDesc, price_retail, cogs, partNumber, inv_qty from we_items where modelID = " & modelID & " and typeID = " & catID & " and partNumber like '%-MLD-%' and master = 1 and hideLive = 0 and ghost = 0 order by price_our"
		end if
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
%>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<%
    priceBracket = 0
    productNum = 0
	if rs.EOF then
	%>
    <tr><td align="center" style="font-weight:bold; font-size:18px; color:#F00;">No Products Found...<br />Please adjust your search criteria</td></tr>
    <%
	else
	    do while not rs.EOF
    	    priceBracket = priceBracket + 1
        	priceOur = rs("price_our")
    %>
    <tr bgcolor="#333333" style="color:#FFF; font-weight:bold;">
        <td nowrap="nowrap">
            <form name="bracketForm_<%=priceBracket%>" onsubmit="return(false)" style="margin:0px; padding:0px;">
            <div style="display:block; width:100%">
                <div style="float:left;">Mobile Line Products That Cost <%=priceOur%> <a onclick="showBracket(<%=priceBracket%>)" style="color:#ccccff; font-size:16px; cursor:pointer;">(view <span id="priceBracket_<%=priceBracket%>"></span> products)</a></div>
                <div style="float:right;">
                    <input type="button" name="myAction" value="Update Bracket" onclick="updateBracketPrice(document.bracketForm_<%=priceBracket%>.newPrice.value,document.bracketForm_<%=priceBracket%>.basePrice.value)" />
                    <input type="hidden" name="basePrice" value="<%=priceOur%>" />
                </div>
                <div style="float:right; padding:1px 10px 0px 0px;"><input type="text" name="newPrice" value="<%=priceOur%>" size="5" /></div>
            </div>
            </form>
        </td>
    </tr>
    <tr>
        <td id="productList_<%=priceBracket%>" style="display:none;">
            <div style="display:block; background-color:#006; height:30px; color:#FFF">
                <div style="float:left; width:340px; padding:5px 0px 0px 5px;">Product Name</div>
                <div style="float:left; width:50px; text-align:left; padding:3px 0px 0px 10px;">COGS</div>
                <div style="float:left; width:60px; text-align:left; padding:3px 0px 0px 3px;">MSRP</div>
                <div style="float:left; width:70px; text-align:left; padding:3px 0px 0px 3px;">Price</div>
                <div style="float:left; width:140px; text-align:left; padding-top:3px;">&nbsp;</div>
            </div>
            <%
            bgColor = "#fff"
            prodAtPrice = 0
            do while priceOur = rs("price_our")
                productNum = productNum + 1
                prodAtPrice = prodAtPrice + 1
                itemID = rs("itemID")
                itemDesc = rs("itemDesc")
                price_retail = rs("price_retail")
                Cogs = rs("cogs")
                partNumber = rs("partNumber")
            %>
            <form name="productForm_<%=productNum%>" onsubmit="return(false)" style="margin:0px; padding:0px;">
            <div style="display:block; background-color:<%=bgColor%>; height:30px;">
                <div style="float:left; font-size:10px; width:340px; padding:5px 0px 0px 5px;"><a href="/product.asp?itemID=<%=itemID%>" target="viewProduct"><%=left(itemDesc,50)%></a></div>
                <div style="float:left; width:50px; text-align:right; padding-top:3px;"><%=formatCurrency(Cogs,2)%></div>
                <div style="float:left; width:60px; text-align:right; padding-top:3px;"><%=formatCurrency(price_retail,2)%></div>
                <div style="float:left; width:70px; text-align:right; padding-top:3px;"><input type="text" name="newPrice" value="<%=priceOur%>" size="5" /></div>
                <div style="float:left; width:150px; text-align:right; padding-top:3px;"><input type="button" name="myAction" value="Update Product" onclick="updateIndvPrice(document.productForm_<%=productNum%>.newPrice.value,'<%=partNumber%>')" /></div>
            </div>
            </form>
            <%
                rs.movenext
                if bgColor = "#fff" then bgColor = "#ccc" else bgColor = "#fff"
                if rs.EOF then exit do
            loop
            %>
            <div id="prodCnt_<%=priceBracket%>" style="display:none;"><%=prodAtPrice%></div>
        </td>
    </tr>
    <%
    	loop
	end if
    %>
</table>
<%
	end if
			
	call CloseConn(oConn)
%>