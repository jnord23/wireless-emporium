<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	
	call getDBConn(oConn)
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
	if prepInt(session("adminID")) = 0 then response.Write("<span style='#fff;'>Action Cancelled<br />Please login to admin again</span>")
	adminID = prepInt(session("adminID"))
	
	function sendNewProdEmail(itemID)
		sql =	"select a.itemDesc, a.itemDesc_CO, a.partNumber, a.price_our, a.price_CO, b.typeID, b.inv_qty, a.itemPic, a.itemPic_CO, c.brandName, d.modelName, e.orders, f.id, b.partnumber " &_
				"from we_Items a " &_
					"join we_Items b on a.partnumber = b.partnumber and b.master = 1 " &_
					"join we_brands c on a.brandID = c.brandID " &_
					"join we_models d on a.modelID = d.modelID " &_
					"outer apply (" &_
						"select count(*) as orders from we_invRecord where itemID = b.itemID and notes like '%Customer Order%'" &_
					") e " &_
					"left join newProducts f on a.partnumber = f.partnumber " &_
				"where a.itemID = " & itemID
		set pnRS = oConn.execute(sql)
		
		if not pnRS.EOF then
			useDesc = pnRS("itemDesc")
			useDescCO = pnRS("itemDesc_CO")
			usePN = pnRS("partNumber")
			usePriceWE = pnRS("price_our")
			usePriceCO = pnRS("price_CO")
			if prepInt(pnRS("inv_qty")) > 0 then
				if prepStr(pnRS("itemPic")) <> "" or prepStr(pnRS("itemPic_CO")) <> "" then
					if prepInt(pnRS("orders")) = 0 then
						if prepInt(pnRS("id")) = 0 then
							brandName = prepStr(pnRS("brandName"))
							modelName = prepStr(pnRS("modelName"))
							'send email to marketing team
							cdo_from = "Purchasing <wepurchasing@wirelessemporium.com>"
							cdo_subject = brandName & " " & modelName & " phone is ready for a marketing campaign to be created!"
							cdo_body =	"Phone: " & brandName & " " & modelName & "<br>" &_
										"Product: " & useDesc & "<br>" &_
										"PartNumber: " & usePN & "<br>" &_
										"WE Price: " & usePriceWE & "<br>" &_
										"CO Price: " & usePriceCO & "<br>" &_
										"WE Link: <a href='http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(useDesc) & "'>ProductLink</a><br>" &_
										"CO Link: <a href='http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(useDescCO) & ".html'>ProductLink</a><br>"
							cdo_to = "wemarketing@wirelessemporium.com;wepurchasing@wirelessemporium.com"
							CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
							
							sql = "insert into newProducts (partnumber) values('" & pnRS("partnumber") & "')"
							oConn.execute(sql)
							
							response.Write("Marketing Msg: message sent to marketing!<br>")
						else
							response.Write("Marketing Msg: Already sent new product message<br>")
						end if
					else
						response.Write("Marketing Msg: This product has been sold already<br>")
					end if
				else
					response.Write("Marketing Msg: No pictures found<br>")
				end if
			else
				response.Write("Marketing Msg: No inventory<br>")
			end if
		else
			response.Write("Marketing Msg: pnRS is blank:" & sql & "<br>")
		end if
	end function
	
	dim sortBy : sortBy = prepStr(request.QueryString("sortBy"))
	dim saveRcvAmt : saveRcvAmt = prepInt(request.QueryString("saveRcvAmt"))
	dim ourPN : ourPN = prepStr(request.QueryString("ourPN"))
	dim orderID : orderID = prepStr(request.QueryString("orderID"))
	dim rcvAmt : rcvAmt = prepInt(request.QueryString("rcvAmt"))
	dim adjustOrder : adjustOrder = prepInt(request.QueryString("adjustOrder"))
	dim saveDateColumn : saveDateColumn = prepStr(request.QueryString("saveDateColumn"))
	dim vendorCode
	dim prioOrderID : prioOrderID = prepStr(request.QueryString("prioOrderID"))
	dim priority : priority = prepStr(request.QueryString("priority"))
	dim voNotes : voNotes = prepStr(request.QueryString("voNotes"))
	dim saveNotes : saveNotes = prepStr(request.QueryString("saveNotes"))
	
	if priority <> "" then
		sql = "update we_vendorOrder set priority = '" & priority & "' where orderID = '" & prioOrderID & "'"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		response.Write("saved record")
		call CloseConn(oConn)
		response.End()
	end if
	
	if voNotes <> "" then
		if saveNotes <> "" then
			sql = "update we_vendorOrder set notes = '" & saveNotes & "' where orderID = '" & voNotes & "'"
			session("errorSQL") = sql
			oConn.execute(sql)
			
			response.Write("save complete")
		else
			sql = "select notes from we_vendorOrder where orderID = '" & voNotes & "'"
			session("errorSQL") = sql
			set rs = oConn.execute(sql)
			
			if not rs.EOF then
				%>
				<div style="width:400px; margin:0px auto; text-align:center;"><textarea cols="32" rows="5" onchange="ajax('/ajax/admin/ajaxVendorReceiveOrders.asp?voNotes=<%=voNotes%>&saveNotes=' + this.value,'dumpZone')"><%=rs("notes")%></textarea></div>
				<div style="width:400px; margin:0px auto; text-align:center; padding-top:10px;"><input type="button" name="myAction" value="Save Notes" onclick="voNotes('<%=voNotes%>')" /></div>
				<%
			end if
		end if
		call CloseConn(oConn)
		response.End()
	end if
	
	if orderID <> "" then
		sql = "select vendorCode from we_vendorOrder where orderID = '" & orderID & "'"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		if not rs.EOF then vendorCode = rs("vendorCode")
	end if
	
	if saveDateColumn <> "" then
		sql = "update we_vendorOrder set " & saveDateColumn & " = '" & now & "' where orderID = '" & orderID & "'"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		response.Write("saved data")
		call CloseConn(oConn)
		response.End()
	end if
	
	if saveRcvAmt = 1 then
		if rcvAmt < 0 then rcvAmt = 0
		
		if adjustOrder = 1 then
			sql = "select receivedAmt from we_vendorOrder where orderID = '" & orderID & "' and partNumber = '" & ourPN & "'"
			session("errorSQL") = sql
			set adjustInfoRS = oConn.execute(sql)
			
			oldReceivedAmt = prepInt(adjustInfoRS("receivedAmt"))
			
			newRcvAmt = rcvAmt - oldReceivedAmt
		end if
		
		sql = "select rowProcessed, complete from we_vendorOrder where orderID = '" & orderID & "' and partNumber = '" & ourPN & "'"
		session("errorSQL") = sql
		set verifyRS = oConn.execute(sql)
		
		dim byPassUpdate : byPassUpdate = 0
		if verifyRS.EOF then
			byPassUpdate = 1
		elseif verifyRS("rowProcessed") then
			byPassUpdate = 1
		elseif verifyRS("complete") then
			byPassUpdate = 1
		end if
		
		if adjustOrder = 1 then byPassUpdate = 0
		
		if byPassUpdate = 0 then
			sql = "update we_vendorOrder set rowProcessed = 1, complete = 1, completeOrderDate = '" & now & "', receivedAmt = " & rcvAmt & " where orderID = '" & orderID & "' and partNumber = '" & ourPN & "'"
			session("errorSQL") = sql
			oConn.execute(sql)
			
			if rcvAmt > 0 or adjustOrder = 1 then
				sql = "select itemID, inv_qty, COGS from we_items where partNumber = '" & ourPN & "' and master = 1"
				session("errorSQL") = sql
				Set rs = oConn.execute(sql)
				
				itemID = prepInt(rs("itemID"))
				inv_qty = prepInt(rs("inv_qty"))
				
				oldCogs = prepInt(rs("COGS"))
				oldTotalCogs = oldCogs * inv_qty
				
				if adjustOrder = 1 then
					rcvAmt = newRcvAmt
					adjReason = "Adjust Vendor Order"
				else
					adjReason = "Receive Vendor Order"
				end if
				
				sql =	"select (((a.inv_qty * a.COGS) + (b.receivedAmt * b.cogs)) / ((a.inv_qty + b.receivedAmt))) as blendedCogs " &_
						"from we_Items a " &_
							"cross apply (select top 1 receivedAmt, cogs from we_vendorOrder where partNumber = a.PartNumber and complete = 1 and isnull(receivedAmt,0) > 0 order by completeOrderDate desc) b " &_
						"where PartNumber = '" & ourPN & "' and master = 1"
				session("errorSQL") = sql
				set rsRetCogs = oConn.execute(sql)
				
				rec_qty = rcvAmt
				avgCogs = round(prepInt(rsRetCogs("blendedCogs")),2)
				
				sql = "insert into we_invRecord (itemID,inv_qty,orderQty,adminID,notes,editDate) values(" & itemID & "," & inv_qty & "," & (rcvAmt*-1) & "," & prepInt(session("adminID")) & ",'" & adjReason & "','" & now & "')"
				session("errorSQL") = sql
				oConn.execute(sql)
					
				if inv_qty = 0 then
					sql = "update we_items set inv_qty = " & rcvAmt & ", COGS = " & avgCogs & ", hideLive = 0, ghost = 0 where partNumber = '" & ourPN & "' and master = 1"
					session("errorSQL") = sql
					oConn.execute(sql)
					
					sql = "insert into we_cogsHistory (vendor,partNumber,cogs,adminID) values('" & vendorCode & "','" & ourPN & "'," & avgCogs & "," & adminID & ")"
					session("errorSQL") = sql
					oConn.execute(sql)
					
					sql = "insert into we_adminActions (adminID,action) values (" & prepInt(adminID) & ",'vendor receive for: " & ourPN & "')"
					session("errorSQL") = sql
					oConn.execute(sql)
				elseif inv_qty > 0 then
					sql = "update we_items set inv_qty = " & inv_qty + rcvAmt & ", COGS = " & avgCogs & ", hideLive = 0, ghost = 0, invLastUpdatedBy = " & prepInt(session("adminID")) & ", invLastUpdatedDate = '" & now & "' where partNumber = '" & ourPN & "' and master = 1"
					session("errorSQL") = sql
					oConn.execute(sql)
					
					sql = "insert into we_cogsHistory (vendor,partNumber,cogs,adminID) values('" & vendorCode & "','" & ourPN & "'," & avgCogs & "," & adminID & ")"
					session("errorSQL") = sql
					oConn.execute(sql)
					
					sql = "insert into we_adminActions (adminID,action) values (" & prepInt(adminID) & ",'vendor receive for: " & ourPN & "')"
					session("errorSQL") = sql
					oConn.execute(sql)
					
					sendNewProdEmail(itemID)
				else
					weEmail "jon@wirelessemporium.com","warehouse@wirelessemporium.com","INV Update Error","Could not update partNumber:" & ourPN
				end if
			end if
			
			response.Write("<span style='color:#fff;'>Inv Adjusted</span>")
		else
			response.Write("<span style='color:#fff;'>Inv Adjusted<br>Blocked Double</span>")
		end if
		call CloseConn(oConn)
		response.End()
	end if
	
	if sortBy = "WEPN" then
		if session("curOrder") = "WEPN" then
			useOrderBy = "a.partNumber desc"
			session("curOrder") = "WEPN Desc"
		else
			useOrderBy = "a.partNumber"
			session("curOrder") = "WEPN"
		end if
	elseif sortBy = "DETAILS" then
		if session("curOrder") = "DETAILS" then
			useOrderBy = "c.itemDesc desc"
			session("curOrder") = "DETAILS Desc"
		else
			useOrderBy = "c.itemDesc"
			session("curOrder") = "DETAILS"
		end if
	elseif sortBy = "VPN" then
		if session("curOrder") = "VPN" then
			useOrderBy = "a.vendorPN desc"
			session("curOrder") = "VPN Desc"
		else
			useOrderBy = "a.vendorPN"
			session("curOrder") = "VPN"
		end if
	end if
	
	sql = replace(session("vendorSQL"),"order by a.partNumber","order by " & useOrderBy)
	session("errorSQL") = sql
	Set rs = oConn.execute(sql)
	
	vendorName = rs("vendor")
	orderID = rs("orderID")
%>
<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0">
    <tr><td colspan="8" align="center" style="font-size:34px; font-weight:bold;">Order For <%=vendorName%></td></tr>
    <tr style="font-weight:bold; color:#FFF; background-color:#333;">
        <td>Image</td>
        <td nowrap="nowrap" style="cursor:pointer;" onclick="sortBy('WEPN')">WE Partnumber</td>
        <td nowrap="nowrap" style="cursor:pointer;" onclick="sortBy('DETAILS')">We Description</td>
        <td nowrap="nowrap" style="cursor:pointer;" onclick="sortBy('VPN')">Vendor Partnumber</td>
        <td align="center">Inventory</td>
        <td align="center">Ordered</td>
        <td align="center">Received</td>
        <td nowrap="nowrap">Action</td>
    </tr>
    <%
    bgColor = "#ffffff"
    lap = 0
    do while not rs.EOF
        lap = lap + 1
        useSize = "11px"
        curBG = ""
        if rs("rowProcessed") then
            useReceived = rs("receivedAmt")
            curBG = bgColor
            bgColor = "#666666"
        else
            useReceived = rs("orderAmt")
        end if
    %>
    <form name="receiveForm_<%=lap%>" action="javascript:processRow(<%=lap%>)" method="post">
    <tr bgcolor="<%=bgColor%>" id="purchaseRow_<%=lap%>">
        <td><img src="/productPics/thumb/<%=rs("itemPic")%>" border="0" /></td>
        <td align="left" style="font-size:<%=useSize%>;"><%=rs("partNumber")%></td>
        <td align="left" style="font-size:<%=useSize%>;"><%=rs("itemDesc")%></td>
        <td align="left">
            <input type="text" name="vpn_<%=lap%>" value="<%=rs("vendorPN")%>" onchange="updateVendorPN('<%=rs("vendorCode")%>','<%=rs("partNumber")%>',this.value)" style="font-size:<%=useSize%>;" />
            <input type="hidden" name="partNumber_<%=lap%>" value="<%=rs("partNumber")%>" />
            <input type="hidden" name="orderAmt_<%=lap%>" value="<%=rs("orderAmt")%>" />
        </td>
        <td align="center"><%=rs("inv_qty")%></td>
        <td align="center" style="font-size:<%=useSize%>;"><%=rs("orderAmt")%></td>
        <% if rs("rowProcessed") then %>
        <td align="center" id="enterReceiveAmt_<%=lap%>" style="font-size:<%=useSize%>;">
            <%=useReceived%>
            <input type="hidden" name="receiveAmt_<%=lap%>" value="<%=useReceived%>" />
        </td>
        <td id="actionBttn_<%=lap%>"><span style="color:#FFF; font-weight:bold;">PROCESSED</span></td>
        <% else %>
        <td align="center" id="enterReceiveAmt_<%=lap%>" style="font-size:<%=useSize%>;"><input type="text" name="receiveAmt_<%=lap%>" value="<%=useReceived%>" size="3" style="font-size:<%=useSize%>;" /></td>
        <td id="actionBttn_<%=lap%>"><input type="submit" name="myAction" value="Received" /></td>
        <% end if %>
    </tr>
    </form>
    <%
        rs.movenext
        if curBG <> "" then bgColor = curBG
        if bgColor = "#ffffff" then bgColor = "#f0f0f0" else bgColor = "#ffffff"
    loop
    %>
</table>
<%
	call CloseConn(oConn)
%>