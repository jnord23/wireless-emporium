<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/includes/asp/inc_dbConn.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%
	dim site : site = prepStr(request.QueryString("site"))
	dim itemID : itemID = prepInt(request.QueryString("itemID"))
	dim curPrefix : curPrefix = prepStr(request.QueryString("curPrefix"))
	dim curVendor : curVendor = prepStr(request.QueryString("curVendor"))
	dim curBrandID : curBrandID = prepInt(request.QueryString("curBrandID"))
	dim curModelID : curModelID = prepInt(request.QueryString("curModelID"))
	
	dim prefixOptions : prefixOptions = prepInt(request.QueryString("prefixOptions"))
	dim vendorOptions : vendorOptions = prepInt(request.QueryString("vendorOptions"))
	dim brandOptions : brandOptions = prepInt(request.QueryString("brandOptions"))
	dim modelOptions : modelOptions = prepInt(request.QueryString("modelOptions"))
	
	dim prefixSelect : prefixSelect = session("prefixList")
	dim vendorSelect : vendorSelect = session("vendorList")
	dim brandSelect : brandSelect = session("brandList")
	dim modelSelect : modelSelect = session("modelList")
	
	dim prefix : prefix = prepStr(request.QueryString("prefix"))
	dim vendor : vendor = prepStr(request.QueryString("vendor"))
	dim brand : brand = prepInt(request.QueryString("brand"))
	dim model : model = prepInt(request.QueryString("model"))
	
	dim inv : inv = prepInt(request.QueryString("inv"))
	dim ven : ven = prepInt(request.QueryString("ven"))
	dim sortBy : sortBy = prepStr(request.QueryString("sortBy"))
	
	if prefixOptions = 1 then
		prefixArray = split(prefixSelect,",")
		%>
        <select name="prefixSelect" onchange="formSub('/admin/missingImages.asp?vendor=<%=curVendor%>&prefix=' + this.value + '&brand=<%=curBrandID%>&model=<%=curModelID%>')" style="width:150px;">
			<option value="">All Products</option>
			<%
            for i = 0 to ubound(prefixArray)
				if prepStr(prefixArray(i)) <> "" then
            %>
            <option value="<%=prefixArray(i)%>"<% if curPrefix = prefixArray(i) then %> selected="selected"<% end if %>><%=prefixArray(i)%></option>
            <%
            	end if
			next
            %>
        </select>
        <%
		call CloseConn(oConn)
		response.End()
	elseif vendorOptions = 1 then
		sql = "exec sp_vendorMissingImages " & curBrandID & "," & curModelID & ",'" & curPrefix & "'"
		session("errorSQL") = sql
		set vendorRS = oConn.execute(sql)
		%>
        <select name="prefixSelect" onchange="formSub('/admin/missingImages.asp?prefix=<%=curPrefix%>&vendor=' + this.value + '&brand=<%=curBrandID%>&model=<%=curModelID%>')" style="width:150px;">
			<option value="">All Vendors</option>
			<%
            do while not vendorRS.EOF
            %>
            <option value="<%=vendorRS("vendor")%>"<% if prepStr(curVendor) = prepStr(vendorRS("vendor")) then %> selected="selected"<% end if %>><%=vendorRS("vendor")%> (<%=vendorRS("itemCnt")%>)</option>
            <%
				vendorRS.movenext
			loop
            %>
        </select>
        <%
		call CloseConn(oConn)
		response.End()
	elseif brandOptions = 1 then
		if prepStr(brandSelect) = "" then brandSelect = 0
		sql = "select brandID, brandName from we_brands where brandID in (" & brandSelect & ") order by brandName"
		session("errorSQL") = sql
		set brandRS = oConn.execute(sql)
		%>
        <select name="brandSelect" onchange="formSub('/admin/missingImages.asp?prefix=<%=curPrefix%>&vendor=<%=curVendor%>&brand=' + this.value + '&model=0')" style="width:150px;">
			<option value="">All Brands</option>
            <optgroup label="Top Brands">
                <% if instr("," & session("brandList"),",9,") > 0 then %><option value="9">Samsung</option><% end if %>
                <% if instr("," & session("brandList"),",17,") > 0 then %><option value="17">Apple</option><% end if %>
                <% if instr("," & session("brandList"),",4,") > 0 then %><option value="4">LG</option><% end if %>
                <% if instr("," & session("brandList"),",30,") > 0 then %><option value="30">ZTE</option><% end if %>
                <% if instr("," & session("brandList"),",5,") > 0 then %><option value="5">Motorola</option><% end if %>
                <% if instr("," & session("brandList"),",7,") > 0 then %><option value="7">Nokia</option><% end if %>
            </optgroup>
            <optgroup label="Brands A-Z">
				<%
                do while not brandRS.EOF
                    useBrandID = brandRS("brandID")
                    useBrandName = brandRS("brandName")
					if instr("," & session("brandList"),"," & useBrandID & ",") > 0 then
                %>
                <option value="<%=useBrandID%>"<% if prepInt(curBrandID) = prepInt(useBrandID) then %> selected="selected"<% end if %>><%=useBrandName%></option>
                <%
                    end if
					brandRS.movenext
                loop
                %>
            </optgroup>
        </select>
        <%
		brandRS = null
		call CloseConn(oConn)
		response.End()
	elseif modelOptions = 1 then
		if prepStr(modelSelect) = "" then modelSelect = 0
		curBrandID = prepInt(curBrandID)
		if curBrandID = 0 then
			%>
            <select name="modelSelect" style="width:150px;">
                <option value="">All Models</option>
            </select>
            <%
		else
			sql	=	"exec sp_accessoryFinder_byBrand " & curBrandID
			set objRs = oConn.execute(sql)
			%>
			<select name="modelSelect" onchange="formSub('/admin/missingImages.asp?prefix=<%=curPrefix%>&vendor=<%=curVendor%>&brand=<%=curBrandID%>&model=' + this.value)" style="width:150px;">
				<option value="">All Models</option>
				<%
				do until objRs.eof
					if objRS("menuCat") <> modelCategory then
						if modelCategory <> "" then
							response.Write("</optgroup>")
						end if
						modelCategory = objRS("menuCat")
						response.Write("<optgroup label=""" & modelCategory & """>")
					end if
					if instr("," & session("modelList"),"," & objRs("modelid") & ",") > 0 then
				%>
					<option value="<%=objRs("modelid")%>"<% if curModelID = prepInt(objRs("modelid")) then %> selected="selected"<% end if %>><%=objRs("modelname")%></option>
				<%
					end if
					objRs.movenext
				loop
				%>
			</select>
        	<%
		end if
		brandRS = null
		call CloseConn(oConn)
		response.End()
	end if

call CloseConn(oConn)	
%>
<form action="/admin/missingImages.asp?prefix=<%=prefix%>&vendor=<%=vendor%>&brand=<%=brand%>&model=<%=model%>" method="post" style="padding:0px; margin:0px;" enctype="multipart/form-data">
<div style="border:3px groove #666; height:230px; width:400px; background-color:#FFF;">
    <div style="height:25px; background-color:#000; color:#FFF; font-weight:bold;">
    	<div style="float:left; padding-left:5px;">Image Upload - MP</div>
        <div style="float:right; padding-right:5px; cursor:pointer;" onclick="closeImgLoader(<%=itemID%>)">Close</div>
    </div>
    <div style="height:35px; padding-top:5px;">
        <div style="float:left; padding-left:5px; width:120px;">WE Primary Pic:</div>
        <div style="float:left; padding-left:10px;"><input type="file" name="wePrimPic" style="font-size:10px;" /></div>
    </div>
    <div style="height:35px;">
        <div style="float:left; padding-left:5px; width:120px;">Alt Pic 1:</div>
        <div style="float:left; padding-left:10px;"><input type="file" name="altPic1" style="font-size:10px;" /></div>
    </div>
    <div style="height:35px;">
        <div style="float:left; padding-left:5px; width:120px;">Alt Pic 2:</div>
        <div style="float:left; padding-left:10px;"><input type="file" name="altPic2" style="font-size:10px;" /></div>
    </div>
    <div style="height:35px;">
        <div style="float:left; padding-left:5px; width:120px;">Alt Pic 3:</div>
        <div style="float:left; padding-left:10px;"><input type="file" name="altPic3" style="font-size:10px;" /></div>
    </div>
    <div style="height:35px;">
        <div style="float:left; padding-left:5px; width:120px;">CO Primary Pic:</div>
        <div style="float:left; padding-left:10px;"><input type="file" name="coPrimPic" style="font-size:10px;" /></div>
    </div>
    <div style="height:20px;">
        <div style="float:right; padding-right:5px;">
        	<input type="submit" name="mySub" value="Upload Images" style="font-size:10px;" />
            <input type="hidden" name="itemID" value="<%=itemID%>" />
            <input type="hidden" name="inv" value="<%=inv%>" />
            <input type="hidden" name="ven" value="<%=ven%>" />
            <input type="hidden" name="sortBy" value="<%=sortBy%>" />
        </div>
    </div>
</div>
</form>