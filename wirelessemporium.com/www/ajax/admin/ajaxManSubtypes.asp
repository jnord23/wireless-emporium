<%
	response.buffer = true
	response.expires = -1

%>
<!--#include virtual="/Framework/Utility/Static.asp"-->
<%	
	call getDBConn(oConn)

	dim brandid, modelid
	brandid = prepInt(request.QueryString("brandid"))
	modelid = prepInt(request.QueryString("modelid"))
	tblid = prepInt(request.QueryString("tblID"))	
	formName = ""
'	if tblid = 2 then 
'		formName = "frmSubRelated"
'	elseif tblid = 3 then
		formName = "frmRelated"
'	end if
	
	if brandid <> "" then
		sql	=	"select	a.modelid, a.modelname, b.brandid, b.brandname, a.isTablet" & vbcrlf & _
				"from	we_models a join we_brands b" & vbcrlf & _
				"	on	a.brandid = b.brandid" & vbcrlf & _
				"where	a.modelname not like '%all model%'" & vbcrlf
		if brandid <> 0 then
			sql = sql & "	and	b.brandid = '" & brandid & "'" & vbcrlf
		end if				
		sql = sql & "order by 2" & vbcrlf

		set objRs = oConn.execute(sql)
		if not objRs.eof then
		%>
        <div style="padding:5px; border:1px solid #ccc; width:120px; float:left;">
        	<input value="" type="checkbox" name="chkMaster" onclick="setAllCheckbox(document.<%=formName%>.modelID, document.<%=formName%>.chkMaster.checked);setHeaderCheckbox(document.<%=formName%>);"/>Select ALL
		</div>
        <div style="padding:5px; border:1px solid #ccc; width:120px; margin-left:5px; float:left;">
        	<input value="" type="checkbox" name="chkMasterTablet" onclick="setTabletCheckbox(document.<%=formName%>)" />Tablets
		</div>
        <table border="0" cellspacing="0" cellpadding="0" width="980">
        	<tr>
        <%
			strTablets = ""
			lap = 0
			do until objRs.eof
				if objRs("isTablet") then
					if strTablets = "" then
						strTablets = objRs("modelid")
					else
						strTablets = strTablets & "," & objRs("modelid")
					end if				
				end if
				
				lap = lap + 1
				if brandid <> 0 then
					response.write "<td align=""left"" width=""25%"" style=""font-size:11px;""><input type=""checkbox"" id=""" & formName & "_modelID_" & objRs("modelid") & """ name=""modelID"" value=""" & objRs("modelid") & """ onclick=""onSetMasterCheckBox(document." & formName & ");"">" & objRs("modelName") & "</td>"
				else
					response.write "<td align=""left"" width=""25%"" style=""font-size:11px;""><input type=""checkbox"" id=""" & formName & "_modelID_" & objRs("modelid") & """ name=""modelID"" value=""" & objRs("modelid") & """ onclick=""onSetMasterCheckBox(document." & formName & ");"">" & objRs("brandname") & " " & objRs("modelName") & "</td>"
				end if					
				if lap > 3 then
					lap = 0
					response.write "</tr><tr>"
				end if
				objRs.movenext
			loop
			%>
            </tr>
		</table>
        <div id="<%=formName%>_id_tablets" style="display:none;"><%=strTablets%></div>
        <%
		end if
	end if
	
	call CloseConn(oConn)
%>