<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%

	call getDBConn(oConn)

	dim brandID : brandID = prepInt(ds(request.QueryString("brandID")))
	dim modelID : modelID = prepInt(ds(request.QueryString("modelID")))
	dim boxID : boxID = prepInt(ds(request.QueryString("boxID")))
	dim modelList : modelList = ""
	dim modelArray, modelName, modelImg, brandName
	
	if modelID > 0 then
		sql = "select modelImg, modelName from we_models where modelID = " & modelID
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
		
		modelList = session("modelList")
		modelArray = split(modelList,"^^")
		modelImg = rs("modelImg")
		modelName = rs("modelName")
%>
		<div style="text-align:center;"><img src="/productPics/models/<%=modelImg%>" border="0" /></div>
		<div style="text-align:center; height:50px;"><%=modelName%></div>
		<div style="text-align:center; height:50px;">
			<select name="topModel_<%=boxID%>" onchange="updateModel(<%=boxID%>,this.value)" style="font-size:10px;">
				<%
				for i = 0 to (ubound(modelArray) - 1)
					curModel = split(modelArray(i),"##")
				%>
				<option value="<%=curModel(0)%>"<% if curModel(1) = modelName then %> selected="selected"<% end if %>><%=curModel(1)%></option>
				<% next %>
			</select>
		</div>
<%
		call CloseConn(oConn)
		response.End()
	end if
	
	sql = "select modelImg, modelName, (select brandName from we_brands with (nolock) where brandID = " & brandID & ") as brandName from we_models where brandID = " & brandID & " and topModel = 1 order by modelName"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	sql = "select modelID, modelName from we_models where brandID = " & brandID & " order by modelName"
	session("errorSQL") = sql
	Set modelRS = Server.CreateObject("ADODB.Recordset")
	modelRS.open sql, oConn, 0, 1
	
	do while not modelRS.EOF
		modelList = modelList & modelRS("modelID") & "##" & modelRS("modelName") & "^^"
		modelRS.movenext
	loop
	session("modelList") = modelList
	modelArray = split(modelList,"^^")
%>
<form action="/admin/topProducts.asp" method="post" name="topProdForm">
<table border="0" cellpadding="3" cellpadding="0" width="550" style="font-size:12px;">
	<tr>
    	<%
		lap = 0
		do while not rs.EOF
			lap = lap + 1
			modelImg = rs("modelImg")
			modelName = rs("modelName")
			brandName = rs("brandName")
		%>
    	<td style="border:1px solid #000;" id="modelBox_<%=lap%>">
        	<div style="text-align:center;"><img src="/productPics/models/<%=modelImg%>" border="0" /></div>
            <div style="text-align:center; height:50px;"><%=modelName%></div>
            <div style="text-align:center; height:50px;">
            	<select name="topModel_<%=lap%>" onchange="updateModel(<%=lap%>,this.value)" style="font-size:10px;">
                	<%
					for i = 0 to (ubound(modelArray) - 1)
						curModel = split(modelArray(i),"##")
					%>
                    <option value="<%=curModel(0)%>"<% if curModel(1) = modelName then %> selected="selected"<% end if %>><%=curModel(1)%></option>
                    <% next %>
                </select>
            </div>
        </td>
        <%
			rs.movenext
			if lap = 4 or lap = 8 or lap = 12 then
				response.Write("</tr><tr>")
			end if
		loop
		rs = null
		%>
    </tr>
    <tr>
    	<td colspan="4" align="right">
        	<input type="submit" name="mySub" value="Save New Top Phones" />
            <input type="hidden" name="totalModels" value="<%=lap%>" />
            <input type="hidden" name="brandID" value="<%=brandID%>" />
            <input type="hidden" name="brandName" value="<%=brandName%>" />
        </td>
    </tr>
</table>
</form>
<%
	call CloseConn(oConn)
%>