<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%

	call getDBConn(oConn)

	dim brandID, modelID, catID, orderBy, itemID, useSql, partNumber, categoryID, keyword, showHistory, viewHideLive, viewGhost, repull
	dim setGhost, ghostDir, setHideLive, hideLiveDir
	
	brandID = prepInt(request.QueryString("brandID"))
	modelID = prepInt(request.QueryString("modelID"))
	catID = prepInt(request.QueryString("catID"))
	orderBy = prepStr(request.QueryString("orderBy"))
	itemID = prepInt(request.QueryString("itemID"))
	partNumber = prepStr(request.QueryString("partNumber"))
	categoryID = prepInt(request.QueryString("categoryID"))
	keyword = prepStr(request.QueryString("keyword"))
	updateItemID = prepInt(request.QueryString("updateItemID"))
	updateInvQty = prepInt(request.QueryString("updateInvQty"))
	curInvQty = prepInt(request.QueryString("curInvQty"))
	showHistory = prepInt(request.QueryString("showHistory"))
	viewHideLive = prepStr(request.QueryString("viewHideLive"))
	viewGhost = prepStr(request.QueryString("viewGhost"))
	repull = prepInt(request.QueryString("repull"))
	setGhost = prepStr(request.QueryString("setGhost"))
	ghostDir = prepStr(request.QueryString("ghostDir"))
	setHideLive = prepStr(request.QueryString("setHideLive"))
	hideLiveDir = prepStr(request.QueryString("hideLiveDir"))
	
	if isnull(request.QueryString("updateInvQty")) or len(request.QueryString("updateInvQty")) < 1 then updateInvQty = -1
	useSql = ""
	
	if viewHideLive = "true" then
		session("viewHideLive") = "true"
	elseif viewHideLive = "false" then
		session("viewHideLive") = ""
	end if
	if viewGhost = "true" then
		session("viewGhost") = "true"
	elseif viewGhost = "false" then
		session("viewGhost") = ""
	end if
	if isnull(session("viewHideLive")) then session("viewHideLive") = ""
	if isnull(session("viewGhost")) then session("viewGhost") = ""
	if repull = 1 then useSql = session("repullSQL")
	
	if updateItemID > 0 then
		sql = "insert into we_invRecord (itemID,inv_qty,adjustQty,adminID,notes,editDate) values(" & updateItemID & "," & curInvQty & "," & updateInvQty & "," & session("adminID") & ",'WE Employee Adjustment','" & now & "')"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "update we_items set inv_qty = " & updateInvQty & " where itemID = " & updateItemID
		session("errorSQL") = sql
		oConn.execute(sql)
		
		response.Write("<div style='color=#F00;'>Update Complete</div>")
		call CloseConn(oConn)
		response.End()
	end if
	
	if showHistory > 0 then
		sql = "select top 10 * from we_invRecord where itemID = " & showHistory & " order by id desc"
		session("errorSQL") = sql
		Set invHistoryRS = Server.CreateObject("ADODB.Recordset")
		invHistoryRS.open sql, oConn, 0, 1
		
		returnValues = ""
		do while not invHistoryRS.EOF
			adjustBy = 0 - cdbl(invHistoryRS("orderQty")) + cdbl(invHistoryRS("adjustQty"))
			useNotes = invHistoryRS("notes")
			if useNotes = "Cancel Order" and adjustBy < 0 then adjustBy = adjustBy * -1
			postQty = cdbl(invHistoryRS("inv_qty")) + adjustBy

			returnValues = returnValues & invHistoryRS("inv_qty") & "%" & adjustBy & "%" & postQty & "%" & invHistoryRS("orderID") & "%" & useNotes & "%" & invHistoryRS("entryDate") & "#"
			invHistoryRS.movenext
		loop
		returnValues = left(returnValues,len(returnValues)-1)
		returnValuesArray = split(returnValues,"#")
		%>
		<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="500">
        	<tr bgcolor="#333333" style="color:#FFF; font-weight:bold; font-size:11px;">
            	<td>PreQty</td>
                <td>Adjust</td>
                <td>PostQty</td>
                <td>OrderID</td>
                <td width="100%">Notes</td>
                <td>Date</td>
            </tr>
			<%
			bgColor = "#ffffff"
			curRow = ubound(returnValuesArray)
			for i = 0 to ubound(returnValuesArray)
				session("errorSQL") = "1:" & curRow & "(" & i & ")"
				curArray = split(returnValuesArray(curRow),"%")
				curRow = curRow - 1
			%>
            <tr style="font-size:10px;" bgcolor="<%=bgColor%>">
            	<td><%=curArray(0)%></td>
                <td><%=curArray(1)%></td>
                <td><%=curArray(2)%></td>
                <td><%=curArray(3)%></td>
                <td><%=curArray(4)%></td>
                <td nowrap="nowrap"><%=curArray(5)%></td>
            </tr>
            <%
				if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
			next
			%>
            <tr bgcolor="#333333"><td colspan="6" align="right"><a onclick="displayInvHistory(<%=showHistory%>)" style="color:#fff; cursor:pointer; text-decoration:underline; font-size:11px">Close History</a></td>
		</table>
		<%
		call CloseConn(oConn)
		response.End()
	elseif setGhost <> "" then
		if ghostDir = "true" then ghostDir = 1 else ghostDir = 0
		sql = "update we_items set ghost = " & ghostDir & " where partNumber = '" & setGhost & "'"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		if ghostDir = 1 then
			response.Write("Part Number set as Ghost")
		else
			response.Write("Part Number no longer a Ghost")
		end if
		
		call CloseConn(oConn)
		response.End()
	elseif setHideLive <> "" then
		if hideLiveDir = "true" then hideLiveDir = 1 else hideLiveDir = 0
		sql = "update we_items set hideLive = " & hideLiveDir & " where partNumber = '" & setHideLive & "'"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		if hideLiveDir = 1 then
			response.Write("Part Number set as HideLive")
		else
			response.Write("Part Number no longer set as HideLive")
		end if
		
		call CloseConn(oConn)
		response.End()
	end if
	
	if itemID > 0 then
		useSql = "select a.vendor as flatVendor, a.cogs, a.price_our, a.price_co, a.price_ca, a.price_ps, a.price_er, a.itemPic, a.itemID, a.itemDesc, a.hideLive, a.ghost, (select top 1 ib.fname + ' ' + ib.lname + '<br />' + cast(ia.editDate as varchar(50)) from we_invRecord ia left join we_adminUsers ib on ib.adminID = ia.adminID where ia.itemID = a.itemID order by ia.entryDate desc) as lastEdit, (select top 1 notes from we_invRecord where itemID = a.itemID order by entryDate desc) as notes, (select itemID from we_items where partNumber = a.partNumber and master = 1) as masterID, a.partNumber, b.vendor, b.partNumber as vendorNumber from we_items a left join we_vendorNumbers b on a.partNumber = b.we_partNumber where #HLvar##GHvar#a.inv_qty = 0 and a.itemID = " & itemID & " and a.master = 1 order by a.itemID"
	elseif partNumber <> "" then
		partNumber = replace(partNumber,"%","")
		useSql = "select a.vendor as flatVendor, a.cogs, a.price_our, a.price_co, a.price_ca, a.price_ps, a.price_er, a.itemPic, a.itemID, a.itemDesc, a.hideLive, a.ghost, (select top 1 ib.fname + ' ' + ib.lname + '<br />' + cast(ia.editDate as varchar(50)) from we_invRecord ia left join we_adminUsers ib on ib.adminID = ia.adminID where ia.itemID = a.itemID order by ia.entryDate desc) as lastEdit, (select top 1 notes from we_invRecord where itemID = a.itemID order by entryDate desc) as notes, a.partNumber, b.vendor, b.partNumber as vendorNumber from we_items a left join we_vendorNumbers b on a.partNumber = b.we_partNumber where #HLvar##GHvar#a.inv_qty = 0 and a.partNumber like '%" & partNumber & "%' and a.master = 1 order by a.itemID"
	elseif categoryID > 0 then
		useSql = "select a.vendor as flatVendor, a.cogs, a.price_our, a.price_co, a.price_ca, a.price_ps, a.price_er, a.itemPic, a.itemID, a.itemDesc, a.hideLive, a.ghost, (select top 1 ib.fname + ' ' + ib.lname + '<br />' + cast(ia.editDate as varchar(50)) from we_invRecord ia left join we_adminUsers ib on ib.adminID = ia.adminID where ia.itemID = a.itemID order by ia.entryDate desc) as lastEdit, (select top 1 notes from we_invRecord where itemID = a.itemID order by entryDate desc) as notes, a.partNumber, b.vendor, b.partNumber as vendorNumber from we_items a left join we_vendorNumbers b on a.partNumber = b.we_partNumber where #HLvar##GHvar#a.inv_qty = 0 and a.typeID = " & categoryID & " and a.master = 1 order by a.itemID"
	elseif keyword <> "" then
		keyword = replace(keyword,"%","")
		useSql = "select a.vendor as flatVendor, a.cogs, a.price_our, a.price_co, a.price_ca, a.price_ps, a.price_er, a.itemPic, a.itemID, a.itemDesc, a.hideLive, a.ghost, (select top 1 ib.fname + ' ' + ib.lname + '<br />' + cast(ia.editDate as varchar(50)) from we_invRecord ia left join we_adminUsers ib on ib.adminID = ia.adminID where ia.itemID = a.itemID order by ia.entryDate desc) as lastEdit, (select top 1 notes from we_invRecord where itemID = a.itemID order by entryDate desc) as notes, a.partNumber, b.vendor, b.partNumber as vendorNumber from we_items a left join we_vendorNumbers b on a.partNumber = b.we_partNumber where #HLvar##GHvar#a.inv_qty = 0 and a.itemDesc like '%" & keyword & "%' and a.master = 1 order by a.itemID"
	elseif catID > 0 then
		if isnull(session("orderBy")) then
			session("orderBy") = orderBy
		else
			if orderBy = session("orderBy") then
				if instr(session("orderBy"),"desc") > 0 then
					session("orderBy") = orderBy
				else
					session("orderBy") = orderBy & " desc"
				end if
			else
				session("orderBy") = orderBy
			end if
		end if
		useSql = "select a.vendor as flatVendor, a.cogs, a.price_our, a.price_co, a.price_ca, a.price_ps, a.price_er, a.itemPic, a.itemID, a.itemDesc, a.hideLive, a.ghost, (select top 1 ib.fname + ' ' + ib.lname + '<br />' + cast(ia.editDate as varchar(50)) from we_invRecord ia left join we_adminUsers ib on ib.adminID = ia.adminID where ia.itemID = a.itemID order by ia.entryDate desc) as lastEdit, (select top 1 notes from we_invRecord where itemID = a.itemID order by entryDate desc) as notes, a.partNumber, b.vendor, b.partNumber as vendorNumber from we_items a left join we_vendorNumbers b on a.partNumber = b.we_partNumber where #HLvar##GHvar#inv_qty = 0 and master = 1 and typeID = " & catID & " and modelID = " & modelID & " order by " & session("orderBy")
	end if
	
	if useSql <> "" then
		if session("viewHideLive") = "true" then
			useSql = replace(useSql,"#HLvar#","hideLive = 1 and ")
			useSql = replace(useSql,"hideLive = 0 and ","hideLive = 1 and ")
		else
			if instr(useSql,"hideLive") > 0 then useSql = replace(useSql,"hideLive = 1 and ","hideLive = 0 and ")
		end if
		if session("viewGhost") = "true" then
			useSql = replace(useSql,"#GHvar#","ghost = 1 and ")
			useSql = replace(useSql,"ghost = 0 and ","ghost = 1 and ")
		else
			if instr(useSql,"ghost") > 0 then useSql = replace(useSql,"ghost = 1 and ","ghost = 0 and ")
		end if
		sql = useSql
		sql = replace(sql,"#HLvar#","hideLive = 0 and ")
		sql = replace(sql,"#GHvar#","ghost = 0 and ")
		session("repullSQL") = useSql
		session("errorSQL") = sql
		Set itemRS = oConn.execute(sql)
		
		if itemRS.EOF and itemID > 0 then
			saveSQL = sql
			sql = "select (select itemID from we_items where partNumber = a.partNumber and master = 1) as masterID, b.vendor, b.partNumber as vendorNumber from we_items a left join we_vendorNumbers b on a.partNumber = b.we_partNumber where a.itemID = " & itemID
			session("errorSQL") = sql
			Set itemRS = oConn.execute(sql)
			
			if not itemRS.EOF then
				sql = replace(saveSQL,itemID,itemRS("masterID"))
				session("repullSQL") = sql
				session("errorSQL") = sql
				Set itemRS = oConn.execute(sql)
			end if
		end if
%>
<div style="float:left; width:600px;">
	<% if itemRS.EOF then %>
    <div style="text-align:center; font-weight:bold; font-size:16px; color:#F00;">No Products Found</div>
    <% else %>
    <table border="0" cellpadding="3" cellspacing="0" width="100%">
		<tr bgcolor="#000000" style="font-weight:bold; color:#FFF; font-size:11px;">
        	<td style="border-right:1px solid #FFF; cursor:pointer;">Pic/ItemID</td>
            <td style="border-right:1px solid #FFF; cursor:pointer;" width="100%">Desc</td>
            <td style="border-right:1px solid #000; cursor:pointer;">Partnumber</td>
        </tr>
		<%
		bgColor = "#ffffff"
		prodLap = 0
		do while not itemRS.EOF
			prodLap = prodLap + 1
			dim flatVendor : flatVendor = prepStr(itemRS("flatVendor"))
			dim cogs : cogs = prepStr(itemRS("cogs"))
			dim wePrice : wePrice = prepStr(itemRS("price_our"))
			dim coPrice : coPrice = prepStr(itemRS("price_co"))
			dim caPrice : caPrice = prepStr(itemRS("price_ca"))
			dim psPrice : psPrice = prepStr(itemRS("price_ps"))
			dim erPrice : erPrice = prepStr(itemRS("price_er"))
			dim itemPic : itemPic = itemRS("itemPic")
			dim curItemID : curItemID = prepStr(itemRS("itemID"))
			dim itemDesc : itemDesc = prepStr(itemRS("itemDesc"))
			dim hideLive : hideLive = itemRS("hideLive")
			dim ghost : ghost = itemRS("ghost")
			dim useLastEdit : useLastEdit = prepStr(itemRS("lastEdit"))
			dim useNotes : useNotes = prepStr(itemRS("notes"))
			dim curPartNumber : curPartNumber = prepStr(itemRS("partNumber"))
			dim vendorData : vendorData = ""
			do while prepStr(itemRS("partNumber")) = curPartNumber
				if not isnull(itemRS("vendor")) then vendorData = vendorData & itemRS("vendor") & "(" & itemRS("vendorNumber") & ") "
				'response.Write("do while " & prepStr(itemRS("partNumber")) & " = " & curPartNumber & "<br>")
				itemRS.movenext
				if itemRS.EOF then exit do
			loop
			if vendorData = "" then
				if flatVendor = "" then vendorData = "No Vendor Data Available" else vendorData = flatVendor
			end if
						
			if isnumeric(cogs) then cogs = formatCurrency(cogs,2) else cogs = "NA"
			if isnumeric(wePrice) then wePrice = formatCurrency(wePrice,2) else wePrice = "NA"
			if isnumeric(coPrice) then coPrice = formatCurrency(coPrice,2) else coPrice = "NA"
			if isnumeric(caPrice) then caPrice = formatCurrency(caPrice,2) else caPrice = "NA"
			if isnumeric(psPrice) then psPrice = formatCurrency(psPrice,2) else psPrice = "NA"
			if isnumeric(erPrice) then erPrice = formatCurrency(erPrice,2) else erPrice = "NA"
			if not isnull(useLastEdit) then useLastEdit = useLastEdit else useLastEdit = "&nbsp;"
			if not isnull(useNotes) then useNotes = useNotes else useNotes = "NA"
		%>
    	<tr bgcolor="<%=bgColor%>" style="font-size:11px;">
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;" rowspan="7" valign="top">
				<div style="width:100px;"><img src="/productpics/thumb/<%=itemPic%>" /></div>
				<div style="width:100px; text-align:center;"><%=curItemID%></div>
            </td>
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;"><%=itemDesc%></td>
            <td style="border-bottom:1px solid #000;" nowrap="nowrap"><%=curPartNumber%></td>
        </tr>
        <tr bgcolor="#000000" style="font-weight:bold; color:#FFF; font-size:11px;">
        	<td colspan="2" style="border-right:1px solid #000; cursor:pointer;">Vendors</td>
        </tr>
        <tr>
        	<td colspan="2" align="left"><%=vendorData%></td>
        </tr>
        <tr bgcolor="#000000" style="font-weight:bold; color:#FFF; font-size:11px;">
        	<td style="border-right:1px solid #FFF; cursor:pointer;">Last Edit</td>
            <td style="border-right:1px solid #000; cursor:pointer;">Actions</td>
        </tr>
        <tr bgcolor="<%=bgColor%>" style="font-size:11px;">
        	<td style="border-right:1px solid #000; border-bottom:1px solid #000;"><%=useLastEdit%></td>
            <td style="border-bottom:1px solid #000;" valign="top" align="center" rowspan="3">
            	<div style="text-align:left; padding-left:10px;"><input type="checkbox"<% if hideLive then %> checked="checked"<% end if %> onclick="setHideLive('<%=curPartNumber%>',this.checked)" /> HideLive</div>
                <div style="text-align:left; padding-left:10px;"><input type="checkbox"<% if ghost then %> checked="checked"<% end if %> onclick="setGhost('<%=curPartNumber%>',this.checked)" /> Ghost</div>
            </td>
        </tr>
        <tr bgcolor="#000000" style="font-weight:bold; color:#FFF; font-size:11px;">
            <td style="border-right:1px solid #000; cursor:pointer;">Notes</td>
        </tr>
        <tr bgcolor="<%=bgColor%>" style="font-size:11px;">
        	<td style="border-right:1px solid #000; border-bottom:1px solid #000;">
				<%=useNotes%><% if useNotes <> "NA" then %> <a onclick="displayInvHistory(<%=curItemID%>)" style="cursor:pointer; color:#00F; text-decoration:underline;">(view details)</a><% end if %>
                <div id="invHistory_<%=curItemID%>" style="position:relative;"></div>
            </td>
        </tr>
	    <%
			if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
		loop
		%>
        <tr><td colspan="3" align="right"><%=prodLap%> Products Found</td></tr>
    </table>
    <% end if %>
</div>
<%
	elseif modelID > 0 then		
		sql = "select typeID, typeName from we_types order by typeName"
		session("errorSQL") = sql
		Set catRS = Server.CreateObject("ADODB.Recordset")
		catRS.open sql, oConn, 0, 1
%>
<div>
	<div style="float:left; width:140px;">Select Category:</div>
	<div style="float:left; padding-left:10px; width:450px;">
    	<select name="model" onchange="catSelect(this.value,<%=modelID%>)">
        	<option value="">Select All Categories</option>
	        <%
    	    do while not catRS.EOF
        	%>
	        <option value="<%=catRS("typeID")%>"<% if cdbl(catID) = cdbl(catRS("typeID")) then %> selected="selected"<% end if %>><%=catRS("typeName")%></option>
    	    <%
        	    catRS.movenext
	        loop
    	    %>
	    </select>
    </div>
</div>
<%
	else
		sql = "select modelID, modelName from we_models where brandID = " & brandID & " order by modelName"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
%>
<div style="width:600px;">
	<div style="float:left; width:140px;">Select Model:</div>
	<div style="float:left; padding-left:10px; width:450px;">
        <select name="model" onchange="modelSelect(this.value)">
        	<option value="">Select Model</option>
	        <%
    	    do while not rs.EOF
        	%>
	        <option value="<%=rs("modelID")%>"><%=rs("modelName")%></option>
    	    <%
        	    rs.movenext
	        loop
    	    %>
	    </select>
    </div>
</div>
<%
	end if
	
	call CloseConn(oConn)
%>