<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	call getDBConn(oConn)
	
	dim setName : setName = prepStr(request.QueryString("setName"))
	dim delatedItemID : deleteItemID = prepInt(request.QueryString("deleteItemID"))
	dim newSetName : newSetName = prepStr(request.QueryString("newSetName"))
	dim addItemID : addItemID = prepInt(request.QueryString("addItemID"))
	dim addRestriction : addRestriction = prepStr(request.QueryString("addRestriction"))
	dim useSetName : useSetName = prepStr(request.QueryString("useSetName"))
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	if useSetName <> "" then
		sql = "select a.itemID, b.itemDesc, c.typeName, a.restrictions from we_defaultRelatedItems a left join we_items b on a.itemID = b.itemID left join we_types c on b.typeID = c.typeID where setName = '" & useSetName & "' order by a.restrictions, b.typeID"
		session("errorSQL") = sql
		set relatedRS = oConn.execute(sql)
		
		sql = "select distinct setName from we_defaultRelatedItems order by setName"
		session("errorSQL") = sql
		set relatedSetsRS = oConn.execute(sql)
%>
<div style="padding-top:10px; font-weight:bold; font-size:18px; border-bottom:1px solid #000; height:25px;">
    <div style="float:left;">Default Related Items</div>
    <div style="float:right;">
        <select name="relatedSetName" onchange="changeRelatedSet(this.value)">
            <% do while not relatedSetsRS.EOF %>
            <option value="<%=relatedSetsRS("setName")%>"<% if useSetName = relatedSetsRS("setName") then %> selected="selected"<% end if %> ><%=relatedSetsRS("setName")%></option>
            <%
                relatedSetsRS.movenext
            loop
            %>
            <option value="Edit" onclick="window.location='/admin/relatedItemSets.asp'">Edit Related Item Sets</option>
        </select>
    </div>
</div>
<div style="height:200px; overflow:scroll; border:1px solid #000; width:100%;">
<%
do while not relatedRS.EOF
    if curType <> relatedRS("typeName") then
        curType = relatedRS("typeName")
        response.Write("<div style='font-size:12px; font-weight:bold; background-color:#666; color:#fff;'>" & relatedRS("typeName") & "</div>")
    end if
    response.Write(relatedRS("itemDesc") & "<br>")
    relatedRS.movenext
    if not isnull(relatedRS("restrictions")) then exit do
loop
%>
</div>
<div style="padding-top:20px; font-weight:bold; font-size:18px; border-bottom:1px solid #000;">Optional Related Items</div>
<div style="height:200px; overflow:scroll; border:1px solid #000;">
<%
dim resType : resType = ""
dim opCnt : opCnt = 0
do while not relatedRS.EOF
    opCnt = opCnt + 1
    if resType <> relatedRS("restrictions") then
        resType = relatedRS("restrictions")
        response.Write("<div style='font-size:12px; font-weight:bold; background-color:#666; color:#fff;'>" & relatedRS("restrictions") & "</div>")
    end if
    response.Write("<input name='relatedOptions_" & opCnt & "' type='checkbox' value='" & relatedRS("itemID") & "' />" & relatedRS("itemDesc") & " - " & relatedRS("typeName") & "<br>")
    relatedRS.movenext
loop
%>
</div>
<%
		call CloseConn(oConn)
		response.End()
	end if
	
	if (newSetName <> "" or addItemID > 0) and setName = "New" then
		sql = "insert into we_defaultRelatedItems (itemID,setName) values(2,'New')"
		response.Write("<span style='color:#fff;'>" & sql & "</span>")
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	if deleteItemID > 0 then
		sql = "delete from we_defaultRelatedItems where itemID = " & deleteItemID & " and setName = '" & setName & "'"
		response.Write("<span style='color:#fff;'>" & sql & "</span>")
		session("errorSQL") = sql
		oConn.execute(sql)
	elseif newSetName <> "" then
		sql = "update we_defaultRelatedItems set setName = '" & newSetName & "' where setName = '" & setName & "'"
		response.Write("<span style='color:#fff;'>" & sql & "</span>")
		session("errorSQL") = sql
		oConn.execute(sql)
		setName = newSetName
	elseif addItemID > 0 then
		if addRestriction = "" then
			sql = "insert into we_defaultRelatedItems (itemID,setName) values(" & addItemID & ",'" & setName & "')"
		else
			sql = "insert into we_defaultRelatedItems (itemID,restrictions,setName) values(" & addItemID & ",'" & addRestriction & "','" & setName & "')"
		end if
		response.Write("<span style='color:#fff;'>" & sql & "</span>")
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	sql = "select a.restrictions, a.itemID, b.itemDesc, c.typeName from we_defaultRelatedItems a left join we_items b on a.itemID = b.itemID left join we_types c on b.typeID = c.typeID where setName = '" & setName & "' order by a.restrictions, b.typeID"
	session("errorSQL") = sql
	set relatedRS = oConn.execute(sql)
%>
<form name="riForm" action="/admin/relatedItemSets.asp" method="post" onsubmit="return(false)">
<table border="0" cellpadding="3" cellpadding="0">
	<tr>
    	<td>
        	<div style="float:left; font-weight:bold;">Set Name:</div>
            <div style="float:left; padding-left:10px;"><input type="text" name="setName" value="<%=setName%>" /></div>
            <div style="float:left; padding-left:10px;"><input type="button" name="myAction" value="Update Set Name" onclick="ajax('/ajax/admin/ajaxRelatedItemSets.asp?setName=<%=setName%>&newSetName=' + document.riForm.setName.value,'result')" /></div>
        </td>
    </tr>
    <tr>
    	<td>
        	<div style="height:200px; overflow:scroll; border:1px solid #000; width:100%; width:600px;">
			<%
            dim curType : curType = ""
			dim bgColor : bgColor = "#fff"
            do while not relatedRS.EOF
                curItemID = relatedRS("itemID")
				curItemDesc = relatedRS("itemDesc")
				curTypeName = relatedRS("typeName")
				if curType <> curTypeName then
                    curType = curTypeName
                    response.Write("<div style='font-size:12px; font-weight:bold; background-color:#666; color:#fff;'>" & curTypeName & "</div>")
                end if
				%>
                <div style="background-color:<%=bgColor%>;"><%=curItemDesc%>&nbsp;<a onclick="ajax('/ajax/admin/ajaxRelatedItemSets.asp?setName=<%=setName%>&deleteItemID=<%=curItemID%>','result')" style="cursor:pointer; color:#F00;">Delete</a></div>
                <%
                relatedRS.movenext
                if not isnull(relatedRS("restrictions")) then exit do
				if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
            loop
            %>
            </div>
            <div style="padding-top:20px; font-weight:bold; font-size:18px; border-bottom:1px solid #000;">Optional Related Items</div>
            <div style="height:200px; overflow:scroll; border:1px solid #000;">
            <%
			bgColor = "#fff"
            do while not relatedRS.EOF
                opCnt = opCnt + 1
				curItemID = relatedRS("itemID")
				curItemDesc = relatedRS("itemDesc")
				curTypeName = relatedRS("typeName")
                if resType <> relatedRS("restrictions") then
                    resType = relatedRS("restrictions")
                    response.Write("<div style='font-size:12px; font-weight:bold; background-color:#666; color:#fff;'>" & relatedRS("restrictions") & "</div>")
                end if
                %>
                <div style="background-color:<%=bgColor%>;"><%=curItemDesc%>&nbsp;<a onclick="ajax('/ajax/admin/ajaxRelatedItemSets.asp?setName=<%=setName%>&deleteItemID=<%=curItemID%>','result')" style="cursor:pointer; color:#F00;">Delete</a></div>
                <%
                relatedRS.movenext
				if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
            loop
            %>
            </div>
        </td>
    </tr>
    <tr>
    	<td>
        	<div style="float:left; font-weight:bold;">Add ItemID:</div>
            <div style="float:left; padding-left:10px;"><input type="text" name="addItemID" value="" size="8" /></div>
            <div style="float:left; font-weight:bold; padding-left:10px;">Add Restriction:</div>
            <div style="float:left; padding-left:10px;"><input type="text" name="addRestriction" value="" size="20" /></div>
            <div style="float:left; padding-left:10px;"><input type="button" name="myAction" value="Add ItemID" onclick="ajax('/ajax/admin/ajaxRelatedItemSets.asp?setName=<%=setName%>&addItemID=' + document.riForm.addItemID.value + '&addRestriction=' + document.riForm.addRestriction.value,'result')" /></div>
        </td>
    </tr>
</table>
</form>
<%
	call CloseConn(oConn)
%>