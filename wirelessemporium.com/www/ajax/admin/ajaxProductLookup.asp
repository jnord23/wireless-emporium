<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%

	call getDBConn(oConn)

	dim brandID, modelID, catID, orderBy, itemID, useSql, partNumber, categoryID, keyword
	
	brandID = prepInt(request.QueryString("brandID"))
	modelID = prepInt(request.QueryString("modelID"))
	catID = prepInt(request.QueryString("catID"))
	orderBy = prepStr(request.QueryString("orderBy"))
	itemID = prepInt(request.QueryString("itemID"))
	partNumber = prepStr(request.QueryString("partNumber"))
	categoryID = prepInt(request.QueryString("categoryID"))
	keyword = prepStr(request.QueryString("keyword"))
	adminID = prepInt(session("adminID"))
	
	if isnull(orderBy) or len(orderBy) < 1 then orderBy = "itemID"
	useSql = ""
	
	if itemID > 0 then
		useSql = 	"select b.partnumber pNotes, c.bunker, a.* " &_
					"from we_items a " &_
						"left join we_partnumberNotes b on a.partnumber = b.partnumber " &_
						"left join we_ItemsExtendedData c on a.partnumber = c.partnumber " &_
					"where a.hideLive = 0 and a.itemID = " & itemID
	elseif partNumber <> "" then
		partNumber = replace(partNumber,"%","")
		useSql = 	"select b.partnumber pNotes, c.bunker, a.* " &_
					"from we_items a " &_
						"left join we_partnumberNotes b on a.partnumber = b.partnumber " &_
						"left join we_ItemsExtendedData c on a.partnumber = c.partnumber " &_
					"where a.hideLive = 0 and a.partNumber like '%" & partNumber & "%'"
	elseif categoryID > 0 then
		useSql = 	"select b.partnumber pNotes, c.bunker, a.* " &_
					"from we_items a " &_
						"left join we_partnumberNotes b on a.partnumber = b.partnumber " &_
						"left join we_ItemsExtendedData c on a.partnumber = c.partnumber " &_
					"where a.hideLive = 0 and a.typeID = " & categoryID
	elseif keyword <> "" then
		keyword = replace(keyword,"%","")
		useSql = 	"select b.partnumber pNotes, c.bunker, a.* " &_
					"from we_items a " &_
						"left join we_partnumberNotes b on a.partnumber = b.partnumber " &_
						"left join we_ItemsExtendedData c on a.partnumber = c.partnumber " &_
					"where a.hideLive = 0 and itemDesc like '%" & keyword & "%'"
	elseif catID > 0 then
		if isnull(session("orderBy")) then
			session("orderBy") = orderBy
		else
			if orderBy = session("orderBy") then
				if instr(session("orderBy"),"desc") > 0 then
					session("orderBy") = orderBy
				else
					session("orderBy") = orderBy & " desc"
				end if
			else
				session("orderBy") = orderBy
			end if
		end if
		useSql = 	"select b.partnumber pNotes, c.bunker, a.* " &_
					"from we_items a " &_
						"left join we_partnumberNotes b on a.partnumber = b.partnumber " &_
						"left join we_ItemsExtendedData c on a.partnumber = c.partnumber " &_
					"where a.typeID = " & catID & " and a.modelID = " & modelID & " and a.hideLive = 0 " &_
					"order by " & session("orderBy")
	end if
	
	if useSql <> "" then
		if instr(useSql,"order by") > 0 then
			useSql = replace(useSql,"order by","order by a.master desc,")
		else
			useSql = useSql & " order by a.master desc"
		end if
		sql = useSql
		session("errorSQL") = sql
		Set itemRS = Server.CreateObject("ADODB.Recordset")
		itemRS.open sql, oConn, 0, 1
%>
<div style="float:left; width:590px; z-index:1;">
	<table border="0" cellpadding="3" cellspacing="0">
		<tr bgcolor="#000000" style="font-weight:bold; color:#FFF; font-size:11px;">
        	<td style="border-right:1px solid #FFF; cursor:pointer;" onclick="resultOrder(<%=catID%>,<%=modelID%>,'itemID')">ItemID</td>
            <td style="border-right:1px solid #FFF; cursor:pointer;" onclick="resultOrder(<%=catID%>,<%=modelID%>,'itemDesc')">Desc</td>
            <td style="border-right:1px solid #FFF; cursor:pointer;" onclick="resultOrder(<%=catID%>,<%=modelID%>,'partNumber')">Partnumber</td>
            <td style="border-right:1px solid #FFF; cursor:pointer;" onclick="resultOrder(<%=catID%>,<%=modelID%>,'inv_qty')">Qty</td>
            <td style="border-right:1px solid #FFF; cursor:pointer;" onclick="resultOrder(<%=catID%>,<%=modelID%>,'itemWeight')">Wt</td>
            <td style="border-right:1px solid #FFF; cursor:pointer;" onclick="resultOrder(<%=catID%>,<%=modelID%>,'cogs')">COGS</td>
            <td style="border-right:1px solid #FFF; cursor:pointer;" onclick="resultOrder(<%=catID%>,<%=modelID%>,'price_our')">WE</td>
            <td style="border-right:1px solid #FFF; cursor:pointer;" onclick="resultOrder(<%=catID%>,<%=modelID%>,'price_co')">CO</td>
            <td style="border-right:1px solid #FFF; cursor:pointer;" onclick="resultOrder(<%=catID%>,<%=modelID%>,'price_ca')">CA</td>
            <td style="border-right:1px solid #FFF; cursor:pointer;" onclick="resultOrder(<%=catID%>,<%=modelID%>,'price_ps')">PS</td>
            <td style="border-right:1px solid #FFF; cursor:pointer;" onclick="resultOrder(<%=catID%>,<%=modelID%>,'price_er')">ER</td>
            <td>Hide</td>
        </tr>
		<%
		bgColor = "#ffffff"
		productLap = 0
		tooMany = 0
		do while not itemRS.EOF
			productLap = productLap + 1
			if isnumeric(itemRS("cogs")) then cogs = formatCurrency(itemRS("cogs"),2) else cogs = "NA"
			if isnumeric(itemRS("price_our")) then wePrice = formatCurrency(itemRS("price_our"),2) else wePrice = "NA"
			if isnumeric(itemRS("price_co")) then coPrice = formatCurrency(itemRS("price_co"),2) else coPrice = "NA"
			if isnumeric(itemRS("price_ca")) then caPrice = formatCurrency(itemRS("price_ca"),2) else caPrice = "NA"
			if isnumeric(itemRS("price_ps")) then psPrice = formatCurrency(itemRS("price_ps"),2) else psPrice = "NA"
			if isnumeric(itemRS("price_er")) then erPrice = formatCurrency(itemRS("price_er"),2) else erPrice = "NA"
			curPartNumber = itemRS("partNumber")
			bunker = itemRS("bunker")
			if isnull(bunker) then bunker = false
		%>
    	<tr bgcolor="<%=bgColor%>" style="font-size:11px;">
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;"><%=itemRS("itemID")%></td>
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;">
				<%=itemRS("itemDesc")%><br />
                <a onclick="displayInvHistory(<%=itemRS("itemID")%>)" style="cursor:pointer; color:#00F; text-decoration:underline;">(Inventory History)</a>
			</td>
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;" nowrap="nowrap">
				<div style="float:left; width:100%;"><%=curPartNumber%></div>
                <div style="float:left; width:50%;">
	                <a href="/admin/productNotes.asp?partNumber=<%=curPartNumber%>" target="_blank">
					<%
                    if not isnull(itemRS("pNotes")) then
                    %>
                    <img src="/images/Notes.gif" border="0" />
                    <% else %>
                    <img src="/images/Notes_none.gif" border="0" />
                    <% end if %>
                    </a>
                </div>
                <% if bunker then %>
	                <% if adminID = 19 or adminID = 54 or adminID = 55 or adminID = 63 then %>
    	            <div style="float:right;" title="Bunker Item"><img src="/images/icons/bunker2.png" border="0" height="25" /></div>
					<% else %>
            	    <div style="float:right;" title="Bunker Item"><img src="/images/icons/bunker1.png" border="0" height="25" /></div>
                	<% end if %>
                <% end if %>
            </td>
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;"><%=itemRS("inv_qty")%></td>
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;"><%=itemRS("itemWeight")%></td>
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;"><%=cogs%></td>
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;"><%=wePrice%></td>
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;"><%=coPrice%></td>
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;"><%=caPrice%></td>
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;"><%=psPrice%></td>
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;"><%=erPrice%></td>
            <td style="border-bottom:1px solid #000;"><%=itemRS("hideLive")%></td>
        </tr>
		<tr>
	        <td colspan="11" id="invHistory_<%=itemRS("itemID")%>" style="display:none;"></td>
        </tr>        
	    <%
			do while curPartNumber = itemRS("partNumber")
				itemRS.movenext
				if itemRS.EOF then exit do
			loop
			if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
			response.Flush()
			if productLap > 1000 then
				tooMany = 1
				exit do
			end if
		loop
		
		if tooMany = 1 then
		%>
        <tr><td colspan="12" align="center" style="font-weight:bold; color:#F00;">Too Many Results<br />Please Refine Your Criteria</td></tr>
        <%
		end if
		%>
    </table>
</div>
<%
	elseif modelID > 0 then		
		sql = "select typeID, typeName from we_types order by typeName"
		session("errorSQL") = sql
		Set catRS = Server.CreateObject("ADODB.Recordset")
		catRS.open sql, oConn, 0, 1
%>
<div>
	<div style="float:left; width:140px;">Select Category:</div>
	<div style="float:left; padding-left:10px; width:450px;">
    	<select name="model" onchange="catSelect(this.value,<%=modelID%>)">
        	<option value="">Select All Categories</option>
	        <%
    	    do while not catRS.EOF
        	%>
	        <option value="<%=catRS("typeID")%>"<% if cdbl(catID) = cdbl(catRS("typeID")) then %> selected="selected"<% end if %>><%=catRS("typeName")%></option>
    	    <%
        	    catRS.movenext
	        loop
    	    %>
	    </select>
    </div>
</div>
<%
	else
		sql = "select modelID, modelName from we_models where brandID = " & brandID & " order by modelName"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
%>
<div style="width:600px;">
	<div style="float:left; width:140px;">Select Model:</div>
	<div style="float:left; padding-left:10px; width:450px;">
        <select name="model" onchange="modelSelect(this.value)">
        	<option value="">Select Model</option>
	        <%
    	    do while not rs.EOF
        	%>
	        <option value="<%=rs("modelID")%>"><%=rs("modelName")%></option>
    	    <%
        	    rs.movenext
	        loop
    	    %>
	    </select>
    </div>
</div>
<%
	end if
	
	call CloseConn(oConn)
%>