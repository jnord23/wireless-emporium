<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%

	call getDBConn(oConn)

	dim modelType : modelType = prepStr(request.QueryString("modelType"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim modelName : modelName = prepStr(request.QueryString("modelName"))
	dim skipModel : skipModel = prepInt(request.QueryString("skipModel"))
	
	if modelID > 0 and modelType <> "" then
		if modelType = "prim" then
			sql = "update we_items_musicSkins set modelID = " & modelID & ", editDate = '" & now & "' where model = '" & modelName & "'"
		elseif modelType = "alt" then
			sql = "update we_items_musicSkins set altModelID = " & modelID & ", editDate = '" & now & "' where model = '" & modelName & "'"
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
		response.Write("Model Data Saved")
	elseif skipModel = 1 then
		sql = "update we_items_musicSkins set skip = 1, editDate = '" & now & "' where model = '" & modelName & "'"
		session("errorSQL") = sql
		oConn.execute(sql)
		response.Write("Model Has Been Skipped")
	else
		if modelType = "prim" then
			sql = "update we_items_musicSkins set modelID = null, editDate = '" & now & "' where model = '" & modelName & "'"
		elseif modelType = "alt" then
			sql = "update we_items_musicSkins set altModelID = null, editDate = '" & now & "' where model = '" & modelName & "'"
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
		response.Write("Item reset to null: " & sql)
	end if
	
	call CloseConn(oConn)
%>