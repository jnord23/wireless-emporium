<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	call getDBConn(oConn)

	dim setName : setName = prepStr(request.QueryString("setName"))
	dim delatedItemID : deleteItemID = prepInt(request.QueryString("deleteItemID"))
	dim newSetName : newSetName = prepStr(request.QueryString("newSetName"))
	dim addItemID : addItemID = prepInt(request.QueryString("addItemID"))
	dim addRestriction : addRestriction = prepStr(request.QueryString("addRestriction"))
	dim useSetName : useSetName = prepStr(request.QueryString("useSetName"))
	dim subtypeid : subtypeid = prepStr(request.QueryString("subtypeid"))
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%

	if useSetName <> "" then
		sql = 	"select	a.id, a.itemID, b.itemDesc, c.subTypeName, a.restrictions" & vbcrlf & _
				"from	we_defaultSubRelatedItems a join we_items b" & vbcrlf & _
				"	on	a.itemID = b.itemID join we_subtypes c" & vbcrlf & _
				"	on	a.subtypeid = c.subtypeid" & vbcrlf & _
				"where	setName = '" & useSetName & "'" & vbcrlf & _
				"order by a.restrictions, c.subtypeid" & vbcrlf
		session("errorSQL") = sql
		set relatedRS = oConn.execute(sql)
		sql = "select distinct setName from we_defaultSubRelatedItems order by setName"
		session("errorSQL") = sql
		set relatedSetsRS = oConn.execute(sql)
%>
<div style="height:200px; overflow:scroll; border:1px solid #000; width:100%;">
<%
do while not relatedRS.EOF
    if curType <> relatedRS("subTypeName") then
        curType = relatedRS("subTypeName")
        response.Write("<div style='font-size:12px; font-weight:bold; background-color:#666; color:#fff;'>" & relatedRS("subTypeName") & "</div>")
    end if
    response.Write(relatedRS("itemDesc") & "<br>")
    relatedRS.movenext
    if not isnull(relatedRS("restrictions")) then exit do
loop
%>
</div>
<div style="padding-top:20px; font-weight:bold; font-size:18px; border-bottom:1px solid #000;">Optional Sub-Related Items</div>
<div style="height:200px; overflow:scroll; border:1px solid #000;">
<%
dim resType : resType = ""
do while not relatedRS.EOF
    if resType <> relatedRS("restrictions") then
        resType = relatedRS("restrictions")
        response.Write("<div style='font-size:12px; font-weight:bold; background-color:#666; color:#fff;'>" & relatedRS("restrictions") & "</div>")
    end if
    response.Write("<input name='relatedOptions' type='checkbox' value='" & relatedRS("id") & "' />" & relatedRS("itemDesc") & " - " & relatedRS("subTypeName") & "<br>")
    relatedRS.movenext
loop
%>
</div>
<%
		call CloseConn(oConn)
		response.End()
	end if
	
	if (newSetName <> "" or addItemID > 0) and setName = "New" then
		sql = "insert into we_defaultSubRelatedItems (itemID,subtypeid,setName) values(0,'" & subtypeid & "','New')"
		response.Write("<span style='color:#fff;'>" & sql & "</span>")
		session("errorSQL") = sql
'		oConn.execute(sql)
	end if
	
	if deleteItemID > 0 then
		sql = "delete from we_defaultSubRelatedItems where id = '" & deleteItemID & "'"
		response.Write("<span style='color:#fff;'>" & sql & "</span>")
		session("errorSQL") = sql
		oConn.execute(sql)
	elseif newSetName <> "" then
		sql = "update we_defaultSubRelatedItems set setName = '" & newSetName & "' where setName = '" & setName & "'"
		response.Write("<span style='color:#fff;'>" & sql & "</span>")
		session("errorSQL") = sql
		oConn.execute(sql)
		setName = newSetName
	elseif addItemID > 0 then
		if addRestriction = "" then
			sql = 	"if not exists (select itemid from we_defaultSubRelatedItems where itemid = '" & addItemID & "' and subtypeid = '" & subtypeid & "' and setName = '" & setName & "') " & vbcrlf & _
					"	insert into we_defaultSubRelatedItems (itemID,subtypeid,setName) values('" & addItemID & "','" & subtypeid & "','" & setName & "')"
		else
			sql = "insert into we_defaultSubRelatedItems (itemID,subtypeid,restrictions,setName) values('" & addItemID & "','" & subtypeid & "','" & addRestriction & "','" & setName & "')"
		end if
		response.Write("<span style='color:#fff;'>" & sql & "</span>")
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	sql = 	"select	a.id, a.itemID, b.itemDesc, c.subTypeName, a.restrictions" & vbcrlf & _
			"from	we_defaultSubRelatedItems a join we_items b" & vbcrlf & _
			"	on	a.itemID = b.itemID join we_subtypes c" & vbcrlf & _
			"	on	a.subtypeid = c.subtypeid" & vbcrlf & _
			"where	setName = '" & setName & "'" & vbcrlf & _
			"order by a.restrictions, c.subtypeid" & vbcrlf
	session("errorSQL") = sql
	set relatedRS = oConn.execute(sql)
%>
<form name="riForm" action="/admin/subRelatedItemSets.asp" method="post" onsubmit="return(false)">
<table border="0" cellpadding="3" cellpadding="0">
	<tr>
    	<td>
        	<div style="float:left; font-weight:bold;">Set Name:</div>
            <div style="float:left; padding-left:10px;"><input type="text" name="setName" value="<%=setName%>" /></div>
            <div style="float:left; padding-left:10px;"><input type="button" name="myAction" value="Update Set Name" onclick="ajax('/ajax/admin/ajaxSubRelatedItemSets.asp?setName=<%=setName%>&newSetName=' + document.riForm.setName.value,'result')" /></div>
        </td>
    </tr>
    <tr>
    	<td>
        	<div style="height:200px; overflow:scroll; border:1px solid #000; width:100%; width:600px;">
			<%
            dim curType : curType = ""
			dim bgColor : bgColor = "#fff"
            do while not relatedRS.EOF
                curRelatedID = relatedRS("id")
				curItemDesc = relatedRS("itemDesc")
				curSubTypeName = relatedRS("subTypeName")
				if curType <> curSubTypeName then
                    curType = curSubTypeName
                    response.Write("<div style='font-size:12px; font-weight:bold; background-color:#666; color:#fff;'>" & curSubTypeName & "</div>")
                end if
				%>
                <div style="background-color:<%=bgColor%>;"><%=curItemDesc%>&nbsp;<a onclick="ajax('/ajax/admin/ajaxSubRelatedItemSets.asp?setName=<%=setName%>&deleteItemID=<%=curRelatedID%>','result')" style="cursor:pointer; color:#F00;">Delete</a></div>
                <%
                relatedRS.movenext
                if not isnull(relatedRS("restrictions")) then exit do
				if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
            loop
            %>
            </div>
            <div style="padding-top:20px; font-weight:bold; font-size:18px; border-bottom:1px solid #000;">Optional Sub-Related Items</div>
            <div style="height:200px; overflow:scroll; border:1px solid #000;">
            <%
			bgColor = "#fff"
            do while not relatedRS.EOF
				curRelatedID = relatedRS("id")
				curItemDesc = relatedRS("itemDesc")
				curSubTypeName = relatedRS("subTypeName")
                if resType <> relatedRS("restrictions") then
                    resType = relatedRS("restrictions")
                    response.Write("<div style='font-size:12px; font-weight:bold; background-color:#666; color:#fff;'>" & relatedRS("restrictions") & "</div>")
                end if
                %>
                <div style="background-color:<%=bgColor%>;"><%=curItemDesc%> - <%=curSubTypeName%>&nbsp;<a onclick="ajax('/ajax/admin/ajaxSubRelatedItemSets.asp?setName=<%=setName%>&deleteItemID=<%=curRelatedID%>','result')" style="cursor:pointer; color:#F00;">Delete</a></div>
                <%
                relatedRS.movenext
				if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
            loop
            %>
            </div>
        </td>
    </tr>
    <tr>
    	<td>
        	<table border="0" cellspacing="0" cellpadding="0">
            	<tr>
                	<td>
                        <div style="float:left; font-weight:bold;">Add ItemID:</div>
                        <div style="float:left; padding-left:10px;"><input type="text" name="addItemID" value="" size="8" /></div>
                        <div style="float:left; font-weight:bold; padding-left:10px;">Add Restriction:</div>
                        <div style="float:left; padding-left:10px;"><input type="text" name="addRestriction" value="" size="20" /></div>
                    </td>
                </tr>
            	<tr>
                	<td style="padding-top:10px;">
                        <div style="float:left;">Select Subtype:</div>            
                        <div style="float:left; padding-left:10px;">
                            <%
                            sql = "select subtypeid, subtypename from v_subtypematrix order by orderTopNav, subTypeOrderNum"
                            session("errorSQL") = sql
                            set objSubTypes = oConn.execute(sql)
                            %>
                            <select name="subtypeid">
                            <%
                            do until objSubTypes.eof
                                %>
                                <option value="<%=objSubTypes("subtypeid")%>"><%=objSubTypes("subtypename")%></option>
                                <%
                                objSubTypes.movenext
                            loop
                            %>
                            </select>
                        </div>
                        <div style="float:left; padding-left:10px;"><input type="button" name="myAction" value="Add ItemID" onclick="ajax('/ajax/admin/ajaxSubRelatedItemSets.asp?subtypeid=' + document.riForm.subtypeid.value + '&setName=<%=setName%>&addItemID=' + document.riForm.addItemID.value + '&addRestriction=' + document.riForm.addRestriction.value,'result')" /></div>            
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</form>
<%
	call CloseConn(oConn)
%>