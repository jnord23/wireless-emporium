<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
	thisUser = Request.Cookies("admin")("username")
	adminID = Request.Cookies("adminID")	

%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%

	call getDBConn(oConn)

	dim itemID : itemID = prepInt(request.querystring("itemid"))
	dim orig_inv_qty : orig_inv_qty = prepInt(request.querystring("orig_inv_qty"))
	dim partnumber : partnumber = prepStr(request.querystring("partnumber"))
	dim prev_partnumber : prev_partnumber = prepStr(request.querystring("prev_partnumber"))
	dim inv_qty : inv_qty = prepInt(request.querystring("inv_qty"))
	dim itemweight : itemweight = prepStr(request.querystring("itemweight"))
	dim cogs : cogs = prepStr(request.querystring("cogs"))
	dim vendor : vendor = prepStr(request.querystring("vendor"))
	dim seasonal : seasonal = prepInt(request.querystring("seasonal"))
	dim ghost : ghost = prepInt(request.querystring("ghost"))
	dim hidelive : hidelive = prepInt(request.querystring("hidelive"))
	dim nodiscount : nodiscount = prepInt(request.querystring("nodiscount"))
	dim price_our : price_our = prepStr(request.querystring("price_our"))
	dim price_ca : price_ca = prepStr(request.querystring("price_ca"))
	dim price_co : price_co = prepStr(request.querystring("price_co"))
	dim price_ps : price_ps = prepStr(request.querystring("price_ps"))
	dim price_er : price_er = prepStr(request.querystring("price_er"))
	dim flag : flag = prepInt(request.querystring("flag"))
	
	if not isnumeric(itemweight) then itemweight = "null"
	if flag = 0	then flag = itemid + 1
	if isnull(price_our) or len(price_our) < 1 or not isnumeric(price_our) then price_our = "null"
	if isnull(price_ca) or len(price_ca) < 1 or not isnumeric(price_ca) then price_ca = "null"
	if isnull(price_co) or len(price_co) < 1 or not isnumeric(price_co) then price_co = "null"
	if isnull(price_ps) or len(price_ps) < 1 or not isnumeric(price_ps) then price_ps = "null"
	if isnull(price_er) or len(price_er) < 1 or not isnumeric(price_er) then price_er = "null"

	if orig_inv_qty <> inv_qty then
		if orig_inv_qty = 0 and inv_qty > 0 then
			SQL = "UPDATE we_items SET inv_qty = -1 WHERE PartNumber = '" & partnumber & "'"
			session("errorSQL") = SQL
			oConn.execute SQL
		elseif inv_qty = 0 then
			SQL = "UPDATE we_items SET inv_qty = 0 WHERE PartNumber = '" & partnumber & "'"
			session("errorSQL") = SQL
			oConn.execute SQL
		end if
		SQL = "UPDATE we_items SET inv_qty = '" & inv_qty & "' WHERE itemID = '" & itemid & "'"
		session("errorSQL") = SQL
		oConn.execute SQL
	end if
	
	if prev_partnumber = partnumber then
		SQL = "UPDATE we_items SET"
		SQL = SQL & " itemWeight='" & itemweight & "', "
		SQL = SQL & " COGS='" & cogs & "', "
		SQL = SQL & " Vendor='" & vendor & "', "
		SQL = SQL & " NoDiscount=" & nodiscount & ", "
		SQL = SQL & " price_our = " & price_our & ", "
		SQL = SQL & " price_ca = " & price_ca & ", "
		SQL = SQL & " price_co = " & price_co & ", "
		SQL = SQL & " price_ps = " & price_ps & ", "
		SQL = SQL & " price_er = " & price_er & " "
		SQL = SQL & " WHERE partnumber = '" & partnumber & "'"
		session("errorSQL") = SQL
		oConn.execute SQL
		
		sql = "select itemID, brandID, modelID, subTypeID from we_Items where partnumber = '" & partnumber & "'"
		session("errorSQL") = sql
		set updateRS = oConn.execute(sql)
		
		if not updateRS.EOF then
			useItemID = updateRS("itemID")
			brandID = updateRS("brandID")
			modelID = updateRS("modelID")
			subTypeID = updateRS("subTypeID")
			autoDeleteCompPages "updateItem",brandID & "##" & modelID & "##" & subtypeID & "##" & useItemID
		end if
	end if
	
	SQL = "UPDATE we_items SET"
	SQL = SQL & " Vendor='" & vendor & "', "
	SQL = SQL & " Seasonal=" & seasonal & ", "
	SQL = SQL & " ghost=" & ghost & ", "
	SQL = SQL & " hideLive=" & hidelive & ", "
	SQL = SQL & " NoDiscount=" & nodiscount & ", "
	SQL = SQL & " flag1 = '" & flag & "'"
	SQL = SQL & " WHERE itemID = '" & itemid & "'"
	session("errorSQL") = SQL
	oConn.execute SQL
	
	sql = "select brandID, modelID, subTypeID from we_Items where itemID = " & itemid
	session("errorSQL") = sql
	set updateRS = oConn.execute(sql)
	
	if not updateRS.EOF then
		brandID = updateRS("brandID")
		modelID = updateRS("modelID")
		subTypeID = updateRS("subTypeID")
		autoDeleteCompPages "updateItem",brandID & "##" & modelID & "##" & subtypeID & "##" & itemID
	end if
	
	response.write "UPDATED"

	call CloseConn(oConn)
%>