<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/Framework/Utility/Static.asp"-->
<!--#include virtual="/includes/admin/UPSxml_all.asp"-->
<%	

call getDBConn(oConn)
set fsThumb = CreateObject("Scripting.FileSystemObject")

siteid = prepInt(request.QueryString("siteid"))
orderid = prepInt(request.QueryString("orderid"))
cartOrderID = prepInt(request.QueryString("cartOrderID"))
email = prepStr(request.QueryString("email"))
uType = prepStr(request.QueryString("uType"))
sZip = prepStr(request.QueryString("sZip"))
sState = prepStr(request.QueryString("sState"))
curShipID = prepInt(request.QueryString("curShipID"))
totalQTY = prepInt(request.QueryString("totalQTY"))
adminID = prepInt(request.QueryString("adminID"))
typeid = prepInt(request.QueryString("typeid"))
modelid = prepInt(request.QueryString("modelid"))
brandid = prepInt(request.QueryString("brandid"))
cartItemid = prepInt(request.QueryString("itemid"))
cartQty = prepInt(request.QueryString("qty"))
cartPrice = prepInt(request.QueryString("price"))
accountid = prepInt(request.QueryString("accountid"))
parentAcctID = prepInt(request.QueryString("parentAcctID"))
sPromoCode = prepStr(request("promocode"))
searchKey = prepStr(request.QueryString("searchKey"))
sPartnumber = prepStr(request.QueryString("partnumber"))
cartSessionID = prepInt(request.QueryString("cartSessionID"))

nDiscountTotal = cdbl(0)

select case siteid
	case 0 
		sqlPrice = "a.price_our > 0"
		itemdesc = "itemdesc"
	case 1 
		sqlPrice = "a.price_ca > 0"
		itemdesc = "itemdesc_ca"
	case 2 
		sqlPrice = "a.price_co > 0"
		itemdesc = "itemdesc_co"
	case 3 
		sqlPrice = "a.price_ps > 0"
		itemdesc = "itemdesc_ps"
	case 10 
		sqlPrice = "(a.price_our > 0 or a.price_er > 0)"
		itemdesc = "itemdesc"
end select

if instr(sZip,"-") > 0 then
	zipArray = split(sZip,"-")
	useZip = zipArray(0)
else
	useZip = sZip
end if

do while len(useZip) < 5
	useZip = "0" & useZip
loop

select case lcase(uType)
	case "getcitybyzip"
		sql	=	"select	city" & vbcrlf & _
				"from	xzipcities" & vbcrlf & _
				"where	zipcode = '" & useZip & "'" & vbcrlf & _
				"order by 1"
		set rs = oConn.execute(sql)
		if rs.eof then
			response.write "no data to display"
		else
			do until rs.eof
				%>
                <div class="left" style="width:100%; text-align:left; padding:3px 0px 3px 0px; border-bottom:1px solid #ccc;">
                    <a href="javascript:populateSCityName('<%=replace(rs("city"), "'", "\'")%>');"><%=rs("city")%></a>
                </div>
                <%
				rs.movenext
			loop
		end if
	case "orderhistory"
		if trim(email) <> "" then
			sql	=	"select	b.store, s.shortDesc, s.color, b.orderid, v.accountid, convert(varchar(10), b.orderdatetime, 20) orderdate" & vbcrlf & _
					"	,	isnull(o.orderdesc, 'Credit Card') orderType" & vbcrlf & _
					"	,	b.orderGrandTotal, b.shipType, case when b.approved = 1 then 'Yes' else 'No' end approved" & vbcrlf & _
					"	, 	case when b.cancelled = 1 then 'Yes' else 'No' end cancelled" & vbcrlf & _
					"from	v_accounts v join we_orders b " & vbcrlf & _
					"	on	v.accountID = b.accountID and v.site_id = b.store join xstore s" & vbcrlf & _
					"	on	b.store = s.site_id	left outer join xordertype o" & vbcrlf & _
					"	on	b.extOrderType = o.typeid" & vbcrlf & _
					"where	v.email = '" & email & "'" & vbcrlf & _
					"order by b.orderid desc"
			set rs = oConn.execute(sql)
			if rs.eof then
				response.write "<div style=""display:block; padding-top:30px; color:#666; font-weight:bold;"" align=""center"">No Order History Found</div>"
			else
				%>
				<table width="370" border="0" cellspacing="0" cellpadding="2" style="font-family:Arial, Helvetica, sans-serif;">
				<%
				lap = 0
				do until rs.eof
					lap = lap + 1
					if (lap mod 2) = 0 then
						rowColor = "#EFEFEF"
					else
						rowColor = "#FFF"
					end if
					%>
					<tr style="background-color:<%=rowColor%>;">
						<td align="center" valign="top" style="width:50px; color:<%=rs("color")%>; font-weight:bold;"><%=rs("shortDesc")%></td>
						<td align="center" valign="top" style="width:70px;"><a href="javascript:void(0)" onClick="printinvoice(<%=rs("orderid")%>,<%=rs("accountid")%>);return false;" target="_blank"><%=rs("orderid")%></a>&nbsp;&nbsp;<a href="javascript:void(0)" onClick="pullOrder('<%=rs("store")%>','<%=rs("orderid")%>','<%=email%>');return false;"><img src="/images/admin/phoneorder/mglass.jpg" border="0" height="12" /></a></td>
						<td align="center" valign="top" style="width:110px;"><%=rs("orderdate")%></td>
						<td align="center" valign="top" style="width:50px;"><%=formatcurrency(rs("orderGrandTotal"))%></td>
						<td align="center" valign="top" style="width:50px;"><%=rs("approved")%>&nbsp;/&nbsp;<%=rs("cancelled")%></td>
					</tr>
					<%
					rs.movenext
				loop
				%>
				</table>
				<%
			end if
		else
			response.write "<div style=""display:block; padding-top:30px; color:#666; font-weight:bold;"" align=""center"">No Order History Found</div>"
		end if
	case "customerdata"
		if trim(email) <> "" then
			sql = 	"select	top 1 a.*" & vbcrlf & _
					"from	v_accounts a join we_orders b " & vbcrlf & _
					"	on	a.accountID = b.accountID and a.site_id = b.store" & vbcrlf & _
					"where	a.email = '" & email & "'" & vbcrlf & _
					"	and	b.store = '" & siteid & "'" & vbcrlf & _
					"order by a.accountid desc" 
			session("errorSQL") = sql
			set rs = Server.CreateObject("ADODB.Recordset")
			rs.open sql, oConn, 0, 1
		
			if not rs.eof then
				response.Write(rs("fname") & "@@" & rs("lname") & "@@" & rs("sAddress1") & "@@" & rs("sAddress2") & "@@" & rs("sCity") & "@@" & rs("sState") & "@@" & rs("sZip") & "@@" & rs("phone") & "@@" & rs("bAddress1") & "@@" & rs("bAddress2") & "@@" & rs("bCity") & "@@" & rs("bState") & "@@" & rs("bZip") & "@@" & rs("accountID"))
				if isnull(rs("parentID")) then
					response.Write("@@" & rs("accountID"))
				else		
					response.Write("@@" & rs("parentID"))
				end if
			else
				sql = 	"select	top 1 a.*" & vbcrlf & _
						"from	v_accounts a join we_orders b " & vbcrlf & _
						"	on	a.accountID = b.accountID and a.site_id = b.store" & vbcrlf & _
						"where	a.email = '" & email & "'" & vbcrlf & _
						"order by b.orderid desc"
				session("errorSQL") = sql
				set rs = Server.CreateObject("ADODB.Recordset")
				rs.open sql, oConn, 0, 1
				if not rs.eof then
					response.Write(rs("fname") & "@@" & rs("lname") & "@@" & rs("sAddress1") & "@@" & rs("sAddress2") & "@@" & rs("sCity") & "@@" & rs("sState") & "@@" & rs("sZip") & "@@" & rs("phone") & "@@" & rs("bAddress1") & "@@" & rs("bAddress2") & "@@" & rs("bCity") & "@@" & rs("bState") & "@@" & rs("bZip") & "@@" & rs("accountID"))
					if isnull(rs("parentID")) then
						response.Write("@@" & rs("accountID"))
					else		
						response.Write("@@" & rs("parentID"))
					end if
				else
					response.write "no data"
				end if
			end if
		else
			response.Write "no data"
		end if
	case "newshipping"
		totalItemWeight = 1
		totalQTY = cint(0)
		phoneInOrder = 0

		sql = 	"select	sum(isnull(a.qty, 0)) totalQty, sum(isnull(b.itemWeight, 1)) totalItemWeight, max(isnull(case when b.typeid = 16 then 1 else 0 end, 0)) phoneInOrder" & vbcrlf & _
				"from	shoppingcart a left outer join we_items b" & vbcrlf & _
				"	on	a.itemid = b.itemid left outer join we_items_musicSkins c" & vbcrlf & _
				"	on	a.itemid = c.id" & vbcrlf & _
				"where	a.store = '" & siteid & "' and a.adminID = '" & adminID & "' and (a.purchasedorderid is null or a.purchasedorderid = 0)"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		if not rs.eof then 
			if not isnull(rs("totalItemWeight")) then totalItemWeight = rs("totalItemWeight")
			if not isnull(rs("totalQty")) then totalQTY = cint(rs("totalQty"))
			if not isnull(rs("phoneInOrder")) then phoneInOrder = rs("phoneInOrder")
		end if
		if totalItemWeight / 16 < 1 then
			totalItemWeight = 1
		else
			totalItemWeight = totalItemWeight / 16
		end if
				
		international = 0
		if instr("|AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT|", "|" & sState & "|") > 0 then international = 1
		if not isnumeric(useZip) then international = 1
'		shippingUPS = UPScode_all(siteid, useZip, totalItemWeight, curShipID, totalQTY, 0,0)
		sql = 	"select	site_id, shippingID, international, includePhones, hidelive, cost, isnull(addlCostPerQty, 0) addlCostPerQty, shippingDesc, isnull(x.shipdesc, 'First Class') shiptypeDesc" & vbcrlf & _
				"from	xsiteshippingcost a left outer join xshiptype x on a.shiptypeid = x.shiptypeid" & vbcrlf & _
				"where	site_id = '" & siteid & "' and international = '" & international & "' and hidelive = 0 and includePhones = '" & phoneInOrder & "' and cost is not null" & vbcrlf

		if instr(shippingUPS, "##") > 0 then
			arrUPS = split(shippingUPS,"##")
			strShippingID = ""
			for i=0 to ubound(arrUPS)-1
				arrItem = split(arrUPS(i),"@@")
				sql = sql & "union select '" & siteid & "' site_id, '" & arrItem(0) & "' shippingID, '" & international & "' international, " & phoneInOrder & " includePhones, 0 hidelive, " & arrItem(1) & " cost, 0 addlCostPerQty, '" & arrItem(2) & "' shippingDesc, '" & arrItem(2) & "' shiptypeDesc" & vbcrlf
			next
		end if
		sql = sql & "order by site_id, international, shippingid"
'		response.write "<pre>" & sql & "</pre>"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		if not rs.eof then
			%>
            Shipping Method:&nbsp;
            <select id="id_cbShip" name="cbShip" onchange="updateGrandTotal();" style="font-size:11px; padding:2px; width:270px;">
            <%
			do until rs.eof
				costPerQty = cdbl(rs("addlCostPerQty") * totalQTY)
				shippingCost = cdbl(rs("cost")) + costPerQty
			%>
				<option value="<%=rs("shippingID")%>" <%if curShipID = rs("shippingID") then%> selected <%end if%>>
					<%=rs("shippingDesc")%>&nbsp;-&nbsp;$<%=formatnumber(shippingCost,2)%>
				</option>
				<%
				rs.movenext
			loop
			rs.movefirst
			%>
            </select>
            <%
			do until rs.eof
				costPerQty = cdbl(rs("addlCostPerQty") * totalQTY)
				shippingCost = cdbl(rs("cost")) + costPerQty
				shiptypeDesc = rs("shiptypeDesc")
				%>
                <input type="hidden" id="id_shiptype_<%=rs("shippingID")%>" value="<%=shippingCost%>" />
                <input type="hidden" id="id_shiptypedesc_<%=rs("shippingID")%>" value="<%=shiptypeDesc%>" />
                <%
				rs.movenext
			loop
		else
			%>
            Shipping Method:&nbsp;
            <select name="cbShip" style="font-size:11px; padding:2px; width:270px;">
				<option value="-1">N/A</option>
			</select>
            <%
		end if
	case "getcurrentcart"
		freeItemTotal = cdbl(0)
		sql	=	"select	a.id, c.brandname, d.modelname, a.musicskins, a.customcost customcost, a.lockqty, a.itemid, isnull(a.qty, 0) qty, b.modelid, b.typeid, b.partnumber" & vbcrlf & _
				"	, 	b." & itemdesc & " itemdesc, b.itempic, b.itempic defaultImg" & vbcrlf & _
				"	,	b.price_our, b.price_ca, b.price_co, b.price_ps, price_er price_tm, b.price_retail, b.condition, b.itemweight, b.nodiscount, t.typeid, t.typename " & vbcrlf & _
				"from	shoppingcart a join we_items b " & vbcrlf & _
				"	on	a.itemid=b.itemid left outer join we_brands c " & vbcrlf & _
				"	on	b.brandid = c.brandid left outer join we_models d " & vbcrlf & _
				"	on	b.modelid = d.modelid left outer join we_types t" & vbcrlf & _
				"	on	b.typeid = t.typeid " & vbcrlf & _
				"where	a.musicSkins = 0 and a.store = '" & siteid & "' and a.adminID = '" & adminID & "' and (a.purchasedorderid is null or a.purchasedorderid = 0)" & vbcrlf & _
				"union" & vbcrlf & _
				"select	a.id, c.brandname, d.modelname, a.musicskins, a.customcost customcost, a.lockqty, a.itemid, isnull(a.qty, 0) qty, b.modelid, 3 as typeid, b.musicskinsid as partnumber" & vbcrlf & _
				"	, 	isnull(c.brandname, '') + ' ' + isnull(d.modelname, '') + ' ' + isnull(b.artist, '') + ' ' + isnull(b.designname, '') as itemdesc, b.image as itempic, b.defaultImg" & vbcrlf & _
				"	,	b.price_we as price_our, b.price_ca, b.price_co, b.price_we price_ps, b.price_we price_tm, b.msrp as price_retail, 0 as condition, 1 as itemweight, 0 as nodiscount, 20 typeid, 'Music Skins' typename" & vbcrlf & _
				"from	shoppingcart a join we_items_musicskins b " & vbcrlf & _
				"	on	a.itemid = b.id left outer join we_brands c " & vbcrlf & _
				"	on	b.brandid = c.brandid left outer join we_models d " & vbcrlf & _
				"	on	b.modelid = d.modelid" & vbcrlf & _
				"where	a.musicskins = 1 and a.store = '" & siteid & "' and a.adminID = '" & adminID & "' and (a.purchasedorderid is null or a.purchasedorderid = 0)" & vbcrlf & _
				"order by id desc"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
'		response.write "<pre>" & sql & "</pre>"
		%>
        <table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
        <%
		nSubTotal = cdbl(0)
		nDiscountTotal = cdbl(0)
		nTotalQty = cint(0)
		PhonePurchased = 0
		if not rs.eof then
			do until rs.eof
				select case siteid
					case 0
						if rs("typeid") = 20 then
							productLink = "http://www.wirelessemporium.com/p-ms-" & rs("itemid") & "-" & formatSEO(rs("itemdesc")) & ".asp"
						else
							productLink = "http://www.wirelessemporium.com/p-" & rs("itemid") & "-" & formatSEO(rs("itemdesc")) & ".asp"
						end if
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"">" & rs("itemdesc") & "</a>"
						orig_price = cdbl(rs("price_our"))
					case 1
						productLink = "http://www.cellphoneaccents.com/p-" & rs("itemid") & "-" & formatSEO(rs("itemdesc")) & ".html"
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"">" & rs("itemdesc") & "</a>"
						orig_price = cdbl(rs("price_ca"))
					case 2
						productLink = "http://www.cellularoutfitter.com/p-" & rs("itemid") & "-" & formatSEO(rs("itemdesc")) & ".html"
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"">" & rs("itemdesc") & "</a>"					
						orig_price = cdbl(rs("price_co"))
					case 3
						if rs("typeid") = 16 then
							productLink = "http://www.phonesale.com/" & formatSEO(rs("brandname")) & "/cell-phones/" & formatSEO(rs("itemdesc")) & "-p-" & rs("itemid")+300001 & ".html"
						else
							productLink = "http://www.phonesale.com/accessories/" & formatSEO(rs("typename")) & "/" & formatSEO(rs("itemdesc")) & "-p-" & rs("itemid")+300001 & ".html"
						end if
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"">" & rs("itemdesc") & "</a>"
						orig_price = cdbl(rs("price_ps"))
					case 10
						productLink = "http://www.tabletmall.com/" & formatSEO(rs("itemdesc")) & "-p-" & rs("itemid")
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"">" & rs("itemdesc") & "</a>"
						orig_price = cdbl(getPricingTM(rs("price_tm"), rs("price_our")))
				end select
				customprice = rs("customcost")
				usePrice = orig_price
				qty = cint(rs("qty"))
				if not isnull(customprice) then usePrice = cdbl(customprice)
				if rs("typeid") = 16 then PhonePurchased = 1

				freeItemTotal = freeItemTotal + ((orig_price * qty) - (usePrice * qty))
				rowItemTotal = qty * usePrice
				nSubTotal = nSubTotal + rowItemTotal
				nTotalQty = nTotalQty + qty
				useImagePath = getUseImage(rs("itempic"),cint(rs("musicSkins")),rs("defaultImg"))
			%>
            <tr>
                <td width="400" align="left" style="border-top:1px solid #ccc;">
                	<div style="float:left; width:45px;"><img src="<%=useImagePath%>" border="0" width="40" height="40" style="border:1px solid #ccc;" /></div>
                    <div style="float:left; padding-left:5px; width:320px;"><%=itemDesc%></div>
				</td>
                <td width="110" align="right" style="border-top:1px solid #ccc;">
                	<div style="float:left; padding-top:5px; width:30px;"><input id="id_curqty_<%=rs("itemid")%>" type="text" name="txtCurQty" value="<%=qty%>" style="width:25px;" /></div>
                    <div style="float:left; padding-left:5px; width:45px;">
                    	<div><a href="javascript:void(0)" onclick="updateItem('<%=rs("itemid")%>');return false;" style="font-size:10px; text-decoration:underline; color:#666;">UPDATE</a></div>
                    	<div><a href="javascript:void(0)" onclick="deleteItem('<%=rs("itemid")%>');return false;" style="font-size:10px; text-decoration:underline; color:#666;">DELETE</a></div>
                    </div>
				</td>
                <td width="80" align="right" style="border-top:1px solid #ccc;">
                    <input id="id_curprice_<%=rs("itemid")%>" type="hidden" name="txtCurPrice" value="<%=rowItemTotal%>" />
                    <div><%=formatcurrency(rowItemTotal)%></div>
                    <div><a onclick="applyItemDiscount(<%=rs("itemid")%>,<%=rowItemTotal%>,0);" style="cursor:pointer; font-size:10px; border:1px solid #ccc; padding:1px; background-color:#999; color:#FFF;">FREE</a></div>                    
				</td>
            </tr>
            <%
				rs.movenext
			loop
		end if

		dim promoMin, promoPercent, cTypeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		if sPromoCode <> "" then
			couponid = 0
			SQL = "select * from v_coupons where site_id = '" & siteid & "' and promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 0, 1
			if not RS.eof then
				couponid = RS("couponid")
				promoMin = RS("promoMin")
				promoPercent = RS("promoPercent")
				cTypeID = RS("typeID")
				excludeBluetooth = RS("excludeBluetooth")
				BOGO = RS("BOGO")
				couponDesc = RS("couponDesc")
				FreeItemPartNumber = RS("FreeItemPartNumber")
				setValue = RS("setValue")
				oneTime = RS("oneTime")
			else
				sPromoCode = ""
			end if
		end if
		%>
		<!--#include virtual="/includes/admin/inc_adminPromoFunctions.asp"-->        
            <input type="hidden" name="nTotalQuantity" value="<%=round(nTotalQty,0)%>" />
            <input type="hidden" name="nSubTotal" value="<%=round(nSubTotal,2)%>" />
            <input type="hidden" name="nFreeItemTotal" value="<%=round(freeItemTotal,2)%>" />            
            <input type="hidden" name="nDiscountTotal" value="<%=round(nDiscountTotal,2)%>" />
            <tr>
                <td colspan="3" align="left" style="border-top:1px solid #ccc;">
                	<div id="id_addNewHere">
                	<table border="0" cellspacing="0" cellpadding="3" width="100%" style="border-collapse:collapse;">
                    	<tr>
                        	<td style="background-color:#f2f2f2; font-weight:bold; border-right:1px solid #ccc; border-bottom:1px solid #ccc;" align="center">Search Items</td>
                            <td style="background-color:#f2f2f2; font-weight:bold; border-right:1px solid #ccc; border-bottom:1px solid #ccc;" align="center">Search By CartID</td>
                            <td style="background-color:#f2f2f2; font-weight:bold; border-bottom:1px solid #ccc;" align="center">Search By Declined OrderID</td>
						</tr>
                    	<tr>
                        	<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;" align="center"><a href="javascript:void(0)" onclick="showSearch(); return false;" style="font-weight:bold; color:#007ABE;">Add New Items</a></td>
                            <td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;" align="center">
                            	<input type="text" id="cartID" name="txtCartID" /> <a href="javascript:void(0)" onclick="getItemByID('cart'); return false;"><img id="img_cartID" src="/images/admin/phoneorder/mglass.jpg" border="0" /></a>
							</td>
                            <td style="border-bottom:1px solid #ccc;" align="center">
                            	<input type="text" id="id_orderID" name="txtOrderID" /> <a href="javascript:void(0)" onclick="getItemByID('order'); return false;"><img id="img_orderID" src="/images/admin/phoneorder/mglass.jpg" border="0" /></a>
                            </td>
						</tr>
                    </table>
                    </div>
				</td>
            </tr>
            <tr>
            	<td width="400" align="left" style="border-top:1px solid #ccc;">
                	<div style="float:left; padding-top:4px;">Enter Promo Code:</div>
                    <div style="float:left; padding-left:5px;"><input type="text" name="sPromoCode" style="width:150px;" value="<%=sPromoCode%>" /></div>
                    <div style="float:left; padding:4px 0px 0px 5px;"><a onclick="applyPromo();" style="cursor:pointer; border:1px solid #ccc; padding:3px; background-color:#999; color:#FFF;">APPLY</a></div>
                </td>
                <td width="110" align="right" style="border-top:1px solid #ccc; padding-top:5px;">Item Subtotal:</td>
                <td width="80" align="right" style="border-top:1px solid #ccc; padding-right:8px;"><%=formatcurrency(nSubTotal)%></td>
            </tr>
            <%
            if freeItemTotal > 0 then
            %>
            <tr>
                <td align="left" style="background-color:#efefef; padding-left:10px;"><b>Free Item Discount: </b></td>
                <td align="right" style="background-color:#efefef;">&nbsp;</td>
                <td align="right" style="background-color:#efefef; color:#9a0002; padding-right:8px;"><%=formatCurrency(freeItemTotal * -1)%></td>
            </tr>
            <%	
            end if

            if nDiscountTotal > 0 then
            %>
            <tr>
                <td align="left" style="background-color:#efefef; padding-left:10px;"><b>Promotions: </b><%=couponDesc%></td>
                <td align="right" style="background-color:#efefef;">Discount:</td>
                <td align="right" style="background-color:#efefef; color:#9a0002; padding-right:8px;"><%=formatCurrency(nDiscountTotal * -1)%></td>
            </tr>
            <%	
            end if
            %>
            <tr>
                <td colspan="2" align="right"><span style="font-size:11px; color:#666;" id="CAtaxMsg"></span>&nbsp;&nbsp;&nbsp;Tax:</td>
                <td align="right" style="padding-right:8px;"><span id="CAtax"><%=formatCurrency(0)%></span></td>
            </tr>
            <tr>
                <td align="left" id="currentShipOptions" style="border-bottom:1px solid #ccc; padding:0px 0px 5px 5px;">&nbsp;</td>
                <td align="right" style="border-bottom:1px solid #ccc;">Shipping:</td>
                <td align="right" style="border-bottom:1px solid #ccc; padding-right:8px;">
                	<div >$<input type="text" name="txtShippingTotal" value="0" style="width:35px" /></div>
                    <div style="padding-top:2px;"><a onclick="applyCustomShipping();" style="cursor:pointer; font-size:10px; border:1px solid #ccc; padding:1px; background-color:#999; color:#FFF;">APPLY</a></div>
				</td>
            </tr>
            <tr>
            	<td align="left" style="border-bottom:1px solid #ccc;" valign="middle">Special Notes: <input type="text" name="txtOrderNotes" style="width:250px" /></td>
                <td align="right" style="border-bottom:1px solid #ccc;" valign="middle">Grand Total:</td>
                <td align="right" style="border-bottom:1px solid #ccc; padding:5px 8px 5px 0px;" valign="middle">
                    <span id="GrandTotal"><%=formatCurrency(0)%></span>
                </td>
            </tr>
            <input type="hidden" name="nPromo" value="<%=couponid%>">
			<input type="hidden" name="nCATax" value="" />
            <input type="hidden" name="nShippingTotal" value="" />
            <input type="hidden" name="nShippingDiscountTotal" value="" />
            <input type="hidden" name="shiptypeID" value="" />
            <input type="hidden" name="strShipType" value="" />
            <input type="hidden" name="nGrandTotal" value="" />
            <input type="hidden" name="couponcode" value="<%=sPromoCode%>" />
            <input type="hidden" name="coupondesc" value="<%=couponDesc%>" />
            <input type="hidden" name="adminID" value="<%=adminID%>" />
        </table>
        <%
	case "showsearch"
		%>
        <div id="floatingList" style="display:block; width:800px; padding:3px; border:3px solid #aaa; background-color:#FFF; margin-left:auto; margin-right:auto; margin-top:50px;">
            <table border="0" cellspacing="0" cellpadding="3" width="100%">
                <tr>
                    <td width="100%" align="center" style="border-bottom:1px solid #ccc;">
                    	<div style="float:left;"><b>Search Items:</b></div>
                        <div style="float:right;"><img onclick="closefloatingList();" src="/images/admin/phoneorder/close_window.jpg" border="0" style="cursor:pointer;" /></div>
                    </td>
                </tr>
                <tr>
                    <td width="100%" style="padding-top:5px;" align="left" id="id_search_current">
                    <%
                        sql = "select typeid, typename from we_typemasking where typeid not in (15) order by 2"
                        set rsType = oConn.execute(sql)
						sql = "select brandid, brandname from we_brands where brandid not in (12) order by 2"
						set rsBrand = oConn.execute(sql)
						sql = "select modelid, modelname from we_models where brandid not in (12) order by 2"
						set rsModel = oConn.execute(sql)
                        %>
                        <div style="width:790px; height:25px; font-size:14px; font-weight:bold; color:#666; padding-bottom:5px; border-bottom:1px solid #ccc;" align="left">
                            <div style="width:100%; padding:5px 0px 0px 0px;">
                                <div style="float:left; font-weight:bold; padding-top:3px;">BRANDS:&nbsp;</div>
                                <div style="float:left;" id="id_search_brand">
                                <select id="id_cbBrand" name="cbBrand" onchange="onBrand(this.value);" style="width:160px;">
                                    <option value="0">-- Select Brand --</option>
                                    <%
                                    do until rsBrand.eof
                                    %>
                                    <option value="<%=rsBrand("brandid")%>"><%=rsBrand("brandname")%></option>
                                    <%
                                        rsBrand.movenext
                                    loop
                                    %>
                                </select>
                                </div>
                                <div style="float:left; font-weight:bold; padding:3px 0px 0px 20px;">MODELS:&nbsp;</div>
                                <div style="float:left;" id="id_search_model">
                                <select id="id_cbModel" name="cbModel" onchange="onModel(this.value);" style="width:160px;">
                                    <option value="0">-- Select Model --</option>
                                    <%
                                    do until rsModel.eof
                                    %>
                                    <option value="<%=rsModel("modelid")%>"><%=rsModel("modelname")%></option>
                                    <%
                                        rsModel.movenext
                                    loop
                                    %>
                                </select>
                                </div>
                                <div style="float:left; font-weight:bold; padding:3px 0px 0px 20px;">TYPES:&nbsp;</div>
                                <div style="float:left; height:25px; margin-left:5px;" id="id_search_type">
                                <select id="id_cbType" name="cbType" onchange="onType(this.value);" style="width:160px;">
                                    <option value="0">-- Select Types --</option>
                                    <%
                                    do until rsType.eof
                                    %>
                                    <option value="<%=rsType("typeid")%>"><%=rsType("typename")%></option>
                                    <%
                                        rsType.movenext
                                    loop
                                    %>
                                </select>
                                </div>
                            </div>
						</div>
                        <div style="width:790px; height:25px; font-size:14px; font-weight:bold; color:#666; padding-bottom:7px; border-bottom:1px solid #ccc;" align="left">
                            <div style="width:100%; padding:5px 0px 0px 0px;">
                                <div style="float:left; font-weight:bold; padding:3px 0px 0px 0px;">ITEM ID:&nbsp;</div>
                                <div style="float:left;"><input type="text" id="id_search_itemid" name="txtSearchItemid" value="Search by Item ID" onclick="this.value='';reloadSearchBox();" style="color:#666; width:120px;" /></div>
                                <div style="float:left; padding:4px 0px 0px 5px;"><a onclick="pullData('i', document.getElementById('id_search_itemid').value);" style="cursor:pointer;"><img src="/images/admin/phoneorder/mglass.jpg" border="0" /></a></div>
                                <div style="float:left; font-weight:bold; padding:3px 0px 0px 20px;">ITEM DESC / PARTNUMBER:&nbsp;</div>
                                <div style="float:left;"><input type="text" id="id_search_itemdesc" name="txtSearchItemdesc" value="Search by PartNumber or Description" onclick="this.value='';reloadSearchBox();" style="color:#666; width:350px;" /></div>
                                <div style="float:left; padding:4px 0px 0px 5px;"><a onclick="pullData('k', document.getElementById('id_search_itemdesc').value);" style="cursor:pointer;"><img src="/images/admin/phoneorder/mglass.jpg" border="0" /></a></div>
                            </div>
                        </div>
                        <div style="width:790px; height:1px; font-size:1px; border-bottom:1px solid #ccc;" align="left"></div>
                        <div style="padding:10px 2px 2px 2px; width:790px; height:600px; overflow:auto;" id="id_searchresult"></div>
					</td>
				</tr>
			</table>
		</div>
        <%
	case "onbrand"
		sql = "select modelid, modelname from we_models where brandid = '" & brandid & "' order by 2"
		set rs = oConn.execute(sql)
		%>
		<select id="id_cbModel" name="cbModel" onchange="onModel(this.value);" style="width:160px;">
			<option value="0">-- Select Model --</option>
        <%
		do until rs.eof
			%>
			<option value="<%=rs("modelid")%>"><%=rs("modelname")%></option>
            <%
			rs.movenext
		loop
		%>
		</select>
	<%		
	case "ontype"
		addlSQL = ""
		if searchKey <> "" then
			addlSQL = " and (a.partnumber like '%" & searchKey & "%' or a." & itemdesc & " like '%" & searchKey & "%')" & vbcrlf
		end if

		if searchKey <> "" then
			sql	=	"select	itemid, partnumber, itempic, " & itemdesc & " itemdesc, price_retail, price_our, price_ca, price_co, price_ps, price_er price_tm" & vbcrlf & _
					"	,	flag1, a.typeid, a.subtypeid, numberOfSales, itempic defaultImg, 0 musicSkins, isnull((select top 1 inv_qty from we_Items where partNumber = a.partNumber and master = 1), 0) inv_qty" & vbcrlf & _
					"from	we_items a with (nolock) " & vbcrlf & _
					"where	" & sqlPrice & vbcrlf & _
					"	and a.hidelive = 0" & vbcrlf & _
					"	and	(a.itempic is not null and a.itempic != '')" & addlSQL
			if modelid > 0 and (typeid <> 5 and typeid <> 16) then
				sql = sql & "	and a.modelid = '" & modelid & "'"
			end if
		elseif cartItemid > 0 then
			sql	=	"select	itemid, partnumber, itempic, " & itemdesc & " itemdesc, price_retail, price_our, price_ca, price_co, price_ps, price_er price_tm" & vbcrlf & _
					"	,	flag1, a.typeid, a.subtypeid, numberOfSales, itempic defaultImg, 0 musicSkins, isnull((select top 1 inv_qty from we_Items where partNumber = a.partNumber and master = 1), 0) inv_qty" & vbcrlf & _
					"from	we_items a with (nolock) " & vbcrlf & _
					"where	a.itemid = '" & cartItemid & "'" & vbcrlf & _
					"	and	" & sqlPrice & vbcrlf & _
					"	and a.hidelive = 0" & vbcrlf & _
					"	and	(a.itempic is not null and a.itempic != '')" & addlSQL
		elseif typeid > 0 then
			if typeid = 5 then
				HandsfreeTypes = ""
				sql = "select handsfreetypes from we_models where modelid = '" & modelid & "' and handsfreetypes is not null and handsfreetypes <> ''"
				session("errorSQL") = sql
				set modelRS = oConn.execute(sql)
				if not modelRS.eof then
					if not isnull(modelRS("handsfreetypes")) then
						HandsfreeTypes = left(modelRS("HandsfreeTypes"),len(modelRS("HandsfreeTypes"))-1)
					end if
				end if
				
				if HandsfreeTypes = "" then
					response.write "<div style=""display:block; padding-top:30px;"" align=""center"">No Data To Display</div>"
					response.end
				else
					sql	=	"select	itemid, partnumber, itempic, a." & itemdesc & " itemdesc, price_retail, price_our, price_ca, price_co, price_ps, price_er price_tm" & vbcrlf & _
							"	,	flag1, v.typeid, v.subtypeid, numberOfSales, itempic defaultImg, 0 musicSkins, 100 inv_qty" & vbcrlf & _
							"from	we_items a with (nolock) join v_subtypematrix v with (nolock)" & vbcrlf & _
							"	on	a.subtypeid = v.subtypeid " & vbcrlf & _
							"where	v.typeid = 5" & vbcrlf & _
							"	and	a.handsfreetype in (" & HandsfreeTypes & ")" & vbcrlf & _
							"	and	" & sqlPrice & vbcrlf & _
							"	and a.hidelive = 0 and (select top 1 inv_qty from we_Items where partNumber = a.partNumber and master = 1) > 0 " & vbcrlf & _
							"	and	(a.itempic is not null and a.itempic != '')" & vbcrlf & addlSQL & _
							"order by flag1 desc" 
				end if
			elseif typeid = 8 then
				sql	=	"select	a.itemid, a.partnumber, a.itempic, a." & itemdesc & " itemdesc, a.price_retail, a.price_our, a.price_ca, a.price_co, a.price_ps, a.price_er price_tm" & vbcrlf & _
						"	,	a.flag1, v.typeid, v.subtypeid, a.numberOfSales, a.itempic defaultImg, 0 musicSkins, 100 inv_qty" & vbcrlf & _
						"from	we_items a with (nolock) join v_subtypematrix v with (nolock)" & vbcrlf & _
						"	on	a.subtypeid = v.subtypeid " & vbcrlf & _
						"where	v.typeid = '8'" & vbcrlf & _
						"	and " & sqlPrice & " and a.hidelive = 0 and (select top 1 inv_qty from we_Items where partNumber = a.partNumber and master = 1) > 0 " & vbcrlf & _
						"	and	(a.itempic is not null and a.itempic != '')" & vbcrlf & addlSQL & _
						"order by a.flag1 desc, a.itemid desc"
			elseif typeid = 16 then
				sql	=	"select	itemid, partnumber, itempic, a." & itemdesc & " itemdesc, price_retail, price_our, price_ca, price_co, price_ps, price_er price_tm" & vbcrlf & _
						"	,	flag1, v.typeid, v.subtypeid, numberOfSales, itempic defaultImg, 0 musicSkins, 100 inv_qty" & vbcrlf & _
						"from	we_items a with (nolock) join v_subtypematrix v with (nolock)" & vbcrlf & _
						"	on	a.subtypeid = v.subtypeid " & vbcrlf & _
						"where	v.typeid = '15'" & vbcrlf & _
						"	and	" & sqlPrice & vbcrlf & _
						"	and a.hidelive = 0 and (select top 1 inv_qty from we_Items where partNumber = a.partNumber and master = 1) > 0 " & vbcrlf & _
						"	and	(a.itempic is not null and a.itempic != '')" & vbcrlf & addlSQL
				if brandid > 0 then
					sql = sql & "	and	brandid = '" & brandid & "'" & vbcrlf
				end if			
				if modelid > 0 then
					sql = sql & "	and	modelid = '" & modelid & "'" & vbcrlf
				end if
				sql = sql & "order by flag1 desc"
			else
				sql = 	"select	itemid, partnumber, itempic, a." & itemdesc & " itemdesc, price_retail, price_our, price_ca, price_co, price_ps, price_er price_tm" & vbcrlf & _
						"	,	flag1, v.typeid, v.subtypeid, numberOfSales, itempic defaultImg, 0 musicSkins, 100 inv_qty" & vbcrlf & _
						"from	we_items a with (nolock) join v_subtypematrix v with (nolock)" & vbcrlf & _
						"	on	a.subtypeid = v.subtypeid " & vbcrlf & _
						"where	a.modelid = '" & modelid & "'" & vbcrlf & _
						"	and	v.typeid = '" & typeid & "'" & vbcrlf & _
						"	and	" & sqlPrice & vbcrlf & _
						"	and a.hidelive = 0 and (select top 1 inv_qty from we_Items where partNumber = a.partNumber and master = 1) > 0 " & vbcrlf & _
						"	and	(a.itempic is not null and a.itempic != '')" & vbcrlf & addlSQL & _
						"union	" & vbcrlf & _
						"select	a.itemid, a.partnumber, a.itempic, a." & itemdesc & " itemdesc, a.price_retail, a.price_our, a.price_ca, a.price_co, a.price_ps, a.price_er price_tm" & vbcrlf & _
						"	,	a.flag1, v.typeid, v.subtypeid, a.numberOfSales, itempic defaultImg, 0 musicSkins, 100 inv_qty" & vbcrlf & _
						"from	we_items a with (nolock) join we_subRelatedItems b" & vbcrlf & _
						"	on	a.itemid = b.itemid join v_subtypematrix v" & vbcrlf & _
						"	on	a.subtypeid = v.subtypeid" & vbcrlf & _
						"where	b.modelid = '" & modelid & "'" & vbcrlf & _
						"	and	v.typeid = '" & typeid & "'" & vbcrlf & _
						"	and	" & sqlPrice & vbcrlf & _
						"	and a.hidelive = 0 and (select top 1 inv_qty from we_Items where partNumber = a.partNumber and master = 1) > 0" & vbcrlf & _
						"	and	(a.itempic is not null and a.itempic != '')" & vbcrlf & addlSQL
				if typeid = 18 then
					Set Jpeg = Server.CreateObject("Persits.Jpeg")
					Jpeg.Quality = 100
					Jpeg.Interpolation = 1					
					sql = sql &	"union" & vbcrlf & _
								"select a.id as itemID, a.musicSkinsID partnumber, a.image as itemPic, a.artist + ' ' + a.designName as itemDesc" & vbcrlf & _
								"	,	isnull(a.msrp, 0.0) price_retail, isnull(a.price_we, 0.0) price_our, isnull(a.price_ca, 0.0) price_ca, isnull(a.price_co, 0.0) price_co" & vbcrlf & _
								"	,	isnull(a.price_we, 0.0) price_ps, isnull(a.price_we, 0.0) price_tm" & vbcrlf & _
								"	,	0 flag1, 20 typeid, 1270 subtypeid, 1 numberOfSales, a.defaultImg, 1 musicSkins, 100 inv_qty" & vbcrlf & _
								"from 	we_items_musicSkins a" & vbcrlf & _
								"where 	a.skip = 0 " & vbcrlf & _
								"	and a.deleteItem = 0 " & vbcrlf & _
								"	and (a.artist <> '' and a.artist is not null) " & vbcrlf & _
								"	and (a.designname <> '' and a.designname is not null) " & vbcrlf & _
								"	and a.deleteItem = 0 " & vbcrlf & _
								"	and a.modelID = '" & modelid & "'" & vbcrlf
				end if						
				sql = sql & "order by flag1 desc"
			end if
		else
		%>
		<div style="display:block; padding-top:30px;" align="center">No Data To Display</div>
		<%
			response.end
		end if
		
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		if not rs.eof then
			%>
			<table border="0" cellpadding="3" cellspacing="2" align="center" width="770px">
				<tr>            
			<%
			lap = 0
			do until rs.eof
				lap = lap + 1
				thumbImagePath = getUseImage(rs("itempic"), rs("musicSkins"), rs("defaultImg"))					
				itemdesc = replace(rs("itemdesc"), "/", " / ")
				price_retail = rs("price_retail")
				inv_qty = rs("inv_qty")
				%>
				<td style="width:144px; border:1px solid #ccc; position:relative; z-index:102;" align="left" valign="top">
					<div style="padding-left:10px;"><img src="<%=thumbImagePath%>" width="100" height="100" border="0" /></div>
					<div style="width:100%; padding-top:5px; font-size:11px; color:#333; text-decoration:none;"><%=itemdesc%></div>
					<div style="width:100%; padding-top:5px; font-size:11px;">ITEMID:&nbsp;<%=rs("itemid")%></div>
					<div style="width:100%; font-size:11px;">(<%=rs("partnumber")%>)</div>
					<div style="width:100%; padding-top:5px;">
					<%
					select case siteid
						case 0 : price_our = rs("price_our")
						case 1 : price_our = rs("price_ca")
						case 2 : price_our = rs("price_co")
						case 3 : price_our = rs("price_ps")
						case 10 : price_our = getPricingTM(rs("price_tm"), rs("price_our"))
					end select
					%>
						<div style="font-size:16px; color:#900; font-weight:bold;"><%=formatcurrency(price_our)%></div>
						<div style="font-size:11px; color:#999;">(<s><%=formatcurrency(price_retail)%></s>)</div>
					</div>
                    <%if inv_qty > 0 then%>
					<div style="width:100%; padding-top:10px; font-size:10px;">
						<div style="float:left;"><input id="id_itemqty_<%=rs("itemid")%>" type="text" name="txtItemQty" value="1" style="width:25px;" />&nbsp;QTY.</div>
						<div style="float:left; margin-left:6px; padding-top:6px;">
                        	<a href="javascript:void(0)" onclick="addItemToCart('<%=rs("itemid")%>','<%=price_our%>');" style="font-size:12px; color:#096; font-size:11px; font-weight:bold; text-decoration:none;">ADD ITEM</a>
						</div>
					</div>
                    <%else%>
					<div style="width:100%; padding-top:10px; font-size:14px; font-weight:bold; color:#900;">**Out of stock**</div>
                    <%end if%>
				</td>
				<%
				if lap >= 5 then
					lap = 0
					response.write "</tr><tr>"
				end if
				rs.movenext
			loop
			response.write "</tr></table>"
		else
		%>
		<div style="display:block; padding-top:30px;" align="center">No Data To Display</div>
		<%
		end if			
	case "additem"
		sql =	"select id from shoppingcart where store = '" & siteid & "' and adminid = '" & adminID & "' and itemid = '" & cartItemid & "' and (purchasedorderid is null or purchasedorderid = 0)"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		if rs.eof then
			musicSkins = 0
			if cartItemid > 1000000 then musicSkins = 1
			sql	=	"insert into shoppingcart(store,accountid,itemid,qty,adminid,musicSkins) " & vbcrlf & _
					"values('" & siteid & "','" & accountid & "','" & cartItemid & "','" & cartQty & "','" & adminID & "'," & musicSkins & ")"
			session("errorSQL") = sql
			oConn.execute(sql)
			response.write "added"
		else
			sql	=	"update	shoppingcart" & vbcrlf & _
					"set	qty = qty + " & cartQty & vbcrlf & _
					"where	store = '" & siteid & "'" & vbcrlf & _
					"	and	adminid = '" & adminID & "'" & vbcrlf & _
					"	and	itemid = '" & cartItemid & "'" & vbcrlf & _
					"	and (purchasedorderid is null or purchasedorderid = 0)"
			session("errorSQL") = sql
			oConn.execute(sql)
			response.write "added"
		end if
	case "addcartitem"
		sql	=	"update	shoppingcart" & vbcrlf & _
				"set	adminid = '" & adminID & "'" & vbcrlf & _
				"where	store = '" & siteid & "'" & vbcrlf & _
				"	and	sessionid = '" & cartSessionID & "'" & vbcrlf & _
				"	and (purchasedorderid is null or purchasedorderid = 0)"
		session("errorSQL") = sql
		oConn.execute(sql)
		response.write "added"
	case "addcartitembyorderid"
		sql	=	"update	a" & vbcrlf & _
				"set	adminid = '" & adminID & "'" & vbcrlf & _
				"from	shoppingcart a join we_orders b" & vbcrlf & _
				"	on	a.sessionid = b.sessionid and a.store = b.store" & vbcrlf & _
				"where	a.store = '" & siteid & "'" & vbcrlf & _
				"	and	b.orderid = '" & cartOrderID & "'" & vbcrlf & _
				"	and (a.purchasedorderid is null or a.purchasedorderid = 0)"
'		response.write "<pre>" & sql & "</pre>"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql	=	"select	b.email" & vbcrlf & _
				"from	we_orders a join v_accounts b" & vbcrlf & _
				"	on	a.store = b.site_id and a.accountid = b.accountid" & vbcrlf & _
				"where	a.orderid = '" & cartOrderID & "'"
		set rs = oConn.execute(sql)
		if not rs.eof then
			response.write rs("email")
		else
			response.write "added"
		end if
	case "updateitem"
		sql	=	"update	shoppingcart" & vbcrlf & _
				"set	qty = '" & cartQty & "'" & vbcrlf & _
				"where	store = '" & siteid & "'" & vbcrlf & _
				"	and	adminid = '" & adminID & "'" & vbcrlf & _
				"	and	itemid = '" & cartItemid & "'" & vbcrlf & _
				"	and (purchasedorderid is null or purchasedorderid = 0)"
		session("errorSQL") = sql
		oConn.execute(sql)
		response.write "updated"		
	case "deleteitem"
		sql	=	"delete" & vbcrlf & _
				"from	shoppingcart" & vbcrlf & _
				"where	store = '" & siteid & "'" & vbcrlf & _
				"	and	adminid = '" & adminID & "'" & vbcrlf & _
				"	and	itemid = '" & cartItemid & "'" & vbcrlf & _
				"	and (purchasedorderid is null or purchasedorderid = 0)"
		session("errorSQL") = sql
		oConn.execute(sql)
		response.write "deleted"
	case "updateprice"
		sql	=	"update	shoppingcart" & vbcrlf & _
				"set	customcost = '" & cartPrice & "'" & vbcrlf & _
				"where	store = '" & siteid & "'" & vbcrlf & _
				"	and	adminid = '" & adminID & "'" & vbcrlf & _
				"	and	itemid = '" & cartItemid & "'" & vbcrlf & _
				"	and (purchasedorderid is null or purchasedorderid = 0)"
		session("errorSQL") = sql
		oConn.execute(sql)
		response.write "updated"		
	case "pullorder"
		sql	=	"delete" & vbcrlf & _
				"from	shoppingcart" & vbcrlf & _
				"where	store = '" & siteid & "'" & vbcrlf & _
				"	and	adminid = '" & adminID & "'" & vbcrlf & _
				"	and (purchasedorderid is null or purchasedorderid = 0)"				
		session("errorSQL") = sql
		oConn.execute(sql)

		sql	=	"insert into shoppingcart(store,accountid,itemid,qty,adminid,musicSkins) " & vbcrlf & _
				"select	a.store, v.accountid, b.itemid, b.quantity, '" & adminID & "', isnull(b.musicSkins, 0) musicSkins " & vbcrlf & _
				"from	we_orders a join we_orderdetails b " & vbcrlf & _
				"	on	a.orderid = b.orderid join v_accounts v " & vbcrlf & _
				"	on	a.accountid = v.accountid and a.store = v.site_id " & vbcrlf & _
				"where	a.orderid = '" & orderid & "' "
		session("errorSQL") = sql
		oConn.execute(sql)
		
		response.write "added"
end select

call CloseConn(oConn)

function getUseImage(pItemPic, pMusicSkins, pMsDefaultImg)
	useImgPath = "/productpics/thumb/imagena.jpg"
	if pMusicSkins = 1 then
		itemImgPath = Server.MapPath("/productpics/musicSkins/musicSkinsLarge") & "\" & pItemPic
		itemImgPath2 = Server.MapPath("/productpics/musicSkins/musicSkinsSmall") & "\" & pItemPic
		useImgPath = "/productpics/musicSkins/musicSkinsSmall/" & pItemPic
	else
		itemImgPath = Server.MapPath("/productpics/thumb") & "\" & pItemPic
		itemImgPath2 = Server.MapPath("/productpics/thumb") & "\" & pItemPic
		useImgPath = "/productpics/thumb/" & pItemPic
	end if
	
	if not fsThumb.FileExists(itemImgPath) then
		if pMusicSkins = 1 then
			if isnull(pMsDefaultImg) then
				pItemPic = "imagena.jpg"
				useImgPath = "/productpics/thumb/imagena.jpg"
			else
				skipRest = 0
				if fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & pMsDefaultImg)) then
					setDefault = pMsDefaultImg
				elseif fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & replace(pMsDefaultImg," ","-"))) then
					setDefault = replace(pMsDefaultImg," ","-")
				else
					setDefault = pMsDefaultImg
				end if
				if skipRest = 0 then
					chkPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & setDefault)
					if not fsThumb.FileExists(chkPath) then
						musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)
						if fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)) then
							musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)
						elseif fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & replace(setDefault," ","-"))) then
							musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & replace(setDefault," ","-"))
						else
							pItemPic = "imagena.jpg"
							useImgPath = "/productpics/thumb/imagena.jpg"
							skipRest = 1
						end if
						if skipRest = 0 then
							session("errorSQL") = musicSkinsDefaultPath
							Jpeg.Open musicSkinsDefaultPath
							Jpeg.Width = 100
							Jpeg.Height = Jpeg.OriginalHeight * 100 / Jpeg.OriginalWidth
							Jpeg.Save chkPath
						end if
					end if
					if skipRest = 0 then
						pItemPic = setDefault
						useImgPath = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & setDefault
					end if
				end if
			end if
		else
			pItemPic = "imagena.jpg"
			useImgPath = "/productpics/thumb/imagena.jpg"
		end if
	elseif pMusicSkins = 1 and not fsThumb.FileExists(itemImgPath2) then
		Jpeg.Open itemImgPath
		if Jpeg.OriginalHeight > Jpeg.OriginalWidth then
			Jpeg.Height = 100
			Jpeg.Width = Jpeg.OriginalWidth * 100 / Jpeg.OriginalHeight
		else
			Jpeg.Width = 100
			Jpeg.Height = Jpeg.OriginalHeight * 100 / Jpeg.OriginalWidth
		end if
		Jpeg.Save itemImgPath2
	end if		
	getUseImage = useImgPath
end function
%>