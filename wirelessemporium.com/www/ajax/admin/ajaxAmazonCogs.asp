<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/Framework/Utility/Static.asp"-->
<%	
call getDBConn(oConn)

partnumber = prepStr(request.QueryString("partnumber"))

sql	=	"select	distinct a.partnumber, a.partnumber_we" & vbcrlf & _
		"	,	coalesce(b.cogs, c.cogs) cogs" & vbcrlf & _
		"	,	case when b.partnumber is not null then b.inv_qty" & vbcrlf & _
		"			when c.musicSkinsID is not null then 999" & vbcrlf & _
		"			else null" & vbcrlf & _
		"		end inv_qty" & vbcrlf & _
		"	,	coalesce(b.price_our, c.price_we) price_we" & vbcrlf & _
		"from	we_amazonProducts a left outer join we_items b" & vbcrlf & _
		"	on	a.partnumber_we = b.partnumber and b.master = 1 left outer join we_items_musicskins c" & vbcrlf & _
		"	on	a.partnumber_we = c.musicSkinsID and c.deleteItem = 0 and c.skip = 0" & vbcrlf & _
		"where	a.partnumber_we like '%" & partnumber & "%'" & vbcrlf & _
		"order by 2 desc, 1"
session("errorSQL") = sql
'response.write "<pre>" & sql & "</pre>"
'response.end
arrCogs = getDbRows(sql)
lap = 0
if not isnull(arrCogs) then
	%>
	<table border="0" cellpadding="3" cellspacing="0" align="center" width="100%" style="font-size:13px;">
		<%
        for i=0 to ubound(arrCogs, 2)
            if isnull(arrCogs(1, i)) then
            %>
            <tr class="grid_row_<%=(i mod 2)%>">
                <td align="right" width="245"><%=arrCogs(0,i)%></td>
                <td align="right" width="175">N/A</td>
                <td align="right" width="120">N/A</td>
                <td align="right" width="130">N/A</td>
                <td align="right" width="120" style="padding-right:20px;">N/A</td>
            </tr>                        
            <%
            else
            %>
            <tr class="grid_row_<%=(i mod 2)%>">
                <td align="right" width="245"><%=replace(arrCogs(0,i), "/", " /<br />")%></td>
                <td align="right" width="175"><%=arrCogs(1,i)%></td>
                <td align="right" width="120">
                <%
                if not isnull(arrCogs(2,i)) and isnumeric(arrCogs(2,i)) then
                    response.write formatcurrency(arrCogs(2,i))
                else
                    response.write arrCogs(2,i)
                end if
                %>
                </td>
                <td align="right" width="130">
                <%
                if not isnull(arrCogs(3,i)) and isnumeric(arrCogs(3,i)) then
                    response.write formatnumber(arrCogs(3,i),0)
                else
                    response.write arrCogs(3,i)
                end if
                %>
                </td>
                <td align="right" width="120" style="padding-right:20px;">
                <%
                if not isnull(arrCogs(4,i)) and isnumeric(arrCogs(4,i)) then
                    response.write formatcurrency(arrCogs(4,i))
                else
                    response.write arrCogs(4,i)
                end if
                %>
                </td>
            </tr>                        
            <%
            end if
        next
        %>
        </table>
	<%
end if

call CloseConn(oConn)
%>