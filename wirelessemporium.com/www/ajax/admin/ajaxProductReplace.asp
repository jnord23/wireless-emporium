<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%

	call getDBConn(oConn)

	brandID = prepInt(request.QueryString("brandID"))
	modelID = prepInt(request.QueryString("modelID"))
	newName = prepStr(request.QueryString("newName"))
	oldName = prepStr(request.QueryString("oldName"))
	
	if newName <> "" then
		sql = "update we_models set modelName = '" & newName & "' where modelID = " & modelID
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "select count(*) as prodCnt from we_Items where itemDesc like '%" & oldName & "%'"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		if prepInt(rs("prodCnt")) = 0 then oldName = replace(oldName," / ","/")
		
		updateFields = 	"WE_URL,itemDesc,itemDesc_PS,itemDesc_CA,itemDesc_CO,itemLongDetail,itemLongDetail_CA,itemLongDetail_CO,itemLongDetail_PS," &_
						"bullet1,bullet2,bullet3,bullet4,bullet5,bullet6,bullet7,bullet8,bullet9,bullet10," &_
						"point1,point2,point3,point4,point5,point6,point7,point8,point9,point10," &_
						"feature1,feature2,feature3,feature4,feature5,feature6,feature7,feature8,feature9,feature10"
		fieldArray = split(updateFields,",")
		
		sql = "update we_items set "
		for i = 0 to ubound(fieldArray)
			if i = 0 then
				sql = sql & fieldArray(i) & " = replace(" & fieldArray(i) & ",'" & formatSEO(oldName) & "','" & formatSEO(newName) & "')"
			else
				sql = sql & "," & fieldArray(i) & " = replace(" & fieldArray(i) & ",'" & oldName & "','" & newName & "')"
			end if
		next
		sql = sql & " where modelID = " & modelID
		session("errorSQL") = sql
		oConn.execute(sql)
		
		response.Write("Product Update Complete")
		call CloseConn(oConn)
		response.End()
	elseif modelID > 0 then
		sql = "select modelID, modelName, (select count(*) from we_items where modelID = " & modelID & ") as itemCnt from we_models where modelID = " & modelID
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
%>
<form method="post" action="#" name="nameChangeForm" onsubmit="return(nameUpdate(<%=modelID%>))">
<div style="float:left; width:350px; margin:10px 0px 10px 0px; border-top:1px solid #000; border-bottom:1px solid #000;">
	<div style="float:left; width:350px; padding-top:5px; font-weight:bold;">Replace:</div>
    <div style="float:left; width:350px; padding-top:5px;"><input type="text" name="currentName" value="<%=rs("modelName")%>" /></div>
    <div style="float:left; width:350px; padding-top:5px; font-weight:bold;">With:</div>
    <div style="float:left; padding-bottom:5px;"><input type="text" name="productName" value="<%=rs("modelName")%>" /></div>
	<div style="float:left; padding:2px 0px 5px 10px;"><input type="submit" name="mySub" value="Update Name" style="font-size:10px;" /></div>
</div>
<br />
<div><%=rs("itemCnt")%> Items to update</div>
</form>
<%
	else
		sql = "select modelID, modelName from we_models where brandID = " & brandID & " order by modelName"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
%>
<div style="width:350px;">
	<div style="float:left;">Select Model:</div>
	<div style="float:left; padding-left:10px;">
    	<select name="model" onchange="modelSelect(this.value)">
        	<option value="">Select Model</option>
	        <%
    	    do while not rs.EOF
        	%>
	        <option value="<%=rs("modelID")%>"><%=rs("modelName")%></option>
    	    <%
        	    rs.movenext
	        loop
    	    %>
	    </select>
    </div>
</div>
<%
	end if
	
	call CloseConn(oConn)
%>