<%
	response.buffer = true
	response.expires = -1
	
	dim thisUser, pageTitle, header
	thisUser = Request.Cookies("admin")("username")
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%

	call getDBConn(oConn)

	ourPN = prepStr(request.QueryString("ourPN"))
	vendorCode = prepStr(request.QueryString("vendorCode"))
	vendorPN = prepStr(request.QueryString("vendorPN"))
	itemID = prepInt(request.QueryString("itemID"))
	lap = prepInt(request.QueryString("lap"))
	weDesc = prepStr(request.QueryString("weDesc"))
	action = prepStr(request.QueryString("action"))
	editID = prepInt(request.QueryString("editID"))
	orderAmt = prepInt(request.QueryString("orderAmt"))
	addVPN = prepStr(request.QueryString("addVPN"))
	vendorReceive = prepInt(request.QueryString("vendorReceive"))
	saveRcvAmt = prepInt(request.QueryString("saveRcvAmt"))
	rcvAmt = prepInt(request.QueryString("rcvAmt"))
	orderID = prepStr(request.QueryString("orderID"))
	
	userMsg = ""
	
	if vendorReceive = 1 then
		sql = "select * from we_vendorNumbers where vendor = '" & vendorCode & "' and we_partNumber = '" & ourPN & "'"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
		
		if addVPN = "" then
			sql = "delete from we_vendorNumbers where vendor = '" & vendorCode & "' and we_partNumber = '" & ourPN & "'"
			sql2 = "update we_vendorOrder set vendorPN = '' where partNumber = '" & ourPN & "' and vendorCode = '" & vendorCode & "'"
		else
			if rs.EOF then
				sql = "insert into we_vendorNumbers (vendor,we_partNumber,partNumber) values('" & vendorCode & "','" & ourPN & "','" & addVPN & "')"
			else
				sql = "update we_vendorNumbers set partNumber = '" & addVPN & "' where vendor = '" & vendorCode & "' and partNumber = '" & ourPN & "'"
			end if
			sql2 = "update we_vendorOrder set vendorPN = '" & addVPN & "' where partNumber = '" & ourPN & "' and vendorCode = '" & vendorCode & "'"
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
		session("errorSQL") = sql2
		oConn.execute(sql2)
		response.Write("item added")
		call CloseConn(oConn)
		response.End()
	elseif saveRcvAmt = 1 then
		sql = "update we_vendorOrder set rowProcessed = 1, receivedAmt = " & rcvAmt & " where orderID = '" & orderID & "' and partNumber = '" & ourPN & "'"
		session("errorSQL") = sql
		oConn.execute(sql)
		response.Write("amount marked")
		call CloseConn(oConn)
		response.End()
	end if
	
	if orderAmt > 0 then
		sql = "select * from we_vendorOrder where vendorCode = '" & session("vendorCode") & "' and partNumber = '" & ourPN & "' and process = 0 and complete = 0"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
		
		if isnull(session("vendorCode")) or len(session("vendorCode")) < 1 then 
			call CloseConn(oConn)
			response.End()
		end if
		
		if rs.EOF then
			sql = "insert into we_vendorOrder (vendorCode,partNumber,vendorPN,orderAmt) values('" & session("vendorCode") & "','" & ourPN & "','" & vendorPN & "'," & orderAmt & ")"
		else
			sql = "update we_vendorOrder set orderAmt = " & orderAmt & " where vendorCode = '" & session("vendorCode") & "' and partNumber = '" & ourPN & "' and process = 0 and complete = 0"
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
		response.Write("item added")
		call CloseConn(oConn)
		response.End()
	end if
	
	if vendorCode <> "" then
		if action = "Delete" then
			sql = "delete from we_vendorNumbers where id = " & editID
		elseif action = "Edit" then
			sql = "update we_vendorNumbers set vendor = '" & vendorCode & "', partNumber = '" & vendorPN & "' where id = " & editID
		else
			sql = "insert into we_vendorNumbers (we_partNumber,vendor,partNumber) values ('" & ourPN & "','" & vendorCode & "','" & vendorPN & "')"
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	sql = "select id, vendor, partNumber from we_vendorNumbers where we_partNumber = '" & ourPN & "'"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 3, 3
	
	displayData = ""
	do while not rs.EOF
		if displayData = "" then displayData = "<a onclick=""editVendorPN(" & rs("id") & ",'" & rs("partNumber") & "','" & rs("vendor") & "'," & lap & "," & itemID & ",'" & ourPN & "','" & ds(replace(weDesc,"'","")) & "')"" style='cursor:pointer; color:#0000ff;'>" & rs("partNumber") & " (" & rs("vendor") & ")</a>" else displayData = displayData & "<br /><a onclick=""editVendorPN(" & rs("id") & ",'" & rs("partNumber") & "','" & rs("vendor") & "'," & lap & "," & itemID & ",'" & ourPN & "','" & ds(replace(weDesc,"'","")) & "')"" style='cursor:pointer; color:#0000ff;'>" & rs("partNumber") & " (" & rs("vendor") & ")</a>"
		rs.movenext
	loop
	if displayData = "" then displayData = "&nbsp;"
	response.Write(displayData)
	
	call CloseConn(oConn)
%>