<%
	response.buffer = true
	response.expires = -1
	server.ScriptTimeout = 4000

%><!--#include virtual="/Framework/Utility/Static.asp"--><%
	
	dim thisUser, pageTitle, header
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.Open Session("ConnectionString")
	
	session("adminID") = prepInt(session("adminID"))
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%	
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim catID : catID = prepInt(request.QueryString("catID"))
	dim orderBy : orderBy = prepStr(request.QueryString("orderBy"))
	dim itemID : itemID = prepInt(request.QueryString("itemID"))
	dim partNumber : partNumber = prepStr(request.QueryString("partNumber"))
	dim vendorPartNumber : vendorPartNumber = prepStr(request.QueryString("vendorPartNumber"))
	dim categoryID : categoryID = prepInt(request.QueryString("categoryID"))
	dim keyword : keyword = prepStr(request.QueryString("keyword"))
	dim updateItemID : updateItemID = prepInt(request.QueryString("updateItemID"))
	dim updateInvQty : updateInvQty = prepInt(request.QueryString("updateInvQty"))
	dim curInvQty : curInvQty = prepInt(request.QueryString("curInvQty"))
	dim showHistory : showHistory = prepInt(request.QueryString("showHistory"))
	dim newProds : newProds = prepInt(request.QueryString("newProds"))
	dim aisPN : aisPN = prepStr(request.QueryString("aisPN"))
	dim aisChecked : aisChecked = prepStr(request.QueryString("aisChecked"))
	dim purch : purch = prepInt(request.QueryString("purch"))
	dim cycleCnt : cycleCnt = prepStr(request.QueryString("cycleCnt"))
	
	if isnull(updateInvQty) or len(updateInvQty) < 1 then updateInvQty = -1
	if isnull(orderBy) or len(orderBy) < 1 then orderBy = "a.itemDesc"
	useSql = ""
	
	if cycleCnt <> "" then
		sql = "exec sp_cycleCount '" & cycleCnt & "'," & prepInt(session("adminID"))
		session("errorSQL") = sql
		set cycleRS = oConn.execute(sql)
		
		response.Write("cycle complete")
		
		response.End()
	end if
	
	if updateItemID > 0 then
		sql = "insert into we_invRecord (itemID,inv_qty,adjustQty,adminID,notes,editDate) values(" & updateItemID & "," & curInvQty & "," & updateInvQty & "," & session("adminID") & ",'WE Employee Adjustment','" & now & "')"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "update we_items set inv_qty = " & updateInvQty & ", ghost = 0 where itemID = " & updateItemID
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "select modelID from we_Items where itemID = " & updateItemID
		session("errorSQL") = sql
		set modelRS = oConn.execute(sql)
		
		if not modelRS.EOF then
			sql = "exec sp_createProductListByModelID " & prepInt(modelRS("modelID"))
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		
		modelRS = null
		
		response.Write("<div style='color=#F00;'>Update Complete</div>")
		response.End()
	elseif aisPN <> "" then
		if aisChecked = "true" then aisChecked = 1 else aisChecked = 0
		sql = "if (select count(*) from we_pnDetails where partnumber = '" & aisPN & "') > 0 update we_pnDetails set alwaysInStock = " & aisChecked & " where partnumber = '" & aisPN & "' else insert into we_pnDetails (partnumber,alwaysInStock) values ('" & aisPN & "'," & aisChecked & ")"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		response.Write("product updated")
		response.End()
	end if
	
	if showHistory > 0 then
		sql = "select top 30 a.*, b.username from we_invRecord a left join we_adminUsers b on a.adminID = b.adminID where a.itemID = " & showHistory & " order by a.id desc"
		sql =	"select distinct top 400 a.*, b.username, e.name as lastVendor, d.orderID as lastOrderID, d.orderAmt as lastOrderAmt, d.receivedAmt as lastReceivedAmt, d.cogs as lastCogs, d.process as lastProcess, d.complete as lastComplete, d.entryDate as lastOrderEntryDate, d.processOrderDate, f.vendorCode " &_
				"from we_invRecord a " &_
					"left join we_adminUsers b on a.adminID = b.adminID " &_
					"left join we_Items c on a.itemID = c.itemID " &_
					"outer apply (select top 1 * from we_vendorOrder where partNumber = c.partNumber order by id desc) d " &_
					"left join we_vendors e on d.vendorCode = e.code " &_
					"left join we_vendorOrder f on a.entryDate = f.completeOrderDate and a.notes like '%vendor%' " &_
				"where a.itemID = " & showHistory & " " &_
				"order by a.id desc"
		session("errorSQL") = sql
		Set invHistoryRS = oConn.execute(sql)
		
		returnValues = ""
		orderData = 0
		do while not invHistoryRS.EOF
			if orderData = 0 and prepStr(invHistoryRS("lastOrderID")) <> "" then
				lastVendor = prepStr(invHistoryRS("lastVendor"))
				lastOrderID = prepStr(invHistoryRS("lastOrderID"))
				lastOrderAmt = prepInt(invHistoryRS("lastOrderAmt"))
				lastReceivedAmt = invHistoryRS("lastReceivedAmt")
				lastCogs = prepInt(invHistoryRS("lastCogs"))
				lastProcess = invHistoryRS("lastProcess")
				lastComplete = invHistoryRS("lastComplete")
				if not lastComplete then lastReceivedAmt = "N/A"
				if invHistoryRS("lastProcess") then
					lastPurchDate = invHistoryRS("processOrderDate")
				else
					lastPurchDate = invHistoryRS("lastOrderEntryDate")
				end if
				orderData = 1
			end if
			useNotes = invHistoryRS("notes")
			if not isnull(invHistoryRS("username")) then useNotes = useNotes & " - " & invHistoryRS("username")
			if cdbl(invHistoryRS("adjustQty")) = 0 then
				adjustBy = 0 - cdbl(invHistoryRS("orderQty"))
				postQty = cdbl(invHistoryRS("inv_qty")) + adjustBy
			else
				if useNotes = "Receive Vendor Order" then
					adjustBy = cdbl(invHistoryRS("adjustQty"))
					postQty = cdbl(invHistoryRS("inv_qty")) + adjustBy
				else
					adjustBy = cdbl(invHistoryRS("adjustQty")) - cdbl(invHistoryRS("inv_qty"))
					postQty = cdbl(invHistoryRS("adjustQty"))
				end if
			end if
			
			if instr(useNotes,"Cancel") > 0 and adjustBy < 0 then
				adjustBy = adjustBy * -1
				postQty = invHistoryRS("inv_qty") + adjustBy
			end if
			returnValues = returnValues & invHistoryRS("inv_qty") & "%" & adjustBy & "%" & postQty & "%" & invHistoryRS("orderID") & "%" & useNotes & "%" & invHistoryRS("entryDate") & "%" & invHistoryRS("vendorCode") & "#"
			invHistoryRS.movenext
		loop
		if returnValues = "" then
			redim preserve returnValuesArray(-1)
		else
			returnValues = left(returnValues,len(returnValues)-1)
			returnValuesArray = split(returnValues,"#")
		end if
		%>
        <div style="display:table; width:500px; border:1px solid #000; padding:3px;">
        	<div style="display:table; background-color:#FFC; width:100%;">
				<% if orderData = 1 then %>
                <div style="display:table; border-bottom:1px solid #000;">
                    <div style="float:left; width:150px;">Order# <%=lastOrderID%></div>
                    <div style="float:left; width:75px; border-left:1px solid #000;">Ordered</div>
                    <div style="float:left; width:75px; border-left:1px solid #000;">Received</div>
                    <div style="float:left; width:75px; border-left:1px solid #000;">Cogs</div>
                    <div style="float:left; width:100px; border-left:1px solid #000;">Date</div>
                </div>
                <div style="display:table;">
                    <% if not lastProcess then %>
                    <div style="float:left; width:150px; color:#900; font-weight:bold;">Building Order<br />(<%=lastVendor%>)</div>
                    <% elseif not lastComplete then %>
                    <div style="float:left; width:150px; color:#009; font-weight:bold;">Order Placed<br />(<%=lastVendor%>)</div>
                    <% else %>
                    <div style="float:left; width:150px; color:#060; font-weight:bold;">Order Received<br />(<%=lastVendor%>)</div>
                    <% end if %>
                    <div style="float:left; width:75px; border-left:1px solid #000;"><%=prepInt(lastOrderAmt)%></div>
                    <div style="float:left; width:75px; border-left:1px solid #000;"><%=lastReceivedAmt%></div>
                    <div style="float:left; width:75px; border-left:1px solid #000;"><%=formatCurrency(lastCogs,2)%></div>
                    <div style="float:left; width:100px; border-left:1px solid #000;"><%=dateOnly(lastPurchDate)%></div>
                </div>
                <% else %>
                <div style="display:table; font-weight:bold;">No Order Data Found</div>
                <% end if %>
            </div>
            <div style="display:table; background-color:#333; color:#FFF; font-weight:bold; font-size:11px; width:100%;">
            	<div style="float:left; width:16px; padding-left:5px;">Pre</div>
                <div style="float:left; width:35px; padding-left:5px;">Adjust</div>
                <div style="float:left; width:21px; padding-left:5px;">Post</div>
                <div style="float:left; width:55px; padding-left:5px;">OrderID</div>
                <div style="float:left; width:190px; padding-left:5px;">Notes</div>
                <div style="float:left; width:130px; padding-left:5px;">Date</div>
            </div>
            <div id="scrollBox_<%=showHistory%>" style="float:left; width:100%; max-height:450px; overflow:auto;">
				<%
                bgColor = "#ffffff"
                curRow = ubound(returnValuesArray)
                saveBgColor = ""
                fontColor = "#000"
                for i = 0 to ubound(returnValuesArray)
                    session("errorSQL") = "1:" & curRow & "(" & i & ")"
                    curArray = split(returnValuesArray(curRow),"%")
                    curRow = curRow - 1
					useNotes = curArray(4)
                    if instr(useNotes,"WE Employee Adjust") > 0 then
                        saveBgColor = bgColor
                        bgColor = "#990000"
                        fontColor = "#fff"
					elseif instr(useNotes,"CYCLE COUNT") > 0 then
                        saveBgColor = bgColor
                        bgColor = "#0087d3"
                        fontColor = "#fff"
                    elseif instr(useNotes,"Receive Vendor") > 0 then
                        saveBgColor = bgColor
                        bgColor = "#006600"
                        fontColor = "#fff"
						useNotes = "Vendor Receive (" & curArray(6) & ")"
                    end if
                %>
                <div style="float:left; background-color:<%=bgColor%>; color:<%=fontColor%>; font-size:11px; width:100%;">
                    <div style="float:left; width:16px; padding-left:5px; text-align:right;"><%=curArray(0)%></div>
                    <div style="float:left; width:35px; padding-left:5px; text-align:right;"><%=curArray(1)%></div>
                    <div style="float:left; width:21px; padding-left:5px; text-align:right;"><%=curArray(2)%></div>
                    <div style="float:left; width:55px; padding-left:5px; text-align:right;"><%=curArray(3)%></div>
                    <div style="float:left; width:190px; padding-left:5px;"><%=useNotes%></div>
                    <div style="float:left; width:130px; padding-left:5px;"><%=curArray(5)%></div>
                </div>
                <%
                    if saveBgColor <> "" then
                        bgColor = saveBgColor
                        saveBgColor = ""
                    end if
                    fontColor = "#000"
                    if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
                next
                %>
            </div>
            <div style="display:table; text-align:right; padding-right:10px; background-color:#333; color:#FFF; font-weight:bold; font-size:11px; width:490px;"><a onclick="displayInvHistory(<%=showHistory%>)" style="color:#fff; cursor:pointer; text-decoration:underline; font-size:11px">Close History</a></div>
        </div>
		<%
		oConn.Close
		oConn = null
		response.End()
	end if
	
	if newProds > 0 then
		useSql = 	"select a.dateTimeEntd as entryDate, e.bunker, d.partnumber pNotes, c.vendor as vCode, c.partNumber as vendorPN, a.*, ( " &_
						"select top 1 ib.fname + ' ' + ib.lname + '<br />' + cast(ia.editDate as varchar(50)) " &_
						"from we_invRecord ia " &_
							"left join we_adminUsers ib on ib.adminID = ia.adminID " &_
						"where ia.itemID = a.itemID " &_
						"order by ia.entryDate desc " &_
					") as lastEdit, ( " &_
						"select top 1 notes " &_
						"from we_invRecord " &_
						"where itemID = a.itemID " &_
						"order by entryDate desc " &_
					") as notes, ( " &_
						"select alwaysInStock " &_
						"from we_pnDetails " &_
						"where partnumber = a.partnumber " &_
					") as alwaysInStock " &_
					", i.partNumber as iPartNumber, i.cogs as iCogs, i.quantity as iQuantity, u.fname, u.lname, u.adminID " &_
					"from we_items a " &_
						"left join we_vendorNumbers c on a.partNumber = c.we_partNumber " &_
						"left join we_partnumberNotes d on a.partnumber = d.partnumber " &_
						"left join we_ItemsExtendedData e on a.partnumber = e.partnumber " &_
						"left join we_Items_Initial i on a.itemid = i.itemId " & vbcrlf &_
						"left join we_AdminUsers u on i.adminId = u.adminID " & vbcrlf &_
					"where DateTimeEntd > '" & date - 3 & "' and master = 1 " &_
					"order by a.itemID"
	elseif itemID > 0 then
		useSql = 	"select a.dateTimeEntd as entryDate, e.bunker, d.partnumber pNotes, c.vendor as vCode, c.partNumber as vendorPN, b.*, (" &_
						"select top 1 ib.fname + ' ' + ib.lname + '<br />' + cast(ia.editDate as varchar(50)) " &_
						"from we_invRecord ia " &_
							"left join we_adminUsers ib on ib.adminID = ia.adminID " &_
						"where ia.itemID = b.itemID " &_
						"order by ia.entryDate desc" &_
					") as lastEdit, (" &_
						"select top 1 notes " &_
						"from we_invRecord " &_
						"where itemID = b.itemID " &_
						"order by entryDate desc" &_
					") as notes, (" &_
						"select alwaysInStock " &_
						"from we_pnDetails " &_
						"where partnumber = a.partnumber" &_
					") as alwaysInStock " &_
					", i.partNumber as iPartNumber, i.cogs as iCogs, i.quantity as iQuantity, u.fname, u.lname, u.adminID " &_
					"from we_items a " &_
						"left join we_items b on b.partNumber = a.partNumber and b.master = 1 " &_
						"left join we_vendorNumbers c on a.partNumber = c.we_partNumber " &_
						"left join we_partnumberNotes d on a.partnumber = d.partnumber " &_
						"left join we_ItemsExtendedData e on a.partnumber = e.partnumber " &_
						"left join we_Items_Initial i on a.itemid = i.itemId " & vbcrlf &_
						"left join we_AdminUsers u on i.adminId = u.adminID " & vbcrlf &_
					"where a.itemID = " & itemID
	elseif partNumber <> "" then
		partNumber = replace(partNumber,"%","")
		useSql = 	"select a.dateTimeEntd as entryDate, e.bunker, d.partnumber pNotes, c.vendor as vCode, c.partNumber as vendorPN, a.*, (" & vbcrlf & _
						"select top 1 ib.fname + ' ' + ib.lname + '<br />' + cast(ia.editDate as varchar(50)) " & vbcrlf & _
						"from we_invRecord ia " & vbcrlf & _
							"left join we_adminUsers ib on ib.adminID = ia.adminID " & vbcrlf & _
						"where ia.itemID = a.itemID " & vbcrlf & _
						"order by ia.entryDate desc" & vbcrlf & _
					") as lastEdit, (" & vbcrlf & _
						"select top 1 notes " & vbcrlf & _
						"from we_invRecord " & vbcrlf & _
						"where itemID = a.itemID " & vbcrlf & _
						"order by entryDate desc" & vbcrlf & _
					") as notes, (" & vbcrlf & _
						"select alwaysInStock " & vbcrlf & _
						"from we_pnDetails " & vbcrlf & _
						"where partnumber = a.partnumber" & vbcrlf & _
					") as alwaysInStock " & vbcrlf & _
					", i.partNumber as iPartNumber, i.cogs as iCogs, i.quantity as iQuantity, u.fname, u.lname, u.adminID " & vbcrlf & _
					"from we_items a " & vbcrlf & _
						"left join we_vendorNumbers c on a.partNumber = c.we_partNumber " & vbcrlf & _
						"left join we_partnumberNotes d on a.partnumber = d.partnumber " & vbcrlf & _
						"left join we_ItemsExtendedData e on a.partnumber = e.partnumber " & vbcrlf & _
						"left join we_Items_Initial i on a.itemid = i.itemId " & vbcrlf &_
						"left join we_AdminUsers u on i.adminId = u.adminID " & vbcrlf &_
					"where a.partNumber like '%" & partNumber & "%' and master = 1 " & vbcrlf & _
					"order by a.itemID"
	elseif vendorPartNumber <> "" then
		vendorPartNumber = replace(vendorPartNumber,"%","")
		useSql = 	"select a.dateTimeEntd as entryDate, e.bunker, d.partnumber pNotes, c.vendor as vCode, c.partNumber as vendorPN, a.*, (" &_
						"select top 1 ib.fname + ' ' + ib.lname + '<br />' + cast(ia.editDate as varchar(50)) " &_
						"from we_invRecord ia " &_
							"left join we_adminUsers ib on ib.adminID = ia.adminID " &_
						"where ia.itemID = a.itemID " &_
						"order by ia.entryDate desc" &_
					") as lastEdit, (" &_
						"select top 1 notes " &_
						"from we_invRecord " &_
						"where itemID = a.itemID " &_
						"order by entryDate desc" &_
					") as notes, (" &_
						"select alwaysInStock " &_
						"from we_pnDetails " &_
						"where partnumber = a.partnumber" &_
					") as alwaysInStock " &_
					", i.partNumber as iPartNumber, i.cogs as iCogs, i.quantity as iQuantity, u.fname, u.lname, u.adminID " &_
					"from we_items a " &_
						"left join we_vendorNumbers c on a.partNumber = c.we_partNumber " &_
						"left outer join we_partnumberNotes d on a.partnumber = d.partnumber " &_
						"left join we_ItemsExtendedData e on a.partnumber = e.partnumber " &_
						"left join we_Items_Initial i on a.itemid = i.itemId " & vbcrlf &_
						"left join we_AdminUsers u on i.adminId = u.adminID " & vbcrlf &_
					"where c.partNumber like '%" & vendorPartNumber & "%' and master = 1 " &_
					"order by a.itemID"
	elseif categoryID > 0 then
		useSql = 	"select a.dateTimeEntd as entryDate, e.bunker, d.partnumber pNotes, c.vendor as vCode, c.partNumber as vendorPN, *, (" &_
						"select top 1 ib.fname + ' ' + ib.lname + '<br />' + cast(ia.editDate as varchar(50)) " &_
						"from we_invRecord ia " &_
							"left join we_adminUsers ib on ib.adminID = ia.adminID " &_
						"where ia.itemID = a.itemID " &_
						"order by ia.entryDate desc" &_
					") as lastEdit, (" &_
						"select top 1 notes " &_
						"from we_invRecord " &_
						"where itemID = a.itemID " &_
						"order by entryDate desc" &_
					") as notes, (" &_
						"select alwaysInStock " &_
						"from we_pnDetails " &_
						"where partnumber = a.partnumber" &_
					") as alwaysInStock " &_
					", i.partNumber as iPartNumber, i.cogs as iCogs, i.quantity as iQuantity, u.fname, u.lname, u.adminID " &_
					"from we_items a " &_
						"left join we_vendorNumbers c on a.partNumber = c.we_partNumber " &_
						"left outer join we_partnumberNotes d on a.partnumber = d.partnumber " &_
						"left join we_ItemsExtendedData e on a.partnumber = e.partnumber " &_
						"left join we_Items_Initial i on a.itemid = i.itemId " & vbcrlf &_
						"left join we_AdminUsers u on i.adminId = u.adminID " & vbcrlf &_
					"where typeID = " & categoryID & " and master = 1 " &_
					"order by a.itemID"
	elseif keyword <> "" then
		keyword = replace(keyword,"%","")
		useSql = 	"select a.dateTimeEntd as entryDate, e.bunker, d.partnumber pNotes, c.vendor as vCode, c.partNumber as vendorPN, a.*, (" &_
						"select top 1 ib.fname + ' ' + ib.lname + '<br />' + cast(ia.editDate as varchar(50)) " &_
						"from we_invRecord ia " &_
							"left join we_adminUsers ib on ib.adminID = ia.adminID " &_
						"where ia.itemID = a.itemID " &_
						"order by ia.entryDate desc" &_
					") as lastEdit, (" &_
						"select top 1 notes " &_
						"from we_invRecord " &_
						"where itemID = a.itemID " &_
						"order by entryDate desc" &_
					") as notes, (" &_
						"select alwaysInStock " &_
						"from we_pnDetails " &_
						"where partnumber = a.partnumber" &_
					") as alwaysInStock " &_
					", i.partNumber as iPartNumber, i.cogs as iCogs, i.quantity as iQuantity, u.fname, u.lname, u.adminID " &_
					"from we_items a " &_
						"left join we_vendorNumbers c on a.partNumber = c.we_partNumber " &_
						"left outer join we_partnumberNotes d on a.partnumber = d.partnumber " &_
						"left join we_ItemsExtendedData e on a.partnumber = e.partnumber " &_
						"left join we_Items_Initial i on a.itemid = i.itemId " & vbcrlf &_
						"left join we_AdminUsers u on i.adminId = u.adminID " & vbcrlf &_
					"where itemDesc like '%" & keyword & "%' and master = 1 " &_
					"order by a.itemID"
	elseif catID > 0 then
		if isnull(session("orderBy")) then
			session("orderBy") = orderBy
		else
			if orderBy = session("orderBy") then
				if instr(session("orderBy"),"desc") > 0 then
					session("orderBy") = orderBy
				else
					session("orderBy") = orderBy & " desc"
				end if
			else
				session("orderBy") = orderBy
			end if
		end if
		useSql = 	"select a.dateTimeEntd as entryDate, e.bunker, d.partnumber pNotes, c.vendor as vCode, c.partNumber as vendorPN, a.vendor, a.partNumber, a.itemID, a.itemPic, a.itemDesc, a.inv_qty, a.price_retail, a.price_our, a.price_co, a.price_ca, a.price_ps, a.price_er, a.cogs, (" &_
						"select top 1 ib.fname + ' ' + ib.lname + '<br />' + cast(ia.editDate as varchar(50)) " &_
						"from we_invRecord ia " &_
							"left join we_adminUsers ib on ib.adminID = ia.adminID " &_
						"where ia.itemID = a.itemID " &_
						"order by ia.entryDate desc" &_
					") as lastEdit, (" &_
						"select top 1 notes " &_
						"from we_invRecord " &_
						"where itemID = a.itemID " &_
						"order by entryDate desc" &_
					") as notes, (" &_
						"select alwaysInStock " &_
						"from we_pnDetails " &_
						"where partnumber = a.partnumber" &_
					") as alwaysInStock " &_
					", i.partNumber as iPartNumber, i.cogs as iCogs, i.quantity as iQuantity, u.fname, u.lname, u.adminID " &_
					"from we_items a " &_
						"left join we_vendorNumbers c on a.partNumber = c.we_partNumber " &_
						"left outer join we_partnumberNotes d on a.partnumber = d.partnumber " &_
						"left join we_ItemsExtendedData e on a.partnumber = e.partnumber " &_
						"left join we_Items_Initial i on a.itemid = i.itemId " & vbcrlf &_
						"left join we_AdminUsers u on i.adminId = u.adminID " & vbcrlf &_
					"where master = 1 and typeID = " & catID & " and modelID = " & modelID & " " &_
					"order by " & session("orderBy")
	end if
	
	if useSql <> "" then
		sql = useSql
		session("errorSQL") = sql
		Set itemRS = oConn.execute(sql)
%>
<div style="float:left; width:600px;">
	<table border="0" cellpadding="3" cellspacing="0" width="100%">
		<tr bgcolor="#000000" style="font-weight:bold; color:#FFF; font-size:11px;">
        	<td style="border-right:1px solid #FFF; cursor:pointer;">Pic/ItemID</td>
            <td style="border-right:1px solid #FFF; cursor:pointer;" width="100%">Desc</td>
            <td style="border-right:1px solid #000; cursor:pointer;">Partnumber</td>
        </tr>
		<%
		bgColor = "#ffffff"
		lap = 0
		do while not itemRS.EOF
			lap = lap + 1
			entryDate = dateOnly(itemRS("entryDate"))
			bunker = itemRS("bunker")
			pNotes = itemRS("pNotes")
			if isnumeric(itemRS("cogs")) then cogs = formatCurrency(itemRS("cogs"),2) else cogs = "NA"
			if isnumeric(itemRS("price_our")) then wePrice = formatCurrency(itemRS("price_our"),2) else wePrice = "NA"
			if isnumeric(itemRS("price_co")) then coPrice = formatCurrency(itemRS("price_co"),2) else coPrice = "NA"
			if isnumeric(itemRS("price_ca")) then caPrice = formatCurrency(itemRS("price_ca"),2) else caPrice = "NA"
			if isnumeric(itemRS("price_ps")) then psPrice = formatCurrency(itemRS("price_ps"),2) else psPrice = "NA"
			if isnumeric(itemRS("price_er")) then erPrice = formatCurrency(itemRS("price_er"),2) else erPrice = "NA"
			if not isnull(itemRS("lastEdit")) then useLastEdit = itemRS("lastEdit") else useLastEdit = "&nbsp;"
			if not isnull(itemRS("notes")) then useNotes = itemRS("notes") else useNotes = "NA"
			if not isnull(itemRS("alwaysInStock")) then alwaysInStock = itemRS("alwaysInStock") else alwaysInStock = false
			itemID = itemRS("itemID")
			itemPic = itemRS("itemPic")
			itemDesc = itemRS("itemDesc")
			itemPN = itemRS("partNumber")
			itemInvQty = itemRS("inv_qty")
			ovc = prepStr(itemRS("vendor"))
			vendorCodes = ""
			initialPartNumber = itemRS("iPartNumber")
			initialCogs = itemRS("iCogs")
			initialQuantity = itemRS("iQuantity")
			initialAdminName = itemRS("fname") & " " & itemRS("lname")
			initialAdminId = itemRS("adminID")
			do while itemPN = itemRS("partNumber")
				if not isnull(itemRS("vCode")) then vendorCodes = vendorCodes & itemRS("vCode") & " (" & itemRS("vendorPN") & "),"
				itemRS.movenext
				if itemRS.EOF then exit do
			loop
			if vendorCodes <> "" then
				vendorCodes = left(vendorCodes,len(vendorCodes)-1)
			else
				if ovc = "" then
					vendorCodes = "No Vendor Code Information Available"
				else
					vendorCodes = ovc
				end if
			end if
			
			'commented by Terry on 12/21/2012 because Merch could not change cogs.
'			sql = "select top 1 cogs from we_vendorOrder where partNumber = '" & itemPN & "' order by id desc"
'			session("errorSQL") = sql
'			set cogsRS = oConn.execute(sql)
'			
'			if not cogsRS.EOF then
'				latestCogs = prepInt(cogsRS("cogs"))
'				if latestCogs <> cogs then
'					sql = "update we_Items set cogs = '" & latestCogs & "' where partNumber = '" & itemPN & "'"
'					session("errorSQL") = sql
'					oConn.execute(sql)		
'					cogs = formatCurrency(latestCogs,2)
'				end if
'			end if
		%>
    	<tr bgcolor="<%=bgColor%>" style="font-size:11px;">
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;" rowspan="15" valign="top">
				<div style="width:100px;"><img src="/productpics/thumb/<%=itemPic%>" /></div>
				<div style="width:100px; text-align:center; font-weight:bold; padding-top:5px;">ItemID: <%=itemID%></div>
                <div style="width:100px; padding-top:10px; text-align:center;"><a href="#topTitle">Back To Top</a></div>
            </td>
            <td style="border-right:1px solid #000; border-bottom:1px solid #000;"><%=itemDesc%></td>
            <td style="border-bottom:1px solid #000;" nowrap="nowrap"><%=itemPN%></td>
        </tr>
        <tr bgcolor="#000000" style="font-weight:bold; color:#FFF; font-size:11px;">
        	<td style="border-right:1px solid #FFF; cursor:pointer;">Last Edit</td>
            <td style="border-right:1px solid #000; cursor:pointer;">Qty</td>
        </tr>
        <tr bgcolor="<%=bgColor%>" style="font-size:11px;">
        	<td style="border-right:1px solid #000; border-bottom:1px solid #000;"><%=useLastEdit%></td>
            <td style="border-bottom:1px solid #000;" valign="top" align="center"><input type="text" name="invQty" value="<%=itemInvQty%>" size="4" onchange="updateInv(<%=itemID%>,this.value,<%=itemInvQty%>)" /></td>
        </tr>
        <tr bgcolor="#000000" style="font-weight:bold; color:#FFF; font-size:11px;">
        	<td align="left" style="border-right:1px solid #FFF;">Other Links</td>
            <td align="left">COGS</td>
        </tr>
        <tr bgcolor="<%=bgColor%>" style="font-size:11px;">
        	<td align="left" style="border-right:1px solid #000; border-bottom:1px solid #000;">
            	<a href="/admin/vendorPartNumbers_enter.asp?partNumber=<%=itemPN%>" style="color:#00F; text-decoration:underline;" target="productLink">Enter Vendor Data</a>, 
                <a href="/admin/db_update_items.asp?itemID=<%=itemID%>" style="color:#00F; text-decoration:underline;" target="productLink">Edit Product</a>, 
                <a href="/admin/db_update_price.asp?itemID=<%=itemID%>" style="color:#00F; text-decoration:underline;" target="productLink">Edit Price</a>
            </td>
            <td align="right">
				<%=cogs%>
			</td>
        </tr>
        <tr bgcolor="#000000" style="font-weight:bold; color:#FFF; font-size:11px;">
        	<td align="left" style="border-right:1px solid #FFF;">Vendor Codes</td>
            <td align="left">Create Date</td>
        </tr>
        <tr bgcolor="<%=bgColor%>" style="font-size:11px; border-bottom:1px solid #000;">
        	<td style="border-right:1px solid #000; border-bottom:1px solid #000;"><%=vendorCodes%></td>
            <td><%=entryDate%></td>
        </tr>
        <tr bgcolor="#000000" style="font-weight:bold; color:#FFF; font-size:11px;">
            <td style="border-right:1px solid #FFF; cursor:pointer;">Product Notes</td>
            <td style="border-right:1px solid #000; cursor:pointer;">Always In Stock</td>
        </tr>
        <tr bgcolor="<%=bgColor%>">
            <td align="left" style="border-right:1px solid #000; border-bottom:1px solid #000;">
            	<div style="float:left;">
                	<a href="/admin/productNotes.asp?partNumber=<%=itemPN%>" target="_blank">
                		<% if not isnull(pNotes) then %>
	                		<img src="/images/Notes.gif" border="0" />
	                    <% else %>
		                	<img src="/images/Notes_none.gif" border="0" />
        	            <% end if %>
					</a>
                </div>
                <% if bunker then %>
                <div style="float:left; padding-left:10px;"><img src="/images/icons/bunker2.png" border="0" height="25" /></div>
                <% end if %>
            </td>
            <td align="center" style="border-bottom:1px solid #000;">
				<input type="checkbox" name="alwaysInStock" value="1" onclick="permStock('<%=itemPN%>',this.checked)"<% if alwaysInStock then %> checked="checked"<% end if %> />
			</td>
        </tr>
        <tr bgcolor="#000000" style="font-weight:bold; color:#FFF; font-size:11px;">
            <td style="border-right:1px solid #FFF; cursor:pointer;">Inventory Notes</td>
            <td style="border-right:1px solid #000; cursor:pointer;">Action</td>
        </tr>
        <tr bgcolor="<%=bgColor%>" style="font-size:11px;">
        	<td style="border-right:1px solid #000; border-bottom:1px solid #000;">
				<%=useNotes%><% if useNotes <> "NA" then %> <a onclick="displayInvHistory(<%=itemID%>)" style="cursor:pointer; color:#00F; text-decoration:underline;">(view details)</a><% end if %>
                <div id="invHistory_<%=itemID%>" style="position:relative;"></div>
            </td>
            <td style="border-bottom:1px solid #000;" align="center" valign="top">
            	<div id="<%=itemID%>_return" style="margin-bottom:10px;"><input type="button" name="myAction" value="Update Qty" style="font-size:12px;" /></div>
                <div><input type="button" name="myAction" value="Cycle Count" style="font-size:12px;" onclick="cycleCount('<%=itemPN%>')" /></div>
            </td>
        </tr>
        <tr bgcolor="#000000" style="font-weight:bold; color:#FFF; font-size:11px;">
            <td style="border-right:1px solid #FFF; cursor:pointer;">Initial Part Number</td>
            <td style="border-right:1px solid #000; cursor:pointer;">Initial Qty</td>
        </tr>
        
        <tr bgcolor="<%=bgColor%>" style="font-size:11px;">
        	<td style="border-right:1px solid #000; border-bottom:1px solid #000;">
            	<%=initialPartNumber%>
            </td>
            <td style="border-bottom:1px solid #000;" align="center" valign="top">
            	<%=initialQuantity%>
            </td>
        </tr>
        
        <tr bgcolor="#000000" style="font-weight:bold; color:#FFF; font-size:11px;">
            <td style="border-right:1px solid #FFF; cursor:pointer;">Initially Entered By</td>
            <td style="border-right:1px solid #000; cursor:pointer;">Initial COGS</td>
        </tr>
       
        <tr bgcolor="<%=bgColor%>" style="font-size:11px;">
        	<td style="border-right:1px solid #000; border-bottom:1px solid #000;">
            	<%=initialAdminName%>
            </td>
            <td style="border-bottom:1px solid #000;" align="center" valign="top">
            	<% if initialCogs > 0 then initialCogs = formatcurrency(initialCogs) end if %>
				<%=initialCogs%>
            </td>
        </tr>

	    <%
			if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
			if lap > 200 then 
				response.flush
				lap = 0
			end if
		loop
		%>
    </table>
</div>
<%
	elseif modelID > 0 then		
		sql = "select typeID, typeName from we_types order by typeName"
		session("errorSQL") = sql
		Set catRS = Server.CreateObject("ADODB.Recordset")
		catRS.open sql, oConn, 0, 1
%>
<div>
	<div style="float:left; width:140px;">Select Category:</div>
	<div style="float:left; padding-left:10px; width:450px;">
    	<select name="model" onchange="catSelect(this.value,<%=modelID%>)">
        	<option value="">Select All Categories</option>
	        <%
    	    do while not catRS.EOF
        	%>
	        <option value="<%=catRS("typeID")%>"<% if cdbl(catID) = cdbl(catRS("typeID")) then %> selected="selected"<% end if %>><%=catRS("typeName")%></option>
    	    <%
        	    catRS.movenext
	        loop
    	    %>
	    </select>
    </div>
</div>
<%
	else
		sql = "select modelID, modelName from we_models where brandID = " & brandID & " order by modelName"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
%>
<div style="width:600px;">
	<div style="float:left; width:140px;">Select Model:</div>
	<div style="float:left; padding-left:10px; width:450px;">
        <select name="model" onchange="modelSelect(this.value)">
        	<option value="">Select Model</option>
	        <%
    	    do while not rs.EOF
        	%>
	        <option value="<%=rs("modelID")%>"><%=rs("modelName")%></option>
    	    <%
        	    rs.movenext
	        loop
    	    %>
	    </select>
    </div>
</div>
<%
	end if
	
	oConn.Close
	set oConn = nothing
%>