<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	
	call getDBConn(oConn)	
	dim thisUser, pageTitle, header
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
	dim taskID : taskID = prepInt(request.QueryString("taskID"))
	dim taskStatus : taskStatus = prepInt(request.QueryString("taskStatus"))
	dim taskDelete : taskDelete = prepStr(request.QueryString("taskDelete"))
	
	if taskID > 0 then
		if taskDelete = "true" then taskDelete = true else taskDelete = false
		if taskStatus = 10 then
			dateCompleted = "'" & now & "'"
			
			sql = "select shovelReady, sortValue, title, details, site, b.email as enteredBy, c.fname + ' ' + c.lname as assignedName from we_Website_Tasks a join we_adminUsers b on a.enteredBy = b.adminID join we_adminUsers c on a.assignedTo = c.adminID where a.id = " & taskID
			session("errorSQL") = sql
			set emailRS = oConn.execute(sql)
			
			shovelReady = emailRS("shovelReady")
			sortValue = emailRS("sortValue")
			title = emailRS("title")
			details = emailRS("details")
			site = emailRS("site")
			EnteredBy = emailRS("enteredBy")
			AssignedName = emailRS("assignedName")
			
			if not isnull(sortValue) then
				sql = "update we_Website_Tasks set shovelReady = 0, sortValue = null where id = " & taskID
				oConn.execute(sql)
				
				sql = "update we_Website_Tasks set sortValue = sortValue - 1 where shovelReady = 1 and sortValue is not null and sortValue > " & sortValue
				oConn.execute(sql)
			end if
			
			cdo_to = emailList("Project Complete")
			if instr(cdo_to, EnteredBy) = 0 then cdo_to = cdo_to & "," & EnteredBy
				
			cdo_from = "sales@WirelessEmporium.com"
			cdo_subject = "A Project Has Been Completed - " & title
			cdo_body = "<p>Completed Project #" & taskID & "</p>" & vbcrlf
			cdo_body = cdo_body & "<p>Title:<br>" & title & "</p>" & vbcrlf
			cdo_body = cdo_body & "<p>Details:<br>" & replace(details,vbcrlf,"<br>") & "', " & "</p>" & vbcrlf
			cdo_body = cdo_body & "<p>Site: " & site & "</p>" & vbcrlf
			cdo_body = cdo_body & "<p>Date Completed: " & now & "</p>" & vbcrlf
			cdo_body = cdo_body & "<p>Assigned To: " & AssignedName & "</p>" & vbcrlf
						
			'CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		else
			dateCompleted = "null"
		end if
		
		SQL = "UPDATE we_Website_Tasks SET "
		if taskDelete then
			SQL = SQL & "deleted = 1 "
		else
			SQL = SQL & "status = '" & taskStatus & "', "
			SQL = SQL & "DateCompleted = " & dateCompleted
		end if
		SQL = SQL & " WHERE id='" & taskID & "'"
		oConn.execute SQL
		
		response.Write(SQL)
		response.write "<h3>TASK #" & taskID & " UPDATED!</h3>"
		call CloseConn(oConn)
		response.End()
	end if

	call CloseConn(oConn)
%>