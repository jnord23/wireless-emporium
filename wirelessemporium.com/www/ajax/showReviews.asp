<%
	response.buffer = true
	response.expires = -1

	function getRatingStar(rating)
		dim nRating 
		dim strRatingImg
		nRating = cint(rating)
		strRatingImg = 	"<img src=""/images/product/review_star_empty.jpg"" border=""0"" width=""14"" height=""14"" />" & vbcrlf & _
						"<img src=""/images/product/review_star_empty.jpg"" border=""0"" width=""14"" height=""14"" />" & vbcrlf & _
						"<img src=""/images/product/review_star_empty.jpg"" border=""0"" width=""14"" height=""14"" />" & vbcrlf & _
						"<img src=""/images/product/review_star_empty.jpg"" border=""0"" width=""14"" height=""14"" />" & vbcrlf & _
						"<img src=""/images/product/review_star_empty.jpg"" border=""0"" width=""14"" height=""14"" />"
		
		if nRating > 0 or nRating <=5 then
			strRatingImg = ""
			for i=1 to 5
				if i <= nRating then 
					strRatingImg = strRatingImg & "<img src=""/images/product/review_star_full.jpg"" border=""0"" width=""14"" height=""14"" />"
				elseif ((i - 1) + .5) <= nRating then 
					strRatingImg = strRatingImg & "<img src=""/images/product/review_star_half.jpg"" border=""0"" width=""14"" height=""14"" />"
				else
					strRatingImg = strRatingImg & "<img src=""/images/product/review_star_empty.jpg"" border=""0"" width=""14"" height=""14"" />"
				end if	
			next	
		end if
		
		getRatingStar = strRatingImg
	end function
%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	
	call getDBConn(oConn)
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<div style="width:95%; padding-left:15px; padding-right:15px; ">
<%
	itemID = prepInt(request.QueryString("itemID"))
	partNumber = prepStr(request.QueryString("partNumber"))
	
    'MG 2013.03.13 Task 1634 -- Display review accross simliar products.
	sql = "EXEC sp_GetFullReview  " & itemID 
	'sql = "select rating, nickname, reviewTitle, convert(varchar(10), datetimeentd, 20) datetimeentd, review from we_Reviews where (itemID = " & itemID & " or PartNumbers like '%" & partNumber & "%') and approved = 1"
	session("errorSQL") = sql
	Set rs = oConn.execute(sql)
	
	if not rs.eof then
		lap = 0
		do while not rs.EOF
			lap = lap + 1		
			addlStyle = "border-top:1px solid #ccc;"
			if lap = 1 then
				addlStyle = ""
			end if

			if lap > 3 then
				addlStyle = addlStyle & " display:none;"
			end if
			%>
			<div id="id_childReview_<%=lap%>" style="float:left; width:100%; padding-bottom:15px; <%=addlStyle%>">
				<div style="color:#666; width:710px; padding-top:10px;" class="smlText"><%=rs("nickname")%>&nbsp;&nbsp;|&nbsp;&nbsp;<%=rs("datetimeentd")%></div>
				<div style="font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; width:710px; padding-top:10px;"><%=rs("reviewTitle")%></div>
				<div style="width:710px;"><%=getRatingStar(rs("rating"))%></div>
				<div style="width:710px; padding-top:10px; font-size:11px; font-family:Arial, Helvetica, sans-serif;"><%=rs("review")%></div>
			</div>
			<%
			rs.movenext
		loop
	end if
	
	call CloseConn(oConn)	
%>
</div>