<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/includes/asp/inc_dbConn.asp"--><%
%><!--#include virtual="/Framework/Utility/Static.asp"--><%
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%

	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim selectName : selectName = prepStr(request.QueryString("selectName"))
	dim maxSize : maxSize = prepInt(request.QueryString("maxSize"))
	dim preSelect : preSelect = prepInt(request.QueryString("preSelect"))
	
	if selectName = "" then selectName = "modelID"
	if maxSize = 0 then maxSize = 150
	
	sql = "select modelID, modelName from we_models where brandID = " & brandID & " order by modelName"
	session("errorSQL") = sql
	Set rs = oConn.execute(sql)
	
%>
<select name="<%=selectName%>" style="width:<%=maxSize%>px">
	<option value="">All Models</option>
	<%
    do while not rs.EOF
    %>
    <option value="<%=rs("modelID")%>"<% if preSelect = prepInt(rs("modelID")) then %> selected="selected"<% end if %>><%=rs("modelName")%></option>
    <%
        rs.movenext
    loop
    %>
</select>
<%
call CloseConn(oConn)
%>