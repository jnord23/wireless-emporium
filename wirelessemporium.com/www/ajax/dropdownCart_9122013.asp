<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.Open Session("ConnectionString")
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	dim removeID : removeID = prepInt(request.QueryString("removeID"))
	dim addID : addID = prepInt(request.QueryString("addID"))
	dim addQty : addQty = prepInt(request.QueryString("addQty"))
	dim mySession : mySession = prepInt(request.cookies("mySession"))
	dim promo : promo = prepInt(request.QueryString("promo"))
	dim itemCnt : itemCnt = 0
	dim itemAmt : itemAmt = 0
	dim itemLap : itemLap = 0
%>
<div class="tb fcTopRow">
	<% if addID > 0 and addQty > 0 then %>
	<div class="fl fcAddedCheck"></div>
    <div class="fl fcAddedText">Item added to cart!</div>
    <% end if %>
    <div class="fr fcCloseCheck" onclick="popUpCart()"></div>
    <div class="fr fcCloseText" onclick="popUpCart()">Close</div>
</div>
<%
	sql = "exec sp_dropdownCart " & mySession & "," & removeID & "," & addID & "," & addQty & "," & promo & ",'" & request.ServerVariables("REMOTE_ADDR") & "'"
	session("errorSQL") = sql
	set fCartRS = oConn.execute(sql)

	do while not fCartRS.EOF
		itemLap = itemLap + 1
		fcID = prepInt(fCartRS("id"))
		fcQty = prepInt(fCartRS("qty"))
		fcPic = fCartRS("itemPic")
		fcDesc = prepStr(fCartRS("itemDesc"))
		fcPrice = prepInt(fCartRS("price_our"))
		fcRetail = prepInt(fCartRS("price_retail"))
		fcCustomCost = fCartRS("customCost")
%>
<div class="tb fcItem">
	<div class="fl fcImg"><img src="/productpics/thumb/<%=fcPic%>" border="0" width="100" height="100" /></div>
    <div class="fl">
    	<div class="tb fcDesc"><%=fcDesc%></div>
        <div class="tb fcPriceRow">
        	<% if isnull(fcCustomCost) then %>
			<div class="fl fcPriceTitle">Our Price:</div>
			<div class="fl fcPrice"><%=formatCurrency(fcPrice,2)%></div>
            <%
			else
				fcPrice = fcCustomCost
			%>
            <div class="fl fcPriceTitle">Promo Price:</div>
            	<% if prepInt(fcCustomCost) = 0 then %>
            	<div class="fl fcPrice">FREE</div>
				<% else %>
				<div class="fl fcPrice"><%=formatCurrency(prepInt(fcCustomCost),2)%></div>
            	<% end if %>
            <% end if %>
        </div>
        <div class="tb fcPriceRow">
			<div class="fl fcRetailTitle">Retail Price:</div>
			<div class="fl fcRetail"><%=formatCurrency(fcRetail,2)%></div>
        </div>
    </div>
    <div class="tb qtyRow">
    	<div class="fl qtyTitle">Qty:</div>
        <div class="fl qtyAmt"><%=fcQty%></div>
        <!-- Start remove without the promo -->
        <div id="popCartRemove" style="display:none;" class="fl fcRemove" onclick="popUpCart_remove(<%=fcID%>)">REMOVE</div>
        <!-- End remove without the promo -->
        <!-- Start remove including the promo -->
        <div id="popCartRemoveWithPromo" class="fl fcRemove" onclick="popUpCartWithPromo_remove(<%=fcID%>)">REMOVE</div>
        <!-- End remove including the promo -->
    </div>
</div>
<%
		fCartRS.movenext
		itemCnt = itemCnt + fcQty
		itemAmt = itemAmt + (fcPrice * fcQty)
		if itemLap = 1 and not fCartRS.EOF and addID > 0 and addQty > 0 then
%>
<div class="tb otherItemSlot">Other items in your cart:</div>
<%
		end if
	loop
	oConn.Close
	set oConn = nothing
%>
<div class="tb subTotalRow">
	<div class="fl fcSubtotalTitle">Subtotal (<%=itemCnt%> items):</div>
    <div class="fl fcSubtotalAmt"><%=formatCurrency(itemAmt)%></div>
</div>
<div class="tb subTotalRow">
    <div class="fl fcSubtotalTitle2">Shipping:</div>
    <div class="fl fcShipAmt">FREE!</div>
</div>
<div class="tb itemTotalRow">
	<div class="fl itemTotalTitle">Item Total:</div>
    <div class="fl itemTotalAmt"><%=formatCurrency(itemAmt)%></div>
</div>
<% if itemCnt > 0 then %>
<div id="fcCheckoutOpt2" class="tb fcCheckoutRow">
	<% if instr(request.ServerVariables("SERVER_NAME"),"staging.") > 0 then %>
    <div class="fl fcCheckout2" onclick="window.location='/cart/checkout.asp'"></div>
	<% else %>
    <div class="fl fcCheckout2" onclick="window.location='https://www.wirelessemporium.com/cart/checkout.asp'"></div>
    <% end if %>
   	<div class="fr fcViewCartLink" onclick="window.location='/cart/basket.asp'">VIEW CART</div>
</div>
<% end if %>