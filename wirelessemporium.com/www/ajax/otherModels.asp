<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	
	call getDBConn(oConn)
	
	function drawStitchModelBoxByArray(pArr, pStitchImg)
		timestamp = getFileLastModified(pStitchImg)
		if timestamp <> "" then pStitchImg = pStitchImg & "?v=" & timestamp	
	%>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
            <%
            useX = 0 : lap = 0 : a = 0
            if not isnull(pArr) then
                for nRows=0 to ubound(pArr,2)
                    a = a + 1
                    lap = lap + 1
                    modelID = cint(pArr(0,nRows))
                    modelName = pArr(1,nRows)
                    modelImg = pArr(2,nRows)
                    
                    if brandid = 17 then
                        altTag = replace(modelName,"/"," / ") & " Accessories"
                    else
                        altTag = brandName & " " & replace(modelName,"/"," / ") & " Accessories"
                    end if
	            %>
                <td width="20%" valign="top" style="border-bottom:1px solid #CCC;<% if a < 5 then %> border-right:1px solid #CCC;<% end if %> padding:10px 5px 5px 5px;">
                    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                        <tr>
                            <td align="center" valign="top">
                                <%if modelID = 940 then%>
                                <a href="/apple-ipad-accessories" title="<%=altTag%>"><div style="width: 70px; height: 112px; margin-bottom:10px; background: url(<%=pStitchImg%>) <%=useX%>px 0px no-repeat; cursor:pointer;" title="<%=altTag%>"></div></a>
                                <div style="width:130px; height:50px;"><a class="cellphone2-link" href="/apple-ipad-accessories" title="<%=altTag%>"><%=replace(modelName,"/"," / ") & " Accessories"%></a></div>
                                <%else%>
                                <a href="/<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>-accessories" title="<%=altTag%>"><div style="width: 70px; height: 112px; margin-bottom:10px; background: url(<%=pStitchImg%>) <%=useX%>px 0px no-repeat; cursor:pointer;" title="<%=altTag%>"></div></a><br />
                                <div style="width:130px; height:50px;"><a class="cellphone2-link" href="/<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>-accessories" title="<%=altTag%>"><%=replace(modelName,"/"," / ") & " Accessories"%></a></div>
                                <%end if%>
                                <% if pArr(5,nRows) then %><div style="width:100%; text-align:center; color:#900;">International Phone</div><% end if %>
                            </td>
                        </tr>
                    </table>
                </td>
    	        <%
                    if a = 5 then
                        response.write "</tr><tr>" & vbcrlf
                        a = 0
                    end if
                    useX = useX - 70
                next					
            end if
            %>
            </tr>
        </table>    
    <%
	end function
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	
	sql	=	"select	a.modelid, a.modelname, a.modelimg, a.topModel, a.oldModel, a.international, b.brandname, b.brandimg " & vbcrlf & _
			"	,	cast(case when a.topModel <> 1 and a.oldModel <> 1 and a.international <> 1 then 1 else 0 end as bit) displayModel, (select count(*) from we_models where brandID = " & brandID & " and (oldModel = 1 or international = 1)) as otherModels " & vbcrlf & _
			"from 	we_models a with (nolock) join we_brands b with (nolock) " & vbcrlf & _
			"	on 	a.brandid = b.brandid " & vbcrlf & _
			"where 	a.modelname is not null and a.hidelive = 0 and (a.oldModel = 1 or a.international = 1) and a.brandid = '" & brandid & "' " & vbcrlf & _
			"	and a.modelname not like 'all model%'" & vbcrlf
			
	if brandID = 17 and phoneOnly = 1 then
		brandName = "iPhone"
		cellPhoneTitle = "Cell Phone"
		SQL = SQL & " and a.modelName not like '%iPod%' and a.modelName not like '%iPad%'" & vbcrlf
	elseif brandID = 17 and phoneOnly = 0 then
		brandName = "iPod/iPad"
		phoneTitle = "Product"
		cellPhoneTitle = ""
		SQL = SQL & " and a.modelName not like '%iPhone%'" & vbcrlf
	else
		cellPhoneTitle = "Cell Phone"
		SQL = SQL & " and a.isTablet = 0" & vbcrlf
	end if
	
	if cbCarrier <> "" then
		fileExt = "_" & cbCarrier
		showPopPhones = 0
		sql	= sql & " and a.carriercode like '%' + '" & cbCarrier & "' + ',%'" & vbcrlf
	end if
	
	sqlOrder = " order by international, oldModel desc, modelname"
	
	sql	= sql & sqlOrder
	session("errorSQL") = sql
	arrRS = getDbRows(sql)
	
	brandName = arrRS(6,0)
	
	otherItemImgPath = formatSEO(brandName) & "_other.jpg"
	if instr(request.ServerVariables("HTTP_HOST"),"staging") > 0 then otherItemImgPath = "staging_" & otherItemImgPath
	
	imgStitchPath = Server.MapPath("/images/stitch/brands/" & otherItemImgPath)
	call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrRS, 2)
%>
<table border="0" cellpadding="3" cellspacing="0">
	<tr>
    	<td><%=drawStitchModelBoxByArray(arrRS, "/images/stitch/brands/" & otherItemImgPath)%></td>
    </tr>
</table>
<%
	call CloseConn(oConn)
%>