<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	
	call getDBConn(oConn)
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_Compatibility.asp"-->
<%
	dim itemID : itemID = prepInt(request.QueryString("itemID"))
	dim musicSkins : musicSkins = prepInt(request.QueryString("musicSkins"))
	dim compatibility, partNumber, modelName
	
	if musicSkins = 1 then
		sql = "exec sp_pullMusicSkinsDetailsByItemID " & itemID
	else
		sql = "exec sp_pullProductDetailsByItemID " & itemID
	end if
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	partNumber = rs("partNumber")
	modelName = rs("modelName")
	compatibility = prepStr(RS("COMPATIBILITY"))
	
	if compatibility = "" then compatibility = CompatibilityList(partNumber,"display")
	
	if compatibility <> "" then 
		response.write "<p><b>COMPATIBILITY:</b>&nbsp;&nbsp;" & compatibility & "</p>" & vbcrlf
	else
		response.write "<p><b>COMPATIBILITY:</b>&nbsp;&nbsp;" & modelName & "</p>" & vbcrlf	
	end if
	
	call CloseConn(oConn)
%>