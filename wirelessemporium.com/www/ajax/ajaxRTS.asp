<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/admin/formatNotes.asp"-->
<%	
	dim siteID : siteID = prepInt(request.QueryString("siteID"))
	dim orderid : orderid = prepInt(request.QueryString("orderid"))
	dim accountid : accountid = prepInt(request.QueryString("accountid"))

	sql	=	"select	a.orderid, b.accountid, b.email, b.fname, b.lname" & vbcrlf & _
			"	,	isnull(b.sAddress1, '') + ' ' + isnull(b.sAddress2, '') sAddress" & vbcrlf & _
			"	,	sCity, sState, sZip" & vbcrlf & _
			"from	we_orders a join v_accounts b" & vbcrlf & _
			"	on	a.store = b.site_id and a.accountid = b.accountid" & vbcrlf & _
			"where	a.store = " & siteID & vbcrlf & _
			"	and	a.orderid = " & orderid & vbcrlf & _
			"	and b.accountid = " & accountid
	set rs = oConn.execute(sql)
	if not rs.eof then
		fromAddress = ""	
		select case siteID
			case 0 
				fromAddress = "service@wirelessemporium.com"
				siteName = "WirelessEmporium.com"
			case 1 
				fromAddress = "service@cellphoneaccents.com"
				siteName = "CellphoneAccents.com"			
			case 2 
				fromAddress = "support@cellularoutfitter.com"
				siteName = "CellularOutfitter.com"			
			case 3 
				fromAddress = "service@phonesale.com"
				siteName = "PhoneSale.com"			
			case 10 
				fromAddress = "service@tabletmall.com"
				siteName = "TabletMall.com"			
		end select
	
		email = rs("email")
		address = rs("sAddress") & "<br>" & rs("sCity") & ", " & rs("sState") & " " & rs("sZip")
		emailBody = "Hello " & rs("fname") & " " & rs("lname") & ",<br><br>" & vbcrlf & _
					"Thank you for your recent order, <b>" & orderid & "</b>, with " & siteName & ".<br><br>" & vbcrlf & _
					"We're writing to inform you the package we shipped your order in was returned to us by the Post Office. The package was mark as undeliverable. <br><br>" & vbcrlf & _
					"In order to have your package reshipped, please reply and confirm that the following address is your correct shipping address:<br><br>" & vbcrlf & _
					address & "<br><br>" & vbcrlf & _
					"Please make sure any necessary apartment or unit numbers are included.<br><br>" & vbcrlf & _
					"We apologize for any inconvenience this may have caused you and we look forward to sending your purchase.<br><br>" & vbcrlf & _
					siteName
		emailSubject = "Alert: Your " & siteName & " Order Could Not Be Delivered"
	
		CDOSend email,fromAddress,emailSubject,emailBody
		
	
		sql	=	"	select	id, orderid, convert(varchar(8000), ordernotes) ordernotes	" & vbcrlf & _
				"	from	we_ordernotes" & vbcrlf & _
				"	where	orderid = '" & orderid & "'" & vbcrlf
		set objRsNotes = oConn.execute(sql)
		
		if not objRsNotes.EOF then
			curOrderNotes 	= 	objRsNotes("ordernotes")
			strMsg			=	"RTS sent"
			AddNotes 		= 	curOrderNotes & vbcrlf & formatNotes(strMsg)
	
			sql = 	"	update	we_ordernotes	" & vbcrlf & _
					"	set		ordernotes = '" & AddNotes & "'" & vbcrlf & _
					"	where	orderid = '" & orderid & "'" & vbcrlf		
	
			session("errorSQL") = sql		
			oConn.execute(sql)
		else
			strMsg		=	"RTS sent"
			AddNotes 	= 	formatNotes(strMsg)
			sql 		= 	"insert into we_ordernotes(orderid, ordernotes) values('" & orderid & "', '" & AddNotes & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		objRsNotes = null		
	
		response.write "done"
	end if
	call CloseConn(oConn)
%>