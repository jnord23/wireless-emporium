<div class="popup-container">
    <a href="javascript:void(0)" class="close" onclick="freeReturnShippingPopup(1)"></a>
	<div class="tb" style="padding:15px 0px 0px 30px;">
    	<div style="text-align:left; color:#ff6600; font-size:48px; font-weight:bold; width:100%; letter-spacing:3px;">
	    	Free Shipping &amp;<br>
    	    Free Return Shipping
        </div>
        <div class="tb" style="width:100%;">
        	<div class="fl" style="text-align:left; color:#888; font-size:24px;">On All Products</div>
        	<div class="fl" style="border-top:1px solid #888; margin:15px 0px 0px 20px; width:310px; height:0px;"></div>
        </div>
    </div>
    <div class="clear" style="height:20px;"></div>
	<div class="tb" style="text-align:left; background-color:#f2f2f2; width:100%; height:278px; position:relative;">
		<div style="position:absolute; top:-15px; left:0px;"><img src="/images/freereturnshipping/free-return-popup-box.png" border="0" /></div>
		<div class="fr" style="width:300px; padding:20px 0px 0px 0px;">
			<div class="ffl" style="padding-top:10px;">
            	<div class="fl"><img src="/images/freereturnshipping/free-return-popup-check.jpg" border="0" /></div>
            	<div class="fl" style="padding:5px 0px 0px 10px;">
                    <div style="font-size:14px; font-weight:bold; color:#111;">ENJOY FREE SHIPPING BOTH WAYS</div>
                    <div style="font-size:12px; color:#666;">ORDER SHIPS WITHIN 24 HOURS</div>
                </div>
            </div>
			<div class="ffl" style="padding-top:20px;">
            	<div class="fl"><img src="/images/freereturnshipping/free-return-popup-check.jpg" border="0" /></div>
            	<div class="fl" style="padding:5px 0px 0px 10px;">
                    <div style="font-size:14px; font-weight:bold; color:#111;">NO RESTOCKING FEES</div>
                    <div style="font-size:12px; color:#666;">RETURN ANY ITEM FREE OF CHARGE</div>
                </div>
            </div>
			<div class="ffl" style="padding-top:20px;">
            	<div class="fl"><img src="/images/freereturnshipping/free-return-popup-check.jpg" border="0" /></div>
            	<div class="fl" style="padding:5px 0px 0px 10px;">
                    <div style="font-size:14px; font-weight:bold; color:#111;">NO RISK SHOPPING</div>
                    <div style="font-size:12px; color:#666;">DON'T LIKE YOUR ITEM? NO PROBLEM.</div>
                </div>
            </div>
			<div class="ffl" style="padding-top:20px;">
            	<div class="fl"><img src="/images/freereturnshipping/free-return-popup-check.jpg" border="0" /></div>
            	<div class="fl" style="padding:5px 0px 0px 10px;">
                    <div style="font-size:14px; font-weight:bold; color:#111;">SIMPLY RETURN IT WITHIN 90-DAYS</div>
                    <div style="font-size:12px; color:#666;">WE'LL EVEN PAY FOR SHIPPING!</div>
                </div>
            </div>
       	</div>
	</div>
</div>