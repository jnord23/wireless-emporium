<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	
	dim useZip : useZip = prepStr(request.QueryString("zipcode"))
	dim zipType : zipType = prepStr(request.QueryString("ziptype"))	

	call getDBConn(oConn)
		
	sql	=	"select	city, state" & vbcrlf & _
			"from	xzipcities" & vbcrlf & _
			"where	zipcode = '" & useZip & "'" & vbcrlf & _
			"order by 1"
	set rs = oConn.execute(sql)
	if rs.eof then
		response.write "<span style=""font-size:12px; color:#555;"">No city names found for " & useZip & "</span>"
	else
		do until rs.eof
			%>
			<div class="left" style="width:100%; text-align:left; padding:5px 0px 5px 0px; border-bottom:1px solid #ccc;">
				<a style="font-size:12px;" href="javascript:populateCityState('<%=zipType%>','<%=replace(rs("city"), "'", "\'")%>','<%=replace(rs("state"), "'", "\'")%>');"><%=rs("city")%></a>
			</div>
			<%
			rs.movenext
		loop
	end if
	
	call CloseConn(oConn)
%>		