<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/includes/asp/inc_BasePage.asp"--><%	
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%

	dim mpPageName : mpPageName = prepStr(request.QueryString("mpPageName"))
	dim compPageName : compPageName = prepStr(request.QueryString("compPageName"))
	dim forceRefresh : prepInt(request.QueryString("forceRefresh"))
	dim replacePage : replacePage = 0
	dim serverName : serverName = useHttp & "://" & request.ServerVariables("SERVER_NAME")
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	
	mpPageName = replace(mpPageName,"$QM","?")
	mpPageName = replace(mpPageName,"$AM","&")
	if instr(mpPageName,"<") > 0 then mpPageName = left(mpPageName,instr(mpPageName,"<")-1)
	if instr(compPageName,"<") > 0 then compPageName = left(compPageName,instr(compPageName,"<")-1)
	
	if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
		stitchLoc = "/productPics/devStitch/"
	else
		stitchLoc = "/productPics/stitch/"
	end if
	
	if instr(mpPageName,".asp") > 0 then
		curPage = compPageName
		testPage = replace(compPageName,".htm","_test.htm")
		
		response.Write("Searching for:" & curPage & "<br><br>")
		sql = "select lastUpdate from we_compressedPages where pageName = '" & curPage & "' and siteID = 0"
		set lastUpdateRS = oConn.execute(sql)
		
		if lastUpdateRS.EOF then
			response.Write("Record not found: Enter new record and force refresh<br><br>")
			sql = "insert into we_compressedPages (siteID,pageName) values(0,'" & curPage & "')"
			oConn.execute(sql)
			forceRefresh = 1
		else
			response.Write("Record found!<br><br>")
			lastDate = lastUpdateRS("lastUpdate")
			if datediff("n",lastDate,now) > 60 then
				response.Write("File has expired: Force refresh<br><br>")
				sql = "update we_compressedPages set lastUpdate = '" & now & "' where pageName = '" & curPage & "' and siteID = 0"
				oConn.execute(sql)
				forceRefresh = 1
			else
				response.Write("File still good: " & lastDate & " {vs} " & now & " = " & datediff("n",lastDate,now) & " minutes old<br><br>")
			end if
		end if
	end if
	
	if forceRefresh = 1 then
		useURL = serverName & "/" & mpPageName
		set XMLHTTP = server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
		XMLHTTP.Open "GET", useURL, False
		session("errorSQL") = useURL
		XMLHTTP.Send
		
		allText = xmlHTTP.ResponseText
		
		response.Write("Compress HTML<br><br>")
		if instr(lcase(allText),"<script") > 0 then
			lapCnt = 0
			do while len(allText) > 0
				lapCnt = lapCnt + 1
				if instr(lcase(allText),"<script") > 0 then
					nextScript = instr(allText,"<script")
					tempHolding = left(allText,nextScript-1)
					session("errorSQL") = "nextScript:" & nextScript & "<br>" & allText
					if nextScript > 1 then allText = mid(allText,nextScript-1)
					
					tempHolding = replace(tempHolding,vbCrLf,"")
					tempHolding = replace(tempHolding,"  ","")
					showPage = showPage & replace(tempHolding,vbTab,"")
					
					endScript = instr(lcase(allText),"</script>")
					showPage = showPage & vbCrLf & left(allText,endScript+8)
					allText = mid(allText,endScript+9)
				else
					tempHolding = replace(allText,vbCrLf,"")
					tempHolding = replace(tempHolding,"  ","")
					showPage = showPage & replace(tempHolding,vbTab,"")
					allText = ""
				end if	
				if lapCnt = 100 then
					response.Write(showPage)
					response.Write("force stop<br />" & vbCrLf)
					call CloseConn(oConn)
					response.End()
				end if
			loop
			
			stitchFileName = replace(compPageName,".htm",".jpg")
			if instr(stitchFileName,"/") > 0 then
				stitchFileName = mid(stitchFileName,instrrev(stitchFileName,"/")+1)
			end if
			
			'Stitch images together and adjustCode
			dim performStitch : performStitch = 0
			'if instr(mpPageName,"brand_mp.asp") > 0 or mpPageName = "index_mp.asp" or instr(mpPageName,"brand-model_mp.asp") > 0 then performStitch = 1
			'if instr(mpPageName,"brand-model-category-details_mp.asp") > 0 then performStitch = 1
			
			dim skipImg : skipImg = 0
			dim compressImg : compressImg = 0
			dim totalSrc : totalSrc = ""
			dim newCanvasWidth : newCanvasWidth = 0
			dim newCanvasHeight : newCanvasHeight = 0
			dim canvasHeight : canvasHeight = 0
			if performStitch = 1 then
				response.Write("Consolidate images to stitch<br><br>")
				if fso.fileExists(server.MapPath(stitchLoc & stitchFileName)) then fso.deleteFile(server.MapPath(stitchLoc & stitchFileName))
				imgLap = 0
				
				if not fso.fileExists(server.MapPath(stitchLoc & stitchFileName)) then
					
					set imgDetailObj = Server.CreateObject("Persits.Jpeg")
					
					do while instr(showPage,"<img") > 0 or instr(showPage,"background:") > 0 or instr(showPage,"background-image:") > 0
						imgLap = imgLap + 1
						if instr(showPage,"<img") > 0 then
							isBackground = false
							bgImg = false
							nextImg = mid(showPage,instr(showPage,"<img"))
							nextImg = left(nextImg,instr(nextImg,">"))
							nextSrc = mid(nextImg,instr(nextImg,"src=")+5)
							endSrcLoc1 = instr(nextSrc,"'")
							endSrcLoc2 = instr(nextSrc,"""")
							if endSrcLoc1 < 1 then
								if endSrcLoc2 > 1 then
									nextSrc = left(nextSrc,instr(nextSrc,"""")-1)
								end if
							else
								if endSrcLoc2 = 0 then endSrcLoc2 = 9999
								if endSrcLoc1 < endSrcLoc2 then
									nextSrc = left(nextSrc,instr(nextSrc,"'")-1)
								else
									nextSrc = left(nextSrc,instr(nextSrc,"""")-1)
								end if
							end if
						elseif instr(showPage,"background:") > 0 then
							isBackground = true
							bgImg = false
							nextImg = mid(showPage,instr(showPage,"background:"))
							nextImg = left(nextImg,instr(nextImg,";"))
							if len(nextImg) > 300 then
								nextImg = left(nextImg,instr(nextImg,")"))
							end if
							tempNextImg = replace(nextImg," ","")
							session("errorSQL") = tempNextImg
							nextSrc = replace(tempNextImg,"background:url(","")
							'nextSrc = replace(nextSrc,")","")
							if instr(nextSrc,")") > 0 then nextSrc = left(nextSrc,instr(nextSrc,")")-1) else nextSrc = "temp.png"
						elseif instr(showPage,"background-image:") > 0 then
							isBackground = true
							bgImg = true
							nextImg = mid(showPage,instr(showPage,"background-image:"))
							nextImg = left(nextImg,instr(nextImg,";"))
							if len(nextImg) > 300 then
								nextImg = left(nextImg,instr(nextImg,")"))
							end if
							tempNextImg = replace(nextImg," ","")
							nextSrc = replace(tempNextImg,"background-image:url(","")
							'nextSrc = replace(nextSrc,")","")
							nextSrc = left(nextSrc,instr(nextSrc,")")-1)
						end if
						
						imgX = 0
						imgY = 0
						pictureCreationStarted = 0
						
						if instr(nextSrc,".png") > 0 or instr(nextSrc,".gif") > 0 or instr(nextSrc,"//") > 0 or instr(nextSrc,"/stitch/") > 0 then
							pngFile = 1
							'stitchFileName = "test.png"
						else
							pngFile = 0
							
							session("errorSQL") = "nextSrc:" & nextSrc
							imgDetailObj.Open server.MapPath(nextSrc)
							curImgHeight = imgDetailObj.Height
							curImgWidth = imgDetailObj.Width
							
							preCanvasWidth = newCanvasWidth
							newCanvasWidth = newCanvasWidth + curImgWidth + 3
							if curImgHeight > newCanvasHeight then newCanvasHeight = curImgHeight
							
							if pngFile = 1 then
								'oJpeg.Canvas.DrawPNG canvasWidth, imgY, server.MapPath(nextSrc)
							else
								totalSrc = totalSrc & nextSrc & "@@" & preCanvasWidth & "##"
							end if
						end if
						
						if pngFile = 1 then
							skipImg = skipImg + 1
							if isBackground then
								newDiv = replace(nextImg,"background","bgPlaceholder")
							else
								newDiv = replace(nextImg,"<img","<notImg")
							end if
						else
							compressImg = compressImg + 1
							if isBackground then
								newDiv = "bgPlaceholder:url(" & stitchLoc & stitchFileName & ") -" & preCanvasWidth & "px 0px no-repeat;"
							else
								newDiv = "<notImg style='width: " & curImgWidth & "px; height: " & curImgHeight & "px; bgPlaceholder: url(" & stitchLoc & stitchFileName & ") -" & preCanvasWidth & "px 0px no-repeat; cursor:pointer;' src='/images/blank.gif' border='0' />"
							end if
						end if
						showPage = replace(showPage,nextImg,newDiv)
						if imgLap = 500 then
							response.Write("time to bail out!<br><br>")
							response.Write("skipImg:" & skipImg & "<br>")
							response.Write("compressImg:" & compressImg & "<br>")
							'response.End()
							exit do
						end if
					loop
					
					set imgDetailObj = nothing
					
					showPage = replace(showPage,"bgPlaceholder","background")
					showPage = replace(showPage,"bgPlaceholder2","background-image")
					showPage = replace(showPage,"notImg","img")
				end if
			end if
		else
			showPage = replace(allText,vbCrLf,"")
			showPage = replace(showPage,"  ","")
			showPage = replace(showPage,vbTab,"")
		end if
		
		response.Write("Create file temp file:" & server.MapPath(testPage) & "<br><br>")
		set tfile = fso.createTextFile(server.MapPath(testPage),true,true)
		tfile.writeLine(showPage)
		tfile.close	
		tfile = null
		newFile = 1
		
		response.Write("curPage:" & curPage & "<br><br>")
		response.Write("testPage:" & testPage & "<br><br>")
		set curFile = fso.OpenTextFile(server.MapPath(curPage),1,0,-1)
		set testFile = fso.OpenTextFile(server.MapPath(testPage),1,0,-1)
	
		x = 0
		response.Write("Compare files!<br><br>")
		do until curFile.AtEndOfStream
			curLine = curFile.ReadLine
			if testFile.AtEndOfStream then
				if trim(curLine) <> "" then replacePage = 1
			else
				testLine = testFile.ReadLine
			end if
			if instr(curLine,"CUSTOMER TESTIMONIALS") > 0 then curLine = left(curLine,instr(curLine,"CUSTOMER TESTIMONIALS"))
			if instr(testLine,"CUSTOMER TESTIMONIALS") > 0 then testLine = left(testLine,instr(testLine,"CUSTOMER TESTIMONIALS"))
			if curLine <> testLine then
				if trim(curLine) <> "" then 
					replacePage = 1
				end if
			end if
			prevCurLine = curLine
			prevTestLine = testLine
			x = x + 1
		loop
		
		if not testFile.AtEndOfStream then replacePage = 1
		
		testFile.close
		testFile = null
		
		if replacePage = 1 then
			response.Write("New Page Found<br><br>")
			sql = "update we_compressedPages set updateWasNeeded = 1 where pageName = '" & curPage & "' and siteID = 0"
			oConn.execute(sql)
			set newCode = fso.OpenTextFile(server.MapPath(testPage),1,0,-1)
			set tfile = fso.createTextFile(server.MapPath(curPage),true,true)
			tfile.writeLine(newCode.readAll)
			tfile.close	
			tfile = null
			newCode.close	
			newCode = null
			
			'Here is where we actually make the stitch image
			if totalSrc <> "" then
				response.Write("make new image with:" & totalSrc & "<br><br>")
				totalSrc = left(totalSrc,len(totalSrc)-2)
				totalSrcArray = split(totalSrc,"##")
				
				set oImg = Server.CreateObject("Persits.Jpeg")
				set oJpeg = Server.CreateObject("Persits.Jpeg")
				oJpeg.Quality = 80
				oJpeg.Interpolation = 1
				
				oJpeg.New newCanvasWidth, newCanvasHeight, &HFFFFFF
				
				for i = 0 to ubound(totalSrcArray)
					imgDetailArray = split(totalSrcArray(i),"@@")
					imgSrc = imgDetailArray(0)
					imgLocX = imgDetailArray(1)
					
					oImg.Open server.MapPath(imgSrc)
					
					oJpeg.Canvas.DrawImage imgLocX, 0, oImg
				next
				
				session("errorSQL") = "stitchFileName:" & stitchFileName
				on error resume next
					oJpeg.Save server.MapPath(stitchLoc & stitchFileName)
					errorLap = 0
					do while err.number <> 0
						errorLap = errorLap + 1
						response.Write("<!-- ImgError:" & errorLap & " -->")
						err.Clear
						oJpeg.Save server.MapPath(stitchLoc & stitchFileName)
						if errorLap = 200 then exit do
					loop
				On Error Goto 0
				
				set oJpeg = nothing
				set oImg = nothing
			end if
		else
			response.Write("file still good (1)<br><br>")
			sql = "update we_compressedPages set updateWasNeeded = 0 where pageName = '" & curPage & "' and siteID = 0"
			oConn.execute(sql)
		end if
		
		'Delete the temp file
		if fso.fileExists(server.MapPath(testPage)) then
			response.Write("Deleting the temp file at:" & server.MapPath(testPage) & "<br><br>")
			response.Flush()
			fso.DeleteFile server.MapPath(testPage),true
		else
			response.Write("Can't find temp file to pop:" & testPage & "<br><br>")
		end if
	else
		response.Write("file still good (2)<br><br>")
	end if
	response.Write("Master Compare Complete<br><br>")

	call CloseConn(oConn)
%>