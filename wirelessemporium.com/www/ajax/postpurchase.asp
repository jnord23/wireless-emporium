<%
	response.buffer = true
	response.expires = -1
	
	itemids = "146466"
	currentTotal = 0.0
	currentTax = 0.0
	if prepStr(request.querystring("itemids")) <> "" then itemids = prepStr(request.querystring("itemids"))
	if prepInt(request.querystring("currentTotal")) > 0 then currentTotal = prepInt(request.querystring("currentTotal"))	
	if prepInt(request.querystring("currentTax")) > 0 then currentTax = prepInt(request.querystring("currentTax"))
	
	orderid = prepInt(request.querystring("orderid"))
	auth_transactionid = prepStr(request.QueryString("transid"))
	
	usePromoPercent = 0
	
	if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then	
		actionURL = "/framework/postProcessing"
	else
		actionURL = "https://www.wirelessemporium.com/framework/postProcessing"
	end if
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<div style="width:700px; padding:5px; text-align:left; margin-left:auto; margin-right:auto; margin-top:100px; position:relative; background-color:#fff; color:#444; border:10px solid #444; border-radius:5px; font-family:Arial, Helvetica, sans-serif;">
	<form name="frmPostPurchase" action="<%=actionURL%>" method="post">
    <div style="position:absolute; top:-20px; right:-20px; cursor:pointer;" onclick="pp_closePopup()"><img src="/images/icon-lightbox-close.png" border="0" /></div>
    <div style="float:left; width:100%; padding-top:10px;">
        <div style="float:left; width:100%; border-bottom:1px solid #ccc;">
            <div style="float:left; padding:0px 20px 10px 30px;"><img src="/images/postpurchase/ico-top-cart.png" border="0" /></div>
            <div style="float:left; font-size:46px; color:#333; padding-top:10px;">Thanks for your purchase!</div>
        </div>
        <div style="float:left; width:680px; padding:10px; border-bottom:1px solid #ccc;">
        	<div style="font-size:18px; color:#444; text-align:center; line-height:24px;">Congrats! Your order is complete and will ship out within 24 hours for <strong>FREE</strong> from our Southern California warehouse.</div>
            <div align="center" style="padding:20px 0px 20px 0px;"><img src="/images/postpurchase/ico-but-wait.png" border="0" /></div>
            <div style="font-size:14px; color:#333; text-align:left; line-height:22px;">
				<%
                sql	=	"select	top 1 promoDate, promoPercent" & vbcrlf & _
                        "from	postpurchasepromo" & vbcrlf & _
                        "where	siteid = 0" & vbcrlf & _
                        "order by 1 desc"
                set rs = oConn.execute(sql)
                if not rs.eof then usePromoPercent = rs("promoPercent")
                
                if usePromoPercent > 0 then
                %>
				However, we did notice that your order is missing some of our most popular items as rated by our customers. For <span style="color:#ff6600; font-weight:bold; font-style:italic;">One Time Only</span>, get an <span style="color:#ff6600; font-weight:bold; text-decoration:underline;">additional <%=usePromoPercent%>% off</span> any of the items below.<br /><br />
				Select the items below to instantly add it to your order at your new discounted price. This offer will never appear again, so make the most of your opportunity!
                <%
				else
				%>
				Save even more money by instantly adding additional value packed accessories. Below is a list of our most popular related products as chosen by our customers. <br />Just click the <span style="font-weight:bold; color:#ff6600;">ADD TO MY ORDER</span> button below to grab some of our best-selling items! Supplies are limited.
                <%
                end if
                %>            
			</div>
        </div>
        <div style="float:left; width:690px; padding:5px;">
            <div style="float:left; width:100%;">
            <%
			sql = "sp_ppUpsell 0, '" & itemids & "', " & usePromoPercent & ", 60"
			set rs = oConn.execute(sql)
			lap = 0
			do until rs.eof
				lap = lap + 1
				itemdesc = rs("itemdesc")
				if len(itemdesc) > 65 then itemdesc = left(itemdesc, 62) & "..."
				price_retail = rs("price_retail")
				price_our = rs("price")
			%>
            	<div style="float:left; width:167px; padding:30px 5px 0px 0px;">
                	<div style="float:left; width:100%; text-align:center;"><img src="/productpics/thumb/<%=rs("itempic")%>" border="0" /></div>
                	<div style="float:left; width:162px; margin-top:5px; padding:5px 0px 5px 0px; background-color:#eaeff5; border-radius:5px;">
                    	<div style="float:left;"><input id="id_chkItem<%=lap%>" type="checkbox" name="chkItem<%=lap%>" value="<%=rs("itemid")%>" onclick="updatePPTotal();" /></div>
                    	<div style="float:left; padding:2px 0px 0px 5px; color:#444; font-weight:bold; font-size:11px;">Add This To My Order</div>
					</div>
                	<div style="float:left; width:100%; padding-top:5px; color:#333; font-weight:bold; font-size:12px;"><input id="id_qty<%=lap%>" type="text" name="chkQty<%=lap%>" maxlength="3" value="1" style="width:35px; height:20px; padding:5px; color:#333; text-align:center; border-radius:3px; border:1px solid #ccc; -moz-box-shadow: inset 0 0 1px 1px #eee; -webkit-box-shadow: inset 0 0 1px 1px #eee; box-shadow: inset 0 0 1px 1px #eee;" onkeyup="updatePPTotal();" /> &nbsp; Quantity</div>
                	<div style="float:left; width:100%; padding-top:5px; font-size:12px; color:#333; height:45px;"><%=itemdesc%></div>
                	<div style="float:left; width:100%; padding-top:5px; color:#ff6600; font-weight:bold; font-size:14px;"><span style="color:#333;">Our Price: </span><%=formatcurrency(price_our)%></div>
                	<div style="float:left; width:100%; padding-top:5px; color:#666; font-size:11px;">List Price: <span style="text-decoration:line-through;"><%=formatcurrency(price_retail)%></span></div>
                    <input id="id_itemPrice<%=lap%>" type="hidden" name="itemPrice<%=lap%>" value="<%=price_our%>" />
                    <input id="id_itemRetail<%=lap%>" type="hidden" name="itemRetail<%=lap%>" value="<%=price_retail%>" />
                </div>
            <%
				rs.movenext
			loop
			%>
            </div>
            <div style="float:left; width:688px; border:1px solid #ccc; background-color:#f7f7f7; border-radius:5px; margin-top:15px;">
                <div style="float:left; width:100%; padding-top:10px;">
                	<div style="float:left; font-size:16px; color:#333; padding:0px 10px 0px 10px;">Please add the selected items to my order now for an additional: </div>
                	<div style="float:left; font-size:17px; color:#ff6600; font-weight:bold;">$<span id="addlTotal">0.0</span>* (Save $<span id="addlSave">0.0</span>)</div>
                </div>            
                <div style="float:left; padding:15px 15px 0px 30px;"><img src="/images/postpurchase/btn-no-thanks.png" border="0" onclick="pp_closePopup()" style="cursor:pointer;" /></div>
                <div style="float:left; padding:15px 0px 25px 0px;">
                	<input type="image" src="/images/postpurchase/btn-buy.png" onclick="return doSubmit();" />
				</div>
            </div>
            <%
			curTax = 0
			curGrandTotal = 0
			sql = 	"select	isnull(ordersubtotal, 0) ordersubtotal, isnull(ordershippingfee, 0) ordershippingfee, isnull(orderTax, 0) orderTax, isnull(ordergrandtotal, 0) ordergrandtotal, accountid" & vbcrlf & _
					"from	we_orders" & vbcrlf & _
					"where	orderid = '" & orderid & "'"
			set rs = oConn.execute(sql)
			if not rs.eof then
				curSubTotal = prepInt(rs("ordersubtotal"))
				curShippingFee = prepInt(rs("ordershippingfee"))
				curTax = prepInt(rs("orderTax"))
				curGrandTotal = prepInt(rs("ordergrandtotal"))
				accountid = prepInt(rs("accountid"))
			end if
			%>
            <div style="float:left; width:100%; margin:15px 0px 0px 0px; font-size:12px;">
                *Items will be added to your current order. You will see a 2nd charge for $<span id="addlGrandTotal">0.00</span> on your credit card. 
                Total includes $<span id="addlTaxTotal">0.00</span> in sales tax for California residents. 
                This is a WirelessEmporium.com offer.
            </div>
        </div>
    </div>
    <div style="clear:both; height:15px;"></div>
    <input type="hidden" name="addlGrandTotal" value="" />
    <input type="hidden" name="grandTotal" value="<%=curGrandTotal%>" />
    <input type="hidden" name="taxTotal" value="<%=curTax%>" />
    <input type="hidden" name="hidCount" value="<%=lap%>" />
    <input type="hidden" name="orderid" value="<%=orderid%>" />
    <input type="hidden" name="transid" value="<%=auth_transactionid%>" />
	</form>
</div>
<%
call CloseConn(oConn)
%>
