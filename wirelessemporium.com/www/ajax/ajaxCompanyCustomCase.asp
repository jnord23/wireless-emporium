<div id="popupSubmit" style="display:block; width:656px; margin-left:auto; margin-right:auto; margin-top:100px;">
    <form name="frmQuote" method="post">
	<div style="width:100%; position:relative;"><div id="closeButton" onclick="closeFloatingBox()" title="Close">x</div></div>
    <div style="clear:both;"></div>
    <div style="float:left; width:606px; border:1px solid #b2b2b2; background-color:#f0f0f0; padding:25px; margin-top:20px;">
        <div style="float:left; width:100%; color:#333; font-size:20px;">Request Quote</div>
        <div style="float:left; width:100%; padding-top:20px;">
            <div style="float:left; width:200px;">
                <div class="left" style="color:#444; font-weight:bold;">Name</div>
                <div class="right"><strong>&gt;</strong>&nbsp;&nbsp;&nbsp;</div>
            </div>
            <div style="float:left; width:400px;">
                <input type="text" name="txtName" style="width:400px;" />
            </div>
        </div>
        <div style="float:left; width:100%; padding-top:10px;">
            <div style="float:left; width:200px;">
                <div class="left" style="color:#444; font-weight:bold;">Company Name</div>
                <div class="right"><strong>&gt;</strong>&nbsp;&nbsp;&nbsp;</div>
            </div>
            <div style="float:left; width:400px;">
                <input type="text" name="txtCName" style="width:400px;" />
            </div>
        </div>
        <div style="float:left; width:100%; padding-top:10px;">
            <div style="float:left; width:200px;">
                <div class="left" style="color:#444; font-weight:bold;">Phone Number</div>
                <div class="right"><strong>&gt;</strong>&nbsp;&nbsp;&nbsp;</div>
            </div>
            <div style="float:left; width:400px;">
                <input type="text" name="txtPhoneNumber" style="width:400px;" />
            </div>
        </div>
        <div style="float:left; width:100%; padding-top:10px;">
            <div style="float:left; width:200px;">
                <div class="left" style="color:#444; font-weight:bold;">Email</div>
                <div class="right"><strong>&gt;</strong>&nbsp;&nbsp;&nbsp;</div>
            </div>
            <div style="float:left; width:400px;">
                <input type="text" name="txtEmail" style="width:400px;" />
            </div>
        </div>
        <div style="float:left; width:100%; text-align:right; padding-top:10px;">
            <input type="image" src="/images/custom/corp/button-submit.jpg" onClick="return quoteSubmit();" />
        </div>            
    </div>
    <div style="clear:both;"></div>        
    </form>
</div>    