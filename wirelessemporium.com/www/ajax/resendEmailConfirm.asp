<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_Webloyalty.asp"-->
<!--#include virtual="/includes/asp/inc_GetMySession.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/framework/utility/outOfStock.asp"-->
<!--#include virtual="/framework/utility/readTextFile.asp"-->
<%
nAccountID = prepInt(request.querystring("accountid"))
nOrderID = prepInt(request.querystring("orderid"))

incEmail = true
if nOrderID > 0 then
	emailReceipt = 1
%>
<!--#include virtual="/includes/asp/inc_receipt_new.asp"-->
<%
end if

for thisEmail = 1 to 2
	if extOrderType = 1 then
		if thisEmail = 1 then
			sFromName = "Automatic E-Mail from WirelessEmporium.com"
			sFromAddress = "sales@WirelessEmporium.com"
			sAddRecipient1 = sEmail
			sSubject = "Wireless Emporium PAYPAL Order Confirmation"
		else
			sFromName = "Automatic E-Mail from WirelessEmporium.com"
			sFromAddress = "sales@WirelessEmporium.com"
			sSubject = "A New PAYPAL Order from Wireless Emporium"
		end if
	else
		if thisEmail = 1 then
			sFromName = "Automatic E-Mail from WirelessEmporium.com"
			sFromAddress = "sales@WirelessEmporium.com"
			sAddRecipient1 = sEmail
			sSubject = "Wireless Emporium Order Confirmation"
		else
			sFromName = "Automatic E-Mail from WirelessEmporium.com"
			sFromAddress = "sales@WirelessEmporium.com"
			sSubject = "A New Order from Wireless Emporium"
		end if
	end if
	cdo_from = sFromName & "<" & sFromAddress & ">"
	cdo_subject = sSubject
	cdo_body = ReceiptText
	on error resume next
		if thisEmail = 1 then
			' to Customer
			cdo_to = "" & sAddRecipient1 & ""
			'cdo_to = "webmaster@wirelessemporium.com"
			CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
			if Err.Number <> 0 then
				session("mailError") = "<p><b>Your order is being fulfilled and shipped to the shipping address you provided.</b></p>"
				session("mailError") = session("mailError") & "<p>However, when attempting to e-mail your Order Confirmation to <b>" & cdo_to & "</b>, "
				session("mailError") = session("mailError") & "we received the following error from your e-mail provider:<br>" & Err.Description & "</p>"
				session("mailError") = session("mailError") & "<p>It is possible that you may not receive any e-mails from us (such as Shipping Confirmation, etc.) "
				session("mailError") = session("mailError") & "if we continue to encounter problems with this e-mail address.</p>"
				session("mailError") = session("mailError") & "<p>Please be assured, however, that your order will be fulfilled and shipped.</p>"
			else
				strSQL = "UPDATE we_orders SET emailSent = 'yes' WHERE orderid = '" & nOrderID & "'"
				oConn.execute strSQL
			end if
		else
			' to Wireless Emporium recipients
			'cdo_to = "shipping@wirelessemporium.com"
			'CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		end if
	on error goto 0
next

call CloseConn(oConn)
%>