<div style="width:844px; display:table; margin:50px auto 0px auto;">
	<div style="background-color:#000; color:#FFF; font-weight:bold; text-align:right; cursor:pointer; padding:5px 10px;" onclick="closePromoDetails()">Close Promo Window X</div>
    <div><img src="/images/promos/july/weletfreedomring.jpg" border="0" width="844" height="206" /></div>
    <div style="background-color:#FFF; text-align:left; padding:15px 10px; width:824px; height:300px; overflow:auto;">
    	For the entire month of July, Wireless Emporium visitors will have a chance to win a daily prize just by tweeting 
        about any product under $60 with the hashtag #weletfreedomring and tagging our Twitter account @WirelessEmp! 
        Visitors can also win an item for a friend (or another one for themselves!) by writing a product review with the 
        hashtag #weletfreedomring in the body as well. 2 lucky winners will be chosen to win the product that they 
        tweeted/reviewed each day!
		<br /><br />
        Instructions:<br />
        Twitter Entries
        <ul>
        	<li>User must follow @WirelessEmp</li>
        	<li>Go to desired product's page and click the Tweet button at the top right corner</li>
        	<li>Tweet must include hashtag #weletfreedomring and tag @WirelessEmp</li>
        	<li>One tweet per account per day</li>
        </ul>        
        Review Entries
        <ul>
        	<li>User must enter a valid email address</li>
        	<li>Go to desired product's page and click the Reviews tab, then click Write a Review.</li>
        	<li>Review must include hashtag #weletfreedomring</li>
        	<li>One review per email per day</li>
        </ul>        
        Restrictions:
        <ul>
        	<li>Valid only to contestants within the United States.</li>
        	<li>Only items under $60 retail price are eligible. All entries with products over $60 will be disregarded.</li>
        </ul>
        Best Chances of Winning:
        <ul>
        	<li>Bookmark our page so you can enter the contest everyday quicker!</li>
        	<li>Be witty and creative! Make your tweet/review stand out.</li>
        	<li>Even if you don't win, sign up for our <a href="/member_program.asp" target="_blank">Rewards Program</a> to collect WEbucks for each tweet/review you post!</li>
        	<li>Only enter once per day: Twitter accounts with multiple product tweets in one day will be disqualified. Email accounts with multiple product reviews in one day will be disqualified.</li>
        </ul>
        Announcements:<br />
        Winners will be announced daily on <a href="http://www.facebook.com/wirelessemporium" target="_blank">Wireless Emporium's Facebook page</a> and notified via twitter/email.
        <br /><br />
        Good luck and Let Freedom Ring!
        <br /><br />
    </div>
</div>