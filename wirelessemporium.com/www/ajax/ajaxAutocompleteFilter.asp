<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/Framework/Utility/Static.asp"-->
<!--#include virtual="/includes/admin/json_2.0.4.asp"-->
<!--#include virtual="/includes/admin/json_util_0.1.1.asp"-->
<%	
call getDBConn(oConn)

brandid = prepInt(request.QueryString("brandid"))
modelid = prepInt(request.QueryString("modelid"))
typeid = prepInt(request.QueryString("typeid"))
siteid = prepInt(request.QueryString("siteid"))
searchType = prepStr(request.QueryString("sType"))
term = prepStr(request.QueryString("term"))

select case siteid
	case 0 
		sqlPrice = "a.price_our > 0"
		itemdesc = "itemdesc"
	case 1 
		sqlPrice = "a.price_ca > 0"
		itemdesc = "itemdesc_ca"
	case 2 
		sqlPrice = "a.price_co > 0"
		itemdesc = "itemdesc_co"
	case 3 
		sqlPrice = "a.price_ps > 0"
		itemdesc = "itemdesc_ps"
	case 10 
		sqlPrice = "(a.price_our > 0 or a.price_er > 0)"
		itemdesc = "itemdesc"
end select


strTerm = ""
if term <> "" then strTerm = trim(replace(replace(term, "+", " "), "'", "''"))

if strTerm <> "" then
	select case lcase(searchType)
		case "c1"
			sql = 	"select	a.slave id, a.slave + ': ' + replace(replace(replace(replace(replace(replace(b.itemdesc, '""', ''''), '[', ' '), ']', ' '), ',', ' '), '{', ' '), '}', ' ') value" & vbcrlf & _
					"from	xproductcolormatrix a join we_items b" & vbcrlf & _
					"	on	a.slave = b.partnumber" & vbcrlf & _
					"where	b.master = 1" & vbcrlf & _
					"	and	(a.slave like '%" & strTerm & "%' or b.itemdesc like '%" & strTerm & "%')" & vbcrlf & _
					"order by 1"
			session("errorSQL") = sql
			QueryToJSON(oConn, sql).Flush
		case "c2"
			sql = 	"select	top 20 partnumber id, itempic, itemdesc value" & vbcrlf & _
					"from	we_items" & vbcrlf & _
					"where	partnumber like '%" & strTerm & "%'" & vbcrlf & _
					"	and	master = 1" & vbcrlf & _
					"	and	typeid = '" & typeid & "'" & vbcrlf & _
					"order by 1"
			session("errorSQL") = sql
			QueryToJSON(oConn, sql).Flush
		case "amazon_cogs"
			sql = 	"select	distinct top 20 partnumber_we id, partnumber_we value" & vbcrlf & _
					"from	we_amazonProducts" & vbcrlf & _
					"where	partnumber_we like '%" & strTerm & "%'"
			session("errorSQL") = sql
			QueryToJSON(oConn, sql).Flush
		case "porder_item"
			select case siteid
				case 0 : sqlPrice = "price_our > 0" & vbcrlf
				case 1 : sqlPrice = "price_ca > 0" & vbcrlf
				case 2 : sqlPrice = "price_co > 0" & vbcrlf
				case 3 : sqlPrice = "price_ps > 0" & vbcrlf
				case 10 : sqlPrice = "(price_our > 0 or price_er > 0)" & vbcrlf
			end select
			sql = 	"select	top 5 itemid id, partnumber, itempic, itemdesc value" & vbcrlf & _
					"from	we_items" & vbcrlf & _
					"where	itemid = '" & strTerm & "'" & vbcrlf & _
					"	and	hidelive = 0 " & vbcrlf & _
					"	and	" & sqlPrice & vbcrlf & _
					"order by itemid desc"
			session("errorSQL") = sql
			QueryToJSON(oConn, sql).Flush
		case "porder_item2"	'partnumber & description
			sql = 	"select	top 10 a.partnumber id, a.itempic, a." & itemdesc & " value" & vbcrlf & _
					"from	we_items a join v_subtypematrix v with (nolock)" & vbcrlf & _
					"	on	a.subtypeid = v.subtypeid " & vbcrlf & _
					"where	(a.partnumber like '%" & strTerm & "%' or a.itemdesc like '%" & strTerm & "%')" & vbcrlf & _
					"	and	a.hidelive = 0 " & vbcrlf & _
					"	and	" & sqlPrice & vbcrlf
			select case typeid
				case 5
					sql = sql & "	and v.typeid = 5" & vbcrlf
				case 8
					sql = sql & "	and v.typeid = 8" & vbcrlf
				case 16
					if brandid > 0 then
						sql = sql & "	and brandid = '" & brandid & "'" & vbcrlf
					end if
				case else
					if brandid > 0 then sql = sql & "	and brandid = '" & brandid & "'" & vbcrlf
					if modelid > 0 then	sql = sql & "	and modelid = '" & modelid & "'" & vbcrlf
					if typeid > 0 then sql = sql & "	and v.typeid = '" & typeid & "'" & vbcrlf
			end select
			session("errorSQL") = sql
			QueryToJSON(oConn, sql).Flush
	end select
end if	

call CloseConn(oConn)
%>