<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	dim newMobile : newMobile = prepInt(request.QueryString("newMobile"))
	
	call getDBConn(oConn)
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->
<!--#include virtual="/cart/UPSxml_d.asp"-->
<%	
	if isnumeric(request.QueryString("zip")) then
		sZip = prepInt(request.QueryString("zip"))
	else
		sZip = prepStr(request.QueryString("zip"))
	end if
	sWeight = prepStr(request.QueryString("weight"))
	shiptype = prepInt(request.QueryString("shiptype"))
	nTotalQuantity = prepInt(request.QueryString("nTotalQuantity"))
	nSubTotal = prepInt(request.QueryString("nSubTotal"))
	useFunction = prepStr(request.QueryString("useFunction"))
	international = prepInt(request.QueryString("international"))	
	
	dim shipcost0, shipcost2, shipcost3, shipcost4, shipcost5
	shipcost0 = 0.00
	if now > cdate("12/13/2011") and date < cdate("12/19/2011") and prepInt(nSubTotal) > 29.99 then
		shipcost2 = 0
		shiptype = 2	
	else
		shipcost2 = 6.99
	end if
	shipcost3 = 24.99
	shipcost4 = 0.00
	'Canadian shipping Cost
	shipcost5 = 21.99	
	
	if instr(sZip,"-") > 0 then
		zipArray = split(sZip,"-")
		useZip = zipArray(0)
	else
		useZip = sZip
	end if
	
	do while len(useZip) < 5
		useZip = "0" & useZip
	loop
	
	if not isnumeric(useZip) then international = 1
	
	if international = 0 then
	%>
    <div id="ship_0" class="<%if shiptype = 0 then%>shippingRowSelected<%else%>shippingRow<%end if%>" onclick="onShippingOption(0)">
        <div class="method">
            <div class="fl"><input type="radio" name="shiptype" value="0" <%if shiptype = 0 then%>checked<%end if%> /></div>
            <div class="desc fl" style="padding:2px 0px 0px 2px;"><strong>FREE:</strong> USPS First Class</div>
        </div>
        <div class="days">4-10 Business Days</div>
        <div class="price"><%=formatcurrency(shipcost0)%></div>
        <input type="hidden" id="id_shipcost0" name="shipcost0" value="<%=shipcost0%>" />
    </div>
    <div id="ship_2" class="<%if shiptype = 2 then%>shippingRowSelected<%else%>shippingRow<%end if%>" onclick="onShippingOption(2)">
        <div class="method">
            <div class="fl"><input type="radio" name="shiptype" value="2" <%if shiptype = 2 then%>checked<%end if%> /></div>
            <div class="desc fl" style="padding:2px 0px 0px 2px;">USPS Priority Mail</div>
        </div>
        <div class="days">2-4 Business Days</div>
        <div class="price"><%=formatcurrency(shipcost2)%></div>
        <input type="hidden" id="id_shipcost2" name="shipcost2" value="<%=shipcost2%>" />
    </div>
    <div id="ship_3" class="<%if shiptype = 3 then%>shippingRowSelected<%else%>shippingRow<%end if%>" onclick="onShippingOption(3)">
        <div class="method">
            <div class="fl"><input type="radio" name="shiptype" value="3" <%if shiptype = 3 then%>checked<%end if%> /></div>
            <div class="desc fl" style="padding:2px 0px 0px 2px;">USPS Express Mail</div>
        </div>
        <div class="days">1-2 Business Days</div>
        <div class="price"><%=formatcurrency(shipcost3)%></div>
        <input type="hidden" id="id_shipcost3" name="shipcost3" value="<%=shipcost3%>" />
    </div>
    <%
	else
	%>
    <div id="ship_0" class="shippingRowSelected" onclick="onShippingOption(0)">
        <div class="method">
            <div class="fl"><input type="radio" name="shiptype" checked="checked" value="0" /></div>
            <div class="fl" style="padding:2px 0px 0px 2px;"><strong>FREE:</strong> USPS First Class Int'l</div>
        </div>
        <div class="days">8-12 Business Days</div>
        <div class="price"><%=formatcurrency(shipcost0)%></div>
        <input type="hidden" id="id_shipcost0" name="shipcost0" value="<%=shipcost0%>" />
    </div>
    <div id="ship_2" class="shippingRow" onclick="onShippingOption(2)">
        <div class="method">
            <div class="fl"><input type="radio" name="shiptype" value="2" /></div>
            <div class="fl" style="padding:2px 0px 0px 2px;">USPS Priority Int'l</div>
        </div>
        <div class="days">3-8 Business Days</div>
        <div class="price"><%=formatcurrency(shipcost5)%></div>
        <input type="hidden" id="id_shipcost2" name="shipcost2" value="<%=shipcost5%>" />
    </div>
    <%
	end if

	if newMobile = 1 then
		response.Write("got the new mobile")
		call CloseConn(oConn)
		response.End()
	end if
	
	if prepStr(session("sr_token")) = "" and international = 0 then
		on error resume next
		if useZip <> "" then
			if newMobile = 1 then
				response.write UPScodeMobile(useZip,sWeight,shiptype,nTotalQuantity,0,0)
			elseif useFunction = "" then
				response.write UPScode(useZip,sWeight,shiptype,nTotalQuantity,0,0)
			elseif useFunction = "upscode2" then
				response.write UPScode2(useZip,sWeight,shiptype,nTotalQuantity,0,0)
			elseif useFunction = "getupsshipping" then
				response.write getUPSShipping(useZip,sWeight,shiptype,nTotalQuantity,0,0)
			end if
		end if
		on error goto 0
		dim shipcost
		shipcost = request.form("shipcost" & request.form("shiptype"))
	else
		response.Write("&nbsp;")
	end if	
	
	call CloseConn(oConn)
%>