<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	
	call getDBConn(oConn)
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	curVal = prepStr(request.QueryString("curVal"))
	
	if curVal = "" then
%>
<div></div>
<%
	end if
	
	'sql = "select top 10 modelName from we_models where modelName like '%" & curVal & "%' order by modelName"
	sql	=	"select	top 10 isnull(b.brandname, 'Universal') brandname, modelName" & vbcrlf & _
			"from	we_models a left outer join we_brands b" & vbcrlf & _
			"	on	a.brandid = b.brandid" & vbcrlf & _
			"where	modelName like '%" & curVal & "%'" & vbcrlf & _ 
			"order by modelName"
	session("errorSQL") = sql
	Set rs = oConn.execute(sql)
%>
<div style="background-color:#FFF; border:1px solid #CCC; position:absolute; bottom:-10; right:-46px; width:200px;">
	<%
	do while not rs.EOF
		setValue = rs("brandname") & " " & rs("modelName")
	%>
    <div style="cursor:pointer; width:100%; text-align:left;" onmouseover="this.style.backgroundColor='#CCFFFF';" onmouseout="this.style.backgroundColor='#ffffff';" onclick="searchHelpSet('<%=setValue%>')"><%=rs("modelName")%></div>
    <%
		rs.movenext
	loop
	%>
</div>
<%
	call CloseConn(oConn)
%>