<%
	response.buffer = true
	response.expires = -1

	function getRatingStar(rating)
		dim nRating 
		dim strRatingImg
		nRating = cint(rating)
		strRatingImg = 	"<img src=""/images/product/review-star-2-empty.png"" border=""0"" width=""25"" height=""25"" />" & vbcrlf & _
						"<img src=""/images/product/review-star-2-empty.png"" border=""0"" width=""25"" height=""25"" />" & vbcrlf & _
						"<img src=""/images/product/review-star-2-empty.png"" border=""0"" width=""25"" height=""25"" />" & vbcrlf & _
						"<img src=""/images/product/review-star-2-empty.png"" border=""0"" width=""25"" height=""25"" />" & vbcrlf & _
						"<img src=""/images/product/review-star-2-empty.png"" border=""0"" width=""25"" height=""25"" />"
		
		if nRating > 0 or nRating <=5 then
			strRatingImg = ""
			for i=1 to 5
				if i <= nRating then 
					strRatingImg = strRatingImg & "<img src=""/images/product/review-star-2-full.png"" border=""0"" width=""25"" height=""25"" style=""margin-right:2px;"" />"
				elseif ((i - 1) + .5) <= nRating then 
					strRatingImg = strRatingImg & "<img src=""/images/product/review-star-2-half.png"" border=""0"" width=""25"" height=""25"" style=""margin-right:2px;"" />"
				else
					strRatingImg = strRatingImg & "<img src=""/images/product/review-star-2-empty.png"" border=""0"" width=""25"" height=""25"" style=""margin-right:2px;"" />"
				end if	
			next	
		end if
		
		getRatingStar = strRatingImg
	end function
%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	
	call getDBConn(oConn)
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<div style="width:95%; padding-left:15px; padding-right:15px; ">
<%
	itemID = prepInt(request.QueryString("itemID"))
	partNumber = prepStr(request.QueryString("partNumber"))
	
    'MG 2013.03.13 Task 1634 -- Display review accross simliar products.
	sql = "EXEC sp_GetFullReview  " & itemID 
	'sql = "select rating, nickname, reviewTitle, convert(varchar(10), datetimeentd, 20) datetimeentd, review from we_Reviews where (itemID = " & itemID & " or PartNumbers like '%" & partNumber & "%') and approved = 1"
	session("errorSQL") = sql
	Set rs = oConn.execute(sql)
	
	if not rs.eof then
		lap = 0
		do while not rs.EOF
			lap = lap + 1		
			addlStyle = "border-top:1px solid #ccc;"
			if lap = 1 then
				addlStyle = ""
			end if

			if lap > 3 then
				addlStyle = addlStyle & " display:none;"
			end if
			%>
			<div id="id_childReview_<%=lap%>" style="float:left; width:100%; padding:10px 0px 10px 0px; <%=addlStyle%>">
				<div style="float:left; width:100%;"><%=getRatingStar(rs("rating"))%></div>
				<div style="float:left; font-size:20px; width:510px; color:#333; padding-top:10px;"><%=rs("reviewTitle")%></div>
				<div style="float:left; color:#555; width:200px; padding-top:10px; text-align:right;"><%=rs("datetimeentd")%></div>
				<div style="float:left; width:100%; padding-top:10px; font-size:14px; font-family:Arial, Helvetica, sans-serif;"><%=rs("review")%></div>
				<div style="float:left; width:100%; padding-top:10px; color:#666;"><span style="color:#333; font-weight:bold;">REVIEWED BY</span>&nbsp;&nbsp;<%=rs("nickname")%></div>
			</div>
			<%
			rs.movenext
		loop
	end if
	
	call CloseConn(oConn)
%>
</div>