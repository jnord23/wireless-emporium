<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/Framework/Data/Const.asp"-->
<!--#include virtual="/Framework/Utility/Static.asp"-->
<script type='text/javascript'>
	document.write(unescape("%3Cscript%20src='"+(('https:' == document.location.protocol ? 'https://' : 'http://') + 'd3aa0ztdn3oibi.cloudfront.net/javascripts/ff.loyalty.widget.js')+"'%20type='text/javascript'%3E%3C/script%3E"));
</script>
<script>_ffLoyalty.initialize("<%=WE_500_ACCID%>");</script>
<%	
call getDBConn(oConn)

email = prepStr(request.QueryString("email"))
if instr(trim(email), "@") > 0 and instr(trim(email), " ") = 0 and instr(trim(email), ".@") = 0 then
	if isValidEmail(email) then
		sql = "exec sp_emailSub 0,'" & email & "',''"
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	%>
    <img src="http://loyalty.500friends.com/api/record.gif?uuid=<%=WE_500_ACCID%>&email=<%=email%>&type=signup" style="position: absolute; left: -10px; visibility: hidden;"/>
    <%
end if

call CloseConn(oConn)

Function isValidEmail(emailAddress)
	'Declare variables 
	Dim ValidEmail, emailParts, iLoopCounter, emailChar, acceptableChars, last2Chars 
	ValidEmail = True 'set the default result to True
	'acceptableChars are the characters that we will allow in our email
	acceptableChars="abcdefghijklmnopqrstuvwxyz.-_@"
	'use the Split function to create an array with the @ as the separator
	'so if your email was test@tester.com the email would be split into an array
	'with the first array element holding "test" and the second "tester.com"
	emailParts = Split(emailAddress, "@")
	'check to make sure that there is only 1 @ and that there are 2 parts
	'remember arrays are zero based
	'Using the UBound function will return the highest element in the array
	'So if it's a valid email the UBound function will return 1, i.e. 0 start
	If UBound(emailParts) <> 1 Then
	ValidEmail = false
	Else 
	'Check the length of each part of the email address
	'first part can be just one character, 2nd part must be atleast 4
	If Len(emailParts(0))<1 OR Len(emailParts(1))<4 Then
	ValidEmail = false
	End If
	'check first character on the left part isn't a "." using Left function
	If Left(emailParts(0), 1)="." Then
	ValidEmail = false
	End If 
	'check that there is a . in the second part of the email address - .com
	If InStr(emailParts(1), ".") <= 0 Then
	ValidEmail = false 
	End If 
	'We know there's a . now make sure there are 2 characters after the . for valid email
	last2Chars=Right(emailParts(1),2)
	If InStr(last2chars,".") Then
	ValidEmail = false
	End If
	'check that there shouldn't be a _ in the second part of the email address 
	If InStr(emailParts(1), "_") >0 Then
	ValidEmail = false 
	End If 
	End If
	'loop through each character of email
	For iLoopCounter = 1 to Len(emailAddress)
	'Use Lcase & Mid functions, Mid function used to return each individual character
	'in the email, and then Lcase converts it into lowercase
	emailChar = Lcase(Mid(emailAddress, iLoopCounter, 1)) 
	'Check if the emailAddress characters are acceptable
	If InStr(acceptableChars, emailChar) = 0 and Not IsNumeric(emailChar) Then
	ValidEmail = false
	End if
	Next
	'check if there is 2 . in a row
	If InStr(emailAddress, "..") > 0 Then
	ValidEmail=false
	End If
	'check if there is @. in a row
	If InStr(emailAddress, "@.") > 0 Then
	ValidEmail=false
	End If 
	isValidEmail=ValidEmail
End function
%>