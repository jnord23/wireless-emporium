<%
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

'ORDER COMPLETE CLEAR CHECKOUT COOKIES
on error resume next
response.Cookies("checkoutPage") = 0
response.Cookies("checkoutPage").expires = now
on error goto 0

HTTP_REFERER = request.ServerVariables("HTTP_X_REWRITE_URL")
if instr(request.ServerVariables("SERVER_NAME"),"staging.") < 1 then
	stagingSite = 0
else
	stagingSite = 1
end if
noCommentBox = true
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_Webloyalty.asp"-->
<!--#include virtual="/includes/asp/inc_GetMySession.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/framework/utility/outOfStock.asp"-->
<!--#include virtual="/framework/utility/readTextFile.asp"-->
<%
dim nAccountID, nOrderID, nOrderGrandTotal, nOrderSubTotal
if not isNumeric(request.querystring("a")) or not isNumeric(request.querystring("o")) or not isNumeric(request.querystring("d")) or not isNumeric(request.querystring("c")) then
	'response.redirect("/")
	'response.end
end if
nAccountID = prepInt(request.querystring("a"))
nOrderID = prepInt(request.querystring("o"))
nOrderGrandTotal = prepInt(request.querystring("d"))
nOrderSubTotal = prepInt(request.querystring("c"))
nOrderTax = request.QueryString("t")
nShipFee = request.QueryString("s")
mobileOrder = prepInt(request.QueryString("mobileOrder"))
bCustomCase = false

SQL = 	"	SELECT 	b.bCity, b.bState, b.bCountry, a.ordershippingfee, a.orderTax, A.mobileSite " & vbcrlf & _
		"	FROM 	we_orders A left join we_accounts b " & vbcrlf & _
		"		on 	a.accountID = b.accountID " & vbcrlf & _
		"	WHERE A.orderid = '" & nOrderID & "' AND A.accountid = '" & nAccountID & "'"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if RS.eof then
	RS.close
	cdo_from = "Automatic E-Mail from Wirelessemporium.com<sales@wirelessemporium.com>"
	cdo_subject = "Order Not Found"
	cdo_body = "<p>" & SQL & "</p>"
	cdo_to = "webmaster@wirelessemporium.com"
	'CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
	response.redirect("/")
	'response.end
else
	nOrderCity = RS("bCity")
	nOrderState = RS("bState")
	nOrderCountry = RS("bCountry")
	if nOrderCountry = "US" then nOrderCountry = "USA"
	if nOrderCountry = "CA" then nOrderCountry = "Canada"
	nOrderShippingFee = RS("ordershippingfee")
	nOrderTax = RS("orderTax")
	'mobileOrder = RS("mobileSite")
end if

sql = "select a.itemid, b.PartNumber, c.musicSkinsID from we_orderdetails a left join we_Items b on a.itemid = b.itemID left join we_items_musicskins c on a.itemid = c.id where a.partNumber is null and a.postpurchase = 1"
session("errorSQL") = sql
set orderRS = oConn.execute(sql)

do while not orderRS.EOF
	itemID = orderRS("itemID")
	partNumber = orderRS("partNumber")
	musicSkinsID = orderRS("musicSkinsID")
	
	if isnull(partNumber) then
		if isnull(musicSkinsID) then
			sql = "update we_orderdetails set partNumber = 'DELETED ITEM' where itemID = " & itemID
		else
			sql = "update we_orderdetails set partNumber = '" & musicSkinsID & "' where itemID = " & itemID
		end if
	else	
		sql = "update we_orderdetails set partNumber = '" & partNumber & "' where itemID = " & itemID
		if instr(partNumber, "WCD-") > 0 then bCustomCase = true
	end if
	session("errorSQL")
	oConn.execute(sql)
	
	orderRS.movenext
loop

if prepInt(session("zeroOrder")) = 1 or prepInt(nOrderGrandTotal) = 0 then
	cdo_from = "zeroOrder@wirelessemporium.com"
	cdo_subject = "Order for zero dollars"
	cdo_body = "orderID:" & nOrderID
	cdo_to = "terry@wirelessemporium.com,jon@wirelessemporium.com,ruben@wirelessemporium.com"
	CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
	response.Write("<!-- Zero Dollar Email Sent -->")
end if

' Adjust Inventory
'dim nProdQuantity, nPartNumber
dim decreaseSQL, RS2
SQL = 	"select	c.typeName, b.itemDesc, B.price_our, B.typeID, A.quantity, B.vendor, B.PartNumber, B.ItemKit_NEW, (select count(*) from we_invRecord where orderID = " & nOrderID & " and itemid = a.itemid) as invAdjust" & vbcrlf & _
		"from	we_orderdetails A inner join we_items B " & vbcrlf & _
		"	ON 	A.itemID=B.itemID left join we_Types c " & vbcrlf & _
		"	on 	b.typeID = c.typeID " & vbcrlf & _
		"WHERE A.orderID = '" & nOrderID & "' and a.postpurchase = 1"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
wgItems = ""

dim ajustInv
ajustInv = 1
if RS.EOF then
	ajustInv = 0
else
	if cdbl(RS("invAdjust")) > 0 then ajustInv = 0
end if

rscycle = 0
productLoop = 0
if ajustInv = 1 then
	do until RS.eof
		productLoop = productLoop + 1
		if wgItems = "" then
			if rs("typeID") = 16 then
				wgItems = 7479 & "::" & rs("price_our")
			else
				wgItems = 7468 & "::" & rs("price_our")
			end if
		else
			if rs("typeID") = 16 then
				wgItems = wgItems & "|" & 7479 & "::" & rs("price_our")
			else
				wgItems = wgItems & "|" & 7468 & "::" & rs("price_our")
			end if
		end if
		nProdQuantity = RS("quantity")
		nPartNumber = RS("PartNumber")
		sVendor = RS("vendor")
		' Updates inv_qty for the Master Part (item with the same Part Number as the item purchased which does not have -1 inv_qty)
		' If the inv_qty would be adjusted to less than or equal to zero, then all items with the same Part Number as the item purchased are set to zero
		if isNull(RS("ItemKit_NEW")) then
			SQL = "SELECT top 1 itemID,typeID,inv_qty FROM we_items WHERE PartNumber = '" & nPartNumber & "' AND master = 1"
		else
			SQL = "SELECT b.itemID,a.typeID,a.partNumber,b.inv_qty FROM we_items a left join we_items b on a.partNumber = b.partNumber and b.master = 1 WHERE a.itemID IN (" & RS("ItemKit_NEW") & ")"
		end if
		session("errorSQL") = SQL
		set RS2 = Server.CreateObject("ADODB.Recordset")
		RS2.open SQL, oConn, 0, 1
		do until RS2.eof
			curQty = RS2("inv_qty")
			if curQty - nProdQuantity > 0 then
				On Error Resume Next
				if mobileOrder = 1 then
					sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & RS2("itemID") & "," & curQty & "," & nProdQuantity & "," & nOrderID & ",0,'WE Mobile Post Purchase Order','" & now & "')"
				else
					sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & RS2("itemID") & "," & curQty & "," & nProdQuantity & "," & nOrderID & ",0,'WE Customer Post Purchase Order','" & now & "')"
				end if
				session("errorSQL") = sql
				oConn.execute(sql)
				On Error Goto 0
				
				decreaseSQL = "UPDATE we_items SET inv_qty = inv_qty - " & nProdQuantity & " WHERE itemID = '" & RS2("itemID") & "' and master = 1"
				'response.write "<p>decreaseSQL = " & decreaseSQL & "</p>" & vbcrlf
				session("errorSQL") = decreaseSql
				oConn.execute(decreaseSQL)
			else
				On Error Resume Next
				if mobileOrder = 1 then
					sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & RS2("itemID") & "," & curQty & "," & nProdQuantity & "," & nOrderID & ",0,'WE Mobile Post Purchase Order *Out of Stock*','" & now & "')"
				else
					sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & RS2("itemID") & "," & curQty & "," & nProdQuantity & "," & nOrderID & ",0,'WE Customer Post Purchase Order *Out of Stock*','" & now & "')"
				end if
				session("errorSQL") = sql
				oConn.execute(sql)
				On Error Goto 0
				
				decreaseSQL = "UPDATE we_items SET inv_qty = 0 WHERE master = 1 and PartNumber = '" & nPartNumber & "'"
	
				if RS2("typeID") <> 3 and sVendor <> "CM" and sVendor <> "DS" and sVendor <> "MLD" then
					' Send zero-inventory e-mail
					outOfStockEmail nPartNumber,curQty,nProdQuantity
				end if
				session("errorSQL") = sql
				oConn.execute(decreaseSQL)
			end if
			RS2.movenext
		loop
		RS2.close
		set RS2 = nothing
		RS.movenext
		rscycle = 1
	loop
	if productLoop > 0 then rs.movefirst
end if


'UPDATE we_itemsSiteReady table
on error resume next
	sql = 	"select	distinct b.modelid" & vbcrlf & _
			"from	we_orderdetails a join we_items b" & vbcrlf & _
			"	on	a.itemid = b.itemid" & vbcrlf & _
			"where	a.orderid = '" & nOrderID & "' and a.postpurchase = 1"
	session("errorSQL") = sql
	set rsSiteReady = oConn.execute(sql)
	if not rsSiteReady.eof then
		do until rsSiteReady.eof
			oConn.execute("sp_createProductListByModelID " & rsSiteReady("modelid"))
			rsSiteReady.movenext
		loop
	end if
on error goto 0


dim thisEmail, incEmail
incEmail = true
if nOrderID > 0 then
	emailReceipt = 1
%>
<!--#include virtual="/includes/asp/inc_receipt.asp"-->
<%
end if

for thisEmail = 1 to 2
	if extOrderType = 1 then
		if thisEmail = 1 then
			sFromName = "Automatic E-Mail from WirelessEmporium.com"
			sFromAddress = "sales@WirelessEmporium.com"
			sAddRecipient1 = sEmail
			sSubject = "Wireless Emporium PAYPAL Order Confirmation"
		else
			sFromName = "Automatic E-Mail from WirelessEmporium.com"
			sFromAddress = "sales@WirelessEmporium.com"
			sSubject = "A New PAYPAL Order from Wireless Emporium"
		end if
	else
		if thisEmail = 1 then
			sFromName = "Automatic E-Mail from WirelessEmporium.com"
			sFromAddress = "sales@WirelessEmporium.com"
			sAddRecipient1 = sEmail
			sSubject = "Wireless Emporium Order Revised Confirmation"
		else
			sFromName = "Automatic E-Mail from WirelessEmporium.com"
			sFromAddress = "sales@WirelessEmporium.com"
			sSubject = "A New Order from Wireless Emporium"
		end if
	end if
	cdo_from = sFromName & "<" & sFromAddress & ">"
	cdo_subject = sSubject
	cdo_body = ReceiptText
	on error resume next
		if thisEmail = 1 then
			' to Customer
			cdo_to = "" & sAddRecipient1 & ""
			'cdo_to = "webmaster@wirelessemporium.com"
			CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
			if Err.Number <> 0 then
				session("mailError") = "<p><b>Your order is being fulfilled and shipped to the shipping address you provided.</b></p>"
				session("mailError") = session("mailError") & "<p>However, when attempting to e-mail your Order Confirmation to <b>" & cdo_to & "</b>, "
				session("mailError") = session("mailError") & "we received the following error from your e-mail provider:<br>" & Err.Description & "</p>"
				session("mailError") = session("mailError") & "<p>It is possible that you may not receive any e-mails from us (such as Shipping Confirmation, etc.) "
				session("mailError") = session("mailError") & "if we continue to encounter problems with this e-mail address.</p>"
				session("mailError") = session("mailError") & "<p>Please be assured, however, that your order will be fulfilled and shipped.</p>"
			else
				strSQL = "UPDATE we_orders SET emailSent = 'yes' WHERE orderid = '" & nOrderID & "'"
				oConn.execute strSQL
			end if
		else
			' to Wireless Emporium recipients
			'cdo_to = "shipping@wirelessemporium.com"
			'CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		end if
	on error goto 0
next

if strOrderType <> "eBillme" then
	dim updateRS
	SQL = "UPDATE we_orders SET approved = 1 WHERE orderid = '" & nOrderID & "'"
	set updateRS = oConn.execute(SQL)
end if

if mobileOrder = 1 then
	gaqAccount = "UA-1311669-2"
	storeName = "Wireless Emporium Mobile"
	mvtGaqAccount = "UA-36271972-1"
else
	gaqAccount = "UA-1311669-1"
	storeName = "Wireless Emporium"
	mvtGaqAccount = "UA-36271972-1"
end if

sql =	"select b.partNumber, b.itemDesc, c.typeName, b.price_our, a.quantity " &_
		"from we_orderDetails a " &_
			"left join we_Items b on a.itemID = b.itemID " &_
			"left join we_types c on b.typeID = c.typeID " &_
		"where orderID = '" & nOrderID & "' and a.postpurchase = 1"
session("errorSQL") = sql
set jsRS = oConn.execute(sql)

jsProductLoop = 0
transItem = ""
do while not jsRS.EOF
	jsProductLoop = jsProductLoop + 1
	transItem = "_gaq.push(['_addItem','" & nOrderID & "','" & jsRS("partNumber") & "','" & jsRS("itemDesc") & "','" & jsRS("typeName") & "','" & jsRS("price_our") & "','" & jsRS("quantity") & "']);_gaq.push(['_trackTrans']);"
	jsRS.movenext
loop
%>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script type="text/javascript">
		//mobileOrder: <%=mobileOrder%>
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', '<%=gaqAccount%>']);
		_gaq.push(['_trackPageview']);
		  _gaq.push(['_addTrans',
		  '<%=nOrderID%>',           // order ID - required
		  '<%=storeName%>',  // affiliation or store name
		  '<%=nOrderGrandTotal%>',          // total - required
		  '<%=nOrderTax%>',           // tax
		  '<%=nOrderShippingFee%>',              // shipping
		  '<%=nOrderCity%>',       // city
		  '<%=nOrderState%>',     // state or province
		  '<%=nOrderCountry%>'             // country
		]);
		<%=transItem%>
	  
	   (function() {
		  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
		
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-36271972-1']);
		_gaq.push(['_trackPageview']);
		  _gaq.push(['_addTrans',
		  '<%=nOrderID%>',           // order ID - required
		  '<%=storeName%>',  // affiliation or store name
		  '<%=nOrderGrandTotal%>',          // total - required
		  '<%=nOrderTax%>',           // tax
		  '<%=nOrderShippingFee%>',              // shipping
		  '<%=nOrderCity%>',       // city
		  '<%=nOrderState%>',     // state or province
		  '<%=nOrderCountry%>'             // country
		]);
		<%=transItem%>
	  
	   (function() {
		  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
		
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-38929999-1']);
		_gaq.push(['_trackPageview']);
		  _gaq.push(['_addTrans',
		  '<%=nOrderID%>',           // order ID - required
		  'desktop/mobile',  // affiliation or store name
		  '<%=nOrderGrandTotal%>',          // total - required
		  '<%=nOrderTax%>',           // tax
		  '<%=nOrderShippingFee%>',              // shipping
		  '<%=nOrderCity%>',       // city
		  '<%=nOrderState%>',     // state or province
		  '<%=nOrderCountry%>'             // country
		]);
		<%=transItem%>
	  
	   (function() {
		  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	  </script>
	<% if Application("shopRunner") = 1 then %>
    <script type="text/javascript">
		_shoprunner_com.orderID = '<%=nOrderID%>';
		_shoprunner_com.tokenID = '<%=shopRunnerID%>';
		_shoprunner_com.totalOrderAmount = <%=nOrderGrandTotal%>;
		_shoprunner_com.submitConfirmationData() ;
	</script>
    <% end if %>
    <title>Your Transaction is Being Processed...</title>
    <script type="text/javascript" src="/cart/includes/jscookie.js"></script>
    <% call printPixel(1) %>
</head>

<%
'----------------------------------------------
'Tracking for troubleshooting the checkout page
'Determine SessionID
if request.Cookies("mySession") <> "" then
	thisHereSession = request.Cookies("mySession")
elseif request.QueryString("passSessionID") <> "" then
	thisHereSession = request.QueryString("passSessionID")
else
	thisHereSession = 0
end if
thisHereHost = request.ServerVariables("HTTP_HOST")
%>
<script src="/framework/userinterface/js/ajax.js"></script>
<script>
function trackingLoad() {
	ajax('/ajax/ajaxTracking.asp?sessionID=<%=thisHereSession%>&visitedComplete=1','trackingZone');
}

setTimeout("trackingLoad()",1000)
</script>
<div id="trackingZone"<% if thisHereHost <> "staging.wirelessemporium.com" then %> style="display:none;"<% end if %>><%=request.ServerVariables("SCRIPT_NAME")%></div>
<%
'----------------------------------------------
%>

<%
dim useMetaLink
if mobileOrder = 0 and stagingSite = 0 and instr(lcase(request.ServerVariables("SERVER_NAME")),"m.wireless") < 1 then
	useMetaLink = "https://one-time-offer.com/sg/gateway.aspx?v=0353A36353&p=92F2624222BE77B7D7F706E72FC383C3D3E3639F16D6060727&cl=F6E606&q=" & strEncryptedValue
	forwardPage = "http://" & request.ServerVariables("SERVER_NAME") & "/confirm.asp?o=" & nOrderID & "&a=" & nAccountID & "&d=" & nOrderGrandTotal & additionalQS
	
%>
<meta http-equiv="refresh" content="5;URL=https://<%=request.ServerVariables("SERVER_NAME")%>/confirm.asp?o=<%=nOrderID%>&a=<%=nAccountID%>&d=<%=emailOrderGrandTotal%>&ppd=<%=nOrderGrandTotal%><%=additionalQS%>">
<%
else
	if session("adTracking") = "AdMob" then
		additionalQS = session("additionalQS")
	else
		additionalQS = "&tracking=none"
	end if
	
	if stagingSite = 0 then
		useMetaLink = "http://m.wirelessemporium.com/confirm.htm?o=" & nOrderID & "&a=" & nAccountID & "&d=" & nOrderGrandTotal & additionalQS
		forwardPage = "http://m.wirelessemporium.com/confirm.htm?o=" & nOrderID & "&a=" & nAccountID & "&d=" & nOrderGrandTotal & additionalQS
%>
<meta http-equiv="refresh" content="2;URL=http://m.wirelessemporium.com/confirm.htm?o=<%=nOrderID%>&a=<%=nAccountID%>&d=<%=nOrderGrandTotal%><%=additionalQS%>">
<%	else
		useMetaLink = "http://staging.wirelessemporium.com/confirm.asp?o=" & nOrderID & "&a=" & nAccountID & "&d=" & nOrderGrandTotal & additionalQS
		forwardPage = "http://staging.wirelessemporium.com/confirm.asp?o=" & nOrderID & "&a=" & nAccountID & "&d=" & nOrderGrandTotal & additionalQS
%>
<meta http-equiv="refresh" content="5;URL=http://staging.wirelessemporium.com/confirm.asp?o=<%=nOrderID%>&a=<%=nAccountID%>&d=<%=emailOrderGrandTotal%>&ppd=<%=nOrderGrandTotal%><%=additionalQS%>">
<%	end if %>
<%end if %>
<script language="javascript" type="text/javascript">
window.onload = function()
{
	eraseCookie("promoPopCnt");
};
</script>
<body>
<% call printPixel(2) %>
<%
' CONFIRM PAGE TAGS & PIXELS:
dim sGoogle, countPriceGrabber, strPriceGrabber, strBecome, strTurnTo, totalFetchback, ntotQty, MYnID, nItemCount
sGoogle = ""
countPriceGrabber = 0
strPriceGrabber = ""
strBecome = "var "
strTurnTo = ""
totalFetchback = 0
ntotQty = 0
MYnID = ""
nItemCount = 0

dim strGoogleSKU, strGoogleNum, strGoogleQty, strGooglePrice, strGoogleCat
strGoogleSKU = "&prdsku="
strGoogleNum = "&prdnm="
strGoogleQty = "&prdqn="
strGooglePrice = "&prdpr="
strGoogleCat = "&prcatid="

call fOpenConn()
SQL = "SELECT A.itemID, A.typeID, A.PartNumber, A.itemDesc, A.price_Our, A.itemPic, A.UPCCode, B.quantity, C.typeName, D.brandName"
SQL = SQL & " FROM ((we_items A INNER JOIN we_orderdetails B ON A.itemid = B.itemid)"
SQL = SQL & " INNER JOIN we_types C ON A.typeID = C.typeID)"
SQL = SQL & " LEFT OUTER JOIN we_brands D ON A.brandID = D.brandID"
SQL = SQL & " WHERE B.orderid = '" & nOrderId & "'"

SQL = "SELECT E.id, E.musicSkinsID, E.designName, E.image, A.itemID, A.typeID, A.PartNumber, A.itemDesc, A.price_Our, A.itemPic, A.UPCCode, B.quantity, b.musicSkins, C.typeName, D.brandName FROM ((we_orderdetails B left JOIN we_items A ON A.itemid = B.itemid) left JOIN we_types C ON A.typeID = C.typeID) left join we_items_musicSkins E on B.itemID = E.id LEFT JOIN we_brands D ON A.brandID = D.brandID WHERE B.itemID > 0 and B.orderid = '" & nOrderId & "'"

dim mytypeName, mycategory_name, myproduct_id, myproduct_name, myproduct_price, myproduct_qty
mytypeName = ""
mycategory_name = ""
myproduct_id = ""
myproduct_name = ""
myproduct_price = ""
myproduct_qty = ""

dim oRsItemDetails
session("errorSQL") = SQL
set oRsItemDetails = Server.CreateObject("ADODB.Recordset")
oRsItemDetails.open SQL, oConn, 3, 3
do until oRsItemDetails.eof
	if not isnull(oRsItemDetails("musicSkinsID")) then
		useID = oRsItemDetails("id")
		usePN = oRsItemDetails("musicSkinsID")
		useDesc = oRsItemDetails("designName")
		usePrice = 8.99
		useType = "Faceplates"
		useTypeID = 3
		useQty = oRsItemDetails("quantity")
		useUPC = ""
		useImg = oRsItemDetails("image")
	else
		useID = oRsItemDetails("itemid")
		usePN = oRsItemDetails("PartNumber")
		useDesc = oRsItemDetails("itemdesc")
		usePrice = oRsItemDetails("price_Our")
		useType = oRsItemDetails("typeName")
		useTypeID = oRsItemDetails("typeID")
		useQty = oRsItemDetails("quantity")
		useUPC = oRsItemDetails("UPCCode")
		useImg = oRsItemDetails("itemPic")
	end if
	ntotQty = ntotQty + useQty
	sGoogle = sGoogle & "pageTracker._addItem("
	sGoogle = sGoogle & """" & useID & ""","
	sGoogle = sGoogle & """" & usePN & ""","
	sGoogle = sGoogle & """" & useDesc & ""","
	sGoogle = sGoogle & """" & useType & ""","
	sGoogle = sGoogle & """" & usePrice & ""","
	sGoogle = sGoogle & """" & useQty & """"
	sGoogle = sGoogle & ");" & vbcrlf
	
	for MYcount = 1 to useQty
		MYnID = MYnID & useID & ","
	next
	
	mytypeName = mytypeName & useTypeID & ", "
	mycategory_name = mycategory_name & useType & ", "
	myproduct_id = myproduct_id & useID & ", "
	myproduct_name = myproduct_name & useDesc & ", "
	myproduct_price = myproduct_price & usePrice & ", "
	myproduct_qty = myproduct_qty & useQty & ", "
	
	countPriceGrabber = countPriceGrabber + 1
	strPriceGrabber = strPriceGrabber & "&item" & countPriceGrabber & "="
	if not isNull(oRsItemDetails("brandName")) and oRsItemDetails("brandName") <> "" then
		strPriceGrabber = strPriceGrabber & server.URLencode(oRsItemDetails("brandName")) & "||"
	else
		strPriceGrabber = strPriceGrabber & "Universal" & "||"
	end if
	strPriceGrabber = strPriceGrabber & usePrice & "|"
	strPriceGrabber = strPriceGrabber & useID & "|"
	strPriceGrabber = strPriceGrabber & useUPC & "|"
	strPriceGrabber = strPriceGrabber & useQty
	
	strBecome = strBecome & "become_item = new Object();" & vbcrlf
	strBecome = strBecome & "become_item.productid = '" & useID & "';" & vbcrlf
	strBecome = strBecome & "become_item.category = '" & useType & "';" & vbcrlf
	strBecome = strBecome & "become_item.price = '" & usePrice & "';" & vbcrlf
	strBecome = strBecome & "become_item.quantity = " & useQty & ";" & vbcrlf
	strBecome = strBecome & "become_purchased_items.push(become_item);" & vbcrlf
	
	strGoogleSKU = strGoogleSKU & useID & "^"
	strGoogleNum = strGoogleNum & "Item%20" & countPriceGrabber & "^"
	strGoogleQty = strGoogleQty & useQty & "^"
	strGooglePrice = strGooglePrice & usePrice & "^"
	strGoogleCat = strGoogleCat & server.URLencode(prepStr(useType)) & "^"
	
	strTurnTo = strTurnTo & "TurnToFeed.addFeedLineItem({title: '" & replace(replace(useDesc,"&","&amp;"),chr(34),"&quot;") & "', url: '/p-" & useID & "-" & formatSEO(useDesc) & ".asp?id=turnto',sku:'" & useID & "', price:'" & usePrice & "',itemImageUrl:'" & useImg & "'});" & vbcrlf
	
	if useTypeID = 16 then
		totalFetchback = totalFetchback + (usePrice * .238)
	else
		totalFetchback = totalFetchback + usePrice
	end if
	
	nItemCount = nItemCount + useQty
	
	oRsItemDetails.movenext
loop
set oRsItemDetails = nothing
'call fCloseConn()

if bCustomCase then
	strTurnTo = strTurnTo & "TurnToFeed.addFeedLineItem({title: 'Design Your Own Cover', url: '/custom-phone-cases',sku:'146579', price:'19.99',itemImageUrl:'customizable_mobile_protection.jpg'});" & vbcrlf
end if

if len(MYnID) > 0 then MYnID = left(MYnID,len(MYnID)-1)

response.write "<script type=""text/javascript"">" & vbcrlf
response.write "var gaJsHost = ((""https:"" == document.location.protocol) ? ""https://ssl."" : ""http://www."");" & vbcrlf
response.write "document.write(unescape(""%3Cscript src='"" + gaJsHost + ""google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E""));" & vbcrlf
response.write "</script>" & vbcrlf

' If this is an Affiliate Referral, add the appropriate tracking code
%>
<% if mobileOrder = 0 then %>
<!--#include virtual="/cart/includes/inc_trackingCodes.asp"-->
<% end if %>
<table width="100%" border="0" align="center" cellpadding="20" cellspacing="0">
	<tr>
		<td align="center">
        	<% if mobileOrder = 0 then %>
			<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
            <% end if %>
			<p>
				<strong><font color="#FF6600" size="4" face="Verdana, Arial, Helvetica, sans-serif">Your Transaction is Being Processed...</font></strong>
			</p>
			<p>
				<font face="verdana,tahoma,helvetica" size="2">VeriSign has routed, processed, and secured your payment information.
				<a href="https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.WIRELESSEMPORIUM.COM&lang=en" target="_blank"><br>More information about VeriSign.</a></font>
			</p>
			<p>
				<a href="https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.WIRELESSEMPORIUM.COM&lang=en" target="_blank"><img src="https://www.wirelessemporium.com/images/seal_m_en.gif" width="115" height="82" border="0" alt="VeriSign Secured Site"></a>
			</p>
			<p>
				Please wait while your transaction is being processed<br>
				(DO NOT HIT BACK ON YOUR BROWSER).<br>
				You will soon be taken to an order receipt page confirming your order!
                <div id="forwardLink" style="margin-left:auto; margin-right:auto; display:none;"><a href="<%=forwardPage%>">Click here if you are not forwarded to the next page</a></div>
			</p>
			<%if session("mailError") <> "" then response.write "<p>" & session("mailError") & "</p>" & vbcrlf%>
			<p>
				<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>.... Exiting Secure Server ....</strong></font>
			</p>
            <input type="hidden" id="totalPrice" value="<%=nOrderGrandTotal%>" />
		</td>
	</tr>
</table>
<script language="javascript">
	window.optimizely = window.optimizely || [];
	var revenueInCents = $("#totalPrice").val() * 100;
	window.optimizely.push(['trackEvent', 'revenue', revenueInCents]);
	
	setTimeout("document.getElementById('forwardLink').style.display=''",4000)
</script>
</body>
</html>
