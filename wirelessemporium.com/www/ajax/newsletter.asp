<%
dim fso : set fso = CreateObject("Scripting.FileSystemObject")
%>
<!--#include virtual="/Framework/Utility/Static.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_GetMySession.asp"-->
<%
call getDBConn(oConn)

dim semail, brandID, modelID, typeID, itemID, discountAmt
semail = prepStr(request.QueryString("semail"))
brandID = prepInt(request.QueryString("brandID"))
modelID = prepInt(request.QueryString("modelID"))
typeID = prepInt(request.QueryString("typeID"))
itemID = prepInt(request.QueryString("itemID"))
discountAmt = prepInt(request.QueryString("dis"))
dim b
b = cdbl(request.QueryString("b"))

if discountAmt = 25 then b = 1 else b = 0

dim newsletterMsg : newsletterMsg = ""

campaign_id = "welcome1"
promo_code = "WELCOME15"
curYear = year(date)
expDate = date+7
bSubscribed = false

if semail <> "" then
	if semail = "checkCouponUsed" then
		if prepInt(request.cookies("welcomeCouponUsed")) = 0 then
			if prepInt(session("widgetCollapsed")) = 0 then
				response.write "show_expand"
			else
				response.write "show_collapse"
			end if
		else
			response.write "hide"
		end if
		call CloseConn(oConn)
		response.end
	elseif semail = "collapsed" then
		session("widgetCollapsed") = mySession
		response.write "collapsed"
		call CloseConn(oConn)
		response.end
	elseif semail = "expanded" then
		session("widgetCollapsed") = ""
		response.write "expanded"
		call CloseConn(oConn)
		response.end
	elseif instr(trim(semail), "@") > 0 and instr(trim(semail), " ") = 0 and instr(trim(semail), ".@") = 0 then
		if isValidEmail(semail) then
			response.Cookies("customerEmail") = semail
			response.Cookies("customerEmail").expires = now + 60
			
			if instr(semail,"@wirelessemporium.com") > 0 then
				sql = "delete from we_emailOpt where email like '%@wirelessemporium.com'"
				session("errorSQL") = sql
				oConn.execute(sql)
			end if
			
			sql = "exec sp_emailSub 0,'" & semail & "',''," & brandID & "," & modelID & "," & typeID & "," & itemID & "," & discountAmt
			session("errorSQL") = sql
			set emailRS = oConn.execute(sql)
			
			if emailRS("returnValue") = "duplicate" then
				newsletterTitle = "Welcome Back!"
				newsletterMsg = "Thanks for your interest in joining our email newsletter. Our records indicate that you are already a member. Unfortunately this promotion is for new subscribers, but as a valued customer you will receive additional offers in the near future. Check your inbox for any active promotions, and to add us to your safe sender's list so you don't miss any of our emails." & _
								"<br /><br />" &_
								"Sincerely,<br />" &_
								"WirelessEmporium.com"
			else
				dim cdo_to, cdo_from, cdo_subject, cdo_body
				'dim emailHTML : emailHTML = "/images/email/blast/template/we-welcome-email.htm"
				'dim emailHTML : emailHTML = "/emails/welcome/index.htm"
				'set curHTML = fso.OpenTextFile(server.MapPath(emailHTML),1,false,0)
				
				sql = "exec sp_ppUpsell 0, 102964, 0, 30"
				session("errorSQL") = sql
				set objRsUpSell = oConn.execute(sql) 

				strUpSell = ""

				if not objRsUpSell.EOF then
					strUpSell	=	"<tr>" & vbcrlf & _
									"	<td style='background-color: #ffffff;' width='640'>" & vbcrlf & _
									"		<table width='640' cellspacing='0' cellpadding='0'>" & vbcrlf & _
									"			<tbody>" & vbcrlf & _
									"				<tr>"
							
					counter = 1
					do until objRsUpSell.EOF
						itemID		= objRsUpSell("itemID")
						itemPic		= objRsUpSell("itemPic")
						itemDesc	= objRsUpSell("itemDesc")
						useItemDesc = replace(itemDesc,"  "," ")
						itemImgPath = server.MapPath("/productpics/big/" & itemPic)
						'itemImgPath = "C:\inetpub\wwwroot\productpics\big\" & itempic
						price		= objRsUpSell("price")
						price_retail= objRsUpSell("price_retail")
						discountAmt = price_retail - price
						discount = discountAmt * 100 / price_retail
						'promoPrice	= price - discount
						
						if b = 1 then
							utm_promocode = "WELCOME25"
						else 
							utm_promocode = "WELCOME15"
						end if
						
						productLink = "http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".html?dzid=email_TRANS_RECS_" & counter & "L_" & itemID & "_" & utm_promocode & "&utm_source=internal&utm_medium=email&utm_campaign=Welcome1&utm_content=welcome&utm_promocode=" & utm_promocode
						
						set fsThumb = CreateObject("Scripting.FileSystemObject")
						if not fsThumb.FileExists(itemImgPath) then
							useImg = "/productPics/big/imagena.jpg"
							DoNotDisplay = 1
						else
							useImg = "/productPics/big/" & itemPic
						end if
						
						if counter = 3 or counter = 6 then
							style = "padding:20px 10px 0 20px;"
						else 
							style = "padding:20px 0 0 20px;"
						end if
						
						strUpSell 	= strUpSell &	"	<td width='210' style='" & style & "vertical-align:top;'>" & vbcrlf & _
														"		<table width='190' cellpadding='0' cellspacing='0'>" & vbcrlf & _
														"			<tbody>" & vbcrlf & _
														"				<tr>" & vbcrlf & _
														"					<td width='190' height='190'>" & vbcrlf & _
														"						<a href='" & productLink & "'><img src='http://www.wirelessemporium.com" & useImg & "' style='display:block;' width='190' /></a>" & vbcrlf & _
														"					</td>" & vbcrlf & _
														"				</tr>" & vbcrlf & _
														"				<tr>" & vbcrlf & _
														"					<td width='190' height='60' style=""padding:7px 0px 7px 0px;"" valign=""top"">" & vbcrlf & _
														"						<a href='" & productLink & "' target=""_blank"" style=""font-size:12px; color:#333; font-family: Arial, sans-serif;"">" & itemDesc & "</a>" & vbcrlf & _
														"					</td>" & vbcrlf & _
														"				</tr>" & vbcrlf & _											
														"				<tr>" & vbcrlf & _
														"					<td>" & vbcrlf & _
														"						<span style='color:#999;font-size:12px;color:#ccc;text-decoration:line-through;font-family:Arial, sans-serif;'>Retail Price: " & formatCurrency(price_retail) & "</span><br />" & vbcrlf & _
														"					</td>" & vbcrlf & _
														"				</tr>" & vbcrlf & _
														"				<tr>" & vbcrlf & _
														"					<td style='padding:10px 0 0 0;'>" & vbcrlf & _
														"						<span style='color:#000;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;'>Our Price:</span> <span style='color:#ff6633;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;'>" & formatCurrency(price) & "</span><br />" & vbcrlf & _
														"						<span style='color:#000;font-size:14px;font-family:Arial, sans-serif;'>You Save:</span> <span style='margin:10px 0 0 0;color:#000;font-size:14px;font-family:Arial, sans-serif;font-weight:bold;'>" & Round(discount) & "% (" & formatCurrency(discountAmt) & ")</span>" & vbcrlf & _
														"					</td>" & vbcrlf & _
														"				</tr>" & vbcrlf & _
														"				<tr>" & vbcrlf & _
														"					<td style='padding:10px 0 0 0;'>" & vbcrlf & _
														"						<table width='160' border='0' cellpadding='0' cellspacing='0'>" & vbcrlf & _
														"							<tbody>" & vbcrlf & _
														"								<tr>" & vbcrlf & _
														"									<td align='center' style='background-color:#ff6633;border-radius:16px;padding:7px 0;'>" & vbcrlf & _
														"										<a href='" & productLink & "' style='color:#fff;font-weight:bold;font-size:14px;font-family:Arial, sans-serif;text-decoration:none;'>SHOP NOW &raquo;</a>" & vbcrlf & _
														"									</td>" & vbcrlf & _
														"								</tr>" & vbcrlf & _
														"							</tbody>" & vbcrlf & _
														"						</table>" & vbcrlf & _
														"					</td>" & vbcrlf & _
														"				</tr>" & vbcrlf & _
														"			</tbody>" & vbcrlf & _
														"		</table>" & vbcrlf & _
														"	</td>"
						
						if counter = 3 then
							strUpSell = strUpSell & "						</tr>" & vbcrlf & _
										"					</tbody>" & vbcrlf & _
										"				</table>" & vbcrlf & _
										"				<br />" & vbcrlf & _
										"			</td>" & vbcrlf & _
										"		</tr>" & vbcrlf & _
										"		<tr>" & vbcrlf & _
										"		  <td style='background-color: #cccccc;' width='640' height='1'></td>" & vbcrlf & _
										"		</tr>" & vbcrlf & _
										"		<tr>" & vbcrlf & _
										"			<td style='background-color: #ffffff;padding:0 0 25px;' width='640'>" & vbcrlf & _
										"				<table width='640' cellspacing='0' cellpadding='0'>" & vbcrlf & _
										"					<tbody>" & vbcrlf & _
										"						<tr>"
							counter = counter + 1
						elseif counter = 6 then
							exit do
						else
							counter = counter + 1
						end if
						objRsUpSell.movenext
					loop
					
					strUpSell = strUpSell & "</tr>" & vbcrlf & _
								"</table>"
				end if
				
				if b = 1 then
					strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\images\email\template\we-subscribe25.htm")
				else
					strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\images\email\template\we-subscribe15.htm")
				end if
				strTemplate = replace(strTemplate, "[UPSELL]", strUpSell)
				strTemplate = replace(strTemplate, "[EXPIRY]", DateAdd("d", 7, Date()))
				
				cdo_body = strTemplate
				
				if semail = "jnord@test.com" then semail = "zach@wirelessemporium.com; jon@wirelessemporium.com"
				cdo_to = semail
				cdo_from = "Wireless Emporium<sales@wirelessemporium.com>"
				if b = 1 then
					cdo_subject = "Save 25% Off Your Order - Welcome To Wireless Emporium"
				else
					cdo_subject = "Save 15% Off Your Order - Welcome To Wireless Emporium"
				end if
				
				CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
				
				newsletterTitle = "Welcome To The Club!"
				newsletterMsg = "Thanks for joining our email newsletter. We've sent your promotion to your inbox. You are now connected to our latest news, special offers, and exclusive deals. Be sure to add us to your safe sender's list so you don't miss your special promotions."
			end if
			session("widgetCollapsed") = mySession
			response.cookies("welcomeCouponUsed") = mySession
			response.cookies("welcomeCouponUsed").expires = now+30
			bSubscribed = true
		else
			newsletterMsg = "Please enter a valid email address and try again. (" & semail & ")"
		end if
	else
		newsletterMsg = "Please make sure you enter a valid email address and try again. (" & semail & ")"
	end if
else
	newsletterMsg = "Email error, please double check that you have entered a valid email. (" & semail & ")"
end if

call CloseConn(oConn)

%>
<div class="email-container">
    <a href="javascript:closeEmailPopup();<%if bSubscribed then%>toggleEmailWidget(0);<%end if%>" class="close"></a>
    <p style="text-align: center; font-size: 32px; font-weight: normal;"><%=newsletterTitle%><br><br></p>
    <p style="text-align: left;"><%=newsletterMsg%></p>
    <div class="continue" onClick="closeEmailPopup();<%if bSubscribed then%>toggleEmailWidget(0);<%end if%>">CONTINUE SHOPPING</div>
</div>
<%

Function isValidEmail(emailAddress)
	'Declare variables 
	Dim ValidEmail, emailParts, iLoopCounter, emailChar, acceptableChars, last2Chars 
	ValidEmail = True 'set the default result to True
	'acceptableChars are the characters that we will allow in our email
	acceptableChars="abcdefghijklmnopqrstuvwxyz.-_@"
	'use the Split function to create an array with the @ as the separator
	'so if your email was test@tester.com the email would be split into an array
	'with the first array element holding "test" and the second "tester.com"
	emailParts = Split(emailAddress, "@")
	'check to make sure that there is only 1 @ and that there are 2 parts
	'remember arrays are zero based
	'Using the UBound function will return the highest element in the array
	'So if it's a valid email the UBound function will return 1, i.e. 0 start
	If UBound(emailParts) <> 1 Then
	ValidEmail = false
	Else 
	'Check the length of each part of the email address
	'first part can be just one character, 2nd part must be atleast 4
	If Len(emailParts(0))<1 OR Len(emailParts(1))<4 Then
	ValidEmail = false
	End If
	'check first character on the left part isn't a "." using Left function
	If Left(emailParts(0), 1)="." Then
	ValidEmail = false
	End If 
	'check that there is a . in the second part of the email address - .com
	If InStr(emailParts(1), ".") <= 0 Then
	ValidEmail = false 
	End If 
	'We know there's a . now make sure there are 2 characters after the . for valid email
	last2Chars=Right(emailParts(1),2)
	If InStr(last2chars,".") Then
	ValidEmail = false
	End If
	'check that there shouldn't be a _ in the second part of the email address 
	If InStr(emailParts(1), "_") >0 Then
	ValidEmail = false 
	End If 
	End If
	'loop through each character of email
	For iLoopCounter = 1 to Len(emailAddress)
	'Use Lcase & Mid functions, Mid function used to return each individual character
	'in the email, and then Lcase converts it into lowercase
	emailChar = Lcase(Mid(emailAddress, iLoopCounter, 1)) 
	'Check if the emailAddress characters are acceptable
	If InStr(acceptableChars, emailChar) = 0 and Not IsNumeric(emailChar) Then
	ValidEmail = false
	End if
	Next
	'check if there is 2 . in a row
	If InStr(emailAddress, "..") > 0 Then
	ValidEmail=false
	End If
	'check if there is @. in a row
	If InStr(emailAddress, "@.") > 0 Then
	ValidEmail=false
	End If 
	isValidEmail=ValidEmail
End function

function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")		
	else
		formatSEO = ""
	end if
	formatSEO = replace(formatSEO, "---", "-")
	formatSEO = replace(formatSEO, "--", "-")	
end function
%>