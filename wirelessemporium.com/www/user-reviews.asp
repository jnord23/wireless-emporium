<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
	noLeftNav = 1
	noCommentBox = true

	itemID = prepInt(request.querystring("itemID"))
	if itemID = 0 then itemID = request.querystring("id")
	
	noProduct = true
	
	SQL = 	"select	a.itemdesc, a.itempic" & vbcrlf & _
			"	,	isnull((select brandName from we_brands with (nolock) where brandid = a.brandid), 'Universal') brandName" & vbcrlf & _
			"	,	isnull((select modelName from we_models with (nolock) where modelid = a.modelid), 'Universal') modelName" & vbcrlf & _
			"from	we_items a with (nolock)" & vbcrlf & _
			"where	a.itemid = '" & itemid & "'"
	session("errorSQL") = sql
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1	

	if not rs.eof then
		noProduct = false
		itemDesc = rs("itemDesc")
		itempic = rs("itempic")
		seTitle = itemDesc & " Reviews | Wireless Emporium"
		seDescription = "Submit reviews for the " & rs("itemDesc") & " you purchased. All other " & rs("brandName") & " " & rs("modelName") & " accessories can also be reviewed!"
	end if

	if request.Form("hidSubmitted") = "YES" then
		siteID = 0
		reviewerEmail = prepStr(request.form("txtEmail"))
		isRecommend = prepInt(request.form("rdoRecommend"))
		reviewerNickName = prepStr(request.form("txtNickname"))
		reviewTitle = prepStr(request.form("txtReviewTitle"))
		reviewBody = prepStr(request.form("txtReview"))
		reviewerLocation = null
		reviewerAge = prepInt(request.form("cbAge"))
		reviewerGender = prepStr(request.form("cbGender"))
		reviewerType = prepStr(request.form("rdoJob"))
		orderID = prepInt(request.form("orderID"))
		itemID = prepInt(request.form("itemID"))
		starRating = prepInt(request.form("hidStar"))
		ownershipPeriod = null
		isProPrice = 0
		isProDesign = 0
		isProWeight = 0
		isProProtection = 0
		otherPro = null
		isConFit = 0
		isConRobust = 0
		isConMaintenance = 0
		isConWeight = 0
		otherCon = null
		
		Dim cmd
		Set cmd = Server.CreateObject("ADODB.Command")
		Set cmd.ActiveConnection = oConn
		cmd.CommandText = "insertXReview"
		cmd.CommandType = adCmdStoredProc 
	
		cmd.Parameters.Append cmd.CreateParameter("siteID", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("reviewerEmail", adVarChar, adParamInput, 120)
		cmd.Parameters.Append cmd.CreateParameter("reviewerNickName", adVarChar, adParamInput, 50)
		cmd.Parameters.Append cmd.CreateParameter("reviewerLocation", adVarChar, adParamInput, 50)
		cmd.Parameters.Append cmd.CreateParameter("reviewerAge", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("reviewerGender", adChar, adParamInput, 1)
		cmd.Parameters.Append cmd.CreateParameter("reviewerType", adVarChar, adParamInput, 10)
		cmd.Parameters.Append cmd.CreateParameter("orderID", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("itemID", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("starRating", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("ownershipPeriod", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("reviewTitle", adVarChar, adParamInput, 100)
		cmd.Parameters.Append cmd.CreateParameter("reviewBody", adVarChar, adParamInput, 8000)
		cmd.Parameters.Append cmd.CreateParameter("isRecommend", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("isProPrice", adCurrency, adParamInput)	
		cmd.Parameters.Append cmd.CreateParameter("isProDesign", adCurrency, adParamInput)	
		cmd.Parameters.Append cmd.CreateParameter("isProWeight", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("isProProtection", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("otherPro", adVarChar, adParamInput, 8000)
		cmd.Parameters.Append cmd.CreateParameter("isConFit", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("isConRobust", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("isConMaintenance", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("isConWeight", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("otherCon", adVarChar, adParamInput, 8000)
	
		cmd("siteID")			=	siteID
		cmd("reviewerEmail")	=	reviewerEmail
		cmd("reviewerNickName")	=	reviewerNickName
		cmd("reviewerLocation")	=	reviewerLocation
		cmd("reviewerAge")		=	reviewerAge
		cmd("reviewerGender")	=	reviewerGender
		cmd("reviewerType")		=	reviewerType
		cmd("orderID")			=	orderID	
		cmd("itemID")			=	itemID
		cmd("starRating")		=	starRating
		cmd("ownershipPeriod")	=	ownershipPeriod
		cmd("reviewTitle")		=	reviewTitle
		cmd("reviewBody")		=	reviewBody
		cmd("isRecommend")		=	isRecommend
		cmd("isProPrice")		=	isProPrice
		cmd("isProDesign")		=	isProDesign
		cmd("isProWeight")		=	isProWeight
		cmd("isProProtection")	=	isProProtection
		cmd("otherPro")			=	otherPro
		cmd("isConFit")			=	isConFit
		cmd("isConRobust")		=	isConRobust
		cmd("isConMaintenance")	=	isConMaintenance
		cmd("isConWeight")		=	isConWeight
		cmd("otherCon")			=	otherCon
		
		cmd.Execute	
	end if
	
	'========= verify human code ========
	set fso = CreateObject("Scripting.FileSystemObject")
	Set Jpeg = Server.CreateObject("Persits.Jpeg")
	Set vhImg = Server.CreateObject("Persits.Jpeg")
	Jpeg.Quality = 100
	Jpeg.Interpolation = 1
	
	randomize
	verifyHumanCode = int(rnd*61565161)+1
	if left(verifyHumanCode,1) < 4 then
		vh_letter = "a"
	elseif left(verifyHumanCode,1) < 7 then
		vh_letter = "b"
	else
		vh_letter = "b"
	end if
	if right(verifyHumanCode,1) < 4 then
		vh_num = 1
	elseif left(verifyHumanCode,1) < 7 then
		vh_num = 2
	else
		vh_num = 2
	end if
	
	Jpeg.New 60, 30, &HFFFFFF
	vhImg.Open server.MapPath("/images/verifyHuman/" & vh_letter & ".gif")
	Jpeg.Canvas.DrawImage 0, 0, vhImg
	vhImg.Open server.MapPath("/images/verifyHuman/" & vh_num & ".gif")
	Jpeg.Canvas.DrawImage 30, 0, vhImg
	if not fso.fileExists(server.MapPath("/images/verifyHuman/results/" & verifyHumanCode & ".jpg")) then
		Jpeg.Save server.MapPath("/images/verifyHuman/results/" & verifyHumanCode & ".jpg")
	end if
	'========= verify human code end ========
%>
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/asp/inc_Email_Validation.asp"-->
<link href="/includes/css/product/productBase.css?v=20140114" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/includes/js/validate.js"></script>
<form name="frmProductReview" method="post">
<%
if request.Form("hidSubmitted") = "YES" then
%>
<div id="reviewSubmission">
    <div class="title">
        <div class="pageTitle">Product Review Submission</div>
        <div class="write"></div>
    </div>
    <div class="product-details">
        <div class="description">
            <h1 style="font-size:16px;">Thank you for your response!</h1>
            <p>Your review must be approved before being posted on the site.</p>
            <p>
            <br /><br />
            <a href="/" style="font-weight: bold; color: #ff6600; font-size: 14px;">&laquo; Browse more items</a>            
            </p>
        </div>
    </div>
</div>
<%
elseif noProduct then
%>
<div id="reviewSubmission">
    <div class="title">
        <div class="pageTitle">Product Review Submission</div>
        <div class="write"></div>
    </div>
    <div class="product-details">
        <div class="description">
            <h1>No product is available for review</h1>
        </div>
    </div>
</div>
<%
else
%>
<div id="reviewSubmission">
    <div class="title">
        <div class="pageTitle">Product Review Submission</div>
        <div class="write"></div>
    </div>
    <div class="product-details">
        <div class="product-image"><img src="/productpics/thumb/<%=itempic%>" width="100" /></div>
        <div class="description">
            <h1><%=itemDesc%></h1>
            <p>Thank you for sharing your thoughts about one of our products! Please focus on the product performance and quality. The more information you provide, the more useful your review will be to others. Your review may be posted on WirelessEmporium.com.</p>
        </div>
    </div>
    <div class="product-rating">
        <div class="header">
            <div class="header-title">Product Rating:</div>
            <div class="required"><span class="asterisk">*</span> Indicates Required Field</div>
        </div>
        <div class="fields">
            <div class="label">Click stars to rate product <span class="asterisk">*</span></div>
            <div class="field">
                <span class="rating">
                    <input type="radio" class="rating-input" id="rating-input-5" name="hidStar" value="5">
                    <label for="rating-input-5" class="rating-star"></label>
                    <input type="radio" class="rating-input" id="rating-input-4" name="hidStar" value="4">
                    <label for="rating-input-4" class="rating-star"></label>
                    <input type="radio" class="rating-input" id="rating-input-3" name="hidStar" value="3">
                    <label for="rating-input-3" class="rating-star"></label>
                    <input type="radio" class="rating-input" id="rating-input-2" name="hidStar" value="2">
                    <label for="rating-input-2" class="rating-star"></label>
                    <input type="radio" class="rating-input" id="rating-input-1" name="hidStar" value="1">
                    <label for="rating-input-1" class="rating-star"></label>
                </span>
                <span id="rating-desc" class="rating-desc"></span>
            </div>
        </div>
        <div class="fields">
            <div class="label">Do you recommend this product? <span class="asterisk">*</span></div>
            <div class="field">
                <input type="radio" name="rdoRecommend" value="1" id="recommend_yes" checked="checked" />&nbsp;<label for="recommend_yes">Yes</label>
                &nbsp;&nbsp;&nbsp;
                <input type="radio" name="rdoRecommend" value="0" id="recommend_no" />&nbsp;<label for="recommend_no">No</label>
            </div>
        </div>
    </div>
    <div class="review-details">
        <div class="header">
            <div class="header-title">Your Review:</div>
            <div class="required"><span class="asterisk">*</span> Indicates Required Field</div>
        </div>
        <div class="fields">
            <div class="label">Email Address: <span class="asterisk">*</span></div>
            <div id="email_field" class="field">
            	<div><input type="text" name="txtEmail" value="" /></div>
				<div id="msgEmail" style="color:#cd0000; display:none;">Please enter the valid email address!</div>
			</div>
        </div>
        <div class="fields">
            <div class="label">Nickname: <span class="asterisk">*</span></div>
            <div id="nick_field" class="field">
            	<input type="text" name="txtNickname" value="" />
				<div id="msgNickname" style="color:#cd0000; display:none;">Please enter nickname!</div>
			</div>
        </div>
        <div class="fields">
            <div class="label">Review title: <span class="asterisk">*</span></div>
            <div id="reviewTitle_field" class="field">
            	<input type="text" name="txtReviewTitle" value="" />
				<div id="msgReviewTitle" style="color:#cd0000; display:none;">Please enter review title!</div>
            </div>
        </div>
        <div class="fields">
            <div class="label">Review: <span class="asterisk">*</span></div>
            <div id="review_field" class="field">
            	<textarea name="txtReview"></textarea>
                <div id="msgReview" style="color:#cd0000; display:none;">Please enter review!</div>
			</div>
        </div>
    </div>
    <div class="review-details">
        <div class="header">
            <div class="header-title">Your Information:</div>
        </div>
        <div class="fields">
            <div class="label">Age:</div>
            <div class="field">
				<select name="cbAge">
                    <option value="0">Please Select</option>
                    <option value="1">17 & under</option>
                    <option value="2">18 - 25</option>
                    <option value="3">26 - 35</option>
                    <option value="4">36 - 45</option>
                    <option value="5">46 - 55</option>
                    <option value="6">56+</option>
                </select>            
            </div>
        </div>
        <div class="fields">
            <div class="label">Gender:</div>
            <div class="field">
                <select name="cbGender">
                    <option value="">Please Select</option>
                    <option value="M">Male</option>
                    <option value="F">Female</option>
                </select>
            </div>
        </div>
        <div class="fields">
            <div class="label">Describe yourself:</div>
            <div class="field">
                <div class="describe"><input type="radio" name="rdoJob" value="STU" id="describe1" checked="checked" />&nbsp;<label for="describe1">Studious Student</label></div>
                <div class="describe"><input type="radio" name="rdoJob" value="TE" id="describe2" />&nbsp;<label for="describe2">Technology Geek</label></div>
                <div class="describe"><input type="radio" name="rdoJob" value="PA" id="describe3" />&nbsp;<label for="describe3">App Addict</label></div>
                <div class="describe"><input type="radio" name="rdoJob" value="CPU" id="describe4" />&nbsp;<label for="describe4">Text and Call Only</label></div>
                <div class="describe"><input type="radio" name="rdoJob" value="BO" id="describe5" />&nbsp;<label for="describe5">Business Jet Setter</label></div>
            </div>
            <div class="humanVerifyBox">
                <div style="width:200px; text-align:center;"><img src='/images/verifyHuman/results/<%=verifyHumanCode%>.jpg' border='0' /></div>
                <div style="width:200px; text-align:center;">Please verify the code above<br />in the box below</div>
                <div style="width:200px; text-align:center;">
                    <input type="text" name="verifyHuman" value="" size="2" maxlength="2" />
                    <input type="hidden" name="verifyHumanCode" value="<%=verifyHumanCode%>"  />
                </div>
            </div>

        </div>
    </div>
    <div class="submit"><input type="submit" class="submit_review" value="" onclick="return validateReview()" /></div>
</div>
<%end if%>
<input type="hidden" name="hidSubmitted" value="NO"  />
<input type="hidden" name="orderID" value="<%=request.queryString("oid")%>"  />
<input type="hidden" name="itemID" value="<%=itemid%>"  />
</form>
<script>
function validateReview() {
	var frm = document.frmProductReview;

	document.getElementById('email_field').className = "field";
	document.getElementById('nick_field').className = "field";
	document.getElementById('reviewTitle_field').className = "field";
	document.getElementById('review_field').className = "field";
			
	document.getElementById('msgEmail').style.display = 'none';
	document.getElementById('msgNickname').style.display = 'none';
	document.getElementById('msgReviewTitle').style.display = 'none';
	document.getElementById('msgReview').style.display = 'none';

	var reviewerEmail = frm.txtEmail.value;
	var reviewerNickName = frm.txtNickname.value;
	var reviewerAge = frm.cbAge.value;
	var reviewerGender = frm.cbGender.value;
	var reviewerType = getSelectedRadioValue(frm.rdoJob);
	
	var orderID = <%=prepInt(request.queryString("oid"))%>;
	var itemID = <%=itemid%>;
	var starRating = getSelectedRadioValue(frm.hidStar);
	var reviewTitle = frm.txtReviewTitle.value;
	var reviewBody = frm.txtReview.value;
	var isRecommend = getSelectedRadioValue(frm.rdoRecommend);
	var verifyHumanCode = frm.verifyHumanCode.value;
	var verifyHuman = frm.verifyHuman.value;

	if (!validateEmail(reviewerEmail)) { 
		document.getElementById('msgEmail').style.display = '';
		document.getElementById('email_field').className = "alertField";
		frm.txtEmail.focus();
		return false;
	}

	if (reviewerNickName == '') { 
		document.getElementById('msgNickname').style.display = '';
		document.getElementById('nick_field').className = "alertField";
		frm.txtNickname.focus();
		return false;
	}

	if (reviewTitle == '') { 
		document.getElementById('msgReviewTitle').style.display = '';
		document.getElementById('reviewTitle_field').className = "alertField";
		frm.txtReviewTitle.focus();
		return false;
	}

	if (reviewBody == '') { 
		document.getElementById('msgReview').style.display = '';
		document.getElementById('review_field').className = "alertField";
		frm.txtReview.focus();
		return false;
	}

	if (starRating == '') {
		alert("Please rate this product!");
		window.scroll(0,0);
		return false;
	}
	
	var vh_letter = "";
	var vh_num = "";
	if (verifyHumanCode.substring(0,1) < 4) {
		vh_letter = "a";
	} else if (verifyHumanCode.substring(0,1) < 7) {
		vh_letter = "b";
	} else {
		vh_letter = "b";
	}
	
	if (verifyHumanCode.substring(verifyHumanCode.length-1,verifyHumanCode.length) < 4) {
		vh_num = 1;
	} else if (verifyHumanCode.substring(verifyHumanCode.length-1,verifyHumanCode.length) < 7) {
		vh_num = 2;
	} else {
		vh_num = 2;
	}
	
	var requiredCode = vh_letter + vh_num;	
	if (requiredCode != verifyHuman) {
		alert("Please enter the correct code in the box!");
		frm.verifyHuman.focus();
		return false;
	}
	
	frm.hidSubmitted.value = "YES";

	return true;
}

function validateEmail(email) {
	var splitted = email.match("^(.+)@(.+)$");
	if (splitted == null) return false;
	if (splitted[1] != null) {
		var regexp_user=/^\"?[\w-_\.]*\"?$/;
		if (splitted[1].match(regexp_user) == null) return false;
	}
	if (splitted[2] != null) {
		var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
		if (splitted[2].match(regexp_domain) == null) {
			var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
			if (splitted[2].match(regexp_ip) == null) return false;
		}
		return true;
	}
	return false;
}
</script>
<!--#include virtual="/includes/template/bottom_product.asp"-->