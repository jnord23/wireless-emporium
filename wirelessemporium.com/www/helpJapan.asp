<table border="0" align="center" cellspacing="0" cellpadding="0" width="600">
    <tr>
        <td align="center"><img src="/images/WE_japan_popubanner.jpg" border="0" /></td>
    </tr>
    <tr>
        <td align="left">
            We have all seen images and videos of the devastation in Japan as a result of the 9.0 magnitude earthquake and 
            resulting tsunami.  From now through the end of March a portion of every single sale on Wireless Emporium will 
            be donated to the American Red Cross Japan relief fund.  Take advantage of this opportunity to donate to the 
            cause by simply shopping.  Thank you in advance for helping us help the survivors of this awful tragedy.
        </td>
    </tr>
</table>
