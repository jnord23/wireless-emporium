<%
pageTitle = "CO RMA System"
header = 0
%>

<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/admin/formatNotes.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->

<h3><font face="verdana">RMA System</font></h3>

<%
OrderID = request("OrderID")
strComments = SQLquote(request("comments"))
btnExchange = request.form("btnExchange")
btnReturn = request.form("btnReturn")
btnBackOrder = request.form("btnBackOrder")
btnBackOrderSend = request.form("btnBackOrderSend")
btnNoItem=request.form("btnNoItem")
btnNoItemSend=request.form("btnNoItemSend")
btnModified = request.form("btnModified")
btnModifiedSend = request.form("btnModifiedSend")
btnOtherRefund = request.form("btnOtherRefund")
btnOtherRefundSend = request.form("btnOtherRefundSend")

adminID = session("adminID")

if OrderID = "" then
	response.write "<b><font color='red'>MISSING ORDER RECORD ID. PLEASE CLICK ON BACK BUTTON TO GO BACK TO PREVIOUS PAGE.</font></b>"
end if

if btnExchange <> "" then
	SQL = "SELECT A.fname,A.lname,A.email FROM CO_Accounts A INNER JOIN WE_Orders B ON A.accountID=B.accountID WHERE B.orderID = '" & OrderID & "'"
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.Open SQL, oConn, 3, 3
	if not RS.eof then
		if not isnull(RS("email")) then strTo = RS("email")
		strFname = RS("fname")
		strLname = RS("lname")
	end if
	'strTo = strTo & ",ruben@CellularOutfitter.com,michael@CellularOutfitter.com"
	if trim(strTo) <> "" then
		select case btnExchange
			case "New EXCHANGE", "Re-Send EXCHANGE"
				if btnExchange = "New EXCHANGE" then
					strSubject = "Instructions for Product Exchange"
					msgSuccess = "New EXCHANGE e-mail SENT"
					strNotes = formatNotes(strSubject & " email sent to customer.")
					RMAstatus = 1
				else
					strSubject = "Re-Send Instructions for Product Exchange"
					msgSuccess = "New EXCHANGE e-mail RE-SENT"
					strNotes = formatNotes(strSubject & " email re-sent to customer.")
					RMAstatus = 1
				end if
				strBody = "<p>Thank you for shopping with CellularOutfitter.com. We apologize for the problems you have encountered with your product.</p>" & vbcrlf
				strBody = strBody & "<p>We offer our famous EZ No-Hassle 30-day Return Policy: If you are not completely satisfied with your order, within 30 DAYS of receipt, you may return original-condition merchandise for an exchange or refund.</p>"
				strBody = strBody & "<p>Please return your exchange item(s) to:<br>" & vbcrlf
				strBody = strBody & "CellularOutfitter.com<br>Attn: EXCHANGE<br>1410 N. Batavia St.<br>Orange, CA&nbsp;&nbsp;92867</p>" & vbcrlf
				strBody = strBody & "Please print out this E-mail and fill in the following information (Returns without this information will not be processed):" & vbcrlf
				strBody = strBody & "<p>Please write the following on the outside of the shipping container:" & vbcrlf
				strBody = strBody & "<p><b>Name on the order:</b>" & vbcrlf
				strBody = strBody & "<p><b>Order number:</b>" & vbcrlf
				strBody = strBody & "<p><b>E-mail Address:</b>" & vbcrlf
				strBody = strBody & "<p><b>Phone Number:</b>" & vbcrlf
				strBody = strBody & "<p><b>Reason for return:</b>" & vbcrlf
				strBody = strBody & "<p><b>Being returned for (circle one): Refund/Exchange/Store Credit</b>" & vbcrlf
				strBody = strBody & "<p><b>If the return is for an exchange please enter the item you would like in the space below:</b>" & vbcrlf
				strBody = strBody & "<p>____________________________________________________________________________</b>" & vbcrlf
				strBody = strBody & "<p>Also, please write the following on the outside of the shipping container: <b>RMA" & OrderID &"</b>" & vbcrlf
				if strComments <> "" then strBody = strBody & "<p>NOTE:<br>" & strComments & "</p>" & vbcrlf
				strBody = strBody & "<p>Best regards,</p>" & vbcrlf
				strBody = strBody & "<p>CellularOutfitter.com Service</b></p>" & vbcrlf
				strBody = strBody & "<p><a href=""mailto:service@CellularOutfitter.com"">service@CellularOutfitter.com</a><br>" & vbcrlf
				strBody = strBody & "<a href=""http://www.CellularOutfitter.com"">http://www.CellularOutfitter.com</a></p>" & vbcrlf
				CDOSend strTo,"service@CellularOutfitter.com",strSubject,strBody
			case "Product(s) Received [re-ship]"
				msgSuccess = "Received return, put in re-ship pile."
				strNotes = formatNotes("Received return, put in re-ship pile.")
				RMAstatus = 2
			case "Product(s) Received [exchange]"
				msgSuccess = "Received return, put in exchange pile."
				strNotes = formatNotes("Received return, put in exchange pile.")
				RMAstatus = 2
			case "Process EXCHANGE"
				strSubject = "Your Exchange Has Been Processed"
				msgSuccess = "EXCHANGE PROCESSED"
				strNotes = formatNotes("EXCHANGE PROCESSED")
				RMAstatus = 5
				strBody = "<p>Dear " & strFname & " " & strLname & ",</p>" & vbcrlf
				strBody = strBody & "<p>This e-mail is to inform you that we have received your returned merchandise. Your exchange has been processed."
				strBody = strBody & "Your replacement product(s) will be shipped in 1-2 business days.</p>" & vbcrlf
				strBody = strBody & "<p>Thank you for shopping with CellularOutfitter.com</p>" & vbcrlf
				strBody = strBody & "<p>Please contact us if you have any questions.</p>" & vbcrlf
				if strComments <> "" then strBody = strBody & "<p>NOTE:<br>" & strComments & "</p>" & vbcrlf
				strBody = strBody & "<p>Best regards,</p>" & vbcrlf
				strBody = strBody & "<p>CellularOutfitter.com Service</b></p>" & vbcrlf
				strBody = strBody & "<p><a href=""mailto:service@CellularOutfitter.com"">service@CellularOutfitter.com</a><br>" & vbcrlf
				strBody = strBody & "<a href=""http://www.CellularOutfitter.com"">http://www.CellularOutfitter.com</a></p>" & vbcrlf
				CDOSend strTo,"service@CellularOutfitter.com",strSubject,strBody
		end select
		
		SQL = "SELECT ID,ordernotes FROM we_ordernotes WHERE orderid = '" & OrderID & "'"
		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.Open SQL, oConn, 3, 3
		if not RS.EOF then
			SQL = "UPDATE we_ordernotes SET ordernotes='" & SQLquote(RS("ordernotes") & strNotes) & "' WHERE ID = '" & RS("ID") & "'"
		else
			SQL = "INSERT INTO we_ordernotes (orderid,ordernotes) VALUES ('" & OrderID & "','" & SQLquote(strNotes) & "')"
		end if
		oConn.execute SQL
		SQL = "UPDATE we_orders SET RMAstatus='" & RMAstatus & "' WHERE orderID = '" & OrderID & "'"
		oConn.execute SQL
		response.write "<h3 align=""center"">" & msgSuccess & "</h3>" & vbcrlf
	else
		response.write "<h3 align=""center"">Could not find e-mail address.</h3>" & vbcrlf
	end if
elseif btnReturn <> "" then
	SQL = "SELECT A.fname,A.lname,A.email FROM CO_Accounts A INNER JOIN WE_Orders B ON A.accountID=B.accountID WHERE B.orderID = '" & OrderID & "'"
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.Open SQL, oConn, 3, 3
	if not RS.eof then
		strFname = RS("fname")
		strLname = RS("lname")
		if not isnull(RS("email")) then strTo = RS("email")
	end if
	'strTo = strTo & ",ruben@CellularOutfitter.com,michael@CellularOutfitter.com"
	if trim(strTo) <> "" then
		select case btnReturn
			case "New RETURN", "Re-Send RETURN"
				if btnReturn = "New RETURN" then
					strSubject = "Instructions for Product Return"
					msgSuccess = "New RETURN e-mail SENT"
					strNotes = formatNotes(strSubject & " email re-sent to customer.")
					RMAstatus = 3
				else
					strSubject = "Re-Send Instructions for Product Return"
					msgSuccess = "New RETURN e-mail RE-SENT"
					strNotes = formatNotes(strSubject & " email sent to customer.")
					RMAstatus = 3
				end if
				strBody = "<p>Dear " & strFname & " " & strLname & ",</p>" & vbcrlf
				strBody = strBody & "<p>Thank you for shopping with CellularOutfitter.com. We apologize for the problems you have encountered with your product.</p>" & vbcrlf
				strBody = strBody & "<p>This is an email confirmation indicating that we have received your request for credit.</p>" & vbcrlf
				strBody = strBody & "<p>Please return your item(s) to:<br>" & vbcrlf
				strBody = strBody & "CellularOutfitter.com<br>Attn: RETURN<br>1410 N. Batavia St.<br>Orange, CA&nbsp;&nbsp;92867</p>" & vbcrlf
				strBody = strBody & "<p>Please print out this E-mail and fill in the following information (Returns without this information will not be processed):</p>" & vbcrlf
				strBody = strBody & "<p><b>Name on the order:</b></p>" & vbcrlf
				strBody = strBody & "<p><b>Order number:</b></p>" & vbcrlf
				strBody = strBody & "<p><b>E-mail Address:</b></p>" & vbcrlf
				strBody = strBody & "<p><b>Phone Number:</b></p>" & vbcrlf
				strBody = strBody & "<p><b>Reason for return:</b></p>" & vbcrlf
				strBody = strBody & "<p><b>Being returned for (circle one): Refund/Exchange/Store Credit</b></p>" & vbcrlf
				strBody = strBody & "<p>Also, please write the following on the outside of the shipping container: <b>RMA" & OrderID &"</b></p>" & vbcrlf
				if strComments <> "" then strBody = strBody & "<p>NOTE:<br>" & strComments & "</p>" & vbcrlf
				strBody = strBody & "<p>Please note: Orders over 90 days old are not available for refund. Please review our return policy at "
				strBody = strBody & "<a href=""http://www.CellularOutfitter.com/shipping-policy.html"">http://www.CellularOutfitter.com/shipping-policy.html</a></p>" & vbcrlf
				strBody = strBody & "<p>Best regards,</p>" & vbcrlf
				strBody = strBody & "<p>CellularOutfitter.com Service</b></p>" & vbcrlf
				strBody = strBody & "<p><a href=""mailto:service@CellularOutfitter.com"">service@CellularOutfitter.com</a><br>" & vbcrlf
				strBody = strBody & "<a href=""http://www.CellularOutfitter.com"">http://www.CellularOutfitter.com</a></p>" & vbcrlf
				CDOSend strTo,"service@CellularOutfitter.com",strSubject,strBody
			case "Product(s) Received"
			
				dim thisPartNumber, thisQuantity, thisItemID
				noReceived = 0
				for a = 1 to request.form("totalCount")
					if request.form("rdoReceive" & a) <> "N" then
						thisPartNumber = request.form("PartNumber" & a)
						thisQuantity = prepInt(request.form("quantity" & a))
						thisItemID = request.form("itemID" & a)
						thisQuantityold = prepInt(request.form("quantityold" & a))
						if thisQuantity > thisQuantityold then strError = "Received item qty can not be more than order qty!"
					else
						noReceived = noReceived + 1
					end if
				next
				if cint(noReceived) = cint(request.form("totalCount")) then strError = "No Item Received!"
				
				if strError = "" then
					strSubject = "Your return merchandise have been received"
					msgSuccess = "Received return, put in refund pile."
					strNotes = formatNotes("Received return, put in refund pile.")
					RMAstatus = 4
					'------------------ add inv adjuest when received item
					strBody = "<p>Hello " & strFname & " " & strLname & ",</p>" & vbcrlf
					strBody = strBody & "<p>The following item(s) from your CellularOutfitter.com order #"& OrderID &" return have been received:"
					for a = 1 to request.form("totalCount")
						if request.form("rdoReceive" & a) <> "N" then					
							strBody = strBody & "<ul><li>Part Number: " & request.form("PartNumber" & a) &"</li> "& vbcrlf
							strBody = strBody & "<li>Item Description: " & request.form("itemdesc" & a) &"</li>" & vbcrlf
							strBody = strBody & "<li>Quantity: " & request.form("quantity" & a) &"</li></ul>" & vbcrlf 
						end if
					next
					strBody = strBody & "<p>Please allow 2-4 business days for your exchange or refund to be processed. Once the exchange or refund is issued you will receive an E-mail to confirm the return is complete.</p>" & vbcrlf
					strBody = strBody & "<p>Thank you for choosing Cellular Outfitter,</p>" & vbcrlf
					'if strComments <> "" then strBody = strBody & "<p>NOTE:<br>" & strComments & "</p>" & vbcrlf
					strBody = strBody & "<p>Best regards,</p>" & vbcrlf
					strBody = strBody & "<p><font color=#999999><b>Cellular</font> <font color=#FF8100>Outfitter</font> Customer Support</b></p>" & vbcrlf
					strBody = strBody & "<p><a href=""mailto:service@CellularOutfitter.com"">service@CellularOutfitter.com</a><br>" & vbcrlf
					strBody = strBody & "<a href=""http://www.CellularOutfitter.com"">http://www.CellularOutfitter.com</a></p>" & vbcrlf
					CDOsend strTo,"service@CellularOutfitter.com",strSubject,strBody
				
					for a = 1 to request.form("totalCount")
						thisPartNumber = request.form("PartNumber" & a)
						thisQuantity = prepInt(request.form("quantity" & a))
						thisItemID = request.form("itemID" & a)
						origItemPrice = request.form("origItemPrice" & a)
						itemTax = request.form("itemTax" & a)
						amountCredited = request.form("amountCredited" & a)
						txtReStockingFeePercent = request.form("txtReStockingFeePercent" & a)
						exPartNumber = request.form("exPartNumber" & a)	
						rdoReshipReason = request.form("rdoReshipReason" & a)
						exchangeNote = request.form("exchangeNote" & a)
						discountPercent = request.form("discountPercent" & a)
						cbReturnReason = request.form("cbReturnReason" & a)
						cbExchangeReturnReason = request.form("cbExchangeReturnReason" & a)
										
						select case request.form("rdoReceive" & a)
							case "R","T"
								if request.form("rdoReceive" & a) = "R" then 
									activity_desc = "RECEIVED"
								else 
									activity_desc = "TRASHED"
								end if
								
								sql = 	"insert into rmalog(site_id,orderid,itemid,qty,partnumber,rmastatus,activity_desc,origItemPrice" & vbcrlf & _
										"				,	restocking_fee_percent,discountPercent,itemTax,amountCredited,exchangedPartnumber,returnReason,adminID)" & vbcrlf & _
										"values(2,'" & orderID & "','" & thisItemID & "','" & thisQuantity & "','" & thisPartNumber & "','" & RMAstatus & "','" & activity_desc & "','" & origItemPrice & "'" & vbcrlf & _
										"		,'" & txtReStockingFeePercent & "','" & discountPercent & "','" & itemTax & "','" & amountCredited & "',null,'" & cbReturnReason & "','" & adminID & "')"
								oConn.execute(sql)
								
								SQL = "UPDATE we_orderdetails SET returned = " & thisQuantity & " WHERE orderID='" & orderID & "' AND itemID='" & thisItemID & "'"
								oConn.execute SQL
							case "E"
								Set cmd = Server.CreateObject("ADODB.Command")
								Set cmd.ActiveConnection = oConn
								cmd.CommandText = "we_ReshipItem"
								cmd.CommandType = adCmdStoredProc 
	
								cmd.Parameters.Append cmd.CreateParameter("ret", adInteger, adParamReturnValue)
								cmd.Parameters.Append cmd.CreateParameter("p_orderid", adInteger, adParamInput)
								cmd.Parameters.Append cmd.CreateParameter("p_partnumber", adVarChar, adParamInput, 100)
								cmd.Parameters.Append cmd.CreateParameter("p_reshipreason", adVarChar, adParamInput, 100)
								cmd.Parameters.Append cmd.CreateParameter("p_qty", adInteger, adParamInput)
								cmd.Parameters.Append cmd.CreateParameter("p_modifier", adVarChar, adParamInput, 50)
								cmd.Parameters.Append cmd.CreateParameter("p_note", adVarChar, adParamInput, 4000)
								cmd.Parameters.Append cmd.CreateParameter("p_doNotSendEmail", adInteger, adParamInput)
								cmd.Parameters.Append cmd.CreateParameter("p_origOrderDetailID", adInteger, adParamInput)
								cmd.Parameters.Append cmd.CreateParameter("p_out_msg", adVarChar, adParamOutput, 500)
	
								cmd("p_orderid")			=	orderID
								cmd("p_partnumber")			=	exPartNumber
								cmd("p_reshipreason")		=	rdoReshipReason
								cmd("p_qty")				=	prepInt(thisQuantity)
								cmd("p_modifier")			=	Request.Cookies("username")
								cmd("p_note")				=	exchangeNote
								cmd("p_doNotSendEmail")		=	0
								cmd("p_origOrderDetailID")	=	orderID
								
								cmd.Execute
	
								retReshipMsg	=	cmd("p_out_msg")
								retReship		= 	cmd("ret")
								
								if -1 = retReship then
									response.write "<font color=""red"">" & retReshipMsg & "</font><br>" 
									response.write "<a href=""javascript:history.back(-1);"">Click here to go back</a>"
									response.end
								end if								
								
								activity_desc = "(E) / " & rdoReshipReason
								if request.form("rdoExchangeReceive" & a) = "R" then
									activity_desc = activity_desc & " / Received"
								elseif request.form("rdoExchangeReceive" & a) = "T" then
									activity_desc = activity_desc & " / Trashed"
								end if
								
								sql = 	"insert into rmalog(site_id,orderid,itemid,qty,partnumber,rmastatus,activity_desc,origItemPrice" & vbcrlf & _
										"				,	restocking_fee_percent,discountPercent,itemTax,amountCredited,exchangedPartnumber,returnReason,adminID)" & vbcrlf & _
										"values(2,'" & orderID & "','" & thisItemID & "','" & thisQuantity & "','" & thisPartNumber & "','" & RMAstatus & "','" & activity_desc & "','" & origItemPrice & "'" & vbcrlf & _
										"		,null,null,null,null,'" & exPartNumber & "','" & cbExchangeReturnReason & "','" & adminID & "')"
								oConn.execute(sql)
						end select
					next
					strMessage = "<meta http-equiv=""refresh"" content=""3;URL=/admin/RMA_CO.asp?orderid=" & OrderID & """>"
					strMessage = strMessage & "<div width=""100%"" align=""center"" style=""text-align:center;""><br><br><strong><font color=""#FF6600"" size=""3"" face=""Verdana, Arial, Helvetica, sans-serif"">You are being transferred to the refund page...</font></strong></div>"					
				else
					response.write "<h3 align=""center""><font color=""#FF0000"">ERROR!<br>" & strError & "</font></h3>" & vbcrlf
					response.write "<form><p align=""center""><input type='button' onclick='history.back();' class='smltext' value='BACK'></p></form>" & vbcrlf
					response.write "</body></html>" & vbcrlf
					response.end
				end if
		'-----------------end add inv adjuest when received item				
			case "Process REFUND","Process EXCHANGE & REFUND","Process EXCHANGE"
				bRefund = false
				bExchange = false
				select case btnReturn
					case "Process REFUND" 				
						strSubject = "Your Refund Has Been Processed"
						bRefund = true
					case "Process EXCHANGE & REFUND" 	
						strSubject = "Your Refund/Exchange Has Been Processed"
						bRefund = true
						bExchange = true
					case "Process EXCHANGE"				
						strSubject = "Your Exchange Has Been Processed"
						bExchange = true
				end select
				strCR = prepInt(request.form("txtcredit"))
				refundreason = request.form("refundreason")				
				strError = ""
				ordergrandtotal = 0
				if not isNumeric(strCR) then strError = "Credit amount must be a numeric value!"
				if strError = "" then
					strCR = cDbl(strCR)
					SQL = "SELECT ordergrandtotal FROM we_orders WHERE orderid = '" & OrderID & "'"
					Set RS = Server.CreateObject("ADODB.Recordset")
					RS.Open SQL, oConn, 3, 3
					if not RS.EOF then
						ordergrandtotal = cDbl(RS("ordergrandtotal"))
						if strCR > ordergrandtotal then strError = "Refund amount may not exceed Order Total!"
					else
						strError = "Order not found!"
					end if
				end if
				strNotes = ""
				msgSuccess = ""
				if strError = "" then
					strBody = "<p>Dear " & strFname & " " & strLname & ",</p>" & vbcrlf
					strBody = strBody & "<p>This e-mail is to inform you that we have received your returned merchandise."
					if bExchange then
						strBody = strBody & " Your exchange has been processed and your replacement product(s) will be shipped in 1-2 business days." & vbcrlf
						strNotes = "EXCHANGE PROCESSED, "
						msgSuccess = "EXCHANGE PROCESSED, "
						RMAstatus = 5
					end if
					if bRefund then
						strBody = strBody & " We have processed your credit in the amount of <b>" & formatCurrency(strCR) & "</b>, please allow a few days for posting." & vbcrlf
						if strCR < ordergrandtotal then
							strNotes = strNotes & "Partial Credit issued for " & formatCurrency(strCR) & ". Email sent to customer."
							msgSuccess = msgSuccess & "PARTIAL REFUND PROCESSED for " & formatCurrency(strCR) & "."
							RMAstatus = 6
							SQL = "UPDATE we_orders SET refundAmount='" & strCR & "', refundDate='" & now & "', refundreason ='"&refundreason&"' WHERE orderID = '" & OrderID & "'"
							'response.write "<p>" & SQL & "</p>" & vbcrlf
							oConn.execute SQL
						else
							strNotes = strNotes & "Full Credit issued for " & formatCurrency(strCR) & ". Email sent to customer."
							msgSuccess = msgSuccess & "FULL REFUND PROCESSED for " & formatCurrency(strCR) & "."
							RMAstatus = 7
							SQL = "UPDATE we_orders SET refundAmount='" & strCR & "', refundDate='" & now & "', refundreason ='"&refundreason&"' WHERE orderID = '" & OrderID & "'"
							'response.write "<p>" & SQL & "</p>" & vbcrlf
							oConn.execute SQL
						end if					
					end if
					strNotes = formatNotes(strNotes)
					
					strBody = strBody & "</p><p>Thank you for shopping with CellularOutfitter.com</p>" & vbcrlf
					strBody = strBody & "<p>Please contact us if you have any questions.</p>" & vbcrlf
					if strComments <> "" then strBody = strBody & "<p>NOTE:<br>" & strComments & "</p>" & vbcrlf
					strBody = strBody & "<p>Best regards,</p>" & vbcrlf
					strBody = strBody & "<p>CellularOutfitter.com Service</b></p>" & vbcrlf
					strBody = strBody & "<p><a href=""mailto:service@CellularOutfitter.com"">service@CellularOutfitter.com</a><br>" & vbcrlf
					strBody = strBody & "<a href=""http://www.CellularOutfitter.com"">http://www.CellularOutfitter.com</a></p>" & vbcrlf
					CDOSend strTo,"service@CellularOutfitter.com",strSubject,strBody

					refundItemIds = prepStr(request.form("hidRefundItemID"))
					if refundItemIds <> "" then
						sql = 	"update	rmalog" & vbcrlf & _
								"set	confirmAmountCredited = 1" & vbcrlf & _
								"	,	RMAstatus = '" & RMAstatus & "'" & vbcrlf & _
								"	,	refundReason = '" & refundreason & "'" & vbcrlf & _
								"where	orderid = '" & orderid & "' and site_id = 2" & vbcrlf & _
								"	and	itemid in (" & refundItemIds & ")"
						oConn.execute(sql)
					end if				
				else
					response.write "<h3 align=""center""><font color=""#FF0000"">ERROR!<br>" & strError & "</font></h3>" & vbcrlf
					response.write "<form><p align=""center""><input type='button' onclick='history.back();' class='smltext' value='BACK'></p></form>" & vbcrlf
					response.write "</body></html>" & vbcrlf
					response.end
				end if
		end select
		
		SQL = "SELECT ID,ordernotes FROM we_ordernotes WHERE orderid = '" & OrderID & "'"
		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.Open SQL, oConn, 3, 3
		if not RS.EOF then
			SQL = "UPDATE we_ordernotes SET ordernotes='" & SQLquote(RS("ordernotes") & strNotes) & "' WHERE ID = '" & RS("ID") & "'"
		else
			SQL = "INSERT INTO we_ordernotes (orderid,ordernotes) VALUES ('" & OrderID & "','" & SQLquote(strNotes) & "')"
		end if
		oConn.execute SQL
		SQL = "UPDATE we_orders SET RMAstatus='" & RMAstatus & "' WHERE orderID = '" & OrderID & "'"
		oConn.execute SQL
		response.write "<h3 align=""center"">" & msgSuccess & "</h3>" & vbcrlf
	else
		response.write "<h3 align=""center"">Could not find e-mail address.</h3>" & vbcrlf
	end if
	'--------------RMA Modified ---------------------
	elseif btnModified  <> "" then
	%>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
		<form name="frmRMAModify" method="post" action="RMA_CO.asp">
			<tr>
				<td colspan="2" class="smlText" align="center">
				  <%
					SQL = "SELECT  A.orderdetailid, A.orderID, A.quantity, B.itemID, B.PartNumber, B.itemDesc_CO FROM we_orderdetails A INNER JOIN we_Items B ON A.itemID=B.itemID WHERE A.orderID = '" & OrderID & "' ORDER BY B.PartNumber"
					set RS = Server.CreateObject("ADODB.Recordset")
					RS.open SQL, oConn, 3, 3
					a = 0
					if RS.eof then
						response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
					else
						%>
						<%=RS.recordcount%> items found
						<br>
						<table border="1" cellpadding="3" cellspacing="0" width="90%" class="smlText">
							<tr>
								<td align="center" width="20"><b>Item#</b></td>
								<td align="center" width="110"><b>Part&nbsp;Number</b></td>
								<td align="center" width="230"><b>Description</b></td>
								<td align="center" width="40"><b>Qty</b></td>
							  <td align="center" width="40">Inv. Qty</td>
								<td align="center" width="50"><b>Out of Stock</b></td>
							</tr>
							<%
							do until RS.eof
								a = a + 1
								%>
								<tr>
									<td align="center"><%=RS("itemID")%><input type="hidden" name="itemID<%=a%>" value="<%=RS("itemID")%>"></td>
									<td align="center"><nobr><%=RS("PartNumber")%><input type="hidden" name="PartNumber<%=a%>" value="<%=RS("PartNumber")%>"></nobr></td>
									<td align="center"><%=RS("itemDesc_CO")%></td>
									<td align="center">
                                    <input type="text" name="quantity<%=a%>" size="1" value="<%=RS("quantity")%>" maxlength="3">
                                    </td>
									<td align="center">&nbsp;
                                    <%
									SQL3 = "SELECT inv_qty from we_items where itemid = '"&RS("itemID")&"'"
									Set RS3 = Server.CreateObject("ADODB.Recordset")
									RS3.Open SQL3, oConn, 3, 3
									Response.Write(rs3("inv_qty"))
									'RS3.close
									'Set RS3 = nothing
									%>
                                    </td>
									<td align="center"><input type="checkbox" name="BackOrder<%=a%>" value="1"></td>
								</tr>
								<%
								RS.movenext
							loop
							%>
						</table>
						<input type="hidden" name="totalCount" value="<%=a%>">
			 		<%
					end if
					%>
			  </td>
			</tr>
			<tr>
				<td class="bigText" align="center"></td>
			</tr>
			<tr>
				<td class="bigText" align="center">
					<input type="hidden" name="ORDERID" value="<%=OrderID%>">
                    <input type="hidden" name= "comments" id ="show" value="<%=request("comments")%>">
					<input type="submit" name="btnModifiedSend" class="smltext" value="Send Order Modified Notification">
				</td>
			</tr>
		</form>
	</table>
<%	
	'--------------End of sent RMA Modified ---------------------
	'--------------Other Refund----------------------------------
elseif btnOtherRefund <> "" then
%>
    <table width="100%">
        <form name="frmOtherrefund" method="post" action="RMA_CO.asp">
            <tr>
                <td class="bigText" bgcolor="#CCCCCC" align="center">
                    Process Other Refund
                </td>
            </tr>
            <tr>
                <td colspan="2" class="bigText" bgcolor="#CCCCCC" align="center">
                    Enter Credit Amount For Order Id: <%=OrderID%><br>
                    Credit Amount: <input type="text" name="txtcredit" size="10">
                    <input type="hidden" name="refundreason" value="9" />
                 </td>
            </tr>
            <tr>
                <td colspan="2" class="bigText" bgcolor="#CCCCCC" align="center">
					Other Refund Reason: 
                    <select name="cbOtherRefundReason">
                    <%
                    set rsOtherRefundReason = oConn.execute("select id, reason from xreturnreason")
                    do until rsOtherRefundReason.eof
                        %>
                        <option value="<%=rsOtherRefundReason("id")%>"><%=rsOtherRefundReason("reason")%></option>
                        <%
                        rsOtherRefundReason.movenext
                    loop
                    %>
                    </select>
                 </td>
            </tr>
            <tr>
                <td colspan="2" class="smlText" align="center">
                    Additional customer comments:<br><textarea name="comments" cols="50" rows="4"></textarea>
                </td>
            </tr>
            <tr>
                <td class="bigText" align="center">
                    <input type="hidden" name="ORDERID" value="<%=OrderID%>">
                    <input type="submit" name="btnOtherRefundSend" id ="btnOtherRefundSend" class="smltext" value="Process Other Refund">
                </td>
            </tr>
        </form>
    </table>
<%
'--------------Other Refund End
elseif btnBackOrder <> "" then
	%>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
		<form name="frmBackOrder" method="post" action="RMA_CO.asp">
			<tr>
				<td colspan="2" class="smlText" align="center">
					<%
					SQL = "SELECT A.orderID, A.quantity, B.itemID, B.PartNumber, B.itemDesc_CO FROM we_orderdetails A INNER JOIN we_Items B ON A.itemID=B.itemID WHERE A.orderID = '" & OrderID & "' ORDER BY B.PartNumber"
					set RS = Server.CreateObject("ADODB.Recordset")
					RS.open SQL, oConn, 3, 3
					a = 0
					if RS.eof then
						response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
					else
						%>
						<%=RS.recordcount%> items found
						<br>
						<table border="1" cellpadding="3" cellspacing="0" width="90%" class="smlText">
							<tr>
								<td align="center" width="20"><b>Item#</b></td>
								<td align="center" width="110"><b>Part&nbsp;Number</b></td>
								<td align="center" width="230"><b>Description</b></td>
								<td align="center" width="40"><b>Qty</b></td>
								<td align="center" width="50"><b>BACK-ORDER</b></td>
							</tr>
							<%
							do until RS.eof
								a = a + 1
								%>
								<tr>
									<td align="center"><%=RS("itemID")%><input type="hidden" name="itemID<%=a%>" value="<%=RS("itemID")%>"></td>
									<td align="center"><nobr><%=RS("PartNumber")%><input type="hidden" name="PartNumber<%=a%>" value="<%=RS("PartNumber")%>"></nobr></td>
									<td align="center"><%=RS("itemDesc_CO")%></td>
									<td align="center"><input type="text" name="quantity<%=a%>" size="1" value="<%=RS("quantity")%>" maxlength="3"></td>
									<td align="center"><input type="checkbox" name="BackOrder<%=a%>" value="1"></td>
								</tr>
								<%
								RS.movenext
							loop
							%>
						</table>
						<input type="hidden" name="totalCount" value="<%=a%>">
						<%
					end if
					%>
				</td>
			</tr>
			<tr>
				<td class="bigText" align="center">
					Number&nbsp;of&nbsp;Business&nbsp;Days:&nbsp;<input type="text" name="NumberOfDays" size="10" value="10 to 21">
				</td>
			</tr>
			<tr>
				<td class="bigText" align="center">
					<input type="hidden" name="ORDERID" value="<%=OrderID%>">
					<input type="submit" name="btnBackOrderSend" class="smltext" value="Send BACK-ORDER Notification">
				</td>
			</tr>
		</form>
	</table>
	<%  
elseif btnNOItem <> "" then
	%>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
		<form name="frmNoItem" method="post" action="RMA_CO.asp">
			<tr>
				<td colspan="2" class="smlText" align="center">
					<%
					SQL = "SELECT A.orderID, A.quantity, B.itemID, B.PartNumber, B.itemDesc_CO FROM we_orderdetails A INNER JOIN we_Items B ON A.itemID=B.itemID WHERE A.orderID = '" & OrderID & "' ORDER BY B.PartNumber"
					set RS = Server.CreateObject("ADODB.Recordset")
					RS.open SQL, oConn, 3, 3
					a = 0
					if RS.eof then
						response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
					else
						%>
						<%=RS.recordcount%> items found
						<br>
						<table border="1" cellpadding="3" cellspacing="0" width="90%" class="smlText">
							<tr>
								<td align="center" width="20"><b>Item#</b></td>
								<td align="center" width="110"><b>Part&nbsp;Number</b></td>
								<td align="center" width="230"><b>Description</b></td>
								<td align="center" width="40"><b>Qty</b></td>
								<td align="center" width="50"><b>No-Item</b></td>
							</tr>
							<%
							do until RS.eof
								a = a + 1
								%>
								<tr>
									<td align="center"><%=RS("itemID")%><input type="hidden" name="itemID<%=a%>" value="<%=RS("itemID")%>"></td>
									<td align="center"><nobr><%=RS("PartNumber")%><input type="hidden" name="PartNumber<%=a%>" value="<%=RS("PartNumber")%>"></nobr></td>
									<td align="center"><%=RS("itemDesc_CO")%></td>
									<td align="center"><input type="text" name="quantity<%=a%>" size="1" value="<%=RS("quantity")%>" maxlength="3"></td>
									<td align="center"><input type="checkbox" name="BackOrder<%=a%>" value="1"></td>
								</tr>
								<%
								RS.movenext
							loop
							%>
						</table>
						<input type="hidden" name="totalCount" value="<%=a%>">
						<%
					end if
						%>
				</td>
			</tr>
			<tr>
				<td class="bigText" align="center">
					<input type="hidden" name="ORDERID" value="<%=OrderID%>">
					<input type="submit" name="btnNoItemSend" class="smltext" value="Send NO Item Notification">
				</td>
			</tr>
		</form>
	</table>
<%
elseif btnNoItemSend <> "" then
	SQL = "SELECT A.fname, A.lname, A.email FROM CO_Accounts A INNER JOIN we_orders B ON A.accountID=B.accountID WHERE B.orderID = '" & OrderID & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if RS.eof then
		strError = "Record not found!"
	else
		'strEmail = "ruben@CellularOutfitter.com"
		strEmail = RS("email")
		fname = RS("fname")
		lname = RS("lname")
		strName = Ucase(left(fname, 1)) & right(fname,(len(fname)-1)) & " " & Ucase(left(lname, 1)) & right(lname,(len(lname)-1))
		cdo_body = "<html>" & vbcrlf
		cdo_body = cdo_body & "<head>" & vbcrlf
		cdo_body = cdo_body & "<style type='text/css'>" & vbcrlf
		cdo_body = cdo_body & "<!--" & vbcrlf
		cdo_body = cdo_body & ".regText {font-family: Times; font-size: 12pt;}" & vbcrlf
		cdo_body = cdo_body & ".headerText {font-family: Times; font-size: 14pt;}" & vbcrlf
		cdo_body = cdo_body & "-->" & vbcrlf
		cdo_body = cdo_body & "</style>" & vbcrlf
		cdo_body = cdo_body & "</head>" & vbcrlf
		cdo_body = cdo_body & "<body class='regText'>"
		cdo_body = cdo_body & "<table width='100%' cellpadding='8' cellspacing='0' align='center' bgcolor='#eaeaea' class='regText'><tr>" & vbcrlf
		cdo_body = cdo_body & "<td class='headerText' align='left' valign='top'><img src='http://www.cellularoutfitter.com/images/CellularOutfitter.jpg' border='0' hspace='12' align='absmiddle'></td>" & vbcrlf
		cdo_body = cdo_body & "<td class='headerText' align='center' valign='middle'><b>Cellular Outfitter Order Information</b></td>" & vbcrlf
		cdo_body = cdo_body & "</tr></table>" & vbcrlf
		cdo_body = cdo_body & "<table width='95%' cellpadding='5' cellspacing='1' align='center' class='regText'>" & vbcrlf
		cdo_body = cdo_body & "<tr><td colspan='2'><br><p><b>Hello " & strName & ",</b></p>"
		cdo_body = cdo_body & "<p>Unfortunately, the following item(s) from your CellularOutfitter.com order #" & OrderID & " are no longer available:</p>" & vbcrlf
		cdo_body = cdo_body & "<ul>" & vbcrlf
		b = 0
		strNotes = formatNotes("Notice sent to customer for item(s):<br>")
		for a = 1 to request.form("totalCount")
			if strError = "" and request.form("BackOrder" & a) = "1" then
				SQL = "SELECT itemDesc_CO FROM we_items WHERE itemID = '" & request.form("itemID" & a) & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if not RS2.eof then
					cdo_body = cdo_body & "<li><b>Part Number: " & request.form("PartNumber" & a) & "<br>" & vbcrlf
					cdo_body = cdo_body & "Item Description: " & RS2("itemDesc_CO") & "<br>" & vbcrlf
					cdo_body = cdo_body & "Quantity: " & request.form("quantity" & a) & "</b></li>" & vbcrlf
					strNotes = strNotes & request.form("quantity" & a) & " x " & RS2("itemDesc_CO") & "<br>"
					b = b + 1
				else
					strError = "Record not found!"
				end if
			end if
		next
		cdo_body = cdo_body & "</ul>" & vbcrlf
		cdo_body = cdo_body & "<p>Please return to our website and choose another item to replace the one above. Simply e-mail us back with your choice, and we will ship it immediately.</p>" & vbcrlf
		cdo_body = cdo_body & "<p>If there is not an item you would like at this time, please reply to this e-mail and let us know if you would prefer Store Credit or a Refund and we will gladly process your request.</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Thank you for choosing Cellular Outfitter,</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Customer Support<br>" & vbcrlf
		cdo_body = cdo_body & "<a href='http://www.CellularOutfitter.com/'>www.CellularOutfitter.com</a>.</p>" & vbcrlf
		cdo_body = cdo_body & "<table width='100%' cellpadding='5' cellspacing='5' align='center' bgcolor='#eaeaea'><tr><td colspan='2' class='regText'><a href='http://www.CellularOutfitter.com/track-your-order.asp'><b>Check Your Order Status Online</b></a></td></tr>" & vbcrlf
		cdo_body = cdo_body & "<tr><td colspan='2' class='regText'><b>Please visit us again soon!</b> - <a href='http://www.CellularOutfitter.com/'>www.CellularOutfitter.com</a></td></tr></table>" & vbcrlf
		cdo_body = cdo_body & "</body></html>"
		response.write "<h1 align=""center"">E-mail sent to " & strEmail & "</h1>" & vbcrlf
	end if
		if b = 0 then strError = "You did not select any products!"
	if strError = "" then
		cdo_from = "Automatic E-Mail from CellularOutfitter.com<sales@cellularoutfitter.com>"
		cdo_subject = "CellularOutfitter.com Order Information"
		'cdo_to = "ruben@CellularOutfitter.com"
		cdo_to = strEmail
		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		SQL = "SELECT ID,ordernotes FROM we_ordernotes WHERE orderid = '" & OrderID & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.Open SQL, oConn, 3, 3
		if not RS.EOF then
			SQL = "UPDATE we_ordernotes SET ordernotes='" & SQLquote(RS("ordernotes") & strNotes) & "' WHERE ID = '" & RS("ID") & "'"
		else
			SQL = "INSERT INTO we_ordernotes (orderid,ordernotes) VALUES ('" & OrderID & "','" & SQLquote(strNotes) & "')"
		end if
		oConn.execute SQL
	else
		response.write "<h1 align=""center"">" & strError & "</h1>" & vbcrlf
	end if
	'end of no longer avaliable


elseif btnBackOrderSend <> "" then
	SQL = "SELECT A.fname, A.lname, A.email FROM CO_accounts A INNER JOIN we_orders B ON A.accountID=B.accountID WHERE B.orderID = '" & OrderID & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if RS.eof then
		strError = "Record not found!"
	else
		strEmail = RS("email")
		fname = RS("fname")
		lname = RS("lname")
		strName = Ucase(left(fname, 1)) & right(fname,(len(fname)-1)) & " " & Ucase(left(lname, 1)) & right(lname,(len(lname)-1))
		cdo_body = "<html>" & vbcrlf
		cdo_body = cdo_body & "<head>" & vbcrlf
		cdo_body = cdo_body & "<style type='text/css'>" & vbcrlf
		cdo_body = cdo_body & "<!--" & vbcrlf
		cdo_body = cdo_body & ".regText {font-family: Times; font-size: 12pt;}" & vbcrlf
		cdo_body = cdo_body & ".headerText {font-family: Times; font-size: 14pt;}" & vbcrlf
		cdo_body = cdo_body & "-->" & vbcrlf
		cdo_body = cdo_body & "</style>" & vbcrlf
		cdo_body = cdo_body & "</head>" & vbcrlf
		cdo_body = cdo_body & "<body class='regText'>"
		cdo_body = cdo_body & "<table width='100%' cellpadding='8' cellspacing='0' align='center' bgcolor='#eaeaea' class='regText'><tr>" & vbcrlf
		cdo_body = cdo_body & "<td class='headerText' align='left' valign='top'><img src='http://www.cellularoutfitter.com/images/CellularOutfitter.jpg' border='0' hspace='12' align='absmiddle'></td>" & vbcrlf
		cdo_body = cdo_body & "<td class='headerText' align='center' valign='middle'><b>Cellular Outfitter Order Information</b></td>" & vbcrlf
		cdo_body = cdo_body & "</tr></table>" & vbcrlf
		cdo_body = cdo_body & "<table width='95%' cellpadding='5' cellspacing='1' align='center' class='regText'>" & vbcrlf
		cdo_body = cdo_body & "<tr><td colspan='2'><br><p><b>Hello " & strName & ",</b></p>"
		cdo_body = cdo_body & "<p>The following item(s) from your CellularOutfitter.com order are out of stock and currently on backorder:</p>" & vbcrlf
		cdo_body = cdo_body & "<ul>" & vbcrlf
		b = 0
		strNotes = "Backorder sent to customer for item(s):"
		for a = 1 to request.form("totalCount")
			if strError = "" and request.form("BackOrder" & a) = "1" then
				SQL = "SELECT itemDesc_CO FROM we_items WHERE itemID = '" & request.form("itemID" & a) & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if not RS2.eof then
					cdo_body = cdo_body & "<li><b>Part Number: " & request.form("PartNumber" & a) & "<br>" & vbcrlf
					cdo_body = cdo_body & "Item Description: " & RS2("itemDesc_CO") & "<br>" & vbcrlf
					cdo_body = cdo_body & "Quantity: " & request.form("quantity" & a) & "</b></li>" & vbcrlf
					strNotes = strNotes & vbcrlf & request.form("quantity" & a) & " x " & RS2("itemDesc_CO")
					sql = 	"insert into we_rma(orderid, partnumber, itemid, qty, rmaType, adminID)" & vbcrlf & _
							"values('" & OrderID & "', '" & request.form("PartNumber" & a) & "', '" & request.form("itemID" & a) & "', '" & request.form("quantity" & a) & "', 8, '" & adminID & "')"
					oConn.execute(sql)
					b = b + 1
				else
					strError = "Record not found!"
				end if
			end if
		next
		cdo_body = cdo_body & "</ul>" & vbcrlf
		cdo_body = cdo_body & "<p>We expect the item(s) to be back in stock within the next " & request.form("NumberOfDays") & " business days. If the status of the back ordered item(s) changes, you will be contacted by E-mail." & "</p>" & vbcrlf
		cdo_body = cdo_body & "<table width='95%'><tr><td class='regText'>"
		cdo_body = cdo_body & "<p>If you have any questions please reply to this E-mail.</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Best Regards,</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Customer Support<br>" & vbcrlf
		cdo_body = cdo_body & "<a href='http://www.CellularOutfitter.com/'>www.CellularOutfitter.com</a>.</p>" & vbcrlf
		cdo_body = cdo_body & "</td></tr></table>"
		cdo_body = cdo_body & "<table width='100%' cellpadding='5' cellspacing='5' align='center' bgcolor='#eaeaea'><tr><td colspan='2' class='regText'><a href='http://www.CellularOutfitter.com/track-your-order.asp'><b>Check Your Order Status Online</b></a></td></tr>" & vbcrlf
		cdo_body = cdo_body & "<tr><td colspan='2' class='regText'><b>Please visit us again soon!</b> - <a href='http://www.CellularOutfitter.com/'>www.CellularOutfitter.com</a></td></tr></table>" & vbcrlf
		cdo_body = cdo_body & "</body></html>"
		response.write "<h1 align=""center"">E-mail sent to " & strEmail & "</h1>" & vbcrlf
	end if
	if b = 0 then strError = "You did not select any products to Back-Order!"
	if strError = "" then
		cdo_from = "Automatic E-Mail from CellularOutfitter.com<sales@cellularoutfitter.com>"
		cdo_subject = "Cellular Outfitter Order Information"
		cdo_to = strEmail
		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		SQL = "SELECT ID,ordernotes FROM we_ordernotes WHERE orderid = '" & OrderID & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.Open SQL, oConn, 3, 3
		strNotes = formatNotes(strNotes)
		if not RS.EOF then
			SQL = "UPDATE we_ordernotes SET ordernotes='" & SQLquote(RS("ordernotes") & strNotes) & "' WHERE ID = '" & RS("ID") & "'"
		else
			SQL = "INSERT INTO we_ordernotes (orderid,ordernotes) VALUES ('" & OrderID & "','" & SQLquote(strNotes) & "')"
		end if
		oConn.execute SQL

		SQL = "UPDATE we_orders SET RMAstatus= '8' WHERE orderID = '" & OrderID & "'"
		oConn.execute SQL

	else
		response.write "<h1 align=""center"">" & strError & "</h1>" & vbcrlf
	end if
%>
<%
'----------------- Sent RMA modifiled Note--------------------
elseif btnModifiedSend <> "" then
	SQL = "SELECT A.fname, A.lname, A.email, B.ordersubtotal, B.ordergrandtotal, b.OrderShippingfee FROM CO_accounts A INNER JOIN we_orders B ON A.accountID=B.accountID WHERE B.orderID = '" & OrderID & "'" 
	'Response.Write(SQL)
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if RS.eof then
		strError = "Record not found!"
	else
		shipping = rs("OrderShippingfee")
		strEmail = RS("email")
		fname = RS("fname")
		lname = RS("lname")
		strName = Ucase(left(fname, 1)) & right(fname,(len(fname)-1)) & " " & Ucase(left(lname, 1)) & right(lname,(len(lname)-1))
		cdo_body = "<html>" & vbcrlf
		cdo_body = cdo_body & "<head>" & vbcrlf
		cdo_body = cdo_body & "<style type='text/css'>" & vbcrlf
		cdo_body = cdo_body & "<!--" & vbcrlf
		cdo_body = cdo_body & ".regText {font-family: Arial; font-size: 9pt;}" & vbcrlf
		cdo_body = cdo_body & ".headerText {font-family: Arial; font-size: 11pt;}" & vbcrlf
		cdo_body = cdo_body & "-->" & vbcrlf
		cdo_body = cdo_body & "</style>" & vbcrlf
		cdo_body = cdo_body & "</head>" & vbcrlf
		cdo_body = cdo_body & "<body class='regText'>"
		cdo_body = cdo_body & "<table width='100%' cellpadding='8' cellspacing='0' align='center' bgcolor='#eaeaea' class='regText'><tr>" & vbcrlf
		cdo_body = cdo_body & "<td class='headerText' align='left' valign='top'><img src='http://www.CellularOutfitter.com/images/CellularOutfitter.jpg' border='0' hspace='12' align='absmiddle'></td>" & vbcrlf
		cdo_body = cdo_body & "<td class='headerText' align='center' valign='middle'><b>Cellular Outfitter Order Information</b></td>" & vbcrlf
		cdo_body = cdo_body & "</tr></table>" & vbcrlf
		cdo_body = cdo_body & "<table width='95%' cellpadding='5' cellspacing='1' align='center' class='regText'>" & vbcrlf
		cdo_body = cdo_body & "<tr><td colspan='2'><br><p><b>Hello " & strName & ",</b></p>"
		cdo_body = cdo_body & "<p> Unfortunately,  the following item(s) from your CellularOutfitter.com order #"&OrderID&" are no  longer available:</p>" & vbcrlf
		cdo_body = cdo_body & "<ul>" & vbcrlf
		b = 0
		strNotes = formatNotes("Item out of stock, order  modified for item(s):<br>")
		for a = 1 to request.form("totalCount")
			if strError = "" and request.form("BackOrder" & a) = "1" then
				SQL = "SELECT itemDesc FROM we_items WHERE itemID = '" & request.form("itemID" & a) & "'"
				'SQL = "SELECT itemDesc, Price_our FROM we_items WHERE itemID = '" & request.form("itemID" & a) & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if not RS2.eof then
					cdo_body = cdo_body & "<li><b>Part Number: " & request.form("PartNumber" & a) & "<br>" & vbcrlf
					cdo_body = cdo_body & "Item Description: " & RS2("itemDesc") & "<br>" & vbcrlf
					cdo_body = cdo_body & "Quantity: " & request.form("quantity" & a) & "</b></li>" & vbcrlf
					strNotes = strNotes & request.form("quantity" & a) & " x " & RS2("itemDesc") & "<br>"
					b = b + 1
					SQLRMA = "UPDATE we_orderdetails SET quantity= '"&request.form("quantity" & a)&"'"
					SQLRMA = SQLRMA & " WHERE orderID = '" & OrderID & "' and itemID = '" & request.form("itemID" & a) & "'" 
					oConn.execute SQLRMA
				else
					strError = "Record not found!"
				end if
			end if
		next
		cdo_body = cdo_body & "</ul>" & vbcrlf
		cdo_body = cdo_body & "<p>The remaining item(s) on your order have been shipped and are currently in transit to you." & "</p>" & vbcrlf
		cdo_body = cdo_body & "<table width='95%'><tr><td class='regText'>" & vbcrlf
		cdo_body = cdo_body & "<p>The total amount charged has been updated to reflect this change in the order and will also be reflected on the order receipt included in your package.</p>" & vbcrlf
		if strComments <> "" then 
			cdo_body = cdo_body & "<p>NOTE:<br>" & strComments & "</p>" & vbcrlf
		end if
		cdo_body = cdo_body & "<p>Thank you for choosing Cellular Outfitter,</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Customer Support<br>" & vbcrlf
		cdo_body = cdo_body & "<a href='http://www.CellularOutfitter.com/'>www.CellularOutfitter.com</a>.</p>" & vbcrlf
		cdo_body = cdo_body & "</td></tr></table>"
		cdo_body = cdo_body & "<table width='100%' cellpadding='5' cellspacing='5' align='center' bgcolor='#eaeaea'><tr><td colspan='2' class='regText'><a href='http://www.CellularOutfitter.com/orderstatus.asp'><b>Check Your Order Status Online</b></a></td></tr>" & vbcrlf
		cdo_body = cdo_body & "<tr><td colspan='2' class='regText'><b>Please visit us again soon!</b> - <a href='http://www.CellularOutfitter.com/'>www.CellularOutfitter.com</a></td></tr></table>" & vbcrlf
		cdo_body = cdo_body & "</body></html>"
		response.write "<h1 align=""center"">E-mail sent to " & strEmail & "</h1>" & vbcrlf
	end if
	if b = 0 then strError = "You did not select any products to Sent Modifiled!"
	if strError = "" then
		cdo_from = "Automatic E-Mail from CellularOutfitter.com<sales@CellularOutfitter.com>"
		cdo_subject = "Cellular Outfitter Order Information"
		cdo_to = strEmail
		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		SQLnote = "SELECT ID,ordernotes FROM we_ordernotes WHERE orderid = '" & OrderID & "'"
		set RSnote = Server.CreateObject("ADODB.Recordset")
		RSnote.Open SQLnote, oConn, 3, 3
		if not RSnote.EOF then
			SQLnote = "UPDATE we_ordernotes SET ordernotes='" & SQLquote(RSnote("ordernotes") & strNotes) & "' WHERE ID = '" & RSnote("ID") & "'"
		else
			SQLnote = "INSERT INTO we_ordernotes (orderid,ordernotes) VALUES ('" & OrderID & "','" & SQLquote(strNotes) & "')"
		end if
		oConn.execute SQLnote
		shipping = rs("OrderShippingfee")
		SQLTotal = "SELECT A.orderID, A.quantity, B.itemID, b.price_CO FROM we_orderdetails A INNER JOIN we_Items B ON A.itemID=B.itemID WHERE A.orderID = '" & OrderID & "'"
		set RStotal = Server.CreateObject("ADODB.Recordset")
		RStotal.Open SQLTotal, oConn, 3, 3
		if not RStotal.eof then
			do while not rstotal.eof
				newtotal = Cint(rstotal("quantity")) * rstotal("price_CO")
				newsubtotal = newsubtotal + newtotal
				rstotal.movenext
			Loop
				if newsubtotal = 0 then
					newgrandtotal  = 0 
				else
					newgrandtotal = newsubtotal +shipping
				end if
			SQLgrandtotal = "UPDATE we_orders SET RMAstatus= '9', ordersubtotal = '"& newsubtotal &"', orderGrandtotal = '"& newGrandtotal &"' WHERE orderID = '" & OrderID & "'"
			oConn.execute SQLgrandtotal
		end if
	else
		response.write "<h1 align=""center"">" & strError & "</h1>" & vbcrlf
	end if
' end if item modifled sent
'-----other refund send-------
elseif btnOtherRefundSend <> "" then
	SQL = "SELECT a.email, a.fname, a.lname FROM CO_Accounts A INNER JOIN WE_Orders B ON A.accountID=B.accountID WHERE B.orderID = '" & OrderID & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		if not isNull(RS("email")) then strTo = RS("email")
	end if
	if trim(strTo) <> "" then
		strSubject = "Your Refund Has Been Processed"
		strCR = request.form("txtcredit")
		refundreason = request.form("refundreason")
		cbOtherRefundReason = request.form("cbOtherRefundReason")
		strError = ""
		strfname = RS("fname")
		strlname = RS("lname")
		ordergrandtotal = 0
		if not isNumeric(strCR) then strError = "Credit amount must be a numeric value!"
		if strError = "" then
			strCR = cDbl(strCR)
			SQL = "SELECT ordergrandtotal FROM we_orders WHERE orderid = '" & OrderID & "'"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			if not RS.eof then
				ordergrandtotal = cDbl(RS("ordergrandtotal"))
				'if strCR > ordergrandtotal then strError = "Refund amount may not exceed Order Total!"
			else
				strError = "Order not found!"
			end if
		end if
		if strError = "" then
			if strCR < ordergrandtotal then
				strNotes = formatNotes("Partial Credit issued for " & formatCurrency(strCR) & ". Email sent to customer.")
				msgSuccess = "PARTIAL REFUND PROCESSED for " & formatCurrency(strCR)
				RMAstatus = 6
				SQL = "UPDATE we_orders SET refundAmount='" & strCR & "', refundDate='" & now & "', refundreason ='"&refundreason&"' WHERE orderID = '" & OrderID & "'"
				'response.write "<p>" & SQL & "</p>" & vbcrlf
				oConn.execute SQL
			else
				strNotes = formatNotes("Full Credit issued for " & formatCurrency(strCR) & ". Email sent to customer.")
				msgSuccess = "FULL REFUND PROCESSED for " & formatCurrency(strCR)
				RMAstatus = 7
				SQL = "UPDATE we_orders SET refundAmount='" & strCR & "', refundDate='" & now & "', refundreason ='"&refundreason&"' WHERE orderID = '" & OrderID & "'"
				'response.write "<p>" & SQL & "</p>" & vbcrlf
				oConn.execute SQL
			end if
			strBody = "<p>Dear " & strFname & " " & strLname & ",</p>" & vbcrlf
			strBody = strBody & "<p>This e-mail is to inform you that we have received your refund request."
			strBody = strBody & " and have processed your credit in the amount of <b>" & formatCurrency(strCR) & "</b>.</p>" & vbcrlf
			strBody = strBody & "<p>Please allow a few days for posting.</p>" & vbcrlf
			strBody = strBody & "<p>Thank you for shopping with CellularOutfitter.com</p>" & vbcrlf
			strBody = strBody & "<p>Please contact us if you have any questions.</p>" & vbcrlf
			if strComments <> "" then strBody = strBody & "<p>NOTE:<br>" & strComments & "</p>" & vbcrlf
			strBody = strBody & "<p>Best regards,</p>" & vbcrlf
			strBody = strBody & "<p><font color=#999999><b>Cellphone</font> <font color=#FF8100>Accents</font> Service</b></p>" & vbcrlf
			strBody = strBody & "<p><a href=""mailto:service@cellularoutfitter.com"">service@cellularoutfitter.com</a><br>" & vbcrlf
			strBody = strBody & "<a href=""http://www.CellularOutfitter.com"">http://www.CellularOutfitter.com</a></p>" & vbcrlf
			CDOsend strTo,"service@cellularoutfitter.com",strSubject,strBody
		else
			response.write "<h3 align=""center""><font color=""#FF0000"">ERROR!<br>" & strError & "</font></h3>" & vbcrlf
			response.write "<form><p align=""center""><input type='button' onclick='history.back();' class='smltext' value='BACK'></p></form>" & vbcrlf
			response.write "</body></html>" & vbcrlf
			response.end
		end if

		sql = 	"insert into rmalog(site_id,orderid,rmastatus,activity_desc,amountCredited,confirmAmountCredited,returnreason,refundreason,adminID)" & vbcrlf & _
				"values(2,'" & orderID & "','" & RMAstatus & "','Other Refund','" & strCR & "',1,'" & cbOtherRefundReason & "','" & cbOtherRefundReason & "','" & adminID & "')"
		oConn.execute(sql)
		
		SQL = "SELECT ID,ordernotes FROM we_ordernotes WHERE orderid = '" & OrderID & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			SQL = "UPDATE we_ordernotes SET ordernotes='" & SQLquote(RS("ordernotes") & strNotes) & "' WHERE ID = '" & RS("ID") & "'"
		else
			SQL = "INSERT INTO we_ordernotes (orderid,ordernotes) VALUES ('" & OrderID & "','" & SQLquote(strNotes) & "')"
		end if
		oConn.execute SQL
		SQL = "UPDATE we_orders SET RMAstatus='" & RMAstatus & "' WHERE orderID = '" & OrderID & "'"
		oConn.execute SQL
		response.write "<h3 align=""center"">" & msgSuccess & "</h3>" & vbcrlf
	else
		response.write "<h3 align=""center"">Could not find e-mail address.</h3>" & vbcrlf
	end if
%>
<!-- end of other refund sent-->
<%
else
	SQL = "SELECT RMAstatus FROM we_orders WHERE orderid = '" & OrderID & "'"
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.Open SQL, oConn, 3, 3
	if not RS.EOF then
		if not isNull(RS("RMAstatus")) then
			RMAstatus = RS("RMAstatus")
		else
			RMAstatus = 0
		end if
	else
		RMAstatus = 0
	end if
	%>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
		<form name="frmRefundExchange" method="post" action="RMA_CO.asp">
		<%
		select case RMAstatus
			case 1
				%>
				<tr>
					<td class="bigText" bgcolor="#CCCCCC" align="center">
						Receive EXCHANGE Product(s)
					</td>
				</tr>
				<tr>
					<td colspan="2" class="smlText" align="center">
						Additional customer comments:<br><textarea name="comments" cols="50" rows="4"></textarea>
					</td>
				</tr>
				<tr>
					<td class="bigText" align="center">
						<input type="hidden" name="ORDERID" value="<%=OrderID%>">
						<input type="submit" name="btnExchange" class="smltext" value="Re-Send EXCHANGE">
						<p>&nbsp;</p>
						<input type="submit" name="btnExchange" class="smltext" value="Product(s) Received [re-ship]">
						<br>
						<input type="submit" name="btnExchange" class="smltext" value="Product(s) Received [exchange]">
					</td>
				</tr>
				<%
			case 2
				%>
				<tr>
					<td class="bigText" bgcolor="#CCCCCC" align="center">
						Process EXCHANGE
					</td>
				</tr>
				<tr>
					<td colspan="2" class="smlText" align="center">
						Additional customer comments:<br><textarea name="comments" cols="50" rows="4"></textarea>
					</td>
				</tr>
				<tr>
					<td class="bigText" align="center">
						<input type="hidden" name="ORDERID" value="<%=OrderID%>">
						<input type="submit" name="btnExchange" class="smltext" value="Process EXCHANGE">
					</td>
				</tr>
				<%
			case 3
				%>
				<tr>
					<td class="bigText" bgcolor="#CCCCCC" align="center">
						Receive RETURN Product(s)
					</td>
				</tr>
				<tr>
					<td colspan="2" class="smlText" align="center">
						Additional customer comments:<br><textarea name="comments" cols="50" rows="4"></textarea>
					</td>
				</tr>
               <% '======change from here%>
                <tr>
					<td colspan="2" class="smlText" align="center">
						<%
						sql = 	"select	a.itemid, b.partnumber, b.itemdesc, isnull((select top 1 inv_qty from we_items where partnumber = b.partnumber and master = 1 order by inv_qty desc), 0) inv_qty, a.quantity" & vbcrlf & _
								"	,	case when c.store = 0 then b.price_our" & vbcrlf & _
								"			when c.store = 1 then b.price_ca" & vbcrlf & _
								"			when c.store = 2 then b.price_co" & vbcrlf & _
								"			when c.store = 3 then b.price_ps" & vbcrlf & _
								"			when c.store = 10 then" & vbcrlf & _
								"				case when b.price_er > 0 then b.price_er" & vbcrlf & _
								"					else case when (b.price_our-5.0) < 3.99 then 3.99 else (b.price_our-5.0) end" & vbcrlf & _
								"				end " & vbcrlf & _
								"			else b.price_our" & vbcrlf & _
								"		end price_our" & vbcrlf & _
								"	,	isnull(c.orderTax, 0) orderTax, v.sState" & vbcrlf & _
								"from	we_orders c join we_orderdetails a " & vbcrlf & _
								"	on	c.orderid = a.orderid join we_items b " & vbcrlf & _
								"	on	a.itemid = b.itemid join v_accounts v" & vbcrlf & _
								"	on	c.accountid = v.accountid and c.store = v.site_id" & vbcrlf & _
								"where	c.orderid = '" & OrderID & "'" & vbcrlf & _
								"order by b.partnumber"
'						response.write "<pre>" & sql & "</pre>"
						set RS = Server.CreateObject("ADODB.Recordset")
						RS.open SQL, oConn, 3, 3
						a = 0
						if RS.eof then
							response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
						else
							rsCount = RS.recordcount
							%>
							<br>
                            <div align="right" style="width:90%;"><%=rsCount%> items found</div>
							<table border="1" cellpadding="3" cellspacing="0" width="90%" class="smlText" style="border-collapse:collapse;">
								<tr>
									<td align="center" width="20"><b>Item#</b></td>
									<td align="center" width="80"><b>EXCHANGE</b></td>
									<td align="center" width="110"><b>Part&nbsp;Number</b></td>
									<td align="center" width="*"><b>Description</b></td>
									<td align="center" width="40"><b>Qty</b></td>
									<td align="center" width="80"><b>RECEIVE</b></td>
								</tr>
								<%
								reStockingFeePercent = 15
								do until RS.eof
									a = a + 1
									price_our = cdbl(RS("price_our"))
									qty = cdbl(RS("quantity"))
									itemTotalPrice = cdbl(price_our * qty)
									itemTax = cdbl(0.0)
									sState = RS("sState")
									if sState = "CA" then itemTax = cdbl(itemTotalPrice*0.0775)
									amountCredit = round(itemTotalPrice * (1-(reStockingFeePercent/100.0)), 2)
									amountCredit = round(amountCredit + itemTax, 2)
									%>
									<tr>
										<td align="center"><%=RS("itemID")%><input type="hidden" name="itemID<%=a%>" value="<%=RS("itemID")%>"></td>
                                        <td align="center"><input type="radio" name="rdoReceive<%=a%>" value="E" onclick="javascript:toggleReceive(this.value,'id_exchange_<%=a%>','id_receive_<%=a%>');" /> Exchange</td>
										<td align="center"><nobr><%=RS("PartNumber")%><input type="hidden" name="PartNumber<%=a%>" value="<%=RS("PartNumber")%>"></nobr></td>
										<td align="center">
											<%=RS("itemDesc")%> - <%=RS("inv_qty")%>
                                            <%if RS("inv_qty") <= 0 then%>
                                            <br /><span style="color:#f00; font-weight:bold;"> - Currently Out Of Stock - </span>
                                            <%end if%>
                                            <input type="hidden" name="itemdesc<%=a%>" value="<%=RS("itemDesc")%>">
                                        </td>
										<td align="center">
                                        	<input type="hidden" name="quantityold<%=a%>" value="<%=qty%>">
											<input type="text" name="quantity<%=a%>" id="id_quantity<%=a%>" size="1" value="<%=qty%>" maxlength="3" onkeyup="updateCreditAmount();">
										</td>
										<td align="left">
                                        	<input type="radio" name="rdoReceive<%=a%>" value="R" onclick="javascript:toggleReceive(this.value,'id_receive_<%=a%>','id_exchange_<%=a%>');" /> Received
                                        	<input type="radio" name="rdoReceive<%=a%>" value="T" onclick="javascript:toggleReceive(this.value,'id_receive_<%=a%>','id_exchange_<%=a%>');" /> Trashed
                                        	<input type="radio" name="rdoReceive<%=a%>" value="N" onclick="javascript:toggleReceive(this.value,'id_receive_<%=a%>','id_exchange_<%=a%>');" checked="checked" /> None
                                        </td>
									</tr>
                                    <tr>
                                    	<td colspan="6" align="left" id="id_receive_<%=a%>" style="display:none;">
                                        	<div style="float:left; padding-top:5px;">
                                            	<b>Item Price:</b> $<span id="id_origItemPrice_<%=a%>"><%=formatnumber(price_our,2)%></span>
	                                        	<input type="hidden" name="origItemPrice<%=a%>" value="<%=formatnumber(price_our,2)%>">
											</div>
                                        	<div style="float:left; padding:5px 0px 0px 25px;">
                                            	<b>Item Tax:</b> $<span id="id_itemTax_<%=a%>"><%=formatnumber(itemTax,2)%></span>
	                                        	<input type="hidden" id="id_itemTax<%=a%>" name="itemTax<%=a%>" value="<%=formatnumber(itemTax,2)%>">
											</div>
                                            <div style="padding:5px 0px 0px 25px; float:left; font-weight:bold;">Discount:</div>
                                            <div style="float:left;">&nbsp;
                                                <input type="text" id="id_discountPercent<%=a%>" name="discountPercent<%=a%>" style="width:40px;" value="0" onkeyup="updateCreditAmount();" />%
                                            </div>
                                        	<div style="padding:5px 0px 0px 25px; float:left; font-weight:bold;">Re-Stocking Fee:</div>
                                        	<div style="float:left;">&nbsp;
                                            	<input type="text" id="id_txtReStockingFeePercent<%=a%>" name="txtReStockingFeePercent<%=a%>" value="15" style="width:30px;" onkeyup="updateCreditAmount();" />%
											</div>
                                        	<div style="padding:5px 0px 0px 25px; float:left; font-weight:bold;">Credit:</div>
                                        	<div>&nbsp;
                                            	$<input type="text" id="id_amountCredited<%=a%>" name="amountCredited<%=a%>" style="width:50px;" value="<%=amountCredit%>" readonly="readonly" />
											</div>
                                        	<div style="padding:5px 0px 0px 0px; float:left; font-weight:bold;">Return Reason:</div>
                                        	<div style="float:left;">&nbsp;
												<select name="cbReturnReason<%=a%>">
                                                <%
												set rsReturnReason = oConn.execute("select id, reason from xreturnreason")
												do until rsReturnReason.eof
													%>
                                                    <option value="<%=rsReturnReason("id")%>"><%=rsReturnReason("reason")%></option>
                                                    <%
													rsReturnReason.movenext
												loop
												%>
                                                </select>
											</div>
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td colspan="6" style="display:none;" id="id_exchange_<%=a%>">
                                            <table width="100%" border="1" cellpadding="2" cellspacing="0" align="left" class="regText" style="border-color:#FFFFFF; border-collapse:collapse; background-color:#EAEAEA;">
                                                <tr>
                                                    <td width="200" align="right"><b>PartNumber:</b></td>
                                                    <td align="left"><input type="text" name="exPartNumber<%=a%>" value="<%=RS("PartNumber")%>" size="15"/></td>
                                                </tr>
                                                <tr>
                                                    <td width="200" align="right"><b>Reship Reason:</b></td>
                                                    <td align="left">
                                                    	<%
														sql = "select reshipDesc from xreshipreason where id in (5,9) order by 1"
														set rsReship = oConn.execute(sql)
														do until rsReship.eof
															%>
                                                            <input type="radio" name="rdoReshipReason<%=a%>" value="<%=rsReship("reshipDesc")%>" <%if "Exchange" = rsReship("reshipDesc") then%>checked<%end if%> /><%=rsReship("reshipDesc")%>&nbsp;
                                                            <%
															rsReship.movenext
														loop
														%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200" align="right"><b>Receive:</b></td>
                                                    <td align="left">
			                                        	<input type="radio" name="rdoExchangeReceive<%=a%>" value="R" checked /> Received
			                                        	<input type="radio" name="rdoExchangeReceive<%=a%>" value="T" /> Trashed
                                            		</td>
                                                </tr>
                                                <tr>
                                                    <td width="200" align="right"><b>Return Reason:</b></td>
                                                    <td align="left">
                                                        <select name="cbExchangeReturnReason<%=a%>">
                                                        <%
                                                        set rsReturnReason = oConn.execute("select id, reason from xreturnreason")
                                                        do until rsReturnReason.eof
                                                            %>
                                                            <option value="<%=rsReturnReason("id")%>"><%=rsReturnReason("reason")%></option>
                                                            <%
                                                            rsReturnReason.movenext
                                                        loop
                                                        %>
                                                        </select>
                                            		</td>
                                                </tr>
                                                <tr>
                                                    <td width="200" align="right"><b>Additional Exchange Note:</b></td>
                                                    <td align="left"><textarea name="exchangeNote<%=a%>" cols="40" rows="3"></textarea></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
									<%
									RS.movenext
								loop
								%>
							</table>
                            <div align="right" style="width:90%;">
                            	Total Credit Amount: $<span id="id_totalCredit"></span>
	                            <input type="hidden" name="totalCredit" value="">
							</div>
							<input type="hidden" name="totalCount" value="<%=a%>">
                            <input type="hidden" name="hidSState" value="<%=sState%>" />                            
							<%
						end if
						%>
					</td>
				</tr>
                <%'end of change here%>               
				<tr>
					<td class="bigText" align="center">
						<input type="hidden" name="ORDERID" value="<%=OrderID%>">
						<input type="submit" name="btnReturn" class="smltext" value="Product(s) Received">
						<br>
						<br />
						<input type="submit" name="btnReturn" class="smltext" value="Re-Send RETURN">
					</td>
				</tr>
				<%
			case 4
				sql = 	"select	a.id, a.itemid, a.qty, a.partnumber, a.activity_desc, a.origItemPrice, isnull(a.restocking_fee_percent, 0) restocking_fee_percent, isnull(a.discountPercent, 0) discountPercent" & vbcrlf & _
						"	, 	isnull(a.itemTax, 0) itemTax, isnull(a.amountCredited, 0) amountCredited, a.logDate received_date, a.returnReason, a.adminID" & vbcrlf & _
						"	,	b.fname + ' ' + b.lname userName" & vbcrlf & _
						"from	rmalog a left outer join we_adminUsers b" & vbcrlf & _
						"	on	a.adminID = b.adminID" & vbcrlf & _
						"where	a.site_id = 2" & vbcrlf & _
						"	and	a.orderid = '" & orderid & "'" & vbcrlf & _
						"	and	(a.confirmAmountCredited is null or a.confirmAmountCredited = 0)" & vbcrlf & _
						"	and	a.rmastatus = 4" & vbcrlf & _
						"order by 1"
				set rsRefund = oConn.execute(sql)

				creditAmount = cdbl(0)
				if rsRefund.eof then
				%>
				<tr>
					<td align="center" style="padding-bottom:10px; font-size:15px;">
                    	No product(s) received history found
					</td>
				</tr>
                <%
				else
				%>
				<tr>
					<td align="center" style="font-size:15px;">
                    	Product(s) received history
                    </td>
				</tr>
				<tr>
					<td align="center" style="padding-bottom:10px;">
						<table border="1" cellpadding="3" cellspacing="0" width="99%" class="smlText" style="border-collapse:collapse;">
                        	<tr bgcolor="#CCCCCC">
                            	<td>ItemID</td>
                            	<td>QTY</td>
                            	<td>Orig.<br />PartNumber</td>
                            	<td>ActivityDesc</td>
                            	<td>OrigItemPrice</td>
                            	<td>RS Fee Percent</td>
                            	<td>Discount Percent</td>
                            	<td>ItemTax</td>
                            	<td>Credit Amount</td>
                            	<td>Received Date</td>
                            	<td>Admin User</td>
                            </tr>
					<%
					bExchange = false
					bRefund = false
					tempReturnReason = 0
					do until rsRefund.eof
						tempReturnReason = rsRefund("returnReason")
						creditAmount = creditAmount + rsRefund("amountCredited")
						if instr(rsRefund("activity_desc"), "(E)") > 0 then
							bExchange = true
						else
							bRefund = true
						end if
						%>
                        	<input type="hidden" name="hidRefundItemID" value="<%=rsRefund("itemid")%>" />
                        	<tr>
                            	<td><%=rsRefund("itemid")%></td>
                            	<td><%=rsRefund("qty")%></td>
                            	<td><%=rsRefund("partnumber")%></td>
                            	<td><%=rsRefund("activity_desc")%></td>
                            	<td><%=formatcurrency(rsRefund("origItemPrice"))%></td>
                            	<td><%=formatnumber(rsRefund("restocking_fee_percent"),2)&"%" %></td>
                            	<td><%=formatnumber(rsRefund("discountPercent"),2)&"%" %></td>                                
                            	<td><%=formatcurrency(rsRefund("itemTax"))%></td>
                            	<td><%=formatcurrency(rsRefund("amountCredited"))%></td>
                            	<td><%=rsRefund("received_date")%></td>
                            	<td><%=rsRefund("userName")%></td>
                            </tr>
                        <%
						rsRefund.movenext
					loop
					%>
                        </table>
					</td>
				</tr>                
                <%
				end if
				
				strSubmit = ""
				if bExchange and bRefund then
					strSubmit = "Process EXCHANGE & REFUND"
				elseif bExchange then
					strSubmit = "Process EXCHANGE"
				elseif bRefund then
					strSubmit = "Process REFUND"
				end if
				%>
				<tr>
					<td class="bigText" bgcolor="#CCCCCC" align="center">
						<%=strSubmit%>
					</td>
				</tr>
                <%if bRefund then%>
				<tr>
					<td colspan="2" class="bigText" bgcolor="#CCCCCC" align="center">
						Enter Credit Amount For Order Id: <%=OrderID%><br>
						<input type="text" name="txtcredit" size="10" value="<%=creditAmount%>">
					</td>
				</tr>
                <%end if%>
				<tr>
					<td colspan="2" class="smlText" align="center">
						Additional customer comments:<br><textarea name="comments" cols="50" rows="4"></textarea>
					</td>
				</tr>
				<tr>
					<td class="bigText" align="center">
						<input type="hidden" name="refundreason" value="<%=tempReturnReason%>" />
						<input type="hidden" name="ORDERID" value="<%=OrderID%>">
						<input type="submit" name="btnReturn" class="smltext" value="<%=strSubmit%>">
					</td>
				</tr>
				<%
			case else
				%>
				<tr>
					<td class="bigText" bgcolor="#CCCCCC" align="center">
						Start a New RMA
					</td>
				</tr>
				<tr>
					<td colspan="2" class="smlText" align="center">
						Additional customer comments:<br><textarea name="comments" cols="50" rows="4"></textarea>
					</td>
				</tr>
				<tr>
					<td class="bigText" align="center">
						<input type="hidden" name="ORDERID" value="<%=OrderID%>">
						<input type="submit" name="btnReturn" class="smltext" value="New RETURN">
                        <input type="submit" name="btnModified" class="smltext" id="Modified" value="Modified" />
			          	<input type="submit" name="btnOtherRefund" class="smltext" id="OtherRefund" value="OtherRefund" />
                        <br>
						<input type="submit" name="btnBackOrder" class="smltext" value="Send BACK-ORDER Notification"><br>
                        <input type="submit" name="btnNoItem" class="smltext" value="ITEM NO LONGER Avaliable Note">
					</td>
				</tr>
				<%
		end select
		%>
		</form>
	</table>
	<%
end if

if isObject(RS) then
	RS.Close
	SET RS = NOTHING
end if

sub UpdateInvQty(nPartNumber, nProdQuantity, KIT)
	' Updates inv_qty for the Master Part (item with the same Part Number as the item purchased which does not have -1 inv_qty)
	' If the inv_qty would be adjusted to less than or equal to zero, then all items with the same Part Number as the item purchased are set to zero
	if isNull(KIT) then
		SQL = "SELECT itemID,typeID,inv_qty FROM we_items WHERE PartNumber = '" & nPartNumber & "' AND inv_qty > -1"
	else
		SQL = "SELECT itemID,typeID,inv_qty FROM we_items WHERE itemID IN (" & KIT & ")"
	end if
	set oRsItemInfo2 = Server.CreateObject("ADODB.Recordset")
	oRsItemInfo2.open SQL, oConn, 3, 3
	do until oRsItemInfo2.eof
		if oRsItemInfo2("inv_qty") + nProdQuantity > 0 then
			decreaseSQL = "UPDATE we_items SET inv_qty = inv_qty + " & nProdQuantity & " WHERE itemID = '" & oRsItemInfo2("itemID") & "'"
			'response.write "<li>" & decreaseSQL & "</li>" & vbcrlf
			oConn.execute(decreaseSQL)
			sql = "insert into we_invRecord (itemID,inv_qty,orderQty,adjustQty,orderID,adminID,notes) values(" & oRsItemInfo2("itemID") & ",0,0," & nProdQuantity & ",0," & prepInt(adminID) & ",'CO RMA for " & nPartNumber & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
		else
			decreaseSQL = "UPDATE we_items SET inv_qty = 0 WHERE PartNumber = '" & nPartNumber & "'"
			if oRsItemInfo2("typeID") = 2 then decreaseSQL = decreaseSQL & " AND inv_qty > -1"
			if oRsItemInfo2("typeID") <> 3 then
				' Send zero-inventory e-mail to Tony
				cdo_from = "Automatic E-Mail from CellularOutfitter.com<sales@CellularOutfitter.com>"
				cdo_subject = nPartNumber & " is out of stock!"
				cdo_body = nPartNumber & " is out of stock!"
				cdo_to = "tony@wirelessemporium.com,charles@wirelessemporium.com,steven@wirelessemporium.com,jon@wirelessemporium.com"
				CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
			end if
			'response.write "<li>" & decreaseSQL & "</li>" & vbcrlf
			oConn.execute(decreaseSQL)
			sql = "insert into we_invRecord (itemID,inv_qty,orderQty,adjustQty,orderID,adminID,notes) values(" & oRsItemInfo2("itemID") & ",0,0,0,0," & prepInt(adminID) & ",'CO RMA for " & nPartNumber & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		oRsItemInfo2.movenext
	loop
	oRsItemInfo2.close
	set oRsItemInfo2 = nothing
end sub

sub NumberOfSales(nIdProd, QTY, KIT)
	if isNull(KIT) then
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID = '" & nIdProd & "'"
	else
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID IN (" & KIT & ")"
	end if
	'response.write "<li>" & SQL & "</li>" & vbcrlf
	oConn.execute(SQL)
end sub
%>

<%
if strMessage <> "" then
	response.write strMessage
end if					
%>

<form name="CloseWindow"><p align="center"><input type="button" onClick="window.close();" class="smltext" value="Close"></p></form>
<script>
	window.onload = function () {updateCreditAmount();}
	
	function getCheckedRadioButton(radioSet) { 
		for (var i=0; i<radioSet.length; i++) 
			if (radioSet[i].checked) return i;
	
		return -1;
	}
	
	function updateCreditAmount() {
//		if (isNaN(qty)) qty = 1;
//		if (isNaN(RSPercent)) RSPercent = 15;
		loopItem = document.frmRefundExchange.totalCount.value;
		sState = document.frmRefundExchange.hidSState.value;		
		totalCreditAmount = CurrencyFormatted(0);			

		for (i=1; i<=loopItem; i++) {
			rdoSelectedIndex = getCheckedRadioButton(eval('document.frmRefundExchange.rdoReceive'+i));
			if (rdoSelectedIndex == 1 || rdoSelectedIndex == 2) {
				origItemPrice = CurrencyFormatted(document.getElementById('id_origItemPrice_'+i).innerHTML);
				itemQty = CurrencyFormatted(document.getElementById('id_quantity'+i).value);
				itemPriceTotal = origItemPrice * itemQty;

				if (sState == "CA") itemTaxTotal = roundUp(itemPriceTotal * 0.0775);
				else itemTaxTotal = 0.00;
				document.getElementById('id_itemTax_'+i).innerHTML = itemTaxTotal;
				document.getElementById('id_itemTax'+i).value = itemTaxTotal;
				
				discountPercent = CurrencyFormatted(document.getElementById('id_discountPercent'+i).value);
				itemPriceTotal = CurrencyFormatted(itemPriceTotal * (1-(discountPercent/100.0)));				
				
				RSPercent = CurrencyFormatted(document.getElementById('id_txtReStockingFeePercent'+i).value);
				creditAmount = CurrencyFormatted(itemPriceTotal*(1-(RSPercent/100.0)));
				creditAmount = CurrencyFormatted(parseFloat(creditAmount) + parseFloat(itemTaxTotal));
				document.getElementById('id_amountCredited'+i).value = creditAmount;
				totalCreditAmount = CurrencyFormatted(parseFloat(totalCreditAmount) + parseFloat(creditAmount));
			}
		}
		document.getElementById('id_totalCredit').innerHTML = totalCreditAmount;
		document.frmRefundExchange.totalCredit.value = totalCreditAmount;
	}
	
	function toggleReceive(rdoVal, on, off) {
		if (rdoVal == "N") {
			document.getElementById(on).style.display = "none";
			document.getElementById(off).style.display = "none";
		} else {
			document.getElementById(on).style.display = "";
			document.getElementById(off).style.display = "none";
		}
		updateCreditAmount();
	}
	
	function roundUp(amount) {
		num = Math.ceil(amount * 100) / 100;
		return num;
	}
		
	function CurrencyFormatted(amount) {
		var i = parseFloat(amount);
		if(isNaN(i)) { i = 0.00; }
		var minus = '';
		if(i < 0) { minus = '-'; }
		i = Math.abs(i);
		i = parseInt(i * 100);
		i = i / 100;
		s = new String(i);
		if(s.indexOf('.') < 0) { s += '.00'; }
		if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
		s = minus + s;
		return s;
	}
</script>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
