<%
pageTitle = "Admin - Upload Tracking Numbers"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<font face="Arial,Helvetica">
<blockquote>
<blockquote>
<br><br><br>

<%
function fFormatEmailBodyRow(sDescription,sValue)
	fFormatEmailBodyRow = "<tr><td valign='top' width='125' bgcolor='#eaeaea'><b>" & sDescription & "</b></td><td valign='top'>" & sValue & "</td></tr>" & vbcrlf
end function

function fFormatAddress(sAdd1,sAdd2,sCity,sState,sZip)
	dim cdo_body
	cdo_body = sAdd1
	if sAdd2 <> "" then cdo_body = cdo_body & "<br>" & sAdd2
	cdo_body = cdo_body & "<br>" & sCity & ", " & sState & "&nbsp;" & sZip & "<br>" & vbcrlf
	fFormatAddress = cdo_body
end function

function formatSEO_CA(val)
	if not isNull(val) then
		formatSEO_CA = lCase(trim(replace(val,"&trade;","-")))
		formatSEO_CA = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO_CA," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO_CA = replace(replace(replace(replace(formatSEO_CA,"(","-"),")","-"),";","-"),":","-")
	else
		formatSEO_CA = ""
	end if
	select case formatSEO_CA
		case "sidekick" : formatSEO_CA = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO_CA = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO_CA = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO_CA = "sprint-nextel"
		case "u-s--cellular" : formatSEO_CA = "us-cellular"
		case "antennas-parts" : formatSEO_CA = "antennas"
		case "faceplates" : formatSEO_CA = "films-faceplates"
		case "bling-kits---charms" : formatSEO_CA = "charms-bling-kits"
		case "data-cable---memory" : formatSEO_CA = "data-cables-memory-cards"
		case "hands-free" : formatSEO_CA = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO_CA = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO_CA = "holsters-phone-holders"
		case "leather-cases" : formatSEO_CA = "cases-pouches-skins"
		case "full-body-protectors" : formatSEO_CA = "protective-films"
	end select
end function

function formatSEO_CO(val)
	if not isNull(val) then
		formatSEO_CO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO_CO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO_CO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO_CO = replace(replace(replace(replace(replace(formatSEO_CO,"(","-"),")","-"),";","-"),"+","-"),":","-")
	else
		formatSEO_CO = ""
	end if
	select case formatSEO_CO
		case "sidekick" : formatSEO_CO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO_CO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO_CO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO_CO = "sprint-nextel"
		case "u-s--cellular" : formatSEO_CO = "us-cellular"
		case "antennas-parts" : formatSEO_CO = "antennas"
		case "faceplates" : formatSEO_CO = "covers-screen-guards"
		case "bling-kits---charms" : formatSEO_CO = "bling-kits-charms"
		case "data-cable---memory" : formatSEO_CO = "data-cables-memory-cards"
		case "hands-free" : formatSEO_CO = "hands-free-kits-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO_CO = "holsters-car-mounts"
		case "leather-cases" : formatSEO_CO = "cases-pouches"
		case "cell-phones" : formatSEO_CO = "wholesale-cell-phone"
		case "full-body-protectors" : formatSEO_CO = "invisible-film-protectors"
	end select
end function

function formatSEO_PS(val)
	if not isNull(val) then
		formatSEO_PS = lCase(trim(replace(val,"&trade;","-")))
		formatSEO_PS = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO_PS," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO_PS = replace(replace(replace(replace(replace(formatSEO_PS,"(","-"),")","-"),";","-"),":","-"),"+","-")
		formatSEO_PS = replace(formatSEO_PS,"---","-")
		formatSEO_PS = replace(formatSEO_PS,"--","-")
	else
		formatSEO_PS = ""
	end if
	select case formatSEO_PS
		case "sidekick" : formatSEO_PS = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO_PS = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO_PS = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO_PS = "sprint-nextel"
		case "u-s--cellular" : formatSEO_PS = "us-cellular"
		case "antennas-parts" : formatSEO_PS = "antennas"
		case "faceplates" : formatSEO_PS = "faceplates-skins"
		case "verizon" : formatSEO_PS = "verizon-wireless"
		case "unlocked" : formatSEO_PS = "unlocked"
		case "audiovox" : formatSEO_PS = "utstarcom"
		case "data-cable-memory" : formatSEO_PS = "data-cables-memory-cards"
		case "hands-free" : formatSEO_PS = "hands-free-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO_PS = "holsters-holders"
		case "leather-cases" : formatSEO_PS = "cases-and-covers"
		case "full-body-protectors" : formatSEO_PS = "full-body-skins"
	end select
	formatSEO_PS = replace(formatSEO_PS,"---","-")
	formatSEO_PS = replace(formatSEO_PS,"--","-")
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function

function formatSEO_ER(val)
	if not isNull(val) then
		formatSEO_ER = lCase(trim(replace(val,"&trade;","-")))
		formatSEO_ER = replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO_ER," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO_ER = replace(replace(replace(replace(formatSEO_ER,"(","-"),")","-"),";","-"),"+","-")
	else
		formatSEO_ER = ""
	end if
	select case formatSEO_ER
		'case "apple" : formatSEO = "apple-ipad"
	end select
end function

server.scripttimeout = 1000 'seconds
dim Path
Path = server.mappath("tempCSV")

' Create an instance of AspUpload object
set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
Count = Upload.Save(Path)
if Upload.form("upload") <> "" then
	' Capture uploaded file. Return the number of files uploaded
	if Count = 0 then
		response.write "No file selected."
		response.end
	else
		' Obtain File object representing uploaded file
		set File = Upload.Files(1)
		myFile = File.path
		
		uploadDate = upload.form("uploadDate")
		if not isDate(uploadDate) then
			response.write "<h3>The date you entered is not a valid date. Please try again.</h3>"
			response.write "<p><a href=""javascipt:history.back();"">BACK</a></p>"
			response.end
		end if
		
		' Delete all existing XML files in /www/admin/tempCSV
		set fs = CreateObject("Scripting.FileSystemObject")
		set demofolder = fs.GetFolder(Path)
		set filecoll = demofolder.Files
		For Each fil in filecoll
			if UCase(right(fil.name,4)) = ".XML" then
				thisfile = Path & "\" & fil.name
				if myFile <> thisfile then
					fs.DeleteFile thisfile
				end if
			end if
		Next
		
		' Remove ampersands:
		if not fs.FileExists(myFile) then
			response.write("File " & myFile & " does not exist, write aborted.<br>")
			response.end
		else
			set readfile = fs.OpenTextFile(myFile, 1)
			strToWrite = ""
			do while readfile.AtEndOfStream <> true
				strToWrite = strToWrite & readfile.readline & vbCrLf
			loop
			readfile.Close()
			
			set writefile = fs.OpenTextFile(myFile, 2)
			strToWrite = replace(strToWrite,"&","")
			strToWrite = replace(strToWrite,"?","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,chr(160)," ")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"  "," ")
			writefile.write strToWrite
			writefile.Close()
						
			' Update SQL DB with Tracking Numbers (PIC) matched to OrderIDs (ReferenceID):
			dim mynodes, xmlDoc, i
			set xmlDoc = Server.CreateObject("Msxml2.DOMDocument.3.0")
			xmlDoc.async = false
			xmlDoc.load(myFile)
			set mynodes = xmlDoc.documentElement.selectNodes("Record")
			dim a
			a = 0
			for each Node in mynodes
				ReferenceID = ""
				PIC = ""
				for i = 0 to Node.ChildNodes.Length - 1
					if Node.childnodes.item(i).nodename = "PIC" then PIC = Node.childnodes.item(i).text
					if Node.childnodes.item(i).nodename = "ReferenceID" then ReferenceID = Node.childnodes.item(i).text
				next
				if len(ReferenceID) > 5 and trim(PIC) <> "" then
					a = a + 1
					SQL = "UPDATE WE_orders SET TrackingNum='" & trim(PIC) & "', confirmSent='yes', confirmdatetime='" & uploadDate & "' WHERE OrderID='" & ReferenceID & "'"
					session("errorSQL") = SQL
					response.write "<p>" & SQL & "</p>"
					oConn.execute SQL
					
					sOrderID = ReferenceID
					
					SQL = "SELECT orderID, store, accountID, shippingid, extOrderType, ordersubtotal, ordershippingfee, shiptype, orderTax, ordergrandtotal FROM we_Orders WHERE orderID = '" & sOrderID & "'"
					response.Write(sql & "<br>")
					set RS3 = server.CreateObject("ADODB.Recordset")
					RS3.open SQL, oConn, 3, 3
					do until RS3.eof
						nOrderID = RS3("orderID")
						extOrderType = RS3("extOrderType")
						storeID = RS3("store")
						nOrderGrandTotal = RS3("ordergrandtotal")
						nOrderSubTotal = RS3("ordersubtotal")
						nOrderTax = RS3("orderTax")
						nShipFee = RS3("ordershippingfee")
						SQL = "SELECT B.PromoCode FROM we_orders A INNER JOIN we_coupons B ON A.couponid=B.couponid WHERE A.orderid='" & nOrderID & "'"
						set RS2 = Server.CreateObject("ADODB.Recordset")
						RS2.open SQL, oConn, 3, 3
						if not RS2.eof then
							sPromoCode = RS2("PromoCode")
						else
							sPromoCode = ""
						end if
						select case storeID
							case 0
								storeAbbr = "WE_"
								storeName = "WirelessEmporium"
								imgLink = "http://www.wirelessemporium.com/images/global/we_logo_solo.gif"
								sHeading = "Order #" & nOrderID & " Successfully Shipped from WirelessEmporium.com"
								'statusLink = "http://www.wirelessemporium.com/orderstatus.asp"
								regText = "font-family: Arial; font-size: 9pt;"
								headerText = "font-family: Arial; font-size: 11pt;"
							case 1
								storeAbbr = "CA_"
								storeName = "CellphoneAccents"
								imgLink = "http://www.cellphoneaccents.com/images/logo.jpg"
								sHeading = "Your CellphoneAccents.com order #" & nOrderID & " is on the way!"
								'statusLink = "http://www.cellphoneaccents.com/track-your-order.asp"
								regText = "font-family: Tahoma; font-size: 9pt;"
								headerText = "font-family: Tahoma; font-size: 11pt;"
							case 2
								storeAbbr = "CO_"
								storeName = "CellularOutfitter"
								imgLink = "http://www.cellularoutfitter.com/images/CellularOutfitter_STANDARD.jpg"
								sHeading = "CellularOutfitter.com Order #" & nOrderID & " - Shipment Status"
								'statusLink = "http://www.cellularoutfitter.com/track-your-order.asp"
								regText = "font-family: Times; font-size: 12pt;"
								headerText = "font-family: Times; font-size: 14pt;"
							case 3
								storeAbbr = "PS_"
								storeName = "Phonesale"
								imgLink = "http://www.phonesale.com/images/phonesale.jpg"
								sHeading = "Phonesale.com Order #" & nOrderID & " - Shipment Status"
								'statusLink = "http://www.phonesale.com/track-your-order.asp"
								regText = "font-family: Times; font-size: 12pt;"
								headerText = "font-family: Times; font-size: 14pt;"
						end select
						
						statusLink = "http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?orig&strOrigTrackNum=" & trim(PIC) & "&CAMEFROM=OK"
						
						if isNull(RS3("shippingid")) or RS3("shippingid") = 0 then
							SQL = "SELECT fname, lname, sAddress1, sAddress2, sCity, sState, sZip, sCountry, email, phone FROM " & storeAbbr & "Accounts WHERE accountID = '" & RS3("accountID") & "'"
						else
							SQL = "SELECT A.*, B.fname, B.lname, B.email FROM we_addl_shipping_addr A INNER JOIN " & storeAbbr & "Accounts B ON A.accountID=B.accountID WHERE A.accountID = '" & RS3("accountID") & "'"
						end if
						dim fname, lname, sAddress1, sAddress2, sCity, sState, sZip
						session("errorSQL") = sql
						set RS2 = server.CreateObject("ADODB.Recordset")
						RS2.open SQL, oConn, 3, 3
						if not RS2.eof then
							fname = RS2("fname")
							lname = RS2("lname")
							sAddress1 = RS2("sAddress1")
							sAddress2 = RS2("sAddress2")
							sCity = RS2("sCity")
							sState = RS2("sState")
							sZip = RS2("sZip")
						else
							SQL = "SELECT fname, lname, sAddress1, sAddress2, sCity, sState, sZip, sCountry, email, phone FROM " & storeAbbr & "Accounts WHERE accountID = '" & RS3("accountID") & "'"
							session("errorSQL") = sql
							set RS2 = server.CreateObject("ADODB.Recordset")
							RS2.open SQL, oConn, 3, 3
							
							fname = RS2("fname")
							lname = RS2("lname")
							sAddress1 = RS2("sAddress1")
							sAddress2 = RS2("sAddress2")
							sCity = RS2("sCity")
							sState = RS2("sState")
							sZip = RS2("sZip")
						end if
						sShipAddress = fFormatAddress(sAddress1,sAddress2,sCity,sState,sZip)
						cdo_from = storeName & "<sales@" & Lcase(storeName) & ".com>"
						cdo_to = "" & RS2("email")
						'cdo_to = cdo_to & ",webmaster@" & storeName & ".com"
						cdo_subject = sHeading
						RS2.close
						set RS2 = nothing
						cdo_body = "<html>" & vbcrlf
						cdo_body = cdo_body & "<head>" & vbcrlf
						cdo_body = cdo_body & "<style type='text/css'>" & vbcrlf
						cdo_body = cdo_body & "<!--" & vbcrlf
						cdo_body = cdo_body & ".regText {" & regText & "}" & vbcrlf
						cdo_body = cdo_body & ".headerText {" & headerText & "}" & vbcrlf
						cdo_body = cdo_body & "-->" & vbcrlf
						cdo_body = cdo_body & "</style>" & vbcrlf
						cdo_body = cdo_body & "</head>" & vbcrlf
						cdo_body = cdo_body & "<body class='regText'>"
						cdo_body = cdo_body & "<table width='100%' cellpadding='8' cellspacing='0' align='center' bgcolor='#eaeaea' class='regText'><tr>" & vbcrlf
						cdo_body = cdo_body & "<td class='headerText' align='left' valign='top'><img src='" & imgLink & "' border='0' hspace='12' align='absmiddle'></td>" & vbcrlf
						cdo_body = cdo_body & "<td class='headerText' align='center' valign='middle'><b>" & sHeading & "</b></td>" & vbcrlf
						cdo_body = cdo_body & "</tr></table>" & vbcrlf
						cdo_body = cdo_body & "<table width='95%' cellpadding='5' cellspacing='1' align='center' class='regText'>" & vbcrlf
						cdo_body = cdo_body & "<tr><td colspan='2'><br><p><b>Hello " & Ucase(left(fname, 1)) & right(fname,(len(fname)-1)) & ":</b></p>"
						cdo_body = cdo_body & "<p>Thank you for ordering from " & storeName & ".com! We would like to notify you that your order has been SHIPPED to:</p>"& vbcrlf
						cdo_body = cdo_body & "</td></tr>" & vbcrlf
						cdo_body = cdo_body & "<tr><td colspan='2'><b>" & sShipAddress & "</b><br></tr></td>"
						cdo_body = cdo_body & "</table>" & vbcrlf
						cdo_body = cdo_body & "<table width='95%' cellpadding='5' cellspacing='1' align='center' bgcolor='#eaeaea' class='regText'>" & vbcrlf
						cdo_body = cdo_body & "<tr><td class='regText'><b>ORDER DETAILS:</b></td></tr>" & vbcrlf
						cdo_body = cdo_body & "</table>" & vbcrlf
						cdo_body = cdo_body & "<table width='95%' cellpadding='5' cellspacing='1' align='center' class='regText'>" & vbcrlf
						cdo_body = cdo_body & fFormatEmailBodyRow("ORDER ID:",nOrderID)
						'get orderdetails
						SQL = "SELECT A.itemid, A.typeid, A.carrierid, A.itemdesc, A.itemDesc_CO, A.itemDesc_PS, A.price_our, A.price_CA, A.price_CO, A.price_PS,"
						SQL = SQL & " B.quantity, C.typename FROM we_items A"
						SQL = SQL & " INNER JOIN we_orderdetails B ON A.itemid=B.itemid"
						SQL = SQL & " INNER JOIN we_types C ON A.typeid=C.typeid"
						SQL = SQL & " WHERE B.orderid = '" & nOrderID & "'"
						cdo_body = cdo_body & "<tr class='regText'><td valign='top' bgcolor='#eaeaea'><b>ITEMS:</b></td><td>"
						set RSOrderDetails = Server.CreateObject("ADODB.Recordset")
						RSOrderDetails.open SQL, oConn, 3, 3
						
						if not RSOrderDetails.eof then
							nCount = 1
							do until RSOrderDetails.eof
								nID = RSOrderDetails("itemid")
								nQty = RSOrderDetails("quantity")
								select case storeID
									case 0
										nPrice = RSOrderDetails("price_our")
										cdo_body = cdo_body & "<b>" & nCount & ": </b><a href='http://www.wirelessemporium.com/p-" & nID & "-" & formatSEO(RSOrderDetails("itemDesc")) & ".asp'>" & RSOrderDetails("itemdesc") & "</a>"
									case 1
										nPrice = RSOrderDetails("price_CA")
										cdo_body = cdo_body & "<b>" & nCount & ": </b><a href='http://www.cellphoneaccents.com/p-" & nID & "-" & formatSEO_CA(RSOrderDetails("itemDesc")) & ".html'>" & RSOrderDetails("itemdesc") & "</a>"
									case 2
										nPrice = RSOrderDetails("price_CO")
										cdo_body = cdo_body & "<b>" & nCount & ": </b><a href='http://www.cellularoutfitter.com/p-" & nID & "-" & formatSEO_CO(RSOrderDetails("itemDesc_CO")) & ".html'>" & RSOrderDetails("itemDesc_CO") & "</a>"
									case 3
										nPrice = RSOrderDetails("price_PS")
										dim thetype, ntc, carriername, rsRecent2
										if RSOrderDetails("typeid") = 16 then
											thetype = "cell-phones"
											ntc = 16
										else
											thetype = formatSEO_PS(RSOrderDetails("typename"))
											ntc = 17
										end if
										if isnull(RSOrderDetails("carrierid")) or RSOrderDetails("carrierid") = "" or RSOrderDetails("carrierid") = 0 then
											carriername = "universal" & "-" & thetype
										else
											SQL = "SELECT carriername FROM we_Carriers WHERE id='" & RSOrderDetails("carrierid") & "'"
											set rsRecent2 = Server.CreateObject("ADODB.Recordset")
											rsRecent2.open SQL, oConn, 3, 3
											if rsRecent2.eof then
												carriername = "universal" & "-" & thetype
											else
												carriername = formatSEO_PS(rsRecent2("carriername")) & "-" & thetype
											end if
											rsRecent2.close
											set rsRecent2 = nothing
										end if
										cdo_body = cdo_body & "<b>" & nCount & ": </b><a href='http://www.phonesale.com/" & carriername & "/p-" & nID & "-tc-" & ntc & "-" & formatSEO_PS(RSOrderDetails("itemDesc_PS")) & ".html'>" & RSOrderDetails("itemDesc_PS") & "</a>"
									case 10
										nPrice = RSOrderDetails("price_ER")
										cdo_body = cdo_body & "<b>" & nCount & ": </b><a href='http://www.ereadersupply.com/p-" & nID & "-" & formatSEO_ER(RSOrderDetails("itemDesc")) & ".html'>" & RSOrderDetails("itemDesc") & "</a>"
								end select
								cdo_body = cdo_body & "<br>&nbsp;&nbsp;&nbsp;Quantity: " & nQty & " | Price: " & FormatCurrency(nPrice,2)
								cdo_body = cdo_body & "<br>&nbsp;&nbsp;&nbsp;Subtotal: " & FormatCurrency(CDbl(nQty) * CDbl(nPrice),2) & "<hr size='1' color='#eaeaea' noshade>"
								nCount = nCount + 1
								RSOrderDetails.movenext
							loop
						end if
						RSOrderDetails.close
						set RSOrderDetails = nothing
						cdo_body = cdo_body & "</td></tr>" & vbcrlf
						cdo_body = cdo_body & fFormatEmailBodyRow("ORDER SUBTOTAL:",FormatCurrency(nOrderSubTotal,2))
						if sPromoCode <> "" then cdo_body = cdo_body & fFormatEmailBodyRow("PROMO CODE:",sPromoCode)
						if nOrderGrandTotal - nOrderSubTotal - nOrderTax - nShipFee < 0 then cdo_body = cdo_body & fFormatEmailBodyRow("DISCOUNT:",FormatCurrency(nOrderGrandTotal - nOrderSubTotal - nOrderTax - nShipFee,2))
						dim sTrackingURL
						sTrackingURL = "<a href='http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?orig&strOrigTrackNum=" & trim(PIC) & "&CAMEFROM=OK'>Click Here</a>"
						cdo_body = cdo_body & fFormatEmailBodyRow("CA RESIDENT TAX (7.75%):",FormatCurrency(nOrderTax,2))
						cdo_body = cdo_body & fFormatEmailBodyRow("SHIPPING TYPE:",RS3("shiptype"))
						cdo_body = cdo_body & fFormatEmailBodyRow("SHIPMENT TRACKING:",sTrackingURL)
						cdo_body = cdo_body & fFormatEmailBodyRow("SHIPPING FEE:",FormatCurrency(nShipFee,2))
						cdo_body = cdo_body & "<tr><td bgcolor='#EAEAEA' class='headerText'><b>GRAND TOTAL: </b></td>"
						cdo_body = cdo_body & "<td class='headerText'>" & FormatCurrency(nOrderGrandTotal,2)
						cdo_body = cdo_body & "</td></tr></table>"
						cdo_body = cdo_body & "</td></tr>"
						cdo_body = cdo_body & "</table><br>" & vbcrlf
						cdo_body = cdo_body & "<table width='95%'><tr><td class='regText'>"
						cdo_body = cdo_body & "<p>Your order will be shipped in accordance with your selected delivery option. Arrival time for your order will vary depending on your selected delivery option, or your proximity to Southern California (for First Class Mail).</p>" & vbcrlf
						cdo_body = cdo_body & "<p>Thanks again for your business. Should you have any additional questions regarding your order, our site and/or services, please feel free to contact us at <a href='mailto:sales@" & Lcase(storeName) & ".com'>sales@" & Lcase(storeName) & ".com</a>. We are eager to serve your wireless needs again in the future.</p>"
						cdo_body = cdo_body & "</td></tr></table>"
						cdo_body = cdo_body & "<table width='100%' cellpadding='5' cellspacing='5' align='center' bgcolor='#eaeaea'><tr><td colspan='2' class='regText'><a href='" & statusLink & "'><b>Check Your Order Status Online</b></a></td></tr>" & vbcrlf
						cdo_body = cdo_body & "<tr><td colspan='2' class='regText'><b>Please visit us again soon!</b> - <a href='http://www." & Lcase(storeName) & ".com/'>" & storeName & ".com</a></td></tr></table>" & vbcrlf
						cdo_body = cdo_body & "</body></html>"
						response.Write("email prepaired (" & extOrderType & "," & redo & ")<br />")
						if isnull(extOrderType) or len(extOrderType) < 1 then extOrderType = 0
						if extOrderType <> 6 and redo <> "1" then
							CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
							response.Write("email sent (" & cdo_to & ")<br />")
						end if
						RS3.moveNext
					loop
				end if
			next
			response.write "<h3>" & a & " records updated!</h3>"
		end if
	end if
else
	%>
	<h3>Select a File to Upload:</h3>
	<form enctype="multipart/form-data" action="uploadTrackingNums.asp" method="POST">
		<p><input type="FILE" name="FILE1"></p>
		<p><input type="text" name="uploadDate" value="<%=date()%>">&nbsp;Date&nbsp;Confirmation&nbsp;Sent</p>
		<p><input type="SUBMIT" name="upload" value="Upload"></p>
	</form>
	<%
end if
%>

</blockquote>
</blockquote>
</font>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
