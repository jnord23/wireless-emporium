<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
strView = request.QueryString("cbView")
strView2 = prepInt(request.QueryString("cbView2"))
strOrderID = request.QueryString("txtOrderID")

strSDate = request.QueryString("txtSDate")
strEDate = request.QueryString("txtEDate")
if strSDate = "" or not isdate(strSDate) then strSDate = Date-7
if strEDate = "" or not isdate(strEDate) then strEDate = Date

sqlWhere = 	"where 	b.approved = 1 and (b.cancelled = 0 or b.cancelled is null) " & vbcrlf & _
			"	and a.entryDate >= '" & strSDate & "' and a.entryDate < '" & dateadd("d",1,strEDate) & "'" & vbcrlf

if strOrderID <> "" then
	strOrderID = trim(replace(replace(strOrderID, chr(10), ""), chr(9), ""))
	sqlWhere = sqlWhere & vbcrlf & "	and a.orderid = '" & ds(strOrderID) & "'"
end if

if strView = "" then strView = "3" end if

if strView = "2" then
	sqlWhere = sqlWhere & vbcrlf & 	"	and a.childOrderID is not null and a.cancelDate is null" & vbcrlf  & _
									"	and	(processDate > dateadd(dd, -30, getdate()) or processDate is null)" & vbcrlf 
elseif strView = "3" then
	sqlWhere = sqlWhere & vbcrlf & 	"	and a.childOrderID is null and a.cancelDate is null and	(processDate > dateadd(dd, -30, getdate()) or processDate is null)"
elseif strView = "4" then
	sqlWhere = sqlWhere & vbcrlf & 	"	and a.cancelDate is not null"
else
	sqlWhere = sqlWhere & vbcrlf & 	"	and	(processDate > dateadd(dd, -30, getdate()) or processDate is null)"
end if

if strView2 = 0 then
	sqlWhere = sqlWhere & vbcrlf & 	"	and	c.inv_qty > 0"
end if

if strVIew = "3" then
	sqlOrder = "order by inv_qty desc, entryDate desc, orderid, partnumber"
else
	sqlOrder = "order by entryDate desc, orderid, partnumber"
end if

sql = 	"select	distinct a.orderid, convert(varchar(16), a.entrydate, 20) entryDate, isnull(s.fname + ' ' + s.lname, 'N/A') username, v.accountid, convert(varchar(16), b.scandate, 20) scandate" & vbcrlf & _
		"	, 	x.shortDesc, x.color, a.partnumber, a.itemid, isnull(a.qty, 0) qty" & vbcrlf & _
		"	,	lower(v.email) email, v.phone, isnull(c.itemdesc, 'N/A') itemdesc, isnull(c.inv_qty, 0) inv_qty" & vbcrlf & _
		"	,	convert(varchar(10), a.processDate, 20) processDate, a.childOrderID, b2.parentOrderID" & vbcrlf & _
		"	, 	isnull(convert(varchar(2000), n.ordernotes), '') ordernotes, isnull(convert(varchar(2000), n2.notes), '') productnotes, convert(varchar(10), a.cancelDate, 20) cancelDate" & vbcrlf & _
		"	,	isnull(isnull(nullif(c.vendor, ''), vn.vendor), '') vendor, isnull(vn.partNumber, '') vn_partnumber" & vbcrlf & _
		"from	we_rma a join we_orders b" & vbcrlf & _
		"	on	a.orderid = b.orderid left outer join we_orders b2" & vbcrlf & _
		"	on	a.orderid = b2.parentOrderID join v_accounts v" & vbcrlf & _
		"	on	b.accountid = v.accountid and b.store = v.site_id join xstore x" & vbcrlf & _
		"	on	b.store = x.site_id left outer join we_items c" & vbcrlf & _
		"	on	a.partnumber = c.partnumber and c.master = 1 left outer join we_adminusers s" & vbcrlf & _
		"	on	a.adminid = s.adminid left outer join we_ordernotes n" & vbcrlf & _
		"	on	a.orderid = n.orderid left outer join we_partNumberNotes n2" & vbcrlf & _
		"	on	a.partnumber = n2.partnumber left outer join we_vendorNumbers vn" & vbcrlf & _
		"	on	a.partnumber = vn.we_partnumber" & vbcrlf & _
		sqlWhere & vbcrlf & _
		sqlOrder
		
sql	=	"select	distinct a.orderid, convert(varchar(16), a.entrydate, 20) entryDate, isnull(s.fname + ' ' + s.lname, 'N/A') username, v.accountid, convert(varchar(16), b.scandate, 20) scandate" & vbcrlf & _
		"	, 	x.shortDesc, x.color, a.partnumber, a.itemid, isnull(a.qty, 0) qty" & vbcrlf & _
		"	,	lower(v.email) email, v.phone, isnull(c.itemdesc, 'N/A') itemdesc, isnull(c.inv_qty, 0) inv_qty" & vbcrlf & _
		"	,	convert(varchar(10), a.processDate, 20) processDate, a.childOrderID, b2.parentOrderID" & vbcrlf & _
		"	, 	isnull(convert(varchar(2000), n.ordernotes), '') ordernotes, isnull(convert(varchar(2000), n2.notes), '') productnotes, convert(varchar(10), a.cancelDate, 20) cancelDate" & vbcrlf & _
		"	,	isnull(substring(	(	select	(', ' + i.vendor)" & vbcrlf & _
		"								from	we_vendorNumbers i" & vbcrlf & _
		"								where	i.we_partnumber = a.partnumber" & vbcrlf & _
		"								for xml path('')" & vbcrlf & _
		"							), 3, 1000), c.vendor) vendor" & vbcrlf & _
		"	,	isnull(substring(	(	select	(', ' + i.partnumber)" & vbcrlf & _
		"								from	we_vendorNumbers i" & vbcrlf & _
		"								where	i.we_partnumber = a.partnumber" & vbcrlf & _
		"								for xml path('')" & vbcrlf & _
		"							), 3, 1000), '') vn_partnumber" & vbcrlf & _
		"	,	(	select	top 1 processOrderDate" & vbcrlf & _
		"			from	we_vendorOrder with (nolock)" & vbcrlf & _
		"			where	partnumber = a.partnumber" & vbcrlf & _
		"				and	receivedAmt > 0" & vbcrlf & _
		"			order by processOrderDate desc	" & vbcrlf & _
		"		) vn_processOrderDate" & vbcrlf & _
		"from	we_rma a join we_orders b" & vbcrlf & _
		"	on	a.orderid = b.orderid left outer join we_orders b2" & vbcrlf & _
		"	on	a.orderid = b2.parentOrderID join v_accounts v" & vbcrlf & _
		"	on	b.accountid = v.accountid and b.store = v.site_id join xstore x" & vbcrlf & _
		"	on	b.store = x.site_id left outer join we_items c" & vbcrlf & _
		"	on	a.partnumber = c.partnumber and c.master = 1 left outer join we_adminusers s" & vbcrlf & _
		"	on	a.adminid = s.adminid left outer join we_ordernotes n" & vbcrlf & _
		"	on	a.orderid = n.orderid left outer join we_partNumberNotes n2" & vbcrlf & _
		"	on	a.partnumber = n2.partnumber " & vbcrlf & _
		sqlWhere & vbcrlf & _
		sqlOrder		
set rs = oConn.execute(sql)
filename	=	"BackOrder_" & Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now) & ".xls"

Response.ContentType = "application/vnd.ms-excel"
'Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
Response.AddHeader "Content-Disposition","attachment;filename="""&filename&""""
'Response.Charset = "" 
'Me.EnableViewState = False


response.write "SITE" & vbTab
response.write "Order ID" & vbTab
response.write "Original Scan Date" & vbTab
response.write "BackOrder Entry Date" & vbTab
response.write "Part Number" & vbTab
response.write "Vendors Vendor PN" & vbTab
response.write "Item ID" & vbTab
response.write "Order QTY" & vbTab
response.write "Item Desc" & vbTab
response.write "QTY on Hand" & vbTab
response.write "Process" & vbTab
response.write "Cancel" & vbNewLine

if not rs.eof then
	nRow = 0
	do until rs.eof
		nRow = nRow + 1
		orderid = rs("orderid")
		entryDate = rs("entryDate")
		username = rs("username")
		accountid = rs("accountid")
		scandate = rs("scandate")
		siteName = rs("shortDesc")
		siteColor = rs("color")
		partnumber = rs("partnumber")
		itemid = rs("itemid")
		orderQty = rs("qty")
		itemdesc = rs("itemdesc")
		inv_qty = rs("inv_qty")
		processDate = rs("processDate")
		parentOrderID = rs("parentOrderID")
		ordernotes = rs("ordernotes")
		productnotes = rs("productnotes")
		cancelDate = rs("cancelDate")
		vendor = rs("vendor")
		vn_partnumber = rs("vn_partnumber")
		
		response.write siteName & vbTab
		response.write orderid & vbTab
		response.write scandate & vbTab
		response.write entryDate & " By " & username & vbTab
		response.write partnumber & vbTab
		response.write vendor & " / " & vn_partnumber & vbTab
		response.write itemid & vbTab
		response.write formatnumber(orderQty, 0) & vbTab
		response.write itemdesc & vbTab

		if inv_qty > 0 then 
			response.write formatnumber(inv_qty, 0) & vbTab
		else
			response.write "-" & vbTab
		end if

		if inv_qty > 0 and isnull(processDate) and isnull(cancelDate) then
			response.write vbTab
		else
			if not isnull(processDate) then
				response.write "Processed on " & processDate & vbTab
			else
				response.write vbTab
			end if
		end if

		if not isnull(cancelDate) then
			response.write "Cancelled on " & cancelDate & vbTab
		else
			response.write vbTab
		end if

		response.write vbNewLine
		
		if (nRow mod 1000) = 0 then
			response.flush
		end if
		rs.movenext
	loop
else
	response.write "No data to display"
end if
%>