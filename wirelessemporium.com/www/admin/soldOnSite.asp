<%
	thisUser = Request.Cookies("username")
	pageTitle = ""
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	useDate = date-7
	
	sql = "select e.brandName, c.itemID, c.itemDesc, d.typeName, b.quantity, c.price_PS, c.cogs from we_orders a left join we_orderdetails b on a.orderid = b.orderid left join we_Items c on b.itemid = c.itemID left join we_Types d on c.typeID = d.typeID left join we_Brands e on c.brandID = e.brandID where a.store = 3 and a.orderdatetime > '" & useDate & "' and a.approved = 1 and a.cancelled is null order by e.brandName, d.typeName, c.itemID"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	unlocked = 0
	unlockedValue = 0
	unlockRefurb = 0
	unlockRefurbValue = 0
	prepaid = 0
	prepaidValue = 0
	preowned = 0
	preownedValue = 0
	refurbished = 0
	refurbishedValue = 0
	newPhone = 0
	newPhoneValue = 0
	totalPhoneSales = 0
	totalPhoneSalesValue = 0
	totalAccessorySales = 0
	totalAccessorySalesValue = 0
	do while not rs.EOF
		brandName = rs("brandName")
		itemID = rs("itemID")
		itemDesc = rs("itemDesc")
		categoryName = rs("typeName")
		quantity = rs("quantity")
		thisValue = (prepInt(rs("price_PS")) - prepInt(rs("cogs"))) * quantity
		
		if categoryName = "Cell Phones" then
			totalPhoneSales = totalPhoneSales + quantity
			totalPhoneSalesValue = totalPhoneSalesValue + thisValue
			
			if instr(lcase(itemDesc),"unlocked") > 0 then
				if instr(lcase(itemDesc),"refurb") > 0 then
					unlockRefurb = unlockRefurb + quantity
					unlockRefurbValue = unlockRefurbValue + thisValue
				else
					unlocked = unlocked + quantity
					unlockedValue = unlockedValue + thisValue
				end if
			elseif instr(lcase(itemDesc),"prepaid") > 0 or instr(lcase(itemDesc),"pre-paid") > 0 then
				prepaid = prepaid + quantity
				prepaidValue = prepaidValue + thisValue
			elseif instr(lcase(itemDesc),"preowned") > 0 or instr(lcase(itemDesc),"pre-owned") > 0 then
				preowned = preowned + quantity
				preownedValue = preownedValue + thisValue
			elseif instr(lcase(itemDesc),"refurb") > 0 then
				refurbished = refurbished + quantity
				refurbishedValue = refurbishedValue + thisValue
			else
				newPhone = newPhone + quantity
				newPhoneValue = newPhoneValue + thisValue
			end if
		else
			totalAccessorySales = totalAccessorySales + quantity
			totalAccessorySalesValue = totalAccessorySalesValue + thisValue
		end if
		
		rs.movenext
	loop
%>
<table border="0" cellpadding="3" cellspacing="0" width="600" align="center">
    <tr><td align="center" style="font-weight:bold; font-size:18px;">Past 7 Days Sales on PhoneSale.com</td></tr>
    <tr><td align="center" style="font-weight:bold;">Starting on <%=useDate%></td></tr>
    <tr>
    	<td align="center">
        	<div style="width:400px; height:30px; border-bottom:1px solid #999; color:#fff; background-color:#000; font-weight:bold;">
            	<div style="float:left; width:277px; padding-top:6px; text-align:left; padding-left:3px;">Category</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;">Sales</div>
                <div style="float:left; text-align:right; width:67px; padding-top:6px; padding-right:3px;">Profit</div>
            </div>
            <div style="width:400px; height:30px; border-bottom:1px solid #999; color:#900;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">Total Phone Sales:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=totalPhoneSales%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(totalPhoneSalesValue,2)%></div>
            </div>
            <div style="width:400px; height:30px; border-bottom:1px solid #999; color:#900;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">Total Accessory Sales:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=totalAccessorySales%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(totalAccessorySalesValue,2)%></div>
            </div>
        	<div style="width:400px; height:30px; border-bottom:1px solid #999;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">Unlocked:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=unlocked%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(unlockedValue,2)%></div>
            </div>
            <div style="width:400px; height:30px; border-bottom:1px solid #999;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">Unlocked Refurbished:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=unlockRefurb%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(unlockRefurbValue,2)%></div>
            </div>
            <div style="width:400px; height:30px; border-bottom:1px solid #999;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">Pre-Paid:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=prepaid%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(prepaidValue,2)%></div>
            </div>
            <div style="width:400px; height:30px; border-bottom:1px solid #999;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">Pre-Owned:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=preowned%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(preownedValue,2)%></div>
            </div>
            <div style="width:400px; height:30px; border-bottom:1px solid #999;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">Refurbished:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=refurbished%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(refurbishedValue,2)%></div>
            </div>
            <div style="width:400px; height:30px; border-bottom:1px solid #999;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">New Phones:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=newPhone%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(newPhoneValue,2)%></div>
            </div>
        </td>
    </tr>
</table>
<%
	useDate = date-14
	useEndDate = date-7
	
	sql = "select e.brandName, c.itemID, c.itemDesc, d.typeName, b.quantity, c.price_PS, c.cogs from we_orders a left join we_orderdetails b on a.orderid = b.orderid left join we_Items c on b.itemid = c.itemID left join we_Types d on c.typeID = d.typeID left join we_Brands e on c.brandID = e.brandID where a.store = 3 and a.orderdatetime > '" & useDate & "' and a.orderdatetime < '" & useEndDate & "' and a.approved = 1 and a.cancelled is null order by e.brandName, d.typeName, c.itemID"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	unlocked = 0
	unlockedValue = 0
	unlockRefurb = 0
	unlockRefurbValue = 0
	prepaid = 0
	prepaidValue = 0
	preowned = 0
	preownedValue = 0
	refurbished = 0
	refurbishedValue = 0
	newPhone = 0
	newPhoneValue = 0
	totalPhoneSales = 0
	totalPhoneSalesValue = 0
	totalAccessorySales = 0
	totalAccessorySalesValue = 0
	do while not rs.EOF
		brandName = rs("brandName")
		itemID = rs("itemID")
		itemDesc = rs("itemDesc")
		categoryName = rs("typeName")
		quantity = rs("quantity")
		thisValue = (prepInt(rs("price_PS")) - prepInt(rs("cogs"))) * quantity
		
		if categoryName = "Cell Phones" then
			totalPhoneSales = totalPhoneSales + quantity
			totalPhoneSalesValue = totalPhoneSalesValue + thisValue
			
			if instr(lcase(itemDesc),"unlocked") > 0 then
				if instr(lcase(itemDesc),"refurb") > 0 then
					unlockRefurb = unlockRefurb + quantity
					unlockRefurbValue = unlockRefurbValue + thisValue
				else
					unlocked = unlocked + quantity
					unlockedValue = unlockedValue + thisValue
				end if
			elseif instr(lcase(itemDesc),"prepaid") > 0 or instr(lcase(itemDesc),"pre-paid") > 0 then
				prepaid = prepaid + quantity
				prepaidValue = prepaidValue + thisValue
			elseif instr(lcase(itemDesc),"preowned") > 0 or instr(lcase(itemDesc),"pre-owned") > 0 then
				preowned = preowned + quantity
				preownedValue = preownedValue + thisValue
			elseif instr(lcase(itemDesc),"refurb") > 0 then
				refurbished = refurbished + quantity
				refurbishedValue = refurbishedValue + thisValue
			else
				newPhone = newPhone + quantity
				newPhoneValue = newPhoneValue + thisValue
			end if
		else
			totalAccessorySales = totalAccessorySales + quantity
			totalAccessorySalesValue = totalAccessorySalesValue + thisValue
		end if
		
		rs.movenext
	loop
%>
<table border="0" cellpadding="3" cellspacing="0" width="600" align="center">
    <tr><td align="center" style="font-weight:bold; font-size:18px;">Previous Week Sales on PhoneSale.com</td></tr>
    <tr><td align="center" style="font-weight:bold;">Starting on <%=useDate%> / Ending on <%=useEndDate-1%></td></tr>
    <tr>
    	<td align="center">
        	<div style="width:400px; height:30px; border-bottom:1px solid #999; color:#fff; background-color:#000; font-weight:bold;">
            	<div style="float:left; width:277px; padding-top:6px; text-align:left; padding-left:3px;">Category</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;">Sales</div>
                <div style="float:left; text-align:right; width:67px; padding-top:6px; padding-right:3px;">Profit</div>
            </div>
            <div style="width:400px; height:30px; border-bottom:1px solid #999; color:#900;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">Total Phone Sales:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=totalPhoneSales%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(totalPhoneSalesValue,2)%></div>
            </div>
            <div style="width:400px; height:30px; border-bottom:1px solid #999; color:#900;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">Total Accessory Sales:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=totalAccessorySales%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(totalAccessorySalesValue,2)%></div>
            </div>
        	<div style="width:400px; height:30px; border-bottom:1px solid #999;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">Unlocked:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=unlocked%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(unlockedValue,2)%></div>
            </div>
            <div style="width:400px; height:30px; border-bottom:1px solid #999;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">Unlocked Refurbished:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=unlockRefurb%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(unlockRefurbValue,2)%></div>
            </div>
            <div style="width:400px; height:30px; border-bottom:1px solid #999;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">Pre-Paid:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=prepaid%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(prepaidValue,2)%></div>
            </div>
            <div style="width:400px; height:30px; border-bottom:1px solid #999;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">Pre-Owned:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=preowned%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(preownedValue,2)%></div>
            </div>
            <div style="width:400px; height:30px; border-bottom:1px solid #999;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">Refurbished:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=refurbished%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(refurbishedValue,2)%></div>
            </div>
            <div style="width:400px; height:30px; border-bottom:1px solid #999;">
            	<div style="float:left; width:280px; padding-top:6px; text-align:left;">New Phones:</div>
                <div style="float:left; text-align:right; width:50px; padding-top:6px;"><%=newPhone%></div>
                <div style="float:left; text-align:right; width:70px; padding-top:6px;"><%=formatCurrency(newPhoneValue,2)%></div>
            </div>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->


