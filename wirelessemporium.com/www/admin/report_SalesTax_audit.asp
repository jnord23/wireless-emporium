<%
response.buffer = false
pageTitle = "Sales Tax Audit report TXT for WirelessEmporium.com"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<%
Server.ScriptTimeout = 2400 'seconds

dim fs, file, filename, path
filename = "Sales_Tax_Audit_report.txt"
path = server.mappath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Sales Tax Audit report TXT for WirelessEmporium.com</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							response.write "<b>CreateFile:</b><br>"
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='/tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
	
<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		response.write "Deleting " & filename & ".<br>"
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	dim RS, SQL, strline
	expDate = dateAdd("d",30,date)
	response.write "Creating " & filename & ".<br>"
	set file = fs.CreateTextFile(path)
	SQL = "SELECT A.orderid, A.accountid, A.ordersubtotal, A.ordershippingfee, A.orderTax, A.ordergrandtotal, A.orderdatetime, A.refundAmount, B.sCity, B.sState, B.sZip"
	SQL = SQL & " FROM we_Orders A INNER JOIN CO_Accounts B ON A.accountID=B.accountID"
	'SQL = "SELECT A.orderid, A.accountid, A.ordersubtotal, A.ordershippingfee, A.orderTax, A.ordergrandtotal, A.orderdatetime, A.refundAmount, C.sCity, C.sState, C.sZip"
	'SQL = SQL & " FROM we_Orders A INNER JOIN WE_Accounts B ON A.accountID=B.accountID INNER JOIN we_addl_shipping_addr C ON A.shippingID=C.id"
	'SQL = "SELECT A.orderid, A.accountid, A.ordersubtotal, A.ordershippingfee, A.orderTax, A.ordergrandtotal, A.orderdatetime, A.refundAmount, B.sCity, B.sState, B.sZip"
	'SQL = SQL & " FROM we_Orders A INNER JOIN WE_Accounts B ON A.accountID=B.accountID"
	'SQL = SQL & " WHERE (A.shippingID IS NULL OR A.shippingID = 0)"
	
	SQL = SQL & " WHERE A.orderdatetime >= '1/1/2006' AND A.orderdatetime <= '12/31/2008'"
	SQL = SQL & " AND A.approved = 1 AND (A.cancelled IS NULL OR A.cancelled = 0)"
	'SQL = SQL & " AND B.sState = 'CA'"
	SQL = SQL & " AND A.store = 2"
	SQL = SQL & " AND B.email <> 'buyer_1228418024_per@cellularoutfitter.com' AND B.email <> 'ebay@wirelessemporium.com' AND B.email <> 'michael@wirelessemporium.com' AND B.email <> 'eugene@cellularoutfitter.com'"
	SQL = SQL & " AND B.email <> 'webmaster@cellularoutfitter.com' AND B.email <> 'webmaster@wirelessemporium.com' AND B.email <> 'robert@wirelessemporium.com' AND B.email <> 'ruben@wirelessemporium.com'"
	SQL = SQL & " AND B.email <> 'artistry@yahoo.com' AND B.email <> 'sang777@gmail.com' AND B.email <> 'sang77@comcast.net'"
	'SQL = SQL & " AND (A.refundAmount IS NULL OR A.refundAmount = 0)"
	SQL = SQL & " ORDER BY A.orderdatetime"
	response.write "<p>" & SQL & "</p>" & vbcrlf
	'response.end
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		strline = ""
		for each whatever in RS.fields
			if whatever.name <> "pword" then strline = strline & whatever.name & vbTab
		next
		file.WriteLine left(strline,len(strline)-1)
		do until RS.eof
			strline = ""
			for each whatever in RS.fields
				if whatever.name <> "pword" then strline = strline & chr(34) & whatever.value & chr(34) & vbTab
			next
			file.WriteLine left(strline,len(strline)-1)
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
	set file = nothing
	set fs = nothing
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
