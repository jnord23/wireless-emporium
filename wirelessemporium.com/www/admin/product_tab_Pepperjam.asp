<%
response.buffer = false
pageTitle = "Create Product List for Pepperjam"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 1000 'seconds

dim fs, file, filename, path
'Map the file name to the physical path on the server.
filename = "productList_pepperjam.txt"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List for Pepperjam</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							Response.Write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href=""../tempCSV/" & filename & """>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
	
<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.Write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	response.write("Creating " & filename & ".<br>")
	set file = fs.CreateTextFile(path)
	dim SQL, RS
	SQL = "SELECT A.itemID,A.PartNumber,A.brandID,A.itemDesc,A.itemPic,A.price_Retail,A.price_Our,A.inv_qty,A.Sports,A.flag1,A.UPCCode,A.itemLongDetail,"
	SQL = SQL & "B.brandName,C.modelName,C.[temp],D.typeName"
	SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
	SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
	SQL = SQL & " LEFT JOIN we_Types D ON A.typeID=D.typeID"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.Open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "name" & vbtab & "sku" & vbtab & "buy_url" & vbtab & "image_url" & vbtab & "image_thumb_url" & vbtab & "description_long" & vbtab & "description_short" & vbtab & "price_sale" & vbtab & "price" & vbtab & "keywords" & vbtab & "manufacturer" & vbtab & "condition" & vbtab & "mpn" & vbtab & "upc"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
				set RS2 = CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				GRPNAME = RS("itemDesc")
				
				if isnull(RS("typeName")) then
					stypeName = "Universal"
				else 
					stypeName = RS("typeName")
				end if
				if isnull(RS("ModelName")) then
					ModelName = "Universal"
				else
					ModelName = RS("BrandName") & " > " & RS("ModelName")
				end if
				if isnull(RS("BrandName")) then
					BrandName = "Universal"
				else 
					BrandName = RS("BrandName")
				end if
				
				ShortDescription = RS("itemLongDetail")
				ShortDescription = replace(ShortDescription,vbcrlf," ")
				if len(ShortDescription) > 255 then ShortDescription = left(ShortDescription,252) & "..."
				
				file.write chr(34) & GRPNAME & chr(34) & vbtab	'name
				file.write RS("itemid") & vbtab	'sku
				file.write "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?id=ppjproduct" & vbtab	'buy_url
				file.write "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab	'image_url"
				file.write "http://www.wirelessemporium.com/productpics/thumb/" & RS("itemPic") & vbtab	'image_thumb_url
				file.write vbtab	'description_long
				file.write chr(34) & ShortDescription & chr(34) & vbtab	'description_short
				file.write RS("price_our") & vbtab	'price
				file.write RS("price_retail") & vbtab	'price_retail
				file.write chr(34) & stypeName & ", " & BrandName & ", " & ModelName & ", " & GRPNAME & chr(34) & vbtab	'keywords
				file.write chr(34) & BrandName & chr(34) & vbtab	'manufacturer
				file.write "new" & vbtab	'condition
				file.write RS("PartNumber") & vbtab	'mpn
				file.write RS("UPCCode") & vbtab	'upc
				file.write vbcrlf
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
