<%
response.buffer = false
pageTitle = "Admin - Upload DecalSkin Images Batch"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
Server.ScriptTimeout = 2400 'seconds

if request.form("submitted") = "Upload New Image Batch" then
	dim path, path_CO, DecalskinFolder, newName, newName_CO, SavePath
	dim filesys, jpeg, demofolder, filecoll, fil
	dim aCount
	aCount = 0
	
	path = server.MapPath("/productpics/")
'	path_CO = "C:\inetpub\wwwroot\cellularoutfitter.com\www\productpics\"
	path_CO = server.MapPath("/productpics_co") & "\"	
	
	DecalskinFolder = "C:\Inetpub\wwwroot\wirelessemporium.com\staging\Decalskin\"
	
	set filesys = Server.CreateObject("Scripting.FileSystemObject")
	set jpeg = Server.CreateObject("Persits.Jpeg")
	set demofolder = filesys.GetFolder(DecalskinFolder)
	set filecoll = demofolder.Files
	for each fil in filecoll
		newName = fil.name
		newName_CO = replace(fil.name,"_skin_","_decal_skin_")
		
		jpeg.Open(DecalskinFolder & newName)
		SavePath = path & "\big\" & newName
		jpeg.Width = 300
		jpeg.Height = 300
		jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		SavePath = path & "\thumb\" & newName
		jpeg.Width = 100
		jpeg.Height = 100
		jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		SavePath = path & "\homepage65\" & newName
		jpeg.Width = 65
		jpeg.Height = 65
		jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		SavePath = path & "\icon\" & newName
		jpeg.Width = 45
		jpeg.Height = 45
		jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		
		jpeg.Open(DecalskinFolder & newName)
		SavePath = path_CO & "big\" & newName_CO
		jpeg.Width = 300
		jpeg.Height = 300
		response.Write("SavePath:" & SavePath & "<br>")
		jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		SavePath = path_CO & "thumb\" & newName_CO
		jpeg.Width = 100
		jpeg.Height = 100
		jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		SavePath = path_CO & "homepage65\" & newName_CO
		jpeg.Width = 65
		jpeg.Height = 65
		jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		SavePath = path_CO & "icon\" & newName_CO
		jpeg.Width = 45
		jpeg.Height = 45
		jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		
		aCount = aCount + 1
	next
	
	response.write "<h3>" & aCount & " images uploaded.</h3>"
	
	set filesys = nothing
	set jpeg = nothing
	set demofolder = nothing
	set filecoll = nothing
else
	%>
	<form action="Decalskin_upload_images_batch.asp" method="post">
		<input type="submit" name="submitted" value="Upload New Image Batch">
	</form>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
