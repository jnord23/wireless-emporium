<%
pageTitle = "Add/Edit Models"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/framework/utility/ftp.asp"-->
<style>
	.featureTitleRow	{ margin-top:15px; font-size:12px; display:table; }
	.featureInputRow	{ margin-top:5px; display:table; }
	.featureInput		{ float:left; }
	.featureInputField	{ width:100px; }
	.featureMeasure		{ float:left; margin-left:5px; }
</style>
<table border="0" width="900" align="center" cellpadding="0" cellspacing="0"><tr><td>
<p>&nbsp;</p>
<p class="biggerText"><%=pageTitle%></p>
<br>

<%
function timeStamp()
	timeStamp = replace(replace(replace(replace(time,":","")," ",""),"PM",""),"AM","")
end function

useFTP = true
if useFTP then
	call initConnection(ftpWEB2,"192.168.112.161","ftpAdmin","GSF79h7yr9r3$C")	'WEB2
	call initConnection(ftpWEB3,"192.168.112.163","ftpAdmin","GSF79h7yr9r3$C")	'WEB3
end if

dim Upload, Path, myCount
Set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
Path = server.MapPath("/productpics/models/")
tempPath = server.MapPath("/productpics/tempImages/models/")
myCount = Upload.Save

dim frmSubmit, strError
frmSubmit = Upload.form("frmSubmit")
strError = ""

dim brandID, subBrandID, modelName, temp, PhonedogID, modelID, excludePouches, includeNFL, includeExtraItem, ReleaseQuarter, ReleaseYear, carrierCode, HandsfreeTypes
dim batteryCapacity, usageTime, standbyTime

brandID = Upload.form("brandID")
subBrandID = Upload.form("subBrandID")
modelName = Upload.form("modelName")
linkName = Upload.form("linkName")
temp = Upload.form("temp")
PhonedogID = Upload.form("PhonedogID")
modelID = Upload.form("modelID")
excludePouches = Upload.form("excludePouches")
includeNFL = Upload.form("includeNFL")
includeExtraItem = Upload.form("includeExtraItem")
ReleaseQuarter = Upload.form("ReleaseQuarter")
ReleaseYear = Upload.form("ReleaseYear")
intPhone = Upload.form("intPhone")
hideLive = Upload.form("hideLive")
imgAlready = Upload.form("imgAlready")
isTablet = Upload.form("isTablet")
opCnt = Upload.form("opCnt")
batteryCapacity = prepInt(Upload.form("batteryCapacity"))
usageTime = prepInt(Upload.form("usageTime"))
standbyTime = prepInt(Upload.form("standbyTime"))

dim relatedSetName : relatedSetName = prepStr(Upload.form("relatedSetName"))
dim subRelatedSetName : subRelatedSetName = prepStr(Upload.form("subRelatedSetName"))
dim relatedOptions : relatedOptions = ""
dim subRelatedOptions : subRelatedOptions = ""
if isnull(opCnt) or len(opCnt) < 1 then opCnt = 0
for i = 1 to opCnt
	curID = Upload.form("relatedOptions_" & i)
	if curID <> "" then
		relatedOptions = relatedOptions & Upload.form("relatedOptions_" & i) & ","
	end if
next

if isnull(intPhone) or len(intPhone) < 1 then intPhone = 0
if isnull(hideLive) or len(hideLive) < 1 then hideLive = 0
if isnull(isTablet) or len(isTablet) < 1 then isTablet = 0

carrierCode = ""
HandsfreeTypes = ""
for each thing in Upload.form
	if thing.name = "carrierCode" and not isEmpty(thing.value) then carrierCode = carrierCode & thing.value & ","
	if thing.name = "HandsfreeTypeID" and not isEmpty(thing.value) then HandsfreeTypes = HandsfreeTypes & thing.value & ","
	if thing.name = "subRelatedOptions" and not isEmpty(thing.value) then subRelatedOptions = subRelatedOptions & thing.value & ","
next

if frmSubmit <> "" then
	autoDeleteCompPages "model",brandID & "##" & modelID
	if imgAlready = 0 then
		if myCount = 0 then strError = strError & "<li>No image selected for upload.</li>"
	end if
	if modelName = "" then strError = strError & "<li>You must supply a modelName.</li>"
	if temp = "" then strError = strError & "<li>You must supply a temp value.</li>"
	if brandID = "" or brandID = "0" then
		strError = strError & "<li>You must select a Brand.</li>"
	else
		SQL = "SELECT brandName FROM we_Brands WHERE brandID='" & brandID & "'"
		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 0, 1
		if not RS.eof then
			brandName = RS("brandName")
		else
			strError = strError & "<li>Invalid Brand selected.</li>"
		end if
	end if
	if PhonedogID = "" then
		PhonedogID = "null"
	else
		PhonedogID = "'" & SQLquote(PhonedogID) & "'"
	end if
	if excludePouches <> "-1" then excludePouches = "0"
	if includeNFL <> "-1" then includeNFL = "0"
	if not isNumeric(includeExtraItem) or includeExtraItem = "" then includeExtraItem = "null"
	if not isNumeric(ReleaseQuarter) or ReleaseQuarter = "" then ReleaseQuarter = "null"
	if not isNumeric(ReleaseYear) or ReleaseYear = "" then ReleaseYear = "null"
	if strError = "" then
		for each file in Upload.files
			if File.ImageType <> "JPEG" and File.ImageType <> "JPG" then
				response.write "<p>Wrong file format. All files must be JPGs.</p>"
				response.write "<p><a href=""javascript:history.back();"">BACK</a></p>"
				response.end
			else
				myFileName = File.ExtractFileName
				myFileName = replace(myFileName,".","_" & timeStamp & ".")
				modelImg = replace(myFileName,".jpg","")
				SavePath = tempPath & "\" & myFileName
				File.SaveAs SavePath
				response.write "Saved: " & SavePath & "<br>" & vbCrLf
				call uploadImagesRemote("\productpics\models", SavePath, myFileName)

				dim filesys, demofolder, fil, filecoll, filist
				set filesys = CreateObject("Scripting.FileSystemObject")
				response.Write("Searching: " & server.MapPath("/productpics/models/" & myFileName) & "<br>")
				rackSpaceFileLoc = server.MapPath("/productpics/models/" & myFileName)
				if filesys.FileExists(server.MapPath("/productpics/models/" & myFileName)) then
					Set myFile = filesys.GetFile(server.MapPath("/productpics/models/" & myFileName))
				else
					response.write "<h3>File does not exist!</h3>"
					response.end
				end if
'				myFolder = server.MapPath("/productpics/modelsYahoo/")
				myFolder = server.MapPath("/productpics/tempImages/modelsYahoo/")

				' FADED
				set jpeg = Server.CreateObject("Persits.Jpeg")
				set img = Server.CreateObject("Persits.Jpeg")
				jpeg.New 100, 160, &HFFFFFF
				img.open myFile
				img.Width = 81
				img.Height = 130
				jpeg.Canvas.DrawImage 0, 0, img, 0.2
				fadeFileName = replace(myFolder,"modelsYahoo","models\faded\") & modelImg & ".jpg"
				response.write "<p>jpeg.save " & fadeFileName & "</p>" & vbcrlf
				jpeg.save fadeFileName
				call uploadImagesRemote("\productpics\models\faded", fadeFileName, modelImg & ".jpg")

				' 75-100 for PS
				set jpeg = Server.CreateObject("Persits.Jpeg")
				set img = Server.CreateObject("Persits.Jpeg")
				jpeg.New 75, 120, &HFFFFFF
				img.open myFile
				img.Width = 75
				img.Height = 120
				jpeg.Canvas.DrawImage 0, 0, img
				psFileName = replace(myFolder,"modelsYahoo","models75-120") & modelImg & ".jpg"
				response.write "<p>jpeg.save " & psFileName & "</p>" & vbcrlf
				jpeg.save psFileName
				call uploadImagesRemote("\productpics\models", psFileName, "models75-120" & modelImg & ".jpg")
			end if
		next

		if frmSubmit = "Edit" then
			SQL = "UPDATE we_Models SET "
			SQL = SQL & "brandID = '" & brandID & "', "
			SQL = SQL & "modelName = '" & SQLquote(modelName) & "', "
			SQL = SQL & "linkName = '" & SQLquote(linkName) & "', "
			if len(trim(myFileName)) > 0 then
			SQL = SQL & "modelImg = '" & SQLquote(myFileName) & "', "
			end if
			SQL = SQL & "temp = '" & temp & "', "
			SQL = SQL & "PhonedogID = " & PhonedogID & ", "
			SQL = SQL & "carrierCode = '" & carrierCode & ",', "
			SQL = SQL & "excludePouches = " & excludePouches & ", "
			SQL = SQL & "includeNFL = " & includeNFL & ", "
			SQL = SQL & "includeExtraItem = " & includeExtraItem & ", "
			SQL = SQL & "ReleaseQuarter = " & ReleaseQuarter & ", "
			SQL = SQL & "ReleaseYear = " & ReleaseYear & ", "
			SQL = SQL & "International = " & intPhone & ", "
			SQL = SQL & "hideLive = " & hideLive & ", "
			SQL = SQL & "isTablet = " & isTablet & ", "
			SQL = SQL & "subBrandID = '" & subBrandID & "', "
			SQL = SQL & "batteryCapacity = " & batteryCapacity & ", "
			SQL = SQL & "usageTime = " & usageTime & ", "
			SQL = SQL & "standbyTime = " & standbyTime & ", "
			if HandsfreeTypes <> "" and HandsfreeTypes <> "," then
				SQL = SQL & "HandsfreeTypes = '" & HandsfreeTypes & "'"
			else
				SQL = SQL & "HandsfreeTypes = null"
			end if
			SQL = SQL & " WHERE modelID='" & modelID & "'"
			session("errorSQL") = SQL
			response.write "<p>" & SQL & "</p>"
			oConn.execute SQL

			sql = "insert into we_adminActions (adminID,action) values(" & adminID & ",'Update Model Info:" & SQLquote(modelName) & " (" & modelID & ")')"
			session("errorSQL") = SQL
			oConn.execute SQL

			strChkRelated = prepStr(Upload.form("chkRelated"))
			if strChkRelated = "" then
				sql = "select a.itemID, b.itemDesc, c.typeID from we_defaultRelatedItems a left join we_items b on a.itemID = b.itemID left join we_types c on b.typeID = c.typeID where a.restrictions is null order by b.typeID"
				session("errorSQL") = sql
				set relatedRS = oConn.execute(sql)

				do while not relatedRS.EOF
					sql = "if (select count(*) from we_relatedItems where modelID = " & modelID & " and itemID = " & relatedRS("itemID") & ") < 1 insert into we_relatedItems (itemID,typeID,modelID,brandID,itemDesc2) values(" & relatedRS("itemID") & "," & relatedRS("typeID") & "," & modelID & "," & brandID & ",'" & ds(relatedRS("itemDesc")) & "')"
					session("errorSQL") = sql
					oConn.execute(sql)
					relatedRS.movenext
				loop

				if relatedOptions <> "" then
					relatedArray = split(relatedOptions,",")
					for i = 0 to (ubound(relatedArray) - 1)
						sql = "select itemDesc, typeID from we_items where itemID = " & relatedArray(i)
						session("errorSQL") = sql
						set relatedRS = oConn.execute(sql)

						sql = "if (select count(*) from we_relatedItems where modelID = " & modelID & " and itemID = " & relatedArray(i) & ") < 1 insert into we_relatedItems (itemID,typeID,modelID,brandID,itemDesc2) values(" & relatedArray(i) & "," & relatedRS("typeID") & "," & modelID & "," & brandID & ",'" & ds(relatedRS("itemDesc")) & "')"
						session("errorSQL") = sql
						oConn.execute(sql)
					next
				end if

				'========= sub related item insert with optional ========
				sql	=	"insert into we_subRelatedItems(itemid, subtypeid, modelid, brandid)" & vbcrlf & _
						"select	a.itemid, a.subtypeid, a.modelid, a.brandid" & vbcrlf & _
						"from	(" & vbcrlf & _
						"		select	distinct itemid, subtypeid, '" & modelID & "' modelid, '" & brandID & "' brandid" & vbcrlf & _
						"		from	we_defaultSubRelatedItems" & vbcrlf & _
						"		where	setName = '" & subRelatedSetName & "' and restrictions is null" & vbcrlf & _
						"		union " & vbcrlf & _
						"		select	distinct itemid, subtypeid, '" & modelID & "' modelid, '" & brandID & "' brandid" & vbcrlf & _
						"		from	we_defaultSubRelatedItems" & vbcrlf & _
						"		where	id in (	select	distinct rString from dbo.tfn_StringToColumn('" & subRelatedOptions & "', ',') )" & vbcrlf & _
						"		) a left outer join we_subRelatedItems b" & vbcrlf & _
						"	on	a.itemid = b.itemid and a.subtypeid = b.subtypeid and a.modelid = b.modelid and a.brandid = b.brandid" & vbcrlf & _
						"where	b.related_id is null" & vbcrlf
'				response.write "<pre>" & sql & "</pre>"
				oConn.execute(sql)
			end if

			response.write "<h3>Model Updated!</h3>" & vbcrlf
		elseif frmSubmit = "Add" then
			sql = "select a.itemID, b.itemDesc, c.typeID from we_defaultRelatedItems a left join we_items b on a.itemID = b.itemID left join we_types c on b.typeID = c.typeID where a.setName = '" & relatedSetName & "' and a.restrictions is null order by b.typeID"
			session("errorSQL") = sql
			set relatedRS = oConn.execute(sql)

			SQL =	"SET NOCOUNT ON; INSERT INTO we_Models (brandID,modelName,modelImg,temp,PhonedogID,carrierCode,excludePouches," &_
					"includeNFL,includeExtraItem,ReleaseQuarter,ReleaseYear,HandsfreeTypes,isTablet,subBrandID,International," &_
					"batteryCapacity,usageTime,standbyTime) " &_
					"VALUES ("
			SQL = SQL & "'" & brandID & "', "
			SQL = SQL & "'" & SQLquote(modelName) & "', "
			SQL = SQL & "'" & SQLquote(myFileName) & "', "
			SQL = SQL & "'" & temp & "', "
			SQL = SQL & PhonedogID & ", "
			SQL = SQL & "'" & carrierCode & ",', "
			SQL = SQL & excludePouches & ", "
			SQL = SQL & includeNFL & ", "
			SQL = SQL & includeExtraItem & ", "
			SQL = SQL & ReleaseQuarter & ", "
			SQL = SQL & ReleaseYear & ", "
			if HandsfreeTypes <> "" and HandsfreeTypes <> "," then
				SQL = SQL & "'" & HandsfreeTypes & "', "
			else
				SQL = SQL & "null, "
			end if
			SQL = SQL & isTablet & ", "
			SQL = SQL & "'" & subBrandID & "',"
			SQL = SQL & intPhone & ","
			SQL = SQL & batteryCapacity & ","
			SQL = SQL & usageTime & ","
			SQL = SQL & standbyTime
			SQL = SQL & "); SELECT @@IDENTITY AS NewID;"
			session("errorSQL") = SQL
			response.write "<p>" & SQL & "</p>"
			set objRS = oConn.execute(SQL)
			modelID = objRS.Fields("NewID").Value

			do while not relatedRS.EOF
				sql = "insert into we_relatedItems (itemID,typeID,modelID,brandID,itemDesc2) values(" & relatedRS("itemID") & ",'" & relatedRS("typeID") & "'," & modelID & "," & brandID & ",'" & ds(relatedRS("itemDesc")) & "')"
				session("errorSQL") = sql
				oConn.execute(sql)
				relatedRS.movenext
			loop

			if relatedOptions <> "" then
				relatedArray = split(relatedOptions,",")
				for i = 0 to (ubound(relatedArray) - 1)
					sql = "select itemDesc, typeID from we_items where itemID = " & relatedArray(i)
					session("errorSQL") = sql
					set relatedRS = oConn.execute(sql)

					sql = "insert into we_relatedItems (itemID,typeID,modelID,brandID,itemDesc2) values('" & relatedArray(i) & "','" & relatedRS("typeID") & "'," & modelID & "," & brandID & ",'" & ds(relatedRS("itemDesc")) & "')"
					session("errorSQL") = sql
					oConn.execute(sql)
				next
			end if

			'========= sub related item insert with optional ========
			sql	=	"insert into we_subRelatedItems(itemid, subtypeid, modelid, brandid)" & vbcrlf & _
					"select	a.itemid, a.subtypeid, a.modelid, a.brandid" & vbcrlf & _
					"from	(" & vbcrlf & _
					"		select	distinct itemid, subtypeid, '" & modelID & "' modelid, '" & brandID & "' brandid" & vbcrlf & _
					"		from	we_defaultSubRelatedItems" & vbcrlf & _
					"		where	setName = '" & subRelatedSetName & "' and restrictions is null" & vbcrlf & _
					"		union " & vbcrlf & _
					"		select	distinct itemid, subtypeid, '" & modelID & "' modelid, '" & brandID & "' brandid" & vbcrlf & _
					"		from	we_defaultSubRelatedItems" & vbcrlf & _
					"		where	id in (	select	distinct rString from dbo.tfn_StringToColumn('" & subRelatedOptions & "', ',') )" & vbcrlf & _
					"		) a left outer join we_subRelatedItems b" & vbcrlf & _
					"	on	a.itemid = b.itemid and a.subtypeid = b.subtypeid and a.modelid = b.modelid and a.brandid = b.brandid" & vbcrlf & _
					"where	b.related_id is null" & vbcrlf
			response.write "<pre>" & sql & "</pre>"
			oConn.execute(sql)

			'============== Set Link Name ====================
			sql =	"select a.modelID, b.brandName, a.modelName, a.linkName " &_
					"from we_models a " &_
						"left join we_brands b on a.brandID = b.brandID " &_
					"order by a.modelID"
			session("errorSQL") = sql
			set modelRS = oConn.execute(sql)

			do while not modelRS.EOF
				modelID = prepInt(modelRS("modelID"))
				brandName = prepStr(modelRS("brandName"))
				modelName = prepStr(modelRS("modelName"))
				linkName = prepStr(modelRS("linkName"))

				useLinkName = lcase(brandName & " " & modelName)

				if useLinkName <> linkName then
					sql = "update we_models set linkName = '" & useLinkName & "' where modelID = " & modelID
					session("errorSQL") = sql
					oConn.execute(sql)
				end if

				modelRS.movenext
			loop
			'============== End Link Name ====================

			'============== Send Email =======================
			cdo_from = "Purchasing <wepurchasing@wirelessemporium.com>"
			if intPhone = 1 then
				cdo_subject = "New International Phone Added"
			else
				cdo_subject = "New Phone Added"
			end if
			cdo_body = "The " & brandName & " " & modelName & " has been added to the system"
			'cdo_to = "wemarketing@wirelessemporium.com,wemerch@wirelessemporium.com,jon@wirelessemporium.com,tony@wirelessemporium.com,wepurchasing@wirelessemporium.com"
			cdo_to = emailList("New Phone Added")
			CDOSend cdo_to,cdo_from,cdo_subject,cdo_body

			response.write "<h3>Model Added! (" & modelID & ")</h3>" & vbcrlf
		end if
		response.write "<p><a href=""/admin/db_search_brands.asp?searchID=5"">Back to Add/Edit Models.</a></p>" & vbcrlf
	else
		response.write "<h3><font color=""#FF0000"">Please <a href=""javascript:history.back();"">go back</a> and correct the following errors:</font></h3>" & vbcrlf
		response.write "<ul>" & strError & "</ul>" & vbcrlf
	end if
else
	BrandID = request.querystring("BrandID")
	modelID = request.querystring("modelID")
	if modelID <> "" then
		SQL = "SELECT A.*, B.brandName FROM we_Models A INNER JOIN we_Brands B ON A.brandID=B.brandID WHERE A.modelID='" & modelID & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 0, 1
		if RS.eof then
			response.write "Model Was Not Found in Database."
			response.end
		else
			modelName = RS("modelName")
			linkName = RS("linkName")
			brandName = RS("brandName")
			subBrandID = RS("subBrandID")
			if isnull(subBrandID) then subBrandID = 0
			GRPNAME = brandName & " " & modelName
			CAmodel = formatSEO(GRPNAME)
			COmodel = formatSEO(GRPNAME)
			modelImg = RS("modelImg")
			carrierCode = RS("carrierCode")
			temp = RS("temp")
			PhonedogID = RS("PhonedogID")
			HandsfreeTypes = "," & RS("HandsfreeTypes")
			excludePouches = RS("excludePouches")
			includeNFL = RS("includeNFL")
			includeExtraItem = RS("includeExtraItem")
			dbReleaseQuarter = RS("ReleaseQuarter")
			dbReleaseYear = RS("ReleaseYear")
			internationalPhone = rs("international")
			hideLive = rs("hideLive")
			isTablet = rs("isTablet")
			batteryCapacity = prepInt(rs("batteryCapacity"))
			usageTime = prepInt(rs("usageTime"))
			standbyTime = prepInt(rs("standbyTime"))
		end if
	end if
	%>
	<form enctype="multipart/form-data" action="db_update_models.asp" method="POST">
		<input type="hidden" name="modelID" value="<%=modelID%>">
		<table border="1" width="100%" cellpadding="5" cellspacing="0">
			<tr><td colspan="2" align="left"><a href="/T-<%=modelID%>-cell-accessoris-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>.asp">View Model on WE Public Site</a></td></tr>
            <tr>
				<td width="40%" valign="top">
					<p>
						brandName:&nbsp;
						<select name="brandID">
							<option value=""<%if isNull(brandID) or brandID = "" then response.write " selected"%>></option>
							<%
							SQL = "SELECT * FROM we_Brands WHERE brandName IS NOT null AND brandName <> '' ORDER BY brandName"
							Set RSbrand = Server.CreateObject("ADODB.Recordset")
							RSbrand.open SQL, oConn, 0, 1
							do until RSbrand.eof
								response.write "<option value=""" & RSbrand("brandID") & """"
								if brandID = cStr(RSbrand("brandID")) then response.write " selected"
								response.write ">" & RSbrand("brandName") & "</option>" & vbcrlf
								RSbrand.movenext
							loop
							%>
						</select><br>
						<%
						SQL = "SELECT * FROM we_subBrands where brandid = '" & brandid & "'"
						Set RSSubBrand = Server.CreateObject("ADODB.Recordset")
						RSSubBrand.open SQL, oConn, 0, 1
						if not RSSubBrand.eof then
						%>
                        subBrandName:&nbsp;
						<select name="subBrandID">
							<option value=""<%if isNull(subBrandID) or subBrandID = "" then response.write " selected"%>></option>
							<%
							do until RSSubBrand.eof
								%>
                                <option value="<%=RSSubBrand("subBrandID")%>" <%if subBrandID = RSSubBrand("subBrandID") then%> selected <%end if%>><%=RSSubBrand("subBrandName_WE")%></option>
                                <%
								RSSubBrand.movenext
							loop
							%>
						</select><br />
                        <%
						end if
						%>

                        International Phone: <input type="checkbox" name="intPhone" value="1"<% if internationalPhone then %> checked="checked"<% end if %> /><br />
                        Hide Live: <input type="checkbox" name="hideLive" value="1"<% if hideLive then %> checked="checked"<% end if %> /><br />
						Is a Tablet: <input type="checkbox" name="isTablet" value="1"<% if isTablet then %> checked="checked"<% end if %> /><br />
                        modelName:<br><input type="text" name="modelName" value="<%=modelName%>" size="30"><br>
                        linkName:<br><input type="text" name="linkName" value="<%=linkName%>" size="50" style="font-size:10px;"><br>
						Carrier:<br>
							<%
							SQL = "SELECT * FROM we_Carriers WHERE carrierName IS NOT null ORDER BY listOrder"
							Set RScarrier = Server.CreateObject("ADODB.Recordset")
							RScarrier.open SQL, oConn, 3, 3
							aCount = 0
							do until RScarrier.eof
								response.write "<input type=""checkbox"" name=""carrierCode"" value=""" & RScarrier("carrierCode") & """"
								if inStr(carrierCode & ",",RScarrier("carrierCode")) > 0 then response.write " checked"
								response.write ">" & RScarrier("carrierName") & "&nbsp;&nbsp;&nbsp;" & vbcrlf
								aCount = aCount + 1
								if (aCount mod 2) = 0 then response.write "<br>"
								RScarrier.movenext
							loop
							%>
                        <br />
						HandsfreeTypes:<br>
						<select name="HandsfreeTypeID" size="5" multiple>
							<option value=""<%if isNull(HandsfreeTypes) or HandsfreeTypes = "" or HandsfreeTypes = "," then response.write " selected"%>></option>
							<option value="1"<%if inStr(HandsfreeTypes,",1,") > 0 then response.write " selected"%>>UNI 2.5mm</option>
							<option value="2"<%if inStr(HandsfreeTypes,",2,") > 0 then response.write " selected"%>>BLUETOOTH</option>
							<option value="3"<%if inStr(HandsfreeTypes,",3,") > 0 then response.write " selected"%>>3.5mm Stereo</option>
							<option value="4"<%if inStr(HandsfreeTypes,",4,") > 0 then response.write " selected"%>>MINI USB</option>
							<option value="5"<%if inStr(HandsfreeTypes,",5,") > 0 then response.write " selected"%>>MICRO USB</option>
							<option value="6"<%if inStr(HandsfreeTypes,",6,") > 0 then response.write " selected"%>>Old Sony Ericcson</option>
							<option value="7"<%if inStr(HandsfreeTypes,",7,") > 0 then response.write " selected"%>>SONY K750/Z520</option>
							<option value="8"<%if inStr(HandsfreeTypes,",8,") > 0 then response.write " selected"%>>LG Chocolate</option>
							<option value="9"<%if inStr(HandsfreeTypes,",9,") > 0 then response.write " selected"%>>NEXT i1000</option>
							<option value="10"<%if inStr(HandsfreeTypes,",10,") > 0 then response.write " selected"%>>NEXT 2.5 w/PTT</option>
							<option value="11"<%if inStr(HandsfreeTypes,",11,") > 0 then response.write " selected"%>>NOK 7210</option>
							<option value="12"<%if inStr(HandsfreeTypes,",12,") > 0 then response.write " selected"%>>NOK 3390</option>
							<option value="13"<%if inStr(HandsfreeTypes,",13,") > 0 then response.write " selected"%>>SAM R225</option>
							<option value="14"<%if inStr(HandsfreeTypes,",14,") > 0 then response.write " selected"%>>SAM T809</option>
							<option value="15"<%if inStr(HandsfreeTypes,",15,") > 0 then response.write " selected"%>>SAM M300/510</option>
							<option value="16"<%if inStr(HandsfreeTypes,",16,") > 0 then response.write " selected"%>>SAM E105/U550</option>
							<option value="17"<%if inStr(HandsfreeTypes,",17,") > 0 then response.write " selected"%>>SAM E315</option>
							<option value="18"<%if inStr(HandsfreeTypes,",18,") > 0 then response.write " selected"%>>SAM S105</option>
							<option value="19"<%if inStr(HandsfreeTypes,",19,") > 0 then response.write " selected"%>>Siemens</option>
							<option value="20"<%if inStr(HandsfreeTypes,",20,") > 0 then response.write " selected"%>>PANT C150</option>
							<option value="21"<%if inStr(HandsfreeTypes,",21,") > 0 then response.write " selected"%>>HTC 3125</option>
							<option value="22"<%if inStr(HandsfreeTypes,",22,") > 0 then response.write " selected"%>>HTC 8525</option>
							<option value="23"<%if inStr(HandsfreeTypes,",23,") > 0 then response.write " selected"%>>APPLE ONLY</option>
							<option value="24"<%if inStr(HandsfreeTypes,",24,") > 0 then response.write " selected"%>>Palm</option>
							<option value="25"<%if inStr(HandsfreeTypes,",25,") > 0 then response.write " selected"%>>Treo 650</option>
							<option value="26"<%if inStr(HandsfreeTypes,",26,") > 0 then response.write " selected"%>>SAM 2.5MM</option>
							<option value="28"<%if inStr(HandsfreeTypes,",28,") > 0 then response.write " selected"%>>IPHONE4/4S</option>
						</select>
						<br>
						temp:<br><input type="text" name="temp" value="<%=temp%>" maxlength="3" size="3"><br>
						PhonedogID:<br><input type="text" name="PhonedogID" value="<%=PhonedogID%>">
                        <div class="featureTitleRow">Battery Capacity:</div>
                        <div class="featureInputRow">
                        	<div class="featureInput"><input type="text" name="batteryCapacity" value="<%=batteryCapacity%>" class="featureInputField" /></div>
                            <div class="featureMeasure">mAh</div>
                        </div>
                        <div class="featureTitleRow">Usage Time:</div>
                        <div class="featureInputRow">
                        	<div class="featureInput"><input type="text" name="usageTime" value="<%=usageTime%>" class="featureInputField" /></div>
                            <div class="featureMeasure">hours</div>
                        </div>
                        <div class="featureTitleRow">Standby Time:</div>
                        <div class="featureInputRow">
                        	<div class="featureInput"><input type="text" name="standbyTime" value="<%=standbyTime%>" class="featureInputField" /></div>
                            <div class="featureMeasure">days</div>
                        </div>
					</p>
				</td>
				<td width="60%" valign="top">
					<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" valign="top">
								<p>
									<%
									if not isNull(modelImg) and modelImg <> "" then
										imgAlready = 1
									%>
										<img src="/productpics/models/<%=modelImg%>" align="right">
										modelImg:<br>
										<%=modelImg%><br>
									<%
									else
										imgAlready = 0
									end if
									%>
									<input type="FILE" name="FILE1">
								</p>
							</td>
						</tr>
						<tr>
							<td width="100%" valign="top">
								<p>&nbsp;</p>
								<hr>
								<p>&nbsp;</p>
							</td>
						</tr>
						<tr>
							<td width="100%" valign="top">
								Exclude Pouches: <input type="checkbox" name="excludePouches" value="-1"<%if excludePouches = true then response.write " checked"%>><br>
								Include NFL: <input type="checkbox" name="includeNFL" value="-1"<%if includeNFL = true then response.write " checked"%>><br>
								Include Extra Item: <input type="text" name="includeExtraItem" value="<%=includeExtraItem%>">
								<br>
								Release Quarter:
								<select name="ReleaseQuarter">
									<option value=""<%if isNull(dbReleaseQuarter) or dbReleaseQuarter = 0 then response.write " selected"%>></option>
									<option value="1"<%if dbReleaseQuarter = 1 then response.write " selected"%>>1</option>
									<option value="2"<%if dbReleaseQuarter = 2 then response.write " selected"%>>2</option>
									<option value="3"<%if dbReleaseQuarter = 3 then response.write " selected"%>>3</option>
									<option value="4"<%if dbReleaseQuarter = 4 then response.write " selected"%>>4</option>
								</select>
								<br>
								Release Year:
								<select name="ReleaseYear">
									<option value=""<%if isNull(dbReleaseYear) or dbReleaseYear = 0 then response.write " selected"%>></option>
									<%
									for a = -14 to 1
										%>
										<option value="<%=year(date) + a%>"<%if dbReleaseYear = year(date) + a then response.write " selected"%>><%=year(date) + a%></option>
										<%
									next
									%>
								</select>
							</td>
						</tr>
                        <%
						sql = "select a.itemID, b.itemDesc, c.typeName, a.restrictions from we_defaultRelatedItems a left join we_items b on a.itemID = b.itemID left join we_types c on b.typeID = c.typeID where setName = 'Cellphone Default' order by a.restrictions, b.typeID"
						session("errorSQL") = sql
						set relatedRS = oConn.execute(sql)

						sql = "select distinct setName from we_defaultRelatedItems order by setName"
						session("errorSQL") = sql
						set relatedSetsRS = oConn.execute(sql)

						sql = 	"select	a.id, a.itemID, b.itemDesc, c.subTypeName, a.restrictions" & vbcrlf & _
								"from	we_defaultSubRelatedItems a join we_items b" & vbcrlf & _
								"	on	a.itemID = b.itemID join we_subtypes c" & vbcrlf & _
								"	on	a.subtypeid = c.subtypeid" & vbcrlf & _
								"where	setName = 'Cellphone Default'" & vbcrlf & _
								"order by a.restrictions, c.subtypeid" & vbcrlf
						session("errorSQL") = sql
						set subRelatedRS = oConn.execute(sql)

						sql = "select distinct setName from we_defaultSubRelatedItems order by setName"
						session("errorSQL") = sql
						set subRelatedSetsRS = oConn.execute(sql)
						%>
                        <tr>
                        	<td style="padding-top:20px;" align="right">
	                            <div style="width:170px; border:1px solid #ccc; border-radius:3px;">Do not update related items <input type="checkbox" name="chkRelated" /></div>
                            </td>
						</tr>
                        <tr>
                        	<td>
                                <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
                                    <tr>
                                        <td align="center" width="150" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-on" id="id_header_related" onClick="showHide('related','subrelated');">Related Item Set</td>
                                        <td align="center" width="200" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-off" id="id_header_subrelated" onClick="showHide('subrelated','related');">Sub-Related Item Set (WE)</td>
                                        <td class="tab-header-off">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                        	<td id="currentRelatedItemSet" style="font-size:10px;">
                            	<div id="tbl_related">
                                    <div style="padding-top:10px; font-weight:bold; font-size:18px; border-bottom:1px solid #000; height:25px;">
                                        <div style="float:left;">Default Related Items</div>
                                        <div style="float:right;">
                                            <select name="relatedSetName" onchange="changeRelatedSet(this.value)">
                                                <% do while not relatedSetsRS.EOF %>
                                                <option value="<%=relatedSetsRS("setName")%>"><%=relatedSetsRS("setName")%></option>
                                                <%
                                                    relatedSetsRS.movenext
                                                loop
                                                %>
                                                <option value="Edit">Edit Related Item Sets</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div style="height:200px; overflow:scroll; border:1px solid #000; width:100%;">
                                    <%
                                    dim curType : curType = ""
                                    do while not relatedRS.EOF
                                        if curType <> relatedRS("typeName") then
                                            curType = relatedRS("typeName")
                                            response.Write("<div style='font-size:12px; font-weight:bold; background-color:#666; color:#fff;'>" & relatedRS("typeName") & "</div>")
                                        end if
                                        response.Write(relatedRS("itemDesc") & "<br>")
                                        relatedRS.movenext
                                        if not isnull(relatedRS("restrictions")) then exit do
                                    loop
                                    %>
                                    </div>
                                    <div style="padding-top:20px; font-weight:bold; font-size:18px; border-bottom:1px solid #000;">Optional Related Items</div>
                                    <div style="height:200px; overflow:scroll; border:1px solid #000;">
                                    <%
                                    dim resType : resType = ""
                                    dim opCnt : opCnt = 0
                                    do while not relatedRS.EOF
                                        opCnt = opCnt + 1
                                        if resType <> relatedRS("restrictions") then
                                            resType = relatedRS("restrictions")
                                            response.Write("<div style='font-size:12px; font-weight:bold; background-color:#666; color:#fff;'>" & relatedRS("restrictions") & "</div>")
                                        end if
                                        response.Write("<input name='relatedOptions_" & opCnt & "' type='checkbox' value='" & relatedRS("itemID") & "' />" & relatedRS("itemDesc") & " - " & relatedRS("typeName") & "<br>")
                                        relatedRS.movenext
                                    loop
                                    %>
                                    </div>
								</div>
                            	<div id="tbl_subrelated" style="display:none;">
                                    <div style="padding-top:10px; font-weight:bold; font-size:18px; border-bottom:1px solid #000; height:25px;">
                                        <div style="float:left;">Default Sub-Related Items</div>
                                        <div style="float:right;">
                                            <select name="subRelatedSetName" onchange="changeSubRelatedSet(this.value); return false;">
                                                <% do while not subRelatedSetsRS.EOF %>
                                                <option value="<%=subRelatedSetsRS("setName")%>"><%=subRelatedSetsRS("setName")%></option>
                                                <%
                                                    subRelatedSetsRS.movenext
                                                loop
                                                %>
                                                <option value="Edit">Edit Sub-Related Item Sets</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="tbl_subrelated_list">
                                        <div style="height:200px; overflow:scroll; border:1px solid #000; width:100%;">
                                        <%
                                        curType = ""
                                        do while not subRelatedRS.EOF
                                            if curType <> subRelatedRS("subTypeName") then
                                                curType = subRelatedRS("subTypeName")
                                                response.Write("<div style='font-size:12px; font-weight:bold; background-color:#666; color:#fff;'>" & subRelatedRS("subTypeName") & "</div>")
                                            end if
                                            response.Write(subRelatedRS("itemDesc") & "<br>")
                                            subRelatedRS.movenext
                                            if not isnull(subRelatedRS("restrictions")) then exit do
                                        loop
                                        %>
                                        </div>
                                        <div style="padding-top:20px; font-weight:bold; font-size:18px; border-bottom:1px solid #000;">Optional Sub-Related Items</div>
                                        <div style="height:200px; overflow:scroll; border:1px solid #000;">
                                        <%
                                        resType = ""
                                        do while not subRelatedRS.EOF
                                            if resType <> subRelatedRS("restrictions") then
                                                resType = subRelatedRS("restrictions")
                                                response.Write("<div style='font-size:12px; font-weight:bold; background-color:#666; color:#fff;'>" & subRelatedRS("restrictions") & "</div>")
                                            end if
                                            response.Write("<input name='subRelatedOptions' type='checkbox' value='" & subRelatedRS("id") & "' />" & subRelatedRS("itemDesc") & " - " & subRelatedRS("subTypeName") & "<br>")
                                            subRelatedRS.movenext
                                        loop
                                        %>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
					</table>
				</td>
			</tr>
		</table>
		<p>
        <input type="submit" name="frmSubmit" value="<%=request.querystring("submitType")%>">
        <input type="hidden" name="imgAlready" value="<%=imgAlready%>" />
        <input type="hidden" name="opCnt" value="<%=opCnt%>" />
        </p>
	</form>
	<%
end if

sub uploadImagesRemote(remoteDir, localFilePath, remoteFileName)
	if useFTP then
		success = ftpWEB2.ChangeRemoteDir(remoteDir)
		success = ftpWEB2.PutFile(localFilePath, remoteFileName)

		success = ftpWEB3.ChangeRemoteDir(remoteDir)
		success = ftpWEB3.PutFile(localFilePath, remoteFileName)
	end if
end sub
%>
</td></tr></table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function changeRelatedSet(setName) {
		if (setName == "Edit") {
			window.location='/admin/relatedItemSets.asp'
		}
		else {
			document.getElementById("tbl_related").innerHTML = 'http://staging.wirelessemporium.com/ajax/admin/ajaxRelatedItemSets.asp?useSetName=' + setName
			ajax('/ajax/admin/ajaxRelatedItemSets.asp?useSetName=' + setName,'tbl_related')
		}
	}

	function changeSubRelatedSet(setName) {
		if (setName == "Edit") {
			window.location='/admin/subRelatedItemSets.asp'
		}
		else {
			document.getElementById("tbl_subrelated_list").innerHTML = 'http://staging.wirelessemporium.com/ajax/admin/ajaxSubRelatedItemSets.asp?useSetName=' + setName
			ajax('/ajax/admin/ajaxSubRelatedItemSets.asp?useSetName=' + setName,'tbl_subrelated_list')
		}
	}

	function showHide(show,hide)
	{
		document.getElementById('id_header_' + hide).className = "tab-header-off";
		document.getElementById('tbl_' + hide).style.display = "none";

		document.getElementById('id_header_' + show).className = "tab-header-on";
		document.getElementById('tbl_' + show).style.display = "";
	}
</script>