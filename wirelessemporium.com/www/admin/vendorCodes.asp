<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - "
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	sql = "select distinct vendor from we_items order by vendor"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 3, 3
	
	dim allCodes
	allCodes = ""
	
	do while not rs.EOF
		if not isnull(rs("vendor")) then
			if len(trim(rs("vendor"))) > 0 then
				if instr(rs("vendor"),",") > 0 then
					venCodes = split(rs("vendor"),",")
					for i = 0 to ubound(venCodes)
						if len(trim(venCodes(i))) > 0 then
							if allCodes = "" then
								allCodes = "," & trim(venCodes(i)) & ","
							elseif instr(allCodes,"," & trim(venCodes(i)) & ",") < 1 then
								if len(trim(venCodes(i))) > 0 then
									allCodes = allCodes & trim(venCodes(i)) & ","
								end if
							end if
						end if
					next
				else
					if allCodes = "" then
						allCodes = "," & trim(rs("vendor")) & ","
					elseif instr(allCodes,"," & trim(rs("vendor")) & ",") < 1 then
						if len(trim(rs("vendor"))) > 0 then
							allCodes = allCodes & trim(rs("vendor")) & ","
						end if
					end if
				end if
			end if
		end if
		rs.movenext
	loop
	
	allCodes = mid(allCodes,1)
	MyArray = split(allCodes,",")
	max = ubound(MyArray)
	For i = 0 to max 
	   For j = i + 1 to max 
		  if MyArray(i) > MyArray(j) then
			  TemporalVariable = MyArray(i)
			  MyArray(i) = MyArray(j)
			  MyArray(j) = TemporalVariable
		 end if
	   next 
	next
%>
<table align="center" border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="500">
	<tr><td colspan="5" align="left" bgcolor="#333333" style="font-weight:bold; color:#FFF">Current Vendor Codes in we_items</td></tr>
    <tr>
    	<%
		cols = 0
		bgColor = "#ffffff"
		for i = 0 to ubound(MyArray)
			if len(trim(MyArray(i))) > 0 then
				cols = cols + 1
		%>
    	<td width="25%" align="center"><%=MyArray(i)%></td>
        <%
				if cols = 5 then
					cols = 0
					if bgColor = "#ffffff" then bgColor = "#f0f0f0" else bgColor = "#ffffff"
					response.Write("</tr><tr bgcolor='" & bgColor & "'>")
				end if
			end if
		next
		%>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
