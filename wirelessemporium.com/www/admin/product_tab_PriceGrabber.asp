<%
pageTitle = "Product List upload for PriceGrabber"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 9000 'seconds

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
'Map the file name to the physical path on the server.
filename = "WE_productList_PriceGrabber.txt"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List upload for PriceGrabber</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							response.write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='../tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		response.write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	dim fs, file
	response.write("<b>CreateFile:</b><br>")
	set fs = CreateObject("Scripting.FileSystemObject")
	'If the file already exists give an error message.
	if fs.FileExists(path) then
		response.write("File " & filename & " already exists, create aborted.<br>" & vbcrlf)
	else
		response.write("Creating " & filename & ".<br>")
		set file = fs.CreateTextFile(path)
		dim SQL, RS, strline
		SQL = "SELECT B.brandName, 'WE-' + CAST(A.itemid+5000 AS varchar(10)) AS MPN, A.itemid, A.PartNumber,"
		SQL = SQL & " A.itemDesc, A.itemLongDetail, A.price_Our AS price, A.itemPic, A.typeid, T.typename, A.UPCCode, A.inv_qty"
		SQL = SQL & " FROM we_Items A JOIN we_Types AS T ON A.typeid = T.typeid"
		SQL = SQL & " INNER JOIN we_Brands B ON A.brandID = B.brandID"
		SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
		SQL = SQL & " AND A.typeID <> 16"
		SQL = SQL & " AND (A.itemID IN (20107,23345,24244,24245,29878,31114,36425,49909,49912,50034,51924,55163,56513,56515,31416,34769,56838)"
		SQL = SQL & " OR (A.PartNumber NOT LIKE 'DEC%' AND A.PartNumber NOT LIKE 'FP2%' AND A.PartNumber NOT LIKE 'FP3%' AND A.PartNumber NOT LIKE 'FP5%' AND A.PartNumber NOT LIKE 'BT1%'"
		SQL = SQL & " AND A.PartNumber NOT LIKE 'LC0%' AND A.PartNumber NOT LIKE 'LC1%' AND A.PartNumber NOT LIKE 'LC2%' AND A.PartNumber NOT LIKE 'LC3%' AND A.PartNumber NOT LIKE 'LC4%' AND A.PartNumber NOT LIKE 'LC5%'"
		SQL = SQL & " AND A.PartNumber NOT LIKE 'HF1%' AND A.PartNumber NOT LIKE 'HF2%' AND A.PartNumber NOT LIKE 'HF3%'"
		SQL = SQL & " AND (A.UPCCode IS NULL OR A.UPCCode = '')))"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			file.WriteLine "Manufacturer" & vbtab & "MFR Part#" & vbtab & "UPC" & vbtab & "DistributorPart#" & vbtab & "ProductTitle" & vbtab & "ProductDescriptionShort" & vbtab & "ProductDescriptionLong" & vbtab & "ProductCondition" & vbtab & "Price" & vbtab & "ShipCost" & vbtab & "StockStatus" & vbtab & "ProductURL" & vbtab & "ImageURL" & vbtab & "Category"
			do until RS.eof
				DoNotInclude = 0
				if RS("inv_qty") < 0 then
					SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
					set RS2 = Server.CreateObject("ADODB.Recordset")
					RS2.open SQL, oConn, 3, 3
					if RS2.eof then DoNotInclude = 1
					RS2.close
					set RS2 = nothing
				end if
				if DoNotInclude = 0 then
					select case RS("typeid")
						case 1 : strCategory = "Electronics:Wireless Phones & Accessories>Batteries"
						case 2 : strCategory = "Electronics:Wireless Phones & Accessories>Chargers"
						case 3 : strCategory = "Electronics:Wireless Phones & Accessories>Face Plates & Carrying Cases"
						case 5 : strCategory = "Electronics:Wireless Phones & Accessories>Hands Free Kits"
						case 6 : strCategory = "Electronics:Wireless Phones & Accessories>Face Plates & Carrying Cases"
						case 7 : strCategory = "Electronics:Wireless Phones & Accessories>Face Plates & Carrying Cases"
						case else : strCategory = "Electronics:Wireless Phones & Accessories>Other Accessories"
					end select
					strline = RS("brandName") & vbtab
					strline = strline & RS("MPN") & vbtab
					strline = strline & RS("UPCCode") & vbtab
					strline = strline & vbtab
					strline = strline & chr(34) & RS("itemDesc") & chr(34) & vbtab
					strline = strline & chr(34) & RS("itemDesc") & chr(34) & vbtab
					strline = strline & chr(34) & removeHTML(replace(RS("itemLongDetail"),vbcrlf," ")) & chr(34) & vbtab
					strline = strline & "NEW" & vbtab
					strline = strline & formatCurrency(RS("price")) & vbtab
					strline = strline & "FREE" & vbtab
					strline = strline & "YES" & vbtab
					strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?id=pricegrabber" & vbtab
					strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab
					strline = strline & strCategory
					file.WriteLine strline
				end if
				RS.movenext
			loop
		end if
		RS.close
		set RS = nothing
		file.Close()
	end if
end sub

function removeHTML(str)
	removeHTML = replace(str,"<br>"," ",1,99,1)
	removeHTML = replace(replace(removeHTML,"<p>"," ",1,99,1),"</p>"," ",1,99,1)
	removeHTML = replace(replace(removeHTML,"<b>","",1,99,1),"</b>","",1,99,1)
	removeHTML = replace(removeHTML,"<b class='bigtext'>","",1,99,1)
	removeHTML = replace(replace(removeHTML,"<li>","",1,99,1),"</li>","",1,99,1)
	removeHTML = replace(replace(removeHTML,"<ul>","",1,99,1),"</ul>","",1,99,1)
	removeHTML = replace(replace(removeHTML,"<font color=""#FF0000"">","",1,99,1),"</font>","",1,99,1)
end function
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
