<%
	pageTitle = "Non Selling Items"
	header = 1
	
	dim fso,csvFile
	set fso = Server.CreateObject("Scripting.FileSystemObject")
	set csvFile = fso.CreateTextFile(server.MapPath("/admin/tempCSV/nonSelling.csv"))
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<script src="/Scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
<script src="/Scripts/jquery-ui-1.8.11.min.js" type="text/javascript"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	.fl { float:left; }
	.fr { float:right; }
	.mCenter { margin:0px auto; }
	.menuTitle { font-weight:bold; font-size:14px; margin-right:10px; padding-top:2px; width:150px; text-align:right; }
	.tb { display:table; }
	.selectMenu { width:500px; margin-bottom:5px; }
	.results { height:500px; width:560px; padding-left:14px; overflow:auto; }
</style>
<%
	dim startDate : startDate = prepStr(request.Form("startDate"))
	dim brandID : brandID = prepInt(request.Form("brandID"))
	dim modelID : modelID = prepInt(request.Form("modelID"))
	dim catID : catID = prepInt(request.Form("catID"))
	dim showVendor : showVendor = prepInt(request.Form("showVendor"))
	dim addDate : addDate = prepStr(request.Form("addDate"))
	
	if startDate = "" then startDate = DateAdd("m", -6, date)
	if addDate = "" then addDate = DateAdd("m", -2, date)
	
	sql = "exec sp_nonSellingItems '" & startDate & "'," & brandID & "," & modelID & "," & catID & "," & showVendor & ",'" & addDate & "'"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	sql = "exec sp_pullAllBrands"
	session("errorSQL") = sql
	set brandRS = oConn.execute(sql)
	
	sql = "exec sp_pullAllCategories"
	session("errorSQL") = sql
	set catRS = oConn.execute(sql)
%>
<form method="post" name="filterForm" action="/admin/nonSellingItems.asp">
<div class="tb mCenter selectMenu">
	<div class="fl menuTitle">Not Sold Since:</div>
    <div class="fl"><input type="text" id="startDate" name="startDate" value="<%=startDate%>" /></div>
</div>
<div class="tb mCenter selectMenu">
	<div class="fl menuTitle">Brand:</div>
    <div class="fl">
    	<select name="brandID" onchange="getModels(this.value)">
        	<option value="">Select Brand</option>
			<%
			do while not brandRS.EOF
				curBrandID = prepInt(brandRS("brandID"))
			%>
            <option value="<%=curBrandID%>"<% if brandID = curBrandID then %> selected="selected"<% end if %>><%=brandRS("brandName")%></option>
            <%
				brandRS.movenext
			loop
			%>
        </select>
    </div>
</div>
<div id="modelCell" class="tb mCenter selectMenu">
	<div class="fl menuTitle">Model:</div>
    <div class="fl">
    	<select name="modelID">
        	<option value="">You must select a brand first</option>
        </select>
    </div>
</div>
<div class="tb mCenter selectMenu">
	<div class="fl menuTitle">Category:</div>
    <div class="fl">
    	<select name="catID">
        	<option value="">Select Category</option>
        	<%
			do while not catRS.EOF
				curCatID = prepInt(catRS("typeID"))
			%>
            <option value="<%=curCatIT%>"<% if catID = curCatID then %> selected="selected"<% end if %>><%=catRS("typeName")%></option>
            <%
				catRS.movenext
			loop
			%>
        </select>
    </div>
</div>
<div class="tb mCenter selectMenu">
	<div class="fl menuTitle">Show Vendors:</div>
    <div class="fl"><input type="checkbox" name="showVendor" value="1"<% if showVendor = 1 then %> checked="checked"<% end if %> /></div>
</div>
<div class="tb mCenter selectMenu" style="display:none;">
	<div class="fl menuTitle">Added Before:</div>
    <div class="fl"><input type="text" id="addDate" name="addDate" value="<%=addDate%>" /></div>
</div>
<div class="tb mCenter selectMenu">
	<div class="fl" style="margin:0px 15px 0px 70px;"><input type="button" name="myAction" value="Export to CSV" onclick="window.open('/admin/tempCSV/nonSelling.csv')" /></div>
	<div class="fl"><input type="submit" name="myAction" value="Filter Products" /></div>
</div>
</form>
<div style="height:40px; width:540px; margin-left:auto; margin-right:auto; background-color:#000; font-weight:bold; color:#FFF; font-size:12px;">
    <div style="float:left; width:200px; text-align:left;">Partnumber</div>
    <div style="float:left; width:70px; text-align:center;">COGS</div>
    <div style="float:left; width:80px; text-align:center;">Sale Price</div>
    <div style="float:left; width:130px; text-align:center;">Vendor</div>
    <div style="float:left; width:50px; text-align:center;">InvQty</div>
</div>
<div class="mCenter results">
	<%
        writeData = "Partnumber,Cogs,SalesPrice,Vendor,InvQty"
		csvFile.WriteLine(writeData)
		bgColor = "#fff"
        prodLap = 0
        prefixList = ""
        vendor = ""
        do while not rs.EOF
            vendorList = ""
			writeData = ""
            partNumber = rs("partNumber")
            cogs = rs("cogs")
            price_our = rs("price_our")
            lastOrder = prepStr(rs("orderdatetime"))
            invQty = prepInt(rs("inv_qty"))
			prodEntryDate = prepStr(rs("DateTimeEntd"))
            if lastOrder <> "" then lastOrder = dateOnly(lastOrder)
			if prodEntryDate <> "" then prodEntryDate = "Product Added: " & prodEntryDate else prodEntryDate = "Unknown Add Date"
            prodLap = prodLap + 1
            
            do while partNumber = rs("partNumber")
                if showVendor = 1 then
                    vendor = rs("vendor")
                    curVendorCode = prepStr(rs("vendorCode"))
                end if
                if curVendorCode <> "" and instr(vendorList,curVendorCode & ",") < 1 then vendorList = vendorList & curVendorCode & ","
                rs.movenext
                if rs.EOF then exit do
            loop
            if vendorList = "" then vendorList = vendor else vendorList = left(vendorList,len(vendorList)-1)
    %>
    <div style="height:20px; width:540px; background-color:<%=bgColor%>; margin-left:auto; margin-right:auto;" id="productRow_<%=prodLap%>">
        <div title="<%=prodEntryDate%>" style="float:left; width:200px; text-align:left; border-right:1px solid #000; height:20px;"><%=partNumber%><% if lastOrder <> "" then %> (<%=lastOrder%>)<% end if %></div>
        <div style="float:left; width:60px; padding-right:10px; text-align:right; border-right:1px solid #000; height:20px;"><%=formatCurrency(cogs,2)%></div>
        <div style="float:left; width:70px; padding-right:10px; text-align:right; border-right:1px solid #000; height:20px;"><%=formatCurrency(price_our,2)%></div>
        <div style="float:left; width:120px; padding-left:10px; text-align:left; border-right:1px solid #000; height:20px;"><%=vendorList%></div>
        <div style="float:left; width:50px; text-align:right; height:20px;"><%=invQty%></div>
    </div>
    <%
            writeData = partNumber & "," & cogs & "," & price_our & "," & vendorList & "," & invQty
			if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
			csvFile.WriteLine(writeData)
        loop
        if prodLap = 0 then
    %>
    <div style="height:20px; width:550px; background-color:<%=bgColor%>; margin-left:auto; margin-right:auto;">No data to display</div>
    <%
        end if
    %>
</div>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function getModels(brandID) {
		ajax('/ajax/admin/ajaxNonSellingItems.asp?brandID=' + brandID + '&modelID=<%=modelID%>','modelCell')
	}
	<% if brandID > 0 then %>
	getModels(<%=brandID%>)
	<% end if %>
	
	$(function () {
        $("#startDate").datepicker();
		$("#addDate").datepicker();
    });
</script>