<%
pageTitle = "WE Admin Site - Questionable Orders Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">

<%
'response.buffer = false

if request("submitted") <> "" then
	'Response.ContentType = "application/vnd.ms-excel"
	
	strError = ""
	dateStart = request("dateStart")
	dateEnd = request("dateEnd")
	approved = request("approved")
	email = request("email")
	confirm = request("confirm")
	ordernotes = request("ordernotes")
	testorders = request("testorders")
	
	if not isDate(dateStart) or not isDate(dateEnd) then
		strError = strError & "<p>dateStart and dateEnd must both be valid dates</p>"
	else
		dateStart = dateValue(dateStart)
		dateEnd = dateValue(dateEnd)
		if dateEnd < dateStart then strError = strError & "<p>dateEnd must be later than or equal to dateStart</p>"
	end if
	
	showblank = "&nbsp;"
	shownull = "-null-"
	
	if strError = "" then
		dateEnd = dateAdd("D",1,dateEnd)
		SQL = "SELECT A.orderid, B.fname, B.lname, A.ordergrandtotal AS [order grandtotal], A.shiptype, A.orderdatetime,"
		SQL = SQL & " A.approved, A.emailSent, A.confirmSent,"
		SQL = SQL & " C.ordernotes"
		SQL = SQL & " FROM (we_Orders A INNER JOIN we_Accounts B ON A.accountID = B.accountID)"
		SQL = SQL & " LEFT JOIN we_ordernotes C ON A.orderid=C.orderid"
		SQL = SQL & " WHERE A.orderdatetime >= '" & dateStart & "' AND A.orderdatetime <= '" & dateEnd & "'"
		SQL = SQL & " AND A.store = 0"
		if approved = "yes" then
			SQL = SQL & " AND A.approved = 1"
		elseif approved = "no" then
			SQL = SQL & " AND (A.approved IS null OR A.approved = 0)"
		end if
		if email = "yes" then
			SQL = SQL & " AND A.emailSent = 'yes'"
		elseif email = "no" then
			SQL = SQL & " AND A.emailSent IS null"
		end if
		if confirm = "yes" then
			SQL = SQL & " AND A.confirmSent = 'yes'"
		elseif confirm = "no" then
			SQL = SQL & " AND A.confirmSent IS null"
		end if
		if ordernotes = "yes" then
			SQL = SQL & " AND C.ordernotes IS NOT null"
		elseif ordernotes = "no" then
			SQL = SQL & " AND C.ordernotes IS null"
		end if
		if testorders = "no" then
			SQL = SQL & " AND B.fname NOT LIKE '%TEST%'"
		end if
		SQL = SQL & " ORDER BY A.orderid DESC"
		set RS = Server.CreateObject("ADODB.Recordset")
		response.write "<h3>" & SQL & "</h3>" & vbCrLf
		'response.end
		RS.open SQL, oConn, 3, 3
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			response.write "<h3>" & RS.recordcount & " TOTAL records found</h3>" & vbCrLf
			%>
			<table border="1">
				<tr>
					<%
					for each whatever in RS.fields
						%>
						<td><b><%=whatever.name%></b></td>
						<%
					next
					%>
				</tr>
				<%
				do until RS.eof
					%>
					<tr>
						<%
						for each whatever in RS.fields
							thisfield = whatever.value
							if isnull(thisfield) then
								thisfield = shownull
							end if
							if trim(thisfield) = "" then
								thisfield = showblank
							end if
							%>
							<td valign="top"><%=thisfield%></td>
							<%
						next
						%>
					</tr>
					<%
					RS.movenext
				loop
				%>
			</table>
			<%
		end if
		
		RS.close
		Set RS = nothing
	else
		%>
		<center>
		<font size="+2" color="red"><%=strError%></font>
		<a href="javascript:history.back();">BACK</a>
		</center>
		<%
	end if
else
	%>
	<p>&nbsp;</p>
	<h3>Questionable Orders Report</h3>
	<br>
	<form action="report_Questionable_Orders.asp" method="post">
		<p><input type="text" name="dateStart">&nbsp;&nbsp;dateStart</p>
		<p><input type="text" name="dateEnd">&nbsp;&nbsp;dateEnd</p>
		<p>approved:&nbsp;&nbsp;<input type="radio" name="approved" value="yes">yes&nbsp;&nbsp;<input type="radio" name="approved" value="no">no&nbsp;&nbsp;<input type="radio" name="approved" value="di" checked>don't&nbsp;consider&nbsp;&nbsp;</p>
		<p>emailSent:&nbsp;&nbsp;<input type="radio" name="email" value="yes">yes&nbsp;&nbsp;<input type="radio" name="email" value="no">no&nbsp;&nbsp;<input type="radio" name="email" value="di" checked>don't&nbsp;consider&nbsp;&nbsp;</p>
		<p>confirmSent:&nbsp;&nbsp;<input type="radio" name="confirm" value="yes">yes&nbsp;&nbsp;<input type="radio" name="confirm" value="no">no&nbsp;&nbsp;<input type="radio" name="confirm" value="di" checked>don't&nbsp;consider&nbsp;&nbsp;</p>
		<p>ordernotes:&nbsp;&nbsp;<input type="radio" name="ordernotes" value="yes">yes&nbsp;&nbsp;<input type="radio" name="ordernotes" value="no">no&nbsp;&nbsp;<input type="radio" name="ordernotes" value="di" checked>don't&nbsp;consider&nbsp;&nbsp;</p>
		<p>"TEST" orders:&nbsp;&nbsp;<input type="radio" name="testorders" value="yes">include&nbsp;&nbsp;<input type="radio" name="testorders" value="no" checked>don't&nbsp;include&nbsp;&nbsp;</p>
		<p><input type="submit" name="submitted" value="Generate Report"></p>
	</form>
	<%
end if
%>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
