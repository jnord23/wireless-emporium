<%
pageTitle = "WE Admin Site - Coupon Statistics"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">

<%
function nullBlank(str)
	if isnull(str) then
		nullBlank = "-null-"
	elseif trim(str) = "" then
		nullBlank = "&nbsp;"
	else
		nullBlank = str
	end if
end function

if request.querystring("couponid") <> "" then
	couponid = request.querystring("couponid")
	dateStart = request.querystring("dateStart")
	dateEnd = request.querystring("dateEnd")
	SQL = "SELECT * FROM we_Orders"
	SQL = SQL & " WHERE orderdatetime >= '" & dateStart & "' AND orderdatetime < '" & dateAdd("D",1,dateEnd) & "'"
	SQL = SQL & " AND couponID = '" & couponid & "'"
	SQL = SQL & " AND approved = 1 and cancelled is null"
	SQL = SQL & " ORDER BY orderdatetime DESC"
	session("errorSQL") = sql
	Set RS = oConn.execute(sql)
	if RS.eof then
		response.write "No records matched<br><br>So cannot make table..."
	else
		response.write "<h3>COUPON DETAILS FOR: " & request.querystring("promocode") & "&nbsp;&#150;&nbsp;" & dateStart & " to " & dateEnd & "</h3>"
		%>
		<table border="1">
			<tr>
				<td align="center"><b>orderID</b></td>
				<td align="left"><b>orderDateTime</b></td>
				<td align="center"><b>orderSubTotal</b></td>
				<td align="center"><b>orderGrandTotal</b></td>
			</tr>
			<%
			do until RS.eof
				%>
				<tr>
					<td valign="top" align="center"><%=nullBlank(RS("orderID"))%></td>
					<td valign="top" align="left"><%=nullBlank(RS("orderDateTime"))%></td>
					<td valign="top" align="center"><%=formatCurrency(RS("orderSubTotal"))%></td>
					<td valign="top" align="center"><%=formatCurrency(RS("orderGrandTotal"))%></td>
				</tr>
				<%
				RS.movenext
			loop
			%>
		</table>
		<%
	end if
	
	RS.close
	Set RS = nothing
elseif request.querystring("submitted") <> "" then
	dateStart = request.querystring("dateStart")
	dateEnd = request.querystring("dateEnd")
	couponCode = prepStr(request.querystring("couponCode"))
	couponType = prepStr(request.querystring("couponType"))
	if not isDate(dateStart) then dateStart = date()
	if not isDate(dateEnd) then dateEnd = date()
	dateStart = dateValue(dateStart)
	dateEnd = dateValue(dateEnd)
	if strError = "" then
		if couponCode <> "" then
			SQL = "select sum(cast(b.ordersubtotal as money)) as subtotal, count(*) as orderCnt, a.couponid, a.typeID, a.promoCode, a.couponDesc, a.expiration from we_coupons a join we_orders b on a.couponid = b.couponid where a.promoCode = '" & couponCode & "' and b.orderdatetime >= '" & dateStart & "' AND b.orderdatetime < '" & dateAdd("D",1,dateEnd) & "' and approved = 1 and cancelled is null group by a.couponid, a.typeID, a.promoCode, a.couponDesc, a.expiration"
		else
			SQL = "select sum(cast(b.ordersubtotal as money)) as subtotal, count(*) as orderCnt, a.couponid, a.typeID, a.promoCode, a.couponDesc, a.expiration from we_coupons a join we_orders b on a.couponid = b.couponid where b.orderdatetime >= '" & dateStart & "' AND b.orderdatetime < '" & dateAdd("D",1,dateEnd) & "' and approved = 1 and cancelled is null group by a.couponid, a.typeID, a.promoCode, a.couponDesc, a.expiration"
		end if
		session("errorSQL") = sql
		set RS = oConn.execute(sql)
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			response.write "<h3>COUPON STATISTICS FOR: " & dateStart & " to " & dateEnd & "</h3>"
			%>
			<table border="1">
				<tr>
					<td align="center"><b>couponid</b></td>
					<td align="left"><b>type</b></td>
					<td align="left"><b>promoCode</b></td>
					<td align="left"><b>couponDesc</b></td>
					<td align="left"><b>expiration</b></td>
					<td align="center"><b>#&nbsp;Used</b></td>
					<td align="center"><b>Discounted<br>Subtotal</b></td>
				</tr>
				<%
				do until RS.eof
					subTotal = prepInt(RS("subtotal"))
					totalCnt = prepInt(RS("orderCnt"))
					couponid = prepInt(RS("couponid"))
					typeID = prepInt(RS("typeID"))
					promoCode = prepStr(RS("promoCode"))
					couponDesc = prepStr(RS("couponDesc"))
					expiration = prepStr(RS("expiration"))
				%>
				<tr>
                    <td valign="top" align="center"><a href="<%="report_CouponStats.asp?couponid=" & couponid & "&promocode=" & promoCode & "&dateStart=" & dateStart & "&dateEnd=" & dateEnd%>"><%=couponid%></a></td>
                    <td valign="top" align="left"><%=typeID%></td>
                    <td valign="top" align="left"><%=promoCode%></td>
                    <td valign="top" align="left"><%=couponDesc%></td>
                    <td valign="top" align="left"><%=expiration%></td>
                    <td valign="top" align="center"><%=totalCnt%></td>
                    <td valign="top" align="center"><%=formatCurrency(subTotal,2)%></td>
                </tr>
				<%
					RS.movenext
				loop
				%>
			</table>
			<%
		end if
		
		RS.close
		Set RS = nothing
	else
		%>
		<center>
		<font size="+2" color="red"><%=strError%></font>
		<a href="javascript:history.back();">BACK</a>
		</center>
		<%
	end if
end if
%>
<p>&nbsp;</p>
<h3>Choose another date range:</h3>
<br>
<form action="report_CouponStats.asp" method="get">
	<p><input type="text" name="dateStart" value="<%=dateStart%>">&nbsp;&nbsp;Start&nbsp;Date</p>
	<p><input type="text" name="dateEnd" value="<%=dateEnd%>">&nbsp;&nbsp;End&nbsp;Date</p>
    <p><input type="text" name="couponCode" value="<%=couponCode%>">&nbsp;&nbsp;Coupon Code (optional)</p>
    <p><input type="submit" name="submitted" value="Generate Report"></p>
</form>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
