<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Amazon Cogs Report"
	header = 1
	
	hidExcel = request("hidExcel")
	if hidExcel = "Y" then
		response.redirect "/admin/report_amazon_cogs_excel.asp?" & request.QueryString
	end if
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/admin/admin_top.asp"-->
<style>
.mouseover	{ background-color:#9BCEFF;}
</style>
<!-- JSON Autocomplete -->
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<style>
	.ui-autocomplete-loading { background: white url('/images/preloading16x16.gif') right center no-repeat; }
</style>
<script>
	$(document).ready(function() {
		
		$("#id_search").autocomplete({
			source: "/ajax/ajaxAutocompleteFilter.asp?sType=amazon_cogs",
			minLength: 3,
			select: function( event, ui ) {pullData(ui);}
		});
	});
</script>
<script>
	function pullData(ui)
	{
		var partnumber = '';
		if(ui.item)
		{
			partnumber = ui.item.id
			
			if (partnumber != "")
			{
				document.getElementById('id_current').innerHTML = '<img src="/images/loading.gif" border="0" />'
				ajax('/ajax/admin/ajaxAmazonCogs.asp?partnumber=' + partnumber, 'id_current');
			}
		}
	}
	function doSubmitExcel()
	{
		document.frmReport.hidExcel.value = 'Y';
		return true;
	}	
</script>
<!-- //JSON Autocomplete -->
<%
sql	=	"update	a" & vbcrlf & _
		"set	partnumber_we = coalesce(b.partnumber, c.partnumber, d.musicSkinsID)" & vbcrlf & _
		"from	we_amazonProducts a left outer join we_items b" & vbcrlf & _
		"	on	a.partnumber = b.partnumber left outer join we_items c" & vbcrlf & _
		"	on	left(a.partnumber, 15) = c.partnumber or left(a.partnumber, 14) = c.partnumber left outer join we_items_musicskins d" & vbcrlf & _
		"	on	a.partnumber = d.musicSkinsID" & vbcrlf & _
		"where	a.partnumber_we is null	"
session("errorSQL") = sql		
oConn.execute(sql)		

sql	=	"select	distinct a.partnumber, a.partnumber_we" & vbcrlf & _
		"	,	coalesce(b.cogs, c.cogs) cogs" & vbcrlf & _
		"	,	case when b.partnumber is not null then b.inv_qty" & vbcrlf & _
		"			when c.musicSkinsID is not null then 999" & vbcrlf & _
		"			else null" & vbcrlf & _
		"		end inv_qty" & vbcrlf & _
		"	,	coalesce(b.price_our, c.price_we) price_we" & vbcrlf & _
		"from	we_amazonProducts a left outer join we_items b" & vbcrlf & _
		"	on	a.partnumber_we = b.partnumber and b.master = 1 left outer join we_items_musicskins c" & vbcrlf & _
		"	on	a.partnumber_we = c.musicSkinsID and c.deleteItem = 0 and c.skip = 0" & vbcrlf & _
		"order by 2 desc, 1"
session("errorSQL") = sql		
arrCogs = getDbRows(sql)
%>
<form name="frmReport" method="get">
<input type="hidden" name="hidExcel" value="" />
<table border="0" cellpadding="0" cellspacing="0" align="center" width="800">
	<tr>
    	<td><h1 style="margin:0px;">Amazon Product Cogs Report</h1></td>
    </tr>
	<tr>
    	<td style="border-bottom:1px dashed #ccc;">
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                <tr>
                	<td align="left">
						Search: <input type="text" id="id_search" name="txtSearch" value="Search By PartNumber" onclick="this.value='';" style="color:#666; width:300px;" />                    
                    </td>
                    <td align="right">
	                    Export to excel <input type="image" name="submitExcel" src="/images/icon-excel-small.gif" onclick="return doSubmitExcel();" />
                    </td>
				</tr>
			</table>
		</td>
    </tr>
    <tr>
    	<td width="100%" align="left">
            <div style="border-bottom:2px solid #E95516;">        
            <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="font-size:13px; padding:5px;">
                <tr>
                    <td align="right" width="245" style="font-weight:bold; font-size:14px;">Amazon Partnumber</td>
                    <td align="right" width="175" style="font-weight:bold; font-size:14px;">WE Partnumber</td>
                    <td align="right" width="120" style="font-weight:bold; font-size:14px;">COGS</td>
                    <td align="right" width="130" style="font-weight:bold; font-size:14px;">QTY on Hand</td>
                    <td align="right" width="120" style="font-weight:bold; font-size:14px; padding-right:30px;">Price WE</td>
				</tr>
			</table>
            </div>
            <div id="id_current" style="width:800px; height:600px; overflow:scroll;">
                <table border="0" cellpadding="3" cellspacing="0" align="center" width="100%" style="font-size:13px;">
				<%
                for i=0 to ubound(arrCogs, 2)
                    if isnull(arrCogs(1, i)) then
                    %>
                    <tr class="grid_row_<%=(i mod 2)%>">
                        <td align="right" width="245"><%=arrCogs(0,i)%></td>
                        <td align="right" width="175">N/A</td>
                        <td align="right" width="120">N/A</td>
                        <td align="right" width="130">N/A</td>
                        <td align="right" width="120" style="padding-right:20px;">N/A</td>
                    </tr>                        
					<%
                    else
                    %>
                    <tr class="grid_row_<%=(i mod 2)%>">
                        <td align="right" width="245"><%=replace(arrCogs(0,i), "/", " /<br />")%></td>
                        <td align="right" width="175"><%=arrCogs(1,i)%></td>
                        <td align="right" width="120">
						<%
						if not isnull(arrCogs(2,i)) and isnumeric(arrCogs(2,i)) then
							response.write formatcurrency(arrCogs(2,i))
						else
							response.write arrCogs(2,i)
						end if
						%>
						</td>
                        <td align="right" width="130">
						<%
						if not isnull(arrCogs(3,i)) and isnumeric(arrCogs(3,i)) then
							response.write formatnumber(arrCogs(3,i),0)
						else
							response.write arrCogs(3,i)
						end if
						%>
                        </td>
                        <td align="right" width="120" style="padding-right:20px;">
						<%
						if not isnull(arrCogs(4,i)) and isnumeric(arrCogs(4,i)) then
							response.write formatcurrency(arrCogs(4,i))
						else
							response.write arrCogs(4,i)
						end if
						%>
						</td>
                    </tr>                        
					<%
                    end if
                next
                %>
                </table>
            </div>
		</td>
    </tr>
</table>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->