<%
pageTitle = "Admin - Update WE Database - Update Coupons"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table width="810" border="0" bordercolor="blue" cellspacing="0" cellpadding="8" class="smlText">
	<tr>
		<td class="bigText">
			<strong>Inbound Call Protocall:</strong>
		</td>
	</tr>
	<tr>
		<td>
			<span class="smlText">
			<a name="Welcome"></a>
			<strong>Official Answer :</strong>
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;Thank you for calling Wireless Emporium where we offer the best TOTAL prices online every day. How may I assist you?
			<br><br>
			<strong>Typical Answer to commence customer service call :</strong> <br>&nbsp;&nbsp;&nbsp;&nbsp;We can certainly help you with that - what is your order id #?
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="/admin/custlist.asp" target="_blank">Customer Lookup</a>
			<br><br>
			<strong>Typical Answer to commence customer service call :</strong> <br>&nbsp;&nbsp;&nbsp;&nbsp;We can certainly help you with that - what are you interested in?
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.wirelessemporium.com/" target="_blank">Order Now</a>
			<br><br><br>
			<p><strong><u>Top 10 Frequently Asked Questions.</u></strong></p>
			<ol>
				<li><a href="#How do I check" class="faqtext">How do I check the status of my order / When will my order arrive?</a><br><br></li>
				<li><a href="#Can my" class="faqtext">Is there any shipment tracking?</a><br><br></li>
				<li><a href="#I have" class="faqtext">I have waited the specified delivery times and my order has still not arrived � what do I do?</a><br><br></li>
				<li><a href="#I changed" class="faqtext">I changed my mind and would like to cancel my order � can I do this?</a><br><br></li>
				<li><a href="#Internationally" class="faqtext">Do you ship internationally?</a><br><br></li>
				<li><a href="#RETURNS" class="faqtext">RETURNS/EXCHANGES � What do I do? What is your policy?</a><br><br></li>
				<li><a href="#I believe" class="faqtext">I believe you sent me the wrong item � what do I do?</a><br><br></li>
				<li><a href="#Status" class="faqtext">What is the status of my return/exchange/refund?</a><br><br></li>
				<li><a href="#Warranty" class="faqtext">What type of warranty do we have on our product?</a><br><br></li>
				<li><a href="#Amazon" class="faqtext">I Purchased through Amazon.com/SHOP.com/Buy.com or a 3rd party Partner and have an inquiry.</a><br><br></li>
			</ol>
			<br>
			<strong>Do you have a question not answered here? Email us at:</strong>
			<a href="mailto:service@wirelessemporium.com">service@wirelessemporium.com</a>.
			<br><br><br>
			<ol style="margin-top:0px;">
				<li>
					<a name="How do I check"><strong>How do I check the status of my order / When will my order arrive?</strong></a>
					<br>
					Depending on shipping method selection (First Class/Priority/Overnight). All orders are fulfilled within 1-2 business days. <br>
					First class mail should arrive anywhere from 3-8 business days; Priority anywhere from 2-4 business days; Overnight 1-2 business days
					(and requires physical signoff).
					<br><br>
					<a href="#Welcome"><i>Back to top</i></a>
					<br><br>
				</li>
				<li>
					<a name="Can my"><strong>Is there any shipment tracking?</strong></a>
					<br>
					Shipment Tracking is currently only provided for Priority and Express shipments.
					(To obtain information you must check the Dazzle shipping logs.)
					<br><br>
					<a href="#Welcome"><i>Back to top</i></a>
					<br><br>
				</li>
				<li>
					<a name="I have"><strong>I have waited the specified delivery times and my order has still not arrived � what do I do?</strong></a>
					<br>
					If customer has not received their order in the projected timeframe, up to the 10th business day for First Class Mail,
					have the customer confirm the shipping address and process a reshipment of order.
					<br><br>
					<a href="#Welcome"><i>Back to top</i></a>
					<br><br>
				</li>
				<li>
					<a name="I changed"><strong>I changed my mind and would like to cancel my order � can I do this?</strong></a>
					<br>
					Order can be cancelled, but might be difficult to stop. This is a case-by-case scenario.
					Collect the customer's callback information and give them a call once the cancellation has been confirmed.
					If the package cannot be cancelled, provide them with information on how to return the unopened package as a "Return to Sender" package. 
					<br><br>
					<a href="#Welcome"><i>Back to top</i></a>
					<br><br>
				</li>
				<li>
					<a name="Internationally"><strong>Do you ship internationally?</strong></a>
					<br>
					At this time Wireless Emporium only ships within the 50 United States, its territories and armed services, and Canada.
					<br><br>
					<a href="#Welcome"><i>Back to top</i></a>
					<br><br>
				</li>
				<li>
					<a name="RETURNS"><strong>RETURNS/EXCHANGES � What do I do? What is your policy?</strong></a>
					<br>
					We're sorry to hear you're having problems. We can either replace the unit under warranty
					for FREE or we can refund your money for the purchase price.
					<br>
					<ol style="list-style-type: upper-alpha;">
						<li>
							<br>
							If EXCHANGE:<br>
							Simply return the item to us in its original packaging along with a copy of the original
							sales receipt stating the reason for return and instructions for "warranty exchange" and
							we will ship out a new unit expeditiously. You can send the package to:
							<br>Wireless Emporium
							<br>Attn: Returns
							<br>1410 N. Batavia St.
							<br>Orange, CA 92867
							<br><br>
						</li>
						<li>
							If REFUND:<br>
							If it is WITHIN 30 days OF RECEIPT, simply return the item to us in its original packaging
							along with a copy of the original sales receipt stating the reason for return and
							instructions for refund. Refund processing takes approx. 2 weeks. You can send the package to:
							<br>Wireless Emporium
							<br>Attn: Returns
							<br>1410 N. Batavia St.
							<br>Orange, CA 92867
							<br><br>
						</li>
					</ol>
					* CUSTOMER ALWAYS PAYS RETURN COST UNLESS IT IS CLEARLY OUR ERROR.
					<br><br>
					** IF THE ORDER IS FROM WE* START THE RMA PROCESS AND INFORM CUSTOMER THEY WILL ALSO RECEIVE A DETAILED E-MAIL.
					<br><br>
					<a href="#Welcome"><i>Back to top</i></a>
					<br><br>
				</li>
				<li>
					<a name="#I believe"><strong>I believe you sent me the wrong item � what do I do?</strong></a>
					<br>
					Obtain information about the product the customer received, i.e. part number or model number of the product they actually received.
					Pull the correct item and compare it to what they received. If it is the wrong item, process a reshipment.
					If they have received the correct item, explain to them that many accessories are cross-compatible.
					<br><br>
					<a href="#Welcome"><i>Back to top</i></a>
					<br><br>
				</li>
				<li>
					<a name="#Status"><strong>What is the status of my return/exchange/refund?</strong></a>
					<br>
					Status can be found in customer notes section here:
					<br>
					<a href="/admin/custlist.asp" target="_blank">Customer Lookup</a>
					<br><br>
					<a href="#Welcome"><i>Back to top</i></a>
					<br><br>
				</li>
				<li>
					<a name="Warranty"><strong>What type of warranty do we have on our product?</strong></a>
					<br>
					All products are warranted for 1 year unless specified otherwise. Our warranty does not cover abuse,
					neglect, or improper installation of product, or acts of nature. PLEASE NOTE: Installation of any
					of our products is at your own risk. Wireless Emporium is not liable for any damages caused by our
					products. Damage(s) due to what Wireless Emporium deems customer negligence is not covered under
					this warranty.
					<br><br>
					<a href="#Welcome"><i>Back to top</i></a>
					<br><br>
				</li>
				<li>
					<a name="Amazon"><strong>I Purchased through Amazon.com/SHOP.com/Buy.com or a 3rd party Partner and have an inquiry.</strong></a>
					<br>
					&lt;forward the inquiry to: <a href="mailto:service@wirelessemporium.com">service@wirelessemporium.com</a>&gt;.
					<br><br>
					<a href="#Welcome"><i>Back to top</i></a>
				</li>
			</ol>
			</span>
		</td>
	</tr>
</table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
