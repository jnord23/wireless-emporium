<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Receive Orders"
	header = 1
%>
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	set fso = CreateObject("Scripting.FileSystemObject")
	
	orderID = prepStr(request.QueryString("orderID"))
	completeOrder = prepInt(request.Form("completeOrder"))
	itemCnt = prepInt(request.Form("itemCnt"))
	vendorName = prepStr(request.Form("vendorName"))
	zeroOut = prepInt(request.QueryString("zeroOut"))
	session("curOrder") = "WEPN"
	
	userMsg = ""
	
	sql = "Select distinct a.orderID, a.vendorCode, a.processOrderDate, b.name from we_vendorOrder a left join we_vendors b on a.vendorCode = b.code where a.process = 1 and a.complete = 1 order by b.name, a.processOrderDate desc"
	session("errorSQL") = sql
	Set vendorRS = oConn.execute(sql)
	
	sql = 	"exec sp_vendorOrderDetails '" & orderID & "',1,''"
	session("vendorSQL") = sql
	session("errorSQL") = sql
	Set rs = oConn.execute(sql)
%>
<style type="text/css">
	.dateStamp { margin-right:10px; padding-right:10px; border-right:1px solid #000; padding-top:5px; height:20px; }
	.dateStampButton { margin-right:10px; padding-right:10px; border-right:1px solid #000; height:25px; }
	
	.dateStamp2 { margin-right:10px; padding-right:10px; padding-top:5px; height:20px; }
	.dateStampButton2 { margin-right:10px; padding-right:10px; height:25px; }
</style>
<table align="center" border="0" cellpadding="3" cellspacing="0">
	<% if userMsg <> "" then %>
    <tr><td align="center" style="font-weight:bold; color:#F00; border-bottom:1px solid #F00;"><%=userMsg%></td></tr>
    <% end if %>
    <tr>
    	<td align="center" style="padding:20px 0px 20px 0px;">
            <table border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td style="font-weight:bold; font-size:14px;" valign="top">Select Order to Receive:</td>
                    <td valign="top">
                    	<% if vendorRS.EOF then %>
                        No orders currently waiting to be received
                        <% else %>
                    	<select name="orderID" onchange="window.location='/admin/vendorAdjustOrders.asp?orderID=' + this.value">
                        	<option value="">Select Vendor</option>
                            <%
							do while not vendorRS.EOF
								if instr(vendorRS("processOrderDate")," ") > 0 then
									showDate = left(vendorRS("processOrderDate"),instr(vendorRS("processOrderDate")," ") - 1)
								else
									showDate = vendorRS("processOrderDate")
								end if
							%>
                            <option value="<%=vendorRS("orderID")%>"<% if orderID = vendorRS("orderID") then %> selected="selected"<% end if %>><%=vendorRS("name")%> - <%=showDate%></option>
                            <%
								vendorRS.movenext
							loop
							%>
                        </select>
                        <% end if %>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <%
	if not rs.EOF then
		vendorName = rs("vendor")
		arrivalDate = prepStr(rs("arrivalDate"))
		breakdownDate = prepStr(rs("breakdownDate"))
		breakdownCompleteDate = prepStr(rs("breakdownCompleteDate"))
		restockDate = prepStr(rs("restockDate"))
		restockCompleteDate = prepStr(rs("restockCompleteDate"))
	%>
    <tr>
    	<td id="orderDetails">
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0">
            	<tr>
                	<td colspan="8" align="center" style="font-size:34px; font-weight:bold;">
                    	<div style="float:left;">Order For <%=vendorName%></div>
                    </td>
                </tr>
                <tr>
                	<td colspan="8">
                    	<% if arrivalDate = "" then %>
                        <div id="arrivalDate" class="fl dateStampButton"><input type="button" name="myAction" value="Shipment Arrived" onclick="saveDate('arrivalDate','<%=date%>')" /></div>
                        <% else %>
                        <div id="arrivalDate" class="fl dateStamp">Arrival: <%=arrivalDate%></div>
                        <% end if %>
                        <% if breakdownDate = "" then %>
                        <div id="breakdownDate" class="fl dateStampButton"><input type="button" name="myAction" value="Start Break-Down" onclick="saveDate('breakdownDate','<%=date%>')" /></div>
                        <% else %>
                        <div id="breakdownDate" class="fl dateStamp">Breakdown Start: <%=breakdownDate%></div>
                        <% end if %>
                        <% if breakdownCompleteDate = "" then %>
                        <div id="breakdownCompleteDate" class="fl dateStampButton"><input type="button" name="myAction" value="Finish Break-Down" onclick="saveDate('breakdownCompleteDate','<%=date%>')" /></div>
                        <% else %>
                        <div id="breakdownCompleteDate" class="fl dateStamp">Breakdown Finish: <%=breakdownCompleteDate%></div>
                        <% end if %>
                        <% if restockDate = "" then %>
                        <div id="restockDate" class="fl dateStampButton"><input type="button" name="myAction" value="Start Restock" onclick="saveDate('restockDate','<%=date%>')" /></div>
                        <% else %>
                        <div id="restockDate" class="fl dateStamp">Restocked Start: <%=restockDate%></div>
                        <% end if %>
                        <% if restockCompleteDate = "" then %>
                        <div id="restockCompleteDate" class="fl dateStampButton2"><input type="button" name="myAction" value="Finish Restock" onclick="saveDate('restockCompleteDate','<%=date%>')" /></div>
                        <% else %>
                        <div id="restockCompleteDate" class="fl dateStamp2">Restocked Finish: <%=restockCompleteDate%></div>
                        <% end if %>
                    </td>
                </tr>
                <tr style="font-weight:bold; color:#FFF; background-color:#333;">
                	<td>Image</td>
                	<td nowrap="nowrap" style="cursor:pointer;" onclick="sortBy('WEPN')">WE Partnumber</td>
                    <td nowrap="nowrap" style="cursor:pointer;" onclick="sortBy('DETAILS')">We Description</td>
                	<td nowrap="nowrap" style="cursor:pointer;" onclick="sortBy('VPN')">Vendor Partnumber</td>
                    <td align="center">Inventory</td>
                    <td align="center">Ordered</td>
                    <td align="center">Received</td>
                    <td nowrap="nowrap">Action</td>
                </tr>
                <%
				bgColor = "#ffffff"
				lap = 0
				saveText = "WE Partnumber,We Description,Vendor Partnumber,Inventory,Ordered,Received," & vbcrlf
				do while not rs.EOF
					lap = lap + 1					
					useSize = "11px"
					curBG = ""
					fontColor = "#000"
					if rs("rowProcessed") then
						useReceived = rs("receivedAmt")
						curBG = bgColor
						bgColor = "#666666"
						fontColor = "#fff"
					else
						useReceived = rs("orderAmt")
					end if
					
					saveText = saveText & rs("partnumber") & ","	'partnumber
					saveText = saveText & rs("itemDesc") & ","	'description
					saveText = saveText & rs("vendorPN") & ","	'vendor partnumber
					saveText = saveText & rs("inv_qty") & "," 'curInv
					saveText = saveText & rs("orderAmt") & ","	'qty ordered
					saveText = saveText & useReceived & ","	'fulfillment qty
					saveText = saveText & vbcrlf
					
					itemPic = rs("itemPic")
					partnumber = rs("partNumber")
				%>
                <form name="receiveForm_<%=lap%>" action="javascript:processRow(<%=lap%>)" method="post">
                <tr bgcolor="<%=bgColor%>" style="color:<%=fontColor%>;" id="purchaseRow_<%=lap%>">
                	<td><img src="/productPics/thumb/<%=itemPic%>" border="0" /></td>
                    <td align="left" style="font-size:<%=useSize%>;"><%=partnumber%></td>
                    <td align="left" style="font-size:<%=useSize%>;">
						<div class="fl" style="width:300px; margin-right:15px;"><%=rs("itemDesc")%></div>
                        <% if prepInt(rs("warehouseLocID")) > 0 then %>
                        <div class="fr"><img src="/images/icons/houseIcon.png" border="0" width="30" height="30" alt="This item has a warehouse location" /></div>
                        <% else %>
                        <div class="fr"><a href="javascript:addHousing(<%=lap%>,'<%=partnumber%>');"><img id="houseID_<%=lap%>" src="/images/icons/houseIcon_inactive.png" border="0" width="30" height="30" alt="This item does not have a warehouse location" /></a></div>
						<% end if %>
                    </td>
                	<td align="left">
                    	<input type="text" name="vpn_<%=lap%>" value="<%=rs("vendorPN")%>" onchange="updateVendorPN('<%=rs("vendorCode")%>','<%=rs("partNumber")%>',this.value)" style="font-size:<%=useSize%>;" />
                        <input type="hidden" name="partNumber_<%=lap%>" value="<%=rs("partNumber")%>" />
                        <input type="hidden" name="orderAmt_<%=lap%>" value="<%=rs("orderAmt")%>" />
                    </td>
                    <td align="center"><%=rs("inv_qty")%></td>
                    <td align="center" style="font-size:<%=useSize%>;"><%=rs("orderAmt")%></td>
                    <td align="center" id="enterReceiveAmt_<%=lap%>" style="font-size:<%=useSize%>;"><input type="text" name="receiveAmt_<%=lap%>" value="<%=useReceived%>" size="3" style="font-size:<%=useSize%>;" /></td>
                    <td id="actionBttn_<%=lap%>"><input type="submit" name="myAction" value="Update Qty" /></td>
                </tr>
                </form>
                <%
					rs.movenext
					if curBG <> "" then bgColor = curBG
					if bgColor = "#ffffff" then bgColor = "#f0f0f0" else bgColor = "#ffffff"
				loop
				
				filename = "receiveVendorOrder_" & formatSEO(vendorName) & "_" & replace(date,"/","") & ".csv"
				path = Server.MapPath("vendorOrders") & "\" & filename
				path = replace(path,"admin\","")
				session("errorSQL") = path
				set file = fso.CreateTextFile(path)
				
				file.WriteLine saveText
				file.close()
				fso = null
				%>
            </table>
        </td>
    </tr>
    <tr><td align="right"><a href="../vendorOrders/<%=filename%>">view csv</a></td></tr>
    <% end if %>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function updateVendorPN(vendor,partNumber,vendorVal) {
		//document.getElementById("testZone").innerHTML = '/ajax/admin/saveVendorNum.asp?vendorReceive=1&vendorCode=' + vendor + '&ourPN=' + partNumber + '&addVPN=' + vendorVal
		ajax('/ajax/admin/saveVendorNum.asp?vendorReceive=1&vendorCode=' + vendor + '&ourPN=' + partNumber + '&addVPN=' + vendorVal,'dumpZone')
	}
	function processRow(rowNum) {
		var partNumber = eval("document.receiveForm_" + rowNum + ".partNumber_" + rowNum + ".value")
		var rcvAmt = eval("document.receiveForm_" + rowNum + ".receiveAmt_" + rowNum + ".value")
		if (isNaN(rcvAmt)) {
			alert("You must enter a number received")
		}
		else {
			document.getElementById("purchaseRow_" + rowNum).style.backgroundColor = "#666666"
			document.getElementById("actionBttn_" + rowNum).innerHTML = '<span style="color:#FFF; font-weight:bold;">PROCESSED</span>'
			document.getElementById("enterReceiveAmt_" + rowNum).innerHTML = rcvAmt + '<input type="hidden" name="receiveAmt_' + rowNum + '" value="' + rcvAmt + '" />'
			ajax('/ajax/admin/ajaxVendorReceiveOrders.asp?adjustOrder=1&saveRcvAmt=1&orderID=<%=orderID%>&ourPN=' + partNumber + '&rcvAmt=' + rcvAmt,'dumpZone')
		}
	}
	function verifyProcess() {
		var yourstate=window.confirm("Are you sure the order is ready to process?")
		if (yourstate) {
			return true
		}
		else {
			return false
		}
	}
	
	function sortBy(which) {
		ajax('/ajax/admin/ajaxVendorReceiveOrders.asp?sortBy=' + which,'orderDetails')
	}
	
	function confirmZeroOut() {
		var yourstate=window.confirm("Are you sure you want to zero out the remaining items?")
		if (yourstate) {
			window.location=window.location+'&zeroOut=1'
		}
	}
	
	function addHousing(houseID,partNumber) {
		ajax('/ajax/addHousing.asp?partNumber=' + partNumber,'dumpZone')
		document.getElementById("houseID_" + houseID).src = "/images/icons/houseIcon.png"
		alert("Housing added to " + partNumber)
	}
	
	function saveDate(dbColumn,curDate) {
		ajax('/ajax/admin/ajaxVendorReceiveOrders.asp?orderID=<%=orderID%>&saveDateColumn=' + dbColumn,'dumpZone')
		document.getElementById(dbColumn).innerHTML = curDate
	}
</script>