<%
pageTitle = "Admin - Update WE Database - Cancel TEST ORDERS"
header = 1
server.scripttimeout = 1000
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<h3>Update WE Database - Cancel TEST ORDERS</h3>

<%
dim orderID
orderID = prepInt(request.form("orderID"))
if request.form("submitted") = "Update" then
	call cancelTestOrder(request.form("orderID"))
	%>
	<h3>UPDATED!</h3>
	<%
elseif request.form("submitted") = "Search" then
	SQL = "SELECT orderID, store, accountID, approved, cancelled, thub_posted_to_accounting, thub_posted_date, OrderDateTime, ordergrandtotal FROM we_Orders WHERE orderID = '" & orderID & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		dim thisStore
		thisStore = store(RS("store"))
		if thisStore <> "NA" then
			SQL = "SELECT fName, lName, email FROM " & thisStore & "_Accounts WHERE accountID = '" & RS("accountID") & "'"
			set RS2 = Server.CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			if not RS2.eof then
				response.write "<p>Store: " & thisStore & "</p>" & vbcrlf
				response.write "<p>Name: " & RS2("fName") & " " & RS2("lName") & "</p>" & vbcrlf
				response.write "<p>Email: " & RS2("email") & "</p>" & vbcrlf
				response.write "<p>Order Date/Time: " & RS("OrderDateTime") & "</p>" & vbcrlf
				response.write "<p>Order Grand Total: " & formatCurrency(RS("ordergrandtotal")) & "</p>" & vbcrlf
				response.write "<p>Approved: " & RS("approved") & "</p>" & vbcrlf
				response.write "<p>Cancelled: " & RS("cancelled") & "</p>" & vbcrlf
				response.write "<p>Processed: " & RS("thub_posted_to_accounting") & "</p>" & vbcrlf
				response.write "<p>Processed Date: " & RS("thub_posted_date") & "</p>" & vbcrlf
				%>
				<form action="db_update_cancel_TEST_ORDERS.asp" method="post">
					<p><input type="hidden" name="orderID" value="<%=RS("orderID")%>">
                    <% if isnull(RS("cancelled")) then %>
                    <input type="submit" name="submitted" value="Update"></p>
                    <% else %>
                    <div style="color:#F00; font-weight:bold; padding-left:10px; font-size:16px;">This order is already cancelled</div>
					<% end if %>
				</form>
				<%
			else
				response.Write(SQL & "<br>")
				%>
				<h3>Not Found! (ec 101)</h3>
				<%
			end if
		else
			%>
			<h3>Not Found! (ec 102)</h3>
			<%
		end if
	else
		%>
		<h3>Not Found! (ec 103)</h3>
		<%
	end if
else
	%>
	<form action="db_update_cancel_TEST_ORDERS.asp" method="post">
		<p>Order #:&nbsp;&nbsp;<input type="text" name="orderID"></p>
		<p><input type="submit" name="submitted" value="Search"></p>
	</form>
	<%
end if

function store(val)
	select case val
		case "0" : store = "WE"
		case "1" : store = "CA"
		case "2" : store = "CO"
		case "3" : store = "PS"
		case "10" : store = "ER"
		case "11" : store = "WL"
		case else : store = "NA"
	end select
end function
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
