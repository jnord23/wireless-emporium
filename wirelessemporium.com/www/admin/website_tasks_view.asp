<%
pageTitle = "Admin - View Website Tasks"
header = 1
dim thisAdminID
thisAdminID = Request.Cookies("adminID")
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<style>
	.hideDiv 	{ display:none; }
	.shovelLink { cursor:pointer; color:#00F; text-decoration:underline; }
</style>
<h3><%=pageTitle%></h3>

<%
dim Deleted, CompletedOnly, SelectSite, SortBy
Deleted = BlankToZero(request.form("Deleted"))
CompletedOnly = BlankToZero(request.form("CompletedOnly"))
SelectSite = BlankToZero(request.form("SelectSite"))
SortBy = BlankToZero(request.form("SortBy"))
thisWeek = prepInt(request.Form("thisWeek"))

'response.write "CompletedOnly : " & CompletedOnly & "<br>"

dim SelectAssignedTo
if request.form("SelectAssignedTo") = "" then
	if prepInt(session("SelectAssignedTo")) = 0 then session("SelectAssignedTo") = adminID
	SelectAssignedTo = prepInt(session("SelectAssignedTo"))
else
	SelectAssignedTo = request.form("SelectAssignedTo")
	session("SelectAssignedTo") = SelectAssignedTo
end if

SQL = "SELECT d.fname + ' ' + d.lname as projectMaster, A.*, B.fname + ' ' + B.lname as fname, c.title as parentTitle, c.details as parentDetails, c.dateCompleted as parentCompleteDate FROM we_Website_Tasks A LEFT JOIN we_AdminUsers B ON A.EnteredBy = B.adminID left join we_Website_Tasks c on c.parentID = a.id left join we_adminUsers d on d.adminID = a.AssignedTo WHERE A.ParentID IS NULL"
if Deleted = "1" then
	SQL = SQL & " AND A.deleted = 1"
else
	SQL = SQL & " AND A.deleted = 0"
end if
if CompletedOnly = "2" then
	SQL = SQL & " AND A.DateCompleted IS NOT NULL" 'show only complete
elseif CompletedOnly = "1" then
	'nothing (show both complete and incomplete)
else
	SQL = SQL & " AND A.DateCompleted IS NULL" 'show only incomplete
end if
if SelectAssignedTo <> "0" then SQL = SQL & " AND A.AssignedTo = '" & SelectAssignedTo & "'"
if SelectSite <> "0" then SQL = SQL & " AND A.site LIKE '%" & SelectSite & "%'"
if SortBy = "0" then
	SQL = SQL & " ORDER BY A.DateCompleted DESC, A.priority, A.status DESC, A.DateEntered"
else
	SQL = SQL & " ORDER BY A.DateCompleted DESC, A.DateEntered, A.priority, A.status"
end if
if thisWeek = 1 then
	SQL = "SELECT d.fname + ' ' + d.lname as projectMaster, A.*, B.fname + ' ' + B.lname as fname, c.title as parentTitle, c.details as parentDetails, c.dateCompleted as parentCompleteDate FROM we_Website_Tasks A LEFT JOIN we_AdminUsers B ON A.EnteredBy = B.adminID left join we_Website_Tasks c on c.parentID = a.id left join we_adminUsers d on d.adminID = a.AssignedTo WHERE A.ParentID IS NULL AND A.DateCompleted > '" & date-7 & "'"
	if SelectAssignedTo <> "0" then SQL = SQL & " AND A.AssignedTo = '" & SelectAssignedTo & "'"
	SQL = SQL & " ORDER BY A.DateCompleted DESC, A.priority, A.status DESC, A.DateEntered"
end if
session("errorSQL") = sql
set RS = oConn.execute(sql)

sql = "select adminID, fname, lname from we_adminUsers where (department = 'Programming' or department = 'Graphics' or taskList = 1) and active = 1 order by fname"
session("errorSQL") = sql
set usersRS = oConn.execute(sql)
	%>
	<table border="0" cellpadding="3" cellspacing="0" align="center" class="smlText" width="1000">
		<tr>
			<td align="left" colspan="6"><font color="#FF0000"><a href="website_tasks_add_edit.asp"><b>ADD NEW TASK</b></a></font></td>
            <td align="right" colspan="2"><a href="javascript:thisWeek()">Completed This Week</a></td>
		</tr>
		<form action="website_tasks_view.asp" name="SelectForm" method="post">
        	<input type="hidden" name="thisWeek" value="0" />
		<tr>
			<td valign="top" align="center"><b>Assigned&nbsp;To</b><br>
				<select name="SelectAssignedTo" onChange="document.SelectForm.submit();">
					<%
					do while not usersRS.EOF
						adminID = prepInt(usersRS("adminID"))
						fname = prepStr(usersRS("fname"))
						lname = prepStr(usersRS("lname"))
					%>
                    <option value="<%=adminID%>"<%if prepInt(SelectAssignedTo) = adminID then response.write " selected"%>><%=fname & " " & lname%></option>
                    <%
						usersRS.movenext
					loop
					%>
				</select>
			</td>
			<td valign="top" align="center"><b>Site</b><br>
				<select name="SelectSite" onChange="document.SelectForm.submit();">
					<option value="0"<%if SelectSite = "0" then response.write " selected"%>>ALL</option>
					<option value="WE"<%if SelectSite = "WE" then response.write " selected"%>>WE</option>
					<option value="CA"<%if SelectSite = "CA" then response.write " selected"%>>CA</option>
					<option value="CO"<%if SelectSite = "CO" then response.write " selected"%>>CO</option>
					<option value="PS"<%if SelectSite = "PS" then response.write " selected"%>>PS</option>
					<option value="FG"<%if SelectSite = "FG" then response.write " selected"%>>FG</option>
					<option value="TM"<%if SelectSite = "TM" then response.write " selected"%>>TM</option>
					<option value="Admin"<%if SelectSite = "Admin" then response.write " selected"%>>Admin</option>
					<option value="other"<%if SelectSite = "other" then response.write " selected"%>>other</option>
				</select>
			</td>
			<td valign="top" align="center"><b>Deleted</b><br>
				<select name="Deleted" onChange="document.SelectForm.submit();">
					<option value="1"<%if Deleted = "1" then response.write " selected"%>>Yes</option>
					<option value="0"<%if Deleted = "0" then response.write " selected"%>>No</option>
				</select>
			</td>
			<td valign="top" align="center"><b>Show&nbsp;Completed</b><br>
				<select name="CompletedOnly" onChange="document.SelectForm.submit();">
					<option value="0"<%if CompletedOnly = "0" then response.write " selected"%>>No</option>
                    <option value="1"<%if CompletedOnly = "1" then response.write " selected"%>>Yes</option>
                    <option value="2"<%if CompletedOnly = "2" then response.write " selected"%>>Only</option>
				</select>
			</td>
			<td valign="top" align="center"><b>Sort&nbsp;By</b><br>
				<select name="SortBy" onChange="document.SelectForm.submit();">
					<option value="1"<%if SortBy = "1" then response.write " selected"%>>Date Entered</option>
					<option value="0"<%if SortBy = "0" then response.write " selected"%>>Priority</option>
				</select>
			</td>
		</tr>
		</form>
	</table>
	<table border="1" cellpadding="2" cellspacing="0" align="center" class="smlText" width="1000">
		<%
		if RS.eof then
			response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
			response.write "<a href=""website_tasks_view.asp"">Try again.</a>"
		else
			if SelectAssignedTo <> "0" then
				sql = 	"SELECT d.fname + ' ' + d.lname as projectMaster, A.*, B.fname + ' ' + B.lname as fname, c.title as parentTitle, c.details as parentDetails, c.dateCompleted as parentCompleteDate FROM we_Website_Tasks A LEFT JOIN we_AdminUsers B ON A.EnteredBy = B.adminID left join we_Website_Tasks c on c.parentID = a.id left join we_adminUsers d on d.adminID = a.AssignedTo WHERE A.ParentID IS NULL AND A.DateCompleted > '" & date-7 & "'" &_
						" AND A.AssignedTo = '" & SelectAssignedTo & "'" &_
						" ORDER BY A.DateCompleted DESC, A.priority, A.status DESC, A.DateEntered"
				session("errorSQL") = sql
'				response.write sql
				set doneRS = oConn.execute(sql)
				
				if not doneRS.EOF then
		%>
        <tr bgcolor="#333333" style="color:#FFF; font-weight:bold; font-size:16px;"><td colspan="5" align="center">Projects Completed This Week</td></tr>
        <%
				end if
				do while not doneRS.EOF
		%>
        <tr bgcolor="#333333" style="color:#FFF; font-size:14px;">
        	<td valign="top" colspan="5" align="center" style="padding:5px; cursor:pointer;" onclick="window.location='/admin/website_tasks_add_edit.asp?id=<%=doneRS("id")%>'"><%=doneRS("title")%></td>
        </tr>
        <%
					doneRS.movenext
				loop
			end if
			parentTest = 0
			taskLap = 0
			do until RS.eof
				taskLap = taskLap + 1
				if parentTest = 0 then
					AssignedTo = RS("projectMaster")
					id = RS("id")
					site = RS("site")
					title = RS("title")
					details = RS("details")
					priority = RS("priority")
					TaskStatus = RS("status")
					DateEntered = RS("DateEntered")
					dateCompleted = RS("DateCompleted")
					RelatedID = RS("RelatedID")
					notes = RS("notes")
					isBug = RS("isBug")
					timeEstimate = prepStr(RS("timeEstimate"))
					shovelReady = RS("shovelReady")
					EnteredBy = RS("fname")
					parentTitle = RS("parentTitle")
					parentDetails = RS("parentDetails")
					parentCompleteDate = RS("parentCompleteDate")
					if timeEstimate = "" then timeEstimate = "Not Entered"
					
					if shovelReady then
						shovelLinkClass = "hideDiv"
						shovelImgClass = ""
					else
						shovelLinkClass = ""
						shovelImgClass = "hideDiv"
					end if
					onThisIdCnt = 1
				end if
				parentTest = 0
				if isDate(dateCompleted) then
					DateCompleted = dateValue(dateCompleted)
				else
					DateCompleted = ""
				end if
				
				select case priority
					case 0 : bgcolor = "FF0000"
					case 1 : bgcolor = "FFFF00"
					case 2 : bgcolor = "9999FF"
					case 3 : bgcolor = "00FF00"
				end select
				
				TaskStatusID = TaskStatus
				select case TaskStatus
					case 0 : TaskStatus = "Delayed"
					case 1 : TaskStatus = "Need Materials"
					case 2 : TaskStatus = "In Progress"
					case 8
						TaskStatus = "Testing"
'						bgcolor = "04B801"
					case 9
						TaskStatus = "Awaiting Approval"
'						bgcolor = "04B801"
					case 10 : TaskStatus = "Completed<br>" & DateCompleted
				end select
				
				if isDate(dateCompleted) then
					TaskStatus = "Completed<br>" & DateCompleted
				end if
				
				showThis = true
'				if TaskStatus = "Testing" and thisAdminID <> "8" and thisAdminID <> "29" and thisAdminID <> "19" and thisAdminID <> "54" and thisAdminID <> "55" then showThis = false
				if showThis = true then
					%>
                        <tr id="taskBox_<%=id%>_1">
                            <td valign="top" align="center" width="50" bgcolor="#<%=bgcolor%>">
                                <span class="normalText"><b>Priority</b></span><br>
                                <span class="biggerText">
                                    <%
    '								if TaskStatus = "Testing" or TaskStatus = "Awaiting Approval" then
    '									response.write "<font color=""#0000DD"">Done</font>"
    '								else
                                        if priority = 0 then
                                            response.write "<font color=""#FFFFFF"">ALERT</font>"
                                        else
                                            response.write priority
                                        end if
    '								end if
                                    %>
                                </span>
                            </td>
                            <td valign="top" align="center" width="150">
                                <span class="normalText"><b>Site</b></span><br>
                                <span class="biggerText"><%=replace(replace(replace(replace(replace(replace(replace(replace(site,"WE","<font color=""#666666"">WE</font>"),"CA","<font color=""#9966CC"">CA</font>"),"CO","<font color=""#FF0000"">CO</font>"),"PS","<font color=""#660033"">PS</font>"),"FG","<font color=""#0000FF"">FG</font>"),"ER","<font color=""#04B801"">ER</font>"),"Admin","<font color=""#000000"">Admin</font>"),"other","<font color=""#000000"">other</font>")%></span>
                            </td>
                            <td valign="top" align="left" width="575" class="boldText"><%=title%></td>
                            <td valign="top" align="center" width="100"><span class="normalText"><b>Status</b></span><br><%=TaskStatus%></td>
                            <td valign="top" align="center" width="125"><span class="normalText"><b>Entered</b></span><br><%=DateEntered%></td>
                        </tr>
                        <tr id="taskBox_<%=id%>_2">
                            <td valign="top" align="center">
                            	<div>
	                            	<span class="normalText"><b>Task&nbsp;#</b></span><br>
    	                            <span class="biggerText"><a href="website_tasks_add_edit.asp?id=<%=id%>"><%=id%></a></span>
                                </div>
                                <div id="shovelLink_<%=id%>" class="shovelLink <%=shovelLinkClass%>" onclick="shovelReady(<%=id%>)">Shovel<br />Ready?</div>
                                <div id="shovelImg_<%=id%>" class="<%=shovelImgClass%>" onclick="shovelReady(<%=id%>)"><img src="/images/icons/shovelReadySmall.gif" border="0" width="46" height="50" /></div>
                            </td>
                            <td valign="top" align="center">
                            	<form name="taskForm_<%=id%>">
                            	<div class="normalText" style="font-weight:bold;">Assigned&nbsp;To</div>
                                <div class="biggerText" style="padding-top:4px;"><%=AssignedTo%></div>
                                <div style="padding-top:4px;"><input type="checkbox" name="deleteTask" />&nbsp;Delete</div>
                                <div style="padding-top:4px;">
                                	<select name="TaskStatus" class="smlText">
                                        <option value="0"<%if TaskStatusID = 0 then response.write " selected"%>>Delayed</option>
                                        <option value="1"<%if TaskStatusID = 1 then response.write " selected"%>>Need Materials</option>
                                        <option value="2"<%if TaskStatusID = 2 then response.write " selected"%>>In Progress</option>
                                        <option value="8"<%if TaskStatusID = 8 then response.write " selected"%>>Testing</option>
                                        <option value="9"<%if TaskStatusID = 9 then response.write " selected"%>>Awaiting Approval</option>
                                        <option value="10"<%if TaskStatusID = 10 then response.write " selected"%>>Completed</option>
                                    </select>
                                </div>
                                <div style="padding-top:4px;"><input type="button" name="myAction" value="Update" style="font-size:10px;" onclick="updateTask(<%=id%>)" /></div>
                                </form>
                             </td>
                            <td valign="top" align="left">
                                <%
                                cellCheck = 0
                                if not isNull(details) and details <> "" then
                                    response.write "<p>" & replace(details,vbcrlf,"<br>") & "</p>" & vbcrlf
                                    cellCheck = 1
                                end if
                                
                                if not isnull(parentTitle) then
                                    parentTest = 1
                                    response.write "<ol style=""margin-top:0px;"">" & vbcrlf
                                    baseID = id
                                    do while baseID = id
                                        response.write "<li>" & vbcrlf
                                        if not isNull(parentCompleteDate) then response.write "<s>"
                                        response.write parentTitle & vbcrlf
                                        if not isNull(parentDetails) and parentDetails <> "" then response.write "<ul style=""margin-top:0px;""><li>" & replace(parentDetails,vbcrlf,"</li><li>") & "</li></ul>" & vbcrlf
                                        if not isNull(parentCompleteDate) then response.write "</s>"
                                        response.write "</li>" & vbcrlf
                                        if RS.EOF then exit do
                                        RS.movenext
                                        if not RS.EOF then
                                            AssignedTo = RS("projectMaster")
                                            id = RS("id")
                                            if baseID = id then
                                                site = RS("site")
                                                title = RS("title")
                                                details = RS("details")
                                                priority = RS("priority")
                                                TaskStatus = RS("status")
                                                DateEntered = RS("DateEntered")
                                                dateCompleted = RS("DateCompleted")
                                                RelatedID = RS("RelatedID")
                                                notes = RS("notes")
                                                'isBug = RS("isBug")
                                                EnteredBy = RS("fname")
                                                parentTitle = RS("parentTitle")
                                                parentDetails = RS("parentDetails")
                                                parentCompleteDate = RS("parentCompleteDate")
                                            else
                                                parentTest = 2
                                            end if
                                        end if
                                    loop
                                    response.write "</ol>" & vbcrlf
                                else
                                    if cellCheck = 0 then response.write "&nbsp;"
                                end if
                                %>
                            </td>
                            <td valign="top" align="center">
                                <div class="normalText" style="font-weight:bold;">Related Task</div>
                                <div class="normalText"><% if prepStr(RelatedID) = "" then response.Write("No Related") else response.Write(RelatedID) end if %></div>
                                <div style="border-top:1px solid #000; margin-top:5px; padding-top:5px;"><% if isBug then %><img src="/images/icons/bug-icon.jpg" border="0" /><% end if %></div>
                            </td>
                            <td valign="top" align="center">
                                <div><span class="normalText"><b>Entered By</b></span><br><%=EnteredBy%></div>
                                <div style="margin-top:5px; border-top:1px solid #999; padding-top:5px;"><span class="normalText"><b>Est Time</b></span><br><%=timeEstimate%></div>
                            </td>
                        </tr>
                        <%if not isNull(notes) and notes <> "" then%>
                        <tr id="taskBox_<%=id%>_3">
                            <td align="left" valign="top" colspan="8"><b>&nbsp;Notes:&nbsp;&nbsp;</b><%=notes%></td>
                        </tr>
                        <%end if%>
					<tr id="taskBox_<%=id%>_4">
						<td align="center" colspan="8" bgcolor="#999999"><img src="/images/spacer.gif" width="1" height="2" border="0"></td>
					</tr>
					<%
				end if
				if parentTest = 0 then RS.movenext
				if parentTest = 2 then parentTest = 0
			loop
			RS.close
		end if
		set RS = nothing
		%>
	</table>
	<%

function BlankToZero(val)
	if val = "" then
		BlankToZero = "0"
	else
		BlankToZero = val
	end if
end function
%>
<p>&nbsp;</p>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function thisWeek() {
		document.SelectForm.thisWeek.value = 1
		document.SelectForm.submit()
	}
	
	function updateTask(taskID) {
		var taskStatus = eval("document.taskForm_" + taskID + ".TaskStatus.value");
		var taskDelete = eval("document.taskForm_" + taskID + ".deleteTask.checked");
		
		document.getElementById("dumpZone").innerHTML = "";
		ajax('/ajax/admin/taskUpdate.asp?taskID=' + taskID + '&taskStatus=' + taskStatus + '&taskDelete=' + taskDelete,'dumpZone');
		
		if (taskStatus == 10 || taskDelete) {
			document.getElementById("taskBox_" + taskID + "_1").style.display = 'none';
			document.getElementById("taskBox_" + taskID + "_2").style.display = 'none';
			if (document.getElementById("taskBox_" + taskID + "_3") != undefined) {
				document.getElementById("taskBox_" + taskID + "_3").style.display = 'none';
			}
			document.getElementById("taskBox_" + taskID + "_4").style.display = 'none';
		}
	}
	
	function shovelReady(taskID) {
		if (document.getElementById("shovelLink_" + taskID).className == "shovelLink hideDiv") {
			document.getElementById("shovelLink_" + taskID).className = "shovelLink"
			document.getElementById("shovelImg_" + taskID).className = "hideDiv"
			ajax('/ajax/admin/webTasks.asp?taskID=' + taskID + '&shovelReady=0','dumpZone')
		}
		else {
			document.getElementById("shovelLink_" + taskID).className = "shovelLink hideDiv"
			document.getElementById("shovelImg_" + taskID).className = ""
			ajax('/ajax/admin/webTasks.asp?taskID=' + taskID + '&shovelReady=1','dumpZone')
		}
	}
</script>