<%
pageTitle = "CO Admin Site - Daily Orders Statistics"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">

<p>&nbsp;</p>

<%
if request.querystring("submitted") <> "" then dateStart = request.querystring("dateStart")
if not isDate(dateStart) then dateStart = date()
dateStart = dateValue(dateStart)

showblank = "&nbsp;"
shownull = "-null-"

myTotal = 0

if request.querystring("details") = "1" then
	selectSQL = "A.*"
else
	selectSQL = "A.orderID, A.accountid, A.ordergrandtotal, A.orderdatetime"
end if

if strError = "" then
	SQL = "SELECT " & selectSQL & " FROM we_orders A INNER JOIN CO_accounts B ON A.accountid=B.accountid"
	SQL = SQL & " WHERE A.orderdatetime >= '" & dateStart & "' AND A.orderdatetime < '" & dateAdd("D",1,dateStart) & "'"
	SQL = SQL & " AND A.approved = 1"
	SQL = SQL & " AND (A.cancelled = 0 OR A.cancelled IS NULL)"
	SQL = SQL & " AND A.store = 2"
	SQL = SQL & " AND (A.extOrderType IS NULL OR A.extOrderType < 4)"
	SQL = SQL & " AND (B.email = 'phoneorder@wirelessemporium.com' OR B.email NOT LIKE '%@wirelessemporium.com')"
	SQL = SQL & " ORDER BY A.orderdatetime"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	
	if RS.eof then
		response.write "No records matched<br><br>So cannot make table..."
	else
		do until RS.eof
			myTotal = myTotal + cDbl(RS("ordergrandtotal"))
			RS.movenext
		loop
		response.write "<h3>CO DAILY ORDERS STATISTICS FOR: " & dateStart & "</h3>"
		response.write "<h3>TOTAL ORDERS: " & RS.recordcount & "</h3>"
		response.write "<h3>TOTAL ORDER AMOUNT: " & formatCurrency(myTotal) & "</h3>"
		response.write "<h3>AVERAGE ORDER AMOUNT: " & formatCurrency(myTotal/RS.recordcount) & "</h3>"
		RS.movefirst
		%>
		<table border="1">
			<tr>
				<%
				for each whatever in RS.fields
					%>
					<td><b><%=whatever.name%></b></td>
					<%
				next
				%>
			</tr>
			<%
			do until RS.eof
				%>
				<tr>
					<%
					for each whatever in RS.fields
						thisfield = whatever.value
						if isnull(thisfield) then thisfield = shownull
						if trim(thisfield) = "" then thisfield = showblank
						if whatever.name = "orderID" then
							%>
							<td valign="top"><%=RS("OrderId")%></td>
							<%
						else
							%>
							<td valign="top"><%=thisfield%></td>
							<%
						end if
					next
					%>
				</tr>
				<%
				RS.movenext
			loop
			%>
		</table>
		<%
	end if
	
	RS.close
	Set RS = nothing
else
	%>
	<center>
	<font size="+2" color="red"><%=strError%></font>
	<a href="javascript:history.back();">BACK</a>
	</center>
	<%
end if
%>
<p>&nbsp;</p>
<h3>Choose another date:</h3>
<br>
<form action="report_OrdersStats_CO.asp" name="frmSalesReport" method="get">
	<p>
		<input type="text" name="dateStart">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.frmSalesReport.dateStart,document.frmSalesReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;Report&nbsp;Date
		<input type="hidden" name="dc2" value="<%=dateAdd("yyyy",1,date)%>">
		<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
	</p>
	<p><input type="checkbox" name="details" value="1">&nbsp;&nbsp;Show&nbsp;Details</p>
	<p><input type="submit" name="submitted" value="Generate Report"></p>
</form>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
