<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Process Phone Orders"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<link href="/includes/css/phoneorder.css?v=20140529_3" rel="stylesheet" type="text/css">
<div id="toast">ITEM ADDED</div>
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
'	response.write request.form
	dim fs : set fs = CreateObject("Scripting.FileSystemObject")
	siteID = prepStr(request.form("cbSite"))
	orderid = prepInt(request.form("hidOrderid"))
	email = prepStr(request.form("email"))
	adminID = session("adminID")
	freeItemTotalLimit = cdbl(15.00)
	
	if email = "Email Address" then email = ""
	if siteID = "" then siteID = 2
	if siteID = 0 then
		strandsAPIID = "PLOSodl5QC"
	elseif siteID = 2 then
		strandsAPIID = "Kx7AmRMCrW"
	end if

	sql = "select * from xstore where site_id = '" & siteID & "' order by 1"
	session("errorSQL") = sql
	set rsSite = oConn.execute(sql)
	siteColor = rsSite("color")
	siteName = rsSite("longDesc")

	sql	=	"select	siteid, configValue" & vbcrlf & _
			"	,	case when siteid = 2 then b.itemdesc_co else b.itemdesc end itemdesc" & vbcrlf & _
			"from	we_config a with (nolock) join we_items b with (nolock)" & vbcrlf & _
			"	on	a.configValue = b.itemid" & vbcrlf & _
			"where	configName = 'Free ItemID'" & vbcrlf & _
			"	and	a.siteid = " & siteID & vbcrlf & _
			"order by 1"
	arrFreeItems = getDbRows(sql)
	for i=0 to ubound(arrFreeItems, 2)
		freeItemIds = freeItemIds & arrFreeItems(1,i) & ","
	next
%>
<!--#include virtual="/includes/asp/inc_getStatesAdmin.asp"-->
<script src="//code.jquery.com/jquery-latest.min.js"></script>
<script src="/framework/userinterface/js/slides/slides.min.jquery.js"></script>
<script type="text/javascript" src="//bizsolutions.strands.com/sbsstatic/js/sbsLib-1.0.min.js"></script>
<form name="frmPhoneOrder" method="post">
<center>
<div class="phoneorder">
	<div class="ffl" id="terry"></div>
	<div class="ffl margin-v">
    	<div class="ffl padding-v" style="padding-left:90px; font-size:18px; font-weight:bold; text-align:left; ">Choose A Store:</div>
    	<div class="ffl padding-v">
        	<%if siteid = 0 then%>
        	<div class="logoOff" onclick="changeSite(2)"><div class="coLogoOff"></div></div>
        	<div class="logoOn" onclick="changeSite(0)"><div class="weLogoOn"></div></div>            
            <%elseif siteid = 2 then%>
        	<div class="logoOn" onclick="changeSite(2)"><div class="coLogoOn"></div></div>
        	<div class="logoOff" onclick="changeSite(0)"><div class="weLogoOff"></div></div>
            <%end if%>
            <div class="fl" style="padding-top:30px;"><a class="lnk" href="javascript:searchItems()" style="font-size:16px;">Search Items</a></div>
        </div>
    </div>
	<div class="headings">
        <div class="left" style="border:0px;">
			<!--<div class="fl" style="width:2%;">&nbsp;</div>-->
			<div class="itemDesc">Item Description</div>
			<div class="qty">Quantity</div>
			<div class="subtotal">Subtotal</div>
        </div>
        <div class="right">
			<div class="store">Store</div>
			<div class="orderid">Order ID</div>
			<div class="orderdate">Order Date</div>
			<div class="ordertotal">Order Total</div>
			<div class="approved">Approved / Cancelled</div>
        </div>
    </div>
	<div class="left" id="id_cart">
    	<div id="cartItems"><div style="width:100%; text-align:center; padding:20px 0px 20px 0px;"><img src="//d2brc35ftammet.cloudfront.net/img/loading-grey.gif" /></div></div>
        <div class="ffl margin-v" style="position:relative;">
        	<div class="fl" style="font-size:15px; font-weight:bold; background-color:#ebebeb; height:22px; padding:8px 0px 5px 0px; text-align:center; margin-right:1px; min-width:95px; width:19%;">
            	Item Look Up
			</div>
        	<div class="fl" style="position:relative; font-size:15px; font-weight:bold; background-color:#ebebeb; height:24px; padding:6px 0px 5px 0px; text-align:center; margin-right:1px; min-width:225px; width:40%;">
            	<input type="radio" id="rdoSearch1" name="rdoSearch" value="o" checked="checked" onclick="onRdoSearch(this.value)" /> <label for="rdoSearch1">Search By Declined OrderID</label>
			</div>
        	<div class="fl" style="position:relative; font-size:15px; font-weight:bold; background-color:#ebebeb; height:24px; padding:6px 0px 5px 0px; text-align:center; min-width:225px; width:40%;">
            	<input type="radio" id="rdoSearch2" name="rdoSearch" value="c" onclick="onRdoSearch(this.value)" /> <label for="rdoSearch2">Search By CartID</label>
			</div>
            <div id="cartSearchBox" style="position:absolute; top:45px; left:28%;">
                <div class="fl"><input class="sField" type="text" name="txtDeclinedID" value="CartID or OrderID" onfocus="onField(this)" onkeydown="if (event.keyCode==13) grabCart()" /></div>
                <div class="fl"><div class="btnSearch" onclick="grabCart()"></div></div>
            </div>
        </div>
        <div class="ffl margin-v padding-v">
        	<div class="fl">
            	<input id="id_txtItemID" class="xsField" type="text" name="txtItemID" value="ItemID" onfocus="onField(this)" onkeydown="if (event.keyCode==13) addItemToCart(this.value,1,'','')" />
			</div>
        	<div class="fl"><div class="btnSearch" onclick="addItemToCart(document.getElementById('id_txtItemID').value,1,'','')"></div></div>
        </div>
        <div class="ffl margin-v padding-v">
			<!--<div class="fr"><a href="javascript:refreshWidget()">refresh Widget</a></div>-->
        	<div class="fl roundBorder" style="width:99%;">
                <div class="slides1" align="center">
                    <div class="arrow-left"></div>
                    <div class="arrow-right"></div>
                    <div id="mainStrands" class="slides_container strandsRecs" tpl="misc_1" item="">
                    </div>                
                </div>            
            </div>
        </div>
        <div class="ffl margin-v padding-v">
        	<div class="fl" style="height:50px; width:100px;">Promo Item:</div>
            <%
			lap = 0
			for i=0 to ubound(arrFreeItems, 2)
				if prepInt(arrFreeItems(0,i)) = prepInt(siteid) then
					lap = lap + 1
				%>
	        	<div class="fl margin-h"><input type="radio" id="rdoPromo<%=lap%>" name="rdoPromo" value="<%=prepInt(arrFreeItems(1,i))%>" <%if lap=1 then%>checked="checked"<%end if%>/> <label for="rdoPromo<%=lap%>">Item <%=lap%>: <%=arrFreeItems(2,i)%></label></div>
                <%
				end if
			next
			%>
        </div>
        <div class="ffl margin-v padding-v">
        	<div class="fl" style="width:70%; padding-right:1%;">
            	<div class="ffl">
				    <div class="fl talign-r" style="padding-top:8px; width:90px;">Promo Code:</div>
                    <div class="fl margin-h"><input class="sField" type="text" name="sPromoCode" value="Promo Code" onfocus="onField(this)" onkeydown="if (event.keyCode==13) getCart()" /></div>
                    <div id="btnCoupon" class="btn" onclick="getCart()">Apply</div>
                </div>
            	<div class="ffl margin-v padding-v">
				    <div class="fl talign-r" style="padding-top:8px; width:90px;">Shipping:</div>
                    <div class="fl margin-h" id="shippingMethods"></div>
                </div>
            </div>
        	<div class="fl" style="width:29%;">
            	<div class="ffl margin-v">
                    <div class="fr" style="text-align:right; font-size:15px;"><span id="id_subtotal">$0.00</span></div>
                    <div class="fl" style="font-weight:bold; text-align:left; font-size:15px;">Item Subtotal:</div>
                </div>
            	<div class="ffl margin-v padding-v">
                    <div class="fr" style="text-align:right; font-size:15px;"><span id="CAtax">$0.00</span></div>
                    <div class="fl" style="font-weight:bold; text-align:left; font-size:15px;">Tax:</div>
                    <div class="ffl talign-r" style="font-size:11px;" id="CAtaxMsg"></div>
                </div>
            	<div class="ffl margin-v padding-v">
                    <div class="fr" style="text-align:right; font-size:15px;"><span id="id_shipping">$0.00</span></div>
                    <div class="fl" style="font-weight:bold; text-align:left; font-size:15px;">Shipping:</div>
                </div>
            	<div class="ffl margin-v padding-v">
                    <div class="fr" style="text-align:right; font-size:15px; font-weight:bold; color:#9a0002;"><span id="id_discount">$0.00</span></div>
                    <div class="fl" style="font-weight:bold; text-align:left; font-size:15px;">Discount<span id="id_couponcode"></span>:</div>
                </div>
            	<div class="ffl margin-v padding-v">
                    <div class="fr" style="text-align:right; font-size:16px; font-weight:bold;"><span id="id_grandtotal">$0.00</span></div>
                    <div class="fl" style="font-weight:bold; text-align:left; font-size:15px;">Grand Total:</div>
                </div>
            </div>
        </div>
    </div>
	<div class="right padding-v" id="id_orderhistory" style="overflow:auto;">Enter email to retrieve order history</div>
	<div class="headings">
    	<div class="fl">
            <div class="fl padding-h"><input type="checkbox" id="id_chkBilling" name="chkUseDiffAddress" value="yes" onclick="toggleWhich('billingAddress',this)" /></div>
            <div class="fl"><label for="id_chkBilling">Billing is different than shipping</label></div>        
        </div>
    	<div class="fl" style="padding-left:40px;">
            <div class="fl padding-h"><input type="checkbox" id="id_chkMail" name="chkOptMail" value="Y" /></div>
            <div class="fl"><label for="id_chkMail">Mailing List</label></div>
        </div>
    	<div class="fl" style="padding-left:40px;">
            <div class="fl padding-h"><input type="checkbox" id="id_chkMobile" name="chkMobile" value="Y" /></div>
            <div class="fl"><label for="id_chkMobile">Mobile Order</label></div>
        </div>
    </div>
    <div class="ffl margin-v padding-v">
        <div class="fl margin-v padding-v" style="padding-left:1%;">
            <div class="addressField">Phone Number:</div>
            <div class="fl"><input class="lField" type="text" name="phonenumber" value="phone number" onfocus="onField(this)" /></div>
        </div>    
    </div>
	<div class="left" style="border:0px;">
    	<div id="shippingAddress">
            <div class="ffl bold margin-v padding-v talign-l" style="font-size:18px;">Shipping Address</div>
            <div class="ffl margin-v padding-v">
                <div class="addressField">Email Address:</div>
                <div class="fl"><input class="lField" type="text" name="email" value="<%if email <> "" then response.write email else response.write "Email Address" end if%>" onfocus="onField(this)" onblur="getCustomerData(this.value);" /></div>
            </div>
            <div class="ffl margin-v padding-v">
                <div class="addressField">First Name:</div>
                <div class="fl"><input class="lField" type="text" name="fname" value="First Name" onfocus="onField(this)" /></div>
            </div>
            <div class="ffl margin-v padding-v">
                <div class="addressField">Last Name:</div>
                <div class="fl"><input class="lField" type="text" name="lname" value="Last Name" onfocus="onField(this)" /></div>
            </div>
            <div class="ffl margin-v padding-v">
                <div class="addressField">Address 1:</div>
                <div class="fl"><input class="lField" type="text" name="sAddress1" value="Address 1" onfocus="onField(this)" /></div>
            </div>
            <div class="ffl margin-v padding-v">
                <div class="addressField">Address 2:</div>
                <div class="fl"><input class="lField" type="text" name="sAddress2" value="Address 2" onfocus="onField(this)" /></div>
            </div>
            <div class="ffl margin-v padding-v">
                <div class="addressField">Zip Code:</div>
                <div class="fl">
                	
					<div style="position:relative;">
	                	<input class="sField" type="text" name="sZip" value="Zip Code" onfocus="onField(this)" onkeyup="getCityByZip()" />
                        <div id="id_sCity" class="left" style="position:absolute; display:none; z-index:100; bottom:-40px; right:-265px; width:250px; padding:5px; border-radius:5px; border:2px solid <%=siteColor%>; background-color:#fff;">
                            <div class="ffl" style="width:100%; padding-bottom:3px; border-bottom:1px solid #000; font-size:12px;">
                                <div class="fl">City Name Suggestion</div>
                                <div class="fr"><a href="javascript:toggle('id_sCity');">Close</a></div>
                            </div>
                            <div id="id_return_sCity" class="left" style="text-align:center; width:100%; height:200px; overflow:auto;"></div>
                        </div>                                        
                    </div>
				</div>
            </div>
            <div class="ffl margin-v padding-v">
                <div class="addressField">CIty:</div>
                <div class="fl">
                	<input class="mField" type="text" name="sCity" value="City Name" onfocus="onField(this)" />
				</div>
            </div>
            <div class="ffl margin-v padding-v">
                <div class="addressField">State:</div>
                <div class="fl">
	                <select class="mCbBox" style="height:35px;" name="sState" onchange="getShipping()"><%getStates(sState)%></select>
				</div>
            </div>
        </div>
    	<div id="billingAddress" style="display:none;">
            <div class="ffl bold margin-v padding-v talign-l" style="font-size:18px;">Billing Address</div>
            <div class="ffl margin-v padding-v">
                <div class="addressField">Address 1:</div>
                <div class="fl"><input class="lField" type="text" name="bAddress1" value="Address 1" onfocus="onField(this)" /></div>
            </div>
            <div class="ffl margin-v padding-v">
                <div class="addressField">Address 2:</div>
                <div class="fl"><input class="lField" type="text" name="bAddress2" value="Address 2" onfocus="onField(this)" /></div>
            </div>
            <div class="ffl margin-v padding-v">
                <div class="addressField">Zip Code:</div>
                <div class="fl"><input class="sField" type="text" name="bZip" value="Zip Code" onfocus="onField(this)" /></div>
            </div>
            <div class="ffl margin-v padding-v">
                <div class="addressField">CIty:</div>
                <div class="fl"><input class="mField" type="text" name="bCity" value="City Name" onfocus="onField(this)" /></div>
            </div>
            <div class="ffl margin-v padding-v">
                <div class="addressField">State:</div>
                <div class="fl">
	                <select class="mCbBox" style="height:35px;" name="bState"><%getStates(bState)%></select>
				</div>
            </div>
        </div>
    </div>
	<div class="right">
        <div class="ffl bold margin-v padding-v talign-l" style="font-size:18px;">Payment Information</div>
        <div class="ffl margin-v padding-v">
            <div class="addressField">Card Holder Name:</div>
            <div class="fl"><input class="lField" type="text" name="cc_cardOwner" value="Card Holder Name" onfocus="onField(this)" /></div>
        </div>
        <div class="ffl margin-v padding-v">
            <div class="addressField">Card Number:</div>
            <div class="fl"><input class="lField" type="text" name="cardNum" value="Card Number" maxlength="16" onfocus="onField(this)" /></div>
        </div>
		<div class="ffl margin-v padding-v">
        	<div class="fl">
				<div class="addressField">Expiration:</div>
                <div class="fl">
					<select class="sCbBox" name="cc_month">
                        <option value="01">January (1)</option>
                        <option value="02">February (2)</option>
                        <option value="03">March (3)</option>
                        <option value="04">April (4)</option>
                        <option value="05">May (5)</option>
                        <option value="06">June (6)</option>
                        <option value="07">July (7)</option>
                        <option value="08">August (8)</option>
                        <option value="09">September (9)</option>
                        <option value="10">October (10)</option>
                        <option value="11">November (11)</option>
                        <option value="12">December (12)</option>
                    </select>
                </div>
                <div class="fl">
                    <select class="sCbBox" name="cc_year">
						<%
                        for countYear = 0 to 14
                            response.write "<option value=""" & right(cStr(year(date) + countYear),2) & """>" & cStr(year(date) + countYear) & "</option>" & vbcrlf
                        next
                        %>
                    </select>
                </div>                
            </div>
        </div>
        <div class="ffl margin-v padding-v">
            <div class="addressField">CVV:</div>
            <div class="fl"><input class="sField" style="width:140px;" type="text" name="secCode" value="CVV" onfocus="onField(this)" maxlength="4" /></div>
        </div>
        <div class="ffl margin-v padding-v">
			<div class="fl margin-v padding-v">
            	<input type="image" src="/images/admin/phoneorder/submit-order.png" onclick="return doSubmit();" border="0" />
            </div>
        </div>
    </div>
</div>
</center>
<input type="hidden" name="cbSite" value="<%=siteID%>" />
<input type="hidden" name="hidOrderID" value="" />
<input type="hidden" name="accountid" value="" />
<input type="hidden" name="parentAcctID" value="" />
</form>
<div id="id_customerdata" style="display:none;"></div>
<div id="id_shippingmethod" style="display:none;"></div>
<script src="/includes/js/validate_new.js"></script>
<script>
	var fnNxtSearchCallback;
	var nxtJSHost = '//d2brc35ftammet.cloudfront.net/';
	var jqNxt = null;
	if (typeof nxtOptions !== 'object') {
		var nxtOptions = {}
	}
	<%if siteid = 0 then%>
	nxtOptions.client_id = '00534521e462bde5d7aa8bbef26bb91f';
	nxtOptions.linkPrefix = '//wirelessemporium.ecomm-nav.com/';
	<%elseif siteid = 2 then%>
	nxtOptions.client_id = '9b1c36137711f5324260883e42567b60';
	nxtOptions.linkPrefix = '//cellularoutfitter.ecomm-nav.com/';
	<%end if%>
	nxtOptions.content_div = '#mainContent';
	nxtOptions.refinement_div = '#sideNav';
	
	/*
	window.onbeforeunload = function(){
		return 'Are you sure you want to leave?';
	};
	*/

	var gblSiteID = <%=siteID%>;
	var gblEmail = '<%=email%>';
	var gblOrderID = <%=orderid%>;
	var gblAdminID = <%=adminID%>;
	var gblTaxMath = <%=Application("taxMath")%>;
	var gblTaxDisplay = '<%=Application("taxDisplay")%>';
	var gblStrandsAPIID = '<%=strandsAPIID%>';
	var gblFreeItemIDs = '<%=freeItemIds%>';
	var nxtBtnTimer;
	var gblNxtBtnAddToCart = 0;
	var gblCurShipID = 0;
	var gblAccountID = 0;
	var gblParentAccID = 0;
	var numDynamicWidgetUsed = [];
	numDynamicWidgetUsed[0] = 0;
	numDynamicWidgetUsed[1] = 0;	
</script>
<script type="text/javascript" src="/includes/js/nxtAdmin.js?v=20140530_2"></script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->