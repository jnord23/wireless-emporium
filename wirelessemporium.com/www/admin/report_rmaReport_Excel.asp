<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
strSDate = prepStr(request.QueryString("txtSDate"))
strEDate = prepStr(request.QueryString("txtEDate"))
updateQTY = prepStr(request.QueryString("updateQTY"))
chkUpdate = prepStr(request.QueryString("chkUpdate"))
siteid = prepStr(request.QueryString("cbSite"))
returnReasonID = prepInt(request.QueryString("cbReturnReason"))
modelid = prepInt(request.QueryString("cbModel"))
typeid = prepInt(request.QueryString("cbType"))
strGroupBy = prepStr(request.QueryString("cbGroupBy"))

if strSDate = "" or not isdate(strSDate) then strSDate = Date-6
if strEDate = "" or not isdate(strEDate) then strEDate = Date

Dim cfgArrGroupBy : cfgArrGroupBy = Array("Date", "Store", "Activity Desc", "Return Reason", "Model", "Category", "ItemID", "Details")
if "" = strGroupBy then strGroupBy = "DETAILS,,,,,,," end if
arrGroupBy = split(strGroupBy, ",")

dim strSqlSelectMain, strSqlWhere, strSqlGroupBy, strSqlOrderBy, strHeadings
strSqlSelectMain	=	""
strSqlWhere			=	""
strSqlGroupBy		=	""
strSqlOrderBy		=	""
strHeadings			=	""

Dim bDate, bStore, bReshipReason, bModel, bCategory, bItemID, bActivityDesc, bDetails
bDate			=	false
bStore			=	false
bActivityDesc	=	false
bReturnReason	=	false
bModel			=	false
bCategory		=	false
bItemID			=	false
bDetails		=	false

For i = 0 To Cint(UBound(arrGroupBy))
	If "" <> trim(arrGroupBy(i)) Then
		Select Case ucase(trim(arrGroupBy(i)))
			Case "DATE"
				If bDate Then
					Response.Write "DATE in [GROUP BY] is duplicated"
					Response.End
				End If
				bDate = true

				strSqlSelectMain	=	strSqlSelectMain 	& 	", convert(varchar(10), a.logdate, 20) logDate"
				strSqlGroupBy		=	strSqlGroupBy		&	"convert(varchar(10), a.logdate, 20), "
				strSqlOrderBy		=	strSqlOrderBy		&	"convert(varchar(10), a.logdate, 20), "
				strHeadings			=	strHeadings			&	"Log Date" & vbTab

			Case "STORE"
				If bStore Then
					Response.Write "STORE in [GROUP BY] is duplicated"
					Response.End
				End If
				bStore = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.site_id"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.site_id, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.site_id, "				
				strHeadings			=	strHeadings			&	"SITE" & vbTab

			Case "RETURN REASON"
				If bReshipReason Then
					Response.Write "Return Reason in [GROUP BY] is duplicated"
					Response.End
				End If
				bReshipReason = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", isnull(c.reason, '')"
				strSqlGroupBy		=	strSqlGroupBy		&	"isnull(c.reason, ''), "
				strSqlOrderBy		=	strSqlOrderBy		&	"isnull(c.reason, ''), "
				strHeadings			=	strHeadings			&	"Return Reason" & vbTab

			Case "MODEL"
				If bModel Then
					Response.Write "Model in [GROUP BY] is duplicated"
					Response.End
				End If
				bModel = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", f.modelName"
				strSqlGroupBy		=	strSqlGroupBy		&	"f.modelName, "
				strSqlOrderBy		=	strSqlOrderBy		&	"f.modelName, "				
				strHeadings			=	strHeadings			&	"Model" & vbTab

			Case "CATEGORY"
				If bCategory Then
					Response.Write "Category in [GROUP BY] is duplicated"
					Response.End
				End If
				bCategory = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", g.typename"
				strSqlGroupBy		=	strSqlGroupBy		&	"g.typename, "
				strSqlOrderBy		=	strSqlOrderBy		&	"g.typename, "				
				strHeadings			=	strHeadings			&	"Category" & vbTab

			Case "ITEMID"
				If bItemID Then
					Response.Write "ItemID in [GROUP BY] is duplicated"
					Response.End
				End If
				bItemID = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.itemid"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.itemid, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.itemid, "				
				strHeadings			=	strHeadings			&	"Item ID" & vbTab

			Case "ACTIVITY DESC"
				If bActivityDesc Then
					Response.Write "Activity Desc in [GROUP BY] is duplicated"
					Response.End
				End If
				bActivityDesc = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.activity_desc"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.activity_desc, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.activity_desc, "				
				strHeadings			=	strHeadings			&	"Activity Desc" & vbTab

			Case "DETAILS"
				bDetails = true			
		End Select
	End If
Next

filename	=	"RmaReturnReport_" & Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now) & ".xls"

Response.ContentType = "application/vnd.ms-excel"
'Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
Response.AddHeader "Content-Disposition","attachment;filename="""&filename&""""
'Response.Charset = "" 
'Me.EnableViewState = False

response.write "RMA Return/Refund Report" & vbNewLine

strSqlWhere = ""
if siteid <> "" 		then strSqlWhere = strSqlWhere & "	and	a.site_id = " & siteid & vbcrlf
if returnReasonID <> 0 	then strSqlWhere = strSqlWhere & "	and	a.returnReason = " & returnReasonID & vbcrlf
if modelid <> 0			then strSqlWhere = strSqlWhere & "	and	f.modelid = " & modelid & vbcrlf
if typeid <> 0			then strSqlWhere = strSqlWhere & "	and	e.typeid = " & typeid & vbcrlf
		
if bDetails then
	sql	=	"select	a.id, convert(varchar(10), a.logdate, 20) logdate, a.site_id, a.orderid, a.itemid, isnull(a.qty, 0) qty, a.partnumber, b.rma_token lastRmaStatus, a.activity_desc" & vbcrlf & _
			"	,	isnull(a.origItemPrice, 0) origItemPrice, isnull(a.restocking_fee_percent, 0) restocking_fee_percent, isnull(a.itemTax, 0) itemTax" & vbcrlf & _
			"	, 	isnull(a.discountPercent, 0) discountPercent, isnull(a.amountCredited, 0) amountCredited, a.exchangedPartnumber" & vbcrlf & _
			"	,	isnull(a.backInStock, 0) backInStock" & vbcrlf & _
			"	, 	a.confirmAmountCredited, isnull(c.reason, '') returnreason, d.fname userName" & vbcrlf & _
			"from	rmalog a join xrmatype b" & vbcrlf & _
			"	on	a.rmastatus = b.id left outer join xreturnreason c" & vbcrlf & _
			"	on	a.returnreason = c.id left outer join we_adminUsers d" & vbcrlf & _
			"	on	a.adminID = d.adminID left outer join we_items e" & vbcrlf & _
			"	on	a.itemid = e.itemid left outer join we_models f" & vbcrlf & _
			"	on	e.modelid = f.modelid" & vbcrlf & _
			"where	a.logDate >= '" & strSDate & "'" & vbcrlf & _
			"	and	a.logDate < '" & dateadd("d", 1, strEDate) & "'" & vbcrlf & _
			strSqlWhere & vbcrlf & _
			"order by 1 desc"
	set rs = oConn.execute(sql)
	if rs.eof then
		response.write "No data to display"
	else
		response.write "SITE" & vbTab
		response.write "Log Date" & vbTab
		response.write "ORDERID" & vbTab
		response.write "ITEMID" & vbTab
		response.write "QTY" & vbTab
		response.write "PART NUMBER" & vbTab
		response.write "Last RMAStatus" & vbTab
		response.write "Activity Desc" & vbTab
		response.write "Credit Amount" & vbTab
		response.write "Credit Issued" & vbTab
		response.write "Return Reason" & vbTab
		response.write "Admin User" & vbNewLine

		lap = 0
		nTotalQty = clng(0)
		nTotalCredit = cdbl(0)
		do until rs.eof
			nTotalQty = nTotalQty + clng(rs("qty"))
			nTotalCredit = nTotalCredit + cdbl(rs("amountCredited"))
			lap = lap + 1
			select case rs("site_id")
				case 0 : site = "WE"
				case 1 : site = "CA"
				case 2 : site = "CO"
				case 3 : site = "PS"
				case 10 : site = "TM"
			end select
			
			confirmAmountCredited = "N"
			if rs("confirmAmountCredited") then confirmAmountCredited = "Y"
			
			response.write site & vbTab
			response.write rs("logdate") & vbTab
			response.write rs("orderid") & vbTab
			response.write rs("itemid") & vbTab
			response.write rs("qty") & vbTab
			response.write rs("partnumber") & vbTab
			response.write rs("lastRmaStatus") & vbTab
			response.write rs("activity_desc") & vbTab
			response.write formatcurrency(rs("amountCredited")) & vbTab
			response.write confirmAmountCredited & vbTab
			response.write rs("returnreason") & vbTab
			response.write rs("userName") & vbNewLine

			rs.movenext
		loop

		response.write "Total QTY: " & formatnumber(nTotalQty,0) & vbTab
		response.write "Total Credit: " & formatcurrency(nTotalCredit,2) & vbNewLine
	end if
else
	if "" <> strSqlGroupBy then
		strSqlGroupBy	=	"group by " & left(trim(strSqlGroupBy), len(trim(strSqlGroupBy))-1)
		strSqlOrderBy	=	"order by " & left(trim(strSqlOrderBy), len(trim(strSqlOrderBy))-1)
	end if
	
	sql	=	"select	isnull(sum(a.qty), 0) qty, isnull(sum(a.amountCredited), 0) amountCredited " & strSqlSelectMain & vbcrlf & _
			"from	rmalog a join xrmatype b" & vbcrlf & _
			"	on	a.rmastatus = b.id left outer join xreturnreason c" & vbcrlf & _
			"	on	a.returnreason = c.id left outer join we_adminUsers d" & vbcrlf & _
			"	on	a.adminID = d.adminID left outer join we_items e" & vbcrlf & _
			"	on	a.itemid = e.itemid left outer join we_models f" & vbcrlf & _
			"	on	e.modelid = f.modelid left outer join we_types g" & vbcrlf & _
			"	on	e.typeid = g.typeid" & vbcrlf & _
			"where	a.logDate >= '" & strSDate & "'" & vbcrlf & _
			"	and	a.logDate < '" & dateadd("d", 1, strEDate) & "'" & vbcrlf & _
			strSqlWhere & vbcrlf & _
			strSqlGroupBy & vbcrlf & _					
			strSqlOrderBy
	arrResult = getDbRows(sql)
	
	response.write strHeadings
	response.write "QTY" & vbTab
	response.write "Credit Amount" & vbNewLine

	if not isnull(arrResult) then
		nTotalQty = clng(0)
		nTotalCredit = cdbl(0)
		for nRow=0 to ubound(arrResult,2)
			nTotalQty = clng(nTotalQty) + clng(arrResult(0,nRow))
			nTotalCredit = cdbl(nTotalCredit) + cdbl(arrResult(1,nRow))

			nSkipColumn = 2
			for i=0 to ubound(arrGroupBy)
				if "" <> trim(arrGroupBy(i)) then
					select case ucase(trim(arrGroupBy(i)))
						Case "STORE"
							select case cint(arrResult(nSkipColumn,nRow))
								case 0 : site = "WE"
								case 1 : site = "CA"
								case 2 : site = "CO"
								case 3 : site = "PS"
								case 10 : site = "TM"
							end select
							response.write site & vbTab
						case else
							response.write arrResult(nSkipColumn,nRow) & vbTab
					end select
					nSkipColumn = nSkipColumn + 1										
				end if
			next
			response.write formatnumber(arrResult(0,nRow),0) & vbTab
			response.write formatcurrency(arrResult(1,nRow)) & vbNewLine
		next
		response.write "Total QTY: " & formatnumber(nTotalQty,0) & vbTab
		response.write "Total Credit: " & formatcurrency(nTotalCredit,2) & vbNewLine
	end if
end if
%>