<%
pageTitle = "Admin - Generate Refund Reason Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" width="750" align="center" cellpadding="0" cellspacing="0"><tr><td>
<p>&nbsp;</p>
<p class="biggerText"><%=pageTitle%></p>
<br>

<%
strError = ""
	StartDate = request("dc1")
	EndDate = request("dc2")
	if not isDate(StartDate) then
		strError = "Start Date must be a valid date."
	else
		StartDate = dateValue(StartDate)
	end if
	if not isDate(EndDate) then
		strError = "End Date must be a valid date."
	else
		EndDate = dateValue(EndDate)
	end if
	if strError = "" then
		if StartDate > EndDate then
			strError = "Start Date must be earlier than or equal to End Date."
		end if
	end if
	if strError = "" then
		response.write "<h3>" & StartDate & "&nbsp;to&nbsp;" & EndDate & "</h3>" & vbcrlf
		
		for a = 0 to 3
			select case a
				case 0 : site = "WE"
				case 1 : site = "CA"
				case 2 : site = "CO"
				case 3 : site = "PS"
			end select
			
			SQL2 = "SELECT sum(a.refundamount) as refundAmount, refundreason "
			SQL2 = SQL2 & " FROM we_orders A INNER JOIN " & site & "_accounts B ON A.accountid = B.accountid"
			SQL2 = SQL2 & " WHERE A.refunddate >= '" & StartDate & "' AND A.refunddate < '" & dateAdd("D",1,EndDate) & "'"
			SQL2 = SQL2 & " AND A.refundAmount IS NOT NULL"
			SQL2 = SQL2 & " AND (extOrderType IS NULL OR extOrderType <= 3)"
			SQL2 = SQL2 & " AND (B.email = 'phoneorder@wirelessemporium.com' OR B.email NOT LIKE '%@wirelessemporium.com')"
			SQL2 = SQL2 & " AND A.store = '" & a & "'"
			SQL2 = SQL2 & " GROUP BY RefundReason"
			set RS2 = Server.CreateObject("ADODB.Recordset")
			RS2.open SQL2, oConn, 3, 3
			response.write "<h3>" & site & ":</h3>" & vbcrlf
			dim GrandTotal, cancelled, cancelledTotal, refundAmount
			GrandTotal = 0
			cancelled = 0
			cancelledTotal = 0
			
			if not RS2.eof then
				%>
				<table border="1" width="100%" cellpadding="2" cellspacing="0">
					<tr>
						<td width="50%"><b>Refund Reason</b></td>
						<td width="50%"><b>Refund Amount</b></td>
					</tr>
					<%
					do until RS2.eof
				%>
					<tr>
						<td align="left">&nbsp;
						<%
						if rs2("refundReason") = 1 then
						Response.Write(" Defective product")
						elseif rs2("refundReason") = 2 then
						Response.Write(" Wrong Item Shipped")
						elseif rs2("refundReason") = 3 then
						Response.Write(" Broken on arrival")
						elseif rs2("refundReason") = 4 then
						Response.Write(" Did not want")
						elseif rs2("refundReason") = 5 then
						Response.Write(" Returned to sender")
						elseif rs2("refundReason") = 6 then
						Response.Write(" Ordered wrong item")
						elseif rs2("refundReason") = 7 then
						Response.Write(" Shipped wrong item")
						elseif rs2("refundReason") = 8 then
						Response.Write(" No longer needed.")
						else 
						Response.Write("No reason")
						end if
						
						if not isnull( rs2("refundAmount")) then
						refundAmount = rs2("refundAmount")
						else
						refundAmount = 0 
						end if
						%>
                        </td>
						<td align="left">&nbsp;<%=formatcurrency(refundAmount)%></td>
					</tr>
				<%		
						RS2.movenext
					loop
					%>
				</table>
				<%
			else
				response.write "<p class=""boldText"">No records matched.</p><p class=""boldText"">No report generated.</p>"
			end if
			response.write "<p>&nbsp;</p>"
		next
		response.write "<p>&nbsp;</p>"
		response.write "<p class=""normalText""><a href=""report_Orders.asp"">Generate another Orders Report</a></p>"
		RS2.close
		set RS2 = nothing
	end if
%>
</td></tr></table>
<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
