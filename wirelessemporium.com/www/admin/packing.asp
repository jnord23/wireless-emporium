<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
if prepInt(request.Cookies("adminID")) = 0 then
	myServer = request.servervariables("SERVER_NAME")
	call responseRedirect("https://" & myServer & "/admin/")
end if

processPDF = prepStr(request.querystring("p"))
sID = prepStr(request.querystring("sid"))	'siteid overwrite
processPDF = replace(replace(processPDF, "[[", "("), "]]", ")")
if sID = "" then sID = "null"

sql	=	"exec processOrders '', '', '', '', '" & processPDF & "', " & sID
'response.write sql
session("errorSQL") = sql
set rs = oConn.execute(sql)
if rs.state = 1 then
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
'response.write sql
%>
<html>
	<head>
		<title>Packing Slips</title>
		<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,600,700,300,800' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="//www.wirelessemporium.com/includes/css/packing.css" type="text/css" />
		<script src="//code.jquery.com/jquery-latest.min.js"></script>
		<!--<script src="//www.wirelessemporium.com/includes/css/jquery-barcode.min.js"></script>-->
	</head>
	<body>
    <div id="popCover" onclick="closePopup()" style="height:100%; display:none; width:100%; background-color:#333; position:fixed; left:0px; top:0px; z-index:3000; opacity:0.6; filter:alpha(opacity=60);">&nbsp;</div>
    <div id="popBox" style="display:none; position:fixed; left:20%; right:20%; top:0px; z-index:3001; margin:auto; text-align:center;"></div>
    <%
	set fs = CreateObject("Scripting.FileSystemObject")
	const MAX_ITEMDISPLAY = 6
	lastOrderID 				= 	-1
	gblCurItemQty				=	clng(0)		'for footer in PDF
	invoiceCount				=	0
	
	gblCurPageNum				=	cint(1)
	gblTotalPageNum				=	cint(1)
	gblCurLineItemNum			=	cint(0)

'	response.write "<div class=""page""><table width=""100%"" cellpadding=""0"" cellspacing=""0"">"
	do until rs is nothing
		do until rs.eof
			siteShortDesc	=	rs("shortDesc")
			siteID 			=	rs("site_id")
			curOrderID		=	rs("orderid")
			pathPDF			=	rs("pathPDF")
			shipToken2		=	rs("shipToken2")
			numLineItems	=	cint(rs("numRegularOrder"))
			
			'new page
			if lastOrderID <> curOrderID then
				invoiceCount = invoiceCount + 1
				if (invoiceCount mod 100) = 0 then response.flush
				if invoiceCount <> 1 then
					response.write "</table></div>"
				end if
				response.write "<div class=""page""><table width=""100%"" cellpadding=""0"" cellspacing=""0"">"
				gblCurPageNum = 1
				gblCurLineItemNum = 0
				if numLineItems <= MAX_ITEMDISPLAY then 
					gblTotalPageNum = 1
				else
					gblTotalPageNum = int(numLineItems / MAX_ITEMDISPLAY)	'round down
					if (numLineItems mod MAX_ITEMDISPLAY) >=1 then gblTotalPageNum = gblTotalPageNum + 1
				end if
				
				'================================ START NEW PAGE ===================================
				gblCurItemQty = 0
				call appendHeader(rs)
				if rs("promoItemID") > 0 then call appendItem(rs, true)
				'//================================ START NEW PAGE ===================================
			end if

			call appendItem(rs, false)
			
			if gblTotalPageNum > 1 then
				if gblCurLineItemNum >= MAX_ITEMDISPLAY then
					gblCurLineItemNum = 0
					gblCurPageNum = gblCurPageNum + 1
					if gblCurPageNum <= gblTotalPageNum then
						response.write "</table></td></tr></table></table></div><div class=""page""><table width=""100%"" cellpadding=""0"" cellspacing=""0"">"
						call appendHeader(rs)
					end if
				end if
			end if

'			end if
			lastOrderID = curOrderID
			rs.movenext
		loop

'		call drawTable(oPage, oTable, 42, 613)	'draw last one.

		set rs = rs.nextrecordset
	loop
	response.write "</table></div>"
	
	sub appendHeader(byref pRS)
	%>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="100%">
                	<table border="0" width="100%" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td align="left" class="invoiceSeq"><%'=invoiceCount%></td>
                            <td align="right">Page <%=gblCurPageNum%>/<%=gblTotalPageNum%></td>
                        </tr>
                    </table>
				</td>
			</tr>
			<tr>
				<td class="header">
					<%=pRS("longDesc")%>.com&nbsp;&nbsp;&nbsp;&#9679;&nbsp;&nbsp;&nbsp;1410 N. Batavia St., Orange, CA 92687&nbsp;&nbsp;&nbsp;&#9679;&nbsp;&nbsp;&nbsp;
                    <%if pRS("site_id") = 2 then%>
	                    1 (800) 871-6926
                    <%else%>
    	                1 (800) 305-1106
                    <%end if%>
				</td>
			</tr>
			<tr>
				<td>
	                <%if pRS("site_id") = 0 then%>
					<div class="logo" style="margin-top:30px;">
						<img src="//www.wirelessemporium.com/images/admin/packing/we-logo.png" width="100%" />
					</div>
    	            <%elseif siteID = 2 then%>
					<div class="logo">
						<img src="//www.cellularoutfitter.com/images/co_packingslip_logo.png" width="80%" />
					</div>
        	        <%end if%>
					<div class="barcode-container">
						<div class="barcode-top">
							<div class="date"><b>Date:</b><br /><%=left(pRS("orderdatetime"), 10)%></div>
							<div class="ordernum"><b>Order #:</b><br /><span id="ordernum"><%=pRS("orderid")%></span></div>
						</div>
						<div class="barcode">
	                        <img src="//www.wirelessemporium.com/framework/utility/barcode/barcode.asp?code=<%=pRS("orderid")%>&height=40&width=2&mode=code39&text=0">
                        </div>
						<div class="barcode-bottom"><%=pRS("shipType2")%></div>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="billing">
						<div class="bill-label">Bill To:</div>
						<div class="bill-address">
							<%=pRS("fname")%>&nbsp;<%=pRS("lname")%><br />
							<%=pRS("bAddress1")%>&nbsp;<%=pRS("bAddress2")%><br />
							<%=pRS("bCity")%>,&nbsp;<%=pRS("bState")%>&nbsp;<%=rs("bZip")%><br />
							<%=pRS("phone")%><br />
							<%=pRS("email")%>
						</div>
					</div>
					<div class="shipping">
						<div class="ship-label">Ship To:</div>
						<div class="ship-address">
							<%=pRS("fname")%>&nbsp;<%=pRS("lname")%><br />
							<%=pRS("sAddress1")%>&nbsp;<%=pRS("sAddress2")%><br />
							<%=pRS("sCity")%>,&nbsp;<%=pRS("sState")%>&nbsp;<%=pRS("sZip")%><br />
							<%=pRS("phone")%>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="items-header">
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="col-itemnum">Part Number</td>
							<td class="col-product">Product</td>
							<td class="col-qty">Qty</td>
							<td class="col-desc">Description</td>
							<td class="col-price">Unit Price</td>
							<td class="col-total">Total</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td>
					<table class="items" width="100%" cellpadding="0" cellspacing="0">
	<%
	end sub
	
	sub appendItem(byref pRS, pPromo)
		gblCurLineItemNum	=	gblCurLineItemNum + 1
		curMasterQty		=	pRS("curMasterQty")
		tOrderTypeID 		= 	pRS("ordertypeid")
		locAisle 			=	pRS("aisle")
		locShelf			=	pRS("shelf")
		locRow 				=	pRS("row")
		if isnull(locAisle) then locAisle = ""
		if isnull(locShelf) then locShelf = ""
		if isnull(locRow) then locRow = ""
		
		if pPromo then
			tItemDesc			=	"*Promo: " & pRS("promoItemDesc")
			nQty 				= 	prepInt(pRS("promoItemQty"))
			tPartNumber			= 	pRS("promoPartnumber")
			itemPrice 			= 	prepInt(0)
			itemPic				=	pRS("promoItemImg")
			
			locAisle = ""
			locShelf = ""
			locRow = ""
		else
			tItemDesc			=	pRS("itemdesc")
			nQty 				= 	prepInt(pRS("quantity"))
			tPartNumber			= 	pRS("Partnumber")
			itemPrice 			= 	prepInt(pRS("price"))
			itemPic				=	pRS("itempic")
		end if

		itemTotalPrice 		= 	prepInt(nQty) * itemPrice
		gblCurItemQty		=	gblCurItemQty + clng(nQty)
		
		if pRS("site_id") = 2 then
			ImgFolderPath = "C:\inetpub\wwwroot\productpics_co\thumb\"
			itemImgPath = ImgFolderPath & itemPic
			itemImgURL = "//www.cellularoutfitter.com/productpics/thumb/" & itemPic
			if not fs.FileExists(itemImgPath) then itemImgURL = "//www.cellularoutfitter.com/productpics/thumb/imagena.jpg"
		else
			ImgFolderPath = "C:\inetpub\wwwroot\productpics\thumb\"
			itemImgPath = ImgFolderPath & itemPic
			itemImgURL = "//www.wirelessemporium.com/productpics/thumb/" & itemPic
			if not fs.FileExists(itemImgPath) then itemImgURL = "//www.wirelessemporium.com/productpics/thumb/imagena.jpg"
		end if
		%>
		<tr>
			<td class="col-itemnum">
				<span style="font-size:16px;"><%=tPartNumber%></span>
                <br />
                <%if locAisle <> "" or locShelf <> "" or locRow <> "" then%>
	                <%=locAisle%>, <%=locShelf%>, <%=locRow%>
                <%end if%>
			</td>
			<td class="col-product"><img src="<%=itemImgURL%>" width="56" /></td>
			<td class="col-qty">
            <%if nQty > 1 then%>
				<span style="font-size:16px; font-weight:bold;"><%=formatnumber(nQty, 0)%></span>
            <%else%>
				<span style="font-size:15px; font-weight:normal;"><%=formatnumber(nQty, 0)%></span>
            <%end if%>
			</td>
			<td class="col-desc"><%=tItemDesc%>
		<%if pRS("isVendorOrder") then%>
			<br><span style="font-size:9px;">(This item will ship separately from the manufacturer.)</span>
		<%end if%>
			</td>
			<td class="col-price"><%=formatcurrency(itemPrice, 2)%></td>
			<td class="col-total"><%=formatcurrency(itemTotalPrice, 2)%></td>
		</tr>
		<%
		if gblCurItemQty = pRS("itemTotalQty") then
			call appendOrderTotal(pRS)
		end if
	end sub
	
	sub appendOrderTotal(byref pRS)
		tOrderTypeID 		= 	pRS("ordertypeid")
		tOrderType			=	pRS("ordertype")
		nBuySafeAmount 		= 	prepInt(pRS("BuySafeAmount"))
		nOrderSubTotal		=	prepInt(pRS("ordersubtotal"))
		nTaxAmount			=	prepInt(pRS("orderTax"))
		nShipFee			=	prepInt(pRS("ordershippingfee"))
		nOrderGrandTotal	=	prepInt(pRS("ordergrandtotal"))
		nDiscount			=	prepInt(pRS("discount"))
		promoCode			=	pRS("promoCode")
		promoDes			=	pRS("couponDesc")

		if nShipFee = 0 then
			strShipping	= "<b>FREE</b>"
		else
			strShipping	= formatcurrency(nShipFee, 2)
		end if
		
		%>
                    </table>
                </td>
            </tr>
            <tr>
                <td><div class="total-title">Order Total</div></td>
            </tr>
            <tr>
                <td>
                    <div class="totals">
                        <div class="total-left">
                            <div class="paid-by"><b>Paid By:</b> <%=tOrderType%></div>
                            <%
                            if pRS("site_id") = 0 then
                                if pRS("shoprunnerid") <> "" then
                                %>
                                <div class="free-shipping">
                                    <div class="shoprunner-logo"><img src="//www.wirelessemporium.com/images/admin/packing/shoprunner.png" width="66" /></div>
                                    <div class="shoprunner-notes">Thanks for using <b>FREE 2-Day Shipping</b> by ShopRunner! For returns, visit https://www.shoprunner.com/returns to print your pre-paid return label. Don't forget, invite a Friend to join.</div>
                                </div>
                                <%
                                else
                                %>
                                <div class="free-shipping">
                                    <div class="shoprunner-logo"><img src="//www.wirelessemporium.com/images/admin/packing/shoprunner.png" width="66" /></div>
                                    <div class="shoprunner-notes">Next time get <b>FREE 2-Day Shipping</b> from Wireless Emporium and other great retailers with your ShopRunner membership. Visit <u>wirelessemporium.com/shoprunner</u> to signup for a <b>FREE 30-Day Trial</b>.</div>
                                </div>
                                <%
                                end if
                            end if
                            %>
                        </div>
                        <div class="total-right">
                            <div class="total-item">
                                <div class="label">Subtotal:</div>
                                <div class="amount"><%=formatcurrency(nOrderSubTotal,2)%></div>
                            </div>
                            <%if nDiscount > 0 then%>
                            <div class="total-item">
                                <div class="label">Discount:</div>
                                <div class="amount">(<%=formatcurrency(nDiscount,2)%>)</div>
                            </div>
                            <%end if%>
                            <%if promoCode <> "" then%>
                            <div class="total-item">
                                <div class="label">Promo Code:</div>
                                <div class="amount">
                                	<b><%=promoCode%></b><br />
                                	<%=promoDesc%>
								</div>
                            </div>
                            <%end if%>
                            <div class="total-item">
                                <div class="label">Sales Tax:</div>
                                <div class="amount"><%=formatcurrency(orderTax,2)%></div>
                            </div>
                            <div class="total-item">
                                <div class="label">Shipping:</div>
                                <div class="amount"><%=strShipping%></div>
                            </div>
                            <div class="total-item">
                                <div class="label">Grand Total:</div>
                                <div class="amount"><%=formatcurrency(nOrderGrandTotal,2)%></div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
            	<td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                            <%if pRS("site_id") = 0 then%>
                            	<div class="WE">
                                    <div class="stamp-zone"><img src="//www.wirelessemporium.com/images/admin/packing/stamp-zone.png" height="168" /></div>
                                    <div class="offer"><img src="//www.wirelessemporium.com/images/admin/packing/offer.png" width="588" /></div>
                                </div>
                            <%elseif pRS("site_id") = 2 then%>
                            	<div class="CO">
                                    <div class="stamp-zone"><img src="//www.wirelessemporium.com/images/admin/packing/stamp-zone.png" height="168" /></div>
                                    <div class="offer"><img src="//www.wirelessemporium.com/images/admin/packing/co_bottom_offer.png" width="588" /></div>
                                </div>
                            <%end if%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <%
	end sub	
	%>
    
	<script type="text/javascript">
		/*
        $(document).ready(function(){
            var btype = "code39";
            var renderer = "css";
            
			var settings = {
				output:renderer,
				bgColor: "#ffffff",
				color: "#000000",
				barWidth: "2",
				barHeight: "40",
				moduleSize: "5",
				posX: "10",
				posY: "3",
				addQuietZone: "1",
				showHRI: false
            };

			$(".ordernum").each(function() {
				value = $(this).find('#ordernum').html();
				$(".barcode").html("").show().barcode(value, btype, settings);
			});
            //$(".barcode").hide();
        });
		*/
    </script>
	</body>
</html>
<%
end if
call CloseConn(oConn)
%>