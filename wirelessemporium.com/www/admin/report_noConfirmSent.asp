<%
pageTitle = "WE Admin Site - Orders with No ConfirmSent"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">

<%
'response.buffer = false

if request("submitted") <> "" then
	'Response.ContentType = "application/vnd.ms-excel"
	
	strError = ""
	dateStart = request("dateStart")
	dateEnd = request("dateEnd")
	
	if not isDate(dateStart) or not isDate(dateEnd) then
		strError = strError & "<p>dateStart and dateEnd must both be valid dates</p>"
	else
		dateStart = dateValue(dateStart)
		dateEnd = dateValue(dateEnd)
		if dateEnd < dateStart then strError = strError & "<p>dateEnd must be later than or equal to dateStart</p>"
	end if
	
	showblank = "&nbsp;"
	shownull = "-null-"
	
	if strError = "" then
		dateEnd = dateAdd("D",1,dateEnd)
		SQL = "SELECT orderid, extOrderType, ordershippingfee, ordergrandtotal, shiptype, orderdatetime, thub_posted_date FROM we_orders"
		SQL = SQL & " WHERE approved = 1 AND confirmSent IS NULL AND store = 0 AND cancelled IS NULL AND (extOrderType < 4 OR extOrderType IS NULL)"
		SQL = SQL & " AND thub_posted_date >= '" & dateStart & "' AND thub_posted_date <= '" & dateEnd & "'"
		SQL = SQL & " AND store = 0"
		SQL = SQL & " ORDER BY orderid DESC"
		set RS = Server.CreateObject("ADODB.Recordset")
		response.write "<h3>" & SQL & "</h3>" & vbCrLf
		'response.end
		RS.open SQL, oConn, 3, 3
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			response.write "<h3>" & RS.recordcount & " TOTAL records found</h3>" & vbCrLf
			%>
			<table border="1">
				<tr>
					<%
					for each whatever in RS.fields
						%>
						<td><b><%=whatever.name%></b></td>
						<%
					next
					%>
				</tr>
				<%
				do until RS.eof
					%>
					<tr>
						<%
						for each whatever in RS.fields
							thisfield = whatever.value
							if isnull(thisfield) then
								thisfield = shownull
							end if
							if trim(thisfield) = "" then
								thisfield = showblank
							end if
							%>
							<td valign="top"><%=thisfield%></td>
							<%
						next
						%>
					</tr>
					<%
					RS.movenext
				loop
				%>
			</table>
			<%
		end if
		
		RS.close
		Set RS = nothing
	else
		%>
		<center>
		<font size="+2" color="red"><%=strError%></font>
		<a href="javascript:history.back();">BACK</a>
		</center>
		<%
	end if
else
	%>
	<p>&nbsp;</p>
	<h3>Questionable Orders Report</h3>
	<br>
	<form action="report_noConfirmSent.asp" method="post">
		<p><input type="text" name="dateStart">&nbsp;&nbsp;dateStart</p>
		<p><input type="text" name="dateEnd">&nbsp;&nbsp;dateEnd</p>
		<p><input type="submit" name="submitted" value="Generate Report"></p>
	</form>
	<%
end if
%>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
