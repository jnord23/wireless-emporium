<%
	thisUser = Request.Cookies("username")
	pageTitle = ""
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim siteID : siteID = prepInt(request.QueryString("siteID"))
	dim typeID : typeID = prepInt(request.QueryString("typeID"))
	dim reportID : reportID = prepInt(request.QueryString("reportID"))
	
	function customSEO(val)
		val = replace(val," ","-")
		customSEO = lcase(val)
	end function
	
	if typeID > 0 then
		if siteID = 0 then
			siteName = "wirelessemporium"
			if reportID = 1 then
				sql = "select distinct b.typeName_WE as typeName, c.brandName, d.modelName, 'sb-' + cast(a.brandID as varchar(2)) + '-sm-' + cast(a.modelID as varchar(5)) + '-sc-' + cast(a.typeID as varchar(2)) + '-' + replace(b.typeName_WE,' ','-') + '-' + replace(c.brandName,' ','-') + '-' + replace(d.modelName,' ','-') + '.asp' as URL from we_Items a join we_types b on a.typeID = b.typeID join we_brands c on a.brandID = c.brandID join we_models d on a.modelID = d.modelID where a.hideLive = 0 and a.price_our is not null and a.price_our > 0 and a.typeID = " & typeID
			else
				sql = "select distinct b.price_our as price, b.cogs, c.brandName, a.modelName, d.typeName_WE as typeName, b.itemDesc, 'p-' + cast(b.itemID as varchar(12)) + '-productDetails.asp' as URL from we_Models a join we_Items b on a.modelID = b.modelID and b.hideLive = 0 and b.ghost = 0 join we_Brands c on a.brandID = c.brandID join we_Types d on b.typeID = d.typeID where b.price_our is not null and b.price_our > 0 and b.typeID = " & typeID
			end if
		elseif siteID = 1 then
			siteName = "cellphoneaccents"
			if reportID = 1 then
				sql = "select distinct b.typeName_CA as typeName, c.brandName, d.modelName, 'sb-' + cast(a.brandID as varchar(2)) + '-sm-' + cast(a.modelID as varchar(5)) + '-sc-' + cast(a.typeID as varchar(2)) + '-' + replace(c.brandName,' ','-') + '-' + replace(d.modelName,' ','-') + '-' + replace(b.typeName_CA,' ','-') + '.html' as URL from we_Items a join we_types b on a.typeID = b.typeID join we_brands c on a.brandID = c.brandID join we_models d on a.modelID = d.modelID where a.hideLive = 0 and a.price_our is not null and a.price_our > 0 and a.typeID = " & typeID
			else
				sql = "select distinct b.price_CA as price, b.cogs, c.brandName, a.modelName, d.typeName_CA as typeName, b.itemDesc_CA as itemDesc, 'p-' + cast(b.itemID as varchar(12)) + '-productDetails.html' as URL from we_Models a join we_Items b on a.modelID = b.modelID and b.hideLive = 0 and b.ghost = 0 join we_Brands c on a.brandID = c.brandID join we_Types d on b.typeID = d.typeID where b.price_CA is not null and b.price_CA > 0 and b.typeID = " & typeID
			end if
		elseif siteID = 2 then
			siteName = "cellularOutfitter"
			if reportID = 1 then
				sql = "select distinct b.typeName_CO as typeName, c.brandName, d.modelName, 'sb-' + cast(a.brandID as varchar(2)) + '-sm-' + cast(a.modelID as varchar(5)) + '-sc-' + cast(a.typeID as varchar(2)) + '-' + replace(c.brandName,' ','-') + '-' + replace(d.modelName,' ','-') + '-' + replace(b.typeName_CO,' ','-') + '.html' as URL from we_Items a join we_types b on a.typeID = b.typeID join we_brands c on a.brandID = c.brandID join we_models d on a.modelID = d.modelID where a.hideLive = 0 and a.price_our is not null and a.price_our > 0 and a.typeID = " & typeID
			else
				sql = "select distinct b.price_CO as price, b.cogs, c.brandName, a.modelName, d.typeName_CO as typeName, b.itemDesc_CO as itemDesc, 'sb-' + cast(c.brandID as varchar(2)) + '-sm-' + cast(a.modelID as varchar(5)) + '-sc-' + cast(d.typeID as varchar(2)) + '-productDetails.html' as URL from we_Models a join we_Items b on a.modelID = b.modelID and b.hideLive = 0 and b.ghost = 0 join we_Brands c on a.brandID = c.brandID join we_Types d on b.typeID = d.typeID where b.price_CO is not null and b.price_CO > 0 and b.typeID = " & typeID
			end if
		elseif siteID = 3 then
			siteName = "phonesale"
			if reportID = 1 then
				sql = "select distinct b.typeName_PS as typeName, c.brandName, d.modelName, 'sb-' + cast(a.brandID as varchar(2)) + '-sm-' + cast(a.modelID as varchar(5)) + '-sc-' + cast(a.typeID as varchar(2)) + '-tc-17-' + '-' + replace(c.brandName,' ','-') + '-' + replace(d.modelName,' ','-') + '-' + replace(b.typeName_PS,' ','-') + '.html' as URL from we_Items a join we_types b on a.typeID = b.typeID join we_brands c on a.brandID = c.brandID join we_models d on a.modelID = d.modelID where a.hideLive = 0 and a.price_our is not null and a.price_our > 0 and a.typeID = " & typeID
			else
				sql = "select distinct b.price_PS as price, b.cogs, c.brandName, a.modelName, d.typeName_PS as typeName, b.itemDesc_PS as itemDesc, 'productDetails-p-' + cast(b.itemID as varchar(12)) + '.html' as URL from we_Models a join we_Items b on a.modelID = b.modelID and b.hideLive = 0 and b.ghost = 0 join we_Brands c on a.brandID = c.brandID join we_Types d on b.typeID = d.typeID where b.price_PS is not null and b.price_PS > 0 and b.typeID = " & typeID
			end if
		elseif siteID = 10 then
			siteName = "tabletmall"
			if reportID = 1 then
				sql = "select distinct b.typeName, c.brandName, d.modelName, 'sb-' + cast(a.brandID as varchar(2)) + '-sm-' + cast(a.modelID as varchar(5)) + '-sc-' + cast(a.typeID as varchar(2)) + '-' + replace(b.typeName,' ','-') + '-' + replace(c.brandName,' ','-') + '-' + replace(d.modelName,' ','-') + '.html' as URL from we_Items a join we_types b on a.typeID = b.typeID join we_brands c on a.brandID = c.brandID join we_models d on a.modelID = d.modelID where a.hideLive = 0 and a.price_our is not null and a.price_our > 0 and a.typeID = " & typeID
			else
				sql = "select distinct b.price_ER as price, b.cogs, c.brandName, a.modelName, d.typeName_WE as typeName, b.itemDesc, '' + c.brandName + '-' + a.modelName + '-' + d.typeName as URL from we_Models a join we_Items b on a.modelID = b.modelID and b.hideLive = 0 and b.ghost = 0 join we_Brands c on a.brandID = c.brandID join we_Types d on b.typeID = d.typeID where a.isTablet = 1 and b.price_ER is not null and b.price_ER > 0 and b.typeID = " & typeID
			end if
		end if
		response.Write(sql & "<br>")
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		if not rs.EOF then
			set fs = CreateObject("Scripting.FileSystemObject")
			
			filename = "productUrlMatch.txt"
			path = Server.MapPath("tempCSV") & "\" & filename
			path = replace(path,"admin\","")
			
			if fs.FileExists(path) then fs.DeleteFile(path)
			set file = fs.CreateTextFile(path)
			
			if reportID = 1 then
				file.WriteLine "Brand" & vbtab & "Model" & vbtab & "Category" & vbtab & "URL"
			else
				file.WriteLine "Brand" & vbtab & "Model" & vbtab & "Category" & vbtab & "Product" & vbtab & "URL" & vbtab & "Price" & vbtab & "Cogs"
			end if
			do while not rs.EOF
				if reportID = 1 then
					categoryName = rs("typeName")
					brandName = rs("brandName")
					modelName = rs("modelName")
					url = rs("url")
					url = replace(replace(formatSEO(URL),"-asp",".asp"),"-html",".html")
					if siteID = 3 then url = "accessories/" & lcase(categoryName) & "/" & url
					file.WriteLine brandName & vbtab & modelName & vbtab & categoryName & vbtab & "http://www." & siteName & ".com/" & url
				else
					price = prepInt(rs("price"))
					cogs = prepInt(rs("cogs"))
					brandName = rs("brandName")
					modelName = rs("modelName")
					categoryName = rs("typeName")
					itemDesc = rs("itemDesc")
					url = replace(rs("url"),"productDetails",formatSEO(itemDesc))
					if siteID = 3 then url = "accessories/" & lcase(categoryName) & "/" & url
					file.WriteLine brandName & vbtab & modelName & vbtab & categoryName & vbtab & itemDesc & vbtab & "http://www." & siteName & ".com/" & url & vbtab & formatCurrency(price,2) & vbtab & formatCurrency(cogs,2)
				end if
				rs.movenext
			loop
			file.close()
			response.Write("<a href='/tempCSV/" & filename & "'>Download File</a>")
		else
			response.Write(sql & "<br>")
			response.Write("No Products Found")
		end if
	end if
	
	sql = "select typeID, typeName from we_types order by typeName"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
%>
<form name="urlMatchForm" onsubmit="return(false)">
<div style="height:30px; padding-top:50px;">
    <div style="float:left; padding-left:20px; width:150px; font-weight:bold; font-size:16px;">Select Website</div>
    <div style="float:left; padding-left:10px;">
        <select name="siteID">
            <option value="0">WirelessEmporium</option>
            <option value="1"<% if siteID = 1 then %> selected="selected"<% end if %>>CellphoneAccents</option>
            <option value="2"<% if siteID = 2 then %> selected="selected"<% end if %>>CellularOutfitter</option>
            <option value="3"<% if siteID = 3 then %> selected="selected"<% end if %>>PhoneSale</option>
            <option value="10"<% if siteID = 10 then %> selected="selected"<% end if %>>TabletMall</option>
        </select>
    </div>
</div>
<div style="height:30px;">
    <div style="float:left; padding-left:20px; width:150px; font-weight:bold; font-size:16px;">Report Type</div>
    <div style="float:left; padding-left:10px;">
        <select name="reportID">
            <option value="1">Category Level</option>
            <option value="2">Product Level</option>
        </select>
    </div>
</div>
<div style="height:30px;">
    <div style="float:left; padding-left:20px; width:150px; font-weight:bold; font-size:16px;">Select Category</div>
    <div style="float:left; padding-left:10px;">
        <select name="typeID" onchange="window.location='/admin/productUrlMatch.asp?siteID=' + document.urlMatchForm.siteID.value + '&reportID=' + document.urlMatchForm.reportID.value + '&typeID=' + this.value">
            <option value=""></option>
            <%
            do while not rs.EOF
            %>
            <option value="<%=rs("typeID")%>"><%=rs("typeName")%></option>
            <%
                rs.movenext
            loop
            %>
        </select>
    </div>
</div>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
