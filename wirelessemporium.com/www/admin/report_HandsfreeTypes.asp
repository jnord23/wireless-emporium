<%
pageTitle = "Find Items by Handsfree Type"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<p class="biggerText"><%=pageTitle%></p>

<%
if request("submitted") <> "" then
	showblank = "&nbsp;"
	shownull = "-null-"
	
	SQL = "SELECT B.brandName, C.modelName, A.PartNumber, A.itemDesc, A.numberOfSales AS [# of Sales], A.inv_qty, A.hideLive FROM we_Items A"
	SQL = SQL & " INNER JOIN we_brands B ON A.brandID=B.brandID"
	SQL = SQL & " INNER JOIN we_models C ON A.modelID=C.modelID"
	SQL = SQL & " WHERE A.HandsfreeType='" & request.form("HandsfreeType") & "' ORDER BY A.PartNumber, A.itemDesc"
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	
	if RS.eof then
		response.write "No records matched<br><br>So cannot make table..."
	else
		%>
		<h3><%=RS.recordcount%> items found</h3>
		<table border="1">
			<tr>
				<%
				for each whatever in RS.fields
					%>
					<td><b><%=whatever.name%></b></td>
					<%
				next
				%>
			</tr>
			<%
			do until RS.eof
				%>
				<tr>
					<%
					for each whatever in RS.fields
						thisfield = whatever.value
						if isnull(thisfield) then
							thisfield = shownull
						end if
						if trim(thisfield) = "" then
							thisfield = showblank
						end if
						%>
						<td valign="top"><%=thisfield%></td>
						<%
					next
					%>
				</tr>
				<%
				RS.movenext
			loop
			%>
		</table>
		<%
	end if
	
	RS.close
	Set RS = nothing
end if
%>

<form action="report_HandsfreeTypes.asp" method="post">
	<p>
		<select name="HandsfreeType">
			<option value="1" selected>UNI 2.5mm</option>
			<option value="2">BLUETOOTH</option>
			<option value="3">3.5mm Stereo</option>
			<option value="4">MINI USB</option>
			<option value="5">MICRO USB</option>
			<option value="6">Old Sony Ericcson</option>
			<option value="7">SONY K750/Z520</option>
			<option value="8">LG Chocolate</option>
			<option value="9">NEXT i1000</option>
			<option value="10">NEXT 2.5 w/PTT</option>
			<option value="11">NOK 7210</option>
			<option value="12">NOK 3390</option>
			<option value="13">SAM R225</option>
			<option value="14">SAM T809</option>
			<option value="15">SAM M300/510</option>
			<option value="16">SAM E105/U550</option>
			<option value="17">SAM E315</option>
			<option value="18">SAM S105</option>
			<option value="19">Siemens</option>
			<option value="20">PANT C150</option>
			<option value="21">HTC 3125</option>
			<option value="22">HTC 8525</option>
			<option value="23">APPLE ONLY</option>
			<option value="24">Palm</option>
			<option value="25">Treo 650</option>
			<option value="26">SAM 2.5MM</option>
		</select>
	</p>
	<p><input type="submit" name="submitted" value="Find"></p>
</form>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
