<%
response.buffer = false
pageTitle = "Upload Buy.com Orders"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->

<%
server.scripttimeout = 1000 'seconds
dim Path
Path = server.mappath("tempTXT")

set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
Count = Upload.Save(Path)
if Upload.form("upload") <> "" then
	if Count = 0 then
		response.write "No file selected."
		response.end
	else
		set File = Upload.Files(1)
		myFile = File.path
		
		dim txtConn, myStr
		set txtConn = Server.CreateObject("ADODB.Connection")
		myStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & myFile & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"";"
		txtConn.ConnectionString = myStr
		txtConn.ConnectionTimeout = 1000		'seconds
		txtConn.Open
		response.Write(myFile)
		SQL = "SELECT * FROM [BuyUpload$] ORDER BY [Orderid]"
		response.write "<h3>" & SQL & "</h3>" & vbCrLf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, txtConn, 3, 3
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			%>
			<h3><%=RS.recordcount%> records found</h3>
			<table border="1" cellpadding="1" cellspacing="0"><tr><td>WE Order Number</td><td>Buy.com Order Number</td></tr>
			<%
			dim Order_Number, SKU, Item_Quantity, KIT, holdOrderNumber, nPartNumber
			dim updateCount
			dim nIdProd, sSCountry, sBCountry, sShipType
			holdOrderNumber = 99999999
			do until RS.eof
				'response.write "<ul>" & vbcrlf
				
				Order_Number = SQLquote(RS("Orderid"))
				SKU = SQLquote(RS("SKU"))
				Item_Quantity = SQLquote(RS("Qty"))
				KIT = null
				
				set rsTemp = server.createobject("ADODB.recordset")
				'SQL = "SELECT itemID FROM we_items WHERE PartNumber='" & SKU & "' AND (inv_qty > -1 OR ItemKit = 1)"
				SQL = "SELECT itemID, PartNumber, ItemKit_NEW FROM we_items WHERE BUY_Sku='" & SKU & "'"
				rsTemp.open SQL, oConn, 3, 3
				if not rsTemp.eof then
					nIdProd = rsTemp("itemID")
					nPartNumber = rsTemp("PartNumber")
					if not isNull(rsTemp("ItemKit_NEW")) then KIT = rsPart("ItemKit_NEW")
					if Order_Number = holdOrderNumber then '9999999 inition
						sSprocString = "sp_InsertOrderDetail '" & nOrderId & "','" & nIdProd & "','" & Item_Quantity & "'"
						response.write "<li>" & sSprocString & "</li>" & vbcrlf
						oConn.execute(sSprocString)
					else
						if inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",SQLquote(RS("ShiptoState"))) > 0 then 
						'len(SQLquote(RS("Shipping Country"))) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",SQLquote(RS("ShiptoState"))) > 0 then
							sSCountry = "CANADA"
							sShipType = "First Class Int'l"
						else
							sSCountry = ""
							sShipType = "First Class"
						end if
						sBCountry = ""
						
						ShipToName = split(trim(SQLquote(rs("shiptoName")))," ")
						ShiptoFName = Ucase(Left(Shiptoname(0),1))&Mid(Shiptoname(0),2)
						
						counts=1
						for i=1 to Ubound(ShiptoName)
						counts=counts+1
						Next
					
						if counts=1 then
							shiptoLName=""
						else
							ShiptoLName = Ucase(Left(Shiptoname(counts-1),1))&Mid(Shiptoname(counts-1),2)
						end if
						
						Grand_Total=FormatNumber(CDbl(SQLquote(RS("ProductRevenue")))+CDbl(SQLquote(RS("ShippingFee")))+CDbl(SQLquote(RS("TaxCost"))),2)
						
						SQL = "sp_InsertAccountInfo '" & ShiptoFName & "','" & ShiptoLName & "','" & SQLquote(RS("ShipToStreet1")) & "','" & SQLquote(RS("ShipToStreet2")) & "','" & SQLquote(RS("ShipToCity")) & "','" & SQLquote(RS("ShipToState")) & "','" & SQLquote(RS("ShipToZipcode")) & "','" & sBCountry & "','" & SQLquote(RS("ShipToStreet1")) & "','" & SQLquote(RS("ShipToStreet2")) & "','" & SQLquote(RS("ShipToCity")) & "','" & SQLquote(RS("ShipToState")) & "','" & SQLquote(RS("ShipToZipcode")) & "','" & sSCountry & "','" & SQLquote(RS("BilltoPhone")) & "','" & SQLquote(RS("Email")) & "','','','" & now & "'"
						session("errorSQL") = SQL
						response.write "<li>" & SQL & "</li>" & vbcrlf
						nAccountId = oConn.execute(SQL).fields(0).value
					
						SQL = "sp_InsertOrder2 '" & nAccountId & "','" & FormatNumber(cDbl(SQLquote(RS("ProductRevenue"))))& "','" & FormatNumber(SQLquote(RS("ShippingFee")),2) & "'," & FormatNumber(cDbl(SQLquote(RS("TaxCost"))),2) & ",'" &Grand_Total& "','" & SQLquote(sShipType) & "',null,0,0,'" & SQLquote(RS("OrderDate")) & "'"
						session("errorSQL") = SQL
						response.write "<li>" & SQL & "</li>" & vbcrlf
						nOrderId = oConn.execute(SQL).fields(0).value
						
						SQL = "UPDATE we_orders SET approved=-1, extOrderType='7', extOrderNumber='" & Order_Number & "' WHERE orderid='" & nOrderId & "'"
						session("errorSQL") = SQL
						response.write "<li>" & SQL & "</li>" & vbcrlf
						oConn.execute SQL
						
						SQL = "sp_InsertOrderDetail '" & nOrderId & "','" & nIdProd & "','" & Item_Quantity & "'"
						session("errorSQL") = SQL
						response.write "<li>" & SQL & "</li>" & vbcrlf
						oConn.execute(SQL)
						
						call UpdateInvQty(nPartNumber, Item_Quantity, KIT)
						call NumberOfSales(nIdProd, Item_Quantity, KIT)
						updateCount = updateCount + 1
						holdOrderNumber = Order_Number
					end if
				else
					response.write "<tr><td colspan=""2"">Part Number <b>" & SQLquote(RS("SKU")) & "</b> not found with inv_qty > -1!</td></tr>" & vbcrlf
				end if
				RS.movenext
				
				'response.write "</ul>" & vbcrlf
				response.write "<tr><td>" & nOrderId & "</td><td>" & Order_Number & "</td></tr>" & vbcrlf
			loop
		end if
		RS.close
		set RS = nothing
		%>
		</table>
		<p><%=updateCount%> orders entered into WE database.</p>
       
    <%
	end if
else
	%>
	<h3>[Upload BUY.COM to WE]<br>
    Select the file you saved to your hard drive:<br>
	(IMPORTANT: Make sure that the Worksheet is named "BuyUpload"!)
    </h3>
	<form enctype="multipart/form-data" action="uploadBuyCom.asp" method="POST">
		<p><input type="FILE" name="FILE1"></p>
		<p><input type="SUBMIT" name="upload" value="Upload"></p>
	</form>
	<%
end if

function SQLquote(strValue)
	if not isNull(strValue) and strValue <> "" then
		SQLquote = trim(replace(strValue,"'","''"))
		SQLquote = trim(replace(SQLquote,chr(34),"''"))
	else
		SQLquote = strValue
	end if
end function

sub UpdateInvQty(nPartNumber, nProdQuantity, KIT)
	' Updates inv_qty for the Master Part (item with the same Part Number as the item purchased which does not have -1 inv_qty)
	' If the inv_qty would be adjusted to less than or equal to zero, then all items with the same Part Number as the item purchased are set to zero
	if isNull(KIT) then
		SQL = "SELECT itemID,typeID,vendor,inv_qty FROM we_items WHERE PartNumber = '" & nPartNumber & "' AND inv_qty > -1"
	else
		SQL = "SELECT itemID,typeID,vendor,inv_qty FROM we_items WHERE itemID IN (" & KIT & ")"
	end if
	'response.write "<li>" & SQL & "</li>" & vbcrlf
	set oRsItemInfo2 = Server.CreateObject("ADODB.Recordset")
	oRsItemInfo2.open SQL, oConn, 3, 3
	do until oRsItemInfo2.eof
		if oRsItemInfo2("inv_qty") - nProdQuantity > 0 then
			decreaseSQL = "UPDATE we_items SET inv_qty = inv_qty - " & nProdQuantity & " WHERE itemID = '" & oRsItemInfo2("itemID") & "'"
			'response.write "<li>" & decreaseSQL & "</li>" & vbcrlf
			oConn.execute(decreaseSQL)
		else
			decreaseSQL = "UPDATE we_items SET inv_qty = 0 WHERE PartNumber = '" & nPartNumber & "'"
			if oRsItemInfo2("typeID") = 2 then decreaseSQL = decreaseSQL & " AND inv_qty > -1"
			if oRsItemInfo2("typeID") <> 3 and oRsItemInfo2("vendor") <> "CM" and oRsItemInfo2("vendor") <> "DS" and oRsItemInfo2("vendor") <> "MLD" then
				' Send zero-inventory e-mail to Tony
				cdo_from = "Automatic E-Mail from Wirelessemporium.com<sales@wirelessemporium.com>"
				cdo_subject = nPartNumber & " is out of stock! (Buy.com upload)"
				cdo_body = nPartNumber & " is out of stock! (Buy.com upload)"
				cdo_to = "tony@wirelessemporium.com,charles@wirelessemporium.com,steven@wirelessemporium.com,jon@wirelessemporium.com"
				CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
			end if
			'response.write "<li>" & decreaseSQL & "</li>" & vbcrlf
			oConn.execute(decreaseSQL)
		end if
		oRsItemInfo2.movenext
	loop
	oRsItemInfo2.close
	set oRsItemInfo2 = nothing
end sub

sub NumberOfSales(nIdProd, QTY, KIT)
	if isNull(KIT) then
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID = '" & nIdProd & "'"
	else
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID IN (" & KIT & ")"
	end if
	response.write "<li>" & SQL & "</li>" & vbcrlf
	oConn.execute(SQL)
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
