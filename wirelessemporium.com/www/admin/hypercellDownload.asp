<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - "
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	useURL = "http://www.hypercelturnkey.com/feeds/livefeeds/wirelessemporium/wirelessemporium.xml"
	
	set XMLHTTP = Server.CreateObject("MSXML2.ServerXMLHTTP")
	XMLHTTP.Open "POST", useURL, False
	XMLHTTP.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
	XMLHTTP.Send
	
	allData = XMLHTTP.responseText
	allXML = XMLHTTP.responseXML.xml
	
	allData = mid(allData,instr(allData,"<wirelessemp>"))
	allData = replace(allData,"<wirelessemp>","")
	allData = replace(allData,"</wirelessemp>","")
	allData = replace(allData,"<itemdata>","")
	allData = replace(allData,"</itemdata>","")
	allData = replace(allData,vbcrlf,"")
	allData = replace(allData,"  ","")
	
	allData = replace(allData,"<Hypercel_Part_Number>","")
	allData = replace(allData,"</Hypercel_Part_Number>","^^")
	allData = replace(allData,"<Category1>","")
	allData = replace(allData,"</Category1>","^^")
	allData = replace(allData,"<Category2>","")
	allData = replace(allData,"</Category2>","^^")
	allData = replace(allData,"<CATEGORY3>","")
	allData = replace(allData,"</CATEGORY3>","^^")
	allData = replace(allData,"<Product_Name>","")
	allData = replace(allData,"</Product_Name>","^^")
	allData = replace(allData,"<Description>","")
	allData = replace(allData,"</Description>","^^")
	allData = replace(allData,"<Short_Description>","")
	allData = replace(allData,"</Short_Description>","^^")
	allData = replace(allData,"<Weight>","")
	allData = replace(allData,"</Weight>","^^")
	allData = replace(allData,"<MSRP>","")
	allData = replace(allData,"</MSRP>","^^")
	allData = replace(allData,"<Hypercel_Price>","")
	allData = replace(allData,"</Hypercel_Price>","^^")
	allData = replace(allData,"<Manufacturer>","")
	allData = replace(allData,"</Manufacturer>","^^")
	allData = replace(allData,"<Model>","")
	allData = replace(allData,"</Model>","^^")
	allData = replace(allData,"<Image>","")
	allData = replace(allData,"</Image>","^^")
	allData = replace(allData,"<Manufacturer_Type>","")
	allData = replace(allData,"</Manufacturer_Type>","^^")
	allData = replace(allData,"<Inventory_Quantity>","")
	allData = replace(allData,"</Inventory_Quantity>","^^")
	allData = replace(allData,"<Keywords>","")
	allData = replace(allData,"</Keywords>","^^")
	allData = replace(allData,"<Date_Created>","")
	allData = replace(allData,"</Date_Created>","^^")
	allData = replace(allData,"<UPC>","")
	allData = replace(allData,"</UPC>","##")
	
	productArray = split(allData,"##")
	productsAdded = 0
	
	sql = "select count(*) as startingCnt from holding_hypercell"
	session("errorSQL") = sql
	set startCntRS = oConn.execute(sql)
	dim startingCnt : startingCnt = startCntRS("startingCnt")
	startCntRS = null
	for i = 0 to (ubound(productArray)-1)
		productsAdded = productsAdded + 1
		curArray = split(productArray(i),"^^")
		sql = 	"if not (select count(*) from holding_hypercell where partNumber = '" & prepStr(curArray(0)) & "') > 0 " &_
				"insert into holding_hypercell (partNumber,cat1,cat2,cat3,name,description,shortDesc,weight,msrp,cogs,manufacturer,model,image," &_
				"manType,inv_qty,keywords,hyp_entryDate) values('" & prepStr(curArray(0)) & "','" & prepStr(curArray(1)) & "','" & prepStr(curArray(2)) & "'," &_
				"'" & prepStr(curArray(3)) & "','" & prepStr(curArray(4)) & "','" & prepStr(curArray(5)) & "','" & prepStr(curArray(6)) & "'," &_
				"'" & prepStr(curArray(7)) & "','" & prepStr(curArray(8)) & "','" & prepStr(curArray(9)) & "','" & prepStr(curArray(10)) & "'," &_
				"'" & prepStr(curArray(11)) & "','" & prepStr(curArray(12)) & "','" & prepStr(curArray(13)) & "'," & prepInt(curArray(14)) & "," &_
				"'" & prepStr(curArray(15)) & "','" & prepStr(curArray(16)) & "') " &_
				"else update we_items set inv_qty = " & prepInt(curArray(14)) & " where partNumber like '%-HYP-%' and mobileLine_sku = '" & prepStr(curArray(0)) & "';" &_
				"insert into we_invRecord (itemID,inv_qty,adjustQty,notes) values((select itemID from we_items where partNumber like '%-HYP-%' and " &_
				"mobileLine_sku = '" & prepStr(curArray(0)) & "'),(select inv_qty from we_items where partNumber like '%-HYP-%' and " &_
				"mobileLine_sku = '" & prepStr(curArray(0)) & "')," & prepInt(curArray(14)) & ",'Hypercell Qty Update')"
		session("errorSQL") = sql
		oConn.execute(sql)
	next
	sql = "select count(*) as endCnt from holding_hypercell"
	session("errorSQL") = sql
	set endCntRS = oConn.execute(sql)
	dim endCnt : endCnt = endCntRS("endCnt")
	endCntRS = null
%>
<table border="0" cellpadding="3" cellspacing="0" align="center" width="300">
	<tr>
    	<td align="center" style="font-size:18px; font-weight:bold;">
        	All Done!<br />
            <%=endCnt - startingCnt%> Products Added
        </td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
