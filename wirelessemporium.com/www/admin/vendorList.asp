<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Vendor List"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim sql : sql = ""
	dim userMsg : userMsg = ""
	dim vendorName : vendorName = prepStr(ds(request.Form("vendorName")))
	dim vendorCode : vendorCode = prepStr(ds(request.Form("vendorCode")))
	
	if vendorName <> "" then
		sql = "select name, code from we_vendors with (nolock) where name = '" & vendorName & "' or code = '" & vendorCode & "'"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
		
		if rs.EOF then
			sql = "insert into we_vendors(name,code) values('" & vendorName & "','" & vendorCode & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
			
			vendorName = ""
			vendorCode = ""
			
			userMsg = "Vendor added to the system"
		else
			userMsg = "Vendor already found in the system: " & rs("name") & " / " & rs("code")
		end if
	end if
	
	if vendorName = "" then vendorName = "Enter Name"
	if vendorCode = "" then vendorCode = "Enter Code"
	
	sql = "select * from we_vendors with (nolock) order by name"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
%>
<table border="0" cellpadding="3" cellspacing="0" style="width:400px;" align="center">
	<tr>
    	<td>
        	<h1>Wireless Emporium Vendor List</h1>
        </td>
    </tr>
    <tr>
    	<td>
        	<form action="/admin/vendorList.asp" method="post" name="newVendorForm" style="margin:0px;" onsubmit="return(chkForm(this))">
        	<div style="float:left; font-weight:bold; width:400px; border-bottom:1px solid #000; margin-bottom:5px;">Enter New Vendor</div>
            <div style="float:left; padding:0px 5px 0px 15px;"><input type="text" name="vendorName" value="<%=vendorName%>" onfocus="chkVal(this,this.value,0)" onblur="chkVal(this,this.value,1)" /></div>
            <div style="float:left; padding-right:5px;"><input type="text" name="vendorCode" value="<%=vendorCode%>" size="8" onfocus="chkVal(this,this.value,0)" onblur="chkVal(this,this.value,2)" /></div>
            <div style="float:left"><input type="submit" name="mySub" value="Submit Vendor" /></div>
            </form>
        </td>
    </tr>
    <tr>
    	<td align="center">
        	<div style="float:left; font-weight:bold; width:400px; border-bottom:1px solid #000; border-top:1px solid #000; margin:10px 0px 5px 0px; text-align:left;">Existing Vendors</div>
        	<div style="height:400px; overflow:auto; float:left; padding-left:25px; width:350px;">
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" style="font-size:12px;">
			    <tr bgcolor="#333333" style="font-weight:bold; color:#FFF;">
			    	<td align="left">Vendor Name</td>
			        <td align="left">Vendor Code</td>
			    </tr>
                <%
				bgColor = "#fff"
				do while not rs.EOF
				%>
                <tr style="background-color:<%=bgColor%>">
			    	<td align="left"><%=rs("name")%></td>
			        <td align="left"><%=rs("code")%></td>
			    </tr>
                <%
					rs.movenext
					if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
				loop
				%>
            </table>
            </div>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function chkVal(obj,curVal,action) {
		if (action == 0) {
			if (curVal == "Enter Name" || curVal == "Enter Code") {
				obj.value = ""
			}
		}
		else if (action == 1) {
			if (curVal == "") {
				obj.value = "Enter Name"
			}
		}
		else if (action == 2) {
			if (curVal == "") {
				obj.value = "Enter Code"
			}
		}
	}
	function chkForm(form) {
		if (form.vendorName.value == "Enter Name" || form.vendorName.value == "") {
			alert("You must enter a valid vendor name")
			form.vendorName.focus()
			return false
		}
		else if (form.vendorCode.value == "Enter Code" || form.vendorCode.value == "") {
			alert("You must enter a valid vendor code")
			form.vendorCode.focus()
			return false
		}
		else {
			return true
		}
	}
	document.newVendorForm.vendorName.focus()
	
	<% if userMsg <> "" then %>alert("<%=userMsg%>")<% end if %>
</script>