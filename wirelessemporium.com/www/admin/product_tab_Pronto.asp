<%
pageTitle = "Create Pronto Product List TXT"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 9000 'seconds

dim fs, file, filename, path
'Map the file name to the physical path on the server.
filename = "productList_Pronto.txt"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List upload for Pronto</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							response.write "<b>CreateFile:</b><br>"
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='../tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		response.write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	Response.Write("Creating " & filename & ".<br>")
	Set file = fs.CreateTextFile(path)
	dim SQL, RS
	SQL = "SELECT A.itemID,A.PartNumber,A.brandID,A.itemDesc,A.itemPic,A.price_Retail,A.price_Our,A.inv_qty,A.Sports,A.flag1,A.itemLongDetail,A.UPCCode,"
	SQL = SQL & "B.brandName,C.modelName,C.[temp],D.typeName"
	SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
	SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
	SQL = SQL & " LEFT JOIN we_Types D ON A.typeID=D.typeID"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " AND A.typeid <> 16"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "Title" & vbtab & "SalePrice" & vbtab & "URL" & vbtab & "Description" & vbtab & "Category" & vbtab & "ImageURL" & vbtab & "Condition" & vbtab & "Brand" & vbtab & "Keywords" & vbtab & "ISBN" & vbtab & "ArtistAuthor" & vbtab & "ProductSKU" & vbtab & "Outlet" & vbtab & "InStock" & vbtab & "ShippingCost" & vbtab & "ShippingWeight" & vbtab & "ZipCode" & vbtab & "ProntoCategoryID" & vbtab & "Other" & vbtab & "ProductBid" & vbtab & "RetailPrice" & vbtab & "SpecialOffer"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
				set RS2 = CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				strItemDesc = replace(RS("itemDesc"),"+","&")
				strItemLongDetail = replace(RS("itemLongDetail"),vbcrlf," ")
				
				if isnull(RS("typeName")) then
					stypeName = "Universal Gear"
				else
					stypeName = RS("typeName")
				end if
				
				if stypeName = "Chargers" or stypeName = "Batteries" then
					ProntoCategoryID = "32"
				else
					ProntoCategoryID = "380"
				end if
				
				if isnull(RS("ModelName")) then
					ModelName = "Universal"
				else
					ModelName = RS("ModelName")
				end if
				if instr(ModelName," > ") then
					ModelName = replace(ModelName," > ","")
				end if
				
				if isnull(RS("BrandName")) then
					BrandName = "Universal"
				else 
					BrandName = RS("BrandName")
				end if
				
				strline = strItemDesc & vbtab
				strline = strline & RS("price_our") & vbtab
				strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?id=pronto" & vbtab
				strline = strline & strItemLongDetail & vbtab
				strline = strline & stypeName & vbtab
				strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab
				strline = strline & "new" & vbtab
				strline = strline & BrandName & vbtab
				strline = strline & stypeName & "," & BrandName & "," & ModelName & "," & strItemDesc & vbtab
				strline = strline & vbtab	'ISBN
				strline = strline & vbtab	'ArtistAuthor
				strline = strline & RS("UPCCode") & vbtab	'ProductSKU
				strline = strline & vbtab	'Outlet
				strline = strline & vbtab	'InStock
				strline = strline & 0 & vbtab
				strline = strline & vbtab
				strline = strline & vbtab
				strline = strline & ProntoCategoryID & vbtab
				strline = strline & vbtab
				strline = strline & vbtab
				strline = strline & RS("price_retail") & vbtab
				strline = strline & vbtab
				file.WriteLine strline
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub	
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
