<%
pageTitle = "Display Wireless Emporium Orders"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<h3>Display Wireless Emporium Orders</h3>
						<%
						if request("submitted") = "Process Orders" then
							if request("orderStart") <> "" and isNumeric(request("orderStart")) then
								strOrderStartNumber = Int(request("orderStart"))
								if request("orderEnd") <> "" and isNumeric(request("orderEnd")) then
									strOrderEndNumber = Int(request("orderEnd"))
									if strOrderStartNumber > strOrderEndNumber then
										strError = "Start Order # must be lower than End Order #"
									else
										andSQL = " AND A.OrderID <= " & request("orderEnd")
									end if
								end if
							else
								strError = "You must enter a valid Start Order #"
							end if
							
							if strError = "" then
								dim SQL, RS, strOrderStartNumber		
								SQL = "SELECT A.OrderID,A.shippingid,A.shiptype,B.accountID,B.fname,B.lname,B.email,B.sCity,B.sAddress1,B.sAddress2,B.sState,B.sZip,B.sCountry,C.quantity,D.itemWeight"
								SQL = SQL & " FROM ((we_orders A INNER JOIN we_accounts B ON A.accountid=B.accountid)"
								SQL = SQL & " INNER JOIN we_orderdetails C ON C.OrderID=A.OrderID)"
								SQL = SQL & " INNER JOIN we_Items D ON D.itemid=C.itemid"
								SQL = SQL & " WHERE A.approved = 1"
								SQL = SQL & " AND A.store = 0"
								SQL = SQL & " AND A.OrderID >= " & strOrderStartNumber
								SQL = SQL & andSQL
								if request("mailOption") = "Priority" then
									SQL = SQL & " AND A.shiptype = 'USPS Priority Mail (2-3 days)'"
								else
									SQL = SQL & " AND A.shiptype != 'USPS Priority Mail (2-3 days)'"
								end if
								if request("mailOption") = "Express" then
									SQL = SQL & " AND A.shiptype LIKE 'USPS Express%'"
								else
									SQL = SQL & " AND A.shiptype NOT LIKE 'USPS Express%'"
								end if
								SQL = SQL & " ORDER BY A.OrderID"
								Set RS = Server.CreateObject("ADODB.Recordset")
								RS.open SQL, oConn, 3, 3
								'response.write "<p>" & SQL & "</p>"
								'response.end
								
								response.write "<table border=""1"" class=""smlText""><tr>" & vbcrlf
								response.write "<td><b>ContactName</b></td>" & vbcrlf
								response.write "<td><b>EXT_REF_CD</b></td>" & vbcrlf
								response.write "<td><b>Email</b></td>" & vbcrlf
								response.write "<td><b>SADDR_CITY</b></td>" & vbcrlf
								response.write "<td><b>SADDR_LINE1</b></td>" & vbcrlf
								response.write "<td><b>SADDR_LINE2</b></td>" & vbcrlf
								response.write "<td><b>SADDR_STATE</b></td>" & vbcrlf
								response.write "<td><b>SADDR_ZIP</b></td>" & vbcrlf
								response.write "<td><b>SHIP_METHOD_REF</b></td>" & vbcrlf
								response.write "<td><b>ItemWeight</b></td>" & vbcrlf
								response.write "<td><b>TotalItemWeight</b></td>" & vbcrlf
								response.write "</tr>" & vbcrlf
								
								if not RS.eof then
									do until RS.eof
										OrderID = RS("OrderID")
										if request("mailOption") <> "Express" then
											if RS("shippingid") > 0 then
												altShipSQL = "SELECT * FROM we_addl_shipping_addr WHERE id='" & RS("shippingid") & "'"
												Set altShipRS = Server.CreateObject("ADODB.Recordset")
												altShipRS.open altShipSQL, oConn, 3, 3
												sAddress1 = altShipRS("sAddress1")
												sAddress2 = altShipRS("sAddress2")
												sCity = altShipRS("sCity")
												sState = altShipRS("sState")
												sZip = altShipRS("sZip")
												sCountry = altShipRS("sCountry")
											else
												sAddress1 = RS("sAddress1")
												sAddress2 = RS("sAddress2")
												sCity = RS("sCity")
												sState = RS("sState")
												sZip = RS("sZip")
												sCountry = RS("sCountry")
											end if
											quantity = cDbl(RS("quantity"))
											itemWeight = cDbl(RS("itemWeight"))
											shiptype = RS("shiptype")
											strToWrite = "<td>" & RS("fname") & "&nbsp;" & RS("lname") & "</td>" & vbcrlf
											strToWrite = strToWrite & "<td>" & blankCell(OrderID) & "</td>" & vbcrlf
											email = blankCell(RS("email"))
											if email <> "&nbsp;" then email = "<a href=""mailto:" & email & """>" & email & "</a>"
											strToWrite = strToWrite & "<td>" & email & "</td>" & vbcrlf
											strToWrite = strToWrite & "<td>" & blankCell(sCity) & "</td>" & vbcrlf
											strToWrite = strToWrite & "<td>" & blankCell(sAddress1) & "</td>" & vbcrlf
											strToWrite = strToWrite & "<td>" & blankCell(sAddress2) & "</td>" & vbcrlf
											strToWrite = strToWrite & "<td>" & blankCell(sState) & "</td>" & vbcrlf
											strToWrite = strToWrite & "<td>" & blankCell(sZip) & "</td>" & vbcrlf
											strToWrite = strToWrite & "<td>" & blankCell(shiptype) & "</td>" & vbcrlf
										end if
										lastOrder = OrderID
										holdOrderID = OrderID
										RS.movenext
										if not RS.eof then
											if RS("OrderID") <> holdOrderID then
												if request("mailOption") <> "Express" then
													orderWeight = orderWeight + (quantity * itemWeight)
													orderQuantity = orderQuantity + quantity
													if orderWeight < 2 then orderWeight = 2
													if orderQuantity > 1 then
														multiOrder = "MULTI-ITEMS"
													else
														multiOrder = ""
													end if
													strToWrite = strToWrite & "<td>" & blankCell(orderWeight) & "</td>" & vbcrlf 'Weight
													strToWrite = strToWrite & "<td>" & blankCell(multiOrder) & "</td>" & vbcrlf
													response.write "<tr>" & strToWrite & "</tr>" & vbcrlf
												end if
												orderWeight = 0
												orderQuantity = 0
											else
												orderWeight = orderWeight + (quantity * itemWeight)
												orderQuantity = orderQuantity + quantity
											end if
										end if
									loop
									
									'if request("mailOption") <> "Express" then
										orderWeight = orderWeight + (quantity * itemWeight)
										orderQuantity = orderQuantity + quantity
										if orderWeight < 2 then orderWeight = 2
										if orderQuantity > 1 then
											multiOrder = "MULTI-ITEMS"
										else
											multiOrder = ""
										end if
										strToWrite = strToWrite & "<td>" & blankCell(orderWeight) & "</td>" & vbcrlf 'Weight
										strToWrite = strToWrite & "<td>" & blankCell(multiOrder) & "</td>" & vbcrlf
										response.write "<tr>" & strToWrite & "</tr>" & vbcrlf
									'end if
								else
									response.write "<tr><td><h3>No Records Found</h3></td></tr>"
								end if
								response.write "</table>" & vbcrlf
							end if
						end if
						%>
						<p>
						<form action="report_OrdersShipping.asp" name="frmProcess" method="post">
							<%if strError <> "" then response.write "<p><font color=""#FF0000""><b>" & strError & "</b></font></p>"%>
							<p>Start Order #:&nbsp;&nbsp;<input type="text" name="orderStart" value="<%=strOrderStartNumber%>"></p>
							<p>End Order #:&nbsp;&nbsp;<input type="text" name="orderEnd" value="<%if strOrderEndNumber > 0 then response.write strOrderEndNumber%>"></p>
							<p>
								<input type="hidden" name="strOrderStartNumber" value="<%=strOrderStartNumber%>">
								<input type="submit" name="submitted" value="Display Orders">
							</p>
						</form>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->

<%
Function blankCell(str)
	if isNull(str) or str = "" then
		blankCell = "&nbsp;"
	else
		blankCell = str
	end if
End Function
%>
