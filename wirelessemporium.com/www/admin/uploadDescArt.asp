<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Upload Product Description Art"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim Upload, Path, myCount
	Set Upload = Server.CreateObject("Persits.Upload")
	Upload.IgnoreNoPost = True
	Path = server.MapPath("/images/productTabs/")
	myCount = Upload.Save
	
	frmSubmit = Upload.form("frmSubmit")
	
	if frmSubmit = "Add Image" then
		for each file in Upload.files
			myFileName = File.ExtractFileName
			SavePath = Path & "/" & myFileName
			File.SaveAs SavePath
			
			dim filesys, demofolder, fil, filecoll, filist
			set filesys = CreateObject("Scripting.FileSystemObject")
			rackSpaceFileLoc = server.MapPath("/images/productTabs/" & myFileName)
			if filesys.FileExists(server.MapPath("/images/productTabs/" & myFileName)) then
				Set myFile = filesys.GetFile(server.MapPath("/images/productTabs/" & myFileName))
				userMsg = "File has been saved and confirmed"
			else
				userMsg = "Error confirming upload"
			end if
		next
	end if
%>
<form enctype="multipart/form-data" action="/admin/uploadDescArt.asp" method="POST">
<table border="0" cellpadding="3" cellspacing="0" align="center" width="550" style="padding-top:20px;">
	<tr><td colspan="3" align="left"><h1>Upload Product Description Art</h1></td></tr>
    <tr>
    	<td colspan="3" style="padding-bottom:20px;">
        	The app is for uploading images to the "/images/productTabs" folder. When the file is 
            uploaded the system will confirm that the file is on the server and ready for use.
        </td>
    </tr>
    <% if userMsg <> "" then %>
    <tr><td colspan="3" align="center" style="color:#F00; font-weight:bold; padding-bottom:20px;"><%=userMsg%></td></tr>
    	<% if userMsg = "File has been saved and confirmed" then %>
    <tr><td colspan="3" align="center"><img src="/images/productTabs/<%=myFileName%>" border="0" /></td></tr>
	    <% end if %>
    <% end if %>
    <tr>
    	<td><div style="width:110px;"></div></td>
    	<td>
        	<div style="float:left; width:330px;">
	            <div style="float:left; font-weight:bold; font-size:14px; width:100px;">Save New File:</div>
    	    	<div style="float:left; width:230px;"><input type="FILE" name="FILE1"></div>
                <div style="float:left; text-align:right; width:330px; padding-top:10px;"><input type="submit" name="frmSubmit" value="Add Image"></div>
            </div>
        </td>
        <td><div style="width:110px;"></div></td>
    </tr>
</table>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
