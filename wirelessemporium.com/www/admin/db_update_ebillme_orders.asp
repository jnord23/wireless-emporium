<%
pageTitle = "Admin - Update WE Database - Update eBillme Orders"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<blockquote>

<h3>Update WE Database - Update eBillme Orders</h3>

<%
function showNothing(strValue)
	if isNull(strValue) then
		showNothing = "-null-"
	elseif strValue = "" then
		showNothing = "&nbsp;"
	else
		showNothing = strValue
	end if
end function

a = 0
strError = ""

if request.form("submitted2") = "Update" then
	for a = 1 to request.form("totalCount")
		SQL = "UPDATE we_Orders SET"
		SQL = SQL & " approved=" & request.form("approved" & a) & ","
		SQL = SQL & " cancelled=" & request.form("cancelled" & a)
		SQL = SQL & " WHERE OrderID='" & request.form("OrderID" & a) & "'"
		'response.write "<p>" & SQL & "</p>"
		oConn.execute SQL
	next
	%>
	<h3>UPDATED!</h3>
	<%
end if

if request.form("submitted") <> "" then
	dim StartDate, EndDate, strError
	StartDate = request.form("dc1")
	EndDate = request.form("dc2")
	if not isDate(StartDate) then
		strError = "Start Date must be a valid date."
	else
		StartDate = dateValue(StartDate)
	end if
	if not isDate(EndDate) then
		strError = "End Date must be a valid date."
	else
		EndDate = dateValue(EndDate)
	end if
	if strError = "" then
		if StartDate > EndDate then
			strError = "Start Date must be earlier than or equal to End Date."
		end if
	end if
	if strError = "" then
		strDates = StartDate & "<br>to<br>" & EndDate
		SQL = "SELECT A.*,B.* FROM we_Orders A INNER JOIN we_Accounts B ON A.AccountID=B.AccountID"
		SQL = SQL & " WHERE A.orderdatetime >= '" & StartDate & "' AND A.orderdatetime < '" & dateAdd("D",1,EndDate) & "'"
		SQL = SQL & " AND (A.approved = 0 OR A.approved IS NULL)"
		SQL = SQL & " AND (A.cancelled = 0 OR A.cancelled IS NULL)"
		SQL = SQL & " AND A.store = 0"
		SQL = SQL & " AND A.extOrderType = 3"
		SQL = SQL & " AND (B.email = 'phoneorder@wirelessemporium.com' OR B.email NOT LIKE '%@wirelessemporium.com')"
		SQL = SQL & " ORDER BY A.orderdatetime"
		'response.write "<h3>" & SQL & "</h3>"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		a = 0
		if RS.eof then
			response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
			response.write "<a href=""db_update_ebillme_orders.asp"">Try again.</a>"
		else
			%>
			<p><a href="db_update_ebillme_orders.asp">Search For Another Date Range</a></p>
			<table border="1" cellpadding="0" cellspacing="0" width="90%" align="center">
				<form name="frmUpdateOrders" action="db_update_ebillme_orders.asp" method="post">
					<tr>
						<td valign="top" align="center"><b>OrderID</b></td>
						<td valign="top" align="center"><b>Order&nbsp;Date</b></td>
						<td valign="top" align="center"><b>Name</b></td>
						<td valign="top" align="center"><b>ReferenceID&nbsp;|&nbsp;AccountNum</b></td>
						<td valign="top" align="center"><b>Approved</b></td>
						<td valign="top" align="center"><b>Cancelled</b></td>
					</tr>
					<%
					do until RS.eof
						OrderID = RS("OrderID")
						approved = RS("approved")
						cancelled = RS("cancelled")
						a = a + 1
						%>
						<tr>
							<td valign="top" align="center">
								<input type="hidden" name="OrderID<%=a%>" value="<%=OrderID%>"><%=showNothing(OrderID)%>
							</td>
							<td valign="top" align="center"><%=dateValue(RS("OrderDateTime"))%></td>
							<td valign="top" align="center"><%=showNothing(RS("fName")) & "&nbsp;" & showNothing(RS("lName"))%></td>
							<td valign="top" align="center"><%=showNothing(RS("extOrderNumber"))%></td>
							<td valign="top" align="center">
								<input type="radio" name="approved<%=a%>" value="-1"<%if approved = true then response.write " checked"%>>Yes&nbsp;
								<input type="radio" name="approved<%=a%>" value="null"<%if approved = false or isNull(approved) then response.write " checked"%>>No
							</td>
							<td valign="top" align="center">
								<input type="radio" name="cancelled<%=a%>" value="-1"<%if cancelled = "yes" then response.write " checked"%>>Yes&nbsp;
								<input type="radio" name="cancelled<%=a%>" value="null"<%if cancelled <> "yes" or isNull(cancelled) then response.write " checked"%>>No
							</td>
						</tr>
						<%
						RS.movenext
					loop
					%>
					<tr>
						<td align="center" colspan="6">
							<input type="hidden" name="totalCount" value="<%=a%>">
							<input type="hidden" name="orderStart" value="<%=strOrderStartNumber%>">
							<input type="hidden" name="orderEnd" value="<%=strOrderEndNumber%>">
							<input type="submit" name="submitted2" value="Update">
						</td>
					</tr>
				</form>
			</table>
			<p>&nbsp;</p>
			<%
		end if
	end if
else
	%>
	<form action="db_update_ebillme_orders.asp" name="frmSalesReport" method="post">
		<p class="normalText">
			<input type="text" name="dc1" value="<%=dateAdd("d",-1,date)%>">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.frmSalesReport.dc1,document.frmSalesReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;Start Date
			<br>
			<input type="text" name="dc2" value="<%=dateAdd("d",-1,date)%>">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fEndPop(document.frmSalesReport.dc1,document.frmSalesReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;End Date
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
		</p>
		<p><input type="submit" name="submitted" value="Find Orders"></p>
	</form>
	<%
end if
%>

</blockquote>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
