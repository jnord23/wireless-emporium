<%
	thisUser = Request.Cookies("username")
	pageTitle = ""
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	sql = 	"exec sp_oosData 'HR','3'"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	dim firstDate : firstDate = date-60
	
	do while not rs.EOF
		curPartNumber = rs("partNumber")
		outOn = ""
		inOn = ""
		if not isnull(rs("notes")) then
			'find range(s)
			do while rs("partNumber") = curPartNumber
				activeNote = rs("notes")
				if instr(activeNote,"Out of Stock") > 0 then
					outOn = dateOnly(rs("entryDate"))
				elseif instr(activeNote,"Receive Vendor") > 0 then
					inOn = dateOnly(rs("entryDate"))
				elseif instr(activeNote,"Employee Adj") > 0 and rs("curInvQty") = 0 then
					inOn = dateOnly(rs("entryDate"))
				elseif instr(activeNote,"Employee Adj") > 0 and rs("adjustQty") = 0 then
					outOn = dateOnly(rs("entryDate"))
				elseif instr(activeNote,"Restock from") > 0 then
					inOn = dateOnly(rs("entryDate"))
				elseif instr(activeNote,"Adjust Vendor") > 0 and rs("adjustQty") = 0 then
					outOn = dateOnly(rs("entryDate"))
				elseif instr(activeNote,"Adjust Vendor") > 0 then
					inOn = dateOnly(rs("entryDate"))
				else
					response.Write("error:" & rs("notes"))
					response.End()
				end if
				
				if inOn <> "" and outOn = "" then
					sql = "insert into we_outOfStockDates (partNumber,oosDate,insDate) values('" & curPartNumber & "','" & firstDate & "','" & inOn & "')"
					oConn.execute(sql)
					inOn = ""
				elseif inOn <> "" and outOn <> "" then
					sql = "insert into we_outOfStockDates (partNumber,oosDate,insDate) values('" & curPartNumber & "','" & outOn & "','" & inOn & "')"
					oConn.execute(sql)
					inOn = ""
					outOn = ""
				end if
				
				rs.movenext
				if rs.EOF then exit do
			loop
			
			if outOn <> "" and inOn = "" then
				sql = "insert into we_outOfStockDates (partNumber,oosDate,insDate) values('" & curPartNumber & "','" & outOn & "',null)"
				oConn.execute(sql)
				outOn = ""
			end if
		elseif rs("inv_qty") = 0 then
			'out all 60 days
			sql = "insert into we_outOfStockDates (partNumber,oosDate,insDate) values('" & curPartNumber & "','" & firstDate & "',null)"
			oConn.execute(sql)
			rs.movenext
		else
			'never went out of stock
			rs.movenext
		end if
	loop
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->