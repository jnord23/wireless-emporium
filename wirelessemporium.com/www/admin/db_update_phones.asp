<%
response.buffer = false
dim thisUser, adminID
pageTitle = "Admin - Update WE Database - Update Phones"
header = 1
thisUser = Request.Cookies("username")
adminID = Request.Cookies("adminID")
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
if request.form("submitted") = "Update" then
	for a = 1 to request.form("totalCount")
		if isNumeric(request.form("price_Retail" & a)) then
			price_Retail = "'" & request.form("price_Retail" & a) & "'"
		else
			price_Retail = "null"
		end if
		if isNumeric(request.form("price_Our" & a)) then
			price_Our = "'" & request.form("price_Our" & a) & "'"
		else
			price_Our = "null"
		end if
		if isNumeric(request.form("price_CO" & a)) then
			price_CO = "'" & request.form("price_CO" & a) & "'"
		else
			price_CO = "null"
		end if
		if isNumeric(request.form("price_PS" & a)) then
			price_PS = "'" & request.form("price_PS" & a) & "'"
		else
			price_PS = "null"
		end if
		if isNumeric(request.form("COGS" & a)) then
			COGS = "'" & request.form("COGS" & a) & "'"
		else
			COGS = "null"
		end if
		SQL = "UPDATE WE_Items SET"
		SQL = SQL & " price_Retail = " & price_Retail & ", "
		SQL = SQL & " price_Our = " & price_Our & ", "
		SQL = SQL & " price_CO = " & price_CO & ", "
		SQL = SQL & " price_PS = " & price_PS & ", "
		SQL = SQL & " COGS = " & COGS & ", "
		SQL = SQL & " Vendor = '" & request.form("Vendor" & a) & "', "
		SQL = SQL & " PriceLastUpdatedBy = '" & adminID & "', "
		SQL = SQL & " PriceLastUpdatedDate = '" & now & "'"
		SQL = SQL & " WHERE itemID='" & request.form("itemID" & a) & "'"
		'response.write "<p>" & SQL & "</p>"
		oConn.execute SQL
	next
	%>
	<h3>UPDATED!</h3>
	<%
else
	SQL = "SELECT itemID, itemDesc, price_Retail, price_Our, price_CO, price_PS, COGS, Vendor, inv_qty FROM WE_Items WHERE TypeID=16 ORDER BY itemDesc"
	'response.write "<p>" & SQL & "</p>"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	a = 0
	if RS.eof then
	    response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
	else
		%>
		<font face="Arial,Helvetica"><h3><%=RS.recordcount%> items found</h3></font>
		<form name="frmEditItem" action="db_update_phones.asp" method="post">
			<table border="1" cellpadding="3" cellspacing="0" width="1000">
				<tr>
					<td align="center" width="10"><b>Item#</b></td>
					<td align="center" width="600"><b>Description</b></td>
					<td align="center" width="35"><b>Inv.</b></td>
					<td align="center" width="65"><b>Retail&nbsp;$</b></td>
					<td align="center" width="65"><b>Our&nbsp;$</b></td>
					<td align="center" width="65"><b>CO&nbsp;$</b></td>
					<td align="center" width="65"><b>PS&nbsp;$</b></td>
					<td align="center" width="65"><b>COGS</b></td>
					<td align="center" width="30"><b>Vendor</b></td>
				</tr>
				<%
				do until RS.eof
					a = a + 1
					%>
					<tr>
						<td align="center">
							<%=RS("itemID")%>
							<input type="hidden" name="itemID<%=a%>" value="<%=RS("itemID")%>">
						</td>
						<td align="center"><%=RS("itemDesc")%></td>
						<td align="center"><%=RS("inv_qty")%></td>
						<td align="center"><input type="text" name="price_Retail<%=a%>" size="4" value="<%=RS("price_Retail")%>"></td>
						<td align="center"><input type="text" name="price_Our<%=a%>" size="4" value="<%=RS("price_Our")%>"></td>
						<td align="center"><input type="text" name="price_CO<%=a%>" size="4" value="<%=RS("price_CO")%>"></td>
						<td align="center"><input type="text" name="price_PS<%=a%>" size="4" value="<%=RS("price_PS")%>"></td>
						<td align="center"><input type="text" name="COGS<%=a%>" size="4" value="<%=RS("COGS")%>"></td>
						<td align="center"><input type="text" name="Vendor<%=a%>" size="4" value="<%=RS("Vendor")%>"></td>
					</tr>
					<%
					RS.movenext
				loop
				%>
			</table>
			<%if thisUser = "YRL_tony" or thisUser = "johnw" or thisUser = "michaelc" or thisUser = "jnord" or thisUser = "ericac" or thisUser = "armandol" or thisUser = "mariog" then%>
				<p>
					<input type="submit" name="submitted" value="Update">
					<input type="hidden" name="totalCount" value="<%=a%>">
				</p>
			<%end if%>
		</form>
		<p>&nbsp;</p>
		<%
	end if
	RS.close
	set RS = nothing
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
