<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
	filename	=	"PPC_" & Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now) & ".xls"
	
	pathToSave = server.mappath("tempCSV\PPC")
	set Upload = Server.CreateObject("Persits.Upload")
	Upload.IgnoreNoPost = True
	uploadCount = Upload.Save(pathToSave)
	
'	targetERS = prepInt(Upload.Form("targetERS"))
	targetERS = cdbl(0.35)
	
	set oFile = Upload.Files(1)
	myFile = oFile.path
	
	set txtConn = Server.CreateObject("ADODB.Connection")
	
	'conn string for .xlsx
	myStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & myFile & ";Extended Properties=""Excel 12.0;HDR=YES;IMEX=1"";"			
	txtConn.ConnectionString = myStr
	txtConn.ConnectionTimeout = 1000
	txtConn.Open
	
	sql = "	select [Search term] as sTerm, [Ad group] as ad, [Campaign] as c, [Clicks] as cl, [Avg CPC] as aCPC, [Conv] as conv, [Value over Conv] as vConv from [Sheet1$] where [Conv] > 0 order by [Conv] desc, [Search term]" 
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open SQL, txtConn, 0, 1
	
	filename	=	"PPC_" & Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now) & ".xls"
	
	Response.ContentType = "application/vnd.ms-excel"
	'Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
	Response.AddHeader "Content-Disposition","attachment;filename="""&filename&""""
	'Response.Charset = "" 
	'Me.EnableViewState = False
	
	nCnt = 0
	response.write "Campaign" & vbTab & "Ad group" & vbTab & "Keyword" & vbTab & "Criterion Type" & vbTab & "Current Avg CPC" & vbTab & "Current ERS" & vbTab & "Target ERS" & vbTab & "New Max CPC" & vbTab & "ReCalculation" & vbNewLine
	if not rs.eof then
		do until rs.eof
			nCnt = nCnt + 1
			for i=0 to 2
				bRecal = "N"
				curERS = getERS(rs)
				newCPC = rs("aCPC")
				if cdbl(curERS) > cdbl(targetERS) then 
					bRecal = "YES"
					newCPC = newERS(rs)
				end if
				
				if i=0 then
					Creiterion	=	"Broad"
				elseif i=1 then
					Creiterion	=	"Phrase"
					newCPC = cdbl(newCPC * 1.1)
				elseif i=2 then
					Creiterion	=	"Exact"
					newCPC = cdbl(newCPC * 1.2)
				end if
				
				curERS		=	formatnumber(curERS, 2)
				targetERS 	=	formatnumber(targetERS, 2)
				newCPC 		=	formatnumber(newCPC, 2)

				response.write rs("c") & vbTab & rs("ad") & vbTab & rs("sTerm") & vbTab & Creiterion & vbTab & rs("aCPC") & vbTab & curERS & vbTab & targetERS & vbTab & newCPC & vbTab & bRecal & vbNewLine
				
				if (nCnt mod 100) = 0 then response.flush
			next
			rs.movenext
		loop
	end if
	
	function getERS(byref oRs)
		ret = 0
		
		if isnumeric(oRs("aCPC")) and isnumeric(oRs("cl")) and isnumeric(oRs("conv")) and isnumeric(oRs("vConv")) then
			avgCPC = cdbl(oRs("aCPC"))
			clicks = cdbl(oRs("cl"))
			conv = cdbl(oRs("conv"))
			vConv = cdbl(oRs("vConv"))
			
			if (conv*vConv) = 0 then
				ret = oRs("aCPC")
			else
				ret = (avgCPC*clicks)/(conv*vConv)
			end if
		else
			ret = oRs("aCPC")
		end if
		
		getERS = cdbl(ret)
	end function
	
	function newERS(byref oRs)
		ret = 0
		
		if isnumeric(targetERS) and isnumeric(oRs("cl")) and isnumeric(oRs("conv")) and isnumeric(oRs("vConv")) then
			clicks = cdbl(oRs("cl"))
			conv = cdbl(oRs("conv"))
			vConv = cdbl(oRs("vConv"))
			
			if clicks = 0 then
				ret = oRs("aCPC")
			else
				ret = (targetERS*(conv*vConv)) / clicks
			end if
		else
			ret = oRs("aCPC")
		end if
		
		newERS = cdbl(ret)
	end function
	
%>