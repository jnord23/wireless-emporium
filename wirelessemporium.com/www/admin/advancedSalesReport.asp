<%
	thisUser = Request.Cookies("username")
	pageTitle = ""
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim siteID : siteID = prepStr(request.Form("siteID"))
	dim startDate : startDate = prepStr(request.Form("startDate"))
	
	if startDate = "" then startDate = date-8 else startDate = cdate(startDate)
	endDate = startDate+8
	
	sql = 	"select c.typeID, d.typeName, CONVERT(date, a.orderdatetime, 101) as saleDate, sum(b.quantity) as sales " &_
			"from we_orders a " &_
				"left join we_orderdetails b on a.orderid = b.orderID " &_
				"left join we_Items c on b.itemid = c.itemID " &_
				"left join we_Types d on c.typeID = d.typeID " &_
			"where d.typeName is not null and a.parentOrderID is null and a.orderdatetime > '" & startDate & "' and a.orderdatetime < '" & startDate+9 & "' and a.approved = 1 and a.cancelled is null and (a.extOrderType is null or a.extOrderType < 4) " &_
			"group by c.typeID, d.typeName, CONVERT(date, a.orderdatetime, 101) " &_
			"order by d.typeName, 3"
	if siteID <> "" then sql = replace(sql,"where","where a.store = " & siteID & " and")
	session("errorSQL") = sql
	set catSalesRS = oConn.execute(sql)
	
	sql = "select isnull(c.brandID,0) as brandID, d.brandName, CONVERT(date, a.orderdatetime, 101) as saleDate, sum(b.quantity) as sales from we_orders a left join we_orderdetails b on a.orderid = b.orderID left join we_Items c on b.itemid = c.itemID left join we_brands d on c.brandID = d.brandID where a.parentOrderID is null and a.orderdatetime > '" & startDate & "' and a.orderdatetime < '" & startDate+9 & "' and a.approved = 1 and a.cancelled is null and (a.extOrderType is null or a.extOrderType < 4) group by isnull(c.brandID,0), d.brandName, CONVERT(date, a.orderdatetime, 101) order by d.brandName, 3"
	if siteID <> "" then sql = replace(sql,"where","where a.store = " & siteID & " and")
	session("errorSQL") = sql
	set brandSalesRS = oConn.execute(sql)
	
	sql = "select isnull(b.typeID,0) as methodID, isnull(b.orderdesc,'CreditCard') as methodName, CONVERT(date, a.orderdatetime, 101) as saleDate, count(*) as sales from we_orders a left join XOrderType b on a.extOrderType = b.typeid where a.parentOrderID is null and a.orderdatetime > '" & startDate & "' and a.orderdatetime < '" & startDate+9 & "' and a.approved = 1 and a.cancelled is null group by isnull(b.typeID,0), isnull(b.orderdesc,'CreditCard'), CONVERT(date, a.orderdatetime, 101) order by methodName, 3"
	if siteID <> "" then sql = replace(sql,"where","where a.store = " & siteID & " and")
	session("errorSQL") = sql
	set methodSalesRS = oConn.execute(sql)
	
	session("errorSQL") = "data pulled"
%>
<div style="position:relative;">
<table border="0" cellpadding="3" cellspacing="0" width="600" align="center" style="padding-top:20px;">
    <tr>
    	<td colspan="3" align="center" style="border-bottom:1px solid #000; padding-bottom:10px;">
        	<form name="optionForm" method="post" style="padding:0px; margin:0px;">
            <div style="height:30px; width:300px; margin-left:auto; margin-right:auto;">
            	<div style="float:left; width:70px; margin-right:5px; text-align:left;">Website:</div>
                <div style="float:left; width:190px; text-align:left;">
                    <select name="siteID" onchange="document.optionForm.submit()">
                        <option value="">All Sites</option>
                        <option value="0"<% if siteID = "0" then %> selected="selected"<% end if %>>WE</option>
                        <option value="2"<% if siteID = "2" then %> selected="selected"<% end if %>>CO</option>
                        <option value="1"<% if siteID = "1" then %> selected="selected"<% end if %>>CA</option>
                        <option value="3"<% if siteID = "3" then %> selected="selected"<% end if %>>PS</option>
                    </select>
                </div>
            </div>
            <div style="height:30px; width:300px; margin-left:auto; margin-right:auto;">
            	<div style="float:left; width:70px; margin-right:5px; text-align:left; padding-top:2px;">Start Date:</div>
                <div style="float:left; width:190px; text-align:left;"><input type="input" name="startDate" value="<%=startDate%>" size="8" onchange="document.optionForm.submit()" /></div>
            </div>
            </form>
        </td>
    </tr>
    <% session("errorSQL") = "optionForm Ready" %>
    <tr>
    	<td valign="top" style="padding-top:20px;">
        	<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0">
            	<tr style="width:100%; background-color:#000; font-weight:bold; color:#FFF;">
                	<td align="left" valign="bottom">Category Sales</td>
                    <td align="center" title="<%=startDate%>"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center" title="<%=startDate+1%>"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+1))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center" title="<%=startDate+2%>"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+2))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center" title="<%=startDate+3%>"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+3))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center" title="<%=startDate+4%>"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+4))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center" title="<%=startDate+5%>"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+5))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center" title="<%=startDate+6%>"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+6))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center" title="<%=startDate+7%>"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+7))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center" title="<%=startDate+8%>"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+8))%>.gif" border="0" width="20" height="89" /></td>
                </tr>
                <%
				bgColor = "#fff"
				catList = ""
				catListLap = 0
				do while not catSalesRS.EOF
					catListLap = catListLap + 1
					session("errorSQL") = "catListLap:" & catListLap
					catID = prepInt(catSalesRS("typeID"))
					catList = catList & "," & catID
					catName = prepStr(catSalesRS("typeName"))
					if catName = "" then showType = "Music Skins" else showType = catName
				%>
                <tr style="background-color:<%=bgColor%>;">
                	<td align="left" nowrap="nowrap" style="font-weight:bold;"><%=showType%></td>
					<%
					innerLap = 0
					do while catName = prepStr(catSalesRS("typeName"))
						innerLap = innerLap + 1
						properDate = endDate - (9-innerLap)
						catSaleDate = catSalesRS("saleDate")
						catSales = prepInt(catSalesRS("sales"))
						
						if cdate(catSaleDate) <> properDate then
							if cdate(properDate) = endDate then catSaleDate = "Today" else catSaleDate = weekdayname(weekday(properDate))
					%>
                    <td align="right" id="<%=catID%>_<%=innerLap%>" title="<%=catSaleDate%>">0</td>
                    <%
						else
							if cdate(catSaleDate) = endDate then catSaleDate = "Today" else catSaleDate = weekdayname(weekday(cdate(catSaleDate)))
					%>
                    <td align="right" id="<%=catID%>_<%=innerLap%>" title="<%=catSaleDate%>"><%=catSales%></td>
                    <%
							catSalesRS.movenext
							if catSalesRS.EOF then exit do
						end if
					loop
					
					do while innerLap < 9
						innerLap = innerLap + 1
						properDate = endDate - (9-innerLap)
						if innerLap = 9 then
							response.Write("<td align='right' id='" & catID & "_" & innerLap & "' title='Today'>0</td>")
						else
							response.Write("<td align='right' id='" & catID & "_" & innerLap & "' title='" & weekdayname(weekday(properDate)) & "'>0</td>")
						end if
					loop
					%>
                </tr>
                <%
					if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
					response.Flush()
				loop
				%>
                <tr style="width:100%; background-color:#000; font-weight:bold; color:#FFF;">
                	<td align="left">Totals</td>
                    <td id="total_1" align="right"></td>
                    <td id="total_2" align="right"></td>
                    <td id="total_3" align="right"></td>
                    <td id="total_4" align="right"></td>
                    <td id="total_5" align="right"></td>
                    <td id="total_6" align="right"></td>
                    <td id="total_7" align="right"></td>
                    <td id="total_8" align="right"></td>
                    <td id="total_9" align="right"></td>
            </table>
        </td>
        <% session("errorSQL") = "table1 Ready"%>
        <td valign="top" style="padding:20px 0px 0px 20px;">
        	<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0">
            	<tr style="width:100%; background-color:#000; font-weight:bold; color:#FFF;">
                	<td align="left" valign="bottom">Brand Sales</td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+1))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+2))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+3))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+4))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+5))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+6))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+7))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+8))%>.gif" border="0" width="20" height="89" /></td>
                </tr>
                <%
				bgColor = "#fff"
				brandList = ""
				do while not brandSalesRS.EOF
					brandID = prepInt(brandSalesRS("brandID"))
					brandList = brandList & "," & brandID
					brandName = prepStr(brandSalesRS("brandName"))
					if brandName = "" then showBrand = "Unknown" else showBrand = brandName
				%>
                <tr style="background-color:<%=bgColor%>;">
                	<td align="left" nowrap="nowrap" style="font-weight:bold;"><%=showBrand%></td>
					<%
					innerLap = 0
					do while brandName = prepStr(brandSalesRS("brandName"))
						innerLap = innerLap + 1
						properDate = endDate - (9-innerLap)
						catSaleDate = brandSalesRS("saleDate")
						catSales = prepInt(brandSalesRS("sales"))
						
						if cdate(catSaleDate) <> properDate then
							if cdate(properDate) = endDate then catSaleDate = "Today" else catSaleDate = weekdayname(weekday(properDate))
					%>
                    <td align="right" id="b_<%=brandID%>_<%=innerLap%>" title="<%=catSaleDate%>">0</td>
                    <%
						else
							if cdate(catSaleDate) = endDate then catSaleDate = "Today" else catSaleDate = weekdayname(weekday(cdate(catSaleDate)))
					%>
                    <td align="right" id="b_<%=brandID%>_<%=innerLap%>" title="<%=catSaleDate%>"><%=catSales%></td>
                    <%
							brandSalesRS.movenext
							if brandSalesRS.EOF then exit do
						end if
					loop
					
					do while innerLap < 9
						innerLap = innerLap + 1
						properDate = endDate - (9-innerLap)
						if innerLap = 9 then
							response.Write("<td align='right' id='b_" & brandID & "_" & innerLap & "' title='Today'>0</td>")
						else
							response.Write("<td align='right' id='b_" & brandID & "_" & innerLap & "' title='" & weekdayname(weekday(properDate)) & "'>0</td>")
						end if
					loop
					%>
                </tr>
                <%
					if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
					response.Flush()
				loop
				%>
                <tr style="width:100%; background-color:#000; font-weight:bold; color:#FFF;">
                	<td align="left">Totals</td>
                    <td id="brandTotal_1" align="right"></td>
                    <td id="brandTotal_2" align="right"></td>
                    <td id="brandTotal_3" align="right"></td>
                    <td id="brandTotal_4" align="right"></td>
                    <td id="brandTotal_5" align="right"></td>
                    <td id="brandTotal_6" align="right"></td>
                    <td id="brandTotal_7" align="right"></td>
                    <td id="brandTotal_8" align="right"></td>
                    <td id="brandTotal_9" align="right"></td>
            </table>
        </td>
        <% session("errorSQL") = "table2 Ready"%>
        <td valign="top" style="padding:20px 0px 0px 20px;">
        	<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0">
            	<tr style="width:100%; background-color:#000; font-weight:bold; color:#FFF;">
                	<td align="left" valign="bottom">Sale Methods</td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+1))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+2))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+3))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+4))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+5))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+6))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+7))%>.gif" border="0" width="20" height="89" /></td>
                    <td align="center"><img src="/images/icons/daysOfWeek/<%=weekdayname(weekday(startDate+8))%>.gif" border="0" width="20" height="89" /></td>
                </tr>
                <%
				bgColor = "#fff"
				methodList = ""
				do while not methodSalesRS.EOF
					methodID = prepInt(methodSalesRS("methodID"))
					methodList = methodList & "," & methodID
					methodName = prepStr(methodSalesRS("methodName"))
					if methodName = "" then showBrand = "Unknown" else showBrand = methodName
				%>
                <tr style="background-color:<%=bgColor%>;">
                	<td align="left" nowrap="nowrap" style="font-weight:bold;"><%=showBrand%></td>
					<%
					innerLap = 0
					do while methodName = prepStr(methodSalesRS("methodName"))
						innerLap = innerLap + 1
						properDate = endDate - (9-innerLap)
						catSaleDate = methodSalesRS("saleDate")
						catSales = prepInt(methodSalesRS("sales"))
						
						if cdate(catSaleDate) <> properDate then
							if cdate(properDate) = endDate then catSaleDate = "Today" else catSaleDate = weekdayname(weekday(properDate))
					%>
                    <td align="right" id="m_<%=methodID%>_<%=innerLap%>" title="<%=catSaleDate%>">0</td>
                    <%
						else
							if cdate(catSaleDate) = endDate then catSaleDate = "Today" else catSaleDate = weekdayname(weekday(cdate(catSaleDate)))
					%>
                    <td align="right" id="m_<%=methodID%>_<%=innerLap%>" title="<%=catSaleDate%>"><%=catSales%></td>
                    <%
							methodSalesRS.movenext
							if methodSalesRS.EOF then exit do
						end if
					loop
					
					do while innerLap < 9
						innerLap = innerLap + 1
						properDate = endDate - (9-innerLap)
						if innerLap = 9 then
							response.Write("<td align='right' id='m_" & methodID & "_" & innerLap & "' title='Today'>0</td>")
						else
							response.Write("<td align='right' id='m_" & methodID & "_" & innerLap & "' title='" & weekdayname(weekday(properDate)) & "'>0</td>")
						end if
					loop
					%>
                </tr>
                <%
					if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
					response.Flush()
				loop
				%>
                <tr style="width:100%; background-color:#000; font-weight:bold; color:#FFF;">
                	<td align="left">Totals</td>
                    <td id="methodTotal_1" align="right"></td>
                    <td id="methodTotal_2" align="right"></td>
                    <td id="methodTotal_3" align="right"></td>
                    <td id="methodTotal_4" align="right"></td>
                    <td id="methodTotal_5" align="right"></td>
                    <td id="methodTotal_6" align="right"></td>
                    <td id="methodTotal_7" align="right"></td>
                    <td id="methodTotal_8" align="right"></td>
                    <td id="methodTotal_9" align="right"></td>
            </table>
        </td>
        <% session("errorSQL") = "table3 Ready"%>
    </tr>
</table>
</div>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<% catList = right(catList,len(catList)-1) %>
<% brandList = right(brandList,len(brandList)-1) %>
<% methodList = right(methodList,len(methodList)-1) %>
<script language="javascript">
	//################# First the categories ##################
	var catList = new Array(<%=catList%>)
	var curHigh, curHighCell, curLow, curLowCell
	var day1Total = 0
	var day2Total = 0
	var day3Total = 0
	var day4Total = 0
	var day5Total = 0
	var day6Total = 0
	var day7Total = 0
	var day8Total = 0
	var day9Total = 0
	for (var i=0; i < catList.length; i++) {
		curHigh = 0
		curHighCell = 0
		curLow = 100
		curLowCell = 0
		for (x=1; x < 10; x++) {
			curVal = parseInt(document.getElementById(catList[i] + "_" + x).innerHTML)
			if (x == 1) { day1Total = day1Total + curVal }
			if (x == 2) { day2Total = day2Total + curVal }
			if (x == 3) { day3Total = day3Total + curVal }
			if (x == 4) { day4Total = day4Total + curVal }
			if (x == 5) { day5Total = day5Total + curVal }
			if (x == 6) { day6Total = day6Total + curVal }
			if (x == 7) { day7Total = day7Total + curVal }
			if (x == 8) { day8Total = day8Total + curVal }
			if (x == 9) { day9Total = day9Total + curVal }
			if (curVal > curHigh) { curHigh = curVal; curHighCell = x }
			if (curVal < curLow) { curLow = curVal; curLowCell = x }
		}
		for (x=1; x < 10; x++) {
			curVal = parseInt(document.getElementById(catList[i] + "_" + x).innerHTML)
			if (curVal == curLow) { document.getElementById(catList[i] + "_" + x).style.backgroundColor = "#eab5c8" }
			if (curVal == curHigh) { document.getElementById(catList[i] + "_" + x).style.backgroundColor = "#bbecb1" }
		}
	}
	document.getElementById("total_1").innerHTML = day1Total
	document.getElementById("total_2").innerHTML = day2Total
	document.getElementById("total_3").innerHTML = day3Total
	document.getElementById("total_4").innerHTML = day4Total
	document.getElementById("total_5").innerHTML = day5Total
	document.getElementById("total_6").innerHTML = day6Total
	document.getElementById("total_7").innerHTML = day7Total
	document.getElementById("total_8").innerHTML = day8Total
	document.getElementById("total_9").innerHTML = day9Total
	
	//################# Now the brands ##################
	var catList = new Array(<%=brandList%>)
	var curHigh, curHighCell, curLow, curLowCell
	var day1Total = 0
	var day2Total = 0
	var day3Total = 0
	var day4Total = 0
	var day5Total = 0
	var day6Total = 0
	var day7Total = 0
	var day8Total = 0
	var day9Total = 0
	for (var i=0; i < catList.length; i++) {
		curHigh = 0
		curHighCell = 0
		curLow = 100
		curLowCell = 0
		for (x=1; x < 10; x++) {
			curVal = parseInt(document.getElementById("b_" + catList[i] + "_" + x).innerHTML)
			if (x == 1) { day1Total = day1Total + curVal }
			if (x == 2) { day2Total = day2Total + curVal }
			if (x == 3) { day3Total = day3Total + curVal }
			if (x == 4) { day4Total = day4Total + curVal }
			if (x == 5) { day5Total = day5Total + curVal }
			if (x == 6) { day6Total = day6Total + curVal }
			if (x == 7) { day7Total = day7Total + curVal }
			if (x == 8) { day8Total = day8Total + curVal }
			if (x == 9) { day9Total = day9Total + curVal }
			if (curVal > curHigh) { curHigh = curVal; curHighCell = x }
			if (curVal < curLow) { curLow = curVal; curLowCell = x }
		}
		for (x=1; x < 10; x++) {
			curVal = parseInt(document.getElementById("b_" + catList[i] + "_" + x).innerHTML)
			if (curVal == curLow) { document.getElementById("b_" + catList[i] + "_" + x).style.backgroundColor = "#eab5c8" }
			if (curVal == curHigh) { document.getElementById("b_" + catList[i] + "_" + x).style.backgroundColor = "#bbecb1" }
		}
	}
	document.getElementById("brandTotal_1").innerHTML = day1Total
	document.getElementById("brandTotal_2").innerHTML = day2Total
	document.getElementById("brandTotal_3").innerHTML = day3Total
	document.getElementById("brandTotal_4").innerHTML = day4Total
	document.getElementById("brandTotal_5").innerHTML = day5Total
	document.getElementById("brandTotal_6").innerHTML = day6Total
	document.getElementById("brandTotal_7").innerHTML = day7Total
	document.getElementById("brandTotal_8").innerHTML = day8Total
	document.getElementById("brandTotal_9").innerHTML = day9Total
	
	//################# Now the methods ##################
	var catList = new Array(<%=methodList%>)
	var curHigh, curHighCell, curLow, curLowCell
	var day1Total = 0
	var day2Total = 0
	var day3Total = 0
	var day4Total = 0
	var day5Total = 0
	var day6Total = 0
	var day7Total = 0
	var day8Total = 0
	var day9Total = 0
	for (var i=0; i < catList.length; i++) {
		curHigh = 0
		curHighCell = 0
		curLow = 100
		curLowCell = 0
		for (x=1; x < 10; x++) {
			curVal = parseInt(document.getElementById("m_" + catList[i] + "_" + x).innerHTML)
			if (x == 1) { day1Total = day1Total + curVal }
			if (x == 2) { day2Total = day2Total + curVal }
			if (x == 3) { day3Total = day3Total + curVal }
			if (x == 4) { day4Total = day4Total + curVal }
			if (x == 5) { day5Total = day5Total + curVal }
			if (x == 6) { day6Total = day6Total + curVal }
			if (x == 7) { day7Total = day7Total + curVal }
			if (x == 8) { day8Total = day8Total + curVal }
			if (x == 9) { day9Total = day9Total + curVal }
			if (curVal > curHigh) { curHigh = curVal; curHighCell = x }
			if (curVal < curLow) { curLow = curVal; curLowCell = x }
		}
		for (x=1; x < 10; x++) {
			curVal = parseInt(document.getElementById("m_" + catList[i] + "_" + x).innerHTML)
			if (curVal == curLow) { document.getElementById("m_" + catList[i] + "_" + x).style.backgroundColor = "#eab5c8" }
			if (curVal == curHigh) { document.getElementById("m_" + catList[i] + "_" + x).style.backgroundColor = "#bbecb1" }
		}
	}
	document.getElementById("methodTotal_1").innerHTML = day1Total
	document.getElementById("methodTotal_2").innerHTML = day2Total
	document.getElementById("methodTotal_3").innerHTML = day3Total
	document.getElementById("methodTotal_4").innerHTML = day4Total
	document.getElementById("methodTotal_5").innerHTML = day5Total
	document.getElementById("methodTotal_6").innerHTML = day6Total
	document.getElementById("methodTotal_7").innerHTML = day7Total
	document.getElementById("methodTotal_8").innerHTML = day8Total
	document.getElementById("methodTotal_9").innerHTML = day8Total
</script>