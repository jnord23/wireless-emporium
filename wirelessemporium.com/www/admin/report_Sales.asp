<%
pageTitle = "Admin - Generate Sales Report"
header = 1
Server.ScriptTimeout = 9000 'seconds
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" width="750" align="center" cellpadding="0" cellspacing="0"><tr><td>
<p>&nbsp;</p>
<p class="biggerText"><%=pageTitle%></p>
<br>

<%
strError = ""

if request("submitted") <> "" then
	store = request.Form("store")
	StartDate = request.form("dc1")
	EndDate = request.form("dc2")
	if not isDate(StartDate) then
		strError = "Start Date must be a valid date."
	else
		StartDate = dateValue(StartDate)
	end if
	if not isDate(EndDate) then
		strError = "End Date must be a valid date."
	else
		EndDate = dateValue(EndDate)
	end if
	if strError = "" then
		if StartDate > EndDate then
			strError = "Start Date must be earlier than or equal to End Date."
		end if
	end if
	if strError = "" then
		strDates = StartDate & "<br>to<br>" & EndDate
		strCriteria = ""
		SQL = "SELECT B.store, A.itemid, B.orderid, B.accountid, C.itemDesc, SUM(A.quantity) AS Qty"
		SQL = SQL & ", case when x.site_id = 0 then c.price_our"
		SQL = SQL & "		when x.site_id = 1 then c.price_ca"
		SQL = SQL & "		when x.site_id = 2 then c.price_co"
		SQL = SQL & "		when x.site_id = 3 then c.price_ps"
		SQL = SQL & "		when x.site_id = 10 then case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end"
		SQL = SQL & "		else c.price_our"
		SQL = SQL & "	end price_our, C.COGS "
		SQL = SQL & " FROM we_orderdetails A INNER JOIN we_orders B ON A.orderid=B.orderid join xstore x on b.store = x.site_id "
		SQL = SQL & " INNER JOIN we_items C ON A.itemid=C.itemid"
		SQL = SQL & " WHERE B.orderdatetime >= '" & StartDate & "' AND B.orderdatetime < '" & dateAdd("D",1,EndDate) & "'"
		SQL = SQL & " AND B.approved = 1 AND (B.cancelled IS NULL OR B.cancelled = 0)"
		SQL = SQL & " AND A.returned = 0"
		if store <> "" then
			SQL = SQL & " AND B.store = " & store
		end if
		if request("category") <> "" then
			SQL = SQL & " AND C.typeID='" & request("category") & "'"
			set RS = oConn.execute("SELECT typeName FROM we_Types WHERE typeID='" & request("category") & "'")
			if not RS.eof then strCriteria = strCriteria & "&nbsp;&nbsp;&nbsp;Cat:&nbsp;<font color=red>" & RS("typeName") & "</font>"
			RS.close
		end if
		if request("itemID") <> "" then
			SQL = SQL & " AND C.itemID='" & request("itemID") & "'"
			strCriteria = strCriteria & "&nbsp;&nbsp;&nbsp;itemID:&nbsp;<font color=red>" & request("itemID") & "</font>"
		end if
		if request("PartNumber") <> "" then
			SQL = SQL & " AND C.PartNumber LIKE '%" & replace(request("PartNumber"),"%","") & "%'"
			if instr(request("PartNumber"),"%") > 0 then
				strCriteria = strCriteria & "&nbsp;&nbsp;&nbsp;Part Number Like: <font color=red>" & ucase(replace(request("PartNumber"),"%","")) & "</font>"
			else
				strCriteria = strCriteria & "&nbsp;&nbsp;&nbsp;Part&nbsp;Number:&nbsp;<font color=red>" & ucase(request("PartNumber")) & "</font>"
			end if
		end if
		if request("Brand") <> "" then
			SQL = SQL & " AND C.brandID='" & request("Brand") & "'"
			set RS = oConn.execute("SELECT brandName FROM we_Brands WHERE brandID='" & request("Brand") & "'")
			if not RS.eof then strCriteria = strCriteria & "&nbsp;&nbsp;&nbsp;Brand:&nbsp;<font color=red>" & RS("brandName") & "</font>"
			RS.close
		end if
		if request("Model") <> "" then
			SQL = SQL & " AND C.modelID='" & request("Model") & "'"
			set RS = oConn.execute("SELECT modelName FROM we_Models WHERE modelID='" & request("Model") & "'")
			if not RS.eof then strCriteria = strCriteria & "&nbsp;&nbsp;&nbsp;Model:&nbsp;<font color=red>" & RS("modelName") & "</font>"
			RS.close
		end if
		SQL = SQL & " GROUP BY B.store, A.itemid, C.itemDesc "
		SQL = SQL & ", case when x.site_id = 0 then c.price_our"
		SQL = SQL & "		when x.site_id = 1 then c.price_ca"
		SQL = SQL & "		when x.site_id = 2 then c.price_co"
		SQL = SQL & "		when x.site_id = 3 then c.price_ps"
		SQL = SQL & "		when x.site_id = 10 then case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end"
		SQL = SQL & "		else c.price_our"
		SQL = SQL & "	end, C.COGS, B.orderid, B.accountid order by B.store"
		session("errorSQL") = SQL
		set RS = Server.CreateObject("ADODB.Recordset")
'		response.write sql
'		response.end
		RS.open SQL, oConn, 0, 1
		
		function storeTotalLine (byRef curQty,byRef curTotal,byRef curCogs, curTitleColor)
		%>
        <tr bgcolor="<%=curTitleColor%>" style="color:#FFF; font-weight:bold;">
        	<td colspan="3">&nbsp;</td>
            <td align="right"><%=curQty%></td>
            <td>&nbsp;</td>
            <td align="right"><%=formatCurrency(curTotal,2)%></td>
            <td align="right"><%=formatCurrency(curCogs,2)%></td>
        </tr>
        <%
			curQty = 0
			curTotal = 0
			curCogs = 0
		end function
		
		dim lap : lap = 0
		dim activeStore : activeStore = ""
		dim storeQty : storeQty = 0
		dim storeTotal : storeTotal = 0
		dim storeCogs : storeCogs = 0
		if not RS.eof then
			lap = lap + 1
			' Save as CSV:
			dim fs, file, filename, path
			set fs = CreateObject("Scripting.FileSystemObject")
			filename = "report_Sales.csv"
			path = Server.MapPath("/admin/tempCSV/" & filename)
			session("errorSQL") = path
			set file = fs.CreateTextFile(path, true)
			file.WriteLine "Store,ItemID,ItemDesc,Qty,Price_Our,Cogs"
			response.write "<p class=""boldText"">Your CSV file is <a href=""/admin/tempCSV/" & filename & """>here</a>.</p>"
			%>
			<table border="1" width="100%" cellpadding="2" cellspacing="0">
				<tr>
					<td><b>Item&nbsp;ID</b></td>
					<td><b>Item&nbsp;Description</b></td>
					<td><b>Order&nbsp;ID</b></td>
					<td><b>Qty</b></td>
					<td><b>Our&nbsp;Price</b></td>
					<td><b>TOTAL</b></td>
                    <td><b>COST</b></td>
				</tr>
				<%
				do until RS.eof
					curStore = RS("store")
					curItemID = RS("itemid")
					curItemDesc = RS("itemDesc")
					curOrderID = RS("orderid")
					curAccountID = RS("accountid")
					curPrice_our = cdbl(RS("price_Our"))
					curQty = RS("Qty")
					curCogs = RS("COGS")
					if curStore <> activeStore then
						activeStore = curStore
						if curStore = 0 then
							storeName = "WirelessEmporium"
							storeTitleColor = "#ff6633"
							storeColor = "#ffb69e"
						elseif curStore = 1 then
							storeTotalLine storeQty, storeTotal, storeCogs, storeTitleColor
							storeName = "CellphoneAccents"
							storeTitleColor = "#663399"
							storeColor = "#c4acdc"
						elseif curStore = 2 then
							storeTotalLine storeQty, storeTotal, storeCogs, storeTitleColor
							storeName = "CellularOutfitter"
							storeTitleColor = "#0066cc"
							storeColor = "#99ccff"
						elseif curStore = 3 then
							storeTotalLine storeQty, storeTotal, storeCogs, storeTitleColor
							storeName = "PhoneSale"
							storeTitleColor = "#993333"
							storeColor = "#e9adad"
						end if
						response.Write("<tr style='background-color:" & storeTitleColor & ";color:#fff;font-weight:bold;'><td align='left' colspan='7'>" & storeName & "</td></tr>")
					end if
					%>
					<tr style="background-color:<%=storeColor%>;">
						<td align="right"><%=curItemID%></td>
						<td align="left"><%=curItemDesc%></td>
						<td align="right"><a href="#" onClick="printinvoice('<%=curOrderID%>','<%=curAccountID%>');"><%=curOrderID%></a></td>
						<td align="right"><%=curQty%></td>
						<td align="right"><%=formatCurrency(curPrice_our)%></td>
						<td align="right"><%=formatCurrency(curQty * curPrice_our)%></td>
                        <td align="right">
							<%
							if trim(curCogs) = "" or isnull(curCogs) then
								COGS = 0
							else
								COGS = curCogs
							end if
							response.write(formatCurrency(curQty * COGS))
							%>
						</td>
					</tr>
					<%
					Qty = Qty + curQty
					GrandTotal = GrandTotal + (curQty * curPrice_our)
					CostTotal = CostTotal + (curQty * COGS)
					storeQty = storeQty + curQty
					storeTotal = storeTotal + (curQty * curPrice_our)
					storeCogs = storeCogs + (curQty * COGS)
					file.WriteLine storeName & "," & curItemID & "," & curItemDesc & "," & curQty & "," & curPrice_Our & "," & curCogs
					RS.movenext
				loop
				storeTotalLine storeQty, storeTotal, storeCogs, storeTitleColor
				%>
				<tr>
					<td align="center"><b><%=strDates%></b></td>
					<td align="left"><b><%=strCriteria%></b></td>
					<td><b>&nbsp;</b></td>
					<td align="right"><b><%=Qty%></b></td>
					<td><b>&nbsp;</b></td>
					<td align="right"><b><%=formatCurrency(GrandTotal)%></b></td>
                    <td align="right"><b><%=formatCurrency(CostTotal)%></b></td>
				</tr>
			</table>
			<%
		else
			response.write "<p class=""boldText"">No records matched.</p><p class=""boldText"">No report generated.</p>"
		end if
		response.write "<p>&nbsp;</p>"
		response.write "<p class=""normalText""><a href=""report_Sales.asp"">Generate another Sales Report</a></p>"
		RS.close
		Set RS = nothing
	end if
end if
if request("submitted") = "" or strError <> "" then
	%>
	<p class="normalText"><font color="#FF0000"><b><%=strError%></b></font></p>
	<form action="report_Sales.asp" name="frmSalesReport" method="post">
		<p class="normalText">
        	<select name="store">
				<option value="">Select All Web Stores</option>
				<option value="0">Wireless Emporium</option>
				<option value="2">Cellular Outfitter</option>
				<option value="1">Cellphone Accents</option>
				<option value="3">Phone Sale</option>
				<option value="10">eReader Supply</option>
			</select>
        </p>
		<hr>
        <p class="normalText">
			<select name="category">
				<option value="">-- Select Category --</option>
				<option value="1">Batteries</option>
				<option value="2">Chargers</option>
				<option value="3">Faceplates</option>
				<option value="7">Leather Cases</option>
				<option value="6">Holsters/Belt Clips</option>
				<option value="5">Hands-Free</option>
				<option value="12">Antennas/Parts</option>
				<option value="13">Data Cable &amp; Memory</option>
				<option value="14">Bling Kits &amp; Charms</option>
				<option value="16">Cell Phones</option>
			</select>
		</p>
		<hr>
		<p class="normalText"><input type="text" name="itemID">&nbsp;Item ID</p>
		<hr>
		<p class="normalText"><input type="text" name="PartNumber">&nbsp;Part Number</p>
		<hr>
		<p class="normalText">
			<select id="Brand" name="Brand" onChange="ajax('/ajax/admin/ajaxReport_Sales.asp?brandID=' + this.value,'modelBox');">
				<option value="">-- Select Brand --</option>
				<%
				SQL = "SELECT * FROM WE_Brands ORDER BY brandName"
				Set RS = Server.CreateObject("ADODB.Recordset")
				RS.open SQL, oConn, 0, 1
				do until RS.eof
				%>
                <option value="<%=RS("brandID")%>"><%=RS("brandName")%></option>
                <%
					RS.movenext
				loop
				RS = null
				%>
			</select>
			<br><br>
            <div id="modelBox">
            	<select id="Model" name="Model">
				    <option value="">-- Select Model --</option>
                </select>
            </div>
		</p>
		<hr>
		<p class="normalText">
			<input type="text" name="dc1" value="<%=dateAdd("d",-1,date)%>">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.frmSalesReport.dc1,document.frmSalesReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;Start Date
			<br>
			<input type="text" name="dc2" value="<%=dateAdd("d",-1,date)%>">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fEndPop(document.frmSalesReport.dc1,document.frmSalesReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;End Date
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
		</p>
		<hr>
		<p class="normalText">
			<input type="submit" name="submitted" value="Submit">
		</p>
	</form>
	<%
end if
%>

</td></tr></table>

<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->

<script language="javascript">
function printinvoice(orderid,accountid) {
	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
</script>
