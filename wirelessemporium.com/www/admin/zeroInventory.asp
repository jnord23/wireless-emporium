<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Zero Inventory"
	header = 1
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim sql, brandArray
	
	session("orderBy") = null
	session("viewHideLive") = null
	session("viewGhost") = null
	session("repullSQL") = null
	
	sql = "select brandID, brandName from we_brands order by brandName"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	sql = "select typeID, typeName from we_types order by typeName"
	session("errorSQL") = sql
	Set catRS = Server.CreateObject("ADODB.Recordset")
	catRS.open sql, oConn, 0, 1
	
	brandArray = ""
	do while not rs.EOF
		brandArray = brandArray & rs("brandID") & "#" & rs("brandName") & "$"
		rs.movenext
	loop
	brandArray = split(brandArray,"$")
%>
<form style="margin:0px;" action="/admin/inventoryAdjust.asp" method="post" name="slaveForm" onsubmit="return(testForm(this))">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="600">
	<tr>
    	<td><h1 style="margin:0px;">Zero Inventory</h1></td>
    </tr>
    <tr>
    	<td style="font-size:10px; padding-bottom:10px;">
        	Quickly view sold out items and HideLive or Ghost the items
        </td>
    </tr>
    <tr>
    	<td style="border-top:1px solid #999; border-bottom:1px solid #999; font-weight:bold; font-size:16px; background-color:#000; color:#FFF; padding-left:10px;">Select Product By Brand/Model</td>
    </tr>
    <tr>
    	<td id="brandBox" style="padding-top:5px;">
        	<div style="width:600px;">
	        	<div style="float:left; width:140px;">Select Brand:</div>
    	        <div style="float:left; padding-left:10px; width:450px;">
	    	    	<select name="brand" onchange="brandSelect(this.value)">
    	    	    	<option value="">Select Brand</option>
        	    	    <%
						for i = 0 to (ubound(brandArray) - 1)
							useArray = split(brandArray(i),"#")
						%>
		                <option value="<%=useArray(0)%>"><%=useArray(1)%></option>
    		            <%
						next
						%>
		            </select>
    	        </div>
            </div>
        </td>
    </tr>
    <tr>
        <td id="modelBox" style="padding-top:5px;"></td>
    </tr>
    <tr>
        <td id="catBox" style="padding-top:5px;"></td>
    </tr>
    <tr>
    	<td style="border-top:1px solid #999; border-bottom:1px solid #999; font-weight:bold; font-size:16px; background-color:#000; color:#FFF; padding-left:10px;">Other Search Methods</td>
    </tr>
    <tr>
    	<td id="brandBox" style="padding-top:5px;">
        	<div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">ItemID:</div>
    	        <div style="float:left; padding-left:10px; width:100px;">
	    	    	<input type="text" name="itemID" value="" onchange="pullByID(this.value)" />
    	        </div>
            </div>
            <div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">Part Number:</div>
    	        <div style="float:left; padding-left:10px; width:100px;">
	    	    	<input type="text" name="partNumber" value="" onchange="pullByPN(this.value)" />
    	        </div>
            </div>
            <div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">Category:</div>
    	        <div style="float:left; padding-left:10px; width:300px;">
	    	    	<select name="catSearch" onchange="pullByCat(this.value)">
                    	<option value=""></option>
						<%
						do while not catRS.EOF
						%>
                        <option value="<%=catRS("typeID")%>"><%=catRS("typeName")%></option>
                        <%
							catRS.movenext
						loop
						%>
                    </select>
    	        </div>
            </div>
            <div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">Keyword:</div>
    	        <div style="float:left; padding-left:10px; width:100px;">
	    	    	<input type="text" name="keyword" value="" onchange="pullByKey(this.value)" />
    	        </div>
            </div>
            <div style="width:600px; float:left;">
	        	<div style="float:left; width:310px;" style="font-size:12px; text-align:center;"><input type="checkbox" onclick="hideLiveSelect(this.checked)" /> Show HideLive / <input type="checkbox" onclick="ghostSelect(this.checked)" /> Show Ghost</div>
                <div style="float:left; width:140px;"><input type="submit" name="searchBttn" value="Search" /></div>
            </div>
        </td>
    </tr>
    <tr>
    	<td align="right" style="border-top:1px solid #999; border-bottom:1px solid #999; font-weight:bold; font-size:16px; background-color:#000;"><input type="button" name="clearForm" value="Clear Form" onclick="window.location='/admin/zeroInventory.asp'" /></td>
    </tr>
    <tr>
        <td id="resultBox" style="padding-top:5px;"></td>
    </tr>
</table>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function brandSelect(brandID) {
		ajax('/ajax/admin/ajaxZeroInventory.asp?brandID=' + brandID,'modelBox')
		document.getElementById("catBox").innerHTML = ""
		document.getElementById("resultBox").innerHTML = ""
	}
	function modelSelect(modelID) {
		ajax('/ajax/admin/ajaxZeroInventory.asp?modelID=' + modelID,'catBox')
		document.getElementById("resultBox").innerHTML = ""
	}
	function catSelect(catID,modelID) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxZeroInventory.asp?modelID=' + modelID + '&catID=' + catID,'resultBox')
	}
	function resultOrder(catID,modelID,orderBy) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxZeroInventory.asp?modelID=' + modelID + '&catID=' + catID + '&orderBy=' + orderBy,'resultBox')
	}
	function pullByID(itemID) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxZeroInventory.asp?itemID=' + itemID,'resultBox')
	}
	function pullByPN(partNumber) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxZeroInventory.asp?partNumber=' + partNumber,'resultBox')
	}
	function pullByCat(catID) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxZeroInventory.asp?categoryID=' + catID,'resultBox')
	}
	function pullByKey(keyword) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxZeroInventory.asp?keyword=' + keyword,'resultBox')
	}
	function updateInv(itemID,invQty,curInvQty) {
		ajax('/ajax/admin/ajaxZeroInventory.asp?updateItemID=' + itemID + '&updateInvQty=' + invQty + '&curInvQty=' + curInvQty,'dumpZone')
		setTimeout("showResult(" + itemID + ")",1000)
	}
	function showResult(itemID) {
		var saveHTML = document.getElementById(itemID + '_return').innerHTML
		document.getElementById(itemID + '_return').innerHTML = document.getElementById('dumpZone').innerHTML
		setTimeout("document.getElementById('" + itemID + "_return').innerHTML = '" + saveHTML + "'",1000)
	}
	function displayInvHistory(itemID) {
		if (document.getElementById("invHistory_" + itemID).innerHTML == "") {
			ajax('/ajax/admin/ajaxZeroInventory.asp?showHistory=' + itemID,'invHistory_' + itemID)
		}
		else {
			document.getElementById("invHistory_" + itemID).innerHTML = ""
		}
	}
	function testForm(curForm) {
		return false
	}
	function hideLiveSelect(dir) {
		if (document.getElementById("resultBox").innerHTML == "") {
			ajax('/ajax/admin/ajaxZeroInventory.asp?viewHideLive=' + dir,'resultBox')
		}
		else {
			document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
			ajax('/ajax/admin/ajaxZeroInventory.asp?viewHideLive=' + dir + '&repull=1','resultBox')
		}
	}
	function ghostSelect(dir) {
		if (document.getElementById("resultBox").innerHTML == "") {
			ajax('/ajax/admin/ajaxZeroInventory.asp?viewGhost=' + dir,'resultBox')
		}
		else {
			document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
			ajax('/ajax/admin/ajaxZeroInventory.asp?viewGhost=' + dir + '&repull=1','resultBox')
		}
	}
	function setGhost(partNumber,dir) {
		ajax('/ajax/admin/ajaxZeroInventory.asp?setGhost=' + partNumber + '&ghostDir=' +  dir,'dumpZone')
		setTimeout("alert(document.getElementById('dumpZone').innerHTML)",500)
	}
	function setHideLive(partNumber,dir) {
		ajax('/ajax/admin/ajaxZeroInventory.asp?setHideLive=' + partNumber + '&hideLiveDir=' +  dir,'dumpZone')
		setTimeout("alert(document.getElementById('dumpZone').innerHTML)",500)
	}
</script>