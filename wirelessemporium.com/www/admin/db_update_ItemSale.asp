<%
response.buffer = false
dim thisUser, adminID
pageTitle = "Admin - Update WE Database - Update Item Sale"
header = 1
thisUser = Request.Cookies("username")
adminID = Request.Cookies("adminID")
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<%
if request.form("submitted") = "Update" then
	dim strError
	strError = ""
	if request.form("SalePartNumber") <> "" then
		if not isNumeric(request.form("SalePriceRegular")) or request.form("SalePriceRegular") = "0" or request.form("SalePriceRegular") = "" then
			strError = "You must enter a valid amount for the Current Sale Item's Regular Price."
		else
			SQL = "UPDATE WE_Items SET"
			SQL = SQL & " price_Our = " & request.form("SalePriceRegular") & ", "
			SQL = SQL & " ItemSale = 0, "
			SQL = SQL & " PriceLastUpdatedBy = '" & adminID & "', "
			SQL = SQL & " PriceLastUpdatedDate = '" & now & "'"
			SQL = SQL & " WHERE PartNumber='" & request.form("SalePartNumber") & "'"
			response.write "<h3>" & SQL & "</h3>"
			'oConn.execute SQL
			response.write "<h3>Current Sale Item UPDATED!</h3>"
		end if
	end if
	if strError = "" then
		if len(request.form("PartNumber")) = 15 then
			if not isNumeric(request.form("price_Our")) or request.form("price_Our") = "0" or request.form("price_Our") = "" then
				strError = "You must enter a valid amount for the New Sale Item's Regular Price."
			else
				SQL = "UPDATE WE_Items SET"
				SQL = SQL & " price_Our = " & request.form("price_Our") & ", "
				SQL = SQL & " ItemSale = -1, "
				SQL = SQL & " PriceLastUpdatedBy = '" & adminID & "', "
				SQL = SQL & " PriceLastUpdatedDate = '" & now & "'"
				SQL = SQL & " WHERE PartNumber='" & request.form("PartNumber") & "'"
				response.write "<h3>" & SQL & "</h3>"
				'oConn.execute SQL
				response.write "<h3>New Sale Item UPDATED!</h3>"
			end if
		end if
	end if
	response.write "<h3>" & strError & "</h3>"
	response.write "<p><a href=""javascript:history.back();"">BACK</a></p>"
else
	SQL = "SELECT * FROM WE_Items WHERE ItemSale = 1 AND (hidelive IS NULL OR hidelive = 0)"
	SQL = SQL & " ORDER BY inv_qty DESC"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	a = 0
	do until RS.eof
		if RS("ItemSale") = true then
			SalePartNumber = RS("PartNumber")
			SalePriceOur = RS("price_Our")
		end if
		RS.movenext
	loop
	%>
	<form name="frmEditItem" action="db_update_ItemSale.asp" method="post">
		<table border="1" cellpadding="3" cellspacing="0" width="400" align="center">
			<tr>
				<td align="center" colspan="3"><b>Current Sale Item:</b></td>
			</tr>
			<tr>
				<td align="center">Part Number: <%=SalePartNumber%><input type="hidden" name="SalePartNumber" value="<%=SalePartNumber%>"></td>
				<td align="center">Sale Price: <%=SalePriceOur%></td>
				<td align="center">Regular Price: <input type="text" name="SalePriceRegular" size="3" value=""></td>
			</tr>
			<tr>
				<td align="center" colspan="3"><b>Enter New Sale Item:</b></td>
			</tr>
			<tr>
				<td align="center">Part Number: <input type="text" name="PartNumber" size="15" value="" maxlength="15"></td>
				<td align="center">Sale Price: <input type="text" name="price_Our" size="3" value=""></td>
				<td align="center">&nbsp;</td>
			</tr>
		</table>
		<%if thisUser = "eugeneku" or thisUser = "YRL_tony" or thisUser = "johnw" or thisUser = "ericac" or thisUser = "armandol" or thisUser = "mariog" or thisUser = "leanneh" or thisUser = "michaelc" or thisUser = "jnord" then%>
			<p align="center"><input type="submit" name="submitted" value="Update"></p>
		<%end if%>
	</form>
	<p>&nbsp;</p>
	<%
	RS.close
	set RS = nothing
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
