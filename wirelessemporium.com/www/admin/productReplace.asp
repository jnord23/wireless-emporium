<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Product Replace"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	sql = "select brandID, brandName from we_brands order by brandName"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
%>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="350">
	<tr>
    	<td><h1 style="margin:0px;">Update Product Name</h1></td>
    </tr>
    <tr>
    	<td style="font-size:10px;">
        	Quickly change the name of a product and update all associated items to use the new name as well.
        </td>
    </tr>
    <tr>
    	<td id="brandBox" style="padding-top:5px;">
        	<div style="width:350px;">
	        	<div style="float:left;">Select Brand:</div>
    	        <div style="float:left; padding-left:10px;">
	    	    	<select name="brand" onchange="brandSelect(this.value)">
    	    	    	<option value="">Select Brand</option>
        	    	    <%
						do while not rs.EOF
						%>
		                <option value="<%=rs("brandID")%>"><%=rs("brandName")%></option>
    		            <%
							rs.movenext
						loop
						%>
		            </select>
    	        </div>
            </div>
        </td>
    </tr>
    <tr>
        <td id="modelBox" style="padding-top:5px;"></td>
    </tr>
    <tr>
        <td id="resultBox" style="padding-top:5px;"></td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	var testLap = 0
	function brandSelect(brandID) {
		ajax('/ajax/admin/ajaxProductReplace.asp?brandID=' + brandID,'modelBox')
	}
	function modelSelect(modelID) {
		ajax('/ajax/admin/ajaxProductReplace.asp?modelID=' + modelID,'resultBox')
	}
	function nameUpdate(modelID) {
		var newName = document.nameChangeForm.productName.value
		var oldName = document.nameChangeForm.currentName.value
		document.getElementById("dumpZone").innerHTML = ""
		ajax('/ajax/admin/ajaxProductReplace.asp?oldName=' + escape(oldName) + '&newName=' + escape(newName) + '&modelID=' + modelID,'dumpZone')
		setTimeout("test4Result()",1000)
		return false
	}
	function test4Result() {
		if (document.getElementById('dumpZone').innerHTML == "") {
			testLap++
			if (testLap < 6) {
				setTimeout("test4Result()",1000)
			}
		}
		else {
			alert(document.getElementById('dumpZone').innerHTML)
			window.location = "/admin/productReplace.asp"
		}
	}
</script>
