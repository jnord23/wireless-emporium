<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Main Banners"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	viewSite = sqlQuote(request.QueryString("viewSite"))
	
	if isnull(viewSite) or len(viewSite) < 1 then viewSite = "CO"
	
	if viewSite = "CO" then siteName = "cellularoutfitter"
	
	sql = "select * from " & viewSite & "_mainbanners where active = 1 order by orderNum"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 3, 3
%>
<form name="bannerForm">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="300">
	<tr>
    	<td align="right" nowrap="nowrap" style="font-weight:bold; font-size:12px;">Site to Manage:</td>
        <td align="left" width="100%">
        	<select name="viewSite" onchange="ajax('/ajax/admin/mainbanners_detail.asp?viewSite=' + this.value,'bannerDetails')">
            	<option value="">Select Site</option>
                <option value="WE"<% if viewSite = "WE" then %> selected="selected"<% end if %>>WirelessEmporium</option>
                <option value="CO"<% if viewSite = "CO" then %> selected="selected"<% end if %>>CellularOutfitter</option>
                <!--<option value="CA"<% if viewSite = "CA" then %> selected="selected"<% end if %>>CellphoneAccents</option>
                <option value="PS"<% if viewSite = "PS" then %> selected="selected"<% end if %>>PhoneSale</option>
                <option value="ER"<% if viewSite = "ER" then %> selected="selected"<% end if %>>eReaderSupply</option>
                <option value="FG"<% if viewSite = "FG" then %> selected="selected"<% end if %>>FatGamer</option>-->
            </select>
        </td>
    </tr>
    <tr>
    	<td colspan="2" style="padding-top:20px;" id="bannerDetails">
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" align="center">
                <tr bgcolor="#333333" style="font-weight:bold; color:#FFF; font-size:12px;">
                    <td align="left">Position</td>
                    <td align="left">Image</td>
                    <td align="left">Action</td>
                </tr>
                <%
                lap = 0
                do while not rs.EOF
                    lap = lap + 1
                %>
                <tr>
                    <td align="center">
						<table border="0" cellpadding="0" cellspacing="0">
                        	<tr><td align="center"><%=lap%></td></tr>
                            <tr><td align="center" nowrap="nowrap" style="font-size:10px">Default View</td></tr>
                            <tr><td align="center"><input type="checkbox" name="defaultView" value="1"<% if rs("defaultBanner") then %> checked="checked"<% end if %> /></td></tr>
                        </table>
                    </td>
                    <td>
                    	<table border="0" cellpadding="0" cellspacing="0">
                        	<tr>
                            	<td colspan="2" align="center"><img src="http://www.<%=siteName%>.com/images/mainbanner/<%=rs("picLoc")%>" height="100" /></td>
                            </tr>
                            <tr>
                            	<td nowrap="nowrap" align="right" style="font-weight:bold; font-size:10px;">Link:</td>
                                <td align="left"><input type="text" name="bLink" value="<%=rs("link")%>" size="42" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=link&updateText=1&newVal=' + escape(this.value),'dumpZone')" /></td>
                            </tr>
                            <tr>
                            	<td nowrap="nowrap" align="right" style="font-weight:bold; font-size:10px;">Title:</td>
                                <td align="left"><input type="text" name="title" value="<%=rs("tabTitle")%>" size="42" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=tabTitle&updateText=1&newVal=' + escape(this.value),'dumpZone')" /></td>
                            </tr>
                            <tr>
                            	<td nowrap="nowrap" align="right" style="font-weight:bold; font-size:10px;">Title Font:</td>
                                <td align="left"><input type="text" name="titleFont" value="<%=rs("tabTitleFont")%>" size="42" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=tabTitleFont&updateText=1&newVal=' + escape(this.value),'dumpZone')" /></td>
                            </tr>
                            <tr>
                            	<td nowrap="nowrap" align="right" style="font-weight:bold; font-size:10px;">Subtitle:</td>
                                <td align="left"><input type="text" name="subtitle" value="<%=rs("tabSubTitle")%>" size="42" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=tabSubTitle&updateText=1&newVal=' + escape(this.value),'dumpZone')" /></td>
                            </tr>
                            <tr>
                            	<td nowrap="nowrap" align="right" style="font-weight:bold; font-size:10px;">subtitle Font:</td>
                                <td align="left"><input type="text" name="subtitleFont" value="<%=rs("tabSubTitleFont")%>" size="42" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=tabSubTitleFont&updateText=1&newVal=' + escape(this.value),'dumpZone')" /></td>
                            </tr>
                            <tr><td colspan="2" align="right"><input type="button" name="SaveBttn" value="Save Text Changes" style="font-size:9px;" /></td></tr>
                        </table>
                    </td>
                    <td align="left">
                    	<div>
                        	<div style="float:left;">
		                        <select name="bannerAction_<%=lap%>" onchange="ajax('/ajax/admin/adjustBanners.asp?viewSite=<%=viewSite%>&lap=<%=lap%>&myAction=' + this.value,'actionArea_<%=lap%>')">
        		                	<option value="">Select Action</option>
                		            <option value="rp">Replace</option>
		                        </select>
                            </div>
                            <div style="float:left; width:80px; text-align:right;"><a style="cursor:pointer; color:#F00;" onclick="cancelAction(<%=lap%>)">Cancel</a></div>                            
                        </div>
                        <div style="width:300px;height:190px;;overflow:auto;" id="actionArea_<%=lap%>"></div>
                    </td>
                </tr>
                <%
                    rs.movenext
                loop
                %>
            </table>
        </td>
    </tr>
</table>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function cancelAction(lap) {
		document.getElementById('actionArea_' + lap).innerHTML=''
		eval("document.bannerForm.bannerAction_" + lap + ".selectedIndex = 0")
	}
</script>