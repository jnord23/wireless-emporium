<%
response.buffer = false
pageTitle = "Upload WE Amazon Orders"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->

<%
dim Path
'Path = "D:\inetpub\wwwroot\wirelessemporium.com\www\admin\tempTXT"
Path = server.mappath("\admin\tempTXT")

set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
Count = Upload.Save(Path)
if Upload.form("upload") <> "" then
	if Count = 0 then
		response.write "No file selected."
		response.end
	else
		dim File, myFile, myDefaultFile, filesys
		set File = Upload.Files(1)
		myFile = File.path
		myDefaultFile = Path & "\Amazon.txt"
		set filesys = CreateObject("Scripting.FileSystemObject")
		filesys.CopyFile myFile, myDefaultFile
		set filesys = nothing
		
		dim txtConn, myStr
		set txtConn = Server.CreateObject("ADODB.Connection")
		myStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Path & ";Extended Properties=""text;HDR=Yes;FMT=Delimited"";"
		txtConn.ConnectionString = myStr
		txtConn.ConnectionTimeout = 1000		'seconds
		txtConn.Open
		
		SQL = "SELECT * FROM Amazon.txt"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, txtConn, 3, 3
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			%>
			<h3><%=RS.recordcount%> records found</h3>
			<table border="1" cellpadding="1" cellspacing="0"><tr><td>WE Order Number</td><td>Amazon Order Number</td></tr>
			<%
			dim Order_Number, holdOrderNumber, SKU, QTY, KIT, updateCount, itemIDarray, itemID
			dim nIdProd, nPartNumber, sSCountry, sBCountry, sShipType, zipcode, subtotal, shippingfee, tax
			dim nameArray, fName, lName
			updateCount = 0
			holdOrderNumber = 99999999
			do until RS.eof
				Order_Number = SQLquote(RS("order-id"))
				SKU = SQLquote(RS("sku"))
				QTY = SQLquote(RS("quantity-purchased"))
				KIT = null
				if SKU <> "" and QTY <> "" then
					itemIDarray = split(SKU,"-")
					nPartNumber = left(SKU,15)
					SQL = "SELECT itemID, PartNumber, ItemKit_NEW FROM we_items WHERE PartNumber='" & nPartNumber & "'"
					set rsTemp = server.createobject("ADODB.recordset")
					rsTemp.open SQL, oConn, 3, 3
					if not rsTemp.eof then
						nIdProd = rsTemp("itemID")
						KIT = rsTemp("ItemKit_NEW")
						subtotal = FormatNumber(RS("item-price"),2)
						shippingfee = FormatNumber(RS("shipping-price"),2)
						tax = FormatNumber(RS("shipping-tax"),2)
						
						if Order_Number = holdOrderNumber then
							sSprocString = "sp_InsertOrderDetail '" & nOrderId & "','" & nIdProd & "','" & QTY & "'"
							'response.write "<li>" & sSprocString & "</li>" & vbcrlf
							oConn.execute(sSprocString)
							
							SQL = "UPDATE we_orders SET"
							SQL = SQL & " ordersubtotal = CAST(ordersubtotal AS REAL) + '" & subtotal & "', "
							SQL = SQL & " ordershippingfee = CAST(ordershippingfee AS REAL) + '" & shippingfee & "', "
							SQL = SQL & " orderTax = orderTax + '" & tax & "', "
							SQL = SQL & " ordergrandtotal = CAST(ordergrandtotal AS REAL) + '" & subtotal & "' + '" & shippingfee & "' + '" & tax & "'"
							SQL = SQL & " WHERE orderid='" & nOrderId & "'"
							'response.write "<li>" & SQL & "</li>" & vbcrlf
							oConn.execute SQL
							
							call UpdateInvQty(nPartNumber, QTY, KIT)
							call NumberOfSales(nIdProd, QTY, KIT)
						else
							if len(SQLquote(RS("ship-country"))) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",SQLquote(RS("ship-state"))) > 0 then
								sSCountry = "CANADA"
								if inStr(RS("ship-service-level"),"Expedited") > 0 then
									sShipType = "USPS Priority Int'l"
								else
									sShipType = "First Class Int'l"
								end if
							else
								sSCountry = ""
								if inStr(RS("ship-service-level"),"Expedited") > 0 then
									sShipType = "USPS Priority"
								else
									sShipType = "First Class"
								end if
							end if
							sBCountry = ""
							
							zipcode = SQLquote(RS("ship-postal-code"))
							do until len(zipcode) > 4
								if len(zipcode) < 5 then
									zipcode = "0" & zipcode
								end if
							loop
							
							nameArray = split(RS("recipient-name")," ")
							lName = nameArray(uBound(nameArray))
							fName = replace(RS("recipient-name")," " & lName,"")
							sState = Ucase(SQLquote(RS("ship-state")))
							
							SQL = "sp_InsertAccountInfo '" & SQLquote(fName) & "','" & SQLquote(lName) & "','" & SQLquote(RS("ship-address-1")) & "','" & SQLquote(RS("ship-address-2")) & "','" & SQLquote(RS("ship-city")) & "','" & stateAbbr(sState) & "','" & zipcode & "','" & sBCountry & "','" & SQLquote(RS("ship-address-1")) & "','" & SQLquote(RS("ship-address-2")) & "','" & SQLquote(RS("ship-city")) & "','" & stateAbbr(sState) & "','" & zipcode & "','" & sSCountry & "','" & SQLquote(RS("ship-phone-number")) & "','" & SQLquote(RS("buyer-email")) & "','','','" & now & "'"
							session("errorSQL") = SQL
							'response.write "<li>" & SQL & "</li>" & vbcrlf
							nAccountId = oConn.execute(SQL).fields(0).value
							
							SQL = "sp_InsertOrder3 0,'" & nAccountId & "','" & subtotal & "','" & shippingfee & "'," & tax & ",'" & cDbl(subtotal) + cDbl(shippingfee) + cDbl(tax) & "','" & SQLquote(sShipType) & "',0,0,'" & now & "'"
							session("errorSQL") = SQL
							'response.write "<li>" & SQL & "</li>" & vbcrlf
							nOrderId = oConn.execute(SQL).fields(0).value
							
							SQL = "UPDATE we_orders SET approved=-1, extOrderType='6', extOrderNumber='" & Order_Number & "' WHERE orderid='" & nOrderId & "'"
							session("errorSQL") = SQL
							'response.write "<li>" & SQL & "</li>" & vbcrlf
							oConn.execute SQL
							
							SQL = "sp_InsertOrderDetail '" & nOrderId & "','" & nIdProd & "','" & QTY & "'"
							session("errorSQL") = SQL
							'response.write "<li>" & SQL & "</li>" & vbcrlf
							oConn.execute(SQL)
							
							call UpdateInvQty(nPartNumber, QTY, KIT)
							call NumberOfSales(nIdProd, QTY, KIT)
							'call CustomerEmail(RS("order-id"), fName, lName, RS("buyer-email"), RS("ship-address-1"), RS("ship-address-2"), RS("ship-city"), stateAbbr(sState), zipcode, RS("ship-phone-number"))
							
							updateCount = updateCount + 1
							holdOrderNumber = Order_Number
						end if
					else
						response.write "<tr><td colspan=""2"">Part Number <b>" & SKU & "</b> not found!</td></tr>" & vbcrlf
					end if
				end if
				RS.movenext
				response.write "<tr><td>" & nOrderId & "</td><td>" & Order_Number & "</td></tr>" & vbcrlf
			loop
		end if
		RS.close
		set RS = nothing
		%>
		</table>
		<p><%=updateCount%> orders entered into WE database.</p>
		<%
	end if
else
	%>
	<h3>Select the file you saved to your hard drive:</h3>
	<form enctype="multipart/form-data" action="uploadAmazon.asp" method="POST">
		<p><input type="FILE" name="FILE1"></p>
		<p><input type="SUBMIT" name="upload" value="Upload"></p>
	</form>
	<%
end if

function SQLquote(strValue)
	if not isNull(strValue) and strValue <> "" then
		SQLquote = trim(replace(strValue,"'","''"))
		SQLquote = trim(replace(SQLquote,chr(34),"''"))
	else
		SQLquote = strValue
	end if
end function

sub CustomerEmail(AmazonID, fName, lName, email, sAddress1, sAddress2, sCity, sState, sZip, sPhone)
	' Send e-mail to Customer
	cdo_from = "Automatic E-Mail from Wirelessemporium.com<amazon@wirelessemporium.com>"
	cdo_subject = "Your Amazon Order: " & AmazonID
	cdo_body = "<p>Dear " & fName & " " & lName & ",</p>" & vbcrlf
	cdo_body = cdo_body & "<p>Thank you for your purchase with Wireless Emporium! We have fulfilled hundreds of thousands of shipments for Amazon.com and are eager to ensure that your buying experience with us is nothing less than stellar! Your order is currently being processed and will be available for shipment within the next business day. You will also receive an email from Amazon confirming shipment of your order by Wireless Emporium.</p>" & vbcrlf
	cdo_body = cdo_body & "<p>At Wireless Emporium, we strive to ensure that our customers are 100% satisfied with their purchases, and make customer service our #1 priority. " & vbcrlf
	cdo_body = cdo_body & "<b><i>So that we may continue to provide the highest level of customer service possible and sustain our stellar service ratings, we ask that if you have any questions, concerns or issues whatsoever with your purchase, PLEASE CONTACT US IMMEDIATELY so that we may promptly address your concerns directly, prior to leaving any merchant feedback through Amazon.</i></b> "
	cdo_body = cdo_body & "Rest assured we will do EVERYTHING WE CAN to ensure your complete satisfaction! We appreciate all of the feedback we have received from our customers throughout the years which has enabled us to be among the top percentile of Amazon Sellers.</p>" & vbcrlf
	cdo_body = cdo_body & "<p>You can reach us directly at:<br>" & vbcrlf
	cdo_body = cdo_body & "1-888-725-7575</p>" & vbcrlf
	cdo_body = cdo_body & "<p>Your order will be shipped to:<br>" & vbcrlf
	cdo_body = cdo_body & fName & " " & lName & "<br>" & vbcrlf
	cdo_body = cdo_body & sAddress1 & "<br>" & vbcrlf
	if trim(sAddress2) <> "" then cdo_body = cdo_body & sAddress2 & "<br>" & vbcrlf
	cdo_body = cdo_body & sCity & ", " & sState & " " & sZip & "<br>" & vbcrlf
	cdo_body = cdo_body & "Phone: " & sPhone & "</p>" & vbcrlf
	cdo_body = cdo_body & "<p>Sincerely,</p>" & vbcrlf
	cdo_body = cdo_body & "<p>Sales Department | Wireless Emporium, Inc.<br>" & vbcrlf
	cdo_body = cdo_body & "1410 N. Batavia St., Orange, CA 92867 <br>" & vbcrlf
	cdo_body = cdo_body & "(P)  888-725-7575    (F)  714- 278-1932<br>" & vbcrlf
	cdo_to = email
	'CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
end sub

sub UpdateInvQty(nPartNumber, nProdQuantity, KIT)
	' Updates inv_qty for the Master Part (item with the same Part Number as the item purchased which does not have -1 inv_qty)
	' If the inv_qty would be adjusted to less than or equal to zero, then all items with the same Part Number as the item purchased are set to zero
	if isNull(KIT) then
		SQL = "SELECT itemID,typeID,vendor,inv_qty FROM we_items WHERE PartNumber = '" & nPartNumber & "' AND master = 1"
	else
		SQL = "SELECT (select itemID from we_items where partNumber = a.partNumber and master = 1) as itemID,typeID,vendor,inv_qty FROM we_items a WHERE itemID IN (" & KIT & ")"
	end if
	'response.write "<li>" & SQL & "</li>" & vbcrlf
	set oRsItemInfo2 = Server.CreateObject("ADODB.Recordset")
	oRsItemInfo2.open SQL, oConn, 3, 3
	do until oRsItemInfo2.eof
		if oRsItemInfo2("inv_qty") - nProdQuantity > 0 then
			On Error Resume Next
			sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & RS2("itemID") & "," & RS2("inv_qty") & "," & nProdQuantity & "," & nOrderID & ",0,'Amazon Customer Order','" & now & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
			On Error Goto 0
			
			decreaseSQL = "UPDATE we_items SET inv_qty = inv_qty - " & nProdQuantity & " WHERE itemID = '" & oRsItemInfo2("itemID") & "'"
			response.write "<li>" & decreaseSQL & "</li>" & vbcrlf
			oConn.execute(decreaseSQL)
		else
			On Error Resume Next
			sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & RS2("itemID") & "," & RS2("inv_qty") & "," & nProdQuantity & "," & nOrderID & ",0,'Amazon Customer Order *Out of Stock*','" & now & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
			On Error Goto 0
			
			decreaseSQL = "UPDATE we_items SET inv_qty = 0 WHERE PartNumber = '" & nPartNumber & "'"
			if oRsItemInfo2("typeID") = 2 then decreaseSQL = decreaseSQL & " AND inv_qty > -1"
			if oRsItemInfo2("typeID") <> 3 and oRsItemInfo2("vendor") <> "CM" and oRsItemInfo2("vendor") <> "DS" and oRsItemInfo2("vendor") <> "MLD" then
				' Send zero-inventory e-mail to Tony
				cdo_from = "Automatic E-Mail from Wirelessemporium.com<sales@wirelessemporium.com>"
				cdo_subject = nPartNumber & " is out of stock! (Amazon upload)"
				cdo_body = nPartNumber & " is out of stock! (Amazon upload)"
				cdo_to = "tony@wirelessemporium.com,charles@wirelessemporium.com,steven@wirelessemporium.com,jon@wirelessemporium.com"
				CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
			end if
			response.write "<li>" & decreaseSQL & "</li>" & vbcrlf
			oConn.execute(decreaseSQL)
		end if
		oRsItemInfo2.movenext
	loop
	oRsItemInfo2.close
	set oRsItemInfo2 = nothing
end sub

sub NumberOfSales(nIdProd, QTY, KIT)
	if isNull(KIT) then
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID = '" & nIdProd & "'"
	else
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID IN (" & KIT & ")"
	end if
	response.write "<li>" & SQL & "</li>" & vbcrlf
	oConn.execute(SQL)
end sub

function stateAbbr(sState)
	if inStr(sState,"DC") > 0 or inStr(sState,"D.C") > 0 then sState = "DC"
	if inStr(sState,"SAMOA") > 0 then sState = "AS"
	if inStr(sState,"MICRONESIA") > 0 then sState = "FM"
	if inStr(sState,"MARIANA") > 0 then sState = "MP"
	if inStr(sState,"VIRGIN") > 0 then sState = "VI"
	select case Lcase(sState)
		case "alabama" : sState = "AL"
		case "alaska" : sState = "AK"
		case "arizona" : sState = "AZ"
		case "arkansas" : sState = "AR"
		case "california" : sState = "CA"
		case "colorado" : sState = "CO"
		case "connecticut" : sState = "CT"
		case "delaware" : sState = "DE"
		case "district of columbia" : sState = "DC"
		case "florida" : sState = "FL"
		case "georgia" : sState = "GA"
		case "hawaii" : sState = "HI"
		case "idaho" : sState = "ID"
		case "illinois" : sState = "IL"
		case "indiana" : sState = "IN"
		case "iowa" : sState = "IA"
		case "kansas" : sState = "KS"
		case "kentucky" : sState = "KY"
		case "louisiana" : sState = "LA"
		case "maine" : sState = "ME"
		case "maryland" : sState = "MD"
		case "massachusetts" : sState = "MA"
		case "michigan" : sState = "MI"
		case "minnesota" : sState = "MN"
		case "mississippi" : sState = "MS"
		case "missouri" : sState = "MO"
		case "montana" : sState = "MT"
		case "nebraska" : sState = "NE"
		case "nevada" : sState = "NV"
		case "new hampshire" : sState = "NH"
		case "new jersey" : sState = "NJ"
		case "new mexico" : sState = "NM"
		case "new york" : sState = "NY"
		case "north carolina" : sState = "NC"
		case "north dakota" : sState = "ND"
		case "ohio" : sState = "OH"
		case "oklahoma" : sState = "OK"
		case "oregon" : sState = "OR"
		case "pennsylvania" : sState = "PA"
		case "rhode island" : sState = "RI"
		case "south carolina" : sState = "SC"
		case "south dakota" : sState = "SD"
		case "tennessee" : sState = "TN"
		case "texas" : sState = "TX"
		case "utah" : sState = "UT"
		case "vermont" : sState = "VT"
		case "virginia" : sState = "VA"
		case "washington" : sState = "WA"
		case "west virginia" : sState = "WV"
		case "wisconsin" : sState = "WI"
		case "wyoming" : sState = "WY"
		case "american samoa" : sState = "AS"
		case "fed. states of micronesia" : sState = "FM"
		case "guam" : sState = "GU"
		case "marshall islands" : sState = "MH"
		case "northern mariana islands" : sState = "MP"
		case "palau" : sState = "PW"
		case "puerto rico" : sState = "PR"
		case "virgin islands" : sState = "VI"
		case "armed forces africa" : sState = "AE"
		case "armed forces americas" : sState = "AA"
		case "armed forces canada" : sState = "AE"
		case "armed forces europe" : sState = "AE"
		case "armed forces middle east" : sState = "AE"
		case "armed forces pacific" : sState = "AP"
	end select
	stateAbbr = sState
end function
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
