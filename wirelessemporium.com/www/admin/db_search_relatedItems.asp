<%
pageTitle = "Admin - Search WE Database - Add/Delete Related Items - Select Brand"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<script type="text/javascript" src="/includes/admin/checkbox.js"></script>
<script>
function onSetMasterCheckBox(frm)
{
	if (isAllChecked(frm.modelID))			frm.chkMaster.checked = true;
	else if (isAllUnChecked(frm.modelID))	frm.chkMaster.checked = false;
}

</script>
<blockquote>
<blockquote>

<font face="Arial,Helvetica">
<%
if request("submitRelatedItemsSearch") = "" then
	%>
	<h3>Select a Brand:</h3>
	<form action="db_search_RelatedItems.asp" method="post" name="frmSearchRelatedItems">
		<%
		SQL = "SELECT * FROM WE_Brands WHERE brandName <> '' AND brandName IS NOT null ORDER BY brandName"
		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		do until RS.eof
			%>
			<input type="radio" name="brandID" value="<%=RS("brandID")%>"><%=RS("brandName")%><br>
			<%
			RS.movenext
		loop
		RS.close
		Set RS = nothing
		%>
		<p><input type="submit" name="submitRelatedItemsSearch" value="Add" onClick="changeAction();">&nbsp;&nbsp;<input type="submit" name="submitRelatedItemsSearch" value="Delete"></p>
	</form>
	<%
else
	strAction = "Search"
	strFormType = "radio"
	brandID = request("brandID")
	if brandID = "" then
		response.redirect("db_search_RelatedItems.asp")
		response.end
	end if
	%>
	<h3>Select a Model:</h3>
	<form action="db_update_RelatedItems.asp" method="post" name="frmSearchRelatedItems">
		<%
		if request("submitRelatedItemsSearch") = "Add" then
			strAction = "Update"
			strFormType = "checkbox"
			%>
			<p>
			<select name="typeID">
				<option value=""></option>
				<%
				SQL = "SELECT * FROM WE_Types WHERE typeName <> '' AND typeName IS NOT null ORDER BY typeName"
				Set RS = Server.CreateObject("ADODB.Recordset")
				RS.open SQL, oConn, 3, 3
				do until RS.eof
					%>
					<option name="modelID" value="<%=RS("typeID")%>"><%=RS("typeName")%></option>
					<%
					RS.movenext
				loop
				RS.close
				Set RS = nothing
				%>
			</select>&nbsp;&nbsp;Category
			</p>
<!--			<p><input type="text" name="itemID" size="5" maxlength="6">&nbsp;&nbsp;itemID</p>-->
				<p>itemID (ex: 11111,22222,33333,44444 or just 11111)<br />
				<textarea name="itemID" cols="60" rows="5"></textarea></p>
				<p><input type="submit" name="submitRelatedItemsSearch" value="<%=strAction%>"></p>
				<p><input value="" type="checkbox" name="chkMaster" onclick="setAllCheckbox(document.frmSearchRelatedItems.modelID, document.frmSearchRelatedItems.chkMaster.checked);"/>Master Checkbox</p>
			<%
		end if
		SQL = "SELECT * FROM WE_Models WHERE brandID='" & brandID & "' ORDER BY modelName"
		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		do until RS.eof
			response.write "<input type=""" & strFormType & """ name=""modelID"" value=""" & RS("modelID") & """ onclick=""onSetMasterCheckBox(document.frmSearchRelatedItems);"">" & RS("modelName") & "<br>" & vbcrlf
			RS.movenext
		loop
		RS.close
		Set RS = nothing
		%>
		<p>
			<input type="hidden" name="brandID" value="<%=brandID%>">
			<input type="submit" name="submitRelatedItemsSearch" value="<%=strAction%>">
		</p>
	</form>
	<%
end if
%>
</font>

</blockquote>
</blockquote>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
