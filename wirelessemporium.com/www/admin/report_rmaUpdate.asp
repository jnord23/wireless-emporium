<%
pageTitle = "RMA Return/Refund Report for Warehouse"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<link href="/includes/css/calendar.css" rel="stylesheet" type="text/css" />
<style>
	.filter-box-header	{ padding:3px; float:left; border-radius:3px; margin-left:28px; text-align:center; font-size:13px; font-weight:bold; background-color:#fff; border:1px solid #e1e1e1; }
	.filter-box			{ padding:0px 3px 3px 3px; float:left; border-radius:3px; margin-left:30px; text-align:center;}
	.table-row			{ width:100%; height:15px; margin:0px; border-radius:3px; padding:5px; }
	.table-cell			{ float:left; padding:3px 1px 3px 1px; display:table-cell; vertical-align:middle;}
</style>
<script type="text/javascript" src="/includes/js/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/includes/js/cal.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function () {
		$('#id_sdate').simpleDatepicker();
		$('#id_edate').simpleDatepicker();
	});
</script>
<%
adminID = session("adminID")
strSDate = prepStr(request.form("txtSDate"))
strEDate = prepStr(request.form("txtEDate"))
updateQTY = prepStr(request.form("updateQTY"))
chkUpdate = prepStr(request.form("chkUpdate"))
siteid = prepStr(request.form("cbSite"))
returnReasonID = prepInt(request.form("cbReturnReason"))
modelid = prepInt(request.form("cbModel"))
typeid = prepInt(request.form("cbType"))
strGroupBy = prepStr(request.form("cbGroupBy"))

if strSDate = "" or not isdate(strSDate) then strSDate = Date-2
if strEDate = "" or not isdate(strEDate) then strEDate = Date

Dim cfgArrGroupBy : cfgArrGroupBy = Array("Date", "Store", "Activity Desc", "Return Reason", "Model", "Category", "ItemID", "Details")
if "" = strGroupBy then strGroupBy = "DETAILS,,,,,,," end if
arrGroupBy = split(strGroupBy, ",")

'========================================================================== Update QTY
if updateQTY <> "" and chkUpdate <> "" then
	sql	=	"update	rmalog" & vbcrlf & _
			"set	backInStock = 1" & vbcrlf & _
			"	,	updateAdminID = '" & adminID & "'" & vbcrlf & _
			"where	id in (" & chkUpdate & ")"
'	response.write "<pre>" & sql & "</pre>"
	session("errorSQL") = sql			
	oConn.execute(sql)

	sql	=	"update	a" & vbcrlf & _
			"set	inv_qty = inv_qty + b.qty" & vbcrlf & _
			"	,	ghost = 0" & vbcrlf & _
			"from	we_items a join (	select	a.partnumber, b.qty" & vbcrlf & _
			"							from	we_items a join (	select	itemid, isnull(sum(qty), 0) qty" & vbcrlf & _
			"														from	rmalog" & vbcrlf & _
			"														where	id in (" & chkUpdate & ")" & vbcrlf & _
			"														group by itemid ) b" & vbcrlf & _
			"								on	a.itemid = b.itemid ) b" & vbcrlf & _
			"	on	a.partnumber = b.partnumber" & vbcrlf & _
			"where	a.master = 1"
'	response.write "<pre>" & sql & "</pre>"
	session("errorSQL") = sql			
	oConn.execute(sql)

	sql	=	"insert into we_invRecord (itemID,inv_qty,adjustQty,adminID,notes,editDate) " & vbcrlf & _
			"select	a.itemid, a.inv_Qty - b.qty, a.inv_Qty, '" & session("adminID") & "', 'Restock from RMA', getdate()" & vbcrlf & _
			"from	we_items a join (	select	a.partnumber, b.qty" & vbcrlf & _
			"							from	we_items a join (	select	itemid, isnull(sum(qty), 0) qty" & vbcrlf & _
			"														from	rmalog" & vbcrlf & _
			"														where	id in (" & chkUpdate & ")" & vbcrlf & _
			"														group by itemid ) b" & vbcrlf & _
			"								on	a.itemid = b.itemid ) b" & vbcrlf & _
			"	on	a.partnumber = b.partnumber" & vbcrlf & _
			"where	a.master = 1"
	session("errorSQL") = sql
	oConn.execute(sql)
	
	'UPDATE we_itemsSiteReady table
	sql = 	"select	distinct a.modelid" & vbcrlf & _
			"from	we_items a join (	select	a.partnumber, b.qty" & vbcrlf & _
			"							from	we_items a join (	select	itemid, isnull(sum(qty), 0) qty" & vbcrlf & _
			"														from	rmalog" & vbcrlf & _
			"														where	id in (" & chkUpdate& ")" & vbcrlf & _
			"														group by itemid ) b" & vbcrlf & _
			"								on	a.itemid = b.itemid ) b" & vbcrlf & _
			"	on	a.partnumber = b.partnumber" & vbcrlf & _
			"where	a.master = 1"
	session("errorSQL") = sql
	set rsSiteReady = oConn.execute(sql)
	if not rsSiteReady.eof then
		do until rsSiteReady.eof
			oConn.execute("sp_createProductListByModelID " & rsSiteReady("modelid"))
			rsSiteReady.movenext
		loop
	end if	
end if
'==========================================================================


dim strSqlWhere
strSqlWhere			=	""
%>
<center>
<form method="post" name="frmReturn">
    <div style="text-align:left; width:800px;">
        <div style="padding-bottom:10px;"><span style="font-size:18px; font-weight:bold;">RMA Return/Refund Report for Warehouse</span></div>
        <!-- filters-->
        <div style="width:100%; border:1px solid #ccc; border-radius:3px; background-color:#f4f7f9; padding:5px;">
            <div style="width:100%; height:30px;">
				<div class="filter-box-header" style="width:60px;">Filter</div>
                <div class="filter-box-header" style="width:180px;">Date</div>
                <div class="filter-box-header" style="width:60px;">Store</div>
                <div class="filter-box-header" style="width:200px;">Action</div>
            </div>
            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%; padding:5px 0px 5px 0px; height:25px;">
				<div class="filter-box-header" style="width:60px;">Value</div>
                <div class="filter-box" style="width:180px;"><input type="text" id="id_sdate" name="txtSDate" value="<%=strSDate%>" size="7"> ~ <input type="text" id="id_edate" name="txtEDate" value="<%=strEDate%>" size="7"></div>
                <div class="filter-box" style="width:60px;">
                	<select name="cbSite">
                    	<option value="">ALL</option>
                    	<option value="0" <%if siteid = "0" then%>selected<%end if%>>WE</option>
                    	<option value="1" <%if siteid = "1" then%>selected<%end if%>>CA</option>
                    	<option value="2" <%if siteid = "2" then%>selected<%end if%>>CO</option>
                    	<option value="3" <%if siteid = "3" then%>selected<%end if%>>PS</option>
                    	<option value="10" <%if siteid = "10" then%>selected<%end if%>>TM</option>
                    </select>
                </div>
                <div class="filter-box" style="width:200px;">
	                <input type="submit" name="search" value="Search" />
	                <input type="submit" name="updateQTY" value="Update QTY" onclick="return checkQTYUpdate();" />
                </div>
            </div>
        </div>
		<!--// filters-->
        <!-- result -->
		<div style="width:100%; padding-top:10px;">
        <%
		strSqlWhere = ""
		if siteid <> "" 		then strSqlWhere = strSqlWhere & "	and	a.site_id = " & siteid & vbcrlf
		if returnReasonID <> 0 	then strSqlWhere = strSqlWhere & "	and	a.returnReason = " & returnReasonID & vbcrlf
		if modelid <> 0			then strSqlWhere = strSqlWhere & "	and	f.modelid = " & modelid & vbcrlf
		if typeid <> 0			then strSqlWhere = strSqlWhere & "	and	e.typeid = " & typeid & vbcrlf
		
		sql	=	"select	a.id, convert(varchar(10), a.logdate, 20) logdate, a.site_id, a.orderid, a.itemid, isnull(a.qty, 0) qty, a.partnumber, b.rma_token lastRmaStatus, a.activity_desc" & vbcrlf & _
				"	,	isnull(a.origItemPrice, 0) origItemPrice, isnull(a.restocking_fee_percent, 0) restocking_fee_percent, isnull(a.itemTax, 0) itemTax" & vbcrlf & _
				"	, 	isnull(a.discountPercent, 0) discountPercent, isnull(a.amountCredited, 0) amountCredited, a.exchangedPartnumber" & vbcrlf & _
				"	,	isnull(a.backInStock, 0) backInStock" & vbcrlf & _
				"	, 	a.confirmAmountCredited, isnull(c.reason, '') returnreason, d.fname userName, g.fname updateUserName" & vbcrlf & _
				"from	rmalog a join xrmatype b" & vbcrlf & _
				"	on	a.rmastatus = b.id left outer join xreturnreason c" & vbcrlf & _
				"	on	a.returnreason = c.id left outer join we_adminUsers d" & vbcrlf & _
				"	on	a.adminID = d.adminID left outer join we_items e" & vbcrlf & _
				"	on	a.itemid = e.itemid left outer join we_models f" & vbcrlf & _
				"	on	e.modelid = f.modelid left outer join we_adminUsers g" & vbcrlf & _
				"	on	a.updateAdminID = g.adminID" & vbcrlf & _
				"where	a.logDate >= '" & strSDate & "'" & vbcrlf & _
				"	and	a.logDate < '" & dateadd("d", 1, strEDate) & "'" & vbcrlf & _
				"	and	a.activity_desc in ('RECEIVED', '(E) / Warranty / Received' , '(E) / Exchange / Received')" & vbcrlf & _
				strSqlWhere & vbcrlf & _
				"order by 1 desc"
'			response.write "<pre>" & sql & "</pre>"
'			response.end
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		if rs.eof then
			response.write "No data to display"
		else
			%>
			<div style="width:100%; height:30px; border:1px solid #c1e0b2; border-radius:3px; background-color:#eaf5e5; padding:5px;">
				<div style="float:left; width:52px; font-size:13px; font-weight:bold;">SITE</div>
				<div style="float:left; width:82px; font-size:13px; font-weight:bold;">Log Date</div>
				<div style="float:left; width:82px; font-size:13px; font-weight:bold;">ORDERID</div>
				<div style="float:left; width:52px; font-size:13px; font-weight:bold;">QTY</div>
				<div style="float:left; width:112px; font-size:13px; font-weight:bold;">PART<br />NUMBER</div>
				<div style="float:left; width:132px; font-size:13px; font-weight:bold;">Admin User</div>
				<div style="float:left; width:132px; font-size:13px; font-weight:bold;">Inv. Update<br />Admin User</div>
                <div style="float:left; width:112px; font-size:13px; font-weight:bold;">Inv. Update<br />Status
                    <input type="checkbox" name="chkMaster" onclick="setAllCheckbox(document.frmReturn.chkUpdate, document.frmReturn.chkMaster.checked);" />
                </div>                
			</div>
			<%
			lap = 0
			nTotalQTY = clng(0)
			do until rs.eof
				nTotalQTY = nTotalQTY + clng(rs("qty"))
				lap = lap + 1			
				select case rs("site_id")
					case 0 : site = "WE"
					case 1 : site = "CA"
					case 2 : site = "CO"
					case 3 : site = "PS"
					case 10 : site = "TM"
				end select

				rowBackgroundColor = "#fff"
				if (lap mod 2) = 0 then rowBackgroundColor = "#eaf5fa"
				%>
			<div class="table-row" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='1px solid <%=rowBackgroundColor%>';this.style.backgroundColor='<%=rowBackgroundColor%>'">
				<div class="table-cell" style="width:50px;"><%=site%>&nbsp;</div>
				<div class="table-cell" style="width:80px;"><%=rs("logdate")%>&nbsp;</div>
				<div class="table-cell" style="width:80px;"><%=rs("orderid")%>&nbsp;</div>
				<div class="table-cell" style="width:50px;"><%=rs("qty")%>&nbsp;</div>
				<div class="table-cell" style="width:110px;"><%=rs("partnumber")%>&nbsp;</div>
				<div class="table-cell" style="width:130px;"><%=rs("userName")%>&nbsp;</div>
				<div class="table-cell" style="width:130px;"><%=rs("updateUserName")%>&nbsp;</div>
                <div class="table-cell" style="width:110px;">
				<%if not rs("backInStock") then%>
                    <input type="checkbox" name="chkUpdate" value="<%=rs("id")%>" />
				<%else%>
                	<span style="color:blue;">Back In Stock</span>
				<%end if%>
                </div>                
			</div>
				<%
				rs.movenext
			loop
			%>
			<div class="table-row" style="margin-top:10px; border-top:2px solid #c1e0b2;" align="right">Total QTY: <%=formatnumber(nTotalQTY,0)%></div>
            <%
		end if
		%>
       	</div>
        <!--// result -->
    </div>
</form>    
</center>
<script type="text/javascript" src="/includes/admin/checkbox.js"></script>
<script>
	function printinvoice(orderid,accountid) {
		var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
		window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	}
	
	function checkQTYUpdate() {
		if(confirm('Inventory Update..\r\nAre you sure to update the selected?')) return true;
		return false;
	}
/*
function onSetMasterCheckBox(frm) {
	if (isAllChecked(frm.chkUpdate))			frm.chkMaster.checked = true;
	else if (isAllUnChecked(frm.chkUpdate))	frm.chkMaster.checked = false;
}
*/
</script>