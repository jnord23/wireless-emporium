<%
	pageTitle = "PS Menu System Update"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	for brandID = 1 to 30
		sql = "select distinct top 10 c.brandName, d.brandOrder, a.modelID, a.modelName, a.brandID, a.releaseYear, a.releaseQuarter from we_models a left join we_items b on a.modelID = b.modelID left join we_brands c on a.brandID = c.brandID left join ps_mainMenu d on a.brandID = d.brandID where b.typeID = 16 and a.brandID = " & brandID & " and b.price_PS is not null and b.price_PS > 0 and b.hideLive = 0 and b.inv_qty > 0 order by a.releaseYear desc, a.releaseQuarter desc"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		sql = "delete from ps_mainMenu where brandID = " & brandID
		session("errorSQL") = sql
		oConn.execute(sql)
		
		if not rs.EOF then			
			lap = 0
			do while not rs.EOF
				lap = lap + 1
				brandName = rs("brandName")
				brandOrder = prepInt(rs("brandOrder"))
				if brandOrder = 0 then brandOrder = 99
				sql = "insert into ps_mainMenu (modelID, modelName, brandID, brandName, brandOrder, modelOrder) values(" & rs("modelID") & ",'" & ds(rs("modelName")) & "'," & brandID & ",'" & ds(brandName) & "'," & brandOrder & "," & lap & ")"
				session("errorSQL") = sql
				oConn.execute(sql)
				
				rs.movenext
			loop
			response.Write(lap & " " & brandName & " phones has been updated<br />")
		end if
	next
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->