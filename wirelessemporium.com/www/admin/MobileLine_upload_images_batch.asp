<%
response.buffer = false
pageTitle = "Admin - Upload MobileLine Images Batch"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
Server.ScriptTimeout = 2400 'seconds

if request.form("submitted") = "Upload New Image Batch" then
	dim path, path_CO, MobileLineFolder, newName, newName_CO, SavePath
	dim filesys, jpeg, demofolder, filecoll, fil
	dim aCount
	aCount = 0
'	path = "D:\inetpub\wwwroot\wirelessemporium.com\productpics\"
'	path_CO = "D:\inetpub\wwwroot\cellularoutfitter.com\www\productpics\"
'	MobileLineFolder = "D:\inetpub\wwwroot\wirelessemporium.com\staging\MobileLine\"
	path = server.MapPath("\productpics") & "\"
	path_CO = server.MapPath("\productpics_co") & "\"
	MobileLineFolder = "c:\inetpub\wwwroot\wirelessemporium.com\staging\MobileLine\"
	set filesys = Server.CreateObject("Scripting.FileSystemObject")
	set jpeg = Server.CreateObject("Persits.Jpeg")
	set demofolder = filesys.GetFolder(MobileLineFolder)
	
	SQL = "SELECT itemID, itemPic, MobileLine_sku FROM we_items WHERE vendor = 'MLD' ORDER BY itemPic"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	do until RS.eof
		set filecoll = demofolder.Files
		for each fil in filecoll
			newName = ""
			if fil.name = replace(RS("itemPic"),"MLD_","") then
				newName = fil.name
				newName_CO = replace(fil.name,"_skin_","_decal_skin_")
				
				if Lcase(right(newName,4)) <> ".jpg" then
					response.write "<h3><font color=""#FF0000"">" & RS("MobileLine_sku") & " : " & RS("itemID") & " : " & newName & " is not a JPG file!</font></h3>" & vbcrlf
					errorCount = errorCount + 1
				else
					for cycleCount = 1 to 4
						select case cycleCount
							case 1
								Subfolder = "big\"
								x = 300
								y = 300
							case 2
								Subfolder = "thumb\"
								x = 100
								y = 100
							case 3
								Subfolder = "homepage65\"
								x = 65
								y = 65
							case 4
								Subfolder = "icon\"
								x = 45
								y = 45
						end select
						
						' Create a "blank" image
						Jpeg.New x, y, &HFFFFFF
						' Draw this image on front page
						Set Img = Server.CreateObject("Persits.Jpeg")
						Img.Open(MobileLineFolder & newName)
						' Resize to inscribe in Width x Height square
						Img.PreserveAspectRatio = True
						If Img.OriginalWidth > Img.OriginalHeight Then
							Img.Width = x
						Else
							Img.Height = y
						End If
						' center image inside frame vert or horiz as needed
						Jpeg.Canvas.DrawImage (x - Img.Width)/2, (y - Img.Height)/2, Img
						Jpeg.Save path & Subfolder & "MLD_" & replace(newName,".jpg","") & ".jpg"
						'Jpeg.Save path_CO & Subfolder & replace(newName_CO,".jpg","") & ".jpg"
						
						'response.write "<p>Saved: " & path & Subfolder & "MLD_" & replace(newName,".jpg","") & ".jpg<br>" & vbcrlf
						'response.write "Saved: " & path_CO & Subfolder & "MLD_" & replace(newName_CO,".jpg","") & ".jpg<br>" & vbcrlf
						'response.write newName & " | " & Img.OriginalWidth & " | " & Img.OriginalHeight & "</p>" & vbcrlf
					next
					aCount = aCount + 1
				end if
				exit for
			end if
		next
		if newName = "" then
			response.write "<h3><font color=""#FF0000"">" & RS("MobileLine_sku") & " : " & RS("itemID") & " : " & replace(RS("itemPic"),"MLD_","") & " not found!</font></h3>" & vbcrlf
			errorCount = errorCount + 1
		end if
		set filecoll = nothing
		RS.movenext
	loop
	
	response.write "<h3>" & aCount & " images uploaded.</h3>"
	response.write "<h3>" & errorCount & " errors.</h3>"
	
	set Img = nothing
	set filesys = nothing
	set jpeg = nothing
	set demofolder = nothing
else
	%>
	<form action="MobileLine_upload_images_batch.asp" method="post">
		<input type="submit" name="submitted" value="Upload New Image Batch">
	</form>
	<%
end if
%>
		
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
