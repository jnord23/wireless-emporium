<%
pageTitle = "Admin - Generate Buy.com Tracking Number Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" width="750" align="center" cellpadding="0" cellspacing="0"><tr><td>
<p>&nbsp;</p>
<p class="biggerText"><%=pageTitle%></p>
<br>

<%
strError = ""

if request("submitted") <> "" then
	StartDate = request.form("dc1")
	if not isDate(StartDate) then
		strError = "Start Date must be a valid date."
	else
		StartDate = dateValue(StartDate)
	end if
	if strError = "" then
		strDates = StartDate & "<br>to<br>" & EndDate
		strCriteria = ""
		SQL = "SELECT A.orderid, A.extOrderNumber, A.trackingNum, C.BUY_sku, B.quantity"
		SQL = SQL & " FROM we_orders A"
		SQL = SQL & " INNER JOIN we_orderdetails B ON A.orderid = B.orderid"
		SQL = SQL & " LEFT OUTER JOIN we_Items C ON B.itemid = C.itemID"
		SQL = SQL & " WHERE A.thub_posted_date >= '" & StartDate & "' AND A.thub_posted_date < '" & dateAdd("D",1,StartDate) & "'"
		SQL = SQL & " AND (A.cancelled IS NULL OR A.cancelled = 0)"
		SQL = SQL & " AND A.extOrderType = 7"
		SQL = SQL & " AND A.store = 0"
		SQL = SQL & " ORDER BY A.orderid"
		'response.write "<p>" & SQL & "</p>"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		
		response.write "<p class=""boldText"">" & RS.recordcount & " records found</p>" & vbcrlf
		response.write "<table border=""1"" width=""100%"" cellpadding=""2"" cellspacing=""0""><tr>" & vbcrlf
		
		if not RS.eof then
			' Save as TXT:
			dim fs, file, filename, path
			set fs = CreateObject("Scripting.FileSystemObject")
			filename = "report_Buy_Tracking.txt"
			path = Server.MapPath("/admin/tempTXT") & "\" & filename
			set file = fs.CreateTextFile(path, true)
			
			strToWrite = "Freceipt-id" & vbtab & "receipt-item-id" & vbtab & "Quantity" & vbtab & "tracking-type" & vbtab & "tracking-number" & vbtab & "Ship-date"
			file.WriteLine strToWrite
			response.write "<td><b>Freceipt-id</b></td>" & vbcrlf
			response.write "<td><b>receipt-item-id</b></td>" & vbcrlf
			response.write "<td><b>Quantity</b></td>" & vbcrlf
			response.write "<td><b>tracking-type</b></td>" & vbcrlf
			response.write "<td><b>tracking-number</b></td>" & vbcrlf
			response.write "<td><b>Ship-date</b></td>" & vbcrlf
			response.write "</tr>" & vbcrlf
			do until RS.eof
				response.write "<tr>" & vbcrlf
				strToWrite = RS("extOrderNumber") & vbtab
				response.write "<td>" & RS("extOrderNumber") & "</td>" & vbcrlf
				strToWrite = strToWrite & vbtab
				response.write "<td>&nbsp;</td>" & vbcrlf
				strToWrite = strToWrite & RS("quantity") & vbtab
				response.write "<td>" & RS("quantity") & "</td>" & vbcrlf
				strToWrite = strToWrite & "3" & vbtab
				response.write "<td>3</td>" & vbcrlf
				strToWrite = strToWrite & RS("trackingNum") & vbtab
				response.write "<td>" & RS("trackingNum") & "</td>" & vbcrlf
				strToWrite = strToWrite & StartDate & vbtab
				response.write "<td>" & StartDate & "</td>" & vbcrlf
				file.WriteLine strToWrite
				RS.movenext
				response.write "</tr>" & vbcrlf
			loop
			response.write "</table><p class=""boldText"">Your TXT file is <a href=""/admin/tempTXT/" & filename & """>here</a>.</p>"
			file.close
			set file = nothing
			set fs = nothing
		else
			response.write "<p class=""boldText"">No records matched.</p><p class=""boldText"">No report generated.</p>"
		end if
		RS.close
		Set RS = nothing
	end if
end if
if request("submitted") = "" or strError <> "" then
	%>
	<p class="normalText"><font color="#FF0000"><b><%=strError%></b></font></p>
	<form action="report_Buy_Tracking.asp" name="frmSalesReport" method="post">
		<p class="normalText">
			<input type="text" name="dc1" value="<%=dateAdd("d",-1,date)%>">&nbsp;&nbsp;Start Date
		</p>
		<p class="normalText">
			<input type="submit" name="submitted" value="Submit">
		</p>
	</form>
	<%
end if
%>

</td></tr></table>

<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
