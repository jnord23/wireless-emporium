<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Product Inventory Adjustment"
	header = 1
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim sql, brandArray
	
	session("orderBy") = null
	
	sql = "select brandID, brandName from we_brands order by brandName"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	sql = "select typeID, typeName from we_types order by typeName"
	session("errorSQL") = sql
	Set catRS = Server.CreateObject("ADODB.Recordset")
	catRS.open sql, oConn, 0, 1
	
	brandArray = ""
	do while not rs.EOF
		brandArray = brandArray & rs("brandID") & "#" & rs("brandName") & "$"
		rs.movenext
	loop
	brandArray = split(brandArray,"$")
%>
<form style="margin:0px;" action="/admin/inventoryAdjust.asp" method="post" name="invForm" onsubmit="return(testForm(this))">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="600">
	<tr>
    	<td><h1 style="margin:0px;" id="topTitle">Product Inventory Adjustment</h1></td>
    </tr>
    <tr>
    	<td style="font-size:10px; padding-bottom:10px;">
        	This application allows the user to quickly look up and adjust product inventory.
        </td>
    </tr>
    <tr>
    	<td style="border-top:1px solid #999; border-bottom:1px solid #999; font-weight:bold; font-size:16px; background-color:#000; color:#FFF; padding-left:10px;">Select Product By Brand/Model</td>
    </tr>
    <tr>
    	<td id="brandBox" style="padding-top:5px;">
        	<div style="width:600px;">
	        	<div style="float:left; width:140px;">Select Brand:</div>
    	        <div style="float:left; padding-left:10px; width:450px;">
	    	    	<select name="brand" onchange="brandSelect(this.value)">
    	    	    	<option value="">Select Brand</option>
        	    	    <%
						for i = 0 to (ubound(brandArray) - 1)
							useArray = split(brandArray(i),"#")
						%>
		                <option value="<%=useArray(0)%>"><%=useArray(1)%></option>
    		            <%
						next
						%>
		            </select>
    	        </div>
            </div>
        </td>
    </tr>
    <tr>
        <td id="modelBox" style="padding-top:5px;"></td>
    </tr>
    <tr>
        <td id="catBox" style="padding-top:5px;"></td>
    </tr>
    <tr>
    	<td style="border-top:1px solid #999; border-bottom:1px solid #999; font-weight:bold; font-size:16px; background-color:#000; color:#FFF; padding-left:10px;">Other Search Methods</td>
    </tr>
    <tr>
    	<td id="brandBox" style="padding-top:5px;">
        	<div style="width:600px; float:left; text-align:right;">
	        	<div style="float:left; width:300px;"><input type="button" name="myAction" value="Pull New Products" onclick="newProds()" style="font-size:12px;" /></div>
            </div>
            <div style="width:600px; float:left; padding-top:5px;">
	        	<div style="float:left; width:140px;">ItemID:</div>
    	        <div style="float:left; padding-left:10px; width:100px;">
	    	    	<input type="text" name="itemID" value="" />
    	        </div>
            </div>
            <div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">Part Number:</div>
    	        <div style="float:left; padding-left:10px; width:100px;">
	    	    	<input type="text" name="partNumber" value="" />
    	        </div>
            </div>
            <div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">Vendor Number:</div>
    	        <div style="float:left; padding-left:10px; width:100px;">
	    	    	<input type="text" name="vendorPartNumber" value="" />
    	        </div>
            </div>
            <div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">Category:</div>
    	        <div style="float:left; padding-left:10px; width:300px;">
	    	    	<select name="catSearch">
                    	<option value=""></option>
						<%
						do while not catRS.EOF
						%>
                        <option value="<%=catRS("typeID")%>"><%=catRS("typeName")%></option>
                        <%
							catRS.movenext
						loop
						%>
                    </select>
    	        </div>
            </div>
            <div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">Keyword:</div>
    	        <div style="float:left; padding-left:10px; width:155px;">
	    	    	<input type="text" name="keyword" value="" />
    	        </div>
                <div style="float:left; width:140px;"><input type="submit" name="searchBttn" value="Search" style="font-size:12px;" /></div>
            </div>
        </td>
    </tr>
    <tr>
    	<td align="right" style="border-top:1px solid #999; border-bottom:1px solid #999; font-weight:bold; font-size:16px; background-color:#000;"><input type="button" name="clearForm" value="Clear Form" onclick="window.location='/admin/inventoryAdjust.asp'" style="font-size:12px;" /></td>
    </tr>
    <tr>
        <td id="resultBox" style="padding-top:5px; z-index:1;"></td>
    </tr>
</table>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function brandSelect(brandID) {
		ajax('/ajax/admin/ajaxInventoryAdjust.asp?brandID=' + brandID,'modelBox')
		document.getElementById("catBox").innerHTML = ""
		document.getElementById("resultBox").innerHTML = ""
	}
	function modelSelect(modelID) {
		ajax('/ajax/admin/ajaxInventoryAdjust.asp?modelID=' + modelID,'catBox')
		document.getElementById("resultBox").innerHTML = ""
	}
	function catSelect(catID,modelID) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxInventoryAdjust.asp?modelID=' + modelID + '&catID=' + catID,'resultBox')
	}
	function resultOrder(catID,modelID,orderBy) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxInventoryAdjust.asp?modelID=' + modelID + '&catID=' + catID + '&orderBy=' + orderBy,'resultBox')
	}
	function newProds() {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxInventoryAdjust.asp?newProds=1','resultBox')
	}
	function updateInv(itemID,invQty,curInvQty) {
		ajax('/ajax/admin/ajaxInventoryAdjust.asp?updateItemID=' + itemID + '&updateInvQty=' + invQty + '&curInvQty=' + curInvQty,'dumpZone')
		setTimeout("showResult(" + itemID + ")",1000)
	}
	function showResult(itemID) {
		var saveHTML = document.getElementById(itemID + '_return').innerHTML
		document.getElementById(itemID + '_return').innerHTML = document.getElementById('dumpZone').innerHTML
		setTimeout("document.getElementById('" + itemID + "_return').innerHTML = '" + saveHTML + "'",1000)
	}
	function displayInvHistory(itemID) {
		if (document.getElementById("invHistory_" + itemID).innerHTML == "") {
			ajax('/ajax/admin/ajaxInventoryAdjust.asp?showHistory=' + itemID,'invHistory_' + itemID)
			setTimeout("historyDataCheck(" + itemID + ")",500)
		}
		else {
			document.getElementById("invHistory_" + itemID).innerHTML = ""
		}
	}
	
	var dataCheck = 0;
	function historyDataCheck(cell) {
		dataCheck++;
		var objDiv = document.getElementById("scrollBox_" + cell);
		if (objDiv.innerHTML == "" && dataCheck < 50) {
			setTimeout("historyDataCheck('" + cell + "')",500)
		}
		else if (objDiv.innerHTML != "") {			
			objDiv.scrollTop = objDiv.scrollHeight;
		}
	}
	
	function testForm(curForm) {
		var itemID = document.invForm.itemID.value
		var partNumber = document.invForm.partNumber.value
		var category = document.invForm.catSearch.value
		var keyword = document.invForm.keyword.value
		var vendorPartNumber = document.invForm.vendorPartNumber.value
		
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxInventoryAdjust.asp?itemID=' + itemID + '&partNumber=' + partNumber + '&vendorPartNumber=' + vendorPartNumber + '&categoryID=' + category + '&keyword=' + keyword,'resultBox')
		return false
	}
	function permStock(partNumber,checked) {
		ajax("/ajax/admin/ajaxInventoryAdjust.asp?aisPN=" + partNumber + "&aisChecked=" + checked,'dumpZone')
		if (checked == true) {
			alert("Product is now always in stock")
		}
		else {
			alert("Product will use actual stock")
		}
	}
	
	function cycleCount(partnumber) {
		ajax('/ajax/admin/ajaxInventoryAdjust.asp?cycleCnt=' + partnumber,'dumpZone')
		alert("cycle count saved for: " + partnumber)
	}
	
	function addHousing(houseID,partNumber) {
		ajax('/ajax/addHousing.asp?partNumber=' + partNumber,'dumpZone')
		document.getElementById("houseID_" + houseID).src = "/images/icons/houseIcon.png"
		alert("Housing added to " + partNumber)
	}
	
	function purchHide(partNumber,dir,itemID) {
		if (dir == 0) {
			document.getElementById("purchVis_" + itemID).innerHTML = "<a href=javascript:ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?hidePN=" + partNumber + "&dir=1','dumpZone') onclick=purchHide('" + partNumber + "',1," + itemID + ") style=color:#00F;>Hide Product</a>"
		}
		else {
			document.getElementById("purchVis_" + itemID).innerHTML = "<a href=javascript:ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?hidePN=" + partNumber + "&dir=0','dumpZone') onclick=purchHide('" + partNumber + "',0," + itemID + ") style=color:#00F;>Unhide Product</a>"
		}
	}
</script>