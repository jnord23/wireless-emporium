<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
processPDF = prepStr(request.QueryString("processPDF"))
filename = processPDF & "_" & Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now) & ".xls"

Response.ContentType = "application/vnd.ms-excel"
'Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
Response.AddHeader "Content-Disposition","attachment;filename="""&filename&""""
'Response.Charset = "" 
'Me.EnableViewState = False

response.write processPDF & vbNewLine

sql	=	"select	a.processPDF, b.partnumber, sum(b.quantity) quantity" & vbcrlf & _
		"from	we_orders a join we_orderdetails b" & vbcrlf & _
		"	on	a.orderid = b.orderid" & vbcrlf & _
		"where	a.processPDF = '" & processPDF & "'" & vbcrlf & _
		"group by a.processPDF, b.partnumber" & vbcrlf & _
		"order by 1, 3 desc, 2"

set rs = oConn.execute(sql)
if rs.eof then
	response.write "No data to display"
else
	response.write "Partnumber" & vbTab
	response.write "Quantity" & vbNewLine

	do until rs.eof
		response.write rs("partnumber") & vbTab
		response.write rs("quantity") & vbNewLine
		rs.movenext
	loop
end if
%>