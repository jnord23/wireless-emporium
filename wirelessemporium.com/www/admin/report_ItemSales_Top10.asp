<%
pageTitle = "Top 10 Item Sales Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">

<%
Server.ScriptTimeout = 2000 'seconds

if request.form("submitted") <> "" then
	dim strError, dateStart, dateEnd, TypeID
	strError = ""
	dateStart = request.form("dateStart")
	dateEnd = request.form("dateEnd")
	TypeID = request.form("TypeID")
	PartNumber = request.form("PartNumber")
	
	if not isDate(dateStart) or not isDate(dateEnd) then
		strError = strError & "<p>dateStart and dateEnd must both be valid dates</p>"
	else
		dateStart = dateValue(dateStart)
		dateEnd = dateValue(dateEnd)
		if dateEnd < dateStart then strError = strError & "<p>dateEnd must be later than or equal to dateStart</p>"
	end if
	
	showblank = "&nbsp;"
	shownull = "-null-"
	
	if strError = "" then
		dim store
		select case request.form("store")
			case "0" : store = "WE"
			case "1" : store = "CA"
			case "2" : store = "CO"
			case "3" : store = "PS"
			case else : store = "ALL"
		end select
		response.write "<p class=""biggerText"">" & pageTitle & " for Date Range:<br>" & dateStart & " &#150; " & dateEnd & "</p>" & vbcrlf
		response.write "<h3>Store: " & store & "</h3>" & vbcrlf
		

		sql	=	"select	top 10 c.itemid, c.itemdesc, c.price_our, sum(b.quantity) quantity" & vbcrlf & _
				"from	we_orders a join we_orderdetails b" & vbcrlf & _
				"	on	a.orderid = b.orderid join we_items c" & vbcrlf & _
				"	on	b.itemid = c.itemid" & vbcrlf & _
				"where	a.orderdatetime >= '" & dateStart & "' and a.orderdatetime < '" & dateAdd("d",1,dateEnd) & "'" & vbcrlf & _
				"	and	a.approved = 1 and (a.cancelled is null or a.cancelled = 0)" & vbcrlf
				
		if request.form("store") <> "" then SQL = SQL & " and a.store = '" & request.form("store") & "'" & vbcrlf
		if TypeID <> "" then
			TypeInfo = split(TypeID,",")
			TypeID = TypeInfo(0)
			NameOfType = TypeInfo(1)
			response.write "<p class=""biggerText"">Category: " & NameOfType & "</p>" & vbcrlf
		end if
		if TypeID <> "" then SQL = SQL & " and c.typeID = '" & TypeID & "'" & vbcrlf
		if instr(PartNumber,"%") > 0 then
			SQL = SQL & " and c.PartNumber LIKE '" & PartNumber & "'" & vbcrlf
		elseif PartNumber <> "" then
			SQL = SQL & " and c.PartNumber = '" & PartNumber & "'" & vbcrlf
		end if
		SQL = SQL & "group by c.itemid, c.itemdesc, c.price_our" & vbcrlf & _
					"order by 4 desc" & vbcrlf
			
		set RS = Server.CreateObject("ADODB.Recordset")

'		response.write "<pre>" & SQL & "</pre>"		
		RS.open SQL, oConn, 3, 1
		response.write "<p><a href=""/admin/report_ItemSales_Top10.asp"">Back to Report Criteria</a></p>" & vbcrlf
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			%>
            <table border="1" bordercolor="#000000" cellpadding="2" cellspacing="0" width="100%">
				<tr style="background-color:#333; font-weight:bold; color:#FFF">
					<td>itemID</td>
					<td>quantity</td>
					<td>price_Our</td>
					<td>total</td>
					<td nowrap="nowrap">itemDesc (Click to view product page)</td>
				</tr>
            <%
			bgColor = "#ffffff"
			do while not rs.EOF
			%>
            	<tr style="background-color:<%=bgColor%>" onMouseOver="this.style.backgroundColor='#9999ff';" onMouseOut="this.style.backgroundColor='<%=bgColor%>';">
                	<td align="left"><%=rs("itemID")%></td>
                    <td align="center"><%=rs("quantity")%></td>
                    <td align="center"><%=formatcurrency(rs("price_our"),2)%></td>
                    <td align="center"><%=formatcurrency((rs("price_our") * rs("quantity")),2)%></td>
                    <td><a href="/p-<%=rs("itemID")%>-<%=formatSEO(rs("itemDesc"))%>.asp"><%=rs("itemDesc")%></a></td>
                </tr>
            <%
				if bgColor = "#ffffff" then bgColor = "#cccccc" else bgColor = "#ffffff"
				rs.movenext
			loop
			%>
            </table>
			<br>
			<p><a href="/admin/report_ItemSales_Top10.asp">Back to Report Criteria</a></p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<%
		end if
		
		RS.close
		set RS = nothing
	else
		%>
		<center>
		<font size="+2" color="red"><%=strError%></font>
		<a href="javascript:history.back();">BACK</a>
		</center>
		<%
	end if
else
	%>
	<form action="report_ItemSales_Top10.asp" method="post">
		<p><input type="text" name="dateStart" value="<%=date - 1%>">&nbsp;&nbsp;dateStart</p>
		<p><input type="text" name="dateEnd" value="<%=date%>">&nbsp;&nbsp;dateEnd</p>
		<h3>Select a Type:</h3>
		<p>
			<select name="TypeID">
				<option value=""></option>
				<%
				SQL = "SELECT * FROM WE_Types ORDER BY TypeName"
				set RS = Server.CreateObject("ADODB.Recordset")
				RS.open SQL, oConn, 3, 3
				do until RS.eof
					%>
					<option value="<%=RS("TypeID") & "," & RS("TypeName")%>"><%=RS("TypeName")%></option>
					<%
					RS.movenext
				loop
				RS.close
				set RS = nothing
				%>
			</select>
		</p>
		<p>
			<input type="text" name="PartNumber">&nbsp;&nbsp;Part #
			<br>[For wildcard searches, add "%" to the end of the partial Part Number. examples: SCR-% or SCR-SAM%]
		</p>
		<h3>Store:</h3>
		<p>
			<select name="store">
				<option value="" selected>ALL</option>
				<option value="0">WE</option>
				<option value="1">CA</option>
				<option value="2">CO</option>
				<option value="3">PS</option>
			</select>
		</p>
		<p><input type="submit" name="submitted" value="Generate Report"></p>
	</form>
	<%
end if
%>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
