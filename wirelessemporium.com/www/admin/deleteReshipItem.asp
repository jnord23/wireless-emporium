<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/admin/formatNotes.asp"-->
<%
	accountID 		= 	request.QueryString("accountid")
	orderID			= 	request.QueryString("orderId")
	reshipOrderID	= 	request.QueryString("reshipOrderID")
	orderDetailID	=	request.QueryString("orderDetailID")
	partNumber		=	request.QueryString("partNumber")
	delQty			=	request.QueryString("delQty")
	masterItemID	=	-1
	curQty = 0
	vendor = ""

'	sql	=	"select itemid, inv_qty, case when partnumber like '%HYP%' then 'HYP' else vendor end vendor from we_items where partnumber = '" & partNumber & "' and master = 1 and hidelive = 0 and inv_qty <> 0"
	sql	=	"select itemid, inv_qty, case when partnumber like '%HYP%' then 'HYP' else vendor end vendor from we_items where partnumber = '" & partNumber & "' and master = 1 and hidelive = 0"
	session("errorSQL") = sql
	set objRsMasterItem = oConn.execute(sql)

	if not objRsMasterItem.eof then
		masterItemID 	= 	objRsMasterItem("itemid")
		vendor			=	objRsMasterItem("vendor")
		curQty			=	objRsMasterItem("inv_qty")
	else
		sql = "select id itemid, 'MS' vendor from we_items_musicskins where musicSkinsID = '" & partNumber & "'"
		session("errorSQL") = sql
		
		set objRsMusicSkins = oConn.execute(sql)
		
		if not objRsMusicSkins.eof then
			masterItemID 	= 	objRsMusicSkins("itemid")
			vendor			=	objRsMusicSkins("vendor")
		else
			response.redirect "/admin/view_invoice.asp?retCustMsg=Delete failed: Could not find master item&orderid=" & orderID & "&accountid=" & accountid
			response.End
		end if
		objRsMusicSkins = null
	end if
	objRsMasterItem = null

	if not isnumeric(delQty) then
		response.redirect "/admin/view_invoice.asp?retCustMsg=Delete failed: Qty is not numeric&orderid=" & orderID & "&accountid=" & accountid
		response.End
	end if

		
	sql	=	"	select	id, orderid, convert(varchar(8000), ordernotes) ordernotes	" & vbcrlf & _
			"	from	we_ordernotes" & vbcrlf & _
			"	where	orderid = '" & orderID & "'" & vbcrlf
			
	set objRsNotes = oConn.execute(sql)
	
	if not objRsNotes.EOF then
		curOrderNotes 	= 	objRsNotes("ordernotes")
		strMsg			=	"A reship item(" & partNumber & ") deleted for reship order id " & reshipOrderID
		AddNotes = SQLquote(curOrderNotes) & vbcrlf & formatNotes(strMsg)

		sql = 	"	update	we_ordernotes	" & vbcrlf & _
				"	set		ordernotes = '" & AddNotes & "'" & vbcrlf & _
				"	where	orderid = '" & orderID & "'" & vbcrlf		

		session("errorSQL") = sql		
		oConn.execute(sql)
	else
		strMsg		=	"A reship item(" & partNumber & ") deleted for reship order id " & reshipOrderID
		AddNotes 	= 	formatNotes(strMsg)
		sql 		= 	"	insert into we_ordernotes(orderid, ordernotes) values('" & orderID & "', '" & AddNotes & "') "
		session("errorSQL") = sql		
		oConn.execute(sql)
	end if
	objRsNotes = null

	adminID = session("adminID")
	if adminID = "" then
		adminID = 0
	end if
	
	if instr("CM,DS,MLD,HYP,MS", vendor) <= 0 then
		sql = 	"insert into we_invRecord(itemID,inv_qty,adjustQty,orderID,adminID,notes)" & vbcrlf & _
				"values('" & masterItemID & "', '" & curQty & "', '" & delQty & "', '" & reshipOrderID  & "', '" & adminID & "', 'Reship Delete')"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = 	"update	we_items" & vbcrlf & _
				"set	inv_qty = inv_qty + " & delQty & vbcrlf & _
				"where	itemid = '" & masterItemID & "'" & vbcrlf
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	sql = 	"	delete	" & vbcrlf & _
			"	from	we_orderdetails" & vbcrlf & _
			"	where	orderdetailid = '" & orderDetailID & "'" & vbcrlf
			
	session("errorSQL") = sql
	oConn.execute(sql)
	
'	sql	=	"	select orderid from we_orderdetails where orderid = '" & reshipOrderID & "'"
'	session("errorSQL") = sql
'	set objRsOrder = oConn.execute(sql)
'	
'	if objRsOrder.eof then
'		sql	=	"delete from we_orders where orderid = '" & reshipOrderID & "'"
'		session("errorSQL") = sql
'		oConn.execute(sql)		
'	end if
'	objRsOrder = null
	
	response.redirect "/admin/view_invoice.asp?orderid=" & orderID & "&accountid=" & accountid
%>