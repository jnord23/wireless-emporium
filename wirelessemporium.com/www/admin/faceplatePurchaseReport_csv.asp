<%
	response.buffer = true
	response.expires = -1
	response.ExpiresAbsolute = Now() - 1
	response.CacheControl = "no-cache"

	pageTitle = "Product Purchase Report - Create CSV"
	Server.ScriptTimeout = 9000
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	if prepStr(session("partList")) <> "" then
		partList = left(session("partList"),len(session("partList"))-1)
		
		sql =	"select a.partNumber, a.itemDesc, a.inv_qty, b.sale30, c.sale60, d.sale90, e.entryDate as orderDate, e.orderAmt, a.cogs, f.vendorList, isnull(g.hidden,0) as hidden, h.complete, h.process, i.qtySoldPerDayAvailable, i.qtySoldPer30DayAvailable " &_
				"from we_Items a " &_
					"left join ( " &_
						"select ia.partNumber, SUM(ia.quantity) as sale30 " &_
						"from we_orderdetails ia " &_
							"left join we_orders ib on ia.orderid = ib.orderid " &_
						"where ib.orderdatetime > DATEADD(d,-30,getdate()) and ib.approved = 1 and ib.cancelled is null and ib.parentOrderID is null " &_
						"group by ia.partNumber " &_
					") b on b.partNumber = a.partNumber " &_
					"left join ( " &_
						"select ia.partNumber, SUM(ia.quantity) as sale60 " &_
						"from we_orderdetails ia " &_
							"left join we_orders ib on ia.orderid = ib.orderid " &_
						"where ib.orderdatetime > DATEADD(d,-60,getdate()) and ib.approved = 1 and ib.cancelled is null and ib.parentOrderID is null " &_
						"group by ia.partNumber " &_
					") c on c.partNumber = a.partNumber " &_
					"left join ( " &_
						"select ia.partNumber, SUM(ia.quantity) as sale90 " &_
						"from we_orderdetails ia " &_
							"left join we_orders ib on ia.orderid = ib.orderid " &_
						"where ib.orderdatetime > DATEADD(d,-90,getdate()) and ib.approved = 1 and ib.cancelled is null and ib.parentOrderID is null " &_
						"group by ia.partNumber " &_
					") d on d.partNumber = a.partNumber " &_
					"outer apply ( " &_
						"select top 1 partNumber, entryDate, orderAmt " &_
						"from we_vendorOrder " &_
						"where partNumber = a.partNumber " &_
						"order by id desc " &_
					") e " &_
					"outer apply ( " &_
						"select stuff((select ',' + vendor as [text()] " &_
						"from we_vendorNumbers ia WITH(NOLOCK) " &_
						"where ia.we_partNumber = a.partNumber for xml path('')),1,1,'') as vendorList " &_
					") f " &_
					"left join we_itemsExtendedData g on a.PartNumber = g.partNumber " &_
					"left join we_vendorOrder h on a.PartNumber = h.partNumber and h.complete = 0 " &_
					"join we_pnDetails i on a.PartNumber = i.partnumber " &_
				"where a.partNumber in (" & partList & ") and a.master = 1 " &_
				"order by a.partNumber"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		if rs.EOF then
			response.Write("total bummer brah!<br><br>")
		else
			dim fs, file, filename, path
			set fs = CreateObject("Scripting.FileSystemObject")
			if fs.fileExists(Server.MapPath("/admin/tempCSV") & "\purchaseReport.csv") then
				fs.deleteFile(Server.MapPath("/admin/tempCSV") & "\purchaseReport.csv")
			end if
			filename = "purchaseReport.csv"
			path = Server.MapPath("/admin/tempCSV") & "\" & filename
			set file = fs.CreateTextFile(path, true)
			
			dim strToWrite, HoldPartNumber, WEsales, CAsales, COsales, PSsales
			strToWrite = ""
			
			strToWrite = strToWrite & chr(34) & "PartNumber" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "itemDesc" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "inv_qty" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "Sale30" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "Sale60" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "Sale90" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "LastOrder" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "OrderAmt" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "Cogs" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "Vendors" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "Status" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "AutoOrder" & chr(34)
			file.WriteLine strToWrite
			
			laps = 0
			do while not rs.EOF
				laps = laps + 1
				strToWrite = ""
				
				curPartNumber = rs("partNumber")
				curItemDesc = replace(RS("itemDesc"),",","-")
				curInvQty = prepInt(RS("inv_qty"))
				sale30 = prepInt(rs("sale30"))
				sale60 = prepInt(rs("sale60"))
				sale90 = prepInt(rs("sale90"))
				orderDate = rs("orderDate")
				orderAmt = prepInt(rs("orderAmt"))
				cogs = prepInt(rs("cogs"))
				curVendors = RS("vendorList")
				irsComplete = RS("complete")
				irsProcess = RS("process")
				hidden = RS("hidden")
				qtySold = RS("qtySoldPer30DayAvailable")
				qtySold = cdbl(qtySold)
				thirtyDays = qtySold * 30
				autoOrder = thirtyDays - curInvQty
				if autoOrder < 0 then autoOrder = 0
				orderStatus = "Open"
				
				if isnull(irsProcess) then irsProcess = true
				if not irsProcess then
					orderStatus = "On Order"
				elseif irsProcess and not irsComplete then
					orderStatus = "Processed"
				elseif isnull(irsVendorNumber) then
					orderStatus = "No Vendor"
				elseif prepInt(irsBackOrders) > 0 then
					orderStatus = "back ordered"
				end if
				
				strToWrite = strToWrite & chr(34) & curPartNumber & chr(34) & ","
				strToWrite = strToWrite & chr(34) & curItemDesc & chr(34) & ","
				strToWrite = strToWrite & chr(34) & curInvQty & chr(34) & ","
				strToWrite = strToWrite & chr(34) & sale30 & chr(34) & ","
				strToWrite = strToWrite & chr(34) & sale60 & chr(34) & ","
				strToWrite = strToWrite & chr(34) & sale90 & chr(34) & ","
				strToWrite = strToWrite & chr(34) & orderDate & chr(34) & ","
				strToWrite = strToWrite & chr(34) & orderAmt & chr(34) & ","
				strToWrite = strToWrite & chr(34) & cogs & chr(34) & ","
				strToWrite = strToWrite & chr(34) & curVendors & chr(34) & ","
				strToWrite = strToWrite & chr(34) & orderStatus & chr(34) & ","
				strToWrite = strToWrite & chr(34) & autoOrder & chr(34)
				
				if not hidden then
					file.WriteLine strToWrite
				end if
				rs.movenext
				if laps = 100000 then exit do
			loop
			
			file.close
			set file = nothing
			set fs = nothing
			response.write "<p class=""boldText"">Your CSV file is <a href=""/admin/tempCSV/" & filename & """>here</a>.</p>"
		end if
	else
		response.Write("You will get nothing and like it:" & session("partList"))
		response.End()
	end if
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
