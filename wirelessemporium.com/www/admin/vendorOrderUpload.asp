<%
	thisUser = Request.Cookies("username")
	pageTitle = ""
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	set fso = CreateObject("Scripting.FileSystemObject")
	Set Upload = Server.CreateObject("Persits.Upload")
	
	Upload.IgnoreNoPost = true
	Count = Upload.Save(server.MapPath("/tempCSV/"))
	
	dim orderID : orderID = prepStr(Upload.form("orderID"))
	
	if prepInt(Upload.form("uploadCsv")) = 1 then		
		set orderAdjCsv = Upload.files("FILE1")
		orderAdjCsv_fileName = orderAdjCsv.filename
		fileLoc = orderAdjCsv.path
		
		set MyFile = fso.OpenTextFile(fileLoc,1)
		dim prodLap : prodLap = 0
		dim prodUpdate : prodUpdate = 0
		
		Do While MyFile.AtEndOfStream <> True
			prodLap = prodLap + 1
			curLine = MyFile.ReadLine
			curArray = split(curLine,",")
			if prodLap > 1 then
				vendorPN = prepStr(curArray(0))
				qty = prepInt(curArray(1))
				
				if vendorPN <> "" then
					sql = "select itemID, partNumber, inv_qty, (select rowProcessed from we_vendorOrder where orderID = '" & orderID & "' and partNumber = a.partNumber) as rowProcessed from we_Items a where master = 1 and PartNumber = (select top 1 we_partNumber from we_vendorNumbers where partNumber = '" & vendorPN & "' order by id desc)"
					session("errorSQL") = sql
					set rs = oConn.execute(sql)
					
					if rs.EOF then
						response.Write("Missing Vendor Number:" & vendorPN & "<br><br>")
					else
						curItemID = prepInt(rs("itemID"))
						curPartNumber = prepStr(rs("partNumber"))
						curQty = prepInt(rs("inv_qty"))
						rowProcessed = rs("rowProcessed")
						
						if rowProcessed then
							response.Write("PartNumber already received:" & curPartNumber & "<br><br>")
						else
							sql =	"update we_vendorOrder " &_
									"set rowProcessed = 1, complete = 1, completeOrderDate = GETDATE(), receivedAmt = " & qty & " " &_
									"where orderID = '" & orderID & "' and partNumber = '" & curPartNumber & "'"
							session("errorSQL") = sql
							oConn.execute(sql)
								
							if qty > 0 then
								prodUpdate = prodUpdate + 1
								
								sql = "update we_Items set inv_qty = " & curQty + qty & " where master = 1 and PartNumber = '" & curPartNumber & "'"
								session("errorSQL") = sql
								oConn.execute(sql)
								
								sql = "insert into we_invRecord (itemID,inv_qty,adjustQty,adminID,notes,editDate) values(" & curItemID & "," & curQty & "," & qty & "," & session("adminID") & ",'Vendor Order - CSV Upload','" & now & "')"
								session("errorSQL") = sql
								oConn.execute(sql)
							end if
						end if
					end if
				end if
			end if
			response.Flush()
		loop
		
		response.Write("upload complete(" & prodUpdate & "/" & prodLap & ")!")
		response.End()
	else
		response.Write("formVal:" & prepInt(Upload.form("uploadCsv")) & "<br>")
	end if
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->