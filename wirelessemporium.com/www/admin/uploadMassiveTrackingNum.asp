<%
pageTitle = "Tracking Number Upload By Excel Spreadsheet"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<center>
<%
path = server.MapPath("tempcsv")
set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
myCount = Upload.Save(path)
%>
<form name="frmTracking" enctype="multipart/form-data" action="uploadMassiveTrackingNum.asp" method="POST">
    <div style="text-align:left; width:800px;">
        <div style="padding-bottom:10px; height:20px;">
        	<div style="font-size:18px; font-weight:bold; float:left;">Tracking Number Upload By Excel Spreadsheet</div>
		</div>
        <div style="width:100%; border:1px solid #ccc; border-radius:3px; background-color:#f4f7f9; padding:5px;">
            <div style="width:100%; height:30px;">
                <div class="filter-box-header" style="width:200px;">Shiptype</div>
                <div class="filter-box-header" style="width:300px;">Select File To Upload</div>
                <div class="filter-box-header" style="width:200px;">Action</div>
            </div>
            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%; padding:5px 0px 5px 0px; height:25px;">
                <div class="filter-box" style="width:200px;"><select name="cbShiptype"><option value="ontrac">OnTrac</option></select></div>
                <div class="filter-box" style="width:300px;"><input type="FILE" name="FILE1"></div>
                <div class="filter-box" style="width:200px;"><input type="SUBMIT" name="upload" value="Upload"></div>
            </div>
        </div>
    </div>
</form>    
<%
if Upload.form("upload") = "Upload" and myCount > 0 then
	set myFiles = Upload.files("FILE1")
	if myCount > 0 then
		for each file in Upload.files
			set txtConn = Server.CreateObject("ADODB.Connection")
			myStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & file.path & ";Extended Properties=""Excel 12.0;HDR=YES"";"
			session("errorSQL") = myStr
			txtConn.ConnectionString = myStr
			txtConn.Open
			
			sql = "select [Order Number] as orderid, [Tracking Number] as trackingnum from [Sheet1$]"	
			set rs = Server.CreateObject("ADODB.Recordset")
			rs.open sql, txtConn, 0, 1
			
			lap = 0
			if rs.eof then
				response.write "<h3 style=""color:red;"">No data is available</h3>"
			else
				do until rs.eof
					orderid = rs("orderid")
					trackingNum = rs("trackingnum")
					if (not isnull(trackingNum) and trackingNum <> "") and (not isnull(orderid) and orderid <> "") then
						lap = lap + 1
						trackingNum = replace(replace(replace(replace(replace(replace(replace(trim(rs("trackingnum")), chr(160), ""), chr(32), ""), chr(10), ""), chr(13), ""), chr(39), ""), chr(34), ""), "'", "''")
						
						oConn.execute("update we_orders set trackingnum = '" & trackingNum & "' where orderid = '" & orderid & "'")
					
						response.write "ORDERID: " & orderid & " &nbsp; &nbsp; &nbsp; trackingNum: " & trackingNum & "<br>"
					end if
					rs.movenext
				loop
			end if
		next
		response.write "<h3 style=""color:blue;"">" & lap & " orders updated!</h3>"
	else
		response.write "<h3 style=""color:red;"">No File Selected</h3>"
	end if
end if
%>
</center>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->