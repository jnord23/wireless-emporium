<%
pageTitle = "Update model names based on browser data "
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<script type="text/javascript" src="/includes/js/jquery/jquery-1.7.1.min.js"></script>
<%
txtRows = prepInt(request.form("txtRows"))
if txtRows = 0 then txtRows = 500

cbView = prepInt(request.form("cbView"))
cbPhoneType = prepInt(request.form("cbPhoneType"))

chkBot = false
if prepInt(request.form("chkBot")) = 1 then chkBot = true

dim strSqlSelectMain, strSqlWhere, strSqlGroupBy, strSqlOrderBy, strHeadings
strSqlSelectMain	=	""
strSqlWhere			=	""
strSqlGroupBy		=	""
strSqlOrderBy		=	""
strHeadings			=	""
%>
<center>
<form method="post" name="frm">
    <div style="text-align:left; width:1200px;">
        <div style="padding-bottom:10px; height:20px;">
        	<div style="font-size:18px; font-weight:bold; float:left;">Mobile Browsing Data<br></div>
		</div>
		<div style="width:500px; border:1px solid #ccc; border-radius:3px; background-color:#f4f7f9; padding:5px;">
            <div style="width:100%; height:30px;">
				<div class="filter-box-header" style="width:50px;">Filter</div>
                <div class="filter-box-header" style="width:100px;">Rows</div>
                <div class="filter-box-header" style="width:100px;">View</div>
                <div class="filter-box-header" style="width:130px;">Action</div>
            </div>
            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%; padding:5px 0px 5px 0px; height:25px;">
				<div class="filter-box-header" style="width:50px;">Value</div>
                <div class="filter-box" style="width:100px;"><input type="text" name="txtRows" value="<%=txtRows%>" style="width:90px;" /></div>
                <div class="filter-box" style="width:100px;">
                	<select name="cbView">
                    	<option value="0" <%if cbView = 0 then%>selected<%end if%>>No Bots</option>
                    	<option value="1" <%if cbView = 1 then%>selected<%end if%>>Bots only</option>
                    	<option value="2" <%if cbView = 2 then%>selected<%end if%>>View All</option>
                    </select>
                </div>
                <div class="filter-box" style="width:130px;">
	                <input type="submit" name="search" value="Search" />
                </div>
            </div>
        </div>        
        <!-- result -->
		<div style="width:100%; padding-top:10px;">
        <%
		strSqlWhere = 	""
		if cbView = 0 then		
			strSqlWhere = strSqlWhere & " and a.isBot = 0" & vbcrlf
		elseif cbView = 1 then
			strSqlWhere = strSqlWhere & " and a.isBot = 1" & vbcrlf
		end if
		if cbPhoneType > 0 then
			strSqlWhere = strSqlWhere & " and d.id = " & cbPhoneType & vbcrlf
		end if
		
		strSqlOrderBy = "order by views desc"
		sql	=	"select	top (" & txtRows & ") a.id, a.browserData, a.brandID, a.modelID, a.visitors, a.views, convert(varchar(10), a.lastHit, 20) lastHit, a.isBot, a.modelNumber, isnull(b.brandname, '') brandname, isnull(c.modelname, '') modelname, isnull(d.phoneTypeName, 'unknown') phoneTypeName" & vbcrlf & _
				"from	mobileBrowsers a with (nolock) left outer join we_brands b with (nolock)" & vbcrlf & _
				"	on	a.brandid = b.brandid left outer join we_models c with (nolock)" & vbcrlf & _
				"	on	a.modelid = c.modelid left outer join XUAFilterRule d with (nolock)" & vbcrlf & _
				"	on	a.phoneTypeID = d.id" & vbcrlf & _				
				"where	1=1" & vbcrlf & _
				strSqlWhere & vbcrlf & _
				strSqlOrderBy
'			response.write "<pre>" & sql & "</pre>"
'			call responseEnd()
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		if not rs.eof then
		%>
		<table border="0" cellpadding="5" cellspacing="0" width="100%" style="font-size:11px;">
			<tr style="background-color:#eaf5e5;">
				<td style="font-size:14px; font-weight:bold; border-top:1px solid #c1e0b2; border-bottom:1px solid #c1e0b2; border-left:1px solid #c1e0b2;" width="35">ID</td>
				<td style="font-size:14px; font-weight:bold; border-top:1px solid #c1e0b2; border-bottom:1px solid #c1e0b2;" width="*">Browser Data</td>
				<td style="font-size:14px; font-weight:bold; border-top:1px solid #c1e0b2; border-bottom:1px solid #c1e0b2;" width="120">WE Brand/Model</td>
				<td style="font-size:14px; font-weight:bold; border-top:1px solid #c1e0b2; border-bottom:1px solid #c1e0b2;" width="65">Visitors</td>
				<td style="font-size:14px; font-weight:bold; border-top:1px solid #c1e0b2; border-bottom:1px solid #c1e0b2;" width="65">Views</td>
				<td style="font-size:14px; font-weight:bold; border-top:1px solid #c1e0b2; border-bottom:1px solid #c1e0b2;" width="70">LastHit</td>
				<td style="font-size:14px; font-weight:bold; border-top:1px solid #c1e0b2; border-bottom:1px solid #c1e0b2;" width="60" align="center">isBot</td>
				<td style="font-size:14px; font-weight:bold; border-top:1px solid #c1e0b2; border-bottom:1px solid #c1e0b2;" width="80" align="left">
                	Phone Type
                    <select name="cbPhoneType" style="width:80px;" onChange="document.frm.submit()">
                    	<option value="0">ALL</option>
                    <%
					sql	=	"select	id, phoneTypeName from XUAFilterRule order by 1"
					set rsPhoneType = oConn.execute(sql)
					do until rsPhoneType.eof
					%>
                    	<option value="<%=rsPhoneType("id")%>" <%if cbPhoneType = rsPhoneType("id") then%>selected<%end if%>><%=rsPhoneType("phoneTypeName")%></option>
                    <%
						rsPhoneType.movenext
					loop
					%>
                    </select>
				</td>
				<td style="font-size:14px; font-weight:bold; border-top:1px solid #c1e0b2; border-bottom:1px solid #c1e0b2; border-right:1px solid #c1e0b2;" width="120" align="left">
                	modelNumber
				</td>
			</tr>
			<%
			lap = 0
			do until rs.eof
				lap = lap + 1			
				rowBackgroundColor = "#fff"
				if (lap mod 2) = 0 then rowBackgroundColor = "#eaf5fa"
			%>
			<tr style="background-color:<%=rowBackgroundColor%>;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='1px solid <%=rowBackgroundColor%>';this.style.backgroundColor='<%=rowBackgroundColor%>'">
				<td id="lapid<%=rs("id")%>_1"><%=rs("id")%></td>
				<td id="lapid<%=rs("id")%>_2"><%=rs("browserData")%></td>
				<td id="lapid<%=rs("id")%>_3">
                    <%if rs("modelName") = "" then%>
                    	<a id="lapid<%=rs("id")%>_3_1" href="javascript:enterModel(<%=rs("id")%>,'lapid<%=rs("id")%>_3_1','lapid<%=rs("id")%>_3_2','<%=rs("modelNumber")%>')">Enter WE Model</a>
                    <%else%> 
					<%=rs("brandName")%><br><%=rs("modelName")%>
                    <%end if%>
					<div id="lapid<%=rs("id")%>_3_2"></div>
                </td>
				<td id="lapid<%=rs("id")%>_4"><%=formatnumber(rs("visitors"), 0)%></td>
				<td id="lapid<%=rs("id")%>_5"><%=formatnumber(rs("views"), 0)%></td>
				<td id="lapid<%=rs("id")%>_6"><%=rs("lastHit")%></td>
				<td id="lapid<%=rs("id")%>_7" align="center"><input type="checkbox" name="chkBot" <%if rs("isBot") then%>checked<%end if%> value="1" onClick="onChkBot(this,<%=rs("id")%>)" /></td>
				<td id="lapid<%=rs("id")%>_8"><%=rs("phoneTypeName")%></td>
				<td id="lapid<%=rs("id")%>_9">
                	<a href="https://www.google.com/search?q=<%=rs("modelNumber")%>" target="_blank" style="text-decoration:none; color:#333;"><%=rs("modelNumber")%></a>
				</td>
			</tr>
			<%
				rs.movenext
			loop
			%>
		</table>
		<br>
		<%
		else
		%>
        <div style="font-size:15px; font-weight:bold; color:#333; padding:10px;">Records not found</div>
        <%
		end if
		%>
       	</div>
        <!--// result -->
    </div>
</form>    
</center>
<script>
	function enterModel(lapid,lnk,retDiv,modelNumber) {
		document.getElementById(lnk).style.display = 'none';
		ajax('/ajax/admin/ajaxUpdateBrowserData.asp?updateType=enterModel&UAID='+lapid+'&modelNumber='+modelNumber,retDiv);
	}
	
	function onChkBot(o,lapid) {
		var isBot = 0;
		for (i=1; i<=9; i++) {
			if (o.checked) {
				isBot = 1;
				document.getElementById('lapid'+lapid+'_'+i).style.textDecoration = "line-through";
			} else {
				document.getElementById('lapid'+lapid+'_'+i).style.textDecoration = "none";
			}
		}
		ajax('/ajax/admin/ajaxUpdateBrowserData.asp?UAID='+lapid+'&updateType=bot&isbot='+isBot,'dumpZone');
	}
	
	function onCbModel(lapid,modelid,modelNumber) {
		document.getElementById('dumpZone').innerHTML = '';
		ajax('/ajax/admin/ajaxUpdateBrowserData.asp?updateType=updateModel&UAID='+lapid+'&modelid='+modelid+'&modelNumber='+modelNumber,'dumpZone');
	}
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->