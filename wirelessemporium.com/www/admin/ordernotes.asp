<%header = 0%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/admin/formatNotes.asp"-->

<%
dim OrderID, txtNotes
OrderID = Request.QueryString("OrderID")
if OrderID = "" then Request.Form("ORDERID")
txtNotes = Request.Form("txtNotes")

if OrderID = "" then
	response.write "<b><font color='red'>MISSING ORDER RECORD ID. PLEASE CLICK ON BACK BUTTON TO GO BACK TO PREVIOUS PAGE.</font></b>"
end if

EditNotes = SQLquote(request.form("txtNotes"))
AddNotes = SQLquote(request.form("txtAddNotes"))

'response.write "EditNotes:<pre>" & EditNotes & "</pre>"
'response.write "AddNotes:<pre>" & AddNotes & "</pre>"

if AddNotes <> "" then EditNotes = EditNotes & vbcrlf & formatNotes(AddNotes) end if

'response.write "final:<pre>" & EditNotes & "</pre>"

dim strMsg, objRs
if Request.Form("hidSave") <> "" then
	Set objRs = Server.CreateObject("ADODB.RECORDSET")
	SQL = "SELECT ordernotes FROM we_ordernotes WHERE orderid='" & OrderID & "'"
	objRs.Open SQL, oConn
	if not objRs.EOF then
		SQL = "UPDATE we_ordernotes SET Ordernotes='" & SQLquote(EditNotes) & "' WHERE OrderID='" & cDbl(OrderID) & "'"
	else
		SQL = "INSERT INTO we_ordernotes(OrderID,Ordernotes) VALUES('" & cDbl(OrderID) & "','" & SQLquote(EditNotes) & "')"
	end if
	oConn.Execute SQL
	If oConn.Errors.Count > 0 Then
		strMsg = "Error while updating notes : " & Err.Number & " : " & Err.Description
	Else
		strMsg = "Order notes updated succesfully."
	End If
	response.write "<script>self.close();</script>"
	objRs.Close
	Set objRs = Nothing
Else
	Set objRs = Server.CreateObject("ADODB.RECORDSET")
	SQL = "SELECT ordernotes FROM we_ordernotes WHERE orderid='" & OrderID & "'"
	objRs.Open SQL, oConn
	if not objRs.EOF then txtNotes = objRs.Fields("ordernotes").value
	objRs.Close
	Set objRs = Nothing
End If
%>

<form name="frmordernotes" method="post">
	<table width="100%" border="1" cellspacing="0" cellpadding="2" align="center">
		<tr class="bigText" bgcolor="#CCCCCC">
			<td colspan="2" align="center"> Enter Notes For Order Id : <%=OrderID%></td>
		</tr>
		<%If strMsg <> "" Then%>
			<tr class="smlText">
				<td colspan="2" align="center"><b><%=strMsg%></b></td>
			</tr>
		<%End If%>
		<tr class="smlText">
			<td valign="top" align="right">Edit Notes :&nbsp;&nbsp;</td>
			<td valign="top" align="left"><textarea name="txtNotes" class="smltext" cols="80" rows="8" readonly="readonly"><%=Trim("" & txtNotes)%></textarea></td>
		</tr>
		<tr class="smlText">
			<td valign="top" align="right">Add Notes :&nbsp;&nbsp;</td>
			<td valign="top" align="left"><textarea name="txtAddNotes" class="smltext" cols="80" rows="7"></textarea></td>
		</tr>
		<tr class="smlText">
			<td colspan="2" align="center">
				<input type="submit" name="btnsave" class="smltext" value="Submit">&nbsp;&nbsp;
				<input type="button" name="btncancel" class="smltext" value="Cancel" onClick="self.close();">
				<input type="hidden" name="ORDERID" value="<%=OrderID%>">
				<input type="hidden" name="hidsave" value="true">
			</td>
		</tr>
	</table>
</form>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
