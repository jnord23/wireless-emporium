<%
response.buffer = false
pageTitle = "Admin - Transmit CellphoneMate Orders"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_switchChars.asp"-->
<%
if request.form("submitted") <> "" then
	dim dateStart
	dateStart = request.form("dateStart")
	if not isDate(dateStart) then
		response.write "<h3>Start Date must be a valid date.</h3>"
		response.end
	end if
	
	dim HoldOrderNum
	HoldOrderNum = 9999999
	
	SQL = "SELECT A.orderID, A.thub_posted_date, A.shiptype, B.quantity, B.orderdetailid, C.itemDesc, C.vendor, D.* FROM we_Orders A"
	SQL = SQL & " INNER JOIN we_Orderdetails B ON A.orderID = B.orderID"
	SQL = SQL & " INNER JOIN we_Items C ON B.itemID = C.itemID"
	SQL = SQL & " INNER JOIN we_Accounts D ON A.accountID = D.AccountID WHERE A.store = 0"
	SQL = SQL & " AND A.approved = 1 AND (A.cancelled IS NULL OR A.cancelled = 0)"
	SQL = SQL & " AND A.thub_posted_date >= '" & dateStart & "' AND A.thub_posted_date < '" & dateAdd("d",1,date) & "'"
	SQL = SQL & " AND C.vendor = 'CM'"
	SQL = SQL & " AND B.SentToDropShipper IS NULL"
	SQL = SQL & " ORDER BY A.orderID"
	response.write "<p>" & SQL & "</p>"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		do until RS.eof
			SQL = "SELECT orderID,accountID FROM we_orders WHERE orderid = '" & RS("orderID") & "'"
			response.write "<p>" & SQL & "</p>"
			set RS2 = Server.CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			if not RS2.eof then
				nOrderID = RS2("orderID")
				filename = "CellphoneMate_" & replace(date,"/","-") & "_WE" & ".pdf"
				path = server.MapPath("/tempCSV/CellphoneMate/" & filename)
				%><!--#include virtual="/admin/ProcessOrders_inc.asp"--><%
				SQL = "UPDATE we_OrderDetails SET SentToDropShipper = '" & now & "' WHERE orderdetailid = " & rs("orderdetailid") & " and orderID = '" & nOrderID & "'"
				response.write "<p>" & SQL & "</p>" & vbcrlf
				oConn.execute SQL
				
				bodyText = "<h3><u>CellphoneMate Orders</u></h3>"
				bodyText = bodyText & "<h3>(PDF FILE ATTACHED)</h3>"
				Set objErrMail = CreateObject("CDO.Message")
				With objErrMail
					.From = "service@wirelessemporium.com"
					.To = "Jessica@cellphone-mate.com,susan@cellphone-mate.com,beverley@cellphone-mate.com"
					.cc = "erica@wirelessemporium.com"
					.bcc = "ruben@wirelessemporium.com,jon@wirelessemporium.com"
					.Subject = "CellphoneMate Orders for " & date
					.HTMLBody = bodyText
					.AddAttachment path
					.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
					.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
					.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
					.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
					.Configuration.Fields.Update
					.Send
				End With
				set objErrMail = nothing
			end if
			RS2.close
			set RS2 = nothing
			RS.movenext
		loop
	else
		response.write "<h3>No CellphoneMate Orders Found.</h3>" & vbcrlf
	end if
	RS.close
	set RS = nothing
	
	response.write "<h3>DONE!</h3>" & vbcrlf
else
	%>
	<form action="CellphoneMate_transmit_orders.asp" method="post">
		<p class="normalText"><input type="text" name="dateStart" value="<%=dateAdd("d",-4,date)%>">&nbsp;&nbsp;Start Date (thub_posted_date)</p>
		<p class="normalText"><input type="submit" name="submitted" value="Submit"></p>
	</form>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
