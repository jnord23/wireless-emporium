<%
pageTitle = "WE Company Directory"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" width="750" align="center" cellpadding="0" cellspacing="0"><tr><td>
<p>&nbsp;</p>
<p class="biggerText"><%=pageTitle%></p>
<br>

<%
if request("submitted") <> "" then
	Set Pdf = Server.CreateObject("Persits.Pdf")
	Set Doc = Pdf.CreateDocument
	Set Page = Doc.Pages.Add
	Set Param = Pdf.CreateParam
	Set Font = Doc.Fonts("Times-Roman")
	
	' add logo image
	Set Image = Doc.OpenImage(Server.MapPath("/images/newimages/WElogo.gif"))
	Param("x") = 30
	Param("y") = 680
	Param("ScaleX") = .5
	Param("ScaleY") = .5
	Page.Canvas.DrawImage Image, Param
	
	' add headlines & body text
	strText = "<center><p><b>" & switchChars(RS("Headline")) & "</b>"
	if not isNull(RS("Headline2")) and trim(RS("Headline2")) <> "" then strText = strText & "<br><i>" & switchChars(RS("Headline2")) & "</i>"
	strText = strText & "</p></center>"
	strText = strText & "<div align='justify'><p><b>Orange, CA</b>&nbsp;&#150;&nbsp;" & formatDateTime(RS("ReleaseDate"),1) & "&nbsp;&#150;&nbsp;"
	strText = strText & switchChars(RS("BodyText")) & "</p>"
	strText = strText & "<p><b>About Wireless Emporium, Inc.</b><br>"
	strText = strText & "Wireless Emporium, Inc. is a recognized leader in the wireless accessory market "
	strText = strText & "supplying over 10,000 manufacturer-direct products to consumers, businesses, "
	strText = strText & "education and government institutions through their Web sites: "
	strText = strText & "<a href=""http://www.WirelessEmporium.com"">www.WirelessEmporium.com</a> and "
	strText = strText & "<a href=""http://www.CellphoneAccents.com"">www.CellphoneAccents.com</a>. "
	strText = strText & "Their product line includes chargers, batteries, Bluetooth headsets, BlackBerry "
	strText = strText & "accessories, data connectivity products plus ringtones, games, graphics and one "
	strText = strText & "of the largest selection of cell phone faceplates online all at discount prices "
	strText = strText & "and with free shipping.</p>"
	strText = strText & "<p><b>Media Contact:</b> Dot Anderson 972-978-5057</p></div>"
	strText = strText & "<center><p>###</p></center>"
	Param("x") = 60
	Param("y") = 650
	Param("width") = 492
	'Param("alignment") = 2
	Param("size") = 11
	Param("HTML") = true
	Page.Canvas.DrawText strText, Param, Font
	
	'Filename = Doc.Save(Server.MapPath("hello.pdf"),true)
	'Response.Write "Success! Download your PDF file <A HREF=" & Filename & ">here</A>"
	
	filename = "WE_Press_Release_" & replace(RS("releasedate"),"/","-") & ".pdf"
	Doc.SaveHttp "attachment;filename=" & filename
else
	sBodyText = "<html><head></head>" & vbCrLf
	
	sBodyText = "<style type='text/css'>" & vbCrLf
	sBodyText = sBodyText & "<!--" & vbCrLf
	sBodyText = sBodyText & ".regText {font-family: Arial,Helvetica; font-size: 9pt}" & vbCrLf
	sBodyText = sBodyText & ".bigText {font-family: Arial,Helvetica; font-size: 12pt}" & vbCrLf
	sBodyText = sBodyText & ".tinyText {font-family: Arial,Helvetica; font-size: 7pt}" & vbCrLf
	sBodyText = sBodyText & ".header {font-family: Arial,Helvetica; font-size: 10pt}" & vbCrLf
	sBodyText = sBodyText & ".tableStyle {border-style: solid; border-width: 1px; border-color: #000000}" & vbCrLf
	sBodyText = sBodyText & "-->" & vbCrLf
	sBodyText = sBodyText & "</style>" & vbCrLf
	sBodyText = sBodyText & "<p class='regText'></p>" & vbCrLf
	sBodyText = sBodyText & "<table width='100%' cellpadding='0' cellspacing='0' border='1'><tr><td align='left' width='200' valign='top'>" & vbcrlf
	sBodyText = sBodyText & "<img src=""/images/newimages/WElogo.gif"" border=""0"" align=""left"" width=""106"" height=""58"">" & vbcrlf
	sBodyText = sBodyText & "<p class=""header""><br>&nbsp;&nbsp;1410&nbsp;N.&nbsp;Batavia&nbsp;St.<br>&nbsp;&nbsp;Orange,&nbsp;CA&nbsp;&nbsp;92867</p>" & vbCrLf
	sBodyText = sBodyText & "</td></tr></table>" & vbcrlf
	
	response.write sBodyText
	
	
'	Tony�s Cell:	(714) 328-0379
'	Gene�s Cell:	(310) 386-6833
'	Dan�s Cell:	(714) 222-7013
	
	
	
'	Extensions
'	Tony:			21
'	Christine:		22
'	Michael:		23
'	Gene:			24
'	Randolph:		25
'	Dan:			26
'	Aldo:			27
end if
%>

</td></tr></table>

<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
