<%
response.buffer = false
pageTitle = "Admin - Upload DecalSkin Images"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
dim Upload, myCount
set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = true
myCount = Upload.Save

if upload.form("Upload") <> "" then
	dim path, path_CO, newName, newName_CO, SavePath
	dim filesys, jpeg, demofolder, filecoll, fil
	dim aCount
	aCount = 0
	
	path = server.MapPath("/productpics") & "\"
'	path_CO = replace(server.MapPath("/productpics"), "\productpics", "\cellularoutfitter.com\www\productpics") & "\"
	path_CO = server.MapPath("/productpics_co") & "\"
	
	set filesys = Server.CreateObject("Scripting.FileSystemObject")
	set jpeg = Server.CreateObject("Persits.Jpeg")
	
	dim fileDS
	set fileDS = upload.files("DecalskinPicUpload")
	if fileDS is nothing then
		response.write "<h1>No image uploaded.</h1>" & vbcrlf
		response.end
	else
		if myCount > 0 then
			for each fil in upload.files
				newName = fil.filename
				newName_CO = replace(fil.filename,"_skin_","_decal_skin_")
				
				jpeg.OpenBinary(fileDS.Binary)
				SavePath = path & "big\" & newName
				jpeg.Width = 300
				jpeg.Height = 300
				jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				SavePath = path & "thumb\" & newName
				jpeg.Width = 100
				jpeg.Height = 100
				jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				SavePath = path & "homepage65\" & newName
				jpeg.Width = 65
				jpeg.Height = 65
				jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				SavePath = path & "icon\" & newName
				jpeg.Width = 45
				jpeg.Height = 45
				jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				
				jpeg.OpenBinary(fileDS.Binary)
				SavePath = path_CO & "big\" & newName_CO
				jpeg.Width = 300
				jpeg.Height = 300
				jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				SavePath = path_CO & "thumb\" & newName_CO
				jpeg.Width = 100
				jpeg.Height = 100
				jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				SavePath = path_CO & "homepage65\" & newName_CO
				jpeg.Width = 65
				jpeg.Height = 65
				jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				SavePath = path_CO & "icon\" & newName_CO
				jpeg.Width = 45
				jpeg.Height = 45
				jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				
				aCount = aCount + 1
			next
			response.write "<h3>" & aCount & " images uploaded.</h3>"
		end if
	end if
	
	set filesys = nothing
	set jpeg = nothing
	set demofolder = nothing
	set filecoll = nothing
else
	%>
	<form action="Decalskin_upload_images.asp" method="post" enctype="multipart/form-data">
		<p><input name="DecalskinPicUpload" type="file"></p>
		<p><input name="Upload" type="submit" value="Upload"></p>
	</form>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
