<%
response.buffer = false
dim thisUser, adminID
pageTitle = "All Current Promotions"
header = 1
thisUser = Request.Cookies("username")
adminID = Request.Cookies("adminID")
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
if request.form("submitted") = "Update" then
	StartDate = request.form("StartDate")
	EndDate = request.form("EndDate")
	if not isDate(StartDate) then
		strError = "Start Date must be a valid date."
	else
		StartDate = dateValue(StartDate)
	end if
	if not isDate(EndDate) then
		strError = "End Date must be a valid date."
	else
		EndDate = dateValue(EndDate)
	end if
	if strError = "" then
		if StartDate > EndDate then
			strError = "Start Date must be earlier than or equal to End Date."
		end if
	end if
	if strError = "" then
		if request.form("id") = "NEW" then
			SQL = "INSERT INTO PromoNotes (Store,Promotion,Details,StartDate,EndDate,EntdBy,EntdDate) VALUES ("
			SQL = SQL & "'" & request.form("Store") & "',"
			SQL = SQL & "'" & SQLquote(request.form("Promotion")) & "',"
			SQL = SQL & "'" & SQLquote(request.form("Details")) & "',"
			SQL = SQL & "'" & StartDate & "',"
			SQL = SQL & "'" & EndDate & "',"
			SQL = SQL & "'" & adminID & "',"
			SQL = SQL & "'" & now & "')"
		else
			SQL = "UPDATE PromoNotes SET"
			SQL = SQL & " Store = '" & request.form("Store") & "',"
			SQL = SQL & " Promotion = '" & SQLquote(request.form("Promotion")) & "',"
			SQL = SQL & " Details = '" & SQLquote(request.form("Details")) & "',"
			SQL = SQL & " StartDate = '" & StartDate & "',"
			SQL = SQL & " EndDate = '" & EndDate & "',"
			SQL = SQL & " EntdBy = '" & adminID & "',"
			SQL = SQL & " EntdDate = '" & now & "'"
			SQL = SQL & " WHERE id = '" & request.form("id") & "'"
		end if
		'response.write "<p>" & SQL & "</p>" & vbcrlf
		oConn.execute SQL
	else
		response.write "<p>" & strError & "</p>" & vbcrlf
		response.write "<p><a href=""javascript:history.back();"">BACK</a></p>" & vbcrlf
		response.end
	end if
	%>
	<table align="center" width="100%">
		<tr bgcolor="#CCCCCC"><td align="center" valign="middle" class="bigText" bgcolor="#CCCCCC" height="20">&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>
		<tr><td align="center"><font face="verdana" size="3"><b>Records Updated Sucessfully</b><br><br><a href="db_update_promo_notes.asp">Back to "Notes on WE Promotions" page</a></font></td></tr>
		<tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>
		<tr bgcolor="#CCCCCC"><td align="center" valign="middle" class="bigText" bgcolor="#CCCCCC" height="20">&nbsp;</td></tr>
	</table>
	<%
elseif request.querystring("id") <> "" then
	dim id, Promotion, Details, StartDate, EndDate
	id = request.querystring("id")
	if isNumeric(id) then
		SQL = "SELECT * FROM PromoNotes WHERE id = '" & id & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			Store = RS("Store")
			Promotion = RS("Promotion")
			Details = RS("Details")
			StartDate = RS("StartDate")
			EndDate = RS("EndDate")
		end if
	end if
	%>
	<form action="db_update_promo_notes.asp" method="post">
		<p>
			Store:<br>
			<input type="radio" name="Store" value="0"<%if Store = 0 then response.write " checked"%>>WE&nbsp;&nbsp;&nbsp;
			<input type="radio" name="Store" value="1"<%if Store = 1 then response.write " checked"%>>CA&nbsp;&nbsp;&nbsp;
			<input type="radio" name="Store" value="2"<%if Store = 2 then response.write " checked"%>>CO&nbsp;&nbsp;&nbsp;
			<input type="radio" name="Store" value="3"<%if Store = 3 then response.write " checked"%>>PS&nbsp;&nbsp;&nbsp;
			<input type="radio" name="Store" value="10"<%if Store = 10 then response.write " checked"%>>ER&nbsp;&nbsp;&nbsp;
		</p>
		<p>Promotion:<br><input type="text" name="Promotion" value="<%=Promotion%>" size="100" maxlength="200"></p>
		<p>Details:<br><textarea name="Details" rows="3" cols="100"><%=Details%></textarea></p>
		<p>Start&nbsp;Date:<br><input type="text" name="StartDate" value="<%=StartDate%>" size="10" maxlength="10"></p>
		<p>End&nbsp;Date:<br><input type="text" name="EndDate" value="<%=EndDate%>" size="10" maxlength="10"></p>
		<p><input type="hidden" name="id" value="<%=id%>"><input type="submit" name="submitted" class="smltext" value="Update"></p>
	</form>
	<%
else
	%>
	<table width="700" border="1" cellspacing="0" cellpadding="3" align="center">
		<tr align="left" valign="top" class="bigText" bgcolor="#CCCCCC">
			<td colspan="4" align="center"><%=pageTitle%></td>
		</tr>
		<tr align="left" valign="top">
			<td colspan="4" align="center"><a href="db_update_promo_notes.asp?id=NEW" class="bottomText-link">ADD NEW</a></td>
		</tr>
		<%
		SQL = "SELECT * FROM PromoNotes WHERE EndDate >= '" & date & "'"
		SQL = SQL & " ORDER BY Store, StartDate, EndDate, id"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		do until RS.eof
			select case RS("Store")
				case 0 : Store = "WE"
				case 1 : Store = "CA"
				case 2 : Store = "CO"
				case 3 : Store = "PS"
				case 10 : Store = "ER"
			end select
			%>
			<tr class="smlText">
				<td width="20" align="center" valign="top" class="bigText"><%=Store%></td>
				<td width="480" align="left" valign="top"><b>&nbsp;<%=RS("Promotion")%></b></td>
				<td width="150" align="center" valign="top"><%=dateValue(RS("StartDate")) & " - " & dateValue(RS("EndDate"))%></td>
				<td width="50" align="center" valign="top"><a href="db_update_promo_notes.asp?id=<%=RS("id")%>" class="bottomText-link">edit</a></td>
			</tr>
			<tr class="smlText">
				<td colspan="4" align="left" valign="top"><%=RS("Details")%></td>
			</tr>
			<tr class="smlText">
				<td colspan="4" align="left" valign="top" bgcolor="#000000"><img src="/images/spacer.gif" width="1" height="2" border="0"></td>
			</tr>
			<%
			RS.movenext
		loop
		RS.close
		set RS = nothing
		%>
	</table>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
