<%
pageTitle = "Admin - Update WE Database - Add/Edit Links"
header = 1
GuestAllowed = "yes"
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<blockquote>

<h3>Update WE Database - Add/Edit Links</h3>

<%
dim SQL, strError
strError = ""
if request.form("submitted") = "Update" then
	dim category, Title, Text, URL
	for a = 1 to request.form("totalCount")
		if request.form("Delete" & a) = "1" then
			SQL = "DELETE FROM we_Links"
		else
			category = request.form("category" & a)
			Title = SQLquote(request.form("Title" & a))
			Text = SQLquote(request.form("Text" & a))
			URL = SQLquote(request.form("URL" & a))
			if Title = "" then strError = strError & "You must enter a Title for #" & a & ".<br>"
			if inStr(URL,"http://") = 0 and inStr(URL,"https://") = 0 then strError = strError & "URL must be in format ""http://[URL]"" or ""https://[URL]"" for #" & a & ".<br>"
			SQL = "UPDATE we_Links SET"
			SQL = SQL & " cat='" & category & "',"
			SQL = SQL & " title='" & Title & "',"
			SQL = SQL & " text='" & Text & "',"
			SQL = SQL & " URL='" & URL & "'"
		end if
		SQL = SQL & " WHERE id='" & request.form("id" & a) & "'"
		if strError = "" then
			'response.write "<p>" & SQL & "</p>" & vbcrlf
			oConn.execute SQL
		else
			response.write "<p>" & strError & "</p>" & vbcrlf
		end if
	next
	response.write "<h3>RECORDS UPDATED!</h3>" & vbcrlf
	strError = ""
	if request.form("TitleNEW") <> "" then
		category = request.form("categoryNEW")
		Title = SQLquote(request.form("TitleNEW"))
		Text = SQLquote(request.form("TextNEW"))
		URL = SQLquote(request.form("URLNEW"))
		if Title = "" then strError = strError & "You must enter a Title for the Link you are adding.<br>"
		if inStr(URL,"http://") = 0 and inStr(URL,"https://") = 0 then strError = strError & "URL must be in format ""http://[URL]"" or ""https://[URL]"" for the Link you are adding.<br>"
		SQL = "INSERT INTO we_Links (cat,title,text,URL) VALUES ("
		SQL = SQL & "'" & category & "',"
		SQL = SQL & "'" & Title & "',"
		SQL = SQL & "'" & Text & "',"
		SQL = SQL & "'" & URL & "')"
		if strError = "" then
			'response.write "<p>" & SQL & "</p>" & vbcrlf
			oConn.execute SQL
			response.write "<h3>NEW RECORD ADDED!</h3>" & vbcrlf
		else
			response.write "<p>" & strError & "</p>" & vbcrlf
		end if
	end if
	response.write "<p><a href=""db_update_links.asp"">Back to Add/Edit Screen.</a></p>" & vbcrlf
else
	dim RS, a
	SQL = "SELECT * FROM we_Links ORDER BY cat,id"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	a = 0
	%>
	<form action="db_update_links.asp" method="post">
		<table border="1" cellpadding="3" cellspacing="0" align="center" class="smlText">
			<%
			if RS.eof then
				%>
				<tr>
					<td valign="top" align="left" colspan="4">
						<h3>No records matched<br><br>So cannot make table...</h3>
						<p><a href="db_update_links.asp">Try again.</a></p>
						<p>&nbsp;</p>
					</td>
				</tr>
				<%
			else
				do until RS.eof
					a = a + 1
					%>
					<tr>
						<td valign="top" align="left">
							<input type="hidden" name="id<%=a%>" value="<%=RS("id")%>">
							<b>Category:</b><br>
							<select type="text" name="Category<%=a%>">
								<option value="1"<%if RS("cat") = 1 then response.write " selected"%>>Cellular Phone Resources</option>
								<option value="2"<%if RS("cat") = 2 then response.write " selected"%>>Electronics Resources</option>
								<option value="3"<%if RS("cat") = 3 then response.write " selected"%>>Software Resources</option>
								<option value="4"<%if RS("cat") = 4 then response.write " selected"%>>Other Resources</option>
							</select>
						</td>
						<td valign="top" align="left"><b>Title:</b><br><input type="text" name="Title<%=a%>" value="<%=RS("title")%>" size="50" maxlength="50"></td>
						<td valign="top" align="left"><b>URL:</b><br><input type="text" name="URL<%=a%>" value="<%=RS("URL")%>" size="50" maxlength="50"></td>
						<td valign="top" align="center"><b>Delete:</b><br><input type="checkbox" name="Delete<%=a%>" value="1"></td>
					</tr>
					<tr>
						<td valign="top" align="left" colspan="4"><b>Text:</b><br><input type="text" name="Text<%=a%>" value="<%=RS("text")%>" size="145" maxlength="450"></td>
						
					</tr>
					<tr>
						<td align="center" colspan="4" bgcolor="#999999"><img src="/images/spacer.gif" width="1" height="2" border="0"></td>
					</tr>
					<%
					RS.movenext
				loop
			end if
			%>
			<tr>
				<td align="left" colspan="4"><font color="#FF0000"><b>ADD NEW LINK</b></font></td>
			</tr>
			<tr>
				<td valign="top" align="left">
					<b>Category:</b><br>
					<select type="text" name="CategoryNEW">
						<option value="1"<%if request.form("CategoryNEW") = "1" then response.write " selected"%>>Cellular Phone Resources</option>
						<option value="2"<%if request.form("CategoryNEW") = "2" then response.write " selected"%>>Electronics Resources</option>
						<option value="3"<%if request.form("CategoryNEW") = "3" then response.write " selected"%>>Software Resources</option>
						<option value="4"<%if request.form("CategoryNEW") = "4" then response.write " selected"%>>Other Resources</option>
					</select>
				</td>
				<td valign="top" align="left"><b>Title:</b><br><input type="text" name="TitleNEW" value="<%=request.form("TitleNEW")%>" size="50" maxlength="50"></td>
				<td valign="top" align="left" colspan="2"><b>URL:</b><br><input type="text" name="URLNEW" value="<%=request.form("URLNEW")%>" size="50" maxlength="50"></td>
			</tr>
			<tr>
				<td valign="top" align="left" colspan="4"><b>Text:</b><br><input type="text" name="TextNEW" value="<%=request.form("TextNEW")%>" size="145" maxlength="450"></td>
			</tr>
			<tr>
				<td align="center" colspan="4">
					<input type="hidden" name="totalCount" value="<%=a%>">
					<input type="submit" name="submitted" value="Update">
				</td>
			</tr>
		</form>
	</table>
	<p>&nbsp;</p>
	<%
	RS.close
	set RS = nothing
end if
%>

</blockquote>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
