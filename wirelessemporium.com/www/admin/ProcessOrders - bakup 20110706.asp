<%
pageTitle = "Admin - Process Wireless Emporium Orders"
header = 1

dim storeID
storeID = 0

' Delete all PDFs older than 1 month
dim filesys, demofolder, fil, filecoll, demofile
set filesys = CreateObject("Scripting.FileSystemObject")
set demofolder = filesys.GetFolder(server.MapPath("\admin\tempPDF\"))
set filecoll = demofolder.Files
for each fil in filecoll
	if fil.DateLastModified < dateAdd("M",-6,date) then
		set demofile = filesys.GetFile(server.MapPath("\admin\tempPDF\" & fil.name))
		demofile.Delete
	end if
next

today = date

dim dateSearch : dateSearch = request.Form("txtSearchDate")
dim dateSearchEnd
if dateSearch = "" then
	dateSearch = today
end if

dateSearchEnd = cdate(dateSearch) + 1
'response.write "dateSearch:" & dateSearch & "<br>"
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_switchChars.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<script>
function showPop(processPDF, shiptoken2) 
{
	var url="processOrders_pop.asp?processPDF=" + escape(processPDF) + "&shiptoken2=" + escape(shiptoken2);
	window.open(url,"OrdersNotProcessed","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
</script>
<style>
.mouseover	{ background-color:#9BCEFF;}
</style>
<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td align="left">
        	<form name="frmSearch" method="post">
				<input type="text" name="txtSearchDate" readonly="readonly" value="<%=dateSearch%>">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.frmSearch.txtSearchDate,<%=today%>);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;Processed Date
				<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>                
				<p><input type="submit" value="Find Orders"></p>                
            </form>
        </td>
    </tr>
	<tr>
		<td align="center">
		<%
        sql	=	"select	case when isnull(q.numRegularOrder, 0) = isnull(q.numVendorOrder, 0) then 1 else 0 end isDropShipOrder" & vbcrlf & _
				"	,	x.shortDesc" & vbcrlf & _
				"	,	isnull(case when o.isShipMethod = 1 then o.token end" & vbcrlf & _
				"				, " & vbcrlf & _
				"				case when t.shiptypeid = 1 then " & vbcrlf & _
				"						case when q.quantity > 1 then t.token + '-MULTI' else t.token + '-SINGLE' end" & vbcrlf & _
				"					else isnull(t.token, a.shiptype) " & vbcrlf & _
				"				end" & vbcrlf & _
				"		) shipToken2" & vbcrlf & _
				"	,	convert(varchar(10), a.thub_posted_date, 20) processed_datetime" & vbcrlf & _
				"	,	a.processPDF, a.processTXT" & vbcrlf & _
				"	,	count(*) numOrders" & vbcrlf & _
				"	,	isnull(sum(case when a.scandate is not null then 1 else 0 end), 0) numScan" & vbcrlf & _
				"	,	isnull(sum(case when a.cancelled is not null or cancelled = 1 then 1 else 0 end), 0) numCancel" & vbcrlf & _
				"from 	we_orders a with (nolock) join xstore x with (nolock)" & vbcrlf & _
				"	on	x.site_id = a.store join " & vbcrlf & _
				"		(" & vbcrlf & _
				"		select	a.store, a.orderid, isnull(sum(b.quantity), 0) quantity, sum(b.quantity * isnull(c.itemWeight, 0.0)) itemWeight" & vbcrlf & _
				"			,	count(*) numRegularOrder" & vbcrlf & _
				"			,	isnull(sum(case when m.id is not null then 1 " & vbcrlf & _
				"								else" & vbcrlf & _
				"									case when c.vendor in ('CM', 'DS', 'MLD') then 1 " & vbcrlf & _
				"										when c.partnumber like '%HYP%' then 1" & vbcrlf & _
				"										else 0 end" & vbcrlf & _
				"							end), 0) numVendorOrder" & vbcrlf & _
				"		from 	we_orders a with (nolock) join we_orderdetails b with (nolock)" & vbcrlf & _
				"			on	a.orderid = b.orderid left outer join we_items c with (nolock)" & vbcrlf & _
				"			on	b.itemid = c.itemid left outer join we_items_musicskins m with (nolock)" & vbcrlf & _
				"			on	b.itemid = m.id" & vbcrlf & _
				"		where	a.thub_posted_date >= '" & dateSearch & "'" & vbcrlf & _
				"			and a.thub_posted_date < '" & dateSearchEnd & "'" & vbcrlf & _
				"			and	a.approved = 1" & vbcrlf & _
				"			and	(b.reship = 0 or b.reship is null)" & vbcrlf & _
				"			and	b.itemid not in (1)" & vbcrlf & _
				"		group by a.store, a.orderid" & vbcrlf & _
				"		) q" & vbcrlf & _
				"	on	a.store = q.store and a.orderid = q.orderid left outer join XShipType t with (nolock)" & vbcrlf & _
				"	on	a.shiptype = t.shipdesc left outer join XOrderType o with (nolock)" & vbcrlf & _
				"	on	a.extOrderType = o.typeid " & vbcrlf & _
				"where	a.thub_posted_date >= '" & dateSearch & "'" & vbcrlf & _
				"	and a.thub_posted_date < '" & dateSearchEnd & "'" & vbcrlf & _
				"	and	x.active = 1" & vbcrlf & _
				"group by	case when isnull(q.numRegularOrder, 0) = isnull(q.numVendorOrder, 0) then 1 else 0 end " & vbcrlf & _
				"		,	x.shortDesc" & vbcrlf & _
				"		,	isnull(case when o.isShipMethod = 1 then o.token end" & vbcrlf & _
				"					, " & vbcrlf & _
				"					case when t.shiptypeid = 1 then " & vbcrlf & _
				"							case when q.quantity > 1 then t.token + '-MULTI' else t.token + '-SINGLE' end" & vbcrlf & _
				"						else isnull(t.token, a.shiptype) " & vbcrlf & _
				"					end" & vbcrlf & _
				"			) " & vbcrlf & _
				"		,	convert(varchar(10), a.thub_posted_date, 20) " & vbcrlf & _
				"		,	a.processPDF, a.processTXT" & vbcrlf & _
				"order by 1 desc, 2 desc, 3, 4" & vbcrlf
				
        set objRsAllOrders = oConn.execute(sql)

		numTotalDropShip = 0
        if not objRsAllOrders.eof then
		%>
            <b>View By Processed Files</b><br />
            <table width="850" border="1" align="center" cellpadding="5" cellspacing="0" style="border-collapse:collapse;font-family: Arial, Helvetica, sans-serif; font-size:12px;">
                <tr>
                    <td bgcolor="#ccc000" width="50" rowspan="2" align="center"><b>STORE</b></td>
                    <td bgcolor="#ccc000" width="80" rowspan="2"><b>SHIP TYPE</b></td>
                    <td bgcolor="#ccc000" width="110" rowspan="2"><b>Processed<br />DateTime</b></td>
                    <td bgcolor="#ccc000" width="280" rowspan="2"><b>Link to download<br />sales orders</b></td>
                    <td bgcolor="#ccc000" width="260" colspan="4" align="center"><b>Number of Orders</b></td>
                </tr>
                <tr>
                    <td bgcolor="#ccc000" width="60"><b>Processed</b></td>
                    <td bgcolor="#ccc000" width="60"><b>Scanned</b></td>
                    <td bgcolor="#ccc000" width="60"><b>Cancelled</b></td>
                    <td bgcolor="#ccc000" width="80"><b>Status</b></td>
                </tr>
			<%		
			do until objRsAllOrders.eof
				numOrders = objRsAllOrders("numOrders")
				numScan = objRsAllOrders("numScan")
				numCancel = objRsAllOrders("numCancel")
				isDropShipper = objRsAllOrders("isDropShipOrder")
				
				processPDF = objRsAllOrders("processPDF")
				processTXT = objRsAllOrders("processTXT")
				
				if isnumeric(numOrders) then
					if numOrders = 0 then
						processRatio = 0.0
					else
						processRatio = (1.0 * (numScan+numCancel) / numOrders) * 100.0
					end if
				end if
				
				if isDropShipper = 1 then
					numTotalDropShip = numTotalDropShip + numOrders
				else
				%>
                <tr onMouseOver="this.className='mouseover'" onMouseOut="this.className=''">
                    <td><%=objRsAllOrders("shortdesc")%></td>
                    <td><%=objRsAllOrders("shiptoken2")%></td>
                    <td><%=objRsAllOrders("processed_datetime")%></td>
                    <td>
					<%
                    if processPDF <> "" then
                    %>
                        - <a target="_blank" href="/admin/tempPDF/salesorders/<%=processPDF%>"><%=processPDF%></a>
					<%
                    end if
                    if processTXT <> "" then
                    %>
                        <br />- <a target="_blank" href="/admin/tempTXT/salesorders/<%=processTXT%>"><%=processTXT%></a>
					<%
                    end if			
                    %>
                    </td>
                    <td>
                    <%
					if processRatio = 100 then
					%>
						<%=formatnumber(numOrders, 0)%>
                    <%
					else
					%>
						<a href="#" onclick="showPop('<%=processPDF%>','<%=objRsAllOrders("shipToken2")%>');return false;"><%=formatnumber(numOrders, 0)%></a>
                    <%
					end if
					%>
                    </td>
                    <td><%=formatnumber(numScan, 0)%></td>
                    <td><%=formatnumber(numCancel, 0)%></td>
                    <td>
                    	<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; font-size:12px;">
                        	<tr>
                            	<td align="left"><%=formatnumber(processRatio, 2)%>%</td>
                                <td align="right">
								<%
                                if processRatio = 100 then
                                %>
                                    <img src="/images/check.jpg" border="0" width="15"/>
                                <%
                                else
                                %>
                                    <img src="/images/x.jpg" border="0" width="15"/>
                                <%
                                end if
                                %>                                
                                </td>
                            </tr>
                        </table>
					</td>
                </tr>                
				<%
				end if
                objRsAllOrders.movenext
            loop
            %>
                <tr onMouseOver="this.className='mouseover'" onMouseOut="this.className=''">
                    <td colspan="4" align="center">Dropship Orders</td>
                    <td colspan="3" align="left"><%=formatnumber(numTotalDropShip, 0)%></td>
                    <td align="center">
                    	<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; font-size:12px;">
                        	<tr>
                            	<td align="left"><%=formatnumber(100, 2)%>%</td>
                                <td align="right"><img src="/images/check.jpg" border="0" width="15" /></td>
                            </tr>
                        </table>
					</td>
                </tr>                            
            </table>
			<%
            else
            %>
			No orders processed found
			<%
            end if		
            %>
        </td>
	</tr>
    
    
    
<%
'===========================================================
'=========== disable old codes for now......
'===========================================================
if false then
%>    
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
<%
Server.ScriptTimeout = 1000 'seconds

dim filenamePDF, filenameTXT
filenamePDF = ""
filenameTXT = ""

if request("submitted") = "Process Orders" or request("submitBuyCom") = "Process Buy.Com Orders" or request("submitAtomic") = "Process AtomicMall Orders" or request("submitAmazon") = "Process Amazon Orders" then
	submitMailOption = request("mailOption")
	orderWeight = 0
	%>
	<p><a href="ProcessOrders.asp?mailOption=Priority">Process&nbsp;More&nbsp;Priority&nbsp;Only</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="ProcessOrders.asp">Process&nbsp;More&nbsp;First&nbsp;Class</a></p>
	<%
	if request("submitBuyCom") = "Process Buy.Com Orders" or request("submitAtomic") = "Process AtomicMall Orders" or request("submitAmazon") = "Process Amazon Orders" then
		strOrderStartNumber = 770000
	else
		if request("orderStart") <> "" and isNumeric(request("orderStart")) then
			strOrderStartNumber = Int(request("orderStart"))
			if request("orderEnd") <> "" and isNumeric(request("orderEnd")) then
				strOrderEndNumber = Int(request("orderEnd"))
				if strOrderStartNumber > strOrderEndNumber then
					strError = "Start Order # must be lower than End Order #"
				else
					andSQL = " AND A.OrderID <= " & request("orderEnd")
				end if
			end if
		else
			strError = "You must enter a valid Start Order #"
		end if
	end if
	
	if strError = "" then
		dim SQL, RS, strOrderStartNumber
		dim file, path
		dim shipname
		select case submitMailOption
			case "Priority" : shipname = "PRIO"
			case "Express" : shipname = "EXP"
			case "UPS" : shipname = "UPS"
			case "FCM-I"
				shipname = "FCM-I"
			case else
				shipname = "FCM"
				if request("qtyOption") = "Multi" then
					shipname = shipname & "-MULTI"
				else
					shipname = shipname & "-SINGLE"
				end if
		end select
		if request("submitBuyCom") = "Process Buy.Com Orders" then shipname = "BUYCOM"
		if request("submitAtomic") = "Process AtomicMall Orders" then shipname = "ATOMIC"
		if request("submitAmazon") = "Process Amazon Orders" then shipname = "AMAZON"
		
		if shipname = "PRIO" or shipname = "EXP" or shipname = "FCM-I" or shipname = "AMAZON" or shipname = "BUYCOM" then
			filenameTXT = "WE_" & replace(cStr(date),"/","-") & "_" & shipname & ".txt"
			path = Server.MapPath("tempTXT") & "\" & filenameTXT
			if filesys.FileExists(path) then filesys.DeleteFile(path)
			set file = filesys.CreateTextFile(path)
		elseif shipname <> "UPS" then
			SQL = "DELETE FROM DHL_temp"
			oConn.execute SQL
		end if
		
		SQL = "SELECT A.OrderID, D.vendor FROM we_orders A"
		SQL = SQL & " INNER JOIN we_orderdetails C ON C.OrderID = A.OrderID"
		SQL = SQL & " INNER JOIN we_Items D ON D.itemid = C.itemid"
		SQL = SQL & " WHERE A.thub_posted_to_accounting IS null"
		SQL = SQL & " AND A.approved = 1 AND (A.cancelled IS NULL OR A.cancelled = 0)"
		SQL = SQL & " AND A.store = " & storeID
		SQL = SQL & " AND ((A.OrderID >= " & strOrderStartNumber
		SQL = SQL & andSQL
		andSQL = ""
		
		if request("submitBuyCom") = "Process Buy.Com Orders" then
			SQL = SQL & " AND A.extOrderType = 7))"
		elseif request("submitAtomic") = "Process AtomicMall Orders" then
			SQL = SQL & " AND A.extOrderType = 5))"
		elseif request("submitAmazon") = "Process Amazon Orders" then
			SQL = SQL & " AND A.extOrderType = 6))"
		else
			SQL = SQL & " AND (A.extOrderType = 1 OR A.extOrderType = 2 OR A.extOrderType IS NULL)"
			if submitMailOption = "Priority" then
				andSQL = andSQL & " AND A.shiptype LIKE 'USPS Priority%'"
			else
				andSQL = andSQL & " AND A.shiptype NOT LIKE 'USPS Priority%'"
			end if
			if submitMailOption = "Express" then
				andSQL = andSQL & " AND A.shiptype LIKE 'USPS Express%'"
			else
				andSQL = andSQL & " AND A.shiptype NOT LIKE 'USPS Express%'"
			end if
			if submitMailOption = "UPS" then
				andSQL = andSQL & " AND (A.shiptype LIKE 'UPS Ground%' OR A.shiptype LIKE 'UPS 3 Day Select%' OR A.shiptype LIKE 'UPS 2nd Day Air%')"
			else
				andSQL = andSQL & " AND A.shiptype NOT LIKE 'UPS Ground%' AND A.shiptype NOT LIKE 'UPS 3 Day Select%' AND A.shiptype NOT LIKE 'UPS 2nd Day Air%'"
			end if
			if shipname = "FCM-I" then
				andSQL = andSQL & " AND A.shiptype = 'First Class Int''l'"
			else
				andSQL = andSQL & " AND A.shiptype <> 'First Class Int''l'"
			end if
			SQL = SQL & andSQL & ") OR (A.extOrderType = 3" & andSQL & "))"
		end if
		
		if request("qtyOption") = "Multi" then
			strSearch = ""
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			do until RS.eof
				SQL2 = "SELECT orderid,SUM(quantity) AS totalQty FROM we_orderdetails WHERE orderid = '" & RS("orderID") & "' GROUP BY orderid"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL2, oConn, 3, 3
				do until RS2.eof
					if RS2("totalQty") > 1 and inStr(strSearch,RS2("orderid") & ",") = 0 then strSearch = strSearch & RS2("orderid") & ","
					RS2.movenext
				loop
				RS.movenext
			loop
			if strSearch <> "" then
				strSearch = left(strSearch,len(strSearch)-1)
				response.write "<p>" & strSearch & "</p>"
				SQL = "SELECT A.OrderID, D.vendor FROM we_orders A"
				SQL = SQL & " INNER JOIN we_orderdetails C ON C.OrderID = A.OrderID"
				SQL = SQL & " INNER JOIN we_Items D ON D.itemid = C.itemid"
				SQL = SQL & " WHERE A.OrderID IN (" & strSearch & ")"
			end if
		end if
		
		' Find "Dropship Only" orders
		SQL = SQL & " AND C.SentToDropShipper IS NULL ORDER BY A.OrderID"
		session("errorSQL") = SQL
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		dim strOrders
		strOrders = ""
		
		if not RS.eof then
			holdOrderID = RS("OrderID")
			do until RS.eof
				if RS("OrderID") <> holdOrderID then
					if replace(replace(replace(strVendors,"|DS|",""),"|CM|",""),"|MLD|","") <> "" then
						if inStr(strOrders,holdOrderID) = 0 then strOrders = strOrders & holdOrderID & ","
					else
						if instr(request.ServerVariables("SERVER_NAME"),"staging") < 1 then
							SQL = "UPDATE WE_orders SET thub_posted_to_accounting='D', thub_posted_date='" & now & "', processTXT = '" & filenameTXT & "', processPDF='" & filenamePDF & "' WHERE orderID = '" & holdOrderID & "'"
							response.write "<p>" & SQL & "</p>" & vbcrlf
							oConn.execute SQL
						end if
					end if
					strVendors = ""
					holdOrderID = RS("OrderID")
				end if
				strVendors = strVendors & "|" & RS("vendor") & "|"
				RS.movenext
			loop
			if replace(replace(replace(strVendors,"|DS|",""),"|CM|",""),"|MLD|","") <> "" then
				if inStr(strOrders,holdOrderID) = 0 then strOrders = strOrders & holdOrderID & ","
			else
				if instr(request.ServerVariables("SERVER_NAME"),"staging") < 1 then
					SQL = "UPDATE WE_orders SET thub_posted_to_accounting='D', thub_posted_date='" & now & "', processTXT = '" & filenameTXT & "', processPDF='" & filenamePDF & "' WHERE orderID = '" & holdOrderID & "'"
					response.write "<p>" & SQL & "</p>" & vbcrlf
					oConn.execute SQL
				end if
			end if
			if strOrders <> "" then strOrders = left(strOrders,len(strOrders)-1)
		else
			response.write "<h3>No Records Found</h3>"
			response.end
		end if
		
		SQL = "SELECT A.OrderID,A.shippingid,A.ordersubtotal,A.shiptype,"
		SQL = SQL & " B.accountID,B.fname,B.lname,B.email,B.phone,B.sCity,B.sAddress1,B.sAddress2,B.sState,B.sZip,B.sCountry,"
		SQL = SQL & " C.quantity,D.itemWeight,D.typeID,D.price_Our,E.typeName"
		SQL = SQL & " FROM (((we_orders A INNER JOIN we_accounts B ON A.accountid=B.accountid)"
		SQL = SQL & " INNER JOIN we_orderdetails C ON C.OrderID=A.OrderID)"
		SQL = SQL & " INNER JOIN we_Items D ON D.itemid=C.itemid)"
		SQL = SQL & " INNER JOIN we_Types E ON D.typeID=E.typeID"
		SQL = SQL & " WHERE A.OrderID IN (" & strOrders & ")"
		
		if request("qtyOption") = "Multi" then
			SQL = SQL & " ORDER BY A.OrderID"
		else
			if request("submitAmazon") = "Process Amazon Orders" then
				SQL = SQL & " ORDER BY D.PartNumber"
			elseif request("submitBuyCom") = "Process Buy.Com Orders" then
				SQL = SQL & " ORDER BY B.fname, B.lname"
			elseif submitMailOption = "Priority" or submitMailOption = "Express" or submitMailOption = "UPS" then
				SQL = SQL & " ORDER BY A.OrderID"
			else
				SQL = SQL & " ORDER BY D.typeID,A.OrderID"
			end if
		end if
		
		session("errorSQL") = SQL
		response.write "<p>" & SQL & "</p>"
		if strOrders <> "" then
			session("errorSQL2") = SQL
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			
			dim strOrdersDHL, strOrdersDupCheck
			strOrdersDHL = ""
			strOrdersDupCheck = ""
			
			if shipname = "PRIO" or shipname = "EXP" or shipname = "FCM-I" or shipname = "AMAZON" or shipname = "BUYCOM" then
				strToWrite = """ContactName""" & vbTab & """ORDER_KEY""" & vbTab & """CUSTOMER_KEY""" & vbTab & """TXN_DT""" & vbTab & """EXT_REF_CD""" & vbTab & """QB_TXN_NUMBER""" & vbTab
				strToWrite = strToWrite & """CCY_CODE""" & vbTab & """REF_NUMBER""" & vbTab & """CLASS_REF""" & vbTab & """ARACCOUNT_NAME""" & vbTab & """DEPOSITACCOUNT_NAME""" & vbTab
				strToWrite = strToWrite & """PAYMENTMETHOD_REF""" & vbTab & """TEMPLATE_REF""" & vbTab
				strToWrite = strToWrite & """BADDR_LINE1""" & vbTab & """BADDR_LINE2""" & vbTab & """BADDR_LINE3""" & vbTab & """BADDR_LINE4""" & vbTab
				strToWrite = strToWrite & """BADDR_CITY""" & vbTab & """BADDR_PROVINCE""" & vbTab & """BADDR_STATE""" & vbTab & """BADDR_ZIP""" & vbTab & """BADDR_COUNTRY""" & vbTab
				strToWrite = strToWrite & """Phone""" & vbTab & """Email""" & vbTab & """SADDR_CITY""" & vbTab
				strToWrite = strToWrite & """SADDR_LINE1""" & vbTab & """SADDR_LINE2""" & vbTab & """SADDR_LINE3""" & vbTab & """SADDR_LINE4""" & vbTab
				strToWrite = strToWrite & """SADDR_PROVINCE""" & vbTab & """SADDR_STATE""" & vbTab & """SADDR_ZIP""" & vbTab & """SADDR_COUNTRY""" & vbTab
				strToWrite = strToWrite & """SADDR_PHONE""" & vbTab & """SADDR_EMAIL""" & vbTab & """IS_PENDING""" & vbTab & """PO_NUMBER""" & vbTab & """TERMS_REF""" & vbTab
				strToWrite = strToWrite & """DUE_DT""" & vbTab & """SALESREP""" & vbTab & """FOB""" & vbTab & """SHIP_DT""" & vbTab & """SHIP_METHOD_REF""" & vbTab & """MEMO""" & vbTab
				strToWrite = strToWrite & """CUSTOMER_MSG_REF""" & vbTab & """IS_TO_BE_PRINTED""" & vbTab & """CUSTOMER_SALES_TAX_REF""" & vbTab
				strToWrite = strToWrite & """TAX1_TOTAL""" & vbTab & """TAX2_TOTAL""" & vbTab & """EXCHANGE_RATE""" & vbTab
				strToWrite = strToWrite & """CUST_FIELD1""" & vbTab & """CUST_FIELD2""" & vbTab & """CUST_FIELD3""" & vbTab & """CUST_FIELD4""" & vbTab & """CUST_FIELD5""" & vbTab
				strToWrite = strToWrite & """CUSTOM1""" & vbTab & """CUSTOM2""" & vbTab & """CUSTOM3""" & vbTab & """CUSTOM4""" & vbTab
				strToWrite = strToWrite & """CUSTOM5""" & vbTab & """CUSTOM6""" & vbTab & """CUSTOM7""" & vbTab & """CUSTOM8""" & vbTab
				strToWrite = strToWrite & """CUSTOM9""" & vbTab & """CUSTOM10""" & vbTab & """CUSTOM11""" & vbTab & """CUSTOM12""" & vbTab
				strToWrite = strToWrite & """TOTAL_SHIP_COST""" & vbTab & """TOTAL_HANDLING_COST""" & vbTab & """TOTAL_ORDER_AMT""" & vbTab & """MEMO_PASS_THRU""" & vbTab
				strToWrite = strToWrite & """SRC_TXN_TYPE""" & vbTab & """SRC_PAYMENT_STATUS""" & vbTab & """SRC_PAYER_ID""" & vbTab & """SRC_PAYMENT_TYPE""" & vbTab
				strToWrite = strToWrite & """SRC_BUSINESS_NAME""" & vbTab & """SRC_NOTIFY_VERSION""" & vbTab & """SRC_PAYER_STATUS""" & vbTab & """SRC_RECEIVER_ID""" & vbTab
				strToWrite = strToWrite & """ITEM_NAME""" & vbTab & """ItemName""" & vbTab & """ITEM_DESC""" & vbTab & """ITEM_QUANTITY""" & vbTab & """ITEM_RATE""" & vbTab
				strToWrite = strToWrite & """ITEM_AMOUNT""" & vbTab & """ITEM_CLASS_REF""" & vbTab & """SERVICE_DT""" & vbTab & """ITEM_SALES_TAX_REF""" & vbTab & """ItemWeight""" & vbTab
				strToWrite = strToWrite & """TotalItemWeight""" & vbTab & """Item_Type"""
				file.WriteLine strToWrite
			end if
			
			dim strItemDetails, columnCount
			strItemDetails = ""
			columnCount = 0
			
			if not RS.eof then
				do until RS.eof
					OrderID = RS("OrderID")
					if RS("shippingid") > 0 then
						altShipSQL = "SELECT * FROM we_addl_shipping_addr WHERE id='" & RS("shippingid") & "'"
						session("errorSQL") = altShipSQL
						set altShipRS = Server.CreateObject("ADODB.Recordset")
						altShipRS.open altShipSQL, oConn, 3, 3
						if altShipRS.EOF then
							sAddress1 = RS("sAddress1")
							sAddress2 = RS("sAddress2")
							sCity = RS("sCity")
							sState = RS("sState")
							sZip = RS("sZip")
							if RS("sCountry") = "CANADA" then
								sCountry = "CANADA"
							else
								sCountry = ""
							end if
						else
							sAddress1 = altShipRS("sAddress1")
							sAddress2 = altShipRS("sAddress2")
							sCity = altShipRS("sCity")
							sState = altShipRS("sState")
							sZip = altShipRS("sZip")
							if altShipRS("sCountry") = "CANADA" then
								sCountry = "CANADA"
							else
								sCountry = ""
							end if
						end if
					else
						sAddress1 = RS("sAddress1")
						sAddress2 = RS("sAddress2")
						sCity = RS("sCity")
						sState = RS("sState")
						sZip = RS("sZip")
						if RS("sCountry") = "CANADA" then
							sCountry = "CANADA"
						else
							sCountry = ""
						end if
					end if
					
					' START variables for later
					quantity = cDbl(RS("quantity"))
					itemWeight = cDbl(RS("itemWeight"))
					orderDesc = "Cellphone " & RS("typeName")
					itemSubtotal = quantity * RS("price_our")
					accountID = RS("accountID")
					BarCode = left(sZip,5) & "-WE" & OrderID & vbcrlf
					AddressBlock = RS("fname") & " " & RS("lname") & vbcrlf & sAddress1 & vbcrlf
					if not isNull(sAddress2) and sAddress2 <> "" then AddressBlock = AddressBlock & sAddress2 & vbcrlf
					AddressBlock = AddressBlock & sCity & ", " & sState & "  " & sZip & vbcrlf
					typeID = RS("typeID")
					' END variables for later
					
					shiptype = RS("shiptype")
					select case shiptype
						case "USPS Priority Mail (2-3 days)" : shipCode = 7
						case "USPS Priority" : shipCode = 7
						case "USPS Express" : shipCode = 3
						case "First Class Int'l" : shipCode = 4
						case "USPS Priority Int'l" : shipCode = 5
						case else : shipCode = 1
					end select
					
					if shipname = "PRIO" or shipname = "EXP" or shipname = "FCM-I" or shipname = "AMAZON" or shipname = "BUYCOM" then
						strToWrite = RS("fname") & " " & RS("lname") & vbTab
						for aCount = 1 to 3
							strToWrite = strToWrite & vbTab
						next
						strToWrite = strToWrite & OrderID & vbTab
						for aCount = 1 to 19
							strToWrite = strToWrite & vbTab
						next
						strToWrite = strToWrite & sCity & vbTab
						strToWrite = strToWrite & sAddress1 & vbTab
						strToWrite = strToWrite & sAddress2 & vbTab
						strToWrite = strToWrite & vbTab 'address3
						strToWrite = strToWrite & vbTab
						strToWrite = strToWrite & vbTab
						strToWrite = strToWrite & sState & vbTab
						strToWrite = strToWrite & sZip & vbTab
						strToWrite = strToWrite & sCountry & vbTab
						strToWrite = strToWrite & RS("phone") & vbTab
						strToWrite = strToWrite & RS("email") & vbTab
						for aCount = 1 to 7
							strToWrite = strToWrite & vbTab
						next
						strToWrite = strToWrite & shipCode & vbTab
						if request("submitBuyCom") = "Process Buy.Com Orders" then
							strToWrite = strToWrite & "Buy.com" & vbTab 'MEMO
						elseif request("submitAtomic") = "Process AtomicMall Orders" then
							strToWrite = strToWrite & "AtomicMall.com" & vbTab 'MEMO
						elseif request("submitAmazon") = "Process Amazon Orders" then
							strToWrite = strToWrite & "Amazon.com" & vbTab 'MEMO
						else
							strToWrite = strToWrite & "UNITED STATES" & vbTab 'MEMO
						end if
						for aCount = 1 to 11
							strToWrite = strToWrite & vbTab
						next
					elseif shipname <> "UPS" then
						'do nothing
					end if
					
					holdOrderID = OrderID
					RS.movenext
					if not RS.eof then
						if RS("OrderID") <> holdOrderID or columnCount = 8 then
							orderWeight = orderWeight + (quantity * itemWeight)
							orderQuantity = orderQuantity + quantity
							orderSubtotal = orderSubtotal + itemSubtotal
							if orderWeight < 2 then orderWeight = 2
							if orderQuantity > 1 then
								multiOrder = "MULTI-ITEMS"
							else
								multiOrder = ""
							end if
							if shipname = "PRIO" or shipname = "EXP" or shipname = "FCM-I" or shipname = "AMAZON" or shipname = "BUYCOM" then
								strItemDetails = strItemDetails & quantity & vbTab 'Items
								strItemDetails = strItemDetails & orderDesc & vbTab 'Item Name
								strItemDetails = strItemDetails & quantity * itemWeight & vbTab
								strItemDetails = strItemDetails & itemSubtotal & vbTab 'TOTAL_ORDER_AMT
								strToWrite = strToWrite & strItemDetails
								for aCount = 1 to 8 - columnCount
									strToWrite = strToWrite & vbTab
								next
								strToWrite = strToWrite & multiOrder & vbTab
								strToWrite = strToWrite & orderQuantity & vbTab
								strToWrite = strToWrite & orderWeight & vbTab
								strToWrite = strToWrite & orderSubtotal & vbTab
								for aCount = 1 to 19
									strToWrite = strToWrite & vbTab
								next
								if inStr(strOrdersDupCheck,OrderID) = 0 or shipCode = 4 or shipCode = 5 then
									file.WriteLine strToWrite
									strOrdersDupCheck = strOrdersDupCheck & orderID & ","
								end if
							elseif shipname <> "UPS" then
								if typeID <> 16 then
									if inStr(strOrdersDHL,OrderID) = 0 then
										SQL = "INSERT INTO DHL_temp (orderID,accountid,BarCode,AddressBlock,DateTimeEntd) VALUES ("
										SQL = SQL & "'" & orderID & "',"
										SQL = SQL & "'" & accountid & "',"
										SQL = SQL & "'" & SQLquote(BarCode) & "',"
										SQL = SQL & "'" & SQLquote(AddressBlock) & "',"
										SQL = SQL & "'" & now & "')"
										session("errorSQL") = SQL
										oConn.execute SQL
										strOrdersDHL = strOrdersDHL & orderID & ","
									end if
								end if
							end if
							orderWeight = 0
							orderQuantity = 0
							orderSubtotal = 0
							strItemDetails = ""
							columnCount = 0
						else
							orderWeight = orderWeight + (quantity * itemWeight)
							orderQuantity = orderQuantity + quantity
							strItemDetails = strItemDetails & quantity & vbTab 'Items
							strItemDetails = strItemDetails & orderDesc & vbTab 'Item Name
							strItemDetails = strItemDetails & quantity * itemWeight & vbTab
							strItemDetails = strItemDetails & itemSubtotal & vbTab 'TOTAL_ORDER_AMT
							columnCount = columnCount + 4
						end if
					end if
				loop
				
				orderWeight = orderWeight + (quantity * itemWeight)
				orderQuantity = orderQuantity + quantity
				orderSubtotal = orderSubtotal + itemSubtotal
				if orderWeight < 2 then orderWeight = 2
				if orderQuantity > 1 then
					multiOrder = "MULTI-ITEMS"
				else
					multiOrder = ""
				end if
				if shipname = "PRIO" or shipname = "EXP" or shipname = "FCM-I" or shipname = "AMAZON" or shipname = "BUYCOM" then
					strItemDetails = strItemDetails & orderQuantity & vbTab 'Items
					strItemDetails = strItemDetails & orderDesc & vbTab 'Item Name
					strItemDetails = strItemDetails & quantity * itemWeight & vbTab
					strItemDetails = strItemDetails & itemSubtotal & vbTab 'TOTAL_ORDER_AMT
					strToWrite = strToWrite & strItemDetails
					for aCount = 1 to 8 - columnCount
						strToWrite = strToWrite & vbTab
					next
					strToWrite = strToWrite & multiOrder & vbTab
					strToWrite = strToWrite & orderQuantity & vbTab
					strToWrite = strToWrite & orderWeight & vbTab
					strToWrite = strToWrite & orderSubtotal & vbTab
					for aCount = 1 to 19
						strToWrite = strToWrite & vbTab
					next
					if inStr(strOrdersDupCheck,OrderID) = 0 or shipCode = 4 or shipCode = 5 then
						file.WriteLine strToWrite
						strOrdersDupCheck = strOrdersDupCheck & orderID & ","
					end if
					file.Close()
				elseif shipname <> "UPS" then
					if typeID <> 16 then
						if inStr(strOrdersDHL,OrderID) = 0 then
							SQL = "INSERT INTO DHL_temp (orderID,accountid,BarCode,AddressBlock,DateTimeEntd) VALUES ("
							SQL = SQL & "'" & orderID & "',"
							SQL = SQL & "'" & accountid & "',"
							SQL = SQL & "'" & SQLquote(BarCode) & "',"
							SQL = SQL & "'" & SQLquote(AddressBlock) & "',"
							SQL = SQL & "'" & now & "')"
							oConn.execute SQL
							strOrdersDHL = strOrdersDHL & orderID & ","
						end if
					end if
				end if
				
				SQL = "SELECT A.OrderID,A.shiptype,C.quantity,D.itemWeight,B.accountID,B.fname,B.lname,B.email,B.sCity,B.sAddress1,B.sAddress2,B.sState,B.sZip,B.sCountry"
				SQL = SQL & " FROM ((we_orders A INNER JOIN we_accounts B ON A.accountid=B.accountid)"
				SQL = SQL & " INNER JOIN we_orderdetails C ON C.OrderID=A.OrderID)"
				SQL = SQL & " INNER JOIN we_Items D ON D.itemid=C.itemid"
				SQL = SQL & " WHERE A.orderid IN (" & strOrders & ")"
				SQL = SQL & " ORDER BY A.orderid"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				%>
				<table border="1">
					<tr>
						<%
						for each whatever in RS2.fields
							%>
							<td><b><%=whatever.name%></b></td>
							<%
						next
						%>
					</tr>
					<%
					do until RS2.eof
						%>
						<tr>
							<%
							for each whatever in RS2.fields
								thisfield = whatever.value
								if isnull(thisfield) then
									thisfield = shownull
								end if
								if trim(thisfield) = "" then
									thisfield = showblank
								end if
								%>
								<td valign="top"><%=thisfield%></td>
								<%
							next
							%>
						</tr>
						<%
						RS2.movenext
					loop
					%>
				</table>
				<%
				'if request("qtyOption") = "Multi" or submitMailOption = "Priority" or submitMailOption = "Express" or submitMailOption = "UPS" or request("submitBuyCom") = "Process Buy.Com Orders" or request("submitAtomic") = "Process AtomicMall Orders" then
				if request("submitBuyCom") = "Process Buy.Com Orders" then
					SQL = "SELECT A.orderID,A.accountID,C.typeID FROM we_orders A INNER JOIN we_orderdetails B ON A.orderID=B.orderID"
					SQL = SQL & " INNER JOIN we_Items C ON B.itemID=C.itemID"
					SQL = SQL & " INNER JOIN we_Accounts D ON A.accountID=D.accountID"
					SQL = SQL & " WHERE A.orderID IN (" & strOrders & ")"
					SQL = SQL & " ORDER BY D.fname, D.lname"
				elseif request("qtyOption") = "Multi" or submitMailOption = "Priority" or submitMailOption = "Express" or submitMailOption = "UPS" then
					SQL = "SELECT orderID,accountID FROM we_orders WHERE orderid IN (" & strOrders & ") ORDER BY orderID"
				else
					SQL = "SELECT A.orderID,A.accountID,C.typeID FROM we_orders A INNER JOIN we_orderdetails B ON A.orderID=B.orderID"
					SQL = SQL & " INNER JOIN we_Items C ON B.itemID=C.itemID"
					SQL = SQL & " WHERE A.orderID IN (" & strOrders & ")"
					SQL = SQL & " ORDER BY C.typeID,A.orderID"
				end if
				response.write "<p>" & SQL & "</p>"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3

				filenamePDF = "WE_SalesOrders_" & replace(date,"/","-") & "_" & shipname & ".pdf"
				
				%><!--#include virtual="/admin/ProcessOrders_inc.asp"--><%
				
				if instr(request.ServerVariables("SERVER_NAME"),"staging") < 1 then
					if shipname = "FCM-MULTI" then
						SQL = "UPDATE we_Orders SET thub_posted_to_accounting='M', thub_posted_date='" & now & "', processPDF = '" & filenamePDF & "', processTXT='" & filenameTXT & "' "
						SQL = SQL & " WHERE orderid IN (" & strOrders & ") AND store = " & storeID
						response.write "<p>" & SQL & "</p>"
						oConn.execute(SQL)
					else
						SQL = "UPDATE we_Orders SET thub_posted_to_accounting='W', thub_posted_date='" & now & "', processPDF = '" & filenamePDF & "', processTXT='" & filenameTXT & "' "
						SQL = SQL & " WHERE orderid IN (" & strOrders & ") AND store = " & storeID
						response.write "<p>" & SQL & "</p>"
						oConn.execute(SQL)
						SQL = "UPDATE we_Orders SET thub_posted_to_accounting='W', processPDF = '" & filenamePDF & "', processTXT='" & filenameTXT & "' "
						SQL = SQL & " WHERE thub_posted_to_accounting='M' AND orderid <= '" & request("orderEnd") & "' AND store = " & storeID
						response.write "<p>" & SQL & "</p>"
						oConn.execute(SQL)
					end if
				end if
				
				if shipname = "PRIO" or shipname = "EXP" or shipname = "FCM-I" or shipname = "AMAZON" or shipname = "BUYCOM" then response.write "<p>Here is the <a href=""/admin/tempTXT/" & filenameTXT & """ target=""_blank"">Tarantula Export File</a></p>"
				response.write "<p><a href=""/admin/tempPDF/" & filenamePDF & """ target=""_blank"">PDF Sales Orders</a></p>"
			else
				response.write "<h3>No Records Found</h3>"
			end if
		else
			response.write "<h3>No Records Found</h3>"
		end if
	end if
end if

if strError <> "" or (request("submitted") <> "Process Orders" and request("submitBuyCom") <> "Process Buy.Com Orders" and request("submitAtomic") <> "Process AtomicMall Orders") then
	mailOption = request.querystring("mailOption")
	if mailOption = "" then mailOption = "First Class"
	if mailOption = "Priority" then
		SQL = "SELECT MAX(OrderID) AS OrderStartNumber FROM we_Orders WHERE thub_posted_to_accounting='W'"
		'SQL = SQL & " AND shiptype = 'USPS Priority Mail (2-3 days)'"
		SQL = SQL & " AND shiptype LIKE 'USPS Priority%'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			strOrderStartNumber = RS("OrderStartNumber") + 1
			strOrderEndNumber = 0
		else
			strOrderStartNumber = 999999
			strOrderEndNumber = 0
		end if
	elseif mailOption = "Express" then
		SQL = "SELECT MAX(OrderID) AS OrderStartNumber FROM we_Orders WHERE thub_posted_to_accounting='W'"
		SQL = SQL & " AND shiptype LIKE 'USPS Express%'"
		SQL = SQL & " AND store = " & storeID
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			strOrderStartNumber = RS("OrderStartNumber") + 1
			strOrderEndNumber = 0
		else
			strOrderStartNumber = 999999
			strOrderEndNumber = 0
		end if
	elseif mailOption = "UPS" then
		SQL = "SELECT MAX(OrderID) AS OrderStartNumber FROM we_Orders WHERE thub_posted_to_accounting='W'"
		SQL = SQL & " AND (shiptype LIKE 'UPS Ground%' OR shiptype LIKE 'UPS 3 Day Select%' OR shiptype LIKE 'UPS 2nd Day Air%')"
		SQL = SQL & " AND store = " & storeID
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			strOrderStartNumber = RS("OrderStartNumber") + 1
			strOrderEndNumber = 0
		else
			strOrderStartNumber = 999999
			strOrderEndNumber = 0
		end if
	else
		SQL = "SELECT MAX(OrderID) AS OrderStartNumber FROM we_Orders WHERE thub_posted_to_accounting='W'"
		SQL = SQL & " AND shiptype NOT LIKE 'USPS Priority%' AND shiptype NOT LIKE 'USPS Express%' AND shiptype NOT LIKE 'UPS Ground%' AND shiptype NOT LIKE 'UPS 3 Day Select%' AND shiptype NOT LIKE 'UPS 2nd Day Air%' AND shiptype <> 'First Class Int''l'"
		SQL = SQL & " AND store = " & storeID
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			strOrderStartNumber = RS("OrderStartNumber") + 1
			'strOrderEndNumber = strOrderStartNumber + 51
		else
			strOrderStartNumber = 999999
			'strOrderEndNumber = 0
		end if
		SQL = "SELECT MAX(OrderID) AS OrderEndNumber FROM we_Orders"
		SQL = SQL & " WHERE shiptype NOT LIKE 'USPS Priority%' AND shiptype NOT LIKE 'USPS Express%' AND shiptype NOT LIKE 'UPS Ground%' AND shiptype NOT LIKE 'UPS 3 Day Select%' AND shiptype NOT LIKE 'UPS 2nd Day Air%' AND shiptype <> 'First Class Int''l'"
		SQL = SQL & " AND orderdatetime < '" & date & "'"
		SQL = SQL & " AND store = " & storeID
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			strOrderEndNumber = RS("OrderEndNumber")
		else
			strOrderEndNumber = 0
		end if
	end if
	%>
	<p>
	<form action="ProcessOrders.asp" name="frmProcess" method="post" onSubmit="return checkSubmit(this);">
		<h3>Process Wireless Emporium <font color="red"><%=mailOption%></font> Orders</h3>
		<%if strError <> "" then response.write "<p><font color=""#FF0000""><b>" & strError & "</b></font></p>"%>
		<p>Start Order #:&nbsp;&nbsp;<input type="text" name="orderStart" value="<%=strOrderStartNumber%>"></p>
		<p>End Order #:&nbsp;&nbsp;<input type="text" name="orderEnd" value="<%if strOrderEndNumber > 0 then response.write strOrderEndNumber%>"></p>
		<!--<p>Multi-Item Orders Only:&nbsp;&nbsp;<input type="checkbox" name="qtyOption" value="Multi"></p>-->
		<table width="800" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<input type="hidden" name="mailOption" value="<%=mailOption%>">
					<input type="hidden" name="strOrderStartNumber" value="<%=strOrderStartNumber%>">
					<!--<input type="submit" name="submitted" value="Process Orders"><br>-->
					<!--<input type="submit" name="submitEbill" value="Process eBillme Orders">-->
				</td>
				<td align="right">
					<input type="submit" name="submitAtomic" value="Process AtomicMall Orders"><br>                
                	<!--
					<input type="submit" name="submitBuyCom" value="Process Buy.Com Orders"><br>
					<input type="submit" name="submitAmazon" value="Process Amazon Orders">
                    -->
				</td>
			</tr>
		</table>
	</form>
    <!--
	</p>
	<p>
		<a href="ProcessOrders.asp?mailOption=Priority">Priority&nbsp;Only</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="ProcessOrders.asp?mailOption=Express">Express&nbsp;Only</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="ProcessOrders.asp">First&nbsp;Class</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="ProcessOrders.asp?mailOption=FCM-I">First&nbsp;Class&nbsp;Int'l</a>
	</p>
	<p><a href="ProcessOrders.asp?mailOption=UPS">ALL&nbsp;UPS&nbsp;Only</a></p>
    -->
	<%
end if
set filesys = nothing
%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->

<script language="JavaScript">
	function checkSubmit(frm) {
		var strOrderStartNumber = frm.strOrderStartNumber.value;
		var orderStart = frm.orderStart.value;
		if (orderStart < strOrderStartNumber) {
			var answer = confirm(orderStart + " is less than " + strOrderStartNumber + " ...\nAre you sure you want to proceed?");
			if (answer) {
				return true;
			} else {
				return false;
			}
		}
	}
</script>
