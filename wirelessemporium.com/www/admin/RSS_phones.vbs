dim oConn
set oConn = CreateObject("ADODB.Connection")
oConn.ConnectionString = "DSN=WEDB01;Uid=wireless_user;Pwd=c3llph0n3;"
oConn.ConnectionTimeout = 1000		'seconds
oConn.Open

dim fs, file, path
set fs = CreateObject("Scripting.FileSystemObject")
path = "e:\inetpub\wwwroot\wirelessemporium.com\www\RSS_phones.xml"
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if
set file = fs.CreateTextFile(path)

dim RFC822, myWeekday, myDays, myMonth, myYear, myHours, myMinutes, mySeconds
myWeekday = WeekdayName(Weekday(now),true)
myDays = zeroPad(day(now),2)
myMonth = MonthName(month(now),true)
myYear = year(now)
myHours = zeroPad(hour(now),2)
myMinutes = zeroPad(minute(now),2)
mySeconds = zeroPad(second(now),2)
RFC822 = myWeekday & ", " & myDays & " " & myMonth & " " & myYear & " " & myHours & ":" & myMinutes & ":" & mySeconds & " PST"

file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
file.WriteLine "<rss version=""2.0"" xmlns:atom=""http://www.w3.org/2005/Atom"">"
file.WriteLine "<channel>"
file.WriteLine vbtab & "<atom:link href=""http://www.wirelessemporium.com/RSS_faceplates.xml"" rel=""self"" type=""application/rss+xml"" />"
file.WriteLine vbtab & "<title>WirelessEmporium.com - Top 10 Phones</title>"
file.WriteLine vbtab & "<link>http://www.wirelessemporium.com</link>"
file.WriteLine vbtab & "<description>WirelessEmporium.com - Top 10 Phones</description>"
file.WriteLine vbtab & "<language>en-us</language>"
file.WriteLine vbtab & "<pubDate>" & RFC822 & "</pubDate>"
file.WriteLine vbtab & "<lastBuildDate>" & RFC822 & "</lastBuildDate>"

dim SQL, RS
SQL = "SELECT TOP 10 A.itemID, A.itemDesc, A.itemPic, A.price_Retail, A.price_Our, A.itemLongDetail, B.brandName, C.modelName"
SQL = SQL & " FROM we_Items A INNER JOIN we_Brands B ON A.brandID = B.brandID INNER JOIN we_Models C ON A.modelID = C.modelID"
SQL = SQL & " WHERE A.typeID = 16 AND A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our IS NOT NULL"
SQL = SQL & " ORDER BY A.numberOfSales DESC"
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
do until RS.eof
	file.WriteLine vbtab & "<item>"
	file.WriteLine vbtab & vbtab & "<title><![CDATA[" & switchChars(RS("itemDesc")) & "]]></title>"
	file.WriteLine vbtab & vbtab & "<link>http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp</link>"
	file.WriteLine vbtab & vbtab & "<guid>http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp</guid>"
	file.WriteLine vbtab & vbtab & "<img_link>http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & "</img_link>"
	file.WriteLine vbtab & vbtab & "<sale_price>" & formatCurrency(RS("price_Our")) & "</sale_price>"
	file.WriteLine vbtab & vbtab & "<regular_price>" & formatCurrency(RS("price_Retail")) & "</regular_price>"
	file.WriteLine vbtab & vbtab & "<description><![CDATA[" & switchChars(replace(replace(replace(replace(RS("itemLongDetail"),vbcrlf," "),"?,""),"<p>",""),"<br>","")) & "]]></description>"
	file.WriteLine vbtab & vbtab & "<pubDate>" & RFC822 & "</pubDate>"
	file.WriteLine vbtab & vbtab & "<item_no>CA-" & cDbl(RS("itemid")) + 500 & "</item_no>"
	file.WriteLine vbtab & vbtab & "<brand>" & XMLencode(RS("brandName")) & "</brand>"
	file.WriteLine vbtab & vbtab & "<ManufacturerModel>" & XMLencode(RS("modelName")) & "</ManufacturerModel>"
	file.WriteLine vbtab & "</item>"
	RS.movenext
loop

file.WriteLine "</channel>"
file.WriteLine "</rss>"
file.close()
RS.close
set RS = nothing

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function

function zeroPad(m,t)
	zeroPad = String(t-Len(m),"0") & m
end function

function XMLencode(str)
	XMLencode = replace(replace(replace(replace(replace(str,"&","&amp;"),"<","&lt;"),">","&gt;"),"'","&apos;"),chr(34),"&quot;")
end function

function switchChars(myText)
	myText = replace(myText,chr(150),"&#150;")
	myText = replace(myText,chr(151),"&#151;")
	myText = replace(myText,chr(153),"&#153;")
	myText = replace(myText,chr(169),"&#169;")
	myText = replace(myText,chr(174),"&#174;")
	myText = replace(myText,"?,chr(34))
	myText = replace(myText,"?,chr(34))
	myText = replace(myText,"?,"'")
	myText = replace(myText,"?,"'")
	myText = replace(myText,vbCrLf,"</p><p>")
	switchChars = myText
end function
