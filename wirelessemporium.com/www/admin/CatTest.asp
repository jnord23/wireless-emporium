<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<%
response.buffer = false
Dim oConn
Set oConn = Server.CreateObject("ADODB.Connection")
oConn.ConnectionString = "DSN=WEDB01;Uid=wireless_user;Pwd=c3llph0n3;"
oConn.ConnectionTimeout = 1000		'seconds
oConn.Open

showblank = "&nbsp;"
shownull = "-null-"

SQL = "SELECT typeid, PartNumber, vendor, hidelive, itemDesc FROM we_items"
SQL = SQL & " WHERE DesignType <> 0 AND DesignType <> 4 AND DesignType <> 8 AND DesignType <> 10"
SQL = SQL & " AND typeID <> 3"
SQL = SQL & " ORDER BY DesignType, PartNumber"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
'response.write "<h3>" & SQL & "</h3>" & vbCrLf

if RS.eof then
    response.write "No records matched<br><br>So cannot make table..."
else
	%>
	<h3><%=RS.recordcount%> records found</h3>
	<table border="1">
		<tr>
			<%
			for each whatever in RS.fields
				%>
				<td><b><%=whatever.name%></b></td>
				<%
			next
			%>
		</tr>
		<%
		do until RS.eof
			%>
			<tr>
				<%
				for each whatever in RS.fields
					thisfield = whatever.value
					if isnull(thisfield) then
						thisfield = shownull
					end if
					if trim(thisfield) = "" then
						thisfield = showblank
					end if
					%>
					<td valign="top"><%=thisfield%></td>
					<%
				next
				%>
			</tr>
			<%
			RS.movenext
		loop
		%>
	</table>
	<%
end if

RS.close
Set RS = nothing
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
