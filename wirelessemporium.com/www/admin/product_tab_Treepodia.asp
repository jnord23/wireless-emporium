<%
response.buffer = false
pageTitle = "Create Treepodia Product List"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 2000 'seconds
dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
'Map the file name to the physical path on the server.
filename = "productList_Treepodia.txt"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Create Treepodia Product List</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							response.write("<b>DeleteFile:</b>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='../tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		response.write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	else
		response.write "<br>"
	end if
end sub

sub CreateFile(path)
	dim fs, file
	response.write("<b>CreateFile:</b>")
	set fs = CreateObject("Scripting.FileSystemObject")
	'If the file already exists give an error message.
	if fs.FileExists(path) then
		response.write("File " & filename & " already exists, create aborted.<br>" & vbCrLf)
	else
		response.write("Creating " & filename & ".<br>")
		set file = fs.CreateTextFile(path)
		dim SQL, RS, strline
		SQL	=	"	select	top 50 " & vbcrlf & _
				"			a.itemid, a.partnumber, a.itemdesc, a.itempic, a.price_our, a.itemlongdetail, a.inv_qty, a.upccode, c.brandname	" & vbcrlf & _
				"		,	a.bullet1, a.bullet2, a.bullet3, a.bullet4, a.bullet5, a.bullet6, a.bullet7, a.bullet8, a.bullet9, a.bullet10	" & vbcrlf & _
				"		,	a.point1, a.point2, a.point3, a.point4, a.point5, a.point6, a.point7, a.point8, a.point9, a.point10, t.typename	" & vbcrlf & _
				"	from	we_items a with (nolock) join we_types t with (nolock)	" & vbcrlf & _
				"		on	a.typeid = t.typeid left outer join 	" & vbcrlf & _
				"			(	" & vbcrlf & _
				"			select	a.partnumber orphan_partnumber	" & vbcrlf & _
				"			from	(	" & vbcrlf & _
				"					select	partnumber	" & vbcrlf & _
				"						,	sum(case when inv_qty > 0 then 1 else 0 end) nMaster	" & vbcrlf & _
				"						,	sum(case when inv_qty < 0 then 1 else 0 end) nSlave	" & vbcrlf & _
				"					from	we_items a with (nolock) 	" & vbcrlf & _
				"					where	hidelive = 0 and inv_qty <> 0 and price_our > 0	" & vbcrlf & _
				"						and	typeid <> 16	" & vbcrlf & _
				"					group by partnumber	" & vbcrlf & _
				"					) a	" & vbcrlf & _
				"			where	a.nMaster = 0	" & vbcrlf & _
				"			) b	" & vbcrlf & _
				"		on	a.partnumber = b.orphan_partnumber join we_brands c with (nolock) " & vbcrlf & _
				"		on	a.brandid = c.brandid " & vbcrlf & _				
				"	where	a.hidelive = 0 and a.inv_qty <> 0	" & vbcrlf & _
				"		and a.price_our > 0	" & vbcrlf & _
				"		and a.typeid <> 16	" & vbcrlf & _
				"		and	b.orphan_partnumber is null	" & vbcrlf & _
				"	order by a.numberofsales desc	" & vbcrlf				

		set RS = Server.CreateObject("ADODB.Recordset")
		dim queryTimeStart, queryTimeEnd
'		response.write "<pre>" & SQL & "</pre>"
'		response.end
		queryTimeStart = now
		RS.open SQL, oConn, 3, 3
		queryTimeEnd = now
		
		response.write "<p><b>Query</b> elapsed time:" & datediff("s", queryTimeStart, queryTimeEnd) & " second(s)<br>"

		if not RS.eof then
			fileWriteTimeStart = now
			heading = 			"Manufacturer" & vbtab & "Manufacturer Part #" & vbtab & "Product Name" & vbtab
			heading = heading & "Product Description" & vbtab & "Click-Out URL" & vbtab & "Price" & vbtab & "Category: Other Format" & vbtab
			heading = heading & "Category: Treepodia Numeric ID" & vbtab & "Image URL" & vbtab & "Ground Shipping" & vbtab & "Stock Status" & vbtab
			heading = heading & "Product Condition" & vbtab & "Marketing Message" & vbtab & "Weight" & vbtab & "Cost-per-Click" & vbtab & "UPC" & vbtab
			heading = heading & "Distributor ID" & vbtab & "MUZE ID" & vbtab & "Features" & vbtab & "Category" & vbTab
			heading = heading & "Image URL(Thumb)" & vbTab & "Image URL(AltView1)" & vbTab & "Image URL(AltView2)" & vbTab & "Image URL(AltView3)"
			file.WriteLine heading
			
			do until RS.eof
				strline = RS("brandName") & vbtab
				strline = strline & "WE-" & RS("itemID") & vbtab
				strline = strline & RS("itemDesc") & vbtab
				strline = strline & replace(RS("itemLongDetail"), vbcrlf, " ") & vbtab
				strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?refer=treepodia" & vbtab
				strline = strline & RS("price_our") & vbtab
				strline = strline & vbtab
				strline = strline & "500070 : Electronics / Phones & Communications / Cell Phone Accessories" & vbtab
				strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab
				strline = strline & "0" & vbtab
				strline = strline & "Yes" & vbtab
				strline = strline & "New" & vbtab
				strline = strline & "Free Shipping on All Orders" & vbtab
				strline = strline & vbtab
				strline = strline & vbtab
				strline = strline & RS("UPCCode") & vbtab
				strline = strline & vbtab
				strline = strline & vbtab

				'features
				if isnull(RS("bullet1")) or len(RS("bullet1")) < 1 then
					for i = 1 to 10
						if i = 1 and not isnull(RS("point" & i)) and len(RS("point" & i)) > 0 then
							strline = strline & RS("point" & i)
						elseif not isnull(RS("point" & i)) and len(RS("point" & i)) > 0 then
							strline = strline & ";" & RS("point" & i)
						end if
					next
					strline = strline & vbtab
				else
					for i = 1 to 10
						if i = 1 and not isnull(RS("bullet" & i)) and len(RS("bullet" & i)) > 0 then
							strline = strline & RS("bullet" & i)
						elseif not isnull(RS("bullet" & i)) and len(RS("bullet" & i)) > 0 then
							strline = strline & ";" & RS("bullet" & i)
						end if
					next
					strline = strline & vbtab
				end if
				
				strline = strline & RS("typename") & vbtab
				strline = strline & "http://www.wirelessemporium.com/productpics/thumb/" & RS("itemPic") & vbtab
				
				for iCount = 0 to 2
					src = ""
					path = "e:\inetpub\wwwroot\wirelessemporium.com\productpics\AltViews\" & replace(RS("itemPic"),".jpg","-" & iCount & ".jpg")
					if fs.FileExists(path) then
						src = "http://www.wirelessemporium.com" & replace(replace(path,"e:\inetpub\wwwroot\wirelessemporium.com",""),"\","/")
					end if

					path = "e:\inetpub\wwwroot\wirelessemporium.com\productpics\AltViews\" & replace(RS("itemPic"),".jpg","-" & iCount & ".gif")
					if fs.FileExists(path) then
						src = "http://www.wirelessemporium.com" & replace(replace(path,"e:\inetpub\wwwroot\wirelessemporium.com",""),"\","/")
					end if

					path = "e:\inetpub\wwwroot\wirelessemporium.com\productpics\AltViews\" & replace(RS("itemPic"),".gif","-" & iCount & ".jpg")
					if fs.FileExists(path) then
						src = "http://www.wirelessemporium.com" & replace(replace(path,"e:\inetpub\wwwroot\wirelessemporium.com",""),"\","/")
					end if
					
					path = "e:\inetpub\wwwroot\wirelessemporium.com\productpics\AltViews\" & replace(RS("itemPic"),".gif","-" & iCount & ".gif")
					if fs.FileExists(path) then
						src = "http://www.wirelessemporium.com" & replace(replace(path,"e:\inetpub\wwwroot\wirelessemporium.com",""),"\","/")
					end if
					
					strline = strline & src & vbtab
				next
				
				file.WriteLine strline

				RS.movenext
			loop
			fileWriteTimeEnd = now
			response.write "<b>FileWrite</b> elapsed time:" & datediff("n", fileWriteTimeStart, fileWriteTimeEnd) & "min, " & datediff("s", fileWriteTimeStart, fileWriteTimeEnd) mod 60 & " second(s)<p>"
		end if
		RS.close
		set RS = nothing
		file.Close()
	end if
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
