<%
	thisUser = Request.Cookies("username")
	pageTitle = ""
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="/Scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
<script src="/Scripts/jquery-ui-1.8.11.min.js" type="text/javascript"></script>
<%
	dim sDate : sDate = prepStr(request.Form("sDate"))
	dim eDate : eDate = prepStr(request.Form("eDate"))
	dim catID : catID = prepInt(request.Form("catID"))
	dim brandID : brandID = prepInt(request.Form("brandID"))
	dim modelID : modelID = prepInt(request.Form("modelID"))
	dim sortBy : sortBy = prepStr(request.Form("sortBy"))
	dim showReport : showReport = 0
	
	if sDate = "" or eDate = "" then
		showReport = 0
	else
		showReport = 1
	end if
	
	if sDate = "" then sDate = date-7 else sDate = cdate(sDate)
	if eDate = "" then eDate = date+1 else eDate = cdate(eDate)+1
	
	if showReport = 1 then
		sql =	"select c.PartNumber, c.itemDesc, d.modelID, d.modelName, e.typeID, e.typeName, h.entryDate, f.brandID, g.vendorCode, g.orderAmt, g.receivedAmt, g.process, g.processOrderDate, g.complete, g.entryDate as orderDate " &_
				"from (select distinct itemID from we_invRecord where entryDate > '" & sDate & "' and entryDate < '" & eDate & "') a " &_
					"left join we_Items b on a.itemID = b.itemID " &_
					"left join we_Items c on b.PartNumber = c.PartNumber and c.master = 1 " &_
					"left join we_Models d on b.modelID = d.modelID " &_
					"left join we_Types e on b.typeID = e.typeID " &_
					"left join we_Brands f on b.brandID = f.brandID " &_
					"outer apply ( " &_
						"select top 1 vendorCode, orderAmt, receivedAmt, process, processOrderDate, complete, entryDate " &_
						"from we_vendorOrder " &_
						"where partNumber = c.PartNumber " &_
						"order by id desc " &_
					") g " &_
					"outer apply ( " &_
						"select top 1 entryDate " &_
						"from we_invRecord " &_
						"where itemID = a.itemID " &_
						"order by id desc " &_
					") h " &_
				"where c.inv_qty < 1 "
				
		if catID > 0 then sql = replace(sql,"c.inv_qty < 1","c.inv_qty < 1 and e.typeID = " & catID)
		if brandID > 0 then sql = replace(sql,"c.inv_qty < 1","c.inv_qty < 1 and f.brandID = " & brandID)
		if modelID > 0 then sql = replace(sql,"c.inv_qty < 1","c.inv_qty < 1 and d.modelID = " & modelID)
		if sortBy = "" then
			sql = sql & "order by e.typeID, d.modelID, c.PartNumber"
		else
			sql = sql & "order by " & sortBy
		end if
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
	end if
	
	sql = "select typeID, typeName from we_types order by typeName"
	session("errorSQL") = sql
	set categoryRS = oConn.execute(sql)
	
	sql = "select brandID, brandName from we_brands order by brandName"
	session("errorSQL") = sql
	set brandRS = oConn.execute(sql)
%>
<form action="/admin/report_outOfStock.asp" method="post" style="padding:0px; margin:0px;" name="searchForm">
<div style="margin-left:auto; margin-right:auto; height:30px; width:300px; margin-top:20px;">
    <div style="float:left; width:100px; text-align:right; font-weight:bold;">Start Date:</div>
	<div style="float:left; padding-left:10px; position:relative;" id="calendar1">
    	<input type="text" id="sDate" name="sDate" value="<%=sDate%>" size="8" />
        <div style="position:absolute; right:-150px; top:0px;" id="sDateCalendar"></div>
    </div>
</div>
<div style="margin-left:auto; margin-right:auto; height:30px; width:300px;">
    <div style="float:left; width:100px; text-align:right; font-weight:bold;">End Date:</div>
    <div style="float:left; padding-left:10px; position:relative;" id="calendar2">
    	<input type="text" id="eDate" name="eDate" value="<%=eDate-1%>" size="8" />
        <div style="position:absolute; right:-150px; top:0px;" id="eDateCalendar"></div>
    </div>
</div>
<div style="margin-left:auto; margin-right:auto; height:30px; width:300px;">
    <div style="float:left; width:100px; text-align:right; font-weight:bold;">Category:</div>
	<div style="float:left; padding-left:10px;">
    	<select name="catID">
        	<option value="0">All</option>
            <%
			do while not categoryRS.EOF
			%>
            <option value="<%=categoryRS("typeID")%>"<% if catID = prepInt(categoryRS("typeID")) then %> selected="selected"<% end if %>><%=categoryRS("typeName")%></option>
            <%
				categoryRS.movenext
			loop
			%>
        </select>
    </div>
</div>
<div style="margin-left:auto; margin-right:auto; height:30px; width:300px;">
    <div style="float:left; width:100px; text-align:right; font-weight:bold;">Brand:</div>
	<div style="float:left; padding-left:10px;">
    	<select name="brandID" onchange="brandSelected(this.value)">
        	<option value="0">All</option>
            <%
			do while not brandRS.EOF
			%>
            <option value="<%=brandRS("brandID")%>"<% if brandID = prepInt(brandRS("brandID")) then %> selected="selected"<% end if %>><%=brandRS("brandName")%></option>
            <%
				brandRS.movenext
			loop
			%>
        </select>
    </div>
</div>
<div style="margin-left:auto; margin-right:auto; height:30px; width:300px;">
    <div style="float:left; width:100px; text-align:right; font-weight:bold;">Model:</div>
	<div style="float:left; padding-left:10px; text-align:center;" id="modelSelect">Select Brand First</div>
</div>
<div style="margin-left:auto; margin-right:auto; height:30px; width:300px;">
    <div style="float:left; width:150px; text-align:right;"><input type="button" name="myAction" value="Pull Products" onclick="pullReport()" /></div>
    <% if showReport = 1 then %>
    <div style="float:left; width:150px; text-align:right;"><input type="button" name="myAction" value="Download CSV" onclick="window.location='/admin/tempCSV/oos_<%=brandID%>_<%=modelID%>_<%=categoryID%>.csv'" /></div>
    <% end if %>
</div>
<input type="hidden" name="sortBy" value="<%=sortBy%>" />
</form>
<div id="reportTable" style="font-size:24px; color:#000; font-weight:bold; text-align:center; margin-left:auto; margin-right:auto;">
<%
if showReport = 1 then
	set fs = CreateObject("Scripting.FileSystemObject")
	if fs.fileExists(Server.MapPath("/admin/tempCSV") & "\oos_" & brandID & "_" & modelID & "_" & categoryID & ".csv") then
		fs.deleteFile(Server.MapPath("/admin/tempCSV") & "\oos_" & brandID & "_" & modelID & "_" & categoryID & ".csv")
	end if
	filename = "oos_" & brandID & "_" & modelID & "_" & categoryID & ".csv"
	path = Server.MapPath("/admin/tempCSV") & "\" & filename
	set file = fs.CreateTextFile(path, true)
	
	strToWrite = strToWrite & chr(34) & "PartNumber" & chr(34) & ","
	strToWrite = strToWrite & chr(34) & "ItemName" & chr(34) & ","
	strToWrite = strToWrite & chr(34) & "Model" & chr(34) & ","
	strToWrite = strToWrite & chr(34) & "Category" & chr(34) & ","
	strToWrite = strToWrite & chr(34) & "Details" & chr(34)
	file.WriteLine strToWrite
%>
<table border="0" cellpadding="3" cellspacing="0" align="center" style="font-size:12px; font-weight:normal;">
	<tr style="background-color:#000; color:#FFF; font-weight:bold;">
		<td style="cursor:pointer;" onclick="sortBy('c.PartNumber')">Part Number</td>
    	<td style="cursor:pointer;" onclick="sortBy('c.itemDesc')">Item Name</td>
    	<td style="cursor:pointer;" onclick="sortBy('d.modelName')">Model</td>
        <td style="cursor:pointer;" onclick="sortBy('e.typeName')">Category</td>
        <td></td>
    </tr>
	<%
	bgColor = "#fff"
	itemList = ""
	itemCnt = 0
	if rs.EOF then
	%>
    <tr><td colspan="5" align="center">No OOS Products Found</td></tr>
    <%
	end if
    do while not rs.EOF
		itemCnt = itemCnt + 1
		partNumber = prepStr(rs("partNumber"))
		itemDesc = prepStr(rs("itemDesc"))
		modelName = prepStr(rs("modelName"))
		category = prepStr(rs("typeName"))
		soldOutDate = prepStr(rs("entryDate"))
		orderAmt = prepInt(rs("orderAmt"))
		receivedAmt = prepInt(rs("receivedAmt"))
		lastOrderDate = prepStr(rs("orderDate"))
		lastReceivedDate = prepStr(rs("processOrderDate"))
		processed = rs("process")
		itemList = 	itemList & partNumber & "##" & itemDesc & "##" & modelName & "##" & category & "##" & soldOutDate &_
					"##" & orderAmt & "##" & receivedAmt & "##" & lastOrderDate & "##" & lastReceivedDate & "##" & processed & "##"
		
		strToWrite = chr(34) & partNumber & chr(34) & ","
		strToWrite = strToWrite & chr(34) & itemDesc & chr(34) & ","
		strToWrite = strToWrite & chr(34) & modelName & chr(34) & ","
		strToWrite = strToWrite & chr(34) & category & chr(34) & ","
		strToWrite = strToWrite & chr(34) & "Sold Out: " & dateOnly(soldOutDate) & chr(34)
		if isDate(lastOrderDate) then
			if not processed then
                strToWrite = strToWrite & " (On Order: " & orderAmt & " Units on " & dateOnly(lastOrderDate) & ")"
            else
                strToWrite = strToWrite & " (Building Order: " & orderAmt & " Units)"
            end if
        elseif isDate(lastReceivedDate) then
            strToWrite = strToWrite & " (Last Received: " & receivedAmt & " Units on " & dateOnly(lastReceivedDate) & ")"
        else
            strToWrite = strToWrite & " (No Order History)"
		end if
		
		file.WriteLine strToWrite
    %>
	<tr style="background-color:<%=bgColor%>; color:#000;">
		<td nowrap="nowrap" style="border-right:1px dotted #CCC; width:100px;"><%=partNumber%></td>
    	<td style="border-right:1px dotted #CCC; width:400px;"><%=itemDesc%></td>
    	<td nowrap="nowrap" style="border-right:1px dotted #CCC; width:50px;"><%=modelName%></td>
        <td nowrap="nowrap" style="border-right:1px dotted #CCC; width:50px;"><%=category%></td>
        <td style="width:250px;">
        	<div>Sold Out: <%=dateOnly(soldOutDate)%></div>
            <% if isDate(lastOrderDate) then %>
            	<% if not processed then %>
                <div style="color:#060; font-weight:bold;">On Order: <%=orderAmt%> Units on <%=dateOnly(lastOrderDate)%></div>
                <% else %>
                <div style="color:#990; font-weight:bold;">Building Order: <%=orderAmt%> Units</div>
                <% end if %>
            <% elseif isDate(lastReceivedDate) then %>
            	<div style="color:#F00; font-weight:bold;">Last Received: <%=receivedAmt%> Units on <%=dateOnly(lastReceivedDate)%></div>
            <% else %>
            No Order History
			<% end if %>
        </td>
    </tr>
	<%
        rs.movenext
		if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
    loop
	
	file.close
	set file = nothing
	set fs = nothing
    %>
</table>
<% end if %>
</div>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	//ajax('/ajax/calendar.asp?formName=calendar1&defaultDate=-7','calendar1')
	//ajax('/ajax/calendar.asp?formName=calendar2&defaultDate=0','calendar2')
	
	<% if brandID > 0 then %>
		document.getElementById("modelSelect").innerHTML = "Loading Models..."
		<% if modelID > 0 then %>
			ajax('/ajax/buildModelSelect.asp?brandID=<%=brandID%>&preSelect=<%=modelID%>','modelSelect')
		<% else %>
			ajax('/ajax/buildModelSelect.asp?brandID=<%=brandID%>','modelSelect')
		<% end if %>
	<% end if %>
	
	function brandSelected(brandID) {
		document.getElementById("modelSelect").innerHTML = "Loading Models..."
		if (brandID == 0) {
			document.getElementById("modelSelect").innerHTML = "Select Brand First"
		}
		else {
			ajax('/ajax/buildModelSelect.asp?brandID=' + brandID,'modelSelect')
		}
	}
	
	function pullReport() {
		document.getElementById('reportTable').innerHTML='Processing Data<br>Please Wait'
		document.searchForm.submit()
	}
	
	function sortBy(columnName) {
		document.searchForm.sortBy.value = columnName
		document.searchForm.submit()
	}
	
	function showCalendar(activeDate,displayBox) {
		if (displayBox == "sDateCalendar") { document.getElementById("eDateCalendar").innerHTML = "" }
		if (displayBox == "eDateCalendar") { document.getElementById("sDateCalendar").innerHTML = "" }
		ajax('/ajax/calendar.asp?activeDate=' + activeDate + '&displayBox=' + displayBox,displayBox)
	}
	
	function calendarDaySelect(displayBox,newDate,inputName) {
		document.getElementById(displayBox).innerHTML = ""
		eval("document.searchForm." + inputName + ".value = '" + newDate + "'")
	}
	
	$(function () {
        $("#sDate").datepicker();
        $("#eDate").datepicker();
    });
</script>