<%
pageTitle = "WE Admin Site - Phone Orders"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<script language="javascript">
	function printinvoice(orderid,accountid) {
		var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
		window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	}
</script>

<table border="0" cellpadding="10" cellspacing="0" align="center" width="860"><tr><td width="100%">

<%
if request.form("submitted") <> "" then
	dateStart = request.form("dateStart")
	dateEnd = request.form("dateEnd")
	if not isDate(dateStart) then dateStart = date()
	if not isDate(dateEnd) then dateEnd = date()
	dateStart = dateValue(dateStart)
	dateEnd = dateValue(dateEnd)
	if strError = "" then
		SQL = "SELECT A.orderID,A.accountID,A.orderdatetime,A.PhoneOrder,B.fname,B.lname"
		SQL = SQL & " FROM we_orders A INNER JOIN we_AdminUsers B ON A.PhoneOrder=B.adminID"
		SQL = SQL & " WHERE A.orderdatetime >= '" & dateStart & "' AND A.orderdatetime < '" & dateAdd("D",1,dateEnd) & "'"
		SQL = SQL & " AND A.PhoneOrder IS NOT NULL"
		SQL = SQL & " ORDER BY A.orderdatetime"
		Set RS = Server.CreateObject("ADODB.Recordset")
		'response.write "<h3>" & SQL & "</h3>" & vbCrLf
		RS.open SQL, oConn, 3, 3
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			response.write "<h3>PHONE ORDERS FOR: " & dateStart & " to " & dateEnd & "</h3>"
			%>
			<table border="1" cellpadding="5" cellspacing="0">
				<tr>
					<td align="center"><b>Order&nbsp;ID</b></td>
					<td align="left"><b>Order&nbsp;Date/Time</b></td>
					<td align="left"><b>Entered&nbsp;By</b></td>
				</tr>
				<%
				do until RS.eof
					%>
					<tr>
						<td valign="top" align="center"><a href="javascript:printinvoice('<%=RS("orderID")%>','<%=RS("accountID")%>');"><%=RS("orderID")%></a></td>
						<td valign="top" align="left"><%=RS("orderdatetime")%></td>
						<td valign="top" align="left"><%=RS("fname") & "&nbsp;" & RS("lname")%></td>
					</tr>
					<%
					RS.movenext
				loop
				%>
			</table>
			<%
		end if
		
		RS.close
		set RS = nothing
	else
		%>
		<center>
		<font size="+2" color="red"><%=strError%></font>
		<a href="javascript:history.back();">BACK</a>
		</center>
		<%
	end if
end if
%>
<p>&nbsp;</p>
<h3>Choose date range:</h3>
<br>
<form action="report_PhoneOrders.asp" method="post">
	<p><input type="text" name="dateStart" value="<%=dateStart%>">&nbsp;&nbsp;Start&nbsp;Date</p>
	<p><input type="text" name="dateEnd" value="<%=dateEnd%>">&nbsp;&nbsp;End&nbsp;Date</p>
	<p><input type="submit" name="submitted" value="Generate Report"></p>
</form>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
