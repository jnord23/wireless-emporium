<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Upload Newgistics Manifest"
	header = 1
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/admin/formatNotes.asp"-->
<style>
.mouseover	{ background-color:#9BCEFF;}
</style>
<%
set fs = CreateObject("Scripting.FileSystemObject")

adminUserID = session("adminID")
if isnull(adminUserID) or adminUserID = "" then
	adminUserID = 0
end if

strMonth 	= right("00" & month(date), 2)
strDay 		= right("00" & day(date), 2)
strYear		= year(date)
strHour		= right("00" & hour(now), 2)
strMinute	= right("00" & minute(now), 2)
strDate		= strMonth & strDay & strYear & strHour & strMinute
customerID	= "1413"
inductionSiteID = "06"

strSDate	= date
strEDate	= date + 1

filename = customerID & inductionSiteID & strDate & ".man"
filePath = "c:\Inetpub\wwwroot\wirelessemporium.com\www\tempCSV\newgistics\"
path = filePath & filename

remoteFileName = filename
	
set ftp = CreateObject("Chilkat.Ftp2")
success = ftp.UnlockComponent("WIRELEFTP_GQ2TEoLE9Bnh")
if success <> 1 then 
	response.write "ftp.UnlockComponent ERROR - " & ftp.LastErrorText
	response.end
end if

ftp.Hostname = "ftp.newgistics.com"
ftp.Username = "FTPWirelessEmporium"
ftp.Password = "3C41e.br25"
'ftp.Passive = 1

success = ftp.Connect()
if success <> 1 then 
	response.write "ftp connection error - " & ftp.LastErrorText
	response.end		
end if

ftp.ChangeRemoteDir("write/Manifests")
'response.write "ftp.GetCurrentRemoteDir():" & ftp.GetCurrentRemoteDir()

if request.form("upload") <> "" then
	'============================================= generate manifest file start
	if fs.FileExists(path) then
		fs.DeleteFile(path)
	end if
	
	'Create the file and write some data to it.
	set file = fs.CreateTextFile(path)
	
	'log with isuploaded = 0
	sql = 	"insert into we_newgistics_upload(orderid, scandate)" & vbcrlf & _
			"select	a.orderid, a.scandate" & vbcrlf & _
			"from	we_orders a join v_accounts b" & vbcrlf & _
			"	on	a.store = b.site_id and a.accountid = b.accountid left outer join we_newgistics_upload c" & vbcrlf & _
			"	on	a.orderid = c.orderid" & vbcrlf & _
			"where	a.scandate >= '" & strSDate & "'" & vbcrlf & _
			"	and	a.scandate < '" & strEDate & "'" & vbcrlf & _
			"	and	a.approved = 1" & vbcrlf & _
			"	and	(a.cancelled = 0 or a.cancelled is null)" & vbcrlf & _
			"	and	a.trackingnum = ''" & vbcrlf & _
			"	and	a.shiptype like 'First Class%'	" & vbcrlf & _
			"	and	c.orderid is null" & vbcrlf & _
			"	and	a.store not in (0,2)" & vbcrlf & _
			"order by 1" & vbcrlf
	oConn.execute(sql)
	
	'generate file from the whatever last log's max orderid
	sql = 	"select	x.newgistics_customerid, a.orderid, a.accountid, a.shippingid, a.shiptype, a.trackingnum, a.scandate, convert(varchar(10), scandate, 20) scandate2" & vbcrlf & _
			"	,	isnull(b.sAddress1, '') sAddress1, isnull(b.sAddress2, '') sAddress2, isnull(b.sCity, '') sCity, isnull(b.sState, '') sState, isnull(b.sZip, '') sZip" & vbcrlf & _
			"	,	case when b.sCountry = 'canada' then 'CAN' else 'USA' end sCountry" & vbcrlf & _
			"	,	isnull(s.sAddress1, '') ssAddress1, isnull(s.sAddress2, '') ssAddress2, isnull(s.sCity, '') ssCity, isnull(s.sState, '') ssState, isnull(s.sZip, '') ssZip" & vbcrlf & _
			"	,	case when s.sCountry = 'canada' then 'CAN' else 'USA' end ssCountry" & vbcrlf & _
			"	,	isnull(b.fname, '') fname, isnull(b.lname, '') lname, isnull(b.phone, '') phone, isnull(b.email, '') email 	" & vbcrlf & _
			"from	we_orders a join xstore x" & vbcrlf & _
			"	on	a.store = x.site_id join v_accounts b" & vbcrlf & _
			"	on	a.store = b.site_id and a.accountid = b.accountid left outer join we_addl_shipping_addr s" & vbcrlf & _
			"	on	a.shippingid = s.id" & vbcrlf & _
			"where	a.scandate >= ( select max(scandate) from we_newgistics_upload where isUploaded = 1 )" & vbcrlf & _
			"	and	a.approved = 1" & vbcrlf & _
			"	and	(a.cancelled = 0 or a.cancelled is null)" & vbcrlf & _
			"	and	a.trackingnum = ''" & vbcrlf & _
			"	and	a.shiptype like 'First Class%'" & vbcrlf & _
			"	and	a.store <> 0" & vbcrlf
	set rs = CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	if not rs.eof then
		do until rs.eof
			newgistics_customerid = rs("newgistics_customerid")
			orderid = rs("orderid")
			trackingnumber = rs("orderid")
			scandate = rs("scandate")
			scandate2 = rs("scandate2")
			customerName = replace(rs("fname") & " " & rs("lname"), ",", "")
			phone = rs("phone")
			email = rs("email")
			if rs("shippingid") > 0 and not isnull(rs("ssAddress1")) then
				sAddress1 = replace(rs("ssAddress1"), ",", "")
				sAddress2 = replace(rs("ssAddress2"), ",", "")
				sCity = replace(rs("ssCity"), ",", "")
				sState = replace(rs("ssState"), ",", "")
				sZip = replace(left(rs("ssZip"), 5), ",", "")
				sCountry = replace(rs("ssCountry"), ",", "")
			else
				sAddress1 = replace(rs("sAddress1"), ",", "")
				sAddress2 = replace(rs("sAddress2"), ",", "")
				sCity = replace(rs("sCity"), ",", "")
				sState = replace(rs("sState"), ",", "")
				sZip = replace(left(rs("sZip"), 5), ",", "")
				sCountry = replace(rs("sCountry"), ",", "")
			end if
	
			strline = 			"1.3,"										'version
			strline = strline & newgistics_customerid & ","					'account number
			strline = strline & scandate2 & ","								'Mailing/Trailer Manifest Number
			strline = strline & ","											'Carrier Trailer/Pro#
			strline = strline & ","											'Carrier Trailer ETA
			strline = strline & ","											'Carrier Code
			strline = strline & inductionSiteID & ","						'Induction Site
			strline = strline & orderid & ","								'Package ID
			strline = strline & trackingnumber & ","						'tracking number
			strline = strline & scandate & ","								'DateTime
			strline = strline & "0,"										'Weight
			strline = strline & "92867,"									'Client Origin ZIP 5
			strline = strline & ","											'Ship To Attn
			strline = strline & replace(customerName, "|", "") & ","		'Ship To Name
			strline = strline & replace(sAddress1, "|", "") & ","			'Ship To Address 1
			strline = strline & replace(sAddress2, "|", "") & ","			'Ship To Address 2
			strline = strline & ","											'Ship To Address 3
			strline = strline & ","											'Ship To PO Box
			strline = strline & sCity & ","									'Ship To City
			strline = strline & sState & ","								'Ship To State
			strline = strline & sZip & ","									'Ship To ZIP5
			strline = strline & ","											'Ship To ZIP4
			strline = strline & sCountry & ","								'Ship To Country
			strline = strline & phone & ","									'Recipient Phone Number
			strline = strline & email & ","									'Recipient Email
			strline = strline & orderid & ","								'Shipment Reference1
			strline = strline & ","											'Shipment Reference2
			strline = strline & "0,"										'Delivery Confirmation
			strline = strline & "0,"										'Signature Confirmation
			strline = strline & ","											'Saturday Delivery
			strline = strline & ","											'Sunday Delivery
			strline = strline & ","											'Non Machinable
			strline = strline & ","											'Balloon Rate	
			strline = strline & ","											'Oversize
			strline = strline & ","											'Hazmat
			strline = strline & ","											'Packaging Type	
			strline = strline & ","											'COD Flag
			strline = strline & ","											'COD Amount	
			strline = strline & ","											'Payment Type
			strline = strline & ","											'Payment Detail
			strline = strline & ","											'Insurance Flag
			strline = strline & ","											'Insurance Fee
			strline = strline & ","											'Declared Value
			strline = strline & ","											'Charges
			strline = strline & "1003,"										'Service Code
			strline = strline & "3,"										'Processing Category
			strline = strline & ","											'Length
			strline = strline & ","											'Width
			strline = strline & ","											'Height
			strline = strline & ","											'Misc1
			strline = strline & ","											'Misc2
			
			file.WriteLine strline
			
			rs.movenext
		loop
	end if
	file.close()

'	response.write path & "<br>"
'	response.write ftp.GetCurrentRemoteDir() & remoteFileName & "<br>"	

	success = ftp.PutFile(path, ftp.GetCurrentRemoteDir() & "/" & remoteFileName)
	if success <> 1 then 
		response.write "ftp connection error - " & ftp.LastErrorText
		response.end		
	end if
	
	'mark as uploaded
	oConn.execute("update we_newgistics_upload set isuploaded = 1, manifestFile = '" & filename & "', adminID = '" & adminUserID & "' where isuploaded = 0")
	
	'============================================= generate manifest file end
end if
%>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="800">
	<tr>
    	<td><h1 style="margin:0px;">Upload Newgistics Manifest File to FTP</h1></td>
    </tr>
    <tr>
    	<td style="font-size:12px; border-bottom:1px solid #000; padding-bottom:10px;">
            <ul>
	            <li>This application allows the user to upload newgistics manifest file to their FTP server</li>
            </ul>
        </td>
    </tr>
    <tr>
    	<td class="tab-header-off" >
			<table border="0" cellpadding="5" cellspacing="0" align="center" style="font-weight:bold; font-size:11px;">
            	<tr>
                	<td width="300" align="center"><b>Uploaded Manifest Files on the FTP server</b></td>
                	<td width="200" align="center"><b>Date uploaded</b></td>
                    <td width="200" align="center"><b>number of orders</b></td>
                    <td width="100" align="center"><b>admin User</b></td>
				</tr>
			</table>
		</td>
	</tr>
    <tr>
    	<td style="border-bottom:1px soild #000;">
        	<div style="height:200px; overflow:scroll;">
			<table border="0" cellpadding="5" cellspacing="0" align="center" style="font-size:11px;">
            <%
			numFiles = ftp.NumFilesAndDirs
			for i = 0 To numFiles - 1
				if ftp.GetIsDirectory(i) <> 1 then
					sql = 	"select	isnull(convert(varchar(16), max(a.entrydate), 20), 'N/A') entryDate, count(*) numOrders, (b.fname + ' ' + b.lname) adminUser" & vbcrlf & _
							"from	we_newgistics_upload a with (nolock) left outer join we_adminUsers b" & vbcrlf & _
							"	on	a.adminID = b.adminID" & vbcrlf & _
							"where	manifestFile = '" & ftp.GetFilename(i) & "'" & vbcrlf & _
							"group by (b.fname + ' ' + b.lname)" & vbcrlf
					set rsTemp = oConn.execute(sql)
					dateModified = ""
					numOrders = 0
					adminUser = ""
					if not rsTemp.eof then
						dateModified = rsTemp("entryDate")
						numOrders = formatnumber(rsTemp("numOrders"), 0)
						adminUser = rsTemp("adminUser")
					end if
				
			%>
			<tr class="grid_row_<%=(i mod 2)%>" onMouseOver="this.className='mouseover'" onMouseOut="this.className='grid_row_<%=(i mod 2)%>'" >
				<td width="300" align="center"><a href="<%=replace(replace("c:\Inetpub\wwwroot\wirelessemporium.com\www\tempCSV\newgistics\" & ftp.GetFilename(i), "c:\Inetpub\wwwroot\wirelessemporium.com\www\", "http://www.wirelessemporium.com/"), "\", "/")%>" target="_blank"><%=ftp.GetFilename(i)%></a></td>
				<td width="200" align="center"><%=dateModified%>
				<%
'				if fs.FileExists(filePath & ftp.GetFilename(i)) then
'					set fsTemp = fs.GetFile(filePath & ftp.GetFilename(i))
'					response.write fsTemp.DateLastModified
'				end if
				%>
                </td>
                <td width="200" align="center"><%=numOrders%></td>
                <td width="100" align="center"><%=adminUser%></td>
			</tr>				
			<%
				end if
			next
			%>
            </table>
            </div>
		</td>
	</tr>
    <tr>
    	<td>
        	<form name="frm" method="post">
            	<input type="submit" name="upload" value="Upload Now" />
            </form>
        </td>
    </tr>
</table>


