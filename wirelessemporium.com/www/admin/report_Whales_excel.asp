<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
today = date
strSDate = request("startDate")
strEDate = request("endDate")
if isDate(strEDate) then
	strEDate = cdate(strEDate) + 1
end if	
minAmount = request("minAmt")
chkApproved = request("chkApproved")
chkCancelled = request("chkCancelled")
curSite = request("cbSite")

filename	=	"LargeOrderReport_" & Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now) & ".xls"

Response.ContentType = "application/vnd.ms-excel"
'Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
Response.AddHeader "Content-Disposition","attachment;filename="""&filename&""""
'Response.Charset = "" 
'Me.EnableViewState = False


if not isDate(strSDate) then strError = strError & "<h3>Start Date must be a valid date.</h3>"
if strEDate <> "" then
	if not isDate(strEDate) then strError = strError & "<h3>End Date must be a valid date.</h3>"
end if
if minAmount = "" then strError = strError & "<h3>You must enter a Minimum Dollar Amount.</h3>"
	
if strError = "" then
	sql	=	"select	x.shortDesc, x.color, a.orderid, a.accountid, b.email, a.orderdatetime, isnull(cast(a.ordergrandtotal as money), 0) ordergrandtotal" & vbcrlf & _
			"	, 	isnull(cast(a.ordersubtotal as money), 0) ordersubtotal, isnull(cast(a.ordershippingfee as money), 0) ordershippingfee, a.approved, a.cancelled" & vbcrlf & _
			"from	we_orders a join v_accounts b" & vbcrlf & _
			"	on	a.store = b.site_id and a.accountid = b.accountid join xstore x" & vbcrlf & _
			"	on	a.store = x.site_id" & vbcrlf & _
			"where	a.orderdatetime >= '" & strSDate & "'" & vbcrlf & _
			"	and	cast(a.ordersubtotal as money) >= " & minAmount & vbcrlf
	if curSite <> "" and curSite <> "-1" then sql = sql & "	and x.site_id = '" & curSite & "'" & vbcrlf			
	if isDate(strEDate) then sql	= sql & "	and	a.orderdatetime < '" & strEDate & "'" & vbcrlf
	if chkApproved = "on" then sql	= sql & "	and	a.approved = 1" & vbcrlf
	if chkCancelled = "on" then 
		sql	= sql & "	and	a.cancelled = 1" & vbcrlf
	else
		sql	= sql & "	and	(a.cancelled = 0 or a.cancelled is null)" & vbcrlf
	end if

	sql = sql & "order by x.site_id, a.orderdatetime desc"

	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if RS.eof then
		response.write "No records matched<br><br>So cannot make table..."
	else
		response.write "Store" & vbTab & "Order ID" & vbTab & "E-Mail" & vbTab & "Order Date/Time" & vbTab & "Grand Total" & vbTab & "subTotal" & vbTab & "ShippingFee" & vbTab & "Approved" & vbTab & "Cancelled" & vbNewLine
		nCnt = 0
		lap = 0
		do until RS.eof
			lap = lap + 1
			nCnt = nCnt + 1
			response.write rs("shortDesc") & vbTab
			response.write RS("orderid") & vbTab
			response.write RS("email") & vbTab
			response.write RS("orderdatetime") & vbTab
			response.write formatCurrency(RS("ordergrandtotal")) & vbTab
			response.write formatCurrency(RS("ordersubtotal")) & vbTab
			response.write formatCurrency(RS("ordershippingfee")) & vbTab
			response.write YesNo(RS("approved")) & vbTab
			response.write YesNo(RS("cancelled")) & vbNewLine
																											
			if nCnt > 2000 then
				nCnt = 0
				response.Flush()
			end if
			RS.movenext
		loop
	end if
	RS.close
	Set RS = nothing
else
	response.write strError & vbcrlf
end if
	
	
function YesNo(val)
	if val = true then
		YesNo = "Yes"
	else
		YesNo = "No"
	end if
end function
%>