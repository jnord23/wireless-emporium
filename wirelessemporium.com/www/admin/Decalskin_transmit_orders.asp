<%
response.buffer = false
pageTitle = "Admin - Transmit DecalSkin Orders"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
if request.form("submitted") <> "" then	
	dim fs, thisFile, filename, path
	filename = "Decalskin_" & replace(cStr(date),"/","-") & ".csv"
	path = server.MapPath("/tempCSV/Decalskin/" & filename)
	set fs = Server.CreateObject("Scripting.FileSystemObject")
	if fs.FileExists(path) then fs.DeleteFile(path)
	set thisFile = fs.CreateTextFile(path)
	
	dim iCount, rCount, HoldOrderNum, PartNumber
	iCount = 0
	rCount = 0
	HoldOrderNum = 9999999
	PartNumber = ""
	
'	strToWrite = "ponum,email,company,address1,address2,city,state,zip,citystatezip,shipvia,qtyord,Color,Description,Dropshipper Item,last name,Country,weOrderNumber"
	strToWrite = "ponum,email,company,address1,address2,city,state,zip,Country,shipvia,qtyord,Dropshipper Item Code,weOrderNumber"
	thisFile.WriteLine strToWrite
	
	for storeCount = 0 to 4
		SQL = "SELECT A.orderID, A.thub_posted_date, A.shiptype, B.orderdetailid, B.quantity, C.itemDesc, C.PartNumber, C.vendor, D.* FROM we_Orders A"
		SQL = SQL & " INNER JOIN we_Orderdetails B ON A.orderID = B.orderID"
		SQL = SQL & " INNER JOIN we_Items C ON B.itemID = C.itemID"
		select case storeCount
			case 0 : SQL = SQL & " INNER JOIN we_Accounts D ON A.accountID = D.AccountID WHERE A.store = 0"
			case 1 : SQL = SQL & " INNER JOIN CA_Accounts D ON A.accountID = D.AccountID WHERE A.store = 1"
			case 2 : SQL = SQL & " INNER JOIN CO_Accounts D ON A.accountID = D.AccountID WHERE A.store = 2"
			case 3 : SQL = SQL & " INNER JOIN PS_Accounts D ON A.accountID = D.AccountID WHERE A.store = 3"
			case 4 : SQL = SQL & " INNER JOIN ER_Accounts D ON A.accountID = D.AccountID WHERE A.store = 10"
		end select
		SQL = SQL & " AND A.approved = 1 AND (A.cancelled IS NULL OR A.cancelled = 0)"
		'SQL = SQL & " AND A.thub_posted_date >= '" & dateStart & "' AND A.thub_posted_date < '" & dateAdd("d",1,date) & "'"
		SQL = SQL & " AND C.vendor = 'DS'"
		SQL = SQL & " AND B.SentToDropShipper IS NULL"
		SQL = SQL & " ORDER BY A.orderID"
		response.write "<p>" & SQL & "</p>"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		do until RS.eof
			if HoldOrderNum = 9999999 then
				HoldOrderNum = RS("orderID")
			elseif RS("orderID") <> HoldOrderNum then
				thisFile.WriteLine strToWrite
				PartNumber = ""
				rCount = rCount + 1
				HoldOrderNum = RS("orderID")
			end if
			if len(RS("sState")) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",RS("sState")) > 0 then
				sSCountry = "CANADA"
			else
				sSCountry = "USA"
			end if
			if PartNumber <> "" then PartNumber = PartNumber & ", "
			if RS("quantity") > 1 then
				PartNumber = PartNumber & "(" & replace(RS("PartNumber"),"DEC-SKN-","") & " x" & RS("quantity") & ")"
			else
				PartNumber = PartNumber & replace(RS("PartNumber"),"DEC-SKN-","")
			end if
			strAddress = RS("sAddress1")
			if not isNull(RS("sAddress2")) and RS("sAddress2") <> "" then strAddress = strAddress & ", " & RS("sAddress2")
			strToWrite = RS("orderID") & ","
			strToWrite = strToWrite & chr(34) & "decalskin@wirelessemporium.com" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("fname") & " " & RS("lname") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("sAddress1") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("sAddress2") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("sCity") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("sState") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("sZip") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & sSCountry & chr(34) & ","
			strToWrite = strToWrite & ","
			strToWrite = strToWrite & ","
			strToWrite = strToWrite & chr(34) & PartNumber & chr(34) & ","
			strToWrite = strToWrite & RS("orderID")
			
			iCount = iCount + RS("quantity")
			SQL = "UPDATE we_OrderDetails SET SentToDropShipper = '" & now & "' WHERE orderdetailid = " & rs("orderdetailid") & " and orderID = '" & RS("orderID") & "'"
			response.write "<p>" & SQL & "</p>" & vbcrlf
			oConn.execute SQL
			RS.movenext
		loop
	next
	
	thisFile.WriteLine strToWrite
	rCount = rCount + 1
	
	bodyText = "<h3><u>Decalskin Orders</u></h3>"
	bodyText = bodyText & "<h3>TOTAL NUMBER OF RECORDS: " & rCount & "</h3>"
	bodyText = bodyText & "<h3>TOTAL NUMBER OF ITEMS: " & iCount & "</h3>"
	bodyText = bodyText & "<h3>(CSV FILE ATTACHED)</h3>"
	response.write bodyText & "<br>" & vbcrlf
	response.write "<p><a href=""http://www.wirelessemporium.com/tempCSV/Decalskin/" & filename & """>" & filename & "</a></p>" & vbcrlf
	
	RS.close
	set RS = nothing
	thisFile.Close()
	
	if rCount > 1 then
		Set objErrMail = CreateObject("CDO.Message")
		With objErrMail
			.From = "service@wirelessemporium.com"
'			.To = "terry@wirelessemporium.com"
			.To = "jackie@decalskin.com"
			.cc = "ronny@decalskin.com"
			.bcc = "ruben@wirelessemporium.com,jon@wirelessemporium.com,steven@wirelessemporium.com,yvonne@wirelessemporium.com"
			.Subject = "Decalskin Orders for " & date
			.HTMLBody = bodyText
			.AddAttachment path
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Update
			.Send
		End With
	end if
	
	set thisFile = nothing
	set objErrMail = nothing
	set filesys = nothing
	set demofolder = nothing
	set demofile = nothing
	set filecoll = nothing
	
	response.write "<h3>DONE!</h3>" & vbcrlf
else
	%>
	<form action="Decalskin_transmit_orders.asp" method="post">
		<p class="normalText"><input type="submit" name="submitted" value="Transmit Orders"></p>
	</form>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
