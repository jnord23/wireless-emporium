<%
server.scripttimeout = 180
pageTitle = "Customer List"
header = 1
%>

<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include file="aspDatetime.asp"-->
<style>
.grid_row_0	{ background-color:#FFFFFF;}
.grid_row_1	{ background-color:#EAEAEA;}
.mouseover	{ background-color:#9BCEFF;}
</style>
<script language="javascript">

	function TrimStrings(stringToTrim)
	{
		return stringToTrim.replace(/^\s+|\s+$/g,"");
	}

	function fngotopg(pgno){
		document.FRMSEARCH.reset();
		document.FRMSEARCH.txtpageno.value = pgno;
		document.FRMSEARCH.action = "custlist.asp"
		document.FRMSEARCH.submit();
	}
	function fngotoorderpg(acctid,site_id){
		document.FRMSEARCH.reset();
		document.FRMSEARCH.txthidacctid.value=acctid;
		document.FRMSEARCH.hidsiteid.value=site_id;		
		document.FRMSEARCH.action = "orderslist.asp"
		document.FRMSEARCH.submit();
	}
	function resetAll(){
		for(i=0; i<document.FRMSEARCH.elements.length; i++){
			if (document.FRMSEARCH.elements[i].type == "text"){
				document.FRMSEARCH.elements[i].value = "";
			}else if (document.FRMSEARCH.elements[i].type == "select-one"){
				document.FRMSEARCH.elements[i].selectedIndex = 0;
			}
		}
	}
	
	function doSubmit()
	{
		var	nValue		=	0;
		var elem 		= 	document.getElementById('id_frmSearch').elements;
		var	nOrderID	=	TrimStrings(document.FRMSEARCH.txtorderid.value);
		var	nCustID		=	TrimStrings(document.FRMSEARCH.txtaccountid.value);
		
		for(var i = 0; i < elem.length; i++)
		{
			if ((("text" == elem[i].type) || ("select-one" == elem[i].type)) && ("selpage" != elem[i].name))
			{
				if ("" != TrimStrings(elem[i].value)) nValue++;
			}
		} 
		
		if (0 == nValue) 
		{
			alert("please select at least one option.");
			return false;
		} else if (isNaN(nCustID))
		{
			alert("Customer # is not a number.");
			return false;			
		} else if (isNaN(nOrderID))
		{
			alert("Order ID is not a number.");
			return false;			
		}
	
		return true;
	}
</script>

<%
dim strDate, strOrderId, strCustId, strFname, strLName, strConfirm, strPhone, strEmail, PgNo, strEmailAddr
strDate = fnRetDateFormat(Trim("" & Request.Form("txtorderdate")),"mm/dd/yyyy")
strLName = Trim("" & Request.Form("txtLName"))
strFname = Trim("" & Request.Form("txtFName"))
strCustId = Trim("" & Request.Form("txtAccountId"))
strPhone = Trim("" & Request.Form("txtPhone"))
strOrderId = Trim("" & Request.Form("txtOrderId"))
strConfirm = Trim("" & Request.Form("txtConfirm"))
strEmail = Trim("" & Request.Form("txtEmail"))
strEmailAddr = Trim("" & Request.Form("txtEmailAddr"))
PgNo = Trim("" & Request.Form("txtpageno"))

if isnumeric(strOrderId) then strOrderId = cdbl(strOrderId)

dim SQL, strFilter, strJoinClause
dim PgSize, strWhere, RecCount, TotalPages
PgSize = 20
If PgNo = "" then PgNo = 1
If Not ISNumeric(PgNo) then PgNo = 1

Function fnstrFilter()
	'THIS FUNCTION WILL BUILD THE WHERE CLAUSE
	dim strFilter
	strFilter = ""
	strFilter = fnstrAppendFilter(strFilter,"A.ACCOUNTID",strCustId,"="," AND ","","")
	strFilter = fnstrAppendFilter(strFilter,"LName",strLName,"LIKE"," AND ","","")
	strFilter = fnstrAppendFilter(strFilter,"FName",strFname,"LIKE"," AND ","","")
	strFilter = fnstrAppendFilter(strFilter,"PHONE",strPhone,"LIKE"," AND ","","")
	if instr(strOrderId, "-") > 0 then
		strFilter = fnstrAppendFilter(strFilter,"extOrderNumber",strOrderId,"="," AND ","","")
	elseif len(strOrderId) > 7 then
'		strFilter = fnstrAppendFilter(strFilter,"extOrderNumber",strOrderId,"="," AND ","","")
		if strFilter <> "" then
			strFilter = strFilter & " AND (orderid = '" & strOrderId & "' or extOrderNumber = '" & strOrderId & "')"
		else
			strFilter = strFilter & "(orderid = '" & strOrderId & "' or extOrderNumber = '" & strOrderId & "')"
		end if
	else
		strFilter = fnstrAppendFilter(strFilter,"ORDERID",strOrderId,"="," AND ","","")
	end if
	strFilter = fnstrAppendFilter(strFilter,"Email",strEmailAddr,"="," AND ","","")
	strFilter = fnstrAppendFilter(strFilter,"Convert(varchar(10),O.ORDERDATETIME ,101)",strDate,"="," AND ","","")
	Select Case strConfirm
		Case "YES":
			If strFilter <> "" then strFilter = strFilter & " AND "
			strFilter = strFilter & " (CONFIRMSENT = 'yes') "
		Case "NO":
			If strFilter <> "" then strFilter = strFilter & " AND "
			strFilter = strFilter & " (CONFIRMSENT IS NULL Or CONFIRMSENT <> 'yes') "
	End Select
	Select Case strEmail
		Case "YES":
			If strFilter <> "" then strFilter = strFilter & " AND "
			strFilter = strFilter & " (APPROVED = 1) "
		Case "NO":
			If strFilter <> "" then strFilter = strFilter & " AND "
			strFilter = strFilter & " (APPROVED IS NULL OR APPROVED = 0) "
	End Select
	
'	if inStr(strFilter,"O.") > 0 then
'		if strFilter <> "" then strFilter = strFilter & " AND "
'		strFilter = strFilter & " O.store = 0 "
'	end if
	
	If strFilter <> "" then strFilter = " WHERE " & strFilter
	fnstrFilter = strFilter
End Function

Function fnstrAppendFilter(strFilter, strFieldName, strSearchVal, strMatchType, strJoin, strFuture1, strFuture2)
	fnstrAppendFilter = strFilter
	If strSearchVal <> "" Then
		If fnstrAppendFilter <> "" then fnstrAppendFilter = fnstrAppendFilter & strJoin
		Select Case strMatchType
			Case "LIKE" : fnstrAppendFilter = fnstrAppendFilter & "( " & strFieldName & " Like '%" & Replace(strSearchVal,"'","''") & "%')"
			Case "=" : fnstrAppendFilter = fnstrAppendFilter & "( " & strFieldName & "='" & Replace(strSearchVal,"'","''") & "')"
		End Select
	End If
End Function

Function fnStrJoinClause(strQryType)
	'This function will return join clause with the Orders table only when required.
	'JOIN CLAUSE WILL BE REQUIRED WHEN CLIENT ENTERS ORDER TABLE RELATED CRITERIA'S ALSO
	fnStrJoinClause=""
	If strOrderId <> "" Or strDate <> "" Or strConfirm <>"" Or strEmail <> "" Then
		Select Case UCASE(strQryType)
			Case "MAINQRY" : fnStrJoinClause = " JOIN WE_ORDERS O ON A.ACCOUNTID = O.ACCOUNTID join xstore x on o.store = x.site_id and a.site_id = x.site_id "
			Case "SUBQRY" : fnStrJoinClause = " JOIN WE_ORDERS SO ON SA.ACCOUNTID = SO.ACCOUNTID join xstore x on so.store = x.site_id and sa.site_id = x.site_id "
		End Select
	else
		Select Case UCASE(strQryType)
			Case "MAINQRY" : fnStrJoinClause = " join xstore x on a.site_id = x.site_id "
			Case "SUBQRY" : fnStrJoinClause = " join xstore x on sa.site_id = x.site_id "
		End Select	
	End If
End Function

if request.querystring("display") <> "none" then
	SQL = "SELECT Distinct Top " & PgSize & " A.accountid, A.FName, A.LName, A.PHONE, A.EMAIL, A.PWORD, A.CustomerIP, nullif(x.logo, '') site_logo, x.shortDesc site, x.color logo_color, x.site_id "
	SQL = SQL & "From v_accounts A "
	'IF ANY OF THE ORDER CRITERIAS ARE GIVEN then LEFT JOIN WITH THE ORDER TABLE
	strJoinClause = fnStrJoinClause("MAINQRY")
	'ADD THE WHERE CLAUSE
	strWhere = fnstrFilter
	'PREPARE THE MAIN QRY
	SQL = SQL & " " & strJoinClause & " " & strWhere
	dim strCountSQL
	strCountSQL = "SELECT Count(Distinct A.AccountId) From v_accounts A "
	strCountSQL = strCountSQL & " " & strJoinClause & " " & strWhere
	If (PgNo <> 1) Then
		If strWhere = "" then SQL = SQL & " Where A.AccountId Not In "
		If strWhere <> "" then SQL = SQL & " And A.AccountId Not In "
		SQL = SQL & "(SELECT Distinct Top " & (PgNo-1) * PgSize & " SA.accountid From v_accounts SA "
		SQL = SQL & fnStrJoinClause("SUBQRY")
		SQL = SQL & Replace(Ucase(strWhere),"A.ACCOUNTID","SA.ACCOUNTID")
		SQL = SQL & " Order By SA.ACCOUNTID) "
	End If
	SQL = SQL & " Order By A.ACCOUNTID"
	session("errorSQL") = SQL
	
'	response.write sql & "<br>"
'	response.write strCountSQL & "<br>"	
	
	dim RS, objCmd
	Set objCmd = Server.CreateObject("ADODB.COMMAND")
	Set RS = Server.CreateObject("ADODB.RECORDSET")
	objCmd.CommandText = "CustomerLookup"
	objCmd.CommandType = 4
	objCmd.Parameters.Append objCmd.CreateParameter("@sqlSTR", 200, 1,4000,SQL)
	objCmd.Parameters.Append objCmd.CreateParameter("@strQryRecCnt", 200, 1,2000,strCountSQL)
	objCmd.Parameters.Append objCmd.CreateParameter("@RecCount", 20, 2)
	objCmd.ActiveConnection = oConn
	RS.CursorLocation = 3
	RS.Open objCmd,,3,1
	RecCount = objCmd.Parameters("@RecCount").Value
	TotalPages = TISPL_fnReturnTotalPages(Cdbl(RecCount),Cdbl(PgSize))
	
	if 0 = clng(RecCount) then
		Set objCmd = nothing
		Set RS = nothing
		Set objCmd = Server.CreateObject("ADODB.COMMAND")
		Set RS = Server.CreateObject("ADODB.RECORDSET")		
		
		SQL 		= replace(SQL, "WE_ORDERS", "WE_ORDERS_HISTORICAL")
		strCountSQL = replace(strCountSQL, "WE_ORDERS", "WE_ORDERS_HISTORICAL")		
		
		objCmd.CommandText = "CustomerLookup"
		objCmd.CommandType = 4
		objCmd.Parameters.Append objCmd.CreateParameter("@sqlSTR", 200, 1,4000,SQL)
		objCmd.Parameters.Append objCmd.CreateParameter("@strQryRecCnt", 200, 1,2000,strCountSQL)
		objCmd.Parameters.Append objCmd.CreateParameter("@RecCount", 20, 2)
		objCmd.ActiveConnection = oConn
		RS.CursorLocation = 3
		RS.Open objCmd,,3,1
		RecCount = objCmd.Parameters("@RecCount").Value
		TotalPages = TISPL_fnReturnTotalPages(Cdbl(RecCount),Cdbl(PgSize))		
	end if
end if
%>

<table width="95%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr class="bigText" bgcolor="#CCCCCC">
		<td colspan="8" width="100%">
			<table border="0" width="100%" class="bigText" bgcolor="#CCCCCC">
				<tr>
					<td align="left"><a href='#' onClick="window.history.go(-1);"><< BACK</a></td>
					<td align="center"> Customer Look-up Search Criteria </td>
					<td align="right"><a href='#' onClick="window.history.go(1);">FORWARD >></a></td>
				</tr>
			</table>
		</td>
	</tr>
	<form name="FRMSEARCH" method="post" action="custlist.asp" id="id_frmSearch">
	<tr>
		<td class="smlText" colspan="8">
			<table border="0">
				<tr>
					<td class="smlText">First Name : <input class="smlText" type="text" name="txtfname" size="8" value="<%=strFname%>">&nbsp;</td>
					<td class="smlText">Last Name : <input class="smlText" type="text" name="txtlname" size="10" value="<%=strLName%>">&nbsp;</td>
					<td class="smlText">Customer # : <input class="smlText" type="text" name="txtaccountid" size="8" value="<%=strCustId%>">&nbsp;</td>
					<td class="smlText">Phone # : <input class="smlText" type="text" name="txtphone" size="8" value="<%=strPhone%>"><br></td>
					<td class="smlText">Email Address : <input class="smlText" type="text" name="txtEmailAddr" size="25" value="<%=Request.Form("txtEmailAddr")%>"><br></td>
				</tr>
				<tr>
					<td class="smlText">&nbsp;&nbsp;&nbsp;Order Id : <input class="smlText" type="text" name="txtorderid" size="8" value="<%=strOrderId%>">&nbsp;</td>
					<td class="smlText">Order Date : <input class="smlText" type="text" name="txtorderdate" size="10" maxlength="10" value="<%=strDate%>">&nbsp;</td>
					<td class="smlText">Email Sent : <select class="smlText" name="txtemail"><option value="">All</option><option value="YES"<%if strEmail="YES" then Response.Write(" selected")%>>Yes</option><option value="NO" <%If strEmail="NO" then Response.Write("selected")%>>No</option></select> &nbsp;</td>
					<td class="smlText" colspan="2">Confirm : <select class="smlText" name="txtconfirm"><option value="">All</option><option value="YES"<%if strConfirm="YES" then Response.Write(" selected")%>>Yes</option><option value="NO" <%If strConfirm="NO" then Response.Write("selected")%>>No</option></select></td>
				</tr>
				<tr>
					<td colspan="12" align="center"><input type="SUBMIT" value="SEARCH" class="smltext" id="SUBMIT1" name="SUBMIT1" onclick="return doSubmit();">&nbsp;&nbsp;&nbsp;<input type="button" value="CLEAR" class="smltext" onClick="resetAll();"><input type="hidden" name="txtpageno" value="1"></td>
				</tr>
				<tr>
					<td colspan="12" align="center" class="smlText"><b>Note:</b> Search on Names and Phone number uses pattern matching and retrieves all records containing entered words.<br>Search on Other fields - OrderId, OrderDate, Customer # will look for records containing exact match.</td>
				</tr>
			</table>
		</td>
	</tr>
	<input type="hidden" name="txthidacctid" value="">
	<input type="hidden" name="hidsiteid" value="">
	</form>
	
	<%if request.querystring("display") <> "none" then%>
		<tr bgcolor="#CCCCCC" class="smlText">
			<td valign="middle" colspan="8" align="right">
				Page No :<select name="selpage" class="smlText" onChange="fngotopg(this.value);">
				<%
				for iLoop = 1 to TotalPages
					response.write "<option value='" & iLoop & "'"
					if cStr(PgNo) = cStr(iLoop) then response.write(" selected")
					response.write ">" & iLoop & "</option>" & vbcrlf
				next
				%>
			</select>
			</td>
		</tr>
		<tr bgcolor="#CCCCCC">
			<td class="bigText" align="center">Store</td>		
			<td class="bigText">&nbsp;Account Id</td>
			<td class="bigText">First&nbsp;Name</td>
			<td class="bigText">Last&nbsp;Name</td>
			<td class="bigText">Telephone</td>
			<td class="bigText">Email</td>
			<td class="bigText">IP&nbsp;Address</td>
			<td class="bigText">Password</td>
		</tr>
		<%
		if not RS.eof then
			dim nRow : nRow = 0
			do until RS.eof
				%>
				<tr class="grid_row_<%=(nRow mod 2)%>" onMouseOver="this.className='mouseover'" onMouseOut="this.className='grid_row_<%=(nRow mod 2)%>'" >
					<td class="smlText" align="center">
					<%
					if isnull(RS("site_logo")) then 
					%>
					<font color="<%=RS("logo_color")%>"><b><%=RS("site")%></b></font>
					<%
					else
					%>
					<img src="<%=RS("site_logo")%>" border="0" width="16" height="16" />
					<%
					end if
					%>					
					</td>
					<td class="smlText"><a href="#" onClick="fngotoorderpg('<%=RS("ACCOUNTID")%>','<%=RS("site_id")%>')"><%=RS("ACCOUNTID")%></a></td>
					<td class="smlText"><%=RS("FName")%></td>
					<td class="smlText"><%=RS("LName")%></td>
					<td class="smlText"><%=RS("Phone")%></td>
					<td class="smlText"><a href="mailto:<%=RS("Email")%>"><%=RS("Email")%></a></td>
					<td class="smlText"><%=RS("CustomerIP")%></td>
					<td class="smlText"><%=RS("Pword")%></td>
				</tr>
				<%
				nRow = nRow + 1				
				RS.MoveNext
			loop
		else
			%>
			<tr><td colspan="7" class="smlText">No Records Found</td></tr>
			<%
		end if
		Set objCmd = Nothing
		RS.Close
		Set RS = Nothing
	end if
	%>
</table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
