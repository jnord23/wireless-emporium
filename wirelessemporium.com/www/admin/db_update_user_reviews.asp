<%
pageTitle = "Update User Reviews"
header = 1

siteSelection = request.QueryString("siteSelection")

set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = true
myCount = Upload.Save

if isnull(siteSelection) or len(siteSelection) < 1 then siteSelection = "WE"

site_id = 0
select case siteSelection
	case "WE"
		site_id = 0
		tableName = "we_reviews"
	case "CA"
		site_id = 1
		tableName = "ca_reviews"
	case "CO"
		site_id = 2
		tableName = "co_reviews"
	case "PS"
		site_id = 3
		tableName = "ps_reviews"
end select
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
sql	=	"delete from we_reviews where (email = '0' and nickname = '0') or (reviewTitle = '0' and convert(varchar(8000), review) = '0') or (reviewTitle = '' and convert(varchar(8000), review) = '')"
oConn.execute(sql)
sql	=	"delete from ps_reviews where (nickname = '0') or (reviewTitle = '0' and convert(varchar(8000), review) = '0') or (reviewTitle = '' and convert(varchar(8000), review) = '') or (reviewTitle = '' and convert(varchar(8000), review) = '0')"
oConn.execute(sql)
%>
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
    	<td><h3>Update WE Database - Update User Reviews</h3></td>
    </tr>
    <tr>
    	<td>
        	<div style="float:left; white-space:200px; text-align:right; font-weight:bold;">Site To Approve Reviews For:</div>
            <div style="float:right; width:100px; text-align:left">
            	<select name="siteSelect" onchange="window.location='/admin/db_update_user_reviews.asp?siteSelection=' + this.value">
                	<option value="WE"<% if siteSelection = "WE" then %> selected="selected"<% end if %>>WE</option>
                	<option value="CA"<% if siteSelection = "CA" then %> selected="selected"<% end if %>>CA</option>
                	<option value="CO"<% if siteSelection = "CO" then %> selected="selected"<% end if %>>CO</option>										
                    <option value="PS"<% if siteSelection = "PS" then %> selected="selected"<% end if %>>PS</option>
                </select>
            </div>
        </td>
    </tr>
</table>
<%
if Upload.form("submitted") = "Update" then
	if site_id = 0 then
	%>
	<script src="http://d3aa0ztdn3oibi.cloudfront.net/javascripts/ff.loyalty.widget.js" type="text/javascript"></script>
    <script>_ffLoyalty.initialize("<%=WE_500_ACCID%>");</script>    
    <%
	end if
	for a = 1 to Upload.form("totalCount")
		select case Upload.form("approved" & a)
			case "0" : approved = "0"
			case "1" : approved = "-1"
			case else : approved = "null"
		end select
		PartNumbers = SQLquote(Upload.form("PartNumbers" & a))
		tPartNumber = prepStr(Upload.form("PartNumber" & a))
		tEmail = prepStr(Upload.form("email" & a))

		if site_id = 0 then
			if approved = "-1" then 'approved
				if instr(tEmail, "@") > 0 and tEmail <> "" then
					%>
					<img src="http://loyalty.500friends.com/api/record.gif?uuid=<%=WE_500_ACCID%>&email=<%=tEmail%>&type=review" style="position: absolute; left: -10px; visibility: hidden;"/>
					<%
				end if
			end if
		end if

		SQL = "UPDATE " & tableName & " SET"
		SQL = SQL & " review = '" & SQLquote(Upload.form("Describe" & a)) & "',"
		if PartNumbers = "" then
			SQL = SQL & " PartNumbers = '" & Ucase(tPartNumber) & "',"
		else
			SQL = SQL & " PartNumbers = '" & Ucase(PartNumbers) & "',"
		end if
		SQL = SQL & " approved = " & approved
		SQL = SQL & " WHERE id='" & Upload.form("id" & a) & "'"
		session("errorSQL") = sql
		oConn.execute SQL
	next
	%>
	<h3>UPDATED!</h3>
	<%
else
	SQL = 	"select	a.id, a.orderid, a.itemid, c.itemdesc, c.partnumber, a.nickname, a.rating, a.reviewTitle, convert(varchar(8000), a.review) review" & vbcrlf & _
			"	,	a.partnumbers, a.datetimeentd, a.email " & vbcrlf & _
			"from	v_reviews a left outer join we_orderdetails B " & vbcrlf & _
			"	ON	A.orderID = B.orderID AND A.itemID = B.itemid left outer JOIN we_items C " & vbcrlf & _
			"	ON	a.itemID = C.itemID " & vbcrlf & _
			"WHERE	A.approved IS NULL " & vbcrlf & _
			"	and	a.site_id = '" & site_id & "'" & vbcrlf & _
			"ORDER BY A.id"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	a = 0
	if RS.eof then
		response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
	else
		%>
		<table border="1" cellpadding="3" cellspacing="0" align="center" class="smlText">
			<form action="db_update_user_reviews.asp?siteSelection=<%=siteSelection%>" method="post" enctype="multipart/form-data">
				<%
				do until RS.eof
					dim id, orderID, itemID, itemDesc, Rating, nickname, reviewTitle, review, DateTimeEntd, a
					id = RS("id")
					orderID = RS("orderID")
					itemID = RS("itemID")
					itemDesc = RS("itemDesc")
					PartNumber = RS("PartNumber")
					nickname = RS("nickname")
					Rating = RS("Rating") * 2
					reviewTitle = RS("reviewTitle")
					review = RS("review")
					PartNumbers = RS("PartNumbers")
					DateTimeEntd = RS("DateTimeEntd")
					email = RS("email")
					a = a + 1
					%>
					<tr>
						<td valign="top" align="center"><b>orderID</b><br><%=orderID%></td>
						<td valign="top" align="left">&nbsp;<b>Item&nbsp;Description:</b><br>&nbsp;<%=itemDesc%></td>
						<td valign="top" align="left">&nbsp;<b>Part&nbsp;Number:</b><br>&nbsp;<%=PartNumber%></td>
						<td valign="top" align="center">
                        	<b>Part&nbsp;Numbers:</b><br>
                            <input type="text" name="PartNumbers<%=a%>" value="<%=PartNumbers%>" size="20" maxlength="15">
                            <input type="hidden" name="PartNumber<%=a%>" value="<%=PartNumber%>">
                            <input type="hidden" name="email<%=a%>" value="<%=email%>">
                        </td>
					</tr>
					<tr>
						<td valign="top" align="center"><b>Nickname:</b><br><%=nickname%></td>
						<td valign="top" align="center" colspan="2"><b>Review&nbsp;Title:</b><br><%=reviewTitle%></td>
						<td valign="top" align="center"><b>Rating:</b><br><img src="/images/reviews/<%=Rating%>.jpg" width="100" height="20" border="0" alt="<%=Rating%> stars" align="absbottom"></td>
					</tr>
					<tr>
						<td valign="top" align="center"><b>DateTimeEntd:</b><br><%=DateTimeEntd%></td>
						<td valign="top" align="center" colspan="3" rowspan="2"><b>User&nbsp;Review:</b><br><textarea name="Describe<%=a%>" rows="5" cols="70"><%=review%></textarea></td>
					</tr>
					<tr>
						<td valign="top" align="center"><b>Approved:</b><br>
							<input type="hidden" name="id<%=a%>" value="<%=id%>">
							<input type="radio" name="approved<%=a%>" value="1">Y&nbsp;
							<input type="radio" name="approved<%=a%>" value="0">N
						</td>
					</tr>
					<tr>
						<td align="center" colspan="4" bgcolor="#999999"><img src="/images/spacer.gif" width="1" height="2" border="0"></td>
					</tr>
					<%
					RS.movenext
				loop
				%>
				<tr>
					<td align="center" colspan="4">
						<input type="hidden" name="totalCount" value="<%=a%>">
						<input type="submit" name="submitted" value="Update">
					</td>
				</tr>
			</form>
		</table>
		<%
	end if
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
