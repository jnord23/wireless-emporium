<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - "
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	set fso = CreateObject("Scripting.FileSystemObject")
	
	addModelID = request.QueryString("addModelID")
	removeModelID = request.QueryString("removeModelID")
	upOrderNum = request.QueryString("upOrderNum")
	downOrderNum = request.QueryString("downOrderNum")
	brandID = request.QueryString("brandID")
	curOrderNum = request.QueryString("curOrderNum")
	
	if isnull(addModelID) or len(addModelID) < 1 then addModelID = 0
	if isnull(removeModelID) or len(removeModelID) < 1 then removeModelID = 0
	if isnull(upOrderNum) or len(upOrderNum) < 1 then upOrderNum = 0
	if isnull(downOrderNum) or len(downOrderNum) < 1 then downOrderNum = 0
	if isnull(brandID) or len(brandID) < 1 then brandID = 0
	if isnull(curOrderNum) or len(curOrderNum) < 1 then curOrderNum = 0
	
	sql = "select a.brandName, b.orderNum from we_brands a left join we_popPhones b on a.brandID = b.brandID and b.storeID = 0 where a.brandID = " & brandID & " order by b.orderNum desc"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 3, 3
	
	if rs.EOF then
		response.End()
	elseif isnull(rs("orderNum")) then
		useOrderNum = 1
	else
		useOrderNum = rs("orderNum") + 1
	end if
	
	if addModelID > 0 then
		sql = "insert into we_popPhones (storeID,brandID,modelID,orderNum) values(0," & brandID & "," & addModelID & "," & useOrderNum & ")"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		fso.DeleteFile(Server.MapPath("/images/stitch/" & session("itemImgPath")))
	elseif removeModelID > 0 then
		sql = "delete from we_popPhones where storeID = 0 and brandID = " & brandID & " and modelID = " & removeModelID
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "update we_popPhones set orderNum = orderNum - 1 where storeID = 0 and brandID = " & brandID & " and orderNum > " & curOrderNum
		session("errorSQL") = sql
		oConn.execute(sql)
		
		fso.DeleteFile(Server.MapPath("/images/stitch/" & session("itemImgPath")))
	elseif upOrderNum > 0 then
		sql = "update we_popPhones set orderNum = orderNum - 1 where storeID = 0 and brandID = " & brandID & " and orderNum = " & (curOrderNum - 1)
		response.Write(sql & "<br>")
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "update we_popPhones set orderNum = orderNum + 1 where storeID = 0 and brandID = " & brandID & " and modelID = " & upOrderNum
		response.Write(sql & "<br>")
		session("errorSQL") = sql
		oConn.execute(sql)
	elseif downOrderNum > 0 then
		sql = "update we_popPhones set orderNum = orderNum + 1 where storeID = 0 and brandID = " & brandID & " and orderNum = " & (curOrderNum + 1)
		response.Write(sql & "<br>")
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "update we_popPhones set orderNum = orderNum - 1 where storeID = 0 and brandID = " & brandID & " and modelID = " & downOrderNum
		response.Write(sql & "<br>")
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	session("errorSQL") = "/images/stitch/" & session("popItemImgPath")
	fso.DeleteFile(Server.MapPath("/images/stitch/" & session("popItemImgPath")))
	response.Redirect(session("curPageLink"))
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
