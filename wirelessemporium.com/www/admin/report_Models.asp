<%
pageTitle = "Admin - Generate Models Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" width="750" align="center" cellpadding="0" cellspacing="0"><tr><td>
<p>&nbsp;</p>
<p class="biggerText"><%=pageTitle%></p>
<br>

<%
strError = ""

if request("submitted") <> "" then
	SQL = "SELECT B.brandName, A.modelName, A.modelID FROM we_models A INNER JOIN we_brands B ON A.brandID = B. brandID ORDER BY B.brandName, A.modelName"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	
	response.write "<p class=""boldText"">" & RS.recordcount & " records found</p>" & vbcrlf
	
	if not RS.eof then
		strToWrite = "Brand Name,Model Name,Model ID" & vbcrlf
		do until RS.eof
			strToWrite = strToWrite & chr(34) & RS("brandName") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("modelName") & chr(34) & ","
			strToWrite = strToWrite & RS("modelID") & vbcrlf
			RS.movenext
		loop
		dim fs, file, filename, path
		set fs = CreateObject("Scripting.FileSystemObject")
		filename = "report_Models.csv"
		path = Server.MapPath("/admin/tempCSV") & "\" & filename
		set file = fs.CreateTextFile(path, true)
		file.WriteLine strToWrite
		response.write "<p class=""boldText"">Your CSV file is <a href=""/admin/tempCSV/" & filename & """>here</a>.</p>"
		
		RS.movefirst
		%>
		<table border="1" width="100%" cellpadding="2" cellspacing="0">
			<tr>
				<td><b>Brand&nbsp;Name</b></td>
				<td><b>Model&nbsp;Name</b></td>
				<td><b>Model&nbsp;ID</b></td>
			</tr>
			<%
			do until RS.eof
				%>
				<tr>
					<td><%=RS("brandName")%></td>
					<td><%=RS("modelName")%></td>
					<td><%=RS("modelID")%></td>
				</tr>
				<%
				RS.movenext
			loop
			%>
		</table>
		<%
	else
		response.write "<p class=""boldText"">No records matched.</p><p class=""boldText"">No report generated.</p>"
	end if
	response.write "<p>&nbsp;</p>"
	RS.close
	Set RS = nothing
end if
%>
<form action="report_Models.asp" method="post">
	<p class="normalText">
		<input type="submit" name="submitted" value="Submit">
	</p>
</form>

</td></tr></table>

<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
