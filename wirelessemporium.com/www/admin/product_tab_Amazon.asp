<%
response.buffer = false
pageTitle = "Create Amazon Product List TXT"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 9000 'seconds

dim fs, file, filename, path
'Map the file name to the physical path on the server.
filename = "productList_Amazon.txt"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Create Amazon Product List TXT</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							Response.write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href=""../tempCSV/" & filename & """>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	response.write("Creating " & filename & ".<br>")
	set file = fs.CreateTextFile(path)
	dim SQL, RS, DoNotInclude, we_itemID
	SQL = "SELECT A.itemID, A.typeid, A.PartNumber, A.itemDesc, A.itemPic, A.price_our, A.itemLongDetail, A.inv_qty, A.UPCCode,"
	SQL = SQL & " B.brandName, C.typeName, D.modelName"
	SQL = SQL & " FROM we_items A"
	SQL = SQL & " INNER JOIN we_brands B ON A.brandid = B.brandid"
	SQL = SQL & " INNER JOIN we_types C ON A.typeid = C.typeid"
	SQL = SQL & " INNER JOIN we_models D ON A.modelid = D.modelid"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " AND A.PartNumber NOT LIKE 'DEC%'"
	SQL = SQL & " ORDER BY itemID"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "Category" & vbtab & "Title" & vbtab & "Link" & vbtab & "SKU" & vbtab & "Price" & vbtab & "UPC" & vbtab & "Image" & vbtab & "Description" & vbtab & "Manufacturer" & vbtab & "Shipping Weight" & vbtab & "Shipping Cost"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				strline = RS("brandName") & " > " & RS("typeName")  & " > " & RS("modelName") & " > " & RS("itemDesc") & vbtab
				strline = strline & RS("itemDesc") & vbtab
				strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp" & vbtab
				strline = strline & "WE-" & 5000 + RS("itemID") & vbtab
				strline = strline & RS("price_our") & vbtab
				strline = strline & RS("UPCCode") & vbtab
				strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab
				strline = strline & replace(RS("itemLongDetail"),vbcrlf," ") & vbtab
				strline = strline & RS("brandName") & vbtab
				strline = strline & vbtab
				strline = strline & "0"
				file.WriteLine strline
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
