<%
pageTitle = "Create Shopping.com Product List CSV"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 9000 'seconds

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
'Map the file name to the physical path on the server.
filename = "productList_shopping.csv"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Create Shopping.com Product List CSV</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							response.write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href=""../tempCSV/" & filename & """>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.Write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	response.write("Creating " & filename & ".<br>")
	set file = fs.CreateTextFile(path)
	dim SQL, RS
	SQL = "SELECT A.itemID,A.PartNumber,A.brandID,A.itemDesc,A.itemPic,A.price_Retail,A.price_Our,A.inv_qty,A.itemLongDetail,A.UPCCode,"
	SQL = SQL & "B.brandName,C.modelName,D.typeName"
	SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
	SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
	SQL = SQL & " LEFT JOIN we_Types D ON A.typeID=D.typeID"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " AND A.typeID <> 16"
	'SQL = SQL & " AND (A.itemID IN (20107,23345,24244,24245,29878,31114,36425,49909,49912,50034,51924,55163,56513,56515,31416,34769,56838)"
	'remove a bunch of products (added back in on 11-18-2010)
	'SQL = SQL & " OR (A.PartNumber NOT LIKE 'DEC%' AND A.PartNumber NOT LIKE 'FP2%' AND A.PartNumber NOT LIKE 'FP3%' AND A.PartNumber NOT LIKE 'FP5%' AND A.PartNumber NOT LIKE 'BT1%'"
	'SQL = SQL & " AND A.PartNumber NOT LIKE 'LC0%' AND A.PartNumber NOT LIKE 'LC1%' AND A.PartNumber NOT LIKE 'LC2%' AND A.PartNumber NOT LIKE 'LC3%' AND A.PartNumber NOT LIKE 'LC4%' AND A.PartNumber NOT LIKE 'LC5%'"
	'SQL = SQL & " AND A.PartNumber NOT LIKE 'HF1%' AND A.PartNumber NOT LIKE 'HF2%' AND A.PartNumber NOT LIKE 'HF3%'"
	'new trucated filter for only decale skins
	SQL = SQL & " AND A.PartNumber NOT LIKE 'DEC%' AND A.PartNumber NOT LIKE 'FP2%' AND A.PartNumber NOT LIKE 'FP3%' AND A.PartNumber NOT LIKE 'FP4%' AND A.PartNumber NOT LIKE 'FP5%' AND A.PartNumber NOT LIKE 'FP6%' AND A.PartNumber NOT LIKE 'FP7%' AND A.PartNumber NOT LIKE 'FP8%' AND A.PartNumber NOT LIKE 'FP9%' and A.PartNumber NOT LIKE 'BT1%'"
	SQL = SQL & " AND (A.UPCCode IS NULL OR A.UPCCode = '')"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "Unique Merchant SKU,Manufacturer Name,UPC,Product name,Product description,Original Price,Current Price,Product URL,Image URL,Ground Shipping,Stock Description,Stock Availability,Shopping.com Categorization,Condition"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
				set RS2 = CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				if inStr("15047,15062,15114,15334,15437,15570,15579,15684,15703,15714,15106,15335,15591,11127,11129,11131,11133,11134,11135,11137,11141,11142,11144,11145,11146,11147,11149,11150,11151,11152,11153,11154,11155,11515,12122,12162,12279,12364,12631,12765,13231,13422,13438,13472,13554,13562,13581,13636,13816,13832,13845,13898,14302,14371,14463,14813,14841,15048,15063,15115,15336,15438,15571,15580,15685,15704,15715,15107,15337,13578,12493,12297,11128,11143,11148,15592,14400,14465,11138,11156,11676,14573,12974,12081,13527,7876,13247,1460,12851,9017,13560,8390,13555,",RS("itemid")&",") = 0 then
					strItemDesc = RS("itemDesc")
					strItemDesc = replace(strItemDesc,"+","&")
					strItemLongDetail = RS("itemLongDetail")
					strItemLongDetail = replace(strItemLongDetail,vbCrLf," ")
					if isnull(RS("itemDesc")) then
						GRPNAME = "Universal"
					else 
						GRPNAME = RS("itemDesc")
					end if
					if isnull(RS("BrandName")) then
						BrandName = "Universal"
					else 
						BrandName = RS("BrandName")
					end if
					
					' For eBay Express:
					GRPNAME = replace(GRPNAME," for "," fits ")
					
					strline = chr(34) & "WE-" & RS("itemid")+5000 & chr(34) & ","
					strline = strline & chr(34) & BrandName & chr(34) & ","
					strline = strline & chr(34) & RS("UPCCode") & chr(34) & ","
					strline = strline & chr(34) & GRPNAME & chr(34) & ","
					strline = strline & chr(34) & strItemLongDetail & chr(34) & ","
					strline = strline & chr(34) & RS("price_retail") & chr(34) & ","
					strline = strline & chr(34) & RS("price_our") & chr(34) & ","
					strline = strline & chr(34) & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?id=shopping" & chr(34) & "," 'Product URL
					strline = strline & chr(34) & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & chr(34) & "," 'Image URL
					strline = strline & chr(34) & "0.00" & chr(34) & "," 'Ground Shipping
					strline = strline & chr(34) & "Ships 1-2 Days" & chr(34) & "," 'Stock Description
					strline = strline & chr(34) & "Y" & chr(34) & "," 'Stock Availability
					strline = strline & chr(34) & "Electronics -> Communications -> Cellular Accessories" & chr(34) & "," 'Shopping.com Categorization
					strline = strline & "New"	'Condition
					file.WriteLine strline
				end if
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
