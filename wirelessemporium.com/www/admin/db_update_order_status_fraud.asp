<%
pageTitle = "Admin - Update WE Database - Update Order Status - Cancel Fraud Order "
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->


<h3>Update WE Database - Cancel Fraud Order</h3>

<%

orderid = SQLquote(request("orderid"))
ordergrandtotal = SQLquote(request("groundtotal"))

if orderid <> "" then

	SQL = "select orderid from we_orders where orderid ='"&orderid&"' and ordergrandtotal='"&ordergrandtotal&"' and cancelled is null"
	Response.Write("-<p>: "&SQL)
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not rs.eof then
	
		SQL = "UPDATE we_Orders SET refundreason = 99, cancelled = 1 where orderid ='"&orderid&"' and ordergrandtotal='"&ordergrandtotal&"'"
		'Response.Write("-<p>"&SQL)
		oConn.execute SQL
		
		'rollback inventory QTY and NumberofSales
		SQL0 = "SELECT  A.orderdetailid, A.orderID, A.quantity, B.itemID, B.PartNumber FROM we_orderdetails A INNER JOIN we_Items B ON A.itemID=B.itemID WHERE A.orderID = '" & OrderID & "' ORDER BY B.PartNumber"
		'Response.Write("-<p>"&SQL0)
		
		set RS0 = Server.CreateObject("ADODB.Recordset")
		RS0.open SQL0, oConn, 3, 3
		if not rs0.eof then
			Do while not rs0.eof 
			thisPartNumber = rs0("PartNumber")
			'Response.Write("<p>part number: "&thisPartNumber )
			thisQuantity = rs0("quantity")
			thisItemID = rs0("itemID")
			SQLPart = "SELECT ItemKit_NEW FROM we_items WHERE itemID='" & thisItemID & "'"
			set rsPart = server.createobject("ADODB.recordset")
			rsPart.open SQLPart, oConn, 3, 3
			'Response.Write("-<p>part: "&SQLPart)
			if not rsPart.eof then
				KIT = rsPart("ItemKit_NEW")
			else
				KIT = null
			end if
			'Response.Write("<br>-KIT: "&KIT)
			call UpdateInvQty(thisPartNumber, thisQuantity, KIT)
			call NumberOfSales(thisItemID, thisQuantity, KIT)
			RS0.movenext
			Loop
		end if
		response.write "<form><p align=""left""><input type='button' onclick='history.back();' class='smltext' value='BACK'></p></form>" & vbcrlf

	else
		Response.Write("Error! Please double check this is a active order!")
		response.write "<form><p align=""left""><input type='button' onclick='history.back();' class='smltext' value='BACK'></p></form>" & vbcrlf
		
	end if
else
	Response.Write("Error! Record not found can not make the update")
	response.write "<form><p align=""left""><input type='button' onclick='history.back();' class='smltext' value='BACK'></p></form>" & vbcrlf
end if

sub UpdateInvQty(nPartNumber, nProdQuantity, KIT)
	' Updates inv_qty for the Master Part (item with the same Part Number as the item purchased which does not have -1 inv_qty)
	' If the inv_qty would be adjusted to less than or equal to zero, then all items with the same Part Number as the item purchased are set to zero
	if isNull(KIT) then
		SQL = "SELECT itemID,typeID,vendor,inv_qty FROM we_items WHERE PartNumber = '" & nPartNumber & "' AND inv_qty > -1"
	else
		SQL = "SELECT itemID,typeID,vendor,inv_qty FROM we_items WHERE itemID IN (" & KIT & ")"
	end if
	set oRsItemInfo2 = Server.CreateObject("ADODB.Recordset")
	oRsItemInfo2.open SQL, oConn, 3, 3
	do until oRsItemInfo2.eof
		updateItemID = oRsItemInfo2("itemID")
		curQty = oRsItemInfo2("inv_qty")
		if curQty + nProdQuantity > 0 then
			decreaseSQL = "UPDATE we_items SET inv_qty = inv_qty + " & nProdQuantity & " WHERE itemID = '" & updateItemID & "'"
			'response.write "<li>" & decreaseSQL & "</li>" & vbcrlf
			oConn.execute(decreaseSQL)
			On Error Resume Next
			sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & updateItemID & "," & curQty & "," & nProdQuantity & "," & orderid & "," & session("adminID") & ",'Cancel Fraud Order','" & now & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
			On Error Goto 0
		else
			sql = "select itemID, inv_Qty from we_items where partNumber = '" & nPartNumber & "' and master = 1"
			session("errorSQL") = sql
			set partRS = oConn.execute(sql)
			
			updateItemID = partRS("itemID")
			curQty = partRS("inv_qty")
			
			decreaseSQL = "UPDATE we_items SET inv_qty = 0 WHERE PartNumber = '" & nPartNumber & "'"
			if oRsItemInfo2("typeID") = 2 then decreaseSQL = decreaseSQL & " AND inv_qty > -1"
			if oRsItemInfo2("typeID") <> 3 and oRsItemInfo2("vendor") <> "CM" and oRsItemInfo2("vendor") <> "DS" and oRsItemInfo2("vendor") <> "MLD" then
				' Send zero-inventory e-mail to Tony
				cdo_from = "Automatic E-Mail from Wirelessemporium.com<sales@wirelessemporium.com>"
				cdo_subject = nPartNumber & " is out of stock!"
				cdo_body = nPartNumber & " is out of stock!"
				cdo_to = "tony@wirelessemporium.com,charles@wirelessemporium.com"
				CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
			end if
			'response.write "<li>" & decreaseSQL & "</li>" & vbcrlf
			oConn.execute(decreaseSQL)
			
			On Error Resume Next
			sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & updateItemID & "," & curQty & "," & nProdQuantity & "," & orderid & "," & session("adminID") & ",'Cancel Fraud Order','" & now & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
			On Error Goto 0
			
			Response.Write("<p><b> Fraud Order Cancelled! Inv Qty update succeed!</b></p>")
		end if
		oRsItemInfo2.movenext
	loop
	oRsItemInfo2.close
	set oRsItemInfo2 = nothing
end sub

sub NumberOfSales(nIdProd, QTY, KIT)
	if isNull(KIT) then
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID = '" & nIdProd & "'"
	else
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID IN (" & KIT & ")"
	end if
	'response.write "<li>" & SQL & "</li>" & vbcrlf
	oConn.execute(SQL)
	Response.Write("<p><b> Fraud Order Cancelled! number Of Sales update succeed!</b></p>")

end sub

%>
<p>
  <!--#include virtual="/includes/admin/admin_bottom.asp"-->
</p>
