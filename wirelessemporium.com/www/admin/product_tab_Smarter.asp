<%
pageTitle = "Create Smarter Product List TXT"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 9000 'seconds

dim fs, file, filename, path
'Map the file name to the physical path on the server.
filename = "Wireless Emporium.txt"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List upload for Smarter</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='../tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.Write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	Response.Write("Creating " & filename & ".<br>")
	Set file = fs.CreateTextFile(path)
	dim SQL, RS
	SQL = "SELECT A.itemID,A.typeID,A.PartNumber,A.brandID,A.itemDesc,A.itemPic,A.price_Retail,A.price_Our,A.inv_qty,A.Sports,A.flag1,A.itemLongDetail,A.UPCCode,"
	SQL = SQL & "B.brandName,C.modelName"
	SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
	SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " AND A.typeid <> 16"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "Product Name" & vbtab & "Product Description" & vbtab & "Manufacturer Name" & vbtab & "Smarter.com Categorization" & vbtab & "MPN/UPC/ISBN/UniqueCode" & vbtab & "Sale Price" & vbtab & "Product URL" & vbtab & "Image URL" & vbtab & "Stock Availability" & vbtab & "Product Condition" & vbtab & "Shipping Costs"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
				set RS2 = CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				strItemDesc = replace(RS("itemDesc"),"+","&")
				strItemLongDetail = replace(RS("itemLongDetail"),vbcrlf," ")
				if isnull(RS("BrandName")) then
					BrandName = "Universal"
				else 
					BrandName = RS("BrandName")
				end if
				
				select case RS("typeID")
					case 1 : catName = "Batteries"
					case 3 : catName = "Face Plates"
					case 2 : catName = "Chargers"
					case 5 : catName = "Hands Free Kits"
					case 13 : catName = "Data Cables"
					case 7 : catName = "Covers & Cases"
					case 5 : catName = "Headsets"
					case else : catName = "Miscellaneous Cell Phone Accessories"
				end select
				
				strline = strItemDesc & vbtab
				strline = strline & stripCallToAction(strItemLongDetail) & vbtab
				strline = strline & BrandName & vbtab
				strline = strline & "Electronics > Cell Phone Accessories > " & catName & vbtab
				strline = strline & "WE-" & RS("itemID") & vbtab
				strline = strline & RS("price_Our") & vbtab
				strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?id=smarter" & vbtab
				strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab
				strline = strline & "YES" & vbtab
				strline = strline & "New" & vbtab
				strline = strline & "FREE"
				file.WriteLine strline
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub

function stripCallToAction(val)
	stripCallToAction = val
	dim a
	a = inStr(1,stripCallToAction,"why pay ridiculous prices",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"don't settle",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"order this quality",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"order this convenient",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"buy this quality",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"buy this convenient",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"order today",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"remember - wireless emporium",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"you won't find a better price",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"buy this stylish",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"buy this vertical",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"for the same product at",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"we're confident you'll be happy",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	stripCallToAction = replace(stripCallToAction,"don't pay retail prices","",1,9,1)
end function
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
