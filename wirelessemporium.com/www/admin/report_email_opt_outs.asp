<%
response.buffer = false
pageTitle = "E-Mail Opt-Outs for WE Mailing List"
header = 1
Server.ScriptTimeout = 1000 'seconds
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<%
if request("createCSV") <> "" then
	dim fs, file, filename, path
	filename = "WE_emailopt_out.txt"
	path = Server.MapPath("tempCSV") & "\" & filename
	path = replace(path,"admin\","")
	Set fs = CreateObject("Scripting.FileSystemObject")
	response.write "<b>Create File:</b><br>"
	DeleteFile(path)
	CreateFile(path)
	response.write "Here is the <a href='/tempCSV/" & filename & "'>file</a>"
else
	%>
	<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
		<tr bgcolor="#CCCCCC">
			<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
		</tr>
		<tr>
			<td width="47%" valign="middle">
				<table width="100%" border="0" cellspacing="0" cellpadding="15">
					<tr>
						<td class="normalText">
							<form name="frmCreateCSV" action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
								<p>E-Mail Opt-Outs for WE Mailing List</p>
								<p><input type="submit" name="createCSV" value="Create CSV"></p>
							</form>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr bgcolor="#CCCCCC">
			<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
		</tr>
	</table>
	<%
end if

sub DeleteFile(path)
	if fs.FileExists(path) then
		response.write "<p>Deleting " & filename & ".<br>"
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	response.write "Creating " & filename & ".</p>"
	Set file = fs.CreateTextFile(path)
	SQL = 	"select	email, datetimeentd, 'WE' siteid " & vbcrlf & _
			"from	we_emailopt_out" & vbcrlf & _
			"union" & vbcrlf & _
			"select	email, datetimeentd, 'CO' siteid " & vbcrlf & _
			"from	co_emailopt_out" & vbcrlf & _
			"union" & vbcrlf & _
			"select	email, datetimeentd, 'PS' siteid " & vbcrlf & _
			"from	ps_emailopt_out" & vbcrlf & _
			"union" & vbcrlf & _
			"select	email, datetimeentd, 'CA' siteid " & vbcrlf & _
			"from	ca_emailopt_out" & vbcrlf & _
			"order by 3, 2"
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.Open SQL, oConn, 0, 1
	file.WriteLine "email,DateTimeEntd,siteid"
	if not RS.EOF then
		do while not RS.eof
			strline = RS("email") & vbTab
			strline = strline & RS("DateTimeEntd") & vbTab
			strline = strline & RS("siteid")
			file.WriteLine strline
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
