<%
response.buffer = false
pageTitle = "Create StreetPrices Product List CSV"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
dim fs, file, filename, path
'Map the file name to the physical path on the server.
filename = "productList_streetprices.csv"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Create StreetPrices Product List CSV</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							Response.Write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='../tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.Write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	response.write("Creating " & filename & ".<br>")
	set file = fs.CreateTextFile(path)
	dim SQL, RS
	SQL = "SELECT A.itemID, A.PartNumber, A.itemDesc, A.ItemLongDetail, A.price_our, A.itemPic, A.inv_qty, A.hideLive, A.UPCCode, B.brandName"
	SQL = SQL & " FROM we_items A INNER JOIN we_brands B ON A.brandID = B.brandID"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "Manufacturer,MFR Part#,UPC,DistributorPart#,ProductTitle,ProductDescriptionShort,ProductDescriptionLong,ProductCondition,Price,ShipCost,StockStatus,ProductURL,ImageURL"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
				set RS2 = CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				strline = RS("brandName") & ","
				strline = strline & chr(34) & "WE-" & RS("ItemId") + 5000 & chr(34) & ","
				strline = strline & chr(34) & RS("UPCCode") & chr(34) & ","
				strline = strline & chr(34) & chr(34) & ","
				strline = strline & chr(34) & RS("itemDesc") & chr(34) & ","
				strline = strline & chr(34) & RS("itemDesc") & chr(34) & ","
				strline = strline & chr(34) & replace(RS("itemLongDetail"),vbcrlf," ") & chr(34) & ","
				strline = strline & chr(34) & "NEW" & chr(34) & ","
				strline = strline & chr(34) & formatCurrency(RS("price_our")) & chr(34) & ","
				strline = strline & chr(34) & "FREE" & chr(34) & ","
				strline = strline & chr(34) & "YES" & chr(34) & ","
				strline = strline & chr(34) & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?refer=streetprices" & chr(34) & ","
				strline = strline & chr(34) & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & chr(34)
				file.WriteLine strline
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
