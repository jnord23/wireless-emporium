<%
dim thisUser, adminID
pageTitle = "WE Admin Site - Faceplate Search"
header = 1
thisUser = Request.Cookies("username")
adminID = Request.Cookies("adminID")
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="800">
	<tr><td style="padding-top:10px;"><a href="/admin/db_search_brands.asp?searchID=6">Select Again</a></td></tr>
    <tr>
    	<td style="padding-top:10px;" valign="top" width="100%">
<%
	dim brandID, modelID, BrandName, ModelName, Faceplates, whereSQL
	if request.querystring("charms") = "1" then
		brandID = 0
		modelID = 0
		BrandName = "Universal"
		ModelName = ""
		Faceplates = " Charms"
		whereSQL = " AND (PartNumber LIKE 'UNI-CHM-%')"
	else
		brandID = request.form("brandID")
		modelID = request.form("modelID")
		SQL = "SELECT brandName FROM we_Brands WHERE brandID='" & brandID & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 0, 1
		if not RS.eof then BrandName = RS("brandName")
		SQL = "SELECT modelName FROM we_Models WHERE modelID='" & modelID & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 0, 1
		if not RS.eof then ModelName = RS("modelName")
		Faceplates = " Faceplates"
		whereSQL = " AND (typeID = 3 OR typeID = 7)"
	end if
	
	SQL = "SELECT itemID, PartNumber, itemDesc, itemPic, vendor, price_Our, COGS, inv_qty, hideLive, flag1, DesignType FROM we_Items"
	SQL = SQL & " WHERE modelID = '" & modelID & "'"
	SQL = SQL & whereSQL
	SQL = SQL & " ORDER BY typeID,itemDesc"
	'response.write "<h3>" & SQL & "</h3>" & vbcrlf
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if RS.eof then
		response.write "<h3>No" & Faceplates & " Found for " & BrandName & "&nbsp;" & ModelName & "</h3>" & vbcrlf
	else
		%>
		<table width="100%" border="1" cellpadding="0" cellspacing="0" align="center">
			<tr>
			<%
			aCount = 0
			nCount = -1
			do until RS.eof
				aCount = aCount + 1
				nCount = nCount + 1
				if (nCount mod 4) = 0 then
					response.write "</tr><tr>" & vbcrlf
					nCount = 0
				end if
				%>
				<td width="25%" valign="top" align="center">
					<div style="width:100%; float:left;" class="smltext">
                    	<div style="float:left; width:100%; height:100px; padding:5px;"><a href="javascript:faceplateDetail('<%=RS("itemPic")%>');"><img src="/productpics/thumb/<%=RS("itemPic")%>" border="0" width="100" height="100"></a></div>
                        <div style="float:left; width:50%; font-weight:bold;"><%=RS("PartNumber")%></div>
                        <div style="float:left; width:50%; font-weight:bold;"><%="CA-" & cDbl(RS("itemID")) + 500%></div>
                        <div style="float:left; width:100%; text-align:left; height:60px; padding:5px;"><%=RS("itemDesc")%></div>
                        <div style="float:left; width:100%; text-align:center; font-weight:bold;">
							<% if RS("inv_qty") < 1 then %>
							<font color="#FF0000">Out of Stock</font>
							<% else %>
							In Stock
							<% end if %>
                        </div>
                    </div>
				</td>
				<%
				RS.movenext
			loop
			%>
			</tr>
		</table>
		<%
	end if
	RS.close
	set RS = nothing
%>
</td></tr></table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
function faceplateDetail(itemPic) {
	var url="faceplates_view.asp?itemPic="+itemPic;
	window.open(url,"invoice","left=0,top=0,width=450,height=450,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
</script>