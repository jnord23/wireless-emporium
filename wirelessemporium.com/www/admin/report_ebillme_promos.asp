<%
pageTitle = "eBillme Promo Code Usage Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<h3>eBillme Promo Code Usage Report</h3>
<p>Orders with differences between Grand Total and Sub Total will show TOTAL in <font color="red">red</font></p>

<%
SQL = "SELECT orderid,accountid,orderdatetime,extOrderNumber,ordersubtotal,ordershippingfee,orderTax,ordergrandtotal,couponid FROM we_orders"
SQL = SQL & " WHERE extOrderType=3 AND approved=1"
SQL = SQL & " AND orderdatetime >= '3/31/2008' AND orderdatetime <= '7/1/2008'"
SQL = SQL & " AND store = 0"
SQL = SQL & " ORDER BY orderdatetime DESC"
'response.write "<h3>" & SQL & "</h3>" & vbCrLf
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3

totalTotal = 0
totalGrandTotal = 0

if RS.eof then
    response.write "No records matched<br><br>So cannot make table..."
else
	%>
	<h3><%=RS.recordcount%> records found</h3>
	<table border="1">
		<tr>
			<td><b>orderid</b></td>
			<td><b>accountid</b></td>
			<td><b>orderdatetime</b></td>
			<td><b>extOrderNumber</b></td>
			<td><b>ordersubtotal</b></td>
			<td><b>ordershippingfee</b></td>
			<td><b>orderTax</b></td>
			<td><b>TOTAL</b></td>
			<td><b>ordergrandtotal</b></td>
			<td><b>couponid</b></td>
		</tr>
		<%
		do until RS.eof
			total = cDbl(RS("ordersubtotal")) + cDbl(RS("ordershippingfee")) + cDbl(RS("orderTax"))
			%>
			<tr>
				<td valign="top"><%=RS("orderid")%></td>
				<td valign="top"><%=RS("accountid")%></td>
				<td valign="top"><%=RS("orderdatetime")%></td>
				<td valign="top"><%=RS("extOrderNumber")%></td>
				<td valign="top"><%=RS("ordersubtotal")%></td>
				<td valign="top"><%=RS("ordershippingfee")%></td>
				<td valign="top"><%=RS("orderTax")%></td>
				<td valign="top">
					<%
					if total > cDbl(RS("ordergrandtotal")) then
						response.write "<font color=""red"">" & total & "</font>" & vbcrlf
					else
						response.write total & vbcrlf
					end if
					totalTotal = totalTotal + total
					totalGrandTotal = totalGrandTotal + cDbl(RS("ordergrandtotal"))
					%>
				</td>
				<td valign="top"><%=RS("ordergrandtotal")%></td>
				<td valign="top"><%=RS("couponid")%></td>
			</tr>
			<%
			RS.movenext
		loop
		%>
		<tr>
			<td valign="top">&nbsp;</td>
			<td valign="top">&nbsp;</td>
			<td valign="top">&nbsp;</td>
			<td valign="top">&nbsp;</td>
			<td valign="top">&nbsp;</td>
			<td valign="top">&nbsp;</td>
			<td valign="top">&nbsp;</td>
			<td valign="top"><b><%=formatCurrency(totalTotal)%></b></td>
			<td valign="top"><b><%=formatCurrency(totalGrandTotal)%></b></td>
			<td valign="top"><b><%=formatCurrency(totalTotal - totalGrandTotal)%></b></td>
		</tr>
	</table>
	<%
end if

RS.close
set RS = nothing
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
