<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Process Phone Orders"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
'	if instr(request.ServerVariables("SERVER_NAME"),"staging.") < 1 then
'		if request.ServerVariables("HTTPS") = "off" then response.Redirect("https://www.wirelessemporium.com/admin/phoneorder.asp")
'	end if

'	response.write "request(""email""):" & request("email") & "<br>"
	siteID = prepInt(request("cbSite"))
	orderid = prepInt(request("hidOrderid"))
	email = request("email")
	sql = "select * from xstore where site_id = '" & siteID & "' order by 1"
	session("errorSQL") = sql
	set rsSite = oConn.execute(sql)
	siteColor = rsSite("color")
	siteName = rsSite("longDesc")
	adminID = session("adminID")
	freeItemTotalLimit = cdbl(15.00)
%>
<!--#include virtual="/includes/asp/inc_getStatesAdmin.asp"-->
<style>
	.preloading-text {display:block; font-size:18px; color:#666; font-weight:bold; padding-top:30px;}
</style>
<form name="frmPhoneOrder" method="post">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="1000">
	<tr>
    	<td width="100%" align="left" style="padding:10px 0px 10px 0px;">
            <table border="0" cellpadding="0" cellspacing="0" align="center" width="1000">
                <tr>
                    <td width="700" align="left" valign="top"><img src="<%=replace(rsSite("logo_big"),"http:","https:")%>" border="0" /></td>
                    <td width="300" align="right" valign="bottom">
						<div style="float:right; width:4px; height:44px;"><img src="/images/admin/phoneorder/header_right.png" border="0" width="4" height="44" /></div>
                        <div style="float:right; width:192px; height:44px; background-image:url(/images/admin/phoneorder/header_middle.png);">
                            <div style="float:left; font-size:14px; color:#FFF; padding:15px 0px 0px 10px; font-weight:bold;">
                                Choose a store:
                            </div>
                            <div style="float:left; font-size:14px; color:#FFF; padding:15px 0px 0px 15px; font-weight:bold;">
                                <select name="cbSite" onChange="document.frmPhoneOrder.submit();">
                                    <option value="0" <%if siteID="0" then%>selected<%end if%> >WE</option>
                                    <option value="2" <%if siteID="2" then%>selected<%end if%> >CO</option>
                                    <option value="1" <%if siteID="1" then%>selected<%end if%> >CA</option>
                                    <option value="3" <%if siteID="3" then%>selected<%end if%> >PS</option>
                                    <option value="10" <%if siteID="10" then%>selected<%end if%> >TM</option>
                                </select>
                            </div>
                        </div>
						<div style="float:right; width:4px; height:44px;"><img src="/images/admin/phoneorder/header_left.png" border="0" width="4" height="44" /></div>
                    </td>
				</tr>
			</table>
		</td>
    </tr>
	<tr>
    	<td width="100%" align="left" style="border-top:2px solid <%=siteColor%>; border-bottom:1px solid <%=siteColor%>;">
            <table border="0" cellpadding="10" cellspacing="0" align="center" width="100%">
                <tr>
                    <td width="590" align="left" style="border-right:1px solid <%=siteColor%>;" valign="top">
                    	<!-- Cart Start -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                            <tr>
                                <td width="400" align="left"><b>ITEM DESCRIPTION</b></td>
                                <td width="110" align="center"><b>QUANTITY</b></td>
                                <td width="80" align="right"><b>SUBTOTAL</b></td>
                            </tr>
                            <tr>
                            	<td colspan="3" width="100%" style="font-size:1px;"><div id="id_currentCart">&nbsp;</div></td>
                            </tr>
                        </table>
						<!-- Cart End -->
                    </td>
                    <!-- Customer History Start -->
                    <td width="390" align="left" valign="top">
                        <table width="390px" border="0" cellspacing="0" cellpadding="2" style="font-family:Arial, Helvetica, sans-serif;">
                            <tr>
                                <td align="center" valign="top" style="width:50px; border-bottom:1px solid #ccc; font-weight:bold;">STORE</td>
                                <td align="center" valign="top" style="width:80px; border-bottom:1px solid #ccc; font-weight:bold;">ORDER ID</td>
                                <td align="center" valign="top" style="width:100px; border-bottom:1px solid #ccc; font-weight:bold;">ORDER DATE</td>
                                <td align="center" valign="top" style="width:60px; border-bottom:1px solid #ccc; font-weight:bold;">ORDER G.TOTAL</td>
                                <td align="center" valign="top" style="width:70px; border-bottom:1px solid #ccc; font-weight:bold;">APPROVED / CANCELLED</td>
                            </tr>
						</table>
                        <div style="width:390px; height:160px; overflow:auto;" id="id_orderhistory"></div>
					</td>
					<!-- Customer History End -->
				</tr>
			</table>
        </td>
	</tr>
    <tr>
    	<td width="100%" align="center" style="padding:20px 0px 0px 10px;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center" valign="top" width="450px">
                        <table width="100%" border="0" cellpadding="2" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                            <tr>
                                <td style="font-size:18px; padding:7px 0px 7px 0px;" nowrap="nowrap">Shipping Address:</td>
                                <td style="font-weight:bold;" align="right">
                                    <div style="float:right;">INDICATES REQUIRED FIELD</div>
                                    <div style="float:right; font-size:18px; color:#F00; padding:0px 0px 0px 5px;">&nbsp;*&nbsp;</div>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" align="left">
                                    <div style="float:left; font-size:18px; color:#F00; padding-right:3px;">*</div>EMAIL ADDRESS:
                                </td>
                                <td align="right" valign="top">
                                	<div><input type="text" name="email" size="40" value="<%=email%>" onblur="getCustomerData(this.value);"></div>
                                	<div style="font-size:10px; color:#666;">* type email address to pull up customer order history</div>
								</td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" align="left">
                                    <div style="float:left; font-size:18px; color:#F00; padding-right:3px;">*</div>FIRST NAME:
                                </td>
                                <td align="right"><input type="text" name="fname" size="40" value=""></td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" align="left">
                                    <div style="float:left; font-size:18px; color:#F00; padding-right:3px;">*</div>LAST NAME:
                                </td>
                                <td align="right"><input type="text" name="lname" size="40" value=""></td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" align="left">
                                    <div style="float:left; font-size:18px; color:#F00; padding-right:3px;">*</div>STREET ADDRESS1:
                                </td>
                                <td align="right"><input type="text" name="sAddress1" size="40" value=""></td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" align="left"><div style="float:left; font-size:18px; color:#F00; padding-right:3px;">&nbsp;</div>STREET ADDRESS2:</td>
                                <td align="right"><input type="text" name="sAddress2" size="40" value=""></td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" align="left">
                                    <div style="float:left; font-size:18px; color:#F00; padding-right:3px;">*</div>CITY:
                                </td>
                                <td align="right">
                                    <div style="position:relative;">
										<input type="text" name="sCity" size="40" value="">
                                        <div id="id_sCity" class="left" style="position:absolute; display:none; z-index:100; top:0px; right:-265px; width:250px; padding:5px; border-radius:5px; border:2px solid <%=siteColor%>; background-color:#fff;">
                                        	<div class="left" style="width:100%; padding-bottom:3px; border-bottom:1px solid #000;">
                                                <div class="left">City Name Suggestion</div>
                                                <div class="right"><a href="javascript:toggle('id_sCity');">Close</a></div>
                                            </div>
                                        	<div id="id_return_sCity" class="left" style="text-align:center; width:100%; height:200px; overflow:auto;"></div>
                                        </div>
                                    </div>
								</td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" align="left">
                                    <div style="float:left; font-size:18px; color:#F00; padding-right:3px;">*</div>STATE:
                                </td>
                                <td align="right"><select name="sState" onChange="onState();"><%getStates(sstate)%></select></td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" align="left">
                                    <div style="float:left; font-size:18px; color:#F00; padding-right:3px;">*</div>ZIP / POSTAL CODE:
                                </td>
                                <td align="right"><input type="text" name="sZip" value="" onchange="getNewShipping();" onkeyup="getCityByZip()"></td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" align="left">
                                    <div style="float:left; font-size:18px; color:#F00; padding-right:3px;">*</div>PRIMARY PHONE:
                                </td>
                                <td align="right"><input type="text" name="phonenumber" size="40" value=""></td>
                            </tr>
                        </table>
                    </td>
                    <td width="80">&nbsp;</td>
                    <td align="center" valign="top" width="450px">
						<%
                        sql	=	"select	b.shortDesc, a.promoMin, a.promoPercent, a.promoAmount, a.excludeBluetooth, a.freeItemPartNumber" & vbcrlf & _
                                "	,	a.promoDesc, a.startDate, a.expiration" & vbcrlf & _
                                "from	XPromotion a join xstore b" & vbcrlf & _
                                "	on	a.site_id = b.site_id" & vbcrlf & _
                                "where	b.site_id = '" & siteID & "'" & vbcrlf & _
                                "	and	a.isActive = 1" & vbcrlf & _
                                "	and	a.isLive = 1" & vbcrlf & _
                                "	and	a.expiration >= getdate()" & vbcrlf & _
                                "order by a.startDate desc"
                        set rs = oConn.execute(sql)
                        if not rs.eof then
                        %>
                    	<div class="left" style="width:95%; border:1px solid <%=siteColor%>; border-radius:3px; padding:10px;">
                        	<div class="left" style="width:100%; text-align:left;"><h3 style="margin-bottom:10px; border-bottom:1px solid <%=siteColor%>;">Current Promotions</h3></div>
                            <%
								do until rs.eof
							%>
                        	<div class="left" style="width:100%; text-align:left; border-bottom:1px solid #ccc;">
                            	<div class="left" style="font-size:14px; font-weight:bold; background-color:#f2f2f2;"><%=rs("promoDesc")%></div>
                                <div class="clear"></div>
                            	<div class="left" style="padding:5px 0px 5px 0px;">
                                    <div class="left">Minimum Purchase Amount: <span <%if rs("promoMin") > 0 then%>style="background-color:#ccc;"<%end if%>><%=formatcurrency(rs("promoMin"))%></span></div>
                                    <div class="left" style="padding-left:20px;">promoPercent: <span <%if rs("promoPercent") > 0 then%>style="background-color:#ccc;"<%end if%>><%=formatpercent(rs("promoPercent"))%></span></div>
                                    <div class="left" style="padding-left:20px;">promoAmount: <span <%if rs("promoAmount") > 0 then%>style="background-color:#ccc;"<%end if%>><%=formatcurrency(rs("promoAmount"))%></span></div>
								</div>
                            	<div class="left" style="font-weight:bold;">VALID FROM&nbsp;&nbsp;&nbsp;<%=rs("startDate")%>&nbsp;&nbsp;&nbsp;TO&nbsp;&nbsp;&nbsp;<%=rs("expiration")%></div>
                            </div>
                            <%
									rs.movenext
								loop
							%>
                        </div>
						<%
                        end if
						%>
                        <div class="clear"></div>
                        <div style="padding-top:10px; font-weight:bold;" align="left"><input type="checkbox" name="chkUseDiffAddress" value="yes" onclick="setBillAddr(this.checked)" /> CHECK IF BILLING ADDRESS IS DIFFERENT THAN THE SHIPPING ADDRESS.</div>
                        <div style="display:none;" id="billAddr">
                        <table width="450px" border="0" cellpadding="2" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif;">
                            <tr><td colspan="2" style="font-size:18px; padding:7px 0px 7px 0px;" align="left">Billing Address:</td></tr>
                            <tr>
                                <td style="padding-left:10px;" align="left">STREET ADDRESS1:</td>
                                <td align="right"><input name="bAddress1" size="40" value="" type="text"></td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" align="left">STREET ADDRESS2:</td>
                                <td align="right"><input name="bAddress2" size="40" value="" type="text"></td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" align="left">CITY:</td>
                                <td align="right"><input name="bCity" size="40" value="" type="text"></td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" align="left">STATE:</td>
                                <td align="right"><select name="bState"><%getStates(bState)%></select></td>
                            </tr>
                            <tr>
                                <td style="padding-left:10px;" align="left">ZIP / POSTAL CODE:</td>
                                <td align="right"><input name="bZip" size="40" value="" type="text"></td>
                            </tr>
                        </table>
                        </div>
                        <div style="padding-top:30px; font-weight:bold;" align="left"><input name="chkOptMail" value="Y" checked="checked" type="checkbox"> Please notify me via email of exclusive periodic <%=siteName%> coupon code offers*</div>
                        <div style="padding-top:10px; font-weight:bold; font-size:16px; color:#f00;" align="left"><input name="chkMobile" value="Y" type="checkbox"> Mobile Order</div>
                    </td>
                </tr>
            </table>
		</td>
	</tr>
	<tr>
		<td width="100%" align="align" style="padding:5px 0px 20px 10px; border-bottom:1px solid <%=siteColor%>;">
            <table width="450px" border="0" cellpadding="2" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif;">
                <tr><td style="font-size:18px; padding:7px 0px 7px 0px;" width="150">Credit Card</td><td align="right"><img src="/images/admin/phoneorder/ca_cards.gif" border="0" /></td></tr>
                <tr>
                    <td style="padding-left:10px;" align="left"><div style="float:left; font-size:18px; color:#F00; padding-right:3px;">*</div>CARD TYPE:</td>
                    <td align="right">
                        <select name="cc_cardType">
                            <option value="">Select Card Type</option>
                            <option value="VISA">VISA</option>
                            <option value="MC">MASTER CARD</option>
                            <option value="AMEX">AMERICAN EXPRESS</option>
                            <option value="DISC">DISCOVER</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left:10px;" align="left"><div style="float:left; font-size:18px; color:#F00; padding-right:3px;">*</div>CARD HOLDER NAME:</td>
                    <td align="right"><input name="cc_cardOwner" type="text" size="35"></td>
                </tr>
                <tr>
                    <td style="padding-left:10px;" align="left"><div style="float:left; font-size:18px; color:#F00; padding-right:3px;">*</div>CREDIT CARD #:</td>
                    <td align="right"><input name="cardNum" type="text" id="cardNum" size="35" maxlength="16"></td>
                </tr>
                <tr>
                    <td style="padding-left:10px;" align="left"><div style="float:left; font-size:18px; color:#F00; padding-right:3px;">*</div>SECURITY CODE (CVV2):</td>
                    <td align="right">
                        <input name="secCode" type="text" id="secCode" size="4" maxlength="4">&nbsp;&nbsp;
                        <a style="text-decoration:none; font-size:11px;" onclick="window.open('/cart/cvv2help.asp','Cvv2help','width=800,height=600,scrollbars=no');return false;" href="javascript:void(0)">(WHAT'S THIS?)</a>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left:10px;" align="left"><div style="float:left; font-size:18px; color:#F00; padding-right:3px;">*</div>EXPIRATION DATE:</td>
                    <td align="right">
                        <select name="cc_month">
                            <option value="01">January (1)</option>
                            <option value="02">February (2)</option>
                            <option value="03">March (3)</option>
                            <option value="04">April (4)</option>
                            <option value="05">May (5)</option>
                            <option value="06">June (6)</option>
                            <option value="07">July (7)</option>
                            <option value="08">August (8)</option>
                            <option value="09">September (9)</option>
                            <option value="10">October (10)</option>
                            <option value="11">November (11)</option>
                            <option value="12">December (12)</option>
                        </select>
                        &nbsp;
                        <select name="cc_year">
                            <%
                            for countYear = 0 to 14
                                response.write "<option value=""" & right(cStr(year(date) + countYear),2) & """>" & cStr(year(date) + countYear) & "</option>" & vbcrlf
                            next
                            %>
                        </select>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td width="100%" align="center" style="padding:30px 0px 0px 10px;">
			<input type="image" onclick="return doSubmit();" src="/images/admin/phoneorder/submitButton.png" width="255" height="40" alt="Place My Order">
        </td>
    </tr>
</table>
<input type="hidden" name="hidOrderID" value="" />
<input type="hidden" name="accountid" value="" />
<input type="hidden" name="parentAcctID" value="" />
<div id="id_customerdata" style="display:none;"></div>
<div id="terryTest"></div>
</form>
<!-- JSON Autocomplete -->
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<style>
	.ui-autocomplete-loading { background: white url('/images/preloading16x16.gif') right center no-repeat; }
</style>
<script src="/includes/js/validate_new.js"></script>
<script>
	var gblBrandID = 0;
	var gblModelID = 0;
	var gblTypeID = 0;
	var gblAccountID = 0;
	var gblParentAccID = 0;
	var gblCurShipID = 0;
	var gblSiteID = <%=siteID%>;
	var gblPromoCode = '';
	var gblCustomShipping = Number(-1);

	window.onload = function() {
		email = '<%=email%>';
		<% if orderid > 0 then %>
			setValue('dumpZone', '');
			ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=pullorder&siteid=<%=siteID%>&adminID=<%=adminID%>&orderid=<%=orderid%>','dumpZone');
			checkUpdate('pullorder');
		<% else %>
			getCurrentCart(gblPromoCode);
		<% end if %>

		if (email != "")
		{
			setValue('id_customerdata', '');
			getCustomerData(email);
		}
	}

	function pullData(searchType, searchKey)
	{
		if (searchType == 'k') {
			setValue('dumpZone', '');
			setValue('id_searchresult', '<div style="display:block;" align="center"><img src="/images/loading.gif" border="0" /></div>');
			ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=ontype&siteid=<%=siteID%>&adminID=<%=adminID%>&searchKey='+searchKey+'&brandid='+gblBrandID+'&modelid='+gblModelID+'&typeid='+gblTypeID,'dumpZone');
			checkUpdate("ontype");
		} else if (searchType == 'p') {
			setValue('dumpZone', '');
			setValue('id_searchresult', '<div style="display:block;" align="center"><img src="/images/loading.gif" border="0" /></div>');
			ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=ontype&siteid=<%=siteID%>&adminID=<%=adminID%>&partnumber='+searchKey+'&searchKey='+searchKey+'&brandid='+gblBrandID+'&modelid='+gblModelID+'&typeid='+gblTypeID,'dumpZone');
			checkUpdate("ontype");
		} else if (searchType == 'i') {
			setValue('dumpZone', '');
			setValue('id_searchresult', '<div style="display:block;" align="center"><img src="/images/loading.gif" border="0" /></div>');
			ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=ontype&siteid=<%=siteID%>&adminID=<%=adminID%>&itemid='+searchKey,'dumpZone');
			checkUpdate("ontype");
		}
	}

	function setValue(id,val) {
		var o = document.getElementById(id);
		if (o != null) o.innerHTML = val;
	}

	function getCityByZip() {
		zipcode = document.frmPhoneOrder.sZip.value;
		if (zipcode.length >= 5) {
			document.getElementById('id_return_sCity').innerHTML = 'Loading..';
			ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=getcitybyzip&sZip=' + zipcode,'id_return_sCity');
			document.getElementById('id_sCity').style.display = '';
		}
	}

	function populateSCityName(cityname) {
		document.frmPhoneOrder.sCity.value = cityname;
		document.getElementById('id_sCity').style.display = 'none';
	}

	function getNewShipping() {
		nSubTotal = document.frmPhoneOrder.nSubTotal.value;
		sState = document.frmPhoneOrder.sState.options[document.frmPhoneOrder.sState.selectedIndex].value

		var nCATax = CurrencyFormatted(nSubTotal * <%=Application("taxMath")%>);
		if (sState == "CA") {
			setValue("CAtaxMsg","(Additional <%=Application("taxDisplay")%>% Tax for Shipments Within CA)");
			setValue("CAtax","$" + nCATax);
			document.frmPhoneOrder.nCATax.value = nCATax;
		} else {
			setValue("CAtaxMsg","");
			setValue("CAtax","$0.00");
			document.frmPhoneOrder.nCATax.value = 0;
		}

		if (document.getElementById('id_cbShip') != null) gblCurShipID = document.frmPhoneOrder.cbShip.options[document.frmPhoneOrder.cbShip.selectedIndex].value;
		setValue('dumpZone', '');
		sState = document.frmPhoneOrder.sState.value;
		sZip = document.frmPhoneOrder.sZip.value;
		ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=newshipping&siteid=<%=siteID%>&adminID=<%=adminID%>&sState=' + sState + '&sZip=' + sZip + '&curShipID=' + gblCurShipID,'dumpZone');
		checkUpdate('newshipping');
	}

	function onModel(modelid) {
		gblModelID = modelid;
		if ((gblTypeID > 0)&&(gblModelID > 0)) onType(gblTypeID);
	}

	function onBrand(brandid) {
		gblBrandID = brandid;
		ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=onbrand&siteid=<%=siteID%>&adminID=<%=adminID%>&brandid='+gblBrandID,'id_search_model');
		if ((gblTypeID == 8)||(gblTypeID == 16)) onType(gblTypeID);
	}

	function onType(typeid) {
		gblTypeID = typeid;
		if ((gblModelID > 0)||(gblBrandID > 0)||(gblTypeID == 8)||(gblTypeID == 16)) {
			setValue("dumpZone","");
			setValue('id_searchresult', '<div style="display:block;" align="center"><img src="/images/loading.gif" border="0" /></div>');
			ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=ontype&siteid=<%=siteID%>&adminID=<%=adminID%>&brandid='+gblBrandID+'&modelid='+gblModelID+'&typeid='+gblTypeID,'dumpZone');
			checkUpdate("ontype");
		}
	}

	function onState() {
		getNewShipping();
	}

	function setBillAddr(setting) {
		if (setting) document.getElementById("billAddr").style.display = '';
		else document.getElementById("billAddr").style.display = 'none';
	}

	function getCurrentCart(promocode) {
		if (document.getElementById('id_cbShip') != null) gblCurShipID = document.frmPhoneOrder.cbShip.options[document.frmPhoneOrder.cbShip.selectedIndex].value;
		setValue("dumpZone","");
		setValue("id_currentCart", '<div class="preloading-text" align="center">Loading...</div>');
		ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=getcurrentcart&siteid=<%=siteID%>&adminID=<%=adminID%>&promocode='+promocode,'dumpZone')
		checkUpdate('getcurrentcart');
	}

	function getCustomerData(email) {
		gblAccountID = 0;
		gblParentAccID = 0;
		setValue("id_customerdata","");
		setValue("id_orderhistory", '<div class="preloading-text" align="center">Loading...</div>');
		ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=orderhistory&email=' + email,'id_orderhistory');

		ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=customerdata&siteid=<%=siteID%>&email=' + email,'id_customerdata');
		checkUpdate('customerdata');
	}

	function checkUpdate(checkType)
	{
		dumpZone = document.getElementById('dumpZone').innerHTML;
		customerZone = document.getElementById('id_customerdata').innerHTML;
		if (checkType == "customerdata") {
			if ("" == customerZone) setTimeout('checkUpdate(\'customerdata\')', 50);
			else if ("" != customerZone) {
				displayData(customerZone);
				getNewShipping();
			}
		} else if (checkType == "getcurrentcart") {
			if ("" == dumpZone) setTimeout('checkUpdate(\'getcurrentcart\')', 50);
			else if ("" != dumpZone) {
				setValue("id_currentCart",dumpZone);
				newHeight = document.getElementById('id_currentCart').scrollHeight;
				document.getElementById('id_orderhistory').style.height = newHeight + 'px';
				getNewShipping();
			}
		} else if (checkType == "ontype") {
			if ("" == dumpZone) setTimeout('checkUpdate(\'ontype\')', 50);
			else if ("" != dumpZone) setValue("id_searchresult", dumpZone);
		} else if (checkType == "showsearch") {
			if ("" == dumpZone) setTimeout('checkUpdate(\'showsearch\')', 50);
			else if ("" != dumpZone) setValue("popBox", dumpZone);
		} else if (checkType == "additem") {
			if ("" == dumpZone) setTimeout('checkUpdate(\'additem\')', 50);
			else if ("" != dumpZone) alert(dumpZone);
		} else if (checkType == "addcartitem") {
			if ("" == dumpZone) setTimeout('checkUpdate(\'addcartitem\')', 50);
			else if ("" != dumpZone) {
				var tempEmail = dumpZone;
				getCurrentCart(gblPromoCode);
				if ('added' != tempEmail) {
					document.frmPhoneOrder.email.value = tempEmail;
					setTimeout('getCustomerData(\'' + tempEmail + '\')', 2000);
				}
			}
		} else if (checkType == "updateitem") {
			if ("" == dumpZone) setTimeout('checkUpdate(\'updateitem\')', 50);
			else if ("" != dumpZone) getCurrentCart(gblPromoCode);
		} else if (checkType == "deleteitem") {
			if ("" == dumpZone) setTimeout('checkUpdate(\'deleteitem\')', 50);
			else if ("" != dumpZone) getCurrentCart(gblPromoCode);
		} else if (checkType == 'newshipping') {
			if ("" == dumpZone) setTimeout('checkUpdate(\'newshipping\')', 50);
			else if ("" != dumpZone) {
				setValue('currentShipOptions', dumpZone);
				document.getElementById('id_addNewHere').style.display = '';
				updateGrandTotal();
			}
		} else if (checkType == 'pullorder') {
			if ("" == dumpZone) setTimeout('checkUpdate(\'pullorder\')', 50);
			else if ("" != dumpZone) getCurrentCart(gblPromoCode);
		} else if (checkType == 'updateprice') {
			if ("" == dumpZone) setTimeout('checkUpdate(\'updateprice\')', 50);
			else if ("" != dumpZone) getCurrentCart(gblPromoCode);
		}
	}

	function displayData(dataReturn) {
		if (dataReturn != "no data" && dataReturn != "") {
			var dataArray = dataReturn.split("@@")
			document.frmPhoneOrder.fname.value = dataArray[0];
			document.frmPhoneOrder.lname.value = dataArray[1];
			document.frmPhoneOrder.sAddress1.value = dataArray[2];
			document.frmPhoneOrder.sAddress2.value = dataArray[3];
			document.frmPhoneOrder.sCity.value = dataArray[4];
			var len_sState = document.frmPhoneOrder.sState.length;
			for (i=0;i<len_sState;i++) {
				if (document.frmPhoneOrder.sState.options[i].value == dataArray[5]) {
					document.frmPhoneOrder.sState.selectedIndex = i;
				}
			}
			document.frmPhoneOrder.sZip.value = dataArray[6];
			document.frmPhoneOrder.phonenumber.value = dataArray[7];
			if (dataArray[2] != dataArray[8] || dataArray[6] != dataArray[12]) {
				document.frmPhoneOrder.chkUseDiffAddress.checked = true;
				setBillAddr(true);
				document.frmPhoneOrder.bAddress1.value = dataArray[8];
				document.frmPhoneOrder.bAddress2.value = dataArray[9];
				document.frmPhoneOrder.bCity.value = dataArray[10];
				var len_bState = document.frmPhoneOrder.bState.length;
				for (i=0;i<len_bState;i++) {
					if (document.frmPhoneOrder.bState.options[i].value == dataArray[11]) {
						document.frmPhoneOrder.bState.selectedIndex = i;
					}
				}
				document.frmPhoneOrder.bZip.value = dataArray[12];
			} else {
				document.frmPhoneOrder.chkUseDiffAddress.checked = false;
				setBillAddr(false);
				document.frmPhoneOrder.bAddress1.value = "";
				document.frmPhoneOrder.bAddress2.value = "";
				document.frmPhoneOrder.bCity.value = "";
				document.frmPhoneOrder.bState.selectedIndex = 0;
				document.frmPhoneOrder.bZip.value = "";
			}
			gblAccountID = dataArray[13];
			gblParentAccID = dataArray[14];
			onState();
		} else {
			document.frmPhoneOrder.fname.value = "";
			document.frmPhoneOrder.lname.value = "";
			document.frmPhoneOrder.sAddress1.value = "";
			document.frmPhoneOrder.sAddress2.value = "";
			document.frmPhoneOrder.sCity.value = "";
			document.frmPhoneOrder.sState.selectedIndex = 0;
			document.frmPhoneOrder.sZip.value = "";
			document.frmPhoneOrder.phonenumber.value = "";
			document.frmPhoneOrder.bState.selectedIndex = 0;
			document.frmPhoneOrder.chkUseDiffAddress.checked = false;
			setBillAddr(false);
			document.frmPhoneOrder.bAddress1.value = "";
			document.frmPhoneOrder.bAddress2.value = "";
			document.frmPhoneOrder.bCity.value = "";
			document.frmPhoneOrder.bState.selectedIndex = 0;
			document.frmPhoneOrder.bZip.value = "";
			gblAccountID = 0;
			gblParentAccID = 0;
		}
	}

	function showSearch() {
		gblBrandID = 0;
		gblModelID = 0;
		gblTypeID = 0;
		setValue('dumpZone', "");
		setValue("id_search_current", "");
		setValue("popBox", "");
		ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=showsearch','dumpZone');
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
		checkUpdate("showsearch");
	}

	function getItemByCartID() {
		cartID = document.getElementById('cartID').value;
		setValue('dumpZone', "");
		ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=addCartItem&siteid=<%=siteID%>&adminID=<%=adminID%>&cartSessionID='+cartID,'dumpZone');
		checkUpdate("addcartitem");
	}

	function getItemByID(type) {
		if (type == 'cart') {
			cartID = document.getElementById('cartID').value;
			setValue('dumpZone', "");
			ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=addCartItem&siteid=<%=siteID%>&adminID=<%=adminID%>&cartSessionID='+cartID,'dumpZone');
		} else {
			cartOrderID = document.getElementById('id_orderID').value;
			setValue('dumpZone', "");
			ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=addCartItemByOrderID&siteid=<%=siteID%>&adminID=<%=adminID%>&cartOrderID='+cartOrderID,'dumpZone');
		}
		checkUpdate("addcartitem");
	}

	function reloadSearchBox() {
		$("#id_search_itemid").autocomplete({
			source: "/ajax/ajaxAutocompleteFilter.asp?sType=porder_item&siteid=<%=siteID%>", minLength: 1, select: function( event, ui ) {pullData('i', ui.item.id);}
		});
		$("#id_search_itemdesc").autocomplete({
			source: "/ajax/ajaxAutocompleteFilter.asp?sType=porder_item2&siteid=<%=siteID%>&brandid="+gblBrandID+"&modelid="+gblModelID+"&typeid="+gblTypeID, minLength: 2, select: function( event, ui ) {pullData('p', ui.item.id);}
		});
		$("#id_search_itemdesc").autocomplete( "option", "position", { my : "right top", at: "right bottom" } );
	}

	function closefloatingList() {
		setValue("popBox", "");
		setValue("dumpZone", "");
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
		getCurrentCart(gblPromoCode);
	}

	function printinvoice(orderid,accountid) {
		var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
		window.open(url,"invoice","left=0,top=0,width=1024,height=768,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	}

	function addItemToCart(itemid,price) {
		setValue('dumpZone', '');
		itemQty = document.getElementById('id_itemqty_'+itemid).value;
		if (isNaN(itemQty)) itemQty = 1;
		else if (eval(itemQty) <= 0) itemQty = 1;

		ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=additem&siteid=<%=siteID%>&adminID=<%=adminID%>&itemid='+itemid+'&qty='+itemQty+'&price='+price+'&accountid='+gblAccountID,'dumpZone');
		checkUpdate('additem');
	}

	function updateItem(itemid) {
		setValue('dumpZone', '');
		itemQty = document.getElementById('id_curqty_'+itemid).value;
		if (isNaN(itemQty)) itemQty = 1;
		else if (eval(itemQty) <= 0) itemQty = 1;

		ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=updateitem&siteid=<%=siteID%>&adminID=<%=adminID%>&itemid='+itemid+'&qty='+itemQty,'dumpZone');
		checkUpdate('updateitem');
	}

	function deleteItem(itemid) {
		setValue('dumpZone', '');
		ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=deleteitem&siteid=<%=siteID%>&adminID=<%=adminID%>&itemid='+itemid,'dumpZone');
		checkUpdate('deleteitem');
	}

	function pullOrder(pSiteID,orderid,email) {
		if (gblSiteID != pSiteID) {
			document.frmPhoneOrder.cbSite.value = pSiteID;
			document.frmPhoneOrder.hidOrderID.value = orderid;
			document.frmPhoneOrder.submit();
		} else {
			setValue('dumpZone', '');
			ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=pullorder&siteid=<%=siteID%>&adminID=<%=adminID%>&orderid='+orderid,'dumpZone');
			checkUpdate('pullorder');
		}
	}

	function applyPromo() {
		gblPromoCode = document.frmPhoneOrder.sPromoCode.value;
		getCurrentCart(gblPromoCode);
	}

	function applyItemDiscount(itemid,origPrice,newPrice) {
		setValue('dumpZone', '');
		if (isNaN(newPrice)) newPrice = 0;
		else if (eval(newPrice) <= 0) newPrice = 0;

		if (isNaN(origPrice)) origPrice = 0;
		else if (eval(origPrice) <= 0) origPrice = 0;

		curFreeItemTotal = Number(document.frmPhoneOrder.nFreeItemTotal.value);
		curFreeItemTotal = curFreeItemTotal + origPrice;
		if (curFreeItemTotal > Number(<%=freeItemTotalLimit%>)) {
			alert("Free item's total amount should be less than <%=freeItemTotalLimit%>!");
		} else {
			ajax('/ajax/admin/ajaxPhoneOrder.asp?uType=updateprice&siteid=<%=siteID%>&adminID=<%=adminID%>&itemid='+itemid+'&price='+newPrice,'dumpZone');
			checkUpdate('updateprice');
		}
	}

	function applyCustomShipping() {
		shippingCost = document.frmPhoneOrder.txtShippingTotal.value;
		if (isNaN(shippingCost)) shippingCost = -1;
		else if (eval(shippingCost) <= 0) shippingCost = -1;

		if (shippingCost == -1)
		{
			updateGrandTotal();
			return false;
		}

		gblCustomShipping = Number(shippingCost);

		shippingCost = Number(shippingCost);
		caTax = Number(document.frmPhoneOrder.nCATax.value);
		subTotal = Number(document.frmPhoneOrder.nSubTotal.value);
		discountTotal = Number(document.frmPhoneOrder.nDiscountTotal.value);

		document.frmPhoneOrder.nShippingTotal.value = CurrencyFormatted(shippingCost);
		document.frmPhoneOrder.txtShippingTotal.value = CurrencyFormatted(shippingCost);

		var GrandTotal = Number(subTotal) + Number(caTax) + Number(shippingCost) - Number(discountTotal);
		if (GrandTotal < 0) GrandTotal = 0;
		setValue("GrandTotal","$" + CurrencyFormatted(GrandTotal));
		document.frmPhoneOrder.nGrandTotal.value = CurrencyFormatted(GrandTotal);
	}

	function updateGrandTotal() {
		shippingCost = 0;
		caTax = Number(document.frmPhoneOrder.nCATax.value);
		subTotal = Number(document.frmPhoneOrder.nSubTotal.value);
		discountTotal = Number(document.frmPhoneOrder.nDiscountTotal.value);
		if (document.getElementById('id_cbShip') != null) {
			gblCurShipID = document.frmPhoneOrder.cbShip.options[document.frmPhoneOrder.cbShip.selectedIndex].value;
			shippingCost = document.getElementById('id_shiptype_'+gblCurShipID).value;
			shiptypeDesc = document.getElementById('id_shiptypedesc_'+gblCurShipID).value;

			if ((gblCustomShipping >= 0)&&(gblCustomShipping <= shippingCost))
				shippingCost = gblCustomShipping;
			else
				gblCustomShipping = Number(-1);

			document.frmPhoneOrder.nShippingTotal.value = shippingCost;
			document.frmPhoneOrder.strShipType.value = shiptypeDesc;
			document.frmPhoneOrder.shiptypeID.value = gblCurShipID;
			document.frmPhoneOrder.txtShippingTotal.value = CurrencyFormatted(shippingCost);
		}
		var GrandTotal = Number(subTotal) + Number(caTax) + Number(shippingCost) - Number(discountTotal);
		if (GrandTotal < 0) GrandTotal = 0;
		setValue("GrandTotal","$" + CurrencyFormatted(GrandTotal));
		document.frmPhoneOrder.nGrandTotal.value = CurrencyFormatted(GrandTotal);
	}

	function CurrencyFormatted(amount) {
		var i = parseFloat(amount);
		if(isNaN(i)) { i = 0.00; }
		var minus = '';
		if(i < 0) { minus = '-'; }
		i = Math.abs(i);
		i = parseInt(i * 100);
		i = i / 100;
		s = new String(i);
		if(s.indexOf('.') < 0) { s += '.00'; }
		if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
		s = minus + s;
		return s;
	}

	function doSubmit() {
		shippingDiscount = 0;
		if (gblCustomShipping >= 0) {
			origShippingPrice = CurrencyFormatted(document.getElementById('id_shiptype_'+gblCurShipID).value);
			shippingDiscount = CurrencyFormatted(origShippingPrice - gblCustomShipping);
		}

		var f = document.frmPhoneOrder;
		bValid = true;

		CheckValidNEW(f.email.value, "Email is a required field!");
		CheckValidNEW(f.fname.value, "Your First Name is a required field!");
		CheckValidNEW(f.lname.value, "Your Last Name is a required field!");
		CheckValidNEW(f.phonenumber.value, "Your Phone Number is a required field!");
		CheckValidNEW(f.sAddress1.value, "Your Address (for Shipping Address) is a required field!");
		CheckValidNEW(f.sCity.value, "Your City (for Shipping Address) is a required field!");
		sString = f.sState.options[f.sState.selectedIndex].value;
		CheckValidNEW(sString, "State (for Shipping Address) is a required field!");
		CheckValidNEW(f.sZip.value, "Your Zip Code (for Shipping Address) is a required field!");
		if (f.sZip.value.length < 5) {
			alert("You must enter a valid US or Canadian postal code");
			bValid = false;
		}
		if (f.chkUseDiffAddress.checked == true) {
			CheckValidNEW(f.bAddress1.value, "Your Address (for Billing Address) is a required field!");
			CheckValidNEW(f.bCity.value, "Your City (for Billing Address) is a required field!");
			sString = f.bState.options[f.bState.selectedIndex].value;
			CheckValidNEW(sString, "State (for Billing Address) is a required field!");
			CheckValidNEW(f.bZip.value, "Your Zip Code (for Billing Address) is a required field!");
			if (f.bZip.value.length < 5) {
				alert("You must enter a valid US or Canadian postal code (for Billing Address).");
				bValid = false;
			}
		}
		sString = f.cc_cardType.options[f.cc_cardType.selectedIndex].value;
		CheckValidNEW(sString, "Credit Card Type is a required field!");
		CheckValidNEW(f.cc_cardOwner.value, "Your Name (as it appears on your credit card) is a required field!");
		CheckValidNEW(f.cardNum.value, "Your Credit Card Number is a required field!");
		CheckValidNEW(f.cc_month.value, "Your Credit Card Expiration Month is a required field!");
		CheckValidNEW(f.cc_year.value, "Your Credit Card Expiration Year is a required field!");
		CheckValidNEW(f.secCode.value, "Your Credit Card Security Code is a required field!");

		if (bValid) {
			document.frmPhoneOrder.accountid.value = gblAccountID;
			document.frmPhoneOrder.parentAcctID.value = gblParentAccID;
			document.frmPhoneOrder.nShippingDiscountTotal.value = shippingDiscount;
			document.frmPhoneOrder.action = '/cart/process/processingAdmin';
			document.frmPhoneOrder.submit();
		}

		return false;
	}
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->