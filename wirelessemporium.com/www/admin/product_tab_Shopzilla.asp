<%
pageTitle = "Product List upload for Shopzilla"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 9000 'seconds

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
'Map the file name to the physical path on the server.
filename = "productList_Shopzilla.txt"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List upload for Shopzilla</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							response.write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='../tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		response.write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	Response.Write("Creating " & filename & ".<br>")
	set file = fs.CreateTextFile(path)
	dim SQL, RS
	SQL = "SELECT A.itemID,A.typeID,A.PartNumber AS MPN,A.itemDesc,A.itemLongDetail,A.HandsfreeType,"
	SQL = SQL & " A.UPCCode,A.itemPic,A.price_Retail,A.price_Our,A.price_Buy,A.inv_qty,A.Sports,"
	SQL = SQL & " A.Bullet1,A.Bullet2,A.Bullet3,A.Bullet4,A.Bullet5,A.Bullet6,"
	SQL = SQL & " B.brandName,C.modelName"
	SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
	SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " AND A.typeID <> 8 AND A.typeID <> 16"
	'Take out a bunch of products (added back in on 11-18-2010)
	'SQL = SQL & " AND A.PartNumber NOT LIKE 'DEC%' AND A.PartNumber NOT LIKE 'FP2%' AND A.PartNumber NOT LIKE 'FP3%' AND A.PartNumber NOT LIKE 'FP5%' AND A.PartNumber NOT LIKE 'BT1%'"
	'Trucated list only leaving decale skins
	'SQL = SQL & " AND A.PartNumber NOT LIKE 'DEC%' AND A.PartNumber NOT LIKE 'FP2%' AND A.PartNumber NOT LIKE 'FP3%' AND A.PartNumber NOT LIKE 'FP5%' and A.PartNumber NOT LIKE 'BT1%'"
	'Very basic filter
	'SQL = SQL & " AND A.PartNumber NOT LIKE 'DEC%'"
	SQL = SQL & " AND (A.UPCCode IS NULL OR A.UPCCode = '')"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		dim strItemDesc, strItemLongDetail, BrandName, category
		file.WriteLine "Category" & vbtab & "Manufacturer" & vbtab & "Title" & vbtab & "Description" & vbtab & "Link" & vbtab & "Image" & vbtab & "SKU" & vbtab & "Quantity On Hand" & vbtab & "Condition" & vbtab & "Shipping Weight" & vbtab & "Ship Cost" & vbtab & "Bid" & vbtab & "Promo Text" & vbtab & "UPC" & vbtab & "Price"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("MPN") & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				strItemDesc = RS("itemDesc")
				strItemDesc = Replace(strItemDesc,"+","&")
				strItemLongDetail = RS("itemLongDetail")
				strItemLongDetail = replace(replace(strItemLongDetail,vbcrlf," "),vbtab," ")
				if isnull(RS("BrandName")) then
					BrandName = "Universal"
				else 
					BrandName = RS("BrandName")
				end if
				select case RS("typeID")
					case 1 : category = 8543
					case 2 : category = 8544
					case 3 : category = 8514
					case 4 : category = 8587
					case 5
						if RS("HandsfreeType") = 2 then
							category = 8560
						else
							category = 8561
						end if
					case 6 : category = 8583
					case 7 : category = 8515
					case 12 : category = 9767
					case 13 : category = 9768
					case 14, 17 : category = 8589
				end select
				
				strline = category & vbtab
				strline = strline & BrandName & vbtab
				strline = strline & chr(34) & strItemDesc & chr(34) & vbtab
				strline = strline & chr(34) & removeHTML(strItemLongDetail) & chr(34) & vbtab
				strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?refer=bizrate" & vbtab
				strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab
				strline = strline & RS("itemid") & vbtab
				strline = strline & "In Stock" & vbtab
				strline = strline & "New" & vbtab
				strline = strline & vbtab
				strline = strline & 0 & vbtab
				strline = strline & vbtab
				strline = strline & vbtab
				strline = strline & RS("UPCCode") & vbtab
				strline = strline & RS("price_our")
				file.WriteLine strline
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub

function removeHTML(str)
	removeHTML = replace(str,"<br>"," ",1,99,1)
	removeHTML = replace(replace(removeHTML,"<p>"," ",1,99,1),"</p>"," ",1,99,1)
	removeHTML = replace(replace(removeHTML,"<b>","",1,99,1),"</b>","",1,99,1)
	removeHTML = replace(removeHTML,"<b class='bigtext'>","",1,99,1)
	removeHTML = replace(replace(removeHTML,"<li>","",1,99,1),"</li>","",1,99,1)
	removeHTML = replace(replace(removeHTML,"<ul>","",1,99,1),"</ul>","",1,99,1)
	removeHTML = replace(replace(removeHTML,"<font color=""#FF0000"">","",1,99,1),"</font>","",1,99,1)
end function
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
