<%
pageTitle = "WE Holidays"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
SQL = "SELECT * FROM we_holidays WHERE HolidayDate >= '" & date & "' ORDER BY HolidayDate"
Set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
%>
<h3 align="center">Upcoming Holidays</h3>
<table border="1" cellpadding="2" cellspacing="0" align="center">
	<tr>
		<td><b>Holiday</b></td>
		<td><b>Date</b></td>
	</tr>
	<%
	do until RS.eof
		%>
		<tr>
			<td valign="top"><%=RS("HolidayName")%></td>
			<td valign="top"><%=FormatDateTime(RS("HolidayDate"),1)%></td>
		</tr>
		<%
		RS.movenext
	loop
	%>
</table>
<%
RS.close
Set RS = nothing
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
