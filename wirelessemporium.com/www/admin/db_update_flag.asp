<%
response.buffer = false
pageTitle = "Admin - Update WE Database - Update Flag"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<font face="Arial,Helvetica">
	<p><a href="/admin/db_search_brands.asp?searchID=9">Back to Brands</a></p>
</font>

<%
if request("submitted") = "Update" then
	for a = 1 to request("totalCount")
		if isNumeric(request("flag1" & a)) then
			flag1 = request("flag1" & a)
		else
			flag1 = 0
		end if
		SQL = "UPDATE WE_Items SET flag1='" & flag1 & "' WHERE itemID='" & request("itemID" & a) & "'"
		response.write "<p>" & SQL & "</p>"
		oConn.execute SQL
	next
	%>
	<h3>UPDATED!</h3>
	<%
else
	brandID = request.querystring("brandID")
	SQL = "SELECT A.itemID, A.PartNumber, A.itemDesc, A.flag1, B.brandName FROM we_Items A INNER JOIN we_Brands B ON A.brandID=B.brandID"
	SQL = SQL & " WHERE typeID = 16 AND A.hideLive = 0 AND A.brandID = '" & brandID & "' ORDER BY flag1"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	a = 0
	if RS.eof then
	    response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
	else
		%>
		<font face="Arial,Helvetica"><h3><%=RS.recordcount%> items found</h3></font>
		<font face="Arial,Helvetica"><h3><%=RS("brandName")%> Cell Phones</h3></font>
		<form name="frmEditItem" action="db_update_flag.asp" method="post">
			<table border="1" cellpadding="3" cellspacing="0" width="1000">
				<tr>
					<td align="left" width="10"><b>Item#</b></td>
					<td align="left" width="100"><b>Part&nbsp;Number</b></td>
					<td align="left" width="680"><b>Description</b></td>
					<td align="left" width="10"><b>flag1</b></td>
				</tr>
				<%
				do until RS.eof
					a = a + 1
					%>
					<tr>
						<td align="left"><%=RS("itemID")%></td>
						<td align="left"><%=RS("PartNumber")%></td>
						<td align="left">
							<%=RS("itemDesc")%>
							<input type="hidden" name="itemID<%=a%>" value="<%=RS("itemID")%>">
						</td>
						<td align="left"><input type="text" name="flag1<%=a%>" size="2" value="<%=RS("flag1")%>" maxlength="3"></td>
					</tr>
					<%
					RS.movenext
				loop
				%>
			</table>
			<p>
				<input type="submit" name="submitted" value="Update">
				<input type="hidden" name="totalCount" value="<%=a%>">
			</p>
		</form>
		<p>&nbsp;</p>
		<%
	end if
	RS.close
	set RS = nothing
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
