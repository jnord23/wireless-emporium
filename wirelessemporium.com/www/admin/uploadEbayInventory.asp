<%
response.buffer = false
pageTitle = "Upload Ebay Inventory"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
server.scripttimeout = 1000 'seconds
dim Path
Path = server.mappath("tempTXT")

set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
Count = Upload.Save(Path)
if Upload.form("upload") <> "" then
	if Count = 0 then
		response.write "No file selected."
		response.end
	else
		dim File, myFile, mySheet
		set File = Upload.Files(1)
		myFile = File.path
		
		dim txtConn, myStr
		set txtConn = Server.CreateObject("ADODB.Connection")
		myStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & myFile & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"";"
		txtConn.ConnectionString = myStr
		txtConn.ConnectionTimeout = 1000		'seconds
		txtConn.Open
		
		mySheet = replace(replace(replace(myFile,Path,""),"\",""),".xls","")
		
		SQL = "SELECT * FROM [" & left(mySheet,31) & "$] ORDER BY [Site Auction ID]"
		response.write "<h3>" & SQL & "</h3>" & vbCrLf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, txtConn, 3, 3
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			%>
			<h3><%=RS.recordcount%> records found</h3>
			<table border="1" cellpadding="1" cellspacing="0"><tr><td>Order_Number</td><td>SKU</td><td>Item Quantity</td></tr>
			<%
			dim Order_Number, updateCount
			dim nIdProd
			do until RS.eof
				Order_Number = SQLquote(RS("Site Auction ID"))
				Item_Quantity = SQLquote(RS("Invoice Quantity"))
				KIT = null
				SKUarray = split(SQLquote(RS("SKU")),"/")
				for a = 0 to Ubound(SKUarray)
					response.write "<tr><td>" & Order_Number & "</td><td>" & SKUarray(a) & "</td><td>" & Item_Quantity & "</td></tr>" & vbcrlf
					response.write "<tr><td colspan=""3"">" & vbcrlf
					SQL = "SELECT itemID, ItemKit_NEW FROM we_items WHERE PartNumber='" & left(SQLquote(SKUarray(a)),15) & "'"
					SQL = SQL & " AND (inv_qty > -1 OR ItemKit_NEW IS NOT NULL)"
					set rsTemp = server.createobject("ADODB.recordset")
					rsTemp.open SQL, oConn, 3, 3
					if not rsTemp.eof then
						if not isNull(rsTemp("ItemKit_NEW")) then KIT = rsTemp("ItemKit_NEW")
						nIdProd = rsTemp("itemID")
						call UpdateInvQty(left(SQLquote(SKUarray(a)),15), Item_Quantity, KIT)
						call NumberOfSales(nIdProd, Item_Quantity, KIT)
						updateCount = updateCount + 1
					else
						response.write "Part Number <b>" & left(SQLquote(SKUarray(a)),15) & "</b> not found with inv_qty > -1!" & vbcrlf
					end if
					response.write "</td></tr>" & vbcrlf
				next
				RS.movenext
			loop
		end if
		RS.close
		set RS = nothing
		%>
		</table>
		<p><%=updateCount%> orders entered into WE database.</p>
		<%
	end if
else
	%>
	<h3>Select the file you saved to your hard drive:</h3>
	<h2>
		Make certain that you have saved the file in Excel 97-2003 (.xls) format,<br>
		and that you have deleted all entries with Part Numbers that do not exist in the WE database!
	</h2>
	<form enctype="multipart/form-data" action="uploadEbayInventory.asp" method="POST">
		<p><input type="FILE" name="FILE1"></p>
		<p><input type="SUBMIT" name="upload" value="Upload"></p>
	</form>
	<%
end if

sub UpdateInvQty(nPartNumber, nProdQuantity, KIT)
	' Updates inv_qty for the Master Part (item with the same Part Number as the item purchased which does not have -1 inv_qty)
	' If the inv_qty would be adjusted to less than or equal to zero, then all items with the same Part Number as the item purchased are set to zero
	if isNull(KIT) then
		SQL = "SELECT itemID,typeID,vendor,inv_qty FROM we_items WHERE PartNumber = '" & nPartNumber & "' AND inv_qty > -1"
	else
		SQL = "SELECT itemID,typeID,vendor,inv_qty FROM we_items WHERE itemID IN (" & KIT & ")"
	end if
	'response.write "<li>" & SQL & "</li>" & vbcrlf
	set oRsItemInfo2 = Server.CreateObject("ADODB.Recordset")
	oRsItemInfo2.open SQL, oConn, 3, 3
	do until oRsItemInfo2.eof
		if oRsItemInfo2("inv_qty") - nProdQuantity > 0 then
			decreaseSQL = "UPDATE we_items SET inv_qty = inv_qty - " & nProdQuantity & " WHERE itemID = '" & oRsItemInfo2("itemID") & "'"
			'response.write "<li>" & decreaseSQL & "</li>" & vbcrlf
			oConn.execute(decreaseSQL)
		else
			decreaseSQL = "UPDATE we_items SET inv_qty = 0 WHERE PartNumber = '" & nPartNumber & "'"
			if oRsItemInfo2("typeID") = 2 then decreaseSQL = decreaseSQL & " AND inv_qty > -1"
			if oRsItemInfo2("typeID") <> 3 and oRsItemInfo2("vendor") <> "CM" and oRsItemInfo2("vendor") <> "DS" and oRsItemInfo2("vendor") <> "MLD" then
				' Send zero-inventory e-mail to Tony
				cdo_from = "Automatic E-Mail from Wirelessemporium.com<sales@wirelessemporium.com>"
				cdo_subject = nPartNumber & " is out of stock! (Ebay upload)"
				cdo_body = nPartNumber & " is out of stock! (Ebay upload)"
				cdo_to = "tony@wirelessemporium.com,charles@wirelessemporium.com,steven@wirelessemporium.com,jon@wirelessemporium.com"
				CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
			end if
			'response.write "<li>" & decreaseSQL & "</li>" & vbcrlf
			oConn.execute(decreaseSQL)
		end if
		oRsItemInfo2.movenext
	loop
	oRsItemInfo2.close
	set oRsItemInfo2 = nothing
end sub

sub NumberOfSales(nIdProd, QTY, KIT)
	if isNull(KIT) then
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID = '" & nIdProd & "'"
	else
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID IN (" & KIT & ")"
	end if
	response.write "<li>" & SQL & "</li>" & vbcrlf
	oConn.execute(SQL)
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
