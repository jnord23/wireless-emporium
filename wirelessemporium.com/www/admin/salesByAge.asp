<%
	thisUser = Request.Cookies("username")
	pageTitle = ""
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim website : website = prepInt(request.QueryString("website"))
	
	if website > 0 then
		useSiteID = website - 1
		sql = "select SUM(a.quantity) as totalSold, c.modelID, c.brandID, d.modelName, d.ReleaseYear, e.brandName, sum(cast(b.ordergrandtotal as money)) as grossSales from we_orderDetails a join we_orders b on a.orderid = b.orderid left join we_Items c on a.itemid = c.itemID left join we_Models d on c.modelID = d.modelID left join we_brands e on c.brandID = e.brandID where b.approved = 1 and b.cancelled is null and d.ReleaseYear is not null and b.orderdatetime > '1/1/2012' and b.store = " & useSiteID & " group by c.modelID, c.brandID, d.modelName, d.ReleaseYear, e.brandName order by d.ReleaseYear, c.brandID, totalSold"
	else
		sql = "select SUM(a.quantity) as totalSold, c.modelID, c.brandID, d.modelName, d.ReleaseYear, e.brandName, sum(cast(b.ordergrandtotal as money)) as grossSales from we_orderDetails a join we_orders b on a.orderid = b.orderid left join we_Items c on a.itemid = c.itemID left join we_Models d on c.modelID = d.modelID left join we_brands e on c.brandID = e.brandID where b.approved = 1 and b.cancelled is null and d.ReleaseYear is not null and b.orderdatetime > '1/1/2012' group by c.modelID, c.brandID, d.modelName, d.ReleaseYear, e.brandName order by d.ReleaseYear, c.brandID, totalSold"
	end if
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
%>
<table border="0" cellpadding="3" cellspacing="0" align="center" style="padding:20px 0px 20px 0px;">
    <tr><td colspan="4" align="center" style="font-weight:bold; font-size:24px;">Sales Since 2012 - By Model Year (<%=website%>)</td></tr>
    <tr>
    	<td colspan="4" align="center" style="font-weight:bold; font-size:14px;">
        	Website: 
        	<select name="website" onchange="window.location='/admin/salesByAge.asp?website=' + this.value">
            	<option value="">All</option>
                <option value="1"<% if website = 1 then %> selected="selected"<% end if %>>WE</option>
                <option value="3"<% if website = 3 then %> selected="selected"<% end if %>>CO</option>
                <option value="2"<% if website = 2 then %> selected="selected"<% end if %>>CA</option>
                <option value="4"<% if website = 4 then %> selected="selected"<% end if %>>PS</option>
            </select>
        </td>
    </tr>
    <tr style="background-color:#000; font-weight:bold; color:#FFF; font-size:14px;">
    	<td align="left" style="width:100px; padding-right:5px;">Model Year</td>
        <td align="left" style="width:100px; padding-right:5px;">Orders</td>
        <td align="left" style="width:100px; padding-right:5px;">Gross Sales</td>
        <td align="left" style="width:300px; padding-left:5px;">Hot Shot</td>
    </tr>
	<%
	bgColor = "#fff"
	do while not rs.EOF
		curYear = prepInt(rs("ReleaseYear"))
		totalSales = 0
		grossSales = 0
		topSeller = ""
		hiSales = 0
		hiOrders = 0
		do while curYear = prepInt(rs("ReleaseYear"))
			curSales = prepInt(rs("totalSold"))
			curGross = prepInt(rs("grossSales"))
			
			totalSales = totalSales + curSales
			grossSales = grossSales + curGross
			
			if curGross > hiSales then
				hiSales = curGross
				hiOrders = curSales
				topSeller = rs("brandName") & " - " & rs("modelName")
			end if
			
			rs.movenext
			if rs.EOF then exit do
		loop
	%>
    <tr style="background-color:<%=bgColor%>;">
    	<td align="right" style="width:100px; border-right:1px solid #000; padding-right:5px;"><%=curYear%></td>
        <td align="right" style="width:100px; border-right:1px solid #000; padding-right:5px;"><%=formatNumber(totalSales,0)%></td>
        <td align="right" style="width:100px; border-right:1px solid #000; padding-right:5px;"><%=formatCurrency(grossSales,2)%></td>
        <td align="left" style="width:300px; padding-left:5px;">
			<%=topSeller%><br />
			<%=formatNumber(hiOrders,0)%> Orders - Grossing <%=formatCurrency(hiSales,2)%>
        </td>
    </tr>
    <%
		if rs.EOF then exit do
		if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
	loop
	%>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
