<%
response.buffer = false
dim thisUser, adminID
'iteration variables [knguyen/20110603]
dim strItemId
dim price_Our
dim price_CA
dim price_CO
dim price_PS
dim price_ER
dim PartNumber

pageTitle = "Admin - Update WE Database - Update Prices"
header = 1
thisUser = Request.Cookies("username")
adminID = Request.Cookies("adminID")
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_DbInventory.asp"-->
<font face="Arial,Helvetica">
	<p><a href="/admin/db_search_brands.asp?searchID=8">Back to Brands</a></p>
</font>

<%


dim dicSitePriceKey: set dicSitePriceKey = CreateObject("Scripting.Dictionary") 'site variable iteration [knguyen/20110603]
dicSitePriceKey( WE_ID) = "price_Our"
dicSitePriceKey( CA_ID) = "price_CA"
dicSitePriceKey( CO_ID) = "price_CO"
dicSitePriceKey( PS_ID) = "price_PS"
dicSitePriceKey( ER_ID) = "price_ER"

function GetItemValueFromRequest( byref dicSitePriceKey) 'parses out form request price data into nested hashtable data structure w/ the blowout price-logic [knguyen/20110603]
	dim dicReturnValue : set dicReturnValue = CreateObject("Scripting.Dictionary")
	dicReturnValue.CompareMode = 1 ' -- case-insensitive mode
	for intIdx = 1 to Fv( "totalCount")
		for each strSiteId in dicSitePriceKey.Keys
			dim strGroupId : strGroupId = strSiteId&"x"&CStr(intIdx)
			dim dicAttribute : set dicAttribute = CreateObject("Scripting.Dictionary")
			dim strDiscountOption: strDiscountOption = Fv( "DiscountOption"&strGroupId)

			dicAttribute( "OriginalPrice") = Fv( dicSitePriceKey( strSiteId)&"x"&Cstr(intIdx))
			if strDiscountOption <> "NoSale" then dicAttribute( "OriginalPrice") = FormatDecimalInput( Fv( "OriginalPrice"&strGroupId)) 'i.e, override w/ hidden field value since the previous OriginalPrice was not modified

			dicAttribute( "DiscountPrice") = FormatDecimalInput( Fv( "DiscountPrice"&strGroupId)) 'i.e, hidden field
			dicAttribute( "DiscountPercent") = FormatDecimalInput( Fv( "DiscountPercent"&strGroupId)) 'i.e, hidden field


			select case( strDiscountOption)
				case "NoSale": 
					dicAttribute( "ActiveItemValueTypeId") = 1
					dicAttribute( "EffectivePrice") = FormatDecimalInput( dicAttribute( "OriginalPrice"))
				case "$": 
					dicAttribute( "ActiveItemValueTypeId") = 2
					dicAttribute( "DiscountPrice") = FormatDecimalInput( Fv( "DiscountValue"&strGroupId)) 'i.e, hidden field
					dicAttribute( "EffectivePrice") = FormatDecimalInput( dicAttribute( "DiscountPrice"))
				case "%off":
					dicAttribute( "ActiveItemValueTypeId") = 3
					dicAttribute( "DiscountPercent") = FormatDecimalInput( Fv( "DiscountValue"&strGroupId)) 'i.e, hidden field
					if IsNumeric( dicAttribute( "DiscountPercent")) and IsNumeric( dicAttribute( "OriginalPrice")) then
						dicAttribute( "EffectivePrice") = FormatDecimalInput( CStr((100.0-CDbl( dicAttribute( "DiscountPercent")))*CDbl( dicAttribute( "OriginalPrice"))/100.0))
					end if
			end select

			set dicReturnValue( strGroupId) = dicAttribute
		next
	next

	set GetItemValueFromRequest=dicReturnValue
end function

function DbPrice( byval strPrice) 'maybe should place this function somewhere global .. [knguyen/20110603]
	DbPrice = "null"
	if isNumeric( strPrice) and strPrice<>"0" and HasLen( strPrice) then DbPrice = DbStr( strPrice)
end function
if request("submitted") = "Update" then
	dim dicItemValueFromRequest: set dicItemValueFromRequest=GetItemValueFromRequest( dicSitePriceKey)

	dim dicItemValueModifySql: set dicItemValueModifySql = CreateObject("Scripting.Dictionary") '-- this is to remove duplicate sqls that may come from multiple slaves on the single update page (optimization)

	for a = 1 to request("totalCount")
		price_Retail = DbPrice( Fv("price_Retail" & a))
		for each strSiteId in dicSitePriceKey.Keys
			execute( dicSitePriceKey( strSiteId) &"="& EvalStr( DbPrice( Obj2Str( dicItemValueFromRequest( strSiteId&"x"&CStr(a))( "EffectivePrice")))))			
		next
'		COGS = DbPrice( request("COGS" & a))
		COGS = prepInt(request("COGS" & a))
		PartNumber = Fv( "PartNumber" & a)
		
		sql = "select price_our, price_CA, price_CO, price_PS, price_ER, Cogs from we_Items where partNumber = '" & PartNumber & "'"
		session("errorSQL") = sql
		set currentRS = oConn.execute(sql)
		
		price_Our_b = prepInt(replace(price_Our,"'",""))
		price_CO_b = prepInt(replace(price_CO,"'",""))
		price_CA_b = prepInt(replace(price_CA,"'",""))
		price_PS_b = prepInt(replace(price_PS,"'",""))
		price_ER_b = prepInt(replace(price_ER,"'",""))
		'response.End()
		if prepInt(price_Our_b) <> prepInt(currentRS("price_our")) then
			sql = "insert into we_priceAdjust (siteID,partnumber,adminID,curPrice,newPrice) values(0,'" & PartNumber & "'," & adminID & ",'" & prepInt(currentRS("price_our")) & "','" & price_Our_b & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		if prepInt(price_CO_b) <> prepInt(currentRS("price_co")) then
			sql = "insert into we_priceAdjust (siteID,partnumber,adminID,curPrice,newPrice) values(2,'" & PartNumber & "'," & adminID & ",'" & prepInt(currentRS("price_co")) & "','" & price_CO_b & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		if prepInt(price_CA_b) <> prepInt(currentRS("price_ca")) then
			sql = "insert into we_priceAdjust (siteID,partnumber,adminID,curPrice,newPrice) values(1,'" & PartNumber & "'," & adminID & ",'" & prepInt(currentRS("price_ca")) & "','" & price_CA_b & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		if prepInt(price_PS_b) <> prepInt(currentRS("price_ps")) then
			sql = "insert into we_priceAdjust (siteID,partnumber,adminID,curPrice,newPrice) values(3,'" & PartNumber & "'," & adminID & ",'" & prepInt(currentRS("price_ps")) & "','" & price_PS_b & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		if prepInt(price_ER_b) <> prepInt(currentRS("price_er")) then
			sql = "insert into we_priceAdjust (siteID,partnumber,adminID,curPrice,newPrice) values(10,'" & PartNumber & "'," & adminID & ",'" & prepInt(currentRS("price_er")) & "','" & price_ER_b & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		if COGS <> prepInt(currentRS("Cogs")) then
			sql = "insert into we_priceAdjust (siteID,partnumber,adminID,curCogs,newCogs) values(0,'" & PartNumber & "'," & adminID & ",'" & prepInt(currentRS("Cogs")) & "','" & COGS & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if

		SQL = "UPDATE WE_Items SET"
		SQL = SQL & " price_Retail = " & price_Retail & ", "
		SQL = SQL & " price_Our = " & price_Our & ", "
		SQL = SQL & " price_CA = " & price_CA & ", "
		SQL = SQL & " price_CO = " & price_CO & ", "
		SQL = SQL & " price_PS = " & price_PS & ", "
		SQL = SQL & " price_ER = " & price_ER & ", "
		SQL = SQL & " PriceLastUpdatedBy = '" & adminID & "', "
		SQL = SQL & " PriceLastUpdatedDate = '" & now & "'"
		SQL = SQL & " WHERE PartNumber="& DbStr( PartNumber)
		response.write "<p>" & SQL & "</p>"
		oConn.execute SQL
		
		sql = "insert into we_adminActions (adminID,action) values (" & adminID & ",'/ajax/saveCogs.asp updated the cogs for: " & PartNumber & "')"
		session("errorSQL")
		oConn.execute(sql)
		
		for each strItemId in GetItemByPartNumber( PartNumber).AsArray '-- outer loop
			for each strSiteId in dicSitePriceKey.Keys
				dim strGroupId: strGroupId = strSiteId&"x"&CStr(a)
				dicItemValueModifySql( _
					"exec sp_ModifyItemValue " &VbCrLf&_
					" "& DbId( strSiteId) &" --SiteId" &VbCrLf&_
					" ,"& DbId( strItemId) &" --ItemId" &VbCrLf&_
					" ,"& DbId( dicItemValueFromRequest( strGroupId)( "ActiveItemValueTypeId")) &" --ActiveItemValueTypeId" &VbCrLf&_
					" ,"& DbNumNil( dicItemValueFromRequest( strGroupId)( "OriginalPrice")) &" --OriginalPrice" &VbCrLf&_
					" ,"& DbNumNil( dicItemValueFromRequest( strGroupId)( "DiscountPrice")) &" --DiscountPrice" &VbCrLf&_
					" ,"& DbNumNil( dicItemValueFromRequest( strGroupId)( "DiscountPercent")) &" --DiscountPercent"&VbCrLf)=1 'using hash as a bag collection to dedupe statements
				
				saleType = DbId( dicItemValueFromRequest( strGroupId)( "ActiveItemValueTypeId"))
				if saleType > 1 then
					if saleType = 2 then
						sql = "update we_priceAdjust set salePrice = '" & DbNumNil( dicItemValueFromRequest( strGroupId)( "DiscountPrice")) & "' where id = (select top 1 id from we_priceAdjust where partnumber = '" & PartNumber & "' and siteID = " & DbId( strSiteId) & " order by id desc)"
						session("errorSQL") = sql
						oConn.execute(sql)
					else
						sql = "update we_priceAdjust set saleDiscount = '" & DbNumNil( dicItemValueFromRequest( strGroupId)( "DiscountPercent")) & "' where id = (select top 1 id from we_priceAdjust where partnumber = '" & PartNumber & "' and siteID = " & DbId( strSiteId) & " order by id desc)"
						session("errorSQL") = sql
						oConn.execute(sql)
					end if
				end if
				'sql = "insert into we_priceAdjust (siteID,partnumber,adminID,salePrice,saleDiscount) values (" & prepInt(strSiteId) & ",'" & PartNumber & "'," & adminID & ",'" & prepInt(DbNumNil( dicItemValueFromRequest( strGroupId)( "DiscountPrice"))) & "'," & prepInt(DbNumNil( dicItemValueFromRequest( strGroupId)( "DiscountPercent"))) & ")"
				'session("errorSQL") = sql
				'oConn.execute(sql)
			next '-- inner loop
		next '-- outer loop
		
		sql = "select itemID, brandID, modelID, typeID from we_Items where partNumber = '" & PartNumber & "'"
		response.Write(sql & "<br>")
		session("errorSQL") = sql
		set updateRS = oConn.execute(sql)
		
		do while not updateRS.EOF
			itemID = updateRS("itemID")
			brandID = updateRS("brandID")
			modelID = updateRS("modelID")
			typeID = updateRS("typeID")
			
			autoDeleteCompPages "price",brandID & "##" & modelID & "##" & typeID & "##" & itemID
			
			updateRS.movenext
		loop
	next
	set dicSitePriceKey = nothing 'clean up
	set dicItemValueFromRequest = nothing 'clean up

	Sql = EMPTY_STRING 'buffer all ItemValue updates before execution
	for each strKey in dicItemValueModifySql.Keys
		Sql = Sql & strKey
	next
	set dicItemValueModifySql = nothing 'clean up

	oConn.execute Sql

	%>
	<h3>UPDATED!</h3>
	<%
else
	SQL = "SELECT A.*, B.brandCode FROM WE_Items A LEFT OUTER JOIN WE_Brands B ON A.BrandID = B.BrandID"
	keywords = SQLquote(request("keywords"))
	if len(request.QueryString("itemID")) > 0 then
		ItemNumber = prepInt(request.QueryString("itemID"))
	else
		ItemNumber = request("ItemNumber")
	end if
	PartNumber = request("PartNumber")
	SQL = SQL & " WHERE A.inv_qty > -1"
	if request("BrandID") = "ALL" and request("TypeID") = "8" then
		SQL = SQL & " AND A.TypeID='8'"
	else
		if keywords = "" then
			if ItemNumber = "" then
				if PartNumber = "" then
					if request("BrandID") <> "" then SQL = SQL & " AND A.BrandID='" & request("BrandID") & "'"
					if request("TypeID") <> "" then SQL = SQL & " AND A.TypeID='" & request("TypeID") & "'"
					if request("ModelID") <> "" then SQL = SQL & " AND A.ModelID='" & request("ModelID") & "'"
				else
					if inStr(PartNumber,"%") > 0 then
						SQL = SQL & " AND A.PartNumber LIKE '" & PartNumber & "'"
					else
						SQL = SQL & " AND A.PartNumber = '" & PartNumber & "'"
					end if
				end if
			else
				SQL = SQL & " AND A.itemID='" & ItemNumber & "'"
			end if
		else
			SQL = SQL & " AND A.itemDesc LIKE '%" & keywords & "%'"
		end if
	end if
	SQL = SQL & " ORDER BY A.itemDesc"
	
	if instr(SQL,"WHERE A.inv_qty > -1 ORDER BY A.itemDesc") > 0 then
		SQL = replace(SQL,"ORDER BY","and A.itemID = 2 ORDER BY")
	end if
	
	set RS = oConn.execute(sql)
	a = 0
	if RS.eof then
	    response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
	else
		BrandID = RS("brandID")
		%>
		<form name="frmEditItem" action="db_update_price.asp" method="post">
<script type="text/javascript" src="/includes/js/jquery-1.2.6.pack.js" ></script>
<script type="text/javascript" src="/includes/admin/js/DbUpdateItems.js" ></script>
			<table border="1" cellpadding="3" cellspacing="0" width="1000">
				<tr>
					<td align="center" width="10"><b>Item#</b></td>
					<td align="center" width="485"><b>Description</b></td>
					<td align="center" width="120"><b>Part&nbsp;Number</b></td>
					<td align="center" width="55"><b>Retail&nbsp;$</b></td>
					<td align="center" width="55"><b>Our&nbsp;$</b></td>
					<td align="center" width="55"><b>CA&nbsp;$</b></td>
					<td align="center" width="55"><b>CO&nbsp;$</b></td>
					<td align="center" width="55"><b>PS&nbsp;$</b></td>
					<td align="center" width="55"><b>TM&nbsp;$</b></td>
					<td align="center" width="55"><b>COGS</b></td>
					<td align="center" width="55"><b>Vendor</b></td>
				</tr>
				<%

'render the tag attributes to avoid typos [knguyen/20110602]
function GetId( byval strFieldBaseName, byval strGroupId)
	GetId="id="""& strFieldBaseName &""& strGroupId &""" name="""& strFieldBaseName &"" &strGroupId &""""
end function

'renders dynamic price panel instance (will called per site after this definition) [knguyen/20110602]
sub RenderPriceInput( byval strSiteId, byval strItemPriceName, byval strItemId, byval strIndex) 
	dim dicItemValue: set dicItemValue = GetItemValue( strItemId) 'retrieve item value
	dim strGroupId: strGroupId = strSiteId& "x" &strIndex
	
%>
						<%=TABLE_START %>
						<tr>
						<td class="normaltext"><input id="Price<%=strGroupId%>" type="text" name="<%=strItemPriceName%>x<%=strIndex%>" size="3" maxlength="6" value="<%=FormatDecimalInput(eval( strItemPriceName))%>" <% if dicItemValue(strSiteId)("ActiveItemValueType")<>"OriginalPrice" then response.write "disabled=""true""" %>>
						</td>
						</tr>
						<tr>
							<td>
								<%=TABLE_START %>
								<tr>
									<td><select <%=GetId( "DiscountOption", strGroupId) %> onchange="OnChangeDiscountOption( this, '<%=strGroupId%>')"><%
										for each strSelectOption in split( "NoSale|%off|$","|")
											dim blnSelectOptionChecked: blnSelectOptionChecked=false
											select case( strSelectOption)
												case "%off": if dicItemValue(strSiteId)("ActiveItemValueType")="DiscountPercent" then blnSelectOptionChecked=true
												case "$": if dicItemValue(strSiteId)("ActiveItemValueType")="DiscountPrice" then blnSelectOptionChecked=true
											end select
											%><option value="<%=strSelectOption%>"<% if blnSelectOptionChecked then response.write " selected" %>><%=strSelectOption%></option><%
										next %>
									</select></td>
									<td><div id="divDiscountValue<%=strGroupId%>" style="display:<% if dicItemValue(strSiteId)("ActiveItemValueType")="OriginalPrice" then response.write "none" else response.write "block" %>"><input type="text" field="DiscountValue" <%=GetId( "DiscountValue", strGroupId) %> size="6" maxlength="6" value="<%

		response.write FormatDecimalInput(dicItemValue(strSiteId)( dicItemValue(strSiteId)("ActiveItemValueType"))) 

%>" onfocus="strSiteId='<%=strGroupId%>'"></div>
									</div></td>
								</tr>
								<%=TABLE_END %>
							</td>
						</tr>
						<%=TABLE_END %>
						<input type="hidden" <%=GetId( "DiscountPrice", strGroupId) %> value="<%=FormatDecimalInput(dicItemValue(strSiteId)("DiscountPrice")) %>">
						<input type="hidden" <%=GetId( "DiscountPercent", strGroupId) %> value="<%=FormatDecimalInput(dicItemValue(strSiteId)("DiscountPercent")) %>"><%
dim strEffectiveOriginalPrice: strEffectiveOriginalPrice=dicItemValue(strSiteId)("OriginalPrice")
if not HasLen( strEffectiveOriginalPrice) then strEffectiveOriginalPrice=eval( strItemPriceName) %>
						<input type="hidden" <%=GetId( "OriginalPrice", strGroupId) %> value="<%=FormatDecimalInput( strEffectiveOriginalPrice) %>"><%
	set dicItemValue = nothing
end sub ' RenderPriceInput

				do until RS.eof					
					strItemId = RS( "itemID")
					price_Our = RS( "price_Our")
					price_CA = RS( "price_CA")
					price_CO = RS( "price_CO")
					price_PS = RS( "price_PS")
					price_ER = RS( "price_ER")

					a = a + 1
					%>
					<tr>
						<td align="center">
							<%=strItemId%>
							<input type="hidden" name="itemID<%=a%>" value="<%=strItemId%>">
							<input type="hidden" name="PartNumber<%=a%>" value="<%=RS("PartNumber")%>">
						</td>
						<td align="center"><%=RS("itemDesc")%></td>
						<td align="center"><nobr>&nbsp;<%=RS("PartNumber")%></nobr></td>
						<td align="center"><input type="text" name="price_Retail<%=a%>" size="3" value="<%=RS("price_Retail")%>"></td>
						<td align="center"><%call RenderPriceInput( WE_ID, "price_Our", strItemId, a)%></td>
						<td align="center"><%call RenderPriceInput( CA_ID, "price_CA", strItemId, a)%></td>
						<td align="center"><%call RenderPriceInput( CO_ID, "price_CO", strItemId, a)%></td>
						<td align="center"><%call RenderPriceInput( PS_ID, "price_PS", strItemId, a)%></td>
                        <td align="center"><%call RenderPriceInput( ER_ID, "price_ER", strItemId, a)%></td>
						<td align="center"><%=RS("COGS")%></td>
						<td align="center">&nbsp;<%=RS("Vendor")%></td>
					</tr>
					<%
					RS.movenext
				loop
				%>
			</table>
			<p>
			<input type="submit" name="submitted" value="Update">
			<input type="hidden" name="totalCount" value="<%=a%>">
			</p>
		</form>
		<p>&nbsp;</p>
		<%
	end if
	RS.close
	set RS = nothing
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
