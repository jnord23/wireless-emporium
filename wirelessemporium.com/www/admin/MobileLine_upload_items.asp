<%
'option explicit
response.buffer = false
pageTitle = "Upload Mobile Line Items"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
server.scripttimeout = 1000 'seconds
dim Path, Upload, Count
Path = server.mappath("tempTXT")
set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
Count = Upload.Save(Path)
if Upload.form("upload") <> "" then
	if Count = 0 then
		response.write "No file selected."
		response.end
	else
		dim File, myFile
		set File = Upload.Files(1)
		myFile = File.path
		
		dim txtConn, myStr
		set txtConn = Server.CreateObject("ADODB.Connection")
		myStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & myFile & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"";"
		txtConn.ConnectionString = myStr
		txtConn.ConnectionTimeout = 1000		'seconds
		txtConn.Open
		'response.write(myFile)
		
		dim SQL, RS, RStemp
		SQL = "SELECT * FROM [Sheet1$] WHERE [Mobile Line Part Number] <> '' ORDER BY [Mobile Line Part Number]"
		response.write "<h3>" & SQL & "</h3>" & vbCrLf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, txtConn, 3, 3
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			dim strMobileLine_skus, strNewPartNumbers, aCount
			strMobileLine_skus = ""
			strNewPartNumbers = ""
			aCount = 1
			longest = 2
			
			do until RS.eof
				if len(RS("Mobile Line Part Number")) > longest then longest = len(RS("Mobile Line Part Number"))
				
				dim modelID, modelName, brandID, brandName
				modelID = RS("WE MODEL ID")
				SQL = "SELECT brandID FROM we_Models WHERE modelID = '" & modelID & "'"
				set RStemp = Server.CreateObject("ADODB.Recordset")
				RStemp.open SQL, oConn, 3, 3
				if RStemp.eof then
					response.write "<h3>Model ID " & modelID & " not found for Mobile Line Part Number " & RS("Mobile Line Part Number") & "</h3>"
					response.end
				else
					brandID = RStemp("brandID")
				end if
				
				dim price_Our, price_CA, price_CO, price_PS, price_ER, price_Buy, COGS, price_Retail
				price_Our = RS("MSRP")
				COGS = RS("Mobile Line Price")
				price_Retail = RS("MSRP")
				
				dim inv_qty, flag1, UPCCode, AMZN_sku, MobileLine_sku
				inv_qty = RS("Quantity")
				if inv_qty < 0 then inv_qty = 0
				flag1 = 0
				UPCCode = SQLquote(RS("UPC"))
				MobileLine_sku = SQLquote(RS("Mobile Line Part Number"))
				strMobileLine_skus = strMobileLine_skus & "'" & MobileLine_sku & "',"
				
				dim itemDesc, itemDesc_PS, itemDesc_CA, itemDesc_CO
				itemDesc = replace(replace(RS("Name"),vbtab,""),chr(9),"")
				
				dim itemLongDetail, itemLongDetail_PS, itemLongDetail_CA, itemLongDetail_CO
				itemLongDetail = replace(RS("Description"),vbcrlf," ")
				
				dim PartPrefix, PartFlag, PartNumber
				PartPrefix = RS("WE ID")
				PartFlag = 1
				do until PartFlag = 0
					PartNumber = PartPrefix & "-MLD-" & zeroPad(aCount,4) & "-01"
					SQL = "SELECT PartNumber FROM we_Items WHERE PartNumber = '" & PartNumber & "'"
					set RStemp = Server.CreateObject("ADODB.Recordset")
					RStemp.open SQL, oConn, 3, 3
					if RStemp.eof then
						PartFlag = 0
					else
						aCount = aCount + 1
					end if
				loop
				
				dim productImage, productImage_CO
				productImage = RS("Image")
				if isNull(productImage) or productImage = "" then
					productImage = "imagena.jpg"
				else
					productImage = "MLD_" & productImage
					productImage = replace(replace(replace(productImage,".png",".jpg"),".gif",".jpg"),".JPG",".jpg")
				end if
				productImage_CO = replace(productImage,"_skin_","_decal_skin_")
				
				dim itemWeight
				if not isNull(RS("Weight (lbs)")) then
					itemWeight = int(round(RS("Weight (lbs)") * 16))
				else
					itemWeight = 0
				end if
				
				dim typeID, HandsfreeType
				if not isNull(RS("WE HANDSFREE")) and RS("WE HANDSFREE") <> "" then
					HandsfreeType = "'" & RS("WE HANDSFREE") & "'"
				else
					HandsfreeType = "null"
				end if
				select case trim(RS("Category3"))
					case "Batteries" : typeID = 1
					case "Battery Doors" : typeID = 1
					case "Bluetooth" : typeID = 5
					case "Carrying Solutions"
						select case RS("Category4")
							case "Cases" : typeID = 7
							case "Holsters" : typeID = 6
							case "Phone Mounts" : typeID = 6
							case "Pouches" : typeID = 7
							case "Screen Protectors" : typeID = 17
							case "Skins" : typeID = 7
							case else : typeID = 9999
						end select
					case "Chargers" : typeID = 2
					case "Data" : typeID = 13
					case "Headsets" : typeID = 5
					case "Memory Cards" : typeID = 13
					case else : typeID = 9999
				end select
				
				SQL = "SELECT itemID FROM we_Items WHERE MobileLine_sku = '" & MobileLine_sku & "'"
				set RStemp = Server.CreateObject("ADODB.Recordset")
				RStemp.open SQL, oConn, 3, 3
				if RStemp.eof then
					strNewPartNumbers = strNewPartNumbers & "<li>" & PartNumber & "</li>" & vbcrlf
					SQL = "SET NOCOUNT ON; "
					SQL = SQL & "INSERT INTO we_Items (brandID,modelID,typeID,carrierID,subtypeID,PartNumber,itemDesc,itemDesc_PS,itemDesc_CA,itemDesc_CO,vendor,price_Retail,price_Our,COGS,"
					SQL = SQL & "numberOfSales,inv_qty,Seasonal,Sports,HandsfreeType,hideLive,itemLongDetail,itemLongDetail_PS,itemLongDetail_CA,itemLongDetail_CO,itemWeight,itemDimensions,itemPic,itemPic_CO,itemInfo,"
					SQL = SQL & "BULLET1,BULLET2,BULLET3,BULLET4,BULLET5,BULLET6,BULLET7,BULLET8,BULLET9,BULLET10,COMPATIBILITY,download_URL,download_TEXT,"
					SQL = SQL & "POINT1,POINT2,POINT3,POINT4,POINT5,POINT6,POINT7,POINT8,POINT9,POINT10,"
					SQL = SQL & "FEATURE1,FEATURE2,FEATURE3,FEATURE4,FEATURE5,FEATURE6,FEATURE7,FEATURE8,FEATURE9,FEATURE10,"
					SQL = SQL & "FormFactor,BandType,Band,PackageContents,TalkTime,Standby,Display,Condition,flag1,UPCCode,AMZN_sku,MobileLine_sku,"
					SQL = SQL & "DateTimeEntd) VALUES ("
					SQL = SQL & "'" & brandID & "', "
					SQL = SQL & "'" & modelID & "', "
					SQL = SQL & "'" & typeID & "', "
					SQL = SQL & "0, "
					SQL = SQL & "0, "
					SQL = SQL & "'" & Ucase(PartNumber) & "', "
					SQL = SQL & "'" & SQLquote(itemDesc) & "', "
					SQL = SQL & "'" & SQLquote(itemDesc_PS) & "', "
					SQL = SQL & "'" & SQLquote(itemDesc_CA) & "', "
					SQL = SQL & "'" & SQLquote(itemDesc_CO) & "', "
					SQL = SQL & "'MLD', "
					SQL = SQL & price_Retail & ", "
					SQL = SQL & price_Our & ", "
					SQL = SQL & COGS & ", "
					SQL = SQL & "0, "
					SQL = SQL & inv_qty & ", "
					SQL = SQL & "0, "
					SQL = SQL & "0, "
					SQL = SQL & HandsfreeType & ", "
					SQL = SQL & "0, "
					SQL = SQL & "'" & SQLquote(itemLongDetail) & "', "
					SQL = SQL & "'" & SQLquote(itemLongDetail_PS) & "', "
					SQL = SQL & "'" & SQLquote(itemLongDetail_CA) & "', "
					SQL = SQL & "'" & SQLquote(itemLongDetail_CO) & "', "
					SQL = SQL & "'" & itemWeight & "', "
					SQL = SQL & "'', "
					SQL = SQL & "'" & productImage & "', "
					SQL = SQL & "'" & productImage_CO & "', "
					SQL = SQL & "'', "
					SQL = SQL & "'" & SQLquote(BULLET1) & "', "
					SQL = SQL & "'" & SQLquote(BULLET2) & "', "
					SQL = SQL & "'" & SQLquote(BULLET3) & "', "
					SQL = SQL & "'" & SQLquote(BULLET4) & "', "
					SQL = SQL & "'" & SQLquote(BULLET5) & "', "
					SQL = SQL & "'" & SQLquote(BULLET6) & "', "
					SQL = SQL & "'" & SQLquote(BULLET7) & "', "
					SQL = SQL & "'" & SQLquote(BULLET8) & "', "
					SQL = SQL & "'" & SQLquote(BULLET9) & "', "
					SQL = SQL & "'" & SQLquote(BULLET10) & "', "
					SQL = SQL & "'" & SQLquote(COMPATIBILITY) & "', "
					SQL = SQL & "'', "
					SQL = SQL & "'', "
					SQL = SQL & "'" & SQLquote(POINT1) & "', "
					SQL = SQL & "'" & SQLquote(POINT2) & "', "
					SQL = SQL & "'" & SQLquote(POINT3) & "', "
					SQL = SQL & "'" & SQLquote(POINT4) & "', "
					SQL = SQL & "'" & SQLquote(POINT5) & "', "
					SQL = SQL & "'" & SQLquote(POINT6) & "', "
					SQL = SQL & "'" & SQLquote(POINT7) & "', "
					SQL = SQL & "'" & SQLquote(POINT8) & "', "
					SQL = SQL & "'" & SQLquote(POINT9) & "', "
					SQL = SQL & "'" & SQLquote(POINT10) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE1) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE2) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE3) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE4) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE5) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE6) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE7) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE8) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE9) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE10) & "', "
					SQL = SQL & "'', 0, '', '', '', '', '', 0, "
					SQL = SQL & "'" & flag1 & "', "
					SQL = SQL & "'" & UPCCode & "', "
					SQL = SQL & "'" & AMZN_sku & "', "
					SQL = SQL & "'" & MobileLine_sku & "', "
					SQL = SQL & "'" & now & "'"
					SQL = SQL & "); SELECT @@IDENTITY [newItemID]"
					session("errorSQL") = SQL
					response.write "<p>" & SQL & "</p>"
					dim RSid, newItemID
					set RSid = oConn.execute(SQL)
					newItemID = RSid("newItemID")
					
					if RS("WE COMPATIBLE") <> "" then
						modelArray = split(RS("WE COMPATIBLE"),",")
						for a = 0 to uBound(modelArray)
							SQL = "SELECT brandID FROM we_Models WHERE modelID = '" & modelArray(a) & "'"
							set RSmodel = Server.CreateObject("ADODB.Recordset")
							RSmodel.open SQL, oConn, 3, 3
							if not RSmodel.eof then
								SQL = "INSERT INTO we_RelatedItems (itemID,TypeID,ModelID,BrandID) VALUES ("
								SQL = SQL & "'" & newItemID & "',"
								SQL = SQL & "'" & typeID & "',"
								SQL = SQL & "'" & modelArray(a) & "',"
								SQL = SQL & "'" & RSmodel("brandID") & "')"
								response.write "<p>" & SQL & "</p>"
								oConn.execute SQL
							end if
						next
					end if
				end if
				RS.movenext
			loop
		end if
		%>
		<h3>
			New Part Numbers:
			<ul>
				<%=strNewPartNumbers%>
			</ul>
		</h3>
		<h3>DONE!</h3>
		<h3>longest = <%=longest%></h3>
		<%
	end if
else
	%>
	<h3>
		[Upload MobileLine spreadhseet to WE]<br>
		Select the file you saved to your hard drive:<br>
		(IMPORTANT: Make sure that the Worksheet is named "Sheet1"!)
	</h3>
	<form enctype="multipart/form-data" action="MobileLine_upload_items.asp" method="POST">
		<p><input type="FILE" name="FILE1"></p>
		<p><input type="SUBMIT" name="upload" value="Upload"></p>
	</form>
	<%
end if

function SQLquote(strValue)
	if not isNull(strValue) and strValue <> "" then
		SQLquote = trim(replace(strValue,"'","''"))
		SQLquote = replace(SQLquote,"&#039;","''")
		SQLquote = replace(SQLquote,chr(34),"''")
	else
		SQLquote = strValue
	end if
end function

function zeroPad(m,t)
	zeroPad = String(t-Len(m),"0") & m
end function
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
