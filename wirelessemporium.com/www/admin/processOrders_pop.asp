
<%
pageTitle = "Admin - Process Wireless Emporium Orders Popup"
header = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
dim processPDF : processPDF = request.QueryString("processPDF")
processPDF = replace(replace(processPDF, "##", "("), "@@", ")")
%>
<style>
.mouseover	{ background-color:#9BCEFF;}
</style>
<%
'search for the orders that have not been scanned.
sql	=	"select	a.orderid, a.orderdatetime, a.thub_posted_date, a.shiptype, isnull(b.orderdesc, 'Credit Card') ordertype, a.extordernumber" & vbcrlf & _
		"	,	v.email, v.sAddress1 + ' ' + v.sAddress2 sAddress, v.sCity, v.sState, v.sZip" & vbcrlf & _
		"	,	a.ordersubtotal, a.ordershippingfee, v.accountid" & vbcrlf & _
		"from	we_orders a left outer join xordertype b" & vbcrlf & _
		"	on	a.extordertype = b.typeid left outer join v_accounts v" & vbcrlf & _
		"	on	a.store = v.site_id and a.accountid = v.accountid" & vbcrlf & _
		"where	a.processPDF = '" & processPDF & "'" & vbcrlf & _
		"	and	a.approved = 1" & vbcrlf & _
		"	and	(a.cancelled is null or a.cancelled = 0)" & vbcrlf & _
		"	and	a.scandate is null" & vbcrlf & _
		"	and	a.RMAStatus is null" & vbcrlf & _
		"	and	a.thub_posted_to_accounting <> 'D'"

set objRsOrders = oConn.execute(sql)

nRows = 0
if not objRsOrders.eof then
%>
    <table width="1000" border="1" align="center" cellpadding="5" cellspacing="0" style="border-collapse:collapse;font-family: Arial, Helvetica, sans-serif; font-size:12px;">
        <tr>
            <td bgcolor="#ccc000" width="60" align="center"><b>Order ID</b></td>
            <td bgcolor="#ccc000" width="120" align="center"><b>Order DateTime</b></td>
            <td bgcolor="#ccc000" width="120" align="center"><b>Processed DateTime</b></td>
            <td bgcolor="#ccc000" width="100" align="center"><b>SHIP TYPE</b></td>
            <td bgcolor="#ccc000" width="80" align="center"><b>ORDER TYPE</b></td>
            <td bgcolor="#ccc000" width="200" align="left"><b>SHIPPING ADDRESS</b></td>            
            <td bgcolor="#ccc000" width="100" align="center"><b>ExtOrderNumber</b></td>
            <td bgcolor="#ccc000" width="60" align="right"><b>Shipping Fee</b></td>
            <td bgcolor="#ccc000" width="60" align="right"><b>Order SubTotal</b></td>
        </tr>
	<%
	do until objRsOrders.eof
		nRows = nRows + 1
	%>
        <tr onMouseOver="this.className='mouseover'" onMouseOut="this.className=''">
        	<td>
            	<a href="#" onclick="showInvoice('<%=objRsOrders("orderid")%>','<%=objRsOrders("accountid")%>');"><%=objRsOrders("orderid")%></a>
			</td>
        	<td><%=objRsOrders("orderdatetime")%></td>
        	<td><%=objRsOrders("thub_posted_date")%></td>
        	<td><%=objRsOrders("shiptype")%></td>
        	<td><%=objRsOrders("ordertype")%></td>
        	<td><%=objRsOrders("sAddress") & "<br>" & objRsOrders("sCity") & "<br>" & objRsOrders("sState") & ", " & objRsOrders("sZip")%></td>
        	<td><%=objRsOrders("extOrderNumber")%></td>
        	<td><%=formatcurrency(objRsOrders("ordershippingfee"))%></td>
        	<td><%=formatcurrency(objRsOrders("ordersubtotal"))%></td>
        </tr>    	
    <%
		objRsOrders.movenext
	loop
	%>
    </table>
    <%
else
	response.write "No data to display"
end if
%>
<script>
function showInvoice(orderid,accountid) 
{
	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
</script>