<%
	thisUser = Request.Cookies("username")
	pageTitle = "Sales Report Totals"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim siteID : siteID = request.form("siteID")
	dim startingDate : startingDate = prepStr(request.form("startingDate"))
	dim endingDate : endingDate = prepStr(request.form("endingDate"))
	dim showShip : showShip = prepInt(request.Form("showShip"))
	if isnull(siteID) or len(siteID) < 1 then siteID = ""
	if isnumeric(siteID) then siteID = cdbl(siteID)
	if startingDate = "" then startingDate = date-8
	if endingDate = "" then endingDate = date-1
	
	sql = "select site_id, longDesc from xStore where site_id < 11"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	myWhere = ""
	if isnumeric(siteID) then myWhere = " and b.store = " & siteID
	sql = "select sum(a.quantity) as qty, b.orderID, b.store, b.orderShippingFee, c.itemDesc, c.price_our, c.price_co, c.price_ca, c.price_ps, c.cogs, c.partNumber, d.typeName from we_orderDetails a left join we_orders b on a.orderID = b.orderID join we_items c on a.itemID = c.itemID left join we_types d on c.typeID = d.typeID where b.approved = 1 and b.cancelled is null and b.store is not null and a.entryDate > '" & startingDate & "' and a.entryDate < '" & cdate(endingDate) + 1 & "'" & myWhere & " group by b.orderID, b.store, b.orderShippingFee, c.itemDesc, c.price_our, c.price_co, c.price_ca, c.price_ps, c.cogs, c.partNumber, d.typeName order by d.typeName, c.partNumber"
	session("errorSQL") = sql
	set reportRS = oConn.execute(sql)
	
	sql = "select top 5 sum(a.quantity) as sold, a.itemID, c.itemPic, c.itemDesc, (select top 1 inv_qty from we_items where partNumber = c.partNumber and master = 1 order by inv_qty desc) as inv_qty from we_orderDetails a left join we_orders b on a.orderID = b.orderID left join we_items c on a.itemID = c.itemID where b.store is not null and b.approved = 1 and b.cancelled is null and a.entryDate > '" & startingDate & "' and a.entryDate < '" & cdate(endingDate) + 1 & "'" & myWhere & " group by a.itemID, c.itemPic, c.itemDesc, c.partNumber order by 1 desc"
	session("errorSQL") = sql
	set top5RS = oConn.execute(sql)
	
	dim fs,tfile
	set fs=Server.CreateObject("Scripting.FileSystemObject")
	set tfile=fs.CreateTextFile(server.MapPath("/admin/tempCSV/salesReportTotals.csv"))
	
	tfile.WriteLine("Part Category,Qty Sold,Selling Price,Cogs,Gross Profit,Cogs %,Gross Magin %,% of Total Sales")
%>
<form action="/admin/salesReportTotals.asp" method="post">
<table border="0" cellpadding="3" cellspacing="0" align="center" style="padding-top:20px;">
	<tr><td align="center" style="font-size:18px; font-weight:bold; border-bottom:1px solid #000;">Wireless Emporium Inc.<br />Sales Report Totals</td></tr>
	<tr>
    	<td align="center" style="padding-top:20px;">
            <div style="float:left; width:420px; margin-left:150px;">
                <div style="float:left; width:200px; text-align:right;">Reporting on Site:</div>
                <div style="float:left; width:200px; padding-left:10px; text-align:left; border:1px solid #fff;">
                    <select name="siteID">
                        <option value="">All Sites</option>
                        <%
                        do while not rs.EOF
                        %>
                        <option value="<%=rs("site_id")%>"<% if cdbl(rs("site_id")) = siteID then %> selected="selected"<% end if %>><%=rs("longDesc")%></option>
                        <%
                            rs.movenext
                        loop
                        %>
                    </select>
                </div>
                <div style="float:left; width:200px; text-align:right; padding-top:10px;">Starting Date:</div>
                <div style="float:left; padding:10px 0px 0px 10px; width:200px; text-align:left;">
                    <input type="text" name="startingDate" value="<%=startingDate%>" size="8" />
                </div>
                <div style="float:left; width:200px; text-align:right; padding-top:10px;">Ending Date:</div>
                <div style="float:left; padding:10px 0px 0px 10px; width:200px; text-align:left;">
                    <input type="text" name="endingDate" value="<%=endingDate%>" size="8" />
                </div>
                <div style="float:left; width:200px; text-align:right; padding-top:10px;"><input type="checkbox" name="showShip" value="1"<% if showShip = 1 then %> checked="checked"<% end if %> /></div>
                <div style="float:left; padding:10px 0px 0px 10px; width:200px; text-align:left;">
                    Include Shipping Data
                </div>
                <div style="float:left; width:200px; text-align:left; padding-top:10px;"><a href="/admin/tempCSV/salesReportTotals.csv">Download CSV</a></div>
                <div style="float:left; width:200px; text-align:right; padding-top:10px;"><input type="submit" name="mySub" value="Pull Report" /></div>
            </div>
        </td>
    </tr>
    <tr>
    	<td>
        	<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0">
            	<tr bgcolor="#333333" style="font-weight:bold; color:#FFF;">
                	<td align="left" valign="bottom">Part Category</td>
                    <td align="center">Qty<br />Sold</td>
                    <td align="center">Selling<br />Price</td>
                    <td align="center" valign="bottom">Cogs</td>
                    <td align="center">Gross<br />Profit</td>
                    <td valign="bottom">Cogs %</td>
                    <td align="center">Gross<br />Margin %</td>
                    <td align="center">% of<br />Total Sales</td>
                </tr>
                <%
				bgColor = "#ffffff"
				curType = ""
				dataRow = 0
				maxQty = 0
				maxSales = 0
				maxCogs = 0
				totalShipping = 0
				sellingList = ""
				orderList = ""
				extraShipping = 0
				detailList = ""
				infoToWrite = ""
				do while not reportRS.EOF
					dataRow = dataRow + 1
					if curType <> reportRS("typeName") then
						curType = reportRS("typeName")
						totalQty = 0
						totleSales = 0
						totalCogs = 0
						do while curType = reportRS("typeName")
							if instr(orderList,reportRS("orderID") & "##") < 1 then
								orderList = orderList & reportRS("orderID") & "##"
							else
								extraShipping = extraShipping + reportRS("qty")
							end if
							curStore = reportRS("store")
							totalShipping = totalShipping + cdbl(reportRS("orderShippingFee"))
							totalQty = totalQty + reportRS("qty")
							extraShipping = extraShipping + (reportRS("qty") - 1)
							if curStore = 0 then
								totleSales = totleSales + (formatNumber(reportRS("price_our"),2) * reportRS("qty"))
								thisSale = reportRS("price_our") * reportRS("qty")
							elseif curStore = 1 then
								if isnull(reportRS("price_ca")) then
									totleSales = totleSales + (formatNumber(reportRS("price_our"),2) * reportRS("qty"))
									thisSale = reportRS("price_our") * reportRS("qty")
								else
									totleSales = totleSales + (formatNumber(reportRS("price_ca"),2) * reportRS("qty"))
									thisSale = reportRS("price_ca") * reportRS("qty")
								end if
							elseif curStore = 2 then
								if isnull(reportRS("price_co")) then
									totleSales = totleSales + (formatNumber(reportRS("price_our"),2) * reportRS("qty"))
									thisSale = reportRS("price_our") * reportRS("qty")
								else
									totleSales = totleSales + (formatNumber(reportRS("price_co"),2) * reportRS("qty"))
									thisSale = reportRS("price_co") * reportRS("qty")
								end if
							elseif curStore = 3 then
								if isnull(reportRS("price_ps")) then
									totleSales = totleSales + (formatNumber(reportRS("price_our"),2) * reportRS("qty"))
									thisSale = reportRS("price_our") * reportRS("qty")
								else
									totleSales = totleSales + (formatNumber(reportRS("price_ps"),2) * reportRS("qty"))
									thisSale = reportRS("price_ps") * reportRS("qty")
								end if
							elseif curStore = 10 then
								if isnull(reportRS("price_er")) then
									totleSales = totleSales + (formatNumber(reportRS("price_our"),2) * reportRS("qty"))
									thisSale = reportRS("price_our") * reportRS("qty")
								else
									totleSales = totleSales + (formatNumber(reportRS("price_er"),2) * reportRS("qty"))
									thisSale = reportRS("price_er") * reportRS("qty")
								end if
							end if
							totalCogs = totalCogs + (formatNumber(prepInt(reportRS("cogs")),2) * reportRS("qty"))
							if curType = "Faceplates" or curType = "Leather Cases" then
								usePN = left(reportRS("partNumber"),3)
								if instr(detailList,usePN & "##") < 1 then
									detailList = detailList & usePN & "##"
									session(usePN & "_qty") = reportRS("qty")
									session(usePN & "_sell") = thisSale
									session(usePN & "_cogs") = reportRS("cogs") * reportRS("qty")
								else
									session(usePN & "_qty") = session(usePN & "_qty") + reportRS("qty")
									session(usePN & "_sell") = session(usePN & "_sell") + thisSale
									session(usePN & "_cogs") = session(usePN & "_cogs") + (reportRS("cogs") * reportRS("qty"))
								end if
							end if
							reportRS.movenext
							if reportRS.EOF then exit do
						loop
						sellingList = sellingList & totleSales & ","
						maxQty = maxQty + totalQty
						maxSales = maxSales + totleSales
						maxCogs = maxCogs + totalCogs
					end if
				%>
                <tr bgcolor="<%=bgColor%>">
                	<% if curType = "Faceplates" or curType = "Leather Cases" then %>
                    <td align="left" valign="bottom"><a onclick="showDetails('<%=curType%>')" style="color:#00F; text-decoration:underline; cursor:pointer;"><%=curType%></a></td>
					<% else %>
                	<td align="left" valign="bottom"><%=curType%></td>
                    <% end if %>
                    <td align="right"><%=formatNumber(totalQty,0)%></td>
                    <td align="right"><%=formatcurrency(totleSales,2)%></td>
                    <td align="right"><%=formatcurrency(totalCogs,2)%></td>
                    <td align="right"><%=formatcurrency(totleSales - totalCogs,2)%></td>
                    <td align="right"><%=formatPercent(totalCogs/totleSales,2)%></td>
                    <td align="right"><%=formatPercent((totleSales - totalCogs)/totleSales,2)%></td>
                    <td align="right" id="dataRow_<%=dataRow%>">&nbsp;</td>
                </tr>
                <%
					infoToWrite = infoToWrite & curType & "," & totalQty & "," & totleSales & "," & totalCogs & "," & totleSales - totalCogs & "," & formatPercent(totalCogs/totleSales,2) & "," & formatPercent((totleSales - totalCogs)/totleSales,2) & ",##ofTotal##@@"
					
					if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
					if curType = "Faceplates" or curType = "Leather Cases" then
						detailArray = split(detailList,"##")
						for z = 0 to (ubound(detailArray) - 1)
							detailQty = prepInt(session(detailArray(z) & "_qty"))
							session(detailArray(z) & "_qty") = null
							detailSell = prepInt(session(detailArray(z) & "_sell"))
							session(detailArray(z) & "_sell") = null
							detailCogs = prepInt(session(detailArray(z) & "_cogs"))
							session(detailArray(z) & "_cogs") = null
							session("errorSQL") = "cur:" & detailArray(z) & "<br>detailQty:" & detailQty & "<br>detailSell:" & detailSell & "<br>detailCogs:" & detailCogs
							if detailQty > 0 then
				%>
                <tr bgcolor="#CCFFFF" id="<%=curType%>_<%=z%>" style="display:none;">
                	<td><%=detailArray(z)%></td>
                    <td align="right"><%=formatNumber(detailQty,0)%></td>
                    <td align="right"><%=formatcurrency(detailSell,2)%></td>
                    <td align="right"><%=formatcurrency(detailCogs,2)%></td>
                    <td align="right"><%=formatcurrency(detailSell - detailCogs,2)%></td>
                    <td align="right"><%=formatPercent(detailCogs/detailSell,2)%></td>
                    <td align="right"><%=formatPercent((detailSell - detailCogs)/detailSell,2)%></td>
                    <td align="right">&nbsp;</td>
                </tr>
                <%
							end if
						next
						if curType = "Faceplates" then
							fpDetails = z
						else
							lcDetails = z
						end if
						detailList = ""
					end if
				loop
				
				if showShip = 1 then
					dataRow = dataRow + 1
					orderArray = split(orderList,"##")
					shippingCogs = (ubound(orderArray)-1) * 3 + extraShipping
				%>
                <tr bgcolor="<%=bgColor%>">
                	<td align="left" valign="bottom">Shipping (Approx)</td>
                    <td align="right">&nbsp;</td>
                    <td align="right"><%=formatcurrency(totalShipping,2)%></td>
                    <td align="right"><%=formatCurrency(shippingCogs,2)%></td>
                    <td align="right"><%=formatcurrency(totalShipping - shippingCogs,2)%></td>
                    <td align="right"><%=formatPercent(shippingCogs/totalShipping,2)%></td>
                    <td align="right"><%=formatPercent((totalShipping - shippingCogs)/totalShipping,2)%></td>
                    <td align="right" id="dataRow_<%=dataRow%>">&nbsp;</td>
                </tr>
                <%
					infoToWrite = infoToWrite & "Shipping (Approx),," & totalShipping & "," & shippingCogs & "," & totalShipping - shippingCogs & "," & formatPercent(shippingCogs/totalShipping,2) & "," & formatPercent((totalShipping - shippingCogs)/totalShipping,2) & ",##ofTotal##@@"
					sellingList = sellingList & totalShipping & ","
					maxSales = maxSales + totalShipping
					maxCogs = maxCogs + shippingCogs
				end if
				
				if dataRow > 0 then
					session("errorSQL") = "dataRow:" & dataRow
				%>
                <tr bgcolor="#333333" style="font-weight:bold; color:#FFF;">
                	<td align="left" valign="bottom">Totals</td>
                    <td align="right"><%=formatNumber(maxQty,0)%></td>
                    <td align="right"><%=formatcurrency(maxSales,2)%></td>
                    <td align="right"><%=formatcurrency(maxCogs,2)%></td>
                    <td align="right"><%=formatcurrency(maxSales - maxCogs,2)%></td>
                    <td align="right"><%=formatPercent(maxCogs/maxSales,2)%></td>
                    <td align="right"><%=formatPercent((maxSales - maxCogs)/maxSales,2)%></td>
                    <td align="right" id="dataRow_<%=dataRow%>">100%</td>
                </tr>
                <%
					infoToWrite = infoToWrite & "Totals," & maxQty & "," & maxSales & "," & maxCogs & "," & maxSales - maxCogs & "," & formatPercent(maxCogs/maxSales,2) & "," & formatPercent((maxSales - maxCogs)/maxSales,2) & ",100%@@"
				end if
				%>
            </table>
        </td>
    </tr>
    <tr><td><div style="width:100%; background-color:#333; font-weight:bold; color:#FFF; font-size:16px; margin-top:20px;">Top Selling Products</div></td></tr>
    <tr>
    	<td>
        	<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="600" align="center">
            	<%
				bgColor = "#ffffff"
				do while not top5RS.EOF
				%>
                <tr bgcolor="<%=bgColor%>">
                	<td align="center"><div style="width:50px;"><%=top5RS("sold")%><br />Sold</div></td>
                	<td><img src="/productPics/thumb/<%=top5RS("itemPic")%>" border="0" /></td>
                    <td width="100%">
						<%=top5RS("itemDesc")%><br />
                        Current Inventory: <%=top5RS("inv_qty")%>
                    </td>
                </tr>
                <%
					top5RS.movenext
					if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
				loop
				%>
            </table>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<% if dataRow = 0 then response.End() %>
<script language="javascript">
	var maxVal = <%=maxSales%>
	var curVal, curPerc
	var fpDetails = <%=fpDetails%>
	var lcDetails = <%=lcDetails%>
	<%
	sellingArray = split(sellingList,",")
	infoArray = split(infoToWrite,"@@")
	lastRead = 0
	for i = 0 to (ubound(sellingArray)-1)
		tfile.WriteLine(replace(infoArray(i),"##ofTotal##",formatpercent(sellingArray(i)/maxSales)))
	%>
	curVal = <%=sellingArray(i)%>
	curPerc = curVal / maxVal
	curPerc = "" + curPerc * 100
	document.getElementById("dataRow_<%=i+1%>").innerHTML = curPerc.substring(0,4) + "%"
	<%
		lastRead = i
	next
	lastRead = lastRead + 1
	tfile.WriteLine(infoArray(lastRead))
	
	tfile.close
	set tfile=nothing
	set fs=nothing
	%>
	
	function showDetails(typeName) {
		if (typeName == "Faceplates") {
			for (i=0;i<fpDetails;i++) {
				if (document.getElementById(typeName + "_" + i).style.display == '') {
					document.getElementById(typeName + "_" + i).style.display = 'none'
				}
				else {
					document.getElementById(typeName + "_" + i).style.display = ''
				}
			}
		}
		if (typeName == "Leather Cases") {
			for (i=0;i<lcDetails;i++) {
				if (document.getElementById(typeName + "_" + i).style.display == '') {
					document.getElementById(typeName + "_" + i).style.display = 'none'
				}
				else {
					document.getElementById(typeName + "_" + i).style.display = ''
				}
			}
		}
	}
</script>