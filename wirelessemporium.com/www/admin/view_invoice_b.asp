<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%header = 0%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
dim nAccountID, nOrderID, InvoiceType
nOrderID = request("orderID")
InvoiceType = "Admin"

if nOrderID > 0 then
	dim sCheckSql, oRsCheck
	nAccountID = 0
	sCheckSql = "SELECT accountID FROM we_Orders WHERE orderID = '" & nOrderID & "'"
	set oRsCheck = oConn.execute(sCheckSql)

	if oRsCheck.eof then
		sCheckSql = replace(sCheckSql, "we_Orders", "we_Orders_historical")
		set oRsCheck = oConn.execute(sCheckSql)
	end if		

	if not oRsCheck.eof then nAccountID = oRsCheck("accountID")
	oRsCheck.close
	set oRsCheck = nothing
	
	if nAccountID > 0 then
		%>
        <div id="orderConDiv">
		<!--#include virtual="/includes/asp/inc_receiptAdmin_b.asp"-->
        </div>
		<%
	else
		response.redirect "view_invoice_b.asp?error=noreturn"
		response.end
	end if
else
	%>
	<form name="form1" method="post" action="view_invoice_b.asp">
		<table width="250" border="0" align="center" cellpadding="1" cellspacing="0">
			<tr bgcolor="#FF6600">
				<td align="center" nowrap>
					<p><font face="Verdana" size="2" color="#FFFFFF"><b>
					<%if request("error") = "noreturn" then response.write "No data to match your request<br>"%>
					</b></font></p>
				</td>
			</tr>
			<tr>
				<td bgcolor="#000066">
					<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0" bgcolor="#EAEAEA">
						<tr bgcolor="#FF6600">
							<td width="28%"><p><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>Account ID:</b></font></p></td>
							<td width="72%"><input type="text" name="accountID" id="accountID"></td>
						</tr>
						<tr bgcolor="#FF6600">
							<td width="28%"><p><b><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Order ID:</font></b></p></td>
							<td width="72%"><input type="text" name="orderID" id="orderID"></td>
						</tr>
						<tr align="center" bgcolor="#FF6600">
							<td colspan="2"><input type="submit" name="Submit" value="View Invoice"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	<%
end if
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function ajax(newURL,rLoc) {
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						//nothing
					}
					else if(document.getElementById(rLoc)) {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				}
				else {
					alert("Error performing action")
				}
			}
		};
		httpRequest.send(null);
	}
	
	function showHide(showDiv,hideDiv) {
		document.getElementById(hideDiv).style.display = 'none';
		document.getElementById(showDiv).style.display = '';
	}
</script>
