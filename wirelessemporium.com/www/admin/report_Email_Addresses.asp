<%
pageTitle = "WE Admin Site - Export E-mail Addresses"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">

<%
if request("submitted") <> "" then
	strError = ""
	dateStart = request("dateStart")
	dateEnd = request("dateEnd")
	if not isDate(dateStart) or not isDate(dateEnd) then
		strError = strError & "<p>dateStart and dateEnd must both be valid dates</p>"
	else
		dateStart = dateValue(dateStart)
		dateEnd = dateValue(dateEnd)
		if dateEnd < dateStart then strError = strError & "<p>dateEnd must be later than or equal to dateStart</p>"
	end if
	
	if strError = "" then
		dim fs, file, filename, path
		'Map the file name to the physical path on the server.
		Set fs = CreateObject("Scripting.FileSystemObject")
		filename = "export_Email_Addresses.csv"
		path = Server.MapPath("tempCSV") & "\" & filename
		path = replace(path,"admin\","")
		Set file = fs.CreateTextFile(path)
		
		dateEnd = dateAdd("D",1,dateEnd)
		SQL = "SELECT * FROM we_emailopt"
		SQL = SQL & " WHERE dateEntered >= '" & dateStart & "' AND dateEntered <= '" & dateEnd & "'"
		SQL = SQL & " AND email LIKE '%@%' AND email LIKE '%.%'"
		SQL = SQL & " AND email NOT IN (SELECT email FROM we_emailopt_out)"
		SQL = SQL & " ORDER BY dateEntered"
		Set RS = Server.CreateObject("ADODB.Recordset")
		response.write "<h3>" & SQL & "</h3>" & vbCrLf
		RS.open SQL, oConn, 3, 3
		response.write "<h3>" & RS.recordcount & " TOTAL records found</h3>" & vbCrLf
		
		strline = "fname,email,dateEntered"
		file.WriteLine strline
		
		do until RS.eof
			strline = chr(34) & trim(RS("fname")) & chr(34) & ","
			strline = strline & chr(34) & trim(RS("email")) & chr(34) & ","
			strline = strline & chr(34) & RS("dateEntered") & chr(34) & ","
			file.WriteLine strline
			RS.movenext
		loop
		
		RS.close
		Set RS = nothing
		file.close()
		response.write "Here is the <a href='/tempCSV/" & filename & "'>file</a>"
	else
		%>
		<center>
		<font size="+2" color="red"><%=strError%></font>
		<a href="javascript:history.back();">BACK</a>
		</center>
		<%
	end if
else
	%>
	<p>&nbsp;</p>
	<h3>Export E-mail Addresses</h3>
	<br>
	<form action="report_Email_Addresses.asp" method="post">
		<p><input type="text" name="dateStart">&nbsp;&nbsp;dateStart</p>
		<p><input type="text" name="dateEnd">&nbsp;&nbsp;dateEnd</p>
		<p><input type="submit" name="submitted" value="Generate CSV"></p>
	</form>
	<%
end if
%>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
