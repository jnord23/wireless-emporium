<%
	thisUser = Request.Cookies("username")
	pageTitle = "Shovel Ready"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim sql, rs
	dim bgColor : bgColor = "#fff"
	
	sql =	"select top 1 sortValue, COUNT(*) as dupVal from we_Website_Tasks where shovelReady = 1 group by sortValue order by 2 desc"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	if prepInt(rs("dupVal")) > 1 then
		'sql = 	"update we_Website_Tasks set shovelReady = 0, sortValue = null where dateCompleted is not null and shovelReady = 1"
		'session("errorSQL") = sql
		'oConn.execute(sql)
		
		sql = 	"select a.id, a.sortValue, b.fname + ' ' + b.lname as adminAssigned, a.title, b.fname + ' ' + b.lname as adminEntered " &_
				"from we_Website_Tasks a " &_
					"join we_adminUsers b on a.assignedTo = b.adminID " &_
					"join we_adminUsers c on a.EnteredBy = c.adminID " &_
				"where shovelReady = 1 and status <> 10 and deleted = 0 " &_
				"order by a.sortValue, a.id"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		dim sortCnt : sortCnt = 1
		do while not rs.EOF
			sql = "update we_Website_Tasks set sortValue = " & sortCnt & " where id = " & rs("id")
			session("errorSQL") = sql
			oConn.execute(sql)
			
			sortCnt = sortCnt + 1
			rs.movenext
		loop
	end if
	
	sql = 	"select a.id, a.sortValue, b.fname + ' ' + b.lname as adminAssigned, a.title, b.fname + ' ' + b.lname as adminEntered " &_
			"from we_Website_Tasks a " &_
				"join we_adminUsers b on a.assignedTo = b.adminID " &_
				"join we_adminUsers c on a.EnteredBy = c.adminID " &_
			"where shovelReady = 1 and status <> 10 and deleted = 0 " &_
			"order by a.sortValue, a.id"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
%>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<style>
	.fullTable 			{ margin:0px auto; width:862px; }
	.titleRow 			{ background-color:#333; color:#FFF; font-weight:bold; width:100%; }
	.taskID				{ width:50px; text-align:center; padding-top:1px; }
	.adminName			{ width:150px; padding:2px; }
	.projTitle			{ width:475px; padding:2px; height:16px; overflow:hidden; }
	.projLine			{ margin:5px 0px; border:1px solid #000; background-image:url(/images/backgrounds/projBg.gif); }
	.shovelReadyGraphic	{ background-image:url(/images/backgrounds/shovelReady.jpg); width:862px; height:191px; margin:20px auto 0px auto; }
	
	#sortable 			{ list-style-type: none; padding:0px; margin:0px; }
	#sortable li 		{ margin:5px 0px; border:1px solid #000; background-image:url(/images/backgrounds/projBg.gif); height:20px; }
</style>
<script>
	var activeTask = 0;
	var activeSort = 0;
	
	function setActiveTask(taskID,sortValue) {
		activeTask = taskID;
		activeSort = sortValue;
	}
	
	<% if adminID = 148 or adminID = 2 or adminID = 54 then %>
	$(function() {
		$( "#sortable" ).sortable({
			stop: function(event, ui) {
				ajax('/ajax/admin/webTasks.asp?taskID=' + activeTask + '&oldPosition=' + activeSort + '&newPosition=' + (parseInt(ui.item.index()) + 1),'dumpZone');
				<% if adminID = 54 then %>
				setTimeout("alert(document.getElementById('dumpZone').innerHTML)",1000);
				<% end if %>
			}
		});
		$( "#sortable" ).disableSelection();
	});
	<% end if %>
</script>
<div class="tb shovelReadyGraphic"></div>
<div class="tb fullTable">
	<div class="tb titleRow">
		<div class="fl taskID">#</div>
        <div class="fl adminName">Assigned To</div>
	    <div class="fl projTitle">Project Title</div>
    	<div class="fl adminName">Entered By</div>
	</div>
    <ul id="sortable">
		<%
		dim lap : lap = 0
		do while not rs.EOF
			taskID = prepInt(rs("id"))
			sortValue = prepInt(rs("sortValue"))
			'sql = "update we_Website_Tasks set sortValue = " & lap & " where id = " & taskID
			'oConn.execute(sql)
			lap = lap + 1
		%>
		<li>
        	<div class="tb" style="width:100%; cursor:pointer;" onmousedown="setActiveTask(<%=taskID%>,<%=sortValue%>)">
				<div class="fl taskID"><a href="/admin/website_tasks_add_edit.asp?id=<%=taskID%>" style="color:#00F;" target="activeTask"><%=taskID%></a></div>
                <div class="fl adminName"><%=rs("adminAssigned")%></div>
			    <div class="fl projTitle"><%=rs("title")%></div>
    			<div class="fl adminName"><%=rs("adminEntered")%></div>
            </div>
		</li>
		<%
			rs.movenext
			if bgColor = "#fff" then bgColor = "#ccc" else bgColor = "#fff"
		loop
		%>
    </ul>
</div>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->