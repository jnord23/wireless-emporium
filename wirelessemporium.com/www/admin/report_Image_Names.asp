<%
pageTitle = "Find Image File Names by Part Number"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<p class="biggerText"><%=pageTitle%></p>

<%
if request("submitted") <> "" then
	showblank = "&nbsp;"
	shownull = "-null-"
	
	SQL = "SELECT itemID,PartNumber,itemDesc,itemPic,inv_qty FROM WE_items WHERE PartNumber LIKE '%" & request("PartNumber") & "%' ORDER BY PartNumber,itemDesc"
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	
	response.write "<h3>" & SQL & "</h3>" & vbCrLf
	
	if RS.eof then
		response.write "No records matched<br><br>So cannot make table..."
	else
		%>
		<h3><%=RS.recordcount%> records found</h3>
		<table border="1">
			<tr>
				<%
				for each whatever in RS.fields
					%>
					<td><b><%=whatever.name%></b></td>
					<%
				next
				%>
			</tr>
			<%
			do until RS.eof
				%>
				<tr>
					<%
					for each whatever in RS.fields
						thisfield = whatever.value
						if isnull(thisfield) then
							thisfield = shownull
						end if
						if trim(thisfield) = "" then
							thisfield = showblank
						end if
						%>
						<td valign="top"><%=thisfield%></td>
						<%
					next
					%>
				</tr>
				<%
				RS.movenext
			loop
			%>
		</table>
		<%
	end if
	
	RS.close
	Set RS = nothing
end if
%>

<form action="report_Image_Names.asp" method="post">
	<p><input type="text" name="PartNumber" value="<%=request("PartNumber")%>"></p>
	<p><input type="submit" name="submitted" value="Find"></p>
</form>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
