<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Manipulate Pixels"
	header = 1
	
'	response.write "request.QueryString:" & request.QueryString & "<br>"
'	response.write "request.Form:" & request.Form("txtEventText")
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/framework/utility/base64.asp"-->
<script type="text/javascript" src="/includes/admin/checkbox.js"></script>
<script>
function onSetMasterCheckBox(frm)
{
	if (isAllChecked(frm.modelID))			frm.chkMaster.checked = true;
	else if (isAllUnChecked(frm.modelID))	frm.chkMaster.checked = false;
}
</script>
<%
sql	=	"select id, site_id, physicalAbsolutePath, path_desc from XPhysicalPathMatrix order by site_id, path_desc"
arrPath = getDbRows(sql)
sql	=	"select brandid, brandName from we_brands where brandid not in (12) order by brandName"
arrBrands = getDbRows(sql)
sql	=	"select typeid, typeName from we_types where typeid not in (4, 9) order by typeName"
arrTypes = getDbRows(sql)
sql	=	"select subtypeid, subtypeName, typeid, typeName from v_subtypeMatrix order by typeName, subtypeName"
arrSubTypes = getDbRows(sql)

adminID = prepInt(session("adminID"))
pixelID = prepInt(request.form("hidPixelID"))
hidSiteID = prepInt(request.form("hidSiteID"))
pathID = prepInt(request.form("cbPath"))
brandID = prepInt(request.form("cbBrands"))
modelID = 0
typeID = prepInt(request.form("cbTypes"))
subtypeID = prepInt(request.form("cbSubTypes"))
carrierID = 0
isPhone = 0
genre = null
vendorCode = prepStr(request.form("txtVendorCode"))
if vendorCode = "" then vendorCode = null
url = null
validFrom = null
validTo = null
isActive = request.form("chkActive")
if isActive = "on" then
	isActive = 1
else
	isActive = 0
end if

isLive = request.form("chkLive")
if isLive = "on" then
	isLive = 1
else
	isLive = 0
end if

tagPosition = prepInt(request.form("cbPosition"))
eventText = prepStr(request.form("txtEventText"))
comment = prepStr(request.form("txtTitle"))
updateType = prepStr(request.form("hidUpdateType"))

if request("submitted") <> "" then
	if request("submitted") = "Delete" then
		sql	=	"delete from we_pixels where id = '" & pixelID & "'"
		oConn.execute(sql)
	else
		session("errorSQL") = "exec UpdatePixel"
'		response.write "eventText:<br>" & base64decode(eventText) & "<br>"
'		response.write "request.form(""chkLive""):" & request.form("chkLive")
'		response.end
		Dim cmd
		Set cmd = Server.CreateObject("ADODB.Command")
		Set cmd.ActiveConnection = oConn
		cmd.CommandText = "UpdatePixel"
		cmd.CommandType = adCmdStoredProc 
	
		cmd.Parameters.Append cmd.CreateParameter("ret", adInteger, adParamReturnValue)
		cmd.Parameters.Append cmd.CreateParameter("p_adminid", adInteger, adParamInput)	
		cmd.Parameters.Append cmd.CreateParameter("p_pixelid", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_siteid", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_pathid", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_brandid", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_modelid", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_typeid", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_subtypeid", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_carrierid", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_isPhones", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_genre", adVarChar, adParamInput, 100)
		cmd.Parameters.Append cmd.CreateParameter("p_vendorCode", adVarChar, adParamInput, 20)
		cmd.Parameters.Append cmd.CreateParameter("p_url", adVarChar, adParamInput, 2000)
		cmd.Parameters.Append cmd.CreateParameter("p_validFrom", adVarChar, adParamInput, 10)
		cmd.Parameters.Append cmd.CreateParameter("p_validTo", adVarChar, adParamInput, 10)
		cmd.Parameters.Append cmd.CreateParameter("p_isActive", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_isLive", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_tagPosition", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_text", adVarChar, adParamInput, 8000)
		cmd.Parameters.Append cmd.CreateParameter("p_comment", adVarChar, adParamInput, 2000)
		cmd.Parameters.Append cmd.CreateParameter("p_updateType", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_out_msg", adVarChar, adParamOutput, 500)
	
		cmd("p_adminid")		=	adminID
		cmd("p_pixelid")		=	pixelID
		cmd("p_siteid")			=	hidSiteID
		cmd("p_pathid")			=	pathID
		cmd("p_brandid")		=	brandID
		cmd("p_modelid")		=	modelID
		cmd("p_typeid")			=	typeID
		cmd("p_subtypeid")		=	subtypeID
		cmd("p_carrierid")		=	carrierID
		cmd("p_isPhones")		=	isPhone
		cmd("p_genre")			=	genre
		cmd("p_vendorCode")		=	vendorCode
		cmd("p_url")			=	url
		cmd("p_validFrom")		=	validFrom
		cmd("p_validTo")		=	validTo
		cmd("p_isActive")		=	isActive
		cmd("p_isLive")			=	isLive
		cmd("p_tagPosition")	=	tagPosition
		cmd("p_text")			=	base64decode(eventText)
		cmd("p_comment")		=	comment
		cmd("p_updateType")		=	updateType
	
		cmd.Execute
	
		retMsg	=	cmd("p_out_msg")
		retVal	= 	cmd("ret")
		
		select case hidSiteID
			case 0 : strSite = "WE"
			case 1 : strSite = "CA"
			case 2 : strSite = "CO"
			case 3 : strSite = "PS"
			case 4 : strSite = "TM"
			case 5 : strSite = "WE Mobile"
			case 6 : strSite = "CO Mobile"
		end select
		
		if retVal = 1 and isLive = 1 then
			targetPage = ""
			sql = "select b.path_desc from we_pixels a join xphysicalpathmatrix b on a.path_id = b.id where a.id = '" & pixelID & "'"
			session("errorSQL") = sql
			set rsTemp = oConn.execute(sql)
			if not rsTemp.eof then targetPage = rsTemp("path_desc")
			cdo_from = "WE Marketing <marketing@wirelessemporium.com>"
			cdo_subject = "(" & strSite & ") New PIxel is up on Live - " & comment
			cdo_body = "'" & comment & "' has been added(updated) on " & strSite & "<br>- Page Implemented: " & targetPage
			cdo_to = "terry@wirelessemporium.com,jon@wirelessemporium.com,wemarketing@wirelessemporium.com,ruben@wirelessemporium.com"
			CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		end if
	end if
end if
%>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="800">
	<tr>
    	<td><h1 style="margin:0px;">Manipulate Pixels</h1></td>
    </tr>
    <tr>
    	<td style="font-size:10px;">
        	This application allows the user to manipulate pixels.
        </td>
    </tr>
    <tr>
    	<td style="font-size:12px; padding-bottom:5px;">
        	- Replacement Keywords: 
            {http}, {brandid}, {categoryid}, {subcategoryid}, {modelid}, {itemid}, {orderid}, {orderSubTotal}, {orderGrandTotal}, {orderTax}, {shippingFee}, {ga_conv_items}, {ga_conv_img_items}
        </td>
    </tr>
    <tr>
    	<td style="padding-bottom:5px;">
            <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
                <tr>
                    <td width="100" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-on" id="id_header_0" onClick="showHide(0);">WE</td>
                    <td width="100" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-off" id="id_header_1" onClick="showHide(1);">CA</td>
                    <td width="100" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-off" id="id_header_2" onClick="showHide(2);">CO</td>
                    <td width="100" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-off" id="id_header_3" onClick="showHide(3);">PS</td>
                    <td width="100" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-off" id="id_header_4" onClick="showHide(4);">TM</td>
                    <td width="100" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-off" id="id_header_5" onClick="showHide(5);">WE Mobile</td>
                    <td width="100" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-off" id="id_header_6" onClick="showHide(6);">CO Mobile</td>
                    <td width="50" class="tab-header-off">&nbsp;</td>
                </tr>
            </table>
		</td>
    </tr>
    <%
	if retVal < 0 then
	%>
    <tr>
    	<td style="padding:5px; border:1px solid red;">
        	<h2>Error occurred:</h2>
	    	<h3><%=retMsg%></h3>
        </td>
	</tr>
    <%
	end if
	%>
    <tr>
    	<td>
        	<%
			for siteID=0 to 6
				if siteID=0 then
				%>
		        	<div id="tbl_<%=siteID%>" style="padding-top:10px;">                
                <%
				else 
				%>
		        	<div id="tbl_<%=siteID%>" style="display:none; padding-top:10px;">
                <%
				end if
				
				arrTemp = filter2DRS(arrPath, 1, siteID)
				%>
            	<form name="frm<%=siteID%>" method="post"> 
                <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
                    <tr>
                        <td>
                        	<select name="cbPath" onchange="onPath(<%=siteID%>, this.value);">
							<%
							for nRow=0 to ubound(arrTemp,2)
							%>
                            <option value="<%=arrTemp(0, nRow)%>" <%if cstr(arrTemp(0, nRow)) = cstr(pathID) then %>selected<% end if%>><%=arrTemp(3, nRow)%></option>
                            <%
							next
							%>
                            </select>
                        </td>
					</tr>
                    <tr>
                    	<td style="padding:5px 0px 5px 0px; border-bottom:2px solid #000;">
                            <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
                                <tr>
                                	<td colspan="2" align="left">
			                        	<strong>ADD NEW CODE</strong>
                                    </td>
								</tr>
                                <tr>
                                	<td align="left">
                                    	Pixel Title: <input type="text" name="txtTitle" value="" size="50" /> <br />
										<textarea rows="5" cols="60" name="txtEventText"></textarea>
                                    </td>
                                	<td align="left" valign="bottom">
                                        <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
                                            <tr>
                                                <td align="left">Brands:</td>
                                                <td align="right">
                                                    <select name="cbBrands" style="width:150px;">
                                                        <option value="0" selected>All Brands</option>
                                                    <%
                                                    for nRow=0 to ubound(arrBrands,2)
                                                    %>
                                                        <option value="<%=arrBrands(0, nRow)%>"><%=arrBrands(1, nRow)%></option>
                                                    <%
                                                    next
                                                    %>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                            	<td align="left">Categories:</td>
                                                <td align="right">
                                                    <select name="cbTypes" style="width:150px;">
                                                        <option value="0" selected>All Categories</option>
                                                    <%
                                                    for nRow=0 to ubound(arrTypes,2)
                                                    %>
                                                        <option value="<%=arrTypes(0, nRow)%>"><%=arrTypes(1, nRow)%></option>
                                                    <%
                                                    next
                                                    %>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
	                                            <td align="left">Sub-Categories:</td>
                                                <td align="right">
                                                    <select name="cbSubTypes" style="width:150px;">
                                                        <option value="0" selected>All Sub-Categories</option>
                                                    <%
                                                    for nRow=0 to ubound(arrSubTypes,2)
                                                    %>
                                                        <option value="<%=arrSubTypes(0, nRow)%>"><%=arrSubTypes(1, nRow)%></option>
                                                    <%
                                                    next
                                                    %>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                            	<td align="left">Tag Position:</td>
                                                <td align="right">
                                                    <select name="cbPosition" style="width:150px;">
                                                        <option value="1">Head</option>
                                                        <option value="2">Body Start</option>
                                                        <option value="3" selected>Body End</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                            	<td align="left">Vendor Code:</td>
                                                <td align="right"><input type="text" name="txtVendorCode" value="" /></td>
                                            </tr>
										</table>
                                    </td>
								</tr>
								<tr>
                                	<td colspan="2" align="left">
										<input type="submit" name="submitted" value="ADD NEW CODE" onclick="return checkForm(document.frm<%=siteID%>);" />
                                    </td>
                                </tr>                                
							</table>
                        </td>
					</tr>
				</table>
                <input type="hidden" name="hidSiteID" value="<%=siteID%>" />
                <input type="hidden" name="chkActive" value="on" />
				<input type="hidden" name="hidUpdateType" value="0" />
				</form>
                <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
					<tr>
						<td><div id="tbl_<%=siteID%>_list">&nbsp;</div></td>
					</tr>
                </table>
            </div>
            <%
			next
			%>
		</td>
    </tr>
</table>    
</form>
<script>
	window.onload = function()
	{
		var siteid = <%=hidSiteID%>;
		showHide(siteid);

		for (nSiteid=0; nSiteid<=6; nSiteid++)
		{
			pathid = eval('document.frm' + nSiteid).cbPath.value;
			onPath(nSiteid, pathid);
		}
	}

	function checkDelForm(form)
	{
		var strPrompt = "Warning: You are about to delete the pixel\r\nDo you wish to proceed?";
		if (confirm(strPrompt)) return true;

		return false;
	}

	
	function checkForm(form)
	{
		isLive = form.chkLive;
		if (typeof isLive == "object") isLive.disabled = false;
			
		pixel = form.txtEventText.value;
		if (pixel == "")
		{
			alert("No code has been provided");
			return false;	
		}
		
		form.txtEventText.value = window.btoa(form.txtEventText.value);
				
		return true;
	}

 	function onPath(siteID, pathID)
	{
		ajax('/ajax/admin/ajaxPixels.asp?siteID=' + siteID + '&pathID=' + pathID + '&adminID=<%=adminID%>', 'tbl_' + siteID + '_list');
	}
	
	function showHide(showID)
	{
		for (i=0; i<=6; i++)
		{
			document.getElementById('id_header_' + i).className = "tab-header-off";
			document.getElementById('tbl_' + i).style.display = "none";			
		}
				
		document.getElementById('id_header_' + showID).className = "tab-header-on";
		document.getElementById('tbl_' + showID).style.display = "";
	}
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->