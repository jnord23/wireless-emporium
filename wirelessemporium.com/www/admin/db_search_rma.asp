<%
pageTitle = "RMA"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<script type="text/javascript" src="/includes/admin/js/sortable/sortable.js"></script>
<style>
.mouseover	{ background-color:#9BCEFF;}
</style>

<link href="/includes/css/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/includes/js/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/includes/js/cal.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function () {
		$('#id_sdate').simpleDatepicker();
		$('#id_edate').simpleDatepicker();
	});
</script>
<%
searchExcel = prepStr(request.form("hidExcel"))
if searchExcel <> "" then response.redirect "/admin/db_search_rma_excel.asp?txtSDate=" & request.form("txtSDate") & "&txtEDate=" & request.form("txtEDate") & "&cbView=" & request.form("cbView") & "&cbView2=" & request.form("cbView2") & "&txtOrderID=" & request.form("txtOrderID")

strSDate = request.form("txtSDate")
strEDate = request.form("txtEDate")
if strSDate = "" or not isdate(strSDate) then strSDate = Date-7
if strEDate = "" or not isdate(strEDate) then strEDate = Date

strProcess = request.form("chkProcess")
strOrderID = request.form("txtOrderID")
strView = request.form("cbView")
strView2 = prepInt(request.form("cbView2"))
if strProcess <> "" then
	strProcess = getCommaString(strProcess)
end if

sqlWhere = 	"where 	b.approved = 1 and (b.cancelled = 0 or b.cancelled is null) " & vbcrlf & _
			"	and a.entryDate >= '" & strSDate & "' and a.entryDate < '" & dateadd("d",1,strEDate) & "'" & vbcrlf

if strOrderID <> "" then
	strOrderID = trim(replace(replace(strOrderID, chr(10), ""), chr(9), ""))
	sqlWhere = sqlWhere & vbcrlf & "	and a.orderid = '" & ds(strOrderID) & "'"
end if

if strView = "" then strView = "3" end if

if strView = "2" then
	sqlWhere = sqlWhere & vbcrlf & 	"	and a.childOrderID is not null and a.cancelDate is null" & vbcrlf  & _
									"	and	(processDate > dateadd(dd, -30, getdate()) or processDate is null)" & vbcrlf 
elseif strView = "3" then
	sqlWhere = sqlWhere & vbcrlf & 	"	and a.childOrderID is null and a.cancelDate is null and	(processDate > dateadd(dd, -30, getdate()) or processDate is null)"
elseif strView = "4" then
	sqlWhere = sqlWhere & vbcrlf & 	"	and a.cancelDate is not null"
else
	sqlWhere = sqlWhere & vbcrlf & 	"	and	(processDate > dateadd(dd, -30, getdate()) or processDate is null)"
end if

if strView2 = 0 then
	sqlWhere = sqlWhere & vbcrlf & 	"	and	c.inv_qty > 0"
end if

if strVIew = "3" then
	sqlOrder = "order by inv_qty desc, entryDate desc, orderid, partnumber"
else
	sqlOrder = "order by entryDate desc, orderid, partnumber"
end if

sql = 	"select	distinct a.orderid, convert(varchar(16), a.entrydate, 20) entryDate, isnull(s.fname + ' ' + s.lname, 'N/A') username, v.accountid, convert(varchar(16), b.scandate, 20) scandate" & vbcrlf & _
		"	, 	x.shortDesc, x.color, a.partnumber, a.itemid, isnull(a.qty, 0) qty" & vbcrlf & _
		"	,	lower(v.email) email, v.phone, isnull(c.itemdesc, 'N/A') itemdesc, isnull(c.inv_qty, 0) inv_qty" & vbcrlf & _
		"	,	convert(varchar(10), a.processDate, 20) processDate, a.childOrderID, b2.parentOrderID" & vbcrlf & _
		"	, 	isnull(convert(varchar(2000), n.ordernotes), '') ordernotes, isnull(convert(varchar(2000), n2.notes), '') productnotes, convert(varchar(10), a.cancelDate, 20) cancelDate" & vbcrlf & _
		"	,	isnull(isnull(nullif(c.vendor, ''), vn.vendor), '') vendor, isnull(vn.partNumber, '') vn_partnumber" & vbcrlf & _
		"from	we_rma a join we_orders b" & vbcrlf & _
		"	on	a.orderid = b.orderid left outer join we_orders b2" & vbcrlf & _
		"	on	a.orderid = b2.parentOrderID join v_accounts v" & vbcrlf & _
		"	on	b.accountid = v.accountid and b.store = v.site_id join xstore x" & vbcrlf & _
		"	on	b.store = x.site_id left outer join we_items c" & vbcrlf & _
		"	on	a.partnumber = c.partnumber and c.master = 1 left outer join we_adminusers s" & vbcrlf & _
		"	on	a.adminid = s.adminid left outer join we_ordernotes n" & vbcrlf & _
		"	on	a.orderid = n.orderid left outer join we_partNumberNotes n2" & vbcrlf & _
		"	on	a.partnumber = n2.partnumber left outer join we_vendorNumbers vn" & vbcrlf & _
		"	on	a.partnumber = vn.we_partnumber" & vbcrlf & _
		sqlWhere & vbcrlf & _
		sqlOrder

sql	=	"select	distinct a.orderid, convert(varchar(16), a.entrydate, 20) entryDate, isnull(s.fname + ' ' + s.lname, 'N/A') username, v.accountid, convert(varchar(16), b.scandate, 20) scandate" & vbcrlf & _
		"	, 	x.shortDesc, x.color, a.partnumber, a.itemid, isnull(a.qty, 0) qty, c.ghost, c.hidelive" & vbcrlf & _
		"	,	lower(v.email) email, v.phone, isnull(c.itemdesc, 'N/A') itemdesc, isnull(c.inv_qty, 0) inv_qty" & vbcrlf & _
		"	,	convert(varchar(10), a.processDate, 20) processDate, a.childOrderID, b2.parentOrderID" & vbcrlf & _
		"	, 	isnull(convert(varchar(2000), n.ordernotes), '') ordernotes, isnull(convert(varchar(2000), n2.notes), '') productnotes, convert(varchar(10), a.cancelDate, 20) cancelDate" & vbcrlf & _
		"	,	isnull(substring(	(	select	(', ' + i.vendor)" & vbcrlf & _
		"								from	we_vendorNumbers i" & vbcrlf & _
		"								where	i.we_partnumber = a.partnumber" & vbcrlf & _
		"								for xml path('')" & vbcrlf & _
		"							), 3, 1000), c.vendor) vendor" & vbcrlf & _
		"	,	isnull(substring(	(	select	(', ' + i.partnumber)" & vbcrlf & _
		"								from	we_vendorNumbers i" & vbcrlf & _
		"								where	i.we_partnumber = a.partnumber" & vbcrlf & _
		"								for xml path('')" & vbcrlf & _
		"							), 3, 1000), '') vn_partnumber" & vbcrlf & _
		"	,	(	select	top 1 processOrderDate" & vbcrlf & _
		"			from	we_vendorOrder with (nolock)" & vbcrlf & _
		"			where	partnumber = a.partnumber" & vbcrlf & _
		"				and	receivedAmt > 0" & vbcrlf & _
		"			order by processOrderDate desc	" & vbcrlf & _
		"		) vn_processOrderDate" & vbcrlf & _
		"	,	p.alwaysinstock" & vbcrlf & _
		"from	we_rma a join we_orders b" & vbcrlf & _
		"	on	a.orderid = b.orderid left outer join we_orders b2" & vbcrlf & _
		"	on	a.orderid = b2.parentOrderID join v_accounts v" & vbcrlf & _
		"	on	b.accountid = v.accountid and b.store = v.site_id join xstore x" & vbcrlf & _
		"	on	b.store = x.site_id left outer join we_items c" & vbcrlf & _
		"	on	a.partnumber = c.partnumber and c.master = 1 left outer join we_adminusers s" & vbcrlf & _
		"	on	a.adminid = s.adminid left outer join we_ordernotes n" & vbcrlf & _
		"	on	a.orderid = n.orderid left outer join we_partNumberNotes n2" & vbcrlf & _
		"	on	a.partnumber = n2.partnumber left outer join we_pndetails p" & vbcrlf & _
		"	on	a.partnumber = p.partnumber" & vbcrlf & _
		sqlWhere & vbcrlf & _
		sqlOrder
		
session("errorSQL") = sql
'response.write "<pre>" & sql & "</pre>"
set rs = oConn.execute(sql)		
%>
<table width="1200" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td width="100%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" align="left" valign="top" style="padding-top:20px;">
                    	<span style="font-size:18px; font-weight:bold;">Back-Order Fulfillment</span>
                    </td>
				</tr>
				<tr>
					<td width="100%" valign="top">
						<form name="frmRMA" method="post">
						<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
                        	<tr>
                            	<td colspan="99" style="padding-bottom:10px;">
                                	<table width="100%" border="1" cellspacing="0" cellpadding="3" align="center" bordercolor="#FFFFFF" style="border-collapse:collapse;" bgcolor="#EAEAEA">
							            <tr>
                                            <td class="normalText" align="center" style="color:#E95516;"><b>FILTER</b></td>
                                            <td class="normalText" align="center"><b>Back-Order DATE</b></td>
                                            <td class="normalText" align="center"><b>Order ID</b></td>
                                            <td class="normalText" align="center"><b>View</b></td>
                                            <td class="normalText" align="center"><b>Action</b></td>
                                        </tr>
                                        <tr>
                                            <td class="normalText" align="center" style="color:#E95516;"><b>VALUE</b></td>
                                            <td class="normalText" align="center">
												<input type="text" id="id_sdate" name="txtSDate" value="<%=strSDate%>" size="7">
                                                ~
                                                <input type="text" id="id_edate" name="txtEDate" value="<%=strEDate%>" size="7">
                                            </td>
                                            <td class="normalText" align="center">
                                            	<input type="text" name="txtOrderID" value="<%=strOrderID%>" />
                                            </td>
                                            <td class="normalText" align="center">
                                            	Status:
                                            	<select name="cbView">
                                                	<option value="1" <%if strView = "1" then %> selected <% end if %>>All</option>
                                                	<option value="2" <%if strView = "2" then %> selected <% end if %>>Processed</option>
                                                	<option value="3" <%if strView = "3" then %> selected <% end if %>>Pending</option>
                                                	<option value="4" <%if strView = "4" then %> selected <% end if %>>Cancelled</option>
                                                </select>
                                                <br />
                                            	QTY on Hand:
                                            	<select name="cbView2">
                                                	<option value="0" <%if strView2 = 0 then %> selected <% end if %>>YES</option>
                                                	<option value="1" <%if strView2 = 1 then %> selected <% end if %>>ALL</option>
                                                </select>
                                            </td>
                                            <td class="normalText" align="left" style="padding-left:50px;">
	                                            <input type="submit" value="SEARCH" name="search" onclick="return doSubmit('S', '');"><br />
	                                            <input type="submit" value="PROCESS ORDER" name="processBackOrder" onclick="return doSubmit('P', document.frmRMA.chkProcess);"><br />
                                                <input type="submit" value="CANCEL" name="cancelBackOrder" onclick="return doSubmit('C', document.frmRMA.chkCancel);">&nbsp;&nbsp; *Cancel Reason: <input type="text" name="txtCancelReason" value="" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                            	<td colspan="99">
                                	<div class="right" style="cursor:pointer;" onclick="ExportToExcel();">Export to excel <img src="/images/excel_icon.gif" border="0" /></div>
									<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center" class="sortable">
										<thead>
                                        <tr>
                                            <th class="normalText" align="center" style="font-weight:bold; border-bottom:1px solid #E95516; border-top:1px solid #E95516;" width="30">SITE</th>
                                            <th class="normalText" align="center" style="font-weight:bold; border-bottom:1px solid #E95516; border-top:1px solid #E95516;" width="80">Order ID</th>
                                            <th class="normalText" align="center" style="font-weight:bold; border-bottom:1px solid #E95516; border-top:1px solid #E95516;" width="100">Original<br />Scan Date</th>
                                            <th class="normalText" align="center" style="font-weight:bold; border-bottom:1px solid #E95516; border-top:1px solid #E95516;" width="120">Back-Order<br />Entry Date</th>
                                            <th class="normalText" align="center" style="font-weight:bold; border-bottom:1px solid #E95516; border-top:1px solid #E95516;" width="130">Part Number</th>
                                            <th class="normalText" align="center" style="font-weight:bold; border-bottom:1px solid #E95516; border-top:1px solid #E95516;" width="130">Vendors<br />Vendor PN</th>
                                            <th class="normalText" align="center" style="font-weight:bold; border-bottom:1px solid #E95516; border-top:1px solid #E95516;" width="60">Item ID</th>
                                            <th class="normalText" align="center" style="font-weight:bold; border-bottom:1px solid #E95516; border-top:1px solid #E95516;" width="40">Order<br />QTY</th>
                                            <th class="normalText" align="center" style="font-weight:bold; border-bottom:1px solid #E95516; border-top:1px solid #E95516;" width="240">Item Desc</th>
                                            <th class="normalText" align="center" style="font-weight:bold; border-bottom:1px solid #E95516; border-top:1px solid #E95516;" width="50">QTY on Hand</th>
                                            <th class="normalText" align="center" style="font-weight:bold; border-bottom:1px solid #E95516; border-top:1px solid #E95516;" width="80">Process<br />
                                                <input value="" type="checkbox" name="chkMasterProcess" onclick="setAllCheckbox(document.frmRMA.chkProcess, document.frmRMA.chkMasterProcess.checked);"/>
                                            </th>
                                            <td class="normalText" align="center" style="font-weight:bold; border-bottom:1px solid #E95516; border-top:1px solid #E95516;" width="80">Cancel<br />
                                                <input value="" type="checkbox" name="chkMasterCancel" onclick="setAllCheckbox(document.frmRMA.chkCancel, document.frmRMA.chkMasterCancel.checked);"/>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <%
                                        if not rs.eof then
                                            nRow = 0
                                            do until rs.eof
                                                nRow = nRow + 1
                                                orderid = rs("orderid")
                                                entryDate = rs("entryDate")
                                                username = rs("username")
                                                accountid = rs("accountid")
                                                scandate = rs("scandate")
                                                siteName = rs("shortDesc")
                                                siteColor = rs("color")
                                                partnumber = rs("partnumber")
                                                itemid = rs("itemid")
                                                orderQty = rs("qty")
												ghost = rs("ghost")
												hidelive = rs("hidelive")
                                                itemdesc = rs("itemdesc")
                                                inv_qty = rs("inv_qty")
                                                processDate = rs("processDate")
                                                parentOrderID = rs("parentOrderID")
                                                ordernotes = rs("ordernotes")
                                                productnotes = rs("productnotes")
                                                cancelDate = rs("cancelDate")
                                                vendor = rs("vendor")
                                                vn_partnumber = rs("vn_partnumber")
												vn_processOrderDate = rs("vn_processOrderDate")
												alwaysInStock = rs("alwaysInStock")
                                        %>
                                        <tr class="grid_row_<%=(nRow mod 2)%>" onMouseOver="this.className='mouseover'" onMouseOut="this.className='grid_row_<%=(nRow mod 2)%>'" style="<%if ghost or hidelive then%>background:#c1e0b2;<%end if%>">
                                            <td class="smlText" align="center" style="color:<%=siteColor%>; font-size:13px; font-weight:bold;"><%=siteName%></td>
                                            <td class="smlText" align="center">
                                                <a href="#" onClick="printinvoice('<%=orderid%>','<%=accountid%>');"><%=orderid%></a>&nbsp;
                                                <span title="<%if ordernotes = "" then response.write("No notes entered") else response.write(Left(Replace(ordernotes,"""",""),100)) end if%>"><a href="#" onClick="fnopennotes('<%=orderid%>'); return false;"><img src='/images/Notes<%if ordernotes = "" then response.write "_none"%>.gif' border=0></a></span>
                                            </td>
                                            <td class="smlText" align="center"><%=scandate%></td>
                                            <td class="smlText" align="center" width="120"><%=entryDate%><br />By <%=username%></td>
                                            <td class="smlText" align="center">
                                                <%=partnumber%>&nbsp;
                                                <span title="<%if productnotes = "" then response.write("No notes entered") else response.write(Left(Replace(productnotes,"""",""),100)) end if%>"><a href="/admin/productNotes.asp?partNumber=<%=partnumber%>" target="_blank"><img src='/images/Notes<%if productnotes = "" then response.write "_none"%>.gif' border=0></a></span>
                                                <br />
												<%if alwaysInStock then%>
                                                <span style="color:blue;">Always In Stock</span>
                                                <%end if%>
                                            </td>
                                            <td class="smlText" align="center">
                                                <span style="font-weight:bold; color:blue;"><%=vendor%></span><br />
                                                <%=vn_partnumber%>
                                            </td>
                                            <td class="smlText" align="center"><%=itemid%></td>
                                            <td class="smlText" align="center" style="color:#FF0000; font-weight:bold;"><%=formatnumber(orderQty, 0)%></td>
                                            <td class="smlText" align="left"><%=itemdesc%></td>
                                            <td class="smlText" align="center" style="font-weight:bold; font-size:14px;">
                                                <%
                                                if inv_qty > 0 then 
													if isnull(vn_processOrderDate) then
                                                    %>
                                                        <font color="#008000"><%=formatnumber(inv_qty, 0)%></font>
                                                    <%
                                                    else
                                                        if datediff("d", cdate(vn_processOrderDate), date) < 6 then 'supposed to be 3 but because of weekends
                                                        %>
                                                        <font color="blue"><%=formatnumber(inv_qty, 0)%></font>
                                                        <%
                                                        else
                                                        %>
                                                        <font color="#008000"><%=formatnumber(inv_qty, 0)%></font>
                                                        <%
                                                        end if
                                                    end if
                                                else
                                                %>
                                                    <font color="#008000"> - </font>
                                                <% 
                                                end if
                                                %>
                                                <%
												if ghost or hidelive then
												%>
                                                <div style="color:#F00; font-size:12px;">Hidden Product</div>
                                                <%
												end if
												%>
                                            </td>
                                            <td class="smlText" align="center">
                                                <% if inv_qty > 0 and isnull(processDate) and isnull(cancelDate) then %>
                                                <input type="checkbox" name="chkProcess" value="<%=orderid%>__<%=partnumber%>__<%=orderQty%>" onclick="onSetMasterCheckBox(document.frmRMA, 'P');" <% if isnull(parentOrderID) then %> checked <% end if %>/>
                                                <% else %>
                                                    <% if not isnull(processDate) then %>
                                                        Processed on <br /><span style="color:blue;"><%=processDate%></span>
                                                    <% end if %>
                                                <% end if %>                                    
                                            </td>
                                            <td class="smlText" align="center">
                                                <% if not isnull(cancelDate) then %>
                                                        Cancelled on <br /><span style="color:red;"><%=cancelDate%></span>
                                                <% else %>
                                                    <input type="checkbox" name="chkCancel" value="<%=orderid%>__<%=partnumber%>" onclick="onSetMasterCheckBox(document.frmRMA, 'C');" />                                    
                                                <% end if %>
                                            </td>
                                        </tr>
                                        <%
                                                if (nRow mod 1000) = 0 then response.flush
                                                rs.movenext
                                            loop
                                        else
                                        %>
                                        <tr><td colspan="99" class="smlText" align="center">No data to display</td></tr>
                                        <%
                                        end if
                                        %>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
						</table>
                        <input type="hidden" name="hidChkProcess" value="" />
                        <input type="hidden" name="hidChkCancel" value="" />
                        <input type="hidden" name="hidUpdateType" value="" />
                        <input type="hidden" name="hidExcel" value="" />
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script type="text/javascript" src="/includes/admin/checkbox.js"></script>
<script>
function doSubmit(type, objChk) {
	document.frmRMA.hidExcel.value = "";
	if ( type == "S" )
	{
		document.frmRMA.action = 'db_search_rma.asp';
	} else
	{
		strChecked = "";
		if ("undefined" != typeof(objChk.length))
		{
			for (i=0; i<objChk.length; i++)	
			{
				if (objChk[i].checked)
				{
					if (strChecked == "")
						strChecked = objChk[i].value;
					else
						strChecked = strChecked + "," + objChk[i].value;					
				}
			}
		}
		else
		{
			strChecked = objChk.value;
		}
	
		if (strChecked == "")
		{
			alert("Please select orders");
			return false;
		}
		
		document.frmRMA.hidUpdateType.value = type;
		document.frmRMA.hidChkProcess.value = strChecked;
		document.frmRMA.hidChkCancel.value = strChecked;
		document.frmRMA.action = 'db_update_rma.asp';		
		var strPrompt = "Warning: Do you wish to proceed?";
		if (!confirm(strPrompt)) return false;		
	}

	return true;
}

function onSetMasterCheckBox(frm,type) {
	if (type == "P")
	{
		if (isAllChecked(frm.chkProcess))			frm.chkMasterProcess.checked = true;
		else if (isAllUnChecked(frm.chkProcess))	frm.chkMasterProcess.checked = false;		
	} else if (type == "C")
	{
		if (isAllChecked(frm.chkCancel))			frm.chkMasterCancel.checked = true;
		else if (isAllUnChecked(frm.chkCancel))	frm.chkMasterCancel.checked = false;
	}
}

function printinvoice(orderid,accountid) {
	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}

function fnopennotes(orderid) {
	var url='ordernotes.asp?orderid='+orderid;
	window.open(url,'ordernote','left=0,top=0,width=800,height=300,resizable=1,locationbar=0,menubar=0,toolbar=0,scrollbars=1');
}

function ExportToExcel() {
	document.frmRMA.hidExcel.value = "Y";
	document.frmRMA.submit();
}
</script>
<%
function getCommaString(val)
	if "" = trim(val) then getCommaString = "" end if
	
	dim arr : arr = split(val, ",")
	dim str : str = ""

	for i=0 to ubound(arr)
		if "" <> trim(arr(i)) then str = str & trim(arr(i)) & "," end if
	next
	
	if "," = right(str, 1) then str = left(str, len(str) - 1) end if 
	
	getCommaString = str
end function 
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
