<%
	thisUser = Request.Cookies("username")
	pageTitle = "Mobile Line Price Adjustment"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	sql = "select price_our, itemID, itemDesc, price_retail, cogs, partNumber, inv_qty from we_items where partNumber like '%-MLD-%' and master = 1 and hideLive = 0 and ghost = 0 order by price_our"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	sql = "select brandID, brandName from we_brands order by brandName"
	session("errorSQL") = sql
	set brandRS = oConn.execute(sql)
	
	sql = "select typeID, typeName from we_types order by typeName"
	session("errorSQL") = sql
	set catRS = oConn.execute(sql)
	
	brandArray = ""
	do while not brandRS.EOF
		brandArray = brandArray & brandRS("brandID") & "#" & brandRS("brandName") & "$"
		brandRS.movenext
	loop
	brandArray = split(brandArray,"$")
%>
<table align="center" border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="720">
	<tr>
    	<td align="center">
        	<div style="width:400px; text-align:left;">
            	<strong>Mobile Line Price Adjustment</strong> is an app designed to quickly update the prices of Mobile Line products 
                on the Wireless Emporium website. Clicking the view link will expand out and display all products in each price bracket. 
                You can update the products individually or as a bracket.
            </div>
        </td>
    </tr>
    <tr>
    	<td style="border-top:1px solid #999; border-bottom:1px solid #999; font-weight:bold; font-size:16px; background-color:#000; color:#FFF; padding-left:10px;">Select Product By Brand/Model</td>
    </tr>
    <tr>
    	<td id="brandBox" style="padding-top:5px;">
        	<div style="width:600px;">
	        	<div style="float:left; width:140px;">Select Brand:</div>
    	        <div style="float:left; padding-left:10px; width:450px;">
	    	    	<select name="brand" onchange="brandSelect(this.value)">
    	    	    	<option value="">Select Brand</option>
        	    	    <%
						for i = 0 to (ubound(brandArray) - 1)
							useArray = split(brandArray(i),"#")
						%>
		                <option value="<%=useArray(0)%>"><%=useArray(1)%></option>
    		            <%
						next
						%>
		            </select>
    	        </div>
            </div>
        </td>
    </tr>
    <tr>
        <td id="modelBox" style="padding-top:5px;"></td>
    </tr>
    <tr>
        <td id="catBox" style="padding-top:5px;"></td>
    </tr>
    <tr>
    	<td style="border-top:1px solid #999; border-bottom:1px solid #999; font-weight:bold; font-size:16px; background-color:#000; color:#FFF; padding-left:10px;">Other Search Methods</td>
    </tr>
    <tr>
    	<td id="brandBox" style="padding-top:5px;">
        	<form style="margin:0px; padding:0px;" action="#" method="post" name="invForm" onsubmit="return(testForm(this))">
        	<!--
            <div style="width:600px; float:left; text-align:right;">
	        	<div style="float:left; width:300px;"><input type="button" name="myAction" value="Pull New Products" onclick="newProds()" style="font-size:12px;" /></div>
            </div>
            -->
            <div style="width:600px; float:left; padding-top:5px;">
	        	<div style="float:left; width:140px;">ItemID:</div>
    	        <div style="float:left; padding-left:10px; width:100px;">
	    	    	<input type="text" name="itemID" value="" />
    	        </div>
            </div>
            <div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">Part Number:</div>
    	        <div style="float:left; padding-left:10px; width:100px;">
	    	    	<input type="text" name="searchPartNumber" value="" />
    	        </div>
            </div>
            <div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">Category:</div>
    	        <div style="float:left; padding-left:10px; width:300px;">
	    	    	<select name="catSearch">
                    	<option value=""></option>
						<%
						do while not catRS.EOF
						%>
                        <option value="<%=catRS("typeID")%>"><%=catRS("typeName")%></option>
                        <%
							catRS.movenext
						loop
						%>
                    </select>
    	        </div>
            </div>
            <div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">Keyword:</div>
    	        <div style="float:left; padding-left:10px; width:155px;">
	    	    	<input type="text" name="keyword" value="" />
    	        </div>
                <div style="float:left; width:140px;"><input type="submit" name="searchBttn" value="Search" style="font-size:12px;" /></div>
            </div>
            </form>
        </td>
    </tr>
    <tr>
    	<td align="right" style="border-top:1px solid #999; border-bottom:1px solid #999; font-weight:bold; font-size:16px; background-color:#000;"><input type="button" name="clearForm" value="Clear Form" onclick="window.location='/admin/mobileLinePriceAdjust.asp'" style="font-size:12px;" /></td>
    </tr>
    <tr>
    	<td id="resultBox">
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<%
                priceBracket = 0
                productNum = 0
                do while not rs.EOF
                    priceBracket = priceBracket + 1
                    priceOur = rs("price_our")
                %>
                <tr bgcolor="#333333" style="color:#FFF; font-weight:bold;">
                    <td>
                        <form name="bracketForm_<%=priceBracket%>" onsubmit="return(false)" style="margin:0px; padding:0px;">
                        <div style="display:block; width:100%">
                            <div style="float:left;">Mobile Line Products That Cost <%=priceOur%> <a onclick="showBracket(<%=priceBracket%>)" style="color:#ccccff; font-size:16px; cursor:pointer;">(view <span id="priceBracket_<%=priceBracket%>"></span> products)</a></div>
                            <div style="float:right;">
                                <input type="button" name="myAction" value="Update Bracket" onclick="updateBracketPrice(document.bracketForm_<%=priceBracket%>.newPrice.value,document.bracketForm_<%=priceBracket%>.basePrice.value)" />
                                <input type="hidden" name="basePrice" value="<%=priceOur%>" />
                            </div>
                            <div style="float:right; padding:1px 10px 0px 0px;"><input type="text" name="newPrice" value="<%=priceOur%>" size="5" /></div>
                        </div>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td id="productList_<%=priceBracket%>" style="display:none;">
                        <div style="display:block; background-color:#006; height:30px; color:#FFF">
                            <div style="float:left; width:340px; padding:5px 0px 0px 5px;">Product Name</div>
                            <div style="float:left; width:50px; text-align:left; padding:3px 0px 0px 10px;">COGS</div>
                            <div style="float:left; width:60px; text-align:left; padding:3px 0px 0px 3px;">MSRP</div>
                            <div style="float:left; width:70px; text-align:left; padding:3px 0px 0px 3px;">Price</div>
                            <div style="float:left; width:140px; text-align:left; padding-top:3px;">&nbsp;</div>
                        </div>
                        <%
                        bgColor = "#fff"
                        prodAtPrice = 0
                        do while priceOur = rs("price_our")
                            productNum = productNum + 1
                            prodAtPrice = prodAtPrice + 1
                            itemID = rs("itemID")
                            itemDesc = rs("itemDesc")
                            price_retail = rs("price_retail")
                            Cogs = rs("cogs")
                            partNumber = rs("partNumber")
                        %>
                        <form name="productForm_<%=productNum%>" onsubmit="return(false)" style="margin:0px; padding:0px;">
                        <div style="display:block; background-color:<%=bgColor%>; height:30px;">
                            <div style="float:left; font-size:10px; width:340px; padding:5px 0px 0px 5px;"><a href="/product.asp?itemID=<%=itemID%>" target="viewProduct"><%=left(itemDesc,60)%></a></div>
                            <div style="float:left; width:50px; text-align:right; padding-top:3px;"><%=formatCurrency(Cogs,2)%></div>
                            <div style="float:left; width:60px; text-align:right; padding-top:3px;"><%=formatCurrency(price_retail,2)%></div>
                            <div style="float:left; width:70px; text-align:right; padding-top:3px;"><input type="text" name="newPrice" value="<%=priceOur%>" size="5" /></div>
                            <div style="float:left; width:150px; text-align:right; padding-top:3px;"><input type="button" name="myAction" value="Update Product" onclick="updateIndvPrice(document.productForm_<%=productNum%>.newPrice.value,'<%=partNumber%>')" /></div>
                        </div>
                        </form>
                        <%
                            rs.movenext
                            if bgColor = "#fff" then bgColor = "#ccc" else bgColor = "#fff"
                            if rs.EOF then exit do
                        loop
                        %>
                        <div id="prodCnt_<%=priceBracket%>" style="display:none;"><%=prodAtPrice%></div>
                    </td>
                </tr>
                <%
                loop
                %>
            </table>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	<% for i = 1 to priceBracket %>
	document.getElementById("priceBracket_<%=i%>").innerHTML = document.getElementById("prodCnt_<%=i%>").innerHTML
	<% next %>
	
	function brandSelect(brandID) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Select Model To Continue Search Process</td></tr></table>"
		ajax('/ajax/admin/ajaxInventoryAdjust.asp?brandID=' + brandID,'modelBox')
		document.getElementById("catBox").innerHTML = ""
	}
	function modelSelect(modelID) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Select Category To Continue Search Process</td></tr></table>"
		ajax('/ajax/admin/ajaxInventoryAdjust.asp?modelID=' + modelID,'catBox')
	}
	function catSelect(catID,modelID) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxMobileLinePriceAdjust.asp?modelID=' + modelID + '&catID=' + catID,'resultBox')
	}
	
	function showBracket(bracketNum) {
		if (document.getElementById("productList_" + bracketNum).style.display == '') {
			document.getElementById("productList_" + bracketNum).style.display = 'none'
		}
		else {
			document.getElementById("productList_" + bracketNum).style.display = ''
		}
	}
	
	function updateIndvPrice(newPrice,partNumber) {
		ajax('/ajax/admin/ajaxMobileLinePriceAdjust.asp?newPrice=' + newPrice + '&partNumber=' + partNumber,'dumpZone')
		alert("update complete!")
	}
	
	function updateBracketPrice(newPrice,basePrice) {
		ajax('/ajax/admin/ajaxMobileLinePriceAdjust.asp?newPrice=' + newPrice + '&basePrice=' + basePrice,'dumpZone')
		alert("update complete!")
		window.location = window.location
	}
	
	function testForm(curForm) {
		var itemID = curForm.itemID.value
		var searchPartNumber = curForm.searchPartNumber.value
		var category = curForm.catSearch.value
		var keyword = curForm.keyword.value
		
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		if (itemID != "") { ajax('/ajax/admin/ajaxMobileLinePriceAdjust.asp?itemID=' + itemID,'resultBox') }
		else if (searchPartNumber != "") { ajax('/ajax/admin/ajaxMobileLinePriceAdjust.asp?searchPartNumber=' + searchPartNumber,'resultBox') }
		else if (category != "") { ajax('/ajax/admin/ajaxMobileLinePriceAdjust.asp?category=' + category,'resultBox') }
		else if (keyword != "") { ajax('/ajax/admin/ajaxMobileLinePriceAdjust.asp?keyword=' + keyword,'resultBox') }
		else {
			document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Error Performing Search...<br>Please Check Your Search Criteria</td></tr></table>"
		}
		return false
	}
</script>