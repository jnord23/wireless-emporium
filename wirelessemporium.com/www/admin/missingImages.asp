<%
	thisUser = Request.Cookies("username")
	pageTitle = ""
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/framework/utility/ftp.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
	function sendNewProdEmail(itemID)
		sql =	"select a.itemDesc, a.itemDesc_CO, a.partNumber, a.price_our, a.price_CO, b.typeID, b.inv_qty, a.itemPic, a.itemPic_CO, c.brandName, d.modelName, e.orders, f.id, b.partnumber " &_
				"from we_Items a " &_
					"join we_Items b on a.partnumber = b.partnumber and b.master = 1 " &_
					"join we_brands c on a.brandID = c.brandID " &_
					"join we_models d on a.modelID = d.modelID " &_
					"outer apply (" &_
						"select count(*) as orders from we_invRecord where itemID = b.itemID and notes like '%Customer Order%'" &_
					") e " &_
					"left join newProducts f on a.partnumber = f.partnumber " &_
				"where a.itemID = " & itemID
		set pnRS = oConn.execute(sql)
		
		if not pnRS.EOF then
			useDesc = pnRS("itemDesc")
			useDescCO = pnRS("itemDesc_CO")
			usePN = pnRS("partNumber")
			usePriceWE = pnRS("price_our")
			usePriceCO = pnRS("price_CO")
			if prepInt(pnRS("inv_qty")) > 0 then
				if prepStr(pnRS("itemPic")) <> "" or prepStr(pnRS("itemPic_CO")) <> "" then
					if prepInt(pnRS("orders")) = 0 then
						if prepInt(pnRS("id")) = 0 then
							brandName = prepStr(pnRS("brandName"))
							modelName = prepStr(pnRS("modelName"))
							'send email to marketing team
							cdo_from = "Purchasing <wepurchasing@wirelessemporium.com>"
							cdo_subject = brandName & " " & modelName & " phone is ready for a marketing campaign to be created!"
							cdo_body =	"Phone: " & brandName & " " & modelName & "<br>" &_
										"Product: " & useDesc & "<br>" &_
										"PartNumber: " & usePN & "<br>" &_
										"WE Price: " & usePriceWE & "<br>" &_
										"CO Price: " & usePriceCO & "<br>" &_
										"WE Link: <a href='http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(useDesc) & "'>ProductLink</a><br>" &_
										"CO Link: <a href='http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(useDescCO) & ".html'>ProductLink</a><br>"
							cdo_to = "wemarketing@wirelessemporium.com;wepurchasing@wirelessemporium.com"
							CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
							
							sql = "insert into newProducts (partnumber) values('" & pnRS("partnumber") & "')"
							oConn.execute(sql)
							
							response.Write("Marketing Msg: message sent to marketing!<br>")
						else
							response.Write("Marketing Msg: Already sent new product message<br>")
						end if
					else
						response.Write("Marketing Msg: This product has been sold already<br>")
					end if
				else
					response.Write("Marketing Msg: No pictures found<br>")
				end if
			else
				response.Write("Marketing Msg: No inventory<br>")
			end if
		else
			response.Write("Marketing Msg: pnRS is blank:" & sql & "<br>")
		end if
	end function
	
	useFTP = true
	if useFTP then
		call initConnection(ftpWEB2,"192.168.112.161","ftpAdmin","GSF79h7yr9r3$C")	'WEB2
		call initConnection(ftpWEB3,"192.168.112.163","ftpAdmin","GSF79h7yr9r3$C")	'WEB3
	end if

	set fso = CreateObject("Scripting.FileSystemObject")
	Set Jpeg = Server.CreateObject("Persits.Jpeg")
	Jpeg.Quality = 100
	Jpeg.Interpolation = 1

	set Upload = Server.CreateObject("Persits.Upload")
	Upload.IgnoreNoPost = true
	myCount = Upload.Save

	dim prefix : prefix = prepStr(request.QueryString("prefix"))
	dim vendor : vendor = prepStr(request.QueryString("vendor"))
	dim curBrandID : curBrandID = prepInt(request.QueryString("brand"))
	dim curModelID : curModelID = prepInt(request.QueryString("model"))
	dim inv : inv = prepInt(upload.Form("inv"))
	dim ven : ven = prepInt(upload.Form("ven"))
	dim sortBy : sortBy = prepStr(upload.Form("sortBy"))
	
	dim buildSessions : buildSessions = 0

	if prefix = "" and vendor = "" and curBrandID = 0 and curModelID = 0 then
		buildSessions = 1
		session("prefixList") = ""
		session("vendorList") = ""
		session("tempBrandList") = ""
		session("brandList") = ""
		session("tempModelList") = ""
		session("modelList") = ""
	elseif prepStr(session("prefixList")) = "" or prepStr(session("vendorList")) = "" or prepStr(session("tempBrandList")) = "" or prepStr(session("brandList")) = "" or prepStr(session("tempModelList")) = "" or prepStr(session("modelList")) = "" then
		response.Write("bounce")
		response.End()
		response.Redirect("/admin/missingImages.asp")
	end if
	
	if myCount < 1 then
		inv = prepInt(request.Form("inv"))
		ven = prepInt(request.Form("ven"))
		sortBy = prepStr(request.Form("sortBy"))
		dim createSort : createSort = "createDate"
		dim receivedSort : receivedSort = "receiveDate"
		dim cogsSort : cogsSort = "cogs"
		if sortBy = "createDate" then createSort = "createDateDesc"
		if sortBy = "receiveDate" then receivedSort = "receiveDateDesc"
		if sortBy = "cogs" then cogsSort = "cogsDesc"
	end if
	
	if inv = 0 then inv = 3
	if ven = 0 then ven = 1

	itemID = prepInt(upload.form("itemID"))

	if itemID > 0 then
		sql = "select itemDesc, itemDesc_co from we_Items where itemID = " & itemID
		session("errorSQL") = sql
		set rs = oConn.execute(sql)

		itemDesc = rs("itemDesc")
		itemDesc_co = rs("itemDesc_co")

		set wePrimPic = upload.files("wePrimPic")
		wePrimPic_fileName = wePrimPic.filename

		set coPrimPic = upload.files("coPrimPic")
		coPrimPic_fileName = coPrimPic.filename

		set altPic1 = upload.files("altPic1")
		if not altPic1 is nothing then altPic1_fileName = altPic1.filename
		set altPic2 = upload.files("altPic2")
		if not altPic2 is nothing then altPic2_fileName = altPic2.filename
		set altPic3 = upload.files("altPic3")
		if not altPic3 is nothing then altPic3_fileName = altPic3.filename

		path = server.mappath("\productpics") & "\"
		path_CO = server.mappath("\productpics_co") & "\"

		tempPath = server.mappath("\productpics") & "\tempImages\"
		tempCOPath = server.mappath("\productpics_co") & "\tempImages\"

		if not wePrimPic is nothing then
			newName = formatSEO(itemDesc) & ".jpg"
			weName = newName
			
			if instr(request.ServerVariables("SERVER_NAME"),"staging") < 1 then
				jpeg.OpenBinary(wePrimPic.Binary)
	
				if jpeg.Width = 800 and jpeg.Height = 800 then
					SavePath = tempPath & "big\zoom\" & newName
					jpeg.Width = 800 : jpeg.Height = 800 : jpeg.Save SavePath
					call uploadImagesRemote("\productpics\big\zoom", SavePath, newName)
				end if
	
				SavePath = tempPath & "big\" & newName
				jpeg.Width = 300
				jpeg.Height = 300
				jpeg.Save SavePath
				'response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics\big", SavePath, newName)
	
				SavePath = tempPath & "thumb\" & newName
				jpeg.Width = 100
				jpeg.Height = 100
				jpeg.Save SavePath
				'response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics\thumb", SavePath, newName)
	
				SavePath = tempPath & "homepage65\" & newName
				jpeg.Width = 65
				jpeg.Height = 65
				jpeg.Save SavePath
				'response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics\homepage65", SavePath, newName)
	
				SavePath = tempPath & "icon\" & newName
				jpeg.Width = 45
				jpeg.Height = 45
				jpeg.Save SavePath
				'response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics\icon", SavePath, newName)
			else
				response.Write("Staging - no files saved<br>")
			end if
			response.Write("WE Images Saved<br>")
		end if
		if not coPrimPic is nothing then
			newName = formatSEO(itemDesc_co) & ".jpg"
			coName = newName
			
			if instr(request.ServerVariables("SERVER_NAME"),"staging") < 1 then
				jpeg.OpenBinary(coPrimPic.Binary)
	
				if jpeg.Width = 800 and jpeg.Height = 800 then
					SavePath = tempCOPath & "big\zoom\" & newName
					jpeg.Width = 800 : jpeg.Height = 800 : jpeg.Save SavePath
					call uploadImagesRemote("\productpics_co\big\zoom", SavePath, newName)
				end if
	
				SavePath = tempCOPath & "big\" & newName
				jpeg.Width = 300
				jpeg.Height = 300
				jpeg.Save SavePath
				'response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics_co\big", SavePath, newName)
	
				SavePath = tempCOPath & "thumb\" & newName
				jpeg.Width = 100
				jpeg.Height = 100
				jpeg.Save SavePath
				'response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics_co\thumb", SavePath, newName)
	
				SavePath = tempCOPath & "homepage65\" & newName
				jpeg.Width = 65
				jpeg.Height = 65
				jpeg.Save SavePath
				'response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics_co\homepage65", SavePath, newName)
	
				SavePath = tempCOPath & "icon\" & newName
				jpeg.Width = 45
				jpeg.Height = 45
				jpeg.Save SavePath
				'response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics_co\icon", SavePath, newName)
			else
				response.Write("Staging - no files saved<br>")
			end if
			response.Write("CO Images Saved<br>")
		end if
		altImgCnt = 0
		for iCount = 1 to 3
			set fileWE = upload.files("altPic" & iCount)
			if not fileWE is nothing then
				altImgCnt = altImgCnt + 1
				
				if instr(request.ServerVariables("SERVER_NAME"),"staging") < 1 then
					jpeg.OpenBinary(fileWE.Binary)
	
					if jpeg.Width = 800 and jpeg.Height = 800 then
						SavePath = tempPath & "AltViews\zoom\" & formatSEO(itemDesc) & "-" & (iCount-1) & ".jpg"
						jpeg.Width = 800 : jpeg.Height = 800 : jpeg.Save SavePath
						call uploadImagesRemote("\productpics\AltViews\zoom", SavePath, formatSEO(itemDesc) & "-" & (iCount-1) & ".jpg")
					end if
	
					SavePath = tempPath & "AltViews\" & formatSEO(itemDesc) & "-" & (iCount-1) & ".jpg"
					jpeg.Width = 300
					jpeg.Height = 300
					jpeg.Save SavePath
					'response.write "Saved: " & SavePath & "<br>" & vbcrlf
					call uploadImagesRemote("\productpics\AltViews", SavePath, formatSEO(itemDesc) & "-" & (iCount-1) & ".jpg")
	
					SavePath = tempPath & "AltViews\" & formatSEO(itemDesc) & "-" & (iCount-1) & "_thumb.jpg"
					jpeg.Width = 40
					jpeg.Height = 40
					jpeg.Save SavePath
					'response.write "Saved: " & SavePath & "<br>" & vbcrlf
					call uploadImagesRemote("\productpics\AltViews", SavePath, formatSEO(itemDesc) & "-" & (iCount-1) & "_thumb.jpg")
				else
					response.Write("Staging - no files saved<br>")
				end if
			end if
		next
		if altImgCnt > 0 then response.Write("Alt Images Saved<br>")

		sql = "update we_Items set itemPic = '" & weName & "', itemPic_CO = '" & coName & "' where itemID = " & itemID
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sendNewProdEmail(itemID)

		sql = "delete from we_missingImages where itemID = " & itemID
		session("errorSQL") = sql
		oConn.execute(sql)

		sql = "select partnumber from we_Items where itemID = " & itemID
		session("errorSQL") = sql
		set pnRS = oConn.execute(sql)

		if not pnRS.EOF then
			sql = "update we_pnDetails set picAdminID = " & adminID & ", picEntryDate = '" & now & "' where partnumber = '" & pnRS("partnumber") & "'"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
	end if

	sql = "select top 1 lastItemID from we_missingImages"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)

	if rs.EOF then
		sql = "select top 5000 itemID, itemPic from we_Items where partNumber not like '%JON-NORD%' and partNumber not like '%XXX-XXXX%' order by itemID desc"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)

		lastItemID = rs("itemID")
		do while not rs.EOF
			curItemID = prepInt(rs("itemID"))
			curPic = prepStr(rs("itemPic"))
			if not fso.fileExists(server.MapPath("/productPics/big/" & curPic)) and curItemID > 0 then
				sql = "if (select count(*) from we_missingImages where itemID = " & curItemID & ") < 1 insert into we_missingImages (itemID,lastItemID) values (" & curItemID & "," & lastItemID & ")"
				session("errorSQL") = sql
				oConn.execute(sql)
			end if
			rs.movenext
		loop
	else
		lastItemID = rs("lastItemID")

		sql = "select top 5000 itemID, itemPic from we_Items where partNumber not like '%JON-NORD%' and partNumber not like '%XXX-XXXX%' and itemID > " & lastItemID & " order by itemID desc"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)

		if not rs.EOF then
			lastItemID = rs("itemID")
			do while not rs.EOF
				curItemID = rs("itemID")
				curPic = rs("itemPic")
				if not fso.fileExists(server.MapPath("/productPics/big/" & curPic)) then
					sql = "if (select count(*) from we_missingImages where itemID = " & curItemID & ") < 1 insert into we_missingImages (itemID,lastItemID) values (" & itemID & "," & lastItemID & ")"
					session("errorSQL") = sql
					oConn.execute(sql)
				end if
				rs.movenext
			loop

			sql = "update we_missingImages set lastItemID = " & lastItemID
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
	end if

	sql = "exec sp_missingImages '" & prefix & "','" & vendor & "'," & curBrandID & "," & curModelID & ",'" & sortBy & "'," & inv & "," & ven
	session("errorSQL") = sql
	set rs = oConn.execute(sql)

	path = Server.MapPath("tempCSV") & "\missingImgReport.csv"
	set file = fso.CreateTextFile(path)
	file.WriteLine "partnumber,itemDesc,created,received,cogs"
%>
<style type="text/css">
	.tableTitles	{ background-color:#000; color:#FFF; font-weight:bold; font-size:12px; cursor:pointer; }
	.pnColumn		{ width:115px; padding-left:5px; }
	.descColumn		{ width:680px; padding-left:5px; }
	.actColumn		{ width:115px; padding-right:5px; position:relative; }
	.itemRow		{ padding-top:5px; border-bottom:1px solid #666; }
	.dateColumn		{ width:75px; padding-left:5px; }
	
	.tb				{ display:table; }
	.fl				{ float:left; }
	.filterOps		{ margin-left:200px; }
	.bold			{ font-weight:bold; }
</style>
<div class="tb">
    <div class="fl">
        <table align="center" border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td style="padding:20px 0px 10px 5px;">
                    <div style="float:left; font-weight:bold; margin-right:10px; font-size:12px; width:150px; text-align:right;">Select Brand:</div>
                    <div style="float:left;" id="brandSelect"></div>
                </td>
            </tr>
            <tr>
                <td style="padding:5px 0px 10px 5px;">
                    <div style="float:left; font-weight:bold; margin-right:10px; font-size:12px; width:150px; text-align:right;">Select Model:</div>
                    <div style="float:left;" id="modelSelect"></div>
                </td>
            </tr>
            <tr>
                <td style="padding:5px 0px 10px 5px;">
                    <div style="float:left; font-weight:bold; margin-right:10px; font-size:12px; width:150px; text-align:right;">Select Product Type:</div>
                    <div style="float:left;" id="prefixSelect"></div>
                </td>
            </tr>
            <tr>
                <td style="padding:5px 0px 10px 5px;">
                    <div style="float:left; font-weight:bold; margin-right:10px; font-size:12px; width:150px; text-align:right;">Select Vendor:</div>
                    <div style="float:left;" id="vendorSelect"></div>
                </td>
            </tr>
            <tr>
                <td style="padding:5px 0px 10px 5px;">
                    <div style="float:left; font-weight:bold; margin-right:10px; font-size:12px; width:150px; text-align:right;">Clear Filter:</div>
                    <div style="float:left;"><input type="button" name="myAction" value="Reset" onclick="window.location='/admin/missingImages.asp'" /></div>
                </td>
            </tr>
        </table>
    </div>
    <div class="fl filterOps">
        <form name="filterForm" method="post">
        <table border="0" cellpadding="3" cellpadding="0">
            <tr>
                <td rowspan="4" valign="top" class="bold">Filter By:</td>
                <td align="right">Inventory:</td>
                <td>
                	<select name="inv" onchange="filterChg()">
                    	<option value="1">In Stock</option>
                        <option value="2"<% if inv = 2 then %> selected="selected"<% end if %>>Out of Stock</option>
                        <option value="3"<% if inv = 3 then %> selected="selected"<% end if %>>Both</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="right">Vendor Code:</td>
                <td>
                	<select name="ven" onchange="filterChg()">
                    	<option value="1">Has a vendor</option>
                        <option value="2"<% if ven = 2 then %> selected="selected"<% end if %>>Doesn't have a vendor</option>
                        <option value="3"<% if ven = 3 then %> selected="selected"<% end if %>>Both</option>
                    </select>
                    <input type="hidden" name="sortBy" value="" />
                </td>
            </tr>
        </table>
        </form>
    </div>
</div>
<table align="center" border="0" cellpadding="3" cellspacing="0">
    <tr><td id="totalItems" style="font-weight:bold; font-size:16px;" align="left"></td></tr>
    <tr><td><a href="/admin/tempCSV/missingImgReport.csv">Download CSV</a></td></tr>
    <tr><td><div id="modelListSelect" style="float:left;"></div></td></tr>
    <tr>
    	<td>
        	<div class="tb tableTitles">
            	<div class="fl pnColumn" onclick="sortBy('')">PartNumber</div>
                <div class="fl descColumn" onclick="sortBy('')">Item Description</div>
                <div class="fl dateColumn" onclick="sortBy('<%=createSort%>')">Created</div>
                <div class="fl dateColumn" onclick="sortBy('<%=receivedSort%>')">Received</div>
                <div class="fl dateColumn" onclick="sortBy('<%=cogsSort%>')">Cogs</div>
                <div class="fl actColumn" style="text-align:center;">Action</div>
            </div>
			<%
			bgColor = "#fff"
			itemCnt = 0
			inStock = 0
			modelOptions = ""
			createLists = 0
			do while not rs.EOF
				cogs = formatCurrency(prepInt(rs("cogs")))
				bornDate = prepStr(rs("entryDate"))
				receiveDate = prepStr(rs("completeOrderDate"))
				curVendorCode = rs("vendor")
				curItemID = rs("itemID")
				curItemDesc = rs("itemDesc")
				curItemDescLong = prepVal(rs("itemLongDetail"))
				curItemPic = rs("itemPic")
				curItemPic_CO = rs("itemPic_CO")
				curVendor = prepStr(rs("name"))
				wePartnumber = prepStr(rs("wePartnumber"))
				curPrefix = left(wePartnumber,3)
				curPartNumber = rs("partnumber")
				curInv = prepInt(rs("inv_qty"))
				modelID = prepInt(rs("modelID"))
				modelName = prepStr(rs("modelName"))
				brandID = prepInt(rs("brandID"))
				brandName = prepStr(rs("brandName"))

				if receiveDate = "" then
					receiveDate = "NA"
				else
					receiveDate = left(receiveDate,instr(receiveDate," ")-1)
				end if
				bornDate = left(bornDate,instr(bornDate," ")-1)

				if buildSessions = 1 then
					if instr(session("prefixList"),curPrefix) < 1 then
						createLists = 1
						session("prefixList") = session("prefixList") & "," & curPrefix
					end if
					if instr(session("vendorList"),curVendorCode) < 1 then
						createLists = 1
						session("vendorList") = session("vendorList") & "," & curVendorCode
					end if
					if instr(session("tempBrandList"),brandName) < 1 then
						createLists = 1
						session("tempBrandList") = session("tempBrandList") & "," & brandName
						session("brandList") = session("brandList") & "," & brandID
					end if
					if instr(session("tempModelList"),modelName) < 1 then
						createLists = 1
						session("tempModelList") = session("tempModelList") & "," & modelName
						session("modelList") = session("modelList") & "," & modelID
					end if
				end if

				if instr(modelOptions,modelName) < 1 then
					modelOptions = modelOptions & "<option value='" & modelID & "'>" & modelName & "</option>"
				end if

				curBGC = bgColor
				if curInv > 0 then
					bgColor = "#ff9999"
					inStock = inStock + 1
				end if
				if curVendor = "" then bgColor = "#94d0ff"
				if curItemDescLong = "" or curItemDescLong = "Enter Product Desc" then bgColor = "#FF0"
				itemCnt = itemCnt + 1
				file.WriteLine wePartnumber & "," & curItemDesc & "," & bornDate & "," & receiveDate & "," & cogs
			%>
        	<div id="fullRow_<%=curItemID%>" class="tb itemRow" style="background-color:<%=bgColor%>;">
            	<div class="fl pnColumn"><%=wePartnumber%></div>
                <div class="fl descColumn">
					<%=curItemDesc%><br />
                    <%=curVendor%> (<%=curPartNumber%>) | Item In Stock: <%=curInv%>&nbsp;&nbsp;(<a href="/admin/db_update_Items.asp?itemID=<%=curItemID%>" target="editProd">Edit Product</a>)
                </div>
                <div class="fl dateColumn"><%=bornDate%></div>
                <div class="fl dateColumn"><%=receiveDate%></div>
                <div class="fl dateColumn"><%=cogs%></div>
                <div class="fl actColumn">
                	<input type="button" name="weImage" value="Upload Images" onclick="showForm(<%=curItemID%>)" />
                    <div style="position:absolute; top:0px; left:-300px; z-index:999;" id="weImg_<%=curItemID%>"></div>
                </div>
            </div>
            <%
				bgColor = curBGC
				rs.movenext
				if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
			loop

			file.close()
			set file = nothing

			if createLists = 1 then
				session("prefixList") = mid(session("prefixList"),2)
				session("brandList") = mid(session("brandList"),2)
				session("modelList") = mid(session("modelList"),2)
			end if
			%>
        </td>
    </tr>
</table>
<form name="sortForm" method="post"><input type="hidden" name="sortBy" value="" /></form>
<%
sub uploadImagesRemote(remoteDir, localFilePath, remoteFileName)
	if useFTP then
		success = ftpWEB2.ChangeRemoteDir(remoteDir)
		success = ftpWEB2.PutFile(localFilePath, remoteFileName)

		success = ftpWEB3.ChangeRemoteDir(remoteDir)
		success = ftpWEB3.PutFile(localFilePath, remoteFileName)
	end if
end sub
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	document.getElementById("totalItems").innerHTML = "Total Items: <%=itemCnt%><br />In Stock: <%=inStock%>"
	ajax('/ajax/admin/ajaxMissingImages.asp?curVendor=<%=vendor%>&curPrefix=<%=prefix%>&curBrandID=<%=curBrandID%>&curModelID=<%=curModelID%>&prefixOptions=1','prefixSelect')
	ajax('/ajax/admin/ajaxMissingImages.asp?curVendor=<%=vendor%>&curPrefix=<%=prefix%>&curBrandID=<%=curBrandID%>&curModelID=<%=curModelID%>&vendorOptions=1','vendorSelect')
	ajax('/ajax/admin/ajaxMissingImages.asp?curVendor=<%=vendor%>&curPrefix=<%=prefix%>&curBrandID=<%=curBrandID%>&curModelID=<%=curModelID%>&brandOptions=1','brandSelect')
	ajax('/ajax/admin/ajaxMissingImages.asp?curVendor=<%=vendor%>&curPrefix=<%=prefix%>&curBrandID=<%=curBrandID%>&curModelID=<%=curModelID%>&modelOptions=1','modelSelect')
	//document.getElementById("modelListSelect").innerHTML = "<select name='modelID'><option value=''>All</option><%=modelOptions%></select>"

	var curFormID = 0
	var curBGcolor = ""

	function showForm(itemID) {
		if (curFormID != 0) {
			if (document.getElementById('weImg_' + curFormID).innerHTML != "") {
				alert("Please close the current upload window first")
				window.location = '#weImg_' + curFormID
			}
			else {
				curBGcolor = document.getElementById("fullRow_" + itemID).style.backgroundColor
				document.getElementById("fullRow_" + itemID).style.backgroundColor = "#FF0"
				curFormID = itemID
				ajax('/ajax/admin/ajaxMissingImages.asp?site=we&itemID=' + itemID + '&prefix=<%=prefix%>&vendor=<%=vendor%>&brand=<%=curBrandID%>&model=<%=curModelID%>&inv=<%=inv%>&ven=<%=ven%>&sortBy=<%=sortBy%>','weImg_' + itemID)
			}
		}
		else {
			curBGcolor = document.getElementById("fullRow_" + itemID).style.backgroundColor
			document.getElementById("fullRow_" + itemID).style.backgroundColor = "#FF0"
			curFormID = itemID
			ajax('/ajax/admin/ajaxMissingImages.asp?site=we&itemID=' + itemID + '&prefix=<%=prefix%>&vendor=<%=vendor%>&brand=<%=curBrandID%>&model=<%=curModelID%>&inv=<%=inv%>&ven=<%=ven%>&sortBy=<%=sortBy%>','weImg_' + itemID)
		}
	}

	function closeImgLoader(itemID) {
		document.getElementById('weImg_' + itemID).innerHTML = ''
		document.getElementById("fullRow_" + itemID).style.backgroundColor = curBGcolor
	}

	function sortBy(which) {
		document.filterForm.sortBy.value = which;
		document.filterForm.action = window.location;
		document.filterForm.submit();
	}
	
	function formSub(newLoc) {
		document.filterForm.action = newLoc;
		document.filterForm.submit();
	}
	
	function filterChg() {
		document.filterForm.action = window.location;
		document.filterForm.submit();
	}
</script>