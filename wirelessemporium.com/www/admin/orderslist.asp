<%
server.scripttimeout = 180
pageTitle = "Orders List"
header = 1
%>

<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include file="aspDatetime.asp"-->
<style>
.mouseover	{ background-color:#9BCEFF;}
</style>
<%
dim adminID
adminID = Request.Cookies("adminID")
'response.write request.form("totalOrders")
if request.form("totalOrders") <> "" then
	for a = 1 to cInt(request.form("totalOrders"))
		SQL = "SELECT orderID,PhoneOrder FROM we_orders WHERE orderID='" & request.form("orderID" & a) & "'"
		if request.form("PhoneOrder" & a) = "1" then
			SQL = SQL & " AND PhoneOrder='" & request.form("PhoneOrder" & a) & "'"
		else
			SQL = SQL & " AND PhoneOrder IS NULL"
		end if
		'response.write "<p>" & SQL & "</p>" & vbcrlf
		set RS = server.createobject("ADODB.recordset")
		RS.open SQL, oConn, 3, 3
		if RS.eof then
			if request.form("PhoneOrder" & a) = "1" then
				SQL = "UPDATE we_orders SET PhoneOrder='" & adminID & "' WHERE orderID='" & request.form("orderID" & a) & "'"
			else
				SQL = "UPDATE we_orders SET PhoneOrder=NULL WHERE orderID='" & request.form("orderID" & a) & "'"
			end if
			'response.write "<p>" & SQL & "</p>" & vbcrlf
			oConn.execute SQL
		end if
		
		set RS = nothing
	next
end if
%>

<script language="javascript">
function doSubmit()
{
	var	nValue		=	0;
	var elem 		= 	document.getElementById('id_frmSearch').elements;
	var	nOrderID	=	TrimStrings(document.frmSearch.txtorderid.value);
	var	nCustID		=	TrimStrings(document.frmSearch.txtaccountid.value);
	
	for(var i = 0; i < elem.length; i++)
	{
		if ((("text" == elem[i].type) || ("select-one" == elem[i].type)) && ("selpage" != elem[i].name))
		{
			if ("" != TrimStrings(elem[i].value)) nValue++;
		}
	} 
	
	if (0 == nValue) 
	{
		alert("please select at least one option.");
		return false;
	} else if (isNaN(nCustID))
	{
		alert("Customer # is not a number.");
		return false;			
	} else if (isNaN(nOrderID))
	{
		alert("Order ID is not a number.");
		return false;			
	}	

	return true;
}

function fngotopg(pgno) {
	document.frmSearch.reset();
	document.frmSearch.txtpageno.value = pgno;
	document.frmSearch.submit();
}
function fnopennotes(orderid) {
	var url='ordernotes.asp?orderid='+orderid;
	window.open(url,'ordernote','left=0,top=0,width=800,height=300,resizable=1,locationbar=0,menubar=0,toolbar=0,scrollbars=1');
}
function fncreditnotes(orderid) {
	var url='creditnotes.asp?orderid='+orderid;
	window.open(url,'ordernote','left=0,top=0,width=400,height=150,resizable=1,locationbar=0,menubar=0,toolbar=0');
}
function fnRMA(orderid,site_id) {
	var url = "";
	if (0 == site_id) url='RMA.asp?orderid='+orderid;
	else if (1 == site_id) url='RMA_CA.asp?orderid='+orderid;
	else if (2 == site_id) url='RMA_CO.asp?orderid='+orderid;	
	else if (3 == site_id) url='RMA_PS.asp?orderid='+orderid;		
	
	window.open(url,'ordernote','left=0,top=0,width=800,height=700,resizable=1,locationbar=1,menubar=1,scrollbars=1,toolbar=1');
}
function fnconfirm(orderid,emailid) {
	var url='xchange.asp?orderid='+orderid+'&emailid='+emailid;
	msg=confirm("Confirm Re-Shipment and Send Item Exchange/Re-Ship Email?")
	if (msg == true) {
		window.open(url,'orderxchange','left=0,top=0,width=400,height=150,resizable=1,locationbar=0,menubar=0,toolbar=0');
	}
}
function printinvoice(orderid,accountid) {
	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
function checkTracking(orderid) {
	var url="view_tracking.asp?orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=800,height=350,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
function resetAll() {
	for(i=0; i<document.frmSearch.elements.length; i++) {
		if (document.frmSearch.elements[i].type == "text") {
			document.frmSearch.elements[i].value = "";
		} else if (document.frmSearch.elements[i].type == "select-one") {
			document.frmSearch.elements[i].selectedIndex = 0;
		}
	}
}
function fnPhoneOrder() {
	document.frmSearch.submit();
}
function fnTracking(shiptype,trackingnum,newgisticsID) {
	if (shiptype == 1) {
		window.open("http://wwwapps.ups.com/WebTracking/OnlineTool?InquiryNumber1=" + trackingnum,"tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else if (shiptype == 2) {
		window.open("https://tools.usps.com/go/TrackConfirmAction!execute.action?formattedLabel=" + trackingnum,"tracking","left=0,top=0,width=1000,height=700,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else if (shiptype == 3) {
		window.open("http://webtrack.dhlglobalmail.com/?trackingnumber=" + trackingnum,"tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else if (shiptype == 4) {
		window.open("http://www.ups-mi.net/packageID/PackageID.aspx?PID=" + trackingnum,"tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else if (shiptype == 5) {
		window.open("https://tools.usps.com/go/TrackConfirmAction!execute.action?formattedLabel=" + trackingnum,"tracking","left=0,top=0,width=1000,height=700,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else if (shiptype == 6) {
		trackLink = "http://www.shipmentmanager.com/Portal.aspx?MerchantID=" + newgisticsID + "&TargetPageID=38&TrackingKey=BID&TrackingIdentifier=" + trackingnum
		window.open(trackLink,"tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else if (shiptype == 7) {
		trackLink = "http://www.ontrac.com/trackingres.asp?tracking_number=" + trackingnum
		window.open(trackLink,"tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	}
}
</script>

<%
dim strDate, strOrderId, strCustId, strFname, strLName, strConfirm, strApproved, strPhone, PgNo, strEmailAddr, strSiteID
strDate = fnRetDateFormat(Trim("" & Request.Form("txtorderdate")),"mm/dd/yyyy")
strLName = Trim("" & Request.Form("txtLName"))
strFname = Trim("" & Request.Form("txtFName"))
strCustId = Trim("" & Request.Form("txtAccountId"))
strPhone = Trim("" & Request.Form("txtPhone"))
strOrderId = Trim("" & Request.Form("txtOrderId"))
strConfirm = Trim("" & Request.Form("txtConfirm"))
strApproved = Trim("" & Request.Form("txtApproved"))
strEmailAddr = Trim("" & Request.Form("txtEmailAddr"))
strSiteID = Trim("" & Request.Form("hidsiteid"))
PgNo = Trim("" & Request.Form("txtpageno"))

if isnumeric(strOrderId) then strOrderId = cdbl(strOrderId) end if

if Trim("" & Request.Form("txthidacctid")) <> "" then strCustId = Trim("" & Request.Form("txthidacctid"))
dim strSQL, strFilter, strJoinClause
dim PgSize, strWhere, RecCount, TotalPages
PgSize = 20
if PgNo = "" then PgNo = 1
if (not ISNumeric(PgNo)) then PgNo = 1

function fnstrFilter()
	'THIS FUNCTION WILL BUILD THE WHERE CLAUSE
	Dim strFilter
	strFilter = ""
	strFilter = fnstrAppendFilter(strFilter,"A.ACCOUNTID",strCustId,"="," AND ","","")
	strFilter = fnstrAppendFilter(strFilter,"A.LName",strLName,"LIKE"," AND ","","")
	strFilter = fnstrAppendFilter(strFilter,"A.FName",strFname,"LIKE"," AND ","","")
	strFilter = fnstrAppendFilter(strFilter,"A.PHONE",strPhone,"LIKE"," AND ","","")
	if instr(strOrderId, "-") > 0 then
		strFilter = fnstrAppendFilter(strFilter,"extOrderNumber",strOrderId,"="," AND ","","")
	elseif len(strOrderId) > 7 then
'		strFilter = fnstrAppendFilter(strFilter,"O.extOrderNumber",strOrderId,"="," AND ","","")
		if strFilter <> "" then
			strFilter = strFilter & " AND (O.orderid = '" & strOrderId & "' or O.extOrderNumber = '" & strOrderId & "')"
		else
			strFilter = strFilter & "(O.orderid = '" & strOrderId & "' or O.extOrderNumber = '" & strOrderId & "')"
		end if
	else
		strFilter = fnstrAppendFilter(strFilter,"O.ORDERID",strOrderId,"="," AND ","","")
	end if
	
	strFilter = fnstrAppendFilter(strFilter,"A.Email",strEmailAddr,"="," AND ","","")
	strFilter = fnstrAppendFilter(strFilter,"Convert(varchar(10),O.ORDERDATETIME ,101)",strDate,"="," AND ","","")
	select case strConfirm
		case "YES":
			if strFilter <> "" then strFilter = strFilter & " AND "
			strFilter = strFilter & " (CONFIRMSENT = 'yes') "
		case "NO":
			if strFilter <> "" then strFilter = strFilter & " AND "
			strFilter = strFilter & " (CONFIRMSENT IS NULL Or CONFIRMSENT <> 'yes') "
	end select
	select case strApproved
		case "YES":
			if strFilter <> "" then strFilter = strFilter & " AND "
			strFilter = strFilter & " (APPROVED = 1) "
		case "NO":
			if strFilter <> "" then strFilter = strFilter & " AND "
			strFilter = strFilter & " (APPROVED IS NULL OR APPROVED = 0) "
	end select
	
	if "" <> strSiteID then
		strFilter = strFilter & " and x.site_id = '" & strSiteID & "' "
	end if
	
'	if inStr(strFilter,"O.") > 0 then
'		if strFilter <> "" then strFilter = strFilter & " AND "
'		strFilter = strFilter & " O.store = 0 "
'	end if
	
	if strFilter <> "" then strFilter = " WHERE " & strFilter
	fnstrFilter = strFilter
end function

function fnstrAppendFilter(strFilter, strFieldName, strSearchVal, strMatchType, strJoin, strFuture1, strFuture2)
	fnstrAppendFilter = strFilter
	if strSearchVal <> "" then
		if fnstrAppendFilter <> "" then fnstrAppendFilter = fnstrAppendFilter & strJoin
		select case strMatchType
			case "LIKE" :
			fnstrAppendFilter = fnstrAppendFilter & "( " & strFieldName & " Like '" &  Replace(strSearchVal,"'","''") & "%')"
			case "=" :
			fnstrAppendFilter = fnstrAppendFilter & "( " & strFieldName & "='" &  Replace(strSearchVal,"'","''") & "')"
		end select
	end if
end function

function fnStrJoinClause(strQryType)
	'This function will return join clause with the Orders table only when required.
	'JOIN CLAUSE WILL BE REQUIRED WHEN CLIENT ENTERS ORDER TABLE RELATED CRITERIA'S ALSO
	fnStrJoinClause=""
	select Case Ucase(strQryType)
'		case "MAINQRY" : fnStrJoinClause = " INNER JOIN WE_ORDERS O ON A.ACCOUNTID = O.ACCOUNTID "
		case "MAINQRY" 
			fnStrJoinClause = 	" INNER JOIN WE_ORDERS O ON A.ACCOUNTID = O.ACCOUNTID join xstore x on o.store = x.site_id and a.site_id = x.site_id join " & vbcrlf & _
								"		(	select	a.orderid, count(*) numItems, sum(isnull(case when a.musicSkins = 1 then 1 " & vbcrlf & _
								"											else" & vbcrlf & _
								"												case when b.vendor in ('CM', 'DS', 'MLD') then 1 " & vbcrlf & _
								"													when b.partnumber like '%HYP%' then 1" & vbcrlf & _
								"													else 0 " & vbcrlf & _
								"												end" & vbcrlf & _
								"										end, 0)) numDropShipItem" & vbcrlf & _
								"				,	sum(case when r.orderid is not null and r.cancelDate is null then 1 else 0 end) backOrder" & vbcrlf & _
								"			from	we_orderdetails a left outer join we_items b" & vbcrlf & _
								"				on	a.itemid = b.itemid left outer join we_rma r" & vbcrlf & _
								"				on	a.orderid = r.orderid" & vbcrlf & _
								"			group by a.orderid" & vbcrlf & _
								"		) b" & vbcrlf & _
								"	on	o.orderid = b.orderid left outer join we_ordernotes n" & vbcrlf & _
								"	on 	o.orderid = n.orderid left outer join we_phoneorders p" & vbcrlf & _
								"	on	o.orderid = p.orderid" & vbcrlf
								
		case "SUBQRY"  : fnStrJoinClause = " INNER JOIN WE_ORDERS SO ON SA.ACCOUNTID = SO.ACCOUNTID join xstore x on so.store = x.site_id and sa.site_id = x.site_id "
	end select
end function

strSQL = "SELECT Top " & PgSize & " A.accountid, A.FName, A.LName, A.Email, A.PHONE, A.pword," & vbcrlf
strSQL = strSQL & " O.ORDERID, O.ORDERDATETIME, O.ORDERSUBTOTAL, O.SHIPTYPE, O.trackingNum, nullif(x.logo, '') site_logo," & vbcrlf
strSQL = strSQL & " x.shortDesc site, x.color logo_color, x.site_id, x.longDesc, x.newgistics_customerid, " & vbcrlf
strSQL = strSQL & " O.ORDERSHIPPINGFEE, O.ORDERGRANDTOTAL, O.APPROVED, O.EMAILSENT, O.CONFIRMSENT," & vbcrlf
strSQL = strSQL & " O.extOrderType, O.PhoneOrder, isnull(O.RmaStatus, 0) RmaStatus, isnull(n.ordernotes, '') ordernotes,  " & vbcrlf
strSQL = strSQL & " O.approved, O.scanDate, O.cancelled,  " & vbcrlf
strSQL = strSQL & " O.thub_posted_to_accounting processed, convert(varchar(16), O.thub_posted_date, 20) thub_posted_date, O.mobileSite, b.numDropShipItem, b.numItems, b.backOrder, p.orderid pOrderID " & vbcrlf
strSQL = strSQL & " FROM v_accounts A " & vbcrlf
'ALWAYS INNER JOIN WITH THE ORDER'S TABLE FOR THIS SCREEN
strJoinClause = fnStrJoinClause("MAINQRY")
'ADD THE WHERE CLAUSE
strWhere = fnstrFilter
'PREPARE THE MAIN QRY
strSQL = strSQL & " " & strJoinClause & " " & strWhere

dim strCountSQL
strCountSQL = "SELECT O.ORDERID From v_accounts A "
strCountSQL = strCountSQL & " " & strJoinClause & " " & strWhere

if (PgNo <> 1) then
	if strWhere = "" then strSQL = strSQL & " WHERE O.OrderId Not In "
	if strWhere <> "" then strSQL = strSQL & " AND O.OrderId Not In "
	strSQL = strSQL & "(SELECT Top " & (PgNo-1) * PgSize & " SO.OrderId From v_accounts SA "
	strSQL = strSQL & fnStrJoinClause("SUBQRY")
	strSQL = strSQL & replace(replace(Ucase(strWhere),"A.","SA."), "O.", "SO.")
	strSQL = strSQL & " Order By SO.ORDERID) "
end if
strSQL = strSQL & " Order By O.ORDERID"

'response.write "strSQL:<pre>" & strSQL & "</pre><br><br>"
'response.write "strCountSQL:<pre>" & strCountSQL & "</pre><br>"

session("errorSQL") = strSQL

dim objRsCount : set objRsCount = Server.CreateObject("ADODB.RECORDSET")
dim objRsOrder : set objRsOrder = Server.CreateObject("ADODB.RECORDSET")
objRsOrder.open strSQL, oConn, 3, 1

if objRsOrder.eof then
	objRsOrder.close
	set objRsOrder = nothing
	set objRsOrder = Server.CreateObject("ADODB.RECORDSET")
	strSQL 		= replace(strSQL, "WE_ORDERS", "WE_ORDERS_HISTORICAL")
	objRsOrder.open strSQL, oConn, 3, 1	
	
	strCountSQL = replace(strCountSQL, "WE_ORDERS", "WE_ORDERS_HISTORICAL")	
end if 

objRsCount.open strCountSQL, oConn, 3, 1

if objRsCount.EOF then 
	RecCount = 0
else 
	RecCount = objRsCount.recordcount
end if 

TotalPages = TISPL_fnReturnTotalPages(Cdbl(RecCount),Cdbl(PgSize))
%>

<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
	<tr class="bigText" bgcolor="#CCCCCC">
		<td colspan="19" width="100%">
			<table border="0" width="100%" class="bigText" bgcolor="#CCCCCC">
				<tr>
					<td colspan="20%" align="left"><a href="#" onClick="window.history.go(-1);"><< BACK</a></td>
					<td colspan="60%" align="center"> Order Look-up Search Criteria </td>
					<td colspan="20%" align="right"><a href="#" onClick="window.history.go(1);">FORWARD >></a></td>
				</tr>
			</table>
		</td>
	</tr>
	<form name="frmSearch" method="post" action="orderslist.asp" id="id_frmSearch">
	<tr>
		<td class="smlText" colspan="19">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="smlText">First Name : <input class="smlText" type="text" name="txtfname" size="8" value="<%=strFname%>">&nbsp;</td>
					<td class="smlText">Last Name : <input class="smlText" type="text" name="txtlname" size="10" value="<%=strLName%>">&nbsp;</td>
					<td class="smlText">Customer # : <input class="smlText" type="text" name="txtaccountid" size="8" value="<%=strCustId%>">&nbsp;</td>
					<td class="smlText">Phone # : <input class="smlText" type="text" name="txtphone" size="8" value="<%=strPhone%>"><br></td>
					<td class="smlText">Email Address : <input class="smlText" type="text" name="txtEmailAddr" size="25" value="<%=Request.Form("txtEmailAddr")%>"><br></td>
				</tr>
				<tr>
					<td class="smlText">&nbsp;&nbsp;&nbsp;Order Id : <input class="smlText" type="text" name="txtorderid" size="8" value="<%=strOrderId%>">&nbsp;</td>
					<td class="smlText">Order Date : <input class="smlText" type="text" name="txtorderdate" size="10" maxlength="10" value="<%=strDate%>">&nbsp;</td>
					<td class="smlText">Approved : <select class="smlText" name="txtApproved"><option value="">All</option><option value="YES"<%if strApproved="YES" then response.write(" selected")%>>Yes</option><option value="NO" <%if strApproved="NO" then response.write("selected")%>>No</option></select> &nbsp;</td>
					<td class="smlText" colspan="2">Confirm : <select class="smlText" name="txtconfirm"><option value="">All</option><option value="YES"<%if strConfirm="YES" then response.write(" selected")%>>Yes</option><option value="NO" <%if strConfirm="NO" then response.write("selected")%>>No</option></select></td>
				</tr>
				<tr>
					<td colspan="7" align="center"><input type="SUBMIT" onclick="return doSubmit();" value="SEARCH" class="smltext" id="SUBMIT1" name="SUBMIT1">&nbsp;&nbsp;&nbsp;<input type="button" value="CLEAR" class="smltext" onClick="resetAll();"><input type="hidden" name="txtpageno" value="1"></td>
				</tr>
				<tr>
					<td colspan="7" align="center" class="smlText"><b>Note:</b> Search on Names and Phone number uses pattern matching and retrieves all records containing entered words.<BR>Search on Other fields - OrderId, OrderDate, Customer # will look for records containing exact match.</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC" class="smlText">
		<td valign="middle" colspan="19" align="right">
		Page No :
		<select name="selpage" class="smlText" onChange="fngotopg(this.value);">
			<%for iLoop = 1 to TotalPages%>
				<option value='<%=iLoop%>' <%if CSTR(PgNo)=Cstr(iLoop) then response.write("Selected")%>><%=iLoop%></a>
			<%next%>
		</select>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td class="bigText" align="center">Store</td>
		<td class="bigText">Order<br>Id</font></td>
		<td class="bigText">Order<br>Date/Time</td>
		<td class="bigText">Scan<br>Date/Time</td>		
		<td class="bigText">First<br>Name</td>
		<td class="bigText">Last<br>Name</td>
		<td class="bigText">Email<br>Address</td>
		<td class="bigText">B.O.</td>
		<td class="bigText">DropShip<br />Items</td>
		<td class="bigText">Multiple<br />Items</td>
		<td class="bigText">Apvd.</td>
		<td class="bigText">Cncld.</td>		
		<td class="bigText">Order<br>Type</td>
		<td class="bigText">Shipping<br>Type</td>
		<td class="bigText">Grand<br>Total</td>
		<td class="bigText">Phone<br>Order</td>
		<td class="bigText">Notes</td>
		<td class="bigText">RMA</td>
		<td class="bigText">Processed</td>
	</tr>
	<%
	dim a, nRow
	a = 0
	nRow = 0
	if not objRsOrder.eof then
		do until objRsOrder.eof
			a = a + 1
			nRmaStatus = objRsOrder("RmaStatus")

			select case objRsOrder("extOrderType")
				case 1 : OrderType = "Paypal"
				case 2 : OrderType = "Google"
				case 3 : OrderType = "eBillme"
				case 4 : OrderType = "Ebay"
				case 5 : OrderType = "AtomicMall"
				case 6 : OrderType = "Amazon"
				case 7 : OrderType = "Buy.Com"
				case 8 : OrderType = "Sears"
				case 9 : OrderType = "Bestbuy"
				case 10 : OrderType = "Newegg"
				case else : OrderType = "Credit Card"
			end select
			
			strTrackingNum = objRsOrder("OrderId")
			numTrackingOption = 0
			newgisticsID = objRsOrder("newgistics_customerid")
			if left(objRsOrder("trackingNum"),2) = "1Z" then
				strTrackingNum = objRsOrder("trackingNum")
				numTrackingOption = 1
			elseif left(objRsOrder("trackingNum"),2) = "EO" or left(objRsOrder("trackingNum"),2) = "91" or left(objRsOrder("trackingNum"),2) = "CJ" or left(objRsOrder("trackingNum"),2) = "LN" or left(objRsOrder("trackingNum"),2) = "LZ" then
				strTrackingNum = objRsOrder("trackingNum")
				numTrackingOption = 2
			elseif left(objRsOrder("trackingNum"),2) = "94" then
				strTrackingNum = objRsOrder("trackingNum")
				numTrackingOption = 5
			elseif left(objRsOrder("trackingNum"),2) = "04" or left(objRsOrder("trackingNum"),1) = "9" then
				strTrackingNum = objRsOrder("trackingNum")
				numTrackingOption = 3
			elseif left(objRsOrder("trackingNum"),1) = "4" then
				strTrackingNum = objRsOrder("trackingNum")
				numTrackingOption = 5
			elseif not isnull(objRsOrder("scanDate")) then
				if objRsOrder("site_id") = "0" then
					if objRsOrder("scandate") >= cdate("12/05/2011") and objRsOrder("scandate") < cdate("04/02/2012") then	'for we newgistics
						strTrackingNum = objRsOrder("OrderId")
						numTrackingOption = 6
					elseif objRsOrder("scandate") >= cdate("04/02/2012") then
						strTrackingNum = "WE" & objRsOrder("OrderId")
						numTrackingOption = 3
					else
						strTrackingNum = "WE" & objRsOrder("OrderId")
						numTrackingOption = 4
					end if
				elseif objRsOrder("site_id") = "2" then
					if objRsOrder("scandate") >= cdate("02/21/2012") and objRsOrder("scandate") < cdate("04/11/2012") then
						strTrackingNum = objRsOrder("OrderId")
						numTrackingOption = 6
					else
						strTrackingNum = "WE" & objRsOrder("OrderId")
						numTrackingOption = 3
					end if
				elseif objRsOrder("site_id") = "1" or objRsOrder("site_id") = "3" or objRsOrder("site_id") = "10" then	' for all the other sites' newgistics
					if objRsOrder("scandate") >= cdate("02/21/2012") and objRsOrder("scandate") < cdate("07/25/2012") then
						strTrackingNum = objRsOrder("OrderId")
						numTrackingOption = 6
					else
						strTrackingNum = "WE" & objRsOrder("OrderId")
						numTrackingOption = 3
					end if
				else	'MI
					strTrackingNum = "WE" & objRsOrder("OrderId")
					numTrackingOption = 4
				end if
			elseif left(objRsOrder("trackingNum"),2) = "D1" then
				strTrackingNum = objRsOrder("trackingNum")
				numTrackingOption = 7
			else
				numTrackingOption = 0
			end if
			%>
			<tr class="grid_row_<%=(nRow mod 2)%>" onMouseOver="this.className='mouseover'" onMouseOut="this.className='grid_row_<%=(nRow mod 2)%>'" >
				<td class="smlText" height="30" align="center">
				<%
				if isnull(objRsOrder("site_logo")) then 
				%>
				<font color="<%=objRsOrder("logo_color")%>"><b><%=objRsOrder("site")%></b></font>
				<%
				else
				%>
				<img src="<%=objRsOrder("site_logo")%>" border="0" width="16" height="16" />
				<%
				end if
				
				if objRsOrder("mobileSite") then response.Write("<br />Mobile")
				%>
				</td>			
				<td class="smlText"><a href="#" onClick="printinvoice('<%=objRsOrder("OrderId")%>','<%=objRsOrder("AccountId")%>');"><%=objRsOrder("OrderId")%></a></td>
				<td class="smlText">&nbsp;<%=objRsOrder("OrderDateTime")%></td>
				<td class="smlText">&nbsp;<%=nullif(objRsOrder("scanDate"), "N/A", objRsOrder("scanDate"))%></td>				
				<td class="smlText">&nbsp;<%=objRsOrder("FName")%></td>
				<td class="smlText">&nbsp;<%=objRsOrder("LName")%></td>
				<td class="smlText">&nbsp;<a href="<%="mailto:" & objRsOrder("Email") & "?subject=" & "Regarding your order %28%23" & objRsOrder("OrderId") & "%29 from " & objRsOrder("longDesc")%>"><%=objRsOrder("Email")%></a></td>
				<td class="smlText"><input type="checkbox" name="chkBO" value="" <% if objRsOrder("backOrder") > 0 then %> checked="checked" <% end if %>/>
				<td class="smlText">&nbsp;
                <%
				if objRsOrder("numDropShipItem") > 0 then
					response.write "<b>YES</b>"
				else
					response.write "NO"
				end if
				%>
                </td>
				<td class="smlText">&nbsp;
                <%
				if objRsOrder("numItems") > 1 then
					response.write "<b>YES</b>"
				else
					response.write "NO"
				end if
				%>
                </td>
				<td class="smlText">&nbsp;<%=trueif(objRsOrder("approved"), "<b>YES</b>", "NO")%></td>
				<td class="smlText">&nbsp;<%=trueif(objRsOrder("cancelled"), "<b>YES</b>", "NO")%></td>				
				<td class="smlText">&nbsp;<%=OrderType%></td>
                <% if numTrackingOption <> 0 then %>
				<td class="smlText">&nbsp;<a href="javascript:fnTracking(<%=numTrackingOption%>,'<%=strTrackingNum%>','<%=newgisticsID%>');"><%=objRsOrder("ShipType")%></a></td>
                <% else %>
                <td class="smlText"><%=objRsOrder("ShipType")%></td>
                <% end if %>
				<td class="smlText">&nbsp;<%=objRsOrder("OrderGrandTotal")%></td>
				<td class="smlText" align="left">
                	<input type="checkbox" name="PhoneOrder<%=a%>" value="1" onClick="fnPhoneOrder();"<%if not isNull(objRsOrder("PhoneOrder")) and objRsOrder("PhoneOrder") <> "" then response.write " checked"%>>
	                <input type="hidden" name="orderID<%=a%>" value="<%=objRsOrder("orderID")%>">
                    <% if not isnull(objRsOrder("pOrderID")) then %>
                    <img src="/images/phone1.jpg" width="16" height="16" border="0" alt="order by phone-order tool" title="order by phone-order tool" />
                    <% end if %>
				</td>
				<td align="left" class="smlText" title="<%if objRsOrder("ordernotes") = "" then response.write("No notes entered") else response.write(Left(Replace(objRsOrder("ordernotes"),"""",""),100)) end if%>"><a href="#" onClick="fnopennotes('<%=objRsOrder("orderid")%>');"><img src='/images/Notes<%if objRsOrder("ordernotes") = "" then response.write "_none"%>.gif' border=0></a></td>
				<%
				sql = "select id, rma_token from XRmaType where id = '" & nRmaStatus & "'"
				set rsRMA = oConn.execute(sql)
				if not rsRMA.eof then 
					strRmaStatus = rsRMA("rma_token")
				else
					strRmaStatus = "START RMA"
				end if
				%>
				<td align="left" class="smlText" title="<%if strRmaStatus = "" then response.write("No RMA started") else response.write(Left(Replace(strRmaStatus,"""",""),30)) end if%>"><a href="#" onClick="fnRMA('<%=objRsOrder("orderid")%>','<%=objRsOrder("site_id")%>')"><b><%=strRmaStatus%></b></a></td>
				<td class="smlText">
					<a style="font-size:8pt;" href="javascript:location.href='/admin/db_update_order_status.asp?orderStart=<%=objRsOrder("OrderId")%>&orderEnd=<%=objRsOrder("OrderId")%>&submitted=Search'">
					<%=nullif(objRsOrder("processed"), "NO", "YES")%>
					<%=nullif(objRsOrder("thub_posted_date"), "", "<br>(" & objRsOrder("thub_posted_date") & ")")%>
					</a>
				</td>
			</tr>
			<%
			nRow = nRow + 1
			objRsOrder.movenext
		loop
	else
		%>
		<tr><td colspan="19" class="smlText" align="center"><b>No Records Found</b></td></tr>
		<%
	end if
	%>
	<input type="hidden" name="totalOrders" value="<%=a%>">
	</form>
</table>
<%
objRsOrder.close
set objRsOrder = nothing
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
