<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Process MobileLine Products"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
'	response.Write("page start:" & time & "<br>")
	dim itemID, mobileLine_sku, brandID, modelID, hld_id, hld_mobileLineID, hld_cat1, hld_cat2, hld_cat3, hld_cat4
	dim hld_name, hld_description, hld_shortDesc, hld_weight, hld_msrp, hld_cogs, hld_model, hld_image, hld_inv_qty, hld_UPC, hld_status
	dim sql, rs, typeID, hfCode, skipProd, left3
	dim lap : lap = 0
	dim procCnt : procCnt = 0
	dim zeroQty : zeroQty = 0
	dim unfoundModel : unfoundModel = 0
	dim unknownItems : unknownItems = ""
	dim pageLap : pageLap = prepInt(request.QueryString("pageLap"))
	dim retrieve : retrieve = prepInt(request.QueryString("retrieve"))
	
	if len(session("zeroQty")) < 1 then session("zeroQty") = 0
	if len(session("unassignedProds")) < 1 then session("unassignedProds") = ""
	
	if pageLap = 0 then pageLap = 1
	
	if retrieve = 1 then
		sql = "update holding_mobileLine set processed = 0"
		session("errorSQL") = sql
		oConn.execute(sql)
'		response.Write("product reset:" & time & "<br>")
	end if
	
	sql = "select top 501 b.itemID, b.mobileLine_sku, b.brandID, b.modelID, a.* from holding_mobileLine a with(nolock) left join we_items b with (nolock) on a.mobileLineID = cast(b.mobileLine_sku as varchar(200)) and b.master = 1 where processed = 0 order by a.id"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
'	response.Write("pull wave:" & time & "<br>")
	
	do while not rs.EOF
		lap = lap + 1
		itemID = rs("itemID")
		mobileLine_sku = rs("mobileLine_sku")
		brandID = rs("brandID")
		modelID = rs("modelID")
		hld_id = rs("id")
		hld_mobileLineID = rs("mobileLineID")
		hld_cat1 = rs("cat1")
		hld_cat2 = rs("cat2")
		hld_cat3 = rs("cat3")
		hld_cat4 = rs("cat4")
		hld_name = rs("name")
		hld_description = rs("description")
		hld_shortDesc = rs("shortDesc")
		hld_weight = rs("weight")
		hld_msrp = rs("msrp")
		hld_cogs = rs("cogs")
		hld_model = rs("model")
		hld_image = rs("image")
		hld_inv_qty = rs("inv_qty")
		hld_UPC = rs("UPC")
		hld_status = rs("status")
		
		if not isnull(itemID) then
			procCnt = procCnt + 1
			if hld_status = "Discontinued" then lastCall = 1 else lastCall = 0
'			if (hld_inv_qty = 0 and lastCall = 1) or hld_status = "Discontinued" then
'				sql = "update we_items set inv_qty = " & hld_inv_qty & ", hideLive = 1 where itemID = " & itemID
'			elseif hld_inv_qty = 0 then
'				sql = "update we_items set inv_qty = " & hld_inv_qty & ", ghost = 1 where itemID = " & itemID
'			else
'				sql = "update we_items set inv_qty = " & hld_inv_qty & ", ghost = 0, lastCall = " & lastCall & " where itemID = " & itemID
'			end if
			
			updateQty = hld_inv_qty
			if updateQty < 0 then updateQty = 0
			if (updateQty < 10 and lastCall = 1) or (updateQty < 5 and lastCall = 0) then
				sql = "update we_items set inv_qty = 0, hideLive = 1, lastCall = " & lastCall & " where partnumber in ( select partnumber from we_items where itemid = " & itemID & ")"
			else
				sql = "update we_items set inv_qty = case when master = 1 then " & updateQty & " else -1 end, lastCall = " & lastCall & " where partnumber in ( select partnumber from we_items where itemid = " & itemID & ")"
			end if
			session("errorSQL") = sql
			oConn.execute(sql)
			
			sql = "delete from holding_mobileLine where id = " & hld_id
			session("errorSQL") = sql
			oConn.execute(sql)
		else
			if hld_inv_qty > 0 then
				if unfoundModel <> hld_model then
					unassignedProds = unassignedProds & rs("id") & "##" & hld_model & "##" & hld_name & "##" & hld_msrp & "##" & hld_cogs & "@@"
					unfoundModel = hld_model
				end if
			else
				zeroQty = zeroQty + 1
			end if
		end if
		rs.movenext
		if lap = 500 then exit do
	loop
'	response.Write("cycle complete:" & time & "<br>")
	sql = "update holding_mobileLine set processed = 1 where processed = 0 and id <= " & hld_id
	session("errorSQL") = sql
	oConn.execute(sql)
'	response.Write("mass update:" & time & "<br>")
	
	session("zeroQty") = session("zeroQty") + zeroQty
	session("unassignedProds") = session("unassignedProds") & unassignedProds
	if rs.EOF then		
		rs = null
		session("totalProducts") = null
		response.Redirect("/admin/mobileLineXLS.asp")
		response.End()
	else
		rs = null
		pageLap = pageLap + 1
	end if
%>
<table align="center" border="0" cellpadding="3" cellspacing="0" style="padding-top:100px; color:#F00; font-weight:bold; font-size:16px;">
	<tr>
    	<td>
        	Products Processed: <%=(pageLap - 1) * 500%>/<%=session("totalProducts")%><br />
            Please wait while the process completes
        </td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	setTimeout("window.location = '/admin/mobileLineXLS2.asp?pageLap=<%=pageLap%>'",1000)
</script>