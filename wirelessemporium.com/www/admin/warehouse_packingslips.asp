<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Warehouse Item Location"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
newPartnumber = prepStr(request.form("newPartnumber"))
rows = prepInt(request.form("txtRows"))
strPartnumber = prepStr(request.form("txtPartnumber"))
if rows = 0 then rows = 10
if strPartnumber = "" then strPartnumber = newPartnumber
	
if request.form("submitted") <> "" then
	newAisle = prepInt(request.form("newAisle"))
	newShelf = prepStr(request.form("newShelf"))
	newRow = prepStr(request.form("newRow"))
	newLabel = prepStr(request.form("newLabel"))	
					
	sql	=	"select id from warehouseItemLocation where partnumber = '" & newPartnumber & "'"
'	response.write sql & "<br>"
	set	rs = oConn.execute(sql)
	if not rs.eof then
		response.write "<div style=""color:#900; width:100%; font-size:20px; font-weight:bold; text-align:center;"">FAILED: Partnumber " & newPartnumber & " already existed in the system</div>"
	else
		sql	=	"insert into warehouseItemLocation(partnumber, aisle, shelf, row, label)" & vbcrlf & _
				"values('" & ucase(newPartnumber) & "', '" & ucase(newAisle) & "', '" & ucase(newShelf) & "', '" & ucase(newRow) & "','" & ucase(newLabel) & "')"
		oConn.execute(sql)
	end if
end if

sql = 	"select	top " & rows & " id, partnumber, aisle, shelf, row, label" & vbcrlf & _
		"from	warehouseItemLocation" & vbcrlf
if strPartnumber <> "" then
	sql	=	sql & "where	partnumber like '" & strPartnumber & "%'" & vbcrlf
end if		
sql	=	sql & "order by 2"
set rsSearch = oConn.execute(sql)
%>
<form name="frm" method="post">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="1000">
	<tr>
    	<td width="100%" align="left">
        	<div class="left">
            	<div style="float:left; width:300px;"><h1 style="font-size:20px;">Warehouse Item Locations</h1></div>
            </div>
            <div class="clear">&nbsp;</div>
            <!-- filters-->
            <div style="width:990px; border:1px solid #ccc; border-radius:3px; background-color:#f4f7f9; padding:5px;">
                <div style="width:100%; height:30px;">
                    <div class="filter-box-header" style="width:50px;">Filter</div>
                    <div class="filter-box-header" style="width:180px;">Rows</div>
                    <div class="filter-box-header" style="width:180px;">Partnumber</div>
                    <div class="filter-box-header" style="width:130px;">Action</div>
                </div>
                <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
                <div style="width:100%; padding:5px 0px 5px 0px; height:25px;">
                    <div class="filter-box-header" style="width:50px;">Value</div>
                    <div class="filter-box" style="width:180px;"><input type="text" name="txtRows" value="<%=rows%>"></div>
                    <div class="filter-box" style="width:180px;"><input type="text" name="txtPartnumber" value="<%=strPartnumber%>"></div>
                    <div class="filter-box" style="width:130px;">
                        <input type="submit" name="search" value="Search" />
                    </div>
                </div>
            </div>
            <!--// filters-->
            <div class="clear">&nbsp;</div>
        	<div class="left w100">
				<div style="float:left; width:100%; padding:5px 0px 5px 0px; border:1px solid #c1e0b2; border-radius:3px; background-color:#eaf5e5;">
					<div style="float:left; width:82px; font-size:13px; font-weight:bold; text-align:left;">
                    	<div class="filter-box-header" style="width:60px;">ID</div>
					</div>
					<div style="float:left; width:202px; font-size:13px; font-weight:bold; text-align:left;">
                    	<div class="filter-box-header" style="width:180px;">Partnumber</div>
					</div>
					<div style="float:left; width:102px; font-size:13px; font-weight:bold; text-align:left;">
                    	<div class="filter-box-header" style="width:80px;">Aisle</div>
					</div>
					<div style="float:left; width:102px; font-size:13px; font-weight:bold; text-align:left;">
                    	<div class="filter-box-header" style="width:80px;">Shelf</div>
					</div>
					<div style="float:left; width:102px; font-size:13px; font-weight:bold; text-align:left;">
                    	<div class="filter-box-header" style="width:80px;">Row</div>
					</div>
					<div style="float:left; width:102px; font-size:13px; font-weight:bold; text-align:left;">
                    	<div class="filter-box-header" style="width:80px;">Label</div>
					</div>
				</div>
                <div class="clear"></div>
			<%
			lap = 0
			do until rsSearch.eof
				lap = lap + 1			
				rowBackgroundColor = "#fff"
				if (lap mod 2) = 0 then rowBackgroundColor = "#eaf5fa"
				if (lap mod 300) = 0 then response.flush
			%>
				<div id="row_<%=rsSearch("id")%>" class="left w100" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;">
                	<div class="left" style="margin-left:5px; padding:5px 0px 0px 5px; width:80px; text-align:center;"><%=rsSearch("id")%></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:200px; text-align:center;"><%=rsSearch("partnumber")%></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:100px;"><input type="text" id="id_aisle_<%=rsSearch("id")%>" name="txtAisle" value="<%=rsSearch("aisle")%>" style="width:65px;" /></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:100px;"><input type="text" id="id_shelf_<%=rsSearch("id")%>" name="txtShelf" value="<%=rsSearch("shelf")%>" style="width:65px;" /></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:100px;"><input type="text" id="id_row_<%=rsSearch("id")%>" name="txtRow" value="<%=rsSearch("row")%>" style="width:65px;" /></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:100px;"><input type="text" id="id_label_<%=rsSearch("id")%>" name="txtLabel" value="<%=rsSearch("label")%>" style="width:65px;" /></div>
                	<div class="left" style="padding:5px 0px 0px 5px;">
                    	<div id="preloading_<%=rsSearch("id")%>" style="display:none;"><img src="/images/indicator.gif" /></div>
                    	<div id="updateBtns_<%=rsSearch("id")%>">
                            <input type="button" name="btnEdit" value="Edit" onClick="edit('e',<%=rsSearch("id")%>);" />
                            <input type="button" name="btnDelete" value="Delete" onClick="edit('d',<%=rsSearch("id")%>);" />
                        </div>
					</div>
				</div>
			<%
				rsSearch.movenext
			loop
			lap = lap + 1			
			rowBackgroundColor = "#fff"
			if (lap mod 2) = 0 then rowBackgroundColor = "#eaf5fa"
			%>
				<div class="left w100" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;">
                	<div class="left" style="margin-left:5px; padding:5px 0px 0px 5px; width:80px; text-align:center;"></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:200px;"><input type="text" name="newPartnumber" value="" style="width:150px;" /></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:100px;"><input type="text" name="newAisle" value="" style="width:65px;" /></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:100px;"><input type="text" name="newShelf" value="" style="width:65px;" /></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:100px;"><input type="text" name="newRow" value="" style="width:65px;" /></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:100px;"><input type="text" name="newLabel" value="" style="width:65px;" /></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:100px;"><input type="submit" name="submitted" value="Add New Value" /></div>
				</div>
            </div>
		</td>
    </tr>
	
</table>
</form>
<script>
	function edit(edittype, id) {
		var aisle = document.getElementById('id_aisle_'+id).value;
		var shelf = document.getElementById('id_shelf_'+id).value;
		var row = document.getElementById('id_row_'+id).value;
		var label = document.getElementById('id_label_'+id).value;
		document.getElementById('updateBtns_'+id).style.display = 'none';
		document.getElementById('preloading_'+id).style.display = '';
		
		ajax("/ajax/admin/ajaxWItemLocation.asp?id=" + id + "&edittype=" + edittype + "&aisle=" + aisle + "&shelf=" + shelf + "&row=" + row + "&label=" + label,"dumpZone");
		setTimeout("doneUpdates('" + id + "','" + edittype + "')",1500);
	}
	
	function doneUpdates(id,edittype) {
		if (edittype == 'd') {
			document.getElementById('row_'+id).style.display = 'none';
		} else {
			document.getElementById('updateBtns_'+id).style.display = '';
			document.getElementById('preloading_'+id).style.display = 'none';
		}
	}
	
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
