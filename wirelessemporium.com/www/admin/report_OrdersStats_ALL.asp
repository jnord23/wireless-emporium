<%
response.buffer = false
pageTitle = "WE Admin Site - Daily Orders - Regenerate E-mail Report for ALL Sites"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
response.write "<h3>" & pageTitle & "</h3>" & vbcrlf
dim dateStart, strError
strError = ""
if request.form("submitted") <> "" then
	dateStart = request.form("dateStart")
	if not isDate(dateStart) then strError = "Please enter a valid Report Date."
	if strError = "" then
		dateStart = dateValue(dateStart)
		dim bodyText
		bodyText = ""
		dim a, b, SQLOrderType, myTotal, myStore, myURL, myTable
		for b = 1 to 10
			myTotal = 0
			select case b
				case 1
					a = 0
					myStore = "Wireless Emporium"
					SQLOrderType = " AND (A.extOrderType IS NULL OR A.extOrderType < 4)"
				case 2
					a = 2
					myStore = "Cellular Outfitter"
					SQLOrderType = " AND (A.extOrderType IS NULL OR A.extOrderType < 4)"
				case 3
					a = 1
					myStore = "Cellphone Accents"
					SQLOrderType = " AND (A.extOrderType IS NULL OR A.extOrderType < 4)"
				case 4
					a = 3
					myStore = "Phonesale"
					SQLOrderType = " AND (A.extOrderType IS NULL OR A.extOrderType < 4)"
				case 5
					a = 10
					myStore = "eReaderSupply.com"
					SQLOrderType = " AND (A.extOrderType IS NULL OR A.extOrderType < 4)"
				case 6
					a = 0
					myStore = "eBay"
					SQLOrderType = " AND A.extOrderType = 4"
				case 7
					a = 0
					myStore = "AtomicMall"
					SQLOrderType = " AND A.extOrderType = 5"
				case 8
					a = 0
					myStore = "Amazon - WE"
					SQLOrderType = " AND A.extOrderType = 6"
				case 9
					a = 0
					myStore = "Buy.com - WE"
					SQLOrderType = " AND A.extOrderType = 7"
				case 10
					a = 0
					myStore = "Sears"
					SQLOrderType = " AND A.extOrderType = 8"
			end select
			select case a
				case 0
					myURL = "wirelessemporium"
					myTable = "we"
				case 1
					myURL = "cellphoneaccents"
					myTable = "CA"
				case 2
					myURL = "cellularoutfitter"
					myTable = "CO"
				case 3
					myURL = "phonesale"
					myTable = "PS"
				case 10
					myURL = "ereadersupply"
					myTable = "ER"
			end select
			
			SQL = "SELECT A.ordergrandtotal FROM we_orders A INNER JOIN " & myTable & "_accounts B ON A.accountid=B.accountid"
			SQL = SQL & " WHERE A.orderdatetime >= '" & dateStart & "' AND A.orderdatetime < '" & dateAdd("D",1,dateStart) & "'"
			SQL = SQL & " AND A.approved = 1"
			SQL = SQL & " AND (A.cancelled = 0 OR A.cancelled IS NULL)"
			SQL = SQL & " AND A.store = '" & a & "'"
			SQL = SQL & SQLOrderType
			SQL = SQL & " AND (B.email = 'phoneorder@wirelessemporium.com' OR B.email NOT LIKE '%@wirelessemporium.com')"
			SQL = SQL & " AND a.parentorderid is null"
			SQL = SQL & " ORDER BY A.orderdatetime"
			set RS = server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			if RS.eof then
				bodyText = bodyText & "<h3>No orders found for " & myStore & " - " & dateStart & "</h3>"
			else
				do until RS.eof
					myTotal = myTotal + cDbl(RS("ordergrandtotal"))
					RS.movenext
				loop
				bodyText = bodyText & "<h3>DAILY ORDERS STATISTICS FOR: " & myStore & " - " & dateStart & "</h3>"
				bodyText = bodyText & "<h3>TOTAL ORDERS: " & RS.recordcount & "</h3>"
				bodyText = bodyText & "<h3>TOTAL ORDER AMOUNT: " & formatCurrency(myTotal) & "</h3>"
				bodyText = bodyText & "<h3>AVERAGE ORDER AMOUNT: " & formatCurrency(myTotal/RS.recordcount) & "</h3>"
				if myStore = "Wireless Emporium" or myStore = "Cellular Outfitter" or myStore = "Cellphone Accents" or myStore = "Phonesale" then
					bodyText = bodyText & "<p><a href=""http://www." & myURL & ".com/admin/report_OrdersStats.asp?dateStart=" & replace(cStr(dateStart),"/","%2F") & "&submitted=Generate+Report"">Summary Report</a></p>"
					bodyText = bodyText & "<p><a href=""http://www." & myURL & ".com/admin/report_OrdersStats.asp?dateStart=" & replace(cStr(dateStart),"/","%2F") & "&details=1&submitted=Generate+Report"">Detail Report</a></p>"
				end if
				bodyText = bodyText & "<br><br>"
				'if myStore = "Wireless Emporium" then
				'	SQL = "SELECT C.quantity, D.price_Our FROM we_orders A INNER JOIN " & myTable & "_accounts B ON A.accountid=B.accountid"
				'	SQL = SQL & " INNER JOIN we_orderdetails C ON A.orderid=C.orderid"
				'	SQL = SQL & " INNER JOIN we_items D ON C.itemid=D.itemid"
				'	SQL = SQL & " WHERE A.orderdatetime >= '" & dateStart & "' AND A.orderdatetime < '" & dateAdd("D",1,dateStart) & "'"
				'	SQL = SQL & " AND A.approved = 1"
				'	SQL = SQL & " AND (A.cancelled = 0 OR A.cancelled IS NULL)"
				'	SQL = SQL & " AND A.store = '" & a & "'"
				'	SQL = SQL & " AND D.typeID = 16"
				'	SQL = SQL & " AND (B.email = 'phoneorder@wirelessemporium.com' OR B.email NOT LIKE '%@wirelessemporium.com')"
				'	SQL = SQL & " ORDER BY A.orderdatetime"
				'	set RS = server.CreateObject("ADODB.Recordset")
				'	RS.open SQL, oConn, 3, 3
				'	if RS.eof then
				'		bodyText = bodyText & "<h3>No orders found for " & myStore & " CELL PHONE SALES - " & dateStart & "</h3>"
				'	else
				'		myTotal = 0
				'		myQty = 0
				'		do until RS.eof
				'			myTotal = myTotal + (RS("quantity") * cDbl(RS("price_Our")))
				'			myQty = myQty + RS("quantity")
				'			RS.movenext
				'		loop
				'		bodyText = bodyText & "<h3>CELL PHONE SALES FOR: " & myStore & " - " & dateStart & "</h3>"
				'		bodyText = bodyText & "<h3>TOTAL ORDERS: " & myQty & "</h3>"
				'		bodyText = bodyText & "<h3>TOTAL ORDER AMOUNT: " & formatCurrency(myTotal) & "</h3>"
				'		bodyText = bodyText & "<h3>AVERAGE ORDER AMOUNT: " & formatCurrency(myTotal/myQty) & "</h3>"
				'		bodyText = bodyText & "<br><br>"
				'	end if
				'end if
			end if
		next
		
		response.write bodyText & vbcrlf
		
		Set filesys = nothing
		Set demofolder = nothing
		Set demofile = nothing
		Set filecoll = nothing
	else
		%>
		<center>
		<font size="+2" color="red"><%=strError%></font>
		<a href="javascript:history.back();">BACK</a>
		</center>
		<%
	end if
end if
%>
<p>&nbsp;</p>
<h3>Choose report date:</h3>
<br>
<form action="report_OrdersStats_ALL.asp" name="frmSalesReport" method="post">
	<p>
		<input type="text" name="dateStart">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.frmSalesReport.dateStart,document.frmSalesReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;Report&nbsp;Date
		<input type="hidden" name="dc2" value="<%=dateAdd("yyyy",1,date)%>">
		<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
	</p>
	<p><input type="submit" name="submitted" value="Generate Report"></p>
</form>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
