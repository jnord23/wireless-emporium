<%
	'thisUser = Request.Cookies("username")
	pageTitle = "Custom Case Contest"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<link href="/Content/CSS/adminUsers.css" rel="stylesheet" type="text/css" />
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="/Scripts/adminHome.js" type="text/javascript"></script>
<script src="/Scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
<script src="/Scripts/jquery-ui-1.8.11.min.js" type="text/javascript"></script>
<%

	if request.form("showCorrupt") = "showCorrupt" then
		showCorrupt = true
	else
		showCorrupt = false
	end if

	sql =	"select		COUNT (*) as totalEntries " & vbcrlf &_
			"from		CustomCaseContest " & vbcrlf &_
			"where		email not like '%@wirelessemporium.com'"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	if not rs.eof then
		totalEntries = rs("totalEntries")
	else
		totalEntries = 0
	end if
	
	
	sql =	"select		top 1 * " & vbcrlf &_
			"from		CustomCaseContest " & vbcrlf &_
			"where		email not like '%@wirelessemporium.com' " & vbcrlf &_
			"order by	NEWID()"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	if not rs.eof then
		randomEntry = "Email: " & rs("email") & "<br />ItemID: " & rs("itemID") & "<br />Entry Date: " & rs("entryDate")
		itemID = rs("itemID")
	else
		randomEntry = "A random entry was not selected."
		itemID = 0
	end if
	
	
	if itemID > 0 then
		sql =	"select		itemPic " & vbcrlf &_
				"from		we_Items " & vbcrlf &_
				"where		itemID = " & itemID
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		if not rs.eof then
			itemPic = rs("itemPic")
			randomEntry = randomEntry & "<br /><img src=""/productpics/big/" & itemPic & """ />"
		else
			itemPic = ""
		end if
	end if
	
%>
<style>
#divRevealEntry, #divTotalEntries, #divAllEntries {
	margin-bottom:3.0em;
}
#divRevealEntry {
	width:50%;
	float:right;
}
#divTotalEntries {
	width:50%;
	float:left;
}
#divAllEntries {
	float:left;
	clear:both;
}
.divSingleEntry {
	float:left;
	width:50%;	
	margin-bottom:3.0em;
}
.imgPic, .divCorruptEntry {
	float:left;
	margin-right:1.0em;
}
.slabel {
	font-weight:bold;
}
.divCorruptEntry {
	width:145px;
	height:300px;
	background:gray;
	border-radius: 10px;
	-moz-border-radius: 10px;
	-khtml-border-radius: 10px;
}
.divCorruptEntry p {
	color:white;
	line-height:50px;
	text-align:center;
	font-size:2.5em;
	font-style:italic;
	padding-top:75px;
}
</style>
<div style="width:85%;margin-left:auto;margin-right:auto;padding-top:3.0em">
	
    <h1><%=pageTitle%></h1>
 
    <div id="divTotalEntries">
        <h2>Total Number of Entries</h2>
        <p style="font-weight:bold;"><%=totalEntries%></p>
	</div>
    
    <div id="divRevealEntry">
        <h2>Select a Random Entry</h2>
        <p>Entries with corrupted images are still eligible</p>
        <p id="pEntry"><a href="javascript:revealEntry()">Click to reveal</a></p>
    </div>
    <div id="divAllEntries">
    	<h2>View All Entries</h2>
        
        <p>Entries from emails ending with @wirelessemporium.com are not shown.</p>
        <p><form name="allEntries" action="" method="post"><input name="showCorrupt" value="showCorrupt" type="checkbox" onchange="this.form.submit()"<% if showCorrupt then %> checked="checked"<% end if %> />
        <label for="showCorrupt">Show entries with corrupted photo</label><input type="hidden" name="submitted" value="submitted" /></form></p>
		<%
		sql = 	"select		c.id, c.email, c.itemID, c.entryDate, i.itemPic, i.brandID " & vbcrlf & _
				"from		CustomCaseContest c left join we_Items i " & vbcrlf & _
				"			on c.itemID = i.itemID " & vbcrlf & _
				"where		c.email not like '%@wirelessemporium.com' "
		'response.write(showCorrupt)
		'response.end
		if not showCorrupt then
			sql = sql & " and c.itemID != '138096' "
		end if
		sql = sql & " order by	c.entryDate asc "
		
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		cnt = 1
		if not rs.eof then
		do while not rs.eof
			id = rs("id")
			email = rs("email")
			itemID = rs("itemID")
			entryDate = rs("entryDate")
			itemPic = rs("itemPic")
			brandID = rs("brandID")
	
			%>    
			<div class="divSingleEntry">
				<% 'Corrupted Entry
				if itemPic = "WCD_7464150.png" then
				%><div class="divCorruptEntry"><p>No<br />Image<br/>Available</p></div><%
				else
				%><img src="/productpics/big/<%=itemPic%>" class="imgPic" /><%
				end if
				%>
                <p class="slabel">Email Address:</p>
                <p><%=email%></p>
                <p class="slabel">Entered on:</p>
                <p><%=entryDate%></p>
			</div>
   			<%
			rs.movenext
			cnt = cnt + 1
		loop
		end if
		%>
    </div>
</div>

<script>
function revealEntry() {
	document.getElementById('pEntry').innerHTML = '<%=randomEntry%>';
}
</script>


