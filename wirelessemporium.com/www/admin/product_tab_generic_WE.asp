<%
response.buffer = false
dim reportType, filename, ANDSQL
select case request.form("reportType")
	case "1"
		pageTitle = "Create Product List TXT for ALL WirelessEmporium.com Products"
		filename = "productList_ALL_WE.txt"
		ANDSQL = ""
	case "2"
		pageTitle = "Create Product List TXT for WirelessEmporium.com ACCESSORIES ONLY"
		filename = "productList_ACCESSORIES_WE.txt"
		ANDSQL = " AND A.typeID <> 16"
	case "3"
		pageTitle = "Create Product List TXT for WirelessEmporium.com PHONES ONLY"
		filename = "productList_PHONES_WE.txt"
		ANDSQL = " AND A.typeID = 16"
	case else
		pageTitle = "Create Generic WirelessEmporium Product List TXT File"
		filename = ""
		ANDSQL = ""
end select
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 9000 'seconds

dim fs, file, path
if filename <> "" then
	path = server.mappath("tempCSV") & "\" & filename
	path = replace(path,"admin\","")
end if
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p><%=pageTitle%></p>
							<p>
								<input type="radio" name="reportType" value="1"<%if request.form("reportType") <> "2" and request.form("reportType") <> "3" then response.write " checked"%>>ALL Products&nbsp;&nbsp;&nbsp;
								<input type="radio" name="reportType" value="2"<%if request.form("reportType") = "2" then response.write " checked"%>>Accessories ONLY&nbsp;&nbsp;&nbsp;
								<input type="radio" name="reportType" value="3"<%if request.form("reportType") = "3" then response.write " checked"%>>Phones ONLY
							</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							response.write "<b>CreateFile:</b><br>"
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='/tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
	
<%
sub DeleteFile(path)
	if path <> "" then
		'If the file already exists, delete it.
		if fs.FileExists(path) then
			response.write "Deleting " & filename & ".<br>"
			fs.DeleteFile(path)
		end if
	end if
end sub

sub CreateFile(path)
	if path <> "" then
		'Create the file and write some data to it.
		dim SQL, RS
		dim ModelName, BrandName, strItemLongDetail, strFeatures, Condition, strline, itemDesc
		response.write "Creating " & filename & ".<br>"
		set file = fs.CreateTextFile(path)
		SQL = "SELECT A.itemID, A.PartNumber, A.itemDesc, A.itemPic, A.price_Our, A.inv_qty, A.itemLongDetail,"
		SQL = SQL & " A.BULLET1, A.BULLET2, A.BULLET3, A.BULLET4, A.BULLET5, A.BULLET6, A.BULLET7, A.BULLET8, A.BULLET9, A.BULLET10,"
		SQL = SQL & " B.brandName, C.TypeName, D.ModelName"
		SQL = SQL & " FROM we_Items A INNER JOIN we_brands B ON A.brandID = B.brandID"
		SQL = SQL & " INNER JOIN we_types C ON A.typeID = C.typeID"
		SQL = SQL & " INNER JOIN we_models D ON A.modelID = D.modelID"
		SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty > 0 AND A.price_Our > 0"
		SQL = SQL & ANDSQL
		SQL = SQL & " ORDER BY A.itemID"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			file.WriteLine "id" & vbtab & "link" & vbtab & "price" & vbtab & "title" & vbtab & "description" & vbtab & "image_link" & vbtab & "brand" & vbtab & "condition" & vbtab & "part_number" & vbtab & "features"
			do until RS.eof
				if isnull(RS("modelName")) then
					ModelName = "Universal"
				else
					ModelName = RS("modelName")
				end if
				
				if isnull(RS("brandName")) then
					BrandName = "Universal"
				else 
					BrandName = RS("brandName")
				end if
				
				strItemLongDetail = replace(replace(RS("ItemLongDetail"),vbcrlf," "),vbtab," ")
				strItemLongDetail = replace(strItemLongDetail,"<a href='/downloads/","<a href='http://www.wirelessemporium.com/downloads/")
				
				strFeatures = "<ul>"
				if not isNull(RS("BULLET1")) and RS("BULLET1") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET1") & "</li>"
				if not isNull(RS("BULLET2")) and RS("BULLET2") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET2") & "</li>"
				if not isNull(RS("BULLET3")) and RS("BULLET3") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET3") & "</li>"
				if not isNull(RS("BULLET4")) and RS("BULLET4") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET4") & "</li>"
				if not isNull(RS("BULLET5")) and RS("BULLET5") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET5") & "</li>"
				if not isNull(RS("BULLET6")) and RS("BULLET6") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET6") & "</li>"
				if not isNull(RS("BULLET7")) and RS("BULLET7") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET7") & "</li>"
				if not isNull(RS("BULLET8")) and RS("BULLET8") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET8") & "</li>"
				if not isNull(RS("BULLET9")) and RS("BULLET9") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET9") & "</li>"
				if not isNull(RS("BULLET10")) and RS("BULLET10") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET10") & "</li>"
				strFeatures = strFeatures & "</ul>"
				strFeatures = replace(strFeatures,vbtab," ")
				if strFeatures = "<ul></ul>" then strFeatures = ""
				
				if inStr(Lcase(strItemLongDetail),"refurbished") > 0 or inStr(Lcase(RS("itemDesc")),"refurbished") > 0 or inStr(Lcase(strFeatures),"refurbished") > 0 then
					Condition = "refurbished"
				else
					Condition = "new"
				end if
				
				itemDesc = RS("itemDesc")
				
				strline = RS("itemid") & vbtab
				strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp" & vbtab
				strline = strline & RS("price_our") & vbtab
				strline = strline & itemDesc & vbtab
				strline = strline & strItemLongDetail & vbtab
				strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab
				strline = strline & BrandName & vbtab
				strline = strline & Condition & vbtab
				strline = strline & RS("PartNumber") & vbtab
				strline = strline & strFeatures
				file.WriteLine strline
				RS.movenext
			loop
		end if
		RS.close
		set RS = nothing
		file.close()
	end if
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
