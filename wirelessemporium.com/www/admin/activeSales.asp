<%
	thisUser = Request.Cookies("username")
	pageTitle = "Company Active Sales"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<style type="text/css">
	.boxTitle { display:block; background-color:#000; color:#FFF; font-weight:bold; text-align:center; padding:5px; font-size:18px; }
	.miniTitle { display:block; background-color:#CFF; color:#000; font-weight:bold; text-align:left; padding-left:3px; font-size:16px;}
	.stat1 { float:left; font-weight:bold; width:145px; text-align:right; padding-right:5px; font-size:14px;}
	.stat2 { float:left; font-weight:bold; width:100px; text-align:left; font-size:14px;}
</style>
<%
	dim incShip : incShip = prepInt(request.QueryString("incShip"))
	
	sql = "select count(*) as cnt, store from we_orders a left join we_accounts b on a.accountID = b.accountID where approved = 1 and cancelled is null and orderDateTime > '" & date-7 & "' and orderdatetime < '" & date & "' and email not like '%@wirelessemporium.com' and parentOrderID is null and (extOrderType is null or extOrderType < 4) group by store order by store"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	sql = "select sum(c.price_our * b.quantity) as weTotal, sum(c.price_ca * b.quantity) as caTotal, sum(c.price_co * b.quantity) as coTotal, sum(c.price_ps * b.quantity) as psTotal, sum((c.price_our - 5) * b.quantity) as tmTotal, sum(c.cogs * b.quantity) as cogs, a.store from we_orders a left join we_orderDetails b on a.orderID = b.orderID left join we_items c on b.itemID = c.itemID left join we_accounts d on a.accountID = d.accountID where a.approved = 1 and a.cancelled is null and a.orderdatetime > '" & date-7 & "' and a.orderdatetime < '" & date & "' and email not like '%@wirelessemporium.com' and parentOrderID is null and (extOrderType is null or extOrderType < 4) group by a.store"
	session("errorSQL") = sql
	set moneyRS = oConn.execute(sql)
	
	sql = "select sum(cast(ordershippingfee as money)) as shippingTotal, store from we_orders a left join we_accounts b on a.accountID = b.accountID where orderdatetime > '" & date-7 & "' and orderdatetime < '" & date & "' and approved = 1 and cancelled is null and email not like '%@wirelessemporium.com' and parentOrderID is null and (extOrderType is null or extOrderType < 4) group by store"
	session("errorSQL") = sql
	set shipRS = oConn.execute(sql)
	
	do while not rs.EOF
		if rs("store") = 0 then
			weAvgSales = formatNumber(rs("cnt") / 7,0)
		elseif rs("store") = 1 then
			caAvgSales = formatNumber(rs("cnt") / 7,0)
		elseif rs("store") = 2 then
			coAvgSales = formatNumber(rs("cnt") / 7,0)
		elseif rs("store") = 3 then
			psAvgSales = formatNumber(rs("cnt") / 7,0)
		elseif rs("store") = 10 then
			tmAvgSales = formatNumber(rs("cnt") / 7,0)
		end if
		rs.movenext
	loop
	
	do while not moneyRS.EOF
		if incShip = 1 then useShip = prepInt(shipRS("shippingTotal")) else useShip = 0
		if moneyRS("store") = 0 then
			if useShip > 0 then useShip = useShip - (weAvgSales * 3)
			weTotal = formatNumber((moneyRS("weTotal") + useShip) / 7,2)
			weCogs = formatNumber(moneyRS("cogs") / 7,2)
		elseif moneyRS("store") = 1 then
			if useShip > 0 then useShip = useShip - (caAvgSales * 3)
			caTotal = formatNumber((moneyRS("caTotal") + useShip) / 7,2)
			caCogs = formatNumber(moneyRS("cogs") / 7,2)
		elseif moneyRS("store") = 2 then
			if useShip > 0 then useShip = useShip - (coAvgSales * 3)
			coTotal = formatNumber((moneyRS("coTotal") + useShip) / 7,2)
			coCogs = formatNumber(moneyRS("cogs") / 7,2)
		elseif moneyRS("store") = 3 then
			if useShip > 0 then useShip = useShip - (psAvgSales * 3)
			psTotal = formatNumber((moneyRS("psTotal") + useShip) / 7,2)
			psCogs = formatNumber(moneyRS("cogs") / 7,2)
		elseif moneyRS("store") = 10 then
			if useShip > 0 then useShip = useShip - (tmAvgSales * 3)
			tmTotal = formatNumber((moneyRS("tmTotal") + useShip) / 7,2)
			tmCogs = formatNumber(moneyRS("cogs") / 7,2)
		end if
		moneyRS.movenext
		shipRS.movenext
	loop
%>
<table border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
    	<td>
        	<div style="width:250px; border:1px solid #000;">
	        	<div class="boxTitle">Wireless Emporium</div>
                <div class="miniTitle">Sales</div>
                <div style="float:left; background-color:#FFF;">
	                <div class="stat1">Avg This Week:</div>
                    <div class="stat2"><%=weAvgSales%></div>
                    <div class="stat1">Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="weYdaSales" class="stat2">???</div>
    	            <div class="stat1">Today:</div>
                    <div id="weSales" class="stat2">???</div>
                    <div class="stat1">Unique Visits:</div>
                    <div id="weUnique" class="stat2">???</div>
                    <div class="stat1">Visits Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="weTotal" class="stat2">???</div>
                </div>
                <div class="miniTitle">Profit</div>
                <div style="float:left; background-color:#FFF;">
	                <div class="stat1">Avg This Week:</div>
                    <div class="stat2"><%=formatCurrency(weTotal-weCogs)%></div>
                    <div class="stat1">Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="weYdaProfit" class="stat2">???</div>
    	            <div class="stat1">Today:</div>
                    <div id="weProfit" class="stat2">???</div>
                </div>
                <div style="display:block;">&nbsp;</div>
            </div>
        </td>
        <td>
        	<div style="width:250px; border:1px solid #000;">
	        	<div class="boxTitle">Cellular Outfitter</div>
                <div class="miniTitle">Sales</div>
                <div style="float:left; background-color:#FFF;">
	                <div class="stat1">Avg This Week:</div>
                    <div class="stat2"><%=coAvgSales%></div>
                    <div class="stat1">Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="coYdaSales" class="stat2">???</div>
    	            <div class="stat1">Today:</div>
                    <div id="coSales" class="stat2">???</div>
                    <div class="stat1">Unique Visits:</div>
                    <div id="coUnique" class="stat2">???</div>
                    <div class="stat1">Visits Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="coTotal" class="stat2">???</div>
                </div>
                <div class="miniTitle">Profit</div>
                <div style="float:left; background-color:#FFF;">
	                <div class="stat1">Avg This Week:</div>
                    <div class="stat2"><%=formatCurrency(coTotal-coCogs)%></div>
                    <div class="stat1">Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="coYdaProfit" class="stat2">???</div>
    	            <div class="stat1">Today:</div>
                    <div id="coProfit" class="stat2">???</div>
                </div>
                <div style="display:block;">&nbsp;</div>
            </div>
        </td>
        <td>
        	<div style="width:250px; border:1px solid #000;">
	        	<div class="boxTitle">Cellphone Accents</div>
                <div class="miniTitle">Sales</div>
                <div style="float:left; background-color:#FFF;">
	                <div class="stat1">Avg This Week:</div>
                    <div class="stat2"><%=caAvgSales%></div>
                    <div class="stat1">Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="caYdaSales" class="stat2">???</div>
    	            <div class="stat1">Today:</div>
                    <div id="caSales" class="stat2">???</div>
                    <div class="stat1">Unique Visits:</div>
                    <div id="caUnique" class="stat2">???</div>
                    <div class="stat1">Visits Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="caTotal" class="stat2">???</div>
                </div>
                <div class="miniTitle">Profit</div>
                <div style="float:left; background-color:#FFF;">
	                <div class="stat1">Avg This Week:</div>
                    <div class="stat2"><%=formatCurrency(caTotal-caCogs)%></div>
                    <div class="stat1">Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="caYdaProfit" class="stat2">???</div>
    	            <div class="stat1">Today:</div>
                    <div id="caProfit" class="stat2">???</div>
                </div>
                <div style="display:block;">&nbsp;</div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
        	<div style="width:250px; border:1px solid #000;">
	        	<div class="boxTitle">Phone Sale</div>
                <div class="miniTitle">Sales</div>
                <div style="float:left; background-color:#FFF;">
	                <div class="stat1">Avg This Week:</div>
                    <div class="stat2"><%=psAvgSales%></div>
                    <div class="stat1">Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="psYdaSales" class="stat2">???</div>
    	            <div class="stat1">Today:</div>
                    <div id="psSales" class="stat2">???</div>
                    <div class="stat1">Unique Visits:</div>
                    <div id="psUnique" class="stat2">???</div>
                    <div class="stat1">Visits Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="psTotal" class="stat2">???</div>
                </div>
                <div class="miniTitle">Profit</div>
                <div style="float:left; background-color:#FFF;">
	                <div class="stat1">Avg This Week:</div>
                    <div class="stat2"><%=formatCurrency(psTotal-psCogs)%></div>
                    <div class="stat1">Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="psYdaProfit" class="stat2">???</div>
    	            <div class="stat1">Today:</div>
                    <div id="psProfit" class="stat2">???</div>
                </div>
                <div style="display:block;">&nbsp;</div>
            </div>
        </td>
        <td>
        	<div style="width:250px; border:1px solid #000;">
	        	<div class="boxTitle">Tablet Mall</div>
                <div class="miniTitle">Sales</div>
                <div style="float:left; background-color:#FFF;">
	                <div class="stat1">Avg This Week:</div>
                    <div class="stat2"><%=tmAvgSales%></div>
                    <div class="stat1">Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="tmYdaSales" class="stat2">???</div>
    	            <div class="stat1">Today:</div>
                    <div id="tmSales" class="stat2">???</div>
                    <div class="stat1">Unique Visits:</div>
                    <div id="tmUnique" class="stat2">???</div>
                    <div class="stat1">Visits Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="tmTotal" class="stat2">???</div>
                </div>
                <div class="miniTitle">Profit</div>
                <div style="float:left; background-color:#FFF;">
	                <div class="stat1">Avg This Week:</div>
                    <div class="stat2"><%=formatCurrency(tmTotal-tmCogs)%></div>
                    <div class="stat1">Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="tmYdaProfit" class="stat2">???</div>
    	            <div class="stat1">Today:</div>
                    <div id="tmProfit" class="stat2">???</div>
                </div>
                <div style="display:block;">&nbsp;</div>
            </div>
        </td>
        <td>
        	<div style="width:250px; border:1px solid #000;">
	        	<div class="boxTitle" style="background-color:#F00;">Total</div>
                <div class="miniTitle" style="background-color:#FCC">Sales</div>
                <div style="float:left; background-color:#FFF;">
	                <div class="stat1">Avg This Week:</div>
                    <div class="stat2"><%=cdbl(weAvgSales)+cdbl(caAvgSales)+cdbl(coAvgSales)+cdbl(psAvgSales)+cdbl(tmAvgSales)%></div>
                    <div class="stat1">Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="totalYdaSales" class="stat2">???</div>
    	            <div class="stat1">Today:</div>
                    <div id="totalSales" class="stat2">???</div>
                </div>
                <div class="miniTitle" style="background-color:#FCC">Profit</div>
                <div style="float:left; background-color:#FFF;">
	                <div class="stat1">Avg This Week:</div>
                    <div class="stat2"><%=formatCurrency(weTotal-weCogs+coTotal-coCogs+caTotal-caCogs+psTotal-psCogs-tmCogs)%></div>
                    <div class="stat1">Last <%=weekdayname(weekday(now-7))%>:</div>
                    <div id="totalYdaProfit" class="stat2">???</div>
    	            <div class="stat1">Today:</div>
                    <div id="totalProfit" class="stat2">???</div>
                </div>
                <div style="display:block;">&nbsp;</div>
            </div>
        </td>
    </tr>
    <tr>
    	<td valign="top">
        	<div style="margin-top:10px; font-weight:bold;">Last Update:</div>
            <div style="margin-left:10px;" id="lastUpdateDate"></div>
            <% if incShip = 1 then %>
            <div style="margin-top:10px; width:100%; text-align:center;"><input type="button" name="myAction" value="Remove Shipping" onclick="window.location='/admin/activeSales.asp'" /></div>
			<% else %>
            <div style="margin-top:10px; width:100%; text-align:center;"><input type="button" name="myAction" value="Add Shipping" onclick="window.location='/admin/activeSales.asp?incShip=1'" /></div>
            <% end if %>
            <div style="margin-top:10px; width:100%; text-align:center;"><a href="/admin/detailedOrderCnt.asp">Switch to Daily Order Count</a></div>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	setTimeout("pullData()",1000)
	function pullData() {
		ajax('/ajax/admin/ajaxActiveSales.asp?incShip=<%=incShip%>','dumpZone')
		setTimeout("processReturn()",1000)
		setTimeout("pullData()",20000)
	}
	
	function processReturn() {
		var returnData = document.getElementById("dumpZone").innerHTML
		if (returnData.indexOf("##") > 0) {
			returnArray = returnData.split("##")
			document.getElementById("weSales").innerHTML = returnArray[0]
			document.getElementById("weProfit").innerHTML = returnArray[1]
			document.getElementById("weUnique").innerHTML = returnArray[2]
			document.getElementById("weTotal").innerHTML = returnArray[3]
			
			document.getElementById("coSales").innerHTML = returnArray[4]
			document.getElementById("coProfit").innerHTML = returnArray[5]
			document.getElementById("coUnique").innerHTML = returnArray[6]
			document.getElementById("coTotal").innerHTML = returnArray[7]
			
			document.getElementById("caSales").innerHTML = returnArray[8]
			document.getElementById("caProfit").innerHTML = returnArray[9]
			document.getElementById("caUnique").innerHTML = returnArray[10]
			document.getElementById("caTotal").innerHTML = returnArray[11]
			
			document.getElementById("psSales").innerHTML = returnArray[12]
			document.getElementById("psProfit").innerHTML = returnArray[13]
			document.getElementById("psUnique").innerHTML = returnArray[14]
			document.getElementById("psTotal").innerHTML = returnArray[15]
			
			document.getElementById("tmSales").innerHTML = returnArray[16]
			document.getElementById("tmProfit").innerHTML = returnArray[17]
			document.getElementById("tmUnique").innerHTML = returnArray[18]
			document.getElementById("tmTotal").innerHTML = returnArray[19]
			
			document.getElementById("totalSales").innerHTML = returnArray[20]
			document.getElementById("totalProfit").innerHTML = returnArray[21]
			
			document.getElementById("weYdaSales").innerHTML = returnArray[22]
			document.getElementById("weYdaProfit").innerHTML = returnArray[27]
			document.getElementById("coYdaSales").innerHTML = returnArray[23]
			document.getElementById("coYdaProfit").innerHTML = returnArray[28]
			document.getElementById("caYdaSales").innerHTML = returnArray[24]
			document.getElementById("caYdaProfit").innerHTML = returnArray[29]
			document.getElementById("psYdaSales").innerHTML = returnArray[25]
			document.getElementById("psYdaProfit").innerHTML = returnArray[30]
			document.getElementById("tmYdaSales").innerHTML = returnArray[26]
			document.getElementById("tmYdaProfit").innerHTML = returnArray[31]
			
			document.getElementById("totalYdaSales").innerHTML = returnArray[32]
			document.getElementById("totalYdaProfit").innerHTML = returnArray[33]
			document.getElementById("lastUpdateDate").innerHTML = returnArray[34]
		}
	}
</script>