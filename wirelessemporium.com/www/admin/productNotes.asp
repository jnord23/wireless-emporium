<%header = 0%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/admin/formatNotes.asp"-->

<%
dim partNumber, txtNotes
partNumber = Request.QueryString("partNumber")
if OrderID = "" then Request.Form("partNumber")
txtNotes = Request.Form("txtNotes")

if partNumber = "" then
	response.write "<b><font color='red'>MISSING PARTNUMBER. PLEASE TRY AGAIN.</font></b>"
end if

EditNotes = SQLquote(request.form("txtNotes"))
AddNotes = SQLquote(request.form("txtAddNotes"))

'response.write "EditNotes:<pre>" & EditNotes & "</pre>"
'response.write "AddNotes:<pre>" & AddNotes & "</pre>"

if AddNotes <> "" then EditNotes = EditNotes & vbcrlf & formatNotes(AddNotes) end if

'response.write "final:<pre>" & EditNotes & "</pre>"

dim strMsg, objRs
if Request.Form("hidSave") <> "" then
	SQL = "SELECT notes FROM we_partNumberNotes WHERE partNumber = '" & partNumber & "'"
	session("errorSQL") = SQL
	set objRs = oConn.execute(sql)
	if not objRs.EOF then
		SQL = "UPDATE we_partNumberNotes SET notes='" & SQLquote(EditNotes) & "' WHERE partNumber='" & partNumber & "'"
	else
		SQL = "INSERT INTO we_partNumberNotes(partNumber,notes) VALUES('" & partNumber & "','" & SQLquote(EditNotes) & "')"
	end if
	oConn.Execute SQL
	If oConn.Errors.Count > 0 Then
		strMsg = "Error while updating notes : " & Err.Number & " : " & Err.Description
	Else
		strMsg = "PartNumber notes updated succesfully."
	End If
	response.write "<script>self.close();</script>"
	objRs.Close
	Set objRs = Nothing
Else
	Set objRs = Server.CreateObject("ADODB.RECORDSET")
	SQL = "SELECT notes FROM we_partNumberNotes WHERE partNumber='" & partNumber & "'"
	objRs.Open SQL, oConn
	if not objRs.EOF then txtNotes = objRs.Fields("notes").value
	objRs.Close
	Set objRs = Nothing
End If
%>

<form name="frmordernotes" method="post">
	<table width="100%" border="1" cellspacing="0" cellpadding="2" align="center">
		<tr class="bigText" bgcolor="#CCCCCC">
			<td colspan="2" align="center"> Enter Notes For partNumber: <%=partNumber%></td>
		</tr>
		<%If strMsg <> "" Then%>
			<tr class="smlText">
				<td colspan="2" align="center"><b><%=strMsg%></b></td>
			</tr>
		<%End If%>
		<tr class="smlText">
			<td valign="top" align="right">Edit Notes :&nbsp;&nbsp;</td>
			<td valign="top" align="left"><textarea name="txtNotes" class="smltext" cols="80" rows="8" readonly="readonly"><%=Trim("" & txtNotes)%></textarea></td>
		</tr>
		<tr class="smlText">
			<td valign="top" align="right">Add Notes :&nbsp;&nbsp;</td>
			<td valign="top" align="left"><textarea name="txtAddNotes" class="smltext" cols="80" rows="7"></textarea></td>
		</tr>
		<tr class="smlText">
			<td colspan="2" align="center">
				<input type="submit" name="btnsave" class="smltext" value="Submit">&nbsp;&nbsp;
				<input type="button" name="btncancel" class="smltext" value="Cancel" onClick="self.close();">
				<input type="hidden" name="partNumber" value="<%=partNumber%>">
				<input type="hidden" name="hidsave" value="true">
			</td>
		</tr>
	</table>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
