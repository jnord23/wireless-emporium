<%
pageTitle = "Admin - Add / Edit Website Tasks"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->

<h3><%=pageTitle%></h3>
<h3><a href="website_tasks_view.asp">VIEW TASKS</a></h3>

<%
dim id, strError
id = request.querystring("id")
if not isNumeric(id) or id = "" then id = 0
strError = ""

if request.form("submitted") <> "" then
	newTaskID = 0
	id = BlankToZero(request.form("id"))
	site = request.form("site")
	title = SQLquote(request.form("title"))
	details = SQLquote(request.form("details"))
	priority = BlankToZero(request.form("priority"))
	TaskStatus = prepInt(request.form("TaskStatus"))
	notes = SQLquote(request.form("Notes"))
	AssignedTo = BlankToZero(request.form("AssignedTo"))
	EnteredBy = prepStr(request.form("EnteredBy"))
	isBug = prepInt(request.form("isBug"))
	estTime = prepStr(request.form("estTime"))
	
	if prepInt(request.form("DeleteParent")) = 1 or TaskStatus = 10 then
		sql = "select shovelReady, sortValue from we_Website_Tasks where id = " & id
		session("errorSQL") = sql
		set emailRS = oConn.execute(sql)
		
		shovelReady = emailRS("shovelReady")
		sortValue = emailRS("sortValue")		
		if not isnull(sortValue) then
			sql = "update we_Website_Tasks set shovelReady = 0, sortValue = null where id = " & id
			oConn.execute(sql)
			
			sql = "update we_Website_Tasks set sortValue = sortValue - 1 where shovelReady = 1 and sortValue is not null and sortValue > " & sortValue
			oConn.execute(sql)
		end if
	end if
	
	select case AssignedTo
		case "99" : AssignedName = "Unassigned"
		case "19" : AssignedName = "Darin"
		case "54" : AssignedName = "Nord"
		case "55" : AssignedName = "Sherwood"
		case "62" : AssignedName = "Terry"
		case "63" : AssignedName = "Michael B"
	end select
	if isDate(request.form("DateEntered")) then
		DateEntered = dateValue(request.form("DateEntered"))
	else
		strError = strError & "You must enter valid DateEntered Date for this task.<br>"
	end if
	if isDate(request.form("DateCompleted")) or TaskStatus = 10 then
		DateCompleted = "'" & date & "'"
		TaskStatus = "10"
		' Send an e-mail
		cdo_to = emailList("Project Complete")
		if instr(cdo_to, EnteredBy) = 0 then cdo_to = cdo_to & "," & EnteredBy
			
		cdo_from = "sales@WirelessEmporium.com"
		cdo_subject = "A Project Has Been Completed - " & title
		cdo_body = "<p>Completed Project #" & id & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Title:<br>" & title & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Details:<br>" & replace(details,vbcrlf,"<br>") & "', " & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Site: " & site & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Date Completed: " & replace(DateCompleted,"'","") & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Assigned To: " & AssignedName & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>This mail has been sent by : <font color=""#408080"">" & session("adminFullName") & " (" & adminID & ")</font></p>" & vbcrlf
					
		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		response.write "<h3>EMAIL SENT TO: " & cdo_to & "!</h3>"
	else
		if TaskStatus = "10" then
			DateCompleted = "'" & dateValue(date) & "'"
		else
			DateCompleted = "null"
		end if
	end if
	if SQLquote(request.form("RelatedID")) <> "" then
		RelatedID = "'" & SQLquote(request.form("RelatedID")) & "'"
	else
		RelatedID = "null"
	end if
	if title = "" then strError = strError & "You must enter a Title for this task.<br>"
	if strError = "" then
		if id = "0" then
			SQL = "SET NOCOUNT ON; "
			SQL = SQL & "INSERT INTO we_Website_Tasks (site, title, details, priority, status, AssignedTo, EnteredBy, DateEntered, DateCompleted, RelatedID, isBug, timeEstimate) VALUES ("
			SQL = SQL & "'" & site & "', "
			SQL = SQL & "'" & title & "', "
			SQL = SQL & "'" & details & "', "
			SQL = SQL & "'" & priority & "', "
			SQL = SQL & "'" & TaskStatus & "', "
			SQL = SQL & "'" & AssignedTo & "', "
			SQL = SQL & "'" & Request.Cookies("adminID") & "', "
			SQL = SQL & "'" & DateEntered & "', "
			SQL = SQL & DateCompleted & ", "
			SQL = SQL & RelatedID & ", "
			SQL = SQL & isBug & ",'" & estTime & "')"
			SQL = SQL & " SELECT @@IDENTITY AS newTaskID; "
			SQL = SQL & "SET NOCOUNT OFF;"
			set RS = oConn.execute(SQL)
			if not RS.eof then newTaskID = RS("newTaskID")
			response.write "<h3>NEW TASK ADDED!</h3>"
			' Send an e-mail
			select case AssignedTo
				case "99" : cdo_to = "darin@wirelessemporium.com,eugene@wirelessemporium.com,tony@wirelessemporium.com,terry@wirelessemporium.com,jon@wirelessemporium.com,scott@wirelessemporium.com"
				case "19" : cdo_to = "darin@wirelessemporium.com,eugene@wirelessemporium.com,tony@wirelessemporium.com,terry@wirelessemporium.com,jon@wirelessemporium.com,scott@wirelessemporium.com"
				case "54" : cdo_to = "eugene@wirelessemporium.com,tony@wirelessemporium.com,terry@wirelessemporium.com,jon@wirelessemporium.com,scott@wirelessemporium.com"
				case "55" : cdo_to = "sherwood@wirelessemporium.com,eugene@wirelessemporium.com,tony@wirelessemporium.com,terry@wirelessemporium.com,jon@wirelessemporium.com,scott@wirelessemporium.com"
				case "62" : cdo_to = "eugene@wirelessemporium.com,tony@wirelessemporium.com,terry@wirelessemporium.com,jon@wirelessemporium.com,scott@wirelessemporium.com"
				case "63" : cdo_to = "michaelb@wirelessemporium.com,eugene@wirelessemporium.com,tony@wirelessemporium.com,terry@wirelessemporium.com,jon@wirelessemporium.com,scott@wirelessemporium.com"
				case "74" : cdo_to = "eugene@wirelessemporium.com,tony@wirelessemporium.com,terry@wirelessemporium.com,jon@wirelessemporium.com,scott@wirelessemporium.com"
			end select
			
'			cdo_to = "terry@wirelessemporium.com"
			
			cdo_from = "sales@WirelessEmporium.com"
			cdo_subject = "A New Project Has Been Added - " & title
			cdo_body = "<p>New Project #" & newTaskID & "</p>" & vbcrlf
			cdo_body = cdo_body & "<p>Title:<br>" & title & "</p>" & vbcrlf
			cdo_body = cdo_body & "<p>Details:<br>" & replace(details,vbcrlf,"<br>") & "', " & "</p>" & vbcrlf
			cdo_body = cdo_body & "<p>Site: " & site & "</p>" & vbcrlf
			cdo_body = cdo_body & "<p>Priority: "
			if priority = "0" then
				cdo_body = cdo_body & "<font color=""" & bgcolor & """>" & "ALERT" & "</font>"
			else
				cdo_body = cdo_body & "<font color=""" & bgcolor & """>" & priority & "</font></p>" & vbcrlf
			end if
			cdo_body = cdo_body & "</p>" & vbcrlf
			cdo_body = cdo_body & "<p>Assigned To: " & AssignedName & "</p>" & vbcrlf
			cdo_body = cdo_body & "<p>This task has been created by <font color=""#408080"">" & session("adminFullName") & "</font></p>" & vbcrlf
						
			'CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
			'response.write "<h3>EMAIL SENT TO: " & cdo_to & "!</h3>"
		else
			SQL = "UPDATE we_Website_Tasks SET "
			if request.form("DeleteParent") = "1" then
				SQL = SQL & "deleted = -1 "
			else
				SQL = SQL & "site = '" & site & "', "
				SQL = SQL & "title = '" & title & "', "
				SQL = SQL & "details = '" & details & "', "
				SQL = SQL & "priority = '" & priority & "', "
				SQL = SQL & "status = '" & TaskStatus & "', "
				SQL = SQL & "AssignedTo = '" & AssignedTo & "', "
				SQL = SQL & "DateEntered = '" & DateEntered & "', "
				SQL = SQL & "DateCompleted = " & DateCompleted & ", "
				SQL = SQL & "RelatedID = " & RelatedID & ", "
				SQL = SQL & "notes = '" & notes & "', "
				SQL = SQL & "isBug = '" & isBug & "', "
				SQL = SQL & "timeEstimate = '" & estTime & "'"
			end if
			SQL = SQL & " WHERE id='" & id & "'"
			oConn.execute SQL
			response.write "<h3>TASK #" & id & " UPDATED!</h3>"
		end if
	else
		response.write "<p>" & strError & "</p>" & vbcrlf
	end if
	
	' Update Existing Sub-Task(s)
	if request.form("SubTaskTotal") > 0 then
		for a = 1 to request.form("SubTaskTotal")
			if request.form("delete" & a) = "1" then
				SQL = SQL & "DELETE FROM we_Website_Tasks WHERE id='" & request.form("SubTaskID" & a) & "'"
				'response.write "<p>" & SQL & "</p>"
				oConn.execute SQL
				response.write "<h3>SUB-TASK #" & a & " DELETED!</h3>"
			else
				title = SQLquote(request.form("title" & a))
				if title = "" then strError = strError & "You must enter a Title for Sub-Task #" & a & ".<br>"
				if strError = "" then
					priority = request.form("priority" & a)
					if SQLquote(request.form("details" & a)) <> "" then
						details = "'" & SQLquote(request.form("details" & a)) & "'"
					else
						details = "null"
					end if
					TaskStatus = BlankToZero(request.form("TaskStatus" & a))
					if isDate(request.form("DateEntered" & a)) then
						DateEntered = dateValue(request.form("DateEntered" & a))
					else
						DateEntered = date
					end if
					if isDate(request.form("DateCompleted" & a)) then
						DateCompleted = "'" & dateValue(request.form("DateCompleted" & a)) & "'"
					else
						DateCompleted = "null"
					end if
					if SQLquote(request.form("RelatedID")) <> "" then
						RelatedID = "'" & SQLquote(request.form("RelatedID")) & "'"
					else
						RelatedID = "null"
					end if
					SQL = "UPDATE we_Website_Tasks SET "
					SQL = SQL & "ParentID = '" & id & "', "
					SQL = SQL & "site = '" & site & "', "
					SQL = SQL & "title = '" & title & "', "
					SQL = SQL & "details = " & details & ", "
					SQL = SQL & "priority = '" & priority & "', "
					SQL = SQL & "status = '" & TaskStatus & "', "
					SQL = SQL & "AssignedTo = '" & AssignedTo & "', "
					SQL = SQL & "DateEntered = '" & DateEntered & "', "
					SQL = SQL & "DateCompleted = " & DateCompleted & ", "
					SQL = SQL & "RelatedID = " & RelatedID & ", "
					SQL = SQL & "timeEstimate = '" & estTime & "' "
					SQL = SQL & " WHERE id='" & request.form("SubTaskID" & a) & "'"
					oConn.execute SQL
					response.write "<h3>SUB-TASK #" & a & " UPDATED!</h3>"
				else
					response.write "<p>" & strError & "</p>" & vbcrlf
				end if
			end if
		next
	end if
	
	' Add New Sub-Task
	title = SQLquote(request.form("titleNEW"))
	if title <> "" then
		priority = request.form("priorityNEW")
		if SQLquote(request.form("detailsNEW")) <> "" then
			details = "'" & SQLquote(request.form("detailsNEW")) & "'"
		else
			details = "null"
		end if
		TaskStatus = BlankToZero(request.form("TaskStatusNEW"))
		if isDate(request.form("DateEnteredNEW")) then
			DateEntered = dateValue(request.form("DateEnteredNEW"))
		else
			DateEntered = date
		end if
		if isDate(request.form("DateCompletedNEW")) then
			DateCompleted = "'" & dateValue(request.form("DateCompletedNEW")) & "'"
		else
			DateCompleted = "null"
		end if
		if id = "0" then
			ParentID = newTaskID
		else
			ParentID = id
		end if
		if ParentID <> "0" then
			SQL = "INSERT INTO we_Website_Tasks (ParentID, site, title, details, priority, status, EnteredBy, AssignedTo, DateEntered, DateCompleted, timeEstimate) VALUES ("
			SQL = SQL & "'" & ParentID & "', "
			SQL = SQL & "'" & site & "', "
			SQL = SQL & "'" & title & "', "
			SQL = SQL & details & ", "
			SQL = SQL & "'" & priority & "', "
			SQL = SQL & "'" & TaskStatus & "', "
			SQL = SQL & "'" & Request.Cookies("adminID") & "', "
			SQL = SQL & "'" & AssignedTo & "', "
			SQL = SQL & "'" & DateEntered & "', "
			SQL = SQL & DateCompleted & ",'" & estTime & "')"
			oConn.execute SQL
			response.write "<h3>NEW SUB-TASK ADDED!</h3>"
		else
			response.write "<h3>UNABLE TO ADD NEW SUB-TASK!</h3>"
		end if
	end if
	response.write "<h3><a href=""javascript:history.back();"">BACK</a></h3>" & vbcrlf
else
	if id > 0 then
		response.write "<h2>Edit Task #" & id & "</h2>" & vbcrlf
'		SQL = "SELECT * FROM we_Website_Tasks WHERE id = '" & id & "'"
		sql	=	"select	a.*, isnull(b.email, '') enterdByEmail" & vbcrlf & _
				"from	we_Website_Tasks a left outer join we_adminusers b" & vbcrlf & _
				"	on	a.EnteredBy = b.adminID" & vbcrlf & _
				"where	id = '" & id & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if RS.eof then
			response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
			response.write "<a href=""db_update_coupons.asp"">Try again.</a>"
		else
			id = RS("id")
			site = RS("site")
			title = RS("title")
			details = RS("details")
			priority = RS("priority")
			TaskStatus = RS("status")
			AssignedTo = RS("AssignedTo")
			if isNull(AssignedTo) then AssignedTo = 0
			DateEntered = RS("DateEntered")
			DateCompleted = RS("DateCompleted")
			RelatedID = RS("RelatedID")
			notes = RS("notes")
			EnteredBy = RS("enterdByEmail")
			isBug = RS("isBug")
			estTime = RS("timeEstimate")
			
			select case priority
				case 0 : bgcolor = "FF0000"
				case 1 : bgcolor = "FFFF00"
				case 2 : bgcolor = "9999FF"
				case 3 : bgcolor = "00FF00"
			end select
			
			dim siteName, siteArray
			siteName = ""
			siteArray = split(site,", ")
			for b = 0 to Ubound(siteArray)
				select case siteArray(b)
					case "0" : siteName = siteName & "WE, "
					case "1" : siteName = siteName & "CA, "
					case "2" : siteName = siteName & "CO, "
					case "3" : siteName = siteName & "PS, "
					case "4" : siteName = siteName & "FG, "
					case "5" : siteName = siteName & "ER, "
					case else : siteName = siteName & siteArray(b) & ", "
				end select
			next
			if siteName = "" then
				siteName = site
			else
				siteName = left(siteName, len(siteName)-2)
			end if
		end if
		RS.close
		set RS = nothing
	else
		response.write "<h2>Add New Task</h2>" & vbcrlf
		DateEntered = date
		bgcolor = "FFFFFF"
	end if
	
	sql = "select adminID, fname, lname, department from we_adminUsers where (department = 'Programming' or department = 'Graphics' or taskList = 1) and active = 1 order by fname"
	session("errorSQL") = sql
	set usersRS = oConn.execute(sql)
	
	sql = "select * from xStore order by longDesc"
	session("errorSQL") = sql
	set siteRS = oConn.execute(sql)
	%>
	<table border="1" cellpadding="2" cellspacing="0" align="center" class="smlText" width="1000">
		<form action="website_tasks_add_edit.asp" name="EditForm" method="post">
		<tr>
			<td align="center" valign="top" width="100" bgcolor="#<%=bgcolor%>">
				<b>Priority</b><br>
				<select name="priority" class="smlText">
					<option value="1"<%if priority = 1 then response.write " selected"%>>1</option>
					<option value="2"<%if priority = 2 then response.write " selected"%>>2</option>
					<option value="3"<%if priority = 3 then response.write " selected"%>>3</option>
					<option value="0"<%if priority = 0 then response.write " selected"%>>ALERT</option>
				</select>
			</td>
			<td align="center" valign="top" width="100">
				<b>Assigned&nbsp;To</b><br>
				<select name="AssignedTo" class="smlText">
                	<%
					do while not usersRS.EOF
						useAdminID = prepInt(usersRS("adminID"))
						fname = prepStr(usersRS("fname"))
						lname = prepStr(usersRS("lname"))
						department = prepStr(usersRS("department"))
						if prepInt(AssignedTo) = 0 then AssignedTo = prepInt(session("SelectAssignedTo"))
						if (department <> "Programming") or adminID = 54 or adminID = 2 or adminID = 62 or adminID = 148 or useAdminID = adminID or useAdminID = AssignedTo then
					%>
                    <option value="<%=useAdminID%>"<%if prepInt(AssignedTo) = useAdminID then response.write " selected"%>><%=fname & " " & lname%></option>
                    <%
						end if
						usersRS.movenext
					loop
					%>
				</select>
			</td>
			<td valign="top" align="left" width="575">
				<b>Title</b><br><input type="text" name="title" value="<%=title%>" size="120" maxlength="250" class="smlText">
			</td>
			<td align="center" valign="top">
				<b>Status</b><br>
				<select name="TaskStatus" class="smlText">
					<option value="0"<%if TaskStatus = 0 then response.write " selected"%>>Delayed</option>
					<option value="1"<%if TaskStatus = 1 then response.write " selected"%>>Need Materials</option>
					<option value="2"<%if TaskStatus = 2 then response.write " selected"%>>In Progress</option>
					<option value="8"<%if TaskStatus = 8 then response.write " selected"%>>Testing</option>
					<option value="9"<%if TaskStatus = 9 then response.write " selected"%>>Awaiting Approval</option>
					<option value="10"<%if TaskStatus = 10 then response.write " selected"%>>Completed</option>
				</select>
			</td>
			<td align="center" valign="top">
				<b>Entered</b><br><input type="text" name="DateEntered" value="<%=DateEntered%>" size="10" maxlength="10" class="smlText">
			</td>
		</tr>
		<tr>
			<td align="center" valign="top" colspan="2">
				<b>Site</b><br>
                <%
				do while not siteRS.EOF
					shortDesc = siteRS("shortDesc")
					longDesc = siteRS("longDesc")
				%>
                <div style="float:left">
	                <div style="float:left;"><input type="checkbox" name="site" value="<%=shortDesc%>"<%if inStr(siteName,shortDesc) > 0 then response.write " checked"%>></div>
					<div style="float:left; padding:3px 5px 0px 3px;"><%=shortDesc%></div>
                </div>
                <%
					siteRS.movenext
				loop
				%>
                <div style="float:left">
	                <div style="float:left;"><input type="checkbox" name="site" value="Admin"<%if inStr(siteName,"Admin") > 0 then response.write " checked"%>></div>
					<div style="float:left; padding:3px 5px 0px 3px;">Admin</div>
                </div>
			</td>
			<td valign="top" align="left">
				<b>Details</b><br>
				<textarea name="details" rows="3" cols="120" class="smlText"><%=details%></textarea>
			</td>
			<td align="center" valign="top">
            	<div style="font-weight:bold; width:100%; text-align:center;">Related Task</div>
				<div><input type="text" name="RelatedID" value="<%=RelatedID%>" size="10" maxlength="10" class="smlText"></div>
                <div style="border-top:1px solid #000; margin-top:5px; padding-top:5px;">Is this a bug?</div>
                <div style="cursor:pointer;" onclick="isBug()">
                	<% if isBug then %>
                    	<img id="bugIcon" src="/images/icons/bug-icon.jpg" border="0" />
    	                <input type="hidden" name="isBug" value="1" />
                    <% else %>
	                    <img id="bugIcon" src="/images/icons/bug-icon_off.jpg" border="0" />
    	                <input type="hidden" name="isBug" value="0" />
                    <% end if %>
                </div>
			</td>
			<td align="center" valign="top">
				<b>Completed</b><br>
				<input type="text" name="DateCompleted" value="<%=DateCompleted%>" size="10" maxlength="10" class="smlText"><br>
				<input type="checkbox" name="DeleteParent" value="1">Delete
			</td>
		</tr>
        <tr>
			<td align="left" valign="top" colspan="8">
				<b>Estimated Time:</b>&nbsp;<input type="text" name="estTime" value="<%=estTime%>" size="42" />
			</td>
		</tr>
		<tr>
			<td align="left" valign="top" colspan="8">
				<b>Notes:</b><br>
				<textarea name="Notes" rows="2" cols="190" class="smlText"><%=notes%></textarea>
			</td>
		</tr>
		<%
		a = 0
		if id > 0 then
			SQL = "SELECT * FROM we_Website_Tasks WHERE ParentID = '" & id & "'"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			do until RS.eof
				a = a + 1
				%>
				<tr>
					<td align="center" colspan="8" bgcolor="#999999"><img src="/images/spacer.gif" width="1" height="2" border="0"></td>
				</tr>
				<tr>
					<td valign="middle" align="center" colspan="2">
						<b>Sub-Task #<%=a%>:</b>
					</td>
					<td valign="top" align="left">
						<input type="hidden" name="SubTaskID<%=a%>" value="<%=RS("id")%>">
						<b>Title</b><br><input type="text" name="title<%=a%>" value="<%=RS("title")%>" size="120" maxlength="250" class="smlText">
					</td>
					<td align="center" valign="top">
						<b>Status</b><br>
						<select name="TaskStatus<%=a%>" class="smlText">
							<option value="0"<%if RS("status") = 0 then response.write " selected"%>>Delayed</option>
							<option value="1"<%if RS("status") = 1 then response.write " selected"%>>Need Materials</option>
							<option value="2"<%if RS("status") = 2 then response.write " selected"%>>In Progress</option>
							<option value="8"<%if RS("status") = 8 then response.write " selected"%>>Testing</option>
							<option value="9"<%if RS("status") = 9 then response.write " selected"%>>Awaiting Approval</option>
							<option value="10"<%if RS("status") = 10 then response.write " selected"%>>Completed</option>
						</select>
					</td>
					<td align="center" valign="top">
						<b>Entered</b><br><input type="text" name="DateEntered<%=a%>" value="<%=RS("DateEntered")%>" size="10" maxlength="10" class="smlText">
					</td>
				</tr>
				<tr>
					<%
					select case RS("priority")
						case 0 : bgcolor = "FF0000"
						case 1 : bgcolor = "FFFF00"
						case 2 : bgcolor = "9999FF"
						case 3 : bgcolor = "00FF00"
					end select
					%>
					<td align="center" valign="top" colspan="2" bgcolor="#<%=bgcolor%>">
						<b>Priority</b><br>
						<select name="priority<%=a%>" class="smlText">
							<option value="1"<%if RS("priority") = 1 then response.write " selected"%>>1</option>
							<option value="2"<%if RS("priority") = 2 then response.write " selected"%>>2</option>
							<option value="3"<%if RS("priority") = 3 then response.write " selected"%>>3</option>
							<option value="0"<%if RS("priority") = 0 then response.write " selected"%>>ALERT</option>
						</select>
					</td>
					<td valign="top" align="left">
						<b>Details</b><br>
						<textarea name="details<%=a%>" rows="3" cols="120" class="smlText"><%=RS("details")%></textarea>
					</td>
					<td align="center" valign="top"><b>Delete</b><br>
						<input type="checkbox" name="delete<%=a%>" value="1">
					</td>
					<td align="center" valign="top">
						<b>Completed</b><br>
						<input type="text" name="DateCompleted<%=a%>" value="<%=RS("DateCompleted")%>" size="10" maxlength="10" class="smlText">
					</td>
				</tr>
				<%
				RS.movenext
			loop
			RS.close
			set RS = nothing
		end if
		%>
		<tr>
			<td align="center" colspan="8" bgcolor="#999999"><img src="/images/spacer.gif" width="1" height="2" border="0"></td>
		</tr>
		<tr>
			<td valign="middle" align="center" colspan="2">
				<b>Add New Sub-Task:</b>
			</td>
			<td valign="top" align="left">
				<b>Title</b><br><input type="text" name="titleNEW" size="120" maxlength="250" class="smlText">
			</td>
			<td align="center" valign="top">
				<b>Status</b><br>
				<select name="TaskStatusNEW" class="smlText">
					<option value="0" selected>Delayed</option>
					<option value="1">Need Materials</option>
					<option value="2">In Progress</option>
					<option value="8">Testing</option>
					<option value="9">Awaiting Approval</option>
					<option value="10">Completed</option>
				</select>
			</td>
			<td align="center" valign="top">
				<b>Entered</b><br><input type="text" name="DateEnteredNEW" value="<%=date%>" size="10" maxlength="10" class="smlText">
			</td>
		</tr>
		<tr>
			<td align="center" valign="top" colspan="2" bgcolor="#FFFFFF">
				<b>Priority</b><br>
				<select name="priorityNEW" class="smlText">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="0">ALERT</option>
				</select>
			</td>
			<td valign="top" align="left">
				<b>Details</b><br>
				<textarea name="detailsNEW" rows="3" cols="120" class="smlText"></textarea>
			</td>
			<td align="center" valign="top">&nbsp;
				
			</td>
			<td align="center" valign="top">
				<b>Completed</b><br>
				<input type="text" name="DateCompletedNEW" size="10" maxlength="10" class="smlText">
			</td>
		</tr>
		<tr>
			<td align="center" colspan="8">
				<input type="hidden" name="id" value="<%=id%>">
				<input type="hidden" name="SubTaskTotal" value="<%=a%>">
				<input type="submit" name="submitted" value="Update">
			</td>
		</tr>
        <input type="hidden" name="enteredby" value="<%=EnteredBy%>">
		</form>
	</table>
	<%
end if

function BlankToZero(val)
	if val = "" then
		BlankToZero = "0"
	else
		BlankToZero = val
	end if
end function
%>

<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function isBug() {
		var curBugImg = document.getElementById("bugIcon").src
		if (curBugImg.indexOf("bug-icon.jpg") > 0) {
			document.getElementById("bugIcon").src = curBugImg.replace(".jpg","_off.jpg")
			document.EditForm.isBug.value = 0
		}
		else {
			document.getElementById("bugIcon").src = curBugImg.replace("_off.jpg",".jpg")
			document.EditForm.isBug.value = 1
		}
	}
</script>