<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Vendor Part Numbers Entry"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	partType = ds(request.Form("partType"))
	partNumberLike = ds(request.Form("partNumberLike"))
	brandID = ds(request.Form("brandID"))
	vendorCode = ds(request.Form("vendorCode"))
	ignoreVendor = ds(request.Form("ignoreVendor"))
	allItems = ds(request.Form("allItems"))
	hideLive = ds(request.Form("hideLive"))
	partNumber = ds(request.QueryString("partNumber"))
	
	if isnull(partType) or len(partType) < 1 then partType = 0
	if isnull(partNumberLike) or len(partNumberLike) < 1 then partNumberLike = ""
	if isnull(brandID) or len(brandID) < 1 then brandID = 0
	if isnull(vendorCode) or len(vendorCode) < 1 then vendorCode = ""
	if isnull(ignoreVendor) or len(ignoreVendor) < 1 then ignoreVendor = 0
	if isnull(allItems) or len(allItems) < 1 then allItems = 0
	if isnull(hideLive) or len(hideLive) < 1 then hideLive = 0
	if isnull(partNumber) or len(partNumber) < 1 then partNumber = ""
	
	if partNumber <> "" then
		partNumberLike = partNumber
		allItems = 1
	end if
	
	userMsg = ""
	
	if vendorCode <> "" then
		session("vendorCode") = vendorCode
	else
		session("vendorCode") = null
	end if
	
	if vendorCode <> "" then
		if ignoreVendor = 0 then vendSelect = "and b.vendor = '" & vendorCode & "' "
	end if
	if partType > 0 then
		showProducts = 1
		
		sql = "select a.itemPic, a.itemID, a.vendor, (select sum(ia.quantity) from we_orderDetails ia left join we_orders ib on ia.orderID = ib.orderID left join we_items ic on ia.itemID = ic.itemID where ib.approved = 1 and ib.orderdatetime > '" & date - 30 & "' and ic.partNumber = a.partNumber) as sold, b.id, a.inv_qty, a.itemID, a.itemDesc, a.partNumber, a.vendor, b.vendor as vendorCode, b.partNumber as vendorNumber from we_items a left join we_vendorNumbers b on a.partNumber = b.we_partNumber " & vendSelect & "where a.hideLive = " & hideLive & " and a.inv_qty > -1 and a.typeID = " & partType & " and a.partNumber not like '%-MLD-%' and a.partNumber not like '%-SKN-%' order by a.partNumber"
		if brandID > 0 then sql = replace(sql,"order by","and a.brandID = " & brandID & " order by")
		if vendorCode <> "" and ignoreVendor = 0 then sql = replace(sql,"order by","and a.vendor like '%" & vendorCode & "%' order by")
	elseif partNumberLike <> "" then
		showProducts = 1
		partNumberLike = replace(partNumberLike,"%","")
		
		sql = "select a.itemPic, a.itemID, a.vendor, (select sum(ia.quantity) from we_orderDetails ia left join we_orders ib on ia.orderID = ib.orderID left join we_items ic on ia.itemID = ic.itemID where ib.approved = 1 and ib.orderdatetime > '" & date - 30 & "' and ic.partNumber = a.partNumber) as sold, (select top 1 itemDesc from we_items where partNumber = a.partNumber) as itemDesc, b.id, a.inv_qty, a.partNumber, b.vendor as vendorCode, b.partNumber as vendorNumber from we_items a left join we_vendorNumbers b on a.partNumber = b.we_partNumber " & vendSelect & "where a.hideLive = " & hideLive & " and a.inv_qty > -1 and a.partnumber like '%" & partNumberLike & "%' and a.partNumber not like '%-MLD-%' and a.partNumber not like '%-SKN-%' order by a.partNumber"
		'sql = "select b.id, a.inv_qty, a.itemID, a.itemDesc, a.partNumber, a.vendor, b.vendor as vendorCode, b.partNumber as vendorNumber from we_items a left join we_vendorNumbers b on a.partNumber = b.we_partNumber " & vendSelect & "where a.inv_qty > -1 and a.partnumber like '%" & partNumberLike & "%' and a.partNumber not like '%-MLD-%' and a.partNumber not like '%-SKN-%' order by a.partNumber"
		if brandID > 0 then sql = replace(sql,"order by","and a.brandID = " & brandID & " order by")
		if vendorCode <> "" and ignoreVendor = 0 then sql = replace(sql,"order by","and a.vendor like '%" & vendorCode & "%' order by")
	elseif vendorCode <> "" then
		showProducts = 1
				
		sql = "select a.itemPic, a.itemID, a.vendor, (select sum(ia.quantity) from we_orderDetails ia left join we_orders ib on ia.orderID = ib.orderID left join we_items ic on ia.itemID = ic.itemID where ib.approved = 1 and ib.orderdatetime > '" & date - 30 & "' and ic.partNumber = a.partNumber) as sold, b.id, a.inv_qty, a.itemID, a.itemDesc, a.partNumber, a.vendor, b.vendor as vendorCode, b.partNumber as vendorNumber from we_items a left join we_vendorNumbers b on a.partNumber = b.we_partNumber " & vendSelect & "where a.hideLive = " & hideLive & " and a.inv_qty > -1 and a.vendor like '%" & vendorCode & "%' and a.partNumber not like '%-MLD-%' and a.partNumber not like '%-SKN-%' order by a.partNumber"
		if brandID > 0 then sql = replace(sql,"order by","and a.brandID = " & brandID & " order by")
	end if
	if not isnull(sql) and len(sql) > 0 then
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
	end if
	
	sql = "select typeName, typeID from we_types where typeID <> 15 and typeID <> 9 order by typeName"
	session("errorSQL") = sql
	Set typeRS = Server.CreateObject("ADODB.Recordset")
	typeRS.open sql, oConn, 0, 1
	
	sql = "select * from we_brands order by brandName"
	session("errorSQL") = sql
	Set brandRS = Server.CreateObject("ADODB.Recordset")
	brandRS.open sql, oConn, 0, 1
	
	sql = "select name, code from we_vendors order by code"
	session("errorSQL") = sql
	Set vendorRS = Server.CreateObject("ADODB.Recordset")
	vendorRS.open sql, oConn, 0, 1
	
	pullOrder = 0
	if vendorCode <> "" then
		sql = "select * from we_vendorOrder where vendorCode = '" & vendorCode & "' and process = 0 order by partNumber"
		session("errorSQL") = sql
		Set vOrderRS = Server.CreateObject("ADODB.Recordset")
		vOrderRS.open sql, oConn, 0, 1
		
		if vOrderRS.EOF then pullOrder = 0 else pullOrder = 1
	end if
%>
<div style="position:relative;">
<table align="center" border="0" cellpadding="3" cellspacing="0" width="600">
	<tr><td style="padding:10px">&nbsp;</td></tr>
    <tr>
    	<td align="center" style="border-bottom:1px solid #000; border-top:1px solid #000; padding-top:20px;">
        	<form name="searchForm" method="post" action="/admin/vendorPartNumbers_enter.asp">
        	<table border="0" cellpadding="3" cellspacing="0">
                <tr>
                	<td align="right" nowrap="nowrap" style="font-weight:bold;">Part Type:</td>
                    <td align="left">
                    	<select name="partType">
                        	<option value="">Select Product Type</option>
							<% session("errorSQL") = "select type" %>
							<% do while not typeRS.EOF %>
                            <option value="<%=typeRS("typeID")%>"<% if typeRS("typeID") = partType then %> selected="selected"<% end if %>><%=typeRS("typeName")%></option>
                            <%
								typeRS.movenext
							loop
							%>
                        </select>
                    </td>
                </tr>
                <tr>
                	<td align="right" nowrap="nowrap" style="font-weight:bold;">Part Number:</td>
                    <td align="left"><input type="text" name="partNumberLike" value="<%=partNumberLike%>" /></td>
                </tr>
                <tr>
                	<td align="right" nowrap="nowrap" style="font-weight:bold;">Brand:</td>
                    <td align="left">
                    	<select name="brandID">
                        	<option value="">Select Brand</option>
							<% session("errorSQL") = "select brand" %>
							<% do while not brandRS.EOF %>
                            <option value="<%=brandRS("brandID")%>"<% if cdbl(brandRS("brandID")) = cdbl(brandID) then %> selected="selected"<% end if %>><%=brandRS("brandName")%></option>
                            <%
								brandRS.movenext
							loop
							%>
                        </select>
                    </td>
                </tr>
                <tr>
                	<td align="right" nowrap="nowrap" style="font-weight:bold;">Vendor:</td>
                    <td align="left">
                    	<select name="vendorCode" onchange="vendorSelect(this.value)">
                        	<option value="">Select Vendor</option>
							<% session("errorSQL") = "select vendor" %>
							<% do while not vendorRS.EOF %>
                            <option value="<%=vendorRS("code")%>"<% if vendorRS("code") = vendorCode then %> selected="selected"<% end if %>><%=vendorRS("name")%></option>
                            <%
								vendorRS.movenext
							loop
							vendorRS.movefirst
							%>
                        </select>
                    </td>
                </tr>
                <tr>
                	<td>&nbsp;</td>
                    <td align="left" style="font-size:9px;"><input type="checkbox" name="allItems" value="1"<% if allItems = 1 then %> checked="checked"<% end if %> />&nbsp;Show all items</td>
                </tr>
                <tr>
                	<td>&nbsp;</td>
                    <td align="left" style="font-size:9px;"><input type="checkbox" name="hideLive" value="1"<% if hideLive = 1 then %> checked="checked"<% end if %> />&nbsp;Show Hidden items</td>
                </tr>
                <tr id="ignoreRow"<% if vendorCode = "" then %> style="display:none;"<% end if %>>
                	<td>&nbsp;</td>
                    <td align="left" style="font-size:9px;"><input type="checkbox" name="ignoreVendor" value="1"<% if ignoreVendor = 1 then %> checked="checked"<% end if %> />&nbsp;Ignore vendor while pulling products (ordering only)</td>
                </tr>
                <tr>
                	<td align="left"><input type="button" name="myAction" value="Clear Form" onclick="window.location='/admin/vendorPartNumbers_enter.asp'" /></td>
                    <td align="right"><input type="submit" name="mySub" value="Search" /></td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr><td align="center" style="font-weight:bold; color:#F00;" id="userMsgCell"><%=userMsg%></td></tr>
    <% if showProducts = 1 then %>
    <tr>
    	<td>
        	<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0">
            	<tr style="font-weight:bold; color:#FFF; background-color:#333;">
                	<td align="left" nowrap="nowrap">Part Number</td>
                    <td align="left">Item</td>
                    <td align="left">Vendors</td>
                    <td align="left" nowrap="nowrap">Vendor Part Numbers</td>
                    <td align="left">Inv</td>
                    <td align="left">Sold</td>
                    <% if vendorCode <> "" then %>
                    <td align="left" nowrap="nowrap">Order Amt</td>
                    <% end if %>
                </tr>
                <%
				bgColor = "#ffffff"
				lap = 0
				session("errorSQL") = "start cycle"
				
				do while not rs.EOF
					lap = lap + 1
					curID = rs("itemID")
					if isnull(rs("vendor")) or len(rs("vendor")) < 1 then vendor = "NA" else vendor = rs("vendor")
					curPN = rs("partNumber")
					curDesc = rs("itemDesc")
					inventory = rs("inv_qty")
					itemPic  = rs("itemPic")
					if isnull(rs("sold")) then sold = 0 else sold = rs("sold")
					vendorPN = ""
					useVendorPN = ""
					useQty = ""
					if allItems = 1 then invLimit = 999999 else invLimit = 11
					if inventory < invLimit or sold >= inventory then
						if pullOrder = 1 then
							if not vOrderRS.EOF then
								if vOrderRS("partNumber") = rs("partNumber") then
									useQty = vOrderRS("orderAmt")
									vOrderRS.movenext
								end if
							end if
						end if
						
						session("errorSQL") = "inner cycle"
						do while curPN = rs("partNumber")
							if not isnull(rs("vendorNumber")) or len(rs("vendorNumber")) > 0 then
								useVendorPN = rs("vendorNumber")
								if vendorPN = "" then vendorPN = "<a onclick=""editVendorPN(" & rs("id") & ",'" & rs("vendorNumber") & "','" & rs("vendorCode") & "'," & lap & "," & curID & ",'" & curPN & "','" & prepVal(replace(replace(curDesc,"'",""),"""","")) & "')"" style='cursor:pointer; color:#0000ff;'>" & rs("vendorNumber") & " (" & rs("vendorCode") & ")</a>" else vendorPN = vendorPN & "<br /><a onclick=""editVendorPN(" & rs("id") & ",'" & rs("vendorNumber") & "','" & rs("vendorCode") & "'," & lap & "," & curID & ",'" & curPN & "','" & ds(replace(curDesc,"'","")) & "')"" style='cursor:pointer; color:#0000ff;'>" & rs("vendorNumber") & " (" & rs("vendorCode") & ")</a>"
							end if
							rs.movenext
							if rs.EOF then exit do
						loop
						if isnull(vendorPN) or len(vendorPN) < 1 then vendorPN = "&nbsp;"
				%>
                <tr bgcolor="<%=bgColor%>" style="font-size:12px;" style="cursor:pointer;" onmouseover="this.style.backgroundColor='#9999ff';" onmouseout="this.style.backgroundColor='';">
                	<td align="left" nowrap="nowrap" valign="top">
						<div style="width:120px;">
                            <a onclick="document.getElementById('<%=curPN%>_pic').style.display=''" style="cursor:pointer; color:#00F; font-size:12px;"><%=curPN%></a>
                            <div id="<%=curPN%>_pic" style="display:none; position:relative; cursor:pointer;">
                                <div style="position:absolute; top:0px; left:0px;" onclick="document.getElementById('<%=curPN%>_pic').style.display='none'"><img src="/productPics/big/<%=itemPic%>" border="0" /></div>
                            </div>
                        </div>
                    </td>
                	<td onclick="showPop(<%=lap%>,<%=curID%>,'<%=curPN%>','<%=ds(replace(curDesc,"'",""))%>')" align="left" nowrap="nowrap" valign="top"><%=curDesc%></td>
                    <td onclick="showPop(<%=lap%>,<%=curID%>,'<%=curPN%>','<%=ds(replace(curDesc,"'",""))%>')" align="left" valign="top"><%=vendor%></td>
                    <td align="left" id="<%=curID%>_vendNum"><%=vendorPN%></td>
                    <td align="center" valign="top"><%=inventory%></td>
                    <td align="center" valign="top" style="color:#F00;"><%=sold%></td>
                    <% if vendorCode <> "" then %>
                    <td align="center" valign="top"><input type="text" name="orderAmt" value="<%=useQty%>" size="3" onchange="ajax('/ajax/admin/saveVendorNum.asp?ourPN=<%=curPN%>&vendorPN=<%=useVendorPN%>&orderAmt=' + this.value,'dumpZone')" /></td>
                    <% end if %>
                </tr>
                <%
						if bgColor = "#ffffff" then bgColor = "#f0f0f0" else bgColor = "#ffffff"
					else
						session("errorSQL") = "cycle partNumber"
						do while curPN = rs("partNumber")
							rs.movenext
							if rs.EOF then exit do
						loop
						lap = lap - 1
					end if
					response.Flush()
				loop
				%>
                <tr bgcolor="#333333">
                	<td colspan="4" align="right">&nbsp;</td>
                    <% if vendorCode <> "" then %>
                    <td colspan="3" align="right"><input type="button" name="myAction" value="Review Order" onclick="window.location='/admin/vendorOrders.asp?vendorCode=<%=vendorCode%>'" /></td>
                    <% else %>
                    <td colspan="3" align="right">&nbsp;</td>
                    <% end if %>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td align="left"><%=formatNumber(lap,0)%> Items Pulled</td></tr>
    <% end if %>
</table>
<div id="popUp" style="position:absolute; top:150px; right:500px; border:2px groove; z-index:99; display:none; background-color:#FFF;">
    <table border="0" cellpadding="3" cellspacing="0" width="300" style="border:2px groove #000; ">
    	<form action="#" onsubmit="return(chkForm())" method="post" name="newPnForm">
    	<tr bgcolor="#000000">
        	<td align="left" style="font-weight:bold; color:#FFF;" nowrap="nowrap">Enter Vendor Code</td>
            <td align="right"><a style="cursor:pointer;" onclick="document.getElementById('popUp').style.display='none'">Close</a></td>
        </tr>
        <tr>
        	<td valign="top" style="font-weight:bold;" align="right">Description:</td>
            <td id="itemDesc"></td>
        </tr>
        <tr>
        	<td valign="top" style="font-weight:bold;" align="right">Our Part Number:</td>
            <td id="ourPN"></td>
        </tr>
        <tr>
        	<td valign="top" style="font-weight:bold;" align="right">Vendor:</td>
            <td align="left" width="100%">
            	<select name="vendorCode">
                	<option value="">Select Vendor</option>
                    <%
					vCnt = 0
					session("errorSQL") = "select vendor 2"
					do while not vendorRS.EOF
						vCnt = vCnt + 1
					%>
                    <option value="<%=vendorRS("code")%>"><%=vendorRS("code")%> - <%=vendorRS("name")%></option>
                    <%
						vendorRS.movenext
					loop
					%>
                </select>
            </td>
        </tr>
        <tr>
        	<td nowrap="nowrap" valign="top" style="font-weight:bold;" align="right">Vendor Part Number:</td>
        	<td id="vendorPnCell"><input type="text" name="vendorPN" value="" /></td>
        </tr>
        <tr>
        	<td align="left">
            	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                	<tr>
                    	<td align="left"><input type="button" name="cancelBttn" value="Cancel" onclick="document.getElementById('popUp').style.display='none'" /></td>
                        <td align="right" id="deleteOp" style="display:none;"><input type="button" name="deleteBttn" value="Delete" onclick="deleteRecord()" /></td>
                    </tr>
                </table>
            </td>
        	<td align="right">
            	<input type="button" name="subBttn" value="Save" onclick="chkForm()" />
                <input type="hidden" name="ourPN" value="" />
                <input type="hidden" name="itemID" value="" />
                <input type="hidden" name="weDesc" value="" />
                <input type="hidden" name="lap" value="" />
                <input type="hidden" name="editID" value="" />
                <input type="hidden" name="partType" value="<%=partType%>" />
            </td>
        </tr>
        </form>
    </table>
</div>
</div>
<% session("errorSQL") = "bottom" %>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<% session("errorSQL") = "javascript" %>
<script language="javascript">
	var myY = 0
	function showPop(lap,itemID,wePN,weDesc) {
		document.getElementById("ourPN").innerHTML = wePN
		document.getElementById("itemDesc").innerHTML = weDesc
		document.newPnForm.ourPN.value = wePN
		document.newPnForm.itemID.value = itemID
		document.newPnForm.weDesc.value = weDesc
		document.newPnForm.lap.value = lap
		document.getElementById("popUp").style.top = myY
		document.newPnForm.vendorCode.selectedIndex = 0
		document.newPnForm.vendorPN.value = ""
		document.newPnForm.editID.value = 0
		document.getElementById("deleteOp").style.display = 'none'
		document.getElementById("popUp").style.display = ''
		document.newPnForm.subBttn.value = "Save"
	}
	
	function editVendorPN(dbID,vpn,vc,lap,itemID,wePN,weDesc) {
		document.getElementById("ourPN").innerHTML = wePN
		document.getElementById("itemDesc").innerHTML = weDesc
		document.newPnForm.ourPN.value = wePN
		document.newPnForm.itemID.value = itemID
		document.getElementById("popUp").style.top = myY
		//vCnt
		for (i=0;i<=<%=vCnt%>;i++) {
			curOption = document.newPnForm.vendorCode.options[i].text
			if (curOption.substring(0,vc.length) == vc) {
				document.newPnForm.vendorCode.selectedIndex = i
				i = <%=vCnt%> + 1
			}
		}
		document.newPnForm.vendorPN.value = vpn
		document.newPnForm.editID.value = dbID
		document.getElementById("deleteOp").style.display = ''
		document.getElementById("popUp").style.display = ''
		document.newPnForm.subBttn.value = "Edit"
	}
	
	function deleteRecord() {
		var qsVal = "?editID=" + document.newPnForm.editID.value + "&action=Delete&weDesc=" + document.newPnForm.weDesc.value + "&lap=" + document.newPnForm.lap.value + "&itemID=" + document.newPnForm.itemID.value + "&ourPN=" + document.newPnForm.ourPN.value + "&vendorCode=" + document.newPnForm.vendorCode.value + "&vendorPN=" + document.newPnForm.vendorPN.value
		ajax('/ajax/admin/saveVendorNum.asp' + qsVal,document.newPnForm.itemID.value + '_vendNum')
		document.getElementById("popUp").style.display = 'none'
		window.location = "#" + document.newPnForm.itemID.value + '_vendNum'
	}
	
	function chkForm() {
		if (document.newPnForm.vendorCode.value == "") {
			alert("You must select a vendor")
			document.newPnForm.vendorCode.focus()
			return false
		}
		else if (document.newPnForm.vendorPN.value == "") {
			alert("You must enter a vendor part number")
			document.newPnForm.vendorPN.focus()
			return false
		}
		else {
			sendData()
			return false
		}
	}
	
	function sendData() {
		var qsVal = "?editID=" + document.newPnForm.editID.value + "&action=" + document.newPnForm.subBttn.value + "&weDesc=" + document.newPnForm.weDesc.value + "&lap=" + document.newPnForm.lap.value + "&itemID=" + document.newPnForm.itemID.value + "&ourPN=" + document.newPnForm.ourPN.value + "&vendorCode=" + document.newPnForm.vendorCode.value + "&vendorPN=" + document.newPnForm.vendorPN.value
		ajax('/ajax/admin/saveVendorNum.asp' + qsVal,document.newPnForm.itemID.value + '_vendNum')
		document.getElementById("popUp").style.display = 'none'
		window.location = "#" + document.newPnForm.itemID.value + '_vendNum'
	}
	
	function mouseY(evt) {
		if (!evt) evt = window.event;
		if (evt.pageY) return evt.pageY; else if (evt.clientY)return evt.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop); else return 0;
	}
	
	document.onmousemove = follow;
	
	function follow(evt) {
		myY = (parseInt(mouseY(evt))) - 350
	}
	
	function vendorSelect(val) {
		if (val == "") {
			document.getElementById("ignoreRow").style.display = "none"
		}
		else {
			document.getElementById("ignoreRow").style.display = ""
		}
	}
</script>