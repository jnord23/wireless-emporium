<%
pageTitle = "Admin - Update WE Database - Update META Tags"
header = 1
skipLogin = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<blockquote>

<%
' **** SQL to find DUP entries: ****
'SELECT id, typeid, brandid, modelid, title, description, keywords FROM we_MetaTags
'WHERE (modelid IN
'(SELECT modelid FROM we_MetaTags AS Tmp
'GROUP BY modelid, brandid, typeid
'HAVING (COUNT(*) > 1) AND (brandid = we_MetaTags.brandid) AND (typeid = we_MetaTags.typeid OR typeid IS NULL)))
'ORDER BY modelid, brandid, typeid, id

if request("submitted") = "Update" then
	strError = ""
	BrandID = stripHTML(request.form("BrandID"))
	ModelID = stripHTML(request.form("ModelID"))
	TypeID = stripHTML(request.form("TypeID"))
	title = stripHTML(request.form("title"))
	strDescription = stripHTML(request.form("strDescription"))
	keywords = lCase(stripHTML(request.form("keywords")))
	
	if BrandID = "" then
		strError = strError & "<li>You must enter a Brand ID.</li>"
	else
		SQL = "SELECT BrandID FROM we_Brands WHERE BrandID='" & BrandID & "'"
		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if RS.eof then strError = strError & "<li>You must enter a <b>valid</b> Brand ID.</li>"
	end if
	
	if TypeID <> "" then
		SQL = "SELECT TypeID FROM we_Types WHERE TypeID='" & TypeID & "'"
		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if RS.eof then strError = strError & "<li>You must enter a <b>valid</b> Type ID.</li>"
	end if
	
	if ModelID = "" then
		strError = strError & "<li>You must enter a Model ID.</li>"
	else
		SQL = "SELECT ModelID FROM we_Models WHERE ModelID='" & ModelID & "'"
		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if RS.eof then strError = strError & "<li>You must enter a <b>valid</b> Model ID.</li>"
	end if
	
	if title = "" then strError = strError & "<li>You must enter a META TITLE.</li>"
	if strDescription = "" then strError = strError & "<li>You must enter a META DESCRIPTION.</li>"
	if keywords = "" then strError = strError & "<li>You must enter META KEYWORDS.</li>"
	
	if strError = "" then
		SQL = "SELECT id FROM we_MetaTags WHERE ModelID='" & ModelID & "' AND BrandID='" & BrandID & "'"
		if TypeID = "" then
			SQL = SQL & " AND TypeID IS NULL"
		else
			SQL = SQL & " AND TypeID='" & TypeID & "'"
		end if
		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if RS.eof then
			SQL = "INSERT INTO we_MetaTags (TypeID,BrandID,ModelID,title,description,keywords) VALUES ("
			if TypeID = "" then
				SQL = SQL & "null,"
			else
				SQL = SQL & "'" & TypeID & "',"
			end if
			SQL = SQL & "'" & BrandID & "',"
			SQL = SQL & "'" & ModelID & "',"
			SQL = SQL & "'" & SQLquote(title) & "',"
			SQL = SQL & "'" & SQLquote(strDescription) & "',"
			SQL = SQL & "'" & SQLquote(keywords) & "')"
		else
			SQL = "UPDATE we_MetaTags SET"
			if TypeID = "" then
				SQL = SQL & " TypeID=null,"
			else
				SQL = SQL & " TypeID='" & TypeID & "',"
			end if
			SQL = SQL & " BrandID='" & BrandID & "',"
			SQL = SQL & " ModelID='" & ModelID & "',"
			SQL = SQL & " title='" & SQLquote(title) & "',"
			SQL = SQL & " description='" & SQLquote(strDescription) & "',"
			SQL = SQL & " keywords='" & SQLquote(keywords) & "'"
			SQL = SQL & " WHERE id='" & RS("id") & "'"
		end if
		'response.write "<p>" & SQL & "</p>"
		oConn.execute SQL
		response.write "<h3>ADDED!</h3>"
		response.write "<p><form name=""back"" action=""metadata_d.asp""><input type=""submit"" value=""ADD ANOTHER""></form></p>"
	else
		response.write "<p><font color=""#FF0000"" face=""Arial,Helvetica""><ul>" & strError & "</ul></font></p>"
		response.write "<p><form name=""back""><input type=""button"" onclick=""history.back();"" value=""BACK""></form></p>"
	end if
else
	%>
	<form name="AddMetaData" action="metadata_d.asp" method="post">
		<table border="1" cellpadding="3" cellspacing="0" width="800">
			<tr>
				<td align="center"><font color="#FF0000" size="+1"><b>*&nbsp;</b></font>Brand&nbsp;ID&nbsp;&nbsp;<input type="text" name="BrandID" size="4" maxlength="2"></td>
				<td align="center">Type&nbsp;ID&nbsp;&nbsp;<input type="text" name="TypeID" size="4" maxlength="2"></td>
				<td align="center"><font color="#FF0000" size="+1"><b>*&nbsp;</b></font>Model&nbsp;ID&nbsp;&nbsp;<input type="text" name="ModelID" size="4" maxlength="3"></td>
			</tr>
			<tr>
				<td align="center" colspan="3"><b>Enter CONTENT ONLY!&nbsp;&nbsp;&nbsp;Do not enter HTML code such as &quot;&lt;TITLE&gt;&quot; or &quot;&lt;META&gt;&quot;!</b></td>
			</tr>
			<tr>
				<td align="left" colspan="3"><font color="#FF0000" size="+1"><b>*&nbsp;</b></font>Title:<br><input type="text" name="title" size="150" maxlength="200"></td>
			</tr>
			<tr>
				<td align="left" colspan="3"><font color="#FF0000" size="+1"><b>*&nbsp;</b></font>Description:<br><input type="text" name="strDescription" size="150" maxlength="300"></td>
			</tr>
			<tr>
				<td align="left" colspan="3"><font color="#FF0000" size="+1"><b>*&nbsp;</b></font>Keywords:<br><input type="text" name="keywords" size="150" maxlength="425"></td>
			</tr>
		</table>
		<p><font color="#FF0000"><font size="+1"><b>*&nbsp;</font>required fields</b></font><br>(only Type ID is not required)</p>
		<p><input type="submit" name="submitted" value="Update"></p>
	</form>
	<%
end if
%>

</blockquote>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->

<%
function stripHTML(myStr)
	myStr = replace(replace(replace(replace(replace(replace(myStr,chr(34),""),"�",""),"�",""),"<",""),">",""),"  "," ")
	myStr = replace(replace(replace(replace(replace(replace(replace(myStr,"/title","",1,9,1),"title","",1,9,1),"description","",1,9,1),"keywords","",1,9,1),"name=","",1,9,1),"content=","",1,9,1),"meta","",1,9,1)
	stripHTML = trim(myStr)
end function
%>
