<%
response.buffer = false
pageTitle = "Admin - Update WE Database - Update Temp Values"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<%
if request("submitted") = "Update" then
	for a = 1 to request("totalCount")
		if isNumeric(request("temp" & a)) then
			temp = request("temp" & a)
		else
			temp = 0
		end if
		SQL = "UPDATE WE_Models SET temp = '" & temp & "' WHERE modelID='" & request("modelID" & a) & "'"
		response.write "<p>" & SQL & "</p>"
		oConn.execute SQL
	next
	%>
	<h3>UPDATED!</h3>
	<%
else
	dim brandid, orderby
	brandid = request.querystring("brandid")
	select case request.querystring("orderby")
		case 1 : orderby = "A.modelName"
		case else : orderby = "A.temp"
	end select
	SQL = "SELECT A.modelID, A.modelName, A.temp, B.brandName FROM we_Models A INNER JOIN we_Brands B ON A.brandID = B.brandID"
	SQL = SQL & " WHERE B.brandID = '" & brandid & "' ORDER BY " & orderby
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	a = 0
	if RS.eof then
	    response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
	else
		%>
		<font face="Arial,Helvetica"><h3><%=RS.recordcount%> items found</h3></font>
		<form action="db_update_temps.asp" method="post">
			<table border="1" cellpadding="3" cellspacing="0" width="1000">
				<tr>
					<td align="left" width="10"><b>Model&nbsp;ID</b></td>
					<td align="left" width="50"><b>Brand</b></td>
					<td align="left" width="100"><a href="db_update_temps.asp?brandid=<%=brandid%>&orderby=1"><b>Model&nbsp;Name</b></a></td>
					<td align="left" width="10"><a href="db_update_temps.asp?brandid=<%=brandid%>&orderby=2"><b>temp</b></a></td>
				</tr>
				<%
				do until RS.eof
					a = a + 1
					%>
					<tr>
						<td align="left">
							<input type="hidden" name="modelID<%=a%>" value="<%=RS("modelID")%>">
							<%=RS("modelID")%>
						</td>
						<td align="left"><%=RS("brandName")%></td>
						<td align="left"><%=RS("modelName")%></td>
						<td align="left"><input type="text" name="temp<%=a%>" size="2" value="<%=RS("temp")%>" maxlength="4"></td>
					</tr>
					<%
					RS.movenext
				loop
				%>
			</table>
			<p>
				<input type="submit" name="submitted" value="Update">
				<input type="hidden" name="totalCount" value="<%=a%>">
			</p>
		</form>
		<p>&nbsp;</p>
		<%
	end if
	RS.close
	set RS = nothing
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
