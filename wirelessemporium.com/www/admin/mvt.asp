<%
	thisUser = Request.Cookies("username")
	pageTitle = "In-House MVT"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<div id="fullContent" style="width:100%; margin-left:auto; margin-right:auto;">
	<div id="mainSelect" style="height:20px; width:800px; margin-left:auto; margin-right:auto;">
        <div style="float:left;">
        	<select name="siteSelect" onchange="getProjects(this.value)">
            	<option value="">Select Website</option>
                <option value="0">WirelessEmporium</option>
                <option value="2">CellularOutfitter</option>
                <option value="1">CellphoneAccents</option>
                <option value="3">PhoneSale</option>
            </select>
        </div>
        <div id="projectList" style="float:left;"></div>
    </div>
    <div id="projectDetails" style="height:20px; margin-top:10px;"></div>
</div>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function getProjects(siteID) {
		ajax('/ajax/ajaxMvt.asp?siteID=' + siteID,'projectList')
		document.getElementById("projectDetails").innerHTML = ""
	}
	
	function projSelect(siteID,projID) {
		ajax('/ajax/ajaxMvt.asp?siteID=' + siteID + '&projID=' + projID,'projectDetails')
	}
	
	function createProject(siteID,name,testPage) {
		if (name == "" || testPage == "") {
			alert("You must enter a project name and test page")
		}
		else {
			ajax('/ajax/ajaxMvt.asp?siteID=' + siteID + '&projName=' + name + '&testPage=' + testPage,'fullContent')
		}
	}
	
	function cancelProject(projID) {
		var confirmAction = window.confirm("Are you sure you want to cancel this project?")
		if (confirmAction) {
			ajax('/ajax/ajaxMvt.asp?cancelID=' + projID,'fullContent')
		}
	}
	
	function comboActivation(comboID,isChecked) {
		ajax('/ajax/ajaxMvt.asp?comboActivationID=' + comboID + '&isChecked=' + isChecked,'dumpZone')
	}
	
	function showVarients(varientOption,projID,siteID) {
		ajax('/ajax/ajaxMvt.asp?siteID=' + siteID + '&projID=' + projID + '&showVarients=' + varientOption,'projectDetails')
	}
	
	function updateLiveSite(projID,liveSite) {
		ajax('/ajax/ajaxMvt.asp?adjustLiveSetting=1&liveSite=' + liveSite + '&projID=' + projID,'dumpZone')
		if (liveSite == 1) {
			alert('The experiment is now live!')
		}
		else {
			alert("The experiment is only on staging now")
		}
	}
	
	function updateStagingSite(projID,stagingSite) {
		ajax('/ajax/ajaxMvt.asp?adjustStagingSetting=1&stagingSite=' + stagingSite + '&projID=' + projID,'dumpZone')
		if (stagingSite == 1) {
			alert('The experiment is active on staging!')
		}
		else {
			alert("The experiment is inactive on staging and live")
		}
	}
	
	function updateTestType(projID,testType) {
		ajax('/ajax/ajaxMvt.asp?adjustTestType=1&testType=' + testType + '&projID=' + projID,'projectDetails')
	}
	
	function newPageA(projID,pageName) {
		ajax('/ajax/ajaxMvt.asp?newPageA=1&pageName=' + pageName + '&projID=' + projID,'dumpZone')
	}
	
	function newLinkPage(projID,pageName) {
		ajax('/ajax/ajaxMvt.asp?newLinkPage=1&pageName=' + pageName + '&projID=' + projID,'dumpZone')
	}
	
	function addObject(projID) {
		var objName = document.newObjectForm.newObjName.value
		var objOpts = document.newObjectForm.newObjOpts.value
		ajax('/ajax/ajaxMvt.asp?projID=' + projID + '&newObjName=' + objName + '&newObjOpts=' + objOpts,'projectDetails')
	}
	
	function removeObject(projID,objID) {
		ajax('/ajax/ajaxMvt.asp?projID=' + projID + '&delObjID=' + objID,'projectDetails')
	}
</script>