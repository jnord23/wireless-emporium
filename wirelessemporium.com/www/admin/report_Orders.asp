<%
pageTitle = "Admin - Generate Orders Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" width="750" align="center" cellpadding="0" cellspacing="0"><tr><td>
<p>&nbsp;</p>
<p class="biggerText"><%=pageTitle%></p>
<br>

<%
strError = ""

if request("submitted") <> "" then
	StartDate = request.form("dc1")
	EndDate = request.form("dc2")
	if not isDate(StartDate) then
		strError = "Start Date must be a valid date."
	else
		StartDate = dateValue(StartDate)
	end if
	if not isDate(EndDate) then
		strError = "End Date must be a valid date."
	else
		EndDate = dateValue(EndDate)
	end if
	if strError = "" then
		if StartDate > EndDate then
			strError = "Start Date must be earlier than or equal to End Date."
		end if
	end if
	if strError = "" then
		response.write "<h3>" & StartDate & "&nbsp;to&nbsp;" & EndDate & "</h3>" & vbcrlf
		
		for a = 0 to 3
			select case a
				case 0 : site = "WE"
				case 1 : site = "CA"
				case 2 : site = "CO"
				case 3 : site = "PS"
			end select
			
			SQL2 = "SELECT sum(a.refundamount) as refundAmount "
			SQL2 = SQL2 & " FROM we_orders A INNER JOIN " & site & "_accounts B ON A.accountid = B.accountid"
			SQL2 = SQL2 & " WHERE A.refunddate >= '" & StartDate & "' AND A.refunddate < '" & dateAdd("D",1,EndDate) & "'"
			SQL2 = SQL2 & " AND A.refundAmount IS NOT NULL"
			SQL2 = SQL2 & " AND (extOrderType IS NULL OR extOrderType <= 3)"
			SQL2 = SQL2 & " AND (B.email = 'phoneorder@wirelessemporium.com' OR B.email NOT LIKE '%@wirelessemporium.com')"
			SQL2 = SQL2 & " AND a.parentorderid is null"
			SQL2 = SQL2 & " AND A.store = '" & a & "'"
			'Response.Write(SQL2)
			set RS2 = Server.CreateObject("ADODB.Recordset")
			RS2.open SQL2, oConn, 3, 3
			if rs2.eof then 
			refundAmount = 0
			elseif isnull( rs2("refundAmount")) then
			refundAmount = 0 
			else
			refundAmount = rs2("refundAmount")
			end if
			
			SQL = "SELECT A.ordergrandtotal, A.cancelled, A.refundAmount, A.refundDate"
			SQL = SQL & " FROM we_orders A INNER JOIN " & site & "_accounts B ON A.accountid = B.accountid"
			SQL = SQL & " WHERE A.orderdatetime >= '" & StartDate & "' AND A.orderdatetime < '" & dateAdd("D",1,EndDate) & "'"
			SQL = SQL & " AND A.approved = 1"
			SQL = SQL & " AND (extOrderType IS NULL OR extOrderType <= 3)"
			SQL = SQL & " AND (B.email = 'phoneorder@wirelessemporium.com' OR B.email NOT LIKE '%@wirelessemporium.com')"
			SQL = SQL & " AND a.parentorderid is null"			
			SQL = SQL & " AND A.store = '" & a & "'"
			'response.write "<p>" & SQL & "</p>"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			
			response.write "<h3>" & site & ":</h3>" & vbcrlf
			
			dim GrandTotal, cancelled, cancelledTotal, refundAmount
			GrandTotal = 0
			cancelled = 0
			cancelledTotal = 0
			
			
			if not RS.eof then
				%>
				<table border="1" width="100%" cellpadding="2" cellspacing="0">
					<tr>
						<td><b>GROSS&nbsp;TOTAL</b></td>
						<td><b>Total&nbsp;Orders</b></td>
						<td><b>Cancelled</b></td>
						<td><b>Cancelled&nbsp;Total</b></td>
						<td><b><a href="/admin/report_refundreason.asp?dc1=<%=StartDate%>&dc2=<%=EndDate%>">Refund&nbsp;Amount</a> <a href="#" title="processed refund between the chosen date">?</a></b></td>
						<td><b>NET&nbsp;TOTAL</b></td>
					</tr>
					<%
					do until RS.eof
						GrandTotal = GrandTotal + cDbl(RS("ordergrandtotal"))
						if RS("cancelled") = true then
							cancelled = cancelled + 1
							cancelledTotal = cancelledTotal + cDbl(RS("ordergrandtotal"))
						end if
						'if not isNull(RS("refundAmount")) and RS("refundDate") >= StartDate  AND RS("refundDate") <  dateAdd("D",1,EndDate) then 
						'	refundAmount = refundAmount + RS("refundAmount")
						'end if
						



						
						
						
						
						RS.movenext
					loop
					%>
					<tr>
						<td align="center"><b><%=formatCurrency(GrandTotal)%></b></td>
						<td><b><%=RS.recordcount%></b></td>
						<td><b><%=cancelled%></b></td>
						<td><b><%=formatCurrency(cancelledTotal)%></b></td>
						<td><b><%=formatCurrency(refundAmount)%></b></td>
						<td><b><%=formatCurrency(GrandTotal - cancelledTotal - refundAmount)%></b></td>
					</tr>
				</table>
				<%
			else
				response.write "<p class=""boldText"">No records matched.</p><p class=""boldText"">No report generated.</p>"
			end if
			response.write "<p>&nbsp;</p>"
		next
		
		response.write "<p>&nbsp;</p>"
		response.write "<p class=""normalText""><a href=""report_Orders.asp"">Generate another Orders Report</a></p>"
		RS.close
		set RS = nothing
	end if
end if
if request("submitted") = "" or strError <> "" then
	%>
	<p class="normalText"><font color="#FF0000"><b><%=strError%></b></font></p>
	<form action="report_Orders.asp" name="frmOrdersReport" method="post">
		<p class="normalText">
			<input type="text" name="dc1" value="<%=dateAdd("d",-1,date)%>">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.frmOrdersReport.dc1,document.frmOrdersReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;Start Date
			<br>
			<input type="text" name="dc2" value="<%=dateAdd("d",-1,date)%>">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fEndPop(document.frmOrdersReport.dc1,document.frmOrdersReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;End Date
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
		</p>
		<hr>
		<p class="normalText">
			<input type="submit" name="submitted" value="Submit">
		</p>
	</form>
	<%
end if
%>

</td></tr></table>

<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
