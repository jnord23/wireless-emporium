<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - update website materials(images)"
	header = 1
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = true
myCount = Upload.Save

url = prepStr(Upload.form("txtURL"))
newpic = prepStr(Upload.form("newpic"))
strSubmit = prepStr(Upload.form("submit"))
if strSubmit <> "" then
	filepath = replace(replace(replace(replace(replace(url, "http://www.wirelessemporium.com", ""), "http://www.cellularoutfitter.com", ""), "http://www.cellphoneaccents.com", ""), "http://www.phonesale.com", ""), "http://www.tabletmall.com", "")
	filepath = server.MapPath(filepath)
	
	set oNewpic = Upload.files("newpic")
	if not oNewpic is nothing then
		savepath = filepath
'		savepath = replace(replace(replace(savepath, ".png", "-test.png"), ".jpg", "-test.jpg"), ".gif", "-test.gif")
		session("errorSQL") = "savepath:" & savepath
		oNewpic.SaveAs savepath
	end if
end if
%>
<form name="frm" method="post" enctype="multipart/form-data">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="1000">
	<tr>
    	<td id="terryZone">&nbsp;</td>
    </tr>
	<tr>
    	<td><h1 style="margin:0px;">Update Website Images</h1></td>
    </tr>
    <tr>
    	<td>URL: <input type="text" name="txtURL" value="" size="50" /></td>
    </tr>
    <tr>
    	<td>New Image: <input type="file" name="newpic" size="30"></td>
    </tr>
    <tr>
    	<td><input type="submit" name="submit" value="Update Images" onClick="return onSubmit();" /></td>
    </tr>
<!--    
    <tr>
    	<td style="padding-bottom:5px;">
        	<br /><br />
        	<span style="font-size:16px;">URL Reference</span> <br />
            <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
                <tr>
                    <td width="100" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-on" id="id_header_0" onClick="showHide(0);">WE</td>
                    <td width="100" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-off" id="id_header_1" onClick="showHide(1);">CA</td>
                    <td width="100" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-off" id="id_header_2" onClick="showHide(2);">CO</td>
                    <td width="100" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-off" id="id_header_3" onClick="showHide(3);">PS</td>
                    <td width="100" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-off" id="id_header_4" onClick="showHide(4);">TM</td>
                    <td width="500" class="tab-header-off">&nbsp;</td>
                </tr>
            </table>
		</td>
    </tr>
    <tr>
    	<td width="100%" align="left">
	        <div id="tbl_0">
                <div style="width:95%; margin-top:20px; font-size:15px; font-weight:bold; color:#494949; border:1px solid #ccc; background-color:#ebebeb; text-align:left; height:25px; padding:10px 0px 0px 10px">TOP NAV.</div>
				<form name="frm0" method="post" enctype="multipart/form-data">                
                	<input type="submit" name="submit" value="Update Images" onClick="return onSubmit();" />
                <%
				sql = 	"select	typeid, subtypeid, typename, subtypename, topNavImg, subtypeImg" & vbcrlf & _
						"from	v_subtypematrix" & vbcrlf & _
						"order by typeid, subtypeid"
				set rs = oConn.execute(sql)
				curTopCategoryID = 0
				lastTopCategoryID = 0
				%>
				<div style="display:none;">
                <%
				picCount = -1
				do until rs.eof
					curSubCategoryID = rs("subtypeid")
					curTopCategoryID = rs("typeid")
					if curTopCategoryID <> lastTopCategoryID then
						picCount = picCount + 1
					%>
				</div>
                <div style="margin-top:3px; width:95%; height:1px; border-top:3px dotted #ccc;"></div>
                <div style="display:inline-block; padding-bottom:20px;">
                    <div style="float:left; margin-right:10px; margin-top:5px; font-size:16px; padding:10px; width:200px; height:210px; border:1px solid #E95516;" align="center">
                        <%=rs("typename")%><br />
                        <img src="http://www.wirelessemporium.com/images/template/top/<%=rs("topNavImg")%>" border="0" alt="<%=rs("typename")%>" /><br />
                        <input type="file" id="id_pic_<%=picCount%>" name="topPic<%=curTopCategoryID%>" size="10">
                    </div>
                    <%
					end if

					picCount = picCount + 1
					%>
                    <div style="float:left; margin-right:10px; margin-top:5px; font-size:16px; padding:10px; width:200px; height:210px; border:1px solid #ccc;" align="center">
                        <%=rs("subtypename")%><br />
                        <img src="http://www.wirelessemporium.com/images/template/top/subCats/<%=rs("subtypeImg")%>" border="0" alt="<%=rs("subtypename")%>" /><br />
                        <input type="file" id="id_pic_<%=picCount%>" name="subPic<%=curSubCategoryID%>" size="10">
                    </div>
                    <%
					lastTopCategoryID = rs("typeid")
					rs.movenext
				loop
				%>
				</div>
                <div style="margin-top:3px; width:95%; height:1px; border-top:3px dotted #ccc;"></div>
                <div style="display:inline-block; padding-bottom:20px;">
	                <input type="submit" name="submit" value="Update Images" onClick="return onSubmit();" />
                </div>
                	<input type="hidden" name="hidsiteid" value="0" />
                </form>
            </div>
	        <div id="tbl_1" style="display:none;"></div>
	        <div id="tbl_2" style="display:none;"></div>
	        <div id="tbl_3" style="display:none;"></div>
	        <div id="tbl_4" style="display:none;"></div>
        </td>
    </tr>
    -->
</table>
</form>
<script>
	function onSubmit()
	{
		if (confirm('You are about to update live site images, continue?')) return true;
		return false;
	}
	
	function showHide(showID)
	{
		for (i=0; i<=4; i++)
		{
			document.getElementById('id_header_' + i).className = "tab-header-off";
			document.getElementById('tbl_' + i).style.display = "none";			
		}
				
		document.getElementById('id_header_' + showID).className = "tab-header-on";
		document.getElementById('tbl_' + showID).style.display = "";
	}
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->