<%
pageTitle = "Admin - Report the Last Date an Item Was Purchased"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<p>&nbsp;</p>
<p class="biggerText"><%=pageTitle%></p>
<br>
<%
if request.form("PartNumber") <> "" then
	dim PartNumber
	PartNumber = request.form("PartNumber")
	SQL = "SELECT * FROM we_orders WHERE orderid = "
	SQL = SQL & "(SELECT TOP (1) orderid FROM we_orderdetails WHERE (itemid IN "
	SQL = SQL & "(SELECT TOP (100) PERCENT itemID FROM we_Items WHERE (PartNumber = '" & PartNumber & "') ORDER BY we_orderdetails.orderdetailid DESC)))"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if RS.eof then
		response.write "No records matched<br><br>So cannot make table..."
	else
		%>
		<h3><%=RS.recordcount%> records found</h3>
		<table border="1">
			<tr>
				<td><b>Order&nbsp;ID</b></td>
				<td><b>Order&nbsp;Date/Time</b></td>
				<td><b>Order&nbsp;Grand&nbsp;Total</b></td>
			</tr>
			<%
			do until RS.eof
				%>
				<tr>
					<td valign="top"><%=RS("orderid")%></td>
					<td valign="top"><%=RS("orderdatetime")%></td>
					<td valign="top"><%=formatCurrency(RS("ordergrandtotal"))%></td>
				</tr>
				<%
				RS.movenext
			loop
			%>
		</table>
		<%
	end if
	RS.close
	set RS = nothing
end if
%>

<form action="report_ItemLastPurchasedDate.asp" method="post">
	<p>Part Number: <input type="text" name="PartNumber" value="<%=request.form("PartNumber")%>"></p>
	<p><input type="submit" name="submitted" value="Submit"></p>
</form>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
