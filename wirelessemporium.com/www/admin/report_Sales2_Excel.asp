<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
strSDate = prepStr(request.querystring("txtSDate"))
strEDate = prepStr(request.querystring("txtEDate"))
siteid = prepStr(request.querystring("cbSite"))
brandid = prepInt(request.querystring("cbBrand"))
modelid = prepInt(request.querystring("cbModel"))
typeid = prepInt(request.querystring("cbType"))
strGroupBy = prepStr(request.querystring("cbGroupBy"))
partnumber = prepStr(request.querystring("txtPartnumber"))
shiptype = prepStr(request.querystring("cbShiptype"))
addressDiff = prepStr(request.querystring("addressDiff"))
orderType = prepInt(request.querystring("cbOrderType"))

if strSDate = "" or not isdate(strSDate) then strSDate = Date
if strEDate = "" or not isdate(strEDate) then strEDate = Date

Dim cfgArrGroupBy : cfgArrGroupBy = Array("Date","Store","Brand","Model","Category","PartNumber","OrderID","ItemID","Details")
if "" = strGroupBy then 
	strGroupBy = "Date,Store,,,,,,," 
end if
arrGroupBy = split(strGroupBy, ",")

dim strSqlSelectMain, strSqlWhere, strSqlFrom, strSqlGroupBy, strSqlOrderBy, strHeadings, strFooter
strSqlSelectMain	=	""
strSqlWhere			=	""
strSqlFrom			=	""
strSqlGroupBy		=	""
strSqlOrderBy		=	""
strHeadings			=	""
strFooter			=	""

Dim bDate, bStore, bBrand, bModel, bCategory, bPartnumber, bItemid, bDetails, bOrderID
bDate		=	false
bStore		=	false
bBrand		=	false
bModel		=	false
bCategory	=	false
bPartnumber	=	false
bItemid		=	false
bDetails	=	false
bOrderID	=	false

For i = 0 To Cint(UBound(arrGroupBy))
	If "" <> trim(arrGroupBy(i)) Then
		Select Case ucase(trim(arrGroupBy(i)))
			Case "DATE"
				If bDate Then
					Response.Write "DATE in [GROUP BY] is duplicated"
					Response.End
				End If
				bDate = true

				strSqlSelectMain	=	strSqlSelectMain 	& 	", convert(varchar(10), a.orderdatetime, 20) logDate"
				strSqlGroupBy		=	strSqlGroupBy		&	"convert(varchar(10), a.orderdatetime, 20), "
				strSqlOrderBy		=	strSqlOrderBy		&	"convert(varchar(10), a.orderdatetime, 20), "
				strHeadings			=	strHeadings			&	"Order Date" & vbTab
				strFooter			=	strFooter			&	vbTab

			Case "STORE"
				If bStore Then
					Response.Write "STORE in [GROUP BY] is duplicated"
					Response.End
				End If
				bStore = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.store"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.store, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.store, "
				strHeadings			=	strHeadings			&	"SITE" & vbTab
				strFooter			=	strFooter			&	vbTab				

			Case "BRAND"
				If bBrand Then
					Response.Write "BRAND in [GROUP BY] is duplicated"
					Response.End
				End If
				bBrand = true
				
				strSqlFrom			=	strSqlFrom			&	"left outer join we_brands e on c.brandid = e.brandid" & vbcrlf
				strSqlSelectMain	=	strSqlSelectMain 	& 	", isnull(e.brandName, 'Universal')"
				strSqlGroupBy		=	strSqlGroupBy		&	"isnull(e.brandName, 'Universal'), "
				strSqlOrderBy		=	strSqlOrderBy		&	"isnull(e.brandName, 'Universal'), "
				strHeadings			=	strHeadings			&	"Brand" & vbTab
				strFooter			=	strFooter			&	vbTab				

			Case "MODEL"
				If bModel Then
					Response.Write "Model in [GROUP BY] is duplicated"
					Response.End
				End If
				bModel = true

				strSqlFrom			=	strSqlFrom			&	"left outer join we_models f on c.modelid = f.modelid" & vbcrlf
				strSqlSelectMain	=	strSqlSelectMain 	& 	", isnull(f.modelName, 'Universal')"
				strSqlGroupBy		=	strSqlGroupBy		&	"isnull(f.modelName, 'Universal'), "
				strSqlOrderBy		=	strSqlOrderBy		&	"isnull(f.modelName, 'Universal'), "				
				strHeadings			=	strHeadings			&	"Model" & vbTab
				strFooter			=	strFooter			&	vbTab				

			Case "CATEGORY"
				If bCategory Then
					Response.Write "Category in [GROUP BY] is duplicated"
					Response.End
				End If
				bCategory = true

				strSqlFrom			=	strSqlFrom			&	"join we_types d on c.typeid = d.typeid" & vbcrlf
				strSqlSelectMain	=	strSqlSelectMain 	& 	", d.typename"
				strSqlGroupBy		=	strSqlGroupBy		&	"d.typename, "
				strSqlOrderBy		=	strSqlOrderBy		&	"d.typename, "				
				strHeadings			=	strHeadings			&	"Category" & vbTab
				strFooter			=	strFooter			&	vbTab				

			Case "PARTNUMBER"
				If bPartnumber Then
					Response.Write "Partnumber in [GROUP BY] is duplicated"
					Response.End
				End If
				bPartnumber = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", c.partnumber"
				strSqlGroupBy		=	strSqlGroupBy		&	"c.partnumber, "
				strSqlOrderBy		=	strSqlOrderBy		&	"c.partnumber, "				
				strHeadings			=	strHeadings			&	"Part Number" & vbTab
				strFooter			=	strFooter			&	vbTab				

			Case "ITEMID"
				If bItemID Then
					Response.Write "ItemID in [GROUP BY] is duplicated"
					Response.End
				End If
				bItemID = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", c.itemid"
				strSqlGroupBy		=	strSqlGroupBy		&	"c.itemid, "
				strSqlOrderBy		=	strSqlOrderBy		&	"c.itemid, "				
				strHeadings			=	strHeadings			&	"Item ID" & vbTab
				strFooter			=	strFooter			&	vbTab
				
			Case "ORDERID"
				If bOrderID Then
					Response.Write "OrderID in [GROUP BY] is duplicated"
					Response.End
				End If
				bOrderID = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.orderid"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.orderid, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.orderid, "				
				strHeadings			=	strHeadings			&	"Order ID" & vbTab
				strFooter			=	strFooter			&	vbTab

			Case "DETAILS"
				bDetails = true			
		End Select
	End If
Next

filename	=	"SalesReport_" & Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now) & ".xls"

Response.ContentType = "application/vnd.ms-excel"
'Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
Response.AddHeader "Content-Disposition","attachment;filename="""&filename&""""
'Response.Charset = "" 
'Me.EnableViewState = False

response.write "New Sales Report" & vbNewLine

strSqlWhere = ""
if siteid <> "" 		then strSqlWhere = strSqlWhere & "	and	a.store = " & siteid & vbcrlf
if brandid <> 0			then strSqlWhere = strSqlWhere & "	and	c.brandid = " & brandid & vbcrlf
if modelid <> 0			then strSqlWhere = strSqlWhere & "	and	c.modelid = " & modelid & vbcrlf
if typeid <> 0			then strSqlWhere = strSqlWhere & "	and	c.typeid = " & typeid & vbcrlf
if orderType <> 0		then 
	if orderType = -1 then 
		strSqlWhere = strSqlWhere & "	and	a.extordertype is null" & vbcrlf
	else
		strSqlWhere = strSqlWhere & "	and	a.extordertype = " & orderType & vbcrlf
	end if
end if
if partnumber <> ""		then strSqlWhere = strSqlWhere & "	and	c.partnumber = '" & partnumber & "'" & vbcrlf
if shiptype <> ""		then strSqlWhere = strSqlWhere & "	and	a.shiptype = '" & shiptype & "'" & vbcrlf
if addressDiff <> ""	then strSqlWhere = strSqlWhere & "	and	v.baddress1 <> v.saddress1 and v.szip <> v.bzip" & vbcrlf

'response.write "strSqlWhere:" & strSqlWhere & vbNewLine
'response.end

if bDetails then
	strSqlOrderBy = "order by 1, 2 desc"
	sql	=	"select	a.store, a.orderid, convert(varchar(10), a.orderdatetime, 20) orderdatetime, c.partnumber, c.itemid, c.itemdesc, isnull(b.quantity, 0) quantity" & vbcrlf & _
			"	,	isnull(c.cogs, 0) cogs" & vbcrlf & _
			"	,	isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
			"					when a.store = 1 then c.price_ca" & vbcrlf & _
			"					when a.store = 2 then c.price_co" & vbcrlf & _
			"					when a.store = 3 then c.price_ps" & vbcrlf & _
			"					when a.store = 10 then" & vbcrlf & _
			"						case when c.price_er > 0 then c.price_er" & vbcrlf & _
			"							else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
			"						end " & vbcrlf & _
			"					else c.price_our" & vbcrlf & _
			"				end, 0) price_our" & vbcrlf & _
			"	,	isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
			"					when a.store = 1 then c.price_ca" & vbcrlf & _
			"					when a.store = 2 then c.price_co" & vbcrlf & _
			"					when a.store = 3 then c.price_ps" & vbcrlf & _
			"					when a.store = 10 then" & vbcrlf & _
			"						case when c.price_er > 0 then c.price_er" & vbcrlf & _
			"							else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
			"						end " & vbcrlf & _
			"					else c.price_our" & vbcrlf & _
			"				end, 0) * isnull(b.quantity, 0) linePriceTotal" & vbcrlf & _
			"	,	(isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
			"					when a.store = 1 then c.price_ca" & vbcrlf & _
			"					when a.store = 2 then c.price_co" & vbcrlf & _
			"					when a.store = 3 then c.price_ps" & vbcrlf & _
			"					when a.store = 10 then" & vbcrlf & _
			"						case when c.price_er > 0 then c.price_er" & vbcrlf & _
			"							else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
			"						end " & vbcrlf & _
			"					else c.price_our" & vbcrlf & _
			"				end, 0) * isnull(b.quantity, 0)) - (isnull(c.cogs, 0) * isnull(b.quantity, 0)) revenue" & vbcrlf & _
			"	,	v.saddress1, isnull(v.saddress2, '') saddress2, v.scity, v.sstate, v.szip, v.baddress1, isnull(v.baddress2, '') baddress2, v.bcity, v.bstate, v.bzip" & vbcrlf & _
			"from	we_orders a join we_orderdetails b" & vbcrlf & _
			"	on	a.orderid = b.orderid join we_items c" & vbcrlf & _
			"	on	b.itemid = c.itemid join v_accounts v" & vbcrlf & _
			"	on	a.accountid = v.accountid and a.store = v.site_id" & vbcrlf & _
			"where	a.orderdatetime >= '" & strSDate & "'" & vbcrlf & _
			"	and	a.orderdatetime < '" & dateadd("d", 1, strEDate) & "'" & vbcrlf & _
			"	and	a.approved = 1 and (a.cancelled = 0 or a.cancelled is null)" & vbcrlf & _
			"	and	b.returned = 0" & vbcrlf & _
			strSqlWhere & vbcrlf & _
			strSqlOrderBy
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	if rs.eof then
		response.write "No data to display"
	else
		response.write "SITE" & vbTab
		response.write "Order Date" & vbTab
		response.write "ORDERID" & vbTab
		response.write "PART NUMBER" & vbTab
		response.write "ITEMID" & vbTab
		response.write "Item Desc" & vbTab
		response.write "QTY" & vbTab
		response.write "Price" & vbTab
		response.write "Total" & vbTab
		response.write "COST" & vbTab
		response.write "Revenue" & vbTab
		response.write "Shipping Address" & vbTab
		response.write "Billing Address" & vbNewLine		

		lap = 0
		priceTotal = cdbl(0)
		cogsTotal = cdbl(0)
		qtyTotal = clng(0)
		revenueTotal = cdbl(0)
		do until rs.eof
			lap = lap + 1			
			
			select case rs("store")
				case 0 : site = "WE"
				case 1 : site = "CA"
				case 2 : site = "CO"
				case 3 : site = "PS"
				case 10 : site = "TM"
			end select

			qty = clng(rs("quantity"))
			price_our = cdbl(rs("price_our"))
			linePriceTotal = cdbl(rs("price_our")) * qty
			lineCogsTotal = cdbl(rs("cogs")) * qty
			revenue = cdbl(rs("revenue"))
			
			priceTotal = priceTotal + linePriceTotal
			cogsTotal = cogsTotal + lineCogsTotal
			qtyTotal = qtyTotal + qty
			revenueTotal = revenueTotal + revenue
			
			itemdesc = rs("itemdesc")

			saddress = rs("saddress1") & " " & rs("saddress2") & " " & rs("scity") & " " & rs("sstate") & " " & rs("szip")
			baddress = rs("baddress1") & " " & rs("baddress2") & " " & rs("bcity") & " " & rs("bstate") & " " & rs("bzip")			
			
			response.write site & vbTab
			response.write rs("orderdatetime") & vbTab
			response.write rs("orderid") & vbTab
			response.write rs("partnumber") & vbTab
			response.write rs("itemid") & vbTab
			response.write itemdesc & vbTab
			response.write formatnumber(qty, 0) & vbTab
			response.write formatcurrency(price_our) & vbTab
			response.write formatcurrency(linePriceTotal) & vbTab
			response.write formatcurrency(lineCogsTotal) & vbTab
			response.write formatcurrency(revenue) & vbTab
			response.write saddress & vbTab
			response.write baddress & vbNewLine

			if (lap mod 500) = 0 then response.flush
			rs.movenext
		loop
		
		response.write vbTab & vbTab & vbTab & vbTab & vbTab & "Total" & vbTab
		response.write formatnumber(qtyTotal, 0) & vbTab
		response.write vbTab
		response.write formatcurrency(priceTotal) & vbTab
		response.write formatcurrency(cogsTotal) & vbTab
		response.write formatcurrency(revenueTotal) & vbTab
		response.write "" & vbTab
		response.write "" & vbNewLine
	end if
else
	if "" <> strSqlGroupBy then
		strSqlGroupBy	=	"group by " & left(trim(strSqlGroupBy), len(trim(strSqlGroupBy))-1)
		strSqlOrderBy	=	"order by " & left(trim(strSqlOrderBy), len(trim(strSqlOrderBy))-1)
	end if
	
	sql	=	"select	sum(isnull(b.quantity, 0)) quantity" & vbcrlf & _
			"	,	sum(isnull(b.quantity, 0) * isnull(c.cogs, 0)) cogs" & vbcrlf & _
			"	,	sum(isnull(b.quantity, 0) * isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
			"												when a.store = 1 then c.price_ca" & vbcrlf & _
			"												when a.store = 2 then c.price_co" & vbcrlf & _
			"												when a.store = 3 then c.price_ps" & vbcrlf & _
			"												when a.store = 10 then" & vbcrlf & _
			"													case when c.price_er > 0 then c.price_er" & vbcrlf & _
			"														else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
			"													end " & vbcrlf & _
			"												else c.price_our" & vbcrlf & _
			"											end, 0)) price_our" & vbcrlf & _
			"	,	sum(isnull(b.quantity, 0) * isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
			"												when a.store = 1 then c.price_ca" & vbcrlf & _
			"												when a.store = 2 then c.price_co" & vbcrlf & _
			"												when a.store = 3 then c.price_ps" & vbcrlf & _
			"												when a.store = 10 then" & vbcrlf & _
			"													case when c.price_er > 0 then c.price_er" & vbcrlf & _
			"														else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
			"													end " & vbcrlf & _
			"												else c.price_our" & vbcrlf & _
			"											end, 0)) - sum(isnull(b.quantity, 0) * isnull(c.cogs, 0)) revenue" & vbcrlf & _
			strSqlSelectMain & vbcrlf & _
			"from	we_orders a join we_orderdetails b" & vbcrlf & _
			"	on	a.orderid = b.orderid join we_items c" & vbcrlf & _
			"	on	b.itemid = c.itemid join v_accounts v" & vbcrlf & _
			"	on	a.accountid = v.accountid and a.store = v.site_id" & vbcrlf & _
			strSqlFrom & _
			"where	a.orderdatetime >= '" & strSDate & "'" & vbcrlf & _
			"	and	a.orderdatetime < '" & dateadd("d", 1, strEDate) & "'" & vbcrlf & _
			"	and	a.approved = 1 and (a.cancelled = 0 or a.cancelled is null)" & vbcrlf & _
			"	and	b.returned = 0" & vbcrlf & _
			strSqlWhere & vbcrlf & _
			strSqlGroupBy & vbcrlf & _					
			strSqlOrderBy
	session("errorSQL") = sql
	arrResult = getDbRows(sql)
	
	response.write strHeadings
	response.write "QTY" & vbTab
	response.write "Total Price" & vbTab
	response.write "Total COST" & vbTab
	response.write "Total Revenue" & vbNewLine

	qtyTotal = clng(0)
	priceTotal = cdbl(0)
	cogsTotal = cdbl(0)
	revenueTotal = cdbl(0)
	if not isnull(arrResult) then
		for nRow=0 to ubound(arrResult,2)
			qty = clng(arrResult(0,nRow))
			linePriceTotal = cdbl(arrResult(2,nRow))
			lineCogsTotal = cdbl(arrResult(1,nRow))
			revenue = cdbl(arrResult(3,nRow))
			
			qtyTotal = qtyTotal + qty								'quantity
			priceTotal = priceTotal + linePriceTotal				'priceTotal
			cogsTotal = cogsTotal + lineCogsTotal					'cogsTotal
			revenueTotal = revenueTotal + revenue

			nSkipColumn = 4
			totalWidth = 0
			for i=0 to ubound(arrGroupBy)
				if "" <> trim(arrGroupBy(i)) then
					select case ucase(trim(arrGroupBy(i)))
						case "DATE"
							response.write arrResult(nSkipColumn,nRow) & vbTab
						Case "STORE"
							select case cint(arrResult(nSkipColumn,nRow))
								case 0 : site = "WE"
								case 1 : site = "CA"
								case 2 : site = "CO"
								case 3 : site = "PS"
								case 10 : site = "TM"
							end select
							response.write site & vbTab
						Case "BRAND"
							response.write arrResult(nSkipColumn,nRow) & vbTab
						Case "MODEL"
							response.write arrResult(nSkipColumn,nRow) & vbTab
						Case "CATEGORY"
							response.write arrResult(nSkipColumn,nRow) & vbTab
						Case "PARTNUMBER"
							response.write arrResult(nSkipColumn,nRow) & vbTab
						Case "ITEMID"
							response.write arrResult(nSkipColumn,nRow) & vbTab
						Case "ORDERID"
							response.write arrResult(nSkipColumn,nRow) & vbTab
					end select
					nSkipColumn = nSkipColumn + 1										
				end if
			next
			response.write formatnumber(qty, 0) & vbTab
			response.write formatcurrency(linePriceTotal) & vbTab
			response.write formatcurrency(lineCogsTotal) & vbTab
			response.write formatcurrency(revenue) & vbNewLine
		next
		
		response.write strFooter
		response.write formatnumber(qtyTotal, 0) & vbTab
		response.write formatcurrency(priceTotal) & vbTab
		response.write formatcurrency(cogsTotal) & vbTab
		response.write formatcurrency(revenueTotal)
	else
		response.write "No data to display"
	end if
end if
%>