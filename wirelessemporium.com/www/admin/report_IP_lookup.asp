<%header = 1%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<%
if request("submitted") <> "" then
	orderID = request("orderID")
	SQL = "SELECT A.orderid, A.accountid, A.orderdatetime, A.ordergrandtotal, A.approved, A.cancelled,"
	SQL = SQL & " B.email, B.BlockedIp, B.IsBlocked, B.InValidCount, B.LastInvalid"
	SQL = SQL & " FROM we_orders A INNER JOIN we_accounts B ON A.accountid = B.accountid"
	SQL = SQL & " WHERE A.orderid = '" & orderid & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if RS.eof then
		response.write "No records matched<br><br>So cannot make table..."
	else
		%>
		<h3><%=RS.recordcount%> records found</h3>
		<table border="1">
			<tr>
				<td><b>Order&nbsp;ID</b></td>
				<td><b>E-Mail</b></td>
				<td><b>Order&nbsp;Notes</b></td>
				<td><b>Order&nbsp;Date/Time</b></td>
				<td><b>Order&nbsp;Grand&nbsp;Total</b></td>
				<td><b>Order&nbsp;BlockedIp</b></td>
				<td><b>Order&nbsp;IsBlocked</b></td>
				<td><b>Order&nbsp;InValidCount</b></td>
				<td><b>Order&nbsp;LastInvalid</b></td>
			</tr>
			<%
			do until RS.eof
				%>
				<tr>
					<td valign="top"><a href="javascript:printinvoice('<%=RS("orderid")%>','<%=RS("accountid")%>');"><%=RS("orderid")%></a></td>
					<td valign="top"><%=RS("email")%></td>
					<td valign="top"><a href="javascript:fnopennotes('<%=RS("orderid")%>');">show&nbsp;notes</a></td>
					<td valign="top"><%=RS("orderdatetime")%></td>
					<td valign="top"><%=formatCurrency(RS("ordergrandtotal"))%></td>
					<td valign="top"><%=RS("BlockedIp")%></td>
					<td valign="top"><%=RS("IsBlocked")%></td>
					<td valign="top"><%=RS("InValidCount")%></td>
					<td valign="top"><%=RS("LastInvalid")%></td>
				</tr>
				<%
				RS.movenext
			loop
			%>
		</table>
		<%
	end if
	RS.close
	set RS = nothing
end if
%>

<form action="report_IP_lookup.asp" method="post">
	<p>Order Number: <input type="text" name="orderID" value="<%=request("orderID")%>"></p>
	<p><input type="submit" name="submitted" value="Submit"></p>
</form>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->

<script language="javascript">
function printinvoice(orderid,accountid) {
	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}

function fnopennotes(orderid) {
	var url='ordernotes.asp?orderid='+orderid;
	window.open(url,'ordernote','left=0,top=0,width=800,height=300,resizable=1,locationbar=0,menubar=0,toolbar=0');
}
</script>
