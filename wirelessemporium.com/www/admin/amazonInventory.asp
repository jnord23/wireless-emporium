<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Amazon Upload Document"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	sql = "select a.id, a.partNumber, b.inv_qty from we_amazonProducts a left join we_items b on left(a.partNumber,15) = b.partNumber and b.master = 1"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	dim fs,tfile
	set fs=Server.CreateObject("Scripting.FileSystemObject")
	set tfile=fs.CreateTextFile(server.MapPath("/admin/tempCSV/amazonInv.txt"))
	
	lineData = "TemplateType=Wireless,Version=1.4,This row for Amazon.com use only. Do not modify or delete.,Rebates,CATEGORY SPECIFIC"
	lineArray = split(lineData,",")
	for i = 0 to ubound(lineArray)
		if lineArray(i) = "Rebates" then
			for x = 0 to 84
				writeData = writeData & vbtab
			next
		end if
		if lineArray(i) = "CATEGORY SPECIFIC" then
			for x = 0 to 9
				writeData = writeData & vbtab
			next
		end if
		writeData = writeData & lineArray(i) & vbtab
	next
	tfile.WriteLine(writeData)
	
	writeData = ""
	lineData = "sku,standard-product-id,product-id-type,title,manufacturer,brand,mfr-part-number,merchant-catalog-number,bullet-point1,bullet-point2,bullet-point3," &_
	"bullet-point4,bullet-point5,description,product_type,legal-disclaimer,prop-65,cpsia-warning1,cpsia-warning2,cpsia-warning3,cpsia-warning4," &_
	"cpsia-warning-description,item-type,used-for1,used-for2,used-for3,used-for4,used-for5,other-item-attributes1,other-item-attributes2," &_
	"other-item-attributes3,other-item-attributes4,other-item-attributes5,subject-content1,subject-content2,subject-content3,subject-content4,subject-content5," &_
	"search-terms1,search-terms2,search-terms3,search-terms4,search-terms5,platinum-keywords1,platinum-keywords2,platinum-keywords3,platinum-keywords4,platinum-keywords5," &_
	"main-image-url,other-image-url1,other-image-url2,other-image-url3,other-image-url4,other-image-url5,other-image-url6,other-image-url7,other-image-url8," &_
	"item-weight-unit-of-measure,item-weight,item-length-unit-of-measure,item-length,item-height,item-width,shipping-weight-unit-of-measure,shipping-weight," &_
	"product-tax-code,launch-date,release-date,msrp,item-price,sale-price,sale-from-date,sale-through-date,quantity,leadtime-to-ship,restock-date," &_
	"max-aggregate-ship-quantity,is-gift-message-available,is-giftwrap-available,is-discontinued-by-manufacturer,update-delete,target-audience-keywords1," &_
	"target-audience-keywords2,target-audience-keywords3,item-condition,condition-note,registered-parameter"
	lineArray = split(lineData,",")
	for i = 0 to ubound(lineArray)
		writeData = writeData & lineArray(i) & vbtab
	next
	tfile.WriteLine(writeData)
	
	do while not rs.EOF
		curID = rs("id")
		curPartNumber = replace(trim(rs("partNumber")),chr(160),"")
		curQty = rs("inv_qty")
		writeData = curPartNumber
		for x = 0 to 72
			writeData = writeData & vbtab
		next
		if isnull(curQty) or len(curQty) < 1 or curQty = 0 then
			if instr(curPartNumber,"DEC-SKN") > 0 then
				useQty = 1000
			elseif left(curPartNumber,3) = "MS-" then
				useQty = 1000
			elseif curPartNumber = "UNL-BBY-UNI-01" then
				useQty = 500
			elseif curPartNumber = "MS-" then
				useQty = 1000
			else
				useQty = curQty
			end if
		else
			useQty = curQty
		end if

		if useQty < 0 then useQty = 0
		writeData = writeData & useQty
		session("errorSQL") = writeData
		tfile.WriteLine(writeData)
		
		if curPartNumber <> rs("partNumber") then
			sql = "update we_amazonProducts set partNumber = '" & curPartNumber & "' where id = " & curID
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		
		rs.movenext
	loop
	
	tfile.close
	set tfile=nothing
	set fs=nothing
%>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-top:20px;">
	<tr>
    	<td>File Created: <a href="/admin/tempCSV/amazonInv.txt">Click here to download</a></td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
