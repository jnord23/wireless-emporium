<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
partnumber = request("txtSearch")
filename	=	"AmazonCogs_" & Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now) & ".xls"

Response.ContentType = "application/vnd.ms-excel"
'Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
Response.AddHeader "Content-Disposition","attachment;filename="""&filename&""""
'Response.Charset = "" 
'Me.EnableViewState = False

if partnumber <> "" then partnumber = replace(partnumber, "Search By PartNumber", "")

sql	=	"select	distinct a.partnumber, a.partnumber_we" & vbcrlf & _
		"	,	coalesce(b.cogs, c.cogs) cogs" & vbcrlf & _
		"	,	case when b.partnumber is not null then b.inv_qty" & vbcrlf & _
		"			when c.musicSkinsID is not null then 999" & vbcrlf & _
		"			else null" & vbcrlf & _
		"		end inv_qty" & vbcrlf & _
		"	,	coalesce(b.price_our, c.price_we) price_we" & vbcrlf & _
		"from	we_amazonProducts a left outer join we_items b" & vbcrlf & _
		"	on	a.partnumber_we = b.partnumber and b.master = 1 left outer join we_items_musicskins c" & vbcrlf & _
		"	on	a.partnumber_we = c.musicSkinsID and c.deleteItem = 0 and c.skip = 0" & vbcrlf & _
		"where	a.partnumber_we like '%" & partnumber & "%'" & vbcrlf & _
		"order by 2 desc, 1"
Set rs = Server.CreateObject("ADODB.Recordset")
rs.open SQL, oConn, 0, 1
if rs.eof then
	response.write "No records matched...So cannot make table..."
else
	response.write "Amazon Partnumber" & vbTab & "WE Partnumber" & vbTab & "COGS" & vbTab & "QTY on Hand" & vbTab & "Price WE" & vbNewLine
	do until rs.eof
		if isnull(rs("partnumber_we")) then
			response.write rs("partnumber") & vbTab
			response.write "N/A" & vbTab
			response.write "N/A" & vbTab
			response.write "N/A" & vbTab
			response.write "N/A" & vbNewLine
		else
			response.write rs("partnumber") & vbTab
			response.write rs("partnumber_we") & vbTab
			if not isnull(rs("cogs")) and isnumeric(rs("cogs")) then
				response.write formatcurrency(rs("cogs")) & vbTab
			else
				response.write rs("cogs") & vbTab
			end if
			if not isnull(rs("inv_qty")) and isnumeric(rs("inv_qty")) then
				response.write formatnumber(rs("inv_qty"),0) & vbTab
			else
				response.write rs("inv_qty") & vbTab
			end if
			if not isnull(rs("price_we")) and isnumeric(rs("price_we")) then
				response.write formatcurrency(rs("price_we")) & vbNewLine
			else
				response.write rs("price_we") & vbNewLine
			end if
		end if
		rs.movenext
	loop
RS.close
Set RS = nothing
end if
%>
