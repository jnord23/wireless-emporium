dim oConn
set oConn = CreateObject("ADODB.Connection")
oConn.ConnectionString = "DSN=WEDB01;Uid=wireless_user;Pwd=c3llph0n3;"
oConn.ConnectionTimeout = 1000		'seconds
oConn.Open

dim SQL, RS, dateProcessed, strToDelete
strToDelete = ""
SQL = "SELECT * FROM UPS_tracking ORDER BY id"
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
do until RS.eof
	if RS("orderid") <> "" and not isNull(RS("orderid")) and len(RS("orderid")) = 6 and isNumeric(RS("orderid")) then
		SQL = "UPDATE we_orders SET"
		if RS("void") = "Y" then
			SQL = SQL & " trackingNum = '',"
			SQL = SQL & " UPScost = null,"
			SQL = SQL & " confirmSent = null,"
			SQL = SQL & " confirmdatetime = null,"
			SQL = SQL & " thub_posted_date = null"
		else
			dateProcessed = RS("dateProcessed")
			dateProcessed = mid(dateProcessed,5,2) & "/" & mid(dateProcessed,7,2) & "/" & left(dateProcessed,4)
			SQL = SQL & " trackingNum = '" & RS("trackingNumber") & "',"
			SQL = SQL & " UPScost = '" & RS("actualShippingCost") & "',"
			SQL = SQL & " confirmSent = 'yes',"
			SQL = SQL & " confirmdatetime = '" & dateProcessed & "',"
			SQL = SQL & " thub_posted_date = '" & RS("DateTimeEntd") & "'"
		end if
		SQL = SQL & " WHERE orderid = '" & RS("orderid") & "'"
		strToDelete = strToDelete & "'" & RS("orderid") & "',"
		oConn.execute SQL
	end if
	RS.movenext
loop

if strToDelete <> "" then
	SQL = "DELETE FROM UPS_tracking WHERE orderid IN (" & left(strToDelete,len(strToDelete)-1) & ")"
	oConn.execute SQL
end if

RS.close
set RS = nothing
oConn.close
set oConn = nothing
