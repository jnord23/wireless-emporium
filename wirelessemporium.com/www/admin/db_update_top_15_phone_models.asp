<%
response.buffer = false
dim thisUser, adminID
pageTitle = "WE Admin Site - Top Models To Push"
header = 1
thisUser = Request.Cookies("username")
adminID = Request.Cookies("adminID")
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
if request.form("submitted") = "Update" then
	dim id
	id = request.form("id")
	if id = "NEW" then
		SQL = "INSERT INTO we_TopPhoneModels (modelID,SortOrder,EntdBy,EntdDate,notes) VALUES ("
		SQL = SQL & " '" & request.form("modelID") & "',"
		SQL = SQL & " '" & request.form("SortOrder") & "',"
		SQL = SQL & " '" & adminID & "',"
		SQL = SQL & " '" & now & "',"
		SQL = SQL & " '" & SQLquote(request.form("notes")) & "')"
	else
		SQL = "UPDATE we_TopPhoneModels SET"
		SQL = SQL & " modelID = '" & request.form("modelID") & "',"
		SQL = SQL & " SortOrder = '" & request.form("SortOrder") & "',"
		SQL = SQL & " EntdBy = '" & adminID & "',"
		SQL = SQL & " EntdDate = '" & now & "',"
		SQL = SQL & " notes = '" & SQLquote(request.form("notes")) & "'"
		SQL = SQL & " WHERE id = '" & request.form("id") & "'"
	end if
	'response.write "<p>" & SQL & "</p>" & vbcrlf
	oConn.execute SQL
	%>
	<table align="center" width="100%">
		<tr bgcolor="#CCCCCC"><td align="center" valign="middle" class="bigText" bgcolor="#CCCCCC" height="20">&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>
		<tr><td align="center"><font face="verdana" size="3"><b>Records Updated Sucessfully</b><br><br><a href="db_update_top_15_phone_models.asp">Back to "Top Models To Push" page</a></font></td></tr>
		<tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>
		<tr bgcolor="#CCCCCC"><td align="center" valign="middle" class="bigText" bgcolor="#CCCCCC" height="20">&nbsp;</td></tr>
	</table>
	<%
elseif request.querystring("delete") <> "" then
	SQL = "DELETE FROM we_TopPhoneModels WHERE id = '" & request.querystring("delete") & "'"
	'response.write "<p>" & SQL & "</p>" & vbcrlf
	oConn.execute SQL
	response.write "<h3>Item #" & request.querystring("delete") & " deleted.</h3>" & vbcrlf
	response.write "<p><a href=""db_update_top_15_phone_models.asp"">Back to ""Top Models To Push"" page.</a></p>" & vbcrlf
elseif request.querystring("id") <> "" then
	dim thisBrandID, modelID, SortOrder, notes
	if request.querystring("id") <> "NEW" then
		SQL = "SELECT A.*, B.brandID FROM we_TopPhoneModels A INNER JOIN we_models B ON A.modelID = B.modelID WHERE A.id = '" & request.querystring("id") & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			thisBrandID = RS("brandID")
			modelID = RS("modelID")
			SortOrder = RS("SortOrder")
			notes = RS("notes")
		end if
	end if
	%>
	<form name="frmModelSelect" action="db_update_top_15_phone_models.asp" method="post">
		<p class="normalText">
			<SCRIPT LANGUAGE="JavaScript">
			var arrItems1 = new Array();
			var arrItemsGrp1 = new Array();
			var arrItemsGrp2 = new Array();
			<%
			dim brandID, holdBrandID, aCount
			SQL = "SELECT * FROM WE_Models ORDER BY brandID, modelName"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			aCount = 0
			do until RS.eof
				brandID = RS("brandID")
				if brandID <> holdBrandID then holdBrandID = brandID
				response.write "arrItems1[" & aCount & "] = """ & RS("modelName") & """;" & vbCrLf
				response.write "arrItemsGrp1[" & aCount & "] = " & brandID & ";" & vbCrLf
				response.write "arrItemsGrp2[" & aCount & "] = " & RS("modelID") & ";" & vbCrLf
				aCount = aCount + 1
				RS.movenext
			loop
			%>
			function selectChange(control, controlToPopulate, ItemArray, GroupArray, ValueArray) {
				var myEle;
				var x;
				// Empty the second drop down box of any choices
				for (var q = controlToPopulate.options.length; q >= 0; q--) controlToPopulate.options[q] = null;
				if (control.name == "Brand") {
					// Empty the third drop down box of any choices
					for (var q = document.frmModelSelect.Model.options.length; q >= 0; q--) document.frmModelSelect.Model.options[q] = null;
				}
				// ADD Default Choice - in case there are no values
				myEle = document.createElement("option");
				theText = document.createTextNode("-- Select Model --");
				myEle.appendChild(theText);
				myEle.setAttribute("value","");
				controlToPopulate.appendChild(myEle);
				// Now loop through the array of individual items
				// Any containing the same child id are added to the second dropdown box
				for (x = 0; x < ItemArray.length; x++) {
					if (GroupArray[x] == control.value) {
						myEle = document.createElement("option") ;
						myEle.setAttribute("value",ValueArray[x]);
						<%
						if modelID > 0 then
							%>
							if (ValueArray[x] == <%=modelID%>) {
								myEle.setAttribute("selected",true);
							}
							<%
						end if
						%>
						var txt = document.createTextNode(ItemArray[x]);
						myEle.appendChild(txt)
						controlToPopulate.appendChild(myEle)
					}
				}
			}
			</SCRIPT>
			<select id="Brand" name="Brand" onChange="selectChange(this, document.frmModelSelect.Model, arrItems1, arrItemsGrp1, arrItemsGrp2);">
				<option value="">-- Select Brand --</option>
				<%
				SQL = "SELECT * FROM WE_Brands A ORDER BY brandName"
				Set RS = Server.CreateObject("ADODB.Recordset")
				RS.open SQL, oConn, 3, 3
				do until RS.eof
					response.write "<option value=""" & RS("brandID") & """"
					if RS("brandID") = thisBrandID then response.write " selected"
					response.write ">" & RS("brandName") & "</option>" & vbCrLf
					RS.movenext
				loop
				%>
			</select>
			<br><br>
			<select id="Model" name="modelID">
				<option value="">-- Select Model --</option>
			</select>
		</p>
		<p>Rank:<br><input type="text" name="SortOrder" value="<%=SortOrder%>"></p>
		<p>Notes:<br><input type="text" name="notes" size="120" maxlength="200" value="<%=notes%>"></p>
		<p><input type="hidden" name="id" value="<%=request.querystring("id")%>"><input type="submit" name="submitted" class="smltext" value="Update"></p>
	</form>
	<%
	if thisBrandID > 0 then
		%>
		<script>
			selectChange(document.frmModelSelect.Brand, document.frmModelSelect.Model, arrItems1, arrItemsGrp1, arrItemsGrp2);
		</script>
		<%
	end if
else
	%>
	<table width="800" border="1" cellspacing="0" cellpadding="3" align="center">
		<tr class="bigText" bgcolor="#CCCCCC">
			<td colspan="5" align="center">Top Models To Push</td>
		</tr>
		<tr class="bigText">
			<td width="30%" align="left">&nbsp;Model</td>
			<td width="40%" align="left">&nbsp;Notes</td>
			<td width="10%" align="center">Ent'd By</td>
			<td width="10%" align="center">Ent'd On</td>
			<td width="10%" align="center">&nbsp;</td>
		</tr>
		<%
		SQL = "SELECT A.*, B.modelName, C.brandName, D.fname FROM we_TopPhoneModels A"
		SQL = SQL & " INNER JOIN we_models B ON A.modelID = B.modelID"
		SQL = SQL & " INNER JOIN we_brands C ON B.brandID = C.brandID"
		SQL = SQL & " INNER JOIN we_AdminUsers D ON A.EntdBy = D.adminID"
		SQL = SQL & " ORDER BY C.brandName, A.SortOrder"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		dim i, brandCount
		i = 0
		HoldBrandName = "99999999"
		do until RS.eof
			i = i + 1
			if RS("brandName") <> HoldBrandName then
				HoldBrandName = RS("brandName")
				brandCount = 1
				%>
				<tr class="smlText">
					<td align="left" colspan="5" bgcolor="#EEEEEE"><b>&nbsp;<%=RS("brandName")%></b></td>
				</tr>
				<%
			end if
			%>
			<tr class="smlText">
				<td align="left"><%=brandCount%>.<b>&nbsp;&nbsp;<%=RS("modelName")%></b></td>
				<td align="left">&nbsp;<%=RS("notes")%></td>
				<td align="center"><%=RS("fname")%></td>
				<td align="center"><%=dateValue(RS("EntdDate"))%></td>
				<td align="center">
					<a href="db_update_top_15_phone_models.asp?id=<%=RS("id")%>" class="bottomText-link">edit</a>&nbsp;|&nbsp;<a href="db_update_top_15_phone_models.asp?delete=<%=RS("id")%>" class="bottomText-link">delete</a>
				</td>
			</tr>
			<%
			brandCount = brandCount + 1
			RS.movenext
		loop
		RS.close
		set RS = nothing
		%>
		<tr class="smlText">
			<td align="left" colspan="5"><a href="db_update_top_15_phone_models.asp?id=NEW" class="bottomText-link">ADD NEW</a></td>
		</tr>
	</table>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
