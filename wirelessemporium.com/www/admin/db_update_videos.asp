<%
pageTitle = "Admin - Update WE Database - Update Promotions"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<script type="text/javascript" src="/includes/js/jquery/jquery-1.7.1.min.js"></script>
<link rel="stylesheet" href="/includes/css/colorbox.css" />
<script src="/includes/js/jquery.colorbox.js"></script>
<script>
    $(document).ready(function(){
        $(".linkVideo").colorbox({iframe:true, innerWidth:"60%", innerHeight:"60%"});
    });
</script>

<script>
	function updateChanges(videoID) {
		var typeid = document.getElementById('cbType'+videoID).value;
		var subtypeid = document.getElementById('cbSubtype'+videoID).value;
		var txtPartnumber = document.getElementById('txtPartnumber'+videoID).value;
		var itemids = document.getElementById('txtItemIds'+videoID).value;

		ajax("/ajax/admin/ajaxUpdateVideo.asp?videoID=" + videoID + "&typeid=" + typeid + "&subtypeid=" + subtypeid + "&partnumber=" + txtPartnumber + "&itemids=" + itemids,"dumpZone");
		document.getElementById('updateBtn'+videoID).value = 'UPDATED';
		setTimeout("doneChanges('" + videoID + "')",2000)
	}
	
	function doneChanges(videoID) {
		document.getElementById('updateBtn'+videoID).value = 'Update Changes';
	}
	
	function doSubmit(submitType) {
		videoURL = document.frmVideo.txtVideoURL.value;

		if (videoURL == 'http://www.youtube.com/...') {
			alert('Invalid URL');
			return false;
		} else if (videoURL.substring(0,22) != 'http://www.youtube.com') {
			alert('Invalid URL');
			return false;
		}
		
		return true;
	}
	
	function removeVideo(videoID) {
		if (confirm('Are you sure to remove this video?')) {
			document.frmVideo.hidRemoveID.value = videoID;
			document.frmVideo.submit();
		}
	}

	/*	
	function toggleHeader(headerID) {
		if (headerID == 0) {
			document.getElementById('header_0').style.backgroundColor = '#f2f2f2';
			document.getElementById('tbl_0').style.display = '';
			document.getElementById('header_1').style.backgroundColor = '#fff';
			document.getElementById('tbl_1').style.display = 'none';
		} else if (headerID == 1) {
			document.getElementById('header_1').style.backgroundColor = '#f2f2f2';
			document.getElementById('tbl_1').style.display = '';
			document.getElementById('header_0').style.backgroundColor = '#fff';
			document.getElementById('tbl_0').style.display = 'none';
		}
		document.frmVideo.hidHeaderID.value = headerID;
	}
	*/
</script>
<%
'response.write request.form & "<br>"
sql = "select typeid, typename from we_types order by typename"
arrTypes = getDbRows(sql)

sql = 	"select	typename, subtypeid, subtypeName from v_subtypeMatrix order by 1, 3"
arrSubTypes = getDbRows(sql)

headerID = prepInt(request.form("hidHeaderID"))
cbSite = prepStr(request.form("cbSite"))
videoTitle = prepStr(request.form("txtTitle"))
txtVideoURL = prepStr(request.form("txtVideoURL"))
cbType = prepInt(request.form("cbType"))
cbSubtype = prepInt(request.form("cbSubtype"))
submitted = prepStr(request.form("submitted"))
txtPartnumber = prepStr(request.form("txtPartnumber"))
removedID = prepInt(request.form("hidRemoveID"))
itemids = prepStr(request.form("txtItemids"))

if itemids = "ex) 16395,16396..." then itemids = ""
if cbSite = "" then cbSite = 0
if removedID > 0 then
	sql	=	"delete from xvideos where id = '" & removedID & "'"
	oConn.execute(sql)
end if

if submitted <> "" then
	sql	=	"insert into xvideos(site_id, title, video_url, typeid, subtypeid, partnumber, itemids) values(" & _
			cbSite & ",'" & SQLQuote(videoTitle) & "','" & SQLQuote(replace(replace(replace(txtVideoURL, chr(13), ""), chr(10), ""), " ", "")) & "'," & _
			cbType & "," & cbSubtype & ",'" & SQLQuote(txtPartnumber) & "','" & SQLQuote(itemids) & "')"
'	response.write sql & "<br>"
	oConn.execute(sql)
end if
%>

<form method="post" name="frmVideo">
<center>
	<div class="talign-l padding-v border-b" style="width:1000px; display:inline-block;">
    	<div style="float:left; width:500px;">
	        <h1 class="biggerText">UPLOAD YOUTUBE VIDEOS</h1>
            <h3 style="display:inline;">SITE:</h3>
            <select name="cbSite" onChange="this.form.submit();">
                <option value="0" <%if cbSite="0" then%>selected<%end if%>>WE</option>
                <option value="1" <%if cbSite="1" then%>selected<%end if%>>CA</option>
                <option value="2" <%if cbSite="2" then%>selected<%end if%>>CO</option>
                <option value="3" <%if cbSite="3" then%>selected<%end if%>>PS</option>
                <option value="10" <%if cbSite="10" then%>selected<%end if%>>TM</option>
            </select>
        </div>
    	<div style="float:right; width:280px; text-align:left;">
            <span style="font-weight:bold; font-size:14px;">Recommended Format</span><br />
             - http://www.youtube.com/embed/Gp8CrRUcAmM<br />
             - http://www.youtube.com/v/Pia-7dqq6JY
        </div>
    </div>
	<div style="width:1000px; padding-top:10px;" align="left">
    	<!--
    	<div id="header_0" class="left margin5 padding5 roundBorder" onClick="toggleHeader(0);" style="cursor:pointer; <%if headerID = 0 then%>background-color:#f2f2f2;<%end if%>">Videos by Categories</div>
    	<div id="header_1" class="left margin5 padding5 roundBorder" onClick="toggleHeader(1);" style="cursor:pointer; <%if headerID = 1 then%>background-color:#f2f2f2;<%end if%>">Videos by Partnumbers</div>
        -->
        <div class="clear"></div>
		<div style="width:100%; border:1px solid #ccc; border-radius:3px; background-color:#f4f7f9; padding:5px;">
            <div style="width:100%; height:30px;">
				<div class="filter-box-header" style="width:130px;">Video Title</div>
                <div class="filter-box-header" style="width:250px;">Video URL</div>
				<div class="filter-box-header" style="width:130px;">Catetories</div>
                <div class="filter-box-header" style="width:100px;">Partnumber</div>
                <div class="filter-box-header" style="width:150px;">Item IDs</div>
                <div class="filter-box-header" style="width:100px;">Action</div>
            </div>
            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%; padding:5px 0px 5px 0px; height:50px;">
                <div class="filter-box" style="width:130px;">
                	<input type="text" name="txtTitle" style="width:120px;" />
				</div>
                <div class="filter-box" style="width:250px;">
					<textarea name="txtVideoURL" cols="27" rows="2" style="color:#666;" onClick="if (this.value=='http://www.youtube.com/...') this.value=''">http://www.youtube.com/...</textarea>
				</div>
                <div class="filter-box" style="width:130px;">
					<select name="cbType" style="width:120px;">
						<option value="">Select Type</option>
					<%
                    for i=0 to ubound(arrTypes, 2)
                    %>
                        <option value="<%=arrTypes(0,i)%>"><%=arrTypes(1,i)%></option>
                    <%
                    next
                    %>
                    </select>
					<select name="cbSubtype" style="width:120px;">
                        <option value="">Select Subtype</option>
                        <%
                        lastLabel = ""
                        for nRow = 0 to ubound(arrSubTypes, 2)
                            if arrSubTypes(0,nRow) <> lastLabel then
                                if lastLabel <> "" then response.write "</optgroup>"
                                response.write "<optgroup label=""" & arrSubTypes(0,nRow) & """>"
                                lastLabel = arrSubTypes(0,nRow)
                            end if
                            %>
                            <option value="<%=arrSubTypes(1,nRow)%>"><%=arrSubTypes(2,nRow)%></option>
                            <%
                        next
                        %>
                        </optgroup>
                    </select>
				</div>
                <div class="filter-box" style="width:100px;">
                	<input type="text" name="txtPartnumber" style="width:90px;" value="" />
				</div>
                <div class="filter-box" style="width:150px;">
					<textarea name="txtItemids" cols="15" rows="2" style="color:#666;" onClick="if (this.value=='ex) 16395,16396...') this.value=''">ex) 16395,16396...</textarea>
				</div>
                <div class="filter-box" style="width:100px;">
					<input type="submit" name="submitted" value="ADD" onClick="return doSubmit();" />
                </div>
            </div>
        </div>
                
        <div id="tbl_0" class="w100 left" style="display:<%if headerID=1 then%>none<%end if%>;">
            <div class="clear"></div>
            <%
            sql	=	"select	a.id, a.site_id, isnull(a.title, 'N/A') title, a.video_url, a.entryDate, b.typeid, b.typename, c.subtypeid, c.subtypename, a.partnumber, a.itemids" & vbcrlf & _
                    "from	wirelessemp_db.dbo.xvideos a left outer join we_types b" & vbcrlf & _
                    "	on	a.typeid = b.typeid left outer join we_subtypes c" & vbcrlf & _
                    "	on	a.subtypeid = c.subtypeid " & vbcrlf & _
                    "where	a.site_id = " & cbSite & vbcrlf & _
                    "order by a.id desc"
'            response.write "<pre>" & sql & "</pre>"
            set rs = oConn.execute(sql)
            if not rs.eof then
                do until rs.eof
					videoTitle = rs("title")
					if len(videoTitle) > 35 then videoTitle = left(videoTitle, 32) & "..."
                    videoID = rs("id")
                    videoURL = rs("video_url")
                    typeid = rs("typeid")
                    categoryName = rs("typename")
                    subtypeid = rs("subtypeid")
                    subCategoryName = rs("subtypename")
                    partnumber = rs("partnumber")
					
                    itemids = rs("itemids")
					youtubeID = replace(replace(videoURL, "http://www.youtube.com/v/", ""), "http://www.youtube.com/embed/", "")
                %>
                <div class="left margin5 padding5 border" style="width:225px;">
                	<div class="left w100" title="<%=rs("title")%>">
						<div class="left bigText"><%=videoTitle%></div>
						<div class="left"><a class="linkVideo" href="<%=videoURL%>?rel=0&amp;wmode=transparent"><img src="http://img.youtube.com/vi/<%=youtubeID%>/0.jpg" border="0" width="220" height="150" alt="<%=rs("title")%>" /></a></div>
	                    <!--<iframe width="225" height="150" src="<%=videoURL%>" frameborder="0" allowfullscreen></iframe>-->
                    </div>
                	<div class="left w100">
	                	<div class="left">Type:&nbsp;</div>
	                	<div class="right">
                            <select id="cbType<%=videoID%>" style="width:150px;">
                                <option value="">Select Type</option>
                            <%
                            for i=0 to ubound(arrTypes, 2)
                                if prepStr(typeid) = prepStr(arrTypes(0,i)) then
                                %>
                                <option value="<%=arrTypes(0,i)%>" selected="selected"><%=arrTypes(1,i)%></option>
                                <%
                                else
                                %>
                                <option value="<%=arrTypes(0,i)%>"><%=arrTypes(1,i)%></option>
                                <%
                                end if
                            next
                            %>
                            </select>                            
                        </div>
                        <div class="clear" style="height:2px;"></div>
	                	<div class="left">Subtype:&nbsp;</div>
	                	<div class="right">
                            <select id="cbSubtype<%=videoID%>" style="width:150px;">
                                <option value="">Select Subtype</option>
                                <%
                                lastLabel = ""
                                for nRow = 0 to ubound(arrSubTypes, 2)
                                    if arrSubTypes(0,nRow) <> lastLabel then
                                        if lastLabel <> "" then response.write "</optgroup>"
                                        response.write "<optgroup label=""" & arrSubTypes(0,nRow) & """>"
                                        lastLabel = arrSubTypes(0,nRow)
                                    end if
                                    
                                    if prepStr(subtypeid) = prepStr(arrSubTypes(1,nRow)) then
                                    %>
                                    <option value="<%=arrSubTypes(1,nRow)%>" selected="selected"><%=arrSubTypes(2,nRow)%></option>
                                    <%
                                    else
                                    %>
                                    <option value="<%=arrSubTypes(1,nRow)%>"><%=arrSubTypes(2,nRow)%></option>
                                    <%
                                    end if
                                next
                                %>
                                </optgroup>
                            </select>
                        </div>
                        <div class="clear" style="height:2px;"></div>
	                	<div class="left">Partnumber:&nbsp;</div>
	                	<div class="right"><input type="text" id="txtPartnumber<%=videoID%>" style="width:147px;" value="<%=partnumber%>" /></div>
                        <div class="clear" style="height:2px;"></div>
	                	<div class="left">Item IDs:&nbsp;</div>
	                	<div class="right"><textarea id="txtItemIds<%=videoID%>" rows="2" style="width:145px; color:#666;"><%=itemids%></textarea></div>
                        <div class="clear" style="height:2px;"></div>
	                	<div class="left"><input type="button" value="Delete" onclick="removeVideo(<%=videoID%>);" /></div>
	                	<div class="right"><input id="updateBtn<%=videoID%>" type="button" value="Update Changes" onclick="updateChanges(<%=videoID%>);" /></div>
                    </div>
                    
                    <!--<div class="left biggerText"><%=rs("typename")%></div>-->

                </div>
                <%
                    rs.movenext
                loop
            end if
            %>
        </div>
    </div>
</center>
<input type="hidden" name="hidRemoveID" value="" />
<!--<input type="hidden" name="hidHeaderID" value="" />-->
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
