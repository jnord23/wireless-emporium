<%
pageTitle = "RMA updates"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/admin/formatNotes.asp"-->
<%
updateType = request.form("hidUpdateType")

if updateType = "C" then
	strReason = request.form("txtCancelReason")
	strCancel = request.form("hidChkCancel")
	if strCancel <> "" then
		strCancel = getCommaString(strCancel)
	end if
	
	nRows = 0
	arrOrders = split(strCancel, ",")
	for i=0 to ubound(arrOrders)
		nRows = nRows + 1
		arrTemp = split(arrOrders(i), "__")
		orderid = arrTemp(0)
		partnumber = arrTemp(1)
		
		strNotes = formatNotes("Back-Order Cancelled for " & partnumber & vbcrlf & " - reason: " & strReason & vbcrlf)
		
		oConn.execute("update we_orders set RMAStatus = null where orderid = '" & orderid & "'")
		oConn.execute("update we_rma set cancelDate = getdate() where orderid = '" & orderid & "' and partnumber = '" & partnumber & "'")

		sql = "SELECT ID, convert(varchar(2000), ordernotes) ordernotes FROM we_ordernotes WHERE orderid = '" & orderid & "'"
		set rs = oConn.execute(sql)
		if not rs.EOF then
			sql = "UPDATE we_ordernotes SET ordernotes='" & SQLquote(rs("ordernotes") & strNotes) & "' WHERE ID = '" & rs("ID") & "'"
		else
			sql = "INSERT INTO we_ordernotes (orderid,ordernotes) VALUES ('" & orderid & "','" & SQLquote(strNotes) & "')"
		end if
		oConn.execute(sql)
	next

	response.redirect "/admin/db_search_rma.asp?cbView=4"
elseif updateType = "P" then
	strProcess = request.form("hidChkProcess")
	if strProcess <> "" then
		strProcess = getCommaString(strProcess)
	end if
	
	adminID = session("adminID")
	arrOrders = split(strProcess, ",")
	batchTimeStamp = now
	for i=0 to ubound(arrOrders)
		arrTemp = split(arrOrders(i), "__")
		orderid = arrTemp(0)
		partnumber = arrTemp(1)
		qty = arrTemp(2)
		
		Set cmd = Server.CreateObject("ADODB.Command") : Set cmd.ActiveConnection = oConn
		cmd.CommandText = "processBackOrder" : cmd.CommandType = adCmdStoredProc 
	
		cmd.Parameters.Append cmd.CreateParameter("ret", adInteger, adParamReturnValue)
		cmd.Parameters.Append cmd.CreateParameter("p_orderid", adInteger, adParamInput)	
		cmd.Parameters.Append cmd.CreateParameter("p_partnumber", adVarChar, adParamInput, 100)
		cmd.Parameters.Append cmd.CreateParameter("p_qty", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_adminID", adInteger, adParamInput)
		cmd.Parameters.Append cmd.CreateParameter("p_batchTimeStamp", adDBTimeStamp, adParamInput)		
		cmd.Parameters.Append cmd.CreateParameter("p_out_msg", adVarChar, adParamOutput, 500)
	
		cmd("p_orderid")			=	orderid
		cmd("p_partnumber")			=	partnumber
		cmd("p_qty")				=	qty
		cmd("p_adminID")			=	adminID
		cmd("p_batchTimeStamp")		=	batchTimeStamp
	
		cmd.Execute
	
		retMsg	=	cmd("p_out_msg")
		response.write retMsg
		retVal	= 	cmd("ret")	
	next
'	response.end
	response.redirect "/admin/processOrders_run.asp?hidRunType=B"
end if



function getCommaString(val)
	if "" = trim(val) then getCommaString = "" end if
	
	dim arr : arr = split(val, ",")
	dim str : str = ""

	for i=0 to ubound(arr)
		if "" <> trim(arr(i)) then str = str & trim(arr(i)) & "," end if
	next
	
	if "," = right(str, 1) then str = left(str, len(str) - 1) end if 
	
	getCommaString = str
end function 
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
