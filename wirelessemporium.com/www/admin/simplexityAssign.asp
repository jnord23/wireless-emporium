<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Assign Simplexity Products"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim sql, rs, itemPic
	dim userMsg : userMsg = ""
	dim noMatch : noMatch = prepInt(request.QueryString("noMatch"))
	dim simID : simID = prepInt(request.Form("simID"))
	dim itemID : itemID = prepInt(request.Form("itemID"))
	dim modelID : modelID = prepInt(request.Form("modelID"))
	dim modelName : modelName = prepStr(request.Form("modelName"))
	dim carrierID : carrierID = prepInt(request.Form("carrierID"))
	dim brandID : brandID = prepInt(request.Form("brandID"))
	dim brandName : brandName = prepStr(request.Form("brandName"))
	dim useDesc : useDesc = prepStr(request.Form("useDesc"))
	dim price : price = prepStr(request.Form("price"))
	dim salePrice : salePrice = prepStr(request.Form("salePrice"))
	
	if noMatch > 0 then
		'no match
		sql = "update xSimplexity set noMatch = 1 where id = " & noMatch
		session("errorSQL") = sql
		oConn.execute(sql)
		
		userMsg = "No Match Updated"
	elseif itemID > 0 and simID > 0 then
		'match the products up
		sql = "update xSimplexity set itemID = " & itemID & " where id = " & simID
		session("errorSQL") = sql
		oConn.execute(sql)
		
		userMsg = "Match Complete!"
	elseif modelID > 0 and simID > 0 then
		'create new phone item
		modelName = replace(replace(modelName," ",""),"-","")
		uniquePN = 0
		usePN = "PHN-" & left(brandName,3) & "-" & left(modelName,4) & "-"
		last2 = 0
		do while uniquePN = 0
			last2 = last2 + 1
			if len(last2) < 2 then useLast2 = "0" & last2 else useLast2 = last2
			sql = "select modelImg,(select top 1 itemID from we_items with (nolock) where partNumber = '" & usePN & useLast2 & "') as itemID from we_models where modelID = " & modelID
			session("errorSQL") = sql
			set pnRS = oConn.execute(sql)
			itemPic = pnRS("modelImg")
			if isnull(pnRS("itemID")) then
				uniquePN = 1
				usePN = usePN & useLast2
				pnRS = null
			end if
		loop
		sql = "SET NOCOUNT ON; insert into we_items (brandID,modelID,typeID,carrierID,vendor,partNumber,itemDesc,itemDesc_PS,itemPic,price_retail,price_PS,inv_qty,master) values(" & brandID & "," & modelID & ",16," & carrierID & ",'SIM','" & ucase(usePN) & "','" & useDesc & "','" & useDesc & "','" & itemPic & "','" & price & "','" & price & "',0,1); SELECT @@IDENTITY AS newItemID;"
		session("errorSQL") = sql
		set insertRS = oConn.execute(sql)
		dim newItemID : newItemID = insertRS("newItemID")
		
		sql = "update xSimplexity set itemID = " & newItemID & " where id = " & simID
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "update a set a.brandID = b.brandID from we_Items a left join we_Models b on a.modelID = b.modelID where a.brandID = 0 and a.modelID > 0"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		set jpeg = Server.CreateObject("Persits.Jpeg")
		set fso = CreateObject("Scripting.FileSystemObject")
		
		jpeg.Open server.MapPath("/productpics/models/" & itemPic)
		jpeg.Save "C:\inetpub\wwwroot\productpics\big\" & itemPic
		
		jpeg.Open server.MapPath("/productpics/models/" & itemPic)
		jpeg.Height = 100
		jpeg.Width = jpeg.OriginalWidth * 100 / jpeg.OriginalHeight
		jpeg.Save "C:\inetpub\wwwroot\productpics\thumb\" & itemPic
		
		userMsg = "New Phone Item Added!"
	end if
	
	sql = "select top 1 *, (select count(*) from xSimplexity where itemID is null and noMatch = 0) as prodCnt from xSimplexity where itemID is null and noMatch = 0 order by id"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	if rs.EOF then
		response.Write("No More Products to Assign<br><br><strong style='font-size:16px;'>Products Not Yet Assigned</strong><br>")
		
		sql = "select *, (select count(*) from xSimplexity where noMatch = 1) as prodCnt from xSimplexity where noMatch = 1 order by id"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		do while not rs.EOF
			response.Write(rs("model") & "(" & rs("modelExt") & ")<br>")
			rs.movenext
		loop
		
		response.End()
	end if
	
	simID = rs("id")
	dim carrierName : carrierName = rs("carrier")
	brandName = rs("brand")
	modelName = rs("model")
	dim modelExt : modelExt = rs("modelExt")
	price = rs("retailPrice")
	salePrice = rs("afterRebatePrice")
	dim prodCnt : prodCnt = rs("prodCnt")
	
	if isnull(modelExt) or len(modelExt) < 1 then
		useDesc = modelName
	else
		if len(replace(modelName,brandName,"")) > len(replace(modelExt,brandName,"")) then useDesc = modelName else useDesc = modelExt
	end if
	if instr(useDesc,brandName) < 1 then useDesc = brandName & " " & useDesc
	
	sql = "select (select id from we_carriers where carrierName like '%" & carrierName & "%') as carrierID, b.modelID, b.modelName from we_brands a left join we_models b on a.brandID = b.brandID where brandName like '%" & brandName & "%' order by b.modelName"
	session("errorSQL") = sql
	set modelRS = oConn.execute(sql)
	
	sql = "select a.brandID, b.itemID, b.itemDesc, d.modelName from we_brands a left join we_items b on a.brandID = b.brandID left join we_carriers c on b.carrierID = c.id left join we_models d on b.modelID = d.modelID where c.carrierName like '%" & carrierName & "%' and b.hideLive = 0 and b.typeID = 16 and a.brandName like '%" & brandName & "%' order by b.itemDesc"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	if modelRS.EOF then
		response.Write("split pull")
		dim modelSplit : modelSplit = split(replace(modelName,brandName,"")," ")
		dim i
		dim custWhere : custWhere = ""
		for i = 0 to ubound(modelSplit)
			if custWhere = "" then
				custWhere = "modelName like '%" & modelSplit(i) & "%'"
			else
				custWhere = custWhere & " or modelName like '%" & modelSplit(i) & "%'"
			end if
		next
		sql = "select (select id from we_carriers where carrierName like '%" & carrierName & "%') as carrierID, b.modelID, b.modelName from we_brands a left join we_models b on a.brandID = b.brandID where " & custWhere & " order by b.modelName"
		session("errorSQL") = sql
		set modelRS = oConn.execute(sql)
	end if
	
	if modelRS.EOF then carrierID = 0 else carrierID = modelRS("carrierID")
	if rs.EOF then brandID = 0 else brandID = rs("brandID")
%>
<form action="/admin/simplexityAssign.asp" method="post" style="margin:0px;" onsubmit="return(verifyForm(this))">
<table border="0" cellpadding="0" cellspacing="0" width="500" align="center">
	<% if userMsg <> "" then %>
    <tr><td colspan="2" align="center" style="padding:10px; color:#F00; font-weight:bold; font-size:16px;"><%=userMsg%></td></tr>
    <% end if %>
    <tr><td align="left"><%=prodCnt%> Unassigned Products</td></tr>
    <tr>
    	<td style="border-right:1px solid #000;" valign="top">
        	<div style="width:250px; height:200px;">
                <div style="width:250px; text-align:center; font-weight:bold; font-size:18px; color:#FFF; background-color:#000; margin-bottom:10px;">Simplexity Product Details</div>
                <div style="width:100px; float:left; text-align:right; padding-right:5px; font-weight:bold;">Carrier:</div>
                <div style="width:140px; float:left;"><%=carrierName%></div>
                <div style="width:100px; float:left; text-align:right; padding-right:5px; font-weight:bold;">Brand:</div>
                <div style="width:140px; float:left;"><%=brandName%></div>
                <div style="width:100px; float:left; text-align:right; padding-right:5px; font-weight:bold;">Model:</div>
                <div style="width:140px; float:left;">
					<%=replace(modelName,brandName,"")%>
                    <% if not isnull(modelExt) and len(modelExt) > 0 then %>
                    <br /><span style="color:#F00;"><%=replace(modelExt,brandName,"")%></span>
                    <% end if %>
                </div>
            </div>
        </td>
        <td valign="top">
        	<div style="width:250px;">
                <div style="width:250px; text-align:center; font-weight:bold; font-size:18px; color:#FFF; background-color:#000; margin-bottom:10px;">Matching WE Product</div>
                <div style="width:100px; float:left; text-align:right; padding-right:5px; font-weight:bold;">Brand:</div>
                <div style="width:140px; float:left;"><%=brandName%></div>
                <div style="width:250px; text-align:center;">
                	<select name="itemID">
                    	<option value="">Select Phone Item</option>
						<%
						do while not rs.EOF
						%>
                        <option value="<%=rs("itemID")%>"><%=replace(replace(rs("itemDesc"),brandName,""),carrierName,"")%> (<%=rs("modelName")%>)</option>
                        <%
							rs.movenext
						loop
						%>
                    </select>
                </div>
                <div style="width:250px; text-align:center; margin-top:20px;">Model Only (Add Phone Item)</div>
                <div style="width:250px; text-align:center;">
                	<select name="modelID">
                    	<option value="">Select Phone Model</option>
						<%
						do while not modelRS.EOF
						%>
                        <option value="<%=modelRS("modelID")%>"><%=replace(modelRS("modelName"),brandName,"")%></option>
                        <%
							modelRS.movenext
						loop
						%>
                    </select>
                </div>
            </div>
        </td>
    </tr>
    <tr>
    	<td colspan="2" style="border-top:1px solid #000;">
        	<div style="float:left; padding-top:10px;"><input type="button" name="noMatch" value="No Match" onclick="window.location='/admin/simplexityAssign.asp?noMatch=<%=simID%>'" /></div>
            <div style="float:right; padding-top:10px;">
            	<input type="submit" name="mySub" value="Match Up" />
                <input type="hidden" name="simID" value="<%=simID%>" />
                <input type="hidden" name="brandID" value="<%=brandID%>" />
                <input type="hidden" name="brandName" value="<%=brandName%>" />
                <input type="hidden" name="carrierID" value="<%=carrierID%>" />
                <input type="hidden" name="modelName" value="<%=modelName%>" />
                <input type="hidden" name="useDesc" value="<%=useDesc%>" />
                <input type="hidden" name="price" value="<%=price%>" />
                <input type="hidden" name="salePrice" value="<%=salePrice%>" />
            </div>
        </td>
    </tr>
</table>
</form>
<%
	rs = null
	modelRS = null
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function verifyForm(matchForm) {
		var matchCode = 0
		if (matchForm.itemID.value != "") { matchCode++ }
		if (matchForm.modelID.value != "") { matchCode++ }
		if (matchCode == 0) {
			alert("You must select an Item or Model")
			return false
		}
		else if (matchCode == 2) {
			alert("You may only select an Item or Model, not both")
			return false
		}
		else {
			return true
		}
	}
</script>