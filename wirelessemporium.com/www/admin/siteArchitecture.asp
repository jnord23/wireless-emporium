<%
	thisUser = Request.Cookies("username")
	pageTitle = ""
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<style type="text/css">
	.teir { border-bottom:2px dotted #000; height:160px; width:600px; margin:10px auto; }
	.page { float:left; position:relative; right:-40%; z-index:1; }
	.pages { float:left; position:relative; right:-30%; z-index:1; }
	.webpageFull { width:100px; height:150px; margin:5px 10px; float:left; border:1px solid #000; cursor:pointer; }
	.webpageName { width:100%; text-align:center; padding-top:5px; }
	.imgZoom { border:2px solid #000; margin:20px auto; width:612px; }
	#pageBlowUp { position:absolute; top:0px; left:0px; width:100%; background-color:#CCC; z-index:999; }
</style>
<%
	class webpage
		public pageName
		public imgLink
		
		public property Get divDesign()
			divDesign =	"<div class='webpageFull' onclick=zoomIn('" & imgLink & "')>" &_
							"<div class='webpageName'>" & pageName & "</div>" &_
						"</div>"
		end property
		
		public property Get divDesign2(pageImg)
			divDesign2 ="<div class='webpageFull' onclick=zoomIn('" & imgLink & "')>" &_
							"<div class='webpageName'>" & pageName & "</div>" &_
							"<div class='webpageName'>" & pageImg & "</div>" &_
						"</div>"
		end property
	end class
	
	dim homepage : set homepage = New webpage
	homepage.pageName = "Homepage"
	homepage.imgLink = "/images/siteArch/weSiteDesign_index.jpg"
	
	dim brandPage : set brandPage = New webpage
	brandPage.pageName = "Brand"
	brandPage.imgLink = "/images/siteArch/weSiteDesign_brand.jpg"
	
	dim categoryPage : set categoryPage = New webpage
	categoryPage.pageName = "Category"
	categoryPage.imgLink = "/images/siteArch/weSiteDesign_categories.jpg"
	
	dim brandModelPage : set brandModelPage = New webpage
	brandModelPage.pageName = "Brand-Model"
	brandModelPage.imgLink = "/images/siteArch/weSiteDesign_brand-Model.jpg"
	
	dim categoryBrandPage : set categoryBrandPage = New webpage
	categoryBrandPage.pageName = "Category-Brand"
	categoryBrandPage.imgLink = "/images/siteArch/weSiteDesign_categoryBrand.jpg"
	
	dim brandModelCategoryPage : set brandModelCategoryPage = New webpage
	brandModelCategoryPage.pageName = "Brand-Model-Category"
	brandModelCategoryPage.imgLink = "/images/siteArch/weSiteDesign_brand-model-category.jpg"
	
	dim brandModelCategoryDetailPage : set brandModelCategoryDetailPage = New webpage
	brandModelCategoryDetailPage.pageName = "Brand-Model-Category-Details"
	brandModelCategoryDetailPage.imgLink = "/images/siteArch/weSiteDesign_brand-model-category-details.jpg"
	
	dim productPage : set productPage = New webpage
	productPage.pageName = "Product"
	productPage.imgLink = "/images/siteArch/weSiteDesign_product.jpg"
	
	dim basketPage : set basketPage = New webpage
	basketPage.pageName = "Basket"
	basketPage.imgLink = "/images/siteArch/weSiteDesign_basket.jpg"
	
	dim checkoutPage : set checkoutPage = New webpage
	checkoutPage.pageName = "Checkout"
	checkoutPage.imgLink = "/images/siteArch/weSiteDesign_checkout.jpg"
%>
<div id="pageBlowUp" onclick="zoomOut()"></div>
<div class="teir">
	<div class="page">
		<%=response.Write(homepage.divDesign())%>
    </div>
</div>
<div class="teir">
	<div class="pages">
		<%=response.Write(brandPage.divDesign())%>
    	<%=response.Write(categoryPage.divDesign())%>
    </div>
</div>
<div class="teir">
	<div class="pages">
		<%=response.Write(brandModelPage.divDesign())%>
    	<%=response.Write(categoryBrandPage.divDesign())%>
    </div>
</div>
<div class="teir">
	<div class="page">
		<%=response.Write(brandModelCategoryPage.divDesign())%>
    </div>
</div>
<div class="teir">
	<div class="page">
		<%=response.Write(brandModelCategoryDetailPage.divDesign())%>
    </div>
</div>
<div class="teir">
	<div class="page">
		<%=response.Write(productPage.divDesign())%>
    </div>
</div>
<div class="teir">
	<div class="page">
		<%=response.Write(basketPage.divDesign())%>
    </div>
</div>
<div class="teir">
	<div class="page">
		<%=response.Write(checkoutPage.divDesign())%>
    </div>
</div>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function zoomIn(fileLoc) {
		window.scroll(0,0)
		document.getElementById("pageBlowUp").innerHTML = "<div class='imgZoom'><img src='" + fileLoc + "' border='0' /></div>"
	}
	
	function zoomOut() {
		document.getElementById("pageBlowUp").innerHTML = ""
	}
</script>