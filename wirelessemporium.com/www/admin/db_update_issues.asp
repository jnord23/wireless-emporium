<%
pageTitle = "Admin - Update WE Database - Update Known Product Issues"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/admin/formatNotes.asp"-->

<blockquote>
<blockquote>

<font face="Arial,Helvetica">
	<p><a href="/admin/db_search_brands.asp?searchID=2">Back to Brands</a></p>
</font>

<%
if request("submitted") = "Update" then
	for a = 1 to request("totalCount")
		issuesText = SQLquote(request("issuesText" & a))
		if issuesText <> SQLquote(request("issuesTextOrig" & a)) then
			if issuesText = "" then
				SQL = "DELETE FROM WE_Items_Issues WHERE itemID=" & request("itemID" & a)
			else
				EditNotes = formatNotes(issuesText)
				SQL = "SELECT * FROM WE_Items_Issues WHERE itemID=" & request("itemID" & a)
				Set RS = Server.CreateObject("ADODB.Recordset")
				RS.open SQL, oConn, 3, 3
				if RS.recordcount > 0 then
					SQL = "UPDATE WE_Items_Issues SET issuesText='" & EditNotes & "'"
					SQL = SQL & " WHERE itemID=" & request("itemID" & a)
				else
					SQL = "INSERT INTO WE_Items_Issues (itemID,issuesText) VALUES ("
					SQL = SQL & request("itemID" & a) & ", "
					SQL = SQL & "'" & EditNotes & "')"
				end if
			end if
			'response.write "<p>" & SQL & "</p>"
			oConn.execute SQL
		end if
	next
	%>
	<h3>UPDATED!</h3>
	</body>
	</html>
	<%
else
	SQL = "SELECT A.*, B.issuesText FROM WE_Items A"
	SQL = SQL & " LEFT JOIN WE_Items_Issues B ON A.itemID = B.itemID"
	SQL = SQL & " WHERE A.BrandID='" & request("BrandID") & "'"
	if request("searchType") = "Type" then
		SQL = SQL & " AND A.TypeID='" & request("TypeID") & "'"
	elseif request("searchType") = "Model" then
		SQL = SQL & " AND A.ModelID='" & request("ModelID") & "'"
	end if
	SQL = SQL & " ORDER BY A.itemDesc"
	'response.write "<h3>" & SQL & "</h3>"
	'response.end
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	a = 0
	if RS.eof then
	    response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
	else
		BrandID = RS("brandID")
		%>
		<font face="Arial,Helvetica"><h3><%=RS.recordcount%> items found</h3></font>
		<form name="frmEditItem" action="db_update_issues.asp" method="post">
			<table border="1" cellpadding="0" cellspacing="0" width="800">
				<%
				do until RS.eof
					a = a + 1
					%>
					<tr>
						<td>
							<table border="1" cellpadding="3" cellspacing="0" align="left" width="100%">
								<tr>
									<td align="center" valign="top" width="300"><b>Description</b></td>
									<td align="center" valign="top" width="50"><b>Item&nbsp;#</b></td>
									<td align="center" valign="top" width="450"><b>Known&nbsp;Product&nbsp;Issue</b></td>
								</tr>
								<tr>
									<td valign="top" align="center">
										<%=RS("itemDesc")%>
										<input type="hidden" name="itemID<%=a%>" value="<%=RS("itemID")%>">
										<input type="hidden" name="issuesTextOrig<%=a%>" value="<%=RS("issuesText")%>">
									</td>
									<td valign="top" align="center">
										<%=RS("itemID")%>
									</td>
									<td valign="top" align="center">
										<textarea name="issuesText<%=a%>" rows="5" cols="50"><%=RS("issuesText")%></textarea>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="black"><font size="1">&nbsp;</font></td>
					</tr>
					<%
					RS.movenext
				loop
				%>
			</table>
			<p>
				<input type="submit" name="submitted" value="Update">
				<input type="hidden" name="totalCount" value="<%=a%>">
			</p>
		</form>
		<p>&nbsp;</p>
		<%
	end if
	RS.close
	Set RS = nothing
end if
%>

</blockquote>
</blockquote>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
