<%
pageTitle = "Copy Lightning Deal Projects"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<center>
	<%
    cbSite = prepInt(request.form("cbSite"))
    if request.form("btnSubmit") <> "" then
		projectid = prepInt(request.form("cbPrject"))
		newProjectName = SQLQuote(prepStr(request.form("txtCopyTo")))
        sql	=	"SET NOCOUNT ON; " & vbcrlf & _
				"insert into lightningDeal_Projects(Name,siteID,releaseDate,expirationDate,bannerFileName,headerTextTerm1,headerTextDefinition1,headerTextTerm2,headerTextDefinition2,headerFrequency,isActive)" & vbcrlf & _
				"select	'" & newProjectName & "',siteID,releaseDate,expirationDate,bannerFileName,headerTextTerm1,headerTextDefinition1,headerTextTerm2,headerTextDefinition2,headerFrequency,0" & vbcrlf & _
				"from	lightningDeal_Projects" & vbcrlf & _
				"where	id = '" & projectid & "';" & vbcrlf & _
				"SELECT @@IDENTITY AS NewID;"
		set newRS = oConn.execute(sql)
		newProjectID = newRS.Fields("NewID").Value
		
        sql	=	"insert into lightningDeal_Items(itemID,projectID,title,bullet1,bullet2,bullet3,bullet4,bullet5,bullet6,bullet7,bullet8,bullet9,bullet10,description,price,releaseDate,expirationDate,isActive)" & vbcrlf & _
				"select	itemID,'" & newProjectID & "',title,bullet1,bullet2,bullet3,bullet4,bullet5,bullet6,bullet7,bullet8,bullet9,bullet10,description,price,releaseDate,expirationDate,isActive" & vbcrlf & _
				"from	lightningDeal_Items" & vbcrlf & _
				"where	projectID = '" & projectid & "'"
		oConn.execute(sql)
		
        sql	=	"insert into lightningDeal_RegItems(projectID,itemID,sortOrder,isActive)" & vbcrlf & _
				"select	'" & newProjectID & "',itemID,sortOrder,isActive" & vbcrlf & _
				"from	lightningDeal_RegItems" & vbcrlf & _
				"where	projectID = '" & projectid & "'"
		oConn.execute(sql)
		
        sql	=	"insert into lightningDeal_Categories(projectID,name,pic,bullet1,bullet2,link,sortOrder,isActive)" & vbcrlf & _
				"select	'" & newProjectID & "',name,pic,bullet1,bullet2,link,sortOrder,isActive" & vbcrlf & _
				"from	lightningDeal_Categories" & vbcrlf & _
				"where	projectID = '" & projectid & "'"
		oConn.execute(sql)	
		
		sql	=	"insert into lightningDeal_CategoryItems(categoryid, itemid, sortOrder, isActive)" & vbcrlf & _
				"select	b.new_id, a.itemid, a.sortOrder, a.isActive" & vbcrlf & _
				"from	lightningDeal_CategoryItems a join " & vbcrlf & _
				"		(" & vbcrlf & _
				"		select	old.id old_id, old.name old_name, new.id new_id, new.name new_name" & vbcrlf & _
				"		from	lightningDeal_Categories old join lightningDeal_Categories new" & vbcrlf & _
				"			on	old.name = new.name" & vbcrlf & _
				"		where	old.projectid = '" & projectid & "' and new.projectid = '" & newProjectID & "'" & vbcrlf & _
				"		) b" & vbcrlf & _
				"	on	a.categoryid = b.old_id"
		oConn.execute(sql)
		%>
    <div style="text-align:center; width:1100px; font-weight:bold; font-size:14px; color:#333;">
    	New project has been created successfully. Click <a href="/admin/LightningDeals">here</a> to make additional changes.
    </div>
        <%					
    end if
    %>
    <div style="text-align:left; width:1100px;">
        <div style="float:left; padding-bottom:10px; height:20px;">
        	<div style="font-size:18px; font-weight:bold; float:left;">Copy Lightning Deal Project</div>
		</div>
		<form name="frmCopy" method="post">
        <div style="width:100%; float:left; border:1px solid #ccc; border-radius:3px; background-color:#f4f7f9; padding:5px;">
        	<div style="width:600px; float:left;">
                <div style="width:100%; float:left;">
                    <div class="filter-box-header" style="width:200px;">Site</div>
                    <div class="filter-box" style="width:300px; text-align:left;">
                    	<select name="cbSite" onchange="document.frmCopy.submit()">
                        	<option value="0" <%if cbSite = 0 then%>selected<%end if%> >WE</option>
                        	<option value="2" <%if cbSite = 2 then%>selected<%end if%> >CO</option>
                        	<option value="1" <%if cbSite = 1 then%>selected<%end if%> >CA</option>
                        	<option value="3" <%if cbSite = 3 then%>selected<%end if%> >PS</option>
                        	<option value="10" <%if cbSite = 10 then%>selected<%end if%> >TM</option>
                        </select>
					</div>                
                </div>
                <div style="width:100%; float:left; padding-top:5px;">
                    <div class="filter-box-header" style="width:200px;">Copy From</div>
                    <div class="filter-box" style="width:300px; text-align:left;">
                    	<select name="cbPrject">
                        <%
						sql	=	"select	id,Name,siteID,entryDate,releaseDate,expirationDate,bannerFileName,headerTextTerm1,headerTextDefinition1,headerTextTerm2,headerTextDefinition2,headerFrequency,isActive" & vbcrlf & _
								"from	lightningDeal_Projects" & vbcrlf & _
								"where	siteid = '" & cbSite & "'" & vbcrlf & _
								"order by 1"
						set rs = oConn.execute(sql)
						do until rs.eof
						%>
                        	<option value="<%=rs("id")%>"><%=rs("Name")%></option>
                        <%
							rs.movenext
						loop
						%>
                        </select>
                    </div>
                </div>
                <div style="width:100%; float:left; padding-top:5px;">
                    <div class="filter-box-header" style="width:200px;">Copy To</div>
                    <div class="filter-box" style="width:300px; text-align:left;">
                    	<input type="text" name="txtCopyTo" value="New Project Name Here" onClick="this.value=''" style="color:#888;" />
                    </div>
                </div>
                <div style="width:100%; float:left; padding-top:5px;">
                	<input type="submit" name="btnSubmit" value="Copy" />
                </div>
            </div>
        </div>
		</form>
		<div style="clear:both;"></div>
    </div>
</center>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
