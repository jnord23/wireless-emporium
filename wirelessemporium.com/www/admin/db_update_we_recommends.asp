<%
pageTitle = "WE Admin Site - Update Featured Products"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<style>
.grid_row_0	{ background-color:#FFFFFF;}
.grid_row_1	{ background-color:#EAEAEA;}
.mouseover	{ background-color:#9BCEFF;}
</style>
<form action="db_update_we_recommends.asp" method="post" name="frm1">
<table align="center" width="700">
    <tr>
        <td><h1 style="margin:0px;">WE Recommends</h1></td>
    </tr>
    <tr>
        <td style="font-size:12px; padding-bottom:10px;">
            This application allows the user to manipulate the featured products on the home page.
            <br />
             - WE: featured products
            <br />
             - CO: new arrivals
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" align="left" width="700">
                <tr>
                    <td width="100" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-on" id="id_header_WE" onClick="showHide('WE','CO');">WE</td>
                    <td width="100" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-off" id="id_header_CO" onClick="showHide('CO','WE');">CO</td>
                    <td width="500" class="tab-header-off">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" width="700">        
            <div id="tbl_WE" width="700">
                <div style="padding-top:10px; font-weight:bold; font-size:18px; border-bottom:1px solid #000; height:25px; width:700;">
                    <div style="float:left;">Top 5 featured products</div>
                </div>
                <div style="height:500px; overflow:scroll; border:1px solid #000; width:700; font-size:11px;" align="left">
                    <table border="0" cellpadding="5" cellspacing="0">
                <%
                sql = 	"	select	a.id, a.itemid, b.partnumber, a.sortOrder, b.itemdesc, b.itempic" & vbcrlf & _
                        "	from	we_recommends a join we_items b" & vbcrlf & _
                        "		on	a.itemid = b.itemid" & vbcrlf & _
                        "	where	a.site_id = 0" & vbcrlf & _
                        "		and	b.hidelive = 0 and b.inv_qty <> 0 and b.price_our > 0" & vbcrlf & _							
                        "	order by a.sortOrder" & vbcrlf
                session("errorSQL") = sql
                set rs = Server.CreateObject("ADODB.Recordset")
                rs.open sql, oConn, 0, 1								
                nRow = 0
                do until rs.EOF
                    nRow = nRow + 1
                %>
                        <tr class="grid_row_<%=(nRow mod 2)%>" onMouseOver="this.className='mouseover'" onMouseOut="this.className='grid_row_<%=(nRow mod 2)%>'" >

                            <td width="50" align="left"><img src="/productpics/icon/<%=rs("itempic")%>" border="0" width="45" height="45" /></td>
                            <td width="*" align="left" style="font-size:11px;"><div style="border-bottom:1px solid #ccc;">ItemID: <%=rs("itemid")%>, PartNumber: <%=rs("partnumber")%></div><div><%=rs("itemdesc")%></div></td>
                            <td width="40" align="center"><input size="3" type="text" id="txtSort_<%=rs("id")%>" name="txtSort" value="<%=rs("sortOrder")%>" /></td>
                            <td width="100" align="center">
                            	<input type="button" name="btnUpdate" value="Update Sort" onClick="return updateSort('WE','<%=rs("id")%>');" /><br />
                            	<input type="button" name="btnDelete" value="Delete" onClick="return deleteItem('WE','<%=rs("id")%>');" />
							</td>
                        </tr>
                        <input type="hidden" name="hidRecommendID" value="<%=rs("id")%>" />
                <%
                    rs.movenext
                loop
                %>
                    </table>
                </div>
                <div width="700" align="left" style="padding-top:5px;">
		            ItemID: <input size="3" type="text" name="addItemID_WE" value="" /> &nbsp;  &nbsp;  &nbsp; Sort: <input size="2" type="text" name="addSortNum_WE" value="" /> &nbsp; <input type="submit" name="btnSubmit" value="Add Item" onclick="return addItem('WE');" />
                </div>                
            </div>
            <div id="tbl_CO" width="700" style="display:none;">
                <div style="padding-top:10px; font-weight:bold; font-size:18px; border-bottom:1px solid #000; height:25px; width:700;">
                    <div style="float:left;">Top 10 new arrivals</div>
                </div>
                <div style="height:500px; overflow:scroll; border:1px solid #000; width:700; font-size:11px;" align="left">
                    <table border="0" cellpadding="5" cellspacing="0">
                <%
                sql = 	"	select	a.id, a.itemid, b.partnumber, a.sortOrder, b.itemdesc_co, b.itempic_co" & vbcrlf & _
                        "	from	we_recommends a join we_items b" & vbcrlf & _
                        "		on	a.itemid = b.itemid" & vbcrlf & _
                        "	where	a.site_id = 2" & vbcrlf & _
                        "		and	b.hidelive = 0 and b.inv_qty <> 0 and b.price_co > 0" & vbcrlf & _
                        "	order by a.sortOrder" & vbcrlf
                session("errorSQL") = sql
                set rs = Server.CreateObject("ADODB.Recordset")
                rs.open sql, oConn, 0, 1
                nRow = 0
                do until rs.EOF
                    nRow = nRow + 1					
                %>
                        <tr class="grid_row_<%=(nRow mod 2)%>" onMouseOver="this.className='mouseover'" onMouseOut="this.className='grid_row_<%=(nRow mod 2)%>'" >
                            <td width="50" align="left"><img src="/productpics/co_temp/productpics/icon/<%=rs("itempic_co")%>" border="0" width="45" height="45" /></td>
                            <td width="*" align="left" style="font-size:11px;"><div style="border-bottom:1px solid #ccc;">ItemID: <%=rs("itemid")%>, PartNumber: <%=rs("partnumber")%></div><div><%=rs("itemdesc_co")%></div></td>
                            <td width="40" align="center"><input size="3" type="text" id="txtSort_<%=rs("id")%>" name="txtSort" value="<%=rs("sortOrder")%>" /></td>
                            <td width="100" align="center">
                            	<input type="button" name="btnUpdate" value="Update Sort" onClick="return updateSort('CO','<%=rs("id")%>');" /><br />
                            	<input type="button" name="btnDelete" value="Delete" onClick="return deleteItem('CO','<%=rs("id")%>');" />
							</td>
                        </tr>
                        <input type="hidden" name="hidRecommendID" value="<%=rs("id")%>" />
                <%
                    rs.movenext
                loop
                %>
                    </table>
                </div>
                <div width="700" align="left" style="padding-top:5px;">
		            ItemID: <input size="3" type="text" name="addItemID_CO" value="" /> &nbsp;  &nbsp;  &nbsp; Sort: <input size="2" type="text" name="addSortNum_CO" value="" /> &nbsp; <input type="submit" name="btnSubmit" value="Add Item" onclick="return addItem('CO');" />
                </div>
            </div>      
        </td>                                            
    </tr>
    <tr bgcolor="#CCCCCC">
        <td align="center" bgcolor="#CCCCCC">&nbsp;</td>
    </tr>
</table>
</form>
<script>
	function showHide(show,hide)
	{
		document.getElementById('id_header_' + hide).className = "tab-header-off";
		document.getElementById('tbl_' + hide).style.display = "none";
				
		document.getElementById('id_header_' + show).className = "tab-header-on";
		document.getElementById('tbl_' + show).style.display = "";
	}

	function updateSort(site,id)
	{
		var sortNum = document.getElementById('txtSort_' + id).value;
		ajax('/ajax/admin/ajaxWeRecommend.asp?site=' + site + '&updateID=' + id + '&updateSortNum=' + sortNum, 'tbl_' + site);
		return false;
	}
	
	function deleteItem(site,id)
	{
		ajax('/ajax/admin/ajaxWeRecommend.asp?site=' + site + '&deleteID=' + id, 'tbl_' + site);
		return false;
	}
	
	function addItem(site)
	{
		var addItemID = "";
		var addSortNum = "";
		if (site == "WE")
		{
			addItemID = frm1.addItemID_WE.value;
			addSortNum = frm1.addSortNum_WE.value;
		} else
		{
			addItemID = frm1.addItemID_CO.value;
			addSortNum = frm1.addSortNum_CO.value;
		}
			
		ajax('/ajax/admin/ajaxWeRecommend.asp?site=' + site + '&addItemID=' + addItemID + '&addSortNum=' + addSortNum, 'tbl_' + site);
		return false;
	}
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
