<%
dim siteID : siteID = prepInt(request.Form("siteID"))

if siteID = 0 then
	pageTitle = "WE Mobile Orders"
elseif siteID = 2 then
	pageTitle = "CO Mobile Orders"
end if
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<style type="text/css">
	.fl { float:left; }
	.mobileSelectBox { display:table; width:200px; margin:10px auto; }
	.mobileSelectLabel { font-weight:bold; font-size:14px; margin-right:10px; }
</style>
<center>
	<form name="mobileSelect" action="/admin/mobileOrders.asp" method="post">
	<div class="mobileSelectBox">
    	<div class="fl mobileSelectLabel">Mobile Site:</div>
        <div class="fl mobileSelectControl">
        	<select name="siteID" onchange="document.mobileSelect.submit();">
            	<option value="0">WE Mobile</option>
                <option value="2"<% if siteID = 2 then %> selected="selected"<% end if %>>CO Mobile</option>
            </select>
        </div>
    </div>
    </form>
	<div style="width:400px;" align="left">
        <h3><%=pageTitle%></h3>
        <table border="1" cellpadding="3" cellspacing="0" width="100%" style="font-size:11px; border-collapse:collapse;">
        	<tr>
            	<td align="center">Date</td>
                <td align="center">Sales</td>
                <td align="center">LastWeek</td>
                <td align="center">Diff.</td>
			</tr>
        <%
		sql = 	"select	a.orderdatetime date1, a.cnt cnt1, a.sales sales1" & vbcrlf & _
				"	,	a.mobileCnt mobileCnt1, a.mobileSales mobileSales1" & vbcrlf & _
				"	,	b.orderdatetime date2, b.cnt cnt2, b.sales sales2" & vbcrlf & _
				"	,	b.mobileCnt mobileCnt2, b.mobileSales mobileSales2" & vbcrlf & _
				"from	(	" & vbcrlf & _
				"		select	convert(varchar(10), orderdatetime, 20) orderdatetime" & vbcrlf & _
				"			,	count(*) cnt, sum(cast(ordergrandtotal as money)) sales" & vbcrlf & _
				"			,	sum(case when store = " & siteID & " and mobileSite = 1 then 1 else 0 end) mobileCnt" & vbcrlf & _
				"			,	sum(case when store = " & siteID & " and mobileSite = 1 then cast(ordergrandtotal as money) else 0 end) mobileSales" & vbcrlf & _
				"		from	we_orders" & vbcrlf & _
				"		where	approved = 1" & vbcrlf & _
				"			and	(cancelled = 0 or cancelled is null)" & vbcrlf & _
				"			and	orderdatetime >= dateadd(m, -1, getdate())" & vbcrlf & _
				"			--and mobileSite = 1" & vbcrlf & _
				"			--and	store = " & siteID & "	" & vbcrlf & _
				"			and	parentOrderID is null" & vbcrlf & _
				"		group by convert(varchar(10), orderdatetime, 20)	" & vbcrlf & _
				"		) a join " & vbcrlf & _
				"		(" & vbcrlf & _
				"		select	convert(varchar(10), orderdatetime, 20) orderdatetime" & vbcrlf & _
				"			,	count(*) cnt, sum(cast(ordergrandtotal as money)) sales" & vbcrlf & _
				"			,	sum(case when store = " & siteID & " and mobileSite = 1 then 1 else 0 end) mobileCnt" & vbcrlf & _
				"			,	sum(case when store = " & siteID & " and mobileSite = 1 then cast(ordergrandtotal as money) else 0 end) mobileSales" & vbcrlf & _
				"		from	we_orders" & vbcrlf & _
				"		where	approved = 1" & vbcrlf & _
				"			and	(cancelled = 0 or cancelled is null)" & vbcrlf & _
				"			and	orderdatetime >= dateadd(m, -1, getdate())" & vbcrlf & _
				"			--and mobileSite = 1" & vbcrlf & _
				"			--and	store = " & siteID & "	" & vbcrlf & _
				"			and	parentOrderID is null" & vbcrlf & _
				"		group by convert(varchar(10), orderdatetime, 20)	" & vbcrlf & _
				"		) b" & vbcrlf & _
				"	on	dateadd(dd, -7, a.orderdatetime) = b.orderdatetime" & vbcrlf & _
				"order by 1 desc"
		set rs = oConn.execute(sql)
		do until rs.eof
			date1 = rs("date1")
			cnt1 = rs("cnt1")
			sales1 = rs("sales1")
			mobileCnt1 = rs("mobileCnt1")
			mobileSales1 = rs("mobileSales1")
			
			date2 = rs("date2")
			cnt2 = rs("cnt2")
			sales2 = rs("sales2")
			mobileCnt2 = rs("mobileCnt2")
			mobileSales2 = rs("mobileSales2")
			
			diff_cnt = rs("cnt1") - rs("cnt2")
			diff_sales = rs("sales1") - rs("sales2")
			diff_mobileCnt = rs("mobileCnt1") - rs("mobileCnt2")
			diff_mobileSales = rs("mobileSales1") - rs("mobileSales2")
		%>
        	<tr>
            	<td align="center"><%=date1%></td>
                <td align="center">
                	<div style="border-bottom:1px solid #ccc;"><%=formatnumber(cnt1,0)%>&nbsp;(<%=formatcurrency(sales1)%>)</div>
                	<div><%=formatnumber(mobileCnt1,0)%>&nbsp;(<%=formatcurrency(mobileSales1)%>)</div>
				</td>
                <td align="center">
                	<div style="border-bottom:1px solid #ccc;"><%=formatnumber(cnt2,0)%>&nbsp;(<%=formatcurrency(sales2)%>)</div>
                	<div><%=formatnumber(mobileCnt2,0)%>&nbsp;(<%=formatcurrency(mobileSales2)%>)</div>
				</td>
                <td align="center">
                	<div style="border-bottom:1px solid #ccc;">
						<%=formatnumber(diff_cnt,0)%>&nbsp;
                        <%if diff_sales < 0 then%>
        	                <span style="color:#f00;"><%=formatcurrency(diff_sales)%></span>
                        <%else%>
            	            (<%=formatcurrency(diff_sales)%>)
                        <%end if%>
					</div>
                	<div>
						<%=formatnumber(diff_mobileCnt,0)%>&nbsp;
                        <%if diff_mobileSales < 0 then%>
	                        <span style="color:#f00;"><%=formatcurrency(diff_mobileSales)%></span>
                        <%else%>
    	                    (<%=formatcurrency(diff_mobileSales)%>)
                        <%end if%>
					</div>
				</td>
			</tr>
        <%
			rs.movenext
		loop
		%>
        </table>
    </div>
</center>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->