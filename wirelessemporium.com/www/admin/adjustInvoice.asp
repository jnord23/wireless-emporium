<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/admin/formatNotes.asp"-->
<%
	dim accountID, orderID, updateType, currentDateTime, curOrderNotes, AddNotes, bOrderNote, storeid
	accountID 		= 	request("accountid")
	orderID			= 	request("orderID")
	updateType		= 	request("updateType")
	storeid			=	request("storeid")
	currentDateTime	=	now
	curOrderNotes	=	""
	bOrderNote		=	false
	
	dim siteURL, lnkLogo, siteColor, siteDesc, useStore
	
	set objRsStore = oConn.execute("select * from xstore where site_id = '" & storeid & "'")
	if not objRsStore.eof then
		siteURL		=	objRsStore("site_url")
		lnkLogo		=	objRsStore("logo_big")
		siteColor	=	objRsStore("color")
		siteDesc	=	objRsStore("longDesc")
		useStore	=	objRsStore("shortDesc")
	else
		response.redirect "/admin/view_invoice.asp?retCustMsg=Cannot find store information&orderid=" & orderID & "&accountid=" & accountid
		response.End	
	end if
	objRsStore.close
	set objRsStore = nothing

	if lcase(useStore) = "tm" then useStore = "er" end if
'	response.write "[request.QueryString:" & request.QueryString & "]<br>"
'	response.write "[request.Form:" & request.Form & "]<br>"

	sql	=	"	select	*	" & vbcrlf & _
			"	from	" & useStore & "_accounts" & vbcrlf & _
			"	where	accountid = '" & accountid & "'" & vbcrlf
			
	set objRsAccount = oConn.execute(sql)
	
	if objRsAccount.EOF then
		response.redirect "/admin/view_invoice.asp?retCustMsg=Cannot find account information&orderid=" & orderID & "&accountid=" & accountid
		response.End
	end if
	
	sql	=	"	select	*	" & vbcrlf & _
			"	from	we_ordernotes" & vbcrlf & _
			"	where	orderid = '" & orderID & "'" & vbcrlf
			
	set objRsNotes = oConn.execute(sql)
	
	if not objRsNotes.EOF then
		bOrderNote		=	true
		curOrderNotes 	= 	objRsNotes("ordernotes")
	end if
	
	if "custInfo" = updateType then
		dim newFName, newLName, newEmailAddress
		dim newBAddress1, newBAddress2, newBCity, newBState, newBZip, newBCountry
		dim newSAddress1, newSAddress2, newSCity, newSState, newSZip, newSCountry
		
		newFName			=	SQLquote(request("newFName"))
		newLName			=	SQLquote(request("newLName"))
		newEmailAddress		=	SQLquote(request("newEmailAddress"))
		
		newBAddress1		=	SQLquote(request("newBAddress1"))
		newBAddress2		=	SQLquote(request("newBAddress2"))
		newBCity			=	SQLquote(request("newBCity"))
		newBState			=	SQLquote(request("newBState"))
		newBZip				=	SQLquote(request("newBZip"))
		newBCountry			=	SQLquote(request("newBCountry"))
		
		newSAddress1		=	SQLquote(request("newSAddress1"))
		newSAddress2		=	SQLquote(request("newSAddress2"))
		newSCity			=	SQLquote(request("newSCity"))
		newSState			=	SQLquote(request("newSState"))
		newSZip				=	SQLquote(request("newSZip"))
		newSCountry			=	SQLquote(request("newSCountry"))
		
		oldFName			=	SQLquote(objRsAccount("fname"))
		oldLName			=	SQLquote(objRsAccount("lname"))
		oldEmailAddress		=	SQLquote(objRsAccount("email"))
		
		oldBAddress1		=	SQLquote(objRsAccount("bAddress1"))
		oldBAddress2		=	SQLquote(objRsAccount("bAddress2"))
		oldBCity			=	SQLquote(objRsAccount("bCity"))
		oldBState			=	SQLquote(objRsAccount("bState"))
		oldBZip				=	SQLquote(objRsAccount("bZip"))
		oldBCountry			=	SQLquote(objRsAccount("bCountry"))
		
		oldSAddress1		=	SQLquote(objRsAccount("sAddress1"))
		oldSAddress2		=	SQLquote(objRsAccount("sAddress2"))
		oldSCity			=	SQLquote(objRsAccount("sCity"))
		oldSState			=	SQLquote(objRsAccount("sState"))
		oldSZip				=	SQLquote(objRsAccount("sZip"))
		oldSCountry			=	SQLquote(objRsAccount("sCountry"))		

		AddNotes = "- customer information updated"

		if oldFName <> newFName 			then AddNotes = AddNotes & vbcrlf & "-- firstname: " & oldFName & " to " & newFName 				end if
		if oldLName <> newLName 			then AddNotes = AddNotes & vbcrlf & "-- lastname: " & oldLName & " to " & newLName 				end if
		if oldBAddress1 <> newBAddress1		then AddNotes = AddNotes & vbcrlf & "-- bill address1: " & oldBAddress1 & " to " & newBAddress1 	end if
		if oldBAddress2 <> newBAddress2		then AddNotes = AddNotes & vbcrlf & "-- bill address2: " & oldBAddress2 & " to " & newBAddress2 	end if
		if oldBCity <> newBCity 			then AddNotes = AddNotes & vbcrlf & "-- bill city: " & oldBCity & " to " & newBCity 				end if
		if oldBState <> newBState 			then AddNotes = AddNotes & vbcrlf & "-- bill state: " & oldBState & " to " & newBState 			end if
		if oldBZip <> newBZip 				then AddNotes = AddNotes & vbcrlf & "-- bill zipcode: " & oldBZip & " to " & newBZip 			end if
		if oldBCountry <> newBCountry 		then AddNotes = AddNotes & vbcrlf & "-- bill country: " & oldBCountry & " to " & newBCountry 	end if
		if oldSAddress1 <> newSAddress1 	then AddNotes = AddNotes & vbcrlf & "-- ship address1: " & oldSAddress1 & " to " & newSAddress1 	end if
		if oldSAddress2 <> newSAddress2 	then AddNotes = AddNotes & vbcrlf & "-- ship address2: " & oldSAddress2 & " to " & newSAddress2 	end if
		if oldSCity <> newSCity 			then AddNotes = AddNotes & vbcrlf & "-- ship city: " & oldSCity & " to " & newSCity 				end if
		if oldSState <> newSState 			then AddNotes = AddNotes & vbcrlf & "-- ship state: " & oldSState & " to " & newSState 			end if
		if oldSZip <> newSZip 				then AddNotes = AddNotes & vbcrlf & "-- ship zipcode: " & oldSZip & " to " & newSZip 			end if
		if oldSCountry <> newSCountry 		then AddNotes = AddNotes & vbcrlf & "-- ship country: " & oldSCountry & " to " & newSCountry 	end if
		if oldEmailAddress <> newEmailAddress 	then AddNotes = AddNotes & vbcrlf & "-- email address: " & oldEmailAddress & " to " & newEmailAddress 	end if
		
		AddNotes = SQLquote(curOrderNotes) & vbcrlf & formatNotes(AddNotes)

'		response.write "=============================================================<br>"
'		response.write "<pre>" & objRsNotes("ordernotes") & "</pre><br>"
'		response.write "=============================================================<br>"
'		response.write "<pre>" & AddNotes & "</pre><br>"
'		response.write "=============================================================<br>"
'		response.write "<pre>" & formatNotes(AddNotes) & "</pre><br>"
'		response.end
		
		if bOrderNote then 
			sql = 	"	update	we_ordernotes	" & vbcrlf & _
					"	set		ordernotes = '" & AddNotes & "'" & vbcrlf & _
					"	where	orderid = '" & orderID & "'" & vbcrlf
		else
			sql = 	"	insert into we_ordernotes(orderid, ordernotes) values('" & orderID & "', '" & AddNotes & "') "
		end if
		
		session("errorSQL") = sql
		oConn.execute(sql)

		sql = 	"	update	" & useStore & "_accounts	" & vbcrlf & _
				"	set		fname = '" & newFName & "'" & vbcrlf & _
				"		,	lname = '" & newLName & "'" & vbcrlf & _
				"		,	bAddress1 = '" & newBAddress1 & "'" & vbcrlf & _
				"		,	bAddress2 = '" & newBAddress2 & "'" & vbcrlf & _
				"		,	bCity = '" & newBCity & "'" & vbcrlf & _
				"		,	bState = '" & newBState & "'" & vbcrlf & _
				"		,	bZip = '" & newBZip & "'" & vbcrlf & _
				"		,	bCountry = '" & newBCountry & "'" & vbcrlf & _
				"		,	sAddress1 = '" & newSAddress1 & "'" & vbcrlf & _
				"		,	sAddress2 = '" & newSAddress2 & "'" & vbcrlf & _
				"		,	sCity = '" & newSCity & "'" & vbcrlf & _
				"		,	sState = '" & newSState & "'" & vbcrlf & _
				"		,	sZip = '" & newSZip & "'" & vbcrlf & _
				"		,	sCountry = '" & newSCountry & "'" & vbcrlf & _
				"		,	email = '" & newEmailAddress & "'" & vbcrlf & _
				"	where	accountid = '" & accountid & "'" & vbcrlf

		session("errorSQL") = sql
		oConn.execute(sql)
'		session("errorSQL") = "sp_ModifyEmailOpt '" & SQLquote(sFname) & "','" & sEmail & "','" & sEmailOpt &"'"
'		oConn.execute ("sp_ModifyEmailOpt '" & SQLquote(sFname) & "','" & sEmail & "','" & sEmailOpt &"'")
		session("errorSQL") = "sp_ModifyEmailOpt '" & SQLquote(sFname) & "','" & sEmail & "','Y'"
		oConn.execute ("sp_ModifyEmailOpt '" & SQLquote(sFname) & "','" & sEmail & "','Y'")

		objRsAccount.close
		set objRsAccount = nothing
		objRsNotes.close
		set objRsNotes = nothing		
		oConn.Close
		set oConn = nothing
	
		response.redirect "/admin/view_invoice.asp?retCustMsg=Customer information updated successfully&orderid=" & orderID & "&accountid=" & accountid
		response.End
	elseif "orderInfo" = updateType then
		
		dim curShipType, newShipType
		curShipType	=	SQLquote(request("curShipType"))
		newShipType	=	SQLquote(request("newShipType"))

		AddNotes = "- shipType updated"
		AddNotes = AddNotes & vbcrlf & "-- shiptype: " & curShipType & " to " & newShipType
		AddNotes = SQLquote(curOrderNotes) & vbcrlf & formatNotes(AddNotes)

		if bOrderNote then 
			sql = 	"	update	we_ordernotes	" & vbcrlf & _
					"	set		ordernotes = '" & AddNotes & "'" & vbcrlf & _
					"	where	orderid = '" & orderID & "'" & vbcrlf
		else
			sql = 	"	insert into we_ordernotes(orderid, ordernotes) values('" & orderID & "', '" & AddNotes & "') "
		end if

		session("errorSQL") = sql
		oConn.execute(sql)
						
		sql	=	"	update	we_orders	" & vbcrlf & _
				"	set		shiptype = '" & newShipType & "'" & vbcrlf & _
				"	where	orderid = '" & orderID & "'" & vbcrlf 

		session("errorSQL") = sql
		oConn.execute(sql)
				
		objRsAccount.close
		set objRsAccount = nothing
		objRsNotes.close
		set objRsNotes = nothing		
		oConn.Close
		set oConn = nothing

		response.redirect "/admin/view_invoice.asp?orderid=" & orderID & "&accountid=" & accountid
	end if
%>