<%
pageTitle = "Admin - Generate Inventory Report by Brand/Type/Model"
dim func
func = request.querystring("func")
if func = "" then
	header = 1
else
	header = 0
end if
%>

<!--#include virtual="/includes/admin/admin_top.asp"-->

<%
select case func
	case "delete"
		SQL = "DELETE FROM we_Report_Inventory WHERE id='" & request.querystring("id") & "'"
		response.write "<h3>Deleted!</h3><p><a href=""report_Inventory_Brand_Model.asp"">BACK TO REPORT CRITERIA</a></p>" & vbcrlf
		oConn.execute SQL
	case else
		if request.form("submitted") <> "" or func = "run" then
			if func = "new" then
				brandID = request.form("brandID")
				typeID = request.form("typeID")
				modelID = replace(request.form("modelID")," ","")
				if brandID = "" or typeID = "" or modelID = "" then
					response.write "<h3>You must select a Brand, a Category, and at least one Model!</h3>" & vbcrlf
					response.write "<p><a href=""javascript:history.back();"">BACK</a></p>" & vbcrlf
					response.end
				end if
				maxQty = replace(request.form("maxQty")," ","")
				qtyArray = split(maxQty,",")
				for a = 0 to Ubound(qtyArray)
					if qtyArray(a) <> "" then strMaxQty = strMaxQty & qtyArray(a) & ","
				next
				strMaxQty = left(strMaxQty,len(strMaxQty)-1)
				qtyArray = split(strMaxQty,",")
				modelArray = split(modelID,",")
				if Ubound(modelArray) <> Ubound(qtyArray) or not isNumeric(replace(strMaxQty,",","")) then
					response.write "<h3>Each Model must have a valid Max Qty amount!</h3>" & vbcrlf
					response.write "<p><a href=""javascript:history.back();"">BACK</a></p>" & vbcrlf
					response.end
				end if
				if request.form("reportid") <> "" then
					SQL = "UPDATE we_Report_Inventory SET"
					SQL = SQL & " brandID='" & brandID & "',"
					SQL = SQL & " typeID='" & typeID & "',"
					SQL = SQL & " modelID='" & modelID & "',"
					SQL = SQL & " maxQty='" & strMaxQty & "'"
					SQL = SQL & " WHERE id='" & request.form("reportid") & "'"
				else
					SQL = "INSERT INTO we_Report_Inventory (brandID,typeID,modelID,maxQty) VALUES ("
					SQL = SQL & "'" & brandID & "',"
					SQL = SQL & "'" & typeID & "',"
					SQL = SQL & "'" & modelID & "',"
					SQL = SQL & "'" & strMaxQty & "')"
				end if
				'response.write "<p>" & SQL & "</p>" & vbcrlf
				oConn.execute SQL
			else
				SQL = "SELECT * FROM we_Report_Inventory WHERE id='" & request.querystring("id") & "'"
				set RS = Server.CreateObject("ADODB.Recordset")
				RS.open SQL, oConn, 3, 3
				if not RS.eof then
					brandID = RS("brandID")
					typeID = RS("typeID")
					modelID = RS("modelID")
					modelArray = split(modelID,",")
					maxQty = RS("maxQty")
					qtyArray = split(maxQty,",")
				end if
			end if
			
			dim vendor, vendorArray, vendorSQL, vCount
			vendorSQL = ""
			vendor = request.querystring("vendor")
			if request.querystring("vendor") <> "" then
				vendorSQL = " AND ("
				vendorArray = split(vendor,", ")
				for vCount = 0 to Ubound(vendorArray)
					vendorSQL = vendorSQL & " A.vendor LIKE '%" & vendorArray(vCount) & "%' OR"
				next
				vendorSQL = left(vendorSQL,len(vendorSQL)-3) & ")"
			end if
			
			SQL = "SELECT A.itemDesc, A.vendor, A.inv_qty, B.modelID, B.modelName FROM we_Items A INNER JOIN we_Models B ON A.modelID=B.modelID"
			SQL = SQL & " WHERE A.brandID='" & brandID & "' AND A.typeID='" & typeID & "' AND A.modelID IN (" & modelID & ") AND inv_qty > -1"
			SQL = SQL & " AND A.PartNumber NOT LIKE 'FP1-%'"
			SQL = SQL & " AND A.HideLive = 0"
			if vendorSQL <> "" then SQL = SQL & vendorSQL
			SQL = SQL & " ORDER BY A.modelID, A.inv_qty, A.itemDesc"
			'response.write "<p>" & SQL & "</p>" & vbcrlf
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			holdModelID = 99999
			response.write "<p><a href=""report_Inventory_Brand_Model.asp"">BACK TO REPORT CRITERIA</a></p>" & vbcrlf
			dim strToWrite, fs, path, myFile
			set fs = Server.CreateObject("Scripting.FileSystemObject")
			path = Server.MapPath("tempCSV") & "\report_Inventory_Brand_Model.csv"
			set myFile = fs.CreateTextFile(path)
			%>
			<p><a href="/admin/tempCSV/report_Inventory_Brand_Model.csv">Download CSV File</a></p>
			<table border="0" cellpadding="0" cellspacing="0" width="300">
				<form action="report_Inventory_Brand_Model.asp" method="get">
				<tr>
					<td width="50%" align="left" valign="top">
						<select name="vendor" multiple="multiple" size="4">
							<option value="" selected>Select Vendor(s):</option>
							<option value="AW">AW</option>
							<option value="BA">BA</option>
							<option value="BC">BC</option>
							<option value="BD">BD</option>
							<option value="CD">CD</option>
							<option value="DW">DW</option>
							<option value="EM">EM</option>
							<option value="EW">EW</option>
							<option value="OC">OC</option>
							<option value="OR">OR</option>
							<option value="OR">VL</option>
						</select>
					</td>
					<td width="50%" align="center" valign="middle">
						<p>
							<input type="hidden" name="id" value="<%=request.querystring("id")%>">
							<input type="hidden" name="func" value="<%=request.querystring("func")%>">
							<input type="submit" value="Filter By Vendor(s)">
						</p>
					</td>
				</tr>
				</form>
			</table>
			<table border="1" class="smlText">
				<%
				do until RS.eof
					strToWrite = ""
					for a = 0 to Ubound(modelArray)
						if modelArray(a) = cStr(RS("modelID")) then thisQty = qtyArray(a)
					next
					if holdModelID <> RS("modelID") then
						strToWrite = strToWrite & chr(34) & RS("modelName") & " (Inv: " & thisQty & ")" & chr(34) & ","
						strToWrite = strToWrite & chr(34) & chr(34) & ","
						strToWrite = strToWrite & chr(34) & chr(34)
						myFile.WriteLine strToWrite
						strToWrite = ""
						%>
						<tr>
							<td valign="top" colspan="3"><b><%=RS("modelName") & " (Inv: " & thisQty & ")"%></b></td>
						</tr>
						<%
					end if
					if cDbl(thisQty) >= RS("inv_qty") then
						strToWrite = strToWrite & chr(34) & RS("itemDesc") & chr(34) & ","
						strToWrite = strToWrite & chr(34) & RS("inv_qty") & chr(34) & ","
						strToWrite = strToWrite & chr(34) & RS("vendor") & chr(34)
						%>
						<tr>
							<td valign="top"><%=RS("itemDesc")%></td>
							<td valign="top" align="center"><b>&nbsp;<%=RS("inv_qty")%>&nbsp;</b></td>
							<td valign="top" align="center">&nbsp;<%=RS("vendor")%>&nbsp;</td>
						</tr>
						<%
					end if
					holdModelID = RS("modelID")
					if strToWrite <> "" then myFile.WriteLine strToWrite
					RS.movenext
				loop
				%>
			</table>
			<%
			RS.close
			set RS = nothing
			myFile.close()
			set myFile = nothing
			set fs = nothing
		else
			brandID = request.form("brandID")
			SQL = "SELECT A.*,B.brandName,C.typeName FROM (we_Report_Inventory A INNER JOIN we_Brands B ON A.brandID=B.brandID)"
			SQL = SQL & " INNER JOIN we_Types C ON A.typeID=C.typeID ORDER BY A.id"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			%>
			<p class="biggerText"><%=pageTitle%></p>
			<table border="1">
				<%
				do until RS.eof
					modelArray = split(RS("modelID"),",")
					qtyArray = split(RS("maxQty"),",")
					%>
					<tr>
						<td valign="top"><b>Report #<%=RS("id")%></b></td>
						<td valign="top"><%=RS("brandName")%></td>
						<td valign="top"><%=RS("typeName")%></td>
						<td valign="top">
							<%
							for a = 0 to Ubound(modelArray)
								SQL = "SELECT modelName FROM we_Models WHERE modelID='" & modelArray(a) & "'"
								set RS2 = Server.CreateObject("ADODB.Recordset")
								RS2.open SQL, oConn, 3, 3
								if not RS2.eof then response.write RS2("modelName") & "&nbsp;:&nbsp;<b>" & qtyArray(a) & "</b><br>" & vbcrlf
							next
							%>
						</td>
						<td valign="top">
							<a href="report_Inventory_Brand_Model.asp?id=<%=RS("id")%>&func=edit">Edit</a><br>
							<a href="report_Inventory_Brand_Model.asp?id=<%=RS("id")%>&func=delete">Delete</a><br>
							<a href="report_Inventory_Brand_Model.asp?id=<%=RS("id")%>&func=run">Run</a>
						</td>
					</tr>
					<%
					RS.movenext
				loop
				%>
			</table>
			<%
			RS.close
			set RS = nothing
			%>
			<form action="report_Inventory_Brand_Model.asp?func=new" name="frmSearch" method="post">
				<%
				if func = "edit" then
					response.write "<input type=""hidden"" name=""reportid"" value=""" & request.querystring("id") & """>" & vbcrlf
					SQL = "SELECT A.*,B.brandName,C.typeName FROM (we_Report_Inventory A INNER JOIN we_Brands B ON A.brandID=B.brandID)"
					SQL = SQL & " INNER JOIN we_Types C ON A.typeID=C.typeID WHERE id='" & request.querystring("id") & "' ORDER BY A.id"
					set RS = Server.CreateObject("ADODB.Recordset")
					RS.open SQL, oConn, 3, 3
					brandID = cStr(RS("brandID"))
					typeID = cStr(RS("typeID"))
					modelID = RS("modelID")
					modelArray = split(modelID,",")
					maxQty = RS("maxQty")
					qtyArray = split(maxQty,",")
				end if
				%>
				<h3>Select a Brand:</h3>
				<p>
					<select name="brandID" onChange="document.frmSearch.submit();">
						<option value=""></option>
						<%
						SQL = "SELECT * FROM WE_Brands ORDER BY brandName"
						set RS = Server.CreateObject("ADODB.Recordset")
						RS.open SQL, oConn, 3, 3
						do until RS.eof
							%>
							<option value="<%=RS("brandID")%>"<%if brandID = cStr(RS("brandID")) then response.write " selected"%>><%=RS("brandName")%></option>
							<%
							RS.movenext
						loop
						%>
					</select>
				</p>
				<h3>Select a Type:</h3>
				<p>
					<select name="typeID">
						<option value=""></option>
						<%
						SQL = "SELECT * FROM WE_Types ORDER BY typeName"
						set RS = Server.CreateObject("ADODB.Recordset")
						RS.open SQL, oConn, 3, 3
						do until RS.eof
							%>
							<option value="<%=RS("TypeID")%>"<%if typeID = cStr(RS("TypeID")) then response.write " selected"%>><%=RS("TypeName")%></option>
							<%
							RS.movenext
						loop
						%>
					</select>
				</p>
				<%
				if brandID <> "" then
					%>
					<h3>Select Model(s):</h3>
					<p>
						<%
						SQL = "SELECT * FROM WE_Models WHERE brandID='" & brandID & "'"
						set RS = Server.CreateObject("ADODB.Recordset")
						RS.open SQL, oConn, 3, 3
						do until RS.eof
							checked = ""
							thisQty = ""
							if modelID <> "" and maxQty <> "" then
								for a = 0 to Ubound(modelArray)
									if modelArray(a) = cStr(RS("modelID")) then
										checked = " checked"
										thisQty = cStr(qtyArray(a))
									end if
								next
							end if
							%>
							<input type="checkbox" name="modelID" value="<%=RS("modelID")%>"<%=checked%>>&nbsp;<%=RS("modelName")%>
							&nbsp;&nbsp;&nbsp;<input type="text" name="maxQty" size="2" value="<%=thisQty%>">&nbsp;Max&nbsp;Qty<br>
							<%
							RS.movenext
						loop
						%>
					</p>
					<%
				end if
				%>
				<p class="normalText"><input type="submit" name="submitted" value="Submit"></p>
			</form>
			<%
		end if
end select
%>

<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
