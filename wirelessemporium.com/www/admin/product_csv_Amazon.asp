<%
response.buffer = false
pageTitle = "Create Amazon Product List CSV"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<%
Server.ScriptTimeout = 2000 'seconds

dim Upload, path
set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = true
path = replace(server.mappath("tempCSV") & "\","admin\","")
Count = Upload.Save(path)

if Upload.form("upload") <> "" then
	if Count = 0 then
		response.write "No file selected."
		response.end
	else
		dim filesys, file, filename, WriteFile
		set filesys = CreateObject("Scripting.FileSystemObject")
		set File = Upload.Files(1)
		myFile = File.path
		
		filename = "productList_Amazon.csv"
		set WriteFile = filesys.CreateTextFile(path & "\" & filename)
		
		dim txtConn, myStr
		set txtConn = Server.CreateObject("ADODB.Connection")
		myStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & myFile & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"";"
		txtConn.ConnectionString = myStr
		txtConn.ConnectionTimeout = 1000		'seconds
		txtConn.Open
		
		SQL = "SELECT * FROM [inv_feed$]"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, txtConn, 3, 3
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			strToWrite = "sku,standard-product-id,product-id-type,title,manufacturer,brand,mfr-part-number,merchant-catalog-number,bullet-point1,bullet-point2,bullet-point3,bullet-point4,bullet-point5,description,product_type,legal-disclaimer,prop-65,cpsia-warning1,cpsia-warning2,cpsia-warning3,cpsia-warning4,cpsia-warning-description,item-type,used-for1,used-for2,used-for3,used-for4,used-for5,other-item-attributes1,other-item-attributes2,other-item-attributes3,other-item-attributes4,other-item-attributes5,subject-content1,subject-content2,subject-content3,subject-content4,subject-content5,"
			strToWrite = strToWrite & "search-terms1,search-terms2,search-terms3,search-terms4,search-terms5,platinum-keywords1,platinum-keywords2,platinum-keywords3,platinum-keywords4,platinum-keywords5,main-image-url,other-image-url1,other-image-url2,other-image-url3,other-image-url4,other-image-url5,other-image-url6,other-image-url7,other-image-url8,item-weight-unit-of-measure,item-weight,item-length-unit-of-measure,item-length,item-height,item-width,shipping-weight-unit-of-measure,shipping-weight,product-tax-code,launch-date,release-date,msrp,item-price,sale-price,sale-from-date,sale-through-date,"
			strToWrite = strToWrite & "quantity,leadtime-to-ship,restock-date,max-aggregate-ship-quantity,is-gift-message-available,is-giftwrap-available,is-discontinued-by-manufacturer,update-delete,target-audience-keywords1,target-audience-keywords2,target-audience-keywords3,item-condition,condition-note,registered-parameter"
			writefile.WriteLine strToWrite
			%>
			<h3><%=RS.recordcount%> records found</h3>
			<%
			dim PartNumber, MaxInvQty
			do until RS.eof
				PartNumber = left(RS("sku"),15)
				sql = "if (select count(*) from we_amazonProducts where partNumber = '" & PartNumber & "') < 1 insert into we_amazonProducts (partNumber) values('" & PartNumber & "')"
				session("errorSQL") = sql
				oConn.execute(sql)
				
				SQL = "SELECT MAX (inv_qty) AS MaxInvQty FROM we_items WHERE PartNumber='" & PartNumber & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				strToWrite = chr(34) & RS("sku") & chr(34) & ","
				for a = 1 to 72
					strToWrite = strToWrite & ","
				next
				MaxInvQty = RS2("MaxInvQty")
				if MaxInvQty <= 0 then
					MaxInvQty = 0
				elseif MaxInvQty <= 4 then
					MaxInvQty = 1
				else
					MaxInvQty = MaxInvQty - 3
				end if
				strToWrite = strToWrite & MaxInvQty & ","
				for a = 1 to 12
					strToWrite = strToWrite & ","
				next
				writefile.WriteLine strToWrite
				RS.movenext
			loop
		end if
		response.write "<p>Download your file <a href=""/tempCSV/" & filename & """>HERE</a>.</p>" & vbcrlf
		
		writefile.close()
		set writefile = nothing
		set filesys = nothing
		set File = nothing
		
		RS.close
		set RS = nothing
		RS2.close
		set RS2 = nothing
		txtConn.close
		set txtConn = nothing
	end if
end if
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<form enctype="multipart/form-data" action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="POST">
							<p>Product List upload for Amazon</p>
							<p><input type="FILE" name="FILE1"></p>
							<p><input type="SUBMIT" name="upload" value="Upload"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
