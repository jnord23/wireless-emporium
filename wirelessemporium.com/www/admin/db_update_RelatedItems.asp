<%
pageTitle = "Admin - Update WE Database - Update Related Items"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<script type="text/javascript" src="/includes/admin/checkbox.js"></script>
<script>
function onSetMasterCheckBox(frm)
{
	if (isAllChecked(frm.chkDelete))			frm.chkMaster.checked = true;
	else if (isAllUnChecked(frm.chkDelete))		frm.chkMaster.checked = false;
}

</script>

<blockquote>
<blockquote>

<font face="Arial,Helvetica">
	<p><a href="/admin/db_search_RelatedItems.asp">Back to Brands</a></p>
</font>

<%
BrandID = request("BrandID")
ModelID = request("ModelID")
TypeID = request("TypeID")
ItemID = request("ItemID")
strError = ""

if BrandID = "" then strError = strError & "<li>You must select a Brand.</li>"
if ModelID = "" then strError = strError & "<li>You must select at least one Model.</li>"
'response.write "request.Form: " & request.Form & "<br>"
'response.write "request.QueryString: " & request.QueryString & "<br>"
'response.End()

if request("submitted") = "Update" then
	if strError = "" then
'		for a = 1 to request("totalCount")
'			if request("delete" & a) = "1" then
'				SQL = "DELETE FROM _temp_we_relatedItems WHERE Related_Id='" & request("RelatedID" & a) & "'"
'				response.write "<p>" & SQL & "</p>"
'				'oConn.execute SQL
'				b = b + 1
'			end if
'		next
		dim strDelete : strDelete = getCommaString(request("chkDelete"))
		
		if "" <> strDelete then
			SQL = "delete from we_relateditems where related_id in (" & strDelete & "); "
'			response.write SQL
			oConn.execute(SQL)
			response.write "<h3> (" & strDelete & ") Related Items Deleted!</h3>"			
		else
			response.write "<h3> No Related Items Deleted!</h3>"			
		end if 
	else
		response.write "<p><font color=""#FF0000"" face=""Arial,Helvetica""><ul>" & strError & "</ul></font></p>"
		response.write "<p><form name=""back""><input type=""button"" onclick=""history.back();"" value=""BACK""></form></p>"
	end if
elseif request("submitRelatedItemsSearch") = "Update" then
	if TypeID = "" then strError = strError & "<li>You must select at least one Category.</li>"
	if trim(ItemID) = "" then
'		SQL = "SELECT ItemID FROM we_items WHERE ItemID='" & ItemID & "'"
'		Set RS = Server.CreateObject("ADODB.Recordset")
'		RS.open SQL, oConn, 3, 3
'		if RS.eof then strError = strError & "<li>Please enter a valid ItemID.</li>"
'	else
		strError = strError & "<li>Please enter a valid ItemID.</li>"
	end if
	if strError = "" then
'		modelArray = split(request("modelID"),", ")
'		for a = 0 to uBound(modelArray)
'			SQL = "INSERT INTO _temp_we_relatedItems (itemID,TypeID,ModelID,BrandID) VALUES ("
'			SQL = SQL & "'" & itemID & "',"
'			SQL = SQL & "'" & TypeID & "',"
'			SQL = SQL & "'" & modelArray(a) & "',"
'			SQL = SQL & "'" & BrandID & "')"
'			response.write "<p>" & SQL & "</p>"
'			'oConn.execute SQL
'		next
	
		'========= remove "carriage return"
		ItemID = replace(replace(ItemID, CHR(13), ""), CHR(10), "")
		ModelID = replace(replace(ModelID, CHR(13), ""), CHR(10), "")
		
		dim strItemID : strItemID = getCommaString(ItemID)
		dim strModelID : strModelID = getCommaString(ModelID)

		SQL =	"	insert into we_relatedItems (itemid, typeid, modelid, brandid) " & vbCRLF & _
				"	select	distinct a.itemid, '" & TypeID & "' typeid, b.modelid, '" & brandid & "' brandid" & vbCRLF & _
				"	from	(	" & vbCRLF & _
				"			select 	distinct rString as itemid	" & vbCRLF & _
				"			from 	dbo.tfn_StringToColumn('" & strItemID & "', ',')	" & vbCRLF & _
				"			) a,	" & vbCRLF & _
				"			(	" & vbCRLF & _
				"			select	distinct rString as modelid	" & vbCRLF & _
				"			from	dbo.tfn_StringToColumn('" & strModelID & "', ',')	" & vbCRLF & _
				"			) b;	" & vbCRLF

'		response.write "<pre>" & SQL & "</pre>"

		oConn.execute(SQL)

		response.write "<h3>ADDED!</h3>"
	else
		response.write "<p><font color=""#FF0000"" face=""Arial,Helvetica""><ul>" & strError & "</ul></font></p>"
		response.write "<p><form name=""back""><input type=""button"" onclick=""history.back();"" value=""BACK""></form></p>"
	end if
else
	SQL = "SELECT A.Related_ID,A.ItemID,A.ModelID,A.BrandID,B.ModelName,C.BrandName,D.ItemDesc,E.TypeName" & vbCRLF
	SQL = SQL & " FROM (((we_relatedItems A INNER JOIN we_models B ON A.ModelID=B.ModelID)" & vbCRLF
	SQL = SQL & " INNER JOIN we_brands C ON A.BrandID=C.BrandID)" & vbCRLF
	SQL = SQL & " INNER JOIN we_items D ON A.ItemID=D.ItemID)" & vbCRLF
	SQL = SQL & " INNER JOIN we_types E ON A.TypeID=E.TypeID" & vbCRLF
	SQL = SQL & " WHERE A.ModelID='" & ModelID & "' AND A.BrandID='" & BrandID & "'" & vbCRLF
	SQL = SQL & " ORDER BY A.Related_ID" & vbCRLF
'	response.write "<pre>" & SQL & "</pre>"
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3

	if RS.eof then
	    response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
	else
		%>
		<font face="Arial,Helvetica"><h3><%=RS.recordcount%> items found</h3></font>
		<form name="frmEditItem" action="db_update_RelatedItems.asp" method="post">
			<table border="1" cellpadding="0" cellspacing="0" width="800">
					<tr>
						<td>
							<table border="1" cellpadding="3" cellspacing="0" align="left" width="100%">
								<tr>
									<td align="center" width="50"><b>Item&nbsp;#</b></td>
									<td align="center" width="150"><b>Model&nbsp;Name</b></td>
									<td align="center" width="150"><b>Category</b></td>
									<td align="left" width="400"><b>Item&nbsp;Description</b></td>
									<td align="center" width="50"><b>Delete</b> <input value="" type="checkbox" name="chkMaster" onclick="setAllCheckbox(document.frmEditItem.chkDelete, document.frmEditItem.chkMaster.checked);"/></td>
								</tr>
				<%
				do until RS.eof
				%>								
								<tr>
									<td align="center"><%=RS("ItemID")%></td>
									<td align="center"><%=RS("ModelName")%></td>
									<td align="center"><%=RS("TypeName")%></td>
									<td align="left"><%=RS("ItemDesc")%></td>
									<td align="center">
										<input type="checkbox" name="chkDelete" value="<%=RS("Related_Id")%>" onclick="onSetMasterCheckBox(document.frmEditItem);">
									</td>
								</tr>
				<%
					RS.movenext
				loop
				%>								
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="black"><font size="1">&nbsp;</font></td>
					</tr>
			</table>
			<p>
				<input type="hidden" name="BrandID" value="<%=BrandID%>">
				<input type="hidden" name="ModelID" value="dummy">
				<input type="submit" name="submitted" value="Update">
			</p>
		</form>
		<%
	end if
	RS.close
	Set RS = nothing
end if

'this function removes the blank and "," at the end when it returns the result
function getCommaString(val)
	if "" = trim(val) then getCommaString = "" end if
	
	dim arr : arr = split(val, ",")
	dim str : str = ""

	for i=0 to ubound(arr)
		if "" <> trim(arr(i)) then str = str & trim(arr(i)) & "," end if
	next
	
	if "," = right(str, 1) then str = left(str, len(str) - 1) end if 
	
	getCommaString = str
end function 
%>

</blockquote>
</blockquote>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
