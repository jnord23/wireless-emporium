<%
	thisUser = Request.Cookies("username")
	pageTitle = "Amazon Orders"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim activeProducts : activeProducts = prepStr(request.Form("activeProducts"))
	dim partNumber : partNumber = prepStr(request.Form("partNumber"))
	dim actionName : actionName = prepStr(request.Form("actionName"))
	
	if partNumber <> "" and actionName = "Add" then
		sql = "insert into we_amazonProducts (partNumber) values('" & partNumber & "')"
		session("errorSQL") = sql
		oConn.execute(sql)
	elseif activeProducts <> "" and actionName = "Delete" then
		sql = "delete from we_amazonProducts where partNumber in ('" & replace(replace(activeProducts," ",""),",","','") & "')"
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	sql = "select * from we_amazonProducts order by partNumber"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
%>
<form action="/admin/amazonProducts.asp" method="post" name="amazonForm">
<table border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
    	<td align="center">
        	<div style="font-weight:bold;">Current Amazon Products</div>
            <div>
                <select name="activeProducts" multiple="multiple" size="10">
                    <%
                    do while not rs.EOF
                    %>
                    <option value="<%=rs("partNumber")%>"><%=rs("partNumber")%></option>
                    <%
                        rs.movenext
                    loop
                    %>
                </select>
            </div>
            <div style="font-weight:bold; margin-top:5px;"><input type="button" name="myAction" value="Remove Product" style="font-size:12px;" onclick="removeProducts()" /></div>
        </td>
    </tr>
    <tr>
    	<td style="padding-top:20px;">
        	<div style="float:left; font-weight:bold; font-size:12px;">Add New Product:</div>
            <div style="float:left; margin-left:10px;"><input type="text" name="partNumber" value="" /></div>
            <div style="float:left; margin-left:10px;"><input type="button" name="myAction" value="Add Product" style="font-size:12px;" onclick="addProduct()" /></div>
        </td>
    </tr>
</table>
<input type="hidden" name="actionName" value="" />
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function removeProducts() {
		var prodList = document.amazonForm.activeProducts.value
		var yourstate=window.confirm("Are you sure you want to delete the selected product(s)?")
		if (yourstate) {
			document.amazonForm.actionName.value = "Delete"
			document.amazonForm.submit()
		}
	}
	
	function addProduct() {
		var partNumber = document.amazonForm.partNumber.value
		ajax('/ajax/admin/ajaxAmazonProducts.asp?partNumber=' + partNumber,'dumpZone')
		setTimeout("testPartNumber()",500)
	}
	
	function testPartNumber() {
		var testValue = document.getElementById("dumpZone").innerHTML
		if (testValue == "not found") {
			alert("Not a valid part number\nPlease recheck and submit again")
		}
		else if (testValue == "already") {
			alert("This part number is already in the Amazon inventory")
		}
		else {
			document.amazonForm.actionName.value = "Add"
			document.amazonForm.submit()
		}
	}
</script>