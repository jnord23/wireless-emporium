<%
header = 0
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
dim itemid, modelID
itemid = request.querystring("itemid")

dim OutOfStock
OutOfStock = 0

SQL = "SELECT modelID FROM WE_items WHERE itemID = '" & itemid & "'"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
if not RS.eof then modelID = RS("modelID")
if modelid <> 0 then
	SQL = "SELECT A.*, B.modelName, B.modelImg, C.brandName, D.typeName FROM ((we_Items A INNER JOIN we_models B ON A.modelID=B.modelID)"
	SQL = SQL & " INNER JOIN we_brands C ON A.brandID=C.brandID)"
	SQL = SQL & " INNER JOIN we_types D ON A.typeID=D.typeID"
else
	SQL = "SELECT A.*, 'UNIVERSAL' AS modelName, 'UNIVERSAL.JPG' AS modelImg, 0 AS brandID, 'UNIVERSAL' AS brandName, D.typeName FROM we_items A INNER JOIN we_types D ON A.typeid = D.typeid"
end if
SQL = SQL & " WHERE A.itemID='" & itemid & "'"
'session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
if not RS.eof then
	dim partnumber, brandID, typeID, itemDesc, itempic, price_retail, price_Our, KIT, inv_qty, HideLive
	partnumber = RS("partnumber")
	brandID = RS("brandID")
	typeID = RS("typeID")
	itemDesc = RS("itemDesc")
	itempic = RS("itempic")
	price_retail = RS("price_retail")
	price_Our = RS("price_Our")
	KIT = RS("ItemKit_NEW")
	modelName = RS("modelName")
	modelImg = RS("modelImg")
	brandName = RS("brandName")
	categoryName = RS("typeName")
	inv_qty = RS("inv_qty")
	HideLive = RS("HideLive")
	if RS("inv_qty") <= 0 then
		SQL = "SELECT itemID, inv_qty FROM we_items WHERE PartNumber = '" & RS("PartNumber") & "'"
		set RS2 = CreateObject("ADODB.Recordset")
		RS2.open SQL, oConn, 3, 3
		if RS2.eof then
			OutOfStock = 1
		else
			inv_qty = RS2("inv_qty")
		end if
		RS2.close
		set RS2 = nothing
	end if
	if OutOfStock = 0 then
		dim strItemLongDetail, COMPATIBILITY, download_URL, download_TEXT, PackageContents
		dim BULLET1, BULLET2, BULLET3, BULLET4, BULLET5, BULLET6, BULLET7, BULLET8, BULLET9, BULLET10
		dim modelName, modelImg, brandName, categoryName
		do until RS.eof
			strItemLongDetail = RS("itemLongDetail")
			if not isNull(strItemLongDetail) then
				strItemLongDetail = replace(strItemLongDetail,"Wireless Emporium","WirelessEmporium.com")
				strItemLongDetail = replace(strItemLongDetail," FREE!"," less!")
				strItemLongDetail = replace(strItemLongDetail,chr(34),"''")
				strItemLongDetail = replace(strItemLongDetail,"<a href='/downloads/","<a href='/downloads/")
				strItemLongDetail = replace(strItemLongDetail,vbcrlf," ")
			end if
			if not isNull(RS("BULLET1")) and RS("BULLET1") <> "" then BULLET1 = RS("BULLET1")
			if not isNull(RS("BULLET2")) and RS("BULLET2") <> "" then BULLET2 = RS("BULLET2")
			if not isNull(RS("BULLET3")) and RS("BULLET3") <> "" then BULLET3 = RS("BULLET3")
			if not isNull(RS("BULLET4")) and RS("BULLET4") <> "" then BULLET4 = RS("BULLET4")
			if not isNull(RS("BULLET5")) and RS("BULLET5") <> "" then BULLET5 = RS("BULLET5")
			if not isNull(RS("BULLET6")) and RS("BULLET6") <> "" then BULLET6 = RS("BULLET6")
			if not isNull(RS("BULLET7")) and RS("BULLET7") <> "" then BULLET7 = RS("BULLET7")
			if not isNull(RS("BULLET8")) and RS("BULLET8") <> "" then BULLET8 = RS("BULLET8")
			if not isNull(RS("BULLET9")) and RS("BULLET9") <> "" then BULLET9 = RS("BULLET9")
			if not isNull(RS("BULLET10")) and RS("BULLET10") <> "" then BULLET10 = RS("BULLET10")
			if not isNull(RS("COMPATIBILITY")) and RS("COMPATIBILITY") <> "" then COMPATIBILITY = replace(RS("COMPATIBILITY"),vbcrlf,"<br>")
			download_URL = RS("download_URL")
			download_TEXT = RS("download_TEXT")
			PackageContents = RS("PackageContents")
			RS.movenext
		loop
	end if
end if
%>
<table border="0" width="90%" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="center" valign="top" width="50%">
			<img src="/productpics/big/<%=itempic%>" width="300" border="0" alt="<%=brandName & "&nbsp;" & modelName & "&nbsp;" & singularSEO(categoryName) & "&nbsp;-&nbsp;" & itemDesc%>" id="imgLarge">
			<br>
			<%
			dim fs, iCount, path, src, a, strAltImage
			set fs = CreateObject("Scripting.FileSystemObject")
			a = 0
			for iCount = 0 to 2
				path = server.mappath("\productpics\AltViews\" & replace(itempic,".jpg","-" & iCount & ".jpg"))
				if fs.FileExists(path) then
					a = a + 1
					src = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".jpg")
					strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('" & src & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & iCount & """ src=""" & src & """ width=""40"" height=""40"" style=""border:1px solid gray;""></a>" & vbcrlf
				end if
				path = server.mappath("\productpics\AltViews\" & replace(itempic,".jpg","-" & iCount & ".gif"))
				if fs.FileExists(path) then
					a = a + 1
					src = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".gif")
					strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('" & src & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & iCount & """ src=""" & src & """ width=""40"" height=""40"" style=""border:1px solid gray;""></a>" & vbcrlf
				end if
				path = server.mappath("\productpics\AltViews\" & replace(itempic,".gif","-" & iCount & ".jpg"))
				if fs.FileExists(path) then
					a = a + 1
					src = "/productpics/AltViews/" & replace(itempic,".gif","-" & iCount & ".jpg")
					strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('" & src & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & iCount & """ src=""" & src & """ width=""40"" height=""40"" style=""border:1px solid gray;""></a>" & vbcrlf
				end if
				path = server.mappath("\productpics\AltViews\" & replace(itempic,".gif","-" & iCount & ".gif"))
				if fs.FileExists(path) then
					a = a + 1
					src = "/productpics/AltViews/" & replace(itempic,".gif","-" & iCount & ".gif")
					strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('" & src & "');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb" & iCount & """ src=""" & src & """ width=""40"" height=""40"" style=""border:1px solid gray;""></a>" & vbcrlf
				end if
			next
			if strAltImage <> "" then
				%>
				<script language="javascript">
					function fnPreviewImage(src) {
						var p = document.getElementById('imgLarge');
						p.src = src;
					}
					function fnDefaultImage() {
						var p = document.getElementById('imgLarge');
						p.src = '/productpics/big/<%=itempic%>';
					}
				</script>
				<%
				if lCase(left(partnumber,3)) = "fp2" or lCase(left(partnumber,3)) = "fp3" then
					a = a + 1
					strAltImage = strAltImage & "<a href=""#"" onmouseover=""javascript:fnPreviewImage('/productpics/AltViews/FP-SNAP-ON-ANIM.gif');"" onmouseout=""javascript:fnDefaultImage();""><img id=""imgThumb2"" src=""/productpics/AltViews/FP-SNAP-ON-ANIM.gif"" width=""40"" height=""40"" style=""border:1px solid gray;""></a>" & vbcrlf
				end if
				response.write strAltImage
			else
				response.write "&nbsp;"
			end if
			%>
		</td>
		<td align="left" valign="top" width="50%">
			<%
			response.write "<p><b>Part Number: </b>" & partnumber & "</p>" & vbcrlf
			response.write "<p><b>Description: </b>" & itemDesc & "</p>" & vbcrlf
			response.write "<p><b>WE Price: </b>" & formatCurrency(price_Our) & "</p>" & vbcrlf
			response.write "<p><b>Brand: </b>" & brandName & "</p>" & vbcrlf
			response.write "<p><b>Model: </b>" & modelName & "</p>" & vbcrlf
			response.write "<p><b>Category: </b>" & categoryName & "</p>" & vbcrlf
			response.write "<p><b>Inventory: </b>" & inv_qty & "</p>" & vbcrlf
			response.write "<p><b>HideLive: </b>" & HideLive & "</p>" & vbcrlf
			%>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="left" valign="top">
			<%
			response.write "<p><b>Details: </b>" & strItemLongDetail & "</p>" & vbcrlf
			response.write "<ul>" & vbcrlf
			if BULLET1 <> "" then response.write "<li>" & BULLET1 & "</li>" & vbcrlf
			if BULLET2 <> "" then response.write "<li>" & BULLET2 & "</li>" & vbcrlf
			if BULLET3 <> "" then response.write "<li>" & BULLET3 & "</li>" & vbcrlf
			if BULLET4 <> "" then response.write "<li>" & BULLET4 & "</li>" & vbcrlf
			if BULLET5 <> "" then response.write "<li>" & BULLET5 & "</li>" & vbcrlf
			if BULLET6 <> "" then response.write "<li>" & BULLET6 & "</li>" & vbcrlf
			if BULLET7 <> "" then response.write "<li>" & BULLET7 & "</li>" & vbcrlf
			if BULLET8 <> "" then response.write "<li>" & BULLET8 & "</li>" & vbcrlf
			if BULLET9 <> "" then response.write "<li>" & BULLET9 & "</li>" & vbcrlf
			if BULLET10 <> "" then response.write "<li>" & BULLET10 & "</li>" & vbcrlf
			response.write "</ul>" & vbcrlf
			response.write "<p><b>COMPATIBILITY: </b>" & COMPATIBILITY & "</p>" & vbcrlf
			%>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center" valign="top">
			<p>&nbsp;</p>
			<form><input type="button" value="CLOSE" onClick="javascript:window.close();"></form>
		</td>
	</tr>
</table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
