<%
pageTitle = "Admin - Unprocessed Orders Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" width="750" align="center" cellpadding="0" cellspacing="0"><tr><td>
<p>&nbsp;</p>
<p class="biggerText"><%=pageTitle%></p>
<br>

<%
strError = ""

if request("submitted") <> "" then
	StartDate = request.form("dc1")
	EndDate = request.form("dc2")
	if not isDate(StartDate) then
		strError = "Start Date must be a valid date."
	else
		StartDate = dateValue(StartDate)
	end if
	if not isDate(EndDate) then
		strError = "End Date must be a valid date."
	else
		EndDate = dateValue(EndDate)
	end if
	if strError = "" then
		if StartDate > EndDate then
			strError = "Start Date must be earlier than or equal to End Date."
		end if
	end if
	if strError = "" then
		strDates = StartDate & "<br>to<br>" & EndDate
		SQL = "SELECT orderid, store, accountid, shippingid, extOrderType, extOrderNumber, ordersubtotal, ordershippingfee, orderTax, ordergrandtotal,"
		SQL = SQL & " shiptype, couponid, orderdatetime, approved, emailSent, confirmSent, confirmdatetime, PhoneOrder, cancelled, RMAstatus,"
		SQL = SQL & " refundAmount, refundDate, thub_posted_to_accounting, thub_posted_date, SentToDropShipper, BuySafeCartID, BuySafeAmount"
		SQL = SQL & " FROM we_orders WHERE (orderdatetime >= '" & StartDate & "') AND (orderdatetime < '" & EndDate & "')"
		SQL = SQL & " AND (approved = 1) AND (cancelled = 0 OR cancelled IS NULL) AND (thub_posted_to_accounting IS NULL)"
		SQL = SQL & " ORDER BY orderid"
		response.write "<p>" & SQL & "</p>" & vbcrlf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		%>
		<table border="1">
			<tr>
				<%
				for each whatever in RS.fields
					%>
					<td><b><%=whatever.name%></b></td>
					<%
				next
				%>
			</tr>
			<%
			do until RS.eof
				%>
				<tr>
					<%
					for each whatever in RS.fields
						thisfield = whatever.value
						if isnull(thisfield) then
							thisfield = shownull
						end if
						if trim(thisfield) = "" then
							thisfield = showblank
						end if
						%>
						<td valign="top"><%=thisfield%></td>
						<%
					next
					%>
				</tr>
				<%
				RS.movenext
			loop
			%>
		</table>
		<%
	end if
end if

if request("submitted") = "" or strError <> "" then
	%>
	<p class="normalText"><font color="#FF0000"><b><%=strError%></b></font></p>
	<form action="report_Unprocessed_Orders.asp" name="frmSalesReport" method="post">
		<p class="normalText">
			<input type="text" name="dc1" value="<%=dateAdd("d",-1,date)%>">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.frmSalesReport.dc1,document.frmSalesReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;Start Date
			<br>
			<input type="text" name="dc2" value="<%=dateAdd("d",-1,date)%>">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fEndPop(document.frmSalesReport.dc1,document.frmSalesReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;End Date
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
		</p>
		<p><input type="submit" name="submitted" value="Generate Report"></p>
	</form>
	<%
end if
%>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
