<%
	thisUser = Request.Cookies("username")
	pageTitle = ""
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<style type="text/css">
	.smallProb { display:none; }
	.smallProbShow { display:; }
</style>
<%
	server.ScriptTimeout = 2147483647
	dim itemList, sql, rs, existingQty
	dim goodItemCnt, badItemCnt, issueCnt
	
	sql = "select distinct itemID from we_invRecord"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	do while not rs.EOF
		itemList = itemList & rs("itemID") & "##"
		rs.movenext
	loop
	
	itemList = left(itemList,len(itemList)-2)
	itemListArray = split(itemList,"##")
	badItemCnt = 0
	for i = 0 to ubound(itemListArray)
		sql =	"select top 100 a.id, b.partnumber, a.inv_qty, a.orderQty, a.adjustQty " &_
				"from we_invRecord a " &_
					"left join we_Items b on a.itemID = b.itemID " &_
				"where a.itemID = " & itemListArray(i) & " and entryDate > '1/1/2012' and b.partNumber not in (select partNumber from tempInventoryAdjust) " &_
				"order by a.id desc"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		existingQty = 0
		goodItemCnt = 0
		issueCnt = 0
		do while not rs.EOF
			lineID = prepInt(rs("id"))
			partNumber = prepStr(rs("partNumber"))
			curQty = prepInt(rs("inv_qty"))
			orderAmount = prepInt(rs("orderQty"))
			adjustAmount = prepInt(rs("adjustQty"))
			qtyDiff = existingQty - curQty
			if qtyDiff < 0 then qtyDiff = qtyDiff * -1
			
			if existingQty > 0 then
				if curQty - orderAmount = existingQty then
					existingQty = curQty
				elseif curQty + orderAmount = existingQty then
					existingQty = curQty
				elseif adjustAmount = existingQty then
					existingQty = curQty
				elseif curQty + adjustAmount = existingQty then
					existingQty = curQty
				elseif curQty - adjustAmount = existingQty then
					existingQty = curQty
				elseif qtyDiff < 5 then
					existingQty = curQty
				else
					if qtyDiff > 99 then
						response.Write("<div style='color:#f00; font-weight:bold;'>" & partNumber & "</div>")
						sql = "insert into tempInventoryAdjust (partNumber,qtyDiff) values('" & partNumber & "'," & qtyDiff & ")"
						oConn.execute(sql)
					else
						response.Write("<div id='smallProbDiv' class='smallProb'>we may have a problem here (line " & lineID & "): " & itemListArray(i) & " (" & qtyDiff & ")</div>")
						sql = "insert into tempInventoryAdjust (partNumber,qtyDiff) values('" & partNumber & "'," & qtyDiff & ")"
						oConn.execute(sql)
					end if
					badItemCnt = badItemCnt + 1
					if badItemCnt = 2000 then i = ubound(itemListArray) else exit do
				end if
			else
				existingQty = curQty
			end if
			rs.movenext
		loop
		response.Flush()
	next
%>
<table border="0" cellpadding="3" cellspacing="0" width="600">
    <tr><td>Done! (<%=badItemCnt%>)</td></tr>
    <tr>
    	<td>
        	<!--<input type="button" name="myAction" value="Show All Problem Products" onclick="showAll()" />-->
        </td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function showAll() {
		document.getElementById("smallProbDiv").className = "smallProbShow"
	}
</script>