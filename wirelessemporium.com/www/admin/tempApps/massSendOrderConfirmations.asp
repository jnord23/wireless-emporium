<%
	thisUser = Request.Cookies("username")
	pageTitle = "Resend Order Confirmations!"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	server.ScriptTimeout = 9999
	
	dim resendTheseOrders : resendTheseOrders = request.Form("resendTheseOrders")
	if len(resendTheseOrders) < 1 then resendTheseOrders = prepStr(resendTheseOrders)
	
	if resendTheseOrders <> "" then
		orderArray = split(resendTheseOrders,vbCrLf)
		for i = 0 to ubound(orderArray)
			orderid = prepInt(orderArray(i))
			if orderid > 0 then
				sql = "select store, accountid from we_orders where orderid = " & orderid
				set resendOrderRS = oConn.execute(sql)
				siteID = prepInt(resendOrderRS("store"))
				accountid = prepInt(resendOrderRS("accountid"))
				
				set XMLHTTP = server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
				
				select case siteID
					case 0 : useURL = "http://www.wirelessemporium.com/ajax/resendEmailConfirm?orderid=" & orderid & "&accountid=" & accountid
					case 1 : useURL = "http://www.cellphoneaccents.com/ajax/resendEmailConfirm.asp?orderid=" & orderid & "&accountid=" & accountid
					case 2 : useURL = "http://www.cellularoutfitter.com/ajax/resendEmailConfirm.asp?orderid=" & orderid & "&accountid=" & accountid
					case 3 : useURL = "http://www.phonesale.com/ajax/resendEmailConfirm.asp?orderid=" & orderid & "&accountid=" & accountid
					case 10 : useURL = "http://www.tabletmall.com/ajax/resendEmailConfirm.asp?orderid=" & orderid & "&accountid=" & accountid
				end select
				
				session("errorSQL") = useURL
				XMLHTTP.Open "GET", useURL, False
				XMLHTTP.Send
				
				sql = "update we_orders set note = note + 'Confirmation Sent' where orderID = " & orderid
				oConn.execute(sql)
				
				response.write i & ": " & orderid & " ## "
				response.Flush()
			end if
		next
	end if
%>
<form method="post">
<div><textarea name="resendTheseOrders" rows="10" cols="42"></textarea></div>
<div><input type="submit" name="myAction" value="Resend Confirmations" /></div>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->