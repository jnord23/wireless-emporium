<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
	server.ScriptTimeout = 9999
	siteid = 2
	apiLoginID = CO_CC_API_LOGIN_ID
	apiTransactionKey = CO_CC_TRANSACTION_KEY
	
	sql	=	"select	orderid, chargeAmount, customerProfileID, customerPaymentProfileID" & vbcrlf & _
			"from	declinedPage_orders_temp" & vbcrlf & _
			"where	customerProfileID > 0 and customerPaymentProfileID > 0 and orderid not in (2830204,2830759,2830829,2830840,2830898,2831366,2831488,2832147,2832604,2832887,2832968,2833094,2833103,2833201) and transactionid = ''" & vbcrlf & _
			"order by 1"
	set rs = oConn.execute(sql)
	if rs.eof then
		response.write "no record found"
		response.end
	end if

	do until rs.eof
		orderid = rs("orderid")
		nOrderTotal = rs("chargeAmount")
		cimProfileID = rs("customerProfileID")
		cimPaymentProfileID = rs("customerPaymentProfileID")
		strInvoiceDescription = "CO - only shipping charged problem fix"
		
		'====== makes post purchase reqeust
		xml	=	"<?xml version=""1.0"" encoding=""utf-8""?>" & _
				"<createCustomerProfileTransactionRequest xmlns=""AnetApi/xml/v1/schema/AnetApiSchema.xsd"">" & _
				"	<merchantAuthentication>" & _
				"		<name>" & apiLoginID & "</name>" & _
				"		<transactionKey>" & apiTransactionKey & "</transactionKey>" & _
				"	</merchantAuthentication>" & _
				"	<transaction>" & _
				"		<profileTransAuthCapture>" & _
				"			<amount>" & nOrderTotal & "</amount>" & _
				"			<customerProfileId>" & cimProfileID & "</customerProfileId>" & _
				"			<customerPaymentProfileId>" & cimPaymentProfileID & "</customerPaymentProfileId>" & _			
				"			<order>" & _
				"				<invoiceNumber>" & orderid & "</invoiceNumber>" & _
				"				<description>" & strInvoiceDescription & "</description>" & _
				"				<purchaseOrderNumber>" & orderid & "</purchaseOrderNumber>" & _
				"			</order>" & _
				"			<recurringBilling>false</recurringBilling>" & _
				"		</profileTransAuthCapture>" & _
				"	</transaction>" & _
				"</createCustomerProfileTransactionRequest>"
	
		set objResponse = SendApiRequest(CIM_APIURL, xml)
		if not IsApiResponseSuccess(objResponse) then
			cimErrorCode = objResponse.selectSingleNode("/*/api:messages/api:message/api:code").Text
			cimErrorText = objResponse.selectSingleNode("/*/api:messages/api:message/api:text").Text
			%>
			<h3>This order cannot be processed for the following reason from payment gateway</h3>
			<ul>
            	<li>orderid: <%=orderid%></li>
				<li>response status: <%=cimErrorCode%></li>
				<li>response: <%=cimErrorText%></li>
			</ul>
			<a href="/">Continue Shopping</a>
			<%
			response.end
		else
			set objDirectResponse = objResponse.selectSingleNode("/*/api:directResponse")
			arrDirectResponseFields = Split(objDirectResponse.Text, ",")
			transactionID = Server.HTMLEncode(arrDirectResponseFields(6))
			
			sql = 	"update	declinedPage_orders_temp" & vbcrlf & _
					"set	transactionid = '" & transactionID & "'" & vbcrlf & _
					"where	orderid = " & orderid
			oConn.execute(sql)
		end if	
		rs.movenext
	loop
	response.write "done"
	response.end
%>
