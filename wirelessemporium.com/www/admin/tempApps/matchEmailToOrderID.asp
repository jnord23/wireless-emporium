<%
	thisUser = Request.Cookies("username")
	pageTitle = "Resend Order Confirmations!"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	server.ScriptTimeout = 9999
	
	dim fso,csvFile
	set fso = Server.CreateObject("Scripting.FileSystemObject")
	set csvFile = fso.CreateTextFile(server.MapPath("/admin/tempCSV/matchingOrders.csv"))
	dim fileReady : fileReady = false
	
	dim matchEmails : matchEmails = request.Form("matchEmails")
	if len(matchEmails) < 1 then matchEmails = prepStr(matchEmails)
	
	if matchEmails <> "" then
		response.Write("got something good<br>")
		response.Flush()
		if instr(matchEmails,vbCrLf) > 0 then
			myEmailList = replace(matchEmails,vbCrLf,"','")
		else
			myEmailList = matchEmails
		end if
		do while right(myEmailList,2) = ",'"
			myEmailList = left(myEmailList,len(myEmailList)-2)
		loop
		if right(myEmailList,1) <> "'" then myEmailList = myEmailList & "'"
		if prepStr(myEmailList) <> "" then
			sql =	"select a.orderid, b.EMAIL " &_
					"from we_orders a " &_
						"join v_accounts b on a.accountid = b.accountid and a.store = b.site_id " &_
					"where b.EMAIL in ('" & myEmailList & ") and a.approved = 1 and a.cancelled is null and a.parentOrderID is null and a.orderdatetime > '1/2/2014' " &_
					"order by b.EMAIL, a.orderid"
			set orderRS = oConn.execute(sql)
			
			do while not orderRS.EOF
				csvFile.WriteLine(orderRS("EMAIL") & "," & orderRS("orderID"))
				orderRS.movenext
			loop
			fileReady = true
		end if
	end if
%>
<% if fileReady then %><a href="/admin/tempCSV/matchingOrders.csv">Download File</a><% end if %>
<form method="post">
<div><textarea name="matchEmails" rows="10" cols="42"></textarea></div>
<div><input type="submit" name="myAction" value="Match Email Addresses" /></div>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->