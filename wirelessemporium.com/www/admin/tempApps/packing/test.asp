

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
	<head>
		<title>Packing Slips</title>
		<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,600,700,300,800' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="//www.wirelessemporium.com/includes/css/packing.css" type="text/css" />
		<script src="//code.jquery.com/jquery-latest.min.js"></script>
		<!--<script src="//www.wirelessemporium.com/includes/css/jquery-barcode.min.js"></script>-->
	</head>
	<body>
    <div class="page">
	<table width="100%" cellpadding="0" cellspacing="0"></table></div><div class="page"><table width="100%" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<td width="100%">
                	<table border="0" width="100%" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td align="left" class="invoiceSeq"></td>
                            <td align="right">Page 1/1</td>
                        </tr>
                    </table>
				</td>
			</tr>
			<tr>
				<td class="header">
					CellularOutfitter.com&nbsp;&nbsp;&nbsp;&#9679;&nbsp;&nbsp;&nbsp;1410 N. Batavia St., Orange, CA 92687&nbsp;&nbsp;&nbsp;&#9679;&nbsp;&nbsp;&nbsp;1 (800) 305-1106
				</td>
			</tr>
			<tr>
				<td>
	                
					<div class="logo">
						<img src="//www.cellularoutfitter.com/images/coLogo2.gif" width="100%" />
					</div>
        	        
					<div class="barcode-container">
						<div class="barcode-top">
							<div class="date"><b>Date:</b><br />4/16/2014 </div>
							<div class="ordernum"><b>Order #:</b><br /><span id="ordernum">2801011</span></div>
						</div>
						<div class="barcode">
	                        <img src="//www.wirelessemporium.com/framework/utility/barcode/barcode.asp?code=2801011&height=40&width=2&mode=code39&text=0">
                        </div>
						<div class="barcode-bottom">First Class</div>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="billing">
						<div class="bill-label">Bill To:</div>
						<div class="bill-address">
							Yamilka&nbsp;Cruz<br />
							120 Village Lane&nbsp;<br />
							MERIDEN,&nbsp;CT&nbsp;06451<br />
							203-809-3093<br />
							yamie_nana@yahoo.com
						</div>
					</div>
					<div class="shipping">
						<div class="ship-label">Ship To:</div>
						<div class="ship-address">
							Yamilka&nbsp;Cruz<br />
							120 Village Lane&nbsp;<br />
							MERIDEN,&nbsp;CT&nbsp;06451<br />
							203-809-3093
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="items-header">
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="col-itemnum">Part Number</td>
							<td class="col-product">Product</td>
							<td class="col-qty">Qty</td>
							<td class="col-desc">Description</td>
							<td class="col-price">Unit Price</td>
							<td class="col-total">Total</td>
						</tr>
					</table>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<table class="items" width="100%" cellpadding="0" cellspacing="0">

                        <tr>
                            <td class="col-itemnum">UNI-CAR-HLDR-43</td>
                            <td class="col-product"><img src="//www.cellularoutfitter.com/productpics/thumb/dashboard-sticky-mat120234.jpg" width="56" /></td>
                            <td class="col-qty">1</td>
                            <td class="col-desc">*Promo: Dashboard Sticky Mat (Black)
                            </td>
                            <td class="col-price">$0.00</td>
                            <td class="col-total">$0.00</td>
                        </tr>
                        
                        <tr>
                            <td class="col-itemnum">FPX-SAM-SGS5-MY21</td>
                            <td class="col-product"><img src="//www.cellularoutfitter.com/productpics/thumb/my-carbon-3-in-1-shell-holster-combo-for-samsung-galaxy-s5-black-purple-32100.jpg" width="56" /></td>
                            <td class="col-qty">1</td>
                            <td class="col-desc">My.Carbon 3-in-1 Shell & Holster Combo for Samsung Galaxy S5 (Black/Purple)
                            </td>
                            <td class="col-price">$11.99</td>
                            <td class="col-total">$11.99</td>
                        </tr>
                        
                
                        <tr>
                            <td class="col-itemnum">SCR-SAM-SGS5-TG01</td>
                            <td class="col-product"><img src="//www.cellularoutfitter.com/productpics/thumb/tempered-glass-screen-guard-samsung-galaxy-s575927.jpg" width="56" /></td>
                            <td class="col-qty">1</td>
                            <td class="col-desc">Tempered Glass Screen Guard - Samsung Galaxy S5
                            </td>
                            <td class="col-price">$9.99</td>
                            <td class="col-total">$9.99</td>
                        </tr>
                        
                        <tr>
                            <td colspan="6" style="background-color:yellow;">appendOrderTotal</td>
                        </tr>
        
                    </table>
                </td>
            </tr>
            <tr>
                <td><div class="total-title">Order Total</div></td>
            </tr>
            <tr>
                <td>
                    <div class="totals">
                        <div class="total-left">
                            <div class="paid-by"><b>Paid By:</b> Credit Card</div>
                            
                        </div>
                        <div class="total-right">
                            <div class="total-item">
                                <div class="label">Subtotal:</div>
                                <div class="amount">$21.98</div>
                            </div>
                            
                            <div class="total-item">
                                <div class="label">Sales Tax:</div>
                                <div class="amount">$0.00</div>
                            </div>
                            <div class="total-item">
                                <div class="label">Shipping:</div>
                                <div class="amount">$7.98</div>
                            </div>
                            <div class="total-item">
                                <div class="label">Grand Total:</div>
                                <div class="amount">$29.96</div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>
                    <div class="stamp-zone"><img src="//www.wirelessemporium.com/images/admin/packing/stamp-zone.png" height="168" /></div>
                    <div class="offer"><img src="//www.wirelessemporium.com/images/admin/packing/offer.png" width="588" /></div>
                </td>
            </tr>
        </tfoot>
	</table>
	</div>
    <div class="page">
   	<table width="100%" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<td width="100%">
                	<table border="0" width="100%" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td align="left" class="invoiceSeq"></td>
                            <td align="right">Page 1/2</td>
                        </tr>
                    </table>
				</td>
			</tr>
			<tr>
				<td class="header">
					CellularOutfitter.com&nbsp;&nbsp;&nbsp;&#9679;&nbsp;&nbsp;&nbsp;1410 N. Batavia St., Orange, CA 92687&nbsp;&nbsp;&nbsp;&#9679;&nbsp;&nbsp;&nbsp;1 (800) 305-1106
				</td>
			</tr>
			<tr>
				<td>
	                
					<div class="logo">
						<img src="//www.cellularoutfitter.com/images/coLogo2.gif" width="100%" />
					</div>
        	        
					<div class="barcode-container">
						<div class="barcode-top">
							<div class="date"><b>Date:</b><br />4/16/2014 </div>
							<div class="ordernum"><b>Order #:</b><br /><span id="ordernum">2801900</span></div>
						</div>
						<div class="barcode">
	                        <img src="//www.wirelessemporium.com/framework/utility/barcode/barcode.asp?code=2801900&height=40&width=2&mode=code39&text=0">
                        </div>
						<div class="barcode-bottom">First Class</div>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="billing">
						<div class="bill-label">Bill To:</div>
						<div class="bill-address">
							Bobbie&nbsp;Vaught<br />
							4200 Hwy K 68&nbsp;<br />
							QUENEMO,&nbsp;KS&nbsp;66528<br />
							7854472717<br />
							sayelarose@yahoo.com
						</div>
					</div>
					<div class="shipping">
						<div class="ship-label">Ship To:</div>
						<div class="ship-address">
							Bobbie&nbsp;Vaught<br />
							4200 Hwy K 68&nbsp;<br />
							QUENEMO,&nbsp;KS&nbsp;66528<br />
							7854472717
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="items-header">
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="col-itemnum">Part Number</td>
							<td class="col-product">Product</td>
							<td class="col-qty">Qty</td>
							<td class="col-desc">Description</td>
							<td class="col-price">Unit Price</td>
							<td class="col-total">Total</td>
						</tr>
					</table>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<table class="items" width="100%" cellpadding="0" cellspacing="0">
	
		<tr>
			<td class="col-itemnum">STY-PEN-UNIV-02</td>
			<td class="col-product"><img src="//www.cellularoutfitter.com/productpics/thumb/capacitive-stylus-ballpoint-pen-w-clip43643.jpg" width="56" /></td>
			<td class="col-qty">1</td>
			<td class="col-desc">*Promo: Capacitive Stylus & Ballpoint Pen w/Clip
			</td>
			<td class="col-price">$0.00</td>
			<td class="col-total">$0.00</td>
		</tr>
		
		<tr>
			<td class="col-itemnum">FP2-MOT-RZRM-521</td>
			<td class="col-product"><img src="//www.cellularoutfitter.com/productpics/thumb/motorola-droid-razr-m-xt907-verizon-white-butterfly-with-pink-flowers-on-black-snap-on-cover.jpg" width="56" /></td>
			<td class="col-qty">1</td>
			<td class="col-desc">Motorola Droid Razr M XT907 (Verizon) White Butterfly with Pink Flowers on Black Snap-On Cover
			</td>
			<td class="col-price">$4.99</td>
			<td class="col-total">$4.99</td>
		</tr>
		
		<tr>
			<td class="col-itemnum">SCR-MOT-RZRM-01</td>
			<td class="col-product"><img src="//www.cellularoutfitter.com/productpics/thumb/motorola-droid-razr-m-xt907-verizon-screen-protector-film.jpg" width="56" /></td>
			<td class="col-qty">4</td>
			<td class="col-desc">Motorola Droid Razr M XT907 (Verizon) Screen Protector Film
			</td>
			<td class="col-price">$1.99</td>
			<td class="col-total">$7.96</td>
		</tr>
		
		<tr>
			<td class="col-itemnum">FP2-MOT-RZRM-174</td>
			<td class="col-product"><img src="//www.cellularoutfitter.com/productpics/thumb/motorola-droid-razr-m-xt907-verizon-black-rainbow-zebra-snap-on-snap-on-cover.jpg" width="56" /></td>
			<td class="col-qty">1</td>
			<td class="col-desc">Motorola Droid Razr M XT907 (Verizon) Black Rainbow Zebra snap-on Cover
			</td>
			<td class="col-price">$4.99</td>
			<td class="col-total">$4.99</td>
		</tr>
		
		<tr>
			<td class="col-itemnum">FP2-MOT-RZRM-123</td>
			<td class="col-product"><img src="//www.cellularoutfitter.com/productpics/thumb/motorola-droid-razr-m-xt907-verizon-black-w-blue-flowers-snap-on-cover.jpg" width="56" /></td>
			<td class="col-qty">1</td>
			<td class="col-desc">Motorola Droid Razr M XT907 (Verizon) Black w/Blue Flowers snap-on Cover
			</td>
			<td class="col-price">$4.99</td>
			<td class="col-total">$4.99</td>
		</tr>
		
		<tr>
			<td class="col-itemnum">FP2-MOT-RZRM-601</td>
			<td class="col-product"><img src="//www.cellularoutfitter.com/productpics/thumb/motorola-droid-razr-m-xt907-verizon-stitched-owl-blue-yellow-snap-on-cover.jpg" width="56" /></td>
			<td class="col-qty">1</td>
			<td class="col-desc">Motorola Droid Razr M XT907 (Verizon) Stitched Owl: Blue/Yellow Snap-On Cover
			</td>
			<td class="col-price">$4.99</td>
			<td class="col-total">$4.99</td>
		</tr>
		
		<tr>
			<td class="col-itemnum">FP2-MOT-RZRM-578</td>
			<td class="col-product"><img src="//www.cellularoutfitter.com/productpics/thumb/motorola-droid-razr-m-xt907-verizon-flora-with-butterflies-on-lime-green-snap-on-cover.jpg" width="56" /></td>
			<td class="col-qty">1</td>
			<td class="col-desc">Motorola Droid Razr M XT907 (Verizon) Flora with Butterflies on Lime Green Snap-On Cover
			</td>
			<td class="col-price">$4.99</td>
			<td class="col-total">$4.99</td>
		</tr>
		
		<tr>
			<td colspan="6" style="background-color:yellow;">appendOrderTotal</td>
		</tr>
        
                    </table>
                </td>
            </tr>
            <tr>
                <td><div class="total-title">Order Total</div></td>
            </tr>
            <tr>
                <td>
                    <div class="totals">
                        <div class="total-left">
                            <div class="paid-by"><b>Paid By:</b> Credit Card</div>
                            
                        </div>
                        <div class="total-right">
                            <div class="total-item">
                                <div class="label">Subtotal:</div>
                                <div class="amount">$32.91</div>
                            </div>
                            
                            <div class="total-item">
                                <div class="label">Promo Code:</div>
                                <div class="amount">
                                	<b>SAVE414</b><br />
                                	35% Off $40 Or More
								</div>
                            </div>
                            
                            <div class="total-item">
                                <div class="label">Sales Tax:</div>
                                <div class="amount">$0.00</div>
                            </div>
                            <div class="total-item">
                                <div class="label">Shipping:</div>
                                <div class="amount">$21.91</div>
                            </div>
                            <div class="total-item">
                                <div class="label">Grand Total:</div>
                                <div class="amount">$54.82</div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>
                    <div class="stamp-zone"><img src="//www.wirelessemporium.com/images/admin/packing/stamp-zone.png" height="168" /></div>
                    <div class="offer"><img src="//www.wirelessemporium.com/images/admin/packing/offer.png" width="588" /></div>
                </td>
            </tr>
        </tfoot>
	</table>
	</div>
    
	<script type="text/javascript">
		/*
        $(document).ready(function(){
            var btype = "code39";
            var renderer = "css";
            
			var settings = {
				output:renderer,
				bgColor: "#ffffff",
				color: "#000000",
				barWidth: "2",
				barHeight: "40",
				moduleSize: "5",
				posX: "10",
				posY: "3",
				addQuietZone: "1",
				showHRI: false
            };

			$(".ordernum").each(function() {
				value = $(this).find('#ordernum').html();
				$(".barcode").html("").show().barcode(value, btype, settings);
			});
            //$(".barcode").hide();
        });
		*/
    </script>
	</body>
</html>
