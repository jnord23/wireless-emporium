<%
pageTitle = "Admin - Update WE Database - Manipulate Subtypes"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<font face="Arial,Helvetica">
	<p><a href="/admin/db_search_mansubtypes.asp">Back to Manipulation</a></p>
</font>
<%
updateType = request.Form("updateType")
relatedUpdatetype = request.Form("doSubmitRelatedItems")
txtPartNumber = request.Form("txtPartNumber")
itemid = request.Form("itemid")	'comma delimited string
brandid = request.Form("cbBrand")
subtypeid = request.Form("cbSubTypes")
typeid = request.Form("cbTypes")
strTypeString = request.Form("hidTypeString")
strSubTypeString = request.Form("hidSubTypeString")
modelid = request.Form("hidModelID")	'comma delimited string
strReleatedSet = request.form("cbReleatedSet")
strSubRelatedSet = request.form("cbSubReleatedSet")

'for each i in request.form
'	response.write i & ": [" & request.form(i) & "]<br>"
'next
'response.End

if updateType = "m_part" then	'mass update by partnumber
	if txtPartNumber = "" then
		response.write "<h3>Partnumber is not assigned</h3>"
		response.end
	end if
	sql	=	"SET NOCOUNT ON;" & vbcrlf & _
			"update	we_items" & vbcrlf & _
			"set	subtypeid = '" & subtypeid & "'" & vbcrlf & _
			"where	partnumber like '" & txtPartNumber & "'" & vbcrlf & _
			"select	@@rowcount rCnt" & vbcrlf
	session("errorSQL") = sql
	
	set objRsUpdated = oConn.execute(sql)
	
	if not objRsUpdated.eof then
		response.write "<h3>" & objRsUpdated("rCnt") & " row(s) updated</h3>"
	else
		response.write "<h3>0 row updated</h3>"
	end if
elseif updateType = "m_item" then	'mass update by itemid set
	if itemid = "" then
		response.write "<h3>Itemid is not assigned</h3>"
		response.end
	end if
	
	dim strItemID : strItemID = getCommaString(replace(replace(itemid, CHR(13), ""), CHR(10), ""))
	
	sql	=	"SET NOCOUNT ON;" & vbcrlf & _
			"update	we_items" & vbcrlf & _
			"set	subtypeid = '" & subtypeid & "'" & vbcrlf & _
			"where	itemid in (" & strItemID & ")" & vbcrlf & _
			"select	@@rowcount rCnt" & vbcrlf	
	session("errorSQL") = sql
	
	set objRsUpdated = oConn.execute(sql)
	
	if not objRsUpdated.eof then
		response.write "<h3>" & objRsUpdated("rCnt") & " row(s) updated</h3>"
	else
		response.write "<h3>0 row updated</h3>"
	end if
elseif updateType = "r" then
	if itemid = "" or modelid = "" then
		response.write "<h3>either any model did not get selected or item set is blank</h3>"
		response.end
	end if
	if strTypeString = "" and strSubTypeString = "" then
		response.write "<h3>Please select types or subtypes</h3>"
		response.end
	end if

	strItemID = getCommaString(replace(replace(itemid, CHR(13), ""), CHR(10), ""))
	strModelID = getCommaString(replace(replace(modelid, CHR(13), ""), CHR(10), ""))
	strTypeID = getCommaString(replace(replace(strTypeString, CHR(13), ""), CHR(10), ""))
	strSubtypeID = getCommaString(replace(replace(strSubTypeString, CHR(13), ""), CHR(10), ""))

	if relatedUpdatetype = "Add" then
		dim newItemCnt : newItemCnt = 0
		dim newSubItemCnt : newSubItemCnt = 0
		
		if strTypeID <> "" then	'related item add
			sql	=	"	SET NOCOUNT ON;" & vbcrlf & _
					"	insert into we_relatedItems(itemid, typeid, modelid, brandid)" & vbcrlf & _
					"	select	a.itemid, a.typeid, a.modelid, a.brandid" & vbcrlf & _
					"	from	(" & vbcrlf & _
					"			select	distinct a.itemid, d.typeid, b.modelid, c.brandid" & vbcrlf & _
					"			from	(	" & vbcrlf & _
					"					select 	distinct rString as typeid	" & vbcrlf & _
					"					from 	dbo.tfn_StringToColumn('" & strTypeID & "', ',')	" & vbcrlf & _
					"					) d,	" & vbcrlf & _
					"					(	" & vbcrlf & _
					"					select 	distinct rString as itemid	" & vbcrlf & _
					"					from 	dbo.tfn_StringToColumn('" & strItemID & "', ',')	" & vbcrlf & _
					"					) a,	" & vbcrlf & _
					"					(	" & vbcrlf & _
					"					select	distinct rString as modelid	" & vbcrlf & _
					"					from	dbo.tfn_StringToColumn('" & strModelID & "', ',')	" & vbcrlf & _
					"					) b join we_models c on b.modelid = c.modelid	" & vbcrlf & _
					"			) a left outer join we_relatedItems b" & vbcrlf & _
					"		on	a.itemid = b.itemid and a.typeid = b.typeid and a.modelid = b.modelid and a.brandid = b.brandid" & vbcrlf & _
					"	where	b.related_id is null" & vbcrlf & _
					"	select	@@rowcount rCnt" & vbcrlf
'			response.write "<pre>" & sql & "</pre>"					
			set objRsUpdated = oConn.execute(sql)
			
			if not objRsUpdated.eof then
				newItemCnt = objRsUpdated("rCnt")
				response.write "<h3>" & newItemCnt & " record(s) added for relateditems</h3>"
			else
				newItemCnt = 0
				response.write "<h3>0 record added for relateditems</h3>"
			end if					
		end if
		
		if strSubtypeID <> "" then	'subrelated item add
			sql	=	"	SET NOCOUNT ON;" & vbCRLF & _
					"	insert into we_subRelatedItems(itemid, subtypeid, modelid, brandid)" & vbCRLF & _
					"	select	a.itemid, a.subtypeid, a.modelid, a.brandid" & vbCRLF & _
					"	from	(" & vbCRLF & _
					"			select	distinct a.itemid, d.subtypeid, b.modelid, c.brandid" & vbCRLF & _
					"			from	(" & vbCRLF & _
					"					select 	distinct rString as subtypeid" & vbCRLF & _
					"					from 	dbo.tfn_StringToColumn('" & strSubtypeID & "', ',')	" & vbCRLF & _
					"					) d," & vbCRLF & _
					"					(	" & vbCRLF & _
					"					select 	distinct rString as itemid	" & vbCRLF & _
					"					from 	dbo.tfn_StringToColumn('" & strItemID & "', ',')	" & vbCRLF & _
					"					) a,	" & vbCRLF & _
					"					(	" & vbCRLF & _
					"					select	distinct rString as modelid	" & vbCRLF & _
					"					from	dbo.tfn_StringToColumn('" & strModelID & "', ',')	" & vbCRLF & _
					"					) b join we_models c on b.modelid = c.modelid	" & vbCRLF & _
					"			) a left outer join we_subRelatedItems b" & vbCRLF & _
					"		on	a.itemid = b.itemid and a.subtypeid = b.subtypeid and a.modelid = b.modelid and a.brandid = b.brandid" & vbCRLF & _
					"	where	b.related_id is null" & vbCRLF & _
					"	select	@@rowcount rCnt" & vbCRLF
'			response.write "<pre>" & sql & "</pre>"										
			set objRsUpdated = oConn.execute(sql)
			
			if not objRsUpdated.eof then
				newSubItemCnt = objRsUpdated("rCnt")
				response.write "<h3>" & newSubItemCnt & " record(s) added for sub-relateditems</h3>"
			else
				newSubItemCnt = 0
				response.write "<h3>0 record added for sub-relateditems</h3>"
			end if
		end if
		
		if strReleatedSet <> "" then
			sql	=	"SET NOCOUNT ON;" & vbCRLF & _
					"insert into we_defaultRelatedItems(itemid, setName)" & vbcrlf & _
					"select	distinct rString as itemid, '" & replace(strReleatedSet, "'", "''") & "'" & vbcrlf & _
					"from	dbo.tfn_StringToColumn('" & strItemID & "', ',')" & vbcrlf & _
					"select	@@rowcount rCnt" & vbCRLF
'			response.write "<pre>" & sql & "</pre>"										
			set objRsUpdated = oConn.execute(sql)
			
			if not objRsUpdated.eof then
				response.write "<h3>" & objRsUpdated("rCnt") & " record(s) added for default related itemset(" & strReleatedSet & ")</h3>"
			end if
		end if
		
		if strSubRelatedSet <> "" and strSubtypeID <> "" then
			sql	=	"SET NOCOUNT ON;" & vbcrlf & _
					"insert into we_defaultSubRelatedItems(itemid, subtypeid, setName)" & vbcrlf & _
					"select	a.itemid, a.subtypeid, '" & replace(strSubRelatedSet, "'", "''") & "' setName" & vbcrlf & _
					"from	(" & vbcrlf & _
					"		select	distinct a.subtypeid, b.itemid" & vbcrlf & _
					"		from	(" & vbcrlf & _
					"				select 	distinct rString as subtypeid" & vbcrlf & _
					"				from 	dbo.tfn_StringToColumn('" & strSubtypeID & "', ',')	" & vbcrlf & _
					"				) a," & vbcrlf & _
					"				(	" & vbcrlf & _
					"				select 	distinct rString as itemid	" & vbcrlf & _
					"				from 	dbo.tfn_StringToColumn('" & strItemID & "', ',')	" & vbcrlf & _
					"				) b" & vbcrlf & _
					"		) a left outer join we_defaultSubRelatedItems b" & vbcrlf & _
					"	on	a.itemid = b.itemid and a.subtypeid = b.subtypeid" & vbcrlf & _
					"where	b.id is null" & vbcrlf & _
					"select	@@rowcount rCnt"
'			response.write "<pre>" & sql & "</pre>"										
			set objRsUpdated = oConn.execute(sql)
			
			if not objRsUpdated.eof then
				response.write "<h3>" & objRsUpdated("rCnt") & " record(s) added for default sub-related itemset(" & strReleatedSet & ")</h3>"
			end if
		end if
		
		if prepInt(newItemCnt) > 0 then
			sql = "select top " & newItemCnt & " modelID from we_relatedItems order by Related_Id desc"
			session("errorSQL") = sql
			set newItems = oConn.execute(sql)
			
			do while not newItems.EOF
				sql = "exec sp_createProductListByModelID " & newItems("modelID")
				session("errorSQL") = sql
				oConn.execute(sql)
				response.Write(".")
				response.Flush()
				newItems.movenext
			loop
		elseif prepInt(newSubItemCnt) > 0 then
			sql = "select top " & we_subRelatedItems & " modelID from we_subRelatedItems order by related_id desc"
			session("errorSQL") = sql
			set newItems = oConn.execute(sql)
			
			do while not newItems.EOF
				sql = "exec sp_createProductListByModelID " & newItems("modelID")
				session("errorSQL") = sql
				oConn.execute(sql)
				response.Write(".")
				response.Flush()
				newItems.movenext
			loop
		end if
	elseif relatedUpdatetype = "Delete" then
		if strTypeID <> "" then	'related item delete
			sql	=	"	SET NOCOUNT ON;" & vbCRLF & _
					"	delete	b" & vbCRLF & _
					"	from	(" & vbCRLF & _
					"			select	distinct a.itemid, d.typeid, b.modelid, c.brandid" & vbCRLF & _
					"			from	(	" & vbCRLF & _
					"					select 	distinct rString as typeid	" & vbCRLF & _
					"					from 	dbo.tfn_StringToColumn('" & strTypeID & "', ',')	" & vbCRLF & _
					"					) d,	" & vbCRLF & _
					"					(	" & vbCRLF & _					
					"					select 	distinct rString as itemid	" & vbCRLF & _
					"					from 	dbo.tfn_StringToColumn('" & strItemID & "', ',')	" & vbCRLF & _
					"					) a,	" & vbCRLF & _
					"					(	" & vbCRLF & _
					"					select	distinct rString as modelid	" & vbCRLF & _
					"					from	dbo.tfn_StringToColumn('" & strModelID & "', ',')	" & vbCRLF & _
					"					) b join we_models c on b.modelid = c.modelid	" & vbCRLF & _
					"			) a join we_relatedItems b" & vbCRLF & _
					"		on	a.itemid = b.itemid and a.typeid = b.typeid and a.modelid = b.modelid and a.brandid = b.brandid" & vbCRLF & _
					"	select	@@rowcount rCnt" & vbCRLF	
'			response.write "<pre>" & sql & "</pre>"
			set objRsUpdated = oConn.execute(sql)
			
			if not objRsUpdated.eof then
				response.write "<h3>" & objRsUpdated("rCnt") & " record(s) deleted for relateditems</h3>"
			else
				response.write "<h3>0 record deleted for relateditems</h3>"
			end if					
		end if
		
		if strSubtypeID <> "" then	'subrelated item delete
			sql	=	"	SET NOCOUNT ON;" & vbCRLF & _
					"	delete	b" & vbCRLF & _
					"	from	(" & vbCRLF & _
					"			select	distinct a.itemid, d.subtypeid, b.modelid, c.brandid" & vbCRLF & _
					"			from	(	" & vbCRLF & _
					"					select 	distinct rString as subtypeid" & vbCRLF & _
					"					from 	dbo.tfn_StringToColumn('" & strSubtypeID & "', ',')	" & vbCRLF & _
					"					) d," & vbCRLF & _
					"					(	" & vbCRLF & _					
					"					select 	distinct rString as itemid	" & vbCRLF & _
					"					from 	dbo.tfn_StringToColumn('" & strItemID & "', ',')	" & vbCRLF & _
					"					) a," & vbCRLF & _
					"					(	" & vbCRLF & _
					"					select	distinct rString as modelid	" & vbCRLF & _
					"					from	dbo.tfn_StringToColumn('" & strModelID & "', ',')	" & vbCRLF & _
					"					) b join we_models c on b.modelid = c.modelid	" & vbCRLF & _
					"			) a join we_subRelatedItems b" & vbCRLF & _
					"		on	a.itemid = b.itemid and a.subtypeid = b.subtypeid and a.modelid = b.modelid and a.brandid = b.brandid" & vbCRLF & _
					"	select	@@rowcount rCnt" & vbCRLF
'			response.write "<pre>" & sql & "</pre>"
			set objRsUpdated = oConn.execute(sql)
			
			if not objRsUpdated.eof then
				response.write "<h3>" & objRsUpdated("rCnt") & " record(s) deleted for sub-relateditems</h3>"
			else
				response.write "<h3>0 record deleted for sub-relateditems</h3>"
			end if					
		end if
		
		sql = "select modelID from we_models where modelID in (" & strModelID & ")"
		session("errorSQL") = sql
		set delItems = oConn.execute(sql)
		
		do while not delItems.EOF
			sql = "exec sp_createProductListByModelID " & delItems("modelID")
			session("errorSQL") = sql
			oConn.execute(sql)
			response.Write(".")
			response.Flush()
			delItems.movenext
		loop
	end if
end if
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<%
function getCommaString(val)
	if "" = trim(val) then getCommaString = "" end if
	
	dim arr : arr = split(val, ",")
	dim str : str = ""

	for i=0 to ubound(arr)
		if "" <> trim(arr(i)) then str = str & trim(arr(i)) & "," end if
	next
	
	if "," = right(str, 1) then str = left(str, len(str) - 1) end if 
	
	getCommaString = str
end function 
%>