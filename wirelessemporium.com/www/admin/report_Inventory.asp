<%
	response.buffer = true
	response.expires = -1
	response.ExpiresAbsolute = Now() - 1
	response.CacheControl = "no-cache"

	pageTitle = "Admin - Generate Inventory Report"
	header = 1
	Server.ScriptTimeout = 9000
	dim adminID
	adminID = Request.Cookies("adminID")
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	useTime = replace(replace(time," ",""),":","")
	categoryID = request.Form("category")
	caseType = request.Form("caseType")
	brandID = prepInt(request.Form("brandID"))
	modelID = request.Form("modelID")
	IncludeAll = request.Form("IncludeAll")
	fullReport = request.Form("fullReport")
	strMonth = request.Form("cbMonth")
	fpType = prepStr(request.Form("fpType"))
	dropShip = prepInt(request.Form("dropShip"))
	invLimitor = prepStr(request.Form("invLimitor"))
	invAmt = prepInt(request.Form("invAmt"))
	salesLimitor = prepStr(request.Form("salesLimitor"))
	salesAmt = prepInt(request.Form("salesAmt"))
	partNumber = replace(prepStr(request.Form("partNumber")),"%","")
	filterMore = 0
	
	if isnull(categoryID) or len(categoryID) < 1 then categoryID = 0
	if isnull(caseType) or len(caseType) < 1 then caseType = 0
	if isnull(modelID) or len(modelID) < 1 then modelID = 0
	if isnull(IncludeAll) or len(IncludeAll) < 1 then IncludeAll = 0
	if isnull(fullReport) or len(fullReport) < 1 then fullReport = 0
	if isnull(strMonth) or len(strMonth) < 1 then strMonth = -1
	
	if categoryID > 0 then
		dim SQL, RS
		
		dateStart = dateAdd("M",strMonth,date)
		
		if fullReport = 1 then
			if partNumber <> "" then usePN = "a.partNumber like '%" & partNumber & "%' and "
			sql = "select c.store, sum(b.quantity) as totalSold, a.brandID, a.partNumber, a.itemDesc, a.vendor, a.inv_qty from we_items a left join we_orderDetails b on a.itemID = b.itemID left join we_orders c on b.orderID = c.orderID and c.approved = 1 and c.cancelled is null and b.entryDate > '" & dateStart & "' where " & usePN & "a.typeID = " & categoryID & " and a.master = 1 and hideLive = 0 and a.partNumber not like '%-HYP-%' and a.partNumber not like '%-MLD-%' and a.vendor <> 'cm' and a.vendor <> 'ds' group by c.store, a.brandID, a.partNumber, a.itemDesc, a.vendor, a.inv_qty order by a.brandID, a.itemDesc"
			if brandID > 0 then sql = replace(sql,"where","where brandID = " & brandID & " and")
		else
			if partNumber <> "" then usePN = "c.partNumber like '%" & partNumber & "%' and "
			sql = "select a.store, sum(b.quantity) as totalSold, c.partNumber, d.itemDesc, c.vendor, d.inv_qty from we_orders a with (nolock) left join we_orderDetails b with (nolock) on a.orderID = b.orderID left join we_items c with (nolock) on b.itemID = c.itemID left join we_items d with (nolock) on c.partNumber = d.partNumber and d.master = 1 where " & usePN & "a.orderdatetime > '" & dateStart & "' and a.approved = 1 and a.cancelled is null and c.typeID = " & categoryID & " and c.partNumber not like '%-HYP-%' and c.partNumber not like '%-MLD-%' and c.vendor <> 'cm' and c.vendor <> 'ds'"
			'sql = "select a.itemID, a.itemDesc, a.PartNumber, a.vendor, a.inv_qty, a.subtypeID, c.store, sum(b.quantity) as totalSold from we_Items a left join we_orderDetails b on a.itemID = b.itemID left join we_orders c on b.orderID = c.orderID and c.approved = 1 and c.orderdatetime > '" & dateStart & "' and c.orderdatetime < '" & date + 1 & "' where a.master = 1 and a.typeID = '" & categoryID & "' and a.hideLive = 0 and a.vendor <> 'cm' and a.vendor <> 'ds' and a.vendor <> 'mld'"
			if categoryID = 1 then
				sql = sql & " and c.itemDesc not like '%flashing%'"
			elseif categoryID = 14 then
				sql = sql & " and c.PartNumber like 'UNI-CHM-%'"
			elseif categoryID = 18 then
				sql = sql & " and c.PartNumber like 'SCR-%'"
			elseif categoryID = 7 then
				if caseType = 1 then
					sql = sql & " and (c.PartNumber like 'APC%' or c.PartNumber like 'FP3%' or c.PartNumber like 'FP4%' or c.PartNumber like 'FP5%') and c.BrandID in (17,14,20,4)"
				elseif caseType = 2 then
					sql = sql & " and (c.PartNumber like 'APC%' or c.PartNumber like 'FP3%' or c.PartNumber like 'FP4%' or c.PartNumber like 'FP5%') and c.BrandID not in (17,14,20,4)"
				elseif caseType = 3 then
					sql = sql & " and (c.PartNumber like 'OTT%' or c.PartNumber like 'LC0%' or c.PartNumber like 'LC1%' or c.PartNumber like 'LC2%' or c.PartNumber like 'LC3%' or c.PartNumber like 'LC4%') and c.PartNumber not like '%-MLD-%'"
				elseif caseType = 4 then
					sql = sql & " and (c.PartNumber like 'LC5%')"
				end if
				
				if IncludeAll = 1 then
					filterMore = 1
					sql = sql & " and d.inv_qty > 0"
				end if
			elseif categoryID = 3 then
				if fpType <> "" then
					sql = sql & " and c.partNumber like '%" & fpType & "%'"
				elseif brandID > 0 and modelID > 0 then
					sql = sql & " and c.brandID = '" & brandID & "' and c.modelID = '" & modelID & "'"
				end if
			end if
			sql = sql & " group by a.store, c.partNumber, d.itemDesc, c.vendor, d.inv_qty order by c.partNumber, a.store"
		end if
		
		if invLimitor <> "" then sql = replace(sql,"where","where d.inv_qty " & invLimitor & " " & invAmt & " and ")
		if dropShip = 1 then
			sql = replace(sql," and a.partNumber not like '%-HYP-%' and a.partNumber not like '%-MLD-%' and a.vendor <> 'cm' and a.vendor <> 'ds'","")
			sql = replace(sql," and c.partNumber not like '%-HYP-%' and c.partNumber not like '%-MLD-%' and c.vendor <> 'cm' and c.vendor <> 'ds'","")
		end if
		
		response.Write("<span style='color:#fff;'>" & sql & "</span>")
		session("errorSQL") = SQL
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 0, 1
		
		if not RS.eof then
			' Save as CSV:
			dim fs, file, filename, path
			set fs = CreateObject("Scripting.FileSystemObject")
			if fs.fileExists(Server.MapPath("/admin/tempCSV") & "\dev_report_Inventory_" & adminID & "_*") then
				fs.deleteFile(Server.MapPath("/admin/tempCSV") & "\dev_report_Inventory_" & adminID & "_*")
			end if
			filename = "dev_report_Inventory_" & adminID & "_" & useTime & ".csv"
			path = Server.MapPath("/admin/tempCSV") & "\" & filename
			set file = fs.CreateTextFile(path, true)
			
			dim strToWrite, HoldPartNumber, WEsales, CAsales, COsales, PSsales
			strToWrite = ""
			
			'if request.form("rptFormat") <> "1" then strToWrite = strToWrite & chr(34) & "itemID" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "itemDesc" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "PartNumber" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "vendor" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "inv_qty" & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "WE Sales " & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "CA Sales " & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "CO Sales " & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "PS Sales " & chr(34) & ","
			strToWrite = strToWrite & chr(34) & "TOTAL Sales " & chr(34)
			file.WriteLine strToWrite
			
			do while not rs.EOF
				DoNotInclude = 0
				WEsales = 0
				CAsales = 0
				COsales = 0
				PSsales = 0
				
				strToWrite = ""
				
				curPartNumber = rs("partNumber")
				'curItemID = RS("itemID")
				curItemDesc = RS("itemDesc")
				curVendor = RS("vendor")
				curInvQty = RS("inv_qty")
				
				do while curPartNumber = rs("partNumber")
					if not isnull(rs("store")) then
						if rs("store") = 0 then
							WEsales = WEsales + rs("totalSold")
						elseif rs("store") = 1 then
							CAsales = CAsales + rs("totalSold")
						elseif rs("store") = 2 then
							COsales = COsales + rs("totalSold")
						elseif rs("store") = 3 then
							PSsales = PSsales + rs("totalSold")
						end if
					end if
					rs.movenext
					if rs.EOF then exit do
				loop
				
				TotalSales = WEsales + CAsales + COsales + PSsales
				
				if salesLimitor <> "" then
					if salesLimitor = ">" then
						if TotalSales < (salesAmt + 1) then DoNotInclude = 1
					else
						if TotalSales > (salesAmt - 1) then DoNotInclude = 1
					end if
				end if
				
				'if request.form("rptFormat") <> "1" then strToWrite = strToWrite & chr(34) & curItemID & chr(34) & ","
				strToWrite = strToWrite & chr(34) & curItemDesc & chr(34) & ","
				strToWrite = strToWrite & chr(34) & curPartNumber & chr(34) & ","
				strToWrite = strToWrite & chr(34) & curVendor & chr(34) & ","
				strToWrite = strToWrite & chr(34) & curInvQty & chr(34) & ","
				strToWrite = strToWrite & chr(34) & WEsales & chr(34) & ","
				strToWrite = strToWrite & chr(34) & CAsales & chr(34) & ","
				strToWrite = strToWrite & chr(34) & COsales & chr(34) & ","
				strToWrite = strToWrite & chr(34) & PSsales & chr(34) & ","
				strToWrite = strToWrite & chr(34) & TotalSales & chr(34)
				
				if filterMore = 1 then
					if caseType = 1 or caseType = 2 then
						if TotalSales = 0 and curInvQty > 1 then
							DoNotInclude = 1
						elseif (TotalSales = 1 or TotalSales = 2 or TotalSales = 3) and curInvQty > 10 then
							DoNotInclude = 1
						elseif (TotalSales = 4 or TotalSales = 5) and curInvQty > 15 then
							DoNotInclude = 1
						elseif (TotalSales = 6 or TotalSales = 7 or TotalSales = 8 or TotalSales = 9 or TotalSales = 10) and curInvQty > 30 then
							DoNotInclude = 1
						end if
					elseif caseType = 4 then
						if TotalSales = 0 and curInvQty > 1 then
							DoNotInclude = 1
						elseif (TotalSales = 1 or TotalSales = 2 or TotalSales = 3) and curInvQty > 10 then
							DoNotInclude = 1
						elseif (TotalSales = 4 or TotalSales = 5) and curInvQty > 15 then
							DoNotInclude = 1
						elseif (TotalSales = 6 or TotalSales = 7 or TotalSales = 8 or TotalSales = 9 or TotalSales = 10) and curInvQty > 20 then
							DoNotInclude = 1
						end if
					end if
				end if
				
				if DoNotInclude = 0 then file.WriteLine strToWrite
				
			loop
			
			file.close
			set file = nothing
			set fs = nothing
			response.write "<p class=""boldText"">Your CSV file is <a href=""/admin/tempCSV/" & filename & """>here</a>.</p>"
		else
			response.write "<p class=""boldText"">No records matched.</p><p class=""boldText"">No report generated.</p>"
		end if
		response.write "<p>&nbsp;</p>"
		response.write "<p class=""normalText""><a href=""report_Inventory.asp"">Generate another Inventory Report</a></p>"
		RS.close
		set RS = nothing
	else
%>
<form action="report_Inventory.asp" name="invReportForm" method="post">
<table border="0" cellpadding="3" cellspacing="0" align="center">
	<tr><td align="left"><p class="biggerText"><%=pageTitle%></p></td></tr>
    <tr>
    	<td class="normalText">report for 
			<select name="cbMonth">
            	<option value="-1">1</option>
            	<option value="-2">2</option>
            	<option value="-3">3</option>
            	<option value="-4">4</option>
            	<option value="-5">5</option>
            	<option value="-6">6</option>
            </select>
            &nbsp;Month(s)
		</td>
    </tr>
    <tr>
    	<td class="normalText"><input type="radio" name="category" value="1" onclick="extraOp('')">&nbsp;Batteries</td>
    </tr>
    <tr>
    	<td class="normalText"><input type="radio" name="category" value="2" onclick="extraOp('')">&nbsp;Chargers</td>
    </tr>
    <tr>
    	<td class="normalText"><input type="radio" name="category" value="3" onclick="extraOp('faceplates')">&nbsp;Faceplates</td>
    </tr>
    <tr>
    	<td class="normalText"><input type="radio" name="category" value="7" onclick="extraOp('cases')">&nbsp;Leather Cases</td>
    </tr>
    <tr>
    	<td class="normalText"><input type="radio" name="category" value="6" onclick="extraOp('')">&nbsp;Holsters/Belt Clips</td>
    </tr>
    <tr>
    	<td class="normalText"><input type="radio" name="category" value="5" onclick="extraOp('')">&nbsp;Hands-Free</td>
    </tr>
    <tr>
    	<td class="normalText"><input type="radio" name="category" value="12" onclick="extraOp('')">&nbsp;Antennas/Parts</td>
    </tr>
    <tr>
    	<td class="normalText"><input type="radio" name="category" value="13" onclick="extraOp('')">&nbsp;Data Cable &amp; Memory</td>
    </tr>
    <tr>
    	<td class="normalText"><input type="radio" name="category" value="14" onclick="extraOp('')">&nbsp;Charms</td>
    </tr>
    <tr>
    	<td class="normalText"><input type="radio" name="category" value="16" onclick="extraOp('')">&nbsp;Cell Phones</td>
    </tr>
    <tr>
    	<td class="normalText"><input type="radio" name="category" value="17" onclick="extraOp('')">&nbsp;Full Body Protectors</td>
    </tr>
    <tr>
    	<td class="normalText"><input type="radio" name="category" value="18" onclick="extraOp('')">&nbsp;Screen Protectors</td>
    </tr>
    <tr>
    	<td class="normalText"><input type="radio" name="category" value="8" onclick="extraOp('')">&nbsp;Other Gear</td>
    </tr>
    <tr>
    	<td>
        	<div style="border-top:1px solid #000; border-bottom:1px solid #000; margin:5px 0px 5px 0px;">
            	<input type="checkbox" name="fullReport" value="1" />&nbsp;Show Full Inventory Report<br />
                <span style="font-size:10px;">(No additional criteria - Full category data with date range)</span>
            </div>
            <div style="border-top:1px solid #000; border-bottom:1px solid #000; margin:5px 0px 5px 0px;">
            	<input type="checkbox" name="dropship" value="1" />&nbsp;Include Dropship Items<br />
            </div>
            <div style="border-top:1px solid #000; border-bottom:1px solid #000; margin:5px 0px 5px 0px;">
            	Part Number: <input type="text" name="partNumber" value="" />
            </div>
            <div style="border-top:1px solid #000; border-bottom:1px solid #000; margin:5px 0px 5px 0px; padding:5px 0px 5px 0px;">
            	Inventory 
                <select name="invLimitor">
                	<option value="">- select -</option>
                    <option value=">">&gt;</option>
                    <option value="<">&lt;</option>
                </select>
                <input type="text" name="invAmt" value="" size="3" />
            </div>
            <div style="border-top:1px solid #000; border-bottom:1px solid #000; margin:5px 0px 5px 0px; padding:5px 0px 5px 0px;">
            	Sales 
                <select name="salesLimitor">
                	<option value="">- select -</option>
                    <option value=">">&gt;</option>
                    <option value="<">&lt;</option>
                </select>
                <input type="text" name="salesAmt" value="" size="3" />
            </div>
        </td>
    </tr>
    <tr>
    	<td id="caseSelection" style="display:none;">
        	<table border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td class="boldText">CASE SELECTION CRITERIA</td>
                </tr>
                <tr>
                	<td>
                    	<select id="CaseType" name="CaseType">
							<option value="1">Protector Cases 1 (Apple Blackberry HTC LG)</option>
							<option value="2">Protector Cases 2 (All Others)</option>
							<option value="3">Leather Cases</option>
							<option value="4">Silicone Cases</option>
						</select>
                    </td>
                </tr>
                <tr>
                	<td><input type="radio" name="rptFormat" value="" checked>&nbsp;Regular&nbsp;Report&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="rptFormat" value="1">&nbsp;Consolidated Sales Numbers</td>
                </tr>
                <tr>
                	<td nowrap="nowrap"><input type="checkbox" name="IncludeAll" value="1" checked="checked">&nbsp;In-Stock Items Only</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td id="faceplateSelection" style="display:none;">
        	<table border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td class="boldText">FACEPLATE SELECTION CRITERIA</td>
                </tr>
                <tr>
                	<td>
						<select name="fpType">
							<option value="">-- Select Type --</option>
                            <option value="FP3">FP3</option>
						</select>
                    </td>
                </tr>
                <tr><td style="font-weight:bold;">- OR -</td></tr>
                <tr>
                	<td>
						<select name="brandID" onChange="ajax('/ajax/admin/report_inv_models.asp?brandID=' + this.value,'fp_models')">
							<option value="">-- Select Brand --</option>
							<%
                            SQL = "SELECT * FROM WE_Brands A ORDER BY brandName"
                            set RS = Server.CreateObject("ADODB.Recordset")
                            RS.open SQL, oConn, 3, 3
                            do until RS.eof
                            %>
                            <option value="<%=RS("brandID")%>"><%=RS("brandName")%></option>
                            <%
                                RS.movenext
                            loop
                            %>
						</select>
                    </td>
                </tr>
                <tr>
                	<td id="fp_models"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td class="normalText" id="subCell"><input type="button" value="Pull Inventory Report" onclick="subForm()"></td>
    </tr>
</table>
</form>
<script language="javascript">
	function subForm() {
		document.getElementById('subCell').innerHTML='Processing Request...'
		document.invReportForm.submit()
	}
	
	function extraOp(val) {
		document.getElementById('faceplateSelection').style.display='none'
		document.getElementById('caseSelection').style.display='none'
		if (val == "faceplates") {
			document.getElementById('faceplateSelection').style.display=''
		}
		if (val == "cases") {
			document.getElementById('caseSelection').style.display=''
		}
	}
</script>
<% end if %>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
