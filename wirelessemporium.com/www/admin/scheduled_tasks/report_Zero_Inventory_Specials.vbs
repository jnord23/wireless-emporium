set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

SQL = "SELECT itemID,itemDesc,PartNumber,inv_qty FROM we_Items"
SQL = SQL & " WHERE (Seasonal = 1) AND (hideLive = 0) AND (inv_qty < 1)"
SQL = SQL & " ORDER BY PartNumber,itemID"
Set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

bodyText = ""

if not RS.eof then
	bodyText = bodyText & "<h3>Seasonal Items with Zero Inventory:</h3>" & vbcrlf
	do until RS.eof
		if RS("inv_qty") = 0 then
			bodyText = bodyText & "<p>itemID: " & RS("itemID") & "<br>PartNumber: " & RS("PartNumber") & "<br>itemDesc: " & RS("itemDesc") & "</p>" & vbcrlf
		elseif RS("inv_qty") < 0 then
			SQL = "SELECT inv_qty FROM we_Items WHERE PartNumber='" & RS("PartNumber") & "' AND inv_qty > -1"
			Set RS2 = oConn.execute(SQL)
			if not RS2.eof then
				if RS2("inv_qty") = 0 then bodyText = bodyText & "<p>itemID: " & RS("itemID") & "<br>PartNumber: " & RS("PartNumber") & "<br>itemDesc: " & RS("itemDesc") & "</p>" & vbcrlf
			end if
		end if
		RS.movenext
	loop
end if

SQL = "SELECT itemID,itemDesc,PartNumber,inv_qty FROM we_Items"
SQL = SQL & " WHERE itemID IN (SELECT itemID FROM we_newarrival) AND (hideLive = 0) AND (inv_qty < 1)"
SQL = SQL & " ORDER BY PartNumber,itemID"
Set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if not RS.eof then
	bodyText = bodyText & "<h3>New Arrivals with Zero Inventory:</h3>" & vbcrlf
	do until RS.eof
		if RS("inv_qty") = 0 then
			bodyText = bodyText & "<p>itemID: " & RS("itemID") & "<br>PartNumber: " & RS("PartNumber") & "<br>itemDesc: " & RS("itemDesc") & "</p>" & vbcrlf
		elseif RS("inv_qty") < 0 then
			SQL = "SELECT inv_qty FROM we_Items WHERE PartNumber='" & RS("PartNumber") & "' AND inv_qty > -1"
			Set RS2 = oConn.execute(SQL)
			if not RS2.eof then
				if RS2("inv_qty") = 0 then bodyText = bodyText & "<p>itemID: " & RS("itemID") & "<br>PartNumber: " & RS("PartNumber") & "<br>itemDesc: " & RS("itemDesc") & "</p>" & vbcrlf
			end if
		end if
		RS.movenext
	loop
end if

RS.close
Set RS = nothing
Set RS2 = nothing

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub


if bodyText <> "" then
	Set objErrMail = CreateObject("CDO.Message")
	With objErrMail
		.From = "service@wirelessemporium.com"
		.To = "leanne@wirelessemporium.com"
		'.cc = "michael@wirelessemporium.com"
		.Subject = "ALERT: Seasonal/New Arrivals with Zero Inventory!"
		.HTMLBody = bodyText
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Update
		.Send
	End With
	Set objErrMail = nothing
end if

