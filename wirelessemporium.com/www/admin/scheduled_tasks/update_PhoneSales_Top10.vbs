set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

for b = 0 to 3
	SQL = "DELETE FROM temp_PhoneSales_Top10 WHERE store = " & b
	oConn.execute SQL
	
	select case b
		case 1 : store = "_CA"
		case 2 : store = "_CO"
		case 3 : store = "_PS"
		case else : store = ""
	end select
	
	SQL = "SELECT B.itemID, B.quantity, C.itemDesc" & store
	SQL = SQL & " FROM (we_Orders A INNER JOIN we_orderdetails B ON A.orderID = B.orderID)"
	SQL = SQL & " INNER JOIN we_Items C ON B.itemID = C.itemID"
	SQL = SQL & " WHERE A.orderdatetime >= '" & dateAdd("m",-1,date) & "' AND A.orderdatetime <= '" & date & "'"
	SQL = SQL & " AND C.typeID = 16"
	SQL = SQL & " AND (A.cancelled IS NULL OR A.cancelled = 0)"
	SQL = SQL & " AND A.approved = 1"
	SQL = SQL & " AND A.store = " & b
	SQL = SQL & " ORDER BY B.itemID"
	set RS = CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	
	if not RS.eof then
		dim holdItemID, quantity
		holdItemID = RS("itemID")
		quantity = RS("quantity")
		RS.movenext
		do until RS.eof
			if RS("itemID") <> holdItemID then
				SQL = "INSERT INTO temp_PhoneSales_Top10 (store,itemID,quantity) VALUES ("
				SQL = SQL & b & ","
				SQL = SQL & "'" & holdItemID & "',"
				SQL = SQL & "'" & quantity & "')"
				oConn.execute SQL
				holdItemID = RS("itemID")
				quantity = RS("quantity")
			else
				quantity = quantity + RS("quantity")
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
next

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
