<%
set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

set sftp = CreateObject("Chilkat.SFtp")
success = sftp.UnlockComponent("Anything for 30-day trial")

sftp.ConnectTimeoutMs = 5000
sftp.IdleTimeoutMs = 15000

username = "srwiem"

'old host data
'ftp.Hostname = "sftp.shoprunner.com"
'ftp.Password = "z38H4T73"

'staging
'hostname = "sftp.stg.wiem.shoprunner.net"
'password = "password"

'live
Hostname = "sftp.wiem.shoprunner.net"
Password = "F8WQ2ga5l5#o"

port = 22
success = sftp.Connect(hostname,port)
success = sftp.AuthenticatePw(username,password)
success = sftp.InitializeSftp()

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")

strSDate = dateadd("d", -1, date)
strSDate = right("00" & month(strSDate), 2) & "/" & right("00" & day(strSDate), 2) & "/" & year(strSDate)
strEDate = date
strEDate = right("00" & month(strEDate), 2) & "/" & right("00" & day(strEDate), 2) & "/" & year(strEDate)

strYear = year(now)
strMonth = Right("0" & month(now), 2)
strDay = Right("0" & day(now), 2)
strHour = hour(now)
strMinute = minute(now)
strSec = second(now)

filename = "WIEM_" & strYear & strMonth & strDay & strHour & strMinute & "_daily-order-count-feed.txt"
path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\shoprunner\" & filename

set file = fs.CreateTextFile(path)

sql	=	"select	count(*) cnt, isnull(sum(cast(ordergrandtotal as money)), 0) ordergrandtotal" & vbcrlf & _
		"from	we_orders" & vbcrlf & _
		"where	orderdatetime >= '" & strSDate & "'" & vbcrlf & _
		"	and	orderdatetime < '" & strEDate & "'" & vbcrlf & _
		"	and	approved = 1" & vbcrlf & _
		"	and	(cancelled = 0 or cancelled is null)" & vbcrlf & _
		"	and	parentOrderID is null" & vbcrlf & _
		"	and	store = 0" & vbcrlf & _
		"	and	(extordertype is null or extordertype in (1,2,3))	-- creditcard, paypal, google checkout and WUPay only"

set rs = CreateObject("ADODB.Recordset")
rs.open SQL, oConn, 0, 1
if not rs.eof then
	'core columns
	headers = "Date" & vbtab & "PartnerCode" & vbtab & "TotalNoOfOrders" & vbtab & "TotalOrderAmount"
	file.writeline headers
	
	do until rs.eof
		strline = 			strSDate & vbTab
		strline = strline & "WIEM" & vbTab
		strline = strline & rs("cnt") & vbTab
		strline = strline & rs("ordergrandtotal") 
		file.writeline strline
		rs.movenext
	loop
end if
rs.close
set rs = nothing
file.close()

handle = sftp.OpenFile("/Inbox/" & filename,"writeOnly","createTruncate")
If (handle = vbNullString ) Then
    response.Write("<pre>" & sftp.LastErrorText & "</pre>")
    response.End()
End If

path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\shoprunner\" & filename
success = sftp.UploadFile(handle,path)

success = sftp.CloseHandle(handle)

response.Write("all done!")
response.End()

remoteFileName = filename
success = ftp.PutFile(path, ftp.GetCurrentRemoteDir() & "/" & remoteFileName)
%>