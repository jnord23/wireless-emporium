set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

dim fs, file, filename, path, folderName
set fs = CreateObject("Scripting.FileSystemObject")
dim oXMLHTTP : set oXMLHTTP = CreateObject("Msxml2.ServerXMLHTTP.3.0")

filename = "wegooglebase"
fileext = "txt"
nRows = 0
folderName = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\"
imgAbsolutePath = "c:\inetpub\wwwroot\productpics\"
path = folderName & filename & "." & fileext

nFileIdx = 0

set folder = fs.GetFolder(folderName)
set files = folder.Files

for each folderIdx in files
	if instr(folderIdx.Name, filename) > 0 then
		if fs.FileExists(folderName & folderIdx.Name) then
			fs.DeleteFile(folderName & folderIdx.Name)
		end if
	end if
next
set file = fs.CreateTextFile(path)

dim SQL, RS
dim ModelName, BrandName, strItemLongDetail, strFeatures, strCompatibility, Condition, strline, itemDesc
sql	=	"exec feed_googlebaseWE"
	
set rs = oConn.execute(sql)

headings = "id" & vbTab & "title" & vbTab & "description" & vbTab & "google_product_category" & vbTab & "product_type" & vbTab & "link" & vbTab & "image_link" & vbTab & "condition" & vbTab & "availability" & vbTab & "price" & vbTab & "brand" & vbTab & "gtin" & vbTab & "mpn" & vbTab & "tax" & vbTab & "shipping" & vbTab & "shipping_weight" & vbTab & "adwords_labels" & vbTab & "adwords_grouping" & vbTab & "identifier_exists" & vbTab & "additional_image_link" & vbTab & "color" & vbTab & "Custom Label 0" & vbTab & "Custom Label 1" & vbTab & "Custom Label 2" & vbTab & "Custom Label 3" & vbTab & "Custom Label 4" & vbTab & "Promotion ID"

if not RS.eof then
	file.WriteLine headings
	do until RS.eof
		itemid = clng(rs("itemid"))
		partnumber = rs("partnumber")
		itemDesc = rs("itemdesc")
		itemdesc_google = rs("itemdesc_google")
		itempic = rs("itempic")
		price_our = rs("price_our")
		itemweight = rs("itemweight")
		itemlongdetail = replace(replace(replace(rs("itemlongdetail"), chr(10), " "), chr(13), " "), chr(9), " ")
		compatibility = rs("compatibility")
		strTypeName = rs("typename")
		strSubTypeName = rs("subtypename")
		priceRange = rs("priceRange")
		salesVelocity = rs("salesVelocity")
		google_promotion_id = rs("google_promotion_id")
		
		if fs.fileExists(imgAbsolutePath & "thumb\" & itempic) or strTypeName = "Custom Case" then
			if not isnull(itemlongdetail) then
				strItemLongDetail = stripCallToAction(itemlongdetail)
				strItemLongDetail = replace(replace(strItemLongDetail,vbCrLf," "),vbTab," ")
				strItemLongDetail = replace(strItemLongDetail,"<a href='/downloads/","<a href='http://www.wirelessemporium.com/downloads/")
			else
				strItemLongDetail = ""
			end if
			
			if not isNull(compatibility) then
				strCompatibility = replace(compatibility,vbCrLf," ")
			else
				strCompatibility = ""
			end if
			
			strFeatures = ""
			if not isNull(RS("BULLET1")) and RS("BULLET1") <> "" then strFeatures = strFeatures & " - " & RS("BULLET1")
			if not isNull(RS("BULLET2")) and RS("BULLET2") <> "" then strFeatures = strFeatures & " - " & RS("BULLET2")
			if not isNull(RS("BULLET3")) and RS("BULLET3") <> "" then strFeatures = strFeatures & " - " & RS("BULLET3")
			if not isNull(RS("BULLET4")) and RS("BULLET4") <> "" then strFeatures = strFeatures & " - " & RS("BULLET4")
			if not isNull(RS("BULLET5")) and RS("BULLET5") <> "" then strFeatures = strFeatures & " - " & RS("BULLET5")
			if not isNull(RS("BULLET6")) and RS("BULLET6") <> "" then strFeatures = strFeatures & " - " & RS("BULLET6")
			if not isNull(RS("BULLET7")) and RS("BULLET7") <> "" then strFeatures = strFeatures & " - " & RS("BULLET7")
			if not isNull(RS("BULLET8")) and RS("BULLET8") <> "" then strFeatures = strFeatures & " - " & RS("BULLET8")
			if not isNull(RS("BULLET9")) and RS("BULLET9") <> "" then strFeatures = strFeatures & " - " & RS("BULLET9")
			if not isNull(RS("BULLET10")) and RS("BULLET10") <> "" then strFeatures = strFeatures & " - " & RS("BULLET10")
			strFeatures = replace(strFeatures,vbTab," ")
			
			
			if inStr(Lcase(strItemLongDetail),"refurbished") > 0 or inStr(Lcase(itemDesc),"refurbished") > 0 or inStr(Lcase(strFeatures),"refurbished") > 0 then
				Condition = "refurbished"
			elseif inStr(Lcase(itemDesc),"used") > 0 or inStr(Lcase(itemDesc),"pre-owned") > 0 then
				Condition = "used"
			else
				Condition = "new"
			end if
			
			if not isNull(itemdesc_google) then
				itemDesc = itemdesc_google
			elseif itemid = 30626 then
				itemDesc = "Stylish EVA Pouch for Samsung Samsung Solstice SGH-A887, Samsung SGH-F480, Samsung SGH-U600, Samsung SGH-A707 SYNC, Sony Ericsson W980, Sony Ericsson W518a, Samsung Highlight SGH-T749, LG Venus VX8800, LG VX 8600 / AX 8600, LG enV2 VX9100, Blackberry Pearl Flip 8220/8230, HTC Shadow, LG Banter AX265/UX265, LG CF360, LG env3 VX9200, HTC 3125/Smartflip/8500/Star Trek, LG KF510, LG KG300, LG Muziq LX-570/UX/AX-565, LG Neon GT365, LG Versa VX9600, Motorola Evoke QA4, Samsung Gravity T459, Samsung Memoir T929, Motorola RAZR MAXX Ve, Motorola Rival A455, Motorola ROKR E2, Motorola V3c Razr, Motorola W220, Motorola W377, Nokia 5610, Pantech Breeze C520, Samsung A900, Samsung SGH-D807, Samsung Knack U310, Samsung SCH-U700 Gleam, Samsung SGH-A517, Samsung SGH-G600, Samsung SGH-T439, Motorola Entice W766, Samsung Intensity SCH-U450, Samsung SPH-M330, Motorola i856, Sony Ericsson F305, HTC Pure, Blackberry Pearl 8110/8120/8130, HTC Touch Diamond (CDMA), HTC Touch Diamond (GSM), HTC Touch Dual, LG KB770, LG Xenon GR500, Motorola Krave ZN4, Motorola Renegade V950, Motorola RIZR Z8, Palm Treo 680, Samsung Behold T919, Samsung Eternity SGH-A867, Samsung Glyde SCH-U940, Samsung Propel Pro SGH-i627, LG Chocolate VX8550, LG KP220, LG KS500, LG Shine CU720, Motorola Adventure V750, Samsung S5230 Star, Nokia 2680, Nokia 6670, Samsung SGH-T659, LG Bliss UX700, Samsung SGH-T629, Samsung SGH-T639, Sony Ericsson W380a, Sony Ericsson W395, Samsung Comeback T559, Motorola V3 Razr, Motorola V3m Razr, Motorola V3xx Razr, Motorola W755, Nokia 2610, Nokia 6555, Nokia 6560, Samsung A777, Samsung FlipShot SCH-U900, Samsung MyShot II SCH-R460, Samsung S3500, Samsung SCH-U550, Samsung SGH-A437/A436, Samsung SGH-A717, Samsung SGH-E215, Samsung SGH-J700, Nokia 6350, Motorola ZN200, LG VX 8700, Motorola EM330, HTC Cingular 8125, HTC Fuze, HTC Shadow 2, HTC XV6850 Touch Pro CDMA (Verizon), LG Dare VX9700, LG KP500/KP501 Cookie, Motorola Rapture VU30, Motorola RAZR VE20, Motorola RAZR2 V8, Motorola RAZR2 V9, Motorola RAZR2 V9m, Motorola V3a/V3i/V3r/V3t/V3e Razr, Motorola W490/W510/W5, Nokia 3555, Pantech PN-820, Samsung Helio Drift SPH-A503, Samsung Helio Fin SPH-A513, Samsung Helio Heat SPH-A303, Samsung Propel A767, Samsung Renown SCH-U810, Samsung SCH-U740, Samsung SGH-T239, LG LX290, Sony Ericsson C905a, LG KS10, LG KU990/GC900 Viewty, LG Rumor LX260, LG Rumor2 LX265, Motorola RAZR Maxx V6, Nextel Stature i9, Motorola Stature i9, Nokia XpressMusic 5800, Palm Centro, Palm Treo 750, Palm Treo 755p, Samsung Helio Mysto SPH-A523, Samsung Propel A767, LG Chocolate VX8500, LG Invision CB630, LG KF350, LG KF750/TU750 Secret, LG KP260, LG KU380, HTC Touch Diamond2, Samsung Smooth SCH-U350, Samsung Trance SCH-U490, Sanyo Pro-200, Sanyo Pro-700, Sanyo SCP-6600/Katana, Sanyo SCP-8500/Katana DLX, Sony Ericsson W350, HTC Fusion/5800/S720, Motorola VE440, Samsung SPH-M240, Nokia Mural 6750, Motorola ROKR U9, Motorola VE465, Nokia 5310, Nokia 6263, Samsung Highnote SPH-M630, LG Trax CU575, LG LX370/UX370, Other Casio Exilim C721, HTC Herman/Touch Pro CDMA (Alltel, Sprint), LG Decoy VX8610, LG Incite CT810, LG KF900 Prada II, LG Vu/CU920/CU915/TU915, Motorola A810, Motorola C261, Motorola Clutch i465, Palm Treo 700wx/700w/700p, Samsung Alias 2 SCH-U750, Sony Ericsson W760, LG CG180, LG Chocolate 3 VX8560, LG KC780, Nokia Surge 6790, Samsung SGH-T809/D820, Samsung SLM SGH-A747, Samsung SPH-M320, LG KM501, Samsung Hue II SCH-R600, Samsung Katalyst SGH-T739, Samsung S3600, Samsung SCH-R211, Samsung SCH-U540, Samsung SGH-D900i, Samsung SGH-T429, Motorola SLVR L9, Sony Ericsson G502, Sony Ericsson K510i, LG Helix AX310/UX310, Sanyo SCP-6650/Katana II, Sony Ericsson S302, Sony Ericsson TM506, Sony Ericsson W580i, Sony Ericsson W595, Sony Ericsson W760, Nokia E75, Blackberry Pearl 8100"
			elseif itemid = 13309 then
				itemDesc = "Retractable-Cord Car Charger for LG VI-125, LG VX 4650/VX 4700/ AX4750, LG PM-325, LG C1500, LG VX 3450, LG VX 8000, LG VX 8100, LG AX5000/UX5000, LG LX-350, LG AX-140/145 Aloha/200c, LG VX 5300/AX245, LG VX 3400, LG AX-390/UX-390, LG FUSIC LX550, LG AX4270, LG VX 9800, LG VX 7000, LG VX 6100, LG VX 3200, LG PM-225, LG VX 3300/3280, LG VX 5200, LG Migo VX1000, LG VX 8300"
			elseif itemid = 8879 then
				itemDesc = "HEAVY-DUTY Car Charger for LG VI-125, LG VX 4650/VX 4700/ AX4750, LG PM-325, LG C1500, LG VX 3450, LG VX 8000, LG VX 8100, LG AX5000/UX5000, LG LX-350, LG AX-140/145 Aloha/200c, LG VX 5300/AX245, LG VX 3400, LG AX-390/UX-390, LG FUSIC LX550, LG AX4270, LG VX 9800, LG VX 7000, LG VX 6100, LG VX 3200, LG PM-225, LG VX 3300/3280, LG VX 5200, LG Migo VX1000, LG VX 8300"
			elseif itemid = 10564 then
				itemDesc = "Car Charger for LG VI-125, LG VX 4650/VX 4700/ AX4750, LG PM-325, LG C1500, LG VX 3450, LG VX 8000, LG VX 8100, LG AX5000/UX5000, LG LX-350, LG AX-140/145 Aloha/200c, LG VX 5300/AX245, LG VX 3400, LG AX-390/UX-390, LG FUSIC LX550, LG AX4270, LG VX 9800, LG VX 7000, LG VX 6100, LG VX 3200, LG PM-225, LG VX 3300/3280, LG VX 5200, LG Migo VX1000, LG VX 8300"
			end if
			
			BrandName = RS("brandName")
			ModelName = RS("modelName")
			typeid = rs("typeid")
			subtypeid = rs("subtypeid")
			smartphone = rs("smartphone")
			
			gpc = rs("google_product_category")
			pt = rs("google_product_type")
			upc = rs("upccode")
			
			if len(itemDesc) > 69 then itemDesc = left(itemDesc, 66) & "..."
			
			strline = itemid & vbTab
			strline = strline & itemDesc & vbTab
			strline = strline & strItemLongDetail & vbtab
			strline = strline & gpc & vbTab	'google_product_category
			strline = strline & pt & vbTab	'product_type
			
			if typeid = 24 then
				strline = strline & "http://www.wirelessemporium.com/custom-phone-cases?model=" & formatSEO(modelName) & "&DZID=PPC_Google_PLA_{adid}_" & RS("itemid") & vbTab
			else
				strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(itemDesc) & "?DZID=PPC_Google_PLA_{adid}_" & RS("itemid") & vbTab
			end if
			
			if typeid = 24 then
				strline = strline & itempic & vbTab
			else
				if fs.FileExists(imgAbsolutePath & "big\zoom\" & itempic) then
					strline = strline & "http://www.wirelessemporium.com/productpics/big/zoom/" & itempic & vbTab
				else
					strline = strline & "http://www.wirelessemporium.com/productpics/big/" & itempic & vbTab
				end if
			end if
			
			strline = strline & Condition & vbTab
			strline = strline & "in stock" & vbTab
			strline = strline & RS("price_our") & vbTab
			strline = strline & BrandName & vbTab
			strline = strline & upc & vbTab
			strline = strline & partnumber & vbTab
			strline = strline & "US:CA:8.00:y" & vbTab
			strline = strline & "US:::0.00" & vbTab
			strline = strline & itemweight & vbTab
			strline = strline & strTypeName & vbTab
			strline = strline & ModelName & vbTab
			if typeid = 24 then
				strline = strline & "FALSE" & vbTab
			else
				strline = strline & "TRUE" & vbTab
			end if

			strAltViews = ""
			if typeid <> 24 then
				for iCount = 0 to 7
					altPic = replace(itempic,".jpg","-" & iCount & ".jpg")
					if fs.FileExists(imgAbsolutePath & "altviews\zoom\" & altPic) then
						if strAltViews = "" then
							strAltViews = "http://www.wirelessemporium.com/productpics/altviews/zoom/" & altPic
						else
							strAltViews = strAltViews & ",http://www.wirelessemporium.com/productpics/altviews/zoom/" & altPic						
						end if
					elseif fs.FileExists(imgAbsolutePath & "altviews\" & altPic) then
						if strAltViews = "" then
							strAltViews = "http://www.wirelessemporium.com/productpics/altviews/" & altPic
						else
							strAltViews = strAltViews & ",http://www.wirelessemporium.com/productpics/altviews/" & altPic						
						end if
					end if				
				next
			end if
			strline = strline & strAltViews & vbTab
			strline = strline & rs("color") & vbTab
			strline = strline & ModelName & vbTab
			strline = strline & strTypeName & vbTab
			strline = strline & strSubTypeName & vbTab
			strline = strline & priceRange & vbTab
			strline = strline & salesVelocity & vbTab
			strline = strline & google_promotion_id

			call fWriteFile(strline)
		end if
		RS.movenext
	loop
end if
file.close()

path = folderName & filename & "_coupons." & fileext
set file = fs.CreateTextFile(path)

set rs = rs.nextrecordset

headings = "promotion_id" & vbTab & "product_applicability" & vbTab & "long_title" & vbTab & "short_title" & vbTab & "promotion_effective_dates" & vbTab & "redemption_channel" & vbTab & "merchant_logo" & vbTab & "offer_type" & vbTab & "generic_redemption_code"

if not RS.eof then
	file.WriteLine headings
	do until RS.eof
		promoDates = rs("promotion_effective_date") & "T00:00:00-08:00/" & rs("expiration_date") & "T00:00:00-08:00"
		
		strline = rs("promotion_id") & vbTab
		strline = strline & rs("product_applicability") & vbTab
		strline = strline & rs("long_title") & vbTab
		strline = strline & rs("short_title") & vbTab
		strline = strline & promoDates & vbTab
		strline = strline & rs("redemption_channel") & vbTab
		strline = strline & rs("merchant_logo") & vbTab
		strline = strline & rs("offer_type") & vbTab
		strline = strline & rs("promoCode")
		
		file.WriteLine strline
		RS.movenext
	loop
end if
file.close()

'sql	=	"WITH MS_CTE (itemid, itemdesc, artist, designname, modelname, brandname, price_we, defaultimg, musicSkinsID, itemweight)" & vbcrlf & _
'		"AS" & vbcrlf & _
'		"(" & vbcrlf & _
'		"	select	id as itemid, artist + ' ' + designName + '-music-skins-for-' + c.brandName + '-' + b.modelName as itemdesc" & vbcrlf & _
'		"		,	a.artist, a.designname, b.modelname, c.brandname, a.price_we, a.defaultImg, a.musicSkinsID, 2 itemweight" & vbcrlf & _
'		"	from 	we_items_musicSkins a join we_models b" & vbcrlf & _
'		"		on	a.modelid = b.modelid join we_brands c" & vbcrlf & _
'		"		on	a.brandid = c.brandid " & vbcrlf & _
'		"	where 	skip = 0" & vbcrlf & _
'		"		and deleteItem = 0" & vbcrlf & _
'		"		and (artist is not null or artist <> '')" & vbcrlf & _
'		"		and (designName is not null or designName <> '')" & vbcrlf & _
'		"		and (b.modelname is not null or b.modelname <> '')" & vbcrlf & _
'		"		and (c.brandname is not null or c.brandname <> '')" & vbcrlf & _
'		"		and b.isTablet = 0" & vbcrlf & _
'		")" & vbcrlf & _
'		"select	a.itemid, a.itemdesc, a.artist, a.designname, a.modelname, a.brandname, a.price_we, a.defaultimg, a.musicSkinsID, a.itemweight" & vbcrlf & _
'		"from	MS_CTE a left outer join ( select itemdesc, count(*) cnt from MS_CTE group by itemdesc having count(*) > 1) b" & vbcrlf & _
'		"	on	a.itemdesc = b.itemdesc" & vbcrlf & _
'		"where	b.itemdesc is null;"
'			
'set objRsMusicSkins = CreateObject("ADODB.Recordset")
'objRsMusicSkins.open SQL, oConn, 0, 1
'
'if not objRsMusicSkins.EOF then 
'	do until objRsMusicSkins.EOF
'		if hasImageOnRemote("http://www.wirelessemporium.com", "productpics/musicSkins/musicSkinsDefault/thumbs/" & objRsMusicSkins("defaultImg")) then	
'			strFeatures = 	""
'			strFeatures =	strFeatures	&	" - Thin material wraps around your " & objRsMusicSkins("brandname") & "&nbsp;" & objRsMusicSkins("modelname") & " without adding any bulk"
'			strFeatures =	strFeatures	&	" - Provides basic layer of protection from scratches and dust"
'			strFeatures =	strFeatures	&	" - Premium grade vinyl material and high gloss finish for a stunning look"
'			strFeatures =	strFeatures	&	" - Custom cut and non-permanent"
'			strFeatures =	strFeatures	&	" - NOT REUSABLE"
'			
'			strItemLongDetail 	= 							objRsMusicSkins("brandname") & " " & objRsMusicSkins("modelname") & " " & objRsMusicSkins("artist") & " " & objRsMusicSkins("designname") & " Music Skins - There is no better way to personalize your phone than with " 
'			strItemLongDetail	=	strItemLongDetail	&	"a Music Skin. Music skins are a thin, ""sticker-like"" covering made from a patented 3M material that wraps around your "  
'			strItemLongDetail	=	strItemLongDetail	&	"entire phone, helping to prevent scratches and dust from marring the look of your phone. Easy to apply and non-permanent, "  
'			strItemLongDetail	=	strItemLongDetail	&	"Music Skins are custom cut to fit your phone and add virtually no bulk at all to your phone. Even with your phone wrapped "  
'			strItemLongDetail	=	strItemLongDetail	&	"in a Music Skin you can use it in conjunction with any case, holster or charging dock."
'			strItemLongDetail	=	strItemLongDetail	&	strFeatures
'			
'			itemDesc = objRsMusicSkins("brandname") & " " & objRsMusicSkins("modelname") & " " & objRsMusicSkins("artist") & " " & objRsMusicSkins("designname") & " Music Skins"
'			if len(itemDesc) > 69 then itemDesc = left(itemDesc, 66) & "..."
'			
'			strline = objRsMusicSkins("itemid") & vbTab
'			strline = strline & itemDesc & vbTab
'			strline = strline & strItemLongDetail & vbtab
'			strline = strline & "Electronics > Communications > Telephony > Mobile Phone Accessories" & vbTab
'			strline = strline & "Electronics > Communications > Telephony > Mobile Phone Accessories" & vbTab
'			strline = strline & "http://www.wirelessemporium.com/p-ms-" & objRsMusicSkins("itemID") & "-" & formatSEO(objRsMusicSkins("itemDesc")) & "?source=gbase" & vbTab
'			strline = strline & "http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault/thumbs/" & objRsMusicSkins("defaultImg") & vbTab		
'			strline = strline & "new" & vbTab
'			strline = strline & "in stock" & vbTab
'			strline = strline & objRsMusicSkins("price_we") & vbTab
'			strline = strline & objRsMusicSkins("brandname") & vbTab
'			strline = strline & "" & vbTab
'			strline = strline & objRsMusicSkins("musicSkinsID") & vbTab
'			strline = strline & "US:CA:8.00:y" & vbTab
'			strline = strline & "US:::0.00" & vbTab
'			strline = strline & objRsMusicSkins("itemweight") & vbTab
'			strline = strline & "Music Skins" & vbTab
'			strline = strline & objRsMusicSkins("modelname") & vbTab
'			strline = strline & "TRUE" & vbTab
'			strline = strline & "" & vbTab
'			strline = strline & ""
'
'			call fWriteFile(strline)
'		end if
'		objRsMusicSkins.movenext
'	loop
'end if
'objRsMusicSkins.close
'set objRsMusicSkins = nothing
file.close()


sub fWriteFile(pStrToWrite)
	nRows = nRows + 1
	
	if nRows > 50000 then
		nFileIdx = nFileIdx + 1
		nRows = 0
		file.close()
		set file = nothing
		
		path = folderName & filename & nFileIdx & ".txt"
		if fs.FileExists(path) then
			fs.DeleteFile(path)
		end if
		set file = fs.CreateTextFile(path)

		file.WriteLine headings
	end if
	
	file.WriteLine pStrToWrite
end sub


function stripCallToAction(val)
	stripCallToAction = val
	dim a
	
	if not isnull(val) then
		a = inStr(1,stripCallToAction,"why pay ridiculous prices",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"don't settle",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"order this quality",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"order this convenient",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"buy this quality",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"buy this convenient",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"order today",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"remember - wireless emporium",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"you won't find a better price",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"buy this stylish",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"buy this vertical",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"for the same product at",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"we're confident you'll be happy",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		stripCallToAction = replace(stripCallToAction,"don't pay retail prices","",1,9,1)
	end if
end function

function changeTypeNameSingular(stypeName)
	select case stypeName
		case "Antennas/Parts"
			changeTypeNameSingular = "Antenna"
		case "Batteries"
			changeTypeNameSingular = "Battery"
		case "Bling Kits & Charms"
			changeTypeNameSingular = "Bling Kit"
		case "Chargers"
			changeTypeNameSingular = "Charger"
		case "Data Cable & Memory"
			changeTypeNameSingular = "Data Cable"
		case "Faceplates"
			changeTypeNameSingular = "Faceplate/Cover"
		case "Hands-Free"
			changeTypeNameSingular = "Headset/Hands Free"
		case "Holsters/Belt Clips"
			changeTypeNameSingular = "Holster"
		case "Key Pads/Flashing"
			changeTypeNameSingular = "Colorful Keypad"
		case "Leather Cases"
			changeTypeNameSingular = "Case/Pouch"
		case else
			changeTypeNameSingular = "Universal Gear"
	end select
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(replace(replace(formatSEO, "*", "-"),"?","-"), "----", "-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function


call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub

function hasImageOnRemote(pRemoteURL)
	dim isExists : isExists = false
	
	oXMLHTTP.Open "HEAD", pRemoteURL, False	'communicating without responseBody, only looks up file header
	oXMLHTTP.Send
	
	if (oXMLHTTP.readyState = "4") and (oXMLHTTP.status = "200") and (instr(oXMLHTTP.getResponseHeader("Content-Type"), "image") > 0) then isExists = true
	
	hasImageOnRemote = isExists
end function