set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim fs, file, filename, path
filename = "zero-inventory.csv"
path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename
set fs = CreateObject("Scripting.FileSystemObject")
if fs.FileExists(path) then fs.DeleteFile(path)
set file = fs.CreateTextFile(path)

SQL = "SELECT itemID,itemDesc,PartNumber,numberOfSales,vendor FROM we_Items"
SQL = SQL & " WHERE (hideLive = 0) AND (typeID = 1) AND (inv_qty = 0) OR"
SQL = SQL & " (hideLive = 0) AND (typeID = 2) AND (inv_qty = 0) OR"
SQL = SQL & " (hideLive = 0) AND (typeID = 7) AND (inv_qty = 0) OR"
SQL = SQL & " (hideLive = 0) AND (typeID = 13) AND (inv_qty = 0)"
SQL = SQL & " ORDER BY PartNumber, itemID"
Set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.eof then
	bodyText = "<h3>No Zero-Inventory Items Found!</h3>"
else
	file.WriteLine "itemID,itemDesc,PartNumber,numberOfSales,vendor"
	holdPart = "9999999"
	a = 0
	do until RS.eof
		if RS("PartNumber") <> holdPart then
			strToWrite = RS("itemID") & ","
			strToWrite = strToWrite & chr(34) & RS("itemDesc") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("PartNumber") & chr(34) & ","
			SQL = "SELECT SUM(numberOfSales) AS SumSales FROM we_Items WHERE PartNumber='" & RS("PartNumber") & "'"
			Set RS2 = oConn.execute(SQL)
			strToWrite = strToWrite & chr(34) & RS2("SumSales") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("vendor") & chr(34)
			file.WriteLine strToWrite
			a = a + 1
		end if
		holdPart = RS("PartNumber")
		RS.movenext
	loop
	bodyText = "<h3>ZERO-INVENTORY REPORT</h3>"
	bodyText = bodyText & "<h3>TOTAL NUMBER OF RECORDS: " & a & "</h3>"
	bodyText = bodyText & "<h3>(CSV FILE ATTACHED)</h3>"
end if

RS.CLOSE
Set RS = nothing
Set RS2 = nothing
file.Close()

Set objErrMail = CreateObject("CDO.Message")
With objErrMail
	.From = "service@wirelessemporium.com"
	.To = "tony@wirelessemporium.com"
'	if weekday(now) = 5 then
'		.cc = "shweta@convonix.com,michael@wirelessemporium.com"
'	end if
	.Subject = "Zero-Inventory Report"
	.HTMLBody = bodyText
	.AddAttachment path
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
	.Configuration.Fields.Update
	.Send
End With

Set objErrMail = nothing
Set filesys = nothing
Set demofolder = nothing
Set demofile = nothing
Set filecoll = nothing

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
