set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

bTestMode = 0

sql	=	"exec [getPostPurchaseEmails] '" & date & "', '" & date & "', '0,2', " & bTestMode
set objRsOrders = oConn.execute(sql)

lastEmail = ""
curEmail = ""
lastOrderID 	= 	-1
curOrderID		=	-1
lastAccountID	=	-1
curAccountID	=	-1
lastSiteID		=	-1
strOrderDetails	=	""
numItem			=	0

if objRsOrders.state = 1 then
	'recordset loop(by store)
	do until objRsOrders is nothing
		do until objRsOrders.eof
			site_id			=	objRsOrders("site_id")
			curEmail 		=	objRsOrders("email")
			curOrderID		=	objRsOrders("orderid")
			'site_id			=	objRsOrders("site_id")
			orderDate		=	objRsOrders("scandate")
			curAccountID	=	objRsOrders("accountid")
			orderYear		=	Year(orderDate)
			orderMonth		=	MonthName(Month(orderDate))
			orderDay		=	Day(orderDate)
			strOrderDate	=	orderMonth & " " & orderDay & ", " & orderYear 
			
			
			if lastOrderID <> curOrderID then
				numItem = 0
				if strOrderDetails <> "" then
					strTemplate = swapAndSend(logoLink, headerText, custName, custNameAddress, topText, strShipAddress,strBillAddress, lastOrderID, strOrderDetails, orderSubTotalAmt, strOrderDate, lastAccountID, telephone, strTelephone, lastSiteID, promoCode, discountAmt, buySafeAmt, taxAmt, shippingFeeAmt, shoprunnerid, shipMethod, orderGrandTotalAmt, storeName, item_ids, lastEmail)
					
					if bTestMode = 1 then
'						response.write cdo_to & "<br />"
						cdo_to = "terry@wirelessemporium.com"
						call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "")
'						response.write site_id & "<br />"
'						response.write strTemplate & "<br />"
					else
						if lastSiteID <> site_id and lastSiteID <> -1 then
							call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "terry@wirelessemporium.com,ruben@wirelessemporium.com,wemarketing@wirelessemporium.com")
						else
							call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "")
						end if						
					end if
				
					strTemplate 	= 	dup_strTemplate	'refresh the template for an another e-mail.			
				end if

				strOrderDetails	= ""
				item_ids = ""
			end if
			
			'========================================== SET REPLACEMENT TEXTS ================================================
			lastOrderID 		= 	curOrderID
			lastAccountID		=	curAccountID
			lastSiteID			=	site_id
			lastEmail			=	curEmail
			fname				=	objRsOrders("fname")
			lname				=	objRsOrders("lname")
			if isnull(fname) or len(fname) <= 0 then
				custName			=	"<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>Customer</p>"
			else
				custName			=	ucase(left(fname, 1)) & lcase(right(fname,len(fname)-1))
				custNameAddress		=	ucase(left(fname, 1)) & lcase(right(fname,len(fname)-1)) & " " & ucase(left(lname, 1)) & lcase(right(lname,len(lname)-1))
			end if
			email				=	objRsOrders("email")
			cdo_to				=	email
			
			storeName			=	objRsOrders("longDesc")
			topText				=	"Thank you for ordering from " & storeName & ".com! We would like to notify you that your order has been SHIPPED to:"			
			trackLink			=	objRsOrders("track_link")	'shipping track
			arr_track 			=	split(trackLink, "=")
			for each x in arr_track
				trackNum		=	x
			next				
			shipMethod			=	objRsOrders("shiptype")
			shippedBy			=	objRsOrders("shipped_by")
			promoCode			=	objRsOrders("promoCode")
			shoprunnerid		=	objRsOrders("shoprunnerid")
						
			orderSubTotalAmt	=	cdbl(objRsOrders("ordersubtotal"))
			orderGrandTotalAmt	=	cdbl(objRsOrders("ordergrandtotal"))
			taxAmt				=	cdbl(objRsOrders("orderTax"))
			discountAmt			=	cdbl(objRsOrders("discount"))
			buySafeAmt			=	cdbl(objRsOrders("BuySafeAmount"))
			shippingFeeAmt		=	cdbl(objRsOrders("ordershippingfee"))
			cdo_from			=	storeName & "<sales@" & Lcase(storeName) & ".com>"
			strOrder			=	"Order"
			if objRsOrders("reshipType") = 2 then strOrder = "Backorder"
			
			'linkDomain	= "https://www.wirelessemporium.com/"
			
			if site_id = 0 then
				self_link = "http://www.wirelessemporium.com/images/email/post-purchase/post-purchase?o=" & lastOrderID & "&a=" & lastAccountID & "&dzid=email_transactional_HEADER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=wePostPurchase&utm_content=Confirmation&utm_promocode=WEPURCHASE"
			elseif site_id = 2 then
				self_link = ""
			end if 
		
			
			telephone			= objRsOrders("phone")
			
			strTelephone		= "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>T: " & telephone & "</p>"
			
			strBillAddress     = formatAddress(objRsOrders("bAddress1"),objRsOrders("bAddress2"),objRsOrders("bCity"),objRsOrders("bState"),objRsOrders("bZip"))
			
			if objRsOrders("shippingid") > 0 then
				strShipAddress = formatAddress(objRsOrders("ssAddress1"),objRsOrders("ssAddress2"),objRsOrders("ssCity"),objRsOrders("ssState"),objRsOrders("ssZip"))
			else
				strShipAddress = formatAddress(objRsOrders("sAddress1"),objRsOrders("sAddress2"),objRsOrders("sCity"),objRsOrders("sState"),objRsOrders("sZip"))			
			end if
			
			strTest = ""
			if bTestMode = 1 then strTest = "[Test Batch]"
			select case site_id
				case 0
					headerText	=	strTest & "20% Off Your Next Order - A Thank You Gift From Wireless Emporium"
					logoLink	=	"http://www.wirelessemporium.com/images/global/we_logo_solo.gif"
				case 1
					headerText	=	"20% Off Your Next Order - A Thank You Gift From Cellphone Accents"
					logoLink	=	"http://www.cellphoneaccents.com/images/logo.jpg"
				case 2
					headerText	=	strTest & "20% Off Your Next Order - A Thank You Gift From Cellular Outfitter"
					logoLink	=	"http://www.cellularoutfitter.com/images/CellularOutfitter_STANDARD.jpg"
				case 3
					headerText	=	"20% Off Your Next Order - A Thank You Gift From Phonesale.com"
					logoLink	=	"http://www.phonesale.com/images/phonesale.jpg"
				case 10
					headerText	=	"20% Off Your Next Order - A Thank You Gift From Tabletmall.com"
					logoLink	=	"http://www.tabletmall.com/images/logo.gif"					
			end select

			cdo_subject	=	headerText			
			'=================================================================================================================
			
			'============================================== ORDER DETAIL TABLE ===============================================
			if instr(email, "@") > 0 and len(custName) > 0 then
				itemid		=	objRsOrders("itemid")
				if item_ids <> "" then
					item_ids = item_ids & ","
				end if
				item_ids = item_ids & itemid
				
				qty			=	cdbl(objRsOrders("quantity"))
				price		=	cdbl(objRsOrders("price"))
				subtotal	=	cdbl(qty * price)
				itemDesc 	=	objRsOrders("itemdesc")
				partnumber	=	objRsOrders("partnumber")
				
				itempic		= objRsOrders("itempic")
				
				'itemImgPath = server.MapPath("/productPics/thumb/" & itempic)
				itemImgPath = "C:\inetpub\wwwroot\productpics\thumb\" & itempic
				set fsThumb = CreateObject("Scripting.FileSystemObject")
				if not fsThumb.FileExists(itemImgPath) then
					useImg = "/productPics/thumb/imagena.jpg"
					DoNotDisplay = 1
				else
					useImg = "/productPics/thumb/" & itempic
				end if
														
				strOrderDetails =	strOrderDetails &	"	<tr>" & vbcrlf & _
														"		<td style='border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;'>" & partnumber & "</td>" & vbcrlf & _
														"		<td style='border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;'><img src='http://www.wirelessemporium.com" & useImg & "' height='65' /></td>" & vbcrlf & _
														"		<td style='border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;'>" & formatnumber(qty,0) & "</td>" & vbcrlf & _
														"		<td style='border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;'>" & itemDesc & "</td>" & vbcrlf & _
														"		<td style='border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;'>" & formatcurrency(price) & "</td>" & vbcrlf & _
														"		<td style='border-bottom:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;'>" & formatcurrency(subtotal) & "</td>" & vbcrlf & _
														"	</tr>"
				numItem = numItem + 1
			end if
			'=================================================================================================================		
		
			objRsOrders.movenext
		loop
		set objRsOrders = objRsOrders.nextrecordset
	loop
	
	if numItem > 0 then
		if strOrderDetails <> "" then
			'strOrderDetails	=	strOrderDetails	&	"			        	</table>"
			
			strTemplate = swapAndSend(logoLink, headerText, custName, custNameAddress, topText, strShipAddress,strBillAddress, lastOrderID, strOrderDetails, orderSubTotalAmt, strOrderDate, lastAccountID, telephone, strTelephone, lastSiteID, promoCode, discountAmt, buySafeAmt, taxAmt, shippingFeeAmt, shoprunnerid, shipMethod, orderGrandTotalAmt, storeName, item_ids, lastEmail)

			if bTestMode = 1 then
'				response.write cdo_to & "<br />"
				cdo_to = "terry@wirelessemporium.com"
				call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "")
'				response.write site_id & "<br />"
'				response.write strTemplate & "<br />"
			else
				call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "terry@wirelessemporium.com,wemarketing@wirelessemporium.com,ruben@wirelessemporium.com")
			end if
		
			strTemplate 	= 	dup_strTemplate	'refresh the template for an another e-mail.
		end if
	end if
end if


function swapAndSend(logoLink, headerText, custName, custNameAddress, topText, strShipAddress,strBillAddress, lastOrderID, strOrderDetails, orderSubTotalAmt, strOrderDate, lastAccountID, telephone, strTelephone, site_id, promoCode, discountAmt, buySafeAmt, taxAmt, shippingFeeAmt, shoprunnerid, shipMethod, orderGrandTotalAmt, storeName, item_ids, lastEmail)
	if site_id = 0 then
		strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\images\email\template\post-purchase.htm")
		dup_strTemplate = strTemplate
		
		strTemplate = replace(strTemplate, "[LOGO IMG LINK]", logoLink)
		strTemplate = replace(strTemplate, "[HEADER TEXT]", headerText)
		strTemplate = replace(strTemplate, "[CUSTOMER NAME]", custName)
		strTemplate = replace(strTemplate, "[CUSTOMER NAME ADDRESS]", custNameAddress)
		strTemplate = replace(strTemplate, "[TOP TEXT]", topText)
		strTemplate = replace(strTemplate, "[SHIPPED TO]", strShipAddress)
		strTemplate = replace(strTemplate, "[BILLED TO]", strBillAddress)
		strTemplate = replace(strTemplate, "[ORDER ID]", lastOrderID)					
		strTemplate = replace(strTemplate, "[ORDER DETAILS]", strOrderDetails)
		strTemplate = replace(strTemplate, "[ORDER SUBTOTAL]", formatcurrency(orderSubTotalAmt))
		strTemplate = replace(strTemplate, "[ORDER DATE]", strOrderDate)
		strTemplate = replace(strTemplate, "[EMAIL_PAGE]", "http://www.wirelessemporium.com/images/email/post-purchase/post-purchase?a=" & lastAccountID & "&o=" & lastOrderID & "&dzid=email_transactional_HEADER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=wePostPurchase&utm_content=Confirmation&utm_promocode=WEPURCHASE")
		strTemplate = replace(strTemplate, "[COPYRIGHT YEAR]", Year(Date))
		
		if telephone <> "" then
			strTemplate = replace(strTemplate, "[TELEPHONE]", strTelephone)
		else 
			strTemplate = replace(strTemplate, "[TELEPHONE]", "")
		end if
		
		if site_id = 0 then
			if shoprunnerid <> "" then
				strTemplate = replace(strTemplate, "[SR]", "Thanks for using FREE 2-Day Shipping by ShopRunner! For returns, visit https://www.shoprunner.com/returns to print your pre-paid return label. Don't forget, invite a Friend to join.")
			else
				strTemplate = replace(strTemplate, "[SR]", "Next time get FREE 2-Day Shipping from Wireless Emporium and other great retailers with your ShopRunner membership. Visit https://www.wirelessemporium.com/shoprunner to sign up for a FREE 30-Day Trial")
	'						strTemplate = replace(strTemplate, "[SR]", "")
			end if
		else
			strTemplate = replace(strTemplate, "[SR]", "")
		end if
		
		if promoCode <> "" then
			strTemp = 	"            	<tr>" & vbcrlf & _
						"					<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>PROMO CODE:</b></td>" & vbcrlf & _
						"					<td width=""80%"" align=""left"" valign=""top"">" & promoCode & "</td>" & vbcrlf & _
						"				</tr>" & vbcrlf
						
			strTemplate = replace(strTemplate, "[PROMO CODE]", strTemp)
		else
			strTemplate = replace(strTemplate, "[PROMO CODE]", "")					
		end if

		if discountAmt > 0 then				
			strTemp =	"(-" & formatcurrency(discountAmt) & ")"
			strTemplate = replace(strTemplate, "[DISCOUNT]", strTemp)
		else
			strTemplate = replace(strTemplate, "[DISCOUNT]", "")
		end if

		if buySafeAmt > 0 then
			strTemp = 	"				<tr>" & vbcrlf & _
						"					<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>buySAFE Bond:</b></td>" & vbcrlf & _
						"					<td width=""80%"" align=""left"" valign=""top"">" & formatcurrency(buySafeAmt) & "</td>" & vbcrlf & _
						"				</tr>" & vbcrlf					
			strTemplate = replace(strTemplate, "[BUY SAFE]", strTemp)
		else
			strTemplate = replace(strTemplate, "[BUY SAFE]", "")
		end if		
		
		strTemplate = replace(strTemplate, "[TAX AMOUNT]", formatcurrency(taxAmt))
		strTemplate = replace(strTemplate, "[SHIPPING FEE]", formatcurrency(shippingFeeAmt))
		if shoprunnerid <> "" then
			strTemplate = replace(strTemplate, "[SHIPPING TYPE]", "ShopRunner, 2-Day Shipping - FREE")
			strTemplate = replace(strTemplate, "[TR TRACKING]", "")
		else
			if shipMethod = "First Class" then
				strTemplate = replace(strTemplate, "[SHIPPING TYPE]", shipMethod & " (3-10 Business Days)")
			else 
				strTemplate = replace(strTemplate, "[SHIPPING TYPE]", shipMethod)
			end if
			if shippedBy = "UPS-MI"	then
				strMemo = "(Tracking may take 24-48 hours to appear after your mail piece has shipped.)"
			else
				strMemo = ""
			end if
			'strTracking = 	"            	<tr>" & vbcrlf & _
			'				"               	<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>SHIPMENT TRACKING:</b></td>" & vbcrlf & _
			'				"					<td width=""80%"" align=""left"" valign=""top""><a href=""" & trackLink & """ target=""_blank""><b>Click Here</b></a>&nbsp;" & strMemo & "</td>" & vbcrlf & _
			'				"				</tr>"
			
			strTracking = "<a href='" & trackLink & "' target='_blank' style='color:#0099ff;font-weight:bold;text-decoration:underline;'>#" & trackNum & "</a>"
			
			strTemplate = replace(strTemplate, "[TR TRACKING]", strTracking)
		end if
		strTemplate = replace(strTemplate, "[GRAND TOTAL]", formatcurrency(orderGrandTotalAmt))
		strTemplate = replace(strTemplate, "[STORE NAME]", storeName)
		
		if item_ids then strUpSell = getCrossSellProducts(site_id, item_ids, "")
		strTemplate = replace(strTemplate, "[UPSELL]", strUpSell)
	elseif site_id = 2 then
		if item_ids then strUpSell = getCrossSellProducts(site_id, item_ids, lastEmail)
		strTemplate = readTextFile("C:\inetpub\wwwroot\cellularoutfitter.com\www\images\email\template\co-post-purchase.htm")
		strTemplate = replace(strTemplate, "[ACCOUNT ID]", lastAccountID)
		strTemplate = replace(strTemplate, "[ORDER ID]", lastOrderID)
		strTemplate = replace(strTemplate, "[UPSELL]", strUpSell)
		strTemplate = replace(strTemplate, "[EXPIRY]", DateAdd("d", 7, Date()))
		strTemplate = replace(strTemplate, "[EMAIL]", lastEmail)
	end if
	
	swapAndSend = strTemplate
	
end function

function getCrossSellProducts(site_id, item_ids, lastEmail)
	if site_id = 0 then
		sqlUpSell = "exec sp_ppUpsell " & site_id & ", '" & item_ids & "', 0, 30"
		set objRsUpSell = oConn.execute(sqlUpSell)
		if not objRsUpSell.EOF then
			strUpSell	=	"<tr>" & vbcrlf & _
							"	<td style='background-color: #ffffff;' width='640'>" & vbcrlf & _
							"		<table width='640' cellspacing='0' cellpadding='0'>" & vbcrlf & _
							"			<tbody>" & vbcrlf & _
							"				<tr>"
			
			counter = 1
			do until objRsUpSell.EOF
				itemID		= objRsUpSell("itemID")
				itemPic		= objRsUpSell("itemPic")
				itemDesc	= objRsUpSell("itemDesc")
				useItemDesc = replace(itemDesc,"  "," ")
				price		= objRsUpSell("price")
				discountRate = 0.2
				discount	= price * discountRate
				promoPrice	= price - discount
				price_retail= objRsUpSell("price_retail")
				
				save_price	= cdbl(price_retail - promoPrice)
				save_percent= Round(save_price * 100 / price_retail)
			
				itemImgPath = "C:\inetpub\wwwroot\productpics\big\" & itempic
				set fsBig = CreateObject("Scripting.FileSystemObject")
				if not fsBig.FileExists(itemImgPath) then
					useImg = "/productPics/big/imagena.jpg"
					DoNotDisplay = 1
				else
					useImg = "/productPics/big/" & itemPic
				end if
				
				if counter = 3 or counter = 6 then
					style = "padding:20px 10px 0 20px;"
				else 
					style = "padding:20px 0 0 20px;"
				end if
				
				productLink = "http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(useItemDesc) & "?dzid=email_transactional_RECOMMENDATIONS_" & counter & "L_" & itemID & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=wePostPurchase&utm_content=Confirmation&utm_promocode=WEPURCHASE"
				
				strUpSell 	= strUpSell &	"	<td width='210' style='" & style & "'>" & vbcrlf & _
												"		<table width='190' cellpadding='0' cellspacing='0'>" & vbcrlf & _
												"			<tbody>" & vbcrlf & _
												"				<tr>" & vbcrlf & _
												"					<td width='190' height='190'>" & vbcrlf & _
												"						<a href='" & productLink & "'><img src='http://www.wirelessemporium.com" & useImg & "' style='display:block;' width='190' /></a>" & vbcrlf & _
												"					</td>" & vbcrlf & _
												"				</tr>" & vbcrlf & _
												"				<tr>" & vbcrlf & _
												"					<td width='190' height='90' style=""padding:7px 0px 7px 0px;"" valign=""top"">" & vbcrlf & _
												"						<a href='" & productLink & "' target=""_blank"" style=""font-size:12px; color:#333; font-family: Arial, sans-serif;"">" & itemDesc & "</a>" & vbcrlf & _
												"					</td>" & vbcrlf & _
												"				</tr>" & vbcrlf & _
												"				<tr>" & vbcrlf & _
												"					<td>" & vbcrlf & _
												"						<span style='color:#999;font-size:12px;color:#ccc;text-decoration:line-through;font-family:Arial, sans-serif;'>Retail Price: " & formatCurrency(price_retail) & "</span><br />" & vbcrlf & _
												"						<span style='color:#999;font-size:12px;color:#ccc;text-decoration:line-through;font-family:Arial, sans-serif;'>Our Price: " & formatCurrency(price) & "</span><br />" & vbcrlf & _
												"					</td>" & vbcrlf & _
												"				</tr>" & vbcrlf & _
												"				<tr>" & vbcrlf & _
												"					<td style='padding:10px 0 0 0;'>" & vbcrlf & _
												"						<span style='color:#000;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;'>Promo Price:</span> <span style='color:#ff6633;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;'>" & formatCurrency(promoPrice) & "</span><br />" & vbcrlf & _
												"						<span style='color:#000;font-size:14px;font-family:Arial, sans-serif;'>You Save:</span> <span style='margin:10px 0 0 0;color:#000;font-size:14px;font-family:Arial, sans-serif;font-weight:bold;'>" & round(save_percent) & "% (" & formatCurrency(save_price) & ")</span>" & vbcrlf & _
												"					</td>" & vbcrlf & _
												"				</tr>" & vbcrlf & _
												"				<tr>" & vbcrlf & _
												"					<td style='padding:10px 0 0 0;'>" & vbcrlf & _
												"						<table width='160' border='0' cellpadding='0' cellspacing='0'>" & vbcrlf & _
												"							<tbody>" & vbcrlf & _
												"								<tr>" & vbcrlf & _
												"									<td align='center' style='background-color:#ff6633;border-radius:16px;padding:7px 0;'>" & vbcrlf & _
												"										<a href='" & productLink & "' style='color:#fff;font-weight:bold;font-size:14px;font-family:Arial, sans-serif;text-decoration:none;'>SHOP NOW &raquo;</a>" & vbcrlf & _
												"									</td>" & vbcrlf & _
												"								</tr>" & vbcrlf & _
												"							</tbody>" & vbcrlf & _
												"						</table>" & vbcrlf & _
												"					</td>" & vbcrlf & _
												"				</tr>" & vbcrlf & _
												"			</tbody>" & vbcrlf & _
												"		</table>" & vbcrlf & _
												"	</td>"
				
				if counter = 3 then
					strUpSell = strUpSell & "						</tr>" & vbcrlf & _
								"					</tbody>" & vbcrlf & _
								"				</table>" & vbcrlf & _
								"				<br />" & vbcrlf & _
								"			</td>" & vbcrlf & _
								"		</tr>" & vbcrlf & _
								"		<tr>" & vbcrlf & _
								"		  <td style='background-color: #cccccc;' width='640' height='1'></td>" & vbcrlf & _
								"		</tr>" & vbcrlf & _
								"		<tr>" & vbcrlf & _
								"			<td style='background-color: #ffffff;' width='640'>" & vbcrlf & _
								"				<table width='640' cellspacing='0' cellpadding='0'>" & vbcrlf & _
								"					<tbody>" & vbcrlf & _
								"						<tr>"
					counter = counter + 1
				elseif counter = 6 then
					exit do
				else
					counter = counter + 1
				end if
				objRsUpSell.movenext
			loop
		end if
		strUpSell		= strUpSell & "						</tr>" & vbcrlf & _
								"					</tbody>" & vbcrlf & _
								"				</table>" & vbcrlf & _
								"				<br />" & vbcrlf & _
								"			</td>" & vbcrlf & _
								"		</tr>"
						
	elseif site_id = 2 then
		sqlUpSell = "exec sp_ppUpsell " & site_id & ", '" & item_ids & "', 0, 30"
		set recommendRS = oConn.execute(sqlUpSell) 

		strUpSell = ""

		if not recommendRS.EOF then
			strUpSell = strUpSell & "		<table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf
			strUpSell = strUpSell & "			<tr>" & vbcrlf
			strUpSell = strUpSell & "				<td style=""background-color: #ffffff;"" width=""20"">&nbsp;</td>" & vbcrlf
			counter = 1
			do until recommendRS.EOF
				itemID		= recommendRS("itemID")
				itemPic		= recommendRS("itemPic")
				itemDesc	= recommendRS("itemDesc")
				useItemDesc = replace(itemDesc,"  "," ")
				itemImgPath = "C:\inetpub\wwwroot\productpics_co\big\" & itemPic
				price		= cdbl(recommendRS("price"))
				price_retail= cdbl(recommendRS("price_retail"))
				price_diff	= cdbl(price_retail - price)
				diff_percent	= cdbl(price_diff * 100 / price_retail) 
				
				our_price	= cdbl(price - (price * 0.2))
				save_price	= cdbl(price_retail - our_price)
				save_percent= Round(save_price * 100 / price_retail)
				
				
				productLink = "http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".html?dzid=email_transactional_RECOMMENDATIONS_" & counter & "L_" & itemID & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=coPostPurchase&utm_content=Confirmation&utm_promocode=THANKS20"
				
				set fsThumb = CreateObject("Scripting.FileSystemObject")
				if not fsThumb.FileExists(itemImgPath) then
					useImg = "http://www.cellularoutfitter.com/productPics/big/imagena.jpg"
					DoNotDisplay = 1
				else
					useImg = "http://www.cellularoutfitter.com/productPics/big/" & itemPic
				end if
				
				strUpSell = strUpSell & "				<td style=""background-color: #ffffff; color: #006699; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 150%; margin: 0;"" width=""200"" align=""center"" valign=""top"" >" & vbcrlf
				strUpSell = strUpSell & "					<a href=""" & productLink & """ target=""_blank""><img style=""display: block;"" src=""" & useImg & """ alt="""" width=""180"" border=""0"" /></a>" & vbcrlf
				strUpSell = strUpSell & "					<p style=""color:#006697; font:normal 11px Verdana, Geneva, sans-serif; line-height:1.5; margin:0; width:180px; height:60px;"">" & vbcrlf
				strUpSell = strUpSell & "						<a style=""color:#006697;"" href=""" & productLink & """ target=""_blank"">" & itemDesc & "</a>" & vbcrlf
				strUpSell = strUpSell & "					</p>" & vbcrlf
				strUpSell = strUpSell & "					<br />" & vbcrlf
				strUpSell = strUpSell & "					<p style=""color:#006697; font:normal 12px Verdana, Geneva, sans-serif; line-height:1.1; margin:0; padding-bottom:10px; "">" & vbcrlf
				strUpSell = strUpSell & "						<span style=""font-size:11px; color:#cccccc;"">Retail Price: <span style=""font:normal Verdana, Geneva, sans-serif; text-decoration:line-through "">" & formatCurrency(price_retail) & "</span></span><br />" & vbcrlf
				strUpSell = strUpSell & "						<span style=""font-size:11px; color:#cccccc;"">Our Price: <span style=""font:normal Verdana, Geneva, sans-serif; text-decoration:line-through "">" & formatCurrency(price) & "</span></span>" & vbcrlf
				strUpSell = strUpSell & "						<br />" & vbcrlf
				strUpSell = strUpSell & "						<strong style=""color:#000; font-size:14px"">Promo Price: " & formatCurrency(our_price) & "</strong>" & vbcrlf
				strUpSell = strUpSell & "						<br /><br />" & vbcrlf
				strUpSell = strUpSell & "						<a href=""" & productLink & """ style=""color:#2dcc70; text-decoration:none; font-size:12px; ""><strong>YOU SAVE " & formatCurrency(save_price) & " (" & round(save_percent) & "%)</strong></a>" & vbcrlf
				strUpSell = strUpSell & "					</p>" & vbcrlf
				strUpSell = strUpSell & "					<a href=""" & productLink & """ target=""_blank"">" & vbcrlf
				strUpSell = strUpSell & "						<table style=""padding-bottom:60px;"" width=""150"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf
				strUpSell = strUpSell & "							<tr>" & vbcrlf
				strUpSell = strUpSell & "								<td style=""text-align:center;font-weight:normal;color:#ffffff;font-size:14px;line-height:18px;background:#33cc66;border:1px solid #009966;border-radius:20px;padding:5px 10px;"">BUY NOW &raquo;</td>" & vbcrlf
				strUpSell = strUpSell & "							</tr>" & vbcrlf
				strUpSell = strUpSell & "						</table>" & vbcrlf
				strUpSell = strUpSell & "					</a>" & vbcrlf
				strUpSell = strUpSell & "				</td>" & vbcrlf
				
				if counter = 3 then
					strUpSell = strUpSell & "			<td style=""background-color: #ffffff;"" width=""20"">&nbsp;</td>" & vbcrlf
					strUpSell = strUpSell & "		</tr>" & vbcrlf
					strUpSell = strUpSell & "		<tr>" & vbcrlf
					strUpSell = strUpSell & "			<td style=""background-color: #ffffff;"" width=""20"">&nbsp;</td>" & vbcrlf
				elseif counter = 6 then
					strUpSell = strUpSell & "			<td style=""background-color: #ffffff;"" width=""20"">&nbsp;</td>" & vbcrlf
					strUpSell = strUpSell & "		</tr>" & vbcrlf
					strUpSell = strUpSell & "	</table>" & vbcrlf
					
					exit do
				end if
				
				recommendRS.movenext
				counter = counter + 1
			loop
		end if
	end if

	getCrossSellProducts = strUpSell
end function


function formatAddress(sAdd1,sAdd2,sCity,sState,sZip)
	dim strTemp
	strTemp = sAdd1
	if sAdd2 <> "" then 
		strTemp = strTemp & "<br />" & sAdd2
	end if
	strTemp = strTemp & "<br />" & sCity & ", " & sState & "&nbsp;" & sZip
	formatAddress = strTemp
end function

function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
end function


sub cdosend(strFrom, strTo, strSubject, strBody, strBcc)
	on error resume next
	if instr(trim(strTo), "@") > 0 and instr(trim(strTo), " ") = 0 then
		set objEmail = CreateObject("CDO.Message")
		with objEmail
			.From = strFrom
			.To = strTo
			.Bcc = strBcc
			.Subject = strSubject
			.HTMLBody = strBody
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Update
			.Send
		end with
	end if
	on error goto 0
end sub

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")		
	else
		formatSEO = ""
	end if
	formatSEO = replace(formatSEO, "---", "-")
	formatSEO = replace(formatSEO, "--", "-")	
end function

Function isValidEmail(emailAddress)
	'Declare variables 
	Dim ValidEmail, emailParts, iLoopCounter, emailChar, acceptableChars, last2Chars 
	ValidEmail = True 'set the default result to True
	'acceptableChars are the characters that we will allow in our email
	acceptableChars="abcdefghijklmnopqrstuvwxyz.-_@"
	'use the Split function to create an array with the @ as the separator
	'so if your email was test@tester.com the email would be split into an array
	'with the first array element holding "test" and the second "tester.com"
	emailParts = Split(emailAddress, "@")
	'check to make sure that there is only 1 @ and that there are 2 parts
	'remember arrays are zero based
	'Using the UBound function will return the highest element in the array
	'So if it's a valid email the UBound function will return 1, i.e. 0 start
	If UBound(emailParts) <> 1 Then
	ValidEmail = false
	Else 
	'Check the length of each part of the email address
	'first part can be just one character, 2nd part must be atleast 4
	If Len(emailParts(0))<1 OR Len(emailParts(1))<4 Then
	ValidEmail = false
	End If
	'check first character on the left part isn't a "." using Left function
	If Left(emailParts(0), 1)="." Then
	ValidEmail = false
	End If 
	'check that there is a . in the second part of the email address - .com
	If InStr(emailParts(1), ".") <= 0 Then
	ValidEmail = false 
	End If 
	'We know there's a . now make sure there are 2 characters after the . for valid email
	last2Chars=Right(emailParts(1),2)
	If InStr(last2chars,".") Then
	ValidEmail = false
	End If
	'check that there shouldn't be a _ in the second part of the email address 
	If InStr(emailParts(1), "_") >0 Then
	ValidEmail = false 
	End If 
	End If
	'loop through each character of email
	For iLoopCounter = 1 to Len(emailAddress)
	'Use Lcase & Mid functions, Mid function used to return each individual character
	'in the email, and then Lcase converts it into lowercase
	emailChar = Lcase(Mid(emailAddress, iLoopCounter, 1)) 
	'Check if the emailAddress characters are acceptable
	If InStr(acceptableChars, emailChar) = 0 and Not IsNumeric(emailChar) Then
	ValidEmail = false
	End if
	Next
	'check if there is 2 . in a row
	If InStr(emailAddress, "..") > 0 Then
	ValidEmail=false
	End If
	'check if there is @. in a row
	If InStr(emailAddress, "@.") > 0 Then
	ValidEmail=false
	End If 
	isValidEmail=ValidEmail
End function