set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()
set fs = CreateObject("Scripting.FileSystemObject")

call initConnection(oFTP,"ftp03.newegg.com","A2SE","bears1986")

success = oFTP.ChangeRemoteDir("/Inbound/Shipping")
if (success <> 1) then
'    Response.Write oFTP.LastErrorText & "<br>"
end if

SDate = dateadd("d", -1, date)
EDate = date
'SDate = date
'EDate = dateadd("d", 1, date)

set fs = CreateObject("Scripting.FileSystemObject")
filename = "ShipNotice_" & year(date) & right("00" & month(date), 2) & right("00" & day(date), 2) & ".xml"
path = "C:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\newegg\upload\" & filename

if fs.FileExists(path) then fs.DeleteFile(path)

'Create the file and write some data to it.
set file = fs.CreateTextFile(path)

sql	=	"select	a.orderid, a.extOrderNumber, b.itemid, c.partnumber, b.extItemID, b.quantity" & vbcrlf & _
		"	,	isnull(a.trackingnum, 'WE-' + convert(varchar(20), a.orderid)) trackingnum" & vbcrlf & _
		"	,	case when a.trackingnum is null or a.trackingnum = '' then 0 else 1 end trackingType" & vbcrlf & _
		"	,	a.scandate" & vbcrlf & _
		"	,	d.sAddress1 + ' ' + d.sAddress2 sAddress, d.sCity, d.sState, d.sZip, d.phone" & vbcrlf & _
		"from	we_orders a join we_orderdetails b" & vbcrlf & _
		"	on	a.orderid = b.orderid join we_items c" & vbcrlf & _
		"	on	b.itemid = c.itemid join we_accounts d" & vbcrlf & _
		"	on	a.accountid = d.accountid" & vbcrlf & _
		"where	a.scandate >= '" & SDate & "'" & vbcrlf & _
		"	and	a.scandate < '" & EDate & "'" & vbcrlf & _
		"	and	a.store = 0" & vbcrlf & _
		"	and	a.approved = 1" & vbcrlf & _
		"	and	(a.cancelled is null or a.cancelled = 0)" & vbcrlf & _
		"	and	a.extOrderType = 10 -- newegg order" & vbcrlf & _
		"order by a.orderid, b.orderdetailid"

Set rs = oConn.execute(sql)
numOrders = 0

if not rs.eof then
	curOrderID = cdbl(-1)
	lastOrderID = cdbl(-1)
	xmlFeed =	"<?xml version=""1.0"" encoding=""UTF-8""?>" & vbcrlf & _
				"<NeweggEnvelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" >" & vbcrlf & _
				"	<Header>" & vbcrlf & _
				"		<DocumentVersion>1.0</DocumentVersion>" & vbcrlf & _
				"	</Header>" & vbcrlf & _
				"	<MessageType>ShipNotice</MessageType>" & vbcrlf & _
				"	<Message>" & vbcrlf & _
				"		<ShipNotice>" & vbcrlf

	do until rs.eof
		numOrders = numOrders + 1
		curOrderID = rs("orderid")
		extOrderid = rs("extOrderNumber")
		itemid = rs("itemid")
		partnumber = rs("partnumber")
		extItemid = rs("extItemID")
		quantity = rs("quantity")
		scandate = rs("scandate")
		trackingnum = rs("trackingnum")
		address = rs("sAddress")
		city = rs("sCity")
		sState = rs("sState")
		sZip = rs("sZip")
		phone = rs("phone")
		
		if lastOrderID <> curOrderID then
			if lastOrderID <> -1 then
				xmlFeed = xmlFeed	&	"				</ItemInformation>" & vbcrlf & _
										"				<ShipDate>" & scandate & "</ShipDate>" & vbcrlf & _
										"				<ActualShippingCarrier>USPS</ActualShippingCarrier>" & vbcrlf & _
										"				<ActualShippingMethod>First Class</ActualShippingMethod>" & vbcrlf & _
										"				<TrackingNumber>" & trackingnum & "</TrackingNumber>" & vbcrlf & _
										"				<ShippingFromInformation>" & vbcrlf & _
										"					<ShippingFromAddress>" & address & "</ShippingFromAddress>" & vbcrlf & _
										"					<ShippingFromCity>" & city & "</ShippingFromCity>" & vbcrlf & _
										"					<ShippingFromState>" & sState & "</ShippingFromState>" & vbcrlf & _
										"					<ShippingFromZipcode>" & sZip & "</ShippingFromZipcode>" & vbcrlf & _
										"					<PhoneNumber>" & phone & "</PhoneNumber>" & vbcrlf & _
										"				</ShippingFromInformation>" & vbcrlf & _
										"			</Package>" & vbcrlf
			end if
										
		
			xmlFeed = xmlFeed	&	"			<Package>" & vbcrlf & _
									"				<OrderNumber>" & extOrderid & "</OrderNumber>" & vbcrlf & _
									"				<ItemInformation>" & vbcrlf
		end if
		
		xmlFeed = xmlFeed	&	"					<Item>" & vbcrlf & _
								"						<SellerPartNumber>" & partnumber & "-" & itemid & "</SellerPartNumber>" & vbcrlf & _
								"						<ShippedQuantity>" & quantity & "</ShippedQuantity>" & vbcrlf & _
								"						<NeweggItemNumber>" & extItemid & "</NeweggItemNumber>" & vbcrlf & _
								"					</Item>" & vbcrlf

		lastOrderID = curOrderID	
		rs.movenext
	loop	

	xmlFeed = xmlFeed	&	"				</ItemInformation>" & vbcrlf & _
							"				<ShipDate>" & scandate & "</ShipDate>" & vbcrlf & _
							"				<ActualShippingCarrier>USPS</ActualShippingCarrier>" & vbcrlf & _
							"				<ActualShippingMethod>First Class</ActualShippingMethod>" & vbcrlf & _
							"				<TrackingNumber>" & trackingnum & "</TrackingNumber>" & vbcrlf & _
							"				<ShippingFromInformation>" & vbcrlf & _
							"					<ShippingFromAddress>" & address & "</ShippingFromAddress>" & vbcrlf & _
							"					<ShippingFromCity>" & city & "</ShippingFromCity>" & vbcrlf & _
							"					<ShippingFromState>" & sState & "</ShippingFromState>" & vbcrlf & _
							"					<ShippingFromZipcode>" & sZip & "</ShippingFromZipcode>" & vbcrlf & _
							"					<PhoneNumber>" & phone & "</PhoneNumber>" & vbcrlf & _
							"				</ShippingFromInformation>" & vbcrlf & _
							"			</Package>" & vbcrlf & _
							"		</ShipNotice>" & vbcrlf & _
							"	</Message>" & vbcrlf & _
							"</NeweggEnvelope>" & vbcrlf
	file.write xmlFeed
end if

file.close()

success = oFTP.PutFile(path, oFTP.GetCurrentRemoteDir() & "/" & filename)
if success <> 1 then 
'	response.write "ftp connection error - " & ftp.LastErrorText
end if

sub initConnection(byref ftp,hostname,username,password)
	success = 0

	set ftp = CreateObject("Chilkat.Ftp2")
	success = ftp.UnlockComponent("WIRELEFTP_GQ2TEoLE9Bnh")

	ftp.Hostname = hostname
	ftp.Username = username
	ftp.Password = password

	success = ftp.Connect()
'	if success <> 1 then 
'		response.write "<br>ftp connection error - " & ftp.LastErrorText & "<br>"
'		response.end
'	end if
end sub

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub