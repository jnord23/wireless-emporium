set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

sDate		=	date
cdo_body	=	""
cdo_to 		= 	"charles@wirelessemporium.com,ruben@wirelessemporium.com,bob@wirelessemporium.com,terry@wirelessemporium.com"
'cdo_to		=	"terry@wirelessemporium.com"
cdo_from 	= 	"sales@wirelessemporium.com"
cdo_subject = 	"*** Whale Order Report ***"
strEmail	=	""

sql	=	"select	distinct x.shortDesc, a.orderid, a.ordergrandtotal, q.quantity, a.processPDF" & vbcrlf & _
		"from 	we_orders a with (nolock) join xstore x with (nolock)" & vbcrlf & _
		"	on	x.site_id = a.store join " & vbcrlf & _
		"		(" & vbcrlf & _
		"		select	a.store, a.orderid, isnull(sum(b.quantity), 0) quantity, sum(b.quantity * isnull(c.itemWeight, 0.0)) itemWeight" & vbcrlf & _
		"			,	count(*) numRegularOrder" & vbcrlf & _
		"			,	isnull(sum(case when m.id is not null then 1 " & vbcrlf & _
		"								else" & vbcrlf & _
		"									case when c.vendor in ('CM', 'DS', 'MLD') then 1 " & vbcrlf & _
		"										when c.partnumber like '%HYP%' then 1 " & vbcrlf & _
		"										else 0" & vbcrlf & _
		"									end" & vbcrlf & _
		"							end), 0) numVendorOrder" & vbcrlf & _
		"			,	sum(case when c.typeid = 16 then 1 else 0 end) numPhones" & vbcrlf & _
		"		from 	we_orders a with (nolock) join we_orderdetails b with (nolock)" & vbcrlf & _
		"			on	a.orderid = b.orderid left outer join we_items c with (nolock)" & vbcrlf & _
		"			on	b.itemid = c.itemid left outer join we_items_musicskins m with (nolock)" & vbcrlf & _
		"			on	b.itemid = m.id" & vbcrlf & _
		"		where	a.thub_posted_date >= '" & sDate & "'" & vbcrlf & _
		"			and	a.approved = 1 and (a.cancelled is null or a.cancelled = 0)" & vbcrlf & _
		"			and	(b.reship = 0 or b.reship is null)" & vbcrlf & _
		"			and	b.itemid not in (1)" & vbcrlf & _
		"		group by a.store, a.orderid" & vbcrlf & _
		"		) q" & vbcrlf & _
		"	on	a.store = q.store and a.orderid = q.orderid left outer join XShipType t with (nolock)" & vbcrlf & _
		"	on	a.shiptype = t.shipdesc left outer join XOrderType o with (nolock)" & vbcrlf & _
		"	on	a.extOrderType = o.typeid " & vbcrlf & _
		"where	a.thub_posted_date >= '" & sDate & "'" & vbcrlf & _
		"	and	a.approved = 1 and (a.cancelled is null or a.cancelled = 0)" & vbcrlf & _		
		"	and	x.active = 1" & vbcrlf & _
		"	and	not (q.quantity = 1 and q.numPhones = 1)" & vbcrlf & _
		"	and	(q.numRegularOrder >= 10 or cast(isnull(a.ordergrandtotal, 0.00) as money) >= 175.00)" & vbcrlf & _
		"order by 1, 2"
		
set objRsEmail = oConn.execute(sql)

if not objRsEmail.eof then
	strEmail = 	"<table width=""600"" border=""1"" align=""center"" cellpadding=""2"" cellspacing=""0"" style=""border-collapse:collapse;font-family: Arial, Helvetica, sans-serif; font-size:10px;"">" & vbcrlf & _
				"	<tr><td colspan=""5""><b>* Current Setting: </b>Any orders over $175 or more than 10 line items</td></tr>" & vbcrlf & _	
				"	<tr>" & vbcrlf & _
				"		<td><b>STORE</b></td>" & vbcrlf & _
				"		<td><b>ORDERID</b></td>" & vbcrlf & _
				"		<td><b>GRAND TOTAL</b></td>" & vbcrlf & _
				"		<td><b>QTY</b></td>" & vbcrlf & _
				"		<td><b>PDF</b></td>" & vbcrlf & _
				"	</tr>" & vbcrlf
				
	do until objRsEmail.eof
		strEmail	=	strEmail	&	"	<tr>" & vbcrlf
		strEmail	=	strEmail	&	"		<td>" & objRsEmail("shortdesc") & "</td>" & vbcrlf
		strEmail	=	strEmail	&	"		<td>" & objRsEmail("orderid") & "</td>" & vbcrlf
		strEmail	=	strEmail	&	"		<td>" & formatcurrency(objRsEmail("ordergrandtotal")) & "</td>" & vbcrlf
		strEmail	=	strEmail	&	"		<td>" & formatnumber(objRsEmail("quantity"), 0) & "</td>" & vbcrlf
		strEmail	=	strEmail	&	"		<td><a target=""_blank"" href=""http://www.wirelessemporium.com/admin/temppdf/salesorders/" & objRsEmail("processPDF") & """>" & objRsEmail("processPDF") & "</a></td>" & vbcrlf
		strEmail	=	strEmail	&	"	</tr>" & vbcrlf
		
		objRsEmail.movenext
	loop
	strEmail	=	strEmail	&	"</table>" & vbcrlf
end if		

cdo_body = strEmail

if cdo_body <> "" then
	call cdosend(cdo_to, cdo_from, cdo_subject, cdo_body)
end if


sub cdosend(strto,strfrom,strsubject,strbody)
	on error resume next
	dim objMail
	set objMail = createobject("cdo.message")
	with objMail
		.from = strfrom
		.to = strto
		.subject = strsubject
		.htmlbody = cstr("" & strbody)
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.configuration.fields.item("urn:schemas:httpmail:sender") = "sales@wirelessemporium.com"
		.configuration.fields.update
		.send
	end with
	set objMail = nothing
	on error goto 0
end sub

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
