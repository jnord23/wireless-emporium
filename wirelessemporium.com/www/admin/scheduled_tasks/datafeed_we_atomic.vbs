set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

'dim fs, file, filename, filename2, filename3, filename4, path, path2, path3, path4, loopCount, currentFile
set fs = CreateObject("Scripting.FileSystemObject")

'filename = "productList_AtomicMall.txt"
'path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename
'filename2 = "productList_AtomicMall2.txt"
'path2 = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename2
'filename3 = "productList_AtomicMall3.txt"
'path3 = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename3
'filename4 = "productList_AtomicMall4.txt"
'path4 = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename4
'currentFile = filename
'
'if fs.FileExists(path) then fs.DeleteFile(path) end if
'if fs.FileExists(path2) then fs.DeleteFile(path2) end if
'if fs.FileExists(path3) then fs.DeleteFile(path3) end if
'if fs.FileExists(path4) then fs.DeleteFile(path4) end if

filename = "productList_AtomicMall"
fileext = "txt"
nRows = 0
folderName = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\"
path = folderName & filename & "." & fileext

nFileIdx = 0

set folder = fs.GetFolder(folderName)
set files = folder.Files

for each folderIdx in files
	if instr(folderIdx.Name, filename) > 0 then
'		response.write folderpath & "\" & folderIdx.Name & "<br>"
		if fs.FileExists(folderName & folderIdx.Name) then
			fs.DeleteFile(folderName & folderIdx.Name)
		end if
	end if
next
set file = fs.CreateTextFile(path)

'Create the file and write some data to it.
sql	=	"	select	a.itemid,a.partnumber,a.brandid,a.typeid,a.itemdesc	" & vbcrlf & _
		"		,	a.itempic,a.price_retail,a.price_our, a.hidelive" & vbcrlf & _
		"		,	case when a.hidelive = 1 then 0" & vbcrlf & _
		"				when a.inv_qty = 0 then 0" & vbcrlf & _
		"				else" & vbcrlf & _
		"					case when a.inv_qty = -1 then isnull(x.inv_qty, 0) else a.inv_qty end " & vbcrlf & _
		"			end inv_qty" & vbcrlf & _
		"		,	a.bullet1,a.bullet2,a.bullet3,a.bullet4,a.bullet5,a.bullet6	" & vbcrlf & _
		"		,	a.compatibility,a.handsfreetype,a.upccode,dbo.fn_stripHTML(replace(replace(replace(convert(varchar(8000), a.itemLongDetail), char(10), ' '), char(13), ' '), char(9), ' ')) itemlongdetail" & vbcrlf & _
		"		,	b.brandname,c.modelname,c.[temp],d.typename	" & vbcrlf & _
		"	from	we_items a with (nolock) join we_types d with (nolock)	" & vbcrlf & _
		"		on	a.typeid = d.typeid left outer join we_brands b with (nolock)	" & vbcrlf & _
		"		on	a.brandid = b.brandid left outer join we_models c with (nolock)	" & vbcrlf & _
		"		on	a.modelid = c.modelid left outer join 	" & vbcrlf & _
		"			(	" & vbcrlf & _
		"			select	partnumber, max(inv_qty) inv_qty" & vbcrlf & _	
		"			from	we_items with (nolock)" & vbcrlf & _	
		"			where	hidelive = 0 and inv_qty > 0 and price_our > 0" & vbcrlf & _	
		"			group by partnumber	" & vbcrlf & _	
		"			) x	" & vbcrlf & _
		"		on	a.partnumber = x.partnumber left outer join " & vbcrlf & _
		"			(" & vbcrlf & _
		"			select	a.partnumber orphan_partnumber" & vbcrlf & _
		"			from	(" & vbcrlf & _
		"					select	partnumber" & vbcrlf & _
		"						,	sum(case when inv_qty > 0 then 1 else 0 end) nMaster" & vbcrlf & _
		"						,	sum(case when inv_qty < 0 then 1 else 0 end) nSlave	" & vbcrlf & _
		"					from	we_items a with (nolock)" & vbcrlf & _
		"					where	hidelive = 0 and inv_qty <> 0 and price_our > 0" & vbcrlf & _
		"					group by partnumber" & vbcrlf & _
		"					) a	" & vbcrlf & _
		"			where	a.nMaster = 0" & vbcrlf & _
		"			) o" & vbcrlf & _
		"		on	a.partnumber = o.orphan_partnumber" & vbcrlf & _
		"	where	a.price_our > 0" & vbcrlf & _
		"		and	o.orphan_partnumber is null and a.partnumber not like 'WCD-%'" & vbcrlf & _
		"	order by a.itemid" & vbcrlf

set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
	
headings = "itemname" & vbTab & "qty" & vbTab & "price" & vbTab & "condition" & vbTab & "mfr" & vbTab & "shiptime" & vbTab & "shipto" & vbTab & "shipping_reg" & vbTab & "shipping_exp" & vbTab & "shipping_intl" & vbTab & "method_reg" & vbTab & "method_exp" & vbTab & "method_intl" & vbTab & "image" & vbTab & "image_2" & vbTab & "image_3" & vbTab & "image_4" & vbTab & "image_5" & vbTab & "image_6" & vbTab & "upc" & vbTab & "sku" & vbTab & "description" & vbTab & "notes" & vbTab & "specs" & vbTab & "warranty" & vbTab & "reg_price" & vbTab & "sale_expdate" & vbTab & "category" & vbTab & "subcategory" & vbTab & "subsub" & vbTab & "storecat"

if not RS.eof then
	dim AddlImages(2)
	dim strItemDesc, LongDescription, price_our, subcategory
	dim myDate, thisMonth, thisDay, expDate	

	file.WriteLine headings
		
	do until RS.eof

		myDate = dateAdd("d",30,date)
		thisMonth = cStr(month(myDate))
		if month(myDate) < 10 then thisMonth = "0" & thisMonth
		thisDay = cStr(day(myDate))
		if day(myDate) < 10 then thisDay = "0" & thisDay
		expDate = cStr(year(myDate)) & "-" & thisMonth & "-" & thisDay
		
		if not isNull(RS("itemDesc")) then strItemDesc = replace(RS("itemDesc"),"+","&")
		LongDescription = RS("itemLongDetail")
		if not isNull(LongDescription) then
			LongDescription = replace(LongDescription,vbCrLf," ")
			LongDescription = replace(LongDescription,vbTab," ")
			LongDescription = replace(LongDescription,"<a href='/downloads/","<a href='http://www.wirelessemporium.com/downloads/")
		end if
		
		if not isNull(RS("BULLET1")) and RS("BULLET1") <> "" then
			if right(RS("BULLET1"),1) <> "." and right(RS("BULLET1"),1) <> "!" then LongDescription = LongDescription & "."
			LongDescription = LongDescription & " "
		end if
		if not isNull(RS("BULLET2")) and RS("BULLET2") <> "" then
			LongDescription = LongDescription & RS("BULLET2")
			if right(RS("BULLET2"),1) <> "." and right(RS("BULLET2"),1) <> "!" then LongDescription = LongDescription & "."
			LongDescription = LongDescription & " "
		end if
		if not isNull(RS("BULLET3")) and RS("BULLET3") <> "" then
			LongDescription = LongDescription & RS("BULLET3")
			if right(RS("BULLET3"),1) <> "." and right(RS("BULLET3"),1) <> "!" then LongDescription = LongDescription & "."
			LongDescription = LongDescription & " "
		end if
		if not isNull(RS("BULLET4")) and RS("BULLET4") <> "" then
			LongDescription = LongDescription & RS("BULLET4")
			if right(RS("BULLET4"),1) <> "." and right(RS("BULLET4"),1) <> "!" then LongDescription = LongDescription & "."
			LongDescription = LongDescription & " "
		end if
		if not isNull(RS("BULLET5")) and RS("BULLET5") <> "" then
			LongDescription = LongDescription & RS("BULLET5")
			if right(RS("BULLET5"),1) <> "." and right(RS("BULLET5"),1) <> "!" then LongDescription = LongDescription & "."
			LongDescription = LongDescription & " "
		end if
		if not isNull(RS("BULLET6")) and RS("BULLET6") <> "" then
			LongDescription = LongDescription & RS("BULLET6")
			if right(RS("BULLET6"),1) <> "." and right(RS("BULLET6"),1) <> "!" then LongDescription = LongDescription & "."
			LongDescription = LongDescription & " "
		end if
		if not isNull(RS("COMPATIBILITY")) and RS("COMPATIBILITY") <> "" then
			COMPATIBILITY = replace(RS("COMPATIBILITY"),vbcrlf,"<br>")
			LongDescription = LongDescription & "Compatible with: " & COMPATIBILITY
			if right(COMPATIBILITY,1) <> "." and right(COMPATIBILITY,1) <> "!" then LongDescription = LongDescription & "."
		end if
		
		select case RS("typeID")
			case 1
				if RS("price_our") >= 7 and inStr(RS("itemDesc"),"flashing") = 0 then
					price_our = int(RS("price_our") * .7) + .99
				else
					price_our = 0
				end if
				subcategory = "Batteries & Chargers"
			case 2
				price_our = int(RS("price_our") * .65) + .99
				subcategory = "Batteries & Chargers"
			case 3
				if inStr(RS("PartNumber"),"FP2") > 0 then
					price_our = int(RS("price_our") * .7) - .01
				else
					price_our = 0
				end if
				subcategory = "Skins"
			case 5
				if inStr(RS("itemDesc"),"Audio Adapter") > 0 then
					price_our = int(RS("price_our") * .8) + .99
				elseif RS("HandsfreeType") <> 2 then
					price_our = (int(RS("price_our") * .7) - .01) + 1
				else
					price_our = 0
				end if
				subcategory = "Phone Accessories"
			case 6
				price_our = int(RS("price_our") * .75) + .99
				subcategory = "Cases & Holsters"
			case 7
				if inStr(RS("PartNumber"),"FP3") > 0 then
					price_our = int(RS("price_our") * .70) + .99
				elseif inStr(RS("PartNumber"),"LC5") > 0 then
					price_our = int(RS("price_our") * .75) + .99
				elseif inStr(RS("PartNumber"),"LC1") > 0 then
					price_our = int(RS("price_our") * .75) + .99
				elseif inStr(RS("PartNumber"),"LC2") > 0 then
					price_our = int(RS("price_our") * .75) + .99
				elseif inStr(RS("PartNumber"),"LC0") > 0 then
					price_our = int(RS("price_our") * .75) + .99
				else
					price_our = 0
				end if
				subcategory = "Cases & Holsters"
			case 13
				if inStr(RS("PartNumber"),"MEM-KIN-MCRO") > 0 or inStr(RS("PartNumber"),"MEM-SAN-MCRO") > 0 then
					price_our = int(RS("price_our") * .85) + .99
				elseif inStr(RS("PartNumber"),"DAT-UNI-BLUE") > 0 then
					price_our = int(RS("price_our") * .8) - .01
				elseif inStr(RS("PartNumber"),"DAT-KIT-MBAC") > 0 or inStr(RS("PartNumber"),"DTP-LGE-UNIV") > 0 or inStr(RS("PartNumber"),"DTP-SAM-UNIV") > 0 or inStr(RS("PartNumber"),"DTP-MOT-UNIV") > 0 or inStr(RS("PartNumber"),"DTP-SYO-UNIV") > 0 then
					price_our = int(RS("price_our") * .85) + .99
				else
					price_our = int(RS("price_our") * .75) + .99
				end if
				subcategory = "Phone Accessories"
			case else
				price_our = 0
		end select
		
		qty = RS("inv_qty")
		
		if price_our > 0 then
			for iCount = 0 to 2
				AddlImages(iCount) = ""
				imgPath = "d:\productpics\AltViews\" & replace(RS("itemPic"),".jpg","-" & iCount & ".jpg")
				if fs.FileExists(imgPath) then AddlImages(iCount) = replace(replace(imgPath,"d:","http://www.wirelessemporium.com"),"\","/") 
				imgPath = "d:\productpics\AltViews\" & replace(RS("itemPic"),".jpg","-" & iCount & ".gif")
				if fs.FileExists(imgPath) then AddlImagesiCount = replace(replace(imgPath,"d:","http://www.wirelessemporium.com"),"\","/") 
				imgPath = "d:\productpics\AltViews\" & replace(RS("itemPic"),".gif","-" & iCount & ".jpg")
				if fs.FileExists(imgPath) then AddlImagesiCount = replace(replace(imgPath,"d:","http://www.wirelessemporium.com"),"\","/") 
				imgPath = "d:\productpics\AltViews\" & replace(RS("itemPic"),".gif","-" & iCount & ".gif")
				if fs.FileExists(imgPath) then AddlImagesiCount = replace(replace(imgPath,"d:","http://www.wirelessemporium.com"),"\","/") 
			next
			
			strline = chr(34) & strItemDesc & chr(34) & vbtab	'itemname
			strline = strline & qty & vbtab	'qty
			strline = strline & price_our & vbtab	'price
			strline = strline & 1 & vbtab	'condition
			strline = strline & vbtab	'mfr
			strline = strline & 1 & vbtab	'shiptime
			strline = strline & "USA 50 States" & vbtab	'shipto
			strline = strline & 0 & vbtab	'shipping_reg
			strline = strline & 6.99 & vbtab	'shipping_exp
			strline = strline & vbtab	'shipping_intl
			strline = strline & "USPS 1st-Class Mail" & vbtab	'method_reg
			strline = strline & "USPS Priority Mail" & vbtab	'method_exp
			strline = strline & vbtab	'method_intl
			strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab	'image
			strline = strline & AddlImages(0) & vbtab	'image_2
			strline = strline & AddlImages(1) & vbtab	'image_3
			strline = strline & AddlImages(2) & vbtab	'image_4
			strline = strline & vbtab	'image_5
			strline = strline & vbtab	'image_6
			strline = strline & RS("UPCCode") & vbtab	'upc
			strline = strline & RS("itemid") & vbtab	'sku
			strline = strline & chr(34) & LongDescription & chr(34) & vbtab	'description
			strline = strline & vbtab	'notes
			strline = strline & vbtab	'specs
			strline = strline & "1-Yr. Full Manufacturer Warranty" & vbtab	'warranty
			strline = strline & RS("price_Retail") & vbtab	'reg_price
			strline = strline & expDate & vbtab	'sale_expdate
			strline = strline & "Cellular" & vbtab	'category
			strline = strline & subcategory & vbtab	'subcategory
			strline = strline & vbtab	'subsub
			strline = strline & ""	'storecat
			call fWriteFile(strline)			
'			file.WriteLine strline
'			
'			loopCount = loopCount + 1
'			if loopCount >= 15000 and currentFile = filename then
'				currentFile = filename2
'				file.close()
'				set file = fs.CreateTextFile(path2)
'				file.WriteLine HeaderLine
'			elseif loopCount >= 30000 and currentFile = filename2 then
'				currentFile = filename3
'				file.close()
'				set file = fs.CreateTextFile(path3)
'				file.WriteLine HeaderLine
'			elseif loopCount >= 45000 and currentFile = filename3 then
'				currentFile = filename4
'				file.close()
'				set file = fs.CreateTextFile(path4)
'				file.WriteLine HeaderLine
'			end if
		end if
		RS.movenext
	loop
end if
RS.close
set RS = nothing
file.close()

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub


sub fWriteFile(pStrToWrite)
	nRows = nRows + 1
	
	if nRows > 5000 then
		nFileIdx = nFileIdx + 1
		nRows = 0
		file.close()
		set file = nothing
		
		path = folderName & filename & nFileIdx & ".txt"
		if fs.FileExists(path) then
			fs.DeleteFile(path)
		end if
		set file = fs.CreateTextFile(path)

		file.WriteLine headings
	end if
	
	file.WriteLine pStrToWrite
end sub


function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(formatSEO, "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function
