<%
'shipping method definitions
'https://seller.marketplace.sears.com/SellerPortal/s/schema/rest/oms/import/v3/asn.xsd
'https://seller.marketplace.sears.com/SellerPortal/s/schema/samples/rest/oms/import/v3/asn-ship.xml

set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

SDate = dateadd("d", -14, date)
EDate = dateadd("d", -1, date)
strSDate = year(SDate) & "-" & right("00" & month(SDate),2) & "-" & right("00" & day(SDate),2)
strEDate = year(EDate) & "-" & right("00" & month(EDate),2) & "-" & right("00" & day(EDate),2)
strToday = year(date) & "-" & right("00" & month(date),2) & "-" & right("00" & day(date),2)

Set xmlHttp = CreateObject("Microsoft.XMLHTTP")
myURL = "https://seller.marketplace.sears.com/SellerPortal/api/oms/purchaseorder/v3?email=kristen@wirelessemporium.com&password=cutler1986&fromdate=" & strSDate & "&todate=" & strEDate & "&status=New"
'myURL = "https://seller.marketplace.sears.com/SellerPortal/api/oms/purchaseorder/v3?email=kristen@wirelessemporium.com&password=cutler1986&fromdate=" & strSDate & "&todate=" & strEDate
'myURL = "https://seller.marketplace.sears.com/SellerPortal/api/reports/v1/processing-report/1000476301?email=kristen@wirelessemporium.com&password=cutler1986"
'myURL = "https://seller.marketplace.sears.com/SellerPortal/api/reports/v1/processing-report/1000555201?email=kristen@wirelessemporium.com&password=cutler1986"
xmlHttp.open "GET", myURL, False
xmlHttp.send

set xmlDoc = CreateObject("MSXML2.DOMDocument")
xmlDoc.async = false
xmlDoc.loadXML(xmlHttp.responseText)
nRow = -1
numOrderColumn = 5
numShippedColumn = 10
redim arrOrders(numOrderColumn, nRow)	'0:customer confirm#, 1:po-number, 2:po-date, 3:line-number, 4:itemid, 5:quantity
redim arrShippedOrders(numShippedColumn, nRow)	'0:customer confirm#, 1:po-number, 2:po-date
												'fields need to be updated ---------> 3:trackingnumber, 4:ship carrier, 5:ship method, 6:orderid, 7:scandate(shipdate), 8:line-number, 9:itemid, 10:quantity

set mynodes = xmlDoc.documentElement.selectNodes("purchase-order")
for each Node in mynodes	'loop through each order
	for i = 0 to Node.ChildNodes.Length - 1
		if "po-line" = Node.childnodes.item(i).nodeName then
			'po-line start
			nRow = nRow + 1
			redim preserve arrOrders(numOrderColumn, nRow)
			arrOrders(0, nRow)	=	Node.childNodes.item(0).text
			arrOrders(1, nRow)	=	Node.childNodes.item(2).text
			arrOrders(2, nRow)	=	Node.childNodes.item(3).text
			
			set subNodes = Node.childnodes.item(i).childNodes
			for each subNode in subNodes
				set subNodes2 = subNode.childNodes
				for each subNode2 in subNodes2
					select case subNode2.nodeName
						case "line-number" : arrOrders(3, nRow) = subNode2.text
						case "item-id" : arrOrders(4, nRow) = subNode2.text
						case "order-quantity" : arrOrders(5, nRow) = subNode2.text
					end select
				next
			next
		end if
	next
next

strCustNum = ""	'customer confirmation number on sears
for i = 0 to ubound(arrOrders,2)
	if strCustNum = "" then
		strCustNum = arrOrders(0, i)
	else
		strCustNum = strCustNum & "," & arrOrders(0, i)
	end if
next

'strCustNum = "304426637,303770672"
sql	=	"select	a.extordernumber, a.shiptype, a.trackingnum, a.orderid, convert(varchar(10), a.scandate, 20) scandate" & vbcrlf & _
		"from	we_orders a join (select distinct rString extOrderNum from dbo.tfn_StringToColumn('" & strCustNum & "', ',')) b" & vbcrlf & _
		"	on	a.extOrderNumber = b.extOrderNum" & vbcrlf & _
		"where	a.trackingnum is not null" & vbcrlf & _
		"	and	a.trackingnum <> ''" & vbcrlf & _
		"	and	a.extordertype = 8" & vbcrlf & _
		"	and	a.scandate is not null" & vbcrlf & _
		"order by a.extordernumber" & vbcrlf

Set rs = CreateObject("ADODB.Recordset")
rs.Open sql, oConn, 0, 1

nRow = -1
numOrders = 0
do until rs.eof
	numOrders = numOrders + 1
	strRows = findRows(arrOrders, 0, rs("extordernumber"))
	arrRows = split(strRows, ",")
	for i=0 to ubound(arrRows)
		nRow = nRow + 1
		redim preserve arrShippedOrders(numShippedColumn, nRow)	
		arrShippedOrders(0, nRow) = arrOrders(0, arrRows(i))
		arrShippedOrders(1, nRow) = arrOrders(1, arrRows(i))
		arrShippedOrders(2, nRow) = arrOrders(2, arrRows(i))
		arrShippedOrders(3, nRow) = rs("trackingnum")
		if instr(lcase(rs("shiptype")), "ups") > 0 then 
			arrShippedOrders(4, nRow) = "UPS"
		else
			arrShippedOrders(4, nRow) = "USPS"
		end if	
		if instr(lcase(rs("shiptype")), "express") > 0 then 
			arrShippedOrders(5, nRow) = "EXPRESS"
		elseif instr(lcase(rs("shiptype")), "priority") > 0 then
			arrShippedOrders(5, nRow) = "PRIORITY"
		else
			arrShippedOrders(5, nRow) = "GROUND"
		end if	
		arrShippedOrders(6, nRow) = cdbl(rs("orderid"))
		arrShippedOrders(7, nRow) = rs("scandate")
		arrShippedOrders(8, nRow) = arrOrders(3, arrRows(i))
		arrShippedOrders(9, nRow) = arrOrders(4, arrRows(i))
		arrShippedOrders(10, nRow) = arrOrders(5, arrRows(i))
	next
	rs.movenext
loop
curOrderID = cdbl(-1)
lastOrderID = cdbl(-1)
xmlFeed =	"<shipment-feed xmlns=""http://seller.marketplace.sears.com/oms/v3"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://seller.marketplace.sears.com/SellerPortal/s/oms/asn-v3.xsd"">" & vbcrlf 

for i = 0 to ubound(arrShippedOrders,2)
	poNumber = arrShippedOrders(1, i)
	poDate = arrShippedOrders(2, i)
	trackingNum = arrShippedOrders(3, nRow)
	shipCarrier = arrShippedOrders(4, nRow)
	shipMethod = arrShippedOrders(5, nRow)
	curOrderID = cdbl(arrShippedOrders(6,i))
	shipDate = arrShippedOrders(7, nRow)
	lineItemNumber = arrShippedOrders(8,i)
	itemid = arrShippedOrders(9,i)
	quantity = arrShippedOrders(10,i)

	if lastOrderID <> curOrderID then
		if lastOrderID <> -1 then
			xmlFeed = xmlFeed	&	"	</shipment>" & vbcrlf
		end if
	
		xmlFeed = xmlFeed	&	"	<shipment>" & vbcrlf & _
								"		<header>" & vbcrlf & _
								"			<asn-number>" & curOrderID & "</asn-number>" & vbcrlf & _
								"			<po-number>" & poNumber & "</po-number>" & vbcrlf & _
								"			<po-date>" & poDate & "</po-date>" & vbcrlf & _
								"		</header>" & vbcrlf
	end if
	
	xmlFeed = xmlFeed	&	"		<detail>" & vbcrlf & _
							"			<tracking-number>" & curOrderID & "-" & lineItemNumber & "</tracking-number>" & vbcrlf & _
							"			<ship-date>" & shipDate & "</ship-date>" & vbcrlf & _
							"			<shipping-carrier>" & shipCarrier & "</shipping-carrier>" & vbcrlf & _
							"			<shipping-method>" & shipMethod & "</shipping-method>" & vbcrlf & _
							"			<package-detail>" & vbcrlf & _
							"				<line-number>" & lineItemNumber & "</line-number>" & vbcrlf & _
							"				<item-id>" & itemid & "</item-id>" & vbcrlf & _
							"				<quantity>" & quantity & "</quantity>" & vbcrlf & _
							"			</package-detail>" & vbcrlf & _
							"		</detail>" & vbcrlf
	
	lastOrderID = curOrderID
next
if ubound(arrShippedOrders,2) >= 0 then xmlFeed = xmlFeed	&	"	</shipment>" & vbcrlf
xmlFeed = xmlFeed	&	"</shipment-feed>" & vbcrlf

'response.write "<pre>" & server.HTMLEncode(xmlFeed) & "</pre>"

Set xmlHttpSend = CreateObject("Microsoft.XMLHTTP")
myURL = "https://seller.marketplace.sears.com/SellerPortal/api/oms/asn/v3?email=kristen@wirelessemporium.com&password=cutler1986"
xmlHttpSend.open "PUT", myURL, False
xmlHttpSend.setRequestHeader "Content-Type", "application/xml"
xmlHttpSend.send xmlFeed

set xmlResult = CreateObject("MSXML2.DOMDocument")
xmlResult.async = false
xmlResult.loadXML(xmlHttpSend.responseText)

'response.write "xmlHttpSend.Status:" & xmlHttpSend.Status & "<br>"
'response.write "xmlHttpSend.statusText:" & xmlHttpSend.statusText & "<br>"
'response.write "xmlHttpSend.responseText:<pre>" & server.HTMLEncode(xmlHttpSend.responseText) & "</pre><br>"
set oNodes = xmlResult.documentElement.selectNodes("document-id")

Set xmlHttp = CreateObject("Microsoft.XMLHTTP")
myURL = "https://seller.marketplace.sears.com/SellerPortal/api/reports/v1/processing-report/" & oNodes.item(0).text & "?email=kristen@wirelessemporium.com&password=cutler1986"
xmlHttp.open "GET", myURL, False
xmlHttp.send

'response.write "xmlHttp.responseText:" & xmlHttp.responseText & "<br>"
strTo = "terry@wirelessemporium.com"
strFrom = "sales@wirelessemporium.com"
strSubject = "*** Sears trackingnumber upload result ***"
strBody = 	"- Number of orders uploaded: <b>" & numOrders & "</b><br><br>" & vbcrlf & _
			"- Message from sears: <b>" & xmlHttp.responseText & "</b>" & vbcrlf

cdosend strTo, strFrom, strSubject, strBody

function findRows(byref pArr, idx, val)
	retRows = ""
	if isArray(pArr) then
		for nRows=0 to ubound(pArr, 2)
			if cstr(val) = cstr(pArr(idx, nRows)) then
				if retRows = "" then
					retRows = nRows
				else
					retRows = retRows & "," & nRows
				end if
			end if
		next
	end if
	findRows = retRows
end function

sub cdosend(strto,strfrom,strsubject,strbody)
	on error resume next
	dim objMail
	set objMail = createobject("cdo.message")
	with objMail
		.from = strfrom
		.to = strto
		.subject = strsubject
		.htmlbody = cstr("" & strbody)
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.configuration.fields.item("urn:schemas:httpmail:sender") = "sales@wirelessemporium.com"
		.configuration.fields.update
		.send
	end with
	set objMail = nothing
	on error goto 0
end sub

'======================== example
'<shipment-feed xmlns="http://seller.marketplace.sears.com/oms/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://seller.marketplace.sears.com/SellerPortal/s/oms/asn-v3.xsd ">
'	<deprecated>2010-12-31</deprecated>
'	<shipment>
'		<header>
'			<asn-number>62917334877</asn-number>
'			<po-number>6291733</po-number>
'			<po-date>2011-06-27</po-date>
'		</header>
'		<detail>
'			<tracking-number>62917334877-1</tracking-number>
'			<ship-date>2011-07-05</ship-date>
'			<shipping-carrier>UPS</shipping-carrier>
'			<shipping-method>EXPRESS</shipping-method>
'			<package-detail>
'				<line-number>1</line-number>
'				<item-id>BSC001</item-id>
'				<quantity>1</quantity>
'			</package-detail>
'		</detail>
'		<detail>
'			<tracking-number>62917334877-2</tracking-number>
'			<ship-date>2011-07-05</ship-date>
'			<shipping-carrier>FDE</shipping-carrier>
'			<shipping-method>EXPRESS</shipping-method>
'			<package-detail>
'				<line-number>2</line-number>
'				<item-id>BSC002</item-id>
'				<quantity>2</quantity>
'			</package-detail>
'		</detail>
'	</shipment>
'	<shipment>
'		<header>
'			<asn-number>62917340872</asn-number>
'			<po-number>6291734</po-number>
'			<po-date>2011-06-27</po-date>
'		</header>
'		<detail>
'			<tracking-number>62917340872-1</tracking-number>
'			<ship-date>2011-07-05</ship-date>
'			<shipping-carrier>UPS</shipping-carrier>
'			<shipping-method>EXPRESS</shipping-method>
'			<package-detail>
'				<line-number>1</line-number>
'				<item-id>BSC001</item-id>
'				<quantity>1</quantity>
'			</package-detail>
'		</detail>
'	</shipment>
'</shipment-feed>
%>
