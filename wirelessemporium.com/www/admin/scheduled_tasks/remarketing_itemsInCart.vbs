set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

isTestMode = false
strTemplate = ""
strTo = ""

sql =	"exec sp_remarketingEmail"
set rs = oConn.execute(sql)

lapCount 	= 0
strItems 	= ""
do until rs.eof
	lapCount = lapCount + 1
	mySessionID = rs("sessionID")
	siteID 		= rs("siteID")
	email 		= rs("email")
	cartItemHTML = ""
	otherItemsHTML = ""
	additionalItems = ""
	itemCount = 0
	
	do while email = rs("email")
		itemCount = itemCount + 1
		itemID = rs("itemID")
		itemDesc = rs("itemDesc")
		itemDesc_CO = rs("itemDesc_CO")
		price_retail = rs("price_retail")
		price_we = rs("price_our")
		price_co = rs("price_co")
		itemPic = rs("itemPic")
		itemPic_CO = rs("itemPic_CO")
		
		if siteID = 0 then
			useDesc = itemDesc
			usePrice = price_we
			usePic = itemPic
			siteName = "WE"
		elseif siteID = 2 then
			useDesc = itemDesc_CO
			usePrice = price_co
			usePic = itemPic_CO
			siteName = "CO"
		end if
		
		if not isnumeric(price_retail) or len(price_retail) < 1 then price_retail = 0
		if not isnumeric(usePrice) or len(usePrice) < 1 then usePrice = 0
		
		if siteID = 0 then
			if itemCount = 1 then
				primaryItemHTML =	"<table border=""0"" cellpadding=""0"" cellspacing=""0"">" &_
										"<tr>" &_
											"<td width=""200"" style=""background-color:#FFF; border:1px solid #CCC;""><a href=""http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(itemDesc) & "?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """><img src=""http://www.wirelessemporium.com/productpics/big/" & usePic & """ border=""0"" width=""200"" height=""200"" /></a></td>" &_
										"</tr>" &_
										"<tr><td width=""200"" style=""padding-top:15px;""><a style=""font-weight:bold; font-size:14px; text-decoration:none; color:#333;"" href=""http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(itemDesc) & "?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """>" & useDesc & "</a></td></tr>" &_
										"<tr>" &_
											"<td width=""200"" style=""padding-top:10px;"">" &_
												"<table border=""0"" cellpadding=""0"" cellspacing=""0"">" &_
													"<tr>" &_
														"<td style=""color:#999; text-decoration:line-through; font-size:14px;"">" & formatCurrency(price_retail,2) & "</td>" &_
														"<td style=""color:#ff6600; font-weight:bold; font-size:14px; padding-left:15px;"">" & formatCurrency(usePrice,2) & "</td>" &_
													"</tr>" &_
												"</table>" &_
											"</td>" &_
										"</tr>" &_
										"<tr>" &_
											"<td width=""200"" style=""padding-top:10px;"">" &_
												"<table border=""0"" cellpadding=""0"" cellspacing=""0"">" &_
													"<tr>" &_
														"<td style=""color:#999; font-size:11px; font-weight:bold;"">FREE SHIPPING</td>" &_
														"<td style=""padding-left:10px;""><a style=""color:#0066cc; font-size:11px;"" href=""http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(itemDesc) & "?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """>See Details</a></td>" &_
													"</tr>" &_
												"</table>" &_
											"</td>" &_
										"</tr>" &_
									"</table>"
			else
				additionalItems =	"<tr>" &_
										"<td style=""padding:30px 0px 0px 30px; color:#333; font-size:24px;"">Additional Items In Your Cart:</td>" &_
									"</tr>"
				cartItemHTML = 	cartItemHTML &	"<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""450"" style=""padding-top:20px;"">" &_
													"<tr>" &_
														"<td><a href=""http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(itemDesc) & "?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """><img src=""http://www.wirelessemporium.com/productpics/big/" & usePic & """ border=""0"" width=""140"" height=""140"" /></a></td>" &_
														"<td valign=""top"" style=""padding-left:20px;"">" &_
															"<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">" &_
																"<tr>" &_
																	"<td style=""padding-top:15px;""><a style=""font-weight:bold; font-size:14px; color:#333; text-decoration:none;"" href=""http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(itemDesc) & "?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """>" & useDesc & "</a></td>" &_
																"</tr>" &_
																"<tr>" &_
																	"<td width=""200"" style=""padding-top:10px;"">" &_
																		"<table border=""0"" cellpadding=""0"" cellspacing=""0"">" &_
																			"<tr>" &_
																				"<td style=""color:#999; text-decoration:line-through; font-size:14px;"">" & formatCurrency(price_retail,2) & "</td>" &_
																				"<td style=""color:#ff6600; font-weight:bold; font-size:14px; padding-left:15px;"">" & formatCurrency(usePrice,2) & "</td>" &_
																			"</tr>" &_
																		"</table>" &_
																	"</td>" &_
																"</tr>" &_
																"<tr>" &_
																	"<td width=""200"" style=""padding-top:10px;"">" &_
																		"<table border=""0"" cellpadding=""0"" cellspacing=""0"">" &_
																			"<tr>" &_
																				"<td style=""color:#999; font-size:11px; font-weight:bold;"">FREE SHIPPING</td>" &_
																				"<td style=""padding-left:10px;""><a style=""color:#0066cc; font-size:11px;"" href=""http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(itemDesc) & "?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """>See Details</a></td>" &_
																			"</tr>" &_
																		"</table>" &_
																	"</td>" &_
																"</tr>" &_
															"</table>" &_
														"</td>" &_
													"</tr>" &_
												"</table>"
			end if
		elseif siteID = 2 then
			cartItemHTML = cartItemHTML &	"<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""618"" align=""center"" style=""border-top:1px solid #CCC; padding:10px 0px"">" &_
												"<tr>" &_
													"<td width=""160""><a href=""http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(useDesc) & ".html?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """ style=""text-decoration:underline; color:#336688; font-weight:bold;""><img src=""http://www.cellularoutfitter.com/productPics/big/" & usePic & """ border=""0"" width=""160"" height=""160""></a></td>" &_
													"<td width=""225"" style=""border-right:1px solid #CCC; padding:0px 20px;"">" &_
														"<table border=""0"" cellpadding=""0"" cellspacing=""0"" style=""font-size:11px;"" width=""225"">" &_
															"<tr><td style=""font-weight:bold;""><a href=""http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(useDesc) & ".html?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """ style=""text-decoration:none; color:#000000; font-weight:bold;"">" & useDesc & "</a></td></tr>" &_
															"<tr><td style=""padding-top:10px;""><a href=""http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(useDesc) & ".html?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """ style=""text-decoration:underline; color:#336688; font-weight:bold;"">Read More.</a></td></tr>" &_
														"</table>" &_
													"</td>" &_
													"<td width=""200"" style=""padding:10px 0px 0px 20px;"" valign=""top"">" &_
														"<table border=""0"" cellpadding=""0"" cellspacing=""0"">" &_
															"<tr><td style=""text-decoration:line-through; color:#666; font-size:12px;"">Was: " & formatCurrency(price_retail,2) & "</td></tr>" &_
															"<tr><td style=""color:#cc0000; font-weight:bold; font-size:28px;"">" & formatCurrency(usePrice,2) & "</td></tr>" &_
															"<tr><td style=""color:#333; font-size:12px; font-weight:bold;"">Save " & formatCurrency((price_retail-usePrice),2) & " (60%)</td></tr>" &_
															"<tr><td style=""padding-top:15px;""><a href=""http://www.cellularoutfitter.com/cart/basket.html?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """><img src=""http://www.cellularoutfitter.com/images/remarketing/email/buyItNow.gif"" border=""0"" width=""169"" height=""35"" /></a></td></tr>" &_
														"</table>" &_
													"</td>" &_
												"</tr>" &_
											"</table>"
		end if
		rs.movenext
		if rs.EOF then exit do
	loop
	
	sql = "exec sp_alsoPurchased " & siteID & "," & mySessionID
	set otherItemsRS = oConn.execute(sql)
	
	otherItemLap = 0
	do while not otherItemsRS.EOF
		itemID = otherItemsRS("itemID")
		if siteID = 0 then
			maxLap = 4
			itemPic = otherItemsRS("itemPic")
			itemDesc = otherItemsRS("itemDesc")
			itemPrice = otherItemsRS("price_our")
			if not isnumeric(itemPrice) or len(itemPrice) < 1 then itemPrice = 0
			itemRetail = otherItemsRS("price_retail")
			if not isnumeric(itemRetail) or len(itemRetail) < 1 then itemRetail = 0
			if len(itemDesc) > 30 then useDesc = left(itemDesc,30) & "..."
			otherItemsHTML = otherItemsHTML &	"<td style=""padding-left:35px;"">" &_
													"<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""130"">" &_
														"<tr><td><a href=""http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(itemDesc) & "?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """><img src=""http://www.wirelessemporium.com/productpics/big/" & itemPic & """ border=""0"" width=""130"" height=""130"" /></a></td></tr>" &_
														"<tr><td style=""padding-top:10px;""><a style=""font-weight:bold; font-size:12px; line-height:20px; color:#333; text-decoration:none;"" href=""http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(itemDesc) & "?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """>" & useDesc & "</a></td></tr>" &_
														"<tr><td style=""text-decoration:line-through; padding-top:6px; color:#999;""><a style=""font-size:13px; color:#999; text-decoration:none;"" href=""http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(itemDesc) & "?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """>" & formatCurrency(itemRetail,2) & "</a></td></tr>" &_
														"<tr><td style=""padding-top:6px;""><a style=""font-size:22px; font-weight:bold; color:#ff6600; text-decoration:none;"" href=""http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(itemDesc) & "?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """>" & formatCurrency(itemPrice,2) & "</a></td></tr>" &_
														"<tr><td style=""padding-top:15px;""><a href=""http://www.wirelessemporium.com/cart/item_add?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & "&prodid=" & itemID & "&qty=1""><img src=""http://www.wirelessemporium.com/images/remarketing/email/addToCart.gif"" border=""0"" width=""88"" height=""21"" /></a></td></tr>" &_
													"</table>" &_
												"</td>"
		elseif siteID = 2 then
			maxLap = 3
			itemPic = otherItemsRS("itemPic_CO")
			itemDesc = otherItemsRS("itemDesc_CO")
			itemPrice = otherItemsRS("price_co")
			if len(itemDesc) > 60 then useDesc = left(itemDesc,60) & "..."
			otherItemsHTML = otherItemsHTML &	"<td>" &_
													"<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""150"">" &_
														"<tr><td align=""center""><a href=""http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".html?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """><img src=""http://www.cellularoutfitter.com/productPics/thumb/" & itemPic & """ border=""0"" width=""100"" height=""100"" /></a></td></tr>" &_
														"<tr><td style=""padding-top:5px;""><a style=""font-size:12px; color:#333; text-decoration:none;"" href=""http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".html?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """>" & useDesc & "</a></td></tr>" &_
														"<tr><td style=""padding-top:5px;""><a style=""font-size:18px; color:#cc0000; font-weight:bold; text-decoration:none;"" href=""http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".html?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & """>" & formatCurrency(itemPrice,2) & "</a></td></tr>" &_
														"<tr>" &_
															"<td style=""padding-top:5px;"">" &_
																"<a href=""http://www.cellularoutfitter.com/cart/item_add.html?utm_source=" & siteName & "&utm_medium=email2&utm_campaign=emailRM&csid=" & mySessionID & "&prodid=" & itemID & "&qty=1""><img src=""http://www.cellularoutfitter.com/images/remarketing/email/addToCart.gif"" border=""0"" width=""92"" height=""21"" /></a>" &_
															"</td>" &_
														"</tr>" &_
													"</table>" &_
												"</td>"
		end if
		otherItemLap = otherItemLap + 1
		if otherItemLap = maxLap then exit do
		otherItemsRS.movenext
	loop
	
	if siteID = 0 then
		strFrom = "WirelessEmporium.com<sales@wirelessemporium.com>"
		strSubject = "Still Shopping?"
		baseHref = "http://www.wirelessemporium.com"
		strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\admin\scheduled_tasks\remarketing-ItemsInCart-template-WE.htm")
	elseif siteID = 2 then
		strFrom = "CellularOutfitter.com<sales@cellularoutfitter.com>"
		strSubject = "Did You Forget Something?"
		baseHref = "http://www.cellularoutfitter.com"
		strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\admin\scheduled_tasks\remarketing-ItemsInCart-template-CO.htm")
	end if

	if strTo <> email then
		strTo = email
		strTemplate = replace(strTemplate, "!##PRIMARY_ITEM##!", strItem)
		strTemplate = replace(strTemplate, "!##ALSO_PURCHASED##!", strItemsAlsoPurchased)
		strTemplate = replace(strTemplate, "!##EmailTitle##!", strSubject)
		strTemplate = replace(strTemplate, "!##baseHref##!", baseHref)
		strTemplate = replace(strTemplate, "!##CurrentCartItems##!", cartItemHTML)
		strTemplate = replace(strTemplate, "!##AlsoPurchased##!", otherItemsHTML)
		strTemplate = replace(strTemplate, "!##SessionID##!", mySessionID)
		strTemplate = replace(strTemplate, "!##Email##!", email)
		strTemplate = replace(strTemplate, "!##PrimaryProduct##!", primaryItemHTML)
		strTemplate = replace(strTemplate, "!##AdditionalItems##!", additionalItems)

		set objEmail = CreateObject("CDO.Message")
		with objEmail
			.From = strFrom
			
			if isTestMode then
				'.To = "doldding82@gmail.com"
				'.To = "jon@wirelessemporium.com"
				.To = "edwin@wirelessemporium.com;sherwood@wirelessemporium.com;jon@wirelessemporium.com;doldding82@gmail.com"
				.Bcc = "doldding82@hotmail.com;doldding82@yahoo.com"					
			else
				.To = strTo
				.Bcc = "ruben@wirelessemporium.com,matt@wirelessemporium.com,jon@wirelessemporium.com"
			end if
			
			on error resume next
				.Subject = strSubject
				.HTMLBody = strTemplate
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
				.Configuration.Fields.Update
				.Send
			on error goto 0
		end with
	
		strTemplate = dup_strTemplate	'refresh the template for an another e-mail.
	end if
	
	if isTestMode then
		exit do
	else
		sql = "exec sp_remarketingEmailUpdate " & mySessionID & "," & siteID
		oConn.execute(sql)
	end if
	'if lapCount = 1 then exit do
loop

rs.close
set rs = nothing

function getRatingAvgStar(rating)
	dim nRating
	dim strRatingImg
	nRating = cdbl(rating)
	strRatingImg = 	"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />"
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""	
		for i=1 to 5
			if nRating => i then
				strRatingImg = strRatingImg & "<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_full.jpg"" border=""0"" width=""30"" height=""29"" /> "
			elseif nRating => ((i - 1) + .1) then
				strRatingImg = strRatingImg & "<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_half.jpg"" border=""0"" width=""30"" height=""29"" /> "				
			else
				strRatingImg = strRatingImg & "<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" /> "
			end if
		next		
	end if
	
	getRatingAvgStar = strRatingImg
end function


Function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
End Function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
