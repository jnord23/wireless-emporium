set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim itemID(9), itemDesc(9), itemPic(9), price_Our(9), a
a = 0
SQL = "SELECT itemID, itemDesc, itemPic, price_Our FROM we_items WHERE itemID IN (27858,29159,34863,35330,37247,38059,39053,49801,51170) ORDER BY itemID"
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
do until RS.eof
	a = a + 1
	itemID(a) = RS("itemID")
	itemDesc(a) = RS("itemDesc")
	itemPic(a) = RS("itemPic")
	price_Our(a) = RS("price_Our")
	RS.movenext
loop

dim fname, strTo, strFrom, strSubject, strBody
strFrom = "sales@wirelessemporium.com"
strSubject = "Wireless Emporium New Product Offering"

SQL = "SELECT A.orderID,A.orderdatetime,B.email,C.fname FROM (we_Orders A INNER JOIN we_Accounts B ON A.accountID=B.accountID)"
SQL = SQL & " INNER JOIN we_emailopt C ON B.email=C.email"
SQL = SQL & " WHERE A.approved = 1 AND A.orderdatetime >= '" & dateAdd("d",-365,date) & "' AND A.orderdatetime < '" & dateAdd("d",-364,date) & "'"
SQL = SQL & " AND A.store = 0"
SQL = SQL & " AND (b.email not like '%@marketplace.amazon.com' and b.email not like '%@seller.sears.com') and (a.extordertype not in (9) or a.extordertype is null) "
SQL = SQL & " and c.email not in (select distinct email from we_emailopt_out) "
SQL = SQL & " ORDER BY A.orderID"
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
do until RS.eof
	fname = Ucase(left(RS("fname"),1)) & Lcase(right(RS("fname"),len(RS("fname"))-1))
	strTo = RS("email")
	strBody = "<html><body>" & vbcrlf
	strBody = strBody & "<table width=""602"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0""><tr>" & vbcrlf
	strBody = strBody & "<td width=""602"" align=""center"" valign=""top"" bgcolor=""#FFFFFF""><a href=""http://www.wirelessemporium.com?refer=YRONE""><img src=""http://www.wirelessemporium.com/images/WE_logo.jpg"" width=""450"" height=""69"" border=""0""></a></td>" & vbcrlf
	strBody = strBody & "</tr><tr>" & vbcrlf
	strBody = strBody & "<td width=""602"" align=""center"" valign=""bottom"" bgcolor=""#FFFFFF""><img src=""http://www.wirelessemporium.com/images/email/Email_unlocked_banner.jpg"" width=""602"" height=""72""></td>" & vbcrlf
	strBody = strBody & "</tr><tr>" & vbcrlf
	strBody = strBody & "<td width=""602"" align=""center"" bgcolor=""#AEAFB1"">" & vbcrlf
	strBody = strBody & "<table width=""600"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0""><tr><td width=""600"" align=""left"" valign=""top"" bgcolor=""#FFFFFF""><table width=""600"" border=""0"" cellspacing=""0"" cellpadding=""5""><tr>" & vbcrlf
	strBody = strBody & "<td><p><font face=""Arial, Helvetica, sans-serif"" size=""2"">Dear " & fname & ",</font></p>" & vbcrlf
	strBody = strBody & "<p align=""center""><font face=""Arial, Helvetica, sans-serif"" size=""2"">Thank you for being a loyal Wireless Emporium customer. We are reaching out to our valued customers with exciting news! Wireless Emporium is now offering Unlocked and Replacement Cell Phones to be used with your current service provider (no new contracts or renewals). We have over 200 models to choose from!</font></p></td>" & vbcrlf
	strBody = strBody & "</tr><tr>" & vbcrlf
	strBody = strBody & "<td width=""600"" align=""center"" bgcolor=""#FF4D11""><font face=""Arial, Helvetica, sans-serif"" size=""3"" color=""#FFFFFF""><b><i>Popular Models Available TODAY!</i></b></font></td>" & vbcrlf
	strBody = strBody & "</tr>" & vbcrlf
	for a = 1 to 3
		strBody = strBody & "<tr><td><table border=""0"" cellpadding=""0"" cellspacing=""20""><tr>" & vbcrlf
		for b = 1 to 3
			strBody = strBody & "<td width=""200"" align=""center"" valign=""bottom""><font face=""Arial, Helvetica, sans-serif"" size=""2"">" & vbcrlf
			strBody = strBody & "<a href=""http://www.wirelessemporium.com/p-" & itemid(a*b) & "-" & formatSEO(itemDesc(a*b)) & "?refer=YRONE""><img src=""http://www.wirelessemporium.com/productpics/big/" & itemPic(a*b) & """ width=""100"" height=""100"" border=""0"" alt=""" & itemDesc(a*b) & """></a><br>" & vbcrlf
			strBody = strBody & "<a href=""http://www.wirelessemporium.com/p-" & itemid(a*b) & "-" & formatSEO(itemDesc(a*b)) & "?refer=YRONE"">" & itemDesc(a*b) & "</a>" & vbcrlf
			strBody = strBody & "<br>" & formatCurrency(price_Our(a*b)) & "" & vbcrlf
			strBody = strBody & "<br><a href=""http://www.wirelessemporium.com/p-" & itemid(a*b) & "-" & formatSEO(itemDesc(a*b)) & "?refer=YRONE""><img src=""http://www.wirelessemporium.com/images/newimages/buy_now1.jpg"" width=""82"" height=""27"" border=""0"" alt=""Buy Now!""></a>" & vbcrlf
			strBody = strBody & "</font></td>" & vbcrlf
		next
		strBody = strBody & "</tr></table></td></tr>" & vbcrlf
	next
	strBody = strBody & "<tr><td align=""left"" valign=""top""><table width=""590"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0""><tr><td align=""left"" valign=""top"" bgcolor=""#FFFFFF"">" & vbcrlf
	strBody = strBody & "<p><font face=""Arial, Helvetica, sans-serif"" size=""2"">As with every purchase, your satisfaction is always 100% guaranteed. So don't hesitate to take advantage of this special offer today!</font></p>" & vbcrlf
	strBody = strBody & "<p><font face=""Arial, Helvetica, sans-serif"" size=""2"">We want to thank you for your loyalty to <a href=""http://www.wirelessemporium.com?refer=YRONE"">WirelessEmporium.com</a>! As <b><i>The Leader</i></b> in wireless accessories online, it is our goal to make sure that your purchase experiences are always above and beyond your expectations. Please visit us today and check out our new line of Cell Phones and Accessories!</font></p>" & vbcrlf
	strBody = strBody & "<p><font face=""Arial, Helvetica, sans-serif"" size=""2"">Sincerely,</font></p>" & vbcrlf
	strBody = strBody & "<p><font face=""Arial, Helvetica, sans-serif"" size=""2""><i>The Sales Team @ WirelessEmporium.com</i><br><a href=""mailto:sales@wirelessemporium.com"">sales@wirelessemporium.com</a><br><a href=""http://www.wirelessemporium.com/"">www.WirelessEmporium.com</a><br>(888) 725-7575</font></p>" & vbcrlf
	strBody = strBody & "<hr>" & vbcrlf
	strBody = strBody & "<p align=""center""><a href=""http://www.squaretrade.com/cellphone?ccode=bs_war_vc_052""><img src=""http://www.wirelessemporium.com/images/ad_banners/Squaretrade2.png"" width=""468"" height=""60"" border=""0""></a></p>" & vbcrlf
	strBody = strBody & "<p><font face=""Arial, Helvetica, sans-serif"" size=""-2"">Wireless Emporium, Inc. takes your privacy very seriously and is a certified licensee of the TRUSTe?Privacy Seal Program. This email correspondence has been sent to you as you have indicated you would like to receive special, exclusive offers from WirelessEmporium.com.<br><br>" & vbcrlf
	strBody = strBody & "If this message has been sent in error, or you would no longer like to receive any more periodic offers from WirelessEmporium.com, you may <a href=""http://www.wirelessemporium.com/unsubscribe?strEmail=" & strTo & """>CLICK HERE</a> to unsubscribe.</font></p>" & vbcrlf
	strBody = strBody & "</td></tr></table></td></tr>" & vbcrlf
	strBody = strBody & "</table></td></tr></table></td>" & vbcrlf
	strBody = strBody & "</tr><tr>" & vbcrlf
	strBody = strBody & "<td align=""center"" bgcolor=""#FFFFFF""><img src=""http://www.wirelessemporium.com/images/newimages/email_bottom_closer.jpg"" width=""602"" height=""20""></td>" & vbcrlf
	strBody = strBody & "</tr>" & vbcrlf
	strBody = strBody & "</table>" & vbcrlf
	strBody = strBody & "</body>" & vbcrlf
	strBody = strBody & "</html>" & vbcrlf
	Set objErrMail = CreateObject("CDO.Message")
	With objErrMail
		.From = strFrom
		.To = strTo
		'.Bcc = "webmaster@wirelessemporium.com"
		.Subject = strSubject
		.HTMLBody = strBody
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Update
		.Send
	End With
	RS.movenext
loop

RS.close
set RS = nothing

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function
