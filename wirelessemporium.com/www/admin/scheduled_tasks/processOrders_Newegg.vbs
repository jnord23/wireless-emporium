set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()
set fs = CreateObject("Scripting.FileSystemObject")

call initConnection(oFTP,"ftp03.newegg.com","A2SE","bears1986")

success = oFTP.ChangeRemoteDir("/Outbound/OrderList")
if (success <> 1) then
'    Response.Write oFTP.LastErrorText & "<br>"
end if

set xmlDoc = CreateObject("MSXML2.DOMDocument.3.0")
xmlDoc.async = false

datetime = now-1
destFiles = "OrderList_" & Year(datetime) & right("00" & Month(datetime), 2) & right("00" & Day(datetime), 2) & "_*.XML"
'destFiles = "OrderList_" & Year(now) & right("00" & Month(now), 2) & "23_*.XML"

downloadPath = "C:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\newegg\download"
nDownloaded = oFTP.MGetFiles(destFiles,downloadPath)
If nDownloaded > 0 Then
	set oFolder = fs.GetFolder(downloadPath)
	for each o in oFolder.Files
		xmlDoc.load (downloadPath & "\" & o.Name)

		for each xmlOrders in xmlDoc.documentElement.selectNodes("Message/OrderReport")
			set xmlOrder = xmlOrders.selectSingleNode("Order")
			orderNumber = xmlOrder.selectSingleNode("OrderNumber").text
					
			sql = "select extOrderNumber from we_orders where extordernumber = '" & orderNumber & "'"
			set rsExists = oConn.execute(sql)
			
			if rsExists.eof then
				orderdatetime = xmlOrder.selectSingleNode("OrderDateTime").text
				orderGrandTotal = xmlOrder.selectSingleNode("OrderTotal").text
				orderShippingTotal = xmlOrder.selectSingleNode("ShipTotal").text
				orderSalesTax = 0
	
				sFname = xmlOrder.selectSingleNode("ShipTo/FirstName").text
				sLname = xmlOrder.selectSingleNode("ShipTo/LastName").text
				sAddress1 = xmlOrder.selectSingleNode("ShipTo/Address1").text
				sAddress2 = xmlOrder.selectSingleNode("ShipTo/Address2").text
				address = sAddress1 & " " & sAddress2
				sCity = xmlOrder.selectSingleNode("ShipTo/City").text
				sState = xmlOrder.selectSingleNode("ShipTo/StateCode").text
				sZip = xmlOrder.selectSingleNode("ShipTo/Zip").text
				sCountryCode = xmlOrder.selectSingleNode("ShipTo/CountryCode").text
				sShippingMethod = xmlOrder.selectSingleNode("ShipTo/ShippingMethod").text
				sPhone = xmlOrder.selectSingleNode("ShipTo/Phone").text
				sCompany = xmlOrder.selectSingleNode("ShipTo/Company").text
				sEmail = xmlOrder.selectSingleNode("ShipTo/Email").text

				sql = "sp_InsertAccountInfo '" & SQLquote(sFname) & "','" & SQLquote(sLname) & "','" & SQLquote(address) & "','','" & SQLquote(sCity) & "','" & SQLquote(sState) & "','" & sZip & "','','" & SQLquote(address) & "','','" & SQLquote(sCity) & "','" & SQLquote(sState) & "','" & sZip & "','','" & SQLquote(sPhone) & "','" & SQLquote(sEmail) & "','','','" & now & "'"
				nAccountId = oConn.execute(SQL).fields(0).value	
				
				sql = "sp_InsertOrder3 0, '" & nAccountId & "','" & FormatNumber(orderGrandTotal,2) & "','0.0'," & FormatNumber(orderSalesTax,2) & ",'" & FormatNumber(orderGrandTotal,2) & "','First Class',null,0,'" & now & "'"
				nOrderID = oConn.execute(SQL).fields(0).value
			
				sql = "UPDATE we_orders SET approved = 1, extOrderType='10', extOrderNumber='" & orderNumber & "' WHERE orderid='" & nOrderID & "'"
				oConn.execute SQL
	
				for each xmlItems in xmlOrder.selectNodes("ItemList")
					set xmlItem = xmlItems.selectSingleNode("Item")
					neItemID = xmlItem.selectSingleNode("NeweggItemNumber").text
					upc = xmlItem.selectSingleNode("UPC").text
					qty = xmlItem.selectSingleNode("Quantity").text
					price_our = xmlItem.selectSingleNode("UnitPrice").text
					MPN = xmlItem.selectSingleNode("ManufacturerPartsNumber").text
					seMPN = xmlItem.selectSingleNode("SellerPartNumber").text
					itemid = mid(seMPN, InStrRev(seMPN, "-")+1)
					partnumber = left(seMPN, InStrRev(seMPN, "-")-1)
'					response.write "seMPN:" & seMPN & "<br>"
					
					sql = "sp_InsertOrderDetail2 '" & nOrderID & "','" & itemid & "','" & qty & "', '" & neItemID & "'"
'					response.write sql & "<br>"
					oConn.execute(SQL)
					
					sql = "UPDATE we_items SET numberOfSales = numberOfSales + " & qty & " WHERE itemID = '" & itemid & "'"
'					response.write sql & "<br>"
					oConn.execute(SQL)
					
					call UpdateInvQty(partnumber, qty)
				next			
			end if
		next
	next
End If

sub initConnection(byref ftp,hostname,username,password)
	success = 0

	set ftp = CreateObject("Chilkat.Ftp2")
	success = ftp.UnlockComponent("WIRELEFTP_GQ2TEoLE9Bnh")

	ftp.Hostname = hostname
	ftp.Username = username
	ftp.Password = password

	success = ftp.Connect()
'	if success <> 1 then 
'		response.write "<br>ftp connection error - " & ftp.LastErrorText & "<br>"
'		response.end
'	end if
end sub

function SQLquote(strValue)
	if not isNull(strValue) and strValue <> "" then
		SQLquote = trim(replace(strValue,"'","''"))
		SQLquote = trim(replace(SQLquote,chr(34),"''"))
	else
		SQLquote = strValue
	end if
end function

sub UpdateInvQty(nPartNumber, nProdQuantity)
	' Updates inv_qty for the Master Part (item with the same Part Number as the item purchased which does not have -1 inv_qty)
	' If the inv_qty would be adjusted to less than or equal to zero, then all items with the same Part Number as the item purchased are set to zero
	SQL = "SELECT itemID,typeID,vendor,inv_qty FROM we_items WHERE PartNumber = '" & nPartNumber & "' AND master = 1"
	set oRsItemInfo2 = CreateObject("ADODB.Recordset")
	oRsItemInfo2.open SQL, oConn, 0, 1
	do until oRsItemInfo2.eof
		curQty = oRsItemInfo2("inv_qty")
		if curQty - nProdQuantity > 0 then
			On Error Resume Next
			sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & oRsItemInfo2("itemID") & "," & curQty & "," & nProdQuantity & "," & nOrderID & ",0,'Newegg Customer Order','" & now & "')"
			oConn.execute(sql)
			On Error Goto 0
			
			decreaseSQL = "UPDATE we_items SET inv_qty = inv_qty - " & nProdQuantity & " WHERE itemID = '" & oRsItemInfo2("itemID") & "'"
			oConn.execute(decreaseSQL)
		else
			On Error Resume Next
			sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & oRsItemInfo2("itemID") & "," & curQty & "," & nProdQuantity & "," & nOrderID & ",0,'Newegg Customer Order *Out of Stock*','" & now & "')"
			oConn.execute(sql)
			On Error Goto 0
			
			decreaseSQL = "UPDATE we_items SET inv_qty = 0 WHERE PartNumber = '" & nPartNumber & "'"
			if oRsItemInfo2("typeID") = 2 then decreaseSQL = decreaseSQL & " AND inv_qty > -1"
			if oRsItemInfo2("typeID") <> 3 and oRsItemInfo2("vendor") <> "CM" and oRsItemInfo2("vendor") <> "DS" and oRsItemInfo2("vendor") <> "MLD" then
				' Send zero-inventory e-mail
				'outOfStockEmail nPartNumber,curQty,nProdQuantity
			end if
			oConn.execute(decreaseSQL)
		end if
		oRsItemInfo2.movenext
	loop
	oRsItemInfo2.close
	set oRsItemInfo2 = nothing
end sub

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
