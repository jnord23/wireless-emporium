<%
set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

isTestMode = false
strTemplate = ""
strFrom = "sales@wirelessemporium.com"
strSubject = "Help Your Fellow Customers - Write a Review"
strTo = ""

sql	=	"select	a.orderid, a.ordergrandtotal, a.orderdatetime, a.thub_posted_date" & vbcrlf & _
		"	,	b.email, c.fname, e.itemid, e.itemdesc, e.itempic" & vbcrlf & _
		"	,	isnull(sum(cast(f.approved as int)), 0) cnt, isnull(avg(f.rating), 0) avgRating" & vbcrlf & _	
		"	,	isnull(a.extOrderType, 0) extOrderType" & vbcrlf & _
		"from	we_orders a join we_accounts b" & vbcrlf & _
		"	on	a.accountid = b.accountid join we_emailopt c" & vbcrlf & _
		"	on	b.email = c.email join we_orderdetails d" & vbcrlf & _
		"	on	a.orderid = d.orderid join we_items e" & vbcrlf & _
		"	on	d.itemid = e.itemid left outer join we_reviews f" & vbcrlf & _
		"	on	e.partnumber = f.partnumbers and f.approved = 1" & vbcrlf & _		
		"where	a.approved = 1 " & vbcrlf & _
		"	and	(a.cancelled = 0 or a.cancelled is null)" & vbcrlf & _
		"	and a.thub_posted_date >= convert(varchar(10), dateadd(day, -14, getdate()), 20)" & vbcrlf & _
		"	and	a.thub_posted_date < convert(varchar(10), dateadd(day, -13, getdate()), 20)" & vbcrlf & _
		"	and	a.scandate is not null" & vbcrlf & _
		"	and	(a.trackingNum is not null or a.trackingNum <> '')" & vbcrlf & _
		"	and	(d.reship is null or d.reship = 0)" & vbcrlf & _
		"	and	a.store = 0" & vbcrlf & _
		"	and (b.email not like '%@marketplace.amazon.com' and b.email not like '%@seller.sears.com')	" & vbcrlf & _		
		"	and (a.extordertype not in (9) or a.extordertype is null)	" & vbcrlf & _
		"group by a.orderid, a.ordergrandtotal, a.orderdatetime, a.thub_posted_date, b.email, c.fname, e.itemid, e.itemdesc, e.itempic, isnull(a.extOrderType, 0)" & vbcrlf & _		
		"order by b.email, a.orderid, e.itemid" & vbcrlf
'response.write "<pre>" & sql & "</pre>"
'response.end
set rs = oConn.execute(sql)

strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\admin\scheduled_tasks\14-day-product-review-email-template.html")
dup_strTemplate = strTemplate


lapCount 	= 0
noSkip 		= false
strItems 	= ""
strCustName = ""
do until rs.eof
	lapCount = lapCount + 1
	if lapCount > 3 then noSkip = false

	if strTo <> rs("email") then
		lapCount = 1
		noSkip = true
		
		if "" <> strItems then
			strItems = strItems & "</table>" & vbcrlf
			strTemplate = replace(strTemplate, "!##CUSTOMER_NAME##!", strCustName)
			strTemplate = replace(strTemplate, "!##ITEM_TABLE##!", strItems)

			'send an e-mail
			response.write strTo & "<br>"
'			response.write strTemplate & "<br><br>"
'			response.Flush()
'			set objEmail = CreateObject("CDO.Message")
'			with objEmail
'				.From = strFrom
'
'				if isTestMode then 				
'					.To = "doldding82@gmail.com"
'					.Bcc = "doldding82@hotmail.com;doldding82@yahoo.com"					
'				else
'					.To = strTo
''					.Bcc = "doldding82@gmail.com"
'				end if
'
'				.Subject = strSubject
'				.HTMLBody = strTemplate
'				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
'				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
'				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
'				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
'				.Configuration.Fields.Update
'				.Send
'			end with
		
			strTemplate = dup_strTemplate	'refresh the template for an another e-mail.
		end if
		
		strItems = "<table width=""710"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; color:#8D8D8D; text-decoration: none;"">" & vbcrlf
	end if

	strCustName = rs("fname")
	strTo 		= rs("email")	

	if instr(rs("email"), "@") > 0 and len(strCustName) > 0 and noSkip then
		strItems = strItems & "	<tr><td colspan=""2"" width=""710"">&nbsp;</td></tr>" & vbcrlf
		strItems = strItems & "	<tr>" & vbcrlf
		strItems = strItems & "		<td width=""260"" style=""padding:0px 20px 0px 20px;""><img src=""http://www.wirelessemporium.com/productpics/big/" & rs("itempic") & """ border=""0"" /></td>" & vbcrlf
		strItems = strItems & "		<td width=""406"" style=""border:2px solid #ccc; padding:15px;"">" & vbcrlf
		strItems = strItems & "			<table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; color:#8D8D8D; text-decoration: none;"">" & vbcrlf
		strItems = strItems & "				<tr>" & vbcrlf
		strItems = strItems & "					<td align=""left"" valign=""top"" height=""60"">" & vbcrlf
		strItems = strItems & "						<a style=""color:#787878; font-size:16px; font-weight:bold; text-decoration:none;"" target=""_blank"" href=""http://www.wirelessemporium.com/p-" & rs("itemid") & "-" & formatSEO(rs("itemDesc")) & ".asp?utm_source=Email&utm_medium=FollowUp&utm_campaign=14DayReview"">" & rs("itemdesc") & "</a><br><br>" & vbcrlf
		strItems = strItems & "						Item ID: " & rs("itemid") & vbcrlf
		strItems = strItems & "					</td>" & vbcrlf
		strItems = strItems & "				</tr>" & vbcrlf
		strItems = strItems & "				<tr>" & vbcrlf
		strItems = strItems & "					<td align=""left"" valign=""top"" height=""60"">" & vbcrlf
		strItems = strItems & "						<table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; color:#8D8D8D; text-decoration: none;"">" & vbcrlf
		strItems = strItems & "							<tr>" & vbcrlf
		strItems = strItems & "								<td align=""left"" valign=""top"">" & vbcrlf
		strItems = strItems & 									getRatingAvgStar(rs("avgRating")) & vbcrlf
		strItems = strItems & "								</td>" & vbcrlf
		strItems = strItems & "								<td align=""left"" valign=""middle"">" & vbcrlf
		strItems = strItems & " 								&nbsp; (" & formatnumber(rs("cnt"),0) & " customer reviews)" & vbcrlf
		strItems = strItems & "								</td>" & vbcrlf
		strItems = strItems & "							</tr>" & vbcrlf
		strItems = strItems & "						</table>" & vbcrlf
		strItems = strItems & "					</td>" & vbcrlf
		strItems = strItems & "				</tr>" & vbcrlf
		strItems = strItems & "				<tr>" & vbcrlf
		strItems = strItems & "					<td align=""center"" valign=""middle"" height=""120""> " & vbcrlf
		strItems = strItems & "						<a target=""_blank"" href=""http://www.wirelessemporium.com/user-reviews.asp?id=" & rs("itemID") & "&oid=" & rs("orderid") & "&utm_source=Email&utm_medium=FollowUp&utm_campaign=14DayReview"" style=""text-decoration:none;""><img src=""http://www.wirelessemporium.com/images/email/14-day-review/button_writereview.jpg"" border=""0"" /></a>" & vbcrlf
		strItems = strItems & "					</td>" & vbcrlf
		strItems = strItems & "				</tr>" & vbcrlf							
		strItems = strItems & "			</table>" & vbcrlf
		strItems = strItems & "		</td>" & vbcrlf
		strItems = strItems & "	</tr>" & vbcrlf
	end if
	
	rs.movenext
loop

strItems = strItems & "</table>" & vbcrlf
strTemplate = replace(strTemplate, "!##CUSTOMER_NAME##!", strCustName)
strTemplate = replace(strTemplate, "!##ITEM_TABLE##!", strItems)

if lapCount > 0 then
	'send a last one
	response.write strTemplate & "<br><br>"
'	set objEmail = CreateObject("CDO.Message")
'	with objEmail
'		.From = strFrom
'	
'		if isTestMode then 				
'			.To = "doldding82@gmail.com"
'			.Bcc = "doldding82@hotmail.com;doldding82@yahoo.com"					
'		else
'			.To = strTO
'			.Bcc = "doldding82@gmail.com"
'		end if
'	
'		.Subject = strSubject
'		.HTMLBody = strTemplate
'		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
'		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
'		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
'		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
'		.Configuration.Fields.Update
'		.Send
'	end with
end if

rs.close
set rs = nothing

function getRatingAvgStar(rating)
	dim nRating 
	dim strRatingImg
	nRating = cdbl(rating)
	strRatingImg = 	"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />"
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""	
		for i=1 to 5
			if nRating => i then
				strRatingImg = strRatingImg & "<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_full.jpg"" border=""0"" width=""30"" height=""29"" /> "
			elseif nRating => ((i - 1) + .1) then
				strRatingImg = strRatingImg & "<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_half.jpg"" border=""0"" width=""30"" height=""29"" /> "				
			else
				strRatingImg = strRatingImg & "<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" /> "
			end if
		next		
	end if
	
	getRatingAvgStar = strRatingImg
end function


Function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
'		Filepath = Server.MapPath(fileName)
'		Filepath = FSO.GetAbsolutePathName(fileName)
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
End Function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function
%>