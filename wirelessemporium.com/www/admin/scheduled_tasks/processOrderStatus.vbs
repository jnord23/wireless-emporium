set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

endDate		=	date
cdo_body	=	""
cdo_to 		= 	"charles@wirelessemporium.com,ruben@wirelessemporium.com,terry@wirelessemporium.com,bob@wirelessemporium.com"
'cdo_to		=	"terry@wirelessemporium.com"
cdo_from 	= 	"sales@wirelessemporium.com"
cdo_subject = 	"*** Daily Order Status ***"
strEmail	=	""

'endDate	=	cdate("5/27/11")

sql	=	"select	x.shortdesc, convert(varchar(10), a.thub_posted_date, 20) processed_datetime" & vbcrlf & _
		"	,	count(*) numOrders, sum(case when a.scandate is not null then 1 else 0 end) numScan" & vbcrlf & _
		"from 	we_orders a with (nolock) join xstore x with (nolock)" & vbcrlf & _
		"	on	x.site_id = a.store join v_accounts v with (nolock)" & vbcrlf & _
		"	on	a.store = v.site_id and a.accountid = v.accountid " & vbcrlf & _
		"where	a.thub_posted_date >= '" & endDate & "'" & vbcrlf & _
		"	and	a.thub_posted_to_accounting = 'W'" & vbcrlf & _
		"	and	a.approved = 1" & vbcrlf & _
		"	and	(a.cancelled is null or a.cancelled = 0)" & vbcrlf & _
		"	and	x.active = 1" & vbcrlf & _
		"group by	x.shortdesc, convert(varchar(10), a.thub_posted_date, 20)" & vbcrlf & _
		"order by 1 desc, 2" & vbcrlf
		
set objRsEmail = oConn.execute(sql)

if not objRsEmail.eof then
	strEmail = 	"<center><b>View By STORE</b></center>" & vbcrlf & _
				"<table width=""400"" border=""1"" align=""center"" cellpadding=""2"" cellspacing=""0"" style=""border-collapse:collapse;font-family: Arial, Helvetica, sans-serif; font-size:10px;"">" & vbcrlf & _
				"	<tr>" & vbcrlf & _
				"		<td width=""50"" style=""font-size:12px;"" bgcolor=""#ccc000""><b>STORE</b></td>" & vbcrlf & _
				"		<td width=""150"" style=""font-size:12px;"" bgcolor=""#ccc000""><b>Processed<br />DateTime</b></td>" & vbcrlf & _					
				"		<td width=""100"" style=""font-size:12px;"" bgcolor=""#ccc000""><b>Number of Orders<br />Processed</b></td>" & vbcrlf & _
				"		<td width=""100"" style=""font-size:12px;"" bgcolor=""#ccc000""><b>Number of Orders<br />Scanned</b></td>" & vbcrlf & _
				"	</tr>" & vbcrlf
				
	do until objRsEmail.eof
		numOrders = objRsEmail("numOrders")
		numScan = objRsEmail("numScan")
		if isnumeric(numOrders) then
			if numOrders = 0 then
				processRatio = 0.0
			else
				processRatio = (1.0 * numScan / numOrders) * 100.0
			end if
		end if

		strEmail	=	strEmail	&	"	<tr>" & vbcrlf
		strEmail	=	strEmail	&	"		<td width=""50"">" & objRsEmail("shortdesc") & "</td>" & vbcrlf
		strEmail	=	strEmail	&	"		<td width=""150"">" & objRsEmail("processed_datetime") & "</td>" & vbcrlf
		strEmail	=	strEmail	&	"		<td width=""100"">" & formatnumber(objRsEmail("numOrders"), 0) & "</td>" & vbcrlf
		strEmail	=	strEmail	&	"		<td width=""100"">" & formatnumber(objRsEmail("numScan"), 0) & " (" & formatnumber(processRatio, 2) & "%)</td>" & vbcrlf
		strEmail	=	strEmail	&	"	</tr>" & vbcrlf
		
		objRsEmail.movenext
	loop
	strEmail	=	strEmail	&	"</table>" & vbcrlf
end if		



sql	=	"select	case when isnull(q.numRegularOrder, 0) = isnull(q.numVendorOrder, 0) then 1 else 0 end isDropShipOrder" & vbcrlf & _
		"	,	x.shortDesc" & vbcrlf & _
		"	,	isnull(case when o.isShipMethod = 1 then o.token end" & vbcrlf & _
		"				, " & vbcrlf & _
		"				case when t.shiptypeid = 1 then " & vbcrlf & _
		"						case when q.quantity > 1 then t.token + '-MULTI' else t.token + '-SINGLE' end" & vbcrlf & _
		"					else isnull(t.token, a.shiptype) " & vbcrlf & _
		"				end" & vbcrlf & _
		"		) shipToken2" & vbcrlf & _
		"	,	cast(	case when q.quantity = 1 and q.numPhones = 1 then 0" & vbcrlf & _
		"					when isnull(q.numRegularOrder, 0) >= 10 then 1" & vbcrlf & _
		"					when cast(isnull(a.ordergrandtotal, 0.00) as money) >= 175.00 then 1" & vbcrlf & _
		"					else 0" & vbcrlf & _
		"				end as bit) isWhaleOrder" & vbcrlf & _
		"	,	count(*) numOrders" & vbcrlf & _
		"	,	isnull(sum(case when a.scandate is not null then 1 else 0 end), 0) numScan" & vbcrlf & _
		"	,	isnull(sum(case when a.cancelled is not null or cancelled = 1 then 1 else 0 end), 0) numCancel" & vbcrlf & _
		"from 	we_orders a with (nolock) join xstore x with (nolock)" & vbcrlf & _
		"	on	x.site_id = a.store join " & vbcrlf & _
		"		(" & vbcrlf & _
		"		select	a.store, a.orderid, isnull(sum(b.quantity), 0) quantity, sum(b.quantity * isnull(c.itemWeight, 0.0)) itemWeight" & vbcrlf & _
		"			,	count(*) numRegularOrder" & vbcrlf & _
		"			,	isnull(sum(case when m.id is not null then 1 " & vbcrlf & _
		"								else" & vbcrlf & _
		"									case when c.vendor in ('CM', 'DS', 'MLD') then 1 " & vbcrlf & _
		"										when c.partnumber like '%HYP%' then 1 " & vbcrlf & _
		"										else 0" & vbcrlf & _
		"									end" & vbcrlf & _
		"							end), 0) numVendorOrder" & vbcrlf & _
		"			,	sum(case when c.typeid = 16 then 1 else 0 end) numPhones" & vbcrlf & _
		"		from 	we_orders a with (nolock) join we_orderdetails b with (nolock)" & vbcrlf & _
		"			on	a.orderid = b.orderid left outer join we_items c with (nolock)" & vbcrlf & _
		"			on	b.itemid = c.itemid left outer join we_items_musicskins m with (nolock)" & vbcrlf & _
		"			on	b.itemid = m.id" & vbcrlf & _
		"		where	a.thub_posted_date >= '" & endDate & "'" & vbcrlf & _
		"			and	a.approved = 1" & vbcrlf & _
		"			and	(b.reship = 0 or b.reship is null)" & vbcrlf & _
		"			and	b.itemid not in (1)" & vbcrlf & _
		"		group by a.store, a.orderid" & vbcrlf & _
		"		) q" & vbcrlf & _
		"	on	a.store = q.store and a.orderid = q.orderid left outer join XShipType t with (nolock)" & vbcrlf & _
		"	on	a.shiptype = t.shipdesc left outer join XOrderType o with (nolock)" & vbcrlf & _
		"	on	a.extOrderType = o.typeid " & vbcrlf & _
		"where	a.thub_posted_date >= '" & endDate & "'" & vbcrlf & _
		"	and	x.active = 1" & vbcrlf & _
		"group by	case when isnull(q.numRegularOrder, 0) = isnull(q.numVendorOrder, 0) then 1 else 0 end " & vbcrlf & _
		"		,	x.shortDesc" & vbcrlf & _
		"		,	isnull(case when o.isShipMethod = 1 then o.token end" & vbcrlf & _
		"					, " & vbcrlf & _
		"					case when t.shiptypeid = 1 then " & vbcrlf & _
		"							case when q.quantity > 1 then t.token + '-MULTI' else t.token + '-SINGLE' end" & vbcrlf & _
		"						else isnull(t.token, a.shiptype) " & vbcrlf & _
		"					end" & vbcrlf & _
		"			) " & vbcrlf & _
		"		,	cast(	case when q.quantity = 1 and q.numPhones = 1 then 0" & vbcrlf & _
		"						when isnull(q.numRegularOrder, 0) >= 10 then 1" & vbcrlf & _
		"						when cast(isnull(a.ordergrandtotal, 0.00) as money) >= 175.00 then 1" & vbcrlf & _
		"						else 0" & vbcrlf & _
		"					end as bit)" & vbcrlf & _
		"order by 1 desc, 2 desc, 3" & vbcrlf
		
		
set objRsAllOrders = oConn.execute(sql)

numTotalDropShip = 0
if not objRsAllOrders.eof then
	strEmail	=	strEmail	&	"<br /><center><b>View By Ship Method</b></center>" & vbcrlf
	strEmail	=	strEmail	&	"	<table width=""460"" border=""1"" align=""center"" cellpadding=""5"" cellspacing=""0"" style=""border-collapse:collapse;font-family: Arial, Helvetica, sans-serif; font-size:12px;"">" & vbcrlf
	strEmail	=	strEmail	&	"		<tr>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""50"" rowspan=""2"" align=""center""><b>STORE</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""80"" rowspan=""2""><b>SHIP TYPE</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""260"" colspan=""4"" align=""center""><b>Number of Orders</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"		</tr>" & vbcrlf
	strEmail	=	strEmail	&	"		<tr>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""60""><b>Processed</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""60""><b>Scanned</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""60""><b>Cancelled</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""80""><b>Status</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"		</tr>" & vbcrlf
	
	do until objRsAllOrders.eof
		numOrders = objRsAllOrders("numOrders")
		numScan = objRsAllOrders("numScan")
		numCancel = objRsAllOrders("numCancel")
		isDropShipper = objRsAllOrders("isDropShipOrder")
		
		if isnumeric(numOrders) then
			if numOrders = 0 then
				processRatio = 0.0
			else
				processRatio = (1.0 * (numScan+numCancel) / numOrders) * 100.0
			end if
		end if
		
		if isDropShipper = 1 then
			numTotalDropShip = numTotalDropShip + numOrders
		else
			strEmail	=	strEmail	&	"		<tr>" & vbcrlf
			if objRsAllOrders("isWhaleOrder") then
				strEmail	=	strEmail	&	"			<td>ALL</td>" & vbcrlf
				strEmail	=	strEmail	&	"			<td>WHALE</td>" & vbcrlf
			else
				strEmail	=	strEmail	&	"			<td>" & objRsAllOrders("shortdesc") & "</td>" & vbcrlf
				strEmail	=	strEmail	&	"			<td>" & objRsAllOrders("shiptoken2") & "</td>" & vbcrlf			
			end if
			strEmail	=	strEmail	&	"			<td>" & formatnumber(numOrders, 0) & "</td>" & vbcrlf
			strEmail	=	strEmail	&	"			<td>" & formatnumber(numScan, 0) & "</td>" & vbcrlf
			strEmail	=	strEmail	&	"			<td>" & formatnumber(numCancel, 0) & "</td>" & vbcrlf
			strEmail	=	strEmail	&	"			<td>" & vbcrlf
			strEmail	=	strEmail	&	"				<table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""font-family: Arial, Helvetica, sans-serif; font-size:12px;"">" & vbcrlf
			strEmail	=	strEmail	&	"					<tr>" & vbcrlf
			strEmail	=	strEmail	&	"						<td align=""left"">" & formatnumber(processRatio, 2) & "%</td>" & vbcrlf
			strEmail	=	strEmail	&	"						<td align=""right"">" & vbcrlf

			if processRatio = 100 then
				strEmail	=	strEmail	&	"						<img src=""http://www.wirelessemporium.com/images/check.jpg"" border=""0"" width=""15""/>" & vbcrlf
			else
				strEmail	=	strEmail	&	"						<img src=""http://www.wirelessemporium.com/images/x.jpg"" border=""0"" width=""15""/>" & vbcrlf			
			end if

			strEmail	=	strEmail	&	"						</td>" & vbcrlf
			strEmail	=	strEmail	&	"					</tr>" & vbcrlf
			strEmail	=	strEmail	&	"				</table>" & vbcrlf
			strEmail	=	strEmail	&	"			</td>" & vbcrlf
			strEmail	=	strEmail	&	"		</tr>" & vbcrlf
		end if
		objRsAllOrders.movenext
	loop

	strEmail	=	strEmail	&	"		<tr>" & vbcrlf
	strEmail	=	strEmail	&	"			<td colspan=""2"" align=""center"">Dropship Orders</td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td colspan=""3"" align=""left"">" & formatnumber(numTotalDropShip, 0) & "</td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td align=""center"">" & vbcrlf
	strEmail	=	strEmail	&	"				<table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""font-family: Arial, Helvetica, sans-serif; font-size:12px;"">" & vbcrlf
	strEmail	=	strEmail	&	"					<tr>" & vbcrlf
	strEmail	=	strEmail	&	"						<td align=""left"">" & formatnumber(100, 2) & "%</td>" & vbcrlf
	strEmail	=	strEmail	&	"						<td align=""right""><img src=""http://www.wirelessemporium.com/images/check.jpg"" border=""0"" width=""15"" /></td>" & vbcrlf
	strEmail	=	strEmail	&	"					</tr>" & vbcrlf
	strEmail	=	strEmail	&	"				</table>" & vbcrlf
	strEmail	=	strEmail	&	"			</td>" & vbcrlf
	strEmail	=	strEmail	&	"		</tr>" & vbcrlf
	strEmail	=	strEmail	&	"	</table>" & vbcrlf
end if		

cdo_body = strEmail

if cdo_body <> "" then
	call cdosend(cdo_to, cdo_from, cdo_subject, cdo_body)
end if


sub cdosend(strto,strfrom,strsubject,strbody)
	on error resume next
	dim objMail
	set objMail = createobject("cdo.message")
	with objMail
		.from = strfrom
		.to = strto
		.subject = strsubject
		.htmlbody = cstr("" & strbody)
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.configuration.fields.item("urn:schemas:httpmail:sender") = "sales@wirelessemporium.com"
		.configuration.fields.update
		.send
	end with
	set objMail = nothing
	on error goto 0
end sub

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
