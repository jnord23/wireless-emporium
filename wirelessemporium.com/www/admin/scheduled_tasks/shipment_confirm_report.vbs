set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

bTestMode = false

call sendReport()

sub sendReport()
	strEmail	=	""
	
	sql	=	"select	x.shortdesc, convert(varchar(10), a.scandate, 20) scandate, a.shiptype, count(*) total, sum(case when b.isUpdated = 1 then 1 else 0 end) sent" & vbcrlf & _
			"from	we_orders a join XShipConfirmEmailSentTemp b" & vbcrlf & _
			"	on	a.orderid = b.orderid join xstore x" & vbcrlf & _
			"	on	a.store = x.site_id" & vbcrlf & _
			"group by x.shortdesc, convert(varchar(10), a.scandate, 20), a.shiptype" & vbcrlf & _
			"order by 1 desc, 2, 3" & vbcrlf
			
	set objRsResult = oConn.execute(sql)
	
	strEmail	=	strEmail	&	"	<center><b>Shipment Confirmation Email Sent</b></center>" & vbcrlf
	strEmail	=	strEmail	&	"	<table width=""550"" border=""1"" align=""center"" cellpadding=""5"" cellspacing=""0"" style=""border-collapse:collapse;font-family: Arial, Helvetica, sans-serif; font-size:12px;"">" & vbcrlf
	strEmail	=	strEmail	&	"		<tr>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""50"" rowspan=""2"" align=""center""><b>STORE</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""100"" rowspan=""2""><b>DATE SCANNED</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""150"" rowspan=""2""><b>SHIP TYPE</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""200"" colspan=""3"" align=""center""><b>Number of Orders</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"		</tr>" & vbcrlf
	strEmail	=	strEmail	&	"		<tr>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""60""><b>Scanned</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""60""><b>Confirm Sent</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""80""><b>Status</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"		</tr>" & vbcrlf
	
	if not objRsResult.eof then
		do until objRsResult.eof
			numScan = cdbl(objRsResult("total"))
			numSent = cdbl(objRsResult("sent"))
			
			if isnumeric(numScan) then
				if numScan = 0 then
					processRatio = 0.0
				else
					processRatio = (1.0 * (numSent) / numScan) * 100.0
				end if
			end if
			
			strEmail	=	strEmail	&	"		<tr>" & vbcrlf
			strEmail	=	strEmail	&	"			<td>" & objRsResult("shortdesc") & "</td>" & vbcrlf
			strEmail	=	strEmail	&	"			<td>" & objRsResult("scandate") & "</td>" & vbcrlf
			strEmail	=	strEmail	&	"			<td>" & objRsResult("shiptype") & "</td>" & vbcrlf
			strEmail	=	strEmail	&	"			<td align=""right"">" & formatnumber(numScan, 0) & "</td>" & vbcrlf
			strEmail	=	strEmail	&	"			<td align=""right"">" & formatnumber(numSent, 0) & "</td>" & vbcrlf
			strEmail	=	strEmail	&	"			<td>" & vbcrlf
			strEmail	=	strEmail	&	"				<table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""font-family: Arial, Helvetica, sans-serif; font-size:12px;"">" & vbcrlf
			strEmail	=	strEmail	&	"					<tr>" & vbcrlf
			strEmail	=	strEmail	&	"						<td align=""left"">" & formatnumber(processRatio, 2) & "%</td>" & vbcrlf
			strEmail	=	strEmail	&	"						<td align=""right"">" & vbcrlf
	
			if processRatio = 100 then
				strEmail	=	strEmail	&	"						<img src=""http://www.wirelessemporium.com/images/check.jpg"" border=""0"" width=""15""/>" & vbcrlf
			else
				strEmail	=	strEmail	&	"						<img src=""http://www.wirelessemporium.com/images/x.jpg"" border=""0"" width=""15""/>" & vbcrlf			
			end if
	
			strEmail	=	strEmail	&	"						</td>" & vbcrlf
			strEmail	=	strEmail	&	"					</tr>" & vbcrlf
			strEmail	=	strEmail	&	"				</table>" & vbcrlf
			strEmail	=	strEmail	&	"			</td>" & vbcrlf
			strEmail	=	strEmail	&	"		</tr>" & vbcrlf
	
			objRsResult.movenext
		loop
	else
		strEmail	=	strEmail	&	"		<tr>" & vbcrlf
		strEmail	=	strEmail	&	"			<td colspan=""6"">No orders found to send out shipment confirmation email today..</td>" & vbcrlf
		strEmail	=	strEmail	&	"		</tr>" & vbcrlf
	end if

	strEmail	=	strEmail	&	"	</table>" & vbcrlf

	cdoFrom		=	"sales@wirelessemporium.com"
	if bTestMode then
		cdoTo		=	"terry@wirelessemporium.com"
	else
		cdoTo		=	"tony@wirelessemporium.com,charles@wirelessemporium.com,ruben@wirelessemporium.com,david@wirelessemporium.com"	
	end if
	cdoSubject	=	"*** Daily Shipment Confirmation Email Status ***"
	cdoBcc		=	"terry@wirelessemporium.com,shipping@wirelessemporium.com"
	
	call cdosend(cdoFrom, cdoTo, cdoSubject, strEmail, cdoBcc)

	sql	=	"	update	a" & vbcrlf & _
			"	set		confirmSent = 'yes'" & vbcrlf & _
			"		,	confirmdatetime = getdate()" & vbcrlf & _
			"	from	we_orders a join XShipConfirmEmailSentTemp b" & vbcrlf & _
			"		on	a.orderid = b.orderid" & vbcrlf & _
			"	where	b.isUpdated = 1" & vbcrlf
	if not bTestMode then				
		oConn.execute(sql)
		
		sql	=	"truncate table XShipConfirmEmailSentTemp"
		oConn.execute(sql)								
	end if
end sub

sub cdosend(strFrom, strTo, strSubject, strBody, strBcc)
	if instr(trim(strTo), "@") > 0 and instr(trim(strTo), " ") = 0 then
		set objEmail = CreateObject("CDO.Message")
		with objEmail
			.From = strFrom
			.To = strTo
			.Bcc = strBcc
			.Subject = strSubject
			.HTMLBody = strBody
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Update
			.Send
		end with
	end if
end sub

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
