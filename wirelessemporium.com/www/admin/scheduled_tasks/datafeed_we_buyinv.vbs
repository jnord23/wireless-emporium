set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
filename = "Buy.com - Inventory.txt"
path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename

if fs.FileExists(path) then
	fs.DeleteFile(path)
end if

'Create the file and write some data to it.
set file = fs.CreateTextFile(path)
dim SQL, RS

sql	=	"	select	a.itemid, a.typeid, a.partnumber as mpn, a.price_our, a.price_retail, a.price_buy" & vbcrlf & _
		"		,	case when a.inv_qty = -1 then isnull(c.inv_qty, 0) else a.inv_qty end inv_qty" & vbcrlf & _
		"		,	a.hidelive, a.buy_sku, isnull(b.modelname, '') modelName, isnull(convert(varchar(8000), a.itemlongdetail), '') itemlongdetail" & vbcrlf & _
		"	from	we_items a with (nolock) left outer join we_models b with (nolock)" & vbcrlf & _
		"		on	a.modelid = b.modelid left outer join " & vbcrlf & _
		"			(" & vbcrlf & _
		"			-- master item's qty" & vbcrlf & _
		"			select	partnumber, max(inv_qty) inv_qty" & vbcrlf & _
		"			from	we_items with (nolock)" & vbcrlf & _
		"			where	hidelive = 0 and inv_qty > 0 and price_our > 0" & vbcrlf & _
		"			group by partnumber	" & vbcrlf & _
		"			) c" & vbcrlf & _
		"		on	a.partnumber = c.partnumber left outer join " & vbcrlf & _
		"			(" & vbcrlf & _
		"			select	a.partnumber orphan_partnumber" & vbcrlf & _
		"			from	(" & vbcrlf & _
		"					select	partnumber" & vbcrlf & _
		"						,	sum(case when master = 1 then 1 else 0 end) nmaster" & vbcrlf & _
		"						,	sum(case when master = 0 then 1 else 0 end) nslave" & vbcrlf & _
		"					from	we_items a with (nolock)" & vbcrlf & _
		"					where	hidelive = 0 and inv_qty <> 0 and price_our > 0" & vbcrlf & _
		"					group by partnumber" & vbcrlf & _
		"					) a	" & vbcrlf & _
		"			where	a.nmaster <> 1	" & vbcrlf & _
		"			) o	" & vbcrlf & _
		"		on	a.partnumber = o.orphan_partnumber	" & vbcrlf & _
		"	where	a.hidelive = 0 and a.inv_qty <> 0 and a.price_our > 0	" & vbcrlf & _
		"		and	o.orphan_partnumber is null	" & vbcrlf & _
		"	order by a.itemid desc"


set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if not RS.eof then
	file.WriteLine "##Type=Inventory;Version=3.0"
	file.WriteLine "ListingId" & vbtab & "ProductId" & vbtab & "ProductIdType" & vbtab & "ItemCondition" & vbtab & "Price" & vbtab & "MAP" & vbtab & "Quantity" & vbtab & "OfferExpeditedShipping" & vbtab & "Description" & vbtab & "ShippingRateStandard" & vbtab & "ShippingRateExpedited" & vbtab & "ShippingLeadTime" & vbtab & "ReferenceId"
	do until RS.eof
		itemid = rs("itemid")
		typeid = rs("typeid")
		mpn = rs("mpn")
		price_our = rs("price_our")
		price_retail = rs("price_retail")
		price_buy = rs("price_buy")
		inv_qty = rs("inv_qty")
		hidelive = rs("hidelive")
		buy_sku = rs("buy_sku")
		modelName = rs("modelName")
		itemlongdetail = rs("itemlongdetail")
		itemCondition = 1 'Brand New
		if instr(lcase(itemlongdetail), "refurbished") > 0 then
			itemCondition = 10
		end if	

		MfgProductIdentifier = "WE" & itemid & mpn
		MfgProductIdentifier = replace(MfgProductIdentifier,"-","",1,2)
		
		strline = vbtab													'ListingId
		if isNull(buy_sku) then
			strline = strline & mpn & "-" & itemid & vbtab				'ProductId
			strline = strline & "3" & vbtab								'ProductIdType
		else
			strline = strline & buy_sku & vbtab							'ProductId
			strline = strline & "0" & vbtab								'ProductIdType
		end if
		strline = strline & itemCondition & vbtab									'ItemCondition
		strline = strline & price_our & vbtab							'Price
		strline = strline & price_retail & vbtab						'MAP
		strline = strline & inv_qty & vbtab								'Quantity
		strline = strline & "1" & vbtab									'OfferExpeditedShipping
		strline = strline & vbtab										'Description
		strline = strline & "0.00" & vbtab								'ShippingRateStandard
		strline = strline & "6.99" & vbtab								'ShippingRateExpedited
		strline = strline & vbtab										'ShippingLeadTime
		strline = strline & MfgProductIdentifier						'ReferenceId
		file.WriteLine strline
		RS.movenext
	loop
end if
RS.close
set RS = nothing
file.close()

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function
