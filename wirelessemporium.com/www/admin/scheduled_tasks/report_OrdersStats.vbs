set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

dim dateStart, bodyText
dateStart = dateAdd("D",-1,date)
bodyText = ""

dim a, b, SQLOrderType, myTotal, cogsTotal, myStore, myURL, myTable
for b = 1 to 15
	myTotal = 0
	cogsTotal = 0
	mobileCnt = 0
	mobileTotal = 0
	mobileCogs = 0
	select case b
		case 1
			a = 0
			myStore = "Wireless Emporium"
			SQLOrderType = " AND (A.extOrderType IS NULL OR A.extOrderType < 4)"
		case 2
			a = 2
			myStore = "Cellular Outfitter"
			SQLOrderType = " AND (A.extOrderType IS NULL OR A.extOrderType < 4)"
		case 3
			a = 1
			myStore = "Cellphone Accents"
			SQLOrderType = " AND (A.extOrderType IS NULL OR A.extOrderType < 4)"
		case 4
			a = 3
			myStore = "Phonesale"
			SQLOrderType = " AND (A.extOrderType IS NULL OR A.extOrderType < 4)"
		case 5
			a = 10
			myStore = "TabletMall.com"
			SQLOrderType = " AND (A.extOrderType IS NULL OR A.extOrderType < 4)"
		case 6
			a = 0
			myStore = "Custom Case - WE"
			SQLOrderType = " AND (A.extOrderType IS NULL OR A.extOrderType < 4) and c.partnumber like 'WCD-%' "
		case 7
			a = 0
			myStore = "Post Purchase - WE"
			SQLOrderType = " AND c.postpurchase = 1 and a.pp_ordergrandtotal > 0"
		case 8
			a = 2
			myStore = "Post Purchase - CO"
			SQLOrderType = " AND c.postpurchase = 1 and a.pp_ordergrandtotal > 0"
		case 9
			a = 2
			myStore = "Custom Case - CO"
			SQLOrderType = " AND (A.extOrderType IS NULL OR A.extOrderType < 4) and c.partnumber like 'WCD-%' "
		case 10
			a = 0
			myStore = "Custom Case(Featured Artist) - WE"
			SQLOrderType = " AND (A.extOrderType IS NULL OR A.extOrderType < 4) and d.subtypeid = 1031 "
		case 11
			a = 0
			myStore = "eBay"
			SQLOrderType = " AND A.extOrderType = 4"
		case 12
			a = 0
			myStore = "AtomicMall"
			SQLOrderType = " AND A.extOrderType = 5"
		case 13
			a = 0
			myStore = "Amazon - WE"
			SQLOrderType = " AND A.extOrderType = 6"
		case 14
			a = 0
			myStore = "Buy.com - WE"
			SQLOrderType = " AND A.extOrderType = 7"
		case 15
			a = 0
			myStore = "Sears"
			SQLOrderType = " AND A.extOrderType = 8"
	end select
	select case a
		case 0
			myURL = "wirelessemporium"
			myTable = "we"
		case 1
			myURL = "cellphoneaccents"
			myTable = "CA"
		case 2
			myURL = "cellularoutfitter"
			myTable = "CO"
		case 3
			myURL = "phonesale"
			myTable = "PS"
		case 10
			myURL = "ereadersupply"
			myTable = "ER"
	end select
	
	SQL = "SELECT A.mobileSite, A.ordergrandtotal FROM we_orders A INNER JOIN " & myTable & "_accounts B ON A.accountid=B.accountid"
	SQL = SQL & " WHERE A.orderdatetime >= '" & dateStart & "' AND A.orderdatetime < '" & dateAdd("D",1,dateStart) & "'"
	SQL = SQL & " AND A.approved = 1"
	SQL = SQL & " AND (A.cancelled = 0 OR A.cancelled IS NULL)"
	SQL = SQL & " AND A.store = '" & a & "'"
	SQL = SQL & " AND A.parentOrderID is null"
	SQL = SQL & SQLOrderType
	SQL = SQL & " AND (B.email = 'phoneorder@wirelessemporium.com' OR B.email NOT LIKE '%@wirelessemporium.com')"
	SQL = SQL & " ORDER BY A.orderdatetime"

	sql =   "SELECT A.mobileSite, a.orderID, A.ordergrandtotal, isnull(a.pp_orderGrandTotal, 0) pp_orderGrandTotal, sum(c.COGS * c.quantity) as COGS FROM we_orders A left join we_orderDetails c on a.orderID = c.orderID left join " & vbcrlf & _
	        "we_items d on c.itemID = d.itemID INNER JOIN " & myTable & "_accounts B ON A.accountid=B.accountid WHERE A.orderdatetime >= '" & dateStart & "' " & vbcrlf & _
	        "AND A.orderdatetime < '" & dateAdd("D",1,dateStart) & "' AND A.approved = 1 AND (A.cancelled = 0 OR A.cancelled IS NULL) AND " & vbcrlf & _
	        "A.store = '" & a & "'" & SQLOrderType & " AND (B.email = 'phoneorder@wirelessemporium.com' OR B.email NOT LIKE '%@wirelessemporium.com') and parentOrderID is null " & vbcrlf & _
	        "group by A.mobileSite, a.orderID, A.ordergrandtotal, isnull(a.pp_orderGrandTotal, 0), A.orderdatetime ORDER BY A.mobileSite, A.orderdatetime"

'	response.write "<pre>" & sql & "</pre>"
	
	set RS = CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if RS.eof then
		bodyText = bodyText & "<h3>No orders found for " & myStore & " - " & dateStart & "</h3>"
	else
	    orderTotalCnt = RS.recordcount
		do until RS.eof
			if RS("mobileSite") then
			    mobileCnt = mobileCnt + 1
				if cDbl(RS("pp_orderGrandTotal")) > 0 then
				    mobileTotal = mobileTotal + cDbl(RS("pp_orderGrandTotal"))
				else
				    mobileTotal = mobileTotal + cDbl(RS("ordergrandtotal"))
				end if
			    if not isnull(RS("cogs")) then
				    mobileCogs = mobileCogs + cdbl(RS("cogs"))
			    end if
			else
				if cDbl(RS("pp_orderGrandTotal")) > 0 then
				    myTotal = myTotal + cDbl(RS("pp_orderGrandTotal"))
				else
				    myTotal = myTotal + cDbl(RS("ordergrandtotal"))
				end if
			    if not isnull(RS("cogs")) then
				    cogsTotal = cogsTotal + cdbl(RS("cogs"))
			    end if
			end if
			RS.movenext
		loop
		orderTotalCnt = orderTotalCnt - mobileCnt
		bodyText = bodyText & "<h3>DAILY ORDERS STATISTICS FOR: " & myStore & " - " & dateStart & "</h3>"
		bodyText = bodyText & "<h3> &nbsp; &nbsp; TOTAL ORDERS: " & orderTotalCnt & "</h3>"
		bodyText = bodyText & "<h3> &nbsp; &nbsp; TOTAL ORDER AMOUNT: " & formatCurrency(myTotal) & "</h3>"
		if orderTotalCnt = 0 then
			bodyText = bodyText & "<h3> &nbsp; &nbsp; AVERAGE ORDER AMOUNT: N/A</h3>"
		else
			bodyText = bodyText & "<h3> &nbsp; &nbsp; AVERAGE ORDER AMOUNT: " & formatCurrency(myTotal/orderTotalCnt) & "</h3>"
		end if
		bodyText = bodyText & "<h3> &nbsp; &nbsp; COGS: " & formatCurrency(cogsTotal) & "</h3>"
		if mobileCnt > 0 then
		    bodyText = bodyText & "<h3>MOBILE SITE DETAILS</h3>"
		    bodyText = bodyText & "<h3> &nbsp; &nbsp; TOTAL MOBILE ORDERS: " & mobileCnt & "</h3>"
		    bodyText = bodyText & "<h3> &nbsp; &nbsp; TOTAL MOBILE ORDER AMOUNT: " & formatCurrency(mobileTotal) & "</h3>"
		    bodyText = bodyText & "<h3> &nbsp; &nbsp; AVERAGE MOBILE ORDER AMOUNT: " & formatCurrency(mobileTotal/mobileCnt) & "</h3>"
		    bodyText = bodyText & "<h3> &nbsp; &nbsp; MOBILE COGS: " & formatCurrency(mobileCogs) & "</h3><!-- " & sql & " -->"
		end if
		if myStore = "Wireless Emporium" or myStore = "Cellular Outfitter" or myStore = "Cellphone Accents" or myStore = "Phonesale" then
			bodyText = bodyText & "<p><a href=""http://www." & myURL & ".com/admin/report_OrdersStats?dateStart=" & replace(cStr(dateStart),"/","%2F") & "&submitted=Generate+Report"">Summary Report</a></p>"
			bodyText = bodyText & "<p><a href=""http://www." & myURL & ".com/admin/report_OrdersStats?dateStart=" & replace(cStr(dateStart),"/","%2F") & "&details=1&submitted=Generate+Report"">Detail Report</a></p>"
		end if
		bodyText = bodyText & "<br><br>"
		'if myStore = "Wireless Emporium" then
		'	SQL = "SELECT C.quantity, D.price_Our FROM we_orders A INNER JOIN " & myTable & "_accounts B ON A.accountid=B.accountid"
		'	SQL = SQL & " INNER JOIN we_orderdetails C ON A.orderid=C.orderid"
		'	SQL = SQL & " INNER JOIN we_items D ON C.itemid=D.itemid"
		'	SQL = SQL & " WHERE A.orderdatetime >= '" & dateStart & "' AND A.orderdatetime < '" & dateAdd("D",1,dateStart) & "'"
		'	SQL = SQL & " AND A.approved = 1"
		'	SQL = SQL & " AND (A.cancelled = 0 OR A.cancelled IS NULL)"
		'	SQL = SQL & " AND A.store = '" & a & "'"
		'	SQL = SQL & " AND D.typeID = 16"
		'	SQL = SQL & " AND (B.email = 'phoneorder@wirelessemporium.com' OR B.email NOT LIKE '%@wirelessemporium.com')"
		'	SQL = SQL & " ORDER BY A.orderdatetime"
		'	set RS = CreateObject("ADODB.Recordset")
		'	RS.open SQL, oConn, 3, 3
		'	if RS.eof then
		'		bodyText = bodyText & "<h3>No orders found for " & myStore & " CELL PHONE SALES - " & dateStart & "</h3>"
		'	else
		'		myTotal = 0
		'		myQty = 0
		'		do until RS.eof
		'			myTotal = myTotal + (RS("quantity") * cDbl(RS("price_Our")))
		'			myQty = myQty + RS("quantity")
		'			RS.movenext
		'		loop
		'		bodyText = bodyText & "<h3>CELL PHONE SALES FOR: " & myStore & " - " & dateStart & "</h3>"
		'		bodyText = bodyText & "<h3>TOTAL ORDERS: " & myQty & "</h3>"
		'		bodyText = bodyText & "<h3>TOTAL ORDER AMOUNT: " & formatCurrency(myTotal) & "</h3>"
		'		bodyText = bodyText & "<h3>AVERAGE ORDER AMOUNT: " & formatCurrency(myTotal/myQty) & "</h3>"
		'		bodyText = bodyText & "<br><br>"
		'	end if
		'end if
	end if
next

Set objErrMail = CreateObject("CDO.Message")
With objErrMail
	.From = "service@wirelessemporium.com"
'	.To = "eugene@wirelessemporium.com,tony@wirelessemporium.com,ruben@wirelessemporium.com,jon@wirelessemporium.com,philippe@wirelessemporium.com,terry@wirelessemporium.com,joseph.enmanuel@sourcefit.com,Bernice@WirelessEmporium.com"
'	.To = "terry@wirelessemporium.com"
	.To = emailList("Daily Orders Statistics")
	.Subject = "Daily Orders Statistics"
	.HTMLBody = bodyText
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
	.Configuration.Fields.Update
	.Send
End With

Set objErrMail = nothing
Set filesys = nothing
Set demofolder = nothing
Set demofile = nothing
Set filecoll = nothing

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub


function emailList(emailName)
	sql = "select a.emailAddr, b.email from we_emailLists a left join we_adminUsers b on a.adminID = b.adminID where emailName = '" & emailName & "'"
	set emailListRS = oConn.execute(sql)
	
	toList = ""
	do while not emailListRS.EOF
		email1 = emailListRS("emailAddr")
		email2 = emailListRS("email")
		
		if isnull(email1) then
			toList = toList & email2 & ","
		else
			toList = toList & email1 & ","
		end if
		emailListRS.movenext
	loop
	if toList <> "" then toList = left(toList,len(toList)-1)
	emailList = toList
end function