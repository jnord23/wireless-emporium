timeStamp = Year(now) & Month(now) & Day(now)

Set objZip = CreateObject("XStandard.Zip")

sourcePath = "C:\ProgramData\Sophos\AutoUpdate\Logs\*.*"
descPath = "C:\sophosLogs\autoupdate_log_" & timeStamp & ".zip"
objZip.Pack sourcePath, descPath

sourcePath = "C:\ProgramData\Sophos\Sophos Anti-Virus\logs\*.*"
descPath = "C:\sophosLogs\antivirus_log_" & timeStamp & ".zip"
objZip.Pack sourcePath, descPath
