set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

WScript.Timeout = 1000
sFindings = ""
sHtml = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Strict//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"">" & vbcrlf & _
		"<html xmlns=""http://www.w3.org/1999/xhtml"">" & vbcrlf & _
		"<head>" & vbcrlf & _
		"<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />" & vbcrlf & _
		"<meta name=""viewport"" content=""width=device-width, initial-scale=1.0"" />" & vbcrlf & _
		"<title>Scan activities</title>" & vbcrlf & _
		"</head>" & vbcrlf & _
		"<body style=""margin: 0; padding: 0; width: 100%; -webkit-text-size-adjust: none;"">" & vbcrlf & _
		"<table width=""100%"" border=""1"" cellspacing=""0"" cellpadding=""2"" style=""font-size:11px; border-collapse:collapse;"">" & vbcrlf & _
		"  	<tr>" & vbcrlf & _
		"  		<td colspan=""8"">Metatags have been inserted, updated or deleted within last 5 minutes</td>"& vbcrlf & _
		"	</tr>"& vbcrlf & _
		"  	<tr>" & vbcrlf & _
		"  		<td align=""left"" style=""background-color:#333; color:#fff;"">RowNumber</td>"& vbcrlf & _
		"  		<td align=""left"" style=""background-color:#333; color:#fff;"">textData</td>"& vbcrlf & _
		"  		<td align=""left"" style=""background-color:#333; color:#fff;"">applicationName</td>"& vbcrlf & _
		"  		<td align=""left"" style=""background-color:#333; color:#fff;"">loginName</td>"& vbcrlf & _
		"  		<td align=""left"" style=""background-color:#333; color:#fff;"">startTime</td>"& vbcrlf & _
		"  		<td align=""left"" style=""background-color:#333; color:#fff;"">hostName</td>"& vbcrlf & _
		"  		<td align=""left"" style=""background-color:#333; color:#fff;"">serverName</td>"& vbcrlf & _
		"  		<td align=""left"" style=""background-color:#333; color:#fff;"">sessionLoginName</td>"& vbcrlf & _
		"	</tr>"& vbcrlf

sql	=	"select	rowNumber, applicationName, loginName, startTime, hostName, serverName, sessionLoginName, textData" & vbcrlf & _
		"from	metatag_monitor" & vbcrlf & _
		"where	starttime >= dateadd(MI, -6, getdate())"
		
set rs = oConn.execute(sql)
do until rs.eof
	rowNumber = rs("rowNumber")
	appName = rs("applicationName")
	loginName = rs("loginName")
	startTime = rs("startTime")
	hostName = rs("hostName")
	serverName = rs("serverName")
	sessionLoginName = rs("sessionLoginName")
	textData = rs("textData")
	
	sFindings = sFindings & "<tr><td align=""left"">" & rowNumber & "</td>"
	sFindings = sFindings & "<td align=""left"">" & textData & "</td>"
	sFindings = sFindings & "<td align=""left"">" & appName & "</td>"
	sFindings = sFindings & "<td align=""left"">" & loginName & "</td>"
	sFindings = sFindings & "<td align=""left"">" & startTime & "</td>"
	sFindings = sFindings & "<td align=""left"">" & hostName & "</td>"
	sFindings = sFindings & "<td align=""left"">" & serverName & "</td>"
	sFindings = sFindings & "<td align=""left"">" & sessionLoginName & "</td></tr>"
	rs.movenext
loop
		
sHtml = sHtml & sFindings & "</table></body></html>"

if sFindings <> "" then
	Set objErrMail = CreateObject("CDO.Message")
	With objErrMail
		.From = "service@wirelessemporium.com"
'		.To = "terry@wirelessemporium.com"
		.To = "terry@wirelessemporium.com,jon@wirelessemporium.com,gene@wirelessemporium.com,ruben@wirelessemporium.com,damian@wirelessemporium.com,edwin@wirelessemporium.com,chiyan@wirelessemporium.com"
		.Subject = "Changes detected on metatag tables"
		.HTMLBody = sHtml
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Update
		.Send
	End With
	
	Set objErrMail = nothing
end if

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
