<%
response.buffer = false
pageTitle = "Create Googlebase Product List TXT for WirelessEmporium.com"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 2400 'seconds

dim fs, file, filename, path
filename = "wegooglebase.txt"
path = server.mappath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List TXT for Googlebase (WirelessEmporium.com)</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							response.write "<b>CreateFile:</b><br>"
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='/tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
	
<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		response.write "Deleting " & filename & ".<br>"
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	dim expDate, myMonth, myDay, myYear
	expDate = dateAdd("d",21,date)
	myMonth = month(expDate)
	if myMonth < 10 then myMonth = "0" & myMonth
	myDay = day(expDate)
	if myDay < 10 then myDay = "0" & myDay
	if myHour < 10 then myHour = "0" & myHour
	myYear = year(expDate)
	expDate = myYear & "-" & myMonth & "-" & myDay
	
	dim SQL, RS
	dim ModelName, BrandName, strItemLongDetail, strFeatures, strCompatibility, Condition, strline, itemDesc
	response.write "Creating " & filename & ".<br>"
	set file = fs.CreateTextFile(path)
	SQL = "SELECT A.itemID, A.PartNumber, A.itemDesc, A.itemDesc_Google, A.itemPic, A.price_Our, A.inv_qty, A.itemLongDetail,"
	SQL = SQL & " A.COMPATIBILITY,"
	SQL = SQL & " A.BULLET1, A.BULLET2, A.BULLET3, A.BULLET4, A.BULLET5, A.BULLET6, A.BULLET7, A.BULLET8, A.BULLET9, A.BULLET10,"
	SQL = SQL & " B.brandName, C.TypeName, D.ModelName"
	SQL = SQL & " FROM ((we_Items A INNER JOIN we_brands B ON A.brandID=B.brandID)"
	SQL = SQL & " INNER JOIN we_types C ON A.typeID=C.typeID)"
	SQL = SQL & " INNER JOIN we_models D ON A.modelID=D.modelID"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty > 0 AND A.price_Our > 0"
	SQL = SQL & " AND A.itemDesc NOT IN (SELECT itemDesc FROM we_Items GROUP BY itemDesc HAVING (Count(itemDesc) > 1))"
	SQL = SQL & " AND A.PartNumber NOT IN ('CC1-APL-IPOD-01','CC1-APL-IPOD-02')"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "description" & vbTab & "id" & vbTab & "link" & vbTab & "price" & vbTab & "title" & vbTab & "quantity" & vbTab & "image_link" & vbTab & "brand" & vbTab & "condition" & vbTab & "mpn" & vbTab & "payment_accepted" & vbTab & "expiration_date" & vbTab & "product_type" & vbTab & "feature" & vbTab & "c:Compatible_With:String" & vbTab & "c:GoogleAffiliateNetworkProductURL:String"
		do until RS.eof
			if isnull(RS("modelName")) then
				ModelName = "Universal"
			else
				ModelName = RS("modelName")
			end if
			
			if isnull(RS("brandName")) then
				BrandName = "Universal"
			else 
				BrandName = RS("brandName")
			end if
			
			strItemLongDetail = stripCallToAction(RS("itemLongDetail"))
			strItemLongDetail = replace(replace(strItemLongDetail,vbCrLf," "),vbTab," ")
			strItemLongDetail = replace(strItemLongDetail,"<a href='/downloads/","<a href='http://www.wirelessemporium.com/downloads/")
			
			strFeatures = "<ul>"
			if not isNull(RS("BULLET1")) and RS("BULLET1") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET1") & "</li>"
			if not isNull(RS("BULLET2")) and RS("BULLET2") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET2") & "</li>"
			if not isNull(RS("BULLET3")) and RS("BULLET3") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET3") & "</li>"
			if not isNull(RS("BULLET4")) and RS("BULLET4") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET4") & "</li>"
			if not isNull(RS("BULLET5")) and RS("BULLET5") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET5") & "</li>"
			if not isNull(RS("BULLET6")) and RS("BULLET6") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET6") & "</li>"
			if not isNull(RS("BULLET7")) and RS("BULLET7") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET7") & "</li>"
			if not isNull(RS("BULLET8")) and RS("BULLET8") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET8") & "</li>"
			if not isNull(RS("BULLET9")) and RS("BULLET9") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET9") & "</li>"
			if not isNull(RS("BULLET10")) and RS("BULLET10") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET10") & "</li>"
			strFeatures = strFeatures & "</ul>"
			strFeatures = replace(strFeatures,vbTab," ")
			if strFeatures = "<ul></ul>" then strFeatures = ""
			
			if not isNull(RS("COMPATIBILITY")) then
				strCompatibility = replace(RS("COMPATIBILITY"),vbCrLf," ")
			else
				strCompatibility = ""
			end if
			
			if inStr(Lcase(strItemLongDetail),"refurbished") > 0 or inStr(Lcase(RS("itemDesc")),"refurbished") > 0 or inStr(Lcase(strFeatures),"refurbished") > 0 then
				Condition = "refurbished"
			else
				Condition = "new"
			end if
			
			if not isNull(RS("itemDesc_Google")) then
				itemDesc = RS("itemDesc_Google")
			elseif RS("itemid") = 30626 then
				itemDesc = "Stylish EVA Pouch for Samsung Samsung Solstice SGH-A887, Samsung SGH-F480, Samsung SGH-U600, Samsung SGH-A707 SYNC, Sony Ericsson W980, Sony Ericsson W518a, Samsung Highlight SGH-T749, LG Venus VX8800, LG VX 8600 / AX 8600, LG enV2 VX9100, Blackberry Pearl Flip 8220/8230, HTC Shadow, LG Banter AX265/UX265, LG CF360, LG env3 VX9200, HTC 3125/Smartflip/8500/Star Trek, LG KF510, LG KG300, LG Muziq LX-570/UX/AX-565, LG Neon GT365, LG Versa VX9600, Motorola Evoke QA4, Samsung Gravity T459, Samsung Memoir T929, Motorola RAZR MAXX Ve, Motorola Rival A455, Motorola ROKR E2, Motorola V3c Razr, Motorola W220, Motorola W377, Nokia 5610, Pantech Breeze C520, Samsung A900, Samsung SGH-D807, Samsung Knack U310, Samsung SCH-U700 Gleam, Samsung SGH-A517, Samsung SGH-G600, Samsung SGH-T439, Motorola Entice W766, Samsung Intensity SCH-U450, Samsung SPH-M330, Motorola i856, Sony Ericsson F305, HTC Pure, Blackberry Pearl 8110/8120/8130, HTC Touch Diamond (CDMA), HTC Touch Diamond (GSM), HTC Touch Dual, LG KB770, LG Xenon GR500, Motorola Krave ZN4, Motorola Renegade V950, Motorola RIZR Z8, Palm Treo 680, Samsung Behold T919, Samsung Eternity SGH-A867, Samsung Glyde SCH-U940, Samsung Propel Pro SGH-i627, LG Chocolate VX8550, LG KP220, LG KS500, LG Shine CU720, Motorola Adventure V750, Samsung S5230 Star, Nokia 2680, Nokia 6670, Samsung SGH-T659, LG Bliss UX700, Samsung SGH-T629, Samsung SGH-T639, Sony Ericsson W380a, Sony Ericsson W395, Samsung Comeback T559, Motorola V3 Razr, Motorola V3m Razr, Motorola V3xx Razr, Motorola W755, Nokia 2610, Nokia 6555, Nokia 6560, Samsung A777, Samsung FlipShot SCH-U900, Samsung MyShot II SCH-R460, Samsung S3500, Samsung SCH-U550, Samsung SGH-A437/A436, Samsung SGH-A717, Samsung SGH-E215, Samsung SGH-J700, Nokia 6350, Motorola ZN200, LG VX 8700, Motorola EM330, HTC Cingular 8125, HTC Fuze, HTC Shadow 2, HTC XV6850 Touch Pro CDMA (Verizon), LG Dare VX9700, LG KP500/KP501 Cookie, Motorola Rapture VU30, Motorola RAZR VE20, Motorola RAZR2 V8, Motorola RAZR2 V9, Motorola RAZR2 V9m, Motorola V3a/V3i/V3r/V3t/V3e Razr, Motorola W490/W510/W5, Nokia 3555, Pantech PN-820, Samsung Helio Drift SPH-A503, Samsung Helio Fin SPH-A513, Samsung Helio Heat SPH-A303, Samsung Propel A767, Samsung Renown SCH-U810, Samsung SCH-U740, Samsung SGH-T239, LG LX290, Sony Ericsson C905a, LG KS10, LG KU990/GC900 Viewty, LG Rumor LX260, LG Rumor2 LX265, Motorola RAZR Maxx V6, Nextel Stature i9, Motorola Stature i9, Nokia XpressMusic 5800, Palm Centro, Palm Treo 750, Palm Treo 755p, Samsung Helio Mysto SPH-A523, Samsung Propel A767, LG Chocolate VX8500, LG Invision CB630, LG KF350, LG KF750/TU750 Secret, LG KP260, LG KU380, HTC Touch Diamond2, Samsung Smooth SCH-U350, Samsung Trance SCH-U490, Sanyo Pro-200, Sanyo Pro-700, Sanyo SCP-6600/Katana, Sanyo SCP-8500/Katana DLX, Sony Ericsson W350, HTC Fusion/5800/S720, Motorola VE440, Samsung SPH-M240, Nokia Mural 6750, Motorola ROKR U9, Motorola VE465, Nokia 5310, Nokia 6263, Samsung Highnote SPH-M630, LG Trax CU575, LG LX370/UX370, Other Casio Exilim C721, HTC Herman/Touch Pro CDMA (Alltel, Sprint), LG Decoy VX8610, LG Incite CT810, LG KF900 Prada II, LG Vu/CU920/CU915/TU915, Motorola A810, Motorola C261, Motorola Clutch i465, Palm Treo 700wx/700w/700p, Samsung Alias 2 SCH-U750, Sony Ericsson W760, LG CG180, LG Chocolate 3 VX8560, LG KC780, Nokia Surge 6790, Samsung SGH-T809/D820, Samsung SLM SGH-A747, Samsung SPH-M320, LG KM501, Samsung Hue II SCH-R600, Samsung Katalyst SGH-T739, Samsung S3600, Samsung SCH-R211, Samsung SCH-U540, Samsung SGH-D900i, Samsung SGH-T429, Motorola SLVR L9, Sony Ericsson G502, Sony Ericsson K510i, LG Helix AX310/UX310, Sanyo SCP-6650/Katana II, Sony Ericsson S302, Sony Ericsson TM506, Sony Ericsson W580i, Sony Ericsson W595, Sony Ericsson W760, Nokia E75, Blackberry Pearl 8100"
			elseif RS("itemid") = 13309 then
				itemDesc = "Retractable-Cord Car Charger for LG VI-125, LG VX 4650/VX 4700/ AX4750, LG PM-325, LG C1500, LG VX 3450, LG VX 8000, LG VX 8100, LG AX5000/UX5000, LG LX-350, LG AX-140/145 Aloha/200c, LG VX 5300/AX245, LG VX 3400, LG AX-390/UX-390, LG FUSIC LX550, LG AX4270, LG VX 9800, LG VX 7000, LG VX 6100, LG VX 3200, LG PM-225, LG VX 3300/3280, LG VX 5200, LG Migo VX1000, LG VX 8300"
			elseif RS("itemid") = 8879 then
				itemDesc = "HEAVY-DUTY Car Charger for LG VI-125, LG VX 4650/VX 4700/ AX4750, LG PM-325, LG C1500, LG VX 3450, LG VX 8000, LG VX 8100, LG AX5000/UX5000, LG LX-350, LG AX-140/145 Aloha/200c, LG VX 5300/AX245, LG VX 3400, LG AX-390/UX-390, LG FUSIC LX550, LG AX4270, LG VX 9800, LG VX 7000, LG VX 6100, LG VX 3200, LG PM-225, LG VX 3300/3280, LG VX 5200, LG Migo VX1000, LG VX 8300"
			elseif RS("itemid") = 10564 then
				itemDesc = "Car Charger for LG VI-125, LG VX 4650/VX 4700/ AX4750, LG PM-325, LG C1500, LG VX 3450, LG VX 8000, LG VX 8100, LG AX5000/UX5000, LG LX-350, LG AX-140/145 Aloha/200c, LG VX 5300/AX245, LG VX 3400, LG AX-390/UX-390, LG FUSIC LX550, LG AX4270, LG VX 9800, LG VX 7000, LG VX 6100, LG VX 3200, LG PM-225, LG VX 3300/3280, LG VX 5200, LG Migo VX1000, LG VX 8300"
			else
				itemDesc = RS("itemDesc")
			end if
			
			strline = strItemLongDetail & vbTab
			strline = strline & RS("itemid") & vbTab
			strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?source=gbase" & vbtab
			strline = strline & RS("price_our") & vbTab
			strline = strline & itemDesc & vbTab
			strline = strline & "1000" & vbTab
			strline = strline & "http://www.wirelessemporium.com/productpics/thumb/" & RS("itemPic") & vbTab
			strline = strline & BrandName & vbTab
			strline = strline & Condition & vbTab
			strline = strline & RS("PartNumber") & vbTab
			strline = strline & "Visa,MasterCard,AmericanExpress,Discover" & vbTab
			strline = strline & expDate & vbTab
			strline = strline & BrandName & " " & ModelName & " Cell Phone " & changeTypeNameSingular(RS("typeName")) & vbTab
			strline = strline & strFeatures & vbTab
			strline = strline & strCompatibility & vbtab
			strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"
			file.WriteLine strline
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub

function stripCallToAction(val)
	stripCallToAction = val
	dim a
	a = inStr(1,stripCallToAction,"why pay ridiculous prices",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"don't settle",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"order this quality",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"order this convenient",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"buy this quality",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"buy this convenient",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"order today",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"remember - wireless emporium",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"you won't find a better price",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"buy this stylish",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"buy this vertical",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"for the same product at",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	a = inStr(1,stripCallToAction,"we're confident you'll be happy",1)
	if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
	stripCallToAction = replace(stripCallToAction,"don't pay retail prices","",1,9,1)
end function

function changeTypeNameSingular(stypeName)
	select case stypeName
		case "Antennas/Parts"
			changeTypeNameSingular = "Antenna"
		case "Batteries"
			changeTypeNameSingular = "Battery"
		case "Bling Kits & Charms"
			changeTypeNameSingular = "Bling Kit"
		case "Chargers"
			changeTypeNameSingular = "Charger"
		case "Data Cable & Memory"
			changeTypeNameSingular = "Data Cable"
		case "Faceplates"
			changeTypeNameSingular = "Faceplate/Cover"
		case "Hands-Free"
			changeTypeNameSingular = "Headset/Hands Free"
		case "Holsters/Belt Clips"
			changeTypeNameSingular = "Holster"
		case "Key Pads/Flashing"
			changeTypeNameSingular = "Colorful Keypad"
		case "Leather Cases"
			changeTypeNameSingular = "Case/Pouch"
		case else
			changeTypeNameSingular = "Universal Gear"
	end select
end function
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
