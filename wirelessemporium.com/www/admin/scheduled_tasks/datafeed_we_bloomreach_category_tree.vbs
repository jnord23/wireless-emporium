set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

'username = "wirelessemporium"	
'Hostname = "ftp.bloomreach.com"
'Password = "veM49ewr"
'port = 21
'set sftp = CreateObject("Chilkat.SFtp")
'success = sftp.UnlockComponent("WIRELESSH_zHJQ3Uch8Hni")
''msgbox("1:" & success)
'success = sftp.Connect(hostname,port)
''msgbox("2:" & success)
'success = sftp.AuthenticatePw(username,password)
''msgbox("3:" & success)
'success = sftp.InitializeSftp()
''msgbox("4:" & success)

dim fs, file, filename, path, folderName
set fs = CreateObject("Scripting.FileSystemObject")
dim oXMLHTTP : set oXMLHTTP = CreateObject("Msxml2.ServerXMLHTTP.3.0")
set file = nothing

nRows = 0
filename = "bloomreach_cat_tree.txt"
folderName = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\"
imgAbsolutePath = "c:\inetpub\wwwroot\productpics\"
path = folderName & filename

if fs.FileExists(path) then fs.DeleteFile(path)

set file = fs.CreateTextFile(path)

dim SQL, RS
dim ModelName, BrandName, strItemLongDetail, strFeatures, strCompatibility, Condition, strline, itemDesc
sql	=	"select	distinct typeid category_id, typename name, '' parent_category_id, '/' + convert(varchar(10), typeid) breadCrumb, 1 sequence" & vbcrlf & _
		"	,	replace(replace(lower(dbo.formatSEO(typeNameSEO_WE)), 'cell-phone-', 'phone-'), 'cell-phones-', 'phone-') url, cast(1 as bit) as primary_category" & vbcrlf & _
		"	,	'categoryid' category_id_desc, '' parent_category_id_desc, 'categoryid' breadcrumb_desc" & vbcrlf & _
		"from	v_subtypematrix" & vbcrlf & _
		"where	typeid not in (23, 24)" & vbcrlf & _
		"order by 1"

set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

strHeadings	 = 	"category_id" & vbTab & "name" & vbTab & "parent_category_id" & vbTab & "breadcrumb" & vbTab & "sequence" & vbTab & "url" & vbTab & "primary_category" & vbTab & "category_id_desc" & vbTab & "parent_category_id_desc" & vbTab & "breadcrumb_desc"
file.WriteLine strHeadings

if not RS.eof then
	do until RS.eof
		category_id = rs("category_id")
		categoryName = rs("name")
		parent_category_id = rs("parent_category_id")
		breadcrumb = rs("breadcrumb")
		sequence = rs("sequence")
		url = rs("url")
		url = "http://www.wirelessemporium.com/" & url
		primary_category = rs("primary_category")
		category_id_desc = rs("category_id_desc")
		parent_category_id_desc = rs("parent_category_id_desc")
		breadcrumb_desc = rs("breadcrumb_desc")
		
		strLine = category_id & vbTab 			
		strLine = strLine & categoryName & vbTab
		strLine = strLine & parent_category_id & vbTab
		strLine = strLine & breadcrumb & vbTab
		strLine = strLine & sequence & vbTab 
		strLine = strLine & url & vbTab
		strLine = strLine & primary_category & vbTab
		strLine = strLine & category_id_desc & vbTab		
		strLine = strLine & parent_category_id_desc & vbTab		
		strLine = strLine & breadcrumb_desc

		file.WriteLine strLine

		rs.movenext
	loop
end if

sql	=	"select	distinct subtypeid category_id, subtypename name, typeid parent_category_id, '/' + convert(varchar(10), typeid) + '/' + convert(varchar(10), subtypeid) breadCrumb, 2 sequence" & vbcrlf & _
		"	,	replace(replace(lower(dbo.formatSEO(subTypeNameSEO_WE)), 'cell-phones-', 'phone-'), 'cell-phone-', 'phone-') url, cast(1 as bit) as primary_category" & vbcrlf & _
		"	,	'subcategoryid' category_id_desc, 'categoryid' parent_category_id_desc, '/categoryid/subcategoryid' breadcrumb_desc" & vbcrlf & _
		"from	v_subtypematrix" & vbcrlf & _
		"where	subtypeid not in (1260,1270)" & vbcrlf & _
		"order by 1"

set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if not RS.eof then
	do until RS.eof
		category_id = rs("category_id")
		categoryName = rs("name")
		parent_category_id = rs("parent_category_id")
		breadcrumb = rs("breadcrumb")
		sequence = rs("sequence")
		url = rs("url")
		url = "http://www.wirelessemporium.com/" & url
		primary_category = rs("primary_category")
		category_id_desc = rs("category_id_desc")
		parent_category_id_desc = rs("parent_category_id_desc")
		breadcrumb_desc = rs("breadcrumb_desc")
		
		strLine = category_id & vbTab 			
		strLine = strLine & categoryName & vbTab
		strLine = strLine & parent_category_id & vbTab
		strLine = strLine & breadcrumb & vbTab
		strLine = strLine & sequence & vbTab 
		strLine = strLine & url & vbTab
		strLine = strLine & primary_category & vbTab
		strLine = strLine & category_id_desc & vbTab		
		strLine = strLine & parent_category_id_desc & vbTab		
		strLine = strLine & breadcrumb_desc

		file.WriteLine strLine

		rs.movenext
	loop
end if


sql	=	"select	modelid category_id, modelName name, b.brandid parent_category_id, '/' + convert(varchar(10), b.brandid) + '/' + convert(varchar(10), a.modelid) breadCrumb, 2 sequence" & vbcrlf & _
		"	,	lower(dbo.formatSEO(b.brandname)) + '-' + lower(dbo.formatSEO(a.modelname)) + '-accessories' url, cast(1 as bit) as primary_category" & vbcrlf & _
		"	,	'modelid' category_id_desc, 'brandid' parent_category_id_desc, '/brandid/modelid' breadcrumb_desc" & vbcrlf & _
		"from	we_models a join we_brands b" & vbcrlf & _
		"	on	a.brandid = b.brandid" & vbcrlf & _
		"where	a.hidelive = 0" & vbcrlf & _
		"	and	a.modelName not like 'all model%'"
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if not RS.eof then
	do until RS.eof
		category_id = rs("category_id")
		categoryName = rs("name")
		parent_category_id = rs("parent_category_id")
		breadcrumb = rs("breadcrumb")
		sequence = rs("sequence")
		url = rs("url")
		url = "http://www.wirelessemporium.com/" & url
		primary_category = rs("primary_category")
		category_id_desc = rs("category_id_desc")
		parent_category_id_desc = rs("parent_category_id_desc")
		breadcrumb_desc = rs("breadcrumb_desc")
		
		strLine = category_id & vbTab 			
		strLine = strLine & categoryName & vbTab
		strLine = strLine & parent_category_id & vbTab
		strLine = strLine & breadcrumb & vbTab
		strLine = strLine & sequence & vbTab 
		strLine = strLine & url & vbTab
		strLine = strLine & primary_category & vbTab
		strLine = strLine & category_id_desc & vbTab		
		strLine = strLine & parent_category_id_desc & vbTab		
		strLine = strLine & breadcrumb_desc

		file.WriteLine strLine

		rs.movenext
	loop
end if


sql	=	"select	distinct c.brandid category_id, c.brandname name, a.typeid parent_category_id, '/' + convert(varchar(10), a.typeid) + '/' + convert(varchar(10), c.brandid) + '/' + convert(varchar(10), m.modelid) breadCrumb, 3 sequence" & vbcrlf & _
		"	,	a.typeName, c.brandname, m.modelname, cast(1 as bit) as primary_category" & vbcrlf & _
		"	,	'brandid' category_id_desc, 'categoryid' parent_category_id_desc, '/typeid/brandid/modelid' breadcrumb_desc" & vbcrlf & _
		"	,	m.modelid" & vbcrlf & _
		"from	v_subtypematrix a join we_items b" & vbcrlf & _
		"	on	a.subtypeid = b.subtypeid join we_brands c" & vbcrlf & _
		"	on	b.brandid = c.brandid join we_models m" & vbcrlf & _
		"	on	b.modelid = m.modelid" & vbcrlf & _
		"where	a.typeid not in (5,8,16)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0 " & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct c.brandid category_id, c.brandname name, a.typeid parent_category_id, '/' + convert(varchar(10), a.typeid) + '/' + convert(varchar(10), c.brandid) + '/' + convert(varchar(10), m.modelid) breadCrumb, 3" & vbcrlf & _
		"	,	a.typeName, c.brandname, m.modelname, cast(1 as bit) as primary_category" & vbcrlf & _
		"	,	'brandid' category_id_desc, 'categoryid' parent_category_id_desc, '/typeid/brandid/modelid' breadcrumb_desc" & vbcrlf & _
		"	,	m.modelid" & vbcrlf & _				
		"from	v_subtypematrix a join we_subRelatedItems t" & vbcrlf & _
		"	on	a.subtypeid = t.subtypeid join we_brands c" & vbcrlf & _
		"	on	t.brandid = c.brandid join we_items b" & vbcrlf & _
		"	on	t.itemid = b.itemid join we_models m" & vbcrlf & _
		"	on	t.modelid = m.modelid" & vbcrlf & _
		"where	a.typeid not in (5,8,16)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0" & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct c.brandid category_id, c.brandname name, 5 parent_category_id, '/5/' + convert(varchar(10), c.brandid) + '/' + convert(varchar(10), m.modelid) breadCrumb, 3" & vbcrlf & _
		"	,	'Handsfree Bluetooth' typeName, c.brandname, m.modelname, cast(1 as bit) as primary_category" & vbcrlf & _
		"	,	'brandid' category_id_desc, 'categoryid' parent_category_id_desc, '/typeid/brandid/modelid' breadcrumb_desc" & vbcrlf & _
		"	,	m.modelid" & vbcrlf & _		
		"from	we_brands c join we_models m" & vbcrlf & _
		"	on	c.brandid = m.brandid" & vbcrlf & _
		"where	m.handsfreetypes is not null" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"order by 1"
		
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if not RS.eof then
	do until RS.eof
		category_id = rs("category_id")
		categoryName = rs("name")
		parent_category_id = rs("parent_category_id")
		breadcrumb = rs("breadcrumb")
		sequence = rs("sequence")
		url = "http://www.wirelessemporium.com/sb-" & category_id & "-sm-" & rs("modelid") & "-sc-" & parent_category_id & "-" & formatSEO(RS("typename")) & "-" & formatSEO(RS("brandName")) & "-" & formatSEO(RS("modelName"))
		primary_category = rs("primary_category")
		category_id_desc = rs("category_id_desc")
		parent_category_id_desc = rs("parent_category_id_desc")
		breadcrumb_desc = rs("breadcrumb_desc")
		
		strLine = category_id & vbTab 			
		strLine = strLine & categoryName & vbTab
		strLine = strLine & parent_category_id & vbTab
		strLine = strLine & breadcrumb & vbTab
		strLine = strLine & sequence & vbTab 
		strLine = strLine & url & vbTab
		strLine = strLine & primary_category & vbTab
		strLine = strLine & category_id_desc & vbTab		
		strLine = strLine & parent_category_id_desc & vbTab		
		strLine = strLine & breadcrumb_desc

		file.WriteLine strLine

		rs.movenext
	loop
end if


sql	=	"select	distinct c.brandid category_id, c.brandname name, a.subtypeid parent_category_id, '/' + convert(varchar(10), a.typeid) + '/' + convert(varchar(10), a.subtypeid) + '/' + convert(varchar(10), c.brandid) + '/' + convert(varchar(10), m.modelid) breadCrumb, 4 sequence" & vbcrlf & _
		"	,	a.subtypename, c.brandname, m.modelname, cast(1 as bit) as primary_category" & vbcrlf & _
		"	,	'brandid' category_id_desc, 'subcategoryid' parent_category_id_desc, '/categoryid/subcategoryid/brandid/modelid' breadcrumb_desc" & vbcrlf & _
		"	,	m.modelid" & vbcrlf & _
		"from	v_subtypematrix a join we_items b" & vbcrlf & _
		"	on	a.subtypeid = b.subtypeid join we_brands c" & vbcrlf & _
		"	on	b.brandid = c.brandid join we_models m" & vbcrlf & _
		"	on	b.modelid = m.modelid" & vbcrlf & _
		"where	a.typeid not in (5,8,15,16,25)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0 " & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct c.brandid, c.brandname, a.subtypeid, '/' + convert(varchar(10), a.typeid) + '/' + convert(varchar(10), a.subtypeid) + '/' + convert(varchar(10), c.brandid) + '/' + convert(varchar(10), m.modelid), 4 sequence" & vbcrlf & _
		"	,	a.subtypename, c.brandname, m.modelname, cast(1 as bit) as primary_category" & vbcrlf & _
		"	,	'brandid' category_id_desc, 'subcategoryid' parent_category_id_desc, '/categoryid/subcategoryid/brandid/modelid' breadcrumb_desc" & vbcrlf & _
		"	,	m.modelid" & vbcrlf & _
		"from	v_subtypematrix a join we_subRelatedItems t" & vbcrlf & _
		"	on	a.subtypeid = t.subtypeid join we_brands c" & vbcrlf & _
		"	on	t.brandid = c.brandid join we_items b" & vbcrlf & _
		"	on	t.itemid = b.itemid join we_models m" & vbcrlf & _
		"	on	t.modelid = m.modelid" & vbcrlf & _
		"where	a.typeid not in (5,8,15,16)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0" & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct c.brandid, c.brandname, a.subtypeid, '/' + convert(varchar(10), a.typeid) + '/' + convert(varchar(10), a.subtypeid) + '/' + convert(varchar(10), c.brandid) + '/' + convert(varchar(10), m.modelid), 4 sequence" & vbcrlf & _
		"	,	a.subtypename, c.brandname, m.modelname, cast(1 as bit) as primary_category" & vbcrlf & _
		"	,	'brandid' category_id_desc, 'subcategoryid' parent_category_id_desc, '/categoryid/subcategoryid/brandid/modelid' breadcrumb_desc" & vbcrlf & _
		"	,	m.modelid" & vbcrlf & _
		"from	we_brands c join we_models m" & vbcrlf & _
		"	on	c.brandid = m.brandid cross join v_subtypematrix a" & vbcrlf & _
		"where	a.typeid = 5" & vbcrlf & _
		"	and	m.handsfreetypes is not null" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"order by 1"
		
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if not RS.eof then
	do until RS.eof
		category_id = rs("category_id")
		categoryName = rs("name")
		parent_category_id = rs("parent_category_id")
		breadcrumb = rs("breadcrumb")
		sequence = rs("sequence")
		url = "http://www.wirelessemporium.com/sb-" & category_id & "-sm-" & rs("modelid") & "-scd-" & parent_category_id & "-" & formatSEO(RS("subtypename")) & "-" & formatSEO(RS("brandName")) & "-" & formatSEO(RS("modelName"))
		primary_category = rs("primary_category")
		category_id_desc = rs("category_id_desc")
		parent_category_id_desc = rs("parent_category_id_desc")
		breadcrumb_desc = rs("breadcrumb_desc")
		
		strLine = category_id & vbTab 			
		strLine = strLine & categoryName & vbTab
		strLine = strLine & parent_category_id & vbTab
		strLine = strLine & breadcrumb & vbTab
		strLine = strLine & sequence & vbTab 
		strLine = strLine & url & vbTab
		strLine = strLine & primary_category & vbTab
		strLine = strLine & category_id_desc & vbTab		
		strLine = strLine & parent_category_id_desc & vbTab		
		strLine = strLine & breadcrumb_desc

		file.WriteLine strLine

		rs.movenext
	loop
end if
file.close()
set file = nothing

'handle = sftp.OpenFile("/" & filename,"writeOnly","createTruncate")
'If (handle = vbNullString ) Then
'    msgbox(sftp.LastErrorText)
''    response.End()
'End If
'
'success = sftp.UploadFile(handle,path)
'success = sftp.CloseHandle(handle)
		
call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
end function