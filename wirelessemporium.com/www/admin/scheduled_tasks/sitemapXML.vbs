set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim fs, file, path, nFileIdx, fileBaseName, folderName, nRows
nRows = 0
nFileIdx = 1
folderName = "C:\inetpub\wwwroot\wirelessemporium.com\www"
'fileBaseName = "sitemap"
fileBaseName = "sitemap"
fileVideoBaseName = "videoSitemap"
set fs = CreateObject("Scripting.FileSystemObject")

path = folderName & "\" & fileBaseName & nFileIdx & ".xml"
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if
set file = fs.CreateTextFile(path)
file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
file.WriteLine "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">"
file.WriteLine "<url>"
file.WriteLine vbtab & "<loc>http://www.wirelessemporium.com/</loc>"
file.WriteLine vbtab & "<priority>0.90</priority>"
file.WriteLine "</url>"

dim temp(100)
temp(1) = "/utstarcom-phone-accessories"
temp(2) = "/sony-ericsson-phone-accessories"
temp(3) = "/kyocera-phone-accessories"
temp(4) = "/lg-phone-accessories"
temp(5) = "/motorola-phone-accessories"
temp(6) = "/nextel-phone-accessories"
temp(7) = "/nokia-phone-accessories"
temp(8) = "/samsung-phone-accessories"
temp(9) = "/sanyo-phone-accessories"
temp(10) = "/siemens-phone-accessories"
temp(11) = "/blackberry-phone-accessories"
temp(12) = "/other-phone-accessories"
temp(13) = "/hp-palm-phone-accessories"
'temp(14) = "/iphone-accessories"
'temp(15) = "/ipod-ipad-accessories"
temp(14) = ""
temp(15) = ""
temp(16) = "/pantech-phone-accessories"
temp(17) = "/sidekick-phone-accessories"
temp(18) = "/htc-phone-accessories"
temp(19) = "/tablet-ereader-accessories"

temp(20) = "/phones-tablets"
temp(21) = "/phone-covers-faceplates"
temp(22) = "/phone-batteries"
temp(23) = "/phone-chargers-cables"
temp(24) = "/phone-pouches-carrying-cases"
temp(25) = "/phone-holsters-holders-mounts"
temp(26) = "/bluetooth-headsets-handsfree"
temp(27) = "/phone-screen-protectors-skins"
'temp(28) = "/cell-phone-bundle-packs"
temp(28) = ""
temp(29) = "/phone-other-accessories"

temp(30) = "/pre-paid-phones"
temp(31) = "/tablets-and-ereaders"
temp(32) = "/unlocked-phones"
temp(33) = "/mobile-broadband-or-usb-modems"
temp(34) = "/phone-design-faceplates-covers"
temp(35) = "/phone-heavy-duty-hybrid-cases"
temp(36) = "/phone-rhinestone-bling-cases"
temp(37) = "/phone-rubberized-hard-covers"
temp(38) = "/phone-silicone-cases"
temp(39) = "/phone-tpu-crystal-candy-cases"
temp(40) = "/phone-replacement-and-extended-batteries"
temp(41) = "/phone-universal-batteries"
temp(42) = "/phone-car-chargers"
temp(43) = "/phone-desktop-cradles-docks"
temp(44) = "/phone-home-chargers"
temp(45) = "/phone-spare-battery-chargers"
temp(46) = "/phone-universal-chargers"
temp(47) = "/phone-data-cables"
temp(48) = "/battery-powered-cell-phone-chargers"
temp(49) = "/phone-horizontal-cases-pouches"
temp(50) = "/phone-vertical-cases-pouches"				
temp(51) = "/sleeves-portfolios"								
temp(52) = "/phone-car-mounts-stands"
temp(53) = "/phone-holsters"
temp(54) = "/phone-bluetooth-car-kits"				
temp(55) = "/phone-bluetooth-headsets"								
temp(56) = "/phone-speakers"												
temp(57) = "/phone-wired-headsets"
temp(58) = "/decal-skin"
temp(59) = "/music-skins"
temp(60) = "/phone-screen-surface-protection"

temp(61) = "/phone-starter-kit"
temp(62) = "/phone-power-pack"
temp(63) = "/phone-dust-plugs-charms"
temp(64) = "/phone-straps-lanyards"
temp(65) = "/phone-signal-boosters"
temp(66) = "/phone-stylus-pens"
temp(67) = "/phone-memory-cards-readers"

temp(68) = "/car-2-phones-att-cingular"
temp(69) = "/car-4-phones-sprint-nextel"
temp(70) = "/car-5-phones-t-mobile"
temp(71) = "/car-6-phones-verizon"

temp(72) = "/cp-sb-17-apple-unlocked-cell-phones"
temp(73) = "/cp-sb-20-htc-unlocked-cell-phones"
temp(74) = "/cp-sb-9-samsung-unlocked-cell-phones"
temp(75) = "/cp-sb-14-blackberry-unlocked-cell-phones"
temp(76) = "/cp-sb-5-motorola-unlocked-cell-phones"
temp(77) = "/cp-sb-4-lg-unlocked-cell-phones"
temp(78) = "/cp-sb-1-utstarcom-unlocked-cell-phones"
temp(79) = "/cp-sb-2-sony-ericsson-unlocked-cell-phones"
temp(80) = "/cp-sb-6-nextel-unlocked-cell-phones"
temp(81) = "/cp-sb-7-nokia-unlocked-cell-phones"
temp(82) = "/cp-sb-15-other-unlocked-cell-phones"
temp(83) = "/cp-sb-16-hp-palm-unlocked-cell-phones"
temp(84) = "/cp-sb-18-pantech-unlocked-cell-phones"
temp(85) = "/cp-sb-19-t-mobile-sidekick-unlocked-cell-phones"

temp(86) = "/unlocked-cell-phones"
temp(87) = "/car-6-phones-unlocked"
temp(88) = "/apple-iphone-leather-carrying-cases-pouches"
'temp(89) = "/iphone-5-accessories"
temp(89) = ""
temp(90) = "/custom-phone-cases"
temp(91) = "/phone-replacement-parts"
temp(92) = "/customized-phone-cases"
temp(93) = "/custom"
temp(94) = "/samsung-galaxy-accessories"


dim a, URL
for a = 1 to 94
	if temp(a) <> "" then
		URL = temp(a)
		file.WriteLine "<url>"
		file.WriteLine vbtab & "<loc>http://www.wirelessemporium.com" & URL & "</loc>"
		file.WriteLine vbtab & "<priority>0.90</priority>"
		file.WriteLine "</url>"
	end if
next




'============================================== BRAND-MODEL
dim strBrandModelSQL, objRsBrandModel
strBrandModelSQL = 	"select	distinct a.modelid, b.brandid, b.brandname, isnull(a.modelname, '') modelname, a.linkName" & vbcrlf & _
					"from 	we_models a with (nolock) join we_brands b with (nolock) " & vbcrlf & _
					"	on 	a.brandid = b.brandid join we_items c with (nolock)" & vbcrlf & _
					"	on	a.modelid = c.modelid" & vbcrlf & _
					"where 	a.modelname is not null and a.hidelive = 0" & vbcrlf & _
					"	and a.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
					"	and (a.modelname not like 'all model%' or a.modelname not like 'universal%')" & vbcrlf & _
					"	and	c.hidelive = 0 and c.inv_qty <> 0 and c.price_our > 0" & vbcrlf
					
set objRsBrandModel	= oConn.execute(strBrandModelSQL)

do until objRsBrandModel.EOF
'	if not isnull(objRsBrandModel("linkName")) then
'		url = "accessories-for-" & formatSEO(objRsBrandModel("linkName")) & ""
'	else	'old structure
'		if objRsBrandModel("brandid") = 17 then
'			if instr(lcase(objRsBrandModel("modelname")), "iphone") > 0 then
'				url = "T-" & objRsBrandModel("modelID") & "-cell-accessories-iphone-" & formatSEO(objRsBrandModel("modelName")) & ""
'			elseif objRsBrandModel("modelid") = 940 then
'				url = "apple-ipad-accessories"
'			else
'				url = "T-" & objRsBrandModel("modelID") & "-cell-accessories-ipod-ipad-" & formatSEO(objRsBrandModel("modelName")) & ""
'			end if
'		else
'			url = "T-" & objRsBrandModel("modelID") & "-cell-accessories-" & formatSEO(objRsBrandModel("brandName")) & "-" & formatSEO(objRsBrandModel("modelName")) & ""
'		end if
'	end if

	url = formatSEO(objRsBrandModel("brandName")) & "-" & formatSEO(objRsBrandModel("modelName")) & "-accessories"
	
	call fWriteFile(url)
	objRsBrandModel.movenext
loop
set objRsBrandModel = nothing
'============================================== BRAND-MODEL END	


'============================================== category-brand START
sql	=	"select	distinct a.subtypeid, a.subtypename, c.brandid, c.brandname" & vbcrlf & _
		"from	v_subtypematrix a with (nolock) join we_items b with (nolock) " & vbcrlf & _
		"	on	a.subtypeid = b.subtypeid join we_brands c with (nolock)" & vbcrlf & _
		"	on	b.brandid = c.brandid" & vbcrlf & _
		"where	a.typeid not in (5,8,15,16)" & vbcrlf & _
		"	and c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0" & vbcrlf & _
		"union " & vbcrlf & _
		"select	distinct a.subtypeid, a.subtypename, c.brandid, c.brandname" & vbcrlf & _
		"from	v_subtypematrix a with (nolock) inner hash join we_subRelatedItems t with (nolock) " & vbcrlf & _
		"	on	a.subtypeid = t.subtypeid join we_brands c with (nolock)" & vbcrlf & _
		"	on	t.brandid = c.brandid join we_items b with (nolock)" & vbcrlf & _
		"	on	t.itemid = b.itemid" & vbcrlf & _
		"where	a.typeid not in (5,8,15,16)" & vbcrlf & _
		"	and c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0" & vbcrlf & _
		"union " & vbcrlf & _
		"select	distinct a.subtypeid, a.subtypename, c.brandid, c.brandname" & vbcrlf & _
		"from	v_subtypematrix a with (nolock) cross join we_brands c with (nolock)" & vbcrlf & _
		"where	a.subtypeid in (1100, 1140, 1150, 1380, 1190, 1270)" & vbcrlf & _
		"	and c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"union " & vbcrlf & _
		"select	distinct a.subtypeid, a.subtypename, c.brandid, c.brandname" & vbcrlf & _
		"from	v_subtypematrix a with (nolock) cross join we_brands c with (nolock)" & vbcrlf & _
		"where	a.typeid = 5" & vbcrlf & _
		"	and c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)"
Set RS = CreateObject("ADODB.Recordset")
set RS = oConn.execute(sql)
do until RS.EOF
	if RS("brandid") = 17 then
		url = "sc-" & RS("subtypeid") & "-sb-" & RS("brandid") & "-" & formatSEO(RS("brandname")) & "-iphone-" & formatSEO(RS("subtypename")) & ""
		call fWriteFile(url)
		url = "sc-" & RS("subtypeid") & "-sb-" & RS("brandid") & "-" & formatSEO(RS("brandname")) & "-ipod-ipad-" & formatSEO(RS("subtypename")) & ""
	else
		url = "sc-" & RS("subtypeid") & "-sb-" & RS("brandid") & "-" & formatSEO(RS("brandname")) & "-" & formatSEO(RS("subtypename")) & ""
	end if

	call fWriteFile(url)
	RS.movenext
loop
set RS = nothing		
'============================================== category-brand END

'============================================== brand-model-category
sql	=	"select	distinct a.typeid, a.typeName, c.brandid, c.brandname, m.modelid, m.modelname" & vbcrlf & _
		"from	v_subtypematrix a with (nolock) join we_items b with (nolock)" & vbcrlf & _
		"	on	a.subtypeid = b.subtypeid join we_brands c with (nolock)" & vbcrlf & _
		"	on	b.brandid = c.brandid join we_models m with (nolock)" & vbcrlf & _
		"	on	b.modelid = m.modelid" & vbcrlf & _
		"where	a.typeid not in (5,8,16)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0 " & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct a.typeid, a.typeName, c.brandid, c.brandname, m.modelid, m.modelname" & vbcrlf & _
		"from	v_subtypematrix a with (nolock) join we_subRelatedItems t with (nolock)" & vbcrlf & _
		"	on	a.subtypeid = t.subtypeid join we_brands c with (nolock)" & vbcrlf & _
		"	on	t.brandid = c.brandid join we_items b with (nolock)" & vbcrlf & _
		"	on	t.itemid = b.itemid join we_models m with (nolock)" & vbcrlf & _
		"	on	t.modelid = m.modelid" & vbcrlf & _
		"where	a.typeid not in (5,8,16)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0" & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct 5 typeid, 'Handsfree Bluetooth' typename, c.brandid, c.brandname, m.modelid, m.modelname" & vbcrlf & _
		"from	we_brands c with (nolock) join we_models m with (nolock)" & vbcrlf & _
		"	on	c.brandid = m.brandid" & vbcrlf & _
		"where	m.handsfreetypes is not null" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"order by 1, 3" & vbcrlf
		
Set RS = CreateObject("ADODB.Recordset")	
set RS = oConn.execute(sql)	
do until RS.EOF
	if RS("typeID") = 1270 then
		url = "sb-" & RS("brandID") & "-sm-" & RS("modelID") & "-sc-" & RS("typeID") & "-" & formatSEO(RS("brandName")) & "-" & formatSEO(RS("modelName")) & "-" & formatSEO(RS("typeName")) & "-genre"
	else
		url = "sb-" & RS("brandID") & "-sm-" & RS("modelID") & "-sc-" & RS("typeID") & "-" & formatSEO(RS("typeName")) & "-" & formatSEO(RS("brandName")) & "-" & formatSEO(RS("modelName")) & ""		
	end if
	
	call fWriteFile(url)
	RS.movenext
loop
set RS = nothing
'============================================== brand-model-category end


'============================================== brand-category-brand-detail 
sql	=	"select	distinct a.subtypeid, a.subtypename, c.brandid, c.brandname, m.modelid, m.modelname" & vbcrlf & _
		"from	v_subtypematrix a with (nolock) join we_items b with (nolock)" & vbcrlf & _
		"	on	a.subtypeid = b.subtypeid join we_brands c with (nolock)" & vbcrlf & _
		"	on	b.brandid = c.brandid join we_models m with (nolock)" & vbcrlf & _
		"	on	b.modelid = m.modelid" & vbcrlf & _
		"where	a.typeid not in (5,8,15,16,25)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0 " & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct a.subtypeid, a.subtypename, c.brandid, c.brandname, m.modelid, m.modelname" & vbcrlf & _
		"from	v_subtypematrix a with (nolock) join we_subRelatedItems t with (nolock)" & vbcrlf & _
		"	on	a.subtypeid = t.subtypeid join we_brands c with (nolock)" & vbcrlf & _
		"	on	t.brandid = c.brandid join we_items b with (nolock)" & vbcrlf & _
		"	on	t.itemid = b.itemid join we_models m with (nolock)" & vbcrlf & _
		"	on	t.modelid = m.modelid" & vbcrlf & _
		"where	a.typeid not in (5,8,15,16)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0" & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct v.subtypeid, v.subtypename, c.brandid, c.brandname, m.modelid, m.modelname" & vbcrlf & _
		"from	we_brands c with (nolock) join we_models m with (nolock)" & vbcrlf & _
		"	on	c.brandid = m.brandid cross join v_subtypematrix v with (nolock)" & vbcrlf & _
		"where	v.typeid = 5" & vbcrlf & _
		"	and	m.handsfreetypes is not null" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"order by 1, 3" & vbcrlf
		
Set RS = CreateObject("ADODB.Recordset")	
set RS = oConn.execute(sql)	
do until RS.EOF
	url = "sb-" & RS("brandID") & "-sm-" & RS("modelID") & "-scd-" & RS("subtypeid") & "-" & formatSEO(RS("subtypename")) & "-" & formatSEO(RS("brandName")) & "-" & formatSEO(RS("modelName")) & ""
	call fWriteFile(url)
	RS.movenext
loop
set RS = nothing
'============================================== brand-category-brand-detail END

'============================================== product page START
dim pSQL, objRsProduct
pSQL	=	"select	a.itemid, a.itemdesc " & vbcrlf & _
			"from	we_items a with (nolock) join we_models b with (nolock) " & vbcrlf & _
			"	on	a.modelid = b.modelid join (select distinct itemid from we_itemssiteready with (nolock)) c" & vbcrlf & _			
			"	on	a.itemid = c.itemid" & vbcrlf & _
			"where	a.hidelive = 0 and a.inv_qty <> 0  " & vbcrlf & _
			"	and a.price_our > 0 " & vbcrlf & _
			"	and b.hidelive = 0 " & vbcrlf & _			
			"order by 1"
			
set objRsProduct = oConn.execute(pSQL)

do until objRsProduct.EOF
	url = "p-" & objRsProduct("itemID") & "-" & formatSEO(objRsProduct("itemDesc")) & ""

	call fWriteFile(url)

	objRsProduct.movenext
loop
set objRsProduct = nothing
'============================================== product page END


'============================================== video sitemap
nRows = 50000
pSQL	=	"exec sp_videoSitemap"
set rsVideo = oConn.execute(pSQL)

do until rsVideo.EOF
	tItemID = rsVideo("itemid")
	itemdesc = "<![CDATA[" & rsVideo("itemdesc") & "]]>"
	videoURL = "<![CDATA[" & rsVideo("video_url") & "]]>"
	itempic = rsVideo("itempic")
	categoryName = "<![CDATA[" & rsVideo("typeName") & "]]>"
	itemlongdetail = rsVideo("itemlongdetail")
	if len(itemlongdetail) > 2045 then itemlongdetail = left(itemlongdetail, 2045) & "..."
	itemlongdetail = "<![CDATA[" & itemlongdetail & "]]>"
	
	url = "p-" & tItemID & "-" & formatSEO(rsVideo("itemdesc")) & ""

	call fWriteFileVideo(url,videoURL,itempic,itemdesc,itemlongdetail,categoryName)

	rsVideo.movenext
loop
set rsVideo = nothing
'============================================== video sitemap END


file.WriteLine "</urlset>"
file.close()
set file = nothing











'============================================== index site map START
path = folderName & "\" & fileBaseName & ".xml"
'If the file already exists, delete it.
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if
set file = fs.CreateTextFile(path)
file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
file.WriteLine "<sitemapindex xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"">"

for i=1 to nFileIdx
	file.WriteLine "<sitemap>"


	file.WriteLine vbtab & "<loc>http://www.wirelessemporium.com/" & fileBaseName & i & ".xml</loc>"
	file.WriteLine vbtab & "<lastmod>" & RFC3339(now) & "</lastmod>"
	file.WriteLine "</sitemap>"
next

'file.WriteLine "<sitemap>"
'file.WriteLine vbtab & "<loc>http://www.wirelessemporium.com/trpdVideositemap.xml</loc>"
'file.WriteLine vbtab & "<lastmod>" & RFC3339(now) & "</lastmod>"
'file.WriteLine "</sitemap>"

file.WriteLine "</sitemapindex>"
file.close()
set file = nothing
'============================================== index site map END

		

sub fWriteFile(pStrUrl)
	nRows = nRows + 1
	
	if nRows > 45000 then
		nFileIdx = nFileIdx + 1
		nRows = 0
		file.WriteLine "</urlset>"
		file.close()
		set file = nothing
		
		path = folderName & "\" & fileBaseName & nFileIdx & ".xml"
		if fs.FileExists(path) then
			fs.DeleteFile(path)
		end if
		set file = fs.CreateTextFile(path)
		file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
		file.WriteLine "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">"
	end if
	
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://www.wirelessemporium.com/" & pStrUrl & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"	
end sub

sub fWriteFileVideo(pStrUrl,videoURL,itempic,itemdesc,itemlongdetail,categoryName)
	nRows = nRows + 1
	
	if nRows > 45000 then
		nFileIdx = nFileIdx + 1
		nRows = 0
		file.WriteLine "</urlset>"
		file.close()
		set file = nothing
		
		path = folderName & "\" & fileBaseName & nFileIdx & ".xml"
		if fs.FileExists(path) then
			fs.DeleteFile(path)
		end if
		set file = fs.CreateTextFile(path)

		file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
		file.WriteLine "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:video=""http://www.google.com/schemas/sitemap-video/1.1"">"
	end if
	
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://www.wirelessemporium.com/" & pStrUrl & "</loc>"
	file.WriteLine vbtab & "<video:video>"
	file.WriteLine vbtab & vbtab & "<video:content_loc>" & videoURL & "</video:content_loc>"
	file.WriteLine vbtab & vbtab & "<video:player_loc allow_embed=""yes"">" & videoURL & "</video:player_loc>"
	file.WriteLine vbtab & vbtab & "<video:thumbnail_loc>http://www.wirelessemporium.com/productpics/big/" & itempic & "</video:thumbnail_loc>"
	file.WriteLine vbtab & vbtab & "<video:title>" & itemdesc & "</video:title>"
	file.WriteLine vbtab & vbtab & "<video:description>" & itemlongdetail & "</video:description>"
	file.WriteLine vbtab & vbtab & "<video:category>" & categoryName & "</video:category>"
	file.WriteLine vbtab & "</video:video>"
	file.WriteLine "</url>"	
end sub


function RFC3339(myDate)
	dim myDays, myMonth, myYear, myHours, myMonths, mySeconds
	myDate = dateAdd("h",8,myDate)
	myDays = zeroPad(day(myDate),2)
	myMonth = zeroPad(month(myDate),2)
	myYear = year(myDate)
	myHours = zeroPad(hour(myDate),2)
	myMinutes = zeroPad(minute(myDate),2)
	mySeconds = zeroPad(second(myDate),2)
	RFC3339 = myYear & "-" & myMonth & "-" & myDays & "T" & myHours & ":" & myMinutes & ":" & mySeconds & "Z"
end function
 
function zeroPad(m,t)
	zeroPad = String(t-Len(m),"0") & m
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
