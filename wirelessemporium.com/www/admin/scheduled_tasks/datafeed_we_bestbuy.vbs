set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
filename = "Bestbuy - New SKU.txt"
path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename
if fs.FileExists(path) then fs.DeleteFile(path)

set ofsAtt = fs.OpenTextFile("c:\inetpub\wwwroot\wirelessemporium.com\www\xmlFiles\Bestbuy\bestbuy_attribute_spec.txt", 1)
strHeader = ofsAtt.readline
arrAttributeHeader = split(strHeader, vbTab)

attColorLookup = "Beige,Black,Blue,Brown,Burgundy,Clear,Gold,Green,Grey,Off White,Orange,Pink,Purple,Red,Silver,White,Yellow,Bronze,Tan,Metallic,Plaid"
attDataCap = "2G,3G,4G,5G"
attEnCert = "Energy Star,EPEAT Bronze,EPEAT Gold,EPEAT Silver,RoHS,TCO'92,TCO'95,TCO'99,TCO'01,TCO'03,TCO'04,TCO'05,TCO'06,TCO'07"
attInterfaceType = "Bluetooth,DVI,FireWire,HDMI,S-Video,Audio Line In,Headphone Output,iPod Connector,USB,Microphone Input,Composite Video,VGA,SCART,Component Video,D-Terminal,SDI,ADC,DVI-D,DVI-I,HDTV,NTSC,PAL,RGB (Analog),SECAM,TV (antenna/cable),Parallel,Ethernet,Serial"
attPhoneCamType = "< 1 MP,1 MP - 2 MP,3 MP - 4 MP,> 5 MP"
attPhoneCelBrand = "GSM,CDMA,WCDMA"
attPhoneFeatures = "Email,Web Browser,GPS,Music Player,Video Player,Video Call,Speakerphone,FM Radio"
attPhoneSlotType = "MMC,SD Card,miniSD Card,microSD Card,Memory Stick Duo,Memory Stick Micro"
attPhoneStyleType = "Flip,Candy Bar,Slide,Swivel"
attOS = "BlackBerry,iPhone,Windows,Android,Palm,Symbian,Maemo"
attOS2 = "BlackBerry OS,iPhone OS,Windows OS,Android OS,Palm OS,Symbian OS,Maemo OS"

set file = fs.CreateTextFile(path)
dim SQL, RS

sql	=	"exec getFeed_Bestbuy"

set rs = CreateObject("ADODB.Recordset")
rs.open SQL, oConn, 0, 1
if not rs.eof then
	'core columns
	headers = "seller-id" & vbtab & "gtin" & vbtab & "isbn" & vbtab & "mfg-name" & vbtab & "mfg-part-number" & vbtab & "asin" & vbtab & "seller-sku" & vbtab & "title" & vbtab & "description" & vbtab & "main-image" & vbtab & "additional-images" & vbtab & "weight" & vbtab & "features" & vbtab & "listing-price" & vbtab & "msrp" & vbtab & "category-id" & vbtab & "keywords" & vbtab & "product-set-id"
	for i=0 to ubound(arrAttributeHeader)
		headers = headers & vbTab & arrAttributeHeader(i)
	next
	file.writeline headers
	
	do until RS.eof
		itemid = rs("itemid")
		typeid = rs("typeid")
		mpn = rs("mpn")
		itemdesc = replace(rs("itemdesc"), vbTab, " ")
		itemlongdetails = replace(replace(replace(rs("itemlongdetail"), vbcrlf, " "), vbtab, " "), " FREE!", " less!")
		upccode = rs("upccode")
		itempic = rs("itempic")
		price_retail = rs("price_retail")
		price_our = rs("price_our")
		price_buy = rs("price_buy")
		inv_qty = rs("inv_qty")
		features = bulletList(rs("Bullet1")) & bulletList(rs("Bullet2")) & bulletList(rs("Bullet3")) & bulletList(rs("Bullet4")) & bulletList(rs("Bullet5")) & bulletList(rs("Bullet6"))
		if right(features,1) = "|" then features = left(features,len(features)-1)
		if features <> "" then features = replace(features, vbTab, " ")
		compatibility = rs("compatibility")
		if len(compatibility) > 230 then
			compatibility = left(compatibility, 226) & " ..."
		end if
		packageContents = rs("packageContents")
		if len(packageContents) > 230 then
			packageContents = left(packageContents, 226) & " ..."
		end if
		brandName = rs("brandName")
		modelName = rs("modelName")
		tempValue = rs("temp")
		strTypeName = rs("typename")
		strSubTypeName = rs("subtypename")
		if strSubTypeName = "" then
			keywords = left(strTypeName, 40) & "|" & left(brandName, 40) & "|" & left(modelName, 40) & "|" & left(replace(replace(replace(compatibility, ", ", "|"), " ,", "|"), ",", "|"), 40)
		else
			keywords = left(strTypeName, 40) & "|" & left(strSubTypeName, 40) & "|" & left(brandName, 40) & "|" & left(modelName, 40) & "|" & left(replace(replace(replace(compatibility, ", ", "|"), " ,", "|"), ",", "|"), 40)
		end if
		if keywords <> "" then 
			keywords = replace(keywords, "||", "|")
			if right(keywords,1) = "|" then keywords = left(keywords, len(keywords)-1)
			if len(keywords) > 249 then keywords = left(keywords, 245) & " ..."
		end if
		categoryID = rs("categoryid")
		mfgName = rs("mfgName")

		AddlImages = ""
		for iCount = 0 to 3
			imgPath = "d:\productpics\AltViews\" & replace(itempic,".jpg","-" & iCount & ".jpg")
			if fs.FileExists(imgPath) then AddlImages = AddlImages & replace(replace(imgPath,"d:","http://www.wirelessemporium.com"),"\","/") & "|"
			imgPath = "d:\productpics\AltViews\" & replace(itempic,".jpg","-" & iCount & ".gif")
			if fs.FileExists(imgPath) then AddlImages = AddlImages & replace(replace(imgPath,"d:","http://www.wirelessemporium.com"),"\","/") & "|"
			imgPath = "d:\productpics\AltViews\" & replace(itempic,".gif","-" & iCount & ".jpg")
			if fs.FileExists(imgPath) then AddlImages = AddlImages & replace(replace(imgPath,"d:","http://www.wirelessemporium.com"),"\","/") & "|"
			imgPath = "d:\productpics\AltViews\" & replace(itempic,".gif","-" & iCount & ".gif")
			if fs.FileExists(imgPath) then AddlImages = AddlImages & replace(replace(imgPath,"d:","http://www.wirelessemporium.com"),"\","/") & "|"
		next
		if right(AddlImages,1) = "|" then AddlImages = left(AddlImages,len(AddlImages)-1)
		
		if typeid = 16 then
			weight = "2"
			features = features & "|COMPATIBILITY: " & compatibility & "|WHAT IS INCLUDED: " & packageContents
			features = replace(features,vbcrlf," ")
			itemlongdetails = itemlongdetails & " Wireless Emporium offers direct replacement and unlocked Cell/PDA/Smart Phones to you WITHOUT ANY CONTRACTS! Order your replacement or unlocked Cell/PDA/Smart Phones today!"
		else
			features = features & "|COMPATIBILITY: " & compatibility
			features = replace(features,vbcrlf," ")		
			weight = "0.3"
		end if
		
		if features <> "" then
			features = replace(replace(features, "|||", "|"), "||", "|")
			if len(features) > 7900 then features = left(features, 7900) & " ..."
		end if
		
		MfgProductIdentifier = "WE" & itemid & mpn
		MfgProductIdentifier = replace(MfgProductIdentifier,"-","",1,2)		
		
		'===================================== CORE
		strline = "11704981" & vbtab								'seller-id
		strline = strline & upccode & vbtab					'gtin
		strline = strline & vbtab									'isbn
		strline = strline & mfgName & vbtab							'mfg-name
		strline = strline & MfgProductIdentifier & vbtab			'mfg-part-number
		strline = strline & vbtab									'asin
		strline = strline & mpn & "-" & itemid & vbtab				'seller-sku
		strline = strline & itemdesc & vbtab						'title
		strline = strline & itemlongdetails & vbtab					'description
		strline = strline & "http://www.wirelessemporium.com/productpics/big/" & itempic & vbtab	'main-image
		strline = strline & addlImages & vbtab						'additional-images
		strline = strline & weight & vbtab							'weight
		strline = strline & features & vbtab						'features
		if upccode = "097738482895" then
			strline = strline & "9.50" & vbtab						'listing-price
		else
			strline = strline & price_our & vbtab						'listing-price
		end if
		strline = strline & price_retail & vbtab					'msrp
		strline = strline & categoryID & vbTab						'category-id		
		strline = strline & keywords & vbtab						'keywords
		strline = strline & ""										'product-set-id
		
		'===================================== ATTRIBUTES
		strCarrier = "" 
		strColor = ""
		strColorClass = ""
		strDataCap = ""
		strEnCert = ""
		strInterfaceType = ""
		strPhoneCamType = ""
		strPhoneCelBrand = ""
		strPhoneFeature = ""
		strPhoneSlotType = ""
		strPhoneStyleType = ""
		strProductLine = ""
		strOS = ""
		
		fullFeatures = itemdesc & " " & itemlongdetails & " " & features
		
		select case categoryID
			case 5680 'accessories
				strColor = replace(getAttributeValues(attColorLookup, itemdesc, "Black"), "^", "/")
				strColorClass = getAttributeValues(attColorLookup, itemdesc, "Black") 
			case 5681 'carrying cases
				strColor = replace(getAttributeValues(attColorLookup, itemdesc, "Black"), "^", "/")
				strColorClass = getAttributeValues(attColorLookup, itemdesc, "Black") 
			case 5682 'cell phone batteries
				strColor = replace(getAttributeValues(attColorLookup, itemdesc, "Black"), "^", "/")
				strColorClass = getAttributeValues(attColorLookup, itemdesc, "Black") 
				strEnCert = getAttributeValues(attEnCert, fullFeatures, "Energy Star")
				strProductLine = compatibility
			case 5683 'cell phones
				strCarrier = "AT&T^Verizon Wireless^T-Mobile^Sprint/Nextel^Virgin Mobile"
				strColor = replace(getAttributeValues(attColorLookup, itemdesc, "Black"), "^", "/")
				strColorClass = getAttributeValues(attColorLookup, itemdesc, "Black") 
				strDataCap = getAttributeValues(attDataCap, fullFeatures, "3G")
				strEnCert = getAttributeValues(attEnCert, fullFeatures, "Energy Star")
				strInterfaceType = getAttributeValues(attInterfaceType, fullFeatures, "Headphone Output")
				strPhoneCamType = getAttributeValues(attPhoneCamType, fullFeatures, "> 5 MP")
				strPhoneCelBrand = getAttributeValues(attPhoneCelBrand, fullFeatures, "GSM")
				strPhoneFeature = getAttributeValues(attPhoneFeatures, fullFeatures, "Speakerphone")
				strPhoneSlotType = getAttributeValues(attPhoneSlotType, fullFeatures, "MMC")
				strPhoneStyleType = getAttributeValues(attPhoneStyleType, fullFeatures, "Candy Bar")
				strProductLine = compatibility
				strOS = getOSValues(attOS, attOS2, fullFeatures, "Windows OS")
			case 5684 'chargers
				strColor = replace(getAttributeValues(attColorLookup, itemdesc, "Black"), "^", "/")
				strColorClass = getAttributeValues(attColorLookup, itemdesc, "Black") 
				strEnCert = getAttributeValues(attEnCert, fullFeatures, "Energy Star")
				strProductLine = compatibility
			case 5685 'data connectivity
				strColor = replace(getAttributeValues(attColorLookup, itemdesc, "Black"), "^", "/")
				strColorClass = getAttributeValues(attColorLookup, itemdesc, "Black") 
			case 5686 'headsets & hands free
				strColor = replace(getAttributeValues(attColorLookup, itemdesc, "Black"), "^", "/")
				strColorClass = getAttributeValues(attColorLookup, itemdesc, "Black") 
				strEnCert = getAttributeValues(attEnCert, fullFeatures, "Energy Star")
				strProductLine = compatibility
		end select		
		
		dicAttribute = null
		set dicAttribute = CreateObject("Scripting.Dictionary")
		dicAttribute.CompareMode = vbTextCompare	
		dicAttribute.Add "Carrier", strCarrier
		dicAttribute.Add "Color", strColor
		dicAttribute.Add "Color Class", strColorClass
		dicAttribute.Add "Data Capabilities", strDataCap
		dicAttribute.Add "Environmental Certification", strEnCert
		dicAttribute.Add "Interface Type", strInterfaceType
		dicAttribute.Add "Phone Camera Type", strPhoneCamType
		dicAttribute.Add "Phone Cellular Band", strPhoneCelBrand
		dicAttribute.Add "Phone Features", strPhoneFeature
		dicAttribute.Add "Phone Slot Type", strPhoneSlotType
		dicAttribute.Add "Phone Style Type", strPhoneStyleType
		dicAttribute.Add "Product Line", strProductLine
		dicAttribute.Add "Smartphone OS", strOS

		strAttribute = ""
		for i=0 to ubound(arrAttributeHeader)
			if dicAttribute.Exists(arrAttributeHeader(i)) then
				if dicAttribute.Item(arrAttributeHeader(i)) <> "" then
					strline = strline & vbTab & dicAttribute.Item(arrAttributeHeader(i))
				else
					strline = strline & vbTab
				end if
			else
				strline = strline & vbTab
			end if
		next
		
		file.writeline strline
		rs.movenext
	loop
end if
rs.close
set rs = nothing
file.close()

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub

function bulletList(val)
	if isnull(val) or len(val) <= 0 then
		bulletList = ""
	else
		if len(val) > 249 then
			val = left(val, 245) & " ..."
		end if
		bulletList = val & "|"
	end if
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function

function getOSValues(byref values, byref values2, byref val, defaultVal)
	arrValues = split(values, ",")
	arrValues2 = split(values2, ",")
	retValue = ""
	
	for i=0 to ubound(arrValues)
		if instr(cstr(val), cstr(arrValues(i))) > 0 then
			retValue = arrValues2(i)
			exit for
		end if
	next
	if retValue = "" then retValue = defaultVal
	getOSValues = retValue
end function

function getAttributeValues(byref values, byref val, defaultVal)
	arrValues = split(values, ",")
	retValue = ""
	
	for i=0 to ubound(arrValues)
		if instr(cstr(val), cstr(arrValues(i))) > 0 then
			if retValue = "" then
				retValue = arrValues(i)
			else
				retValue = retValue & "^" & arrValues(i)
			end if
		end if
	next
	if retValue = "" then retValue = defaultVal
	getAttributeValues = retValue
end function