set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim fs, file, filename, path, folderName
set fs = CreateObject("Scripting.FileSystemObject")

fileBaseName = "wegooglebase"
folderName = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\"
path = folderName & "\" & fileBaseName & ".txt"
if fs.FileExists(path) then fs.DeleteFile(path)
set file = fs.CreateTextFile(path)

dim expDate, myMonth, myDay, myYear
expDate = dateAdd("d",21,date)
myMonth = month(expDate)
if myMonth < 10 then myMonth = "0" & myMonth
myDay = day(expDate)
if myDay < 10 then myDay = "0" & myDay
if myHour < 10 then myHour = "0" & myHour
myYear = year(expDate)
expDate = myYear & "-" & myMonth & "-" & myDay

dim SQL, RS
dim ModelName, BrandName, strItemLongDetail, strFeatures, strCompatibility, Condition, strline, itemDesc
'SQL = "SELECT A.itemID, A.PartNumber, A.itemDesc, A.itemDesc_Google, A.itemPic, A.price_Our, A.inv_qty, convert(varchar(8000), A.itemLongDetail) itemLongDetail,"
'SQL = SQL & " A.COMPATIBILITY,"
'SQL = SQL & " A.BULLET1, A.BULLET2, A.BULLET3, A.BULLET4, A.BULLET5, A.BULLET6, A.BULLET7, A.BULLET8, A.BULLET9, A.BULLET10,"
'SQL = SQL & " B.brandName, C.TypeName, D.ModelName"
'SQL = SQL & " FROM ((we_Items A INNER JOIN we_brands B ON A.brandID=B.brandID)"
'SQL = SQL & " INNER JOIN we_types C ON A.typeID=C.typeID)"
'SQL = SQL & " INNER JOIN we_models D ON A.modelID=D.modelID"
'SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty > 0 AND A.price_Our > 0"
'SQL = SQL & " AND A.itemDesc NOT IN (SELECT itemDesc FROM we_Items GROUP BY itemDesc HAVING (Count(itemDesc) > 1))"
'SQL = SQL & " AND A.PartNumber NOT IN ('CC1-APL-IPOD-01','CC1-APL-IPOD-02')"
'SQL = SQL & " ORDER BY A.itemID"

sql	=	"select	a.itemid, a.partnumber, a.itemdesc, a.itemdesc_google, a.itempic, a.price_our, a.inv_qty, isnull(a.itemweight, 2) itemweight, convert(varchar(8000), a.itemlongdetail) itemlongdetail" & vbcrlf & _
		"	,	convert(varchar(8000), a.compatibility) compatibility, a.bullet1, a.bullet2, a.bullet3, a.bullet4, a.bullet5, a.bullet6, a.bullet7, a.bullet8, a.bullet9, a.bullet10" & vbcrlf & _
		"	,	convert(varchar(100), isnull(nullif(b.brandname, ''), 'Universal')) brandName, c.typename, isnull(nullif(d.modelname, ''), 'Universal') modelname" & vbcrlf & _
		"from	we_items a join we_brands b " & vbcrlf & _
		"	on	a.brandid=b.brandid join we_types c " & vbcrlf & _
		"	on	a.typeid=c.typeid join we_models d " & vbcrlf & _
		"	on	a.modelid=d.modelid" & vbcrlf & _
		"where	a.hidelive = 0 and a.inv_qty > 0 and a.price_our > 0" & vbcrlf & _
		"	and a.itemdesc not in (select itemdesc from we_items group by itemdesc having (count(itemdesc) > 1))" & vbcrlf & _
		"	and a.partnumber not in ('cc1-apl-ipod-01','cc1-apl-ipod-02')" & vbcrlf & _
		"	and d.isTablet = 0" & vbcrlf & _
		"order by a.itemid" & vbcrlf
		
set RS = CreateObject("ADODB.Recordset")
set RS = oConn.execute(sql)

if not RS.eof then
	file.WriteLine "description" & vbTab & "id" & vbTab & "link" & vbTab & "price" & vbTab & "title" & vbTab & "quantity" & vbTab & "shipping_weight" & vbTab & "image_link" & vbTab & "brand" & vbTab & "condition" & vbTab & "mpn" & vbTab & "payment_accepted" & vbTab & "expiration_date" & vbTab & "product_type" & vbTab & "feature" & vbTab & "c:Compatible_With:String" & vbTab & "c:GoogleAffiliateNetworkProductURL:String"
	do until RS.eof
		ModelName = RS("modelName")
		BrandName = RS("brandName")

		if not isnull(RS("itemLongDetail")) then
			strItemLongDetail = stripCallToAction(RS("itemLongDetail"))
			strItemLongDetail = replace(replace(strItemLongDetail,vbCrLf," "),vbTab," ")
			strItemLongDetail = replace(strItemLongDetail,"<a href='/downloads/","<a href='http://www.wirelessemporium.com/downloads/")
		else
			strItemLongDetail = ""
		end if
		
		strFeatures = "<ul>"
		if not isNull(RS("BULLET1")) and RS("BULLET1") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET1") & "</li>"
		if not isNull(RS("BULLET2")) and RS("BULLET2") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET2") & "</li>"
		if not isNull(RS("BULLET3")) and RS("BULLET3") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET3") & "</li>"
		if not isNull(RS("BULLET4")) and RS("BULLET4") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET4") & "</li>"
		if not isNull(RS("BULLET5")) and RS("BULLET5") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET5") & "</li>"
		if not isNull(RS("BULLET6")) and RS("BULLET6") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET6") & "</li>"
		if not isNull(RS("BULLET7")) and RS("BULLET7") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET7") & "</li>"
		if not isNull(RS("BULLET8")) and RS("BULLET8") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET8") & "</li>"
		if not isNull(RS("BULLET9")) and RS("BULLET9") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET9") & "</li>"
		if not isNull(RS("BULLET10")) and RS("BULLET10") <> "" then strFeatures = strFeatures & "<li>" & RS("BULLET10") & "</li>"
		strFeatures = strFeatures & "</ul>"
		strFeatures = replace(strFeatures,vbTab," ")
		if strFeatures = "<ul></ul>" then strFeatures = ""
		
		if not isNull(RS("COMPATIBILITY")) then
			strCompatibility = replace(RS("COMPATIBILITY"),vbCrLf," ")
		else
			strCompatibility = ""
		end if
		
		if inStr(Lcase(strItemLongDetail),"refurbished") > 0 or inStr(Lcase(RS("itemDesc")),"refurbished") > 0 or inStr(Lcase(strFeatures),"refurbished") > 0 then
			Condition = "refurbished"
		else
			Condition = "new"
		end if
		
		if not isNull(RS("itemDesc_Google")) then
			itemDesc = RS("itemDesc_Google")
		elseif RS("itemid") = 30626 then
			itemDesc = "Stylish EVA Pouch for Samsung Samsung Solstice SGH-A887, Samsung SGH-F480, Samsung SGH-U600, Samsung SGH-A707 SYNC, Sony Ericsson W980, Sony Ericsson W518a, Samsung Highlight SGH-T749, LG Venus VX8800, LG VX 8600 / AX 8600, LG enV2 VX9100, Blackberry Pearl Flip 8220/8230, HTC Shadow, LG Banter AX265/UX265, LG CF360, LG env3 VX9200, HTC 3125/Smartflip/8500/Star Trek, LG KF510, LG KG300, LG Muziq LX-570/UX/AX-565, LG Neon GT365, LG Versa VX9600, Motorola Evoke QA4, Samsung Gravity T459, Samsung Memoir T929, Motorola RAZR MAXX Ve, Motorola Rival A455, Motorola ROKR E2, Motorola V3c Razr, Motorola W220, Motorola W377, Nokia 5610, Pantech Breeze C520, Samsung A900, Samsung SGH-D807, Samsung Knack U310, Samsung SCH-U700 Gleam, Samsung SGH-A517, Samsung SGH-G600, Samsung SGH-T439, Motorola Entice W766, Samsung Intensity SCH-U450, Samsung SPH-M330, Motorola i856, Sony Ericsson F305, HTC Pure, Blackberry Pearl 8110/8120/8130, HTC Touch Diamond (CDMA), HTC Touch Diamond (GSM), HTC Touch Dual, LG KB770, LG Xenon GR500, Motorola Krave ZN4, Motorola Renegade V950, Motorola RIZR Z8, Palm Treo 680, Samsung Behold T919, Samsung Eternity SGH-A867, Samsung Glyde SCH-U940, Samsung Propel Pro SGH-i627, LG Chocolate VX8550, LG KP220, LG KS500, LG Shine CU720, Motorola Adventure V750, Samsung S5230 Star, Nokia 2680, Nokia 6670, Samsung SGH-T659, LG Bliss UX700, Samsung SGH-T629, Samsung SGH-T639, Sony Ericsson W380a, Sony Ericsson W395, Samsung Comeback T559, Motorola V3 Razr, Motorola V3m Razr, Motorola V3xx Razr, Motorola W755, Nokia 2610, Nokia 6555, Nokia 6560, Samsung A777, Samsung FlipShot SCH-U900, Samsung MyShot II SCH-R460, Samsung S3500, Samsung SCH-U550, Samsung SGH-A437/A436, Samsung SGH-A717, Samsung SGH-E215, Samsung SGH-J700, Nokia 6350, Motorola ZN200, LG VX 8700, Motorola EM330, HTC Cingular 8125, HTC Fuze, HTC Shadow 2, HTC XV6850 Touch Pro CDMA (Verizon), LG Dare VX9700, LG KP500/KP501 Cookie, Motorola Rapture VU30, Motorola RAZR VE20, Motorola RAZR2 V8, Motorola RAZR2 V9, Motorola RAZR2 V9m, Motorola V3a/V3i/V3r/V3t/V3e Razr, Motorola W490/W510/W5, Nokia 3555, Pantech PN-820, Samsung Helio Drift SPH-A503, Samsung Helio Fin SPH-A513, Samsung Helio Heat SPH-A303, Samsung Propel A767, Samsung Renown SCH-U810, Samsung SCH-U740, Samsung SGH-T239, LG LX290, Sony Ericsson C905a, LG KS10, LG KU990/GC900 Viewty, LG Rumor LX260, LG Rumor2 LX265, Motorola RAZR Maxx V6, Nextel Stature i9, Motorola Stature i9, Nokia XpressMusic 5800, Palm Centro, Palm Treo 750, Palm Treo 755p, Samsung Helio Mysto SPH-A523, Samsung Propel A767, LG Chocolate VX8500, LG Invision CB630, LG KF350, LG KF750/TU750 Secret, LG KP260, LG KU380, HTC Touch Diamond2, Samsung Smooth SCH-U350, Samsung Trance SCH-U490, Sanyo Pro-200, Sanyo Pro-700, Sanyo SCP-6600/Katana, Sanyo SCP-8500/Katana DLX, Sony Ericsson W350, HTC Fusion/5800/S720, Motorola VE440, Samsung SPH-M240, Nokia Mural 6750, Motorola ROKR U9, Motorola VE465, Nokia 5310, Nokia 6263, Samsung Highnote SPH-M630, LG Trax CU575, LG LX370/UX370, Other Casio Exilim C721, HTC Herman/Touch Pro CDMA (Alltel, Sprint), LG Decoy VX8610, LG Incite CT810, LG KF900 Prada II, LG Vu/CU920/CU915/TU915, Motorola A810, Motorola C261, Motorola Clutch i465, Palm Treo 700wx/700w/700p, Samsung Alias 2 SCH-U750, Sony Ericsson W760, LG CG180, LG Chocolate 3 VX8560, LG KC780, Nokia Surge 6790, Samsung SGH-T809/D820, Samsung SLM SGH-A747, Samsung SPH-M320, LG KM501, Samsung Hue II SCH-R600, Samsung Katalyst SGH-T739, Samsung S3600, Samsung SCH-R211, Samsung SCH-U540, Samsung SGH-D900i, Samsung SGH-T429, Motorola SLVR L9, Sony Ericsson G502, Sony Ericsson K510i, LG Helix AX310/UX310, Sanyo SCP-6650/Katana II, Sony Ericsson S302, Sony Ericsson TM506, Sony Ericsson W580i, Sony Ericsson W595, Sony Ericsson W760, Nokia E75, Blackberry Pearl 8100"
		elseif RS("itemid") = 13309 then
			itemDesc = "Retractable-Cord Car Charger for LG VI-125, LG VX 4650/VX 4700/ AX4750, LG PM-325, LG C1500, LG VX 3450, LG VX 8000, LG VX 8100, LG AX5000/UX5000, LG LX-350, LG AX-140/145 Aloha/200c, LG VX 5300/AX245, LG VX 3400, LG AX-390/UX-390, LG FUSIC LX550, LG AX4270, LG VX 9800, LG VX 7000, LG VX 6100, LG VX 3200, LG PM-225, LG VX 3300/3280, LG VX 5200, LG Migo VX1000, LG VX 8300"
		elseif RS("itemid") = 8879 then
			itemDesc = "HEAVY-DUTY Car Charger for LG VI-125, LG VX 4650/VX 4700/ AX4750, LG PM-325, LG C1500, LG VX 3450, LG VX 8000, LG VX 8100, LG AX5000/UX5000, LG LX-350, LG AX-140/145 Aloha/200c, LG VX 5300/AX245, LG VX 3400, LG AX-390/UX-390, LG FUSIC LX550, LG AX4270, LG VX 9800, LG VX 7000, LG VX 6100, LG VX 3200, LG PM-225, LG VX 3300/3280, LG VX 5200, LG Migo VX1000, LG VX 8300"
		elseif RS("itemid") = 10564 then
			itemDesc = "Car Charger for LG VI-125, LG VX 4650/VX 4700/ AX4750, LG PM-325, LG C1500, LG VX 3450, LG VX 8000, LG VX 8100, LG AX5000/UX5000, LG LX-350, LG AX-140/145 Aloha/200c, LG VX 5300/AX245, LG VX 3400, LG AX-390/UX-390, LG FUSIC LX550, LG AX4270, LG VX 9800, LG VX 7000, LG VX 6100, LG VX 3200, LG PM-225, LG VX 3300/3280, LG VX 5200, LG Migo VX1000, LG VX 8300"
		else
			itemDesc = RS("itemDesc")
		end if
		
		strline = strItemLongDetail & vbTab
		strline = strline & RS("itemid") & vbTab
		strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?source=gbase" & vbtab
		strline = strline & RS("price_our") & vbTab
		strline = strline & itemDesc & vbTab
		strline = strline & "1000" & vbTab
		strline = strline & RS("itemweight") & vbTab		
		strline = strline & "http://www.wirelessemporium.com/productpics/thumb/" & RS("itemPic") & vbTab
		strline = strline & BrandName & vbTab
		strline = strline & Condition & vbTab
		strline = strline & RS("PartNumber") & vbTab
		strline = strline & "Visa,MasterCard,AmericanExpress,Discover" & vbTab
		strline = strline & expDate & vbTab
		strline = strline & BrandName & " " & ModelName & " Cell Phone " & changeTypeNameSingular(RS("typeName")) & vbTab
		strline = strline & strFeatures & vbTab
		strline = strline & strCompatibility & vbTab
		strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"
		file.WriteLine strline
		RS.movenext
	loop
	
'	' FTP the file to Google
'	dim ftp, success
'	set ftp = CreateObject("Chilkat.Ftp2")
'	success = ftp.UnlockComponent("WIRELEFTP_GQ2TEoLE9Bnh")
'	if success <> 1 then msgbox ftp.LastErrorText
'	
'	ftp.Hostname = "uploads.google.com"
'	ftp.Username = "wirelessemporium"
'	ftp.Password = "eugeneku1986"
'	ftp.Passive = 1
'	
'	' Connect and login to the FTP server.
'	success = ftp.Connect()
'	if success <> 1 then msgbox ftp.LastErrorText
'	
'	'  Upload the file.
'	dim localFilename, remoteFilename
'	localFilename = path
'	remoteFilename = filename
'	success = ftp.PutFile(localFilename,remoteFilename)
'	if success <> 1 then msgbox ftp.LastErrorText
'	
'	ftp.Disconnect
'	set ftp = nothing
end if
RS.close
set RS = nothing
file.close()

sql	=	"select	id as itemid, artist + ' ' + designName + '-music-skins-for-' + c.brandName + '-' + b.modelName as itemdesc" & vbcrlf & _
		"	,	a.artist, a.designname, b.modelname, c.brandname, a.price_we, a.defaultImg, a.musicSkinsID, 2 itemweight" & vbcrlf & _	
		"from 	we_items_musicSkins a join we_models b" & vbcrlf & _
		"	on	a.modelid = b.modelid join we_brands c" & vbcrlf & _
		"	on	a.brandid = c.brandid" & vbcrlf & _
		"where 	skip = 0" & vbcrlf & _
		"	and deleteItem = 0" & vbcrlf & _
		"	and (artist is not null or artist <> '')" & vbcrlf & _
		"	and (designName is not null or designName <> '')" & vbcrlf & _
		"	and (b.modelname is not null or b.modelname <> '')" & vbcrlf & _
		"	and (c.brandname is not null or c.brandname <> '')" & vbcrlf & _
		"	and b.isTablet = 0" & vbcrlf & _
		"order by 1" & vbcrlf
			
set objRsMusicSkins = CreateObject("ADODB.Recordset")
objRsMusicSkins.open SQL, oConn, 0, 1

nRows = 0
nFileIdx = 1

if not objRsMusicSkins.EOF then 
	path = folderName & "\" & fileBaseName & nFileIdx & ".txt"
	if fs.FileExists(path) then
		fs.DeleteFile(path)
	end if
	set file = fs.CreateTextFile(path)
	file.WriteLine "description" & vbTab & "id" & vbTab & "link" & vbTab & "price" & vbTab & "title" & vbTab & "quantity" & vbTab & "shipping_weight" & vbTab & "image_link" & vbTab & "brand" & vbTab & "condition" & vbTab & "mpn" & vbTab & "payment_accepted" & vbTab & "expiration_date" & vbTab & "product_type" & vbTab & "feature" & vbTab & "c:Compatible_With:String" & vbTab & "c:GoogleAffiliateNetworkProductURL:String"
	
	do until objRsMusicSkins.EOF
		strFeatures = 					"<ul>"
		strFeatures =	strFeatures	&	"<li>Thin material wraps around your " & objRsMusicSkins("brandname") & "&nbsp;" & objRsMusicSkins("modelname") & " without adding any bulk</li>"
		strFeatures =	strFeatures	&	"<li>Provides basic layer of protection from scratches and dust</li>"
		strFeatures =	strFeatures	&	"<li>Premium grade vinyl material and high gloss finish for a stunning look</li>"
		strFeatures =	strFeatures	&	"<li>Custom cut and non-permanent</li>"
		strFeatures =	strFeatures	&	"<li>NOT REUSABLE</li>"
		strFeatures =	strFeatures	&	"</ul>"		
		
		strItemLongDetail 	= 							objRsMusicSkins("brandname") & " " & objRsMusicSkins("modelname") & " " & objRsMusicSkins("artist") & " " & objRsMusicSkins("designname") & " Music Skins - There is no better way to personalize your phone than with " 
		strItemLongDetail	=	strItemLongDetail	&	"a Music Skin. Music skins are a thin, ""sticker-like"" covering made from a patented 3M material that wraps around your "  
		strItemLongDetail	=	strItemLongDetail	&	"entire phone, helping to prevent scratches and dust from marring the look of your phone. Easy to apply and non-permanent, "  
		strItemLongDetail	=	strItemLongDetail	&	"Music Skins are custom cut to fit your phone and add virtually no bulk at all to your phone. Even with your phone wrapped "  
		strItemLongDetail	=	strItemLongDetail	&	"in a Music Skin you can use it in conjunction with any case, holster or charging dock."
		strItemLongDetail	=	strItemLongDetail	&	strFeatures
	
		strline = strItemLongDetail & vbTab
		strline = strline & objRsMusicSkins("itemid") & vbTab
		strline = strline & "http://www.wirelessemporium.com/p-ms-" & objRsMusicSkins("itemID") & "-" & formatSEO(objRsMusicSkins("itemDesc")) & ".asp?source=gbase" & vbtab
		strline = strline & objRsMusicSkins("price_we") & vbTab
		strline = strline & objRsMusicSkins("brandname") & " " & objRsMusicSkins("modelname") & " " & objRsMusicSkins("artist") & " " & objRsMusicSkins("designname") & " Music Skins" & vbTab
		strline = strline & "1000" & vbTab
		strline = strline & objRsMusicSkins("itemweight") & vbTab		
		strline = strline & "http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault/thumbs/" & objRsMusicSkins("defaultImg") & vbTab
		strline = strline & objRsMusicSkins("brandname") & vbTab
		strline = strline & "new" & vbTab
		strline = strline & objRsMusicSkins("musicSkinsID") & vbTab
		strline = strline & "Visa,MasterCard,AmericanExpress,Discover" & vbTab
		strline = strline & expDate & vbTab
		strline = strline & objRsMusicSkins("brandname") & " " & objRsMusicSkins("modelname") & " Cell Phone Music Skins" & vbTab
		strline = strline & strFeatures & vbTab
		strline = strline & objRsMusicSkins("brandname") & " " & objRsMusicSkins("modelname") & vbTab
		strline = strline & "http://www.wirelessemporium.com/p-ms-" & objRsMusicSkins("itemID") & "-" & formatSEO(objRsMusicSkins("itemDesc")) & ".asp"
		
		call fWriteFile(strline)

		objRsMusicSkins.movenext
	loop
end if
objRsMusicSkins.close
set objRsMusicSkins = nothing
file.close()


sub fWriteFile(pStrToWrite)
	nRows = nRows + 1
	
	if nRows > 20000 then
		nFileIdx = nFileIdx + 1
		nRows = 0
		file.close()
		set file = nothing
		
		path = folderName & "\" & fileBaseName & nFileIdx & ".txt"
		if fs.FileExists(path) then
			fs.DeleteFile(path)
		end if
		set file = fs.CreateTextFile(path)
		file.WriteLine "description" & vbTab & "id" & vbTab & "link" & vbTab & "price" & vbTab & "title" & vbTab & "quantity" & vbTab & "shipping_weight" & vbTab & "image_link" & vbTab & "brand" & vbTab & "condition" & vbTab & "mpn" & vbTab & "payment_accepted" & vbTab & "expiration_date" & vbTab & "product_type" & vbTab & "feature" & vbTab & "c:Compatible_With:String" & vbTab & "c:GoogleAffiliateNetworkProductURL:String"		
	end if
	
	file.WriteLine pStrToWrite
end sub


function stripCallToAction(val)
	stripCallToAction = val
	dim a
	
	if not isnull(val) then
		a = inStr(1,stripCallToAction,"why pay ridiculous prices",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"don't settle",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"order this quality",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"order this convenient",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"buy this quality",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"buy this convenient",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"order today",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"remember - wireless emporium",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"you won't find a better price",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"buy this stylish",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"buy this vertical",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"for the same product at",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"we're confident you'll be happy",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		stripCallToAction = replace(stripCallToAction,"don't pay retail prices","",1,9,1)
	end if
end function

function changeTypeNameSingular(stypeName)
	select case stypeName
		case "Antennas/Parts"
			changeTypeNameSingular = "Antenna"
		case "Batteries"
			changeTypeNameSingular = "Battery"
		case "Bling Kits & Charms"
			changeTypeNameSingular = "Bling Kit"
		case "Chargers"
			changeTypeNameSingular = "Charger"
		case "Data Cable & Memory"
			changeTypeNameSingular = "Data Cable"
		case "Faceplates"
			changeTypeNameSingular = "Faceplate/Cover"
		case "Hands-Free"
			changeTypeNameSingular = "Headset/Hands Free"
		case "Holsters/Belt Clips"
			changeTypeNameSingular = "Holster"
		case "Key Pads/Flashing"
			changeTypeNameSingular = "Colorful Keypad"
		case "Leather Cases"
			changeTypeNameSingular = "Case/Pouch"
		case else
			changeTypeNameSingular = "Universal Gear"
	end select
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function


call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
