<%
set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")

strSDate = dateadd("d", -10, date)
strSDate = right("00" & month(strSDate), 2) & "/" & right("00" & day(strSDate), 2) & "/" & year(strSDate)
strEDate = date
'strEDate = dateadd("d", 1, date)
strEDate = right("00" & month(strEDate), 2) & "/" & right("00" & day(strEDate), 2) & "/" & year(strEDate)

strYear = year(now)
strMonth = Right("0" & month(now), 2)
strDay = Right("0" & day(now), 2)
strHour = hour(now)
strMinute = minute(now)
strSec = second(now)

filename = "WIEM_" & strYear & strMonth & strDay & strHour & strMinute & "_order-feed.xml"
path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\shoprunner\" & filename

set file = fs.CreateTextFile(path)

xmlOrder 		=	""
xmlItemDetail 	=	""
lastOrderID 	= 	-1
curOrderID		=	-1

sql	=	"select	a.orderdatetime, a.orderid, a.mobilesite, b.fname, b.lname, b.email, b.baddress1, b.baddress2, b.bcity, b.bstate, b.bzip, b.bcountry" & vbcrlf & _
		"	,	b.saddress1, b.saddress2, b.scity, b.sstate, b.szip, b.scountry, a.shiptype, a.shoprunnerid" & vbcrlf & _
		"	,	a.ordergrandtotal, a.ordersubtotal, a.orderTax, a.ordershippingfee" & vbcrlf & _
		"	,	c.quantity,	q.numOfTotalItems, q.numOfSRTotalItems" & vbcrlf & _
		"	,	case when m.id is not null then 0 else case when d.vendor in ('CM', 'DS', 'MLD') then 0 when d.partnumber like '%-HYP-%' then 0 else 1 end end SREligible" & vbcrlf & _
		"	,	case when m.id is not null then isnull(m.brand, '') + ' ' + isnull(m.model, '') + ' ' + isnull(m.artist, '') + ' ' + isnull(m.designName, '') + ' Music Skins'" & vbcrlf & _
		"			else d.itemdesc" & vbcrlf & _
		"		end itemdesc" & vbcrlf & _
		"	,	isnull(case when m.id is not null then m.price_we" & vbcrlf & _
		"					else d.price_our" & vbcrlf & _
		"				end, 0.00) price_our" & vbcrlf & _
		"	,	case when m.id is not null then m.id else c.itemid end itemid" & vbcrlf & _
		"	,	isnull(	case when (q.item_subtotal - replace(a.ordersubtotal, ',', '')) > 0 then " & vbcrlf & _
		"					abs(q.item_subtotal - replace(a.ordersubtotal, ',', '')) else 0.0 " & vbcrlf & _
		"				end" & vbcrlf & _
		"			,	0.0) discountTotal" & vbcrlf & _
		"from	we_orders a join v_accounts b" & vbcrlf & _
		"	on	a.store = b.site_id and a.accountid = b.accountid join we_orderdetails c" & vbcrlf & _
		"	on	a.orderid = c.orderid join" & vbcrlf & _
		"		(" & vbcrlf & _
		"		select	a.store, a.orderid" & vbcrlf & _
		"			,	sum(isnull(c.quantity, 0)) numOfTotalItems" & vbcrlf & _
		"			,	sum(case when m.id is not null then 0 else case when d.vendor in ('CM', 'DS', 'MLD') then 0 when d.partnumber like '%-HYP-%' then 0 else c.quantity end end) numOfSRTotalItems" & vbcrlf & _
		"			,	sum(isnull(case when m.id is not null then m.price_we" & vbcrlf & _
		"								else" & vbcrlf & _
		"									case when a.store = 0 then d.price_our" & vbcrlf & _
		"										when a.store = 1 then d.price_ca" & vbcrlf & _
		"										when a.store = 2 then d.price_co" & vbcrlf & _
		"										when a.store = 3 then d.price_ps" & vbcrlf & _
		"										when a.store = 10 then case when (d.price_our-5.0) < 3.99 then 3.99 else (d.price_our-5.0) end" & vbcrlf & _
		"										else d.price_our" & vbcrlf & _
		"									end" & vbcrlf & _
		"							end, 0.00) *	-- item price" & vbcrlf & _
		"					isnull(c.quantity, 0)) item_subtotal" & vbcrlf & _
		"		from	we_orders a join we_orderdetails c" & vbcrlf & _
		"			on	a.orderid = c.orderid left outer join we_items d" & vbcrlf & _
		"			on	c.itemid = d.itemid left outer join we_items_musicskins m" & vbcrlf & _
		"			on	c.itemid = m.id" & vbcrlf & _
		"		where	a.orderdatetime >= '" & strSDate & "'" & vbcrlf & _
		"			and	a.orderdatetime < '" & strEDate & "'" & vbcrlf & _
		"			and	a.approved = 1 and (a.cancelled = 0 or a.cancelled is null)" & vbcrlf & _
		"			and	a.store = 0 and c.returned = 0" & vbcrlf & _
		"			and a.shoprunnerID is not null and a.shoprunnerid <> ''" & vbcrlf & _
		"			and	c.itemid not in (0,1)" & vbcrlf & _
		"		group by a.store, a.orderid" & vbcrlf & _
		"		) q" & vbcrlf & _
		"	on	a.store = q.store and a.orderid = q.orderid left outer join we_items d with (nolock)" & vbcrlf & _
		"	on	c.itemid = d.itemid left outer join we_items_musicskins m with (nolock)" & vbcrlf & _
		"	on	c.itemid = m.id" & vbcrlf & _
		"where	a.orderdatetime >= '" & strSDate & "'" & vbcrlf & _
		"	and	a.orderdatetime < '" & strEDate & "'" & vbcrlf & _
		"	and	a.approved = 1 and (a.cancelled = 0 or a.cancelled is null)" & vbcrlf & _
		"	and	a.store = 0 and c.returned = 0" & vbcrlf & _
		"	and a.shoprunnerID is not null and a.shoprunnerid <> ''" & vbcrlf & _
		"	and	c.itemid not in (0,1)" & vbcrlf & _
		"	and	a.parentOrderID is null	 -- skip reship orders" & vbcrlf & _
		"order by orderid, itemid" & vbcrlf

set rs = CreateObject("ADODB.Recordset")
rs.open SQL, oConn, 0, 1
if not rs.eof then
	'core columns
	heading	=	"<?xml version=""1.0"" encoding=""utf-8""?>" & vbcrlf & _
				"<Orders>" & vbcrlf & _
				"	<Partner>WIEM</Partner>"
	response.write "<pre>" & heading & "</pre>"
	
	do until rs.eof
		curOrderID	=	rs("orderid")
		if lastOrderID <> curOrderID then
			numItem = 0
			if xmlItemDetail <> "" then
				xmlOrder	=	"	<Order>" & vbcrlf & _
								"		<OrderType>create</OrderType>" & vbcrlf & _
								"		<OrderSource>" & mobilesite & "</OrderSource>" & vbcrlf & _
								"		<OrderNumber>" & curOrderID & "</OrderNumber>" & vbcrlf & _
								"		<OrderDate>" & strSRDate & "</OrderDate>" & vbcrlf & _
								"		<CustomerFirstName>" & fname & "</CustomerFirstName>" & vbcrlf & _
								"		<CustomerLastName>" & lname & "</CustomerLastName>" & vbcrlf & _
								"		<CustomerEmail>" & email & "</CustomerEmail>" & vbcrlf & _
								"		<BillingAddress>" & vbcrlf & _
								"			<Name>" & fname & " " & lname & "</Name>" & vbcrlf & _
								"			<Line1>" & bAddress1 & "</Line1>" & vbcrlf & _
								"			<Line2>" & bAddress2 & "</Line2>" & vbcrlf & _
								"			<City>" & bCity & "</City>" & vbcrlf & _
								"			<State>" & bState & "</State>" & vbcrlf & _
								"			<PostalCode>" & bZip & "</PostalCode>" & vbcrlf & _
								"			<CountryCode>" & bCountry & "</CountryCode>" & vbcrlf & _
								"		</BillingAddress>" & vbcrlf & _
								"		<TotalNumberOfItems>" & numOfTotalItems & "</TotalNumberOfItems>" & vbcrlf & _
								"		<TotalNumberOfShopRunnerItems>" & numOfSRTotalItems & "</TotalNumberOfShopRunnerItems>" & vbcrlf & _
								"		<SRAuthenticationToken>" & shoprunnerid & "</SRAuthenticationToken>" & vbcrlf & _
								"		<CurrencyCode>USD</CurrencyCode>" & vbcrlf & _
								"		<OrderTotal>" & orderGrandTotal & "</OrderTotal>" & vbcrlf & _
								"		<OrderSubTotal>" & orderSubTotal & "</OrderSubTotal>" & vbcrlf & _				
								"		<OrderTax>" & orderTax & "</OrderTax>" & vbcrlf & _
								"		<ShipHandling>" & ordershippingFee & "</ShipHandling>" & vbcrlf & _
								"		<FeesAndChargers>0</FeesAndChargers>" & vbcrlf & _
								"		<OrderDiscount>" & orderDiscountTotal & "</OrderDiscount>" & vbcrlf & _
								xmlItemDetail & _
								"		<ShippingAddress>" & vbcrlf & _
								"			<ShippingMethod>" & srShipType & "</ShippingMethod>" & vbcrlf & _
								"			<IsShopRunnerShippingMethod>True</IsShopRunnerShippingMethod>" & vbcrlf & _
								"			<NumberOfItems>" & numOfTotalItems & "</NumberOfItems>" & vbcrlf & _
								"			<Name>" & fname & " " & lname & "</Name>" & vbcrlf & _
								"			<Line1>" & sAddress1 & "</Line1>" & vbcrlf & _
								"			<Line2>" & sAddress2 & "</Line2>" & vbcrlf & _
								"			<City>" & sCity & "</City>" & vbcrlf & _
								"			<State>" & sState & "</State>" & vbcrlf & _
								"			<PostalCode>" & sZip & "</PostalCode>" & vbcrlf & _
								"			<CountryCode>" & bCountry & "</CountryCode>" & vbcrlf & _
								"		</ShippingAddress>" & vbcrlf & _
								"	</Order>" & vbcrlf
				response.write "<pre>" & xmlOrder & "</pre>"
			end if
			xmlItemDetail = ""
		end if
		
		lastOrderID 		= 	curOrderID
		mobilesite 			= 	""
		if rs("mobilesite") then mobilesite = "mobile"
		
		strDate = cdate(rs("orderdatetime"))
		strYYYY = year(strDate)
		strMM = right("00" & month(strDate), 2)
		strDD = right("00" & day(strDate), 2)
		strHH = right("00" & hour(strDate), 2)
'		if strHH = "00" then strHH = "12"
		strMI = right("00" & minute(strDate), 2)
		strSS = right("00" & second(strDate), 2)
		strAMPM = right(strDate, 2)
		strSRDate = strYYYY & "-" & strMM & "-" & strDD & "T" & strHH & ":" & strMI & ":" & strSS
		
		bCountry = rs("bCountry")
		if bCountry <> "US" then bCountry = "CA"
		
		fname = rs("fname")
		lname = rs("lname")
		email = rs("email")
		bAddress1 = rs("bAddress1")
		bAddress2 = rs("bAddress2")
		bCity = rs("bCity")
		bState = rs("bState")
		bZip = rs("bZip")
		sAddress1 = rs("sAddress1")
		sAddress2 = rs("sAddress2")
		sCity = rs("sCity")
		sState = rs("sState")
		sZip = rs("sZip")
		numOfTotalItems = rs("numOfTotalItems")
		numOfSRTotalItems = rs("numOfSRTotalItems")
		shoprunnerid = rs("shoprunnerid")
		orderGrandTotal = rs("ordergrandtotal")
		orderSubTotal = rs("ordersubtotal")
		orderTax = rs("orderTax")
		ordershippingFee = rs("ordershippingfee")
		orderDiscountTotal = rs("discountTotal")
		itemid = rs("itemid")
		itemdesc = rs("itemdesc")
		quantity = rs("quantity")
		price_our = rs("price_our")
		sr_eligible = false
		if rs("SREligible") = 1 then sr_eligible = true
		
		srShipType = "SHOPRUNNER.UPS.2DA"
		if left(sZip, 5) <> "" then
			if isnumeric(left(sZip, 5)) then
				if left(sZip, 5) > 80000 and left(sZip, 5) < 99355 then srShipType = "SHOPRUNNER.ONTRAC.2DA"
			end if
		end if
		
		numItem = numItem + 1
		xmlItemDetail =	xmlItemDetail	&	"		<Item>" & vbcrlf & _
											"			<SKU>" & itemid & "</SKU>" & vbcrlf & _
											"			<Name>" & itemdesc & "</Name>" & vbcrlf & _
											"			<Quantity>" & quantity & "</Quantity>" & vbcrlf & _
											"			<UnitPrice>" & price_our & "</UnitPrice>" & vbcrlf & _
											"			<SREligible>" & sr_eligible & "</SREligible>" & vbcrlf & _
											"		</Item>" & vbcrlf

		rs.movenext
	loop
	
	if numItem > 0 then
		if xmlItemDetail <> "" then
			xmlOrder	=	"	<Order>" & vbcrlf & _
							"		<OrderType>create</OrderType>" & vbcrlf & _
							"		<OrderSource>" & mobilesite & "</OrderSource>" & vbcrlf & _
							"		<OrderNumber>" & curOrderID & "</OrderNumber>" & vbcrlf & _
							"		<OrderDate>" & strSRDate & "</OrderDate>" & vbcrlf & _
							"		<CustomerFirstName>" & fname & "</CustomerFirstName>" & vbcrlf & _
							"		<CustomerLastName>" & lname & "</CustomerLastName>" & vbcrlf & _
							"		<CustomerEmail>" & email & "</CustomerEmail>" & vbcrlf & _
							"		<BillingAddress>" & vbcrlf & _
							"			<Name>" & fname & " " & lname & "</Name>" & vbcrlf & _
							"			<Line1>" & bAddress1 & "</Line1>" & vbcrlf & _
							"			<Line2>" & bAddress2 & "</Line2>" & vbcrlf & _
							"			<City>" & bCity & "</City>" & vbcrlf & _
							"			<State>" & bState & "</State>" & vbcrlf & _
							"			<PostalCode>" & bZip & "</PostalCode>" & vbcrlf & _
							"			<CountryCode>" & bCountry & "</CountryCode>" & vbcrlf & _
							"		</BillingAddress>" & vbcrlf & _
							"		<TotalNumberOfItems>" & numOfTotalItems & "</TotalNumberOfItems>" & vbcrlf & _
							"		<TotalNumberOfShopRunnerItems>" & numOfSRTotalItems & "</TotalNumberOfShopRunnerItems>" & vbcrlf & _
							"		<SRAuthenticationToken>" & shoprunnerid & "</SRAuthenticationToken>" & vbcrlf & _
							"		<CurrencyCode>USD</CurrencyCode>" & vbcrlf & _
							"		<OrderTotal>" & orderGrandTotal & "</OrderTotal>" & vbcrlf & _
							"		<OrderSubTotal>" & orderSubTotal & "</OrderSubTotal>" & vbcrlf & _				
							"		<OrderTax>" & orderTax & "</OrderTax>" & vbcrlf & _
							"		<ShipHandling>" & ordershippingFee & "</ShipHandling>" & vbcrlf & _
							"		<FeesAndChargers>0</FeesAndChargers>" & vbcrlf & _
							"		<OrderDiscount>" & orderDiscountTotal & "</OrderDiscount>" & vbcrlf & _
							xmlItemDetail & _
							"		<ShippingAddress>" & vbcrlf & _
							"			<ShippingMethod>" & srShipType & "</ShippingMethod>" & vbcrlf & _
							"			<IsShopRunnerShippingMethod>True</IsShopRunnerShippingMethod>" & vbcrlf & _
							"			<NumberOfItems>" & numOfTotalItems & "</NumberOfItems>" & vbcrlf & _
							"			<Name>" & fname & " " & lname & "</Name>" & vbcrlf & _
							"			<Line1>" & sAddress1 & "</Line1>" & vbcrlf & _
							"			<Line2>" & sAddress2 & "</Line2>" & vbcrlf & _
							"			<City>" & sCity & "</City>" & vbcrlf & _
							"			<State>" & sState & "</State>" & vbcrlf & _
							"			<PostalCode>" & sZip & "</PostalCode>" & vbcrlf & _
							"			<CountryCode>" & bCountry & "</CountryCode>" & vbcrlf & _
							"		</ShippingAddress>" & vbcrlf & _
							"	</Order>" & vbcrlf
			response.write "<pre>" & xmlOrder & "</pre>"
		end if
	end if
	response.write "<pre>" & "</Orders>" & "</pre>"	
end if
rs.close
set rs = nothing
file.close()
%>