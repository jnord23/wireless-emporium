set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
'Map the file name to the physical path on the server.
filename = "Buy.com - New SKU.txt"
path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename

if fs.FileExists(path) then
	fs.DeleteFile(path)
end if

'Create the file and write some data to it.
set file = fs.CreateTextFile(path)
dim SQL, RS

sql	=	"	select	a.itemid, a.typeid, a.partnumber as mpn, a.itemdesc" & vbcrlf & _
		"		,	dbo.fn_stripHTML(isnull(convert(varchar(8000), a.itemlongdetail), '')) itemlongdetail	" & vbcrlf & _
		"		,	a.upccode, a.itempic, a.price_retail ,a.price_our, a.price_buy, a.inv_qty, a.sports	" & vbcrlf & _
		"		,	isnull(a.bullet1, '') bullet1, isnull(a.bullet2, '') bullet2, isnull(a.bullet3, '') bullet3" & vbcrlf & _
		"		,	isnull(a.bullet4, '') bullet4, isnull(a.bullet5, '') bullet5, isnull(a.bullet6, '') bullet6" & vbcrlf & _
		"		,	dbo.fn_stripHTML(isnull(convert(varchar(8000), a.compatibility), '')) compatibility, a.packagecontents" & vbcrlf & _
		"		,	isnull(b.brandname, 'Universal') brandName, isnull(c.modelname, 'Universal') modelName, c.temp, d.typename, isnull(e.subtypename, '') subtypename" & vbcrlf & _
		"	from	we_items a with (nolock) join we_types d with (nolock)	" & vbcrlf & _
		"		on	a.typeid = d.typeid join " & vbcrlf & _
		"			(" & vbcrlf & _
		"			select	partnumber -- top 10 selling for last quarter" & vbcrlf & _
		"			from	(" & vbcrlf & _
		"					select	top (10) percent with ties b.partnumber, sum(orderQty) qty" & vbcrlf & _
		"					from	we_invRecord a with (nolock) join we_items b with (nolock)" & vbcrlf & _
		"						on	a.itemid = b.itemid" & vbcrlf & _
		"					where	a.notes like '%customer order%'" & vbcrlf & _
		"						and	a.notes not like '%out of stock%'" & vbcrlf & _
		"						and	a.entryDate > dateadd(dd, -90, getdate())" & vbcrlf & _
		"						and	b.hidelive = 0 and b.inv_qty <> 0 and b.price_our > 0" & vbcrlf & _
		"						and	b.typeid <> 16 -- exclude phones" & vbcrlf & _
		"						and	b.typeid <> 5 -- exclude all handsfree" & vbcrlf & _
		"					group by b.partnumber" & vbcrlf & _
		"					order by 2 desc" & vbcrlf & _
		"					) a" & vbcrlf & _
		"			union" & vbcrlf & _
		"			select	partnumber -- bluetooth headset, speakers and unlocked cell phones" & vbcrlf & _
		"			from	(" & vbcrlf & _
		"					select	distinct a.partnumber" & vbcrlf & _
		"					from	we_items a with (nolock) left outer join we_subtypes b" & vbcrlf & _
		"						on	a.subtypeid = b.subtypeid" & vbcrlf & _
		"					where	(a.inv_qty <> 0 and a.hidelive = 0)" & vbcrlf & _
		"						and	((a.typeid = 16 and a.carrierid = 12) or (a.subtypeid in (1230,1240)))" & vbcrlf & _
		"					) b" & vbcrlf & _
		"			) x " & vbcrlf & _
		"		on 	a.partnumber = x.partnumber left outer join we_subtypes e" & vbcrlf & _
		"		on	a.subtypeid = e.subtypeid left outer join we_brands b	with (nolock)" & vbcrlf & _
		"		on	a.brandid = b.brandid left outer join we_models c with (nolock)	" & vbcrlf & _
		"		on	a.modelid = c.modelid left outer join " & vbcrlf & _
		"			(" & vbcrlf & _
		"			select	a.partnumber orphan_partnumber" & vbcrlf & _
		"			from	(" & vbcrlf & _
		"					select	partnumber" & vbcrlf & _
		"						,	sum(case when master = 1 then 1 else 0 end) nmaster" & vbcrlf & _
		"						,	sum(case when master = 0 then 1 else 0 end) nslave" & vbcrlf & _
		"					from	we_items a with (nolock)" & vbcrlf & _
		"					where	hidelive = 0 and inv_qty <> 0 and price_our > 0" & vbcrlf & _
		"					group by partnumber" & vbcrlf & _
		"					) a	" & vbcrlf & _
		"			where	a.nmaster <> 1" & vbcrlf & _
		"			) o	" & vbcrlf & _
		"		on	a.partnumber = o.orphan_partnumber	" & vbcrlf & _
		"	where	a.hidelive = 0 and a.inv_qty <> 0 and a.price_our > 0	" & vbcrlf & _
		"		and	o.orphan_partnumber is null	" & vbcrlf & _
		"	order by a.itemid desc"
set rs = CreateObject("ADODB.Recordset")
rs.open SQL, oConn, 0, 1
if not rs.eof then
	file.writeline "seller-id" & vbtab & "gtin" & vbtab & "isbn" & vbtab & "mfg-name" & vbtab & "mfg-part-number" & vbtab & "asin" & vbtab & "seller-sku" & vbtab & "title" & vbtab & "description" & vbtab & "main-image" & vbtab & "additional-images" & vbtab & "weight" & vbtab & "features" & vbtab & "listing-price" & vbtab & "msrp" & vbtab & "keywords" & vbtab & "product-set-id" & vbtab & "store-code" & vbtab & "category-id"
	do until RS.eof
		itemid = rs("itemid")
		typeid = rs("typeid")
		mpn = rs("mpn")
		itemdesc = rs("itemdesc")
		itemlongdetails = replace(replace(replace(rs("itemlongdetail"), vbcrlf, " "), vbtab, " "), " FREE!", " less!")
		upccode = rs("upccode")
		itempic = rs("itempic")
		price_retail = rs("price_retail")
		price_our = rs("price_our")
		price_buy = rs("price_buy")
		inv_qty = rs("inv_qty")
		features = bulletList(rs("Bullet1")) & bulletList(rs("Bullet2")) & bulletList(rs("Bullet3")) & bulletList(rs("Bullet4")) & bulletList(rs("Bullet5")) & bulletList(rs("Bullet6"))
		if right(features,1) = "|" then features = left(features,len(features)-1)
		compatibility = rs("compatibility")
		packageContents = rs("packageContents")
		brandName = rs("brandName")
		modelName = rs("modelName")
		tempValue = rs("temp")
		strTypeName = rs("typename")
		strSubTypeName = rs("subtypename")
		if strSubTypeName = "" then
			keywords = strTypeName & "|" & brandName & "|" & modelName & "|" & itemdesc
		else
			keywords = strTypeName & "|" & strSubTypeName & "|" & brandName & "|" & modelName & "|" & itemdesc
		end if

		AddlImages = ""
		for iCount = 0 to 3
			imgPath = "d:\productpics\AltViews\" & replace(itempic,".jpg","-" & iCount & ".jpg")
			if fs.FileExists(imgPath) then AddlImages = AddlImages & replace(replace(imgPath,"d:","http://www.wirelessemporium.com"),"\","/") & "|"
			imgPath = "d:\productpics\AltViews\" & replace(itempic,".jpg","-" & iCount & ".gif")
			if fs.FileExists(imgPath) then AddlImages = AddlImages & replace(replace(imgPath,"d:","http://www.wirelessemporium.com"),"\","/") & "|"
			imgPath = "d:\productpics\AltViews\" & replace(itempic,".gif","-" & iCount & ".jpg")
			if fs.FileExists(imgPath) then AddlImages = AddlImages & replace(replace(imgPath,"d:","http://www.wirelessemporium.com"),"\","/") & "|"
			imgPath = "d:\productpics\AltViews\" & replace(itempic,".gif","-" & iCount & ".gif")
			if fs.FileExists(imgPath) then AddlImages = AddlImages & replace(replace(imgPath,"d:","http://www.wirelessemporium.com"),"\","/") & "|"
		next
		if right(AddlImages,1) = "|" then AddlImages = left(AddlImages,len(AddlImages)-1)
		
		if typeid = 16 then
			mfgName = brandName
			MfgProductIdentifier = modelName
			weight = "2"
			categoryID = "40654"
			features = features & "|COMPATIBILITY: " & compatibility & "|WHAT IS INCLUDED: " & packageContents
			features = replace(features,vbcrlf," ")
			itemlongdetails = itemlongdetails & " Wireless Emporium offers direct replacement and unlocked Cell/PDA/Smart Phones to you WITHOUT ANY CONTRACTS! Order your replacement or unlocked Cell/PDA/Smart Phones today!"
		else
			mfgName = "Wireless Emporium, Inc."
			MfgProductIdentifier = "WE" & itemid & mpn
			MfgProductIdentifier = replace(MfgProductIdentifier,"-","",1,2)
			weight = "0.3"
			categoryID = "3054"
		end if
		
		strline = "11704981" & vbtab								'seller-id
		strline = strline & upccode & vbtab							'gtin
		strline = strline & vbtab									'isbn
		strline = strline & mfgName & vbtab							'mfg-name
		strline = strline & MfgProductIdentifier & vbtab			'mfg-part-number
		strline = strline & vbtab									'asin
		strline = strline & mpn & "-" & itemid & vbtab				'seller-sku
		strline = strline & itemdesc & vbtab						'title
		strline = strline & itemlongdetails & vbtab					'description
		strline = strline & "http://www.wirelessemporium.com/productpics/big/" & itempic & vbtab	'main-image
		strline = strline & addlImages & vbtab						'additional-images
		strline = strline & weight & vbtab							'weight
		strline = strline & features & vbtab						'features
		strline = strline & price_our & vbtab						'listing-price
		strline = strline & price_retail & vbtab					'msrp
		strline = strline & keywords & vbtab						'keywords
		strline = strline & vbtab									'product-set-id
		strline = strline & "7000" & vbtab							'store-code
		strline = strline & categoryID								'category-id
		file.writeline strline
		rs.movenext
	loop
end if
rs.close
set rs = nothing
file.close()

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub

function bulletList(val)
	if isnull(val) or len(val) <= 0 then
		bulletList = ""
	else
		bulletList = val & "|"
	end if
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function
