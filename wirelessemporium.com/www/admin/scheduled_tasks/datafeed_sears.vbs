set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
'Map the file name to the physical path on the server.
filename = "productList_Sears_WE.xls"
path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename

if fs.FileExists(path) then
	fs.DeleteFile(path)
end if

'Create the file and write some data to it.
set file = fs.CreateTextFile(path)
dim SQL, RS

sql	=	"	select	a.itemid, a.itemdesc, convert(varchar(8000), dbo.fn_stripHTML(a.itemlongdetail)) itemlongdetail" & vbcrlf & _
		"		,	convert(varchar(8000), dbo.fn_stripHTML(" & vbcrlf & _
		"			replace(" & vbcrlf & _
		"			replace(" & vbcrlf & _
		"			replace(" & vbcrlf & _
		"				convert(varchar(8000), a.itemlongdetail)" & vbcrlf & _
		"				+	case when nullif(a.bullet1, '') is not null then ' - ' + a.bullet1 + '.' else '' end" & vbcrlf & _
		"				+	case when nullif(a.bullet2, '') is not null then ' - ' + a.bullet2 + '.' else '' end" & vbcrlf & _
		"				+	case when nullif(a.bullet3, '') is not null then ' - ' + a.bullet3 + '.' else '' end" & vbcrlf & _
		"				+	case when nullif(a.bullet4, '') is not null then ' - ' + a.bullet4 + '.' else '' end" & vbcrlf & _
		"				+	case when nullif(a.bullet5, '') is not null then ' - ' + a.bullet5 + '.' else '' end" & vbcrlf & _
		"				+	case when nullif(a.bullet6, '') is not null then ' - ' + a.bullet6 + '.' else '' end" & vbcrlf & _
		"				+	case when nullif(convert(varchar(8000), a.compatibility), '') is not null then ' - COMPATIBILITY: ' + convert(varchar(8000), a.compatibility) + '.' else '' end" & vbcrlf & _
		"			,	char(10), ' ')" & vbcrlf & _
		"			,	char(13), ' ')" & vbcrlf & _
		"			,	char(9), ' '))) itemlongdetail2" & vbcrlf & _
		"		,	a.partnumber, isnull(b.brandName, 'Universal') brandName, isnull(d.modelName, 'Universal') modelName" & vbcrlf & _
		"		,	c.typeid, c.typeName, a.price_retail, a.price_our, isnull(a.itemweight, 2) itemweight, a.itempic, a.subtypeid, isnull(s.colorName, 'N/A') colorName" & vbcrlf & _
		"	from	we_items a with (nolock) join we_types c with (nolock)" & vbcrlf & _
		"		on	a.typeid = c.typeid join we_brands b with (nolock) " & vbcrlf & _
		"		on	a.brandid = b.brandid join we_models d with (nolock)" & vbcrlf & _
		"		on	a.modelid = d.modelid left outer join sears_productcolors s" & vbcrlf & _
		"		on	a.colorid = s.we_id left outer join" & vbcrlf & _
		"			(" & vbcrlf & _
		"			select	a.partnumber orphan_partnumber" & vbcrlf & _
		"			from	(" & vbcrlf & _
		"					select	partnumber" & vbcrlf & _
		"						,	sum(case when inv_qty > 0 then 1 else 0 end) nmaster" & vbcrlf & _
		"						,	sum(case when inv_qty < 0 then 1 else 0 end) nslave" & vbcrlf & _
		"					from	we_items a with (nolock)" & vbcrlf & _
		"					where	hidelive = 0 and inv_qty <> 0 and price_our > 0" & vbcrlf & _
		"					group by partnumber" & vbcrlf & _
		"					) a" & vbcrlf & _
		"			where	a.nmaster = 0" & vbcrlf & _
		"			) o" & vbcrlf & _
		"		on	a.partnumber = o.orphan_partnumber" & vbcrlf & _
		"	where	a.hidelive = 0 and a.inv_qty <> 0 and a.price_our > 0 " & vbcrlf & _
		"		and	o.orphan_partnumber is null" & vbcrlf

set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if not RS.eof then
	strHeadings = 	"Item Id" & vbTab & _
					"Action Flag" & vbTab & _
					"FBS Item" & vbTab & _
					"Variation Group ID" & vbTab & _
					"Title" & vbTab & _
					"Short Description" & vbTab & _
					"Long Description" & vbTab & _
					"Seller Categories" & vbTab & _
					"Packing Slip Description" & vbTab & _
					"Category" & vbTab & _
					"UPC" & vbTab & _
					"Manufacturer Model #" & vbTab & _
					"Cost" & vbTab & _
					"Standard Price" & vbTab & _
					"Sale Price" & vbTab & _
					"Handling Fee" & vbTab & _
					"Sale Start Date" & vbTab & _
					"Sale End Date" & vbTab & _
					"Promotional Text" & vbTab & _
					"Shipping Override" & vbTab & _
					"Free Shipping Start Date" & vbTab & _
					"Free Shipping End Date" & vbTab & _
					"Free Shipping Promotional Text" & vbTab & _
					"Low Inventory Alert" & vbTab & _
					"MAP Price Indicator" & vbTab & _
					"Brand Name" & vbTab & _
					"Shipping Length" & vbTab & _
					"Shipping Width" & vbTab & _
					"Shipping Height" & vbTab & _
					"Shipping Weight" & vbTab & _
					"Shipping Restrictions" & vbTab & _
					"Shipping Cost: Ground" & vbTab & _
					"Shipping Cost: Expedited" & vbTab & _
					"Shipping Cost: Premium" & vbTab & _
					"Choking Hazard: small parts" & vbTab & _
					"Choking Hazard: balloons" & vbTab & _
					"Choking Hazard: small ball" & vbTab & _
					"Choking Hazard: contains small ball" & vbTab & _
					"Choking Hazard: contains marble" & vbTab & _
					"Choking Hazard: other" & vbTab & _
					"Safety Warning: other" & vbTab & _
					"No Warning" & vbTab & _
					"Energy Star Compliant" & vbTab & _
					"Good HouseKeeping" & vbTab & _
					"Hazardous Material" & vbTab & _
					"Restricted Product" & vbTab & _
					"California Emissions" & vbTab & _
					"Web Exclusive" & vbTab & _
					"Food Item" & vbTab & _
					"Requires Refrigeration" & vbTab & _
					"Frozen Item" & vbTab & _
					"Alcohol" & vbTab & _
					"Tobacco" & vbTab & _
					"Product Image URL" & vbTab & _
					"Mature Content" & vbTab & _
					"Swatch Image URL" & vbTab & _
					"Feature Image URL #1" & vbTab & _
					"Feature Image URL #2" & vbTab & _
					"Feature Image URL #3" & vbTab & _
					"Feature Image URL #4" & vbTab & _
					"Feature Image URL #5" & vbTab & _
					"Feature Image URL #6" & vbTab & _
					"Accessory Type [8923] - important, search attribute" & vbTab & _
					"Device Type [8924] - important, search attribute" & vbTab & _
					"Overall Color [412] - important, variation attribute"
					
'					"By Type of Item(COMPARE - GC_Shop_ByItemType)" & vbTab & _
'					"Length(COMPARE - X085_5_All_Dimen_Length)" & vbTab & _
'					"Item Weight (lbs.)(COMPARE - Y100_5_Overview_Weight_Item)" & vbTab & _
'					"Color(VARIATION - AP_W_Color)" & vbTab & _
'					"Uses Ni-Cad(COMPARE - X088_Batteries_Ni-Cad)" & vbTab & _
'					"Overall Color(VARIATION - 03_Color_Overall)" & vbTab & _
'					"Select Dollar Amount(COMPARE - GC_GIFTCARD_Denomination_ONLY)" & vbTab & _
'					"Type(COMPARE - 03_HE_OC_Acc_Type)" & vbTab & _
'					"Gift Card Type(COMPARE - GC_GiftCard_ONLY)" & vbTab & _
'					"Item Type(COMPARE - GF_SHPPERSN_All_ItemType)" & vbTab & _
'					"Item Type(COMPARE - Generic Data Attribute)" & vbTab & _
'					"Device Type(SEARCH,COMPARE - zhe_HE_PHONES_Acc_DeviceType)" & vbTab & _
'					"Accessory Type(SEARCH,COMPARE - zhe_HE_PHONES_Acc_Type)" & vbTab & _
'					"Select Gift Wrap Style(COMPARE - GC_GiftWrap_ONLY)" & vbTab & _
'					"Cell Type(COMPARE - X088_1_Battery_Cell_Type)" & vbTab & _
'					"General Warranty(COMPARE - Y100_6_Overview_Warranty_Short)"
	
	file.writeline strHeadings
	
	do until RS.eof
		strLine	=				"WE-" & RS("itemid") & vbTab '[Item Id]
		strLine	=	strLine & 	vbTab '[Action Flag]
		strLine	=	strLine & 	vbTab '[FBS Item]
		strLine	=	strLine & 	vbTab '[Variation Group ID]
		strLine	=	strLine & 	RS("itemdesc") & vbTab '[Title]

		if RS("itemlongdetail") <> "" then
			strLine	=	strLine & 	replace(replace(replace(left(RS("itemlongdetail"), 2399), vbcr, " "), vblf, " "), vbTab, " ") & vbTab '[Short Description]		
		else
			strLine	=	strLine & 	vbTab '[Short Description]
		end if

		if RS("itemlongdetail2") <> "" then
			strLine	=	strLine & 	replace(replace(replace(left(RS("itemlongdetail2"), 4999), vbcr, " "), vblf, " "), vbTab, " ") & vbTab '[Long Description]		
		else
			strLine	=	strLine & 	vbTab '[Long Description]
		end if		

		strLine	=	strLine & 	vbTab '[Seller Categories]
		strLine	=	strLine & 	vbTab '[Packing Slip Description]
		
		if RS("typeid") = 16 then
			strLine	=	strLine & 	"ComputersElectronics-PhonesCommunications-CellPhone" & vbTab '[Category]		
		else
			strLine	=	strLine & 	"ComputersElectronics-PhonesCommunications-Accessories" & vbTab '[Category]		
		end if

		strLine	=	strLine & 	vbTab '[UPC]
		strLine	=	strLine & 	RS("partnumber") & vbTab '[Manufacturer Model #]
		strLine	=	strLine & 	vbTab '[Cost]
		strLine	=	strLine & 	RS("price_retail") & vbTab '[Standard Price]
		strLine	=	strLine & 	RS("price_our") & vbTab '[Sale Price]
		strLine	=	strLine & 	vbTab '[Handling Fee]
		strLine	=	strLine & 	"1/1/2012" & vbTab '[Sale Start Date]
		strLine	=	strLine & 	"1/1/2015" & vbTab '[Sale End Date]
		strLine	=	strLine & 	vbTab '[Promotional Text]
		strLine	=	strLine & 	"0.00" & vbTab '[Shipping Override]
		strLine	=	strLine & 	"1/1/2012" & vbTab '[Free Shipping Start Date]
		strLine	=	strLine & 	"1/1/2015" & vbTab '[Free Shipping End Date]
		strLine	=	strLine & 	vbTab '[Free Shipping Promotional Text]
		strLine	=	strLine & 	vbTab '[Low Inventory Alert]
		strLine	=	strLine & 	vbTab '[MAP Price Indicator]
		strLine	=	strLine & 	RS("brandName") & vbTab '[Brand Name]
		strLine	=	strLine & 	"1" & vbTab '[Shipping Length]
		strLine	=	strLine & 	"1" & vbTab '[Shipping Width]
		strLine	=	strLine & 	"1" & vbTab '[Shipping Height]
		strLine	=	strLine & 	RS("itemweight") & vbTab '[Shipping Weight]
		strLine	=	strLine & 	vbTab '[Shipping Restrictions]
		strLine	=	strLine & 	vbTab '[Shipping Cost: Ground]
		strLine	=	strLine & 	vbTab '[Shipping Cost: Expedited]
		strLine	=	strLine & 	vbTab '[Shipping Cost: Premium]
		strLine	=	strLine & 	vbTab '[Choking Hazard: small parts]
		strLine	=	strLine & 	vbTab '[Choking Hazard: balloons]
		strLine	=	strLine & 	vbTab '[Choking Hazard: small ball]
		strLine	=	strLine & 	vbTab '[Choking Hazard: contains small ball]
		strLine	=	strLine & 	vbTab '[Choking Hazard: contains marble]
		strLine	=	strLine & 	vbTab '[Choking Hazard: other]
		strLine	=	strLine & 	vbTab '[Safety Warning: other]
		strLine	=	strLine & 	vbTab '[No Warning]
		strLine	=	strLine & 	vbTab '[Energy Star Compliant]
		strLine	=	strLine & 	vbTab '[Good HouseKeeping]
		strLine	=	strLine & 	vbTab '[Hazardous Material]
		strLine	=	strLine & 	"No" & vbTab '[Restricted Product]
		strLine	=	strLine & 	vbTab '[California Emissions]
		strLine	=	strLine & 	vbTab '[Web Exclusive]
		strLine	=	strLine & 	"No" & vbTab '[Food Item]
		strLine	=	strLine & 	"No" & vbTab '[Requires Refrigeration]
		strLine	=	strLine & 	"No" & vbTab '[Frozen Item]
		strLine	=	strLine & 	"No" & vbTab '[Alcohol]
		strLine	=	strLine & 	"No" & vbTab '[Tobacco]
		strLine	=	strLine & 	"http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab '[Product Image URL]
		strLine	=	strLine & 	vbTab '[Mature Content]
		strLine	=	strLine & 	vbTab '[Swatch Image URL]
		
		for iCount = 0 to 3
			imgPath = "d:\productpics\AltViews\" & replace(RS("itemPic"),".jpg","-" & iCount & ".jpg")
			if fs.FileExists(imgPath) then 
				strLine	=	strLine & 	replace(replace(imgPath,"d:","http://www.wirelessemporium.com"),"\","/") & vbTab
			else
				strLine	=	strLine & 	vbTab
			end if
		next
		strLine	=	strLine & 	vbTab '[Feature Image URL #5]
		strLine	=	strLine & 	vbTab '[Feature Image URL #6]
		strLine	=	strLine & 	getAccessoryType(rs("typeid"), rs("subtypeid")) & vbTab		'"Accessory Type [8923] - important, search attribute"
		if rs("typeid") = "16" then
			strLine	=	strLine & 	"CELL PHONES" & vbTab		'"Device Type [8924] - important, search attribute"		
		else
			strLine	=	strLine & 	 vbTab		'"Device Type [8924] - important, search attribute"
		end if
		strLine	=	strLine & 	rs("colorName")		'"Overall Color [412] - important, variation attribute"

'		strLine	=	strLine & 	vbTab '[By Type of Item(COMPARE - GC_Shop_ByItemType)]
'		strLine	=	strLine & 	vbTab '[Length(COMPARE - X085_5_All_Dimen_Length)]
'		strLine	=	strLine & 	vbTab '[Item Weight (lbs.)(COMPARE - Y100_5_Overview_Weight_Item)]
'		strLine	=	strLine & 	vbTab '[Color(VARIATION - AP_W_Color)]
'		strLine	=	strLine & 	vbTab '[Uses Ni-Cad(COMPARE - X088_Batteries_Ni-Cad)]
'		strLine	=	strLine & 	vbTab '[Overall Color(VARIATION - 03_Color_Overall)]
'		strLine	=	strLine & 	vbTab '[Select Dollar Amount(COMPARE - GC_GIFTCARD_Denomination_ONLY)]
'		strLine	=	strLine & 	vbTab '[Type(COMPARE - 03_HE_OC_Acc_Type)]
'		strLine	=	strLine & 	vbTab '[Gift Card Type(COMPARE - GC_GiftCard_ONLY)]
'		strLine	=	strLine & 	vbTab '[Item Type(COMPARE - GF_SHPPERSN_All_ItemType)]
'		strLine	=	strLine & 	vbTab '[Item Type(COMPARE - Generic Data Attribute)]
'		strLine	=	strLine & 	vbTab '[Device Type(SEARCH,COMPARE - zhe_HE_PHONES_Acc_DeviceType)]
'		strLine	=	strLine & 	vbTab '[Accessory Type(SEARCH,COMPARE - zhe_HE_PHONES_Acc_Type)]
'		strLine	=	strLine & 	vbTab '[Select Gift Wrap Style(COMPARE - GC_GiftWrap_ONLY)]
'		strLine	=	strLine & 	vbTab '[Cell Type(COMPARE - X088_1_Battery_Cell_Type)]
'		strLine	=	strLine & 	""		'[General Warranty(COMPARE - Y100_6_Overview_Warranty_Short)]
					
		file.writeline strline
		RS.movenext
	loop
end if
RS.close
set RS = nothing
file.close()


set fs = CreateObject("Scripting.FileSystemObject")
'Map the file name to the physical path on the server.
filename = "productList_Sears_WE_Inv.xls"
path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename

if fs.FileExists(path) then
	fs.DeleteFile(path)
end if

'Create the file and write some data to it.
set file = fs.CreateTextFile(path)

sql	=	"	select	a.itemid " & vbcrlf & _
		"		,	case when a.hidelive = 1 then 0" & vbcrlf & _
		"				when a.inv_qty = 0 then 0" & vbcrlf & _
		"				else" & vbcrlf & _
		"					case when a.inv_qty = -1 then isnull(c.inv_qty, 0) else a.inv_qty end " & vbcrlf & _
		"			end inv_qty	" & vbcrlf & _
		"	from	we_items a with (nolock) join we_models b with (nolock)	" & vbcrlf & _
		"		on	a.modelid = b.modelid left outer join " & vbcrlf & _
		"			(" & vbcrlf & _
		"			-- qty by part number" & vbcrlf & _
		"			select	partnumber, max(inv_qty) inv_qty" & vbcrlf & _
		"			from	we_items with (nolock)	" & vbcrlf & _
		"			where	hidelive = 0 and inv_qty > 0 and price_our > 0	" & vbcrlf & _
		"				and	typeid <> 16" & vbcrlf & _
		"			group by partnumber	" & vbcrlf & _
		"			) c	" & vbcrlf & _
		"		on	a.partnumber = c.partnumber left outer join " & vbcrlf & _
		"			(" & vbcrlf & _
		"			-- part number does not have master on it" & vbcrlf & _
		"			select	a.partnumber orphan_partnumber" & vbcrlf & _
		"			from	(" & vbcrlf & _
		"					select	partnumber" & vbcrlf & _
		"						,	sum(case when inv_qty > 0 then 1 else 0 end) nMaster" & vbcrlf & _
		"						,	sum(case when inv_qty < 0 then 1 else 0 end) nSlave" & vbcrlf & _
		"					from	we_items a with (nolock) " & vbcrlf & _
		"					where	hidelive = 0 and inv_qty <> 0 and price_our > 0	" & vbcrlf & _
		"					group by partnumber	" & vbcrlf & _
		"					) a	" & vbcrlf & _
		"			where	a.nMaster = 0" & vbcrlf & _
		"			) o	" & vbcrlf & _
		"		on	a.partnumber = o.orphan_partnumber" & vbcrlf & _
		"	where	a.price_our > 0	" & vbcrlf & _
		"		and	o.orphan_partnumber is null" & vbcrlf & _
		"	order by a.itemid" & vbcrlf

set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if not RS.eof then
	strHeadings = 	"Item Id" & vbTab & _
					"Location Id" & vbTab & _
					"Quantity" & vbTab & _
					"Pickup Now Eligible"

	file.writeline strHeadings
	
	do until RS.eof
		strLine	=				"WE-" & RS("itemid") & vbTab '[Item Id]
		strLine	=	strLine & 	"289543813" & vbTab '[Location Id]
		strLine	=	strLine & 	RS("inv_qty") & vbTab '[Quantity]
		strLine	=	strLine & 	"No" '[Pickup Now Eligible]				
		
		file.writeline strLine
		RS.movenext
	loop
end if
RS.close
set RS = nothing
file.close()

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub


function getAccessoryType(pTypeID, pSubTypeID)
	ret = ""
	select case pSubTypeID
		case "1220": ret = "Car Kits"
		case else
			select case pTypeID
				case "1": ret = "Batteries"
				case "2": ret = "Chargers"
				case "3": ret = "Cases & Covers"
				case "5": ret = "Headsets"
				case "12": ret = "Plates & Parts"
				case "13": ret = "Cords & Adapters"
			end select		
	end select
	getAccessoryType = ret
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function
