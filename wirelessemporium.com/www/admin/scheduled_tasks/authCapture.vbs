'==========WE
const CC_API_LOGIN_ID = "53gJrLkDFk6x"
const CC_TRANSACTION_KEY = "54rN2d9vMB8hC4z6"
const CC_TRANS_POST_URL = "https://secure.authorize.net/gateway/transact.dll"

'==========CO
const CO_CC_API_LOGIN_ID = "327tsFNsB7"
const CO_CC_TRANSACTION_KEY = "5ySL343Wq3v492nC"
const CO_CC_TRANS_POST_URL = "https://secure.authorize.net/gateway/transact.dll"

'==========CA
const CA_CC_API_LOGIN_ID = "8zm79tMw2W9"
const CA_CC_TRANSACTION_KEY = "5rfgT7D45GT3d5ZT"
const CA_CC_TRANS_POST_URL = "https://secure.authorize.net/gateway/transact.dll"

'==========PS
const PS_CC_API_LOGIN_ID = "47EjQx2h"
const PS_CC_TRANSACTION_KEY = "26Cvh83R86fK4Tyb"
const PS_CC_TRANS_POST_URL = "https://secure.authorize.net/gateway/transact.dll"

'==========TM
const TM_CC_API_LOGIN_ID = "86Ry7xP69"
const TM_CC_TRANSACTION_KEY = "2A4VzhX9KYe886py"
const TM_CC_TRANS_POST_URL = "https://secure.authorize.net/gateway/transact.dll"

set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

today = date
sql	=	"exec authnet_getAuthOrders '" & today & "'"

set rs = oConn.execute(sql)
do until rs.eof
	api_login_id = ""
	api_trans_key = ""
	api_trans_url = ""
	
	orderid = rs("orderid")
	seq = rs("seq")
	siteid = rs("siteid")
	transactionid = rs("prevTransactionid")
	select case siteid
		case 0
			api_login_id = CC_API_LOGIN_ID
			api_trans_key = CC_TRANSACTION_KEY
			api_trans_url = CC_TRANS_POST_URL
		case 1
			api_login_id = CA_CC_API_LOGIN_ID
			api_trans_key = CA_CC_TRANSACTION_KEY
			api_trans_url = CA_CC_TRANS_POST_URL
		case 2
			api_login_id = CO_CC_API_LOGIN_ID
			api_trans_key = CO_CC_TRANSACTION_KEY
			api_trans_url = CO_CC_TRANS_POST_URL
		case 3
			api_login_id = PS_CC_API_LOGIN_ID
			api_trans_key = PS_CC_TRANSACTION_KEY
			api_trans_url = PS_CC_TRANS_POST_URL
		case 10
			api_login_id = TM_CC_API_LOGIN_ID
			api_trans_key = TM_CC_TRANSACTION_KEY
			api_trans_url = TM_CC_TRANS_POST_URL
	end select
	
	post_url = api_trans_url
	Set post_values = nothing
	Set post_values = CreateObject("Scripting.Dictionary")
	post_values.CompareMode = vbTextCompare

	post_values.Add "x_login", api_login_id
	post_values.Add "x_tran_key", api_trans_key

	post_values.Add "x_version", "3.1"
	post_values.Add "x_type", "PRIOR_AUTH_CAPTURE"
	post_values.Add "x_trans_id", transactionid
	
	post_values.Add "x_delim_data", true
	post_values.Add "x_delim_char", "|"
	'====================================================================

	post_string = ""
	for each key in post_values
		post_string = post_string & key & "=" & URLEncode(post_values(key)) & "&"
	next
	post_string = Left(post_string,Len(post_string)-1)
	
	Set objRequest = CreateObject("Msxml2.ServerXMLHTTP.3.0")
		objRequest.open "POST", post_url, false
		objRequest.send post_string
		post_response = objRequest.responseText
'	response.write "post_response:" & post_response & "<br>"
	response_array = split(post_response, post_values("x_delim_char"), -1)
	nResponseStatus = response_array(0)
	nResponseReasonCode = response_array(2)
	responseText = response_array(3)

	sql	=	"update	we_authorizenet" & vbcrlf & _
			"set	responseStatus = " & nResponseStatus & vbcrlf & _
			"	,	responseReasonCode = " & nResponseReasonCode & vbcrlf & _
			"	,	responseText = '" & responseText & "'" & vbcrlf & _
			"	,	batchDate = getdate()" & vbcrlf & _
			"where	orderid = " & orderid & vbcrlf & _
			"	and	seq = " & seq
	oConn.execute(sql)

	if nResponseStatus = 1 and nResponseReasonCode = 1 then
		sql	=	"update	we_authorizenet" & vbcrlf & _
				"set	authCaptureDate = getdate()" & vbcrlf & _
				"where	orderid = " & orderid & vbcrlf & _
				"	and	seq = " & seq
		oConn.execute(sql)
	end if

	Set objRequest = nothing
	Set post_values = nothing
	rs.movenext
loop

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub

Function URLEncode(ByVal str)
 Dim strTemp, strChar
 Dim intPos, intASCII
 strTemp = ""
 strChar = ""
 For intPos = 1 To Len(str)
  intASCII = Asc(Mid(str, intPos, 1))
  If intASCII = 32 Then
   strTemp = strTemp & "+"
  ElseIf ((intASCII < 123) And (intASCII > 96)) Then
   strTemp = strTemp & Chr(intASCII)
  ElseIf ((intASCII < 91) And (intASCII > 64)) Then
   strTemp = strTemp & Chr(intASCII)
  ElseIf ((intASCII < 58) And (intASCII > 47)) Then
   strTemp = strTemp & Chr(intASCII)
  Else
   strChar = Trim(Hex(intASCII))
   If intASCII < 16 Then
    strTemp = strTemp & "%0" & strChar
   Else
    strTemp = strTemp & "%" & strChar
   End If
  End If
 Next
 URLEncode = strTemp
End Function
