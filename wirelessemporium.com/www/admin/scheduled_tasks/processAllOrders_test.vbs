set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getStagingConn()

'Set objNetwork = wscript.CreateObject("wscript.network")

startDate	=	date - 7
endDate		=	date
curTime		=	now

'startDate	=	cdate("5/26/11")
'endDate	=	cdate("5/27/11")

'sql	=	"exec processOrdersAll '" & startDate & "', '" & endDate & "'"
sql	=	"exec processOrders_test '" & startDate & "', '" & endDate & "', '', 'M'"
set objRsOrders = oConn.execute(sql)

dim oPdf, oDoc, oPage, oParam, pathToSave, pathPDF
dim site_id, shipType, siteShortDesc, curOrderID, lastOrderID
dim oTableOut, oTableOutLast, objFields, orderList, oFileOut
dim fso : set fso = CreateObject("Scripting.FileSystemObject")

const MAX_ITEM_DISPLAY_TXT	=	3
const MAX_ITEM_DISPLAY_PDF	=	15
lastOrderID 				= 	-1
gblSubTotalPrice 			= 	cdbl(0.0)	'item price sub total for each orderid
gblCurItemQty				=	clng(0)		'for footer in PDF
gblCurItemDetailCnt			=	cint(0)		'TXT file can contain up to 3 item details
gblCoverThumbImagePosX		=	40
gblCoverThumbImagePosY		=	40
orderList					=	""
set	oTable					=	nothing
firstLap					=	true

gblCurPageNum				=	cint(1)
gblTotalPageNum				=	cint(1)
gblCurLineItemNum			=	cint(0)

'=====================================================================================
'============================ GENERATING PDF FILES ============= START ===============
'=====================================================================================
if objRsOrders.state = 1 then
	'recordset loop( by store, shiptype)
	do until objRsOrders is nothing
	
		if not objRsOrders.eof then
			call createTxtDocHeader(fso, objRsOrders, oFileOut, endDate)	'===== this is for tarantula export file for PRIO, EXP and FCM-I
		end if		
	
		'new pdf
		call initPDF(oPdf, oDoc, oParam)
		orderList = ""
		firstLap = true
		do until objRsOrders.eof
'			response.write objRsOrders("site_id") & vbTab & vbTab & objRsOrders("orderid") & vbTab & vbTab & objRsOrders("shipToken2") & vbTab & vbTab & objRsOrders("ordertype") & "<br>"

			siteShortDesc	=	objRsOrders("shortDesc")
			curOrderID		=	objRsOrders("orderid")
			pathPDF			=	objRsOrders("pathPDF")
			shipToken2		=	objRsOrders("shipToken2")
			numLineItems	=	cint(objRsOrders("numRegularOrder"))
			
			'new page
			if lastOrderID <> curOrderID then
				gblCurPageNum = 1
				gblCurLineItemNum = 0
				if numLineItems <= MAX_ITEM_DISPLAY_PDF then 
					gblTotalPageNum = 1
				else
					gblTotalPageNum = int(numLineItems / MAX_ITEM_DISPLAY_PDF)	'round down
					if (numLineItems mod MAX_ITEM_DISPLAY_PDF) >=1 then gblTotalPageNum = gblTotalPageNum + 1
				end if
				
				gblCoverThumbImagePosX = 40
				gblCoverThumbImagePosY = 40
				
				if orderList = "" then
					orderList = curOrderID
				else
					orderList = orderList & "," & curOrderID
				end if

				if not firstLap then 
					call drawTable(oPage, oTable, 42, 613) 
				else
					firstLap = false 
				end if
			
				'================================ START NEW PDF PAGE ===================================
				set oPage = oDoc.Pages.Add
				set oFont = oDoc.Fonts("Arial")
			
				call createPdfDocHeader(oDoc, oParam, oPage, oFont, objRsOrders)	'create and draw

				set oTable = oDoc.CreateTable("rows=2; cols=5; width=541; height=1; CellBorder=.1; border=0; CellSpacing=0; CellPadding=2;")
				oTable.Font = oDoc.Fonts("Arial")
				with oTable.Rows(1)
					.Cells(1).Width = 130
					.Cells(2).Width = 241
					.Cells(3).Width = 30
					.Cells(4).Width = 70
					.Cells(5).Width = 70
				end with

				gblCurItemQty		=	0
				gblSubTotalPrice 	= 	0.0
				call appendOrderDetailHeaderPDF(oDoc, oParam, oPage, oFont, objRsOrders, oTable)
				if objRsOrders("promoItemID") > 0 then
					call appendPromoItemPDF(oDoc, oParam, oPage, oFont, objRsOrders, oTable)
				end if
				'//================================ START NEW PDF PAGE ===================================
				
				call appendGeneralOrderInfoTXT(oFileOut, objRsOrders)
			end if

			call appendItemPDF(oDoc, oParam, oPage, oFont, objRsOrders, oTable)
			call appendItemDetailTXT(oFileOut, objRsOrders)
			
			gblCurLineItemNum = gblCurLineItemNum + 1
			if gblTotalPageNum > 1 then
				if gblCurLineItemNum >= MAX_ITEM_DISPLAY_PDF then
					call drawTable(oPage, oTable, 42, 613) 
					gblCurLineItemNum = 0
					gblCurPageNum = gblCurPageNum + 1

					if gblCurLineItemNum < numLineItems then
						'================================ START NEW PDF PAGE ===================================
						set oPage = oDoc.Pages.Add
						set oFont = oDoc.Fonts("Arial")
					
						call createPdfDocHeader(oDoc, oParam, oPage, oFont, objRsOrders)	'create and draw
		
						set oTable = oDoc.CreateTable("rows=2; cols=5; width=541; height=1; CellBorder=.1; border=0; CellSpacing=0; CellPadding=2;")
						oTable.Font = oDoc.Fonts("Arial")
						with oTable.Rows(1)
							.Cells(1).Width = 130
							.Cells(2).Width = 241
							.Cells(3).Width = 30
							.Cells(4).Width = 70
							.Cells(5).Width = 70
						end with
		
						call appendOrderDetailHeaderPDF(oDoc, oParam, oPage, oFont, objRsOrders, oTable)
						'//================================ START NEW PDF PAGE ===================================
					end if
				end if
			end if

			lastOrderID = curOrderID
			objRsOrders.movenext
		loop

		call drawTable(oPage, oTable, 42, 613)	'draw last one.

		if shipToken2 = "PRIO" or shipToken2 = "EXP" or shipToken2 = "FCM-I" then
			txtFileName = siteShortDesc & "_" & replace(cStr(endDate),"/","-") & "_" & shipToken2 & ".txt"
		else
			txtFileName = ""
		end if
		
		pdfFileName = siteShortDesc & "_SalesOrders_" & replace(endDate,"/","-") & "_" & shipToken2 & ".pdf"
		pathToSave = pathPDF & "\terrytest\" & pdfFileName
		call savePages(oDoc, pathToSave)
	
		'==================================================================================	
		'======= update db record =========================================================
		'drop ship orders are being updated by [processOrderAll]		: NO HARD COPIES
		'this updates regular orders that are processed by warehouse	: HARD COPIES
		'==================================================================================
		if orderList <> "" then
			sql	=	"update	we_orders" & vbcrlf & _
					"set	thub_posted_to_accounting = 'W'" & vbcrlf & _
					"	,	thub_posted_date = '" & curTime & "'" & vbcrlf & _
					"	,	processPDF = '" & pdfFileName & "'" & vbcrlf & _
					"	,	processTXT = '" & txtFileName & "'" & vbcrlf & _
					"where	orderid in (" & orderList & ")" & vbcrlf
					
'			oConn.execute(sql)
		end if
		
		set objRsOrders = objRsOrders.nextrecordset
	loop
end if

'=====================================================================================
'============================ GENERATING PDF FILES =============== END ===============
'=====================================================================================


sub appendOrderDetailHeaderPDF(byref pDoc, byref pParam, byref pPage, byref pFont, byref pObjRsOrders, byref oTable)
	tOrderTypeID = pObjRsOrders("ordertypeid")
	strHeading = ""
	select case pObjRsOrders("site_id")
		case 0
			if 5 = tOrderTypeID then		'atomic mall
				strHeading = "Questions regarding your order? Please email us at: service@wirelessemporium.com"
				
				oTable(1, 1).ColSpan = 5
				oTable(1, 1).AddText strHeading, "size=8; expand=true; alignment=center", pFont
				oTable(2, 1).AddText "Item #", "size=9; expand=true; alignment=center", pFont
				oTable(2, 2).ColSpan = 3
				strText = "AtomicMall Order Number: " & pObjRsOrders("extOrderNumber")
				oTable(2, 2).AddText strText, "size=9; expand=true; alignment=left", pFont
				oTable(2, 5).AddText "Qty.", "size=9; expand=true; alignment=center", pFont
			elseif 8 = tOrderTypeID then		'Sears
				strHeading = "Questions regarding your order? Please email us at: service@wirelessemporium.com"
				
				oTable(1, 1).ColSpan = 5
				oTable(1, 1).AddText strHeading, "size=8; expand=true; alignment=center", pFont
				oTable(2, 1).AddText "Item #", "size=9; expand=true; alignment=center", pFont
				oTable(2, 2).ColSpan = 3
				strText = "Sears Customer Order Confirmation Number: " & pObjRsOrders("extOrderNumber")
				oTable(2, 2).AddText strText, "size=9; expand=true; alignment=left", pFont
				oTable(2, 5).AddText "Qty.", "size=9; expand=true; alignment=center", pFont
			elseif 9 = tOrderTypeID then		'Bestbuy
				strHeading = "Questions regarding your order? Please email us at: service@wirelessemporium.com"
				
				oTable(1, 1).ColSpan = 5
				oTable(1, 1).AddText strHeading, "size=8; expand=true; alignment=center", pFont
				oTable(2, 1).AddText "Item #", "size=9; expand=true; alignment=center", pFont
				oTable(2, 2).ColSpan = 3
				strText = "Bestbuy Order Number: " & pObjRsOrders("extOrderNumber")
				oTable(2, 2).AddText strText, "size=9; expand=true; alignment=left", pFont
				oTable(2, 5).AddText "Qty.", "size=9; expand=true; alignment=center", pFont
			elseif 10 = tOrderTypeID then		'Newegg
				strHeading = "Questions regarding your order? Please email us at: service@wirelessemporium.com"
				
				oTable(1, 1).ColSpan = 5
				oTable(1, 1).AddText strHeading, "size=8; expand=true; alignment=center", pFont
				oTable(2, 1).AddText "Item #", "size=9; expand=true; alignment=center", pFont
				oTable(2, 2).ColSpan = 3
				strText = "Newegg Order Number: " & pObjRsOrders("extOrderNumber")
				oTable(2, 2).AddText strText, "size=9; expand=true; alignment=left", pFont
				oTable(2, 5).AddText "Qty.", "size=9; expand=true; alignment=center", pFont
			else
				strHeading = "Questions regarding your order? Most answers can be found at our self-service Frequently-Asked Questions page. "
				strHeading = strHeading & "Simply visit us at: www." & pObjRsOrders("longDesc") & ".com/faq. Or click on the ""FAQ"" link on our site."
				ret = addRowsToTable(1, 1, oTable)
				
				oTable(1, 1).ColSpan = 3
				oTable(1, 1).RowSpan = 2
				oTable(1, 1).AddText strHeading, "size=8; expand=false; alignment=center", pFont
				oTable(1, 4).AddText "Paid By", "size=9; expand=true; alignment=center", pFont
				oTable(1, 5).AddText "Ship Via", "size=9; expand=true; alignment=center", pFont
				oTable(2, 4).AddText pObjRsOrders("ordertype"), "size=9; expand=true; alignment=center", pFont
				oTable(2, 5).AddText pObjRsOrders("shiptype"), "size=9; expand=true; alignment=center", pFont
				oTable(3, 1).AddText "Item #", "size=9; expand=true; alignment=center", pFont
				oTable(3, 2).AddText "Description", "size=9; expand=true; alignment=center", pFont
				oTable(3, 3).AddText "Qty.", "size=9; expand=true; alignment=center", pFont
				oTable(3, 4).AddText "Rate", "size=9; expand=true; alignment=center", pFont
				oTable(3, 5).AddText "Amount", "size=9; expand=true; alignment=center", pFont
			end if

		case 1, 2, 3
			ret = addRowsToTable(1, 1, oTable)'		
			oTable(1, 1).ColSpan = 3
			oTable(1, 1).RowSpan = 2
			strHeading = "Questions regarding your order? Most answers can be found at our self-service Frequently-Asked Questions page. "
			strHeading = strHeading & "Simply visit us at: www." & pObjRsOrders("longDesc") & ".com/faq.html. Or click on the ""FAQ"" link on our site."
			oTable(1, 1).AddText strHeading, "size=8; expand=false; alignment=center", pFont
			oTable(1, 4).AddText "Paid By", "size=9; expand=true; alignment=center", pFont
			oTable(1, 5).AddText "Ship Via", "size=9; expand=true; alignment=center", pFont
			oTable(2, 4).AddText pObjRsOrders("ordertype"), "size=9; expand=true; alignment=center", pFont
			oTable(2, 5).AddText pObjRsOrders("shiptype"), "size=9; expand=true; alignment=center", pFont
			oTable(3, 1).AddText "Item #", "size=9; expand=true; alignment=center", pFont
			oTable(3, 2).AddText "Description", "size=9; expand=true; alignment=center", pFont
			oTable(3, 3).AddText "Qty.", "size=9; expand=true; alignment=center", pFont
			oTable(3, 4).AddText "Rate", "size=9; expand=true; alignment=center", pFont
			oTable(3, 5).AddText "Amount", "size=9; expand=true; alignment=center", pFont
		case 10
			ret = addRowsToTable(1, 1, oTable)
			oTable(1, 1).ColSpan = 3
			oTable(1, 1).RowSpan = 2
			strHeading = "Questions regarding your order? Most answers can be found at our self-service Frequently-Asked Questions page. "
			strHeading = strHeading & "Simply visit us at: http://www.tabletmall.com/aboutUs.html#aboutCS."
			oTable(1, 1).AddText strHeading, "size=8; expand=false; alignment=center", pFont
			oTable(1, 4).AddText "Paid By", "size=9; expand=true; alignment=center", pFont
			oTable(1, 5).AddText "Ship Via", "size=9; expand=true; alignment=center", pFont
			oTable(2, 4).AddText pObjRsOrders("ordertype"), "size=9; expand=true; alignment=center", pFont
			oTable(2, 5).AddText pObjRsOrders("shiptype"), "size=9; expand=true; alignment=center", pFont
			oTable(3, 1).AddText "Item #", "size=9; expand=true; alignment=center", pFont
			oTable(3, 2).AddText "Description", "size=9; expand=true; alignment=center", pFont
			oTable(3, 3).AddText "Qty.", "size=9; expand=true; alignment=center", pFont
			oTable(3, 4).AddText "Rate", "size=9; expand=true; alignment=center", pFont
			oTable(3, 5).AddText "Amount", "size=9; expand=true; alignment=center", pFont
	end select
end sub


sub appendGeneralOrderInfoTXT(byref pFileToWrite, byref pObjRsOrders)
	gblCurItemDetailCnt = 	0
	tOrderTypeID 		= 	pObjRsOrders("ordertypeid")
	
	if shipToken2 = "PRIO" or shipToken2 = "EXP" or shipToken2 = "FCM-I" then
		strToWrite = pObjRsOrders("fname") & " " & pObjRsOrders("lname") & vbTab
		for i = 1 to 3
			strToWrite = strToWrite & vbTab
		next
		
		strToWrite = strToWrite & pObjRsOrders("orderid") & vbTab
		for i = 1 to 19
			strToWrite = strToWrite & vbTab
		next
		
		'if there is an optional shipping address use them.	
		if pObjRsOrders("shippingid") > 0 and len(pObjRsOrders("ssAddress1")) > 0 and len(pObjRsOrders("ssCity")) > 0 then
			sAddress1 	= 	pObjRsOrders("ssAddress1")
			sAddress2 	= 	pObjRsOrders("ssAddress2")
			sCity 		= 	pObjRsOrders("ssCity")
			sState 		= 	pObjRsOrders("ssState")
			sZip 		= 	pObjRsOrders("ssZip")
			sCountry	=	ucase(pObjRsOrders("ssCountry"))
		else
			sAddress1 	= 	pObjRsOrders("sAddress1")
			sAddress2 	= 	pObjRsOrders("sAddress2")
			sCity 		= 	pObjRsOrders("sCity")
			sState 		= 	pObjRsOrders("sState")
			sZip 		= 	pObjRsOrders("sZip")
			sCountry	=	ucase(pObjRsOrders("sCountry"))
		end if
		
		strToWrite = strToWrite & sCity & vbTab
		strToWrite = strToWrite & sAddress1 & vbTab
		strToWrite = strToWrite & sAddress2 & vbTab
		strToWrite = strToWrite & vbTab 'address3
		strToWrite = strToWrite & vbTab
		strToWrite = strToWrite & vbTab
		strToWrite = strToWrite & sState & vbTab
		strToWrite = strToWrite & sZip & vbTab
		strToWrite = strToWrite & sCountry & vbTab
		strToWrite = strToWrite & pObjRsOrders("phone") & vbTab
		strToWrite = strToWrite & pObjRsOrders("email") & vbTab
		for aCount = 1 to 7
			strToWrite = strToWrite & vbTab
		next

'		=================================
'		1	First Class			FCM
'		
'		2	USPS Priority		PRIO
'		3	USPS Express		EXP
'		8	USPS Priority Int'l	PRIO
'
'		4	First Class Int'l	FCM-I
'
'		5	UPS 2nd Day Air		UPS
'		6	UPS Ground			UPS
'		7	UPS 3 Day Select	UPS
'		=================================

		select case pObjRsOrders("shiptypeid")
			case 2 		: shipCode = 7		
			case 8 		: shipCode = 5
			case 3 		: shipCode = 3
			case 4 		: shipCode = 4
			case else 	: shipCode = 1
		end select
		
		strToWrite = strToWrite & shipCode & vbTab

		if 5 = tOrderTypeID then
			strToWrite = strToWrite & "AtomicMall.com" & vbTab 'MEMO
		elseif 8 = tOrderTypeID then
			strToWrite = strToWrite & "Sears.com" & vbTab 'MEMO			
		elseif 9 = tOrderTypeID then
			strToWrite = strToWrite & "Bestbuy" & vbTab 'MEMO
		elseif 10 = tOrderTypeID then
			strToWrite = strToWrite & "Newegg" & vbTab 'MEMO
		else
			strToWrite = strToWrite & "UNITED STATES" & vbTab 'MEMO
		end if
		for i = 1 to 11
			strToWrite = strToWrite & vbTab
		next
		
		pFileToWrite.Write strToWrite
	end if
end sub

sub appendItemDetailTXT(byref pFileToWrite, byref pObjRsOrders)
	shipToken2 			= 	pObjRsOrders("shipToken2")
	gblCurItemDetailCnt	=	gblCurItemDetailCnt + 1
	itemQty		 		= 	pObjRsOrders("quantity")	
	itemWeight	 		= 	pObjRsOrders("itemWeight")
	itemPrice			=	0.0

	if itemWeight < 2 then itemWeight = 2 end if

	itemWeight			=	itemQty * itemWeight
	itemPrice			=	itemQty * cdbl(pObjRsOrders("price"))
	itemDesc			=	"Cellphone " & pObjRsOrders("typename")

	if shipToken2 = "PRIO" or shipToken2 = "EXP" or shipToken2 = "FCM-I" then
		strItemDetails = ""	
		if gblCurItemDetailCnt <= MAX_ITEM_DISPLAY_TXT then
			strItemDetails = strItemDetails & itemQty & vbTab
			strItemDetails = strItemDetails & itemDesc & vbTab
			strItemDetails = strItemDetails & itemWeight & vbTab
			strItemDetails = strItemDetails & itemPrice & vbTab
			pFileToWrite.Write strItemDetails
		end if
		
		strItemDetails = ""	
		if gblCurItemQty = pObjRsOrders("itemTotalQty") then
			if gblCurItemDetailCnt >= MAX_ITEM_DISPLAY_TXT then
				tempSkipCol = MAX_ITEM_DISPLAY_TXT
			else
				tempSkipCol = gblCurItemDetailCnt
			end if
			
			for i=1 to (12-(tempSkipCol*4))
				strItemDetails = strItemDetails & vbTab
			next
			
			strMultiOrder 		=	 ""
			orderTotalQty 		= 	pObjRsOrders("itemTotalQty")
			orderTotalWeight 	= 	pObjRsOrders("itemTotalWeight")
			orderSubTotal		=	pObjRsOrders("ordersubtotal")
			if orderTotalQty > 1 then strMultiOrder = "MULTI-ITEMS" end if
			
			strItemDetails = strItemDetails & strMultiOrder & vbTab
			strItemDetails = strItemDetails & orderTotalQty & vbTab
			strItemDetails = strItemDetails & orderTotalWeight & vbTab
			strItemDetails = strItemDetails & orderSubTotal & vbNewLine
			
			pFileToWrite.Write strItemDetails
		end if
	end if	
end sub

sub createTxtDocHeader(byref pFso, byref pObjRsOrders, byref pFileOut, pDate)
	shipToken2 	= 	pObjRsOrders("shipToken2")
	
	if shipToken2 = "PRIO" or shipToken2 = "EXP" or shipToken2 = "FCM-I" then
		txtFileName = 	objRsOrders("shortDesc") & "_" & replace(cStr(pDate),"/","-") & "_" & shipToken2 & ".txt"
		pathTXT		=	objRsOrders("pathTXT") & "\terrytest\" & txtFileName
		if pFso.FileExists(pathTXT) then 
			pFso.DeleteFile(pathTXT)
		end if

		set pFileOut = pFso.CreateTextFile(pathTXT)	
		
		strToWrite = """ContactName""" & vbTab & """ORDER_KEY""" & vbTab & """CUSTOMER_KEY""" & vbTab & """TXN_DT""" & vbTab & """EXT_REF_CD""" & vbTab & """QB_TXN_NUMBER""" & vbTab
		strToWrite = strToWrite & """CCY_CODE""" & vbTab & """REF_NUMBER""" & vbTab & """CLASS_REF""" & vbTab & """ARACCOUNT_NAME""" & vbTab & """DEPOSITACCOUNT_NAME""" & vbTab
		strToWrite = strToWrite & """PAYMENTMETHOD_REF""" & vbTab & """TEMPLATE_REF""" & vbTab
		strToWrite = strToWrite & """BADDR_LINE1""" & vbTab & """BADDR_LINE2""" & vbTab & """BADDR_LINE3""" & vbTab & """BADDR_LINE4""" & vbTab
		strToWrite = strToWrite & """BADDR_CITY""" & vbTab & """BADDR_PROVINCE""" & vbTab & """BADDR_STATE""" & vbTab & """BADDR_ZIP""" & vbTab & """BADDR_COUNTRY""" & vbTab
		strToWrite = strToWrite & """Phone""" & vbTab & """Email""" & vbTab & """SADDR_CITY""" & vbTab
		strToWrite = strToWrite & """SADDR_LINE1""" & vbTab & """SADDR_LINE2""" & vbTab & """SADDR_LINE3""" & vbTab & """SADDR_LINE4""" & vbTab
		strToWrite = strToWrite & """SADDR_PROVINCE""" & vbTab & """SADDR_STATE""" & vbTab & """SADDR_ZIP""" & vbTab & """SADDR_COUNTRY""" & vbTab
		strToWrite = strToWrite & """SADDR_PHONE""" & vbTab & """SADDR_EMAIL""" & vbTab & """IS_PENDING""" & vbTab & """PO_NUMBER""" & vbTab & """TERMS_REF""" & vbTab
		strToWrite = strToWrite & """DUE_DT""" & vbTab & """SALESREP""" & vbTab & """FOB""" & vbTab & """SHIP_DT""" & vbTab & """SHIP_METHOD_REF""" & vbTab & """MEMO""" & vbTab
		strToWrite = strToWrite & """CUSTOMER_MSG_REF""" & vbTab & """IS_TO_BE_PRINTED""" & vbTab & """CUSTOMER_SALES_TAX_REF""" & vbTab
		strToWrite = strToWrite & """TAX1_TOTAL""" & vbTab & """TAX2_TOTAL""" & vbTab & """EXCHANGE_RATE""" & vbTab
		strToWrite = strToWrite & """CUST_FIELD1""" & vbTab & """CUST_FIELD2""" & vbTab & """CUST_FIELD3""" & vbTab & """CUST_FIELD4""" & vbTab & """CUST_FIELD5""" & vbTab
		strToWrite = strToWrite & """CUSTOM1""" & vbTab & """CUSTOM2""" & vbTab & """CUSTOM3""" & vbTab & """CUSTOM4""" & vbTab
		strToWrite = strToWrite & """CUSTOM5""" & vbTab & """CUSTOM6""" & vbTab & """CUSTOM7""" & vbTab & """CUSTOM8""" & vbTab
		strToWrite = strToWrite & """CUSTOM9""" & vbTab & """CUSTOM10""" & vbTab & """CUSTOM11""" & vbTab & """CUSTOM12""" & vbTab
		strToWrite = strToWrite & """TOTAL_SHIP_COST""" & vbTab & """TOTAL_HANDLING_COST""" & vbTab & """TOTAL_ORDER_AMT""" & vbTab & """MEMO_PASS_THRU""" & vbTab
		strToWrite = strToWrite & """SRC_TXN_TYPE""" & vbTab & """SRC_PAYMENT_STATUS""" & vbTab & """SRC_PAYER_ID""" & vbTab & """SRC_PAYMENT_TYPE""" & vbTab
		strToWrite = strToWrite & """SRC_BUSINESS_NAME""" & vbTab & """SRC_NOTIFY_VERSION""" & vbTab & """SRC_PAYER_STATUS""" & vbTab & """SRC_RECEIVER_ID""" & vbTab
		strToWrite = strToWrite & """ITEM_NAME""" & vbTab & """ItemName""" & vbTab & """ITEM_DESC""" & vbTab & """ITEM_QUANTITY""" & vbTab & """ITEM_RATE""" & vbTab
		strToWrite = strToWrite & """ITEM_AMOUNT""" & vbTab & """ITEM_CLASS_REF""" & vbTab & """SERVICE_DT""" & vbTab & """ITEM_SALES_TAX_REF""" & vbTab & """ItemWeight""" & vbTab
		strToWrite = strToWrite & """TotalItemWeight""" & vbTab & """Item_Type"""
		pFileOut.WriteLine strToWrite
	end if
end sub

sub createPdfDocHeader(byref pDoc, byref pParam, byref pPage, byref pFont, byref pObjRsOrders)
	call drawLogo(pDoc, oPage, pParam, pObjRsOrders)
	call drawCompanyAddress(oPage, pParam, oFont, pObjRsOrders)
	call drawBarcode(oPage, pObjRsOrders)
	call drawHeaders(pDoc, oPage, pParam, oFont, pObjRsOrders)
end sub

sub appendFooter(byref pDoc, byref pParam, byref pFont, byref pTable, byref pObjRsOrders)
	tOrderTypeID 		= 	pObjRsOrders("ordertypeid")
	nBuySafeAmount 		= 	cdbl(pObjRsOrders("BuySafeAmount"))
	nOrderSubTotal		=	cdbl(pObjRsOrders("ordersubtotal"))
	nTaxAmount			=	cdbl(pObjRsOrders("orderTax"))
	nShipFee			=	cdbl(pObjRsOrders("ordershippingfee"))
	nOrderGrandTotal	=	cdbl(pObjRsOrders("ordergrandtotal"))
	
	if isobject(pTable) then
		if pTable.Rows.Count > 0 then
			select case pObjRsOrders("site_id")
				case 0
					curRowPos = addRowsToTable(1, 1, pTable)
					pTable(curRowPos, 1).ColSpan = 5
					pTable(curRowPos, 1).AddText " ", "size=12; expand=true; alignment=center", pFont
					
					nDiscount = abs(cDbl(gblSubTotalPrice) - nOrderSubTotal)
					if nDiscount > 0 then nDiscount = nDiscount * -1 end if

					nRowSpan = 6
					if nBuysafeamount > 0 then nRowSpan = nRowSpan + 1 end if
					
					if 5 = tOrderTypeID then 
						ret = addRowsToTable(nRowSpan, 5, pTable)
						curRowPos = curRowPos + 1						
						pTable(curRowPos, 1).ColSpan = 2
						pTable(curRowPos, 1).RowSpan = nRowSpan
						pTable(curRowPos, 1).Height = 105
						
						'====================================== subtotal						
						pTable(curRowPos, 3).ColSpan = 2
						pTable(curRowPos, 3).Height = 17
						pTable(curRowPos, 3).AddText "SUBTOTAL:", "size=9; expand=true; alignment=left", pFont
						pTable(curRowPos, 5).AddText formatCurrency(nOrderSubTotal), "size=9; expand=true; alignment=right", pFont						
					else
						nRowSpan = nRowSpan + 1
						ret = addRowsToTable(nRowSpan, 5, pTable)
						curRowPos = curRowPos + 1
						pTable(curRowPos, 1).ColSpan = 2
						pTable(curRowPos, 1).RowSpan = nRowSpan
						pTable(curRowPos, 1).Height = 122

						'====================================== EZ No-Hassle Policy image
						set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/SalesOrders/WE-Return-Policy.gif")
						pParam("x") = 12		:	pParam("y") = 3
						pParam("ScaleX") = .5	:	pParam("ScaleY") = .5
						pTable(curRowPos, 1).Canvas.DrawImage oImage, pParam
						
						'====================================== discount
						pTable(curRowPos, 3).ColSpan = 2
						pTable(curRowPos, 3).Height = 17
						pTable(curRowPos, 3).AddText "DISCOUNT:", "size=9; expand=true; alignment=left", pFont
						pTable(curRowPos, 5).AddText formatCurrency(nDiscount), "size=9; expand=true; alignment=right", pFont

						'====================================== buysafe
						if nBuysafeamount > 0 then
							curRowPos = curRowPos + 1
							pTable(curRowPos, 3).ColSpan = 2
							pTable(curRowPos, 3).AddText "buySAFE Bond:", "size=9; expand=true; alignment=left", pFont
							pTable(curRowPos, 5).AddText formatCurrency(nBuysafeamount), "size=9; expand=true; alignment=right", pFont
						end if

						'====================================== subtotal
						curRowPos = curRowPos + 1
						pTable(curRowPos, 3).ColSpan = 2
						pTable(curRowPos, 3).AddText "SUBTOTAL:", "size=9; expand=true; alignment=left", pFont
						pTable(curRowPos, 5).AddText formatCurrency(nOrderSubTotal), "size=9; expand=true; alignment=right", pFont
					end if
					
					'====================================== tax
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "8% SALES TAX" & vbcrlf & "(CA ONLY):", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nTaxAmount), "size=9; expand=true; alignment=right", pFont
					
					'====================================== shipping & handling
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "SHIPPING & HANDLING:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nShipFee), "size=9; expand=true; alignment=right", pFont
					
					'====================================== grand total
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "GRAND TOTAL:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nOrderGrandTotal), "size=9; expand=true; alignment=right", pFont

					'====================================== total paid
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "TOTAL PAID:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nOrderGrandTotal * -1.0), "size=9; expand=true; alignment=right", pFont
					
					'====================================== total due
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "TOTAL DUE:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(0.0), "size=9; expand=true; alignment=right", pFont					

					if 5 = tOrderTypeID then 
						'do nothing for now
					else
						curRowPos = addRowsToTable(1, 5, pTable)					
						set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/SalesOrders/PDFcoupon3.jpg")
						pParam("x") = 325		:	pParam("y") = 5
						pParam("ScaleX") = .5	:	pParam("ScaleY") = .5
						pTable(curRowPos, 1).ColSpan = 5
						pTable(curRowPos, 1).Height = 70
						pTable(curRowPos, 1).Canvas.DrawImage oImage, pParam
						
						if pObjRsOrders("shoprunnerid") <> "" then
							srText = "Thanks for using FREE 2-Day Shipping by ShopRunner! For returns, visit https://www.shoprunner.com/returns to print your pre-paid return label. Don't forget, invite a Friend to join."
							
							set oSRTable = pDoc.CreateTable("rows=1; cols=1; width=250; height=1; CellBorder=0; border=0")
							oSRTable.Font = pDoc.Fonts("Arial")
							oSRTable(1, 1).AddText srText, "size=9; expand=true; alignment=left", pFont						
							pTable(curRowPos, 1).Canvas.DrawTable oSRTable, "x=10, y=60"
						else
							srText = "Next time get FREE 2-Day Shipping from Wireless Emporium and other great retailers with your ShopRunner membership. Visit https://www.wirelessemporium.com/shoprunner to sign up for a FREE 30-Day Trial"
							
							set oSRTable = pDoc.CreateTable("rows=1; cols=1; width=250; height=1; CellBorder=0; border=0")
							oSRTable.Font = pDoc.Fonts("Arial")
							oSRTable(1, 1).AddText srText, "size=9; expand=true; alignment=left", pFont						
							pTable(curRowPos, 1).Canvas.DrawTable oSRTable, "x=10, y=60"
						end if
					end if
				case 1
					nCompareSubTotal = nOrderSubTotal + nShipFee
					nTaxAmount = 0
					if nCompareSubTotal <> nOrderGrandTotal then 
						nTaxAmount = cDbl(nOrderGrandTotal) - nCompareSubTotal 
					end if
					
					nDiscount = abs(cDbl(gblSubTotalPrice) - nOrderSubTotal)
					if nDiscount > 0 then nDiscount = nDiscount * -1 end if					
			
					curRowPos = addRowsToTable(1, 1, pTable)
					pTable(curRowPos, 1).ColSpan = 5
					pTable(curRowPos, 1).AddText " ", "size=12; expand=true; alignment=center", pFont
					
					nRowSpan = 7
					ret = addRowsToTable(nRowSpan, 5, pTable)
					curRowPos = curRowPos + 1
					pTable(curRowPos, 1).ColSpan = 2
					pTable(curRowPos, 1).RowSpan = nRowSpan
					pTable(curRowPos, 1).Height = 122

					set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/cart/CA-Return-Policy.gif")
					pParam("x") = 12		:	pParam("y") = 3
					pParam("ScaleX") = .5	:	pParam("ScaleY") = .5
					pTable(curRowPos, 1).Canvas.DrawImage oImage, pParam

					'====================================== discount
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).Height = 17
					pTable(curRowPos, 3).AddText "DISCOUNT:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nDiscount), "size=9; expand=true; alignment=right", pFont
					
					'====================================== subtotal
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "SUBTOTAL:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nOrderSubTotal), "size=9; expand=true; alignment=right", pFont
					
					'====================================== tax
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "8% SALES TAX" & vbcrlf & "(CA ONLY):", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nTaxAmount), "size=9; expand=true; alignment=right", pFont					
					
					'====================================== shipping
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "SHIPPING & HANDLING:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nShipFee), "size=9; expand=true; alignment=right", pFont					
					
					'====================================== grand total
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "GRAND TOTAL:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nOrderGrandTotal), "size=9; expand=true; alignment=right", pFont
					
					'====================================== total paid
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "TOTAL PAID:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nOrderGrandTotal * -1.0), "size=9; expand=true; alignment=right", pFont					
					
					'====================================== total due
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "TOTAL DUE:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(0.0), "size=9; expand=true; alignment=right", pFont										
				case 2
					curRowPos = addRowsToTable(1, 1, pTable)
					pTable(curRowPos, 1).ColSpan = 5
					pTable(curRowPos, 1).AddText " ", "size=12; expand=true; alignment=center", pFont
					
					nRowSpan = 7
					if nBuysafeamount > 0 then nRowSpan = nRowSpan + 1 end if

					ret = addRowsToTable(nRowSpan, 5, pTable)
					curRowPos = curRowPos + 1
					pTable(curRowPos, 1).ColSpan = 2
					pTable(curRowPos, 1).RowSpan = nRowSpan
					pTable(curRowPos, 1).Height = 140
					
'					set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/cart/co_policy3.gif")
					set oImage = pDoc.OpenImage("c:\Inetpub\wwwroot\wirelessemporium.com\www\images\CO-Return-Policy.gif")
					
					pParam("x") = 12		:	pParam("y") = 1
					pParam("ScaleX") = .5	:	pParam("ScaleY") = .5
					pTable(curRowPos, 1).Canvas.DrawImage oImage, pParam
					
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).Height = 18
					pTable(curRowPos, 3).AddText "DISCOUNT:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nDiscount), "size=9; expand=true; alignment=right", pFont

					if nBuysafeamount > 0 then
						curRowPos = curRowPos + 1					
						pTable(curRowPos, 3).ColSpan = 2
						pTable(curRowPos, 3).Height = 18
						pTable(curRowPos, 3).AddText "buySAFE Bond:", "size=9; expand=true; alignment=left", pFont
						pTable(curRowPos, 5).AddText formatCurrency(nBuysafeamount), "size=9; expand=true; alignment=right", pFont
					end if
					
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).Height = 18
					pTable(curRowPos, 3).AddText "SUBTOTAL:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nOrderSubTotal), "size=9; expand=true; alignment=right", pFont
					
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "8% SALES TAX" & vbcrlf & "(CA ONLY):", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nTaxAmount), "size=9; expand=true; alignment=right", pFont					
					
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "SHIPPING & HANDLING:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nShipFee), "size=9; expand=true; alignment=right", pFont
					
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).Height = 18					
					pTable(curRowPos, 3).AddText "GRAND TOTAL:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nOrderGrandTotal), "size=9; expand=true; alignment=right", pFont
					
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).Height = 18					
					pTable(curRowPos, 3).AddText "TOTAL PAID:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nOrderGrandTotal * -1.0), "size=9; expand=true; alignment=right", pFont
					
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).Height = 18					
					pTable(curRowPos, 3).AddText "TOTAL DUE:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(0.0), "size=9; expand=true; alignment=right", pFont					
				case 3
					nCompareSubTotal = nOrderSubTotal + nShipFee + nBuysafeamount
					nTaxAmount = 0
					if nCompareSubTotal <> nOrderGrandTotal then 
						nTaxAmount = cDbl(nOrderGrandTotal) - nCompareSubTotal 
					end if
									
					curRowPos = addRowsToTable(1, 1, pTable)
					pTable(curRowPos, 1).ColSpan = 5
					pTable(curRowPos, 1).AddText " ", "size=12; expand=true; alignment=center", pFont
					
					nRowSpan = 7
					if nBuysafeamount > 0 then nRowSpan = nRowSpan + 1 end if			
				
					ret = addRowsToTable(nRowSpan, 5, pTable)
					curRowPos = curRowPos + 1
					pTable(curRowPos, 1).ColSpan = 2
					pTable(curRowPos, 1).RowSpan = nRowSpan
					pTable(curRowPos, 1).Height = 122

					set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/cart/PS-Return-Policy.gif")			

					pParam("x") = 12		:	pParam("y") = 3
					pParam("ScaleX") = .5	:	pParam("ScaleY") = .5
					pTable(curRowPos, 1).Canvas.DrawImage oImage, pParam
			
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).Height = 17
					pTable(curRowPos, 3).AddText "DISCOUNT:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nDiscount), "size=9; expand=true; alignment=right", pFont

					if nBuysafeamount > 0 then
						curRowPos = curRowPos + 1					
						pTable(curRowPos, 3).ColSpan = 2
						pTable(curRowPos, 3).AddText "buySAFE Bond:", "size=9; expand=true; alignment=left", pFont
						pTable(curRowPos, 5).AddText formatCurrency(nBuysafeamount), "size=9; expand=true; alignment=right", pFont
					end if
					
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "SUBTOTAL:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nOrderSubTotal), "size=9; expand=true; alignment=right", pFont
					
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "8% SALES TAX" & vbcrlf & "(CA ONLY):", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nTaxAmount), "size=9; expand=true; alignment=right", pFont					
					
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "SHIPPING & HANDLING:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nShipFee), "size=9; expand=true; alignment=right", pFont
					
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "GRAND TOTAL:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nOrderGrandTotal), "size=9; expand=true; alignment=right", pFont
					
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "TOTAL PAID:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nOrderGrandTotal * -1.0), "size=9; expand=true; alignment=right", pFont
					
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "TOTAL DUE:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(0.0), "size=9; expand=true; alignment=right", pFont	
				case 10
					nCompareSubTotal = nOrderSubTotal + nShipFee
					nTaxAmount = 0
					if nCompareSubTotal <> nOrderGrandTotal then 
						nTaxAmount = cDbl(nOrderGrandTotal) - nCompareSubTotal 
					end if
			
					curRowPos = addRowsToTable(1, 1, pTable)
					pTable(curRowPos, 1).ColSpan = 5
					pTable(curRowPos, 1).AddText " ", "size=12; expand=true; alignment=center", pFont
					
					nRowSpan = 7
					ret = addRowsToTable(nRowSpan, 5, pTable)
					curRowPos = curRowPos + 1
					pTable(curRowPos, 1).ColSpan = 2
					pTable(curRowPos, 1).RowSpan = nRowSpan
					pTable(curRowPos, 1).Height = 122

'					set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/cart/ca_policy3.gif")
'					pParam("x") = 12		:	pParam("y") = 3
'					pParam("ScaleX") = .5	:	pParam("ScaleY") = .5
'					pTable(curRowPos, 1).Canvas.DrawImage oImage, pParam

					'====================================== discount
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).Height = 17
					pTable(curRowPos, 3).AddText "DISCOUNT:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nDiscount), "size=9; expand=true; alignment=right", pFont
					
					'====================================== subtotal
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "SUBTOTAL:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nOrderSubTotal), "size=9; expand=true; alignment=right", pFont
					
					'====================================== tax
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "8% SALES TAX" & vbcrlf & "(CA ONLY):", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nTaxAmount), "size=9; expand=true; alignment=right", pFont					
					
					'====================================== shipping
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "SHIPPING & HANDLING:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nShipFee), "size=9; expand=true; alignment=right", pFont					
					
					'====================================== grand total
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "GRAND TOTAL:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nOrderGrandTotal), "size=9; expand=true; alignment=right", pFont
					
					'====================================== total paid
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "TOTAL PAID:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(nOrderGrandTotal * -1.0), "size=9; expand=true; alignment=right", pFont					
					
					'====================================== total due
					curRowPos = curRowPos + 1
					pTable(curRowPos, 3).ColSpan = 2
					pTable(curRowPos, 3).AddText "TOTAL DUE:", "size=9; expand=true; alignment=left", pFont
					pTable(curRowPos, 5).AddText formatCurrency(0.0), "size=9; expand=true; alignment=right", pFont						
			end select
		end if
	end if
end sub


sub appendPromoItemPDF(byref pDoc, byref pParam, byref pPage, byref pFont, byref pObjRsOrders, byref pTable)
	curMasterQty		=	pObjRsOrders("curMasterQty")
	tItemDesc			=	pObjRsOrders("promoItemDesc")
	tOrderTypeID 		= 	pObjRsOrders("ordertypeid")
	nQty 				= 	pObjRsOrders("promoItemQty")
	itemPrice 			= 	0
	itemTotalPrice 		= 	cDbl(nQty) * cDbl(itemPrice)

	gblSubTotalPrice	=	gblSubTotalPrice + itemTotalPrice
	gblCurItemQty		=	gblCurItemQty + clng(nQty)
	
	tPartNumber	= pObjRsOrders("promoPartnumber")

	strQty = cstr(nQty)	
	qtyFontSize = 12
	set qtyFont = pFont
	set descFont = pFont
	if nQty > 1 then 
		qtyFontSize = 14
		if curMasterQty = 0 then 
			set qtyFont = pDoc.Fonts("Times-BoldItalic")
			set descFont = pDoc.Fonts("Times-Italic")
		else
			set qtyFont = pDoc.Fonts("Helvetica-Bold")
		end if
	elseif curMasterQty = 0 then 
		set descFont = pDoc.Fonts("Times-Italic")
		set qtyFont = pDoc.Fonts("Times-Italic")
	end if

	curRowPos = addRowsToTable(1, 1, pTable)
			
	pTable(curRowPos, 1).AddText tPartNumber, "size=12; expand=true; alignment=center", pFont
	pTable(curRowPos, 2).AddText "*Promo: " & switchChars(tItemDesc), "size=9; expand=true; alignment=left", descFont
	if pObjRsOrders("isVendorOrder") then
		pTable(curRowPos, 2).AddText vbcrlf & vbcrlf & vbcrlf & "(This item will ship separately from the manufacturer.)", "size=7; expand=true; alignment=left", descFont
	end if
	pTable(curRowPos, 3).AddText strQty, "size=" & qtyFontSize & "; expand=true; alignment=center", qtyFont
	pTable(curRowPos, 4).AddText formatCurrency(itemPrice), "size=9; expand=true; alignment=right", pFont
	pTable(curRowPos, 5).AddText formatCurrency(itemTotalPrice), "size=9; expand=true; alignment=right", pFont

	'faceplate and covers image rendering on the bottom
	if pObjRsOrders("numOrders") <= 6 then
		if pObjRsOrders("site_id") = 2 then
			ImgFolderPath = "C:\inetpub\wwwroot\productpics_co\thumb\"
		else
			ImgFolderPath = "C:\inetpub\wwwroot\productpics\thumb\"		
		end if
		itemImgPath = ImgFolderPath & pObjRsOrders("promoItemImg")
		
		if fso.FileExists(itemImgPath) and not isnull(pObjRsOrders("promoPartnumber")) then
			pPage.Canvas.DrawText pObjRsOrders("promoPartnumber"), "x=" & gblCoverThumbImagePosX & ", y=" & gblCoverThumbImagePosY & "; size=9; expand=true; alignment=left;", pFont

			gblCoverThumbImagePosX	= gblCoverThumbImagePosX + 15
			pParam("x") = gblCoverThumbImagePosX
			pParam("y") = gblCoverThumbImagePosY
			pParam("ScaleX") = .5
			pParam("ScaleY") = .5
			set oImage = pDoc.OpenImage(itemImgPath)
			pPage.Canvas.DrawImage oImage, pParam

			'============ set bunkered item image
			bunkerSQL = "select	top 1 partnumber" & vbcrlf & _
						"from	we_ItemsExtendedData" & vbcrlf & _
						"where	bunker = 1" & vbcrlf & _
						"	and	partnumber = '" & pObjRsOrders("promoPartnumber") & "'"
			set rsBunker = oConn.execute(bunkerSQL)
			if not rsBunker.eof then
				bunkerImgPath = "c:\inetpub\wwwroot\wirelessemporium.com\www\images\icons\bunker1.png"
				if fso.FileExists(bunkerImgPath) then
					set oBunkerImage = pDoc.OpenImage(bunkerImgPath)
					pParam("y") = gblCoverThumbImagePosY + 45
					pPage.Canvas.DrawImage oBunkerImage, pParam
				end if
			end if
			'============// set bunkered item image			

			gblCoverThumbImagePosX	= gblCoverThumbImagePosX + 75
			if gblCoverThumbImagePosX = 580 then
				gblCoverThumbImagePosX	= 40
				gblCoverThumbImagePosY = gblCoverThumbImagePosY + 60
			end if
		end if
	end if
		
	if gblCurItemQty = pObjRsOrders("itemTotalQty") then
		call appendFooter(pDoc, pParam, pFont, pTable, pObjRsOrders)
	end if
end sub

sub appendItemPDF(byref pDoc, byref pParam, byref pPage, byref pFont, byref pObjRsOrders, byref pTable)
	curMasterQty		=	pObjRsOrders("curMasterQty")
	tItemDesc			=	pObjRsOrders("itemdesc")
	tOrderTypeID 		= 	pObjRsOrders("ordertypeid")
	nQty 				= 	pObjRsOrders("quantity")
	itemPrice 			= 	pObjRsOrders("price")

	itemTotalPrice 		= 	cDbl(nQty) * cDbl(itemPrice)

	gblSubTotalPrice	=	gblSubTotalPrice + itemTotalPrice
	gblCurItemQty		=	gblCurItemQty + clng(nQty)
	
'	if len(pObjRsOrders("partnumber2")) > 0 then
'		tPartNumber	= replace(pObjRsOrders("partnumber2"), ",", "," & vbcrlf)
'	else
		tPartNumber	= pObjRsOrders("partnumber")
'	end if

	' Send an e-mail if this order contains a cell phone!
	if pObjRsOrders("typeID") = 16 then
		cdo_body = "<html>" & vbcrlf
		cdo_body = cdo_body & "<head>" & vbcrlf
		cdo_body = cdo_body & "<style type='text/css'>" & vbcrlf
		cdo_body = cdo_body & "<!--" & vbcrlf
		cdo_body = cdo_body & "..regText {  font-family: Arial; font-size: 9pt}" & vbcrlf
		cdo_body = cdo_body & "..header {  font-family: Arial; font-size: 11pt}" & vbcrlf
		cdo_body = cdo_body & "-->" & vbcrlf
		cdo_body = cdo_body & "</style>" & vbcrlf
		cdo_body = cdo_body & "</head>" & vbcrlf
		cdo_body = cdo_body & "<body class='regText'>" & vbcrlf
		cdo_body = cdo_body & "<p>Order #" & pObjRsOrders("orderid") & " contains the following cell phone:</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Item #: " & pObjRsOrders("itemid") & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Part #: " & tPartNumber & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Qty: " & nQty & "</p>" & vbcrlf
		cdo_body = cdo_body & "</body></html>" & vbcrlf
		cdo_to = "tony@wirelessemporium.com,charles@wirelessemporium.com,ruben@wirelessemporium.com,terry@wirelessemporium.com"
'		cdo_to = "terry@wirelessemporium.com"
		cdo_from = "sales@wirelessemporium.com"
		cdo_subject = "*** New CELL PHONE Order! ***"
'		call cdosend(cdo_to, cdo_from, cdo_subject, cdo_body)
	end if

	strQty = cstr(nQty)	
	qtyFontSize = 12
	set qtyFont = pFont
	set descFont = pFont
	if nQty > 1 then 
		qtyFontSize = 14
		if curMasterQty = 0 then 
			set qtyFont = pDoc.Fonts("Times-BoldItalic")
			set descFont = pDoc.Fonts("Times-Italic")
		else
			set qtyFont = pDoc.Fonts("Helvetica-Bold")
		end if
	elseif curMasterQty = 0 then 
		set descFont = pDoc.Fonts("Times-Italic")
		set qtyFont = pDoc.Fonts("Times-Italic")
	end if

	select case pObjRsOrders("site_id")
		case 0
			if 5 = tOrderTypeID then		'atomic mall
				curRowPos = addRowsToTable(1, 1, pTable)

				pTable(curRowPos, 1).AddText tPartNumber, "size=12; expand=true; alignment=center", pFont
				pTable(curRowPos, 2).ColSpan = 3
				pTable(curRowPos, 2).AddText switchChars(tItemDesc), "size=9; expand=true; alignment=left", descFont
				if pObjRsOrders("isVendorOrder") then
					pTable(curRowPos, 2).AddText vbcrlf & vbcrlf & vbcrlf & "(This item will ship separately from the manufacturer.)", "size=7; expand=true; alignment=left", descFont
				end if
				pTable(curRowPos, 5).AddText strQty, "size=" & qtyFontSize & "; expand=true; alignment=right", qtyFont
			elseif 8 = tOrderTypeID then		'Sears
				curRowPos = addRowsToTable(1, 1, pTable)

				pTable(curRowPos, 1).AddText tPartNumber, "size=12; expand=true; alignment=center", pFont
				pTable(curRowPos, 2).ColSpan = 3
				pTable(curRowPos, 2).AddText switchChars(tItemDesc), "size=9; expand=true; alignment=left", descFont
				if pObjRsOrders("isVendorOrder") then
					pTable(curRowPos, 2).AddText vbcrlf & vbcrlf & vbcrlf & "(This item will ship separately from the manufacturer.)", "size=7; expand=true; alignment=left", descFont
				end if
				pTable(curRowPos, 5).AddText strQty, "size=" & qtyFontSize & "; expand=true; alignment=right", qtyFont
			elseif 9 = tOrderTypeID then		'Bestbuy
				curRowPos = addRowsToTable(1, 1, pTable)

				pTable(curRowPos, 1).AddText tPartNumber, "size=12; expand=true; alignment=center", pFont
				pTable(curRowPos, 2).ColSpan = 3
				pTable(curRowPos, 2).AddText switchChars(tItemDesc), "size=9; expand=true; alignment=left", descFont
				if pObjRsOrders("isVendorOrder") then
					pTable(curRowPos, 2).AddText vbcrlf & vbcrlf & vbcrlf & "(This item will ship separately from the manufacturer.)", "size=7; expand=true; alignment=left", descFont
				end if
				pTable(curRowPos, 5).AddText strQty, "size=" & qtyFontSize & "; expand=true; alignment=right", qtyFont
			elseif 10 = tOrderTypeID then		'Newegg
				curRowPos = addRowsToTable(1, 1, pTable)

				pTable(curRowPos, 1).AddText tPartNumber, "size=12; expand=true; alignment=center", pFont
				pTable(curRowPos, 2).ColSpan = 3
				pTable(curRowPos, 2).AddText switchChars(tItemDesc), "size=9; expand=true; alignment=left", descFont
				if pObjRsOrders("isVendorOrder") then
					pTable(curRowPos, 2).AddText vbcrlf & vbcrlf & vbcrlf & "(This item will ship separately from the manufacturer.)", "size=7; expand=true; alignment=left", descFont
				end if
				pTable(curRowPos, 5).AddText strQty, "size=" & qtyFontSize & "; expand=true; alignment=right", qtyFont
			else
				curRowPos = addRowsToTable(1, 1, pTable)
							
				pTable(curRowPos, 1).AddText tPartNumber, "size=12; expand=true; alignment=center", pFont
				pTable(curRowPos, 2).AddText switchChars(tItemDesc), "size=9; expand=true; alignment=left", descFont
				if pObjRsOrders("isVendorOrder") then
					pTable(curRowPos, 2).AddText vbcrlf & vbcrlf & vbcrlf & "(This item will ship separately from the manufacturer.)", "size=7; expand=true; alignment=left", descFont
				end if
				pTable(curRowPos, 3).AddText strQty, "size=" & qtyFontSize & "; expand=true; alignment=center", qtyFont
				pTable(curRowPos, 4).AddText formatCurrency(itemPrice), "size=9; expand=true; alignment=right", pFont
				pTable(curRowPos, 5).AddText formatCurrency(itemTotalPrice), "size=9; expand=true; alignment=right", pFont
			end if
		case 1, 2, 3, 10
			curRowPos = addRowsToTable(1, 1, pTable)
			
			pTable(curRowPos, 1).AddText tPartNumber, "size=12; expand=true; alignment=center", pFont
			pTable(curRowPos, 2).AddText switchChars(tItemDesc), "size=9; expand=true; alignment=left", descFont
			if pObjRsOrders("isVendorOrder") then
				pTable(curRowPos, 2).AddText vbcrlf & vbcrlf & vbcrlf & "(This item will ship separately from the manufacturer.)", "size=7; expand=true; alignment=left", descFont
			end if
			pTable(curRowPos, 3).AddText strQty, "size=" & qtyFontSize & "; expand=true; alignment=center", qtyFont
			pTable(curRowPos, 4).AddText formatCurrency(itemPrice), "size=9; expand=true; alignment=right", pFont
			pTable(curRowPos, 5).AddText formatCurrency(itemTotalPrice), "size=9; expand=true; alignment=right", pFont
	end select

	'faceplate and covers image rendering on the bottom
	if pObjRsOrders("numOrders") <= 6 then
		if pObjRsOrders("site_id") = 2 then
			ImgFolderPath = "C:\inetpub\wwwroot\productpics_co\thumb\"
		else
			ImgFolderPath = "C:\inetpub\wwwroot\productpics\thumb\"		
		end if
		itemImgPath = ImgFolderPath & pObjRsOrders("itempic")
		
		if fso.FileExists(itemImgPath) and not isnull(pObjRsOrders("partnumber")) then
			pPage.Canvas.DrawText pObjRsOrders("partnumber"), "x=" & gblCoverThumbImagePosX & ", y=" & gblCoverThumbImagePosY & "; size=9; expand=true; alignment=left;", pFont

			gblCoverThumbImagePosX	= gblCoverThumbImagePosX + 15
			pParam("x") = gblCoverThumbImagePosX
			pParam("y") = gblCoverThumbImagePosY
			pParam("ScaleX") = .5
			pParam("ScaleY") = .5
			set oImage = pDoc.OpenImage(itemImgPath)
			pPage.Canvas.DrawImage oImage, pParam

			'============ set bunkered item image
			bunkerSQL = "select	top 1 partnumber" & vbcrlf & _
						"from	we_ItemsExtendedData" & vbcrlf & _
						"where	bunker = 1" & vbcrlf & _
						"	and	partnumber = '" & pObjRsOrders("partnumber") & "'"
			set rsBunker = oConn.execute(bunkerSQL)
			if not rsBunker.eof then
				bunkerImgPath = "c:\inetpub\wwwroot\wirelessemporium.com\www\images\icons\bunker1.png"
				if fso.FileExists(bunkerImgPath) then
					set oBunkerImage = pDoc.OpenImage(bunkerImgPath)
					pParam("y") = gblCoverThumbImagePosY + 45
					pPage.Canvas.DrawImage oBunkerImage, pParam
				end if
			end if
			'============// set bunkered item image			

			gblCoverThumbImagePosX	= gblCoverThumbImagePosX + 75
			if gblCoverThumbImagePosX = 580 then
				gblCoverThumbImagePosX	= 40
				gblCoverThumbImagePosY = gblCoverThumbImagePosY + 60
			end if
		end if
	end if
		
	if gblCurItemQty = pObjRsOrders("itemTotalQty") then
		call appendFooter(pDoc, pParam, pFont, pTable, pObjRsOrders)
	end if
end sub

sub savePages(byref pDoc, pPathToSave)
	if len(pPathToSave) > 0 and isobject(pDoc) then
		if pDoc.Pages.Count > 0 then
'			if pFso.FileExists(pPathToSave) then 
'				response.write "deleted : " & pPathToSave & "<br>"
'				pFso.DeleteFile(pPathToSave)
'			end if
'			response.write pDoc.Pages.Count & " pages saved on " & pPathToSave & "<br>"			
			pDoc.Save pPathToSave
		end if
	end if
end sub

sub drawTable(byref pPage, byref pTable, pX, pY)
	if isobject(pTable) and isobject(pPage) then
		if pTable.Rows.Count > 0 then
			pPage.Canvas.DrawTable pTable, "x=" & pX & ", y=" & pY
		end if		
	end if
end sub

function addRowsToTable(nRows, nHeights, byref pTable)
	if isobject(pTable) then
		for i=1 to nRows
			pTable.Rows.Add(nHeights)			
		next
		addRowsToTable = pTable.Rows.Count
		exit function
	end if
	addRowsToTable = 0
end function

sub initPDF(byref pPdf, byref pDoc, byref pParam)
	set pPdf	= nothing
	set pDoc	= nothing
	set pParam 	= nothing
		
	set pPdf 	= CreateObject("Persits.Pdf")
	set pDoc 	= pPdf.CreateDocument
	set pParam 	= pPdf.CreateParam	
end sub

sub drawLogo(byref pDoc, byref pPage, byref pParam, byref pObjRsOrders)
	select case pObjRsOrders("site_id")
		case 0
			set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/SalesOrders/WElogo_STANDARD.gif")
			pParam("x") = 30 		: pParam("y") = 720
			pParam("ScaleX") = .5 	: pParam("ScaleY") = .5
			pPage.Canvas.DrawImage oImage, pParam
		
			if 5 = pObjRsOrders("ordertypeid") then		'atomic mall
				set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/SalesOrders/logoAtomicMall.gif")
				pParam("x") = 220		:	pParam("y") = 713
				pParam("ScaleX") = .8	:	pParam("ScaleY") = .8
				pPage.Canvas.DrawImage oImage, pParam
			elseif 8 = pObjRsOrders("ordertypeid") then		'Sears
				set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/sears.jpg")
				pParam("x") = 130		:	pParam("y") = 713
				pParam("ScaleX") = .8	:	pParam("ScaleY") = .8
				pPage.Canvas.DrawImage oImage, pParam			
			elseif 9 = pObjRsOrders("ordertypeid") then		'Bestbuy
				set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/SalesOrders/bestbuylogo.jpg")
				pParam("x") = 130		:	pParam("y") = 695
				pParam("ScaleX") = .7	:	pParam("ScaleY") = .7
				pPage.Canvas.DrawImage oImage, pParam
			elseif 10 = pObjRsOrders("ordertypeid") then		'Newegg
				set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/logo_newegg.jpg")
				pParam("x") = 130		:	pParam("y") = 680
				pParam("ScaleX") = .7	:	pParam("ScaleY") = .7
				pPage.Canvas.DrawImage oImage, pParam
			elseif "" <> pObjRsOrders("shoprunnerid") then	'shoprunner
				set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/icons/shopRunner.gif")
				pParam("x") = 130		:	pParam("y") = 705
				pParam("ScaleX") = .7	:	pParam("ScaleY") = .7
				pPage.Canvas.DrawImage oImage, pParam
			end if
		case 1
			set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/logo_STANDARD.jpg")
			pParam("x") = 40		:	pParam("y") = 735
			pParam("ScaleX") = .5	:	pParam("ScaleY") = .5
			pPage.Canvas.DrawImage oImage, pParam
		case 2
'			set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/cart/CO_SalesOrder_logo.jpg")
			set oImage = pDoc.OpenImage("c:\Inetpub\wwwroot\wirelessemporium.com\www/images/CO_SalesOrder_logo2.jpg")
			pParam("x") = 41		:	pParam("y") = 725
			pParam("ScaleX") = .4	:	pParam("ScaleY") = .4
			pPage.Canvas.DrawImage oImage, pParam
		case 3
			set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/phonesale.jpg")
			pParam("x") = 40		:	pParam("y") = 700
			pParam("ScaleX") = .7	:	pParam("ScaleY") = .7
			pPage.Canvas.DrawImage oImage, pParam
		case 10
			set oImage = pDoc.OpenImage(pObjRsOrders("iis_home") & "\" & "/images/logo.gif")
			pParam("x") = 40		:	pParam("y") = 725
			pParam("ScaleX") = .6	:	pParam("ScaleY") = .6
			pPage.Canvas.DrawImage oImage, pParam
	end select
end sub 

sub drawCompanyAddress(byref pPage, byref pParam, byref pFont, byref pObjRsOrders)
	strAddress = "<p>1410 N. Batavia St.<br>Orange, CA 92867</p>"
	select case pObjRsOrders("site_id")
		case 0
			pParam("x") = 133	:	pParam("y") = 761
			pParam("size") = 9	:	pParam("HTML") = true
			pPage.Canvas.DrawText strAddress, pParam, pFont
		case 1
			pParam("x") = 45	:	pParam("y") = 730
			pParam("size") = 9	:	pParam("HTML") = true
			pPage.Canvas.DrawText strAddress, pParam, pFont
		case 2
			pParam("x") = 45	:	pParam("y") = 725
			pParam("size") = 9	:	pParam("HTML") = true
			pPage.Canvas.DrawText strAddress, pParam, pFont
		case 3
			pParam("x") = 190	:	pParam("y") = 761
			pParam("size") = 9	:	pParam("HTML") = true
			pPage.Canvas.DrawText strAddress, pParam, pFont
		case 10
			pParam("x") = 40	:	pParam("y") = 725
			pParam("size") = 9	:	pParam("HTML") = true
			pPage.Canvas.DrawText strAddress, pParam, pFont
	end select
end sub 

sub drawBarcode(byref pPage, byref pObjRsOrders)
	strData = pObjRsOrders("orderid")
	strParam = "x=290; y=716; height=25; width=125; DrawText=false; type=17"
	pPage.Canvas.DrawBarcode strData, strParam	
end sub 

sub drawHeaders(byref pDoc, byref pPage, byref pParam, byref pFont, byref pObjRsOrders)
	strText = "<p><font color=""#FF0000"">Multi-Item Order</font></p>"
	isMultiOrder = false
	if isnumeric(pObjRsOrders("itemTotalQty")) then
		if clng(pObjRsOrders("itemTotalQty")) > 1 then
			isMultiOrder = true
		end if
	end if

	if isMultiOrder then
		pParam("x") = 315	:	pParam("y") = 761
		pParam("size") = 12	:	pParam("HTML") = true
		pPage.Canvas.DrawText strText, pParam, pFont			
	end if

	pParam("size") = 9
	pParam("color") = "&H000000"
	
	'receipt number and date ordered
	set oTable = pDoc.CreateTable("rows=3; cols=2; width=150; height=1; CellBorder=.1; border=0")
	oTable.Font = pDoc.Fonts("Arial")
	oTable(1, 1).ColSpan = 2
	oTable(1, 1).AddText "Sales Receipt", "size=12; color=&H000000; expand=true; alignment=center", pFont
	oTable(2, 1).AddText "Date", "size=11; expand=true; alignment=center", pFont
	oTable(2, 2).AddText "Order #", "size=11; expand=true; alignment=center", pParam
	oTable(3, 1).AddText dateValue(pObjRsOrders("orderdatetime")), "size=14; expand=true; alignment=center", pFont
	oTable(3, 2).AddText pObjRsOrders("orderid"), "size=14; expand=true; alignment=center", pFont
	pPage.Canvas.DrawTable oTable, "x=433, y=760"
	
	if gblTotalPageNum > 1 then
		strPagination = "Page " & gblCurPageNum & " / " & gblTotalPageNum
		set tblPagination = pDoc.CreateTable("rows=1; cols=1; width=150; height=1; CellBorder=0; border=0")
		tblPagination.Font = pDoc.Fonts("Arial")
		tblPagination(1, 1).AddText strPagination, "size=12; color=&H000000; expand=true; alignment=right", pFont
		pPage.Canvas.DrawTable tblPagination, "x=433, y=775"
	end if
	
	call drawShippingAddr(pDoc, pPage, pParam, pFont, pObjRsOrders)
	call drawBillingAddr(pDoc, pPage, pFont, pObjRsOrders)	
end sub 

sub drawShippingAddr(byref pDoc, byref pPage, byref pParam, byref pFont, byref pObjRsOrders)
	with pPage.Canvas
		.MoveTo 334, 676
		.LineTo 582, 676
		.ClosePath
		.Stroke
	end with
	
	'if there is an optional shipping address use them.	
	if pObjRsOrders("shippingid") > 0 and len(pObjRsOrders("ssAddress1")) > 0 and len(pObjRsOrders("ssCity")) > 0 then
		sAddress1 	= 	pObjRsOrders("ssAddress1")
		sAddress2 	= 	pObjRsOrders("ssAddress2")
		sCity 		= 	pObjRsOrders("ssCity")
		sState 		= 	pObjRsOrders("ssState")
		sZip 		= 	pObjRsOrders("ssZip")
	else
		sAddress1 	= 	pObjRsOrders("sAddress1")
		sAddress2 	= 	pObjRsOrders("sAddress2")
		sCity 		= 	pObjRsOrders("sCity")
		sState 		= 	pObjRsOrders("sState")
		sZip 		= 	pObjRsOrders("sZip")	
	end if
	
	tRows = 4	
	if len(sAddress2) > 0 then 
		tRows = 5
	end if

	Set oTable = pDoc.CreateTable("rows=" & tRows & "; cols=1; width=250; height=1; CellBorder=0; border=1")
	oTable.Font = pDoc.Fonts("Arial")
'	oConn.execute("insert into _terry(msg) values('orderid:" & objRsOrders("orderid") & ":" & replace(sAddress1, "'", "''") & "')")	
	oTable(1, 1).AddText "Shipping Address", "size=10; expand=true; CellBorder=1; alignment=center", pFont
	oTable(2, 1).AddText switchChars(" " & pObjRsOrders("fname") & " " & pObjRsOrders("lname")), "size=9; expand=true; alignment=left", pFont
	oTable(3, 1).AddText switchChars(" " & sAddress1), "size=9; expand=true; alignment=left", pFont
	if tRows = 5 then
		oTable(4, 1).AddText switchChars(" " & sAddress2), "size=9; expand=true; alignment=left", pFont
	end if
	oTable(tRows, 1).AddText switchChars(" " & sCity & ", " & sState & "  " & sZip), "size=9; expand=true; alignment=left", pFont
	pPage.Canvas.DrawTable oTable, "x=333, y=690"

	select case pObjRsOrders("site_id")
		case 0
			if left(sAddress1,20) = "955 Connecticut Ave." and sCity = "Bridgeport" and sZip = "06607" then
				strText = "<p><font color=""#FF0000"">International Shipping</font></p>"
				pParam("x") = 290
				pParam("y") = 712
				pParam("size") = 14
				pParam("HTML") = true
				pPage.Canvas.DrawText strText, pParam, pFont
			end if	
		case 1
		case 2
		case 3
		case 10
	end select
end sub

sub drawBillingAddr(byref pDoc, byref pPage, byref pFont, byref pObjRsOrders)
	with pPage.Canvas
		.MoveTo 43, 676
		.LineTo 291, 676
		.ClosePath
		.Stroke
	end with

	bAddress1 	= 	pObjRsOrders("bAddress1")
	bAddress2 	= 	pObjRsOrders("bAddress2")
	bCity 		= 	pObjRsOrders("bCity")
	bState 		= 	pObjRsOrders("bState")
	bZip 		= 	pObjRsOrders("bZip")
	phone		=	pObjRsOrders("phone")
	email		=	pObjRsOrders("email")
	fname		=	pObjRsOrders("fname")
	lname		=	pObjRsOrders("lname")
				
	tRows = 6
	if len(bAddress2) > 0 then 
		tRows = 7
	end if
	
	set oTable = pDoc.CreateTable("rows=" & tRows & "; cols=1; width=250; height=1; CellBorder=0; border=1")
	oTable.Font = pDoc.Fonts("Arial")
	oTable(1, 1).AddText "Billing Address", "size=10; expand=true; CellBorder=1; alignment=center", pFont
	oTable(2, 1).AddText switchChars(" " & fname & " " & lname), "size=9; expand=true; alignment=left", pFont
	oTable(3, 1).AddText switchChars(" " & bAddress1), "size=9; expand=true; alignment=left", pFont
	if tRows = 7 then oTable(4, 1).AddText switchChars(" " & bAddress2), "size=9; expand=true; alignment=left", pFont
	oTable(tRows - 2, 1).AddText switchChars(" " & bCity & ", " & bState & "  " & bZip), "size=9; expand=true; alignment=left", pFont
	oTable(tRows - 1, 1).AddText switchChars(" " & phone), "size=8; expand=true; alignment=left", pFont
	oTable(tRows, 1).AddText switchChars(" " & email), "size=8; expand=true; alignment=left", pFont
	pPage.Canvas.DrawTable oTable, "x=42, y=690"	
end sub

'still have no idea why this should be used. keep it for now.
function switchChars(myText)
	myText = replace(myText,chr(149),"-")
	myText = replace(myText,chr(150),"-")
	myText = replace(myText,chr(151),"-")
	myText = replace(myText,chr(153)," ")
	myText = replace(myText,chr(169)," ")
	myText = replace(myText,chr(174)," ")
	myText = replace(myText,chr(225)," ")
	myText = replace(myText,chr(252)," ")
	myText = replace(myText,chr(246)," ")
	myText = replace(myText,vbCrLf,"</p><p>")
	switchChars = myText
end function


sub cdosend(strto,strfrom,strsubject,strbody)
	on error resume next
	dim objMail
	set objMail = createobject("cdo.message")
	with objMail
		.from = strfrom
		.to = strto
		.subject = strsubject
		.htmlbody = cstr("" & strbody)
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.configuration.fields.item("urn:schemas:httpmail:sender") = "sales@wirelessemporium.com"
		.configuration.fields.update
		.send
	end with
	set objMail = nothing
	on error goto 0
end sub



'sub drawHeaders(byref pDoc, byref pPage, byref pParam, pSiteID, pOrderID)
'	Param("size") = 9
'	Param("color") = "&H000000"
'	
'	' add header
'	set Table = Doc.CreateTable("rows=3; cols=2; width=150; height=1; CellBorder=.1; border=0")
'	Table.Font = Doc.Fonts("Arial")
'	Table(1, 1).ColSpan = 2
'	Table(1, 1).AddText "Sales Receipt", "size=12; color=&H000000; expand=true; alignment=center", Font
'	Table(2, 1).AddText "Date", "size=11; expand=true; alignment=center", Font
'	Table(2, 2).AddText "Order #", "size=11; expand=true; alignment=center", Param
'	Table(3, 1).AddText dateValue(RSmc("OrderDateTime")), "size=14; expand=true; alignment=center", Font
'	Table(3, 2).AddText RSmc("OrderID"), "size=14; expand=true; alignment=center", Font
'	Page.Canvas.DrawTable Table, "x=433, y=760"
'end sub 

'set	folderPDF	=	fso.GetFolder(pdfFilePath)
'set	folderTXT	=	fso.GetFolder(txtFilePath)
'set	collPDF		=	folderPDF.Files
'set	collTXT		=	folderTXT.Files
'
'for each i in collPDF
'	if i.DateLastModified < dateAdd("M",-6,date) then
'		response.write "<font color=red>" & i.name & " " & i.DateLastModified & "<br></font>"
'	else
'		response.write i.name & " " & i.DateLastModified & "<br>"	
'	end if
'next
'
'for each i in collTXT
'	if i.DateLastModified < dateAdd("M",-6,date) then
'		response.write "<font color=red>" & i.name & " " & i.DateLastModified & "<br></font>"
'	else
'		response.write i.name & " " & i.DateLastModified & "<br>"	
'	end if
'next
'
'response.write "====================================<br><br><br>"

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
