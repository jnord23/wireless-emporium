set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

strMonth 	= right("00" & month(date), 2)
strDay 		= right("00" & day(date), 2)
strYear		= year(date)
strHour		= right("00" & hour(now), 2)
strMinute	= right("00" & minute(now), 2)
strDate		= strMonth & strDay & strYear & strHour & strMinute
customerID	= "1413"
inductionSiteID = "06"


strSDate	= date - 1
strEDate	= date

filename = customerID & inductionSiteID & strDate & "__SampleForAllStores.man"
path = "c:\Inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename

set fs = CreateObject("Scripting.FileSystemObject")
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if

'Create the file and write some data to it.
set file = fs.CreateTextFile(path)
sql = 	"select	x.newgistics_customerid n_id, a.orderid, a.accountid, a.shippingid, a.shiptype, a.trackingnum, a.scandate, convert(varchar(10), scandate, 20) scandate2" & vbcrlf & _
		"	,	isnull(b.sAddress1, '') sAddress1, isnull(b.sAddress2, '') sAddress2, isnull(b.sCity, '') sCity, isnull(b.sState, '') sState, isnull(b.sZip, '') sZip" & vbcrlf & _
		"	,	case when b.sCountry = 'canada' then 'CAN' else 'USA' end sCountry" & vbcrlf & _
		"	,	isnull(s.sAddress1, '') ssAddress1, isnull(s.sAddress2, '') ssAddress2, isnull(s.sCity, '') ssCity, isnull(s.sState, '') ssState, isnull(s.sZip, '') ssZip" & vbcrlf & _
		"	,	case when s.sCountry = 'canada' then 'CAN' else 'USA' end ssCountry" & vbcrlf & _
		"	,	isnull(b.fname, '') fname, isnull(b.lname, '') lname, isnull(b.phone, '') phone, isnull(b.email, '') email 	" & vbcrlf & _
		"from	we_orders a join v_accounts b" & vbcrlf & _
		"	on	a.accountid = b.accountid and a.store = b.site_id join xstore x" & vbcrlf & _
		"	on	a.store = x.site_id left outer join we_addl_shipping_addr s" & vbcrlf & _
		"	on	a.shippingid = s.id" & vbcrlf & _
		"where	a.orderid in (1290866,1290821,1291286,1291318,1290819,1291077,1284483,1283589,1289186)" & vbcrlf & _
		"order by 1"

set rs = CreateObject("ADODB.Recordset")
rs.open sql, oConn, 0, 1
if not rs.eof then
	do until rs.eof
		customerID = rs("n_id")
		orderid = rs("orderid")
		trackingnumber = rs("orderid")
		scandate = rs("scandate")
		scandate2 = rs("scandate2")
		customerName = replace(rs("fname") & " " & rs("lname"), ",", "")
		phone = rs("phone")
		email = rs("email")
		if rs("shippingid") > 0 and not isnull(rs("ssAddress1")) then
			sAddress1 = replace(rs("ssAddress1"), ",", "")
			sAddress2 = replace(rs("ssAddress2"), ",", "")
			sCity = replace(rs("ssCity"), ",", "")
			sState = replace(rs("ssState"), ",", "")
			sZip = replace(left(rs("ssZip"), 5), ",", "")
			sCountry = replace(rs("ssCountry"), ",", "")
		else
			sAddress1 = replace(rs("sAddress1"), ",", "")
			sAddress2 = replace(rs("sAddress2"), ",", "")
			sCity = replace(rs("sCity"), ",", "")
			sState = replace(rs("sState"), ",", "")
			sZip = replace(left(rs("sZip"), 5), ",", "")
			sCountry = replace(rs("sCountry"), ",", "")
		end if

		strline = 			"1.3,"										'version
		strline = strline & customerID & ","							'account number
		strline = strline & scandate2 & ","								'Mailing/Trailer Manifest Number
		strline = strline & ","											'Carrier Trailer/Pro#
		strline = strline & ","											'Carrier Trailer ETA
		strline = strline & ","											'Carrier Code
		strline = strline & inductionSiteID & ","						'Induction Site
		strline = strline & orderid & ","								'Package ID
		strline = strline & trackingnumber & ","						'tracking number
		strline = strline & scandate & ","								'DateTime
		strline = strline & "0,"										'Weight
		strline = strline & "92867,"									'Client Origin ZIP 5
		strline = strline & ","											'Ship To Attn
		strline = strline & replace(customerName, "|", "") & ","		'Ship To Name
		strline = strline & replace(sAddress1, "|", "") & ","			'Ship To Address 1
		strline = strline & replace(sAddress2, "|", "") & ","			'Ship To Address 2
		strline = strline & ","											'Ship To Address 3
		strline = strline & ","											'Ship To PO Box
		strline = strline & sCity & ","									'Ship To City
		strline = strline & sState & ","								'Ship To State
		strline = strline & sZip & ","									'Ship To ZIP5
		strline = strline & ","											'Ship To ZIP4
		strline = strline & sCountry & ","								'Ship To Country
		strline = strline & phone & ","									'Recipient Phone Number
		strline = strline & email & ","									'Recipient Email
		strline = strline & orderid & ","								'Shipment Reference1
		strline = strline & ","											'Shipment Reference2
		strline = strline & "0,"										'Delivery Confirmation
		strline = strline & "0,"										'Signature Confirmation
		strline = strline & ","											'Saturday Delivery
		strline = strline & ","											'Sunday Delivery
		strline = strline & ","											'Non Machinable
		strline = strline & ","											'Balloon Rate	
		strline = strline & ","											'Oversize
		strline = strline & ","											'Hazmat
		strline = strline & ","											'Packaging Type	
		strline = strline & ","											'COD Flag
		strline = strline & ","											'COD Amount	
		strline = strline & ","											'Payment Type
		strline = strline & ","											'Payment Detail
		strline = strline & ","											'Insurance Flag
		strline = strline & ","											'Insurance Fee
		strline = strline & ","											'Declared Value
		strline = strline & ","											'Charges
		strline = strline & "1003,"										'Service Code
		strline = strline & "3,"										'Processing Category
		strline = strline & ","											'Length
		strline = strline & ","											'Width
		strline = strline & ","											'Height
		strline = strline & ","											'Misc1
		strline = strline & ","											'Misc2
		
		file.WriteLine strline
		
		rs.movenext
	loop
end if
rs.close
set rs = nothing
file.close()

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
