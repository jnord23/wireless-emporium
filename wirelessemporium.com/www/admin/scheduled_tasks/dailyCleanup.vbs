Set wshNetwork = WScript.CreateObject( "WScript.Network" )
strComputerName = wshNetwork.ComputerName

set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

set fs = CreateObject("Scripting.FileSystemObject")

sql = "select id, siteid, folderPath, folderDesc, folderDepth, monthToken from xserverpath order by 1"
set rs = oConn.execute(sql)

on error resume next
do until rs.eof
	numDel = 0
	set folder = fs.GetFolder(rs("folderPath"))
	set files = folder.Files
	for each oFile in files
		if oFile.DateLastModified < DateAdd("m", 0-rs("monthToken"), date) then
			numDel = numDel + 1
'			response.write oFile & ", " & vbTab & vbTab & oFile.DateLastModified & "<br>"
			fs.deleteFile(oFile)
		end if
	next
	
	if rs("folderDepth") > 1 then
		for each oSubFolder in folder.SubFolders
			if oSubFolder.DateLastModified < DateAdd("m", 0-rs("monthToken"), date) and instr(oSubFolder, "SureSync") = 0 then
				numDel = numDel + 1
'				response.write oSubFolder & ", " & vbTab & vbTab & oSubFolder.DateLastModified & "<br>"
				fs.DeleteFolder(oSubFolder)
			end if
		next	
	end if

	lastNumFileDeleted = "lastNumFileDeleted2"
	if instr(strComputerName, "WEB3") > 0 then lastNumFileDeleted = "lastNumFileDeleted3"
	
	sql	=	"	update 	xserverpath " & vbcrlf & _
			"	set 	lastDateCleanup = getdate()" & vbcrlf & _
			"		, 	" & lastNumFileDeleted  & " = " & numDel & vbcrlf & _
			"	where 	id = " & rs("id")	
	oConn.execute(sql)

	rs.movenext
loop

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub

