set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim fs, file, filename, path, folderName
set fs = CreateObject("Scripting.FileSystemObject")
dim oXMLHTTP : set oXMLHTTP = CreateObject("Msxml2.ServerXMLHTTP.3.0")
set file = nothing

nRows = 0
qtyPadding = 2
filename = "newegg"
fileext = "txt"
folderName = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\"
imgAbsolutePath = "c:\inetpub\wwwroot\productpics\"
path = folderName & filename & "." & fileext
'timeStamp = "_" & Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now)
timeStamp = ""

set folder = fs.GetFolder(folderName)
set files = folder.Files

for each folderIdx in files
	if instr(folderIdx.Name, filename) > 0 then
		if fs.FileExists(folderName & folderIdx.Name) then fs.DeleteFile(folderName & folderIdx.Name)
	end if
next

dim SQL, RS
dim ModelName, BrandName, strItemLongDetail, strFeatures, strCompatibility, Condition, strline, itemDesc
sql	=	"select	case when d.typeid in (3,7) then 3 else d.typeid end typeid, a.itemid, a.partnumber" & vbcrlf & _
		"	,	isnull(a.itemdesc, '') itemdesc" & vbcrlf & _
		"	,	convert(varchar(8000), " & vbcrlf & _
		"		dbo.fn_stripHTML(" & vbcrlf & _
		"		replace(" & vbcrlf & _
		"		replace(" & vbcrlf & _
		"		replace(" & vbcrlf & _
		"			convert(varchar(8000), a.itemlongdetail)" & vbcrlf & _
		"			+	case when nullif(a.bullet1, '') is not null then ' - ' + a.bullet1 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.bullet2, '') is not null then ' - ' + a.bullet2 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.bullet3, '') is not null then ' - ' + a.bullet3 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.bullet4, '') is not null then ' - ' + a.bullet4 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.bullet5, '') is not null then ' - ' + a.bullet5 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.bullet6, '') is not null then ' - ' + a.bullet6 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.bullet7, '') is not null then ' - ' + a.bullet7 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.bullet8, '') is not null then ' - ' + a.bullet8 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.bullet9, '') is not null then ' - ' + a.bullet9 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(a.bullet10, '') is not null then ' - ' + a.bullet10 + '.' else '' end" & vbcrlf & _
		"			+	case when nullif(convert(varchar(2000), a.compatibility), '') is not null then ' - COMPATIBILITY: ' + convert(varchar(2000), a.compatibility) + '.' else '' end" & vbcrlf & _
		"		,	char(10), ' ')" & vbcrlf & _
		"		,	char(13), ' ')" & vbcrlf & _
		"		,	char(9), ' '))) itemlongdetail" & vbcrlf & _
		"	,	a.itempic, a.price_our, a.price_retail, case when e.alwaysinstock = 1 then 999 else w.inv_qty end inv_qty" & vbcrlf & _
		"	,	a.brandid, isnull(convert(varchar(1000), b.brandname), 'Universal') brandname, isnull(convert(varchar(1000), c.modelname), 'Universal') modelname" & vbcrlf & _
		"	,	isnull(a.itemweight, 2) itemweight, isnull(f.color, '') color, isnull(a.upccode, '') upccode" & vbcrlf & _
		"from	we_items a with (nolock) join v_subtypematrix d with (nolock) " & vbcrlf & _
		"	on	a.subtypeid = d.subtypeid left outer join we_brands b with (nolock)" & vbcrlf & _
		"	on	a.brandid=b.brandid left outer join we_models c with (nolock)" & vbcrlf & _
		"	on	a.modelid=c.modelid left outer join we_pnDetails e" & vbcrlf & _
		"	on	a.partnumber = e.partnumber left outer join xproductcolors f" & vbcrlf & _
		"	on	a.colorid = f.id join we_items w with (nolock)" & vbcrlf & _
		"	on	a.partnumber = w.partnumber and w.master = 1" & vbcrlf & _
		"where	a.hidelive = 0 and (w.inv_qty > 4 or e.alwaysinstock = 1)" & vbcrlf & _
		"	and a.price_our > 0	" & vbcrlf & _
		"	and a.itemdesc is not null and a.itemdesc <> ''" & vbcrlf & _
		"	and a.itempic is not null and a.itempic <> ''" & vbcrlf & _		
		"	and	a.partnumber not like 'WCD%'" & vbcrlf & _
		"	and	d.typeid in (1,2,3,6,7)" & vbcrlf & _
		"	--and	a.itemid in ('258995','258004','253253','251959','251955','262155','262143','262142','262141','262061','261890','261889','261236','247827','247824','247823','247822','247821')" & vbcrlf & _
		"	and	1 = case when b.brandid = 17 and d.typeid = 2 then 0 else 1 end -- exclude apple chargers & cables" & vbcrlf & _
		"order by case when d.typeid in (3, 7) then 3 else d.typeid end, a.itemid desc"

set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

dim arrHeadings(3)
dim arrCats(3)
dim arrQueryString(3)
dim configHeader(3)
dim arrFileIdx(3)

arrFileIdx(0) = 0
arrFileIdx(1) = 0
arrFileIdx(2) = 0
arrFileIdx(3) = 0

configHeader(0)	=	"Version=1.01" & vbTab & "SubCategoryID=614" & vbTab & "Overwrite=No"
configHeader(1)	=	"Version=1.01" & vbTab & "SubCategoryID=613" & vbTab & "Overwrite=No"
configHeader(2)	=	"Version=1.01" & vbTab & "SubCategoryID=612" & vbTab & "Overwrite=No"
configHeader(3)	=	"Version=1.01" & vbTab & "SubCategoryID=2955" & vbTab & "Overwrite=No"

'batteries
arrHeadings(0) = 	"Seller Part #" & vbTab & "Manufacturer" & vbTab & "Manufacturer Part # / ISBN" & vbTab & "UPC" & vbTab & "Manufacturer Item URL" & vbTab & "Related Seller Part#" & vbTab & "Website Short Title" & vbTab & "Website Long Title" & vbTab & "Product Description" & _
					vbTab & "Item Length" & vbTab & "Item Width" & vbTab & "Item Height" & vbTab & "Item Weight" & vbTab & "Item Condition" & vbTab & "Item Package" & vbTab & "Shipping Restriction" & vbTab & "Currency" & vbTab & "MSRP" & vbTab & "MAP" & vbTab & "CheckoutMAP"  & _
					vbTab & "Selling Price" & vbTab & "Shipping" & vbTab & "Inventory" & vbTab & "Activation Mark" & vbTab & "Action" & vbTab & "Item Images" & vbTab & "Prop 65" & vbTab & "Prop 65 - Motherboard" & vbTab & "Age 18+ Verification" & vbTab & "Choking Hazard 1" & _
					vbTab & "Choking Hazard 2" & vbTab & "Choking Hazard 3" & vbTab & "Choking Hazard 4" & vbTab & "CellphoneACCBrand" & vbTab & "CellphoneACCColor" & vbTab & "CellphoneACCCompatiblePhoneManufacturer" & vbTab & "CellphoneACCBatteryType" & _
					vbTab & "CellphoneACCBatteryCapacity" & vbTab & "CellphoneACCBatteryChemical" & vbTab & "CellphoneACCBatteryVolt" & vbTab & "CellphoneACCBatteryAmp" & vbTab & "CellphoneACCCompatiblePhoneModel" & vbTab & "CellphoneACCSpecifications" & _
					vbTab & "CellphoneACCSeries" & vbTab & "CellphoneACCBatteryDimension" & vbTab & "CellphoneACCFeatures" & vbTab & "CellphoneACCBatteryWeight" & vbTab & "CellphoneACCModel" & vbTab & "CellphoneACCWarranty"

'chargers & cables
arrHeadings(1) = 	"Seller Part #" & vbTab & "Manufacturer" & vbTab & "Manufacturer Part # / ISBN" & vbTab & "UPC" & vbTab & "Manufacturer Item URL" & vbTab & "Related Seller Part#" & vbTab & "Website Short Title" & vbTab & "Website Long Title" & vbTab & "Product Description" & _
					vbTab & "Item Length" & vbTab & "Item Width" & vbTab & "Item Height" & vbTab & "Item Weight" & vbTab & "Item Condition" & vbTab & "Item Package" & vbTab & "Shipping Restriction" & vbTab & "Currency" & vbTab & "MSRP" & vbTab & "MAP" & vbTab & "CheckoutMAP"	& _
					vbTab & "Selling Price" & vbTab & "Shipping" & vbTab & "Inventory" & vbTab & "Activation Mark" & vbTab & "Action" & vbTab & "Item Images" & vbTab & "Prop 65" & vbTab & "Prop 65 - Motherboard" & vbTab & "Age 18+ Verification" & vbTab & "Choking Hazard 1" & _
					vbTab & "Choking Hazard 2" & vbTab & "Choking Hazard 3" & vbTab & "Choking Hazard 4" & vbTab & "CellphoneACCBrand" & vbTab & "CellphoneACCColor" & vbTab & "CellphoneACCCompatiblePhoneManufacturer" & vbTab & "CellphoneACCChargerType" & _
					vbTab & "CellphoneACCChargerAdapter" & vbTab & "CellphoneACCChargerLength" & vbTab & "CellphoneACCChargerStandardUSBPort" & vbTab & "CellphoneACCChargerHDMIPort" & vbTab & "CellphoneACCCompatiblePhoneModel" & vbTab & "CellphoneACCChargerSupportVolt" & _
					vbTab & "CellphoneACCSpecifications" & vbTab & "CellphoneACCSeries" & vbTab & "CellphoneACCChargerOutputVolt" & vbTab & "CellphoneACCFeatures" & vbTab & "CellphoneACCChargerOutputAmp" & vbTab & "CellphoneACCModel" & vbTab & "CellphoneACCWarranty"

'covers & cases
arrHeadings(2) = 	"Seller Part #" & vbTab & "Manufacturer" & vbTab & "Manufacturer Part # / ISBN" & vbTab & "UPC" & vbTab & "Manufacturer Item URL" & vbTab & "Related Seller Part#" & vbTab & "Website Short Title" & vbTab & "Website Long Title" & vbTab & "Product Description" & _
					vbTab & "Item Length" & vbTab & "Item Width" & vbTab & "Item Height" & vbTab & "Item Weight" & vbTab & "Item Condition" & vbTab & "Item Package" & vbTab & "Shipping Restriction" & vbTab & "Currency" & vbTab & "MSRP" & vbTab & "MAP" & vbTab & "CheckoutMAP" & _
					vbTab & "Selling Price" & vbTab & "Shipping" & vbTab & "Inventory" & vbTab & "Activation Mark" & vbTab & "Action" & vbTab & "Item Images" & vbTab & "Prop 65" & vbTab & "Prop 65 - Motherboard" & vbTab & "Age 18+ Verification" & vbTab & "Choking Hazard 1" & _
					vbTab & "Choking Hazard 2" & vbTab & "Choking Hazard 3" & vbTab & "Choking Hazard 4" & vbTab & "CellphoneACCBrand" & vbTab & "CellphoneACCColor" & vbTab & "CellphoneACCCompatiblePhoneManufacturer" & vbTab & "CellphoneACCCasePattern" & vbTab & "CellphoneACCCaseType" & _
					vbTab & "CellphoneACCCaseMaterials" & vbTab & "CellphoneACCCaseBeltClipInclude" & vbTab & "CellphoneACCCaseScreenProtectorInclude" & vbTab & "CellphoneACCCaseStrapLanyardInclude" & vbTab & "CellphoneACCCaseWaterproof" & vbTab & "CellphoneACCCompatiblePhoneModel" & _
					vbTab & "CellphoneACCSpecifications" & vbTab & "CellphoneACCSeries" & vbTab & "CellphoneACCFeatures" & vbTab & "CellphoneACCModel" & vbTab & "CellphoneACCWarranty"

'holsters & mounts
arrHeadings(3) = 	"Seller Part #" & vbTab & "Manufacturer" & vbTab & "Manufacturer Part # / ISBN" & vbTab & "UPC" & vbTab & "Manufacturer Item URL" & vbTab & "Related Seller Part#" & vbTab & "Website Short Title" & vbTab & "Website Long Title" & vbTab & "Product Description" & _
					vbTab & "Item Length" & vbTab & "Item Width" & vbTab & "Item Height" & vbTab & "Item Weight" & vbTab & "Item Condition" & vbTab & "Item Package" & vbTab & "Shipping Restriction" & vbTab & "Currency" & vbTab & "MSRP" & vbTab & "MAP" & vbTab & "CheckoutMAP" & _
					vbTab & "Selling Price" & vbTab & "Shipping" & vbTab & "Inventory" & vbTab & "Activation Mark" & vbTab & "Action" & vbTab & "Item Images" & vbTab & "Prop 65" & vbTab & "Prop 65 - Motherboard" & vbTab & "Age 18+ Verification" & vbTab & "Choking Hazard 1" & _
					vbTab & "Choking Hazard 2" & vbTab & "Choking Hazard 3" & vbTab & "Choking Hazard 4" & vbTab & "CellphoneACCBrand" & vbTab & "CellphoneACCColor" & vbTab & "CellphoneACCCompatiblePhoneManufacturer" & vbTab & "CellphoneACCMountType" & vbTab & "CellphoneACCCompatiblePhoneModel" & _
					vbTab & "CellphoneACCSpecifications" & vbTab & "CellphoneACCSeries" & vbTab & "CellphoneACCFeatures" & vbTab & "CellphoneACCModel" & vbTab & "CellphoneACCWarranty"

arrCats(0)	=	"_Batteries" & timeStamp
arrCats(1)	=	"_ChargersCables" & timeStamp
arrCats(2)	=	"_CaseCovers" & timeStamp
arrCats(3)	=	"_MountsHolders" & timeStamp

arrQueryString(0)	=	"utm_source=newegg&utm_medium=marketplace&utm_campaign=AG-Batteries"
arrQueryString(1)	=	"utm_source=newegg&utm_medium=marketplace&utm_campaign=AG-ChargerCables"
arrQueryString(2)	=	"utm_source=newegg&utm_medium=marketplace&utm_campaign=AG-CasesCovers"
arrQueryString(3)	=	"utm_source=newegg&utm_medium=marketplace&utm_campaign=AG-MountsHolders"

nCatLap = -1
lastTypeID = 999
if not RS.eof then
	do until RS.eof
		typeid = rs("typeid")
		itempic = rs("itempic")
		brandName = rs("brandName")
		
		if instr("universal,apple,blackberry,htc,nokia,motorola,samsung,lg,sony,hp/palm,sidekick,panasonic,sanyo,siemens,pantech,nec,utstarcom,huawei,zte,amazon,casio,", lcase(brandName)) > 0 then
			if typeid <> lastTypeID then
				nCatLap = nCatLap + 1
				call fNewFile()			
			end if
	
			itemid = rs("itemid")
			partnumber = rs("partnumber")
			itemdesc = rs("itemdesc")
			itemlongdetail = rs("itemlongdetail")
			price_our = rs("price_our")
			price_retail = rs("price_retail")
			inv_qty = rs("inv_qty")
			upccode = rs("upccode")
			itemweight = rs("itemweight")
			productURL = "http://www.wirelessemporium.com/p-" & itemid & "-" & formatSEO(itemdesc) & "?" & arrQueryString(nCatLap)
			
			if fs.FileExists(imgAbsolutePath & "big\zoom\" & itempic) then
				imageURL = 	"http://www.wirelessemporium.com/productpics/big/zoom/" & itempic
			else
				imageURL = 	"http://www.wirelessemporium.com/productpics/big/" & itempic		
			end if				
			
			strLine = partnumber & "-" & itemid & vbTab 'Seller Part #
			strLine = strLine & "Wireless Emporium Inc." & vbTab 'Manufacturer
			strLine = strLine & "WE-" & itemid & vbTab 'Manufacturer Part # / ISBN
			strLine = strLine & "" & vbTab 'UPC
			strLine = strLine & productURL & vbTab 'Manufacturer Item URL
			strLine = strLine & vbTab 'Related Seller Part#
			strLine = strLine & itemdesc & vbTab 'Website Short Title
			strLine = strLine & vbTab 'Website Long Title
			strLine = strLine & itemlongdetail & vbTab 'Product Description
			strLine = strLine & "1" & vbTab 'Item Length
			strLine = strLine & "1" & vbTab 'Item Width
			strLine = strLine & "1" & vbTab 'Item Height
			strLine = strLine & "1" & vbTab 'Item Weight
			strLine = strLine & "New" & vbTab 'Item Condition
			strLine = strLine & "Retail" & vbTab 'Item Package
			strLine = strLine & "No" & vbTab 'Shipping Restriction
			strLine = strLine & "USD" & vbTab 'Currency
			strLine = strLine & price_retail & vbTab 'MSRP
			strLine = strLine & 0 & vbTab 'MAP
			strLine = strLine & vbTab 'CheckoutMAP
			strLine = strLine & price_our & vbTab 'Selling Price
			strLine = strLine & "Free" & vbTab 'Shipping
			strLine = strLine & (inv_qty-qtyPadding) & vbTab 'Inventory
			strLine = strLine & "True" & vbTab 'Activation Mark
			strLine = strLine & "Create Item" & vbTab 'Action
	
			strAltViews = ""
			for iCount = 0 to 7
				altPic = replace(itempic,".jpg","-" & iCount & ".jpg")
				if fs.FileExists(imgAbsolutePath & "altviews\zoom\" & altPic) then
					if strAltViews = "" then
						strAltViews = "http://www.wirelessemporium.com/productpics/altviews/zoom/" & altPic
					else
						strAltViews = strAltViews & ",http://www.wirelessemporium.com/productpics/altviews/zoom/" & altPic						
					end if
				elseif fs.FileExists(imgAbsolutePath & "altviews\" & altPic) then
					if strAltViews = "" then
						strAltViews = "http://www.wirelessemporium.com/productpics/altviews/" & altPic
					else
						strAltViews = strAltViews & ",http://www.wirelessemporium.com/productpics/altviews/" & altPic
					end if
				end if
			next
			if strAltViews <> "" then imageURL = imageURL & "," & strAltViews
			
			strLine = strLine & imageURL & vbTab 'Item Images
			strLine = strLine & vbTab 'Prop 65
			strLine = strLine & vbTab 'Prop 65 - Motherboard
			strLine = strLine & vbTab 'Age 18+ Verification
			strLine = strLine & vbTab 'Choking Hazard 1
			strLine = strLine & vbTab 'Choking Hazard 2
			strLine = strLine & vbTab 'Choking Hazard 3
			strLine = strLine & vbTab 'Choking Hazard 4
			strLine = strLine & vbTab 'CellphoneACCBrand
			strLine = strLine & vbTab 'CellphoneACCColor
			strLine = strLine & brandName & vbTab 'CellphoneACCCompatiblePhoneManufacturer

			select case nCatLap
				case 0
					strLine = strLine & vbTab 'CellphoneACCBatteryType
					strLine = strLine & vbTab 'CellphoneACCBatteryCapacity
					strLine = strLine & vbTab 'CellphoneACCBatteryChemical
					strLine = strLine & vbTab 'CellphoneACCBatteryVolt
					strLine = strLine & vbTab 'CellphoneACCBatteryAmp
					strLine = strLine & vbTab 'CellphoneACCCompatiblePhoneModel
					strLine = strLine & vbTab 'CellphoneACCSpecifications
					strLine = strLine & vbTab 'CellphoneACCSeries
					strLine = strLine & vbTab 'CellphoneACCBatteryDimension
					strLine = strLine & vbTab 'CellphoneACCFeatures
					strLine = strLine & vbTab 'CellphoneACCBatteryWeight
					strLine = strLine & vbTab 'CellphoneACCModel
				case 1
					strLine = strLine & vbTab 'CellphoneACCChargerType
					strLine = strLine & vbTab 'CellphoneACCChargerAdapter
					strLine = strLine & vbTab 'CellphoneACCChargerLength
					strLine = strLine & vbTab 'CellphoneACCChargerStandardUSBPort
					strLine = strLine & vbTab 'CellphoneACCChargerHDMIPort
					strLine = strLine & vbTab 'CellphoneACCCompatiblePhoneModel
					strLine = strLine & vbTab 'CellphoneACCChargerSupportVolt
					strLine = strLine & vbTab 'CellphoneACCSpecifications
					strLine = strLine & vbTab 'CellphoneACCSeries
					strLine = strLine & vbTab 'CellphoneACCChargerOutputVolt
					strLine = strLine & vbTab 'CellphoneACCFeatures
					strLine = strLine & vbTab 'CellphoneACCChargerOutputAmp
					strLine = strLine & vbTab 'CellphoneACCModel
				case 2
					strLine = strLine & vbTab 'CellphoneACCCasePattern
					strLine = strLine & vbTab 'CellphoneACCCaseType
					strLine = strLine & vbTab 'CellphoneACCCaseMaterials
					strLine = strLine & vbTab 'CellphoneACCCaseBeltClipInclude
					strLine = strLine & vbTab 'CellphoneACCCaseScreenProtectorInclude
					strLine = strLine & vbTab 'CellphoneACCCaseStrapLanyardInclude
					strLine = strLine & vbTab 'CellphoneACCCaseWaterproof
					strLine = strLine & vbTab 'CellphoneACCCompatiblePhoneModel
					strLine = strLine & vbTab 'CellphoneACCSpecifications
					strLine = strLine & vbTab 'CellphoneACCSeries
					strLine = strLine & vbTab 'CellphoneACCFeatures
					strLine = strLine & vbTab 'CellphoneACCModel
				case 3
					strLine = strLine & vbTab 'CellphoneACCMountType
					strLine = strLine & vbTab 'CellphoneACCCompatiblePhoneModel
					strLine = strLine & vbTab 'CellphoneACCSpecifications
					strLine = strLine & vbTab 'CellphoneACCSeries
					strLine = strLine & vbTab 'CellphoneACCFeatures
					strLine = strLine & vbTab 'CellphoneACCModel
			end select		

			strLine = strLine & "90-Day No-Hassle Returns & 1 Year Warranty" 'CellphoneACCWarranty
	
			call fWriteFile(strLine)
			lastTypeID = rs("typeid")
		end if
		rs.movenext
	loop
end if
file.close()
set file = nothing

sub fNewFile()
	nRows = 0
	if not file is nothing then
		file.close()
		set file = nothing
	end if
	
	path = folderName & filename & arrCats(nCatLap) & ".txt"
	if fs.FileExists(path) then fs.DeleteFile(path)
	set file = fs.CreateTextFile(path)
	file.WriteLine configHeader(nCatLap)
	file.WriteLine arrHeadings(nCatLap)
end sub

sub fWriteFile(pStrToWrite)
	nRows = nRows + 1
	
	if nRows > 5000 then
		arrFileIdx(nCatLap) = arrFileIdx(nCatLap) + 1
		nRows = 0
		file.close()
		set file = nothing
		
		path = folderName & filename & arrCats(nCatLap) & arrFileIdx(nCatLap) & ".txt"
		if fs.FileExists(path) then fs.DeleteFile(path)
		set file = fs.CreateTextFile(path)
		file.WriteLine configHeader(nCatLap)
		file.WriteLine arrHeadings(nCatLap)
	end if
	
	file.WriteLine pStrToWrite
end sub


function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(replace(replace(formatSEO, "*", "-"),"?","-"), "----", "-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
