set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

username = "srwiem"	
Hostname = "sftp.wiem.shoprunner.net"
Password = "F8WQ2ga5l5#o"
port = 22
set sftp = CreateObject("Chilkat.SFtp")
success = sftp.UnlockComponent("WIRELESSH_zHJQ3Uch8Hni")
success = sftp.Connect(hostname,port)
success = sftp.AuthenticatePw(username,password)
success = sftp.InitializeSftp()

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")

strYear = year(now)
strMonth = Right("0" & month(now), 2)
strDay = Right("0" & day(now), 2)
strHour = hour(now)
strMinute = minute(now)
strSec = second(now)

filename = "FULL_Shoprunner_Google_Feed_WIEM_" & strYear & strMonth & strDay & strHour & strMinute & ".txt"
path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\shoprunner\" & filename

set folder = fs.GetFolder("c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\shoprunner")
set files = folder.Files

for each oFile in files
	if oFile.DateLastModified < dateadd("d", -7, date) then
		oFile.Delete True
	end if
next

set file = fs.CreateTextFile(path)

sql	=	"select	a.itemid, a.itemdesc, a.itempic, a.partnumber, c.typename, isnull(b.brandname, 'Universal') brandName, isnull(d.modelName, 'Universal') modelName" & vbcrlf & _
		"	,	isnull(e.color, '') color, a.price_our, case when dbo.fn_validateUPC(a.upccode) = 1 then a.upccode else '' end upccode, isnull(itemDimensions, '') itemDimensions" & vbcrlf & _
		"	,	convert(varchar(8000), replace(replace(replace(dbo.fn_stripHTML(convert(varchar(8000), a.itemlongdetail)), char(10), ' '), char(13), ' '), char(9), ' ')) itemlongdetail" & vbcrlf & _
		"from	we_items a with (nolock) join we_types c with (nolock)" & vbcrlf & _
		"	on	a.typeid = c.typeid left outer join we_brands b with (nolock) " & vbcrlf & _
		"	on	a.brandid = b.brandid left outer join we_models d with (nolock)" & vbcrlf & _
		"	on	a.modelid = d.modelid left outer join xproductcolors e with (nolock)" & vbcrlf & _
		"	on	a.colorid = e.id left outer join" & vbcrlf & _
		"		(" & vbcrlf & _
		"		select	a.partnumber orphan_partnumber" & vbcrlf & _
		"		from	(" & vbcrlf & _
		"				select	partnumber" & vbcrlf & _
		"					,	sum(case when inv_qty > 0 then 1 else 0 end) nmaster" & vbcrlf & _
		"					,	sum(case when inv_qty < 0 then 1 else 0 end) nslave" & vbcrlf & _
		"				from	we_items a with (nolock)" & vbcrlf & _
		"				where	hidelive = 0 and inv_qty <> 0 and price_our > 0" & vbcrlf & _
		"				group by partnumber" & vbcrlf & _
		"				) a" & vbcrlf & _
		"		where	a.nmaster = 0" & vbcrlf & _
		"		) o" & vbcrlf & _
		"	on	a.partnumber = o.orphan_partnumber" & vbcrlf & _
		"where	a.hidelive = 0 and a.inv_qty <> 0 and a.price_our > 0" & vbcrlf & _
		"	and	o.orphan_partnumber is null" & vbcrlf & _
		"	--and	a.vendor not in ('CM', 'DS', 'MLD') and a.partnumber not like '%-HYP-%'"
set rs = CreateObject("ADODB.Recordset")
rs.open SQL, oConn, 0, 1
if not rs.eof then
	'core columns
	headers = "Partner" & vbtab & "Partner Code" & vbtab & "Brand Name" & vbtab & "Product ID" & vbtab & "SKU" & vbtab & "UPC" & vbtab & "Name" & vbtab & "Description" & vbtab & "Product URL" & vbtab & "Image URL" & vbtab & "Price"
	headers = headers & vbTab & "Promo Type ID" & vbTab & "Native Category" & vbTab & "Google Product Category" & vbTab & "Attributes"
	file.writeline headers
	
	do until rs.eof
		itemid = rs("itemid")
		itemdesc = rs("itemdesc")
		itempic = rs("itempic")
		partnumber = rs("partnumber")
		sTypename = rs("typename")
		brandName = rs("brandName")
		modelName = rs("modelName")
		color = rs("color")
		price_our = rs("price_our")
		upccode = rs("upccode")
		itemDimensions = rs("itemDimensions")
		itemlongdetail = rs("itemlongdetail")
		nativeCategory = brandName & " > " & modelName & " > " & sTypename
		attr = ""
		if color <> "" then call appendAttr(attr, "color:" & color)
		if itemDimensions <> "" then call appendAttr(attr, "itemDimensions:" & itemDimensions)		
		
		strline = 			"Wireless Emporium, Inc" & vbTab	'partner
		strline = strline & "WIEM" & vbTab ' [Partner Code]
		strline = strline & brandName & vbTab ' [Brand Name]
		strline = strline & itemid & vbTab ' [Product ID]
		strline = strline & itemid & vbTab ' [SKU]
		strline = strline & upccode & vbTab ' [UPC]
		strline = strline & itemdesc & vbTab ' [Name]
		strline = strline & itemlongdetail & vbTab ' [Description]
		strline = strline & "http://www.wirelessemporium.com/p-" & itemid & "-" & formatSEO(itemdesc) & vbTab ' [Product URL]
		strline = strline & "http://www.wirelessemporium.com/productpics/big/" & itempic & vbTab ' [Image URL]
		strline = strline & price_our & vbTab ' [Price]
		strline = strline & vbTab ' [Promo Type ID]
		strline = strline & nativeCategory & vbTab ' [Native Category]
		strline = strline & sTypename & vbTab ' [Google Product Category]
		strline = strline & attr ' [Attributes]

		file.writeline strline
		rs.movenext
	loop
end if
rs.close
set rs = nothing
file.close()

handle = sftp.OpenFile("/Inbox/" & filename,"writeOnly","createTruncate")
If (handle = vbNullString ) Then
    response.Write("<pre>" & sftp.LastErrorText & "</pre>")
    response.End()
End If

path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\shoprunner\" & filename
success = sftp.UploadFile(handle,path)
success = sftp.CloseHandle(handle)

sub appendAttr(byref pAttr, newAttr)
	if pAttr = "" then
		pAttr = newAttr
	else
		pAttr = pAttr & "|" & newAttr
	end if
end sub

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(replace(formatSEO,"?","-"), "----", "-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
