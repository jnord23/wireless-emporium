Set fs = CreateObject("Scripting.FileSystemObject")
exceptExt = ",jpg,jpeg,png,gif,txt,htm,pdf,csv,xml,xls,xlsx,css,cshtml,"
WScript.Timeout = 1000
minuteScan = -15
numFolders = 0
numFiles = 0
sFindings = ""
sHtml = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Strict//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"">" & vbcrlf & _
		"<html xmlns=""http://www.w3.org/1999/xhtml"">" & vbcrlf & _
		"<head>" & vbcrlf & _
		"<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />" & vbcrlf & _
		"<meta name=""viewport"" content=""width=device-width, initial-scale=1.0"" />" & vbcrlf & _
		"<title>Scan malware</title>" & vbcrlf & _
		"</head>" & vbcrlf & _
		"<body style=""margin: 0; padding: 0; width: 100%; -webkit-text-size-adjust: none;"">" & vbcrlf & _
		"<table width=""100%"" border=""1"" cellspacing=""0"" cellpadding=""2"" style=""font-size:11px; border-collapse:collapse;"">" & vbcrlf & _
		"  	<tr>" & vbcrlf & _
		"  		<td colspan=""2"">List of files have been created or modified within last " & abs(minuteScan) & " minutes</td>"& vbcrlf & _
		"	</tr>"& vbcrlf & _
		"  	<tr>" & vbcrlf & _
		"  		<td align=""left"" style=""background-color:#333; color:#fff;"">Path</td>"& vbcrlf & _
		"  		<td align=""left"" style=""background-color:#333; color:#fff;"">Date Modified</td>"& vbcrlf & _
		"	</tr>"& vbcrlf

ShowSubfolders fs.GetFolder("C:\inetpub\wwwroot\"), 25, sFindings

'ShowSubfolders fs.GetFolder("C:\inetpub\wwwroot\wirelessemporium.com\"), 25, sFindings
'ShowSubfolders fs.GetFolder("C:\inetpub\wwwroot\cellularoutfitter.com\"), 25, sFindings
'ShowSubfolders fs.GetFolder("C:\inetpub\wwwroot\Framework\"), 25, sFindings
'ShowSubfolders fs.GetFolder("C:\inetpub\wwwroot\phonesale.com\"), 25, sFindings
'ShowSubfolders fs.GetFolder("C:\inetpub\wwwroot\tabletmall.com\"), 25, sFindings
'ShowSubfolders fs.GetFolder("C:\inetpub\wwwroot\aspnet_client\"), 25, sFindings
'ShowSubfolders fs.GetFolder("C:\inetpub\wwwroot\productpics\"), 25, sFindings
'ShowSubfolders fs.GetFolder("C:\inetpub\wwwroot\productpics_co\"), 25, sFindings

sHtml = sHtml & sFindings & "</table></body></html>"

if sFindings <> "" then
	Set objErrMail = CreateObject("CDO.Message")
	With objErrMail
		.From = "service@wirelessemporium.com"
'		.To = "terry@wirelessemporium.com"
		.To = "terry@wirelessemporium.com,jon@wirelessemporium.com,gene@wirelessemporium.com,ruben@wirelessemporium.com,damian@wirelessemporium.com,edwin@wirelessemporium.com,chiyan@wirelessemporium.com"
		.Subject = "New file created on web2"
		.HTMLBody = sHtml
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Update
		.Send
	End With
	
	Set objErrMail = nothing
end if




sub recentFiles(folder, byref sFindings)
	for each oFile in folder.Files
		if oFile.DateLastModified >= dateadd("n", minuteScan, now) and instr(exceptExt, "," & fs.GetExtensionName(oFile) & ",") = 0 then
			numFiles = numFiles + 1
			sFindings = sFindings & "<tr><td align=""left"">" & oFile.Path & "</td><td align=""left"">" & oFile.DateLastModified & "</td></tr>" & vbcrlf
		end if
	next
end sub

Sub ShowSubFolders(Folder, Depth, byref sFindings)
    If Depth > 0 then
'		call recentFiles(Folder)
        For Each Subfolder in Folder.SubFolders
			if Subfolder.DateLastModified >= dateadd("n", minuteScan, now) then
				call recentFiles(Subfolder, sFindings)
			end if
            ShowSubFolders Subfolder, Depth - 1, sFindings 
        Next
'		response.write "<br>"
    End if
End Sub

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
