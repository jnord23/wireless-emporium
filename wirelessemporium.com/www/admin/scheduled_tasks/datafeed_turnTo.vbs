set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

dim fs, file, filename, path, folderName
set fs = CreateObject("Scripting.FileSystemObject")
dim oXMLHTTP : set oXMLHTTP = CreateObject("Msxml2.ServerXMLHTTP")

filename = "turnto"
fileext = "txt"
nRows = 0
folderName = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\"
path = folderName & filename & "." & fileext

nFileIdx = 0

set folder = fs.GetFolder(folderName)
set files = folder.Files

for each folderIdx in files
	if instr(folderIdx.Name, filename) > 0 then
'		response.write folderpath & "\" & folderIdx.Name & "<br>"
		if fs.FileExists(folderName & folderIdx.Name) then
			fs.DeleteFile(folderName & folderIdx.Name)
		end if
	end if
next
set file = fs.CreateTextFile(path)

dim expDate, myMonth, myDay, myYear
expDate = dateAdd("d",21,date)
myMonth = month(expDate)
if myMonth < 10 then myMonth = "0" & myMonth
myDay = day(expDate)
if myDay < 10 then myDay = "0" & myDay
if myHour < 10 then myHour = "0" & myHour
myYear = year(expDate)
expDate = myYear & "-" & myMonth & "-" & myDay

dim SQL, RS
dim ModelName, BrandName, strItemLongDetail, strFeatures, strCompatibility, Condition, strline, itemDesc
sql	=	"select	a.itemid, a.partnumber, a.itemdesc, a.itemdesc_google, a.itempic, a.price_our, a.inv_qty, isnull(a.itemweight, 2) itemweight" & vbcrlf & _
		"	,	convert(varchar(8000), isnull(dbo.fn_stripHTML(convert(varchar(8000), a.itemlongdetail)), '')) itemlongdetail" & vbcrlf & _
		"	,	convert(varchar(8000), a.compatibility) compatibility, a.bullet1, a.bullet2, a.bullet3, a.bullet4, a.bullet5, a.bullet6, a.bullet7, a.bullet8, a.bullet9, a.bullet10" & vbcrlf & _
		"	,	'Universal' brandName, 'Custom Case' typename, 'Universal' modelname" & vbcrlf & _
		"from	we_items a" & vbcrlf & _
		"where	a.itemid = 146579" & vbcrlf & _
		"order by a.itemid desc"
	
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

headings = "id" & vbTab & "title" & vbTab & "description" & vbTab & "google_product_category" & vbTab & "product_type" & vbTab & "link" & vbTab & "image_link" & vbTab & "condition" & vbTab & "availability" & vbTab & "price" & vbTab & "brand" & vbTab & "gtin" & vbTab & "mpn" & vbTab & "tax" & vbTab & "shipping" & vbTab & "shipping_weight" & vbTab & "adwords_labels" & vbTab & "adwords_grouping"

if not RS.eof then
	'###### OLD DESIGN ######
	'file.WriteLine "description" & vbTab & "id" & vbTab & "link" & vbTab & "price" & vbTab & "title" & vbTab & "quantity" & vbTab & "shipping_weight" & vbTab & "image_link" & vbTab & "brand" & vbTab & "condition" & vbTab & "mpn" & vbTab & "payment_accepted" & vbTab & "expiration_date" & vbTab & "product_type" & vbTab & "feature" & vbTab & "c:Compatible_With:String" & vbTab & "c:GoogleAffiliateNetworkProductURL:String"
	
	file.WriteLine headings
	do until RS.eof
		itemid = rs("itemid")
		partnumber = rs("partnumber")
		itemDesc = rs("itemdesc")
		itemdesc_google = rs("itemdesc_google")
		itempic = rs("itempic")
		price_our = rs("price_our")
		itemweight = rs("itemweight")
		itemlongdetail = replace(replace(replace(rs("itemlongdetail"), chr(10), " "), chr(13), " "), chr(9), " ")
		compatibility = rs("compatibility")
		strTypeName = rs("typename")
		
		if fs.FileExists("c:\inetpub\wwwroot\productpics\thumb\" & itempic) then
			if not isnull(itemlongdetail) then
				strItemLongDetail = stripCallToAction(itemlongdetail)
				strItemLongDetail = replace(replace(strItemLongDetail,vbCrLf," "),vbTab," ")
				strItemLongDetail = replace(strItemLongDetail,"<a href='/downloads/","<a href='http://www.wirelessemporium.com/downloads/")
			else
				strItemLongDetail = ""
			end if
			
			if not isNull(compatibility) then
				strCompatibility = replace(compatibility,vbCrLf," ")
			else
				strCompatibility = ""
			end if
			
			strFeatures = ""
			if not isNull(RS("BULLET1")) and RS("BULLET1") <> "" then strFeatures = strFeatures & " - " & RS("BULLET1")
			if not isNull(RS("BULLET2")) and RS("BULLET2") <> "" then strFeatures = strFeatures & " - " & RS("BULLET2")
			if not isNull(RS("BULLET3")) and RS("BULLET3") <> "" then strFeatures = strFeatures & " - " & RS("BULLET3")
			if not isNull(RS("BULLET4")) and RS("BULLET4") <> "" then strFeatures = strFeatures & " - " & RS("BULLET4")
			if not isNull(RS("BULLET5")) and RS("BULLET5") <> "" then strFeatures = strFeatures & " - " & RS("BULLET5")
			if not isNull(RS("BULLET6")) and RS("BULLET6") <> "" then strFeatures = strFeatures & " - " & RS("BULLET6")
			if not isNull(RS("BULLET7")) and RS("BULLET7") <> "" then strFeatures = strFeatures & " - " & RS("BULLET7")
			if not isNull(RS("BULLET8")) and RS("BULLET8") <> "" then strFeatures = strFeatures & " - " & RS("BULLET8")
			if not isNull(RS("BULLET9")) and RS("BULLET9") <> "" then strFeatures = strFeatures & " - " & RS("BULLET9")
			if not isNull(RS("BULLET10")) and RS("BULLET10") <> "" then strFeatures = strFeatures & " - " & RS("BULLET10")
			strFeatures = replace(strFeatures,vbTab," ")
			
			
			if inStr(Lcase(strItemLongDetail),"refurbished") > 0 or inStr(Lcase(itemDesc),"refurbished") > 0 or inStr(Lcase(strFeatures),"refurbished") > 0 then
				Condition = "refurbished"
			else
				Condition = "new"
			end if
			
			if not isNull(itemdesc_google) then
				itemDesc = itemdesc_google
			elseif itemid = 30626 then
				itemDesc = "Stylish EVA Pouch for Samsung Samsung Solstice SGH-A887, Samsung SGH-F480, Samsung SGH-U600, Samsung SGH-A707 SYNC, Sony Ericsson W980, Sony Ericsson W518a, Samsung Highlight SGH-T749, LG Venus VX8800, LG VX 8600 / AX 8600, LG enV2 VX9100, Blackberry Pearl Flip 8220/8230, HTC Shadow, LG Banter AX265/UX265, LG CF360, LG env3 VX9200, HTC 3125/Smartflip/8500/Star Trek, LG KF510, LG KG300, LG Muziq LX-570/UX/AX-565, LG Neon GT365, LG Versa VX9600, Motorola Evoke QA4, Samsung Gravity T459, Samsung Memoir T929, Motorola RAZR MAXX Ve, Motorola Rival A455, Motorola ROKR E2, Motorola V3c Razr, Motorola W220, Motorola W377, Nokia 5610, Pantech Breeze C520, Samsung A900, Samsung SGH-D807, Samsung Knack U310, Samsung SCH-U700 Gleam, Samsung SGH-A517, Samsung SGH-G600, Samsung SGH-T439, Motorola Entice W766, Samsung Intensity SCH-U450, Samsung SPH-M330, Motorola i856, Sony Ericsson F305, HTC Pure, Blackberry Pearl 8110/8120/8130, HTC Touch Diamond (CDMA), HTC Touch Diamond (GSM), HTC Touch Dual, LG KB770, LG Xenon GR500, Motorola Krave ZN4, Motorola Renegade V950, Motorola RIZR Z8, Palm Treo 680, Samsung Behold T919, Samsung Eternity SGH-A867, Samsung Glyde SCH-U940, Samsung Propel Pro SGH-i627, LG Chocolate VX8550, LG KP220, LG KS500, LG Shine CU720, Motorola Adventure V750, Samsung S5230 Star, Nokia 2680, Nokia 6670, Samsung SGH-T659, LG Bliss UX700, Samsung SGH-T629, Samsung SGH-T639, Sony Ericsson W380a, Sony Ericsson W395, Samsung Comeback T559, Motorola V3 Razr, Motorola V3m Razr, Motorola V3xx Razr, Motorola W755, Nokia 2610, Nokia 6555, Nokia 6560, Samsung A777, Samsung FlipShot SCH-U900, Samsung MyShot II SCH-R460, Samsung S3500, Samsung SCH-U550, Samsung SGH-A437/A436, Samsung SGH-A717, Samsung SGH-E215, Samsung SGH-J700, Nokia 6350, Motorola ZN200, LG VX 8700, Motorola EM330, HTC Cingular 8125, HTC Fuze, HTC Shadow 2, HTC XV6850 Touch Pro CDMA (Verizon), LG Dare VX9700, LG KP500/KP501 Cookie, Motorola Rapture VU30, Motorola RAZR VE20, Motorola RAZR2 V8, Motorola RAZR2 V9, Motorola RAZR2 V9m, Motorola V3a/V3i/V3r/V3t/V3e Razr, Motorola W490/W510/W5, Nokia 3555, Pantech PN-820, Samsung Helio Drift SPH-A503, Samsung Helio Fin SPH-A513, Samsung Helio Heat SPH-A303, Samsung Propel A767, Samsung Renown SCH-U810, Samsung SCH-U740, Samsung SGH-T239, LG LX290, Sony Ericsson C905a, LG KS10, LG KU990/GC900 Viewty, LG Rumor LX260, LG Rumor2 LX265, Motorola RAZR Maxx V6, Nextel Stature i9, Motorola Stature i9, Nokia XpressMusic 5800, Palm Centro, Palm Treo 750, Palm Treo 755p, Samsung Helio Mysto SPH-A523, Samsung Propel A767, LG Chocolate VX8500, LG Invision CB630, LG KF350, LG KF750/TU750 Secret, LG KP260, LG KU380, HTC Touch Diamond2, Samsung Smooth SCH-U350, Samsung Trance SCH-U490, Sanyo Pro-200, Sanyo Pro-700, Sanyo SCP-6600/Katana, Sanyo SCP-8500/Katana DLX, Sony Ericsson W350, HTC Fusion/5800/S720, Motorola VE440, Samsung SPH-M240, Nokia Mural 6750, Motorola ROKR U9, Motorola VE465, Nokia 5310, Nokia 6263, Samsung Highnote SPH-M630, LG Trax CU575, LG LX370/UX370, Other Casio Exilim C721, HTC Herman/Touch Pro CDMA (Alltel, Sprint), LG Decoy VX8610, LG Incite CT810, LG KF900 Prada II, LG Vu/CU920/CU915/TU915, Motorola A810, Motorola C261, Motorola Clutch i465, Palm Treo 700wx/700w/700p, Samsung Alias 2 SCH-U750, Sony Ericsson W760, LG CG180, LG Chocolate 3 VX8560, LG KC780, Nokia Surge 6790, Samsung SGH-T809/D820, Samsung SLM SGH-A747, Samsung SPH-M320, LG KM501, Samsung Hue II SCH-R600, Samsung Katalyst SGH-T739, Samsung S3600, Samsung SCH-R211, Samsung SCH-U540, Samsung SGH-D900i, Samsung SGH-T429, Motorola SLVR L9, Sony Ericsson G502, Sony Ericsson K510i, LG Helix AX310/UX310, Sanyo SCP-6650/Katana II, Sony Ericsson S302, Sony Ericsson TM506, Sony Ericsson W580i, Sony Ericsson W595, Sony Ericsson W760, Nokia E75, Blackberry Pearl 8100"
			elseif itemid = 13309 then
				itemDesc = "Retractable-Cord Car Charger for LG VI-125, LG VX 4650/VX 4700/ AX4750, LG PM-325, LG C1500, LG VX 3450, LG VX 8000, LG VX 8100, LG AX5000/UX5000, LG LX-350, LG AX-140/145 Aloha/200c, LG VX 5300/AX245, LG VX 3400, LG AX-390/UX-390, LG FUSIC LX550, LG AX4270, LG VX 9800, LG VX 7000, LG VX 6100, LG VX 3200, LG PM-225, LG VX 3300/3280, LG VX 5200, LG Migo VX1000, LG VX 8300"
			elseif itemid = 8879 then
				itemDesc = "HEAVY-DUTY Car Charger for LG VI-125, LG VX 4650/VX 4700/ AX4750, LG PM-325, LG C1500, LG VX 3450, LG VX 8000, LG VX 8100, LG AX5000/UX5000, LG LX-350, LG AX-140/145 Aloha/200c, LG VX 5300/AX245, LG VX 3400, LG AX-390/UX-390, LG FUSIC LX550, LG AX4270, LG VX 9800, LG VX 7000, LG VX 6100, LG VX 3200, LG PM-225, LG VX 3300/3280, LG VX 5200, LG Migo VX1000, LG VX 8300"
			elseif itemid = 10564 then
				itemDesc = "Car Charger for LG VI-125, LG VX 4650/VX 4700/ AX4750, LG PM-325, LG C1500, LG VX 3450, LG VX 8000, LG VX 8100, LG AX5000/UX5000, LG LX-350, LG AX-140/145 Aloha/200c, LG VX 5300/AX245, LG VX 3400, LG AX-390/UX-390, LG FUSIC LX550, LG AX4270, LG VX 9800, LG VX 7000, LG VX 6100, LG VX 3200, LG PM-225, LG VX 3300/3280, LG VX 5200, LG Migo VX1000, LG VX 8300"
			end if
			
			BrandName = RS("brandName")
			ModelName = RS("modelName")
			
			if len(itemDesc) > 69 then itemDesc = left(itemDesc, 66) & "..."
			
			strline = itemid & vbTab
			strline = strline & itemDesc & vbTab
			strline = strline & strItemLongDetail & vbtab
			strline = strline & "Electronics > Communications > Telephony > Mobile Phone Accessories" & vbTab
			strline = strline & "Electronics > Communications > Telephony > Mobile Phone Accessories" & vbTab
			strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(itemDesc) & "?source=gbase" & vbTab
			strline = strline & "http://www.wirelessemporium.com/productpics/thumb/" & itempic & vbTab
			strline = strline & Condition & vbTab
			strline = strline & "in stock" & vbTab
			strline = strline & RS("price_our") & vbTab
			strline = strline & BrandName & vbTab
			strline = strline & "" & vbTab
			strline = strline & partnumber & vbTab
			strline = strline & "US:CA:8.00:y" & vbTab
			strline = strline & "US:::0.00" & vbTab
			strline = strline & itemweight & vbTab
			strline = strline & strTypeName & vbTab
			strline = strline & ModelName

			call fWriteFile(strline)
		end if
		RS.movenext
	loop
	
'	' FTP the file to Google
'	dim ftp, success
'	set ftp = CreateObject("Chilkat.Ftp2")
'	success = ftp.UnlockComponent("WIRELEFTP_GQ2TEoLE9Bnh")
'	if success <> 1 then msgbox ftp.LastErrorText
'	
'	ftp.Hostname = "uploads.google.com"
'	ftp.Username = "wirelessemporium"
'	ftp.Password = "eugeneku1986"
'	ftp.Passive = 1
'	
'	' Connect and login to the FTP server.
'	success = ftp.Connect()
'	if success <> 1 then msgbox ftp.LastErrorText
'	
'	'  Upload the file.
'	dim localFilename, remoteFilename
'	localFilename = path
'	remoteFilename = filename
'	success = ftp.PutFile(localFilename,remoteFilename)
'	if success <> 1 then msgbox ftp.LastErrorText
'	
'	ftp.Disconnect
'	set ftp = nothing
end if
file.close()

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub


sub fWriteFile(pStrToWrite)
	nRows = nRows + 1
	
	if nRows > 20000 then
		nFileIdx = nFileIdx + 1
		nRows = 0
		file.close()
		set file = nothing
		
		path = folderName & filename & nFileIdx & ".txt"
		if fs.FileExists(path) then
			fs.DeleteFile(path)
		end if
		set file = fs.CreateTextFile(path)

		file.WriteLine headings
	end if
	
	file.WriteLine pStrToWrite
end sub


function stripCallToAction(val)
	stripCallToAction = val
	dim a
	
	if not isnull(val) then
		a = inStr(1,stripCallToAction,"why pay ridiculous prices",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"don't settle",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"order this quality",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"order this convenient",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"buy this quality",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"buy this convenient",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"order today",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"remember - wireless emporium",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"you won't find a better price",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"buy this stylish",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"buy this vertical",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"for the same product at",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		a = inStr(1,stripCallToAction,"we're confident you'll be happy",1)
		if a > 0 then stripCallToAction = left(stripCallToAction,a - 1)
		stripCallToAction = replace(stripCallToAction,"don't pay retail prices","",1,9,1)
	end if
end function

function changeTypeNameSingular(stypeName)
	select case stypeName
		case "Antennas/Parts"
			changeTypeNameSingular = "Antenna"
		case "Batteries"
			changeTypeNameSingular = "Battery"
		case "Bling Kits & Charms"
			changeTypeNameSingular = "Bling Kit"
		case "Chargers"
			changeTypeNameSingular = "Charger"
		case "Data Cable & Memory"
			changeTypeNameSingular = "Data Cable"
		case "Faceplates"
			changeTypeNameSingular = "Faceplate/Cover"
		case "Hands-Free"
			changeTypeNameSingular = "Headset/Hands Free"
		case "Holsters/Belt Clips"
			changeTypeNameSingular = "Holster"
		case "Key Pads/Flashing"
			changeTypeNameSingular = "Colorful Keypad"
		case "Leather Cases"
			changeTypeNameSingular = "Case/Pouch"
		case else
			changeTypeNameSingular = "Universal Gear"
	end select
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function


function hasImageOnRemote(pRemoteURL, pFileName)
	dim isExists : isExists = false
	dim strTemp : strTemp = pRemoteURL & "/" & pFileName
	
	oXMLHTTP.Open "HEAD", strTemp, False	'communicating without responseBody, only looks up file header
	oXMLHTTP.Send
	
	if (oXMLHTTP.readyState = "4") and (oXMLHTTP.status = "200") and (instr(oXMLHTTP.getResponseHeader("Content-Type"), "image") > 0) then isExists = true
	
	hasImageOnRemote = isExists
	
end function