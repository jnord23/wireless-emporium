set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

strFrom = "sales@wirelessemporium.com"
strSubject = "Save 10% Off Your Next Purchase!"
dim strItem(6)

SQL = "SELECT A.orderID, A.ordergrandtotal, A.orderdatetime, B.email, C.fname FROM we_Orders A INNER JOIN we_Accounts B ON A.accountID = B.accountID "
SQL = SQL & " INNER JOIN we_emailopt C ON B.email=C.email "
SQL = SQL & " WHERE A.approved = 1 AND A.orderdatetime <= '" & dateAdd("d",-20,date) & "' AND A.orderdatetime >= '" & dateAdd("d",-21,date) & "' "
SQL = SQL & " AND A.store = 0 and (a.extordertype not in (9) or a.extordertype is null) "
SQL = SQL & " and c.email not in (select distinct email from we_emailopt_out) "
SQL = SQL & " ORDER BY A.orderID "
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

do until RS.eof
	orderID = RS("orderID")
	for a = 0 to 6
		strItem(a) = ""
	next
	strTo = RS("email")
	strBody = "<html>" & vbcrlf
	strBody = strBody & "<body>" & vbcrlf
	strBody = strBody & "<style>" & vbcrlf
	strBody = strBody & ".TopText { font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight: bold; color:#626262; text-decoration: none; }" & vbcrlf
	strBody = strBody & ".BottomText { font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; color:#000000; text-decoration: none; }" & vbcrlf
	strBody = strBody & "a.TopLink:link { font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #999999; text-decoration: none; }" & vbcrlf
	strBody = strBody & "a.TopLink:visited { font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #999999; text-decoration: none; }" & vbcrlf
	strBody = strBody & "a.TopLink:active { font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #999999; text-decoration: underline; }" & vbcrlf
	strBody = strBody & "a.TopLink:hover { font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #999999; text-decoration: underline; }" & vbcrlf
	strBody = strBody & "a.ProductLink:link { font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #494949; text-decoration: none; }" & vbcrlf
	strBody = strBody & "a.ProductLink:visited { font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #494949; text-decoration: none; }" & vbcrlf
	strBody = strBody & "a.ProductLink:active { font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #494949; text-decoration: underline; }" & vbcrlf
	strBody = strBody & "a.ProductLink:hover { font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #494949; text-decoration: underline; }" & vbcrlf
	strBody = strBody & ".RetailPrice { font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: normal; color: #999999; text-decoration: line-through; }" & vbcrlf
	strBody = strBody & ".OurPrice { font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold; color: #CC0000; text-decoration: none; }" & vbcrlf
	strBody = strBody & "</style>" & vbcrlf
	strBody = strBody & "<table width=""628"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf
	strBody = strBody & "<tr>" & vbcrlf
	strBody = strBody & "<td width=""620"" align=""center"" valign=""top"" bgcolor=""#FFFFFF"">" & vbcrlf
	strBody = strBody & "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"" style=""border:4px solid #EBEBEB;"">" & vbcrlf
	strBody = strBody & "<tr>" & vbcrlf
	strBody = strBody & "<td width=""100%"" align=""center"" valign=""bottom"" bgcolor=""#FFFFFF""><a href=""#review""><img src=""http://www.wirelessemporium.com/images/email/21-day-2/WE_review_header3.jpg"" width=""620"" height=""25"" border=""0"" alt=""Review Your Recent Purchase""></a></td>" & vbcrlf
	strBody = strBody & "</tr>" & vbcrlf
	strBody = strBody & "<tr>" & vbcrlf
	strBody = strBody & "<td width=""100%"" align=""center"" valign=""bottom"" bgcolor=""#FFFFFF""><img src=""http://www.wirelessemporium.com/images/spacer.gif"" width=""1"" height=""10"" border=""0""></td>" & vbcrlf
	strBody = strBody & "</tr>" & vbcrlf
	'Start iPad Promo
	'strBody = strBody & "<tr>" & vbcrlf
	'strBody = strBody & "<td width=""100%"" align=""right"" valign=""top"" bgcolor=""#FFFFFF""><a href=""http://www.facebook.com/notes.php?id=80966662054&notes_tab=app_2347471856""><img src=""http://www.wirelessemporium.com/images/email/21-day-2/fb_promo2.jpg"" border=""0"" alt=""""></a></td>" & vbcrlf
	'strBody = strBody & "</tr>" & vbcrlf
	'End iPad Promo
	strBody = strBody & "<tr>" & vbcrlf
	strBody = strBody & "<td width=""100%"" align=""left"" valign=""top"" bgcolor=""#FFFFFF""><a href=""http://www.wirelessemporium.com/?utm_source=Email&utm_medium=FollowUp&utm_campaign=21DayEmail""><img src=""http://www.wirelessemporium.com/images/email/21-day-2/WE_logo4.jpg"" width=""450"" height=""69"" border=""0"" alt=""Wireless Emporium - The #1 Name in Cell Phone Accessories""></a></td>" & vbcrlf
	strBody = strBody & "</tr>" & vbcrlf
	strBody = strBody & "<tr>" & vbcrlf
	strBody = strBody & "<td width=""100%"" align=""center"" valign=""bottom"" bgcolor=""#FFFFFF""><img src=""http://www.wirelessemporium.com/images/spacer.gif"" width=""1"" height=""10"" border=""0""></td>" & vbcrlf
	strBody = strBody & "</tr>" & vbcrlf
	strBody = strBody & "<tr>" & vbcrlf
	strBody = strBody & "<td width=""100%"" align=""left"" valign=""bottom"" bgcolor=""#FFFFFF"">" & vbcrlf
	strBody = strBody & "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">" & vbcrlf
	strBody = strBody & "<tr>" & vbcrlf
	strBody = strBody & "<td width=""395"" align=""left"" valign=""top"" bgcolor=""#FFFFFF"">" & vbcrlf
	strBody = strBody & "&nbsp;<a class=""TopLink"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors?utm_source=Email&utm_medium=FollowUp&utm_campaign=21DayEmail"">FACEPLATES</a>&nbsp;&nbsp;<a class=""TopLink"" href=""http://www.wirelessemporium.com/cell-phone-batteries?utm_source=Email&utm_medium=FollowUp&utm_campaign=21DayEmail"">BATTERIES</a>&nbsp;&nbsp;<a class=""TopLink"" href=""http://www.wirelessemporium.com/cell-phone-chargers?utm_source=Email&utm_medium=FollowUp&utm_campaign=21DayEmail"">CHARGERS</a>&nbsp;&nbsp;<a class=""TopLink"" href=""http://www.wirelessemporium.com/cell-phone-cases?utm_source=Email&utm_medium=FollowUp&utm_campaign=21DayEmail"">CASES</a>&nbsp;&nbsp;<a class=""TopLink"" href=""http://www.wirelessemporium.com/bluetooth-headsets-handsfree?utm_source=Email&utm_medium=FollowUp&utm_campaign=21DayEmail"">HEADSETS</a>" & vbcrlf
	strBody = strBody & "</td>" & vbcrlf
	strBody = strBody & "<td width=""225"" align=""right"" valign=""top"" bgcolor=""#FFFFFF"">" & vbcrlf
	strBody = strBody & "<img src=""http://www.wirelessemporium.com/images/email/21-day-2/freeshipping.jpg"" width=""225"" height=""21"" border=""0"" alt=""FREE SHIPPING ON ALL ORDERS!"">" & vbcrlf
	strBody = strBody & "</td>" & vbcrlf
	strBody = strBody & "</tr>" & vbcrlf
	strBody = strBody & "</table>" & vbcrlf
	strBody = strBody & "</td>" & vbcrlf
	strBody = strBody & "</tr>" & vbcrlf
	strBody = strBody & "<tr>" & vbcrlf
	strBody = strBody & "<td width=""100%"" align=""center"" valign=""bottom"" bgcolor=""#FFFFFF""><a href=""http://www.wirelessemporium.com/?utm_source=Email&utm_medium=FollowUp&utm_campaign=21DayEmail""><img src=""http://www.wirelessemporium.com/images/email/21-day-2/WE_21day_banner.jpg"" width=""620"" height=""300"" border=""0"" alt=""Save on Entire Order! 10% off on your next purchase* Use Promotion code WESHOP10 at checkout.""></a></td>" & vbcrlf
	strBody = strBody & "</tr>" & vbcrlf
	strBody = strBody & "<tr>" & vbcrlf
	strBody = strBody & "<td width=""100%"" height=""2"" align=""center"" valign=""bottom"" bgcolor=""#CCCCCC""><img src=""http://www.wirelessemporium.com/images/spacer.gif"" width=""1"" height=""2"" border=""0""></td>" & vbcrlf
	strBody = strBody & "</tr>" & vbcrlf
	strBody = strBody & "<tr>" & vbcrlf
	strBody = strBody & "<td width=""100%"" align=""left"" valign=""top"" bgcolor=""#FFFFFF"">" & vbcrlf
	strBody = strBody & "<table width=""600"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"">" & vbcrlf
	strBody = strBody & "<tr>" & vbcrlf
	strBody = strBody & "<td width=""100%"" align=""left"" valign=""top"" bgcolor=""#FFFFFF"">" & vbcrlf
	strBody = strBody & "<p class=""TopText"">" & vbcrlf
	strBody = strBody & "<br>Hello " & RS("fname") & ",<br><br>" & vbcrlf
	strBody = strBody & "Based upon the item(s) that you recently ordered with us, we feel you may also be interested in these great related products available now for your phone:" & vbcrlf
	strBody = strBody & "</p>" & vbcrlf
	strBody = strBody & "</td>" & vbcrlf
	strBody = strBody & "</tr>" & vbcrlf
	
	SQL = "SELECT A.*, B.itemDesc FROM we_Orderdetails A"
	SQL = SQL & " INNER JOIN we_Items B ON A.itemID = B.itemID"
	SQL = SQL & " WHERE orderID = '" & RS("orderID") & "'"
	SQL = SQL & " AND A.orderID <> 0 AND A.quantity <> 0"
	SQL = SQL & " ORDER BY orderdetailid"
	set RS2 = CreateObject("ADODB.Recordset")
	RS2.open SQL, oConn, 3, 3
	numRecs = RS2.recordcount
	itemid = ""
	quantity = ""
	relatedProdID = ""
	a = 0
	if numRecs > 0 then
		relatedProdID = ""
		do until RS2.eof
			itemid = itemid & RS2("itemid") & ", "
			quantity = quantity & RS2("quantity") & ", "
			RS2.movenext
		loop
		RS2.movefirst
		do until RS2.eof
			orderdetailid = RS2("orderdetailid")
			select case numRecs
				case 1 : SQL = "SELECT TOP 6"
				case 2 : SQL = "SELECT TOP 3"
				case 3 : SQL = "SELECT TOP 2"
				case else : SQL = "SELECT TOP 1"
			end select
			SQL = SQL & " we_items.itemID, we_items.itemDesc, we_items.itemPic, we_items.price_retail, we_items.modelID, we_items.price_our, we_items.numberOfSales"
			SQL = SQL & " FROM we_items LEFT JOIN we_OrderDetails ON (we_items.ItemID = we_OrderDetails.ItemID) LEFT JOIN we_Orders ON (we_Orders.orderID = we_orderDetails.orderID)"
			SQL = SQL & " WHERE (hideLive = 0) AND (inv_qty <> 0) AND brandID IN (SELECT brandID FROM we_items WHERE we_items.itemID = '" & RS2("itemid") & "') AND we_items.itemID NOT IN (" & left(itemid,len(itemid)-2) & ")"
			SQL = SQL & " AND we_items.modelID IN (SELECT modelID FROM we_items WHERE we_items.itemID = '" & RS2("itemid") & "')"
			SQL = SQL & " AND we_Items.typeID <> 16 AND (we_Items.HandsfreeType IS NULL OR we_Items.HandsfreeType <> 2)"
			SQL = SQL & " GROUP BY we_items.itemID, we_items.itemDesc, we_items.itemPic, we_items.price_retail, we_items.price_our, we_items.numberOfSales, we_items.modelID"
			SQL = SQL & " ORDER BY numberOfSales DESC"
			set RS3 = CreateObject("ADODB.Recordset")
			RS3.open SQL, oConn, 0, 1
			do until RS3.eof
				if inStr(relatedProdID,RS3("itemID") & ",") = 0 then
					a = a + 1
					if a <= 6 then
						relatedProdID = relatedProdID & RS3("itemID") & ", "
						strItem(a) = "<td width=""200"" align=""center"" valign=""bottom"" style=""border:1px solid #EBEBEB;"">" & vbcrlf
						strItem(a) = strItem(a) & "<a href=""http://www.wirelessemporium.com/p-" & RS3("itemid") & "-" & formatSEO(RS3("itemDesc")) & "?utm_source=Email&utm_medium=FollowUp&utm_campaign=21DayEmail""><img src=""http://www.wirelessemporium.com/productpics//big/" & RS3("itemPic") & """ width=""100"" height=""100"" border=""0"" alt=""" & RS3("itemDesc") & """></a><br>" & vbcrlf
						strItem(a) = strItem(a) & "<a class=""ProductLink"" href=""http://www.wirelessemporium.com/p-" & RS3("itemid") & "-" & formatSEO(RS3("itemDesc")) & "?utm_source=Email&utm_medium=FollowUp&utm_campaign=21DayEmail"">" & RS3("itemDesc") & "</a>" & vbcrlf
						strItem(a) = strItem(a) & "<br><span class=""RetailPrice"">" & formatCurrency(RS3("price_retail")) & "</span>&nbsp;<span class=""OurPrice"">" & formatCurrency(RS3("price_our")) & "</span>" & vbcrlf
						strItem(a) = strItem(a) & "<br><img src=""http://www.wirelessemporium.com/images/email/21-day-2/additional10.jpg"" width=""111"" height=""17"" border=""0"" alt=""Additional 10% Off""></a>" & vbcrlf
						strItem(a) = strItem(a) & "</td>" & vbcrlf
					end if
				end if
				RS3.movenext
			loop
			RS2.movenext
		loop
		if a < 6 then
			SQL = "SELECT TOP " & 6 - a
			SQL = SQL & " we_items.itemID, we_items.itemDesc, we_items.itemPic, we_items.price_retail, we_items.modelID, we_items.price_our, we_items.numberOfSales"
			SQL = SQL & " FROM we_items LEFT JOIN we_OrderDetails ON (we_items.ItemID = we_OrderDetails.ItemID) LEFT JOIN we_Orders ON (we_Orders.orderID = we_orderDetails.orderID)"
			SQL = SQL & " WHERE we_items.itemID IN (57460,25924,33705,56727,10848,7288,2385)"
			SQL = SQL & " GROUP BY we_items.itemID, we_items.itemDesc, we_items.itemPic, we_items.price_retail, we_items.price_our, we_items.numberOfSales, we_items.modelID"
			SQL = SQL & " ORDER BY we_items.itemID DESC"
			set RS3 = CreateObject("ADODB.Recordset")
			RS3.open SQL, oConn, 0, 1
			do until RS3.eof
				if inStr(relatedProdID,RS3("itemID") & ",") = 0 then
					a = a + 1
					if a <= 6 then
						relatedProdID = relatedProdID & RS3("itemID") & ", "
						strItem(a) = "<td width=""200"" align=""center"" valign=""bottom"" style=""border:1px solid #EBEBEB;"">" & vbcrlf
						strItem(a) = strItem(a) & "<a href=""http://www.wirelessemporium.com/p-" & RS3("itemid") & "-" & formatSEO(RS3("itemDesc")) & "?utm_source=Email&utm_medium=FollowUp&utm_campaign=21DayEmail""><img src=""http://www.wirelessemporium.com/productpics//big/" & RS3("itemPic") & """ width=""100"" height=""100"" border=""0"" alt=""" & RS3("itemDesc") & """></a><br>" & vbcrlf
						strItem(a) = strItem(a) & "<a class=""ProductLink"" href=""http://www.wirelessemporium.com/p-" & RS3("itemid") & "-" & formatSEO(RS3("itemDesc")) & "?utm_source=Email&utm_medium=FollowUp&utm_campaign=21DayEmail"">" & RS3("itemDesc") & "</a>" & vbcrlf
						strItem(a) = strItem(a) & "<br><span class=""RetailPrice"">" & formatCurrency(RS3("price_retail")) & "</span>&nbsp;<span class=""OurPrice"">" & formatCurrency(RS3("price_our")) & "</span>" & vbcrlf
						strItem(a) = strItem(a) & "<br><img src=""http://www.wirelessemporium.com/images/email/21-day-2/additional10.jpg"" width=""111"" height=""17"" border=""0"" alt=""Additional 10% Off""></a>" & vbcrlf
						strItem(a) = strItem(a) & "</td>" & vbcrlf
					end if
				end if
				RS3.movenext
			loop
		end if
		strBody = strBody & "<tr><td><table border=""0"" cellpadding=""0"" cellspacing=""20""><tr>" & vbcrlf
		if a >= 6 then a = 6
		for c = 1 to a
			strBody = strBody & strItem(c)
			if (c mod 3) = 0 and c < a then strBody = strBody & "</tr></table></td></tr><tr><td><table border=""0"" cellpadding=""0"" cellspacing=""20""><tr>" & vbcrlf
		next
		strBody = strBody & "</tr></table></td></tr>" & vbcrlf
		RS2.close
		set RS2 = nothing
		RS3.close
		set RS3 = nothing
		
		strBody = strBody & "<tr>" & vbcrlf
		strBody = strBody & "<td width=""100%"" align=""center"" bgcolor=""#FFFFFF"">" & vbcrlf
		strBody = strBody & "<table width=""600"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"">" & vbcrlf
		strBody = strBody & "<tr><td width=""100%"" align=""left"" bgcolor=""#FFFFFF""><a name=""review""><img src=""http://www.wirelessemporium.com/images/email/21-day-2/review_header.jpg"" width=""356"" height=""48"" border=""0""></a></td></tr>" & vbcrlf
		SQL = "SELECT A.itemID, B.itemDesc, B.itemPic FROM we_orderdetails A INNER JOIN we_items B ON A.itemID = B.itemID WHERE A.orderID = '" & orderID & "'"
		set RS3 = CreateObject("ADODB.Recordset")
		RS3.open SQL, oConn, 0, 1
		do until RS3.eof
			strBody = strBody & "<tr><td width=""100%"" align=""center"" bgcolor=""#FFFFFF""><table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"">" & vbcrlf
			strBody = strBody & "<tr><td colspan=""3"" align=""center"" valign=""bottom"" bgcolor=""#EBEBEB""><img src=""http://www.wirelessemporium.com/images/email/21-day-2/reviewBG_top.jpg"" width=""600"" height=""13"" border=""0""></td></tr>" & vbcrlf
			strBody = strBody & "<tr>" & vbcrlf
			strBody = strBody & "<td width=""60"" align=""center"" bgcolor=""#EBEBEB""><img src=""http://www.wirelessemporium.com/productpics/icon/" & RS3("itemPic") & """ width=""45"" height=""45"" border=""0""></td>" & vbcrlf
			strBody = strBody & "<td width=""370"" align=""left"" valign=""top"" bgcolor=""#EBEBEB""><font face=""Arial, Helvetica, sans-serif"" size=""2"" color=""#5CA5D3""><b>" & RS3("itemDesc") & "</b></font></td>" & vbcrlf
			strBody = strBody & "<td width=""150"" align=""right"" bgcolor=""#EBEBEB""><a href=""http://www.wirelessemporium.com/user-reviews?id=" & RS3("itemID") & "&oid=" & orderID & """><img src=""http://www.wirelessemporium.com/images/email/21-day-2/review_button.jpg"" width=""127"" height=""38"" border=""0"" alt=""Write a Review""></a></td>" & vbcrlf
			strBody = strBody & "</tr>" & vbcrlf
			strBody = strBody & "<tr><td colspan=""3"" align=""center"" valign=""top"" bgcolor=""#EBEBEB""><img src=""http://www.wirelessemporium.com/images/email/21-day-2/reviewBG_bottom.jpg"" width=""600"" height=""13"" border=""0""></td></tr>" & vbcrlf
			strBody = strBody & "<tr><td colspan=""3"" align=""center"" valign=""top"" bgcolor=""#FFFFFF""><img src=""http://www.wirelessemporium.com/images/spacer.gif"" width=""1"" height=""10"" border=""0""></td></tr>" & vbcrlf
			strBody = strBody & "</table></td></tr>" & vbcrlf
			RS3.movenext
		loop
		RS3.close
		set RS3 = nothing
		strBody = strBody & "</table></td></tr>" & vbcrlf
		
		strBody = strBody & "</table></td></tr>" & vbcrlf
		strBody = strBody & "</table></td></tr>" & vbcrlf
		
		strBody = strBody & "<tr><td width=""100%"" align=""center"" valign=""top"" bgcolor=""#FFFFFF""><img src=""http://www.wirelessemporium.com/images/spacer.gif"" width=""1"" height=""10"" border=""0""></td></tr>" & vbcrlf
		strBody = strBody & "<tr>" & vbcrlf
		strBody = strBody & "<td width=""100%"" align=""left"" valign=""top"" bgcolor=""#FFFFFF"">" & vbcrlf
		strBody = strBody & "<table width=""100%"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf
		strBody = strBody & "<tr><td width=""100%"" height=""2"" align=""left"" valign=""top"" bgcolor=""#999999""><img src=""http://www.wirelessemporium.com/images/spacer.gif"" width=""1"" height=""2"" border=""0""></td></tr>" & vbcrlf
		strBody = strBody & "<tr><td width=""100%"" align=""center"" valign=""top"" bgcolor=""#ECECEA""><img src=""http://www.wirelessemporium.com/images/email/21-day-2/we_connect.jpg"" width=""245"" height=""55"" border=""0"" alt=""Connect With Us""><img src=""http://www.wirelessemporium.com/images/spacer.gif"" width=""61"" height=""55"" border=""0""><a href=""http://www.facebook.com/WirelessEmporium""><img src=""http://www.wirelessemporium.com/images/email/21-day-2/we_fb.jpg"" width=""118"" height=""55"" border=""0"" alt=""Facebook""></a><a href=""http://twitter.com/wirelessemp/""><img src=""http://www.wirelessemporium.com/images/email/21-day-2/we_twitter.jpg"" width=""111"" height=""55"" border=""0"" alt=""Twitter""></a><a href=""http://www.wirelessemporium.com/blog/?utm_source=Email&utm_medium=FollowUp&utm_campaign=21DayEmail""><img src=""http://www.wirelessemporium.com/images/email/21-day-2/we_blog.jpg"" width=""93"" height=""55"" border=""0"" alt=""Blog""></a></td></tr>" & vbcrlf
		strBody = strBody & "<tr><td width=""100%"" height=""2"" align=""left"" valign=""top"" bgcolor=""#999999""><img src=""http://www.wirelessemporium.com/images/spacer.gif"" width=""1"" height=""2"" border=""0""></td></tr>" & vbcrlf
		strBody = strBody & "<tr>" & vbcrlf
		strBody = strBody & "<td width=""100%"" align=""left"" valign=""top"" bgcolor=""#FFFFFF"">" & vbcrlf
		strBody = strBody & "<p class=""BottomText""><br>As with every purchase, your satisfaction is always 100% guaranteed. So don't hesitate to take advantage of this special offer today!</p>" & vbcrlf
		strBody = strBody & "<p class=""BottomText"">We want to thank you for your recent purchase from <a href=""http://www.wirelessemporium.com/?utm_source=Email&utm_medium=FollowUp&utm_campaign=21DayEmail"">WirelessEmporium.com</a>! As <b><i>The Leader</i></b> in wireless accessories online, it is our goal to make sure that your purchase experience was above and beyond your expectations. If for any reason it was not, please <a href=""http://www.wirelessemporium.com/contactus?utm_source=Email&utm_medium=FollowUp&utm_campaign=21DayEmail"">CONTACT US</a> and we will do our very best to assist you in ensuring your satisfaction.</p>" & vbcrlf
		strBody = strBody & "<p class=""BottomText"">Sincerely,</p>" & vbcrlf
		strBody = strBody & "<p class=""BottomText""><i>The Sales Team @ WirelessEmporium.com</i><br><a href=""mailto:sales@wirelessemporium.com"">sales@wirelessemporium.com</a><br><a href=""http://www.wirelessemporium.com/"">www.WirelessEmporium.com</a></p>" & vbcrlf
		strBody = strBody & "<p class=""BottomText"">Wireless Emporium, Inc. takes your privacy very seriously and is a certified licensee of the TRUSTe?Privacy Seal Program. This email correspondence has been sent to you as you have indicated you would like to receive special, exclusive offers from WirelessEmporium.com.<br><br>If this message has been sent in error, or you would no longer like to receive any more periodic offers from WirelessEmporium.com, you may <a href=""http://www.wirelessemporium.com/unsubscribe?strEmail=" & RS("email") & """>CLICK HERE</a> to unsubscribe.</p>" & vbcrlf
		strBody = strBody & "<p class=""BottomText"">*Promotion excludes cellular phones, Bluetooth headsets, and OEM products including Ed Hardy, Case Mate, Otterbox and Body Glove accessories.</p>" & vbcrlf
		strBody = strBody & "</td></tr></table>" & vbcrlf
		strBody = strBody & "</td></tr>" & vbcrlf
		strBody = strBody & "<tr><td width=""100%"" align=""center"" valign=""top"" bgcolor=""#FFFFFF""><br><p align=""center""><a href=""http://www.squaretrade.com/cellphone?ccode=bs_war_vc_052""><img src=""http://www.wirelessemporium.com/images/ad_banners/Squaretrade2.png"" width=""468"" height=""60"" border=""0""></a></p></td></tr>" & vbcrlf
		strBody = strBody & "</table>" & vbcrlf
		
		strBody = strBody & "</body>" & vbcrlf
		strBody = strBody & "</html>" & vbcrlf
		
		if instr(strTo, "@mail.marketplace.buy.com") = 0 then
			set objErrMail = CreateObject("CDO.Message")
			with objErrMail
				.From = strFrom
				.To = strTo
				'.Bcc = "webmaster@wirelessemporium.com"
				.Subject = strSubject
				.HTMLBody = strBody
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
				.Configuration.Fields.Update
				.Send
			end with
		end if
	end if
	RS.movenext
loop

RS.close
set RS = nothing

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub


function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function
