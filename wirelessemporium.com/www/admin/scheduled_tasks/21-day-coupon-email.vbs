set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

isTestMode = false
strTemplate = ""
strFrom = "coupon@wirelessemporium.com"
strSubject = "Share and EARN $ with Wireless Emporium"
strTo = ""

sql	= "select distinct b.email, b.accountID from we_orders a with (nolock) join we_accounts b with (nolock) on a.accountid = b.accountid where a.orderdatetime >= '" & date-21 & "' and a.orderdatetime < '" & date-20 & "' and a.store = 0 and a.approved = 1 and a.cancelled is null and (b.email not like '%@marketplace.amazon.com' and b.email not like '%@seller.sears.com') and (a.extordertype not in (9) or a.extordertype is null) and b.email not in ( select email from we_emailopt_out ) order by b.email"
if isTestMode then sql = replace(sql,"select distinct","select top 1")
set rs = oConn.execute(sql)
saveSQL = sql

strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\admin\scheduled_tasks\21-day-coupon-email-template.html")
dup_strTemplate = strTemplate

lapCount = 0
do until rs.eof
	lapCount = lapCount + 1
	strTo = rs("email")
	
	if isTestMode then
		newCode = "tonylee10"
	else
		randomize
		uniqueSeq = 0
		do while uniqueSeq = 0
			numberSeg = ""
			for x = 1 to 10
				if x > 1 and x < 5 then
					numberSeg = numberSeg & chr(Int(Rnd * 24)+65)
				else
					numberSeg = numberSeg & Int(Rnd * 10)
				end if
			next
			sql = "select * from we_coupons where promoCode = '" & numberSeg & "'"
			'session("errorSQL") = sql
			set testRS = oConn.execute(sql)
			
			if testRS.EOF then uniqueSeq = 1
		loop
		masterList = masterList & numberSeg & "##"
		sql = "insert into we_coupons (promoCode,promoPercent,couponDesc,expiration,activate,cashBack) values('" & numberSeg & "',.1,'Customer Affiliate Program','" & date + 365 & "',1,1)"
		'session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "update we_accounts set personalCode = '" & numberSeg & "' where accountID = " & rs("accountID")
		'session("errorSQL") = sql
		oConn.execute(sql)
		newCode = numberSeg
	end if
		
	strTemplate = replace(strTemplate, "##CODE##", newCode)

	'send an e-mail
	if instr(strTo, "@mail.marketplace.buy.com") = 0 then	
		set objEmail = CreateObject("CDO.Message")
		with objEmail
			.From = strFrom
		
			if isTestMode then 				
				.To = "terry@wirelessemporium.com"
'				.To = "jon@wirelessemporium.com"
				'.Bcc = "doldding82@hotmail.com;doldding82@yahoo.com"					
			else
				.To = strTo
				if lapCount = 1 then
					.Bcc = "jon@wirelessemporium.com"
				end if
			end if
		
			.Subject = strSubject
			.HTMLBody = strTemplate
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Update
			.Send
		end with
	end if

	strTemplate = dup_strTemplate	'refresh the template for an another e-mail.
	
	rs.movenext
loop

rs.close
set rs = nothing

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub

function getRatingAvgStar(rating)
	dim nRating 
	dim strRatingImg
	nRating = cdbl(rating)
	strRatingImg = 	"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />"
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""	
		for i=1 to 5
			if nRating => i then
				strRatingImg = strRatingImg & "<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_full.jpg"" border=""0"" width=""30"" height=""29"" /> "
			elseif nRating => ((i - 1) + .1) then
				strRatingImg = strRatingImg & "<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_half.jpg"" border=""0"" width=""30"" height=""29"" /> "				
			else
				strRatingImg = strRatingImg & "<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" /> "
			end if
		next		
	end if
	
	getRatingAvgStar = strRatingImg
end function


Function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
'		Filepath = Server.MapPath(fileName)
'		Filepath = FSO.GetAbsolutePathName(fileName)
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
End Function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function
