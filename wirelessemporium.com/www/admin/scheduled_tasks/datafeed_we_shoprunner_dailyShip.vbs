set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

username = "srwiem"	
Hostname = "sftp.wiem.shoprunner.net"
Password = "F8WQ2ga5l5#o"
port = 22
set sftp = CreateObject("Chilkat.SFtp")
success = sftp.UnlockComponent("WIRELESSH_zHJQ3Uch8Hni")
success = sftp.Connect(hostname,port)
success = sftp.AuthenticatePw(username,password)
success = sftp.InitializeSftp()

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")

strSDate = dateadd("d", -1, date)
strSDate = right("00" & month(strSDate), 2) & "/" & right("00" & day(strSDate), 2) & "/" & year(strSDate)
strEDate = date
strEDate = right("00" & month(strEDate), 2) & "/" & right("00" & day(strEDate), 2) & "/" & year(strEDate)

strYear = year(now)
strMonth = Right("0" & month(now), 2)
strDay = Right("0" & day(now), 2)
strHour = hour(now)
strMinute = minute(now)
strSec = second(now)

filename = "WIEM_" & strYear & strMonth & strDay & strHour & strMinute & "_shipment-feed.xml"
path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\shoprunner\" & filename

set file = fs.CreateTextFile(path)

sql	=	"select	a.scandate, a.orderid, isnull(nullif(a.trackingnum, ''), a.orderid) trackingnum, a.shiptype" & vbcrlf & _
		"	,	b.fname, b.lname, b.saddress1, b.saddress2, b.scity, b.sstate, b.szip, b.scountry, a.shiptype" & vbcrlf & _
		"	,	isnull(sum(c.quantity), 0) numOfItems" & vbcrlf & _
		"	,	sum(case when m.id is not null then 0 else case when d.vendor in ('CM', 'DS', 'MLD') then 0 when d.partnumber like '%-HYP-%' then 0 else c.quantity end end) numOfSRItems" & vbcrlf & _
		"from	we_orders a join we_accounts b" & vbcrlf & _
		"	on	a.accountid = b.accountid join we_orderdetails c" & vbcrlf & _
		"	on	a.orderid = c.orderid left outer join we_items d" & vbcrlf & _
		"	on	c.itemid = d.itemid left outer join we_items_musicskins m" & vbcrlf & _
		"	on	c.itemid = m.id" & vbcrlf & _
		"where	a.scandate >= '" & strSDate & "'" & vbcrlf & _
		"	and	a.scandate < '" & strEDate & "'" & vbcrlf & _
		"	and	a.thub_posted_to_accounting is not null and	a.approved = 1" & vbcrlf & _
		"	and	(a.cancelled is null or a.cancelled = 0) and a.parentOrderID is null" & vbcrlf & _
		"	and	(a.extordertype in (1,2,3) or a.extordertype is null)" & vbcrlf & _
		"	and	a.store = 0 and a.shoprunnerID is not null and a.shoprunnerid <> ''" & vbcrlf & _
		"--where	a.orderid in (1432393,1433578,1433597)" & vbcrlf & _
		"group by	a.scandate, a.orderid, isnull(nullif(a.trackingnum, ''), a.orderid), a.shiptype" & vbcrlf & _
		"		,	b.fname, b.lname, b.saddress1, b.saddress2, b.scity, b.sstate, b.szip, b.scountry, a.shiptype" & vbcrlf & _
		"order by 2"

set rs = CreateObject("ADODB.Recordset")
rs.open SQL, oConn, 0, 1
if not rs.eof then
	'core columns
	heading	=	"<?xml version=""1.0"" encoding=""utf-8""?>" & vbcrlf & _
				"<Shipments>" & vbcrlf & _
				"	<Partner>WIEM</Partner>"
	file.writeline heading
	
	do until rs.eof
		sZip = rs("szip")
		carrierCode = "UPS"
		srShipType = "SHOPRUNNER.UPS.2DA"
		if left(sZip, 5) <> "" then
			if isnumeric(left(sZip, 5)) then
				if left(sZip, 5) > 80000 and left(sZip, 5) < 99355 then 
					carrierCode = "ONTRAC"
					srShipType = "SHOPRUNNER.ONTRAC.2DA"
				end if
			end if
		end if
		
		sCountry = rs("sCountry")
		if sCountry <> "US" then sCountry = "CA"
		
		xml	=	"	<Shipment>" & vbcrlf & _
				"		<ShipmentType></ShipmentType>" & vbcrlf & _
				"		<RetailerOrderNumber>" & rs("orderid") & "</RetailerOrderNumber>" & vbcrlf & _
				"		<CarrierCode>" & carrierCode & "</CarrierCode>" & vbcrlf & _
				"		<TrackingNumber>" & rs("trackingnum") & "</TrackingNumber>" & vbcrlf & _
				"		<ShippingMethod>" & srShipType & "</ShippingMethod>" & vbcrlf & _
				"		<NumberOfItems>" & rs("numOfItems") & "</NumberOfItems>" & vbcrlf & _
				"		<NumberOfSRItems>" & rs("numOfSRItems") & "</NumberOfSRItems>" & vbcrlf & _
				"		<ShippingAddress>" & vbcrlf & _
				"			<Name>" & replace(rs("fname"), "&", "&amp;") & " " & replace(rs("lname"), "&", "&amp;") & "</Name>" & vbcrlf & _
				"			<Line1>" & replace(rs("saddress1"), "&", "&amp;") & "</Line1>" & vbcrlf & _
				"			<Line2>" & replace(rs("saddress2"), "&", "&amp;") & "</Line2>" & vbcrlf & _
				"			<City>" & replace(rs("scity"), "&", "&amp;") & "</City>" & vbcrlf & _
				"			<State>" & rs("sstate") & "</State>" & vbcrlf & _
				"			<PostalCode>" & sZip & "</PostalCode>" & vbcrlf & _
				"			<CountryCode>" & sCountry & "</CountryCode>" & vbcrlf & _
				"		</ShippingAddress>" & vbcrlf & _
				"	</Shipment>"
		file.writeline xml
		rs.movenext
	loop
	file.writeline "</Shipments>"
end if
rs.close
set rs = nothing
file.close()

handle = sftp.OpenFile("/Inbox/" & filename,"writeOnly","createTruncate")
If (handle = vbNullString ) Then
    response.Write("<pre>" & sftp.LastErrorText & "</pre>")
    response.End()
End If

path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\shoprunner\" & filename
success = sftp.UploadFile(handle,path)
success = sftp.CloseHandle(handle)

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
