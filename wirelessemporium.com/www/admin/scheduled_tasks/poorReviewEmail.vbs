set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

dim fs : set fs = CreateObject("Scripting.FileSystemObject")

strFrom = "sales@wirelessemporium.com"
strSubject = "*** Poor Reviews ***"

oConn.execute("delete from we_reviews where (email = '0' and nickname = '0') or (reviewTitle = '0' and convert(varchar(8000), review) = '0') or (reviewTitle = '' and convert(varchar(8000), review) = '')")
oConn.execute("delete from ps_reviews where (nickname = '0') or (reviewTitle = '0' and convert(varchar(8000), review) = '0') or (reviewTitle = '' and convert(varchar(8000), review) = '') or (reviewTitle = '' and convert(varchar(8000), review) = '0')")

sql	=	"select	a.id, a.site_id, c.shortDesc, c.color, a.id, isnull(a.orderid, 0) orderid, a.nickname, a.rating, a.reviewTitle, a.review, isnull(nullif(a.email, ''), 'N/A') email, b.itemid, b.price_our, b.partnumber, a.datetimeentd" & vbcrlf & _
		"	,	b.itemdesc, b.itempic, b.itempic_co" & vbcrlf & _
		"from	v_reviews a left outer join we_items b" & vbcrlf & _
		"	on	a.itemid = b.itemid join xstore c" & vbcrlf & _
		"	on	a.site_id = c.site_id left outer join we_orders d" & vbcrlf & _
		"	on	a.orderid = d.orderid left outer join v_accounts e" & vbcrlf & _
		"	on	d.accountid = e.accountid and d.store = e.site_id" & vbcrlf & _
		"where	a.datetimeentd >= convert(varchar(10), getdate()-1, 20)" & vbcrlf & _
		"	and	a.datetimeentd < convert(varchar(10), getdate(), 20)" & vbcrlf & _
		"	and	a.rating < 3" & vbcrlf & _
		"order by 2, 5"


set rs = oConn.execute(sql)

if not rs.eof then
	strHtml = "<table width=""700"" border=""0"" cellpadding=""0"" cellspacing=""0"">"
	do until rs.eof
		useImgPath = "http://www.wirelessemporium.com/productpics/thumb/" & rs("itemPic")
		if not fs.FileExists("C:\inetpub\wwwroot\productpics\thumb\" & rs("itemPic")) then 
			if fs.FileExists("C:\inetpub\wwwroot\productpics_co\thumb\" & rs("itemPic_co")) then
				useImgPath = "http://www.cellularoutfitter.com/productpics/thumb/" & rs("itemPic_co")			
			else
				useImgPath = "http://www.wirelessemporium.com/productpics/thumb/imagena.jpg"
			end if
		end if
'		if rs("site_id") = 0 then
'			useImgPath = "http://www.wirelessemporium.com/productpics/thumb/" & rs("itemPic")
'		elseif rs("site_id") = 2 then
'			useImgPath = "http://www.cellularoutfitter.com/productpics/thumb/" & rs("itemPic_co")		
'		end if
		
		dupContent = false
		if lastContent = rs("review") then dupContent = true
		
		if not dupContent then
			strHtml = strHtml & "<tr>"
			strHtml = strHtml & "	<td style=""border-bottom:1px solid #ccc; text-align:center; padding:20px 10px 20px 10px;"">"
			strHtml = strHtml & "		<img src=""" & useImgPath & """ border=""0"" width=""100"" height=""100"" />"
			strHtml = strHtml & "	</td>"
			strHtml = strHtml & "	<td style=""border-bottom:1px solid #ccc; border-right:1px solid #ccc; text-align:center;"">"
			strHtml = strHtml & "		<table width=""100%"" border=""0"" cellpadding=""5"" cellspacing=""0"" style=""font-size:12px;"">"
			strHtml = strHtml & "			<tr>"
			strHtml = strHtml & "				<td colspan=""6"" style=""border-bottom:1px solid #ccc;"" align=""left""><b>Review ID</b>: " & rs("id")
			strHtml = strHtml & "				</td>"	
			strHtml = strHtml & "			</tr>"
			strHtml = strHtml & "			<tr>"
			strHtml = strHtml & "				<td style=""border-right:1px solid #ccc; border-bottom:1px solid #ccc;"" align=""left"">Site<br /><span style=""font-weight:bold; color:" & rs("color") & """>" & rs("shortDesc") & "</span>"
			strHtml = strHtml & "				</td>"		
			strHtml = strHtml & "				<td style=""border-right:1px solid #ccc; border-bottom:1px solid #ccc;"" align=""left"">Partnumber<br />" & rs("partnumber")
			strHtml = strHtml & "				</td>"	
			strHtml = strHtml & "				<td style=""border-right:1px solid #ccc; border-bottom:1px solid #ccc;"" align=""left"">Review Date<br />" & rs("datetimeentd")
			strHtml = strHtml & "				</td>"	
			strHtml = strHtml & "				<td style=""border-right:1px solid #ccc; border-bottom:1px solid #ccc;"" align=""left"">Email<br />" & rs("email")
			strHtml = strHtml & "				</td>"
			strHtml = strHtml & "				<td style=""border-right:1px solid #ccc; border-bottom:1px solid #ccc;"" align=""left"">OrderID<br />" & rs("orderid")
			strHtml = strHtml & "				</td>"	
			strHtml = strHtml & "				<td style=""border-bottom:1px solid #ccc;"" align=""left"">Rating<br />" & rs("rating")
			strHtml = strHtml & "				</td>"
			strHtml = strHtml & "			</tr>"
			strHtml = strHtml & "			<tr>"
			strHtml = strHtml & "				<td colspan=""6"" align=""left""><b>" & rs("reviewTitle") & "</b><br />" & rs("review")
			strHtml = strHtml & "				</td>"	
			strHtml = strHtml & "			</tr>"
			strHtml = strHtml & "		</table>"
			strHtml = strHtml & "	</td>"
			strHtml = strHtml & "</tr>"		
		end if
		
		lastContent = rs("review")
		
		rs.movenext
	loop
	strHtml = strHtml & "</table>"
	
'	response.write strHtml
	set objEmail = CreateObject("CDO.Message")
	with objEmail
		.From = strFrom
		.To = emailList("Bad reviews email")
		.Bcc = "terry@wirelessemporium.com"
'		.To = "terry@wirelessemporium.com"
		.Subject = strSubject
		.HTMLBody = strHtml
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Update
		.Send
	end with
end if
rs.close
set rs = nothing


function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function

function emailList(emailName)
	sql = "select a.emailAddr, b.email from we_emailLists a left join we_adminUsers b on a.adminID = b.adminID where emailName = '" & emailName & "'"
	set emailListRS = oConn.execute(sql)
	
	toList = ""
	do while not emailListRS.EOF
		email1 = emailListRS("emailAddr")
		email2 = emailListRS("email")
		
		if isnull(email1) then
			toList = toList & email2 & ","
		else
			toList = toList & email1 & ","
		end if
		emailListRS.movenext
	loop
	if toList <> "" then toList = left(toList,len(toList)-1)
	emailList = toList
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
