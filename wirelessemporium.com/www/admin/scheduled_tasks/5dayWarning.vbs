set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim fs, file, filename, path
filename = "fiveDayWarning.csv"
path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename
set fs = CreateObject("Scripting.FileSystemObject")
if fs.FileExists(path) then fs.DeleteFile(path)
set file = fs.CreateTextFile(path)

SQL = "exec sp_fiveDayWarning"
Set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.eof then
	bodyText = "<h3>Inventory levels are solid!</h3>"
else
	file.WriteLine "PartNumber,Description,Inv,RunRate,Vendors"
	do until RS.eof
		vedorList = ""
		curPartNumber = RS("PartNumber")
		strToWrite = curPartNumber & ","
		strToWrite = strToWrite & chr(34) & RS("itemDesc") & chr(34) & ","
		strToWrite = strToWrite & chr(34) & RS("inv_qty") & chr(34) & ","
		strToWrite = strToWrite & chr(34) & RS("perWeek") & chr(34) & ","
		do while RS("PartNumber") = curPartNumber
			vedorList = vedorList & RS("vendor") & ","
			RS.movenext
			if RS.EOF then exit do
		loop
		if vedorList = "," then vedorList = ""
		if vedorList <> "" then vedorList = left(vedorList,len(vedorList)-1)
		strToWrite = strToWrite & chr(34) & vedorList & chr(34)
		file.WriteLine strToWrite
	loop
	bodyText = "<h3>Inventory running low on some products!</h3>"
	bodyText = bodyText & "<h3>(CSV FILE ATTACHED)</h3>"
end if

RS.CLOSE
Set RS = nothing
Set RS2 = nothing
file.Close()

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub

Set objErrMail = CreateObject("CDO.Message")
With objErrMail
	.From = "admin@wirelessemporium.com"
	.To = "wepurchasing@wirelessemporium.com"
	'.To = "jon@wirelessemporium.com"
	.Subject = "5 Day Inventory Warning"
	.HTMLBody = bodyText
	.AddAttachment path
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
	.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
	.Configuration.Fields.Update
	.Send
End With

Set objErrMail = nothing
Set filesys = nothing
Set demofolder = nothing
Set demofile = nothing
Set filecoll = nothing
