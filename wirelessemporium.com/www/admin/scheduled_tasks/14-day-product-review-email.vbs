set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

isTestMode = 0
strTemplate = ""
strFrom = "sales@wirelessemporium.com"
strSubject = "Help Your Fellow Customers - Write a Review"
if isTestMode = 1 then strSubject = "[TestBatch] Help Your Fellow Customers - Write a Review"
strTo = ""

sql	=	"exec getReviewEmail 0, 14, " & isTestMode

set rs = oConn.execute(sql)

strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\images\email\template\order-products-review.htm")
dup_strTemplate = strTemplate

lapCount 	= 0
noSkip 		= false
strItems 	= ""
strCustName = ""
do until rs.eof
	lapCount = lapCount + 1
	if lapCount > 3 then noSkip = false

	if strTo <> rs("email") then
		numItem	= 0
		lapCount = 1
		noSkip = true
		
		if "" <> strItems then
			strUpSell = getCrossSellProducts(item_ids)
			item_ids = ""
			strTemplate = replace(strTemplate, "[FIRST NAME]", strCustName)
			strTemplate = replace(strTemplate, "[ORDER DETAILS]", strItems)
			strTemplate = replace(strTemplate, "[UPSELL]", strUpSell)
			strTemplate = replace(strTemplate, "[EMAIL_PAGE]", "http://www.wirelessemporium.com/images/email/review/review?a=" & lastAccountID & "&o=" & lastOrderID & "&dzid=email_transactional_HEADER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=weReview&utm_content=CONFR&utm_promocode=NA")
			strTemplate = replace(strTemplate, "[COPYRIGHT YEAR]", Year(Date))
			strItems = ""
			'send an e-mail
'			response.write strTemplate & "<br><br>"
		
			on error resume next
			set objEmail = CreateObject("CDO.Message")
			with objEmail
				.From = strFrom

				if isTestMode = 1 then
					.To = "terry@wirelessemporium.com,wemarketing@wirelessemporium.com"
					.Bcc = ""					
				else
					.To = strTo
					.Bcc = ""
				end if

				.Subject = strSubject
				.HTMLBody = strTemplate
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
				.Configuration.Fields.Update
				.Send
			end with
			on error goto 0
		
			strTemplate = dup_strTemplate	'refresh the template for an another e-mail.
		end if
	end if

	strCustName = rs("fname")
	strTo 		= rs("email")	
	lastAccountID = rs("accountid")
	lastOrderID = rs("orderid")

	if instr(rs("email"), "@") > 0 and len(strCustName) > 0 and noSkip then
		itemid		=	rs("itemid")
		if item_ids <> "" then
			item_ids = item_ids & ","
		end if
		item_ids = item_ids & itemid
		
		itemDesc 	=	rs("itemdesc")
		partnumber	=	rs("partnumber")
		
		itempic		= rs("itempic")
		
		'itemImgPath = server.MapPath("/productPics/thumb/" & itempic)
		itemImgPath = "C:\inetpub\wwwroot\productpics\thumb\" & itempic
		set fsThumb = CreateObject("Scripting.FileSystemObject")
		if not fsThumb.FileExists(itemImgPath) then
			useImg = "http://www.wirelessemporium.com/productPics/thumb/imagena.jpg"
			DoNotDisplay = 1
		else
			useImg = "http://www.wirelessemporium.com/productPics/thumb/" & itempic
		end if
		
		productLink = "http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".html?dzid=email_transactional_ORDERREVIEW_" & numItem & "L_" & itemid & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=weReview&utm_content=CONFR&utm_promocode=NA"
		
		strItems =	strItems &	"	<tr>" & vbcrlf & _
								"		<td width=""20"">&nbsp;</td>" & vbcrlf & _
								"		<td width=""598"" style=""border:1px solid #cdcdcd;"">" & vbcrlf & _
								"			<table width=""598"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf & _
								"				<tr><td bgcolor=""#FFFFFF""; >&nbsp;</td></tr>" & vbcrlf & _
								"				<tr>" & vbcrlf & _
								"					<td width=""20"">&nbsp;</td>" & vbcrlf & _
								"					<td width=""67"" height=""67"" style=""text-align:center;vertical-align:middle;border:1px solid #ccc;border-radius:5px;""><a href=""#""><img src=""" & useImg & """ alt=""" & itemDesc & """ height=""67"" border=""0"" /></a></td>" & vbcrlf & _
								"					<td width=""20"">&nbsp;</td>" & vbcrlf & _
								"					<td width=""332"" valign=""top"" style=""font:normal 12px Verdana, Geneva, sans-serif; line-height:18px;""><a href=""" & productLink & """ style=""color:#0687c9; text-decoration:none; "">" & itemDesc & "</a></td>" & vbcrlf & _
								"					<td width=""20"">&nbsp;</td>" & vbcrlf & _
								"					<td width=""119"" valign=""middle"" ><a href=""http://www.wirelessemporium.com/user-reviews-b?itemID=" & itemid & "&partNumber=" & partnumber & """ target=""_blank""><img src=""http://www.wirelessemporium.com/images/order-products-review/WE-review-email_cta_review.jpg"" alt="""" width=""119"" height=""35"" border=""0"" style=""display:block"" /></a></td>" & vbcrlf & _
								"					<td width=""20"">&nbsp;</td>" & vbcrlf & _
								"				</tr>" & vbcrlf & _
								"				<tr><td bgcolor=""#FFFFFF""; >&nbsp;</td></tr>" & vbcrlf & _
								"			</table>" & vbcrlf & _
								"		</td>" & vbcrlf & _
								"		<td width=""20"">&nbsp;</td>" & vbcrlf & _
								"	</tr>" & vbcrlf & _
								"	<tr><td>&nbsp;</td></tr>"
		numItem = numItem + 1
	end if
	
	rs.movenext
loop

strUpSell = getCrossSellProducts(item_ids)
			
strTemplate = replace(strTemplate, "[FIRST NAME]", strCustName)
strTemplate = replace(strTemplate, "[ORDER DETAILS]", strItems)
strTemplate = replace(strTemplate, "[UPSELL]", strUpSell)
strTemplate = replace(strTemplate, "[EMAIL_PAGE]", "http://www.wirelessemporium.com/images/email/review/review?a=" & lastAccountID & "&o=" & lastOrderID & "&dzid=email_transactional_HEADER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=weReview&utm_content=CONFR&utm_promocode=NA")
strTemplate = replace(strTemplate, "[COPYRIGHT YEAR]", Year(Date))
strItems = ""
if lapCount > 0 then
	'send a last one
	'response.write strTemplate & "<br><br>"
	set objEmail = CreateObject("CDO.Message")
	on error resume next
	with objEmail
		.From = strFrom
	
		if isTestMode = 1 then 				
			.To = "terry@wirelessemporium.com,wemarketing@wirelessemporium.com"
			.Bcc = ""					
		else
			.To = strTO
			.Bcc = "terry@wirelessemporium.com,wemarketing@wirelessemporium.com"
		end if
	
		.Subject = strSubject
		.HTMLBody = strTemplate
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Update
		.Send
	end with
	on error goto 0
end if

rs.close
set rs = nothing

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub

function getRatingAvgStar(rating)
	dim nRating 
	dim strRatingImg
	nRating = cdbl(rating)
	strRatingImg = 	"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />" & vbcrlf & _
					"<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" />"
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""	
		for i=1 to 5
			if nRating => i then
				strRatingImg = strRatingImg & "<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_full.jpg"" border=""0"" width=""30"" height=""29"" /> "
			elseif nRating => ((i - 1) + .1) then
				strRatingImg = strRatingImg & "<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_half.jpg"" border=""0"" width=""30"" height=""29"" /> "				
			else
				strRatingImg = strRatingImg & "<img src=""http://www.wirelessemporium.com/images/email/14-day-review/star_empty.jpg"" border=""0"" width=""30"" height=""29"" /> "
			end if
		next		
	end if
	
	getRatingAvgStar = strRatingImg
end function


Function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
'		Filepath = Server.MapPath(fileName)
'		Filepath = FSO.GetAbsolutePathName(fileName)
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
End Function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function

function getCrossSellProducts(item_ids)
	sqlUpSell = "exec sp_ppUpsell 0, '" & item_ids & "', 0, 30"
	set objRsUpSell = oConn.execute(sqlUpSell)
	if not objRsUpSell.EOF then
		strUpSell	=	"<tr>" & vbcrlf & _
						"	<td style='background-color: #ffffff;' width='640'>" & vbcrlf & _
						"		<table width='640' cellspacing='0' cellpadding='0'>" & vbcrlf & _
						"			<tbody>" & vbcrlf & _
						"				<tr>"
		
		counter = 1
		do until objRsUpSell.EOF
			itemID		= objRsUpSell("itemID")
			itemPic		= objRsUpSell("itemPic")
			itemDesc	= objRsUpSell("itemDesc")
			useItemDesc = replace(itemDesc,"  "," ")
			price		= objRsUpSell("price")
			discount	= price * 0.15
			promoPrice	= price - discount
			price_retail= objRsUpSell("price_retail")
			discount_pct= Round(cdbl((cdbl(price_retail) - cdbl(price)) * 100 / cdbl(price_retail)))
			discount	= cdbl(cdbl(price_retail) - cdbl(price))
			
			itemImgPath = "C:\inetpub\wwwroot\productpics\big\" & itempic
			set fsBig = CreateObject("Scripting.FileSystemObject")
			if not fsBig.FileExists(itemImgPath) then
				useImg = "/productPics/big/imagena.jpg"
				DoNotDisplay = 1
			else
				useImg = "/productPics/big/" & itemPic
			end if
			
			if counter = 3 or counter = 6 then
				style = "padding:20px 10px 0 20px;"
			else 
				style = "padding:20px 0 0 20px;"
			end if
			
			productLink = "http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(useItemDesc) & "?dzid=email_transactional_RECS_" & counter & "L_" & itemID & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=weReview&utm_content=CONFR&utm_promocode=NA"
			
			strUpSell 	= strUpSell &	"	<td width='210' style='" & style & "'>" & vbcrlf & _
											"		<table width='190' cellpadding='0' cellspacing='0'>" & vbcrlf & _
											"			<tbody>" & vbcrlf & _
											"				<tr>" & vbcrlf & _
											"					<td width='190' height='190'>" & vbcrlf & _
											"						<a href='" & productLink & "'><img src='http://www.wirelessemporium.com" & useImg & "' style='display:block;' width='190' /></a>" & vbcrlf & _
											"					</td>" & vbcrlf & _
											"				</tr>" & vbcrlf & _
											"				<tr>" & vbcrlf & _
											"					<td width='190' height='60' valign='top' style='font-family:Arial, sans-serif;font-size:14px;color:#000;'>" & vbcrlf & _
																	itemDesc & vbcrlf & _
											"					</td>" & vbcrlf & _
											"				</tr>" & vbcrlf & _
											"				<tr>" & vbcrlf & _
											"					<td>" & vbcrlf & _
											"						<span style='color:#999;font-size:12px;color:#ccc;text-decoration:line-through;font-family:Arial, sans-serif;'>Retail Price: " & formatCurrency(price_retail) & "</span><br />" & vbcrlf & _
											"					</td>" & vbcrlf & _
											"				</tr>" & vbcrlf & _
											"				<tr>" & vbcrlf & _
											"					<td style='padding:10px 0 0 0;'>" & vbcrlf & _
											"						<span style='color:#000;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;'>Our Price:</span> <span style='color:#ff6633;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;'>" & formatCurrency(price) & "</span><br />" & vbcrlf & _
											"						<span style='color:#000;font-size:14px;font-family:Arial, sans-serif;'>You Save:</span> <span style='margin:10px 0 0 0;color:#000;font-size:14px;font-family:Arial, sans-serif;font-weight:bold;'>" & Round(discount_pct)  & "% (" & formatCurrency(discount) & ")</span>" & vbcrlf & _
											"					</td>" & vbcrlf & _
											"				</tr>" & vbcrlf & _
											"				<tr>" & vbcrlf & _
											"					<td style='padding:10px 0 0 0;'>" & vbcrlf & _
											"						<table width='160' border='0' cellpadding='0' cellspacing='0'>" & vbcrlf & _
											"							<tbody>" & vbcrlf & _
											"								<tr>" & vbcrlf & _
											"									<td align='center' style='background-color:#ff6633;border-radius:16px;padding:7px 0;'>" & vbcrlf & _
											"										<a href='" & productLink & "' style='color:#fff;font-weight:bold;font-size:14px;font-family:Arial, sans-serif;text-decoration:none;'>SHOP NOW &raquo;</a>" & vbcrlf & _
											"									</td>" & vbcrlf & _
											"								</tr>" & vbcrlf & _
											"							</tbody>" & vbcrlf & _
											"						</table>" & vbcrlf & _
											"					</td>" & vbcrlf & _
											"				</tr>" & vbcrlf & _
											"			</tbody>" & vbcrlf & _
											"		</table>" & vbcrlf & _
											"	</td>"
			
			if counter = 3 then
				strUpSell = strUpSell & "						</tr>" & vbcrlf & _
							"					</tbody>" & vbcrlf & _
							"				</table>" & vbcrlf & _
							"				<br />" & vbcrlf & _
							"			</td>" & vbcrlf & _
							"		</tr>" & vbcrlf & _
							"		<tr>" & vbcrlf & _
							"		  <td style='background-color: #cccccc;' width='640' height='1'></td>" & vbcrlf & _
							"		</tr>" & vbcrlf & _
							"		<tr>" & vbcrlf & _
							"			<td style='background-color: #ffffff;' width='640'>" & vbcrlf & _
							"				<table width='640' cellspacing='0' cellpadding='0'>" & vbcrlf & _
							"					<tbody>" & vbcrlf & _
							"						<tr>"
				counter = counter + 1
			elseif counter = 6 then
				exit do
			else
				counter = counter + 1
			end if
			objRsUpSell.movenext
		loop
	end if
	strUpSell		= strUpSell & "						</tr>" & vbcrlf & _
							"					</tbody>" & vbcrlf & _
							"				</table>" & vbcrlf & _
							"				<br />" & vbcrlf & _
							"			</td>" & vbcrlf & _
							"		</tr>"
					

	getCrossSellProducts = strUpSell
end function
