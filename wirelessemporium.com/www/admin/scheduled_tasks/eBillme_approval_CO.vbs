set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

const SEB_PAYEE = "VPvNJXzrKmuB05GpkSAT4g=="
const SEB_USERNAME = "p0u#Ce11@dM1n"
const SEB_PASSWORD = "z3#G67YZe2fE"
'dim seb, r, REFERENCE_ID, extOrderNumber
'set seb = CreateObject("SeB.SeBImpl")
'r = seb.getPaymentDetails(SEB_PAYEE, SEB_USERNAME, SEB_PASSWORD)

Set WUPay = New eBillme_V3
orderStatusResponse = WUPay.GetOrderStatus()
'orderStatusResponse = 	"<getorderstatus_response base:generationdate=""2012-07-12 14:11:40"" xmlns=""http://ebillme.com/ws/v3"" xmlns:base=""http://ebillme.com/ws/base""> " & _
'						"<paymentchanges base:recordsize=""1""> " & _
'						"	<order> " & _
'						"		<orderrefid>469348</orderrefid> " & _
'						"		<ordernumber>1491192</ordernumber> " & _
'						"		<currency>USD</currency> " & _
'						"		<paystatus>F</paystatus> " & _
'						"		<paymentdate>2012-07-10 10:26:00</paymentdate> " & _
'						"		<totalprice>6.99</totalprice> " & _
'						"		<amountpaidtodate>6.99</amountpaidtodate> " & _
'						"		<amountowing>0.00</amountowing> " & _
'						"		<amountshippedtodate>0.00</amountshippedtodate> " & _
'						"		<amountrefundedtodate>0.00</amountrefundedtodate> " & _
'						"		<payments base:recordsize=""1""> " & _
'						"			<paymentdetails> " & _
'						"				<name>SCOTT MACK</name> " & _
'						"				<paymentdate>2012-07-10 10:26:00</paymentdate> " & _
'						"				<settlementdate>2012-07-12 01:00</settlementdate> " & _
'						"				<paymentsource>eBillme-RPPS</paymentsource> " & _
'						"				<amountpaid>6.99</amountpaid> " & _
'						"			</paymentdetails> " & _
'						"		</payments> " & _
'						"		<buyerdetails> " & _
'						"			<accountnumber>EB2312917</accountnumber> " & _
'						"		</buyerdetails> " & _
'						"	</order> " & _
'						"</paymentchanges> " & _
'						"</getorderstatus_response>"

Set xmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
xmlDoc.async = false
xmlDoc.loadXml orderStatusResponse

Set RootNode = xmlDoc.documentElement

Set PaymentNode = RootNode.selectNodes("paymentchanges/order")
for a = 0 to PaymentNode.Length - 1
	OrderNumber = GetElementText(PaymentNode(a),"ordernumber")
	PaymentStatus = GetElementText(PaymentNode(a),"paystatus")
	wupay_accountnumber = GetElementText(PaymentNode(a),"buyerdetails/accountnumber")
	amountpaidtodate = GetElementText(PaymentNode(a),"amountpaidtodate")
	paymentdate = GetElementText(PaymentNode(a),"paymentdate")
	
	sql = 	"insert into WUPay_Temp(orderid, ebillme_accountnumber, amountPaid, datePaid, statusPaid)" & vbcrlf & _
			"values('" & replace(OrderNumber, "'", "''") & "','" & replace(wupay_accountnumber, "'", "''") & "','" & replace(amountpaidtodate, "'", "''") & "','" & replace(paymentdate, "'", "''") & "','" & replace(PaymentStatus, "'", "''") & "')"	
	oConn.execute(sql)			
			
	if PaymentStatus = "F" then
		'make sure the order number is a real order
		SQL = "SELECT ordergrandtotal, orderid FROM we_orders WHERE orderid='" & OrderNumber & "'"
		Set RS = CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 0, 1
		'if the order is real then process inventory
		if not RS.eof then
			amountPaidToDate = cdbl(amountPaidToDate)
			orderGrandTotal = cdbl(RS("ordergrandtotal"))
			if amountPaidToDate >= orderGrandTotal then
				SQL = "UPDATE we_orders SET approved=1, emailSent='yes' WHERE orderid='" & OrderNumber & "'"
				oConn.execute SQL
				
				SQL = "SELECT a.itemid, a.quantity, b.ItemKit_NEW FROM we_orderdetails a left join we_items b on a.itemID = b.itemID WHERE a.orderid='" & OrderNumber & "'"
				Set RSinv = CreateObject("ADODB.Recordset")
				RSinv.open SQL, oConn, 0, 1
				'loop through each item in the order
				do until RSinv.eof
					dim decreaseSql, nProdIdCheck, nProdQuantity, nPartNumber, kit
					nProdIdCheck = RSinv("itemid")
					nProdQuantity = RSinv("quantity")
					kit = RSinv("ItemKit_NEW")
					if isNull(kit) then
						'grab the single items master id and master qty
						SQL = "SELECT a.PartNumber, b.itemID as masterID, b.inv_qty FROM we_items a left join we_items b on a.partNumber = b.partNumber and b.master = 1 WHERE a.itemID = '" & nProdIdCheck & "'"
					else
						'grab the master id and master qty for each item in the kit
						SQL = "SELECT a.PartNumber, b.itemID as masterID, b.inv_qty FROM we_items a left join we_items b on a.partNumber = b.partNumber and b.master = 1 WHERE a.itemID IN (" & kit & ")"
					end if
					set RS = oConn.execute(SQL)
					'loop through all items in kit, or single item if not in kit
					do while not RS.EOF
						nPartNumber = RS("PartNumber")
						masterID = RS("masterID")
						'adjust inventory
						decreaseSql = "UPDATE we_items SET inv_qty = CASE WHEN (inv_qty - " & nProdQuantity & " > 0) THEN inv_qty - " & nProdQuantity & " ELSE 0 END WHERE PartNumber = '" & nPartNumber & "' AND master = 1"
						oConn.execute(decreaseSql)
						'save inventory adjustment
						On Error Resume Next
						sql = "if not (select count(*) from we_invRecord where itemID = '" & masterID & "' and orderID = '" & OrderNumber & "') > 0 insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & nProdIdCheck & "," & RS("inv_qty") & "," & nProdQuantity & "," & OrderNumber & ",0,'WE eBillMe Customer Order','" & now & "')"
						session("errorSQL") = sql
						oConn.execute(sql)
						On Error Goto 0
						'set number of sales for select item
						sqlQuery = "UPDATE we_items SET numberOfSales = numberOfSales + " & nProdQuantity & " WHERE itemID = '" & nProdIdCheck & "'"
						oConn.Execute(SQL)
						
						RS.movenext
					loop
					RSinv.movenext
				loop
			end if
		else
			cdo_body = "WE Order Number " & OrderNumber & " not found for eBillme order!"
			cdo_to = "jon@wirelessemporium.com,terry@wirelessemporium.com,ruben@wirelessemporium.com"
			cdo_from = "service@wirelessemporium.com"
			cdo_subject = "eBillme Order Not Found!"
			CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		end if
	end if
next

set seb = nothing

Function GetElementText(Node, Tagname)
	Dim NodeList
	Set NodeList = Node.getElementsByTagname(Tagname)
	If NodeList.Length > 0 Then
		For b = 0 To NodeList.Length - 1
			Set CurrNode = NodeList.nextNode
			GetElementText = CurrNode.text
		Next
	Else
		GetElementText = ""
	End If
	Set NodeList = Nothing
End Function

FUNCTION CDOSend(strTo,strFrom,strSubject,strBody)
	Dim objErrMail
	Set objErrMail = CreateObject("CDO.Message")
	With objErrMail
		.From = strFrom
		.To = strTo
		'.bcc = "webmaster@wirelessemporium.com"
		.Subject = strSubject
		.HTMLBody = CStr("" & strBody)
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Update
		.Send
	End With
	Set objErrMail = nothing
END FUNCTION

Class eBillme_V3
	Dim serviceURL
	Dim oXmlHTTP
	Dim username
	Dim password
	Dim payeeToken
	Dim fso
	Dim dataPath

	Sub InitWSDL()
		username = SEB_USERNAME
		password = SEB_PASSWORD
		payeeToken = SEB_PAYEE
'		serviceUrl = "https://test.modasolutions.com/axis/eBillmeServiceV3.jws?WSDL"
		serviceUrl = "https://www.modasolutions.com/axis/eBillmeServiceV3.jws?WSDL"
'		Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
		Set oXmlHTTP = CreateObject("Msxml2.ServerXMLHTTP.3.0")
		oXmlHTTP.Open "POST", serviceUrl, False
		oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
		oXmlHTTP.setRequestHeader "SOAPAction", serviceUrl
		
		Set fso = CreateObject("Scripting.FileSystemObject")
		dataPath = fso.BuildPath("C:\inetpub\wwwroot\wirelessemporium.com\www\xmlFiles", "WUPay")
	End Sub


	Function GetSoapRequestXml(methodXml, methodName)
		Dim soapRequestXml
		soapRequestXml = "<?xml version='1.0' encoding='utf-8'?>"
		soapRequestXml = soapRequestXml & " <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:def=""http://DefaultNamespace"">"
		soapRequestXml = soapRequestXml & " <soapenv:Body>"
		soapRequestXml = soapRequestXml & " <def:" & methodName & " soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">"
		soapRequestXml = soapRequestXml & " <" & methodName & "Xml xsi:type=""xsd:string""><![CDATA[" & methodXml & " ]]></" & methodName & "Xml>"
		soapRequestXml = soapRequestXml & " </def:" & methodName & ">"
		soapRequestXml = soapRequestXml & " </soapenv:Body>"
		soapRequestXml = soapRequestXml & " </soapenv:Envelope>"
		GetSoapRequestXml = soapRequestXml
	End Function


	Function GetSettlementDetails(startdate, enddate)
		Dim soapRequestXml, soapResponseXml, methodXml
		Dim objXmlDoc, objXmlNode
		
		InitWSDL()
		
		Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
		objXmlDoc.Async = False
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		objXmlDoc.Load(fso.BuildPath(dataPath, "getsettlementdetails_request.xml"))
		objXmlDoc.setProperty "SelectionNamespaces", "xmlns:ns='http://ebillme.com/ws/v3'"

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getsettlementdetails_request/ns:authorization/ns:username")
		objXmlNode.text = username

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getsettlementdetails_request/ns:authorization/ns:password")
		objXmlNode.text = password

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getsettlementdetails_request/ns:authorization/ns:payeetoken")
		objXmlNode.text = payeeToken

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getsettlementdetails_request/ns:startdate")
		objXmlNode.text = startdate

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getsettlementdetails_request/ns:enddate")
		objXmlNode.text = enddate
		
		methodXml = objXmlDoc.xml
		
		soapRequestXml = GetSoapRequestXml(methodXml, "getSettlementDetails")
		oXmlHTTP.send soapRequestXml
		soapResponseXml = oXmlHTTP.responseXML.xml
		GetSettlementDetails = GetSoapResponseXml(soapResponseXml, "getSettlementDetails")
	End Function


	Function GetOrderInformation(orderid)
		Dim soapRequestXml, soapResponseXml, methodXml
		Dim objXmlDoc, objXmlNode
		
		InitWSDL()

		Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
		objXmlDoc.Async = False
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		objXmlDoc.Load(fso.BuildPath(dataPath, "getorderinformation_request.xml"))
		objXmlDoc.setProperty "SelectionNamespaces", "xmlns:ns='http://ebillme.com/ws/v3'"

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getorderinformation_request/ns:authorization/ns:username")
		objXmlNode.text = username

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getorderinformation_request/ns:authorization/ns:password")
		objXmlNode.text = password

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getorderinformation_request/ns:authorization/ns:payeetoken")
		objXmlNode.text = payeeToken

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getorderinformation_request/ns:orderrefid")
		objXmlNode.text = orderid

		methodXml = objXmlDoc.xml

		soapRequestXml = GetSoapRequestXml(methodXml, "getOrderInformation")
		oXmlHTTP.send soapRequestXml
		soapResponseXml = oXmlHTTP.responseXML.xml
		GetOrderInformation = GetSoapResponseXml(soapResponseXml, "getOrderInformation")
	End Function


	Function GetOrderStatus()
		Dim soapRequestXml, soapResponseXml, methodXml
		Dim objXmlDoc, objXmlNode

		InitWSDL()
		
		Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
		objXmlDoc.Async = False
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		objXmlDoc.Load(fso.BuildPath(dataPath, "getorderstatus_request.xml"))
		objXmlDoc.setProperty "SelectionNamespaces", "xmlns:ns='http://ebillme.com/ws/v3'"

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getorderstatus_request/ns:authorization/ns:username")
		objXmlNode.text = username

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getorderstatus_request/ns:authorization/ns:password")
		objXmlNode.text = password

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:getorderstatus_request/ns:authorization/ns:payeetoken")
		objXmlNode.text = payeeToken

		methodXml = objXmlDoc.xml

		soapRequestXml = GetSoapRequestXml(methodXml, "getOrderStatus")

		oXmlHTTP.send soapRequestXml
		soapResponseXml = oXmlHTTP.responseXML.xml
		GetOrderStatus = GetSoapResponseXml(soapResponseXml, "getOrderStatus")
	End Function
	

	Function UpdateOrder(orderid, expirydate)
		Dim soapRequestXml, soapResponseXml, methodXml
		Dim objXmlDoc, objXmlNode

		InitWSDL()
		
		Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
		objXmlDoc.Async = False
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		objXmlDoc.Load(fso.BuildPath(dataPath, "updateorderinformation_request.xml"))
		objXmlDoc.setProperty "SelectionNamespaces", "xmlns:ns='http://ebillme.com/ws/v3'"

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:updateorderinformation_request/ns:authorization/ns:username")
		objXmlNode.text = username

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:updateorderinformation_request/ns:authorization/ns:password")
		objXmlNode.text = password

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:updateorderinformation_request/ns:authorization/ns:payeetoken")
		objXmlNode.text = payeeToken

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:updateorderinformation_request/ns:order/ns:orderrefid")
		objXmlNode.text = orderid

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:updateorderinformation_request/ns:order/ns:expirydate")
		objXmlNode.text = expirydate
		
		methodXml = objXmlDoc.xml
		
		soapRequestXml = GetSoapRequestXml(methodXml, "updateOrderInformation")
		oXmlHTTP.send soapRequestXml
		soapResponseXml = oXmlHTTP.responseXML.xml
		UpdateOrder = GetSoapResponseXml(soapResponseXml, "updateOrderInformation")
	End Function


	Function CancelOrder(orderid, reasonid)
		Dim soapRequestXml, soapResponseXml, methodXml
		Dim objXmlDoc, objXmlNode

		InitWSDL()

		Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
		objXmlDoc.Async = False
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		objXmlDoc.Load(fso.BuildPath(dataPath, "cancelorder_request.xml"))
		objXmlDoc.setProperty "SelectionNamespaces", "xmlns:ns='http://ebillme.com/ws/v3'"

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:cancelorder_request/ns:authorization/ns:username")
		objXmlNode.text = username

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:cancelorder_request/ns:authorization/ns:password")
		objXmlNode.text = password

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:cancelorder_request/ns:authorization/ns:payeetoken")
		objXmlNode.text = payeeToken

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:cancelorder_request/ns:orderrefid")
		objXmlNode.text = orderid

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:cancelorder_request/ns:reasonid")
		objXmlNode.text = reasonid
		
		methodXml = objXmlDoc.xml
		
		soapRequestXml = GetSoapRequestXml(methodXml, "cancelOrder")
		oXmlHTTP.send soapRequestXml
		soapResponseXml = oXmlHTTP.responseXML.xml
		CancelOrder = GetSoapResponseXml(soapResponseXml, "cancelOrder")
	End Function
	

	Function SubmitRefund(orderid, refundamount, refundreason)
		Dim soapRequestXml, soapResponseXml, methodXml
		Dim objXmlDoc, objXmlNode

		InitWSDL()

		Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
		objXmlDoc.Async = False
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		objXmlDoc.Load(fso.BuildPath(dataPath, "submitrefund_request.xml"))
		objXmlDoc.setProperty "SelectionNamespaces", "xmlns:ns='http://ebillme.com/ws/v3'"

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitrefund_request/ns:authorization/ns:username")
		objXmlNode.text = username

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitrefund_request/ns:authorization/ns:password")
		objXmlNode.text = password

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitrefund_request/ns:authorization/ns:payeetoken")
		objXmlNode.text = payeeToken

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitrefund_request/ns:orderrefid")
		objXmlNode.text = orderid

		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitrefund_request/ns:refundamount")
		objXmlNode.text = refundamount
		
		Set objXmlNode = objXmlDoc.selectSingleNode("/ns:submitrefund_request/ns:refundreason")
		objXmlNode.text = refundreason

		methodXml = objXmlDoc.xml
		
		soapRequestXml = GetSoapRequestXml(methodXml, "submitRefund")
		oXmlHTTP.send soapRequestXml
		soapResponseXml = oXmlHTTP.responseXML.xml
		SubmitRefund = GetSoapResponseXml(soapResponseXml, "submitRefund")
	End Function
	
	
	Function GetSoapResponseXml(responseXml, methodName)
		Dim objXmlDoc, objXmlNode
		
		GetSoapResponseXml = ""
		
		Set objXmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
		objXmlDoc.Async = False
		objXmlDoc.setProperty "SelectionLanguage", "XPath"

		objXmlDoc.LoadXml(responseXml)
		
		Set objXmlNode = objXmlDoc.selectSingleNode("//" & methodName & "Return")
		If Not (objXmlNode Is Nothing) Then
			GetSoapResponseXml = objXmlNode.Text
			Set objXmlNode = Nothing
		Else
			objXmlDoc.setProperty "SelectionNamespaces", "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'"
			Set objXmlNode = objXmlDoc.selectSingleNode("//soapenv:Fault")
			If Not (objXmlNode Is Nothing) Then
				GetSoapResponseXml = objXmlNode.Xml
				Set objXmlNode = Nothing
			End If			
		End If
		Set objXmlDoc = Nothing
	End Function

End Class

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
