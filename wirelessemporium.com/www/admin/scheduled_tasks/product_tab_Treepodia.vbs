set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
'Map the file name to the physical path on the server.
filename = "productList_Treepodia.txt"
'path = "D:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename
path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename

if fs.FileExists(path) then fs.DeleteFile(path)
set file = fs.CreateTextFile(path)

dim SQL, RS, strline
set RS = oConn.execute("exec feed_trpdVideo")
if not RS.eof then
	heading = 			"Manufacturer" & vbtab & "Manufacturer Part #" & vbtab & "Product Name" & vbtab
	heading = heading & "Product Description" & vbtab & "Click-Out URL" & vbtab & "Price" & vbtab & "Category: Other Format" & vbtab
	heading = heading & "Category: Treepodia Numeric ID" & vbtab & "Image URL" & vbtab & "Ground Shipping" & vbtab & "Stock Status" & vbtab
	heading = heading & "Product Condition" & vbtab & "Marketing Message" & vbtab & "Weight" & vbtab & "Cost-per-Click" & vbtab & "UPC" & vbtab
	heading = heading & "Distributor ID" & vbtab & "MUZE ID" & vbtab & "Features" & vbtab & "Category" & vbTab
	heading = heading & "Image URL(Thumb)" & vbTab & "Image URL(AltView1)" & vbTab & "Image URL(AltView2)" & vbTab & "Image URL(AltView3)" & vbTab & "PartNumber"
	file.WriteLine heading
	
	do until RS.eof
		if not isnull(RS("itemlongdetail")) then
			strline = RS("brandName") & vbtab
			strline = strline & "WE-" & RS("itemID") & vbtab
			strline = strline & RS("itemDesc") & vbtab
			strline = strline & replace(RS("itemLongDetail"), vbcrlf, " ") & vbtab
			strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?refer=treepodia" & vbtab
			strline = strline & RS("price_our") & vbtab
			strline = strline & vbtab
			strline = strline & "500070 : Electronics / Phones & Communications / Cell Phone Accessories" & vbtab
			strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab
			strline = strline & "0" & vbtab
			strline = strline & "Yes" & vbtab
			strline = strline & "New" & vbtab
			strline = strline & "Free Shipping on All Orders" & vbtab
			strline = strline & vbtab
			strline = strline & vbtab
			strline = strline & RS("UPCCode") & vbtab
			strline = strline & vbtab
			strline = strline & vbtab
	
			'features
			if isnull(RS("bullet1")) or len(RS("bullet1")) < 1 then
				for i = 1 to 10
					if i = 1 and not isnull(RS("point" & i)) and len(RS("point" & i)) > 0 then
	
						strline = strline & RS("point" & i)
					elseif not isnull(RS("point" & i)) and len(RS("point" & i)) > 0 then
						strline = strline & ";" & RS("point" & i)
					end if
				next
				strline = strline & " - " & RS("itemDesc")
				strline = strline & vbtab
			else
				for i = 1 to 10
					if i = 1 and not isnull(RS("bullet" & i)) and len(RS("bullet" & i)) > 0 then
						strline = strline & RS("bullet" & i)
					elseif not isnull(RS("bullet" & i)) and len(RS("bullet" & i)) > 0 then
						strline = strline & ";" & RS("bullet" & i)
					end if
				next
				strline = strline & " - " & RS("itemDesc")
				strline = strline & vbtab
			end if
			
			strline = strline & RS("typename") & vbtab
			strline = strline & "http://www.wirelessemporium.com/productpics/thumb/" & RS("itemPic") & vbtab
			
			for iCount = 0 to 2
				src = ""
				path = "d:\productpics\AltViews\" & replace(RS("itemPic"),".jpg","-" & iCount & ".jpg")
				if fs.FileExists(path) then
					src = "http://www.wirelessemporium.com/productpics/altviews/" & replace(RS("itemPic"),".jpg","-" & iCount & ".jpg")
				end if
	
				path = "d:\productpics\AltViews\" & replace(RS("itemPic"),".jpg","-" & iCount & ".gif")
				if fs.FileExists(path) then
					src = "http://www.wirelessemporium.com/productpics/altviews/" & replace(RS("itemPic"),".jpg","-" & iCount & ".gif")
				end if
	
				path = "d:\productpics\AltViews\" & replace(RS("itemPic"),".gif","-" & iCount & ".jpg")
				if fs.FileExists(path) then
					src = "http://www.wirelessemporium.com/productpics/altviews/" & replace(RS("itemPic"),".gif","-" & iCount & ".jpg")
				end if
				
				path = "d:\productpics\AltViews\" & replace(RS("itemPic"),".gif","-" & iCount & ".gif")
				if fs.FileExists(path) then
					src = "http://www.wirelessemporium.com/productpics/altviews/" & replace(RS("itemPic"),".gif","-" & iCount & ".gif")
				end if
				
				strline = strline & src & vbtab
			next
	
			strline = strline & RS("PartNumber")
			
			file.WriteLine strline
		end if

		RS.movenext
	loop
end if
RS.close
set RS = nothing
file.Close()

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(formatSEO,"---","-")
		formatSEO = replace(formatSEO,"--","-")		
		formatSEO = replace(formatSEO,"?","-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
