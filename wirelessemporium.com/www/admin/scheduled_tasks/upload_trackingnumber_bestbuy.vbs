set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

SDate = dateadd("d", -13, date)
EDate = dateadd("d", 1, date)

set fs = CreateObject("Scripting.FileSystemObject")
filename = "35819779_" & right("00" & month(date), 2) & right("00" & day(date), 2) & year(date) & "_ShippingConfirmation.txt"
path = "c:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\bestbuy\" & filename

if fs.FileExists(path) then
	fs.DeleteFile(path)
end if

'Create the file and write some data to it.
set file = fs.CreateTextFile(path)

sql	=	"select	a.extOrderNumber, b.extItemID, isnull(a.trackingnum, a.orderid) trckingnum, b.quantity" & vbcrlf & _
		"	,	case when a.trackingnum is null or a.trackingnum = '' then 5 else 3 end trackingType" & vbcrlf & _
		"	,	convert(varchar(10), a.scandate, 101) scandate" & vbcrlf & _
		"from	we_orders a join we_orderdetails b" & vbcrlf & _
		"	on	a.orderid = b.orderid left outer join we_bestbuy_upload c" & vbcrlf & _
		"	on	b.orderid = c.orderid and b.orderdetailid = c.orderdetailid and c.uploadType = 'SHIPPING CONFIRMATION'" & vbcrlf & _
		"where	a.orderdatetime >= '" & SDate & "'" & vbcrlf & _
		"	and	a.orderdatetime < '" & EDate & "'" & vbcrlf & _
		"	and	a.store = 0" & vbcrlf & _
		"	and	scandate is not null" & vbcrlf & _
		"	and	a.approved = 1" & vbcrlf & _
		"	and	(a.cancelled is null or a.cancelled = 0)" & vbcrlf & _
		"	and	a.extOrderType = 9" & vbcrlf & _
		"	and	c.orderid is null	-- exclude already posted to their FTP" & vbcrlf & _
		"order by a.orderid, b.orderdetailid"

Set rs = CreateObject("ADODB.Recordset")
rs.Open sql, oConn, 0, 1
numItems = 0

if not rs.eof then
	file.writeline "receipt-id" & vbtab & "receipt-item-id" & vbtab & "quantity" & vbtab & "tracking-type" & vbtab & "tracking-number" & vbtab & "ship-date"
	do until rs.eof
		numItems = numItems + 1
		extOrderNumber = rs("extOrderNumber")
		extItemID = rs("extItemID")
		trckingnum = rs("trckingnum")
		quantity = rs("quantity")
		trackingType = rs("trackingType")
		shipDate = rs("scandate")
	
		strline = extOrderNumber & vbtab
		strline = strline & extItemID & vbtab
		strline = strline & quantity & vbtab
		strline = strline & trackingType & vbtab
		strline = strline & trckingnum & vbtab
		strline = strline & shipDate
		file.writeline strline
		
		rs.movenext
	loop
end if
file.close()

sql = 	"insert into we_bestbuy_upload(orderid, orderdetailid, uploadType)" & vbcrlf & _
		"select	a.orderid, b.orderdetailid, 'SHIPPING CONFIRMATION'" & vbcrlf & _
		"from	we_orders a join we_orderdetails b" & vbcrlf & _
		"	on	a.orderid = b.orderid left outer join we_bestbuy_upload c" & vbcrlf & _
		"	on	b.orderid = c.orderid and b.orderdetailid = c.orderdetailid and c.uploadType = 'SHIPPING CONFIRMATION'" & vbcrlf & _
		"where	a.orderdatetime >= '" & SDate & "'" & vbcrlf & _
		"	and	a.orderdatetime < '" & EDate & "'" & vbcrlf & _
		"	and	a.store = 0" & vbcrlf & _
		"	and	scandate is not null" & vbcrlf & _
		"	and	a.approved = 1" & vbcrlf & _
		"	and	(a.cancelled is null or a.cancelled = 0)" & vbcrlf & _
		"	and	a.extOrderType = 9" & vbcrlf & _
		"	and	c.orderid is null	-- exclude already posted to their FTP"
oConn.execute(sql)


'===================================================
''''''' FTP THE FILE TO BESTBUY
'===================================================
set ftp = CreateObject("Chilkat.Ftp2")
success = ftp.UnlockComponent("WIRELEFTP_GQ2TEoLE9Bnh")
if success <> 1 then 
'	response.write "ftp.UnlockComponent ERROR - " & ftp.LastErrorText
'	response.end
end if

ftp.Hostname = "ftp://trade.marketplace.buy.com"
ftp.Username = "kristen@wirelessemporium.com"
ftp.Password = "3mp0r1um"
'ftp.Passive = 1

success = ftp.Connect()
if success <> 1 then
'	response.write "ftp connection error - " & ftp.LastErrorText
'	response.end
end if

ftp.ChangeRemoteDir("Fulfillment")

success = ftp.PutFile(path, ftp.GetCurrentRemoteDir() & "/" & filename)
if success <> 1 then 
'	response.write "ftp connection error - " & ftp.LastErrorText
'	response.end		
end if


strTo = "terry@wirelessemporium.com,ruben@wirelessemporium.com,philippe@wirelessemporium.com"
'strTo = "terry@wirelessemporium.com"
strFrom = "sales@wirelessemporium.com"
strSubject = "*** Bestbuy trackingnumber upload result ***"
strBody = 	"- Number of line items uploaded: <b>" & numItems & "</b><br>" & vbcrlf & _
			"- File processed: " & filename

cdosend strTo, strFrom, strSubject, strBody

sub cdosend(strto,strfrom,strsubject,strbody)
	on error resume next
	dim objMail
	set objMail = createobject("cdo.message")
	with objMail
		.from = strfrom
		.to = strto
		.subject = strsubject
		.htmlbody = cstr("" & strbody)
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.configuration.fields.item("urn:schemas:httpmail:sender") = "sales@wirelessemporium.com"
		.configuration.fields.update
		.send
	end with
	set objMail = nothing
	on error goto 0
end sub

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
