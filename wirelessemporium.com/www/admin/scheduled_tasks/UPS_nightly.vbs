set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim SQL, RS, dateProcessed, strToDelete
strToDelete = ""
SQL = "SELECT * FROM UPS_tracking ORDER BY id"
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
do until RS.eof
	if RS("orderid") <> "" and not isNull(RS("orderid")) and len(RS("orderid")) = 6 and isNumeric(RS("orderid")) then
		SQL = "UPDATE we_orders SET"
		if RS("void") = "Y" then
			SQL = SQL & " trackingNum = '',"
			SQL = SQL & " UPScost = null,"
			SQL = SQL & " confirmSent = null,"
			SQL = SQL & " confirmdatetime = null,"
			SQL = SQL & " thub_posted_date = null"
		else
			dateProcessed = RS("dateProcessed")
			dateProcessed = mid(dateProcessed,5,2) & "/" & mid(dateProcessed,7,2) & "/" & left(dateProcessed,4)
			SQL = SQL & " trackingNum = '" & RS("trackingNumber") & "',"
			SQL = SQL & " UPScost = '" & RS("actualShippingCost") & "',"
			SQL = SQL & " confirmSent = 'yes',"
			SQL = SQL & " confirmdatetime = '" & dateProcessed & "',"
			SQL = SQL & " thub_posted_date = '" & RS("DateTimeEntd") & "'"
		end if
		SQL = SQL & " WHERE orderid = '" & RS("orderid") & "'"
		strToDelete = strToDelete & "'" & RS("orderid") & "',"
		oConn.execute SQL
	end if
	RS.movenext
loop

if strToDelete <> "" then
	SQL = "DELETE FROM UPS_tracking WHERE orderid IN (" & left(strToDelete,len(strToDelete)-1) & ")"
	oConn.execute SQL
end if

RS.close
set RS = nothing
call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub