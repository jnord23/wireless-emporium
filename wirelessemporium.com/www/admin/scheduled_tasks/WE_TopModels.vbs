set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

'myArray = array(2,4,5,7,9,14,16,20)
myArray = array(2,3,4,5,6,7,9,10,14,16,20)
for b = 0 to 3
	if b <> 1 then
		SQL = "DELETE FROM temp_TopModels WHERE store = " & b
		oConn.execute SQL
		
		for c = 0 to uBound(myArray)
			SQL = "SELECT SUM(b.quantity) AS SumQty, c.modelID, d.modelName, c.brandID, e.brandName"
			SQL = SQL & " FROM we_orders a"
			SQL = SQL & " INNER JOIN we_orderdetails b ON a.orderid = b.orderid"
			SQL = SQL & " INNER JOIN we_Items c ON b.itemid = c.itemID"
			SQL = SQL & " INNER JOIN we_Models d ON c.modelID = d.modelID"
			SQL = SQL & " INNER JOIN we_Brands e ON c.BrandID = e.BrandID"
			SQL = SQL & " WHERE a.orderdatetime >= '" & dateAdd("d",-30,date) & "' AND a.orderdatetime <= '" & dateAdd("d",1,date) & "'"
			SQL = SQL & " AND a.approved = 1 AND (a.cancelled IS NULL OR a.cancelled = 0) AND c.modelID > 1"
			SQL = SQL & " AND a.store = " & b
			SQL = SQL & " AND c.brandID = '" & myArray(c) & "'"
			SQL = SQL & " AND d.modelName NOT LIKE '%2.5mm%'"
			SQL = SQL & " GROUP BY c.modelID, d.modelName, c.brandID, e.brandName"
			SQL = SQL & " ORDER BY SumQty DESC"
			'msgbox "<p>" & SQL & "</p>" & vbcrlf
			set RS = CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 0, 1
			a = 0
			do until RS.eof
				SQL = "INSERT INTO temp_TopModels (store, brandID, brandName, modelID, modelName, SumQty) VALUES ("
				SQL = SQL & b & ","
				SQL = SQL & "'" & myArray(c) & "',"
				SQL = SQL & "'" & RS("brandName") & "',"
				SQL = SQL & "'" & RS("modelID") & "',"
				SQL = SQL & "'" & RS("modelName") & "',"
				SQL = SQL & "'" & RS("SumQty") & "')"
				oConn.execute SQL
				RS.movenext
				a = a + 1
				if a = 5 then
					exit do
				end if
			loop
		next
	end if
next

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub