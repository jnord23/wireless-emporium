set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

SQL = "DELETE FROM we_ccinfo WHERE orderid IN"
SQL = SQL & " (SELECT A.orderid FROM WE_ccinfo A INNER JOIN WE_orders B ON A.orderid=B.orderid"
SQL = SQL & " WHERE B.orderdatetime < '" & dateAdd("h",-25,now) & "')"
oConn.execute SQL

'SQL = "DELETE FROM we_orderdetails WHERE itemid = 0 OR quantity = 0 OR orderid = 0"
SQL = "DELETE FROM we_orderdetails WHERE itemid = 0 OR orderid = 0"
oConn.execute SQL

SQL = "DELETE FROM ShoppingCart WHERE dateEntd < '" & dateAdd("d",-90,now) & "'"
oConn.execute SQL

SQL = "DELETE FROM ShoppingCart WHERE (purchasedOrderID IS NULL OR purchasedOrderID = 0) AND dateEntd < '" & dateAdd("d",-10,now) & "'"
oConn.execute SQL

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
