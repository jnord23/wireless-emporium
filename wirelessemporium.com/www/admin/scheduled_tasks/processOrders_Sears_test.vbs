set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

SDate = dateadd("d", -10, date)
EDate = dateadd("d", -1, date)
strSDate = year(SDate) & "-" & right("00" & month(SDate),2) & "-" & right("00" & day(SDate),2)
strEDate = year(EDate) & "-" & right("00" & month(EDate),2) & "-" & right("00" & day(EDate),2)
'strSDate = "2011-11-06"
'strEDate = "2011-11-06"

Set xmlHttp = CreateObject("Microsoft.XMLHTTP")
myURL = "https://seller.marketplace.sears.com/SellerPortal/api/oms/purchaseorder/v3?email=kristen@wirelessemporium.com&password=cutler1986&fromdate=" & strSDate & "&todate=" & strEDate
'session("errorSQL") = myURL
xmlHttp.open "GET", myURL, False
xmlHttp.send

set xmlDoc = CreateObject("MSXML2.DOMDocument")
xmlDoc.async = false
xmlDoc.loadXML(xmlHttp.responseText)

set mynodes = xmlDoc.documentElement.selectNodes("purchase-order")
for each Node in mynodes
	nRowItem = -1
	redim arrItemDetail(1,-1)
	fname 			= ""
	lname 			= ""
	shipType		= ""
	poStatus		= ""
	orderSubTotal	= 0.0
	orderCommission = 0.0
	orderSalesTax	= 0.0
	orderGrandTotal = 0.0
	extOrderNumber 	= Node.childNodes.item(0).text
	email			= Node.childNodes.item(1).text
	poNumber		= Node.childNodes.item(2).text	
	shipName		= (Node.childNodes.item(11)).childNodes.item(0).text
	address			= (Node.childNodes.item(11)).childNodes.item(1).text
	city			= (Node.childNodes.item(11)).childNodes.item(2).text
	strState		= (Node.childNodes.item(11)).childNodes.item(3).text
	zipcode			= (Node.childNodes.item(11)).childNodes.item(4).text
	phone			= (Node.childNodes.item(11)).childNodes.item(5).text

	call getFirstLastName(shipName, fname, lname)
	
	if inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",strState) > 0 then
		shipType = "First Class Int'l"	
	else
		shipType = "First Class"
	end if
	
	for i = 0 to Node.ChildNodes.Length - 1
		if "po-line" = Node.childnodes.item(i).nodeName then
			'po-line start
			set subNodes = Node.childnodes.item(i).childNodes
			nRowItem = nRowItem + 1
			redim preserve arrItemDetail(1,nRowItem)
			for each subNode in subNodes
				set subNodes2 = subNode.childNodes
				for each subNode2 in subNodes2
					select case subNode2.nodeName
						case "item-id" : arrItemDetail(0,nRowItem) = replace(subNode2.text, "WE-", "")
						case "order-quantity" : arrItemDetail(1,nRowItem) = cint(subNode2.text)
					end select
				next
			next
		elseif "order-total-sell-price" = Node.childnodes.item(i).nodeName then
			orderSubTotal	= Node.childnodes.item(i).text
			orderSubTotal	= cdbl(orderSubTotal)
		elseif "total-commision" = Node.childnodes.item(i).nodeName then
			orderCommission = Node.childnodes.item(i).text
			orderCommission = cdbl(orderCommission)
		elseif "sales-tax" = Node.childnodes.item(i).nodeName then
			orderSalesTax	= Node.childnodes.item(i).text
			orderSalesTax	= cdbl(orderSalesTax)
		elseif "po-status" = Node.childnodes.item(i).nodeName then
			poStatus = Node.childnodes.item(i).text
		end if
	next

	if poStatus = "New" then
		sql = "select top 1 orderid from _we_orders where extordernumber = '" & extOrderNumber & "'"		
		set rsExists = createobject("ADODB.recordset")
		rsExists.open sql, oConn, 0, 1
		
		if rsExists.eof then
			orderGrandTotal = orderSubTotal + orderSalesTax
	
			SQL = "sp_InsertAccountInfo_test '" & SQLquote(fname) & "','" & SQLquote(lname) & "','" & SQLquote(address) & "','','" & SQLquote(city) & "','" & SQLquote(strState) & "','" & zipcode & "','','" & SQLquote(address) & "','','" & SQLquote(city) & "','" & SQLquote(strState) & "','" & zipcode & "','" & sSCountry & "','" & SQLquote(phone) & "','" & SQLquote(email) & "','','','" & now & "'"
			nAccountId = oConn.execute(SQL).fields(0).value	
			
			sql = "sp_InsertOrder3_test 0, '" & nAccountId & "','" & FormatNumber(orderSubTotal,2) & "','0.0'," & FormatNumber(orderSalesTax,2) & ",'" & FormatNumber(orderGrandTotal,2) & "','" & SQLquote(shipType) & "',null,0,'" & now & "'"
			nOrderID = oConn.execute(SQL).fields(0).value
		
			sql = "UPDATE _we_orders SET approved = 1, extOrderType='8', extOrderNumber='" & extOrderNumber & "' WHERE orderid='" & nOrderID & "'"
			oConn.execute SQL
			
			for nRow = 0 to ubound(arrItemDetail,2)
				nItemID = arrItemDetail(0,nRow)
	
				nQty = arrItemDetail(1,nRow)
				KIT = null
				SQL = "select partnumber, itemkit_new from we_items where itemid = '" & nItemID & "'"		
				set rsPart = createobject("ADODB.recordset")
				rsPart.open SQL, oConn, 0, 1
				if not rsPart.eof then
					if not isNull(rsPart("itemkit_new")) then KIT = rsPart("itemkit_new")
					SKU = rsPart("partnumber")
				end if
				rsPart.close
				set rsPart = nothing
				
				SQL = "sp_InsertOrderDetail_test '" & nOrderID & "','" & nItemID & "','" & nQty & "'"
				oConn.execute(SQL)		
				
				call UpdateInvQty(SKU, nQty, KIT)
				call NumberOfSales(nItemID, nQty, KIT)
			next
		end if
	end if
next

sub getFirstLastName(byval fullname, byref firstName, byref lastName)
	firstName = fullname
	lastName = ""
	if fullname <> "" then
		arrName = split(fullname, " ")
		if isArray(arrName) then
			firstName = arrName(lbound(arrName))
			lastName = arrName(ubound(arrName))
		end if
	end if
end sub

sub UpdateInvQty(nPartNumber, nProdQuantity, KIT)
	' Updates inv_qty for the Master Part (item with the same Part Number as the item purchased which does not have -1 inv_qty)
	' If the inv_qty would be adjusted to less than or equal to zero, then all items with the same Part Number as the item purchased are set to zero
	if isNull(KIT) then
		SQL = "SELECT itemID,typeID,vendor,inv_qty FROM we_items WHERE PartNumber = '" & nPartNumber & "' AND master = 1"
	else
		SQL = "SELECT (select itemID from we_items where partNumber = a.partNumber and master = 1) as itemID,typeID,vendor,inv_qty FROM we_items a WHERE itemID IN (" & KIT & ")"
	end if
	set oRsItemInfo2 = CreateObject("ADODB.Recordset")
	oRsItemInfo2.open SQL, oConn, 0, 1
	do until oRsItemInfo2.eof
		curQty = oRsItemInfo2("inv_qty")
		if curQty - nProdQuantity > 0 then
			On Error Resume Next
			sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & oRsItemInfo2("itemID") & "," & curQty & "," & nProdQuantity & "," & nOrderID & ",0,'Sears Customer Order','" & now & "')"
'			oConn.execute(sql)
			On Error Goto 0
			
			decreaseSQL = "UPDATE we_items SET inv_qty = inv_qty - " & nProdQuantity & " WHERE itemID = '" & oRsItemInfo2("itemID") & "'"
'			oConn.execute(decreaseSQL)
		else
			On Error Resume Next
			sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & oRsItemInfo2("itemID") & "," & curQty & "," & nProdQuantity & "," & nOrderID & ",0,'Sears Customer Order *Out of Stock*','" & now & "')"
'			oConn.execute(sql)
			On Error Goto 0
			
			decreaseSQL = "UPDATE we_items SET inv_qty = 0 WHERE PartNumber = '" & nPartNumber & "'"
			if oRsItemInfo2("typeID") = 2 then decreaseSQL = decreaseSQL & " AND inv_qty > -1"
			if oRsItemInfo2("typeID") <> 3 and oRsItemInfo2("vendor") <> "CM" and oRsItemInfo2("vendor") <> "DS" and oRsItemInfo2("vendor") <> "MLD" then
				' Send zero-inventory e-mail
				outOfStockEmail nPartNumber,curQty,nProdQuantity
			end if
'			oConn.execute(decreaseSQL)
		end if
		oRsItemInfo2.movenext
	loop
	oRsItemInfo2.close
	set oRsItemInfo2 = nothing
end sub

sub NumberOfSales(nIdProd, QTY, KIT)
	if isNull(KIT) then
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID = '" & nIdProd & "'"
	else
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID IN (" & KIT & ")"
	end if
'	oConn.execute(SQL)
end sub

sub cdosend(strto,strfrom,strsubject,strbody)
	on error resume next
	dim objMail
	set objMail = createobject("cdo.message")
	with objMail
		.from = strfrom
		.to = strto
		.subject = strsubject
		.htmlbody = cstr("" & strbody)
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.configuration.fields.item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.configuration.fields.item("urn:schemas:httpmail:sender") = "sales@wirelessemporium.com"
		.configuration.fields.update
		.send
	end with
	set objMail = nothing
	on error goto 0
end sub

function SQLquote(strValue)
	if not isNull(strValue) and strValue <> "" then
		SQLquote = trim(replace(strValue,"'","''"))
		SQLquote = trim(replace(SQLquote,chr(34),"''"))
	else
		SQLquote = strValue
	end if
end function

function outOfStockEmail(nPartNumber,curQty,nProdQuantity)	
	strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\admin\scheduled_tasks\outOfStock.html")
	strTemplate = replace(strTemplate,"#nPartNumber#",nPartNumber)
	strTemplate = replace(strTemplate,"#curDate#",now)
	strTemplate = replace(strTemplate,"#curQty#",curQty)
	strTemplate = replace(strTemplate,"#orderQty#",nProdQuantity)
	
	sql = "select top 10 (select top 1 itemPic from we_Items where partNumber = '" & nPartNumber & "') as itemPic, vendorCode, vendorPN, orderAmt, receivedAmt, complete, processOrderDate, completeOrderDate from we_vendorOrder where partNumber = '" & nPartNumber & "' and process = 1 order by id desc"
	set rs = oConn.execute(sql)
	
	if rs.EOF then
		sql = "select top 1 itemPic from we_Items where partNumber = '" & nPartNumber & "'"
		set rs = oConn.execute(sql)
		
		strTemplate = replace(strTemplate,"#prodPic#",rs("itemPic"))
		if not rs.eof then 
			rs.movenext
		end if
	else
		strTemplate = replace(strTemplate,"#prodPic#",rs("itemPic"))	
	end if
	
	purchDetails = "<table border=1 bordercolor=#000000 cellpadding=3 cellspacing=0><tr bgColor='#333333' style='color:#fff; font-weight:bold;'><td>Vendor</td><td>OrderAmt</td><td>ReceivedAmt</td><td>PurchaseDate</td></tr>"
	bgColor = "#ffffff"
	purchLap = 0
	do while not rs.EOF
		if not isnull(rs("vendorCode")) then
			purchLap = purchLap + 1
			vendorCode = rs("vendorCode")
			orderAmt = rs("orderAmt")
			receivedAmt = rs("receivedAmt")
			orderComplete = rs("complete")
			if not orderComplete then receivedAmt = "Pending"
			purchaseDate = rs("processOrderDate")
			purchDetails = purchDetails & "<tr bgColor='" & bgColor & "'><td>" & vendorCode & "</td><td align='right'>" & orderAmt & "</td><td align='right'>" & receivedAmt & "</td><td align='right'>" & purchaseDate & "</td></tr>"
			if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
		end if
		rs.movenext
	loop
	if purchLap = 0 then purchDetails = purchDetails & "<tr><td colspan='4' align='center' style='font-weight:bold;'>No Purchase Data Found</td></tr>"
	purchDetails = purchDetails & "</table>"
	strTemplate = replace(strTemplate,"#purchHistory#",purchDetails)
	
	sql = "select top 30 sum(quantity) as totalSales, CONVERT(varchar(11), a.entrydate, 101) as saleDate from we_orderDetails a left join we_Items b on a.itemid = b.itemID left join we_orders c on a.orderid = c.orderID where b.PartNumber = '" & nPartNumber & "' and c.approved = 1 and c.cancelled is null and c.parentOrderID is null and CONVERT(varchar(11), a.entrydate, 101) > '" & date-30 & "' group by CONVERT(varchar(11), a.entrydate, 101) order by CONVERT(varchar(11), a.entrydate, 101)"
	set rs = oConn.execute(sql)
	
	salesDetails = "<table border=1 bordercolor=#000000 cellpadding=0 cellspacing=0><tr bgColor='#333333'><td align='center' style='font-weight:bold; color:#fff; padding:10px;'>Last 30 Days of Sales</td></tr><tr><td valign='bottom'><div style='float:left; vertical-align:bottom;'>"
	salesLap = 0
	curDate = date - 29
	do while not rs.EOF
		salesLap = salesLap + 1
		totalSales = rs("totalSales")
		saleDate = rs("saleDate")
		
		if cdate(saleDate) <> cdate(curDate) then
			salesDetails = salesDetails & "<div style='float:left; height:11px; width:15px; padding-left:5px;'><div style='width:15px; height:10px; text-align:center;'>0</div><div style='background-color:#00F; height:1px; width:15px;' title='" & curDate & "'></div></div>"
		else
			salesDetails = salesDetails & "<div style='float:left; height:" & (totalSales*10)+10 & "px; width:15px; padding-left:5px; vertical-align:bottom;'><div style='width:15px; height:10px; text-align:center;'>" & totalSales & "</div><div style='background-color:#00F; height:" & (totalSales*10) & "px; width:15px;' title='" & saleDate & "'></div></div>"
			rs.movenext
		end if
		curDate = curDate + 1
	loop
	if salesLap = 0 then salesDetails = salesDetails & "<div style='font-weight:bold; width:200px; text-align:center;'>No Sales Data Found</div>"
	salesDetails = salesDetails & "</div></td></tr></table>"
	strTemplate = replace(strTemplate,"#salesHistory#",salesDetails)
	
	cdo_from = "warehouseAI@wirelessemporium.com"
	cdo_subject = nPartNumber & " is out of stock! (Sears)"
	cdo_body = strTemplate
'	cdo_to = "tony@wirelessemporium.com,bob@wirelessemporium.com,charles@wirelessemporium.com,steven@wirelessemporium.com,armando@wirelessemporium.com,jon@wirelessemporium.com,edwin@wirelessemporium.com"
	cdo_to = "terry@wirelessemporium.com"
	CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
end function

function readTextFile(fileName)
	Dim strContents	: strContents = ""

	if fileName <> "" then
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile = strContents
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub
