set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

Set xmlDoc = CreateObject("Msxml2.DOMDocument")
set fs = CreateObject("Scripting.FileSystemObject")

dim arrFolder(0)
arrFolder(0) = "c:\inetpub\wwwroot\wirelessemporium.com\www\blog\app_data\posts"

'each site
for i=0 to ubound(arrFolder)
	folderPath = arrFolder(i)
	set folder = fs.GetFolder(folderPath)
	set files = folder.Files

	dateSearchFrom = cdate(date-7)
	sql = "select count(*) cnt from xblogposts where site_id = 0"
	set rs = oConn.execute(sql)
	if rs("cnt") = 0 then dateSearchFrom = cdate(date-365)

	'each file
	for each oFile in files
		if oFile.DateLastModified > dateSearchFrom then
			fileName = oFile.Name
			fullFilePath = folderPath & "\" & fileName
			
			xmlDoc.load(fullFilePath)
			Set objNodeList = xmlDoc.getElementsByTagName("post")
			
			'each post within the file - usually there is only 1 post.
			for each oNode in objNodeList
				author = ""
				title = ""
				strDescription = ""
				content = ""
				ispublished = ""
				isdeleted = ""
				iscommentsenabled = ""
				pubDate = ""
				lastModified = ""
				raters = ""
				rating = ""
				slug = ""
				
				for nCnt = 0 to oNode.ChildNodes.Length - 1
					strNodeName = lcase(oNode.childnodes.item(nCnt).nodeName)
					strNodeText = oNode.childnodes.item(nCnt).text
					select case strNodeName
						case "author"				:	author = strNodeText
						case "title"				:	title = strNodeText
						case "content"				:	content = strNodeText
						case "description"			:	strDescription = strNodeText
						case "ispublished"			:	ispublished = strNodeText
						case "isdeleted"			:	isdeleted = strNodeText
						case "iscommentsenabled"	:	iscommentsenabled = strNodeText
						case "pubdate"				:	pubDate = strNodeText
						case "lastmodified"			:	lastModified = strNodeText
						case "raters"				:	raters = strNodeText
						case "rating"				:	rating = strNodeText
						case "slug"					:	slug = strNodeText
					end select
				next
				
				sql	=	"declare @datemodified smalldatetime" & vbcrlf & _
						"select @datemodified = lastModified from xblogposts where xmlFileName = '" & fileName & "'" & vbcrlf & _
						"if @datemodified is null" & vbcrlf & _
						"begin" & vbcrlf & _
						"	insert into xblogposts(	xmlFileName, site_id, author, title, description, content, ispublished" & vbcrlf & _
						"							,	isdeleted, iscommentsenabled, pubDate" & vbcrlf & _
						"							,	lastModified, raters, rating, slug)" & vbcrlf & _
						"	values('" & SQLquote(fileName) & "','0','" & SQLquote(author) & "','" & SQLquote(title) & "','" & SQLquote(strDescription) & "','" & SQLquote(content) & "','" & SQLquote(ispublished) & "'" & vbcrlf & _
						"		,	'" & SQLquote(isdeleted) & "','" & SQLquote(iscommentsenabled) & "','" & SQLquote(pubDate) & "'" & vbcrlf & _
						"		,	'" & SQLquote(lastModified) & "','" & SQLquote(raters) & "','" & SQLquote(rating) & "','" & SQLquote(slug) & "')" & vbcrlf & _
						"end" & vbcrlf & _
						"else" & vbcrlf & _
						"begin" & vbcrlf & _
						"	if '" & lastModified & "' > @datemodified" & vbcrlf & _
						"	begin" & vbcrlf & _
						"		update	xblogposts" & vbcrlf & _
						"		set		author = '" & SQLquote(author) & "'" & vbcrlf & _
						"			,	title = '" & SQLquote(title) & "'" & vbcrlf & _
						"			,	description = '" & SQLquote(strDescription) & "'" & vbcrlf & _
						"			,	content = '" & SQLquote(content) & "'" & vbcrlf & _
						"			,	ispublished = '" & SQLquote(ispublished) & "'" & vbcrlf & _
						"			,	isdeleted = '" & SQLquote(isdeleted) & "'" & vbcrlf & _
						"			,	iscommentsenabled = '" & SQLquote(iscommentsenabled) & "'" & vbcrlf & _
						"			,	pubDate = '" & SQLquote(pubDate) & "'" & vbcrlf & _
						"			,	lastModified = '" & SQLquote(lastModified) & "'" & vbcrlf & _
						"			,	raters = '" & SQLquote(raters) & "'" & vbcrlf & _
						"			,	rating = '" & SQLquote(rating) & "'" & vbcrlf & _
						"			,	slug = '" & SQLquote(slug) & "'" & vbcrlf & _
						"		where	xmlFileName = '" & fileName & "'" & vbcrlf & _
						"	end" & vbcrlf & _
						"end"
'				response.write "<pre>" & sql & "</pre>"
'				session("errorSQL") = fileName
				oConn.execute(sql)
			next
		end if
	next
next

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub

function SQLquote(strValue)
	strContent = trim(strValue)
	if not isNull(strContent) and strContent <> "" then
		strContent = trim(replace(strContent,chr(34),"&quot;"))
		strContent = replace(strContent,"%26%2343%3B1","+")
		strContent = replace(strContent,"&#43;","+")
		strContent = replace(strContent,Chr(39),"''")
		strContent = replace(strContent,"--","")
		strContent = replace(strContent,"/*","")
		strContent = replace(strContent,"*/","")
		strContent = replace(strContent,"xp_","")
		strContent = trim(replace(strContent,chr(146),"''"))
		strContent = trim(replace(strContent,chr(147),"&quot;"))
		strContent = trim(replace(strContent,chr(148),"&quot;"))
	else
		strContent = strValue
	end if
	SQLquote = strContent
end function
