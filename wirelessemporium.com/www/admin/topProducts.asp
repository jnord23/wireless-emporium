<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Top Products"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	dim brandID : brandID = prepInt(ds(request.Form("brandID")))
	dim brandName : brandName = prepStr(ds(request.Form("brandName")))
	dim topModel_1 : topModel_1 = prepInt(ds(request.Form("topModel_1")))
	dim topModel_2 : topModel_2 = prepInt(ds(request.Form("topModel_2")))
	dim topModel_3 : topModel_3 = prepInt(ds(request.Form("topModel_3")))
	dim topModel_4 : topModel_4 = prepInt(ds(request.Form("topModel_4")))
	dim topModel_5 : topModel_5 = prepInt(ds(request.Form("topModel_5")))
	dim topModel_6 : topModel_6 = prepInt(ds(request.Form("topModel_6")))
	dim topModel_7 : topModel_7 = prepInt(ds(request.Form("topModel_7")))
	dim topModel_8 : topModel_8 = prepInt(ds(request.Form("topModel_8")))
	dim topModel_9 : topModel_9 = prepInt(ds(request.Form("topModel_9")))
	dim topModel_10 : topModel_10 = prepInt(ds(request.Form("topModel_10")))
	dim popItemImgPath
	
	if brandID > 0 then
		set fso = CreateObject("Scripting.FileSystemObject")
		Set Img = Server.CreateObject("Persits.Jpeg")
		set Jpeg = Server.CreateObject("Persits.Jpeg")
		Jpeg.Quality = 60
		Jpeg.Interpolation = 1
		
		sql = "update we_models set topModel = 0 where brandID = " & brandID
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "update we_models set topModel = 1 where modelID = " & topModel_1 & " or modelID = " & topModel_2 & " or modelID = " & topModel_3 & " or modelID = " & topModel_4 & " or modelID = " & topModel_5 & " or modelID = " & topModel_6 & " or modelID = " & topModel_7 & " or modelID = " & topModel_8 & " or modelID = " & topModel_9 & " or modelID = " & topModel_10
		session("errorSQL") = sql
		oConn.execute(sql)
		
		if fso.FileExists(Server.MapPath("/images/stitch/brands/" & formatSEO(brandName) & "_pop.jpg")) then
			fso.deleteFile(Server.MapPath("/images/stitch/brands/" & formatSEO(brandName) & "_pop.jpg"))
			response.Write("delete pop stitch<br>")
		end if
		if fso.FileExists(Server.MapPath("/images/stitch/brands/" & formatSEO(brandName) & "_all_AZ.jpg")) then
			fso.deleteFile(Server.MapPath("/images/stitch/brands/" & formatSEO(brandName) & "_all_AZ.jpg"))
			response.Write("delete standard stitch<br>")
		end if
		if fso.FileExists(Server.MapPath("/images/stitch/brands/" & formatSEO(brandName) & "_old_AZ.jpg")) then
			fso.deleteFile(Server.MapPath("/images/stitch/brands/" & formatSEO(brandName) & "_old_AZ.jpg"))
			response.Write("delete old model stitch<br>")
		end if
		if fso.FileExists(Server.MapPath("/images/stitch/brands/" & formatSEO(brandName) & "_int_AZ.jpg")) then
			fso.deleteFile(Server.MapPath("/images/stitch/brands/" & formatSEO(brandName) & "_int_AZ.jpg"))
			response.Write("delete international stitch<br>")
		end if
		response.Write("New Pop Model Update Complete!<br>")
	end if
	
	sql = "select brandID, brandName from we_brands with (nolock) order by brandName"
	session("errorSQL") = sql
	Set brandRS = Server.CreateObject("ADODB.Recordset")
	brandRS.open sql, oConn, 0, 1
%>
<table border="0" cellpadding="3" cellspacing="0" align="center" width="650" style="font-size:14px;">
	<tr><td align="center"><h1>Adjust Top Products</h1></td></tr>
    <tr>
    	<td>
        	<div style="float:left; padding:0px 10px 0px 160px; font-weight:bold;">Select Brand To Update:</div>
            <div style="float:left;">
            	<select name="brandID" onchange="getModels(this.value)">
                	<option value="">Select Brand</option>
                    <%
					do while not brandRS.EOF
					%>
                    <option value="<%=brandRS("brandID")%>"><%=brandRS("brandName")%></option>
                    <%
						brandRS.movenext
					loop
					brandRS = null
					%>
                </select>
            </div>
        </td>
    </tr>
    <tr>
    	<td id="modelDisplayBox" align="center"></td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function getModels(brandID) {
		ajax('/ajax/admin/ajaxTopProducts.asp?brandID=' + brandID,'modelDisplayBox')
	}
	function updateModel(boxID,modelID) {
		document.getElementById("testZone").innerHTML = '/ajax/admin/ajaxTopProducts.asp?modelID=' + modelID + ',modelBox_' + boxID
		ajax('/ajax/admin/ajaxTopProducts.asp?modelID=' + modelID + '&boxID=' + boxID,'modelBox_' + boxID)
	}
</script>