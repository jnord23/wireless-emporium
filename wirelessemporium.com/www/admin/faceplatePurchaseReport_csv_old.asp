<%
	response.buffer = true
	response.expires = -1
	response.ExpiresAbsolute = Now() - 1
	response.CacheControl = "no-cache"

	pageTitle = "Product Purchase Report - Create CSV"
	Server.ScriptTimeout = 9000
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%		
	sql = session("csvSQL")
	session("errorSQL") = SQL
	set rs = oConn.execute(sql)
	
	if not RS.eof then
		' Save as CSV:
		dim fs, file, filename, path
		set fs = CreateObject("Scripting.FileSystemObject")
		if fs.fileExists(Server.MapPath("/admin/tempCSV") & "\ppr_" & adminID & "_*") then
			fs.deleteFile(Server.MapPath("/admin/tempCSV") & "\ppr_" & adminID & "_*")
		end if
		filename = "ppr_" & adminID & "_" & useTime & ".csv"
		path = Server.MapPath("/admin/tempCSV") & "\" & filename
		set file = fs.CreateTextFile(path, true)
		
		dim strToWrite, HoldPartNumber, WEsales, CAsales, COsales, PSsales
		strToWrite = ""
		
		strToWrite = strToWrite & chr(34) & "itemDesc" & chr(34) & ","
		strToWrite = strToWrite & chr(34) & "PartNumber" & chr(34) & ","
		strToWrite = strToWrite & chr(34) & "vendor" & chr(34) & ","
		strToWrite = strToWrite & chr(34) & "inv_qty" & chr(34) & ","
		strToWrite = strToWrite & chr(34) & "TOTAL Sales " & chr(34)
		file.WriteLine strToWrite
		
		laps = 0
		do while not rs.EOF			
			laps = laps + 1
			strToWrite = ""
			
			curPartNumber = rs("partNumber")
			curItemDesc = RS("itemDesc")
			curVendor = RS("vendor")
			curInvQty = prepInt(RS("inv_qty"))
			TotalSales = prepInt(rs("sales"))
			
			strToWrite = strToWrite & chr(34) & curItemDesc & chr(34) & ","
			strToWrite = strToWrite & chr(34) & curPartNumber & chr(34) & ","
			strToWrite = strToWrite & chr(34) & curVendor & chr(34) & ","
			strToWrite = strToWrite & chr(34) & curInvQty & chr(34) & ","
			strToWrite = strToWrite & chr(34) & TotalSales & chr(34)
			
			file.WriteLine strToWrite
			do while curPartNumber = rs("partNumber")
				rs.movenext
				if rs.EOF then exit do
			loop
			if laps = 100000 then exit do
		loop
		
		file.close
		set file = nothing
		set fs = nothing
		response.write "<p class=""boldText"">Your CSV file is <a href=""/admin/tempCSV/" & filename & """>here</a>.</p>"
	else
		response.write "<p class=""boldText"">No records matched.</p><p class=""boldText"">No report generated.</p>"
	end if
	RS.close
	set RS = nothing
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
