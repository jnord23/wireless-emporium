<%
	thisUser = Request.Cookies("username")
	pageTitle = ""
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	if prepVal(request.Form("reportDate")) <> "" then
		useDate = prepVal(request.Form("reportDate"))
	else
		useDate = date - 1
	end if
	
	sql	=	"exec sp_scanConfirmReport '" & useDate & "'" & vbcrlf
			
	set objRsResult = oConn.execute(sql)
	
	strEmail	=	strEmail	&	"	<table width=""550"" border=""1"" align=""center"" cellpadding=""5"" cellspacing=""0"" style=""border-collapse:collapse;font-family: Arial, Helvetica, sans-serif; font-size:12px;"">" & vbcrlf
	strEmail	=	strEmail	&	"		<tr>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""50"" rowspan=""2"" align=""center""><b>STORE</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""100"" rowspan=""2""><b>DATE SCANNED</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""150"" rowspan=""2""><b>SHIP TYPE</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""200"" colspan=""3"" align=""center""><b>Number of Orders</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"		</tr>" & vbcrlf
	strEmail	=	strEmail	&	"		<tr>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""60""><b>Scanned</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""60""><b>Confirm Sent</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"			<td bgcolor=""#ccc000"" width=""80""><b>Status</b></td>" & vbcrlf
	strEmail	=	strEmail	&	"		</tr>" & vbcrlf
	
	if not objRsResult.eof then
		do until objRsResult.eof
			numScan = cdbl(objRsResult("scanCnt"))
			numSent = cdbl(objRsResult("confirmSent"))
			
			if isnumeric(numScan) then
				if numScan = 0 then
					processRatio = 0.0
				else
					processRatio = (1.0 * (numSent) / numScan) * 100.0
				end if
			end if
			
			strEmail	=	strEmail	&	"		<tr>" & vbcrlf
			strEmail	=	strEmail	&	"			<td>" & objRsResult("shortdesc") & "</td>" & vbcrlf
			strEmail	=	strEmail	&	"			<td>" & objRsResult("scandate") & "</td>" & vbcrlf
			strEmail	=	strEmail	&	"			<td>" & objRsResult("shiptype") & "</td>" & vbcrlf
			strEmail	=	strEmail	&	"			<td align=""right"">" & formatnumber(numScan, 0) & "</td>" & vbcrlf
			strEmail	=	strEmail	&	"			<td align=""right"">" & formatnumber(numSent, 0) & "</td>" & vbcrlf
			strEmail	=	strEmail	&	"			<td>" & vbcrlf
			strEmail	=	strEmail	&	"				<table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""font-family: Arial, Helvetica, sans-serif; font-size:12px;"">" & vbcrlf
			strEmail	=	strEmail	&	"					<tr>" & vbcrlf
			strEmail	=	strEmail	&	"						<td align=""left"">" & formatnumber(processRatio, 2) & "%</td>" & vbcrlf
			strEmail	=	strEmail	&	"						<td align=""right"">" & vbcrlf
	
			if processRatio = 100 then
				strEmail	=	strEmail	&	"						<img src=""http://www.wirelessemporium.com/images/check.jpg"" border=""0"" width=""15""/>" & vbcrlf
			else
				strEmail	=	strEmail	&	"						<img src=""http://www.wirelessemporium.com/images/x.jpg"" border=""0"" width=""15""/>" & vbcrlf			
			end if
	
			strEmail	=	strEmail	&	"						</td>" & vbcrlf
			strEmail	=	strEmail	&	"					</tr>" & vbcrlf
			strEmail	=	strEmail	&	"				</table>" & vbcrlf
			strEmail	=	strEmail	&	"			</td>" & vbcrlf
			strEmail	=	strEmail	&	"		</tr>" & vbcrlf
	
			objRsResult.movenext
		loop
	else
		strEmail	=	strEmail	&	"		<tr>" & vbcrlf
		strEmail	=	strEmail	&	"			<td colspan=""6"">No orders found to send out shipment confirmation email today..</td>" & vbcrlf
		strEmail	=	strEmail	&	"		</tr>" & vbcrlf
	end if

	strEmail	=	strEmail	&	"	</table>" & vbcrlf
%>
<form method="post">
<div style="margin:20px auto; display:table;">
	<div class="fl" style="font-size:14px; font-weight:bold; padding-top:2px;">Report Date:</div>
    <div class="fl" style="margin-left:10px;"><input type="text" name="reportDate" value="<%=date-1%>" style="width:65px;" onchange="this.form.submit();" /></div>
</div>
</form>
<div style="margin:0px auto;"><%=strEmail%></div>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->