<%
pageTitle = "Admin - Update WE Database - Update Press Releases"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
if request.form("submitted") = "Update" then
	for a = 1 to request.form("totalCount")
		if request.form("delete" & a) = "1" then
			SQL = "DELETE FROM we_PressReleases"
		else
			ReleaseDate = request.form("ReleaseDate" & a)
			if isDate(ReleaseDate) then
				ReleaseDate = "'" & dateValue(ReleaseDate) & "'"
			else
				ReleaseDate = "null"
			end if
			SQL = "UPDATE we_PressReleases SET"
			SQL = SQL & " ReleaseDate=" & ReleaseDate & ", "
			SQL = SQL & " Headline='" & SQLquote(request.form("Headline" & a)) & "', "
			SQL = SQL & " Headline2='" & SQLquote(request.form("Headline2" & a)) & "', "
			SQL = SQL & " BodyText='" & SQLquote(request.form("BodyText" & a)) & "', "
			SQL = SQL & " DateEntered='" & now & "'"
		end if
		SQL = SQL & " WHERE ID=" & request.form("ID" & a)
		'response.write "<p>" & SQL & "</p>"
		oConn.execute SQL
	next
	if SQLquote(request.form("HeadlineNEW")) <> "" then
		ReleaseDate = request.form("ReleaseDateNEW")
		if isDate(ReleaseDate) then
			ReleaseDate = "'" & dateValue(ReleaseDate) & "'"
		else
			ReleaseDate = "null"
		end if
		SQL = "INSERT INTO we_PressReleases (ReleaseDate, Headline, Headline2, BodyText, DateEntered) VALUES ("
		SQL = SQL & ReleaseDate & ", "
		SQL = SQL & "'" & SQLquote(request.form("HeadlineNEW")) & "', "
		SQL = SQL & "'" & SQLquote(request.form("Headline2NEW")) & "', "
		SQL = SQL & "'" & SQLquote(request.form("BodyTextNEW")) & "', "
		SQL = SQL & "'" & now & "')"
		'response.write "<p>" & SQL & "</p>"
		oConn.execute SQL
	end if
	%>
	<h3>UPDATED!</h3>
	</body>
	</html>
	<%
else
	SQL = "SELECT * FROM we_PressReleases ORDER BY ReleaseDate DESC"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	a = 0
	%>
	<font face="Arial,Helvetica"><h3><%=RS.recordcount%> items found</h3></font>
	<form name="frmEditItem" action="db_update_PressReleases.asp" method="post">
		<table border="1" cellpadding="0" cellspacing="0" width="800">
			<%
			do until RS.eof
				a = a + 1
				%>
				<tr>
					<td>
						<table border="1" cellpadding="3" cellspacing="0" align="left" width="100%">
							<tr>
								<td align="left" width="140"><b>Release&nbsp;Date</b></td>
								<td align="left" width="60"><b>Headline</b></td>
								<td align="left" width="60"><b>Headline&nbsp;2</b></td>
								<td align="left" width="100"><b>delete</b></td>
							</tr>
							<tr>
								<td align="left">
									<input type="hidden" name="ID<%=a%>" value="<%=RS("ID")%>">
									<input type="text" name="ReleaseDate<%=a%>" size="15" value="<%=RS("ReleaseDate")%>">
								</td>
								<td align="left">
									<input type="text" name="Headline<%=a%>" size="45" value="<%=RS("Headline")%>">
								</td>
								<td align="left">
									<input type="text" name="Headline2<%=a%>" size="45" value="<%=RS("Headline2")%>">
								</td>
								<td align="left">
									<input type="checkbox" name="delete<%=a%>" value="1">
								</td>
							</tr>
							<tr>
								<td align="left" colspan="4">
									<textarea name="BodyText<%=a%>" rows="10" cols="100"><%=RS("BodyText")%></textarea>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td bgcolor="black"><font size="1">&nbsp;</font></td>
				</tr>
				<%
				RS.movenext
			loop
			%>
			<tr>
				<td align="left"><b>ADD&nbsp;NEW</b></td>
			</tr>
			<tr>
				<td>
					<table border="1" cellpadding="3" cellspacing="0" align="left" width="100%">
						<tr>
							<td align="left" width="140"><b>Release&nbsp;Date</b></td>
							<td align="left" width="60"><b>Headline</b></td>
							<td align="left" width="60"><b>Headline&nbsp;2</b></td>
							<td align="left" width="100"><b>&nbsp;</b></td>
						</tr>
						<tr>
							<td align="left">
								<input type="text" name="ReleaseDateNEW" size="15">
							</td>
							<td align="left">
								<input type="text" name="HeadlineNEW" size="45">
							</td>
							<td align="left">
								<input type="text" name="Headline2NEW" size="45">
							</td>
							<td align="left">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td align="left" colspan="4">
								<textarea name="BodyTextNEW" rows="10" cols="100"></textarea>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<p>
			<input type="submit" name="submitted" value="Update">
			<input type="hidden" name="totalCount" value="<%=a%>">
		</p>
	</form>
	<p>&nbsp;</p>
	<%
	RS.close
	Set RS = nothing
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
