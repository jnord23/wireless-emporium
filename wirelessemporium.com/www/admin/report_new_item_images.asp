<%
pageTitle = "New Item Image Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="800"><tr><td valign="top" width="100%">

<%
TypeID = request("TypeID")
numberToShow = prepInt(request.form("numberToShow"))
if numberToShow = 0 then numberToShow = 100
%>

<form action="report_new_item_images.asp" method="post">
	<p>
		<select name="TypeID">
			<option value=""></option>
			<%
			SQL = "SELECT * FROM WE_Types"
			SQL = SQL & " ORDER BY TypeName"
			Set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			do until RS.eof
				%>
				<option value="<%=RS("TypeID")%>"<%if cStr(RS("TypeID")) = TypeID then response.write " selected"%>><%=RS("TypeName")%></option>
				<%
				RS.movenext
			loop
			%>
		</select>
	</p>
	<p>
		<select name="numberToShow">
			<%
			for aCount = 100 to 2000 step 50
				%>
				<option value="<%=aCount%>"<% if numberToShow = aCount then %> selected="selected"<% end if %>><%=aCount%></option>
				<%
			next
			%>
		</select>&nbsp;# to display
	</p>
	<p><input type="submit" value="Search"></p>
</form>

<%
SQL = "SELECT TOP " & numberToShow & " itemID, WE_URL, itemDesc, itemPic FROM we_Items WHERE hideLive = 0"
if TypeID <> "" then SQL = SQL & " AND typeID = '" & TypeID & "'"
if typeID = "16" then SQL = replace(sql,"typeID = '16'","(typeID = 16 or typeID = 1020 or typeID = 1000)")
SQL = SQL & " ORDER BY itemID DESC"
Set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
%>

<table border="1" cellpadding="0" cellspacing="0" align="center" width="100%">
	<tr>
	<%
	nCount = -1
	totalCount = 0
	do until RS.eof
		nCount = nCount + 1
		totalCount = totalCount + 1
		if (nCount mod 4) = 0 then
			response.write "</tr><tr>" & vbcrlf
			nCount = 0
		end if
		%>
		<td valign="top" align="center" width="25%" class="smltext" title="<%=totalCount%>">
			<a href="/p-<%=RS("itemID")%>-<%=formatSEO(RS("itemDesc"))%>.asp" target="_blank"><img src="/productpics/thumb/<%=RS("itemPic")%>" border="0"></a><br><%=RS("itemDesc")%>
		</td>
		<%
		RS.movenext
		if totalCount = 1000 then response.End()
	loop
	%>
	</tr>
</table>

<%
RS.close
Set RS = nothing
%>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
