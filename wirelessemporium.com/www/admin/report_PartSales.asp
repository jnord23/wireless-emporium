<%
pageTitle = "Non-Processed Sales by Part Number Report for ALL SITES"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<script language="javascript">
function printinvoice(orderid,accountid) {
	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
</script>

<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">
<p class="biggerText"><%=pageTitle%></p>
<br>

<%
if request.form("PartNumber") <> "" then
	SQL = "SELECT a.itemID, a.PartNumber, a.itemDesc, a.inv_qty, a.hideLive, b.orderid, b.quantity,"
	SQL = SQL & " c.store, c.accountid, c.orderdatetime, c.approved, c.emailSent, c.confirmSent, c.confirmdatetime, c.PhoneOrder, c.cancelled, c.thub_posted_to_accounting, c.thub_posted_date"
	SQL = SQL & " FROM we_Items AS a INNER JOIN we_orderdetails AS b ON a.itemID = b.itemid INNER JOIN we_orders AS c ON b.orderid = c.orderid"
	SQL = SQL & " WHERE a.PartNumber = '" & request.form("PartNumber") & "'"
	SQL = SQL & " AND c.approved = 1 AND (c.cancelled IS NULL OR c.cancelled <> 0)"
	SQL = SQL & " AND c.orderdatetime > '" & dateAdd("d",-7,date) & "'"
	SQL = SQL & " AND a.inv_qty > - 1 AND a.hideLive = 0"
	SQL = SQL & " AND c.thub_posted_to_accounting IS NULL"
	SQL = SQL & " ORDER BY b.orderid"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	
	response.write "<p><a href=""/admin/report_PartSales.asp"">Back to Report Criteria</a></p>" & vbcrlf
	
	if RS.eof then
		response.write "No records matched<br><br>So cannot make table..."
	else
		%>
		<table border="1" width="100%" cellpadding="2" cellspacing="0">
			<tr>
				<td><b>Item&nbsp;ID</b></td>
				<td><b>Part&nbsp;Number</b></td>
				<td><b>Item&nbsp;Description</b></td>
				<td><b>Inv_Qty</b></td>
				<td><b>Order&nbsp;Qty</b></td>
				<td><b>Order&nbsp;#</b></td>
			</tr>
			<%
			do until RS.eof
				%>
				<tr>
					<td><%=RS("itemID")%></td>
					<td><%=RS("PartNumber")%></td>
					<td><%=RS("itemDesc")%></td>
					<td><%=RS("inv_qty")%></td>
					<td><%=RS("quantity")%></td>
					<td><a href="#" onClick="printinvoice('<%=RS("OrderId")%>','<%=RS("AccountId")%>');"><%=RS("OrderId")%></a></td>
				</tr>
				<%
				RS.movenext
			loop
			%>
		</table>
		<br>
		<p><a href="/admin/report_ItemSales.asp">Back to Report Criteria</a></p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<%
	end if
	
	RS.close
	set RS = nothing
else
	%>
	<form action="report_PartSales.asp" method="post">
		<table border="0" width="800" align="center">
			<tr>
				<td width="60%" valign="top">
					<p><input type="text" name="PartNumber">&nbsp;&nbsp;Part #</p>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<p>&nbsp;</p>
					<p><input type="submit" name="submitted" value="Generate Report"></p>
				</td>
			</tr>
		</table>
	</form>
	<%
end if
%>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
