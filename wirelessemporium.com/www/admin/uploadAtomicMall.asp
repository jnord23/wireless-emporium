<%
response.buffer = false
pageTitle = "Upload AtomicMall Orders"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->

<%
server.scripttimeout = 1000 'seconds
dim Path
Path = server.mappath("tempTXT")

set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
Count = Upload.Save(Path)
if Upload.form("upload") <> "" then
	if Count = 0 then
		response.write "No file selected."
		response.end
	else
		set File = Upload.Files(1)
		myFile = File.path
		
		dim txtConn, myStr
		set txtConn = Server.CreateObject("ADODB.Connection")
		myStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & myFile & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"";"
		txtConn.ConnectionString = myStr
		txtConn.ConnectionTimeout = 1000		'seconds
		txtConn.Open
		
		SQL = "SELECT * FROM [orders$] ORDER BY [order_id]"
		'response.write "<h3>" & SQL & "</h3>" & vbCrLf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, txtConn, 3, 3
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			%>
			<h3><%=RS.recordcount%> records found</h3>
			<table border="1" cellpadding="1" cellspacing="0"><tr><td>WE Order Number</td><td>AtomicMall Order Number</td></tr>
			<%
			dim Order_Number, SKU, KIT, QTY, updateCount, loopCount
			dim nIdProd, sSCountry, sBCountry, sShipType, zipcode
			updateCount = 0
			do until RS.eof
				Order_Number = SQLquote(RS("order_id"))
				loopCount = 0
				for a = 1 to 10
					SKU = SQLquote(RS("sku" & a))
					QTY = SQLquote(RS("qty" & a))
					KIT = null
					if SKU <> "" and QTY <> "" then
						if isNumeric(SKU) then
							SQL = "SELECT PartNumber, ItemKit_NEW FROM we_items WHERE itemID='" & SKU & "'"
							set rsPart = server.createobject("ADODB.recordset")
							rsPart.open SQL, oConn, 3, 3
							if not rsPart.eof then
								if not isNull(rsPart("ItemKit_NEW")) then KIT = rsPart("ItemKit_NEW")
								SKU = rsPart("PartNumber")
							else
								response.write "<tr><td colspan=""2"">Item # <b>" & SKU & "</b> not found!</td></tr>" & vbcrlf
							end if
							rsPart.close
							set rsPart = nothing
						end if
'						SQL = "SELECT itemID FROM we_items WHERE PartNumber='" & SKU & "' AND (inv_qty > -1 OR ItemKit_NEW IS NOT NULL)"
						SQL = "SELECT itemID FROM we_items WHERE PartNumber='" & SKU & "'"
						set rsTemp = server.createobject("ADODB.recordset")
						rsTemp.open SQL, oConn, 3, 3
						if not rsTemp.eof then
							nIdProd = rsTemp("itemID")
							if loopCount > 0 then
								sSprocString = "sp_InsertOrderDetail '" & nOrderId & "','" & nIdProd & "','" & QTY & "'"
								'response.write "<li>" & sSprocString & "</li>" & vbcrlf
								oConn.execute(sSprocString)
							else
								if len(SQLquote(RS("country"))) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",SQLquote(RS("state_province"))) > 0 then
									sSCountry = "CANADA"
									if inStr(RS("shipmethod"),"Priority") > 0 then
										sShipType = "USPS Priority Int'l"
									else
										sShipType = "First Class Int'l"
									end if
								else
									sSCountry = ""
									if inStr(RS("shipmethod"),"Priority") > 0 then
										sShipType = "USPS Priority"
									else
										sShipType = "First Class"
									end if
								end if
								sBCountry = ""
								
								zipcode = SQLquote(RS("zipcode"))
								do until len(zipcode) > 4
									if len(zipcode) < 5 then
										zipcode = "0" & zipcode
									end if
								loop
								
								SQL = "sp_InsertAccountInfo '" & SQLquote(RS("firstname")) & "','" & SQLquote(RS("lastname")) & "','" & SQLquote(RS("address1")) & "','" & SQLquote(RS("address2")) & "','" & SQLquote(RS("city")) & "','" & SQLquote(RS("state_province")) & "','" & zipcode & "','" & sBCountry & "','" & SQLquote(RS("address1")) & "','" & SQLquote(RS("address2")) & "','" & SQLquote(RS("city")) & "','" & SQLquote(RS("state_province")) & "','" & zipcode & "','" & sSCountry & "','" & SQLquote(RS("phone")) & "','" & SQLquote(RS("email")) & "','','','" & now & "'"
								session("errorSQL") = SQL
								'response.write "<li>" & SQL & "</li>" & vbcrlf
								nAccountId = oConn.execute(SQL).fields(0).value
								
								'SQL = "sp_InsertOrder2 '" & nAccountId & "','" & FormatNumber(SQLquote(RS("subtotal")),2) & "','" & FormatNumber(SQLquote(RS("shipping_charge")),2) & "'," & FormatNumber(SQLquote(RS("tax")),2) & ",'" & FormatNumber(SQLquote(RS("order_total")),2) & "','" & SQLquote(sShipType) & "',null,0,0,'" & SQLquote(RS("date")) & "'"
								SQL = "sp_InsertOrder2 '" & nAccountId & "','" & FormatNumber(SQLquote(RS("subtotal")),2) & "','" & FormatNumber(SQLquote(RS("shipping_charge")),2) & "'," & FormatNumber(SQLquote(RS("tax")),2) & ",'" & FormatNumber(SQLquote(RS("order_total")),2) & "','" & SQLquote(sShipType) & "',null,0,0,'" & now & "'"
								session("errorSQL") = SQL
								'response.write "<li>" & SQL & "</li>" & vbcrlf
								nOrderId = oConn.execute(SQL).fields(0).value
								
								SQL = "UPDATE we_orders SET approved=-1, extOrderType='5', extOrderNumber='" & Order_Number & "' WHERE orderid='" & nOrderId & "'"
								session("errorSQL") = SQL
								'response.write "<li>" & SQL & "</li>" & vbcrlf
								oConn.execute SQL
								
								SQL = "sp_InsertOrderDetail '" & nOrderId & "','" & nIdProd & "','" & QTY & "'"
								session("errorSQL") = SQL
								'response.write "<li>" & SQL & "</li>" & vbcrlf
								oConn.execute(SQL)
								
								updateCount = updateCount + 1
								loopCount = loopCount + 1
							end if
							call UpdateInvQty(SKU, QTY, KIT)
							call NumberOfSales(nIdProd, QTY, KIT)
						else
							response.write "<tr><td colspan=""2"">Part Number <b>" & SKU & "</b> not found with inv_qty > -1!</td></tr>" & vbcrlf
						end if
					end if
				next
				RS.movenext
				response.write "<tr><td>" & nOrderId & "</td><td>" & Order_Number & "</td></tr>" & vbcrlf
			loop
		end if
		RS.close
		set RS = nothing
		%>
		</table>
		<p><%=updateCount%> orders entered into WE database.</p>
		<%
	end if
else
	%>
	<h3>Select the file you saved to your hard drive:</h3>
	<p>(Worksheet must be named "orders".)</p>
	<form enctype="multipart/form-data" action="uploadAtomicMall.asp" method="POST">
		<p><input type="FILE" name="FILE1"></p>
		<p><input type="SUBMIT" name="upload" value="Upload"></p>
	</form>
	<%
end if

function SQLquote(strValue)
	if not isNull(strValue) and strValue <> "" then
		SQLquote = trim(replace(strValue,"'","''"))
		SQLquote = trim(replace(SQLquote,chr(34),"''"))
	else
		SQLquote = strValue
	end if
end function

sub UpdateInvQty(nPartNumber, nProdQuantity, KIT)
	' Updates inv_qty for the Master Part (item with the same Part Number as the item purchased which does not have -1 inv_qty)
	' If the inv_qty would be adjusted to less than or equal to zero, then all items with the same Part Number as the item purchased are set to zero
	if isNull(KIT) then
		SQL = "SELECT itemID,typeID,vendor,inv_qty FROM we_items WHERE PartNumber = '" & nPartNumber & "' AND master = 1"
	else
		SQL = "SELECT (select itemID from we_items where partNumber = a.partNumber and master = 1) as itemID,typeID,vendor,inv_qty FROM we_items a WHERE itemID IN (" & KIT & ")"
	end if
	'response.write "<li>" & SQL & "</li>" & vbcrlf
	set oRsItemInfo2 = Server.CreateObject("ADODB.Recordset")
	oRsItemInfo2.open SQL, oConn, 3, 3
	do until oRsItemInfo2.eof
		if oRsItemInfo2("inv_qty") - nProdQuantity > 0 then
			On Error Resume Next
			sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & RS2("itemID") & "," & RS2("inv_qty") & "," & nProdQuantity & "," & nOrderID & ",0,'Atomic Mall Customer Order','" & now & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
			On Error Goto 0
			
			decreaseSQL = "UPDATE we_items SET inv_qty = inv_qty - " & nProdQuantity & " WHERE itemID = '" & oRsItemInfo2("itemID") & "'"
			'response.write "<li>" & decreaseSQL & "</li>" & vbcrlf
			oConn.execute(decreaseSQL)
		else
			On Error Resume Next
			sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & RS2("itemID") & "," & RS2("inv_qty") & "," & nProdQuantity & "," & nOrderID & ",0,'Atomic Mall Customer Order *Out of Stock*','" & now & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
			On Error Goto 0
			
			decreaseSQL = "UPDATE we_items SET inv_qty = 0 WHERE PartNumber = '" & nPartNumber & "'"
			if oRsItemInfo2("typeID") = 2 then decreaseSQL = decreaseSQL & " AND inv_qty > -1"
			if oRsItemInfo2("typeID") <> 3 and oRsItemInfo2("vendor") <> "CM" and oRsItemInfo2("vendor") <> "DS" and oRsItemInfo2("vendor") <> "MLD" then
				' Send zero-inventory e-mail to Tony
				cdo_from = "Automatic E-Mail from Wirelessemporium.com<sales@wirelessemporium.com>"
				cdo_subject = nPartNumber & " is out of stock! (Atomic Mall upload)"
				cdo_body = nPartNumber & " is out of stock! (Atomic Mall upload)"
				cdo_to = "tony@wirelessemporium.com,charles@wirelessemporium.com,steven@wirelessemporium.com,jon@wirelessemporium.com"
				CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
			end if
			'response.write "<li>" & decreaseSQL & "</li>" & vbcrlf
			oConn.execute(decreaseSQL)
		end if
		oRsItemInfo2.movenext
	loop
	oRsItemInfo2.close
	set oRsItemInfo2 = nothing
end sub

sub NumberOfSales(nIdProd, QTY, KIT)
	if isNull(KIT) then
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID = '" & nIdProd & "'"
	else
		SQL = "UPDATE we_items SET numberOfSales = numberOfSales + " & QTY & " WHERE itemID IN (" & KIT & ")"
	end if
	'response.write "<li>" & SQL & "</li>" & vbcrlf
	oConn.execute(SQL)
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
