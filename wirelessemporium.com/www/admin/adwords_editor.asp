<%
pageTitle = "Adwords Editor"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<link href="/includes/css/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/includes/js/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/includes/js/cal.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function () {
		$('#id_sdate').simpleDatepicker();
//		$('#id_edate').simpleDatepicker();
	});
</script>
<script>
	function showHide(show, hide) {
		document.getElementById(show).style.display = '';
		document.getElementById(hide).style.display = 'none';
	}
	
	function checkSubmit() {

		if (document.frm.rdoTemplate[0].checked) {
			o = document.frm.txtHeading1;
			for (i=0; i<(o.length-1); i++) {
				if (o[i].value == '') {
					alert(o[i].title + ' is requried!');
					return false;
				}
			}
		}
		else if (document.frm.rdoTemplate[1].checked) {
			o = document.frm.txtHeading2;
			for (i=0; i<o.length; i++) {
				if (o[i].value == '') {
					alert(o[i].title + ' is requried!');
					return false;
				}
			}
		}

		return true;
	}
</script>
<center>
<%
searchExcel = prepStr(request.form("searchExcel"))
if searchExcel <> "" then response.redirect "/admin/adwords_editor_excel.asp?" & request.form

siteid = prepInt(request("cbSite"))
brandid = prepInt(request("cbBrand"))
modelid = prepInt(request("cbModel"))
typeid = prepInt(request("cbType"))
partnumber = prepStr(request("txtPartnumber"))
itemid = prepStr(request("txtItemID"))
strGroupBy = prepStr(request("cbGroupBy"))
strCbRules = prepStr(request("cbRules"))
strSDate = prepStr(request("txtSDate"))
strHeading1 = prepStr(request("txtHeading1"))
strHeading2 = prepStr(request("txtHeading2"))
strRules = prepStr(request("txtRules"))
rdoTemplate = prepStr(request("rdoTemplate"))
if rdoTemplate = "" then rdoTemplate = "A"

cfgArrRulesFrom = Array("AAA", "BBB", "CCC", "DDD")
cfgArrCbRules = Array("BrandName", "ModelName", "CategoryName", "SubCategoryName")

if "" = strRules then strRules = ",,," 
arrRules = split(strRules, ",")

if "" = strCbRules then strCbRules = ",,," 
arrCbRules = split(strCbRules, ",")

cfgArrGroupBy = Array("Brand","Model","Category","SubCategory")
if "" = strGroupBy then strGroupBy = ",,," 
arrGroupBy = split(strGroupBy, ",")

'for each i in request.Form
'	response.write i & ":" & request.form(i) & "<br>"
'next

if strHeading1 = "" then strHeading1 = ",,,,,,"
if strHeading2 = "" then strHeading2 = ",,,,"
arrHeading1 = split(strHeading1, ",")
arrHeading2 = split(strHeading2, ",")

siteTypename = "typeName"
select case siteid
	case 0 
		itemdesc = "itemdesc"
		siteTypename = "typeName_WE"
		priceDesc = "price_our"
	case 1 
		itemdesc = "itemdesc_ca"
		siteTypename = "typeName_CA"
		priceDesc = "price_ca"		
	case 2 
		itemdesc = "itemdesc_co"
		siteTypename = "typeName_CO"
		priceDesc = "price_co"		
	case 3 
		itemdesc = "itemdesc_ps"
		siteTypename = "typeName_PS"
		priceDesc = "price_ps"		
	case 10 
		itemdesc = "itemdesc"
		siteTypename = "typeName"
		priceDesc = "price_our"		
end select

bDetails 		= 	true
bBrand			=	false
bModel			=	false
bCategory		=	false
bSubcategory	=	false
pageLevel		=	""

For i = 0 To Cint(UBound(arrGroupBy))
	If "" <> trim(arrGroupBy(i)) Then
		Select Case ucase(trim(arrGroupBy(i)))
			Case "BRAND"
				If not bBrand Then
					bBrand = true
					bDetails = false
	
					strSqlSelectMain	=	strSqlSelectMain 	& 	"c.brandid, c.brandName, "
					strSqlGroupBy		=	strSqlGroupBy		&	"c.brandid, c.brandName, "
					strSqlOrderBy		=	strSqlOrderBy		&	"c.brandid, c.brandName, "
					
					pageLevel = pageLevel & "b"
				End If
			Case "MODEL"
				If not bModel Then
					bModel = true
					bDetails = false
	
					strSqlSelectMain	=	strSqlSelectMain 	& 	"d.modelid, d.modelName, "
					strSqlGroupBy		=	strSqlGroupBy		&	"d.modelid, d.modelName, "
					strSqlOrderBy		=	strSqlOrderBy		&	"d.modelid, d.modelName, "
					
					pageLevel = pageLevel & "m"
				End If
			Case "CATEGORY"
				If not bCategory Then
					bCategory = true
					bDetails = false
	
					strSqlSelectMain	=	strSqlSelectMain 	& 	"f.typeid, f.typename, "
					strSqlGroupBy		=	strSqlGroupBy		&	"f.typeid, f.typename, "
					strSqlOrderBy		=	strSqlOrderBy		&	"f.typeid, f.typename, "
					
					pageLevel = pageLevel & "c"
				End If
			Case "SUBCATEGORY"
				If not bSubCategory Then
					bSubCategory = true
					bDetails = false
	
					strSqlSelectMain	=	strSqlSelectMain 	& 	"f.subtypeid, f.subtypename, "
					strSqlGroupBy		=	strSqlGroupBy		&	"f.subtypeid, f.subtypename, "
					strSqlOrderBy		=	strSqlOrderBy		&	"f.subtypeid, f.subtypename, "
					
					pageLevel = pageLevel & "d"
				End If
		End Select
	End If
Next

If bModel Then
	If not bBrand Then
		bBrand = true
		bDetails = false

		strSqlSelectMain	=	strSqlSelectMain 	& 	"c.brandid, c.brandName, "
		strSqlGroupBy		=	strSqlGroupBy		&	"c.brandid, c.brandName, "
		strSqlOrderBy		=	strSqlOrderBy		&	"c.brandid, c.brandName, "
		
		pageLevel = "b" & pageLevel
	end if
end if

if pageLevel = "" then pageLevel = "p"
%>
<form method="post" name="frm">
    <div style="text-align:left; width:1100px;">
        <div style="padding-bottom:10px; height:20px;">
        	<div style="font-size:18px; font-weight:bold; float:left;">Adwords Editor</div>
		</div>
        <!-- filters-->
        <div style="width:100%; border:1px solid #ccc; border-radius:3px; background-color:#f4f7f9; padding:5px;">
            <div style="width:100%; height:30px;">
				<div class="filter-box-header" style="width:150px;">Filter</div>
                <div class="filter-box-header" style="width:100px;">Entry Date</div>
                <div class="filter-box-header" style="width:60px;">Store</div>
                <div class="filter-box-header" style="width:80px;">Brand</div>
                <div class="filter-box-header" style="width:80px;">Model</div>
                <div class="filter-box-header" style="width:80px;">Category</div>
                <div class="filter-box-header" style="width:130px;">Partnumber</div>
                <div class="filter-box-header" style="width:80px;">ItemID</div>
                <div class="filter-box-header" style="width:130px;">Action</div>
            </div>
            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%; padding:5px 0px 5px 0px; height:25px;">
				<div class="filter-box-header" style="width:150px;">Value</div>
                <div class="filter-box" style="width:100px;">
                	<input type="text" id="id_sdate" name="txtSDate" value="<%=strSDate%>" size="9">
                    <!-- ~ <input type="text" id="id_edate" name="txtEDate" value="<%=strEDate%>" size="7">-->
				</div>
                <div class="filter-box" style="width:60px;">
                	<select name="cbSite">
                    	<option value="0" <%if siteid = "0" then%>selected<%end if%>>WE</option>
                    	<option value="1" <%if siteid = "1" then%>selected<%end if%>>CA</option>
                    	<option value="2" <%if siteid = "2" then%>selected<%end if%>>CO</option>
                    	<option value="3" <%if siteid = "3" then%>selected<%end if%>>PS</option>
                    	<option value="10" <%if siteid = "10" then%>selected<%end if%>>TM</option>
                    </select>
                </div>
                <div class="filter-box" style="width:80px;">
					<%
                    sql = "select brandid, brandName from we_brands order by 2"
                    set rsBrand = oConn.execute(sql)
                    %>
                    <select name="cbBrand" style="width:75px;" onchange="onBrand(this.value);">
                        <option value="">ALL</option>
                    <%
                    do until rsBrand.eof
						if brandid = rsBrand("brandid") then
						%>
						<option value="<%=rsBrand("brandid")%>" selected ><%=rsBrand("brandName")%></option>
                        <%
						else
						%>
						<option value="<%=rsBrand("brandid")%>"><%=rsBrand("brandName")%></option>
                        <%
						end if

                        rsBrand.movenext
                    loop
                    %>
                    </select>
                </div>
                <div id="div_model" class="filter-box" style="width:80px;">
					<%
                    sql = 	"select	modelid, modelname" & vbcrlf & _
                            "from	we_models" & vbcrlf & _
                            "where	hidelive = 0" & vbcrlf & _
                            "order by 2"
                    set rsModel = oConn.execute(sql)
                    %>
                    <select name="cbModel" style="width:75px;">
                        <option value="">ALL</option>
                    <%
                    do until rsModel.eof
						if modelid = rsModel("modelid") then
						%>
						<option value="<%=rsModel("modelid")%>" selected><%=rsModel("modelname")%></option>
                        <%
						else
						%>
						<option value="<%=rsModel("modelid")%>"><%=rsModel("modelname")%></option>
                        <%
						end if

                        rsModel.movenext
                    loop
                    %>
                    </select>
                </div>
                <div class="filter-box" style="width:80px;">
					<%
                    sql = 	"select	typeid, subtypeid, typename, subtypename" & vbcrlf & _
							"from	v_subtypematrix" & vbcrlf & _
							"order by typename, subtypename"
                    set rsType = oConn.execute(sql)
					lastTypeID = 0
                    %>
                    <select name="cbType" style="width:75px;">
                        <option value="">ALL</option>
                    <%
                    do until rsType.eof
						if lastTypeID <> rsType("typeid") then
							if lastTypeID <> 0 then response.write "</optgroup>"
						%>
						<optgroup label="<%=rsType("typename")%>"> 
                        <%
							if typeid = rsType("typeid") then
							%>
	                        <option style="font-weight:bold;" value="<%=rsType("typeid")%>" selected ><%=rsType("typename")%></option>
                            <%
							else
							%>
	                        <option style="font-weight:bold;" value="<%=rsType("typeid")%>" ><%=rsType("typename")%></option>
                            <%
							end if
						end if

						if typeid = rsType("subtypeid") then
						%>
	                        <option style="padding-left:10px;" value="<%=rsType("subtypeid")%>" selected><%=rsType("subtypename")%></option>
                        <%
						else
						%>
							<option style="padding-left:10px;" value="<%=rsType("subtypeid")%>" ><%=rsType("subtypename")%></option>
                        <%
						end if

						lastTypeID = rsType("typeid")
                        rsType.movenext
                    loop
                    %>
                    </select>
                </div>
                <div class="filter-box" style="width:130px;"><input type="text" name="txtPartnumber" value="<%=partnumber%>" style="width:120px;" /></div>
                <div class="filter-box" style="width:80px;"><input type="text" name="txtItemID" value="<%=itemid%>" style="width:70px;" /></div>
                <div class="filter-box" style="width:130px;">
	                <input type="submit" name="search" value="Search" onClick="return checkSubmit();" />
	                <input type="submit" name="searchExcel" value="Excel" />
                </div>
            </div>

            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%;  padding:5px 0px 5px 0px; height:40px;">
				<div class="filter-box-header" style="width:150px; margin-top:5px;">Keywords Rules</div>
				<div class="filter-box" style="width:900px; padding-top:3px; text-align:left;" align="left">
				<%
                if not isnull(cfgArrCbRules) then
                    for i=0 to ubound(cfgArrCbRules)
                        response.write "<div class=""filter-box-header"" style=""margin:0px 8px 0px 0px;"">"
                        response.write "<span style=""color:blue;"">" & trim(cfgArrRulesFrom(i)) & "</span>: <input type=""text"" name=""txtRules"" size=""8"" value=""" & trim(arrRules(i)) & """/> or "
                        response.write "<select name=""cbRules"" style=""width:70px;""><option value="""" "
                        if "" = trim(arrCbRules(i)) then response.write " selected " end if
                        response.write ">--</option>"
                        
                        for j=0 to ubound(cfgArrCbRules)
                            response.write "<option value=""" & trim(cfgArrCbRules(j)) & """ "
                            if ucase(trim(arrCbRules(i))) = ucase(trim(cfgArrCbRules(j))) then response.write " selected " end if
                            response.write ">" & trim(cfgArrCbRules(j)) & "</option>"
                        next
                        response.write "</select></div>"
                    next
                end if
                %>
                </div>
            </div>

            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%; padding:5px 0px 5px 0px; height:55px;">
				<div class="filter-box-header" style="text-align:left; width:150px; height:45px;">
					<input id="id_rdoTemplate1" type="radio" name="rdoTemplate" value="A" onClick="showHide('id_ad','id_keywords');" <%if rdoTemplate = "A" then%>checked<%end if%> ><label style="cursor:pointer;" for="id_rdoTemplate1">AD</label><br />
					<input id="id_rdoTemplate2" type="radio" name="rdoTemplate" value="K" onClick="showHide('id_keywords','id_ad');" <%if rdoTemplate = "K" then%>checked<%end if%>><label style="cursor:pointer;" for="id_rdoTemplate2">Keywords</label>
				</div>
				<div class="filter-box-header" style="width:900px; height:45px;">
                	<div id="id_ad" class="left" <%if rdoTemplate = "K" then%>style="display:none;"<%end if%>>
                    	<div class="left">
							<div class="left margin-h" style="width:80px;">Campaign</div>
							<div class="left margin-h" style="width:80px;">Ad Group</div>
							<div class="left margin-h" style="width:80px;">Headline</div>
							<div class="left margin-h" style="width:150px;">Description Line1</div>
							<div class="left margin-h" style="width:150px;">Description Line2</div>
							<div class="left margin-h" style="width:120px;">Display URL</div>
							<div class="left margin-h" style="width:130px;">Destination URL</div>
                        </div>
                    	<div class="left">
							<div class="left margin-h" style="width:80px;"><input type="text" name="txtHeading1" style="width:75px;" value="<%=trim(arrHeading1(0))%>" title="Campaign" /></div>
							<div class="left margin-h" style="width:80px;"><input type="text" name="txtHeading1" style="width:75px;" value="<%=trim(arrHeading1(1))%>" title="Ad Group" /></div>
							<div class="left margin-h" style="width:80px;"><input type="text" name="txtHeading1" style="width:75px;" value="<%=trim(arrHeading1(2))%>" title="Headline" /></div>
							<div class="left margin-h" style="width:150px;"><input type="text" name="txtHeading1" style="width:145px;" value="<%=trim(arrHeading1(3))%>" title="Description Line1" /></div>
							<div class="left margin-h" style="width:150px;"><input type="text" name="txtHeading1" style="width:145px;" value="<%=trim(arrHeading1(4))%>" title="Description Line2" /></div>
							<div class="left margin-h" style="width:120px;"><input type="text" name="txtHeading1" style="width:115px;" value="<%=trim(arrHeading1(5))%>" title="Display URL" /></div>
							<div class="left margin-h" style="width:130px;"><input type="text" name="txtHeading1" style="width:125px;" value="<%=trim(arrHeading1(6))%>" title="Destination URL" /></div>
                        </div>
                    </div>
                	<div id="id_keywords" class="left" <%if rdoTemplate = "A" then%>style="display:none;"<%end if%>>
                    	<div class="left">
							<div class="left margin-h" style="width:100px;">Campaign</div>
							<div class="left margin-h" style="width:100px;">Ad Group</div>
							<div class="left margin-h" style="width:200px;">Keyword</div>
							<div class="left margin-h" style="width:150px;">Criterion Type</div>
							<div class="left margin-h" style="width:150px;">MAX CPC</div>
                        </div>
                    	<div class="left">
							<div class="left margin-h" style="width:100px;"><input type="text" name="txtHeading2" style="width:75px;" value="<%=trim(arrHeading2(0))%>" title="Campaign" /></div>
							<div class="left margin-h" style="width:100px;"><input type="text" name="txtHeading2" style="width:75px;" value="<%=trim(arrHeading2(1))%>" title="Ad Group" /></div>
							<div class="left margin-h" style="width:200px;"><input type="text" name="txtHeading2" style="width:175px;" value="<%=trim(arrHeading2(2))%>" title="Keyword" /></div>
							<div class="left margin-h" style="width:150px;"><input type="text" name="txtHeading2" style="width:145px;" value="<%=trim(arrHeading2(3))%>" title="Criterion Type" /></div>
							<div class="left margin-h" style="width:150px;"><input type="text" name="txtHeading2" style="width:145px;" value="<%=trim(arrHeading2(4))%>" title="MAX CPC" /></div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="clear"></div>
            
            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%; padding-top:5px; height:25px;">
				<div class="filter-box-header" style="width:150px;">Group By</div>
				<div class="filter-box" style="width:800px; padding-top:3px; text-align:left;" align="left">
                <%
				if not isnull(cfgArrGroupBy) then
					for i=0 to ubound(cfgArrGroupBy)
						response.write "<select name=""cbGroupBy""><option value="""" "
						if "" = trim(arrGroupBy(i)) then response.write " selected " end if
						response.write ">--</option>"
						
						for j=0 to ubound(cfgArrGroupBy,1)
							response.write "<option value=""" & cfgArrGroupBy(j) & """ "
							if ucase(trim(arrGroupBy(i))) = ucase(trim(cfgArrGroupBy(j))) then response.write " selected " end if
							response.write ">" & trim(cfgArrGroupBy(j)) & "</option>"
						next
						response.write "</select>&nbsp;"
					next
				end if
				%>
                </div>
            </div>
        </div>
		<!--// filters-->
        <!-- result -->
		<div style="width:100%; padding-top:10px;">
        <%
		if request.form("search") <> "" then
			strSqlWhere = ""
			strSqlWhere2 = ""
			bUniversal = false
			univTypes = ",8,1310,1320,1330,1350,1370,1395,"
			if instr(univTypes, "," & typeid & ",") > 0 then bUniversal = true
			
			if brandid <> 0 and not bUniversal then 
				strSqlWhere = strSqlWhere & "	and	a.brandid = " & brandid & vbcrlf
				strSqlWhere2 = strSqlWhere2 & "	and	a.brandid = " & brandid & vbcrlf
			end if
			if modelid <> 0 and not bUniversal then 
				strSqlWhere = strSqlWhere & "	and	a.modelid = " & modelid & vbcrlf
				strSqlWhere2 = strSqlWhere2 & "	and	a.modelid = " & modelid & vbcrlf
			end if
			if typeid > 0 and typeid < 1000 then 
				strSqlWhere = strSqlWhere & "	and	f.typeid = " & typeid & vbcrlf
				strSqlWhere2 = strSqlWhere2 & "	and	f.typeid = " & typeid & vbcrlf
			end if
			if typeid > 0 and typeid >= 1000 then 
				strSqlWhere = strSqlWhere & "	and	f.subtypeid = " & typeid & vbcrlf
				strSqlWhere2 = strSqlWhere2 & "	and	f.subtypeid = " & typeid & vbcrlf
			end if
			if itemid <> "" then 
				strSqlWhere = strSqlWhere & "	and	a.itemid = '" & itemid & "'" & vbcrlf
				strSqlWhere2 = strSqlWhere2 & "	and	e.itemid = " & itemid & vbcrlf
			end if
			if partnumber <> "" then 
				strSqlWhere = strSqlWhere & "	and	a.partnumber like '" & partnumber & "'" & vbcrlf
				strSqlWhere2 = strSqlWhere2 & "	and	e.partnumber like '" & partnumber & "'" & vbcrlf
			end if
			if strSDate <> "" then 
				strSqlWhere = strSqlWhere & "	and	(a.datetimeentd > dateadd(dd, -30, '" & strSDate & "') or a.invlastupdateddate > dateadd(dd, -30, '" & strSDate & "') or a.pricelastupdateddate > dateadd(dd, -30, '" & strSDate & "'))" & vbcrlf
				strSqlWhere2 = strSqlWhere2 & "	and	(e.datetimeentd > dateadd(dd, -30, '" & strSDate & "') or e.invlastupdateddate > dateadd(dd, -30, '" & strSDate & "') or e.pricelastupdateddate > dateadd(dd, -30, '" & strSDate & "'))" & vbcrlf
			end if
			
			if bDetails then
				sql	=	"select	distinct top 5 a.brandid, c.brandName, a.modelid, d.modelName, a.typeid, b." & siteTypename & " typeName, a.itemid, a.subtypeid, f.subtypename, a." & itemdesc & " itemdesc" & vbcrlf & _
						"from	we_items a join we_types b" & vbcrlf & _
						"	on	a.typeid = b.typeid join we_brands c" & vbcrlf & _
						"	on	a.brandid = c.brandid join we_models d" & vbcrlf & _
						"	on	a.modelid = d.modelid join v_subtypematrix f" & vbcrlf & _
						"	on	a.subtypeid = f.subtypeid" & vbcrlf & _
						"where	a.hidelive = 0 and a.inv_qty <> 0 and a." & priceDesc & " > 0 " & vbcrlf & _
						strSqlWhere & vbcrlf & _
						"union" & vbcrlf & _
						"-- related list" & vbcrlf & _
						"select	distinct top 5 a.brandid, c.brandName, a.modelid, d.modelName, a.typeid, b." & siteTypename & " typeName, e.itemid, e.subtypeid, f.subtypename, e." & itemdesc & " itemdesc" & vbcrlf & _
						"from	we_relateditems a join we_types b" & vbcrlf & _
						"	on	a.typeid = b.typeid join we_brands c" & vbcrlf & _
						"	on	a.brandid = c.brandid join we_models d" & vbcrlf & _
						"	on	a.modelid = d.modelid join we_items e" & vbcrlf & _
						"	on	a.itemid = e.itemid join v_subtypematrix f" & vbcrlf & _
						"	on	e.subtypeid = f.subtypeid" & vbcrlf & _
						"where	e.hidelive = 0 and e.inv_qty <> 0 and e." & priceDesc & " > 0 " & vbcrlf & _
						strSqlWhere2 & vbcrlf & _
						"union" & vbcrlf & _
						"-- sub-related list" & vbcrlf & _
						"select	distinct top 5 a.brandid, c.brandName, a.modelid, d.modelName, f.typeid, f.typeName, e.itemid, e.subtypeid, f.subtypename, e." & itemdesc & " itemdesc" & vbcrlf & _
						"from	we_subrelateditems a join we_brands c" & vbcrlf & _
						"	on	a.brandid = c.brandid join we_models d" & vbcrlf & _
						"	on	a.modelid = d.modelid join we_items e " & vbcrlf & _
						"	on	a.itemid = e.itemid join we_types b" & vbcrlf & _
						"	on	e.typeid = b.typeid join v_subtypematrix f" & vbcrlf & _
						"	on	a.subtypeid = f.subtypeid" & vbcrlf & _
						"where	e.hidelive = 0 and e.inv_qty <> 0 and e." & priceDesc & " > 0 " & vbcrlf & _
						strSqlWhere2 & vbcrlf
			
			else
				strSqlSelectMain = "select top 5 " & left(trim(strSqlSelectMain), len(trim(strSqlSelectMain))-1)
				strSqlGroupBy	=	"group by " & left(trim(strSqlGroupBy), len(trim(strSqlGroupBy))-1)
				strSqlOrderBy	=	"order by " & left(trim(strSqlOrderBy), len(trim(strSqlOrderBy))-1)
				
				sql	=	strSqlSelectMain & vbcrlf & _
						"from	we_items a join we_types b" & vbcrlf & _
						"	on	a.typeid = b.typeid join we_brands c" & vbcrlf & _
						"	on	a.brandid = c.brandid join we_models d" & vbcrlf & _
						"	on	a.modelid = d.modelid join v_subtypematrix f" & vbcrlf & _
						"	on	a.subtypeid = f.subtypeid" & vbcrlf & _
						"where	a.hidelive = 0 and a.inv_qty <> 0 and a." & priceDesc & " > 0 " & vbcrlf & _
						strSqlWhere & vbcrlf & _
						strSqlGroupBy & vbcrlf & _
						"union" & vbcrlf & _
						"-- related list" & vbcrlf & _
						strSqlSelectMain & vbcrlf & _
						"from	we_relateditems a join we_types b" & vbcrlf & _
						"	on	a.typeid = b.typeid join we_brands c" & vbcrlf & _
						"	on	a.brandid = c.brandid join we_models d" & vbcrlf & _
						"	on	a.modelid = d.modelid join we_items e" & vbcrlf & _
						"	on	a.itemid = e.itemid join v_subtypematrix f" & vbcrlf & _
						"	on	e.subtypeid = f.subtypeid" & vbcrlf & _
						"where	e.hidelive = 0 and e.inv_qty <> 0 and e." & priceDesc & " > 0 " & vbcrlf & _
						strSqlWhere2 & vbcrlf & _
						strSqlGroupBy & vbcrlf & _
						"union" & vbcrlf & _
						"-- sub-related list" & vbcrlf & _
						strSqlSelectMain & vbcrlf & _
						"from	we_subrelateditems a join we_brands c" & vbcrlf & _
						"	on	a.brandid = c.brandid join we_models d" & vbcrlf & _
						"	on	a.modelid = d.modelid join we_items e " & vbcrlf & _
						"	on	a.itemid = e.itemid join we_types b" & vbcrlf & _
						"	on	e.typeid = b.typeid join v_subtypematrix f" & vbcrlf & _
						"	on	a.subtypeid = f.subtypeid" & vbcrlf & _
						"where	e.hidelive = 0 and e.inv_qty <> 0 and e." & priceDesc & " > 0 " & vbcrlf & _
						strSqlWhere2 & vbcrlf & _
						strSqlGroupBy & vbcrlf					
			end if

'			response.write "<pre>" & sql & "</pre>"
'			response.end
			set rs = oConn.execute(sql)
			if rs.eof then
				response.write "No data to display"
			else
				%>
				<div style="width:100%; height:30px; border:1px solid #c1e0b2; border-radius:3px; background-color:#eaf5e5; padding:5px;">
					<div class="left">
                   	<%if rdoTemplate = "A" then%>
						<div class="filter-box-header" style="width:80px;">Campaign</div>
						<div class="filter-box-header" style="width:80px;">Ad Group</div>
						<div class="filter-box-header" style="width:80px;">Headline</div>
						<div class="filter-box-header" style="width:130px;">Description Line1</div>
						<div class="filter-box-header" style="width:130px;">Description Line2</div>
						<div class="filter-box-header" style="width:450px;">Display URL / Destination URL</div>
					<%else%>
						<div class="filter-box-header" style="width:200px;">Campaign</div>
						<div class="filter-box-header" style="width:200px;">Ad Group</div>
						<div class="filter-box-header" style="width:200px;">Keyword</div>
						<div class="filter-box-header" style="width:150px;">Criterion Type</div>
						<div class="filter-box-header" style="width:150px;">MAX CPC</div>
					<%end if%>
					</div>
				</div>
				<div class="clear"></div>
				<div class="left w100" id="searchResult">
				<%
				lap = 0

				criType = prepStr(arrHeading2(3))
				maxCPC = prepStr(arrHeading2(4))

				if criType <> "" then arrCriType = split(criType, "//")
				if maxCPC <> "" then arrMaxCPC = split(maxCPC, "//")
				
				do until rs.eof
					lap = lap + 1
					
					rowBackgroundColor = "#fff"
					if (lap mod 2) = 0 then rowBackgroundColor = "#eaf5fa"

					brandName = ""
					arrModelName = ""
					modelName = ""
					categoryName = ""
					subCategoryName = ""
					sqlBrandID = 0
					sqlModelID = 0
					sqlCategoryID = 0
					sqlSubcategoryID = 0
					sqlItemID = 0
					itemdesc = ""
					
					if bDetails then
						brandName = replace(rs("brandName"), "/", " / ")
						modelName = replace(rs("modelName"), "/", " / ")
						categoryName = replace(rs("typeName"), "/", " / ")
						subCategoryName = replace(rs("subtypename"), "/", " / ")
						sqlItemID = rs("itemid")
						itemdesc = rs("itemdesc")
					else
						if bBrand then 
							sqlBrandID = rs("brandid")
							brandName = replace(rs("brandName"), "/", " / ")
						end if
						if bModel then 
							sqlModelID = rs("modelid")
							modelName = replace(rs("modelName"), "/", " / ")
						end if
						if bCategory then 
							sqlCategoryID = rs("typeid")
							categoryName = replace(rs("typeName"), "/", " / ")
						end if
						if bSubcategory then 
							sqlSubcategoryID = rs("subtypeid")
							subCategoryName = replace(rs("subtypename"), "/", " / ")
						end if
					end if

					arrModelName = split(modelName, " / ")	'ex Fascinate i500 (Verizon) / Mesmerize (US Cellular) / Showcase (C-Spire)
					for m=0 to ubound(arrModelName)
						newModelName = trim(arrModelName(m))
						destURL = buildURL(pageLevel, siteid, sqlBrandID, sqlModelID, sqlCategoryID, sqlSubcategoryID, sqlItemID, brandName, newModelName, categoryName, subCategoryName, itemdesc)
	
						if rdoTemplate = "A" then
							campaign = performKeywordReplace(arrHeading1(0), brandName, newModelName, categoryName, subCategoryName)
							adGroup = performKeywordReplace(arrHeading1(1), brandName, newModelName, categoryName, subCategoryName)
							headline = performKeywordReplace(arrHeading1(2), brandName, newModelName, categoryName, subCategoryName)
							desc1 = performKeywordReplace(arrHeading1(3), brandName, newModelName, categoryName, subCategoryName)
							desc2 = performKeywordReplace(arrHeading1(4), brandName, newModelName, categoryName, subCategoryName)
							dispURL = performKeywordReplace(arrHeading1(5), brandName, newModelName, categoryName, subCategoryName)
						%>
						<div class="left w100 padding5" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='1px solid <%=rowBackgroundColor%>';this.style.backgroundColor='<%=rowBackgroundColor%>'">
							<div class="filter-box-header" style="width:82px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=campaign%></div>
							<div class="filter-box-header" style="width:82px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=adGroup%></div>
							<div class="filter-box-header" style="width:82px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=headline%></div>
							<div class="filter-box-header" style="width:132px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=desc1%></div>
							<div class="filter-box-header" style="width:132px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=desc2%></div>
							<div class="filter-box-header" style="width:452px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;">
								<%=dispURL%><br />
								<%=destURL%>
							</div>
						</div>
						<%
						else
							campaign = performKeywordReplace(arrHeading2(0), brandName, newModelName, categoryName, subCategoryName)
							adGroup = performKeywordReplace(arrHeading2(1), brandName, newModelName, categoryName, subCategoryName)
							keyword = performKeywordReplace(arrHeading2(2), brandName, newModelName, categoryName, subCategoryName)
							
							if isarray(arrCriType) and isarray(arrMaxCPC) then
								if ubound(arrCriType) = ubound(arrMaxCPC) then
									for i=0 to ubound(arrCriType)
									%>
									<div class="left w100 padding5" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='1px solid <%=rowBackgroundColor%>';this.style.backgroundColor='<%=rowBackgroundColor%>'">
										<div class="filter-box-header" style="width:200px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=campaign%></div>
										<div class="filter-box-header" style="width:200px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=adGroup%></div>
										<div class="filter-box-header" style="width:200px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=keyword%></div>
										<div class="filter-box-header" style="width:150px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=trim(arrCriType(i))%></div>
										<div class="filter-box-header" style="width:150px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=trim(arrMaxCPC(i))%></div>
									</div>
									<%
									next
								else
								%>
								<div class="left w100 padding5" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='1px solid <%=rowBackgroundColor%>';this.style.backgroundColor='<%=rowBackgroundColor%>'">
									<div class="filter-box-header" style="width:200px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=campaign%></div>
									<div class="filter-box-header" style="width:200px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=adGroup%></div>
									<div class="filter-box-header" style="width:200px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=keyword%></div>
									<div class="filter-box-header" style="width:150px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;">N/A</div>
									<div class="filter-box-header" style="width:150px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;">N/A</div>
								</div>
								<%
								end if
							else
						%>
						<div class="left w100 padding5" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='1px solid <%=rowBackgroundColor%>';this.style.backgroundColor='<%=rowBackgroundColor%>'">
							<div class="filter-box-header" style="width:200px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=campaign%></div>
							<div class="filter-box-header" style="width:200px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=adGroup%></div>
							<div class="filter-box-header" style="width:200px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;"><%=keyword%></div>
							<div class="filter-box-header" style="width:150px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;">N/A</div>
							<div class="filter-box-header" style="width:150px; border:0px; background:none; text-align:left; font-weight:normal; font-size:11px;">N/A</div>
						</div>
						<%
							end if
						end if
					next
					rs.movenext
				loop
				%>
				</div>
				<%
			end if
		end if
		%>
       	</div>				
        <!--// result -->
    </div>
</form>    
</center>
<%
function buildURL(pageLevel, site_id, brandid, modelid, categoryid, subcategoryid, itemid, brandname, modelname, categoryname, subcategoryname, itemdesc)
	siteURL = ""
	select case site_id
		case 0 : siteURL = "http://www.wirelessemporium.com"
		case 1 : siteURL = "http://www.cellphoneaccents.com"
		case 2 : siteURL = "http://www.cellularoutfitter.com"
		case 3 : siteURL = "http://www.phonesale.com"
		case 10 : siteURL = "http://www.tabletmall.com"
	end select

	strLink = ""
	select case lcase(pageLevel)
		case "p"
			select case site_id
				case 0
					strLink = siteURL & "/p-" & itemid & "-" & formatSEO(itemdesc) & ".asp"
				case 1
					strLink = siteURL & "/p-" & itemid & "-" & formatSEO(itemdesc) & ".html"
				case 2
					strLink = siteURL & "/p-" & itemid & "-" & formatSEO(itemdesc) & ".html"
				case 3
					if categoryid = 16 then
						strLink = siteURL & "/" & formatSEO(brandname) & "/cell-phones/p-" & itemid+300001 & "-tc-" & categoryid & "-" & formatSEO(itemdesc) & ".html"
					else
						strLink = siteURL & "/accessories/" & formatSEO(categoryname) & "/" & formatSEO(itemdesc) & "-p-" & itemid+300001 & ".html"
					end if
				case 10
					strLink = siteURL & "/" & formatSEO(itemdesc) & "-p-" & itemid
			end select
		case "b"
			select case site_id
				case 0
					set oParam = CreateObject("Scripting.Dictionary")
					oParam.CompareMode = vbTextCompare
					oParam.Add "x_brandID", brandid
					oParam.Add "x_phoneOnly", 1
					strLink = siteURL & getMasterURL("b", brandid, oParam)
			end select
		case "c"
			select case site_id
				case 0
					strLink = siteURL & getMasterURL("c", categoryid, null)
			end select
		case "bc","cb"
			select case site_id
				case 0
					set oParam = CreateObject("Scripting.Dictionary")
					oParam.CompareMode = vbTextCompare
					oParam.Add "x_brandID", brandID
					oParam.Add "x_categoryID", categoryID
'					if brandid = 17 and phoneOnly = 1 then
'						oParam.Add "x_brandName", "apple-iphone"
'					elseif brandid = 17 and phoneOnly = 0 then
'						oParam.Add "x_brandName", "apple-ipod-ipad"
'					else
						oParam.Add "x_brandName", brandName	
'					end if
					oParam.Add "x_categoryName", categoryName	
					strLink = siteURL & getMasterURL("cb", "", oParam)
					'=========================================================================================
			end select
		case "bm","m"
			select case site_id
				case 0
					strLink = siteURL & "/T-" & modelid & "-cell-accessories-" & formatSEO(brandname) & "-" & formatSEO(modelName)
			end select
		case "bmc","mc"
			select case site_id
				case 0
					set oParam = CreateObject("Scripting.Dictionary")
					oParam.CompareMode = vbTextCompare
					oParam.Add "x_brandID", brandID
					oParam.Add "x_modelID", modelID
					oParam.Add "x_categoryID", categoryID
					oParam.Add "x_brandName", brandName
					oParam.Add "x_modelName", modelName
					oParam.Add "x_categoryName", categoryName
					strLink = siteURL & getMasterURL("bmc", "", oParam)
			end select
		case "mcd","bmcd","bmd"
			select case site_id
				case 0
					set oParam = CreateObject("Scripting.Dictionary")
					oParam.CompareMode = vbTextCompare
					oParam.Add "x_brandID", brandID
					oParam.Add "x_modelID", modelID
					oParam.Add "x_categoryID", subcategoryid
					oParam.Add "x_brandName", brandName
					oParam.Add "x_modelName", modelName
					oParam.Add "x_categoryName", subCategoryName
					strLink = siteURL & getMasterURL("bmcd", "", oParam)
			end select
	end select
	buildURL = strLink
end function

function performKeywordReplace(byval content, byval pBrandName, byval pModelName, byval pCategoryName, byval pSubcategoryName)
	if not isnull(content) then
		for i=0 to ubound(arrRules)
			if trim(arrRules(i)) <> "" then
				content = replace(content, trim(cfgArrRulesFrom(i)), trim(arrRules(i)))
			else
				keywordTo = "[keyword missing]"
				select case lcase(trim(arrCbRules(i)))
					case "brandname"
						if pBrandName <> "" then keywordTo = pBrandName
					case "modelname"
						if pModelName <> "" then keywordTo = pModelName
					case "categoryname"
						if pCategoryName <> "" then keywordTo = pCategoryName
					case "subcategoryname"
						if pSubcategoryName <> "" then keywordTo = pSubcategoryName
				end select
				
				content = replace(content, trim(cfgArrRulesFrom(i)), keywordTo)
			end if
		next
	end if
	performKeywordReplace = content
end function
%>
<script>
	function onBrand(brandid) {
		ajax('/ajax/admin/ajaxAdwords.asp?brandid='+brandid,'div_model');
	}
</script>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->