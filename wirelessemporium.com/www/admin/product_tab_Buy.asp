<%
Server.ScriptTimeout = 9000 'seconds
pageTitle = "Product List upload for Buy.com"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
'Map the file name to the physical path on the server.
filename = "Buy.com - New SKU.txt"
path = Server.MapPath("tempTXT") & "\" & filename
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List upload for Buy.com</p>
							<p><input type="text" name="lastItemID">&nbsp;Last Item ID</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							lastItemID = request.form("lastItemID")
							if not isNumeric(lastItemID) then
								response.write "<h3>You must enter a valid &quot;Last Item ID&quot;</h3>" & vbcrlf
							else
								response.write("<b>CreateFile:</b><br>")
								DeleteFile(path)
								CreateFile(path)
								response.write "Here is the <a href='tempTXT/" & filename & "'>file</a>"
							end if
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		response.write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	dim RS, SQL, RS2, DoNotInclude
	response.write("Creating " & filename & ".<br>")
	set file = fs.CreateTextFile(path)
	SQL = "SELECT A.itemID,A.typeID,A.PartNumber AS MPN,A.itemDesc,A.itemLongDetail,"
	SQL = SQL & " A.UPCCode,A.itemPic,A.price_Retail,A.price_Our,A.price_Buy,A.inv_qty,A.Sports,"
	SQL = SQL & " A.Bullet1,A.Bullet2,A.Bullet3,A.Bullet4,A.Bullet5,A.Bullet6,A.COMPATIBILITY,A.PackageContents,"
	SQL = SQL & " B.brandName,C.modelName,C.[temp],D.typeName"
	SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
	SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
	SQL = SQL & " LEFT JOIN we_Types D ON A.typeID=D.typeID"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " AND A.itemID > '" & lastItemID & "'"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		dim stypeName, BrandName, strItemLongDetail, MfgProductIdentifier, priceOur
		dim strPartNumbers, itemDesc
		dim Features, AddlImages, UPCCode, strUPCCode
		dim imgPath, strline
		file.writeline "seller-id" & vbtab & "gtin" & vbtab & "isbn" & vbtab & "mfg-name" & vbtab & "mfg-part-number" & vbtab & "asin" & vbtab & "seller-sku" & vbtab & "title" & vbtab & "description" & vbtab & "main-image" & vbtab & "additional-images" & vbtab & "weight" & vbtab & "features" & vbtab & "listing-price" & vbtab & "msrp" & vbtab & "keywords" & vbtab & "product-set-id" & vbtab & "store-code" & vbtab & "category-id"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("MPN") & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				itemDesc = RS("itemDesc")
				if inStr(strPartNumbers,"|" & RS("MPN") & "|" & itemDesc & "|") = 0 then
					strPartNumbers = strPartNumbers & "|" & RS("MPN") & "|" & itemDesc & "|"
					if isnull(RS("typeName")) then
						stypeName = "Universal Gear"
					else
						stypeName = RS("typeName")
					end if
					if isnull(RS("BrandName")) then
						BrandName = "Universal"
					else 
						BrandName = RS("BrandName")
					end if
					ModelName = RS("ModelName")
					strItemLongDetail = RS.fields("itemLongDetail")
					strItemLongDetail = replace(strItemLongDetail,vbcrlf," ")
					strItemLongDetail = replace(strItemLongDetail,vbtab," ")
					strItemLongDetail = replace(strItemLongDetail," FREE!"," less!")
					strItemLongDetail = replace(strItemLongDetail,chr(34),"''")
					strItemLongDetail = replace(strItemLongDetail,"<a href='/downloads/","<a href='http://www.wirelessemporium.com/downloads/")
					
					AddlImages = ""
					for iCount = 0 to 2
						imgPath = "e:\inetpub\wwwroot\wirelessemporium.com\productpics\AltViews\" & replace(RS("itemPic"),".jpg","-" & iCount & ".jpg")
						if fs.FileExists(imgPath) then AddlImages = AddlImages & replace(replace(imgPath,"e:\inetpub\wwwroot\wirelessemporium.com","http://www.wirelessemporium.com"),"\","/") & "|"
						imgPath = "e:\inetpub\wwwroot\wirelessemporium.com\productpics\AltViews\" & replace(RS("itemPic"),".jpg","-" & iCount & ".gif")
						if fs.FileExists(imgPath) then AddlImages = AddlImages & replace(replace(imgPath,"e:\inetpub\wwwroot\wirelessemporium.com","http://www.wirelessemporium.com"),"\","/") & "|"
						imgPath = "e:\inetpub\wwwroot\wirelessemporium.com\productpics\AltViews\" & replace(RS("itemPic"),".gif","-" & iCount & ".jpg")
						if fs.FileExists(imgPath) then AddlImages = AddlImages & replace(replace(imgPath,"e:\inetpub\wwwroot\wirelessemporium.com","http://www.wirelessemporium.com"),"\","/") & "|"
						imgPath = "e:\inetpub\wwwroot\wirelessemporium.com\productpics\AltViews\" & replace(RS("itemPic"),".gif","-" & iCount & ".gif")
						if fs.FileExists(imgPath) then AddlImages = AddlImages & replace(replace(imgPath,"e:\inetpub\wwwroot\wirelessemporium.com","http://www.wirelessemporium.com"),"\","/") & "|"
					next
					if right(AddlImages,1) = "|" then AddlImages = left(AddlImages,len(AddlImages)-1)
					
					if inStr(strUPCCode,"|" & RS("UPCCode") & "|") = 0 then
						UPCCode = RS("UPCCode")
						strUPCCode = strUPCCode & "|" & UPCCode & "|"
					else
						UPCCode = ""
					end if
					
					Features = bulletList(RS("Bullet1")) & bulletList(RS("Bullet2")) & bulletList(RS("Bullet3")) & bulletList(RS("Bullet4")) & bulletList(RS("Bullet5")) & bulletList(RS("Bullet6")) & vbtab
					Features = replace(Features,chr(9),"")
					if right(Features,1) = "~" then Features = left(Features,len(Features)-1)
					
					if RS("typeID") = 16 then
						priceOur = RS("price_Buy")
						mfgName = BrandName
						MfgProductIdentifier = ModelName
						weight = "2"
						categoryID = "40654"
						Features = Features & "~COMPATIBILITY: " & RS("COMPATIBILITY") & "~WHAT IS INCLUDED: " & RS("PackageContents")
						Features = replace(Features,vbcrlf," ")
						strItemLongDetail = strItemLongDetail & " Wireless Emporium offers direct replacement and unlocked Cell/PDA/Smart Phones to you WITHOUT ANY CONTRACTS! Order your replacement or unlocked Cell/PDA/Smart Phones today!"
					else
						mfgName = "Wireless Emporium, Inc."
						MfgProductIdentifier = "WE" & RS("itemid") & RS("MPN")
						MfgProductIdentifier = replace(MfgProductIdentifier,"-","",1,2)
						weight = "0.3"
						categoryID = "3054"
						if RS("price_Buy") > 0 then
							priceOur = formatNumber(cdbl(RS("price_Buy")))
						elseif isNumeric(RS("price_our")) then
							priceOur = formatNumber(cdbl(RS("price_our")) - 3.01,2)
						else
							priceOur = RS("price_our")
						end if
					end if
					
					strline = "11704981" & vbtab								'seller-id
					strline = strline & UPCCode & vbtab							'gtin
					strline = strline & vbtab									'isbn
					strline = strline & mfgName & vbtab		'mfg-name
					strline = strline & MfgProductIdentifier & vbtab			'mfg-part-number
					strline = strline & vbtab									'asin
					strline = strline & RS("MPN") & "-" & RS("itemID") & vbtab	'seller-sku
					strline = strline & itemDesc & vbtab						'title
					strline = strline & removeHTML(strItemLongDetail) & vbtab	'description
					strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab	'main-image
					strline = strline & addlImages & vbtab						'additional-images
					strline = strline & weight & vbtab							'weight
					strline = strline & removeHTML(Features) & vbtab			'features
					strline = strline & priceOur & vbtab						'listing-price
					strline = strline & RS("price_Retail") & vbtab				'msrp
					strline = strline & stypeName & "|" & BrandName & "|" & ModelName & "|" & itemDesc & vbtab	'keywords
					strline = strline & vbtab									'product-set-id
					strline = strline & "7000" & vbtab							'store-code
					strline = strline & categoryID								'category-id
					file.writeline strline
				end if
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub

function bulletList(str)
	if len(str) > 1 then
		bulletList = replace(str,"~","") & "~"
	else
		bulletList = ""
	end if
end function

function removeHTML(str)
	removeHTML = replace(str,"<br>"," ",1,99,1)
	removeHTML = replace(replace(removeHTML,"<p>"," ",1,99,1),"</p>"," ",1,99,1)
	removeHTML = replace(replace(removeHTML,"<b>","",1,99,1),"</b>","",1,99,1)
	removeHTML = replace(removeHTML,"<b class='bigtext'>","",1,99,1)
	removeHTML = replace(replace(removeHTML,"<li>","",1,99,1),"</li>","",1,99,1)
	removeHTML = replace(replace(removeHTML,"<ul>","",1,99,1),"</ul>","",1,99,1)
	removeHTML = replace(replace(removeHTML,"<font color=""#FF0000"">","",1,99,1),"</font>","",1,99,1)
end function
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
