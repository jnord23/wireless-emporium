<%
pageTitle = "Admin - Update WE Database - Update Order Cancellations"
header = 1
server.scripttimeout = 1000
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<blockquote>
<blockquote>

<h3>Update WE Database - Update Order Cancellations</h3>

<%
function showNothing(strValue)
	if isNull(strValue) then
		showNothing = "-null-"
	elseif strValue = "" then
		showNothing = "&nbsp;"
	else
		showNothing = strValue
	end if
end function

a = 0
strError = ""

if request("submitted2") = "Update" then
	for a = 1 to request("totalCount")
		SQL = "UPDATE we_Orders SET cancelled=" & request("cancelled" & a) & " WHERE OrderID='" & request("OrderID" & a) & "'"
		oConn.execute SQL
	next
	%>
	<h3>UPDATED!</h3>
	<%
end if

if request("submitted") <> "" or request("submitted2") <> "" then
	if isDate(request("orderStart")) then
		strOrderStartDate = dateValue(request("orderStart"))
	else
		strError = strError & "Start Order Date must be a valid date.<br>"
	end if
	if isDate(request("orderEnd")) then
		strOrderEndDate = dateValue(request("orderEnd"))
	else
		strError = strError & "End Order Date must be a valid date.<br>"
	end if
	if strError = "" then
		if strOrderStartDate > strOrderEndDate then
			strError = strError & "Start Order Date must be earlier than or equal to End Order Date.<br>"
		end if
	end if
	
	SQL = "SELECT A.OrderID,A.emailSent,A.approved,A.cancelled,A.thub_posted_to_accounting,A.thub_posted_date,A.OrderDateTime,"
	SQL = SQL & " B.fName,B.lName,A.ordergrandtotal,C.ordernotes FROM (we_Orders A INNER JOIN we_Accounts B ON A.AccountID=B.AccountID)"
	'SQL = SQL & " LEFT OUTER JOIN we_ordernotes C ON A.OrderID=C.OrderID"
	SQL = SQL & " INNER JOIN we_ordernotes C ON A.OrderID=C.OrderID"
	SQL = SQL & " WHERE A.OrderDateTime >= '" & strOrderStartDate & "' AND A.OrderDateTime <= '" & strOrderEndDate & "'"
	SQL = SQL & " AND A.approved=1"
	SQL = SQL & " AND A.store=0"
	'SQL = SQL & " AND C.ordernotes IS NOT null"
	SQL = SQL & " ORDER BY A.OrderDateTime"
	'response.write "<h3>" & SQL & "</h3>"
	'response.end
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	a = 0
	if strError <> "" then
		response.write "<h3>" & strError & "</h3>"
		response.write "<a href=""db_update_order_cancellations.asp"">Try again.</a>"
	elseif RS.eof then
		response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
		response.write "<a href=""db_update_order_cancellations.asp"">Try again.</a>"
	else
		%>
		<p><a href="db_update_order_cancellations.asp">Search Another Order Date Range</a></p>
		<table border="1" cellpadding="0" cellspacing="0" width="90%" align="center">
			<form name="frmUpdateOrders" action="db_update_order_cancellations.asp" method="post">
				<tr>
					<td valign="top" align="center"><b>OrderID</b></td>
					<td valign="top" align="center"><b>Order&nbsp;Date</b></td>
					<td valign="top" align="center"><b>Name</b></td>
					<td valign="top" align="center"><b>Total</b></td>
					<td valign="top" align="center"><b>Cancelled</b></td>
					<td valign="top" align="center"><b>Order&nbsp;Notes</b></td>
					<td valign="top" align="center"><b>Processed</b></td>
					<td valign="top" align="center"><b>Processed&nbsp;Date</b></td>
				</tr>
				<%
				do until RS.eof
					OrderID = RS("OrderID")
					emailSent = RS("emailSent")
					approved = RS("approved")
					cancelled = RS("cancelled")
					thub_posted_to_accounting = RS("thub_posted_to_accounting")
					thub_posted_date = RS("thub_posted_date")
					a = a + 1
					%>
					<tr>
						<td valign="top" align="center">
							<input type="hidden" name="OrderID<%=a%>" value="<%=OrderID%>"><%=showNothing(OrderID)%>
						</td>
						<td valign="top" align="center"><%=dateValue(RS("OrderDateTime"))%></td>
						<td valign="top" align="center"><%=showNothing(RS("fName")) & "&nbsp;" & showNothing(RS("lName"))%></td>
						<td valign="top" align="center"><%=formatCurrency(RS("ordergrandtotal"))%></td>
						<td valign="top" align="center">
							<input type="radio" name="cancelled<%=a%>" value="-1"<%if cancelled = true then response.write " checked"%>>Yes&nbsp;
							<input type="radio" name="cancelled<%=a%>" value="null"<%if cancelled = false or isNull(cancelled) then response.write " checked"%>>No&nbsp;
						</td>
						<td valign="top" align="center"><%=showNothing(RS("OrderNotes"))%></td>
						<td valign="top" align="center"><%if not isNull(thub_posted_to_accounting) then response.write "Yes" else response.write "No"%></td>
						<td valign="top" align="center"><%=showNothing(thub_posted_date)%></td>
					</tr>
					<%
					RS.movenext
				loop
				%>
				<tr>
					<td align="center" colspan="8">
						<input type="hidden" name="totalCount" value="<%=a%>">
						<input type="hidden" name="orderStart" value="<%=strOrderStartNumber%>">
						<input type="hidden" name="orderEnd" value="<%=strOrderEndNumber%>">
						<input type="submit" name="submitted2" value="Update">
					</td>
				</tr>
			</form>
		</table>
		<p>&nbsp;</p>
		<%
	end if
else
	%>
	<form name="frmSearchOrders" action="db_update_order_cancellations.asp" method="post">
		<p>Start Order Date:&nbsp;&nbsp;<input type="text" name="orderStart"></p>
		<p>End Order Date:&nbsp;&nbsp;<input type="text" name="orderEnd"></p>
		<p><input type="submit" name="submitted" value="Search"></p>
	</form>
	<%
end if
%>

</blockquote>
</blockquote>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
