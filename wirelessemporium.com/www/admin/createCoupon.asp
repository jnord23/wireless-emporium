<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Create Coupons"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim couponCnt : couponCnt = prepInt(request.Form("couponCnt"))
	dim couponValue : couponValue = prepInt(request.Form("couponValue"))
	dim couponDesc : couponDesc = prepStr(request.Form("couponDesc"))
	dim orderID : orderID = prepInt(request.Form("orderID"))
	dim numberSeg, uniqueSeq
	dim masterList : masterList = ""
	dim dateTime : dateTime = replace(replace(replace(now," ",""),"/",""),":","")
	
	if couponCnt > 0 then
		for i = 1 to couponCnt
			randomize
			uniqueSeq = 0
			do while uniqueSeq = 0
				numberSeg = ""
				for x = 1 to 16
					if x > 4 and x < 9 then
						numberSeg = numberSeg & chr(Int(Rnd * 24)+65)
					else
						numberSeg = numberSeg & Int(Rnd * 10)
					end if
				next
				sql = "select * from we_coupons where promoCode = '" & numberSeg & "'"
				session("errorSQL") = sql
				set testRS = oConn.execute(sql)
				
				if testRS.EOF then uniqueSeq = 1
			loop
			masterList = masterList & numberSeg & "##"
			sql = "insert into we_coupons (promoCode,promoMin,typeID,couponDesc,expiration,activate,oneTime,setValue,refundOrderID) values('" & numberSeg & "',0,0,'" & couponDesc & "','" & date + 365 & "',1,1," & couponValue & "," & orderID & ")"
			session("errorSQL") = sql
			oConn.execute(sql)
		next
	end if
%>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="500" style="padding-top:20px;">
	<tr><td align="center" style="font-weight:bold; font-size:18px; border-bottom:1px solid #000;">Single Use Coupon Creator</td></tr>
    <tr>
    	<td>
        	<form action="/admin/createCoupon.asp" method="post">
        	<div style="float:left; width:500px;">
	        	<div style="float:left; width:290px; text-align:right; font-weight:bold; padding:10px 10px 0px 0px;">Number of coupons to make:</div>
    	        <div style="float:left; width:200px; text-align:left; padding-top:10px;"><input type="text" name="couponCnt" size="4" value="1" /></div>
                <div style="float:left; width:290px; text-align:right; font-weight:bold; padding:10px 10px 0px 0px;">Coupon Value:</div>
    	        <div style="float:left; width:200px; text-align:left; padding-top:10px;"><input type="text" name="couponValue" size="4" value="10" /> USD</div>
                <div style="float:left; width:290px; text-align:right; font-weight:bold; padding:10px 10px 0px 0px;">Description:</div>
    	        <div style="float:left; width:200px; text-align:left; padding-top:10px;"><input type="text" name="couponDesc" size="20" value="" /> </div>
                <div style="float:left; width:290px; text-align:right; font-weight:bold; padding:10px 10px 0px 0px;">Refund OrderID:</div>
    	        <div style="float:left; width:200px; text-align:left; padding-top:10px;"><input type="text" name="orderID" size="20" value="" /> </div>
                <div style="float:left; width:500px; text-align:right; padding-top:10px; margin-top:10px; border-top:1px solid #000;"><input type="submit" name="mySub" value="Create Coupon(s)" /></div>
            </div>
            </form>
        </td>
    </tr>
    <%
	if masterList <> "" then
	%>
    <tr><td><a href="/admin/tempCSV/oneTimeCoupons_<%=dateTime%>.csv">Download CSV</a></td></tr>
    <tr>
    	<td>
        	<div style="height:200px; width:500px; overflow:scroll; border:1px solid #000;">
            	<div style="float:left; width:180px; text-align:left; border-bottom:1px solid #000; font-weight:bold; border-right:1px solid #000;">PromoCode</div>
                <div style="float:left; width:70px; text-align:left; border-bottom:1px solid #000; font-weight:bold; border-right:1px solid #000; padding-left:2px;">Amount</div>
                <div style="float:left; width:200px; text-align:left; border-bottom:1px solid #000; font-weight:bold; padding-left:2px;">Description</div>
    <%
		masterArray = split(masterList,"##")
		dim fs,tfile
		set fs=Server.CreateObject("Scripting.FileSystemObject")
		set tfile=fs.CreateTextFile(server.MapPath("/admin/tempCSV/oneTimeCoupons_" & dateTime & ".csv"))
		
		tfile.WriteLine("promoCode,Amount,Description")
		bgColor = "#fff"
		for i = 0 to (ubound(masterArray) - 1)
			tfile.WriteLine(masterArray(i) & "," & formatCurrency(couponValue,2) & "," & couponDesc)
			%>
            <div style="float:left; width:180px; text-align:left; border-bottom:1px solid #000; background-color:<%=bgColor%>; border-right:1px solid #000;"><%=masterArray(i)%></div>
            <div style="float:left; width:70px; text-align:left; border-bottom:1px solid #000; background-color:<%=bgColor%>; border-right:1px solid #000; padding-left:2px;"><%=formatCurrency(couponValue,2)%></div>
            <div style="float:left; width:200px; text-align:left; border-bottom:1px solid #000; background-color:<%=bgColor%>; padding-left:2px;"><%=couponDesc%></div>
            <%
			if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
		next
		
		tfile.close
		set tfile=nothing
		set fs=nothing
	%>
    		</div>
        </td>
    </tr>
    <%
	end if
	%>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
