<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Update Product Color Matrix"
	header = 1
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
sql = "select id, color, colorCodes, borderColor from xproductcolors order by 1"
session("errorSQL") = sql
arrColors = getDbRows(sql)
%>
<form name="frmColor" method="post">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="1000">
	<tr>
    	<td id="terryZone">&nbsp;</td>
    </tr>
	<tr>
    	<td><h1 style="margin:0px;">Update Product Color Matrix</h1></td>
    </tr>
    <tr>
    	<td style="font-size:11px; padding:10px 0px 10px 0px;">
        	This application allows the user to manipulate grouping of product colors
        </td>
    </tr>
    <tr>
    	<td style="padding-bottom:5px;">
            <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
                <tr>
                    <td width="150" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-off" id="id_header_0" onClick="showHide(0);">ADD/EDIT</td>
                    <td width="150" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-on" id="id_header_1" onClick="showHide(1);">SHOW ALL</td>
                    <td width="700" class="tab-header-off">&nbsp;</td>
                </tr>
            </table>
		</td>
    </tr>
    <%
	if retVal < 0 then
	%>
    <tr>
    	<td style="padding:5px; border:1px solid red;">
        	<h2>Error occurred:</h2>
	    	<h3><%=retMsg%></h3>
        </td>
	</tr>
    <%
	end if
	%>
    <tr>
    	<td width="100%" align="left">
	        <div id="tbl_0" style="display:none;">
                <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
                    <tr>
                        <td width="100%" align="left" style="border-bottom:1px dashed #ccc; padding-bottom:5px;">
                            <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
                                <tr>
                                    <td width="200" style="font-size:13px;">Current Plan Matrix Search: 
                                    <td width="800"><input type="text" id="id_Search_Current" name="txtSearchCurrent" value="Search By PartNumber or Master Item Description" onclick="this.value='';" style="color:#666; width:800px;" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" align="right"><a href="/admin/db_update_color_matrix.asp" style="font-size:14px;">Start New Plan</a></td>
                    </tr>
                    <tr>
                        <td width="100%" align="left">
                            <div width="100%" id="id_current">
                                <div id="id_new" style="float:left; margin:5px 0px 0px 4px; width:180px; height:290px; padding:5px; border:1px solid #333;">
                                    <div style="width:180px; font-weight:bold;"><a href="#" onclick="document.getElementById('id_new_types').style.display=''">Add new product here</a></div>
                                    <div id="id_new_types" style="width:180px; display:none;">
                                    <%
                                    sql = "select typeid, typename from we_types order by typeid"
                                    session("errorSQL") = sql
                                    arrTypes = getDbRows(sql)
                                    if not isnull(arrTypes) then
                                        %>
                                        <select name="cbNew" style="width:170px;" onChange="if (this.value) searchNewItem(this.value);">
                                            <option value="">Select category</option>
                                        <%
                                        for i=0 to ubound(arrTypes, 2)
                                            %>
                                            <option value="<%=arrTypes(0,i)%>"><%=arrTypes(1,i)%></option>
                                            <%
                                        next
                                        %>
                                        </select>
                                        <%
                                    end if
                                    %>
                                    </div>
                                    <div id="id_new_search" style="width:180px;">&nbsp;</div>
                                </div>
                            </div>
                        </td>
                    </tr>                
				</table>
            </div>
	        <div id="tbl_1" style="width:1000px; height:600px; border:1px solid #ccc; overflow:scroll;">
            	<%
				set fsThumb = CreateObject("Scripting.FileSystemObject")
				lap = 0
				sql	=	"select	a.master, a.master2, b.itemid, b.itemdesc, b.itempic, b.colorid, b.inv_qty, c.colorcodes, c.bordercolor, a.colorSlaves, a.cnt" & vbcrlf & _
						"from	v_colorMatrix a join we_items b" & vbcrlf & _
						"	on	a.master2 = b.partnumber left outer join xproductcolors c" & vbcrlf & _
						"	on	b.colorid = c.id" & vbcrlf & _
						"where	b.master = 1" & vbcrlf & _
						"order by 2"
				session("errorSQL") = sql
				set objRsAll = oConn.execute(sql)
				if not objRsAll.eof then
					do until objRsAll.eof
						lap = lap + 1
						master = objRsAll("master")
						master2 = objRsAll("master2")
						defaultItemID = objRsAll("itemid")
						itemdesc = objRsAll("itemdesc")
						itempic = objRsAll("itempic")
						colorid = objRsAll("colorid")
						colorCodes = objRsAll("colorCodes")
						borderColor = objRsAll("borderColor")
						colorSlaves = objRsAll("colorSlaves")
						numAvailColors = objRsAll("cnt")
						onSale = false
			
						itemImgPath = Server.MapPath("/productpics/thumb") & "\" & itemPic
						useImgPath = "/productpics/thumb/" & itemPic
						if not fsThumb.FileExists(itemImgPath) then useImgPath = "/productpics/thumb/imagena.jpg"
					
				%>
					<div style="float:left; margin:5px 0px 0px 4px; width:180px; height:265px; padding:5px; border:1px solid #ccc; font-size:12px;">
						<div id="id_productImage_<%=defaultItemID%>" style="width:150px; padding-left:30px;" align="left">
                        	<a href="javascript:void(0)" onClick="Edit('<%=master%>');return false;">
                            	<img src="<%=useImgPath%>" border="0" width="100" height="100" />
							</a>
						</div>
						<div style="width:180px; padding-top:10px;"><a target="_blank" href="/admin/db_update_items.asp?ItemID=<%=defaultItemID%>"><%=itemdesc%></a></div>
						<div style="width:180px; padding-top:5px; font-size:12px;">Default partnumber:<br /><%=master2%></div>
						<div align="center" style="width:160px; padding:10px;"><%=generateColorPicker(arrColors, defaultItemID, colorSlaves, colorid, numAvailColors, onSale)%></div>
					</div>
				<%
						objRsAll.movenext
					loop
				end if
				%>
            </div>
        </td>
    </tr>
</table>
</form>
<!-- JSON Autocomplete -->
<link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<style>
	.ui-autocomplete-loading { background: white url('/images/preloading16x16.gif') right center no-repeat; }
</style>
<script>
	$(document).ready(function() {
		
		$("#id_Search_Current").autocomplete({
			source: "/ajax/ajaxAutocompleteFilter.asp?sType=c1",
			minLength: 2,
			select: function( event, ui ) {pullData(ui);}
		});
	});
</script>
<!-- //JSON Autocomplete -->

<script>
	lastUpdatePartnumber = '';
	flipSuggestionBox = 'NO';
	function preloading(pItemID, selectedColorID){}
	function changeProduct(pItemID, selectedColorID, numColors, selectedIDX, onSale)
	{
		for(i=0; i < numColors; i++) 
			if (document.getElementById('div_colorOuterBox_' + pItemID + '_' + i) != null) document.getElementById('div_colorOuterBox_' + pItemID + '_' + i).className = 'colorBoxUnselected';

		document.getElementById('div_colorOuterBox_' + pItemID + '_' + selectedIDX).className = 'colorBoxSelected';

		ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmcd&uType=img&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&onsale=' + onSale, 'id_productImage_'+pItemID);
	}
	
	function Edit(masterPartnumber)
	{
		showHide(0);
		document.getElementById('id_current').innerHTML = '<img src="/images/loading.gif" border="0" />'
		ajax('/ajax/admin/ajaxProductColor.asp?uType=current&masterPartnumber=' + masterPartnumber, 'id_current');
	}
	
	function searchNewItem(typeid,isFlip)
	{
		if (isFlip != "") flipSuggestionBox = isFlip;
		document.getElementById('id_new_search').style.display = '';
		getNewAutosearch(typeid);
	}
	
	function getNewAutosearch(typeid)
	{
		document.getElementById('id_new_search').innerHTML = '<input type="text" id="id_new_txt_search" name="txtNewSearch" value="Search By PartNumber" onclick="this.value=\'\';" style="color:#666; width:175px;" />'
		
		$('#id_new_txt_search').autocomplete({
		source: "/ajax/ajaxAutocompleteFilter.asp?sType=c2&typeid="+typeid,
		minLength: 2,
		select: function( event, ui ) {getNewItem(ui);}
		});
		
		if (flipSuggestionBox == "YES")
		{
			$("#id_new_txt_search").autocomplete( "option", "position", { my : "right top", at: "right bottom" } );
		}
		
	}		
	
	function getNewItem(ui)
	{
		if(ui.item) ajax('/ajax/admin/ajaxProductColor.asp?uType=newitem&partnumber=' + ui.item.id, 'id_new');		
	}
	
	function pullData(ui)
	{
		var partnumber = '';
		if(ui.item)
		{
			partnumber = ui.item.id
			
			if (partnumber != "")
			{
				document.getElementById('id_current').innerHTML = '<img src="/images/loading.gif" border="0" />'
				ajax('/ajax/admin/ajaxProductColor.asp?uType=current&partnumber=' + partnumber, 'id_current');
			}
		}
	}
	
	function addNewItem()
	{
		document.getElementById('dumpZone').innerHTML = "";
		masterPartnumber = '';
		if (null != document.getElementById('id_hidMasterPartnumber')) masterPartnumber = document.frmColor.hidMasterPartnumber.value;
		slavePartnumber = document.frmColor.hidSlavePartnumber.value;
		colorid = document.frmColor.cbColor.value;

		if (colorid == "")
		{
			alert(" please select color");
			return false;
		}
		
		if (slavePartnumber != "")
		{
			 ajax('/ajax/admin/ajaxProductColor.asp?uType=addnewitem&partnumber=' + slavePartnumber + '&masterPartnumber=' + masterPartnumber + '&colorid=' + colorid, 'dumpZone');
			 lastUpdatePartnumber = slavePartnumber;
			 setTimeout('checkUpdate(\'addNewItem\')', 50);
		}
	}
	
	function cancelNewItem()
	{
		ajax('/ajax/admin/ajaxProductColor.asp?uType=getblank', 'id_new');			
	}
	
	function checkUpdate(checkType)
	{
		if (checkType == "addNewItem")
		{
			if ("" == document.getElementById('dumpZone').innerHTML)
			{
				setTimeout('checkUpdate(\'addNewItem\')', 50);
			}
			else if ("color exists" == document.getElementById('dumpZone').innerHTML)
			{
				alert('Please select another color');
			}
			else if ("partnumber exists" == document.getElementById('dumpZone').innerHTML)
			{
				alert('this partnumber has already been assigned');
			}			
			else if ("added" == document.getElementById('dumpZone').innerHTML)
			{
				document.getElementById('id_current').innerHTML = '<img src="/images/loading.gif" border="0" />'
				ajax('/ajax/admin/ajaxProductColor.asp?uType=current&partnumber=' + lastUpdatePartnumber, 'id_current');
			}
		}
		else if (checkType == "delete")
		{
			if ("" == document.getElementById('dumpZone').innerHTML)
			{
				setTimeout('checkUpdate(\'delete\')', 50);
			}
			else if ("deleted" == document.getElementById('dumpZone').innerHTML)
			{
				document.getElementById('id_current').innerHTML = '<img src="/images/loading.gif" border="0" />'
				ajax('/ajax/admin/ajaxProductColor.asp?uType=current&masterPartnumber=' + lastUpdatePartnumber, 'id_current');
			}
		}
		else if (checkType == "hidelive")
		{
			if ("" == document.getElementById('dumpZone').innerHTML)
			{
				setTimeout('checkUpdate(\'hidelive\')', 50);
			}
			else if ("updated" == document.getElementById('dumpZone').innerHTML)
			{
				// do nothing for now
			}
		}
	}
	
	function DeleteThis(masterPartnumber,slavePartnumber)
	{
		lastUpdatePartnumber = masterPartnumber;
		ajax('/ajax/admin/ajaxProductColor.asp?uType=delete&partnumber=' + slavePartnumber, 'dumpZone');
		setTimeout('checkUpdate(\'delete\')', 50);
	}
	
	function updateHidelive()
	{
		masterPartnumber = '';
		if (null != document.getElementById('id_hidMasterPartnumber')) masterPartnumber = document.frmColor.hidMasterPartnumber.value;
		if (masterPartnumber == "") 
		{
			alert('cannot update');
			return false;
		}
		
		var	chk_hl_we		=	document.frmColor.chk_hl_we.checked;
		var	chk_hl_co		=	document.frmColor.chk_hl_co.checked;
		var	chk_hl_ca		=	document.frmColor.chk_hl_ca.checked;
		var	chk_hl_ps		=	document.frmColor.chk_hl_ps.checked;
		var	chk_hl_tm		=	document.frmColor.chk_hl_tm.checked;								
		
		chk_hl_we	=	(chk_hl_we) ? 1:0;
		chk_hl_co	=	(chk_hl_co) ? 1:0;
		chk_hl_ca	=	(chk_hl_ca) ? 1:0;
		chk_hl_ps	=	(chk_hl_ps) ? 1:0;
		chk_hl_tm	=	(chk_hl_tm) ? 1:0;				

		param = 'uType=updatehidelive&masterPartnumber=' + masterPartnumber + '&chk_hl_we=' + chk_hl_we + '&chk_hl_co=' + chk_hl_co;
		param = param + '&chk_hl_ca=' + chk_hl_ca + '&chk_hl_ps=' + chk_hl_ps + '&chk_hl_tm=' + chk_hl_tm;

		ajax('/ajax/admin/ajaxProductColor.asp?' + param, 'dumpZone');
		setTimeout('checkUpdate(\'hidelive\')', 50);		
		
	}

	function showHide(showID)
	{
		for (i=0; i<=1; i++)
		{
			document.getElementById('id_header_' + i).className = "tab-header-off";
			document.getElementById('tbl_' + i).style.display = "none";			
		}
				
		document.getElementById('id_header_' + showID).className = "tab-header-on";
		document.getElementById('tbl_' + showID).style.display = "";
	}
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->