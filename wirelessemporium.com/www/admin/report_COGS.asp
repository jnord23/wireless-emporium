<%
response.buffer = false
pageTitle = "Create COGS Report CSV"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
Server.ScriptTimeout = 2000 'seconds

dim Upload, path
set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = true
path = server.mappath("tempCSV")
Count = Upload.Save(path)

if Upload.form("upload") <> "" then
	if Count = 0 then
		response.write "No file selected."
		response.end
	else
		dim filesys, file, filename, WriteFile
		set filesys = Server.CreateObject("Scripting.FileSystemObject")
		set File = Upload.Files(1)
		myFile = File.path
		
		filename = "COGS.csv"
		set WriteFile = filesys.CreateTextFile(path & "\" & filename)
		
		dim txtConn, myStr
		set txtConn = Server.CreateObject("ADODB.Connection")
		myStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & myFile & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"";"
		txtConn.ConnectionString = myStr
		txtConn.ConnectionTimeout = 1000		'seconds
		txtConn.Open
		
		SQL = "SELECT * FROM [Sheet1$]"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, txtConn, 3, 3
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			writefile.WriteLine "PartNumber,COGS"
			%>
			<h3><%=RS.recordcount%> records found</h3>
			<%
			dim PartNumber, strWriteLine
			do until RS.eof
				PartNumber = left(RS("PartNumber"),15)
				SQL = "SELECT COGS FROM we_items WHERE PartNumber = '" & PartNumber & "' AND inv_qty >= 0 ORDER BY inv_qty DESC"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				strWriteLine = PartNumber & ","
				if not RS2.eof then strWriteLine = strWriteLine & RS2("COGS")
				writefile.WriteLine strWriteLine
				RS.movenext
			loop
		end if
		response.write "<p>Download your file <a href=""/admin/tempCSV/" & filename & """>HERE</a>.</p>" & vbcrlf
		
		writefile.close()
		set writefile = nothing
		set filesys = nothing
		set File = nothing
		
		RS.close
		set RS = nothing
		RS2.close
		set RS2 = nothing
		txtConn.close
		set txtConn = nothing
	end if
else
	%>
	<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
		<tr bgcolor="#CCCCCC">
			<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
		</tr>
		<tr>
			<td width="47%" valign="middle">
				<table width="100%" border="0" cellspacing="0" cellpadding="15">
					<tr>
						<td class="normalText">
							<form enctype="multipart/form-data" action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="POST">
								<p><%=pageTitle%></p>
								<p>
									Upload your file:
									<ul style="margin-top:0px;">
										<li>MUST be in 97-2003 XLS format</li>
										<li>Worksheet MUST be named "Sheet1"</li>
										<li>Column MUST be named "PartNumber"</li>
									</ul>
								</p>
								<p><input type="FILE" name="FILE1"></p>
								<p><input type="SUBMIT" name="upload" value="Upload"></p>
							</form>
							<p>&nbsp;</p>
							<p>&nbsp;</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr bgcolor="#CCCCCC">
			<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
		</tr>
	</table>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
