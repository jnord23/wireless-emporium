<%
pageTitle = "New Sales Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<link href="/includes/css/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/includes/js/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/includes/js/cal.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function () {
		$('#id_sdate').simpleDatepicker();
		$('#id_edate').simpleDatepicker();
	});
</script>
<%
searchExcel = prepStr(request.form("searchExcel"))
if searchExcel <> "" then response.redirect "/admin/report_Sales2_Excel.asp?" & request.form

formData = request.form
if formData = "" then formData = "txtSDate=" & Date & "&txtEDate=" & Date & "&cbGroupBy=Date&cbGroupBy=Store"
strSDate = prepStr(request.form("txtSDate"))
strEDate = prepStr(request.form("txtEDate"))
siteid = prepStr(request.form("cbSite"))
brandid = prepInt(request.form("cbBrand"))
modelid = prepInt(request.form("cbModel"))
typeid = prepInt(request.form("cbType"))
strGroupBy = prepStr(request.form("cbGroupBy"))
partnumber = prepStr(request.form("txtPartnumber"))
shiptype = prepStr(request.form("cbShiptype"))
addressDiff = prepStr(request.form("addressDiff"))
orderType = prepInt(request.form("cbOrderType"))

if strSDate = "" or not isdate(strSDate) then strSDate = Date
if strEDate = "" or not isdate(strEDate) then strEDate = Date

Dim cfgArrGroupBy : cfgArrGroupBy = Array("Date","Store","Brand","Model","Category","PartNumber","OrderID","ItemID","Details")
if "" = strGroupBy then 
	strGroupBy = "Date,Store,,,,,,," 
end if
arrGroupBy = split(strGroupBy, ",")

dim strSqlSelectMain, strSqlWhere, strSqlFrom, strSqlGroupBy, strSqlOrderBy, strHeadings
strSqlSelectMain	=	""
strSqlWhere			=	""
strSqlFrom			=	""
strSqlGroupBy		=	""
strSqlOrderBy		=	""
strHeadings			=	""

Dim bDate, bStore, bBrand, bModel, bCategory, bPartnumber, bItemid, bDetails, bOrderID
bDate		=	false
bStore		=	false
bBrand		=	false
bModel		=	false
bCategory	=	false
bPartnumber	=	false
bItemid		=	false
bDetails	=	false
bOrderID	=	false

For i = 0 To Cint(UBound(arrGroupBy))
	If "" <> trim(arrGroupBy(i)) Then
		Select Case ucase(trim(arrGroupBy(i)))
			Case "DATE"
				If bDate Then
					Response.Write "<Script>alert('DATE in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bDate = true

				strSqlSelectMain	=	strSqlSelectMain 	& 	", convert(varchar(10), a.orderdatetime, 20) logDate"
				strSqlGroupBy		=	strSqlGroupBy		&	"convert(varchar(10), a.orderdatetime, 20), "
				strSqlOrderBy		=	strSqlOrderBy		&	"convert(varchar(10), a.orderdatetime, 20), "
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:92px; font-size:13px; font-weight:bold; cursor:pointer; position:relative;"" onclick=""doSort('convert(varchar(10), a.orderdatetime, 20)',this);"" title=""Order Date"">Order Date</div>"

			Case "STORE"
				If bStore Then
					Response.Write "<Script>alert('STORE in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bStore = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.store"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.store, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.store, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:42px; font-size:13px; font-weight:bold; cursor:pointer; position:relative;"" onclick=""doSort('a.store',this);"" title=""SITE"">SITE</div>"

			Case "BRAND"
				If bBrand Then
					Response.Write "<Script>alert('BRAND in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bBrand = true
				
				strSqlFrom			=	strSqlFrom			&	"left outer join we_brands e on c.brandid = e.brandid" & vbcrlf
				strSqlSelectMain	=	strSqlSelectMain 	& 	", isnull(e.brandName, 'Universal')"
				strSqlGroupBy		=	strSqlGroupBy		&	"isnull(e.brandName, 'Universal'), "
				strSqlOrderBy		=	strSqlOrderBy		&	"isnull(e.brandName, 'Universal'), "
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:120px; font-size:13px; font-weight:bold; cursor:pointer; position:relative;"" onclick=""doSort('isnull(e.brandName, \'Universal\')',this);"" title=""Brand"">Brand</div>"

			Case "MODEL"
				If bModel Then
					Response.Write "<Script>alert('Model in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bModel = true

				strSqlFrom			=	strSqlFrom			&	"left outer join we_models f on c.modelid = f.modelid" & vbcrlf
				strSqlSelectMain	=	strSqlSelectMain 	& 	", isnull(f.modelName, 'Universal')"
				strSqlGroupBy		=	strSqlGroupBy		&	"isnull(f.modelName, 'Universal'), "
				strSqlOrderBy		=	strSqlOrderBy		&	"isnull(f.modelName, 'Universal'), "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:252px; font-size:13px; font-weight:bold; cursor:pointer; position:relative;"" onclick=""doSort('isnull(f.modelName, \'Universal\')',this);"" title=""Model"">Model</div>"

			Case "CATEGORY"
				If bCategory Then
					Response.Write "<Script>alert('Category in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bCategory = true

				strSqlFrom			=	strSqlFrom			&	"join we_types d on c.typeid = d.typeid" & vbcrlf
				strSqlSelectMain	=	strSqlSelectMain 	& 	", d.typename"
				strSqlGroupBy		=	strSqlGroupBy		&	"d.typename, "
				strSqlOrderBy		=	strSqlOrderBy		&	"d.typename, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:152px; font-size:13px; font-weight:bold; cursor:pointer; position:relative;"" onclick=""doSort('d.typename',this);"" title=""Category"">Category</div>"

			Case "PARTNUMBER"
				If bPartnumber Then
					Response.Write "<Script>alert('Partnumber in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bPartnumber = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", c.partnumber"
				strSqlGroupBy		=	strSqlGroupBy		&	"c.partnumber, "
				strSqlOrderBy		=	strSqlOrderBy		&	"c.partnumber, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:122px; font-size:13px; font-weight:bold; cursor:pointer; position:relative;"" onclick=""doSort('c.partnumber',this);"" title=""Part Number"">Part Number</div>"

			Case "ITEMID"
				If bItemID Then
					Response.Write "<Script>alert('ItemID in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bItemID = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", c.itemid"
				strSqlGroupBy		=	strSqlGroupBy		&	"c.itemid, "
				strSqlOrderBy		=	strSqlOrderBy		&	"c.itemid, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:72px; font-size:13px; font-weight:bold; cursor:pointer; position:relative;"" onclick=""doSort('c.itemid',this);"" title=""Item ID"">Item ID</div>"

			Case "ORDERID"
				If bOrderID Then
					Response.Write "<Script>alert('orderID in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bOrderID = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.orderid"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.orderid, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.orderid, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:82px; font-size:13px; font-weight:bold; cursor:pointer; position:relative;"" onclick=""doSort('a.orderid',this);"" title=""order ID"">Order ID</div>"				

			Case "DETAILS"
				bDetails = true			
		End Select
	End If
Next
%>
<center>
<form method="post" name="frmSales">
    <div style="text-align:left; width:1100px;">
        <div style="padding-bottom:10px; height:20px;">
        	<div style="font-size:18px; font-weight:bold; float:left;">New Sales Report</div>
            <div style="font-size:11px; float:right;">* The result with more than <span style="color:red;">3,000</span> rows will automatically be exported to Excel.</div>
		</div>
        <!-- filters-->
        <div style="width:100%; border:1px solid #ccc; border-radius:3px; background-color:#f4f7f9; padding:5px;">
            <div style="width:100%; height:30px;">
				<div class="filter-box-header" style="width:50px;">Filter</div>
                <div class="filter-box-header" style="width:180px;">Date</div>
                <div class="filter-box-header" style="width:60px;">Store</div>
                <div class="filter-box-header" style="width:70px;">Brand</div>
                <div class="filter-box-header" style="width:70px;">Model</div>
                <div class="filter-box-header" style="width:80px;">Category</div>
                <div class="filter-box-header" style="width:90px;" title="Use the asterik (*) as a wildcard to search for partial matches (i.e. WCD-* for Custom Case Designs or FPA-*-A01 for Artist Cases by A01) ">Partnumber</div>
                <div class="filter-box-header" style="width:70px;">ShipType</div>
                <div class="filter-box-header" style="width:80px;">OrderType</div>
                <div class="filter-box-header" style="width:130px;">Action</div>
            </div>
            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%; padding:5px 0px 5px 0px; height:25px;">
				<div class="filter-box-header" style="width:50px;">Value</div>
                <div class="filter-box" style="width:180px;"><input type="text" id="id_sdate" name="txtSDate" value="<%=strSDate%>" size="7"> ~ <input type="text" id="id_edate" name="txtEDate" value="<%=strEDate%>" size="7"></div>
                <div class="filter-box" style="width:60px;">
                	<select name="cbSite">
                    	<option value="">ALL</option>
                    	<option value="0" <%if siteid = "0" then%>selected<%end if%>>WE</option>
                    	<option value="1" <%if siteid = "1" then%>selected<%end if%>>CA</option>
                    	<option value="2" <%if siteid = "2" then%>selected<%end if%>>CO</option>
                    	<option value="3" <%if siteid = "3" then%>selected<%end if%>>PS</option>
                    	<option value="10" <%if siteid = "10" then%>selected<%end if%>>TM</option>
                    </select>
                </div>
                <div class="filter-box" style="width:70px;">
					<%
                    sql = "select brandid, brandName from we_brands order by 2"
                    set rsBrand = oConn.execute(sql)
                    %>
                    <select name="cbBrand" style="width:65px;">
                        <option value="">ALL</option>
                    <%
                    do until rsBrand.eof
                        %>
                        <option value="<%=rsBrand("brandid")%>" <%if brandid = rsBrand("brandid") then%>selected<% end if%>><%=rsBrand("brandName")%></option>
                        <%
                        rsBrand.movenext
                    loop
                    %>
                    </select>
                </div>
                <div class="filter-box" style="width:70px;">
					<%
                    sql = 	"select	modelid, modelname" & vbcrlf & _
                            "from	we_models" & vbcrlf & _
                            "where	hidelive = 0" & vbcrlf & _
                            "order by 2"
                    set rsModel = oConn.execute(sql)
                    %>
                    <select name="cbModel" style="width:65px;">
                        <option value="">ALL</option>
                    <%
                    do until rsModel.eof
                        %>
                        <option value="<%=rsModel("modelid")%>" <%if modelid = rsModel("modelid") then%>selected<%end if%>><%=rsModel("modelname")%></option>
                        <%
                        rsModel.movenext
                    loop
                    %>
                    </select>
                </div>
                <div class="filter-box" style="width:80px;">
					<%
                    sql = 	"select	typeid, typename" & vbcrlf & _
                            "from	we_types" & vbcrlf & _
                            "order by 2"
                    set rsType = oConn.execute(sql)
                    %>
                    <select name="cbType" style="width:75px;">
                        <option value="">ALL</option>
                    <%
                    do until rsType.eof
                        %>
                        <option value="<%=rsType("typeid")%>" <%if typeid = rsType("typeid") then%>selected<%end if%>><%=rsType("typename")%></option>
                        <%
                        rsType.movenext
                    loop
                    %>
                    </select>                
                </div>
                <div class="filter-box" style="width:90px;"><input type="text" name="txtPartnumber" value="<%=partnumber%>" style="width:80px;" /></div>
                <div class="filter-box" style="width:70px;">
					<%
                    sql = 	"select * from xshiptype order by 2"
                    set rsShiptype = oConn.execute(sql)
                    %>
                    <select name="cbShiptype" style="width:60px;">
                        <option value="">ALL</option>
                    <%
                    do until rsShiptype.eof
                        %>
                        <option value="<%=rsShiptype("shipDesc")%>" <%if replace(shiptype, "''", "'") = rsShiptype("shipDesc") then%>selected<%end if%>><%=rsShiptype("shipDesc")%></option>
                        <%
                        rsShiptype.movenext
                    loop
                    %>
                    </select>                
                </div>
                <div class="filter-box" style="width:80px;">
                <%
                    sql = 	"select -1 typeid, 'Credit Card' orderdesc" & vbcrlf & _
							"union" & vbcrlf & _
							"select typeid, orderdesc from xordertype "
                    set rsOrderType = oConn.execute(sql)
                    %>
                    <select name="cbOrderType" style="width:70px;">
                        <option value="">ALL</option>
                    <%
                    do until rsOrderType.eof
                        %>
                        <option value="<%=rsOrderType("typeid")%>" <%if orderType = rsOrderType("typeid") then%>selected<%end if%>><%=rsOrderType("orderdesc")%></option>
                        <%
                        rsOrderType.movenext
                    loop
                    %>
                    </select>
                </div>
                <div class="filter-box" style="width:130px;">
	                <input type="submit" name="search" value="Search" />
	                <input type="submit" name="searchExcel" value="Excel" />
                </div>
            </div>

            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%; padding-top:5px; height:25px;">            
				<div class="filter-box-header" style="width:80px;">Group By</div>
				<div class="filter-box" style="width:600px; padding-top:3px; text-align:left;" align="left">
                <%
				if not isnull(cfgArrGroupBy) then
					for i=0 to 4
						response.write "<select name=""cbGroupBy""><option value="""" "
						if "" = trim(arrGroupBy(i)) then response.write " selected " end if
						response.write ">--</option>"
						
						for j=0 to ubound(cfgArrGroupBy,1)
							response.write "<option value=""" & cfgArrGroupBy(j) & """ "
							if ucase(trim(arrGroupBy(i))) = ucase(trim(cfgArrGroupBy(j))) then response.write " selected " end if
							response.write ">" & trim(cfgArrGroupBy(j)) & "</option>"
						next
						response.write "</select>&nbsp;"
					next
				end if
				%>
                </div>
				<div class="filter-box-header" style="width:120px;">Address Diff. <input type="checkbox" name="addressDiff" value="Y" <%if addressDiff = "Y" then%>checked="checked"<%end if%> /></div>
            </div>

        </div>
		<!--// filters-->
        <!-- result -->
		<div style="width:100%; padding-top:10px;">
        <%
		strSqlWhere = ""
		if siteid <> "" 		then strSqlWhere = strSqlWhere & "	and	a.store = " & siteid & vbcrlf
		if brandid <> 0			then strSqlWhere = strSqlWhere & "	and	c.brandid = " & brandid & vbcrlf
		if modelid <> 0			then strSqlWhere = strSqlWhere & "	and	c.modelid = " & modelid & vbcrlf
		if typeid <> 0			then strSqlWhere = strSqlWhere & "	and	c.typeid = " & typeid & vbcrlf
		if orderType <> 0		then 
			if orderType = -1 then 
				strSqlWhere = strSqlWhere & "	and	a.extordertype is null" & vbcrlf
			else
				strSqlWhere = strSqlWhere & "	and	a.extordertype = " & orderType & vbcrlf
			end if
		end if
		if partnumber <> ""		then
			if instr(partnumber, "*") > 0 then
				strSqlWhere = strSqlWhere & "	and	c.partnumber like '" & replace(partnumber, "*", "%") & "'" & vbcrlf
			else
				strSqlWhere = strSqlWhere & "	and	c.partnumber = '" & partnumber & "'" & vbcrlf
			end if
		end if
		if shiptype <> ""		then strSqlWhere = strSqlWhere & "	and	a.shiptype = '" & shiptype & "'" & vbcrlf
		if addressDiff <> ""	then strSqlWhere = strSqlWhere & "	and	v.baddress1 <> v.saddress1 and v.szip <> v.bzip" & vbcrlf
		
		if bDetails then
			strSqlOrderBy = "order by 1, 2 desc"
			sql	=	"select	a.store, a.orderid, convert(varchar(10), a.orderdatetime, 20) orderdatetime, c.partnumber, c.itemid, c.itemdesc, isnull(b.quantity, 0) quantity" & vbcrlf & _
					"	,	isnull(c.cogs, 0) cogs" & vbcrlf & _
					"	,	isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
					"					when a.store = 1 then c.price_ca" & vbcrlf & _
					"					when a.store = 2 then c.price_co" & vbcrlf & _
					"					when a.store = 3 then c.price_ps" & vbcrlf & _
					"					when a.store = 10 then" & vbcrlf & _
					"						case when c.price_er > 0 then c.price_er" & vbcrlf & _
					"							else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
					"						end " & vbcrlf & _
					"					else c.price_our" & vbcrlf & _
					"				end, 0) price_our" & vbcrlf & _
					"	,	isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
					"					when a.store = 1 then c.price_ca" & vbcrlf & _
					"					when a.store = 2 then c.price_co" & vbcrlf & _
					"					when a.store = 3 then c.price_ps" & vbcrlf & _
					"					when a.store = 10 then" & vbcrlf & _
					"						case when c.price_er > 0 then c.price_er" & vbcrlf & _
					"							else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
					"						end " & vbcrlf & _
					"					else c.price_our" & vbcrlf & _
					"				end, 0) * isnull(b.quantity, 0) linePriceTotal" & vbcrlf & _
					"	,	(isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
					"					when a.store = 1 then c.price_ca" & vbcrlf & _
					"					when a.store = 2 then c.price_co" & vbcrlf & _
					"					when a.store = 3 then c.price_ps" & vbcrlf & _
					"					when a.store = 10 then" & vbcrlf & _
					"						case when c.price_er > 0 then c.price_er" & vbcrlf & _
					"							else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
					"						end " & vbcrlf & _
					"					else c.price_our" & vbcrlf & _
					"				end, 0) * isnull(b.quantity, 0)) - (isnull(c.cogs, 0) * isnull(b.quantity, 0)) revenue" & vbcrlf & _
					"from	we_orders a join we_orderdetails b" & vbcrlf & _
					"	on	a.orderid = b.orderid join we_items c" & vbcrlf & _
					"	on	b.itemid = c.itemid join v_accounts v" & vbcrlf & _
					"	on	a.accountid = v.accountid and a.store = v.site_id" & vbcrlf & _
					"where	a.orderdatetime >= '" & strSDate & "'" & vbcrlf & _
					"	and	a.orderdatetime < '" & dateadd("d", 1, strEDate) & "'" & vbcrlf & _
					"	and	a.approved = 1 and (a.cancelled = 0 or a.cancelled is null)" & vbcrlf & _
					"	and	b.returned = 0" & vbcrlf & _
					strSqlWhere & vbcrlf & _
					strSqlOrderBy
'			response.write "<pre>" & sql & "</pre>"
'			response.end
			session("errorSQL") = sql
			set rs = oConn.execute(sql)
			if rs.eof then
				response.write "No data to display"
			else
				lap = 0
				do until rs.eof
					lap = lap + 1
					rs.movenext
				loop
				if lap > 3000 then 
					response.redirect "/admin/report_Sales2_Excel.asp?" & request.form
					response.End
				end if
				rs.movefirst
				%>
				<div style="width:100%; height:30px; border:1px solid #c1e0b2; border-radius:3px; background-color:#eaf5e5; padding:5px;">
					<div style="float:left; width:42px; font-size:13px; font-weight:bold; position:relative; cursor:pointer;" onclick="doSort('a.store',this);" title="SITE">SITE</div>
					<div style="float:left; width:92px; font-size:13px; font-weight:bold; position:relative; cursor:pointer;" onclick="doSort('a.orderdatetime',this);" title="Order Date">Order Date</div>
					<div style="float:left; width:92px; font-size:13px; font-weight:bold; position:relative; cursor:pointer;" onclick="doSort('a.orderid',this);" title="ORDERID">ORDERID</div>
					<div style="float:left; width:122px; font-size:13px; font-weight:bold; position:relative; cursor:pointer;" onclick="doSort('c.partnumber',this);" title="PART NUMBER">PART NUMBER</div>
					<div style="float:left; width:62px; font-size:13px; font-weight:bold; position:relative; cursor:pointer;" onclick="doSort('c.itemid',this);" title="ITEMID">ITEMID</div>
					<div style="float:left; width:322px; font-size:13px; font-weight:bold; position:relative; cursor:pointer;" onclick="doSort('c.itemdesc',this);" title="Item Desc">Item Desc</div>
					<div style="float:left; width:42px; font-size:13px; font-weight:bold; position:relative; cursor:pointer;" onclick="doSort('b.quantity',this);" title="QTY">QTY</div>
					<div style="float:left; width:72px; font-size:13px; font-weight:bold; position:relative; cursor:pointer;" onclick="doSort('price_our',this);" title="Price">Price</div>
					<div style="float:left; width:82px; font-size:13px; font-weight:bold;"><div class="filter-box-header" style="cursor:pointer; margin:0px; width:65px; position:relative;" onclick="doSort('linePriceTotal',this);" title="Total">Total</div></div>
					<div style="float:left; width:82px; font-size:13px; font-weight:bold;"><div class="filter-box-header" style="cursor:pointer; margin:0px; width:65px; position:relative;" onclick="doSort('isnull(c.cogs, 0)*isnull(b.quantity, 0)',this);" title="COST">COST</div></div>
					<div style="float:left; width:82px; font-size:13px; font-weight:bold;"><div class="filter-box-header" style="cursor:pointer; margin:0px; width:75px; position:relative;" onclick="doSort('revenue',this);" title="Revenue">Revenue</div></div>
				</div>                
                <div id="searchResult">
				<%
				lap = 0
				priceTotal = cdbl(0)
				cogsTotal = cdbl(0)
				qtyTotal = clng(0)
				revenueTotal = cdbl(0)
				do until rs.eof
					lap = lap + 1			
					rowBackgroundColor = "#fff"
					if (lap mod 2) = 0 then rowBackgroundColor = "#eaf5fa"
					
					select case rs("store")
						case 0 : site = "WE"
						case 1 : site = "CA"
						case 2 : site = "CO"
						case 3 : site = "PS"
						case 10 : site = "TM"
					end select

					qty = clng(rs("quantity"))
					price_our = cdbl(rs("price_our"))
					linePriceTotal = cdbl(rs("price_our")) * qty
					lineCogsTotal = cdbl(rs("cogs")) * qty
					revenue = cdbl(rs("revenue"))
					
					priceTotal = priceTotal + linePriceTotal
					cogsTotal = cogsTotal + lineCogsTotal
					qtyTotal = qtyTotal + qty
					revenueTotal = revenueTotal + revenue
					
					itemdesc = rs("itemdesc")
					if len(itemdesc) > 55 then itemdesc = left(itemdesc, 52) & "..."
					

					%>
				<div class="table-row" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='1px solid <%=rowBackgroundColor%>';this.style.backgroundColor='<%=rowBackgroundColor%>'">
					<div class="table-cell" style="width:40px;"><%=site%>&nbsp;</div>
					<div class="table-cell" style="width:90px;"><%=rs("orderdatetime")%>&nbsp;</div>
					<div class="table-cell" style="width:90px;"><%=rs("orderid")%>&nbsp;</div>
					<div class="table-cell" style="width:120px;"><%=rs("partnumber")%>&nbsp;</div>
					<div class="table-cell" style="width:60px;"><%=rs("itemid")%>&nbsp;</div>
					<div class="table-cell" style="width:320px;" title="<%=rs("itemdesc")%>"><%=itemdesc%>&nbsp;</div>
					<div class="table-cell" style="width:40px;"><%=formatnumber(qty, 0)%>&nbsp;</div>
					<div class="table-cell" style="width:70px;"><%=formatcurrency(price_our)%>&nbsp;</div>
					<div class="table-cell" style="width:80px;" align="right"><%=formatcurrency(linePriceTotal)%>&nbsp;&nbsp;</div>
					<div class="table-cell" style="width:80px;" align="right"><%=formatcurrency(lineCogsTotal)%>&nbsp;&nbsp;</div>
					<div class="table-cell" style="width:80px; color:#060;" align="right"><%=formatcurrency(revenue)%>&nbsp;&nbsp;</div>
				</div>
					<%
					if (lap mod 300) = 0 then response.flush
					rs.movenext
				loop
				%>
				<div class="table-row" style="background-color:#fff; border-top:1px solid #ccc;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='';this.style.borderTop='1px solid #ccc';this.style.backgroundColor='#fff'">
					<div class="table-cell" style="width:720px; padding-right:12px; font-weight:bold;" align="right">Total&nbsp;</div>
					<div class="table-cell" style="width:40px; font-weight:bold;"><%=formatnumber(qtyTotal, 0)%>&nbsp;</div>
					<div class="table-cell" style="width:70px; font-weight:bold;">&nbsp;</div>
					<div class="table-cell" style="width:80px; font-weight:bold;" align="right"><%=formatcurrency(priceTotal)%>&nbsp;&nbsp;</div>
					<div class="table-cell" style="width:80px; font-weight:bold;" align="right"><%=formatcurrency(cogsTotal)%>&nbsp;&nbsp;</div>
					<div class="table-cell" style="width:80px; font-weight:bold; color:#060;" align="right"><%=formatcurrency(revenueTotal)%>&nbsp;&nbsp;</div>
				</div>
			</div>
                <%
			end if
		else
			if "" <> strSqlGroupBy then
				strSqlGroupBy	=	"group by " & left(trim(strSqlGroupBy), len(trim(strSqlGroupBy))-1)
				strSqlOrderBy	=	"order by " & left(trim(strSqlOrderBy), len(trim(strSqlOrderBy))-1)
			end if
			
			sql	=	"select	sum(isnull(b.quantity, 0)) quantity" & vbcrlf & _
					"	,	sum(isnull(b.quantity, 0) * isnull(c.cogs, 0)) cogs" & vbcrlf & _
					"	,	sum(isnull(b.quantity, 0) * isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
					"												when a.store = 1 then c.price_ca" & vbcrlf & _
					"												when a.store = 2 then c.price_co" & vbcrlf & _
					"												when a.store = 3 then c.price_ps" & vbcrlf & _
					"												when a.store = 10 then" & vbcrlf & _
					"													case when c.price_er > 0 then c.price_er" & vbcrlf & _
					"														else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
					"													end " & vbcrlf & _
					"												else c.price_our" & vbcrlf & _
					"											end, 0)) price_our" & vbcrlf & _
					"	,	sum(isnull(b.quantity, 0) * isnull(case when a.store = 0 then c.price_our" & vbcrlf & _
					"												when a.store = 1 then c.price_ca" & vbcrlf & _
					"												when a.store = 2 then c.price_co" & vbcrlf & _
					"												when a.store = 3 then c.price_ps" & vbcrlf & _
					"												when a.store = 10 then" & vbcrlf & _
					"													case when c.price_er > 0 then c.price_er" & vbcrlf & _
					"														else case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
					"													end " & vbcrlf & _
					"												else c.price_our" & vbcrlf & _
					"											end, 0)) - sum(isnull(b.quantity, 0) * isnull(c.cogs, 0)) revenue" & vbcrlf & _
					strSqlSelectMain & vbcrlf & _
					"from	we_orders a join we_orderdetails b" & vbcrlf & _
					"	on	a.orderid = b.orderid join we_items c" & vbcrlf & _
					"	on	b.itemid = c.itemid join v_accounts v" & vbcrlf & _
					"	on	a.accountid = v.accountid and a.store = v.site_id" & vbcrlf & _
					strSqlFrom & _
					"where	a.orderdatetime >= '" & strSDate & "'" & vbcrlf & _
					"	and	a.orderdatetime < '" & dateadd("d", 1, strEDate) & "'" & vbcrlf & _
					"	and	a.approved = 1 and (a.cancelled = 0 or a.cancelled is null)" & vbcrlf & _
					"	and	b.returned = 0" & vbcrlf & _
					strSqlWhere & vbcrlf & _
					strSqlGroupBy & vbcrlf & _					
					strSqlOrderBy
'			response.write "<pre>" & sql & "</pre>"
			session("errorSQL") = sql
			arrResult = getDbRows(sql)
			%>
            <div style="width:100%; height:30px; border:1px solid #c1e0b2; border-radius:3px; background-color:#eaf5e5; padding:5px;">
            	<%=strHeadings%>
                <div style="float:left; width:62px; font-size:13px; font-weight:bold; cursor:pointer; position:relative;" onclick="doSort('quantity',this);" title="QTY">QTY</div>
                <div style="float:left; width:112px; font-size:13px; font-weight:bold;"><div class="filter-box-header" style="cursor:pointer; position:relative; margin:0px; width:95px;" onclick="doSort('price_our',this);" title="Total Price">Total Price</div></div>
                <div style="float:left; width:112px; font-size:13px; font-weight:bold;"><div class="filter-box-header" style="cursor:pointer; position:relative; margin:0px; width:95px;" onclick="doSort('cogs',this);" title="Total COST">Total COST</div></div>
                <div style="float:left; width:112px; font-size:13px; font-weight:bold;"><div class="filter-box-header" style="cursor:pointer; position:relative; margin:0px; width:105px;" onclick="doSort('revenue',this);" title="Total Revenue">Total Revenue</div></div>
            </div>
            <div id="searchResult">
	        <%
			qtyTotal = clng(0)
			priceTotal = cdbl(0)
			cogsTotal = cdbl(0)
			revenueTotal = cdbl(0)
			if not isnull(arrResult) then
				for nRow=0 to ubound(arrResult,2)
					rowBackgroundColor = "#eaf5fa"
					if (nRow mod 2) = 0 then rowBackgroundColor = "#fff"				
					if (nRow mod 300) = 0 then response.Flush()
					
					qty = clng(arrResult(0,nRow))
					linePriceTotal = cdbl(arrResult(2,nRow))
					lineCogsTotal = cdbl(arrResult(1,nRow))
					revenue = cdbl(arrResult(3,nRow))
					
					qtyTotal = qtyTotal + qty								'quantity
					priceTotal = priceTotal + linePriceTotal				'priceTotal
					cogsTotal = cogsTotal + lineCogsTotal					'cogsTotal
					revenueTotal = revenueTotal + revenue
				%>
				<div class="table-row" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='1px solid <%=rowBackgroundColor%>';this.style.backgroundColor='<%=rowBackgroundColor%>'">
                <%
					nSkipColumn = 4
					totalWidth = 0
					for i=0 to ubound(arrGroupBy)
						if "" <> trim(arrGroupBy(i)) then
							select case ucase(trim(arrGroupBy(i)))
								case "DATE"
									totalWidth = totalWidth + 93
								%>
                                    <div class="table-cell" style="width:90px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
                                <%
								Case "STORE"
									totalWidth = totalWidth + 42
									select case cint(arrResult(nSkipColumn,nRow))
										case 0 : site = "WE"
										case 1 : site = "CA"
										case 2 : site = "CO"
										case 3 : site = "PS"
										case 10 : site = "TM"
									end select
									%>
                                    <div class="table-cell" style="width:40px;"><%=site%>&nbsp;</div>
                                    <%
								Case "BRAND"
									totalWidth = totalWidth + 122
								%>
                                    <div class="table-cell" style="width:120px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
                                <%
								Case "MODEL"
									totalWidth = totalWidth + 252
									modelDesc = arrResult(nSkipColumn,nRow)
									if len(modelDesc) > 40 then modelDesc = left(modelDesc, 37) & "..."
								%>
                                    <div class="table-cell" style="width:250px;" title="<%=arrResult(nSkipColumn,nRow)%>"><%=modelDesc%>&nbsp;</div>
                                <%
								Case "CATEGORY"
									totalWidth = totalWidth + 152
								%>
                                    <div class="table-cell" style="width:150px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
                                <%
								Case "PARTNUMBER"
									totalWidth = totalWidth + 122
								%>
                                    <div class="table-cell" style="width:120px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
                                <%
								Case "ITEMID"
									totalWidth = totalWidth + 72
								%>
                                    <div class="table-cell" style="width:70px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
                                <%
								Case "ORDERID"
									totalWidth = totalWidth + 82
								%>
                                    <div class="table-cell" style="width:80px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
                                <%
							end select
							nSkipColumn = nSkipColumn + 1										
						end if
					next
					%>
					<div class="table-cell" style="width:60px;"><%=formatnumber(qty, 0)%>&nbsp;</div>
					<div class="table-cell" style="width:110px;" align="right"><%=formatcurrency(linePriceTotal)%>&nbsp;&nbsp;&nbsp;</div>
					<div class="table-cell" style="width:110px;" align="right"><%=formatcurrency(lineCogsTotal)%>&nbsp;&nbsp;&nbsp;</div>
					<div class="table-cell" style="width:110px; color:#060;" align="right"><%=formatcurrency(revenue)%>&nbsp;&nbsp;&nbsp;</div>
				</div>
                    <%
				next
				%>
				<div class="table-row" style="background-color:#fff; border-top:1px solid #ccc;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='';this.style.borderTop='1px solid #ccc';this.style.backgroundColor='#fff'">
                	<%if totalWidth > 0 then%>
	                <div class="table-cell" style="width:<%=totalWidth%>px; font-weight:bold;">Total</div>
                    <%end if%>
					<div class="table-cell" style="width:60px; font-weight:bold;"><%=formatnumber(qtyTotal, 0)%>&nbsp;</div>
					<div class="table-cell" style="width:110px; font-weight:bold;" align="right"><%=formatcurrency(priceTotal)%>&nbsp;&nbsp;&nbsp;</div>
					<div class="table-cell" style="width:110px; font-weight:bold;" align="right"><%=formatcurrency(cogsTotal)%>&nbsp;&nbsp;&nbsp;</div>
					<div class="table-cell" style="width:110px;  font-weight:bold; color:#060;" align="right"><%=formatcurrency(revenueTotal)%>&nbsp;&nbsp;&nbsp;</div>
				</div>
                <%
			else
				response.write "No data to display"
			end if
			%>
            </div>
            <%
		end if
		%>
       	</div>
        <!--// result -->
    </div>
</form>    
</center>
<script>
	var curSortColumnName = "";
	var curSort = "ASC";
	var curSortHeader = null;
	function doSort(sortColumn,oHeader) {
		if (sortColumn == curSortColumnName) {
			if (curSort == "ASC") curSort = "DESC";
			else curSort = "ASC";
		} else {
			curSort = "DESC";
		}
		curSortColumnName = sortColumn;
		
//		alert('curSortColumnName:' + curSortColumnName + '\r\ncurSort:' + curSort);
		if (curSortHeader != null) curSortHeader.innerHTML = curSortHeader.title;
		if (curSort == "DESC") oHeader.innerHTML = oHeader.title + '<div id="sortDesc"></div>';
		else oHeader.innerHTML = oHeader.title + '<div id="sortAsc"></div>';		
		curSortHeader = oHeader;
		
		document.getElementById('searchResult').innerHTML = '<div align="center" style="width:100%; font-size:18px; font-weight:bold;">Sorting Data..</div>'
		ajax('/ajax/admin/ajaxSalesSort.asp?<%=formData%>&curSortColumnName='+curSortColumnName+'&curSort='+curSort,'searchResult');
	}
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->