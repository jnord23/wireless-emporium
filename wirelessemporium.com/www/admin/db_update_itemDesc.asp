<%
response.buffer = false
pageTitle = "Update Item Descriptions / Details"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 2400 'seconds

response.write "<p>&nbsp;</p>" & vbcrlf
response.write "<h3>" & pageTitle & "</h3>" & vbcrlf

if request.form("submitted") <> "" then
	dim strError, site, oldText, newText
	strError = ""
	site = request.form("site")
	response.write "<p>site = " & site & "</p>" & vbcrlf
	oldText = SQLquote(request.form("oldText"))
	newText = SQLquote(request.form("newText"))
	if trim(site) = "" then strError = "You must select at least one Site."
	if trim(oldText) = "" then strError = "You must enter the Old Text you wish to modify."
	if strError = "" then
		if request.form("ReplaceType") = "Desc" then
			SQL = "SELECT itemID, WE_URL, itemDesc, itemDesc_CA, itemDesc_CO, itemDesc_PS FROM we_Items"
			SQL = SQL & " WHERE itemDesc LIKE '%" & oldText & "%'"
			SQL = SQL & " OR itemDesc_CA LIKE '%" & oldText & "%'"
			SQL = SQL & " OR itemDesc_CO LIKE '%" & oldText & "%'"
			SQL = SQL & " OR itemDesc_PS LIKE '%" & oldText & "%'"
			SQL = SQL & " ORDER BY itemID"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			response.write "<h3>" & SQL & "</h3>" & vbCrLf
			if RS.eof then
				response.write "No records matched<br><br>So cannot make table..."
			else
				do until RS.eof
					a = 0
					SQL = "UPDATE we_Items SET"
					if inStr(1,RS("itemDesc"),oldText,1) > 0 and inStr(site,"WE") > 0 then
						SQL = SQL & " itemDesc = '" & SQLquote(replace(RS("itemDesc"),oldText,newText,1,9,1)) & "', "
						'SQL = SQL & " WE_URL = '" & formatSEO(replace(RS("itemDesc"),formatSEO(oldText),formatSEO(newText),1,9,1)) & "'"
						SQL = SQL & " WE_URL = '" & formatSEO(replace(RS("itemDesc"),oldText,newText,1,9,1)) & "'"
						' Add all other fields (brand name, category, model, etc.)!!!
						response.write "<p>WE_URL = " & RS("WE_URL") & "</p>" & vbcrlf
						a = 1
					end if
					if inStr(1,RS("itemDesc_CA"),oldText,1) > 0 and inStr(site,"CA") > 0 then
						if a = 1 then SQL = SQL & ","
						SQL = SQL & " itemDesc_CA = '" & SQLquote(replace(RS("itemDesc_CA"),oldText,newText,1,9,1)) & "'"
						a = 1
					end if
					if inStr(1,RS("itemDesc_CO"),oldText,1) > 0 and inStr(site,"CO") > 0 then
						if a = 1 then SQL = SQL & ","
						SQL = SQL & " itemDesc_CO = '" & SQLquote(replace(RS("itemDesc_CO"),oldText,newText,1,9,1)) & "'"
					end if
					if inStr(1,RS("itemDesc_PS"),oldText,1) > 0 and inStr(site,"PS") > 0 then
						if a = 1 then SQL = SQL & ","
						SQL = SQL & " itemDesc_PS = '" & SQLquote(replace(RS("itemDesc_PS"),oldText,newText,1,9,1)) & "'"
						a = 1
					end if
					if inStr(SQL," = ") > 0 then
						SQL = SQL & " WHERE itemID = '" & RS("itemID") & "'"
						response.write "<p>" & SQL & "</p>" & vbcrlf
						oConn.execute SQL
					end if
					RS.movenext
				loop
			end if
		else
			SQL = "SELECT itemID, itemLongDetail, itemLongDetail_CA, itemLongDetail_CO, itemLongDetail_PS FROM we_Items"
			SQL = SQL & " WHERE itemLongDetail LIKE '%" & oldText & "%'"
			SQL = SQL & " OR itemLongDetail_CA LIKE '%" & oldText & "%'"
			SQL = SQL & " OR itemLongDetail_CO LIKE '%" & oldText & "%'"
			SQL = SQL & " OR itemLongDetail_PS LIKE '%" & oldText & "%'"
			SQL = SQL & " ORDER BY itemID"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			response.write "<h3>" & SQL & "</h3>" & vbCrLf
			if RS.eof then
				response.write "No records matched<br><br>So cannot make table..."
			else
				do until RS.eof
					a = 0
					SQL = "UPDATE we_Items SET"
					if inStr(1,RS("itemLongDetail"),oldText,1) > 0 and inStr(site,"WE") > 0 then
						SQL = SQL & " itemLongDetail = '" & SQLquote(replace(RS("itemLongDetail"),oldText,newText,1,9,1)) & "'"
						a = 1
					end if
					if inStr(1,RS("itemLongDetail_CA"),oldText,1) > 0 and inStr(site,"CA") > 0 then
						if a = 1 then SQL = SQL & ","
						SQL = SQL & " itemLongDetail_CA = '" & SQLquote(replace(RS("itemLongDetail_CA"),oldText,newText,1,9,1)) & "'"
						a = 1
					end if
					if inStr(1,RS("itemLongDetail_CO"),oldText,1) > 0 and inStr(site,"CO") > 0 then
						if a = 1 then SQL = SQL & ","
						SQL = SQL & " itemLongDetail_CO = '" & SQLquote(replace(RS("itemLongDetail_CO"),oldText,newText,1,9,1)) & "'"
					end if
					if inStr(1,RS("itemLongDetail_PS"),oldText,1) > 0 and inStr(site,"PS") > 0 then
						if a = 1 then SQL = SQL & ","
						SQL = SQL & " itemLongDetail_PS = '" & SQLquote(replace(RS("itemLongDetail_PS"),oldText,newText,1,9,1)) & "'"
						a = 1
					end if
					if inStr(SQL," = ") > 0 then
						SQL = SQL & " WHERE itemID = '" & RS("itemID") & "'"
						response.write "<p>" & SQL & "</p>" & vbcrlf
						oConn.execute SQL
					end if
					RS.movenext
				loop
			end if
		end if
		response.write "<h3>UPDATED!</h3>" & vbcrlf
		RS.close
		set RS = nothing
	else
		response.write "<p>" & strError & "</p>" & vbcrlf
		response.write "<p><a href=""javascript:history.back();"">BACK</a></p>" & vbcrlf
	end if
else
	%>
	<br>
	<form action="db_update_itemDesc.asp" method="post">
		<p>
			<b>itemDesc:</b>&nbsp;<input type="radio" name="ReplaceType" value="Desc" checked>&nbsp;&nbsp;&nbsp;&nbsp;<b>itemLongDetail:</b>&nbsp;<input type="radio" name="ReplaceType" value="Detail">
		</p>
		<p>
			<input type="text" name="oldText" size="50">&nbsp;&nbsp;Old Text<br>
			<input type="text" name="newText" size="50">&nbsp;&nbsp;New Text
		</p>
		<p>
			<input type="checkbox" name="site" value="WE">&nbsp;WE&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="site" value="CA">&nbsp;CA&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="site" value="CO">&nbsp;CO&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="site" value="PS">&nbsp;PS
		</p>
		<p><input type="submit" name="submitted" value="<%=pageTitle%>"></p>
	</form>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
