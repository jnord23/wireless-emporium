<%
pageTitle = "Admin - Process Wireless Emporium Orders"
header = 1

' Delete all PDFs older than 1 month
dim filesys, demofolder, fil, filecoll, demofile
set filesys = CreateObject("Scripting.FileSystemObject")
set demofolder = filesys.GetFolder(server.mappath("\admin\tempPDF") & "\")
set filecoll = demofolder.Files
for each fil in filecoll
	if fil.DateLastModified < dateAdd("M",-1,date) then
		set demofile = filesys.GetFile(server.mappath("\admin\tempPDF\" & fil.name))
		demofile.Delete
	end if
next
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_switchChars.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
<%
Server.ScriptTimeout = 1000 'seconds

if request("submitted") = "Process Orders" then
	submitMailOption = request("mailOption")
	holdOrderID = 99999999
	orderWeight = 0
	%>
	<p><a href="ProcessReshipOrders.asp?mailOption=Priority">Process&nbsp;More&nbsp;Reship Priority&nbsp;Only</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="ProcessReshipOrders.asp">Process&nbsp;More&nbsp;Reship First&nbsp;Class</a></p>
	<%
	
		if request("orderStart") <> "" and isNumeric(request("orderStart")) then
			strOrderStartNumber = Int(request("orderStart"))
			if request("orderEnd") <> "" and isNumeric(request("orderEnd")) then
				strOrderEndNumber = Int(request("orderEnd"))
				if strOrderStartNumber > strOrderEndNumber then
					strError = "Start Order # must be lower than End Order #"
				else
					andSQL = " AND A.OrderID <= " & request("orderEnd")
				end if
			end if
		else
			strError = "You must enter a valid Start Order #"
		end if

	if strError = "" then
		dim SQL, RS, strOrderStartNumber
		dim file, filename, path
		dim shipname
		select case submitMailOption
			case "Priority" : shipname = "PRIO"
			case else
				shipname = "FCM"
				if request("qtyOption") = "Multi" then
					shipname = shipname & "-MULTI"
				else
					shipname = shipname & "-SINGLE"
				end if
		end select
		
		if shipname = "PRIO" then
			filenameTXT = "WE_" & replace(cStr(date),"/","-") & "_" & shipname & ".txt"
			path = Server.MapPath("tempTXT") & "\" & filenameTXT
			if filesys.FileExists(path) then filesys.DeleteFile(path)
			set file = filesys.CreateTextFile(path)
		elseif shipname <> "UPS" then
			SQL = "DELETE FROM DHL_temp"
			oConn.execute SQL
		end if
		'f.shipped = 1 ...shipped
		'f.shipped = 2 ...cancled
		
		SQL = "SELECT A.OrderID, A.shippingID,"
		SQL = SQL & " F.reshipid, F.shiptype,"
		SQL = SQL & " B.accountID,B.fname,B.lname,B.email,B.phone,B.sCity,B.sAddress1,B.sAddress2,B.sState,B.sZip,B.sCountry,"
		SQL = SQL & " C.quantity,D.itemWeight,D.typeID,D.price_Our,E.typeName"
		SQL = SQL & " FROM ((((we_orders A INNER JOIN we_accounts B ON A.accountid=B.accountid)"
		SQL = SQL & " INNER JOIN we_reshipment F ON F.OrderID=A.OrderID)"
		SQL = SQL & " INNER JOIN we_reshipdetail C ON C.reshipid=F.reshipid)"
		SQL = SQL & " INNER JOIN we_Items D ON D.itemid=C.itemid)"
		SQL = SQL & " INNER JOIN we_Types E ON D.typeID=E.typeID"
		SQL = SQl & " WHERE"
		'SQL = SQL & " WHERE A.thub_posted_to_accounting = 'R'"
		'SQL = SQL & " AND A.approved = 1 AND (A.cancelled IS NULL OR A.cancelled = 0)"
		'SQL = SQL & " (F.shipped <> 1 or F.Shipped <> 2)"
		SQL = SQL & " (F.shipped is null)"
		SQL = SQL & " And A.store = 0"
		SQL = SQL & " AND ((A.OrderID >= " & strOrderStartNumber
			
		if submitMailOption = "Priority" then
			SQL = SQL & " AND F.shiptype LIKE 'USPS Priority%'"
		else
			SQL = SQL & " AND F.shiptype NOT LIKE 'USPS Priority%'"
		end if
		
		SQL = SQL & "))"
		
		'if submitMailOption = "Priority" then
		'	SQL = SQL & " ORDER BY A.OrderID"
		'else
			SQL = SQL & " ORDER BY D.typeID,A.OrderID"
		'end if
		session("errorSQL") = SQL
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		
		if request("qtyOption") = "Multi" then
			strSearch = ""
			do until RS.eof
				SQL = "SELECT reshipid,SUM(quantity) AS totalQty FROM we_reshipdetail WHERE reshipid = '" & RS("reshipid") & "' GROUP BY reshipid"
				'SQL = "SELECT reshipid,SUM(QTy) AS totalQty FROM we_reshipdetail WHERE reshipid = '" & RS("reshipid") & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				do until RS2.eof
					if RS2("totalQty") > 1 and inStr(strSearch,RS2("reshipid") & ",") = 0 then strSearch = strSearch & RS2("reshipid") & ","
					RS2.movenext
				loop
				RS.movenext
			loop
			
			Response.Write("<p>strSearch:"&strSearch)
			if strSearch <> "" then
				strSearch = left(strSearch,len(strSearch)-1)
				response.write "<p>" & strSearch & "</p>"

				SQL = "SELECT A.OrderID,A.shippingID,"
				SQL = SQL & " F.reshipid, F.shiptype,"
				SQL = SQL & " B.accountID,B.fname,B.lname,B.email,B.phone,B.sCity,B.sAddress1,B.sAddress2,B.sState,B.sZip,B.sCountry,"
				SQL = SQL & " C.quantity,D.itemWeight,D.typeID,D.price_Our,E.typeName"
				SQL = SQL & " FROM (((((we_orders A INNER JOIN we_accounts B ON A.accountid=B.accountid)"
				SQL = SQL & " INNER JOIN we_reshipment F ON F.OrderID=A.OrderID)"
				SQL = SQL & " INNER JOIN we_reshipdetail C ON C.reshipid=F.reshipid)"
				SQL = SQL & " INNER JOIN we_Items D ON D.itemid=C.itemid)"
				SQL = SQL & " INNER JOIN we_Types E ON D.typeID=E.typeID)"
				SQL = SQL & " WHERE F.reshipid IN (" & strSearch & ")"
				'SQL = SQL & " AND (F.shipped <> 1 or F.Shipped <> 2)"
				SQL = SQL & " (F.shipped is null)"
				'SQL = SQL & " isnull(F.shipped) "
				SQL = SQL & " ORDER BY A.OrderID"

				set RS = Server.CreateObject("ADODB.Recordset")
				RS.open SQL, oConn, 3, 3
			end if
		end if
		
		'response.write "<p>" & SQL & "</p>"
		
		dim strOrders, strOrdersDHL, strOrdersDupCheck
		strOrders = ""
		strOrdersDHL = ""
		strOrdersDupCheck = ""
		
		if shipname = "PRIO" then
			strToWrite = """ContactName""" & vbTab & """ORDER_KEY""" & vbTab & """CUSTOMER_KEY""" & vbTab & """TXN_DT""" & vbTab & """EXT_REF_CD""" & vbTab & """QB_TXN_NUMBER""" & vbTab
			strToWrite = strToWrite & """CCY_CODE""" & vbTab & """REF_NUMBER""" & vbTab & """CLASS_REF""" & vbTab & """ARACCOUNT_NAME""" & vbTab & """DEPOSITACCOUNT_NAME""" & vbTab
			strToWrite = strToWrite & """PAYMENTMETHOD_REF""" & vbTab & """TEMPLATE_REF""" & vbTab
			strToWrite = strToWrite & """BADDR_LINE1""" & vbTab & """BADDR_LINE2""" & vbTab & """BADDR_LINE3""" & vbTab & """BADDR_LINE4""" & vbTab
			strToWrite = strToWrite & """BADDR_CITY""" & vbTab & """BADDR_PROVINCE""" & vbTab & """BADDR_STATE""" & vbTab & """BADDR_ZIP""" & vbTab & """BADDR_COUNTRY""" & vbTab
			strToWrite = strToWrite & """Phone""" & vbTab & """Email""" & vbTab & """SADDR_CITY""" & vbTab
			strToWrite = strToWrite & """SADDR_LINE1""" & vbTab & """SADDR_LINE2""" & vbTab & """SADDR_LINE3""" & vbTab & """SADDR_LINE4""" & vbTab
			strToWrite = strToWrite & """SADDR_PROVINCE""" & vbTab & """SADDR_STATE""" & vbTab & """SADDR_ZIP""" & vbTab & """SADDR_COUNTRY""" & vbTab
			strToWrite = strToWrite & """SADDR_PHONE""" & vbTab & """SADDR_EMAIL""" & vbTab & """IS_PENDING""" & vbTab & """PO_NUMBER""" & vbTab & """TERMS_REF""" & vbTab
			strToWrite = strToWrite & """DUE_DT""" & vbTab & """SALESREP""" & vbTab & """FOB""" & vbTab & """SHIP_DT""" & vbTab & """SHIP_METHOD_REF""" & vbTab & """MEMO""" & vbTab
			strToWrite = strToWrite & """CUSTOMER_MSG_REF""" & vbTab & """IS_TO_BE_PRINTED""" & vbTab & """CUSTOMER_SALES_TAX_REF""" & vbTab
			strToWrite = strToWrite & """TAX1_TOTAL""" & vbTab & """TAX2_TOTAL""" & vbTab & """EXCHANGE_RATE""" & vbTab
			strToWrite = strToWrite & """CUST_FIELD1""" & vbTab & """CUST_FIELD2""" & vbTab & """CUST_FIELD3""" & vbTab & """CUST_FIELD4""" & vbTab & """CUST_FIELD5""" & vbTab
			strToWrite = strToWrite & """CUSTOM1""" & vbTab & """CUSTOM2""" & vbTab & """CUSTOM3""" & vbTab & """CUSTOM4""" & vbTab
			strToWrite = strToWrite & """CUSTOM5""" & vbTab & """CUSTOM6""" & vbTab & """CUSTOM7""" & vbTab & """CUSTOM8""" & vbTab
			strToWrite = strToWrite & """CUSTOM9""" & vbTab & """CUSTOM10""" & vbTab & """CUSTOM11""" & vbTab & """CUSTOM12""" & vbTab
			strToWrite = strToWrite & """TOTAL_SHIP_COST""" & vbTab & """TOTAL_HANDLING_COST""" & vbTab & """TOTAL_ORDER_AMT""" & vbTab & """MEMO_PASS_THRU""" & vbTab
			strToWrite = strToWrite & """SRC_TXN_TYPE""" & vbTab & """SRC_PAYMENT_STATUS""" & vbTab & """SRC_PAYER_ID""" & vbTab & """SRC_PAYMENT_TYPE""" & vbTab
			strToWrite = strToWrite & """SRC_BUSINESS_NAME""" & vbTab & """SRC_NOTIFY_VERSION""" & vbTab & """SRC_PAYER_STATUS""" & vbTab & """SRC_RECEIVER_ID""" & vbTab
			strToWrite = strToWrite & """ITEM_NAME""" & vbTab & """ItemName""" & vbTab & """ITEM_DESC""" & vbTab & """ITEM_QUANTITY""" & vbTab & """ITEM_RATE""" & vbTab
			strToWrite = strToWrite & """ITEM_AMOUNT""" & vbTab & """ITEM_CLASS_REF""" & vbTab & """SERVICE_DT""" & vbTab & """ITEM_SALES_TAX_REF""" & vbTab & """ItemWeight""" & vbTab
			strToWrite = strToWrite & """TotalItemWeight""" & vbTab & """Item_Type"""
			file.WriteLine strToWrite
		end if
		
		dim strItemDetails, columnCount
		strItemDetails = ""
		columnCount = 0
		
		if not RS.eof then
			do until RS.eof
				OrderID = RS("OrderID")
				if RS("shippingid") > 0 then
					altShipSQL = "SELECT * FROM we_addl_shipping_addr WHERE id='" & RS("shippingid") & "'"
					set altShipRS = Server.CreateObject("ADODB.Recordset")
					altShipRS.open altShipSQL, oConn, 3, 3
					sAddress1 = altShipRS("sAddress1")
					sAddress2 = altShipRS("sAddress2")
					sCity = altShipRS("sCity")
					sState = altShipRS("sState")
					sZip = altShipRS("sZip")
					if altShipRS("sCountry") = "CANADA" then
						sCountry = "CANADA"
					else
						sCountry = ""
					end if
				else
					sAddress1 = RS("sAddress1")
					sAddress2 = RS("sAddress2")
					sCity = RS("sCity")
					sState = RS("sState")
					sZip = RS("sZip")
					if RS("sCountry") = "CANADA" then
						sCountry = "CANADA"
					else
						sCountry = ""
					end if
				end if
				
				' START variables for later
				quantity = cDbl(RS("quantity"))
				itemWeight = cDbl(RS("itemWeight"))
				orderDesc = "Cellphone " & RS("typeName")
				itemSubtotal = quantity * RS("price_our")
				accountID = RS("accountID")
				BarCode = left(sZip,5) & "-WE" & OrderID & vbcrlf
				AddressBlock = RS("fname") & " " & RS("lname") & vbcrlf & sAddress1 & vbcrlf
				if not isNull(sAddress2) and sAddress2 <> "" then AddressBlock = AddressBlock & sAddress2 & vbcrlf
				AddressBlock = AddressBlock & sCity & ", " & sState & "  " & sZip & vbcrlf
				typeID = RS("typeID")
				' END variables for later
				
				shiptype = RS("shiptype")
				select case shiptype
					case "USPS Priority Mail (2-3 days)" : shipCode = 2
					case "USPS Priority" : shipCode = 2
					case "First Class Int'l" : shipCode = 4
					case else : shipCode = 1
				end select
				
				if shipname = "PRIO" then
					strToWrite = RS("fname") & " " & RS("lname") & vbTab
					for aCount = 1 to 3
						strToWrite = strToWrite & vbTab
					next
					strToWrite = strToWrite & OrderID & vbTab
					for aCount = 1 to 19
						strToWrite = strToWrite & vbTab
					next
					strToWrite = strToWrite & sCity & vbTab
					strToWrite = strToWrite & sAddress1 & vbTab
					strToWrite = strToWrite & sAddress2 & vbTab
					strToWrite = strToWrite & vbTab 'address3
					strToWrite = strToWrite & vbTab
					strToWrite = strToWrite & vbTab
					strToWrite = strToWrite & sState & vbTab
					strToWrite = strToWrite & sZip & vbTab
					strToWrite = strToWrite & sCountry & vbTab
					strToWrite = strToWrite & RS("phone") & vbTab
					strToWrite = strToWrite & RS("email") & vbTab
					for aCount = 1 to 7
						strToWrite = strToWrite & vbTab
					next
					strToWrite = strToWrite & shipCode & vbTab
						strToWrite = strToWrite & "UNITED STATES" & vbTab 'MEMO
					for aCount = 1 to 11
						strToWrite = strToWrite & vbTab
					next
				elseif shipname <> "UPS" then
					'do nothing
				end if
				
				lastOrder = OrderID
				if inStr(strOrders,OrderID) = 0 then strOrders = strOrders & OrderID & ","
				holdOrderID = OrderID
				RS.movenext
				if not RS.eof then
					if RS("OrderID") <> holdOrderID or columnCount = 8 then
						orderWeight = orderWeight + (quantity * itemWeight)
						orderQuantity = orderQuantity + quantity
						orderSubtotal = orderSubtotal + itemSubtotal
						if orderWeight < 2 then orderWeight = 2
						if orderQuantity > 1 then
							multiOrder = "MULTI-ITEMS"
						else
							multiOrder = ""
						end if
						if shipname = "PRIO" then
							strItemDetails = strItemDetails & quantity & vbTab 'Items
							strItemDetails = strItemDetails & orderDesc & vbTab 'Item Name
							strItemDetails = strItemDetails & quantity * itemWeight & vbTab
							strItemDetails = strItemDetails & itemSubtotal & vbTab 'TOTAL_ORDER_AMT
							strToWrite = strToWrite & strItemDetails
							for aCount = 1 to 8 - columnCount
								strToWrite = strToWrite & vbTab
							next
							strToWrite = strToWrite & multiOrder & vbTab
							strToWrite = strToWrite & orderQuantity & vbTab
							strToWrite = strToWrite & orderWeight & vbTab
							strToWrite = strToWrite & orderSubtotal & vbTab
							for aCount = 1 to 19
								strToWrite = strToWrite & vbTab
							next
							if inStr(strOrdersDupCheck,OrderID) = 0 or shipCode = 4 or shipCode = 5 then
								file.WriteLine strToWrite
								strOrdersDupCheck = strOrdersDupCheck & orderID & ","
							end if
						end if
						orderWeight = 0
						orderQuantity = 0
						orderSubtotal = 0
						strItemDetails = ""
						columnCount = 0
					else
						orderWeight = orderWeight + (quantity * itemWeight)
						orderQuantity = orderQuantity + quantity
						strItemDetails = strItemDetails & quantity & vbTab 'Items
						strItemDetails = strItemDetails & orderDesc & vbTab 'Item Name
						strItemDetails = strItemDetails & quantity * itemWeight & vbTab
						strItemDetails = strItemDetails & itemSubtotal & vbTab 'TOTAL_ORDER_AMT
						columnCount = columnCount + 4
					end if
				end if
			loop
			
			orderWeight = orderWeight + (quantity * itemWeight)
			orderQuantity = orderQuantity + quantity
			orderSubtotal = orderSubtotal + itemSubtotal
			if orderWeight < 2 then orderWeight = 2
			if orderQuantity > 1 then
				multiOrder = "MULTI-ITEMS"
			else
				multiOrder = ""
			end if
			if shipname = "PRIO" then
				strItemDetails = strItemDetails & orderQuantity & vbTab 'Items
				strItemDetails = strItemDetails & orderDesc & vbTab 'Item Name
				strItemDetails = strItemDetails & quantity * itemWeight & vbTab
				strItemDetails = strItemDetails & itemSubtotal & vbTab 'TOTAL_ORDER_AMT
				strToWrite = strToWrite & strItemDetails
				for aCount = 1 to 8 - columnCount
					strToWrite = strToWrite & vbTab
				next
				strToWrite = strToWrite & multiOrder & vbTab
				strToWrite = strToWrite & orderQuantity & vbTab
				strToWrite = strToWrite & orderWeight & vbTab
				strToWrite = strToWrite & orderSubtotal & vbTab
				for aCount = 1 to 19
					strToWrite = strToWrite & vbTab
				next
				if inStr(strOrdersDupCheck,OrderID) = 0 or shipCode = 4 or shipCode = 5 then
					file.WriteLine strToWrite
					strOrdersDupCheck = strOrdersDupCheck & orderID & ","
				end if
				file.Close()
			end if
			
			strOrders = left(strOrders,len(strOrders)-1)
			
			SQL = " SELECT A.reshipID,A.orderid,A.shiptype,C.quantity,D.itemWeight,B.accountID,B.fname,B.lname,B.email,B.sCity,B.sAddress1,B.sAddress2,B.sState,B.sZip,B.sCountry"
			SQL = SQL & " FROM ((( we_reshipment A  INNER JOIN we_Orders F ON A.Orderid=F.Orderid)"
			SQL = SQL & " INNER JOIN we_accounts B ON F.accountid=B.accountid)"
			SQL = SQL & " INNER JOIN we_reshipdetail C ON C.reshipID=A.ReshipID)"
			SQL = SQL & " INNER JOIN we_Items D ON D.itemid=C.itemid"
			SQL = SQL & " WHERE A.orderID IN (" & strOrders & ")"
			SQL = SQL & " ORDER BY A.OrderID"
			
			set RS2 = Server.CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			
			%>
			<table border="1">
				<tr>
					<%
					for each whatever in RS2.fields
						%>
						<td><b><%=whatever.name%></b></td>
						<%
					next
					%>
				</tr>
				<%
				do until RS2.eof
					%>
					<tr>
						<%
						for each whatever in RS2.fields
							thisfield = whatever.value
							if isnull(thisfield) then
								thisfield = shownull
							end if
							if trim(thisfield) = "" then
								thisfield = showblank
							end if
							%>
							<td valign="top"><%=thisfield%></td>
							<%
						next
						%>
					</tr>
					<%
					RS2.movenext
				loop
				%>
			</table>
			<%
			'if request("qtyOption") = "Multi" or submitMailOption = "Priority" or submitMailOption = "Express" or submitMailOption = "UPS" or request("submitBuyCom") = "Process Buy.Com Orders" or request("submitAtomic") = "Process AtomicMall Orders" then
		
			if request("qtyOption") = "Multi" or submitMailOption = "Priority" then
				SQL = "SELECT orderID FROM we_reshipment WHERE orderid IN (" & strOrders & ") ORDER BY orderID"
			else
				SQL = "SELECT A.reshipID, A.orderID,C.typeID" 
				SQL = SQL & " FROM ((we_reshipment A INNER JOIN we_reshipdetail B ON A.reshipID=B.reshipID)"
				SQL = SQL & " INNER JOIN we_Items C ON B.itemID=C.itemID)"
				SQL = SQL & " WHERE A.orderID IN (" & strOrders & ")"
				SQL = SQL & " ORDER BY C.typeID,A.orderID"
			end if
			set RS2 = Server.CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			
			filename = "WE_ReshipOrders_" & replace(date,"/","-") & "_" & shipname & ".pdf"
			
			%><!--#include virtual="/admin/ProcessReshipOrders_inc.asp"--><%
			
			SQL = "UPDATE we_RESHIPMENT SET SHIPPED='1', ShipDatetime='" & now & "'"
			SQL = SQL & " WHERE orderid IN (" & strOrders & ")"
			response.write "<p>" & SQL & "</p>"
			'oConn.execute(SQL)
			
			if shipname = "PRIO" then response.write "<p>Here is the <a href=""/admin/tempTXT/" & filenameTXT & """ target=""_blank"">Tarantula Export File</a></p>"
			response.write "<p><a href=""/admin/tempPDF/" & filename & """ target=""_blank"">PDF Sales Orders</a></p>"
		else
			response.write "<h3>No Records Found</h3>"
		end if
	end if
end if

if strError <> "" or (request("submitted") <> "Process Orders") then
	mailOption = request.querystring("mailOption")
	if mailOption = "" then mailOption = "First Class"
	if mailOption = "Priority" then
		'SQL = "SELECT MAX(OrderID) AS OrderStartNumber FROM we_Orders WHERE thub_posted_to_accounting='R'"
		'SQL = SQL & " AND shiptype = 'USPS Priority Mail (2-3 days)'"
		'SQL = SQL & " AND shiptype LIKE 'USPS Priority%'"
		
		SQL = "SELECT Min(a.OrderID) AS OrderStartNumber, Max(a.OrderID) AS OrderEndNumber FROM we_Orders a inner join we_reshipment b on a.orderid = b.orderid" 
		SQL = SQL & " WHERE a.thub_posted_to_accounting='R'"
		SQL = SQL & " and b.shiptype LIKE 'USPS Priority%'"
		SQL = SQL & " AND a.store = 0"
		
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			'strOrderStartNumber = RS("OrderStartNumber") + 1
			strOrderStartNumber = RS("OrderStartNumber")
			strOrderEndNumber = 0
		else
			strOrderStartNumber = 999999
			strOrderEndNumber = 0
		end if
	else
		'SQL = "SELECT MAX(OrderID) AS OrderStartNumber FROM we_Orders WHERE thub_posted_to_accounting='R'"
		'SQL = SQL & " AND shiptype NOT LIKE 'USPS Priority%' AND shiptype NOT LIKE 'USPS Express%' AND shiptype NOT LIKE 'UPS Ground%' AND shiptype NOT LIKE 'UPS 3 Day Select%' AND shiptype NOT LIKE 'UPS 2nd Day Air%' AND shiptype <> 'First Class Int''l'"
		'SQL = SQL & " AND store = 0"
		
		SQL = "SELECT Min(a.OrderID) AS OrderStartNumber, Max(a.OrderID) AS OrderEndNumber FROM we_Orders a inner join we_reshipment b on a.orderid = b.orderid" 
		SQL = SQL & " WHERE a.thub_posted_to_accounting='R'"
		SQL = SQL & " and b.shiptype LIKE 'USPS First Class%'"
		SQL = SQL & " AND a.store = 0"

		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			'strOrderStartNumber = RS("OrderStartNumber") + 1
			strOrderStartNumber = RS("OrderStartNumber")
			strOrderEndNumber = RS("OrderEndNumber")
		else
			strOrderStartNumber = 999999
			'strOrderEndNumber = 0
		end if
		SQL = "SELECT Min(a.OrderID) AS OrderStartNumber, Max(a.OrderID) AS OrderEndNumber FROM we_Orders a inner join we_reshipment b on a.orderid = b.orderid"
		SQL = SQL & " WHERE b.shiptype NOT LIKE 'USPS Priority%' AND b.shiptype NOT LIKE 'USPS Express%' AND b.shiptype NOT LIKE 'UPS Ground%' AND b.shiptype NOT LIKE 'UPS 3 Day Select%' AND b.shiptype NOT LIKE 'UPS 2nd Day Air%' AND b.shiptype <> 'First Class Int''l'"
		SQL = SQL & " AND a.orderdatetime < '" & date & "'"
		SQL = SQL & " AND a.store = 0"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			strOrderEndNumber = RS("OrderEndNumber")
		else
			strOrderEndNumber = 0
		end if
	end if
	%>
	<p>
	<form action="ProcessReshipOrders.asp" name="frmProcess" method="post" onSubmit="return checkSubmit(this);">
		<h3>Process Wireless Emporium Reshipment <font color="red"><%=mailOption%></font> Orders</h3>
		<%if strError <> "" then response.write "<p><font color=""#FF0000""><b>" & strError & "</b></font></p>"%>
		<p>Start Order #:&nbsp;&nbsp;<input type="text" name="orderStart" value="<%=strOrderStartNumber%>"></p>
		<p>End Order #:&nbsp;&nbsp;<input type="text" name="orderEnd" value="<%if strOrderEndNumber > 0 then response.write strOrderEndNumber%>"></p>
		<p>Multi-Item Orders Only:&nbsp;&nbsp;<input type="checkbox" name="qtyOption" value="Multi"></p>
		<table width="800" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<input type="show" name="mailOption" value="<%=mailOption%>">
					<input type="hidden" name="strOrderStartNumber" value="<%=strOrderStartNumber%>">
					<input type="submit" name="submitted" value="Process Orders"><br>
					<!--<input type="submit" name="submitEbill" value="Process eBillme Orders">-->
				</td>
				<td align="right">
					<!--<input type="submit" name="submitBuyCom" value="Process Buy.Com Orders"><br>
					<input type="submit" name="submitAtomic" value="Process AtomicMall Orders"><br>
					<input type="submit" name="submitAmazon" value="Process Amazon Orders">-->
				</td>
			</tr>
		</table>
	</form>
	</p>
	<p>
		<a href="ProcessreshipOrders.asp?mailOption=Priority">Priority&nbsp;Only</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="ProcessreshipOrders.asp">First&nbsp;Class</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%
end if
set filesys = nothing
%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->

<script language="JavaScript">
	function checkSubmit(frm) {
		var strOrderStartNumber = frm.strOrderStartNumber.value;
		var orderStart = frm.orderStart.value;
		if (orderStart < strOrderStartNumber) {
			var answer = confirm(orderStart + " is less than " + strOrderStartNumber + " ...\nAre you sure you want to proceed?");
			if (answer) {
				return true;
			} else {
				return false;
			}
		}
	}
</script>
