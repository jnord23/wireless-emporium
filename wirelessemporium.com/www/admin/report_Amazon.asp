<%
pageTitle = "Admin - Amazon Order Inventory/COGS Report"
header = 1
response.buffer = false
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<font face="Arial,Helvetica">
<blockquote>
<blockquote>

<%
server.scripttimeout = 1000 'seconds
dim Path, Upload, myCount
Path = server.mappath("\admin\tempTXT\amazonReport")


' Create an instance of AspUpload object
set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
myCount = Upload.Save(Path)
if Upload.form("upload") <> "" then
	' Capture uploaded file. Return the number of files uploaded
	if myCount = 0 then
		response.write "No file selected."
		response.end
	else
		' Obtain File object representing uploaded file
		dim uploadFile, myFile, myDefaultFile, fs
		set uploadFile = Upload.Files(1)
		myFile = uploadFile.path
		myDefaultFile = Path & "\AmazonTemp.txt"
		set fs = CreateObject("Scripting.FileSystemObject")
		fs.CopyFile myFile, myDefaultFile
		
		dim file, filename, filepath
		'Map the file name to the physical path on the server.
		filename = "report_Amazon.txt"
		filepath = "D:\inetpub\wwwroot\wirelessemporium.com\www\admin\tempTXT\amazonReport\" & filename
		if fs.FileExists(filepath) then
			Response.Write("Deleting " & filename & ".<br>")
			fs.DeleteFile(filepath)
		end if
		'Create the file and write some data to it.
		Response.Write("Creating " & filename & ".<br>")
		set file = fs.CreateTextFile(filepath)
		
		dim txtConn, myStr, ReferenceId
		set txtConn = Server.CreateObject("ADODB.Connection")
		myStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Path & ";Extended Properties=""text;HDR=Yes;FMT=Delimited"";"
		txtConn.ConnectionString = myStr
		txtConn.ConnectionTimeout = 1000		'seconds
		txtConn.Open
		
		dim strline
		strline = "order-id" & vbtab & "WEorderID" & vbtab & "PartNumber" & vbtab & "COGS" & vbtab & "inv_qty" & vbtab & "order-item-id" & vbtab & "purchase-date" & vbtab & "payments-date" & vbtab & "buyer-email" & vbtab & "buyer-name" & vbtab & "buyer-phone-number" & vbtab & "sku" & vbtab & "product-name" & vbtab & "quantity-purchased" & vbtab & "currency" & vbtab & "item-price" & vbtab & "item-tax" & vbtab & "shipping-price" & vbtab & "shipping-tax" & vbtab & "ship-service-level" & vbtab & "recipient-name" & vbtab & "ship-address-1" & vbtab & "ship-address-2" & vbtab & "ship-address-3" & vbtab & "ship-city" & vbtab & "ship-state" & vbtab & "ship-postal-code" & vbtab & "ship-country" & vbtab & "ship-phone-number" & vbtab & "item-promotion-discount" & vbtab & "item-promotion-id" & vbtab & "ship-promotion-discount" & vbtab & "ship-promotion-id" & vbtab & "delivery-start-date" & vbtab & "delivery-end-date" & vbtab & "delivery-time-zone" & vbtab & "delivery-Instructions" & vbtab & "sales-channel" & vbtab
		file.WriteLine strline
		
		SQL = "SELECT * FROM AmazonTemp.txt"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, txtConn, 3, 3
		
		do until RS.eof
			dim WEorderID, PartNumber, COGS, inv_qty
			WEorderID = ""
			PartNumber = ""
			COGS = ""
			inv_qty = ""
			dim extOrderNumber, SQL, rsTemp, rsTemp2
			extOrderNumber = trim(RS("order-id"))
			if not isNull(extOrderNumber) and extOrderNumber <> "" then
				SQL = "SELECT A.orderid, C.PartNumber, C.COGS, C.inv_qty FROM we_orders A"
				SQL = SQL & " INNER JOIN we_orderdetails B ON A.orderid = B.orderid"
				SQL = SQL & " INNER JOIN we_Items C ON B.itemid = C.itemID"
				SQL = SQL & " WHERE A.extOrderType = 6 AND A.extOrderNumber = '" & extOrderNumber & "'"
				set rsTemp = server.createobject("ADODB.recordset")
				rsTemp.open SQL, oConn, 3, 3
				if not rsTemp.eof then
					WEorderID = rsTemp("orderID")
					PartNumber = rsTemp("PartNumber")
					COGS = rsTemp("COGS")
					if rsTemp("inv_qty") = -1 then
						SQL = "SELECT inv_qty FROM we_Items WHERE inv_qty > -1 AND PartNumber = '" & PartNumber & "'"
						set rsTemp2 = server.createobject("ADODB.recordset")
						rsTemp2.open SQL, oConn, 3, 3
						if not rsTemp2.eof then
							inv_qty = cStr(rsTemp2("inv_qty"))
						else
							inv_qty = "N/A"
						end if
						rsTemp2.close
						set rsTemp2 = nothing
					else
						inv_qty = cStr(rsTemp("inv_qty"))
					end if
				end if
				rsTemp.close
				set rsTemp = nothing
			end if
			strline = extOrderNumber & vbtab
			strline = strline & WEorderID & vbtab
			strline = strline & PartNumber & vbtab
			strline = strline & COGS & vbtab
			strline = strline & inv_qty & vbtab
			for each whatever in RS.fields
				if whatever.name <> "order-id" then strline = strline & whatever.value & vbtab
			next
			file.WriteLine strline
			RS.movenext
		loop
		response.write "<h3>DONE!</h3>" & vbcrlf
		response.write "<p><a href=""/admin/tempTXT/amazonReport/" & filename & """>Here</a> is your report.</p>" & vbcrlf
		RS.close
		set RS = nothing
		txtConn.close
		set txtConn = nothing
	end if
else
	%>
	<h3>Select the file you saved to your hard drive:</h3>
	<form enctype="multipart/form-data" action="report_Amazon.asp" method="POST">
		<p><input type="FILE" name="FILE1"></p>
		<p><input type="SUBMIT" name="upload" value="Upload"></p>
	</form>
	<%
end if
%>

</blockquote>
</blockquote>
</font>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
