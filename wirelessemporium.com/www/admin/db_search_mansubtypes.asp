<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Manipulate SubTypes"
	header = 1
	
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/admin/admin_top.asp"-->
<script type="text/javascript" src="/includes/admin/checkbox.js"></script>
<script>
function onSetMasterCheckBox(frm)
{
	if (isAllChecked(frm.modelID))			frm.chkMaster.checked = true;
	else if (isAllUnChecked(frm.modelID))	frm.chkMaster.checked = false;
}
</script>
<%
sql = "select subtypeid, subtypeName, subTypeOrderNum, typename from v_subtypematrix order by 4, 3"
session("errorSQL") = sql
arrSubTypes = getDbRows(sql)

sql = "select	typeid, typeName from	we_types order by 2"
session("errorSQL") = sql
arrTypes = getDbRows(sql)
%>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="1000">
	<tr>
    	<td><h1 style="margin:0px;">Manipulate Subtypes</h1></td>
    </tr>
    <tr>
    	<td style="font-size:10px; padding-bottom:10px;">
        	This application allows the user to manipulate subtype-related works.
        </td>
    </tr>
    <tr>
    	<td>
            <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
                <tr>
                    <td width="130" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-on" id="id_header_1" onClick="showHide(1);">Mass Updates</td>
                    <td width="200" align="center" style="cursor:pointer; font-size:11px; font-weight:bold;" class="tab-header-off" id="id_header_2" onClick="showHide(2);">Related Items Update</td>
                    <td width="270" class="tab-header-off">&nbsp;</td>
                </tr>
            </table>
		</td>
    </tr>
    <tr>
    	<td>
        	<div id="tbl_1">
				<table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
                    <tr><td width="100%" align="left">&nbsp;</td></tr>
                    <tr>
                        <td width="100%" align="left" style="border-top:1px solid #ccc; border-bottom:1px solid #ccc; padding:5px;">
                        	Mass update to assign sub-subtypes by partnumber or itemid sets.
                        </td>
					</tr>
                    <tr>
                        <td width="100%" align="left">
                        <br />
                        by Part Number:<br />
                        [For wildcard searches, add "%" to the end of the partial Part Number. examples: SCR-% or SCR-SAM%]                    
                        <form style="margin:0px;" action="/admin/db_update_mansubtypes.asp" method="post" name="frm1">                    
                            <input id="id_partnumber" type="text" name="txtPartNumber" value="" />
						<%
						if not isnull(arrSubTypes) then
						%>
                        	<select name="cbSubTypes">
                        <%
							for nRows=0 to ubound(arrSubTypes, 2)
							%>
                            	<option value="<%=arrSubTypes(0,nRows)%>"><%=arrSubTypes(1,nRows)%></option>
                            <%
							next
							%>
                            </select>
                            <%
						end if
						%>
                            <input type="submit" name="submitPartNumberUpdate" value="Update">
                            <input type="hidden" name="updateType" value="m_part" />
                        </form>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" align="left">
                        <br /><br /><br />
                        ItemID set (ex: 11111,22222,33333,44444 or just 11111):<br />
                        <form style="margin:0px;" action="/admin/db_update_mansubtypes.asp" method="post" name="frm2">
						<%
						if not isnull(arrSubTypes) then
						%>
                        	<select name="cbSubTypes">
                        <%
							for nRows=0 to ubound(arrSubTypes, 2)
							%>
                            	<option value="<%=arrSubTypes(0,nRows)%>"><%=arrSubTypes(1,nRows)%></option>
                            <%
							next
							%>
                            </select>
                            <%
						end if
						%>
                        	<br />
                            <textarea name="itemid" cols="60" rows="5"></textarea><br />
                            <input type="submit" name="submitItemIdUpdate" value="Update">
                            <input type="hidden" name="updateType" value="m_item" />
                        </form>
                        </td>
                    </tr>
                </table>            
            </div>
        	<div id="tbl_2" style="display:none;">
				<form style="margin:0px;" action="/admin/db_update_mansubtypes.asp" method="post" name="frmRelated">
				<input type="hidden" name="updateType" value="r" />
                <input type="hidden" name="hidTypeString" value="" />
                <input type="hidden" name="hidSubTypeString" value="" />
                <input type="hidden" name="hidModelID" value="" />
                <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
                    <tr>
                        <td width="100%" align="center" style="font-size:14px; font-weight:bold;"><br />
                        <%
                        sql	=	"select brandid, brandname from we_brands order by brandname"
                        set objRsBrand = oConn.execute(sql)
                        %>
                            SELECT BRAND:&nbsp;
                            <select name="cbBrand" onchange="onBrand(2, this.value);">
                                <option value="">Select Brand</option>
                                <option value="0">All Brands/Models</option>
                        <%
                        do until objRsBrand.eof
                        %>
                                <option value="<%=objRsBrand("brandid")%>"><%=objRsBrand("brandname")%></option>
                        <%
                            objRsBrand.movenext
                        loop
                        %>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" align="left">
                            <div class="left">
                                <div style="padding-top:10px; width:450px; font-weight:bold; font-size:18px; border-bottom:1px solid #000;">Select SubTypes</div>                        
                                <div style="height:200px; width:450px; overflow:auto; border:1px solid #000;">
                                <%
								curTypeName = ""
                                for nRows=0 to ubound(arrSubTypes, 2)
									sTypeName = arrSubTypes(3,nRows)
									if sTypeName <> curTypeName then
									%>
                                    <div class="w100 left bold" style="background-color:#efefef;">&nbsp;<%=sTypeName%></div>
                                    <%
									end if
									%>
									<div class="w100 left">&nbsp; &nbsp;<input name='chkSubTypes' type='checkbox' value='<%=arrSubTypes(0,nRows)%>' /><%=arrSubTypes(1,nRows)%></div>
	                                <%
									curTypeName = sTypeName
                                next
                                %>
                                </div>                        
                            </div>
                            <div class="left" style="margin-left:20px;">
                                <div style="padding-top:10px; width:450px; font-weight:bold; font-size:18px; border-bottom:1px solid #000;">Select Types</div>
                                <div style="height:200px; width:450px; overflow:auto; border:1px solid #000;">
                                <%
                                for nRows=0 to ubound(arrTypes, 2)
								%>
                                    <input name='chkTypes' type='checkbox' value='<%=arrTypes(0,nRows)%>' /><%=arrTypes(1,nRows)%><br>
                                <%
                                next
                                %>
                                </div>                        
                            </div>
                        </td>
                    </tr>
                    <tr><td width="100%" align="left" style="padding-top:10px;">ItemID set (ex: 11111,22222,33333,44444 or just 11111):<br /><textarea name="itemid" cols="110" rows="5"></textarea></td></tr>
                    <tr>
                    	<td width="100%" align="left" style="padding-top:10px;">
                        	<div class="left">
								<font color="blue">Add as default related item set</font>
                                <select name="cbReleatedSet">
                                    <option value="">N/A</option>
									<%
                                    sql = 	"select	distinct setName" & vbcrlf & _
                                            "from	we_defaultRelatedItems " & vbcrlf & _
                                            "order by setName"
                                    set rs = oConn.execute(sql)
                                    do until rs.eof
                                    %>
									<option value="<%=rs("setName")%>"><%=rs("setName")%></option>
                                    <%
                                        rs.movenext
                                    loop
                                    %>
                                </select>                            
                            </div>
                        	<div class="right">
                            	<a href="/admin/relatedItemSets.asp" target="_blank">ADD/EDIT DEFAULT RELEATED ITEM SETS</a>
                            </div>
                            <div class="clear"></div>
                        	<div class="left">
								<font color="blue">Add as default sub-related item set</font>
                                <select name="cbSubReleatedSet">
                                    <option value="">N/A</option>
									<%
                                    sql = 	"select	distinct setName" & vbcrlf & _
                                            "from	we_defaultSubRelatedItems " & vbcrlf & _
                                            "order by setName"
                                    set rs = oConn.execute(sql)
                                    do until rs.eof
                                    %>
									<option value="<%=rs("setName")%>"><%=rs("setName")%></option>
                                    <%
                                        rs.movenext
                                    loop
                                    %>
                                </select>                            
                            </div>
                        	<div class="right">
                            	<a href="/admin/subrelatedItemSets.asp" target="_blank">ADD/EDIT DEFAULT SUB-RELEATED ITEM SETS</a>
                            </div>
                    	</td>
					</tr>
                    <tr>
                        <td width="100%" align="left" style="padding-top:5px;">
                            <input type="submit" name="doSubmitRelatedItems" value="Add" onclick="return doSubmit(this.form);" />
                            <input type="submit" name="doSubmitRelatedItems" value="Delete" onclick="return doSubmit(this.form);" />
                        </td>
                    </tr>
                    <tr><td width="100%" align="left" style="padding-top:5px;"><div id="div_2_modelbox"></div></td></tr>
                </table>
				</form>
            </div>
		</td>
    </tr>
</table>    
<script>
	function getChkObjectToComma(o) {
		strReturn = '';

		if ("undefined" != typeof(o)) 	{
			if ("undefined" != typeof(o.length)) 	{
				for (i=0; i<o.length; i++)	 {
					if (o[i].checked) 	{
						if (strReturn == "")
							strReturn = o[i].value;
						else
							strReturn = strReturn + "," + o[i].value;					
					}
				}
			} else {
				strReturn = o.value;
			}
		}

		return strReturn;
	}
	
	function doSubmit(oForm) {
		strModels = getChkObjectToComma(oForm.modelID);
		strTypes = getChkObjectToComma(oForm.chkTypes);
		strSubTypes = getChkObjectToComma(oForm.chkSubTypes);
	
		if ((strTypes == "")&&(strSubTypes == "")) {
			alert("Please select types or subtypes");
			return false;
		}
		
		if (strModels == "") {
			alert("Please select models");
			return false;
		}
		
		if (oForm.itemid.value == "") {
			alert("ItemID set is required!");
			return false;
		}

		oForm.hidSubTypeString.value = strSubTypes;
		oForm.hidTypeString.value = strTypes;
		oForm.hidModelID.value = strModels;
				
		var strPrompt = "Warning: Do you wish to proceed?";
		if (!confirm(strPrompt)) return false;		
	
		return true;
	}

 	function onBrand(tblID, brandid)
	{
		ajax('/ajax/admin/ajaxManSubtypes.asp?brandid=' + brandid + '&tblID=' + tblID, 'div_' + tblID + '_modelbox');
	}
	
	function showHide(showID)
	{
		for (i=1; i<=2; i++)
		{
			document.getElementById('id_header_' + i).className = "tab-header-off";
			document.getElementById('tbl_' + i).style.display = "none";			
		}
				
		document.getElementById('id_header_' + showID).className = "tab-header-on";
		document.getElementById('tbl_' + showID).style.display = "";
	}
	
	function setTabletCheckbox(oForm) {
		formName = oForm.name;
		checked = oForm.chkMasterTablet.checked;
		tabletIds = document.getElementById(formName + '_id_tablets').innerHTML;
		if (tabletIds != '') {
			var arrTablets = tabletIds.split(',');
			for (i=0; i<arrTablets.length; i++) {
				document.getElementById(formName + '_modelID_'+arrTablets[i]).checked = checked;
			}
		}
	}
	
	function setHeaderCheckbox(oForm) {
		oForm.chkMasterTablet.checked = oForm.chkMaster.checked;
		setTabletCheckbox(oForm);
	}
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->