<%
pageTitle = "phone order by tool"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<script>
function doSubmit(reportType)
{
	document.frmSearch.reportType.value = reportType;
	document.frmSearch.submit();
}
</script>
<style>
.mouseover	{ background-color:#ACCEFF;}
</style>
<%
strSDate		=	ds(request("txtSDate"))
strEDate		=	ds(request("txtEDate"))
strReportType	=	ds(request("reportType"))
siteid			=	request("cbStore")

if "EXCEL" = strReportType then
'	response.redirect "/admin/report_reship_excel.asp?" & request.QueryString
end if

if not isdate(strEDate) then
	sql	=	"select	convert(varchar(10), max(a.orderdatetime), 20) dateRecent from we_orders a join we_phoneorders b on a.orderid = b.orderid"
	session("errorSQL") = sql
	set rsDate = oConn.execute(sql)
	if rsDate.eof then 
		strEDate = date
	else
		strEDate = cdate(rsDate("dateRecent"))
	end if
end if	

if not isdate(strSDate) then
	strSDate = strEDate - 14
end if

strSDate = cdate(strsDate)
strEDate = cdate(strEDate)
'response.write "strSDate:" & strSDate & "<br>"
'response.write "strEDate:" & strEDate & "<br>"

sql	=	"select	a.store, convert(varchar(16), a.orderdatetime, 20) orderdatetime, a.orderid, a.accountid, b.specialnotes, c.fname + ' ' + c.lname adminName, d.shortDesc, d.color siteColor, e.email, f.promoCode" & vbcrlf & _
		"	,	isnull(cast(a.ordergrandtotal as money), 0) orderGrandTotal, isnull(cast(a.ordersubtotal as money), 0) orderSubTotal, isnull(cast(a.ordershippingfee as money), 0) orderShippingFee, isnull(a.orderTax, 0) orderTax" & vbcrlf & _
		"	,	a.shiptype, a.approved, isnull(a.cancelled, 0) cancelled, a.transactionID" & vbcrlf & _
		"from	we_orders a join we_phoneorders b" & vbcrlf & _
		"	on	a.orderid = b.orderid join we_adminUsers c" & vbcrlf & _
		"	on	b.adminid = c.adminid join xstore d" & vbcrlf & _
		"	on	a.store = d.site_id join v_accounts e" & vbcrlf & _
		"	on	a.accountid = e.accountid and a.store = e.site_id left outer join v_coupons f" & vbcrlf & _
		"	on	a.couponid = f.couponid and a.store = f.site_id " & vbcrlf & _
		"where	a.orderdatetime >= '" & strSDate & "' and a.orderdatetime < '" & (strEDate+1) & "'" & vbcrlf
if siteid <> "" then
	sql = sql & "	and	d.site_id = " & siteid & vbcrlf
end if
sql = sql & "order by 1, 2"
'response.write "<pre>" & sql & "</pre><br />"		
session("errorSQL") = sql
set rs = oConn.execute(sql)
%>
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td width="100%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top">
						<form name="frmSearch" method="get">
						<input type="hidden" name="reportType" value="">
						<table width="100%" border="1" cellspacing="0" cellpadding="3" align="center" bordercolor="#FFFFFF" style="border-collapse:collapse;" bgcolor="#EAEAEA">
							<tr>
								<td class="normalText" align="center"><b>FILTER</b></td>
								<td class="normalText" align="center"><b>DATE</b></td>
								<td class="normalText" align="center"><b>STORE</b></td>
								<td class="normalText" align="center"><b>Action</b></td>
							</tr>
							<tr>
								<td class="normalText" align="center"><b>VALUE</b></td>
								<td class="normalText" align="center">
									<input type="text" name="txtSDate" value="<%=strSDate%>" size="7"> ~ <input type="text" name="txtEDate" value="<%=strEDate%>" size="7">
								</td>
								<td class="normalText" align="center">
									<select name="cbStore">
										<option value="">ALL</option>
										<option value="0" <%if siteid = "0" then %>selected<% end if%>>WE</option>
										<option value="1" <%if siteid = "1" then %>selected<% end if%>>CA</option>
										<option value="2" <%if siteid = "2" then %>selected<% end if%>>CO</option>
										<option value="3" <%if siteid = "3" then %>selected<% end if%>>PS</option>
										<option value="10" <%if siteid = "10" then %>selected<% end if%>>TM</option>
									</select>
								</td>
								<td class="normalText" align="center"><input type="submit" value="Search" onClick="doSubmit('WEB');">&nbsp;<!--<input type="button" name="btnExcel" value="Export" onclick="doSubmit('EXCEL');">--></td>
							</tr>	
						</table>
						</form>
					</td>
				</tr>
				<tr>
					<td width="100%" valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
							<tr>
								<td align="center" style="border-bottom:1px solid #F3671C;"><b>STORE</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;"><b>ORDERID</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;"><b>ORDERDATETIME</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;"><b>CUSTOMER EMAIL</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;"><b>Grand Total</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;"><b>Sub Total</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;"><b>Shipping Fee</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;"><b>Order Tax</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;"><b>Discount Total</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;"><b>Ship Type</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;"><b>Approved / Cancelled</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;"><b>Admin User</b></td>
                            </tr>
							<%
                            if rs.eof then
                                response.write "<td colspan=""11"" align=""center"">no data to display</td>"
                            else
                                lap = -1
                                do until rs.eof
                                    lap = lap + 1
									nDiscountTotal = (rs("orderSubTotal") + rs("orderShippingFee") + rs("orderTax")) - rs("orderGrandTotal")
                                    %>
                                    <tr class="grid_row_<%=(lap mod 2)%>">
                                        <td align="center" style="color:<%=rs("siteColor")%>;"><%=rs("shortDesc")%></td>
                                        <td align="center"><%=rs("orderid")%></td>
                                        <td align="center"><%=rs("orderdatetime")%></td>
                                        <td align="center"><%=rs("email")%></td>
                                        <td align="center"><%=formatcurrency(rs("orderGrandTotal"))%></td>
                                        <td align="center"><%=formatcurrency(rs("orderSubTotal"))%></td>
                                        <td align="center"><%=formatcurrency(rs("orderShippingFee"))%></td>
                                        <td align="center"><%=formatcurrency(rs("orderTax"))%></td>
                                        <td align="center" style="color:#900;">(<%=formatcurrency(nDiscountTotal*(-1))%>)</td>
                                        <td align="center"><%=rs("shiptype")%></td>
                                        <td align="center">
                                        <%
                                        if rs("approved") = "1" then 
                                            response.write "Yes"
                                        else
                                            response.write "No"
                                        end if
                                        response.write " / "
                                        if rs("cancelled") = "1" then 
                                            response.write "Yes"
                                        else
                                            response.write "No"
                                        end if
                                        %>
                                        </td>
                                        <td align="center"><%=rs("adminName")%></td>
                                    </tr>
                                    <%
                                    rs.movenext
                                loop
                            end if
                            %>                            
						</table>
                    </td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
