<%
response.buffer = false
pageTitle = "Upload WE Amazon Order Tracking Numbers"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
response.write "<h3>" & pageTitle & "</h3>" & vbcrlf

dim Path
path = server.mappath("\admin\tempTXT")

set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
Count = Upload.Save(Path)
if Upload.form("upload") <> "" then
	if Count = 0 then
		response.write "No file selected."
		response.end
	else
		dim File, myFile, myDefaultFile, filesys
		set File = Upload.Files(1)
		myFile = File.path
		myDefaultFile = Path & "\AmazonTracking.txt"
		set filesys = CreateObject("Scripting.FileSystemObject")
		filesys.CopyFile myFile, myDefaultFile
		set File = nothing
		set filesys = nothing
		
		dim txtConn, myStr
		set txtConn = Server.CreateObject("ADODB.Connection")
		myStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Path & ";Extended Properties=""text;HDR=Yes;FMT=Delimited"";"
		txtConn.ConnectionString = myStr
		txtConn.ConnectionTimeout = 1000		'seconds
		txtConn.Open
		
		SQL = "SELECT * FROM AmazonTracking.txt"
		response.write "<h3>" & SQL & "</h3>" & vbcrlf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, txtConn, 3, 3
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			dim filename
			set filesys = Server.CreateObject("Scripting.FileSystemObject")
			filename = "\AmazonTracking-" & replace(date,"/","-") & ".txt"
			set file = filesys.CreateTextFile(Path & filename)
			file.WriteLine "order-id" & vbtab & "order-item-id" & vbtab & "quantity" & vbtab & "ship-date" & vbtab & "carrier-code" & vbtab & "carrier-name" & vbtab & "tracking-number" & vbtab & "ship-method"
			response.write "<h3>" & RS.recordcount & " records found</h3>" & vbcrlf
			do until RS.eof
				SQL = "SELECT trackingNum FROM we_Orders WHERE extOrderType = 6 AND extOrderNumber = '" & RS("order-id") & "'"
				'response.write "<p>" & SQL & "</p>" & vbcrlf
				set RSwe = Server.CreateObject("ADODB.Recordset")
				RSwe.open SQL, oConn, 3, 3
				if not RSwe.eof then
					strline = RS("order-id") & vbtab
					strline = strline & RS("order-item-id") & vbtab
					strline = strline & RS("quantity") & vbtab
					strline = strline & RS("ship-date") & vbtab
					strline = strline & RS("carrier-code") & vbtab
					strline = strline & RS("carrier-name") & vbtab
					strline = strline & RSwe("trackingNum") & vbtab
					strline = strline & RS("ship-method")
					'response.write "<p>" & strline & "</p>" & vbcrlf
					file.WriteLine strline
				end if
				RSwe.close
				set RSwe = nothing
				RS.movenext
			loop
			file.close()
			set filesys = nothing
		end if
		RS.close
		set RS = nothing
		response.write "Here is the <a href=""/admin/tempTXT/" & replace(filename,"\","") & """>file</a>"
	end if
else
	%>
	<h3>Select the file you saved to your hard drive:</h3>
	<form enctype="multipart/form-data" action="uploadAmazonTracking.asp" method="POST">
		<p><input type="FILE" name="FILE1"></p>
		<p><input type="SUBMIT" name="upload" value="Upload"></p>
	</form>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
