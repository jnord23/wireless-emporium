<%
	pageTitle = "Bunker Items"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	strBunkerPartnumber = prepStr(request.form("chkBunker"))
	strSubmit = prepStr(request.form("submit"))
	cbBunker = prepStr(request("cbBunker"))
	sqlBunkered = 0
	
	if cbBunker = "" then cbBunker = "N"
	if cbBunker = "N" then 
		submitString = "Make Selected Bunker Item"
		sqlBunkered = 0
	else
		submitString = "Make Selected Non-Bunker Item"
		sqlBunkered = 1
	end if

	if strSubmit <> "" then
		if strBunkerPartnumber <> "" then
			strBunkerPartnumber = "'" & replace(strBunkerPartnumber, ", ", "','") & "'"

			'===== insert partnumbers that do not exists on we_itemsextendedData table			
			sql	=	"insert into we_ItemsExtendedData(hidden, partnumber)" & vbcrlf & _
					"select	a.hidelive, a.partnumber" & vbcrlf & _
					"from	we_items a left outer join we_ItemsExtendedData b" & vbcrlf & _
					"	on	a.partnumber = b.partnumber" & vbcrlf & _
					"where	a.partnumber in (" & strBunkerPartnumber & ")" & vbcrlf & _
					"	and	a.master = 1" & vbcrlf & _
					"	and	b.partnumber is null" & vbcrlf
'			response.write sql & "<br>"
			session("errorSQL") = sql
			oConn.execute(sql)

			if cbBunker = "N" then
				sql = 	"update we_ItemsExtendedData set bunker = 1 where partnumber in (" & strBunkerPartnumber & ")"
			else
				sql = 	"update we_ItemsExtendedData set bunker = 0 where partnumber in (" & strBunkerPartnumber & ")"
			end if
'			response.write sql & "<br>"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
	end if
	
	date6Months = DateAdd("m", -6, now)
	date1Year = DateAdd("yyyy", -1, now)
	sql = "select distinct a.partNumber, b.numSold, c.numSoldOld, d.hideLive, d.inv_qty, cast(isnull(e.bunker, 0) as tinyint) bunker from we_Items a left join (select partnumber, COUNT(*) as numSold from we_orderdetails where entryDate > '" & date6Months & "' group by partNumber) b on a.PartNumber = b.partNumber left join (select partnumber, COUNT(*) as numSoldOld from we_orderdetails where entryDate > '" & date1Year & "' group by partNumber) c on a.PartNumber = c.partNumber left join we_Items d on a.PartNumber = d.PartNumber and d.master = 1 left join we_ItemsExtendedData e on a.partnumber = e.partnumber where a.partnumber not like '%-MLD-%' and a.partnumber not like '%DEC-SKN%' and a.DateTimeEntd < '" & date1Year & "' and (b.numSold is null or b.numSold = 0) and d.inv_qty > 0 order by c.numSoldOld, a.partNumber"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)	
%>
<div style="width:550px; height:30px; margin-left:auto; margin-right:auto; margin-top:50px;">
	<div style="float:left;">Select By Prefix</div>
    <div id="prefixSelect" style="float:left; padding-left:10px;"></div>
</div>
<form name="frmBunker" method="post">
<div style="height:30px; width:550px; margin-left:auto; margin-right:auto; margin-top:10px;" align="right"><input type="submit" name="submit" value="<%=submitString%>" /></div>
<div style="height:40px; width:550px; margin-left:auto; margin-right:auto; background-color:#000; font-weight:bold; color:#FFF; font-size:12px;">
    <div style="float:left; width:200px; text-align:left;">Partnumber</div>
    <div style="float:left; width:80px; text-align:center;">6 Months</div>
    <div style="float:left; width:80px; text-align:center;">1 Year</div>
    <div style="float:left; width:70px; text-align:center;">Inv</div>
    <div style="float:left; width:120px; text-align:center;">
    	<div style="width:120px; height:18px;" align="center">Bunker Item</div>
    	<div width="120px;" align="center">
        <select name="cbBunker" style="font-size:11px;" onchange="onBunker(this.value);">
        	<option value="N" <%if cbBunker="N" then%> selected <%end if%>>Non-bunker</option>
        	<option value="Y" <%if cbBunker="Y" then%> selected <%end if%>>Bunker</option>
        </select>
        </div>
	</div>
</div>
<%	
    bgColor = "#fff"
    prodLap = 0
	prefixList = ""
	do while not rs.EOF
		curBunker = rs("bunker")
		partNumber = rs("partNumber")
		numSold = prepInt(rs("numSold"))
		numSoldOld = prepInt(rs("numSoldOld"))
		
		if numSoldOld = 0 then
			if sqlBunkered = curBunker then
				prodLap = prodLap + 1
				if instr(prefixList,left(partNumber,3)) < 1 then prefixList = prefixList & left(partNumber,3) & "##"
				if len(prepStr(partNumber)) > 12 then
					if rs("hideLive") then hideLive = " (hidden)" else hideLive = ""
%>
<div style="height:20px; width:550px; background-color:<%=bgColor%>; margin-left:auto; margin-right:auto;" id="productRow_<%=prodLap%>">
    <div style="float:left; width:200px; text-align:left; border-right:1px solid #000; height:20px;" id="productPN_<%=prodLap%>"><%=prepStr(partNumber)%><%=hideLive%></div>
    <div style="float:left; width:40px; padding-right:40px; text-align:right; border-right:1px solid #000; height:20px;"><%=numSold%></div>
    <div style="float:left; width:40px; padding-right:40px; text-align:right; border-right:1px solid #000; height:20px;"><%=numSoldOld%></div>
    <div style="float:left; width:40px; padding-right:20px; text-align:right; border-right:1px solid #000; height:20px;"><%=prepInt(rs("inv_qty"))%></div>
    <div style="float:left; width:120px; height:20px;" align="center"><input type="checkbox" name="chkBunker" value="<%=prepStr(partNumber)%>" /></div>
</div>
<%
				end if
				if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
				session("errorSQL") = "bunkerable items:" & prodLap
				if (prodLap mod 1000) = 0 then response.Flush()
			end if
		end if
		rs.movenext
	loop
	if prodLap = 0 then
%>
<div style="height:20px; width:550px; background-color:<%=bgColor%>; margin-left:auto; margin-right:auto;">No data to display</div>
<%
	end if
%>
<div style="height:20px; width:550px; margin-left:auto; margin-right:auto;" align="right"><input type="submit" name="submit" value="<%=submitString%>" /></div>
</form>
<div id="actualPrefixBox" style="display:none;">
	<select onchange="chgDisplay(this.value)">
    	<option value="">Show All</option>
<%
	prefixArray = split(prefixList,"##")
	for i = 0 to ubound(prefixArray)
%>
		<option value="<%=prefixArray(i)%>"><%=prefixArray(i)%></option>
<%
	next
%>
	</select>
</div>
<script>
	document.getElementById("prefixSelect").innerHTML = document.getElementById("actualPrefixBox").innerHTML
	
	function chgDisplay(prefix) {
		if (prefix != "") {
			for (i = 1; i < <%=prodLap+1%>; i++) {
				var curPN = document.getElementById("productPN_" + i).innerHTML
				if (curPN.substring(0,3) != prefix) {
					document.getElementById("productRow_" + i).style.display = 'none'
				}
				else {
					document.getElementById("productRow_" + i).style.display = ''
				}
			}
		}
		else {
			for (i=1;i<<%=prodLap%>;i++) {
				document.getElementById("productRow_" + i).style.display = ''
			}
		}
	}

	function onBunker(s) {
		window.location.href = '/admin/bunkerItems.asp?cbBunker='+s;
	}
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->