<%
pageTitle = "New Sales Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<link href="/includes/css/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/includes/js/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/includes/js/cal.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function () {
		$('#id_sdate').simpleDatepicker();
		$('#id_edate').simpleDatepicker();
	});
</script>
<%
siteid = prepInt(request.form("cbSite"))
strSDate = prepStr(request.form("txtSDate"))
strEDate = prepStr(request.form("txtEDate"))

if strSDate = "" or not isdate(strSDate) then strSDate = Date-3
if strEDate = "" or not isdate(strEDate) then strEDate = Date
%>
<center>
<form method="post" name="frmReviews">
    <div style="text-align:left; width:1100px;">
        <div style="padding-bottom:10px; height:20px;">
        	<div style="font-size:18px; font-weight:bold; float:left;">Update User Reviews</div>
		</div>
        <!-- filters-->
        <div style="width:100%; border:1px solid #ccc; border-radius:3px; background-color:#f4f7f9; padding:5px;">
            <div style="width:100%; height:30px;">
				<div class="filter-box-header" style="width:50px;">Filter</div>
                <div class="filter-box-header" style="width:180px;">Date</div>
                <div class="filter-box-header" style="width:60px;">Store</div>
                <div class="filter-box-header" style="width:130px;">Action</div>
            </div>
            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%; padding:5px 0px 5px 0px; height:25px;">
				<div class="filter-box-header" style="width:50px;">Value</div>
                <div class="filter-box" style="width:180px;"><input type="text" id="id_sdate" name="txtSDate" value="<%=strSDate%>" size="7"> ~ <input type="text" id="id_edate" name="txtEDate" value="<%=strEDate%>" size="7"></div>
                <div class="filter-box" style="width:60px;">
                	<select name="cbSite">
                    	<option value="0" <%if siteid = 0 then%>selected<%end if%>>WE</option>
                    	<option value="1" <%if siteid = 1 then%>selected<%end if%>>CA</option>
                    	<option value="2" <%if siteid = 2 then%>selected<%end if%>>CO</option>
                    	<option value="3" <%if siteid = 3 then%>selected<%end if%>>PS</option>
                    	<option value="10" <%if siteid = 10 then%>selected<%end if%>>TM</option>
                    </select>
                </div>
                <div class="filter-box" style="width:130px;">
	                <input type="submit" name="search" value="Search" />
                </div>
            </div>
        </div>
		<!--// filters-->
        <!-- result -->
		<div style="width:100%; padding-top:10px;">
	        <table border="0" cellpadding="5" cellspacing="0" width="100%" style="font-size:11px;">
                <tr style="background-color:#eaf5e5;">
                    <td width="*">Reviews</td>
                </tr>
                <%
				sql	=	"select	id, siteID" & vbcrlf & _
						"	,	nullif(reviewerEmail, '') reviewerEmail, nullif(reviewerNickName, '') reviewerNickName, nullif(reviewerLocation, '') reviewerLocation" & vbcrlf & _
						"	,	nullif(reviewerAge, 0) reviewerAge, nullif(reviewerGender, '') reviewerGender, nullif(reviewerType, '') reviewerType" & vbcrlf & _
						"	,	nullif(orderID, 0) orderID, a.itemID, starRating" & vbcrlf & _
						"	,	nullif(ownershipPeriod, 0) ownershipPeriod, reviewTitle" & vbcrlf & _
						"	,	isRecommend, isProPrice, isProDesign, isProWeight, isProProtection" & vbcrlf & _
						"	,	isConFit, isConRobust, isConMaintenance, isConWeight" & vbcrlf & _
						"	,	thumbsUpCount, thumbsDownCount" & vbcrlf & _
						"	,	a.entryDate, convert(varchar(12), a.entryDate, 107) entryDate2, isApproved, a.processedDate, c.fname, c.lname, c.adminid" & vbcrlf & _
						"	,	b.partnumber, b.itemdesc" & vbcrlf & _
						"	,	nullif(otherCon, '') otherCon, nullif(otherPro, '') otherPro, reviewBody" & vbcrlf & _
						"from	XReview a left outer join we_items b" & vbcrlf & _
						"	on	a.itemid = b.itemid left outer join we_adminusers c" & vbcrlf & _
						"	on	a.processedAdminID = c.adminID" & vbcrlf & _
						"where	a.entryDate >= '" & strSDate & "' and a.entryDate < '" & dateAdd("D",1,strEDate) & "'" & vbcrlf & _
						"	and	siteid = " & siteid & vbcrlf & _
						"order by id desc"
'				response.write "<pre>" & sql & "</pre>"
				set objRsReviews = oConn.execute(sql)
                lap = 0
				if objRsReviews.eof then
				%>
                <tr>
                    <td width="*">No reviews to display</td>
                </tr>
                <%
				else
					do until objRsReviews.eof
						lap = lap + 1			
						rowBackgroundColor = "#fff"
						if (lap mod 2) = 0 then rowBackgroundColor = "#eaf5fa"		
						if (lap mod 1000) = 0 then response.flush
	
						reviewID = objRsReviews("id")
						itemID = objRsReviews("itemid")
						partnumber = objRsReviews("partnumber")
						itemdesc = objRsReviews("itemdesc")
						orderid = objRsReviews("orderid")
						reviewerEmail = objRsReviews("reviewerEmail")
						reviewTitle = objRsReviews("reviewTitle")
						reviewerNickName = objRsReviews("reviewerNickName")
						reviewerLocation = objRsReviews("reviewerLocation")
						reviewerAge = objRsReviews("reviewerAge")
						reviewerGender = objRsReviews("reviewerGender")
						reviewerType = objRsReviews("reviewerType")
						starRating = objRsReviews("starRating")
						ownershipPeriod = objRsReviews("ownershipPeriod")
						entryDate = objRsReviews("entryDate")
	
						isRecommend = objRsReviews("isRecommend")
						isProPrice = objRsReviews("isProPrice")
						isProDesign = objRsReviews("isProDesign")
						isProWeight = objRsReviews("isProWeight")
						isProProtection = objRsReviews("isProProtection")
						isConFit = objRsReviews("isConFit")
						isConRobust = objRsReviews("isConRobust")
						isConMaintenance = objRsReviews("isConMaintenance")
						isConWeight = objRsReviews("isConWeight")
						thumbsUpCount = objRsReviews("thumbsUpCount")
						thumbsDownCount = objRsReviews("thumbsDownCount")
						isApproved = objRsReviews("isApproved")
						strAdminUserName = objRsReviews("fname") & " " & objRsReviews("lname")
						approvalProcessedDate = objRsReviews("processedDate")
						
						otherCon = objRsReviews("otherCon")
						otherPro = objRsReviews("otherPro")
						reviewBody = objRsReviews("reviewBody")
					%>
					<tr style="background-color:<%=rowBackgroundColor%>;">
						<td align="left" valign="top">
							<div>
								<%=itemID%> (<%=partnumber%>)
								<br />
								<span style="font-size:16px; font-weight:bold;"><%=itemdesc%></span>
								<br />
								OrderID: 
								<%if isnull(orderid) then%>
								N/A
								<%else%>
									<%=orderid%>
								<%end if%>
							</div>
							<div style="display:table;">
								<div style="float:left; width:250px;">
									<div style="float:left; background-color:#f7f7f7; width:230px; padding:10px;">
										<div style="float:left; width:100%;">
											<div style="float:left; padding-top:2px;"><img src="/images/admin/ico-head.png" border="0" /></div>
											<div style="float:left; padding-left:5px;">
												<div style="font-weight:bold; color:#034076;"><%=reviewerNickName%></div>
												<div><%=reviewerLocation%></div>
											</div>
										</div>
										<div style="float:left; width:100%; padding-top:15px;">
											<%if isRecommend then%>
											<div style="float:left;"><img src="/images/admin/recommend-check.png" border="0" alt="I recommend this!" /></div>
											<div style="float:left; padding-left:3px; font-style:italic;"><span style="font-size:15px; font-weight:bold;">Yes,</span> I recommend this!</div>
											<%else%>
											<div style="float:left;"><img src="/images/admin/recommend-x.png" border="0" /></div>
											<div style="float:left; padding-left:3px; font-style:italic;"><span style="font-size:15px; font-weight:bold;">No,</span> I don't recommend this!</div>
											<%end if%>
										</div>
										<div style="float:left; width:100%; padding-top:15px;">
											<div style="display:table;">
												<div class="filter-box-header" style="width:220px; text-align:left; margin:0px; font-size:12px;">
													<strong>Approved:</strong> 
													<input type="checkbox" name="chkApprove<%=reviewID%>" value="<%=isApproved%>" <%if isApproved then%>checked<%end if%> onClick="approveReview(<%=reviewID%>,this)" />
													<span id="row_approve_<%=reviewID%>">
                                                    <%
													if isApproved then
														response.write "<br />Admin: " & strAdminUserName
														response.write "<br />Approval Date: " & approvalProcessedDate                                                    													
													else
														response.write "&nbsp;"
													end if
													%>
                                                    </span>                                            
												</div>
											</div>
											<div style="padding-top:7px;">
												<strong>Email:</strong> 
												<%if not isnull(reviewerEmail) then%>
													<%=reviewerEmail%>
												<%else%>
													N/A
												<%end if%>
											</div>
											<div style="padding-top:7px;">
												<strong>Age:</strong> 
												<%
												if not isnull(reviewerAge) then
													select case cint(reviewerAge)
														case 1 : response.write "17 & under"
														case 2 : response.write "18 - 25"
														case 3 : response.write "26 - 35"
														case 4 : response.write "36 - 45"
														case 5 : response.write "46 - 55"
														case 6 : response.write "56+"
														case else : response.write "N/A"
													end select
												else
												%>
													N/A
												<%end if%>
											</div>
											<div style="padding-top:7px;">
												<strong>Gender:</strong> 
												<%
												if not isnull(reviewerGender) then
													if reviewerGender = "M" then
														response.write "Male"
													else
														response.write "Female"
													end if
												else
												%>
													N/A
												<%end if%>
											</div>
											<div style="padding-top:7px;">
												<strong>Description:</strong> 
												<%
												if not isnull(reviewerType) then
													select case ucase(reviewerType)
														case "STU" : response.write "Student"
														case "BO" : response.write "Business Owner"
														case "TE" : response.write "Technology Enthusiast"
														case "PA" : response.write "Phone Addict"
														case "CPU" : response.write "Casual Phone User"
													end select
												else
												%>
													N/A
												<%end if%>
											</div>
											<div style="padding-top:7px;">
												<strong>Ownership:</strong> 
												<%
												if not isnull(ownershipPeriod) then
													select case cint(ownershipPeriod)
														case 1 : response.write "2 ~ 4 Weeks"
														case 2 : response.write "1 ~ 2 Months"
														case 3 : response.write "3 ~ 12 Months"
														case 4 : response.write "1 year +"
													end select
												else
												%>
													N/A
												<%end if%>
											</div>
										</div>
									</div>
								</div>
								<div style="float:left; margin-left:20px; width:750px;">
									<div style="float:left; width:100%;">
										<div style="float:left;"><%=getRatingAvgStarBig(starRating)%></div>
										<div style="float:left; padding-left:10px; font-size:16px; font-weight:bold;"><%=formatnumber(starRating,1)%></div>
									</div>
									<div style="float:left; width:100%; font-size:18px; padding-top:3px;"><%=reviewTitle%></div>
									<div style="float:left; width:100%; font-size:12px; padding-top:3px; color:#999; font-style:italic;">Posted on <%=entryDate%></div>
									<div style="float:left; width:100%; padding-top:10px; line-height:22px; font-size:14px;"><%=reviewBody%></div>
									<div style="float:left; width:100%; padding-top:15px;">
										<div>
											<span style="font-size:16px; font-weight:bold;">Pros:</span>
											<%
											strPros = ""
											if isProPrice then strPros = "Great Price, "
											if isProDesign then strPros = strPros & "Beautifully Designed, "
											if isProWeight then strPros = strPros & "Lightweight, "
											if isProProtection then strPros = strPros & "Protects Contents, "
											if strPros <> "" then response.write left(strPros, len(strPros)-2)
											if not isnull(otherPro) then 
												if strPros <> "" then response.write "<br>"
												response.write "<span style=""font-style:italic;"">""" & otherPro & """</span>"
											end if
											%>
										</div>
										<div style="padding-top:7px;">
											<span style="font-size:16px; font-weight:bold;">Cons:</span>
											<%
											strCons = ""
											if isConFit then strCons = "Doesn't Fit Well, "
											if isConRobust then strCons = strCons & "Scratches Easily, "
											if isConMaintenance then strCons = strCons & "Difficult To Clean, "
											if isConWeight then strCons = strCons & "Too Bulky/Heavy, "
											if strCons <> "" then response.write left(strCons, len(strCons)-2)
											if not isnull(otherCon) then 
												if strCons <> "" then response.write "<br>"
												response.write "<span style=""font-style:italic;"">""" & otherCon & """</span>"
											end if
											%>
										</div>
									</div>                                
								</div>
							</div>                        
						</td>
					</tr>
					<%
						objRsReviews.movenext
					loop
				end if
				%>
            </table>
       	</div>
        <!--// result -->
    </div>
</form>    
</center>
<script>
	function approveReview(reviewID,o) {
		var isApproved = 0;
		if (o.checked) isApproved = 1;
		ajax('/ajax/admin/ajaxApproveReview.asp?reviewid='+reviewID+'&isApproved='+isApproved,'row_approve_'+reviewID);
	}
</script>
<%
function getRatingAvgStarBig(rating)
	dim nRating 
	dim strRatingImg
	nRating = cdbl(rating)
	strRatingImg = 	"<img src=""/images/admin/star-empty.png"" border=""0"" />" & vbcrlf & _
					"<img src=""/images/admin/star-empty.png"" border=""0"" />" & vbcrlf & _
					"<img src=""/images/admin/star-empty.png"" border=""0"" />" & vbcrlf & _
					"<img src=""/images/admin/star-empty.png"" border=""0"" />" & vbcrlf & _
					"<img src=""/images/admin/star-empty.png"" border=""0"" />" & vbcrlf
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""	
		for i=1 to 5
			if nRating => i then
				strRatingImg = strRatingImg & "<img src=""/images/admin/star-full.png"" border=""0"" /> "
			elseif nRating => ((i - 1) + .1) then
				strRatingImg = strRatingImg & "<img src=""/images/admin/star-full.png"" border=""0"" /> "				
			else
				strRatingImg = strRatingImg & "<img src=""/images/admin/star-empty.png"" border=""0"" /> "
			end if
		next		
	end if
	
	getRatingAvgStarBig = strRatingImg
end function
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->