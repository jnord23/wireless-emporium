<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
Dim cfgArrGroupBy : cfgArrGroupBy = Array("DATE", "STORE", "ReshipReason", "Category")
dim arrGroupBy, sql
dim strSDate, strEDate, strStore, strReshipReason, strCategory, strReportType

strSDate		=	trim(request("txtSDate"))
strEDate		=	trim(request("txtEDate"))
strStore		=	trim(request("cbStore"))
strReshipReason	=	trim(request("cbReshipReason"))
strGroupBy		=	trim(request("cbGroupBy"))
strCategory		=	trim(request("cbCategory"))

filename	=	"ProductSalesActivity_" & Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now) & ".xls"

Response.ContentType = "application/vnd.ms-excel"
'Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
Response.AddHeader "Content-Disposition","attachment;filename="""&filename&""""
'Response.Charset = "" 
'Me.EnableViewState = False

dim arrDate
sql	=	"select	convert(varchar(10), max(entrydate), 20), convert(varchar(10), dbo.[fn_getFirstDayOfMonth](max(entrydate)), 20)" & vbcrlf & _
		"from	we_orderdetails" & vbcrlf & _
		"where	reship = 1" & vbcrlf
arrDate	= getDbRows(sql)

if "" = strSDate then strSDate = arrDate(1,0) end if
if "" = strEDate then strEDate = arrDate(0,0) end if
If "" = strGroupBy then strGroupBy = "STORE,ReshipReason,," end if
arrGroupBy = split(strGroupBy, ",")

dim strSqlSelectMain, strSqlWhere, strSqlGroupBy, strSqlOrderBy, strHeadings
strSqlSelectMain	=	""
strSqlWhere			=	""
strSqlGroupBy		=	""
strSqlOrderBy		=	""
strHeadings			=	""

if "" <> strStore 		then	strSqlWhere	=	strSqlWhere & " and x.site_id = '" & strStore & "'" & vbcrlf end if
if "" <> strCategory 	then 	strSqlWhere	=	strSqlWhere & " and t.typeid = '" & strCategory & "'" & vbcrlf end if

Dim bDate, bStore, bReshipReason, bCategory
bDate			=	false
bStore			=	false
bReshipReason	=	false
bCategory		=	false

For i = 0 To Cint(UBound(arrGroupBy))
	If "" <> trim(arrGroupBy(i)) Then
		Select Case ucase(trim(arrGroupBy(i)))
			Case "DATE"
				If bDate Then
					Response.Write "DATE in [GROUP BY] is duplicated"
					Response.End
				End If
				bDate = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.eDate"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.eDate, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.eDate, "
				strHeadings			=	strHeadings			&	"RESHIP DATE" & vbTab

			Case "STORE"
				If bStore Then
					Response.Write "STORE in [GROUP BY] is duplicated"
					Response.End
				End If
				bStore = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.shortDesc, a.favicon, a.color"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.shortDesc, a.favicon, a.color, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.shortDesc, "				
				strHeadings			=	strHeadings			&	"STORE" & vbTab

			Case "RESHIPREASON"
				If bReshipReason Then
					Response.Write "ReshipReason in [GROUP BY] is duplicated"
					Response.End
				End If
				bReshipReason = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.reshipReason"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.reshipReason, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.reshipReason, "
				strHeadings			=	strHeadings			&	"RESHIP REASON" & vbTab

			Case "CATEGORY"
				If bCategory Then
					Response.Write "Category in [GROUP BY] is duplicated"
					Response.End
				End If
				bCategory = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.typename"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.typename, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.typename, "				
				strHeadings			=	strHeadings			&	"CATEGORY" & vbTab
		End Select
	End If
Next

dim arrResult, nSkipColumn, nRow, nTotalQty
nTotalQty = 0

if "" <> strSqlGroupBy then
	strSqlGroupBy	=	"group by " & left(trim(strSqlGroupBy), len(trim(strSqlGroupBy))-1)
	strSqlOrderBy	=	"order by " & left(trim(strSqlOrderBy), len(trim(strSqlOrderBy))-1)
end if

sql	=	"select	isnull(sum(a.quantity), 0) qty " & strSqlSelectMain & vbcrlf & _
		"from	(" & vbcrlf & _
		"		select	a.orderid, x.site_id, x.shortDesc, nullif(x.logo, '') favicon, x.color" & vbcrlf & _
		"			,	b.orderdetailid, b.quantity, b.reshipReason, b.modifier, b.note, b.entrydate" & vbcrlf & _
		"			, 	convert(varchar(10), b.entrydate, 20) eDate, t.typename " & vbcrlf & _
		"		from	we_orders a join we_orderdetails b" & vbcrlf & _
		"			on	a.orderid = b.orderid join xstore x" & vbcrlf & _
		"			on	a.store = x.site_id  join we_items i" & vbcrlf & _
		"			on	b.itemid = i.itemid join we_types t" & vbcrlf & _
		"			on	i.typeid = t.typeid" & vbcrlf & _	
		"		where	b.reship = 1" & vbcrlf & _
		"			and	b.entrydate >= '" & strSDate & "'" & vbcrlf & _
		"			and	b.entrydate < dateadd(dd, 1, '" & strEDate & "')" & vbcrlf & _
				strSqlWhere & vbcrlf & _
		"		) a" & vbcrlf & _
		strSqlGroupBy & vbcrlf & _
		strSqlOrderBy & vbcrlf
		
'response.write "<pre>" & sql & "</pre>"
arrResult = getDbRows(sql)

if not isnull(arrResult) then
	for i=0 to ubound(arrResult,2)
		nTotalQty = cdbl(nTotalQty) + cdbl(arrResult(0,i))
	next
end if

response.write strHeadings
response.write "Quantity" & vbNewLine

if not isnull(arrResult) then
	response.write "Total : " & formatnumber(nTotalQty, 0) & vbNewLine
	for nRow = 0 to ubound(arrResult,2)
		nSkipColumn = 1
		for i=0 to ubound(arrGroupBy)
			if "" <> trim(arrGroupBy(i)) then
				select case ucase(trim(arrGroupBy(i)))
					case "STORE"
						response.write arrResult(nSkipColumn, nRow) & vbTab
						nSkipColumn = nSkipColumn + 2									
					case else
						response.write arrResult(nSkipColumn, nRow) & vbTab
				end select
				nSkipColumn = nSkipColumn + 1										
			end if
		next
	
		response.write formatnumber(arrResult(0, nRow),0) & vbTab
		response.write vbNewLine
	next
else
	response.write "No data to display"
end if	

function getDbRows(pSql)
	session("errorSQL") = pSql
	Dim objRs	:	Set objRs = Server.CreateObject("ADODB.RecordSet")
	Dim tRet	:	tRet = NULL
	
	'CursorType : adOpenForwardOnly 0 , LockType adLockReadOnly 1
	objRs.Open pSql, oConn, 0, 1

	If objRs.EOF Then
		tRet = NULL
	Else
		tRet = objRs.GetRows()		
	End If

	objRs.Close : Set objRs = Nothing

	getDbRows = tRet
end function

oConn.close
set oConn = nothing
%>