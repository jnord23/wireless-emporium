<%
	thisUser = Request.Cookies("username")
	pageTitle = "Music Skins Product Upload"
	header = 1
	server.ScriptTimeout = 30000
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim Upload, path, userMsg, fso, myFile, fileLoc, curLine, curArray, saveProd
	dim artDesign, artist, design, musicSkinID, defaultImg, sampleImg, model, brand, category
	dim prodLap : prodLap = 0
	dim remaining : remaining = 0
	dim lastID : lastID = 0
	
	set jpeg = Server.CreateObject("Persits.Jpeg")
	set fso = CreateObject("Scripting.FileSystemObject")
	
	imagePath = server.MapPath("/productpics")
	
	set Upload = Server.CreateObject("Persits.Upload")
	Upload.IgnoreNoPost = true
	path = server.MapPath("/xmlFiles/mobileLine/")
	Count = Upload.Save(path)
	
	sql = "select count(*) as cnt from we_items_musicSkins"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	startingCnt = rs("cnt")
	rs = null
	
	function addItems()
		sql = "select top 200 * from holding_musicSkins"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		addLoop = 0
		do while not rs.EOF					
			saveProd = 0
			addLoop = addLoop + 1
			lastID = rs("id")
			artDesign = split(replace(rs("artist"),"""","")," - ")
			session("errorSQL") = rs("artist") & "(" & ubound(artDesign) & ")"
			if ubound(artDesign) > 0 then
				artist = artDesign(0)
				design = artDesign(1)
			else
				artist = artDesign(0)
				design = artDesign(0)
			end if
			musicSkinID = replace(rs("model"),"""","")
			defaultImg = mid(rs("defaultImg"),instrRev(rs("defaultImg"),"/") + 1)
			fullDefault = rs("defaultImg")
			sampleImg = mid(rs("sampleImg"),instrRev(rs("sampleImg"),"/") + 1)
			fullSample = rs("sampleImg")
			model = replace(rs("device"),"""","")
			brand = replace(rs("brand"),"""","")
			category = replace(rs("category"),"""","")
			model = trim(replace(model,brand,""))
			
			if category = "Phones" or category = "MP3 Players" then
				cogs = "6.50"
				msrp = "14.99"
				price_WE = "12.99"
				saveProd = 1
			elseif category = "Tablets" or category = "E-Readers" then
				cogs = "8.25"
				msrp = "19.99"
				price_WE = "17.99"
				saveProd = 1
			end if
			
			if saveProd = 1 then
				sql =	"if not (select count(*) from we_items_musicSkins where musicSkinsID = '" & musicSkinID & "') > 0 " &_
						"insert into we_items_musicSkins (musicSkinsID,brandID,brand,modelID,altModelID,model,artist,designName,defaultImg,sampleImg,cogs,msrp,price_WE) " &_
						"select '" & musicSkinID & "',brandID,'" & brand & "',(select top 1 modelID from we_items_musicSkins where model = '" & model & "'),(select top 1 altModelID from we_items_musicSkins where model = '" & model & "'),'" & model & "','" & ds(artist) & "','" & ds(design) & "','" & defaultImg & "','" & sampleImg & "','" & cogs & "','" & msrp & "','" & price_WE & "' from we_brands where brandName like '%" & brand & "%'"
				session("errorSQL") = "addLoop:" & addLoop & "<br>" & sql
				oConn.execute(sql)
				
'				if not fso.FileExists("C:\inetpub\wwwroot\productpics\musicSkins\musicSkinsDefault\" & defaultImg) then
				if not fso.FileExists(imagePath & "\musicSkins\musicSkinsDefault\" & defaultImg) then
					session("errorSQL") = "default pic not found"
					set XMLHTTP = server.CreateObject("MSXML2.ServerXMLHTTP")
					XMLHTTP.Open "GET", fullDefault, False
					session("errorSQL") = "url = " & fullDefault
					XMLHTTP.Send
					
					If XMLHTTP.Status = "200" AND XMLHTTP.Status <> "404" then	
						set DataStream = CreateObject("ADODB.Stream")
						DataStream.Open
						DataStream.Type = 1
						DataStream.Write xmlHTTP.ResponseBody
						DataStream.Position = 0
'						DataStream.SaveToFile "C:/inetpub/wwwroot/productpics/temp/" & defaultImg
						DataStream.SaveToFile imagePath & "\temp\" & defaultImg
						DataStream.Close
						set DataStream = Nothing
					else
						session("errorSQL2") = "error accessing image"
					end if
					
					Set XMLHTTP = Nothing
					
					jpeg.Open imagePath & "\temp\" & defaultImg
					if jpeg.OriginalHeight > jpeg.OriginalWidth then
						jpeg.Height = 300
						jpeg.Width = jpeg.OriginalWidth * 300 / jpeg.OriginalHeight
					else
						jpeg.Width = 300
						jpeg.Height = jpeg.OriginalHeight * 300 / jpeg.OriginalWidth
					end if
					jpeg.Save imagePath & "\musicSkins\musicSkinsDefault\" & defaultImg
					
					jpeg.Open imagePath & "\temp\" & defaultImg
					if jpeg.OriginalHeight > jpeg.OriginalWidth then
						jpeg.Height = 100
						jpeg.Width = jpeg.OriginalWidth * 100 / jpeg.OriginalHeight
					else
						jpeg.Width = 100
						jpeg.Height = jpeg.OriginalHeight * 100 / jpeg.OriginalWidth
					end if
					jpeg.Save imagePath & "\musicSkins\musicSkinsDefault\thumbs\" & defaultImg
					
					fso.deleteFile(imagePath & "\temp\" & defaultImg)
					
					set XMLHTTP = server.CreateObject("MSXML2.ServerXMLHTTP")
					XMLHTTP.Open "GET", fullSample, False
					session("errorSQL") = "url = " & fullSample
					XMLHTTP.Send
					
					If XMLHTTP.Status = "200" AND XMLHTTP.Status <> "404" then	
						set DataStream = CreateObject("ADODB.Stream")
						DataStream.Open
						DataStream.Type = 1
						DataStream.Write xmlHTTP.ResponseBody
						DataStream.Position = 0
'						DataStream.SaveToFile "C:/inetpub/wwwroot/productpics/temp/" & sampleImg
						DataStream.SaveToFile imagePath & "\temp\" & sampleImg
						DataStream.Close
						set DataStream = Nothing
					else
						session("errorSQL2") = "error accessing image"
					end if
					
					Set XMLHTTP = Nothing
					
					if fso.fileExists(imagePath & "\temp\" & sampleImg) then
						session("errorSQL") = imagePath & "\temp\" & sampleImg
						jpeg.Open imagePath & "\temp\" & sampleImg
						if jpeg.OriginalHeight > jpeg.OriginalWidth then
							jpeg.Height = 300
							jpeg.Width = jpeg.OriginalWidth * 300 / jpeg.OriginalHeight
						else
							jpeg.Width = 300
							jpeg.Height = jpeg.OriginalHeight * 300 / jpeg.OriginalWidth
						end if
						jpeg.Save imagePath & "\musicSkins\musicSkinsSample\" & sampleImg
						
						fso.deleteFile(imagePath & "\temp\" & sampleImg)
					end if
				end if
			end if
			rs.movenext
		loop
		
		sql = "delete from holding_musicSkins where id <= " & lastID
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "select count(*) as cnt, (select count(*) from holding_musicSkins) as remaining from we_items_musicSkins"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		endingCnt = rs("cnt")
		remaining = rs("remaining")
		rs = null
		
		session("totalNewProds") = session("totalNewProds") + (endingCnt - startingCnt)
		userMsg = session("totalNewProds") & " New Music Skins Files Added To The System"
	end function
	
	if Upload.form("upload") <> "" then
		session("totalNewProds") = 0
		if Count = 0 then
			userMsg = "## No file selected for upload ##"
		else
			set File = Upload.Files(1)
			fileLoc = File.path
			
			set MyFile = fso.OpenTextFile(fileLoc,1)
			Do While MyFile.AtEndOfStream <> True
				prodLap = prodLap + 1
				curLine = MyFile.ReadLine
				curArray = split(curLine,",")
				if prodLap > 1 then
					if curArray(0) <> "" and curArray(1) <> "" and curArray(2) <> "" and curArray(3) <> "" and curArray(4) <> "" and curArray(5) <> "" and curArray(6) <> "" then
						sql = "insert into holding_musicSkins (artist,model,defaultImg,sampleImg,device,brand,category) values('" & ds(replace(curArray(0),"""","")) & "','" & ds(replace(curArray(1),"""","")) & "','" & ds(replace(curArray(2),"""","")) & "','" & ds(replace(curArray(3),"""","")) & "','" & ds(replace(curArray(4),"""","")) & "','" & ds(replace(curArray(5),"""","")) & "','" & ds(replace(curArray(6),"""","")) & "')"
						session("errorSQL") = sql
						oConn.execute(sql)
					end if
				end if
			loop
			
			sql = "select count(*) as remaining from holding_musicSkins"
			session("errorSQL") = sql
			set rs = oConn.execute(sql)
			remaining = rs("remaining")
			rs = null
			
			userMsg = "CSV Added To Database<br>Process For Adding Products Starting"
		end if
	else
		addItems()
	end if
	
	'sql = "if not (select count(*) from we_items_musicSkins where musicSkinsID = '" & musicSkinsID & "') > 0 insert into we_items_musicSkins(name,code) values('Cult of the Laughing Hedgehog','CLH')"
%>
<table border="0" cellpadding="0" cellspacing="0" align="center">
	<% if userMsg <> "" then %>
    <tr><td align="center" style="font-weight:bold; color:#F00; padding-bottom:20px;"><%=userMsg%></td></tr>
    <% end if %>
    <%
	if remaining = 0 then
		sql = "select distinct top 500 artist, (select top 1 genre from we_items_musicSkins where artist = a.artist and genre is not null) as setGenre from we_items_musicSkins a where genre is null"
		session("errorSQL") = sql
		set genreRS = oConn.execute(sql)
		
		fixedGenres = 0
		notFixed = 0
		do while not genreRS.EOF
			artist = genreRS("artist")
			setGenre = genreRS("setGenre")
			if not isnull(setGenre) then
				fixedGenres = fixedGenres + 1
				sql = "update we_items_musicSkins set genre = '" & setGenre & "' where artist = '" & ds(artist) & "' and genre is null"
				session("errorSQL") = sql
				oConn.execute(sql)
			else
				notFixed = notFixed + 1
			end if
			genreRS.movenext
		loop
		
		if notFixed > 0 then response.Write("notFixed:" & notFixed & "<br>")
		if fixedGenres > 0 then response.Write("fixedGenres:" & fixedGenres & "<br>")
	%>
    <tr>
    	<td>
        	<div style="width:375px; border:1px solid #666; padding:5px; float:left">
                <div style="width:360px; float:left;">
                    <div style="float:left; font-weight:bold;">Starting Date:</div>
                    <div style="float:left;"><input type="text" name="startingDate" value="2011-04-04" size="8" onchange="chgDates('s',this.value)" /></div>
                    <div style="float:left; font-weight:bold; padding-left:10px;">Ending Date:</div>
                    <div style="float:left;"><input type="text" name="startingDate" value="2011-04-05" size="8" onchange="chgDates('e',this.value)" /></div>
                </div>
                <div style="width:350px; float:right; padding-top:10px;">
                    <div style="float:right;"><a id="generateLink" href="#" target="msProducts">Generate Music Skins File</a></div>
                </div>
            </div>
        </td>
    </tr>
	<tr><td align="center" style="padding:20px 0px 20px 0px;"><a href="http://data.musicskins.com/wirelessemporium/csv/products.csv">Downlod Music Skins File</a></td></tr>
    <tr>
    	<td>
        	<form enctype="multipart/form-data" action="/admin/musicSkins_upload.asp" method="post">
				<div style="padding:5px;">Upload new Music Skins Product File</div>
				<div style="text-align:right; padding:5px;"><input type="FILE" name="FILE1"></div>
				<div style="text-align:right; padding:5px; border-bottom:1px solid #000;"><input type="SUBMIT" name="upload" value="Upload"></div>
			</form>
        </td>
    </tr>
    <% else %>
    <tr><td align="center" style="font-weight:bold; font-size:18px;">Processing New Products<br />Please wait...</td></tr>
    <% end if %>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	<% if remaining = 0 then %>
	var startDateDefault = "2011-04-04"
	var endDateDefault = "2011-04-05"
	var genLinkDefault = "http://data.musicskins.com/wirelessemporium/products.php?from=##Start##&to=##End##&email=jon@wirelessemporium.com"
	document.getElementById("generateLink").href = 'http://data.musicskins.com/wirelessemporium/products.php?from=' + startDateDefault + '&to=' + endDateDefault + '&email=jon@wirelessemporium.com'
	function chgDates(dir,dateVal) {
		if (dir == 's') {
			startDateDefault = dateVal
			var tempLink = genLinkDefault.replace("##Start##",startDateDefault)
			document.getElementById("generateLink").href = tempLink.replace("##End##",endDateDefault)
		}
		else if (dir == 'e') {
			endDateDefault = dateVal
			var tempLink = genLinkDefault.replace("##Start##",startDateDefault)
			document.getElementById("generateLink").href = tempLink.replace("##End##",endDateDefault)
		}
	}
	<% else %>
	setTimeout("window.location='/admin/musicSkins_upload.asp'",2000)
	<% end if %>
</script>