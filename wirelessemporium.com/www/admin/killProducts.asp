<%
	thisUser = Request.Cookies("username")
	pageTitle = "Kill Products"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	dim partNumber : partNumber = prepStr(request.Form("partNumber"))
	dim typeID : typeID = prepInt(request.Form("typeID"))
	dim keywords : keywords = prepStr(request.Form("keywords"))
	dim myWhere : myWhere = ""
	
	if partNumber <> "" then myWhere = " and partNumber like '%" & replace(partNumber,"%","") & "%'"
	if typeID > 0 then myWhere = myWhere & " and typeID = " & typeID
	if keywords <> "" then myWhere = " and itemDesc like '%" & replace(keywords,"%","") & "%'"
	
	sql = "select top 100 a.*, b.brandName, c.modelName from we_items a left join we_brands b on a.brandID = b.brandID left join we_models c on a.modelID = c.modelID where a.hideLive = 1" & myWhere & " order by a.itemID"
	session("errorSQL") = sql
	Set rs = oConn.execute(sql)
	
	sql = "select typeID, typeName from we_types order by typeName"
	session("errorSQL") = sql
	set catRS = oConn.execute(sql)
%>
<table border="0" cellpadding="3" cellspacing="0" width="100%">
	<tr>
    	<td align="center">
        	<div style="width:650px; font-size:12px; text-align:left;">
            	<h1>WE Kill Products</h1>
                This page will pull the top 100 oldest products that are marked <strong>HideLive</strong>. 
                You will then have the option to make it a Ghost or delete the product from the system. 
                Once a product has been deleted it can no longer be accessed again.
            </div>
        </td>
    </tr>
    <tr>
    	<td align="center">
        	<form action="/admin/killProducts.asp" method="post">
        	<div style="padding:10px 0px 10px 0px; border-top:1px solid #000; border-bottom:1px solid #000; width:400px;">
                <div style="float:left; width:200px; text-align:right; padding-right:10px;">Part Number:</div>
   	            <div style="float:left;"><input type="text" name="partNumber" value="" /></div>
                <div style="float:left; width:400px; height:10px;"></div>
                <div style="float:left; width:200px; text-align:right; padding-right:10px;">Category:</div>
                <div style="float:left;">
                	<select name="typeID">
                    	<option value="">Select Category</option>
                        <%
						do while not catRS.EOF
						%>
                        <option value="<%=catRS("typeID")%>"><%=catRS("typeName")%></option>
                        <%
							catRS.movenext
						loop
						%>
                    </select>
                </div>
                <div style="float:left; width:400px; height:10px;"></div>
                <div style="float:left; width:200px; text-align:right; padding-right:10px;">Keyword(s):</div>
   	            <div style="float:left;"><input type="text" name="keywords" value="" /></div>
                <div style="float:left; width:400px; height:10px;"></div>
                <div style="display:block; text-align:right;"><input type="submit" name="mySub" value="Search" /></div>
            </div>
            </form>
        </td>
    </tr>
    <tr>
    	<td align="center">
        	<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="650">
            	<tr bgcolor="#333333" style="font-weight:bold; color:#FFF;">
                	<td>ItemID</td>
                    <td nowrap="nowrap">Part Number</td>
                    <td>Brand</td>
                    <td>Model</td>
                    <td width="100%">Product Description</td>
                    <td>Inv</td>
                    <td>Actions</td>
                </tr>
                <%
				bgColor = "#fff"
				do while not rs.EOF
					itemID = rs("itemID")
				%>
                <tr style="background-color:<%=bgColor%>; font-size:12px;" id="prodRow_<%=itemID%>">
                	<td align="right"><%=itemID%></td>
                    <td nowrap="nowrap"><%=rs("partNumber")%></td>
					<% if rs("brandID") = 0 and rs("modelID") = 0 then %>
                    <td colspan="2" align="center" style="font-weight:bold;">Universal</td>
                    <% else %>
                    <td nowrap="nowrap"><%=rs("brandName")%></td>
                    <td nowrap="nowrap"><%=rs("modelName")%></td>
                    <% end if %>
                    <td nowrap="nowrap">
						<%=rs("itemDesc")%> (<a style="color:#00F; text-decoration:underline; cursor:pointer;" onclick="viewPic(<%=itemID%>,'<%=rs("itemPic")%>')">view pic</a>)</div>
                        <div style="position:relative;"><div style="position:absolute; top:0px; left:0px; z-index:99;" id="picBox_<%=itemID%>"></div></div>
                    </td>
                    <td align="right"><%=rs("inv_qty")%></td>
                    <td nowrap="nowrap">
                    	<input type="button" name="myAction" value="Ghost" onclick="ghostProd(<%=itemID%>)" style="font-size:12px;" />
                        <input type="button" name="myAction" value="Delete" onclick="deleteProd(<%=itemID%>)" style="font-size:12px;" />
                    </td>
                </tr>
                <%
					rs.movenext
					if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
				loop
				%>
                <tr><td colspan="7" align="right"><a href="/admin/killProducts.asp">Pull More Products</a></td></tr>
            </table>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	var curPic = 0
	function viewPic(itemID,itemPic) {
		if (curPic > 0) { document.getElementById("picBox_" + curPic).innerHTML = "" }
		if (itemID != curPic) {
			if (document.getElementById("picBox_" + itemID).innerHTML == "") {
				document.getElementById("picBox_" + itemID).innerHTML = "<a style='cursor:pointer;' onclick=viewPic(" + itemID + ",'" + itemPic + "')><img src='/productPics/big/" + itemPic + "' border='1' /></a>"
				curPic = itemID
			}
		}
		else {
			curPic = 0
		}
	}
	function ghostProd(itemID) {
		document.getElementById("prodRow_" + itemID).style.display = 'none'
		ajax('/ajax/admin/ajaxKillProducts.asp?ghostProd=' + itemID,'dumpZone')
	}
	function deleteProd(itemID) {
		var yourstate=window.confirm("Are you sure you want to delete this product?")
		if (yourstate) {
			document.getElementById("prodRow_" + itemID).style.display = 'none'
			document.getElementById("testZone").innerHTML = '/ajax/admin/ajaxKillProducts.asp?deleteProd=' + itemID
			ajax('/ajax/admin/ajaxKillProducts.asp?deleteProd=' + itemID,'dumpZone')
		}
		else {
			return false
		}
	}
</script>