dim oConn
Set oConn = CreateObject("ADODB.Connection")
oConn.ConnectionString = "DSN=WEDB01;Uid=wireless_user;Pwd=c3llph0n3;"
oConn.ConnectionTimeout = 1000		'seconds
oConn.Open

dim fs, file, filename, path
filename = "productList_pepperjam.txt"
path = "e:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename

set fs = CreateObject("Scripting.FileSystemObject")
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if

'Create the file and write some data to it.
set file = fs.CreateTextFile(path)
dim SQL, RS
SQL = "SELECT A.itemID,A.PartNumber,A.brandID,A.itemDesc,A.itemPic,A.price_Retail,A.price_Our,A.inv_qty,A.Sports,A.flag1,A.UPCCode,A.itemLongDetail,"
SQL = SQL & "B.brandName,C.modelName,C.[temp],D.typeName"
SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
SQL = SQL & " LEFT JOIN we_Types D ON A.typeID=D.typeID"
SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
SQL = SQL & " ORDER BY A.itemID"
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
if not RS.eof then
	file.WriteLine "name" & vbtab & "sku" & vbtab & "buy_url" & vbtab & "image_url" & vbtab & "image_thumb_url" & vbtab & "description_long" & vbtab & "description_short" & vbtab & "price" & vbtab & "price_retail" & vbtab & "keywords" & vbtab & "manufacturer" & vbtab & "condition" & vbtab & "mpn" & vbtab & "upc"
	do until RS.eof
		DoNotInclude = 0
		if RS("inv_qty") < 0 then
			SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
			set RS2 = CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			if RS2.eof then DoNotInclude = 1
			RS2.close
			set RS2 = nothing
		end if
		if DoNotInclude = 0 then
			GRPNAME = RS("itemDesc")
			
			if isnull(RS("typeName")) then
				stypeName = "Universal"
			else 
				stypeName = RS("typeName")
			end if
			if isnull(RS("ModelName")) then
				ModelName = "Universal"
			else
				ModelName = RS("BrandName") & " > " & RS("ModelName")
			end if
			if isnull(RS("BrandName")) then
				BrandName = "Universal"
			else 
				BrandName = RS("BrandName")
			end if
			
			ShortDescription = RS("itemLongDetail")
			ShortDescription = replace(ShortDescription,vbcrlf," ")
			if len(ShortDescription) > 255 then ShortDescription = left(ShortDescription,252) & "..."
			
			file.write chr(34) & GRPNAME & chr(34) & vbtab	'name
			file.write RS("itemid") & vbtab	'sku
			file.write "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?id=ppjproduct" & vbtab	'buy_url
			file.write "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab	'image_url"
			file.write "http://www.wirelessemporium.com/productpics/thumb/" & RS("itemPic") & vbtab	'image_thumb_url
			file.write vbtab	'description_long
			file.write chr(34) & ShortDescription & chr(34) & vbtab	'description_short
			file.write RS("price_our") & vbtab	'price
			file.write RS("price_retail") & vbtab	'price_retail
			file.write chr(34) & stypeName & ", " & BrandName & ", " & ModelName & ", " & GRPNAME & chr(34) & vbtab	'keywords
			file.write chr(34) & BrandName & chr(34) & vbtab	'manufacturer
			file.write "new" & vbtab	'condition
			file.write RS("PartNumber") & vbtab	'mpn
			file.write RS("UPCCode") & vbtab	'upc
			file.write vbcrlf
		end if
		RS.movenext
	loop
end if
RS.close
set RS = nothing
oConn.close
file.close()

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function
