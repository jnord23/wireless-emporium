dim oConn
Set oConn = CreateObject("ADODB.Connection")
oConn.ConnectionString = "DSN=WEDB01;Uid=wireless_user;Pwd=c3llph0n3;"
oConn.ConnectionTimeout = 1000		'seconds
oConn.Open

dim fs, file, filename, path
filename = "productList_cj.txt"
path = "e:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename

set fs = CreateObject("Scripting.FileSystemObject")
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if

'Create the file and write some data to it.
set file = fs.CreateTextFile(path)
dim SQL, RS
SQL = "SELECT A.itemID,A.brandID,A.PartNumber AS MPN,A.itemDesc AS GRPNAME,A.itemLongDetail,"
SQL = SQL & "A.itemPic,A.price_Retail,A.price_our,A.inv_qty,A.Sports,A.flag1,A.UPCCode,"
SQL = SQL & "B.brandName,Rtrim(B.brandName) + ' > ' + C.modelName AS ModelName,C.[temp],D.typeName,C.modelName AS model"
SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
SQL = SQL & " LEFT JOIN we_Types D ON A.typeID=D.typeID"
SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
SQL = SQL & " ORDER BY A.itemID"
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
if not RS.eof then
	dim RS2, DoNotInclude, strItemDesc, strItemLongDetail, stypeName, ModelName, BrandName
	file.writeLine "&CID=1629329"
	file.writeLine "&SUBID=6534"
	file.writeLine "&PROCESSTYPE=OVERwrite"
	file.writeLine "&AID=10398960"
	file.writeLine "&PARAMETERS=NAME|KEYWORDS|DESCRIPTION|SKU|BUYURL|AVAILABLE|IMAGEURL|PRICE|RETAILPRICE|PROMOTIONALTEXT|ADVERTISERCATEGORY|MANUFACTURER|STANDARDSHIPPINGCOST"
	do until RS.eof
		DoNotInclude = 0
		if RS("inv_qty") < 0 then
			SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("MPN") & "'"
			set RS2 = CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			if RS2.eof then DoNotInclude = 1
			RS2.close
			set RS2 = nothing
		end if
		if DoNotInclude = 0 then
			strItemDesc = RS("GRPNAME")
			strItemDesc = replace(strItemDesc,"+","&")
			strItemLongDetail = RS("itemLongDetail")
			strItemLongDetail = replace(strItemLongDetail,vbcrlf," ")
			if isnull(RS("typeName")) then
				stypeName = "Universal Gear"
			else
				stypeName = RS("typeName")
			end if
			if isnull(RS("ModelName")) then
				ModelName = "Universal"
			else
				ModelName = RS("ModelName")
			end if
			if isnull(RS("BrandName")) then
				BrandName = "Universal"
			else 
				BrandName = RS("BrandName")
			end if
			
			file.write RS("GRPNAME") & vbtab
			file.write stypeName & "," & replace(ModelName," > "," ") & "," & strItemDesc & vbtab
			file.write strItemLongDetail & vbtab
			file.write "WE-" & RS("itemID") + 5000 & vbtab
			file.write "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("GRPNAME")) & ".asp" & vbtab
			file.write "YES" & vbtab
			file.write "http://www.wirelessemporium.com/productpics/thumb/" & RS("itemPic") & vbtab
			file.write RS("price_our") & vbtab
			file.write RS("price_retail") & vbtab
			file.write "Free Shipping on All Orders"  & vbtab
			file.write "Cell Phone Accessories > Cell Phone " & stypeName & " > " & ModelName & " > " & replace(ModelName," > "," ") & " " & stypeName & vbtab
			file.write BrandName & vbtab
			file.write "0" & vbcrlf
		end if
		RS.movenext
	loop
end if
RS.close
set RS = nothing
file.close()

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function
