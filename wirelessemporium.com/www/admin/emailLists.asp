<%
	thisUser = Request.Cookies("username")
	pageTitle = ""
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%	
	dim editList : editList = prepStr(request.Form("editList"))
	dim removeID : removeID = prepInt(request.QueryString("removeID"))
	dim showList : showList = 0
	dim addUserID : addUserID = prepInt(request.Form("addUserID"))
	dim addEmailAddr : addEmailAddr = prepStr(request.Form("addEmailAddr"))
	dim newEmail : newEmail = prepStr(request.Form("newEmail"))
	
	if removeID > 0 then
		sql = "delete from we_emailLists where id = " & removeID
		session("errorSQL") = sql
		oConn.execute(sql)
		
		response.Write("it's done")
		response.End()
	else
		if newEmail <> "" then
			sql = "insert into we_emailLists (emailName,adminID) values('" & newEmail & "',54)"
			session("errorSQL") = sql
			oConn.execute(sql)
			
			editList = newEmail
		end if
		if addUserID > 0 then
			sql = "insert into we_emailLists (emailName,adminID) values('" & editList & "'," & addUserID & ")"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		if addEmailAddr <> "" then
			sql = "insert into we_emailLists (emailName,emailAddr) values('" & editList & "','" & addEmailAddr & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
	end if
	
	if editList <> "" then
		sql = "select a.id, a.emailAddr, b.email, b.fname + ' ' + b.lname as userName from we_emailLists a left join we_adminUsers b on a.adminID = b.adminID where emailName = '" & editList & "'"
		session("errorSQL") = sql
		set listRS = oConn.execute(sql)
		
		if not listRS.EOF then showList = 1
		
		sql = "select adminID, fname + ' ' + lname as userName from we_adminUsers where active = 1 order by userName"
		session("errorSQL") = sql
		set adminAccountsRS = oConn.execute(sql)
	end if
	
	sql = 	"select distinct emailName from we_emailLists order by emailName"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
%>
<form name="emailForm" action="/admin/emailLists.asp" method="post">
<div style="width:300px; text-align:center; color:#999;" class="tb mCenter">Example: cdo_to = emailList("Project Complete")</div>
<div style="width:300px; padding:20px 0px;" class="tb mCenter">
	<div class="fl bl pr15">Select Email to Edit:</div>
    <div class="fl">
    	<select name="editList" onchange="document.emailForm.submit();">
        	<option value="">Select Email</option>
            <option value="new">Add New Email</option>
            <%
			do while not rs.EOF
				curEmail = rs("emailName")
			%>
            <option value="<%=curEmail%>"<% if editList = curEmail then %> selected="selected"<% end if %>><%=curEmail%></option>
            <%
				rs.movenext
			loop
			%>
        </select>
    </div>
</div>
</form>
<% if showList = 1 or editList = "new" then %>
<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="400" align="center">
	<tr style="background-color:#333; font-weight:bold; color:#FFF;">
    	<td>Name</td>
        <td>Email</td>
        <td>Delete</td>
    </tr>
    <%
	do while not listRS.EOF
		emailID = listRS("id")
		if isnull(listRS("emailAddr")) then
			useEmail = listRS("email")
		else
			useEmail = listRS("emailAddr")
		end if
	%>
    <tr>
    	<td><%=listRS("username")%></td>
        <td><%=useEmail%></td>
        <td><a href="javascript:popUser(<%=emailID%>)" style="color:#F00; font-weight:bold;">Remove</a></td>
    </tr>
    <%
		listRS.movenext
	loop
	%>
</table>
<form name="addUserForm" method="post" action="/admin/emailLists.asp">
<% if editList = "new" then %>
<div class="tb mCenter" style="width:300px; padding:20px 0px;">
	<div class="fl pr15 bl">New Email:</div>
    <div class="fl pr15"><input type="text" name="newEmail" value="" size="20" /></div>
    <div class="fl">
    	<input type="submit" name="myAction" value="Add" />
    </div>
</div>
<% else %>
<div class="tb mCenter" style="width:300px; padding:20px 0px;">
	<div class="fl pr15 bl">Add User:</div>
    <div class="fl pr15">
    	<select name="addUserID">
        	<option value="">Select Admin User</option>
            <%
			do while not adminAccountsRS.EOF
			%>
            <option value="<%=adminAccountsRS("adminID")%>"><%=adminAccountsRS("userName")%></option>
            <%
				adminAccountsRS.movenext
			loop
			%>
        </select>
    </div>
    <div class="fl"><input type="submit" name="myAction" value="Add" /></div>
</div>
<div class="tb mCenter" style="width:300px;">
	<div class="fl pr15 bl">Add Email:</div>
    <div class="fl pr15"><input type="text" name="addEmailAddr" value="" size="20" /></div>
    <div class="fl">
    	<input type="submit" name="myAction" value="Add" />
        <input type="hidden" name="editList" value="<%=editList%>" />
    </div>
</div>
<% end if %>
</form>
<% end if %>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function popUser(emailID) {
		var yourstate=window.confirm("Are you sure you want remove this user?")
		if (yourstate) {
			ajax('/admin/emailLists.asp?removeID=' + emailID,'dumpZone');
			setTimeout("document.emailForm.submit()",2000);
		}
	}
</script>