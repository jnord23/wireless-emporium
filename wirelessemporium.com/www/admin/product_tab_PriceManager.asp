<%
response.buffer = false
pageTitle = "Create Price Manager Product List"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
dim fs, file, filename, path
'Map the file name to the physical path on the server.
filename = "productList_PriceManager.txt"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Create Price Manager Product List</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							Response.write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href=""../tempCSV/" & filename & """>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	response.write("Creating " & filename & ".<br>")
	set file = fs.CreateTextFile(path)
	dim SQL, RS, DoNotInclude, we_itemID
	SQL = "SELECT A.itemID, A.PartNumber, A.itemDesc, A.itemPic, A.price_our, A.itemLongDetail, A.inv_qty, A.UPCCode, B.brandName, C.modelName, D.typeName"
	SQL = SQL & " FROM we_items A INNER JOIN we_brands B ON A.brandid = B.brandid"
	SQL = SQL & " INNER JOIN we_models C ON A.modelid = C.modelid"
	SQL = SQL & " INNER JOIN we_types D ON A.typeid = D.typeid"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " AND A.typeid = 16"
	SQL = SQL & " ORDER BY itemID"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "manufacturer" & vbtab & "model number" & vbtab & "SKU" & vbtab & "UPC" & vbtab & "product title" & vbtab & "product categories" & vbtab & "list price" & vbtab & "product URL"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				strline = RS("brandName") & vbtab
				strline = strline & RS("modelName") & vbtab
				strline = strline & RS("itemID") & vbtab
				strline = strline & RS("UPCCode") & vbtab
				strline = strline & RS("itemDesc") & vbtab
				strline = strline & RS("typeName") & vbtab
				strline = strline & formatCurrency(RS("price_our"),2) & vbtab
				strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"
				file.WriteLine strline
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
