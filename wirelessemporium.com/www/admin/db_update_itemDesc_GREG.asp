<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
call fOpenConn()
dim brandModel, aCount, IDs
SQL = "SELECT A.itemID, B.brandName, C.modelName FROM we_Items A INNER JOIN we_brands B ON A.brandID = B.brandID INNER JOIN we_Models C ON A.modelID = C.modelID"
SQL = SQL & " WHERE PartNumber LIKE 'BAT-%-01' AND itemID NOT IN (18750,12508,12509,2029)"
set RS = server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
aCount = 0
IDs = ""
do until RS.eof
	brandModel = RS("brandName") & "/" & RS("modelName")
	SQL = "UPDATE we_Items SET"
	SQL = SQL & " itemLongDetail = '" & brandModel & " Replacement Lithium Ion Battery � If you find yourself charging your phone multiple times a day your battery may be nearing the end of its usable life meaning it is time for a replacement battery. This " & brandModel & " battery is a perfect way to inject life back into your phone or just provide the security of having a backup. Guaranteed brand new and manufactured with premium grade lithium cells, this battery meets OEM specifications.',"
	SQL = SQL & " BULLET1 = 'Direct replacement to original battery',"
	SQL = SQL & " BULLET2 = 'Premium grade lithium cells',"
	SQL = SQL & " BULLET3 = 'Brand new battery',"
	SQL = SQL & " BULLET4 = 'Full Manufacturer 1-Yr Warranty',"
	SQL = SQL & " BULLET5 = '90-Day EZ No-Hassle Returns',"
	SQL = SQL & " BULLET6 = '',"
	SQL = SQL & " BULLET7 = '',"
	SQL = SQL & " BULLET8 = '',"
	SQL = SQL & " BULLET9 = '',"
	SQL = SQL & " BULLET10 = '',"
	
	SQL = SQL & " itemLongDetail_CA = 'Li-Ion Replacement Battery for " & brandModel & " � This brand new battery is the perfect choice for individuals in need of a spare battery or looking to replace a battery that isn''t holding its charge anymore. This " & brandModel & " battery is guaranteed to meet OEM standards and comes with Cellphoneaccents.com''s 100% Satisfaction Guarantee.',"
	
	SQL = SQL & " itemLongDetail_CO = '',"
	SQL = SQL & " POINT1 = 'Brand New',"
	SQL = SQL & " POINT2 = 'Fits into original desktop chargers and holsters',"
	SQL = SQL & " POINT3 = 'Use as a spare for the home or office',"
	SQL = SQL & " POINT4 = 'Guaranteed to meet OEM standards',"
	SQL = SQL & " POINT5 = 'Must fully charge before first use',"
	SQL = SQL & " POINT6 = 'Keep out of extreme heat/cold to preserve battery life',"
	SQL = SQL & " POINT7 = 'Talk and standby times vary depending on user habits',"
	SQL = SQL & " POINT8 = '30 day money back guarantee',"
	SQL = SQL & " POINT9 = '1 year warranty',"
	SQL = SQL & " POINT10 = '',"
	
	SQL = SQL & " itemLongDetail_PS = 'Standard Lithium-Ion Cell Battery for " & brandModel & " � Individuals looking to double up on battery life with a spare battery need to buy this Lithium-Ion battery for the " & brandModel & ". Also good as a replacement battery, this Lithium -Ion battery will have your phone functioning like new again. This battery is guaranteed to meet the same specifications as the battery that originally came with your phone for a fraction of the cost.',"
	SQL = SQL & " FEATURE1 = 'Guaranteed to meet OEM specs',"
	SQL = SQL & " FEATURE2 = 'Internal microchip prevents overcharging',"
	SQL = SQL & " FEATURE3 = 'Won''t suffer memory loss from partial charge',"
	SQL = SQL & " FEATURE4 = 'Use as a backup or a replacement',"
	SQL = SQL & " FEATURE5 = '1 year warranty, 30 day money back guarantee',"
	SQL = SQL & " FEATURE6 = '',"
	SQL = SQL & " FEATURE7 = '',"
	SQL = SQL & " FEATURE8 = '',"
	SQL = SQL & " FEATURE9 = '',"
	SQL = SQL & " FEATURE10 = ''"
	
	SQL = SQL & " WHERE itemID = '" & RS("itemID") & "'"
	response.write "<p>" & SQL & "</p>" & vbcrlf
	'oConn.execute SQL
	aCount = aCount + 1
	IDs = IDs & RS("itemID") & ","
	RS.movenext
loop
RS.close
set RS = nothing
call fCloseConn()

response.write "<h1>DONE!</h1>" & vbcrlf
response.write "<p>" & IDs & "</p>" & vbcrlf
response.write "<h3>" & aCount & " items updated.</h3>" & vbcrlf
%>
