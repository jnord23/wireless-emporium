<%
pageTitle = "phone order by tool"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<style>
.mouseover	{ background-color:#ACCEFF;}
</style>
<%
'response.write request.querystring
strSDate		=	prepStr(request("txtSDate"))
strEDate		=	prepStr(request("txtEDate"))
strReportType	=	prepStr(request("cbReportType"))
siteid			=	request("cbStore")
adminid			=	prepInt(request("cbAdmin"))
mobilesite		=	prepInt(request("cbMobile"))

if "EXCEL" = strReportType then
	response.redirect "/admin/report_phoneorders2_excel.asp?" & request.QueryString
	response.end
end if

if not isdate(strEDate) then
	sql	=	"select	convert(varchar(10), max(a.orderdatetime), 20) dateRecent from we_orders a join we_phoneorders b on a.orderid = b.orderid"
	session("errorSQL") = sql
	set rsDate = oConn.execute(sql)
	if rsDate.eof then 
		strEDate = date
	else
		strEDate = cdate(rsDate("dateRecent"))
	end if
end if	

if not isdate(strSDate) then
	strSDate = strEDate - 14
end if

strSDate = cdate(strsDate)
strEDate = cdate(strEDate)
'response.write "strSDate:" & strSDate & "<br>"
'response.write "strEDate:" & strEDate & "<br>"

sql	=	"select	a.store, convert(varchar(16), a.orderdatetime, 20) orderdatetime, a.orderid, a.accountid, b.specialnotes, c.fname + ' ' + c.lname adminName, d.shortDesc, d.color siteColor, e.email, e.phone, f.promoCode" & vbcrlf & _
		"	,	isnull(cast(a.ordergrandtotal as money), 0) orderGrandTotal, isnull(cast(a.ordersubtotal as money), 0) orderSubTotal, isnull(cast(a.ordershippingfee as money), 0) orderShippingFee, isnull(a.orderTax, 0) orderTax" & vbcrlf & _
		"	,	isnull(b.shippingDiscount, 0) shippingdiscount, isnull(b.itemDiscount, 0) itemDiscount" & vbcrlf & _
		"	,	a.shiptype, a.approved, isnull(a.cancelled, 0) cancelled, a.transactionID, a.mobileSite" & vbcrlf & _
		"from	we_orders a with (nolock) join we_phoneorders b with (nolock)" & vbcrlf & _
		"	on	a.orderid = b.orderid join we_adminUsers c with (nolock)" & vbcrlf & _
		"	on	b.adminid = c.adminid join xstore d with (nolock)" & vbcrlf & _
		"	on	a.store = d.site_id join v_accounts e with (nolock)" & vbcrlf & _
		"	on	a.accountid = e.accountid and a.store = e.site_id left outer join v_coupons f with (nolock)" & vbcrlf & _
		"	on	a.couponid = f.couponid and a.store = f.site_id " & vbcrlf & _
		"where	a.orderdatetime >= '" & strSDate & "' and a.orderdatetime < '" & (strEDate+1) & "'" & vbcrlf
if siteid <> "" then
	sql = sql & "	and	d.site_id = " & siteid & vbcrlf
end if
if adminid > 0 then
	sql = sql & "	and	b.adminid = " & adminid & vbcrlf
end if
if mobilesite > 0 then
	sql = sql & "	and	a.mobileSite = 1" & vbcrlf
end if
sql = sql & "order by 2 desc"
'response.write "<pre>" & sql & "</pre><br />"		
session("errorSQL") = sql
set rs = oConn.execute(sql)
%>
<form name="frmSearch" method="get">
<table width="1200" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td width="100%" align="left" valign="middle" style="padding-bottom:5px; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:24px; color:#333;">
        	Order History By Phone-Order tool
        </td>
	</tr>
	<tr>
		<td width="100%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top">
						<table width="100%" border="1" cellspacing="0" cellpadding="3" align="center" bordercolor="#FFFFFF" style="border-collapse:collapse;" bgcolor="#EAEAEA">
							<tr>
								<td class="normalText" align="center"><b>FILTER</b></td>
								<td class="normalText" align="center"><b>DATE</b></td>
								<td class="normalText" align="center"><b>SITE</b></td>
								<td class="normalText" align="center"><b>MOBILE SITE</b></td>
								<td class="normalText" align="center"><b>STORE</b></td>
								<td class="normalText" align="center"><b>REPORT TYPE</b></td>
								<td class="normalText" align="center"><b>Action</b></td>
							</tr>
							<tr>
								<td class="normalText" align="center"><b>VALUE</b></td>
								<td class="normalText" align="center">
									<input type="text" name="txtSDate" value="<%=strSDate%>" size="7"> ~ <input type="text" name="txtEDate" value="<%=strEDate%>" size="7">
								</td>
								<td class="normalText" align="center">
									<select name="cbMobel">
										<option value="">ALL</option>
										<option value="M" <%if mobileSite = "M" then %>selected<% end if%>>Mobile</option>
									</select>
								</td>
								<td class="normalText" align="center">
									<select name="cbMobile">
										<option value="">ALL</option>
										<option value="1" <%if mobilesite = "1" then %>selected<% end if%>>MOBILE</option>
									</select>
								</td>
								<td class="normalText" align="center">
									<select name="cbStore">
										<option value="">ALL</option>
										<option value="0" <%if siteid = "0" then %>selected<% end if%>>WE</option>
										<option value="1" <%if siteid = "1" then %>selected<% end if%>>CA</option>
										<option value="2" <%if siteid = "2" then %>selected<% end if%>>CO</option>
										<option value="3" <%if siteid = "3" then %>selected<% end if%>>PS</option>
										<option value="10" <%if siteid = "10" then %>selected<% end if%>>TM</option>
									</select>
								</td>
								<td class="normalText" align="center">
									<select name="cbReportType">
										<option value="WEB">WEB</option>
										<option value="EXCEL">EXCEL</option>
									</select>
                                </td>
								<td class="normalText" align="center"><input type="submit" value="Search">&nbsp;<!--<input type="button" name="btnExcel" value="Export" onclick="doSubmit('EXCEL');">--></td>
							</tr>	
						</table>
					</td>
				</tr>
				<tr>
					<td width="100%" valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="4" align="center">
							<tr>
								<td align="center" style="border-bottom:1px solid #F3671C;" rowspan="2"><b>STORE</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;" rowspan="2"><b>ORDERID</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;" rowspan="2"><b>ORDER<Br />DATETIME</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;" rowspan="2"><b>CUSTOMER<Br />EMAIL</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;" rowspan="2"><b>Customer<Br />Phone</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;" rowspan="2"><b>Grand<Br />Total</b></td>
                                <td align="center" style="border-bottom:1px solid #F3671C;" rowspan="2"><b>Discount<Br />Total</b></td>
								<td align="center" style="border-left:1px solid #F3671C; border-right:1px solid #F3671C; border-bottom:1px solid #F3671C;" colspan="3"><b>Discounts</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;" rowspan="2"><b>Ship Type</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;" rowspan="2"><b>Approved /<br />Cancelled</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;" rowspan="2"><b>Mobile</b></td>
								<td align="center" ><b>Admin User</b></td>
                            </tr>
							<tr>
								<td align="center" style="border-left:1px solid #F3671C; border-bottom:1px solid #F3671C;"><b>Promotion</b></td>
								<td align="center" style="border-bottom:1px solid #F3671C;"><b>Shipping</b></td>
								<td align="center" style="border-right:1px solid #F3671C; border-bottom:1px solid #F3671C;"><b>Item</b></td>
                                <td align="center" style="border-bottom:1px solid #F3671C;">
								<%
								sql = 	"select	case when department in ('customer service', 'Customer Support') then 1 else 0 end cs, *" & vbcrlf & _
										"from	we_adminusers" & vbcrlf & _
										"where	adminlvl >= 1" & vbcrlf & _
										"	and	active = 1" & vbcrlf & _
										"order by cs desc, department desc"
								set rsAdmin = oConn.execute(sql)
								%>
                                <select name="cbAdmin">
                                    <option value="">ALL</option>
                                <%
								do until rsAdmin.eof
									%>
                                    <option value="<%=rsAdmin("adminid")%>" <%if adminid = rsAdmin("adminid") then%> selected <%end if%>><%=rsAdmin("fname")%></option>
                                    <%
									rsAdmin.movenext
								loop
								%>
                                </select>
                                </td>
                            </tr>
							<%
                            if rs.eof then
                                response.write "<td colspan=""99"" align=""center"">no data to display</td>"
                            else
                                lap = -1
                                do until rs.eof
                                    lap = lap + 1
									nDiscountTotal = cdbl(0)
									nPromtionDiscount = (rs("orderSubTotal") + rs("orderShippingFee") + rs("orderTax")) - rs("orderGrandTotal")
									nDiscountTotal = nPromtionDiscount + rs("shippingdiscount") + rs("itemDiscount")
                                    %>
                                    <tr class="grid_row_<%=(lap mod 2)%>">
                                        <td align="center" style="color:<%=rs("siteColor")%>;"><%=rs("shortDesc")%></td>
                                        <td align="center">
                                        	<a href="#" onClick="printinvoice('<%=rs("orderid")%>','<%=rs("accountid")%>'); return false;"><%=rs("orderid")%></a>&nbsp;
                                        	<!--<a href="#" onClick="getOrderDetails(<%=rs("orderid")%>); return false;"><img src="/images/admin/phoneorder/mglass.jpg" border="0" width="16" /></a>&nbsp;-->
										</td>
                                        <td align="center"><%=rs("orderdatetime")%></td>
                                        <td align="center"><%=rs("email")%></td>
                                        <td align="center"><%=rs("phone")%></td>
                                        <td align="center"><%=formatcurrency(rs("orderGrandTotal"))%></td>
                                        <td align="center">
										<%
											if nDiscountTotal > 0 then
												response.write "<span style=""color:#900; font-weight:bold;"">" & formatcurrency(nDiscountTotal*(-1)) & "</span>"
											else
												response.write formatcurrency(nDiscountTotal*(-1))
											end if
										%>
                                        </td>
                                        <td align="center"><%=formatcurrency(nPromtionDiscount*(-1))%></td>
                                        <td align="center"><%=formatcurrency(rs("shippingdiscount")*(-1))%></td>
                                        <td align="center"><%=formatcurrency(rs("itemDiscount")*(-1))%></td>
                                        <td align="center"><%=rs("shiptype")%></td>
                                        <td align="center">
                                        <%
                                        if rs("approved") then 
                                            response.write "<span style=""color:#00F; font-weight:bold;"">Yes</span>"
                                        else
                                            response.write "No"
                                        end if
                                        response.write " / "
                                        if rs("cancelled") then 
                                            response.write "<span style=""color:#00F; font-weight:bold;"">Yes</span>"
                                        else
                                            response.write "No"
                                        end if
                                        %>
                                        </td>
                                        <td align="center">
                                        <%
										if rs("mobileSite") then
											response.write "<span style=""color:#00F; font-weight:bold;"">Yes</span>"
										else
											response.write "No"
										end if
										%>
										</td>
                                        <td align="center"><%=rs("adminName")%></td>
                                    </tr>
                                    <%
                                    rs.movenext
                                loop
                            end if
                            %>                            
						</table>
                    </td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<script>
function printinvoice(orderid,accountid) {
	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
