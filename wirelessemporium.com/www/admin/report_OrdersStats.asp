<%
pageTitle = "WE Admin Site - Daily Orders Statistics"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim dateStart : dateStart = prepStr(request.form("dateStart"))
	if not isDate(dateStart) then dateStart = date()
	siteID = prepInt(request.form("siteID"))
%>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">
<h3>Choose another date:</h3>
<br>
<form action="report_OrdersStats.asp" name="frmSalesReport" method="post">
    <select name="siteID">
        <option value="0">WE</option>
        <option value="2"<% if siteID = 2 then %> selected="selected"<% end if %>>CO</option>
        <option value="1"<% if siteID = 1 then %> selected="selected"<% end if %>>CA</option>
        <option value="3"<% if siteID = 3 then %> selected="selected"<% end if %>>PS</option>
        <option value="10"<% if siteID = 10 then %> selected="selected"<% end if %>>TM</option>
    </select>
	<p>
		<input type="text" name="dateStart" value="<%=dateStart%>">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.frmSalesReport.dateStart,document.frmSalesReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;Report&nbsp;Date
		<input type="hidden" name="dc2" value="<%=dateAdd("yyyy",1,date)%>">
		<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
	</p>
	<!--<p><input type="checkbox" name="details" value="1">&nbsp;&nbsp;Show&nbsp;Details</p>-->
	<p><input type="submit" name="submitted" value="Generate Report"></p>
</form>
<p>&nbsp;</p>
<%
showblank = "&nbsp;"
shownull = "-null-"

myTotal = 0

if request.querystring("details") = "1" then
	selectSQL = "A.*"
else
	selectSQL = "A.orderID, A.accountid, A.ordergrandtotal, A.orderdatetime"
end if

if strError = "" then
	SQL = "SELECT " & selectSQL & " FROM we_orders A INNER JOIN we_accounts B ON A.accountid=B.accountid"
	SQL = SQL & " WHERE A.orderdatetime >= '" & dateStart & "' AND A.orderdatetime < '" & dateAdd("D",1,dateStart) & "'"
	SQL = SQL & " AND A.approved = 1"
	SQL = SQL & " AND (A.cancelled = 0 OR A.cancelled IS NULL)"
	SQL = SQL & " AND A.store = " & siteID
	SQL = SQL & " AND (A.extOrderType IS NULL OR A.extOrderType < 4)"
	SQL = SQL & " AND (B.email = 'phoneorder@wirelessemporium.com' OR B.email NOT LIKE '%@wirelessemporium.com')"
	SQL = SQL & " AND a.parentorderid is null"	
	SQL = SQL & " ORDER BY A.orderdatetime"
	session("errorSQL") = SQL
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	
	if RS.eof then
		response.write "No records matched<br><br>So cannot make table..."
	else
		dim lap : lap = 0
		dim orderDetails : orderDetails = ""
		do until RS.eof
			orderID = RS("orderID")
			accountID = RS("accountID")
			grandTotal = RS("ordergrandtotal")
			orderDate = RS("orderdatetime")
			lap = lap + 1
			
			orderDetails = orderDetails & orderID & "##" & accountID & "##" & grandTotal & "##" & orderDate & "$$"
			
			myTotal = myTotal + cDbl(grandTotal)
			RS.movenext
		loop
		response.write "<h3>DAILY ORDERS STATISTICS FOR: " & dateStart & "</h3>"
		response.write "<h3>TOTAL ORDERS: " & lap & "</h3>"
		response.write "<h3>TOTAL ORDER AMOUNT: " & formatCurrency(myTotal) & "</h3>"
		response.write "<h3>AVERAGE ORDER AMOUNT: " & formatCurrency(myTotal/lap) & "</h3>"
		%>
		<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0">
			<tr style="background-color:#000; font-weight:bold; color:#FFF;">
				<td>orderID</td>
                <td>accountID</td>
                <td>orderTotal</td>
                <td>orderDate</td>
			</tr>
			<%
			bgColor = "#ffffff"
			orderArray = split(orderDetails,"$$")
			for i = 0 to (ubound(orderArray) - 1)
				curArray = split(orderArray(i),"##")
				%>
				<tr bgcolor="<%=bgColor%>">
                	<td align="right"><%=curArray(0)%></td>
					<td align="right"><%=curArray(1)%></td>
                    <td align="right"><%=formatCurrency(curArray(2),2)%></td>
                    <td><%=curArray(3)%></td>
				</tr>
				<%
				if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
			next
			%>
		</table>
		<%
	end if
	
	RS.close
	Set RS = nothing
else
	%>
	<center>
	<font size="+2" color="red"><%=strError%></font>
	<a href="javascript:history.back();">BACK</a>
	</center>
	<%
end if
%>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->

<script language="javascript">
function printinvoice(orderid,accountid) {
	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
</script>
