<%header = 1%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<style>
.mouseover	{ background-color:#9BCEFF;}
</style>
<%
today = date
strSDate = request("startDate")
strEDate = request("endDate")
if isDate(strEDate) then
	strEDate = cdate(strEDate) + 1
end if	
minAmount = prepInt(request("minAmt"))
chkApproved = request("chkApproved")
chkCancelled = request("chkCancelled")
hidExcel = request("hidExcel")
curSite = request("cbSite")
if curSite = "" then curSite = "-1"
if hidExcel = "Y" then
	response.redirect "/admin/report_Whales_excel.asp?" & request.QueryString
end if

sql = "select	site_id, shortDesc from	xstore order by 1"
session("errorSQL") = sql
set rsSite = oConn.execute(sql)
%>
<form action="report_Whales.asp" method="get" name="frmSearch">
	<p>
    	Store : 
        <select name="cbSite">
        	<option value="-1" <%if curSite = "-1" then%>selected<%end if%>>All Stores</option>
		<%
		do until rsSite.eof
			%>
            <option value="<%=rsSite("site_id")%>" <%if cstr(curSite) = cstr(rsSite("site_id")) then%>selected<%end if%> ><%=rsSite("shortDesc")%></option>
            <%
			rsSite.movenext
		loop
		%>
        </select>
    </p>
	<p>
    	Start Date: <input type="text" name="startDate" value="<%=strSDate%>" >
		<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.frmSearch.startDate,<%=today%>);return false;" style="text-decoration:none;">
        	<img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt="">
		</a>
		<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>    
    	&nbsp; ~ &nbsp;
        End Date: <input type="text" name="endDate" value="<%=strEDate%>" >
		<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.frmSearch.endDate,<%=today%>);return false;" style="text-decoration:none;">
        	<img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt="">
		</a>
		<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
	</p>
	<p>Approved: <input type="checkbox" name="chkApproved" <%if chkApproved = "on" then %>checked<% end if%> /> &nbsp; &nbsp; &nbsp; Cancelled: <input type="checkbox" name="chkCancelled" <%if chkCancelled = "on" then %>checked<% end if%> /></p>
	<p>Minimum Dollar Amount(OrderSubTotal): <input type="text" name="minAmt" value="<%=minAmount%>"></p>
	<p><input type="submit" name="submitted" value="Submit"> &nbsp; &nbsp; <input type="image" name="submitExcel" src="/images/icon-excel-small.gif" onclick="return doSubmitExcel();" /></p>
    <input type="hidden" name="hidExcel" value="" />
</form>
<%
function YesNo(val)
	if val = true then
		YesNo = "Yes"
	else
		YesNo = "No"
	end if
end function

strError = ""

if request("submitted") <> "" then
	if not isDate(strSDate) then strError = strError & "<h3>Start Date must be a valid date.</h3>"
	if strEDate <> "" then
		if not isDate(strEDate) then strError = strError & "<h3>End Date must be a valid date.</h3>"
	end if
	if minAmount = "" then strError = strError & "<h3>You must enter a Minimum Dollar Amount.</h3>"
	
	if strError = "" then
		sql	=	"select	x.shortDesc, x.color, a.orderid, a.accountid, b.email, a.orderdatetime, isnull(cast(a.ordergrandtotal as money), 0) ordergrandtotal" & vbcrlf & _
				"	, 	isnull(cast(a.ordersubtotal as money), 0) ordersubtotal, isnull(cast(a.ordershippingfee as money), 0) ordershippingfee, a.approved, a.cancelled" & vbcrlf & _
				"from	we_orders a join v_accounts b" & vbcrlf & _
				"	on	a.store = b.site_id and a.accountid = b.accountid join xstore x" & vbcrlf & _
				"	on	a.store = x.site_id" & vbcrlf & _
				"where	a.orderdatetime >= '" & strSDate & "'" & vbcrlf & _
				"	and	cast(a.ordersubtotal as money) >= " & minAmount & vbcrlf
		if curSite <> "" and curSite <> "-1" then sql = sql & "	and x.site_id = '" & curSite & "'" & vbcrlf
		if isDate(strEDate) then sql = sql & "	and	a.orderdatetime < '" & strEDate & "'" & vbcrlf
		if chkApproved = "on" then sql	= sql & "	and	a.approved = 1" & vbcrlf
		if chkCancelled = "on" then 
			sql	= sql & "	and	a.cancelled = 1" & vbcrlf
		else
			sql	= sql & "	and	(a.cancelled = 0 or a.cancelled is null)" & vbcrlf
		end if
		
		sql = sql & "order by x.site_id, a.orderdatetime desc"
'		response.write "<h3><pre>" & SQL & "</pre></h3>" & vbCrLf

		Set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 0, 1
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			%>
            <center>
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>Store</b></td>
					<td><b>Order&nbsp;ID</b></td>
					<td><b>E-Mail</b></td>
					<td><b>Order&nbsp;Notes</b></td>
					<td><b>Order&nbsp;Date/Time</b></td>
					<td><b>Grand&nbsp;Total</b></td>
					<td><b>SubTotal</b></td>
					<td><b>ShippingFee</b></td>
					<td><b>Approved</b></td>
					<td><b>Cancelled</b></td>
				</tr>
				<tr><td colspan="99" width="2" bgcolor="#E95516" style="padding:0px;"><img src="/images/spacer.gif" width="2" height="2" border="0"></td></tr>
				<%
				nCnt = 0
				lap = 0
				do until RS.eof
					lap = lap + 1
					nCnt = nCnt + 1
					%>
					<tr class="grid_row_<%=(lap mod 2)%>" onMouseOver="this.className='mouseover'" onMouseOut="this.className='grid_row_<%=(lap mod 2)%>'" >
						<td class="smlText" valign="top" style="color:<%=rs("color")%>;"><%=rs("shortDesc")%></td>
						<td class="smlText" valign="top"><a href="javascript:printinvoice('<%=RS("orderid")%>','<%=RS("accountid")%>');"><%=RS("orderid")%></a></td>
						<td class="smlText" valign="top"><%=RS("email")%></td>
						<td class="smlText" valign="top"><a href="javascript:fnopennotes('<%=RS("orderid")%>');">show&nbsp;notes</a></td>
						<td class="smlText" valign="top"><%=RS("orderdatetime")%></td>
						<td class="smlText" valign="top" align="right" style="padding-right:10px;"><%=formatCurrency(RS("ordergrandtotal"))%></td>
						<td class="smlText" valign="top" align="right" style="padding-right:10px;"><%=formatCurrency(RS("ordersubtotal"))%></td>
						<td class="smlText" valign="top" align="right" style="padding-right:10px;"><%=formatCurrency(RS("ordershippingfee"))%></td>
						<td class="smlText" valign="top" align="center"><%=YesNo(RS("approved"))%></td>
						<td class="smlText" valign="top" align="center"><%=YesNo(RS("cancelled"))%></td>
					</tr>
					<%
					if nCnt > 2000 then
						nCnt = 0
						response.Flush()
					end if
					RS.movenext
				loop
				%>
			</table>
			<h3><%=lap%> records found</h3>            
            </center>
			<%
		end if
		RS.close
		Set RS = nothing
	else
		response.write "<font color=""#FF0000"">" & strError & "</font>" & vbcrlf
	end if
end if
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->

<script language="javascript">
function printinvoice(orderid,accountid) {
	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}

function fnopennotes(orderid) {
	var url='ordernotes.asp?orderid='+orderid;
	window.open(url,'ordernote','left=0,top=0,width=800,height=300,resizable=1,locationbar=0,menubar=0,toolbar=0');
}

function doSubmitExcel()
{
	document.frmSearch.hidExcel.value = 'Y';
	return true;
}
</script>
