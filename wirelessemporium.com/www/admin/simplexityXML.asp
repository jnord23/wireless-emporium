<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Process Simplexity XML"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim ftp, success, localFilePath, remoteFileName
	
	localFilePath = Server.MapPath("/xmlFiles/simplexity") & "\"
	localFileName = "EquipmentDataFeed_phonesale.xml"
	localFullPath = localFilePath & localFileName
	remoteFileName = "EquipmentDataFeed_phonesale.xml"
		
	set ftp = CreateObject("Chilkat.Ftp2")
	success = ftp.UnlockComponent("WIRELEFTP_GQ2TEoLE9Bnh")
	if success <> 1 then 
		response.write "ftp.UnlockComponent ERROR - " & ftp.LastErrorText
		response.end
	end if
	
	ftp.Hostname = "ftp.simplexity.com"
	ftp.Username = "WirelEmp2"
	ftp.Password = "w!r3lmp2!"
	ftp.Passive = 1

	success = ftp.Connect()
	if success <> 1 then 
		response.write "ftp connection error - " & ftp.LastErrorText
		response.end		
	end if

	success = ftp.GetFile(ftp.GetCurrentRemoteDir() & remoteFileName, localFullPath)
	if success <> 1 then 
		response.write "ftp download error - <br>" & ftp.LastErrorText
		response.end		
	end if

	dim objXMLDOM, strError, oNode, sql, rs
	Set objXMLDOM = Server.CreateObject("Microsoft.XMLDOM")
	objXMLDOM.async = False
	objXMLDOM.load (localFullPath)

	
	if objXMLDOM.parseError.errorCode <> 0 then
		strError = strError & "<h3>Parser error found.</h3>"
	else
		dim carrier, brand, model, modelExt, prodType, retailPrice, displayPrice, afterRebatePrice, purchaseMode, link
		set oNode = objXMLDOM.getElementsByTagName("Table")
		if (not oNode is nothing) then
			dim nodeLap : nodeLap = -1
			dim actualProdLap : actualProdLap = 0
			dim insertProd : insertProd = 0
			dim updateProd : updateProd = 0
			for each prod in oNode
				nodeLap = nodeLap + 1
				carrier = objXMLDOM.getElementsByTagName("Table").Item(nodeLap).childNodes(0).text
				brand = objXMLDOM.getElementsByTagName("Table").Item(nodeLap).childNodes(1).text
				model = objXMLDOM.getElementsByTagName("Table").Item(nodeLap).childNodes(2).text
				modelExt = objXMLDOM.getElementsByTagName("Table").Item(nodeLap).childNodes(4).text
				prodType = objXMLDOM.getElementsByTagName("Table").Item(nodeLap).childNodes(5).text
				retailPrice = objXMLDOM.getElementsByTagName("Table").Item(nodeLap).childNodes(8).text
				displayPrice = objXMLDOM.getElementsByTagName("Table").Item(nodeLap).childNodes(9).text
				afterRebatePrice = objXMLDOM.getElementsByTagName("Table").Item(nodeLap).childNodes(13).text
				purchaseMode = objXMLDOM.getElementsByTagName("Table").Item(nodeLap).childNodes(18).text
				link = objXMLDOM.getElementsByTagName("Table").Item(nodeLap).childNodes(19).text
				link = "http://www.cellstores.com/" & link
				if purchaseMode = "New Contract" then
					actualProdLap = actualProdLap + 1
					sql = "select id from xSimplexity where carrier = '" & carrier & "' and brand = '" & brand & "' and model = '" & model & "'"
					session("errorSQL") = sql
					set rs = oConn.execute(sql)
					if rs.EOF then
						insertProd = insertProd + 1
						sql = "insert into xSimplexity (carrier,brand,model,modelExt,retailPrice,displayPrice,afterRebatePrice,link) values('" & carrier & "','" & brand & "','" & model & "','" & modelExt & "','" & retailPrice & "','" & replace(displayPrice,"$","") & "','" & replace(afterRebatePrice,"$","") & "','" & link & "')"
						session("errorSQL") = sql
						oConn.execute(sql)
					else
						updateProd = updateProd + 1
						sql = "update xSimplexity set link = '" & link & "', modelExt = '" & modelExt & "', retailPrice = '" & retailPrice & "', displayPrice = '" & replace(displayPrice,"$","") & "',afterRebatePrice = '" & replace(afterRebatePrice,"$","") & "' where id = " & rs("id")
						session("errorSQL") = sql
						oConn.execute(sql)
					end if
				end if				
			next
			response.Write("all done!<br>insertProd:" & insertProd & "<br>updateProd:" & updateProd)
			'response.Write("Start:" & oNode.item(0).text & ":End<br><br>")
		else
			strError = strError & "<h3>No data received.</h3>"
		end if
	end if

	set objXMLDOM = nothing
	set oNode = nothing
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->