<%
response.buffer = false
pageTitle = "WE Admin Site - New Items Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">
<%
if request("submitted") <> "" then
	dim strError, dateStart, dateEnd
	strError = ""
	dateStart = request.form("dateStart")
	dateEnd = request.form("dateEnd")
	
	if not isDate(dateStart) or not isDate(dateEnd) then
		strError = strError & "<p>dateStart and dateEnd must both be valid dates</p>"
	else
		dateStart = dateValue(dateStart)
		dateEnd = dateValue(dateEnd)
		if dateEnd < dateStart then strError = strError & "<p>dateEnd must be later than or equal to dateStart</p>"
	end if
	
	if strError = "" then
		SQL = "SELECT A.itemID, A.DateTimeEntd, A.PartNumber, A.WE_URL, B.brandName, C.modelName, A.itemDesc, A.price_Our, A.price_CA, A.price_CO, A.price_PS, A.COGS"
		SQL = SQL & " FROM we_Items AS A INNER JOIN we_Brands AS B ON A.brandID = B.brandID INNER JOIN we_Models AS C ON A.modelID = C.modelID"
		SQL = SQL & " WHERE A.DateTimeEntd >= '" & dateStart & "' AND A.DateTimeEntd <= '" & dateAdd("d",1,dateEnd) & "'"
		SQL = SQL & " ORDER BY A.DateTimeEntd DESC"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			dim strToWrite, fs, path, myFile
			set fs = CreateObject("Scripting.FileSystemObject")
			path = Server.MapPath("tempCSV") & "\report_New_Items.csv"
			set myFile = fs.CreateTextFile(path)
			myFile.WriteLine "itemID,DateTimeEntd,PartNumber,Brand,Model,itemDesc,WE,CA,CO,PS,COGS"
			%>
			<h3><%=RS.recordcount%> records found</h3>
			<p><a href="/admin/tempCSV/report_New_Items.csv">Download CSV File</a></p>
			<table border="1">
				<tr>
					<td><b>itemID</b></td>
					<td><b>DateTimeEntd</b></td>
					<td><b>PartNumber</b></td>
					<td><b>Brand</b></td>
					<td><b>Model</b></td>
					<td><b>itemDesc</b></td>
					<td><b>WE</b></td>
					<td><b>CA</b></td>
					<td><b>CO</b></td>
					<td><b>PS</b></td>
					<td><b>COGS</b></td>
				</tr>
				<%
				do until RS.eof
					%>
					<tr>
						<td valign="top"><%=RS("itemID")%></td>
						<td valign="top"><%=RS("DateTimeEntd")%></td>
						<td valign="top"><%=RS("PartNumber")%></td>
						<td valign="top"><%=RS("brandName")%></td>
						<td valign="top"><%=RS("modelName")%></td>
						<td valign="top"><a href="/products/<%=RS("WE_URL")%>.asp" target="_blank"><%=RS("itemDesc")%></a></td>
						<td valign="top"><%=moneyCell(RS("price_Our"))%></td>
						<td valign="top"><%=moneyCell(RS("price_CA"))%></td>
						<td valign="top"><%=moneyCell(RS("price_CO"))%></td>
						<td valign="top"><%=moneyCell(RS("price_PS"))%></td>
						<td valign="top"><%=moneyCell(RS("COGS"))%></td>
					</tr>
					<%
					strToWrite = RS("itemID") & ","
					strToWrite = strToWrite & RS("DateTimeEntd") & ","
					strToWrite = strToWrite & RS("PartNumber") & ","
					strToWrite = strToWrite & chr(34) & RS("brandName") & chr(34) & ","
					strToWrite = strToWrite & chr(34) & RS("modelName") & chr(34) & ","
					strToWrite = strToWrite & chr(34) & RS("itemDesc") & chr(34) & ","
					strToWrite = strToWrite & moneyCSV(RS("price_Our")) & ","
					strToWrite = strToWrite & moneyCSV(RS("price_CA")) & ","
					strToWrite = strToWrite & moneyCSV(RS("price_CO")) & ","
					strToWrite = strToWrite & moneyCSV(RS("price_PS")) & ","
					strToWrite = strToWrite & moneyCSV(RS("COGS"))
					myFile.WriteLine strToWrite
					RS.movenext
				loop
				%>
			</table>
			<%
		end if
		RS.close
		set RS = nothing
		myFile.close()
		set myFile = nothing
		set fs = nothing
	else
		%>
		<center>
		<font size="+2" color="red"><%=strError%></font>
		<a href="javascript:history.back();">BACK</a>
		</center>
		<%
	end if
else
	%>
	<p>&nbsp;</p>
	<h3>New Items Report</h3>
	<br>
	<form action="report_newItems.asp" method="post">
		<p><input type="text" name="dateStart">&nbsp;&nbsp;dateStart</p>
		<p><input type="text" name="dateEnd">&nbsp;&nbsp;dateEnd</p>
		<p><input type="submit" name="submitted" value="Generate Report"></p>
	</form>
	<%
end if

function moneyCell(val)
	if isNull(val) then
		moneyCell = "<i>NULL</i>"
	else
		moneyCell = formatCurrency(val)
	end if
end function

function moneyCSV(val)
	if isNull(val) then
		moneyCSV = ""
	else
		moneyCSV = formatCurrency(val)
	end if
end function
%>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
