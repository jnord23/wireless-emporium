<%
	thisUser = Request.Cookies("username")
	pageTitle = "Delete Compressed Files"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim website : website = prepStr(request.Form("website"))
	dim myAction : myAction = prepStr(request.Form("myAction"))
	
	if myAction = "Delete Files" then
		set fso = CreateObject("Scripting.FileSystemObject")
		if website = "WE" then
			activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\*.htm"
			response.Write("pop base...<br>")
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0

			activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\brand\*.htm"
			response.Write("pop brand...<br>")
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
			
			activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\brandModel\*.htm"
			response.Write("pop brandModel...<br>")
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
			
			activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\brandModelCat\*.htm"
			response.Write("pop brandModelCat...<br>")
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
			
			activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\brandModelCatDetails\*.htm"
			response.Write("pop brandModelCatDetails...<br>")
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
			
			activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\product\*.htm"
			response.Write("pop product...<br>")
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
		elseif website = "WE (Homepage Only)" then
			activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\index.htm"
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
		elseif website = "WE (Brand Only)" then
			activeFiles = "C:\inetpub\wwwroot\wirelessemporium.com\www\compressedPages\brand_*.htm"
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
		elseif website = "CO" then
			activeFiles = "C:\inetpub\wwwroot\cellularoutfitter.com\www\compressedPages\*.htm"
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
			
			activeFiles = "C:\inetpub\wwwroot\cellularoutfitter.com\www\compressedPages\products\*.htm"
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
			
			activeFiles = "C:\inetpub\wwwroot\cellularoutfitter.com\www\compressedPages\bmc\*.htm"
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
		elseif website = "CO (Products Only)" then
			activeFiles = "C:\inetpub\wwwroot\cellularoutfitter.com\www\compressedPages\products\*.htm"
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
		elseif website = "CO (BMC Only)" then
			activeFiles = "C:\inetpub\wwwroot\cellularoutfitter.com\www\compressedPages\bmc\*.htm"
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
		elseif website = "CO (Homepage Only)" then
			activeFiles = "C:\inetpub\wwwroot\cellularoutfitter.com\www\compressedPages\index.htm"
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
		elseif website = "CA" then
			activeFiles = "C:\inetpub\wwwroot\cellphoneaccents.com\www\compressedPages\*.htm"
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
			
			activeFiles = "C:\inetpub\wwwroot\cellphoneaccents.com\www\compressedPages\bmc\*.htm"
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
		elseif website = "CA (BMC Only)" then
			activeFiles = "C:\inetpub\wwwroot\cellphoneaccents.com\www\compressedPages\bmc\*.htm"
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
		elseif website = "CA (Homepage Only)" then
			activeFiles = "C:\inetpub\wwwroot\cellphoneaccents.com\www\compressedPages\index.htm"
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
		elseif website = "PS" then
			activeFiles = "C:\inetpub\wwwroot\phonesale.com\www\compressedPages\products\*.htm"
			session("errorSQL") = activeFiles
			On Error Resume Next
			fso.deleteFile(activeFiles)
			On Error Goto 0
		end if
	end if
%>
<form action="/admin/deleteCompFiles.asp" method="post">
<table border="0" cellpadding="3" cellspacing="0" align="center">
    <tr>
    	<td align="right" style="font-weight:bold; font-size:14px;">Select Website:</td>
        <td align="left">
            <select name="website">
                <option value="WE">WE (All)</option>
                <option value="WE (Homepage Only)">WE (Homepage Only)</option>
                <option value="WE (Brand Only)">WE (Brand Only)</option>
                <option value="CO">CO (All)</option>
                <option value="CO (Homepage Only)">CO (Homepage Only)</option>
                <option value="CO (Products Only)">CO (Products Only)</option>
                <option value="CO (BMC Only)">CO (BMC Only)</option>
                <option value="CA">CA (All)</option>
                <option value="CA (Homepage Only)">CA (Homepage Only)</option>
                <option value="CA (BMC Only)">CA (BMC Only)</option>
                <option value="PS">PS (All)</option>
            </select>
        </td>
    </tr>
    <tr><td align="center" colspan="2"><input type="submit" name="myAction" value="Delete Files" /></td></tr>
</table>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
