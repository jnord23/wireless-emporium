<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Missing COGS"
	header = 1
	
	sbpn = ds(request.QueryString("sbpn"))
	
	if isnull(sbpn) or len(sbpn) < 1 then sbpn = ""
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	sql = "select distinct a.partNumber, a.cogs, b.cogs as useCogs from we_items a left join we_items b on a.partNumber = b.partNumber and b.cogs is not null and b.cogs > 0 where (a.cogs is null or a.cogs <= 0) and (b.cogs is not null and b.cogs > 0)"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	do while not rs.EOF
		sql = "update we_items set cogs = " & rs("useCogs") & " where partNumber = '" & rs("partNumber") & "'"
		session("errorSQL")
		oConn.execute(sql)
		
		sql = "insert into we_adminActions (adminID,action) values (" & adminID & ",'MissingCogs.asp updated the cogs for: " & rs("partNumber") & "')"
		session("errorSQL")
		oConn.execute(sql)
		
		rs.movenext
	loop
	
	if sbpn = "" then
		sql = "select top 50 a.partnumber, a.itemDesc, a.price_our from we_items a where (a.cogs is null or a.cogs <= 0) and a.hideLive = 0 and a.inv_qty > 0 order by a.partNumber"
	else
		sbpn = replace(sbpn,"%","")
		sql = "select top 50 a.partnumber, a.itemDesc, a.price_our from we_items a where a.partNumber like '%" & sbpn & "%' and (a.cogs is null or a.cogs <= 0) and a.hideLive = 0 and a.inv_qty > 0 order by a.partNumber"
	end if
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
%>
<table align="center" cellpadding="3" cellspacing="0" style="border:1px solid #000;">
    <tr bgcolor="#cccccc">
    	<td colspan="6" align="left" style="font-weight:bold;">Search By Part Number: <input type="text" name="sbpn" value="" onkeyup="chkCode(event.keyCode, this.value)" onchange="window.location='/admin/missingCogs.asp?sbpn=' + this.value" /></td>
    </tr>
    <tr style=" background-color:#333; font-weight:bold; color:#FFF;">
        <td align="left">#</td>
        <td align="left">Part Number</td>
        <td align="left">Item Description</td>
        <td align="left">WE Price</td>
        <td align="left">COGS</td>
        <td><div style="width:100px;">Result</div></td>
    </tr>
    <%
    lap = 0
	bgColor = "#ffffff"
    do while not rs.EOF
        lap = lap + 1
    %>
    <tr bgcolor="<%=bgColor%>">
        <td align="center"><%=lap%></td>
        <td align="left"><%=rs("partnumber")%></td>
        <td align="left"><%=rs("itemDesc")%></td>
        <td align="right"><%=formatCurrency(rs("price_our"),2)%></td>
        <td align="left"><input type="text" name="cogs_<%=lap%>" value="" size="6" onfocus="document.getElementById('result_<%=lap%>').innerHTML=''" onchange="saveCOGS(<%=lap%>,'<%=rs("partnumber")%>',this.value)" /></td>
        <td align="left" id="result_<%=lap%>"></td>
    </tr>
    <%
        rs.movenext
		if bgColor = "#ffffff" then bgColor = "#f0f0f0" else bgColor = "#ffffff"
    loop
    %>
    <tr bgcolor="#333333"><td colspan="6" align="right"><input type="button" name="myAction" value="Refresh Page" onclick="window.location='/admin/missingCogs.asp'" /></td></tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function saveCOGS(lap,pn,cogs) {
		ajax("/ajax/admin/saveCOGS.asp?pn=" + pn + "&cogs=" + cogs,"result_" + lap)
	}
	function chkCode(code,val) {
		if (code == 13) {
			window.location = "/admin/missingCogs.asp?sbpn=" + val
		}
	}
</script>
