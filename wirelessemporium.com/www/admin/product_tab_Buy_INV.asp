<%
pageTitle = "Product Inventory upload for Buy.com"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<%
dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
'Map the file name to the physical path on the server.
filename = "Buy.com - Inventory.txt"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product Inventory upload for Buy.com</p>
							<p><input type="text" name="lastItemID">&nbsp;Last Item ID</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							lastItemID = request.form("lastItemID")
							if not isNumeric(lastItemID) then
								response.write "<h3>You must enter a valid &quot;Last Item ID&quot;</h3>" & vbcrlf
							else
								response.write("<b>CreateFile:</b><br>")
								DeleteFile(path)
								CreateFile(path)
								response.write "Here is the <a href='../tempCSV/" & filename & "'>file</a>"
							end if
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.Write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	response.write "Creating " & filename & ".<br>"
	set file = fs.CreateTextFile(path)
	SQL = "SELECT A.itemID, A.typeID, A.PartNumber AS MPN, A.price_our, A.price_Buy, A.inv_qty, A.hideLive, A.BUY_sku, B.modelName"
	SQL = SQL & " FROM we_Items A"
	SQL = SQL & " INNER JOIN we_Models B ON A.modelID=B.modelID"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " AND A.itemID > '" & lastItemID & "'"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "##Type=Inventory;Version=3.0"
		file.WriteLine "ListingId" & vbtab & "ProductId" & vbtab & "ProductIdType" & vbtab & "ItemCondition" & vbtab & "Price" & vbtab & "MAP" & vbtab & "Quantity" & vbtab & "OfferExpeditedShipping" & vbtab & "Description" & vbtab & "ShippingRateStandard" & vbtab & "ShippingRateExpedited" & vbtab & "ShippingLeadTime" & vbtab & "ReferenceId"
		do until RS.eof
			if RS("hideLive") = true then
				qty = 0
			else
				if RS("inv_qty") < 0 then
					SQL = "SELECT inv_qty FROM we_Items WHERE hideLive = 0 AND inv_qty > -1 AND PartNumber = '" & RS("MPN") & "'"
					set RS2 = Server.CreateObject("ADODB.Recordset")
					RS2.open SQL, oConn, 3, 3
					if not RS2.eof then
						qty = RS2("inv_qty")
					else
						qty = 0
					end if
					RS2.close
					set RS2 = nothing
				else
					qty = RS("inv_qty")
				end if
			end if
			
			MfgProductIdentifier = "WE" & RS("itemid") & RS("MPN")
			MfgProductIdentifier = replace(MfgProductIdentifier,"-","",1,2)
			
			if RS("typeID") = 16 then
				'MfgProductIdentifier = RS("modelName")
				priceOur = RS("price_Buy")
			else
				'MfgProductIdentifier = "WE" & RS("itemid") & RS("MPN")
				'MfgProductIdentifier = replace(MfgProductIdentifier,"-","",1,2)
				if RS("price_Buy") > 0 then
					priceOur = formatNumber(cdbl(RS("price_Buy")))
				elseif isNumeric(RS("price_our")) then
					priceOur = formatNumber(cdbl(RS("price_our")) * .9,2)
				else
					priceOur = RS("price_our")
				end if
			end if
			
			strline = vbtab													'ListingId
			if isNull(RS("BUY_sku")) then
				strline = strline & RS("MPN") & "-" & RS("itemID") & vbtab	'ProductId
				strline = strline & "3" & vbtab								'ProductIdType
			else
				strline = strline & RS("BUY_sku") & vbtab					'ProductId
				strline = strline & "0" & vbtab								'ProductIdType
			end if
			strline = strline & "1" & vbtab									'ItemCondition
			strline = strline & priceOur & vbtab							'Price
			strline = strline & vbtab										'MAP
			strline = strline & qty & vbtab									'Quantity
			strline = strline & "1" & vbtab									'OfferExpeditedShipping
			strline = strline & vbtab										'Description
			strline = strline & "2.99" & vbtab								'ShippingRateStandard
			strline = strline & "8.99" & vbtab								'ShippingRateExpedited
			strline = strline & vbtab										'ShippingLeadTime
			strline = strline & MfgProductIdentifier						'ReferenceId
			file.WriteLine strline
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
