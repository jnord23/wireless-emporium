<%
pageTitle = "Look Up Handsfree Types for a Specific Model"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">
<p class="biggerText"><%=pageTitle%></p>
<br>

<%
if request.querystring("brandID") = "" then
	SQL = "SELECT brandID,brandName FROM WE_Brands ORDER BY brandName"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	do until RS.eof
		%>
		<a href="/admin/report_models_handsfree_types.asp?brandID=<%=RS("brandID")%>"><%=RS("brandName")%></a><br>
		<%
		RS.movenext
	loop
	RS.close
	set RS = nothing
elseif request.querystring("brandID") <> "" and request.querystring("modelID") = "" then
	SQL = "SELECT modelID,modelName FROM WE_Models WHERE brandID='" & request.querystring("brandID") & "' ORDER BY modelName"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	do until RS.eof
		%>
		<a href="/admin/report_models_handsfree_types.asp?brandID=<%=request.querystring("brandID")%>&modelID=<%=RS("modelID")%>"><%=RS("modelName")%></a><br>
		<%
		RS.movenext
	loop
	RS.close
	set RS = nothing
elseif request.querystring("modelid") <> "" then
	SQL = "SELECT A.*,B.brandName FROM WE_Models A INNER JOIN WE_Brands B ON A.brandID=B.brandID WHERE A.modelID='" & request.querystring("modelID") & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	response.write "<table border=""1"" cellpadding=""5""><tr><td><b>Model&nbsp;ID</b></td><td><b>Brand&nbsp;Name</b></td><td><b>Model&nbsp;Name</b></td><td><b>Handsfree&nbsp;Type</b></td></tr>" & vbcrlf
	do until RS.eof
		myArray = split(RS("HandsfreeTypes"),",")
		for a = 0 to Ubound(myArray)
			if myArray(a) <> "" then
				response.write "<tr><td>" & RS("modelID") & "</td><td>" & RS("brandName") & "</td><td>" & RS("modelName") & "</td><td>" & strHandsfreeTypes(myArray(a)) & "</td></tr>" & vbcrlf
			end if
		next
		RS.movenext
	loop
	response.write "</table>" & vbcrlf
	RS.close
	set RS = nothing
	%>
	<p><a href="/admin/report_models_handsfree_types.asp">Look Up Handsfree Type for Another Model</a></p>
	<table border="1" cellpadding="0" cellspacing="0" width="200" class="smlText">
		<tr>
			<td colspan="2" bgcolor="#CCCCCC">Handsfree Type Index</td>
		</tr>
		<%
		for a = 1 to 25
			response.write "<tr><td>" & a & "</td><td>" & strHandsfreeTypes(a) & "</td></tr>" & vbcrlf
		next
		%>
	</table>
	<%
end if

function strHandsfreeTypes(HandsfreeTypes)
	select case HandsfreeTypes
		case "1" : strHandsfreeTypes = "UNIVERSAL 2.5mm"
		case "2" : strHandsfreeTypes = "BLUETOOTH"
		case "3" : strHandsfreeTypes = "3.5mm Stereo"
		case "4" : strHandsfreeTypes = "MINI USB"
		case "5" : strHandsfreeTypes = "MICRO USB"
		case "6" : strHandsfreeTypes = "Old Sony Ericcson"
		case "7" : strHandsfreeTypes = "K750 / Z520"
		case "8" : strHandsfreeTypes = "Chocolate"
		case "9" : strHandsfreeTypes = "i1000"
		case "10" : strHandsfreeTypes = "2.5 w/PTT"
		case "11" : strHandsfreeTypes = "NOK 7210"
		case "12" : strHandsfreeTypes = "NOK 3390"
		case "13" : strHandsfreeTypes = "SAM R225"
		case "14" : strHandsfreeTypes = "SAM T809"
		case "15" : strHandsfreeTypes = "M300"
		case "16" : strHandsfreeTypes = "E105 / U550"
		case "17" : strHandsfreeTypes = "SAM E315"
		case "18" : strHandsfreeTypes = "SAM S105"
		case "19" : strHandsfreeTypes = "Siemens"
		case "20" : strHandsfreeTypes = "C150"
		case "21" : strHandsfreeTypes = "HTC 3125"
		case "22" : strHandsfreeTypes = "HTC 8525"
		case "23" : strHandsfreeTypes = "APPLE"
		case "24" : strHandsfreeTypes = "Palm"
		case "25" : strHandsfreeTypes = "Treo 650"
		case "26" : strHandsfreeTypes = "SAM 2.5MM"
	end select
end function
%>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
