<%
pageTitle = "Create Atomic Mall Product List TXT"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<%
Server.ScriptTimeout = 9000 'seconds

dim fs, file, filename, filename2, filename3, filename4, path, path2, path3, path4, loopCount, currentFile
'Map the file name to the physical path on the server.
filename = "productList_AtomicMall.txt"
path = replace(server.mappath("tempCSV") & "\" & filename,"admin\","")
filename2 = "productList_AtomicMall2.txt"
path2 = replace(server.mappath("tempCSV") & "\" & filename2,"admin\","")
filename3 = "productList_AtomicMall3.txt"
path3 = replace(server.mappath("tempCSV") & "\" & filename3,"admin\","")
filename4 = "productList_AtomicMall4.txt"
path4 = replace(server.mappath("tempCSV") & "\" & filename4,"admin\","")
currentFile = filename
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List upload for Atomic Mall</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							Response.Write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							if fs.FileExists(path) then response.write "<p>Here is <a href=""/tempCSV/" & filename & """>file #1</a></p>" & vbcrlf
							if fs.FileExists(path2) then response.write "<p>Here is <a href=""/tempCSV/" & filename2 & """>file #2</a></p>" & vbcrlf
							if fs.FileExists(path3) then response.write "<p>Here is <a href=""/tempCSV/" & filename3 & """>file #3</a></p>" & vbcrlf
							if fs.FileExists(path4) then response.write "<p>Here is <a href=""/tempCSV/" & filename4 & """>file #4</a></p>" & vbcrlf
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		response.write "<p>Deleting " & filename & ".<br>"
		fs.DeleteFile(path)
	end if
	if fs.FileExists(path2) then
		response.write "<p>Deleting " & filename2 & ".<br>"
		fs.DeleteFile(path2)
	end if
	if fs.FileExists(path3) then
		response.write "<p>Deleting " & filename3 & ".<br>"
		fs.DeleteFile(path3)
	end if
	if fs.FileExists(path4) then
		response.write "<p>Deleting " & filename4 & ".<br>"
		fs.DeleteFile(path4)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	Response.Write("Creating " & filename & ".<br>")
	set file = fs.CreateTextFile(path)
	dim HeaderLine
	HeaderLine = "itemname" & vbTab & "qty" & vbTab & "price" & vbTab & "condition" & vbTab & "mfr" & vbTab & "shiptime" & vbTab & "shipto" & vbTab & "shipping_reg" & vbTab & "shipping_exp" & vbTab & "shipping_intl" & vbTab & "method_reg" & vbTab & "method_exp" & vbTab & "method_intl" & vbTab & "image" & vbTab & "image_2" & vbTab & "image_3" & vbTab & "image_4" & vbTab & "image_5" & vbTab & "image_6" & vbTab & "upc" & vbTab & "sku" & vbTab & "description" & vbTab & "notes" & vbTab & "specs" & vbTab & "warranty" & vbTab & "reg_price" & vbTab & "sale_expdate" & vbTab & "category" & vbTab & "subcategory" & vbTab & "subsub" & vbTab & "storecat"
	dim SQL, RS
	SQL = "SELECT A.itemID,A.PartNumber,A.brandID,A.typeID,A.itemDesc,A.itemPic,A.price_Retail,A.price_Our,A.inv_qty,A.hideLive,"
	SQL = SQL & "A.BULLET1,A.BULLET2,A.BULLET3,A.BULLET4,A.BULLET5,A.BULLET6,A.COMPATIBILITY,A.HandsfreeType,A.UPCCode,A.itemLongDetail,"
	SQL = SQL & "B.brandName,C.modelName,C.[temp],D.typeName"
	SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
	SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
	SQL = SQL & " LEFT JOIN we_Types D ON A.typeID=D.typeID"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		dim AddlImages(2)
		file.WriteLine HeaderLine
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				dim strItemDesc, LongDescription, price_our, subcategory
				dim myDate, thisMonth, thisDay, expDate
				myDate = dateAdd("d",30,date)
				thisMonth = cStr(month(myDate))
				if month(myDate) < 10 then thisMonth = "0" & thisMonth
				thisDay = cStr(day(myDate))
				if day(myDate) < 10 then thisDay = "0" & thisDay
				expDate = cStr(year(myDate)) & "-" & thisMonth & "-" & thisDay
				
				if not isNull(RS("itemDesc")) then strItemDesc = replace(RS("itemDesc"),"+","&")
				LongDescription = RS("itemLongDetail")
				if not isNull(LongDescription) then
					LongDescription = replace(LongDescription,vbCrLf," ")
					LongDescription = replace(LongDescription,vbTab," ")
					LongDescription = replace(LongDescription,"<a href='/downloads/","<a href='http://www.wirelessemporium.com/downloads/")
				end if
				
				if not isNull(RS("BULLET1")) and RS("BULLET1") <> "" then
					if right(RS("BULLET1"),1) <> "." and right(RS("BULLET1"),1) <> "!" then LongDescription = LongDescription & "."
					LongDescription = LongDescription & " "
				end if
				if not isNull(RS("BULLET2")) and RS("BULLET2") <> "" then
					LongDescription = LongDescription & RS("BULLET2")
					if right(RS("BULLET2"),1) <> "." and right(RS("BULLET2"),1) <> "!" then LongDescription = LongDescription & "."
					LongDescription = LongDescription & " "
				end if
				if not isNull(RS("BULLET3")) and RS("BULLET3") <> "" then
					LongDescription = LongDescription & RS("BULLET3")
					if right(RS("BULLET3"),1) <> "." and right(RS("BULLET3"),1) <> "!" then LongDescription = LongDescription & "."
					LongDescription = LongDescription & " "
				end if
				if not isNull(RS("BULLET4")) and RS("BULLET4") <> "" then
					LongDescription = LongDescription & RS("BULLET4")
					if right(RS("BULLET4"),1) <> "." and right(RS("BULLET4"),1) <> "!" then LongDescription = LongDescription & "."
					LongDescription = LongDescription & " "
				end if
				if not isNull(RS("BULLET5")) and RS("BULLET5") <> "" then
					LongDescription = LongDescription & RS("BULLET5")
					if right(RS("BULLET5"),1) <> "." and right(RS("BULLET5"),1) <> "!" then LongDescription = LongDescription & "."
					LongDescription = LongDescription & " "
				end if
				if not isNull(RS("BULLET6")) and RS("BULLET6") <> "" then
					LongDescription = LongDescription & RS("BULLET6")
					if right(RS("BULLET6"),1) <> "." and right(RS("BULLET6"),1) <> "!" then LongDescription = LongDescription & "."
					LongDescription = LongDescription & " "
				end if
				if not isNull(RS("COMPATIBILITY")) and RS("COMPATIBILITY") <> "" then
					COMPATIBILITY = replace(RS("COMPATIBILITY"),vbcrlf,"<br>")
					LongDescription = LongDescription & "Compatible with: " & COMPATIBILITY
					if right(COMPATIBILITY,1) <> "." and right(COMPATIBILITY,1) <> "!" then LongDescription = LongDescription & "."
				end if
				
				select case RS("typeID")
					case 1
						if RS("price_our") >= 7 and inStr(RS("itemDesc"),"flashing") = 0 then
							price_our = int(RS("price_our") * .7) + .99
						else
							price_our = 0
						end if
						subcategory = "Batteries & Chargers"
					case 2
						price_our = int(RS("price_our") * .65) + .99
						subcategory = "Batteries & Chargers"
					case 3
						if inStr(RS("PartNumber"),"FP2") > 0 then
							price_our = int(RS("price_our") * .7) - .01
						else
							price_our = 0
						end if
						subcategory = "Skins"
					case 5
						if inStr(RS("itemDesc"),"Audio Adapter") > 0 then
							price_our = int(RS("price_our") * .8) + .99
						elseif RS("HandsfreeType") <> 2 then
							price_our = (int(RS("price_our") * .7) - .01) + 1
						else
							price_our = 0
						end if
						subcategory = "Phone Accessories"
					case 6
						price_our = int(RS("price_our") * .75) + .99
						subcategory = "Cases & Holsters"
					case 7
						if inStr(RS("PartNumber"),"FP3") > 0 then
							price_our = int(RS("price_our") * .70) + .99
						elseif inStr(RS("PartNumber"),"LC5") > 0 then
							price_our = int(RS("price_our") * .75) + .99
						elseif inStr(RS("PartNumber"),"LC1") > 0 then
							price_our = int(RS("price_our") * .75) + .99
						elseif inStr(RS("PartNumber"),"LC2") > 0 then
							price_our = int(RS("price_our") * .75) + .99
						elseif inStr(RS("PartNumber"),"LC0") > 0 then
							price_our = int(RS("price_our") * .75) + .99
						else
							price_our = 0
						end if
						subcategory = "Cases & Holsters"
					case 13
						if inStr(RS("PartNumber"),"MEM-KIN-MCRO") > 0 or inStr(RS("PartNumber"),"MEM-SAN-MCRO") > 0 then
							price_our = int(RS("price_our") * .85) + .99
						elseif inStr(RS("PartNumber"),"DAT-UNI-BLUE") > 0 then
							price_our = int(RS("price_our") * .8) - .01
						elseif inStr(RS("PartNumber"),"DAT-KIT-MBAC") > 0 or inStr(RS("PartNumber"),"DTP-LGE-UNIV") > 0 or inStr(RS("PartNumber"),"DTP-SAM-UNIV") > 0 or inStr(RS("PartNumber"),"DTP-MOT-UNIV") > 0 or inStr(RS("PartNumber"),"DTP-SYO-UNIV") > 0 then
							price_our = int(RS("price_our") * .85) + .99
						else
							price_our = int(RS("price_our") * .75) + .99
						end if
						subcategory = "Phone Accessories"
					case else
						price_our = 0
				end select
				
				dim qty, RS2
				select case RS("inv_qty")
					case -1
						SQL = "SELECT inv_qty FROM we_Items WHERE inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
						set RS2 = Server.CreateObject("ADODB.Recordset")
						RS2.open SQL, oConn, 3, 3
						if RS2.eof then
							qty = 0
						else
							qty = RS2("inv_qty")
						end if
					case 0 : qty = 0
					case else : qty = RS("inv_qty")
				end select
				if RS("hideLive") = -1 or isNull(RS("hideLive")) then qty = 0
				
				if price_our > 0 then
					for iCount = 0 to 2
						AddlImages(iCount) = ""
						imgPath = "e:\inetpub\wwwroot\wirelessemporium.com\productpics\AltViews\" & replace(RS("itemPic"),".jpg","-" & iCount & ".jpg")
						if fs.FileExists(imgPath) then AddlImages(iCount) = replace(replace(imgPath,"e:\inetpub\wwwroot\wirelessemporium.com","http://www.wirelessemporium.com"),"\","/") & "|"
						imgPath = "e:\inetpub\wwwroot\wirelessemporium.com\productpics\AltViews\" & replace(RS("itemPic"),".jpg","-" & iCount & ".gif")
						if fs.FileExists(imgPath) then AddlImagesiCount = replace(replace(imgPath,"e:\inetpub\wwwroot\wirelessemporium.com","http://www.wirelessemporium.com"),"\","/") & "|"
						imgPath = "e:\inetpub\wwwroot\wirelessemporium.com\productpics\AltViews\" & replace(RS("itemPic"),".gif","-" & iCount & ".jpg")
						if fs.FileExists(imgPath) then AddlImagesiCount = replace(replace(imgPath,"e:\inetpub\wwwroot\wirelessemporium.com","http://www.wirelessemporium.com"),"\","/") & "|"
						imgPath = "e:\inetpub\wwwroot\wirelessemporium.com\productpics\AltViews\" & replace(RS("itemPic"),".gif","-" & iCount & ".gif")
						if fs.FileExists(imgPath) then AddlImagesiCount = replace(replace(imgPath,"e:\inetpub\wwwroot\wirelessemporium.com","http://www.wirelessemporium.com"),"\","/") & "|"
					next
					
					strline = chr(34) & strItemDesc & chr(34) & vbtab	'itemname
					strline = strline & qty & vbtab	'qty
					strline = strline & price_our & vbtab	'price
					strline = strline & 1 & vbtab	'condition
					strline = strline & vbtab	'mfr
					strline = strline & 1 & vbtab	'shiptime
					strline = strline & "USA 50 States" & vbtab	'shipto
					strline = strline & 0 & vbtab	'shipping_reg
					strline = strline & 6.99 & vbtab	'shipping_exp
					strline = strline & vbtab	'shipping_intl
					strline = strline & "USPS 1st-Class Mail" & vbtab	'method_reg
					strline = strline & "USPS Priority Mail" & vbtab	'method_exp
					strline = strline & vbtab	'method_intl
					strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab	'image
					strline = strline & AddlImages(0) & vbtab	'image_2
					strline = strline & AddlImages(1) & vbtab	'image_3
					strline = strline & AddlImages(2) & vbtab	'image_4
					strline = strline & vbtab	'image_5
					strline = strline & vbtab	'image_6
					strline = strline & RS("UPCCode") & vbtab	'upc
					strline = strline & RS("itemid") & vbtab	'sku
					strline = strline & chr(34) & LongDescription & chr(34) & vbtab	'description
					strline = strline & vbtab	'notes
					strline = strline & vbtab	'specs
					strline = strline & "1-Yr. Full Manufacturer Warranty" & vbtab	'warranty
					strline = strline & RS("price_Retail") & vbtab	'reg_price
					strline = strline & expDate & vbtab	'sale_expdate
					strline = strline & "Cellular" & vbtab	'category
					strline = strline & subcategory & vbtab	'subcategory
					strline = strline & vbtab	'subsub
					strline = strline & ""	'storecat
					file.WriteLine strline
					
					loopCount = loopCount + 1
					if loopCount >= 5000 and currentFile = filename then
						currentFile = filename2
						file.close()
						set file = fs.CreateTextFile(path2)
						file.WriteLine HeaderLine
					elseif loopCount >= 10000 and currentFile = filename2 then
						currentFile = filename3
						file.close()
						set file = fs.CreateTextFile(path3)
						file.WriteLine HeaderLine
					elseif loopCount >= 15000 and currentFile = filename3 then
						currentFile = filename4
						file.close()
						set file = fs.CreateTextFile(path4)
						file.WriteLine HeaderLine
					end if
				end if
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub	
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
