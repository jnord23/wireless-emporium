<%
pageTitle = "Product List upload for SHOP.com"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 9000 'seconds

dim fs, file, filename, path
'Map the file name to the physical path on the server.
set fs = CreateObject("Scripting.FileSystemObject")
filename = "productList_Shop.txt"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List upload for SHOP.com</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							response.write "<b>CreateFile:</b><br>"
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='../tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
	
<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		response.write "Deleting " & filename & ".<br>"
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	Option_Type = ""
	Option_Value = ""
	Permutation = ""
	Permutation_Code = ""
	
	'Create the file and write some data to it.
	response.write "Creating " & filename & ".<br>"
	set file = fs.CreateTextFile(path)
	dim SQL, RS
	SQL = "SELECT A.itemID, A.brandID, A.PartNumber AS MPN, A.itemDesc, A.itemLongDetail,"
	SQL = SQL & " A.itemPic, A.price_Retail, A.price_our, A.inv_qty, A.UPCCode, A.itemWeight,"
	SQL = SQL & " B.brandName, C.modelName, D.typeName"
	SQL = SQL & " FROM we_Items A LEFT JOIN we_Brands B ON A.brandID = B.BrandID"
	SQL = SQL & " LEFT JOIN we_Models C ON A.modelID = C.modelID"
	SQL = SQL & " LEFT JOIN we_Types D ON A.typeID = D.typeID"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " AND A.typeid <> 16"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		strHeader = "Group Name" & vbtab & "Group Description"  & vbtab & "Line Item Code" & vbtab & "Line Item Name" & vbtab & "Line Item Price" & vbtab & "Image URL" & vbtab & "Department" & vbtab & "Sub-Department" & vbtab & "Sub-Sub-Department" & vbtab & "Keywords" & vbtab & "Line Item Sale Price" & vbtab & "Per Product Shipping" & vbtab & "Line Item Inventory Status" & vbtab & "Weight" & vbtab & "UPC Code" & vbtab & "Manfacturer Name" & vbtab & "Manufacturer Part Number" & vbtab & "ISBN" & vbtab & "Brand" & vbtab & "Cost Per Click" & vbtab & "EAN" & vbtab & "URL to Product Page" & vbtab & "Alternate Image Link Text" & vbtab & "Alternate Image Reference" & vbtab & "Cross Sell" & vbtab & "Condition" & vbtab & "Attribute"
		file.writeLine strHeader
		holdMPN = "9999999999"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty < 1 AND PartNumber = '" & RS("MPN") & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				strItemDesc = RS("itemDesc")
				strItemDesc = replace(strItemDesc,"+","&")
				
				if not isNull(RS("itemLongDetail")) then
					strItemLongDetail = replace(RS("itemLongDetail"),vbcrlf," ")
				else
					strItemLongDetail = ""
				end if
				
				if isnull(RS("typeName")) then
					stypeName = "Universal"
				else
					stypeName = RS("typeName")
				end if
				
				if isnull(RS("BrandName")) then
					BrandName = "Universal"
				else
					BrandName = RS("BrandName")
				end if
				
				if isnull(RS("ModelName")) then
					ModelName = "Universal"
				else
					ModelName = RS("brandName") & " > " & RS("ModelName")
				end if
				
				keywords = RS("typeName") & "," & RS("brandName") & "," & RS("modelName") & "," & RS("itemDesc")
				
				thisItemID = cStr(RS("itemid"))
				thisMPN = RS("MPN")
				
				strToWrite = strItemDesc & vbtab
				strToWrite = strToWrite & strItemLongDetail & vbtab
				strToWrite = strToWrite & thisItemID & vbtab
				strToWrite = strToWrite & strItemDesc & vbtab
				strToWrite = strToWrite & RS("price_retail") & vbtab
				strToWrite = strToWrite & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab
				strToWrite = strToWrite & stypeName & vbtab
				strToWrite = strToWrite & BrandName & vbtab
				strToWrite = strToWrite & ModelName & vbtab
				strToWrite = strToWrite & keywords & vbtab
				strToWrite = strToWrite & RS("price_our") & vbtab
				strToWrite = strToWrite & 0 & vbtab
				strToWrite = strToWrite & 1000 & vbtab
				strToWrite = strToWrite & RS("itemWeight") & vbtab
				strToWrite = strToWrite & RS("UPCCode") & vbtab
				strToWrite = strToWrite & BrandName & vbtab
				strToWrite = strToWrite & thisMPN & vbtab
				strToWrite = strToWrite & vbtab	'ISBN
				strToWrite = strToWrite & BrandName & vbtab
				strToWrite = strToWrite & vbtab	'Cost Per Click
				strToWrite = strToWrite & vbtab	'EAN
				strToWrite = strToWrite & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?utm_source=ShopCom&utm_medium=CPC&utm_campaign=Feed" & vbtab
				strToWrite = strToWrite & vbtab	'Alternate Image Link Text
				strToWrite = strToWrite & vbtab	'Alternate Image Reference
				strToWrite = strToWrite & vbtab	'Cross Sell
				strToWrite = strToWrite & "New" & vbtab
				strToWrite = strToWrite & "[v2.3](1585623)Electronics/(1799964)Cell Phone Accessory"
				
				if thisMPN <> holdMPN then
					if Option_Type = "Manufacturer > Model" then
						file.write strToWrite & Option_Type & vbtab & left(Option_Value,len(Option_Value)-1) & vbtab & left(Permutation,len(Permutation)-3) & vbtab & chr(34) & left(Permutation_Code,len(Permutation_Code)-1) & chr(34) & vbcrlf
					else
						Option_Value = ""
						Permutation = ""
						Permutation_Code = ""
						file.write strToWrite & Option_Type & vbtab & Option_Value & vbtab & Permutation & vbtab & Permutation_Code & vbcrlf
					end if
					Option_Type = ""
				else
					Option_Type = "Manufacturer > Model"
					Option_Value = Option_Value & ModelName & ","
					Permutation = Permutation & ModelName & " | "
					Permutation_Code = Permutation_Code & thisItemID & ","
				end if
				holdMPN = thisMPN
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
