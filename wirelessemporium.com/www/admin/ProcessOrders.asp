<%
Server.ScriptTimeout = 1000

pageTitle = "Admin - Process Wireless Emporium Orders"
header = 1

dim filesys, demofolder, fil, filecoll, demofile
set filesys = CreateObject("Scripting.FileSystemObject")
set demofolder = filesys.GetFolder(server.MapPath("\admin\tempPDF\SalesOrders\"))
set filecoll = demofolder.Files
'for each fil in filecoll
'	if fil.DateLastModified < dateAdd("M",-3,date) then
'		set demofile = filesys.GetFile(server.MapPath("\admin\tempPDF\SalesOrders\" & fil.name))
'		demofile.Delete
'	end if
'next
set filesys = nothing

today = date
formData = request.form
dim errMsg : errMsg = request("errMsg")
dim dateSearch : dateSearch = request.Form("txtSearchDate")
dim siteid : siteid = request.form("cbSite")
dim shiptypeid : shiptypeid = request.form("cbShiptype")
dim shiptypeid2 : shiptypeid2 = request.form("cbShiptype2")
dim dateSearchEnd
if dateSearch = "" then
	dateSearch = today
end if
dateSearchEnd = cdate(dateSearch) + 1

if formData = "" then formData = "txtSearchDate=" & dateSearch & "&cbSite=" & siteid & "&cbShiptype=" & shiptypeid & "&cbShiptype2=" & shiptypeid2
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_switchChars.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<link href="/includes/css/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/includes/js/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/includes/js/cal.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function () {
		$('#id_sdate').simpleDatepicker();
	});
</script>

<script>
function showPop(processPDF, shiptoken2) 
{
	var url="processOrders_pop.asp?processPDF=" + escape(processPDF) + "&shiptoken2=" + escape(shiptoken2);
	window.open(url,"OrdersNotProcessed","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
</script>
<style>
.mouseover	{ background-color:#9BCEFF;}
</style>
<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
    <%
	if errMsg <> "" then
    %>
    <tr>
    	<td width="100%" align="center">
        	<div style="width:99%; border:1px solid red; padding:3px;"><h3><%=errMsg%></h3></div>
        </td>
    </tr>
    <%
	end if
	%>
	<tr>
		<td align="center">
            <table width="1000" border="1" cellspacing="0" cellpadding="5" align="center" style="border-collapse:collapse;">
                <tr bgcolor="#F5F5F5">
                    <td align="center"><b>Process Orders</b></td>
                    <td align="center"><b>Process Single Orders</b><br />ex) 12345 &nbsp; or &nbsp; 12345,23456,34567...</td>
                    <td align="center"><b>Process Reship Single Orders</b><br />ex) 12345 &nbsp; or &nbsp; 12345,23456,34567...</td>
                </tr>
                <tr>
                    <td align="center" width="400">
                        <form name="frmProcessOrder" method="post" action="/admin/processorders_run.asp" style="margin:0px; display:inline;">
                            <input type="hidden" name="hidRunType" value="M" />
                            <input type="submit" name="btnRun" value="Process Additional Orders" onclick="if (confirm('Wish to continue?')) return true; else return false;" />
                        </form>
                        <form name="frmProcessReship" method="post" action="/admin/processorders_run.asp" style="margin:0px; display:inline;">
                            <input type="hidden" name="hidRunType" value="R" />
                            <input type="submit" name="btnRun" value="Process Reship Orders" onclick="if (confirm('Wish to continue?')) return true; else return false;" />
                        </form>
                    </td>
                    <td align="center">
                        <form name="frmProcessSingle" method="post" action="/admin/processorders_run.asp" style="margin:0px;">
                            <input type="hidden" name="hidRunType" value="S" />
                            <input type="text" name="txtOrderID" value="" style="color:#999; font-style:italic;" />
                            <input type="submit" name="btnRun" value="Submit" onclick="if (confirm('Wish to continue?')) return true; else return false;" />
                        </form>                    
                    </td>
                    <td align="center">
                        <form name="frmProcessReshipSingle" method="post" action="/admin/processorders_run.asp" style="margin:0px;">
                            <input type="hidden" name="hidRunType" value="X" />
                            <input type="text" name="txtOrderID" value="" style="color:#999; font-style:italic;" />
                            <input type="submit" name="btnRun" value="Submit" onclick="if (confirm('Wish to continue?')) return true; else return false;" />
                        </form>                    
                    </td>
                </tr>
            </table>
        </td>
    </tr>    
	<tr>
		<td align="center">
        	<form name="frmSearch" method="post" style="margin:0px;">
				<table width="1000" border="1" cellspacing="0" cellpadding="5" align="center" style="border-collapse:collapse;">
                	<tr bgcolor="#F5F5F5">
						<td align="center"><b>Search Date</b></td>
						<td align="center"><b>Site</b></td>
						<td align="center"><b>Ship type</b></td>
						<td align="center"><b>Download Zip</b></td>
						<td align="center"><b>Action</b></td>
					</tr>
                	<tr>
						<td align="center">
	                        <input type="text" id="id_sdate" name="txtSearchDate" value="<%=dateSearch%>" size="7">
                        </td>
                        <td align="center">
							<select name="cbSite">
                            	<option value="" <%if siteid="" then%>selected<%end if%>>ALL</option>
                            	<option value="0" <%if siteid="0" then%>selected<%end if%>>WE</option>
                            	<option value="1" <%if siteid="1" then%>selected<%end if%>>CA</option>
                            	<option value="2" <%if siteid="2" then%>selected<%end if%>>CO</option>
                            	<option value="3" <%if siteid="3" then%>selected<%end if%>>PS</option>
                            	<option value="10" <%if siteid="10" then%>selected<%end if%>>TM</option>
                            </select>
                        </td>
                        <td align="center">
                        	<select name="cbShiptype" style="width:200px;">
                            	<option value="" <%if ""=shiptypeid then%>selected<%end if%>>ALL</option>
                        	<%
							sql	=	"select	shiptypeid, shipdesc from xshiptype order by 1"
							set rsShiptype = oConn.execute(sql)
							do until rsShiptype.eof
							%>
                            	<option value="<%=rsShiptype("shiptypeid")%>" <%if cstr(shiptypeid)=cstr(rsShiptype("shiptypeid")) then%>selected<%end if%>><%=rsShiptype("shipdesc")%></option>
                            <%
								rsShiptype.movenext
							loop
							%>
                            </select>
                        	<select name="cbShiptype2">
                            	<option value="" <%if ""=shiptypeid2 then%>selected<%end if%>>ALL</option>
                            	<option value="R" <%if "R"=shiptypeid2 then%>selected<%end if%>>Reship</option>
                            	<option value="B" <%if "B"=shiptypeid2 then%>selected<%end if%>>Back-Order</option>
                            	<option value="A" <%if "A"=shiptypeid2 then%>selected<%end if%>>Additional</option>
                            	<option value="S" <%if "S"=shiptypeid2 then%>selected<%end if%>>Single</option>
                            	<option value="X" <%if "X"=shiptypeid2 then%>selected<%end if%>>Reship Single</option>
                            </select>
                        </td>
						<td align="center">
                        	<div style="display:block;" id="id_zipresult"><a href="javascript:createZipFile();">Get ZipFile</a></div>
						</td>
                        <td align="center">
	                        <input type="submit" value="Find Orders">
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="errMsg" value="" />
            </form>
        </td>
    </tr>
	<tr>
		<td align="center"> 
		<%
        sql	=	"select	case when a.thub_posted_to_accounting in ('R', 'X') then 1 else 0 end isRehipOrder" & vbcrlf & _
				"	,	case when a.thub_posted_to_accounting = 'B' then 1 else 0 end isBackOrder" & vbcrlf & _
				"	,	case when isnull(q.numRegularOrder, 0) = isnull(q.numVendorOrder, 0) then 1 " & vbcrlf & _
				"			when a.thub_posted_to_accounting = 'D' then 1" & vbcrlf & _
				"			else 0 " & vbcrlf & _
				"		end isDropShipOrder" & vbcrlf & _
				"	--,	case when a.processPDF like '%WHALE%' then 'WHALE' else x.shortDesc end shortDesc" & vbcrlf & _
				"	--,	case when a.processPDF like '%WHALE%' then '' " & vbcrlf & _
				"	--		else isnull(case when o.isShipMethod = 1 then o.token end," & vbcrlf & _
				"	--					case when t.shiptypeid = 1 then " & vbcrlf & _
				"	--							case when q.quantity > 1 then t.token + '-MULTI' else t.token + '-SINGLE' end" & vbcrlf & _
				"	--						else isnull(t.token, a.shiptype) " & vbcrlf & _
				"	--					end) " & vbcrlf & _
				"	--	end shipToken2" & vbcrlf & _
				"	,	'ALL' shortDesc" & vbcrlf & _
				"	,	'ALL' shipToken2" & vbcrlf & _
				"	,	convert(varchar(10), a.thub_posted_date, 20) processed_datetime" & vbcrlf & _
				"	,	a.processPDF, a.processTXT" & vbcrlf & _
				"	,	sum(case when x.site_id = 0 then 1 else 0 end) weOrders" & vbcrlf & _
				"	,	sum(case when x.site_id = 2 then 1 else 0 end) coOrders" & vbcrlf & _
				"	,	count(*) numOrders" & vbcrlf & _
				"	,	isnull(sum(case when a.rmastatus is null and a.scandate is not null then 1 else 0 end), 0) numScan" & vbcrlf & _
				"	,	isnull(sum(case when a.cancelled is not null or cancelled = 1 then 1 else 0 end), 0) numCancel" & vbcrlf & _
				"	,	isnull(sum(case when a.rmastatus is not null then 1 else 0 end), 0) numBackorder" & vbcrlf & _
				"from 	we_orders a with (nolock) join v_accounts v" & vbcrlf & _
				"	on	a.store = v.site_id and a.accountid = v.accountid join xstore x with (nolock)" & vbcrlf & _
				"	on	x.site_id = a.store join " & vbcrlf & _
				"		(" & vbcrlf & _
				"		select	a.store, a.orderid, isnull(sum(b.quantity), 0) quantity, sum(b.quantity * isnull(c.itemWeight, 0.0)) itemWeight" & vbcrlf & _
				"			,	count(*) numRegularOrder" & vbcrlf & _
				"			,	isnull(sum(case when m.id is not null then 1 " & vbcrlf & _
				"								else" & vbcrlf & _
				"									case when c.vendor in ('CM', 'DS', 'MLD') then 1 " & vbcrlf & _
				"										when c.partnumber like '%HYP%' then 1" & vbcrlf & _
				"										else 0 end" & vbcrlf & _
				"							end), 0) numVendorOrder" & vbcrlf & _
				"		from 	we_orders a with (nolock) join we_orderdetails b with (nolock)" & vbcrlf & _
				"			on	a.orderid = b.orderid left outer join we_items c with (nolock)" & vbcrlf & _
				"			on	b.itemid = c.itemid left outer join we_items_musicskins m with (nolock)" & vbcrlf & _
				"			on	b.itemid = m.id" & vbcrlf & _
				"		where	a.thub_posted_date >= '" & dateSearch & "'" & vbcrlf & _
				"			and a.thub_posted_date < '" & dateSearchEnd & "'" & vbcrlf & _
				"			and	a.approved = 1" & vbcrlf & _
				"			and	b.itemid not in (1)" & vbcrlf & _
				"		group by a.store, a.orderid" & vbcrlf & _
				"		) q" & vbcrlf & _
				"	on	a.store = q.store and a.orderid = q.orderid left outer join XShipType t with (nolock)" & vbcrlf & _
				"	on	a.shiptype = t.shipdesc left outer join XOrderType o with (nolock)" & vbcrlf & _
				"	on	a.extOrderType = o.typeid " & vbcrlf & _
				"where	a.thub_posted_date >= '" & dateSearch & "'" & vbcrlf & _
				"	and a.thub_posted_date < '" & dateSearchEnd & "'" & vbcrlf & _
				"	and	x.active = 1" & vbcrlf
		if siteid <> "" then
			sql = sql & "	and	a.store = '" & siteid & "'" & vbcrlf
		end if
		if shiptypeid <> "" then
			sql = sql & "	and	t.shiptypeid = '" & shiptypeid & "'" & vbcrlf
		end if
		if shiptypeid2 <> "" then
			if shiptypeid2 = "A" then
				sql = sql & "	and	(a.processPDF like '%AM%' or a.processPDF like '%PM%') and a.thub_posted_to_accounting not in ('R','B','X')" & vbcrlf
			else
				sql = sql & "	and	a.thub_posted_to_accounting = '" & shiptypeid2 & "'" & vbcrlf
			end if
		end if
		sql = sql & "group by	case when a.thub_posted_to_accounting in ('R', 'X') then 1 else 0 end" & vbcrlf & _
					"		,	case when a.thub_posted_to_accounting = 'B' then 1 else 0 end" & vbcrlf & _				
					"		,	case when isnull(q.numRegularOrder, 0) = isnull(q.numVendorOrder, 0) then 1 " & vbcrlf & _
					"				when a.thub_posted_to_accounting = 'D' then 1" & vbcrlf & _
					"				else 0 " & vbcrlf & _
					"			end " & vbcrlf & _
					"		--,	case when a.processPDF like '%WHALE%' then 'WHALE' else x.shortDesc end" & vbcrlf & _
					"		--,	case when a.processPDF like '%WHALE%' then '' " & vbcrlf & _
					"		--		else isnull(case when o.isShipMethod = 1 then o.token end," & vbcrlf & _
					"		--					case when t.shiptypeid = 1 then " & vbcrlf & _
					"		--							case when q.quantity > 1 then t.token + '-MULTI' else t.token + '-SINGLE' end" & vbcrlf & _
					"		--						else isnull(t.token, a.shiptype) " & vbcrlf & _
					"		--					end) " & vbcrlf & _
					"		--	end" & vbcrlf & _
					"		,	convert(varchar(10), a.thub_posted_date, 20) " & vbcrlf & _
					"		,	a.processPDF, a.processTXT" & vbcrlf & _
					"order by isRehipOrder desc, isBackOrder desc, isDropShipOrder desc, shortDesc, shipToken2"

'		response.write "<pre>" & sql & "</pre>"
		sqlSiteID = "null"
		sqlShipType = "null"
		sqlShipType2 = "null"
		if siteid <> "" then sqlSiteID = siteid
		if shiptypeid <> "" then sqlShipType = shiptypeid
		if shiptypeid2 <> "" then sqlShipType2 = shiptypeid2
		
		sql	=	"exec getPackingSlips '" & dateSearch & "', '" & dateSearchEnd & "', " & sqlSiteID & ", " & sqlShipType & ", " & sqlShipType2
		'response.write sql
		
        set objRsAllOrders = oConn.execute(sql)

		numTotalDropShip = 0
        if not objRsAllOrders.eof then
		%>
            <table width="1065" border="1" align="center" cellpadding="5" cellspacing="0" style="border-collapse:collapse;font-family: Arial, Helvetica, sans-serif; font-size:12px;">
                <tr>
                    <td width="50" rowspan="2" align="center" style="position:relative; cursor:pointer;" onclick="doSort('shortDesc',this);" title="STORE"><b>STORE</b></div></td>
                    <td width="90" rowspan="2" style="position:relative; cursor:pointer;" onclick="doSort('shipToken2',this);" title="SHIP TYPE"><b>SHIP TYPE</b></td>
                    <td width="80" rowspan="2" style="position:relative; cursor:pointer;" onclick="doSort('processed_datetime',this);" title="Processed DateTime"><b>Processed DateTime</b></td>
                    <td width="420" rowspan="2" style="position:relative; cursor:pointer;" onclick="doSort('a.processPDF',this);" title="Links to download invoices"><b>Links to download invoices</b></td>
                    <td colspan="5" align="center"><b>Number of Orders</b></td>
                </tr>
                <tr>
                    <td width="65"><b>Processed</b></td>
                    <td width="65"><b>Scanned</b></td>
                    <td width="65"><b>Cancelled</b></td>
                    <td width="65"><b>BO</b></td>
                    <td width="65"><b>Status</b></td>
                </tr>
			</table>
            <div id="searchResult">
            <table width="1065" border="1" align="center" cellpadding="5" cellspacing="0" style="border-collapse:collapse;font-family: Arial, Helvetica, sans-serif; font-size:12px;">
			<%		
			do until objRsAllOrders.eof
				numOrders = objRsAllOrders("numOrders")
				numScan = objRsAllOrders("numScan")
				numCancel = objRsAllOrders("numCancel")
				numBackorder = objRsAllOrders("numBackorder")
				isDropShipper = objRsAllOrders("isDropShipOrder")
				
				processPDF = objRsAllOrders("processPDF")
				processTXT = objRsAllOrders("processTXT")
				
				if isnumeric(numOrders) then
					if numOrders = 0 then
						processRatio = 0.0
					else
						processRatio = (1.0 * (numScan+numCancel+numBackorder) / numOrders) * 100.0
					end if
				end if
				
				if isDropShipper = 1 then
					numTotalDropShip = numTotalDropShip + numOrders
				else
				%>
                <tr onMouseOver="this.className='mouseover'" onMouseOut="this.className=''">
                    <td width="50"><%=objRsAllOrders("shortdesc")%></td>
                    <td width="90" align="left">
                        <%
						if objRsAllOrders("isRehipOrder") = 1 then
							response.write "<span style=""color:red;"">Reship</span><br />"
						elseif objRsAllOrders("isBackOrder") = 1 then
							response.write "<span style=""color:green; font-weight:bold;"">Back-Order</span><br />"
						elseif instr(processPDF, "(Single") > 0 then
							response.write "<span style=""color:#C60; font-weight:bold;"">Single</span><br />"
						elseif instr(processPDF, "AM") > 0 or instr(processPDF, "PM") > 0 then
							response.write "<span style=""color:blue;"">Additional</span><br />"
						end if
						%>
						<%=objRsAllOrders("shiptoken2")%>                        
                    </td>
                    <td width="80" align="left"><%=objRsAllOrders("processed_datetime")%></td>
                    <td width="420" align="left">
					<%
                    if processPDF <> "" then
						if numOrders = numCancel then
                    %>
                        - <%=processPDF%>
					<%						
						else
                    %>
	                    <ul style="margin:0px;">
    	                    <li><a target="_blank" href="http://www.wirelessemporium.com/admin/tempPDF/salesorders/<%=processPDF%>"><%=processPDF%></a></li>
                        	<li><a target="_blank" href="http://www.wirelessemporium.com/admin/packing.asp?p=<%=replace(replace(processPDF, "(", "[["), ")", "]]")%>"><%=replace(processPDF, ".pdf", ".html")%></a></li>
                            <%if objRsAllOrders("weOrders") > 0 then%>
	                       	<li><a target="_blank" href="http://www.wirelessemporium.com/admin/packing.asp?sid=0&p=<%=replace(replace(processPDF, "(", "[["), ")", "]]")%>"><%=replace(replace(processPDF, ".pdf", ".html"), "ALL_", "WE_")%></a></li>
                            <%end if%>
                            <%if objRsAllOrders("coOrders") > 0 then%>
    	                   	<li><a target="_blank" href="http://www.wirelessemporium.com/admin/packing.asp?sid=2&p=<%=replace(replace(processPDF, "(", "[["), ")", "]]")%>"><%=replace(replace(processPDF, ".pdf", ".html"), "ALL_", "CO_")%></a></li>
                            <%end if%>
                        </ul>
					<%						
						end if
                    end if
                    %>
                    </td>
                    <td width="65" align="left">
                    <%
					if processRatio >= 100 then
					%>
						<%=formatnumber(numOrders, 0)%>
                    <%
					else
					%>
						<a href="#" onclick="showPop('<%=replace(replace(processPDF, "(", "##"), ")", "@@")%>','<%=objRsAllOrders("shipToken2")%>');return false;"><%=formatnumber(numOrders, 0)%></a>
                    <%
					end if
					%>
                    </td>
                    <td width="65" align="left"><%=formatnumber(numScan, 0)%></td>
                    <td width="65" align="left"><%=formatnumber(numCancel, 0)%></td>
                    <td width="65" align="left"><%=formatnumber(numBackorder, 0)%></td>
                    <td width="65" align="left">
                    	<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; font-size:12px;">
                        	<tr>
                            	<td align="left"><%=formatnumber(processRatio, 2)%>%</td>
                                <td align="right">
								<%
                                if processRatio >= 100 then
                                %>
                                    <img src="/images/check.jpg" border="0" width="15"/>
                                <%
                                else
                                %>
                                    <img src="/images/x.jpg" border="0" width="15"/>
                                <%
                                end if
                                %>                                
                                </td>
                            </tr>
                        </table>
					</td>
                </tr>                
				<%
				end if
                objRsAllOrders.movenext
            loop
            %>
                <tr onMouseOver="this.className='mouseover'" onMouseOut="this.className=''">
                    <td colspan="4" align="center">Dropship Orders</td>
                    <td colspan="4" align="left"><%=formatnumber(numTotalDropShip, 0)%></td>
                    <td align="center">
                    	<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; font-size:12px;">
                        	<tr>
                            	<td align="left"><%=formatnumber(100, 2)%>%</td>
                                <td align="right"><img src="/images/check.jpg" border="0" width="15" /></td>
                            </tr>
                        </table>
					</td>
                </tr>
            </table>
			<%
            sql = 	"select	processPDF, count(*) orderCount" & vbcrlf & _
                    "from	we_orders" & vbcrlf & _
                    "where	thub_posted_date >= '" & dateSearch & "'" & vbcrlf & _
                    "	and thub_posted_date < '" & dateSearchEnd & "'" & vbcrlf & _						
                    "	and	approved = 1" & vbcrlf & _
                    "	and (cancelled is null or cancelled = 0)" & vbcrlf & _
                    "	and	processPDF like '%FCM%'" & vbcrlf & _
                    "group by processPDF" & vbcrlf & _
                    "having count(*) >= 50"
            set sumRs = oConn.execute(sql)
            if not sumRs.eof then
            %>
            <div style="width:1000px; padding:20px 0px 30px 0px;" align="left">
            	<div style="text-align:left; font-size:20px; color:#222;">Summary by partnumber &nbsp; &nbsp; <span style="font-size:12px;">(*currently first class and more than 50 orders)</span></div>
            	<div style="text-align:left;">
                <%
				do until sumRs.eof
					%>
	            	<div style="text-align:left;"><a href="/admin/processorders_excel.asp?processPDF=<%=sumRs("processPDF")%>"><img src="/images/excel_icon.jpg" border="0" width="16" /> <%=sumRs("processPDF")%></a> (<%=formatnumber(sumRs("orderCount"), 0) & " orders"%>)</div>
                    <%
					sumRs.movenext
				loop
				%>
                </div>
            </div>
			<%
            end if
            else
            %>
			No orders processed found
			<%
            end if		
            %>
			</div>
        </td>
	</tr>
</table>
<script>
	var curSortColumnName = "";
	var curSort = "ASC";
	var curSortHeader = null;
	function doSort(sortColumn,oHeader) {
		if (sortColumn == curSortColumnName) {
			if (curSort == "ASC") curSort = "DESC";
			else curSort = "ASC";
		} else {
			curSort = "DESC";
		}
		curSortColumnName = sortColumn;
		
//		alert('curSortColumnName:' + curSortColumnName + '\r\ncurSort:' + curSort);
		if (curSortHeader != null) curSortHeader.innerHTML = '<b>' + curSortHeader.title + '</b>';
		if (curSort == "DESC") oHeader.innerHTML = '<b>' + oHeader.title + '</b><div id="sortDesc"></div>';
		else oHeader.innerHTML = '<b>' + oHeader.title + '</b><div id="sortAsc"></div>';		
		curSortHeader = oHeader;
		
		document.getElementById('searchResult').innerHTML = '<div align="center" style="padding-top:50px; width:100%; font-size:18px; font-weight:bold;">Sorting Data..</div>'
		ajax('/ajax/admin/ajaxProcessOrder.asp?<%=formData%>&curSortColumnName='+curSortColumnName+'&curSort='+curSort,'searchResult');
	}
	
	function createZipFile() {
		document.getElementById('id_zipresult').innerHTML = '<div align=""center"" style=""display:block; font-weight:bold;""><img src="/images/preloading.gif" border="0" /></div>';
		ajax('/ajax/admin/ajaxCreateZip.asp?dateSearch=<%=dateSearch%>','id_zipresult');
	}
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->