<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - "
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	vendorCode = prepStr(request.QueryString("vendorCode"))
	processOrder = prepInt(request.QueryString("processOrder"))
	createCSV = prepInt(request.QueryString("createCSV"))
	cancelOrder = prepInt(request.QueryString("cancelOrder"))
	processed = prepInt(request.QueryString("processed"))
	purchaseApp = prepInt(request.QueryString("purchaseApp"))
	orderNumMod = prepStr(request.QueryString("orderNumMod"))
	
	userMsg = ""
	
	if vendorCode <> "" then
		session("vendorCode") = vendorCode
	else
		session("vendorCode") = null
	end if
	
	if cancelOrder = 1 then
		sql = "delete from we_vendorOrder where vendorCode = '" & vendorCode & "' and process = 0 and complete = 0 and adminID = " & adminID
		session("errorSQL") = sql
		oConn.execute(sql)
		userMsg = "Order has been cancelled"
		session("vendorCode") = null
		vendorCode = ""
	end if
	
	if processOrder = 1 then
		orderID = orderNumMod & "-" & replace(date,"/","") & "-" & replace(left(time,instr(time," ") - 1),":","")
		sql = "update we_vendorOrder set process = 1, processOrderDate = '" & now & "', orderID = '" & orderID & "' where vendorCode = '" & vendorCode & "' and process = 0 and complete = 0 and adminID = " & adminID
		session("errorSQL") = sql
		oConn.execute(sql)
		userMsg = "Order has been processed<br>Warehouse can now access the order for check-in"
		session("vendorCode") = null
		vendorCode = ""
	end if
	
	if createCSV = 1 then
		sql = "Select a.*, (select top 1 replace(itemDesc,',','-') from we_items where partNumber = a.partNumber) as itemDesc, b.inv_qty from we_vendorOrder a left join we_Items b on a.partNumber = b.partNumber where a.vendorCode = '" & vendorCode & "' and a.process = 0 and a.complete = 0 and a.adminID = " & adminID & " and b.master = 1"
		
		if purchaseApp > 0 then sql = session("csvSQL")
		
		response.Write("<!-- " & sql & " -->")
		
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
		
		if not rs.EOF then
			set fs = CreateObject("Scripting.FileSystemObject")
			if purchaseApp > 0 then
				filename = "purchApp_" & replace(replace(replace(now,"/","")," ",""),":","") & ".csv"
				path = Server.MapPath("/tempCSV/" & filename)
				session("errorSQL") = path
				set file = fs.CreateTextFile(path)
			
				file.WriteLine "ItemDesc,Sold,Inv"
				do while not rs.EOF
					file.write rs("itemDesc") & ","	'partnumber
					file.write rs("sales") & ","	'description
					file.write rs("inv_qty")		'vendor partnumber
					file.write vbcrlf
					
					rs.movenext
				loop
			else
				filename = "vendorOrder_" & vendorCode & "_" & replace(date,"/","") & ".csv"
				path = Server.MapPath("vendorOrders") & "\" & filename
				path = replace(path,"admin\","")
				session("errorSQL") = path
				set file = fs.CreateTextFile(path)
			
				file.WriteLine "WE_Partnumber,WE_Description,Vendor_Partnumber,Qty_Ordered,Fulfillment_Qty,InStock_Qty"
				do while not rs.EOF
					file.write rs("partnumber") & ","	'partnumber
					file.write rs("itemDesc") & ","	'description
					file.write rs("vendorPN") & ","	'vendor partnumber
					file.write rs("orderAmt") & ","	'qty ordered
					file.write ","	'fulfillment qty
					file.write rs("inv_qty")	'in-stock qty
					file.write vbcrlf
					
					rs.movenext
				loop
			end if
			file.close()
			fs = null
		%>
        <table align="center" border="0" cellpadding="0" cellspacing="0">
        	<tr><td align="left" style="font-weight:bold; font-size:24px; padding-top:20px;">Order Creation Complete</td></tr>
            <tr>
            	<% if purchaseApp > 0 then %>
                <td align="left" style="padding-top:20px;">Download Products <a href="/tempCSV/<%=filename%>">file</a>!</td>
				<% else %>
            	<td align="left" style="padding-top:20px;">Download Order <a href="../vendorOrders/<%=filename%>">file</a>!</td>
                <% end if %>
            </td>
        </td>
        <%
		else
			response.Write("No items on order<br>Action has been cancelled")
		end if
		response.End()
	end if
	
	sql = "Select distinct a.vendorCode, b.name from we_vendorOrder a left join we_vendors b on a.vendorCode = b.code where a.process = 0 and a.complete = 0 and a.adminID = " & adminID
	session("errorSQL") = sql
	Set vendorRS = Server.CreateObject("ADODB.Recordset")
	vendorRS.open sql, oConn, 0, 1
	
	sql = "Select (select name from we_vendors where code = '" & vendorCode & "') as vendor, a.*, (select top 1 itemDesc from we_items where partNumber = a.partNumber) as itemDesc, b.inv_qty from we_vendorOrder a left join we_Items b on a.partNumber = b.PartNumber and b.master = 1 where a.vendorCode = '" & vendorCode & "' and a.process = " & processed & " and a.complete = 0 and a.adminID = " & adminID
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
%>
<table align="center" border="0" cellpadding="3" cellspacing="0">
	<% if userMsg <> "" then %>
    <tr><td align="center" style="font-weight:bold; color:#F00; border-bottom:1px solid #F00;"><%=userMsg%></td></tr>
    <% end if %>
    <% if processed = 0 then %>
    <tr>
    	<td align="center" style="padding:20px 0px 20px 0px;">
            <table border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td style="font-weight:bold; font-size:14px;" valign="top">Select Order to Process:</td>
                    <td valign="top">
                    	<% if vendorRS.EOF then %>
                        No orders currently waiting to be processed<br />
                        <input type="button" name="myAction" value="Enter New Order" onclick="window.location='/admin/vendorPartNumbers_enter.asp'" />
                        <% else %>
                    	<select name="vendorCode" onchange="window.location='/admin/vendorOrders.asp?vendorCode=' + this.value">
                        	<option value="">Select Vendor</option>
                            <%
							do while not vendorRS.EOF
							%>
                            <option value="<%=vendorRS("vendorCode")%>"><%=vendorRS("name")%></option>
                            <%
								vendorRS.movenext
							loop
							%>
                        </select>
                        <% end if %>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <% end if %>
    <% if not rs.EOF then %>
    <tr>
    	<td>
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0">
            	<tr>
                	<td colspan="5" align="center">
                    	<div style="float:left; font-size:34px; font-weight:bold;">Order For <%=rs("vendor")%></div>
                        <div style="float:right">
                        	<div style="display:table;">
                            	<div style="float:left;"><input type="checkbox" name="newOrder" value="N" style="height:10px;" onclick="modOrderNum(this.checked,'N')" /></div>
                                <div style="float:left; font-size:12px; font-weight:normal; width:50px;">New</div>
                                <div style="float:left; margin-left:20px;"><input type="checkbox" name="tier2Order" value="T2" onclick="modOrderNum(this.checked,'T2')" /></div>
                                <div style="float:left; font-size:12px; font-weight:normal; width:50px;">Tier 2</div>
                            </div>
                            <div style="display:table;">
                            	<div style="float:left;"><input type="checkbox" name="tier1Order" value="T1" onclick="modOrderNum(this.checked,'T1')" /></div>
                                <div style="float:left; font-size:12px; font-weight:normal; width:50px;">Tier 1</div>
                                <div style="float:left; margin-left:20px;"><input type="checkbox" name="tier3Order" value="T3" onclick="modOrderNum(this.checked,'T3')" /></div>
                                <div style="float:left; font-size:12px; font-weight:normal; width:50px;">Tier 3</div>
                            </div>
                        </div>
                    </td>
                </tr>
                <% if processed = 0 then %>
                <tr bgcolor="#000000">
                	<td colspan="5">
                    	<table border="0" cellpadding="3" cellspacing="0" width="100%">
                        	<tr>
                            	<td align="center" width="25%"><input type="button" name="myAction" value="Cancel Order" onclick="window.location='/admin/vendorOrders.asp?vendorCode=<%=vendorCode%>&cancelOrder=1'" /></td>
                                <td align="center" width="25%"><input type="button" name="myAction" value="Add More Items" onclick="window.close()" /></td>
                                <td align="center" width="25%"><input type="button" name="myAction" value="Create CSV" onclick="window.location='/admin/vendorOrders.asp?vendorCode=<%=vendorCode%>&createCSV=1'" /></td>
                                <td align="center" width="25%"><input type="button" name="myAction" id="processOrderTop" value="Process Order" onclick="alert('You must select a tier before processing')" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <% end if %>
                <tr style="font-weight:bold; color:#FFF; background-color:#333;">
                	<td nowrap="nowrap">WE Partnumber</td>
                    <td nowrap="nowrap">We Description</td>
                	<td nowrap="nowrap">Vendor Partnumber</td>
                    <td nowrap="nowrap">On-Hand</td>
                    <td nowrap="nowrap">Qty Ordered</td>
                </tr>
                <%
				bgColor = "#ffffff"
				do while not rs.EOF
				%>
                <tr bgcolor="<%=bgColor%>">
                	<td align="left"><%=rs("partNumber")%></td>
                    <td align="left"><%=rs("itemDesc")%></td>
                	<td align="left"><%=rs("vendorPN")%></td>
                    <td align="right"><%=rs("inv_qty")%></td>
                    <% if processed = 0 then %>
                    <td align="center"><input type="text" name="orderAmt" value="<%=rs("orderAmt")%>" size="3" onchange="ajax('/ajax/admin/saveVendorNum.asp?ourPN=<%=rs("partNumber")%>&vendorPN=<%=rs("vendorPN")%>&orderAmt=' + this.value,'dumpZone')" /></td>
                    <% else %>
                    <td align="center"><%=rs("orderAmt")%></td>
                    <% end if %>
                </tr>
                <%
					rs.movenext
					if bgColor = "#ffffff" then bgColor = "#f0f0f0" else bgColor = "#ffffff"
				loop
				%>
                <% if processed = 0 then %>
                <tr bgcolor="#000000">
                	<td colspan="5">
                    	<table border="0" cellpadding="3" cellspacing="0" width="100%">
                        	<tr>
                            	<td align="center" width="25%"><input type="button" name="myAction" value="Cancel Order" onclick="window.location='/admin/vendorOrders.asp?vendorCode=<%=vendorCode%>&cancelOrder=1'" /></td>
                                <td align="center" width="25%"><input type="button" name="myAction" value="Add More Items" onclick="window.close()" /></td>
                                <td align="center" width="25%"><input type="button" name="myAction" value="Create CSV" onclick="window.location='/admin/vendorOrders.asp?vendorCode=<%=vendorCode%>&createCSV=1'" /></td>
                                <td align="center" width="25%"><input type="button" name="myAction" id="processOrderBottom" value="Process Order" onclick="alert('You must select a tier before processing')" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <% end if %>
            </table>
        </td>
    </tr>
    <% end if %>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script>
	var orderNumMod = "";
	function modOrderNum(checked,value) {
		if (checked == true) {
			if (orderNumMod == "") {
				orderNumMod = value;
			}
			else {
				orderNumMod = orderNumMod + "-" + value;
			}
		}
		else {
			if (orderNumMod.indexOf("-") > 0) {
				orderNumMod = orderNumMod.replace(value + "-","");
				orderNumMod = orderNumMod.replace("-" + value,"");
			}
			else {
				orderNumMod = "";
			}
		}
		if (orderNumMod == "") {
			document.getElementById("processOrderTop").setAttribute("onclick","alert('You must select a tier before processing')");
			document.getElementById("processOrderBottom").setAttribute("onclick","alert('You must select a tier before processing')");
		}
		else {
			document.getElementById("processOrderTop").setAttribute("onclick","window.location='/admin/vendorOrders.asp?vendorCode=<%=vendorCode%>&orderNumMod=" + orderNumMod + "&processOrder=1'");
			document.getElementById("processOrderBottom").setAttribute("onclick","window.location='/admin/vendorOrders.asp?vendorCode=<%=vendorCode%>&orderNumMod=" + orderNumMod + "&processOrder=1'");
		}
	}
</script>