<%
	thisUser = Request.Cookies("username")
	pageTitle = "Top Phones"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	set fso = CreateObject("Scripting.FileSystemObject")
	set tfile = fso.CreateTextFile(server.MapPath("/admin/tempCSV/topPhones.csv"))
	tfile.WriteLine("brand,model,itemID,partNumber,sold")
%>
<form name="sortForm" style="padding:0px; margin:0px;">
<div style="width:400px; height:20px; margin-bottom:10px; margin-left:auto; margin-right:auto;">
	<div style="float:left;">Number of Phones:</div>
    <div style="float:left; padding-left:5px;"><input type="text" name="numOfPhones" value="50" size="3" onchange="updateList()" /></div>
</div>
<div style="width:400px; height:20px; margin-bottom:10px; margin-left:auto; margin-right:auto;">
	<div style="float:left;">List Phones By:</div>
    <div style="float:left; padding-left:5px;">
    	<select name="listBy" onchange="listByChng(this.value)">
        	<option value="allTime">All Time Sales</option>
            <option value="dateRange">Sales Date Range</option>
            <option value="new2Old">Newest to Oldest</option>
        </select>
    </div>
</div>
<div id="dateRangeBox" style="width:400px; height:20px; margin-bottom:10px; margin-left:auto; margin-right:auto; display:none;">
	<div style="float:left; padding-top:3px;">Start Date:</div>
    <div style="float:left; padding-left:5px;"><input type="text" name="sDate" value="<%=date-14%>" size="10" /></div>
    <div style="float:left; padding:3px 0px 0px 5px;">End Date:</div>
    <div style="float:left; padding-left:5px;"><input type="text" name="eDate" value="<%=date%>" size="10" /></div>
    <div style="float:left; padding-left:5px;"><input type="button" name="dateBttn" value="Pull Phones" onclick="getPhoneByRange()" /></div>
</div>
</form>
<div style="width:400px; height:20px; margin-bottom:10px; text-align:center; margin-left:auto; margin-right:auto;"><a href="/admin/tempCSV/topPhones.csv">Download CSV</a></div>
<div style="width:400px; height:20px; margin-bottom:10px; background-color:#000; font-weight:bold; color:#FFF; margin-left:auto; margin-right:auto;">
	<div style="float:left; width:20px; text-align:center;">#</div>
    <div style="float:left; width:150px; text-align:center; margin-left:10px;">Image</div>
    <div style="float:left; width:200px; text-align:left; margin-left:10px;">Name</div>
</div>
<div id="phoneList">
<%
	sql = 	"select top 50 a.itemID, c.brandName, d.modelName, a.itemDesc, a.partnumber, a.itemPic, a.cogs, b.sales from we_Items a " &_
				"left join (" &_
					"select sum(quantity) as sales, partNumber " &_
					"from we_orders ia " &_
						"left join we_orderDetails ib on ia.orderID = ib.orderID " &_
					"where ia.approved = 1 and ia.cancelled is null " &_
					"group by partNumber" &_
				") b on a.partNumber = b.partNumber " &_
				"join we_brands c on a.brandID = c.brandID " &_
				"join we_models d on a.modelID = d.modelID " &_
			" where typeID = 16 order by b.sales desc"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	phoneCnt = 0
	do while not rs.EOF
		phoneCnt = phoneCnt + 1
		itemID = prepInt(rs("itemID"))
		brandName = prepStr(rs("brandName"))
		modelName = prepStr(rs("modelName"))
		itemDesc = prepStr(rs("itemDesc"))
		partnumber = prepStr(rs("partnumber"))
		itemPic = "/productpics/thumb/" & prepStr(rs("itemPic"))
		sold = prepInt(rs("sales"))
		cogs = prepInt(rs("cogs"))
		if not fso.fileExists(server.MapPath("/productpics/thumb/" & prepStr(rs("itemPic")))) then itemPic = "/productpics/thumb/imagena.jpg"
		tfile.WriteLine(brandName & "," & modelName & "," & itemID & "," & partnumber & "," & sold)
%>
<div style="width:400px; height:110px; border-bottom:1px solid #333; margin-bottom:10px; margin-left:auto; margin-right:auto;">
	<div style="float:left; width:20px; text-align:center;"><%=phoneCnt%></div>
    <div style="float:left; width:150px; text-align:center; margin-left:10px;"><img src="<%=itemPic%>" border="0" /></div>
    <div style="float:left; width:200px; text-align:left; margin-left:10px;">
		<%=itemDesc%>
        <br /><br />
        Sold: <%=sold%><br />
        Cogs: <%=formatCurrency(cogs,2)%>
    </div>
</div>
<%
		rs.movenext
	loop
	
	tfile.close
	set tfile = nothing
%>
</div>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function listByChng(listBy) {
		if (listBy == "dateRange") {
			document.getElementById("dateRangeBox").style.display = ""
		}
		else {
			document.getElementById("dateRangeBox").style.display = "none"
			var numOfPhones = document.sortForm.numOfPhones.value
			ajax('/ajax/admin/ajaxTopPhones.asp?listBy=' + listBy + '&numOfPhones=' + numOfPhones,'phoneList')
		}
	}
	
	function getPhoneByRange() {
		var sDate = document.sortForm.sDate.value
		var eDate = document.sortForm.eDate.value
		var numOfPhones = document.sortForm.numOfPhones.value
		ajax('/ajax/admin/ajaxTopPhones.asp?listBy=dateRange&sDate=' + sDate + '&eDate=' + eDate + '&numOfPhones=' + numOfPhones,'phoneList')
	}
	
	function updateList() {
		var listBy = document.sortForm.listBy.value
		var sDate = document.sortForm.sDate.value
		var eDate = document.sortForm.eDate.value
		var numOfPhones = document.sortForm.numOfPhones.value
		
		if (listBy == "dateRange") {
			ajax('/ajax/admin/ajaxTopPhones.asp?listBy=dateRange&sDate=' + sDate + '&eDate=' + eDate + '&numOfPhones=' + numOfPhones,'phoneList')
		}
		else {
			ajax('/ajax/admin/ajaxTopPhones.asp?listBy=' + listBy + '&numOfPhones=' + numOfPhones,'phoneList')
		}
	}
</script>