<%
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

pageTitle = "Datafeeds"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<style>
.mouseover	{ background-color:#9BCEFF;}
</style>
<%
timeStamp = Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now)
dim storeID : storeID = request("cbSite")
if "" = storeID then storeID = 0 end if

sql	=	"select	a.site_id, a.shortDesc, a.longDesc, a.site_url, a.logo" & vbcrlf & _
		"	,	null, null, a.color, b.feedFileName, b.feedFileDesc" & vbcrlf & _
		"	,	b.nDownload, b.lastDownload, b.lastUser, b.multiFile, b.schedule" & vbcrlf & _
		"from	xstore a right outer join xdatafeed b" & vbcrlf & _
		"	on	a.site_id = b.site_id" & vbcrlf & _
		"where	b.site_id = '" & storeID & "'" & vbcrlf & _
		"	and	b.isActive = 1" & vbcrlf & _		
		"order by a.site_id, b.feedFileDesc" & vbcrlf
session("errorSQL") = sql		
dim arrList : 	arrList = 	getDbRows(sql)
dim arrSite	:	arrSite	=	getDbRows("select site_id, shortDesc, color from xstore order by 1")

dim fs, fsTemp, isFileExist, filePath, fullFileName, fileName, fileExt
'fullFileName : aaaa.txt
'fileName : aaaa
'fileExt : txt
'filePath : e:\webservice\wirelessemporium.com\aaaa.txt

set fs=Server.CreateObject("Scripting.FileSystemObject")
isFileExist = false
%>
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td width="100%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top">
						<form name="frmFeed">
						<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
							<tr>
								<td width="100" class="normalText" align="center"><b>SITE</b>&nbsp;
									<select name="cbSite" onChange="this.form.submit();">
										<%
										if not isnull(arrSite) then
											for nRow = 0 to ubound(arrSite, 2)
											%>
												<option style="color:<%=arrSite(2, nRow)%>;" value="<%=arrSite(0, nRow)%>" <%if cstr(arrSite(0, nRow)) = cstr(storeID) then %>selected<% end if%>><%=arrSite(1, nRow)%></option>
											<%
											next
										end if
										%>
										<option value="99" <%if 99 = storeID then %>selected<% end if%>>WE_TEMP</option>
									</select>
								</td>
								<td class="normalText" align="left" style="padding-left:10px;"><b>Product Name</b></td>
								<td class="normalText" align="left" style="padding-left:10px;"><b>Date Modified</b></td>
								<td class="normalText" align="left" style="padding-left:10px;"><b>Link</b></td>
								<td class="normalText" align="left" style="padding-left:10px;"><b>Run Schedule</b></td>
							</tr>
							<tr><td colspan="99" height="1" bgcolor="#E95516" style="font-size:1px;">&nbsp;</td></tr>							
							<%
							if not isnull(arrList) then
								for nRow = 0 to ubound(arrList, 2)
									if 99 = storeID then
										physicalBasePath = server.mappath("\tempCSV\temp_we")
									else
										physicalBasePath = server.mappath("\tempCSV")
									end if
									
									if 99 = storeID then
										siteDesc = "TabletMall"
									else
										siteDesc = arrList(2, nRow)
									end if
								
									if arrList(9, nRow) = "ShopRunner" then
										downloadFolderURL = arrList(3, nRow) & "/tempCSV/ShopRunner"
										folderPath = replace(physicalBasePath, "wirelessemporium.com", siteDesc & ".com") & "\shoprunner\"
										set folder = fs.GetFolder(folderPath)
										set files = folder.Files

										lap = 0										
										for each oFile in files
											lap = lap + 1
											isFileExist = true
											set fsTemp = oFile
											fullFileName = fsTemp.Name
											
											if lap = 1 then exit for
										next
									else
										downloadFolderURL = arrList(3, nRow) & "/tempCSV"
										fullFileName = arrList(8, nRow)
										fileName = left(fullFileName, len(fullFileName)-4)
										fileExt = right(fullFileName, 3)
										filePath = replace(physicalBasePath, "wirelessemporium.com", siteDesc & ".com") & "\" & fullFileName
										isFileExist = fs.FileExists(filePath)
										if isFileExist then
											set fsTemp = fs.GetFile(filePath)
										end if
										
										if not isFileExist then	'ex) mediaforge20110418.txt, there are some feed that change its filename daily basis following created date.
											fullFileName = fileName & year(now) & Right("0" & month(now), 2) & Right("0" & day(now), 2) & "." & fileExt
											filePath = replace(physicalBasePath, "wirelessemporium.com", siteDesc & ".com") & "\" & fullFileName
											isFileExist = fs.FileExists(filePath)
											if isFileExist then
												set fsTemp = fs.GetFile(filePath)
											end if
										end if
									end if
							%>
							<tr class="grid_row_<%=(nRow mod 2)%>" onMouseOver="this.className='mouseover'" onMouseOut="this.className='grid_row_<%=(nRow mod 2)%>'" >
								<td class="smlText" align="center" style="color:<%=arrList(7, nRow)%>"><%=arrList(1, nRow)%></td>
								<td class="smlText" align="left" style="padding-left:10px;"><%=arrList(9, nRow)%></td>
								<td class="smlText" align="left" style="padding-left:10px;">
								<%
									if arrList(13, nRow) > 0 then
										if fs.FolderExists(replace(physicalBasePath, "wirelessemporium.com", siteDesc & ".com")) then
											set oFolder = fs.GetFolder(replace(physicalBasePath, "wirelessemporium.com", siteDesc & ".com"))
											set arrFiles = oFolder.Files
											
											for each iFile in arrFiles
												if instr(iFile.Name, fileName) > 0 and instr(iFile.Name, ".zip") = 0 then
													response.write iFile.DateLastModified
													exit for
												end if
											next
										end if
									else
										if isFileExist then
											response.write fsTemp.DateLastModified
										else
											response.write "N/A"
										end if
									end if
								%>
								</td>
								<td class="smlText" align="left" style="padding-left:10px;" >
								<%
									if arrList(13, nRow) > 0 then
										if fs.FolderExists(replace(physicalBasePath, "wirelessemporium.com", siteDesc & ".com")) then
											set oFolder = fs.GetFolder(replace(physicalBasePath, "wirelessemporium.com", siteDesc & ".com"))
											set arrFiles = oFolder.Files
											
											for each iFile in arrFiles
												if instr(iFile.Name, fileName) > 0 and instr(iFile.Name, ".zip") = 0 then
									%>
										<a href="<%=downloadFolderURL%>/<%=iFile.Name%>?v=<%=timeStamp%>" target="_blank"><%=iFile.Name%></a> - <%=formatnumber(iFile.size / 1048576.0, 2) & " MB<br>"%>
									<%				
												end if
											next
										end if
									else
										if isFileExist then 									
								%>
									<a href="<%=downloadFolderURL%>/<%=fullFileName%>?v=<%=timeStamp%>" target="_blank"><%=fullFileName%></a> - <%=formatnumber(fsTemp.size / 1048576.0, 2) & " MB"%>
								<%
										else
											response.write "N/A"
										end if
									end if
								%>
								</td>
								<td class="smlText" align="left" style="padding-left:10px;" >
								<%
									response.write arrList(14, nRow) & "<br>"
									if prepInt(arrList(13, nRow)) > 0 then
									%>
                                    <div style="display:block;" id="id_<%=nRow%>"><a href="javascript:createZipFile('<%=replace(arrList(8, nRow), ".txt", "")%>','id_<%=nRow%>');">Get ZipFile</a></div>
                                    <%
									end if
								%>
                                </td>								
							</tr>
							</div>
							<%		
									set fsTemp = nothing
								next 
							else
							%>
							<tr><td colspan="99" class="smlText" align="center">No data to display</td></tr>
							<%
							end if
							%>
						</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script>
	function createZipFile(feedName,retZip) {
		document.getElementById(retZip).innerHTML = '<div align=""center"" style=""display:block; font-weight:bold;""><img src="/images/preloading.gif" border="0" /></div>';
		ajax('/ajax/admin/ajaxCreateZip.asp?datafeed='+feedName,retZip);
	}
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
