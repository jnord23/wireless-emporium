<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Upload dropshipper's shipment tracking numbers"
	header = 1
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/admin/formatNotes.asp"-->
<style>
.grid_row_0	{ background-color:#FFFFFF;}
.grid_row_1	{ background-color:#EAEAEA;}
.mouseover	{ background-color:#9BCEFF;}
</style>
<%
adminUserID = session("adminID")
if isnull(adminUserID) or adminUserID = "" then
	adminUserID = 0
end if

savePath = server.mappath("/admin/tempcsv")
Set objUpload = Server.CreateObject("Persits.Upload")
objUpload.IgnoreNoPost = True
saveCount = objUpload.Save(savePath)

if objUpload.form("upload") <> "" and saveCount > 0 then
	set File = objUpload.Files(1)
	myFile = File.path
	
	set txtConn = Server.CreateObject("ADODB.Connection")
	myStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & myFile & ";Extended Properties=""Excel 12.0;HDR=YES"";"
	txtConn.ConnectionString = myStr
	txtConn.Open
	
	sql = "select [order_number], [dropshipper], [tracking_type], [tracking_number], [shipment_date] from [Sheet1$]"	
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, txtConn, 0, 1
	
	hasError = false
	if rs.eof then
		response.write "<h3>No data is available</h3>"
	else
		nRow = 1
		do until rs.eof
			nRow = nRow + 1
			if isnull(rs("order_number")) or rs("order_number") = "" then
				hasError = true
				response.write "<div align=""center""><h3 style=""color:red;"">order number is incorrect (Rows in the file: " & nRow & ")</h3></div>"
			end if
			if isnull(rs("tracking_number")) or rs("tracking_number") = "" then
				hasError = true			
				response.write "<div align=""center""><h3 style=""color:red;"">tracking number is incorrect (Rows in the file: " & nRow & ")</h3></div>"
			end if		
			if isnull(rs("tracking_type")) or rs("tracking_type") = "" then
				hasError = true			
				response.write "<div align=""center""><h3 style=""color:red;"">tracking type is incorrect (Rows in the file: " & nRow & ")</h3></div>"
			end if
			if isnull(rs("dropshipper")) or rs("dropshipper") = "" then
				hasError = true
				response.write "<div align=""center""><h3 style=""color:red;"">dropshipper is blank (Rows in the file: " & nRow & ")</h3></div>"
			end if
			rs.movenext
		loop

		orderProcessed = ""
		rowProcessed = 0		
		if not hasError then
			rs.movefirst
			do until rs.eof
				sql = "select 1 from we_dropship_tracking_history where orderid = '" & rs("order_number") & "'"
				set rsTemp = oConn.execute(sql)
				if rsTemp.eof then
					trackingNum = replace(replace(replace(replace(replace(replace(trim(rs("tracking_number")), chr(160), ""), chr(32), ""), chr(10), ""), chr(13), ""), chr(39), ""), chr(34), "")
					sql = 	"	insert into we_dropship_tracking_history(orderid, dropshipper, trackingType, trackingNum, shipment_date, adminUser) " & vbcrlf & _
							"	values('" & rs("order_number") & "', '" & replace(rs("dropshipper"), "'", "''") & "', '" & replace(rs("tracking_type"), "'", "''") & "', '" & trackingNum & "', '" & rs("shipment_date") & "', '" & adminUserID & "')"
					session("errorSQL") = sql
					oConn.execute(sql)
					
					' update tracking number on orders if applicable ============================================
					sql = 	"select trackingnum from we_orders where orderid = '" & rs("order_number") & "'"
					session("errorSQL") = sql
					set rsOrder = oConn.execute(sql)
					if not rsOrder.eof then
						if isnull(rsOrder("trackingnum")) or rsOrder("trackingnum") = "" then
							oConn.execute("update we_orders set trackingnum = '" & trackingNum & "' where orderid = '" & rs("order_number") & "'")
						end if
					end if
					'============================================================================================
										
					' order notes ===============================================================================
					AddNotes = "Dropship(" & rs("dropshipper") & ") tracking upload #" & trackingNum & vbcrlf & " - tracking type:" & rs("tracking_type")
					bOrderNote = false
					curOrderNotes = ""
					sql	=	"select	ordernotes from we_ordernotes where orderid = '" & rs("order_number") & "'"
					session("errorSQL") = sql
					set objRsNotes = oConn.execute(sql)
					
					if not objRsNotes.EOF then
						bOrderNote		=	true
						curOrderNotes 	= 	objRsNotes("ordernotes")
					end if
					AddNotes = SQLquote(curOrderNotes) & vbcrlf & formatNotes(AddNotes)
					
					if bOrderNote then 
						sql = 	"update we_ordernotes set ordernotes = '" & AddNotes & "' where orderid = '" & rs("order_number") & "'"
					else
						sql = 	"insert into we_ordernotes(orderid, ordernotes) values('" & rs("order_number") & "', '" & AddNotes & "') "
					end if
					session("errorSQL") = sql
					oConn.execute(sql)
					'============================================================================================
					
					orderProcessed = orderProcessed & "," & rs("order_number")
					rowProcessed = rowProcessed + 1
				end if
				rs.movenext
			loop

			if objUpload.form("chkEmail") <> "Y" then
				call sendEmail(getCommaString(orderProcessed))
			end if
		end if
	end if
end if
%>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="1000">
	<tr>
    	<td><h1 style="margin:0px;">Upload dropshipper tracking numbers</h1></td>
    </tr>
    <tr>
    	<td style="font-size:12px; border-bottom:1px solid #000; padding-bottom:10px;">
            <ul>
            <li>This application allows the user to upload dropshipper's shipment tracking numbers.</li>
            <li>How it works</li>
            <ol class="smlText" style="margin-top:2px;">
				<li>single item order: update tracking numbers to the orders and send out shipment emails</li>
				<li>multiple item order: update tracking numbers to the notes and send out shipment emails only for dropship items</li>
			</ol>
            </ul>
        </td>
    </tr>
    <tr>
    	<td class="tab-header-off" >
			<table border="0" cellpadding="5" cellspacing="0" align="center" style="font-weight:bold; font-size:11px;">
            	<tr>
                	<td align="center" width="50">ID</td>
                	<td align="center" width="50">Store</td>
                	<td align="center" width="80">OrderID</td>
                	<td align="center" width="80">Drop Shipper</td>
                	<td align="center" width="100">Tracking Type</td>
                	<td align="center" width="350">Tracking Number</td>
                	<td align="center" width="100">Shipment Date</td>
                	<td align="center" width="80">Admin User</td>
                	<td align="center" width="120">processed Date</td>
				</tr>
			</table>
		</td>
	</tr>
    <tr>
    	<td style="border-bottom:1px soild #000;">
			<%
            sql = 	"	select	x.site_id, a.id, a.orderid, a.dropshipper, a.trackingType, a.trackingNum, convert(varchar(10), a.shipment_date, 20) shipment_date" & vbcrlf & _
					"		, 	isnull(b.fname, 'Unknown') adminUser, convert(varchar(10), a.processed_date, 20) processed_date, c.scandate, c.orderdatetime, c.store, nullif(x.logo, '') site_logo" & vbcrlf & _
					"		,	x.shortDesc site, x.color logo_color" & vbcrlf & _
					"	from	we_dropship_tracking_history a left outer join we_adminUsers b" & vbcrlf & _
					"		on	a.adminUser = b.adminID left outer join we_orders c" & vbcrlf & _
					"		on	a.orderid = c.orderid left outer join xstore x" & vbcrlf & _
					"		on	c.store = x.site_id" & vbcrlf & _
					"	where	a.processed_date > dateadd(mm, -1, getdate())" & vbcrlf & _
					"	order by 1 desc" & vbcrlf
            session("errorSQL") = sql
            set rs = oConn.execute(sql)
			if rs.eof then
			%>
        	<div style="padding:20px;" align="center">No upload history found</div>
            <%
			else
			%>
        	<div style="height:200px; overflow:scroll;">
			<table border="0" cellpadding="5" cellspacing="0" align="center" style="font-size:11px;">            
	            <%
				nRow = 0
				do until rs.eof
					nRow = nRow + 1
				%>
				<tr class="grid_row_<%=(nRow mod 2)%>" onMouseOver="this.className='mouseover'" onMouseOut="this.className='grid_row_<%=(nRow mod 2)%>'" >
                	<td align="center" width="50"><%=rs("id")%></td>
					<td align="center" width="50">                    
					<%
                    if isnull(rs("site_logo")) then 
                    %>
                    <font color="<%=rs("logo_color")%>"><b><%=rs("site")%></b></font>
                    <%
                    else
                    %>
                    <img src="<%=rs("site_logo")%>" border="0" width="16" height="16" />
                    <%
                    end if
                    %>                    
                    </td>
                	<td align="center" width="80"><a href="#" onClick="printinvoice('<%=rs("orderid")%>');"><%=rs("orderid")%></a></td>
                	<td align="center" width="80"><%=rs("dropshipper")%></td>
                	<td align="center" width="100"><%=rs("trackingType")%></td>

				<%if left(rs("trackingNum"),2) = "1Z" then%>
					<td align="center" width="350"><a href="javascript:fnTracking(1,'<%=rs("trackingNum")%>');"><%=rs("trackingNum")%></a></td>
				<%elseif left(rs("trackingNum"),2) = "EO" or left(rs("trackingNum"),2) = "91" or left(rs("trackingNum"),2) = "CJ" or left(rs("trackingNum"),2) = "LN" then%>
					<td align="center" width="350"><a href="javascript:fnTracking(2,'<%=rs("trackingNum")%>');"><%=rs("trackingNum")%></a></td>
				<%elseif left(rs("trackingNum"),2) = "04" then%>
					<td align="center" width="350"><a href="javascript:fnTracking(3,'<%=rs("trackingNum")%>');"><%=rs("trackingNum")%></a></td>
				<%elseif left(rs("trackingNum"),1) = "9" or left(rs("trackingNum"),1) = "4" then%>
                	<td align="center" width="350"><a href="javascript:fnTracking(5,'<%=rs("trackingNum")%>');"><%=rs("trackingNum")%></a></td>
				<%elseif not isnull(rs("scanDate")) then
                    if rs("site_id") = "0" then
                        if rs("scandate") < cdate("04/02/2012") then	'for we newgistics
						%>
						<td align="center" width="350"><a href="javascript:fnTracking(6,'<%=rs("OrderId")%>');"><%=rs("trackingNum")%></a></td>
                        <%else%>
						<td align="center" width="350"><a href="javascript:fnTracking(3,'WE<%=rs("OrderId")%>');"><%=rs("trackingNum")%></a></td>
                        <%end if%>
                    <%elseif rs("site_id") = "2" then
                        if rs("scandate") >= cdate("02/21/2012") and rs("scandate") < cdate("04/11/2012") then%>
						<td align="center" width="350"><a href="javascript:fnTracking(6,'<%=rs("OrderId")%>');"><%=rs("trackingNum")%></a></td>
                        <%else%>
						<td align="center" width="350"><a href="javascript:fnTracking(3,'WE<%=rs("OrderId")%>');"><%=rs("trackingNum")%></a></td>
                        <%end if%>
                    <%elseif rs("site_id") = "1" or rs("site_id") = "3" or rs("site_id") = "10" then	' for all the other sites' newgistics
                        if rs("scandate") >= cdate("02/21/2012") and rs("scandate") < cdate("07/25/2012") then%>
						<td align="center" width="350"><a href="javascript:fnTracking(6,'<%=rs("OrderId")%>');"><%=rs("trackingNum")%></a></td>
                        <%else%>
						<td align="center" width="350"><a href="javascript:fnTracking(3,'WE<%=rs("OrderId")%>');"><%=rs("trackingNum")%></a></td>
                        <%end if%>
					<%end if%>
				<%elseif left(rs("trackingNum"),2) = "D1" then%>
					<td align="center" width="350"><a href="javascript:fnTracking(7,'<%=rs("trackingNum")%>');"><%=rs("trackingNum")%></a></td>
				<%else%>
					<td align="center" width="350"><%=rs("trackingNum")%></td>
				<%end if%>
                	<td align="center" width="100"><%=rs("shipment_date")%></td>
                	<td align="center" width="80"><%=rs("adminUser")%></td>
                	<td align="center" width="100"><%=rs("processed_date")%></td>
				</tr>
					<%
                    rs.movenext
                loop
                %>
			</table>
            </div>            
            <%
				if rowProcessed > 0 then
					response.write "<h3 style=""color:blue;"">" & rowProcessed & " dropshippers tracking updated</h3>"
				end if
			end if
			%>
		</td>
	</tr>
	<tr>
    	<td style="padding-top:10px;">
            <form enctype="multipart/form-data" action="upload_dropship_tracking.asp" method="post">        
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="1000">
            	<tr>
                	<td align="left"><input type="FILE" name="FILE1"></td>
                	<td align="right"><input type="checkbox" name="chkEmail" value="Y"> Do not send Emails</td>
                    <td align="left" style="padding-left:20px;"><input type="SUBMIT" name="upload" value="Upload"></td>
                </tr>
            </table>
            </form>
		</td>
    </tr>
</table>    
<script>
function printinvoice(orderid) {
	var url="view_invoice.asp?orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}

function fnTracking(shiptype,trackingnum) {
	if (shiptype == 1) {
		window.open("http://wwwapps.ups.com/WebTracking/OnlineTool?InquiryNumber1=" + trackingnum,"tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else if (shiptype == 2) {
		window.open("https://tools.usps.com/go/TrackConfirmAction!execute.action?formattedLabel=" + trackingnum + "&CAMEFROM=OK","tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else if (shiptype == 3) {
		window.open("http://webtrack.dhlglobalmail.com/?trackingnumber=" + trackingnum,"tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else if (shiptype == 4) {
		window.open("http://www.ups-mi.net/packageID/PackageID.aspx?PID=" + trackingnum,"tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else if (shiptype == 5) {
		window.open("https://tools.usps.com/go/TrackConfirmAction!execute.action?formattedLabel=" + trackingnum,"tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else if (shiptype == 6) {
		window.open("http://www.shipmentmanager.com/Portal.aspx?MerchantID=1413&TargetPageID=38&TrackingKey=BID&TrackingIdentifier=" + trackingnum,"tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	} else if (shiptype == 7) {
		window.open("http://www.ontrac.com/trackingres.asp?tracking_number=" + trackingnum,"tracking","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	}
}
</script>

<%

sub sendEmail(strOrderID)
	if strOrderID <> "" then
		sql	=	"		select	x.site_id, x.shortdesc, x.longdesc, a.orderid, a.scandate, a.shiptype origShipType" & vbcrlf & _
				"			,	case when w.trackingnum like '1Z%' then 'http://wwwapps.ups.com/WebTracking/OnlineTool?InquiryNumber1=' + isnull(w.trackingNum, '')" & vbcrlf & _
				"					when w.trackingnum like 'EO%' or w.trackingnum like '91%' or w.trackingnum like 'CJ%' or w.trackingnum like 'LN%' then 'http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=' + isnull(w.trackingNum, '')" & vbcrlf & _
				"					when w.trackingnum like '04%' then 'http://webtrack.dhlglobalmail.com/?trackingnumber=' + isnull(w.trackingNum, '')" & vbcrlf & _
				"					when w.trackingnum like '9%' or w.trackingnum like '4%' then 'http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=' + isnull(w.trackingNum, '')" & vbcrlf & _
				"					when a.trackingnum like 'D1%' then 'http://www.ontrac.com/trackingres.asp?tracking_number=' + isnull(a.trackingNum, '') " & vbcrlf & _
				"					else  " & vbcrlf & _
				"						case when a.store = 0 then " & vbcrlf & _
				"								case when a.scandate >= '12/05/2011' and a.scandate < '4/2/2012' then" & vbcrlf & _
				"									'http://www.shipmentmanager.com/Portal.aspx?MerchantID=' + convert(varchar(10), x.newgistics_customerid) + '&TargetPageID=38&TrackingKey=BID&TrackingIdentifier=' + convert(varchar(20), a.orderid)" & vbcrlf & _
				"								else" & vbcrlf & _
				"									'http://webtrack.dhlglobalmail.com/?mobile=&trackingnumber=WE' + convert(varchar(20), a.orderid)" & vbcrlf & _
				"								end" & vbcrlf & _
				"							when a.store = 2 then" & vbcrlf & _
				"								case when a.scandate >= '02/12/2012' and a.scandate < '4/11/2012' then" & vbcrlf & _
				"									'http://www.shipmentmanager.com/Portal.aspx?MerchantID=' + convert(varchar(10), x.newgistics_customerid) + '&TargetPageID=38&TrackingKey=BID&TrackingIdentifier=' + convert(varchar(20), a.orderid)" & vbcrlf & _
				"								else" & vbcrlf & _
				"									'http://webtrack.dhlglobalmail.com/?mobile=&trackingnumber=WE' + convert(varchar(20), a.orderid)" & vbcrlf & _
				"								end" & vbcrlf & _
				"							when a.store in (1,3,10) and a.scandate >= '02/12/2012' then" & vbcrlf & _
				"								case when a.scandate >= '02/12/2012' and a.scandate < '7/25/2012' then" & vbcrlf & _
				"									'http://www.shipmentmanager.com/Portal.aspx?MerchantID=' + convert(varchar(10), x.newgistics_customerid) + '&TargetPageID=38&TrackingKey=BID&TrackingIdentifier=' + convert(varchar(20), a.orderid)" & vbcrlf & _
				"								else" & vbcrlf & _
				"									'http://webtrack.dhlglobalmail.com/?mobile=&trackingnumber=WE' + convert(varchar(20), a.orderid)" & vbcrlf & _
				"								end" & vbcrlf & _
				"							else " & vbcrlf & _
				"							'http://www.ups-mi.net/packageID/PackageID.aspx?PID=' + 'WE' + convert(varchar(20), a.orderid)" & vbcrlf & _
				"						end" & vbcrlf & _
				"				end track_link" & vbcrlf & _
				"			,	case when m.id is not null then " & vbcrlf & _
				"						isnull(m.brand, '') + ' ' + isnull(m.model, '') + ' ' + isnull(m.artist, '') + ' ' + isnull(m.designName, '') + ' Music Skins'" & vbcrlf & _
				"					else" & vbcrlf & _
				"						case when x.site_id = 0 then c.itemdesc" & vbcrlf & _
				"							when x.site_id = 1 then c.itemdesc_ca" & vbcrlf & _
				"							when x.site_id = 2 then c.itemdesc_co" & vbcrlf & _
				"							when x.site_id = 3 then c.itemdesc_ps" & vbcrlf & _
				"							when x.site_id = 10 then isnull(c.itemdesc_er, c.itemdesc)" & vbcrlf & _
				"							else c.itemdesc" & vbcrlf & _
				"						end" & vbcrlf & _
				"				end itemdesc" & vbcrlf & _
				"			,	isnull(w.trackingType, 'First Class') shiptype" & vbcrlf & _
				"			,	isnull(b.quantity, 0) quantity" & vbcrlf & _
				"			,	isnull(case when m.id is not null then m.price_we" & vbcrlf & _
				"							else" & vbcrlf & _
				"								case when x.site_id = 0 then c.price_our" & vbcrlf & _
				"									when x.site_id = 1 then c.price_ca" & vbcrlf & _
				"									when x.site_id = 2 then c.price_co" & vbcrlf & _
				"									when x.site_id = 3 then c.price_ps" & vbcrlf & _
				"									when x.site_id = 10 then case when (c.price_our-5.0) < 3.99 then 3.99 else (c.price_our-5.0) end" & vbcrlf & _
				"									else c.price_our" & vbcrlf & _
				"								end" & vbcrlf & _
				"						end, 0.00) price" & vbcrlf & _
				"			,	cast(case when m.id is not null then 1 " & vbcrlf & _
				"								else" & vbcrlf & _
				"									case when c.vendor in ('CM', 'DS', 'MLD') then 1 " & vbcrlf & _
				"										when c.partnumber like '%HYP%' then 1" & vbcrlf & _
				"										else 0 " & vbcrlf & _
				"									end" & vbcrlf & _
				"							end as bit) isVendorOrder" & vbcrlf & _
				"			,	case when m.id is not null then m.id else c.itemid end itemid" & vbcrlf & _
				"			,	case when m.id is not null then 'MS' else c.vendor end vendor" & vbcrlf & _
				"			,	v.fname, v.lname, v.phone, v.email" & vbcrlf & _
				"			,	isnull(a.shippingid, 0) shippingid" & vbcrlf & _
				"			,	v.sAddress1, v.sAddress2, v.sCity, v.sState, v.sZip, case when v.sCountry = 'canada' then 'Canada' else '' end sCountry" & vbcrlf & _
				"			,	s.sAddress1 ssAddress1, s.sAddress2 ssAddress2, s.sCity ssCity, s.sState ssState, s.sZip ssZip, case when s.sCountry = 'canada' then 'Canada' else '' end ssCountry" & vbcrlf & _
				"		from	we_orders a with (nolock) join xstore x with (nolock)" & vbcrlf & _
				"			on	a.store = x.site_id join we_orderdetails b with (nolock)" & vbcrlf & _
				"			on	a.orderid = b.orderid join v_accounts v with (nolock)" & vbcrlf & _
				"			on	a.store = v.site_id and a.accountid = v.accountid join we_dropship_tracking_history w with (nolock)" & vbcrlf & _
				"			on	a.orderid = w.orderid left outer join we_items c with (nolock)" & vbcrlf & _
				"			on	b.itemid = c.itemid left outer join we_items_musicskins m with (nolock)" & vbcrlf & _
				"			on	b.itemid = m.id left outer join we_addl_shipping_addr s with (nolock)" & vbcrlf & _
				"			on	a.shippingid = s.id " & vbcrlf & _
				"		where	w.orderid in (" & strOrderID & ")" & vbcrlf & _
				"			and	(m.id is not null or c.vendor in ('CM', 'DS', 'MLD') or c.partnumber like '%HYP%') " & vbcrlf & _
				"			and (a.extordertype not in (9) or a.extordertype is null) " & vbcrlf & _
				"		order by a.orderid, itemid" & vbcrlf
'		response.write "<pre>" & sql & "</pre>"
		session("errorSQL") = sql
		set objRsOrders = oConn.execute(sql)
				
		strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\admin\scheduled_tasks\shipment_confirm_email_template_dropship.html")
		dup_strTemplate = strTemplate
		
		lastOrderID 	= 	-1
		curOrderID		=	-1
		strOrderDetails	=	""
		numItem			=	0

		do until objRsOrders.eof
			curOrderID	=	objRsOrders("orderid")
			site_id		=	objRsOrders("site_id")
			if lastOrderID <> curOrderID then
				numItem = 0
				if strOrderDetails <> "" then
					strOrderDetails	=	strOrderDetails	&	"			        	</table>"
					strTemplate = replace(strTemplate, "[LOGO IMG LINK]", logoLink)
					strTemplate = replace(strTemplate, "[HEADER TEXT]", headerText)
					strTemplate = replace(strTemplate, "[CUSTOMER NAME]", custName)
					strTemplate = replace(strTemplate, "[TOP TEXT]", topText)
					strTemplate = replace(strTemplate, "[SHIPPED TO]", strShipAddress)
					strTemplate = replace(strTemplate, "[ORDER ID]", lastOrderID)
					strTemplate = replace(strTemplate, "[ORDER DETAILS]", strOrderDetails)
					strTemplate = replace(strTemplate, "[MEMO]", "")
					strTemplate = replace(strTemplate, "[STORE NAME]", storeName)
					strTemplate = replace(strTemplate, "[SHIPPING TYPE]", shipMethod)					
					strTemplate = replace(strTemplate, "[TRACK LINK]", trackLink)

'					response.write cdo_to & "<br>" & strTemplate & "<br /><br />"
					call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "shipping@wirelessemporium.com")
'					call cdosend(cdo_from, "doldding82@gmail.com", "Sent to [" & cdo_to & "]. " & cdo_subject, strTemplate, "")
								
					strTemplate 	= 	dup_strTemplate	'refresh the template for an another e-mail.				
				end if

				strOrderDetails	=	"			        	<table width=""100%"" border=""1"" cellspacing=""1"" cellpadding=""5"" style=""font-family:Arial; font-size:11px; border-collapse:collapse;"">" & vbcrlf & _
									"			        		<tr>" & vbcrlf & _
									"								<td align=""center"" valign=""top"" width=""70%"" bgcolor=""#ededed""><b>Item Description</b></td>" & vbcrlf & _
									"								<td align=""center"" valign=""top"" width=""10%"" bgcolor=""#ededed""><b>Quantity</b></td>" & vbcrlf & _
									"								<td align=""center"" valign=""top"" width=""10%"" bgcolor=""#ededed""><b>Price</b></td>" & vbcrlf & _
									"								<td align=""center"" valign=""top"" width=""10%"" bgcolor=""#ededed""><b>SubTotal</b></td>" & vbcrlf & _
									"							</tr>" & vbcrlf				
			end if
			
			'========================================== SET REPLACEMENT TEXTS ================================================
			lastOrderID 		= 	curOrderID
			fname				=	objRsOrders("fname")
			custName			=	ucase(left(fname, 1)) & lcase(right(fname,len(fname)-1))
			email				=	objRsOrders("email")
			cdo_to				=	email
			
			storeName			=	objRsOrders("longDesc")
			topText				=	"Thank you for ordering from " & storeName & ".com! We would like to notify you that your order has been SHIPPED to:"			
			trackLink			=	objRsOrders("track_link")	'shipping track
			cdo_from			=	storeName & "<sales@" & Lcase(storeName) & ".com>"
			shipMethod			=	objRsOrders("shiptype")
						
			if objRsOrders("shippingid") > 0 then
				strShipAddress = formatAddress(objRsOrders("ssAddress1"),objRsOrders("ssAddress2"),objRsOrders("ssCity"),objRsOrders("ssState"),objRsOrders("ssZip"))
			else
				strShipAddress = formatAddress(objRsOrders("sAddress1"),objRsOrders("sAddress2"),objRsOrders("sCity"),objRsOrders("sState"),objRsOrders("sZip"))			
			end if
			
			select case site_id
				case 0
					headerText	=	"Order #" & curOrderID & " Successfully Shipped from WirelessEmporium.com"
					logoLink	=	"http://www.wirelessemporium.com/images/global/we_logo_solo.gif"
				case 1
					headerText	=	"Your CellphoneAccents.com order #" & curOrderID & " is on the way!"
					logoLink	=	"http://www.cellphoneaccents.com/images/logo.jpg"
				case 2
					headerText	=	"Shipment Status from CellularOutfitter.com Order #" & curOrderID
					logoLink	=	"http://www.cellularoutfitter.com/images/CellularOutfitter_STANDARD.jpg"
				case 3
					headerText	=	"Phonesale.com Order #" & curOrderID & " - Shipment Status"
					logoLink	=	"http://www.phonesale.com/images/phonesale.jpg"
				case 10
					headerText	=	"Your TabletMall.com Order #" & curOrderID & " - Shipment Status"
					logoLink	=	"http://www.tabletmall.com/images/logo.gif"					
			end select

			cdo_subject	=	headerText			
			'=================================================================================================================
			
			'============================================== ORDER DETAIL TABLE ===============================================
			if instr(email, "@") > 0 and len(custName) > 0 then
				numItem = numItem + 1
				
				select case site_id
					case 0
						if objRsOrders("vendor") = "MS" then
							productLink = "http://www.wirelessemporium.com/p-ms-" & objRsOrders("itemid") & "-" & formatSEO(objRsOrders("itemdesc"))
						else
							productLink = "http://www.wirelessemporium.com/p-" & objRsOrders("itemid") & "-" & formatSEO(objRsOrders("itemdesc"))
						end if
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"" style=""font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#FB5908; text-decoration:none;"">" & objRsOrders("itemdesc") & "</a>"
					case 1
						productLink = "http://www.cellphoneaccents.com/p-" & objRsOrders("itemid") & "-" & formatSEO_CA(objRsOrders("itemdesc")) & ".html"
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"" style=""font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#601D90; text-decoration:none;"">" & objRsOrders("itemdesc") & "</a>"
					case 2
						productLink = "http://www.cellularoutfitter.com/p-" & objRsOrders("itemid") & "-" & formatSEO_CA(objRsOrders("itemdesc")) & ".html"
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"" style=""font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#0099CC; text-decoration:none;"">" & objRsOrders("itemdesc") & "</a>"					
					case 3
						if objRsOrders("typeid") = 16 then
							productLink = "http://www.phonesale.com/" & formatSEO_PS(objRsOrders("brandname")) & "/cell-phones/p-" & objRsOrders("itemid")+300001 & "-tc-" & objRsOrders("typeid") & "-" & formatSEO_PS(objRsOrders("itemdesc")) & ".html"
						else
							productLink = "http://www.phonesale.com/accessories/" & formatSEO_PS(objRsOrders("typename")) & "/p-" & objRsOrders("itemid")+300001 & "-tc-" & objRsOrders("typeid") & "-" & formatSEO_PS(objRsOrders("itemdesc")) & ".html"
						end if
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"" style=""font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#9B1D21; text-decoration:none;"">" & objRsOrders("itemdesc") & "</a>"
					case 10
						productLink = "http://www.tabletmall.com/" & formatSEO(objRsOrders("itemdesc")) & "-p-" & objRsOrders("itemid")
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"" style=""font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#357043; text-decoration:none;"">" & objRsOrders("itemdesc") & "</a>"
				end select

				qty			=	cdbl(objRsOrders("quantity"))
				price		=	cdbl(objRsOrders("price"))
				subtotal	=	cdbl(qty * price)
				
				if objRsOrders("isVendorOrder") then
					itemDesc = itemDesc & " <b>- This item shipped directly from manufacture.</b>"
				end if
				
				strOrderDetails	=	strOrderDetails	&	"			        		<tr>" & vbcrlf & _
														"								<td align=""left"" valign=""top"" width=""70%""><b>" & numItem & ":</b> " & itemDesc & "</td>" & vbcrlf & _
														"								<td align=""right"" valign=""top"" width=""10%"">" & formatnumber(qty,0) & "</td>" & vbcrlf & _
														"								<td align=""right"" valign=""top"" width=""10%"">" & formatcurrency(price) & "</td>" & vbcrlf & _
														"								<td align=""right"" valign=""top"" width=""10%"">" & formatcurrency(subtotal) & "</td>" & vbcrlf & _
														"							</tr>" & vbcrlf
			end if
			'=================================================================================================================
			
			objRsOrders.movenext
		loop
		
		if numItem > 0 then
			if strOrderDetails <> "" then
				strOrderDetails	=	strOrderDetails	&	"			        	</table>"
				strTemplate = replace(strTemplate, "[LOGO IMG LINK]", logoLink)
				strTemplate = replace(strTemplate, "[HEADER TEXT]", headerText)
				strTemplate = replace(strTemplate, "[CUSTOMER NAME]", custName)
				strTemplate = replace(strTemplate, "[TOP TEXT]", topText)
				strTemplate = replace(strTemplate, "[SHIPPED TO]", strShipAddress)
				strTemplate = replace(strTemplate, "[ORDER ID]", lastOrderID)					
				strTemplate = replace(strTemplate, "[ORDER DETAILS]", strOrderDetails)
				strTemplate = replace(strTemplate, "[MEMO]", "")
				strTemplate = replace(strTemplate, "[STORE NAME]", storeName)
				strTemplate = replace(strTemplate, "[SHIPPING TYPE]", shipMethod)
				strTemplate = replace(strTemplate, "[TRACK LINK]", trackLink)
						
				'send an e-mail
'				response.write cdo_to & "<br>" & strTemplate & "<br /><br />"
				call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "terry@wirelessemporium.com,doldding82@gmail.com,shipping@wirelessemporium.com")
'				call cdosend(cdo_from, "doldding82@gmail.com", "Sent to [" & cdo_to & "]. " & cdo_subject, strTemplate, "")
			
				strTemplate 	= 	dup_strTemplate	'refresh the template for an another e-mail.
			end if
		end if
	end if
end sub

function formatAddress(sAdd1,sAdd2,sCity,sState,sZip)
	dim strTemp
	strTemp = sAdd1
	if sAdd2 <> "" then 
		strTemp = strTemp & "<br />" & sAdd2 
	end if
	strTemp = strTemp & "<br />" & sCity & ", " & sState & "&nbsp;" & sZip & "<br />" & vbcrlf
	formatAddress = strTemp
end function

function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
end function

function getCommaString(val)
	if "" = trim(val) then getCommaString = "" end if
	
	dim arr : arr = split(val, ",")
	dim str : str = ""

	for i=0 to ubound(arr)
		if "" <> trim(arr(i)) then str = str & trim(arr(i)) & "," end if
	next
	
	if "," = right(str, 1) then str = left(str, len(str) - 1) end if 
	
	getCommaString = str
end function 

sub cdosend2(strFrom, strTo, strSubject, strBody, strBcc)
	if instr(trim(strTo), "@") > 0 and instr(trim(strTo), " ") = 0 then
		set objEmail = CreateObject("CDO.Message")
		with objEmail
			.From = strFrom
			.To = strTo
			.Bcc = strBcc
			.Subject = strSubject
			.HTMLBody = strBody
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Update
			.Send
		end with
	end if
end sub

sub cdosend(strFrom, strTo, strSubject, strBody, strBcc)
	if instr(trim(strTo), "@") > 0 and instr(trim(strTo), " ") = 0 then
		if isValidEmail(strTo) then
			set objEmail = CreateObject("CDO.Message")
			with objEmail
				.From = strFrom
				.To = strTo
				.Bcc = strBcc
				.Subject = strSubject
				.HTMLBody = strBody
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
				.Configuration.Fields.Update
				.Send
			end with
		end if
	end if
end sub

function formatSEO_CA(val)
	if not isNull(val) then
		formatSEO_CA = lCase(trim(replace(val,"&trade;","-")))
		formatSEO_CA = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO_CA," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO_CA = replace(replace(replace(replace(formatSEO_CA,"(","-"),")","-"),";","-"),":","-")
		formatSEO_CA = replace(replace(replace(replace(formatSEO_CA, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO_CA = replace(replace(formatSEO_CA, "---", "-"), "--", "-")
	else
		formatSEO_CA = ""
	end if
	select case formatSEO_CA
		case "sidekick" : formatSEO_CA = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO_CA = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO_CA = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO_CA = "sprint-nextel"
		case "u-s--cellular" : formatSEO_CA = "us-cellular"
		case "antennas-parts" : formatSEO_CA = "antennas"
		case "faceplates" : formatSEO_CA = "films-faceplates"
		case "bling-kits---charms" : formatSEO_CA = "charms-bling-kits"
		case "data-cable---memory" : formatSEO_CA = "data-cables-memory-cards"
		case "hands-free" : formatSEO_CA = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO_CA = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO_CA = "holsters-phone-holders"
		case "leather-cases" : formatSEO_CA = "cases-pouches-skins"
		case "full-body-protectors" : formatSEO_CA = "protective-films"
	end select
end function

function formatSEO_CO(val)
	if not isNull(val) then
		formatSEO_CO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO_CO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO_CO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO_CO = replace(replace(replace(replace(replace(formatSEO_CO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO_CO = replace(replace(replace(replace(formatSEO_CO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO_CO = replace(replace(formatSEO_CO, "---", "-"), "--", "-")		
	else
		formatSEO_CO = ""
	end if
	select case formatSEO_CO
		case "sidekick" : formatSEO_CO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO_CO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO_CO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO_CO = "sprint-nextel"
		case "u-s--cellular" : formatSEO_CO = "us-cellular"
		case "antennas-parts" : formatSEO_CO = "antennas"
		case "faceplates" : formatSEO_CO = "covers-screen-guards"
		case "bling-kits---charms" : formatSEO_CO = "bling-kits-charms"
		case "data-cable---memory" : formatSEO_CO = "data-cables-memory-cards"
		case "hands-free" : formatSEO_CO = "hands-free-kits-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO_CO = "holsters-car-mounts"
		case "leather-cases" : formatSEO_CO = "cases-pouches"
		case "cell-phones" : formatSEO_CO = "wholesale-cell-phone"
		case "full-body-protectors" : formatSEO_CO = "invisible-film-protectors"
	end select
end function

function formatSEO_PS(val)
	if not isNull(val) then
		formatSEO_PS = lCase(trim(replace(val,"&trade;","-")))
		formatSEO_PS = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO_PS," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO_PS = replace(replace(replace(replace(replace(formatSEO_PS,"(","-"),")","-"),";","-"),":","-"),"+","-")
		formatSEO_PS = replace(formatSEO_PS,"---","-")
		formatSEO_PS = replace(formatSEO_PS,"--","-")
		formatSEO_PS = replace(replace(replace(replace(formatSEO_PS, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
	else
		formatSEO_PS = ""
	end if
	select case formatSEO_PS
		case "sidekick" : formatSEO_PS = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO_PS = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO_PS = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO_PS = "sprint-nextel"
		case "u-s--cellular" : formatSEO_PS = "us-cellular"
		case "antennas-parts" : formatSEO_PS = "antennas"
		case "faceplates" : formatSEO_PS = "faceplates-skins"
		case "verizon" : formatSEO_PS = "verizon-wireless"
		case "unlocked" : formatSEO_PS = "unlocked"
		case "audiovox" : formatSEO_PS = "utstarcom"
		case "data-cable-memory" : formatSEO_PS = "data-cables-memory-cards"
		case "hands-free" : formatSEO_PS = "hands-free-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO_PS = "holsters-holders"
		case "leather-cases" : formatSEO_PS = "cases-and-covers"
		case "full-body-protectors" : formatSEO_PS = "full-body-skins"
	end select
	formatSEO_PS = replace(formatSEO_PS,"---","-")
	formatSEO_PS = replace(formatSEO_PS,"--","-")
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")		
		formatSEO = replace(replace(formatSEO, "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function

Function isValidEmail(emailAddress)
	'Declare variables 
	Dim ValidEmail, emailParts, iLoopCounter, emailChar, acceptableChars, last2Chars 
	ValidEmail = True 'set the default result to True
	'acceptableChars are the characters that we will allow in our email
	acceptableChars="abcdefghijklmnopqrstuvwxyz.-_@"
	'use the Split function to create an array with the @ as the separator
	'so if your email was test@tester.com the email would be split into an array
	'with the first array element holding "test" and the second "tester.com"
	emailParts = Split(emailAddress, "@")
	'check to make sure that there is only 1 @ and that there are 2 parts
	'remember arrays are zero based
	'Using the UBound function will return the highest element in the array
	'So if it's a valid email the UBound function will return 1, i.e. 0 start
	If UBound(emailParts) <> 1 Then
	ValidEmail = false
	Else 
	'Check the length of each part of the email address

	'first part can be just one character, 2nd part must be atleast 4
	If Len(emailParts(0))<1 OR Len(emailParts(1))<4 Then
	ValidEmail = false
	End If
	'check first character on the left part isn't a "." using Left function
	If Left(emailParts(0), 1)="." Then
	ValidEmail = false
	End If 
	'check that there is a . in the second part of the email address - .com
	If InStr(emailParts(1), ".") <= 0 Then
	ValidEmail = false 
	End If 
	'We know there's a . now make sure there are 2 characters after the . for valid email
	last2Chars=Right(emailParts(1),2)
	If InStr(last2chars,".") Then
	ValidEmail = false
	End If
	'check that there shouldn't be a _ in the second part of the email address 
	If InStr(emailParts(1), "_") >0 Then
	ValidEmail = false 
	End If 
	End If
	'loop through each character of email
	For iLoopCounter = 1 to Len(emailAddress)
	'Use Lcase & Mid functions, Mid function used to return each individual character
	'in the email, and then Lcase converts it into lowercase
	emailChar = Lcase(Mid(emailAddress, iLoopCounter, 1)) 
	'Check if the emailAddress characters are acceptable
	If InStr(acceptableChars, emailChar) = 0 and Not IsNumeric(emailChar) Then
	ValidEmail = false
	End if
	Next
	'check if there is 2 . in a row
	If InStr(emailAddress, "..") > 0 Then
	ValidEmail=false
	End If
	'check if there is @. in a row
	If InStr(emailAddress, "@.") > 0 Then
	ValidEmail=false
	End If 
	isValidEmail=ValidEmail
End function
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->