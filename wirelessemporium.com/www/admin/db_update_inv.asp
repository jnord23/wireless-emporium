<%
'latest update removes inv adjustment functions
response.buffer = false
dim thisUser, adminID
pageTitle = "WE Admin Site - Update WE Database - Update Product Details"
header = 1
thisUser = Request.Cookies("username")
adminID = session("adminID")
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<style>
.grid_header 			{ background-color:#BDBDBD; font-size: 10pt; color: #000000; font-family: Arial, Helvetica, sans-serif;}
.grid_header_td			{ border-right-color:#FFFFFF; border-right-width:1px; border-right-style:solid;}
.grid_row_0 			{ background-color:#FFFFFF;}
.grid_row_1 			{ background-color:#E5E5E5;}
</style>
<font face="Arial,Helvetica">
	<p><a href="/admin/db_search_brands.asp?searchID=1">Back to Brands</a></p>
</font>
<script>
var isPageLoaded = false;

function onItemUpdate(rowid)
{
	if (isPageLoaded)
	{
		var	updateURL		=	"";
		var	itemid			=	document.getElementById('id_itemID_' + rowid).value;
		var	partnumber		=	document.getElementById('id_PartNumber_' + rowid).value;
		var	prev_partnumber	=	document.getElementById('prev_id_PartNumber_' + rowid).value;	
		var	itemweight		=	document.getElementById('id_itemWeight_' + rowid).value;	
		var	cogs			=	document.getElementById('id_COGS_' + rowid).value;	
		var	vendor			=	document.getElementById('id_Vendor_' + rowid).value;	
		var	seasonal		=	document.getElementById('id_Seasonal_' + rowid).checked;
		var	ghost			=	document.getElementById('id_ghost_' + rowid).checked;
		var	hidelive		=	document.getElementById('id_hideLive_' + rowid).checked;	
		var	nodiscount		=	document.getElementById('id_NoDiscount_' + rowid).checked;
		var	price_our		=	document.getElementById('id_price_our_' + rowid).value;
		var	price_ca		=	document.getElementById('id_price_ca_' + rowid).value;
		var	price_co		=	document.getElementById('id_price_co_' + rowid).value;
		var	price_ps		=	document.getElementById('id_price_ps_' + rowid).value;
		var	price_er		=	document.getElementById('id_price_er_' + rowid).value;
		var	flag			=	document.getElementById('id_Flag_' + rowid).value;
		
		seasonal	=	(seasonal) ? 1:0;
		ghost		=	(ghost) ? 1:0;
		hidelive	=	(hidelive) ? 1:0;
		nodiscount	=	(nodiscount) ? 1:0;
	
		updateURL	=					"itemid=" + itemid + "&partnumber=" + partnumber + "&prev_partnumber=" + prev_partnumber;
		updateURL	=	updateURL	+	"&itemweight=" + itemweight + "&cogs=" + cogs + "&vendor=" + vendor;
		updateURL	=	updateURL	+	"&seasonal=" + seasonal + "&ghost=" + ghost + "&hidelive=" + hidelive + "&nodiscount=" + nodiscount;
		updateURL	=	updateURL	+	"&price_our=" + price_our + "&price_ca=" + price_ca + "&price_co=" + price_co + "&price_ps=" + price_ps;
		updateURL	=	updateURL	+	"&price_er=" + price_er + "&flag=" + flag;
		updateURL	=	"/ajax/admin/adjustInv.asp?" + updateURL;
	
		document.getElementById('id_div_update_' + rowid).innerHTML = "<img src='/images/preloading.gif' border='0'>";
		ajax(updateURL,'dumpZone');

		setTimeout('checkUpdate(' + rowid + ')', 1000);
	}
	else
	{
		alert("please wait until page is fully loaded..");
	}
}

function checkUpdate(rowid)
{
	if ("UPDATED" != document.getElementById('dumpZone').innerHTML) 
	{
		setTimeout('checkUpdate(' + rowid + ')', 1000);
	}
	else 	
	{
		clearTimeout();
		document.getElementById('id_div_update_' + rowid).innerHTML = "<font color='blue'><b>UPDATED</b></font>";		
		document.getElementById('dumpZone').innerHTML = "";
		setTimeout('setUpdateBtn(' + rowid + ')', 1500);
	}
}

function setUpdateBtn(rowid)
{
	var	el	=	'<input type="button" id="id_update_' + rowid + '" name="btn_update" value="Update" onclick="onItemUpdate(\'' + rowid + '\');">';
	document.getElementById('id_div_update_' + rowid).innerHTML = el;	
}

window.onload = function()
{
	isPageLoaded = true;
}
</script>

<%
dim modelList : modelList = "##"
dim itemIdList : itemIdList = ""
if request.form("submitted") = "Update" then
%>
<div style="width:800px; margin:0px auto;">
<%
	dim hideLive, NoDiscount, Seasonal, ghost, itemWeight, inv_qty, COGS
	dim price_our, price_ca, price_co, price_ps, price_er, flag1
	
'	call addTracking(gblTrackingID, 0, "UPDATE START(totalCount:" & request.form("totalCount") & ")")	
	for a = 0 to request.form("totalCount") - 1
		if request.form("hideLive" & a) = "on" then
			hideLive = 1
		else
			hideLive = 0
		end if
		if request.form("NoDiscount" & a) = "on" then
			NoDiscount = 1
		else
			NoDiscount = 0
		end if
		if request.form("Seasonal" & a) = "on" then
			Seasonal = 1
		else
			Seasonal = 0
		end if
		if request.form("ghost" & a) = "on" then
			ghost = 1
		else
			ghost = 0
		end if
		if isNumeric(request.form("itemWeight" & a)) then
			itemWeight = request.form("itemWeight" & a)
		else
			itemWeight = "null"
		end if
		if isNumeric(request.form("COGS" & a)) then
			COGS = cDbl(request.form("COGS" & a))
		else
			COGS = 0
		end if

		if isNumeric(request.form("Flag" & a)) then
			flag1 = cdbl(request.form("Flag" & a))
		else
			flag1 = cdbl(request.form("itemID" & a)) + 1
		end if
		if isNumeric(request.form("price_our" & a)) then
			price_our = request.form("price_our" & a)
		else
			price_our = "null"
		end if
		if isNumeric(request.form("price_ca" & a)) then
			price_ca = request.form("price_ca" & a)
		else
			price_ca = "null"
		end if
		if isNumeric(request.form("price_co" & a)) then
			price_co = request.form("price_co" & a)
		else
			price_co = "null"
		end if
		if isNumeric(request.form("price_ps" & a)) then
			price_ps = request.form("price_ps" & a)
		else
			price_ps = "null"
		end if
		if isNumeric(request.form("price_er" & a)) then
			price_er = request.form("price_er" & a)
		else
			price_er = "null"
		end if										
		
		if trim(request.form("PartNumber" & a)) <> "" then
			SQL = "UPDATE we_items SET"
			SQL = SQL & " PartNumber='" & request.form("PartNumber" & a) & "', "
			SQL = SQL & " itemWeight='" & itemWeight & "', "
			SQL = SQL & " COGS='" & COGS & "', "
			SQL = SQL & " Vendor='" & request.form("Vendor" & a) & "', "
			SQL = SQL & " NoDiscount=" & NoDiscount & ", "
			SQL = SQL & " price_our = " & price_our & ", "
			SQL = SQL & " price_ca = " & price_ca & ", "
			SQL = SQL & " price_co = " & price_co & ", "
			SQL = SQL & " price_ps = " & price_ps & ", "
			SQL = SQL & " price_er = " & price_er & ", "
			SQL = SQL & " PriceLastUpdatedBy = " & adminID & ", "
			SQL = SQL & " PriceLastUpdatedDate = '" & now & "' "
			SQL = SQL & " WHERE PartNumber = '" & request.form("PartNumber" & a) & "'"
			'response.write "<p>" & SQL & "</p>"
			response.Write(". ")
			oConn.execute SQL
			
			SQL = "UPDATE we_items SET"
			SQL = SQL & " Seasonal=" & Seasonal & ", "
			SQL = SQL & " ghost=" & ghost & ", "
			SQL = SQL & " hideLive=" & hideLive & ", "
			SQL = SQL & " flag1 = '" & flag1 & "'"
			SQL = SQL & " WHERE itemID = " & request.form("itemID" & a)
			'response.write "<p>" & SQL & "</p>"
			oConn.execute SQL
			
			sql = "insert into we_adminActions (adminID,action) values (" & adminID & ",'db_update_inv.asp for: " & request.form("PartNumber" & a) & "')"
			session("errorSQL")
			oConn.execute(sql)
			
			itemIdList = itemIdList & request.form("itemID" & a) & ","

'			sql = "select top 1 brandid, modelid, typeid, subtypeid, itemid from we_items where itemID = '" & request.form("itemID" & a) & "'"
'			set tempRS = oConn.execute(sql)
'			if not tempRS.eof then
'				tBrandID = tempRS("brandid")
'				tModelID = tempRS("modelid")
'				tTypeID = tempRS("typeid")
'				tSubTypeID = tempRS("subtypeid")
'				tItemID = tempRS("itemid")
'	
'				autoDeleteCompPages "updateItem",tBrandID & "##" & tModelID & "##" & tTypeID & "##" & tItemID
'				autoDeleteCompPages "updateItem",tBrandID & "##" & tModelID & "##" & tSubTypeID & "##" & itemID
'				if instr(modelList,"##" & tModelID & "##") < 1 then modelList = modelList & tModelID & "##"
'			end if
		end if
	next
	itemIdList = left(itemIdList,len(itemIdList)-1)
	sql = "select distinct modelID from we_Items where itemID in (" & itemIdList & ")"
	set modelsRS = oConn.execute(sql)
	do while not modelsRS.EOF
		updateModelID = modelsRS("modelID")
		sql = "exec sp_createProductListByModelID " & updateModelID
		oConn.execute(sql)
		response.Write("<br>Updateing Products For ModelID: " & updateModelID)
		modelsRS.movenext
	loop
	sql = "update we_compressedPages set lastUpdate = '" & date-1 & "'"
	session("errorSQL") = sql
	oConn.execute(sql)
'	call addTracking(gblTrackingID, 0, "UPDATE END")		
	%>
</div>
	<h3>UPDATED!</h3>
	<%
else
'	response.write request.form
	SQL = 	"select	vendor, itemid, inv_qty, itempic, itemdesc, partnumber, itemweight" & vbcrlf & _
			"	,	cogs, seasonal, ghost, hidelive, nodiscount, price_our, price_ca, price_co, price_ps" & vbcrlf & _
			"	,	price_er, flag1, master " & vbcrlf & _
			"from	we_items a	" & vbcrlf & _
			"where	1=1" & vbcrlf
			
	keywords 		= 	SQLquote(request.form("keywords"))
	ItemNumber 		= 	request.form("ItemNumber")
	PartNumber 		= 	request("PartNumber")
	showUpdateBtn	=	false
	showMobileLine	=	request.form("chkMobileLine")
	showDecal		=	request.form("chkDecal")
	showSlaves		=	request.form("showSlaves")
	showHidden		=	request.Form("showHidden")
	
	if showHidden = "on" then showHidden = 1 else showHidden = 0
	
	if isnull(showHidden) or len(showHidden) < 1 then showHidden = 0
	
	showUpdateBtn = true

	if showSlaves <> "on" then
		SQL = SQL & " AND A.inv_qty > -1" & vbcrlf
	end if
	
	if request.form("BrandID") = "ALL" and request.form("TypeID") = "8" then
		SQL = SQL & " AND A.TypeID='8'"
	else
		if keywords = "" then
			if ItemNumber = "" then
				if PartNumber = "" then
					if request.form("BrandID") <> "" then SQL = SQL & " AND A.BrandID='" & request.form("BrandID") & "'"
					if request.form("TypeID") <> "" then SQL = SQL & " AND A.TypeID='" & request.form("TypeID") & "'"
					if request.form("ModelID") <> "" then SQL = SQL & " AND A.ModelID='" & request.form("ModelID") & "'"
				else
					SQL = SQL & " AND A.PartNumber LIKE '%" & replace(PartNumber,"%","") & "%'"
				end if
			else
				SQL = SQL & " AND A.itemID='" & ItemNumber & "'"
			end if
		else
			SQL = SQL & " AND A.itemDesc LIKE '%" & keywords & "%'"
		end if
	end if
	if showHidden = 0 then
		SQL = SQL & " AND A.hideLive = 0"
	else
		SQL = replace(SQL,"AND A.inv_qty > -1","AND (A.inv_qty > -1 or A.hideLive = 1)")
	end if
	SQL = SQL & " ORDER BY A.itemDesc"
	session("errorSQL") = SQL
'	call addTracking(gblTrackingID, 0, "QUERY START:" & SQL)

	'vendor, itemid, inv_qty, itempic, itemdesc, partnumber, itemweight, cogs, seasonal, ghost, hidelive
	'nodiscount, price_our, price_ca, price_co, price_ps, price_er, flag1	
	dim arrItem : arrItem = getDbRows(SQL)
	dim nTotalRow

	if isnull(arrItem) then
'		call addTracking(gblTrackingID, 0, "QUERY END(recordcount:0):" & SQL)	
%>
		<form name="frmOptions" action="db_update_inv.asp" method="post">
			<input type="checkbox" name="showSlaves" onClick="this.form.submit();" <%if showSlaves = "on" then response.write " checked" end if%>>&nbsp;Show Slaves
            <input type="checkbox" name="showHidden" onClick="this.form.submit();" <%if showHidden = 1 then response.write " checked" end if%>>&nbsp;Show Hidden
		<%			
		for each thing in request.form
			if thing <> "showSlaves" and thing <> "chkMobileLine" and thing <> "chkDecal" then 
				response.write "<input type=""hidden"" name=""" & thing & """ value=""" & request.form(thing) & """>" & vbcrlf
			end if
		next
		%>
			<input type="submit" style="visibility:hidden; display:none">
		</form>				
<%		
	    response.write "<h3>No records matched</h3>"
	else
		nTotalRow = ubound(arrItem,2) + 1
'		call addTracking(gblTrackingID, 0, "QUERY END(recordcount:" & nTotalRow & "):" & SQL)				
		
		dim numMobile 	: 	numMobile 	= numberOfItem(arrItem, 0, "MLD", 0)
		dim numDecal 	: 	numDecal 	= numberOfItem(arrItem, 5, "DEC", 3)
	%>
		<font face="Arial,Helvetica"><h3><%=nTotalRow%> items total</h3></font>	
		<form name="frmOptions" action="db_update_inv.asp" method="post">
			<input type="checkbox" name="showSlaves" onClick="this.form.submit();" <%if showSlaves = "on" then response.write " checked" end if%>>&nbsp;Show Slaves
            <input type="checkbox" name="showHidden" onClick="this.form.submit();" <%if showHidden = 1 then response.write " checked" end if%>>&nbsp;Show Hidden
		<%
		if numMobile > 0 then
		%>
			<input type="checkbox" name="chkMobileLine" onClick="this.form.submit();" <%if (showMobileLine = "on") or (nTotalRow = numMobile) then response.write " checked" end if%>>&nbsp;Show Mobile Line (<%=numMobile%> items)
		<%
		end if
		
		if numDecal > 0 then
		%>	
			<input type="checkbox" name="chkDecal" onClick="this.form.submit();" <%if (showDecal = "on") or (nTotalRow = numDecal) then response.write " checked" end if%>>&nbsp;Show Decal Skins (<%=numDecal%> items)
		<%
		end if 
		
		for each thing in request.form
			if thing <> "showHidden" and thing <> "showSlaves" and thing <> "chkMobileLine" and thing <> "chkDecal" then 
				response.write "<input type=""hidden"" name=""" & thing & """ value=""" & request.form(thing) & """>" & vbcrlf
			end if
		next
		%>
			<input type="submit" style="visibility:hidden; display:none">
		</form>	
		<form name="frmEditItem" action="db_update_inv.asp" method="post">
		<%if showUpdateBtn then%>
			<input type="submit" name="btn_update" value="Update All">
			<input type="hidden" name="submitted" value="Update" />
		<%end if%>
			<table cellpadding="3" cellspacing="0" width="99%">
				<tr class="grid_header">
					<td align="center" width="10" class="grid_header_td"><b>Item#</b></td>
					<td align="center" width="620" class="grid_header_td"><b>Description</b></td>
					<td align="center" width="130" class="grid_header_td"><b>Part&nbsp;Number</b></td>
					<td align="center" width="40" class="grid_header_td"><b>QTY</b></td>
					<td align="center" width="40" class="grid_header_td"><b>Weight</b></td>
					<td align="center" width="40" class="grid_header_td"><b>COGS</b></td>
					<td align="center" width="40" class="grid_header_td"><b>Vendor</b></td>
					<td align="center" width="40" class="grid_header_td"><b>Seas.</b></td>
                    <td align="center" width="40" class="grid_header_td"><b>Ghost</b></td>
					<td align="center" width="40" class="grid_header_td"><b>Hide</b></td>
					<td align="center" width="60" class="grid_header_td"><b>NoDisc.</b></td>
					<td align="center" width="50" class="grid_header_td"><b>WE Pricing</b></td>
					<td align="center" width="50" class="grid_header_td"><b>CA Pricing</b></td>
					<td align="center" width="50" class="grid_header_td"><b>CO Pricing</b></td>
					<td align="center" width="50" class="grid_header_td"><b>PS Pricing</b></td>
					<td align="center" width="50" class="grid_header_td"><b>ER Pricing</b></td>
					<td align="center" width="50" class="grid_header_td"><b>flag</b></td>
				<%if showUpdateBtn then%>											
					<td align="center" width="60" class="grid_header_td"><b>Action</b></td>
				<%end if%>					
				</tr>
				<%
'				call addTracking(gblTrackingID, 0, "LOOP START")
				prodLap = 0
				for nRow=0 to ubound(arrItem,2)
					do
						if not isNull(arrItem(5,nRow)) and not isEmpty(arrItem(5,nRow)) and arrItem(5,nRow) <> "" then
							PartNumber = arrItem(5,nRow)
						else
							PartNumber = ""
						end if
	
						vendor		=	arrItem(0,nRow)					
						itemid 		= 	arrItem(1,nRow)
						inv_qty		=	arrItem(2,nRow)
						itempic		=	arrItem(3,nRow)
						itemdesc	=	arrItem(4,nRow)			
						itemweight	=	arrItem(6,nRow)
						cogs		=	arrItem(7,nRow)
						seasonal	=	arrItem(8,nRow)
						ghost		=	arrItem(9,nRow)
						hidelive	=	arrItem(10,nRow)					
						nodiscount	=	arrItem(11,nRow)
						price_our	=	arrItem(12,nRow)
						price_ca	=	arrItem(13,nRow)
						price_co	=	arrItem(14,nRow)
						price_ps	=	arrItem(15,nRow)
						price_er	=	arrItem(16,nRow)
						flag1		=	arrItem(17,nRow)
						master		=	arrItem(18,nRow)

						if 	nTotalRow <> numMobile then
							if "on" <> showMobileLine then 
								if "MLD" = vendor then
									exit do
								end if
							end if
						end if
						
						if 	nTotalRow <> numDecal then
							if "on" <> showDecal then 
								if "DEC" = left(partnumber, 3) then
									exit do
								end if
							end if
						end if	

						%>
						<tr class="grid_row_<%=(nRow mod 2)%>" style="font-size:12px;">
							<td align="center">
								<a href="/admin/db_update_items.asp?ItemID=<%=itemid%>"><%=itemid%></a>
								<input type="hidden" id="id_itemID_<%=nRow%>" name="itemID<%=nRow%>" value="<%=itemid%>">
							</td>
                            <% if ghost then %>
                            <td align="center"><a href="javascript:faceplateDetail('<%=itempic%>');" style="color:#666;"><%=itemdesc%> (GHOST)</a></td>
							<% else %>
							<td align="center"><a href="javascript:faceplateDetail('<%=itempic%>');"><%=itemdesc%></a></td>
                            <% end if %>
                            <% if master then %>
							<td align="center">
                            	<input type="text" id="id_PartNumber_<%=nRow%>" name="PartNumber<%=nRow%>" size="16" value="<%=PartNumber%>" maxlength="17">
                                <input type="hidden" id="prev_id_PartNumber_<%=nRow%>" name="prev_PartNumber<%=nRow%>" value="<%=PartNumber%>">
                            </td>
							<td align="center"><%=inv_qty%></td>
							<td align="center"><input type="text" id="id_itemWeight_<%=nRow%>" name="itemWeight<%=nRow%>" size="2" value="<%=itemweight%>" maxlength="3"></td>
							<td align="center"><input type="text" id="id_COGS_<%=nRow%>" name="COGS<%=nRow%>" size="3" value="<%=cogs%>" maxlength="6"></td>
							<td align="center"><input type="text" id="id_Vendor_<%=nRow%>" name="Vendor<%=nRow%>" size="3" value="<%=vendor%>" maxlength="11"></td>
							<td align="center"><input type="checkbox" id="id_Seasonal_<%=nRow%>" name="Seasonal<%=nRow%>" <%if seasonal then response.write " checked"%>></td>
                            <td align="center"><input type="checkbox" id="id_ghost_<%=nRow%>" name="ghost<%=nRow%>" <%if ghost then response.write " checked"%>></td>
							<td align="center"><input type="checkbox" id="id_hideLive_<%=nRow%>" name="hideLive<%=nRow%>" <%if hidelive then response.write " checked"%>></td>
							<td align="center"><input type="checkbox" id="id_NoDiscount_<%=nRow%>" name="NoDiscount<%=nRow%>" <%if nodiscount then response.write " checked"%>></td>
							<td align="center"><input type="text" id="id_price_our_<%=nRow%>" name="price_our<%=nRow%>" size="2" value="<%=price_our%>"></td>
							<td align="center"><input type="text" id="id_price_ca_<%=nRow%>" name="price_ca<%=nRow%>" size="2" value="<%=price_ca%>"></td>
							<td align="center"><input type="text" id="id_price_co_<%=nRow%>" name="price_co<%=nRow%>" size="2" value="<%=price_co%>"></td>
							<td align="center"><input type="text" id="id_price_ps_<%=nRow%>" name="price_ps<%=nRow%>" size="2" value="<%=price_ps%>"></td>
							<td align="center"><input type="text" id="id_price_er_<%=nRow%>" name="price_er<%=nRow%>" size="2" value="<%=price_er%>"></td>
							<td align="center"><input type="text" id="id_Flag_<%=nRow%>" name="Flag<%=nRow%>" size="2" value="<%=flag1%>"></td>
								<%if showUpdateBtn then%>						
								<td align="left" id="id_td_update_<%=nRow%>">
									<div id="id_div_update_<%=nRow%>" style="font-size:11px;" align="center">
									<input type="button" id="id_update_<%=nRow%>" name="btn_update" value="Update" onclick="onItemUpdate('<%=nRow%>');">
									<!--<input type="submit" id="id_update_<%=nRow%>" name="btn_update" value="Update">								
									<input type="hidden" name="submitted" value="Update" />-->
									</div>								
								</td>
								<%end if%>
                            <% else %>
                            <td align="center">
								<%=PartNumber%>
                                <input type="hidden" id="id_PartNumber_<%=nRow%>" name="PartNumber<%=nRow%>" value="<%=PartNumber%>">
                                <input type="hidden" id="prev_id_PartNumber_<%=nRow%>" name="prev_PartNumber<%=nRow%>" value="<%=PartNumber%>">
                            </td>
							<td align="center"><%=inv_qty%></td>
							<td align="center">
								<%=itemweight%>
                                <input type="hidden" id="id_itemWeight_<%=nRow%>" name="itemWeight<%=nRow%>" value="<%=itemweight%>">
                            </td>
							<td align="center">
								<%=cogs%>
                                <input type="hidden" id="id_COGS_<%=nRow%>" name="COGS<%=nRow%>" value="<%=cogs%>">
                            </td>
							<td align="center">
								<%=vendor%>
                                <input type="hidden" id="id_Vendor_<%=nRow%>" name="Vendor<%=nRow%>" value="<%=vendor%>">
                            </td>
							<td align="center"><input type="checkbox" id="id_Seasonal_<%=nRow%>" name="Seasonal<%=nRow%>" <%if seasonal then response.write " checked"%>></td>
                            <td align="center"><input type="checkbox" id="id_ghost_<%=nRow%>" name="ghost<%=nRow%>" <%if ghost then response.write " checked"%>></td>
							<td align="center"><input type="checkbox" id="id_hideLive_<%=nRow%>" name="hideLive<%=nRow%>" <%if hidelive then response.write " checked"%>></td>
							<td align="center"><input type="checkbox" id="id_NoDiscount_<%=nRow%>" name="NoDiscount<%=nRow%>" <%if nodiscount then response.write " checked"%>></td>
							<td align="center">
								<%=price_our%>
                                <input type="hidden" id="id_price_our_<%=nRow%>" name="price_our<%=nRow%>" value="<%=price_our%>">
                            </td>
							<td align="center">
								<%=price_ca%>
                                <input type="hidden" id="id_price_ca_<%=nRow%>" name="price_ca<%=nRow%>" value="<%=price_ca%>">
                            </td>
							<td align="center">
								<%=price_co%>
                                <input type="hidden" id="id_price_co_<%=nRow%>" name="price_co<%=nRow%>" value="<%=price_co%>">
                            </td>
							<td align="center">
								<%=price_ps%>
                                <input type="hidden" id="id_price_ps_<%=nRow%>" name="price_ps<%=nRow%>" value="<%=price_ps%>">
                            </td>
							<td align="center">
								<%=price_er%>
                                <input type="hidden" id="id_price_er_<%=nRow%>" name="price_er<%=nRow%>" value="<%=price_er%>">
                            </td>
							<td align="center">
								<input type="text" id="id_Flag_<%=nRow%>" name="Flag<%=nRow%>" size="3" value="<%=flag1%>">
                            </td>
                            	<%if showUpdateBtn then%>						
								<td align="left" id="id_td_update_<%=nRow%>">
									<div id="id_div_update_<%=nRow%>" style="font-size:11px;" align="center">
									<input type="button" id="id_update_<%=nRow%>" name="btn_update" value="Update" onclick="onItemUpdate('<%=nRow%>');">
									<!--<input type="submit" id="id_update_<%=nRow%>" name="btn_update" value="Update">								
									<input type="hidden" name="submitted" value="Update" />-->
									</div>								
								</td>
								<%end if%>
                            <% end if %>
						</tr>
					<%
					loop while false
					prodLap = prodLap + 1
				next
'				call addTracking(gblTrackingID, 0, "LOOP END")
				%>
			</table>
				<%if showUpdateBtn then%>
				<input type="submit" name="btn_update" value="Update All">
                <input type="hidden" name="totalCount" value="<%=prodLap%>" />
				<%end if%>
			</form>
<%
	end if
end if

function numberOfItem(byref arr, idx, val, nLeft)
	dim nCnt : nCnt = 0
	
	for i=0 to ubound(arr,2)
		if nLeft > 0 then
			if not isnull(arr(idx,i)) then		
				if cstr(val) = left(cstr(arr(idx,i)), nLeft) then
					nCnt = nCnt + 1
				end if				
			end if	
		else
			if not isnull(arr(idx,i)) then
				if cstr(val) = cstr(arr(idx,i)) then
					nCnt = nCnt + 1
				end if		
			end if
		end if
	next
	
	numberOfItem = cdbl(nCnt)
end function
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->

<script language="javascript">
function faceplateDetail(itemPic) {
	var url="faceplates_view.asp?itemPic="+itemPic;
	window.open(url,"invoice","left=0,top=0,width=450,height=450,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
</script>