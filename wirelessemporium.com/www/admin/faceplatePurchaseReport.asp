<%
	thisUser = Request.Cookies("username")
	pageTitle = "Faceplate Purchase Report"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<style type="text/css">
	.vertTxt {
		color:#333;
		writing-mode:tb-rl;
		-webkit-transform:rotate(90deg);
		-moz-transform:rotate(90deg);
		-o-transform: rotate(90deg);
		font-size:9px;
	}
	.prodLinkOption 		{ color:#00F; text-decoration:underline; cursor:pointer; font-size:10px; }
	.prodLinkOption:hover 	{ color:#00F; text-decoration:underline; cursor:pointer; font-size:10px; }
	
	.fl 					{ float:left; }
	.fr 					{ float:right; }
	.tb 					{ display:table; }
	.pb5 					{ padding-bottom:5px; }
	.ft14px 				{ font-size:14px; }
	.ft8px 					{ font-size:8px; }
	
	.numCol 				{ width:50px; border-right:1px solid #000; height:60px; }	
	
	.itemCol 				{ width:445px; font-size:10px; border-right:1px solid #000; padding-left:10px; height:60px; }
	.itemTableRow 			{ width:100%; cursor:pointer; }
	.onOrder 				{ padding-left:10px; color:#fff; }
	.lastPurch 				{ padding-left:10px; color:#F00; }
	.loadingBox 			{ width:100%; border-top:1px solid #000; margin-top:40px; }
	
	.soldCol 				{ width:35px; border-right:1px solid #000; padding-right:5px; height:60px; }
	.invCol 				{ width:35px; padding-right:5px; height:60px; }
	.topNum 				{ width:30px; text-align:right; }
	.exSalesData 			{ font-size:10px; padding:3px 0px 0px 3px; }
	.sellThrough			{ width:30px; border:1px solid #F00; padding:1px; font-weight:bold; text-align:center; }
</style>
<%
	session("vendorCode") = ""
	session("sbInv") = null
	session("sbSold") = null
	
	dim fprTypeID : fprTypeID = prepInt(request.QueryString("fprTypeID"))
	dim vendorCode : vendorCode = prepStr(request.QueryString("vendorCode"))
	dim showHidden : showHidden = prepInt(request.QueryString("showHidden"))
	dim partNumber : partNumber = prepStr(request.QueryString("partNumber"))
	dim invDays : invDays = prepInt(request.QueryString("invDays"))
	
	if vendorCode <> "" then session("vendorCode") = vendorCode else session("vendorCode") = ""
	if fprTypeID > 0 then session("fprTypeID") = fprTypeID else	session("fprTypeID") = 0
	if showHidden > 0 then session("showHidden") = showHidden else session("showHidden") = 0
	if prepInt(session("fprDays")) = 0 then session("fprDays") = 30
	
	sql = "select name, code from we_vendors order by name"
	session("errorSQL") = sql
	set vendorRS = oConn.execute(sql)
	
	sql = "select typeID, typeName from we_types order by typeName"
	session("errorSQL") = sql
	set catRS = oConn.execute(sql)
	
	sql = "select brandID, brandName from we_brands order by brandName"
	session("errorSQL") = sql
	set brandRS = oConn.execute(sql)
%>
<div style="position:relative;">
<table border="0" cellpadding="3" cellspacing="0" width="600" align="center">
    <tr>
    	<td align="center">
        	<div style="float:left; width:600px; height:20px; margin-bottom:10px;">
            	<div style="float:left; width:190px; text-align:right; padding-right:10px; font-weight:bold;">Partnumber:</div>
                <div style="float:left; width:400px; text-align:left;"><input type="text" id="frm_partnumber" name="partnumber" value="" /></div>
            </div>
            <div style="float:left; width:600px; height:60px; margin-bottom:10px;">
            	<div style="float:left; width:190px; text-align:right; padding-right:10px; font-weight:bold;">Partnumber List:</div>
                <div style="float:left; width:400px; text-align:left;"><textarea id="frm_partnumberList" name="partnumberList"></textarea></div>
            </div>
            <div style="float:left; width:600px; height:20px; margin-bottom:10px;">
            	<div style="float:left; width:190px; text-align:right; padding-right:10px; font-weight:bold;">Keyword(s):</div>
                <div style="float:left; width:400px; text-align:left;"><input type="text" id="frm_keywords" name="keywords" value="" /></div>
            </div>
            <div style="float:left; width:600px; height:20px; margin-bottom:10px;">
                <div style="float:left; width:190px; text-align:right; padding-right:10px; font-weight:bold;">Inventory Days:</div>
                <div style="float:left; width:400px; text-align:left;"><input type="text" id="invDaysTop" value="<%=invDays%>" size="3" /></div>
            </div>
            <div style="float:left; width:600px; padding-bottom:10px; margin-bottom:10px; border-bottom:1px solid #000;"><input type="button" name="myAction" value="Search" onclick="quickSearch()" /></div>
            <% if vendorCode <> "" then %>
            <div style="float:left; width:600px; padding-bottom:10px; margin-bottom:10px; border-bottom:1px solid #000;">
            	<div class="tb">
	                <div style="float:left; width:190px; text-align:right; padding-right:10px; font-weight:bold;">All Order:</div>
    	            <div style="float:left; padding-right:10px;"><input type="text" id="allOrderAmt" value="10" size="3" /></div>
        	        <div style="float:left;"><input type="button" name="myAction" value="Add To Order" onclick="allAddToOrder()" /></div>
                </div>
            </div>
            <% end if %>
            <div style="float:left; width:600px; height:20px; margin-bottom:10px;">
            	<div style="float:left; width:190px; text-align:right; padding-right:10px; font-weight:bold;">Hidden Product:</div>
                <div style="float:left; width:400px; text-align:left;">
                	<select name="showHidden" onchange="window.location = '/admin/faceplatePurchaseReport.asp?vendorCode=<%=session("vendorCode")%>&fprTypeID=<%=fprTypeID%>&showHidden=' + this.value">
                    	<option value="0">Hide</option>
                        <option value="1"<% if showHidden = 1 then %> selected="selected"<% end if %>>Show</option>
                    </select>
                </div>
            </div>
            <div style="float:left; width:600px; height:20px; margin-bottom:10px;">
            	<div style="float:left; width:190px; text-align:right; padding-right:10px; font-weight:bold;">Select Vendor:</div>
                <div style="float:left; width:400px; text-align:left;">
                	<select id="vendorSelect" name="vendorCode" onchange="window.location = '/admin/faceplatePurchaseReport.asp?vendorCode=' + this.value + '&invDays=' + document.getElementById('invDays').value">
                    	<option value="">All Vendors</option>
                        <%
						do while not vendorRS.EOF
						%>
                        <option value="<%=vendorRS("code")%>"<% if prepStr(vendorRS("code")) = prepStr(session("vendorCode")) then %> selected="selected"<% end if %>><%=vendorRS("name")%></option>
                        <%
							vendorRS.movenext
						loop
						%>
                    </select>
                </div>
            </div>
            <div style="float:left; width:600px; height:20px; margin-bottom:10px;">
            	<div style="float:left; width:190px; text-align:right; padding-right:10px; font-weight:bold;">Inventory Days:</div>
                <div style="float:left; width:400px; text-align:left;"><input type="text" id="invDays" name="invDays" size="3" value="<%=invDays%>" onchange="window.location = '/admin/faceplatePurchaseReport.asp?vendorCode=' + document.getElementById('vendorSelect').value + '&fprTypeID=' + document.getElementById('categorySelect').value + '&invDays=' + this.value" /></div>
            </div>
            <div style="float:left; width:600px; height:20px; margin-bottom:10px;">
            	<div style="float:left; width:190px; text-align:right; padding-right:10px; font-weight:bold;">Select Category:</div>
                <div style="float:left; width:400px; text-align:left;">
                	<select id="categorySelect" name="catID" onchange="window.location = '/admin/faceplatePurchaseReport.asp?vendorCode=<%=session("vendorCode")%>&fprTypeID=' + this.value + '&invDays=' + document.getElementById('invDays').value">
                    	<option value="">- Select Category -</option>
                        <%
						do while not catRS.EOF
						%>
                        <option value="<%=catRS("typeID")%>"<% if prepInt(catRS("typeID")) = prepInt(session("fprTypeID")) then %> selected="selected"<% end if %>><%=catRS("typeName")%></option>
                        <%
							catRS.movenext
						loop
						%>
                    </select>
                </div>
            </div>
            <div style="float:left; width:600px; height:30px; margin-bottom:10px;">
            	<div style="float:left; width:190px; text-align:right; padding-right:10px; font-weight:bold;">Select Brand:</div>
                <form name="brandSelect" onsubmit="return(false)">
                <div style="float:left; width:400px; text-align:left;">
                	<select name="brandID" onchange="selectBrand(this.value)">
                    	<option value="">- Select Brand -</option>
                        <%
						do while not brandRS.EOF
						%>
                        <option value="<%=brandRS("brandID")%>"><%=brandRS("brandName")%></option>
                        <%
							brandRS.movenext
						loop
						%>
                    </select>
                </div>
                </form>
            </div>
            <div style="float:left; width:600px; height:30px; margin-bottom:10px;"><input type="button" name="myAction" value="Reset Form" onclick="window.location='/admin/faceplatePurchaseReport.asp'" /></div>
            <div style="float:left; width:600px; height:120px;" id="modelSelection"></div>
        </td>
    </tr>
    <tr><td id="results" align="center"></td></tr>
</table>
	<div style="position:fixed; bottom:0px; right:0px; width:285px; text-align:right; overflow:auto;" id="floatingCart"></div>
</div>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	<% if prepStr(request.QueryString("vendorCode")) <> "" then %>
	document.getElementById("results").innerHTML = "<div style='margin-top:200px; font-size:24px; font-weight:bold;'>Loading Data<br>Please Wait...</div>"
	ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullVendorCode=<%=vendorCode%>&invDays=<%=invDays%>&fprTypeID=<%=fprTypeID%>','results')
	<% elseif fprTypeID > 0 then %>
	document.getElementById("results").innerHTML = "<div style='margin-top:200px; font-size:24px; font-weight:bold;'>Loading Data<br>Please Wait...</div>"
	ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullVendorCode=<%=vendorCode%>&invDays=<%=invDays%>&fprTypeID=<%=fprTypeID%>&fullCatID=<%=fprTypeID%>','results')
	<% elseif partNumber <> "" then %>
	document.getElementById("results").innerHTML = "<div style='margin-top:200px; font-size:24px; font-weight:bold;'>Loading Data<br>Please Wait...</div>"
	ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?soloPartNumber=<%=partNumber%>','results')
	<% end if %>
	
	function pageItemList(vendorCode,typeID,blockID,sortBy,listDirection) {
		document.getElementById("results").innerHTML = "<div style='margin-top:200px; font-size:24px; font-weight:bold;'>Loading Data<br>Please Wait...</div>"
		ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullVendorCode=' + vendorCode + '&invDays=<%=invDays%>&fprTypeID=' + typeID + '&blockID=' + blockID + '&sb=' + sortBy + '&listDirection=' + listDirection,'results')
	}
	
	function selectBrand(brandID) {
		document.getElementById("results").innerHTML = "<div style='margin-top:200px; font-size:24px; font-weight:bold;'>Loading Data<br>Please Wait...</div>"
		if (brandID == "") {
			document.getElementById("modelSelection").innerHTML = ""
		}
		else {
			ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?brandID=' + brandID,'modelSelection')
		}
		ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullVendorCode=<%=vendorCode%>&fprTypeID=<%=fprTypeID%>&fullBrandID=' + brandID,'results')
	}
	
	function prepData() {
		document.getElementById("results").innerHTML = "<div style='margin-top:200px; font-size:24px; font-weight:bold;'>Loading Data<br>Please Wait...</div>"
	}
	
	function expandData(itemID,usePN) {
		if (document.getElementById("item_" + itemID).style.display == '') {
			document.getElementById("item_" + itemID).style.display = 'none'
			document.getElementById("item_" + itemID).innerHTML = ''
		}
		else {
			var sellThrough = Math.round(parseFloat(document.getElementById("runRate_" + itemID).innerHTML));
			var curInventory = parseInt(document.getElementById("curInv_" + itemID).innerHTML);
			var setPurchAmt = sellThrough - curInventory;
			ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?itemDetails=' + itemID + '&usePN=' + usePN + '&setPurchAmt=' + setPurchAmt,'item_' + itemID)
			document.getElementById("item_" + itemID).style.display = ''
		}
	}
	
	function addToOrder(voCode,voPartNumber,voWePartNumber,voQty,itemID,cogs) {
		ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?voCode=' + voCode + '&voPartNumber=' + voPartNumber + '&voWePartNumber=' + voWePartNumber + '&voQty=' + voQty + '&voCogs=' + cogs,'dumpZone')
		ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?itemDetails=' + itemID,'item_' + itemID)
		return false
	}
	
	function updateCogs(voCode,voPartNumber,voWePartNumber,itemID,cogs) {
		ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?voCode=' + voCode + '&voPartNumber=' + voPartNumber + '&voWePartNumber=' + voWePartNumber + '&voCogs=' + cogs + '&justCogs=1','cogs_' + itemID)
		setTimeout("ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?itemDetails=" + itemID + "','item_" + itemID + "')",1000)
		return false
	}
	
	function selectModel(modelName,brandID) {
		for (i=0;i<document.optionForm.modelID.length;i++) {
			if (document.optionForm.modelID.options[i].text == modelName) {
				document.optionForm.modelID.selectedIndex = i
				prepData()
				ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?fullBrandID=' + document.brandSelect.brandID.options[document.brandSelect.brandID.selectedIndex].value + '&modelID=' + document.optionForm.modelID.options[document.optionForm.modelID.selectedIndex].value + '&fpType=' + document.optionForm.fpType.options[document.optionForm.fpType.selectedIndex].value + '&dsType=' + document.optionForm.dsType.options[document.optionForm.dsType.selectedIndex].value,'results')
			}
		}
	}
	
	function showNewVendor(itemID) {
		ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?newVendor=' + itemID,'newVendor_' + itemID)
	}
	
	function updateVendorPN(vendor,vpn,cogs,itemID) {
		ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?newVendor=' + itemID + '&updVendor=' + vendor + '&updVPN=' + vpn + '&updCogs=' + cogs,'newVendor_' + itemID)
	}
	
	function addVendor(itemID) {
		var vendorCode = eval("document.newVendorForm_" + itemID + ".vendorCode.value")
		var partNumber = eval("document.newVendorForm_" + itemID + ".partNumber.value")
		var cogs = eval("document.newVendorForm_" + itemID + ".cogs.value")
		var we_partNumber = eval("document.newVendorForm_" + itemID + ".we_partNumber.value")
		var ogVend_partNumber = eval("document.newVendorForm_" + itemID + ".ogVend_partNumber.value")
		
		if (vendorCode == "" || partNumber == "" || cogs == "") {
			alert("Please complete the new vendor form: select vendor, partnumber, and cogs")
		}
		else {
			ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?vendorCode=' + vendorCode + '&partNumber=' + partNumber + '&cogs=' + cogs + '&we_partNumber=' + we_partNumber + '&ogVend_partNumber=' + ogVend_partNumber,'dumpZone')
			setTimeout("refreshProdView(" + itemID + ")",500)
		}
	}
	
	function refreshProdView(itemID) {
		ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?itemDetails=' + itemID,'item_' + itemID)
	}
	
	function showHistory(masterItemID,itemDetails) {
		if (document.getElementById('newVendor_' + itemDetails).innerHTML == "") {
			ajax('/ajax/admin/ajaxInventoryAdjust.asp?purch=1&showHistory=' + masterItemID,'newVendor_' + itemDetails)
			setTimeout("historyDataCheck(" + masterItemID + ")",500)
		}
		else {
			document.getElementById('newVendor_' + itemDetails).innerHTML = ""
		}
	}
	
	var dataCheck = 0;
	function historyDataCheck(cell) {
		dataCheck++;
		var objDiv = document.getElementById("scrollBox_" + cell);
		if (objDiv.innerHTML == "" && dataCheck < 50) {
			setTimeout("historyDataCheck('" + cell + "')",500)
		}
		else if (objDiv.innerHTML != "") {			
			objDiv.scrollTop = objDiv.scrollHeight;
		}
	}
	
	ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?cartDetails=1','floatingCart')
	
	function quickSearch() {
		var partnumber = document.getElementById("frm_partnumber").value
		var partnumberList = escape(document.getElementById("frm_partnumberList").value)
		partnumberList = partnumberList.replace(/%0A/g,'bbrr')
		var keywords = document.getElementById("frm_keywords").value
		var invDays = document.getElementById("invDaysTop").value
		
		document.getElementById("results").innerHTML = "<div style='margin-top:200px; font-size:24px; font-weight:bold;'>Loading Data<br>Please Wait...</div>"
		ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?quickSearch=1&partNumber=' + partnumber + '&partnumberList=' + partnumberList + '&keywords=' + keywords + '&invDays=' + invDays,'results')
	}
	
	function showPurch(curPartNumber,itemDetails) {
		var curCellVal = document.getElementById('newVendor_' + itemDetails).innerHTML;
		if (curCellVal.indexOf("RcvAmt") > 0) {
			document.getElementById('newVendor_' + itemDetails).innerHTML = "";
		}
		else {
			ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?vendorHistory=' + curPartNumber,'newVendor_' + itemDetails);
		}
	}
	
	function allAddToOrder() {
		var orderAmt = document.getElementById("allOrderAmt").value;
		var prodCnt = document.getElementById("fullResultCnt").value;
		var partNumber = "";
		var returnVal = "";
		for (i = 1; i <= prodCnt; i++) {
			partNumber = document.getElementById("partnumber_o" + i).value;
			ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?voCode=<%=vendorCode%>&massOrderPN=' + partNumber + '&massOrderQty=' + orderAmt,'dumpZone');
			returnVal = returnVal + '/ajax/admin/ajaxFaceplatePurchaseReport.asp?voCode=<%=vendorCode%>&massOrderPN=' + partNumber + '&massOrderQty=' + orderAmt + '\n';
		}
		setTimeout("ajax('/ajax/admin/ajaxFaceplatePurchaseReport.asp?cartDetails=1','floatingCart')",1000);
	}
</script>