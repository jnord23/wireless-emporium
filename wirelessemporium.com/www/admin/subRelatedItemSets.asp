<%
	thisUser = Request.Cookies("username")
	pageTitle = "Edit Sub-Related Item Sets"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%	
	sql = "select distinct setName from we_defaultSubRelatedItems order by setName"
	session("errorSQL") = sql
	set relatedSetsRS = oConn.execute(sql)
%>
<table border="0" cellpadding="3" cellspacing="0" width="600" align="center">
    <tr>
    	<td>
        	<div style="float:left; font-weight:bold;">Select Set to Edit:</div>
            <div style="float:left; padding-left:10px;">
            	<select name="setName" onchange="pullData(this.value)">
                	<option value="">Select Set</option>
					<% do while not relatedSetsRS.EOF %>
                    <option value="<%=relatedSetsRS("setName")%>"><%=relatedSetsRS("setName")%></option>
                    <%
                        relatedSetsRS.movenext
                    loop
                    %>
                    <option value="New">New Related Item Set</option>
                </select>
            </div>
        </td>
    </tr>
    <tr>
    	<td id="result"></td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function pullData(setName) {
		ajax('/ajax/admin/ajaxSubRelatedItemSets.asp?setName=' + setName,'result')
	}
</script>