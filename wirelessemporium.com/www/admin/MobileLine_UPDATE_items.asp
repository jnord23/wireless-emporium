<%
'option explicit
response.buffer = false
pageTitle = "UPDATE Mobile Line Items"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
server.scripttimeout = 1000 'seconds
dim Path, Upload, Count
Path = server.mappath("tempTXT")
set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
Count = Upload.Save(Path)
if Upload.form("UPDATE") <> "" then
	if Count = 0 then
		response.write "No file selected."
		response.end
	else
		dim File, myFile
		set File = Upload.Files(1)
		myFile = File.path
		
		dim txtConn, myStr
		set txtConn = Server.CreateObject("ADODB.Connection")
		myStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & myFile & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"";"
		txtConn.ConnectionString = myStr
		txtConn.ConnectionTimeout = 1000		'seconds
		txtConn.Open
		response.write("file: " & myFile)
		
		dim SQL, RS, RStemp
		SQL = "SELECT * FROM [Sheet1$]"
		' WHERE [Mobile Line Part Number] <> '' ORDER BY [Mobile Line Part Number]"
		response.write "<h3>SQL: " & SQL & "</h3>" & vbCrLf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, txtConn, 3, 3
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			dim strMobileLine_skus, strNewPartNumbers, updateCount, deleteCount
			strMobileLine_skus = ""
			strNewPartNumbers = ""
			updateCount = 0
			deleteCount = 0
			
			do until RS.eof
				dim price_Our, price_CA, price_CO, price_PS, price_ER, price_Buy, COGS, price_Retail
				price_Our = RS("MSRP")
				COGS = RS("Mobile Line Price")
				price_Retail = RS("MSRP")
				
				dim inv_qty, UPCCode, MobileLine_sku
				inv_qty = RS("Quantity")
				if inv_qty < 0 then inv_qty = 0
				UPCCode = SQLquote(RS("UPC"))
				MobileLine_sku = SQLquote(RS("Mobile Line Part Number"))
				strMobileLine_skus = strMobileLine_skus & "'" & MobileLine_sku & "',"
				
				dim itemDesc, itemDesc_PS, itemDesc_CA, itemDesc_CO
				itemDesc = replace(replace(RS("Name"),vbtab,""),chr(9),"")
				
				dim itemLongDetail, itemLongDetail_PS, itemLongDetail_CA, itemLongDetail_CO
				itemLongDetail = replace(replace(RS("Description"),vbcrlf," "),"<br /><br />","<br />")
				
				dim productImage, productImage_CO
				productImage = RS("Image")
				if isNull(productImage) or productImage = "" then
					productImage = "imagena.jpg"
				else
					productImage = "MLD_" & productImage
					productImage = replace(replace(replace(productImage,".png",".jpg"),".gif",".jpg"),".JPG",".jpg")
				end if
				productImage_CO = replace(productImage,"_skin_","_decal_skin_")
				
				dim itemWeight
				if not isNull(RS("Weight (lbs)")) then
					itemWeight = int(round(RS("Weight (lbs)") * 16))
				else
					itemWeight = 0
				end if
				
				SQL = "SELECT itemID FROM we_Items WHERE MobileLine_sku = '" & MobileLine_sku & "'"
				set RStemp = Server.CreateObject("ADODB.Recordset")
				RStemp.open SQL, oConn, 3, 3
				if not RStemp.eof then
					updateCount = updateCount + 1
					SQL = "UPDATE we_Items SET "
					SQL = SQL & "itemDesc = '" & SQLquote(itemDesc) & "', "
					SQL = SQL & "price_Retail = " & price_Retail & ", "
					SQL = SQL & "price_Our = " & price_Our & ", "
					SQL = SQL & "COGS = " & COGS & ", "
					SQL = SQL & "inv_qty = " & inv_qty & ", "
					SQL = SQL & "itemLongDetail = '" & SQLquote(itemLongDetail) & "', "
					SQL = SQL & "itemWeight = '" & itemWeight & "', "
					SQL = SQL & "itemPic = '" & productImage & "', "
					SQL = SQL & "itemPic_CO = '" & productImage_CO & "', "
					SQL = SQL & "UPCCode = '" & UPCCode & "', "
					SQL = SQL & "MobileLine_sku = '" & MobileLine_sku & "'"
					SQL = SQL & " WHERE itemID = '" & RStemp("itemID") & "'"
					session("errorSQL") = SQL
					'response.write "<p>" & SQL & "</p>"
					oConn.execute SQL
				end if
				RS.movenext
			loop
		end if
		
		SQL = "SELECT itemID FROM we_items WHERE vendor = 'MLD' AND MobileLine_sku NOT IN (" & left(strMobileLine_skus,len(strMobileLine_skus)-1) & ")"
		response.write "<p>" & SQL & "</p>"
		set RStemp = Server.CreateObject("ADODB.Recordset")
		RStemp.open SQL, oConn, 3, 3
		do until RStemp.eof
			deleteCount = deleteCount + 1
			SQL = "UPDATE we_items SET inv_qty = 0 WHERE itemID = '" & RStemp("itemID") & "'"
			session("errorSQL") = SQL
			'response.write "<p>" & SQL & "</p>"
			oConn.execute SQL
			RStemp.movenext
		loop
		RS.close
		set RS = nothing
		%>
		<h3><%=updateCount%> items updated.</h3>
		<h3><%=deleteCount%> items had inventory set to zero.</h3>
		<h3>DONE!</h3>
		<%
	end if
else
	%>
	<h3>
		[UPDATE MobileLine items in WE]<br>
		Select the file you saved to your hard drive:<br>
		(IMPORTANT: Make sure that the Worksheet is named "Sheet1"!)
	</h3>
	<form enctype="multipart/form-data" action="MobileLine_UPDATE_items.asp" method="POST">
		<p><input type="FILE" name="FILE1"></p>
		<p><input type="SUBMIT" name="UPDATE" value="UPDATE"></p>
	</form>
	<%
end if

function SQLquote(strValue)
	if not isNull(strValue) and strValue <> "" then
		SQLquote = trim(replace(strValue,"'","''"))
		SQLquote = replace(SQLquote,"&#039;","''")
		SQLquote = replace(SQLquote,chr(34),"''")
	else
		SQLquote = strValue
	end if
end function

function zeroPad(m,t)
	zeroPad = String(t-Len(m),"0") & m
end function
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
