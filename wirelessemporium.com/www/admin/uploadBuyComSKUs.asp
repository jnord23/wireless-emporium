<%
pageTitle = "Admin - Upload Buy.com SKUs"
header = 1
response.buffer = false
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<font face="Arial,Helvetica">
<blockquote>
<blockquote>

<%
server.scripttimeout = 1000 'seconds
dim Path
Path = server.mappath("tempTXT")

' Create an instance of AspUpload object
Set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
Count = Upload.Save(Path)
if Upload.form("upload") <> "" then
	' Capture uploaded file. Return the number of files uploaded
	if Count = 0 then
		response.write "No file selected."
		response.end
	else
		' Obtain File object representing uploaded file
		set File = Upload.Files(1)
		myFile = File.path
		
		dim txtConn, myStr, ReferenceId
		set txtConn = Server.CreateObject("ADODB.Connection")
		myStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & myFile & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"";"
		txtConn.ConnectionString = myStr
		txtConn.ConnectionTimeout = 1000		'seconds
		txtConn.Open
		
		'SQL = "SELECT * FROM [Sheet1$] ORDER BY SKU"
		SQL = "SELECT * FROM [Sheet1$] ORDER BY ProductId"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, txtConn, 3, 3
		
		do until RS.eof
			ReferenceId = trim(RS("ReferenceId"))
			if not isNull(ReferenceId) and ReferenceId <> "" then
				itemID = left(replace(ReferenceId,"WE",""),5)
				if not isNumeric(itemID) then itemID = left(itemID,4)
				if not isNumeric(itemID) then itemID = left(itemID,3)
				if not isNumeric(itemID) then itemID = left(itemID,2)
				if not isNumeric(itemID) then itemID = left(itemID,1)
				'SQL = "UPDATE we_items SET BUY_sku='" & RS("SKU") & "' WHERE itemID='" & itemID & "'"
				SQL = "UPDATE we_items SET BUY_sku='" & RS("ProductId") & "' WHERE itemID='" & itemID & "'"
				response.write SQL & "<br>" & vbcrlf
				oConn.execute SQL
			end if
			RS.movenext
		loop
		response.write "<h3>DONE!</h3>" & vbcrlf
		
		RS.close
		set RS = nothing
		txtConn.close
		set txtConn = nothing
	end if
else
	%>
	<h3>Select the file you saved to your hard drive:</h3>
	<p>(IMPORTANT: Make sure the first column of your .XLS file is named "SKU" and the Worksheet is named "Sheet1"!)</p>
	<form enctype="multipart/form-data" action="uploadBuyComSKUs.asp" method="POST">
		<p><input type="FILE" name="FILE1"></p>
		<p><input type="SUBMIT" name="upload" value="Upload"></p>
	</form>
	<%
end if
%>

</blockquote>
</blockquote>
</font>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
