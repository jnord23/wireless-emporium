<%
Function fnstrFileOutputChar(strFileType,strData,strFuture1)
	Dim strTemp
	strTemp = Trim("" & strData)
	strFileType = uCASE(strFileType)
	'CONVERT FOLLOWING CHARACTERS TO EQUIVALENT HTML CHARACTERS
	strTemp = Replace(strTemp,Chr(34),"&#034;")	'DOUBLE QUOTE
	strTemp = Replace(strTemp,Chr(39),"&#039;")	'SINGLE QUOTE
	strTemp = Replace(strTemp,Chr(96),"&#096;")	'APOSTROPHE
	strTemp = Replace(strTemp,Chr(44),"&#044;")	'COMMA
	strTemp = Replace(strTemp,Chr(9),"&#009;")		'Tab Character &#009;
	strTemp = Replace(strTemp,Chr(10),"<BR>")	'Line Feed
	strTemp = Replace(strTemp,Chr(11),"<BR>")	'Vertical Tab
	strTemp = Replace(strTemp,Chr(12)," ")	'Form Feed
	strTemp = Replace(strTemp,Chr(13),"<BR>")	'Carriage Return
	Select Case strFileType
		Case "PIPE" : strTemp = Replace(strTemp,Chr(124),"&#124;")	'FOR PIPE DELIMITED FILE, REPLACE PIPE SYMBOL HTML EQUIVALENT
	End Select
	fnstrFileOutputChar = strTemp
End Function

Function fnReturnNumber(strAmount,deccount,defvalue,strFormat)
	Dim strTemp
	strTemp = strAmount
	strTemp = Trim("" & strTemp)
	strFormat = UCASE(strFormat)
	If strTemp = "" Then
		fnReturnNumber=FormatNumber(defvalue,deccount,,,0)	'0 - NO COMMAS
	ElseIf IsNumeric(strTemp) = False Then
		fnReturnNumber=FormatNumber(defvalue,deccount,,,0)	'0 - NO COMMAS
	Else
		fnReturnNumber = FormatNumber(strAmount,deccount,,,0)	'0 - NO COMMAS
	End If
	Select Case strFormat
		Case "CURRENCY": fnReturnNumber = FormatCurrency(fnReturnNumber,deccount,,,0)
	End Select
End Function
%>
