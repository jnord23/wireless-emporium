<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%header = 1%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 9000 'seconds

dim fs, file, filename, path
'Map the file name to the physical path on the server.
filename = "productList_cj.txt"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List upload for Commission Junction</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							Response.write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='../tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
	
<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	response.write("Creating " & filename & ".<br>")
	set file = fs.CreateTextFile(path)
	dim SQL, RS
	SQL = "SELECT A.itemID,A.brandID,A.PartNumber AS MPN,A.itemDesc AS GRPNAME,A.itemLongDetail,"
	SQL = SQL & "A.itemPic,A.price_Retail,A.price_our,A.inv_qty,A.Sports,A.flag1,A.UPCCode,"
	SQL = SQL & "B.brandName,Rtrim(B.brandName) + ' > ' + C.modelName AS ModelName,C.[temp],D.typeName,C.modelName AS model"
	SQL = SQL & " FROM ((we_Items A LEFT JOIN we_Brands B ON A.brandID=B.BrandID)"
	SQL = SQL & " LEFT JOIN we_Models C ON A.modelID=C.modelID)"
	SQL = SQL & " LEFT JOIN we_Types D ON A.typeID=D.typeID"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		dim RS2, DoNotInclude, strItemDesc, strItemLongDetail, stypeName, ModelName, BrandName
		file.writeLine "&CID=1629329"
		file.writeLine "&SUBID=6534"
		file.writeLine "&PROCESSTYPE=OVERwrite"
		file.writeLine "&AID=10398960"
		file.writeLine "&PARAMETERS=NAME|KEYWORDS|DESCRIPTION|SKU|BUYURL|AVAILABLE|IMAGEURL|PRICE|RETAILPRICE|PROMOTIONALTEXT|ADVERTISERCATEGORY|MANUFACTURER|STANDARDSHIPPINGCOST"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("MPN") & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				strItemDesc = RS("GRPNAME")
				strItemDesc = Replace(strItemDesc,"+","&")
				strItemLongDetail = RS("itemLongDetail")
				strItemLongDetail = replace (strItemLongDetail,vbcrlf," ")
				if isnull(RS("typeName")) then
					stypeName = "Universal Gear"
				else
					stypeName = RS("typeName")
				end if
				if isnull(RS("ModelName")) then
					ModelName = "Universal"
				else
					ModelName = RS("ModelName")
				end if
				if isnull(RS("BrandName")) then
					BrandName = "Universal"
				else 
					BrandName = RS("BrandName")
				end if
								
				file.write RS("GRPNAME") & vbtab
				file.write stypeName & "," & replace(ModelName," > "," ") & "," & strItemDesc & vbtab
				file.write strItemLongDetail & vbtab
				file.write "WE-" & RS("itemID") + 5000 & vbtab
				file.write "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("GRPNAME")) & ".asp" & vbtab
				file.write "YES" & vbtab
				file.write "http://www.wirelessemporium.com/productpics/thumb/" & RS("itemPic") & vbtab
				file.write RS("price_our") & vbtab
				file.write RS("price_retail") & vbtab
				file.write "Free Shipping on All Orders"  & vbtab
				file.write "Cell Phone Accessories > Cell Phone " & stypeName & " > " & ModelName & " > " & replace(ModelName," > "," ") & " " & stypeName & vbtab
				file.write BrandName & vbtab
				file.write "0" & vbcrlf
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
