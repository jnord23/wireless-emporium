<%
pageTitle = "Admin - Update WE Database - Add/Edit Products"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_DbInventory.asp"-->
<!--#include virtual="/framework/utility/ftp.asp"-->
<%
useFTP = true
if useFTP then
	call initConnection(ftpWEB2,"192.168.112.161","ftpAdmin","GSF79h7yr9r3$C")	'WEB2
	call initConnection(ftpWEB3,"192.168.112.163","ftpAdmin","GSF79h7yr9r3$C")	'WEB3
end if

set filesys = CreateObject("Scripting.FileSystemObject")

set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = true
myCount = Upload.Save

strError = ""
frmSubmit = upload.form("frmSubmit")
if frmSubmit = "Copy" then
	frmSubmitCopy = "Copy to New Item"
	frmSubmit = "Add"
else
	frmSubmitCopy = ""
end if
if frmSubmit <> "Add" and frmSubmit <> "Edit" then
	call CloseConn(oConn)
	response.write "<h3>You have reached this page in error. This page is only for processing submissions sent from a form page.frmSubmit" & frmSubmit & "</h3>"
	response.end
end if

dim showAnimation
dim BrandID, modelID, typeID, ItemID, carrierID, vendor, PartNumber
dim price_Retail, price_Our, price_CA, price_CO, price_PS, price_Buy, COGS
dim Seasonal, Sports, Bluetooth, HandsfreeType, ItemKit_NEW, hotDeal, hideLive
dim UPCCode, AMZN_sku, itemWeight, itemDimensions, itemPic, itemPic_CO, flag1
dim COMPATIBILITY, download_URL, download_TEXT
dim itemDesc, itemLongDetail, itemInfo, BULLET1, BULLET2, BULLET3, BULLET4, BULLET5, BULLET6, BULLET7, BULLET8, BULLET9, BULLET10
dim itemDesc_CO, itemLongDetail_CO, POINT1, POINT2, POINT3, POINT4, POINT5, POINT6, POINT7, POINT8, POINT9, POINT10
dim itemDesc_CA, itemLongDetail_CA
dim itemDesc_PS, itemLongDetail_PS, FEATURE1, FEATURE2, FEATURE3, FEATURE4, FEATURE5, FEATURE6, FEATURE7, FEATURE8, FEATURE9, FEATURE10
dim itemDesc_ER, itemLongDetail_ER
dim Features, FormFactor, BandType, Band, Smartphone, PackageContents, TalkTime, Standby, Display, NoDiscount, colorID
dim batteryCapacity

showAnimation = upload.form("inputShowAnimation")
ItemID = upload.form("ItemID")
BrandID = upload.form("BrandID")
modelID = upload.form("modelID")
typeID = upload.form("typeID")
subtypeID = upload.form("subtypeID")
carrierID = upload.form("carrierID")
HandsfreeTypeID = upload.form("HandsfreeTypeID")
ItemKit = upload.form("ItemKit")
customize = upload.form("customize")
vendor = upload.form("vendor")
PartNumber = upload.form("PartNumber")
og_PartNumber = upload.form("og_PartNumber")
price_Retail = upload.form("price_Retail")
colorID = prepInt(upload.form("rdColor"))
artistID = prepInt(upload.form("artistID"))
batteryCapacity = prepInt(upload.form("batteryCapacity"))

if batteryCapacity > 0 then
	sql = "update we_pnDetails set batteryCapacity = " & batteryCapacity & " where partNumber = '" & PartNumber & "'"
	session("errorSQL") = sql
	oConn.execute(sql)
else
	sql = "update we_pnDetails set batteryCapacity = null where partNumber = '" & PartNumber & "'"
	session("errorSQL") = sql
	oConn.execute(sql)
end if

set fso = CreateObject("Scripting.FileSystemObject")

autoDeleteCompPages "updateItem",brandID & "##" & modelID & "##" & subtypeID & "##" & itemID
response.Write("updateItem," & brandID & "##" & modelID & "##" & subtypeID & "##" & itemID & "<br>")

function GetItemValue( byref objUpload)
	dim dicSitePriceKey : set dicSitePriceKey = CreateObject("Scripting.Dictionary")
	dicSitePriceKey( WE_ID) = "price_Our"
	dicSitePriceKey( CA_ID) = "price_CA"
	dicSitePriceKey( CO_ID) = "price_CO"
	dicSitePriceKey( PS_ID) = "price_PS"
	dicSitePriceKey( ER_ID) = "price_ER"

	dim dicReturnValue : set dicReturnValue = CreateObject("Scripting.Dictionary")
	dicReturnValue.CompareMode = 1 ' -- case-insensitive mode

	for each strSiteId in dicSitePriceKey.Keys
		dim dicAttribute : set dicAttribute = CreateObject("Scripting.Dictionary")
		dim strDiscountOption: strDiscountOption = Obj2Str( objUpload.form( "DiscountOption"&strSiteId))

		dicAttribute( "OriginalPrice") = Obj2Str( objUpload.form( dicSitePriceKey( strSiteId)))

		if HasNoLen( strDiscountOption) then 'i.e., not posting from the special discount form version
			'call dbg( strSiteId, "bad"): response.Write "<br />"
			dicAttribute( "ActiveItemValueTypeId") = 1 '--default to original price
			dicAttribute( "DiscountPrice") = EMPTY_STRING
			dicAttribute( "DiscountPercent") = EMPTY_STRING

			execute( dicSitePriceKey( strSiteId)&"="&EvalStr( FormatDecimalInput( Obj2Str( dicAttribute( "OriginalPrice")))))
		else
			'call dbg( strSiteId, "good"): response.Write "<br />"
			if strDiscountOption <> "NoSale" then dicAttribute( "OriginalPrice") = FormatDecimalInput( Obj2Str( objUpload.form( "OriginalPrice"&strSiteId))) 'i.e, override w/ hidden field value since the previous OriginalPrice was not modified

			dicAttribute( "DiscountPrice") = FormatDecimalInput( Obj2Str( objUpload.form( "DiscountPrice"&strSiteId))) 'i.e, hidden field
			dicAttribute( "DiscountPercent") = FormatDecimalInput( Obj2Str( objUpload.form( "DiscountPercent"&strSiteId))) 'i.e, hidden field


			select case( strDiscountOption)
				case "NoSale":
					dicAttribute( "ActiveItemValueTypeId") = 1
					execute( dicSitePriceKey( strSiteId)&"="&EvalStr( FormatDecimalInput( Obj2Str( dicAttribute( "OriginalPrice")))))
				case "$":
					dicAttribute( "ActiveItemValueTypeId") = 2
					dicAttribute( "DiscountPrice") = FormatDecimalInput( Obj2Str( objUpload.form( "DiscountValue"&strSiteId))) 'i.e, hidden field
					execute( dicSitePriceKey( strSiteId)&"="&EvalStr( FormatDecimalInput( Obj2Str( dicAttribute( "DiscountPrice")))))
				case "%off":
					dicAttribute( "ActiveItemValueTypeId") = 3
					dicAttribute( "DiscountPercent") = FormatDecimalInput( Obj2Str( objUpload.form( "DiscountValue"&strSiteId))) 'i.e, hidden field
					if IsNumeric( dicAttribute( "DiscountPercent")) and IsNumeric( dicAttribute( "OriginalPrice")) then
						execute( dicSitePriceKey( strSiteId)&"="& EvalStr( FormatDecimalInput( CStr((100.0-CDbl( dicAttribute( "DiscountPercent")))*CDbl( dicAttribute( "OriginalPrice"))/100.0))))
					end if
			end select

		end if

		set dicReturnValue( strSiteId) = dicAttribute
	next
	set GetItemValue=dicReturnValue
end function


dim dicItemValue: set dicItemValue = GetItemValue( upload) 'sets the effective price based on selected discount options, hash values are to be used in setting the ItemValue table [knguyen/20110602]

price_Buy = upload.form("price_Buy")
COGS = upload.form("COGS")
NoDiscount = upload.form("NoDiscount")
if NoDiscount <> "1" then NoDiscount = "0"

Seasonal = trueFalse(upload.form("Seasonal"))
Sports = upload.form("Sports")
if Sports = "" then Sports = "0"
hotDeal = trueFalse(upload.form("hotDeal"))
hideLive = trueFalse(upload.form("hideLive"))
UPCCode = upload.form("UPCCode")
AMZN_sku = upload.form("AMZN_sku")
itemWeight = upload.form("itemWeight")
itemDimensions = upload.form("itemDimensions")
itemPic = upload.form("itemPic")
flag1 = upload.form("flag1")
Smartphone = upload.form("Smartphone")
if Smartphone <> "1" then Smartphone = "0"

itemDesc = upload.form("itemDesc")
itemLongDetail = upload.form("itemLongDetail")
itemInfo = upload.form("itemInfo")

dim fileWE, fileCO
set fileWE = upload.files("itemPicUpload")
if not fileWE is nothing then fileWEname = fileWE.filename
if itemPic = "" then
	if fileWE is nothing then
		strError = strError & "<li>No image uploaded.</li>"
	else
		if myCount > 0 then
			for each file in upload.files
				if fileWE.ImageType <> "JPEG" and fileWE.ImageType <> "JPG" then strError = strError & "<li>" & fileWE.ImageType & " is not an acceptable image format.</li>"
			next
		end if
	end if
end if

if frmSubmit = "Edit" and ItemID = "" then strError = strError & "<li>No ItemID.</li>"
if itemDesc = "" then strError = strError & "<li>You must enter an itemDesc.</li>"
if itemLongDetail = "" then strError = strError & "<li>You must enter an itemLongDetail.</li>"
if price_Retail = "" then strError = strError & "<li>You must enter a price_Retail.</li>"
if price_Our = "" then strError = strError & "<li>You must enter a price_Our.</li>"

if itemWeight = "" or not isNumeric(itemWeight) then strError = strError & "<li>You must enter an itemWeight.</li>"

if PartNumber = "" then
	strError = strError & "<li>You must enter a PartNumber.</li>"
end if

if not isNumeric(price_Retail) then
	strError = strError & "<li>Retail Price must be a valid number.</li>"
else
	price_Retail = cDbl(price_Retail)
	if not isNumeric(price_Our) then
		strError = strError & "<li>WE Price must be a valid number.</li>"
	else
		price_Our = cDbl(price_Our)
		'if price_Our >= price_Retail then strError = strError & "<li>WE Price must be less than Retail Price.</li>"
		if price_Our < 1.99 then strError = strError & "<li>WE Price must be at least $1.99.</li>"
	end if
end if

if price_CA <> "" then
	if not isNumeric(price_CA) then
		strError = strError & "<li>CA Price must be a valid number.</li>"
	else
		price_CA = cDbl(price_CA)
	end if
else
	price_CA = 0
end if

set fileCO = upload.files("itemPic_CO_Upload")
if not fileCO is nothing then fileCOname = fileCO.filename
if price_CO <> "" then
	itemPic_CO = upload.form("itemPic_CO")
	if not isNumeric(price_CO) then
		strError = strError & "<li>CO Price must be a valid number.</li>"
	else
		price_CO = cDbl(price_CO)
	end if
	if itemPic_CO = "" then
		if fileCO is nothing then
			strError = strError & "<li>No CO image uploaded.</li>"
		end if
	end if
else
	price_CO = 0
end if

if price_PS <> "" then
	if not isNumeric(price_PS) then
		strError = strError & "<li>Phonesale Price must be a valid number.</li>"
	else
		price_PS = cDbl(price_PS)
	end if
else
	price_PS = 0
end if

if price_ER <> "" then
	if not isNumeric(price_ER) then
		strError = strError & "<li>TabletMall Price must be a valid number.</li>"
	else
		price_ER = cDbl(price_ER)
	end if
else
	price_ER = 0
end if

if price_Buy <> "" then
	if not isNumeric(price_Buy) then
		strError = strError & "<li>price_Buy must be a valid number.</li>"
	else
		price_Buy = cDbl(price_Buy)
	end if
else
	price_Buy = 0
end if

if COGS <> "" then
	if not isNumeric(COGS) then
		strError = strError & "<li>COGS must be a valid number.</li>"
	else
		COGS = cDbl(COGS)
	end if
else
	COGS = 0
end if

if flag1 = "" then
	SQL = "SELECT MAX(ItemID) AS MaxItemID FROM we_items"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then MaxItemID = RS("MaxItemID")
	flag1 = cStr(MaxItemID+1)
end if

itemDesc_CA = upload.form("itemDesc_CA")
itemLongDetail_CA = upload.form("itemLongDetail_CA")

itemDesc_PS = upload.form("itemDesc_PS")
itemLongDetail_PS = upload.form("itemLongDetail_PS")

itemDesc_ER = upload.form("itemDesc_ER")
itemLongDetail_ER = upload.form("itemLongDetail_ER")

BULLET1 = upload.form("BULLET1")
BULLET2 = upload.form("BULLET2")
BULLET3 = upload.form("BULLET3")
BULLET4 = upload.form("BULLET4")
BULLET5 = upload.form("BULLET5")
BULLET6 = upload.form("BULLET6")
BULLET7 = upload.form("BULLET7")
BULLET8 = upload.form("BULLET8")
BULLET9 = upload.form("BULLET9")
BULLET10 = upload.form("BULLET10")
COMPATIBILITY = upload.form("COMPATIBILITY")
download_URL = upload.form("download_URL")
download_TEXT = upload.form("download_TEXT")

POINT1 = upload.form("POINT1")
POINT2 = upload.form("POINT2")
POINT3 = upload.form("POINT3")
POINT4 = upload.form("POINT4")
POINT5 = upload.form("POINT5")
POINT6 = upload.form("POINT6")
POINT7 = upload.form("POINT7")
POINT8 = upload.form("POINT8")
POINT9 = upload.form("POINT9")
POINT10 = upload.form("POINT10")
itemDesc_CO = upload.form("itemDesc_CO")
itemLongDetail_CO = upload.form("itemLongDetail_CO")

Features = ""
if price_PS > 0 then
	FEATURE1 = upload.form("FEATURE1")
	FEATURE2 = upload.form("FEATURE2")
	FEATURE3 = upload.form("FEATURE3")
	FEATURE4 = upload.form("FEATURE4")
	FEATURE5 = upload.form("FEATURE5")
	FEATURE6 = upload.form("FEATURE6")
	FEATURE7 = upload.form("FEATURE7")
	FEATURE8 = upload.form("FEATURE8")
	FEATURE9 = upload.form("FEATURE9")
	FEATURE10 = upload.form("FEATURE10")
	itemDesc_PS = upload.form("itemDesc_PS")
	itemLongDetail_PS = upload.form("itemLongDetail_PS")
	FormFactor = upload.form("FormFactor")
	BandType = upload.form("BandType")
	Band = upload.form("Band")
	PackageContents = upload.form("PackageContents")
	TalkTime = upload.form("TalkTime")
	Standby = upload.form("Standby")
	Display = upload.form("Display")
	for each thing in upload.form
		if thing.name = "Features" and not isEmpty(thing.value) then Features = Features & "," & thing.value
	next
end if
if Features = "" then
	Features = "null"
else
	Features = "'" & Features & ",'"
end if

if strError = "" then
	dim id_new_field, id_new_field_CO, path, path_CO, newName, newName_CO, myFileName, myFileName_CO, myFileType, myFileType_CO
	id_new_field = formatSEO(replace(itemDesc, chr(34), "") & replace(replace(replace(replace(time,":","")," ",""),"PM",""),"AM",""))
	id_new_field_CO = formatSEO(replace(itemDesc_CO, chr(34), "") & replace(replace(replace(replace(time,":","")," ",""),"PM",""),"AM",""))

	path = server.mappath("\productpics") & "\"
	path_CO = server.mappath("\productpics_co") & "\"

	tempPath = server.mappath("\productpics") & "\tempImages\"
	tempCOPath = server.mappath("\productpics_co") & "\tempImages\"

	set jpeg = Server.CreateObject("Persits.Jpeg")
	if not fileWE is nothing then
		'response.write "not fileWE is nothing" & "<br>"
		jpeg.OpenBinary(fileWE.Binary)
		myFileType = ".jpg"
		if fileWEname = "imagena.jpg" then
			newName = "imagena.jpg"
		else
			newName = id_new_field & myFileType
		end if
		myFileName = newName

		if jpeg.Width = 800 and jpeg.Height = 800 then
			SavePath = tempPath & "big\zoom\" & newName
			jpeg.Width = 800 : jpeg.Height = 800 : jpeg.Save SavePath
			response.write "Saved: " & SavePath & "<br>" & vbcrlf
			call uploadImagesRemote("\productpics\big\zoom", SavePath, newName)
		end if

		SavePath = tempPath & "big\" & newName
		session("errorSQL") = SavePath
		jpeg.Width = 300 : jpeg.Height = 300 : jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		call uploadImagesRemote("\productpics\big", SavePath, newName)

		SavePath = tempPath & "thumb\" & newName
		jpeg.Width = 100 : jpeg.Height = 100 : jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		call uploadImagesRemote("\productpics\thumb", SavePath, newName)

		SavePath = tempPath & "homepage65\" & newName
		jpeg.Width = 65 : jpeg.Height = 65 : jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		call uploadImagesRemote("\productpics\homepage65", SavePath, newName)

		SavePath = tempPath & "icon\" & newName
		jpeg.Width = 45 : jpeg.Height = 45 : jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		call uploadImagesRemote("\productpics\icon", SavePath, newName)
	else
		'response.write "not fileWE is nothing - ELSE" & "<br>"
		myFileName = itemPic
		myFileType = right(myFileName,4)
		if fileWEname = "imagena.jpg" then
			newName = "imagena.jpg"
		else
			newName = id_new_field & myFileType
		end if

		if filesys.fileExists(path & "big\zoom\" & myFileName) then
			jpeg.Open(path & "big\zoom\" & myFileName)
			SavePath = tempPath & "big\zoom\" & newName
			jpeg.Width = 800 : jpeg.Height = 800 : jpeg.Save SavePath
			response.write "Saved: " & SavePath & "<br>" & vbcrlf
			call uploadImagesRemote("\productpics\big\zoom", SavePath, newName)
		end if

		jpeg.Open(path & "big\" & myFileName)
		SavePath = tempPath & "big\" & newName
		jpeg.Width = 300 : jpeg.Height = 300 : jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		call uploadImagesRemote("\productpics\big", SavePath, newName)

		SavePath = tempPath & "thumb\" & newName
		jpeg.Width = 100 : jpeg.Height = 100 : jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		call uploadImagesRemote("\productpics\thumb", SavePath, newName)

		SavePath = tempPath & "homepage65\" & newName
		jpeg.Width = 65 : jpeg.Height = 65 : jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		call uploadImagesRemote("\productpics\homepage65", SavePath, newName)

		SavePath = tempPath & "icon\" & newName
		jpeg.Width = 45 : jpeg.Height = 45 : jpeg.Save SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		call uploadImagesRemote("\productpics\icon", SavePath, newName)
	end if
	' Alt Views
	for iCount = 0 to 7
		set fileWE = upload.files("itemPicUpload" & iCount)
		if not fileWE is nothing then
			jpeg.OpenBinary(fileWE.Binary)

			if jpeg.Width = 800 and jpeg.Height = 800 then
				SavePath = tempPath & "AltViews\zoom\" & id_new_field & "-" & iCount & myFileType
				jpeg.Width = 800 : jpeg.Height = 800 : jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics\AltViews\zoom", SavePath, id_new_field & "-" & iCount & myFileType)
			end if

			SavePath = tempPath & "AltViews\" & id_new_field & "-" & iCount & myFileType
			session("errorSQL") = "SavePath:" & SavePath
			jpeg.Width = 300 : jpeg.Height = 300 : jpeg.Save SavePath
			response.write "Saved: " & SavePath & "<br>" & vbcrlf
			call uploadImagesRemote("\productpics\AltViews", SavePath, id_new_field & "-" & iCount & myFileType)
		end if
	next
	'flash view
	set fileWE = upload.files("itemFlash")
	if not fileWE is nothing then
		SavePath = tempPath & "swf\" & id_new_field & ".swf"
		session("errorSQL") = "SavePath:" & SavePath
		fileWE.SaveAs SavePath
		response.write "Saved: " & SavePath & "<br>" & vbcrlf
		call uploadImagesRemote("\productpics\swf", SavePath, id_new_field & ".swf")
	end if

	' CO image
	if price_CO <> "" then
		if not fileCO is nothing then
			'response.write "not fileCO is nothing" & "<br>"
			jpeg.OpenBinary(fileCO.Binary)
			myFileType_CO = ".jpg"
			if fileCOname = "imagena.jpg" then
				newName_CO = "imagena.jpg"
			else
				newName_CO = id_new_field_CO & myFileType_CO
			end if
			myFileName_CO = newName_CO

			if jpeg.Width = 800 and jpeg.Height = 800 then
				SavePath = tempCOPath & "big\zoom\" & newName_CO
				jpeg.Width = 800 : jpeg.Height = 800 : jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics_co\big\zoom", SavePath, newName_CO)
			end if

			SavePath = tempCOPath & "big\" & newName_CO
			jpeg.Width = 300 : jpeg.Height = 300 : jpeg.Save SavePath
			response.write "Saved: " & SavePath & "<br>" & vbcrlf
			call uploadImagesRemote("\productpics_co\big", SavePath, newName_CO)

			SavePath = tempCOPath & "thumb\" & newName_CO
			jpeg.Width = 100 : jpeg.Height = 100 : jpeg.Save SavePath
			response.write "Saved: " & SavePath & "<br>" & vbcrlf
			call uploadImagesRemote("\productpics_co\thumb", SavePath, newName_CO)

			SavePath = tempCOPath & "homepage65\" & newName_CO
			jpeg.Width = 65 : jpeg.Height = 65 : jpeg.Save SavePath
			response.write "Saved: " & SavePath & "<br>" & vbcrlf
			call uploadImagesRemote("\productpics_co\homepage65", SavePath, newName_CO)

			SavePath = tempCOPath & "icon\" & newName_CO
			jpeg.Width = 45 : jpeg.Height = 45 : jpeg.Save SavePath
			response.write "Saved: " & SavePath & "<br>" & vbcrlf
			call uploadImagesRemote("\productpics_co\icon", SavePath, newName_CO)

		elseif itemPic_CO <> "" then
			'response.write "itemPic_CO <> """"" & "<br>"
			myFileName_CO = itemPic_CO
			myFileType_CO = right(myFileName_CO,4)
			if fileCOname = "imagena.jpg" then
				newName_CO = "imagena.jpg"
			else
				newName_CO = id_new_field_CO & myFileType_CO
			end if

			if filesys.fileExists(path_CO & "big\zoom\" & myFileName_CO) then
				jpeg.Open(path_CO & "big\zoom\" & myFileName_CO)
				SavePath = tempCOPath & "big\zoom\" & newName_CO
				jpeg.Width = 800 : jpeg.Height = 800 : jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics_co\big\zoom", SavePath, newName_CO)
			end if

			if filesys.fileExists(path_CO & "big\" & myFileName_CO) then
				session("errorSQL") = "openFile: " & path_CO & "big\" & myFileName_CO
				jpeg.Open(path_CO & "big\" & myFileName_CO)

				SavePath = tempCOPath & "big\" & newName_CO
				session("errorSQL") = "SavePath:" & SavePath
				jpeg.Width = 300 : jpeg.Height = 300 : jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics_co\big", SavePath, newName_CO)

				SavePath = tempCOPath & "thumb\" & newName_CO
				jpeg.Width = 100 : jpeg.Height = 100 : jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics_co\thumb", SavePath, newName_CO)

				SavePath = tempCOPath & "homepage65\" & newName_CO
				jpeg.Width = 65 : jpeg.Height = 65 : jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics_co\homepage65", SavePath, newName_CO)

				SavePath = tempCOPath & "icon\" & newName_CO
				jpeg.Width = 45 : jpeg.Height = 45 : jpeg.Save SavePath
				response.write "Saved: " & SavePath & "<br>" & vbcrlf
				call uploadImagesRemote("\productpics_co\icon", SavePath, newName_CO)
			end if
		end if
	end if

	'--- START WE_URL
	SQL = "SELECT typeName FROM we_Types WHERE typeID = '" & typeID & "'"
	session("errorSQL") = sql
	set RS = oConn.execute(SQL)
	if not RS.eof then
		if isnull(RS("typeName")) then
			stypeName = "Universal Gear"
		else
			stypeName = RS("typeName")
		end if
		stypeName = changeTypeName(stypeName)
	end if
	SQL = "SELECT ModelName FROM we_Models WHERE modelID = '" & modelID & "'"
	session("errorSQL") = sql
	set RS = oConn.execute(SQL)
	if not RS.eof then
		if isnull(RS("ModelName")) then
			ModelName = "Universal"
		else
			ModelName = RS("ModelName")
		end if
	end if

	SQL = "SELECT BrandName FROM we_Brands WHERE brandID = '" & brandID & "'"
	session("errorSQL") = sql
	set RS = oConn.execute(SQL)
	if not RS.eof then
		if isnull(RS("BrandName")) then
			BrandName = "Universal"
		else
			BrandName = RS("BrandName")
		end if
	end if
	id_new_field = BrandName & "-" & ModelName & "-" & stypeName & "-" & itemDesc
	id_new_field = formatSEO(id_new_field)
	id_new_field = replace(id_new_field,"---","-")
	id_new_field = replace(id_new_field,"--","-")
	if left(id_new_field,1) = "-" then id_new_field = right(id_new_field,len(id_new_field)-1)
	if right(id_new_field,1) = "-" then id_new_field = left(id_new_field,len(id_new_field)-1)
	'--- END WE_URL

	' ______ EDIT HANDLER __________________________________________________________________________________

	dim blnIsMaster: blnIsMaster = IsMasterItem( ItemId)

	if frmSubmit = "Edit" then
		sql = "select isnull(itemPic, '') itemPic from we_Items where itemID = " & itemId
		session("errorSQL") = sql
		set editRS = oConn.execute(sql)
		useAltImg = editRS("itemPic")

		response.write "<br>"
		if useAltImg <> "" then
			for i = 0 to 7
				curAltImg = left(useAltImg,len(useAltImg)-4) & "-" & i & ".jpg"
				response.Write("<br>useAltImg:" & useAltImg & "<br>")
				response.Write("curAltImg:" & curAltImg & "<br>")
				response.Write("newName:" & newName & "<br>")
				if filesys.fileExists(server.MapPath("/productpics/AltViews/" & curAltImg)) then
					set objFile = filesys.getFile(server.MapPath("/productpics/AltViews/" & curAltImg))
					newAltName = left(newName,len(newName)-4) & "-" & i & ".jpg"
					session("errorSQL") = "curAltImg:" & curAltImg & "<br>newAltName:" & newAltName
					if filesys.fileExists(server.MapPath("/productpics/AltViews/" & newAltName)) then
						response.Write("<div style='color:#f00; font-weight:bold;'>file already exists:" & server.MapPath("/productpics/AltViews/" & newAltName) & "</div>")
					else
						objFile.name = newAltName
					end if
				end if
				call uploadImagesRemote("\productpics\AltViews", server.MapPath("/productpics/AltViews/" & newAltName), newAltName)

				if filesys.fileExists(server.MapPath("/productpics/AltViews/zoom/" & curAltImg)) then
					set objFile = filesys.getFile(server.MapPath("/productpics/AltViews/zoom/" & curAltImg))
					newAltName = left(newName,len(newName)-4) & "-" & i & ".jpg"
					session("errorSQL") = "curAltImg:" & curAltImg & "<br>newAltName:" & newAltName
					if filesys.fileExists(server.MapPath("/productpics/AltViews/zoom/" & newAltName)) then
						response.Write("<div style='color:#f00; font-weight:bold;'>file already exists:" & server.MapPath("/productpics/AltViews/zoom/" & newAltName) & "</div>")
					else
						objFile.name = newAltName
					end if
				end if
				call uploadImagesRemote("\productpics\AltViews\zoom", server.MapPath("/productpics/AltViews/zoom/" & newAltName), newAltName)
			next

			curSwf = replace(useAltImg, ".jpg", ".swf")
			if filesys.fileExists(server.MapPath("/productpics/swf/" & curSwf)) then
				set objFile = filesys.getFile(server.MapPath("/productpics/swf/" & curSwf))
				newSwfName = replace(newName, ".jpg", ".swf")
				session("errorSQL") = "curSwf:" & curSwf & "<br>newSwfName:" & newSwfName
				response.Write("curSwf:" & curSwf & "<br>")
				response.Write("newSwfName:" & newSwfName & "<br>")
				if filesys.fileExists(server.MapPath("/productpics/swf/" & newSwfName)) then
					response.Write("<div style='color:#f00; font-weight:bold;'>file already exists:" & server.MapPath("/productpics/swf/" & newSwfName) & "</div>")
				else
					objFile.name = newSwfName
				end if
			end if

		end if

		sql = "update we_items set price_Retail = '" & price_Retail & "', price_Our = '" & price_Our & "', price_CA = '" & price_CA & "', "
		sql = sql & "price_CO = '" & price_CO & "', price_PS = '" & price_PS & "', price_ER = '" & price_ER & "', price_Buy = '" & price_Buy & "', colorid='" & colorID & "', "
		sql = sql & "COGS = '" & COGS & "', subtypeID = '" & subtypeID & "', itemWeight = '" & itemWeight & "', itemDimensions = '" & replace(itemDimensions, "'", "''") & "', flag1 = '" & flag1 & "', designType='" & artistID & "' where partNumber = '" & PartNumber & "'" '-- flag1 (sort order) added per jon [knguyen/20110603]
		session("errorSQL") = SQL
		oConn.execute SQL

		sql = "insert into we_adminActions (adminID,action) values(" & adminID & ",'update partNumber data:" & SQLquote(PartNumber) & "')"
		session("errorSQL") = SQL
		oConn.execute SQL

		SQL = "UPDATE we_items SET "
		SQL = SQL & "WE_URL = '" & id_new_field & "', "
		SQL = SQL & "brandID = '" & brandID & "', "
		SQL = SQL & "modelID = '" & modelID & "', "
		SQL = SQL & "typeID = '" & typeID & "', "
		SQL = SQL & "carrierID = '" & carrierID & "', "
		SQL = SQL & "PartNumber = '" & Ucase(PartNumber) & "', "
		SQL = SQL & "itemDesc = '" & SQLquote2(itemDesc) & "', "
		SQL = SQL & "itemDesc_PS = '" & SQLquote2(itemDesc_PS) & "', "
		SQL = SQL & "itemDesc_CA = '" & SQLquote2(itemDesc_CA) & "', "
		SQL = SQL & "itemDesc_CO = '" & SQLquote2(itemDesc_CO) & "', "
		SQL = SQL & "vendor = '" & vendor & "', "
		SQL = SQL & "price_Retail = '" & price_Retail & "', "
		SQL = SQL & "price_Our = '" & price_Our & "', "
		if price_CA = 0 then
			SQL = SQL & "price_CA = null, "
		else
			SQL = SQL & "price_CA = '" & price_CA & "', "
		end if
		if price_CO = 0 then
			SQL = SQL & "price_CO = null, "
		else
			SQL = SQL & "price_CO = '" & price_CO & "', "
		end if
		SQL = SQL & "price_PS = '" & price_PS & "', "
		SQL = SQL & "price_ER = '" & price_ER & "', "
		SQL = SQL & "price_Buy = '" & price_Buy & "', "
		SQL = SQL & "COGS = '" & COGS & "', "
		SQL = SQL & "Seasonal = " & Seasonal & ", "
		SQL = SQL & "Sports = '" & Sports & "', "
		if typeID = "5" and HandsfreeTypeID <> "" then SQL = SQL & "HandsfreeType = '" & HandsfreeTypeID & "', "
		if SQLquote2(ItemKit) <> "" then SQL = SQL & "ItemKit_NEW = '" & SQLquote2(ItemKit) & "', "
		SQL = SQL & "hotDeal = " & hotDeal & ", "
		SQL = SQL & "hideLive = " & hideLive & ", "
		SQL = SQL & "itemLongDetail = '" & SQLquote2(itemLongDetail) & "', "
		SQL = SQL & "itemInfo = '" & SQLquote2(itemInfo) & "', "
		SQL = SQL & "itemLongDetail_PS = '" & SQLquote2(itemLongDetail_PS) & "', "
		SQL = SQL & "itemLongDetail_CA = '" & SQLquote2(itemLongDetail_CA) & "', "
		SQL = SQL & "itemLongDetail_CO = '" & SQLquote2(itemLongDetail_CO) & "', "
		SQL = SQL & "UPCCode = '" & UPCCode & "', "
		SQL = SQL & "AMZN_sku = '" & AMZN_sku & "', "
		SQL = SQL & "itemWeight = '" & itemWeight & "', "
		SQL = SQL & "itemDimensions = '" & SQLquote2(itemDimensions) & "', "
		SQL = SQL & "itemPic = '" & newName & "', "
		SQL = SQL & "itemPic_CO = '" & newName_CO & "', "
		SQL = SQL & "flag1 = '" & flag1 & "', "
		SQL = SQL & "BULLET1 = '" & SQLquote2(BULLET1) & "', "
		SQL = SQL & "BULLET2 = '" & SQLquote2(BULLET2) & "', "
		SQL = SQL & "BULLET3 = '" & SQLquote2(BULLET3) & "', "
		SQL = SQL & "BULLET4 = '" & SQLquote2(BULLET4) & "', "
		SQL = SQL & "BULLET5 = '" & SQLquote2(BULLET5) & "', "
		SQL = SQL & "BULLET6 = '" & SQLquote2(BULLET6) & "', "
		SQL = SQL & "BULLET7 = '" & SQLquote2(BULLET7) & "', "
		SQL = SQL & "BULLET8 = '" & SQLquote2(BULLET8) & "', "
		SQL = SQL & "BULLET9 = '" & SQLquote2(BULLET9) & "', "
		SQL = SQL & "BULLET10 = '" & SQLquote2(BULLET10) & "', "
		SQL = SQL & "COMPATIBILITY = '" & SQLquote2(COMPATIBILITY) & "', "
		SQL = SQL & "download_URL = '" & SQLquote2(download_URL) & "', "
		SQL = SQL & "download_TEXT = '" & SQLquote2(download_TEXT) & "', "
		SQL = SQL & "POINT1 = '" & SQLquote2(POINT1) & "', "
		SQL = SQL & "POINT2 = '" & SQLquote2(POINT2) & "', "
		SQL = SQL & "POINT3 = '" & SQLquote2(POINT3) & "', "
		SQL = SQL & "POINT4 = '" & SQLquote2(POINT4) & "', "
		SQL = SQL & "POINT5 = '" & SQLquote2(POINT5) & "', "
		SQL = SQL & "POINT6 = '" & SQLquote2(POINT6) & "', "
		SQL = SQL & "POINT7 = '" & SQLquote2(POINT7) & "', "
		SQL = SQL & "POINT8 = '" & SQLquote2(POINT8) & "', "
		SQL = SQL & "POINT9 = '" & SQLquote2(POINT9) & "', "
		SQL = SQL & "POINT10 = '" & SQLquote2(POINT10) & "', "
		SQL = SQL & "FEATURE1 = '" & SQLquote2(FEATURE1) & "', "
		SQL = SQL & "FEATURE2 = '" & SQLquote2(FEATURE2) & "', "
		SQL = SQL & "FEATURE3 = '" & SQLquote2(FEATURE3) & "', "
		SQL = SQL & "FEATURE4 = '" & SQLquote2(FEATURE4) & "', "
		SQL = SQL & "FEATURE5 = '" & SQLquote2(FEATURE5) & "', "
		SQL = SQL & "FEATURE6 = '" & SQLquote2(FEATURE6) & "', "
		SQL = SQL & "FEATURE7 = '" & SQLquote2(FEATURE7) & "', "
		SQL = SQL & "FEATURE8 = '" & SQLquote2(FEATURE8) & "', "
		SQL = SQL & "FEATURE9 = '" & SQLquote2(FEATURE9) & "', "
		SQL = SQL & "FEATURE10 = '" & SQLquote2(FEATURE10) & "', "
		SQL = SQL & "Features = " & Features & ", "
		SQL = SQL & "FormFactor = '" & SQLquote2(FormFactor) & "', "
		SQL = SQL & "BandType = '" & SQLquote2(BandType) & "', "
		SQL = SQL & "Band = '" & SQLquote2(Band) & "', "
		SQL = SQL & "Smartphone = " & Smartphone & ", "
		SQL = SQL & "PackageContents = '" & SQLquote2(PackageContents) & "', "
		SQL = SQL & "TalkTime = '" & SQLquote2(TalkTime) & "', "
		SQL = SQL & "Standby = '" & SQLquote2(Standby) & "', "
		SQL = SQL & "Display = '" & SQLquote2(Display) & "', "
		SQL = SQL & "NoDiscount = " & NoDiscount & ", "
		SQL = SQL & "DateTimeEntd = '" & now & "'"
		SQL = SQL & " WHERE ItemID='" & ItemID & "'"
		session("errorSQL") = SQL
		oConn.execute SQL

		if prepStr(partNumber) <> prepStr(og_PartNumber) then
			sql = "update we_Items set partNumber = '" & partNumber & "' where partNumber = '" & og_PartNumber & "'"
			session("errorSQL") = sql
			oConn.execute(sql)

			sql = "update we_vendorNumbers set we_partNumber = '" & partNumber & "' where we_partNumber = '" & og_PartNumber & "'"
			session("errorSQL") = sql
			oConn.execute(sql)

			sql = "update we_vendorOrder set partNumber = '" & partNumber & "' where partNumber = '" & og_PartNumber & "'"
			session("errorSQL") = sql
			oConn.execute(sql)
		end if

		sql = "exec sp_createProductListByModelID " & modelID
		session("errorSQL") = sql
		oConn.execute(sql)

		sql = "insert into we_adminActions (adminID,action) values(" & adminID & ",'update item data:" & ds(itemDesc) & " (" & ItemID & ")')"
		session("errorSQL") = SQL
		oConn.execute SQL

		if itemLongDetail_ER <> "" then
			sql = "update we_items set itemDesc_ER = '" & sqlQuote(itemDesc_ER) & "', itemLongDetail_ER = '" & sqlQuote(itemLongDetail_ER) & "' where itemID = " & itemID
			session("errorSQL") = SQL
			oConn.execute SQL
		end if

		' Update these fields for ALL items with this Part Number:
		' NoDiscount
		SQL = "UPDATE we_items SET "
		SQL = SQL & "NoDiscount = " & NoDiscount
		SQL = SQL & " WHERE PartNumber = '" & Ucase(PartNumber) & "'"
		session("errorSQL") = SQL
		'response.write "<p>" & SQL & "</p>"
		oConn.execute SQL

		'safe update the ItemValue table

		if blnIsMaster then
			Sql = EMPTY_STRING '-- inner loop

			for each strItemId in GetItemByPartNumber( PartNumber).AsArray '-- outer loop
				for each strSiteId in dicItemValue.Keys
					Sql = Sql &_
					"exec sp_ModifyItemValue " &VbCrLf&_
					" "& DbId( strSiteId) &" --SiteId" &VbCrLf&_
					" ,"& DbId( strItemId) &" --ItemId" &VbCrLf&_
					" ,"& DbId( dicItemValue( strSiteId)( "ActiveItemValueTypeId")) &" --ActiveItemValueTypeId" &VbCrLf&_
					" ,"& DbNumNil( dicItemValue( strSiteId)( "OriginalPrice")) &" --OriginalPrice" &VbCrLf&_
					" ,"& DbNumNil( dicItemValue( strSiteId)( "DiscountPrice")) &" --DiscountPrice" &VbCrLf&_
					" ,"& DbNumNil( dicItemValue( strSiteId)( "DiscountPercent")) &" --DiscountPercent"&VbCrLf
				next '-- inner loop
			next '-- outer loop

		oConn.execute SQL
		end if

		response.write "<h3>Product Updated!</h3>" & vbcrlf

	' ______ ADD HANDLER __________________________________________________________________________________
	elseif frmSubmit = "Add" then
		SQL = "SET NOCOUNT ON; "
		SQL = SQL & "INSERT INTO we_items (WE_URL,brandID,modelID,typeID,carrierID,subtypeID,PartNumber,itemDesc,itemDesc_PS,itemDesc_CA,itemDesc_CO,itemDesc_ER,vendor,price_Retail,price_Our,price_CA,price_CO,price_PS,price_ER,price_Buy,COGS,"
		SQL = SQL & "numberOfSales,Seasonal,Sports,HandsfreeType,ItemKit_NEW,hotDeal,hideLive,itemLongDetail,itemInfo,itemLongDetail_PS,itemLongDetail_CA,itemLongDetail_CO,itemLongDetail_ER,UPCCode,AMZN_sku,itemWeight,itemDimensions,itemPic,itemPic_CO,flag1,"
		SQL = SQL & "BULLET1,BULLET2,BULLET3,BULLET4,BULLET5,BULLET6,BULLET7,BULLET8,BULLET9,BULLET10,COMPATIBILITY,download_URL,download_TEXT,POINT1,POINT2,POINT3,POINT4,POINT5,POINT6,POINT7,POINT8,POINT9,POINT10,"
		SQL = SQL & "FEATURE1,FEATURE2,FEATURE3,FEATURE4,FEATURE5,FEATURE6,FEATURE7,FEATURE8,FEATURE9,FEATURE10,Features,FormFactor,BandType,Band,Smartphone,PackageContents,TalkTime,Standby,Display,NoDiscount,"
		SQL = SQL & "DateTimeEntd,master,colorID,designType) VALUES ("
		SQL = SQL & "'" & id_new_field & "', "
		SQL = SQL & "'" & brandID & "', "
		SQL = SQL & "'" & modelID & "', "
		SQL = SQL & "'" & typeID & "', "
		SQL = SQL & "'" & carrierID & "', "
		SQL = SQL & "'" & subtypeID & "', "
		SQL = SQL & "'" & Ucase(PartNumber) & "', "
		SQL = SQL & "'" & SQLquote2(itemDesc) & "', "
		SQL = SQL & "'" & SQLquote2(itemDesc_PS) & "', "
		SQL = SQL & "'" & SQLquote2(itemDesc_CA) & "', "
		SQL = SQL & "'" & SQLquote2(itemDesc_CO) & "', "
		SQL = SQL & "'" & SQLquote2(itemDesc_ER) & "', "
		SQL = SQL & "'" & vendor & "', "
		SQL = SQL & "'" & price_Retail & "', "
		SQL = SQL & "'" & price_Our & "', "
		if price_CA = 0 then
			SQL = SQL & "null, "
		else
			SQL = SQL & "'" & price_CA & "', "
		end if
		if price_CO = 0 then
			SQL = SQL & "null, "
		else
			SQL = SQL & "'" & price_CO & "', "
		end if
		SQL = SQL & "'" & price_PS & "', "
		SQL = SQL & "'" & price_ER & "', "
		SQL = SQL & "'" & price_Buy & "', "
		SQL = SQL & "'" & COGS & "', "
		SQL = SQL & "0, "
		SQL = SQL & Seasonal & ", "
		SQL = SQL & "'" & Sports & "', "
		if typeID = "5" and HandsfreeTypeID <> "" then
			SQL = SQL & "'" & HandsfreeTypeID & "', "
		else
			SQL = SQL & "null, "
		end if
		if SQLquote2(ItemKit) <> "" then
			SQL = SQL & "'" & SQLquote2(ItemKit) & "', "
		else
			SQL = SQL & "null, "
		end if
		SQL = SQL & hotDeal & ", "
		SQL = SQL & hideLive & ", "
		SQL = SQL & "'" & SQLquote2(itemLongDetail) & "', "
		SQL = SQL & "'" & SQLquote2(itemInfo) & "', "
		SQL = SQL & "'" & SQLquote2(itemLongDetail_PS) & "', "
		SQL = SQL & "'" & SQLquote2(itemLongDetail_CA) & "', "
		SQL = SQL & "'" & SQLquote2(itemLongDetail_CO) & "', "
		SQL = SQL & "'" & SQLquote2(itemLongDetail_ER) & "', "
		SQL = SQL & "'" & UPCCode & "', "
		SQL = SQL & "'" & AMZN_sku & "', "
		SQL = SQL & "'" & itemWeight & "', "
		SQL = SQL & "'" & SQLquote2(itemDimensions) & "', "
		SQL = SQL & "'" & newName & "', "
		SQL = SQL & "'" & newName_CO & "', "
		SQL = SQL & "'" & flag1 & "', "
		SQL = SQL & "'" & SQLquote2(BULLET1) & "', "
		SQL = SQL & "'" & SQLquote2(BULLET2) & "', "
		SQL = SQL & "'" & SQLquote2(BULLET3) & "', "
		SQL = SQL & "'" & SQLquote2(BULLET4) & "', "
		SQL = SQL & "'" & SQLquote2(BULLET5) & "', "
		SQL = SQL & "'" & SQLquote2(BULLET6) & "', "
		SQL = SQL & "'" & SQLquote2(BULLET7) & "', "
		SQL = SQL & "'" & SQLquote2(BULLET8) & "', "
		SQL = SQL & "'" & SQLquote2(BULLET9) & "', "
		SQL = SQL & "'" & SQLquote2(BULLET10) & "', "
		SQL = SQL & "'" & SQLquote2(COMPATIBILITY) & "', "
		SQL = SQL & "'" & SQLquote2(download_URL) & "', "
		SQL = SQL & "'" & SQLquote2(download_TEXT) & "', "
		SQL = SQL & "'" & SQLquote2(POINT1) & "', "
		SQL = SQL & "'" & SQLquote2(POINT2) & "', "
		SQL = SQL & "'" & SQLquote2(POINT3) & "', "
		SQL = SQL & "'" & SQLquote2(POINT4) & "', "
		SQL = SQL & "'" & SQLquote2(POINT5) & "', "
		SQL = SQL & "'" & SQLquote2(POINT6) & "', "
		SQL = SQL & "'" & SQLquote2(POINT7) & "', "
		SQL = SQL & "'" & SQLquote2(POINT8) & "', "
		SQL = SQL & "'" & SQLquote2(POINT9) & "', "
		SQL = SQL & "'" & SQLquote2(POINT10) & "', "
		SQL = SQL & "'" & SQLquote2(FEATURE1) & "', "
		SQL = SQL & "'" & SQLquote2(FEATURE2) & "', "
		SQL = SQL & "'" & SQLquote2(FEATURE3) & "', "
		SQL = SQL & "'" & SQLquote2(FEATURE4) & "', "
		SQL = SQL & "'" & SQLquote2(FEATURE5) & "', "
		SQL = SQL & "'" & SQLquote2(FEATURE6) & "', "
		SQL = SQL & "'" & SQLquote2(FEATURE7) & "', "
		SQL = SQL & "'" & SQLquote2(FEATURE8) & "', "
		SQL = SQL & "'" & SQLquote2(FEATURE9) & "', "
		SQL = SQL & "'" & SQLquote2(FEATURE10) & "', "
		SQL = SQL & Features & ", "
		SQL = SQL & "'" & SQLquote2(FormFactor) & "', "
		SQL = SQL & "'" & SQLquote2(BandType) & "', "
		SQL = SQL & "'" & SQLquote2(Band) & "', "
		SQL = SQL & Smartphone & ", "
		SQL = SQL & "'" & SQLquote2(PackageContents) & "', "
		SQL = SQL & "'" & SQLquote2(TalkTime) & "', "
		SQL = SQL & "'" & SQLquote2(Standby) & "', "
		SQL = SQL & "'" & SQLquote2(Display) & "', "
		SQL = SQL & NoDiscount & ", "
		SQL = SQL & "'" & now & "',1,'" & colorID & "','" & artistID & "'"
		SQL = SQL & ");"  &VbCrLf

 		'update the ItemValue table (covers the cloning & add functionality) [knguyen/20110611]
		SQL = SQL & "declare @intItemId int; set @intItemId=@@IDENTITY;" &VbCrLf
		for each strSiteId in dicItemValue.Keys
			SQL = SQL &_
			"	insert into ItemValue ( " &VbCrLf&_
			"		[ActiveItemValueTypeId] " &VbCrLf&_
			"		,[SiteId] " &VbCrLf&_
			"		,[ItemId] " &VbCrLf&_
			"		,[OriginalPrice] " &VbCrLf&_
			"		,[DiscountPrice] " &VbCrLf&_
			"		,[DiscountPercent] " &VbCrLf&_
			"	) values( " &VbCrLf&_
			"		"& DbId( dicItemValue( strSiteId)( "ActiveItemValueTypeId")) &VbCrLf&_
			"		,"& DbId( strSiteId) &VbCrLf&_
			"		,@intItemId" &VbCrLf&_
			"		,"& DbNumNil( dicItemValue( strSiteId)( "OriginalPrice")) &VbCrLf&_
			"		,"& DbNumNil( dicItemValue( strSiteId)( "DiscountPrice")) &VbCrLf&_
			"		,"& DbNumNil( dicItemValue( strSiteId)( "DiscountPercent")) &VbCrLf&_
			"	)" &VbCrLf
		next

		SQL = SQL & "SELECT @intItemId as [newItemID]"


		session("errorSQL") = SQL
		'response.write "<p>" & SQL & "</p>"
		set RS = oConn.execute(SQL)
		ItemID = RS("newItemID")
		response.write "<h3>Product Added!</h3>"

		sql = "exec sp_createProductListByModelID " & modelID
		session("errorSQL") = sql
		oConn.execute(sql)

		sql = "select code, name from we_vendors order by name"
		session("errorSQL") = sql
		Set vendorRS = Server.CreateObject("ADODB.Recordset")
		vendorRS.open sql, oConn, 0, 1
%>
<form action="/admin/db_update_items_submit2.asp" method="post">
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
    	<td align="right">COGS:</td>
        <td><input type="text" name="cogs" value="1" size="3" /></td>
    </tr>
    <tr>
    	<td align="right">Initial Qty:</td>
        <td><input type="text" name="inv_qty" value="0" size="3" /></td>
    </tr>
    <tr>
    	<td align="right">Vendor:</td>
        <td>
        	<select name="vendor">
            	<option value="">Select Vendor</option>
                <%
				do while not vendorRS.EOF
				%>
                <option value="<%=vendorRS("code")%>"><%=vendorRS("name")%></option>
                <%
					vendorRS.movenext
				loop
				%>
            </select>
        </td>
    </tr>
    <tr>
    	<td align="right">Vendor Partnumber:</td>
        <td><input type="text" name="vendorPN" value="" /></td>
    </tr>
    <tr>
    	<td colspan="2" align="right">
        	<input type="submit" name="mySub" value="Update Product" />
            <input type="hidden" name="ItemID" value="<%=ItemID%>" />
            <input type="hidden" name="wePN" value="<%=Ucase(PartNumber)%>" />
            <input type="hidden" name="updateNewProd" value="1" />
        </td>
    </tr>
</table>
</form>
<%
	end if

	'Handle showAnimation form value
	if frmSubmit = "Edit" or frmSubmit = "Add" then
		if showAnimation = "inputShowAnimation" then 'Value passed from form's input type checkbox is inputShowAnimation
			showAnimationBit = 1 'If execution reaches this point, showAnimation is true and SQL expects a 1
		else
			showAnimationBit = 0 'Value is false, SQL expects 0
		end if
		SQL = "if (select COUNT( * ) from we_ItemsExtendedData where partNumber = '" & partNumber & "') > 0 update we_ItemsExtendedData set showAnimation = " & showAnimationBit & ", customize = " & prepInt(customize) & " where partNumber = '" & partNumber & "' else insert into we_ItemsExtendedData (partNumber,showAnimation,customize) values('" & partNumber & "'," & showAnimationBit & "," & prepInt(customize) & ")"
		session("errorSQL") = SQL
		oconn.execute SQL
	end if

	RS.close
	set RS = nothing

	response.write "<p><a href=""/admin/db_search_brands.asp?searchID=4"">Back to Add/Edit Product.</a></p>" & vbcrlf
else
	response.write "<h3><font color=""#FF0000"">Please <a href=""javascript:history.back();"">go back</a> and correct the following errors:</font></h3>" & vbcrlf
	response.write "<ul>" & strError & "</ul>" & vbcrlf
end if

function trueFalse(val)
	if val = "1" then
		trueFalse = -1
	else
		trueFalse = 0
	end if
end function

function changeTypeName(stypeName)
	select case stypeName
		case "Antennas/Parts"
			changeTypeName = "Antennas"
		case "Batteries"
			changeTypeName = "Batteries"
		case "Bling Kits & Charms"
			changeTypeName = "Blings & Charms"
		case "Chargers"
			changeTypeName = "Chargers"
		case "Data Cable & Memory"
			changeTypeName = "Data Cables/Memory"
		case "Faceplates"
			changeTypeName = "Faceplates/Covers"
		case "Hands-Free"
			changeTypeName = "Headsets/Bluetooth"
		case "Holsters/Belt Clips"
			changeTypeName = "Holsters"
		case "Key Pads/Flashing"
			changeTypeName = "Colorful Keypads"
		case "Leather Cases"
			changeTypeName = "Cases & Pouches"
		case "Cell Phones"
			changeTypeName = "Unlocked Cell Phones"
		case else
			changeTypeName = "Universal Accessories"
	end select
end function

'function SQLquote2(strValue)
'	if not isNull(strValue) and strValue <> "" then
'		SQLquote2 = trim(replace(strValue,"'","''"))
'		SQLquote2 = replace(SQLquote2,chr(34),"''''")
'		SQLquote2 = replace(SQLquote2,vbcrlf," ")
'		SQLquote2 = replace(SQLquote2,vbtab," ")
'	else
'		SQLquote2 = strValue
'	end if
'end function

function SQLquote2(strValue)
	retValue = strValue
	if not isNull(retValue) and retValue <> "" then
		if instr(retValue,"'") > 0 then
			retValue = trim(replace(retValue,"'","''"))
		end if
		retValue = replace(retValue,vbcrlf," ")
		retValue = replace(retValue,vbtab," ")
	else
		retValue = retValue
	end if
	SQLquote2 = retValue
end function

sub uploadImagesRemote(remoteDir, localFilePath, remoteFileName)
	if useFTP then
		success = ftpWEB2.ChangeRemoteDir(remoteDir)
		success = ftpWEB2.PutFile(localFilePath, remoteFileName)

		success = ftpWEB3.ChangeRemoteDir(remoteDir)
		success = ftpWEB3.PutFile(localFilePath, remoteFileName)
	end if
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
