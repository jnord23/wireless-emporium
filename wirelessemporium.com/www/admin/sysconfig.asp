<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - General Sites Configuration"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
siteID = prepInt(request("cbSite"))
	
if request.form("submitted") <> "" then
	newConfigValue = prepStr(request.form("newConfigValue"))
	newConfigName = prepStr(request.form("newConfigName"))

	sql	=	"insert into we_config(siteid, configName, configValue, adminUser)" & vbcrlf & _
			"values(" & siteID & ", '" & newConfigName & "', '" & newConfigValue & "', '" & session("adminID") & "')"
	oConn.execute(sql)
end if


%>
<form name="frmPhoneOrder" method="post">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="1000">
	<tr>
    	<td width="100%" align="left">
        	<div class="left">
            	<div style="float:left; width:300px;"><h1 style="font-size:20px;">General Sites Configuration</h1></div>
                <div style="float:right; width:700px;">
					<div style="float:right; width:4px; height:44px;"><img src="/images/admin/phoneorder/header_right.png" border="0" width="4" height="44" /></div>
                    <div style="float:right; width:130px; height:44px; background-image:url(/images/admin/phoneorder/header_middle.png);">
                        <div style="float:right; font-size:14px; color:#FFF; padding:15px 0px 0px 15px; font-weight:bold;">
                            <select name="cbSite" onChange="document.frmPhoneOrder.submit();">
                                <option value="0" <%if siteID="0" then%>selected<%end if%> >WE</option>
                                <option value="2" <%if siteID="2" then%>selected<%end if%> >CO</option>
                                <option value="1" <%if siteID="1" then%>selected<%end if%> >CA</option>
                                <option value="3" <%if siteID="3" then%>selected<%end if%> >PS</option>
                                <option value="10" <%if siteID="10" then%>selected<%end if%> >TM</option>
                            </select>
                        </div>
                        <div style="float:left; font-size:14px; color:#FFF; padding:15px 0px 0px 10px; font-weight:bold;">
                            SITE:
                        </div>
                    </div>
                    <div style="float:right; width:4px; height:44px;"><img src="/images/admin/phoneorder/header_left.png" border="0" width="4" height="44" /></div>
                </div>
            </div>
            <div class="clear">&nbsp;</div>
        	<div class="left w100">
				<div style="float:left; width:100%; padding:5px 0px 5px 0px; border:1px solid #c1e0b2; border-radius:3px; background-color:#eaf5e5;">
					<div style="float:left; width:82px; font-size:13px; font-weight:bold; text-align:left;">
                    	<div class="filter-box-header" style="width:60px;">Config ID</div>
					</div>
					<div style="float:left; width:202px; font-size:13px; font-weight:bold; text-align:left;">
                    	<div class="filter-box-header" style="width:180px;">Config Name</div>
					</div>
					<div style="float:left; width:202px; font-size:13px; font-weight:bold; text-align:left;">
                    	<div class="filter-box-header" style="width:180px;">Config Value</div>
					</div>
					<div style="float:left; width:202px; font-size:13px; font-weight:bold; text-align:left;">
                    	<div class="filter-box-header" style="width:180px;">Last Update</div>
					</div>
				</div>
                <div class="clear"></div>
			<%
			sql = 	"select	a.id, a.siteid, a.configName, a.configValue, a.lastUpdate, b.fname, b.lname" & vbcrlf & _
					"from	we_config a left outer join we_adminusers b" & vbcrlf & _
					"	on	a.adminUser = b.adminID" & vbcrlf & _
					"where	a.siteid = " & siteID & vbcrlf & _					
					"order by 1"
			lap = 0
			set rs = oConn.execute(sql)
			do until rs.eof
				lap = lap + 1			
				rowBackgroundColor = "#fff"
				if (lap mod 2) = 0 then rowBackgroundColor = "#eaf5fa"
			%>
				<div class="left w100" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;">
                	<div class="left" style="margin-left:5px; padding:5px 0px 0px 5px; width:80px; text-align:center;"><%=rs("id")%></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:200px;"><%=rs("configName")%></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:200px;"><input type="text" name="txtConfigValue" value="<%=rs("configValue")%>" id="cfgval_<%=rs("id")%>" /></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:200px;"><%=rs("lastUpdate")%><br /><%=rs("fname")%>&nbsp;<%=rs("lname")%></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:100px;"><input type="button" name="btnEdit" value="UPDATE" onClick="edit(<%=rs("id")%>);" id="btn_<%=rs("id")%>" /></div>
				</div>
			<%
				rs.movenext
			loop
			lap = lap + 1			
			rowBackgroundColor = "#fff"
			if (lap mod 2) = 0 then rowBackgroundColor = "#eaf5fa"
			%>
				<div class="left w100" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;">
                	<div class="left" style="margin-left:5px; padding:5px 0px 0px 5px; width:80px;">&nbsp;</div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:200px;"><input type="text" name="newConfigName" value="" /></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:200px;"><input type="text" name="newConfigValue" value="" /></div>
                	<div class="left" style="padding:5px 0px 0px 5px; width:200px;"><input type="submit" name="submitted" value="Add New Value" /></div>
				</div>
            </div>
		</td>
    </tr>
	
</table>
</form>
<script>
	function edit(id) {
		var cfgval = document.getElementById('cfgval_'+id).value;
		ajax("/ajax/admin/ajaxSysConfig.asp?configID=" + id + "&cfgval=" + cfgval,"dumpZone");
		document.getElementById('btn_'+id).value = 'UPDATED';
		document.getElementById('btn_'+id).disabled = true;
		setTimeout("doneUpdates('" + id + "')",2000);
	}
	
	function doneUpdates(id) {
		document.getElementById('btn_'+id).value = 'UPDATE';
		document.getElementById('btn_'+id).disabled = false;
	}
	
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
