<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Assign ModelID"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim sql, rs, modelArray, curArray
	dim curBrand : curBrand = 0
	dim curModels : curModels = ""
	
	sql = "select distinct brand, brandID, model from we_items_musicSkins where skip = 0 and deleteItem = 0 and brandID is not null and modelID is null order by brandID, model"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
%>
<table border="1" bordercolor="#333333" cellpadding="3" cellspacing="0" align="center">
	<tr bgcolor="#000000" style="color:#FFF; font-weight:bold;">
    	<td>Brand</td>
        <td>Model Name</td>
        <td>WE Model</td>
        <td>WE Alt Model</td>
        <td>Unsupported</td>
    </tr>
    <%
	bgColor = "#ffffff"
	lap = 0
	do while not rs.EOF
		lap = lap + 1
		if cdbl(rs("brandID")) <> curBrand then
			curBrand = cdbl(rs("brandID"))
			curModels = ""
			sql = "select modelID, modelName from we_models where brandID = " & curBrand & " order by modelName"
			session("errorSQL") = sql
			set modelRS = oConn.execute(sql)
			
			do while not modelRS.EOF
				curModels = curModels & modelRS("modelID") & "##" & modelRS("modelName") & "@@"
				modelRS.movenext
			loop
			modelArray = split(curModels,"@@")
		end if
	%>
    <tr bgcolor="<%=bgColor%>" id="modelLap_<%=lap%>">
    	<td><%=rs("brand")%></td>
        <td><%=rs("model")%></td>
        <td>
        	<select name="modelID" onchange="saveModel('prim',this.value,'<%=rs("model")%>')">
            	<option value="">Select WE Model</option>
                <%
				for i = 0 to (ubound(modelArray) - 1)
					curArray = split(modelArray(i),"##")
				%>
                <option value="<%=curArray(0)%>"><%=curArray(1)%></option>
                <%
				next
				%>
            </select>
        </td>
        <td>
        	<select name="altModelID" onchange="saveModel('alt',this.value,'<%=rs("model")%>')">
            	<option value="">Select WE Alt Model</option>
                <%
				for i = 0 to (ubound(modelArray) - 1)
					curArray = split(modelArray(i),"##")
				%>
                <option value="<%=curArray(0)%>"><%=curArray(1)%></option>
                <%
				next
				%>
            </select>
        </td>
        <td><input type="button" name="myAction" value="Unsupported" onclick="hideModel('modelLap_<%=lap%>','<%=rs("model")%>')" /></td>
    </tr>
    <%
		rs.movenext
		if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
	loop
	%>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function saveModel(which,modelID,modelName) {
		ajax('/ajax/admin/ajaxMusicSkins_assign.asp?modelType=' + which + '&modelID=' + modelID + '&modelName=' + modelName.replace('+','%2B'),'dumpZone')
		setTimeout("alert(document.getElementById('dumpZone').innerHTML)",1000)
	}
	function hideModel(lap,modelName) {
		ajax('/ajax/admin/ajaxMusicSkins_assign.asp?skipModel=1&modelName=' + modelName.replace('+','%2B'),'dumpZone')
		document.getElementById(lap).style.display = 'none'
		setTimeout("alert(document.getElementById('dumpZone').innerHTML)",1000)
	}
</script>