<%
	thisUser = Request.Cookies("username")
	pageTitle = ""
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	sql = "select (select COUNT(*) from we_accounts where personalCode is not null) as totalCodes, a.promoCode, b.ordersubtotal from we_coupons a with (nolock) join we_orders b with (nolock) on a.couponid = b.couponid and a.cashBack = 1"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	sql = "select COUNT(*) as total, (select COUNT(*) from we_orders where couponid in (select couponid from we_coupons where couponDesc = 'CountOff')) as usedCodes from we_coupons where oneTime = 1 and couponDesc = 'CountOff'"
	session("errorSQL") = sql
	set countOffRS = oConn.execute(sql)
	
	dim lap : lap = 0
	dim couponCodes : couponCodes = ""
	dim subTotals : subTotals = ""
	dim uniqueCodes : uniqueCodes = 0
	dim totalCodes : totalCodes = 0
	do while not rs.EOF
		lap = lap + 1
		if lap = 1 then totalCodes = rs("totalCodes")
		if instr(couponCodes,rs("promoCode")) < 1 then uniqueCodes = uniqueCodes + 1
		couponCodes = couponCodes & rs("promoCode") & "##"
		subTotals = subTotals & rs("ordersubtotal") & "##"
		rs.movenext
	loop
	
	dim codeArray : codeArray = split(left(couponCodes,len(couponCodes)-2),"##")
	dim totalArray : totalArray = split(left(subTotals,len(subTotals)-2),"##")
	dim max : max = ubound(codeArray)
	dim TemporalVariable, i, j
	For i = 0 to max 
	   For j = i + 1 to max 
		  if codeArray(i) > codeArray(j) then
			  TemporalVariable = codeArray(i)
			  codeArray(i) = codeArray(j)
			  codeArray(j) = TemporalVariable
			  
			  TemporalVariable = totalArray(i)
			  totalArray(i) = totalArray(j)
			  totalArray(j) = TemporalVariable
		 end if
	   next 
	next
	
	dim curCode : curCode = ""
	dim curTotal : curTotal = ""
	dim used : used = 0
	dim finalCodes : finalCodes = ""
	dim finalTotal : finalTotal = ""
	dim finalCnt : finalCnt = ""
	dim fCodeArray, fTotalArray, fCntArray
	for i = 0 to ubound(codeArray)
		if codeArray(i) <> curCode then
			if curCode <> "" then
				finalCodes = finalCodes & curCode & "##"
				finalTotal = finalTotal & curTotal & "##"
				finalCnt = finalCnt & used & "##"
			end if
			curCode = codeArray(i)
			curTotal = cdbl(totalArray(i))
			used = 1
		else
			curTotal = curTotal + cdbl(totalArray(i))
			used = used + 1
		end if
	next
	finalCodes = finalCodes & curCode & "##"
	finalTotal = finalTotal & curTotal & "##"
	finalCnt = finalCnt & used & "##"
	
	fCodeArray = split(left(finalCodes,len(finalCodes)-2),"##")
	fTotalArray = split(left(finalTotal,len(finalTotal)-2),"##")
	fCntArray = split(left(finalCnt,len(finalCnt)-2),"##")
%>
<table border="0" cellpadding="3" cellspacing="0" width="600" align="center">
    <tr><td style="border-top:1px solid #000; font-weight:bold; font-size:18px; border-bottom:1px solid #000; background-color:#CFF;">Share &amp; Earn</td></tr>
    <tr>
    	<td>
        	<div style="font-weight:bold; float:left;">Total Codes Generated:</div>
            <div style="float:left; margin-left:10px;"><%=formatNumber(totalCodes,0)%></div>
        </td>
    </tr>
    <tr>
    	<td>
        	<div style="font-weight:bold; float:left;">Total Codes Used:</div>
            <div style="float:left; margin-left:10px;"><%=formatNumber(uniqueCodes,0)%></div>
        </td>
    </tr>
    <tr>
    	<td style="padding-bottom:20px;">
        	<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0">
            	<tr bgcolor="#333333" style="font-weight:bold; color:#FFF;">
                	<td>Coupon Code</td>
                    <td>Used</td>
                    <td>SubTotal</td>
                </tr>
                <%
				bgColor = "#ffffff"
				for i = 0 to ubound(fCodeArray)
				%>
                <tr bgcolor="<%=bgColor%>">
                	<td align="left"><%=fCodeArray(i)%></td>
                    <td align="right"><%=fCntArray(i)%></td>
                    <td align="right"><%=formatCurrency(fTotalArray(i),2)%></td>
                </tr>
                <%
					if bgColor = "#ffffff" then bgColor = "#ebebeb" else bgColor = "#ffffff"
				next
				%>
            </table>
        </td>
    </tr>
    <tr><td style="border-top:1px solid #000; font-weight:bold; font-size:18px; border-bottom:1px solid #000; background-color:#CFF;">Count Off Codes</td></tr>
    <tr>
    	<td>
        	<div style="font-weight:bold; float:left;">Total Codes Generated:</div>
            <div style="float:left; margin-left:10px;"><%=formatNumber(countOffRS("total"),0)%></div>
        </td>
    </tr>
    <tr>
    	<td>
        	<div style="font-weight:bold; float:left;">Total Codes Used:</div>
            <div style="float:left; margin-left:10px;"><%=formatNumber(countOffRS("usedCodes"),0)%></div>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
