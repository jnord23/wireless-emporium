<%
	thisUser = Request.Cookies("username")
	pageTitle = "New Product Quick Create"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	dim jpeg : set jpeg = Server.CreateObject("Persits.Jpeg")
	
	session("prodList") = null
	
	function removeBrandModel(val)
		val = replace(val,"Apple","XXX")
		val = replace(val,"iPhone 4","YYY")
		removeBrandModel = val
	end function
	
	sql = "select id, name, code from we_vendors order by name"
	session("errorSQL") = sql
	set vendorRS = oConn.execute(sql)
	
	sql = "select brandID, brandName from we_Brands order by brandName"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	sql = "select typeID, typeName from we_types order by typeName"
	session("errorSQL") = sql
	set catRS = oConn.execute(sql)
%>
<table border="0" cellpadding="3" cellspacing="0" width="700" align="center">
    <tr><td></td></tr>
    <tr>
    	<td>
        	<form name="createForm" onsubmit="return(false)">
            <div style="float:left; width:420px;">
            	<div style="float:left; width:420px;">
	                <div style="float:left; width:130px; font-weight:bold; text-align:right;">Select Template:</div>
    	            <div style="float:left; width:260px; text-align:left; margin-left:10px;">
        	        	<select name="templateCode" onchange="resetForm(this.value)" style="width:200px;">
                        	<option value="GEN">Generic</option>
                            <option value="BAT">BAT</option>
                            <option value="FP2">FP2</option>
                            <option value="FP3">FP3</option>
                            <option value="FP5">FP5</option>
                            <option value="FPC">FPC Slim Snap-On Case</option>
                            <option value="HST">HST</option>
                            <option value="LC5">LC5</option>
                            <option value="SCR">SCR</option>
                            <option value="SCR Supreme Guardz">SCR Supreme Guardz</option>
                            <option value="SCR Tempered Glass">SCR Tempered Glass</option>
                            <option value="TPU">TPU</option>
                            <option value="TPU Crystal Skin w/Stand">TPU Crystal Skin w/Stand</option>
                            <option value="TRIAD">TRIAD</option>
                            <option value="TRI">TRI Aegis Series Protective Case w/Screen Protector</option>
                            <option value="WLT">WLT Goospery Fancy Diary Wallet Case by Mercury</option>
                            <optgroup label="FPX">
	                            <option value="FPX My.Carbon Armor">FPX My.Carbon Armor</option>
    	                        <option value="FPX Rubberized Snap-On">FPX Rubberized Snap-On</option>
                                <option value="FPX Protex Rubberized">FPX Protex Rubberized Snap-On Case and Heavy-Duty Holster Combo w/Screen Protector</option>
                            </optgroup>
                            <optgroup label="LC7">
                                <option value="LC7 Heavy-Duty Dual Layer">LC7 Heavy-Duty Dual Layer</option>
                                <option value="LC7 Perforated Armor">LC7 Perforated Armor</option>
                                <option value="LC7 Defense Duo">LC7 Defense Duo</option>
                                <option value="LC7 Heavy-Duty Dual Layer w/ Kickstand">LC7 Heavy-Duty Dual Layer w/ Kickstand</option>
                                <option value="LC7 CarbonShield">LC7 CarbonShield</option>
                                <option value="LC7 Armor Guard Hybrid Case w/Kickstand">LC7 Armor Guard Hybrid Case w/Kickstand</option>
                                <option value="LC7 Duo Shield Armor Case">LC7 Duo Shield Armor Case</option>
                                <option value="LC7 Tri Shield Hybrid">LC7 Tri Shield Hybrid Case w/Kickstand and Screen Protector</option>
                                <option value="LC7 Goospery Focus Bumper">LC7 Goospery Focus Bumper</option>
                                <option value="LC7 3-In-1 ArmorCase w/ Kickstand">LC7 3-In-1 ArmorCase w/ Kickstand</option>
                                <option value="LC7 Textured w/ Kickstand">LC7 Textured w/ Kickstand</option>
                            </optgroup>
                	    </select>
	                </div>
                </div>
                <div id="genCatBox" style="float:left; width:420px; padding-top:10px; display:none;">
	                <div style="float:left; width:130px; font-weight:bold; text-align:right;">Select Category:</div>
    	            <div style="float:left; width:260px; text-align:left; margin-left:10px;">
        	        	<select name="genTypeID">
                        	<option value="3" selected="selected">Faceplates</option>
                            <option value="7">Leather Cases</option>
                            <option value="13">Data Cable & Memory</option>
                            <option value="18">Screen Protectors</option>
                            <option value="">---------------</option>
							<%
							do while not catRS.EOF
							%>
                            <option value="<%=catRS("typeID")%>"><%=catRS("typeName")%></option>
                            <%
								catRS.movenext
							loop
							%>
                	    </select>
	                </div>
                    <div style="float:left; width:130px; font-weight:bold; text-align:right; margin-top:10px;">Brief Description:</div>
    	            <div style="float:left; width:260px; text-align:left; margin-left:10px; margin-top:10px; border-right:1px solid #F00;">
        	        	<input type="text" name="genDesc" value="My name is Chuck and I like peanut butter" size="60" />
	                </div>
                </div>
                <div id="subTypeBox" style="float:left; width:420px; padding-top:10px; display:none;">
	                <div style="float:left; width:130px; font-weight:bold; text-align:right;">Select Type:</div>
    	            <div style="float:left; width:260px; text-align:left; margin-left:10px;">
        	        	<select name="subType" onchange="subTypeSelect()">
                        	<option value="0">All Types</option>
                            <option value="1">Standard</option>
                            <option value="2">Argyle</option>
                            <option value="3">Windowed</option>
                	    </select>
	                </div>
                </div>
                <div style="float:left; width:420px; padding-top:10px;">
	                <div style="float:left; width:130px; font-weight:bold; text-align:right;">Select Brand:</div>
    	            <div style="float:left; width:260px; text-align:left; margin-left:10px;">
        	        	<select name="brandID" onchange="selectBrand(this.value)">
            	        	<option value="9">Samsung</option>
                            <option value="17">Apple</option>
                            <option value="4">LG</option>
                            <option value="5">Motorola</option>
                            <option value="7">Nokia</option>
                            <option value="30">ZTE</option>
                            <option value="">----------------</option>
							<% do while not rs.EOF %>
                    		<option value="<%=rs("brandID")%>"><%=rs("brandName")%></option>
                        	<%
								rs.movenext
							loop
							%>
        	            </select>
            	    </div>
                </div>
                <div style="float:left; width:420px; padding-top:10px;">
	                <div style="float:left; width:130px; font-weight:bold; text-align:right;">Brand Code:</div>
    	            <div style="float:left; width:260px; text-align:left; margin-left:10px;" id="enterBrandCode"></div>
                </div>
                <div style="float:left; width:420px; padding-top:10px;">
	                <div style="float:left; width:130px; font-weight:bold; text-align:right;">Select Model:</div>
    	            <div style="float:left; width:260px; text-align:left; margin-left:10px;" id="selectModel"></div>
                </div>
                <div style="float:left; width:420px; padding-top:10px;">
	                <div style="float:left; width:130px; font-weight:bold; text-align:right;">Model Code:</div>
    	            <div style="float:left; width:260px; text-align:left; margin-left:10px;" id="modelCode"></div>
                </div>
                <div style="float:left; width:420px; padding-top:10px;">
	                <div style="float:left; width:130px; font-weight:bold; text-align:right;">COGS:</div>
    	            <div style="float:left; width:260px; text-align:left; margin-left:10px;"><input type="text" name="cogs" value="" size="4" /></div>
                </div>
                <div style="float:left; width:420px; padding-top:10px;">
	                <div style="float:left; width:130px; font-weight:bold; text-align:right;">Select Vendor:</div>
    	            <div style="float:left; width:260px; text-align:left; margin-left:10px;">
        	        	<select name="vendorCode">
                        	<option value="HR">Highest Rated</option>
                            <option value="EW">Emax Wireless Inc</option>
                            <option value="BT">Balaji Trading Inc</option>
                            <option value="BC">Beyond Cell Int'l Inc</option>
                            <option value="AW">Aimo Wireless</option>
                            <option value="CT">CellTeck Inc</option>
                            <option value="DW">Dream Wireless</option>
                            <option value="">------------------</option>
							<% do while not vendorRS.EOF %>
                            <option value="<%=vendorRS("code")%>"><%=vendorRS("name")%> (<%=vendorRS("code")%>)</option>
                            <%
								vendorRS.movenext
							loop
							%>
                	    </select>
	                </div>
                </div>
                <div style="float:left; width:420px; text-align:right; padding-top:10px;">
                	<input type="button" name="myAction" value="Create Product" disabled="disabled" onclick="processProds()" />
                    <input type="hidden" name="prodList" value="" />
                </div>
            </div>
            </form>
        </td>
    </tr>
    <tr><td><div style="height:300px; overflow:auto; display:none; border:1px solid #000;" id="newProdChart"></div></td></tr>
    <tr>
    	<td id="addNewDesign" style="display:none;">
    		<form action="#" method="post" name="newDesignForm" style="margin:0px; padding:0px;" onsubmit="return(false)">
        		<div style="float:left;">Add New Design:</div>
	            <div style="float:left; padding-left:10px;"><input type="text" name="newDesc" value="" size="40" /></div>
    	        <div style="float:left; padding-left:10px;">
                	<input type="button" name="mySub" value="Add Design" onclick="addDesign()" />
                    <input type="hidden" name="prodCode" value="" />
                </div>
            </form>
        </td>
    </tr>
    <tr><td id="results"></td></tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	var prodList = "";
	resetForm("GEN");
	selectBrand(9);
	setTimeout("modelSelect(9,1674)",1000);
	
	function deleteItem(designID) {
		var yourstate=window.confirm("Are you sure you want to delete this product?")
		if (yourstate) {
			ajax('/ajax/admin/ajaxProductQuickCreate.asp?deleteDesign=' + designID,'dumpZone')
			document.getElementById("designRow_" + designID).style.display = 'none'
		}
	}
	
	function addDesign() {
		var itemDesc = document.newDesignForm.newDesc.value
		var prodCode = document.newDesignForm.prodCode.value
		var brandID = document.createForm.brandID.value
		var modelID = document.createForm.modelID.value
		ajax('/ajax/admin/ajaxProductQuickCreate.asp?prodChart=' + prodCode + '&brandID=' + brandID + '&modelID=' + modelID + '&newDesign=' + itemDesc + '&prodCode=' + prodCode,'newProdChart')
		document.newDesignForm.newDesc.value = ""
	}
	
	function modelSelect(brandID,modelID) {
		ajax('/ajax/admin/ajaxProductQuickCreate.asp?brandID=' + brandID + '&modelID=' + modelID,'modelCode')
		setTimeout("readyToProcess()",500)
		var showList = 0;
		var curTemplate = document.createForm.templateCode.value;
		if (curTemplate.substring(0,4) == "LC7 " || curTemplate.substring(0,4) == "TPU " || curTemplate.substring(0,4) == "FPX " || curTemplate.substring(0,3) == "SCR") { showList = 1 }
		if (curTemplate == "TRI" || curTemplate == "WLT") { showList = 1 }
		
		if (curTemplate == "FP2" || showList == 1) {
			document.newDesignForm.prodCode.value = curTemplate
			document.getElementById("newProdChart").style.display = ''
			ajax('/ajax/admin/ajaxProductQuickCreate.asp?prodChart=' + curTemplate + '&brandID=' + brandID + '&modelID=' + modelID,'newProdChart')
			document.getElementById("addNewDesign").style.display = ''
		}
		else if (curTemplate == "GEN") {
			document.newDesignForm.prodCode.value = curTemplate
			document.getElementById("newProdChart").style.display = ''
			ajax('/ajax/admin/ajaxProductQuickCreate.asp?prodChart=' + curTemplate + '&brandID=' + brandID + '&modelID=' + modelID,'newProdChart')
			document.getElementById("addNewDesign").style.display = ''
		}
		else if (curTemplate == "FP5") {
			document.newDesignForm.prodCode.value = "FP5"
			document.getElementById("newProdChart").style.display = ''
			ajax('/ajax/admin/ajaxProductQuickCreate.asp?prodChart=' + curTemplate + '&brandID=' + brandID + '&modelID=' + modelID,'newProdChart')
			document.getElementById("addNewDesign").style.display = ''
		}
		else if (curTemplate == "TPU") {
			document.newDesignForm.prodCode.value = "TPU"
			subType = document.createForm.subType.value
			document.getElementById("newProdChart").style.display = ''
			//document.getElementById("testZone").innerHTML = '/ajax/admin/ajaxProductQuickCreate.asp?prodChart=' + curTemplate + '&brandID=' + brandID + '&modelID=' + modelID + '&subType=' + subType
			ajax('/ajax/admin/ajaxProductQuickCreate.asp?prodChart=' + curTemplate + '&brandID=' + brandID + '&modelID=' + modelID + '&subType=' + subType,'newProdChart')
			document.getElementById("addNewDesign").style.display = ''
		}
		else if (curTemplate == "FP3") {
			document.newDesignForm.prodCode.value = "FP3"
			subType = document.createForm.subType.value
			document.getElementById("newProdChart").style.display = ''
			//document.getElementById("testZone").innerHTML = '/ajax/admin/ajaxProductQuickCreate.asp?prodChart=' + curTemplate + '&brandID=' + brandID + '&modelID=' + modelID + '&subType=' + subType
			ajax('/ajax/admin/ajaxProductQuickCreate.asp?prodChart=' + curTemplate + '&brandID=' + brandID + '&modelID=' + modelID + '&subType=' + subType,'newProdChart')
			document.getElementById("addNewDesign").style.display = ''
		}
		else if (curTemplate == "LC5") {
			document.newDesignForm.prodCode.value = "LC5"
			subType = document.createForm.subType.value
			document.getElementById("newProdChart").style.display = ''
			//document.getElementById("testZone").innerHTML = '/ajax/admin/ajaxProductQuickCreate.asp?prodChart=' + curTemplate + '&brandID=' + brandID + '&modelID=' + modelID + '&subType=' + subType
			ajax('/ajax/admin/ajaxProductQuickCreate.asp?prodChart=' + curTemplate + '&brandID=' + brandID + '&modelID=' + modelID + '&subType=' + subType,'newProdChart')
			document.getElementById("addNewDesign").style.display = ''
		}
		else if (curTemplate == "TRIAD") {
			document.newDesignForm.prodCode.value = "TRIAD"
			subType = document.createForm.subType.value
			document.getElementById("newProdChart").style.display = ''
			//document.getElementById("testZone").innerHTML = '/ajax/admin/ajaxProductQuickCreate.asp?prodChart=' + curTemplate + '&brandID=' + brandID + '&modelID=' + modelID + '&subType=' + subType
			ajax('/ajax/admin/ajaxProductQuickCreate.asp?prodChart=' + curTemplate + '&brandID=' + brandID + '&modelID=' + modelID + '&subType=' + subType,'newProdChart')
			document.getElementById("addNewDesign").style.display = ''
		}
		else {
			document.getElementById("newProdChart").style.display = 'none'
			document.getElementById("addNewDesign").style.display = 'none'
		}
	}
	
	function saveProdID(checked,prodID) {
		var curList = document.createForm.prodList.value
		if (checked) {
			//ajax('/ajax/admin/ajaxProductQuickCreate.asp?saveProdID=' + prodID,'testZone')
			curList = curList + prodID + ","
			document.createForm.prodList.value = curList
		}
		else {
			//ajax('/ajax/admin/ajaxProductQuickCreate.asp?removeProdID=' + prodID,'testZone')
			curList = curList.replace(prodID + ",","")
			document.createForm.prodList.value = curList
		}
	}
	
	function saveModelCode(brandID,modelID,modelCode) {
		if (modelCode.length == 4) {
			ajax('/ajax/admin/ajaxProductQuickCreate.asp?brandID=' + brandID + '&modelID=' + modelID + '&saveModelCode=' + modelCode,'dumpZone')
			setTimeout("readyToProcess()",500)
		}
		else {
			alert("The Model Code Must Be 4 Characters Long")
		}
	}

	function readyToProcess() {
		var modelCode = document.createForm.modelCode.value
		
		if (modelCode == "") {
			document.createForm.myAction.disabled = true
		}
		else {
			document.createForm.myAction.disabled = false
		}
	}
	
	function selectBrand(brandID) {
		ajax('/ajax/admin/ajaxProductQuickCreate.asp?getBrandCode=' + brandID,'enterBrandCode')
		ajax('/ajax/admin/ajaxProductQuickCreate.asp?brandID=' + brandID,'selectModel')
		document.getElementById("newProdChart").style.display = 'none'
		document.getElementById("addNewDesign").style.display = 'none'
		
		if (brandID == 9) {
			setTimeout("modelSelect(9,1674)",1000);
		}
		else if (brandID == 17) {
			setTimeout("modelSelect(17,1628)",1000);
		}
		else if (brandID == 4) {
			setTimeout("modelSelect(4,1608)",1000);
		}
		else if (brandID == 5) {
			setTimeout("modelSelect(5,1658)",1000);
		}
		else if (brandID == 7) {
			setTimeout("modelSelect(7,1632)",1000);
		}
	}
	
	function processProds() {
		var templateCode = document.createForm.templateCode.value
		var genTypeID = document.createForm.genTypeID.value
		var genDesc = escape(document.createForm.genDesc.value)
		var brandID = document.createForm.brandID.value
		var modelID = document.createForm.modelID.value
		var modelCode = document.createForm.modelCode.value
		var brandCode = document.createForm.brandCode.value
		var vendorCode = document.createForm.vendorCode.value
		var prodList = document.createForm.prodList.value
		var cogs = document.createForm.cogs.value
		
		document.getElementById("results").innerHTML = "<div style='font-size:18px; font-weight:bold; margin-left:auto; margin-right:auto;'>Processing Request<br />Please Wait...</div>"
		ajax('/ajax/admin/ajaxProductQuickCreate.asp?prodList=' + prodList + '&templateCode=' + templateCode + '&brandID=' + brandID + '&brandCode=' + brandCode + '&modelID=' + modelID + '&pnModel=' + modelCode + '&vendorCode=' + vendorCode + '&defaultCogs=' + cogs + '&genTypeID=' + genTypeID + '&genDesc=' + genDesc,'results')
		resetForm("GEN");
	}
	
	function resetForm(tempCode) {
		document.createForm.brandID.selectedIndex = 0
		document.createForm.vendorCode.selectedIndex = 0
		document.createForm.genTypeID.selectedIndex = 0
		//document.createForm.genDesc.value = ''
		document.getElementById("enterBrandCode").innerHTML = ''
		document.getElementById("selectModel").innerHTML = ''
		document.getElementById("modelCode").innerHTML = ''
		document.createForm.myAction.disabled = true
		document.getElementById("newProdChart").style.display = 'none'
		document.getElementById("addNewDesign").style.display = 'none'
		document.createForm.prodList.value = ''
		
		var selectedType = document.createForm.templateCode.options[document.createForm.templateCode.selectedIndex].value
		
		if (selectedType == "TPU") {
			document.getElementById("subTypeBox").style.display = ''
			document.getElementById("genCatBox").style.display = 'none'
		}
		else if (tempCode == "GEN") {
			document.getElementById("genCatBox").style.display = ''
			document.getElementById("subTypeBox").style.display = 'none'
		}
		else {
			document.getElementById("genCatBox").style.display = 'none'
			document.getElementById("subTypeBox").style.display = 'none'
		}
		selectBrand(9);
		setTimeout("modelSelect(9,1674)",1000);
	}
	
	function addVendor(itemID) {
		var vendorID = eval("document.vendorData_" + itemID + ".vendorID.value")
		var cogs = eval("document.vendorData_" + itemID + ".cogs.value")
		var vendorCode = eval("document.vendorData_" + itemID + ".vendorCode.value")
		var newPN = eval("document.vendorData_" + itemID + ".newPN.value")
		var itemID = eval("document.vendorData_" + itemID + ".itemID.value")
		var updatedPN = eval("document.vendorData_" + itemID + ".updatedPN.value")
		
		ajax('/ajax/admin/ajaxProductQuickCreate.asp?vendorID=' + vendorID + '&cogs=' + cogs + '&vendorCode=' + vendorCode + '&newPN=' + newPN + '&updatedPN=' + updatedPN + '&newItemID=' + itemID,'dumpZone')
	}
	
	function subTypeSelect() {
		if (document.createForm.modelID != undefined) {
			var brandID = document.createForm.brandID.value
			var modelID = document.createForm.modelID.value
			if (Number(brandID) != NaN && Number(modelID) != NaN) {
				modelSelect(brandID,modelID)
			}
			else {
				alert("missing data")
			}
		}
		else {
			alert("missing crital component")
		}
	}
	
	function showBuildMsg() {
		if (document.getElementById("buildMsgBttn").innerHTML == "View buildMsg") {
			document.getElementById("buildMsgBttn").innerHTML = "Hide buildMsg";
			document.getElementById("buildMsgBox").style.display = '';
		}
		else {
			document.getElementById("buildMsgBttn").innerHTML = "View buildMsg";
			document.getElementById("buildMsgBox").style.display = 'none';
		}
	}
</script>