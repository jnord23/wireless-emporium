<%
response.buffer = false
pageTitle = "Create Nextag Store Product List"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 9000 'seconds

dim fs, file, filename, path
'Map the file name to the physical path on the server.
filename = "productList_nextag_store.txt"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Create Nextag Store Product List</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							Response.write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href=""../tempCSV/" & filename & """>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	response.write("Creating " & filename & ".<br>")
	set file = fs.CreateTextFile(path)
	dim SQL, RS, DoNotInclude, strline, stypeName
	SQL = "SELECT A.itemID, A.PartNumber, A.itemDesc, A.itemPic, A.price_our, A.itemLongDetail, A.inv_qty, A.UPCCode, B.brandName, D.typeName"
	SQL = SQL & " FROM we_items A INNER JOIN we_brands B ON A.brandid = B.brandid"
	SQL = SQL & " INNER JOIN we_types D ON A.typeid = D.typeid"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "Manufacturer" & vbtab & "Manufacturer Part #" & vbtab & "UPC" & vbtab & "ISBN" & vbtab & "MUZE ID" & vbtab & "Distributor ID" & vbtab & "Product Name" & vbtab & "Product Description" & vbtab & "Price" & vbtab & "Merchant Product Cat." & vbtab & "NexTag Product Cat." & vbtab & "Click-Out URL--OPTIONAL FOR STORES ONLY" & vbtab & "Image URL" & vbtab & "Ground Shipping" & vbtab & "Stock Status" & vbtab & "Expedited Shipping" & vbtab & "Overnight Shipping" & vbtab & "Marketing Message" & vbtab & "Product Condition" & vbtab & "Weight"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				if isnull(RS("typeName")) then
					stypeName = "Universal Gear"
				else
					stypeName = RS("typeName")
				end if
				
				strline = RS("brandName") & vbtab
				strline = strline & "WE-" & RS("itemID") & vbtab
				strline = strline & RS("UPCCode") & vbtab
				strline = strline & vbtab	'ISBN
				strline = strline & vbtab	'MUZE ID
				strline = strline & vbtab	'Distributor ID
				strline = strline & RS("itemDesc") & vbtab
				strline = strline & replace(replace(RS("itemLongDetail"),vbcrlf," "),vbtab," ") & vbtab
				strline = strline & RS("price_our") & vbtab
				strline = strline & stypeName & vbtab
				strline = strline & "500070 : Electronics / Phones & Communications / Cell Phone Accessories" & vbtab
				strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?refer=nextagstore" & vbtab
				strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab
				strline = strline & "0" & vbtab
				strline = strline & "Yes" & vbtab
				strline = strline & "6.99" & vbtab
				strline = strline & "19.99" & vbtab
				strline = strline & "Free Shipping on All Orders" & vbtab
				strline = strline & "New" & vbtab
				strline = strline	'Weight
				file.WriteLine strline
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
