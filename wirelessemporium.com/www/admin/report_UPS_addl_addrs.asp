<%
pageTitle = "WE UPS Orders w/Alternate Addresses To Be Shipped"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<h3>WE UPS Orders w/Alternate Addresses To Be Shipped</h3>

<%
SQL = "SELECT orderid FROM we_orders"
SQL = SQL & " WHERE shiptype LIKE 'UPS%' AND approved=1 AND (cancelled = 0 OR cancelled IS NULL)"
SQL = SQL & " AND thub_posted_date IS NOT NULL AND thub_posted_to_accounting = 'W' AND orderdatetime > '10/1/2008'"
SQL = SQL & " AND shippingid <> 0 AND shippingid IS NOT NULL"
SQL = SQL & " AND store = 0"
SQL = SQL & " ORDER BY orderdatetime DESC"
response.write "<h3>" & SQL & "</h3>" & vbCrLf
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3

if RS.eof then
    response.write "No records matched<br><br>So cannot make table..."
else
	%>
	<h3><%=RS.recordcount%> records found</h3>
	<table border="1">
		<tr>
			<%
			for each whatever in RS.fields
				%>
				<td><b><%=whatever.name%></b></td>
				<%
			next
			%>
		</tr>
		<%
		do until RS.eof
			%>
			<tr>
				<%
				for each whatever in RS.fields
					thisfield = whatever.value
					if isnull(thisfield) then
						thisfield = shownull
					end if
					if trim(thisfield) = "" then
						thisfield = showblank
					end if
					%>
					<td valign="top"><%=thisfield%></td>
					<%
				next
				%>
			</tr>
			<%
			RS.movenext
		loop
		%>
	</table>
	<%
end if

RS.close
set RS = nothing
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
