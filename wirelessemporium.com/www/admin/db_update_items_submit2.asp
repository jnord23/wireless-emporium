<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Update New Product"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	itemID = ds(request("ItemID"))
	inv_qty = ds(request("inv_qty"))
	cogs = ds(request("cogs"))
	wePN = ds(request("wePN"))
	vendor = ds(request("vendor"))
	vendorPN = ds(request("vendorPN"))
	updateNewProd = ds(request("updateNewProd"))

	if isnull(updateNewProd) or len(updateNewProd) < 1 then updateNewProd = 0
	if isnull(itemID) or len(itemID) < 1 or not isnumeric(itemID) then itemID = 0
	if isnull(inv_qty) or len(inv_qty) < 1 or not isnumeric(inv_qty) then inv_qty = 0
	if isnull(wePN) or len(wePN) < 1 then wePN = ""
	if isnull(vendor) or len(vendor) < 1 then vendor = ""
	if isnull(vendorPN) or len(vendorPN) < 1 then vendorPN = ""
	
	if updateNewProd = 1 then		
		sql = "update we_items set inv_qty = " & inv_qty & ", cogs = " & cogs & " where itemID = " & itemID
		response.Write(sql & "<br>")
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "insert into we_invRecord (itemID,inv_qty,adjustQty,adminID,notes,editDate) values(" & itemID & ",0," & inv_qty & "," & session("adminID") & ",'Items_Submit2','" & now & "')"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "select modelID from we_Items where itemID = " & itemID
		session("errorSQL") = sql
		set modelRS = oConn.execute(sql)
		
		if not modelRS.EOF then
			sql = "exec sp_createProductListByModelID " & prepInt(modelRS("modelID"))
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		
		if vendor <> "" and vendorPN <> "" then
			sql = "if not exists ( select top 1 id from we_vendornumbers where	we_partnumber = '" & wePN & "' and vendor = '" & vendor & "' ) insert into we_vendorNumbers (we_partNumber,vendor,partNumber) values('" & wePN & "','" & vendor & "','" & vendorPN & "')"
			response.Write(sql & "<br>")
			session("errorSQL") = sql
			oConn.execute(sql)
			
			'Record intitial item values for historical purposes
			
			sql =	"insert into we_Items_Initial " & vbcrlf & _
					"(itemId,partNumber,cogs,quantity,dateEntd,adminId) " & vbcrlf & _
					"values " & vbcrlf & _
					"(" & itemID & ",'" & wePN & "'," & cogs & "," & inv_qty & ",GETDATE()," & request.Cookies("adminID") & ")"
			response.Write(sql & "<br>")
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
		
		response.write "<h3>Product Update Complete</h3>"
		response.write "<p><a href=""/admin/db_search_brands.asp?searchID=4"">Back to Add/Edit Product.</a></p>"
	end if
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->