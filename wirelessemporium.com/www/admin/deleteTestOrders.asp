<%
	thisUser = Request.Cookies("username")
	pageTitle = "Delete Test Orders"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	server.ScriptTimeout = 9999
	
	dim sql, rs
	dim deleteTheseOrders : deleteTheseOrders = request.Form("deleteTheseOrders")
	if len(deleteTheseOrders) < 1 then deleteTheseOrders = prepStr(deleteTheseOrders)
	
	if deleteTheseOrders = "" then
		sql =	"select a.orderID " &_
				"from we_orders a " &_
					"join v_accounts b on a.accountid = b.accountid and a.store = b.site_id " &_
				"where cancelled is null and parentOrderID is null and orderdatetime > '1/1/2014' and b.bAddress1 = '4040 N. Palm St.'"
		set testOrdersRS = oConn.execute(sql)
		
		response.Write(sql & "<br>")
		response.Write("got results:" & testOrdersRS.EOF & "<br>")
		response.Flush()
		dim lap : lap = 0
		dim curOrderID : curOrderID = 0
		do while not testOrdersRS.EOF
			lap = lap + 1
			curOrderID = prepInt(testOrdersRS("orderID"))
			call cancelTestOrder(curOrderID)
			response.Write(lap & ": done one - " & curOrderID & "<br>")
			response.Flush()
			'if lap = 100 then
				'response.End()
			'end if
			testOrdersRS.movenext
		loop
	else
		orderArray = split(deleteTheseOrders,vbCrLf)
		response.Write("got results:" & ubound(orderArray) & "<br>")
		response.Flush()
		for i = 0 to ubound(orderArray)
			curOrderID = prepInt(orderArray(i))
			if curOrderID > 0 then
				call cancelTestOrder(curOrderID)
				response.Write((i+1) & ": done one - " & curOrderID & " ## ")
				response.Flush()
				sql = "update we_orders set note = 'deleted for mass duplicate error' where orderID = " & curOrderID
				oConn.execute(sql)
			end if
		next
	end if
%>
<form method="post" action="/admin/deleteTestOrders.asp">
<div><textarea name="deleteTheseOrders"></textarea></div>
<div><input type="submit" name="myAction" value="Delete Orders" /></div>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->