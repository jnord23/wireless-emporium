<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - "
	header = 1
	
	'http://developer.yahoo.com/yui/examples/autocomplete/ac_basic_array.html
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<title>Testing Autocomplete</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="/includes/js/acCombo.js"></script>
	<script type="text/javascript" src="/includes/js/autocomplete-min.js"></script>
    <style type="text/css">
	body{font:13px/1.231 arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}
	select,input,button,textarea,button{font:99% arial,helvetica,clean,sans-serif;}
	table{font-size:inherit;font:100%;}
	pre,code,kbd,samp,tt{font-family:monospace;*font-size:108%;line-height:100%;}
	.yui-skin-sam .yui-ac{position:relative;font-family:arial;font-size:100%;}
	.yui-skin-sam .yui-ac-input{position:absolute;width:100%;}
	.yui-skin-sam .yui-ac-container{position:absolute;top:1.6em;width:100%;}
	.yui-skin-sam .yui-ac-content{position:absolute;width:100%;border:1px solid #808080;background:#fff;overflow:hidden;z-index:9050;}
	.yui-skin-sam .yui-ac-shadow{position:absolute;margin:.3em;width:100%;background:#000;-moz-opacity:.10;opacity:.10;filter:alpha(opacity=10);z-index:9049;}
	.yui-skin-sam .yui-ac iframe{opacity:0;filter:alpha(opacity=0);padding-right:.3em;padding-bottom:.3em;}
	.yui-skin-sam .yui-ac-content ul{margin:0;padding:0;width:100%;}
	.yui-skin-sam .yui-ac-content li{margin:0;padding:2px 5px;cursor:default;white-space:nowrap;list-style:none;zoom:1;}
	.yui-skin-sam .yui-ac-content li.yui-ac-prehighlight{background:#B3D4FF;}
	.yui-skin-sam .yui-ac-content li.yui-ac-highlight{background:#cccccc;color:#FFF;}
    body {
		margin:0;
		padding:0;
	}
	#myAutoComplete {
	    width:15em; /* set width here or else widget will expand to fit its container */
    	padding-bottom:2em;
	}
    </style>
	<!--end custom header content for this example--> 
</head>
<body id="yahoo-com" class="yui-skin-sam">
<form name="searchForm" method="get">
<table border="0" cellpadding="3" cellpadding="0">
	<tr>
    	<td>
        	<label for="myInput">Enter a state:</label>
            <div id="myAutoComplete">
                <input id="myInput" name="stateName" type="text">
                <div id="myContainer"></div>
            </div>
        </td>
    </tr>
</table>
</form>
</body>
</html>
<script type="text/javascript">
YAHOO.example.Data = {
	arrayStates: [
		"Alabama",
		"Alaska",
		"Arizona",
		"Arkansas",
		"California",
		"Colorado",
		"Connecticut",
		"Delaware",
		"Florida",
		"Georgia",
		"Hawaii",
		"Idaho",
		"Illinois",
		"Indiana",
		"Iowa",
		"Kansas",
		"Kentucky",
		"Louisiana",
		"Maine",
		"Maryland",
		"Massachusetts",
		"Michigan",
		"Minnesota",
		"Mississippi",
		"Missouri",
		"Montana",
		"Nebraska",
		"Nevada",
		"New Hampshire",
		"New Jersey",
		"New Mexico",
		"New York",
		"North Dakota",
		"North Carolina",
		"Ohio",
		"Oklahoma",
		"Oregon",
		"Pennsylvania",
		"Rhode Island",
		"South Carolina",
		"South Dakota",
		"Tennessee",
		"Texas",
		"Utah",
		"Vermont",
		"Virginia",
		"Washington",
		"West Virginia",
		"Wisconsin",
		"Wyoming"
	]
};

YAHOO.example.BasicLocal = function() {
    var oDS = new YAHOO.util.LocalDataSource(YAHOO.example.Data.arrayStates);
    var oAC = new YAHOO.widget.AutoComplete("myInput", "myContainer", oDS);
    oAC.prehighlightClassName = "yui-ac-prehighlight";
    oAC.useShadow = false;
    
    return {
        oDS: oDS,
        oAC: oAC
    };
}();
</script>