<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Create Slave Product"
	header = 1
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim sql, brandArray
	
	dim linkItemID : linkItemID = prepInt(request.QueryString("linkItemID"))
	
	if not isnull(request.Form("masterProducts")) and len(request.Form("masterProducts")) > 0 then
		dim masterProducts, brandSlave, modelSlave, brandName, modelID, modelName, brandSlaveName, modelSlaveName
		masterProducts = ds(request.Form("masterProducts"))
		brandSlave = ds(request.Form("brandSlave"))
		modelSlave = ds(request.Form("modelSlave"))
		
		sql = "delete from we_ItemsSlave"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		brandName = ""
		prodArray = split(mid(masterProducts,2),",")
		for i = 0 to ubound(prodArray)
			sql = "select b.itemID from we_Items a left join we_Items b on a.PartNumber = b.PartNumber and b.master = 1 where a.itemID = " & prodArray(i)
			response.write "<br>" & sql & "<br>"
			session("errorSQL") = sql
			set masterIdRS = oConn.execute(sql)
			if not masterIdRS.EOF then
				if isnull(masterIdRS("itemID")) then
					useItemID = prodArray(i)
				else
					useItemID = masterIdRS("itemID")
				end if
			else
				useItemID = prodArray(i)
			end if
			if brandName = "" then
				sql = "select b.brandName, c.modelID, c.modelName, (select brandName from we_brands where brandID = " & brandSlave & ") as brandSlave, (select modelName from we_models where modelID = " & modelSlave & ") as modelSlave from we_items a left join we_brands b on a.brandID = b.brandID left join we_models c on a.modelID = c.modelID where a.itemID = " & useItemID
				session("errorSQL") = sql
				Set rs = Server.CreateObject("ADODB.Recordset")
				rs.open sql, oConn, 0, 1
				
				brandName = rs("brandName")
				if isnull(rs("modelID")) then
					response.Write("no master modelID found!")
					response.End()
				else
					modelID = rs("modelID")
				end if
				modelName = rs("modelName")
				brandSlaveName = rs("brandSlave")
				
				if isnull(rs("modelSlave")) then
					response.Write("no slave model found!")
					response.End()
				else
					modelSlaveName = rs("modelSlave")
				end if
				
				sql = "SET NOCOUNT ON; insert into we_ItemsSlave select [WE_URL],[brandID],[modelID],[typeID],[carrierID],[subtypeID],[vendor],[PartNumber],[itemDesc],[itemDesc_PS],[itemDesc_CA],[itemDesc_CO],[itemDesc_Google],[excludeFromCA],[ProductCatalog],[itemPic],[itemPic_CO],[itemSale],[price_Retail],[price_Our],[price_Our_BU],[price_CA],[price_CO],[price_PS],[price_ER],[price_Buy],[COGS],[numberOfSales],[inv_qty],[Seasonal],[Sports],[Bluetooth],[HandsfreeType],[ItemKit_OLD],[ItemKit_NEW],[hotDeal],[hideLive],[itemLongDetail],[itemHeader],[itemInfo],[BULLET1],[BULLET2],[BULLET3],[BULLET4],[BULLET5],[BULLET6],[BULLET7],[BULLET8],[BULLET9],[BULLET10],[COMPATIBILITY],[download_URL],[download_TEXT],[itemLongDetail_CA],[itemLongDetail_CO],[POINT1],[POINT2],[POINT3],[POINT4],[POINT5],[POINT6],[POINT7],[POINT8],[POINT9],[POINT10],[itemLongDetail_PS],[FEATURE1],[FEATURE2],[FEATURE3],[FEATURE4],[FEATURE5],[FEATURE6],[FEATURE7],[FEATURE8],[FEATURE9],[FEATURE10],[Features],[FormFactor],[BandType],[Band],[PackageContents],[TalkTime],[Standby],[Display],[Condition],[flag1],[UPCCode],[AMZN_sku],[BUY_sku],[itemDimensions],[itemWeight],[DateTimeEntd],[InvLastUpdatedBy],[InvLastUpdatedDate]," & prepInt(adminID) & " as PriceLastUpdatedBy,[PriceLastUpdatedDate],[PS_videos],[Smartphone],[NoDiscount],[MobileLine_sku],[Phone_Compatibility],[DesignType],0 as master,0 as ghost,0 as lastCall,0 as topModel_PS,'' as itemLongDetail_ER,'' as itemDesc_ER,[colorId] from we_items where itemID = " & useItemID & "; SELECT @@IDENTITY AS NewID;"
				session("errorSQL") = sql
				set objRS = oConn.execute(sql)
				newItemID = objRS.Fields("NewID").Value
			else
				sql = "insert into we_ItemsSlave select [WE_URL],[brandID],[modelID],[typeID],[carrierID],[subtypeID],[vendor],[PartNumber],[itemDesc],[itemDesc_PS],[itemDesc_CA],[itemDesc_CO],[itemDesc_Google],[excludeFromCA],[ProductCatalog],[itemPic],[itemPic_CO],[itemSale],[price_Retail],[price_Our],[price_Our_BU],[price_CA],[price_CO],[price_PS],[price_ER],[price_Buy],[COGS],[numberOfSales],[inv_qty],[Seasonal],[Sports],[Bluetooth],[HandsfreeType],[ItemKit_OLD],[ItemKit_NEW],[hotDeal],[hideLive],[itemLongDetail],[itemHeader],[itemInfo],[BULLET1],[BULLET2],[BULLET3],[BULLET4],[BULLET5],[BULLET6],[BULLET7],[BULLET8],[BULLET9],[BULLET10],[COMPATIBILITY],[download_URL],[download_TEXT],[itemLongDetail_CA],[itemLongDetail_CO],[POINT1],[POINT2],[POINT3],[POINT4],[POINT5],[POINT6],[POINT7],[POINT8],[POINT9],[POINT10],[itemLongDetail_PS],[FEATURE1],[FEATURE2],[FEATURE3],[FEATURE4],[FEATURE5],[FEATURE6],[FEATURE7],[FEATURE8],[FEATURE9],[FEATURE10],[Features],[FormFactor],[BandType],[Band],[PackageContents],[TalkTime],[Standby],[Display],[Condition],[flag1],[UPCCode],[AMZN_sku],[BUY_sku],[itemDimensions],[itemWeight],[DateTimeEntd],[InvLastUpdatedBy],[InvLastUpdatedDate]," & prepInt(adminID) & " as PriceLastUpdatedBy,[PriceLastUpdatedDate],[PS_videos],[Smartphone],[NoDiscount],[MobileLine_sku],[Phone_Compatibility],[DesignType],0 as master,0 as ghost,0 as lastCall,0 as topModel_PS,'' as itemLongDetail_ER,'' as itemDesc_ER,[colorId] from we_items where itemID = " & useItemID
				set objRS = oConn.execute(sql)
			end if
		next
		
		oldName = brandName & " " & modelName
		newName = brandSlaveName & " " & modelSlaveName
		
		oldName = replace(oldName,"iPhone 3G/3G S","iPhone 3G/3GS")
		oldNameEdited = replace(oldName," / ","/")
		newNameEdited = replace(newName," / ","/")
		
		updateFields = "WE_URL,itemDesc,itemDesc_PS,itemDesc_CA,itemDesc_CO,itemLongDetail_CA,itemLongDetail_CO,itemLongDetail_PS,"
		updateFields = updateFields & "Bullet1,Bullet2,Bullet3,Bullet4,Bullet5,Bullet6,Bullet7,Bullet8,Bullet9,Bullet10,"
		updateFields = updateFields & "Point1,Point2,Point3,Point4,Point5,Point6,Point7,Point8,Point9,Point10,"
		updateFields = updateFields & "Feature1,Feature2,Feature3,Feature4,Feature5,Feature6,Feature7,Feature8,Feature9,Feature10"
		fieldArray = split(updateFields,",")
		
		if left(masterProducts,1) = "," then masterProducts = mid(masterProducts,2)
		if right(masterProducts,1) = "," then masterProducts = left(masterProducts,len(masterProducts)-1)
		sql =	"select distinct d.brandName, c.modelName " &_
				"from we_Items a " &_
					"left join we_Items b on a.PartNumber = b.PartNumber and b.master = 1 " &_
					"left join we_models c on b.modelID = c.modelID " &_
					"left join we_brands d on b.brandID = d.brandID " &_
				"where a.itemID in (" & masterProducts & ")"
		session("errorSQL") = sql
		set brandModelRS = oConn.execute(sql)
		
		do while not brandModelRS.EOF
			brandName = brandModelRS("brandName")
			modelName = brandModelRS("modelName")
			
			oldName = brandName & " " & modelName
			newName = brandSlaveName & " " & modelSlaveName
			
			oldName = replace(oldName,"iPhone 3G/3G S","iPhone 3G/3GS")
			oldNameEdited = replace(oldName," / ","/")
			newNameEdited = replace(newName," / ","/")
			
			session("errorSQL") = "oldName:" & oldName & "<br>newName:" & newName
			sql = "update we_ItemsSlave set "
			for i = 0 to ubound(fieldArray)
				if i = 0 then
					sql = sql & fieldArray(i) & " = replace(replace(" & fieldArray(i) & ",'" & formatSEO(oldName) & "','" & formatSEO(newName) & "'),'" & formatSEO(oldNameEdited) & "','" & formatSEO(nameNameEdited) & "')"
				else
					sql = sql & "," & fieldArray(i) & " = replace(replace(" & fieldArray(i) & ",'" & oldName & "','" & newName & "'),'" & formatSEO(oldNameEdited) & "','" & formatSEO(newNameEdited) & "')"
				end if
			next
			sql = sql & ", modelID = " & modelSlave & ", brandID = " & brandSlave & ", numberOfSales = 0, inv_qty = -1, dateTimeEntd = '" & now & "' where PriceLastUpdatedBy = " & prepInt(adminID)
			session("errorSQL") = sql
			oConn.execute(sql)
			
			sql = "insert into we_adminActions (adminID,action) values(" & adminID & ",'Create Slave - Update Item Info:" & SQLquote(newName) & " (> " & newItemID & ")')"
			session("errorSQL") = SQL
			oConn.execute SQL
			
			'### Start Text Field Replace ###
			'text field can not use replace
			sql = "select itemID, itemLongDetail, (select max(inv_qty) from we_items where partNumber = a.partNumber) as masterQty from we_ItemsSlave a where modelID = " & modelSlave & " and itemID > " & (cdbl(newItemID) - 1)
			session("errorSQL") = sql
			Set rs = Server.CreateObject("ADODB.Recordset")
			rs.open sql, oConn, 0, 1
			
			do while not rs.EOF
				curItemID = rs("itemID")
				curItemLongDetail = rs("itemLongDetail")
				curItemLongDetail = replace(curItemLongDetail,oldName,newName)
				curItemLongDetail = replace(curItemLongDetail,oldNameEdited,newNameEdited)
				if rs("masterQty") = 0 then
					sql = "update we_ItemsSlave set inv_qty = -1, itemLongDetail = '" & ds(curItemLongDetail) & "' where master = 0 and modelID = " & modelSlave & " and itemID = " & curItemID
				else
					sql = "update we_ItemsSlave set itemLongDetail = '" & ds(curItemLongDetail) & "' where modelID = " & modelSlave & " and itemID = " & curItemID
				end if
				session("errorSQL") = sql
				oConn.execute(sql)
				rs.movenext
			loop
			'### End Text Field Replace ###
			brandModelRS.movenext
		loop
		
		slavesCreated = ubound(prodArray) + 1
		
		sql = "insert into we_Items select [WE_URL],[brandID],[modelID],[typeID],[carrierID],[subtypeID],[vendor],[PartNumber],[itemDesc],[itemDesc_PS],[itemDesc_CA],[itemDesc_CO],[itemDesc_Google],[excludeFromCA],[ProductCatalog],[itemPic],[itemPic_CO],[itemSale],[price_Retail],[price_Our],[price_Our_BU],[price_CA],[price_CO],[price_PS],[price_ER],[price_Buy],[COGS],[numberOfSales],[inv_qty],[Seasonal],[Sports],[Bluetooth],[HandsfreeType],[ItemKit_OLD],[ItemKit_NEW],[hotDeal],[hideLive],[itemLongDetail],[itemHeader],[itemInfo],[BULLET1],[BULLET2],[BULLET3],[BULLET4],[BULLET5],[BULLET6],[BULLET7],[BULLET8],[BULLET9],[BULLET10],[COMPATIBILITY],[download_URL],[download_TEXT],[itemLongDetail_CA],[itemLongDetail_CO],[POINT1],[POINT2],[POINT3],[POINT4],[POINT5],[POINT6],[POINT7],[POINT8],[POINT9],[POINT10],[itemLongDetail_PS],[FEATURE1],[FEATURE2],[FEATURE3],[FEATURE4],[FEATURE5],[FEATURE6],[FEATURE7],[FEATURE8],[FEATURE9],[FEATURE10],[Features],[FormFactor],[BandType],[Band],[PackageContents],[TalkTime],[Standby],[Display],[Condition],[flag1],[UPCCode],[AMZN_sku],[BUY_sku],[itemDimensions],[itemWeight],[DateTimeEntd],[InvLastUpdatedBy],[InvLastUpdatedDate],[PriceLastUpdatedBy],[PriceLastUpdatedDate],[PS_videos],[Smartphone],[NoDiscount],[MobileLine_sku],[Phone_Compatibility],[DesignType],0 as master,0 as ghost,0 as lastCall,0 as topModel_PS,'' as itemLongDetail_ER,'' as itemDesc_ER,[colorId] from we_ItemsSlave"
		set objRS = oConn.execute(sql)
		
		sql = "delete from we_ItemsSlave"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "exec sp_createProductListByModelID " & modelSlave
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "exec sp_updateQtyByModelCat"
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	sql = "select brandID, brandName from we_brands order by brandName"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	brandArray = ""
	do while not rs.EOF
		brandArray = brandArray & rs("brandID") & "#" & rs("brandName") & "$"
		rs.movenext
	loop
	brandArray = split(brandArray,"$")
	
	if linkItemID > 0 then formSubmit = "/admin/createSlave.asp?linkItemID=" & linkItemID else formSubmit = "/admin/createSlave.asp"
%>
<form style="margin:0px;" action="<%=formSubmit%>" method="post" name="slaveForm" onsubmit="return(testForm(this))">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="600">
	<tr>
    	<td><h1 style="margin:0px;">Create Slave Product</h1></td>
    </tr>
    <tr>
    	<td style="font-size:10px; padding-bottom:10px;">
        	This application allows the user to quickly create a slave product for a new model.
        </td>
    </tr>
    <tr>
    	<td style="border-top:1px solid #000; border-bottom:1px solid #000; font-weight:bold; font-size:16px;">Product Receiving Slave</td>
    </tr>
    <tr>
    	<td id="brandBox" style="padding-top:5px;">
        	<div style="width:600px;">
	        	<div style="float:left; width:140px;">Select Brand:</div>
    	        <div style="float:left; padding-left:10px; width:450px;">
	    	    	<select name="brandSlave" onchange="brandSlaveSelect(this.value)">
    	    	    	<option value="">Select Brand</option>
        	    	    <%
						for i = 0 to (ubound(brandArray) - 1)
							useArray = split(brandArray(i),"#")
						%>
		                <option value="<%=useArray(0)%>"><%=useArray(1)%></option>
    		            <%
						next
						%>
		            </select>
    	        </div>
            </div>
        </td>
    </tr>
    <tr>
        <td id="modelSlaveBox" style="padding-top:5px;"></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
    	<td style="border-top:1px solid #000; border-bottom:1px solid #000; font-weight:bold; font-size:16px;">Product To Copy From</td>
    </tr>
    <tr>
    	<td style="padding-top:10px;">
        	<div style="width:600px;">
	        	<div style="float:left; width:140px;">ItemID:</div>
    	        <div style="float:left; padding-left:10px; width:450px;">
                	<% if linkItemID = 0 then linkItemID = "" %>
	    	    	<input type="text" name="itemID" value="<%=linkItemID%>" onfocus="return(chkTopProd())" />
                    <input type="button" name="findPart" value="Search" onclick="itemIDSelect(document.slaveForm.itemID.value)" />
    	        </div>
            </div>
        </td>
    </tr>
    <tr><td style="font-weight:bold; font-size:16px;">- OR -</td></tr>
    <tr>
    	<td id="brandBox" style="padding-top:5px;">
        	<div style="width:600px;">
	        	<div style="float:left; width:140px;">Select Brand:</div>
    	        <div style="float:left; padding-left:10px; width:450px;">
	    	    	<select name="brand" onfocus="return(chkTopProd())" onchange="brandSelect(this.value)">
    	    	    	<option value="">Select Brand</option>
        	    	    <%
						for i = 0 to (ubound(brandArray) - 1)
							useArray = split(brandArray(i),"#")
						%>
		                <option value="<%=useArray(0)%>"><%=useArray(1)%></option>
    		            <%
						next
						%>
		            </select>
    	        </div>
            </div>
        </td>
    </tr>
    <tr>
        <td id="modelBox" style="padding-top:5px;"></td>
    </tr>
    <tr>
        <td id="itemBox" style="padding-top:5px;"></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
    	<td style="border-top:1px solid #000; padding-top:5px;" align="right">
        	<input type="submit" name="mySub" value="Save Slave Product(s)" />
            <input type="hidden" name="masterProducts" value="" />
        </td>
    </tr>
</table>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	var modelSlaveID = 0;
	var modelMasterID = 0;
	function itemIDSelect(itemID) {
		ajax('/ajax/admin/ajaxCreateSlave.asp?itemID=' + itemID,'modelBox')
	}
	function brandSelect(brandID) {
		ajax('/ajax/admin/ajaxCreateSlave.asp?brandID=' + brandID,'modelBox')
	}
	function modelSelect(modelID) {
		modelMasterID = document.slaveForm.model.value;
		ajax('/ajax/admin/ajaxCreateSlave.asp?modelSlaveID=' + modelSlaveID + '&modelID=' + modelID,'itemBox')
	}	
	function brandSlaveSelect(brandID) {
		ajax('/ajax/admin/ajaxCreateSlave.asp?brandID=' + brandID + '&slave=1','modelSlaveBox')
	}
	function catSelect(catID,modelID) {
		ajax('/ajax/admin/ajaxCreateSlave.asp?modelSlaveID=' + modelSlaveID + '&modelID=' + modelID + '&catID=' + catID + '&slave=1','itemBox')
	}
	function saveModelSlave(modelID) {
		modelSlaveID = modelID
	}
	function chkTopProd() {
		if (document.slaveForm.brandSlave.value == "") {
			alert("Select the top brand first")
			document.slaveForm.brandSlave.focus()
			return false
		}
		else if (document.slaveForm.modelSlave.value == "") {
			alert("Select the top model first")
			document.slaveForm.modelSlave.focus()
			return false
		}
		else {
			return true
		}
	}
	function testForm(form) {
		var masterIdList = ""
		
		for (i=4; i < form.elements.length; i++) {
			if (form.elements[i].checked == true) { masterIdList = masterIdList + ',' + form.elements[i].value }
		}
		
		if (form.itemID.value == "" && form.brand.value == "") {
			alert("You must enter an itemID or select a brand/model")
			return false
		}
		else if (form.itemID.value != "") {
			masterIdList = ',' + form.itemID.value
		}
		else if (document.slaveForm.brand.value != "" && modelMasterID == 0) {
			alert("You must select a model: (" + form.brand.value + "," + modelMasterID + "," + form + ")")
			form.model.focus()
			return false
		}
		else if (masterIdList == "") {
			alert("You must select at least one master product")
			return false
		}
		
		if (document.slaveForm.brandSlave.value == "") {
			alert("You must select a brand/model for the slave")
			form.brandSlave.focus()
			return false
		}
		else if (document.slaveForm.modelSlave.value == "") {
			alert("You must select a model for the slave")
			form.modelSlave.focus()
			return false
		}
		else {
			document.slaveForm.masterProducts.value = masterIdList
			return true
		}
	}
	<% if slavesCreated = 1 then %>
	alert("Slave Product Created")
	<% elseif slavesCreated > 2 then %>
	alert("All Slave Products Created")
	<% end if %>
</script>