<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Hypercel Transmit Order"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim fs, thisFile, filename, path
	filename = "hypercel_" & replace(cStr(date),"/","-") & ".csv"
	path = server.MapPath("/tempCSV/" & filename)
	set fs = Server.CreateObject("Scripting.FileSystemObject")
	if fs.FileExists(path) then fs.DeleteFile(path)
	set thisFile = fs.CreateTextFile(path)
	
	sql = 	"select a.store, b.orderdetailid, a.orderID, c.fname, c.lname, c.sAddress1, c.sAddress2, c.sCity, c.sState, c.sZip, c.phone, c.email, d.MobileLine_sku, b.quantity, b.itemID from we_orders a left join we_orderdetails b on a.orderID = b.orderID left join we_accounts c on a.accountID = c.accountID left join we_items d on b.itemID = d.itemID where a.store = 0 and a.approved = 1 and d.partNumber like '%-HYP-%' and a.cancelled is null and b.sentToDropShipper is null order by a.store, a.orderID"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	strToWrite = "store,orderID,fname,lname,address1,address2,city,state,zip,country,phone,email,partNumber,quantity"
	thisFile.WriteLine strToWrite
	rCount = 0
	iCount = 0
	curOrderID = ""
	dim itemList : itemList = ""
	do while not rs.EOF
		store = rs("store")
		if store = 0 then storeName = "WirelessEmporium"
		if store = 1 then storeName = "CellphoneAccents"
		if store = 2 then storeName = "CellularOutfitter"
		if store = 4 then storeName = "PhoneSale"
		if len(RS("sState")) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",rs("sState")) > 0 then
			sCountry = "CA"
		else
			sCountry = "US"
		end if
		
		useOrderID = rs("orderID")
		if curOrderID = "" or curOrderID <> useOrderID then rCount = rCount + 1
		
		strToWrite = storeName & "," & useOrderID & ","
		strToWrite = strToWrite & rs("fname") & ","
		strToWrite = strToWrite & rs("lname") & ","
		strToWrite = strToWrite & rs("sAddress1") & ","
		strToWrite = strToWrite & rs("sAddress2") & ","
		strToWrite = strToWrite & rs("sCity") & ","
		strToWrite = strToWrite & rs("sState") & ","
		strToWrite = strToWrite & rs("sZip") & ","
		strToWrite = strToWrite & sCountry & ","
		strToWrite = strToWrite & rs("phone") & ","
		strToWrite = strToWrite & rs("email") & ","
		strToWrite = strToWrite & rs("MobileLine_sku") & ","
		useQty = rs("quantity")
		iCount = iCount + useQty
		strToWrite = strToWrite & useQty
		thisFile.WriteLine strToWrite
		
		lastOrderDetailID = rs("orderdetailid")
		if instr(itemList,rs("itemID") & ",") < 1 then itemList = itemList & "'" & rs("itemID") & "',"
		
		rs.movenext
	loop
	if itemList <> "" then
		itemList = left(itemList,len(itemList)-1)
	
		if instr(itemList,",") > 0 then
			sql = "update we_orderdetails set sentToDropShipper = '" & now & "' where sentToDropShipper is null and itemID IN (" & itemList & ")"
		else
			sql = "update we_orderdetails set sentToDropShipper = '" & now & "' where sentToDropShipper is null and itemID = " & itemList
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	thisFile.Close()
	
	bodyText = "<h3><u>Hypercel Orders</u></h3>"
	bodyText = bodyText & "<h3>TOTAL NUMBER OF RECORDS: " & rCount & "</h3>"
	bodyText = bodyText & "<h3>TOTAL NUMBER OF ITEMS: " & iCount & "</h3>"
	if rCount > 0 then
		bodyText = bodyText & "<h3>(CSV FILE ATTACHED)</h3>"
	end if
	response.write bodyText & "<br>" & vbcrlf
	
	if rCount > 0 then
		Set objErrMail = CreateObject("CDO.Message")
		With objErrMail
			.From = "ruben@wirelessemporium.com"
			.To = "trapp@hypercel.com,dlucks@outlook.com,Willp@wirelessemporium.com"
			'.To = "orders@hypercel.com"
			'.cc = ""
			.bcc = "ruben@wirelessemporium.com,jon@wirelessemporium.com"
			.Subject = "Wireless Emporium - Hypercel Orders for " & date
			.HTMLBody = bodyText
			.AddAttachment path
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Update
			.Send
			response.write "<h3>Email Sent</h3>" & vbcrlf
		End With
	else
		response.write "<h3>No Email Required</h3>" & vbcrlf
	end if
	
	response.write "<h3>DONE!</h3>" & vbcrlf
%>
<table border="0" cellpadding="3" cellspacing="0">
	<tr>
    	<td></td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
