<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Music Skins Transmit Order"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim fs, thisFile, filename, path
	filename = "musicSkins_" & replace(cStr(date),"/","-") & ".csv"
	path = server.MapPath("/tempCSV/" & filename)
	set fs = Server.CreateObject("Scripting.FileSystemObject")
	if fs.FileExists(path) then fs.DeleteFile(path)
	set thisFile = fs.CreateTextFile(path)
	
	sql = "update we_orderDetails set musicSkins = 1 where itemID > 999999 and musicSkins is null"
	session("errorSQL") = sql
	oConn.execute(sql)
	
	'sql = "select a.store, b.orderdetailid, a.orderID, c.fname, c.lname, c.sAddress1, c.sAddress2, c.sCity, c.sState, c.sZip, c.phone, c.email, d.musicSkinsID, b.quantity from we_orders a left join we_orderdetails b on a.orderID = b.orderID left join we_accounts c on a.accountID = c.accountID left join we_items_musicSkins d on b.itemID = d.id where a.approved = 1 and b.musicSkins = 1 and a.cancelled is null and b.sentToDropShipper is null order by a.store, a.orderID"
'	sql = 	"select a.store, b.orderdetailid, a.orderID, c.fname, c.lname, c.sAddress1, c.sAddress2, c.sCity, c.sState, c.sZip, c.phone, c.email, d.musicSkinsID, b.quantity from we_orders a left join we_orderdetails b on a.orderID = b.orderID left join we_accounts c on a.accountID = c.accountID left join we_items_musicSkins d on b.itemID = d.id where a.store = 0 and a.approved = 1 and b.musicSkins = 1 and a.cancelled is null and b.sentToDropShipper is null " &_
'			"union " &_
'			"select a.store, b.orderdetailid, a.orderID, c.fname, c.lname, c.sAddress1, c.sAddress2, c.sCity, c.sState, c.sZip, c.phone, c.email, d.musicSkinsID, b.quantity from we_orders a left join we_orderdetails b on a.orderID = b.orderID left join co_accounts c on a.accountID = c.accountID left join we_items_musicSkins d on b.itemID = d.id where a.store = 2 and a.approved = 1 and b.musicSkins = 1 and a.cancelled is null and b.sentToDropShipper is null " &_
'			"order by a.store, a.orderID"

	sql = 	"select	a.store, b.orderdetailid, a.orderID, c.fname, c.lname, c.sAddress1, c.sAddress2, c.sCity, c.sState, c.sZip, c.phone, c.email, d.musicSkinsID, b.quantity" & vbcrlf & _
			"from	we_orders a join we_orderdetails b" & vbcrlf & _
			"	on	a.orderid = b.orderid join v_accounts c" & vbcrlf & _
			"	on	a.store = c.site_id and a.accountid = c.accountid join we_items_musicSkins d" & vbcrlf & _
			"	on	b.itemid = d.id" & vbcrlf & _
			"where	a.approved = 1 and b.musicSkins = 1 and (a.cancelled is null or a.cancelled = 0)" & vbcrlf & _
			"	and	b.sentToDropShipper is null" & vbcrlf & _
			"order by a.store, a.orderID" & vbcrlf

	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	strToWrite = "store,orderID,fname,lname,address1,address2,city,state,zip,country,phone,email,partNumber,quantity"
	thisFile.WriteLine strToWrite
	rCount = 0
	iCount = 0
	curOrderID = ""
	do while not rs.EOF
		store = rs("store")
		if store = 0 then storeName = "WirelessEmporium"
		if store = 1 then storeName = "CellphoneAccents"
		if store = 2 then storeName = "CellularOutfitter"
		if len(RS("sState")) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",rs("sState")) > 0 then
			sCountry = "CA"
		else
			sCountry = "US"
		end if
		
		useOrderID = rs("orderID")
		if curOrderID = "" or curOrderID <> useOrderID then rCount = rCount + 1
		
		strToWrite = storeName & "," & useOrderID & ","
		strToWrite = strToWrite & rs("fname") & ","
		strToWrite = strToWrite & rs("lname") & ","
		strToWrite = strToWrite & rs("sAddress1") & ","
		strToWrite = strToWrite & rs("sAddress2") & ","
		strToWrite = strToWrite & rs("sCity") & ","
		strToWrite = strToWrite & rs("sState") & ","
		strToWrite = strToWrite & rs("sZip") & ","
		strToWrite = strToWrite & sCountry & ","
		strToWrite = strToWrite & rs("phone") & ","
		strToWrite = strToWrite & rs("email") & ","
		strToWrite = strToWrite & rs("musicSkinsID") & ","
		useQty = rs("quantity")
		iCount = iCount + useQty
		strToWrite = strToWrite & useQty
		thisFile.WriteLine strToWrite
		
		lastOrderDetailID = rs("orderdetailid")
		
		rs.movenext
	loop
	
	sql = "update we_orderdetails set sentToDropShipper = '" & now & "' where sentToDropShipper is null and musicSkins = 1"
	session("errorSQL") = sql
	oConn.execute(sql)
	
	thisFile.Close()
	
	bodyText = "<h3><u>Music Skins Orders</u></h3>"
	bodyText = bodyText & "<h3>TOTAL NUMBER OF RECORDS: " & rCount & "</h3>"
	bodyText = bodyText & "<h3>TOTAL NUMBER OF ITEMS: " & iCount & "</h3>"
	if rCount > 0 then
		bodyText = bodyText & "<h3>(CSV FILE ATTACHED)</h3>"
	end if
	response.write bodyText & "<br>" & vbcrlf
	
	if rCount > 0 then
		Set objErrMail = CreateObject("CDO.Message")
		With objErrMail
			.From = "ruben@wirelessemporium.com"
			'.To = "jon@wirelessemporium.com"
			'.To = "orders@musicskins.com"
			.To = "bob@wirelessemporium.com,yvonne@wirelessemporium.com,terry.pierson@ZingRevolution.com"
			.cc = "ruben@wirelessemporium.com"
			.bcc = "jon@wirelessemporium.com,terry@wirelessemporium.com"
			.Subject = "Music Skins Orders for " & date
			.HTMLBody = bodyText
			.AddAttachment path
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Update
			.Send
			response.write "<h3>Email Sent</h3>" & vbcrlf
		End With
	else
		response.write "<h3>No Email Required</h3>" & vbcrlf
	end if
	
	response.write "<h3>DONE!</h3>" & vbcrlf
%>
<table border="0" cellpadding="3" cellspacing="0">
	<tr>
    	<td></td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
