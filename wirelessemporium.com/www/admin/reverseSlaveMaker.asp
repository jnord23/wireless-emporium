<%
	thisUser = Request.Cookies("username")
	pageTitle = "The Lincoln: Reverse Slave Maker"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	server.ScriptTimeout = 9999
	
	dim newItemID : newItemID = prepInt(request.Form("newItemID"))
	dim blueprintItemID : blueprintItemID = prepInt(request.Form("blueprintItemID"))
	dim modelIDs : modelIDs = prepStr(request.Form("modelList"))
	dim slavesCreated : slavesCreated = 0
	dim slavesSkipped : slavesSkipped = 0
	dim brandID, brandName, modelName, updateFields, fieldArray, modelArray, sql, rs, activeModelList
	
	activeModelList = ""
	
	if newItemID > 0 and blueprintItemID > 0 and modelIDs <> "" then
		response.Write("<!-- modelIDs:" & modelIDs & " -->")
		modelArray = split(modelIDs,", ")
		updateFields = "WE_URL,itemDesc,itemDesc_PS,itemDesc_CA,itemDesc_CO,itemLongDetail_CA,itemLongDetail_CO,itemLongDetail_PS,"
		updateFields = updateFields & "Bullet1,Bullet2,Bullet3,Bullet4,Bullet5,Bullet6,Bullet7,Bullet8,Bullet9,Bullet10,"
		updateFields = updateFields & "Point1,Point2,Point3,Point4,Point5,Point6,Point7,Point8,Point9,Point10,"
		updateFields = updateFields & "Feature1,Feature2,Feature3,Feature4,Feature5,Feature6,Feature7,Feature8,Feature9,Feature10"
		fieldArray = split(updateFields,",")
		
		sql = "delete from we_ItemsSlave"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		for i = 0 to ubound(modelArray)
			response.Write("<!-- lap:" & i & " of " & ubound(modelArray) & " -->")
			sql = "select a.brandID, b.brandName, a.modelName, (select brandName + '##' + modelName + '##' + partNumber from we_items ia left join we_brands ib on ia.brandID = ib.brandID left join we_models ic on ia.modelID = ic.modelID where ia.itemID = " & newItemID & ") as oldBrandModel from we_models a left join we_brands b on a.brandID = b.brandID where a.modelID = " & modelArray(i)
			session("errorSQL") = sql
			set rs = oConn.execute(sql)
			
			brandID = rs("brandID")
			brandName = rs("brandName")
			modelName = rs("modelName")
			oldBrandModel = rs("oldBrandModel")
			bmArray = split(oldBrandModel,"##")
			oldBrandName = bmArray(0)
			oldModelName = bmArray(1)
			partNumber = bmArray(2)
			
			oldName = oldBrandName & " " & oldModelName
			newName = brandName & " " & modelName
			
			oldBrandName = replace(oldBrandName," / ","/")
			'oldModelName = replace(oldModelName," / ","/")
			brandName = replace(brandName," / ","/")
			modelName = replace(modelName," / ","/")
			
			sql = "select count(*) as itemCnt from we_Items where partNumber = '" & partNumber & "' and modelID = " & modelArray(i)
			session("errorSQL") = sql
			set itemCntRS = oConn.execute(sql)
			
			if prepInt(itemCntRS("itemCnt")) = 0 then
				sql = "SET NOCOUNT ON; insert into we_ItemsSlave select [WE_URL],[brandID],[modelID],[typeID],[carrierID],[subtypeID],[vendor],[PartNumber],[itemDesc],[itemDesc_PS],[itemDesc_CA],[itemDesc_CO],[itemDesc_Google],[excludeFromCA],[ProductCatalog],[itemPic],[itemPic_CO],[itemSale],[price_Retail],[price_Our],[price_Our_BU],[price_CA],[price_CO],[price_PS],[price_ER],[price_Buy],[COGS],[numberOfSales],[inv_qty],[Seasonal],[Sports],[Bluetooth],[HandsfreeType],[ItemKit_OLD],[ItemKit_NEW],[hotDeal],[hideLive],[itemLongDetail],[itemHeader],[itemInfo],[BULLET1],[BULLET2],[BULLET3],[BULLET4],[BULLET5],[BULLET6],[BULLET7],[BULLET8],[BULLET9],[BULLET10],[COMPATIBILITY],[download_URL],[download_TEXT],[itemLongDetail_CA],[itemLongDetail_CO],[POINT1],[POINT2],[POINT3],[POINT4],[POINT5],[POINT6],[POINT7],[POINT8],[POINT9],[POINT10],[itemLongDetail_PS],[FEATURE1],[FEATURE2],[FEATURE3],[FEATURE4],[FEATURE5],[FEATURE6],[FEATURE7],[FEATURE8],[FEATURE9],[FEATURE10],[Features],[FormFactor],[BandType],[Band],[PackageContents],[TalkTime],[Standby],[Display],[Condition],[flag1],[UPCCode],[AMZN_sku],[BUY_sku],[itemDimensions],[itemWeight],[DateTimeEntd],[InvLastUpdatedBy],[InvLastUpdatedDate],[PriceLastUpdatedBy],[PriceLastUpdatedDate],[PS_videos],[Smartphone],[NoDiscount],[MobileLine_sku],[Phone_Compatibility],[DesignType],0 as master,0 as ghost,0 as lastCall,0 as topModel_PS,'' as itemLongDetail_ER,'' as itemDesc_ER,[colorID] from we_items where itemID = " & newItemID & "; SELECT @@IDENTITY AS NewID;"
				session("errorSQL") = sql
				set objRS = oConn.execute(sql)
				createdItemID = objRS.Fields("NewID").Value
				
				sql = "update we_ItemsSlave set "
				for faX = 0 to ubound(fieldArray)
					if faX = 0 then
						sql = sql & fieldArray(faX) & " = replace(replace(" & fieldArray(faX) & ",'" & formatSEO(oldBrandName) & "','" & formatSEO(brandName) & "'),'" & formatSEO(oldModelName) & "','" & formatSEO(modelName) & "')"
					else
						sql = sql & "," & fieldArray(faX) & " = replace(replace(" & fieldArray(faX) & ",'" & oldBrandName & "','" & brandName & "'),'" & oldModelName & "','" & prepStr(modelName) & "')"
					end if
				next
				sql = sql & ", modelID = " & modelArray(i) & ", brandID = " & brandID & ", numberOfSales = 0, inv_qty = -1, master = 0, dateTimeEntd = '" & now & "' where itemID = " & createdItemID
				session("errorSQL") = sql
				oConn.execute(sql)
				
				sql = "insert into we_adminActions (adminID,action) values(" & adminID & ",'reverseSlaveMaker update we_Items:" & prepStr(newName) & " (" & createdItemID & ")')"
				session("errorSQL") = SQL
				oConn.execute SQL
				
				'### Start Text Field Replace ###
				'text field can not use replace
				sql =	"select modelID, brandID, subtypeID, itemLongDetail, itemLongDetail_CA, itemLongDetail_CO, itemLongDetail_PS, itemLongDetail_ER " &_
						"from we_ItemsSlave a " &_
						"where itemID = " & createdItemID
				session("errorSQL") = sql
				set rs = oConn.execute(sql)
				
				if not rs.eof then
					modelID = rs("modelID")
					brandID = rs("brandID")
					subtypeID = rs("subtypeID")
					curItemLongDetail = rs("itemLongDetail")
					curItemLongDetail = replace(replace(curItemLongDetail,oldBrandName,brandName),oldModelName,modelName)
					curItemLongDetail_CA = rs("itemLongDetail_CA")
					curItemLongDetail_CA = replace(replace(curItemLongDetail_CA,oldBrandName,brandName),oldModelName,modelName)
					curItemLongDetail_CO = rs("itemLongDetail_CO")
					curItemLongDetail_CO = replace(replace(curItemLongDetail_CO,oldBrandName,brandName),oldModelName,modelName)
					curItemLongDetail_PS = rs("itemLongDetail_PS")
					curItemLongDetail_PS = replace(replace(curItemLongDetail_PS,oldBrandName,brandName),oldModelName,modelName)
					curItemLongDetail_ER = rs("itemLongDetail_ER")
					curItemLongDetail_ER = replace(replace(curItemLongDetail_ER,oldBrandName,brandName),oldModelName,modelName)
					sql = "update we_ItemsSlave set itemLongDetail = '" & replace(curItemLongDetail, "'", "''") & "', itemLongDetail_CA = '" & replace(curItemLongDetail_CA, "'", "''") & "', itemLongDetail_CO = '" & replace(curItemLongDetail_CO, "'", "''") & "', itemLongDetail_PS = '" & replace(curItemLongDetail_PS, "'", "''") & "', itemLongDetail_ER = '" & replace(curItemLongDetail_ER, "'", "''") & "' where itemID = " & createdItemID
					session("errorSQL") = sql
					oConn.execute(sql)
				end if
				'### End Text Field Replace ###
				
				response.flush
				
				sql = "SET NOCOUNT ON; insert into we_Items select [WE_URL],[brandID],[modelID],[typeID],[carrierID],[subtypeID],[vendor],[PartNumber],[itemDesc],[itemDesc_PS],[itemDesc_CA],[itemDesc_CO],[itemDesc_Google],[excludeFromCA],[ProductCatalog],[itemPic],[itemPic_CO],[itemSale],[price_Retail],[price_Our],[price_Our_BU],[price_CA],[price_CO],[price_PS],[price_ER],[price_Buy],[COGS],[numberOfSales],[inv_qty],[Seasonal],[Sports],[Bluetooth],[HandsfreeType],[ItemKit_OLD],[ItemKit_NEW],[hotDeal],[hideLive],[itemLongDetail],[itemHeader],[itemInfo],[BULLET1],[BULLET2],[BULLET3],[BULLET4],[BULLET5],[BULLET6],[BULLET7],[BULLET8],[BULLET9],[BULLET10],[COMPATIBILITY],[download_URL],[download_TEXT],[itemLongDetail_CA],[itemLongDetail_CO],[POINT1],[POINT2],[POINT3],[POINT4],[POINT5],[POINT6],[POINT7],[POINT8],[POINT9],[POINT10],[itemLongDetail_PS],[FEATURE1],[FEATURE2],[FEATURE3],[FEATURE4],[FEATURE5],[FEATURE6],[FEATURE7],[FEATURE8],[FEATURE9],[FEATURE10],[Features],[FormFactor],[BandType],[Band],[PackageContents],[TalkTime],[Standby],[Display],[Condition],[flag1],[UPCCode],[AMZN_sku],[BUY_sku],[itemDimensions],[itemWeight],[DateTimeEntd],[InvLastUpdatedBy],[InvLastUpdatedDate],[PriceLastUpdatedBy],[PriceLastUpdatedDate],[PS_videos],[Smartphone],[NoDiscount],[MobileLine_sku],[Phone_Compatibility],[DesignType],0 as master,0 as ghost,0 as lastCall,0 as topModel_PS,'' as itemLongDetail_ER,'' as itemDesc_ER,[colorID] from we_itemsSlave where itemID = " & createdItemID & "; SELECT @@IDENTITY AS NewID;"
				session("errorSQL") = sql
				set objRS = oConn.execute(sql)
				
				activeModelList = activeModelList & modelID & "##"
				autoDeleteCompPages "updateItem",brandID & "##" & modelID & "##" & subtypeID & "##" & createdItemID
				slavesCreated = slavesCreated + 1
			else
				slavesSkipped = slavesSkipped + 1
			end if
		next
		
		if right(activeModelList,2) = "##" then activeModelList = left(activeModelList,len(activeModelList)-2)
		response.Write("activeModelList:" & activeModelList & "<br>")
		response.Flush()
		activeModelListArray = split(activeModelList,"##")
		for i = 0 to ubound(activeModelListArray)
			sql = "exec sp_createProductListByModelID " & activeModelListArray(i)
			response.Write(sql & "<br>")
			response.Flush()
			session("errorSQL") = sql
			oConn.execute(sql)
		next
		
		sql = "delete from we_ItemsSlave"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		response.Write("Slaves Created:" & slavesCreated & "<br>")
		response.Write("Slaves Skipped:" & slavesSkipped & "<br>")
	end if
%>
<form action="/admin/reverseSlaveMaker.asp" name="rsmForm" method="post" style="margin:0px; padding:0px;">
<table border="0" cellpadding="3" cellspacing="0" align="center" width="300">
    <tr>
    	<td>
        	<div style="float:left; font-weight:bold; text-align:right; width:150px;">New ItemID:</div>
            <div style="float:left; padding-left:10px;"><input type="text" name="newItemID" value="" size="17" tabindex="1" /></div>
        </td>
    </tr>
    <tr>
    	<td style="border-bottom:1px solid #000;">
        	<div style="float:left;" id="newItemPic"></div><br />
			<div style="float:left; padding-left:10px;" id="newItemDesc"></div>
        </td>
    </tr>
    <tr>
    	<td>
        	<div style="float:left; font-weight:bold; text-align:right; width:150px;">Blueprint ItemID:</div>
            <div style="float:left; padding-left:10px;"><input type="text" name="blueprintItemID" value="" size="17" tabindex="2" /></div>
        </td>
    </tr>
    <tr>
    	<td style="border-bottom:1px solid #000;">
        	<div style="float:left;" id="bpItemPic"></div><br />
            <div style="float:left; padding-left:10px;" id="bpItemDesc"></div>
        </td>
    </tr>
    <tr>
    	<td align="right" style="padding-right:13px;"><input type="button" name="myAction" value="Gathering Data" onclick="gatherData(document.rsmForm.newItemID.value,document.rsmForm.blueprintItemID.value,1)" /></td>
    </tr>
    <tr>
    	<td>
        	<div style="float:left;">Model List</div>
            <div style="float:right;">Select <a href="javascript:gatherData(document.rsmForm.newItemID.value,document.rsmForm.blueprintItemID.value,1)">All</a>/<a href="javascript:gatherData(document.rsmForm.newItemID.value,document.rsmForm.blueprintItemID.value,0)">None</a></div>
        </td>
    </tr>
    <tr>
    	<td>
        	<div style="float:left; width:300px; height:200px; overflow:auto; border:1px solid #000;" id="modelResult"></div>
        </td>
    </tr>
    <tr>
    	<td align="right">
        	<input type="hidden" name="modelList" value="" />
            <input type="submit" name="mySub" value="Create Slaves" onclick="return(testing())" />
        </td>
    </tr>
</table>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	var failedReturn = 0
	
	function testing() {
		var o = document.rsmForm.modelIDs;
		for (i=0 ; i< o.length ; i++) {
			if (o[i].checked == true) {
				if (document.rsmForm.modelList.value == "") {
					document.rsmForm.modelList.value = o[i].value
				}
				else {
					document.rsmForm.modelList.value = document.rsmForm.modelList.value + ", " + o[i].value
				}
			}
		}
	}
	
	function gatherData(newItemID,bpItemID,selectOp) {
		failedReturn = 0
		document.getElementById("dumpZone").innerHTML = ""
		ajax('/ajax/admin/ajaxReverseSlaveMaker.asp?newItemID=' + newItemID + '&bpItemID=' + bpItemID + '&selectOp=' + selectOp,'dumpZone')
		//document.getElementById("testZone").innerHTML = '/ajax/admin/ajaxReverseSlaveMaker.asp?newItemID=' + newItemID + '&bpItemID=' + bpItemID + '&selectOp=' + selectOp
		setTimeout("reviewReturn()",500)
	}
	
	function reviewReturn() {
		var fullData = document.getElementById("dumpZone").innerHTML
		if (fullData == "" && failedReturn < 3) {
			failedReturn++
			document.getElementById("testZone").innerHTML = "Trying to get data again: " + failedReturn
			setTimeout("reviewReturn()",500)
		}
		if (fullData == "Blank itemID(s)") {
			alert("One or more of the itemID fields is blank or invalid")
		}
		else if (fullData == "Invalid itemID(s)") {
			alert("One or more of the itemIDs is an invalid item")
		}
		else if (fullData == "Invalid Blueprint") {
			alert("The blueprint item has no models assigned")
		}
		else {
			var newItem = fullData.substring(0,fullData.indexOf("##ItemData##"))
			var bpItem = fullData.substring(fullData.indexOf("##ItemData##")+12,fullData.indexOf("##ModelResults##"))
			var modelList = fullData.substring(fullData.indexOf("##ModelResults##")+16)
			var newItemArray = newItem.split("@@")
			document.getElementById("newItemPic").innerHTML = "<img src='/productPics/thumb/" + newItemArray[2] + "' border='0' />"
			document.getElementById("newItemDesc").innerHTML = newItemArray[1]
			var bpItemArray = bpItem.split("@@")
			document.getElementById("bpItemPic").innerHTML = "<img src='/productPics/thumb/" + bpItemArray[2] + "' border='0' />"
			document.getElementById("bpItemDesc").innerHTML = bpItemArray[1]
			document.getElementById("modelResult").innerHTML = modelList
		}
	}
</script>