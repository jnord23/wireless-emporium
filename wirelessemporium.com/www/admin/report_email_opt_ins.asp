<%
pageTitle = "WE Admin Site - E-Mail Opt-In Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<p>&nbsp;</p>

<%
dim a, store, RS, SQL
for a = 1 to 4
	select case a
		case 1 : store = "WE"
		case 2 : store = "CA"
		case 3 : store = "CO"
		case 4 : store = "PS"
	end select
	SQL = "SELECT COUNT(email) AS OptInCount FROM " & store & "_emailopt"
	SQL = SQL & " WHERE (email NOT IN (SELECT email FROM " & store & "_emailopt_out))"
	SQL = SQL & " AND (email NOT LIKE '%@wirelessemporium.com')"
	SQL = SQL & " AND (email NOT LIKE '%@cellphoneaccents.com')"
	SQL = SQL & " AND (email NOT LIKE '%@cellularoutfitter.com')"
	SQL = SQL & " AND (email NOT LIKE '%@phonesale.com')"
	set RS = Server.CreateObject("ADODB.Recordset")
	'response.write "<h3>" & SQL & "</h3>" & vbCrLf
	'response.end
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		response.write "<h3>" & store & " Opt-In Count: " & RS("OptInCount") & "</h3>" & vbcrlf
	end if
next
RS.close
set RS = nothing
%>

<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
