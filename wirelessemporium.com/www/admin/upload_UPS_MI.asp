<%
' NOTES:
' 1)  Do itemID's have to have +300001 added to them?
' 2)  Need to add Squaretrade
'**************************************************************************

response.buffer = false
Server.ScriptTimeout = 9000 'seconds
pageTitle = "Upload UPS Mail Innovations Shipments -"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
dim fileDate, redo, strError, strComplete
fileDate = request.form("fileDate")
redo = request.form("redo")
if isnull(redo) or len(redo) < 1 then redo = 0
strError = ""
strComplete = ""

dim ftp, myReadFile, myFile, myFolder, localDir, remoteDir, success, numFilesDownloaded
if fileDate <> "" then
	if len(fileDate) <> 8 or mid(fileDate,3,1) <> "-" or mid(fileDate,6,1) <> "-" then strError = "Date is not in the correct format."
	if strError = "" then
		set ftp = server.CreateObject("Chilkat.Ftp2")
		success = ftp.UnlockComponent("WIRELEFTP_S8B5SYv09Bum")
		if (success <> 1) then
			response.write "Unlock Failed<br /><p>" & ftp.LastErrorText & "</p>" & vbcrlf
			response.End()
		end if
		
		myFile = server.mappath("\admin\UPS\UPStemp.csv")
		myFolder = server.mappath("\admin\UPS") & "\"
		
		' ***DELETE***
		'myDate = "10-20-09"
		myDate = fileDate
		myReadFile = "REPORT_MI01_*" & myDate & "*.TXT"
		
		ftp.Hostname = "ftp2.ups.com"
		ftp.Username = "wireless0709"
		ftp.Password = "9u1v0k"
		ftp.Passive = 0
		ftp.Port = 21
		
		' Connect and login to the FTP server.
		success = ftp.Connect()
		if (success <> 1) then
			response.write "No Connection Made<br /><p>" & replace(ftp.LastErrorText,vbcrlf,"<br>") & "</p>" & vbcrlf
			response.End()
		end if
		
		' Change to the remote directory where the file will be uploaded.
		success = ftp.ChangeRemoteDir("/outbound")
		if (success <> 1) then
			response.write "Cannot Find Outbound Directory<br /><p>" & replace(ftp.LastErrorText,vbcrlf,"<br>") & "</p>" & vbcrlf
			response.End()
		end if
		
				
		' Download all files matching pattern in myReadFile.
		numFilesDownloaded = ftp.MGetFiles(myReadFile,myFolder)
'		response.write "numFilesDownloaded:" & numFilesDownloaded & "<br>"
		if numFilesDownloaded <= 0 then
			response.write "Cannot Find Matching Files<br /><p>" & replace(ftp.LastErrorText,vbcrlf,"<br>") & "</p>" & vbcrlf
			response.End()
		end if
		
		ftp.Disconnect
		set ftp = nothing
		
		dim filesys, demofolder, filecoll, fil, FileDownloaded
		set filesys = server.CreateObject("Scripting.FileSystemObject")
		set demofolder = filesys.GetFolder(myFolder)
		set filecoll = demofolder.Files
		for each fil in filecoll
			if fil.name = myFile then 
'				response.write "delete file" & "<br>"
				filesys.DeleteFile fil.path
			end if
		next
		for each fil in filecoll
			if inStr(fil.name,"REPORT_MI01_") > 0 and inStr(fil.name,"_" & myDate) > 0 then
				if redo <> "1" then filesys.CopyFile fil.path, myFile
				FileDownloaded = fil.name
			end if
		next
		set demofolder = nothing
		set filecoll = nothing
		set filesys = nothing
		
'		response.write "FileDownloaded:" & FileDownloaded & "<br>"
		
		if FileDownloaded <> "" then
			bodyText = "<p>File Downloaded: " & FileDownloaded & "</p>" & vbcrlf
			dim txtConn, myStr
			set txtConn = server.CreateObject("ADODB.Connection")
			myStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & myFolder & ";Extended Properties=""text;HDR=Yes;FMT=Delimited"";"
			txtConn.ConnectionString = myStr
			txtConn.ConnectionTimeout = 1000		'seconds
			txtConn.Open
			
			dim SQL, RS, MaxOrderID
			SQL = "SELECT MAX(orderID) AS MaxOrderID FROM we_orders"
			set RS = server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			if not RS.eof then
				MaxOrderID = RS("MaxOrderID")
			else
				MaxOrderID = 550000
			end if
			
			dim OrderNumber, bodyText, aCount
			aCount = 0
			SQL = "SELECT * FROM UPStemp.csv ORDER BY [PACKAGE ID]"
			set RS = server.CreateObject("ADODB.Recordset")
			RS.open SQL, txtConn, 3, 3
			do until RS.eof
				OrderNumber = RS("PACKAGE ID")
				
				if isnull(OrderNumber) then
					OrderNumber = ""
				else
					OrderNumber = replace(Lcase(OrderNumber),"we","")				
				end if
				
				if isNumeric(OrderNumber) then
					if int(OrderNumber) = cDbl(OrderNumber) then
						OrderNumber = int(OrderNumber)
'						if OrderNumber > MaxOrderID - 20000 and OrderNumber < MaxOrderID + 20000 then
							SQL = "UPDATE we_orders SET"
							SQL = SQL & " trackingNum='" & RS("SEQUENCE") & "',"
							SQL = SQL & " confirmSent='yes',"
							SQL = SQL & " confirmdatetime='" & now & "'"
							SQL = SQL & " WHERE orderid='" & OrderNumber & "'"
							response.write SQL & "<br>" & vbcrlf
							oConn.execute SQL
							call SendConfirmEmails(OrderNumber)
							bodyText = bodyText & "<p>Order Number " & OrderNumber & " processed with Tracking Number " & RS("SEQUENCE") & "</p>" & vbcrlf
							aCount = aCount + 1
'						else
'							bodyText = bodyText & "<p>OUT-OF-RANGE Order Number: " & OrderNumber & "!</p>" & vbcrlf
'						end if
					else
						bodyText = bodyText & "<p>INVALID Order Number: " & OrderNumber & "!</p>" & vbcrlf
					end if
				else
					bodyText = bodyText & "<p>INVALID Order Number: " & OrderNumber & "!</p>" & vbcrlf
				end if
				RS.movenext
			loop
			
			if bodyText <> "" then
				bodyText = bodyText & "<h3>" & aCount & " Packages Processed</h3>" & vbcrlf
				CDOSend "charles@wirelessemporium.com,ruben@wirelessemporium.com,service@wirelessemporium.com,webmaster@wirelessemporium.com;terry@wirelessemporium.com","service@wirelessemporium.com","UPS Mail Innovations Report for " & date,bodyText
'				CDOSend "terry@wirelessemporium.com","service@wirelessemporium.com","UPS Mail Innovations Report for " & date,bodyText
			end if
			
			RS.close
			set RS = nothing
			txtConn.close
			set txtConn = nothing
		end if
		
		strComplete = "<h3>" & fileDate & " COMPLETE!</h3>"
		response.write strComplete & vbcrlf
	end if
end if

if strError <> "" or strComplete = "" then
	response.write "<h4>" & pageTitle & "</h4>" & vbcrlf
	response.write "<br>" & vbcrlf
	if strError <> "" then response.write "<h3><font color=""#FF0000"">ERROR: " & strError & "</font></h3>" & vbcrlf
	
	set ftp = server.CreateObject("Chilkat.Ftp2")
	success = ftp.UnlockComponent("WIRELEFTP_S8B5SYv09Bum")
	if (success <> 1) then
		response.write "Unlock Failed<br /><p>" & ftp.LastErrorText & "</p>" & vbcrlf
		response.End()
	end if
	
	ftp.Hostname = "ftp2.ups.com"
	ftp.Username = "wireless0709"
	ftp.Password = "9u1v0k"
	ftp.Passive = 0
	ftp.Port = 21
	
	' Connect and login to the FTP server.
	success = ftp.Connect()
	if (success <> 1) then
		response.write "No Connection Made<br /><p>" & replace(ftp.LastErrorText,vbcrlf,"<br>") & "</p>" & vbcrlf
		response.End()
	end if
	
	' Change to the remote directory where the file will be uploaded.
	success = ftp.ChangeRemoteDir("/outbound")
	if (success <> 1) then
		response.write "Cannot Find Directory<br /><p>" & replace(ftp.LastErrorText,vbcrlf,"<br>") & "</p>" & vbcrlf
		response.End()
	end if
	
	n = ftp.NumFilesAndDirs
	if (n < 0) then
		response.write "NumFilesAndDir<br /><p>" & replace(ftp.LastErrorText,vbcrlf,"<br>") & "</p>" & vbcrlf
		response.End()
	end if
	
	if (n > 0) then
		for i = 0 To n - 1
			'  Display the filename
			response.write ftp.GetFilename(i) & "<br>" & vbcrlf
		next
	end if
	
	ftp.Disconnect
	set ftp = nothing
	%>
	<br>
	<h3>Enter the date of the UPS Mail Innovations file in this format: MM-DD-YY</h3>
	<h3>(Hyphens and leading zeros are REQUIRED!)</h3>
	<form action="upload_UPS_MI.asp" method="post">
		<p><input type="text" name="fileDate" size="8" maxlength="8"></p>
		<p><input type="checkbox" name="redo" value="1">&nbsp;Do-Over</p>
		<p><input type="submit" name="upload" value="Upload"></p>
	</form>
	<%
end if

sub SendConfirmEmails(sOrderID)
	'response.write "<p>" & sOrderID & "</p>" & vbcrlf
	dim RS2, RS3, RSOrderDetails
	dim nOrderID, extOrderType, nCount, sPromoCode, nOrderGrandTotal, nOrderSubTotal, nOrderTax, nShipFee
	dim storeID, storeAbbr, storeName, imgLink, sHeading, statusLink
	dim nID, nPrice, nQty
	dim sShipAddress, cdo_from, cdo_to, cdo_subject, cdo_body, regText, headerText
	if sOrderID <> "" then
		SQL = "SELECT orderID, store, accountID, shippingid, extOrderType, ordersubtotal, ordershippingfee, shiptype, orderTax, ordergrandtotal FROM we_Orders WHERE orderID = '" & sOrderID & "'"
		response.Write(sql & "<br>")
		set RS3 = server.CreateObject("ADODB.Recordset")
		RS3.open SQL, oConn, 3, 3
		do until RS3.eof
			nOrderID = RS3("orderID")
			extOrderType = RS3("extOrderType")
			storeID = RS3("store")
			nOrderGrandTotal = RS3("ordergrandtotal")
			nOrderSubTotal = RS3("ordersubtotal")
			nOrderTax = RS3("orderTax")
			nShipFee = RS3("ordershippingfee")
			SQL = "SELECT B.PromoCode FROM we_orders A INNER JOIN we_coupons B ON A.couponid=B.couponid WHERE A.orderid='" & nOrderID & "'"
			set RS2 = Server.CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			if not RS2.eof then
				sPromoCode = RS2("PromoCode")
			else
				sPromoCode = ""
			end if
			select case storeID
				case 0
					storeAbbr = "WE_"
					storeName = "WirelessEmporium"
					imgLink = "http://www.wirelessemporium.com/images/global/we_logo_solo.gif"
					sHeading = "Order #" & nOrderID & " Successfully Shipped from WirelessEmporium.com"
					'statusLink = "http://www.wirelessemporium.com/orderstatus.asp"
					regText = "font-family: Arial; font-size: 9pt;"
					headerText = "font-family: Arial; font-size: 11pt;"
				case 1
					storeAbbr = "CA_"
					storeName = "CellphoneAccents"
					imgLink = "http://www.cellphoneaccents.com/images/logo.jpg"
					sHeading = "Your CellphoneAccents.com order #" & nOrderID & " is on the way!"
					'statusLink = "http://www.cellphoneaccents.com/track-your-order.asp"
					regText = "font-family: Tahoma; font-size: 9pt;"
					headerText = "font-family: Tahoma; font-size: 11pt;"
				case 2
					storeAbbr = "CO_"
					storeName = "CellularOutfitter"
					imgLink = "http://www.cellularoutfitter.com/images/CellularOutfitter_STANDARD.jpg"
					sHeading = "CellularOutfitter.com Order #" & nOrderID & " - Shipment Status"
					'statusLink = "http://www.cellularoutfitter.com/track-your-order.asp"
					regText = "font-family: Times; font-size: 12pt;"
					headerText = "font-family: Times; font-size: 14pt;"
				case 3
					storeAbbr = "PS_"
					storeName = "Phonesale"
					imgLink = "http://www.phonesale.com/images/phonesale.jpg"
					sHeading = "Phonesale.com Order #" & nOrderID & " - Shipment Status"
					'statusLink = "http://www.phonesale.com/track-your-order.asp"
					regText = "font-family: Times; font-size: 12pt;"
					headerText = "font-family: Times; font-size: 14pt;"
			end select
			
			statusLink = "http://www.ups-mi.net/packageID/PackageID.aspx?PID=WE" & nOrderiD
			
			if isNull(RS3("shippingid")) or RS3("shippingid") = 0 then
				SQL = "SELECT fname, lname, sAddress1, sAddress2, sCity, sState, sZip, sCountry, email, phone FROM " & storeAbbr & "Accounts WHERE accountID = '" & RS3("accountID") & "'"
			else
				SQL = "SELECT A.*, B.fname, B.lname, B.email FROM we_addl_shipping_addr A INNER JOIN " & storeAbbr & "Accounts B ON A.accountID=B.accountID WHERE A.id = '" & RS3("shippingid") & "'"
			end if
			dim fname, lname, sAddress1, sAddress2, sCity, sState, sZip
			set RS2 = server.CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			if not RS2.eof then
				fname = RS2("fname")
				lname = RS2("lname")
				sAddress1 = RS2("sAddress1")
				sAddress2 = RS2("sAddress2")
				sCity = RS2("sCity")
				sState = RS2("sState")
				sZip = RS2("sZip")
			end if
			sShipAddress = fFormatAddress(sAddress1,sAddress2,sCity,sState,sZip)
			cdo_from = storeName & "<sales@" & Lcase(storeName) & ".com>"
			cdo_to = "" & RS2("email")
			'cdo_to = cdo_to & ",webmaster@" & storeName & ".com"
			cdo_subject = sHeading
			RS2.close
			set RS2 = nothing
			cdo_body = "<html>" & vbcrlf
			cdo_body = cdo_body & "<head>" & vbcrlf
			cdo_body = cdo_body & "<style type='text/css'>" & vbcrlf
			cdo_body = cdo_body & "<!--" & vbcrlf
			cdo_body = cdo_body & ".regText {" & regText & "}" & vbcrlf
			cdo_body = cdo_body & ".headerText {" & headerText & "}" & vbcrlf
			cdo_body = cdo_body & "-->" & vbcrlf
			cdo_body = cdo_body & "</style>" & vbcrlf
			cdo_body = cdo_body & "</head>" & vbcrlf
			cdo_body = cdo_body & "<body class='regText'>"
			cdo_body = cdo_body & "<table width='100%' cellpadding='8' cellspacing='0' align='center' bgcolor='#eaeaea' class='regText'><tr>" & vbcrlf
			cdo_body = cdo_body & "<td class='headerText' align='left' valign='top'><img src='" & imgLink & "' border='0' hspace='12' align='absmiddle'></td>" & vbcrlf
			cdo_body = cdo_body & "<td class='headerText' align='center' valign='middle'><b>" & sHeading & "</b></td>" & vbcrlf
			cdo_body = cdo_body & "</tr></table>" & vbcrlf
			cdo_body = cdo_body & "<table width='95%' cellpadding='5' cellspacing='1' align='center' class='regText'>" & vbcrlf
			cdo_body = cdo_body & "<tr><td colspan='2'><br><p><b>Hello " & Ucase(left(fname, 1)) & right(fname,(len(fname)-1)) & ":</b></p>"
			cdo_body = cdo_body & "<p>Thank you for ordering from " & storeName & ".com! We would like to notify you that your order has been SHIPPED to:</p>"& vbcrlf
			cdo_body = cdo_body & "</td></tr>" & vbcrlf
			cdo_body = cdo_body & "<tr><td colspan='2'><b>" & sShipAddress & "</b><br></tr></td>"
			cdo_body = cdo_body & "</table>" & vbcrlf
			cdo_body = cdo_body & "<table width='95%' cellpadding='5' cellspacing='1' align='center' bgcolor='#eaeaea' class='regText'>" & vbcrlf
			cdo_body = cdo_body & "<tr><td class='regText'><b>ORDER DETAILS:</b></td></tr>" & vbcrlf
			cdo_body = cdo_body & "</table>" & vbcrlf
			cdo_body = cdo_body & "<table width='95%' cellpadding='5' cellspacing='1' align='center' class='regText'>" & vbcrlf
			cdo_body = cdo_body & fFormatEmailBodyRow("ORDER ID:",nOrderID)
			'get orderdetails
			SQL = "SELECT A.itemid, A.typeid, A.carrierid, A.itemdesc, A.itemDesc_CO, A.itemDesc_PS, A.price_our, A.price_CA, A.price_CO, A.price_PS,"
			SQL = SQL & " B.quantity, C.typename FROM we_items A"
			SQL = SQL & " INNER JOIN we_orderdetails B ON A.itemid=B.itemid"
			SQL = SQL & " INNER JOIN we_types C ON A.typeid=C.typeid"
			SQL = SQL & " WHERE B.orderid = '" & nOrderID & "'"
			cdo_body = cdo_body & "<tr class='regText'><td valign='top' bgcolor='#eaeaea'><b>ITEMS:</b></td><td>"
			set RSOrderDetails = Server.CreateObject("ADODB.Recordset")
			RSOrderDetails.open SQL, oConn, 3, 3
			
			if not RSOrderDetails.eof then
				nCount = 1
				do until RSOrderDetails.eof
					nID = RSOrderDetails("itemid")
					nQty = RSOrderDetails("quantity")
					select case storeID
						case 0
							nPrice = RSOrderDetails("price_our")
							cdo_body = cdo_body & "<b>" & nCount & ": </b><a href='http://www.wirelessemporium.com/p-" & nID & "-" & formatSEO(RSOrderDetails("itemDesc")) & ".asp'>" & RSOrderDetails("itemdesc") & "</a>"
						case 1
							nPrice = RSOrderDetails("price_CA")
							cdo_body = cdo_body & "<b>" & nCount & ": </b><a href='http://www.cellphoneaccents.com/p-" & nID & "-" & formatSEO_CA(RSOrderDetails("itemDesc")) & ".html'>" & RSOrderDetails("itemdesc") & "</a>"
						case 2
							nPrice = RSOrderDetails("price_CO")
							cdo_body = cdo_body & "<b>" & nCount & ": </b><a href='http://www.cellularoutfitter.com/p-" & nID & "-" & formatSEO_CO(RSOrderDetails("itemDesc_CO")) & ".html'>" & RSOrderDetails("itemDesc_CO") & "</a>"
						case 3
							nPrice = RSOrderDetails("price_PS")
							dim thetype, ntc, carriername, rsRecent2
							if RSOrderDetails("typeid") = 16 then
								thetype = "cell-phones"
								ntc = 16
							else
								thetype = formatSEO_PS(RSOrderDetails("typename"))
								ntc = 17
							end if
							if isnull(RSOrderDetails("carrierid")) or RSOrderDetails("carrierid") = "" or RSOrderDetails("carrierid") = 0 then
								carriername = "universal" & "-" & thetype
							else
								SQL = "SELECT carriername FROM we_Carriers WHERE id='" & RSOrderDetails("carrierid") & "'"
								set rsRecent2 = Server.CreateObject("ADODB.Recordset")
								rsRecent2.open SQL, oConn, 3, 3
								if rsRecent2.eof then
									carriername = "universal" & "-" & thetype
								else
									carriername = formatSEO_PS(rsRecent2("carriername")) & "-" & thetype
								end if
								rsRecent2.close
								set rsRecent2 = nothing
							end if
							cdo_body = cdo_body & "<b>" & nCount & ": </b><a href='http://www.phonesale.com/" & carriername & "/p-" & nID & "-tc-" & ntc & "-" & formatSEO_PS(RSOrderDetails("itemDesc_PS")) & ".html'>" & RSOrderDetails("itemDesc_PS") & "</a>"
						case 10
							nPrice = RSOrderDetails("price_ER")
							cdo_body = cdo_body & "<b>" & nCount & ": </b><a href='http://www.ereadersupply.com/p-" & nID & "-" & formatSEO_ER(RSOrderDetails("itemDesc")) & ".html'>" & RSOrderDetails("itemDesc") & "</a>"
					end select
					cdo_body = cdo_body & "<br>&nbsp;&nbsp;&nbsp;Quantity: " & nQty & " | Price: " & FormatCurrency(nPrice,2)
					cdo_body = cdo_body & "<br>&nbsp;&nbsp;&nbsp;Subtotal: " & FormatCurrency(CDbl(nQty) * CDbl(nPrice),2) & "<hr size='1' color='#eaeaea' noshade>"
					nCount = nCount + 1
					RSOrderDetails.movenext
				loop
			end if
			RSOrderDetails.close
			set RSOrderDetails = nothing
			cdo_body = cdo_body & "</td></tr>" & vbcrlf
			cdo_body = cdo_body & fFormatEmailBodyRow("ORDER SUBTOTAL:",FormatCurrency(nOrderSubTotal,2))
			if sPromoCode <> "" then cdo_body = cdo_body & fFormatEmailBodyRow("PROMO CODE:",sPromoCode)
			if nOrderGrandTotal - nOrderSubTotal - nOrderTax - nShipFee < 0 then cdo_body = cdo_body & fFormatEmailBodyRow("DISCOUNT:",FormatCurrency(nOrderGrandTotal - nOrderSubTotal - nOrderTax - nShipFee,2))
			dim sTrackingURL
			sTrackingURL = "<a href='http://www.ups-mi.net/packageID/PackageID.aspx?PID=WE" & nOrderiD & "'>Click Here</a>"
			cdo_body = cdo_body & fFormatEmailBodyRow("CA RESIDENT TAX (7.75%):",FormatCurrency(nOrderTax,2))
			cdo_body = cdo_body & fFormatEmailBodyRow("SHIPPING TYPE:",RS3("shiptype"))
			cdo_body = cdo_body & fFormatEmailBodyRow("SHIPMENT TRACKING:",sTrackingURL)
			cdo_body = cdo_body & fFormatEmailBodyRow("SHIPPING FEE:",FormatCurrency(nShipFee,2))
			cdo_body = cdo_body & "<tr><td bgcolor='#EAEAEA' class='headerText'><b>GRAND TOTAL: </b></td>"
			cdo_body = cdo_body & "<td class='headerText'>" & FormatCurrency(nOrderGrandTotal,2)
			cdo_body = cdo_body & "</td></tr></table>"
			cdo_body = cdo_body & "</td></tr>"
			cdo_body = cdo_body & "</table><br>" & vbcrlf
			cdo_body = cdo_body & "<table width='95%'><tr><td class='regText'>"
			cdo_body = cdo_body & "<p>Your order will be shipped in accordance with your selected delivery option. Arrival time for your order will vary depending on your selected delivery option, or your proximity to Southern California (for First Class Mail).</p>" & vbcrlf
			cdo_body = cdo_body & "<p>Thanks again for your business. Should you have any additional questions regarding your order, our site and/or services, please feel free to contact us at <a href='mailto:sales@" & Lcase(storeName) & ".com'>sales@" & Lcase(storeName) & ".com</a>. We are eager to serve your wireless needs again in the future.</p>"
			cdo_body = cdo_body & "</td></tr></table>"
			cdo_body = cdo_body & "<table width='100%' cellpadding='5' cellspacing='5' align='center' bgcolor='#eaeaea'><tr><td colspan='2' class='regText'><a href='" & statusLink & "'><b>Check Your Order Status Online</b></a></td></tr>" & vbcrlf
			cdo_body = cdo_body & "<tr><td colspan='2' class='regText'><b>Please visit us again soon!</b> - <a href='http://www." & Lcase(storeName) & ".com/'>" & storeName & ".com</a></td></tr></table>" & vbcrlf
			cdo_body = cdo_body & "</body></html>"
			response.Write("email prepaired (" & extOrderType & "," & redo & ")<br />")
			if isnull(extOrderType) or len(extOrderType) < 1 then extOrderType = 0
			if extOrderType <> 6 and redo <> "1" then
				CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
				response.Write("email sent (" & cdo_to & ")<br />")
			end if
			RS3.moveNext
		loop
		RS3.close
		set RS3 = nothing
	end if
end sub

function fFormatEmailBodyRow(sDescription,sValue)
	fFormatEmailBodyRow = "<tr><td valign='top' width='125' bgcolor='#eaeaea'><b>" & sDescription & "</b></td><td valign='top'>" & sValue & "</td></tr>" & vbcrlf
end function

function fFormatAddress(sAdd1,sAdd2,sCity,sState,sZip)
	dim cdo_body
	cdo_body = sAdd1
	if sAdd2 <> "" then cdo_body = cdo_body & "<br>" & sAdd2
	cdo_body = cdo_body & "<br>" & sCity & ", " & sState & "&nbsp;" & sZip & "<br>" & vbcrlf
	fFormatAddress = cdo_body
end function

function formatSEO_CA(val)
	if not isNull(val) then
		formatSEO_CA = lCase(trim(replace(val,"&trade;","-")))
		formatSEO_CA = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO_CA," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO_CA = replace(replace(replace(replace(formatSEO_CA,"(","-"),")","-"),";","-"),":","-")
	else
		formatSEO_CA = ""
	end if
	select case formatSEO_CA
		case "sidekick" : formatSEO_CA = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO_CA = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO_CA = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO_CA = "sprint-nextel"
		case "u-s--cellular" : formatSEO_CA = "us-cellular"
		case "antennas-parts" : formatSEO_CA = "antennas"
		case "faceplates" : formatSEO_CA = "films-faceplates"
		case "bling-kits---charms" : formatSEO_CA = "charms-bling-kits"
		case "data-cable---memory" : formatSEO_CA = "data-cables-memory-cards"
		case "hands-free" : formatSEO_CA = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO_CA = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO_CA = "holsters-phone-holders"
		case "leather-cases" : formatSEO_CA = "cases-pouches-skins"
		case "full-body-protectors" : formatSEO_CA = "protective-films"
	end select
end function

function formatSEO_CO(val)
	if not isNull(val) then
		formatSEO_CO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO_CO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO_CO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO_CO = replace(replace(replace(replace(replace(formatSEO_CO,"(","-"),")","-"),";","-"),"+","-"),":","-")
	else
		formatSEO_CO = ""
	end if
	select case formatSEO_CO
		case "sidekick" : formatSEO_CO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO_CO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO_CO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO_CO = "sprint-nextel"
		case "u-s--cellular" : formatSEO_CO = "us-cellular"
		case "antennas-parts" : formatSEO_CO = "antennas"
		case "faceplates" : formatSEO_CO = "covers-screen-guards"
		case "bling-kits---charms" : formatSEO_CO = "bling-kits-charms"
		case "data-cable---memory" : formatSEO_CO = "data-cables-memory-cards"
		case "hands-free" : formatSEO_CO = "hands-free-kits-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO_CO = "holsters-car-mounts"
		case "leather-cases" : formatSEO_CO = "cases-pouches"
		case "cell-phones" : formatSEO_CO = "wholesale-cell-phone"
		case "full-body-protectors" : formatSEO_CO = "invisible-film-protectors"
	end select
end function

function formatSEO_PS_PS(val)
	if not isNull(val) then
		formatSEO_PS = lCase(trim(replace(val,"&trade;","-")))
		formatSEO_PS = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO_PS," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO_PS = replace(replace(replace(replace(replace(formatSEO_PS,"(","-"),")","-"),";","-"),":","-"),"+","-")
		formatSEO_PS = replace(formatSEO_PS,"---","-")
		formatSEO_PS = replace(formatSEO_PS,"--","-")
	else
		formatSEO_PS = ""
	end if
	select case formatSEO_PS
		case "sidekick" : formatSEO_PS = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO_PS = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO_PS = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO_PS = "sprint-nextel"
		case "u-s--cellular" : formatSEO_PS = "us-cellular"
		case "antennas-parts" : formatSEO_PS = "antennas"
		case "faceplates" : formatSEO_PS = "faceplates-skins"
		case "verizon" : formatSEO_PS = "verizon-wireless"
		case "unlocked" : formatSEO_PS = "unlocked"
		case "audiovox" : formatSEO_PS = "utstarcom"
		case "data-cable-memory" : formatSEO_PS = "data-cables-memory-cards"
		case "hands-free" : formatSEO_PS = "hands-free-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO_PS = "holsters-holders"
		case "leather-cases" : formatSEO_PS = "cases-and-covers"
		case "full-body-protectors" : formatSEO_PS = "full-body-skins"
	end select
	formatSEO_PS = replace(formatSEO_PS,"---","-")
	formatSEO_PS = replace(formatSEO_PS,"--","-")
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function

function formatSEO_ER(val)
	if not isNull(val) then
		formatSEO_ER = lCase(trim(replace(val,"&trade;","-")))
		formatSEO_ER = replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO_ER," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO_ER = replace(replace(replace(replace(formatSEO_ER,"(","-"),")","-"),";","-"),"+","-")
	else
		formatSEO_ER = ""
	end if
	select case formatSEO_ER
		'case "apple" : formatSEO = "apple-ipad"
	end select
end function
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
