<%
pageTitle = "Admin - Update WE Database - Update Part Numbers"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<blockquote>
<blockquote>

<font face="Arial,Helvetica">
	<p><a href="/admin/db_search_models_types.asp?searchID=3">Back to Types</a></p>
</font>

<%
function replaceNull(myVal)
	if isNull(myVal) then
		replaceNull = "&nbsp;"
	else
		replaceNull = myVal
	end if
end function

if request("submitted") = "Update" then
	for a = 1 to request("totalCount")
		SQL = "UPDATE WE_Items SET PartNumber='" & request("PartNumber" & a) & "'"
		SQL = SQL & " WHERE itemID=" & request("itemID" & a)
		'response.write "<p>" & SQL & "</p>"
		oConn.execute SQL
	next
	%>
	<h3>UPDATED!</h3>
	<%
else
	SQL = "SELECT A.*, B.TypeName, C.BrandName, D.ModelName FROM"
	SQL = SQL & " ((WE_Items A INNER JOIN WE_Types B ON A.TypeID = B.TypeID)"
	SQL = SQL & " INNER JOIN WE_Brands C ON A.BrandID = C.BrandID)"
	SQL = SQL & " INNER JOIN WE_Models D ON A.ModelID = D.ModelID"
	SQL = SQL & " WHERE A.TypeID='" & request("TypeID") & "'"
	SQL = SQL & " ORDER BY "
	select case request("sort1")
		case "brand" : SQL = SQL & "C.BrandName,"
		case "model" : SQL = SQL & "D.ModelName,"
		case "description" : SQL = SQL & "A.itemDesc,"
		case "subtype" : SQL = SQL & "A.subtypeID,"
	end select
	if request("sort1") <> request("sort2") then
		select case request("sort2")
			case "brand" : SQL = SQL & "C.BrandName,"
			case "model" : SQL = SQL & "D.ModelName,"
			case "description" : SQL = SQL & "A.itemDesc,"
			case "subtype" : SQL = SQL & "A.subtypeID,"
		end select
	end if
	SQL = SQL & "A.itemID"
	'response.write "<h3>" & SQL & "</h3>"
	'response.end
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	a = 0
	if RS.eof then
	    response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
	else
		BrandID = RS("brandID")
		%>
		<font face="Arial,Helvetica"><h3><%=RS.recordcount%> items found</h3></font>
		<font face="Arial,Helvetica"><h3 align="center"><%=RS("TypeName")%></h3></font>
		<form name="frmEditItem" action="db_update_part_numbers.asp" method="post">
			<table border="1" cellpadding="2" cellspacing="0" width="830">
				<tr>
					<td align="center" width="20"><b>ID</b></td>
					<td align="center" width="400"><b>Description</b></td>
					<td align="center" width="100"><b>Brand</b></td>
					<td align="center" width="150"><b>Model</b></td>
					<td align="center" width="10"><b>st</b></td>
					<td align="center" width="150"><b>Part&nbsp;Number</b></td>
				</tr>
				<%
				do until RS.eof
					a = a + 1
					if not isNull(RS("PartNumber")) and not isEmpty(RS("PartNumber")) and RS("PartNumber") <> "" then
						PartNumber = RS("PartNumber")
					else
						PartNumber = ""
						'PartNumber = Ucase(left(RS("brandName"),2)) & "-"
						'PartNumber = PartNumber & Ucase(left(RS("ModelID"),2)) & "-"
						'PartNumber = PartNumber & Ucase(left(RS("ModelID"),5)) & "-"
						'PartNumber = RS("brandCode") & "-"
						'select case RS("BrandID")
							'case 1 : PartNumber = PartNumber & "BAT-"
							'case 2 : PartNumber = PartNumber & "CCH-"
							'case 3 : PartNumber = PartNumber & "-"
							'case 4 : PartNumber = PartNumber & "KPD-"
							'case 5 : PartNumber = PartNumber & "HFR-"
							'case 6 : PartNumber = PartNumber & "HST-"
							'case 7 : PartNumber = PartNumber & "-"
							'case 8 : PartNumber = PartNumber & "-"
							'case 9 : PartNumber = PartNumber & "-"
							'case 12 : PartNumber = PartNumber & "-"
							'case 13 : PartNumber = PartNumber & "-"
							'case 14 : PartNumber = PartNumber & "-"
						'end select
						'PartNumber = PartNumber & Ucase(left(RS("ModelID"),2)) & "-"
						'PartNumber = PartNumber & Ucase(left(RS("ModelID"),5)) & "-"
						'PartNumber = PartNumber & RS("itemID")
					end if
					%>
					<tr>
						<td align="center"><%=RS("itemID")%></td>
						<td align="center">
							<%=RS("itemDesc")%>
							<input type="hidden" name="itemID<%=a%>" value="<%=RS("itemID")%>">
						</td>
						<td align="center"><%=RS("BrandName")%></td>
						<td align="center"><%=RS("ModelName")%></td>
						<td align="center"><%=replaceNull(RS("subtypeID"))%></td>
						<td align="center">
							<input type="text" name="PartNumber<%=a%>" size="15" value="<%=PartNumber%>" maxlength="17">
						</td>
					</tr>
					<%
					RS.movenext
				loop
				%>
				<tr>
					<td bgcolor="black" colspan="6"><font size="1">&nbsp;</font></td>
				</tr>
			</table>
			<p>
				<input type="submit" name="submitted" value="Update">
				<input type="hidden" name="totalCount" value="<%=a%>">
			</p>
		</form>
		<p>&nbsp;</p>
		<%
	end if
	RS.close
	Set RS = nothing
end if
%>

</blockquote>
</blockquote>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
