<%
'Customer Follow-up emails" on Prio. sheet
'21-day should not include products ordered by same e-mail address
pageTitle = ""
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<%
						if request("submitted") <> "" then
							strError = ""
							dateStart = request("dateStart")
							dateEnd = request("dateEnd")
							if not isDate(dateStart) or not isDate(dateEnd) then
								strError = strError & "<p>dateStart and dateEnd must both be valid dates</p>"
							else
								dateStart = dateValue(dateStart)
								dateEnd = dateValue(dateEnd)
								if dateEnd < dateStart then strError = strError & "<p>dateEnd must be later than or equal to dateStart</p>"
							end if
							if strError = "" then
								dim SQL, RS, strOrderStartNumber
								SQL = "SELECT a.orderid, a.orderdatetime, b.email"
								SQL = SQL & " FROM we_orders AS a INNER JOIN we_accounts AS b ON a.accountid = b.accountid"
								SQL = SQL & " WHERE (a.orderdatetime >= '3/1/2009') AND (a.approved = 1) AND (a.cancelled IS NULL OR a.cancelled = 0) AND (a.store = 0) AND (b.email IN"
								SQL = SQL & " (SELECT email FROM"
								SQL = SQL & " (SELECT b_1.email, a.orderid, a.accountid FROM we_orders AS a INNER JOIN"
								SQL = SQL & " (SELECT email, accountid FROM we_accounts WHERE (email IN"
								SQL = SQL & " (SELECT email FROM we_accounts AS we_accounts_1"
								SQL = SQL & " GROUP BY email HAVING (COUNT(*) > 1)))) AS b_1 ON a.accountid = b_1.accountid"
								SQL = SQL & " WHERE (a.orderdatetime >= '" & dateStart & "' AND a.orderdatetime <= '" & dateEnd & "') AND (a.approved = 1) AND (a.cancelled IS NULL OR a.cancelled = 0) AND (a.store = 0))"
								SQL = SQL & " AS derivedtbl_1 GROUP BY email HAVING (COUNT(*) > 1)))"
								SQL = SQL & " ORDER BY b.email, a.orderid"
								set RS = Server.CreateObject("ADODB.Recordset")
								RS.open SQL, oConn, 3, 3
								response.write "<h3>Display Multiple Wireless Emporium Orders by the Same Person<br>Between " & dateStart & " and " & dateEnd & "</h3>" & vbcrlf
								if RS.eof then
									response.write "No records matched<br><br>So cannot make table..."
								else
									%>
									<h3><%=RS.recordcount%> records found</h3>
									<table border="1">
										<tr>
											<%
											for each whatever in RS.fields
												%>
												<td><b><%=whatever.name%></b></td>
												<%
											next
											%>
										</tr>
										<%
										holdEmail = "99999"
										do until RS.eof
											if inStr(RS("email"),"@wirelessemporium.com") = 0 then
												%>
												<tr>
													<td valign="top" class="normaltext"><%=RS("orderid")%></td>
													<td valign="top" class="normaltext"><%=RS("orderdatetime")%></td>
													<td valign="top" class="normaltext">
														<%
														if Ucase(RS("email")) = Ucase(holdEmail) then
															response.write "&nbsp;"
														else
															response.write "<a href=""mailto:" & RS("email") & "?subject=Some%20Subject%20Goes%20Here"">" & RS("email") & "</a>"
														end if
														holdEmail = RS("email")
														%>
													</td>
												</tr>
												<%
											end if
											RS.movenext
										loop
										%>
									</table>
									<%
								end if
							end if
						end if
						%>
						<p>
						<form action="report_CS_email.asp" name="frmProcess" method="post">
							<p>Start Date:&nbsp;&nbsp;<input type="text" name="dateStart" value="<%=dateStart%>"></p>
							<p>End Date:&nbsp;&nbsp;<input type="text" name="dateEnd" value="<%=dateEnd%>"></p>
							<p><input type="submit" name="submitted" value="Display Orders"></p>
						</form>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
