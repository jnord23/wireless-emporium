<%
response.buffer = false
pageTitle = "Admin - Transmit MobileLine Orders"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
if request.form("submitted") <> "" then
	dim dateStart
	dateStart = request.form("dateStart")
	if not isDate(dateStart) then
		response.write "<h3>Start Date must be a valid date.</h3>"
		response.end
	end if
	
	dim fs, thisFile, filename, path
	filename = "MobileLine_" & replace(cStr(date),"/","-") & ".csv"
	path = server.MapPath("/tempCSV/MobileLine/" & filename)
	set fs = Server.CreateObject("Scripting.FileSystemObject")
	if fs.FileExists(path) then fs.DeleteFile(path)
	set thisFile = fs.CreateTextFile(path)
	
	dim iCount, rCount, PartNumber
	iCount = 0
	rCount = 0
	PartNumber = ""
	
	strToWrite = "Customer # *,Payment *,PO #,SKU *,QTY *,Cost,Comment,Shipping Method *,First Name *,Last Name,Address *,Address 2,City *,State/Province *,Zipcode *,Country *,Phone *,Fax,Email"
	thisFile.WriteLine strToWrite
	
	for storeCount = 0 to 4
		SQL = "SELECT A.orderID, A.thub_posted_date, A.shiptype, B.orderdetailid, B.quantity,"
		SQL = SQL & " C.itemDesc, C.PartNumber, C.vendor, C.MobileLine_sku, D.* FROM we_Orders A"
		SQL = SQL & " INNER JOIN we_Orderdetails B ON A.orderID = B.orderID"
		SQL = SQL & " INNER JOIN we_Items C ON B.itemID = C.itemID"
		select case storeCount
			case 0 : SQL = SQL & " INNER JOIN we_Accounts D ON A.accountID = D.AccountID WHERE A.store = 0"
			case 1 : SQL = SQL & " INNER JOIN CA_Accounts D ON A.accountID = D.AccountID WHERE A.store = 1"
			case 2 : SQL = SQL & " INNER JOIN CO_Accounts D ON A.accountID = D.AccountID WHERE A.store = 2"
			case 3 : SQL = SQL & " INNER JOIN PS_Accounts D ON A.accountID = D.AccountID WHERE A.store = 3"
			case 4 : SQL = SQL & " INNER JOIN ER_Accounts D ON A.accountID = D.AccountID WHERE A.store = 10"
		end select
		SQL = SQL & " AND A.approved = 1 AND (A.cancelled IS NULL OR A.cancelled = 0)"
		SQL = SQL & " AND A.thub_posted_date >= '" & dateStart & "' AND A.thub_posted_date < '" & dateAdd("d",1,date) & "'"
		SQL = SQL & " AND C.vendor = 'MLD'"
		SQL = SQL & " AND B.SentToDropShipper IS NULL"
		SQL = SQL & " ORDER BY A.orderID"
		response.write "<p>" & SQL & "</p>"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		do until RS.eof
			PartNumber = ""
			rCount = rCount + 1
			
			if len(RS("sState")) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",RS("sState")) > 0 then
				sSCountry = "CA"
			else
				sSCountry = "US"
			end if
			
			select case RS("shiptype")
				case "First Class" : shiptype = "usps_First-Class Mail Parcel"
				case "First Class Int'l" : shiptype = "usps_First-Class Mail Parcel"
				case "UPS 2nd Day Air" : shiptype = "ups_02"
				case "UPS 3 Day Select" : shiptype = "ups_12"
				case "UPS Ground" : shiptype = "ups_03"
				case "USPS Express" : shiptype = "usps_Express Mail"
				case "USPS Priority" : shiptype = "usps_Priority Mail"
				case "USPS Priority Int'l" : shiptype = "usps_Priority Mail"
			end select
			
			phone = trim(RS("phone"))
			for a = 1 to len(phone)
				if inStr("1234567890",mid(phone,a,1)) = 0 then phone = replace(phone,mid(phone,a,1),"")
			next
			phone = left(phone,10)
			if len(phone) < 10 then phone = "1010101010"
			
			strToWrite = "4858D,"
			strToWrite = strToWrite & "cconfile,"
			strToWrite = strToWrite & RS("orderID") & ","
			strToWrite = strToWrite & RS("MobileLine_sku") & ","
			strToWrite = strToWrite & RS("quantity") & ","
			strToWrite = strToWrite & ","	'Cost
			strToWrite = strToWrite & ","	'Comment
			strToWrite = strToWrite & shiptype & ","
			strToWrite = strToWrite & chr(34) & RS("fname") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("lname") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("sAddress1") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("sAddress2") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("sCity") & chr(34) & ", "
			strToWrite = strToWrite & RS("sState") & ", "
			strToWrite = strToWrite & RS("sZip") & ","
			strToWrite = strToWrite & sSCountry & ","
			strToWrite = strToWrite & phone & ","
			strToWrite = strToWrite & ","	'Fax,Email
			thisFile.WriteLine strToWrite
			
			iCount = iCount + RS("quantity")
			SQL = "UPDATE we_OrderDetails SET SentToDropShipper = '" & now & "' WHERE orderdetailid = " & RS("orderdetailid") & " and orderID = '" & RS("orderID") & "'"
			response.write "<p>" & SQL & "</p>" & vbcrlf
			oConn.execute SQL
			RS.movenext
		loop
	next
	
	bodyText = "<h3><u>MobileLine Orders</u></h3>"
	bodyText = bodyText & "<h3>TOTAL NUMBER OF RECORDS: " & rCount & "</h3>"
	bodyText = bodyText & "<h3>TOTAL NUMBER OF ITEMS: " & iCount & "</h3>"
	bodyText = bodyText & "<h3>(CSV FILE ATTACHED)</h3>"
	response.write bodyText & "<br>" & vbcrlf
	
	RS.close
	set RS = nothing
	thisFile.Close()
	
	if rCount > 0 then
		Set objErrMail = CreateObject("CDO.Message")
		With objErrMail
			.From = "ruben@wirelessemporium.com"
			.To = "hector.nazario@mobileline.com"
			.cc = "batchorders@mobileline.com"
			.bcc = "ruben@wirelessemporium.com,jon@wirelessemporium.com,steven@wirelessemporium.com,bobby.lee@mobileline.com,regina@wirelessemporium.com"
			.Subject = "MobileLine Orders for " & date
			.HTMLBody = bodyText
			.AddAttachment path
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Update
			.Send
		End With
	end if
	
	set thisFile = nothing
	set objErrMail = nothing
	set filesys = nothing
	set demofolder = nothing
	set demofile = nothing
	set filecoll = nothing
	
	response.write "<h3>DONE!</h3>" & vbcrlf
else
	%>
	<form action="MobileLine_transmit_orders.asp" method="post">
		<p class="normalText"><input type="text" name="dateStart" value="<%=dateAdd("d",-4,date)%>">&nbsp;&nbsp;Start Date (thub_posted_date)</p>
		<p class="normalText"><input type="submit" name="submitted" value="Submit"></p>
	</form>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
