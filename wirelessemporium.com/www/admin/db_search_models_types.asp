<%
pageTitle = "Admin - Update WE Database - Select Models/Types"
header = 1
BrandID = request("BrandID")
searchID = request.querystring("searchID")
if searchID = "" then
	response.redirect("menu.asp")
end if
select case searchID
	case "1" : myURL = "db_update_inv.asp"
	case "2" : myURL = "db_update_issues.asp"
	case "3" : myURL = "db_update_part_numbers.asp"
	case "4" : myURL = "db_update_items.asp"
	case "5" : myURL = "db_update_models.asp"
	case "6" : myURL = "faceplates.asp"
	case "7" : myURL = "db_update_BUY.asp"
	case "8" : myURL = "db_update_price.asp"
	case "11" : myURL = "report_ItemSales_By_Model.asp"
end select
myPost = "post"
if searchID = "5" then myPost = "get"
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<blockquote>
<blockquote>

<font face="Arial,Helvetica">

<%
if searchID = "1" or searchID = "7" or searchID = "8" then
	%>
	<p><a href="/admin/db_search_brands.asp?searchID=<%=searchID%>">Back to Brands</a></p>
	<form action="<%=myURL%>" method="<%=myPost%>" name="frmModel">
		<h3>Select a Model:</h3>
		<p>
			<select name="ModelID">
				<option value=""></option>
				<%
				SQL = "SELECT * FROM WE_Models"
				SQL = SQL & " WHERE BrandID='" & BrandID & "'"
				SQL = SQL & " ORDER BY ModelName"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				do until RS2.eof
					%>
					<option value="<%=RS2("ModelID")%>"><%=RS2("ModelName")%></option>
					<%
					RS2.movenext
				loop
				%>
			</select>
			<input type="hidden" name="BrandID" value="<%=BrandID%>">
		</p>
		<h3>Select a Type:</h3>
		<p>
			<select name="TypeID">
				<option value=""></option>
				<%
				SQL = "SELECT * FROM WE_Types"
				SQL = SQL & " ORDER BY TypeName"
				Set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				do until RS2.eof
					%>
					<option value="<%=RS2("TypeID")%>"><%=RS2("TypeName")%></option>
					<%
					RS2.movenext
				loop
				%>
			</select>
		</p>
		<p><input type="submit" name="submitUpdateInv" value="Search"></p>
	</form>
	<%
else
	if searchID <> "3" then
		%>
		<p><a href="/admin/db_search_brands.asp?searchID=<%=searchID%>">Back to Brands</a></p>
		<form action="<%=myURL%>" method="<%=myPost%>" name="frmModel">
			<h3>Select a Model:</h3>
			<p>
				<select name="ModelID">
					<option value=""></option>
					<%
					SQL = "SELECT * FROM WE_Models"
					SQL = SQL & " WHERE BrandID='" & BrandID & "'"
					SQL = SQL & " ORDER BY ModelName"
					set RS2 = Server.CreateObject("ADODB.Recordset")
					RS2.open SQL, oConn, 3, 3
					do until RS2.eof
						%>
						<option value="<%=RS2("ModelID")%>"><%=RS2("ModelName")%></option>
						<%
						RS2.movenext
					loop
					%>
				</select>
				<input type="hidden" name="BrandID" value="<%=BrandID%>">
				<input type="hidden" name="searchType" value="Model">
				<input type="hidden" name="submitType" value="Edit">
				<%if searchID <> "4" then%>
					&nbsp;&nbsp;<input type="submit" name="submitModel" value="Search">
				<%end if%>
				<%if searchID = "5" then%>
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;OR&nbsp;&nbsp;-</p>
					<p><a href="<%=myURL%>?BrandID=<%=BrandID%>&submitType=Add">Add New Model</a></p>
				<%end if%>
			</p>
		</form>
		<%
	end if
	if searchID <> "5" and searchID <> "6" and searchID <> "11" then
		%>
		<form action="<%=myURL%>" method="post" name="frmType">
			<h3>Select a Type:</h3>
			<p>
				<select name="TypeID">
					<option value=""></option>
					<%
					SQL = "SELECT * FROM WE_Types"
					SQL = SQL & " ORDER BY TypeName"
					set RS2 = Server.CreateObject("ADODB.Recordset")
					RS2.open SQL, oConn, 3, 3
					do until RS2.eof
						%>
						<option value="<%=RS2("TypeID")%>"><%=RS2("TypeName")%></option>
						<%
						RS2.movenext
					loop
					
					if searchID = "4" then
						%>
						<option value="1000">Sports</option>
						<%
					end if
					%>
				</select>
				<br><br>
				<%if searchID = "3" then%>
					<b>Primary Sort:&nbsp;</b>
					<input type="radio" name="sort1" value="brand">Brand&nbsp;&nbsp;
					<input type="radio" name="sort1" value="model">Model&nbsp;&nbsp;
					<input type="radio" name="sort1" value="description">Description&nbsp;&nbsp;
					<input type="radio" name="sort1" value="subtype">Subtype
					<br><br>
					<b>Secondary Sort:&nbsp;</b>
					<input type="radio" name="sort2" value="brand">Brand&nbsp;&nbsp;
					<input type="radio" name="sort2" value="model">Model&nbsp;&nbsp;
					<input type="radio" name="sort2" value="description">Description&nbsp;&nbsp;
					<input type="radio" name="sort2" value="subtype">Subtype
					<br><br>
				<%end if%>
				<input type="hidden" name="BrandID" value="<%=BrandID%>">
				<input type="hidden" name="searchType" value="Type">
				<%if searchID = "4" then%>
					<input type="hidden" name="ModelID" value="">
					<input type="hidden" name="updateItems" value="">
					&nbsp;&nbsp;<input type="button" name="submitType" value="Add" onClick="setVals('Add');">
					&nbsp;&nbsp;<input type="button" name="submitType" value="Edit" onClick="setVals('Edit');">
				<%else%>
					&nbsp;&nbsp;<input type="submit" name="submitType" value="Search">
				<%end if%>
			</p>
		</form>
		<%
	end if
end if

RS2.close
set RS2 = nothing
%>

</font>

</blockquote>
</blockquote>

<script language="JavaScript">
	function setVals(updateType) {
		document.frmType.ModelID.value = document.frmModel.ModelID.value;
		document.frmType.updateItems.value = updateType;
		document.frmType.submit();
	}
</script>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
