<%
header = 1
pageTitle = "Compatible Products Search"
PartNumber = Ucase(request.form("PartNumber"))
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<h3>Compatible Products Search</h3>

<form action="CompatibleProducts.asp" method="post">
	<p>
		<font face="Arial,Helvetica">
			Enter Part Number:<br>
			<input type="text" name="PartNumber" value="<%=PartNumber%>">&nbsp;&nbsp;&nbsp;
			<input type="submit" name="submitted" value="Search">
		</font>
	</p>
</form>

<%
if request.form("submitted") <> "" and PartNumber <> "" then
	SQL = "SELECT A.itemID, A.modelID, A.ItemDesc, A.PartNumber, B.brandName, C.modelName FROM we_Items A"
	SQL = SQL & " INNER JOIN we_Brands B ON A.brandID = B.brandID"
	SQL = SQL & " INNER JOIN we_Models C ON A.modelID = C.modelID"
	SQL = SQL & " WHERE A.PartNumber LIKE '%" & PartNumber & "%'"
	SQL = SQL & " AND A.HideLive = 0"
	SQL = SQL & " ORDER BY A.ItemDesc"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	
	if RS.eof then
		response.write "<h3>No items found.</h3>"
	else
		dim fs, file, filename, path, strline
		set fs = Server.CreateObject("Scripting.FileSystemObject")
		filename = "CompatibleProducts.csv"
		path = Server.MapPath("tempCSV") & "\" & filename
		set file = fs.CreateTextFile(path)
		file.WriteLine "itemID,brandName,modelName,modelID,itemDesc,PartNumber"
		%>
		<h3><%=RS.recordcount%> items found</h3>
		<table width="1000" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr>
				<td width="840" valign="top">
					<table width="100%" border="1" cellpadding="0" cellspacing="0">
						<tr>
							<td><b>item&nbsp;ID</b></td>
							<td><b>Brand</b></td>
							<td><b>Model</b></td>
							<td><b>Model&nbsp;ID</b></td>
							<td><b>Item&nbsp;Desc</b></td>
							<td><b>Part&nbsp;Number</b></td>
						</tr>
						<%
						do until RS.eof
							%>
							<tr>
								<td><%=RS("itemID")%></td>
								<td><%=RS("brandName")%></td>
								<td><%=RS("modelName")%></td>
								<td><%=RS("modelID")%></td>
								<td><%=RS("ItemDesc")%></td>
								<td><%=RS("PartNumber")%></td>
							</tr>
							<%
							strline = RS("itemID") & ","
							strline = strline & chr(34) & RS("brandName") & chr(34) & ","
							strline = strline & chr(34) & RS("modelName") & chr(34) & ","
							strline = strline & RS("modelID") & ","
							strline = strline & chr(34) & RS("ItemDesc") & chr(34) & ","
							strline = strline & RS("PartNumber")
							file.WriteLine strline
							RS.movenext
						loop
						%>
					</table>
				</td>
				<td width="10" valign="top">&nbsp;</td>
				<td width="150" valign="top">
					<table width="100%" border="1" cellpadding="0" cellspacing="0">
						<tr>
							<td><b>Brand</b></td>
							<td><b>Code</b></td>
						</tr>
						<%
						SQL = "SELECT brandName, brandCode FROM we_Brands WHERE brandName IS NOT NULL"
						set RS = Server.CreateObject("ADODB.Recordset")
						RS.open SQL, oConn, 3, 3
						do while not RS.eof
							%>
							<tr>
								<td><%=RS("brandName")%></td>
								<td><%=RS("brandCode")%></td>
							</tr>
							<%
							RS.movenext
						loop
						%>
					</table>
				</td>
			</tr>
		</table>
		<%
		file.close()
		set file = nothing
		set fs = nothing
	end if
	RS.close
	set RS = nothing
end if
%>

<p>Here is the <a href="/admin/tempCSV/<%=filename%>">CSV file</a></p>

<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
