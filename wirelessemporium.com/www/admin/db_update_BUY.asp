<%
pageTitle = "Admin - Update WE Database - Update Inventory"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<font face="Arial,Helvetica">
	<p><a href="/admin/db_search_brands.asp?searchID=1">Back to Brands</a></p>
</font>

<%
if request("submitted") = "Update" then
	for a = 1 to request("totalCount")
		if isNumeric(request("price_Buy" & a)) then
			price_Buy = request("price_Buy" & a)
		else
			price_Buy = "null"
		end if
		SQL = "UPDATE WE_Items SET"
		SQL = SQL & " price_Buy=" & price_Buy & ""
		SQL = SQL & " WHERE itemID=" & request("itemID" & a)
		session("errorSQL") = SQL
		oConn.execute SQL
	next
	%>
	<h3>UPDATED!</h3>
	<%
else
	SQL = "SELECT A.*, B.brandCode FROM WE_Items A"
	SQL = SQL & " LEFT OUTER JOIN WE_Brands B ON A.BrandID = B.BrandID"
	keywords = SQLquote(request("keywords"))
	ItemNumber = request("ItemNumber")
	PartNumber = request("PartNumber")
	SQL = SQL & " WHERE A.hideLive = 0"
	if request("BrandID") = "ALL" and request("TypeID") = "8" then
		SQL = SQL & " AND A.TypeID='8'"
	else
		if keywords = "" then
			if ItemNumber = "" then
				if PartNumber = "" then
					if request("BrandID") <> "" then SQL = SQL & " AND A.BrandID='" & request("BrandID") & "'"
					if request("TypeID") <> "" then SQL = SQL & " AND A.TypeID='" & request("TypeID") & "'"
					if request("ModelID") <> "" then SQL = SQL & " AND A.ModelID='" & request("ModelID") & "'"
				else
					SQL = SQL & " AND A.PartNumber='" & PartNumber & "'"
				end if
			else
				SQL = SQL & " AND A.itemID='" & ItemNumber & "'"
			end if
		else
			SQL = SQL & " AND A.itemDesc LIKE '%" & keywords & "%'"
		end if
	end if
	SQL = SQL & " ORDER BY A.itemDesc"
	session("errorSQL") = SQL
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	a = 0
	if RS.eof then
	    response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
	else
		BrandID = RS("brandID")
		%>
		<font face="Arial,Helvetica"><h3><%=RS.recordcount%> items found</h3></font>
		<form name="frmEditItem" action="db_update_BUY.asp" method="post">
			<table border="1" cellpadding="3" cellspacing="0" align="center" width="900">
				<tr>
					<td align="center" width="10"><b>Item#</b></td>
					<td align="center" width="800"><b>Description</b></td>
					<td align="center" width="120"><b>Part&nbsp;Number</b></td>
					<td align="center" width="40"><b>Buy&nbsp;$</b></td>
				</tr>
				<%
				do until RS.eof
					a = a + 1
					if not isNull(RS("PartNumber")) and not isEmpty(RS("PartNumber")) and RS("PartNumber") <> "" then
						PartNumber = RS("PartNumber")
					else
						PartNumber = ""
					end if
					%>
					<tr>
						<td align="center"><%=RS("itemID")%></td>
						<td align="center"><%=RS("itemDesc")%></td>
						<td align="center"><%=PartNumber%></td>
						<td align="center">
							<input type="text" name="price_Buy<%=a%>" size="5" value="<%=RS("price_Buy")%>" maxlength="6">
							<input type="hidden" name="itemID<%=a%>" value="<%=RS("itemID")%>">
						</td>
					</tr>
					<%
					RS.movenext
				loop
				%>
			</table>
			<p align="center">
				<input type="submit" name="submitted" value="Update">
				<input type="hidden" name="totalCount" value="<%=a%>">
			</p>
		</form>
		<p>&nbsp;</p>
		<%
	end if
	RS.close
	set RS = nothing
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
