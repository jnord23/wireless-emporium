<%
	thisUser = Request.Cookies("username")
	pageTitle = "Hypercel Assign"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<style>
	.title { font-weight:bold; color:#FFF; background-color:#000; font-size:14px; }
	.mainTable { font-size:12px; width:650px; }
	.hypeSide { padding-right:5px; border-right:1px solid #000; }
	.weSide { padding-left:5px; width:100%; text-align:left; }
</style>
<%
	imageFolderPath = server.MapPath("/productpics")

	set jpeg = Server.CreateObject("Persits.Jpeg")
	set fso = CreateObject("Scripting.FileSystemObject")
	
	dim addProduct : addProduct = prepInt(request.Form("addProduct"))
	dim brandID : brandID = prepInt(request.Form("brandID"))
	dim modelID : modelID = prepInt(request.Form("modelID"))
	dim typeID : typeID = prepInt(request.Form("typeID"))
	dim partNumber : partNumber = prepStr(request.Form("partNumber"))
	dim shortDesc : shortDesc = prepStr(request.Form("shortDesc"))
	dim itemPic : itemPic = prepStr(request.Form("itemPic"))
	dim msrp : msrp = prepStr(request.Form("msrp"))
	dim wePrice : wePrice = prepStr(request.Form("wePrice"))
	dim cogs : cogs = prepStr(request.Form("cogs"))
	dim inv_qty : inv_qty = prepInt(request.Form("inv_qty"))
	dim fullDesc : fullDesc = prepStr(request.Form("fullDesc"))
	dim weight : weight = prepStr(request.Form("weight"))
	dim vendorPN : vendorPN = prepStr(request.Form("vendorPN"))
	dim hideLive : hideLive = prepInt(request.Form("hideLive"))
	dim parentID : parentID = prepInt(request.Form("parentID"))
	dim handsFreeType : handsFreeType = prepInt(request.Form("handsFreeType"))
	dim holdingID : holdingID = prepInt(request.Form("holdingID"))
	dim cat2 : cat2 = prepStr(request.Form("cat2"))
	dim hcPartNumber : hcPartNumber = prepStr(request.Form("hcPartNumber"))
	dim skipID : skipID = prepInt(request.QueryString("skipID"))
	dim altPic : altPic = ""
	dim altPicName : altPicName = ""
	if addProduct = 1 then
		itemPicName = replace(itemPic,"http://208.57.176.154/mmhypercelonline/Images/products/actual/","")
		set XMLHTTP = server.CreateObject("MSXML2.ServerXMLHTTP")
		XMLHTTP.Open "GET", itemPic, False
		session("errorSQL") = "url = " & itemPic
		XMLHTTP.Send
		
		If XMLHTTP.Status = "200" AND XMLHTTP.Status <> "404" then	
			set DataStream = CreateObject("ADODB.Stream")
			DataStream.Open
			DataStream.Type = 1
			DataStream.Write xmlHTTP.ResponseBody
			DataStream.Position = 0
'			DataStream.SaveToFile "C:/inetpub/wwwroot/productpics/temp/" & itemPicName
			DataStream.SaveToFile imageFolderPath & "\temp\" & itemPicName
			DataStream.Close
			set DataStream = Nothing
		else
			session("errorSQL2") = "error accessing image"
		end if
		
		Set XMLHTTP = Nothing
		
		if instr(itemPicName,"_1") > 0 then
			altPic = replace(itemPic,"_1","_2")
			altPicName = replace(itemPicName,".jpg","-2.jpg")
			set XMLHTTP = server.CreateObject("MSXML2.ServerXMLHTTP")
			XMLHTTP.Open "GET", altPic, False
			session("errorSQL") = "url = " & altPic
			XMLHTTP.Send
			
			If XMLHTTP.Status = "200" AND XMLHTTP.Status <> "404" then	
				set DataStream = CreateObject("ADODB.Stream")
				DataStream.Open
				DataStream.Type = 1
				DataStream.Write xmlHTTP.ResponseBody
				DataStream.Position = 0
'				DataStream.SaveToFile "C:/inetpub/wwwroot/productpics/temp/" & altPicName
				DataStream.SaveToFile imageFolderPath & "\temp\" & altPicName
				DataStream.Close
				set DataStream = Nothing
			else
				session("errorSQL2") = "error accessing image"
			end if
			
			Set XMLHTTP = Nothing
		end if
		
		if fso.fileExists(imageFolderPath & "\temp\" & itemPicName) then
			jpeg.Open imageFolderPath & "\temp\" & itemPicName
			jpeg.Height = 300
			jpeg.Width = 300
			jpeg.Save imageFolderPath & "\big\" & itemPicName
			
			jpeg.Open imageFolderPath & "\temp\" & itemPicName
			jpeg.Height = 100
			jpeg.Width = 100
			jpeg.Save imageFolderPath & "\thumb\" & itemPicName
			
			jpeg.Open imageFolderPath & "\temp\" & itemPicName
			jpeg.Height = 65
			jpeg.Width = 65
			jpeg.Save imageFolderPath & "\homepage65\" & itemPicName
			
			jpeg.Open imageFolderPath & "\temp\" & itemPicName
			jpeg.Height = 45
			jpeg.Width = 45
			jpeg.Save imageFolderPath & "\icon\" & itemPicName
			
			fso.deleteFile(imageFolderPath & "\temp\" & itemPicName)
		end if
		
		if altPic <> "" then
			if fso.fileExists(imageFolderPath & "\temp\" & altPicName) then
				jpeg.Open imageFolderPath & "\temp\" & altPicName
				jpeg.Height = 300
				jpeg.Width = 300
				jpeg.Save imageFolderPath & "\AltViews\" & altPicName
				
				fso.deleteFile(imageFolderPath & "\temp\" & altPicName)
			end if
		end if
		
		sql = "insert into we_items(brandID,modelID,typeID,vendor,partNumber,itemDesc,itemPic,price_retail,price_our,cogs,inv_qty,itemLongDetail,itemWeight,master,mobileLine_Sku,hideLive,amzn_sku,handsfreeType) values(" & brandID & "," & modelID & "," & typeID & ",'HC','" & partNumber & "','" & shortDesc & "','" & itemPicName & "','" & msrp & "','" & wePrice & "','" & cogs & "','" & inv_qty & "','" & fullDesc & "','" & weight & "',1,'" & vendorPN & "'," & hideLive & ",'" & parentID & "'," & handsFreeType & ")"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "update holding_hypercell set processed = 1 where id = " & holdingID
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	if hcPartNumber <> "" then
		sql = "select top 1 * from holding_hypercell with (NOLOCK) where partNumber = '" & hcPartNumber & "' and processed = 0 and bypass = 0 order by id"
	elseif cat2 <> "" then
		sql = "select top 1 * from holding_hypercell with (NOLOCK) where cat2 = '" & cat2 & "' and processed = 0 and bypass = 0 order by id"
	else
		sql = "select top 1 * from holding_hypercell with (NOLOCK) where processed = 0 and bypass = 0 order by id"
	end if
	if skipID > 0 then sql = replace(sql,"order by","and id > " & skipID & " order by")
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	if rs.EOF then
		response.Write("no more products")
		response.End()
	else
		holdingID = rs("id")
		partNumber = rs("partNumber")
		cat1 = rs("cat1")
		cat2 = rs("cat2")
		cat3 = rs("cat3")
		prodName = rs("name")
		prodDesc = rs("description")
		shortDesc = rs("shortDesc")
		weight = rs("weight")
		msrp = rs("msrp")
		cogs = rs("cogs")
		manufacturer = rs("manufacturer")
		model = rs("model")
		itemPic = rs("image")
		manType = rs("manType")
		inv_qty = rs("inv_qty")
		keywords = rs("keywords")
		do while len(partNumber) < 6
			partNumber = partNumber & "0"
		loop
		if instr(itemPic,"/icon") > 0 then itemPic = replace(itemPic,"/icon","/actual")
		itemPic = replace(itemPic,"65.116.253.88","208.57.176.154")
	end if
	
	sql = "select brandID as id, brandName as name, 1 as catID from we_brands with (nolock) union select typeID as id, typeName as name, 2 as catID from we_types with (nolock) order by catID, name"
	session("errorSQL") = sql
	Set menuRS = Server.CreateObject("ADODB.Recordset")
	menuRS.open sql, oConn, 0, 1
	
	sql = "select id, name from we_handsfree order by id"
	session("errorSQL") = sql
	Set hfRS = Server.CreateObject("ADODB.Recordset")
	hfRS.open sql, oConn, 0, 1
	
	if isnumeric(right(partNumber,1)) then
		if right(partNumber,1) = 0 then
			usePartNumber = left(partNumber,len(partNumber)-1)
		else
			usePartNumber = partNumber
		end if
	else
		usePartNumber = partNumber
	end if
%>
<form name="searchForm" method="post" action="/admin/hypercellAssign.asp">
<table border="0" cellpadding="0" cellspacing="0" style="padding-top:20px;" align="center" width="400">
	<tr>
    	<td colspan="3" align="left" style="font-weight:bold; font-size:18px; border-bottom:1px solid #000;">Hypercel Product Search</td>
    </tr>
    <tr>
    	<td align="right" style="font-weight:bold; padding-top:10px;">Category:</td>
        <td align="left" width="100%" style="padding:10px 0px 0px 10px;">
        	<select name="cat2" onchange="document.searchForm.submit()">
            	<option value="">-- Select --</option>
                <%
				sql = "select distinct cat2 from holding_hypercell where processed = 0 and bypass = 0 order by cat2"
				session("errorSQL") = sql
				set catRS = oConn.execute(sql)
				do while not catRS.EOF
				%>
                <option value="<%=catRS("cat2")%>"<% if cat2 = catRS("cat2") then %> selected="selected"<% end if %>><%=catRS("cat2")%></option>
                <%
					catRS.movenext
				loop
				%>
            </select>
        </td>
    </tr>
    <tr>
        <td align="right" style="font-weight:bold; padding:10px 0px 10px 0px; border-bottom:1px solid #000;" nowrap="nowrap">Part Number:</td>
        <td align="left" width="100%" style="padding:10px 0px 10px 10px; border-bottom:1px solid #000;"><input type="text" name="hcPartNumber" value="" /></td>
        <td style="padding-left:10; border-bottom:1px solid #000;"><input type="submit" name="mySub" value="Search" /></td>
    </tr>
</table>
</form>
<form name="newProdForm" method="post" action="/admin/hypercellAssign.asp" onsubmit="return(verifyHypercellForm(this))">
<table align="center" border="0" cellpadding="3" cellspacing="0" class="mainTable">
	<tr>
    	<td align="center" colspan="2">
        	<img src="<%=itemPic%>" border="0" />
            <br /><br />
            <a href="http://www.hypercel.com/<%=usePartNumber%>" target="hypercel">http://www.hypercel.com/<%=usePartNumber%></a>
        </td>
    </tr>
    <tr class="title"><td colspan="2">Name</td></tr>
    <tr><td colspan="2" align="left"><%=prodName%></td></tr>
    <tr class="title"><td colspan="2">Brand</td></tr>
    <tr>
    	<td colspan="2" align="left">
			<select name="brandID" onchange="ajax('/ajax/admin/ajaxHypercellAssign.asp?brandID=' + this.value,'modelBox')">
            	<option value="">Select Brand</option>
                <%
				do while not menuRS.EOF
				%>
                <option value="<%=menuRS("id")%>"><%=menuRS("name")%></option>
                <%
					menuRS.movenext
					if menuRS("catID") = 2 then exit do
				loop
				%>
            </select>
        </td>
    </tr>
    <tr class="title"><td colspan="2">Model</td></tr>
    <tr>
    	<td colspan="2" align="left" id="modelBox">Select Brand First</td>
    </tr>
    <tr class="title"><td colspan="2">Category</td></tr>
    <tr>
    	<td nowrap="nowrap" class="hypeSide">
        	<%=cat1%>, <%=cat2%>, <%=cat3%>
        </td>
        <td valign="top" class="weSide">
        	<select name="typeID" onchange="updatePartNumber()" id="typeSelection">
            	<option value="">Select Product Type</option>
                <%
				do while not menuRS.EOF
				%>
                <option value="<%=menuRS("id")%>"><%=menuRS("name")%></option>
                <%
					menuRS.movenext
				loop
				%>
            </select>
        </td>
    </tr>
    <tr class="title" id="hft1" style="display:none;"><td colspan="2">HandsFree Type</td></tr>
    <tr id="hft2" style="display:none;">
        <td colspan="2">
        	<select name="handsFreeType" id="handsFreeType">
            	<option value="">Select HandsFree Type</option>
                <%
				do while not hfRS.EOF
				%>
                <option value="<%=hfRS("id")%>"><%=hfRS("name")%></option>
                <%
					hfRS.movenext
				loop
				%>
            </select>
        </td>
    </tr>
    <tr class="title"><td colspan="2">Part Number</td></tr>
    <tr>
    	<td colspan="2" align="left">
			<input type="text" name="partNumber" value="-HYP-<%=left(partNumber,4)%>-<%=right(partNumber,2)%>" />
        </td>
    </tr>
    <tr class="title"><td colspan="2">Short Description</td></tr>
    <tr><td colspan="2" align="left"><%=shortDesc%></td></tr>
    <tr class="title"><td colspan="2">Full Description</td></tr>
    <tr><td colspan="2" align="left"><textarea name="fullDesc" onchange="verifyDescEdit()" cols="52" rows="4"><%=prodDesc%></textarea></td></tr>
    <tr class="title"><td colspan="2">Details</td></tr>
    <tr>
    	<td colspan="2" align="left">
			<table border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td>Weight:</td>
					<td class="hypeSide"><%=weight%></td>
                    <td>Inventory:</td>
					<td class="hypeSide"><%=inv_qty%></td>
                    <td>MSRP:</td>
					<td class="hypeSide"><%=formatCurrency(msrp,2)%></td>
                    <td>COGS:</td>
					<td class="hypeSide"><%=formatCurrency(cogs,2)%></td>
                    <td>WE Price:</td>
					<td><input type="text" name="wePrice" value="<%=msrp%>" size="4" /></td>
                </tr>
                <tr>
                	<td style="border-top:1px solid #000;">HideLive:</td>
					<td class="hypeSide" style="border-top:1px solid #000;">
                    	<select name="hideLive">
                        	<option value="0">Off</option>
                            <option value="1">On</option>
                        </select>
                    </td>
                	<td style="border-top:1px solid #000;">ParentID:</td>
					<td class="hypeSide" style="border-top:1px solid #000;"><input type="text" name="parentID" value="" size="6" /></td>
                    <td colspan="6" style="border-top:1px solid #000;">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td align="left" style="border-top:1px solid #000; padding-top:10px;"><input type="button" name="myAction" value="Skip Product" onclick="window.location='/admin/hypercellAssign.asp?skipID=<%=holdingID%>'" /></td>
    	<td align="right" style="border-top:1px solid #000; padding-top:10px;">
        	<input type="submit" name="mySub" value="Add to WE" />
            <input type="hidden" name="shortDesc" value="<%=shortDesc%>" />
            <input type="hidden" name="weight" value="<%=weight%>" />
            <input type="hidden" name="inv_qty" value="<%=inv_qty%>" />
            <input type="hidden" name="msrp" value="<%=msrp%>" />
            <input type="hidden" name="cogs" value="<%=cogs%>" />
            <input type="hidden" name="name" value="<%=prodName%>" />
            <input type="hidden" name="itemPic" value="<%=itemPic%>" />
            <input type="hidden" name="vendorPN" value="<%=partNumber%>" />
            <input type="hidden" name="holdingID" value="<%=holdingID%>" />
            <input type="hidden" name="addProduct" value="1" />
            <input type="hidden" name="cat2" value="<%=cat2%>" />
        </td>
    </tr>
</table>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	var curPN = "-HYP-<%=left(partNumber,4)%>-<%=right(partNumber,2)%>"
	function verifyHypercellForm(form) {
		if (form.brandID.value == "") {
			alert("You must select a brand")
			form.brandID.focus()
			return false
		}
		else if (form.modelID.value == "") {
			alert("You must select a model")
			form.modelID.focus()
			return false
		}
		else if (form.typeID.value == "") {
			alert("You must select a product category")
			form.typeID.focus()
			return false
		}
		else if (form.wePrice.value == "") {
			alert("You must select a selling price")
			form.wePrice.focus()
			return false
		}
		else {
			return true
		}
	}
	
	function verifyDescEdit() {
		var yourstate=window.confirm("Are you sure you want edit the Full Description?")
		if (yourstate) {
			document.newProdForm.brandID.focus()
		}
		else {
			document.newProdForm.fullDesc.value = "<%=prodDesc%>"
		}
	}
	
	function updatePartNumber() {
		var selIndex = document.getElementById('typeSelection').selectedIndex;
		var newPart = document.getElementById('typeSelection').options[selIndex].text.substring(0,3)
		if (document.getElementById('typeSelection').options[selIndex].text == "Hands-Free") {
			document.getElementById("hft1").style.display = ''
			document.getElementById("hft2").style.display = ''
		}
		else {
			document.getElementById("handsFreeType").selectedIndex = 0
			document.getElementById("hft1").style.display = 'none'
			document.getElementById("hft2").style.display = 'none'
		}
		newPart = newPart.toUpperCase()
		document.newProdForm.partNumber.value = newPart + curPN
	}
</script>