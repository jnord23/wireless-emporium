<%
pageTitle = "Admin - Sales Tax Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<link href="/includes/css/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/includes/js/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/includes/js/cal.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function () {
		$('#id_sdate').simpleDatepicker();
		$('#id_edate').simpleDatepicker();
	});
</script>
<%
strSDate = prepStr(request.form("txtSDate"))
strEDate = prepStr(request.form("txtEDate"))
siteid = prepInt(request.form("cbSite"))
if strSDate = "" or not isdate(strSDate) then strSDate = Date
if strEDate = "" or not isdate(strEDate) then strEDate = Date

response.write "<h3>" & pageTitle & "</h3>" & vbCrLf
%>
    <center>
	<form action="report_SalesTax.asp" method="post">
    	<p>
	        <select name="cbSite">
            	<option value="0" <%if siteid=0 then%>selected<%end if%>>WE</option>
            	<option value="2" <%if siteid=2 then%>selected<%end if%>>CO</option>
            	<option value="1" <%if siteid=1 then%>selected<%end if%>>CA</option>
            	<option value="3" <%if siteid=3 then%>selected<%end if%>>PS</option>
            	<option value="10" <%if siteid=10 then%>selected<%end if%>>TM</option>
            </select>
        </p>
		<p>
			<input type="text" id="id_sdate" name="txtSDate" value="<%=strSDate%>" size="7"> ~ <input type="text" id="id_edate" name="txtEDate" value="<%=strEDate%>" size="7">
		</p>
		<p>
        	<!--
			<select name="myYear">
				<option value="<%=a%>"><%=a%></option>
				<option value="<%=a - 1%>"><%=a - 1%></option>
			</select>&nbsp;&nbsp;Year
            -->
		</p>
		<p><input type="submit" name="submitted" value="Generate Report"></p>
	</form>
    </center>

<%
if request("submitted") <> "" then
	showblank = "&nbsp;"
	shownull = "-null-"

	SQL = "SELECT A.ordergrandtotal, a.ordershippingfee, a.ordersubtotal, a.orderid, a.orderdatetime, a.orderTax, a.RMAStatus, a.refundAmount, B.sState, convert(varchar(8000), C.ordernotes) ordernotes"
	SQL = SQL & " FROM (WE_orders A INNER JOIN v_accounts B ON A.accountid=B.accountid and a.store = b.site_id)"
	SQL = SQL & " LEFT JOIN WE_ordernotes C ON A.orderid=C.orderid"
	SQL = SQL & " WHERE B.sState='ca' and a.orderdatetime >= '" & strSDate & "' and a.orderdatetime < '" & dateadd("d", 1, strEDate) & "' "
	SQL = SQL & " AND A.approved=1 AND A.confirmSent='yes' AND (A.cancelled = 0 OR A.cancelled IS NULL)"
	SQL = SQL & " AND (B.email = 'phoneorder@wirelessemporium.com' OR B.email NOT LIKE '%@wirelessemporium.com')"
	SQL = SQL & " AND B.accountID != 1 AND B.accountID != 8 AND B.accountID != 165305 AND B.accountID != 224190 AND B.accountID != 228690"
	SQL = SQL & " AND (A.cancelled = 0 OR A.cancelled IS NULL)"
	SQL = SQL & " AND A.store = '" & siteid & "'"
	SQL = SQL & " ORDER BY A.orderdatetime"
	
	'SQL = "SELECT A.*, B.sState, D.sState AS Addl_sState, C.ordernotes"
	'SQL = SQL & " FROM ((we_orders AS A INNER JOIN we_accounts AS B ON A.accountid = B.accountid)"
	'SQL = SQL & " LEFT OUTER JOIN we_ordernotes AS C ON A.orderid = C.orderid)"
	'SQL = SQL & " LEFT OUTER JOIN we_addl_shipping_addr AS D ON A.shippingid = D.id"
	'SQL = SQL & " WHERE (B.sState = 'ca')"
	'SQL = SQL & " AND (MONTH(A.orderdatetime) = '" & request("myMonth") & "') AND (YEAR(A.orderdatetime) = '" & request("myYear") & "')"
	'SQL = SQL & " AND (A.confirmSent = 'yes') AND"
	'SQL = SQL & " (B.email NOT LIKE '%@wirelessemporium.com') AND (B.accountid <> 1) AND (B.accountid <> 8) AND (B.accountid <> 165305) AND"
	'SQL = SQL & " (B.accountid <> 224190) AND (B.accountid <> 228690) AND (A.cancelled = 0 OR A.cancelled IS NULL)"
	'SQL = SQL & " ORDER BY A.orderTax, A.orderdatetime DESC"
	
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	
	
	if RS.eof then
		response.write "No records matched<br><br>So cannot make table..."
	else
		nRecordCount = 0
		do until RS.eof
			nRecordCount = nRecordCount + 1
			RS.movenext
		loop
		RS.movefirst
		%>
		<h3><%=nRecordCount%> records found</h3>
		<table border="1" style="border-collapse:collapse;" cellpadding="5">
			<tr bgcolor="#CCCCCC">
				<td><b>orderid</b></td>
				<td><b>orderdatetime</b></td>
				<td><b>ordersubtotal</b></td>
				<td><b>orderTax</b></td>
				<td><b>CalcTax</b></td>
				<td><b>RMAstatus</b></td>
				<td><b>refundAmount</b></td>
				<td><b>shipState</b></td>
				<td><b>ordernotes</b></td>
			</tr>
			<%
			lap = 0
			do until RS.eof
				lap = lap + 1
				ordergrandtotal = cDbl(RS("ordergrandtotal"))
				ordershippingfee = cDbl(RS("ordershippingfee"))
				ordersubtotal = cDbl(RS("ordersubtotal"))
				'CalcTax = ordergrandtotal - ordershippingfee - ordersubtotal
				CalcTax = ordersubtotal * 0.0775
				GrandTotal = GrandTotal + ordersubtotal
				CalcTaxTotal = CalcTaxTotal + CalcTax
				%>
				<tr>
					<td><%=RS("orderid")%></td>
					<td><%=RS("orderdatetime")%></td>
					<td><%=formatCurrency(ordersubtotal)%></td>
					<td>
						<%
						orderTax = RS("orderTax")
						fontColor = "#000000"
						if not isNull(orderTax) then
							response.write formatCurrency(orderTax)
							orderTaxTotal = orderTaxTotal + orderTax
							if formatNumber(CalcTax,2) <> formatNumber(orderTax,2) then fontColor = "#FF0000"
						else
							response.write "-null-"
						end if
						%>
					</td>
					<td><font color="<%=fontColor%>"><%=formatCurrency(CalcTax)%></font></td>
					<td>
						<%
						select case RS("RMAstatus")
							case 1 : RMAstatus = "EXCH SENT"
							case 2 : RMAstatus = "EXCH REC'D"
							case 3 : RMAstatus = "REF SENT"
							case 4 : RMAstatus = "REF REC'D"
							case 5 : RMAstatus = "EXCHANGED"
							case 6 : RMAstatus = "PART RFND"
							case 7 : RMAstatus = "FULL RFND"
							case else : RMAstatus = shownull
						end select
						response.write RMAstatus
						%>
					</td>
					<td>
						<%
						if not isNull(RS("refundAmount")) then
							response.write formatCurrency(RS("refundAmount"))
							refundAmount = refundAmount + RS("refundAmount")
						else
							response.write shownull
						end if
						%>
					</td>
					<td><%=RS("sState")%></td>
					<td>
						<%
						if not isNull(RS("ordernotes")) then
							response.write RS("ordernotes")
						else
							response.write "-null-"
						end if
						%>
					</td>
				</tr>
				<%
				if (lap mod 1000) = 0 then response.flush
				RS.movenext
			loop
			%>
			<tr>
				<td><b>&nbsp;</b></td>
				<td><b>&nbsp;</b></td>
				<td><b><%=formatCurrency(GrandTotal)%></b></td>
				<td><b><%=formatCurrency(orderTaxTotal)%></b></td>
				<td><b><%=formatCurrency(CalcTaxTotal)%></b></td>
				<td><b>&nbsp;</b></td>
				<td><b><%=formatCurrency(refundAmount)%></b></td>
				<td><b>&nbsp;</b></td>
				<td><b>&nbsp;</b></td>
			</tr>
		</table>
		<h3><%=formatCurrency(GrandTotal) & " - " & formatCurrency(refundAmount) & " = " & formatCurrency(GrandTotal - refundAmount)%></h3>
		<h3><%="7.75% of " & formatCurrency(GrandTotal - refundAmount) & " = " & formatCurrency((GrandTotal - refundAmount) * 0.0775)%></h3>
		<%
	end if
	
	RS.close
	Set RS = nothing
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
