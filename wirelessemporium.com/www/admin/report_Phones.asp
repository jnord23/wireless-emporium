<%
pageTitle = "Admin - Generate Phone Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" width="750" align="center" cellpadding="0" cellspacing="0"><tr><td>
<p>&nbsp;</p>
<p class="biggerText"><%=pageTitle%></p>
<br>

<%
strError = ""

if request("submitted") <> "" then
	SQL = "SELECT * FROM we_items C WHERE typeid = 16 AND hidelive = 0 ORDER BY itemid"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	
	response.write "<p class=""boldText"">" & RS.recordcount & " records found</p>" & vbCrLf
	
	if not RS.eof then
		strToWrite = "ItemID,ItemDescription,PartNumber,COGS,Vendor,Qty" & vbCrLf
		do until RS.eof
			strToWrite = strToWrite & chr(34) & RS("itemid") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("itemDesc") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("PartNumber") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("COGS") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("Vendor") & chr(34) & ","
			strToWrite = strToWrite & chr(34) & RS("inv_qty") & chr(34) & vbCrLf
			RS.movenext
		loop
		dim fs, file, filename, path
		set fs = CreateObject("Scripting.FileSystemObject")
		filename = "report_Phones.csv"
		path = Server.MapPath("/admin/tempCSV") & "\" & filename
		set file = fs.CreateTextFile(path, true)
		file.WriteLine strToWrite
		response.write "<p class=""boldText"">Your CSV file is <a href=""/admin/tempCSV/" & filename & """>here</a>.</p>"
		
		RS.movefirst
		%>
		<table border="1" width="100%" cellpadding="2" cellspacing="0">
			<tr>
				<td><b>Item&nbsp;ID</b></td>
				<td><b>Item&nbsp;Description</b></td>
				<td><b>Part&nbsp;Number</b></td>
				<td><b>COGS</b></td>
				<td><b>Vendor</b></td>
				<td><b>Qty</b></td>
			</tr>
			<%
			do until RS.eof
				%>
				<tr>
					<td><%=RS("itemid")%></td>
					<td><%=RS("itemDesc")%></td>
					<td><nobr><%=RS("PartNumber")%></nobr></td>
					<td><%=RS("COGS")%></td>
					<td><%=RS("Vendor")%></td>
					<td><%=RS("inv_qty")%></td>
				</tr>
				<%
				RS.movenext
			loop
			%>
		</table>
		<%
	else
		response.write "<p class=""boldText"">No records matched.</p><p class=""boldText"">No report generated.</p>"
	end if
	response.write "<p>&nbsp;</p>"
	response.write "<p class=""normalText""><a href=""report_Phones.asp"">Generate Another Phone Report</a></p>"
	RS.close
	Set RS = nothing
end if
%>
<form action="report_Phones.asp" method="post">
	<p class="normalText">
		<input type="submit" name="submitted" value="Submit">
	</p>
</form>

</td></tr></table>

<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
