<%
pageTitle = "RMA Return/Refund Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<link href="/includes/css/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/includes/js/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/includes/js/cal.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function () {
		$('#id_sdate').simpleDatepicker();
		$('#id_edate').simpleDatepicker();
	});
</script>
<%
searchExcel = prepStr(request.form("searchExcel"))
if searchExcel <> "" then response.redirect "/admin/report_rmaReport_Excel.asp?" & request.form
					
strSDate = prepStr(request.form("txtSDate"))
strEDate = prepStr(request.form("txtEDate"))
updateQTY = prepStr(request.form("updateQTY"))
chkUpdate = prepStr(request.form("chkUpdate"))
siteid = prepStr(request.form("cbSite"))
returnReasonID = prepInt(request.form("cbReturnReason"))
modelid = prepInt(request.form("cbModel"))
typeid = prepInt(request.form("cbType"))
strGroupBy = prepStr(request.form("cbGroupBy"))

if strSDate = "" or not isdate(strSDate) then strSDate = Date-6
if strEDate = "" or not isdate(strEDate) then strEDate = Date

Dim cfgArrGroupBy : cfgArrGroupBy = Array("Date", "Store", "Activity Desc", "Return Reason", "Model", "Category", "ItemID", "Details")
if "" = strGroupBy then strGroupBy = "DETAILS,,,,,,," end if
arrGroupBy = split(strGroupBy, ",")

dim strSqlSelectMain, strSqlWhere, strSqlGroupBy, strSqlOrderBy, strHeadings
strSqlSelectMain	=	""
strSqlWhere			=	""
strSqlGroupBy		=	""
strSqlOrderBy		=	""
strHeadings			=	""

Dim bDate, bStore, bReshipReason, bModel, bCategory, bItemID, bActivityDesc, bDetails
bDate			=	false
bStore			=	false
bActivityDesc	=	false
bReturnReason	=	false
bModel			=	false
bCategory		=	false
bItemID			=	false
bDetails		=	false

For i = 0 To Cint(UBound(arrGroupBy))
	If "" <> trim(arrGroupBy(i)) Then
		Select Case ucase(trim(arrGroupBy(i)))
			Case "DATE"
				If bDate Then
					Response.Write "<Script>alert('DATE in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bDate = true

				strSqlSelectMain	=	strSqlSelectMain 	& 	", convert(varchar(10), a.logdate, 20) logDate"
				strSqlGroupBy		=	strSqlGroupBy		&	"convert(varchar(10), a.logdate, 20), "
				strSqlOrderBy		=	strSqlOrderBy		&	"convert(varchar(10), a.logdate, 20), "
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:102px; font-size:13px; font-weight:bold;"">Log Date</div>"

			Case "STORE"
				If bStore Then
					Response.Write "<Script>alert('STORE in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bStore = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.site_id"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.site_id, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.site_id, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:42px; font-size:13px; font-weight:bold;"">SITE</div>"

			Case "RETURN REASON"
				If bReshipReason Then
					Response.Write "<Script>alert('Return Reason in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bReshipReason = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", isnull(c.reason, '')"
				strSqlGroupBy		=	strSqlGroupBy		&	"isnull(c.reason, ''), "
				strSqlOrderBy		=	strSqlOrderBy		&	"isnull(c.reason, ''), "
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:202px; font-size:13px; font-weight:bold;"">Return Reason</div>"

			Case "MODEL"
				If bModel Then
					Response.Write "<Script>alert('Model in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bModel = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", f.modelName"
				strSqlGroupBy		=	strSqlGroupBy		&	"f.modelName, "
				strSqlOrderBy		=	strSqlOrderBy		&	"f.modelName, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:252px; font-size:13px; font-weight:bold;"">Model</div>"

			Case "CATEGORY"
				If bCategory Then
					Response.Write "<Script>alert('Category in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bCategory = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", g.typename"
				strSqlGroupBy		=	strSqlGroupBy		&	"g.typename, "
				strSqlOrderBy		=	strSqlOrderBy		&	"g.typename, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:122px; font-size:13px; font-weight:bold;"">Category</div>"

			Case "ITEMID"
				If bItemID Then
					Response.Write "<Script>alert('ItemID in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bItemID = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.itemid"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.itemid, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.itemid, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:72px; font-size:13px; font-weight:bold;"">Item ID</div>"

			Case "ACTIVITY DESC"
				If bActivityDesc Then
					Response.Write "<Script>alert('Activity Desc in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bActivityDesc = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.activity_desc"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.activity_desc, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.activity_desc, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:152px; font-size:13px; font-weight:bold;"">Activity Desc</div>"

			Case "DETAILS"
				bDetails = true			
		End Select
	End If
Next
%>
<center>
<form method="post" name="frmReturn">
    <div style="text-align:left; width:1100px;">
        <div style="padding-bottom:10px;"><span style="font-size:18px; font-weight:bold;">RMA Return/Refund Report</span></div>
        <!-- filters-->
        <div style="width:100%; border:1px solid #ccc; border-radius:3px; background-color:#f4f7f9; padding:5px;">
            <div style="width:100%; height:30px;">
				<div class="filter-box-header" style="width:60px;">Filter</div>
                <div class="filter-box-header" style="width:180px;">Date</div>
                <div class="filter-box-header" style="width:60px;">Store</div>
                <div class="filter-box-header" style="width:130px;">Return Reason</div>
                <div class="filter-box-header" style="width:100px;">Model</div>
                <div class="filter-box-header" style="width:100px;">Category</div>
                <div class="filter-box-header" style="width:150px;">Action</div>
            </div>
            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%; padding:5px 0px 5px 0px; height:25px;">
				<div class="filter-box-header" style="width:60px;">Value</div>
                <div class="filter-box" style="width:180px;"><input type="text" id="id_sdate" name="txtSDate" value="<%=strSDate%>" size="7"> ~ <input type="text" id="id_edate" name="txtEDate" value="<%=strEDate%>" size="7"></div>
                <div class="filter-box" style="width:60px;">
                	<select name="cbSite">
                    	<option value="">ALL</option>
                    	<option value="0" <%if siteid = "0" then%>selected<%end if%>>WE</option>
                    	<option value="1" <%if siteid = "1" then%>selected<%end if%>>CA</option>
                    	<option value="2" <%if siteid = "2" then%>selected<%end if%>>CO</option>
                    	<option value="3" <%if siteid = "3" then%>selected<%end if%>>PS</option>
                    	<option value="10" <%if siteid = "10" then%>selected<%end if%>>TM</option>
                    </select>
                </div>
                <div class="filter-box" style="width:130px;">
                <%
				sql = "select id, reason from xreturnreason"
				set rsRefReason = oConn.execute(sql)
				%>
				<select name="cbReturnReason" style="width:125px;">
					<option value="">ALL</option>
                <%
				do until rsRefReason.eof
					%>
                    <option value="<%=rsRefReason("id")%>" <%if returnReasonID = rsRefReason("id") then%>selected<% end if%>><%=rsRefReason("reason")%></option>
                    <%
					rsRefReason.movenext
				loop
				%>
				</select>
                </div>
                <div class="filter-box" style="width:100px;">
                <%
				sql = 	"select	modelid, modelname" & vbcrlf & _
						"from	we_models" & vbcrlf & _
						"where	hidelive = 0" & vbcrlf & _
						"order by 2"
				set rsModel = oConn.execute(sql)
				%>
				<select name="cbModel" style="width:95px;">
					<option value="">ALL</option>
                <%
				do until rsModel.eof
					%>
                    <option value="<%=rsModel("modelid")%>" <%if modelid = rsModel("modelid") then%>selected<%end if%>><%=rsModel("modelname")%></option>
                    <%
					rsModel.movenext
				loop
				%>
				</select>
                </div>
                <div class="filter-box" style="width:100px;">
                <%
				sql = 	"select	typeid, typename" & vbcrlf & _
						"from	we_types" & vbcrlf & _
						"order by 2"
				set rsType = oConn.execute(sql)
				%>
				<select name="cbType" style="width:95px;">
					<option value="">ALL</option>
                <%
				do until rsType.eof
					%>
                    <option value="<%=rsType("typeid")%>" <%if typeid = rsType("typeid") then%>selected<%end if%>><%=rsType("typename")%></option>
                    <%
					rsType.movenext
				loop
				%>
				</select>                
                </div>
                <div class="filter-box" style="width:150px;">
	                <input type="submit" name="search" value="Search" />
	                <input type="submit" name="searchExcel" value="Excel" />
                </div>
            </div>

            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%; padding-top:5px; height:25px;">            
				<div class="filter-box-header" style="width:80px;">Group By</div>
				<div class="filter-box" style="width:800px; padding-top:3px; text-align:left;" align="left">
                <%
				if not isnull(cfgArrGroupBy) then
					for i=0 to 5
						response.write "<select name=""cbGroupBy""><option value="""" "
						if "" = trim(arrGroupBy(i)) then response.write " selected " end if
						response.write ">--</option>"
						
						for j=0 to ubound(cfgArrGroupBy,1)
							response.write "<option value=""" & cfgArrGroupBy(j) & """ "
							if ucase(trim(arrGroupBy(i))) = ucase(trim(cfgArrGroupBy(j))) then response.write " selected " end if
							response.write ">" & trim(cfgArrGroupBy(j)) & "</option>"
						next
						response.write "</select>&nbsp;"
					next
				end if
				%>
                </div>
            </div>

        </div>
		<!--// filters-->
        <!-- result -->
		<div style="width:100%; padding-top:10px;">
        <%
		strSqlWhere = ""
		if siteid <> "" 		then strSqlWhere = strSqlWhere & "	and	a.site_id = " & siteid & vbcrlf
		if returnReasonID <> 0 	then strSqlWhere = strSqlWhere & "	and	a.returnReason = " & returnReasonID & vbcrlf
		if modelid <> 0			then strSqlWhere = strSqlWhere & "	and	f.modelid = " & modelid & vbcrlf
		if typeid <> 0			then strSqlWhere = strSqlWhere & "	and	e.typeid = " & typeid & vbcrlf
		
		if bDetails then
			sql	=	"select	a.id, convert(varchar(10), a.logdate, 20) logdate, a.site_id, a.orderid, a.itemid, isnull(a.qty, 0) qty, a.partnumber, b.rma_token lastRmaStatus, a.activity_desc" & vbcrlf & _
					"	,	isnull(a.origItemPrice, 0) origItemPrice, isnull(a.restocking_fee_percent, 0) restocking_fee_percent, isnull(a.itemTax, 0) itemTax" & vbcrlf & _
					"	, 	isnull(a.discountPercent, 0) discountPercent, isnull(a.amountCredited, 0) amountCredited, a.exchangedPartnumber" & vbcrlf & _
					"	,	isnull(a.backInStock, 0) backInStock" & vbcrlf & _
					"	, 	a.confirmAmountCredited, isnull(c.reason, '') returnreason, d.fname userName" & vbcrlf & _
					"from	rmalog a join xrmatype b" & vbcrlf & _
					"	on	a.rmastatus = b.id left outer join xreturnreason c" & vbcrlf & _
					"	on	a.returnreason = c.id left outer join we_adminUsers d" & vbcrlf & _
					"	on	a.adminID = d.adminID left outer join we_items e" & vbcrlf & _
					"	on	a.itemid = e.itemid left outer join we_models f" & vbcrlf & _
					"	on	e.modelid = f.modelid" & vbcrlf & _
					"where	a.logDate >= '" & strSDate & "'" & vbcrlf & _
					"	and	a.logDate < '" & dateadd("d", 1, strEDate) & "'" & vbcrlf & _
					strSqlWhere & vbcrlf & _
					"order by 1 desc"
'			response.write "<pre>" & sql & "</pre>"
'			response.end
			session("errorSQL") = sql
			set rs = oConn.execute(sql)
			if rs.eof then
				response.write "No data to display"
			else
				%>
				<div style="width:100%; height:30px; border:1px solid #c1e0b2; border-radius:3px; background-color:#eaf5e5; padding:5px;">
					<div style="float:left; width:42px; font-size:13px; font-weight:bold;">SITE</div>
					<div style="float:left; width:72px; font-size:13px; font-weight:bold;">Log Date</div>
					<div style="float:left; width:72px; font-size:13px; font-weight:bold;">ORDERID</div>
					<div style="float:left; width:62px; font-size:13px; font-weight:bold;">ITEMID</div>
					<div style="float:left; width:42px; font-size:13px; font-weight:bold;">QTY</div>
					<div style="float:left; width:112px; font-size:13px; font-weight:bold;">PART<br />NUMBER</div>
					<div style="float:left; width:102px; font-size:13px; font-weight:bold;">Last<br />RMAStatus</div>
					<div style="float:left; width:152px; font-size:13px; font-weight:bold;">Activity Desc</div>
					<div style="float:left; width:72px; font-size:13px; font-weight:bold;">Credit Amount</div>
					<div style="float:left; width:52px; font-size:13px; font-weight:bold;">Credit Issued</div>
					<div style="float:left; width:202px; font-size:13px; font-weight:bold;">Return Reason</div>
					<div style="float:left; width:112px; font-size:13px; font-weight:bold;">Admin User</div>
				</div>
				<%
				lap = 0
				nTotalQty = clng(0)
				nTotalCredit = cdbl(0)
				do until rs.eof
					nTotalQty = nTotalQty + clng(rs("qty"))
					nTotalCredit = nTotalCredit + cdbl(rs("amountCredited"))
					lap = lap + 1
					select case rs("site_id")
						case 0 : site = "WE"
						case 1 : site = "CA"
						case 2 : site = "CO"
						case 3 : site = "PS"
						case 10 : site = "TM"
					end select
					
					confirmAmountCredited = "N"
					if rs("confirmAmountCredited") then confirmAmountCredited = "<b>Y</b>"
	
					rowBackgroundColor = "#fff"
					if (lap mod 2) = 0 then rowBackgroundColor = "#eaf5fa"
					
					altText = "origItemPrice: " & formatcurrency(rs("origItemPrice"), 2) & ", RS_FEE: " & rs("restocking_fee_percent") & "%, itemTax: " & formatcurrency(rs("itemTax"), 2) & ", Discount: " & rs("discountPercent") & "%"
					%>
				<div class="table-row" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='1px solid <%=rowBackgroundColor%>';this.style.backgroundColor='<%=rowBackgroundColor%>'">
					<div class="table-cell" style="width:40px;"><%=site%>&nbsp;</div>
					<div class="table-cell" style="width:70px;"><%=rs("logdate")%>&nbsp;</div>
					<div class="table-cell" style="width:70px;"><%=rs("orderid")%>&nbsp;</div>
					<div class="table-cell" style="width:60px;"><%=rs("itemid")%>&nbsp;</div>
					<div class="table-cell" style="width:40px;"><%=rs("qty")%>&nbsp;</div>
					<div class="table-cell" style="width:110px;"><%=rs("partnumber")%>&nbsp;</div>
					<div class="table-cell" style="width:100px;"><%=rs("lastRmaStatus")%>&nbsp;</div>
					<div class="table-cell" style="width:150px;"><%=rs("activity_desc")%>&nbsp;</div>
					<div class="table-cell" style="width:70px;" title="<%=altText%>"><%=formatcurrency(rs("amountCredited"))%>&nbsp;</div>
					<div class="table-cell" style="width:50px;"><%=confirmAmountCredited%>&nbsp;</div>
					<div class="table-cell" style="width:200px;"><%=rs("returnreason")%>&nbsp;</div>
					<div class="table-cell" style="width:110px;"><%=rs("userName")%>&nbsp;</div>
				</div>
					<%
					rs.movenext
				loop
				%>
				<div style="padding:5px; margin-top:10px; border-top:2px solid #c1e0b2; font-weight:bold; font-size:16px;" align="right">Total QTY: <%=formatnumber(nTotalQty,0)%> &nbsp; &nbsp; Total Credit: <%=formatcurrency(nTotalCredit,2)%></div>
                <%
			end if
		else
			if "" <> strSqlGroupBy then
				strSqlGroupBy	=	"group by " & left(trim(strSqlGroupBy), len(trim(strSqlGroupBy))-1)
				strSqlOrderBy	=	"order by " & left(trim(strSqlOrderBy), len(trim(strSqlOrderBy))-1)
			end if
			
			sql	=	"select	isnull(sum(a.qty), 0) qty, isnull(sum(a.amountCredited), 0) amountCredited " & strSqlSelectMain & vbcrlf & _
					"from	rmalog a join xrmatype b" & vbcrlf & _
					"	on	a.rmastatus = b.id left outer join xreturnreason c" & vbcrlf & _
					"	on	a.returnreason = c.id left outer join we_adminUsers d" & vbcrlf & _
					"	on	a.adminID = d.adminID left outer join we_items e" & vbcrlf & _
					"	on	a.itemid = e.itemid left outer join we_models f" & vbcrlf & _
					"	on	e.modelid = f.modelid left outer join we_types g" & vbcrlf & _
					"	on	e.typeid = g.typeid" & vbcrlf & _
					"where	a.logDate >= '" & strSDate & "'" & vbcrlf & _
					"	and	a.logDate < '" & dateadd("d", 1, strEDate) & "'" & vbcrlf & _
					strSqlWhere & vbcrlf & _
					strSqlGroupBy & vbcrlf & _					
					strSqlOrderBy
			session("errorSQL") = sql
			arrResult = getDbRows(sql)
			%>
            <div style="width:100%; height:30px; border:1px solid #c1e0b2; border-radius:3px; background-color:#eaf5e5; padding:5px;">
            	<%=strHeadings%>
                <div style="float:left; width:42px; font-size:13px; font-weight:bold;">QTY</div>
                <div style="float:left; width:82px; font-size:13px; font-weight:bold;">Credit Amount</div>
            </div>
	        <%
			if not isnull(arrResult) then
				nTotalQty = clng(0)
				nTotalCredit = cdbl(0)
				for nRow=0 to ubound(arrResult,2)
					nTotalQty = clng(nTotalQty) + clng(arrResult(0,nRow))
					nTotalCredit = cdbl(nTotalCredit) + cdbl(arrResult(1,nRow))
					rowBackgroundColor = "#eaf5fa"
					if (nRow mod 2) = 0 then rowBackgroundColor = "#fff"				
				%>
				<div class="table-row" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='1px solid <%=rowBackgroundColor%>';this.style.backgroundColor='<%=rowBackgroundColor%>'">
                <%
					nSkipColumn = 2
					for i=0 to ubound(arrGroupBy)
						if "" <> trim(arrGroupBy(i)) then
							select case ucase(trim(arrGroupBy(i)))
								case "DATE"
								%>
                                    <div class="table-cell" style="width:100px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
                                <%
								Case "STORE"
									select case cint(arrResult(nSkipColumn,nRow))
										case 0 : site = "WE"
										case 1 : site = "CA"
										case 2 : site = "CO"
										case 3 : site = "PS"
										case 10 : site = "TM"
									end select
									%>
                                    <div class="table-cell" style="width:40px;"><%=site%>&nbsp;</div>
                                    <%
								Case "RETURN REASON"
								%>
                                    <div class="table-cell" style="width:200px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
                                <%
								Case "MODEL"
								%>
                                    <div class="table-cell" style="width:250px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
                                <%
								Case "CATEGORY"
								%>
                                    <div class="table-cell" style="width:120px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
                                <%
								Case "ITEMID"
								%>
                                    <div class="table-cell" style="width:70px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
                                <%
								Case "ACTIVITY DESC"
								%>
                                    <div class="table-cell" style="width:150px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
                                <%								
							end select
							nSkipColumn = nSkipColumn + 1										
						end if
					next
					%>
                	<div class="table-cell" style="width:40px;"><%=formatnumber(arrResult(0,nRow),0)%></div>
                	<div class="table-cell" style="width:80px;"><%=formatcurrency(arrResult(1,nRow))%></div>                    
				</div>
                    <%
				next
				%>
				<div style="padding:5px; margin-top:10px; border-top:2px solid #c1e0b2; font-weight:bold; font-size:16px;" align="right">Total QTY: <%=formatnumber(nTotalQty,0)%> &nbsp; &nbsp; Total Credit: <%=formatcurrency(nTotalCredit,2)%></div>
                <%
			end if
		end if
		%>
       	</div>
        <!--// result -->
    </div>
</form>    
</center>
<script type="text/javascript" src="/includes/admin/checkbox.js"></script>
<script>
	function printinvoice(orderid,accountid) {
		var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
		window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
	}
/*
function onSetMasterCheckBox(frm) {
	if (isAllChecked(frm.chkUpdate))			frm.chkMaster.checked = true;
	else if (isAllUnChecked(frm.chkUpdate))	frm.chkMaster.checked = false;
}
*/
