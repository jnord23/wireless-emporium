<%
	thisUser = Request.Cookies("username")
	pageTitle = ""
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	storeID = prepInt(request.Form("storeID"))
	dayCnt = prepInt(request.Form("dayCnt"))
	viewType = prepInt(request.Form("viewType"))
	
	sql = 	"select a.orderdatetime, b.quantity, a.store, a.orderID " &_
			"from we_orders a " &_
				"join we_orderDetails b on a.orderID = b.orderID " &_
			"where a.orderdatetime > '" & (date - dayCnt) & "' and store = " & storeID & " and approved = 1 and cancelled is null and parentOrderID is null and (extOrderType is null or extOrderType < 4) " &_
			"order by a.orderdatetime"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	sql = 	"select a.orderdatetime, b.quantity, a.store, a.orderID " &_
			"from we_orders a " &_
				"join we_orderDetails b on a.orderID = b.orderID " &_
			"where a.orderdatetime > '" & (date - (dayCnt + 7)) & "' and a.orderdatetime < '" & (date - (dayCnt + (6-dayCnt))) & "' and store = " & storeID & " and approved = 1 and cancelled is null and parentOrderID is null and (extOrderType is null or extOrderType < 4) " &_
			"order by a.orderdatetime"
	session("errorSQL") = sql
	set prevRS = oConn.execute(sql)
	
	prevPosition = 0
	curPosition = 0
	bumpAmt = 220
%>
<table border="0" cellpadding="3" cellspacing="0" width="600" style="padding-top:20px;">
    <tr>
    	<td colspan="4">
        	<form action="/admin/detailedOrderCnt.asp" method="post" name="optionForm">
            <div style="height:30px; width:500px; text-align:left;">
                <div style="float:left; font-weight:bold; width:150px; height:30px; text-align:right;">Website to review:</div>
                <div style="float:left; padding-left:10px; width:300px; height:30px;">
                    <select name="storeID" onchange="document.optionForm.submit()">
                        <option value="0">Wireless Emporium</option>
                        <option value="2"<% if storeID = 2 then %> selected="selected"<% end if %>>Cellular Outfitter</option>
                        <option value="1"<% if storeID = 1 then %> selected="selected"<% end if %>>Cellphone Accents</option>
                        <option value="3"<% if storeID = 3 then %> selected="selected"<% end if %>>Phone Sale</option>
                        <option value="10"<% if storeID = 10 then %> selected="selected"<% end if %>>Tablet Mall</option>
                    </select>
                </div>
            </div>
            <div style="height:30px; width:500px; text-align:left;">
            	<div style="float:left; font-weight:bold; width:150px; height:30px; text-align:right;">Days to Review:</div>
                <div style="float:left; padding-left:10px; width:300px; height:30px;">
                    <select name="dayCnt" onchange="document.optionForm.submit()">
                        <option value="0">Just Today</option>
                        <option value="1"<% if dayCnt = 1 then %> selected="selected"<% end if %>>2 Days</option>
                        <option value="2"<% if dayCnt = 2 then %> selected="selected"<% end if %>>3 Days</option>
                        <option value="3"<% if dayCnt = 3 then %> selected="selected"<% end if %>>4 Days</option>
                        <option value="4"<% if dayCnt = 4 then %> selected="selected"<% end if %>>5 Days</option>
                    </select>
                </div>
            </div>
            <div style="height:30px; width:500px; text-align:left;">
            	<div style="float:left; font-weight:bold; width:150px; height:30px; text-align:right;">Previous Week:</div>
                <div style="float:left; padding-left:10px; width:300px; height:30px;">
                    <select name="viewType" onchange="document.optionForm.submit()">
                        <option value="0">Show</option>
                        <option value="1"<% if viewType = 1 then %> selected="selected"<% end if %>>Hide</option>
                    </select>
                </div>
            </div>
            <div style="height:30px; width:200px; text-align:left; padding-left:150px;"><a href="/admin/activeSales.asp">Switch to Active Sales Report</a></div>
            </form>
        </td>
    </tr>
    <tr>
    	<td>
        	<div style="position:absolute; display:table;">
				<%
				if viewType = 0 then
					bumpAmt = 440
					curPosition = 220
					dayLap = 0
					cellNum = 0
					hiOrderCell_1 = 0
					hiOrderCell_2 = 0
					hiOrderCell_3 = 0
					hiOrderCell_4 = 0
					hiOrderCell_5 = 0
					hiItemCell_1 = 0
					hiItemCell_2 = 0
					hiItemCell_3 = 0
					hiItemCell_4 = 0
					hiItemCell_5 = 0
					
					if prevRS.EOF then
					%>
					<div style="font-weight:bold; color:#F00; font-size:18px;" nowrap="nowrap">No Sales Recorded</div>
					<%
					end if
					
					do while not prevRS.EOF
						'reset for a new day
						hiOrders = 0
						hiItems = 0
						dayOrderCnt = 0
						dayItemsPurchased = 0
						dayLap = dayLap + 1
						headerDate = left(prevRS("orderdatetime"),instr(prevRS("orderdatetime")," ")-1)
						bgColor = "#fff"
						cellNum = 0
					%>
					<div style="position:absolute; left:<%=prevPosition%>px; top:0px;">
						<div style="height:20px; border:1px solid #000; width:210px; text-align:center; background-color:#606def; color:#fff; font-weight:bold;"><%=headerDate%> (<%=WeekdayName(Weekday(cdate(headerDate)))%>)</div>
						<div style="height:20px; border:1px solid #000; width:210px; background-color:#bcc1f8; color:#000;">
							<div style="float:left; padding-left:5px; border-right:1px solid #000; width:60px;">Time</div>
							<div style="float:left; padding:0px 5px 0px 5px; border-right:1px solid #000; width:60px;">Orders</div>
							<div style="float:left; padding-left:5px; width:60px;">Items</div>
						</div>
						<%
						curTime = 0
						endNow = 0
						do while not prevRS.EOF
							'reset for a new time
							orderCnt = 0
							itemsPurchased = 0
							do while not prevRS.EOF
								'total numbers for this hour
								dateTime = prevRS("orderdatetime")
								curDate = left(dateTime,instr(dateTime," ")-1)
								if curDate > headerDate then exit do
								itemCnt = prevRS("quantity")
								store = prevRS("store")
								orderID = prevRS("orderID")
								if curTime+1 = 24 then
									stopDate = cdate(curDate)+1
								else
									if curTime+1 > 11 then
										stopDate = cdate(curDate & " " & curTime+1 & ":00:00 PM")
									else
										stopDate = cdate(curDate & " " & curTime+1 & ":00:00 AM")
									end if
									'response.Write("stopDate:" & stopDate & "<br>")
								end if
								if dateTime < stopDate then
									orderCnt = orderCnt + 1
									do while orderID = prevRS("orderID")
										itemsPurchased = itemsPurchased + itemCnt
										prevRS.movenext
										if prevRS.EOF then
											exit do
										else
											newDate = left(prevRS("orderdatetime"),instr(prevRS("orderdatetime")," ")-1)
										end if
									loop
								else
									exit do
								end if
								if newDate > curDate then exit do
							loop
							cellNum = cellNum + 1
							if orderCnt > hiOrders then
								hiOrders = orderCnt
								if dayLap = 1 then hiOrderCell_1 = cellNum
								if dayLap = 2 then hiOrderCell_2 = cellNum
								if dayLap = 3 then hiOrderCell_3 = cellNum
								if dayLap = 4 then hiOrderCell_4 = cellNum
								if dayLap = 5 then hiOrderCell_5 = cellNum
							end if
							if itemsPurchased > hiItems then
								hiItems = itemsPurchased
								if dayLap = 1 then hiItemCell_1 = cellNum
								if dayLap = 2 then hiItemCell_2 = cellNum
								if dayLap = 3 then hiItemCell_3 = cellNum
								if dayLap = 4 then hiItemCell_4 = cellNum
								if dayLap = 5 then hiItemCell_5 = cellNum
							end if
						%>
						<div style="height:20px; border:1px solid #000; width:210px; background-color:<%=bgColor%>;">
							<div style="float:left; padding-left:5px; border-right:1px solid #000; width:60px;"><%=curTime%>:00</div>
							<div id="prevDay_<%=dayLap%>_orderCell_<%=cellNum%>" style="float:left; padding:0px 5px 0px 5px; border-right:1px solid #000; width:60px; text-align:right;" title="<%=dayOrderCnt + orderCnt%> so far"><%=orderCnt%></div>
							<div id="prevDay_<%=dayLap%>_itemCell_<%=cellNum%>" style="float:left; padding:0px 5px 0px 5px; width:63px; text-align:right;" title="<%=dayItemsPurchased + itemsPurchased%> so far"><%=itemsPurchased%></div>
						</div>
						<%
							dayOrderCnt = dayOrderCnt + orderCnt
							dayItemsPurchased = dayItemsPurchased + itemsPurchased
							if bgColor = "#fff" then bgColor = "#d3d6f3" else bgColor = "#fff"
							curTime = curTime + 1
							if curTime > 23 then exit do
						loop
						%>
						<div style="height:20px; border:1px solid #000; width:210px; background-color:#000; color:#FFF;">
							<div style="float:left; padding-left:5px; border-right:1px solid #000; width:60px;">Total</div>
							<div style="float:left; padding:0px 5px 0px 5px; border-right:1px solid #000; width:60px; text-align:right;"><%=dayOrderCnt%></div>
							<div style="float:left; padding-left:5px; width:60px; text-align:right;"><%=dayItemsPurchased%></div>
						</div>
					</div>
					<%
						prevPosition = prevPosition + bumpAmt
					loop
				end if
                
                dayLap = 0
                cellNum = 0
                hiOrderCell_1 = 0
                hiOrderCell_2 = 0
                hiOrderCell_3 = 0
                hiOrderCell_4 = 0
                hiOrderCell_5 = 0
                hiItemCell_1 = 0
                hiItemCell_2 = 0
                hiItemCell_3 = 0
                hiItemCell_4 = 0
                hiItemCell_5 = 0
                
                if rs.EOF then
                %>
                <div style="font-weight:bold; color:#F00; font-size:18px;" nowrap="nowrap">No Sales Recorded</div>
                <%
                end if
                
                do while not rs.EOF
                    'reset for a new day
                    hiOrders = 0
                    hiItems = 0
                    dayOrderCnt = 0
                    dayItemsPurchased = 0
                    dayLap = dayLap + 1
                    headerDate = left(rs("orderdatetime"),instr(rs("orderdatetime")," ")-1)
                    bgColor = "#fff"
					cellNum = 0
                %>
                <div style="position:absolute; left:<%=curPosition%>px; top:0px;">
                    <div style="height:20px; border:1px solid #000; width:210px; text-align:center; background-color:#000; color:#FFF; font-weight:bold;"><%=headerDate%> (<%=WeekdayName(Weekday(cdate(headerDate)))%>)</div>
                    <div style="height:20px; border:1px solid #000; width:210px; background-color:#666; color:#FFF;">
                        <div style="float:left; padding-left:5px; border-right:1px solid #000; width:60px;">Time</div>
                        <div style="float:left; padding:0px 5px 0px 5px; border-right:1px solid #000; width:60px;">Orders</div>
                        <div style="float:left; padding-left:5px; width:60px;">Items</div>
                    </div>
                    <%
                    curTime = 0
                    endNow = 0
                    do while not rs.EOF
                        'reset for a new time
                        orderCnt = 0
                        itemsPurchased = 0
                        do while not rs.EOF
                            'total numbers for this hour
                            dateTime = rs("orderdatetime")
                            curDate = left(dateTime,instr(dateTime," ")-1)
                            if curDate > headerDate then exit do
                            itemCnt = rs("quantity")
                            store = rs("store")
                            orderID = rs("orderID")
                            if curTime+1 = 24 then
                                stopDate = cdate(curDate)+1
                            else
                                if curTime+1 > 11 then
                                    stopDate = cdate(curDate & " " & curTime+1 & ":00:00 PM")
                                else
                                    stopDate = cdate(curDate & " " & curTime+1 & ":00:00 AM")
                                end if
                                'response.Write("stopDate:" & stopDate & "<br>")
                            end if
                            if dateTime < stopDate then
                                orderCnt = orderCnt + 1
                                do while orderID = rs("orderID")
                                    itemsPurchased = itemsPurchased + itemCnt
                                    rs.movenext
                                    if rs.EOF then
                                        exit do
                                    else
                                        newDate = left(rs("orderdatetime"),instr(rs("orderdatetime")," ")-1)
                                    end if
                                loop
                            else
                                exit do
                            end if
                            if newDate > curDate then exit do
                        loop
                        cellNum = cellNum + 1
                        if orderCnt > hiOrders then
                            hiOrders = orderCnt
                            if dayLap = 1 then hiOrderCell_1 = cellNum
                            if dayLap = 2 then hiOrderCell_2 = cellNum
                            if dayLap = 3 then hiOrderCell_3 = cellNum
                            if dayLap = 4 then hiOrderCell_4 = cellNum
                            if dayLap = 5 then hiOrderCell_5 = cellNum
                        end if
                        if itemsPurchased > hiItems then
                            hiItems = itemsPurchased
                            if dayLap = 1 then hiItemCell_1 = cellNum
                            if dayLap = 2 then hiItemCell_2 = cellNum
                            if dayLap = 3 then hiItemCell_3 = cellNum
                            if dayLap = 4 then hiItemCell_4 = cellNum
                            if dayLap = 5 then hiItemCell_5 = cellNum
                        end if
                    %>
                    <div style="height:20px; border:1px solid #000; width:210px; background-color:<%=bgColor%>;">
                        <div style="float:left; padding-left:5px; border-right:1px solid #000; width:60px;"><%=curTime%>:00</div>
                        <div id="day_<%=dayLap%>_orderCell_<%=cellNum%>" style="float:left; padding:0px 5px 0px 5px; border-right:1px solid #000; width:60px; text-align:right;" title="<%=dayOrderCnt + orderCnt%> so far"><%=orderCnt%></div>
                        <div id="day_<%=dayLap%>_itemCell_<%=cellNum%>" style="float:left; padding:0px 5px 0px 5px; width:63px; text-align:right;" title="<%=dayItemsPurchased + itemsPurchased%> so far"><%=itemsPurchased%></div>
                    </div>
                    <%
                        dayOrderCnt = dayOrderCnt + orderCnt
                        dayItemsPurchased = dayItemsPurchased + itemsPurchased
                        if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
                        curTime = curTime + 1
                        if curTime > 23 then exit do
                    loop
                    %>
                    <div style="height:20px; border:1px solid #000; width:210px; background-color:#000; color:#FFF;">
                        <div style="float:left; padding-left:5px; border-right:1px solid #000; width:60px;">Total</div>
                        <div style="float:left; padding:0px 5px 0px 5px; border-right:1px solid #000; width:60px; text-align:right;"><%=dayOrderCnt%></div>
                        <div style="float:left; padding-left:5px; width:60px; text-align:right;"><%=dayItemsPurchased%></div>
                    </div>
                </div>
                <%
					curPosition = curPosition + bumpAmt
                loop
                %>
	        </div>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	var dayLap = <%=dayLap%>;
	
	var prevOrderHigh = 0;
	var prevOrderHighCell = 0;
	var prevItemHigh = 0;
	var prevItemHighCell = 0;
	
	var orderHigh = 0;
	var orderHighCell = 0;
	var itemHigh = 0;
	var itemHighCell = 0;
	
	var curOrder = 0;
	var curItem = 0;
	
	for(i = 1;i <= dayLap;i++) {
		for(x = 1;x <= 24;x++) {
			if (document.getElementById("prevDay_" + i + "_orderCell_" + x) != null) {
				curOrder = parseInt(document.getElementById("prevDay_" + i + "_orderCell_" + x).innerHTML);
				if (curOrder > prevOrderHigh) { prevOrderHigh = curOrder; prevOrderHighCell = x }
				curItem = parseInt(document.getElementById("prevDay_" + i + "_itemCell_" + x).innerHTML);
				if (curItem > prevItemHigh) { prevItemHigh = curItem; prevItemHighCell = x }
			}
			
			if (document.getElementById("day_" + i + "_orderCell_" + x) != null) {
				curOrder = parseInt(document.getElementById("day_" + i + "_orderCell_" + x).innerHTML);
				if (curOrder > orderHigh) { orderHigh = curOrder; orderHighCell = x }
				curItem = parseInt(document.getElementById("day_" + i + "_itemCell_" + x).innerHTML);
				if (curItem > itemHigh) { itemHigh = curItem; itemHighCell = x }
			}
		}
		
		if (prevOrderHighCell > 0) {
			document.getElementById("prevDay_" + i + "_orderCell_" + prevOrderHighCell).style.backgroundColor = "#ff0000";
			document.getElementById("prevDay_" + i + "_itemCell_" + prevItemHighCell).style.backgroundColor = "#FF0";
		}
		
		if (orderHighCell > 0) {
			document.getElementById("day_" + i + "_orderCell_" + orderHighCell).style.backgroundColor = "#ff0000";
			document.getElementById("day_" + i + "_itemCell_" + itemHighCell).style.backgroundColor = "#FF0";
		}
		
		orderHigh = 0;
		orderHighCell = 0;
		itemHigh = 0;
		itemHighCell = 0;
	}
</script>