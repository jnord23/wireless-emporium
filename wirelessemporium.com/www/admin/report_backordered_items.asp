<%
pageTitle = "Admin - Generate Back-Ordered Items Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<h3>Back-Ordered Items Report</h3>

<%
strError = ""
if request("submitted") = "Update" then
	for a = 1 to request("totalCount")
		if request.form("delete" & a) = "1" then
			SQL = "DELETE FROM BackOrdered_Items WHERE id='" & request.form("id" & a) & "'"
			'response.write "<p>" & SQL & "</p>"
			oConn.execute SQL
		end if
	next
	PartNumber = request.form("partNEW")
	Qty = request.form("qtyNEW")
	if PartNumber = "" then
		response.write "<p>You must enter a valid Part Number.</p>" & vbcrlf
	else
		SQL = "INSERT INTO BackOrdered_Items (PartNumber,Qty,DateEntered) VALUES ('" & PartNumber & "','" & Qty & "','" & now & "')"
		'response.write "<p>" & SQL & "</p>" & vbcrlf
		oConn.execute SQL
		response.write "<h3>UPDATED!</h3>" & vbcrlf
	end if
end if

SQL = "SELECT * FROM BackOrdered_Items ORDER BY PartNumber"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
a = 0
%>
<table border="1" cellpadding="3" cellspacing="0" width="95%" align="center" class="smlText">
	<form action="report_backordered_items.asp" method="post">
		<tr>
			<td valign="top" align="center"><b>Part&nbsp;Number</b></td>
			<td valign="top" align="left"><b>Qty</b></td>
			<td valign="top" align="left"><b>Description</b></td>
			<td valign="top" align="left"><b>Date&nbsp;Ent'd</b></td>
			<td valign="top" align="center"><b>Delete</b></td>
		</tr>
		<%
		do until RS.eof
			a = a + 1
			SQL = "SELECT itemDesc FROM we_Items WHERE PartNumber = '" & RS("PartNumber") & "' AND hideLive = 0 ORDER BY inv_qty DESC"
			set RS2 = Server.CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			if RS2.eof then
				itemDesc = "NOT FOUND!"
			else
				itemDesc = RS2("itemDesc")
			end if
			%>
			<tr>
				<td valign="top" align="center"><%=RS("PartNumber")%></td>
				<td valign="top" align="center"><%=RS("Qty")%></td>
				<td valign="top" align="left"><%=itemDesc%></td>
				<td valign="top" align="center"><%=RS("DateEntered")%></td>
				<td valign="top" align="center">
					<input type="hidden" name="id<%=a%>" value="<%=RS("id")%>">
					<input type="checkbox" name="delete<%=a%>" value="1">
				</td>
			</tr>
			<%
			RS.movenext
		loop
		%>
		<tr>
			<td align="left" colspan="5"><font color="#FF0000"><b>ADD NEW ITEM</b></font></td>
		</tr>
		<tr>
			<td valign="top" align="center"><input type="text" name="partNEW" value=""></td>
			<td valign="top" align="center"><input type="text" name="qtyNEW" value=""></td>
			<td valign="top" align="left" colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td align="center" colspan="5">
				<input type="hidden" name="totalCount" value="<%=a%>">
				<input type="submit" name="submitted" value="Update">
			</td>
		</tr>
	</form>
</table>
<p>&nbsp;</p>

<%
SQL = "SELECT PartNumber, itemDesc, inv_qty FROM we_Items"
SQL = SQL & " WHERE PartNumber IN (SELECT PartNumber FROM BackOrdered_Items)"
SQL = SQL & " AND hideLive = 0 AND inv_qty <> 0"
SQL = SQL & " ORDER BY PartNumber ASC, inv_qty DESC"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
if RS.eof then
	response.write "<h3>No records matched<br><br>So cannot make table...</h3>"
else
	%>
	<table border="1" cellpadding="3" cellspacing="0" width="95%" align="center" class="smlText">
		<tr>
			<td valign="top" align="center"><b>Part&nbsp;Number</b></td>
			<td valign="top" align="left"><b>Description</b></td>
			<td valign="top" align="center"><b>Qty</b></td>
		</tr>
		<%
		do until RS.eof
			inv_qty = RS("inv_qty")
			if RS("inv_qty") > 0 then
				%>
				<tr>
					<td valign="top" align="center"><%=RS("PartNumber")%></td>
					<td valign="top" align="left"><%=RS("itemDesc")%></td>
					<td valign="top" align="center"><%=inv_qty%></td>
				</tr>
				<%
			end if
			RS.movenext
		loop
		%>
	</table>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
