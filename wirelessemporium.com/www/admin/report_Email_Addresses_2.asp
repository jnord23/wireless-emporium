<%
pageTitle = "WE Admin Site - Export E-mail Addresses"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<script src="/Scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
<script src="/Scripts/jquery-ui-1.8.11.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function showLoadingAnimation() {
        var e = document.getElementById('LoadingAnimation');
        e.style.visibility = 'visible';
    }
    $(function () {
        $("#dateStart").datepicker();
        $("#dateEnd").datepicker();
    });
</script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
<style>
    #LoadingAnimation {
        visibility:hidden;
        display: inline-block;
        padding-top: 4px;
		white-space:nowrap;
		overflow:visible;
    }
</style>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">

<%
if request("submitted") <> "" then
	strError = ""
	dateStart = request("dateStart")
	dateEnd = request("dateEnd")
	intStore = request.form("Sites")
	if not isDate(dateStart) or not isDate(dateEnd) then
		strError = strError & "<p>dateStart and dateEnd must both be valid dates</p>"
	else
		dateStart = dateValue(dateStart)
		dateEnd = dateValue(dateEnd)
		if dateEnd < dateStart then strError = strError & "<p>dateEnd must be later than or equal to dateStart</p>"
	end if
	'TODO: Make sure a checkbox is checked
	
	if strError = "" then
		dim fs, file, filename, path
		'Map the file name to the physical path on the server.
		'Set fs = CreateObject("Scripting.FileSystemObject")
		filename = "export_Email_Addresses_2.csv"
		path = Server.MapPath("tempCSV") & "\" & filename
		path = replace(path,"admin\","")
		'Set file = fs.CreateTextFile(path)
		
		iMode = 8
		set ofs = server.CreateObject("Scripting.FileSystemObject")
		'response.write path
		'response.end
		if ofs.FileExists(path) then 'Delete the file manually because it is not being overwritten properly
			ofs.DeleteFile path, true
		end if
		
		set file = ofs.opentextfile(path, iMode, True)
		
		'Determine which sites to show results for
		strSiteIDs = ""
		sql = "select site_id, shortDesc from XStore"
		set rs = oConn.Execute(sql)
		'while not rs.eof
		'	iName = rs("shortDesc")
		'	iId = rs("site_id")
		'	'response.write "iName: " & iName & " iId: " & iId
		'	'response.write "request: " & request.form(iName)
		'	if trim(request.form(iName)) = trim(iId) then
		'		if strSiteIDs = "" then
		'			strSiteIDs = iId
		'		else
		'			strSiteIDs = strSiteIDs & "," & iId
		'		end if
		'	end if
		'	'response.write "{" & iName & ": " & request.form(iName) & "}"
		'	rs.movenext
		'wend
		'if strSiteIDs = "" then response.write " !EMPTY! "
		'response.write "strSiteIDs: " & strSiteIDs
		'response.end
		
		while not rs.eof
			
			if trim(rs("site_id")) = trim(intStore) then
				strStore = rs("shortDesc")
			end if
			rs.movenext
			
		wend
		
		'Tablet Mall has a new name, but database table is still ER
		if trim(strStore) = "TM" then strStore = "ER"
		
		'SQL = "select e.fname, e.email, e.dateEntered from " & strStore & "_emailopt e "
		'SQL = SQL & "left join v_accounts v on e.email = v.EMAIL "
		'SQL = SQL & "where v.site_id = " & intStore & " "
		'SQL = SQL & "and e.email not in (select email from " & strStore & "_emailopt_out) "
		'SQL = SQL & "and dateEntered >= '" & dateStart & "' "
		'SQL = SQL & "and dateEntered < dateadd(day, 1, '" & dateEnd & "') "
		'SQL = SQL & "order by dateEntered desc"
		
		
		SQL = "select distinct e.fname, e.email from " & strStore & "_emailopt e "
		SQL = SQL & "left join v_accounts v on e.email = v.EMAIL "
		SQL = SQL & "where v.site_id = " & intStore & " "
		SQL = SQL & "and e.email not in (select email from " & strStore & "_emailopt_out) "
		SQL = SQL & "and dateEntered >= '" & dateStart & "' "
		SQL = SQL & "and dateEntered < dateadd(day, 1, '" & dateEnd & "') "

		
		

		Set RS = Server.CreateObject("ADODB.Recordset")
		response.write "<h3>" & SQL & "</h3>" & vbCrLf
		RS.open SQL, oConn, 3, 3
		response.write "<h3>" & RS.recordcount & " TOTAL records found</h3>" & vbCrLf
		
		'strline = "fname,email,dateEntered"
		strline = "fname,email,dateEntered"
		file.WriteLine strline
		
		
		do until RS.eof
			'strline = (chr(34) & trim(RS("fname")) & chr(34) & "," & chr(34) & trim(RS("email")) & chr(34) & "," & chr(34) & RS("dateEntered") & chr(34))
			strline = (chr(34) & trim(RS("fname")) & chr(34) & "," & chr(34) & trim(RS("email")) & chr(34))
			file.WriteLine strline
			RS.movenext
		loop
		
		RS.close
		Set RS = nothing
		file.close()
		set ofs = nothing
		response.write "Here is the <a href='/tempCSV/" & filename & "'>file</a>"
	else
		%>
		<center>
		<font size="+2" color="red"><%=strError%></font>
		<a href="javascript:history.back();">BACK</a>
		</center>
		<%
	end if
else
	%>
	<p>&nbsp;</p>
	<h3>Export E-mail Addresses</h3>
	<br>
	<form action="" method="post">
		<p><input type="text" name="dateStart" id="dateStart" value="<%=date%>">&nbsp;&nbsp;Date Start</p>
		<p><input type="text" name="dateEnd" id="dateEnd" value="<%=date%>">&nbsp;&nbsp;Date End</p>
        <p><select name="Sites" id="Sites">
        	<% 
			sql = "select site_id, shortDesc from XStore"
			set rs = oConn.Execute(sql)
			while not rs.eof
				iName = rs("shortDesc")
				iId = rs("site_id")
				%>
				<option name="<%=iName%>" value="<%=iId%>"><%=iName%></option>
				<%
				rs.movenext
			wend
			%>
        </select> Site</p>
		<p><input type="submit" name="submitted" id="submitted" value="Generate CSV" onclick="showLoadingAnimation();document.getElementById('submitted').click();" ><div id="LoadingAnimation"><img src="/Content/Images/LoadingBar.gif" /></div></p>
	</form>
    
	<%
end if
%>

</td></tr></table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
