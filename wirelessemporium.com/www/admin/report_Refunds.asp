<%
pageTitle = "Admin - Generate Refunds Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" width="750" align="center" cellpadding="0" cellspacing="0"><tr><td>
<p>&nbsp;</p>
<p class="biggerText"><%=pageTitle%></p>
<br>

<%
strError = ""

if request("submitted") <> "" then
	StartDate = request.form("dc1")
	EndDate = request.form("dc2")
	if not isDate(StartDate) then
		strError = "Start Date must be a valid date."
	else
		StartDate = dateValue(StartDate)
	end if
	if not isDate(EndDate) then
		strError = "End Date must be a valid date."
	else
		EndDate = dateValue(EndDate)
	end if
	if strError = "" then
		if StartDate > EndDate then
			strError = "Start Date must be earlier than or equal to End Date."
		end if
	end if
	if strError = "" then
		response.write "<h3>" & StartDate & "&nbsp;to&nbsp;" & EndDate & "</h3>" & vbcrlf
		
		for a = 0 to 3
			select case a
				case 0 : site = "WE"
				case 1 : site = "CA"
				case 2 : site = "CO"
				case 3 : site = "PS"
			end select
			
			SQL = "SELECT A.orderid, A.ordergrandtotal, A.refundAmount, B.accountid"
			SQL = SQL & " FROM we_orders A INNER JOIN " & site & "_accounts B ON A.accountid = B.accountid"
			SQL = SQL & " WHERE A.refunddate >= '" & StartDate & "' AND A.refunddate < '" & dateAdd("D",1,EndDate) & "'"
			SQL = SQL & " AND A.refundAmount IS NOT NULL"
			SQL = SQL & " AND (extOrderType IS NULL OR extOrderType <= 3)"
			SQL = SQL & " AND (B.email = 'phoneorder@wirelessemporium.com' OR B.email NOT LIKE '%@wirelessemporium.com')"
			SQL = SQL & " AND A.store = '" & a & "'"
			'response.write "<p>" & SQL & "</p>"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			
			response.write "<h3>" & site & ":</h3>" & vbcrlf
			
			dim GrandTotal, refundAmount, totalOrders
			GrandTotal = 0
			refundAmount = 0
			totalOrders = 0
			
			if not RS.eof then
				%>
				<table border="1" width="100%" cellpadding="2" cellspacing="0">
					<tr>
						<td align="center"><b>OrderID</b></td>
						<td><b>Order&nbsp;Total</b></td>
						<td><b>Refund&nbsp;Amount</b></td>
						<td><b>Net&nbsp;Total</b></td>
					</tr>
					<%
					do until RS.eof
						%>
						<tr>
							<td align="center"><a href="javascript:printinvoice('<%=RS("OrderId")%>','<%=RS("AccountId")%>');"><%=RS("orderid")%></a></td>
							<td><%=formatCurrency(cDbl(RS("ordergrandtotal")))%></td>
							<td><%=formatCurrency(RS("refundAmount"))%></td>
							<td><%=formatCurrency(cDbl(RS("ordergrandtotal")) - RS("refundAmount"))%></td>
						</tr>
						<%
						totalOrders = totalOrders + 1
						GrandTotal = GrandTotal + cDbl(RS("ordergrandtotal"))
						if not isNull(RS("refundAmount")) then refundAmount = refundAmount + RS("refundAmount")
						RS.movenext
					loop
					%>
					<tr>
						<td align="center"><b><%=totalOrders%></b></td>
						<td><b><%=formatCurrency(GrandTotal)%></b></td>
						<td><b><%=formatCurrency(refundAmount)%></b></td>
						<td><b><%=formatCurrency(GrandTotal - refundAmount)%></b></td>
					</tr>
				</table>
				<%
			else
				response.write "<p class=""boldText"">No records matched.</p><p class=""boldText"">No report generated.</p>"
			end if
			response.write "<p>&nbsp;</p>"
		next
		
		response.write "<p>&nbsp;</p>"
		response.write "<p class=""normalText""><a href=""report_Refunds.asp"">Generate another Refunds Report</a></p>"
		RS.close
		set RS = nothing
	end if
end if
if request("submitted") = "" or strError <> "" then
	%>
	<p class="normalText"><font color="#FF0000"><b><%=strError%></b></font></p>
	<form action="report_Refunds.asp" name="frmRefundsReport" method="post">
		<p class="normalText">
			<input type="text" name="dc1" value="<%=dateAdd("d",-1,date)%>">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.frmRefundsReport.dc1,document.frmRefundsReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;Start Date
			<br>
			<input type="text" name="dc2" value="<%=dateAdd("d",-1,date)%>">&nbsp;<a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fEndPop(document.frmRefundsReport.dc1,document.frmRefundsReport.dc2);return false;"><img class="PopcalTrigger" align="absmiddle" src="calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a>&nbsp;&nbsp;End Date
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
		</p>
		<hr>
		<p class="normalText">
			<input type="submit" name="submitted" value="Submit">
		</p>
	</form>
	<%
end if
%>

</td></tr></table>

<p>&nbsp;</p>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->

<script language="javascript">
function printinvoice(orderid,accountid) {
	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
</script>
