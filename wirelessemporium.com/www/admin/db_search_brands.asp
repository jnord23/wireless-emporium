<%
pageTitle = "Admin - Update WE Database - Select Brand"
header = 1
searchID = request.querystring("searchID")
if searchID = "" then
	response.redirect("menu.asp")
end if
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<blockquote>
<blockquote>

<font face="Arial,Helvetica">
<h3 style="margin-bottom:0px;">Select a Brand:</h3>

<%
SQL = "SELECT * FROM WE_Brands ORDER BY brandName"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
do until RS.eof
	if searchID = "9" then
		%>
		<a href="/admin/db_update_flag.asp?brandID=<%=RS("brandID")%>"><%=RS("brandName")%></a><br>
		<%
	elseif searchID = "10" then
		%>
		<a href="/admin/db_update_temps.asp?brandID=<%=RS("brandID")%>"><%=RS("brandName")%></a><br>
		<%
	else
		%>
		<a href="/admin/db_search_models_types.asp?brandID=<%=RS("brandID")%>&searchID=<%=searchID%>"><%=RS("brandName")%></a><br>
		<%
	end if
	RS.movenext
loop
RS.close
set RS = nothing

if searchID = "1" then
	%>
	<a href="/admin/db_update_inv.asp?brandID=ALL&searchID=1&TypeID=8">Other Gear (ALL BRANDS)</a><br>
	<%
elseif searchID = "8" then
	%>
	<a href="/admin/db_update_price.asp?brandID=ALL&searchID=1&TypeID=8">Other Gear (ALL BRANDS)</a><br>
	<%
elseif searchID = "4" then
	%>
	<a href="/admin/db_search_models_types.asp?brandID=0&searchID=4">BRAND 0</a><br>
	<%
end if

if searchID = "1" or searchID = "2" then
	formAction = "db_update_inv.asp"
elseif searchID = "8" then
	formAction = "db_update_price.asp"
else
	formAction = "db_update_BUY.asp"
end if

if searchID = "1" or searchID = "2" or searchID = "7" or searchID = "8" then
	%>
	<br>
	<form action="<%=formAction%>" method="post" name="frmSearch1">
		<h3 style="margin-bottom:0px;">Search by Item Name Keywords:</h3>
		<input type="text" name="Keywords" size="50" value="">&nbsp;&nbsp;<input type="submit" name="submitItemNameSearch" value="Search">
	</form>
	<form action="<%=formAction%>" method="post" name="frmSearch2">
		<h3 style="margin-bottom:0px;">Search by Item Number:</h3>
		<input type="text" name="ItemNumber" size="10" value="">&nbsp;&nbsp;<input type="submit" name="submitItemNumberSearch" value="Search">
	</form>
	<form action="<%=formAction%>" method="post" name="frmSearch3">
		<h3 style="margin-bottom:0px;">
			Search by Part Number:
			<%if searchID = "1" or searchID = "8" then%>
			<br>[For wildcard searches, add "%" to the end of the partial Part Number. examples: SCR-% or SCR-SAM%]
			<%end if%>
		</h3>
		<input type="text" name="PartNumber" size="16" value="">&nbsp;&nbsp;<input type="submit" name="submitPartNumberSearch" value="Search">
	</form>
	<%
end if

if searchID = "1" or searchID = "7" or searchID = "8" then
	%>
	<form action="<%=formAction%>" method="post" name="frmSearch4">
		<h3 style="margin-bottom:0px;">Search by Type:</h3>
		<select name="TypeID">
			<option value=""></option>
			<%
			SQL = "SELECT * FROM WE_Types ORDER BY TypeName"
			Set RS2 = Server.CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			do until RS2.eof
				%>
				<option value="<%=RS2("TypeID")%>"><%=RS2("TypeName")%></option>
				<%
				RS2.movenext
			loop
			%>
		</select>&nbsp;&nbsp;<input type="submit" name="submitUpdateInv" value="Search">
	</form>
	<%
end if
%>

</font>

</blockquote>
</blockquote>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
