<%
pageTitle = "Product List for Fetchback - WE"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 9000 'seconds

dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
'Map the file name to the physical path on the server.
filename = "productList_Fetchback.csv"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List upload for Fetchback</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							response.write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='../tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		response.write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	dim fs, file
	response.write("<b>CreateFile:</b><br>")
	set fs = CreateObject("Scripting.FileSystemObject")
	'If the file already exists give an error message.
	if fs.FileExists(path) then
		response.write("File " & filename & " already exists, create aborted.<br>" & vbCrLf)
	else
		response.write("Creating " & filename & ".<br>")
		set file = fs.CreateTextFile(path)
		dim SQL, RS, strline
		SQL = "SELECT A.itemid, A.PartNumber, A.itemDesc, A.itemLongDetail, A.price_Our, A.itemPic, A.UPCCode, A.inv_qty, T.typename"
		SQL = SQL & " FROM we_Items A JOIN we_Types AS T ON A.typeid = T.typeid"
		SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
		SQL = SQL & " AND A.typeid <> 16"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			file.WriteLine "product name,product page url,product image url,product category,product description,Product ID,Price"
			do until RS.eof
				DoNotInclude = 0
				if RS("inv_qty") < 0 then
					SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
					set RS2 = Server.CreateObject("ADODB.Recordset")
					RS2.open SQL, oConn, 3, 3
					if RS2.eof then DoNotInclude = 1
					RS2.close
					set RS2 = nothing
				end if
				if DoNotInclude = 0 then
					strline = chr(34) & RS("itemDesc") & chr(34) & ","
					strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?id=fbproduct,"
					strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & ","
					strline = strline & chr(34) & RS("typeName") & chr(34) & ","
					strline = strline & chr(34) & replace(RS("itemLongDetail"),vbcrlf," ") & chr(34) & ","
					strline = strline & RS("itemID") & ","
					strline = strline & formatCurrency(RS("price_Our"))
					file.WriteLine strline
				end if
				RS.movenext
			loop
		end if
		RS.close
		set RS = nothing
		file.Close()
	end if
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
