<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Receive Orders"
	header = 1
%>
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%	
	set fso = CreateObject("Scripting.FileSystemObject")
	
	orderID = prepStr(request.QueryString("orderID"))
	completeOrder = prepInt(request.Form("completeOrder"))
	itemCnt = prepInt(request.Form("itemCnt"))
	vendorName = prepStr(request.Form("vendorName"))
	zeroOut = prepInt(request.QueryString("zeroOut"))
	uploadCsv = prepInt(request.QueryString("uploadCsv"))
	sortBy = prepStr(request.QueryString("sortBy"))
	session("curOrder") = "WEPN"
	
	userMsg = ""
	
	sql = "exec sp_vendorReceiveList"
	session("errorSQL") = sql
	Set vendorRS = oConn.execute(sql)
	
	'sql = "Select (select name from we_vendors where code = a.vendorCode) as vendor, a.*, (select top 1 itemDesc from we_items where partNumber = a.partNumber) as itemDesc from we_vendorOrder a where a.orderID = '" & orderID & "' and a.process = 1 and a.complete = 0 order by processOrderDate"
	sql = 	"exec sp_vendorOrderDetails '" & orderID & "',0,'" & sortBy & "'"
	'response.write "<pre>" & sql & "</pre>"
	session("vendorSQL") = sql
	session("errorSQL") = sql
	Set rs = oConn.execute(sql)
	
	if zeroOut = 1 then
		do while not rs.EOF
			if not rs("rowProcessed") then
				sql = "update we_vendorOrder set rowProcessed = 1, complete = 1, receivedAmt = 0 where orderID = '" & orderID & "' and id = '" & rs("orderLineID") & "'"
				session("errorSQL") = sql
				oConn.execute(sql)
			end if
			rs.movenext
		loop
		
		sql = "select a.id as orderLineID, b.name as vendor, a.orderID, a.rowProcessed, a.receivedAmt, a.orderAmt, a.partNumber, a.vendorPN, a.vendorCode, c.itemDesc, c.itemPic, c.inv_qty from we_vendorOrder a join we_vendors b on a.vendorCode = b.code left join we_Items c on a.partNumber = c.PartNumber and c.master = 1 where a.orderID = '" & orderID & "' and a.process = 1 order by a.receivedAmt, a.partNumber"
		session("errorSQL") = sql
		Set rs = oConn.execute(sql)
		
		baseEmailTxt =	"<div style='float:left; width:475px;'>" &_
							"<div style='float:left; font-size:18px; color:#000; padding:5px 0px 5px 0px; width:450px;'>Vendor:" & rs("vendor") & " (" & orderID & ")</div>"
		emailTxt = baseEmailTxt
		do while not rs.EOF
			useColor = "#F00"
			if prepInt(rs("receivedAmt")) <> prepInt(rs("orderAmt")) then
				emailTxt = emailTxt & 	"<div style='color:#000; padding:5px 0px 5px 0px; float:left;'>" &_
											"<div style='float:left; width:250px; height:30px; border-right:1px solid #000;'>Part Number:" & rs("partNumber") & "<br>Vendor Number:<span style='color:" & useColor & ";'>" & rs("vendorPN") & "</span></div>" &_
											"<div style='float:left; width:100px; height:30px; border-right:1px solid #000; padding-left:10px;'>OrderAmt:" & rs("orderAmt") & "</div>" &_
											"<div style='float:left; width:100px; height:30px; border-right:1px solid #000; padding-left:10px; color:" & useColor & ";'>ReceivedAmt:" & rs("receivedAmt") & "</div>" &_
										"</div>"
			end if
			rs.movenext
		loop
		if emailTxt <> baseEmailTxt then
			emailTxt = emailTxt & "</div>"
			set objErrMail = Server.CreateObject("CDO.Message")
			with objErrMail
				.From = "warehouse@wirelessemporium.com"
				'.To = "jon@wirelessemporium.com"
				.To = "jon@wirelessemporium.com;charles@wirelessemporium.com;yuna@wirelessemporium.com;gilbert@wirelessemporium.com"
				.Subject = "Order Received - Details"
				.HTMLBody = emailTxt
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
				.Configuration.Fields.Item("urn:schemas:httpmail:sender") = "warehouse@wirelessemporium.com"
				.Configuration.Fields.Update
				.Send
			end with
		end if
		response.Redirect("/admin/vendorReceiveOrders.asp")
	end if
%>
<style type="text/css">
	.dateStamp 			{ margin-right:10px; padding-right:10px; border-right:1px solid #000; padding-top:5px; height:20px; }
	.dateStampButton 	{ margin-right:10px; padding-right:10px; border-right:1px solid #000; height:25px; }
	
	.dateStamp2 		{ margin-right:10px; padding-right:10px; padding-top:5px; height:20px; }
	.dateStampButton2 	{ margin-right:10px; padding-right:10px; height:25px; }
	.awaitingOrders 	{ width:850px; border:1px solid #000; padding:5px; height:200px; overflow:auto; position:relative; }
	.vOrderID			{ width:100px; height:20px; margin-left:5px; }
	.vTitle				{ background-color:#000; color:#FFF; font-weight:bold; font-size:12px; width:100%; padding:2px; }
	.vName				{ width:150px; overflow:hidden; height:20px; }
	.vLink				{ color:#00F; cursor:pointer; }
	.vPriority			{ width:50px; height:20px; margin-left:5px; }
	.vBase				{ width:80px; height:20px; margin-left:5px; }
	.vBaseC				{ width:80px; height:20px; margin-left:5px; text-align:center; }
	.vBaseR				{ width:80px; height:20px; margin-left:5px; text-align:right; }
	.vOrderDate			{ width:80px; overflow:hidden; height:20px; margin-left:5px; }
	.pd2				{ padding:2px; }
	.notesPop			{ position:absolute; top:50px; left:250px; width:400px; height:50px; background-color:#FFF; border:2px solid #000; padding:5px; }
</style>
<table align="center" border="0" cellpadding="3" cellspacing="0">
	<% if userMsg <> "" then %>
    <tr><td align="center" style="font-weight:bold; color:#F00; border-bottom:1px solid #F00;"><%=userMsg%></td></tr>
    <% end if %>
    <tr>
    	<td align="center" style="padding:20px 0px 20px 0px;">
            <table border="0" cellpadding="3" cellspacing="0">
            	<tr>
                    <td valign="top">
                    	<% if vendorRS.EOF then %>
                        No orders currently waiting to be received
                        <% else %>
                        <div class="awaitingOrders">
                        	<div class="tb vTitle">
								<div class="fl vName">Vendor</div>
                                <div class="fl vOrderID">OrderID</div>
                                <div class="fl vPriority">Priority</div>
                                <div class="fl vBaseC">BackOrders</div>
                                <div class="fl vBaseC">OutOfStock</div>
                                <div class="fl vBaseC">Value</div>
                                <div class="fl vBaseC">EstArrival</div>
                                <div class="fl vBaseC">OrderDate</div>
                                <div class="fl vBaseC">Notes</div>
                            </div>
							<%
							do while not vendorRS.EOF
								if instr(vendorRS("processOrderDate")," ") > 0 then
									showDate = left(vendorRS("processOrderDate"),instr(vendorRS("processOrderDate")," ") - 1)
								else
									showDate = vendorRS("processOrderDate")
								end if
								useOrderID = vendorRS("orderID")
								vendorName = vendorRS("name")
								totalValue = vendorRS("totalValue")
								outOfStock = vendorRS("outOfStock")
								boQty = vendorRS("boQty")
								avgShip = vendorRS("avgShip")
								priority = vendorRS("priority")
								notes = prepStr(vendorRS("notes"))
								session("errorSQL") = "avgShip:" & avgShip
							%>
                            <div id="awaitingOrder_<%=useOrderID%>" class="tb pd2">
								<div class="fl vName vLink" onclick="window.location='/admin/vendorReceiveOrders.asp?orderID=<%=useOrderID%>'"><%=vendorName%></div>
                                <div class="fl vOrderID"><%=useOrderID%></div>
                                <div class="fl vPriority"><input type="text" name="prio" value="<%=priority%>" size="2" onkeyup="ajax('/ajax/admin/ajaxVendorReceiveOrders.asp?prioOrderID=<%=useOrderID%>&priority=' + this.value,'dumpZone')" /></div>
                                <div class="fl vBaseC"><%=boQty%></div>
                                <div class="fl vBaseC"><%=outOfStock%></div>
                                <div class="fl vBaseR"><%=formatCurrency(prepInt(totalValue),2)%></div>
                                <div class="fl vBaseR">
									<% if isnull(avgShip) then %>
                                    Unknown
									<% else %>
									<%=dateAdd("d",cint(avgShip),showDate)%>
                                    <% end if %>
                                </div>
                                <div class="fl vBaseR"><%=showDate%></div>
                                <div class="fl vBaseC">
                                	<% if notes = "" then %>
                                    <a href="javascript:voNotes('<%=useOrderID%>')" style="color:#0F0;">Add Notes</a>
                                    <% else %>
                                    <a href="javascript:voNotes('<%=useOrderID%>')" style="color:#C00;">View Notes</a>
                                    <% end if %>
                                </div>
                            </div>
                            <%
								vendorRS.movenext
							loop
							%>
                            <div id="voNotes" style="display:none;" class="tb notesPop"></div>
                        </div>
                        <% end if %>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <%
	if not rs.EOF then
		vendorName = rs("vendor")
		arrivalDate = prepStr(rs("arrivalDate"))
		breakdownDate = prepStr(rs("breakdownDate"))
		breakdownCompleteDate = prepStr(rs("breakdownCompleteDate"))
		restockDate = prepStr(rs("restockDate"))
		restockCompleteDate = prepStr(rs("restockCompleteDate"))
	%>
    <tr>
    	<td id="orderDetails">
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0">
            	<tr>
                	<td colspan="9" align="center" style="font-size:34px; font-weight:bold;">
                    	<div style="float:left;">Order For <%=vendorName%></div>
                        <div style="float:right;"><input type="button" name="clearOut" value="Zero Out Remaining Items" onclick="confirmZeroOut()" /></div>
                        <div style="float:right; padding-right:10px; margin-right:10px; border-right:1px dotted #CCC;">
                        <form action="/admin/vendorOrderUpload.asp" method="post" style="padding:0px; margin:0px;" enctype="multipart/form-data">
                            <div style="float:right;">
                            	<input type="hidden" name="uploadCsv" value="1" />
                                <input type="hidden" name="orderID" value="<%=orderID%>" />
                                <input type="submit" name="mySub" value="Upload CSV" style="font-size:10px;" />
                            </div>
                            <div style="float:right; margin-right:10px;"><input type="FILE" name="FILE1" style="font-size:10px;"></div>
                        </form>
                        </div>
                    </td>
                </tr>
                <tr>
                	<td colspan="9">
                    	<% if arrivalDate = "" then %>
                        <div id="arrivalDate" class="fl dateStampButton"><input type="button" name="myAction" value="Shipment Arrived" onclick="saveDate('arrivalDate','<%=date%>')" /></div>
                        <% else %>
                        <div id="arrivalDate" class="fl dateStamp">Arrival: <%=arrivalDate%></div>
                        <% end if %>
                        <% if breakdownDate = "" then %>
                        <div id="breakdownDate" class="fl dateStampButton"><input type="button" name="myAction" value="Start Break-Down" onclick="saveDate('breakdownDate','<%=date%>')" /></div>
                        <% else %>
                        <div id="breakdownDate" class="fl dateStamp">Breakdown Start: <%=breakdownDate%></div>
                        <% end if %>
                        <% if breakdownCompleteDate = "" then %>
                        <div id="breakdownCompleteDate" class="fl dateStampButton"><input type="button" name="myAction" value="Finish Break-Down" onclick="saveDate('breakdownCompleteDate','<%=date%>')" /></div>
                        <% else %>
                        <div id="breakdownCompleteDate" class="fl dateStamp">Breakdown Finish: <%=breakdownCompleteDate%></div>
                        <% end if %>
                        <% if restockDate = "" then %>
                        <div id="restockDate" class="fl dateStampButton"><input type="button" name="myAction" value="Start Restock" onclick="saveDate('restockDate','<%=date%>')" /></div>
                        <% else %>
                        <div id="restockDate" class="fl dateStamp">Restocked Start: <%=restockDate%></div>
                        <% end if %>
                        <% if restockCompleteDate = "" then %>
                        <div id="restockCompleteDate" class="fl dateStampButton2"><input type="button" name="myAction" value="Finish Restock" onclick="saveDate('restockCompleteDate','<%=date%>')" /></div>
                        <% else %>
                        <div id="restockCompleteDate" class="fl dateStamp2">Restocked Finish: <%=restockCompleteDate%></div>
                        <% end if %>
                    </td>
                </tr>
                <tr style="font-weight:bold; color:#FFF; background-color:#333;">
                	<td>Image</td>
                	<td nowrap="nowrap" style="cursor:pointer;" onclick="sortBy('WEPN')">WE Partnumber</td>
                    <td nowrap="nowrap" style="cursor:pointer;" onclick="sortBy('DETAILS')">We Description</td>
                	<td nowrap="nowrap" style="cursor:pointer;" onclick="sortBy('VPN')">Vendor Partnumber</td>
                    <td align="center">B.O.</td>
                    <td align="center">Inventory</td>
                    <td align="center">Ordered</td>
                    <td align="center">Received</td>
                    <td nowrap="nowrap">Action</td>
                </tr>
                <%
				bgColor = "#ffffff"
				lap = 0
				saveText = "WE Partnumber,We Description,Vendor Partnumber,B.O.,Inventory,Ordered,Received,Housing," & vbcrlf
				do while not rs.EOF
					lap = lap + 1					
					useSize = "11px"
					curBG = ""
					if rs("rowProcessed") then
						useReceived = rs("receivedAmt")
						curBG = bgColor
						bgColor = "#666666"
					else
						useReceived = rs("orderAmt")
					end if
					
					warehouseLocID = prepInt(rs("warehouseLocID"))
					tempItemDesc = prepVal(rs("itemDesc"))
					if instr(tempItemDesc,",") > 0 then tempItemDesc = replace(tempItemDesc,",","")
					tempVendorPN = prepVal(rs("vendorPN"))
					if instr(tempVendorPN,",") > 0 then tempVendorPN = replace(tempVendorPN,",","")
					
					saveText = saveText & rs("partnumber") & ","	'partnumber
					saveText = saveText & tempItemDesc & ","	'description
					saveText = saveText & tempVendorPN & ","	'vendor partnumber
					saveText = saveText & rs("boQty") & "," 'curInv
					saveText = saveText & rs("inv_qty") & "," 'curInv
					saveText = saveText & rs("orderAmt") & ","	'qty ordered
					saveText = saveText & useReceived & ","	'fulfillment qty
					if warehouseLocID > 0 then
						saveText = saveText & "Y,"
					else
						saveText = saveText & "N,"
					end if
					saveText = saveText & vbcrlf
				%>
                <form name="receiveForm_<%=lap%>" action="javascript:processRow(<%=lap%>)" method="post">
                <tr bgcolor="<%=bgColor%>" id="purchaseRow_<%=lap%>">
                	<td><img src="/productPics/thumb/<%=rs("itemPic")%>" border="0" /></td>
                    <td align="left" style="font-size:<%=useSize%>;"><%=rs("partNumber")%></td>
                    <td align="left" style="font-size:<%=useSize%>;">
						<div class="fl" style="width:400px;"><%=rs("itemDesc")%></div>
                        <% if prepInt(rs("warehouseLocID")) > 0 then %>
                        <div class="fr"><img src="/images/icons/houseIcon.png" border="0" width="30" height="30" alt="This item has a warehouse location" /></div>
                        <% end if %>
                    </td>
                	<td align="left">
                    	<input type="text" name="vpn_<%=lap%>" value="<%=rs("vendorPN")%>" onchange="updateVendorPN('<%=rs("vendorCode")%>','<%=rs("partNumber")%>',this.value)" style="font-size:<%=useSize%>;" />
                        <input type="hidden" name="partNumber_<%=lap%>" value="<%=rs("partNumber")%>" />
                        <input type="hidden" name="orderAmt_<%=lap%>" value="<%=rs("orderAmt")%>" />
                    </td>
                    <td align="center" style="color:#00F; font-weight:bold;"><%=rs("boQty")%></td>
                    <td align="center"><%=rs("inv_qty")%></td>
                    <td align="center" style="font-size:<%=useSize%>;"><%=rs("orderAmt")%></td>
                    <% if rs("rowProcessed") then %>
                    <td align="center" id="enterReceiveAmt_<%=lap%>" style="font-size:<%=useSize%>;">
						<%=useReceived%>
                        <input type="hidden" name="receiveAmt_<%=lap%>" value="<%=useReceived%>" />
                    </td>
                    <td id="actionBttn_<%=lap%>"><span style="color:#FFF; font-weight:bold;">PROCESSED</span></td>
					<% else %>
                    <td align="center" id="enterReceiveAmt_<%=lap%>" style="font-size:<%=useSize%>;"><input type="text" name="receiveAmt_<%=lap%>" value="<%=useReceived%>" size="3" style="font-size:<%=useSize%>;" /></td>
                    <td id="actionBttn_<%=lap%>"><input type="submit" name="myAction" value="Received" /></td>
                    <% end if %>
                </tr>
                </form>
                <%
					rs.movenext
					if curBG <> "" then bgColor = curBG
					if bgColor = "#ffffff" then bgColor = "#f0f0f0" else bgColor = "#ffffff"
				loop
				
				filename = "receiveVendorOrder_" & formatSEO(vendorName) & "_" & replace(date,"/","") & ".csv"
				path = Server.MapPath("vendorOrders") & "\" & filename
				path = replace(path,"admin\","")
				session("errorSQL") = path
				set file = fso.CreateTextFile(path)
				
				file.WriteLine saveText
				file.close()
				fso = null
				%>
            </table>
        </td>
    </tr>
    <tr><td align="right"><a href="../vendorOrders/<%=filename%>">view csv</a></td></tr>
    <% end if %>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function updateVendorPN(vendor,partNumber,vendorVal) {
		//document.getElementById("testZone").innerHTML = '/ajax/admin/saveVendorNum.asp?vendorReceive=1&vendorCode=' + vendor + '&ourPN=' + partNumber + '&addVPN=' + vendorVal
		ajax('/ajax/admin/saveVendorNum.asp?vendorReceive=1&vendorCode=' + vendor + '&ourPN=' + partNumber + '&addVPN=' + vendorVal,'dumpZone')
	}
	function processRow(rowNum) {
		var partNumber = eval("document.receiveForm_" + rowNum + ".partNumber_" + rowNum + ".value")
		var rcvAmt = eval("document.receiveForm_" + rowNum + ".receiveAmt_" + rowNum + ".value")
		if (isNaN(rcvAmt)) {
			alert("You must enter a number received")
		}
		else {
			document.getElementById("purchaseRow_" + rowNum).style.backgroundColor = "#666666"
			//document.getElementById("actionBttn_" + rowNum).innerHTML = '<span style="color:#FFF; font-weight:bold;">PROCESSED</span>'
			document.getElementById("enterReceiveAmt_" + rowNum).innerHTML = rcvAmt + '<input type="hidden" name="receiveAmt_' + rowNum + '" value="' + rcvAmt + '" />'
			ajax('/ajax/admin/ajaxVendorReceiveOrders.asp?saveRcvAmt=1&orderID=<%=orderID%>&ourPN=' + partNumber + '&rcvAmt=' + rcvAmt,'actionBttn_' + rowNum)
		}
	}
	function verifyProcess() {
		var yourstate=window.confirm("Are you sure the order is ready to process?")
		if (yourstate) {
			return true
		}
		else {
			return false
		}
	}
	
	function sortBy(which) {
		window.location = window.location + "&sortBy=" + which
	}
	
	function confirmZeroOut() {
		var yourstate=window.confirm("Are you sure you want to zero out the remaining items?")
		if (yourstate) {
			window.location=window.location+'&zeroOut=1'
		}
	}
	
	function saveDate(dbColumn,curDate) {
		ajax('/ajax/admin/ajaxVendorReceiveOrders.asp?orderID=<%=orderID%>&saveDateColumn=' + dbColumn,'dumpZone');
		document.getElementById(dbColumn).innerHTML = curDate;
	}
	
	function voNotes(orderID) {
		if (document.getElementById("voNotes").style.display == 'none') {
			ajax('/ajax/admin/ajaxVendorReceiveOrders.asp?voNotes=' + orderID,'voNotes');
			document.getElementById("voNotes").style.display = '';
		}
		else {
			document.getElementById("voNotes").style.display = 'none';
			document.getElementById("voNotes").innerHTML = '';
		}
	}
</script>