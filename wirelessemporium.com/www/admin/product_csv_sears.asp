<%
pageTitle = "Product List for Sears - WE"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
dim fs, file, filename, path
set fs = CreateObject("Scripting.FileSystemObject")
'Map the file name to the physical path on the server.
filename = "productList_sears_we.csv"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List upload for Sears</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							response.write("<b>DeleteFile:</b>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='../tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		response.write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	else
		response.write "<br>"
	end if
end sub

sub CreateFile(path)
	dim fs, file
	response.write("<b>CreateFile:</b>")
	set fs = CreateObject("Scripting.FileSystemObject")
	'If the file already exists give an error message.
	if fs.FileExists(path) then
		response.write("File " & filename & " already exists, create aborted.<br>" & vbCrLf)
	else
		response.write("Creating " & filename & ".<br>")
		set file = fs.CreateTextFile(path)
		dim SQL, RS, strline
		SQL	=	"	select	a.itemid, a.partnumber, a.itemdesc, a.itemlongdetail	" & vbcrlf & _
				"		,	a.price_our, a.itempic, isnull(a.upccode, '') upccode, a.inv_qty, t.typename, c.brandname	" & vbcrlf & _
				"	from	we_items a with (nolock) join we_types t with (nolock)	" & vbcrlf & _
				"		on	a.typeid = t.typeid join we_brands c with (nolock)" & vbcrlf & _
				"		on	a.brandid = c.brandid left outer join 	" & vbcrlf & _
				"			(	" & vbcrlf & _
				"			select	a.partnumber orphan_partnumber	" & vbcrlf & _
				"			from	(	" & vbcrlf & _
				"					select	partnumber	" & vbcrlf & _
				"						,	sum(case when inv_qty > 0 then 1 else 0 end) nMaster	" & vbcrlf & _
				"						,	sum(case when inv_qty < 0 then 1 else 0 end) nSlave	" & vbcrlf & _
				"					from	we_items a with (nolock) 	" & vbcrlf & _
				"					where	hidelive = 0 and inv_qty <> 0 and price_our > 0	" & vbcrlf & _
				"						and	typeid <> 16	" & vbcrlf & _
				"					group by partnumber	" & vbcrlf & _
				"					) a	" & vbcrlf & _
				"			where	a.nMaster = 0	" & vbcrlf & _
				"			) b	" & vbcrlf & _
				"		on	a.partnumber = b.orphan_partnumber	" & vbcrlf & _
				"	where	a.hidelive = 0 and a.inv_qty <> 0	" & vbcrlf & _
				"		and a.price_our > 0	" & vbcrlf & _
				"		and a.typeid <> 16	" & vbcrlf & _
				"		and	b.orphan_partnumber is null	" & vbcrlf

'		response.write "<pre>" & SQL & "</pre>"
'		response.end

		set RS = Server.CreateObject("ADODB.Recordset")
		dim queryTimeStart, queryTimeEnd
'		response.write "<pre>" & SQL & "</pre>"
		queryTimeStart = now
		RS.open SQL, oConn, 3, 1
		queryTimeEnd = now
		
		response.write "<p><b>Query</b> elapsed time:" & datediff("s", queryTimeStart, queryTimeEnd) & " second(s)<br>"

		if not RS.eof then
			fileWriteTimeStart = now
'			heading = 				"CATEGORY" & vbTab & "TITLE" & vbTab & "ITEM URL" & vbTab & "SKU" & vbTab
'			heading =	heading & 	"PRICE" & vbTab & "SALE PRICE" & vbTab & "SALE START DATE" & vbTab & "SALE END DATE" & vbTab
'			heading =	heading & 	"UPC" & vbTab & "IMAGE URL" & vbTab & "SHORT DESCRIPTION" & vbTab & "MANUFACTURER MODEL #" & vbTab
'			heading =	heading & 	"SHIPPING COST" & vbTab & "BRAND" & vbTab & "ACTION FLAG"			
'			file.WriteLine heading
			file.WriteLine "CATEGORY,TITLE,ITEM URL,SKU,PRICE,SALE PRICE,SALE START DATE,SALE END DATE,UPC,IMAGE URL,SHORT DESCRIPTION,MANUFACTURER MODEL #,SHIPPING COST,BRAND,ACTION FLAG"
			do until RS.eof
				if isnull(RS("itemDesc")) then
					itemDesc = "Universal"
				else
					itemDesc = RS("itemDesc")
				end if
				if isnull(RS("typeName")) then
					sTypeName = "Universal"
				else
					sTypeName = RS("typeName")
				end if
				if isnull(RS("BrandName")) then
					sBrandName = "Universal"
				else 
					sBrandName = RS("BrandName")
				end if				
				
				id_new_field 	= 	formatSEO(itemDesc)
'				strPrice 		= 	formatCurrency(RS("price_our"))
				strPrice 		= 	formatnumber(RS("price_our"),2)
				itemLongDetail	=	replace(RS("itemLongDetail"),vbcrlf," ")
				
				strline	=				chr(34) & sTypeName & chr(34) & ","
				strline	=	strline	&	chr(34) & itemDesc & chr(34) & ","
				strline = 	strline & 	chr(34) & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & id_new_field & ".asp?id=sears" & chr(34) & ","
				strline = 	strline & 	chr(34) & RS("itemid") & chr(34) & ","
				strline = 	strline & 	chr(34) & strPrice & chr(34) & ","
				strline = 	strline & 	chr(34) & chr(34) & ","
				strline = 	strline & 	chr(34) & chr(34) & ","
				strline = 	strline & 	chr(34) & chr(34) & ","
				strline = 	strline & 	chr(34) & RS("upccode") & chr(34) & ","
				strline = 	strline & 	chr(34) & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & chr(34) & ","
				strline = 	strline & 	chr(34) & itemLongDetail & chr(34) & ","
				strline = 	strline & 	chr(34) & RS("partnumber") & chr(34) & ","
				strline = 	strline & 	chr(34) & "0.00" & chr(34) & ","				
				strline = 	strline & 	chr(34) & sBrandName & chr(34) & ","
				strline = 	strline & 	chr(34) & chr(34)

				file.WriteLine strline
				RS.movenext
			loop
	
			fileWriteTimeEnd = now
			response.write "<b>FileWrite</b> elapsed time:" & datediff("n", fileWriteTimeStart, fileWriteTimeEnd) & "min, " & datediff("s", fileWriteTimeStart, fileWriteTimeEnd) mod 60 & " second(s)<p>"
		end if
		RS.close
		set RS = nothing
		file.Close()
	end if
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
