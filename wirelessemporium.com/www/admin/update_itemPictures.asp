<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Update Item Pictures"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/framework/utility/ftp.asp"-->
<%
useFTP = true
if useFTP then
	call initConnection(ftpWEB2,"192.168.112.161","ftpAdmin","GSF79h7yr9r3$C")	'WEB2
	call initConnection(ftpWEB3,"192.168.112.163","ftpAdmin","GSF79h7yr9r3$C")	'WEB3
end if
%>
<table border="0" cellpadding="0" cellspacing="0" width="600" align="center">
	<tr>
    	<td align="left" style="font-weight:bold; font-size:14px;">WE Part Image Duplicator</td>
    </tr>
    <tr>
    	<td align="left" style="font-size:12px;">
        	Enter the part number for the products you want to unify the images for. The items will then be listed out and the
            images for the master item will be displayed. By clicking the "Assign to all products" button the images displayed
            will be copied to all the products listed.
        </td>
    </tr>
    <tr>
    	<td style="padding-top:20px;">
<%
	set fs = CreateObject("Scripting.FileSystemObject")
	Set Jpeg = Server.CreateObject("Persits.Jpeg")

	partNumber = ds(request("partNumber"))
	picToSave = request.Form("picToSave")
	picSlot = ds(request.Form("picSlot"))
	site = ds(request.Form("site"))
	desc = ds(request.Form("desc"))
	itemDesc = ds(request.Form("itemDesc"))
	feature1 = ds(request.Form("feature1"))
	feature2 = ds(request.Form("feature2"))
	feature3 = ds(request.Form("feature3"))
	feature4 = ds(request.Form("feature4"))
	feature5 = ds(request.Form("feature5"))
	feature6 = ds(request.Form("feature6"))
	feature7 = ds(request.Form("feature7"))
	feature8 = ds(request.Form("feature8"))
	feature9 = ds(request.Form("feature9"))
	feature10 = ds(request.Form("feature10"))
	myAction = ds(request.Form("myAction"))
	price = ds(request.Form("price"))
	itemID = ds(request.Form("itemID"))
	dim lastItemID : lastItemID = prepInt(request.Form("lastItemID"))

	if isnull(partNumber) or len(partNumber) < 1 then partNumber = ""
	if isnull(picToSave) or len(picToSave) < 1 then picToSave = ""
	if isnull(picSlot) or len(picSlot) < 1 then picSlot = 0
	if isnull(site) or len(site) < 1 then site = ""
	if isnull(desc) or len(desc) < 1 then desc = ""
	if isnull(myAction) or len(myAction) < 1 then myAction = ""
	if isnull(price) or len(price) < 1 then price = 0
	if isnull(itemID) or len(itemID) < 1 then itemID = 0

	if myAction = "Copy Details" then
		sql = "select a.itemID, b.brandName, c.modelName, (select brandName from we_brands ia join we_items ib on ia.brandID = ib.brandID and ib.itemID = " & itemID & ") as masterBrandName, (select modelName from we_models ic join we_items id on ic.modelID = id.modelID and id.itemID = " & itemID & ") as masterModelName from we_items a join we_brands b on a.brandID = b.brandID join we_models c on a.modelID = c.modelID where a.partNumber = '" & partNumber & "'"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1

		lap = 0
		do while not rs.EOF
			useItemID = rs("itemID")
			useBrand = rs("brandName")
			useModel = rs("modelName")
			useMasterBrand = rs("masterBrandName")
			useMasterModel = rs("masterModelName")
			if useBrand = "Other" then useBrand = ""
			if instr(itemDesc,useMasterBrand & " " & useMasterModel) > 0 or itemDesc = "" then
				lap = lap + 1

				session("errorSQL") = "in the update"
				if itemDesc <> "" then
					session("errorSQL") = "replace brand/model names"
					useDesc = replace(desc,useMasterBrand & " " & useMasterModel,useBrand & " " & useModel)
					useItemDesc = replace(itemDesc,useMasterBrand & " " & useMasterModel,useBrand & " " & useModel)
					useFeature1 = replace(feature1,useMasterBrand & " " & useMasterModel,useBrand & " " & useModel)
					useFeature2 = replace(feature2,useMasterBrand & " " & useMasterModel,useBrand & " " & useModel)
					useFeature3 = replace(feature3,useMasterBrand & " " & useMasterModel,useBrand & " " & useModel)
					useFeature4 = replace(feature4,useMasterBrand & " " & useMasterModel,useBrand & " " & useModel)
					useFeature5 = replace(feature5,useMasterBrand & " " & useMasterModel,useBrand & " " & useModel)
					useFeature6 = replace(feature6,useMasterBrand & " " & useMasterModel,useBrand & " " & useModel)
					useFeature7 = replace(feature7,useMasterBrand & " " & useMasterModel,useBrand & " " & useModel)
					useFeature8 = replace(feature8,useMasterBrand & " " & useMasterModel,useBrand & " " & useModel)
					useFeature9 = replace(feature9,useMasterBrand & " " & useMasterModel,useBrand & " " & useModel)
					useFeature10 = replace(feature10,useMasterBrand & " " & useMasterModel,useBrand & " " & useModel)
				else
					session("errorSQL") = "bypass replace"
				end if

				if site = "we" then
					sql = "update we_items set itemLongDetail = '" & useDesc & "', itemDesc = '" & useItemDesc & "', bullet1 = '" & useFeature1 & "', bullet2 = '" & useFeature2 & "', bullet3 = '" & useFeature3 & "', bullet4 = '" & useFeature4 & "', bullet5 = '" & useFeature5 & "', bullet6 = '" & useFeature6 & "', bullet7 = '" & useFeature7 & "', bullet8 = '" & useFeature8 & "', bullet9 = '" & useFeature9 & "', bullet10 = '" & useFeature10 & "' where itemID = " & useItemID
				elseif site = "co" then
					sql = "update we_items set itemLongDetail_CO = '" & useDesc & "', itemDesc_CO = '" & useItemDesc & "', point1 = '" & useFeature1 & "', point2 = '" & useFeature2 & "', point3 = '" & useFeature3 & "', point4 = '" & useFeature4 & "', point5 = '" & useFeature5 & "', point6 = '" & useFeature6 & "', point7 = '" & useFeature7 & "', point8 = '" & useFeature8 & "', point9 = '" & useFeature9 & "', point10 = '" & useFeature10 & "' where itemID = " & useItemID
				elseif site = "ca" then
					sql = "update we_items set itemLongDetail_CA = '" & useDesc & "', itemDesc_CA = '" & useItemDesc & "' where itemID = " & useItemID
				elseif site = "ps" then
					sql = "update we_items set itemLongDetail_PS = '" & useDesc & "', itemDesc_PS = '" & useItemDesc & "', feature1 = '" & useFeature1 & "', feature2 = '" & useFeature2 & "', feature3 = '" & useFeature3 & "', feature4 = '" & useFeature4 & "', feature5 = '" & useFeature5 & "', feature6 = '" & useFeature6 & "', feature7 = '" & useFeature7 & "', feature8 = '" & useFeature8 & "', feature9 = '" & useFeature9 & "', feature10 = '" & useFeature10 & "' where itemID = " & useItemID
				end if
				session("errorSQL") = sql
				oConn.execute(sql)

				sql = "insert into we_adminActions (adminID,action) values(" & adminID & ",'Copy Master Pics/Desc/Price:" & useItemID & "')"
				session("errorSQL") = SQL
				oConn.execute SQL

				rs.movenext
			else
				response.Write("<strong>Master product name not found!</strong><br>")
				response.Write("<strong>Master Product:</strong> " & useMasterBrand & " " & useMasterModel & "<br>")
				response.Write("<strong>Product Title:</strong>" & itemDesc)
				response.End()
			end if
		loop
		response.Write("Details copied to products")
	elseif myAction = "Copy Price" then
		if site = "we" then
			sql = "update we_items set price_our = '" & price & "' where partNumber = '" & partNumber & "'"
		elseif site = "co" then
			sql = "update we_items set price_co = '" & price & "' where partNumber = '" & partNumber & "'"
		elseif site = "ca" then
			sql = "update we_items set price_ca = '" & price & "' where partNumber = '" & partNumber & "'"
		elseif site = "ps" then
			sql = "update we_items set price_ps = '" & price & "' where partNumber = '" & partNumber & "'"
		elseif site = "er" then
			sql = "update we_items set price_er = '" & price & "' where partNumber = '" & partNumber & "'"
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
		response.Write("Price copied to products")

		sql = "insert into we_adminActions (adminID,action) values(" & adminID & ",'update_itemPictures - Price - update we_Items:" & partNumber & "')"
		session("errorSQL") = SQL
		oConn.execute SQL
	end if

	if picToSave <> "" then
		sql = "select itemID, itemDesc, itemDesc_CO, itemPic, itemPic_CO from we_items where hideLive = 0 and partNumber like '%" & partNumber & "%' order by inv_qty desc, itemID"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 3, 3

		lap = 0
		updates = 0

'		if site = "CO" then
'			dim ftp, success, localFilename, remoteFilename
'			set ftp = CreateObject("Chilkat.Ftp2")
'			success = ftp.UnlockComponent("WIRELEFTP_GQ2TEoLE9Bnh")
'			if success <> 1 then
'				response.write "ftp.UnlockComponent ERROR - " & ftp.LastErrorText
'				response.end
'			end if
'
'			ftp.Hostname = "216.139.235.21"
'			ftp.Username = "coUpload"
'			ftp.Password = "co9513"
'
'			' Connect and login to the FTP server.
'			success = ftp.Connect()
'			if success <> 1 then
'				response.write "ftp connection error - " & ftp.LastErrorText
'				response.end
'			end if
'		end if

		path = server.mappath("\productpics") & "\"
		path_CO = server.mappath("\productpics_co") & "\"
		tempPath = server.mappath("\productpics") & "\tempImages\"
		tempCOPath = server.mappath("\productpics_co") & "\tempImages\"
		
		dim saveZoom : saveZoom = false
		if fs.FileExists(path & "big\zoom\" & picToSave) then saveZoom = true
		dim saveBig : saveBig = false
		if fs.FileExists(path & "big\" & picToSave) then saveBig = true
		dim saveThumb : saveThumb = false
		if fs.FileExists(path & "thumb\" & picToSave) then saveThumb = true
		dim keepGoing : keepGoing = false
		
		itemCnt = 0
		if lastItemID > 0 and not rs.EOF then
			rs.movenext
			do while rs("itemID") <= lastItemID
				rs.movenext
				itemCnt = itemCnt + 1
				if rs.EOF then exit do
			loop
			response.Write("<p style='font-weight:bold; color:#f00; font-size:12px;'>Fast Forward Complete<br>" & itemCnt & " Processed so far<br>Resume Processing...</p><br><br>")
			response.Flush()
		end if

		do while not rs.EOF
			lap = lap + 1
			itemCnt = itemCnt + 1
			'response.Write(itemCnt & " , ")
			'response.Flush()
			if picToSave <> rs("itemPic") and picToSave <> replace(rs("itemPic"),".","-" & picSlot & ".") then
				updates = updates + 1
				useItemID = prepStr(rs("itemID"))
				useItemDesc = prepStr(rs("itemDesc"))
				useItemDesc_CO = prepStr(rs("itemDesc_CO"))
				useItemPic = prepStr(rs("itemPic"))
				useItemPic_CO = prepStr(rs("itemPic_CO"))

				if site = "WE" then
					if useItemPic <> "" then
						useFileName = useItemPic
					else
						useFileName = formatSEO(useItemDesc) & ".jpg"
						sql = "update we_Items set itemPic = '" & useFileName & "' where itemID = " & useItemID
						oConn.execute(sql)
					end if

					session("errorSQL2") = picToSave
					session("errorSQL") = "Trying to save: " & useFileName
					on error resume next
'						Jpeg.Open server.MapPath("/productpics/big/" & picToSave)
'						Jpeg.Save server.MapPath("/productpics/big/" & useFileName)
'						Jpeg.Open server.MapPath("/productpics/thumb/" & picToSave)
'						Jpeg.Save server.MapPath("/productpics/thumb/" & useFileName)

						if saveZoom then
							SavePath = tempPath & "big\zoom\" & useFileName
							Jpeg.Open path & "big\zoom\" & picToSave
							Jpeg.Save SavePath
							call uploadImagesRemote("\productpics\big\zoom", SavePath, useFileName)
						end if
						if saveBig then
							SavePath = tempPath & "big\" & useFileName
							Jpeg.Open path & "big\" & picToSave
							Jpeg.Save SavePath
							call uploadImagesRemote("\productpics\big", SavePath, useFileName)
						end if
						if saveThumb then
							SavePath = tempPath & "thumb\" & useFileName
							Jpeg.Open path & "thumb\" & picToSave
							Jpeg.Save SavePath
							call uploadImagesRemote("\productpics\thumb", SavePath, useFileName)
						end if

'						errorLap = 0
'						do while err.number <> 0
'							errorLap = errorLap + 1
'							response.Write("<!-- ImgError:" & errorLap & " -->")
'							err.Clear
''							Jpeg.Open server.MapPath("/productpics/big/" & picToSave)
''							Jpeg.Save server.MapPath("/productpics/big/" & useFileName)
''							Jpeg.Open server.MapPath("/productpics/thumb/" & picToSave)
''							Jpeg.Save server.MapPath("/productpics/thumb/" & useFileName)
'
'							if errorLap = 200 then exit do
'						loop
					On Error Goto 0
				elseif site = "CO" then
					if useItemPic_CO <> "" then
						useFileName = useItemPic_CO
					else
						useFileName = formatSEO(useItemDesc_CO) & ".jpg"
						sql = "update we_Items set itemPic_CO = '" & useFileName & "' where itemID = " & useItemID
						oConn.execute(sql)
					end if

					session("errorSQL2") = "lap:" & lap

'					openPathBig 	= server.MapPath("/productpics_co/big/" & picToSave)
'					savePathBig		= server.MapPath("/productpics_co/big/" & useFileName)
'					openPathThumb 	= server.MapPath("/productpics_co/thumb/" & picToSave)
'					savePathThumb	= server.MapPath("/productpics_co/thumb/" & useFileName)
					if fs.FileExists(path_CO & "big\zoom\" & picToSave) then
						SavePath = tempCOPath & "big\zoom\" & useFileName
						Jpeg.Open path_CO & "big\zoom\" & picToSave
						Jpeg.Save SavePath
						call uploadImagesRemote("\productpics_co\big\zoom", SavePath, useFileName)
					end if
					if fs.FileExists(path_CO & "big\" & picToSave) then
						SavePath = tempCOPath & "big\" & useFileName
						Jpeg.Open path_CO & "big\" & picToSave
						Jpeg.Save SavePath
						call uploadImagesRemote("\productpics_co\big", SavePath, useFileName)
					end if
					if fs.FileExists(path_CO & "thumb\" & picToSave) then
						SavePath = tempCOPath & "thumb\" & useFileName
						Jpeg.Open path_CO & "thumb\" & picToSave
						Jpeg.Save SavePath
						call uploadImagesRemote("\productpics_co\thumb", SavePath, useFileName)
					end if

'					if openPathBig <> savePathBig then
'						if fs.FileExists(openPathBig) then
'							'on error resume next
'								Jpeg.Open openPathBig
'								Jpeg.Save savePathBig
'								errorLap = 0
'								do while err.number <> 0
'									errorLap = errorLap + 1
'									response.Write("<!-- ImgError:" & errorLap & " -->")
'									err.Clear
'									Jpeg.Open openPathBig
'									Jpeg.Save savePathBig
'									if errorLap = 200 then exit do
'								loop
'							'On Error Goto 0
'						else
'							response.Write("error finding large image file: " & openPathBig & "<br>")
'						end if
'
'						if fs.FileExists(openPathThumb) then
'							'on error resume next
'								Jpeg.Open openPathThumb
'								Jpeg.Save savePathThumb
'								do while err.number <> 0
'									errorLap = errorLap + 1
'									response.Write("<!-- ImgError:" & errorLap & " -->")
'									err.Clear
'									Jpeg.Open openPathThumb
'									Jpeg.Save savePathThumb
'									if errorLap = 200 then exit do
'								loop
'							'On Error Goto 0
'						else
'							response.Write("error finding small image file: " & openPathThumb & "<br>")
'						end if
'					end if
				elseif site = "Alt" then
					if useItemPic <> "" then
						useFileName = useItemPic
					else
						useFileName = formatSEO(useItemDesc) & ".jpg"
						sql = "update we_Items set itemPic = '" & useFileName & "' where itemID = " & useItemID
						oConn.execute(sql)
					end if

					session("errorSQL") = "/productpics/AltViews/" & picToSave
'					Jpeg.Open server.MapPath("/productpics/AltViews/" & picToSave)
'					Jpeg.Save server.MapPath("/productpics/AltViews/" & replace(useFileName,".","-" & picSlot & "."))
					if fs.FileExists(path & "AltViews\zoom\" & picToSave) then
						SavePath = tempPath & "AltViews\zoom\" & replace(useFileName,".","-" & picSlot & ".")
						Jpeg.Open path & "AltViews\zoom\" & picToSave
						Jpeg.Save SavePath
						call uploadImagesRemote("\productpics\AltViews\zoom", SavePath, replace(useFileName,".","-" & picSlot & "."))
					end if
					if fs.FileExists(path & "AltViews\" & picToSave) then
						SavePath = tempPath & "AltViews\" & replace(useFileName,".","-" & picSlot & ".")
						Jpeg.Open path & "AltViews\" & picToSave
						Jpeg.Save SavePath
						call uploadImagesRemote("\productpics\AltViews", SavePath, replace(useFileName,".","-" & picSlot & "."))
					end if
				end if
			end if
			rs.movenext
			if lap > 250 and not rs.EOF then
				keepGoing = true
				exit do
			end if
		loop
		
		if keepGoing then
%>
<form name="repeatActionForm" action="/admin/update_itemPictures.asp" method="post">
	<input type="hidden" name="partNumber" value="<%=request("partNumber")%>" />
    <input type="hidden" name="picToSave" value="<%=request("picToSave")%>" />
    <input type="hidden" name="picSlot" value="<%=request("picSlot")%>" />
    <input type="hidden" name="site" value="<%=request("site")%>" />
    <input type="hidden" name="desc" value="<%=request("desc")%>" />
    <input type="hidden" name="itemDesc" value="<%=request("itemDesc")%>" />
    <input type="hidden" name="feature1" value="<%=request("feature1")%>" />
    <input type="hidden" name="feature2" value="<%=request("feature2")%>" />
    <input type="hidden" name="feature3" value="<%=request("feature3")%>" />
    <input type="hidden" name="feature4" value="<%=request("feature4")%>" />
    <input type="hidden" name="feature5" value="<%=request("feature5")%>" />
    <input type="hidden" name="feature6" value="<%=request("feature6")%>" />
    <input type="hidden" name="feature7" value="<%=request("feature7")%>" />
    <input type="hidden" name="feature8" value="<%=request("feature8")%>" />
    <input type="hidden" name="feature9" value="<%=request("feature9")%>" />
    <input type="hidden" name="feature10" value="<%=request("feature10")%>" />
    <input type="hidden" name="myAction" value="<%=request("myAction")%>" />
    <input type="hidden" name="price" value="<%=request("price")%>" />
    <input type="hidden" name="itemID" value="<%=request("itemID")%>" />
    <input type="hidden" name="lastItemID" value="<%=useItemID%>" />
</form>
<script>document.repeatActionForm.submit();</script>
<%
			response.End()
		end if

'		if site = "CO" then
'			ftp.Disconnect
'			set ftp = nothing
'		end if

		response.Write("Process Complete<br>" & itemCnt & " pictures processed")
		savePN = partNumber
	end if

	if partNumber <> "" then
		sql = "select distinct partNumber from we_items where partNumber like '%" & partNumber & "%'"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 3, 3

		if rs.EOF then
			userMsg = "ERROR: No part numbers found with the criteria '" & partNumber & "'"
			partNumber = ""
		else
			if rs.recordCount > 1 then
%>
<table border="0" cellpadding="3" cellspacing="0" align="center" style="padding-top:10px; padding-bottom:10px;">
	<tr>
    	<td colspan="3" align="center" style="font-weight:bold; color:#F00">
        	Multiple part numbers found<br />
			Please select the part number you would like to edit
        </td>
    </tr>
    <%
	do while not rs.EOF
	%>
    <tr>
    	<%
		for i = 0 to 2
			if rs.EOF then
		%>
        <td>&nbsp;</td>
        <%
			else
		%>
    	<td align="left"><a href="/admin/update_itemPictures.asp?partNumber=<%=rs("partNumber")%>"><%=rs("partNumber")%></a></td>
        <%
				rs.movenext
			end if
		next
		%>
    </tr>
    <%
	loop
	%>
</table>
<%
			else
				sql = "select * from we_items where hideLive = 0 and partNumber like '%" & partNumber & "%' and master = 1 order by inv_qty desc"
				session("errorSQL") = sql
				Set rs = Server.CreateObject("ADODB.Recordset")
				rs.open sql, oConn, 3, 3

				if rs.EOF then
					userMsg = "No items found with that part number"
					partNumber = ""
				elseif rs("inv_qty") < 1 then
					userMsg = "No master product found - enter inventory qty"
					partNumber = ""
				else
%>
<table border="0" cellpadding="3" cellspacing="0" align="center" style="padding-top:10px; padding-bottom:10px;">
	<tr><td colspan="3" align="center"><a href="/admin/update_itemPictures.asp" style="font-size:14px; color:#F00">Exit and enter new part number</a></td></tr>
    <tr>
    	<td colspan="3" align="center" style="font-weight:bold; color:#F00">
        	Part number <%=partNumber%> found<br />
			<%=rs.recordCount%> Items found with this part number
        </td>
    </tr>
    <tr>
    	<td colspan="3" align="center">
        	<form action="/admin/update_itemPictures.asp" method="post" name="saveForm">
            <table border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" style="border:1px solid #000;">
			        	<%=rs("itemID")%><br />
                        WE/CA Pic<br />
			        	<img src="/productpics/big/<%=rs("itemPic")%>" width="100" /><br />
            			<input type="button" name="myAction" value="Assign to all products" onclick="startProcess('<%=rs("itemPic")%>',1)" />
                        <input type="hidden" name="partNumber" value="<%=partNumber%>" />
                        <input type="hidden" name="picToSave" value="" />
                        <input type="hidden" name="picSlot" value="" />
                        <input type="hidden" name="site" value="" />
                    </td>
                    <td align="center" style="border:1px solid #000;">
			        	<%=rs("itemID")%><br />
                        CO Pic<br />
			        	<img src="http://www.cellularoutfitter.com/productpics/big/<%=rs("itemPic_CO")%>" width="100" /><br />
            			<input type="button" name="myAction" value="Assign to all products" onclick="startCOProcess('<%=rs("itemPic_CO")%>',1)" />
                    </td>
                </tr>
                <tr>
                    <%
					for iCount = 0 to 7
						strAltImage = ""
						itempic = rs("itemPic")
						path = server.MapPath("/productpics/AltViews/" & replace(itempic,".","-" & iCount & "."))
						usePicName = replace(itempic,".","-" & iCount & ".")
						for imgType = 0 to 2
							if imgType = 0 then curPath = path
							if imgType = 1 then curPath = replace(path,".jpg",".gif")
							if imgType = 2 then curPath = replace(path,".gif",".jpg")
							if fs.FileExists(path) then
								imgName = replace(itempic,".","-" & iCount & ".")
								strAltImage = "<img border='0' src='/productPics/AltViews/" & usePicName & "' width='100'>"
							end if
						next
						if strAltImage <> "" then
					%>
                    <td align="center" style="border:1px solid #000;">
			        	<%=rs("itemID")%><br />
                        Alt Pic <%=(iCount + 1)%><br />
			        	<%=strAltImage%><br />
            			<input type="button" name="myAction" value="Assign to all products" onclick="startAltProcess('<%=imgName%>',<%=iCount%>)" />
                    </td>
                    <%
						end if
					next
					%>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr>
    	<td colspan="3" align="center">
        	<form action="/admin/update_itemPictures.asp" method="post">
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="80%">
            	<tr>
                	<td valign="top" nowrap="nowrap" style="font-weight:bold;">WE Item Description:</td>
                    <td width="100%" style="font-size:10px;"><%=rs("itemLongDetail")%></td>
                    <td valign="top">
                    	<input type="hidden" name="site" value="we" />
                        <input type="hidden" name="partnumber" value="<%=partNumber%>" />
                        <input type="hidden" name="itemID" value="<%=rs("itemID")%>" />
                        <input type="hidden" name="desc" value="<%=replace(rs("itemLongDetail"),"""","&quot;")%>" />
                        <input type="hidden" name="itemDesc" value="<%=replace(rs("itemDesc"),"""","&quot;")%>" />
                        <input type="hidden" name="feature1" value="<%=replace(nullIf(rs("bullet1"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature2" value="<%=replace(nullIf(rs("bullet2"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature3" value="<%=replace(nullIf(rs("bullet3"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature4" value="<%=replace(nullIf(rs("bullet4"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature5" value="<%=replace(nullIf(rs("bullet5"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature6" value="<%=replace(nullIf(rs("bullet6"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature7" value="<%=replace(nullIf(rs("bullet7"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature8" value="<%=replace(nullIf(rs("bullet8"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature9" value="<%=replace(nullIf(rs("bullet9"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature10" value="<%=replace(nullIf(rs("bullet10"), ""),"""","&quot;")%>" />
                        <input type="submit" name="myAction" value="Copy Details" />
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr>
    	<td colspan="3" align="center">
        	<form action="/admin/update_itemPictures.asp" method="post">
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="80%">
            	<tr>
                	<td valign="top" nowrap="nowrap" style="font-weight:bold;">CO Item Description:</td>
                    <td width="100%" style="font-size:10px;"><%=rs("itemLongDetail_CO")%></td>
                    <td valign="top">
                    	<input type="hidden" name="site" value="co" />
                        <input type="hidden" name="partnumber" value="<%=partNumber%>" />
                        <input type="hidden" name="itemID" value="<%=rs("itemID")%>" />
                    	<input type="hidden" name="desc" value="<%=replace(rs("itemLongDetail_CO"),"""","&quot;")%>" />
                        <input type="hidden" name="itemDesc" value="<%=replace(rs("itemDesc_CO"),"""","&quot;")%>" />
                        <input type="hidden" name="feature1" value="<%=replace(nullIf(rs("point1"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature2" value="<%=replace(nullIf(rs("point2"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature3" value="<%=replace(nullIf(rs("point3"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature4" value="<%=replace(nullIf(rs("point4"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature5" value="<%=replace(nullIf(rs("point5"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature6" value="<%=replace(nullIf(rs("point6"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature7" value="<%=replace(nullIf(rs("point7"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature8" value="<%=replace(nullIf(rs("point8"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature9" value="<%=replace(nullIf(rs("point9"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature10" value="<%=replace(nullIf(rs("point10"), ""),"""","&quot;")%>" />
                        <input type="submit" name="myAction" value="Copy Details" />
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr>
    	<td colspan="3" align="center">
        	<form action="/admin/update_itemPictures.asp" method="post">
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="80%">
            	<tr>
                	<td valign="top" nowrap="nowrap" style="font-weight:bold;">CA Item Description:</td>
                    <td width="100%" style="font-size:10px;"><%=rs("itemLongDetail_CA")%></td>
                    <td valign="top">
                    	<input type="hidden" name="site" value="ca" />
                        <input type="hidden" name="partnumber" value="<%=partNumber%>" />
                        <input type="hidden" name="itemID" value="<%=rs("itemID")%>" />
                    	<input type="hidden" name="desc" value="<%=replace(rs("itemLongDetail_CA"),"""","&quot;")%>" />
                        <input type="hidden" name="itemDesc" value="<%=replace(rs("itemDesc_CA"),"""","&quot;")%>" />
                        <input type="submit" name="myAction" value="Copy Details" />
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr>
    	<td colspan="3" align="center">
        	<form action="/admin/update_itemPictures.asp" method="post">
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="80%">
            	<tr>
                	<td valign="top" nowrap="nowrap" style="font-weight:bold;">PS Item Description:</td>
                    <td width="100%" style="font-size:10px;"><%=rs("itemLongDetail_PS")%></td>
                    <td valign="top">
                    	<input type="hidden" name="site" value="ps" />
                        <input type="hidden" name="partnumber" value="<%=partNumber%>" />
                        <input type="hidden" name="itemID" value="<%=rs("itemID")%>" />
                    	<input type="hidden" name="desc" value="<%=replace(rs("itemLongDetail_PS"),"""","&quot;")%>" />
                        <input type="hidden" name="itemDesc" value="<%=replace(rs("itemDesc_PS"),"""","&quot;")%>" />
                        <input type="hidden" name="feature1" value="<%=replace(nullIf(rs("feature1"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature2" value="<%=replace(nullIf(rs("feature2"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature3" value="<%=replace(nullIf(rs("feature3"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature4" value="<%=replace(nullIf(rs("feature4"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature5" value="<%=replace(nullIf(rs("feature5"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature6" value="<%=replace(nullIf(rs("feature6"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature7" value="<%=replace(nullIf(rs("feature7"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature8" value="<%=replace(nullIf(rs("feature8"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature9" value="<%=replace(nullIf(rs("feature9"), ""),"""","&quot;")%>" />
                        <input type="hidden" name="feature10" value="<%=replace(nullIf(rs("feature10"), ""),"""","&quot;")%>" />
                        <input type="submit" name="myAction" value="Copy Details" />
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr>
    	<td colspan="3" align="center">
        	<form action="/admin/update_itemPictures.asp" method="post">
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="50%">
            	<tr>
                	<td valign="top" nowrap="nowrap" style="font-weight:bold;">WE Item Price:</td>
                    <td width="100%"><%=rs("price_our")%></td>
                    <td valign="top">
                    	<input type="hidden" name="site" value="we" />
                        <input type="hidden" name="partnumber" value="<%=partNumber%>" />
                    	<input type="hidden" name="price" value="<%=rs("price_our")%>" />
                        <input type="submit" name="myAction" value="Copy Price" />
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr>
    	<td colspan="3" align="center">
        	<form action="/admin/update_itemPictures.asp" method="post">
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="50%">
            	<tr>
                	<td valign="top" nowrap="nowrap" style="font-weight:bold;">CO Item Price:</td>
                    <td width="100%"><%=rs("price_co")%></td>
                    <td valign="top">
                    	<input type="hidden" name="site" value="co" />
                        <input type="hidden" name="partnumber" value="<%=partNumber%>" />
                    	<input type="hidden" name="price" value="<%=rs("price_co")%>" />
                        <input type="submit" name="myAction" value="Copy Price" />
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr>
    	<td colspan="3" align="center">
        	<form action="/admin/update_itemPictures.asp" method="post">
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="50%">
            	<tr>
                	<td valign="top" nowrap="nowrap" style="font-weight:bold;">CA Item Price:</td>
                    <td width="100%"><%=rs("price_ca")%></td>
                    <td valign="top">
                    	<input type="hidden" name="site" value="ca" />
                        <input type="hidden" name="partnumber" value="<%=partNumber%>" />
                    	<input type="hidden" name="price" value="<%=rs("price_ca")%>" />
                        <input type="submit" name="myAction" value="Copy Price" />
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr>
    	<td colspan="3" align="center">
        	<form action="/admin/update_itemPictures.asp" method="post">
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="50%">
            	<tr>
                	<td valign="top" nowrap="nowrap" style="font-weight:bold;">PS Item Price:</td>
                    <td width="100%"><%=rs("price_PS")%></td>
                    <td valign="top">
                    	<input type="hidden" name="site" value="ps" />
                        <input type="hidden" name="partnumber" value="<%=partNumber%>" />
                    	<input type="hidden" name="price" value="<%=rs("price_PS")%>" />
                        <input type="submit" name="myAction" value="Copy Price" />
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr>
    	<td colspan="3" align="center">
        	<form action="/admin/update_itemPictures.asp" method="post">
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" width="50%">
            	<tr>
                	<td valign="top" nowrap="nowrap" style="font-weight:bold;">ER Item Price:</td>
                    <td width="100%"><%=rs("price_ER")%></td>
                    <td valign="top">
                    	<input type="hidden" name="site" value="er" />
                        <input type="hidden" name="partnumber" value="<%=partNumber%>" />
                    	<input type="hidden" name="price" value="<%=rs("price_ER")%>" />
                        <input type="submit" name="myAction" value="Copy Price" />
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr><td colspan="3" align="center"><a href="/admin/update_itemPictures.asp" style="font-size:14px; color:#F00">Exit and enter new part number</a></td></tr>
	<tr><td colspan="3" align="left" style="padding-top:10px; border-bottom:1px solid #000;">ItemIDs using this part number</td></tr>
	<%
	rs.movenext
	lap = 0
	do while not rs.EOF
		lap = lap + 1
	%>
    <tr>
    	<td align="right"><a href="/p-<%=rs("itemID")%>-<%=formatSEO(rs("itemDesc"))%>.asp" target="_blank"><%=rs("itemID")%></a></td>
        <td align="left" width="100%"><a href="/p-<%=rs("itemID")%>-<%=formatSEO(rs("itemDesc"))%>.asp" target="weLink"><%=rs("itemDesc")%></a></td>
        <% if not isnull(rs("price_CO")) and rs("price_CO") > 0 then %><td align="left" width="100%"><a href="http://www.cellularoutfitter.com/p-<%=rs("itemID")%>-<%=formatSEO(rs("itemDesc_CO"))%>.html" target="coLink">CO Link</a></td><% end if %>
    </tr>
    <%
		rs.movenext
	loop
	%>
</table>
<%
				end if
			end if
		end if
	end if

	if partNumber = "" then
%>
<form action="/admin/update_itemPictures.asp" method="post">
<table border="0" cellpadding="3" cellspacing="0" align="center" style="padding-top:10px; padding-bottom:10px;">
	<% if userMsg <> "" then %>
    <tr>
    	<td colspan="3" align="center" style="font-weight:bold; color:#F00"><%=userMsg%></td>
    </tr>
    <% end if %>
    <tr>
    	<td style="font-weight:bold; font-size:12px;">Enter the Part Number:</td>
        <td><input type="text" name="partNumber" value="<%=savePN%>" /></td>
        <td><input type="submit" name="myAction" value="Find Part Number" style="font-size:12px;" /></td>
    </tr>
</table>
</form>
<%
	end if
%>
		</td>
    </tr>
</table>
<%
function nullIf(val,val2)
	ret = val
	if isnull(val) then ret = val2

	nullIf = ret
end function

sub uploadImagesRemote(remoteDir, localFilePath, remoteFileName)
	if useFTP then
		success = ftpWEB2.ChangeRemoteDir(remoteDir)
		success = ftpWEB2.PutFile(localFilePath, remoteFileName)

		success = ftpWEB3.ChangeRemoteDir(remoteDir)
		success = ftpWEB3.PutFile(localFilePath, remoteFileName)
	end if
end sub
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function startProcess(val,slot) {
		document.saveForm.picToSave.value = val
		document.saveForm.picSlot.value = slot
		document.saveForm.site.value = "WE"
		document.saveForm.submit()
	}
	function startAltProcess(val,slot) {
		document.saveForm.picToSave.value = val
		document.saveForm.picSlot.value = slot
		document.saveForm.site.value = "Alt"
		document.saveForm.submit()
	}
	function startCOProcess(val,slot) {
		document.saveForm.picToSave.value = val
		document.saveForm.picSlot.value = slot
		document.saveForm.site.value = "CO"
		document.saveForm.submit()
	}
</script>