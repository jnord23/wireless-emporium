<%
pageTitle = "Update Password"
header = 1
skipLogin = 1
securePage = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim pass1 : pass1 = prepStr(request.Form("pass1"))
	dim pass2 : pass2 = prepStr(request.Form("pass2"))
	dim alertMsg : alertMsg = ""
	
	if pass1 <> "" and adminID > 0 then
		if pass1 = pass2 and len(pass1) > 6 then
			sql = "select password from we_adminUsers where adminID = " & adminID
			session("errorSQL") = sql
			set passChkRS = oConn.execute(sql)
			
			if not passChkRS.EOF then
				if passChkRS("password") <> pass1 then
					sql = "update we_adminUsers set lastPassword = password, passwordDate = getdate(), password = '" & pass1 & "' where adminID = " & adminID
					session("errorSQL") = sql
					oConn.execute(sql)
					
					response.Redirect("/admin/menu.asp")
				else
					alertMsg = "Your new password matches your current<br />Please select a new password"
				end if
			end if
		end if
	end if
%>
<table width="752" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
    	<td align="center">
        	<form name="newPassForm" action="/admin/updatePassword.asp" method="post" onsubmit="return(testPass())">
            <div style="width:300px; height:150px; text-align:center;">
            	<div style="text-align:center; font-size:18px; font-weight:bold;">
                	YOUR PASSWORD HAS EXPIRED<br />
                    PLEASE ENTER A NEW PASSWORD<br />
                    <span style="font-size:9px; text-align:left;">
                    	Password Criteria:
                        <ul>
                        	<li>Must be at least 7 characters long</li>
                            <li>Must have at least one capital letter</li>
                            <li>Must have at least one special character (!@#$%^&amp;*?)</li>
                            <li>Can't be the same as your current password</li>
                        </ul>
                    </span>
                </div>
                <% if alertMsg <> "" then %>
                <div style="padding-top:10px; display:table; color:#930; font-weight:bold; width:100%;"><%=alertMsg%></div>
                <% end if %>
                <div style="padding-top:10px; display:table;">
                	<div style="float:left; font-weight:bold; width:120px; text-align:right;">New Password:</div>
                    <div style="float:left; margin-left:10px;"><input type="password" name="pass1" value="" autocomplete="off" /></div>
                </div>
                <div style="padding-top:10px; display:table;">
                	<div style="float:left; font-weight:bold; width:120px; text-align:right;">Retype Password:</div>
                    <div style="float:left; margin-left:10px;"><input type="password" name="pass2" value="" autocomplete="off" /></div>
                </div>
                <div style="padding-top:10px;"><input type="submit" name="mySub" value="Update Password" /></div>
            </div>
            </form>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	document.newPassForm.pass1.focus();
	var pass1;
	var pass2;
	var passCriteria;
	var re;
	var keepTesting = 1;
	function testPass() {
		keepTesting = 1;
		pass1 = document.newPassForm.pass1.value;
		pass2 = document.newPassForm.pass2.value;
		
		passCriteria = "^.*(?=.{7,}).*$";
		re = new RegExp(passCriteria);
		regExpTest("Invalid Password: must be a least 7 characters long")
		
		if (keepTesting == 1) {
			passCriteria = "^.*(?=.*[A-Z]).*$";
			re = new RegExp(passCriteria);
			regExpTest("Invalid Password: must contain a capital letter")
		}
		
		if (keepTesting == 1) {
			passCriteria = "^.*(?=.*[!@#$%^&*?]).*$";
			re = new RegExp(passCriteria);
			regExpTest("Invalid Password: must contain a special character (!@#$%^&*?)")
		}
		
		if (keepTesting == 1) {
			if (pass1 == pass2) {
				return true;
			}
			else {
				alert("The re-entered password must match");
				document.newPassForm.pass2.value = "";
				document.newPassForm.pass2.focus();
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	function regExpTest(alertMsg) {
		if (pass1.match(re)) {
			keepTesting = 1;
		} else {
			keepTesting = 0;
			document.newPassForm.pass2.value = "";
			document.newPassForm.pass1.select();
			alert(alertMsg);
		}
	}
</script>