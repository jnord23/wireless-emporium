<%
dim thisUser
pageTitle = "WE Employee Page"
header = 1
thisUser = lcase(Request.Cookies("username"))
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<h3 align="center">WE Employee Page</h3>
<table width="752" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="right" valign="top" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="100%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="50%" valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="376" valign="top" class="normalText">
									<p>
										<strong><br>Info Pages:</strong>
										<ol style="margin-top:2px;">
											<li><a href="/admin/holidays.asp">WE Holidays</a></li>
											<li><a href="/admin/db_update_top_15_phone_models.asp">Top Models To Push</a></li>
										</ol>
									</p>
								</td>
							</tr>
							<tr>
								<td bgcolor="#E95516" height="2"><img src="/images/spacer.gif" width="2" height="2" border="0"></td>
							</tr>
							<tr>
								<td width="375" valign="top" class="normalText">
									<p>
										<strong><br>Task Lists:</strong>
										<ol style="margin-top:2px;">
                                        	<li><a href="/admin/shovelReady.asp">Shovel Ready</a></li>
											<li><a href="/admin/website_tasks_view.asp">Programming / Graphics</a></li>
										</ol>
									</p>
								</td>
							</tr>
						</table>
					</td>
					<td width="2" bgcolor="#E95516" rowspan="3"><img src="/images/spacer.gif" width="2" height="2" border="0"></td>
					<td width="375" valign="top" class="normalText" rowspan="3">
						<blockquote>
							<p>
								<strong><br>Downloads:</strong>
								<ol style="margin-top:2px;">
									<li><a href="/admin/WE%20Phone%20List.doc">WE Phone List</a></li>
								</ol>
							</p>
						</blockquote>
					</td>
				</tr>
				
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
