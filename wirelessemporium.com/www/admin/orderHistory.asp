<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - "
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	if day(date) < 7 then
		useDate = (month(date) - 1) & "/1/" & year(date)
	else
		useDate = month(date) & "/1/" & year(date)
	end if
	sql = "select count(*) as totalOrders, store, convert(varchar(10),orderdatetime,101) as orderDate from we_orders where orderdatetime >= '" & useDate & "' and approved = 1 group by convert(varchar(10),orderdatetime,101), store order by cast(convert(varchar(10),orderdatetime,101) as datetime) desc, store"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 3, 3
%>
<table align="center" border="1" bordercolor="#000000" cellpadding="3" cellspacing="0">
	<tr style="background-color:#333; color:#FFF; font-weight:bold;">
    	<td align="left">Date</td>
        <td align="left">WE</td>
        <td align="left">CA</td>
        <td align="left">CO</td>
        <td align="left">PS</td>
        <td align="left">ER</td>
    </tr>
    <%
	totalWE = 0
	totalCA = 0
	totalCO = 0
	totalPS = 0
	totalER = 0
	laps = 0
	bgColor = "#ffffff"
	do while not rs.EOF
		curDate = rs("orderDate")
		weOrders = 0
		caOrders = 0
		coOrders = 0
		psOrders = 0
		erOrders = 0
		do while curDate = rs("orderDate")
			if rs("store") = 0 then weOrders = rs("totalOrders")
			if rs("store") = 1 then caOrders = rs("totalOrders")
			if rs("store") = 2 then coOrders = rs("totalOrders")
			if rs("store") = 3 then psOrders = rs("totalOrders")
			if rs("store") = 10 then erOrders = rs("totalOrders")
			rs.movenext
			if rs.EOF then exit do
		loop
		totalWE = totalWE + weOrders
		totalCA = totalCA + caOrders
		totalCO = totalCO + coOrders
		totalPS = totalPS + psOrders
		totalER = totalER + erOrders
		laps = laps + 1
	%>
    <tr bgcolor="<%=bgColor%>">
    	<td><%=curDate%></td>
        <% if weOrders = 0 then %>
        <td align="right" style="color:#F00;">0</td>
        <% else %>
        <td align="right"><%=weOrders%></td>
        <% end if %>
        <% if caOrders = 0 then %>
        <td align="right" style="color:#F00;">0</td>
        <% else %>
        <td align="right"><%=caOrders%></td>
        <% end if %>
        <% if coOrders = 0 then %>
        <td align="right" style="color:#F00;">0</td>
        <% else %>
        <td align="right"><%=coOrders%></td>
        <% end if %>
        <% if psOrders = 0 then %>
        <td align="right" style="color:#F00;">0</td>
        <% else %>
        <td align="right"><%=psOrders%></td>
        <% end if %>
        <% if erOrders = 0 then %>
        <td align="right" style="color:#F00;">0</td>
        <% else %>
        <td align="right"><%=erOrders%></td>
        <% end if %>
    </tr>        
    <%
		if bgColor = "#ffffff" then bgColor = "#f0f0f0" else bgColor = "#ffffff"
	loop
	%>
    <tr style="background-color:#333; font-weight:bold; color:#FFF;">
    	<td align="right">Totals</td>
        <td align="right"><%=formatnumber(totalWE,0)%></td>
        <td align="right"><%=formatnumber(totalCA,0)%></td>
        <td align="right"><%=formatnumber(totalCO,0)%></td>
        <td align="right"><%=formatnumber(totalPS,0)%></td>
        <td align="right"><%=formatnumber(totalER,0)%></td>
    </tr>
    <tr style="background-color:#666; font-weight:bold; color:#FFF;">
    	<td align="right">Averages</td>
        <td align="right"><%=formatnumber(totalWE / laps,0)%></td>
        <td align="right"><%=formatnumber(totalCA / laps,0)%></td>
        <td align="right"><%=formatnumber(totalCO / laps,0)%></td>
        <td align="right"><%=formatnumber(totalPS / laps,0)%></td>
        <td align="right"><%=formatnumber(totalER / laps,0)%></td>
    </tr>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
