<%
'option explicit
response.buffer = false
pageTitle = "Upload DecalSkin Items"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
server.scripttimeout = 1000 'seconds
dim Path, Upload, Count
Path = server.mappath("tempTXT")
set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
Count = Upload.Save(Path)
if Upload.form("upload") <> "" then
	if Count = 0 then
		response.write "No file selected."
		response.end
	else
		dim modelID, modelName, brandID, brandName, File, myFile
		modelID = Upload.form("modelID")
		if modelID = "" or not isNumeric(modelID) then
			response.write "<h3>Model ID is required.</h3>"
			response.end
		else
			SQL = "SELECT A.modelName, B.brandID, B.brandName FROM we_Models A INNER JOIN we_Brands B ON A.brandID = B. brandID WHERE A.modelID = '" & modelID & "'"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			if RS.eof then
				response.write "<h3>Model ID not found.</h3>"
				response.end
			else
				modelName = RS("modelName")
				brandName = RS("brandName")
				brandID = RS("brandID")
			end if
		end if
		set File = Upload.Files(1)
		myFile = File.path
		
		dim txtConn, myStr
		set txtConn = Server.CreateObject("ADODB.Connection")
		myStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & myFile & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"";"
		txtConn.ConnectionString = myStr
		txtConn.ConnectionTimeout = 1000		'seconds
		txtConn.Open
		'response.write(myFile)
		
		dim SQL, RS
		SQL = "SELECT * FROM [Sheet1$] WHERE [product_sku] <> '' ORDER BY [product_sku]"
		response.write "<h3>" & SQL & "</h3>" & vbCrLf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, txtConn, 3, 3
		
		if RS.eof then
			response.write "No records matched<br><br>So cannot make table..."
		else
			dim PartNumber, strPartNumbers, strNewPartNumbers, updateCount, deleteCount
			strPartNumbers = ""
			strNewPartNumbers = ""
			updateCount = 0
			deleteCount = 0
			
			dim price_Our, price_CA, price_CO, price_PS, price_ER, price_Buy, COGS, price_Retail
			if modelID = "940" then
				price_Our = "21.99"
				price_CA = "19.99"
				price_CO = "19"
				price_PS = "21.99"
				price_ER = "21.99"
				price_Buy = "20.99"
				COGS = "14.50"
				price_Retail = RS("product_price") + 2
			else
				price_Our = "11.99"
				price_CA = "9.99"
				price_CO = "9"
				price_PS = "11.99"
				price_ER = "null"
				price_Buy = "9.99"
				COGS = "5.75"
				price_Retail = RS("product_price") + 6
			end if
			
			dim inv_qty, flag1, UPCCode, AMZN_sku
			inv_qty = 1000
			flag1 = 100
			
			dim itemLongDetail, itemLongDetail_PS, itemLongDetail_CA, itemLongDetail_CO
			itemLongDetail = "Forget about those hard cases, thick epoxy or silicon skins that bulk up your devices and making them look fat and hard to hold. DecalSkin covers the front and back of your " & brandName & " " & modelName & " and does not add any volume or weight, so you can keep it original with a great new look! Made with RapidAir technology ensures bubble-free application and stress-free removal. Once applied, DecalSkin covers up old ugly scratches and protects the device surface from occasional nicks and scratches. DecalSkin is not a paper sticker, it leaves no harmful residue when removed, so you may change your skin as often as you change your clothes."
			itemLongDetail_PS = "Not satisfied with your current cell phone case and looking for something with a little more personality? DecalSkins are available for your " & brandName & " " & modelName & " and are available in a great number of modern and different designs for you to choose from. These incredibly lightweight skins differ from traditional cases in that they stick directly to the large surfaces of your device, maintaining your " & brandName & " " & modelName & "'s original feel while transforming its look."
			itemLongDetail_CA = "Say goodbye to the days of bulky cases in boring colors. Now with DecalSkins, you can choose from a myriad of designs to simultaneously protect your device and make a style statement. These high gloss, no-fading skins are designed specifically to cover the main surfaces of your " & brandName & " " & modelName & ". Made in the US of high quality materials and a cutting-edge process, this skin will easily let you express your cell."
			itemLongDetail_CO = "If you're looking for a truly lightweight solution to protecting your " & brandName & " " & modelName & " while adding a touch of customization to your phone, then this skin is for you! These skins weigh practically nothing and don't add bulk to your device like hard or even silicone cases do. Cover up damage or give your " & brandName & " " & modelName & " a new look with a DecalSkin. So affordable and easy to install and remove, you can buy more than one and change it as often as you want."
			dim BULLET1, BULLET2, BULLET3, BULLET4, BULLET5, BULLET6, BULLET7, BULLET8, BULLET9, BULLET10, COMPATIBILITY
			BULLET1 = "Eye popping designs processed with UV resistant inks guarantees no fading!"
			BULLET2 = "Premium grade vinyl with air release channels ensures each application is easy and bubble free!"
			BULLET3 = "Coated with ultra high glossy laminated film for the ultimate durability."
			BULLET4 = "DecalSkin is removable but not reusable."
			COMPATIBILITY = brandName & " " & modelName
			dim POINT1, POINT2, POINT3, POINT4, POINT5, POINT6, POINT7, POINT8, POINT9, POINT10
			POINT1 = "Plenty of designs to choose from"
			POINT2 = "Unique process that ensures no fading"
			POINT3 = "Though DecalSkins are easily removable, they aren't reusable"
			dim FEATURE1, FEATURE2, FEATURE3, FEATURE4, FEATURE5, FEATURE6, FEATURE7, FEATURE8, FEATURE9, FEATURE10
			FEATURE1 = "Easily remove DecalSkins and put on another!"
			FEATURE2 = "New designs are constantly being added"
			FEATURE3 = "These skins are durable and won't fade"
			
			do until RS.eof
				PartNumber = "DEC-SKN-" & RS("product_sku")
				strPartNumbers = strPartNumbers & "'" & PartNumber & "',"
				dim itemDesc, itemDesc_PS, itemDesc_CA, itemDesc_CO
				itemDesc = replace(replace(RS("product_name"),vbtab,""),chr(9),"")
				itemDesc_PS = replace(itemDesc,"skin - ","Skin: ",1,9,1)
				itemDesc_CA = replace(itemDesc,"skin - ","Decal - ",1,9,1)
				itemDesc_CO = replace(itemDesc,"skin - ","Decal Skin: ",1,9,1)
				dim productImage, productImage_CO
				productImage = RS("product_full_image")
				productImage_CO = replace(productImage,"_skin_","_decal_skin_")
				
				dim RStemp
				SQL = "SELECT itemID FROM we_Items WHERE PartNumber = '" & PartNumber & "'"
				set RStemp = Server.CreateObject("ADODB.Recordset")
				RStemp.open SQL, oConn, 3, 3
				if RStemp.eof then
					strNewPartNumbers = strNewPartNumbers & "<li>" & PartNumber & "</li>" & vbcrlf
					SQL = "INSERT INTO we_Items (WE_URL,brandID,modelID,typeID,carrierID,subtypeID,PartNumber,itemDesc,itemDesc_PS,itemDesc_CA,itemDesc_CO,vendor,price_Retail,price_Our,price_CA,price_CO,price_PS,price_ER,price_Buy,COGS,"
					SQL = SQL & "numberOfSales,inv_qty,Seasonal,Sports,hideLive,itemLongDetail,itemLongDetail_PS,itemLongDetail_CA,itemLongDetail_CO,itemWeight,itemDimensions,itemPic,itemPic_CO,itemInfo,"
					SQL = SQL & "BULLET1,BULLET2,BULLET3,BULLET4,BULLET5,BULLET6,BULLET7,BULLET8,BULLET9,BULLET10,COMPATIBILITY,download_URL,download_TEXT,"
					SQL = SQL & "POINT1,POINT2,POINT3,POINT4,POINT5,POINT6,POINT7,POINT8,POINT9,POINT10,"
					SQL = SQL & "FEATURE1,FEATURE2,FEATURE3,FEATURE4,FEATURE5,FEATURE6,FEATURE7,FEATURE8,FEATURE9,FEATURE10,"
					SQL = SQL & "FormFactor,BandType,Band,PackageContents,TalkTime,Standby,Display,Condition,flag1,UPCCode,AMZN_sku,"
					SQL = SQL & "DateTimeEntd) VALUES ("
					SQL = SQL & "'" & formatSEO(itemDesc) & "', "
					SQL = SQL & "'" & brandID & "', "
					SQL = SQL & "'" & modelID & "', "
					SQL = SQL & "19, "
					SQL = SQL & "0, "
					SQL = SQL & "1260, "
					SQL = SQL & "'" & Ucase(PartNumber) & "', "
					SQL = SQL & "'" & SQLquote(itemDesc) & "', "
					SQL = SQL & "'" & SQLquote(itemDesc_PS) & "', "
					SQL = SQL & "'" & SQLquote(itemDesc_CA) & "', "
					SQL = SQL & "'" & SQLquote(itemDesc_CO) & "', "
					SQL = SQL & "'DS', "
					SQL = SQL & price_Retail & ", "
					SQL = SQL & price_Our & ", "
					SQL = SQL & price_CA & ", "
					SQL = SQL & price_CO & ", "
					SQL = SQL & price_PS & ", "
					SQL = SQL & price_ER & ", "
					SQL = SQL & price_Buy & ", "
					SQL = SQL & COGS & ", "
					SQL = SQL & "0, "
					SQL = SQL & inv_qty & ", "
					SQL = SQL & "0, "
					SQL = SQL & "0, "
					SQL = SQL & "0, "
					SQL = SQL & "'" & SQLquote(itemLongDetail) & "', "
					SQL = SQL & "'" & SQLquote(itemLongDetail_PS) & "', "
					SQL = SQL & "'" & SQLquote(itemLongDetail_CA) & "', "
					SQL = SQL & "'" & SQLquote(itemLongDetail_CO) & "', "
					SQL = SQL & "'" & RS("product_weight") & "', "
					SQL = SQL & "'" & RS("product_length") & " x " & RS("product_width") & "', "
					SQL = SQL & "'" & productImage & "', "
					SQL = SQL & "'" & productImage_CO & "', "
					SQL = SQL & "'', "
					SQL = SQL & "'" & SQLquote(BULLET1) & "', "
					SQL = SQL & "'" & SQLquote(BULLET2) & "', "
					SQL = SQL & "'" & SQLquote(BULLET3) & "', "
					SQL = SQL & "'" & SQLquote(BULLET4) & "', "
					SQL = SQL & "'" & SQLquote(BULLET5) & "', "
					SQL = SQL & "'" & SQLquote(BULLET6) & "', "
					SQL = SQL & "'" & SQLquote(BULLET7) & "', "
					SQL = SQL & "'" & SQLquote(BULLET8) & "', "
					SQL = SQL & "'" & SQLquote(BULLET9) & "', "
					SQL = SQL & "'" & SQLquote(BULLET10) & "', "
					SQL = SQL & "'" & SQLquote(COMPATIBILITY) & "', "
					SQL = SQL & "'', "
					SQL = SQL & "'', "
					SQL = SQL & "'" & SQLquote(POINT1) & "', "
					SQL = SQL & "'" & SQLquote(POINT2) & "', "
					SQL = SQL & "'" & SQLquote(POINT3) & "', "
					SQL = SQL & "'" & SQLquote(POINT4) & "', "
					SQL = SQL & "'" & SQLquote(POINT5) & "', "
					SQL = SQL & "'" & SQLquote(POINT6) & "', "
					SQL = SQL & "'" & SQLquote(POINT7) & "', "
					SQL = SQL & "'" & SQLquote(POINT8) & "', "
					SQL = SQL & "'" & SQLquote(POINT9) & "', "
					SQL = SQL & "'" & SQLquote(POINT10) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE1) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE2) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE3) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE4) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE5) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE6) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE7) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE8) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE9) & "', "
					SQL = SQL & "'" & SQLquote(FEATURE10) & "', "
					SQL = SQL & "'', 0, '', '', '', '', '', 0, "
					SQL = SQL & "'" & flag1 & "', "
					SQL = SQL & "'" & UPCCode & "', "
					SQL = SQL & "'" & AMZN_sku & "', "
					SQL = SQL & "'" & now & "')"
					session("errorSQL") = SQL
					response.write "<p>" & SQL & "</p>"
					oConn.execute SQL
				else
					updateCount = updateCount + 1
					SQL = "UPDATE we_Items SET "
					SQL = SQL & "WE_URL = '" & formatSEO(RS("product_name")) & "', "
					SQL = SQL & "brandID = '" & brandID & "', "
					SQL = SQL & "modelID = '" & modelID & "', "
					SQL = SQL & "typeID = 17, "
					SQL = SQL & "carrierID = 0, "
					SQL = SQL & "PartNumber = '" & Ucase(PartNumber) & "', "
					SQL = SQL & "itemDesc = '" & SQLquote(itemDesc) & "', "
					SQL = SQL & "itemDesc_PS = '" & SQLquote(itemDesc_PS) & "', "
					SQL = SQL & "itemDesc_CA = '" & SQLquote(itemDesc_CA) & "', "
					SQL = SQL & "itemDesc_CO = '" & SQLquote(itemDesc_CO) & "', "
					SQL = SQL & "vendor = 'DS', "
					SQL = SQL & "price_Retail = " & price_Retail & ", "
					SQL = SQL & "price_Our = " & price_Our & ", "
					SQL = SQL & "price_CA = " & price_CA & ", "
					SQL = SQL & "price_CO = " & price_CO & ", "
					SQL = SQL & "price_PS = " & price_PS & ", "
					SQL = SQL & "price_ER = " & price_ER & ", "
					SQL = SQL & "price_Buy = " & price_Buy & ", "
					SQL = SQL & "COGS = " & COGS & ", "
					SQL = SQL & "inv_qty = " & inv_qty & ", "
					SQL = SQL & "hideLive = 0, "
					SQL = SQL & "itemLongDetail = '" & SQLquote(itemLongDetail) & "', "
					SQL = SQL & "itemLongDetail_PS = '" & SQLquote(itemLongDetail_PS) & "', "
					SQL = SQL & "itemLongDetail_CA = '" & SQLquote(itemLongDetail_CA) & "', "
					SQL = SQL & "itemLongDetail_CO = '" & SQLquote(itemLongDetail_CO) & "', "
					SQL = SQL & "itemWeight = '" & RS("product_weight") & "', "
					SQL = SQL & "itemDimensions = '" & RS("product_length") & " x " & RS("product_width") & "', "
					SQL = SQL & "itemPic = '" & productImage & "', "
					SQL = SQL & "itemPic_CO = '" & productImage_CO & "', "
					SQL = SQL & "BULLET1 = '" & SQLquote(BULLET1) & "', "
					SQL = SQL & "BULLET2 = '" & SQLquote(BULLET2) & "', "
					SQL = SQL & "BULLET3 = '" & SQLquote(BULLET3) & "', "
					SQL = SQL & "BULLET4 = '" & SQLquote(BULLET4) & "', "
					SQL = SQL & "BULLET5 = '" & SQLquote(BULLET5) & "', "
					SQL = SQL & "BULLET6 = '" & SQLquote(BULLET6) & "', "
					SQL = SQL & "BULLET7 = '" & SQLquote(BULLET7) & "', "
					SQL = SQL & "BULLET8 = '" & SQLquote(BULLET8) & "', "
					SQL = SQL & "BULLET9 = '" & SQLquote(BULLET9) & "', "
					SQL = SQL & "BULLET10 = '" & SQLquote(BULLET10) & "', "
					SQL = SQL & "COMPATIBILITY = '" & SQLquote(COMPATIBILITY) & "', "
					SQL = SQL & "POINT1 = '" & SQLquote(POINT1) & "', "
					SQL = SQL & "POINT2 = '" & SQLquote(POINT2) & "', "
					SQL = SQL & "POINT3 = '" & SQLquote(POINT3) & "', "
					SQL = SQL & "POINT4 = '" & SQLquote(POINT4) & "', "
					SQL = SQL & "POINT5 = '" & SQLquote(POINT5) & "', "
					SQL = SQL & "POINT6 = '" & SQLquote(POINT6) & "', "
					SQL = SQL & "POINT7 = '" & SQLquote(POINT7) & "', "
					SQL = SQL & "POINT8 = '" & SQLquote(POINT8) & "', "
					SQL = SQL & "POINT9 = '" & SQLquote(POINT9) & "', "
					SQL = SQL & "POINT10 = '" & SQLquote(POINT10) & "', "
					SQL = SQL & "FEATURE1 = '" & SQLquote(FEATURE1) & "', "
					SQL = SQL & "FEATURE2 = '" & SQLquote(FEATURE2) & "', "
					SQL = SQL & "FEATURE3 = '" & SQLquote(FEATURE3) & "', "
					SQL = SQL & "FEATURE4 = '" & SQLquote(FEATURE4) & "', "
					SQL = SQL & "FEATURE5 = '" & SQLquote(FEATURE5) & "', "
					SQL = SQL & "FEATURE6 = '" & SQLquote(FEATURE6) & "', "
					SQL = SQL & "FEATURE7 = '" & SQLquote(FEATURE7) & "', "
					SQL = SQL & "FEATURE8 = '" & SQLquote(FEATURE8) & "', "
					SQL = SQL & "FEATURE9 = '" & SQLquote(FEATURE9) & "', "
					SQL = SQL & "FEATURE10 = '" & SQLquote(FEATURE10) & "', "
					SQL = SQL & "UPCCode = '" & UPCCode & "', "
					SQL = SQL & "AMZN_sku = '" & AMZN_sku & "', "
					SQL = SQL & "DateTimeEntd = '" & now & "'"
					SQL = SQL & " WHERE itemID = '" & RStemp("itemID") & "'"
					session("errorSQL") = SQL
					response.write "<p>" & SQL & "</p>"
					oConn.execute SQL
				end if
				RStemp.close
				set RStemp = nothing
				RS.movenext
			loop
		end if
		SQL = "SELECT itemID FROM we_items WHERE modelID = '" & modelID & "'"
		SQL = SQL & " AND PartNumber LIKE 'DEC-SKN-%' AND PartNumber NOT IN (" & left(strPartNumbers,len(strPartNumbers)-1) & ")"
		response.write "<p>" & SQL & "</p>"
		set RStemp = Server.CreateObject("ADODB.Recordset")
		RStemp.open SQL, oConn, 3, 3
		do until RStemp.eof
			deleteCount = deleteCount + 1
			SQL = "UPDATE we_items SET inv_qty = 0, hidelive = -1 WHERE itemID = '" & RStemp("itemID") & "'"
			session("errorSQL") = SQL
			response.write "<p>" & SQL & "</p>"
			oConn.execute SQL
			
			sql = "insert into we_invRecord (itemID,inv_qty,adjustQty,adminID,notes,editDate) values(" & itemID & ",0," & inv_qty & "," & session("adminID") & ",'Decalskin_upload','" & now & "')"
			session("errorSQL") = sql
			oConn.execute(sql)
			
			RStemp.movenext
		loop
		RS.close
		set RS = nothing
		%>
		<p><%=updateCount%> items updated.</p>
		<p><%=deleteCount%> items "deleted".</p>
		<p>
			New Part Numbers:
			<ul>
				<%=strNewPartNumbers%>
			</ul>
		</p>
		<h3>DONE!</h3>
		<%
	end if
else
	%>
	<h3>
		[Upload Decalskin spreadhseet to WE]<br>
		Select the file you saved to your hard drive:<br>
		(IMPORTANT: Make sure that the Worksheet is named "Sheet1"!)
	</h3>
	<form enctype="multipart/form-data" action="Decalskin_upload_items.asp" method="POST">
		<p><input type="FILE" name="FILE1"></p>
		<p><input type="text" name="modelID">&nbsp;&nbsp;Model ID</p>
		<p><input type="SUBMIT" name="upload" value="Upload"></p>
	</form>
	<%
end if

function SQLquote(strValue)
	if not isNull(strValue) and strValue <> "" then
		SQLquote = trim(replace(strValue,"'","''"))
		SQLquote = trim(replace(SQLquote,chr(34),"''"))
	else
		SQLquote = strValue
	end if
end function
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
