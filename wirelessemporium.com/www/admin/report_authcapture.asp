<%
pageTitle = "Authorize.net Capture Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<link href="/includes/css/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/includes/js/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/includes/js/cal.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function () {
		$('#id_sdate1').simpleDatepicker();
		$('#id_edate1').simpleDatepicker();
		$('#id_sdate2').simpleDatepicker();
		$('#id_edate2').simpleDatepicker();		
	});
</script>
<%
strSDate1 = prepStr(request.form("txtSDate1"))
strEDate1 = prepStr(request.form("txtEDate1"))
strSDate2 = prepStr(request.form("txtSDate2"))
strEDate2 = prepStr(request.form("txtEDate2"))
txtOrderID = request.form("txtOrderID")
strRdoDate = prepStr(request.form("rdoDate"))
strGroupBy = prepStr(request.form("cbGroupBy"))
chkDetails = prepInt(request.form("chkDetails"))

bNotScanned = false
if prepInt(request.form("chkNotScanned")) = 1 then bNotScanned = true
bNotCaptured = false
if prepInt(request.form("chkNotCaptured")) = 1 then bNotCaptured = true
bFailed = false
if prepInt(request.form("chkFailed")) = 1 then bFailed = true

if chkDetails = 1 then bDetails = true
if strRdoDate = "" then strRdoDate = "s"
if strSDate1 = "" or not isdate(strSDate1) then strSDate1 = Date
if strEDate1 = "" or not isdate(strEDate1) then strEDate1 = Date
if strSDate2 = "" or not isdate(strSDate2) then strSDate2 = Date
if strEDate2 = "" or not isdate(strEDate2) then strEDate2 = Date

if cdate(strSDate1) < cdate("2/25/2014") then 
	strSDate1 = "2/25/2014"
end if	

if cdate(strSDate2) < cdate("2/25/2014") then 
	strSDate2 = "2/25/2014"
end if	

Dim cfgArrGroupBy : cfgArrGroupBy = Array("Date","Store","Capture Type")
if "" = strGroupBy then 
	strGroupBy = "Date,Store,Capture Type" 
end if
arrGroupBy = split(strGroupBy, ",")
groupBySize = ubound(arrGroupBy)

dim strSqlSelectMain, strSqlWhere, strSqlGroupBy, strSqlOrderBy, strHeadings
strSqlSelectMain	=	""
strSqlWhere			=	""
strSqlGroupBy		=	""
strSqlOrderBy		=	""
strHeadings			=	""

bDate			=	false
bStore			=	false
bCaptureType	=	false

For i = 0 To Cint(UBound(arrGroupBy))
	If "" <> trim(arrGroupBy(i)) Then
		Select Case ucase(trim(arrGroupBy(i)))
			Case "DATE"
				If bDate Then
					Response.Write "<Script>alert('DATE in [GROUP BY] is duplicated');history.back();</Script>"
					call responseEnd()
				End If
				bDate = true

				if strRdoDate = "s" then
					dateToken = "convert(varchar(10), a.scandate, 20)"
					dateTokenName = "scanDate"
				else
					dateToken = "convert(varchar(10), a.thub_posted_date, 20)"
					dateTokenName = "processDate"
				end if
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", " & dateToken & " " & dateTokenName
				strSqlGroupBy		=	strSqlGroupBy		&	dateToken & ", "
				strSqlOrderBy		=	strSqlOrderBy		&	dateToken & ", "
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:112px; font-size:13px; font-weight:bold; position:relative;"">" & dateTokenName & "</div>"

			Case "STORE"
				If bStore Then
					Response.Write "<Script>alert('STORE in [GROUP BY] is duplicated');history.back();</Script>"
					call responseEnd()
				End If
				bStore = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.store"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.store, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.store, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:42px; font-size:13px; font-weight:bold; position:relative;"">SITE</div>"

			Case "CAPTURE TYPE"
				If bCaptureType Then
					Response.Write "<Script>alert('Capture Type in [GROUP BY] is duplicated');history.back();</Script>"
					call responseEnd()
				End If
				bCaptureType = true

				strSqlSelectMain	=	strSqlSelectMain 	& 	", case when a.scandate is null then 'NOT SCANNED' else isnull(b.captureType, 'NOT CAPTURED') end captureType"
				strSqlGroupBy		=	strSqlGroupBy		&	"case when a.scandate is null then 'NOT SCANNED' else isnull(b.captureType, 'NOT CAPTURED') end, "
				strSqlOrderBy		=	strSqlOrderBy		&	"case when a.scandate is null then 'NOT SCANNED' else isnull(b.captureType, 'NOT CAPTURED') end desc, "				
				strHeadings			=	strHeadings			&	"<div style=""float:left; width:112px; font-size:13px; font-weight:bold; position:relative;"">Capture Type</div>"
		End Select
	End If
Next
%>
<center>
<form method="post">
    <div style="text-align:left; width:1100px;">
        <div style="padding-bottom:10px; height:20px;">
        	<div style="font-size:18px; font-weight:bold; float:left;">Authorize.net Capture Report</div>
            <!--<div style="font-size:11px; float:right;">* The result with more than <span style="color:red;">3,000</span> rows will automatically be exported to Excel.</div>-->
		</div>
        <!-- filters-->
        <div style="width:100%; border:1px solid #ccc; border-radius:3px; background-color:#f4f7f9; padding:5px;">
            <div style="width:100%; height:30px;">
				<div class="filter-box-header" style="width:50px;">Filter</div>
                <div class="filter-box-header" style="width:180px;"><input type="radio" name="rdoDate" value="s" <%if strRdoDate = "s" then%>checked<%end if%>/>Scan Date</div>
                <div class="filter-box-header" style="width:180px;"><input type="radio" name="rdoDate" value="p" <%if strRdoDate = "p" then%>checked<%end if%>/>Process Date</div>
                <div class="filter-box-header" style="width:90px;">ORDERID</div>
                <div class="filter-box-header" style="width:90px;">Not Scanned</div>
                <div class="filter-box-header" style="width:90px;">Not Captured</div>
                <div class="filter-box-header" style="width:90px;">Failed</div>
                <div class="filter-box-header" style="width:130px;">Action</div>
            </div>
            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%; padding:5px 0px 5px 0px; height:25px;">
				<div class="filter-box-header" style="width:50px;">Value</div>
                <div class="filter-box" style="width:180px;"><input type="text" id="id_sdate1" name="txtSDate1" value="<%=strSDate1%>" size="7"> ~ <input type="text" id="id_edate1" name="txtEDate1" value="<%=strEDate1%>" size="7"></div>
                <div class="filter-box" style="width:180px;"><input type="text" id="id_sdate2" name="txtSDate2" value="<%=strSDate2%>" size="7"> ~ <input type="text" id="id_edate2" name="txtEDate2" value="<%=strEDate2%>" size="7"></div>
                <div class="filter-box" style="width:90px;"><input type="text" name="txtOrderID" value="<%=txtOrderID%>" style="width:80px;" /></div>
                <div class="filter-box" style="width:90px;"><input type="checkbox" name="chkNotScanned" value="1" <%if bNotScanned then%>checked<%end if%> /></div>
                <div class="filter-box" style="width:90px;"><input type="checkbox" name="chkNotCaptured" value="1" <%if bNotCaptured then%>checked<%end if%> /></div>
                <div class="filter-box" style="width:90px;"><input type="checkbox" name="chkFailed" value="1" <%if bFailed then%>checked<%end if%> /></div>
                <div class="filter-box" style="width:130px;">
	                <input type="submit" name="search" value="Search" />
                </div>
            </div>

            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff;"></div>
            <div style="width:100%; padding-top:5px; height:25px;">     
				<div class="filter-box-header" style="width:100px;">Group By</div>
				<div class="filter-box" style="width:350px; padding-top:3px; text-align:left;" align="left">
                    <div id="groupBySelect" style="<%if bDetails then%>display:none;<%end if%>">
                    <%
                    if not isnull(cfgArrGroupBy) then
                        for i=0 to groupBySize
                            response.write "<select name=""cbGroupBy""><option value="""" "
                            if "" = trim(arrGroupBy(i)) then response.write " selected " end if
                            response.write ">--</option>"
                            
                            for j=0 to ubound(cfgArrGroupBy,1)
                                response.write "<option value=""" & cfgArrGroupBy(j) & """ "
                                if ucase(trim(arrGroupBy(i))) = ucase(trim(cfgArrGroupBy(j))) then response.write " selected " end if
                                response.write ">" & trim(cfgArrGroupBy(j)) & "</option>"
                            next
                            response.write "</select>&nbsp;"
                        next
                    end if
                    %>
                    </div>
                </div>
				<div class="filter-box-header" style="width:150px;">Show me details <input type="checkbox" name="chkDetails" value="1" onclick="onDetails(this.checked)" <%if bDetails then%>checked<%end if%> /></div>
            </div>

        </div>
		<!--// filters-->
        <!-- result -->
		<div style="width:100%; padding-top:10px;">
        <%
		strSqlWhere = ""
		if txtOrderID <> "" then strSqlWhere = strSqlWhere & " and	a.orderid = '" & txtOrderID & "'" & vbcrlf
		if strRdoDate = "s" then
			strSqlWhere = strSqlWhere & " and	a.scandate >= '" & strSDate1 & "' and a.scandate < '" & dateadd("d", 1, strEDate1) & "'" & vbcrlf
		else
			strSqlWhere = strSqlWhere & " and	a.thub_posted_date >= '" & strSDate2 & "' and a.thub_posted_date < '" & dateadd("d", 1, strEDate2) & "'" & vbcrlf		
		end if
		if bNotScanned then strSqlWhere = strSqlWhere & " and	a.scandate is null " & vbcrlf		
		if bNotCaptured then strSqlWhere = strSqlWhere & " and a.scandate is not null and b.captureType is null " & vbcrlf
		if bFailed then strSqlWhere = strSqlWhere & " and not (b.responseStatus = 1 and b.responseReasonCode = 1) " & vbcrlf
		
		if bDetails then
			strSqlOrderBy = "order by a.store, a.orderid"
			sql	=	"select	a.store, a.orderid" & vbcrlf & _
					"	,	isnull(case when b.seq = 2 then a.pp_ordergrandtotal else a.ordergrandtotal end, 0) captureAmount" & vbcrlf & _
					"	,	case when b.seq = 2 then a.pp_transactionid else a.transactionid end transactionid" & vbcrlf & _
					"	,	a.RMAStatus" & vbcrlf & _
					"	,	convert(varchar(10), a.orderdatetime, 20) orderdate, convert(varchar(10), a.scandate, 20) scandate, convert(varchar(10), a.thub_posted_date, 20) processDate" & vbcrlf & _
					"	,	convert(varchar(10), b.batchDate, 20) batchDate, convert(varchar(10), b.authCaptureDate, 20) authCaptureDate" & vbcrlf & _
					"	,	b.seq, case when a.scandate is null then 'NOT SCANNED' else isnull(b.captureType, 'NOT CAPTURED') end captureType, b.responseStatus, b.responseReasonCode, b.responseText" & vbcrlf & _
					"	,	a.parentorderid, c.rma_token" & vbcrlf & _
					"from	we_orders a with (nolock) left outer join we_authorizenet b with (nolock)" & vbcrlf & _
					"	on	a.orderid = b.orderid left outer join xrmatype c with (nolock)" & vbcrlf & _
					"	on	a.RMAstatus = c.id" & vbcrlf & _
					"where	a.approved = 1" & vbcrlf & _
					"	and	(a.cancelled is null or a.cancelled = 0)" & vbcrlf & _
					"	and	a.transactionid is not null and a.transactionid <> ''" & vbcrlf & _
					strSqlWhere & vbcrlf & _
					strSqlOrderBy
'			response.write "<pre>" & sql & "</pre>"
'			call responseEnd()
			session("errorSQL") = sql
			set rs = oConn.execute(sql)
			if rs.eof then
				response.write "No data to display"
			else
				%>
				<div style="width:100%; height:30px; border:1px solid #c1e0b2; border-radius:3px; background-color:#eaf5e5; padding:5px;">
					<div style="float:left; width:42px; font-size:13px; font-weight:bold; position:relative;">SITE</div>
					<div style="float:left; width:82px; font-size:13px; font-weight:bold; position:relative;">ORDERID</div>
					<div style="float:left; width:102px; font-size:13px; font-weight:bold; position:relative;">Capture Type</div>
					<div style="float:left; width:82px; font-size:13px; font-weight:bold; position:relative;" align="right">Capture Amount</div>
					<div style="float:left; width:102px; font-size:13px; font-weight:bold; position:relative;" align="right">Transaction ID</div>
					<div style="float:left; width:92px; font-size:13px; font-weight:bold; position:relative;" align="center">OrderDate</div>                    
					<div style="float:left; width:92px; font-size:13px; font-weight:bold; position:relative;">ScanDate</div>
					<div style="float:left; width:92px; font-size:13px; font-weight:bold; position:relative;">ProcessDate</div>
					<div style="float:left; width:92px; font-size:13px; font-weight:bold; position:relative;">Batch Run<Br />Date</div>
                    <!--
					<div style="float:left; width:112px; font-size:13px; font-weight:bold; position:relative;"></div>
                    -->
					<div style="float:left; width:82px; font-size:13px; font-weight:bold; position:relative;">Response<br />Code</div>
					<div style="float:left; width:82px; font-size:13px; font-weight:bold; position:relative;">Parent<br />OrderID</div>
					<div style="float:left; width:82px; font-size:13px; font-weight:bold; position:relative;">RMA</div>
				</div>
                <div id="searchResult">
				<%
				lap = 0
				do until rs.eof
					lap = lap + 1			
					rowBackgroundColor = "#fff"
					if (lap mod 2) = 0 then rowBackgroundColor = "#eaf5fa"
					
					store = rs("store")
					select case store
						case 0 : store = "WE"
						case 1 : store = "CA"
						case 2 : store = "CO"
						case 3 : store = "PS"
						case 10 : store = "TM"
					end select

					orderid = rs("orderid")
					captureType = rs("captureType")
					if captureType = "NOT SCANNED" then
						captureType = "<span style=""color:#f00;"">" & captureType & "</span>"
					elseif captureType = "NOT CAPTURED" then
						captureType = "<span style=""color:blue;"">" & captureType & "</span>"
					end if
					captureAmount = rs("captureAmount")
					transactionid = rs("transactionid")
					rmastatus = rs("rmastatus")
					orderdate = rs("orderdate")
					scandate = rs("scandate")
					processdate = rs("processdate")
					batchRunDate = rs("batchDate")
					responseCode = "<span style=""color:#f00;"">FAILED</span>"
					responseText = rs("responseText")
					parentOrderID = rs("parentorderid")
					rmastatus = rs("rma_token")

					if rs("responseStatus") = 1 and rs("responseReasonCode") = 1 then
						responseCode = "CAPTURED"
					end if

					%>
				<div class="table-row" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='1px solid <%=rowBackgroundColor%>';this.style.backgroundColor='<%=rowBackgroundColor%>'">
					<div class="table-cell" style="width:40px;"><%=store%>&nbsp;</div>
					<div class="table-cell" style="width:80px;"><%=orderid%>&nbsp;</div>
					<div class="table-cell" style="width:100px;"><%=captureType%>&nbsp;</div>
                    <div class="table-cell" style="width:80px;" align="right"><%=formatcurrency(captureAmount)%>&nbsp;</div>
					<div class="table-cell" style="width:100px;" align="right"><%=transactionid%>&nbsp;</div>
					<div class="table-cell" style="width:90px;" align="center"><%=orderdate%>&nbsp;</div>
					<div class="table-cell" style="width:90px;"><%=scandate%>&nbsp;</div>
					<div class="table-cell" style="width:90px;"><%=processdate%>&nbsp;</div>
					<div class="table-cell" style="width:90px;"><%=batchRunDate%>&nbsp;</div>
					<div class="table-cell" style="width:80px;" title="<%=responseText%>"><%=responseCode%>&nbsp;</div>
					<div class="table-cell" style="width:80px;"><%=parentOrderID%>&nbsp;</div>
					<div class="table-cell" style="width:80px;"><%=rmastatus%>&nbsp;</div>
				</div>
					<%
					if (lap mod 1000) = 0 then response.flush
					rs.movenext
				loop
				%>
			</div>
            <div style="clear:both; border-top:1px solid #ccc; padding:5px;">- End of Report -</div>
			<%
			end if
		else
			if "" <> strSqlGroupBy then
				strSqlGroupBy	=	"group by " & left(trim(strSqlGroupBy), len(trim(strSqlGroupBy))-1)
				strSqlOrderBy	=	"order by " & left(trim(strSqlOrderBy), len(trim(strSqlOrderBy))-1)
			end if
			
			sql	=	"select	count(*) totalTransactions" & vbcrlf & _
					"	,	sum(case when authCaptureDate is not null then 1 else 0 end) captured" & vbcrlf & _
					"	,	sum(case when authCaptureDate is null then 1 else 0 end) failed" & vbcrlf & _
					strSqlSelectMain & vbcrlf & _
					"from	we_orders a with (nolock) left outer join we_authorizenet b with (nolock)" & vbcrlf & _
					"	on	a.orderid = b.orderid" & vbcrlf & _
					"where	a.approved = 1 and (a.cancelled = 0 or a.cancelled is null)" & vbcrlf & _
					"	and	a.transactionid is not null and a.transactionid <> ''" & vbcrlf & _
					strSqlWhere & vbcrlf & _
					strSqlGroupBy & vbcrlf & _					
					strSqlOrderBy
'			response.write "<pre>" & sql & "</pre>"
'			call responseEnd()
			session("errorSQL") = sql
			arrResult = getDbRows(sql)
			%>
            <div style="width:100%; height:30px; border:1px solid #c1e0b2; border-radius:3px; background-color:#eaf5e5; padding:5px;">
            	<%=strHeadings%>
                <div style="float:left; width:152px; font-size:13px; font-weight:bold; cursor:pointer; position:relative;" align="right">Total Transactions</div>
                <div style="float:left; width:102px; font-size:13px; font-weight:bold; cursor:pointer; position:relative;" align="right">Captured</div>
                <div style="float:left; width:102px; font-size:13px; font-weight:bold; cursor:pointer; position:relative;" align="right">Failed</div>
            </div>
            <div id="searchResult">
	        <%
			qtyTotal = clng(0)
			priceTotal = cdbl(0)
			cogsTotal = cdbl(0)
			revenueTotal = cdbl(0)
			if not isnull(arrResult) then
				for nRow=0 to ubound(arrResult,2)
					rowBackgroundColor = "#eaf5fa"
					if (nRow mod 2) = 0 then rowBackgroundColor = "#fff"				
					if (nRow mod 500) = 0 then response.Flush()
					
					numTotal = formatnumber(clng(arrResult(0,nRow)), 0)
					numCaptured = formatnumber(cdbl(arrResult(1,nRow)), 0)
					numFailed = formatnumber(cdbl(arrResult(2,nRow)), 0)
				%>
				<div class="table-row" style="background-color:<%=rowBackgroundColor%>; border:1px solid <%=rowBackgroundColor%>;" onmouseover="this.style.border='1px solid #ffa952';this.style.backgroundColor='#ffe7b2'" onmouseout="this.style.border='1px solid <%=rowBackgroundColor%>';this.style.backgroundColor='<%=rowBackgroundColor%>'">
                <%
					nSkipColumn = 3
					totalWidth = 0
					for i=0 to ubound(arrGroupBy)
						if "" <> trim(arrGroupBy(i)) then
							select case ucase(trim(arrGroupBy(i)))
								case "DATE"
								%>
                                    <div class="table-cell" style="width:110px;"><%=arrResult(nSkipColumn,nRow)%>&nbsp;</div>
                                <%
								Case "STORE"
									select case cint(arrResult(nSkipColumn,nRow))
										case 0 : site = "WE"
										case 1 : site = "CA"
										case 2 : site = "CO"
										case 3 : site = "PS"
										case 10 : site = "TM"
									end select
									%>
                                    <div class="table-cell" style="width:40px;"><%=site%>&nbsp;</div>
                                    <%
								Case "CAPTURE TYPE"
								%>
                                    <div class="table-cell" style="width:110px;">
									<%
										if arrResult(nSkipColumn,nRow) = "NOT SCANNED" then
											response.write "<span style=""color:#f00;"">" & arrResult(nSkipColumn,nRow)& "</span>"
										elseif arrResult(nSkipColumn,nRow) = "NOT CAPTURED" then
											response.write "<span style=""color:blue;"">" & arrResult(nSkipColumn,nRow)& "</span>"
										else
											response.write arrResult(nSkipColumn,nRow)
										end if
									%>
                                        &nbsp;
                                    </div>
                                <%
							end select
							nSkipColumn = nSkipColumn + 1										
						end if
					next
					%>
					<div class="table-cell" style="width:150px;" align="right"><%=numTotal%>&nbsp;</div>
					<div class="table-cell" style="width:100px;" align="right"><b><%=numCaptured%></b>&nbsp;</div>
					<div class="table-cell" style="width:100px;" align="right"><%=numFailed%>&nbsp;</div>
				</div>
                    <%
				next
			else
				response.write "No data to display"
			end if
			%>
            </div>
            <%
		end if
		%>
       	</div>
        <!--// result -->
    </div>
</form>    
</center>
<script>
	function onDetails(checked) {
		if (checked) {
			document.getElementById('groupBySelect').style.display = 'none';
		} else {
			document.getElementById('groupBySelect').style.display = '';
		}
	}
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->