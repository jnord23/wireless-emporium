dim oConn
Set oConn = CreateObject("ADODB.Connection")
oConn.ConnectionString = "DSN=WEDB01;Uid=wireless_user;Pwd=c3llph0n3;"
oConn.ConnectionTimeout = 1000		'seconds
oConn.Open

dim fs, file, filename, path
filename = "productList_Fetchback.csv"
path = "e:\inetpub\wwwroot\wirelessemporium.com\www\tempCSV\" & filename

set fs = CreateObject("Scripting.FileSystemObject")
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if

'Create the file and write some data to it.
set file = fs.CreateTextFile(path)
dim SQL, RS, strline
SQL = "SELECT A.itemid, A.PartNumber, A.itemDesc, A.itemLongDetail, A.price_Our, A.itemPic, A.UPCCode, A.inv_qty, T.typename"
SQL = SQL & " FROM we_Items A JOIN we_Types AS T ON A.typeid = T.typeid"
SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
SQL = SQL & " AND A.typeid <> 16"
set RS = CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
if not RS.eof then
	file.WriteLine "product name,product page url,product image url,product category,product description,Product ID,Price"
	do until RS.eof
		DoNotInclude = 0
		if RS("inv_qty") < 0 then
			SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
			set RS2 = CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 3, 3
			if RS2.eof then DoNotInclude = 1
			RS2.close
			set RS2 = nothing
		end if
		if DoNotInclude = 0 then
			strline = chr(34) & RS("itemDesc") & chr(34) & ","
			strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?id=fbproduct,"
			strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & ","
			strline = strline & chr(34) & RS("typeName") & chr(34) & ","
			strline = strline & chr(34) & replace(RS("itemLongDetail"),vbcrlf," ") & chr(34) & ","
			strline = strline & RS("itemID") & ","
			strline = strline & formatCurrency(RS("price_Our"))
			file.WriteLine strline
		end if
		RS.movenext
	loop
end if
RS.close
set RS = nothing
oConn.close
file.close()

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function
