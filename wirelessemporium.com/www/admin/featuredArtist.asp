<%
pageTitle = "Featured Artist"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
'Path = "C:\inetpub\wwwroot\productpics\featuredArtist"
Path = server.MapPath("/productpics/featuredArtist")

set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = true
myCount = Upload.Save(Path)

if Upload.form("submit") <> "" then
	set imgArtist = Upload.files("imgArtist")
	set imgSmallBanner = Upload.files("imgSmallBanner")
	set imgLargeBanner = Upload.files("imgLargeBanner")
	txtArtist = Upload.form("txtArtist")
	txtStudio = Upload.form("txtStudio")
	txtBio = Upload.form("txtBio")
	chkHidelive = prepInt(Upload.form("chkHidelive"))
	
	if Upload.form("submit") = "Add New" then
		sql	=	"insert into fa_artist(artistname, bio, artistPic, banner1, studioname, studiopic, hidelive)" & vbcrlf & _
				"values('" & SQLQuote(txtArtist) & "','" & SQLQuote(txtBio) & "','" & SQLQuote(imgArtist.filename) & "','" & SQLQuote(imgLargeBanner.filename) & "','" & SQLQuote(txtStudio) & "','" & SQLQuote(imgSmallBanner.filename) & "'," & chkHidelive & ")"
				
		oConn.execute(sql)
	elseif Upload.form("submit") = "Edit" then

		sql	=	"update	fa_artist" & vbcrlf & _
				"set	artistname = '" & SQLQuote(txtArtist) & "'" & vbcrlf & _
				"	,	bio = '" & SQLQuote(txtBio) & "'" & vbcrlf & _
				"	,	studioname = '" & SQLQuote(txtStudio) & "'" & vbcrlf & _
				"	,	hidelive = " & chkHidelive & vbcrlf

		if not imgArtist is nothing then sql = sql & "	,	artistPic = '" & SQLQuote(imgArtist.filename) & "'" & vbcrlf
		if not imgSmallBanner is nothing then sql = sql & "	,	studiopic = '" & SQLQuote(imgSmallBanner.filename) & "'" & vbcrlf
		if not imgLargeBanner is nothing then sql = sql & "	,	banner1 = '" & SQLQuote(imgLargeBanner.filename) & "'" & vbcrlf
		
		sql = sql & "where	id = '" & prepInt(Upload.form("artistID")) &  "'"
		
		oConn.execute(sql)
	end if
end if
%>
<center>
    <div style="text-align:left; width:1100px;">
        <div style="float:left; padding-bottom:10px; height:20px;">
        	<div style="font-size:18px; font-weight:bold; float:left;">Featured Artists</div>
		</div>
		<form name="frm" method="post" enctype="multipart/form-data">
        <div style="width:100%; float:left; border:1px solid #ccc; border-radius:3px; background-color:#f4f7f9; padding:5px;">
        	<div style="width:600px; float:left;">
                <div style="width:100%; float:left;">
                    <div class="filter-box-header" style="width:200px;">Artist Name</div>
                    <div class="filter-box" style="width:300px; text-align:left;"><input type="text" name="txtArtist" style="width:200px;" /></div>                
                </div>
                <div style="width:100%; float:left; padding-top:5px;">
                    <div class="filter-box-header" style="width:200px;">Studio Name</div>
                    <div class="filter-box" style="width:300px; text-align:left;"><input type="text" name="txtStudio" style="width:200px;" /></div>
                </div>
                <div style="width:100%; float:left; padding-top:5px;">
                    <div class="filter-box-header" style="width:200px;">Artist Picture</div>
                    <div class="filter-box" style="width:300px; text-align:left;"><input type="file" name="imgArtist" /></div>
                </div>
                <div style="width:100%; float:left; padding-top:5px;">
                    <div class="filter-box-header" style="width:200px;">BMC Banner Image</div>
                    <div class="filter-box" style="width:300px; text-align:left;"><input type="file" name="imgSmallBanner" /></div>
                </div>
                <div style="width:100%; float:left; padding-top:5px;">
                    <div class="filter-box-header" style="width:200px;">BMCD Top Banner Image</div>
                    <div class="filter-box" style="width:300px; text-align:left;"><input type="file" name="imgLargeBanner" /></div>
                </div>
            </div>
        	<div style="width:500px; float:left;">
                <div style="width:100%; float:left;">
                    <div class="filter-box-header" style="width:200px;">About this artist</div>
                    <div style="clear:both; height:5px;"></div>
                    <div class="filter-box" style="padding:0px; width:100%; text-align:left;">
                    	<textarea name="txtBio" cols="55" rows="6"></textarea>
					</div>
                </div>
            </div>
            <div style="border-top:1px solid #ccc; border-bottom:1px solid #fff; width:100%; float:left; margin:5px 0px 5px 0px;"></div>
            <div style="width:100%; float:left; text-align:right;">
            	<div style="float:right;"><input type="submit" name="submit" value="Add New" /></div>
            	<div style="float:right; padding-right:10px;"><input type="checkbox" name="chkHidelive" value="1" />Hidelive</div>
            </div>
        </div>
		</form>
		<div style="clear:both;"></div>
		<div style="width:100%; float:left;">
		<%
		sql = "select id, artistname, bio, artistPic, banner1, studioname, studiopic, hidelive from fa_artist order by id"
		set rs = oConn.execute(sql)
		do until rs.eof
		%>
        	<form method="post" enctype="multipart/form-data">
            <input type="hidden" name="artistID" value="<%=rs("id")%>" />
	        <div class="filter-box-header" style="margin:5px 0px 0px 0px; width:100%;">
            	<div style="float:left; width:150px;"><img src="/productpics/featuredartist/<%=rs("artistPic")%>" border="0" /></div>
            	<div style="float:left; width:180px;"><img src="/productpics/featuredartist/<%=rs("studiopic")%>" border="0" /></div>
            	<div style="float:left; width:770px;"><img src="/productpics/featuredartist/<%=rs("banner1")%>" border="0" /></div>
                <div style="float:left; width:100%; text-align:left;">Artist Picture: <input type="file" name="imgArtist" /></div>
                <div style="float:left; width:100%; text-align:left;">BMC Banner Image: <input type="file" name="imgSmallBanner" /></div>
                <div style="float:left; width:100%; text-align:left;">BMCD Top Banner Image: <input type="file" name="imgLargeBanner" /></div>
            	<div style="float:left; width:100%; text-align:left;">Artist Name: <input type="text" name="txtArtist" value="<%=rs("artistname")%>" /></div>
            	<div style="float:left; width:100%; text-align:left;">Studio Name: <input type="text" name="txtStudio" value="<%=rs("studioname")%>" /></div>
            	<div style="float:left; width:100%; text-align:left;">hidelive <input type="checkbox" name="chkHidelive" value="1" <%if rs("hidelive") then%>checked="checked"<%end if%> /> </div>
            	<div style="float:left; width:100%; text-align:left;">Bio: <textarea name="txtBio" cols="120" rows="4"><%=rs("bio")%></textarea></div>
            	<div style="float:left; width:100%; text-align:left;"><input type="submit" name="submit" value="Edit" /></div>
            </div>
            </form>
        <%
			rs.movenext
		loop
        %>
        </div>
    </div>
</center>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
