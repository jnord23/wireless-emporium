<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
siteid = prepInt(request("cbSite"))
brandid = prepInt(request("cbBrand"))
modelid = prepInt(request("cbModel"))
typeid = prepInt(request("cbType"))
partnumber = prepStr(request("txtPartnumber"))
itemid = prepStr(request("txtItemID"))
strGroupBy = prepStr(request("cbGroupBy"))
strCbRules = prepStr(request("cbRules"))
strSDate = prepStr(request("txtSDate"))
strHeading1 = prepStr(request("txtHeading1"))
strHeading2 = prepStr(request("txtHeading2"))
strRules = prepStr(request("txtRules"))
rdoTemplate = prepStr(request("rdoTemplate"))
if rdoTemplate = "" then rdoTemplate = "A"

cfgArrRulesFrom = Array("AAA", "BBB", "CCC", "DDD")
cfgArrCbRules = Array("BrandName", "ModelName", "CategoryName", "SubCategoryName")

if "" = strRules then strRules = ",,," 
arrRules = split(strRules, ",")

if "" = strCbRules then strCbRules = ",,," 
arrCbRules = split(strCbRules, ",")

cfgArrGroupBy = Array("Brand","Model","Category","SubCategory")
if "" = strGroupBy then strGroupBy = ",,," 
arrGroupBy = split(strGroupBy, ",")

'for each i in request.Form
'	response.write i & ":" & request.form(i) & "<br>"
'next

if strHeading1 = "" then strHeading1 = ",,,,,,"
if strHeading2 = "" then strHeading2 = ",,,,"
arrHeading1 = split(strHeading1, ",")
arrHeading2 = split(strHeading2, ",")

siteTypename = "typeName"
select case siteid
	case 0 
		itemdesc = "itemdesc"
		siteTypename = "typeName_WE"
		priceDesc = "price_our"
	case 1 
		itemdesc = "itemdesc_ca"
		siteTypename = "typeName_CA"
		priceDesc = "price_ca"		
	case 2 
		itemdesc = "itemdesc_co"
		siteTypename = "typeName_CO"
		priceDesc = "price_co"		
	case 3 
		itemdesc = "itemdesc_ps"
		siteTypename = "typeName_PS"
		priceDesc = "price_ps"		
	case 10 
		itemdesc = "itemdesc"
		siteTypename = "typeName"
		priceDesc = "price_our"		
end select

bDetails 		= 	true
bBrand			=	false
bModel			=	false
bCategory		=	false
bSubcategory	=	false
pageLevel		=	""

For i = 0 To Cint(UBound(arrGroupBy))
	If "" <> trim(arrGroupBy(i)) Then
		Select Case ucase(trim(arrGroupBy(i)))
			Case "BRAND"
				If not bBrand Then
					bBrand = true
					bDetails = false
	
					strSqlSelectMain	=	strSqlSelectMain 	& 	"c.brandid, c.brandName, "
					strSqlGroupBy		=	strSqlGroupBy		&	"c.brandid, c.brandName, "
					strSqlOrderBy		=	strSqlOrderBy		&	"c.brandid, c.brandName, "
					
					pageLevel = pageLevel & "b"
				End If
			Case "MODEL"
				If not bModel Then
					bModel = true
					bDetails = false
	
					strSqlSelectMain	=	strSqlSelectMain 	& 	"d.modelid, d.modelName, "
					strSqlGroupBy		=	strSqlGroupBy		&	"d.modelid, d.modelName, "
					strSqlOrderBy		=	strSqlOrderBy		&	"d.modelid, d.modelName, "
					
					pageLevel = pageLevel & "m"
				End If
			Case "CATEGORY"
				If not bCategory Then
					bCategory = true
					bDetails = false
	
					strSqlSelectMain	=	strSqlSelectMain 	& 	"f.typeid, f.typename, "
					strSqlGroupBy		=	strSqlGroupBy		&	"f.typeid, f.typename, "
					strSqlOrderBy		=	strSqlOrderBy		&	"f.typeid, f.typename, "
					
					pageLevel = pageLevel & "c"
				End If
			Case "SUBCATEGORY"
				If not bSubCategory Then
					bSubCategory = true
					bDetails = false
	
					strSqlSelectMain	=	strSqlSelectMain 	& 	"f.subtypeid, f.subtypename, "
					strSqlGroupBy		=	strSqlGroupBy		&	"f.subtypeid, f.subtypename, "
					strSqlOrderBy		=	strSqlOrderBy		&	"f.subtypeid, f.subtypename, "
					
					pageLevel = pageLevel & "d"
				End If
		End Select
	End If
Next

If bModel Then
	If not bBrand Then
		bBrand = true
		bDetails = false

		strSqlSelectMain	=	strSqlSelectMain 	& 	"c.brandid, c.brandName, "
		strSqlGroupBy		=	strSqlGroupBy		&	"c.brandid, c.brandName, "
		strSqlOrderBy		=	strSqlOrderBy		&	"c.brandid, c.brandName, "
		
		pageLevel = "b" & pageLevel
	end if
end if
				
if pageLevel = "" then pageLevel = "p"

if rdoTemplate = "A" then
	filename	=	"ADWORDS_AD_" & Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now) & ".xls"
else
	filename	=	"ADWORDS_KEYWORDS_" & Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now) & ".xls"
end if

Response.ContentType = "application/vnd.ms-excel"
'Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
Response.AddHeader "Content-Disposition","attachment;filename="""&filename&""""
'Response.Charset = "" 
'Me.EnableViewState = False

strSqlWhere = ""
strSqlWhere2 = ""
bUniversal = false
univTypes = ",8,1310,1320,1330,1350,1370,1395,"
if instr(univTypes, "," & typeid & ",") > 0 then bUniversal = true

if brandid <> 0 and not bUniversal then 
	strSqlWhere = strSqlWhere & "	and	a.brandid = " & brandid & vbcrlf
	strSqlWhere2 = strSqlWhere2 & "	and	a.brandid = " & brandid & vbcrlf
end if
if modelid <> 0 and not bUniversal then 
	strSqlWhere = strSqlWhere & "	and	a.modelid = " & modelid & vbcrlf
	strSqlWhere2 = strSqlWhere2 & "	and	a.modelid = " & modelid & vbcrlf
end if
if typeid > 0 and typeid < 1000 then 
	strSqlWhere = strSqlWhere & "	and	f.typeid = " & typeid & vbcrlf
	strSqlWhere2 = strSqlWhere2 & "	and	f.typeid = " & typeid & vbcrlf
end if
if typeid > 0 and typeid >= 1000 then 
	strSqlWhere = strSqlWhere & "	and	f.subtypeid = " & typeid & vbcrlf
	strSqlWhere2 = strSqlWhere2 & "	and	f.subtypeid = " & typeid & vbcrlf
end if
if itemid <> "" then 
	strSqlWhere = strSqlWhere & "	and	a.itemid = '" & itemid & "'" & vbcrlf
	strSqlWhere2 = strSqlWhere2 & "	and	e.itemid = " & itemid & vbcrlf
end if
if partnumber <> "" then 
	strSqlWhere = strSqlWhere & "	and	a.partnumber like '" & partnumber & "'" & vbcrlf
	strSqlWhere2 = strSqlWhere2 & "	and	e.partnumber like '" & partnumber & "'" & vbcrlf
end if
if strSDate <> "" then 
	strSqlWhere = strSqlWhere & "	and	(a.datetimeentd > dateadd(dd, -30, '" & strSDate & "') or a.invlastupdateddate > dateadd(dd, -30, '" & strSDate & "') or a.pricelastupdateddate > dateadd(dd, -30, '" & strSDate & "'))" & vbcrlf
	strSqlWhere2 = strSqlWhere2 & "	and	(e.datetimeentd > dateadd(dd, -30, '" & strSDate & "') or e.invlastupdateddate > dateadd(dd, -30, '" & strSDate & "') or e.pricelastupdateddate > dateadd(dd, -30, '" & strSDate & "'))" & vbcrlf
end if

if bDetails then
	sql	=	"select	distinct a.brandid, c.brandName, a.modelid, d.modelName, a.typeid, b." & siteTypename & " typeName, a.itemid, a.subtypeid, f.subtypename, a." & itemdesc & " itemdesc" & vbcrlf & _
			"from	we_items a join we_types b" & vbcrlf & _
			"	on	a.typeid = b.typeid join we_brands c" & vbcrlf & _
			"	on	a.brandid = c.brandid join we_models d" & vbcrlf & _
			"	on	a.modelid = d.modelid join v_subtypematrix f" & vbcrlf & _
			"	on	a.subtypeid = f.subtypeid" & vbcrlf & _
			"where	a.hidelive = 0 and a.inv_qty <> 0 and a." & priceDesc & " > 0 " & vbcrlf & _
			strSqlWhere & vbcrlf & _
			"union" & vbcrlf & _
			"-- related list" & vbcrlf & _
			"select	distinct a.brandid, c.brandName, a.modelid, d.modelName, a.typeid, b." & siteTypename & " typeName, e.itemid, e.subtypeid, f.subtypename, e." & itemdesc & " itemdesc" & vbcrlf & _
			"from	we_relateditems a join we_types b" & vbcrlf & _
			"	on	a.typeid = b.typeid join we_brands c" & vbcrlf & _
			"	on	a.brandid = c.brandid join we_models d" & vbcrlf & _
			"	on	a.modelid = d.modelid join we_items e" & vbcrlf & _
			"	on	a.itemid = e.itemid join v_subtypematrix f" & vbcrlf & _
			"	on	e.subtypeid = f.subtypeid" & vbcrlf & _
			"where	e.hidelive = 0 and e.inv_qty <> 0 and e." & priceDesc & " > 0 " & vbcrlf & _
			strSqlWhere2 & vbcrlf & _
			"union" & vbcrlf & _
			"-- sub-related list" & vbcrlf & _
			"select	distinct a.brandid, c.brandName, a.modelid, d.modelName, f.typeid, f.typeName, e.itemid, e.subtypeid, f.subtypename, e." & itemdesc & " itemdesc" & vbcrlf & _
			"from	we_subrelateditems a join we_brands c" & vbcrlf & _
			"	on	a.brandid = c.brandid join we_models d" & vbcrlf & _
			"	on	a.modelid = d.modelid join we_items e " & vbcrlf & _
			"	on	a.itemid = e.itemid join we_types b" & vbcrlf & _
			"	on	e.typeid = b.typeid join v_subtypematrix f" & vbcrlf & _
			"	on	a.subtypeid = f.subtypeid" & vbcrlf & _
			"where	e.hidelive = 0 and e.inv_qty <> 0 and e." & priceDesc & " > 0 " & vbcrlf & _
			strSqlWhere2 & vbcrlf

else
	strSqlSelectMain = "select " & left(trim(strSqlSelectMain), len(trim(strSqlSelectMain))-1)
	strSqlGroupBy	=	"group by " & left(trim(strSqlGroupBy), len(trim(strSqlGroupBy))-1)
	strSqlOrderBy	=	"order by " & left(trim(strSqlOrderBy), len(trim(strSqlOrderBy))-1)
	
	sql	=	strSqlSelectMain & vbcrlf & _
			"from	we_items a join we_types b" & vbcrlf & _
			"	on	a.typeid = b.typeid join we_brands c" & vbcrlf & _
			"	on	a.brandid = c.brandid join we_models d" & vbcrlf & _
			"	on	a.modelid = d.modelid join v_subtypematrix f" & vbcrlf & _
			"	on	a.subtypeid = f.subtypeid" & vbcrlf & _
			"where	a.hidelive = 0 and a.inv_qty <> 0 and a." & priceDesc & " > 0 " & vbcrlf & _
			strSqlWhere & vbcrlf & _
			strSqlGroupBy & vbcrlf & _
			"union" & vbcrlf & _
			"-- related list" & vbcrlf & _
			strSqlSelectMain & vbcrlf & _
			"from	we_relateditems a join we_types b" & vbcrlf & _
			"	on	a.typeid = b.typeid join we_brands c" & vbcrlf & _
			"	on	a.brandid = c.brandid join we_models d" & vbcrlf & _
			"	on	a.modelid = d.modelid join we_items e" & vbcrlf & _
			"	on	a.itemid = e.itemid join v_subtypematrix f" & vbcrlf & _
			"	on	e.subtypeid = f.subtypeid" & vbcrlf & _
			"where	e.hidelive = 0 and e.inv_qty <> 0 and e." & priceDesc & " > 0 " & vbcrlf & _
			strSqlWhere2 & vbcrlf & _
			strSqlGroupBy & vbcrlf & _
			"union" & vbcrlf & _
			"-- sub-related list" & vbcrlf & _
			strSqlSelectMain & vbcrlf & _
			"from	we_subrelateditems a join we_brands c" & vbcrlf & _
			"	on	a.brandid = c.brandid join we_models d" & vbcrlf & _
			"	on	a.modelid = d.modelid join we_items e " & vbcrlf & _
			"	on	a.itemid = e.itemid join we_types b" & vbcrlf & _
			"	on	e.typeid = b.typeid join v_subtypematrix f" & vbcrlf & _
			"	on	a.subtypeid = f.subtypeid" & vbcrlf & _
			"where	e.hidelive = 0 and e.inv_qty <> 0 and e." & priceDesc & " > 0 " & vbcrlf & _
			strSqlWhere2 & vbcrlf & _
			strSqlGroupBy & vbcrlf					
end if

set rs = oConn.execute(sql)
if rs.eof then
	response.write "No data to display"
else
	if rdoTemplate = "A" then
		response.write "Campaign" & vbTab
		response.write "Ad Group" & vbTab
		response.write "Headline" & vbTab
		response.write "Description Line1" & vbTab
		response.write "Description Line2" & vbTab
		response.write "Display URL" & vbTab
		response.write "Destination URL" & vbNewLine
	else
		response.write "Campaign" & vbTab
		response.write "Ad Group" & vbTab
		response.write "Keyword" & vbTab
		response.write "Criterion Type" & vbTab
		response.write "MAX CPC" & vbNewLine
	end if

	lap = 0

	criType = prepStr(arrHeading2(3))
	maxCPC = prepStr(arrHeading2(4))

	if criType <> "" then arrCriType = split(criType, "//")
	if maxCPC <> "" then arrMaxCPC = split(maxCPC, "//")
	
	do until rs.eof
		lap = lap + 1
		
		brandName = ""
		modelName = ""
		categoryName = ""
		subCategoryName = ""
		sqlBrandID = 0
		sqlModelID = 0
		sqlCategoryID = 0
		sqlSubcategoryID = 0
		sqlItemID = 0
		itemdesc = ""
		
		if bDetails then
			brandName = replace(rs("brandName"), "/", " / ")
			modelName = replace(rs("modelName"), "/", " / ")
			categoryName = replace(rs("typeName"), "/", " / ")
			subCategoryName = replace(rs("subtypename"), "/", " / ")
			sqlItemID = rs("itemid")
			itemdesc = rs("itemdesc")
		else
			if bBrand then 
				sqlBrandID = rs("brandid")
				brandName = replace(rs("brandName"), "/", " / ")
			end if
			if bModel then 
				sqlModelID = rs("modelid")
				modelName = replace(rs("modelName"), "/", " / ")
			end if
			if bCategory then 
				sqlCategoryID = rs("typeid")
				categoryName = replace(rs("typeName"), "/", " / ")
			end if
			if bSubcategory then 
				sqlSubcategoryID = rs("subtypeid")
				subCategoryName = replace(rs("subtypename"), "/", " / ")
			end if
		end if

		arrModelName = split(modelName, " / ")	'ex Fascinate i500 (Verizon) / Mesmerize (US Cellular) / Showcase (C-Spire)
		for m=0 to ubound(arrModelName)
			newModelName = trim(arrModelName(m))
			destURL = buildURL(pageLevel, siteid, sqlBrandID, sqlModelID, sqlCategoryID, sqlSubcategoryID, sqlItemID, brandName, newModelName, categoryName, subCategoryName, itemdesc)
	
			if rdoTemplate = "A" then
				campaign = performKeywordReplace(arrHeading1(0), brandName, newModelName, categoryName, subCategoryName)
				adGroup = performKeywordReplace(arrHeading1(1), brandName, newModelName, categoryName, subCategoryName)
				headline = performKeywordReplace(arrHeading1(2), brandName, newModelName, categoryName, subCategoryName)
				desc1 = performKeywordReplace(arrHeading1(3), brandName, newModelName, categoryName, subCategoryName)
				desc2 = performKeywordReplace(arrHeading1(4), brandName, newModelName, categoryName, subCategoryName)
				dispURL = performKeywordReplace(arrHeading1(5), brandName, newModelName, categoryName, subCategoryName)
				
				response.write campaign & vbTab
				response.write adGroup & vbTab
				response.write headline & vbTab
				response.write desc1 & vbTab
				response.write desc2 & vbTab
				response.write dispURL & vbTab
				response.write destURL & vbNewLine
			else
				campaign = performKeywordReplace(arrHeading2(0), brandName, newModelName, categoryName, subCategoryName)
				adGroup = performKeywordReplace(arrHeading2(1), brandName, newModelName, categoryName, subCategoryName)
				keyword = performKeywordReplace(arrHeading2(2), brandName, newModelName, categoryName, subCategoryName)
				
				if isarray(arrCriType) and isarray(arrMaxCPC) then
					if ubound(arrCriType) = ubound(arrMaxCPC) then
						for i=0 to ubound(arrCriType)
							response.write campaign & vbTab
							response.write adGroup & vbTab
							response.write keyword & vbTab
							response.write trim(arrCriType(i)) & vbTab
							response.write trim(arrMaxCPC(i)) & vbNewLine
						next
					else
						response.write campaign & vbTab
						response.write adGroup & vbTab
						response.write keyword & vbTab
						response.write trim(arrCriType(i)) & vbTab
						response.write trim(arrMaxCPC(i)) & vbNewLine
					end if
				else
					response.write campaign & vbTab
					response.write adGroup & vbTab
					response.write keyword & vbTab
					response.write trim(arrCriType(i)) & vbTab
					response.write trim(arrMaxCPC(i)) & vbNewLine
				end if
			end if
		next

		if (lap mod 500) = 0 then response.Flush()
		
		rs.movenext
	loop
end if


function buildURL(pageLevel, site_id, brandid, modelid, categoryid, subcategoryid, itemid, brandname, modelname, categoryname, subcategoryname, itemdesc)
	siteURL = ""
	select case site_id
		case 0 : siteURL = "http://www.wirelessemporium.com"
		case 1 : siteURL = "http://www.cellphoneaccents.com"
		case 2 : siteURL = "http://www.cellularoutfitter.com"
		case 3 : siteURL = "http://www.phonesale.com"
		case 10 : siteURL = "http://www.tabletmall.com"
	end select

	strLink = ""
	select case lcase(pageLevel)
		case "p"
			select case site_id
				case 0
					strLink = siteURL & "/p-" & itemid & "-" & formatSEO(itemdesc)
				case 1
					strLink = siteURL & "/p-" & itemid & "-" & formatSEO(itemdesc) & ".html"
				case 2
					strLink = siteURL & "/p-" & itemid & "-" & formatSEO(itemdesc) & ".html"
				case 3
					if categoryid = 16 then
						strLink = siteURL & "/" & formatSEO(brandname) & "/cell-phones/p-" & itemid+300001 & "-tc-" & categoryid & "-" & formatSEO(itemdesc) & ".html"
					else
						strLink = siteURL & "/accessories/" & formatSEO(categoryname) & "/" & formatSEO(itemdesc) & "-p-" & itemid+300001 & ".html"
					end if
				case 10
					strLink = siteURL & "/" & formatSEO(itemdesc) & "-p-" & itemid
			end select
		case "b"
			select case site_id
				case 0
					set oParam = CreateObject("Scripting.Dictionary")
					oParam.CompareMode = vbTextCompare
					oParam.Add "x_brandID", brandid
					oParam.Add "x_phoneOnly", 1
					strLink = siteURL & getMasterURL("b", brandid, oParam)
			end select
		case "c"
			select case site_id
				case 0
					strLink = siteURL & getMasterURL("c", categoryid, null)
			end select
		case "bc","cb"
			select case site_id
				case 0
					set oParam = CreateObject("Scripting.Dictionary")
					oParam.CompareMode = vbTextCompare
					oParam.Add "x_brandID", brandID
					oParam.Add "x_categoryID", categoryID
'					if brandid = 17 and phoneOnly = 1 then
'						oParam.Add "x_brandName", "apple-iphone"
'					elseif brandid = 17 and phoneOnly = 0 then
'						oParam.Add "x_brandName", "apple-ipod-ipad"
'					else
						oParam.Add "x_brandName", brandName	
'					end if
					oParam.Add "x_categoryName", categoryName	
					strLink = siteURL & getMasterURL("cb", "", oParam)
					'=========================================================================================
			end select
		case "bm","m"
			select case site_id
				case 0
					strLink = siteURL & "/T-" & modelid & "-cell-accessories-" & formatSEO(brandname) & "-" & formatSEO(modelName) & ".asp"
			end select
		case "bmc","mc"
			select case site_id
				case 0
					set oParam = CreateObject("Scripting.Dictionary")
					oParam.CompareMode = vbTextCompare
					oParam.Add "x_brandID", brandID
					oParam.Add "x_modelID", modelID
					oParam.Add "x_categoryID", categoryID
					oParam.Add "x_brandName", brandName
					oParam.Add "x_modelName", modelName
					oParam.Add "x_categoryName", categoryName
					strLink = siteURL & getMasterURL("bmc", "", oParam)
			end select
		case "mcd","bmcd","bmd"
			select case site_id
				case 0
					set oParam = CreateObject("Scripting.Dictionary")
					oParam.CompareMode = vbTextCompare
					oParam.Add "x_brandID", brandID
					oParam.Add "x_modelID", modelID
					oParam.Add "x_categoryID", subcategoryid
					oParam.Add "x_brandName", brandName
					oParam.Add "x_modelName", modelName
					oParam.Add "x_categoryName", subCategoryName
					strLink = siteURL & getMasterURL("bmcd", "", oParam)
			end select
	end select
	buildURL = strLink
end function

function performKeywordReplace(byval content, byval pBrandName, byval pModelName, byval pCategoryName, byval pSubcategoryName)
	if not isnull(content) then
		for i=0 to ubound(arrRules)
			if trim(arrRules(i)) <> "" then
				content = replace(content, trim(cfgArrRulesFrom(i)), trim(arrRules(i)))
			else
				keywordTo = "[keyword missing]"
				select case lcase(trim(arrCbRules(i)))
					case "brandname"
						if pBrandName <> "" then keywordTo = pBrandName
					case "modelname"
						if pModelName <> "" then keywordTo = pModelName
					case "categoryname"
						if pCategoryName <> "" then keywordTo = pCategoryName
					case "subcategoryname"
						if pSubcategoryName <> "" then keywordTo = pSubcategoryName
				end select
				
				content = replace(content, trim(cfgArrRulesFrom(i)), keywordTo)
			end if
		next
	end if
	performKeywordReplace = content
end function
%>