<html>
<head>
	<title>Wireless Emporium - Create New ASP Pages for the Reference Section</title>
</head>

<body>

<h3>
	Wireless Emporium&nbsp;&#151;
	<br>
	Create New ASP Pages (or Edit Existing ASP &amp; HTML Pages)<br>
	for the Reference Section
</h3>

<%
myFilename = request("editfile")

if request("submitted") <> "" then
	strError = ""
	strToWrite = ""
	myFilename = trim(request("myFilename"))
	myTitle = trim(request("myTitle"))
	myKeywords = trim(request("myKeywords"))
	myDescription = trim(request("myDescription"))
	myHeader = trim(request("myHeader"))
	HTMLcontent = trim(request("HTMLcontent"))
	
	if request("myaction") = "Add New" then
		if inStr(myFilename,".asp") = 0 then strError = strError & "You must enter an ASP filename for this file!<br>"
	else
		if inStr(myFilename,".asp") = 0 and inStr(myFilename,".html") = 0 then strError = strError & "You must enter an ASP or HTML filename for this file!<br>"
	end if
	if myTitle = "" then strError = strError & "You must enter a Title!<br>"
	'if myKeywords = "" then strError = strError & "You must enter some Keywords!<br>"
	'if myDescription = "" then strError = strError & "You must enter a Description!<br>"
	if myHeader = "" then strError = strError & "You must enter a Header!<br>"
	if HTMLcontent = "" then strError = strError & "You must enter some HTML content!<br>"
	
	if strError = "" then
		select case lCase(right(myFilename,4))
			case ".asp" : myReadFile = server.mappath("/references/template.asp")
			case "html" : myReadFile = server.mappath("/references/template.html")
		end select
		Set filesys = CreateObject("Scripting.FileSystemObject")
		Set readfile = filesys.OpenTextFile(myReadFile, 1)
		do while readfile.AtEndOfStream <> true
			strToWrite = strToWrite & readfile.readline & vbCrLf
		loop
		readfile.Close()
		
		myFile = server.mappath("/references/" & myFilename)
		'response.write "<p>" & myFile & "</p>" & vbCrLf
		Set writefile = filesys.CreateTextFile(myFile, true)
		strToWrite = replace(strToWrite,"#TITLE#",myTitle)
		strToWrite = replace(strToWrite,"#KEYWORDS#",myKeywords)
		strToWrite = replace(strToWrite,"#DESCRIPTION#",myDescription)
		strToWrite = replace(strToWrite,"#HEADER#",myHeader)
		strToWrite = replace(strToWrite,"#CONTENT#",HTMLcontent)
		'response.write strToWrite
		writefile.write strToWrite
		writefile.Close()
		
		response.write "<h3><font color=""#FF0000"">Updated!</font></h3>" & vbCrLf
		response.write "<p>See your new page <a href=""http://www.wirelessemporium.com/references/" & myFilename & """ target=""_blank"">here</a>.</p>" & vbCrLf
		response.write "<p>To add or edit another page, <a href=""http://www.wirelessemporium.com/admin/references_d.asp"">click here</a>.</p>" & vbCrLf
		response.write "</body></html>" & vbCrLf
		response.end
	end if
elseif myFilename <> "" then
	Set filesys = CreateObject("Scripting.FileSystemObject")
	myFile = server.mappath("/references/" & myFilename)
	set readfile = filesys.OpenTextFile(myFile, 1)
	do while readfile.AtEndOfStream <> true
		strToWrite = readfile.readline
		'if lCase(left(strToWrite,7)) = "<title>" then
		'	myTitle = replace(replace(strToWrite,"<TITLE>","",1,9,1),"</TITLE>","",1,9,1)
		'	myTitle = trim(replace(myTitle,chr(34),""))
		'elseif lCase(left(strToWrite,21)) = "<meta name=""keywords""" then
		'	myKeywords = replace(replace(strToWrite,"<META NAME=""Keywords"" CONTENT=","",1,9,1),""">","",1,9,1)
		'	myKeywords = trim(replace(myKeywords,chr(34),""))
		'elseif lCase(left(strToWrite,24)) = "<meta name=""description""" then
		'	myDescription = replace(replace(strToWrite,"<META NAME=""Description"" CONTENT=","",1,9,1),""">","",1,9,1)
		'	myDescription = trim(replace(myDescription,chr(34),""))
		if left(strToWrite,12) = "strMetaTitle" then
			myTitle = replace(replace(strToWrite,"strMetaTitle = ""<TITLE>","",1,9,1),"</TITLE>""","",1,9,1)
			myTitle = trim(replace(myTitle,chr(34),""))
		elseif left(strToWrite,15) = "strMetaKeywords" then
			myKeywords = replace(replace(strToWrite,"strMetaKeywords = ""<META NAME=""""Keywords"""" CONTENT=""","",1,9,1),""">","",1,9,1)
			myKeywords = trim(replace(myKeywords,chr(34),""))
		elseif left(strToWrite,18) = "strMetaDescription" then
			myDescription = replace(replace(strToWrite,"strMetaDescription = ""<META NAME=""""Description"""" CONTENT=""","",1,9,1),""">","",1,9,1)
			myDescription = trim(replace(myDescription,chr(34),""))
		elseif strToWrite = "<!--#START HTML HEADER#-->" then
			do until strToWrite = "<!--#END HTML HEADER#-->"
				strToWrite = readfile.readline
				if strToWrite <> "<!--#END HTML HEADER#-->" then myHeader = trim(replace(strToWrite,vbTab,""))
			loop
		elseif strToWrite = "<!--#START HTML CONTENT#-->" then
			HTMLcontent = ""
			do until strToWrite = "<!--#END HTML CONTENT#-->"
				strToWrite = readfile.readline
				if strToWrite <> "<!--#END HTML CONTENT#-->" then
					HTMLcontent = HTMLcontent & trim(replace(strToWrite,vbTab,"")) & vbCrLf
				end if
			loop
		end if
	loop
	readfile.Close()
elseif request("myaction") = "" then
	response.write "<form action='references_d.asp' 'name='frmEdit' method='post'>" & vbCrLf
	Set filesys = CreateObject("Scripting.FileSystemObject")
	Set demofolder = filesys.GetFolder(server.mappath("/references/"))
	Set filecoll = demofolder.Files
	For Each fil in filecoll
		if (right(fil.name,4) = ".asp" or right(fil.name,5) = ".html") and fil.name <> "template.asp" and fil.name <> "template.html" then
			filist = filist & "<input type='radio' name='editfile' value='" & fil.name & "'>" & fil.name & "<br>" & vbCrLf
		end if
	Next
	response.write filist & vbCrLf
	response.write "<p><input type='submit' name='myaction' value='Edit Selected'></p>" & vbCrLf
	response.write "</form>" & vbCrLf
	response.write "<form action='references_d.asp' name='frmAdd' method='post'>" & vbCrLf
	response.write "<p><input type='submit' name='myaction' value='Add New'></p>" & vbCrLf
	response.write "</form>" & vbCrLf
	response.write "</body></html>" & vbCrLf
	response.end
end if

if strError <> "" then
	%>
	<font color="#FF0000">
		<h3>ERROR!</h3>
		<p><%=strError%></p>
	</font>
	<%
end if
%>

<form action="references_d.asp" method="post">
	<input type="hidden" name="myaction2" value="<%=request("myaction")%>">
	<p><font color="#FF0000"><b>*&nbsp;</b></font>Filename:&nbsp;<input type="text" name="myFilename" size="100" value="<%=myFilename%>"></p>
	<p><font color="#FF0000"><b>*&nbsp;</b></font>META Title:&nbsp;<input type="text" name="myTitle" size="100" value="<%=myTitle%>"></p>
	<p>META Keywords:&nbsp;<input type="text" name="myKeywords" size="100" value="<%=myKeywords%>"></p>
	<p>META Description:&nbsp;<input type="text" name="myDescription" size="100" value="<%=myDescription%>"></p>
	<p><font color="#FF0000"><b>*&nbsp;</b></font>Header:&nbsp;<input type="text" name="myHeader" size="100" value="<%=myHeader%>"></p>
	<p>
		<font color="#FF0000"><b>*&nbsp;</b></font>HTML&nbsp;Content:
		<br>
		<textarea name="HTMLcontent" rows="20" cols="100"><%=HTMLcontent%></textarea>
	</p>
	<p><input type="submit" name="submitted" value="Submit"></p>
</form>

</body>
</html>
