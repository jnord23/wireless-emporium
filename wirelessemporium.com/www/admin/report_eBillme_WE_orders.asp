<%
response.buffer = false
pageTitle = "Create WE Orders CSV Report for eBillme"
header = 1
Server.ScriptTimeout = 1000 'seconds
%>

<!--#include virtual="/includes/admin/admin_top.asp"-->

<h3>WE Orders CSV Report for eBillme</h3>

<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
	<p><input type="submit" name="submitted" value="Create Report"></p>
</form>

<%
if request.form("submitted") <> "" then
	dim fs, file, filename, path
	'Map the file name to the physical path on the server.
	Set fs = CreateObject("Scripting.FileSystemObject")
	filename = "eBillme_WE_orders.csv"
	path = Server.MapPath("tempCSV") & "\" & filename
	path = replace(path,"admin\","")
	
	if fs.FileExists(path) then fs.DeleteFile(path)
	
	myDate = dateAdd("m",-1,date)
	dateStart = dateValue(month(myDate) & "/1/" & year(myDate))
	myDate = dateAdd("m",1,dateStart)
	dateEnd = dateAdd("d",-1,myDate)
	
	Set file = fs.CreateTextFile(path)
	SQL = "SELECT orderid,accountid,shippingid,shiptype,couponid,trackingNum,orderdatetime,approved,emailSent,confirmSent FROM we_orders"
	SQL = SQL & " WHERE confirmSent = 'yes' AND orderdatetime >= '" & dateStart & "' AND orderdatetime <= '" & dateEnd & "'"
	SQL = SQL & " AND store = 0"
	SQL = SQL & " AND parentorderid is null"	
	SQL = SQL & " ORDER BY orderdatetime"
	response.write "<h3>" & SQL & "</h3>" & vbcrlf
	Set RS = Server.CreateObject("ADODB.Recordset")
	RS.Open SQL, oConn, 3, 3
	if not RS.EOF then
		file.WriteLine "orderid,accountid,shippingid,shiptype,couponid,trackingNum,shiplogdatetime,approved,emailSent,confirmSent"
		do until RS.EOF
			strline = chr(34) & RS("orderid") & chr(34) & ","
			strline = strline & chr(34) & RS("accountid") & chr(34) & ","
			strline = strline & chr(34) & RS("shippingid") & chr(34) & ","
			strline = strline & chr(34) & RS("shiptype") & chr(34) & ","
			strline = strline & chr(34) & RS("couponid") & chr(34) & ","
			strline = strline & chr(34) & RS("trackingNum") & chr(34) & ","
			strline = strline & chr(34) & RS("orderdatetime") & chr(34) & ","
			strline = strline & chr(34) & RS("approved") & chr(34) & ","
			strline = strline & chr(34) & RS("emailSent") & chr(34) & ","
			strline = strline & chr(34) & RS("confirmSent") & chr(34)
			file.WriteLine strline
			RS.MoveNext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
	response.write "<p>Here is the <a href=""/tempCSV/" & filename & """>file</a></p>" & vbcrlf
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->

