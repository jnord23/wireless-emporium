<%
pageTitle = "Admin - Update WE Database - Update Coupons"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<blockquote>
<blockquote>

<h3>Update WE Database - Update Coupons</h3>

<%
dim siteID : siteID = prepInt(request.Form("siteID"))

strError = ""
if request.form("submitted") = "Update" then
	for a = 1 to request.form("totalCount")
		promoCode = SQLquote(request.form("promoCode" & a))
		promoMin = request.form("promoMin" & a)
		promoPercent = replace(request.form("promoPercent" & a),"%","")
		TypeID = prepInt(request.form("TypeID" & a))
		FreeItemPartNumber = request.form("FreeItemPartNumber" & a)
		excludeBluetooth = request.form("excludeBluetooth" & a)
		if excludeBluetooth = "" then excludeBluetooth = "0"
		BOGO = request.form("BOGO" & a)
		if BOGO = "" then BOGO = "0"
		expiration = request.form("expiration" & a)
		if promoCode = "" then strError = strError & "You must enter a Promo Code for coupon #" & a & ".<br>"
		if not isNumeric(promoMin) then strError = strError & "You must enter valid Promo Min for coupon #" & a & ".<br>"
		if not isNumeric(promoPercent) then strError = strError & "You must enter valid Promo Percent for coupon #" & a & ".<br>"
		if not isDate(expiration) then strError = strError & "You must enter valid Expiration Date for coupon #" & a & ".<br>"
		
		chkGoogle = prepInt(request.form("chkGoogle" & a))		
		gcouponid = prepInt(request.form("g_couponid" & a))
		promotion_id = prepStr(request.form("g_promotion_id" & a))
		product_applicability = prepStr(request.form("g_product_applicability" & a))
		short_title = prepStr(request.form("g_short_title" & a))
		long_title = prepStr(request.form("g_long_title" & a))
		promotion_effective_date = prepStr(request.form("g_promotion_effective_date" & a))
		redemption_channel = prepStr(request.form("g_redemption_channel" & a))
		merchant_logo = prepStr(request.form("g_merchant_logo" & a))
		offer_type = prepStr(request.form("g_offer_type" & a))
		siteid = prepInt(request.form("siteid" & a))
		if chkGoogle > 0 then
			if promotion_id = "" then strError = strError & "You must enter valid google promotion_id" & a & ".<br>"
			if product_applicability = "" then strError = strError & "You must enter valid google product_applicability" & a & ".<br>"
			if short_title = "" then strError = strError & "You must enter valid google short_title" & a & ".<br>"
			if long_title = "" then strError = strError & "You must enter valid google long_title" & a & ".<br>"
			if promotion_effective_date = "" then strError = strError & "You must enter valid google promotion_effective_date" & a & ".<br>"
			if redemption_channel = "" then strError = strError & "You must enter valid google redemption_channel" & a & ".<br>"
			if merchant_logo = "" then strError = strError & "You must enter valid google merchant_logo" & a & ".<br>"
			if offer_type = "" then strError = strError & "You must enter valid google offer_type" & a & ".<br>"
			if TypeID = 0 then strError = strError & "Category should be assigned when added to google feed" & a & ".<br>"
		end if
		
		if strError = "" then
			SQL = "UPDATE we_coupons SET"
			SQL = SQL & " promoCode='" & promoCode & "',"
			SQL = SQL & " promoMin='" & promoMin & "',"
			SQL = SQL & " promoPercent='" & promoPercent/100 & "',"
			SQL = SQL & " TypeID='" & TypeID & "',"
			SQL = SQL & " excludeBluetooth=" & excludeBluetooth & ","
			SQL = SQL & " BOGO=" & BOGO & ","
			if FreeItemPartNumber = "" then
				SQL = SQL & " FreeItemPartNumber=null,"
			else
				SQL = SQL & " FreeItemPartNumber='" & FreeItemPartNumber & "',"
			end if
			SQL = SQL & " couponDesc='" & SQLquote(request.form("couponDesc" & a)) & "',"
			SQL = SQL & " expiration='" & dateValue(expiration) & "',"
			SQL = SQL & " activate=" & request.form("activate" & a)
			SQL = SQL & " WHERE couponid='" & request.form("couponid" & a) & "'"
			session("errorSQL") = sql
			session("errorSQL2") = "desc:" & request.form("couponDesc" & a) & "<br />" & sql
			oConn.execute SQL
			
			if chkGoogle > 0 then
				if gcouponid > 0 then
					sql	=	"update	gcoupons" & vbcrlf & _
							"set	promotion_id = '" & promotion_id & "'" & vbcrlf & _
							"	,	long_title = '" & long_title & "'" & vbcrlf & _
							"	,	short_title = '" & short_title & "'" & vbcrlf & _
							"	,	product_applicability = '" & product_applicability & "'" & vbcrlf & _
							"	,	promotion_effective_date = '" & promotion_effective_date & "'" & vbcrlf & _
							"	,	redemption_channel = '" & redemption_channel & "'" & vbcrlf & _
							"	,	merchant_logo = '" & merchant_logo & "'" & vbcrlf & _
							"	,	offer_type = '" & offer_type & "'" & vbcrlf & _
							"where	id = " & gcouponid & vbcrlf
				else
					sql	=	"insert into gcoupons(siteid, promotion_id, product_applicability, long_title, short_title, promotion_effective_date, redemption_channel, merchant_logo, offer_type, couponid)" & vbcrlf & _
							"values(" & siteid & ", '" & promotion_id & "', '" & product_applicability & "', '" & long_title & "', '" & short_title & "', '" & promotion_effective_date & "', '" & redemption_channel & "', '" & merchant_logo & "', '" & offer_type & "', " & request.form("couponid" & a) & ")" & vbcrlf
				end if
'				response.write "<p>" & SQL & "</p>"
				oConn.execute SQL
			else
				if gcouponid > 0 then
					sql	=	"delete" & vbcrlf & _
							"from	gcoupons" & vbcrlf & _
							"where	id = " & gcouponid
'					response.write "<p>" & SQL & "</p>"
					oConn.execute SQL
				end if
			end if
		end if
	next

	if strError = "" and request.form("promoCodeNEW") <> "" then
		promoCode = SQLquote(request.form("promoCodeNEW"))
		promoMin = request.form("promoMinNEW")
		promoPercent = replace(request.form("promoPercentNEW"),"%","")
		TypeID = prepInt(request.form("TypeIDNEW"))
		FreeItemPartNumber = request.form("FreeItemPartNumberNEW")
		excludeBluetooth = request.form("excludeBluetoothNEW")
		if excludeBluetooth = "" then excludeBluetooth = "0"
		BOGO = request.form("BOGONEW")
		if BOGO = "" then BOGO = "0"
		expiration = request.form("expirationNEW")
		activate = request.form("activateNEW")
		if promoCode = "" then strError = strError & "You must enter a Promo Code for the NEW coupon.<br>"
		if not isNumeric(promoMin) then strError = strError & "You must enter valid Promo Min for the NEW coupon.<br>"
		if not isNumeric(promoPercent) then strError = strError & "You must enter valid Promo Percent for the NEW coupon.<br>"
		if not isDate(expiration) then strError = strError & "You must enter valid Expiration Date for the NEW coupon.<br>"
		
		chkGoogleNEW = prepInt(request.form("chkGoogleNEW"))
		if chkGoogleNEW > 0 then
			g_promotion_idNEW = prepStr(request.form("g_promotion_idNEW"))
			g_short_titleNEW = prepStr(request.form("g_short_titleNEW"))
			g_long_titleNEW = prepStr(request.form("g_long_titleNEW"))
			g_product_applicabilityNEW = prepStr(request.form("g_product_applicabilityNEW"))
			g_promotion_effective_dateNEW = prepStr(request.form("g_promotion_effective_dateNEW"))
			g_redemption_channelNEW = prepStr(request.form("g_redemption_channelNEW"))
			g_merchant_logoNEW = prepStr(request.form("g_merchant_logoNEW"))
			g_offer_typeNEW = prepStr(request.form("g_offer_typeNEW"))
	
			if g_promotion_idNEW = "" then strError = strError & "You must enter new valid google promotion_id.<br>"
			if g_product_applicabilityNEW = "" then strError = strError & "You must enter new valid google product_applicability.<br>"
			if g_short_titleNEW = "" then strError = strError & "You must enter new valid google short_title.<br>"
			if g_long_titleNEW = "" then strError = strError & "You must enter new valid google long_title.<br>"
			if g_promotion_effective_dateNEW = "" then strError = strError & "You must enter new valid google promotion_effective_date.<br>"
			if g_redemption_channelNEW = "" then strError = strError & "You must enter new valid google redemption_channel.<br>"
			if g_merchant_logoNEW = "" then strError = strError & "You must enter new valid google merchant_logo.<br>"
			if g_offer_typeNEW = "" then strError = strError & "You must enter new valid google offer_type.<br>"
			if TypeID = 0 then strError = strError & "Category should be assigned when added to google feed.<br>"
			
			sql	=	"select	id from gcoupons where promotion_id = '" & g_promotion_idNEW & "' and siteid = '" & siteID & "'"
			set rs = oConn.execute(sql)
			if not rs.eof then strError = strError & "New google promotion id is duplicated.<br>"
		end if
		
		if strError = "" then
			SQL = "SET NOCOUNT ON; INSERT INTO we_coupons (promoCode,promoMin,promoPercent,TypeID,excludeBluetooth,BOGO,FreeItemPartNumber,couponDesc,expiration,activate,siteID)"
			SQL = SQL & " VALUES ("
			SQL = SQL & "'" & promoCode & "',"
			SQL = SQL & "'" & promoMin & "',"
			SQL = SQL & "'" & promoPercent/100 & "',"
			SQL = SQL & "'" & TypeID & "',"
			SQL = SQL & excludeBluetooth & ","
			SQL = SQL & BOGO & ","
			if FreeItemPartNumber = "" then
				SQL = SQL & "null,"
			else
				SQL = SQL & "'" & FreeItemPartNumber & "',"
			end if
			SQL = SQL & "'" & SQLquote(request.form("couponDescNEW")) & "',"
			SQL = SQL & "'" & dateValue(expiration) & "',"
			SQL = SQL & request.form("activateNEW") & "," & siteID & ")"
			SQL = SQL & "; SELECT @@identity couponid;"
			session("errorSQL") = sql
			set newCouponRS = oConn.execute(sql)

			if not newCouponRS.eof and chkGoogleNEW > 0 then
				sql	=	"insert into gcoupons(siteid, promotion_id, product_applicability, long_title, short_title, promotion_effective_date, redemption_channel, merchant_logo, offer_type, couponid)" & vbcrlf & _
						"values(" & siteID & ", '" & g_promotion_idNEW & "', '" & g_product_applicabilityNEW & "', '" & g_long_titleNEW & "', '" & g_short_titleNEW & "', '" & g_promotion_effective_dateNEW & "', '" & g_redemption_channelNEW & "', '" & g_merchant_logoNEW & "', '" & g_offer_typeNEW & "', " & newCouponRS("couponid") & ")" & vbcrlf
'				response.write "<p>" & SQL & "</p>"
				oConn.execute SQL
			end if
		end if
	end if
	if strError <> "" then 
		response.write "<p>" & strError & "</p>" & vbcrlf
		response.write "<p><a href=""/admin/db_update_coupons.asp"">Back to coupons</a></p>" & vbcrlf
	else
		response.write "<h3>UPDATED!</h3>" & vbcrlf
		response.write "<p><a href=""/admin/db_update_coupons.asp"">Back to coupons</a></p>" & vbcrlf
	end if
elseif request.form("sortpage") <> "" then
	sql = 	"select	typeid, typename, 1 tableid " & vbcrlf & _
			"from	we_types " & vbcrlf & _
			"union" & vbcrlf & _
			"select	-1 typeid, '--------------------------------', 2 tableid" & vbcrlf & _
			"union" & vbcrlf & _
			"select	subtypeid typeid, subtypename typename, 3 tableid " & vbcrlf & _
			"from	we_subtypes " & vbcrlf & _
			"order by tableid, typename"
	session("errorSQL") = sql
	set typeRS = oConn.execute(sql)
	
	prodTypes = ""
	do while not typeRS.EOF
		prodTypes = prodTypes & typeRS("typeID") & "##" & typeRS("typeName") & "$$"
		typeRS.movenext
	loop
	typeRS = null
	typeArray = split(prodTypes,"$$")
	
	SQL = 	"select	a.couponid, a.promoCode, a.promoMin, a.promoPercent, a.typeID, a.excludeBluetooth, a.BOGO, a.FreeItemPartNumber, a.couponDesc, a.expiration, a.activate, a.onetime, a.cashback, a.setValue, a.siteid" & vbcrlf & _
			"	,	isnull(b.id, 0) gcouponid, isnull(b.promotion_id, '') promotion_id, isnull(b.long_title, '') long_title, isnull(b.short_title, '') short_title" & vbcrlf & _
			"	,	isnull(b.product_applicability, 'SPECIFIC_PRODUCTS') product_applicability" & vbcrlf & _
			"	,	convert(varchar(10), isnull(b.promotion_effective_date, getdate()), 20) promotion_effective_date" & vbcrlf & _
			"	,	isnull(b.redemption_channel, 'ONLINE') redemption_channel" & vbcrlf & _
			"	,	isnull(b.merchant_logo, 'https://www.wirelessemporium.com/images/we-google-logo-mini.jpg') merchant_logo" & vbcrlf & _
			"	,	isnull(b.offer_type, 'GENERIC_CODE') offer_type" & vbcrlf & _
			"from	we_coupons a with (nolock) left outer join gcoupons b with (nolock)" & vbcrlf & _
			"	on	a.couponid = b.couponid and b.siteid = 0" & vbcrlf
	if request.form("Expired") = "0" then
		SQL = SQL & " where	a.expiration >= '" & date & "'" & vbcrlf
	else
		SQL = SQL & " where	a.expiration < '" & date & "'" & vbcrlf
	end if
	SQL = SQL & " and a.oneTime = 0 and a.cashBack = 0 and a.siteID = " & siteID & vbcrlf
	if request.form("SortBy") = "0" then
		SQL = SQL & " order by a.promoCode, a.couponid" & vbcrlf
	else
		SQL = SQL & " order by a.couponDesc, a.promoCode, a.couponid" & vbcrlf
	end if
'	response.write "<pre>" & sql & "</pre>"
	
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	a = 0
	if RS.eof then
		response.write "<h3>No coupons found with current criteria</h3>"
		response.write "<a href=""db_update_coupons.asp"">Try again!</a>"
	end if
	%>
	<table border="1" cellpadding="3" cellspacing="0" align="center" style="width:1000px;">
		<form action="db_update_coupons.asp" method="post">
			<%
			do until RS.eof
				couponid = RS("couponid")
				promoCode = RS("promoCode")
				promoMin = RS("promoMin")
				if isnull(RS("promoPercent")) then
					promoPercent = 0
				else
					promoPercent = RS("promoPercent")					
				end if
				typeID = RS("typeID")
				FreeItemPartNumber = RS("FreeItemPartNumber")
				excludeBluetooth = RS("excludeBluetooth")
				if isNull(excludeBluetooth) then excludeBluetooth = 0
				BOGO = RS("BOGO")
				if isNull(BOGO) then BOGO = 0
				couponDesc = RS("couponDesc")
				expiration = RS("expiration")
				activate = RS("activate")
				
				'====================== google variables
				gcouponid = rs("gcouponid")
				promotion_id = rs("promotion_id")
				long_title = rs("long_title")
				short_title = rs("short_title")
				product_applicability = rs("product_applicability")
				promotion_effective_date = rs("promotion_effective_date")
				redemption_channel = rs("redemption_channel")
				merchant_logo = rs("merchant_logo")
				offer_type = rs("offer_type")
				siteid = rs("siteid")
				
				a = a + 1
				%>
				<tr>
					<td valign="top" align="center"><b>Promo&nbsp;Code:</b><br><input type="text" name="promoCode<%=a%>" value="<%=promoCode%>" size="15" maxlength="20"></td>
					<td valign="top" align="left" colspan="6">&nbsp;<b>Description:</b><br>&nbsp;<input type="text" name="couponDesc<%=a%>" value="<%=couponDesc%>" size="80" maxlength="200"></td>
					<td valign="top" align="center"><b>Activate:</b><br>
						<input type="hidden" name="couponid<%=a%>" value="<%=couponid%>">
						<input type="radio" name="activate<%=a%>" value="-1"<%if activate = true then response.write " checked"%>>Y&nbsp;
						<input type="radio" name="activate<%=a%>" value="0"<%if activate = false then response.write " checked"%>>N
					</td>
				</tr>
				<tr>
					<td valign="top" align="center"><input id="id_chkGoogle<%=a%>" name="chkGoogle<%=a%>" type="checkbox" value="1" onclick="onAddGoogle(<%=a%>)" <%if gcouponid > 0 then%>checked<%end if%> /> <label style="cursor:pointer;" for="id_chkGoogle<%=a%>">Add to Google Feeds?</label></td>
					<td valign="top" align="center">
						<b>Category:</b><br />
						<select name="TypeID<%=a%>">
							<option value="0">None Selected</option>
							<%
							for i = 0 to (ubound(typeArray) - 1)
								curArray = split(typeArray(i),"##")
							%>
							<option value="<%=curArray(0)%>"<% if cdbl(typeID) = cdbl(curArray(0)) then %> selected="selected"<% end if %>><%=curArray(1)%></option>
							<%
							next
							%>
						</select>
					</td>
					<td valign="top" align="center"><b>Free&nbsp;Item&nbsp;#:</b><br><input type="text" name="FreeItemPartNumber<%=a%>" value="<%=FreeItemPartNumber%>" size="8" maxlength="15"></td>
					<td valign="top" align="center"><b>Promo&nbsp;Min:</b><br><input type="text" name="promoMin<%=a%>" value="<%=promoMin%>" size="4" maxlength="4"></td>
					<td valign="top" align="center"><b>Promo&nbsp;%:</b><br><input type="text" name="promoPercent<%=a%>" value="<%=formatPercent(promoPercent,0)%>" size="4" maxlength="4"></td>
					<td valign="top" align="center"><b>excl.&nbsp;Bluetooth:</b><br>
						<input type="radio" name="excludeBluetooth<%=a%>" value="-1"<%if excludeBluetooth = true then response.write " checked"%>>Y&nbsp;
						<input type="radio" name="excludeBluetooth<%=a%>" value="0"<%if excludeBluetooth = false then response.write " checked"%>>N
					</td>
					<td valign="top" align="center"><b>BOGO:</b><br>
						<input type="radio" name="BOGO<%=a%>" value="-1"<%if BOGO = true then response.write " checked"%>>Y&nbsp;
						<input type="radio" name="BOGO<%=a%>" value="0"<%if BOGO = false then response.write " checked"%>>N
					</td>
					<td valign="top" align="center"><b>Expiration:</b><br><input type="text" name="expiration<%=a%>" value="<%=expiration%>" size="10" maxlength="10"></td>
				</tr>
                <tr>
                    <td id="gpromo_<%=a%>" align="left" colspan="8" style="<%if gcouponid = 0 then%>display:none;<%end if%>">
                        <table border="1" cellpadding="3" cellspacing="0" align="center" style="font-size:12px; border-collapse:collapse; width:100%;">
                            <tr>
                                <td style="background-color:#f2f2f2;">Google Promotion Desc.</td>
                                <td style="background-color:#f2f2f2;">Promotion ID</td>
                                <td style="background-color:#f2f2f2;">Short Title /<br />Long Title</td>
                                <td style="background-color:#f2f2f2;">Product Applicability</td>
                                <td style="background-color:#f2f2f2;">Promotion Effective Date</td>
                                <td style="background-color:#f2f2f2;">Redemption Channel</td>
                                <td style="background-color:#f2f2f2;">Merchant Logo</td>
                                <td style="background-color:#f2f2f2;">Offer Type</td>
                                <td style="background-color:#f2f2f2;">Generic Redemption Code</td>
                            </tr>
                            <tr>
                                <td style="background-color:#f2f2f2;">Values</td>
                                <td><input type="text" name="g_promotion_id<%=a%>" value="<%=promotion_id%>" style="width:100px;" /></td>
                                <td>
                                    <input type="text" name="g_short_title<%=a%>" value="<%=short_title%>" style="width:200px;" /><br />
                                    <input type="text" name="g_long_title<%=a%>" value="<%=long_title%>" style="width:200px;" />
                                </td>
                                <td><input type="text" name="g_product_applicability<%=a%>" value="<%=product_applicability%>" style="width:80px;" /></td>
                                <td><input type="text" name="g_promotion_effective_date<%=a%>" value="<%=promotion_effective_date%>" style="width:80px;" /></td>
                                <td><input type="text" name="g_redemption_channel<%=a%>" value="<%=redemption_channel%>" style="width:80px;" /></td>
                                <td><input type="text" name="g_merchant_logo<%=a%>" value="<%=merchant_logo%>" style="width:80px;" /></td>
                                <td><input type="text" name="g_offer_type<%=a%>" value="<%=offer_type%>" style="width:80px;" /></td>
                                <td>
                                    <%=promoCode%>
                                    <input type="hidden" name="g_couponid<%=a%>" value="<%=gcouponid%>" />
                                    <input type="hidden" name="siteid<%=a%>" value="<%=siteid%>" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>                
				<tr>
					<td align="center" colspan="8" bgcolor="#999999"></td>
				</tr>
				<%
				RS.movenext
			loop
			%>
			<tr>
				<td align="left" colspan="8"><font color="#FF0000"><b>ADD NEW COUPON</b></font></td>
			</tr>
			<tr>
				<td valign="top" align="center"><b>Promo&nbsp;Code:</b><br><input type="text" name="promoCodeNEW" value="" size="15" maxlength="20"></td>
				<td valign="top" align="left" colspan="6">&nbsp;<b>Description:</b><br>&nbsp;<input type="text" name="couponDescNEW" value="" size="80" maxlength="200"></td>
				<td valign="top" align="center"><b>Activate:</b><br>
					<input type="radio" name="activateNEW" value="-1" checked>Y&nbsp;
					<input type="radio" name="activateNEW" value="0">N
				</td>
			</tr>
			<tr>
				<td valign="top" align="center"><input id="id_chkGoogleNEW" name="chkGoogleNEW" type="checkbox" value="1" onclick="onAddGoogle('NEW')" /> <label style="cursor:pointer;" for="id_chkGoogleNEW">Add to Google Feeds?</label></td>
				<td valign="top" align="center">
					<b>Category:</b><br>
					<select name="TypeIDNEW">
						<option value="0">None Selected</option>
						<%
						for i = 0 to (ubound(typeArray) - 1)
							curArray = split(typeArray(i),"##")
						%>
						<option value="<%=curArray(0)%>"<% if cdbl(typeID) = cdbl(curArray(0)) then %> selected="selected"<% end if %>><%=curArray(1)%></option>
						<%
						next
						%>
					</select>
				</td>
				<td valign="top" align="center"><b>Free&nbsp;Item&nbsp;Part#:</b><br><input type="text" name="FreeItemPartNumberNEW" value="" size="8" maxlength="15"></td>
				<td valign="top" align="center"><b>Promo&nbsp;Min:</b><br><input type="text" name="promoMinNEW" value="" size="4" maxlength="4"></td>
				<td valign="top" align="center"><b>Promo&nbsp;%:</b><br><input type="text" name="promoPercentNEW" value="" size="4" maxlength="4"></td>
				<td valign="top" align="center"><b>excl.&nbsp;Bluetooth:</b><br>
					<input type="radio" name="excludeBluetoothNEW" value="-1">Y&nbsp;
					<input type="radio" name="excludeBluetoothNEW" value="0" checked>N
				</td>
				<td valign="top" align="center"><b>BOGO:</b><br>
					<input type="radio" name="BOGONEW" value="-1">Y&nbsp;
					<input type="radio" name="BOGONEW" value="0" checked>N
				</td>
				<td valign="top" align="center"><b>Expiration:</b><br><input type="text" name="expirationNEW" value="" size="10" maxlength="10"></td>
			</tr>
			<tr>
                <td id="gpromo_NEW" colspan="8" style="display:none;">
                    <table border="1" cellpadding="3" cellspacing="0" align="center" style="font-size:12px; border-collapse:collapse; width:100%;">
                        <tr>
                            <td style="background-color:#f2f2f2;">Google Promotion Desc.</td>
                            <td style="background-color:#f2f2f2;">Promotion ID</td>
                            <td style="background-color:#f2f2f2;">Short Title /<br />Long Title</td>
                            <td style="background-color:#f2f2f2;">Product Applicability</td>
                            <td style="background-color:#f2f2f2;">Promotion Effective Date</td>
                            <td style="background-color:#f2f2f2;">Redemption Channel</td>
                            <td style="background-color:#f2f2f2;">Merchant Logo</td>
                            <td style="background-color:#f2f2f2;">Offer Type</td>
                        </tr>
                        <tr>
                            <td style="background-color:#f2f2f2;">Values</td>
                            <td><input type="text" name="g_promotion_idNEW" value="" style="width:100px;" /></td>
                            <td>
                                <input type="text" name="g_short_titleNEW" value="" style="width:200px;" /><br />
                                <input type="text" name="g_long_titleNEW" value="" style="width:200px;" />
                            </td>
                            <td><input type="text" name="g_product_applicabilityNEW" value="SPECIFIC_PRODUCTS" style="width:80px;" /></td>
                            <td><input type="text" name="g_promotion_effective_dateNEW" value="<%=date%>" style="width:80px;" /></td>
                            <td><input type="text" name="g_redemption_channelNEW" value="ONLINE" style="width:80px;" /></td>
                            <td><input type="text" name="g_merchant_logoNEW" value="https://www.wirelessemporium.com/images/we-google-logo-mini.jpg" style="width:80px;" /></td>
                            <td>
                                <input type="text" name="g_offer_typeNEW" value="GENERIC_CODE" style="width:80px;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>            
			<tr>
				<td align="center" colspan="8">
					<input type="hidden" name="totalCount" value="<%=a%>">
                    <input type="hidden" name="siteID" value="<%=siteID%>">
					<input type="submit" name="submitted" value="Update">
				</td>
			</tr>
		</form>
	</table>
	<%
else
	%>
	<table border="1" cellpadding="3" cellspacing="0" align="center" width="300" class="normalText">
		<form action="db_update_coupons.asp" method="post">
			<tr>
				<td>
					<p><b>Sort By:</b></p>
                    <p>
                    	Website:
                        <select name="siteID">
                        	<option value="0">Wireless Emporium</option>
                            <option value="10">Tablet Mall</option>
                        </select>
                    </p>
					<p><input type="radio" name="SortBy" value="0" checked>Promo Code&nbsp;&nbsp;&nbsp;<input type="radio" name="SortBy" value="1">Description</p>
					<p><input type="radio" name="Expired" value="0" checked>Not Expired&nbsp;&nbsp;&nbsp;<input type="radio" name="Expired" value="1">Expired</p>
					<p><input type="submit" name="sortpage" value="Search"></p>
				</td>
			</tr>
		</form>
	</table>
	<%
end if
%>

<p>&nbsp;</p>

</blockquote>
</blockquote>
<script>
	function onAddGoogle(idx) {
		if (document.getElementById('id_chkGoogle'+idx).checked) {
			document.getElementById('gpromo_'+idx).style.display = '';
		} else {
			document.getElementById('gpromo_'+idx).style.display = 'none';
		}
	}
</script>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
