<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Product Look Up"
	header = 1
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	dim sql, brandArray
	
	session("orderBy") = null
	
	sql = "select brandID, brandName from we_brands order by brandName"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	sql = "select typeID, typeName from we_types order by typeName"
	session("errorSQL") = sql
	Set catRS = Server.CreateObject("ADODB.Recordset")
	catRS.open sql, oConn, 0, 1
	
	brandArray = ""
	do while not rs.EOF
		brandArray = brandArray & rs("brandID") & "#" & rs("brandName") & "$"
		rs.movenext
	loop
	brandArray = split(brandArray,"$")
%>
<form style="margin:0px;" action="/admin/createSlave.asp" method="post" name="slaveForm" onsubmit="return(testForm(this))">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="600">
	<tr>
    	<td><h1 style="margin:0px;">Product Look Up</h1></td>
    </tr>
    <tr>
    	<td style="font-size:10px; padding-bottom:10px;">
        	This application allows the user to quickly look up product details.
        </td>
    </tr>
    <tr>
    	<td style="border-top:1px solid #999; border-bottom:1px solid #999; font-weight:bold; font-size:16px; background-color:#000; color:#FFF; padding-left:10px;">Select Product By Brand/Model</td>
    </tr>
    <tr>
    	<td id="brandBox" style="padding-top:5px;">
        	<div style="width:600px;">
	        	<div style="float:left; width:140px;">Select Brand:</div>
    	        <div style="float:left; padding-left:10px; width:450px;">
	    	    	<select name="brand" onchange="brandSelect(this.value)">
    	    	    	<option value="">Select Brand</option>
        	    	    <%
						for i = 0 to (ubound(brandArray) - 1)
							useArray = split(brandArray(i),"#")
						%>
		                <option value="<%=useArray(0)%>"><%=useArray(1)%></option>
    		            <%
						next
						%>
		            </select>
    	        </div>
            </div>
        </td>
    </tr>
    <tr>
        <td id="modelBox" style="padding-top:5px;"></td>
    </tr>
    <tr>
        <td id="catBox" style="padding-top:5px;"></td>
    </tr>
    <tr>
    	<td style="border-top:1px solid #999; border-bottom:1px solid #999; font-weight:bold; font-size:16px; background-color:#000; color:#FFF; padding-left:10px;">Other Search Methods</td>
    </tr>
    <tr>
    	<td id="brandBox" style="padding-top:5px;">
        	<div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">ItemID:</div>
    	        <div style="float:left; padding-left:10px; width:100px;">
	    	    	<input type="text" name="itemID" value="" onchange="pullByID(this.value)" />
    	        </div>
            </div>
            <div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">Part Number:</div>
    	        <div style="float:left; padding-left:10px; width:100px;">
	    	    	<input type="text" name="partNumber" value="" onchange="pullByPN(this.value)" />
    	        </div>
            </div>
            <div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">Category:</div>
    	        <div style="float:left; padding-left:10px; width:300px;">
	    	    	<select name="catSearch" onchange="pullByCat(this.value)">
                    	<option value=""></option>
						<%
						do while not catRS.EOF
						%>
                        <option value="<%=catRS("typeID")%>"><%=catRS("typeName")%></option>
                        <%
							catRS.movenext
						loop
						%>
                    </select>
    	        </div>
            </div>
            <div style="width:600px; float:left;">
	        	<div style="float:left; width:140px;">Keyword:</div>
    	        <div style="float:left; padding-left:10px; width:100px;">
	    	    	<input type="text" name="keyword" value="" onchange="pullByKey(this.value)" />
    	        </div>
                <div style="float:left; width:140px;"><input type="button" name="searchBttn" value="Search" /></div>
            </div>
        </td>
    </tr>
    <tr>
    	<td align="right" style="border-top:1px solid #999; border-bottom:1px solid #999; font-weight:bold; font-size:16px; background-color:#000;"><input type="button" name="clearForm" value="Clear Form" onclick="window.location='/admin/productLookup.asp'" /></td>
    </tr>
    <tr>
        <td id="resultBox" style="padding-top:5px;"></td>
    </tr>
</table>
</form>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function brandSelect(brandID) {
		ajax('/ajax/admin/ajaxProductLookup.asp?brandID=' + brandID,'modelBox')
		document.getElementById("catBox").innerHTML = ""
		document.getElementById("resultBox").innerHTML = ""
	}
	function modelSelect(modelID) {
		ajax('/ajax/admin/ajaxProductLookup.asp?modelID=' + modelID,'catBox')
		document.getElementById("resultBox").innerHTML = ""
	}
	function catSelect(catID,modelID) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxProductLookup.asp?modelID=' + modelID + '&catID=' + catID,'resultBox')
	}
	function resultOrder(catID,modelID,orderBy) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxProductLookup.asp?modelID=' + modelID + '&catID=' + catID + '&orderBy=' + orderBy,'resultBox')
	}
	function pullByID(itemID) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxProductLookup.asp?itemID=' + itemID,'resultBox')
	}
	function pullByPN(partNumber) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxProductLookup.asp?partNumber=' + partNumber,'resultBox')
	}
	function pullByCat(catID) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxProductLookup.asp?categoryID=' + catID,'resultBox')
	}
	function pullByKey(keyword) {
		document.getElementById("resultBox").innerHTML = "<table width='600'><tr><td align='center' style='font-size:16px; font-weight:bold;'>Searching for products...</td></tr></table>"
		ajax('/ajax/admin/ajaxProductLookup.asp?keyword=' + keyword,'resultBox')
	}
	function displayInvHistory(itemID) {
		ajax('/ajax/admin/ajaxInventoryAdjust.asp?showHistory=' + itemID,'invHistory_'+itemID);
		if (document.getElementById('invHistory_'+itemID).style.display == '') 
			document.getElementById('invHistory_'+itemID).style.display='none'
		else 
			document.getElementById('invHistory_'+itemID).style.display=''		
	}

</script>