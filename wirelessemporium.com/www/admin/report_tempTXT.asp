<%
pageTitle = "WE Admin Site - Uploads to /Admin/tempTXT Directory"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<h3 align="center"><%=pageTitle%></h3>

<table border="1" cellpadding="0" cellspacing="0" align="center" width="600">
<tr><td align="center"><b>Filename</b></td><td align="center"><b>Date&nbsp;Last&nbsp;Modified</b></td></tr>

<%
if request.form("submitted") <> "" then
	dateStart = request.form("dateStart")
	if not isDate(dateStart) then dateStart = date()
	dateStart = dateValue(dateStart)
	set filesys = CreateObject("Scripting.FileSystemObject")
	set demofolder = filesys.GetFolder(server.mappath("\admin\tempTXT"))
	set filecoll = demofolder.Files
	for each fil in filecoll
		if dateValue(fil.DateLastModified) = dateStart then
			filist = filist & "<tr><td>" & fil.name & "</td><td>" & fil.DateLastModified & "</td></tr>"
		end if
	next
	response.write filist
	set filesys = nothing
	set demofolder = nothing
	set filecoll = nothing
end if
%>

<form action="/admin/report_tempTXT.asp" method="post">
	<p><input type="text" name="dateStart" value="<%=dateStart%>">&nbsp;&nbsp;Date&nbsp;to&nbsp;Search</p>
	<p><input type="submit" name="submitted" value="Generate Report"></p>
</form>

</table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
