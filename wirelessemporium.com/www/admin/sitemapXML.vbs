dim oConn
set oConn = CreateObject("ADODB.Connection")
oConn.ConnectionString = "DSN=WEDB01;Uid=wireless_user;Pwd=c3llph0n3;"
oConn.Open

dim fs, file, path, nFileIdx, fileBaseName, folderName, nRows
nRows = 0
nFileIdx = 1
folderName = "C:\inetpub\wwwroot\wirelessemporium.com\www"
fileBaseName = "sitemap"
set fs = CreateObject("Scripting.FileSystemObject")

path = folderName & "\" & fileBaseName & nFileIdx & ".xml"
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if
set file = fs.CreateTextFile(path)
file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
file.WriteLine "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">"
file.WriteLine "<url>"
file.WriteLine vbtab & "<loc>http://www.wirelessemporium.com/</loc>"
file.WriteLine vbtab & "<priority>0.90</priority>"
file.WriteLine "</url>"

dim temp(100)
temp(1) = "iphone-accessories.asp"
temp(2) = "blackberry-cell-phone-accessories.asp"
temp(3) = "htc-cell-phone-accessories.asp"
temp(4) = "kyocera-cell-phone-accessories.asp"
temp(5) = "lg-cell-phone-accessories.asp"
temp(6) = "motorola-cell-phone-accessories.asp"
temp(7) = "nextel-cell-phone-accessories.asp"
temp(8) = "nokia-cell-phone-accessories.asp"
temp(9) = "palm-cell-phone-accessories.asp"
temp(10) = "pantech-cell-phone-accessories.asp"
temp(11) = "samsung-cell-phone-accessories.asp"
temp(12) = "sanyo-cell-phone-accessories.asp"
temp(13) = "sidekick-cell-phone-accessories.asp"
temp(14) = "siemens-cell-phone-accessories.asp"
temp(15) = "sony-ericsson-cell-phone-accessories.asp"
temp(16) = "utstarcom-cell-phone-accessories.asp"
temp(17) = "cell-phone-covers-faceplates-screen-protectors.asp"
temp(18) = "cell-phone-batteries.asp"
temp(19) = "cell-phone-chargers.asp"
temp(20) = "cell-phone-cases.asp"
temp(21) = "bluetooth-headsets-handsfree.asp"
temp(22) = "cell-phone-holsters-belt-clips-holders.asp"
temp(23) = "cell-phone-data-cables-memory-cards.asp"
temp(24) = "cell-phone-antennas.asp"
temp(25) = "cell-phone-charms-bling-kits.asp"
temp(26) = "unlocked-cell-phones.asp"
temp(27) = "category.asp?categoryid=17"
temp(28) = "car-2-att-cingular-accessories.asp"
temp(29) = "car-4-sprint-nextel-accessories.asp"
temp(30) = "car-5-t-mobile-accessories.asp"
temp(31) = "car-6-verizon-accessories.asp"
temp(32) = "car-1-alltel-accessories.asp"
temp(33) = "car-8-boost-mobile-southern-linc-accessories.asp"
temp(34) = "car-9-cricket-accessories.asp"
temp(35) = "car-3-metro-pcs-accessories.asp"
temp(36) = "car-7-prepaid-accessories.asp"
temp(37) = "car-11-us-cellular-accessories.asp"
temp(38) = "apple-ipad-accessories.asp"
temp(39) = "ipod-ipad-accessories.asp"
temp(40) = "bluetooth.asp"
temp(41) = "car-2-phones-" & formatSEO("AT&T / Cingular") & ".asp"
temp(42) = "car-4-phones-" & formatSEO("Sprint / Nextel") & ".asp"
temp(43) = "car-5-phones-" & formatSEO("T-Mobile") & ".asp"
temp(44) = "car-6-phones-" & formatSEO("Verizon") & ".asp"
temp(45) = "screen-protectors.asp"
temp(46) = "decal-skin.asp"
temp(47) = "music-skins.asp"
temp(48) = "bundle-packs.asp"


dim a, URL
for a = 1 to 48
	URL = temp(a)
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://www.wirelessemporium.com/" & URL & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"
next


'============================================== BRAND-MODEL & (HANDS-FREE for its model) START
dim strBrandModelSQL, objRsBrandModel
strBrandModelSQL = 	"select	distinct a.modelid, c.brandname, a.modelname " & vbcrlf & _
					"from	we_models a join we_items b  " & vbcrlf & _
					"	on	a.modelid = b.modelid join we_brands c" & vbcrlf & _
					"	on	a.brandid = c.brandid" & vbcrlf & _
					"where	b.hidelive = 0 " & vbcrlf & _
					"	and b.inv_qty <> 0 " & vbcrlf & _
					"	and b.price_our > 0 " & vbcrlf & _
					"	and a.modelname not like 'all model%' " & vbcrlf & _					
					"order by 1"
					
set objRsBrandModel	= oConn.execute(strBrandModelSQL)

do until objRsBrandModel.EOF
	if 968 <> objRsBrandModel("modelID") and 940 <> objRsBrandModel("modelID") then
		url = "T-" & objRsBrandModel("modelID") & "-cell-accessories-" & formatSEO(objRsBrandModel("brandName")) & "-" & formatSEO(objRsBrandModel("modelName")) & ".asp"
		call fWriteFile(url)
	end if

	url = "hf-sm-" & objRsBrandModel("modelID") & "-bluetooth-headsets-headphones-for-" & formatSEO(objRsBrandModel("brandName")) & "-" & formatSEO(objRsBrandModel("modelName")) & ".asp"
	call fWriteFile(url)	
	objRsBrandModel.movenext
loop
set objRsBrandModel = nothing
'============================================== BRAND-MODEL & (HANDS-FREE for its model) END	

'============================================== category-brand START
dim strCateBrandSQL, objRsCateBrand
strCateBrandSQL	=	"select	distinct c.typeid, c.typename, a.brandid, a.brandname" & vbcrlf & _
					"from	we_items b join we_types c " & vbcrlf & _
					"	on	b.typeid = c.typeid join we_brands a" & vbcrlf & _
					"	on	b.brandid = a.brandid" & vbcrlf & _
					"where	b.typeid in (1,2,3,6,7,8,12,13,16,17,18,19,20,21,22)" & vbcrlf & _
					"	and b.hidelive = 0" & vbcrlf & _
					"	and b.inv_qty <> 0" & vbcrlf & _
					"	and b.price_our > 0 " & vbcrlf & _
					"order by 1"

set objRsCateBrand = oConn.execute(strCateBrandSQL)

do until objRsCateBrand.EOF
	url = "sc-" & objRsCateBrand("typeID") & "-sb-" & objRsCateBrand("brandID") & "-" & formatSEO(objRsCateBrand("brandName")) & "-" & formatSEO(objRsCateBrand("typeName")) & ".asp"
	call fWriteFile(url)
	objRsCateBrand.movenext
loop
set objRsCateBrand = nothing
'============================================== category-brand END

'============================================== category-brand for music skins START
dim objRsCbMusicSkins
sql	=	"select	distinct 20 typeid, 'Music Skins' as typename, a.brandid, a.brandname" & vbcrlf & _
		"from	we_items_musicskins b join we_brands a" & vbcrlf & _
		"	on	b.brandid = a.brandid" & vbcrlf & _
		"where	b.skip = 0" & vbcrlf & _
		"	and b.deleteItem = 0" & vbcrlf & _
		"order by 1" & vbcrlf

set objRsCbMusicSkins = oConn.execute(sql)

do until objRsCbMusicSkins.EOF
	url = "sc-" & objRsCbMusicSkins("typeID") & "-sb-" & objRsCbMusicSkins("brandID") & "-" & formatSEO(objRsCbMusicSkins("brandName")) & "-" & formatSEO(objRsCbMusicSkins("typeName")) & ".asp"
	call fWriteFile(url)
	objRsCbMusicSkins.movenext
loop
set objRsCbMusicSkins = nothing
'============================================== category-brand for music skins END

'============================================== carrier-brand START
dim strCarrierBrandSQL, objRsCarrierBrand
strCarrierBrandSQL	=	"	select	distinct a.brandid, b.brandname, c.id carrierid, c.carriername" & vbcrlf & _
						"	from	we_models a join we_brands b " & vbcrlf & _
						"		on	a.brandid = b.brandid join we_carriers c" & vbcrlf & _
						"		on	a.carriercode like '%' + c.carriercode + ',%'" & vbcrlf & _
						"	where	c.id not in (12)" & vbcrlf & _						
						"	order by 2" & vbcrlf

set objRsCarrierBrand = oConn.execute(strCarrierBrandSQL)

do until objRsCarrierBrand.EOF
	
	if objRsCarrierBrand("brandID") = 15 then
		URL = "car-" & objRsCarrierBrand("carrierid") & "-b-15-" & formatSEO(objRsCarrierBrand("carrierName")) & "-other-phone-accessories.asp"
	else
		URL = "car-" & objRsCarrierBrand("carrierid") & "-b-" & objRsCarrierBrand("brandID") & "-" & formatSEO(objRsCarrierBrand("carrierName")) & "-" & formatSEO(objRsCarrierBrand("brandName")) & "-phone-accessories.asp"
	end if

	call fWriteFile(url)

	objRsCarrierBrand.movenext
loop
set objRsCarrierBrand = nothing
'============================================== carrier-brand END		


'============================================== brand-model-category START
dim strBMCSQL, objRsBMC
strBMCSQL	=	"select	distinct a.modelid, a.modelname, c.brandid, c.brandname, d.typeid, d.typename" & vbcrlf & _
				"from	we_models a join we_items b " & vbcrlf & _
				"	on	a.modelid = b.modelid join we_brands c" & vbcrlf & _
				"	on	a.brandid = c.brandid join we_types d" & vbcrlf & _
				"	on	b.typeid = d.typeid" & vbcrlf & _
				"where	b.hidelive = 0 and a.modelname not like 'all model%'" & vbcrlf & _
				"	and b.inv_qty <> 0" & vbcrlf & _
				"	and b.price_our > 0 " & vbcrlf & _
				"order by 1"


set objRsBMC = oConn.execute(strBMCSQL)

do until objRsBMC.EOF
	if objRsBMC("typeid") = 16 then
		URL = "cp-sb-" & objRsBMC("brandID") & "-sm-" & objRsBMC("modelID") & "-" & formatSEO(objRsBMC("typeName")) & "-for-" & formatSEO(objRsBMC("brandName")) & "-" & formatSEO(objRsBMC("modelName")) & ".asp"
	else
		URL = "sb-" & objRsBMC("brandID") & "-sm-" & objRsBMC("modelID") & "-sc-" & objRsBMC("typeID") & "-" & formatSEO(objRsBMC("typeName")) & "-" & formatSEO(objRsBMC("brandName")) & "-" & formatSEO(objRsBMC("modelName")) & ".asp"
	end if

	call fWriteFile(url)

	objRsBMC.movenext
loop
set objRsBMC = nothing
'============================================== brand-model-category END


'============================================== product page START
dim pSQL, objRsProduct
pSQL	=	"select	a.itemid, a.itemdesc " & vbcrlf & _
			"from	we_items a join we_models b " & vbcrlf & _
			"	on	a.modelid = b.modelid " & vbcrlf & _			
			"where	a.hidelive = 0 and a.inv_qty <> 0  " & vbcrlf & _
			"	and a.price_our > 0 " & vbcrlf & _
			"	and b.hidelive = 0 " & vbcrlf & _			
			"order by 1"
			
set objRsProduct = oConn.execute(pSQL)

do until objRsProduct.EOF
	url = "p-" & objRsProduct("itemID") & "-" & formatSEO(objRsProduct("itemDesc")) & ".asp"

	call fWriteFile(url)

	objRsProduct.movenext
loop
set objRsProduct = nothing
'============================================== product page END

'============================================== treepodia video sitemap
dim objRsVideo
pSQL	=	"select	b.itemid, b.itemdesc	" & vbcrlf & _
			"from	we_items_treepodia a join we_items b" & vbcrlf & _
			"	on	a.itemid = b.itemid" & vbcrlf & _
			"where	b.hidelive = 0 and b.inv_qty <> 0 and b.price_our > 0" & vbcrlf & _
			"order by 1" & vbcrlf
			
set objRsVideo = oConn.execute(pSQL)

do until objRsVideo.EOF
	url = "pv-" & objRsVideo("itemID") & "-" & formatSEO(objRsVideo("itemDesc")) & ".asp"

	call fWriteFile(url)

	objRsVideo.movenext
loop
set objRsVideo= nothing
'============================================== product page END


'============================================== musicSkins products
dim objRsMusicSkins
pSQL	=	"select	id as itemid, artist + ' ' + designName + '-music-skins-for-' + c.brandName + '-' + b.modelName as itemdesc" & vbcrlf & _
			"from 	we_items_musicSkins a join we_models b" & vbcrlf & _
			"	on	a.modelid = b.modelid join we_brands c" & vbcrlf & _
			"	on	a.brandid = c.brandid" & vbcrlf & _
			"where 	skip = 0" & vbcrlf & _
			"	and deleteItem = 0" & vbcrlf & _
			"	and (artist is not null or artist <> '')" & vbcrlf & _
			"	and (designName is not null or designName <> '')" & vbcrlf & _
			"order by 1" & vbcrlf			
			
set objRsMusicSkins = oConn.execute(pSQL)

do until objRsMusicSkins.EOF
	url = "p-ms-" & objRsMusicSkins("itemID") & "-" & formatSEO(objRsMusicSkins("itemDesc")) & ".asp"
	call fWriteFile(url)
	objRsMusicSkins.movenext
loop
set objRsMusicSkins = nothing
file.WriteLine "</urlset>"
file.close()
set file = nothing
'============================================== musicSkins products END



'============================================== index site map START
path = folderName & "\" & fileBaseName & ".xml"
'If the file already exists, delete it.
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if
set file = fs.CreateTextFile(path)
file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
file.WriteLine "<sitemapindex xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"">"

for i=1 to nFileIdx
	file.WriteLine "<sitemap>"
	file.WriteLine vbtab & "<loc>http://www.wirelessemporium.com/" & fileBaseName & i & ".xml</loc>"
	file.WriteLine vbtab & "<lastmod>" & RFC3339(now) & "</lastmod>"
	file.WriteLine "</sitemap>"
next

file.WriteLine "</sitemapindex>"
file.close()
set file = nothing
'============================================== index site map END



sub fWriteFile(pStrUrl)
	nRows = nRows + 1
	
	if nRows > 45000 then
		nFileIdx = nFileIdx + 1
		nRows = 0
		file.WriteLine "</urlset>"
		file.close()
		set file = nothing
		
		path = folderName & "\" & fileBaseName & nFileIdx & ".xml"
		if fs.FileExists(path) then
			fs.DeleteFile(path)
		end if
		set file = fs.CreateTextFile(path)
		file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
		file.WriteLine "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">"
	end if
	
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://www.wirelessemporium.com/" & pStrUrl & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"	
end sub

function RFC3339(myDate)
	dim myDays, myMonth, myYear, myHours, myMonths, mySeconds
	myDate = dateAdd("h",8,myDate)
	myDays = zeroPad(day(myDate),2)
	myMonth = zeroPad(month(myDate),2)
	myYear = year(myDate)
	myHours = zeroPad(hour(myDate),2)
	myMinutes = zeroPad(minute(myDate),2)
	mySeconds = zeroPad(second(myDate),2)
	RFC3339 = myYear & "-" & myMonth & "-" & myDays & "T" & myHours & ":" & myMinutes & ":" & mySeconds & "Z"
end function
 
function zeroPad(m,t)
	zeroPad = String(t-Len(m),"0") & m
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(formatSEO,"?","-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function