<%
	thisUser = Request.Cookies("username")
	pageTitle = "WE Admin Site - Add/Update MobileLine Products"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/framework/utility/ftp.asp"-->
<%
'	call initConnection(ftpDB,"192.168.100.162","","")	'database ftp
	call initConnection(ftpDB,"192.168.100.162","ftpAdmin","GSF79h7yr9r3$C")	'database ftp

	Server.ScriptTimeout = 9999 'seconds
	dim Upload, path, sql, rs, txtConn, myStr, brandRS, brandStr, brandArray, typeID
	dim filesys, file, filename, WriteFile, myFile, prodLap, curArray, curBrandArray
	dim userMsg : userMsg = ""
	dim unassignedProds : unassignedProds = ""

	dbFilePath = "\webserver\mobileline_upload"
	dbAbsoluteFilePath = "c:\ftp\webserver\mobileline_upload"

	set Upload = Server.CreateObject("Persits.Upload")
	Upload.IgnoreNoPost = true
	path = server.MapPath("/xmlFiles/mobileLine/")
	Count = Upload.Save(path)

	if Upload.form("upload") <> "" then
		set fileWE = upload.files("file1")
		ftpSuccess = ftpDB.ChangeRemoteDir(dbFilePath)
		ftpSuccess = ftpDB.PutFile(path & "\" & fileWE.filename, fileWE.filename)

		sql = "delete from holding_mobileLine"
		session("errorSQL") = sql
		oConn.execute(sql)

		if Count = 0 then
			userMsg = "## No file selected for upload ##"
		else
'			set filesys = CreateObject("Scripting.FileSystemObject")
'			set File = Upload.Files(1)
'			myFile = File.path
'
'			myFile = server.MapPath("/xmlFiles/mobileLine/MBL_ACC_FEED_FULL_CATEGORY.xls")
'			set txtConn = Server.CreateObject("ADODB.Connection")
'			myStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & myFile & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"";"
'			session("errorSQL") = myStr
'			txtConn.ConnectionString = myStr
'			txtConn.ConnectionTimeout = 9999		'seconds
'			txtConn.Open
'
'			sql = "SELECT * FROM [MBL_ACC_FEED_FULL_CATEGORY$]"
'			session("errorSQL") = sql
'			set rs = Server.CreateObject("ADODB.Recordset")
'			rs.open sql, txtConn, 0, 1
'
'			prodLap = 0
'			do while not rs.EOF
'				prodLap = prodLap + 1
'				oConn.execute("insert into _terry(msg) values('prodLap: " & prodLap & ", " & ds(rs("Mobile Line Part Number")) & "')")
'				sql = "insert into holding_mobileLine (mobileLineID,cat1,cat2,cat3,cat4,cat5,cat6,name,description,shortDesc,weight,msrp,cogs,manufacturer,model,image,manType,keywords,inv_qty,createDate,UPC,status) values('" & ds(rs("Mobile Line Part Number")) & "','" & ds(rs("Category1")) & "','" & ds(rs("Category2")) & "','" & ds(rs("Category3")) & "','" & ds(rs("Category4")) & "','" & ds(rs("Category5")) & "','" & ds(rs("Category6")) & "','" & ds(rs("Name")) & "','" & ds(rs("Description")) & "','" & ds(rs("Short Description")) & "','" & ds(rs("Weight (lbs)")) & "','" & ds(rs("MSRP")) & "','" & ds(rs("Mobile Line Price")) & "','" & ds(rs("Manufacturer")) & "','" & ds(rs("Model")) & "','" & ds(rs("Image")) & "','" & ds(rs("Manufacturer Type")) & "','','" & ds(rs("Quantity")) & "','" & ds(rs("Date Created")) & "','" & ds(hld_UPC) & "','" & ds(rs("Status")) & "')"
'				session("errorSQL") = sql
'				oConn.execute(sql)
'				response.flush
'				rs.movenext
'			loop
'			session("totalProducts") = prodLap
'
'			txtConn.close
'			set txtConn = nothing
'			set filesys = nothing
'			set File = nothing

			sql	=	"insert into holding_mobileLine(mobileLineID,cat1,cat2,cat3,cat4,cat5,cat6,name,description,shortDesc,weight,msrp,cogs,manufacturer,model,image,manType,keywords,inv_qty,createDate,UPC,status) " & vbcrlf & _
					"select [Mobile Line Part Number],isnull([Category1], ''),isnull([Category2], ''),isnull([Category3], ''),isnull([Category4], ''),isnull([Category5], ''),isnull([Category6], ''),[Name],[Description],[Short Description],isnull([Weight (lbs)], 0),[MSRP],[Mobile Line Price],[Manufacturer],[Model],isnull([Image], '') [Image],[Manufacturer Type],'',[Quantity],[Date Created],'',[Status]" & vbcrlf & _
					"from 	openrowset('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=" & dbAbsoluteFilePath & "\" & fileWE.filename & ";HDR=YES', 'SELECT * FROM [MBL_ACC_FEED_FULL_CATEGORY$]')"
			oConn.execute(sql)

			userMsg = "Excel file accepted<br>Now processing the products"
		end if
'		response.write sql
	else
		sql = "select top 1 entryDate from holding_mobileLine"
		set rs = oConn.execute(sql)
		if not rs.EOF then lastPulled = rs("entryDate") else lastPulled = cdate("1/1/1999")
		rs = null
	end if
	unassignedProds = session("unassignedProds")
	session("unassignedProds") = null
	if unassignedProds <> "" then
		sql = "select brandID, brandName from we_brands with (nolock) order by brandName"
		session("errorSQL") = sql
		Set brandRS = Server.CreateObject("ADODB.Recordset")
		brandRS.open sql, oConn, 0, 1

		do while not brandRS.EOF
			brandStr = brandStr & brandRS("brandID") & "##" & brandRS("brandName") & "@@"
			brandRS.movenext
		loop
		brandRS = null
		brandArray = split(brandStr,"@@")
	end if

	sql = "select typeid, typename from we_types order by typename"
	arrTypes = getDbRows(sql)

	sql = 	"select	typename, subtypeid, subtypeName from v_subtypeMatrix order by 1, 3"
	arrSubTypes = getDbRows(sql)
%>
<table border="0" cellpadding="3" cellspacing="0" align="center" style="padding-top:50px;">
	<% if userMsg <> "" then %>
    <tr>
    	<td style="font-weight:bold; color:#F00; text-align:center; border-bottom:1px solid #F00; border-top:1px solid #F00;">
			<%=userMsg%>
            <% if userMsg = "Excel file accepted<br>Now processing the products" then %>
            <br /><br /><%=prodLap%> Products to Process
            <% end if %>
        </td>
    </tr>
    <% end if %>
    <%
	if session("unknownItems") <> "" then
		unknownItems = session("unknownItems")
		session("unknownItems") = null
	%>
    <tr>
    	<td align="left">
			<strong>Uncassified Products</strong><br />
			<%=unknownItems%>
        </td>
    </tr>
    <% end if %>
    <%
	if unassignedProds <> "" then
		unassignedArray = split(unassignedProds,"@@")
	%>
    <tr>
    	<td>
        	<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" style="font-size:10px;">
            	<tr style="background-color:#333; color:#FFF; font-weight:bold;">
                	<td>ID</td>
                    <td>Model</td>
                    <td>Name</td>
                    <td nowrap>Assign More Info.</td>
                    <td nowrap>Assign Brand</td>
                    <td nowrap>Assign Model</td>
                </tr>
                <%
				lap = 0
				for i = 0 to (ubound(unassignedArray) - 1)
					lap = lap + 1
					curArray = split(unassignedArray(i),"##")
					msrp = prepInt(curArray(3))
					cogs = prepInt(curArray(4))
				%>
                <tr>
                	<td><%=curArray(0)%></td>
                    <td><a href="http://www.mobileline.com/store/catalogsearch/result/?q=<%=curArray(1)%>&x=0&y=0" target="mobileLine"><%=curArray(1)%></a></td>
                    <td><%=curArray(2)%></td>
                    <td>
                    	<select id="cb_typeid_<%=lap%>" name="typeid" onchange="getSubtypes(this.value,<%=lap%>)" style="width:200px;">
                        	<option value="">Select Type</option>
                            <%
							for nRow=0 to ubound(arrTypes, 2)
							%>
                            <option value="<%=arrTypes(0,nRow)%>"><%=arrTypes(1,nRow)%></option>
                            <%
							next
							%>
                        </select>
                        <br />
                        <select id="cb_subtypeid_<%=lap%>" name="subtypeid" style="width:200px;">
                            <option value="">Select Subtype</option>
                            <%
                            lastLabel = ""
							for nRow = 0 to ubound(arrSubTypes, 2)
                                if arrSubTypes(0,nRow) <> lastLabel then
                                    if lastLabel <> "" then response.write "</optgroup>"
                                    response.write "<optgroup label=""" & arrSubTypes(0,nRow) & """>"
                                    lastLabel = arrSubTypes(0,nRow)
                                end if
                                %>
                                <option value="<%=arrSubTypes(1,nRow)%>"><%=arrSubTypes(2,nRow)%></option>
                                <%
							next
                            %>
                            </optgroup>
                        </select>
                        <br />
                        <table border="0" width="100%">
                        	<tr><td>Retail</td><td>WE</td><td>Cogs</td></tr>
                        	<tr>
                            	<td><input type="text" id="txtRetailPrice_<%=lap%>" value="<%=msrp%>" style="width:50px;"></td>
	                            <td><input type="text" id="txtOurPrice_<%=lap%>" value="<%=(msrp-1)%>" style="width:50px;"></td>
                                <td><input type="text" id="txtCogs_<%=lap%>" value="<%=cogs%>" style="width:50px;"></td></tr>
                        </table>
                    </td>
                    <td>
                    	<select name="brandID" onChange="getModels(this.value,<%=lap%>,<%=curArray(0)%>)">
                        	<option value="">Select Brand</option>
                            <%
							for z = 0 to (ubound(brandArray) - 1)
								curBrandArray = split(brandArray(z),"##")
							%>
                            <option value="<%=curBrandArray(0)%>"><%=curBrandArray(1)%></option>
                            <%
							next
							%>
                        </select>
                    </td>
                    <td id="modelSelect_<%=lap%>">Select Brand First</td>
                </tr>
                <%
					response.flush
				next
				%>
            </table>
        </td>
    </tr>
    <% end if %>
    <% if userMsg <> "Excel file accepted<br>Now processing the products" then %>
    <tr>
    	<td>
        	<form enctype="multipart/form-data" action="/admin/mobileLineXLS.asp" method="POST">
				<div style="padding:5px;">Upload new MobileLine Product File</div>
				<div style="text-align:right; padding:5px;"><input type="FILE" name="FILE1"></div>
				<div style="text-align:right; padding:5px; border-bottom:1px solid #000;"><input type="SUBMIT" name="upload" value="Upload"></div>
                <% if dateOnly(lastPulled) = date then %>
                <div style="padding-top:20px; width:450px; font-size:12px;">
                	The MobileLine product sheet was already pulled today. Please use the <strong style="color:#F00;">"Retrieve New Unassigned Products"</strong>
                    button below to assign new products to their brand/model
                </div>
                <div style="text-align:right; padding:5px;"><input type="button" name="retrieve" value="Retrieve New Unassigned Products" onclick="window.location='/admin/mobileLineXLS2.asp?retrieve=1'"></div>
                <% end if %>
			</form>
        </td>
    </tr>
    <% end if %>
</table>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	<% if userMsg = "Excel file accepted<br>Now processing the products" then %>
	setTimeout("window.location = '/admin/mobileLineXLS2.asp'",2000)
	<% end if %>
	function getModels(brandID,lap,dbID) {
		ajax("/ajax/admin/ajaxMobileLineXLS.asp?brandID=" + brandID + "&lap=" + lap + "&dbID=" + dbID,"modelSelect_" + lap)
	}
	function setModel(modelID,brandID,lap,dbID) {
		typeid = document.getElementById('cb_typeid_'+lap).value;
		subtypeid = document.getElementById('cb_subtypeid_'+lap).value;
		priceRetail = document.getElementById('txtRetailPrice_'+lap).value;
		priceWE = document.getElementById('txtOurPrice_'+lap).value;
		cogs = document.getElementById('txtCogs_'+lap).value;

		updateURL = "/ajax/admin/ajaxMobileLineXLS.asp?priceRetail=" + priceRetail + "&priceWE=" + priceWE + "&cogs=" + cogs + "&typeid=" + typeid + "&subtypeid=" + subtypeid + "&modelID=" + modelID + "&brandID=" + brandID + "&lap=" + lap + "&dbID=" + dbID;
//		alert(updateURL);
		ajax(updateURL,"modelSelect_" + lap);
	}
</script>