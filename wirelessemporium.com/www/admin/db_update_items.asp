<%
Response.AddHeader "expires", "0"
Response.AddHeader "pragma", "no-cache"
Response.AddHeader "cache-control", "no-cache"

pageTitle = "Admin - Update WE Database - Add/Edit Products"
header = 1
dim fs : set fs = CreateObject("Scripting.FileSystemObject")
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_DbInventory.asp"-->
<script>
function doSubmit(submitType) {
	document.frmUpdateItem.frmSubmit.value = submitType
	if (submitType == "Copy" || submitType == "Add") {
		ajax('/ajax/admin/testPartNumber.asp?partNumber=' + document.frmUpdateItem.PartNumber.value,'dumpZone')
		setTimeout("finishSubmit('" + submitType + "')",700)
	}
	else {
		document.getElementById("dumpZone").innerHTML = "Good PN"
		finishSubmit(submitType)
	}
}

function finishSubmit(submitType) {
	var pnResult = document.getElementById("dumpZone").innerHTML
	
	if (pnResult == "Good PN") {
		var strPrompt = "Warning: Do you wish to proceed?";
	
		if ("Add" == submitType) strPrompt = "Warning: You are about to add.\r\nDo you wish to proceed?";
		else if ("Edit" == submitType) strPrompt = "Warning: You are about to edit.\r\nDo you wish to proceed?";
		else if ("Copy" == submitType) strPrompt = "Warning: You are about to copy to new.\r\nDo you wish to proceed?";
		
		if (confirm(strPrompt)) document.frmUpdateItem.submit()
	}
	else {
		alert(pnResult)
		alert("That part number is already being used, please select a unique part number:" + document.frmUpdateItem.PartNumber.value)
		document.frmUpdateItem.PartNumber.select()
	}
}

function TrimStrings(stringToTrim)
{
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function replaceAll()
{
	var	strFrom = escape(TrimStrings(document.frmUpdateItem.txtReplaceFrom.value));
	var	strTo	= escape(TrimStrings(document.frmUpdateItem.txtReplaceTo.value));
	var	nMatchCount = 0;
	
	if ("" != strFrom) {
		var strOld = '';
		var strNew = '';
		
		var oMatch = new RegExp(strFrom, "ig");
		var elem = document.getElementById('id_frmUpdateItem').elements;

        for(var i = 0; i < elem.length; i++) {
			if (("text" == elem[i].type) || ("textarea" == elem[i].type)) {
				if (("txtReplaceFrom" != elem[i].name) && ("txtReplaceTo" != elem[i].name)) {
					strOld = escape(elem[i].value);
					if (strOld.search(strFrom) >= 0) {
						enableUpdateBtn('div_'+elem[i].name.toLowerCase());
						nMatchCount = nMatchCount + 1;
						strNew = strOld.replace(oMatch, strTo);
						elem[i].value = unescape(strNew);
					}
				}
			}
        } 
	}
	
	var strMsg = "No item found";
	
	if (eval(nMatchCount) > 0) strMsg = "<b>Items found in " + nMatchCount + " text area and replaced.</b>";
	document.getElementById('id_replaceMsg').innerHTML = strMsg;
}

function clearTextFields()
{
	var e = document.getElementById('id_frmUpdateItem').elements;
	
	for(var i = 0; i < e.length; i++)
	{
		if ("textarea" == e[i].type)
		{
			e[i].value = "";
		}
	}
}

function onChangeBrand(selected)
{
	if (0 != eval(selected))
	{
		location.href = "/admin/db_update_items.asp?ItemID=" + document.frmUpdateItem.ItemID.value + "&BrandID=" + selected;
	}
}

function enableUpdateBtn(objID)
{
	if ((typeof document.getElementById(objID) == "object")&&(document.getElementById(objID) != null))
	{
		document.getElementById(objID+'_statusbox').style.display = 'none';
		document.getElementById(objID).style.display = '';		
	}
}

function ajaxUpdateDesc(column, obj, id_resposneBack)
{
	document.getElementById(id_resposneBack).style.display = 'none';
	document.getElementById(id_resposneBack+'_statusbox').innerHTML = "<img src='/images/preloading.gif' border='0'>";
	document.getElementById(id_resposneBack+'_statusbox').style.display = '';

	updateURL	=	"/ajax/admin/ajaxItemUpdate.asp";
	updateParam = 	"itemid=" + document.frmUpdateItem.ItemID.value + "&column=" + column + "&newvalue=" + escape(obj.value);	
	ajaxPost(updateURL, updateParam, id_resposneBack+'_statusbox');
	setTimeout('checkUpdate(\'' + id_resposneBack + '\')', 1000);
}

function ajaxLoadPrefix(prefixValue)
{
	updateURL	=	"/ajax/admin/ajaxProductFeatures.asp?requestType=L&templateName=" + escape(prefixValue);
	ajax(updateURL,'id_prefix')
}

function ajaxAddBulletTemplate()
{
	textPrefix = "";
	templateName = document.frmUpdateItem.txtTemplateName.value;
	for (i=1; i<=10; i++) textPrefix = textPrefix + '&prefix' + i + '=' + document.getElementById('id_prefix'+i).value;

	updateURL	=	"/ajax/admin/ajaxProductFeatures.asp?requestType=A&templateName=" + escape(templateName) + textPrefix;
	ajax(updateURL,'id_prefix');
	alert('ADDED!');
}

function copyBullets(siteid)
{
	bulletPointName = '';
	btnUpdateButton = '';
	if (siteid == 0) 
	{
		bulletPointName = 'BULLET';	
		btnUpdateButton = 'div_bullet';
	}
	else if (siteid == 2)
	{
		bulletPointName = 'POINT';
		btnUpdateButton = 'div_point';
	}
	else if (siteid == 3)
	{
		bulletPointName = 'FEATURE';
		btnUpdateButton = 'div_feature';
	}
	
	for (i=1; i<=10; i++) {
		if (eval('document.frmUpdateItem.textPrefix' + i).value == '') 
			newDescription = eval('document.frmUpdateItem.textTemplate' + i).value;
		else
			newDescription = eval('document.frmUpdateItem.textPrefix' + i).value + ' ' + eval('document.frmUpdateItem.textTemplate' + i).value;
			
		eval('document.frmUpdateItem.' + bulletPointName + i).value = newDescription;
		enableUpdateBtn(btnUpdateButton+i);
	}
}

function checkUpdate(objID)
{
	if ("updated" != document.getElementById(objID+'_statusbox').innerHTML) 
	{
		setTimeout('checkUpdate(\'' + objID + '\')', 1000);
	} else
	{
		clearTimeout();
		document.getElementById(objID+'_statusbox').innerHTML = "<img src='/images/check.jpg' border='0'>";		
	}
}

/*
test code for issues regarding replace function
window.onload = function() {
	str = "Apple iPad 3 (2012)/The New iPad TPU Crystal Candy Silicone Back-Plate (Frosted Red) Apple iPad 3 (2012)/The New iPad Apple iPad 3 (2012)/The New iPad";
	document.getElementById('terryZone').innerHTML = document.getElementById('terryZone').innerHTML + str + "<br />";
	document.getElementById('terryZone').innerHTML = document.getElementById('terryZone').innerHTML + "escape(str):" + escape(str) + "<br />";
	document.getElementById('terryZone').innerHTML = document.getElementById('terryZone').innerHTML + escape(str).search(escape("Apple iPad 3 (2012)/The New iPad")) + "<br />";
	var oMatch = new RegExp(escape("Apple iPad 3 (2012)/The New iPad"), "ig");
	document.getElementById('terryZone').innerHTML = document.getElementById('terryZone').innerHTML + unescape(escape(str).replace(oMatch, "aaaaaaaa"));
}
*/
</script>
<%
BrandID = request.form("BrandID")
BrandID2 = request("BrandID")
modelID = request.form("modelID")
typeID = request.form("typeID")
SubTypeID = request.form("subtypeID")
ItemID = request.querystring("ItemID")
updateItems = request.form("updateItems")
deleteAlt = request.QueryString("deleteAlt")
deleteAltZoom = request.QueryString("deleteAltZoom")

if updateItems = "Edit" then
	SQL = "SELECT a.*, b.description FROM we_items a with(nolock) left join er_items b with(nolock) on a.itemID = b.we_itemID WHERE a.BrandID='" & BrandID & "'"
	if modelID <> "" then SQL = SQL & " AND a.modelID='" & modelID & "'"
	if typeID <> "" then SQL = SQL & " AND a.typeID='" & typeID & "'"
	if typeID = "14" then SQL = "SELECT * FROM we_items with(nolock) WHERE typeID='14'"
	if typeID = "1000" then SQL = "SELECT * FROM we_items with(nolock) WHERE Sports > 0"
	set RS = oConn.execute(SQL)
	do until RS.eof
		%>
		<a href="/admin/db_update_items.asp?ItemID=<%=RS("ItemID")%>"><%=RS("itemDesc")%></a><br>
		<%
		RS.movenext
	loop
	call CloseConn(oConn)
	response.end
end if

dim dicItemValue : set dicItemValue = GetItemValue( ItemID) 'retrieve item value

if ItemID <> "" then
	sql = "select partnumber, (select count(*) from we_items where partnumber = a.partnumber and master = 1) as masters, (select top 1 itemID from we_items where partNumber = a.partNumber order by inv_qty desc, itemID) as newMaster from we_items a where itemID = " & itemID
	session("errorSQL") = SQL
	set rs = oConn.execute(SQL)
	if not rs.EOF then
		if rs("masters") > 1 then
			sql = "update we_items set master = 0, inv_qty = -1 where partnumber = '" & rs("partNumber") & "' and itemID <> " & rs("newMaster")
			session("errorSQL") = SQL
			oConn.execute(sql)
			
			sql = "insert into we_adminActions (adminID,action) values(" & adminID & ",'Set as slave:" & SQLquote(rs("partNumber")) & " (<> " & rs("newMaster") & ")')"
			session("errorSQL") = SQL
			oConn.execute SQL
		elseif rs("masters") = 0 then
			sql = "update we_items set master = 1 where itemID = " & rs("newMaster")
			session("errorSQL") = SQL
			oConn.execute(sql)
			
			sql = "insert into we_adminActions (adminID,action) values(" & adminID & ",'Set as master:" & SQLquote(rs("partNumber")) & " (" & rs("newMaster") & ")')"
			session("errorSQL") = SQL
			oConn.execute SQL
			
			sql = "update we_items set master = 0, inv_qty = -1 where partnumber = '" & rs("partNumber") & "' and itemID <> " & rs("newMaster")
			session("errorSQL") = SQL
			oConn.execute(sql)
			
			sql = "insert into we_adminActions (adminID,action) values(" & adminID & ",'Set as slave:" & SQLquote(rs("partNumber")) & " (<> " & rs("newMaster") & ")')"
			session("errorSQL") = SQL
			oConn.execute SQL
		end if
	end if
	
	SQL = 	"SELECT	a.*, (select itemID from we_items where partNumber = a.partNumber and master = 1) as masterID, e.showAnimation, e.customize " & vbcrlf & _
			"	,	i.id, i.partnumber initPartNumber, i.cogs initCogs, i.quantity initQuantity, i.dateEntd initDateEntd, u.fname, u.lname, u.adminID, p.* " & vbcrlf & _
			"FROM	we_items a " & vbcrlf & _
				"left join we_ItemsExtendedData e on a.PartNumber = e.partNumber " & vbcrlf & _
				"left join we_Items_Initial i on a.itemid = i.itemId " & vbcrlf & _
				"left join we_AdminUsers u on i.adminId = u.adminID " & vbcrlf & _
				"left join we_pnDetails p on p.partNumber = a.partNumber " & vbcrlf & _
			"WHERE a.ItemID='" & itemid & "'"
'	response.write "<pre>" & sql & "</pre>"
	session("errorSQL") = SQL
	'set RS = oConn.execute(SQL)
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	
	dim BrandID, modelID, typeID,carrierid, ItemID, vendor, inv_qty, PartNumber
	dim price_Retail, price_Our, price_CA, price_CO, price_PS, price_Buy, COGS
	dim Seasonal, Sports, Bluetooth, HandsfreeType, ItemKit_NEW, hotDeal, hideLive
	dim UPCCode, AMZN_sku, itemWeight, itemDimensions, itemPic, itemPic_CO, flag1
	dim COMPATIBILITY, download_URL, download_TEXT
	dim itemDesc, itemLongDetail, itemInfo, BULLET1, BULLET2, BULLET3, BULLET4, BULLET5, BULLET6, BULLET7, BULLET8, BULLET9, BULLET10
	dim itemDesc_CO, itemLongDetail_CO, POINT1, POINT2, POINT3, POINT4, POINT5, POINT6, POINT7, POINT8, POINT9, POINT10
	dim itemDesc_CA, itemLongDetail_CA
	dim itemDesc_PS, itemLongDetail_PS, FEATURE1, FEATURE2, FEATURE3, FEATURE4, FEATURE5, FEATURE6, FEATURE7, FEATURE8, FEATURE9, FEATURE10
	dim itemLongDetail_ER
	dim Features, FormFactor, BandType, Band, Smartphone, PackageContents, TalkTime, Standby, Display, NoDiscount, master, ghost, colorID, artistID
	if not RS.eof then
		ItemID = RS("ItemID")
		BrandID = RS("BrandID")
		modelID = RS("modelID")
		typeID = RS("typeID")
		subtypeID = RS("subtypeID")
		carrierID=RS("carrierid")
		vendor = RS("vendor")
		PartNumber = RS("PartNumber")
		itemDesc = RS("itemDesc")
		itemDesc_PS = RS("itemDesc_PS")
		itemDesc_CA = RS("itemDesc_CA")
		itemDesc_CO = RS("itemDesc_CO")
		itemPic = RS("itemPic")
		itemPic_CO = RS("itemPic_CO")
		price_Retail = RS("price_Retail")
		price_Our = RS("price_Our")
		price_CA = RS("price_CA")
		price_CO = RS("price_CO")
		price_PS = RS("price_PS")
		price_ER = RS("price_ER")
		price_Buy = RS("price_Buy")
		COGS = RS("COGS")
		inv_qty = RS("inv_qty")
		Seasonal = RS("Seasonal")
		Sports = RS("Sports")
		Bluetooth = RS("Bluetooth")
		HandsfreeType = RS("HandsfreeType")
		ItemKit_NEW = RS("ItemKit_NEW")
		hotDeal = RS("hotDeal")
		hideLive = RS("hideLive")
		itemLongDetail = RS("itemLongDetail")
		itemInfo = RS("itemInfo")
		BULLET1 = RS("BULLET1")
		BULLET2 = RS("BULLET2")
		BULLET3 = RS("BULLET3")
		BULLET4 = RS("BULLET4")
		BULLET5 = RS("BULLET5")
		BULLET6 = RS("BULLET6")
		BULLET7 = RS("BULLET7")
		BULLET8 = RS("BULLET8")
		BULLET9 = RS("BULLET9")
		BULLET10 = RS("BULLET10")
		COMPATIBILITY = RS("COMPATIBILITY")
		download_URL = RS("download_URL")
		download_TEXT = RS("download_TEXT")
		itemLongDetail_CA = RS("itemLongDetail_CA")
		itemLongDetail_CO = RS("itemLongDetail_CO")
		POINT1 = RS("POINT1")
		POINT2 = RS("POINT2")
		POINT3 = RS("POINT3")
		POINT4 = RS("POINT4")
		POINT5 = RS("POINT5")
		POINT6 = RS("POINT6")
		POINT7 = RS("POINT7")
		POINT8 = RS("POINT8")
		POINT9 = RS("POINT9")
		POINT10 = RS("POINT10")
		itemLongDetail_PS = RS("itemLongDetail_PS")
		FEATURE1 = RS("FEATURE1")
		FEATURE2 = RS("FEATURE2")
		FEATURE3 = RS("FEATURE3")
		FEATURE4 = RS("FEATURE4")
		FEATURE5 = RS("FEATURE5")
		FEATURE6 = RS("FEATURE6")
		FEATURE7 = RS("FEATURE7")
		FEATURE8 = RS("FEATURE8")
		FEATURE9 = RS("FEATURE9")
		FEATURE10 = RS("FEATURE10")
		Features = RS("Features")
		FormFactor = RS("FormFactor")
		BandType = RS("BandType")
		Band = RS("Band")
		PackageContents = RS("PackageContents")
		TalkTime = RS("TalkTime")
		Standby = RS("Standby")
		Display = RS("Display")
		flag1 = RS("flag1")
		UPCCode = RS("UPCCode")
		AMZN_sku = RS("AMZN_sku")
		itemDimensions = RS("itemDimensions")
		itemWeight = RS("itemWeight")
		Smartphone = RS("Smartphone")
		NoDiscount = RS("NoDiscount")
		mobileLinePN = RS("MobileLine_sku")
		master = rs("master")
		ghost = rs("ghost")
		lastCall = rs("lastCall")
		itemLongDetail_ER = rs("itemLongDetail_ER")
		itemDesc_ER = rs("itemDesc_ER")
		colorID = prepInt(rs("colorid"))
		masterID = rs("masterID")
		showAnimation = rs("showAnimation")
		customize = rs("customize")
		artistID = rs("designType")
		initialPartNumber = rs("initPartNumber")
		initialCogs = rs("initCogs")
		initialQuantity = rs("initQuantity")
		initialDateEntd = rs("initDateEntd")
		initialAdminName = rs("fname") & " " & rs("lname")
		batteryCapacity = rs("batteryCapacity")
		
		if isnull(masterID) then
			sql = "select top 1 inv_qty, itemID from we_items where partNumber = '" & PartNumber & "' order by inv_qty desc, itemID"
			session("errorSQL") = sql
			set masterRS = oConn.execute(SQL)
			
			useInvQty = masterRS("inv_qty")
			if useInvQty < 0 then useInvQty = 0
			
			sql = "update we_items set master = 1, inv_qty = " & useInvQty & " where itemID = " & masterRS("itemID")
			session("errorSQL") = sql
			oConn.execute(sql)
			
			masterRS = null
			
			masterID = ItemID
		end if
	end if
end if

if len(deleteAlt) > 0 then
	deleteAlt = prepInt(deleteAlt)
	path = server.MapPath("/productpics/AltViews/" & replace(itemPic,".jpg","-" & deleteAlt & ".jpg"))
	if fs.FileExists(path) then
		fs.DeleteFile(path)
	end if
end if

if len(deleteAltZoom) > 0 then
	deleteAltZoom = prepInt(deleteAltZoom)
	path = server.MapPath("/productpics/AltViews/zoom/" & replace(itemPic,".jpg","-" & deleteAltZoom & ".jpg"))
	if fs.FileExists(path) then
		fs.DeleteFile(path)
	end if
end if



if isnull(subtypeID) then subtypeID = 0
if updateItems = "" then updateItems = "Edit"
if "" = BrandID2 then BrandID2 = BrandID
%>
<form id="id_frmUpdateItem" name="frmUpdateItem" action="db_update_items_submit.asp" method="post" enctype="multipart/form-data">
<input type="hidden" name="ItemID" value="<%=ItemID%>">
<input type="hidden" name="itemPic" value="<%=itemPic%>">
<input type="hidden" name="itemPic_CO" value="<%=itemPic_CO%>">
<div id="terryZone"></div>
<table border="1" cellpadding="5" cellspacing="0" width="840" align="center">
	<tr>
    	<% if master then %>
        <td align="left"><a href="http://www.wirelessemporium.com/p-<%=itemID%>-<%=formatSEO(itemDesc)%>/?id=admin">View Product On WE Public Site</a></td>
        <td align="right"><a href="#" onclick="deleteItem('<%=itemID%>')">Delete Product</a></td>
		<% else %>
    	<td align="left"><a href="http://www.wirelessemporium.com/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.asp">View Product On WE Public Site</a></td>
        <td align="right"><a href="/admin/db_update_items.asp?ItemID=<%=masterID%>">Edit Master Product</a></td>
        <% end if %>
    </tr>
    <tr>
		<td width="50%" class="normaltext">
			<%
			if itemPic <> "" and not isNull(itemPic) then
				%>
				<div id="imgLarge-location" style="height:350px;">
					<img src="/productpics/big/<%=itemPic%>" border="0" id="imgLarge">
                </div>
                <%if fs.fileExists(server.MapPath("/productpics/big/zoom/" & itemPic)) then%>
					<div style="float:left; padding:3px; border:1px solid #ccc; background-color:#999; color:#fff;" onmouseover="document.getElementById('id_zoomimage').style.display=''" onmouseout="document.getElementById('id_zoomimage').style.display='none'">Mouseover to see a zoom image</div>
                    <div style="float:left; position:relative;">
                    	<div id="id_zoomimage" style="position:absolute; top:-400px; left:0px; display:none; background-color:#fff; border:2px solid #000;"><img src="/productpics/big/zoom/<%=itemPic%>" border="0" /></div>
                    </div>
                <%end if%>
				<script language="javascript">
					function fnPreviewImage(imgSrc) 
					{
						var objToDisplay = '<img src="' + imgSrc + '" border="0" width="300" />';
						document.getElementById('imgLarge-location').innerHTML = objToDisplay;
					}
					function fnDefaultImage(imgSrc)
					{
						var objToDisplay = '<img src="' + imgSrc + '" border="0" width="300" />';
						document.getElementById('imgLarge-location').innerHTML = objToDisplay;
					}
				</script>
<div id="altImageFiles" style="float:left; width:100%;">
	<div style="clear:both;">Alt Views: </div>
				<%
				dim path
				for iCount = 0 to 7
					path = server.MapPath("/productpics/AltViews/" & replace(itemPic,".jpg","-" & iCount & ".jpg"))
					if fs.FileExists(path) and instr(itemPic,".jpg") > 0 then
						src = "/productpics/AltViews/" & replace(itemPic,".jpg","-" & iCount & ".jpg")
%>
<div style="float:left; width:50px;">
	<div style="float:left; width:100%;"><a href="#" onmouseover="javascript:fnPreviewImage('<%=src%>');" onmouseout="javascript:fnDefaultImage('/productpics/big/<%=itemPic%>');"><img id="imgThumb<%=iCount%>" name="imgOpt1" src="<%=src%>" width="40" height="40" style="border:1px solid gray;"></a></div>
    <div style="float:left; width:100%; text-align:center;"><a style="font-size:10px; color:#900;" href="/admin/db_update_items.asp?itemID=<%=itemID%>&deleteAlt=<%=iCount%>">delete</a></div>
</div>
<%
					end if
				next
%>
	<div style="clear:both;">Alt Zoom Views: </div>
<%
				for iCount = 0 to 7
					path = server.MapPath("/productpics/AltViews/zoom/" & replace(itemPic,".jpg","-" & iCount & ".jpg"))
					if fs.FileExists(path) and instr(itemPic,".jpg") > 0 then
						src = "/productpics/AltViews/zoom/" & replace(itemPic,".jpg","-" & iCount & ".jpg")
%>
<div style="float:left; width:50px;">
	<div style="float:left; width:100%;"><a href="#" onmouseover="javascript:fnPreviewImage('<%=src%>');" onmouseout="javascript:fnDefaultImage('/productpics/big/<%=itemPic%>');"><img id="imgThumb<%=iCount%>" name="imgOpt1" src="<%=src%>" width="40" height="40" style="border:1px solid gray;"></a></div>
    <div style="float:left; width:100%; text-align:center;"><a style="font-size:10px; color:#900;" href="/admin/db_update_items.asp?itemID=<%=itemID%>&deleteAltZoom=<%=iCount%>">delete</a></div>
</div>
<%
					end if
				next

				
				flashImgPath = "/productpics/swf/" & replace(itemPic, ".jpg", ".swf")
				if fs.FileExists(Server.MapPath(flashImgPath)) then %>
					<script>
						function loadFlash(pathToPlay)
						{
							var playGround = document.getElementById('imgLarge-location')
							strObj = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="300" height="300" id="movie" align="">';
							strObj = strObj + '<param name="movie" value="' + pathToPlay + '">';
							strObj = strObj + '<embed src="' + pathToPlay + '" quality="high" width="300" height="300" name="movie" align="" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer">';
							strObj = strObj + '</object>';

							playGround.innerHTML = strObj;
						}
					</script>
					<img src="/images/360icon.jpg" border="0" width="40" style="cursor:pointer;" onclick="loadFlash('<%=flashImgPath%>');" />
			<%
				end if
			else
				%>
				&nbsp;
				<%
			end if
			response.write "<br>" & vbcrlf
			if updateItems = "Add" then response.write "<span class=""biggerText"">*&nbsp;</span>"
			%>
</div>
			<div style="float:left;">Upload New Image:</div>
            <div style="clear:both;"></div>
			<br>
			<input type="file" name="itemPicUpload" size="45"><br />
			<%if updateItems = "Add" then response.write "<span class=""biggerText"">&nbsp;&nbsp;&nbsp;&nbsp;</span>"%>
			Alt Views:<br>
			<input type="file" name="itemPicUpload0" size="45"><br />
			<input type="file" name="itemPicUpload1" size="45"><br />
			<input type="file" name="itemPicUpload2" size="45"><br />
			<input type="file" name="itemPicUpload3" size="45"><br />
			<input type="file" name="itemPicUpload4" size="45"><br />
			<input type="file" name="itemPicUpload5" size="45"><br />
			<input type="file" name="itemPicUpload6" size="45"><br />
			<input type="file" name="itemPicUpload7" size="45"><br />
            Flash File:<br />
            <input type="file" name="itemFlash" size="45"><br />
		</td>
		<td width="50%" class="normaltext" valign="top" style="padding-top:10px;">
			<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center" style="font-size:12px">
				<tr>
                	<td colspan="2">
                    	<div class="tb copyPnCell">
                        	<div class="fl copyPnText">Copy From Partnumber:</div>
                            <div class="fl copyPnInput"><input type="text" name="copyPN" value="" size="18" /></div>
                            <div class="fl"><input type="button" name="myAction" value="Copy Data" onclick="copyPn(document.frmUpdateItem.copyPN.value)" /></div>
                        </div>
                    </td>
                </tr>
                <tr>
					<td width="*" align="left">Replace Keywords<br /> (case sensitive):</td>
					<td width="230px" align="right"><div id="id_replaceMsg" align="right">&nbsp;</div></td>
				</tr>
				<tr>
					<td width="100%" align="left" colspan="2">From <input type="text" name="txtReplaceFrom" value="" size="13"/> To <input type="text" name="txtReplaceTo" value="" size="13"/> 
										<input type="button" name="btnReplace" value="Replace All" onclick="javascript:replaceAll();"/>
					</td>
				</tr>
			</table>
            <table border="0" cellpadding="5" cellspacing="0" width="100%" align="left" class="normaltext">
            	<tr>
                	<td align="left">Brand:</td>
                    <td align="left">
                        <select name="BrandID" onchange="onChangeBrand(this.value);">
                            <option value="0"<%if cStr(BrandID) = "0" then response.write " selected"%>></option>
                            <%
                            SQL = "SELECT [BrandId], [BrandName] FROM WE_Brands with(nolock) ORDER BY BrandName"
                            set brandRS = oConn.execute(SQL)
                            do until brandRS.eof
                                %>
                                <option value="<%=brandRS("BrandID")%>" <%if cStr(brandRS("BrandID")) = cStr(BrandID2) then response.write " selected"%> ><%=brandRS("BrandName")%></option>
                                <%
                                brandRS.movenext
                            loop
                            %>
                        </select>                    
                        <% brandRS = null %>
                    </td>
                </tr>
            	<tr>
                	<td align="left">Type:</td>
                    <td align="left">
						<% if master then %>
                        <select name="TypeID" id="TypeID" onchange="DetermineOnOffAnimationApplies()">
                            <option value="0"<%if cStr(TypeID) = "0" then response.write " selected"%>></option>
                            <%
                            SQL = "SELECT [TypeId], [TypeName] FROM WE_Types with(nolock) ORDER BY TypeName"
                            set typeRS = oConn.execute(SQL)
                            do until typeRS.eof
                                %>
                                <option value="<%=typeRS("TypeID")%>"<%if cStr(typeRS("TypeID")) = cStr(TypeID) then response.write " selected"%>><%=typeRS("TypeName")%></option>
                                <%
                                typeRS.movenext
                            loop
                            %>
                        </select>
						<script language="javascript" type="text/javascript">
							function DetermineOnOffAnimationApplies()
							{
								selectElement = document.getElementById('TypeID');
								var o = selectElement.options[selectElement.selectedIndex].value;
								//Faceplates = 3
								divQ = document.getElementById('divShowAnimation');
								if (o == '3')
								{
									divQ.style.display = 'inline-block';
//									alert('true');
								}else{
									divQ.style.display = 'none';
//									alert('false');
								};
								//alert(o);//TypeID
							}
						</script>
						<% 'Hide inputShowAnimation when the page renders unless Faceplates is the current TypeID 
						if cStr(TypeID) = "3" then %><style>#divShowAnimation{display:none;}</style><% end if
						if isnull(showAnimation) then showAnimation = true end if %>
                        <div id="divShowAnimation" style="display:inline-block">
                            <input type="checkbox" value="inputShowAnimation" name="inputShowAnimation" id="inputShowAnimation"<% if showAnimation then response.write " checked=""checked""" end if %> /><label for="inputShowAnimation">Show On/Off Animation</label>
                        </div>
						<%
                        else
                            SQL = "SELECT typeName FROM WE_Types with(nolock) where typeID = " & prepInt(typeID)
                            session("errorSQL") = SQL
                            set typeRS = oConn.execute(SQL)
                            if typeRS.EOF then response.Write("") else response.Write(typeRS("typeName"))
                        %>
                        <input type="hidden" name="TypeID" value="<%=TypeID%>" />
                        <%
                        end if
                        typeRS = null
                        %>
                    </td>
                </tr>
            	<tr>
                	<td align="left">SubType:</td>
                    <td align="left">
						<% if master then %>
                        	<script>
							function onArtist(o) {
								if (o.value == 1031) document.getElementById('div_artist').style.display = ''
								else document.getElementById('div_artist').style.display = 'none'
							}
							</script>
                            <select name="SubTypeID" onchange="onArtist(this)">
                                <option value="0"></option>
                                <%
                                SQL = 	"	select	subtypeid, subtypeName, subTypeOrderNum" & vbcrlf & _
                                        "	from	_v_subtypematrix" & vbcrlf & _
                                        "	where	1=1" & vbcrlf & _
                                        "	order by 3" & vbcrlf
                                session("errorSQL") = SQL
                                set subtypeRS = oConn.execute(SQL)
                                do until subtypeRS.eof
                                    %>
                                    <option value="<%=subtypeRS("subtypeid")%>"<%if cStr(subtypeRS("subtypeid")) = cStr(SubTypeID) then response.write " selected"%>><%=subtypeRS("subtypeName")%></option>
                                    <%
                                    subtypeRS.movenext
                                loop
                                %>
                            </select>
                            <br />
                            <div id="div_artist" <%if cstr(SubTypeID) <> "1031" then%>style="display:none;"<%end if%>>
                            	Artist: 
                                <select name="artistID">
	                                <option value="0">Select One</option>
                                    <%
                                    SQL = 	"select id, artistName, bio, artistPic, banner1, studioname, studiopic, hidelive from fa_artist order by id"
                                    session("errorSQL") = SQL
                                    set rsArtist = oConn.execute(SQL)
                                    do until rsArtist.eof
                                        %>
                                        <option value="<%=rsArtist("id")%>"<%if cStr(rsArtist("id")) = cStr(artistID) then response.write " selected"%>><%=rsArtist("artistName")%></option>
                                        <%
                                        rsArtist.movenext
                                    loop
                                    %>
                                </select>
                            </div>
                        <%
                        else
                            SQL = 	"	select	subtypeid, subtypeName" & vbcrlf & _
                                    "	from	we_subtypes" & vbcrlf & _
                                    "	where	subtypeid = '" & subtypeID & "'" & vbcrlf
                            session("errorSQL") = SQL
                            set subtypeRS = oConn.execute(SQL)
                            if subtypeRS.EOF then 
                                response.Write("") 
                            else 
                                response.Write(subtypeRS("subtypeName")) 
                                %>
                            <input type="hidden" name="SubTypeID" value="<%=subtypeID%>" />
                                <%
                            end if
	                        subtypeRS = null
							
                            SQL = 	"select id, artistName, bio, artistPic, banner1, studioname, studiopic, hidelive from fa_artist where id = '" & artistID & "'"
                            session("errorSQL") = SQL
                            set rsArtist = oConn.execute(SQL)
                            if rsArtist.EOF then 
                                response.Write("") 
                            else 
                                response.Write(rsArtist("artistName")) 
                                %>
                            <input type="hidden" name="artistID" value="<%=artistID%>" />
                                <%
                            end if
	                        rsArtist = null
                        end if
                        %>
                    </td>
                </tr>
            	<tr>
                	<td align="left">Handsfree Type:</td>
                    <td align="left">
						<% 
                        if master then 
                            sql = "select id, name from we_handsfree order by 1"
                            set hfRS = oConn.execute(sql)
                        %>
                        <select name="HandsfreeTypeID">
                            <option value=""<%if isNull(HandsfreeType) then response.write " selected"%>></option>
                            <%
                            do until hfRS.eof
                                %>
                                <option value="<%=hfRS("id")%>" <%if HandsfreeType = hfRS("id") then response.write "selected"%> ><%=hfRS("name")%></option>
                                <%
                                hfRS.movenext
                            loop
                            %>
                        </select>
                        <%
                        else
                            sql = "select id, name from we_handsfree where id = '" & HandsfreeType & "'"
                            set hfRS = oConn.execute(sql)
                            if not hfRS.eof then
                                response.write hfRS("name")
                                %>
                                <input type="hidden" name="HandsfreeTypeID" value="<%=HandsfreeType%>" />
                                <%
                            end if
                        end if
                        %>
                    </td>
                </tr>
            	<tr>
                	<td align="left">Kit Item(s):</td>
                    <td align="left">
						<% if master then %>
                        <input name="ItemKit" type="text" length="20" maxlength="50" value="<%=ItemKit_NEW%>">
                        <% else %>
                            <%=ItemKit_NEW%>
                            <input type="hidden" name="ItemKit" value="<%=ItemKit_NEW%>" />
                        <% end if %>
                    </td>
                </tr>         
            	<tr>
                	<td align="left">Model:</td>
                    <td align="left">
						<div id="div_id_model">                    
                        <select name="ModelID">
                            <option value="0"<%if ModelID = "0" then response.write " selected"%>></option>
                            <%
                            SQL = "SELECT [ModelId], [ModelName] FROM WE_Models with(nolock) "
        
                            if "" <> BrandID2 and not isnull(BrandID2) then
                                SQL = SQL & " where brandid = '" & BrandID2 & "'"
                            end if 
                            
                            SQL = SQL & " ORDER BY ModelName"
                            set modelRS = oConn.execute(SQL)
                            do until modelRS.eof
                                %>
                                <option value="<%=modelRS("ModelID")%>"<%if modelRS("ModelID") = ModelID then response.write " selected"%>><%=modelRS("ModelName")%></option>
                                <%
                                modelRS.movenext
                            loop
                            %>
                        </select>
                        </div>
	                    <% modelRS = null %>
					</td>
                </tr>
            	<tr>
                	<td align="left">Carrier:</td>
                    <td align="left">
				        <select name="carrierID">
                            <option value="0"<%if Carrierid = "" or carrierid = 0 or isnull(carrierid) then response.write " selected"%>></option>
                            <%
                            SQL = "SELECT [Id], [CarrierName] FROM WE_Carriers with(nolock) ORDER BY listOrder"
                            set RSC = oConn.execute(SQL)
                            do until RSC.eof
                                %>
                                <option value="<%=RSC("ID")%>"<%if RSC("ID") = carrierid then response.write " selected"%>><%=RSC("carrierName")%></option>
                                <%
                                RSC.movenext
                            loop
                            RSC = null
                            %>
                        </select>
					</td>
                </tr>
                <% if not isnull(mobileLinePN) and len(mobileLinePN) > 0 then %>
            	<tr>
                	<td align="left" colspan="2">MLD PartNumber: <a href="http://www.mobileline.com/store/catalogsearch/result/?q=<%=mobileLinePN%>&x=0&y=0" target="mobileLine"><%=mobileLinePN%></a></td>
                </tr>
				<% end if %>
            	<tr>
                	<td align="left" colspan="2">
                    	<span class="biggerText">*&nbsp;</span>PartNumber&nbsp;(TTT-BBB-MMMM-##):<br />
					    <% if master then %>
                        <input type="text" name="PartNumber" size="17" maxlength="17" value="<%=PartNumber%>">
						<%
                        dim partNumberHead : partNumberHead = getPartNumberHeader(PartNumber)
                        %>
                        <select name="cbPartNumber">										
                        <%
                        if len(partNumberHead) > 10 then
                            SQL = " select distinct partnumber from we_items with (nolock) where partnumber like '" & partNumberHead & "%' order by 1"
                            set objRsPart = oConn.execute(SQL)
    
                            do until objRsPart.eof
                        %>	
                            <option value="<%=objRsPart("partnumber")%>" <%if PartNumber = objRsPart("partnumber") then %>selected<% end if%>><%=objRsPart("partnumber")%></option>
                        <%
                                objRsPart.Movenext
                            loop						
                        end if 
                        %>
                        </select>
                        <input type="hidden" name="og_PartNumber" value="<%=PartNumber%>">
                        <%
                        else
                        %>
                        <%=PartNumber%>
                        <input type="hidden" name="PartNumber" value="<%=PartNumber%>">
                        <input type="hidden" name="og_PartNumber" value="<%=PartNumber%>">
                        <% end if %>                        
					</td>
                </tr>
            	<tr>
                	<td align="left" colspan="2">
						<% if master then %>
                        <div>Primary colors to choose:</div>  
                        <div>              
                        <%
                            sql = "select id, color, colorCodes from xproductcolors order by 1"
                            set objRsColor = oConn.execute(sql)
                        %>
                            <div style="float:left; width:130px; height:25px;"><input type="radio" name="rdColor" value="0" <%if 0 = colorID then %>checked<% end if%> /> N/A</div>
                            <%
                            do until objRsColor.eof
                            %>
                           	<div style="float:left; width:130px; height:25px;">
	                            <div style="float:left; width:20px; height:25px; margin-top:2px; font-size:1px;"><input type="radio" name="rdColor" value="<%=objRsColor("id")%>" <%if objRsColor("id") = colorID then %>checked<% end if%> /></div>
                                <div style="float:left; width:20px; height:15px; margin-top:5px; font-size:1px; background-color:<%=objRsColor("colorCodes")%>; <%if objRsColor("id") = 2 then %>border:1px solid #ccc;<% end if%>">&nbsp;</div>
                                <div style="float:left; width:80px; height:25px; margin-top:4px; font-size:11px;">&nbsp;<%=objRsColor("color")%></div>
							</div>
                            <%
                                objRsColor.movenext
                            loop
                            %>
                        </div>
						<%
                        else
                        %>
                        <div style="float:left;">Primary color:&nbsp;</div>
                        <%
                            sql = "select id, color, colorCodes from xproductcolors where id = '" & colorID & "'"
                            set objRsColor = oConn.execute(sql)
                            if not objRsColor.eof then
                                %>
                                <div style="float:left; width:35px; height:15px; margin:2px 0px 0px 5px; font-size:1px; background-color:<%=objRsColor("colorCodes")%>; <%if objRsColor("id") = 2 then %>border:1px solid #ccc;<% end if%>">&nbsp;</div>
                                <div style="float:left; display:inline-block;">&nbsp;<%=objRsColor("color")%></div>
                                <%
                            else
                                response.write "N/A"
                            end if
                        end if
                        objRsColor.Close
                        set objRsColor = nothing
                        %>
                        <input type="hidden" name="rdColor" value="<%=colorID%>" />
                    </td>
                </tr>
                	<td align="left" colspan="2">
						Compatibility:<br>
						<% if master then %>
                        <textarea name="COMPATIBILITY" cols="45" rows="5"><%=COMPATIBILITY%></textarea>
                        <% else %>
                        <%=COMPATIBILITY%>
                        <input type="hidden" name="COMPATIBILITY" value="<%=COMPATIBILITY%>">
                        <% end if %>             
                    </td>
                </tr>
            </table>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="2">
<script type="text/javascript" src="/includes/js/jquery-1.2.6.pack.js" ></script>
<script type="text/javascript" src="/includes/admin/js/DbUpdateItems.js" ></script><%


'renders dynamic price panel instance (will called per site after this definition) [knguyen/20110602]
sub RenderPriceInput( byval strSiteId, byval strLabelHtml, byval strItemPriceName, byval strItemId) %>
						<%=TABLE_START %>
						<tr>
						<td class="normaltext"><%=strLabelHtml%><br>
						$<input id="Price<%=strSiteId%>" type="text" name="<%=strItemPriceName%>" size="6" maxlength="6" value="<%=FormatDecimalInput(eval( strItemPriceName))%>" <% if isId( strItemId) then if dicItemValue(strSiteId)("ActiveItemValueType")<>"OriginalPrice" then response.write "disabled=""true""" %>>
						</td>
						</tr>
						<% if not isId( strItemId) then response.Write TABLE_END: exit sub %>
						<tr>
							<td>
								<%=TABLE_START %>
								<tr>
									<td><select id="DiscountOption<%=strSiteId%>" name="DiscountOption<%=strSiteId%>" onchange="OnChangeDiscountOption( this, '<%=strSiteId %>')"><%
										for each strSelectOption in split( "NoSale|%off|$","|")
											dim blnSelectOptionChecked: blnSelectOptionChecked=false
											select case( strSelectOption)
												case "%off": if dicItemValue(strSiteId)("ActiveItemValueType")="DiscountPercent" then blnSelectOptionChecked=true
												case "$": if dicItemValue(strSiteId)("ActiveItemValueType")="DiscountPrice" then blnSelectOptionChecked=true
											end select
											%><option value="<%=strSelectOption%>"<% if blnSelectOptionChecked then response.write " selected" %>><%=strSelectOption%></option><%
										next %>
									</select></td>
									<td><div id="divDiscountValue<%=strSiteId%>" style="display:<% if dicItemValue(strSiteId)("ActiveItemValueType")="OriginalPrice" then response.write "none" else response.write "block" %>"><input type="text" field="DiscountValue" id="DiscountValue<%=strSiteId%>" name="DiscountValue<%=strSiteId%>" size="6" maxlength="6" value="<%

		response.write FormatDecimalInput(dicItemValue(strSiteId)( dicItemValue(strSiteId)("ActiveItemValueType"))) 

%>" onfocus="strSiteId='<%=strSiteId%>'"></div>
									</div></td>
								</tr>
								<%=TABLE_END %>
							</td>
						</tr>
						<%=TABLE_END %>
						<input type="hidden" id="DiscountPrice<%=strSiteId%>" name="DiscountPrice<%=strSiteId%>" size="6" maxlength="6" value="<%=FormatDecimalInput(dicItemValue(strSiteId)("DiscountPrice")) %>">
						<input type="hidden" id="DiscountPercent<%=strSiteId%>" name="DiscountPercent<%=strSiteId%>" size="6" maxlength="6" value="<%=FormatDecimalInput(dicItemValue(strSiteId)("DiscountPercent")) %>"><%
dim strEffectiveOriginalPrice: strEffectiveOriginalPrice=dicItemValue(strSiteId)("OriginalPrice")
if not HasLen( strEffectiveOriginalPrice) then strEffectiveOriginalPrice=eval( strItemPriceName) %>
						<input type="hidden" id="OriginalPrice<%=strSiteId%>" name="OriginalPrice<%=strSiteId%>" size="6" maxlength="6" value="<%=FormatDecimalInput( strEffectiveOriginalPrice) %>"><%

end sub ' RenderPriceInput



%>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="12%" class="normaltext" align="center" valign="top">
						<span class="biggerText">*&nbsp;</span>Retail&nbsp;Price<br>
                        <% if master then %>
						$<input type="text" name="price_Retail" size="6" maxlength="6" value="<%=price_Retail%>">
                        <% else %>
                        $<%=price_Retail%>
                        <input type="hidden" name="price_Retail" value="<%=price_Retail%>">
                        <% end if %>
					</td><%
					 
					if master then %>
					<td width="12%" class="normaltext" align="center" valign="top"><%

					call RenderPriceInput( WE_ID, "<span class=""biggerText"">*&nbsp;</span>WE&nbsp;Price", "price_Our", ItemID) 

					%></td>
					<td width="12%" class="normaltext" align="center" valign="top"><%

					call RenderPriceInput( CA_ID, "CA&nbsp;Price", "price_CA", ItemID) 

					%></td>
					<td width="12%" class="normaltext" align="center" valign="top"><%

					call RenderPriceInput( CO_ID, "CO&nbsp;Price", "price_CO", ItemID) 

					%></td>
					<td width="14%" class="normaltext" align="center" valign="top"><%

					call RenderPriceInput( PS_ID, "Phonesale&nbsp;Price", "price_PS", ItemID) 

					%></td>
                    <td width="14%" class="normaltext" align="center" valign="top"><%

					call RenderPriceInput( ER_ID, "TabletMall&nbsp;Price", "price_ER", ItemID) 

					%></td><% 
					
					else 
					
					%>					
					<td width="12%" class="normaltext" align="center" valign="top">
						<span class="biggerText">*&nbsp;</span>WE&nbsp;Price<br>
                        $<%=price_Our%>
                        <input type="hidden" name="price_Our" value="<%=price_Our%>">
					</td>
					<td width="12%" class="normaltext" align="center" valign="top">
						CA&nbsp;Price<br>
                        $<%=price_CA%>
                        <input type="hidden" name="price_CA" value="<%=price_CA%>">
					</td>
					<td width="12%" class="normaltext" align="center" valign="top">
						CO&nbsp;Price<br>
                        $<%=price_CO%>
                        <input type="hidden" name="price_CO" value="<%=price_CO%>">
					</td>
					<td width="14%" class="normaltext" align="center" valign="top">
						Phonesale&nbsp;Price<br>
                        $<%=price_PS%>
                        <input type="hidden" name="price_PS" value="<%=price_PS%>">
					</td>
                    <td width="14%" class="normaltext" align="center" valign="top">
						TabletMall&nbsp;Price<br>
                        $<%=price_ER%>
                        <input type="hidden" name="price_ER" value="<%=price_ER%>">
					</td><% 
					
					end if %>
				</tr>
                <tr>
                	<td width="14%" class="normaltext" align="center" valign="top">
						Buy.com&nbsp;Price<br>
                        <% if master then %>
						$<input type="text" name="price_Buy" size="6" maxlength="6" value="<%=price_Buy%>">
                        <% else %>
                        $<%=price_Buy%>
                        <input type="hidden" name="price_Buy" value="<%=price_Buy%>">
                        <% end if %>
					</td>
					<td width="12%" class="normaltext" align="center" valign="top">
						COGS<br>
                        <% if master then %>
						$<input type="text" name="COGS" size="6" maxlength="6" value="<%=COGS%>">
                        <% else %>
                        $<%=COGS%>
                        <input type="hidden" name="COGS" value="<%=COGS%>">
                        <% end if %>
					</td>
					<td width="12%" class="normaltext" align="center" valign="top">
						No&nbsp;Discount<br>
                        <% if master then %>
						<input type="checkbox" name="NoDiscount" value="1"<%if NoDiscount then response.write " checked"%>>
                        <%
						else
							if NoDiscount = 1 then response.Write("True") else response.Write("False")
						%>
                        <input type="hidden" name="NoDiscount" value="<%=NoDiscount%>">
                        <% end if %>
					</td>
                </tr>
			</table>
		</td>
	</tr>
    <% if initialPartNumber <> "" and initialCogs <> "" and initialQuantity <> "" and initialDateEntd <> "" and initialAdminName <> "" then%>
    <tr>
    	<td colspan="2">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                    <td align="center" valign="top">Initial Part Number<br />
					<%=initialPartNumber%>
                    </td>
                    <td align="center" valign="top">Initial COGS<br />
                    <%=formatcurrency(initialCogs)%>
                    </td>
                    <td align="center" valign="top">Initial Quantity<br />
                    <%=initialQuantity%>
                    </td>
                    <td align="center" valign="top">Date Entered<br />
                    <%=initialDateEntd%>
                    </td>
                    <td align="center" valign="top">Entered By<br />
                    <%=initialAdminName%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <% end if %>
	<tr>
		<td width="100%" colspan="2">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="20%" class="normaltext" align="center" valign="top">
						UPCCode<br>
                        <% if master then %>
						<input type="text" name="UPCCode" size="20" maxlength="20" value="<%=UPCCode%>">
                        <% else %>
                        <%=UPCCode%>
                        <input type="hidden" name="UPCCode" value="<%=UPCCode%>">
                        <% end if %>
					</td>
					<td width="20%" class="normaltext" align="center" valign="top">
						AMZN_sku<br>
                        <% if master then %>
						<input type="text" name="AMZN_sku" size="20" maxlength="20" value="<%=AMZN_sku%>">
                        <% else %>
                        <%=AMZN_sku%>
                        <input type="hidden" name="AMZN_sku" value="<%=AMZN_sku%>">
                        <% end if %>
					</td>
					<td width="15%" class="normaltext" align="center" valign="top">
						flag1<br>
                        <% if master then %>
						<input type="text" name="flag1" size="5" maxlength="5" value="<%=flag1%>">
                        <% else %>
                        <%=flag1%>
                        <input type="hidden" name="flag1" value="<%=flag1%>">
                        <% end if %>
					</td>
					<td width="20%" class="normaltext" align="center" valign="top">
						<span class="biggerText">*&nbsp;</span>itemWeight<br>
                        <% if master then %>
						<input type="text" name="itemWeight" size="6" maxlength="6" value="<%=itemWeight%>">
                        <% else %>
                        <%=itemWeight%>
                        <input type="hidden" name="itemWeight" value="<%=itemWeight%>">
                        <% end if %>
					</td>
					<td width="25%" class="normaltext" align="center" valign="top">
						itemDimensions<br>
                        <% if master then %>
						<input type="text" name="itemDimensions" size="20" maxlength="50" value="<%=itemDimensions%>">
                        <% else %>
                        <%=itemDimensions%>
                        <input type="hidden" name="itemDimensions" value="<%=itemDimensions%>">
                        <% end if %>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="2">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="normaltext" align="center" valign="top">
						Seasonal<br>
                        <% if master then %>
						<input type="checkbox" name="Seasonal" value="1"<%=isChecked(Seasonal)%>>
                        <% else %>
                        <%=Seasonal%>
                        <input type="hidden" name="Seasonal" value="<%=Seasonal%>">
                        <% end if %>
					</td>
					<td class="normaltext" align="center" valign="top">
						Sports<br>
                        <% if master then %>
						<input type="text" name="Sports" size="2" maxlength="2" value="<%=Sports%>">
                        <% else %>
                        <%=Sports%>
                        <input type="hidden" name="Sports" value="<%=Sports%>">
                        <% end if %>
					</td>
					<td class="normaltext" align="center" valign="top">
						hotDeal<br>
                        <% if master then %>
						<input type="checkbox" name="hotDeal" value="1"<%=isChecked(hotDeal)%>>
                        <% else %>
                        <%=hotDeal%>
                        <input type="hidden" name="hotDeal" value="<%=hotDeal%>">
                        <% end if %>
					</td>
					<td class="normaltext" align="center" valign="top">
						hideLive<br>
						<input type="checkbox" name="hideLive" value="1"<%=isChecked(hideLive)%>>
					</td>
                    <td class="normaltext" align="center" valign="top">
						Customizable<br>
						<input type="checkbox" name="customize" value="1"<%=isChecked(customize)%>>
					</td>
					<td class="normaltext" align="center" valign="top">
						vendor<br>
                        <% if master then %>
						<input type="text" name="vendor" size="3" maxlength="11" value="<%=vendor%>">
                        <% else %>
                        <%=vendor%>
                        <input type="hidden" name="vendor" value="<%=vendor%>">
                        <% end if %>
					</td>
					<td class="normaltext" align="center" valign="top">
						inv_qty:<br>
						<%=inv_qty%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="50%" class="normaltext" align="center">
			Additional Hardware/Software URL:<br>
            <% if master then %>
			<input type="text" name="download_URL" size="50" maxlength="250" value="<%=download_URL%>">
            <% else %>
            <%=download_URL%>
            <input type="hidden" name="download_URL" value="<%=download_URL%>">
            <% end if %>
		</td>
		<td width="50%" class="normaltext" align="center">
			Additional Hardware/Software TEXT:<br>
            <% if master then %>
			<input type="text" name="download_TEXT" size="50" maxlength="250" value="<%=download_TEXT%>">
            <% else %>
            <%=download_TEXT%>
            <input type="hidden" name="download_TEXT" value="<%=download_TEXT%>">
            <% end if %>
		</td>
	</tr>
    <tr>
    	<td width="50%">
        	<div style="float:left; padding-top:2px;">Battery Capacity:</div>
            <div style="float:left; margin-left:5px;"><input type="text" name="batteryCapacity" value="<%=batteryCapacity%>" style="width:55px;" /></div>
            <div style="float:left; margin-left:5px; padding-top:2px;">mAh</div>
        </td>
    	<td width="50%">&nbsp;</td>
    </tr>
    <tr>
    	<td width="100%" colspan="2">
        	<div id="weTextFields_tab" style="float:left; background-color:#ccc; color:#000; font-weight:bold; cursor:pointer; padding:5px; border-right:1px solid #ccc;" onclick="showFields('weTextFields')">WE Text Fields</div>
            <div id="coTextFields_tab" style="float:left; background-color:#000; color:#FFF; font-weight:bold; cursor:pointer; padding:5px; border-right:1px solid #FFF;" onclick="showFields('coTextFields')">CO Text Fields</div>
            <div id="caTextFields_tab" style="float:left; background-color:#000; color:#FFF; font-weight:bold; cursor:pointer; padding:5px; border-right:1px solid #FFF;" onclick="showFields('caTextFields')">CA Text Fields</div>
            <div id="psTextFields_tab" style="float:left; background-color:#000; color:#FFF; font-weight:bold; cursor:pointer; padding:5px; border-right:1px solid #FFF;" onclick="showFields('psTextFields')">PS Text Fields</div>
            <div id="erTextFields_tab" style="float:left; background-color:#000; color:#FFF; font-weight:bold; cursor:pointer; padding:5px;" onclick="showFields('erTextFields')">TM Text Fields</div>
            <div style="float:left; padding:5px;"><input type="button" name="btnClearTextFields" value="Clear Text Fields" onclick="clearTextFields();" /></div>
            <div style="float:left; padding:5px;"><input type="button" name="btnFeatureTemplate" value="Bullet Templates" onclick="toggle('id_feature_template');" /></div>
            <div style="float:left; width:100%; position:relative;">
                <div id="id_feature_template" style="float:left; display:none; position:absolute; top:0px; left:0px; padding:5px; width:820px; height:400px; border:2px solid #000; background-color:#F2F2F2; z-index:1000px;">
                    <div style="float:left; width:200px;" id="id_prefix">
                        <div style="float:left; width:100%; height:30px; text-align:center; font-weight:bold;">
                            <select name="cbPrefix" onchange="ajaxLoadPrefix(this.value);" style="width:180px;">
                                <option value="">-select-</option>
                                <%
                                sql = 	"select distinct templateName from XPhoneFeatureTemplate order by 1"
                                set rsPrefix = oConn.execute(sql)
                                do until rsPrefix.eof
                                %>
                                <option value="<%=rsPrefix("templateName")%>"><%=rsPrefix("templateName")%></option>
                                <%
                                    rsPrefix.movenext
                                loop
                                %>
                            </select>                    
                        </div>
                        <%for i=1 to 10%>
                            <div style="width:100%; float:left; margin-top:5px;">
                                <div style="float:left; width:20px; padding-top:5px;"><%=i%>.</div>
                                <textarea id="id_prefix<%=i%>" name="textPrefix<%=i%>" cols="18" rows="1" style="height:18px;"></textarea>
                            </div>
                        <%next%>                    
                    </div>
                    <div style="float:left; width:620px;">
                        <div style="float:left; width:100%; height:30px; text-align:center; font-size:14px; font-weight:bold;">BULLET POINTS</div>
                        <%for i=1 to 10%>
                            <div style="width:100%; float:left; margin-top:5px;">
                                <textarea id="id_textTemplate<%=i%>" name="textTemplate<%=i%>" cols="70" rows="1" style="height:18px;"></textarea>
                            </div>
                        <%next%>
                    </div>
                    <div style="padding-top:10px; float:left; width:100%;" align="left">
                    	<div style="float:left; padding-right:100px; border-right:1px dotted #ccc;">Template Name: <input type="text" name="txtTemplateName" value="" />&nbsp;<input type="button" name="btn" value="ADD THIS" onclick="ajaxAddBulletTemplate()" /></div>
                    	<div style="float:right; padding-right:30px;" align="right">
                        	<input type="button" name="btnCopyWE" value="Copy to WE" onclick="copyBullets(0);" />&nbsp;<input type="button" name="btnCopyCO" value="Copy to CO" onclick="copyBullets(2);" />&nbsp;<input type="button" name="btnCopyPS" value="Copy to PS" onclick="copyBullets(3);" />
						</div>
                    </div>
                </div>            
            </div>
        </td>
    </tr>
	<tr>
		<td width="100%" colspan="2" class="normaltext" align="center" id="weTextFields">
			<table border="1" bordercolor="#0000FF" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="100%" align="center">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="100%" class="biggerText">
									<span style="color:#0000FF">WE Fields:</span>
								</td>
							</tr>
							<tr>
								<td width="100%" class="normaltext" align="left" style="padding-left:20px;">
                                	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="normaltext">
                                    	<tr>
                                        	<td align="left" valign="top" colspan="2">
	                                            <span class="biggerText">*&nbsp;</span>WE Item Desc<br>
												<textarea name="itemDesc" cols="84" rows="1" onkeyup="enableUpdateBtn('div_itemdesc');"><%=itemDesc%></textarea>
                                            </td>
                                            <td align="left" valign="bottom" width="100">
	                                            <div id="div_itemdesc" style="display:none;"><input type="button" name="btnItemDesc_WE" value="Update" onclick="ajaxUpdateDesc('itemdesc', document.frmUpdateItem.itemDesc, 'div_itemdesc')" /></div>
                                                <div id="div_itemdesc_statusbox" style="display:none;"></div>
                                            </td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top" colspan="2">
                                                <span class="biggerText">*&nbsp;</span>WE Item Long Detail:<br>
                                                <textarea name="itemLongDetail" cols="84" rows="8" onkeyup="enableUpdateBtn('div_itemlongdetail');"><%=itemLongDetail%></textarea>
                                            </td>
                                            <td align="left" valign="middle" width="100">
	                                            <div id="div_itemlongdetail" style="display:none;"><input type="button" name="btnItemLongDetail_WE" value="Update" onclick="ajaxUpdateDesc('itemlongdetail', document.frmUpdateItem.itemLongDetail, 'div_itemlongdetail')" /></div>
												<div id="div_itemlongdetail_statusbox" style="display:none;"></div>
                                            </td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top" colspan="2">
										        WE Item Info (Staff Reviews):<br>
                                                <textarea name="itemInfo" cols="84" rows="6" onkeyup="enableUpdateBtn('div_iteminfo');"><%=itemInfo%></textarea>
                                            </td>
                                            <td align="left" valign="middle" width="100">
	                                            <div id="div_iteminfo" style="display:none;"><input type="button" name="btnItemInfo_WE" value="Update" onclick="ajaxUpdateDesc('iteminfo', document.frmUpdateItem.itemInfo, 'div_iteminfo')" /></div>
												<div id="div_iteminfo_statusbox" style="display:none;"></div>
                                            </td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top" width="110">WE Bullet Point 1:</td>
                                        	<td align="left" valign="top"><textarea name="BULLET1" cols="70" rows="1" onkeyup="enableUpdateBtn('div_bullet1');"><%=BULLET1%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_bullet1" style="display:none;"><input type="button" name="btnBullet1" value="Update" onclick="ajaxUpdateDesc('bullet1', document.frmUpdateItem.BULLET1, 'div_bullet1')" /></div>
                                                <div id="div_bullet1_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">WE Bullet Point 2:</td>
                                        	<td align="left" valign="top"><textarea name="BULLET2" cols="70" rows="1" onkeyup="enableUpdateBtn('div_bullet2');"><%=BULLET2%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_bullet2" style="display:none;"><input type="button" name="btnBullet2" value="Update" onclick="ajaxUpdateDesc('bullet2', document.frmUpdateItem.BULLET2, 'div_bullet2')" /></div>
                                                <div id="div_bullet2_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">WE Bullet Point 3:</td>
                                        	<td align="left" valign="top"><textarea name="BULLET3" cols="70" rows="1" onkeyup="enableUpdateBtn('div_bullet3');"><%=BULLET3%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_bullet3" style="display:none;"><input type="button" name="btnBullet3" value="Update" onclick="ajaxUpdateDesc('bullet3', document.frmUpdateItem.BULLET3, 'div_bullet3')" /></div>
                                                <div id="div_bullet3_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">WE Bullet Point 4:</td>
                                        	<td align="left" valign="top"><textarea name="BULLET4" cols="70" rows="1" onkeyup="enableUpdateBtn('div_bullet4');"><%=BULLET4%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_bullet4" style="display:none;"><input type="button" name="btnBullet4" value="Update" onclick="ajaxUpdateDesc('bullet4', document.frmUpdateItem.BULLET4, 'div_bullet4')" /></div>
                                                <div id="div_bullet4_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">WE Bullet Point 5:</td>
                                        	<td align="left" valign="top"><textarea name="BULLET5" cols="70" rows="1" onkeyup="enableUpdateBtn('div_bullet5');"><%=BULLET5%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_bullet5" style="display:none;"><input type="button" name="btnBullet5" value="Update" onclick="ajaxUpdateDesc('bullet5', document.frmUpdateItem.BULLET5, 'div_bullet5')" /></div>
                                                <div id="div_bullet5_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">WE Bullet Point 6:</td>
                                        	<td align="left" valign="top"><textarea name="BULLET6" cols="70" rows="1" onkeyup="enableUpdateBtn('div_bullet6');"><%=BULLET6%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_bullet6" style="display:none;"><input type="button" name="btnBullet6" value="Update" onclick="ajaxUpdateDesc('bullet6', document.frmUpdateItem.BULLET6, 'div_bullet6')" /></div>
                                                <div id="div_bullet6_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">WE Bullet Point 7:</td>
                                        	<td align="left" valign="top"><textarea name="BULLET7" cols="70" rows="1" onkeyup="enableUpdateBtn('div_bullet7');"><%=BULLET7%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_bullet7" style="display:none;"><input type="button" name="btnBullet7" value="Update" onclick="ajaxUpdateDesc('bullet7', document.frmUpdateItem.BULLET7, 'div_bullet7')" /></div>
                                                <div id="div_bullet7_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">WE Bullet Point 8:</td>
                                        	<td align="left" valign="top"><textarea name="BULLET8" cols="70" rows="1" onkeyup="enableUpdateBtn('div_bullet8');"><%=BULLET8%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_bullet8" style="display:none;"><input type="button" name="btnBullet8" value="Update" onclick="ajaxUpdateDesc('bullet8', document.frmUpdateItem.BULLET8, 'div_bullet8')" /></div>
                                                <div id="div_bullet8_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">WE Bullet Point 9:</td>
                                        	<td align="left" valign="top"><textarea name="BULLET9" cols="70" rows="1" onkeyup="enableUpdateBtn('div_bullet9');"><%=BULLET9%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_bullet9" style="display:none;"><input type="button" name="btnBullet9" value="Update" onclick="ajaxUpdateDesc('bullet9', document.frmUpdateItem.BULLET9, 'div_bullet9')" /></div>
                                                <div id="div_bullet9_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">WE Bullet Point 10:</td>
                                        	<td align="left" valign="top"><textarea name="BULLET10" cols="70" rows="1" onkeyup="enableUpdateBtn('div_bullet10');"><%=BULLET10%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_bullet10" style="display:none;"><input type="button" name="btnBullet10" value="Update" onclick="ajaxUpdateDesc('bullet10', document.frmUpdateItem.BULLET10, 'div_bullet10')" /></div>
                                                <div id="div_bullet10_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    </table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="2" class="normaltext" align="center" id="caTextFields" style="display:none;">
			<table border="1" bordercolor="#339933" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="100%" align="center">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="100%" class="biggerText">
									<span style="color:#339933">CA Fields:</span>
								</td>
							</tr>
							<tr>
								<td width="100%" class="normaltext" align="left" style="padding-left:20px;">
                                	<table border="0" cellpadding="0" cellspacing="0" width="100%" class="normaltext">
                                    	<tr>
                                        	<td align="left" valign="top">
												<span class="biggerText">*&nbsp;</span>CA Item Desc<br>
												<textarea name="itemDesc_CA" cols="80" rows="1" onkeyup="enableUpdateBtn('div_itemdesc_ca');"><%=itemDesc_CA%></textarea>
                                            </td>
                                            <td align="left" valign="bottom">
	                                            <div id="div_itemdesc_ca" style="display:none;"><input type="button" name="btnItemDesc_CA" value="Update" onclick="ajaxUpdateDesc('itemdesc_ca', document.frmUpdateItem.itemDesc_CA, 'div_itemdesc_ca')" /></div>
												<div id="div_itemdesc_ca_statusbox" style="display:none;"></div>
                                            </td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">
									            <span class="biggerText">*&nbsp;</span>CA Item Long Detail:<br>
                                                <textarea name="itemLongDetail_CA" cols="80" rows="8" onkeyup="enableUpdateBtn('div_itemlongdetail_ca');"><%=itemLongDetail_CA%></textarea> 
                                            </td>
                                            <td align="left" valign="middle">
	                                            <div id="div_itemlongdetail_ca" style="display:none;"><input type="button" name="btnItemLongDetail_CA" value="Update" onclick="ajaxUpdateDesc('itemlongdetail_ca', document.frmUpdateItem.itemLongDetail_CA, 'div_itemlongdetail_ca')" /></div>
												<div id="div_itemlongdetail_ca_statusbox" style="display:none;"></div>
                                            </td>
                                        </tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="2" class="normaltext" align="center" id="coTextFields" style="display:none;">
			<table border="1" bordercolor="#FF6666" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="100%" align="center">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="100%" class="biggerText">
									<span style="color:#FF6666">CO Fields:</span>
								</td>
							</tr>
							<tr>
								<td width="100%" class="normaltext" align="center">
									<table border="1" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="50%" class="normaltext">
												<%
												if itemPic_CO <> "" and not isNull(itemPic_CO) then
													%>
													<p><img src="http://www.cellularoutfitter.com/productpics/big/<%=itemPic_CO%>" border="0"></p>
													<%
												end if
												%>
												<%if fs.fileExists(server.mappath("\productpics_co\big\zoom") & "\" & itemPic_CO) then%>                                                
                                                    <div style="float:left; padding:3px; border:1px solid #ccc; background-color:#999; color:#fff;" onmouseover="document.getElementById('id_zoomimage_co').style.display=''" onmouseout="document.getElementById('id_zoomimage_co').style.display='none'">Mouseover to see a zoom image</div>
                                                    <div style="float:left; position:relative;">
                                                        <div id="id_zoomimage_co" style="position:absolute; top:-400px; left:0px; display:none; background-color:#fff; border:2px solid #000;"><img src="http://www.cellularoutfitter.com/productpics/big/zoom/<%=itemPic_CO%>" border="0" /></div>
                                                    </div>
                                                <%end if%>                                                
											</td>
											<td width="50%" class="normaltext">
												<%
												if updateItems = "Add" then response.write "<span class=""biggerText"">*&nbsp;</span>"
												%>
												Upload New Image:<br>
												<input type="file" name="itemPic_CO_Upload" size="50"><br>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="100%" class="normaltext" align="left" style="padding-left:20px;">
                                	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="normaltext">
                                    	<tr>
                                        	<td align="left" valign="top" colspan="2">
                                                <span class="biggerText">*&nbsp;</span>CO Item Desc<br>
                                                <textarea name="itemDesc_CO" cols="84" rows="1" onkeyup="enableUpdateBtn('div_itemdesc_co');"><%=itemDesc_CO%></textarea>
                                            </td>
                                            <td align="left" valign="bottom" width="100">
	                                            <div id="div_itemdesc_co" style="display:none;"><input type="button" name="btnItemDesc_CO" value="Update" onclick="ajaxUpdateDesc('itemdesc_co', document.frmUpdateItem.itemDesc_CO, 'div_itemdesc_co')" /></div>
												<div id="div_itemdesc_co_statusbox" style="display:none;"></div>
                                            </td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top" colspan="2">
												<span class="biggerText">*&nbsp;</span>CO Item Long Detail:<br>
												<textarea name="itemLongDetail_CO" cols="84" rows="8" onkeyup="enableUpdateBtn('div_itemlongdetail_co');"><%=itemLongDetail_CO%></textarea>
                                            </td>
                                            <td align="left" valign="middle" width="100">
	                                            <div id="div_itemlongdetail_co" style="display:none;"><input type="button" name="btnItemLongDetail_CO" value="Update" onclick="ajaxUpdateDesc('itemLongDetail_CO', document.frmUpdateItem.itemLongDetail_CO, 'div_itemlongdetail_co')" /></div>
												<div id="div_itemlongdetail_co_statusbox" style="display:none;"></div>
                                            </td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top" width="110">CO Bullet Point 1:</td>
                                        	<td align="left" valign="top"><textarea name="POINT1" cols="70" rows="1" onkeyup="enableUpdateBtn('div_point1');"><%=POINT1%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_point1" style="display:none;"><input type="button" name="btnPoint1" value="Update" onclick="ajaxUpdateDesc('point1', document.frmUpdateItem.POINT1, 'div_point1')" /></div>
												<div id="div_point1_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">CO Bullet Point 2:</td>
                                        	<td align="left" valign="top"><textarea name="POINT2" cols="70" rows="1" onkeyup="enableUpdateBtn('div_point2');"><%=POINT2%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_point2" style="display:none;"><input type="button" name="btnPoint2" value="Update" onclick="ajaxUpdateDesc('point2', document.frmUpdateItem.POINT2, 'div_point2')" /></div>
												<div id="div_point2_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">CO Bullet Point 3:</td>
                                        	<td align="left" valign="top"><textarea name="POINT3" cols="70" rows="1" onkeyup="enableUpdateBtn('div_point3');"><%=POINT3%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_point3" style="display:none;"><input type="button" name="btnPoint3" value="Update" onclick="ajaxUpdateDesc('point3', document.frmUpdateItem.POINT3, 'div_point3')" /></div>
												<div id="div_point3_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">CO Bullet Point 4:</td>
                                        	<td align="left" valign="top"><textarea name="POINT4" cols="70" rows="1" onkeyup="enableUpdateBtn('div_point4');"><%=POINT4%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_point4" style="display:none;"><input type="button" name="btnPoint4" value="Update" onclick="ajaxUpdateDesc('point4', document.frmUpdateItem.POINT4, 'div_point4')" /></div>
												<div id="div_point4_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">CO Bullet Point 5:</td>
                                        	<td align="left" valign="top"><textarea name="POINT5" cols="70" rows="1" onkeyup="enableUpdateBtn('div_point5');"><%=POINT5%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_point5" style="display:none;"><input type="button" name="btnPoint5" value="Update" onclick="ajaxUpdateDesc('point5', document.frmUpdateItem.POINT5, 'div_point5')" /></div>
												<div id="div_point5_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">CO Bullet Point 6:</td>
                                        	<td align="left" valign="top"><textarea name="POINT6" cols="70" rows="1" onkeyup="enableUpdateBtn('div_point6');"><%=POINT6%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_point6" style="display:none;"><input type="button" name="btnPoint6" value="Update" onclick="ajaxUpdateDesc('point6', document.frmUpdateItem.POINT6, 'div_point6')" /></div>
												<div id="div_point6_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">CO Bullet Point 7:</td>
                                        	<td align="left" valign="top"><textarea name="POINT7" cols="70" rows="1" onkeyup="enableUpdateBtn('div_point7');"><%=POINT7%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_point7" style="display:none;"><input type="button" name="btnPoint7" value="Update" onclick="ajaxUpdateDesc('point7', document.frmUpdateItem.POINT7, 'div_point7')" /></div>
												<div id="div_point7_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">CO Bullet Point 8:</td>
                                        	<td align="left" valign="top"><textarea name="POINT8" cols="70" rows="1" onkeyup="enableUpdateBtn('div_point8');"><%=POINT8%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_point8" style="display:none;"><input type="button" name="btnPoint8" value="Update" onclick="ajaxUpdateDesc('point8', document.frmUpdateItem.POINT8, 'div_point8')" /></div>
												<div id="div_point8_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">CO Bullet Point 9:</td>
                                        	<td align="left" valign="top"><textarea name="POINT9" cols="70" rows="1" onkeyup="enableUpdateBtn('div_point9');"><%=POINT9%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_point9" style="display:none;"><input type="button" name="btnPoint9" value="Update" onclick="ajaxUpdateDesc('point9', document.frmUpdateItem.POINT9, 'div_point9')" /></div>
												<div id="div_point9_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">CO Bullet Point 10:</td>
                                        	<td align="left" valign="top"><textarea name="POINT10" cols="70" rows="1" onkeyup="enableUpdateBtn('div_point10');"><%=POINT10%></textarea></td>
                                            <td align="left" valign="top" width="100">
                                            	<div id="div_point10" style="display:none;"><input type="button" name="btnPoint10" value="Update" onclick="ajaxUpdateDesc('point10', document.frmUpdateItem.POINT10, 'div_point10')" /></div>
												<div id="div_point10_statusbox" style="display:none;"></div>
											</td>
                                        </tr>   
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="2" class="normaltext" align="center" id="psTextFields" style="display:none;">
			<table border="1" bordercolor="#3399FF" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="100%" align="center">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="100%" class="biggerText">
									<span style="color:#3399FF">Phonesale Fields:</span>
								</td>
							</tr>
							<tr>
								<td width="100%" class="normaltext" align="left" style="padding-left:20px;">
                                	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="normaltext">
                                    	<tr>
                                        	<td align="left" valign="top" colspan="2">
												<span class="biggerText">*&nbsp;</span>Phonesale Item Desc<br>
												<textarea name="itemDesc_PS" cols="81" rows="1" onkeyup="enableUpdateBtn('div_itemdesc_ps');"><%=itemDesc_PS%></textarea>
                                            </td>
                                            <td align="left" valign="bottom">
	                                            <div id="div_itemdesc_ps" style="display:none;"><input type="button" name="btnItemDesc_PS" value="Update" onclick="ajaxUpdateDesc('itemdesc_ps', document.frmUpdateItem.itemDesc_PS, 'div_itemdesc_ps')" /></div>
												<div id="div_itemdesc_ps_statusbox" style="display:none;"></div>
                                            </td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top" colspan="2">
												<span class="biggerText">*&nbsp;</span>Phonesale Item Long Detail:<br>
												<textarea name="itemLongDetail_PS" cols="81" rows="8" onkeyup="enableUpdateBtn('div_itemlongdetail_ps');"><%=itemLongDetail_PS%></textarea>
                                            </td>
                                            <td align="left" valign="middle">
	                                            <div id="div_itemlongdetail_ps" style="display:none;"><input type="button" name="btnItemLongDetail_PS" value="Update" onclick="ajaxUpdateDesc('itemLongDetail_PS', document.frmUpdateItem.itemLongDetail_PS, 'div_itemlongdetail_ps')" /></div>
												<div id="div_itemlongdetail_ps_statusbox" style="display:none;"></div>
                                            </td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">Phonesale Feature 1:</td>
                                        	<td align="left" valign="top"><textarea name="FEATURE1" cols="70" rows="1" onkeyup="enableUpdateBtn('div_feature1');"><%=FEATURE1%></textarea></td>
                                            <td align="left" valign="top">
                                            	<div id="div_feature1" style="display:none;"><input type="button" name="btnFeature1" value="Update" onclick="ajaxUpdateDesc('FEATURE1', document.frmUpdateItem.FEATURE1, 'div_feature1')" /></div>
												<div id="div_feature1_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">Phonesale Feature 2:</td>
                                        	<td align="left" valign="top"><textarea name="FEATURE2" cols="70" rows="1" onkeyup="enableUpdateBtn('div_feature2');"><%=FEATURE2%></textarea></td>
                                            <td align="left" valign="top">
                                            	<div id="div_feature2" style="display:none;"><input type="button" name="btnFeature2" value="Update" onclick="ajaxUpdateDesc('FEATURE2', document.frmUpdateItem.FEATURE2, 'div_feature2')" /></div>
												<div id="div_feature2_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">Phonesale Feature 3:</td>
                                        	<td align="left" valign="top"><textarea name="FEATURE3" cols="70" rows="1" onkeyup="enableUpdateBtn('div_feature3');"><%=FEATURE3%></textarea></td>
                                            <td align="left" valign="top">
                                            	<div id="div_feature3" style="display:none;"><input type="button" name="btnFeature3" value="Update" onclick="ajaxUpdateDesc('FEATURE3', document.frmUpdateItem.FEATURE3, 'div_feature3')" /></div>
												<div id="div_feature3_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">Phonesale Feature 4:</td>
                                        	<td align="left" valign="top"><textarea name="FEATURE4" cols="70" rows="1" onkeyup="enableUpdateBtn('div_feature4');"><%=FEATURE4%></textarea></td>
                                            <td align="left" valign="top">
                                            	<div id="div_feature4" style="display:none;"><input type="button" name="btnFeature4" value="Update" onclick="ajaxUpdateDesc('FEATURE4', document.frmUpdateItem.FEATURE4, 'div_feature4')" /></div>
												<div id="div_feature4_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">Phonesale Feature 5:</td>
                                        	<td align="left" valign="top"><textarea name="FEATURE5" cols="70" rows="1" onkeyup="enableUpdateBtn('div_feature5');"><%=FEATURE5%></textarea></td>
                                            <td align="left" valign="top">
                                            	<div id="div_feature5" style="display:none;"><input type="button" name="btnFeature5" value="Update" onclick="ajaxUpdateDesc('FEATURE5', document.frmUpdateItem.FEATURE5, 'div_feature5')" /></div>
												<div id="div_feature5_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">Phonesale Feature 6:</td>
                                        	<td align="left" valign="top"><textarea name="FEATURE6" cols="70" rows="1" onkeyup="enableUpdateBtn('div_feature6');"><%=FEATURE6%></textarea></td>
                                            <td align="left" valign="top">
                                            	<div id="div_feature6" style="display:none;"><input type="button" name="btnFeature6" value="Update" onclick="ajaxUpdateDesc('FEATURE6', document.frmUpdateItem.FEATURE6, 'div_feature6')" /></div>
												<div id="div_feature6_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">Phonesale Feature 7:</td>
                                        	<td align="left" valign="top"><textarea name="FEATURE7" cols="70" rows="1" onkeyup="enableUpdateBtn('div_feature7');"><%=FEATURE7%></textarea></td>
                                            <td align="left" valign="top">
                                            	<div id="div_feature7" style="display:none;"><input type="button" name="btnFeature7" value="Update" onclick="ajaxUpdateDesc('FEATURE7', document.frmUpdateItem.FEATURE7, 'div_feature7')" /></div>
												<div id="div_feature7_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">Phonesale Feature 8:</td>
                                        	<td align="left" valign="top"><textarea name="FEATURE8" cols="70" rows="1" onkeyup="enableUpdateBtn('div_feature8');"><%=FEATURE8%></textarea></td>
                                            <td align="left" valign="top">
                                            	<div id="div_feature8" style="display:none;"><input type="button" name="btnFeature8" value="Update" onclick="ajaxUpdateDesc('FEATURE8', document.frmUpdateItem.FEATURE8, 'div_feature8')" /></div>
												<div id="div_feature8_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">Phonesale Feature 9:</td>
                                        	<td align="left" valign="top"><textarea name="FEATURE9" cols="70" rows="1" onkeyup="enableUpdateBtn('div_feature9');"><%=FEATURE9%></textarea></td>
                                            <td align="left" valign="top">
                                            	<div id="div_feature9" style="display:none;"><input type="button" name="btnFeature9" value="Update" onclick="ajaxUpdateDesc('FEATURE9', document.frmUpdateItem.FEATURE9, 'div_feature9')" /></div>
												<div id="div_feature9_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top">Phonesale Feature 10:</td>
                                        	<td align="left" valign="top"><textarea name="FEATURE10" cols="70" rows="1" onkeyup="enableUpdateBtn('div_feature10');"><%=FEATURE10%></textarea></td>
                                            <td align="left" valign="top">
                                            	<div id="div_feature10" style="display:none;"><input type="button" name="btnFeature10" value="Update" onclick="ajaxUpdateDesc('FEATURE10', document.frmUpdateItem.FEATURE10, 'div_feature10')" /></div>
												<div id="div_feature10_statusbox" style="display:none;"></div>
											</td>
                                        </tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="100%" class="normaltext" align="center">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="20%" class="normaltext" align="center">
												Form Factor<br>
												<select name="FormFactor">
													<option value=""<%if isNull(FormFactor) then response.write " selected"%>></option>
													<option value="Bar"<%if FormFactor = "Bar" then response.write " selected"%>>Bar</option>
													<option value="Flip"<%if FormFactor = "Flip" then response.write " selected"%>>Flip</option>
													<option value="Slide"<%if FormFactor = "Slide" then response.write " selected"%>>Slide</option>
													<option value="Swivel"<%if FormFactor = "Swivel" then response.write " selected"%>>Swivel</option>
													<option value="Watch"<%if FormFactor = "Watch" then response.write " selected"%>>Watch</option>
												</select>
											</td>
											<td width="20%" class="normaltext" align="center">
												Band Type<br>
												<select name="BandType">
													<option value=""<%if isNull(BandType) then response.write " selected"%>></option>
													<option value="2"<%if BandType = 2 then response.write " selected"%>>Dual</option>
													<option value="3"<%if BandType = 3 then response.write " selected"%>>Tri</option>
													<option value="4"<%if BandType = 4 then response.write " selected"%>>Quad</option>
												</select>
											</td>
											<td width="45%" class="normaltext" align="center">
												Band Text<br>
												<input type="text" name="Band" size="50" maxlength="250" value="<%=Band%>">
											</td>
											<td width="15%" class="normaltext" align="center">
												Smartphone<br>
												<input type="checkbox" name="Smartphone" value="1"<%if Smartphone = true then response.write " checked"%>>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="100%" class="normaltext" align="center">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="15%" class="normaltext" align="center">
												Talk Time<br>
												<input type="text" name="TalkTime" size="15" maxlength="15" value="<%=TalkTime%>">
											</td>
											<td width="15%" class="normaltext" align="center">
												Standby<br>
												<input type="text" name="Standby" size="15" maxlength="15" value="<%=Standby%>">
											</td>
											<td width="30%" class="normaltext" align="center">
												Display<br>
												<input type="text" name="Display" size="30" maxlength="50" value="<%=Display%>">
											</td>
											<td width="40%" class="normaltext" align="center">
												Package Contents<br>
												<input type="text" name="PackageContents" size="50" maxlength="250" value="<%=PackageContents%>">
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="100%" class="normaltext" align="center">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="15%" class="normaltext" align="center" valign="top">
												TV
												<input type="checkbox" name="Features" value="1"<%if inStr(Features,",1,") > 0 then response.write " checked"%>>
											</td>
											<td width="15%" class="normaltext" align="center" valign="top">
												MP3 Player
												<input type="checkbox" name="Features" value="2"<%if inStr(Features,",2,") > 0 then response.write " checked"%>>
											</td>
											<td width="17%" class="normaltext" align="center" valign="top">
												FM Radio
												<input type="checkbox" name="Features" value="3"<%if inStr(Features,",3,") > 0 then response.write " checked"%>>
											</td>
											<td width="16%" class="normaltext" align="center" valign="top">
												Camera
												<input type="checkbox" name="Features" value="4"<%if inStr(Features,",4,") > 0 then response.write " checked"%>>
											</td>
											<td width="20%" class="normaltext" align="center" valign="top">
												Video Camera
												<input type="checkbox" name="Features" value="5"<%if inStr(Features,",5,") > 0 then response.write " checked"%>>
											</td>
											<td width="17%" class="normaltext" align="center" valign="top">
												Push-to-Talk
												<input type="checkbox" name="Features" value="6"<%if inStr(Features,",6,") > 0 then response.write " checked"%>>
											</td>
										</tr>
										<tr>
											<td width="15%" class="normaltext" align="center" valign="top">
												Wi-Fi
												<input type="checkbox" name="Features" value="7"<%if inStr(Features,",7,") > 0 then response.write " checked"%>>
											</td>
											<td width="15%" class="normaltext" align="center" valign="top">
												GPS
												<input type="checkbox" name="Features" value="8"<%if inStr(Features,",8,") > 0 then response.write " checked"%>>
											</td>
											<td width="17%" class="normaltext" align="center" valign="top">
												Web Browser
												<input type="checkbox" name="Features" value="9"<%if inStr(Features,",9,") > 0 then response.write " checked"%>>
											</td>
											<td width="16%" class="normaltext" align="center" valign="top">
												Bluetooth
												<input type="checkbox" name="Features" value="10"<%if inStr(Features,",10,") > 0 then response.write " checked"%>>
											</td>
											<td width="20%" class="normaltext" align="center" valign="top">
												QWERTY Keyboard
												<input type="checkbox" name="Features" value="11"<%if inStr(Features,",11,") > 0 then response.write " checked"%>>
											</td>
											<td width="17%" class="normaltext" align="center" valign="top">
												Touch Screen
												<input type="checkbox" name="Features" value="12"<%if inStr(Features,",12,") > 0 then response.write " checked"%>>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
    <tr>
		<td width="100%" colspan="2" class="normaltext" align="center" id="erTextFields" style="display:none;">
			<table border="1" bordercolor="#00CC00" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="100%" align="center">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="100%" class="biggerText">
									<span style="color:#00CC00">TabletMall Fields:</span>
								</td>
							</tr>
                            <tr>
								<td width="100%" class="normaltext" align="left" style="padding-left:20px;">
                                	<table border="0" cellpadding="0" cellspacing="0" width="100%" class="normaltext">
                                    	<tr>
                                        	<td align="left" valign="top" colspan="2">
												<span class="biggerText">*&nbsp;</span>TabletMall Item Desc:<br>
			                                    <textarea name="itemDesc_ER" cols="80" rows="1" onkeyup="enableUpdateBtn('div_itemdesc_er');"><%=itemDesc_ER%></textarea>
                                            </td>
                                            <td align="left" valign="bottom">
	                                            <div id="div_itemdesc_er" style="display:none;"><input type="button" name="btnItemDesc_ER" value="Update" onclick="ajaxUpdateDesc('itemDesc_ER', document.frmUpdateItem.itemDesc_ER, 'div_itemdesc_er')" /></div>
												<div id="div_itemdesc_er_statusbox" style="display:none;"></div>
                                            </td>
                                        </tr>
                                    	<tr>
                                        	<td align="left" valign="top" colspan="2">
												<span class="biggerText">*&nbsp;</span>TabletMall Item Long Detail:<br>
												<textarea name="itemLongDetail_ER" cols="80" rows="8" onkeyup="enableUpdateBtn('div_itemlongdetail_er');"><%=itemLongDetail_ER%></textarea>
                                            </td>
                                            <td align="left" valign="middle">
	                                            <div id="div_itemlongdetail_er" style="display:none;"><input type="button" name="btnItemLongDetail_ER" value="Update" onclick="ajaxUpdateDesc('itemLongDetail_ER', document.frmUpdateItem.itemLongDetail_ER, 'div_itemlongdetail_er')" /></div>
												<div id="div_itemlongdetail_er_statusbox" style="display:none;"></div>
                                            </td>
                                        </tr>
									</table>                                
									 &nbsp; 
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="2" align="center">
			<input type="button" name="addEdit" onclick="doSubmit('<%=updateItems%>');" value="<%=updateItems%>">
			<%if updateItems = "Edit" then%>
				&nbsp;&nbsp;&nbsp;
				<input type="button" name="copyItem" onclick="doSubmit('Copy');" value="Copy to New Item">
			<%end if%>
            <input type="hidden" name="frmSubmit" value="" />
		</td>
	</tr>
</table>
</form>
<%
function isChecked(val)
	if val = true then
		isChecked = " checked"
	else
		isChecked = ""
	end if
end function

' it returns FP2-SAM-I500-AM from FP2-SAM-I500-AM11
function getPartNumberHeader(partNum)
	'part number suggestion box start
	dim partNumberHead, partNumberTail, nLengthTail
'	partNum = ""
	'test ID : "",7657,2,39888,55842,39540,59059
	partNumberHead = left(partNum, instrrev(partNum,"-"))
	partNumberTail = mid(partNum, instrrev(partNum,"-")+1, len(partNum) - instrrev(partNum,"-"))
	nLengthTail = len(partNumberTail)

	if nLengthTail > 0 then
		dim startPos : startPos = 1
		for i=0 to nLengthTail
			startPos = nLengthTail-i
			
			if 0 = startPos then exit for end if 
			
			if not isnumeric(mid(partNumberTail, startPos, 1)) then exit for end if 
		next
	end if 
	
	if startPos > 0 then partNumberHead = partNumberHead & left(partNumberTail, startPos) end if
	
	getPartNumberHeader = partNumberHead
end function
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function showFields(showTab) {
		var textFieldArray = new Array("weTextFields","coTextFields","caTextFields","psTextFields","erTextFields");
		for (var i=0; i < textFieldArray.length; i++) {
			document.getElementById(textFieldArray[i]).style.display = 'none'
			document.getElementById(textFieldArray[i] + "_tab").style.backgroundColor = '#000'
			document.getElementById(textFieldArray[i] + "_tab").style.color = '#fff'
			document.getElementById(textFieldArray[i] + "_tab").style.borderRight = '1px solid #fff'
		}
				
		document.getElementById(showTab + "_tab").style.backgroundColor = '#ccc'
		document.getElementById(showTab + "_tab").style.color = '#000'
		document.getElementById(showTab + "_tab").style.borderRight = '1px solid #ccc'
		document.getElementById(showTab).style.display = ''
	}
	
	function copyPn(sourcePn) {
		document.getElementById("dumpZone").innerHTML = "";
		ajax('/ajax/admin/ajaxCopyPn.asp?source=' + sourcePn + '&target=<%=partnumber%>','dumpZone');
		setTimeout("copyPnCheckDump()",500)
	}
	
	var checkDumpCnt = 0;
	function copyPnCheckDump() {
		checkDumpCnt++;
		dumpVal = document.getElementById("dumpZone").innerHTML
		if (checkDumpCnt < 10 && dumpVal == "") {
			setTimeout("copyPnCheckDump()",500)
		}
		else if (dumpVal != "") {
			if (dumpVal == "Error") {
				alert("Error processing the request\nPlease make sure you are using a valid partnumber")
			}
			else {
				window.location = window.location;
			}
		}
	}
	
	function deleteItem(itemID) {
		var yourstate=window.confirm("Are you sure you want to delete this product?");
		if (yourstate) {
			document.getElementById("dumpZone").innerHTML = "";
			ajax('/ajax/admin/ajaxKillProducts.asp?deleteProd=' + itemID,'dumpZone');
			setTimeout("testDelete()",500)
		}
	}
	
	function testDelete() {
		if (document.getElementById("dumpZone").innerHTML != "") {
			if (document.getElementById("dumpZone").innerHTML == "Product Deleted") {
				window.location.href = "/admin/inventoryAdjust.asp";
			}
			else {
				alert("Error while deleting the product");
			}
		}
		else {
			setTimeout("testDelete()",500)
		}
	}
</script>