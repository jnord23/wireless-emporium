<%
header = 1
pageTitle = "WE Admin Login"
securePage = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
function checkNull(str, nullif)
	retVal = str
	if isnull(str) then
		retVal = nullif
	end if
	checkNull = retVal
end function

dim userID, strPWD
userID = prepStr(lcase(request.form("userID")))
strPWD = prepStr(request.form("pword"))
if strPWD <> "" then
	response.Write("trying to login<br>")
	
	strSQL = "SELECT * FROM WE_AdminUsers WHERE username='" & userID & "'"
	SET oRS = oConn.EXECUTE(strSQL)
	
	'if there is a user by that username from the form
	if not oRS.EOF then
		adminID = oRS("adminID")
		username = oRS("username")
		activePassword = oRS("password")
		lastPassword = oRS("lastPassword")
		passwordDate = oRS("passwordDate")
		fname = oRS("fname")
		lname = oRS("lname")
		adminLvl = oRS("adminLvl")
		adminNav = oRS("adminNav")
		'then check if the password matches for that user
		if oRS("password") = strPWD then
			'if they do match then set some cookies to allow access
			Response.Cookies("adminID") = adminID
			Response.Cookies("adminNav") = adminNav
			Response.Cookies("username") = username
			Response.Cookies("fname") = fname
			Response.Cookies("lname") = lname
			Response.Cookies("adminUser") = "True"
			session("adminID") = adminID
			session("adminLvl") = adminLvl
			session("adminNav") = adminNav
			session("adminFullName") = fname & " " & lname
			'and send them on their way
			oRS.Close
			set oRS = nothing
			
			sql = "insert into we_adminLogin (adminID) values(" & adminID & ")"
			session("errorSQL") = sql
			oConn.execute(sql)
			
			if activePassword = lastPassword or passwordDate < dateadd("m",-3,date) then
				session("validPassword") = 0
			else
				session("validPassword") = 1
			end if
			
			if session("validPassword") = 1 then
				response.redirect "/admin/menu.asp"
			else
				response.redirect "/admin/updatePassword.asp"
			end if
		else
			'else if the username and password do not match
			response.redirect "/admin/default.asp?show=error"
		end if
	else
		'if there is no user by that name
		'redirect to login page and set variable so we can tell user that there is no user by that name
		response.redirect "/admin/default.asp?show=error"
	end if
else
	dim myAction
	myAction = "/admin/default.asp"
	%>
	<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
		<tr bgcolor="#CCCCCC">
			<td align="center" colspan="2" valign="middle">&nbsp;</td>
		</tr>
		<tr>
			<td width="47%" valign="middle" bgcolor="eaeaea">
				<table width="85%" border="0" cellspacing="0" cellpadding="15" align="center">
					<tr>
						<td bgcolor="#FFFFFF">
							<form name="login" method="post" action="/admin/">
								<font face="Verdana, Arial, Helvetica, sans-serif" size="1">
								<br>
								<%
								if request.querystring("show") = "error" then
									%>
									<p class="text"><font color="#CC0000">Sorry, there's a problem with your login. Please try again.</font></p>
									<%
								else
									%>
									<p class="text"><font color="#CC0000">Please login to make modifications.</font></p>
									<br>
									<%
								end if
								%>
								</font>
								<table width="250" border="0" cellspacing="0" cellpadding="1">
									<tr>
										<td bgcolor="#000066">
											<table width="100%" border="0" cellpadding="6" bgcolor="#eaeaea" cellspacing="0">
												<tr bgcolor="#FF6600">
													<td width="28%">
														<p><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>Username:</b></font></p>
													</td>
													<td width="72%">
														<input type="text" name="userid" autocomplete="off">
													</td>
												</tr>
												<tr bgcolor="#FF6600">
													<td width="28%">
														<p><b><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Password:</font></b></p>
													</td>
													<td width="72%">
														<input name="pword" type="password" id="pword" autocomplete="off">
													</td>
												</tr>
												<tr bgcolor="#FF6600">
													<td colspan="2">
														<input type="submit" name="Submit" value="Log In"> 
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</form>
						</td>
					</tr>
				</table>
			</td>
			<td valign="top" bgcolor="eaeaea" width="53%">
				<table width="95%" border="0" cellspacing="0" cellpadding="15" align="center">
					<tr>
						<td bgcolor="#FFFFFF">
							<p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Please 
							log in to perform administrative functions.</b> </font></p>
							<p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Forget 
							your login information? Please contact
							<a href="mailto:webmaster@wirelessemporium.com">webmaster@wirelessemporium.com</a>.</font></p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr bgcolor="#CCCCCC">
			<td colspan="2" valign="middle">&nbsp;</td>
		</tr>
	</table>
	<%
end if
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
