<%
pageTitle = "Admin - Upload Tracking Numbers"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<font face="Arial,Helvetica">
<blockquote>
<blockquote>
<br><br><br>

<%
server.scripttimeout = 1000 'seconds
dim Path
Path = server.mappath("tempCSV")

' Create an instance of AspUpload object
set Upload = Server.CreateObject("Persits.Upload")
Upload.IgnoreNoPost = True
Count = Upload.Save(Path)
if Upload.form("upload") <> "" then
	' Capture uploaded file. Return the number of files uploaded
	if Count = 0 then
		response.write "No file selected."
		response.end
	else
		' Obtain File object representing uploaded file
		set File = Upload.Files(1)
		myFile = File.path
		
		uploadDate = upload.form("uploadDate")
		if not isDate(uploadDate) then
			response.write "<h3>The date you entered is not a valid date. Please try again.</h3>"
			response.write "<p><a href=""javascipt:history.back();"">BACK</a></p>"
			response.end
		end if
		
		' Delete all existing XML files in /www/admin/tempCSV
		set fs = CreateObject("Scripting.FileSystemObject")
		set demofolder = fs.GetFolder(Path)
		set filecoll = demofolder.Files
		For Each fil in filecoll
			if UCase(right(fil.name,4)) = ".XML" then
				thisfile = Path & "\" & fil.name
				if myFile <> thisfile then
					fs.DeleteFile thisfile
				end if
			end if
		Next
		
		' Remove ampersands:
		if not fs.FileExists(myFile) then
			response.write("File " & myFile & " does not exist, write aborted.<br>")
			response.end
		else
			set readfile = fs.OpenTextFile(myFile, 1)
			strToWrite = ""
			do while readfile.AtEndOfStream <> true
				strToWrite = strToWrite & readfile.readline & vbCrLf
			loop
			readfile.Close()
			
			set writefile = fs.OpenTextFile(myFile, 2)
			strToWrite = replace(strToWrite,"&","")
			strToWrite = replace(strToWrite,"?","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"�","")
			strToWrite = replace(strToWrite,"  "," ")
			writefile.write strToWrite
			writefile.Close()
						
			' Update SQL DB with Tracking Numbers (PIC) matched to OrderIDs (ReferenceID):
			dim mynodes, xmlDoc, i
			set xmlDoc = Server.CreateObject("Msxml2.DOMDocument.3.0")
			xmlDoc.async = false
			xmlDoc.load(myFile)
			set mynodes = xmlDoc.documentElement.selectNodes("Record")
			dim a
			a = 0
			for each Node in mynodes
				ReferenceID = ""
				PIC = ""
				for i = 0 to Node.ChildNodes.Length - 1
					if Node.childnodes.item(i).nodename = "PIC" then PIC = Node.childnodes.item(i).text
					if Node.childnodes.item(i).nodename = "ReferenceID" then ReferenceID = Node.childnodes.item(i).text
				next
				if len(ReferenceID) = 6 and trim(PIC) <> "" then
					'shipped = 1 New insert, 2 cancled, 3 confimed sent
					a = a + 1
					SQL = "UPDATE WE_reshipment SET TrackingNum='" & trim(PIC) & "', shipped='3', confirmdatetime='" & uploadDate & "' WHERE OrderID='" & ReferenceID & "'"
					session("errorSQL") = SQL
					response.write "<p>" & SQL & "</p>"
					oConn.execute SQL
				end if
			next
			response.write "<h3>" & a & " records updated!</h3>"
		end if
	end if
else
	%>
	<h3>Select a File to Upload:</h3>
	<form enctype="multipart/form-data" action="uploadTrackingNums.asp" method="POST">
		<p><input type="FILE" name="FILE1"></p>
		<p><input type="text" name="uploadDate" value="<%=date()%>">&nbsp;Date&nbsp;Confirmation&nbsp;Sent</p>
		<p><input type="SUBMIT" name="upload" value="Upload"></p>
	</form>
	<%
end if
%>

</blockquote>
</blockquote>
</font>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
