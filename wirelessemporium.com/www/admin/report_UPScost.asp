<%
pageTitle = "WE Admin Site - UPS Shipping Fee Report"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->

<table border="0" cellpadding="0" cellspacing="0" align="center" width="860"><tr><td width="100%">

<%
if request.querystring("submitted") <> "" then dateStart = request.querystring("dateStart")
if not isDate(dateStart) then dateStart = dateAdd("d",-1,date())
dateStart = dateValue(dateStart)

showblank = "&nbsp;"
shownull = "-null-"

if strError = "" then
	SQL = "SELECT orderID, AccountId, ordershippingfee, UPScost, shiptype, orderdatetime FROM we_orders"
	SQL = SQL & " WHERE orderdatetime >= '" & dateStart & "' AND orderdatetime < '" & dateAdd("D",1,dateStart) & "'"
	SQL = SQL & " AND approved = 1"
	SQL = SQL & " AND (shiptype = 'UPS 2nd Day Air' OR shiptype = 'UPS Ground' OR shiptype = 'UPS 3 Day Select')"
	SQL = SQL & " AND (cancelled = 0 OR cancelled IS NULL)"
	SQL = SQL & " AND store = 0"
	SQL = SQL & " ORDER BY orderdatetime"
	set RS = Server.CreateObject("ADODB.Recordset")
	'response.write "<h3>" & SQL & "</h3>" & vbCrLf
	'response.end
	RS.open SQL, oConn, 3, 3
	
	if RS.eof then
		response.write "No records matched<br><br>So cannot make table..."
	else
		%>
		<table border="1">
			<tr>
				<%
				for each whatever in RS.fields
					%>
					<td><b><%=whatever.name%></b></td>
					<%
				next
				%>
			</tr>
			<%
			do until RS.eof
				%>
				<tr>
					<%
					for each whatever in RS.fields
						thisfield = whatever.value
						if isNull(thisfield) then
							thisfield = shownull
						else
							if whatever.name = "UPScost" or whatever.name = "ordershippingfee" then
								thisfield = formatCurrency(thisfield)
							end if
						end if
						if trim(thisfield) = "" then thisfield = showblank
						if whatever.name = "orderID" then
							%>
							<td valign="top"><a href="javascript:printinvoice('<%=RS("OrderId")%>','<%=RS("AccountId")%>');"><%=RS("OrderId")%></a></td>
							<%
						elseif whatever.name = "UPScost" then
							if ordershippingfee < UPScost then
								fontColor = "#FF0000"
							else
								fontColor = "#000000"
							end if
							%>
							<td valign="top"><font color="<%=fontColor%>"><b><%=thisfield%></b></font></td>
							<%
						else
							%>
							<td valign="top"><%=thisfield%></td>
							<%
						end if
					next
					%>
				</tr>
				<%
				RS.movenext
			loop
			%>
		</table>
		<%
	end if
	
	RS.close
	Set RS = nothing
else
	%>
	<center>
	<font size="+2" color="red"><%=strError%></font>
	<a href="javascript:history.back();">BACK</a>
	</center>
	<%
end if
%>
<p>&nbsp;</p>
<h3>Choose another date:</h3>
<br>
<form action="report_UPScost.asp" method="get">
	<p><input type="text" name="dateStart">&nbsp;&nbsp;Report&nbsp;Date</p>
	<p><input type="submit" name="submitted" value="Generate Report"></p>
</form>

</td></tr></table>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->

<script language="javascript">
function printinvoice(orderid,accountid) {
	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=900,height=550,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}
</script>
