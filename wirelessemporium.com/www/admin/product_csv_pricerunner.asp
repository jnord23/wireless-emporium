<%
pageTitle = "Product List upload for Pricerunner"
header = 1
response.buffer = false
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
dim fs, file, filename, path
'Map the file name to the physical path on the server.
filename = "productList_Pricerunner.csv"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Product List upload for Pricerunner</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							response.write "<b>Create File:</b><br>"
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href='/tempCSV/" & filename & "'>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
	
<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		response.write "<p>Deleting " & filename & ".<br>"
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	response.write "Creating " & filename & ".<br>"
	set file = fs.CreateTextFile(path)
	dim SQL, RS
	SQL = "SELECT A.itemID, 'WE-' + Cast(A.Itemid + 5000 AS varchar(10)) AS sku, A.PartNumber AS MPN,"
	SQL = SQL & " A.itemDesc, A.itemPic, A.price_our, A.itemLongDetail, A.inv_qty,"
	SQL = SQL & " B.brandName, C.typename"
	SQL = SQL & " FROM (we_items A INNER JOIN we_brands B ON A.brandid=B.brandid)"
	SQL = SQL & " INNER JOIN we_types C ON A.typeid=C.typeid"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " ORDER BY A.itemID"
	dim strline
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "SKU,Price,Product URL,Product Name,Category,Product state,UPC Codes,Manufacturer Name,Manufacturer SKU,Description,Graphic URL,Stock info,Added Value Message,Delivery time,Shipping cost"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("MPN") & "'"
				set RS2 = CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				strline = chr(34) & RS("sku") & chr(34) & ","
				strline = strline & chr(34) & RS("price_our") & chr(34) & ","
				strline = strline & chr(34) & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?id=pricerunner" & chr(34) & ","
				strline = strline & chr(34) & RS("itemDesc") & chr(34) & ","
				strline = strline & chr(34) & RS("typename") & chr(34) & ","
				strline = strline & chr(34) & "NEW" & chr(34) & ","
				strline = strline & chr(34) & chr(34) & ","
				strline = strline & chr(34) & RS("brandName") & chr(34) & ","
				strline = strline & chr(34) & RS("sku") & chr(34) & ","
				strline = strline & chr(34) & replace(replace(RS("itemLongDetail"),vbCrLf," "),chr(34),"") & chr(34) & ","
				strline = strline & chr(34) & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & chr(34) & ","
				strline = strline & chr(34) & "yes" & chr(34) & ","
				strline = strline & chr(34) & "Get Fast FREE First Class Shipping on all orders" & chr(34) & ","
				strline = strline & chr(34) & "2-5 days" & chr(34) & ","
				strline = strline & chr(34) & "FREE" & chr(34)
				file.WriteLine strline
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
