<%
	thisUser = Request.Cookies("username")
	pageTitle = "Top Models"
	header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	set fso = CreateObject("Scripting.FileSystemObject")
%>
<form name="sortForm" style="padding:0px; margin:0px;">
<div style="width:400px; height:20px; margin-bottom:10px; margin-left:auto; margin-right:auto;">
	<div style="float:left;">Number of Models:</div>
    <div style="float:left; padding-left:5px;"><input type="text" name="numOfPhones" value="50" size="3" onchange="updateList()" /></div>
</div>
<div style="width:400px; height:20px; margin-bottom:10px; margin-left:auto; margin-right:auto;">
	<div style="float:left;">Website:</div>
    <div style="float:left; padding-left:5px;">
    	<select name="website" onchange="websiteChng(this.value)">
        	<option value="">All Sites</option>
            <option value="we">WirelessEmporium</option>
            <option value="co">CellularOutfitter</option>
            <option value="ca">CellphoneAccents</option>
            <option value="ps">PhoneSale</option>
        </select>
    </div>
</div>
<div style="width:400px; height:20px; margin-bottom:10px; margin-left:auto; margin-right:auto;">
	<div style="float:left;">List Models By:</div>
    <div style="float:left; padding-left:5px;">
    	<select name="listBy" onchange="listByChng(this.value)">
        	<option value="allTime">All Time Sales</option>
            <option value="dateRange">Sales Date Range</option>
            <option value="new2Old">Newest to Oldest</option>
        </select>
    </div>
</div>
<div id="dateRangeBox" style="width:400px; height:20px; margin-bottom:10px; margin-left:auto; margin-right:auto; display:none;">
	<div style="float:left; padding-top:3px;">Start Date:</div>
    <div style="float:left; padding-left:5px;"><input type="text" name="sDate" value="<%=dateadd("m",-1,date)%>" size="10" /></div>
    <div style="float:left; padding:3px 0px 0px 5px;">End Date:</div>
    <div style="float:left; padding-left:5px;"><input type="text" name="eDate" value="<%=date%>" size="10" /></div>
    <div style="float:left; padding-left:5px;"><input type="button" name="dateBttn" value="Pull Models" onclick="getPhoneByRange()" /></div>
</div>
</form>
<div style="width:400px; height:20px; margin-bottom:10px; text-align:center; margin-left:auto; margin-right:auto;"><a href="/admin/tempCSV/topModels.csv">Download CSV</a></div>
<div style="width:450px; height:20px; margin-bottom:10px; background-color:#000; font-weight:bold; color:#FFF; margin-left:auto; margin-right:auto;">
	<div style="float:left; width:10%; text-align:center;">#</div>
    <div style="float:left; width:30%; text-align:center; margin-left:10px;">Image</div>
    <div style="float:left; width:50%; text-align:left; margin-left:10px;">Name</div>
</div>
<div id="phoneList" style="width:450px; margin-left:auto; margin-right:auto;"></div>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	var website = ""
	ajax('/ajax/admin/ajaxTopModels.asp?website=' + website + '&listBy=allTime&numOfPhones=50','phoneList')
	
	function listByChng(listBy) {
		if (listBy == "dateRange") {
			document.getElementById("dateRangeBox").style.display = ""
		}
		else {
			document.getElementById("phoneList").innerHTML = "Processing Request...<br />Please Wait"
			document.getElementById("dateRangeBox").style.display = "none"
			var numOfPhones = document.sortForm.numOfPhones.value
			ajax('/ajax/admin/ajaxTopModels.asp?website=' + website + '&listBy=' + listBy + '&numOfPhones=' + numOfPhones,'phoneList')
		}
	}
	
	function websiteChng(selectedSite) {
		website = selectedSite
		ajax('/ajax/admin/ajaxTopModels.asp?website=' + website + '&listBy=allTime&numOfPhones=50','phoneList')
	}
	
	function getPhoneByRange() {
		document.getElementById("phoneList").innerHTML = "Processing Request...<br />Please Wait"
		var sDate = document.sortForm.sDate.value
		var eDate = document.sortForm.eDate.value
		var numOfPhones = document.sortForm.numOfPhones.value
		ajax('/ajax/admin/ajaxTopModels.asp?website=' + website + '&listBy=dateRange&sDate=' + sDate + '&eDate=' + eDate + '&numOfPhones=' + numOfPhones,'phoneList')
	}
	
	function updateList() {
		document.getElementById("phoneList").innerHTML = "Processing Request...<br />Please Wait"
		var listBy = document.sortForm.listBy.value
		var sDate = document.sortForm.sDate.value
		var eDate = document.sortForm.eDate.value
		var numOfPhones = document.sortForm.numOfPhones.value
		
		if (listBy == "dateRange") {
			ajax('/ajax/admin/ajaxTopModels.asp?website=' + website + '&listBy=dateRange&sDate=' + sDate + '&eDate=' + eDate + '&numOfPhones=' + numOfPhones,'phoneList')
		}
		else {
			ajax('/ajax/admin/ajaxTopModels.asp?website=' + website + '&listBy=' + listBy + '&numOfPhones=' + numOfPhones,'phoneList')
		}
	}
	
	function expandBreakDown(bdBox,modelID) {
		var listBy = document.sortForm.listBy.value
		var sDate = document.sortForm.sDate.value
		var eDate = document.sortForm.eDate.value
		if (document.getElementById(bdBox).innerHTML == "") {
			document.getElementById(bdBox).innerHTML = "Processing Request...<br />Please Wait"
			ajax('/ajax/admin/ajaxTopModels.asp?website=' + website + '&listBy=' + listBy + '&sDate=' + sDate + '&eDate=' + eDate + '&breakDownBy=' + modelID,bdBox)
		}
		else {
			document.getElementById(bdBox).innerHTML = ""
		}
	}
	
	function catBreakDown(bdBox,modelID,catID,subID) {
		var listBy = document.sortForm.listBy.value
		var sDate = document.sortForm.sDate.value
		var eDate = document.sortForm.eDate.value
		if (document.getElementById(bdBox).innerHTML == "") {
			document.getElementById(bdBox).innerHTML = "Processing Request...<br />Please Wait"
			ajax('/ajax/admin/ajaxTopModels.asp?website=' + website + '&listBy=' + listBy + '&sDate=' + sDate + '&eDate=' + eDate + '&catID=' + catID + '&modelID=' + modelID + '&subID=' + subID,bdBox)
		}
		else {
			document.getElementById(bdBox).innerHTML = ""
		}
	}
</script>