<%
pageTitle = "reship"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<script>
function doSubmit(reportType)
{
	document.frmSearch.reportType.value = reportType;
	document.frmSearch.submit();
}
</script>
<style>
.mouseover	{ background-color:#ACCEFF;}
</style>
<%
Dim cfgArrGroupBy : cfgArrGroupBy = Array("DATE", "STORE", "ReshipReason", "Category")
dim arrGroupBy, sql
dim strSDate, strEDate, strStore, strReshipReason, strCategory, strReportType

strSDate		=	trim(request("txtSDate"))
strEDate		=	trim(request("txtEDate"))
strStore		=	trim(request("cbStore"))
strReshipReason	=	trim(request("cbReshipReason"))
strGroupBy		=	trim(request("cbGroupBy"))
strCategory		=	trim(request("cbCategory"))
strReportType	=	trim(request("reportType"))

if "EXCEL" = strReportType then
	response.redirect "/admin/report_reship_excel.asp?" & request.QueryString
end if

dim arrDate
sql	=	"select	convert(varchar(10), max(entrydate), 20), convert(varchar(10), dbo.[fn_getFirstDayOfMonth](max(entrydate)), 20)" & vbcrlf & _
		"from	we_orderdetails" & vbcrlf & _
		"where	reship = 1" & vbcrlf
arrDate	= getDbRows(sql)

if "" = strSDate then strSDate = arrDate(1,0) end if
if "" = strEDate then strEDate = arrDate(0,0) end if
If "" = strGroupBy then strGroupBy = "STORE,ReshipReason,," end if
arrGroupBy = split(strGroupBy, ",")

'for i=0 to ubound(arrGroupBy)
'	response.write "[" & trim(arrGroupBy(i)) & "]<br>"
'next
'response.write isdate(strSDate) & "<br>"
'response.write isdate(strEDate) & "<br>"

dim strSqlSelectMain, strSqlWhere, strSqlGroupBy, strSqlOrderBy, strHeadings
strSqlSelectMain	=	""
strSqlWhere			=	""
strSqlGroupBy		=	""
strSqlOrderBy		=	""
strHeadings			=	""

if "" <> strStore 		then	strSqlWhere	=	strSqlWhere & " and x.site_id = '" & strStore & "'" & vbcrlf end if
if "" <> strCategory 	then 	strSqlWhere	=	strSqlWhere & " and t.typeid = '" & strCategory & "'" & vbcrlf end if

Dim bDate, bStore, bReshipReason, bCategory
bDate			=	false
bStore			=	false
bReshipReason	=	false
bCategory		=	false

For i = 0 To Cint(UBound(arrGroupBy))
	If "" <> trim(arrGroupBy(i)) Then
		Select Case ucase(trim(arrGroupBy(i)))
			Case "DATE"
				If bDate Then
					Response.Write "<Script>alert('DATE in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bDate = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.eDate"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.eDate, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.eDate, "
				strHeadings			=	strHeadings			&	"<td class=""normalText"" align=""left"" style=""padding-left:10px;""><b>RESHIP DATE</b></td>"

			Case "STORE"
				If bStore Then
					Response.Write "<Script>alert('STORE in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bStore = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.shortDesc, a.favicon, a.color"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.shortDesc, a.favicon, a.color, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.shortDesc, "				
				strHeadings			=	strHeadings			&	"<td class=""normalText"" align=""left"" style=""padding-left:10px;""><b>STORE</b></td>"

			Case "RESHIPREASON"
				If bReshipReason Then
					Response.Write "<Script>alert('ReshipReason in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bReshipReason = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.reshipReason"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.reshipReason, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.reshipReason, "
				strHeadings			=	strHeadings			&	"<td class=""normalText"" align=""left"" style=""padding-left:10px;""><b>RESHIP REASON</b></td>"

			Case "CATEGORY"
				If bCategory Then
					Response.Write "<Script>alert('Category in [GROUP BY] is duplicated');history.back();</Script>"
					Response.End
				End If
				bCategory = true
				
				strSqlSelectMain	=	strSqlSelectMain 	& 	", a.typename"
				strSqlGroupBy		=	strSqlGroupBy		&	"a.typename, "
				strSqlOrderBy		=	strSqlOrderBy		&	"a.typename, "				
				strHeadings			=	strHeadings			&	"<td class=""normalText"" align=""left"" style=""padding-left:10px;""><b>CATEGORY</b></td>"
		End Select
	End If
Next

dim arrStore, arrCategory, arrResult, nSkipColumn, nRow, nTotalQty
nTotalQty = 0

sql	=	"select site_id, shortDesc" & vbcrlf & _
		"from	xstore" & vbcrlf & _
		"order by 1" & vbcrlf
arrStore 	= getDbRows(sql)

sql	=	"select	typeid, typeName" & vbcrlf & _
		"from	we_types" & vbcrlf & _
		"where	typeid not in (9)" & vbcrlf & _
		"order by 1" & vbcrlf
arrCategory = getDbRows(sql)

if "" <> strSqlGroupBy then
	strSqlGroupBy	=	"group by " & left(trim(strSqlGroupBy), len(trim(strSqlGroupBy))-1)
	strSqlOrderBy	=	"order by " & left(trim(strSqlOrderBy), len(trim(strSqlOrderBy))-1)
end if

sql	=	"select	isnull(sum(a.quantity), 0) qty " & strSqlSelectMain & vbcrlf & _
		"from	(" & vbcrlf & _
		"		select	a.orderid, x.site_id, x.shortDesc, nullif(x.logo, '') favicon, x.color" & vbcrlf & _
		"			,	b.orderdetailid, b.quantity, b.reshipReason, b.modifier, b.note, b.entrydate" & vbcrlf & _
		"			, 	convert(varchar(10), b.entrydate, 20) eDate, t.typename " & vbcrlf & _
		"		from	we_orders a join we_orderdetails b" & vbcrlf & _
		"			on	a.orderid = b.orderid join xstore x" & vbcrlf & _
		"			on	a.store = x.site_id  join we_items i" & vbcrlf & _
		"			on	b.itemid = i.itemid join we_types t" & vbcrlf & _
		"			on	i.typeid = t.typeid" & vbcrlf & _	
		"		where	b.reship = 1" & vbcrlf & _
		"			and	b.entrydate >= '" & strSDate & "'" & vbcrlf & _
		"			and	b.entrydate < dateadd(dd, 1, '" & strEDate & "')" & vbcrlf & _
				strSqlWhere & vbcrlf & _
		"		) a" & vbcrlf & _
		strSqlGroupBy & vbcrlf & _
		strSqlOrderBy & vbcrlf
		
'response.write "<pre>" & sql & "</pre>"
arrResult = getDbRows(sql)

if not isnull(arrResult) then
	for i=0 to ubound(arrResult,2)
		nTotalQty = cdbl(nTotalQty) + cdbl(arrResult(0,i))
	next
end if

%>
<table width="1000" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td width="100%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top"><br>
						<form name="frmSearch" method="get">
						<input type="hidden" name="reportType" value="">
						<table width="100%" border="1" cellspacing="0" cellpadding="3" align="center" bordercolor="#FFFFFF" style="border-collapse:collapse;" bgcolor="#EAEAEA">
							<tr>
								<td class="normalText" align="center" style="color:#E95516;"><b>FILTER</b></td>
								<td class="normalText" align="center"><b>DATE</b></td>
								<td class="normalText" align="center"><b>STORE</b></td>
								<td class="normalText" align="center"><b>CATEGORY</b></td>
								<td class="normalText" align="center"><b>GROUP BY</b></td>
								<td class="normalText" align="center"><b>Action</b></td>
							</tr>
							<tr>
								<td class="normalText" align="center" style="color:#E95516;"><b>VALUE</b></td>
								<td class="normalText" align="center">
									<input type="text" name="txtSDate" value="<%=strSDate%>" size="7"> ~ <input type="text" name="txtEDate" value="<%=strEDate%>" size="7">
								</td>
								<td class="normalText" align="center">
									<select name="cbStore">
										<option value="">ALL</option>
									<%
									for i=0 to ubound(arrStore,2)
									%>
										<option value="<%=arrStore(0, i)%>" <%if cstr(strStore) = cstr(arrStore(0, i)) then%> selected <%end if%> ><%=arrStore(1,i)%></option>
									<%	
									next
									%>
									</select>
								</td>
								<td class="normalText" align="center">
									<select name="cbCategory">
										<option value="">ALL</option>
									<%
									for i=0 to ubound(arrCategory,2)
									%>
										<option value="<%=arrCategory(0,i)%>" <%if cstr(strCategory) = cstr(arrCategory(0,i)) then%> selected <%end if%> ><%=arrCategory(1,i)%></option>
									<%	
									next
									%>									
									</select>								
								</td>
								<td class="normalText" align="center">
								<%
								if not isnull(cfgArrGroupBy) then
									for i=0 to 2
										response.write "<select name=""cbGroupBy""><option value="""" "
										if "" = trim(arrGroupBy(i)) then response.write " selected " end if
										response.write ">--</option>"
										
										for j=0 to ubound(cfgArrGroupBy,1)
											response.write "<option value=""" & cfgArrGroupBy(j) & """ "
											if trim(arrGroupBy(i)) = trim(cfgArrGroupBy(j)) then response.write " selected " end if
											response.write ">" & trim(cfgArrGroupBy(j)) & "</option>"
										next
										response.write "</select>&nbsp;"
									next
								end if
								%>
								</td>
								<td class="normalText" align="center"><input type="submit" value="Search" onClick="doSubmit('WEB');">&nbsp;<input type="button" name="btnExcel" value="Export" onclick="doSubmit('EXCEL');"></td>
							</tr>							
						</table><br>
						<table width="80%" border="0" cellspacing="0" cellpadding="3" align="center">
							<tr><td colspan="99" width="1" height="1" bgcolor="#E95516" style="font-size:1px; padding:0px;">&nbsp;</td></tr>
							<tr>
								<%=strHeadings%>
								<td class="normalText" align="right" style="padding-right:10px;"><b>Quantity</b></td>								
							</tr>
							<tr><td colspan="99" width="1" height="1" bgcolor="#E95516" style="font-size:1px; padding:0px;">&nbsp;</td></tr>
							<%
							if not isnull(arrResult) then
							%>
							<tr><td colspan="99" class="smlText" align="right" bgcolor="#EAEAEA" style="padding-right:10px;"><b>Total : <%=formatnumber(nTotalQty, 0)%></b></td></tr>
							<%
								for nRow = 0 to ubound(arrResult,2)
							%>	
							<tr class="grid_row_<%=(nRow mod 2)%>" onMouseOver="this.className='mouseover'" onMouseOut="this.className='grid_row_<%=(nRow mod 2)%>'" >
							<!--<tr class="grid_row_<%=(nRow mod 2)%>">-->
								<%
								nSkipColumn = 1
								for i=0 to ubound(arrGroupBy)
									if "" <> trim(arrGroupBy(i)) then
										select case ucase(trim(arrGroupBy(i)))
											case "STORE"
									%>
									<td class="smlText" align="left" style="padding-left:10px;"><%=getStoreShortName(arrResult(nSkipColumn, nRow), arrResult(nSkipColumn+1, nRow), arrResult(nSkipColumn+2, nRow))%></td>
									<%
												nSkipColumn = nSkipColumn + 2									
											case else
									%>
									<td class="smlText" align="left" style="padding-left:10px;"><%=arrResult(nSkipColumn, nRow)%></td>
									<%
										end select
										nSkipColumn = nSkipColumn + 1										
									end if
								next
								%>
								<td class="smlText" align="right" style="padding-right:10px;"><%=formatnumber(arrResult(0, nRow),0)%></td>
							</tr>
							<%
								next
							else
							%>
							<tr><td colspan="99" class="smlText" align="center">No data to display</td></tr>							
							<%
							end if	
							%>
						</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
function getStoreShortName(shortDesc, pathFavicon, color)
	if isnull(pathFavicon) then 
		getStoreShortName = "<font color=""" & color & """><b>" & shortDesc & "</b></font>"
	else
		getStoreShortName = "<img src=""" & pathFavicon & """ border=""0"" width=""16"" height=""16"" />"
	end if
end function
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
