<%
response.buffer = false
pageTitle = "Create Nextag Product List"
header = 1
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
Server.ScriptTimeout = 9000 'seconds

dim fs, file, filename, path
'Map the file name to the physical path on the server.
filename = "productList_nextag.txt"
path = Server.MapPath("tempCSV") & "\" & filename
path = replace(path,"admin\","")
%>

<table width="85%" border="0" cellspacing="0" cellpadding="6" align="center">
	<tr bgcolor="#CCCCCC">
		<td align="center" valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
	<tr>
		<td width="47%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="15">
				<tr>
					<td class="normalText">
						<p>
						<form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method="post">
							<p>Create Nextag Product List</p>
							<p><input type="submit" name="submitted" value="Create"></p>
						</form>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<%
						if request("submitted") = "Create" then
							set fs = CreateObject("Scripting.FileSystemObject")
							Response.write("<b>CreateFile:</b><br>")
							DeleteFile(path)
							CreateFile(path)
							response.write "Here is the <a href=""../tempCSV/" & filename & """>file</a>"
						end if
						%>
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>

<%
sub DeleteFile(path)
	'If the file already exists, delete it.
	if fs.FileExists(path) then
		Response.write("Deleting " & filename & ".<br>")
		fs.DeleteFile(path)
	end if
end sub

sub CreateFile(path)
	'Create the file and write some data to it.
	response.write("Creating " & filename & ".<br>")
	set file = fs.CreateTextFile(path)
	dim SQL, RS, DoNotInclude, we_itemID
	SQL = "SELECT A.itemID, A.PartNumber, A.itemDesc, A.itemPic, A.price_our, A.itemLongDetail, A.inv_qty, A.UPCCode, B.brandName"
	SQL = SQL & " FROM we_items A INNER JOIN we_brands B ON A.brandid = B.brandid"
	SQL = SQL & " WHERE A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0"
	SQL = SQL & " AND A.typeID <> 16"
	SQL = SQL & " AND (A.itemID IN (20107,23345,24244,24245,29878,31114,36425,49909,49912,50034,51924,55163,56513,56515,31416,34769,56838)"
	SQL = SQL & " OR (A.PartNumber NOT LIKE 'DEC%' AND A.PartNumber NOT LIKE 'FP2%' AND A.PartNumber NOT LIKE 'FP3%' AND A.PartNumber NOT LIKE 'FP5%' AND A.PartNumber NOT LIKE 'BT1%'"
	SQL = SQL & " AND A.PartNumber NOT LIKE 'LC0%' AND A.PartNumber NOT LIKE 'LC1%' AND A.PartNumber NOT LIKE 'LC2%' AND A.PartNumber NOT LIKE 'LC3%' AND A.PartNumber NOT LIKE 'LC4%' AND A.PartNumber NOT LIKE 'LC5%'"
	SQL = SQL & " AND A.PartNumber NOT LIKE 'HF1%' AND A.PartNumber NOT LIKE 'HF2%' AND A.PartNumber NOT LIKE 'HF3%'"
	SQL = SQL & " AND (A.UPCCode IS NULL OR A.UPCCode = '')))"
	SQL = SQL & " ORDER BY A.itemID"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		file.WriteLine "Manufacturer" & vbtab & "Manufacturer Part #" & vbtab & "Product Name" & vbtab & "Product Description" & vbtab & "Click-Out URL" & vbtab & "Price" & vbtab & "Category: Other Format" & vbtab & "Category: NexTag Numeric ID" & vbtab & "Image URL" & vbtab & "Ground Shipping" & vbtab & "Stock Status" & vbtab & "Product Condition" & vbtab & "Marketing Message" & vbtab & "Weight" & vbtab & "Cost-per-Click" & vbtab & "UPC" & vbtab & "Distributor ID" & vbtab & "MUZE ID" & vbtab & "ISBN"
		do until RS.eof
			DoNotInclude = 0
			if RS("inv_qty") < 0 then
				SQL = "SELECT itemID FROM we_items WHERE hideLive = 0 AND inv_qty > 0 AND PartNumber = '" & RS("PartNumber") & "'"
				set RS2 = Server.CreateObject("ADODB.Recordset")
				RS2.open SQL, oConn, 3, 3
				if RS2.eof then DoNotInclude = 1
				RS2.close
				set RS2 = nothing
			end if
			if DoNotInclude = 0 then
				strline = RS("brandName") & vbtab
				strline = strline & "WE-" & RS("itemID") & vbtab
				strline = strline & RS("itemDesc") & vbtab
				strline = strline & replace(RS("itemLongDetail"), vbcrlf, " ") & vbtab
				strline = strline & "http://www.wirelessemporium.com/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp?refer=nextag" & vbtab
				strline = strline & RS("price_our") & vbtab
				strline = strline & vbtab
				strline = strline & "500070 : Electronics / Phones & Communications / Cell Phone Accessories" & vbtab
				strline = strline & "http://www.wirelessemporium.com/productpics/big/" & RS("itemPic") & vbtab
				strline = strline & "0" & vbtab
				strline = strline & "Yes" & vbtab
				strline = strline & "New" & vbtab
				strline = strline & "Free Shipping on All Orders" & vbtab
				strline = strline & vbtab
				strline = strline & vbtab
				strline = strline & RS("UPCCode") & vbtab
				strline = strline & vbtab
				strline = strline & vbtab
				strline = strline & vbtab
				file.WriteLine strline
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	file.close()
end sub
%>

<!--#include virtual="/includes/admin/admin_bottom.asp"-->
