<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<table width="100%" border="0" bordercolor="blue" cellspacing="0" cellpadding="8" class="smlText">
    <tr>
        <td align="left" class="biggerText">
            <img src="/images/press/WE-press.jpg" width="258" height="72" alt="WE Press Releases" border="0">
        </td>
    </tr>
    <%
    call fOpenConn()
    SQL = "SELECT * FROM we_PressReleases ORDER BY ReleaseDate DESC"
    set RS = Server.CreateObject("ADODB.Recordset")
    RS.open SQL, oConn, 3, 3
    %>
    <tr>
        <td>
            <table border="1" width="90%">
                <%
                do while not RS.eof
                    response.write "<tr>"
                    response.write "<td class=""content""><a href=""pressRelease?releaseNum=" & RS("ID") & """ target=""_blank"">" & RS("ReleaseDate") & "</a></td>" & vbCrLf
                    response.write "<td class=""smlText"">" & RS("Headline") & "</td>" & vbCrLf
                    response.write "<td class=""content""><a href=""pressReleasePDF?releaseNum=" & RS("ID") & """ target=""_blank"">PDF</a></td>" & vbCrLf
                    response.write "</tr>" & vbCrLf
                    RS.movenext
                loop
                %>
            </table>
        </td>
    </tr>
    <%
    RS.close
    set RS = nothing
    call fCloseConn()
    %>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->