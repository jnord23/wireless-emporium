<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
response.buffer = true
dim productListingPage
productListingPage = 1
%>
<!--#include virtual="/includes/template/top.asp"-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td width="100%" align="left" valign="middle">
            <h1>Current Promotions</h1>
            <p class="topText" style="margin-top:0px;margin-bottom:6px;">New promotions are always happening at Wireless Emporium. Bookmark this page and check back frequently to see what's going on in our cell phone accessories store.</p>
            <p class="topText" style="margin-top:0px;margin-bottom:6px;">Join our <a href="/mailing_list.asp">Mailing List</a> and receive even better deals, promotions, and access to contests and special events right to your inbox.</p>
        </td>
    </tr>
    <tr>
        <td width="100%" height="35" align="center" valign="middle">
            <img src="/images/promotions/hline_double.jpg" width="800" height="8" border="0">
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" valign="top">
            <table width="780" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                    <td width="280" align="left" valign="top">
                        <img src="/images/promotions/promo_charger.jpg" width="263" height="151" border="0" alt="15% off all Chargers!">
                    </td>
                    <td width="500" align="left" valign="top">
                        <p style="margin-top:0px;" class="biggerText">Ends 10/31/2010</p>
                        <p class="boldText">15% off all Chargers!</p>
                        <p>From now until 10/31, save an additional 15% off our already low prices on all cell phone chargers.  No coupon code needed: just add to your cart and save!</p>
                        <p><a href="http://www.wirelessemporium.com/cell-phone-chargers.asp">Shop Chargers</a></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!--
    <tr>
        <td width="100%" height="35" align="center" valign="middle">
            <img src="/images/promotions/hline.jpg" width="798" height="5" border="0">
        </td>
    </tr>
    -->
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<!--#include virtual="/includes/template/bottom.asp"-->