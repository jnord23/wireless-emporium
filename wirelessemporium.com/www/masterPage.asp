<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%	
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	dim forceRefresh : forceRefresh = 0
	dim brand : brand = request.QueryString("brand")
	dim pageName : pageName = prepStr(request.QueryString("pageName"))
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim categoryID : categoryID = prepInt(request.QueryString("categoryID"))
	dim featuredArtistID : featuredArtistID = prepInt(request.querystring("fArtistID"))	
	dim musicSkins : musicSkins = prepInt(request.QueryString("musicSkins"))
	dim musicSkinGenre : musicSkinGenre = prepStr(request.QueryString("musicSkinGenre"))
	dim musicSkinArtist : musicSkinArtist = prepStr(request.QueryString("musicSkinArtist"))
	dim strSort : strSort = prepStr(request("strSort"))
	dim brandModel : brandModel = request("brandModel")
	dim itemid : itemid = prepInt(request.QueryString("itemid"))
	dim page : page = prepInt(request.QueryString("page"))
	dim xPage : xPage = request.ServerVariables("HTTP_X_REWRITE_URL")
	dim utm_newsletter : utm_newsletter = prepStr(request.QueryString("utm_newsletter"))		
	
	if prepStr(request.QueryString("utm_promocode")) <> "" then 
		'cookie no longer work properly in wirelessemporium
		session("promocode") = prepStr(request.QueryString("utm_promocode"))
		response.Cookies("promocode") = prepStr(request.QueryString("utm_promocode"))
		Response.Cookies("promocode").Expires = DateAdd("d", 1, now)
	end if
	
	if instr(xPage,"utm_") > 0 then xPage = left(xPage,instr(xPage,"utm_")-2)
	
	if instr(request.ServerVariables("QUERY_STRING"),"?") > 0 then response.Redirect(request.ServerVariables("URL") & "?" & replace(request.ServerVariables("QUERY_STRING"),"?","&"))
	
	if isnull(request.Cookies("headsetPromo")) or prepInt(request.Cookies("headsetPromo")) = 0 then
		response.Cookies("headsetPromo") = 1
		response.Cookies("headsetPromo").Expires = date + 5
		session("showPromoCode") = 1
	end if
	
'	forceServerName = "staging"
	forceServerName = "cancelStagingForce"
	
	brandModel = replace(brandModel,"--","-")
	brandModel = prepStr(brandModel)
	
	serverName = useHttp & "://" & request.ServerVariables("SERVER_NAME")
	
	userBrowser = request.ServerVariables("HTTP_USER_AGENT")
	mobileAccess = prepStr(request.QueryString("mobileAccess"))
	noMobileRedirect = prepInt(noMobileRedirect)
	curPage = prepStr(xPage)
	do while instr(curPage,"--") > 0
		curPage = replace(curPage,"--","-")
	loop

	if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
		stitchLoc = "/productPics/devStitch/"
	else
		stitchLoc = "/productPics/stitch/"
	end if
	
	if noMobileRedirect = 0 then
		if isnull(session("mobileAccess")) or len(session("mobileAccess")) < 1 then
			'no mobile bypass session found
			if (instr(userBrowser,"Mobile") > 0 or instr(userBrowser,"BlackBerry") > 0 or instr(userBrowser,"Opera Mini") > 0) and instr(userBrowser,"iPad") < 1 then
				'when it is e-mail blast do not send them WE mobile site
				if lcase(request.querystring("utm_medium")) <> "email" then
					'mobile browser detected
					if mobileAccess = "" then
						'no mobile bypass in QS - move to mobile site with brand/model/category info
						response.Redirect("http://m.wirelessemporium.com" & xPage & ".htm")
					else
						'mobile bypass found in QS - save session
						session("mobileAccess") = 1
					end if			
				end if
			end if
		end if
	end if
	
	dim path : path = "/compressedPages/"
	dim testStitch : testStitch = 0
	
	if brand <> "" then
		if brand = "iphone" then
			compPageName = "brand_iPhone.htm"
			basePageName = "iphone-accessories_mp.asp"
			mpPageName = "iphone-accessories_mp.asp?curPage=" & curPage
		else
			if brand = "hp-palm" then brand = "hp/palm"
			if brand = "sony-ericsson" then brand = "Sony Ericsson"
			if brand = "other-brand" then brand = "other"
			sql = "select brandID from we_brands where brandName like '" & brand & "%'"
			session("errorSQL") = sql
			set brandRS = oConn.execute(sql)
			
			if brandRS.EOF then
				if instr(request.ServerVariables("SCRIPT_NAME"),"staging") > 0 then
					response.Write("can't find the brand")
					response.End()
				else
					response.Redirect("/")
				end if
			end if
			brandID = brandRS("brandID")
			compPageName = "brand_" & brandID & ".htm"
			basePageName = "brandNew2.asp"
			mpPageName = "brand_mp.asp?brandID=" & brandID & "&curPage=" & curPage
		end if
		path = "/compressedPages/brand/"
		
		testStitch = 1
	elseif pageName = "bmc" then
		compPageName = "brand_" & brandID & "_model_" & modelID & "_category_" & categoryID & ".htm"
		basePageName = "brand-model-category.asp"
		mpPageName = "brand-model-category_mp.asp?curPage=" & curPage & "&brandID=" & brandID & "&modelID=" & modelID & "&categoryID=" & categoryID & "&page=" & page
		path = "/compressedPages/brandModelCat/"
		
		testStitch = 1
	elseif pageName = "bmcd" then
		sql = "exec sp_createProductListByModelID " & modelID
		session("errorSQL") = sql
		'oConn.execute(sql)
		
		compPageName = "brand_" & brandID & "_model_" & modelID & "_category_" & categoryID
		if featuredArtistID <> 0 then compPageName = compPageName & "_fartist_" & featuredArtistID
		if strSort <> "" then compPageName = compPageName & "_sb_" & strSort
		compPageName = compPageName & ".htm"
		
		basePageName = "brand-model-category-details.asp"
		mpPageName = "brand-model-category-details_mp.asp?curPage=" & curPage & "&brandID=" & brandID & "&modelID=" & modelID & "&categoryID=" & categoryID & "&fArtistID=" & featuredArtistID & "&page=" & page & "&musicSkins=" & musicSkins & "&musicSkinGenre=" & musicSkinGenre & "&musicSkinArtist=" & musicSkinArtist & "&strSort=" & strSort
		path = "/compressedPages/brandModelCatDetails/"
		
		testStitch = 1
	elseif brandModel <> "" then
		compPageName = "accessories-for-" & brandModel & ".htm"
		basePageName = "brand-model.asp"
		mpPageName = "brand-model_mp.asp?curPage=" & curPage & "&brandModel=" & brandModel
		path = "/compressedPages/brandModel/"
		
		testStitch = 1
	elseif pageName = "checkout" then
		compPageName = "checkout_" & mySession & ".htm"
		basePageName = "cart/checkout.asp"
		mpPageName = "cart/checkout_mp.asp?curPage=" & curPage & "&passSessionID=" & mySession & "&passPromocode=" & session("promocode") & "&passSrToken=" & request.Cookies("srLogin")
		path = "/compressedPages/"
	elseif pageName = "cb" then
		compPageName = "category_" & categoryID & "_" & brandID & ".htm"
		basePageName = "category-brand.asp"
		mpPageName = "category-brand_mp.asp?curPage=" & curPage & "&categoryID=" & categoryID & "&brandID=" & brandID
		path = "/compressedPages/categoryBrands/"
	elseif categoryID > 0 then
		compPageName = "category_" & categoryID & ".htm"
		basePageName = "category.asp"
		mpPageName = "category_mp.asp?curPage=" & curPage & "&categoryID=" & categoryID
		path = "/compressedPages/categories/"
	else
		compPageName = "index.htm"
		basePageName = "index.asp"
		mpPageName = "index_mp.asp?curPage=" & curPage
		
		testStitch = 1
	end if
	
	cms_basepage = mpPageName
	if inStr(cms_basepage,"?") > 0 then cms_basepage = left(cms_basepage,inStr(cms_basepage,"?")-1)
	
	if instr(mpPageName,"?") > 0 then
		mpPageName = mpPageName & "&promoCookie=1"
	else
		mpPageName = mpPageName & "?promoCookie=1"
	end if
	
	if testStitch = 1 then
		bottomMsg = "st"
		if fso.fileExists(server.MapPath(stitchLoc & replace(compPageName,".htm",".jpg"))) then
			bottomMsg = bottomMsg & " - found"
			set curFile = fso.getFile(server.MapPath(stitchLoc & replace(compPageName,".htm",".jpg")))
			if curFile.size < 5000 then
				refreshReason = " - too small"
				forceRefresh = 1
			else
				bottomMsg = bottomMsg & " - Good:" & formatNumber(curFile.size,0)
			end if
			curFile = null
		else
			refreshReason = "No Stitch Found"
			forceRefresh = 1
		end if
	end if
	
	siteId = 0
	pageName = mpPageName
	if instr(mpPageName, "index_mp.asp") > 0 then pageName = "/"
	if basePageName = "product.asp" then pageName = "product.asp"
	
'	cmsData(xPage)
'	metaArray = split(session("metaTags"),"##")
'	if isarray(metaArray) then
'		SEtitle = metaArray(0)
'		SEdescription = metaArray(1)
'		SEkeywords = metaArray(2)
'		SeTopText = metaArray(3)
'		SeBottomText = metaArray(4)
'		if ubound(metaArray) > 4 then SeH1 = metaArray(5)
'	end if
	if instr(compPageName,"utm_") > 0 then compPageName = left(compPageName,instr(compPageName,"utm_")-2)
	session("errorSQL") = path & compPageName
	
	if fso.fileExists(server.MapPath(path & compPageName)) then
		if instr(mpPageName,"?") > 0 then getMpPage = left(mpPageName,instr(mpPageName,"?")-1) else getMpPage = mpPageName
		set f1 = fso.getFile(server.MapPath("/" & getMpPage))
		set f2 = fso.getFile(server.MapPath(path & compPageName))
		if f1.DateLastModified > f2.DateLastModified then
			forceRefresh = 1
			refreshReason = "modified file"
		end if
		'if DateDiff("h", f2.DateLastModified, now) > 1 then forceRefresh = 1
		if instr(request.ServerVariables("SERVER_NAME"),forceServerName) > 0 then
			forceRefresh = 1
			refreshReason = "staging forced"
		end if
		if basePageName = "cart/checkout.asp" then forceRefresh = 1
	else
		refreshReason = "no file found"
		forceRefresh = 1
	end if
	
	if forceRefresh = 1 then
		server.ScriptTimeout = 99999
		useURL = serverName & "/" & mpPageName
		set XMLHTTP = server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
		XMLHTTP.Open "GET", useURL, False
		session("errorSQL") = useURL
		
		'on error resume next
		XMLHTTP.Send
		'errorLap = 0
		'do while err.number <> 0
			'errorLap = errorLap + 1
			'response.Write("<!-- ImgError:" & errorLap & " -->")
			'err.Clear
			'XMLHTTP.Send
			'if errorLap = 200 then exit do
		'loop
		'on error goto 0
		
		allText = xmlHTTP.ResponseText
		
		if instr(lcase(allText),"<script") > 0 then
			lapCnt = 0
			do while len(allText) > 0
				lapCnt = lapCnt + 1
				if instr(lcase(allText),"<script") > 0 then
					nextScript = instr(allText,"<script")
					tempHolding = left(allText,nextScript-1)
					session("errorSQL") = "nextScript:" & nextScript & "<br>" & allText
					if nextScript > 1 then allText = mid(allText,nextScript-1)
					
					tempHolding = replace(tempHolding,vbCrLf,"")
					tempHolding = replace(tempHolding,"  ","")
					showPage = showPage & replace(tempHolding,vbTab,"")
					
					endScript = instr(lcase(allText),"</script>")
					showPage = showPage & vbCrLf & left(allText,endScript+8)
					allText = mid(allText,endScript+9)
				else
					tempHolding = replace(allText,vbCrLf,"")
					tempHolding = replace(tempHolding,"  ","")
					showPage = showPage & replace(tempHolding,vbTab,"")
					allText = ""
				end if	
				if lapCnt = 100 then
					response.Write(showPage)
					response.Write("force stop<br />" & vbCrLf)
					response.End()
				end if
			loop
			
			stitchFileName = replace(compPageName,".htm",".jpg")
			if instr(stitchFileName,"/") > 0 then
				stitchFileName = mid(stitchFileName,instrrev(stitchFileName,"/")+1)
			end if
			
			'Stitch images together and adjustCode
			dim performStitch : performStitch = 0
			'if instr(mpPageName,"brand_mp.asp") > 0 or mpPageName = "index_mp.asp" or instr(mpPageName,"brand-model_mp.asp") > 0 then performStitch = 1
			'if instr(mpPageName,"brand-model-category-details_mp.asp") > 0 or instr(mpPageName,"brand-model-category_mp.asp") > 0 then performStitch = 1
			'if instr(mpPageName,"category_mp.asp") > 0 or instr(mpPageName,"category-brand_mp.asp") > 0 then performStitch = 1
			
			dim skipImg : skipImg = 0
			dim compressImg : compressImg = 0
			dim totalSrc : totalSrc = ""
			dim newCanvasWidth : newCanvasWidth = 0
			dim newCanvasHeight : newCanvasHeight = 0
			dim canvasHeight : canvasHeight = 0
			
			if performStitch = 1 then
				if fso.fileExists(server.MapPath(stitchLoc & stitchFileName)) then fso.deleteFile(server.MapPath(stitchLoc & stitchFileName))
				imgLap = 0
				
				if not fso.fileExists(server.MapPath(stitchLoc & stitchFileName)) then
					
					set imgDetailObj = Server.CreateObject("Persits.Jpeg")
					
					do while instr(showPage,"<img") > 0 or instr(showPage,"background:") > 0 or instr(showPage,"background-image:") > 0
						imgLap = imgLap + 1
						if instr(showPage,"<img") > 0 then
							isBackground = false
							bgImg = false
							nextImg = mid(showPage,instr(showPage,"<img"))
							nextImg = left(nextImg,instr(nextImg,">"))
							nextSrc = mid(nextImg,instr(nextImg,"src=")+5)
							endSrcLoc1 = instr(nextSrc,"'")
							endSrcLoc2 = instr(nextSrc,"""")
							if endSrcLoc1 < 1 then
								if endSrcLoc2 > 1 then
									nextSrc = left(nextSrc,instr(nextSrc,"""")-1)
								end if
							else
								if endSrcLoc2 = 0 then endSrcLoc2 = 9999
								if endSrcLoc1 < endSrcLoc2 then
									nextSrc = left(nextSrc,instr(nextSrc,"'")-1)
								else
									nextSrc = left(nextSrc,instr(nextSrc,"""")-1)
								end if
							end if
						elseif instr(showPage,"background:") > 0 then
							isBackground = true
							bgImg = false
							nextImg = mid(showPage,instr(showPage,"background:"))
							nextImg = left(nextImg,instr(nextImg,";"))
							if len(nextImg) > 300 then
								nextImg = left(nextImg,instr(nextImg,")"))
							end if
							tempNextImg = replace(nextImg," ","")
							session("errorSQL") = tempNextImg
							nextSrc = replace(tempNextImg,"background:url(","")
							'nextSrc = replace(nextSrc,")","")
							if instr(nextSrc,")") > 0 then nextSrc = left(nextSrc,instr(nextSrc,")")-1) else nextSrc = "temp.png"
						elseif instr(showPage,"background-image:") > 0 then
							isBackground = true
							bgImg = true
							nextImg = mid(showPage,instr(showPage,"background-image:"))
							nextImg = left(nextImg,instr(nextImg,";"))
							if len(nextImg) > 300 then
								nextImg = left(nextImg,instr(nextImg,")"))
							end if
							tempNextImg = replace(nextImg," ","")
							nextSrc = replace(tempNextImg,"background-image:url(","")
							'nextSrc = replace(nextSrc,")","")
							nextSrc = left(nextSrc,instr(nextSrc,")")-1)
						end if
						
						imgX = 0
						imgY = 0
						pictureCreationStarted = 0
						
						if instr(nextSrc,".png") > 0 or instr(nextSrc,".gif") > 0 or instr(nextSrc,"//") > 0 or instr(nextSrc,"/stitch/") > 0 then
							pngFile = 1
							'stitchFileName = "test.png"
						else
							pngFile = 0
							
							session("errorSQL") = "nextSrc:" & nextSrc
							if fso.fileExists(server.MapPath(nextSrc)) then
								imgDetailObj.Open server.MapPath(nextSrc)
								curImgHeight = imgDetailObj.Height
								curImgWidth = imgDetailObj.Width
								
								preCanvasWidth = newCanvasWidth
								newCanvasWidth = newCanvasWidth + curImgWidth + 3
								if curImgHeight > newCanvasHeight then newCanvasHeight = curImgHeight
							else
								pngFile = 1
							end if
							
							if pngFile = 1 then
								'oJpeg.Canvas.DrawPNG canvasWidth, imgY, server.MapPath(nextSrc)
							else
								totalSrc = totalSrc & nextSrc & "@@" & preCanvasWidth & "##"
							end if
						end if
						
						if pngFile = 1 then
							skipImg = skipImg + 1
							if isBackground then
								newDiv = replace(nextImg,"background","bgPlaceholder")
							else
								newDiv = replace(nextImg,"<img","<notImg")
							end if
						else
							compressImg = compressImg + 1
							if isBackground then
								newDiv = "bgPlaceholder:url(" & stitchLoc & stitchFileName & ") -" & preCanvasWidth & "px 0px no-repeat;"
							else
								newDiv = "<notImg style='width: " & curImgWidth & "px; height: " & curImgHeight & "px; bgPlaceholder: url(" & stitchLoc & stitchFileName & ") -" & preCanvasWidth & "px 0px no-repeat; cursor:pointer;' src='/images/blank.gif' border='0' />"
							end if
						end if
						showPage = replace(showPage,nextImg,newDiv)
						if imgLap = 500 then
							response.Write("time to bail out!")
							response.End()
						end if
					loop
					
					set imgDetailObj = nothing
					
					if totalSrc <> "" then
						totalSrc = left(totalSrc,len(totalSrc)-2)
						totalSrcArray = split(totalSrc,"##")
						
						set oImg = Server.CreateObject("Persits.Jpeg")
						set oJpeg = Server.CreateObject("Persits.Jpeg")
						oJpeg.Quality = 80
						oJpeg.Interpolation = 1
						
						oJpeg.New newCanvasWidth, newCanvasHeight, &HFFFFFF
						
						for i = 0 to ubound(totalSrcArray)
							imgDetailArray = split(totalSrcArray(i),"@@")
							imgSrc = imgDetailArray(0)
							imgLocX = imgDetailArray(1)
							
							oImg.Open server.MapPath(imgSrc)
							
							oJpeg.Canvas.DrawImage imgLocX, 0, oImg
						next
						
						'on error resume next
							oJpeg.Save server.MapPath(stitchLoc & stitchFileName)
							'errorLap = 0
							'do while err.number <> 0
								'errorLap = errorLap + 1
								'response.Write("<!-- ImgError:" & errorLap & " -->")
								'err.Clear
								'oJpeg.Save server.MapPath(stitchLoc & stitchFileName)
								'if errorLap = 200 then exit do
							'loop
						'On Error Goto 0
						
						set oJpeg = nothing
						set oImg = nothing
					end if
					
					showPage = replace(showPage,"bgPlaceholder","background")
					showPage = replace(showPage,"bgPlaceholder2","background-image")
					showPage = replace(showPage,"notImg","img")
				end if
			end if
		else
			showPage = replace(allText,vbCrLf,"")
			showPage = replace(showPage,"  ","")
			showPage = replace(showPage,vbTab,"")
		end if
		
		session("errorSQL") = path & compPageName
		'on error resume next
		set tfile = fso.createTextFile(server.MapPath(path & compPageName),true,true)
		'errorLap = 0
		'do while err.number <> 0
			'errorLap = errorLap + 1
			'response.Write("<!-- ImgError:" & errorLap & " -->")
			'err.Clear
			'set tfile = fso.createTextFile(server.MapPath(path & compPageName),true,true)
			'if errorLap = 200 then exit do
		'loop
		'on error goto 0
		
		tfile.writeLine(showPage)
		tfile.close	
		tfile = null
		newFile = 1
	else
		newFile = 0
	end if
	
	f1 = null
	f2 = null
	
	useURL = serverName & path & compPageName

	if showPage = "" then
		set curHTML = fso.OpenTextFile(server.MapPath(path & compPageName),1,false,-1)
		response.Write(curHTML.readall)
	else
		if instr(lcase(showPage),"problem with the page you are trying to reach") > 0 or instr(showPage,"ASPDescription") > 0 then
			fso.deleteFile(server.MapPath(path & compPageName))
			errorPage = 1
		else
			response.Write(showPage)
		end if
	end if
	
	fso = null
	
	response.Flush()
	if modelID > 0 then
		sql = "exec sp_createProductListByModelID " & modelID
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
%>
<%
if newFile = 1 then
	response.Write("<!-- new file created:" & refreshReason & " -->")
else
	response.Write("<!-- old file still fresh -->")
end if
if performStitch = 1 then
	response.Write("<!-- mp newImg -->")
else
	response.Write("<!-- mp oldImg -->")
end if
%>
<% response.Write("<!-- bottomMsg: " & bottomMsg & " -->") %>
<% response.Write("<!-- ErrorCode: " & session("errorCode") & " -->") %>
<%="<!-- sid:" & session.SessionID & " -->"%>
<%="<!-- csid:" & prepInt(request.cookies("mySession")) & " -->"%>
<div style="display:none;" id="compareResultDiv"></div>
<%
if session("showPromoCode") = 1 then
%>
<script language="javascript" src="/includes/js/headsetPromo.js"></script>
<%
	session("showPromoCode") = 0
else
%>
<script language="javascript">//headsetPromo.js not needed: <%=session("showPromoCode")%> (<%=request.ServerVariables("REMOTE_ADDR")%>)</script>
<%
end if
%>
<script language="javascript">
	<%	
	if errorPage = 1 then
	%>
	//window.location = window.location
	<% end if %>
	function dynamicLoad() {
		var randomnumber=Math.floor(Math.random()*100001);

		ajax('/ajax/dynamicUpdate.asp?myCartDetails=1','siteTop-CartBar')
		ajax('/ajax/dynamicUpdate.asp?loginStatus=1&rn='+randomnumber,'account_nav');		
		<% if basePageName = "index.asp" then %>
			ajax('/ajax/dynamicUpdate.asp?featuredProducts=1','featuredProducts')
			<% if utm_newsletter <> "" then %>
				addNewsletter('<%=utm_newsletter%>','newsletter');
			<%end if%>
		<% end if %>
	}
	dynamicLoad()
	
	<%
	mpPageName = replace(mpPageName,"?","$QM")
	mpPageName = replace(mpPageName,"&","$AM")
	%>
	setTimeout("fetchCompare()",2000)
	
	function fetchCompare() {
		ajax('/ajax/masterCompare.asp?mpPageName=<%=mpPageName%>&compPageName=<%=path & compPageName%>','compareResultDiv')
		//setTimeout("compareResult()",500)
	}
	
	function compareResult() {
		if (document.getElementById("compareResultDiv").innerHTML == "") {
			setTimeout("compareResult()",500)
		}
		else if (document.getElementById("compareResultDiv").innerHTML == "New Page Found") {
			//location.reload(true)
		}
	}
</script>