<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_functions.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
response.buffer = true
dim productListingPage
productListingPage = 1

dim seriesid, categoryid
seriesid = request.querystring("seriesid")
if seriesid = "" or not isNumeric(seriesid) then
	call PermanentRedirect("/")
end if
categoryid = request.querystring("categoryid")
if categoryid = "" or not isNumeric(categoryid) then
	call PermanentRedirect("/")
end if

call fOpenConn()
SQL = "SELECT A.*, C.brandName FROM we_ModelSeries A INNER JOIN we_brands C ON A.brandid = C.brandid WHERE A.id = '" & seriesid & "'"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
end if

dim modelSeriesName, modelSeriesIDs, brandName, brandID
modelSeriesName = RS("modelSeriesName")
modelSeriesIDs = RS("modelSeriesIDs")
brandName = RS("brandName")
brandID = RS("brandID")

SQL = "SELECT A.itemID, A.brandID, A.modelID, A.typeID, A.subtypeID, A.itemDesc, A.itemPic, A.flag1, A.price_Retail, A.price_Our, A.inv_qty,"
SQL = SQL & " A.ItemKit_NEW, A.hotDeal, A.hideLive, B.modelName, B.modelImg, B.excludePouches, B.includeNFL, B.includeExtraItem, D.typeName"
SQL = SQL & " FROM we_Items AS A INNER JOIN"
SQL = SQL & " we_Models AS B ON A.modelID = B.modelID INNER JOIN"
SQL = SQL & " we_Types AS D ON A.typeID = D.typeID"
SQL = SQL & " WHERE (A.itemID IN"
SQL = SQL & " (SELECT itemID FROM we_Items AS W"
SQL = SQL & " WHERE (Sports = 0) AND (typeID = '1') AND (modelID IN (" & modelSeriesIDs & ")) AND (inv_qty <> 0) AND (price_Our > 0)"
SQL = SQL & " OR (Sports = 0) AND (typeID = '1') AND (modelID IN (" & modelSeriesIDs & ")) AND (price_Our > 0) AND (ItemKit_NEW IS NOT NULL)"
SQL = SQL & " UNION ALL"
SQL = SQL & " SELECT R.ITEMID FROM we_Items AS I INNER JOIN"
SQL = SQL & " we_relatedItems AS R ON R.ITEMID = I.itemID"
SQL = SQL & " WHERE (I.Sports = 0) AND (R.typeid = '1') AND (R.modelid IN (" & modelSeriesIDs & ")) AND (I.inv_qty <> 0) AND (I.price_Our > 0)"
SQL = SQL & " OR (I.Sports = 0) AND (R.typeid = '1') AND (R.modelid IN (" & modelSeriesIDs & ")) AND (I.price_Our > 0) AND (I.ItemKit_NEW IS NOT NULL)))"
SQL = SQL & " AND (A.hideLive = 0) AND (A.inv_qty <> 0) AND (A.modelID IN (" & modelSeriesIDs & "))"
SQL = SQL & " OR (A.itemID IN"
SQL = SQL & " (SELECT itemID FROM we_Items AS W"
SQL = SQL & " WHERE (Sports = 0) AND (typeID = '1') AND (modelID IN (" & modelSeriesIDs & ")) AND (inv_qty <> 0) AND (price_Our > 0)"
SQL = SQL & " OR (Sports = 0) AND (typeID = '1') AND (modelID IN (" & modelSeriesIDs & ")) AND (price_Our > 0) AND (ItemKit_NEW IS NOT NULL)"
SQL = SQL & " UNION ALL"
SQL = SQL & " SELECT R.ITEMID FROM we_Items AS I INNER JOIN"
SQL = SQL & " we_relatedItems AS R ON R.ITEMID = I.itemID"
SQL = SQL & " WHERE (I.Sports = 0) AND (R.typeid = '1') AND (R.modelid IN (" & modelSeriesIDs & ")) AND (I.inv_qty <> 0) AND (I.price_Our > 0)"
SQL = SQL & " OR (I.Sports = 0) AND (R.typeid = '1') AND (R.modelid IN (" & modelSeriesIDs & ")) AND (I.price_Our > 0) AND (I.ItemKit_NEW IS NOT NULL)))"
SQL = SQL & " AND (A.hideLive = 0) AND (A.ItemKit_NEW IS NOT NULL)"
SQL = SQL & " ORDER BY A.modelID, A.flag1 DESC"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3

if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
end if

dim categoryName
categoryName = RS("typeName")

select case seriesid
	case "1"
		select case categoryid
			case "1"
				SEtitle = "BlackBerry Curve Batteries � Discount BlackBerry Curve Accessories"
				SEdescription = "Shop online at the #1 Cell Phone Accessories Store � WirelessEmporium for the best deals on BlackBerry Curve Accessories like BlackBerry Curve Batteries and more!"
				SEkeywords = "blackberry curve batteries, blackberry curve battery, blackberry curve 8330 batteries, blackberry curve 8310 batteries, blackberry curve 8320 batteries, blackberry curve accessories"
				topText = "BlackBerry Curve <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-batteries.asp"" title=""BlackBerry Cell Phone Batteries"">cell phone batteries</a> are at the heart of every BlackBerry Curve cell phone. We're trusted by thousands of businesses and customers to provide quality batteries at discounted prices. Wireless Emporium offers the lowest prices on BlackBerry Curve <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""BlackBerrry Cell phone Accessories"">cell phone accessories</a> including BlackBerry Curve 8300/8310/8320 and BlackBerry Curve 8900 models because we know it's important to our customers to get the most out of their money. Choose from OEM, standard, and extended batteries and choose a type that fits your lifestyle. All of our <a class=""topText-link"" href=""http://www.wirelessemporium.com/blackberry-cell-phone-accessories.asp"" title=""BlackBerry Curve Accessories"">BlackBerry Accessories</a> like batteries have been quality tested and meet or surpass manufacturers' standards. And when you buy from Wireless Emporium, your order is shipped for free and your satisfaction is guaranteed!"
		end select
	case "2"
		select case categoryid
			case "1" : topText = "Motorola Razr cell phone batteries are at the heart of every Motorola Razr cell phone. We're trusted by thousands of businesses and customers to provide quality batteries at discounted prices. Wireless Emporium offers the lowest prices on Motorola Razr cell phone batteries including Motorola V3 Razr, Motorola Razr V3m, and Motorola Razr V3c models because we know it's important to our customers to get the most out of their money. Choose from OEM, standard, and extended batteries and choose a type that fits your lifestyle. All of our batteries have been quality tested and meet or surpass manufacturers' standards. And when you buy from Wireless Emporium, your order is shipped for free and your satisfaction is guaranteed!"
		end select
	case "3"
		select case categoryid
			case "1" : topText = "Motorola Krzr cell phone batteries are at the heart of every Motorola Krzr cell phone. We're trusted by thousands of businesses and customers to provide quality batteries at discounted prices. Wireless Emporium offers the lowest prices on Motorola Krzr cell phone batteries including Motorola Krzr K1 and Motorola Krzr K1m models because we know it's important to our customers to get the most out of their money. Choose from OEM, standard, and extended batteries and choose a type that fits your lifestyle. All of our batteries have been quality tested and meet or surpass manufacturers' standards. And when you buy from Wireless Emporium, your order is shipped for free and your satisfaction is guaranteed!"
		end select
	case "4"
		select case categoryid
			case "1" : topText = "LG enV cell phone batteries are at the heart of every LG enV cell phone. We're trusted by thousands of businesses and customers to provide quality batteries at discounted prices. Wireless Emporium offers the lowest prices on LG enV cell phone batteries including LG enV Touch VX11000 and LG enV3 VX9200 models because we know it's important to our customers to get the most out of their money. Choose from OEM, standard, and extended batteries and choose a type that fits your lifestyle. All of our batteries have been quality tested and meet or surpass manufacturers' standards. And when you buy from Wireless Emporium, your order is shipped for free and your satisfaction is guaranteed!"
		end select
	case "5"
		select case categoryid
			case "1" : topText = "LG Rumor cell phone batteries are at the heart of every LG Rumor cell phone. We're trusted by thousands of businesses and customers to provide quality batteries at discounted prices. Wireless Emporium offers the lowest prices on LG Rumor cell phone batteries including LG Rumor LX260 and LG Rumor2 LX65 models because we know it's important to our customers to get the most out of their money. Choose from OEM, standard, and extended batteries and choose a type that fits your lifestyle. All of our batteries have been quality tested and meet or surpass manufacturers' standards. And when you buy from Wireless Emporium, your order is shipped for free and your satisfaction is guaranteed!"
		end select
	case "6"
		select case categoryid
			case "1" : topText = "LG Chocolate cell phone batteries are at the heart of every LG Chocolate cell phone. We're trusted by thousands of businesses and customers to provide quality batteries at discounted prices. Wireless Emporium offers the lowest prices on LG Chocolate cell phone batteries including LG Chocolate Touch VX8575 and LG Chocolate 3 VX8560 models because we know it's important to our customers to get the most out of their money. Choose from OEM, standard, and extended batteries and choose a type that fits your lifestyle. All of our batteries have been quality tested and meet or surpass manufacturers' standards. And when you buy from Wireless Emporium, your order is shipped for free and your satisfaction is guaranteed!"
	end select
end select

dim SEtitle, SEdescription, SEkeywords, topText, holdModelName
if SEtitle = "" then SEtitle = "Discount " & brandName & " " & modelSeriesName & " " & nameSEO(categoryName) & " � " & brandName & " " & modelSeriesName & " Cell Phone " & nameSEO(categoryName)
if SEdescription = "" then SEdescription = brandName & " " & modelSeriesName & " " & nameSEO(categoryName) & " � Wireless Emporium is the largest store online for " & brandName & " " & modelSeriesName & " " & nameSEO(categoryName) & " & other " & brandName & " " & modelSeriesName & " Cell Phone Accessories at the lowest prices."
if SEkeywords = "" then
	SEkeywords = brandName & " " & modelSeriesName & " " & nameSEO(categoryName) & ", " & brandName & " " & modelSeriesName & " Cell Phone " & nameSEO(categoryName) & ", "
	holdModelName = "99999999"
	do until RS.eof
		if RS("modelName") <> holdModelName then
			SEkeywords = SEkeywords & brandName & " " & RS("modelName") & " " & nameSEO(categoryName) & ", "
			SEkeywords = SEkeywords & brandName & " " & modelSeriesName & " " & RS("modelName") & " " & nameSEO(categoryName) & ", "
			holdModelName = RS("modelName")
		end if
		RS.movenext
	loop
	SEkeywords = left(SEkeywords,len(SEkeywords)-2)
	RS.movefirst
end if

dim modelID, modelName, modelImg, excludePouches, includeNFL, includeExtraItem
modelID = RS("modelID")
modelName = RS("modelName")
modelImg = RS("modelImg")
excludePouches = RS("excludePouches")
includeNFL = RS("includeNFL")
includeExtraItem = RS("includeExtraItem")

dim strBreadcrumb
strBreadcrumb = brandName & " " & modelSeriesName & " " & nameSEO(categoryName)
%>
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="/<%=brandSEO(brandID)%>"><%=brandName%> Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle" width="100%">
            <h1><%=brandName & " " & modelSeriesName & " " & nameSEO(categoryName)%></h1>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="750">
            <table border="0" cellspacing="7" cellpadding="0">
                <tr>
                    <td align="center" valign="top" width="150">
                        <p class="topText">
                            <img src="/productpics/models/<%=modelImg%>" border="0" alt="<%=brandName & " " & modelSeriesName & " " & nameSEO(categoryName)%>">
                        </p>
                    </td>
                    <td align="left" valign="top" width="600">
                        <p class="topText" style="margin-top:0px;margin-bottom:6px;"><%=topText%></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="798">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                            <%
                            dim holdModelID, OLDModelID, DoNotDisplay, RSkit
                            holdModelID = RS("modelID")
                            OLDModelID = holdModelID
                            a = 0
                            response.write "<tr><td align=""left"" valign=""top"" colspan=""7""><h4>" & brandName & " " & modelName & " " & nameSEO(categoryName) & "</h4></td></tr>" & vbcrlf
                            response.write "<tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""798"" height=""5"" border=""0""></td></tr>" & vbcrlf
                            response.write "<tr>" & vbcrlf
                            do until RS.eof
                                modelID = RS("modelID")
                                modelName = RS("modelName")
                                modelImg = RS("modelImg")
                                if holdModelID <> OLDModelID then
                                    response.write "<tr><td align=""left"" valign=""top"" colspan=""7""><p>&nbsp;</p><h4>" & brandName & " " & modelName & " " & nameSEO(categoryName) & "</h4></td></tr>" & vbcrlf
                                    response.write "<tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""798"" height=""5"" border=""0""></td></tr>" & vbcrlf
                                    response.write "<tr>" & vbcrlf
                                end if
                                
                                dim altText, aCount, startCount, endCount, RSextra
                                a = 0
                                
                                if categoryid = 3 or categoryid = 7 then
                                    SQL = "SELECT MIN(price_Retail) AS minPrice_Retail, MIN(price_Our) AS minPrice_Our FROM we_items WHERE hidelive = 0 AND inv_qty > 0 AND typeid = 17 AND modelID = '" & modelID & "'"
                                    set RSextra = Server.CreateObject("ADODB.Recordset")
                                    RSextra.open SQL, oConn, 3, 3
                                    if not RSextra.eof then
                                        if RSextra("minPrice_Retail") > 0 and RSextra("minPrice_Our") > 0 then
                                            response.write "<td align=""center"" valign=""top"" width=""192"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""192"" height=""100%"">" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""middle"" width=""192"" height=""154""><a href=""/sb-" & brandID & "-sm-" & modelID & "-sc-17-full-body-protectors-" & formatSEO(brandname) & "-" & formatSEO(modelName) & ".asp""><img src=""/images/GadgetGuards/we_decal.jpg"" border=""0"" alt=""Gadget Guards & Decal Skins""></a></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""left"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""left"" valign=""top""><a class=""product-description2"" href=""/sb-" & brandID & "-sm-" & modelID & "-sc-17-full-body-protectors-" & formatSEO(brandname) & "-" & formatSEO(modelName) & ".asp"" title=""Gadget Guards & Decal Skins"">GADGET GUARDS & DECAL SKINS</a><br><br></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""right"" valign=""top"" height=""8""><img src=""/images/spacer.gif"" width=""1"" height=""8""></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""right"" valign=""bottom"">" & vbcrlf
                                            response.write "<span class=""pricing-gray"">Starting&nbsp;At:&nbsp;<s>" & formatCurrency(RSextra("minPrice_Retail")) & "</s></span><br><span class=""pricing-black"">You&nbsp;Save:&nbsp;" & formatCurrency(RSextra("minPrice_Retail") - RSextra("minPrice_Our")) & "</span><br><a href=""/sb-" & brandID & "-sm-" & modelID & "-sc-17-full-body-protectors-" & formatSEO(brandname) & "-" & formatSEO(modelName) & ".asp"" class=""product-description3""><img src=""/images/link_arrow.gif"" border=""0"" align=""abs-middle"">&nbsp;&nbsp;<u><i>Click Here For Our Complete Lineup</i></u></a><br><span class=""pricing-red"">" & formatCurrency(RSextra("minPrice_Our")) & "&nbsp;</span>" & vbcrlf
                                            response.write "</td></tr>" & vbcrlf
                                            response.write "</table></td>" & vbcrlf
                                            
                                            response.write "<td align=""center"" valign=""top"" width=""192"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""192"" height=""100%"">" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""middle"" width=""192"" height=""154""><a href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp""><img src=""/productpics/thumb/" & RS("itemPic") & """ border=""0"" alt=""" & altText & """></a></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""top""><a class=""product-description4"" href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"" title=""" & altText & """>" & RS("itemDesc") & "</a></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""top"" height=""8""><img src=""/images/spacer.gif"" width=""1"" height=""8""></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""bottom""><span class=""boldText"">Our&nbsp;Price:&nbsp;</span><span class=""pricing-orange2"">" & formatCurrency(RS("price_Our")) & "</span><br><span class=""pricing-gray2"">List&nbsp;Price:&nbsp;<s>" & formatCurrency(RS("price_Retail")) & "</s></span><br><span class=""pricing-gray2"">You&nbsp;Save:&nbsp;" & formatCurrency(RS("price_Retail") - RS("price_Our")) & "</span></td></tr>" & vbcrlf
                                            response.write "</table></td>" & vbcrlf
                                            
                                            response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                                            a = 1
                                        end if
                                    end if
                                end if
                                
                                if categoryid = 7 then
                                    dim boxHeader(6), imgSrc(6)
                                    if excludePouches = false then
                                        boxHeader(1) = "MLB OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                                        boxHeader(2) = "NCAA OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                                        boxHeader(3) = "DISNEY OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                                        boxHeader(4) = "BLING UNIVERSAL CELL PHONE POUCHES"
                                        imgSrc(1) = "LC-MLB-thumb.jpg"
                                        imgSrc(2) = "LC-NCAA-Thumb.jpg"
                                        imgSrc(3) = "LC-Disney-Thumb.jpg"
                                        imgSrc(4) = "LC-Blingpouch-Thumb.jpg"
                                        startCount = 1
                                        endCount = 4
                                    elseif includeNFL = true then
                                        boxHeader(5) = "NFL OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                                        imgSrc(5) = "LC-NFL-Thumb.jpg"
                                        startCount = 5
                                        endCount = 6
                                        boxHeader(6) = "NBA OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                                        imgSrc(6) = "LC-NBA-Thumb.jpg"
                                    end if
                                    if startCount > 0 and endCount > 0 then
                                        for aCount = startCount to endCount
                                            SQL = "SELECT TOP 1 price_Retail, price_Our FROM we_items WHERE hidelive=0 AND inv_qty > 0 AND Sports = '" & aCount & "'"
                                            set RSextra = Server.CreateObject("ADODB.Recordset")
                                            RSextra.open SQL, oConn, 3, 3
                                            if not RSextra.eof then
                                                altText = boxHeader(aCount)
                                                response.write "<td align=""center"" valign=""top"" width=""192"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""192"" height=""100%"">" & vbcrlf
                                                response.write "<tr><td align=""center"" valign=""middle"" width=""192"" height=""154""><a href=""/pouches-" & aCount & "-" & formatSEO(boxHeader(aCount)) & ".asp""><img src=""/productpics/thumb/" & imgSrc(aCount) & """ border=""0"" alt=""" & altText & """></a></td></tr>" & vbcrlf
                                                response.write "<tr><td align=""center"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
                                                response.write "<tr><td align=""center"" valign=""top""><a class=""product-description4"" href=""/pouches-" & aCount & "-" & formatSEO(boxHeader(aCount)) & ".asp"" title=""" & altText & """>" & boxHeader(aCount) & "</a></td></tr>" & vbcrlf
                                                response.write "<tr><td align=""center"" valign=""top"" height=""8""><img src=""/images/spacer.gif"" width=""1"" height=""8""></td></tr>" & vbcrlf
                                                response.write "<tr><td align=""center"" valign=""bottom""><span class=""boldText"">Our&nbsp;Price:&nbsp;</span><span class=""pricing-orange2"">" & formatCurrency(RSextra("price_Our")) & "</span><br><span class=""pricing-gray2"">List&nbsp;Price:&nbsp;<s>" & formatCurrency(RSextra("price_Retail")) & "</s></span><br><span class=""pricing-gray2"">You&nbsp;Save:&nbsp;" & formatCurrency(RSextra("price_Retail") - RSextra("price_Our")) & "</span></td></tr>" & vbcrlf
                                                response.write "</table></td>" & vbcrlf
                                                a = a + 1
                                                if a = 4 then
                                                    response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""798"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
                                                    a = 0
                                                else
                                                    response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                                                end if
                                            end if
                                            RSextra.close
                                            set RSextra = nothing
                                        next
                                    end if
                                elseif categoryid = 2 then
                                    if not isNull(includeExtraItem) and includeExtraItem > 0 then
                                        SQL = "SELECT itemID, itemDesc, itemPic, price_Retail, price_Our FROM we_items WHERE itemid = '" & includeExtraItem & "'"
                                        set RSextra = Server.CreateObject("ADODB.Recordset")
                                        RSextra.open SQL, oConn, 3, 3
                                        do until RSextra.eof
                                            altText = singularSEO(categoryName) & " for " & brandName & " " & modelName & " � " & RSextra("itemDesc")
                                            response.write "<td align=""center"" valign=""top"" width=""192"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""192"" height=""100%"">" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""middle"" width=""192"" height=""154""><a href=""/p-" & RSextra("itemid") & "-" & formatSEO(RSextra("itemDesc")) & ".asp""><img src=""/productpics/thumb/" & RSextra("itemPic") & """ border=""0"" alt=""" & altText & """></a></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""top""><a class=""product-description4"" href=""/p-" & RSextra("itemid") & "-" & formatSEO(RSextra("itemDesc")) & ".asp"" title=""" & altText & """>" & RSextra("itemDesc") & "</a></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""top"" height=""8""><img src=""/images/spacer.gif"" width=""1"" height=""8""></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""bottom""><span class=""boldText"">Our&nbsp;Price:&nbsp;</span><span class=""pricing-orange2"">" & formatCurrency(RSextra("price_Our")) & "</span><br><span class=""pricing-gray2"">List&nbsp;Price:&nbsp;<s>" & formatCurrency(RSextra("price_Retail")) & "</s></span><br><span class=""pricing-gray2"">You&nbsp;Save:&nbsp;" & formatCurrency(RSextra("price_Retail") - RSextra("price_Our")) & "</span></td></tr>" & vbcrlf
                                            response.write "</table></td>" & vbcrlf
                                            a = a + 1
                                            if a = 4 then
                                                response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""798"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
                                                a = 0
                                            else
                                                response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                                            end if
                                            RSextra.movenext
                                        loop
                                        RSextra.close
                                        set RSextra = nothing
                                    end if
                                end if
                                
                                DoNotDisplay = 0
                                if not isNull(RS("ItemKit_NEW")) then
                                    SQL = "SELECT inv_qty FROM we_Items WHERE itemID IN (" & RS("ItemKit_NEW") & ")"
                                    set RSkit = Server.CreateObject("ADODB.recordset")
                                    RSkit.open SQL, oConn, 3, 3
                                    do until RSkit.eof
                                        if RSkit("inv_qty") < 1 then DoNotDisplay = 1
                                        RSkit.movenext
                                    loop
                                    RSkit.close
                                    set RSkit = nothing
                                end if
                                if DoNotDisplay = 0 then
                                    altText = brandName & " " & modelName & " " & singularSEO(categoryName) & " � " & RS("itemDesc")
                                    if categoryid = "14" and modelid = "0" then altText = "Cell Phone Charms & Bling : " & altText
                                    response.write "<td align=""center"" valign=""top"" width=""192"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""192"" height=""100%"">" & vbcrlf
                                    response.write "<tr><td align=""center"" valign=""middle"" width=""192"" height=""154""><a href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp""><img src=""/productpics/thumb/" & RS("itemPic") & """ border=""0"" alt=""" & altText & """></a></td></tr>" & vbcrlf
                                    response.write "<tr><td align=""center"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
                                    response.write "<tr><td align=""center"" valign=""top""><a class=""product-description4"" href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"" title=""" & altText & """>" & RS("itemDesc") & "</a></td></tr>" & vbcrlf
                                    response.write "<tr><td align=""center"" valign=""top"" height=""8""><img src=""/images/spacer.gif"" width=""1"" height=""8""></td></tr>" & vbcrlf
                                    response.write "<tr><td align=""center"" valign=""bottom""><span class=""boldText"">Our&nbsp;Price:&nbsp;</span><span class=""pricing-orange2"">" & formatCurrency(RS("price_Our")) & "</span><br><span class=""pricing-gray2"">List&nbsp;Price:&nbsp;<s>" & formatCurrency(RS("price_Retail")) & "</s></span><br><span class=""pricing-gray2"">You&nbsp;Save:&nbsp;" & formatCurrency(RS("price_Retail") - RS("price_Our")) & "</span></td></tr>" & vbcrlf
                                    response.write "</table></td>" & vbcrlf
                                    a = a + 1
                                    if a < 4 then
                                        response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                                    end if
                                end if
                                
                                RS.movenext
                                
                                OLDModelID = holdModelID
                                if not RS.eof then holdModelID = RS("modelID")
                                if RS.eof or OLDModelID <> holdModelID then
                                    if a = 1 then
                                        response.write "<td width=""192"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""192"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""192"">&nbsp;</td>" & vbcrlf
                                    elseif a = 2 then
                                        response.write "<td width=""192"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""192"">&nbsp;</td>" & vbcrlf
                                    elseif a = 3 then
                                        response.write "<td width=""192"">&nbsp;</td>" & vbcrlf
                                    end if
                                    response.write "</tr>" & vbcrlf
                                    a = 0
                                end if
                            loop
                            %>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->