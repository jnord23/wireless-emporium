<script type="text/javascript" src="sortable.js"></script>
<table class="sortable">
  <tr>
    <th>Name</th><th>Salary</th><th>Extension</th><th>Start date</th>
    <th>Start date (American)</th>
  </tr>
  <tr><td>Bloggs, Fred</td><td>$12000.00</td>
      <td>1353</td><td>18/08/2003</td><td>08/18/2003</td></tr>
  <tr><td>Turvey, Kevin</td><td>$191200.00</td>
      <td>2342</td><td>02/05/1979</td><td>05/02/1979</td></tr>
  <tr><td>Mbogo, Arnold</td><td>$32010.12</td>
      <td>2755</td><td>09/08/1998</td><td>08/09/1998</td></tr>
  <tr><td>Shakespeare, Bill</td><td>$122000.00</td>
      <td>3211</td><td>12/11/1961</td><td>11/12/1961</td></tr>
  <tr><td>Shakespeare, Hamnet</td><td>$9000</td>
      <td>9005</td><td>01/01/2002</td><td>01/01/2002</td></tr>
  <tr><td>Fitz, Marvin</td><td>$3300</td>
      <td>5554</td><td>22/05/1995</td><td>05/22/1995</td></tr>
</table>

<table class="sortable">
  <thead>
  <tr><th>Name</th><th>Team</th>
  <th class="sorttable_nosort">Number of legs</th></tr>
  </thead>
  <tbody>
  <tr><td>Molby, Jan</td><td>Liverpool</td><td>2</td></tr>
  <tr><td>Hughes, Mark</td><td>Manchester Utd</td><td>2</td></tr>
  <tr><td>Nicol, Steve</td><td>Liverpool</td><td>2</td></tr>
  <tr><td>Ardiles, Ossie</td><td>Tottenham Hotspur</td><td>2</td></tr>
  <tr><td>Charlie Nicholas</td><td>Arsenal</td><td>2</td></tr>
  </tbody>
</table>