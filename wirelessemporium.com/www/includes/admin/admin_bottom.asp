
</body>
</html>
<div id="testZone"></div>
<div id="dumpZone" style="display:none;"></div>
<%
'BOTTOM INC
'if isObject(RS) then
'	if RS.state then
'		RS.close()
'		Set RS = nothing
'	end if
'end if

'if isObject(RS2) then
'	if RS2.state then
'		RS2.close()
'		Set RS2 = nothing
'	end if
'end if

'call addTracking(gblTrackingID, 0, "TRACKING END")

call CloseConn(oConn)
%>
<script language="javascript">
	var useHttps = <%=prepInt(securePage)%>;
	var winLoc = window.location.toString();
	winLoc = winLoc.toLowerCase();
	
	if (useHttps == 1 && winLoc.indexOf("https:") < 0) {
		window.location = winLoc.replace("http:","https:")
	}
	else if (useHttps == 0 && winLoc.indexOf("http:") < 0) {
		window.location = winLoc.replace("https:","http:")
	}
	
	function showCalendar(activeDate,displayBox) {
		if (displayBox == "sDateCalendar") { document.getElementById("eDateCalendar").innerHTML = "" }
		if (displayBox == "eDateCalendar") { document.getElementById("sDateCalendar").innerHTML = "" }
		ajax('/ajax/calendar.asp?activeDate=' + activeDate + '&displayBox=' + displayBox,displayBox)
	}
	
	function calendarDaySelect(displayBox,newDate,inputName) {
		document.getElementById(displayBox).innerHTML = ""
		eval("document.searchForm." + inputName + ".value = '" + newDate + "'")
	}
	
	function ajax(newURL,rLoc) {
		if (newURL.indexOf("?") > 0) {
			newURL = newURL + '&ajaxID=' + Math.floor(Math.random()*999999)
		}
		else {
			newURL = newURL + '?ajaxID=' + Math.floor(Math.random()*999999)
		}
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						alert("No data available")
					}
					else if (rVal == "refresh") {
						curLoc = window.location
						window.location = curLoc
					}
					else if(document.getElementById(rLoc)) {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				}
				else {
					document.getElementById("testZone").innerHTML = newURL
					alert("Error performing action")
				}
			}
		};
		httpRequest.send(null);
	}

	function ajaxPost(newURL, params, rLoc) {
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('POST', newURL, true);
		httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		httpRequest.setRequestHeader("Content-length", params.length);
		httpRequest.send(params);
				
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						alert("No data available")
					}
					else if (rVal == "refresh") {
						curLoc = window.location
						window.location = curLoc
					}
					else if(document.getElementById(rLoc)) {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				}
				else {
					document.getElementById("testZone").innerHTML = newURL
					alert("Error performing action")
				}
			}
		};
	}	
</script>