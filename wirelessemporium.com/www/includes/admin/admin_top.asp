<%
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"
securePage = 1
%>
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<%
dim pageTitle, header, SuperAdmin, skipLogin, Guest, GuestAllowed, strBody
dim myServer

call getDBConn(oConn)
oConn.CommandTimeout = 120

%>
<!--#include virtual="/includes/admin/extraFunctions.asp"-->
<%
session("adminID") = prepInt(session("adminID"))
if session("adminID") = 0 then
	session("adminID") = prepInt(request.Cookies("adminID"))
	session("username") = prepStr(request.Cookies("username"))
	session("fname") = prepStr(request.Cookies("fname"))
	session("lname") = prepStr(request.Cookies("lname"))
	session("adminUser") = "True"
	if session("adminID") > 0 then
		sql = "insert into we_adminLogin (adminID) values(" & session("adminID") & ")"
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
end if
adminID = prepInt(session("adminID"))
adminUser = prepStr(session("adminUser"))
curPage = lcase(request.ServerVariables("URL"))
myServer = request.servervariables("SERVER_NAME")
if pageTitle <> "WE Admin Login" and adminID = 0 then
	response.redirect "https://" & myServer & "/admin/"
end if

'ADMINSECURE
if pageTitle = "WE Admin Login" then
	if adminUser = "True" and adminID > 0 then
		sql = "select password, lastPassword, passwordDate from we_adminUsers where adminID = " & adminID
		session("errorSQL") = sql
		set adminRS = oConn.execute(sql)
		
		if not adminRS.EOF then
			curPassword = adminRS("password")
			lastPassword = adminRS("lastPassword")
			passwordDate = adminRS("passwordDate")
			
			if curPassword = lastPassword or passwordDate < dateadd("m",-3,date) then
				response.redirect "/admin/updatePassword.asp"
			else
				if Guest = true and GuestAllowed = "" then
					response.redirect "/db_update_links.asp"
				else
					response.redirect "/admin/menu.asp"
				end if
			end if
		end if
	end if
else
	if skipLogin <> 1 then
		if adminID = 0 then
			if inStr(myServer,"staging.") > 0 then
				response.redirect "http://staging.wirelessemporium.com/admin"
			else
				response.redirect "https://www.wirelessemporium.com/admin"
			end if
		else
			sql = "select password, lastPassword, passwordDate from we_adminUsers where adminID = " & adminID
			session("errorSQL") = sql
			set adminRS = oConn.execute(sql)
			
			if not adminRS.EOF then
				curPassword = adminRS("password")
				lastPassword = adminRS("lastPassword")
				passwordDate = adminRS("passwordDate")
				
				if curPassword = lastPassword or passwordDate < dateadd("m",-3,date) then
					response.redirect "/admin/updatePassword.asp"
				end if
			end if
		end if
	end if
end if

if request.ServerVariables("URL") = "/admin/default.asp" then
	pubLink = "http://www.wirelessemporium.com"
	logoLink = "http://www.wirelessemporium.com"
else
	pubLink = "/"
	logoLink = "/admin/menu.asp"
end if
%>
<!DOCTYPE html>
<html>
<head>
<title><%="WE ADMIN :: " & pageTitle%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/includes/css/style.css?v=20120731" rel="stylesheet" type="text/css">
<meta name="robots" content="noindex, nofollow">
<!--#include virtual="/includes/admin/inc_adminFunc.asp"-->
</head>

<%
if strBody <> "" then
	response.write strBody & vbcrlf
elseif pageTitle = "WE Admin Login" then
	response.write "<body onLoad=""document.login.userid.focus();"">" & vbcrlf
else
	response.write "<body>" & vbcrlf
end if

'Holiday Logo Code
if month(date) = 10 then
	useLogo = "holidayLogos/halloween_admin.png"
else
	useLogo = "WElogo.gif"
end if
%>
<div id="popCover" style="height:100%; display:none; width:100%; background-color:#999; position:fixed; left:0px; top:0px; z-index:100; opacity:0.5; filter:alpha(opacity=50);">&nbsp;</div>
<div id="popBox" style="height:600px; display:none; width:100%; position:fixed; left:0px; top:0px; z-index:101; text-align:center;"></div>
<%if header = 1 then%>
	<table width="85%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td width="120">
				<font face="Verdana, Arial, Helvetica, sans-serif" size="2">
					<a href="<%=logoLink%>"><img src="/images/<%=useLogo%>" vspace="15" border="0" title="my adminID: <%=adminID%>"></a>
				</font>
			</td>
			<td valign="bottom" align="right">
				<font face="Verdana, Arial, Helvetica, sans-serif" size="2">
					<b>:: // Welcome to WE.com Admin Pages<br></b>
				</font>
				<hr noshade width="350" size="1" align="right">
				<font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#FF6600">
					<%
					if pageTitle = "WE Admin Login" then
						%>
						Please Login
						<%
					else
						%>
						<a href="/admin/signout.asp">Logout</a>
						<%
					end if
					%>
				</font>
				<br><br>
			</td>
		</tr>
        <tr><td align="left"><a href="<%=pubLink%>">Return to Public Site</a></td></tr>
	</table>
<%end if%>