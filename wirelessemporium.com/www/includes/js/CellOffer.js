//externalized [knguyen/20110517]
function verifyCellOffer(mobileNumber,elmt) {
	for (i=0;i<mobileNumber.length;i++) {
		if(isNaN(mobileNumber.substring(i,i+1))) {
			mobileNumber = mobileNumber.replace(mobileNumber.substring(i,i+1),""); i=-1
		}
		else if (mobileNumber.substring(i,i+1) == " ") {
			mobileNumber = mobileNumber.replace(mobileNumber.substring(i,i+1),""); i=-1
		}
	}
	if (mobileNumber.length < 10) {
		alert("You must enter a valid number\n(123)123-1234")
		elmt.select()
	}
	else {
		closeCellOfferWindow(document.cellOffersForm.myPhoneNumber,document.getElementById('enterCellNumber'))
		ajax('/ajax/saveMobileNumber.asp?mobileNumber=' + mobileNumber,'dumpZone')
		setTimeout("chkReturnValue()",500)
	}
	return false
}

function chkReturnValue() {
	if (document.getElementById("dumpZone").innerHTML == "mobile number saved") {
		alert("Mobile number saved")
	}
	else if (document.getElementById("dumpZone").innerHTML != "") {
		alert("ERROR: Mobile number already in system")
	}
	else {
		setTimeout("chkReturnValue()",500)
	}
}

function cellPhoneFocusValue(elmt,val) {
	if (val=="Enter Cellphone Number") { elmt.value = "" }
}

function closeCellOfferWindow(val1, val2) {
	val1.value = "Enter Cellphone Number"
	val2.style.display = "none"
}