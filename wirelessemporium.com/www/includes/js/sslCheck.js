var winLoc = window.location.toString();
winLoc = winLoc.toLowerCase();

if ((winLoc.indexOf("staging.") < 0)&&(winLoc.indexOf("mdev.") < 0)) {
	if (useHttps == 1 && winLoc.indexOf("https:") < 0) {
		window.location = winLoc.replace("http:","https:")
	}
	else if (useHttps == 0 && winLoc.indexOf("http:") < 0) {
		window.location = winLoc.replace("https:","http:")
	}
}