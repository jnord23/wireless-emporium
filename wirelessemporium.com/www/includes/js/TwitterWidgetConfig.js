new TWTR.Widget({
  version: 2,
  type: 'profile',
  rpp: 2,
  interval: 6000,
  width: 200,
  height: 185,
  theme: {
    shell: {
      background: '#000000',
      color: '#f57608'
    },
    tweets: {
      background: '#ffffff',
      color: '#000000',
      links: '#f57608'
    }
  },
  features: {
    scrollbar: false,
    loop: false,
    live: false,
    hashtags: true,
   timestamp: true,
    avatars: true,
    behavior: 'all'
  }
}).render().setUser('WirelessEmp').start();