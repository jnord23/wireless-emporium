ajax('/ajax/compatibility.asp?itemID=' + itemID + '&musicSkins=' + musicSkins,'compBody')

function closeFloatingBox() {
	if (document.getElementById('prodDescription') != null) document.getElementById('prodDescription').style.display = '';

	//------------------ change it to original -------------------//
	document.getElementById("popBox").style.position = "fixed";
	document.getElementById("popCover").onclick = function () {}
	document.getElementById("popBox").onclick = function () {}
	//------------------------------------------------------------//
			
	document.getElementById("popCover").style.display = "none";
	document.getElementById("popBox").style.display = "none";

	document.getElementById("popCover").innerHTML = "";
	document.getElementById("popBox").innerHTML = "";
}

function showScreenProtectorInfo() {
	viewPortW = $(window).width();
	window.scrollTo(0, 0);
	if (document.getElementById('prodDescription') != null) document.getElementById('prodDescription').style.display = 'none';	
	ajax("/ajax/ajaxScreenProtectorInfographic.asp?screenW="+viewPortW,'popBox');
	document.getElementById("popBox").style.position = "absolute";
	document.getElementById("popCover").onclick = function () { closeFloatingBox() }
	document.getElementById("popBox").onclick = function () { closeFloatingBox() }
	
	document.getElementById("popCover").style.display = "";
	document.getElementById("popBox").style.display = "";
}

function showFloatingZoomImage() {
	viewPortH = $(window).height(); //get browser's viewport height.
	itemid = document.frmSubmit.prodid.value;
	ajax("/ajax/ajaxZoomImage.asp?itemid=" + itemid + "&screenH="+viewPortH,'popBox');
	document.getElementById("popCover").style.display = "";
	document.getElementById("popBox").style.display = "";
}

function showBaseFloatingZoomImage(img) {
	viewPortH = $(window).height(); //get browser's viewport height.
	itemid = document.frmSubmit.prodid.value;
	ajax("/ajax/ajaxZoomImage.asp?itemid=" + itemid + "&useImg=" + img + "&screenH="+viewPortH,'popBox');
	document.getElementById("popCover").style.display = "";
	document.getElementById("popBox").style.display = "";
}

function showFloatingShipping() {
	viewPortH = $(window).height(); //get browser's viewport height.
	ajax("/ajax/ajaxZoomImage.asp?shipping=1&screenH="+viewPortH,'popBox');
	document.getElementById("popCover").style.display = "";
	document.getElementById("popBox").style.display = "";
}

function getNewShipping() {
	var zipcode = document.myZip.zip.value
	ajax('/ajax/shippingDetails.asp?zip=' + zipcode + '&preview=1&weight=1&shiptype=999&nTotalQuantity=1&nSubTotal=7.99&useFunction=upscode2','myShippingOptions')
}

function show500Help() {
	ajax("/ajax/ajax500.asp",'popBox');
	document.getElementById("popCover").style.display = "";
	document.getElementById("popBox").style.display = "";
}	

function fnSwapZoomImage(imgSrc) {
	document.getElementById('id_img_zoom').src = imgSrc;
}

function showRecaptcha() { 
	 Recaptcha.create(recaptcha_public_key, 'div_recaptcha', {theme: "clean"});
}


setTimeout("showRecaptcha()",500)

if (numAvailColors > 1) {
	setTimeout("ajax('/ajax/ajaxSwapProductColor3.asp?uPage=p&uType=pre&itemid=" + itemID + "', 'preloading_deck')",500)
	
	function preloading(pItemID, selectedColorID){}
	function changeProduct(pItemID, selectedColorID, numColors, selectedIDX, onSale)
	{
		document.getElementById('imgLarge-location-pool').innerHTML = '';
		for(i=0; i < numColors; i++) 
		{
			if (document.getElementById('div_colorOuterBox_' + pItemID + '_' + i) != null) document.getElementById('div_colorOuterBox_' + pItemID + '_' + i).className = 'colorBoxUnselected';
			if (document.getElementById('id_colorText_' + i) != null) document.getElementById('id_colorText_' + i).style.display = 'none';
		}
		document.getElementById('div_colorOuterBox_' + pItemID + '_' + selectedIDX).className = 'colorBoxSelected';
		document.getElementById('id_colorText_' + selectedIDX).style.display = 'inline-block';
	
		if (document.getElementById('prodDetails1') != null) document.getElementById('prodDetails1').innerHTML = document.getElementById('new_logdesc_' + selectedIDX).innerHTML;
		if ((document.getElementById('prodDescription') != null) && (document.getElementById('new_logdesc_' + selectedIDX) != null)) 	document.getElementById('prodDescription').innerHTML = document.getElementById('new_logdesc_' + selectedIDX).innerHTML;
	
		document.getElementById('imgLarge-location-pool').innerHTML = '';
		ajax('/ajax/ajaxSwapProductColor3.asp?uPage=p&uType=img&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&onsale=' + onSale + '&hasflash=' + hasFlashFile, 'imgLarge-location-pool');
		ajax('/ajax/ajaxSwapProductColor3.asp?uPage=p&uType=altimg&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&testAB=' + testAB, 'altviewInner');
		ajax('/ajax/ajaxSwapProductColor3.asp?uPage=p&uType=title&itemid=' + pItemID + '&selectedColorID=' + selectedColorID, 'id_h1');
		ajax('/ajax/ajaxSwapProductColor3.asp?uPage=p&uType=partnumber&itemid=' + pItemID + '&selectedColorID=' + selectedColorID, 'part_item_number');
		setTimeout('checkUpdate()', 300);
	}
	
	function checkUpdate()
	{
		if ("" == document.getElementById('imgLarge-location-pool').innerHTML) {
			setTimeout('checkUpdate()', 50);
		}
		else 
		{
			document.getElementById('imgLarge-location').innerHTML = document.getElementById('imgLarge-location-pool').innerHTML;
			document.frmSubmit.prodid.value = document.getElementById('new_itemid').innerHTML;
		}
	}
}

function fnPreviewImage(imgSrc) {
	//var objToDisplay = '<img src="' + imgSrc + '" border="0" width="300" />';
	//document.getElementById('imgLarge-location').innerHTML = objToDisplay;
	document.getElementById("imgLarge").src = imgSrc;
}
function fnDefaultImage() {
	document.getElementById('imgLarge-location').innerHTML = document.getElementById('imgLarge-location-pool').innerHTML;
}

var commentLoop = 0
var commentReturn
var commentValue = '';

function changeProdTab(num) {
	for (i=0;i<=5;i++) {
		if (document.getElementById("prodTabLink" + i) != null) {
			document.getElementById("prodDetails" + i).style.display = 'none';
			document.getElementById("prodTabLink" + i).className = '';				
		}
	}
	document.getElementById("prodDetails" + num).style.display = '';
	document.getElementById("prodTabLink" + num).className = 'selected';
}

function Open_Popup(theURL,winName,features) {
	window.open(theURL,winName,features);
}

function checkall() {	 
  var GreForm = this.document.frmComment;
	
	if (GreForm.textarea1.value=="") {
		alert("Enter Your Comments");
		GreForm.textarea1.focus();
		return false;
	}
	
	ajax('/ajax/saveCommentCard.asp?itemID=' + itemID + '&email=' + document.frmComment.email.value + '&phone=' + document.frmComment.phone.value + '&comment=' + document.frmComment.textarea1.value + '&re_challenge=' + Recaptcha.get_challenge() + '&re_response=' + Recaptcha.get_response() + '&re_privatekey=' + recaptcha_private_key,'dumpZone');
	commentValue = GreForm.textarea1.value;
	GreForm.textarea1.value = "";
	
	setTimeout("chkCommentReturn()",500)
	
	return true;
}

function chkCommentReturn() {
	commentLoop++
	if (commentLoop < 5) {
		if (document.getElementById("dumpZone").innerHTML != "") {
			if (document.getElementById("dumpZone").innerHTML == "wrong recaptcha") {
				document.getElementById('textarea1').value = commentValue;
				alert('You have entered the wrong secret words.');
			}
			else if (document.getElementById("dumpZone").innerHTML == "commentGood") {
				alert("Comment saved")
			}
			else {
				alert("Error saving comment\nPlease resubmit\n" + document.getElementById("dumpZone").innerHTML)
				document.getElementById('textarea1').value = commentValue
			}
		}
	}
	else {
		alert("Error saving comment\nPlease resubmit")
		document.getElementById('textarea1').value = commentValue
	}
}

setTimeout("otherStyle()",2000)

function otherStyle() {
	ajax('/ajax/ajaxProduct.asp?partNumber=' + partNumber + '&modelID=' + modelID + '&itemID=' + itemID + '&typeID=' + typeID + '&HandsfreeType=' + handsFreeType,'otherStyles')
}

function onDetail(idx) {
	if (idx == 1) { 
		document.getElementById('detailHeader_2').className = 'middle';
		document.getElementById('detailHeader_3').className = 'last';
		document.getElementById('detail_2').style.display = 'none';
		document.getElementById('detail_3').style.display = 'none';
	} else if (idx == 2) {
		document.getElementById('detailHeader_1').className = 'first';
		document.getElementById('detailHeader_3').className = 'last';					
		document.getElementById('detail_1').style.display = 'none';
		document.getElementById('detail_3').style.display = 'none';			
	} else if (idx == 3) {
		document.getElementById('detailHeader_1').className = 'first';
		document.getElementById('detailHeader_2').className = 'middle';
		document.getElementById('detail_1').style.display = 'none';
		document.getElementById('detail_2').style.display = 'none';
	}
	document.getElementById('detailHeader_'+idx).className = 'selected';
	document.getElementById('detail_'+idx).style.display = '';
}

function recScroll(num) {
	if (num == 1) {
		recScrollVal = recScrollVal + 150;
		if (recScrollVal > maxRecScroll) { recScrollVal = maxRecScroll; }
		document.getElementById("recScrollBox").scrollLeft = recScrollVal;
	}
	else {
		recScrollVal = recScrollVal - 150;
		if (recScrollVal < 0) { recScrollVal = 0; }
		document.getElementById("recScrollBox").scrollLeft = recScrollVal;
	}
}

function rvScroll(num) {
	if (num == 1) {
		rvScrollVal = rvScrollVal + 150;
		if (rvScrollVal > maxRvScroll) { rvScrollVal = maxRvScroll; }
		document.getElementById("rvScrollBox").scrollLeft = rvScrollVal;
	}
	else {
		rvScrollVal = rvScrollVal - 150;
		if (rvScrollVal < 0) { rvScrollVal = 0; }
		document.getElementById("rvScrollBox").scrollLeft = rvScrollVal;
	}
}

function rollProducts(which) {
	if (recScrollNum == 1) {
		if (recScrollVal < maxRecScroll) {
			recScrollVal = recScrollVal + 20;
		}
		if (recScrollVal > maxRecScroll) { recScrollVal = maxRecScroll }
		document.getElementById(which).scrollLeft = recScrollVal;
	}
	else {
		if (recScrollVal > 9) {
			recScrollVal = recScrollVal - 20;
		}
		document.getElementById(which).scrollLeft = recScrollVal;
	}
	if (recScrollActive == 1) { setTimeout("rollProducts('" + which + "')",50); }
}

function changeTab(whichTab,whichDetail) {
    document.getElementById("overviewTab").className = "inactiveDetailTab";
    document.getElementById("compatTab").className = "inactiveDetailTab";
    document.getElementById("reviewTab").className = "inactiveDetailTab";
    document.getElementById("qaTab").className = "inactiveDetailTab";

    document.getElementById("prodDetails").className = "tabContentInactive";
    document.getElementById("compatDetails").className = "tabContentInactive";
    document.getElementById("reviewDetails").className = "tabContentInactive";
    document.getElementById("qaDetails").className = "tabContentInactive";

    document.getElementById(whichTab).className = "activeDetailTab";
    document.getElementById(whichDetail).className = "tabContent";
}

function toggleAnswer(questionID, numAnswers) {
    for (i = 0; i <= numAnswers; i++) {
        if (document.getElementById("ans_" + questionID + "_" + i) != null) {
            toggle("ans_" + questionID + "_" + i);
        }
    }
    if (document.getElementById(questionID + "_id_viewAns").innerHTML == "View all answers")
        document.getElementById(questionID + "_id_viewAns").innerHTML = "Hide answers";
    else
        document.getElementById(questionID + "_id_viewAns").innerHTML = "View all answers";
}