// JavaScript Document
function nxt_js_loader(script_array, position, is_sequential, final_process) {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = script_array[position];

	if (is_sequential != true) {
		if (position+1 < script_array.length) {
			nxt_js_loader(script_array, position+1, false, null);
		}
	}
	else {
		if (position+1 < script_array.length) {
			if (typeof script.addEventListener === 'function') {
				script.addEventListener('load', function () {
					nxt_js_loader(script_array, position+1, is_sequential, final_process);
				}, false);
			}
			else {
				script.onreadystatechange = function() {
					if (script.readyState in {loaded: 1, complete: 1}) {
						script.onreadystatechange = null;
						nxt_js_loader(script_array, position+1, is_sequential, final_process);
					}
				}
			}
		}
		else if (typeof final_process === 'function') {
			if (typeof script.addEventListener === 'function') {
				script.addEventListener('load', function () {
					final_process();
				}, false);
			}
			else {
				script.onreadystatechange = function() {
					if (script.readyState in {loaded: 1, complete: 1}) {
						script.onreadystatechange = null;
						final_process();
					}
				}
			}
		}
	}
	document.getElementsByTagName('head')[0].appendChild(script);
}

function load_nxt_autocomplete(selector) {
	nxt_js_loader(['//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js','//vector.nextopiasoftware.com/nxt-ac-js-ui-3.1-min.js'],0,	true,function() {
		var cssFiles = ['//vector.nextopiasoftware.com/ac-3.0-theme4-min.css','//nxtcfm.s3.amazonaws.com/' + nxtOptions.client_id + '-ac.css'];
		for (i = 0; i < cssFiles.length; i++) {
			var icssFiles = document.createElement('link');
			icssFiles.type = 'text/css';
			icssFiles.rel = 'stylesheet';
			icssFiles.media = 'screen';
			icssFiles.href = cssFiles[i];
			document.getElementsByTagName('head')[0].appendChild(icssFiles);
		}
		var jnxtac = jQuery.noConflict(true);

		jnxtac.ui.nxt_autocomplete.prototype._setPosition = function () {
			this.ac_main_container.position({
				my: "left top",		// position the top left of my autocomplete dropdown (valid values: top, center, bottom, left, right)
				at: "left bottom",	// to the bottom left of 
				of: this.element,	// my search box the autocomplete is being attached to
				offset: "-36 5",	// adding 50 to the left positioning and 50 to the top positioning of the dropdown
				collision: "none"	// with no collision detection
			});
			this.ac_main_container.css("z-index","6999");
		};

		if (selector == "") selector = ".searchInputBig";
		jnxtac(selector).nxt_autocomplete({cid:nxtOptions.client_id});
	});
}

function load_nxt_searchresult() {
	nxt_js_loader(['//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js','//nxtcfm.s3.amazonaws.com/' + nxtOptions.client_id + '.js'],0,true,function() {
		jqNxt = jQuery.noConflict(true);
//		alert('nxtOptions.cfm_match:' + nxtOptions.cfm_match + '\r\nwindow.location.href:' + window.location.href + '\r\nnxt_check_for_js_cfm(window.location.href):' + nxt_check_for_js_cfm(window.location.href));
//		alert('nxtOptions.cfm_match:' + nxtOptions.cfm_match);
		if ((nxtOptions.cfm_match = nxt_check_for_js_cfm(window.location.href)) !== false) {
			var cssFiles = ['//d2brc35ftammet.cloudfront.net/css/jquery-ui-1.8.16.css',nxtOptions.linkPrefix + 'nxt_styles.css'];
			for (i = 0; i < cssFiles.length; i++) {
				var icssFiles = document.createElement('link');
				icssFiles.type = 'text/css';
				icssFiles.rel = 'stylesheet';
				icssFiles.media = 'screen';
				icssFiles.href = cssFiles[i];
				document.getElementsByTagName('head')[0].appendChild(icssFiles);
			}
			nxt_js_loader(['//d2brc35ftammet.cloudfront.net/js/jquery.address-1.4.min.js','//d2brc35ftammet.cloudfront.net/js/jquery.ui.widget-1.8.16-min.js','//d2brc35ftammet.cloudfront.net/js/jquery.ui.mouse-1.8.16-min.js','//d2brc35ftammet.cloudfront.net/js/jquery.ui.slider-1.8.16-min.js','//d2brc35ftammet.cloudfront.net/js/nxt_js-1.1-min.js','//d2brc35ftammet.cloudfront.net/js/init-1.4.1-min.js'],0,true,fnNxtSearchCallback);
		}
	});
}

window.onload = function() {
	try{
		if (gblOrderID > 0) {
			setValue('dumpZone', '');
			ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=pullorder&siteid=' + gblSiteID + '&adminID=' + gblAdminID + '&orderid=' + gblOrderID,'dumpZone');
			checkUpdate('pullorder');
		} else {
			getCart();
		}
		
		if (gblEmail != "") getCustomerData(gblEmail);

		$("input[type=text]").keypress(function(e){
			if ( e.which == 13 ) e.preventDefault();
		});
	} catch (e) {
		alert(e.message);
	};
}

fnNxtSearchCallback = function() {
	hideAutocompleteBox();
	checkUpdate("nxtResult");
}

function nxtLoadingComplete() {
	hideAutocompleteBox();
	nxtBtnTimer = setTimeout('nxtButtonWatch()', 1000);
}

function nxtButtonWatch() {
	if (gblSiteID == 2) {
		$('.nxt_addtocart').each(function(index) {
			if (!jQuery.hasData(this)) {
				$(this).bind( "click", function(e) {
					e.preventDefault();
					addItemToCart($(this).parent().find('[name^="prodid"]').val(),1,'','');
					return false;
				});
			}
		});		
	} else {
		$('.nxt_addtocart').each(function(index) {
			var onclickString = $(this).attr("onclick").toString();
			if (onclickString.indexOf("add('") != -1) {
				var itemid = onclickString.substring(onclickString.indexOf("add('")+5);
				itemid = itemid.substring(0, itemid.indexOf("'"));
	
				var $parent = $(this).parent();
				$(this).remove();
				var btnAddItem = "<input type='button' class='prod-call-to-action nxt_addtocart' onclick=\"addItemToCart(" + itemid + ",1,'','')\" value='Add to Cart'>";
				$parent.html(btnAddItem);
			}
		});		
	}
	
	clearTimeout(nxtBtnTimer);
	nxtBtnTimer = setTimeout('nxtButtonWatch()', 1000);
}

function onRdoSearch(cartSearchType) {
	if (cartSearchType == "o") {
		$('#cartSearchBox').animate({left:"28%"}, 500, function(){});
	} else if (cartSearchType == "c") {
		$('#cartSearchBox').animate({left:"65%"}, 500, function(){});
	}
}

function hideAutocompleteBox() {
	if ($('.nxt-ac-main').css("display") != "none") $('.nxt-ac-main').css("display","none");
}

function getCustomerData(email) {
	gblAccountID = 0;
	gblParentAccID = 0;
	setValue("id_customerdata","");
	setValue("id_orderhistory", '<div style="width:100%; text-align:center; padding:50px 0px 50px 0px;"><img src="//d2brc35ftammet.cloudfront.net/img/loading-grey.gif" /></div>');
	ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=orderhistory&email=' + email,'id_orderhistory');
	ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=customerdata&siteid=' + gblSiteID + '&email=' + email,'id_customerdata');
	checkUpdate('customerData');
}

function grabCart() {
	var id = document.frmPhoneOrder.txtDeclinedID.value;
	if (isNaN(id)||(id == "")) {
		alert("Please use valid cartID or orderID!");
		return false;
	}

	setValue('dumpZone', '');
	if (getSelectedRadioValue(document.frmPhoneOrder.rdoSearch) == "c") {
		ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=grabcart&siteid=' + gblSiteID + '&adminID=' + gblAdminID + '&cartSessionID='+id,'dumpZone');
	} else {
		ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=grabdeclinedorder&siteid=' + gblSiteID + '&adminID=' + gblAdminID + '&declinedOrderID='+id,'dumpZone');
	}
	checkUpdate('grabCart');
}

function getCityByZip() {
	zipcode = document.frmPhoneOrder.sZip.value;
	if (zipcode.length >= 5) {
		document.getElementById('id_return_sCity').innerHTML = 'Loading..';
		ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=getcitybyzip&sZip=' + zipcode,'id_return_sCity');
		document.getElementById('id_sCity').style.display = '';
	}
}

function populateSCityName(dataReturn) {
	var dataArray = dataReturn.split("##")		
	var city = dataArray[0];
	var state = dataArray[1];
	onField(document.frmPhoneOrder.sCity);
	document.frmPhoneOrder.sCity.value = city;
	document.frmPhoneOrder.sState.value = state;
	document.getElementById('id_sCity').style.display = 'none';
}
	
function getShipping() {
//		alert(document.frmPhoneOrder.nSubTotal);
	nSubTotal = document.frmPhoneOrder.nSubTotal.value;
	sState = document.frmPhoneOrder.sState.options[document.frmPhoneOrder.sState.selectedIndex].value;
//		alert("nSubTotal:" + nSubTotal + '\r\nsState:' + sState);
	var nCATax = CurrencyFormatted(nSubTotal * gblTaxMath);
	if (sState == "CA") {
		setValue("CAtaxMsg","(Additional " + gblTaxDisplay + "% Tax for CA)");
		setValue("CAtax","$" + nCATax);
		document.frmPhoneOrder.nCATax.value = nCATax;
	} else {
		setValue("CAtaxMsg","");
		setValue("CAtax","$0.00");
		document.frmPhoneOrder.nCATax.value = 0;
	}

	if (document.getElementById('id_cbShip') != null) gblCurShipID = document.frmPhoneOrder.cbShip.options[document.frmPhoneOrder.cbShip.selectedIndex].value;
	setValue('shippingMethods', '');
	sState = document.frmPhoneOrder.sState.value;
	sZip = document.frmPhoneOrder.sZip.value;
	ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=getShipping&siteid=' + gblSiteID + '&adminID=' + gblAdminID + '&sState=' + sState + '&sZip=' + sZip + '&curShipID=' + gblCurShipID,'shippingMethods');
	checkUpdate('getShipping');
}

function getCart() {
	if (document.getElementById('btnCoupon').innerHTML == "Remove") document.frmPhoneOrder.sPromoCode.value = "Promo Code";
	var sPromoCode = document.frmPhoneOrder.sPromoCode.value;	
	setValue('cartItems', '<div style="width:100%; text-align:center; padding:50px 0px 50px 0px;"><img src="//d2brc35ftammet.cloudfront.net/img/loading-grey.gif" /></div>');
	ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=cartitems&siteid=' + gblSiteID + '&adminID=' + gblAdminID + '&viewPortH='+getObjectHeight(window)+'&sPromoCode='+sPromoCode,'cartItems');
	checkUpdate('cartItems');
}

function addItemToCart(itemid,nQty,oQty,customPrice) {
	setValue('dumpZone', '');
	if (!isNaN(nQty)&&(nQty!="")) {
		var itemQty = nQty;
	} else {
		var itemQty = document.getElementById(oQty).value;
	}
	var promoItemID = getSelectedRadioValue(document.frmPhoneOrder.rdoPromo);
	ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=additem&siteid=' + gblSiteID + '&adminID=' + gblAdminID + '&addItemID='+itemid+'&addQty='+itemQty+'&addItemPrice='+customPrice+'&addPromoItemID='+promoItemID,'dumpZone');
	checkUpdate('addItem');
	$('#toast').fadeIn(400).delay(1000).fadeOut(400);
}

function updateItem(itemid) {
	setValue('dumpZone', '');
	itemQty = document.getElementById('id_curqty_'+itemid).value;
	ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=updateitem&siteid=' + gblSiteID + '&adminID=' + gblAdminID + '&addItemID='+itemid+'&addQty='+itemQty,'dumpZone');
	checkUpdate('updateItem');
}

function deleteItem(itemid) {
	setValue('dumpZone', '');
	ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=deleteitem&siteid=' + gblSiteID + '&adminID=' + gblAdminID + '&addItemID='+itemid,'dumpZone');
	checkUpdate('deleteItem');
}

function getRatingAvgStar(rating) {
	var nRating = rating;
	var strRatingImg = "<img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> <img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> <img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> <img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> <img src='/images/review/grayStar.gif' border='0' width='14' height='13' />";

	if (!isNaN(nRating)) {
		if ((parseFloat(nRating) > 0)||(parseFloat(nRating) <=5)) {
			strRatingImg = "";
			for (nCnt=1; nCnt<=5; nCnt++) {
				if (parseFloat(nRating) >= parseFloat(nCnt)) {
					strRatingImg = strRatingImg + "<img src='/images/review/greenStarFull.gif' border='0' width='14' height='13' /> ";
				} else if (parseFloat(nRating) >= parseFloat((nCnt - 1) + 0.1)) {
					strRatingImg = strRatingImg + "<img src='/images/review/greenStarHalf.gif' border='0' width='8' height='13' /> ";
				} else {
					strRatingImg = strRatingImg + "<img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> ";
				}
			}
		}
	}

	return strRatingImg;
}

function getObjectHeight(o) {
	var h;
	try {
		h = $(o).height();
	} catch (e) {
		h = 600;
	}
	return h;
}

function refreshMainStrands() {
	try {
		var strandsItemIDs = "";
		$(".cartLineItems").each(function(index){
			var itemid = $(this).val();
			if (gblFreeItemIDs.indexOf(itemid + ",") == -1) {
				if (strandsItemIDs == "") strandsItemIDs = itemid;
				else strandsItemIDs = strandsItemIDs + "_._" + itemid;
			}
		});

		document.getElementById("mainStrands").setAttribute('item',strandsItemIDs);
		if (numDynamicWidgetUsed[0] == 0) {
			SBS.Recs.setRenderer(strands_rendering, "misc_1");
			SBS.Worker.go(gblStrandsAPIID);
		} else {
			SBS.Recs.seeds['misc_1'] = undefined;
			SBS.Worker.update(['misc_1']);
		}

		numDynamicWidgetUsed[0]++;
	} catch (e) {
		alert('Error occurred!\r\nError Message:' + e.message);
	}
}

function getLightBox() {
	document.getElementById('popCover').style.display = '';	
	document.getElementById('popBox').style.display = '';
}

function closeLightBox() {
	document.getElementById('popCover').style.display = 'none';
	document.getElementById('popBox').style.display = 'none';
	document.getElementById('popBox').innerHTML = '';
	clearTimeout(nxtBtnTimer);
}

function searchItems() {
//		alert(viewPortH);
	ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=searchItems&siteid=' + gblSiteID + '&adminID=' + gblAdminID + '&viewPortH='+getObjectHeight(window),'popBox');
	checkUpdate('popBox');
}

function onField(o) {
	o.value = '';
	o.style.color = '#333';
	o.style.fontWeight = 'bold';
}

function doSearch(searchType,idx) {
	if (searchType == 1) {
		var brandid = document.getElementById('id_cbBrand').value;
		var modelid = document.getElementById('id_cbModel').value;
		var categoryid = document.getElementById('id_cbCategory').value;
		var brandName = document.getElementById('id_cbBrand').options[document.getElementById('id_cbBrand').selectedIndex].text;
		var modelName = document.getElementById('id_cbModel').options[document.getElementById('id_cbModel').selectedIndex].text;
		var categoryName = document.getElementById('id_cbCategory').options[document.getElementById('id_cbCategory').selectedIndex].text;
		
		if (idx == 1) {
			ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=onbrand&siteid=' + gblSiteID + '&adminID=' + gblAdminID + '&brandid='+brandid,'cbModelBox');
			document.getElementById('id_cbCategory').selectedIndex = 0;
		} else if (idx == 2) {
			document.getElementById('id_cbCategory').selectedIndex = 0;
		} else {
			if ((brandid>0)&&(modelid>0)&&(categoryid>0)) {
				setValue('searchResult', '');
				brandName = brandName.replace("(", "[[").replace(")", "]]");
				modelName = modelName.replace("(", "[[").replace(")", "]]");
				categoryName = categoryName.replace("(", "[[").replace(")", "]]");
				
				ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=strands&siteid=' + gblSiteID + '&adminID=' + gblAdminID + '&brandid='+brandid+'&modelid='+modelid+'&categoryid='+categoryid+'&brandName='+escape(brandName)+'&modelName='+escape(modelName)+'&categoryName='+escape(categoryName)+'&viewPortH='+getObjectHeight(window),'searchResult');
				checkUpdate('strands');
			}
		}
	} else if (searchType == 2) {
		document.getElementById('nxtSearchInput').value = document.getElementById('nxtSearchInput').value + " .";	// prevent redirection
		var searchKeyword = document.getElementById('nxtSearchInput').value;
		window.history.pushState("string", "Title", "phoneorder2.asp?search="+searchKeyword);
		setValue('searchResult', '');
		ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=nextopia&siteid=' + gblSiteID + '&adminID=' + gblAdminID + '&viewPortH='+getObjectHeight(window),'searchResult');			
		checkUpdate('nextopia');
	} else if (searchType == 3) {
		setValue('searchResult', '');
		ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=strands&siteid=' + gblSiteID + '&adminID=' + gblAdminID + '&searchItemID=' + idx + '&viewPortH='+getObjectHeight(window),'searchResult');
		checkUpdate('strands');
	}
}

function checkUpdate(checkType)
{
	if (checkType == "strands") {
		if ("" == getValue('searchResult')) setTimeout('checkUpdate(\'strands\')', 300);
		else {
			try{
				if (numDynamicWidgetUsed[1] > 0) {
					SBS.Recs.seeds['CTGY-1'] = undefined;
					SBS.Recs.seeds['CTGY-2'] = undefined;
					SBS.Recs.seeds['CTGY-3'] = undefined;
				} else {
					SBS.Recs.setRenderer(strands_rendering2, "CTGY-1");
					SBS.Recs.setRenderer(strands_rendering2, "CTGY-2");
					SBS.Recs.setRenderer(strands_rendering2, "CTGY-3");
				}
				SBS.Worker.update(['CTGY-1']);
				SBS.Worker.update(['CTGY-2']);
				SBS.Worker.update(['CTGY-3']);
				numDynamicWidgetUsed[1]++;
			} catch (e) {
				alert('Error occurred!\r\nError Message:' + e.message);
			};
		}
	} else if (checkType == "popBox") {
		if ("" == getValue('popBox')) setTimeout('checkUpdate(\'popBox\')', 300);
		else {
			getLightBox();
			load_nxt_autocomplete("#nxtSearchInput");
		}
	} else if (checkType == "nextopia") {
		if ("" == getValue('searchResult')) setTimeout('checkUpdate(\'nextopia\')', 300);
		else load_nxt_searchresult();
	} else if (checkType == "updateItem") {
		if ("" == getValue('dumpZone')) setTimeout('checkUpdate(\'updateItem\')', 300);
		else getCart();
	} else if (checkType == "deleteItem") {
		if ("" == getValue('dumpZone')) setTimeout('checkUpdate(\'deleteItem\')', 300);
		else getCart();
	} else if (checkType == "addItem") {
		if ("" == getValue('dumpZone')) setTimeout('checkUpdate(\'addItem\')', 300);
		else getCart();
	} else if (checkType == "cartItems") {
		if (getValue('cartItems').indexOf("//d2brc35ftammet.cloudfront.net/img/loading-grey.gif") >= 0) setTimeout('checkUpdate(\'cartItems\')', 300);
		else {
			copyDivHeight('id_cart','id_orderhistory');
			refreshMainStrands();
			getShipping();
			setBtnCoupon();
		}
	} else if (checkType == "customerData") {
		if ("" == getValue('id_customerdata')) setTimeout('checkUpdate(\'customerData\')', 300);
		else {
			displayCustomerData(getValue('id_customerdata'));
			getShipping();
		}
	} else if (checkType == "nxtResult") {
		if (($("#nxt-nrf").length == 0)&&($(".nxt-products").length == 0)) setTimeout('checkUpdate(\'nxtResult\')', 300);
		else nxtLoadingComplete();
	} else if (checkType == "getShipping") {
		if ("" == getValue('shippingMethods')) setTimeout('checkUpdate(\'getShipping\')', 300);
		else updateGrandTotal();
	} else if (checkType == "grabCart") {
		if ("" == getValue('dumpZone')) setTimeout('checkUpdate(\'grabCart\')', 300);
		else getCart();
	} else if (checkType == "pullorder") {
		if ("" == getValue('dumpZone')) setTimeout('checkUpdate(\'pullorder\')', 300);
		else getCart();
	} 
}

function setBtnCoupon() {
	if ((document.frmPhoneOrder.sPromoCode.value != "")&&(document.frmPhoneOrder.sPromoCode.value != "Promo Code")) {
		document.getElementById('btnCoupon').innerHTML = "Remove";
	} else {
		document.getElementById('btnCoupon').innerHTML = "Apply";
	}
}

function copyDivHeight(from,to) {
	$('#' + to).height($('#' + from).height()-20);
}

function strands_rendering2(rec_info) {
	var rec = rec_info.recommendations;
	var lap = 0;
	var content = "";

	lap = 0;
	for(i=0;i<=rec.length-1;i++){
		var itemid = rec[i]["itemId"];
		var itemPic = rec[i]["metadata"]["picture"];
		var price_retail = rec[i]["metadata"]["price"];
		var price = rec[i]["metadata"]["properties"]["saleprice"];
		var itemDesc = rec[i]["metadata"]["name"];
//			if (itemDesc.length > 50) itemDesc = itemDesc.substring(0,46) + '...';
		if (!isNaN(price_retail) && !isNaN(price)) {
			if (gblFreeItemIDs.indexOf(itemid + ",") == -1) {
				lap = lap + 1;
				content = content + "<div class='item fl' align='center' style='width:155px; padding:0px 0px 0px 5px;'>";
				content = content + "	<div class='padding5 roundBorder wrapper'><img src='" + itemPic.replace("http://", "https://") + "' border='0' width='100' /></div>";
				content = content + "	<div style='width:100%; padding-top:10px; font-size:13px; font-weight:bold; color:#333;' align='center' title='" + rec[i]["metadata"]["name"] + "'>" + itemDesc + "</div>";
				content = content + "	<div style='width:100%; padding-top:10px; font-size:12px; color:#333; font-weight:normal;' align='center'>" + itemid + "</div>";
				content = content + "	<div style='width:100%; padding-top:10px; font-size:13px; color:#888; font-weight:normal;' align='center'>Retail Price: <span style='text-decoration:line-through;'>$" + price_retail + "</span></div>";
				content = content + "	<div style='width:100%; padding-top:10px; font-size:13px; color:#333; font-weight:bold;' align='center'>Wholesale Price: <span style='color:#c00;'>$" + price + "</span></div>";
				content = content + "	<div style='width:100%; padding-top:10px;' align='center'>" + getRatingAvgStar(rec[i]["rating"]) + "</div>";
				content = content + "	<div style='width:100%; padding-top:10px;' align='center'>";
				content = content + "		<div class='btnAddToCart' onclick=\"addItemToCart('" + itemid + "',1,'',''); SBS.Tracking.onRecClick('" + itemid + "','" + rec_info.tpl + "','" + rec_info.rrq + "');\">Add To Cart</div>";
				content = content + "	</div>";
				content = content + "</div>";
				if (lap >= 6) break;
			}
		}
	}
	
	if (content == "") content = "<span style='font-weight:normal;'>No products found for this criteria</span>";
	
	if (rec_info.tpl == "CTGY-1") {
		$("#strandsResult1").html(content);
	} else if (rec_info.tpl == "CTGY-2") {
		$("#strandsResult2").html(content);
	} else if (rec_info.tpl == "CTGY-3") {
		$("#strandsResult3").html(content);
	}
}

function strands_rendering(rec_info) {
	var rec = rec_info.recommendations;
	var lap = 0;
	var content = "<div class='slide'>";
//	alert(JSON.stringify(rec_info));
	lap = 0;
	for(i=0;i<=rec.length-1;i++){
//			alert(JSON.stringify(rec[i]));
		var itemid = rec[i]["itemId"];
		var itemPic = rec[i]["metadata"]["picture"];
		var price_retail = rec[i]["metadata"]["price"];
		var price = rec[i]["metadata"]["properties"]["saleprice"];
		var itemDesc = rec[i]["metadata"]["name"];
		if (itemDesc.length > 70) itemDesc = itemDesc.substring(0,66) + '...';
		if (!isNaN(price_retail) && !isNaN(price)) {
			if (gblFreeItemIDs.indexOf(itemid + ",") == -1) {
				lap = lap + 1;
				content = content + "<div class='item fl' align='center' style='width:160px; padding:0px 0px 0px 5px;'>";
				content = content + "	<div class='padding5 roundBorder wrapper'><img src='" + itemPic.replace("http://", "https://") + "' border='0' width='100' /></div>";
				content = content + "	<div style='width:100%; padding-top:10px; font-size:13px; font-weight:bold; color:#333;' align='center' title='" + rec[i]["metadata"]["name"] + "'>" + itemDesc + "</div>";
				content = content + "	<div style='width:100%; padding-top:10px; font-size:13px; color:#333;' align='center'>ItemID: " + itemid + "</div>";
				content = content + "	<div style='width:100%; padding-top:10px;' align='center'><span style='color:#c00; font-weight:bold; font-size:16px;'>$" + price + "</span> &nbsp; <span style='text-decoration:line-through; font-size:13px;'>$" + price_retail + "</span></div>";
				content = content + "	<div style='width:100%; padding-top:10px;' align='center'>";
				content = content + "		<div class='fl' style='margin-left:20px;'><input id='itemStrands1_" + itemid + "' class='xxsField' type='text' value='1' /></div>";
				content = content + "		<div class='btn margin-h' onclick=\"addItemToCart('" + itemid + "','','itemStrands1_" + itemid + "',''); SBS.Tracking.onRecClick('" + itemid + "','" + rec_info.tpl + "','" + rec_info.rrq + "');\">Add</div>";
				content = content + "	</div>";
				content = content + "</div>";
			}
		}
		
		if ((lap > 0)&&((lap % 3) == 0)&&(rec.length != lap)) {
			content = content + "</div><div class='slide'>";
		}
	}
	content = content + "</div>";
	$("#mainStrands").html(content);
	if (numDynamicWidgetUsed[0] > 0) $(".pagination").remove();
	$('.slides1').slides({preload: true, preloadImage: '/images/preloading.gif', generatePagination: true, /*play: 4000, pause: 3000, hoverPause: true,*/ prev: 'arrow-left', next: 'arrow-right'});
}

function setValue(id,val) {
	var o = document.getElementById(id);
	if (o != null) o.innerHTML = val;
}

function getValue(id) {
	var o = document.getElementById(id);
	if (o != null) return o.innerHTML;
}	

function CurrencyFormatted(amount) {
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt(i * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}

function toggleWhich(o, oClicked) {
	if (oClicked.checked) {
		document.getElementById(o).style.display = '';
	} else {
		document.getElementById(o).style.display = 'none';
	}
}

function changeSite(siteid) {
	document.frmPhoneOrder.cbSite.value = siteid;
	document.frmPhoneOrder.submit();
}

function setBillAddr(setting) {
	if (setting) document.getElementById("billingAddress").style.display = '';
	else document.getElementById("billingAddress").style.display = 'none';
}

function doSubmit() {
	shippingDiscount = 0;
	/*
	if (gblCustomShipping >= 0) {
		origShippingPrice = CurrencyFormatted(document.getElementById('id_shiptype_'+gblCurShipID).value);
		shippingDiscount = CurrencyFormatted(origShippingPrice - gblCustomShipping);
	}
	*/
	
	var f = document.frmPhoneOrder;
	bValid = true;

	CheckValidNEW(f.email.value, "Email is a required field!");
	CheckValidNEW(f.fname.value, "Your First Name is a required field!");
	CheckValidNEW(f.lname.value, "Your Last Name is a required field!");
	CheckValidNEW(f.phonenumber.value, "Your Phone Number is a required field!");
	CheckValidNEW(f.sAddress1.value, "Your Address (for Shipping Address) is a required field!");
	CheckValidNEW(f.sCity.value, "Your City (for Shipping Address) is a required field!");
	sString = f.sState.options[f.sState.selectedIndex].value;
	CheckValidNEW(sString, "State (for Shipping Address) is a required field!");
	CheckValidNEW(f.sZip.value, "Your Zip Code (for Shipping Address) is a required field!");
	if (f.sZip.value.length < 5) {
		alert("You must enter a valid US or Canadian postal code");
		bValid = false;
	}
	if (f.chkUseDiffAddress.checked == true) {
		CheckValidNEW(f.bAddress1.value, "Your Address (for Billing Address) is a required field!");
		CheckValidNEW(f.bCity.value, "Your City (for Billing Address) is a required field!");
		sString = f.bState.options[f.bState.selectedIndex].value;
		CheckValidNEW(sString, "State (for Billing Address) is a required field!");
		CheckValidNEW(f.bZip.value, "Your Zip Code (for Billing Address) is a required field!");
		if (f.bZip.value.length < 5) {
			alert("You must enter a valid US or Canadian postal code (for Billing Address).");
			bValid = false;
		}
	}
	/*
	sString = f.cc_cardType.options[f.cc_cardType.selectedIndex].value;
	CheckValidNEW(sString, "Credit Card Type is a required field!");
	*/
	CheckValidNEW(f.cc_cardOwner.value, "Your Name (as it appears on your credit card) is a required field!");
	CheckValidNEW(f.cardNum.value, "Your Credit Card Number is a required field!");
	CheckValidNEW(f.cc_month.value, "Your Credit Card Expiration Month is a required field!");
	CheckValidNEW(f.cc_year.value, "Your Credit Card Expiration Year is a required field!");
	CheckValidNEW(f.secCode.value, "Your Credit Card Security Code is a required field!");

	if (bValid) {
		document.frmPhoneOrder.accountid.value = gblAccountID;
		document.frmPhoneOrder.parentAcctID.value = gblParentAccID;
		document.frmPhoneOrder.nShippingDiscountTotal.value = shippingDiscount;
		document.frmPhoneOrder.action = '/cart/process/processingAdmin';
		return true;
//			document.frmPhoneOrder.submit();
	}

	return false;
}	

function updateGrandTotal() {
	shippingCost = 0;
	caTax = Number(document.frmPhoneOrder.nCATax.value);
	subTotal = Number(document.frmPhoneOrder.nSubTotal.value);
	discountTotal = Number(document.frmPhoneOrder.nDiscountTotal.value);
	if (document.getElementById('id_cbShip') != null) {
		gblCurShipID = document.frmPhoneOrder.cbShip.options[document.frmPhoneOrder.cbShip.selectedIndex].value;
		shippingCost = document.getElementById('id_shiptype_'+gblCurShipID).value;
		shiptypeDesc = document.getElementById('id_shiptypedesc_'+gblCurShipID).value;
		
		/*
		if ((gblCustomShipping >= 0)&&(gblCustomShipping <= shippingCost)) 
			shippingCost = gblCustomShipping;
		else
			gblCustomShipping = Number(-1);
		*/
			
		document.frmPhoneOrder.nShippingTotal.value = shippingCost;
		document.frmPhoneOrder.strShipType.value = shiptypeDesc;
		document.frmPhoneOrder.shiptypeID.value = gblCurShipID;
//			document.frmPhoneOrder.txtShippingTotal.value = CurrencyFormatted(shippingCost);
	}
	var GrandTotal = Number(subTotal) + Number(caTax) + Number(shippingCost) - Number(discountTotal);
	if (GrandTotal < 0) GrandTotal = 0;
	setValue("id_shipping","$" + CurrencyFormatted(shippingCost));
	setValue("id_discount","$" + CurrencyFormatted(discountTotal));	
	setValue("id_subtotal","$" + CurrencyFormatted(subTotal));
	setValue("id_grandtotal","$" + CurrencyFormatted(GrandTotal));
	
	if (document.frmPhoneOrder.couponcode.value != "") setValue("id_couponcode", "(" + document.frmPhoneOrder.couponcode.value + ")");
	else setValue("id_couponcode", "");
	
	document.frmPhoneOrder.nGrandTotal.value = CurrencyFormatted(GrandTotal);		
}


function displayCustomerData(dataReturn) {
	if (dataReturn != "No customer order history found" && dataReturn != "") {
		var dataArray = dataReturn.split("@@")
		onField(document.frmPhoneOrder.fname); document.frmPhoneOrder.fname.value = dataArray[0]; 
		onField(document.frmPhoneOrder.lname); document.frmPhoneOrder.lname.value = dataArray[1]; 
		onField(document.frmPhoneOrder.sAddress1); document.frmPhoneOrder.sAddress1.value = dataArray[2]; 
		onField(document.frmPhoneOrder.sAddress2); document.frmPhoneOrder.sAddress2.value = dataArray[3]; 
		onField(document.frmPhoneOrder.sCity); document.frmPhoneOrder.sCity.value = dataArray[4]; 
		onField(document.frmPhoneOrder.sState);
		var len_sState = document.frmPhoneOrder.sState.length; 
		for (i=0;i<len_sState;i++) {
			if (document.frmPhoneOrder.sState.options[i].value == dataArray[5]) {
				document.frmPhoneOrder.sState.selectedIndex = i;
			}
		}
		onField(document.frmPhoneOrder.sZip); document.frmPhoneOrder.sZip.value = dataArray[6]; 
		onField(document.frmPhoneOrder.phonenumber); document.frmPhoneOrder.phonenumber.value = dataArray[7]; 
		if (dataArray[2] != dataArray[8] || dataArray[6] != dataArray[12]) {
			document.frmPhoneOrder.chkUseDiffAddress.checked = true;
			setBillAddr(true);
			onField(document.frmPhoneOrder.bAddress1); document.frmPhoneOrder.bAddress1.value = dataArray[8]; 
			onField(document.frmPhoneOrder.bAddress2); document.frmPhoneOrder.bAddress2.value = dataArray[9]; 
			onField(document.frmPhoneOrder.bCity); document.frmPhoneOrder.bCity.value = dataArray[10]; 
			onField(document.frmPhoneOrder.bState);
			var len_bState = document.frmPhoneOrder.bState.length; 
			for (i=0;i<len_bState;i++) {
				if (document.frmPhoneOrder.bState.options[i].value == dataArray[11]) {
					document.frmPhoneOrder.bState.selectedIndex = i;
				}
			}
			onField(document.frmPhoneOrder.bZip); document.frmPhoneOrder.bZip.value = dataArray[12]; 
		} else {
			document.frmPhoneOrder.chkUseDiffAddress.checked = false;
			setBillAddr(false);
			document.frmPhoneOrder.bAddress1.value = "";
			document.frmPhoneOrder.bAddress2.value = "";
			document.frmPhoneOrder.bCity.value = "";
			document.frmPhoneOrder.bState.selectedIndex = 0;
			document.frmPhoneOrder.bZip.value = "";
		}
		gblAccountID = dataArray[13];
		gblParentAccID = dataArray[14];
	} else {
		document.frmPhoneOrder.fname.value = "";
		document.frmPhoneOrder.lname.value = "";
		document.frmPhoneOrder.sAddress1.value = "";
		document.frmPhoneOrder.sAddress2.value = "";
		document.frmPhoneOrder.sCity.value = "";
		document.frmPhoneOrder.sState.selectedIndex = 0;
		document.frmPhoneOrder.sZip.value = "";
		document.frmPhoneOrder.phonenumber.value = "";
		document.frmPhoneOrder.bState.selectedIndex = 0;
		document.frmPhoneOrder.chkUseDiffAddress.checked = false;
		setBillAddr(false);
		document.frmPhoneOrder.bAddress1.value = "";
		document.frmPhoneOrder.bAddress2.value = "";
		document.frmPhoneOrder.bCity.value = "";
		document.frmPhoneOrder.bState.selectedIndex = 0;
		document.frmPhoneOrder.bZip.value = "";
		gblAccountID = 0;
		gblParentAccID = 0;			
	}
}

function printinvoice(orderid,accountid) {

	var url="view_invoice.asp?accountid="+accountid+"&orderId="+orderid;
	window.open(url,"invoice","left=0,top=0,width=1024,height=768,locationbar=0,resizable=1,toolbar=0,scrollbars=1,menubar=1");
}

function pullOrder(pSiteID,orderid,email) {
	if (gblSiteID != pSiteID) {
		document.frmPhoneOrder.cbSite.value = pSiteID;
		document.frmPhoneOrder.hidOrderID.value = orderid;
		document.frmPhoneOrder.submit();
	} else {
		setValue('dumpZone', '');
		ajax('/ajax/admin/ajaxPhoneOrder2.asp?uType=pullorder&siteid=' + gblSiteID + '&adminID=' + gblAdminID + '&orderid='+orderid,'dumpZone');
		checkUpdate('pullorder');
	}
}


function getSelectedRadioValue(buttonGroup) {
	var i = getSelectedRadio(buttonGroup);
   if (i == -1) {
	  return "";
   } else {
	  if (buttonGroup[i]) { // Make sure the button group is an array (not just one button)
		 return buttonGroup[i].value;
	  } else { // The button group is just the one button, and it is checked
		 return buttonGroup.value;
	  }
   }
}

function getSelectedRadio(buttonGroup) {
   if (buttonGroup[0]) {
	  for (var i=0; i<buttonGroup.length; i++) {
		 if (buttonGroup[i].checked) {
			return i
		 }
	  }
   } else {
	  if (buttonGroup.checked) { return 0; } // if the one button is checked, return zero
   }
   return -1;
}
