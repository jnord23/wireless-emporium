	// Set default (on page load)
	//setTimeout("updateFilter()",1000)
	
	// Update visible content
	
	
	var paginator = "";
	
	function generatePagination(totalFiltered){
		perPage = 40;
		paginator = "";
		if(totalFiltered > 40) {
			paginator = "<div style=\"float:right;\"> | <a class=\"inactivePageNum\" onclick=\"showAllProducts()\">Show All Products</a></div>";
			paginator += "<div style=\"float:right;\">";
			paginator += "<div style=\"padding:2px 5px 0px 5px; float:left;\">View Page:</div>";
			for(i=1; i<=Math.ceil(totalFiltered/perPage); i++) {
				paginator += "<div id=\"pageLinkBox_" + i + "\" class=\"paginationLink1\"><a id=\"pageLink_" + i + "\" class=\"inactivePageNum\" onclick=\"showPage(" + i + ")\">" + i + "</a></div>";
			}
			paginator += "</div>";
		}
		
		document.getElementById("upperPagination").innerHTML = paginator;
		document.getElementById("lowerPagination").innerHTML = paginator;
	}
	
	
	function showAllProducts(){
		var totalShownItems = 0;
		var paginator = "";
		// Blank all current items out
		for (i=1;i<=totalItems;i++) {
			document.getElementById("itemListID_" + i).setAttribute("class", "bmc_productBox2")
		}
		
		for (j=0, i=0;j<totalItems;i++) {
			if (document.getElementById("itemListID_" + i) != undefined) {
				currentSubType = document.getElementById("subtypeid_" + i).value;
				if(selectedSubTypes.indexOf(currentSubType) > -1) {
					totalShownItems = totalShownItems + 1
					document.getElementById("itemListID_" + i).setAttribute("class", "bmc_productBox")
					curProduct = document.getElementById("picDiv_" + i).innerHTML
					document.getElementById("picDiv_" + i).innerHTML = curProduct.replace(/<tempimg/gi,'<img')
					j++
				}
			}

			if(i > totalItems) {
				break
			}
		}
		
		paginator += "<div style=\"float:right;\">";
		paginator += "<div style=\"padding:2px 5px 0px 5px; float:left;\">View Page:</div>";
		paginator += "<div id=\"pageLinkBox_1\" class=\"paginationLink1\"><a id=\"pageLink_1\" class=\"inactivePageNum\" href=\"#\">1</a></div>";
		paginator += "</div>";
		
		document.getElementById("upperPagination").innerHTML = paginator;
		document.getElementById("lowerPagination").innerHTML = paginator;
	}
	
	function showPage(pageNum) {
		if (pageNum == 0) { perPage = totalItems } else { perPage = 40 }
		// Blank all current items out
		for (i=1;i<=totalItems;i++) {
			document.getElementById("itemListID_" + i).setAttribute("class", "bmc_productBox2")
		}
		// Set pagination
		if (curPage == 0) { curPage = 1 }
		if (document.getElementById("pageLink_" + curPage) != undefined) { document.getElementById("pageLink_" + curPage).setAttribute("class", "inactivePageNum") }
		if (pageNum > 0) {
			if (document.getElementById("pageLink_" + pageNum) != undefined) { document.getElementById("pageLink_" + pageNum).setAttribute("class", "activePageNum") }
		}
		// Scroll to top
		//window.scroll(0,20)
		
		// Get items to show
		if (pageNum == 0) { nowShow = perPage } else { nowShow = pageNum * perPage }
		if (useFilter == "" && useFilter2 == "") {
			var totalShownItems = 0;
			
			document.getElementById("noProducts").setAttribute("class", "alertBoxHidden")
			
			for (j=0, i=0;j<totalItems;i++) {
				if (document.getElementById("itemListID_" + i) != undefined) {
					currentSubType = document.getElementById("subtypeid_" + i).value;
					if(selectedSubTypes.indexOf(currentSubType) > -1) {
						totalShownItems = totalShownItems + 1
						document.getElementById("itemListID_" + i).setAttribute("class", "bmc_productBox")
						curProduct = document.getElementById("picDiv_" + i).innerHTML
						document.getElementById("picDiv_" + i).innerHTML = curProduct.replace(/<tempimg/gi,'<img')
						j++
					}
				}
				
				if(i > totalItems) {
					break
				}
			}
			if(j < perPage) {
				totalPages = 1
			} else {
				totalPages = Math.ceil(j/perPage)
			}
			if (pageNum == 0) { 
				//updatePagination(1) 
			} else { 
				//updatePagination(totalPages) 
			}
			//alert("Total Shown Items: " + j + " Total Pages: " + totalItems);
		}
		else {
			filterItemCnt = 0
			if (pageNum == 0) {
				lastProductNum = totalItems
				firstProductNum = 0
			}
			else {
				lastProductNum = pageNum * perPage
				firstProductNum = lastProductNum - perPage
			}
			for (j=0, i=1;j<=totalItems;i++) {
				checkSub = document.getElementById("subType_" + i).innerHTML
				checkPrice = document.getElementById("price_" + i).innerHTML
				currentSubType = document.getElementById("subtypeid_" + i).value;
				if (checkPrice < 5) { checkPrice = 4 }
				if (checkPrice >= 5 && checkPrice < 10) { checkPrice = 5 }
				if (checkPrice >= 10 && checkPrice < 20) { checkPrice = 10 }
				if (checkPrice >= 20 && checkPrice < 30) { checkPrice = 20 }
				if (checkPrice >= 30) { checkPrice = 30 }
				if (useFilter != "") {
					if (useFilter.indexOf('##' + checkSub + '##') >= 0) {
						if(selectedSubTypes.indexOf(currentSubType) > -1) {
							// Found an item that matches the array
							if (useFilter2 == "" || useFilter2.indexOf('##' + checkPrice + '##') >= 0) {
								filterItemCnt++
								if (filterItemCnt > firstProductNum && filterItemCnt <= lastProductNum) {
									document.getElementById("itemListID_" + i).setAttribute("class", "bmc_productBox")
									curProduct = document.getElementById("picDiv_" + i).innerHTML
									document.getElementById("picDiv_" + i).innerHTML = curProduct.replace(/<tempimg/gi,'<img')
									j++
								}
							}
						}
					}
				}
				else {
					if (useFilter2.indexOf('##' + checkPrice + '##') >= 0) {
						if(selectedSubTypes.indexOf(currentSubType) > -1) {
							// Found an item that matches the array
							filterItemCnt++
							if (filterItemCnt > firstProductNum && filterItemCnt <= lastProductNum) {
								document.getElementById("itemListID_" + i).setAttribute("class", "bmc_productBox")
								curProduct = document.getElementById("picDiv_" + i).innerHTML
								document.getElementById("picDiv_" + i).innerHTML = curProduct.replace(/<tempimg/gi,'<img')
								j++
							}
						}
					}
				}
			}
			if (filterItemCnt == 0) {
				document.getElementById("noProducts").setAttribute("class", "alertBox")
			}
			else {
				document.getElementById("noProducts").setAttribute("class", "alertBoxHidden")
			}
			newPageTotal = Math.ceil(j/perPage)
			//updatePagination(newPageTotal)
			//alert("Total Shown Items: " + j + " Total Pages: " + totalPages);
		}
		// Update current page
		curPage = pageNum
		if(pageNum == 1) generatePagination(j);
		updateDisplay(curPage);
		
		
		
		$("#upperPagination .paginationLink1 a").each(function(){
			$(this).attr("class", "inactivePageNum");
		});
		
		$("#upperPagination #pageLink_"+pageNum).each(function(){
			$(this).attr("class", "activePageNum");
		});
		
		$("#lowerPagination .paginationLink1 a").each(function(){
			$(this).attr("class", "inactivePageNum");
		});
		
		$("#lowerPagination #pageLink_"+pageNum).each(function(){
			$(this).attr("class", "activePageNum");
		});
	}
	
	function updateDisplay(currentPage) {
		var products = document.getElementsByClassName("bmc_productBox");
		var total = products.length;
		var perPage = 40;
		start = perPage * (currentPage - 1);
		end = start + (perPage - 1);
		//alert("Start: " + start + " End: " + end);
		//alert(total);
		
		var hide = []; 
		
		for(i=0; i<total;i++){
			var current_id = document.getElementsByClassName("bmc_productBox")[i].id;
			if(i < start || i > end) {
				hide.push(current_id);
			}
		}
		
		for(k=0;k<hide.length;k++){
			document.getElementById(hide[k]).className = "bmc_productBox2";
		}
	}
	
	function updatePagination(newCnt) {
		if (parseInt(newCnt) < newCnt) { newCnt = parseInt(newCnt) + 1 }
		for(i=1;i<=totalPages;i++) {
			if (i > newCnt) {
				if (document.getElementById("pageLinkBox_" + i) != undefined) { document.getElementById("pageLinkBox_" + i).setAttribute("class", "paginationLink2") }
			}
			else {
				if (document.getElementById("pageLinkBox_" + i) != undefined) { document.getElementById("pageLinkBox_" + i).setAttribute("class", "paginationLink1") }
			}
		}
		if (newCnt > 0) {
			linkData = document.getElementById("lowerPagination").innerHTML.replace(/pageLink/g,'upperLink')
			linkData = linkData.replace(/pageLinkBox/g,'upperLinkBox')
			document.getElementById("upperPagination").innerHTML = linkData
		}
		else {
			document.getElementById("upperPagination").innerHTML = ""
		}
	}
	
	// Update filter settings
	function updateFilter() {
		useFilter = "##"
		useFilter2 = "##"
		for(i=0;i<document.filterForm.elements.length;i++) {
			if (document.filterForm.elements[i].checked == true) {
				if (document.filterForm.elements[i].name == "styleType") {
					useFilter = useFilter + document.filterForm.elements[i].value + '##'
				}
				else {
					useFilter2 = useFilter2 + document.filterForm.elements[i].value + '##'
				}
			}
		}
		if (useFilter == "##") { useFilter = "" }
		if (useFilter2 == "##") { useFilter2 = "" }
		//showPage(1)
	}
	
	function sortProduct(brandID,modelID,categoryID,sb) {
		document.getElementById("allProducts").innerHTML = "<div class='alertBox'>Sorting Products<br />Please Wait</div>"
		document.getElementById("testZone").innerHTML = '/ajax/bmcSortProducts.asp?brandID=' + brandID + '&modelID=' + modelID + '&categoryID=' + categoryID + '&sb=' + sb
		ajax('/ajax/bmcSortProducts.asp?brandID=' + brandID + '&modelID=' + modelID + '&categoryID=' + categoryID + '&sb=' + sb,'allProducts')
		setTimeout("delayUpdate()",300)
	}
	
	function delayUpdate() {
		if (document.getElementById("allProducts").innerHTML.indexOf("Sorting Products") > 0) {
			setTimeout("delayUpdate()",300)
		}
		else {
			setTimeout("updateFilter()",500)
		}
	}
	
	function subTypeFilter(dont_reset) {
		if(typeof dont_reset === 'undefined') {
			selectedSubTypes = new Array();
		}
		for(i=0, j=0; i<document.getElementsByName("subtypes[]").length;i++) {
			if(document.getElementsByName("subtypes[]")[i].checked) {
				selectedSubTypes[j] = document.getElementsByName("subtypes[]")[i].value;
				j++;
			}			
		}
		
		if(!selectedSubTypes.length) {
			for(i=0, j=0; i<document.getElementsByName("subtypes[]").length;i++) {
				selectedSubTypes[j] = document.getElementsByName("subtypes[]")[i].value;
				j++;
			}	
		}
		
		showPage(1);
	}
	
	//normally under updateFilter
	if(reset_selection) {
		subTypeFilter()
	} else {
		subTypeFilter(1);
	}
	
	showPage(1)
	