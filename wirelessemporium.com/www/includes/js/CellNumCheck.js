//note: ajax() is a dependency
function chkCellNum() {
	cellNum = document.enterCellNum.cellNumber.value
	for (i=0; i<cellNum.length; i++) {
		if (isNaN(cellNum.substring(i,i+1))) {
			cellNum = cellNum.replace(cellNum.substring(i,i+1),"")
		}
	}
	if (cellNum.length < 10) {
		alert("You must enter a valid 10 digit mobile number")
		document.enterCellNum.cellNumber.select()
	}
	else {
		document.getElementById('mobileNumber').style.display='none'
		ajax('/ajax/saveMobileNumber.asp?mobileNumber=' + cellNum,'dumpZone')
		setTimeout("alert(document.getElementById('dumpZone').innerHTML)",500)
	}
	return false
}

function chkCellNumCheckout(cellNum) {
	for (i=0; i<cellNum.length; i++) {
		if (isNaN(cellNum.substring(i,i+1))) {
			cellNum = cellNum.replace(cellNum.substring(i,i+1),"")
		}
	}
	if (cellNum.length < 10) {
		alert("You must enter a valid 10 digit mobile number")
		document.frmProcessOrder.cellNumber.select()
	}
	else {
		document.getElementById('exclusiveOfferCheckout').style.display='none'
		ajax('/ajax/saveMobileNumber.asp?mobileNumber=' + cellNum,'dumpZone')
		setTimeout("alert(document.getElementById('dumpZone').innerHTML)",500)
	}
}