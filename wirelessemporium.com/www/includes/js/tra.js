if (typeof TurnTo == 'undefined'){TurnTo = {};TurnTo.matchData={"site":{"siteName":"Wireless Emporium","gteaser":" ttLogo + ' <a class=\"TTloginLink\">See what your friends are buying at Wireless Emporium<\/a>'","floatTeaser":true,"ask":false,"ftid":30213}};}/*
 * Turnto TRA version: v2.0.2169
 * Build: /trunk.rev.2169
 * Date: Thu Mar 24 19:14:37 EDT 2011
 *
 * Copyright (c) 2010, TurnTo Networks, Inc.
 * All Rights Reserved.
 */
TurnToHTML={dog:'spike',titlebarLoggedOut:'Used TurnTo before? <a id="TTsignUpLink" class="TTlogoutLink" style="color:#000">Log in</a>',titlebarLoggedIn:'<span id="TTmatchesHi" class="TThi"></span><a id="TTlogoutLink" class="TTlogoutLink TTblue1" style="font-size:10px;float:left;margin-left:10px;margin-top:1px">Not <span class="TTfirstName"></span>?</a>',lessLink:'<a href="javascript:void(0);" style="font-size:9px;" class="TTlessLink">Less</a>',moreLessBlock:'<div class="TTmoreLess"></div>',forwardMatchesHeader:new Function("obj","var p=[];with(obj){p.push('<span class=\"TTsecHed\">',ffCount,'');if(flfCount > 0){p.push(' Other');}p.push(' friend');if(ffCount > 1){p.push('s');}p.push(' ', turnToSettings.verb ,'</span> <a href=\"javascript:void(0)\" id=\"forwardMatchesHeaderPreviewLn\" class=\"TTexternalLink\" style=\"font-size:10px; font-weight:bold; padding-left:5px;\">');if(ffCount == 1){p.push('(Who is it?)');}else{p.push('(Who are they?)');}p.push('</a> ');}return p.join('');"),preUserMessage:new Function("obj","var p=[];with(obj){p.push('<span class=\"TTsecHed\">Friends</span><div style=\"margin:10px 0; font-size:11px;\">Hi ', user.firstName ,' <a class=\"TTlogoutLink\" style=\"font-weight:bold; color:black;\">(Not <span class=\"TTfirstName\">',user.firstName,'</span>?)</a>. We still need a bit more info. <a id=\"TTpreUserMessageCont\" href=\"javascript:void(0)\" style=\"text-decoration:underline;\">Continue</a>.</div> ');}return p.join('');"),unConfirmedUserMessage:new Function("obj","var p=[];with(obj){p.push('<span class=\"TTsecHed\">Friends</span><div style=\"margin:10px 0; font-size:11px;\">Hi ', user.firstName ,' <a class=\"TTlogoutLink\" style=\"font-weight:bold; color:black;\">(Not <span class=\"TTfirstName\">',user.firstName,'</span>?)</a>. You still need to confirm your email address ',user.uIdents[0].ident ,'. Please check your inbox for a message from TurnTo, or <a id=\"TTpreUserResendLink\" class=\"TTexternalLink\" href=\"javascript:void(0)\" style=\"text-decoration:underline;\">click here</a> to resend it.</div> ');}return p.join('');"),additionalEmail:new Function("obj","var p=[];with(obj){p.push('<input type=\"text\" id=\"TTaddEmail_',i,'\" name=\"TTaddEmail_',i,'\" style=\"display:block\" ttInputLabelText=\"Email address\" value=\"Email address\" onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\" class=\"TTdefaultInput\"/>');}return p.join('');"),zipMatches:new Function("obj","var p=[];with(obj){p.push('<div><span class=\"TTsecHed\">', neighborCount ,' Neighbor', neighborCount==1?'':'s' ,' in ',ornear ,' zip ', zip,'</span> <a href=\"javascript:void(0)\" id=\"TTeditZip\" class=\"TTeditZippy\">(edit zip)</a> ', turnToSettings.verb ,':  <ul id=\"TT-zipMatch-row\"></ul></div>');}return p.join('');"),inviteEmailPreview:new Function("obj","var p=[];with(obj){p.push('<div style=\"margin-bottom:20px; padding:0 5px; font-size:10px;\">',preview,'</div><input type=\"submit\" id=\"TTInviteEmailPreviewBack\" class=\"TTsubmitButton\" style=\"background-color:#999;\" value=\"Back\">');}return p.join('');"),confirmationMessageScreen:new Function("obj","var p=[];with(obj){p.push('<div style=\"margin:20px 0; font-size:12px;\">',message,'</div><input type=\"submit\" id=\"confirmationMessageScreenBtn\" class=\"TTsubmitButton\" style=\"background-color:#999;\" value=\"Done\">');}return p.join('');"),moreLink:new Function("obj","var p=[];with(obj){p.push('<a href=\"javascript:void(0);\" style=\"font-size:9px;\" class=\"TTmoreLink\">', moreCount ,' more</a>');}return p.join('');"),newfbuser:new Function("obj","var p=[];with(obj){p.push('<div class=\"TT2yellowMessageLine1\" id=\"TTnewfbusermessage\"><img class=\"TT2messageSymbol\" src=\"',TurnTo.getStaticUrl(\"/tra2/images/blueexclaimmark.png\"),'\"/><a style=\"display:none\" href=\"javascript:void(0)\" id=\"TTnewfbuserdismiss\"><img src=\"', TurnTo.getStaticUrl(\"/tra2/images/subCloseButton.png\") ,'\" border=\"0\" /></a> Checking for ', params.count ,' facebook friends. To check for even more friends, <a href=\"javascript:void(0);\" id=\"TT2newfbuserclickhere\" style=\"text-decoration:underline;\">click here.</a><div style=\"clear:both\"></div></div>');}return p.join('');"),addemailmessage:new Function("obj","var p=[];with(obj){p.push('<div class=\"TT2yellowMessageLine1\" id=\"TT2addemailMessage\"\"><img class=\"TT2messageSymbol\" src=\"',TurnTo.getStaticUrl(\"/tra2/images/blueexclaimmark.png\"),'\"/>Enter an email address to check for purchases you made here in the past. <a href=\"javascript:void(0);\" id=\"TT2addemailclickhere\" style=\"text-decoration:underline;\">Click Here.</a><div style=\"clear:both\"></div></div>');}return p.join('');"),unconfirmedEmailMessage:new Function("obj","var p=[];with(obj){p.push('<div class=\"TT2yellowMessageLine1\" id=\"TT2unconfirmedEmailMessage',params.id,'\"><img class=\"TT2messageSymbol\" src=\"',TurnTo.getStaticUrl(\"/tra2/images/blueexclaimmark.png\"),'\"/><a style=\"display:none\" href=\"javascript:void(0)\" id=\"TT2unconfirmedEmailDismiss',params.id,'\"><img src=\"', TurnTo.getStaticUrl(\"/tra2/images/subCloseButton.png\") ,'\" border=\"0\" /></a> Your email address ',params.email,' is unconfirmed. Please look for the confirmation message in your in-box. <a href=\"javascript:void(0);\" id=\"TT2resendEmailConfirmation',params.id,'\" style=\"text-decoration:underline;\">Resend confirmation messsage.</a><div style=\"clear:both\"></div></div>');}return p.join('');"),noUserPhotoMessage:new Function("obj","var p=[];with(obj){p.push('<div class=\"TT2yellowMessageLine1\" id=\"TT2noUserPhotoMessage\" class=\"TT2yellowMessageLine1\"><img class=\"TT2messageSymbol\" src=\"',TurnTo.getStaticUrl(\"/tra2/images/blueexclaimmark.png\"),'\"/> <a style=\"display:none\" href=\"javascript:void(0)\" id=\"TT2noUserPhotoDismiss\"><img src=\"', TurnTo.getStaticUrl(\"/tra2/images/subCloseButton.png\") ,'\" border=\"0\" /></a> You don&#39;t have a photo! To add a photo, <a href=\"javascript:void(0);\" id=\"TT2AddUserPhoto\" style=\"text-decoration:underline;\">click here.</a><div style=\"clear:both\"></div></div>');}return p.join('');"),forwardMatchInvitationEmailSentMessage:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2inviteMessage',params.suid,'\">OK. We&#39;ve just sent this person a note asking if they want to share their name. We will let you know as soon as they do.</div>');}return p.join('');"),instantTeaSpinScreen:new Function("obj","var p=[];with(obj){p.push('<div id=\"TTtraLayout\"><div id=\"TT-spinner\" style=\"text-align:center;padding-top:60px;padding-bottom:50px;\"></div></div>');}return p.join('');"),TTtraLoggedInMain:'<div id="TTtraDialogTitleBar2" class="TT-ui-helper-clearfix TurnToWidgetBarColor" unselectable="on"style="-moz-user-select: none;position: relative"><div class="TT2topbarLI" style="padding-top:20px;padding-left:0px;padding-right:10px;width:670px;float:right;"><span id="TTtraDialogTitle" unselectable="on" style="-moz-user-select: none;"></span><a id="TTtraWindowClose" href="javascript:void(0)" role="button" unselectable="on" style="margin-top:-33px;height:25px;width:25px"></a><span id="TTtraDialogFirstName" style=""></span><span class="TT2verticalSeparator">|</span><a id="TTaddFriendsHdrLink" href="javascript:void(0)" class="TTexternalLink"style="font-size:11px;display:none;float:left">Check for more friends</a><span class="TT2verticalSeparator">|</span><a id="TT2userSettingLink" href="javascript:void(0)" class="TTexternalLink"style="font-size:11px;display:none;float:left">Settings</a><span class="TT2verticalSeparator">|</span><a id="TT2ManagePurchasesLink" href="javascript:void(0)" class="TTexternalLink"style="font-size:11px;display:none;float:left"></a><span id="TT2myAskLinkSeparator" class="TT2verticalSeparator" style="display:none;">|</span><a id="TT2myAskLink" href="javascript:void(0)" class="TTexternalLink"style="color:#777777;float:left;display:none;font-size:11px">My Q&A</a></div></div><div id="TTmainContent" class="TTdialog TTtra-ui-dialog-content"style="height: auto; min-height: 93px; width: auto;"><div id="TT2MainSection"><div id="TTLImessageArea" style="display:none;"></div><div id="TT2topMessageSection" class="TTsectionBox" style="display:none;"></div><div class="TT2sectionHeader"><div id="TTnetworkTitle" style="float:left;"></div><div id="TT2zipbox" style="float:right;"></div><div class="TTclearN"></div></div><div id="TT2myNetworkSection"><div id="TT-regflow"></div><div id="TTtraLoggedInMain"></div><div id="TTtraLayout" style="position:relative;width:650px"><div id="TT-spinner" style="text-align:center;padding-top:60px;padding-bottom:50px;"></div></div></div><p>&nbsp;</p></div></div><div id="TT2footer" class="TurnToWidgetBarColor"><a id="TT2footerFeedback" style="font-size: 10px;">Feedback</a><span id="TTtraFooterLogo">&nbsp;</span></div>',TTtraLoggedOutMain:'<div id="TTtraDialogTitleBar" class="TT-ui-helper-clearfix TurnToWidgetBarColor" unselectable="on"style="-moz-user-select: none;"><span id="TTtraDialogTitle" unselectable="on" style="-moz-user-select: none;"></span><a id="TTtraWindowClose" href="javascript:void(0)" role="button" unselectable="on"style="-moz-user-select: none;"></a><div style="float:right;margin:15px 25px 0 0"><a id="TTsignUpLink3" href="javascript:void(0)" class="TTlogoutLink"style="font-weight:bold; font-size:10px;color:#6D89C0;text-decoration:underline;cursor:pointer">Log In</a></div></div><div id="TTmainContent" class="TTdialog TTtra-ui-dialog-content"style="height: auto; min-height: 93px; width: auto; min-width:670px"><div id="TT2MainSection"><div id="TT2topMessageSection" class="TTsectionBox" style="display:none;"></div><div class="TT2sectionHeader" style="position: relative;"><div id="TTnetworkTitle" style="margin-left: 15px"><h1 style="padding-top:10px;padding-bottom:10px;font-size:19px" id="TT2loheadercopy">Some ideas from other customers</h1><div id="TT2fbHeader"></div></div><div class="TTclearN"></div></div><div id="TT2myNetworkSection" style="position: relative;"><div id="TTtraLayout" style="position:relative;width:650px" ><div style="text-align:center;"><h1 style="font-weight: bold;padding-top:10px;padding-bottom:10px;font-size:19px">Hang on while we get your info...</h1><div id="TT-spinner" style="text-align:center;padding-top:60px;padding-bottom:50px;"></div></div></div></div></div></div><div id="TT2footer" class="TurnToWidgetBarColor"><a id="TT2footerFeedback" style="font-size: 10px;">Feedback</a><span id="TTtraFooterLogo">&nbsp;</span></div>',mainscreen:'<div id="TTtraBackOverlay" class="TTui-widget-overlay" style="display:none;z-index: 120001;"></div><div id="TTtraWindow" class="TTtra-ui-dialog TT2mainDialog"style="overflow: hidden; display: none; position: absolute; z-index: 120002; outline-color: -moz-use-text-color; outline-style: none; outline-width: 0px; width: 715px; top: 10px;"tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-TTsigninWrapper"><div id="TTtraUserStateMain" style="position: relative;"></div><div id="TTtraInsideOverlay" class="TTsub-ui-widget-overlay" style="display:none;z-index: 120001;"></div><div id="TTtraSubWindow" class="TTtra-ui-dialog TTSubDialog"style="overflow: hidden; display: none; position: absolute; z-index: 120005; outline-color: -moz-use-text-color; outline-style: none; outline-width: 0px; height: auto; width: 575px; top: 75px; left:75px"tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-TTSubWrapper"><div id="TTtrasubtop"><a id="TTSubWindowClose" href="javascript:void(0)" role="button" unselectable="on" style="-moz-user-select: none;"></a></div><div id="TTtrasubmid"><div id="TTSubDialogSection"    class="TTdialog"style="height: auto; min-height: 93px; width: auto;"><div id="TTSubScreen"></div></div></div><div id="TTtrasubbot"></div></div><div id="TTtraBubbleWindow" class="TTtra-ui-dialog"style="overflow: hidden; display: none; position: absolute; z-index: 120005; outline-color: -moz-use-text-color; outline-style: none; outline-width: 0px; height: 352px; width: 462px; top: 22%; left:126px"tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-TTSubWrapper"><a id="TTBubbleWindowClose" href="javascript:void(0)" role="button" unselectable="on" style="-moz-user-select: none;"></a><div id="TTBubbleDialogSection"style="height: auto; min-height: 93px; width: auto;margin-top:40px;"><div id="TTBubbleScreen"></div></div></div><div id="TTtoolTip" class="TTtipCopy"></div></div>',spinscreen:'<div id="TTtraBackOverlay" class="TTui-widget-overlay" style="display:block;z-index: 120001;"></div><div id="TTtraWindow" class="TTtra-ui-dialog TT2mainDialog"style="overflow: hidden; display: block; position: absolute; z-index: 120002; outline-color: -moz-use-text-color; outline-style: none; outline-width: 0px; width: 715px; top: 10px;"tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-TTsigninWrapper"><div id="TTtraUserStateMain" style="position: relative;"><div id="TTtraDialogTitleBar" class="TT-ui-helper-clearfix TurnToWidgetBarColor" unselectable="on"style="-moz-user-select: none;"><span id="TTtraDialogTitle" unselectable="on" style="-moz-user-select: none;"></span><a id="TTspinnerClose" href="javascript:void(0)" role="button" unselectable="on"style="-moz-user-select: none;float:left;position: absolute; left: .3em; top: 50%; width: 25px; margin: -15px 1px 0 -3px; padding: 1px; height: 25px;"></a></div><div id="TTmainContent" class="TTdialog TTtra-ui-dialog-content"style="height: auto; min-height: 93px; width: auto; min-width:670px"><div id="TT2MainSection"><div id="TT2topMessageSection" class="TTsectionBox" style="display:none;"></div><div class="TT2sectionHeader" style="position: relative;"><div id="TTnetworkTitle" style="text-align:center;width:620px;"><h1 style="padding-top:10px;padding-bottom:10px;font-size:19px"></h1><div id="TT-spinner" style="text-align:center;padding-top:60px;padding-bottom:50px;"></div></div></div></div></div><div id="TT2footer" class="TurnToWidgetBarColor"><a id="TT2footerFeedback" style="font-size: 10px;"></a><span id="TTtraFooterLogo">&nbsp;</span></div></div></div>',traMatchesError:'<div>Service is currently under maintance. Please come back to check soon!</div>',FLFitemRow:new Function("obj","var p=[];with(obj){p.push('        <div class=\"TTmatchDetail\" style=\"display: block;\">            <div class=\"TTraveArea\" style=\"padding-left: 0pt;\">                <div class=\"TTbullet\">   ');if (m.url) {p.push('                    <a class=\"TTtransactionItemLink\"                       href=\"', matchLinkRoot ,'&pHref=', encodeURIComponent(m.url) ,'&pd=&v=', TurnTo.version ,'&mId=', m.id ,'&tId=', tId ,'&tiId=', m.id ,' \">                        ', m.title ,'</a> ');} else {p.push('   <i>', m.title ,'</i>');}p.push('                    '); if ( m.rave) { p.push('                    '); if ( m.raveComment) { p.push('                    <img class=\"TTraveLink\" src=\"', TurnTo.getStaticUrl('/tra/images/starwithdoc.png'),'\" border=\"0\" style=\"padding-left:0\"/>                    '); } else { p.push('                    <img class=\"TTraveLink\" src=\"', TurnTo.getStaticUrl('/tra/images/star.png'),'\" border=\"0\" style=\"padding-left:0\"/> '); } p.push('                    '); } p.push('                </div>                <div style=\"display: none;\" class=\"TTraveOuterBox\">                    <div class=\"TTraveTitle\"><img src=\"', TurnTo.getStaticUrl('/tra/images/star.png'),'\" border=\"0\"/> ', m.firstName ,' ', m.lastName ,'                        recommends this                    </div>            '); if ( m.raveComment) { p.push('                    <div class=\"TTraveCommentBox\">                        &#147;', m.raveCommentValue ,'&#148;                    </div>            '); } p.push('                </div>            </div>        </div>');}return p.join('');"),FLFrow:new Function("obj","var p=[];with(obj){p.push('<div id=\"TTmatchesDiv\">    <div class=\"TTmemberRows\">        <div class=\"TTtraName TTraveArea\">            <a class=\"TTmatchLink\" href=\"javascript:void(0)\">', m.firstName ,' ', m.lastName ,' ', turnToSettings.verb ,'</a>            '); if (m.siteRaveComment){ p.push('                <img class=\"TTsiteRaveLink\" src=\"', TurnTo.getStaticUrl('/tra/images/starwithdoc.png'),'\" border=\"0\"/>            '); } else if (m.siteRave){ p.push('                <img class=\"TTsiteRaveLink\" src=\"', TurnTo.getStaticUrl('/tra/images/star.png'),'\" border=\"0\"/>            ');}p.push('            <div  class=\"TTraveOuterBox\" style=\"display:none\">                <div class=\"TTraveTitle\"><img src=\"', TurnTo.getStaticUrl('/tra/images/star.png'),'\" border=\"0\"/>', m.firstName ,' ', m.lastName ,'                    recommends ', site.siteName ,'                </div>                '); if (m.siteRaveComment){ p.push('                <div class=\"TTraveCommentBox\">                    &#147;', m.siteRaveComment ,' &#148;                </div>                ');}p.push('            </div>        </div>        <a class=\"TTmemberMail\" title=\"Send email to ', m.firstName ,'\" target=\"_blank\" href=\"mailto:', m.email ,'\"/>        '); if (m.aim) { p.push('        <a class=\"TTAimIcon\" title=\"Send AIM message to ', m.firstName ,'. You must have an AIM client installed.\"           href=\"aim:GoIM?screenname=', m.aim ,'\"/>        '); } p.push('        <ul id=\"TT-FLFmatch-row', rowNum ,'\"></ul>    </div></div>');}return p.join('');"),_connectAndSourceSelector:new Function("obj","var p=[];with(obj){p.push('    <div style=\"float:left\">    <ul id=\"TTimportLogos\">        ');if (!hideFacebook) {p.push('        <li>            <a href=\"javascript:void(0)\" id=\"TTfbConnect2\"  style=\"display:block;height:40px;width:120px;text-align:center;line-height:40px\">                Facebook            </a>        </li>        ');}p.push('    ');        var prevType = TurnTo.matchData.importSrc[0].type;        for (var i=0; i < TurnTo.matchData.importSrc.length; i++) {            var impSrc = TurnTo.matchData.importSrc[i];        p.push('        <li id=\"TT2importSrc-',impSrc.id,'\">        <a href=\"javascript:void(0)\" id=\"TT2importSrcLink',impSrc.id,'\" style=\"display:block;height:40px;width:120px;text-align:center;line-height:40px;',i == TurnTo.matchData.importSrc.length - 1 ? 'border-bottom:#DDD 1px solid' : '','\">            ', impSrc.disp ,'        </a>        </li>        '); } p.push('    </ul>        '); if (!TurnTo.matchData.user) { p.push('    <br/>            <a href=\"javascript:void(0)\" id=\"TT2NoImportLink\">I don&#39;t use any of these</a>        '); } p.push('    </div>    <!--Put a empty cell here (TT2importedEmptyMsg) if no msg so the pick on image stay at the same position-->    <div id=\"',showConfirm && matches.delAuthImportMsg ? 'TT2importedMsg' : 'TT2importedEmptyMsg','\">        '); if (showConfirm && matches.delAuthImportMsg) { p.push('            ', matches.delAuthImportMsg,'            '); if (matches.importHistory && matches.importHistory[0].src == 'Facebook' && matches.importHistory.length == 1) { p.push('                <span style=\"font-weight:bold;font-size:10px\">Be sure to add friends from other sources as well.</span>                <span style=\"font-size:10px\" id=\"TTtipWhy\">(Why?)</span>                <div id=\"TTtipWhy-tip\" class=\"TTtip\">TurnTo uses email addresses to make connections, and Facebook doesn&#39;t                    provide your friends&#39; email addresses.                    So if you only add friends from Facebook,we might miss lots of your friends.                </div>            '); } p.push('        '); } else { p.push('             <br/><br/>        '); } p.push('    </div>     <div id=\"TT2ImportSrcForm\" style=\"margin-left: 35%;\">        <ul id=\"TTSubDialogErrors\"></ul>        <div id=\"TTpickone\"></div>    </div>    ');if (matches.importHistory && matches.importHistory.length > 0) {p.push('    <div id=\"TT2importHistory\">        Currently checking for ',matches.totalFriends,' friends.<br/>        Most recent additions:<br/>        ');for (var i = 0; i < matches.importHistory.length; i++) {p.push('            <span style=\"font-weight:bold;padding-left:25px\">',matches.importHistory[i].src,'</span> on ',matches.importHistory[i].date,'            <br/>        ');}p.push('    </div>    ');}p.push('    <div class=\"TT2clearBoth\"></div>    <div id=\"TT2importRegForm\" style=\"display:none;margin-top:20px;\">        ', TurnToHTML.regform({matches:TurnTo.matchData, getUrl:TurnTo.getUrl, getStaticUrl:TurnTo.getStaticUrl,regflow:null}) ,'    </div>');}return p.join('');"),addMoreEmails:new Function("obj","var p=[];with(obj){p.push('<div>    <div style=\"margin: 0; color:#666666;font-size:10px;\">        '); if (hasEmail) { p.push('        Enter other email addresses your friends have for you, since they may be trying to connect with you under any one of them.        '); } else { p.push('        Enter the email addresses your friends have for you.  If you have more than one, enter all, since your friends may be trying to connect with you under any one of them.        '); } p.push('    </div>    <ul id=\"TTSubDialogErrors\"></ul>    <form id=\"TTaddMoreEmailsForm\">        <div style=\"margin: 10px 0; font-size:10px; color:#666666;\">            '); for (var u = 0; u < matches.user.uIdents.length; u++) { if(matches.user.uIdents[u].type == 1) { p.push('', matches.user.uIdents[u].ident ,'<br>'); } } p.push('        </div>        <div>            <div id=\"TTmoreEmails\" style=\"float:left; font-size:10px;\">                <input type=\"text\" id=\"TTaddEmail_0\" name=\"TTaddEmail_0\" style=\"display:block\" ttInputLabelText=\"Email address\" value=\"Email address\"                       onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\" class=\"TTdefaultInput\"/>            </div>            <div style=\"float:left; font-size:10px; font-weight:bold; margin-left:5px; margin-top:2px;\">                <a id=\"TTaddMoreEmailsForm-moreLn\" href=\"javascript:void(0)\" style=\"font-size:11px;\">[+] Add another</a>            </div>            <div class=\"TT-ui-helper-clearfix\"></div>            <div class=\"TTclearN\"></div>                    </div>        <div style=\"float:right; margin-top:20px;\">            <input type=\"submit\" id=\"TTaddMoreEmailsFormSubmit\" class=\"TTsubmitButton\" style=\"background-color:#4e3790;\" value=\"Done\">        </div>        <div class=\"TT-ui-helper-clearfix\"></div>        <div class=\"TTclearN\"></div>    </form></div>');}return p.join('');"),answerBlock:new Function("obj","var p=[];with(obj){p.push('');var thisIsYourAnswer = TurnTo.matchData.user && TurnTo.matchData.user.id == answerer.uid;  var thisIsYourQuestion = TurnTo.matchData.user && TurnTo.matchData.user.id == quid;  var youAreManager = TurnTo.matchData.user && TurnTo.isSiteManager(TurnTo.matchData.user.userSiteRoles);p.push('<input type=\"hidden\" id=\"TT2qid4answer-',aid,'\" name=\"TT2qid4answer-',aid,'\" value=\"',qid,'\"/>        <div class=\"TT2answersBlock\" style=\"margin-bottom:2px;\">    <div class=\"TT2askedUserPhoto\" style=\"margin:0px 10px 0px 0px\">        <img style=\"display:inline;width:35px;height:35px\"             src=\"', TurnTo.getUserPhoto(answerer.uid, answerer.pid) ,'\"             alt=\"\"/>            ');if (answerer.isStaff) {p.push('            <img style=\"display: block;height: 15px;width: 35px;margin-top:-3px\" src=\"', TurnTo.getStaticUrl('/tra2/images/stafflabel.png') ,'\" alt=\"\"/>            ');}p.push('    </div>    <div style=\"margin-top:5px;margin-left:45px;\">        <input id=\"TT2rawAnswerText-',aid,'\" type=\"hidden\" value=\"',rawAnswerText,'\"/>        ');if (thisIsYourAnswer) {p.push('        <span class=\"TT2right\"><input id=\"TT2shareAnswerBtn-',aid,'\" aid=\"',aid,'\"                                      class=\"TT2shareAnswerBtn  TT2button\"                                      style=\"font-size:10px;font-weight:bold;color:#FFFFFF;background:#6175A9;border-color:#1D4088\"                                      type=\"button\" value=\"Share\"/></span>        ');}p.push('        <span class=\"TT2bold\">',thisIsYourAnswer ? 'You' : answerer.fName + (answerer.lName ? ' ' + answerer.lName.substring(0, 1) : ''),':</span>        <span>',answerText,'</span>    </div>    ');    var lastAnswererReply = -1;    var lastAskerReply = -1;    var lastManagerReply = -1;    if (showReply) {        if (thisIsYourAnswer || thisIsYourQuestion || youAreManager) {            for (var i = 0; i < replys.length; i++) {                if (replys[i].uid == answerer.uid) {                    lastAnswererReply = i;                } else if (replys[i].uid == quid) {                    lastAskerReply = i;                } else {                    lastManagerReply = i;                }            }        }    }p.push('    ');if (showReply && lastAnswererReply == -1 && ((thisIsYourQuestion && !thisIsYourAnswer) || (youAreManager && !thisIsYourAnswer))) {p.push('    <div style=\"margin-left:35px;\">        <a id=\"TT2answerReply-',qid,'-',aid,'-',answerer.uid,'\" class=\"TT2answerReply TT2right\" style=\"font-size:10px;color:#314C9B\" href=\"javascript:void(0)\">Reply to ',answerer.fName,'</a>        ',TurnToHTML.inlineAnswerInputBox({qid:qid, defaultText:\"Enter your reply here. We&#39;ll email it to \" + answerer.fName, aid:aid, show:false, replyInput:true, related:related, replyToUid:answerer.uid}),'    </div>    ');}p.push('    ');for (var i = 0; i < replys.length; i++) {        var showReplyLink = showReply && ((lastAnswererReply == i && (thisIsYourQuestion || youAreManager)) || (lastAskerReply == i && (thisIsYourAnswer || youAreManager)) || (lastManagerReply == i && (thisIsYourQuestion || thisIsYourAnswer)));    p.push('        ',TurnToHTML.replyBlock({qid:qid,aid:aid,showReplyLink:showReplyLink,reply:replys[i], related:related}),'    ');}p.push('    <div id=\"TT2lastAnswerblock-',aid,'\" style=\"display:none\"></div>    <div id=\"TT2answerSubmitted-',aid,'\" class=\"TT2inlineMessage\" style=\"margin-top:2px;margin-left: 45px;margin-bottom:4px;\">        <span style=\"float:right\"><a id=\"TT2dismissSubmitted-',aid,'\" href=\"javascript:void(0)\"><img src=\"',TurnTo.getStaticUrl('/images/tabClose.gif'),'\" alt=\"Dismiss\"/></a></span>        <b>Your answer has been submitted.</b>    </div>    <div id=\"TT2answerSubmittedNotConfirmed-',aid,'\" class=\"TT2inlineMessage\" style=\"margin-top:2px;margin-left: 45px;margin-bottom:4px;\">        <span style=\"float:right\"><a id=\"TT2dismissNotConfirmed-',aid,'\" href=\"javascript:void(0)\"><img src=\"',TurnTo.getStaticUrl('/images/tabClose.gif'),'\" alt=\"Dismiss\"/></a></span>        <div style=\"margin-right:16px;\">            <b>Awaiting confirmation</b> - Before we submit your answer, please confirm your email address. Go to your inbox            and look for the confirmation email. (If you don&#39;t receive this email within a few minutes, please check your spam filter.)*            <a id=\"TT2resendConfirmation-',aid,'\" class=\"TT2resendConfirmation\" href=\"javascript:void(0)\">Resend confirmation message</a>            <span style=\"display:block;font-size:10px;margin-top:5px;\">*You only need to do this once - then you can ask and answer other questions all you like.</span>        </div>    </div>    <div class=\"TT2clearBoth\"></div></div>');}return p.join('');"),answerRegFlow:new Function("obj","var p=[];with(obj){p.push('<div id=\"TTSubDialogErrors\"></div><form id=\"TTregForm\" action=\"javascript:void()\">    <div id=\"TT2answerRegPhoto\" style=\"margin-bottom: 20px;\">        <div style=\"margin-bottom:5px;\">Optional: Add a photo of yourself. It helps to make these interactions more friendly.</div>        <div style=\"float:left;margin-right:10px;position:relative;\">            <img id=\"TT2userImg\" src=\"',TurnTo.getUserPhoto(),'\" style=\"width:45px;height:45px\"/>            <img id=\"TT2userImgLoading\" src=\"',TurnTo.getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"                 style=\"border: 1px solid rgb(204, 204, 204); width:45px;height:45px; display: none;\">        </div>        <div id=\"TT2regImgUpload\" style=\"float:left;position:relative;\">            <input type=\"file\" name=\"TT2userImgFile\" id=\"TT2userImgFile\"                   onchange=\"TurnTo.inputUserImage(this)\"/><br/>            Select an image file on your computer (4MB max)        </div>        <div style=\"clear:both; padding-top:10px;\">            Or <a href=\"javascript:void(0)\" id=\"TTpostRegFbConnect\">            <img src=\"',TurnTo.fbButtonSrc ,'\" alt=\"Connect\" style=\"border:none; vertical-align:middle;\"/>            </a> to use your Facebook photo<br>            <span style=\"font-size:10px;\">(Facebook will ask you to confirm this)</span>        </div>    </div>    <div style=\"margin-bottom: 20px;\">        <div style=\"margin-bottom:5px;\">        Optional: Create a password so you can add or edit your comments in the future.        </div>        <input type=\"text\" name=\"TTpasswordText\" id=\"TTpasswordText\" class=\"TTdefaultInput\"                           value=\"Password\"                           ttpasswordpair=\"TTpassword\" onfocus=\"TurnTo.inputFocus(this)\"                           onblur=\"TurnTo.inputLoseFocus(this)\"               style=\"float:left; width:170px;position:relative;\">        <input type=\"password\" name=\"TTpassword\" id=\"TTpassword\" style=\"display: none;float:left; width:170px;position:relative;\"               ttpasswordpair=\"TTpasswordText\" onblur=\"TurnTo.inputLoseFocus(this)\"               >        <input type=\"text\" name=\"TTrePasswordText\" id=\"TTrePasswordText\" class=\"TTdefaultInput\"                           value=\"Re-type Password\" ttpasswordpair=\"TTrePassword\"                           onfocus=\"TurnTo.inputFocus(this)\"                           onblur=\"TurnTo.inputLoseFocus(this)\"               style=\"float:left; width:170px; margin-left:10px;position:relative;\">        <input type=\"password\" name=\"TTrePassword\" id=\"TTrePassword\" style=\"display: none;float:left; width:170px;margin-left:10px;position:relative;\"               ttpasswordpair=\"TTrePasswordText\" onblur=\"TurnTo.inputLoseFocus(this)\"               >        <div style=\"clear:both; padding-top:10px;\">            Or just <a href=\"javascript:void(0)\" id=\"TTpostRegFbConnect2\">            <img src=\"',TurnTo.fbButtonSrc ,'\" alt=\"Connect\" style=\"border:none; vertical-align:middle;\"/>            </a><br>            <span style=\"font-size:10px;\">(Facebook will ask you to confirm this)</span>                    </div>    </div>    <div style=\"margin-top: 7px;float:right;position:relative;\">        <input type=\"submit\" class=\"TT2doneSub\" value=\"\" style=\"width:75px; height:22px;\"/>        <img id=\"TTregSubmitLoading\" src=\"',TurnTo.getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"             style=\"border: 1px solid rgb(204, 204, 204); padding: 1px 23px; display: none;\">    </div>    <div class=\"TTclearN\"></div></form>');}return p.join('');"),answerSharePanel:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2shareAnswerPanel\">    ');if (answerSubmitted) {p.push('    <div style=\"font-weight:bold;text-align:center;margin-bottom:18px;position:relative;\">        <span class=\"TT2inlineMessage\" style=\"display:inline;color:#777777;padding:6px\">Your answer has been submitted.</span>    </div>    ');}p.push('    <div style=\"font-weight:bold;text-align:center;font-size:16px;position:relative;\">        Want to share your thought with your friends, too?        <br/>        Share this answer.    </div>    <div style=\"margin-top:18px;\">        <div class=\"TT2left\" style=\"',!catImgProcessed?'display:none;':'','margin-left:20px;margin-right:20px\">            <img class=\"TT2productImg\" src=\"',TurnTo.getProductImage(catImgId,'SS'),'\"/>        </div>        <div class=\"TT2left\" style=\"margin-left:2px\">            <div id=\"TT2shareError\" style=\"color:red\"></div>            <div style=\"margin-bottom:4px;\">                <div class=\"TT2left\" style=\"margin-right: 10px;\">                    <input type=\"checkbox\" id=\"TT2facebookBox\" name=\"TT2facebookBox\" value=\"true\" class=\"TT2left\"                           style=\"margin-right:4px;margin-top:2px;\"/>                    <span style=\"color:#1D4088\"><img src=\"',TurnTo.getStaticUrl('/tra2/images/smallfbicon.png'),'\" alt=\"Facebook\" style=\"vertical-align:bottom\"/> Facebook</span>                </div>                <div id=\"TT2twitterDiv\" class=\"TT2left\" style=\"margin-right: 12px;\">                    <input type=\"checkbox\" id=\"TT2twitterBox\" name=\"TT2twitterBox\" value=\"true\" class=\"TT2left\"                           style=\"margin-right:4px;margin-top:2px;\"/>                    <span style=\"color:#1D4088\"><img id=\"TT2twitterImg\" src=\"',TurnTo.getStaticUrl('/tra2/images/smalltwittericon.png'),'\"                         alt=\"Twitter\" style=\"vertical-align:bottom\"/> Twitter</span>                </div>            </div>            <textarea id=\"TT2shareTextArea\" style=\"width:',!catImgProcessed?'502':'378','px;margin-top:4px\">My answer to: \"',questionText,'\"</textarea>            <div style=\"width:',!catImgProcessed?'508':'384','px;margin-top:6px;\">                <div class=\"TT2left\">                    <span style=\"font-size:10px;color:#777777\">Click in the box to modify your answer</span>                </div>                <div id=\"TT2shareBtns\" class=\"TT2right\">                    <input id=\"TT2shareBtn\" class=\"TT2button\" style=\"font-size:10px;font-weight:bold;color:#FFFFFF;background:#6175A9;border-color:#1D4088\" type=\"button\" value=\"Share\"/>                    <input id=\"TT2cancelShareBtn\" class=\"TT2button\" style=\"font-size:10px;font-weight:bold;background-color:#FFFFFF;color:#000000;border-color:#000000\" type=\"button\" value=\"No Thanks\"/>                </div>                <img id=\"TT2shareLoading\" style=\"display:none\" class=\"TT2imgLoading TT2right\" src=\"',TurnTo.getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\">            </div>        </div>        <div class=\"TT2clear\"></div>    </div></div>');}return p.join('');"),answerWidget:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2answerWidget\">    <div class=\"TT2awHeader\"><b>',answerer.fName ? answerer.fName : answerer.email,'</b>, thanks for helping <b>',asker.fName,' ',asker.lName ? asker.lName.substring(0, 1) : \"\",'</b></div>    <div>        <div id=\"TT2awQuestionSection\">            <div class=\"TT2askedUserPhoto\">                <img style=\"display:inline\" src=\"', TurnTo.getUserPhoto(asker.id, asker.pid) ,'\" alt=\"\"/>            </div>            <div style=\"margin-left:63px\">                <span class=\"TT2bold\">', asker.fName ,' ', asker.lName ? asker.lName.substring(0, 1) : \"\" ,' asked about <a id=\"TT2itemLink-title-',item.id,'\" class=\"TT2itemLink\" href=\"javascript:void(0)\" style=\"font-size:12px;font-weight:bold;color:#314C9B\">',TurnTojQuery.trim(item.title),'</a>:</span><br>                <span>',question.text,'</span>            </div>            <div class=\"TT2clearBoth\"></div>            <div id=\"TT2answerPanel\" class=\"TT2userInputArea\" style=\"padding:12px;\">                    <div id=\"TT2error\" class=\"TT2right\"></div>                    <textarea id=\"TT2answerText\" rows=\"12\" style=\"width:430px;\" class=\"TTdefaultInput\" ttinputlabeltext=\"Please enter your answer\"                            onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\">Please enter your answer</textarea>                    <div id=\"TT2anwerCharCount\" class=\"TT2charLimitCount TT2right\"></div>                                       <div style=\"margin-top:10px; clear:both; display:none\">                        <input type=\"checkbox\" id=\"TT2answerNotify\" checked=\"true\"/>&nbsp;Tell me when other people answer or comment on this.                    </div>                    <ul style=\"font-size:11px; padding-top:10px; list-style-type:disc; margin-left:14px;\">                        <li>                            We will send your answer to ',asker.fName,' (so it arrives right away) and post it on our site so other shoppers can also benefit from your experience.                        </li>                        <li>Do not include: HTML, references to other stores, pricing, contact information (inc. your email &mdash; you can add that later), or inappropriate language. Posts which include these will be edited or removed.</li>                    </ul>                    <div id=\"TT2answerBtnPanel\" >                        <img id=\"TT2imgLoading\" class=\"TT2imgLoading\" src=\"',TurnTo.getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"                            style=\"display: none;\">                        <input id=\"TT2answerBtn\" type=\"button\" class=\"TT2buttonSub\" value=\"\" style=\"height:29px; width:91px; margin-top:10px;\">                    </div>            </div>            <div id=\"TT2answerSubmittedPanel\" style=\"display:none;padding:12px;margin-bottom:2px;\" class=\"TT2userInputArea\">                    <div style=\"font-weight:bold\">Your answer has been submitted:</div>                    <div id=\"TT2answerSubmittedPanelTxt\" style=\"margin-top:4px;\"></div>               </div>            <div  id=\"TT2answerRegFlow\" style=\"display:none;padding:12px; margin-top:0;\" class=\"TT2userInputArea\">                    ', TurnToHTML.answerRegFlow({}) ,'            </div>        </div>        <div style=\"float:right;margin-right:20px;\">            <a id=\"TT2itemLink-image-',item.id,'\" class=\"TT2itemLink\" href=\"javascript:void(0)\"><img src=\"',TurnTo.getProductImage(item.imgId,'L'),'\" alt=\"',item.title,'\" style=\"width:165px;height:165px;\"></a>        </div>    </div>    <div class=\"TT2clearBoth\"></div></div>');}return p.join('');"),askCoupon:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2askCoupon\">    ',couponText,'    <br/>    For your records we just emailed this to you.    <div style=\"text-align:center;margin-top:20px\">        <input id=\"TT2closeCoupon\" class=\"TT2button\" style=\"font-size:16px;width:150px;height:30px;\" type=\"button\" value=\"Close\"/>    </div></div>');}return p.join('');"),askWidgetQuestionSection:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2questionSection\">    <div>        <div style=\"height:165px;width:165px;float:left;',!catImgProcessed?'display:none;':'','\">            <a id=\"TT2itemLink-image-',catItmId,'\" class=\"TT2itemLink\" href=\"javascript:void(0)\">                <img class=\"TT2productImg\" src=\"',TurnTo.getProductImage(catImgId,'L'),'\" style=\"width:100%;height:100%;\"/>            </a>        </div>        <div style=\"float:right;width:',!catImgProcessed?'100%':'435px',';position:relative;\">            <input type=\"hidden\" id=\"TT2iid\" value=\"',iid,'\"/>            <input type=\"hidden\" id=\"TT2catItmId\" value=\"',catItmId,'\"/>            <input type=\"hidden\" id=\"TT2catImgId\" value=\"',catImgId,'\"/>            <input type=\"hidden\" id=\"TT2catImgProcessed\" value=\"',catImgProcessed,'\"/>                        <div class=\"TT2bold\" style=\"margin-left:2px;height:50px;font-size:16px;\">', TurnTo.customCopy.questionHeader ,' <span style=\"font-weight:normal\">We&#39;ll email them your question, then email you their replies.</span></div>            <div><textarea id=\"TT2questionText\" style=\"height:114px;width:',!catImgProcessed?'100%':'433px',';font-size:16px;margin:0px;padding:0px\"                    ttinputlabeltext=\"Enter your question here.\"                    onfocus=\"TurnTo.inputFocus(this,TurnTo.TT2questionTextFocus)\"                    class=\"TTdefaultInput\"                    onblur=\"TurnTo.inputLoseFocus(this,TurnTo.TT2questionTextLoseFocus)\">Enter your question here.</textarea></div>        </div>        <div class=\"TT2clearBoth\" style=\"height:5px\"></div>        <div id=\"TT2anwerCharCount\" class=\"TT2charLimitCount TT2right\"></div>        <div class=\"TT2clearBoth\" style=\"height:5px\"></div>        <div>            <span style=\"float:right;position:relative\">                <img id=\"TT2questionSubImgLoading\" class=\"TT2imgLoading\"                 src=\"',TurnTo.getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"                 style=\"display: none;\">                <img id=\"TT2askOwnersBtn\" class=\"TT2buttonImg\"                 src=\"',TurnTo.getStaticUrl('/tra2/images/askbttn.png'),'\"                 alt=\"Ask\"/>            </span>            <span><img alt=\"\" src=\"',TurnTo.getStaticUrl('/tra2/images/blueexclaimmark.png'),'\" style=\"float:left;\"> Your question is more likely to be answered if you say a few words about why you are asking and if it&#39;s nicely written and punctuated.</span>            <br/><br/>            <span style=\"font-size:10px;\">Do not include: HTML, references to other stores, pricing, contact information (inc. your email &mdash; you can add that later), or inappropriate language. Posts which include these will be edited or removed.</span>        </div>        <div class=\"TT2clearBoth\"></div>    </div></div><div class=\"TT2clearBoth\"></div>');}return p.join('');"),bubble:new Function("obj","var p=[];with(obj){p.push('<div align=\"center\" style=\"padding-bottom:20px;\"><div id=\"TTbubble\" align=\"center\"> <div id=\"tmpImport\" style=\"text-align:center;font-size:11px;\">    <form id=\"TTimportForm1B\" >    <p style=\"margin-bottom:16px; font-size:14px;color:#000;\">        Got questions? Need ideas?<br> </p>    <p style=\"margin-bottom:6px; font-size:14px;font-weight:bold;color:#000;\">', turnToSettings.text2 ,'    </p>    <div align=\"center\" style=\"float:none;text-align:center;\">        <a href=\"javascript:void(0)\" id=\"TTfbConnectB\" style=\"text-align:center;float:none\">            <img src=\"',fbButtonSrc ,'\" alt=\"Connect\" style=\"border:none;float:none\" align=\"middle\" />        </a>    </div>        <br>    <div style=\"padding:0px 10px 0 10px;text-align:center;color:#000;\"></div>    <a id=\"TT2addFriendsLogoutLink3\" href=\"javascript:void(0)\" style=\"font-weight:bold;font-size:10px;color:#3994d8;text-decoration:none\">I don&#39;t use Facebook</a>    <div class=\"TT-ui-helper-clearfix\"></div>        <p style=\"font-size:10px;padding-top:10px;color:#000;\">Nothing will be posted to Facebook or sent to your friends.            <br>No one will be added to any mailing lists.            <span style=\"font-weight:bold;font-size:10px; color: #3994D8;cursor:pointer;\" id=\"TTtipSpecialB\"> (more info) </span></p>        <!-- <div class=\"TTclearN\"></div> -->    </form>    <p style=\"margin-top:6px; font-size:11px;;color:#000;\">Already have a TurnTo account? <a id=\"TT2signUpLink3\" href=\"javascript:void(0)\" class=\"TTlogoutLink TTexternalLink\" style=\"font-weight:bold; font-size:11px; color: #3994D8;text-decoration:none\">Log in</a></p></div></div></div>');}return p.join('');"),confirmEmailsReminderForPurchases:new Function("obj","var p=[];with(obj){p.push('<div>    <div style=\"font-size:16px\">Got it.    <br/><br/><br/>    Before we check for purchases, please confirm ',emails.length > 1 ? 'that email is' : 'those emails are',' really yours.    Go to your email in-box, look for the confirmation message, and click on the link.    <br/><br/><br/>    If you don&#39;t receive the confirmation message within a couple minutes, please check your spam filter.    <br/><br/>    </div>    <div style=\"margin: 10px 0; font-size:14px; color:#666666;\">        <ul>            '); for (var i = 0; i < emails.length; i++){p.push('            <li>                ',emails[i],'            </li>            ');}p.push('        </ul>    </div></div>');}return p.join('');"),findPurchasesEmails:new Function("obj","var p=[];with(obj){p.push('<div>    <h3 style=\"font-size:20px; font-weight: bold\">We didn&#39;t find any past purchases for you.</h3>    <br/>    <div style=\"margin: 10px 0;font-size:16px\">        If we&#39;re missing some, it may be because you used a different email when you made those purchases.        <br/><br/><br/>        Enter other emails you use for online shopping:    </div>    <ul id=\"TTSubDialogErrors\"></ul>    <form id=\"TTaddMoreEmailsForm\">        <div style=\"margin: 10px 0; font-size:14px; color:#666666;\">            '); for (var u = 0; u < matches.user.uIdents.length; u++) { if(matches.user.uIdents[u].type == 1) { p.push('                ', matches.user.uIdents[u].ident ,' ',matches.user.uIdents[u].confirmed ? '' : '- Unconfirmed','            ');if (!matches.user.uIdents[u].confirmed) {p.push('                <a style=\"color:#003399\" onclick=\"TurnTo.resendEmailConfirmation(&#39;',matches.user.uIdents[u].ident,'&#39;)\" href=\"javascript:void(0)\">(Resend confirmation email)</a>            ');}p.push('            <br>            '); } } p.push('        </div>        <div>            <div id=\"TTmoreEmails\" style=\"float:left; font-size:14px;\">                <input type=\"text\" id=\"TTaddEmail_0\" name=\"TTaddEmail_0\" style=\"display:block\" ttInputLabelText=\"Email address\" value=\"Email address\"                       onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\" class=\"TTdefaultInput\"/>                <input type=\"text\" id=\"TTaddEmail_1\" name=\"TTaddEmail_1\" style=\"display:block\" ttInputLabelText=\"Email address\" value=\"Email address\"                       onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\" class=\"TTdefaultInput\"/>                <input type=\"text\" id=\"TTaddEmail_2\" name=\"TTaddEmail_2\" style=\"display:block\" ttInputLabelText=\"Email address\" value=\"Email address\"                       onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\" class=\"TTdefaultInput\"/>            </div>            <div class=\"TT-ui-helper-clearfix\"></div>            <div class=\"TTclearN\"></div>        </div>        <div style=\"float:right; margin-top:20px;\">            <input type=\"submit\" id=\"TTaddMoreEmailsFormSubmit\" class=\"TTsubmitButton\" style=\"background-color:#4e3790\" value=\"Submit\">        </div>        <div class=\"TT-ui-helper-clearfix\"></div>        <div class=\"TTclearN\"></div>    </form></div>');}return p.join('');"),foafItemRow:new Function("obj","var p=[];with(obj){p.push('        <div class=\"TTmatchDetail\" style=\"display: block;\">            <div class=\"TTraveArea\" style=\"padding-left: 0pt;\">                <div class=\"TTbullet\">');if (m.url) {p.push('                    <a class=\"TTtransactionItemLink\"                       href=\"', matchLinkRoot ,'&pHref=', encodeURIComponent(m.url) ,'&pd=&v=', TurnTo.version ,'&mId=', m.id ,'&tId=', tId ,'&tiId=', m.id ,' \">                        ', m.title ,'</a>');} else {p.push(' <i>', m.title ,'</i>');}p.push('                    '); if ( m.raveCommentCount >0) { p.push('                    <img class=\"TTraveLink\" src=\"', TurnTo.getStaticUrl('/tra/images/starwithdoc.png'),'\" border=\"0\"                         style=\"padding-left:0\"/>                    '); } else if (m.raveCount > 0){ p.push('                    <img class=\"TTraveLink\" src=\"', TurnTo.getStaticUrl('/tra/images/star.png'),'\" border=\"0\" style=\"padding-left:0\"/>                    '); } p.push('                    <div style=\"display: none;\" class=\"TTraveOuterBox\" sku=\"', m.id ,'\">                        <div class=\"TTraveTitle\"><img src=\"', TurnTo.getStaticUrl('/tra/images/star.png'),'\" border=\"0\"/>                            ',m.raveCount,' Recommendation',m.raveCount==1?'':'s','                         </div>                        '); if ( m.raveCommentCount >0) { p.push('                        <div class=\"TTraveCommentBox\"></div>                        '); } p.push('                    </div>        </div>        </div>        </div>');}return p.join('');"),foafRow:new Function("obj","var p=[];with(obj){p.push('<div class=\"TTmemberRows\" style=\"border-bottom:#ddd solid 1px;\">    <b>Friend of <span>    '); for (var ff = 0; ff < m.frndData.length; ff++) { p.push('        ',m.frndData[ff].fFName,' ',m.frndData[ff].fLName,'        ');   if (ff < m.frndData.length - 1 && ff < 2) { p.push('           and        ');   } else { p.push('        </span>        '); break; } p.push('    '); } p.push('    '); if (m.frndData.length > 3) { p.push('        <a href=\"javascript:void(0)\" style=\"text-decoration:underline\" onclick=\"TurnTo.traJq(this).hide().next().show();\"> and others</a>        <span style=\"display:none\"> and        '); for (var ff = 3; ff < m.frndData.length; ff++) { p.push('            ',m.frndData[ff].fFName,' ',m.frndData[ff].fLName,'            ');   if (ff < m.frndData.length - 1) { p.push('               and            '); } p.push('        '); } p.push('                    </span>    '); } p.push('    ', turnToSettings.verb ,'    </b>     <ul id=\"TT-FOAFmatch-row', rowNum ,'\"></ul></div>');}return p.join('');"),forgotPwdScreen:new Function("obj","var p=[];with(obj){p.push('<div style=\"margin:40px 30px 30px 40px; text-align:left;\">    <div style=\"text-align:left;\">        <p class=\"TT2textColor1\" style=\"font-size:22px;font-weight:bold\">Forgot Password?</p>    </div>    <div id=\"TTresetPwdMessage\"></div>    <div class=\"TTclearN\"></div>    <form id=\"TTforgotPwdForm\">    <div>        <input type=\"text\" name=\"TTloginUsername\" id=\"TTloginUsername\" ttInputLabelText=\"Email Address\"               value=\"Email Address\"               onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\" class=\"TTdefaultInput\"               style=\"float: left; margin-top: 3px\"/>        <input type=\"button\" id=\"TTresetPwdBtn\" value=\"Reset Password\" class=\"TTsubmitButton\"/>    </div>    </form>    <div class=\"TT-ui-helper-clearfix\"></div>    <!-- <div class=\"TTclearN\"></div> --></div>');}return p.join('');"),forwardRow:new Function("obj","var p=[];with(obj){p.push('<div class=\"TTmatchDetail\" style=\"display: block;\">    <div class=\"TTbullet\">');if (m.url) {p.push('        <a class=\"TTtransactionItemLink\" href=\"', matchLinkRoot ,'&pHref=', encodeURIComponent(m.url) ,'&pd=&v=x&mId=', m.id ,'&tId=0&tiId=', m.id ,'\">', m.title ,'</a>');} else {p.push('    <i>', m.title ,'</i>');}p.push('        '); if (m.itemCount > 1) { p.push('            <span style=\"font-size:10px; padding-left:2px;\">x',m.itemCount,'</span>        '); } p.push('    </div></div>');}return p.join('');"),importContactsForm:new Function("obj","var p=[];with(obj){p.push('<div id=\"tmpImport\" style=\"width:85%\">    <ul id=\"TTSubDialogErrors\"></ul>    <form id=\"TTimportForm\" action=\"javascript:void(0)\">        '); var isEmailSrc = (importSrc.type == 1); p.push('        <input type=\"hidden\" id=\"TTimportSrc\" value=\"', importSrc.id ,'\"/>        <input type=\"text\" id=\"TTimportName\" name=\"TTimportName\" style=\"',isEmailSrc? 'width:75%' : 'width:100%','\"               ttInputLabelText=\"', importSrc.identType == 1 ? 'E-mail' : 'Username' ,'\"               value=\"', importSrc.identType == 1 ? 'E-mail' : 'Username' ,'\"               onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\" class=\"TTdefaultInput\"/>        '); if (isEmailSrc) { p.push('        @', importSrc.domain ,'        '); } p.push('        <br/>        <input type=\"password\" id=\"TTimportPwd\" name=\"TTimportPwd\"               style=\"display:none;',isEmailSrc? 'width:75%' : 'width:100%','\"               ttPasswordPair=\"TTimportPwdTxt\"               onblur=\"TurnTo.inputLoseFocus(this)\"/>        <input type=\"text\" id=\"TTimportPwdTxt\" name=\"TTimportPwdTxt\" class=\"TTdefaultInput\" value=\"Password\" style=\"',isEmailSrc? 'width:75%' : 'width:100%','\"               ttPasswordPair=\"TTimportPwd\"               onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\"/>        <br/>        <div style=\"float: right;height:50px\">            <input type=\"submit\" id=\"TTimportSubmit\" class=\"TTsubmitButton\"                   style=\"background-color: #4e3790; width: 52px; margin-bottom: 3px; border-color:#4e3790\" value=\"Submit\"/>            <img id=\"TTimportSubmitLoading\" src=\"',getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"                 style=\"border: 1px solid rgb(204, 204, 204); padding: 1px 17px; display: none;\"/>        </div>        <span style=\"color:#666666;font-size:10px\">            This information is used only to connect you with the people you know. We won&#39;t save your password or spam your friends. Promise.        </span>    </form></div>');}return p.join('');"),importSourceSelect:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2importSourceSelect\" style=\"padding: 10px 0;overflow:auto\">    <div style=\"margin-bottom:5px;\">        <div style=\"font-size:18px;font-weight:bold;padding-bottom:10px;\">            ');if (headerText) {p.push('                ',headerText,'            ');} else {p.push('                ');if (matches.user) {p.push('                    Use other friend lists to check for more friends                ');} else {p.push('                    Check for friends                ');}p.push('            ');}p.push('        </div>        <div id=\"TTtip-whysafe2-tip\" class=\"TTtip\">We will never contact your friends or provide their contact info to others. For the long-winded version, see our privacy policy at www.turnto.com/privacy. </div>    </div>    ', TurnToHTML._connectAndSourceSelector({matches:matches, getUrl:getUrl, fbButtonSrc:fbButtonSrc, showHistory:true, showConfirm:showConfirm, hideFacebook:hideFacebook}) ,'     <div style=\"clear:both\"></div>    <div style=\"float:right;margin-right:30px;margin-top:-15px;\">        <input type=\"submit\" class=\"TT2ppDoneBtnClass\"  id=\"TTimportSourceSkip\" value=\"Done\">    </div>    <div class=\"TT-ui-helper-clearfix\"></div>    <!-- <div class=\"TTclearN\"></div> --></div>');}return p.join('');"),importSourceSelectMain:new Function("obj","var p=[];with(obj){p.push('<div style=\"padding: 10px 0;\">    <div style=\"font-size:12px; margin-bottom:10px;\">    '); var friendCount = TurnTo.matchData.counts.friendCount; p.push('    '); if (friendCount == 0) { p.push('        No friend matches. But you\\'re not checking for any friends, yet.    '); } else if (friendCount < 50) { p.push('        No friend matches. But you\\'re only checking for ',friendCount,' friend');if(friendCount > 1){p.push('s');}p.push('.    '); } else { p.push('    '); } p.push('    </div>    ', TurnToHTML._connectAndSourceSelector({showHistory:false,individualOpt:true}) ,'    <div class=\"TT-ui-helper-clearfix\"></div>    <!-- <div class=\"TTclearN\"></div> --></div>');}return p.join('');"),inlineAnswerInputBox:new Function("obj","var p=[];with(obj){p.push('');var qarid = qid + (aid?'-' + aid : '') + (replyToUid ? '-' + replyToUid : '');p.push('<div id=\"TT2answersBlock-',qarid,'\" class=\"TT2answersBlock\" style=\"',!show ? 'display:none;':'','',aid?'padding:0':'','\">    <input type=\"hidden\" name=\"TT2replyToUid\" id=\"TT2replyToUid-',qarid,'\" value=\"',replyToUid,'\"/>    <textarea id=\"TT2inlineAnswer-',qarid,'\" name=\"TT2inlineAnswer-',qarid,'\"              ttinputlabeltext=\"',defaultText,'\"              onfocus=\"TurnTo.inputFocus(this, TurnTo.showInlineAnswerInput)\"              class=\"TTdefaultInput\"              onblur=\"TurnTo.inputLoseFocus(this, TurnTo.hideInlineAnswerInput)\"              style=\"width:',replyInput ? (related ? '345' : '535'):(related ? '381' : '571'),'px;height:20px;margin:0 0 2px 0;\">',defaultText,'</textarea>    <div id=\"TT2anwerCharCount-',qarid,'\" class=\"TT2charLimitCount TT2right\"></div>    <div id=\"TT2error-',qarid,'\" class=\"TT2right\" style=\"color:red\"></div>    <div class=\"TT2clearBoth\"></div>    <div id=\"TT2answerBtns-',qarid,'\" style=\"display:none\">        <div style=\"display:none\">            <input type=\"checkbox\" id=\"TT2answerNotify-',qarid,'\" checked=\"true\"/> Tell me when other people answer or comment on this.        </div>        <div>            <span style=\"float:right;position:relative\">                <a id=\"TT2cancelBtn-',qarid,'\" class=\"TT2cancelBtn\" href=\"javascript:void(0)\"                   style=\"display:block;float:left;margin-right:10px;margin-left:5px;padding-top:8px; color:#314C9B;position:relative;\">Cancel</a>                <img id=\"TT2imgLoading-',qarid,'\" class=\"TT2imgLoading\"                     src=\"',TurnTo.getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"                     style=\"display: none;\">                <img id=\"TT2answerBtn-',qarid,'\" class=\"TT2buttonImg TT2answerBtn\"                     src=\"',TurnTo.getStaticUrl('/tra2/images/submitbttn.png'),'\" alt=\"Submit\"/>            </span>            <span style=\"font-size:10px;\">Do not include: HTML, references to other stores, pricing, contact information (inc. your email &mdash; you can add that later), or inappropriate language. Posts which include these will be edited or removed.</span>        </div>        <div class=\"TT2clearBoth\"></div>    </div>    <div class=\"TT2clearBoth\"></div></div>');}return p.join('');"),inlineAnswerRegFlow:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2questionRegFlow\">    <div style=\"font-size:14px;\">        '); if (regSource == TurnTo.regSourceAskAnswer) { p.push('            We need to know who you are before we post your answer.        '); } else { p.push('            Tell us your email address so we can send you answers.            <br/>            <span style=\"font-size:11px;\">(You will not be added to any mailing lists or receive any spam.)</span>        '); } p.push('    </div>    <div class=\"TT2border\"></div>    <div style=\"background:#E6EBF5;padding:15px;margin-top:15px;\">        <div>            <div>                <span style=\"font-size:12px;font-weight:bold\">Click if you use one of these services - its a quick way to provide a validated email address.</span>                <span style=\"font-size:10px\">(They will ask you to approve a connection to TurnTo, our service provider.)</span>            </div>            <p>&nbsp;</p>        </div>        <div>            <a id=\"TT2askDelAuthLink1\" href=\"javascript:void(0)\" class=\"TT2left\" >facebook</a>            <a id=\"TT2askDelAuthLink2\" class=\"TT2delAuth TT2left\" href=\"javascript:void(0)\">gmail.com</a>            <a id=\"TT2askDelAuthLink3\" class=\"TT2delAuth TT2left\" href=\"javascript:void(0)\">hotmail.com</a>            <a id=\"TT2askDelAuthLink4\" class=\"TT2delAuth TT2left\" href=\"javascript:void(0)\">msn.com</a>        </div>        <div class=\"TT2clearBoth\"></div>        <div style=\"margin-top:10px; background-color:#edf3f7; width:100%;padding-top:10px;text-align:center;font-size:10px;height:25px;border:#d8dfe4 solid 1px;\">            <a id=\"TT2noImport\" class=\"TT2link TT2bold\" href=\"javascript:void(0)\">I don&#39;t use any of these</a>            <span id=\"TT2closeForm\" style=\"display:none;float:right;margin-right:10px;position:relative;\">            x            <a class=\"TT2link\" href=\"javascript:void(0)\">close</a>            </span>        </div>        <div id=\"TT2questionReg\">            <div id=\"TTSubDialogErrors\"></div>            <form id=\"TTregForm\" action=\"javascript:void()\">                <input type=\"hidden\" id=\"TTregSource\" name=\"TTregSource\" value=\"',regSource,'\"/>                <input type=\"hidden\" id=\"TT2regCatItmId\" name=\"TT2regCatItmId\" value=\"',catItmId,'\"/>                <input type=\"hidden\" id=\"TT2regIid\" name=\"TT2regIid\" value=\"',iid,'\"/>                <table id=\"TT2regformTable\">                    <tbody>                    <tr>                        <td><input type=\"text\" name=\"TTfirstName\" id=\"TTfirstName\" ttinputlabeltext=\"First Name\"                                   value=\"First Name\"                                   onfocus=\"TurnTo.inputFocus(this)\"                                   class=\"TTdefaultInput\"                                   onblur=\"TurnTo.inputLoseFocus(this)\"/>                        <input type=\"text\" name=\"TTlastName\" id=\"TTlastName\" ttinputlabeltext=\"Last Name\"                                   value=\"Last Name\"                                   onfocus=\"TurnTo.inputFocus(this)\"                                   onblur=\"TurnTo.inputLoseFocus(this)\"                                   class=\"TTdefaultInput\"/>                        </td>                    </tr>                    <tr>                        <td><input type=\"text\" class=\"TTdefaultInput\"                                   onblur=\"TurnTo.inputLoseFocus(this)\"                                   onfocus=\"TurnTo.inputFocus(this)\"                                   value=\"Email\"                                   ttinputlabeltext=\"Email\" id=\"TTemail\"                                   name=\"TTemail\" style=\"width:412px\"/>                            <div>We hate spam, too. So we won&#39;t send you any.</div>                        </td>                    </tr>                    <tr>                        <td><input type=\"text\" name=\"TTpasswordText\" id=\"TTpasswordText\" class=\"TTdefaultInput\"                                   value=\"Password\"                                   ttpasswordpair=\"TTpassword\" onfocus=\"TurnTo.inputFocus(this)\"                                   onblur=\"TurnTo.inputLoseFocus(this)\">                            <input type=\"password\" name=\"TTpassword\" id=\"TTpassword\" style=\"display: none;\"                                   ttpasswordpair=\"TTpasswordText\" onblur=\"TurnTo.inputLoseFocus(this)\">                            <input type=\"text\" name=\"TTrePasswordText\" id=\"TTrePasswordText\" class=\"TTdefaultInput\"                                   value=\"Confirm Password\" ttpasswordpair=\"TTrePassword\"                                   onfocus=\"TurnTo.inputFocus(this)\"                                   onblur=\"TurnTo.inputLoseFocus(this)\">                            <input type=\"password\" name=\"TTrePassword\" id=\"TTrePassword\" style=\"display: none;\"                                   ttpasswordpair=\"TTrePasswordText\" onblur=\"TurnTo.inputLoseFocus(this)\">                            <div>Optional. So you can access your questions and answers in the future.</div>                        </td>                    </tr>                    <tr>                        <td>                            <div style=\"float:left;margin-right:5px;position:relative;\">Profile Picture</div>                            <div style=\"float:left;margin-right:5px;position:relative;\">                                <img id=\"TT2userImg\" src=\"\" style=\"width:45px;height:45px\"/>                                <img id=\"TT2userImgLoading\" src=\"',TurnTo.getStaticUrl('/images/spinner.gif'),'\"                                     alt=\"Loading...\"                                     style=\"border: 1px solid rgb(204, 204, 204); width:45px;height:45px; display: none;\">                            </div>                            <div id=\"TT2regImgUpload\">                                <input type=\"file\" name=\"TT2userImgFile\" id=\"TT2userImgFile\"                                       onchange=\"TurnTo.inputUserImage(this)\" size=\"35\"/>                            </div>                            <div>                                Optional. It helps to make our store a friendly place to shop, and you are more likely to get                                an answer.</div>                        </td>                    </tr>                    <tr>                        <td><input type=\"text\" name=\"TTcaptcha\" id=\"TTcaptcha\" ttinputlabeltext=\"Please fill in the code on the right for verification\"                                   value=\"Please fill in the code on the right for verification\"                                   onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\"                                   class=\"TTdefaultInput\"                                   style=\"float: left;width:320px;position:relative;\">                            <img src=\"', TurnTo.getUrl('/captcha/index?tt='+(new Date().getTime())) ,'\"                                 style=\"margin: -2px 7px; float: left; vertical-align: middle; opacity: 0.65;position:relative;\"></td>                    </tr>                    </tbody>                </table>                <div style=\"margin-top: 7px;text-align:right\">                    <input type=\"submit\" id=\"TTregSubmit\" class=\"TTsubmitButton\" style=\"height:27px;width:89px;\" value=\"\">                    <img id=\"TTregSubmitLoading\" src=\"',TurnTo.getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"                         style=\"border: 1px solid rgb(204, 204, 204); padding: 1px 23px; display: none;\">                </div>            </form>        </div>        <div style=\"margin-top:10px\">Done this before? <a id=\"TT2login\" class=\"TT2link TT2bold\" href=\"javascript:void(0)\">Log in</a>        </div>    </div></div>');}return p.join('');"),inviteForwardMatches:new Function("obj","var p=[];with(obj){p.push('<div style=\"margin-bottom:15px; font-size:11px;\">    These people are on <b>your</b> friends list, but you are not on <b>theirs</b>. If you    like, we\\'ll ask them to connect with you.</div><form id=\"TTInviteForwardMatches\" action=\"javascript:void()\">    <!--<textarea style=\"margin-bottom:15px; width:325px;\" name=\"TTinviteMessage\" id=\"TTinviteMessage\" ttInputLabelText=\"Personal message (optional)\"-->              <!--onfocus=\"TurnTo.inputFocus(this, function(inputEl){inputEl.addClass(&#39;TTinviteTextareaBig&#39;).removeClass(&#39;TTinviteTextarea&#39;);})\"-->              <!--onblur=\"TurnTo.inputLoseFocus(this, function(inputEl){inputEl.addClass(&#39;TTinviteTextarea&#39;).removeClass(&#39;TTinviteTextareaBig&#39;);})\" class=\"TTdefaultInput TTinviteTextarea\" >Personal message (optional)</textarea>-->    <div style=\"float:left;\">        <input type=\"submit\" id=\"TTInviteForwardMatchesSubmit\" class=\"TTsubmitButton\" style=\"background-color:#999;\" value=\"Yes, please ask them\">    </div>    <!-- <a href=\"',getUrl('/myConnections'),'\" target=\"_blank\" class=\"TTexternalLink\" style=\"display:block; float:right; font-size:11px; margin-top:4px;\">Send to just some</a> -->    <div class=\"TT-ui-helper-clearfix\"></div>    <!-- <div class=\"TTclearN\"></div> -->    <!--<div style=\"margin-top:15px;\">-->        <!--<a href=\"javascript:void(0)\" target=\"_blank\" id=\"TTInviteMessagePreview\" class=\"TTexternalLink\" style=\"font-size:10px;\">Preview</a>-->    <!--</div>-->    </form>');}return p.join('');"),loggedInTopLeft:new Function("obj","var p=[];with(obj){p.push('<p style=\"font-size:20px;font-weight:bold;padding-left:15px; width:400px\" id=\"TT2liheadercopy\">Some ideas from friends, neighbors, and other customers</p>');}return p.join('');"),loggedOutRegFlow1:new Function("obj","var p=[];with(obj){p.push('<div id=\"tmpImport\" style=\"padding-top:8px; font-size:11px;\">    <div id=\"TT2loginicon1\"></div>    <p style=\"margin-bottom:20px; font-size:13px;font-weight:bold;padding-right:8px;padding-left:8px;\">        ', turnToSettings.text2 ,'    </p>    <div style=\"padding-top:10px;text-align:center;width:198px;\">        <form id=\"TTimportForm1\">            <div style=\"text-align:center;\">                <a href=\"javascript:void(0)\" id=\"TTfbConnect\">                    <img src=\"',fbButtonSrc ,'\" alt=\"Connect\" style=\"border:none\"/>                </a>            </div>            <div class=\"TT-ui-helper-clearfix\"></div>            <!-- <div class=\"TTclearN\"></div> -->        </form>        <p style=\"padding-top:8px;\"><a href=\"javascript:void(0)\" id=\"TT2addFriendsLogoutLink2\" style=\"font-size: 10px; color: #3994D8\">I don&#39;t use Facebook</a></p>    </div></div><div style=\"text-align:center;width:198px;\">    <a id=\"TTsignUpLink2\" href=\"javascript:void(0)\" class=\"TTlogoutLink TTexternalLink\"        style=\"font-weight:bold;font-size: 10px; color: #3994D8\">Log in</a> | <span        style=\"font-weight:bold;font-size: 10px; color: #3994D8;cursor:pointer;\" id=\"TTtipSpecial\"> More Info </span></div>');}return p.join('');"),loggedOutTopLeft:new Function("obj","var p=[];with(obj){p.push('<p style=\"font-size:20px;font-weight:bold;padding-left:15px\">Some ideas from other customers</p><p style=\"font-weight:bold;color:#3994d8;padding-left:15px;margin-top:15px;\">Personalize this! See what your friends bought ></p>    <div style=\"float:left;margin-left:15px;margin-top:5px;\">        <form id=\"TTimportForm4\" >            <div style=\"text-align:center;\">                <a href=\"javascript:void(0)\" id=\"TTfbConnect4\">                    <img src=\"',fbButtonSrc ,'\" alt=\"Connect\" style=\"border:none\"/>                </a>            </div>        </form>    </div><div style=\"float:left;line-height:40px;margin-left:10px;\"><a id=\"TT2addFriendsLogoutLink\" href=\"javascript:void(0)\" style=\"font-size:10px;color:#3994d8;\">I don&#39;t use Facebook</a></div>    ');}return p.join('');"),loginscreen:new Function("obj","var p=[];with(obj){p.push('<div style=\"margin:40px 30px 30px 40px; text-align:left;\"><div style=\"text-align:left;\">    <p class=\"TT2textColor1\" style=\"font-size:22px;font-weight:bold\">Log In.</p>    </div><form id=\"TTloginForm\">    <div id=\"TTloginMessage\"></div>    <div style=\"float:left;\">        <a href=\"javascript:void(0)\" id=\"TTfbConnectLogin\">            <img src=\"',fbButtonSrc ,'\" alt=\"Connect\" style=\"border:none\"/>        </a>    </div>    <div class=\"TTclearN\"></div>    <div style=\"font-size:11px; margin-bottom:10px; margin-top:10px;text-align:left\">or</div>    <table>        <tr>            <td>            <input type=\"text\" name=\"TTloginUsername\" id=\"TTloginUsername\" ttInputLabelText=\"Email\" value=\"Email\"           onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\" class=\"TTdefaultInput\" style=\"margin-right:10px; width:148px;\"/>            </td>            <td>    <input type=\"text\" name=\"TTloginPasswordText\" id=\"TTloginPasswordText\" class=\"TTdefaultInput\" value=\"Password\" style=\"width:148px;\"           ttPasswordPair=\"TTloginPassword\" onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\"/>    <input type=\"password\" name=\"TTloginPassword\" id=\"TTloginPassword\" style=\"display:none;width:148px;\"           ttPasswordPair=\"TTloginPasswordText\" onblur=\"TurnTo.inputLoseFocus(this)\"/>            </td>        </tr>        <tr>            <td><input id=\"TTloginRememberMe\" style=\"vertical-align: bottom; border:none; margin:0; overflow:hidden; height:12px; width:12px;\" type=\"checkbox\" name=\"rememberme\" TABINDEX=3 checked/><label for=\"TTloginRememberMe\" style=\"font-size:11px\"> Remember me</label></td>            <td><a id=\"TTloginForgotPwd\" href=\"javascript:void(0)\"> Forgot Password?</a></td>        </tr>        <tr>            <td></td>            <td></td>            <td>                <input type=\"submit\" id=\"TTloginSubmit\" value=\"Submit\" TABINDEX=\"4\" class=\"TTsubmitButton\" />                <div style=\"width: 66px;height: 26px;background-color:#fff;text-align:center;\"><img id=\"TTloginSpinner\" height=\"26\" src=\"',TurnTo.getStaticUrl('/tra2/images/traspin.gif'),'\" alt=\"Loading...\" style=\"display:none;\"/></div>            </td>        </tr>    </table></form></div>');}return p.join('');"),mainMessage:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2mainMessage\">    <h3>',message,'</h3>    <br/><br/>    <input type=\"button\" id=\"TT2bttn1\" class=\"TT2button\" style=\"font-size:16px;width:150px;height:30px;\" name=\"TT2bttn1\" value=\"',bttn1Label,'\"/>    <input type=\"button\" id=\"TT2bttn2\" class=\"TT2button\" style=\"font-size:16px;width:150px;height:30px;\" name=\"TT2bttn2\" value=\"',bttn2Label,'\"/></div>');}return p.join('');"),myAsk:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2myAsk\" style=\"overflow:auto\">    <h3 style=\"font-weight:bold;font-size:18px;margin-bottom:15px;\">My questions and answers</h3>    ');for (var i = 0; i < questions.length; i++) {p.push('        <a id=\"TT2itemLink-image-',questions[i].catItmId,'\" qid=\"',questions[i].id,'\" class=\"TT2itemLink TT2right\" href=\"javascript:void(0)\" style=\"width:80px;margin-right:10px;\">            <img class=\"TT2right\" src=\"',TurnTo.getProductImage(questions[i].catImgId,'SS'),'\" alt=\"\"/>            <input type=\"hidden\" id=\"TT2catImgId-',questions[i].id,'\" value=\"',questions[i].catImgId,'\"/>        </a>        <div class=\"TT2left\" style=\"width:400px;\">            <div class=\"TT2askedUserPhoto\" style=\"margin-right:0px;\">                <img style=\"display:inline\"                     src=\"', TurnTo.getUserPhoto(questions[i].uid, questions[i].pid) ,'\"                     alt=\"\"/>                ');if (questions[i].isStaff) {p.push('                <img style=\"display: block;height: 22px;width: 45px;margin-top:-3px\" src=\"', TurnTo.getStaticUrl('/tra2/images/stafflabel.png') ,'\" alt=\"\"/>                ');}p.push('            </div>            <div style=\"float:right;width:335px\">                <div style=\"margin-bottom:10px;\">                    ');if (TurnTo.matchData.user.id == questions[i].uid) {p.push('                        <span class=\"TT2right\"><input id=\"TT2shareQuestionBtn-',questions[i].id,'\" qid=\"',questions[i].id,'\"                                                      class=\"TT2shareQuestionBtn  TT2button\"                                                      style=\"font-size:10px;font-weight:bold;color:#FFFFFF;background:#6175A9;border-color:#1D4088\"                                                      type=\"button\" value=\"Share\"/></span>                        <input id=\"TT2rawQuestionText-',questions[i].id,'\" type=\"hidden\" value=\"',questions[i].rawText,'\"/>                    ');}p.push('                    <span class=\"TT2bold\">',TurnTo.matchData.user.id == questions[i].uid ? 'You': questions[i].fName + ' ' + (questions[i].lName ? questions[i].lName.substring(0, 1) : ''),' asked about <a id=\"TT2itemLink-title-',questions[i].catItmId,'\" qid=\"',questions[i].id,'\" class=\"TT2itemLink\" href=\"javascript:void(0)\">',TurnTojQuery.trim(questions[i].itmTitle),'</a>: </span>                    <span>',questions[i].text,'</span>                </div>                ');if (questions[i].answers.length == 0) {p.push('                <div class=\"TT2answersBlock\" style=\"min-height:30px;\">                    <div style=\"margin-top:5px;\">                        <span>No answers yet.</span>                    </div>                    <div class=\"TT2clearBoth\"></div>                </div>                ');} else {p.push('                    ');for (var s = 0; s < questions[i].answers.length; s++) {                    var answer = questions[i].answers[s];                    p.push('                    <input type=\"hidden\" id=\"TT2qid4answer-',answer.id,'\" value=\"',questions[i].id,'\"/>                    ',TurnToHTML.answerBlock({answerer:{uid:answer.uid, pid:answer.pid, fName:answer.fName,                    lName:answer.lName, isStaff:answer.isStaff}, answerText:answer.text, rawAnswerText:answer.rawText, aid:answer.id, qid:questions[i].id, quid:questions[i].uid, replys:answer.replys, related:false, showReply:false}),'                    ');}p.push('                ');}p.push('            </div>            <div class=\"TT2clearBoth\"></div>        </div>        <div class=\"TT2clearBoth\"></div>        <div style=\"border-bottom:1px solid #CCCCCC;margin-bottom:10px;margin-top:10px;\"></div>    ');}p.push('</div>');}return p.join('');"),pastPurchases:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2pastPurchasesHeader\">    <div>            '); if(TurnTo.newfbuserExitMessage || TurnTo.noSharingPrefExitFlow || TurnTo.postPurchaseFlowStarted){ p.push('<span class=\"TT2ppReminderText\">Hi ',TurnTo.postPurchaseFlowStarted ? TurnTojQuery.trim(TurnTo.turnToFeed.firstName) : matches.user.firstName,'.            <br/>Before you go... ', TurnTo.customCopy.pswHeader ,'</span>                    '); } else { p.push('<span class=\"TT2ppReminderText\">',matches.user ? matches.user.firstName : TurnTo.matchData.siteUser ? TurnTo.matchData.siteUser.firstName : 'Hi',', ', TurnTo.customCopy.pswHeader ,'    ');if (TurnTo.matchData.invitor) {p.push('    <div style=\"background:#ffffd7;border: #ddd solid 1px;padding:4px;\">We&#39;ll also send a note to        ',TurnTo.matchData.invitor.firstName,' letting them know you can help.    </div>    ');}p.push('            </span>                    '); } p.push('        <p style=\"padding-top:8px;\">        <a href=\"javascript:void(0)\" id=\"TT2pastPurchasesTips\">(How does this work?)</a>        </p>    </div>    <div class=\"TT2pastPurchasesTipsDesc\" style=\"display:none\">       <a class=\"TTclosexyello\" href=\"javascript:void(0)\" id=\"TT2pastPurchasesTipsDismiss\" style=\"float:left;margin-right:5px\"></a>       <p style=\"line-height:17px;\">        When your friends shop here, if they click our        \"Get advice from friends\" button, they will be able to see your full name,        your photo (if you have one), and the items you chose here (no prices).        Shoppers who are not your friends will see your first name last initial, your photo (if you have one),        and the items you chose here (no prices).       </p>     </div>    <div class=\"TT2ppBtnGroup\">        <input id=\"TT2managePastPurchaseBtn\" type=\"button\" value=\"Sure!\"/>        <img id=\"TT2managePastPurchaseLoading\" src=\"',getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"             style=\"border: 1px solid rgb(204, 204, 204); display: none; width:45px; height: 45px\">        <a href=\"javascript:void(0)\" id=\"TT2NotManagePastPurchaseLink\">Rather not</a>    </div></div><div id=\"TT2pastPurchases\">    <div id=\"TT2pastPurchasesDisplay\">        ');var items = matches.items4Conf ? matches.items4Conf : matches.user.items;        var containsNewOrder = false;        var containsOldOrder = false;        for (var i = 0; i < items.length; i++) {            if (!containsNewOrder && items[i].newOrder) {                containsNewOrder = true;            } else if (!containsOldOrder && !items[i].newOrder) {                containsOldOrder = true;            } else if (containsNewOrder && containsOldOrder) {            }        }        p.push('        ');if (items && items.length > 0) {p.push('            ');if (containsNewOrder && containsOldOrder) {                var previousPurchases = false;p.push('            <div>Current Purchases:</div>            <table>                ');for (var i = 0; i < items.length; i++) {                    if (!items[i].newOrder) {                        previousPurchases = true;                        continue;                    }p.push('                <tr>                    <td class=\"TT2ppImage\">                        <a class=\"TT2ppName\" href=\"javascript:void(0)\" onclick=\"window.open(&#39;',items[i].url,'&#39;)\">                            <img src=\"', TurnTo.getProductImage(items[i].catImgId, 'S') ,'\" alt=\"\"/>                        </a>                    </td>                    <td class=\"TT2ppDescription\">                        <a class=\"TT2ppName\" href=\"javascript:void(0)\" onclick=\"window.open(&#39;',items[i].url,'&#39;)\">',items[i].title,'</a>                        <br/>                        Purchased on <span class=\"TT2ppDate\">',items[i].tDate,'</span>                    </td>                </tr>                ');}p.push('            </table>            ');if (previousPurchases) {p.push('                <div>Previous Purchases:</div>                <table>                    ');for (var i = 0; i < items.length; i++) {                        if (items[i].newOrder) {                            continue;                        }p.push('                    <tr>                        <td class=\"TT2ppImage\">                            <a class=\"TT2ppName\" href=\"javascript:void(0)\" onclick=\"window.open(&#39;',items[i].url,'&#39;)\">                                <img src=\"', TurnTo.getProductImage(items[i].catImgId, 'S') ,'\" alt=\"\"/>                            </a>                        </td>                        <td class=\"TT2ppDescription\">                            <a class=\"TT2ppName\" href=\"javascript:void(0)\" onclick=\"window.open(&#39;',items[i].url,'&#39;)\">',items[i].title,'</a>                            <br/>                            Purchased on <span class=\"TT2ppDate\">',items[i].tDate,'</span>                        </td>                    </tr>                    ');}p.push('                </table>            ');}p.push('            ');} else {p.push('            <div>Your Purchases:</div>            <table>                ');for (var i = 0; i < items.length; i++) {p.push('                <tr>                    <td class=\"TT2ppImage\">                        <a class=\"TT2ppName\" href=\"javascript:void(0)\" onclick=\"window.open(&#39;',items[i].url,'&#39;)\">                            <img src=\"', TurnTo.getProductImage(items[i].catImgId, 'S') ,'\" alt=\"\"/>                        </a>                    </td>                    <td class=\"TT2ppDescription\">                        <a class=\"TT2ppName\" href=\"javascript:void(0)\" onclick=\"window.open(&#39;',items[i].url,'&#39;)\">',items[i].title,'</a>                        <br/>                        Purchased on <span class=\"TT2ppDate\">',items[i].tDate,'</span>                    </td>                </tr>                ');}p.push('            </table>            ');}p.push('        ');}p.push('    </div></div>');}return p.join('');"),pastPurchasesManagement:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2pastPurchasesHeader\" style=\"padding-bottom:10px;\">        <span class=\"TT2ppReminderText\">            ');if (orderConf && !TurnTo.newSharePastPurchases) {p.push('                ',matches.user.firstName,', thanks for your purchase. Now tell people why you chose these items to help them make decisions.            ');} else {p.push('                ',TurnTo.newSharePastPurchases ? 'Thanks ' + matches.user.firstName + '. Now ' : matches.user.firstName + ',',' tell people why you chose these items to help them make decisions.                ');TurnTo.newSharePastPurchases = false;p.push('            ');}p.push('        </span></div><div id=\"TT2pastPurchases\">    <div id=\"TT2pastPurchasesDisplay\">        ');var items = matches.items4Conf ? matches.items4Conf : matches.user.items;        var containsNewOrder = false;        var containsOldOrder = false;        for (var i = 0; i < items.length; i++) {            if (!containsNewOrder && items[i].newOrder) {                containsNewOrder = true;            } else if (!containsOldOrder && !items[i].newOrder) {                containsOldOrder = true;            } else if (containsNewOrder && containsOldOrder) {            }        }        p.push('        ');if (containsNewOrder && containsOldOrder) {            var previousPurchases = false;p.push('            <div>Current Purchases:</div>            <table>            ');if (items) {                for (var i = 0; i < items.length; i++) {                    if (!items[i].newOrder) {                        previousPurchases = true;                        continue;                    }p.push('                <tr>                    <td class=\"TT2ppImage\">                        <a href=\"javascript:void(0)\" onclick=\"window.open(&#39;',items[i].url,'&#39);\">                            <img src=\"', TurnTo.getProductImage(items[i].catImgId, 'S') ,'\" alt=\"\"/>                        </a>                    </td>                    <td class=\"TT2ppDescription\">                        <input type=\"hidden\" name=\"TT2stiIds\" value=\"',items[i].id,'\"/>                        <span class=\"TT2ppName\">                            <a href=\"javascript:void(0)\" onclick=\"window.open(&#39;',items[i].url,'&#39;)\">',items[i].title,'</a>                        </span>                        <br/>                        Purchased on <span class=\"TT2ppDate\">',items[i].tDate,'</span>                        <br/>                        <br/>                        <div class=\"TT2commentArea\">                            <span class=\"TT2bold\">Why did you choose this?</span>                            <span class=\"TT2limitReminder\"><br/>(<span class=\"TT2charLimitCount\">500</span> characters left)</span>                            <br/>                            ');if (items[i].raveComment) {p.push('                                <textarea class=\"TT2ppComment\" style=\"display:none\">',items[i].raveComment,'</textarea>                                <p class=\"TT2ppSavedComment\">\"',items[i].raveComment,'\"</p>                            ');} else {p.push('                                <textarea class=\"TT2ppComment\"></textarea>                                <p class=\"TT2ppSavedComment\" style=\"display:none\"></p>                            ');}p.push('                            <div class=\"TT2postCheckboxGroup\">                                <div style=\"margin:0 5px 0 0\">Also post to </div>                                <table>                                    <tr>                                        <td style=\"white-space:nowrap;\">                                            <input type=\"checkbox\" name=\"TT2facebookBox\" value=\"true\"/>                                            <img src=\"',getStaticUrl('/tra2/images/smallfbicon.png'),'\" alt=\"Facebook\"/>                                        </td>                                        <td style=\"white-space:nowrap;\">                                            <input type=\"checkbox\" name=\"TT2twitterBox\" value=\"true\" ',connectedTwitter ? 'checked=\"true\"' : '','/>                                            <img src=\"',getStaticUrl('/tra2/images/smalltwittericon.png'),'\" alt=\"Twitter\"/>                                        </td>                                    </tr>                                </table>                            </div>                            <img class=\"TT2submitImgLoading\" src=\"',getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"                                style=\"width:25px;height:25px; display: none; float:right\">                            <input class=\"TT2ppSaveCommentButton\" type=\"button\" value=\"Submit\"/>                            <a href=\"javascript:void(0)\" class=\"TT2ppDelCommentLink\">Delete</a>                            <a href=\"javascript:void(0)\" class=\"TT2ppEditCommentLink\">Edit</a>                        </div>                    </td>                </tr>                ');}p.push('            ');}p.push('            </table>            ');if (previousPurchases) {p.push('            <div>Previous Purchases:</div>            <table>            ');if (items) {                for (var i = 0; i < items.length; i++) {                    if (items[i].newOrder) {                        continue;                    }p.push('                <tr>                    <td class=\"TT2ppImage\">                        <a href=\"javascript:void(0)\" onclick=\"window.open(&#39;',items[i].url,'&#39);\">                            <img src=\"', TurnTo.getProductImage(items[i].catImgId, 'S') ,'\" alt=\"\"/>                        </a>                    </td>                    <td class=\"TT2ppDescription\">                        <input type=\"hidden\" name=\"TT2stiIds\" value=\"',items[i].id,'\"/>                        <span class=\"TT2ppName\">                            <a href=\"javascript:void(0)\" onclick=\"window.open(&#39;',items[i].url,'&#39;)\">',items[i].title,'</a>                        </span>                        <br/>                        Purchased on <span class=\"TT2ppDate\">',items[i].tDate,'</span>                        <br/>                        <br/>                        <div class=\"TT2commentArea\">                            <span class=\"TT2bold\">Why did you choose this?</span>                            <span class=\"TT2limitReminder\"><br/>(<span class=\"TT2charLimitCount\">500</span> characters left)</span>                            <br/>                            ');if (items[i].raveComment) {p.push('                                <textarea class=\"TT2ppComment\" style=\"display:none\">',items[i].raveComment,'</textarea>                                <p class=\"TT2ppSavedComment\">\"',items[i].raveComment,'\"</p>                            ');} else {p.push('                                <textarea class=\"TT2ppComment\"></textarea>                                <p class=\"TT2ppSavedComment\" style=\"display:none\"></p>                            ');}p.push('                            <div class=\"TT2postCheckboxGroup\">                                <div style=\"margin:0 5px 0 0\">Also post to </div>                                <table>                                    <tr>                                        <td style=\"white-space:nowrap;\">                                            <input type=\"checkbox\" name=\"TT2facebookBox\" value=\"true\"/>                                            <img src=\"',getStaticUrl('/tra2/images/smallfbicon.png'),'\" alt=\"Facebook\"/>                                        </td>                                        <td style=\"white-space:nowrap;\">                                            <input type=\"checkbox\" name=\"TT2twitterBox\" value=\"true\" ',connectedTwitter ? 'checked=\"true\"' : '','/>                                            <img src=\"',getStaticUrl('/tra2/images/smalltwittericon.png'),'\" alt=\"Twitter\"/>                                        </td>                                    </tr>                                </table>                            </div>                            <img class=\"TT2submitImgLoading\" src=\"',getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"                                style=\"width:25px;height:25px; display: none; float:right\">                            <input class=\"TT2ppSaveCommentButton\" type=\"button\" value=\"Submit\"/>                            <a href=\"javascript:void(0)\" class=\"TT2ppDelCommentLink\">Delete</a>                            <a href=\"javascript:void(0)\" class=\"TT2ppEditCommentLink\">Edit</a>                        </div>                    </td>                </tr>                ');}p.push('            ');}p.push('            </table>            ');}p.push('        ');} else {p.push('            <div>Your Purchases:</div>            <table>                ');if (items) {                    for (var i = 0; i < items.length; i++) {p.push('                    <tr>                        <td class=\"TT2ppImage\">                            <a href=\"javascript:void(0)\" onclick=\"window.open(&#39;',items[i].url,'&#39);\">                                <img src=\"', TurnTo.getProductImage(items[i].catImgId, 'S') ,'\" alt=\"\"/>                            </a>                        </td>                        <td class=\"TT2ppDescription\">                            <input type=\"hidden\" name=\"TT2stiIds\" value=\"',items[i].id,'\"/>                            <span class=\"TT2ppName\">                                <a href=\"javascript:void(0)\" onclick=\"window.open(&#39;',items[i].url,'&#39;)\">',items[i].title,'</a>                            </span>                            <br/>                            Purchased on <span class=\"TT2ppDate\">',items[i].tDate,'</span>                            <br/>                            <br/>                            <div class=\"TT2commentArea\">                                <span class=\"TT2bold\">Why did you choose this?</span>                                <span class=\"TT2limitReminder\"><br/>(<span class=\"TT2charLimitCount\">500</span> characters left)</span>                                <br/>                                ');if (items[i].raveComment) {p.push('                                    <textarea class=\"TT2ppComment\" style=\"display:none\">',items[i].raveComment,'</textarea>                                    <p class=\"TT2ppSavedComment\">\"',items[i].raveComment,'\"</p>                                ');} else {p.push('                                    <textarea class=\"TT2ppComment\"></textarea>                                    <p class=\"TT2ppSavedComment\" style=\"display:none\"></p>                                ');}p.push('                                <div class=\"TT2postCheckboxGroup\">                                    <div style=\"margin:0 5px 0 0\">Also post to </div>                                    <table>                                        <tr>                                            <td style=\"white-space:nowrap;\">                                                <input type=\"checkbox\" name=\"TT2facebookBox\" value=\"true\"/>                                                <img src=\"',getStaticUrl('/tra2/images/smallfbicon.png'),'\" alt=\"Facebook\"/>                                            </td>                                            <td style=\"white-space:nowrap;\">                                                <input type=\"checkbox\" name=\"TT2twitterBox\" value=\"true\" ',connectedTwitter ? 'checked=\"true\"' : '','/>                                                <img src=\"',getStaticUrl('/tra2/images/smalltwittericon.png'),'\" alt=\"Twitter\"/>                                            </td>                                        </tr>                                    </table>                                </div>                                <img class=\"TT2submitImgLoading\" src=\"',getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"                                    style=\"width:25px;height:25px; display: none; float:right\">                                <input class=\"TT2ppSaveCommentButton\" type=\"button\" value=\"Submit\"/>                                <a href=\"javascript:void(0)\" class=\"TT2ppDelCommentLink\">Delete</a>                                <a href=\"javascript:void(0)\" class=\"TT2ppEditCommentLink\">Edit</a>                            </div>                        </td>                    </tr>                    ');}p.push('                ');}p.push('            </table>        </div>    ');}p.push('    <div id=\"TT2doneGroup\" class=\"TT2ppBtnGroup\">        <input id=\"TT2ppDoneBtn\" type=\"button\" value=\"Done\"/>    </div>    <div id=\"TT2remindGroup\">        Your comments will not be saved unless you click \"Submit\".        <div class=\"TT2ppBtnGroup\">            <input id=\"TT2goBackCommentBtn\" type=\"button\" value=\"Go Back\"/>            <input id=\"TT2ignoreCommentBtn\" type=\"button\" value=\"Continue\"/>        </div>    </div>    ');if (!orderConf) {p.push('    <div>        <a href=\"javascript:void(0)\" id=\"TT2seeMorePurchases\">Don&#39;t see all of your purchases?</a>    </div>    <div id=\"TT2doneAddedEmail\">        Got it. Before we check for purchases, please confirm those emails are really yours.        Go to you email in-box, look for the confirmation message, and click on the link.        (If you don&#39;t receive the confirmation message within a couple minutes, please check your spam filter.)    </div>    <div id=\"TT2addtionalEmail\">        <div id=\"TT2emailList\">            ');if (idents) {p.push('                ');for (var i = 0; i < idents.length; i++) {                    if (idents[i].type == 1 && idents[i].confirmed) {p.push('                        ',idents[i].ident,'                        <br/>                ');  }                  }p.push('                ');for (var i = 0; i < idents.length; i++) {                    if (idents[i].type == 1 && !idents[i].confirmed) {p.push('                        ',idents[i].ident,' - Unconfirmed                        <a style=\"color:#003399\" onclick=\"TurnTo.resendEmailConfirmation(&#39;',idents[i].ident,'&#39;)\" href=\"javascript:void(0)\">(Resend confirmation email)</a>                        <br/>                ');  }                  }p.push('            ');}p.push('        </div>        Maybe you used a different email for some? Enter it here:        <br/>        <div id=\"TTSubDialogErrors\" style=\"margin:0;padding:0\"></div>        <input type=\"input\" name=\"TT2email\" id=\"TT2email\" />        <input id=\"TT2addEmailBtn\" type=\"button\" value=\"Submit\"/>    </div>    ');}p.push('</div>');}return p.join('');"),popularRow:new Function("obj","var p=[];with(obj){p.push('<div class=\"TTmatchDetail TTraveArea\" style=\"display: block;\">    <div class=\"TTbullet\">        ');if (m.url) {p.push('        <a class=\"TTtransactionItemLink\"           href=\"', matchLinkRoot ,'&pHref=', encodeURIComponent(m.url) ,'&pd=&v=', TurnTo.version  ,'&mId=', m.id ,'&tId=0&tiId=', m.id ,' \">            ', m.title ,'</a>            ');} else {p.push('            <i>', m.title ,'</i>        ');}p.push('        '); if ( m.raveCommentCount >0) { p.push('        <img class=\"TTraveLink\" src=\"', TurnTo.getStaticUrl('/tra/images/starwithdoc.png'),'\" border=\"0\"             style=\"padding-left:0\"/>        '); } else if (m.raveCount > 0){ p.push('        <img class=\"TTraveLink\" src=\"', TurnTo.getStaticUrl('/tra/images/star.png'),'\" border=\"0\" style=\"padding-left:0\"/>        '); } p.push('        <div style=\"display: none;\" class=\"TTraveOuterBox\" sku=\"', m.sku ,'\">            <div class=\"TTraveTitle\"><img src=\"', TurnTo.getStaticUrl('/tra/images/star.png'),'\" border=\"0\"/>                ',m.raveCount,' Recommendation',m.raveCount==1?'':'s',' '); if(m.raveCommentCount>0){ p.push(' | ',m.raveCommentCount,' Comment',m.raveCommentCount==1?'':'s',' '); }p.push('            </div>            '); if ( m.raveCommentCount >0) { p.push('            <div class=\"TTraveCommentBox\"></div>            '); } p.push('                          </div>    </div></div>');}return p.join('');"),postRegInfo:new Function("obj","var p=[];with(obj){p.push('<ul id=\"TTSubDialogErrors\"></ul><div id=\"TT2regFormDesc\">To help us make our store a friendly place to shop:<br/><br/></div><form id=\"TTregForm\" action=\"javascript:void()\">    <ol style=\"list-style-type: none; margin-left: 20px;\">        <li id=\"TT2postRegPhoto\" style=\"margin-bottom: 20px;\">            Be sure to add a photo of yourself.<br/>            <div style=\"float:left;margin-right:5px\">                <img id=\"TT2userImg\" src=\"',TurnTo.getFacebookUserPhoto(),'\" style=\"width:45px;height:45px\"/>                <img id=\"TT2userImgLoading\" src=\"',getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"                     style=\"border: 1px solid rgb(204, 204, 204); width:45px;height:45px; display: none;\">            </div>            <div id=\"TT2regImgUpload\">                <input type=\"file\" name=\"TT2userImgFile\" id=\"TT2userImgFile\"                       onchange=\"TurnTo.inputUserImage(this)\"/><br/>                Select an image file on your computer (4MB max)            </div>            Or  <a href=\"javascript:void(0)\" id=\"TTpostRegFbConnect\">                <img src=\"',fbButtonSrc ,'\" alt=\"Connect\" style=\"border:none;vertical-align:middle;\"/>                </a> to use your Facebook photo        </li>        <li style=\"margin-bottom: 20px;\">            Create a password so you can add or edit your comments in the future.            <table>                <tr>                    <td style=\"font-size:12px;padding-right:2px\">Password </td>                    <td><input type=\"text\" name=\"TTpasswordText\" id=\"TTpasswordText\" class=\"TTdefaultInput\" value=\"Password\"                       ttpasswordpair=\"TTpassword\" onfocus=\"TurnTo.inputFocus(this)\"                       onblur=\"TurnTo.inputLoseFocus(this)\">                <input type=\"password\" name=\"TTpassword\" id=\"TTpassword\" style=\"display: none;\"                       ttpasswordpair=\"TTpasswordText\" onblur=\"TurnTo.inputLoseFocus(this)\"></td>                </tr>                <tr>                    <td style=\"font-size:12px;padding-right:2px\">Confirm </td>                    <td><input type=\"text\" name=\"TTrePasswordText\" id=\"TTrePasswordText\" class=\"TTdefaultInput\"                       value=\"Confirm Password\" ttpasswordpair=\"TTrePassword\" onfocus=\"TurnTo.inputFocus(this)\"                       onblur=\"TurnTo.inputLoseFocus(this)\">                <input type=\"password\" name=\"TTrePassword\" id=\"TTrePassword\" style=\"display: none;\"                       ttpasswordpair=\"TTrePasswordText\" onblur=\"TurnTo.inputLoseFocus(this)\"></td>                </tr>            </table>            Or just <a href=\"javascript:void(0)\" id=\"TTpostRegFbConnect2\">                <img src=\"',fbButtonSrc ,'\" alt=\"Connect\" style=\"border:none;vertical-align:middle;\"/>                </a>        </li>    </ol>        <div style=\"margin-top: 7px;float:right\">        <input type=\"submit\" id=\"TTregSubmit\" class=\"TT2doneSub\" value=\"\" style=\"width:75px; height:27px;background:url(',getStaticUrl('/tra2/images/donebttn.png'),') no-repeat scroll left center #FFFFFF;\"/>        <img id=\"TTregSubmitLoading\" src=\"',getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"             style=\"border: 1px solid rgb(204, 204, 204); padding: 1px 23px; display: none;\">    </div></form>');}return p.join('');"),postToFacebookPanel:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2postToFacebookPanel\">    <div style=\"font-weight:bold;text-align:center;margin-bottom:18px;position:relative;\">        <span class=\"TT2inlineMessage\" style=\"display:inline;color:#777777;padding:6px\">Your question has been submitted.</span>    </div>    <div style=\"font-weight:bold;text-align:center;font-size:16px;position:relative;\">        Want advice from your friends, too?        <br/>        Post this question to your Facebook wall.    </div>    <div style=\"margin-top:18px;\">        <div class=\"TT2left\" style=\"margin-left:20px;margin-right:20px\">            <img class=\"TT2productImg\" src=\"',TurnTo.getProductImage(catImgId,'SS'),'\"/>        </div>        <div class=\"TT2left\" style=\"margin-left:2px\">            <div id=\"TT2fbError\" style=\"color:red\"></div>            <textarea id=\"TT2postToFbTextArea\" style=\"width:378px\">',questionText,'</textarea>            <div style=\"width:384px;margin-top:6px;\">                <div class=\"TT2left\">                    <span style=\"font-size:10px;color:#777777\">Click in the box to modify your question</span>                    </div>                <div id=\"TT2fbBtns\" class=\"TT2right\">                    <input id=\"TT2postToFbBtn\" style=\"font-size:10px;font-weight:bold;color:#FFFFFF;background:#6175A9;border-color:#1D4088\" type=\"button\" value=\"Post to Facebook\"/>                    <input id=\"TT2cancelToFbBtn\" style=\"font-size:10px;font-weight:bold\" type=\"button\" value=\"No Thanks\"/>                    <img id=\"TT2fbLoading\" style=\"display:none\" class=\"TT2imgLoading\" src=\"',TurnTo.getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\">                </div>            </div>        </div>        <div class=\"TT2clear\"></div>    </div></div>');}return p.join('');"),questionAnswerBlock:new Function("obj","var p=[];with(obj){p.push('<div class=\"TT2cqa\">    <div class=\"TT2askedUserPhoto\" style=\"margin-right: 0px;margin-bottom:0px\">        <img style=\"display:inline\" class=\"TT2youPhotoImg\"             src=\"', TurnTo.getUserPhoto(asker.uid, asker.pid) ,'\" alt=\"\"/>        ');if (isStaff) {p.push('            <img style=\"display: block;height: 22px;width: 45px;margin-top:-3px\" src=\"', TurnTo.getStaticUrl('/tra2/images/stafflabel.png') ,'\" alt=\"\"/>        ');}p.push('    </div>    <div style=\"width:590px;float:right;position:relative;\">        <div id=\"TT2questionBlock-',qid,'\" style=\"margin-bottom:10px;\">            <span class=\"TT2right\"><input id=\"TT2shareQuestionBtn-',qid,'\" qid=\"',qid,'\"                                          class=\"TT2shareQuestionBtn  TT2button\"                                          style=\"font-size:10px;font-weight:bold;color:#FFFFFF;background:#6175A9;border-color:#1D4088\"                                          type=\"button\" value=\"Share\"/></span>            <input id=\"TT2rawQuestionText-',qid,'\" type=\"hidden\" value=\"',rawQuestionText,'\"/>            <span class=\"TT2bold\">You asked: </span>            <span>',questionText,'</span>        </div>        ',TurnToHTML.inlineAnswerInputBox({qid:qid, defaultText:\"Enter your answer or comment here\", aid:null, show:true, replyInput:false, related:false, replyToUid:null}),'    </div>    <div class=\"TT2clearBoth\" style=\"margin-bottom:10px;\"></div></div>');}return p.join('');"),questionRegFlow:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2questionRegFlow\">    <div class=\"TT2border\"></div>    <ul style=\"margin-top:0px;margin-bottom:5px;list-style:none;font-size:14px\">        <li style=\"margin-bottom:2px;\"><input type=\"radio\" name=\"TT2questionRegRadio\" id=\"TT2questionRegRadio1\" value=\"1\" checked=\"true\"/>        Here&#39;s my email address.  Send me answers right away. <br/>        <div style=\"font-size:10px;margin-left:15px\">(This will not add you to any mailing lists. No spam.)</div></li>                <li><input type=\"radio\" name=\"TT2questionRegRadio\" id=\"TT2questionRegRadio2\" value=\"2\"/>        I&#39;d rather ask anonymously.  I&#39;ll check back here to see the answers.</li>    </ul>    <div class=\"TT2border\"></div>    <div class=\"TT2questionRegSel2\" style=\"background:#E6EBF5;margin-top:15px;display:none\">        <div style=\"text-align:center;padding:10px\">            <a id=\"TT2questionRegClose\" href=\"javascript:void(0)\"><img style=\"vertical-align:middle;\" src=\"',TurnTo.getStaticUrl('/tra2/images/donebttn.png'),'\" alt=\"Done\"/></a>        </div>    </div>    <div class=\"TT2questionRegSel1\" style=\"background:#E6EBF5;padding:15px;margin-top:15px;\">        <div>            <div>                <span style=\"font-size:12px;font-weight:bold\">Click if you use one of these services - its a quick way to provide a validated email address.</span>                <span style=\"font-size:10px\">(They will ask you to approve a connection to TurnTo, our service provider.)</span>            </div>            <p>&nbsp;</p>        </div>            <div style=\"float:left\"><a id=\"TT2askDelAuthLink1\" href=\"javascript:void(0)\">facebook</a></div>            <div style=\"float:left\"><a id=\"TT2askDelAuthLink2\" class=\"TT2delAuth\" href=\"javascript:void(0)\">gmail.com</a></div>            <div style=\"float:left\"><a id=\"TT2askDelAuthLink3\" class=\"TT2delAuth\" href=\"javascript:void(0)\">hotmail.com</a></div>            <div style=\"float:left\"><a id=\"TT2askDelAuthLink4\" class=\"TT2delAuth\" href=\"javascript:void(0)\">msn.com</a></div>        <div class=\"TT2clearBoth\"></div>    </div>    <div class=\"TT2questionRegSel1\" style=\"background:#E6EBF5;padding:15px;margin-top:10px;\">        <div id=\"TT2questionLogin\" style=\"display:none\">            <div style=\"font-weight:bold\">Login to to use your existing profile</div>            <form id=\"TTloginForm\">                <div id=\"TTloginMessage\" style=\"text-align:left;margin-top:0px;margin-bottom:0;\"></div>                <div style=\"float:left;padding-top:5px;\">                    <input type=\"text\" name=\"TTloginUsername\" id=\"TTloginUsername\" ttInputLabelText=\"Email\" value=\"Email\"                       onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\" class=\"TTdefaultInput\"                       style=\"margin-right:10px; width:148px;\"/>                    <input type=\"text\" name=\"TTloginPasswordText\" id=\"TTloginPasswordText\" class=\"TTdefaultInput\"                       value=\"Password\" style=\"width:148px;\"                       ttPasswordPair=\"TTloginPassword\" onfocus=\"TurnTo.inputFocus(this)\"                       onblur=\"TurnTo.inputLoseFocus(this)\"/>                    <input type=\"password\" name=\"TTloginPassword\" id=\"TTloginPassword\" style=\"display:none;width:148px;\"                       ttPasswordPair=\"TTloginPasswordText\" onblur=\"TurnTo.inputLoseFocus(this)\"/>                    <br/>                    <input id=\"TTloginRememberMe\" style=\"vertical-align: bottom; border:none; margin:0; overflow:hidden; height:12px; width:12px;\" type=\"checkbox\" name=\"rememberme\" checked/> <label for=\"TTloginRememberMe\" style=\"font-size:11px\">Remember me</label>                    <a style=\"margin-left:80px\" id=\"TTloginForgotPwd\" href=\"javascript:void(0)\">Forgot Password?</a>                </div>                <div style=\"float:right\">                    <input type=\"submit\" id=\"TTloginSubmit\" value=\"\" class=\"TTsubmitButton\" style=\"background:url(',TurnTo.getStaticUrl('/tra2/images/submitbttn.png'),') no-repeat scroll left center #FFFFFF;height:28px;width:91px;\" />                    <img id=\"TTloginSpinner\" height=\"26\" src=\"',TurnTo.getStaticUrl('/tra2/images/traspin.gif'),'\" alt=\"Loading...\" style=\"display:none;\"/>                </div>                <div class=\"TT2clearBoth\"></div>            </form>             <div style=\"margin-top:15px;\">Don&#39;t have a login? <a id=\"TT2register\" class=\"TT2link TT2bold\" href=\"javascript:void(0)\">Create One</a></div>        </div>        <div id=\"TT2questionForgotPwd\" style=\"display:none\">            <div style=\"text-align:left;\">                <p style=\"font-size:16px;font-weight:bold\">Forgot Password?</p>            </div>            <div id=\"TTresetPwdMessage\" style=\" float: left;margin-bottom: 0;margin-top: 5px;\"></div>            <div class=\"TTclearN\"></div>            <form id=\"TTforgotPwdForm\">                <div>                    <input type=\"text\" name=\"TT2forgotLoginUsername\" id=\"TT2forgotLoginUsername\" ttInputLabelText=\"Email Address\"                           value=\"Email Address\"                           onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\" class=\"TTdefaultInput\"                           style=\"float: left; margin-top: 3px\"/>                    <input type=\"button\" id=\"TTresetPwdBtn\" value=\"Reset Password\" class=\"TTsubmitButton\"/>                </div>            </form>            <div class=\"TT-ui-helper-clearfix\"></div>        </div>        <div id=\"TT2questionReg\" style=\"display:block\">            <div style=\"margin-bottom:2px;font-weight:bold\">Or do it the old-fashioned way:</div>            <div id=\"TTSubDialogErrors\"></div>            <form id=\"TTregForm\" action=\"javascript:void()\">                <input type=\"hidden\" id=\"TTregSource\" name=\"TTregSource\" value=\"',regSource,'\"/>                <input type=\"hidden\" id=\"TT2regCatItmId\" name=\"TT2regCatItmId\" value=\"',catItmId,'\"/>                <input type=\"hidden\" id=\"TT2regIid\" name=\"TT2regIid\" value=\"',iid,'\"/>                <table id=\"TT2questionRegFormTable\" cellspacing=\"2px\">                    <tbody>                    <tr>                        <td style=\"padding-bottom:10px\"><input type=\"text\" class=\"TTdefaultInput\"                                   onblur=\"TurnTo.inputLoseFocus(this)\"                                   onfocus=\"TurnTo.inputFocus(this)\"                                   value=\"Email\"                                   ttinputlabeltext=\"Email\" id=\"TTemail\"                                   name=\"TTemail\" style=\"width:412px\"/>                        </td>                    </tr>                    <tr>                        <td style=\"padding-bottom:10px\"><input type=\"text\" name=\"TTfirstName\" id=\"TTfirstName\" ttinputlabeltext=\"First Name\"                                   value=\"First Name\"                                   onfocus=\"TurnTo.inputFocus(this)\"                                   class=\"TTdefaultInput\"                                   onblur=\"TurnTo.inputLoseFocus(this)\"/>                        <input type=\"text\" name=\"TTlastName\" id=\"TTlastName\" ttinputlabeltext=\"Last Name\"                                   value=\"Last Name\"                                   onfocus=\"TurnTo.inputFocus(this)\"                                   onblur=\"TurnTo.inputLoseFocus(this)\"                                   class=\"TTdefaultInput\"/>                        <div>Only your first name and last initial will appear with the question.</div>                        </td>                    </tr>                    <tr>                        <td style=\"padding-bottom:10px\">                            <input type=\"text\" name=\"TTpasswordText\" id=\"TTpasswordText\" class=\"TTdefaultInput\"                                   value=\"Create a password\"                                   ttpasswordpair=\"TTpassword\" onfocus=\"TurnTo.inputFocus(this)\"                                   onblur=\"TurnTo.inputLoseFocus(this)\">                            <input type=\"password\" name=\"TTpassword\" id=\"TTpassword\" style=\"display: none;\"                                   ttpasswordpair=\"TTpasswordText\" onblur=\"TurnTo.inputLoseFocus(this)\">                            <input type=\"text\" name=\"TTrePasswordText\" id=\"TTrePasswordText\" class=\"TTdefaultInput\"                                   value=\"Confirm password\" ttpasswordpair=\"TTrePassword\"                                   onfocus=\"TurnTo.inputFocus(this)\"                                   onblur=\"TurnTo.inputLoseFocus(this)\">                            <input type=\"password\" name=\"TTrePassword\" id=\"TTrePassword\" style=\"display: none;\"                                   ttpasswordpair=\"TTrePasswordText\" onblur=\"TurnTo.inputLoseFocus(this)\">                            <div>So you can easily look up all your questions and answers in the future.</div>                        </td>                    </tr>                    </tbody>                </table>                <div style=\"margin-top: 7px;text-align:right\">                    <input type=\"submit\" id=\"TTregSubmit\" class=\"TTsubmitButton\" style=\"height:28px;width:91px;\" value=\"\">                    <img id=\"TTregSubmitLoading\" src=\"',TurnTo.getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"                         style=\"border: 1px solid rgb(204, 204, 204); padding: 1px 23px; display: none;\">                </div>            </form>            <div style=\"margin-top:10px\">Done this before? <a id=\"TT2login\" class=\"TT2link TT2bold\" href=\"javascript:void(0)\">Log in</a></div>        </div>                    </div>    </div></div>');}return p.join('');"),questionSharePanel:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2shareQuestionPanel\">    ');if (questionSubmitted) {p.push('    <div style=\"font-weight:bold;text-align:center;margin-bottom:18px;position:relative;\">        <span class=\"TT2inlineMessage\" style=\"display:inline;color:#777777;padding:6px\">Your question has been submitted.</span>    </div>    ');}p.push('    <div style=\"font-weight:bold;text-align:center;font-size:16px;position:relative;\">        Want advice from your friends, too?        <br/>        Share this question.    </div>    <div style=\"margin-top:18px;\">        <div class=\"TT2left\" style=\"',!catImgProcessed?'display:none;':'','margin-left:20px;margin-right:20px\">            <img class=\"TT2productImg\" src=\"',TurnTo.getProductImage(catImgId,'SS'),'\"/>        </div>        <div class=\"TT2left\" style=\"margin-left:2px\">            <div id=\"TT2shareError\" style=\"color:red\"></div>            <div style=\"margin-bottom:4px;\">                <div class=\"TT2left\" style=\"margin-right: 10px;\">                    <input type=\"checkbox\" id=\"TT2facebookBox\" name=\"TT2facebookBox\" value=\"true\" class=\"TT2left\"                           style=\"margin-right:4px;margin-top:2px;\"/>                    <span style=\"color:#1D4088\"><img src=\"',TurnTo.getStaticUrl('/tra2/images/smallfbicon.png'),'\" alt=\"Facebook\" style=\"vertical-align:bottom\"/> Facebook</span>                </div>                <div id=\"TT2twitterDiv\" class=\"TT2left\" style=\"margin-right: 12px;\">                    <input type=\"checkbox\" id=\"TT2twitterBox\" name=\"TT2twitterBox\" value=\"true\" class=\"TT2left\"                           style=\"margin-right:4px;margin-top:2px;\"/>                    <span style=\"color:#1D4088\"><img id=\"TT2twitterImg\" src=\"',TurnTo.getStaticUrl('/tra2/images/smalltwittericon.png'),'\"                         alt=\"Twitter\" style=\"vertical-align:bottom\"/> Twitter</span>                </div>            </div>            <textarea id=\"TT2shareTextArea\" style=\"width:',!catImgProcessed?'502':'378','px;margin-top:4px\">',questionText,'</textarea>            <div style=\"width:',!catImgProcessed?'508':'384','px;margin-top:6px;\">                <div class=\"TT2left\">                    <span style=\"font-size:10px;color:#777777\">Click in the box to modify your question</span>                </div>                <div id=\"TT2shareBtns\" class=\"TT2right\">                    <input id=\"TT2shareBtn\" class=\"TT2button\" style=\"font-size:10px;font-weight:bold;color:#FFFFFF;background:#6175A9;border-color:#1D4088\" type=\"button\" value=\"Share\"/>                    <input id=\"TT2cancelShareBtn\" class=\"TT2button\" style=\"font-size:10px;font-weight:bold;background-color:#FFFFFF;color:#000000;border-color:#000000\" type=\"button\" value=\"No Thanks\"/>                </div>                <img id=\"TT2shareLoading\" style=\"display:none\" class=\"TT2imgLoading TT2right\" src=\"',TurnTo.getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\">            </div>        </div>        <div class=\"TT2clear\"></div>    </div></div>');}return p.join('');"),questionWidget:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2askWidget\">    <div id=\"TT2questionWidgetMessageArea\" style=\"display:none;\"></div>    <img id=\"TT2goToMatches\" class=\"TT2buttonImg\" src=\"',TurnTo.getStaticUrl('/tra2/images/backtotrabttn.png'),'\" style=\"margin-bottom:10px;\" alt=\"See what people bought\"/>    <div class=\"TT2clearBoth\"></div>    <div id=\"TT2askWigetHeader\" style=\"background-color:#B8CCE4;padding:5px;margin-bottom:5px;\">        <a id=\"TT2itemLink-title-',item.id,'\" class=\"TT2itemLink\" href=\"javascript:void(0)\" style=\"font-size:16px;font-weight:bold;color:#314C9B\">',TurnTojQuery.trim(item.title),'</a>    </div>    <div id=\"TT2replySubmitted\" class=\"TT2inlineMessage\" style=\"margin-top:0px;margin-bottom:5px;text-align:center;\">        <span style=\"float:right;position:relative;\"><a id=\"TT2dismissReplySubmitted\" href=\"javascript:void(0)\"><img                src=\"',TurnTo.getStaticUrl('/images/tabClose.gif'),'\" alt=\"Dismiss\"/></a></span>        <img src=\"',TurnTo.getStaticUrl('/tra2/images/blueexclaimmark.png'),'\" style=\"vertical-align:middle\"/> <b>Thank you! Your reply has been submitted.</b>    </div>    <div id=\"TT2questionSubmitted\" class=\"TT2inlineMessage\" style=\"margin-top:0px;margin-bottom:5px;text-align:center;\">        <span style=\"float:right;position:relative;\"><a id=\"TT2dismissSubmitted\" href=\"javascript:void(0)\"><img                src=\"',TurnTo.getStaticUrl('/images/tabClose.gif'),'\" alt=\"Dismiss\"/></a></span>        <img src=\"',TurnTo.getStaticUrl('/tra2/images/blueexclaimmark.png'),'\" style=\"vertical-align:middle\"/> <b>Thank you! Your question has been submitted.</b> - <span class=\"TT2youCheckAnswers\">');if (TurnTo.matchData.user){p.push('We&#39;ll email you as soon as someone answers.');} else {p.push('Please come back to check your answers.');}p.push('</span>    </div>    <div id=\"TT2questionSubmittedNotConfirmed\" class=\"TT2inlineMessage\" style=\"margin-top:0px;margin-bottom:5px;text-align:center;\">        <span style=\"float:right;position:relative;\"><a id=\"TT2dismissNotConfirmed\"                                                        href=\"javascript:void(0)\"><img                src=\"',TurnTo.getStaticUrl('/images/tabClose.gif'),'\" alt=\"Dismiss\"/></a></span>        <div style=\"margin-right:16px;\">            <img src=\"',TurnTo.getStaticUrl('/tra2/images/blueexclaimmark.png'),'\" style=\"vertical-align:middle\"/> <b>Go to your email inbox to confirm your email address so we can send you your replies as soon as we get them.</b>        </div>    </div>    <div id=\"TT2error\" style=\"margin-top:0px;margin-bottom:5px\"></div>    ', TurnToHTML.askWidgetQuestionSection({iid:match.iid,catItmId:item.id,catImgId:item.catImgId, catImgProcessed:item.catImgProcessed}) ,'    <div id=\"TT2showQuestionBox\" style=\"background-color:#E7ECF3;padding:5px;margin-bottom:5px;margin-top:5px;font-size:12px;display:none;\">        <a href=\"javascript:void(0)\" id=\"TT2showQuestionBoxLink\">            <span style=\"color:#5288D1;font-weight:bold;\">Ask a question ></span>        </a>    </div>    <div id=\"TT2whatShouldAsk\" style=\"background-color:#E7ECF3;padding:5px;margin-bottom:5px;margin-top:5px;font-size:12px;\">        <a href=\"javascript:void(0)\" id=\"TT2askQuestionTips\">            <span style=\"color:#5288D1;font-weight:bold;\">What should I ask?</span>            <img id=\"TT2askQuestionTipsExpandImg\" src=\"',TurnTo.getStaticUrl('/tra2/images/askarrow2.png'),'\" style=\"vertical-align:bottom;width:14px;margin-bottom:2px\" />            <img id=\"TT2askQuestionTipsShrinkImg\" src=\"',TurnTo.getStaticUrl('/tra2/images/askarrow1.png'),'\" style=\"vertical-align:bottom;display:none;width:14px;margin-bottom:2px\" />        </a>        <div id=\"TT2askQuestionExamples\" style=\"display:none;margin-top:15px;\">            <div style=\"width:50%;float:left;position:relative\">                <span style=\"font-weight:bold\">Thinking of buying it</span>                <ul style=\"margin-top:5px;margin-bottom:8px; list-style-type:disc;margin-left:14px;\">                    <li style=\"margin-bottom:4px;\">                        How well did it work?                    </li>                    <li style=\"margin-bottom:4px;\">                        How long did it last?                    </li>                    <li>                        Why did you pick this one vs. others?                    </li>                </ul>            </div>            <div style=\"width:50%;float:left;position:relative\">                <span style=\"font-weight:bold\">Already own it</span>                <ul style=\"margin-top:5px;margin-bottom:8px; list-style-type:disc; margin-left:14px;\">                    <li style=\"margin-bottom:4px;\">                        How to get the most out of it?                    </li>                    <li style=\"margin-bottom:4px;\">                        How to take care of it?                    </li>                </ul>            </div>            <div class=\"TT2clearBoth\"></div>            <div style=\"margin-top:8px;margin-bottom:8px;\">You can also ask a general question that people who bought this might know about.</div>            <div style=\"font-style:italic;font-size:10px;\">*Ask like you&#39;re asking your friends, not a search engine. Your fellow customers are more likely to reply.*</div>        </div>    </div>    <div id=\"TT2questionSectionTitle\" style=\"');if (questions.length ==0){p.push('display:none;');}p.push('background-color:#E7ECF3;padding:5px;margin-bottom:5px;margin-top:5px;\">        <span href=\"javascript:void(0)\" style=\"font-size:14px;font-weight:bold;color:#314C9B\">            Questions with this item        </span>    </div>    ');if (questions.length > 0) {p.push('        ');for (var questionIndex = 0; questionIndex < questions.length; questionIndex++) {            var qid = questions[questionIndex].id;            var thisIsYou = TurnTo.matchData.user && TurnTo.matchData.user.id == questions[questionIndex].uid;        p.push('        <div class=\"TT2cqa\" style=\"');if (questionIndex == questions.length-1){p.push('border-bottom:none;');}p.push('\">            <div class=\"TT2askedUserPhoto\" style=\"margin-right:0px;margin-bottom:0px\">                <img style=\"display:inline\" src=\"', TurnTo.getUserPhoto(questions[questionIndex].uid, questions[questionIndex].pid) ,'\" alt=\"\"/>                ');if (questions[questionIndex].isStaff) {p.push('                    <img style=\"display: block;height: 22px;width: 45px;margin-top:-3px\" src=\"', TurnTo.getStaticUrl('/tra2/images/stafflabel.png') ,'\" alt=\"\"/>                    ');}p.push('            </div>            <div style=\"width:590px;float:right;position:relative;\">                <div id=\"TT2questionBlock-',qid,'\" style=\"margin-bottom:10px;\">                    <input id=\"TT2rawQuestionText-',qid,'\" type=\"hidden\" value=\"',questions[questionIndex].rawText,'\"/>                    ');if (thisIsYou) {p.push('                    <span class=\"TT2right\" style=\"margin-left:5px;\"><input id=\"TT2shareQuestionBtn-',qid,'\" qid=\"',qid,'\" class=\"TT2shareQuestionBtn  TT2button\" style=\"font-size:10px;font-weight:bold;color:#FFFFFF;background:#6175A9;border-color:#1D4088\" type=\"button\" value=\"Share\"/></span>                    ');}p.push('                    ');if (TurnTo.matchData.user && TurnTo.isSiteModerator(TurnTo.matchData.user.userSiteRoles)) {p.push('                        <span style=\"margin-top:3px\" class=\"TT2right\"><a target=\"_blank\" href=\"',TurnTo.partnerProtocol + '//' + turnToSettings.host,'/moderation/qsolo?id=',questions[questionIndex].id,'\" class=\"TTblue1\" style=\"font-size:10px;text-decoration:none\">Moderate</a></span>                     ');}p.push('                    <span class=\"TT2bold\">',thisIsYou ? 'You' : questions[questionIndex].fName + (questions[questionIndex].lName ? ' ' + questions[questionIndex].lName.substring(0, 1) : ''),' asked: </span>                    <span>',questions[questionIndex].text,'</span>                </div>                ');for (var s = 0; s < questions[questionIndex].answers.length; s++) {                    var answer = questions[questionIndex].answers[s];                p.push('                    ',TurnToHTML.answerBlock({answerer:{uid:answer.uid, pid:answer.pid, fName:answer.fName, lName:answer.lName, isStaff:answer.isStaff},                    answerText:answer.text, rawAnswerText:answer.rawText, aid:answer.id, qid:qid, quid:questions[questionIndex].uid, replys:answer.replys, showReply:true, related:false}),'                ');}p.push('                ',TurnToHTML.inlineAnswerInputBox({qid:qid, defaultText:\"Enter your answer or comment here\", aid:null, show:true, replyInput:false, related:false, replyToUid:null}),'            </div>            <div class=\"TT2clearBoth\" style=\"margin-bottom: 10px;\"></div>        </div>        ');}p.push('    ');}p.push('    ');if (relatedQuestions.length > 0) {p.push('        <div            style=\"');if (relatedQuestions.length ==0){p.push('display:none;');}p.push('background-color:#E7ECF3;padding:5px;margin-bottom:5px;margin-top:5px;\">            <span href=\"javascript:void(0)\" style=\"font-size:14px;font-weight:bold;color:#314C9B\">                Questions about related items            </span>        </div>        ');for (var questionIndex = 0; questionIndex < relatedQuestions.length; questionIndex++) {            var qid = relatedQuestions[questionIndex].id;            var thisIsYou = TurnTo.matchData.user && TurnTo.matchData.user.id == relatedQuestions[questionIndex].uid;        p.push('        <div class=\"TT2cqa\" style=\"');if (questionIndex == relatedQuestions.length-1){p.push('border-bottom:none;');}p.push('\">            <div class=\"TT2askedUserPhoto\" style=\"margin-right:0px;margin-bottom:0px\">                <img style=\"display:inline\" src=\"', TurnTo.getUserPhoto(relatedQuestions[questionIndex].uid, relatedQuestions[questionIndex].pid) ,'\" alt=\"\"/>                ');if (relatedQuestions[questionIndex].isStaff) {p.push('                    <img style=\"display: block;height: 22px;width: 45px;margin-top:-3px\" src=\"', TurnTo.getStaticUrl('/tra2/images/stafflabel.png') ,'\" alt=\"\"/>                ');}p.push('            </div>            <div style=\"float: right;\"><a id=\"TT2itemLink-image-',relatedQuestions[questionIndex].catItmId,'\" class=\"TT2itemLink\" href=\"javascript:void(0)\"><img src=\"',TurnTo.getProductImage(relatedQuestions[questionIndex].catImgId, 'S'),'\"></a></div>            <div style=\"width:400px;float:right;position:relative;\">                <div id=\"TT2questionBlock-',qid,'\" style=\"margin-bottom:10px;\">                    <span class=\"TT2bold\">',thisIsYou ? 'You' : relatedQuestions[questionIndex].fName + (relatedQuestions[questionIndex].lName ? ' ' + relatedQuestions[questionIndex].lName.substring(0, 1) : ''),' asked about                        <a id=\"TT2itemLink-title-',relatedQuestions[questionIndex].catItmId,'\" class=\"TT2itemLink\" href=\"javascript:void(0)\" style=\"font-size:12px;font-weight:bold;color:#314C9B\">',TurnTojQuery.trim(relatedQuestions[questionIndex].catItmTitle),'</a>: </span>                    <span>',relatedQuestions[questionIndex].text,'</span>                </div>                ');for (var s = 0; s < relatedQuestions[questionIndex].answers.length; s++) {                    var answer = relatedQuestions[questionIndex].answers[s];                p.push('                    ',TurnToHTML.answerBlock({answerer:{uid:answer.uid, pid:answer.pid, fName:answer.fName, lName:answer.lName, isStaff:answer.isStaff},                    answerText:answer.text, rawAnswerText:answer.rawText, aid:answer.id, qid:qid, quid:relatedQuestions[questionIndex].uid, replys:answer.replys, showReply:true, related:true}),'                ');}p.push('                ',TurnToHTML.inlineAnswerInputBox({qid:qid, defaultText:\"Enter your answer or comment here\", aid:null, show:true, replyInput:false, related:true, replyToUid:null}),'            </div>            <div class=\"TT2clearBoth\" style=\"margin-bottom: 10px;\"></div>        </div>        ');}p.push('    ');}p.push('    ');if (comments.length > 0) {p.push('        <div style=\"background-color:#E7ECF3;padding:5px;margin-bottom:5px;margin-top:5px;\">            <span href=\"javascript:void(0)\" style=\"font-size:14px;font-weight:bold;color:#314C9B\">                Comments from people who bought this            </span>        </div>        ');for (var commentIndex = 0; commentIndex < comments.length; commentIndex++) {            var thisIsYou = TurnTo.matchData.user && TurnTo.matchData.user.id == comments[commentIndex].uid;        p.push('        <div class=\"TT2cqa\" style=\"');if (commentIndex == comments.length-1){p.push('border-bottom:none;');}p.push('\">            <div class=\"TT2askedUserPhoto\" style=\"margin-right:0px;margin-bottom:0px\">                <img style=\"display:inline\" src=\"', TurnTo.getUserPhoto(comments[commentIndex].uid, comments[commentIndex].pid) ,'\" alt=\"\"/>            </div>            <div style=\"width:590px;float:right;position:relative;\">                <span class=\"TT2bold\">',thisIsYou ? 'You' : comments[commentIndex].fName + (comments[commentIndex].lName ? ' ' + comments[commentIndex].lName.substring(0, 1) : ''),' chose this because:</span><span> ',comments[commentIndex].text,'</span>            </div>            <div class=\"TT2clearBoth\" style=\"margin-bottom: 10px;\"></div>        </div>        ');}p.push('    ');}p.push('</div>');}return p.join('');"),recommendRow:new Function("obj","var p=[];with(obj){p.push('<div class=\"TTRave\">    <div class=\"TTRRaveTitle\">           ');if (m.url) {p.push('<a class=\"TTtransactionItemLink\"                                 href=\"', matchLinkRoot ,'&pHref=', encodeURIComponent(m.url) ,'&pd=&v=', TurnTo.version  ,'&mId=', m.id ,'&tId=0&tiId=', m.id ,' \">        ', m.title ,'    </a>        ');} else {p.push('                  <i>', m.title ,'</i>              ');}p.push('            </div>    <div class=\"TTRRaveInfo\">        '); if ( m.raveCommentCount >0) { p.push('        <img src=\"', TurnTo.getStaticUrl('/tra/images/starwithdoc.png'),'\" border=\"0\"             style=\"padding-left:0\"/>        '); } else if (m.raveCount > 0){ p.push('        <img src=\"', TurnTo.getStaticUrl('/tra/images/star.png'),'\" border=\"0\" style=\"padding-left:0\"/>        '); } p.push('        ',m.raveCount,' Recommendation',m.raveCount==1?'':'s','        '); if(m.raveCommentCount>0){ p.push(' | ',m.raveCommentCount,' Comment',m.raveCommentCount==1?'':'s',' '); }        p.push('    </div>    '); for ( var i = 0; i < m.raveComments.length; i++ ) { p.push('    <div class=\"TTRRaveComm\">&#147;',m.raveComments[i] ,'&#148;</div>    '); } p.push('    </a></div>');}return p.join('');"),regform:new Function("obj","var p=[];with(obj){p.push('<ul id=\"TTSubDialogErrors\"></ul><div id=\"TT2regFormDesc\">    '); if (matches.user) { p.push('        Almost done.        <br>        Confirm this is correct and fill in what&#39;s missing:    '); } else {p.push('        OK. Start here. You can always add friends later.    '); } p.push('</div><form id=\"TTregForm\" action=\"javascript:void()\">    <input type=\"hidden\" name=\"TTregflow\" value=\"', regflow ,'\" />    <table id=\"TT2regformTable\">        <tbody>        <tr>            <td>First Name*</td>            <td><input type=\"text\" name=\"TTfirstName\" id=\"TTfirstName\" ttinputlabeltext=\"First Name\" value=\"', matches.user && matches.user.firstName ?matches.user.firstName:'First Name' ,'\"                        onfocus=\"', matches.user && matches.user.firstName ? '' : 'TurnTo.inputFocus(this)','\"  class=\"', matches.user && matches.user.firstName ? '' : 'TTdefaultInput','\" onblur=\"', matches.user && matches.user.firstName ? '' : 'TurnTo.inputLoseFocus(this)','\">            </td>        </tr>        <tr>            <td>Last Name*</td>            <td><input type=\"text\" name=\"TTlastName\" id=\"TTlastName\" ttinputlabeltext=\"Last Name\" value=\"', matches.user && matches.user.lastName ? matches.user.lastName:'Last Name' ,'\"                        onfocus=\"', matches.user && matches.user.lastName ? '' : 'TurnTo.inputFocus(this)','\" onblur=\"', matches.user && matches.user.lastName ? '' : 'TurnTo.inputLoseFocus(this)','\" class=\"', matches.user && matches.user.lastName ? '' : 'TTdefaultInput','\">            </td>        </tr>        <tr>            <td>Email*</td>            <td><input type=\"text\" class=\"', matches.user ? '' : 'TTdefaultInput','\" onblur=\"', matches.user ? '' : 'TurnTo.inputLoseFocus(this)','\"                       onfocus=\"', matches.user ? '' : 'TurnTo.inputFocus(this)','\" value=\"', matches.user ? matches.user.uIdents[0].ident : 'Email','\" ttinputlabeltext=\"Email\" id=\"TTemail\"                       name=\"TTemail\" ', matches.user ? 'readonly=\"readonly\"' : '','></td>        </tr>        <tr>            <td>Password*</td>            <td><input type=\"text\" name=\"TTpasswordText\" id=\"TTpasswordText\" class=\"TTdefaultInput\" value=\"Password\"                       ttpasswordpair=\"TTpassword\" onfocus=\"TurnTo.inputFocus(this)\"                       onblur=\"TurnTo.inputLoseFocus(this)\">                <input type=\"password\" name=\"TTpassword\" id=\"TTpassword\" style=\"display: none;\"                       ttpasswordpair=\"TTpasswordText\" onblur=\"TurnTo.inputLoseFocus(this)\"></td>        </tr>        <tr>            <td>Confirm Password*</td>            <td><input type=\"text\" name=\"TTrePasswordText\" id=\"TTrePasswordText\" class=\"TTdefaultInput\"                       value=\"Confirm Password\" ttpasswordpair=\"TTrePassword\" onfocus=\"TurnTo.inputFocus(this)\"                       onblur=\"TurnTo.inputLoseFocus(this)\">                <input type=\"password\" name=\"TTrePassword\" id=\"TTrePassword\" style=\"display: none;\"                       ttpasswordpair=\"TTrePasswordText\" onblur=\"TurnTo.inputLoseFocus(this)\"></td>        </tr>        '); if (!matches.user) { p.push('        <tr>            <td>Are you a human?*<br/><span style=\"font-size:10px\">(sorry, we have to ask)</span></td>            <td><input type=\"text\" name=\"TTcaptcha\" id=\"TTcaptcha\" ttinputlabeltext=\"Verification\" value=\"Verification\"                       onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\" class=\"TTdefaultInput\"                       style=\"width: 70px; float: left;\">                <img src=\"', getUrl('/captcha/index?tt='+(new Date().getTime())) ,'\"                     style=\"margin: -2px 7px; float: left; vertical-align: middle; opacity: 0.65;\"></td>        </tr>        '); } p.push('        <tr>            <td></td>            <td>                <div style=\"float:left;margin-right:5px\">                    <img id=\"TT2userImg\" src=\"\" style=\"display:none;width:45px;height:45px\"/>                    <img id=\"TT2userImgLoading\" src=\"',getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"                        style=\"border: 1px solid rgb(204, 204, 204); width:45px;height:45px; display: none;\">                </div>                <div id=\"TT2regImgUpload\">                    Add Photo<br/>                    <input type=\"file\" name=\"TT2userImgFile\" id=\"TT2userImgFile\" onchange=\"TurnTo.inputUserImage(this)\"/><br/>                    Select an image file on your computer (4MB max)                </div>            </td>        </tr>        <tr>            <td></td>            <td style=\"font-size:10px\">                Or just                 <a href=\"javascript:void(0)\" id=\"TT2regFromfbConnect\">                    <img src=\"',TurnTo.fbButtonSrc ,'\" alt=\"Connect\" style=\"border:none;vertical-align:middle;\"/>                </a> to use your Facebook photo            </td>        </tr>        </tbody>    </table>    <div style=\"margin-top: 7px;float:right\">        <input type=\"submit\" id=\"TTregSubmit\" class=\"TTsubmitButton\" style=\"height:27px;width:89px;\" value=\"\">        <img id=\"TTregSubmitLoading\" src=\"',getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\"             style=\"border: 1px solid rgb(204, 204, 204); padding: 1px 23px; display: none;\">    </div></form>');}return p.join('');"),replyBlock:new Function("obj","var p=[];with(obj){p.push('');var thisIsYourReply = TurnTo.matchData.user && TurnTo.matchData.user.id == reply.uid;p.push('<input type=\"hidden\" id=\"TT2qid4reply-',reply.id,'\" value=\"',qid,'\"/><input type=\"hidden\" id=\"TT2aid4reply-',reply.id,'\" name=\"TT2aid4reply-',reply.id,'\" value=\"',aid,'\"/><div class=\"TT2clearBoth\"></div><div style=\"margin-left:35px;\">    <div class=\"TT2askedUserPhoto\" style=\"margin:0px 10px 0px 0px\">        <img style=\"display:inline;width:35px;height:35px\"             src=\"', TurnTo.getUserPhoto(reply.uid, reply.pid) ,'\"             alt=\"\"/>        ');if (reply.isStaff) {p.push('            <img style=\"display: block;height: 15px;width: 35px;margin-top:-3px\" src=\"', TurnTo.getStaticUrl('/tra2/images/stafflabel.png') ,'\" alt=\"\"/>        ');}p.push('    </div>    <div style=\"margin-top:5px;margin-left:45px\">        ');if (thisIsYourReply) {p.push('        <span class=\"TT2right\"><input id=\"TT2shareReplyBtn-',reply.id,'\" aid=\"',reply.id,'\"                                      class=\"TT2shareReplyBtn  TT2button\"                                      style=\"font-size:10px;font-weight:bold;color:#FFFFFF;background:#6175A9;border-color:#1D4088\"                                      type=\"button\" value=\"Share\"/></span>        <input id=\"TT2rawReplyText-',reply.id,'\" type=\"hidden\" value=\"',reply.rawText,'\"/>        ');}p.push('        <span class=\"TT2bold\">',thisIsYourReply ? 'You' : reply.fName + (reply.lName ? ' ' + reply.lName.substring(0, 1) : ''),':</span>        <span>',reply.text,'</span>    </div></div>');if (showReplyLink && !thisIsYourReply) {p.push('<div style=\"margin-left:35px;\">    <a id=\"TT2answerReply-',qid,'-',aid,'-',reply.uid,'\" class=\"TT2answerReply TT2right\" style=\"font-size:10px;color:#314C9B\"       href=\"javascript:void(0)\">Reply to ',reply.fName,'</a>    ',TurnToHTML.inlineAnswerInputBox({qid:qid, defaultText:\"Enter your reply here. We&#39;ll email it to \" +    reply.fName, aid:aid, show:false, replyInput:true, related:related, replyToUid:reply.uid}),'</div>');}p.push('<div id=\"TT2replySubmitted-',reply.id,'\" class=\"TT2inlineMessage\"     style=\"margin-top:2px;margin-left: 80px;margin-bottom:4px;\">    <span style=\"float:right\"><a id=\"TT2dismissReplied-',reply.id,'\" href=\"javascript:void(0)\"><img            src=\"',TurnTo.getStaticUrl('/images/tabClose.gif'),'\" alt=\"Dismiss\"/></a></span>    <b>Your reply has been submitted.</b></div>');}return p.join('');"),replySharePanel:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2shareReplyPanel\">    ');if (replySubmitted) {p.push('    <div style=\"font-weight:bold;text-align:center;margin-bottom:18px;position:relative;\">        <span class=\"TT2inlineMessage\" style=\"display:inline;color:#777777;padding:6px\">Your reply has been submitted.</span>    </div>    ');}p.push('    <div style=\"font-weight:bold;text-align:center;font-size:16px;position:relative;\">        Want to share your thought with your friends, too?        <br/>        Share this reply.    </div>    <div style=\"margin-top:18px;\">        <div class=\"TT2left\" style=\"',!catImgProcessed?'display:none;':'','margin-left:20px;margin-right:20px\">            <img class=\"TT2productImg\" src=\"',TurnTo.getProductImage(catImgId,'SS'),'\"/>        </div>        <div class=\"TT2left\" style=\"margin-left:2px\">            <div id=\"TT2shareError\" style=\"color:red\"></div>            <div style=\"margin-bottom:4px;\">                <div class=\"TT2left\" style=\"margin-right: 10px;\">                    <input type=\"checkbox\" id=\"TT2facebookBox\" name=\"TT2facebookBox\" value=\"true\" class=\"TT2left\"                           style=\"margin-right:4px;margin-top:2px;\"/>                    <span style=\"color:#1D4088\"><img src=\"',TurnTo.getStaticUrl('/tra2/images/smallfbicon.png'),'\" alt=\"Facebook\" style=\"vertical-align:bottom\"/> Facebook</span>                </div>                <div id=\"TT2twitterDiv\" class=\"TT2left\" style=\"margin-right: 12px;\">                    <input type=\"checkbox\" id=\"TT2twitterBox\" name=\"TT2twitterBox\" value=\"true\" class=\"TT2left\"                           style=\"margin-right:4px;margin-top:2px;\"/>                    <span style=\"color:#1D4088\"><img id=\"TT2twitterImg\" src=\"',TurnTo.getStaticUrl('/tra2/images/smalltwittericon.png'),'\"                         alt=\"Twitter\" style=\"vertical-align:bottom\"/> Twitter</span>                </div>            </div>            <textarea id=\"TT2shareTextArea\" style=\"width:',!catImgProcessed?'502':'378','px;margin-top:4px\">My reply to: \"',answerText,'\"</textarea>            <div style=\"width:',!catImgProcessed?'508':'384','px;margin-top:6px;\">                <div class=\"TT2left\">                    <span style=\"font-size:10px;color:#777777\">Click in the box to modify your reply</span>                </div>                <div id=\"TT2shareBtns\" class=\"TT2right\">                    <input id=\"TT2shareBtn\" class=\"TT2button\" style=\"font-size:10px;font-weight:bold;color:#FFFFFF;background:#6175A9;border-color:#1D4088\" type=\"button\" value=\"Share\"/>                    <input id=\"TT2cancelShareBtn\" class=\"TT2button\" style=\"font-size:10px;font-weight:bold;background-color:#FFFFFF;color:#000000;border-color:#000000\" type=\"button\" value=\"No Thanks\"/>                </div>                <img id=\"TT2shareLoading\" style=\"display:none\" class=\"TT2imgLoading TT2right\" src=\"',TurnTo.getStaticUrl('/images/spinner.gif'),'\" alt=\"Loading...\">            </div>        </div>        <div class=\"TT2clear\"></div>    </div></div>');}return p.join('');"),replyWidget:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2replyWidget\">    <div class=\"TT2awHeader\"><b>',reply.fName,'</b> replied to you regarding        <b><a id=\"TT2itemLink-title-',catItem.id,'\" class=\"TT2itemLink\" href=\"javascript:void(0)\"              style=\"font-size:16px;font-weight:bold;color:#314C9B\">',TurnTojQuery.trim(catItem.title),'</a></b>    </div>    ');var qarid = question.id + '-' + answer.id + '-' + reply.uid;p.push('    <input type=\"hidden\" id=\"TT2replyqid\" name=\"TT2replyqid\" value=\"',question.id,'\"/>    <input type=\"hidden\" id=\"TT2replyaid\" name=\"TT2replyaid\" value=\"',answer.id,'\"/>    <input type=\"hidden\" id=\"TT2replyuid\" name=\"TT2replyuid\" value=\"',reply.uid,'\"/>    <input type=\"hidden\" id=\"TT2replyToUid-',qarid,'\" name=\"TT2replyToUid-',qarid,'\" value=\"',reply.uid,'\"/>    <div style=\"float:left;position:relative;width:430px;\">        <div>            <div class=\"TT2askedUserPhoto\" style=\"margin-top:0px;\">                <img style=\"display:inline\" src=\"', TurnTo.getUserPhoto(question.uid, question.pid) ,'\" alt=\"\"/>                ');if (question.isStaff) {p.push('                <img style=\"display: block;height: 22px;width: 45px;margin-top:-3px\" src=\"', TurnTo.getStaticUrl('/tra2/images/stafflabel.png') ,'\" alt=\"\"/>                ');}p.push('            </div>            <div>                <span class=\"TT2bold\">', TurnTo.matchData.user.id == question.uid ? 'You' : question.fName ,' asked:</span>                <span>',question.text,'</span>            </div>        </div>        <div class=\"TT2clearBoth\"></div>        <div>            <div class=\"TT2askedUserPhoto\" style=\"margin-top:0px;\">                <img style=\"display:inline\" src=\"', TurnTo.getUserPhoto(answer.uid, answer.pid) ,'\" alt=\"\"/>                ');if (answer.isStaff) {p.push('                <img style=\"display: block;height: 22px;width: 45px;margin-top:-3px\" src=\"', TurnTo.getStaticUrl('/tra2/images/stafflabel.png') ,'\" alt=\"\"/>                ');}p.push('            </div>            <div>                <span class=\"TT2bold\">', TurnTo.matchData.user.id == answer.uid ? 'You' : answer.fName ,':</span>                <span>',answer.text,'</span>            </div>        </div>        <div class=\"TT2clearBoth\"></div>        ');for (var i = 0; i < replys.length; i++) {p.push('        <div>            <div class=\"TT2askedUserPhoto\" style=\"margin-top:0px;\">                <img style=\"display:inline\" src=\"', TurnTo.getUserPhoto(replys[i].uid, replys[i].pid) ,'\" alt=\"\"/>                ');if (replys[i].isStaff) {p.push('                <img style=\"display: block;height: 22px;width: 45px;margin-top:-3px\" src=\"', TurnTo.getStaticUrl('/tra2/images/stafflabel.png') ,'\" alt=\"\"/>                ');}p.push('            </div>            <div>                <span class=\"TT2bold\">', TurnTo.matchData.user.id == replys[i].uid ? 'You' : replys[i].fName ,':</span>                <span>',replys[i].text,'</span>            </div>        </div>        <div class=\"TT2clearBoth\"></div>        ');}p.push('    </div>    <div style=\"float:right;margin-right:20px;position:relative;\">        <a id=\"TT2itemLink-image-',catItem.id,'\" class=\"TT2itemLink\" href=\"javascript:void(0)\"><img                src=\"',TurnTo.getProductImage(catItem.imgId,'L'),'\" alt=\"',catItem.title,'\"                style=\"width:165px;height:165px;\"></a>    </div>    <div class=\"TT2clearBoth\"></div>    <div id=\"TT2answerPanel\" class=\"TT2userInputArea\" style=\"padding:12px;\">        <div id=\"TT2error-',qarid,'\" class=\"TT2right\"></div>        <textarea id=\"TT2inlineAnswer-',qarid,'\" name=\"TT2inlineAnswer-',qarid,'\" rows=\"12\" class=\"TTdefaultInput\"                  ttinputlabeltext=\"Enter your reply here. We&#39;ll email it to ',TurnTo.matchData.user.id == question.uid ? answer.fName : question.fName,'\"                  onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\">Enter your reply here. We&#39;ll email it to ',TurnTo.matchData.user.id == question.uid ? answer.fName : question.fName,'</textarea>        <div id=\"TT2anwerCharCount-',qarid,'\" class=\"TT2charLimitCount TT2right\"></div>        <div style=\"margin-top:10px; clear:both; display:none\">            <input type=\"checkbox\" id=\"TT2answerNotify\" checked=\"true\"/>&nbsp;Tell me when other people answer or            comment on this.        </div>        <div class=\"TT2left\" style=\"font-size:11px;margin-top:10px;\">            <div id=\"TT2answerBtnPanel\" class=\"TT2right\">                <img id=\"TT2imgLoading-',qarid,'\" class=\"TT2imgLoading\"                     src=\"',TurnTo.getStaticUrl('/images/spinner.gif'),'\"                     alt=\"Loading...\"                     style=\"display: none;\">                <input id=\"TT2answerBtn-',qarid,'\" type=\"button\" class=\"TT2buttonSub\" value=\"\"                       style=\"height:29px; width:91px;\">            </div>            Do not include: HTML, references to other stores, pricing, contact information (inc. your email &mdash; you can add that later), or inappropriate language. Posts which include these will be edited or removed.        </div>        <div class=\"TT2clearBoth\"></div>    </div></div>');}return p.join('');"),siteVisitsAdmin:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2siteVisitsAdmin\">    ');if (blocking) {p.push('        <h3 style=\"color:#cc0000\">This browser will not send site visit data to TurnTo.        This browser will now be excluded from TurnTo reports.        Keep this setting if you are internal at ',siteDisplayName,'.        </h3>        <br/><br/>        <input type=\"button\" id=\"TT2clear\" class=\"TT2button\" style=\"font-size:16px;width:150px;height:30px;\" name=\"TT2clear\" value=\"Resume Sending\"/>    ');} else {p.push('        <h3 style=\"color:#cc0000\">Block this browser from sending site visit data to TurnTo.        This will exclude activity on this browser from TurnTo reports.        Use this if you are internal at ',siteDisplayName ? siteDisplayName : \"this site\",'.         </h3>        <br/><br/>        <input type=\"button\" id=\"TT2block\" class=\"TT2button\" style=\"font-size:16px;width:150px;height:30px;\" name=\"TT2block\" value=\"Yes\"/>    ');}p.push('</div>');}return p.join('');"),subSpinScreen:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2subSpinScreen\" style=\"text-align: center;\">    <p style=\"font-weight: bold; margin-top: 10px; margin-bottom: 10px;\">',description,'</p>    <img src=\"',TurnTo.getStaticUrl('/tra2/images/traspin.gif'),'\" alt=\"Loading\"/></div>');}return p.join('');"),thankAnswerWidget:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2replyWidget\">    <div class=\"TT2awHeader\"><b>',answer.fName,'</b> answered your question regarding        <b><a id=\"TT2itemLink-title-',catItem.id,'\" class=\"TT2itemLink\" href=\"javascript:void(0)\"              style=\"font-size:16px;font-weight:bold;color:#314C9B\">',TurnTojQuery.trim(catItem.title),'</a></b>    </div>    ');var qarid = question.id + '-' + answer.id + '-' + answer.uid;p.push('    <input type=\"hidden\" id=\"TT2replyqid\" name=\"TT2replyqid\" value=\"',question.id,'\"/>    <input type=\"hidden\" id=\"TT2replyaid\" name=\"TT2replyaid\" value=\"',answer.id,'\"/>    <input type=\"hidden\" id=\"TT2replyuid\" name=\"TT2replyuid\" value=\"',answer.uid,'\"/>    <input type=\"hidden\" id=\"TT2replyToUid-',qarid,'\" name=\"TT2replyToUid-',qarid,'\" value=\"',answer.uid,'\"/>    <div style=\"float:left;position:relative;width:430px;\">        <div>            <div class=\"TT2askedUserPhoto\" style=\"margin-top:0px;\">                <img style=\"display:inline\" src=\"', TurnTo.getUserPhoto(question.uid, question.pid) ,'\" alt=\"\"/>                ');if (question.isStaff) {p.push('                <img style=\"display: block;height: 22px;width: 45px;margin-top:-3px\" src=\"', TurnTo.getStaticUrl('/tra2/images/stafflabel.png') ,'\" alt=\"\"/>                ');}p.push('            </div>            <div style=\"margin-left:63px\">                <span class=\"TT2bold\">', TurnTo.matchData.user.id == question.uid ? 'You' : question.fName ,' asked:</span>                <span>',question.text,'</span>            </div>        </div>        <div class=\"TT2clearBoth\"></div>        <div>            <div class=\"TT2askedUserPhoto\" style=\"margin-top:0px;\">                <img style=\"display:inline\" src=\"', TurnTo.getUserPhoto(answer.uid, answer.pid) ,'\" alt=\"\"/>                ');if (answer.isStaff) {p.push('                <img style=\"display: block;height: 22px;width: 45px;margin-top:-3px\" src=\"', TurnTo.getStaticUrl('/tra2/images/stafflabel.png') ,'\" alt=\"\"/>                ');}p.push('            </div>            <div style=\"margin-left:63px\">                <span class=\"TT2bold\">', TurnTo.matchData.user.id == answer.uid ? 'You' : answer.fName ,' answered:</span>                <span>',answer.text,'</span>            </div>        </div>        <div class=\"TT2clearBoth\"></div>    </div>    <div style=\"float:right;margin-right:20px;position:relative;\">        <a id=\"TT2itemLink-image-',catItem.id,'\" class=\"TT2itemLink\" href=\"javascript:void(0)\"><img                src=\"',TurnTo.getProductImage(catItem.imgId,'L'),'\" alt=\"',catItem.title,'\"                style=\"width:165px;height:165px;\"></a>    </div>    <div class=\"TT2clearBoth\"></div>    <div id=\"TT2answerPanel\" class=\"TT2userInputArea\" style=\"padding:12px;\">        <div id=\"TT2error-',qarid,'\" class=\"TT2right\"></div>        <b>You:</b>        <textarea id=\"TT2inlineAnswer-',qarid,'\" name=\"TT2inlineAnswer-',qarid,'\" rows=\"12\" class=\"TTdefaultInput\"                  ttinputlabeltext=\"Enter your reply here. We&#39;ll email it to ',TurnTo.matchData.user.id == question.uid ? answer.fName : question.fName,'\"                  onfocus=\"TurnTo.inputFocus(this)\" onblur=\"TurnTo.inputLoseFocus(this)\">Enter your reply here. We&#39;ll email it to ',TurnTo.matchData.user.id == question.uid ? answer.fName : question.fName,'</textarea>        <div id=\"TT2anwerCharCount-',qarid,'\" class=\"TT2charLimitCount TT2right\"></div>        <div style=\"margin-top:10px; clear:both; display:none\">            <input type=\"checkbox\" id=\"TT2answerNotify\" checked=\"true\"/>&nbsp;Tell me when other people answer or            comment on this.        </div>        <div class=\"TT2left\" style=\"font-size:11px;margin-top:10px;\">            <div id=\"TT2answerBtnPanel\" class=\"TT2right\">                <img id=\"TT2imgLoading-',qarid,'\" class=\"TT2imgLoading\"                     src=\"',TurnTo.getStaticUrl('/images/spinner.gif'),'\"                     alt=\"Loading...\"                     style=\"display: none;\">                <input id=\"TT2answerBtn-',qarid,'\" type=\"button\" class=\"TT2buttonSub\" value=\"\"                       style=\"height:29px; width:91px;\">            </div>            Do not include: HTML, references to other stores, pricing, contact information (inc. your email &mdash; you can add that later), or inappropriate language. Posts which include these will be edited or removed.        </div>        <div class=\"TT2clearBoth\"></div>    </div></div>');}return p.join('');"),traLoggedInLayout:new Function("obj","var p=[];with(obj){p.push('');var rowsDisplayed = 0;p.push('');for (var page = 0; page < numOfPages; page++) {p.push('<div class=\"TT2LoggedInItems\">    ');for (var row = 0; row < rowsPerPage; row++) {        if (rowsDisplayed > matches.length - 1) {            break;        }p.push('    <hr style=\"border:#eee solid 1px\">        <div id=\"TT2liuserphoto\">            <img style=\"display:block\" src=\"', TurnTo.getUserPhoto(matches[rowsDisplayed].uid, matches[rowsDisplayed].pid) ,'\" alt=\"\"/></div>        <div id=\"TT2linameline\" style=\"clear:right\">            <b>', matches[rowsDisplayed].fName ,' ', matches[rowsDisplayed].lName ,'</b>'); if(matches[rowsDisplayed].mtype < 5 && matches[rowsDisplayed].mtype + matches[rowsDisplayed].stype > 0){ p.push(', '); } p.push('', TurnTo.userStatusDisplayLI(matches[rowsDisplayed], zip, matches[rowsDisplayed].items[0].suid) ,''); if(matches[rowsDisplayed].mtype + matches[rowsDisplayed].stype > 0){ p.push(', '); } p.push(' bought:            <div id=\"TT2loggedInItemMessageArea',matches[rowsDisplayed].items[0].suid,'\" class=\"TT2LoggedInItemMessageArea\" style=\"display:none;\">            </div>        </div>        <div class=\"TT2liitemrow\" style=\"float:right;margin-top:-20px;\">            <div class=\"TTslidebox\" ');if (!TurnTo.quirksMode){p.push('style=\"overflow:hidden;\"');}p.push(' >            <ul class=\"TtLiSlider\" ');if (!TurnTo.quirksMode){p.push('style=\"overflow:hidden;\"');}p.push('>            <li style=\"list-style:none;float:left;display:inline;\">  '); var maxcol2display=(TurnTo.quirksMode)?5:matches[rowsDisplayed].items.length; for (var xi=0; (xi< maxcol2display  && xi< matches[rowsDisplayed].items.length) ; xi++) {               var item = matches[rowsDisplayed].items[xi];        p.push('            '); if(xi != 0 && xi % 5 == 0){  p.push('               </li><li>            '); } p.push('            <div class=\"TT2liitem TTitemRollOverLi\" style=\"text-align:left;\">                    <div class=\"TT2liitemBox\" style=\"text-align:left;background:url(', TurnTo.getProductImage(item.catImgId,'SS') ,') center no-repeat;height:80px;\">                        <!-- rollover -->                        <div style=\"position:absolute\">                            <div class=\"TTrolloverHotArea\"                                 style=\"position:absolute;width:110px;height:90px;display:none;z-index:129001;margin: -5px 0 0 -5px;\">                                <div class=\"TurnToRolloLiAskButton1\" matchId=\"TT2askBtn-',rowsDisplayed,'-',xi,'\"                                     style=\"padding-top:0px;margin:0px 0px 0px 0px;height:49px;width:115px;  \">                                </div>                                <div class=\"TurnToRolloLiItemButton1\" rave=\"', item.rave ,'\"  matchId=\"',item.iid,'\" uid=\"', matches[rowsDisplayed].uid ,'\"                                     style=\"padding-top:0px;margin:0px 0px 0px 0px;height:40px;width:110px;   \">                                </div>                            </div>                            <div style=\"display: none; top:-20px;left: 109px;z-index:129000; width:235px;padding-top:10px;\"                                 id=\"TTloggedOutRollo1Box\" class=\"TTloggedOutDetailBox TurnTorolloverDetail\">                                <div style=\"width:235px;margin-top:0px;\">                                 <div class=\"TT2commentSpin\" style=\"width:235px;text-align:center;display:none\" align=\"center\"><img src=\"',TurnTo.getStaticUrl('/tra/images/traspin.gif') ,'\" alt=\"\"  /></div>                                 <div class=\"TTbluecommentbox TTbluebox\" style=\"display:none;white-space:normal;\">                                    <div class=\"TTloggedOutRollo1name\"></div>                                    <div class=\"TTloggedOutRollo1Comments\"></div>                                </div>                                <img src=\"\"                                     thephoto=\"', TurnTo.getProductImage(item.catImgId,'L') ,'\"                                     class=\"TTloggedOutRollo1Img\"                                     style=\"margin-left:10px; border: medium none; display: block;\"                                     align=\"center\" width=\"220\">                                <div class=\"TTloggedOutRollo1Title\" style=\"text-align:center;padding:15px;white-space:normal;\">', item.title ,'</div>                            </div>                            </div>                        </div>                        '); if(item.rave){ p.push('                    <div id=\"TTcommentBub2\" border=\"0\" width=\"14\" height=\"14\">&nbsp;</div>                    ');}p.push('                    <a href=\"javascript:void(0)\" id=\"TT2loggedInItemImgLink',rowsDisplayed,'',xi,'\" >                        <div class=\"TTloggedInRollo\" catImgId=\"', item.catImgId ,'\" itemTitle=\"', item.title ,'\" rave=\"', item.rave ,'\" ttuid=\"', matches[rowsDisplayed].uid ,'\" itemid=\"', item.iid ,'\" style=\"height:76px; width:76px;\">&nbsp;</div>                        <!--<img  src=\"xxxxxx\" alt=\"\" width=\"80\" height=\"80\" class=\"TTloggedOutRollo\" catImgId=\"', item.catImgId ,'\"/>-->                    </a>                                        </div>                <p id=\"TT2liitemtitle\">                <a href=\"javascript:void(0)\" id=\"TT2loggedInItemTitleLink',rowsDisplayed,'',xi,'\">', item.title ,'</a>                </p>                <div class=\"TTrolloverBack\" style=\"display:none;\"></div>           <div class=\"TTrolloverButtons\"                style=\"background-color:transparent;display:none;text-align:center;\">           </div>                            </div>        '); } p.push('            </ul>                <div class=\"TTnext TTscrollArrowL1\" style=\"display:none;float:right;width:12px;height:12px;margin-top:10px\"></div>                <div class=\"TTprev TTscrollArrowR1\" style=\"display:none;float:right;width:12px;height:12px;margin-top:10px;margin-right:5px\"></div>            </div>                    </div>    <br style=\"clear:right\"/>    ');    rowsDisplayed++;    }p.push('</div>');}p.push('<br><div class=\"TT2navLinks\">    <a href=\"javascript:void(0)\" id=\"TT2nextLink\">Show More ></a></div>');}return p.join('');"),traLoggedOutLayout:new Function("obj","var p=[];with(obj){p.push('');var colsPerRow = 3;var rowPerItemTable = 6;var itemIndex = 0;var boxesPerPage = 18;var allItemsLengthIncludingReg = matches.length;var numOfPages = parseInt(allItemsLengthIncludingReg / boxesPerPage) + (allItemsLengthIncludingReg % boxesPerPage == 0 ? 0 : 1);p.push('');    for (var page = 0; page < numOfPages; page++) {p.push('<div class=\"TT2LoggedOutItems\">    <table>        ');for (var row = 0; row < rowPerItemTable; row++) {p.push('        <tr>            ');for (var col = 0; col < colsPerRow; col++) {            if (itemIndex >= matches.length) {                break;            }p.push('            <td>                <div class=\"TTrolloverButtons\"                     style=\"position:absolute;width:220px;height:220px;background-color:transparent;display:none;text-align:center;\">                    <div style=\"display: none; left: 212px; top: -20px;;z-index:129000; width:235px;min-height:300px;padding-top:10px;\"                         id=\"TTloggedOutRollo1Box\" class=\"TTrolloverShadow TTloggedOutDetailBox TurnTorolloverDetail\">                        <div style=\"width:235px;margin-top:0px;text-align:center\" align=\"center\">                            <div class=\"TT2commentSpin\" style=\"width:235px;text-align:center;display:none\" align=\"center\"><img src=\"',TurnTo.getStaticUrl('/tra/images/traspin.gif') ,'\" alt=\"\"  /></div>                            <div class=\"TTbluecommentbox TTbluebox\" style=\"display:none;\">                                <div class=\"TTloggedOutRollo1name\"></div>                                <div class=\"TTloggedOutRollo1Comments\"></div>                            </div>                            <img src=\"\"                                 thephoto=\"', TurnTo.getProductImage(matches[itemIndex].catImgId,'L') ,'\"                                 class=\"TTloggedOutRollo1Img\"                                 style=\"border:none; display: block;margin-left:10px\"                                 align=\"center\" width=\"215\" border=\"none\">                            <div class=\"TTloggedOutRollo1Title\" style=\"text-align:center;padding:15px;\">', matches[itemIndex].title ,'</div>                        </div>                    </div>                </div>                <div class=\"TTrolloverHotArea\"                     style=\"position:absolute;width:197px;height:220px;background-color:transparent;display:none;z-index:129001\">                    <div class=\"TurnToRolloLoAskButton1\"  id=\"TT2askBtn-',itemIndex,'\"                         style=\"margin:69px 0px 0px 5px;height:84px;width:208px;  text-align:center;\">                    </div>                    <div class=\"TurnToRolloLoItemButton1\" matchindex=\"', itemIndex ,'\"                         style=\"padding-top:0px;margin:0px 0px 0px 5px;height:55px;width:202px;  text-align:center;\">                    </div>                </div>                <div class=\"TT2loblock\">                    <div style=\"padding-top:8px; font-size:11px;\">                        <div class=\"TT2traUserPhoto1\"><img style=\"display:inline\"                                                           src=\"', TurnTo.getUserPhoto(matches[itemIndex].uid, matches[itemIndex].pid) ,'\"                                                           alt=\"\"/></div>                        <p style=\"margin-bottom:14px;padding-right:8px;padding-left:8px;height:50px;font-size:12px;\">                            <span style=\"font-size:13px;font-weight:bold\">', matches[itemIndex].fName ,' ', matches[itemIndex].lName ,'</span>',                            userStatusDisplay(matches[itemIndex].mtype ,matches[itemIndex].stype                            ,matches[itemIndex].pCode ) ,'                        </p>                        <div style=\"padding-top:0;text-align:center;width:198px;border:#000 solid 0px;\">                            '); if(matches[itemIndex].rave){ p.push('                            <div id=\"TTcommentBub\" border=\"0\" width=\"17\" height=\"16\">&nbsp;</div>                            ');}p.push('                            <a href=\"javascript:void(0)\" id=\"TT2loggedOutItemImgLink',itemIndex,'\"><img style=\"cursor: pointer;\" class=\"TTitemRollOver\"                                               src=\"', TurnTo.getProductImage(matches[itemIndex].catImgId,'S') ,'\"                                               alt=\"\" /></a>                        </div>                    </div>                    <div style=\"text-align:center;width:198px;\">                        <span class=\"TT2productTitle\"><a href=\"javascript:void(0)\" style=\"color:#6d89c0\" id=\"TT2loggedOutItemTitleLink',itemIndex,'\">                            ', truncate(matches[itemIndex].title, 25, \"&#8230;\") ,'</a>                                             </div>                </div>                ');                itemIndex++;                p.push('            </td>            ');            }p.push('        </tr>        ');}p.push('    </table></div>');}p.push('<img id=\"TT2tipBox\" src=\"',TurnTo.getStaticUrl('/tra2/images/tip-box.png'),'\" alt=\"\" style=\"display:',showToolTip?'':'none','\"/><div class=\"TT2navLinks\">    <a href=\"javascript:void(0)\" id=\"TT2nextLink\">Show More ></a></div>');}return p.join('');"),userSetting:new Function("obj","var p=[];with(obj){p.push('<div id=\"TT2userSetting\">    <div id=\"userSettingErrors\">    </div>    <form id=\"profileForm\" name=\"profileForm\" method=\"post\" style=\"width:490px\">          <input type=\"hidden\" value=\"',matches.user.id,'\" name=\"id\">          <div class=\"dialog\" style=\"margin-bottom: 5px;\">          <div style=\"border: 1px solid rgb(204, 204, 204);\">              <table style=\"width: 490px; border: medium none;\">                <tbody>                  <tr class=\"prop\">                        <td valign=\"top\" colspan=\"2\" style=\"text-align: left;\" class=\"nameRF\">                            <b>Basic Information</b>                        </td>                  </tr>                  <tr class=\"prop\">                    <td valign=\"top\" class=\"nameRF\">                      <label for=\"firstName\">*First Name:</label>                    </td>                    <td valign=\"top\" class=\"valueL \">                      <input type=\"text\" value=\"',matches.user.firstName,'\" maxlength=\"200\" name=\"firstName\" id=\"firstName\">                    </td>                  </tr>                  <tr class=\"prop\">                    <td valign=\"top\" class=\"nameRF\">                      <label for=\"lastName\">*Last Name:</label>                    </td>                    <td valign=\"top\" class=\"valueL \">                      <input type=\"text\" value=\"',matches.user.lastName,'\" maxlength=\"200\" id=\"lastName\" name=\"lastName\">                    </td>                  </tr>                  <tr class=\"prop\">                    <td valign=\"top\" class=\"nameRF\">                    </td>                    <td valign=\"top\" class=\"valueL\">                        <a href=\"javascript:void(0)\" id=\"pwd_change\">',matches.user.setting.defaultPassword ? \"Create\" : \"Change\",' password</a>                        <a style=\"display: none;\" href=\"javascript:void(0)\" id=\"pwd_nochange\">Do not ',matches.user.setting.defaultPassword ? \"create\" : \"change\",' password</a>                        <input type=\"hidden\" value=\"false\" id=\"editingPasswd\" name=\"editingPasswd\">                    </td>                  </tr>                  ');if (!matches.user.setting.defaultPassword) {p.push('                  <tr style=\"display: none;\" id=\"currpasswd\" class=\"prop\">                    <td valign=\"top\" class=\"nameRF\">                      <label for=\"currpasswd\">Current Password:</label>                    </td>                    <td valign=\"top\" class=\"valueL \">                      <input type=\"password\" maxlength=\"200\" value=\"\" name=\"currpasswd\">                    </td>                  </tr>                  ');}p.push('                  <tr style=\"display: none;\" id=\"pwd_1\" class=\"prop\">                    <td valign=\"top\" class=\"nameRF\">                      New Password:</label>                    </td>                    <td valign=\"top\" class=\"valueL \">                      <input type=\"password\" maxlength=\"200\" value=\"\" name=\"passwd\">                    </td>                  </tr>                  <tr style=\"display: none;\" id=\"pwd_2\" class=\"prop\">                    <td valign=\"top\" class=\"nameRF\">                      <label for=\"repasswd\">Confirm Password:</label>                    </td>                    <td valign=\"top\" class=\"valueL \">                      <input type=\"password\" maxlength=\"200\" value=\"\" name=\"repasswd\">                    </td>                  </tr>                </tbody>              </table>          </div>          <div style=\"border-right: 1px solid rgb(204, 204, 204); border: rgb(204, 204, 204); border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(204, 204, 204) rgb(204, 204, 204);\">            <table style=\"width: 490px; border: medium none;\">                <tbody>                <tr class=\"prop\">                    <td valign=\"top\" style=\"text-align: left;\" class=\"nameRF\">                        <b>Photo</b>                    </td>                    <td valign=\"top\" class=\"valueL\">                        <a id=\"showPhotoEditorLink1\" href=\"javascript:void(0)\" style=\"',matches.user.setting.hasPhoto ? '' : 'display:none','\">                            <img style=\"float: left; border: 1px solid rgb(204, 204, 204); width: 45px; height: 45px;\"                                 src=\"',TurnTo.getUserPhoto(matches.user.id, matches.user.pid),'\" id=\"userImg\">                        </a>                        <img style=\"float: left; border: 1px solid rgb(204, 204, 204); width: 45px; height: 45px; display: none;\"                             alt=\"Loading...\" src=\"',getStaticUrl('/images/spinner.gif'),'\" id=\"userImgLoading\">                        &nbsp;<a href=\"javascript:void(0)\" id=\"showPhotoEditorLink2\">',matches.user.setting.hasPhoto ?                        'Change Photo' : 'Add Photo','</a>                    </td>                </tr>                <tr id=\"editPhoto\" style=\"display:none\" class=\"prop\">                    <td valign=\"top\" class=\"nameRF\">                                            </td>                    <td valign=\"top\" class=\"valueL\">                        <img id=\"tmpUserImg\" src=\"');if (matches.user.setting.hasPhoto) {p.push('',TurnTo.getUserPhoto(TurnTo.matchData.user.id, TurnTo.matchData.user.pid),'');}p.push('\"                             style=\"border: 1px solid rgb(204, 204, 204); width:45px;height:45px;',matches.user.setting.hasPhoto? '' :'display:none','\"/>                        <img id=\"tmpUserImgLoading\" src=\"',getStaticUrl('/images/spinner.gif'),'\"                             alt=\"Loading...\"                             style=\"border: 1px solid rgb(204, 204, 204); width:45px;height:45px; display: none;\">                        <br/>                        &nbsp;<input type=\"file\" name=\"TT2userImgFile\" id=\"TT2userImgFile\"                               onchange=\"TurnTo.inputUserImageForUserSetting(this)\"/><br/>                        &nbsp;Select an image file on your computer (4MB max)                        <div style=\"clear:both\"/>                        <span id=\"TT2customUserImgRadio\" style=\"',matches.user.setting.hasPhoto ? '' : 'display:none','\">                            <input type=\"radio\" name=\"userImgSelection\" value=\"custom\" ',matches.user.setting.hasPhoto && matches.user.setting.photoId.indexOf('fb') < 0 ? 'checked=\"checked\"' : '','/> use custom photo                        </span>                        <br/><br/>                        &nbsp;&nbsp;&nbsp;&nbsp;or                        <br/><br/>                        <span id=\"TT2fbNotConnected\" style=\"',matches.user.isFbConnected ? 'display:none' : '','\">                            <a href=\"javascript:void(0)\" id=\"TT2userSettingFbConnect\" style=\"vertical-align:middle\">                                <img src=\"',TurnTo.getStaticUrl('/tra2/images/fb-login-s1.png') ,'\" alt=\"Connect\" style=\"border:none\"/>                            </a>                            to use your Facebook photo                        </span>                        <span id=\"TT2fbConnected\" style=\"',!matches.user.isFbConnected ? 'display:none' : '','\">                            <span><input type=\"radio\" name=\"userImgSelection\" value=\"facebook\" ',matches.user.setting.hasPhoto && matches.user.setting.photoId.indexOf('fb') > 0 ? 'checked=\"checked\"' : '','/> use facebook photo</span>                        </span>                    </td>                </tr>                <tr id=\"editPhotoButtons\" style=\"display:none\" class=\"prop\">                    <td>                    </td>                    <td>                        <input type=\"button\" id=\"savePhotoButton\" value=\"Save\"/>                        <input type=\"button\" id=\"closePhotoButton\" value=\"Cancel\"/>                    </td>                </tr>                </tbody>            </table>          </div>          <div style=\"border-right: 1px solid rgb(204, 204, 204); border: rgb(204, 204, 204); border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(204, 204, 204) rgb(204, 204, 204);\" id=\"contactInfo\">          </div>          ');if (matches.user.setting.showSharingPref) {p.push('          <div style=\"border-right: 1px solid rgb(204, 204, 204); border-left: rgb(204, 204, 204); border-bottom: rgb(204, 204, 204); border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(204, 204, 204) rgb(204, 204, 204);\">              <table style=\"border: medium none; width: 490px;\">                  <tbody>                  <tr class=\"prop\">                      <td valign=\"top\" style=\"text-align: left;\" class=\"nameRF\">                          <b>Sharing</b>                      </td>                      <td valign=\"top\" class=\"valueL\">                          <input type=\"checkbox\" name=\"sharing\" id=\"sharing\" value=\"true\" ',matches.user.setting.sharing ? 'checked=\"checked\"' : '',' /> Allow my friends and other shoppers to see what I bought                      </td>                  </tr>                  </tbody>              </table>          </div>          ');}p.push('          ');if (matches.user.setting.showAskPref) {p.push('          <div style=\"border-right: 1px solid rgb(204, 204, 204); border-left: rgb(204, 204, 204); border-bottom: rgb(204, 204, 204); border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(204, 204, 204) rgb(204, 204, 204);\">              <table style=\"border: medium none; width: 490px;\">                  <tbody>                  <tr class=\"prop\">                      <td valign=\"top\" style=\"text-align: left;\" class=\"nameRF\">                          <b>Q & A</b>                      </td>                      <td valign=\"top\" class=\"valueL\">                          <input type=\"checkbox\" name=\"askMe\" id=\"askMe\" value=\"true\" ',matches.user.setting.askOptOut ? '' : 'checked=\"checked\"',' /> Receive questions from other shoppers.                      </td>                  </tr>                  </tbody>              </table>                            </div>          ');}p.push('                      </div>            <div style=\"float: right;\" class=\"buttons\">              <span class=\"button\">                  <input id=\"updateButton\" type=\"button\" value=\"Update\" name=\"update\"/>                  <img style=\"float: left; border: 1px solid rgb(204, 204, 204); width: 45px; height: 45px; display: none;\"                             alt=\"Loading...\" src=\"',getStaticUrl('/images/spinner.gif'),'\" id=\"updateLoading\"/>              </span>            </div>            <div style=\"clear: both;\"></div>      </form></div>');}return p.join('');"),userSettingEmailAddresses:new Function("obj","var p=[];with(obj){p.push('      <table style=\"border: medium none; width: 490px;\">        <tbody>            <tr class=\"prop\">                <td valign=\"top\" colspan=\"2\" style=\"text-align: left;\" class=\"nameRF\">                    <b>Email Addresses</b>                </td>            </tr>              '); var confirmedEmails = [];                 var unconfirmedEmails = [];                p.push('              ');for (var i = 0; i < matches.user.setting.emailIdentifiers.length; i++) {                        if (!matches.user.setting.emailIdentifiers[i].isPrimary && matches.user.setting.emailIdentifiers[i].isConfirmed) {                            confirmedEmails[confirmedEmails.length] = matches.user.setting.emailIdentifiers[i];                        } else if (!matches.user.setting.emailIdentifiers[i].isConfirmed) {                            unconfirmedEmails[unconfirmedEmails.length] = matches.user.setting.emailIdentifiers[i];                        } else if (matches.user.setting.emailIdentifiers[i].isPrimary) {p.push('                          <tr class=\"prop\">                            <td valign=\"top\" class=\"nameRF\">                                <label for=\"email\">Primary Email:</label>                            </td>                            <td valign=\"top\" class=\"valueL\">                            ',matches.user.setting.emailIdentifiers[i].ident,'                            </td>                          </tr>              ');        }               }p.push('              ');for (var i = 0; i < confirmedEmails.length; i++) {p.push('              <tr class=\"prop\">                <td valign=\"top\" class=\"nameRF\">                    <label for=\"email\">Email:</label>                </td>                <td valign=\"top\" class=\"valueL\">                            ',confirmedEmails[i].ident,'                    <a onclick=\"TurnTo.makePrimaryIdent(',confirmedEmails[i].id,')\" href=\"javascript:void(0)\">(make primary)</a>                    &nbsp;<a onclick=\"TurnTo.removeIdent(',confirmedEmails[i].id,')\" href=\"javascript:void(0)\"><img border=\"0\" alt=\"remove\" src=\"',getStaticUrl('/images/tabClose.gif'),'\" style=\"margin-left: 5px; vertical-align: middle;\"></a>                </td>              </tr>              ');}p.push('              ');for (var i = 0; i < unconfirmedEmails.length; i++) {p.push('              <tr class=\"prop\">                <td valign=\"top\" class=\"nameRF\">                    <label for=\"email\">Email:</label>                </td>                <td valign=\"top\" class=\"valueL\">                            ',unconfirmedEmails[i].ident,' - Unconfirmed                    <a onclick=\"TurnTo.resendEmailConfirmation(&#39;',unconfirmedEmails[i].ident,'&#39;, function(){})\" href=\"javascript:void(0)\">(Resend confirmation email)</a>                    &nbsp;<a onclick=\"TurnTo.removeIdent(',unconfirmedEmails[i].id,')\" href=\"javascript:void(0)\"><img border=\"0\" alt=\"remove\" src=\"',getStaticUrl('/images/tabClose.gif'),'\" style=\"margin-left: 5px; vertical-align: middle;\"></a>                </td>              </tr>              ');}p.push('         </tbody>      </table>      <table style=\"border: medium none; float: left; width: 315px; padding-top: 0pt;\">        <tbody>            <tr style=\"padding-top: 0pt;\" class=\"prop\">                <td valign=\"top\" class=\"nameRF\">                    <label for=\"email\">Add Email:</label>                </td>                <td valign=\"top\" class=\"valueL \" style=\"width: 175px\">                    <input type=\"text\" value=\"\" maxlength=\"200\" name=\"newEmail0\" id=\"newEmail0\">                </td>            </tr>            <tr class=\"prop\">                <td valign=\"top\" class=\"nameRF\">                </td>                <td valign=\"top\" class=\"valueL \" style=\"width: 175px\">                    <input type=\"text\" value=\"\" maxlength=\"200\" name=\"newEmail1\" id=\"newEmail1\">                </td>            </tr>            <tr class=\"prop\">                <td valign=\"top\" class=\"nameRF\">                </td>                <td valign=\"top\" class=\"valueL \" style=\"width: 175px\">                    <input type=\"text\" value=\"\" maxlength=\"200\" name=\"newEmail2\" id=\"newEmail2\">                </td>            </tr>        </tbody>      </table>      <div style=\"clear: both;\"></div>');}return p.join('');"),whyshouldi:new Function("obj","var p=[];with(obj){p.push('<div style=\"overflow-y:auto;height:300px;padding:10px;padding-top:0px\"><div><b>Why should I connect?</b></div> <div class=\"TTwhyshould2\">We&#39;ll check if your friends have purchased here before and tell you what they bought.  Then, if you have questions about what to get, you&#39;ll know who you can turn to for advice.  And if you&#39;re looking for inspiration, it&#39;s a great way to spark ideas.</div>  <br> <div><b>How long will this take?</b></div> <div  class=\"TTwhyshould2\">Just a few seconds.</div>  <br> <div><b>Will my friends get spammed?</b></div> <div  class=\"TTwhyshould2\">No.  Your friends will not be contacted by anyone.  And nothing with be posted to your or their Facebook pages.  Your friends list is used only to find \"trusted references\" for you.</div>  <br> <div><b>Will I get spammed?</b></div> <div class=\"TTwhyshould2\">No.  You will not be added to any mailing lists.</div>  <br> <div><b>Does this mean that purchases here aren&#39;t private?</b></div> <div  class=\"TTwhyshould2\">No way!  We take your privacy very seriously.  Shoppers&#39; personally - identifiable information ONLY appears when they have given permission.  All other information is anonymous.</div>  <br> <div><b>Who is TurnTo?</b></div> <div  class=\"TTwhyshould2\">TurnTo is a trusted partner of ours &#151; they&#39;re the software company that provides our trusted reference system.  You can use TurnTo to find references when you shop at any online store or travel site in the TurnTo network.  For more information, see <a href=\"http://www.turnto.com\" target=\"_blank\">www.turnto.com</a>.</div></div>');}return p.join('');"),zipForm:new Function("obj","var p=[];with(obj){p.push('<div style=\"border:#ccc solid 5px;width:180px;padding:10px;\">    <div id=\"TTZipFormMessage\" style=\"padding:0;display:none\"></div>    <form id=\"TTzipForm\">        <p id=\"TT2zipReadyP\" style=\"font-size:12px;padding-bottom:10px;font-weight:bold;\">           <span class=\"TT2textColor1\">Neighbors</span> in zip <span id=\"TT2userZip\">', zip ,'</span> ',            turnToSettings.verb ,' here.        </p>        <p id=\"TT2noZipP\" style=\"font-size:12px;padding-bottom:10px;font-weight:bold;\">           See what your <span class=\"TT2textColor1\">neighbors</span> ',turnToSettings.verb ,' here.        </p>        <a id=\"TT2changeZipLink\" href=\"javascript:void(0)\" style=\"font-size:10px;float:left\">Change zip</a>        <div id=\"TT2zipInput\">            <input type=\"text\" class=\"TTdefaultInput\" id=\"TTzip\" ttInputLabelText=\"Zipcode\"                    onfocus=\"TurnTo.inputFocus(this)\"                    onblur=\"TurnTo.inputLoseFocus(this)\" value=\"Zipcode\"/>            <input type=\"submit\" value=\"Ok\" class=\"TTsubmitButton\" id=\"TTzipSubmit\"/>        </div>        <div style=\"clear:both\"/>    </form></div>');}return p.join('');"),zipRow:new Function("obj","var p=[];with(obj){p.push('<div class=\"TTmatchDetail TTraveArea\" style=\"display: block;\">    <div class=\"TTbullet\">');if (m.url) {p.push('        <a class=\"TTtransactionItemLink\"           href=\"', matchLinkRoot ,'&pHref=', encodeURIComponent(m.url) ,'&pd=&v=', TurnTo.version  ,'&mId=', m.id ,'&tId=0&tiId=', m.id ,' \">            ', m.title ,'</a>');} else {p.push('<i>', m.title ,'</i>');}p.push('        '); if ( m.raveCommentCount >0) { p.push('        <img class=\"TTraveLink\" src=\"', TurnTo.getStaticUrl('/tra/images/starwithdoc.png'),'\" border=\"0\"             style=\"padding-left:0\"/>        '); } else if (m.raveCount > 0){ p.push('        <img class=\"TTraveLink\" src=\"', TurnTo.getStaticUrl('/tra/images/star.png'),'\" border=\"0\" style=\"padding-left:0\"/>        '); } p.push('        <div style=\"display: none;\" class=\"TTraveOuterBox\" sku=\"', m.sku ,'\">            <div class=\"TTraveTitle\"><img src=\"', TurnTo.getStaticUrl('/tra/images/star.png'),'\" border=\"0\"/>                ',m.raveCount,' Neighbor',m.raveCount==1?'':'s',' recommend this            </div>            '); if ( m.raveCommentCount >0) { p.push('            <div class=\"TTraveCommentBox\"></div>            '); } p.push('        </div>    </div></div>');}return p.join('');")};

/*!
 * jQuery JavaScript Library v1.3.2
 * http://jquery.com/
 *
 * Copyright (c) 2009 John Resig
 * Dual licensed under the MIT and GPL licenses.
 * http://docs.jquery.com/License
 *
 * Date: 2009-02-19 17:34:21 -0500 (Thu, 19 Feb 2009)
 * Revision: 6246
 */
(function(){

var 
	// Will speed up references to window, and allows munging its name.
	window = this,
	// Will speed up references to undefined, and allows munging its name.
	undefined,
	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,
	// Map over the $ in case of overwrite
	_$ = window.$,

	jQuery = window.jQuery = window.$ = function( selector, context ) {
		// The jQuery object is actually just the init constructor 'enhanced'
		return new jQuery.fn.init( selector, context );
	},

	// A simple way to check for HTML strings or ID strings
	// (both of which we optimize for)
	quickExpr = /^[^<]*(<(.|\s)+>)[^>]*$|^#([\w-]+)$/,
	// Is it a simple selector
	isSimple = /^.[^:#\[\.,]*$/;

jQuery.fn = jQuery.prototype = {
	init: function( selector, context ) {
		// Make sure that a selection was provided
		selector = selector || document;

		// Handle $(DOMElement)
		if ( selector.nodeType ) {
			this[0] = selector;
			this.length = 1;
			this.context = selector;
			return this;
		}
		// Handle HTML strings
		if ( typeof selector === "string" ) {
			// Are we dealing with HTML string or an ID?
			var match = quickExpr.exec( selector );

			// Verify a match, and that no context was specified for #id
			if ( match && (match[1] || !context) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[1] )
					selector = jQuery.clean( [ match[1] ], context );

				// HANDLE: $("#id")
				else {
					var elem = document.getElementById( match[3] );

					// Handle the case where IE and Opera return items
					// by name instead of ID
					if ( elem && elem.id != match[3] )
						return jQuery().find( selector );

					// Otherwise, we inject the element directly into the jQuery object
					var ret = jQuery( elem || [] );
					ret.context = document;
					ret.selector = selector;
					return ret;
				}

			// HANDLE: $(expr, [context])
			// (which is just equivalent to: $(content).find(expr)
			} else
				return jQuery( context ).find( selector );

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( jQuery.isFunction( selector ) )
			return jQuery( document ).ready( selector );

		// Make sure that old selector state is passed along
		if ( selector.selector && selector.context ) {
			this.selector = selector.selector;
			this.context = selector.context;
		}

		return this.setArray(jQuery.isArray( selector ) ?
			selector :
			jQuery.makeArray(selector));
	},

	// Start with an empty selector
	selector: "",

	// The current version of jQuery being used
	jquery: "1.3.2",

	// The number of elements contained in the matched element set
	size: function() {
		return this.length;
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {
		return num === undefined ?

			// Return a 'clean' array
			Array.prototype.slice.call( this ) :

			// Return just the object
			this[ num ];
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems, name, selector ) {
		// Build a new jQuery matched element set
		var ret = jQuery( elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;

		ret.context = this.context;

		if ( name === "find" )
			ret.selector = this.selector + (this.selector ? " " : "") + selector;
		else if ( name )
			ret.selector = this.selector + "." + name + "(" + selector + ")";

		// Return the newly-formed element set
		return ret;
	},

	// Force the current matched set of elements to become
	// the specified array of elements (destroying the stack in the process)
	// You should use pushStack() in order to do this, but maintain the stack
	setArray: function( elems ) {
		// Resetting the length to 0, then using the native Array push
		// is a super-fast way to populate an object with array-like properties
		this.length = 0;
		Array.prototype.push.apply( this, elems );

		return this;
	},

	// Execute a callback for every element in the matched set.
	// (You can seed the arguments with an array of args, but this is
	// only used internally.)
	each: function( callback, args ) {
		return jQuery.each( this, callback, args );
	},

	// Determine the position of an element within
	// the matched set of elements
	index: function( elem ) {
		// Locate the position of the desired element
		return jQuery.inArray(
			// If it receives a jQuery object, the first element is used
			elem && elem.jquery ? elem[0] : elem
		, this );
	},

	attr: function( name, value, type ) {
		var options = name;

		// Look for the case where we're accessing a style value
		if ( typeof name === "string" )
			if ( value === undefined )
				return this[0] && jQuery[ type || "attr" ]( this[0], name );

			else {
				options = {};
				options[ name ] = value;
			}

		// Check to see if we're setting style values
		return this.each(function(i){
			// Set all the styles
			for ( name in options )
				jQuery.attr(
					type ?
						this.style :
						this,
					name, jQuery.prop( this, options[ name ], type, i, name )
				);
		});
	},

	css: function( key, value ) {
		// ignore negative width and height values
		if ( (key == 'width' || key == 'height') && parseFloat(value) < 0 )
			value = undefined;
		return this.attr( key, value, "curCSS" );
	},

	text: function( text ) {
		if ( typeof text !== "object" && text != null )
			return this.empty().append( (this[0] && this[0].ownerDocument || document).createTextNode( text ) );

		var ret = "";

		jQuery.each( text || this, function(){
			jQuery.each( this.childNodes, function(){
				if ( this.nodeType != 8 )
					ret += this.nodeType != 1 ?
						this.nodeValue :
						jQuery.fn.text( [ this ] );
			});
		});

		return ret;
	},

	wrapAll: function( html ) {
		if ( this[0] ) {
			// The elements to wrap the target around
			var wrap = jQuery( html, this[0].ownerDocument ).clone();

			if ( this[0].parentNode )
				wrap.insertBefore( this[0] );

			wrap.map(function(){
				var elem = this;

				while ( elem.firstChild )
					elem = elem.firstChild;

				return elem;
			}).append(this);
		}

		return this;
	},

	wrapInner: function( html ) {
		return this.each(function(){
			jQuery( this ).contents().wrapAll( html );
		});
	},

	wrap: function( html ) {
		return this.each(function(){
			jQuery( this ).wrapAll( html );
		});
	},

	append: function() {
		return this.domManip(arguments, true, function(elem){
			if (this.nodeType == 1)
				this.appendChild( elem );
		});
	},

	prepend: function() {
		return this.domManip(arguments, true, function(elem){
			if (this.nodeType == 1)
				this.insertBefore( elem, this.firstChild );
		});
	},

	before: function() {
		return this.domManip(arguments, false, function(elem){
			this.parentNode.insertBefore( elem, this );
		});
	},

	after: function() {
		return this.domManip(arguments, false, function(elem){
			this.parentNode.insertBefore( elem, this.nextSibling );
		});
	},

	end: function() {
		return this.prevObject || jQuery( [] );
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: [].push,
	sort: [].sort,
	splice: [].splice,

	find: function( selector ) {
		if ( this.length === 1 ) {
			var ret = this.pushStack( [], "find", selector );
			ret.length = 0;
			jQuery.find( selector, this[0], ret );
			return ret;
		} else {
			return this.pushStack( jQuery.unique(jQuery.map(this, function(elem){
				return jQuery.find( selector, elem );
			})), "find", selector );
		}
	},

	clone: function( events ) {
		// Do the clone
		var ret = this.map(function(){
			if ( !jQuery.support.noCloneEvent && !jQuery.isXMLDoc(this) ) {
				// IE copies events bound via attachEvent when
				// using cloneNode. Calling detachEvent on the
				// clone will also remove the events from the orignal
				// In order to get around this, we use innerHTML.
				// Unfortunately, this means some modifications to
				// attributes in IE that are actually only stored
				// as properties will not be copied (such as the
				// the name attribute on an input).
				var html = this.outerHTML;
				if ( !html ) {
					var div = this.ownerDocument.createElement("div");
					div.appendChild( this.cloneNode(true) );
					html = div.innerHTML;
				}

				return jQuery.clean([html.replace(/ jQuery\d+="(?:\d+|null)"/g, "").replace(/^\s*/, "")])[0];
			} else
				return this.cloneNode(true);
		});

		// Copy the events from the original to the clone
		if ( events === true ) {
			var orig = this.find("*").andSelf(), i = 0;

			ret.find("*").andSelf().each(function(){
				if ( this.nodeName !== orig[i].nodeName )
					return;

				var events = jQuery.data( orig[i], "events" );

				for ( var type in events ) {
					for ( var handler in events[ type ] ) {
						jQuery.event.add( this, type, events[ type ][ handler ], events[ type ][ handler ].data );
					}
				}

				i++;
			});
		}

		// Return the cloned set
		return ret;
	},

	filter: function( selector ) {
		return this.pushStack(
			jQuery.isFunction( selector ) &&
			jQuery.grep(this, function(elem, i){
				return selector.call( elem, i );
			}) ||

			jQuery.multiFilter( selector, jQuery.grep(this, function(elem){
				return elem.nodeType === 1;
			}) ), "filter", selector );
	},

	closest: function( selector ) {
		var pos = jQuery.expr.match.POS.test( selector ) ? jQuery(selector) : null,
			closer = 0;

		return this.map(function(){
			var cur = this;
			while ( cur && cur.ownerDocument ) {
				if ( pos ? pos.index(cur) > -1 : jQuery(cur).is(selector) ) {
					jQuery.data(cur, "closest", closer);
					return cur;
				}
				cur = cur.parentNode;
				closer++;
			}
		});
	},

	not: function( selector ) {
		if ( typeof selector === "string" )
			// test special case where just one selector is passed in
			if ( isSimple.test( selector ) )
				return this.pushStack( jQuery.multiFilter( selector, this, true ), "not", selector );
			else
				selector = jQuery.multiFilter( selector, this );

		var isArrayLike = selector.length && selector[selector.length - 1] !== undefined && !selector.nodeType;
		return this.filter(function() {
			return isArrayLike ? jQuery.inArray( this, selector ) < 0 : this != selector;
		});
	},

	add: function( selector ) {
		return this.pushStack( jQuery.unique( jQuery.merge(
			this.get(),
			typeof selector === "string" ?
				jQuery( selector ) :
				jQuery.makeArray( selector )
		)));
	},

	is: function( selector ) {
		return !!selector && jQuery.multiFilter( selector, this ).length > 0;
	},

	hasClass: function( selector ) {
		return !!selector && this.is( "." + selector );
	},

	val: function( value ) {
		if ( value === undefined ) {			
			var elem = this[0];

			if ( elem ) {
				if( jQuery.nodeName( elem, 'option' ) )
					return (elem.attributes.value || {}).specified ? elem.value : elem.text;
				
				// We need to handle select boxes special
				if ( jQuery.nodeName( elem, "select" ) ) {
					var index = elem.selectedIndex,
						values = [],
						options = elem.options,
						one = elem.type == "select-one";

					// Nothing was selected
					if ( index < 0 )
						return null;

					// Loop through all the selected options
					for ( var i = one ? index : 0, max = one ? index + 1 : options.length; i < max; i++ ) {
						var option = options[ i ];

						if ( option.selected ) {
							// Get the specifc value for the option
							value = jQuery(option).val();

							// We don't need an array for one selects
							if ( one )
								return value;

							// Multi-Selects return an array
							values.push( value );
						}
					}

					return values;				
				}

				// Everything else, we just grab the value
				return (elem.value || "").replace(/\r/g, "");

			}

			return undefined;
		}

		if ( typeof value === "number" )
			value += '';

		return this.each(function(){
			if ( this.nodeType != 1 )
				return;

			if ( jQuery.isArray(value) && /radio|checkbox/.test( this.type ) )
				this.checked = (jQuery.inArray(this.value, value) >= 0 ||
					jQuery.inArray(this.name, value) >= 0);

			else if ( jQuery.nodeName( this, "select" ) ) {
				var values = jQuery.makeArray(value);

				jQuery( "option", this ).each(function(){
					this.selected = (jQuery.inArray( this.value, values ) >= 0 ||
						jQuery.inArray( this.text, values ) >= 0);
				});

				if ( !values.length )
					this.selectedIndex = -1;

			} else
				this.value = value;
		});
	},

	html: function( value ) {
		return value === undefined ?
			(this[0] ?
				this[0].innerHTML.replace(/ jQuery\d+="(?:\d+|null)"/g, "") :
				null) :
			this.empty().append( value );
	},

	replaceWith: function( value ) {
		return this.after( value ).remove();
	},

	eq: function( i ) {
		return this.slice( i, +i + 1 );
	},

	slice: function() {
		return this.pushStack( Array.prototype.slice.apply( this, arguments ),
			"slice", Array.prototype.slice.call(arguments).join(",") );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map(this, function(elem, i){
			return callback.call( elem, i, elem );
		}));
	},

	andSelf: function() {
		return this.add( this.prevObject );
	},

	domManip: function( args, table, callback ) {
		if ( this[0] ) {
			var fragment = (this[0].ownerDocument || this[0]).createDocumentFragment(),
				scripts = jQuery.clean( args, (this[0].ownerDocument || this[0]), fragment ),
				first = fragment.firstChild;

			if ( first )
				for ( var i = 0, l = this.length; i < l; i++ )
					callback.call( root(this[i], first), this.length > 1 || i > 0 ?
							fragment.cloneNode(true) : fragment );
		
			if ( scripts )
				jQuery.each( scripts, evalScript );
		}

		return this;
		
		function root( elem, cur ) {
			return table && jQuery.nodeName(elem, "table") && jQuery.nodeName(cur, "tr") ?
				(elem.getElementsByTagName("tbody")[0] ||
				elem.appendChild(elem.ownerDocument.createElement("tbody"))) :
				elem;
		}
	}
};

// Give the init function the jQuery prototype for later instantiation
jQuery.fn.init.prototype = jQuery.fn;

function evalScript( i, elem ) {
	if ( elem.src )
		jQuery.ajax({
			url: elem.src,
			async: false,
			dataType: "script"
		});

	else
		jQuery.globalEval( elem.text || elem.textContent || elem.innerHTML || "" );

	if ( elem.parentNode )
		elem.parentNode.removeChild( elem );
}

function now(){
	return +new Date;
}

jQuery.extend = jQuery.fn.extend = function() {
	// copy reference to target object
	var target = arguments[0] || {}, i = 1, length = arguments.length, deep = false, options;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;
		target = arguments[1] || {};
		// skip the boolean and the target
		i = 2;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !jQuery.isFunction(target) )
		target = {};

	// extend jQuery itself if only one argument is passed
	if ( length == i ) {
		target = this;
		--i;
	}

	for ( ; i < length; i++ )
		// Only deal with non-null/undefined values
		if ( (options = arguments[ i ]) != null )
			// Extend the base object
			for ( var name in options ) {
				var src = target[ name ], copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy )
					continue;

				// Recurse if we're merging object values
				if ( deep && copy && typeof copy === "object" && !copy.nodeType )
					target[ name ] = jQuery.extend( deep, 
						// Never move original objects, clone them
						src || ( copy.length != null ? [ ] : { } )
					, copy );

				// Don't bring in undefined values
				else if ( copy !== undefined )
					target[ name ] = copy;

			}

	// Return the modified object
	return target;
};

// exclude the following css properties to add px
var	exclude = /z-?index|font-?weight|opacity|zoom|line-?height/i,
	// cache defaultView
	defaultView = document.defaultView || {},
	toString = Object.prototype.toString;

jQuery.extend({
	noConflict: function( deep ) {
		window.$ = _$;

		if ( deep )
			window.jQuery = _jQuery;

		return jQuery;
	},

	// See test/unit/core.js for details concerning isFunction.
	// Since version 1.3, DOM methods and functions like alert
	// aren't supported. They return false on IE (#2968).
	isFunction: function( obj ) {
		return toString.call(obj) === "[object Function]";
	},

	isArray: function( obj ) {
		return toString.call(obj) === "[object Array]";
	},

	// check if an element is in a (or is an) XML document
	isXMLDoc: function( elem ) {
		return elem.nodeType === 9 && elem.documentElement.nodeName !== "HTML" ||
			!!elem.ownerDocument && jQuery.isXMLDoc( elem.ownerDocument );
	},

	// Evalulates a script in a global context
	globalEval: function( data ) {
		if ( data && /\S/.test(data) ) {
			// Inspired by code by Andrea Giammarchi
			// http://webreflection.blogspot.com/2007/08/global-scope-evaluation-and-dom.html
			var head = document.getElementsByTagName("head")[0] || document.documentElement,
				script = document.createElement("script");

			script.type = "text/javascript";
			if ( jQuery.support.scriptEval )
				script.appendChild( document.createTextNode( data ) );
			else
				script.text = data;

			// Use insertBefore instead of appendChild  to circumvent an IE6 bug.
			// This arises when a base node is used (#2709).
			head.insertBefore( script, head.firstChild );
			head.removeChild( script );
		}
	},

	nodeName: function( elem, name ) {
		return elem.nodeName && elem.nodeName.toUpperCase() == name.toUpperCase();
	},

	// args is for internal usage only
	each: function( object, callback, args ) {
		var name, i = 0, length = object.length;

		if ( args ) {
			if ( length === undefined ) {
				for ( name in object )
					if ( callback.apply( object[ name ], args ) === false )
						break;
			} else
				for ( ; i < length; )
					if ( callback.apply( object[ i++ ], args ) === false )
						break;

		// A special, fast, case for the most common use of each
		} else {
			if ( length === undefined ) {
				for ( name in object )
					if ( callback.call( object[ name ], name, object[ name ] ) === false )
						break;
			} else
				for ( var value = object[0];
					i < length && callback.call( value, i, value ) !== false; value = object[++i] ){}
		}

		return object;
	},

	prop: function( elem, value, type, i, name ) {
		// Handle executable functions
		if ( jQuery.isFunction( value ) )
			value = value.call( elem, i );

		// Handle passing in a number to a CSS property
		return typeof value === "number" && type == "curCSS" && !exclude.test( name ) ?
			value + "px" :
			value;
	},

	className: {
		// internal only, use addClass("class")
		add: function( elem, classNames ) {
			jQuery.each((classNames || "").split(/\s+/), function(i, className){
				if ( elem.nodeType == 1 && !jQuery.className.has( elem.className, className ) )
					elem.className += (elem.className ? " " : "") + className;
			});
		},

		// internal only, use removeClass("class")
		remove: function( elem, classNames ) {
			if (elem.nodeType == 1)
				elem.className = classNames !== undefined ?
					jQuery.grep(elem.className.split(/\s+/), function(className){
						return !jQuery.className.has( classNames, className );
					}).join(" ") :
					"";
		},

		// internal only, use hasClass("class")
		has: function( elem, className ) {
			return elem && jQuery.inArray( className, (elem.className || elem).toString().split(/\s+/) ) > -1;
		}
	},

	// A method for quickly swapping in/out CSS properties to get correct calculations
	swap: function( elem, options, callback ) {
		var old = {};
		// Remember the old values, and insert the new ones
		for ( var name in options ) {
			old[ name ] = elem.style[ name ];
			elem.style[ name ] = options[ name ];
		}

		callback.call( elem );

		// Revert the old values
		for ( var name in options )
			elem.style[ name ] = old[ name ];
	},

	css: function( elem, name, force, extra ) {
		if ( name == "width" || name == "height" ) {
			var val, props = { position: "absolute", visibility: "hidden", display:"block" }, which = name == "width" ? [ "Left", "Right" ] : [ "Top", "Bottom" ];

			function getWH() {
				val = name == "width" ? elem.offsetWidth : elem.offsetHeight;

				if ( extra === "border" )
					return;

				jQuery.each( which, function() {
					if ( !extra )
						val -= parseFloat(jQuery.curCSS( elem, "padding" + this, true)) || 0;
					if ( extra === "margin" )
						val += parseFloat(jQuery.curCSS( elem, "margin" + this, true)) || 0;
					else
						val -= parseFloat(jQuery.curCSS( elem, "border" + this + "Width", true)) || 0;
				});
			}

			if ( elem.offsetWidth !== 0 )
				getWH();
			else
				jQuery.swap( elem, props, getWH );

			return Math.max(0, Math.round(val));
		}

		return jQuery.curCSS( elem, name, force );
	},

	curCSS: function( elem, name, force ) {
		var ret, style = elem.style;

		// We need to handle opacity special in IE
		if ( name == "opacity" && !jQuery.support.opacity ) {
			ret = jQuery.attr( style, "opacity" );

			return ret == "" ?
				"1" :
				ret;
		}

		// Make sure we're using the right name for getting the float value
		if ( name.match( /float/i ) )
			name = styleFloat;

		if ( !force && style && style[ name ] )
			ret = style[ name ];

		else if ( defaultView.getComputedStyle ) {

			// Only "float" is needed here
			if ( name.match( /float/i ) )
				name = "float";

			name = name.replace( /([A-Z])/g, "-$1" ).toLowerCase();

			var computedStyle = defaultView.getComputedStyle( elem, null );

			if ( computedStyle )
				ret = computedStyle.getPropertyValue( name );

			// We should always get a number back from opacity
			if ( name == "opacity" && ret == "" )
				ret = "1";

		} else if ( elem.currentStyle ) {
			var camelCase = name.replace(/\-(\w)/g, function(all, letter){
				return letter.toUpperCase();
			});

			ret = elem.currentStyle[ name ] || elem.currentStyle[ camelCase ];

			// From the awesome hack by Dean Edwards
			// http://erik.eae.net/archives/2007/07/27/18.54.15/#comment-102291

			// If we're not dealing with a regular pixel number
			// but a number that has a weird ending, we need to convert it to pixels
			if ( !/^\d+(px)?$/i.test( ret ) && /^\d/.test( ret ) ) {
				// Remember the original values
				var left = style.left, rsLeft = elem.runtimeStyle.left;

				// Put in the new values to get a computed value out
				elem.runtimeStyle.left = elem.currentStyle.left;
				style.left = ret || 0;
				ret = style.pixelLeft + "px";

				// Revert the changed values
				style.left = left;
				elem.runtimeStyle.left = rsLeft;
			}
		}

		return ret;
	},

	clean: function( elems, context, fragment ) {
		context = context || document;

		// !context.createElement fails in IE with an error but returns typeof 'object'
		if ( typeof context.createElement === "undefined" )
			context = context.ownerDocument || context[0] && context[0].ownerDocument || document;

		// If a single string is passed in and it's a single tag
		// just do a createElement and skip the rest
		if ( !fragment && elems.length === 1 && typeof elems[0] === "string" ) {
			var match = /^<(\w+)\s*\/?>$/.exec(elems[0]);
			if ( match )
				return [ context.createElement( match[1] ) ];
		}

		var ret = [], scripts = [], div = context.createElement("div");

		jQuery.each(elems, function(i, elem){
			if ( typeof elem === "number" )
				elem += '';

			if ( !elem )
				return;

			// Convert html string into DOM nodes
			if ( typeof elem === "string" ) {
				// Fix "XHTML"-style tags in all browsers
				elem = elem.replace(/(<(\w+)[^>]*?)\/>/g, function(all, front, tag){
					return tag.match(/^(abbr|br|col|img|input|link|meta|param|hr|area|embed)$/i) ?
						all :
						front + "></" + tag + ">";
				});

				// Trim whitespace, otherwise indexOf won't work as expected
				var tags = elem.replace(/^\s+/, "").substring(0, 10).toLowerCase();

				var wrap =
					// option or optgroup
					!tags.indexOf("<opt") &&
					[ 1, "<select multiple='multiple'>", "</select>" ] ||

					!tags.indexOf("<leg") &&
					[ 1, "<fieldset>", "</fieldset>" ] ||

					tags.match(/^<(thead|tbody|tfoot|colg|cap)/) &&
					[ 1, "<table>", "</table>" ] ||

					!tags.indexOf("<tr") &&
					[ 2, "<table><tbody>", "</tbody></table>" ] ||

				 	// <thead> matched above
					(!tags.indexOf("<td") || !tags.indexOf("<th")) &&
					[ 3, "<table><tbody><tr>", "</tr></tbody></table>" ] ||

					!tags.indexOf("<col") &&
					[ 2, "<table><tbody></tbody><colgroup>", "</colgroup></table>" ] ||

					// IE can't serialize <link> and <script> tags normally
					!jQuery.support.htmlSerialize &&
					[ 1, "div<div>", "</div>" ] ||

					[ 0, "", "" ];

				// Go to html and back, then peel off extra wrappers
				div.innerHTML = wrap[1] + elem + wrap[2];

				// Move to the right depth
				while ( wrap[0]-- )
					div = div.lastChild;

				// Remove IE's autoinserted <tbody> from table fragments
				if ( !jQuery.support.tbody ) {

					// String was a <table>, *may* have spurious <tbody>
					var hasBody = /<tbody/i.test(elem),
						tbody = !tags.indexOf("<table") && !hasBody ?
							div.firstChild && div.firstChild.childNodes :

						// String was a bare <thead> or <tfoot>
						wrap[1] == "<table>" && !hasBody ?
							div.childNodes :
							[];

					for ( var j = tbody.length - 1; j >= 0 ; --j )
						if ( jQuery.nodeName( tbody[ j ], "tbody" ) && !tbody[ j ].childNodes.length )
							tbody[ j ].parentNode.removeChild( tbody[ j ] );

					}

				// IE completely kills leading whitespace when innerHTML is used
				if ( !jQuery.support.leadingWhitespace && /^\s/.test( elem ) )
					div.insertBefore( context.createTextNode( elem.match(/^\s*/)[0] ), div.firstChild );
				
				elem = jQuery.makeArray( div.childNodes );
			}

			if ( elem.nodeType )
				ret.push( elem );
			else
				ret = jQuery.merge( ret, elem );

		});

		if ( fragment ) {
			for ( var i = 0; ret[i]; i++ ) {
				if ( jQuery.nodeName( ret[i], "script" ) && (!ret[i].type || ret[i].type.toLowerCase() === "text/javascript") ) {
					scripts.push( ret[i].parentNode ? ret[i].parentNode.removeChild( ret[i] ) : ret[i] );
				} else {
					if ( ret[i].nodeType === 1 )
						ret.splice.apply( ret, [i + 1, 0].concat(jQuery.makeArray(ret[i].getElementsByTagName("script"))) );
					fragment.appendChild( ret[i] );
				}
			}
			
			return scripts;
		}

		return ret;
	},

	attr: function( elem, name, value ) {
		// don't set attributes on text and comment nodes
		if (!elem || elem.nodeType == 3 || elem.nodeType == 8)
			return undefined;

		var notxml = !jQuery.isXMLDoc( elem ),
			// Whether we are setting (or getting)
			//set = value !== undefined;

            //rauch.2009.7.1 change for ie bug:
            set = value != 'NaNpx' && value !== undefined;

		// Try to normalize/fix the name
		name = notxml && jQuery.props[ name ] || name;

		// Only do all the following if this is a node (faster for style)
		// IE elem.getAttribute passes even for style
		if ( elem.tagName ) {

			// These attributes require special treatment
			var special = /href|src|style/.test( name );

			// Safari mis-reports the default selected property of a hidden option
			// Accessing the parent's selectedIndex property fixes it
			if ( name == "selected" && elem.parentNode )
				elem.parentNode.selectedIndex;

			// If applicable, access the attribute via the DOM 0 way
			if ( name in elem && notxml && !special ) {
				if ( set ){
					// We can't allow the type property to be changed (since it causes problems in IE)
					if ( name == "type" && jQuery.nodeName( elem, "input" ) && elem.parentNode )
						throw "type property can't be changed";

					elem[ name ] = value;
				}

				// browsers index elements by id/name on forms, give priority to attributes.
				if( jQuery.nodeName( elem, "form" ) && elem.getAttributeNode(name) )
					return elem.getAttributeNode( name ).nodeValue;

				// elem.tabIndex doesn't always return the correct value when it hasn't been explicitly set
				// http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
				if ( name == "tabIndex" ) {
					var attributeNode = elem.getAttributeNode( "tabIndex" );
					return attributeNode && attributeNode.specified
						? attributeNode.value
						: elem.nodeName.match(/(button|input|object|select|textarea)/i)
							? 0
							: elem.nodeName.match(/^(a|area)$/i) && elem.href
								? 0
								: undefined;
				}

				return elem[ name ];
			}

			if ( !jQuery.support.style && notxml &&  name == "style" )
				return jQuery.attr( elem.style, "cssText", value );

			if ( set )
				// convert the value to a string (all browsers do this but IE) see #1070
				elem.setAttribute( name, "" + value );

			var attr = !jQuery.support.hrefNormalized && notxml && special
					// Some attributes require a special call on IE
					? elem.getAttribute( name, 2 )
					: elem.getAttribute( name );

			// Non-existent attributes return null, we normalize to undefined
			return attr === null ? undefined : attr;
		}

		// elem is actually elem.style ... set the style

		// IE uses filters for opacity
		if ( !jQuery.support.opacity && name == "opacity" ) {
			if ( set ) {
				// IE has trouble with opacity if it does not have layout
				// Force it by setting the zoom level
				elem.zoom = 1;

				// Set the alpha filter to set the opacity
				elem.filter = (elem.filter || "").replace( /alpha\([^)]*\)/, "" ) +
					(parseInt( value ) + '' == "NaN" ? "" : "alpha(opacity=" + value * 100 + ")");
			}

			return elem.filter && elem.filter.indexOf("opacity=") >= 0 ?
				(parseFloat( elem.filter.match(/opacity=([^)]*)/)[1] ) / 100) + '':
				"";
		}

		name = name.replace(/-([a-z])/ig, function(all, letter){
			return letter.toUpperCase();
		});

		if ( set )
			elem[ name ] = value;

		return elem[ name ];
	},

	trim: function( text ) {
		return (text || "").replace( /^\s+|\s+$/g, "" );
	},

	makeArray: function( array ) {
		var ret = [];

		if( array != null ){
			var i = array.length;
			// The window, strings (and functions) also have 'length'
			if( i == null || typeof array === "string" || jQuery.isFunction(array) || array.setInterval )
				ret[0] = array;
			else
				while( i )
					ret[--i] = array[i];
		}

		return ret;
	},

	inArray: function( elem, array ) {
		for ( var i = 0, length = array.length; i < length; i++ )
		// Use === because on IE, window == document
			if ( array[ i ] === elem )
				return i;

		return -1;
	},

	merge: function( first, second ) {
		// We have to loop this way because IE & Opera overwrite the length
		// expando of getElementsByTagName
		var i = 0, elem, pos = first.length;
		// Also, we need to make sure that the correct elements are being returned
		// (IE returns comment nodes in a '*' query)
		if ( !jQuery.support.getAll ) {
			while ( (elem = second[ i++ ]) != null )
				if ( elem.nodeType != 8 )
					first[ pos++ ] = elem;

		} else
			while ( (elem = second[ i++ ]) != null )
				first[ pos++ ] = elem;

		return first;
	},

	unique: function( array ) {
		var ret = [], done = {};

		try {

			for ( var i = 0, length = array.length; i < length; i++ ) {
				var id = jQuery.data( array[ i ] );

				if ( !done[ id ] ) {
					done[ id ] = true;
					ret.push( array[ i ] );
				}
			}

		} catch( e ) {
			ret = array;
		}

		return ret;
	},

	grep: function( elems, callback, inv ) {
		var ret = [];

		// Go through the array, only saving the items
		// that pass the validator function
		for ( var i = 0, length = elems.length; i < length; i++ )
			if ( !inv != !callback( elems[ i ], i ) )
				ret.push( elems[ i ] );

		return ret;
	},

	map: function( elems, callback ) {
		var ret = [];

		// Go through the array, translating each of the items to their
		// new value (or values).
		for ( var i = 0, length = elems.length; i < length; i++ ) {
			var value = callback( elems[ i ], i );

			if ( value != null )
				ret[ ret.length ] = value;
		}

		return ret.concat.apply( [], ret );
	}
});

// Use of jQuery.browser is deprecated.
// It's included for backwards compatibility and plugins,
// although they should work to migrate away.

var userAgent = navigator.userAgent.toLowerCase();

// Figure out what browser is being used
jQuery.browser = {
	version: (userAgent.match( /.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/ ) || [0,'0'])[1],
	safari: /webkit/.test( userAgent ),
	opera: /opera/.test( userAgent ),
	msie: /msie/.test( userAgent ) && !/opera/.test( userAgent ),
	mozilla: /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent )
};

jQuery.each({
	parent: function(elem){return elem.parentNode;},
	parents: function(elem){return jQuery.dir(elem,"parentNode");},
	next: function(elem){return jQuery.nth(elem,2,"nextSibling");},
	prev: function(elem){return jQuery.nth(elem,2,"previousSibling");},
	nextAll: function(elem){return jQuery.dir(elem,"nextSibling");},
	prevAll: function(elem){return jQuery.dir(elem,"previousSibling");},
	siblings: function(elem){return jQuery.sibling(elem.parentNode.firstChild,elem);},
	children: function(elem){return jQuery.sibling(elem.firstChild);},
	contents: function(elem){return jQuery.nodeName(elem,"iframe")?elem.contentDocument||elem.contentWindow.document:jQuery.makeArray(elem.childNodes);}
}, function(name, fn){
	jQuery.fn[ name ] = function( selector ) {
		var ret = jQuery.map( this, fn );

		if ( selector && typeof selector == "string" )
			ret = jQuery.multiFilter( selector, ret );

		return this.pushStack( jQuery.unique( ret ), name, selector );
	};
});

jQuery.each({
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function(name, original){
	jQuery.fn[ name ] = function( selector ) {
		var ret = [], insert = jQuery( selector );

		for ( var i = 0, l = insert.length; i < l; i++ ) {
			var elems = (i > 0 ? this.clone(true) : this).get();
			jQuery.fn[ original ].apply( jQuery(insert[i]), elems );
			ret = ret.concat( elems );
		}

		return this.pushStack( ret, name, selector );
	};
});

jQuery.each({
	removeAttr: function( name ) {
		jQuery.attr( this, name, "" );
		if (this.nodeType == 1)
			this.removeAttribute( name );
	},

	addClass: function( classNames ) {
		jQuery.className.add( this, classNames );
	},

	removeClass: function( classNames ) {
		jQuery.className.remove( this, classNames );
	},

	toggleClass: function( classNames, state ) {
		if( typeof state !== "boolean" )
			state = !jQuery.className.has( this, classNames );
		jQuery.className[ state ? "add" : "remove" ]( this, classNames );
	},

	remove: function( selector ) {
		if ( !selector || jQuery.filter( selector, [ this ] ).length ) {
			// Prevent memory leaks
			jQuery( "*", this ).add([this]).each(function(){
				jQuery.event.remove(this);
				jQuery.removeData(this);
			});
			if (this.parentNode)
				this.parentNode.removeChild( this );
		}
	},

	empty: function() {
		// Remove element nodes and prevent memory leaks
		jQuery(this).children().remove();

		// Remove any remaining nodes
		while ( this.firstChild )
			this.removeChild( this.firstChild );
	}
}, function(name, fn){
	jQuery.fn[ name ] = function(){
		return this.each( fn, arguments );
	};
});

// Helper function used by the dimensions and offset modules
function num(elem, prop) {
	return elem[0] && parseInt( jQuery.curCSS(elem[0], prop, true), 10 ) || 0;
}
var expando = "jQuery" + now(), uuid = 0, windowData = {};

jQuery.extend({
	cache: {},

	data: function( elem, name, data ) {
		elem = elem == window ?
			windowData :
			elem;

		var id = elem[ expando ];

		// Compute a unique ID for the element
		if ( !id )
			id = elem[ expando ] = ++uuid;

		// Only generate the data cache if we're
		// trying to access or manipulate it
		if ( name && !jQuery.cache[ id ] )
			jQuery.cache[ id ] = {};

		// Prevent overriding the named cache with undefined values
		if ( data !== undefined )
			jQuery.cache[ id ][ name ] = data;

		// Return the named cache data, or the ID for the element
		return name ?
			jQuery.cache[ id ][ name ] :
			id;
	},

	removeData: function( elem, name ) {
		elem = elem == window ?
			windowData :
			elem;

		var id = elem[ expando ];

		// If we want to remove a specific section of the element's data
		if ( name ) {
			if ( jQuery.cache[ id ] ) {
				// Remove the section of cache data
				delete jQuery.cache[ id ][ name ];

				// If we've removed all the data, remove the element's cache
				name = "";

				for ( name in jQuery.cache[ id ] )
					break;

				if ( !name )
					jQuery.removeData( elem );
			}

		// Otherwise, we want to remove all of the element's data
		} else {
			// Clean up the element expando
			try {
				delete elem[ expando ];
			} catch(e){
				// IE has trouble directly removing the expando
				// but it's ok with using removeAttribute
				if ( elem.removeAttribute )
					elem.removeAttribute( expando );
			}

			// Completely remove the data cache
			delete jQuery.cache[ id ];
		}
	},
	queue: function( elem, type, data ) {
		if ( elem ){
	
			type = (type || "fx") + "queue";
	
			var q = jQuery.data( elem, type );
	
			if ( !q || jQuery.isArray(data) )
				q = jQuery.data( elem, type, jQuery.makeArray(data) );
			else if( data )
				q.push( data );
	
		}
		return q;
	},

	dequeue: function( elem, type ){
		var queue = jQuery.queue( elem, type ),
			fn = queue.shift();
		
		if( !type || type === "fx" )
			fn = queue[0];
			
		if( fn !== undefined )
			fn.call(elem);
	}
});

jQuery.fn.extend({
	data: function( key, value ){
		var parts = key.split(".");
		parts[1] = parts[1] ? "." + parts[1] : "";

		if ( value === undefined ) {
			var data = this.triggerHandler("getData" + parts[1] + "!", [parts[0]]);

			if ( data === undefined && this.length )
				data = jQuery.data( this[0], key );

			return data === undefined && parts[1] ?
				this.data( parts[0] ) :
				data;
		} else
			return this.trigger("setData" + parts[1] + "!", [parts[0], value]).each(function(){
				jQuery.data( this, key, value );
			});
	},

	removeData: function( key ){
		return this.each(function(){
			jQuery.removeData( this, key );
		});
	},
	queue: function(type, data){
		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
		}

		if ( data === undefined )
			return jQuery.queue( this[0], type );

		return this.each(function(){
			var queue = jQuery.queue( this, type, data );
			
			 if( type == "fx" && queue.length == 1 )
				queue[0].call(this);
		});
	},
	dequeue: function(type){
		return this.each(function(){
			jQuery.dequeue( this, type );
		});
	}
});/*!
 * Sizzle CSS Selector Engine - v0.9.3
 *  Copyright 2009, The Dojo Foundation
 *  Released under the MIT, BSD, and GPL Licenses.
 *  More information: http://sizzlejs.com/
 */
(function(){

var chunker = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^[\]]*\]|['"][^'"]*['"]|[^[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?/g,
	done = 0,
	toString = Object.prototype.toString;

var Sizzle = function(selector, context, results, seed) {
	results = results || [];
	context = context || document;

	if ( context.nodeType !== 1 && context.nodeType !== 9 )
		return [];
	
	if ( !selector || typeof selector !== "string" ) {
		return results;
	}

	var parts = [], m, set, checkSet, check, mode, extra, prune = true;
	
	// Reset the position of the chunker regexp (start from head)
	chunker.lastIndex = 0;
	
	while ( (m = chunker.exec(selector)) !== null ) {
		parts.push( m[1] );
		
		if ( m[2] ) {
			extra = RegExp.rightContext;
			break;
		}
	}

	if ( parts.length > 1 && origPOS.exec( selector ) ) {
		if ( parts.length === 2 && Expr.relative[ parts[0] ] ) {
			set = posProcess( parts[0] + parts[1], context );
		} else {
			set = Expr.relative[ parts[0] ] ?
				[ context ] :
				Sizzle( parts.shift(), context );

			while ( parts.length ) {
				selector = parts.shift();

				if ( Expr.relative[ selector ] )
					selector += parts.shift();

				set = posProcess( selector, set );
			}
		}
	} else {
		var ret = seed ?
			{ expr: parts.pop(), set: makeArray(seed) } :
			Sizzle.find( parts.pop(), parts.length === 1 && context.parentNode ? context.parentNode : context, isXML(context) );
		set = Sizzle.filter( ret.expr, ret.set );

		if ( parts.length > 0 ) {
			checkSet = makeArray(set);
		} else {
			prune = false;
		}

		while ( parts.length ) {
			var cur = parts.pop(), pop = cur;

			if ( !Expr.relative[ cur ] ) {
				cur = "";
			} else {
				pop = parts.pop();
			}

			if ( pop == null ) {
				pop = context;
			}

			Expr.relative[ cur ]( checkSet, pop, isXML(context) );
		}
	}

	if ( !checkSet ) {
		checkSet = set;
	}

	if ( !checkSet ) {
		throw "Syntax error, unrecognized expression: " + (cur || selector);
	}

	if ( toString.call(checkSet) === "[object Array]" ) {
		if ( !prune ) {
			results.push.apply( results, checkSet );
		} else if ( context.nodeType === 1 ) {
			for ( var i = 0; checkSet[i] != null; i++ ) {
				if ( checkSet[i] && (checkSet[i] === true || checkSet[i].nodeType === 1 && contains(context, checkSet[i])) ) {
					results.push( set[i] );
				}
			}
		} else {
			for ( var i = 0; checkSet[i] != null; i++ ) {
				if ( checkSet[i] && checkSet[i].nodeType === 1 ) {
					results.push( set[i] );
				}
			}
		}
	} else {
		makeArray( checkSet, results );
	}

	if ( extra ) {
		Sizzle( extra, context, results, seed );

		if ( sortOrder ) {
			hasDuplicate = false;
			results.sort(sortOrder);

			if ( hasDuplicate ) {
				for ( var i = 1; i < results.length; i++ ) {
					if ( results[i] === results[i-1] ) {
						results.splice(i--, 1);
					}
				}
			}
		}
	}

	return results;
};

Sizzle.matches = function(expr, set){
	return Sizzle(expr, null, null, set);
};

Sizzle.find = function(expr, context, isXML){
	var set, match;

	if ( !expr ) {
		return [];
	}

	for ( var i = 0, l = Expr.order.length; i < l; i++ ) {
		var type = Expr.order[i], match;
		
		if ( (match = Expr.match[ type ].exec( expr )) ) {
			var left = RegExp.leftContext;

			if ( left.substr( left.length - 1 ) !== "\\" ) {
				match[1] = (match[1] || "").replace(/\\/g, "");
				set = Expr.find[ type ]( match, context, isXML );
				if ( set != null ) {
					expr = expr.replace( Expr.match[ type ], "" );
					break;
				}
			}
		}
	}

	if ( !set ) {
		set = context.getElementsByTagName("*");
	}

	return {set: set, expr: expr};
};

Sizzle.filter = function(expr, set, inplace, not){
	var old = expr, result = [], curLoop = set, match, anyFound,
		isXMLFilter = set && set[0] && isXML(set[0]);

	while ( expr && set.length ) {
		for ( var type in Expr.filter ) {
			if ( (match = Expr.match[ type ].exec( expr )) != null ) {
				var filter = Expr.filter[ type ], found, item;
				anyFound = false;

				if ( curLoop == result ) {
					result = [];
				}

				if ( Expr.preFilter[ type ] ) {
					match = Expr.preFilter[ type ]( match, curLoop, inplace, result, not, isXMLFilter );

					if ( !match ) {
						anyFound = found = true;
					} else if ( match === true ) {
						continue;
					}
				}

				if ( match ) {
					for ( var i = 0; (item = curLoop[i]) != null; i++ ) {
						if ( item ) {
							found = filter( item, match, i, curLoop );
							var pass = not ^ !!found;

							if ( inplace && found != null ) {
								if ( pass ) {
									anyFound = true;
								} else {
									curLoop[i] = false;
								}
							} else if ( pass ) {
								result.push( item );
								anyFound = true;
							}
						}
					}
				}

				if ( found !== undefined ) {
					if ( !inplace ) {
						curLoop = result;
					}

					expr = expr.replace( Expr.match[ type ], "" );

					if ( !anyFound ) {
						return [];
					}

					break;
				}
			}
		}

		// Improper expression
		if ( expr == old ) {
			if ( anyFound == null ) {
				throw "Syntax error, unrecognized expression: " + expr;
			} else {
				break;
			}
		}

		old = expr;
	}

	return curLoop;
};

var Expr = Sizzle.selectors = {
	order: [ "ID", "NAME", "TAG" ],
	match: {
		ID: /#((?:[\w\u00c0-\uFFFF_-]|\\.)+)/,
		CLASS: /\.((?:[\w\u00c0-\uFFFF_-]|\\.)+)/,
		NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF_-]|\\.)+)['"]*\]/,
		ATTR: /\[\s*((?:[\w\u00c0-\uFFFF_-]|\\.)+)\s*(?:(\S?=)\s*(['"]*)(.*?)\3|)\s*\]/,
		TAG: /^((?:[\w\u00c0-\uFFFF\*_-]|\\.)+)/,
		CHILD: /:(only|nth|last|first)-child(?:\((even|odd|[\dn+-]*)\))?/,
		POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^-]|$)/,
		PSEUDO: /:((?:[\w\u00c0-\uFFFF_-]|\\.)+)(?:\((['"]*)((?:\([^\)]+\)|[^\2\(\)]*)+)\2\))?/
	},
	attrMap: {
		"class": "className",
		"for": "htmlFor"
	},
	attrHandle: {
		href: function(elem){
			return elem.getAttribute("href");
		}
	},
	relative: {
		"+": function(checkSet, part, isXML){
			var isPartStr = typeof part === "string",
				isTag = isPartStr && !/\W/.test(part),
				isPartStrNotTag = isPartStr && !isTag;

			if ( isTag && !isXML ) {
				part = part.toUpperCase();
			}

			for ( var i = 0, l = checkSet.length, elem; i < l; i++ ) {
				if ( (elem = checkSet[i]) ) {
					while ( (elem = elem.previousSibling) && elem.nodeType !== 1 ) {}

					checkSet[i] = isPartStrNotTag || elem && elem.nodeName === part ?
						elem || false :
						elem === part;
				}
			}

			if ( isPartStrNotTag ) {
				Sizzle.filter( part, checkSet, true );
			}
		},
		">": function(checkSet, part, isXML){
			var isPartStr = typeof part === "string";

			if ( isPartStr && !/\W/.test(part) ) {
				part = isXML ? part : part.toUpperCase();

				for ( var i = 0, l = checkSet.length; i < l; i++ ) {
					var elem = checkSet[i];
					if ( elem ) {
						var parent = elem.parentNode;
						checkSet[i] = parent.nodeName === part ? parent : false;
					}
				}
			} else {
				for ( var i = 0, l = checkSet.length; i < l; i++ ) {
					var elem = checkSet[i];
					if ( elem ) {
						checkSet[i] = isPartStr ?
							elem.parentNode :
							elem.parentNode === part;
					}
				}

				if ( isPartStr ) {
					Sizzle.filter( part, checkSet, true );
				}
			}
		},
		"": function(checkSet, part, isXML){
			var doneName = done++, checkFn = dirCheck;

			if ( !part.match(/\W/) ) {
				var nodeCheck = part = isXML ? part : part.toUpperCase();
				checkFn = dirNodeCheck;
			}

			checkFn("parentNode", part, doneName, checkSet, nodeCheck, isXML);
		},
		"~": function(checkSet, part, isXML){
			var doneName = done++, checkFn = dirCheck;

			if ( typeof part === "string" && !part.match(/\W/) ) {
				var nodeCheck = part = isXML ? part : part.toUpperCase();
				checkFn = dirNodeCheck;
			}

			checkFn("previousSibling", part, doneName, checkSet, nodeCheck, isXML);
		}
	},
	find: {
		ID: function(match, context, isXML){
			if ( typeof context.getElementById !== "undefined" && !isXML ) {
				var m = context.getElementById(match[1]);
				return m ? [m] : [];
			}
		},
		NAME: function(match, context, isXML){
			if ( typeof context.getElementsByName !== "undefined" ) {
				var ret = [], results = context.getElementsByName(match[1]);

				for ( var i = 0, l = results.length; i < l; i++ ) {
					if ( results[i].getAttribute("name") === match[1] ) {
						ret.push( results[i] );
					}
				}

				return ret.length === 0 ? null : ret;
			}
		},
		TAG: function(match, context){
			return context.getElementsByTagName(match[1]);
		}
	},
	preFilter: {
		CLASS: function(match, curLoop, inplace, result, not, isXML){
			match = " " + match[1].replace(/\\/g, "") + " ";

			if ( isXML ) {
				return match;
			}

			for ( var i = 0, elem; (elem = curLoop[i]) != null; i++ ) {
				if ( elem ) {
					if ( not ^ (elem.className && (" " + elem.className + " ").indexOf(match) >= 0) ) {
						if ( !inplace )
							result.push( elem );
					} else if ( inplace ) {
						curLoop[i] = false;
					}
				}
			}

			return false;
		},
		ID: function(match){
			return match[1].replace(/\\/g, "");
		},
		TAG: function(match, curLoop){
			for ( var i = 0; curLoop[i] === false; i++ ){}
			return curLoop[i] && isXML(curLoop[i]) ? match[1] : match[1].toUpperCase();
		},
		CHILD: function(match){
			if ( match[1] == "nth" ) {
				// parse equations like 'even', 'odd', '5', '2n', '3n+2', '4n-1', '-n+6'
				var test = /(-?)(\d*)n((?:\+|-)?\d*)/.exec(
					match[2] == "even" && "2n" || match[2] == "odd" && "2n+1" ||
					!/\D/.test( match[2] ) && "0n+" + match[2] || match[2]);

				// calculate the numbers (first)n+(last) including if they are negative
				match[2] = (test[1] + (test[2] || 1)) - 0;
				match[3] = test[3] - 0;
			}

			// TODO: Move to normal caching system
			match[0] = done++;

			return match;
		},
		ATTR: function(match, curLoop, inplace, result, not, isXML){
			var name = match[1].replace(/\\/g, "");
			
			if ( !isXML && Expr.attrMap[name] ) {
				match[1] = Expr.attrMap[name];
			}

			if ( match[2] === "~=" ) {
				match[4] = " " + match[4] + " ";
			}

			return match;
		},
		PSEUDO: function(match, curLoop, inplace, result, not){
			if ( match[1] === "not" ) {
				// If we're dealing with a complex expression, or a simple one
				if ( match[3].match(chunker).length > 1 || /^\w/.test(match[3]) ) {
					match[3] = Sizzle(match[3], null, null, curLoop);
				} else {
					var ret = Sizzle.filter(match[3], curLoop, inplace, true ^ not);
					if ( !inplace ) {
						result.push.apply( result, ret );
					}
					return false;
				}
			} else if ( Expr.match.POS.test( match[0] ) || Expr.match.CHILD.test( match[0] ) ) {
				return true;
			}
			
			return match;
		},
		POS: function(match){
			match.unshift( true );
			return match;
		}
	},
	filters: {
		enabled: function(elem){
			return elem.disabled === false && elem.type !== "hidden";
		},
		disabled: function(elem){
			return elem.disabled === true;
		},
		checked: function(elem){
			return elem.checked === true;
		},
		selected: function(elem){
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			elem.parentNode.selectedIndex;
			return elem.selected === true;
		},
		parent: function(elem){
			return !!elem.firstChild;
		},
		empty: function(elem){
			return !elem.firstChild;
		},
		has: function(elem, i, match){
			return !!Sizzle( match[3], elem ).length;
		},
		header: function(elem){
			return /h\d/i.test( elem.nodeName );
		},
		text: function(elem){
			return "text" === elem.type;
		},
		radio: function(elem){
			return "radio" === elem.type;
		},
		checkbox: function(elem){
			return "checkbox" === elem.type;
		},
		file: function(elem){
			return "file" === elem.type;
		},
		password: function(elem){
			return "password" === elem.type;
		},
		submit: function(elem){
			return "submit" === elem.type;
		},
		image: function(elem){
			return "image" === elem.type;
		},
		reset: function(elem){
			return "reset" === elem.type;
		},
		button: function(elem){
			return "button" === elem.type || elem.nodeName.toUpperCase() === "BUTTON";
		},
		input: function(elem){
			return /input|select|textarea|button/i.test(elem.nodeName);
		}
	},
	setFilters: {
		first: function(elem, i){
			return i === 0;
		},
		last: function(elem, i, match, array){
			return i === array.length - 1;
		},
		even: function(elem, i){
			return i % 2 === 0;
		},
		odd: function(elem, i){
			return i % 2 === 1;
		},
		lt: function(elem, i, match){
			return i < match[3] - 0;
		},
		gt: function(elem, i, match){
			return i > match[3] - 0;
		},
		nth: function(elem, i, match){
			return match[3] - 0 == i;
		},
		eq: function(elem, i, match){
			return match[3] - 0 == i;
		}
	},
	filter: {
		PSEUDO: function(elem, match, i, array){
			var name = match[1], filter = Expr.filters[ name ];

			if ( filter ) {
				return filter( elem, i, match, array );
			} else if ( name === "contains" ) {
				return (elem.textContent || elem.innerText || "").indexOf(match[3]) >= 0;
			} else if ( name === "not" ) {
				var not = match[3];

				for ( var i = 0, l = not.length; i < l; i++ ) {
					if ( not[i] === elem ) {
						return false;
					}
				}

				return true;
			}
		},
		CHILD: function(elem, match){
			var type = match[1], node = elem;
			switch (type) {
				case 'only':
				case 'first':
					while (node = node.previousSibling)  {
						if ( node.nodeType === 1 ) return false;
					}
					if ( type == 'first') return true;
					node = elem;
				case 'last':
					while (node = node.nextSibling)  {
						if ( node.nodeType === 1 ) return false;
					}
					return true;
				case 'nth':
					var first = match[2], last = match[3];

					if ( first == 1 && last == 0 ) {
						return true;
					}
					
					var doneName = match[0],
						parent = elem.parentNode;
	
					if ( parent && (parent.sizcache !== doneName || !elem.nodeIndex) ) {
						var count = 0;
						for ( node = parent.firstChild; node; node = node.nextSibling ) {
							if ( node.nodeType === 1 ) {
								node.nodeIndex = ++count;
							}
						} 
						parent.sizcache = doneName;
					}
					
					var diff = elem.nodeIndex - last;
					if ( first == 0 ) {
						return diff == 0;
					} else {
						return ( diff % first == 0 && diff / first >= 0 );
					}
			}
		},
		ID: function(elem, match){
			return elem.nodeType === 1 && elem.getAttribute("id") === match;
		},
		TAG: function(elem, match){
			return (match === "*" && elem.nodeType === 1) || elem.nodeName === match;
		},
		CLASS: function(elem, match){
			return (" " + (elem.className || elem.getAttribute("class")) + " ")
				.indexOf( match ) > -1;
		},
		ATTR: function(elem, match){
			var name = match[1],
				result = Expr.attrHandle[ name ] ?
					Expr.attrHandle[ name ]( elem ) :
					elem[ name ] != null ?
						elem[ name ] :
						elem.getAttribute( name ),
				value = result + "",
				type = match[2],
				check = match[4];

			return result == null ?
				type === "!=" :
				type === "=" ?
				value === check :
				type === "*=" ?
				value.indexOf(check) >= 0 :
				type === "~=" ?
				(" " + value + " ").indexOf(check) >= 0 :
				!check ?
				value && result !== false :
				type === "!=" ?
				value != check :
				type === "^=" ?
				value.indexOf(check) === 0 :
				type === "$=" ?
				value.substr(value.length - check.length) === check :
				type === "|=" ?
				value === check || value.substr(0, check.length + 1) === check + "-" :
				false;
		},
		POS: function(elem, match, i, array){
			var name = match[2], filter = Expr.setFilters[ name ];

			if ( filter ) {
				return filter( elem, i, match, array );
			}
		}
	}
};

var origPOS = Expr.match.POS;

for ( var type in Expr.match ) {
	Expr.match[ type ] = RegExp( Expr.match[ type ].source + /(?![^\[]*\])(?![^\(]*\))/.source );
}

var makeArray = function(array, results) {
	array = Array.prototype.slice.call( array );

	if ( results ) {
		results.push.apply( results, array );
		return results;
	}
	
	return array;
};

// Perform a simple check to determine if the browser is capable of
// converting a NodeList to an array using builtin methods.
try {
	Array.prototype.slice.call( document.documentElement.childNodes );

// Provide a fallback method if it does not work
} catch(e){
	makeArray = function(array, results) {
		var ret = results || [];

		if ( toString.call(array) === "[object Array]" ) {
			Array.prototype.push.apply( ret, array );
		} else {
			if ( typeof array.length === "number" ) {
				for ( var i = 0, l = array.length; i < l; i++ ) {
					ret.push( array[i] );
				}
			} else {
				for ( var i = 0; array[i]; i++ ) {
					ret.push( array[i] );
				}
			}
		}

		return ret;
	};
}

var sortOrder;

if ( document.documentElement.compareDocumentPosition ) {
	sortOrder = function( a, b ) {
		var ret = a.compareDocumentPosition(b) & 4 ? -1 : a === b ? 0 : 1;
		if ( ret === 0 ) {
			hasDuplicate = true;
		}
		return ret;
	};
} else if ( "sourceIndex" in document.documentElement ) {
	sortOrder = function( a, b ) {
		var ret = a.sourceIndex - b.sourceIndex;
		if ( ret === 0 ) {
			hasDuplicate = true;
		}
		return ret;
	};
} else if ( document.createRange ) {
	sortOrder = function( a, b ) {
		var aRange = a.ownerDocument.createRange(), bRange = b.ownerDocument.createRange();
		aRange.selectNode(a);
		aRange.collapse(true);
		bRange.selectNode(b);
		bRange.collapse(true);
		var ret = aRange.compareBoundaryPoints(Range.START_TO_END, bRange);
		if ( ret === 0 ) {
			hasDuplicate = true;
		}
		return ret;
	};
}

// Check to see if the browser returns elements by name when
// querying by getElementById (and provide a workaround)
(function(){
	// We're going to inject a fake input element with a specified name
	var form = document.createElement("form"),
		id = "script" + (new Date).getTime();
	form.innerHTML = "<input name='" + id + "'/>";

	// Inject it into the root element, check its status, and remove it quickly
	var root = document.documentElement;
	root.insertBefore( form, root.firstChild );

	// The workaround has to do additional checks after a getElementById
	// Which slows things down for other browsers (hence the branching)
	if ( !!document.getElementById( id ) ) {
		Expr.find.ID = function(match, context, isXML){
			if ( typeof context.getElementById !== "undefined" && !isXML ) {
				var m = context.getElementById(match[1]);
				return m ? m.id === match[1] || typeof m.getAttributeNode !== "undefined" && m.getAttributeNode("id").nodeValue === match[1] ? [m] : undefined : [];
			}
		};

		Expr.filter.ID = function(elem, match){
			var node = typeof elem.getAttributeNode !== "undefined" && elem.getAttributeNode("id");
			return elem.nodeType === 1 && node && node.nodeValue === match;
		};
	}

	root.removeChild( form );
})();

(function(){
	// Check to see if the browser returns only elements
	// when doing getElementsByTagName("*")

	// Create a fake element
	var div = document.createElement("div");
	div.appendChild( document.createComment("") );

	// Make sure no comments are found
	if ( div.getElementsByTagName("*").length > 0 ) {
		Expr.find.TAG = function(match, context){
			var results = context.getElementsByTagName(match[1]);

			// Filter out possible comments
			if ( match[1] === "*" ) {
				var tmp = [];

				for ( var i = 0; results[i]; i++ ) {
					if ( results[i].nodeType === 1 ) {
						tmp.push( results[i] );
					}
				}

				results = tmp;
			}

			return results;
		};
	}

	// Check to see if an attribute returns normalized href attributes
	div.innerHTML = "<a href='#'></a>";
	if ( div.firstChild && typeof div.firstChild.getAttribute !== "undefined" &&
			div.firstChild.getAttribute("href") !== "#" ) {
		Expr.attrHandle.href = function(elem){
			return elem.getAttribute("href", 2);
		};
	}
})();

if ( document.querySelectorAll ) (function(){
	var oldSizzle = Sizzle, div = document.createElement("div");
	div.innerHTML = "<p class='TEST'></p>";

	// Safari can't handle uppercase or unicode characters when
	// in quirks mode.
	if ( div.querySelectorAll && div.querySelectorAll(".TEST").length === 0 ) {
		return;
	}
	
	Sizzle = function(query, context, extra, seed){
		context = context || document;

		// Only use querySelectorAll on non-XML documents
		// (ID selectors don't work in non-HTML documents)
		if ( !seed && context.nodeType === 9 && !isXML(context) ) {
			try {
				return makeArray( context.querySelectorAll(query), extra );
			} catch(e){}
		}
		
		return oldSizzle(query, context, extra, seed);
	};

	Sizzle.find = oldSizzle.find;
	Sizzle.filter = oldSizzle.filter;
	Sizzle.selectors = oldSizzle.selectors;
	Sizzle.matches = oldSizzle.matches;
})();

if ( document.getElementsByClassName && document.documentElement.getElementsByClassName ) (function(){
	var div = document.createElement("div");
	div.innerHTML = "<div class='test e'></div><div class='test'></div>";

	// Opera can't find a second classname (in 9.6)
	if ( div.getElementsByClassName("e").length === 0 )
		return;

	// Safari caches class attributes, doesn't catch changes (in 3.2)
	div.lastChild.className = "e";

	if ( div.getElementsByClassName("e").length === 1 )
		return;

	Expr.order.splice(1, 0, "CLASS");
	Expr.find.CLASS = function(match, context, isXML) {
		if ( typeof context.getElementsByClassName !== "undefined" && !isXML ) {
			return context.getElementsByClassName(match[1]);
		}
	};
})();

function dirNodeCheck( dir, cur, doneName, checkSet, nodeCheck, isXML ) {
	var sibDir = dir == "previousSibling" && !isXML;
	for ( var i = 0, l = checkSet.length; i < l; i++ ) {
		var elem = checkSet[i];
		if ( elem ) {
			if ( sibDir && elem.nodeType === 1 ){
				elem.sizcache = doneName;
				elem.sizset = i;
			}
			elem = elem[dir];
			var match = false;

			while ( elem ) {
				if ( elem.sizcache === doneName ) {
					match = checkSet[elem.sizset];
					break;
				}

				if ( elem.nodeType === 1 && !isXML ){
					elem.sizcache = doneName;
					elem.sizset = i;
				}

				if ( elem.nodeName === cur ) {
					match = elem;
					break;
				}

				elem = elem[dir];
			}

			checkSet[i] = match;
		}
	}
}

function dirCheck( dir, cur, doneName, checkSet, nodeCheck, isXML ) {
	var sibDir = dir == "previousSibling" && !isXML;
	for ( var i = 0, l = checkSet.length; i < l; i++ ) {
		var elem = checkSet[i];
		if ( elem ) {
			if ( sibDir && elem.nodeType === 1 ) {
				elem.sizcache = doneName;
				elem.sizset = i;
			}
			elem = elem[dir];
			var match = false;

			while ( elem ) {
				if ( elem.sizcache === doneName ) {
					match = checkSet[elem.sizset];
					break;
				}

				if ( elem.nodeType === 1 ) {
					if ( !isXML ) {
						elem.sizcache = doneName;
						elem.sizset = i;
					}
					if ( typeof cur !== "string" ) {
						if ( elem === cur ) {
							match = true;
							break;
						}

					} else if ( Sizzle.filter( cur, [elem] ).length > 0 ) {
						match = elem;
						break;
					}
				}

				elem = elem[dir];
			}

			checkSet[i] = match;
		}
	}
}

var contains = document.compareDocumentPosition ?  function(a, b){
	return a.compareDocumentPosition(b) & 16;
} : function(a, b){
	return a !== b && (a.contains ? a.contains(b) : true);
};

var isXML = function(elem){
	return elem.nodeType === 9 && elem.documentElement.nodeName !== "HTML" ||
		!!elem.ownerDocument && isXML( elem.ownerDocument );
};

var posProcess = function(selector, context){
	var tmpSet = [], later = "", match,
		root = context.nodeType ? [context] : context;

	// Position selectors must be done after the filter
	// And so must :not(positional) so we move all PSEUDOs to the end
	while ( (match = Expr.match.PSEUDO.exec( selector )) ) {
		later += match[0];
		selector = selector.replace( Expr.match.PSEUDO, "" );
	}

	selector = Expr.relative[selector] ? selector + "*" : selector;

	for ( var i = 0, l = root.length; i < l; i++ ) {
		Sizzle( selector, root[i], tmpSet );
	}

	return Sizzle.filter( later, tmpSet );
};

// EXPOSE
jQuery.find = Sizzle;
jQuery.filter = Sizzle.filter;
jQuery.expr = Sizzle.selectors;
jQuery.expr[":"] = jQuery.expr.filters;

Sizzle.selectors.filters.hidden = function(elem){
	return elem.offsetWidth === 0 || elem.offsetHeight === 0;
};

Sizzle.selectors.filters.visible = function(elem){
	return elem.offsetWidth > 0 || elem.offsetHeight > 0;
};

Sizzle.selectors.filters.animated = function(elem){
	return jQuery.grep(jQuery.timers, function(fn){
		return elem === fn.elem;
	}).length;
};

jQuery.multiFilter = function( expr, elems, not ) {
	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	return Sizzle.matches(expr, elems);
};

jQuery.dir = function( elem, dir ){
	var matched = [], cur = elem[dir];
	while ( cur && cur != document ) {
		if ( cur.nodeType == 1 )
			matched.push( cur );
		cur = cur[dir];
	}
	return matched;
};

jQuery.nth = function(cur, result, dir, elem){
	result = result || 1;
	var num = 0;

	for ( ; cur; cur = cur[dir] )
		if ( cur.nodeType == 1 && ++num == result )
			break;

	return cur;
};

jQuery.sibling = function(n, elem){
	var r = [];

	for ( ; n; n = n.nextSibling ) {
		if ( n.nodeType == 1 && n != elem )
			r.push( n );
	}

	return r;
};

return;

window.Sizzle = Sizzle;

})();
/*
 * A number of helper functions used for managing events.
 * Many of the ideas behind this code originated from
 * Dean Edwards' addEvent library.
 */
jQuery.event = {

	// Bind an event to an element
	// Original by Dean Edwards
	add: function(elem, types, handler, data) {
		if ( elem.nodeType == 3 || elem.nodeType == 8 )
			return;

		// For whatever reason, IE has trouble passing the window object
		// around, causing it to be cloned in the process
		if ( elem.setInterval && elem != window )
			elem = window;

		// Make sure that the function being executed has a unique ID
		if ( !handler.guid )
			handler.guid = this.guid++;

		// if data is passed, bind to handler
		if ( data !== undefined ) {
			// Create temporary function pointer to original handler
			var fn = handler;

			// Create unique handler function, wrapped around original handler
			handler = this.proxy( fn );

			// Store data in unique handler
			handler.data = data;
		}

		// Init the element's event structure
		var events = jQuery.data(elem, "events") || jQuery.data(elem, "events", {}),
			handle = jQuery.data(elem, "handle") || jQuery.data(elem, "handle", function(){
				// Handle the second event of a trigger and when
				// an event is called after a page has unloaded
				return typeof jQuery !== "undefined" && !jQuery.event.triggered ?
					jQuery.event.handle.apply(arguments.callee.elem, arguments) :
					undefined;
			});
		// Add elem as a property of the handle function
		// This is to prevent a memory leak with non-native
		// event in IE.
		handle.elem = elem;

		// Handle multiple events separated by a space
		// jQuery(...).bind("mouseover mouseout", fn);
		jQuery.each(types.split(/\s+/), function(index, type) {
			// Namespaced event handlers
			var namespaces = type.split(".");
			type = namespaces.shift();
			handler.type = namespaces.slice().sort().join(".");

			// Get the current list of functions bound to this event
			var handlers = events[type];
			
			if ( jQuery.event.specialAll[type] )
				jQuery.event.specialAll[type].setup.call(elem, data, namespaces);

			// Init the event handler queue
			if (!handlers) {
				handlers = events[type] = {};

				// Check for a special event handler
				// Only use addEventListener/attachEvent if the special
				// events handler returns false
				if ( !jQuery.event.special[type] || jQuery.event.special[type].setup.call(elem, data, namespaces) === false ) {
					// Bind the global event handler to the element
					if (elem.addEventListener)
						elem.addEventListener(type, handle, false);
					else if (elem.attachEvent)
						elem.attachEvent("on" + type, handle);
				}
			}

			// Add the function to the element's handler list
			handlers[handler.guid] = handler;

			// Keep track of which events have been used, for global triggering
			jQuery.event.global[type] = true;
		});

		// Nullify elem to prevent memory leaks in IE
		elem = null;
	},

	guid: 1,
	global: {},

	// Detach an event or set of events from an element
	remove: function(elem, types, handler) {
		// don't do events on text and comment nodes
		if ( elem.nodeType == 3 || elem.nodeType == 8 )
			return;

		var events = jQuery.data(elem, "events"), ret, index;

		if ( events ) {
			// Unbind all events for the element
			if ( types === undefined || (typeof types === "string" && types.charAt(0) == ".") )
				for ( var type in events )
					this.remove( elem, type + (types || "") );
			else {
				// types is actually an event object here
				if ( types.type ) {
					handler = types.handler;
					types = types.type;
				}

				// Handle multiple events seperated by a space
				// jQuery(...).unbind("mouseover mouseout", fn);
				jQuery.each(types.split(/\s+/), function(index, type){
					// Namespaced event handlers
					var namespaces = type.split(".");
					type = namespaces.shift();
					var namespace = RegExp("(^|\\.)" + namespaces.slice().sort().join(".*\\.") + "(\\.|$)");

					if ( events[type] ) {
						// remove the given handler for the given type
						if ( handler )
							delete events[type][handler.guid];

						// remove all handlers for the given type
						else
							for ( var handle in events[type] )
								// Handle the removal of namespaced events
								if ( namespace.test(events[type][handle].type) )
									delete events[type][handle];
									
						if ( jQuery.event.specialAll[type] )
							jQuery.event.specialAll[type].teardown.call(elem, namespaces);

						// remove generic event handler if no more handlers exist
						for ( ret in events[type] ) break;
						if ( !ret ) {
							if ( !jQuery.event.special[type] || jQuery.event.special[type].teardown.call(elem, namespaces) === false ) {
								if (elem.removeEventListener)
									elem.removeEventListener(type, jQuery.data(elem, "handle"), false);
								else if (elem.detachEvent)
									elem.detachEvent("on" + type, jQuery.data(elem, "handle"));
							}
							ret = null;
							delete events[type];
						}
					}
				});
			}

			// Remove the expando if it's no longer used
			for ( ret in events ) break;
			if ( !ret ) {
				var handle = jQuery.data( elem, "handle" );
				if ( handle ) handle.elem = null;
				jQuery.removeData( elem, "events" );
				jQuery.removeData( elem, "handle" );
			}
		}
	},

	// bubbling is internal
	trigger: function( event, data, elem, bubbling ) {
		// Event object or event type
		var type = event.type || event;

		if( !bubbling ){
			event = typeof event === "object" ?
				// jQuery.Event object
				event[expando] ? event :
				// Object literal
				jQuery.extend( jQuery.Event(type), event ) :
				// Just the event type (string)
				jQuery.Event(type);

			if ( type.indexOf("!") >= 0 ) {
				event.type = type = type.slice(0, -1);
				event.exclusive = true;
			}

			// Handle a global trigger
			if ( !elem ) {
				// Don't bubble custom events when global (to avoid too much overhead)
				event.stopPropagation();
				// Only trigger if we've ever bound an event for it
				if ( this.global[type] )
					jQuery.each( jQuery.cache, function(){
						if ( this.events && this.events[type] )
							jQuery.event.trigger( event, data, this.handle.elem );
					});
			}

			// Handle triggering a single element

			// don't do events on text and comment nodes
			if ( !elem || elem.nodeType == 3 || elem.nodeType == 8 )
				return undefined;
			
			// Clean up in case it is reused
			event.result = undefined;
			event.target = elem;
			
			// Clone the incoming data, if any
			data = jQuery.makeArray(data);
			data.unshift( event );
		}

		event.currentTarget = elem;

		// Trigger the event, it is assumed that "handle" is a function
		var handle = jQuery.data(elem, "handle");
		if ( handle )
			handle.apply( elem, data );

		// Handle triggering native .onfoo handlers (and on links since we don't call .click() for links)
		if ( (!elem[type] || (jQuery.nodeName(elem, 'a') && type == "click")) && elem["on"+type] && elem["on"+type].apply( elem, data ) === false )
			event.result = false;

		// Trigger the native events (except for clicks on links)
		if ( !bubbling && elem[type] && !event.isDefaultPrevented() && !(jQuery.nodeName(elem, 'a') && type == "click") ) {
			this.triggered = true;
			try {
				elem[ type ]();
			// prevent IE from throwing an error for some hidden elements
			} catch (e) {}
		}

		this.triggered = false;

		if ( !event.isPropagationStopped() ) {
			var parent = elem.parentNode || elem.ownerDocument;
			if ( parent )
				jQuery.event.trigger(event, data, parent, true);
		}
	},

	handle: function(event) {
		// returned undefined or false
		var all, handlers;

		event = arguments[0] = jQuery.event.fix( event || window.event );
		event.currentTarget = this;
		
		// Namespaced event handlers
		var namespaces = event.type.split(".");
		event.type = namespaces.shift();

		// Cache this now, all = true means, any handler
		all = !namespaces.length && !event.exclusive;
		
		var namespace = RegExp("(^|\\.)" + namespaces.slice().sort().join(".*\\.") + "(\\.|$)");

		handlers = ( jQuery.data(this, "events") || {} )[event.type];

		for ( var j in handlers ) {
			var handler = handlers[j];

			// Filter the functions by class
			if ( all || namespace.test(handler.type) ) {
				// Pass in a reference to the handler function itself
				// So that we can later remove it
				event.handler = handler;
				event.data = handler.data;

				var ret = handler.apply(this, arguments);

				if( ret !== undefined ){
					event.result = ret;
					if ( ret === false ) {
						event.preventDefault();
						event.stopPropagation();
					}
				}

				if( event.isImmediatePropagationStopped() )
					break;

			}
		}
	},

	props: "altKey attrChange attrName bubbles button cancelable charCode clientX clientY ctrlKey currentTarget data detail eventPhase fromElement handler keyCode metaKey newValue originalTarget pageX pageY prevValue relatedNode relatedTarget screenX screenY shiftKey srcElement target toElement view wheelDelta which".split(" "),

	fix: function(event) {
		if ( event[expando] )
			return event;

		// store a copy of the original event object
		// and "clone" to set read-only properties
		var originalEvent = event;
		event = jQuery.Event( originalEvent );

		for ( var i = this.props.length, prop; i; ){
			prop = this.props[ --i ];
			event[ prop ] = originalEvent[ prop ];
		}

		// Fix target property, if necessary
		if ( !event.target )
			event.target = event.srcElement || document; // Fixes #1925 where srcElement might not be defined either

		// check if target is a textnode (safari)
		if ( event.target.nodeType == 3 )
			event.target = event.target.parentNode;

		// Add relatedTarget, if necessary
		if ( !event.relatedTarget && event.fromElement )
			event.relatedTarget = event.fromElement == event.target ? event.toElement : event.fromElement;

		// Calculate pageX/Y if missing and clientX/Y available
		if ( event.pageX == null && event.clientX != null ) {
			var doc = document.documentElement, body = document.body;
			event.pageX = event.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0) - (doc.clientLeft || 0);
			event.pageY = event.clientY + (doc && doc.scrollTop || body && body.scrollTop || 0) - (doc.clientTop || 0);
		}

		// Add which for key events
		if ( !event.which && ((event.charCode || event.charCode === 0) ? event.charCode : event.keyCode) )
			event.which = event.charCode || event.keyCode;

		// Add metaKey to non-Mac browsers (use ctrl for PC's and Meta for Macs)
		if ( !event.metaKey && event.ctrlKey )
			event.metaKey = event.ctrlKey;

		// Add which for click: 1 == left; 2 == middle; 3 == right
		// Note: button is not normalized, so don't use it
		if ( !event.which && event.button )
			event.which = (event.button & 1 ? 1 : ( event.button & 2 ? 3 : ( event.button & 4 ? 2 : 0 ) ));

		return event;
	},

	proxy: function( fn, proxy ){
		proxy = proxy || function(){ return fn.apply(this, arguments); };
		// Set the guid of unique handler to the same of original handler, so it can be removed
		proxy.guid = fn.guid = fn.guid || proxy.guid || this.guid++;
		// So proxy can be declared as an argument
		return proxy;
	},

	special: {
		ready: {
			// Make sure the ready event is setup
			setup: bindReady,
			teardown: function() {}
		}
	},
	
	specialAll: {
		live: {
			setup: function( selector, namespaces ){
				jQuery.event.add( this, namespaces[0], liveHandler );
			},
			teardown:  function( namespaces ){
				if ( namespaces.length ) {
					var remove = 0, name = RegExp("(^|\\.)" + namespaces[0] + "(\\.|$)");
					
					jQuery.each( (jQuery.data(this, "events").live || {}), function(){
						if ( name.test(this.type) )
							remove++;
					});
					
					if ( remove < 1 )
						jQuery.event.remove( this, namespaces[0], liveHandler );
				}
			}
		}
	}
};

jQuery.Event = function( src ){
	// Allow instantiation without the 'new' keyword
	if( !this.preventDefault )
		return new jQuery.Event(src);
	
	// Event object
	if( src && src.type ){
		this.originalEvent = src;
		this.type = src.type;
	// Event type
	}else
		this.type = src;

	// timeStamp is buggy for some events on Firefox(#3843)
	// So we won't rely on the native value
	this.timeStamp = now();
	
	// Mark it as fixed
	this[expando] = true;
};

function returnFalse(){
	return false;
}
function returnTrue(){
	return true;
}

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	preventDefault: function() {
		this.isDefaultPrevented = returnTrue;

		var e = this.originalEvent;
		if( !e )
			return;
		// if preventDefault exists run it on the original event
		if (e.preventDefault)
			e.preventDefault();
		// otherwise set the returnValue property of the original event to false (IE)
		e.returnValue = false;
	},
	stopPropagation: function() {
		this.isPropagationStopped = returnTrue;

		var e = this.originalEvent;
		if( !e )
			return;
		// if stopPropagation exists run it on the original event
		if (e.stopPropagation)
			e.stopPropagation();
		// otherwise set the cancelBubble property of the original event to true (IE)
		e.cancelBubble = true;
	},
	stopImmediatePropagation:function(){
		this.isImmediatePropagationStopped = returnTrue;
		this.stopPropagation();
	},
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse
};
// Checks if an event happened on an element within another element
// Used in jQuery.event.special.mouseenter and mouseleave handlers
var withinElement = function(event) {
	// Check if mouse(over|out) are still within the same parent element
	var parent = event.relatedTarget;
	// Traverse up the tree
	while ( parent && parent != this )
		try { parent = parent.parentNode; }
		catch(e) { parent = this; }
	
	if( parent != this ){
		// set the correct event type
		event.type = event.data;
		// handle event if we actually just moused on to a non sub-element
		jQuery.event.handle.apply( this, arguments );
	}
};
	
jQuery.each({ 
	mouseover: 'mouseenter', 
	mouseout: 'mouseleave'
}, function( orig, fix ){
	jQuery.event.special[ fix ] = {
		setup: function(){
			jQuery.event.add( this, orig, withinElement, fix );
		},
		teardown: function(){
			jQuery.event.remove( this, orig, withinElement );
		}
	};			   
});

jQuery.fn.extend({
	bind: function( type, data, fn ) {
		return type == "unload" ? this.one(type, data, fn) : this.each(function(){
			jQuery.event.add( this, type, fn || data, fn && data );
		});
	},

	one: function( type, data, fn ) {
		var one = jQuery.event.proxy( fn || data, function(event) {
			jQuery(this).unbind(event, one);
			return (fn || data).apply( this, arguments );
		});
		return this.each(function(){
			jQuery.event.add( this, type, one, fn && data);
		});
	},

	unbind: function( type, fn ) {
		return this.each(function(){
			jQuery.event.remove( this, type, fn );
		});
	},

	trigger: function( type, data ) {
		return this.each(function(){
			jQuery.event.trigger( type, data, this );
		});
	},

	triggerHandler: function( type, data ) {
		if( this[0] ){
			var event = jQuery.Event(type);
			event.preventDefault();
			event.stopPropagation();
			jQuery.event.trigger( event, data, this[0] );
			return event.result;
		}		
	},

	toggle: function( fn ) {
		// Save reference to arguments for access in closure
		var args = arguments, i = 1;

		// link all the functions, so any of them can unbind this click handler
		while( i < args.length )
			jQuery.event.proxy( fn, args[i++] );

		return this.click( jQuery.event.proxy( fn, function(event) {
			// Figure out which function to execute
			this.lastToggle = ( this.lastToggle || 0 ) % i;

			// Make sure that clicks stop
			event.preventDefault();

			// and execute the function
			return args[ this.lastToggle++ ].apply( this, arguments ) || false;
		}));
	},

	hover: function(fnOver, fnOut) {
		return this.mouseenter(fnOver).mouseleave(fnOut);
	},

	ready: function(fn) {
		// Attach the listeners
		bindReady();

		// If the DOM is already ready
		if ( jQuery.isReady )
			// Execute the function immediately
			fn.call( document, jQuery );

		// Otherwise, remember the function for later
		else
			// Add the function to the wait list
			jQuery.readyList.push( fn );

		return this;
	},
	
	live: function( type, fn ){
		var proxy = jQuery.event.proxy( fn );
		proxy.guid += this.selector + type;

		jQuery(document).bind( liveConvert(type, this.selector), this.selector, proxy );

		return this;
	},
	
	die: function( type, fn ){
		jQuery(document).unbind( liveConvert(type, this.selector), fn ? { guid: fn.guid + this.selector + type } : null );
		return this;
	}
});

function liveHandler( event ){
	var check = RegExp("(^|\\.)" + event.type + "(\\.|$)"),
		stop = true,
		elems = [];

	jQuery.each(jQuery.data(this, "events").live || [], function(i, fn){
		if ( check.test(fn.type) ) {
			var elem = jQuery(event.target).closest(fn.data)[0];
			if ( elem )
				elems.push({ elem: elem, fn: fn });
		}
	});

	elems.sort(function(a,b) {
		return jQuery.data(a.elem, "closest") - jQuery.data(b.elem, "closest");
	});
	
	jQuery.each(elems, function(){
		if ( this.fn.call(this.elem, event, this.fn.data) === false )
			return (stop = false);
	});

	return stop;
}

function liveConvert(type, selector){
	return ["live", type, selector.replace(/\./g, "`").replace(/ /g, "|")].join(".");
}

jQuery.extend({
	isReady: false,
	readyList: [],
	// Handle when the DOM is ready
	ready: function() {
		// Make sure that the DOM is not already loaded
		if ( !jQuery.isReady ) {
			// Remember that the DOM is ready
			jQuery.isReady = true;

			// If there are functions bound, to execute
			if ( jQuery.readyList ) {
				// Execute all of them
				jQuery.each( jQuery.readyList, function(){
					this.call( document, jQuery );
				});

				// Reset the list of functions
				jQuery.readyList = null;
			}

			// Trigger any bound ready events
			jQuery(document).triggerHandler("ready");
		}
	}
});

var readyBound = false;

function bindReady(){
	if ( readyBound ) return;
	readyBound = true;

	// Mozilla, Opera and webkit nightlies currently support this event
	if ( document.addEventListener ) {
		// Use the handy event callback
		document.addEventListener( "DOMContentLoaded", function(){
			document.removeEventListener( "DOMContentLoaded", arguments.callee, false );
			jQuery.ready();
		}, false );

	// If IE event model is used
	} else if ( document.attachEvent ) {
		// ensure firing before onload,
		// maybe late but safe also for iframes
		document.attachEvent("onreadystatechange", function(){
			if ( document.readyState === "complete" ) {
				document.detachEvent( "onreadystatechange", arguments.callee );
				jQuery.ready();
			}
		});

		// If IE and not an iframe
		// continually check to see if the document is ready
		if ( document.documentElement.doScroll && window == window.top ) (function(){
			if ( jQuery.isReady ) return;

			try {
				// If IE is used, use the trick by Diego Perini
				// http://javascript.nwbox.com/IEContentLoaded/
				document.documentElement.doScroll("left");
			} catch( error ) {
				setTimeout( arguments.callee, 0 );
				return;
			}

			// and execute any waiting functions
			jQuery.ready();
		})();
	}

	// A fallback to window.onload, that will always work
	jQuery.event.add( window, "load", jQuery.ready );
}

jQuery.each( ("blur,focus,load,resize,scroll,unload,click,dblclick," +
	"mousedown,mouseup,mousemove,mouseover,mouseout,mouseenter,mouseleave," +
	"change,select,submit,keydown,keypress,keyup,error").split(","), function(i, name){

	// Handle event binding
	jQuery.fn[name] = function(fn){
		return fn ? this.bind(name, fn) : this.trigger(name);
	};
});

// Prevent memory leaks in IE
// And prevent errors on refresh with events like mouseover in other browsers
// Window isn't included so as not to unbind existing unload events
jQuery( window ).bind( 'unload', function(){ 
	for ( var id in jQuery.cache )
		// Skip the window
		if ( id != 1 && jQuery.cache[ id ].handle )
			jQuery.event.remove( jQuery.cache[ id ].handle.elem );
}); 
(function(){

	jQuery.support = {};

	var root = document.documentElement,
		script = document.createElement("script"),
		div = document.createElement("div"),
		id = "script" + (new Date).getTime();

	div.style.display = "none";
	div.innerHTML = '   <link/><table></table><a href="/a" style="color:red;float:left;opacity:.5;">a</a><select><option>text</option></select><object><param/></object>';

	var all = div.getElementsByTagName("*"),
		a = div.getElementsByTagName("a")[0];

	// Can't get basic test support
	if ( !all || !all.length || !a ) {
		return;
	}

	jQuery.support = {
		// IE strips leading whitespace when .innerHTML is used
		leadingWhitespace: div.firstChild.nodeType == 3,
		
		// Make sure that tbody elements aren't automatically inserted
		// IE will insert them into empty tables
		tbody: !div.getElementsByTagName("tbody").length,
		
		// Make sure that you can get all elements in an <object> element
		// IE 7 always returns no results
		objectAll: !!div.getElementsByTagName("object")[0]
			.getElementsByTagName("*").length,
		
		// Make sure that link elements get serialized correctly by innerHTML
		// This requires a wrapper element in IE
		htmlSerialize: !!div.getElementsByTagName("link").length,
		
		// Get the style information from getAttribute
		// (IE uses .cssText insted)
		style: /red/.test( a.getAttribute("style") ),
		
		// Make sure that URLs aren't manipulated
		// (IE normalizes it by default)
		hrefNormalized: a.getAttribute("href") === "/a",
		
		// Make sure that element opacity exists
		// (IE uses filter instead)
		opacity: a.style.opacity === "0.5",
		
		// Verify style float existence
		// (IE uses styleFloat instead of cssFloat)
		cssFloat: !!a.style.cssFloat,

		// Will be defined later
		scriptEval: false,
		noCloneEvent: true,
		boxModel: null
	};
	
	script.type = "text/javascript";
	try {
		script.appendChild( document.createTextNode( "window." + id + "=1;" ) );
	} catch(e){}

	root.insertBefore( script, root.firstChild );
	
	// Make sure that the execution of code works by injecting a script
	// tag with appendChild/createTextNode
	// (IE doesn't support this, fails, and uses .text instead)
	if ( window[ id ] ) {
		jQuery.support.scriptEval = true;
		delete window[ id ];
	}

	root.removeChild( script );

	if ( div.attachEvent && div.fireEvent ) {
		div.attachEvent("onclick", function(){
			// Cloning a node shouldn't copy over any
			// bound event handlers (IE does this)
			jQuery.support.noCloneEvent = false;
			div.detachEvent("onclick", arguments.callee);
		});
		div.cloneNode(true).fireEvent("onclick");
	}

	// Figure out if the W3C box model works as expected
	// document.body must exist before we can do this
	jQuery(function(){
		var div = document.createElement("div");
		div.style.width = div.style.paddingLeft = "1px";

		document.body.appendChild( div );
		jQuery.boxModel = jQuery.support.boxModel = div.offsetWidth === 2;
		document.body.removeChild( div ).style.display = 'none';
	});
})();

var styleFloat = jQuery.support.cssFloat ? "cssFloat" : "styleFloat";

jQuery.props = {
	"for": "htmlFor",
	"class": "className",
	"float": styleFloat,
	cssFloat: styleFloat,
	styleFloat: styleFloat,
	readonly: "readOnly",
	maxlength: "maxLength",
	cellspacing: "cellSpacing",
	rowspan: "rowSpan",
	tabindex: "tabIndex"
};
jQuery.fn.extend({
	// Keep a copy of the old load
	_load: jQuery.fn.load,

	load: function( url, params, callback ) {
		if ( typeof url !== "string" )
			return this._load( url );

		var off = url.indexOf(" ");
		if ( off >= 0 ) {
			var selector = url.slice(off, url.length);
			url = url.slice(0, off);
		}

		// Default to a GET request
		var type = "GET";

		// If the second parameter was provided
		if ( params )
			// If it's a function
			if ( jQuery.isFunction( params ) ) {
				// We assume that it's the callback
				callback = params;
				params = null;

			// Otherwise, build a param string
			} else if( typeof params === "object" ) {
				params = jQuery.param( params );
				type = "POST";
			}

		var self = this;

		// Request the remote document
		jQuery.ajax({
			url: url,
			type: type,
			dataType: "html",
			data: params,
			complete: function(res, status){
				// If successful, inject the HTML into all the matched elements
				if ( status == "success" || status == "notmodified" )
					// See if a selector was specified
					self.html( selector ?
						// Create a dummy div to hold the results
						jQuery("<div/>")
							// inject the contents of the document in, removing the scripts
							// to avoid any 'Permission Denied' errors in IE
							.append(res.responseText.replace(/<script(.|\s)*?\/script>/g, ""))

							// Locate the specified elements
							.find(selector) :

						// If not, just inject the full result
						res.responseText );

				if( callback )
					self.each( callback, [res.responseText, status, res] );
			}
		});
		return this;
	},

	serialize: function() {
		return jQuery.param(this.serializeArray());
	},
	serializeArray: function() {
		return this.map(function(){
			return this.elements ? jQuery.makeArray(this.elements) : this;
		})
		.filter(function(){
			return this.name && !this.disabled &&
				(this.checked || /select|textarea/i.test(this.nodeName) ||
					/text|hidden|password|search/i.test(this.type));
		})
		.map(function(i, elem){
			var val = jQuery(this).val();
			return val == null ? null :
				jQuery.isArray(val) ?
					jQuery.map( val, function(val, i){
						return {name: elem.name, value: val};
					}) :
					{name: elem.name, value: val};
		}).get();
	}
});

// Attach a bunch of functions for handling common AJAX events
jQuery.each( "ajaxStart,ajaxStop,ajaxComplete,ajaxError,ajaxSuccess,ajaxSend".split(","), function(i,o){
	jQuery.fn[o] = function(f){
		return this.bind(o, f);
	};
});

var jsc = now();

jQuery.extend({
  
	get: function( url, data, callback, type ) {
		// shift arguments if data argument was ommited
		if ( jQuery.isFunction( data ) ) {
			callback = data;
			data = null;
		}

		return jQuery.ajax({
			type: "GET",
			url: url,
			data: data,
			success: callback,
			dataType: type
		});
	},

	getScript: function( url, callback ) {
		return jQuery.get(url, null, callback, "script");
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get(url, data, callback, "json");
	},

	post: function( url, data, callback, type ) {
		if ( jQuery.isFunction( data ) ) {
			callback = data;
			data = {};
		}

		return jQuery.ajax({
			type: "POST",
			url: url,
			data: data,
			success: callback,
			dataType: type
		});
	},

	ajaxSetup: function( settings ) {
		jQuery.extend( jQuery.ajaxSettings, settings );
	},

	ajaxSettings: {
		url: location.href,
		global: true,
		type: "GET",
		contentType: "application/x-www-form-urlencoded",
		processData: true,
		async: true,
		/*
		timeout: 0,
		data: null,
		username: null,
		password: null,
		*/
		// Create the request object; Microsoft failed to properly
		// implement the XMLHttpRequest in IE7, so we use the ActiveXObject when it is available
		// This function can be overriden by calling jQuery.ajaxSetup
		xhr:function(){
			return window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest();
		},
		accepts: {
			xml: "application/xml, text/xml",
			html: "text/html",
			script: "text/javascript, application/javascript",
			json: "application/json, text/javascript",
			text: "text/plain",
			_default: "*/*"
		}
	},

	// Last-Modified header cache for next request
	lastModified: {},

	ajax: function( s ) {
		// Extend the settings, but re-extend 's' so that it can be
		// checked again later (in the test suite, specifically)
		s = jQuery.extend(true, s, jQuery.extend(true, {}, jQuery.ajaxSettings, s));

		var jsonp, jsre = /=\?(&|$)/g, status, data,
			type = s.type.toUpperCase();

		// convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" )
			s.data = jQuery.param(s.data);

		// Handle JSONP Parameter Callbacks
		if ( s.dataType == "jsonp" ) {
			if ( type == "GET" ) {
				if ( !s.url.match(jsre) )
					s.url += (s.url.match(/\?/) ? "&" : "?") + (s.jsonp || "callback") + "=?";
			} else if ( !s.data || !s.data.match(jsre) )
				s.data = (s.data ? s.data + "&" : "") + (s.jsonp || "callback") + "=?";
			s.dataType = "json";
		}

		// Build temporary JSONP function
		if ( s.dataType == "json" && (s.data && s.data.match(jsre) || s.url.match(jsre)) ) {
			jsonp = "jsonp" + jsc++;

			// Replace the =? sequence both in the query string and the data
			if ( s.data )
				s.data = (s.data + "").replace(jsre, "=" + jsonp + "$1");
			s.url = s.url.replace(jsre, "=" + jsonp + "$1");

			// We need to make sure
			// that a JSONP style response is executed properly
			s.dataType = "script";

			// Handle JSONP-style loading
			window[ jsonp ] = function(tmp){
				data = tmp;
				success();
				complete();
				// Garbage collect
				window[ jsonp ] = undefined;
				try{ delete window[ jsonp ]; } catch(e){}
				if ( head )
					head.removeChild( script );
			};
		}

		if ( s.dataType == "script" && s.cache == null )
			s.cache = false;

		if ( s.cache === false && type == "GET" ) {
			var ts = now();
			// try replacing _= if it is there
			var ret = s.url.replace(/(\?|&)_=.*?(&|$)/, "$1_=" + ts + "$2");
			// if nothing was replaced, add timestamp to the end
			s.url = ret + ((ret == s.url) ? (s.url.match(/\?/) ? "&" : "?") + "_=" + ts : "");
		}

		// If data is available, append data to url for get requests
		if ( s.data && type == "GET" ) {
			s.url += (s.url.match(/\?/) ? "&" : "?") + s.data;

			// IE likes to send both get and post data, prevent this
			s.data = null;
		}

		// Watch for a new set of requests
		if ( s.global && ! jQuery.active++ )
			jQuery.event.trigger( "ajaxStart" );

		// Matches an absolute URL, and saves the domain
		var parts = /^(\w+:)?\/\/([^\/?#]+)/.exec( s.url );

		// If we're requesting a remote document
		// and trying to load JSON or Script with a GET
		if ( s.dataType == "script" && type == "GET" && parts
			&& ( parts[1] && parts[1] != location.protocol || parts[2] != location.host )){

			var head = document.getElementsByTagName("head")[0];
			var script = document.createElement("script");
			script.src = s.url;
			if (s.scriptCharset)
				script.charset = s.scriptCharset;

			// Handle Script loading
			if ( !jsonp ) {
				var done = false;

				// Attach handlers for all browsers
				script.onload = script.onreadystatechange = function(){
					if ( !done && (!this.readyState ||
							this.readyState == "loaded" || this.readyState == "complete") ) {
						done = true;
						success();
						complete();

						// Handle memory leak in IE
						script.onload = script.onreadystatechange = null;
						head.removeChild( script );
					}
				};
			}

			head.appendChild(script);

			// We handle everything using the script element injection
			return undefined;
		}

		var requestDone = false;

		// Create the request object
		var xhr = s.xhr();

		// Open the socket
		// Passing null username, generates a login popup on Opera (#2865)
		if( s.username )
			xhr.open(type, s.url, s.async, s.username, s.password);
		else
			xhr.open(type, s.url, s.async);

		// Need an extra try/catch for cross domain requests in Firefox 3
		try {
			// Set the correct header, if data is being sent
			if ( s.data )
				xhr.setRequestHeader("Content-Type", s.contentType);

			// Set the If-Modified-Since header, if ifModified mode.
			if ( s.ifModified )
				xhr.setRequestHeader("If-Modified-Since",
					jQuery.lastModified[s.url] || "Thu, 01 Jan 1970 00:00:00 GMT" );

			// Set header so the called script knows that it's an XMLHttpRequest
			xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");

			// Set the Accepts header for the server, depending on the dataType
			xhr.setRequestHeader("Accept", s.dataType && s.accepts[ s.dataType ] ?
				s.accepts[ s.dataType ] + ", */*" :
				s.accepts._default );
		} catch(e){}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend && s.beforeSend(xhr, s) === false ) {
			// Handle the global AJAX counter
			if ( s.global && ! --jQuery.active )
				jQuery.event.trigger( "ajaxStop" );
			// close opended socket
			xhr.abort();
			return false;
		}

		if ( s.global )
			jQuery.event.trigger("ajaxSend", [xhr, s]);

		// Wait for a response to come back
		var onreadystatechange = function(isTimeout){
			// The request was aborted, clear the interval and decrement jQuery.active
			if (xhr.readyState == 0) {
				if (ival) {
					// clear poll interval
					clearInterval(ival);
					ival = null;
					// Handle the global AJAX counter
					if ( s.global && ! --jQuery.active )
						jQuery.event.trigger( "ajaxStop" );
				}
			// The transfer is complete and the data is available, or the request timed out
			} else if ( !requestDone && xhr && (xhr.readyState == 4 || isTimeout == "timeout") ) {
				requestDone = true;

				// clear poll interval
				if (ival) {
					clearInterval(ival);
					ival = null;
				}

				status = isTimeout == "timeout" ? "timeout" :
					!jQuery.httpSuccess( xhr ) ? "error" :
					s.ifModified && jQuery.httpNotModified( xhr, s.url ) ? "notmodified" :
					"success";

				if ( status == "success" ) {
					// Watch for, and catch, XML document parse errors
					try {
						// process the data (runs the xml through httpData regardless of callback)
						data = jQuery.httpData( xhr, s.dataType, s );
					} catch(e) {
						status = "parsererror";
					}
				}

				// Make sure that the request was successful or notmodified
				if ( status == "success" ) {
					// Cache Last-Modified header, if ifModified mode.
					var modRes;
					try {
						modRes = xhr.getResponseHeader("Last-Modified");
					} catch(e) {} // swallow exception thrown by FF if header is not available

					if ( s.ifModified && modRes )
						jQuery.lastModified[s.url] = modRes;

					// JSONP handles its own success callback
					if ( !jsonp )
						success();
				} else
					jQuery.handleError(s, xhr, status);

				// Fire the complete handlers
				complete();

				if ( isTimeout )
					xhr.abort();

				// Stop memory leaks
				if ( s.async )
					xhr = null;
			}
		};

		if ( s.async ) {
			// don't attach the handler to the request, just poll it instead
			var ival = setInterval(onreadystatechange, 13);

			// Timeout checker
			if ( s.timeout > 0 )
				setTimeout(function(){
					// Check to see if the request is still happening
					if ( xhr && !requestDone )
						onreadystatechange( "timeout" );
				}, s.timeout);
		}

		// Send the data
		try {
			xhr.send(s.data);
		} catch(e) {
			jQuery.handleError(s, xhr, null, e);
		}

		// firefox 1.5 doesn't fire statechange for sync requests
		if ( !s.async )
			onreadystatechange();

		function success(){
			// If a local callback was specified, fire it and pass it the data
			if ( s.success )
				s.success( data, status );

			// Fire the global callback
			if ( s.global )
				jQuery.event.trigger( "ajaxSuccess", [xhr, s] );
		}

		function complete(){
			// Process result
			if ( s.complete )
				s.complete(xhr, status);

			// The request was completed
			if ( s.global )
				jQuery.event.trigger( "ajaxComplete", [xhr, s] );

			// Handle the global AJAX counter
			if ( s.global && ! --jQuery.active )
				jQuery.event.trigger( "ajaxStop" );
		}

		// return XMLHttpRequest to allow aborting the request etc.
		return xhr;
	},

	handleError: function( s, xhr, status, e ) {
		// If a local callback was specified, fire it
		if ( s.error ) s.error( xhr, status, e );

		// Fire the global callback
		if ( s.global )
			jQuery.event.trigger( "ajaxError", [xhr, s, e] );
	},

	// Counter for holding the number of active queries
	active: 0,

	// Determines if an XMLHttpRequest was successful or not
	httpSuccess: function( xhr ) {
		try {
			// IE error sometimes returns 1223 when it should be 204 so treat it as success, see #1450
			return !xhr.status && location.protocol == "file:" ||
				( xhr.status >= 200 && xhr.status < 300 ) || xhr.status == 304 || xhr.status == 1223;
		} catch(e){}
		return false;
	},

	// Determines if an XMLHttpRequest returns NotModified
	httpNotModified: function( xhr, url ) {
		try {
			var xhrRes = xhr.getResponseHeader("Last-Modified");

			// Firefox always returns 200. check Last-Modified date
			return xhr.status == 304 || xhrRes == jQuery.lastModified[url];
		} catch(e){}
		return false;
	},

	httpData: function( xhr, type, s ) {
		var ct = xhr.getResponseHeader("content-type"),
			xml = type == "xml" || !type && ct && ct.indexOf("xml") >= 0,
			data = xml ? xhr.responseXML : xhr.responseText;

		if ( xml && data.documentElement.tagName == "parsererror" )
			throw "parsererror";
			
		// Allow a pre-filtering function to sanitize the response
		// s != null is checked to keep backwards compatibility
		if( s && s.dataFilter )
			data = s.dataFilter( data, type );

		// The filter can actually parse the response
		if( typeof data === "string" ){

			// If the type is "script", eval it in global context
			if ( type == "script" )
				jQuery.globalEval( data );

			// Get the JavaScript object, if JSON is used.
			if ( type == "json" )
				data = window["eval"]("(" + data + ")");
		}
		
		return data;
	},

	// Serialize an array of form elements or a set of
	// key/values into a query string
	param: function( a ) {
		var s = [ ];

		function add( key, value ){
			s[ s.length ] = encodeURIComponent(key) + '=' + encodeURIComponent(value);
		};

		// If an array was passed in, assume that it is an array
		// of form elements
		if ( jQuery.isArray(a) || a.jquery )
			// Serialize the form elements
			jQuery.each( a, function(){
				add( this.name, this.value );
			});

		// Otherwise, assume that it's an object of key/value pairs
		else
			// Serialize the key/values
			for ( var j in a )
				// If the value is an array then the key names need to be repeated
				if ( jQuery.isArray(a[j]) )
					jQuery.each( a[j], function(){
						add( j, this );
					});
				else
					add( j, jQuery.isFunction(a[j]) ? a[j]() : a[j] );

		// Return the resulting serialization
		return s.join("&").replace(/%20/g, "+");
	}

});
var elemdisplay = {},
	timerId,
	fxAttrs = [
		// height animations
		[ "height", "marginTop", "marginBottom", "paddingTop", "paddingBottom" ],
		// width animations
		[ "width", "marginLeft", "marginRight", "paddingLeft", "paddingRight" ],
		// opacity animations
		[ "opacity" ]
	];

function genFx( type, num ){
	var obj = {};
	jQuery.each( fxAttrs.concat.apply([], fxAttrs.slice(0,num)), function(){
		obj[ this ] = type;
	});
	return obj;
}

jQuery.fn.extend({
	show: function(speed,callback){
		if ( speed ) {
			return this.animate( genFx("show", 3), speed, callback);
		} else {
			for ( var i = 0, l = this.length; i < l; i++ ){
				var old = jQuery.data(this[i], "olddisplay");
				
				this[i].style.display = old || "";
				
				if ( jQuery.css(this[i], "display") === "none" ) {
					var tagName = this[i].tagName, display;
					
					if ( elemdisplay[ tagName ] ) {
						display = elemdisplay[ tagName ];
					} else {
						var elem = jQuery("<" + tagName + " />").appendTo("body");
						
						display = elem.css("display");
						if ( display === "none" )
							display = "block";
						
						elem.remove();
						
						elemdisplay[ tagName ] = display;
					}
					
					jQuery.data(this[i], "olddisplay", display);
				}
			}

			// Set the display of the elements in a second loop
			// to avoid the constant reflow
			for ( var i = 0, l = this.length; i < l; i++ ){
				this[i].style.display = jQuery.data(this[i], "olddisplay") || "";
			}
			
			return this;
		}
	},

	hide: function(speed,callback){
		if ( speed ) {
			return this.animate( genFx("hide", 3), speed, callback);
		} else {
			for ( var i = 0, l = this.length; i < l; i++ ){
				var old = jQuery.data(this[i], "olddisplay");
				if ( !old && old !== "none" )
					jQuery.data(this[i], "olddisplay", jQuery.css(this[i], "display"));
			}

			// Set the display of the elements in a second loop
			// to avoid the constant reflow
			for ( var i = 0, l = this.length; i < l; i++ ){
				this[i].style.display = "none";
			}

			return this;
		}
	},

	// Save the old toggle function
	_toggle: jQuery.fn.toggle,

	toggle: function( fn, fn2 ){
		var bool = typeof fn === "boolean";

		return jQuery.isFunction(fn) && jQuery.isFunction(fn2) ?
			this._toggle.apply( this, arguments ) :
			fn == null || bool ?
				this.each(function(){
					var state = bool ? fn : jQuery(this).is(":hidden");
					jQuery(this)[ state ? "show" : "hide" ]();
				}) :
				this.animate(genFx("toggle", 3), fn, fn2);
	},

	fadeTo: function(speed,to,callback){
		return this.animate({opacity: to}, speed, callback);
	},

	animate: function( prop, speed, easing, callback ) {
		var optall = jQuery.speed(speed, easing, callback);

		return this[ optall.queue === false ? "each" : "queue" ](function(){
		
			var opt = jQuery.extend({}, optall), p,
				hidden = this.nodeType == 1 && jQuery(this).is(":hidden"),
				self = this;
	
			for ( p in prop ) {
				if ( prop[p] == "hide" && hidden || prop[p] == "show" && !hidden )
					return opt.complete.call(this);

				if ( ( p == "height" || p == "width" ) && this.style ) {
					// Store display property
					opt.display = jQuery.css(this, "display");

					// Make sure that nothing sneaks out
					opt.overflow = this.style.overflow;
				}
			}

			if ( opt.overflow != null )
				this.style.overflow = "hidden";

			opt.curAnim = jQuery.extend({}, prop);

			jQuery.each( prop, function(name, val){
				var e = new jQuery.fx( self, opt, name );

				if ( /toggle|show|hide/.test(val) )
					e[ val == "toggle" ? hidden ? "show" : "hide" : val ]( prop );
				else {
					var parts = val.toString().match(/^([+-]=)?([\d+-.]+)(.*)$/),
						start = e.cur(true) || 0;

					if ( parts ) {
						var end = parseFloat(parts[2]),
							unit = parts[3] || "px";

						// We need to compute starting value
						if ( unit != "px" ) {
							self.style[ name ] = (end || 1) + unit;
							start = ((end || 1) / e.cur(true)) * start;
							self.style[ name ] = start + unit;
						}

						// If a +=/-= token was provided, we're doing a relative animation
						if ( parts[1] )
							end = ((parts[1] == "-=" ? -1 : 1) * end) + start;

						e.custom( start, end, unit );
					} else
						e.custom( start, val, "" );
				}
			});

			// For JS strict compliance
			return true;
		});
	},

	stop: function(clearQueue, gotoEnd){
		var timers = jQuery.timers;

		if (clearQueue)
			this.queue([]);

		this.each(function(){
			// go in reverse order so anything added to the queue during the loop is ignored
			for ( var i = timers.length - 1; i >= 0; i-- )
				if ( timers[i].elem == this ) {
					if (gotoEnd)
						// force the next step to be the last
						timers[i](true);
					timers.splice(i, 1);
				}
		});

		// start the next in the queue if the last step wasn't forced
		if (!gotoEnd)
			this.dequeue();

		return this;
	}

});

// Generate shortcuts for custom animations
jQuery.each({
	slideDown: genFx("show", 1),
	slideUp: genFx("hide", 1),
	slideToggle: genFx("toggle", 1),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" }
}, function( name, props ){
	jQuery.fn[ name ] = function( speed, callback ){
		return this.animate( props, speed, callback );
	};
});

jQuery.extend({

	speed: function(speed, easing, fn) {
		var opt = typeof speed === "object" ? speed : {
			complete: fn || !fn && easing ||
				jQuery.isFunction( speed ) && speed,
			duration: speed,
			easing: fn && easing || easing && !jQuery.isFunction(easing) && easing
		};

		opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ? opt.duration :
			jQuery.fx.speeds[opt.duration] || jQuery.fx.speeds._default;

		// Queueing
		opt.old = opt.complete;
		opt.complete = function(){
			if ( opt.queue !== false )
				jQuery(this).dequeue();
			if ( jQuery.isFunction( opt.old ) )
				opt.old.call( this );
		};

		return opt;
	},

	easing: {
		linear: function( p, n, firstNum, diff ) {
			return firstNum + diff * p;
		},
		swing: function( p, n, firstNum, diff ) {
			return ((-Math.cos(p*Math.PI)/2) + 0.5) * diff + firstNum;
		}
	},

	timers: [],

	fx: function( elem, options, prop ){
		this.options = options;
		this.elem = elem;
		this.prop = prop;

		if ( !options.orig )
			options.orig = {};
	}

});

jQuery.fx.prototype = {

	// Simple function for setting a style value
	update: function(){
		if ( this.options.step )
			this.options.step.call( this.elem, this.now, this );

		(jQuery.fx.step[this.prop] || jQuery.fx.step._default)( this );

		// Set display property to block for height/width animations
		if ( ( this.prop == "height" || this.prop == "width" ) && this.elem.style )
			this.elem.style.display = "block";
	},

	// Get the current size
	cur: function(force){
		if ( this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null) )
			return this.elem[ this.prop ];

		var r = parseFloat(jQuery.css(this.elem, this.prop, force));
		return r && r > -10000 ? r : parseFloat(jQuery.curCSS(this.elem, this.prop)) || 0;
	},

	// Start an animation from one number to another
	custom: function(from, to, unit){
		this.startTime = now();
		this.start = from;
		this.end = to;
		this.unit = unit || this.unit || "px";
		this.now = this.start;
		this.pos = this.state = 0;

		var self = this;
		function t(gotoEnd){
			return self.step(gotoEnd);
		}

		t.elem = this.elem;

		if ( t() && jQuery.timers.push(t) && !timerId ) {
			timerId = setInterval(function(){
				var timers = jQuery.timers;

				for ( var i = 0; i < timers.length; i++ )
					if ( !timers[i]() )
						timers.splice(i--, 1);

				if ( !timers.length ) {
					clearInterval( timerId );
					timerId = undefined;
				}
			}, 13);
		}
	},

	// Simple 'show' function
	show: function(){
		// Remember where we started, so that we can go back to it later
		this.options.orig[this.prop] = jQuery.attr( this.elem.style, this.prop );
		this.options.show = true;

		// Begin the animation
		// Make sure that we start at a small width/height to avoid any
		// flash of content
		this.custom(this.prop == "width" || this.prop == "height" ? 1 : 0, this.cur());

		// Start by showing the element
		jQuery(this.elem).show();
	},

	// Simple 'hide' function
	hide: function(){
		// Remember where we started, so that we can go back to it later
		this.options.orig[this.prop] = jQuery.attr( this.elem.style, this.prop );
		this.options.hide = true;

		// Begin the animation
		this.custom(this.cur(), 0);
	},

	// Each step of an animation
	step: function(gotoEnd){
		var t = now();

		if ( gotoEnd || t >= this.options.duration + this.startTime ) {
			this.now = this.end;
			this.pos = this.state = 1;
			this.update();

			this.options.curAnim[ this.prop ] = true;

			var done = true;
			for ( var i in this.options.curAnim )
				if ( this.options.curAnim[i] !== true )
					done = false;

			if ( done ) {
				if ( this.options.display != null ) {
					// Reset the overflow
					this.elem.style.overflow = this.options.overflow;

					// Reset the display
					this.elem.style.display = this.options.display;
					if ( jQuery.css(this.elem, "display") == "none" )
						this.elem.style.display = "block";
				}

				// Hide the element if the "hide" operation was done
				if ( this.options.hide )
					jQuery(this.elem).hide();

				// Reset the properties, if the item has been hidden or shown
				if ( this.options.hide || this.options.show )
					for ( var p in this.options.curAnim )
						jQuery.attr(this.elem.style, p, this.options.orig[p]);
					
				// Execute the complete function
				this.options.complete.call( this.elem );
			}

			return false;
		} else {
			var n = t - this.startTime;
			this.state = n / this.options.duration;

			// Perform the easing function, defaults to swing
			this.pos = jQuery.easing[this.options.easing || (jQuery.easing.swing ? "swing" : "linear")](this.state, n, 0, 1, this.options.duration);
			this.now = this.start + ((this.end - this.start) * this.pos);

			// Perform the next step of the animation
			this.update();
		}

		return true;
	}

};

jQuery.extend( jQuery.fx, {
	speeds:{
		slow: 600,
 		fast: 200,
 		// Default speed
 		_default: 400
	},
	step: {

		opacity: function(fx){
			jQuery.attr(fx.elem.style, "opacity", fx.now);
		},

		_default: function(fx){
			if ( fx.elem.style && fx.elem.style[ fx.prop ] != null )
				fx.elem.style[ fx.prop ] = fx.now + fx.unit;
			else
				fx.elem[ fx.prop ] = fx.now;
		}
	}
});
if ( document.documentElement["getBoundingClientRect"] )
	jQuery.fn.offset = function() {
		if ( !this[0] ) return { top: 0, left: 0 };
		if ( this[0] === this[0].ownerDocument.body ) return jQuery.offset.bodyOffset( this[0] );
		var box  = this[0].getBoundingClientRect(), doc = this[0].ownerDocument, body = doc.body, docElem = doc.documentElement,
			clientTop = docElem.clientTop || body.clientTop || 0, clientLeft = docElem.clientLeft || body.clientLeft || 0,
			top  = box.top  + (self.pageYOffset || jQuery.boxModel && docElem.scrollTop  || body.scrollTop ) - clientTop,
			left = box.left + (self.pageXOffset || jQuery.boxModel && docElem.scrollLeft || body.scrollLeft) - clientLeft;
		return { top: top, left: left };
	};
else 
	jQuery.fn.offset = function() {
		if ( !this[0] ) return { top: 0, left: 0 };
		if ( this[0] === this[0].ownerDocument.body ) return jQuery.offset.bodyOffset( this[0] );
		jQuery.offset.initialized || jQuery.offset.initialize();

		var elem = this[0], offsetParent = elem.offsetParent, prevOffsetParent = elem,
			doc = elem.ownerDocument, computedStyle, docElem = doc.documentElement,
			body = doc.body, defaultView = doc.defaultView,
			prevComputedStyle = defaultView.getComputedStyle(elem, null),
			top = elem.offsetTop, left = elem.offsetLeft;

		while ( (elem = elem.parentNode) && elem !== body && elem !== docElem ) {
			computedStyle = defaultView.getComputedStyle(elem, null);
			top -= elem.scrollTop, left -= elem.scrollLeft;
			if ( elem === offsetParent ) {
				top += elem.offsetTop, left += elem.offsetLeft;
				if ( jQuery.offset.doesNotAddBorder && !(jQuery.offset.doesAddBorderForTableAndCells && /^t(able|d|h)$/i.test(elem.tagName)) )
					top  += parseInt( computedStyle.borderTopWidth,  10) || 0,
					left += parseInt( computedStyle.borderLeftWidth, 10) || 0;
				prevOffsetParent = offsetParent, offsetParent = elem.offsetParent;
			}
			if ( jQuery.offset.subtractsBorderForOverflowNotVisible && computedStyle.overflow !== "visible" )
				top  += parseInt( computedStyle.borderTopWidth,  10) || 0,
				left += parseInt( computedStyle.borderLeftWidth, 10) || 0;
			prevComputedStyle = computedStyle;
		}

		if ( prevComputedStyle.position === "relative" || prevComputedStyle.position === "static" )
			top  += body.offsetTop,
			left += body.offsetLeft;

		if ( prevComputedStyle.position === "fixed" )
			top  += Math.max(docElem.scrollTop, body.scrollTop),
			left += Math.max(docElem.scrollLeft, body.scrollLeft);

		return { top: top, left: left };
	};

jQuery.offset = {
	initialize: function() {
		if ( this.initialized ) return;
		var body = document.body, container = document.createElement('div'), innerDiv, checkDiv, table, td, rules, prop, bodyMarginTop = body.style.marginTop,
			html = '<div style="position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;"><div></div></div><table style="position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;" cellpadding="0" cellspacing="0"><tr><td></td></tr></table>';

		rules = { position: 'absolute', top: 0, left: 0, margin: 0, border: 0, width: '1px', height: '1px', visibility: 'hidden' };
		for ( prop in rules ) container.style[prop] = rules[prop];

		container.innerHTML = html;
		body.insertBefore(container, body.firstChild);
		innerDiv = container.firstChild, checkDiv = innerDiv.firstChild, td = innerDiv.nextSibling.firstChild.firstChild;

		this.doesNotAddBorder = (checkDiv.offsetTop !== 5);
		this.doesAddBorderForTableAndCells = (td.offsetTop === 5);

		innerDiv.style.overflow = 'hidden', innerDiv.style.position = 'relative';
		this.subtractsBorderForOverflowNotVisible = (checkDiv.offsetTop === -5);

		body.style.marginTop = '1px';
		this.doesNotIncludeMarginInBodyOffset = (body.offsetTop === 0);
		body.style.marginTop = bodyMarginTop;

		body.removeChild(container);
		this.initialized = true;
	},

	bodyOffset: function(body) {
		jQuery.offset.initialized || jQuery.offset.initialize();
		var top = body.offsetTop, left = body.offsetLeft;
		if ( jQuery.offset.doesNotIncludeMarginInBodyOffset )
			top  += parseInt( jQuery.curCSS(body, 'marginTop',  true), 10 ) || 0,
			left += parseInt( jQuery.curCSS(body, 'marginLeft', true), 10 ) || 0;
		return { top: top, left: left };
	}
};


jQuery.fn.extend({
	position: function() {
		var left = 0, top = 0, results;

		if ( this[0] ) {
			// Get *real* offsetParent
			var offsetParent = this.offsetParent(),

			// Get correct offsets
			offset       = this.offset(),
			parentOffset = /^body|html$/i.test(offsetParent[0].tagName) ? { top: 0, left: 0 } : offsetParent.offset();

			// Subtract element margins
			// note: when an element has margin: auto the offsetLeft and marginLeft 
			// are the same in Safari causing offset.left to incorrectly be 0
			offset.top  -= num( this, 'marginTop'  );
			offset.left -= num( this, 'marginLeft' );

			// Add offsetParent borders
			parentOffset.top  += num( offsetParent, 'borderTopWidth'  );
			parentOffset.left += num( offsetParent, 'borderLeftWidth' );

			// Subtract the two offsets
			results = {
				top:  offset.top  - parentOffset.top,
				left: offset.left - parentOffset.left
			};
		}

		return results;
	},

	offsetParent: function() {
		var offsetParent = this[0].offsetParent || document.body;
		while ( offsetParent && (!/^body|html$/i.test(offsetParent.tagName) && jQuery.css(offsetParent, 'position') == 'static') )
			offsetParent = offsetParent.offsetParent;
		return jQuery(offsetParent);
	}
});


// Create scrollLeft and scrollTop methods
jQuery.each( ['Left', 'Top'], function(i, name) {
	var method = 'scroll' + name;
	
	jQuery.fn[ method ] = function(val) {
		if (!this[0]) return null;

		return val !== undefined ?

			// Set the scroll offset
			this.each(function() {
				this == window || this == document ?
					window.scrollTo(
						!i ? val : jQuery(window).scrollLeft(),
						 i ? val : jQuery(window).scrollTop()
					) :
					this[ method ] = val;
			}) :

			// Return the scroll offset
			this[0] == window || this[0] == document ?
				self[ i ? 'pageYOffset' : 'pageXOffset' ] ||
					jQuery.boxModel && document.documentElement[ method ] ||
					document.body[ method ] :
				this[0][ method ];
	};
});
// Create innerHeight, innerWidth, outerHeight and outerWidth methods
jQuery.each([ "Height", "Width" ], function(i, name){

	var tl = i ? "Left"  : "Top",  // top or left
		br = i ? "Right" : "Bottom", // bottom or right
		lower = name.toLowerCase();

	// innerHeight and innerWidth
	jQuery.fn["inner" + name] = function(){
		return this[0] ?
			jQuery.css( this[0], lower, false, "padding" ) :
			null;
	};

	// outerHeight and outerWidth
	jQuery.fn["outer" + name] = function(margin) {
		return this[0] ?
			jQuery.css( this[0], lower, false, margin ? "margin" : "border" ) :
			null;
	};
	
	var type = name.toLowerCase();

	jQuery.fn[ type ] = function( size ) {
		// Get window width or height
		return this[0] == window ?
			// Everyone else use document.documentElement or document.body depending on Quirks vs Standards mode
			document.compatMode == "CSS1Compat" && document.documentElement[ "client" + name ] ||
			document.body[ "client" + name ] :

			// Get document width or height
			this[0] == document ?
				// Either scroll[Width/Height] or offset[Width/Height], whichever is greater
				Math.max(
					document.documentElement["client" + name],
					document.body["scroll" + name], document.documentElement["scroll" + name],
					document.body["offset" + name], document.documentElement["offset" + name]
				) :

				// Get or set width or height on the element
				size === undefined ?
					// Get width or height on the element
					(this.length ? jQuery.css( this[0], type ) : null) :

					// Set the width or height on the element (default to pixels if value is unitless)
					this.css( type, typeof size === "string" ? size : size + "px" );
	};

});
})();

jQuery.ui||(function(c){var i=c.fn.remove,d=c.browser.mozilla&&(parseFloat(c.browser.version)<1.9);c.ui={version:"1.7.2",plugin:{add:function(k,l,n){var m=c.ui[k].prototype;for(var j in n){m.plugins[j]=m.plugins[j]||[];m.plugins[j].push([l,n[j]])}},call:function(j,l,k){var n=j.plugins[l];if(!n||!j.element[0].parentNode){return}for(var m=0;m<n.length;m++){if(j.options[n[m][0]]){n[m][1].apply(j.element,k)}}}},contains:function(k,j){return document.compareDocumentPosition?k.compareDocumentPosition(j)&16:k!==j&&k.contains(j)},hasScroll:function(m,k){if(c(m).css("overflow")=="hidden"){return false}var j=(k&&k=="left")?"scrollLeft":"scrollTop",l=false;if(m[j]>0){return true}m[j]=1;l=(m[j]>0);m[j]=0;return l},isOverAxis:function(k,j,l){return(k>j)&&(k<(j+l))},isOver:function(o,k,n,m,j,l){return c.ui.isOverAxis(o,n,j)&&c.ui.isOverAxis(k,m,l)},keyCode:{BACKSPACE:8,CAPS_LOCK:20,COMMA:188,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,INSERT:45,LEFT:37,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SHIFT:16,SPACE:32,TAB:9,UP:38}};if(d){var f=c.attr,e=c.fn.removeAttr,h="http://www.w3.org/2005/07/aaa",a=/^aria-/,b=/^wairole:/;c.attr=function(k,j,l){var m=l!==undefined;return(j=="role"?(m?f.call(this,k,j,"wairole:"+l):(f.apply(this,arguments)||"").replace(b,"")):(a.test(j)?(m?k.setAttributeNS(h,j.replace(a,"aaa:"),l):f.call(this,k,j.replace(a,"aaa:"))):f.apply(this,arguments)))};c.fn.removeAttr=function(j){return(a.test(j)?this.each(function(){this.removeAttributeNS(h,j.replace(a,""))}):e.call(this,j))}}c.fn.extend({remove:function(){c("*",this).add(this).each(function(){c(this).triggerHandler("remove")});return i.apply(this,arguments)},enableSelection:function(){return this.attr("unselectable","off").css("MozUserSelect","").unbind("selectstart.ui")},disableSelection:function(){return this.attr("unselectable","on").css("MozUserSelect","none").bind("selectstart.ui",function(){return false})},scrollParent:function(){var j;if((c.browser.msie&&(/(static|relative)/).test(this.css("position")))||(/absolute/).test(this.css("position"))){j=this.parents().filter(function(){return(/(relative|absolute|fixed)/).test(c.curCSS(this,"position",1))&&(/(auto|scroll)/).test(c.curCSS(this,"overflow",1)+c.curCSS(this,"overflow-y",1)+c.curCSS(this,"overflow-x",1))}).eq(0)}else{j=this.parents().filter(function(){return(/(auto|scroll)/).test(c.curCSS(this,"overflow",1)+c.curCSS(this,"overflow-y",1)+c.curCSS(this,"overflow-x",1))}).eq(0)}return(/fixed/).test(this.css("position"))||!j.length?c(document):j}});c.extend(c.expr[":"],{data:function(l,k,j){return !!c.data(l,j[3])},focusable:function(k){var l=k.nodeName.toLowerCase(),j=c.attr(k,"tabindex");return(/input|select|textarea|button|object/.test(l)?!k.disabled:"a"==l||"area"==l?k.href||!isNaN(j):!isNaN(j))&&!c(k)["area"==l?"parents":"closest"](":hidden").length},tabbable:function(k){var j=c.attr(k,"tabindex");return(isNaN(j)||j>=0)&&c(k).is(":focusable")}});function g(m,n,o,l){function k(q){var p=c[m][n][q]||[];return(typeof p=="string"?p.split(/,?\s+/):p)}var j=k("getter");if(l.length==1&&typeof l[0]=="string"){j=j.concat(k("getterSetter"))}return(c.inArray(o,j)!=-1)}c.widget=function(k,j){var l=k.split(".")[0];k=k.split(".")[1];c.fn[k]=function(p){var n=(typeof p=="string"),o=Array.prototype.slice.call(arguments,1);if(n&&p.substring(0,1)=="_"){return this}if(n&&g(l,k,p,o)){var m=c.data(this[0],k);return(m?m[p].apply(m,o):undefined)}return this.each(function(){var q=c.data(this,k);(!q&&!n&&c.data(this,k,new c[l][k](this,p))._init());(q&&n&&c.isFunction(q[p])&&q[p].apply(q,o))})};c[l]=c[l]||{};c[l][k]=function(o,n){var m=this;this.namespace=l;this.widgetName=k;this.widgetEventPrefix=c[l][k].eventPrefix||k;this.widgetBaseClass=l+"-"+k;this.options=c.extend({},c.widget.defaults,c[l][k].defaults,c.metadata&&c.metadata.get(o)[k],n);this.element=c(o).bind("setData."+k,function(q,p,r){if(q.target==o){return m._setData(p,r)}}).bind("getData."+k,function(q,p){if(q.target==o){return m._getData(p)}}).bind("remove",function(){return m.destroy()})};c[l][k].prototype=c.extend({},c.widget.prototype,j);c[l][k].getterSetter="option"};c.widget.prototype={_init:function(){},destroy:function(){this.element.removeData(this.widgetName).removeClass(this.widgetBaseClass+"-disabled "+this.namespace+"-state-disabled").removeAttr("aria-disabled")},option:function(l,m){var k=l,j=this;if(typeof l=="string"){if(m===undefined){return this._getData(l)}k={};k[l]=m}c.each(k,function(n,o){j._setData(n,o)})},_getData:function(j){return this.options[j]},_setData:function(j,k){this.options[j]=k;if(j=="disabled"){this.element[k?"addClass":"removeClass"](this.widgetBaseClass+"-disabled "+this.namespace+"-state-disabled").attr("aria-disabled",k)}},enable:function(){this._setData("disabled",false)},disable:function(){this._setData("disabled",true)},_trigger:function(l,m,n){var p=this.options[l],j=(l==this.widgetEventPrefix?l:this.widgetEventPrefix+l);m=c.Event(m);m.type=j;if(m.originalEvent){for(var k=c.event.props.length,o;k;){o=c.event.props[--k];m[o]=m.originalEvent[o]}}this.element.trigger(m,n);return !(c.isFunction(p)&&p.call(this.element[0],m,n)===false||m.isDefaultPrevented())}};c.widget.defaults={disabled:false};c.ui.mouse={_mouseInit:function(){var j=this;this.element.bind("mousedown."+this.widgetName,function(k){return j._mouseDown(k)}).bind("click."+this.widgetName,function(k){if(j._preventClickEvent){j._preventClickEvent=false;k.stopImmediatePropagation();return false}});if(c.browser.msie){this._mouseUnselectable=this.element.attr("unselectable");this.element.attr("unselectable","on")}this.started=false},_mouseDestroy:function(){this.element.unbind("."+this.widgetName);(c.browser.msie&&this.element.attr("unselectable",this._mouseUnselectable))},_mouseDown:function(l){l.originalEvent=l.originalEvent||{};if(l.originalEvent.mouseHandled){return}(this._mouseStarted&&this._mouseUp(l));this._mouseDownEvent=l;var k=this,m=(l.which==1),j=(typeof this.options.cancel=="string"?c(l.target).parents().add(l.target).filter(this.options.cancel).length:false);if(!m||j||!this._mouseCapture(l)){return true}this.mouseDelayMet=!this.options.delay;if(!this.mouseDelayMet){this._mouseDelayTimer=setTimeout(function(){k.mouseDelayMet=true},this.options.delay)}if(this._mouseDistanceMet(l)&&this._mouseDelayMet(l)){this._mouseStarted=(this._mouseStart(l)!==false);if(!this._mouseStarted){l.preventDefault();return true}}this._mouseMoveDelegate=function(n){return k._mouseMove(n)};this._mouseUpDelegate=function(n){return k._mouseUp(n)};c(document).bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate);(c.browser.safari||l.preventDefault());l.originalEvent.mouseHandled=true;return true},_mouseMove:function(j){if(c.browser.msie&&!j.button){return this._mouseUp(j)}if(this._mouseStarted){this._mouseDrag(j);return j.preventDefault()}if(this._mouseDistanceMet(j)&&this._mouseDelayMet(j)){this._mouseStarted=(this._mouseStart(this._mouseDownEvent,j)!==false);(this._mouseStarted?this._mouseDrag(j):this._mouseUp(j))}return !this._mouseStarted},_mouseUp:function(j){c(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate);if(this._mouseStarted){this._mouseStarted=false;this._preventClickEvent=(j.target==this._mouseDownEvent.target);this._mouseStop(j)}return false},_mouseDistanceMet:function(j){return(Math.max(Math.abs(this._mouseDownEvent.pageX-j.pageX),Math.abs(this._mouseDownEvent.pageY-j.pageY))>=this.options.distance)},_mouseDelayMet:function(j){return this.mouseDelayMet},_mouseStart:function(j){},_mouseDrag:function(j){},_mouseStop:function(j){},_mouseCapture:function(j){return true}};c.ui.mouse.defaults={cancel:null,distance:1,delay:0}})(jQuery);
if(typeof TurnToTrack=="undefined"){TurnToTrack={};TurnToTrack.version="0.5";var TurnTojQuery=jQuery.noConflict(true);TurnToTrack.init=function(){r();if(!turnToConfig){turnToConfig={}}TurnToTrack.partnerProtocol=document.location.protocol;TurnToTrack.xhrIntervals=[];TurnToTrack.versionSuffix=turnToConfig.devMode?"":"-"+TurnToTrack.version;turnToSettings={};turnToSettings.siteKey=turnToConfig.siteKey;turnToSettings.host=turnToConfig.host||"www.turnto.com";var m=(new Date()).getTime();TurnToTrack.baseEventUrl=z("/webEvent/s.gif?siteKey="+turnToConfig.siteKey+"&v="+encodeURIComponent(TurnToTrack.version)+"&et=visit&e=500&cid="+m);TurnToTrack.onload=G;TurnToTrack.visitTeaserShown=a;TurnToTrack.visitTeaserClick=x;TurnToTrack.visitEmbeddedReg=j;TurnToTrack.disableGlobal=y;TurnToTrack.makeVisitorPartsQS=q;TurnToTrack.newUserRegistration=i;TurnToTrack.isSiteVisitTrackingBlock=C;TurnToTrack.blockSiteVisitTracking=w;TurnToTrack.unblockSiteVisitTracking=u;TurnToTrack.getSiteVisitId=e;var p="TURNTO_VISITOR_COOKIE";var A="TURNTO_VISITOR_SESSION";var v=0;var H=1;var B=2;var l=3;var f=4;var F=5;var o=6;var h="TURNTO_SITEVISIT_BLOCKED";var I=30;var n=true;TurnToTrack.getVisitorParts=c;function G(){if(n){var K=c();var J=(K==null);K=k(K);if(J){d(K)}E(K)}}function y(){n=false}function k(J){if(D(A)==null){t(A,1,-1);J=null}if(J==null){J=new Array(4);J[v]=g();J[H]=0;J[B]=0;J[l]=0;J[f]=0;J[F]="";J[o]=""}return J}function x(){var K=c();K=k(K);if(K[B]==0){K[B]=1;var J=document.location+"";var L=J.indexOf("#");if(L>0){J=J.substring(0,L)}K[F]=J;d(K)}E(K)}function a(L){var K=c();K=k(K);var J=null;if(K[H]==0){K[H]=1;K[f]=D("turnto_teaser_variant");if(L){d(K)}J=K}E(K);return J}function i(K){var L=c();L=k(L);var J=null;L[o]=K;d(L);J=L;E(L);return J}function j(){var J=c();J=k(J);if(J[l]==0){J[l]=1;d(J)}E(J)}function e(){var J=k(c());return J[v]}function E(J){t(p,J[v]+","+J[H]+","+J[B]+","+J[l]+","+J[f]+","+J[F]+","+J[o],I*60)}function d(J){if(C()){return}b("&vid="+J[v]+"&vshown="+J[H]+"&vclick="+J[B]+"&vembeddedReg="+J[l]+"&variId="+J[f]+"&vclurl="+J[F]+"&vregId="+J[o])}function q(J){if(J==undefined){return""}if(C()){return""}return"&vid="+J[v]+"&vshown="+J[H]+"&vclick="+J[B]+"&vembeddedReg="+J[l]+"&variId="+J[f]+"&vclurl="+J[F]+"&vregId="+J[o]}function c(){var J=D(p);if(J==null){return null}return J.split(",")}function z(L,M){var J=turnToSettings.urlPref||"";var K=(M)?"http:":TurnToTrack.partnerProtocol;return K+"//"+turnToSettings.host+J+L}var s=0;function b(K){var M=document.createElement("img");M.width="1";M.width="1";M.border="0";M.style.position="absolute";M.style.left="-100px";var L=s++;M.id="TTTrackEvent"+L;document.body.appendChild(M);console.debug(TurnToTrack.baseEventUrl+"track with params "+K);var J=document.getElementById("TTTrackEvent"+L);J.src=TurnToTrack.baseEventUrl+K}function g(){var M="0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";var N=15;var L="";for(var K=0;K<N;K++){var J=Math.floor(Math.random()*M.length);L+=M.substring(J,J+1)}return L}function C(){var J=D(h);if(J==null){return false}if(J==1){return true}return false}function w(){t(h,1,946080000)}function u(){t(h)}function t(J,M,L){var K=J+"=";if(typeof M!="undefined"){K+=M+"; path=/";if(typeof L!="undefined"){if(L!=-1){var N=new Date();N.setTime(N.getTime()+L*1000);K+="; Expires="+N.toUTCString()}}}else{K+="; path=/; Expires="+new Date(0).toUTCString()}console.debug("setting cookie: "+J+" to: "+K);document.cookie=K}function D(J){var N=null;var L=document.cookie;var M=L.indexOf(J+"=");if(M>=0){M+=J.length+1;var K=L.indexOf(";",M);if(K==-1){K=L.length}N=L.substring(M,K)}return N}function r(){var L=(typeof window.console!="undefined");var K=(L&&("dirxml" in window.console));if(!K){var M=["log","debug","info","warn","error","assert","dir","dirxml","group","groupEnd","time","timeEnd","count","trace","profile","profileEnd"];window.console={};for(var J=0;J<M.length;++J){window.console[M[J]]=function(){}}}}};TurnToTrack.init();TurnTojQuery(document).ready(TurnToTrack.onload)};
if(typeof TurnToLoaded=="undefined"){TurnToLoaded=true;if(typeof TurnTo=="undefined"){TurnTo={}}TurnTo.version="0.5";TurnTo.init=function(){TurnToTrack.disableGlobal();checkFirebug();if(!turnToConfig){turnToConfig={}}TurnTo.partnerProtocol=document.location.protocol;TurnTo.xhrIntervals=[];TurnTo.versionSuffix=turnToConfig.devMode?"":"-"+TurnTo.version;turnToSettings={};customCopy={};turnToSettings.siteKey=turnToConfig.siteKey;turnToSettings.useSetup=turnToConfig.useSetup;turnToSettings.host=turnToConfig.host||"www.turnto.com";turnToSettings.staticHost=turnToConfig.staticHost||"static.www.turnto.com";turnToSettings.traTopOffset=turnToConfig.traTopOffset||"6";customCopy.friendsHeader=turnToConfig.friendsHeader||"Ask other customers about the things they bought here";customCopy.questionHeader=turnToConfig.questionHeader||"Ask customers who bought this.";customCopy.pswHeader=turnToConfig.pswHeader||"Please help make our store a friendly place to shop. Let your friends and other shoppers see what you chose.";customCopy.friendFbHeader1=turnToConfig.friendFbHeader1||getStaticUrl("/tra2/images/fbheader1.png");customCopy.friendFbHeader2=turnToConfig.friendFbHeader2||getStaticUrl("/tra2/images/fbheaderopened.png");TurnTo.customCopy=customCopy;turnToSettings.imageStoreBase=turnToConfig.imageStoreBase||"wac.edgecastcdn.net/001A39/prod";turnToSettings.iTeaserFunc=turnToConfig.iTeaserFunc||defaultItemTeaserDisplay;turnToSettings.fTeaserFunc=turnToConfig.fTeaserFunc||doNothing;turnToSettings.fTeaserFunc=new turnToSettings.fTeaserFunc();turnToSettings.siteType=turnToConfig.siteType||1;if(turnToSettings.siteType==1){turnToSettings.verb="bought";turnToSettings.text1="See what your neighbors bought here.";turnToSettings.text2="Which of your friends shop here? Find out! "}turnToSettings.timeout=turnToConfig.timeout||30000;turnToSettings.useAbsoluteFteaser=turnToConfig.useAbsoluteFteaser||false;turnToSettings.regOnly=turnToConfig.regOnly||false;turnToSettings.fbOpen=turnToConfig.fbOpen||false;turnToSettings.allowEscKey=turnToConfig.allowEscKey||false;turnToSettings.useFlashXHR=turnToConfig.useFlashXHR||(typeof TurnToUseFlashXhr!="undefined");turnToSettings.showGteaser=turnToConfig.showGteaser||false;var showbubble=!(turnToSettings.fbOpen||turnToSettings.regOnly);turnToSettings.gteaserHTML=turnToConfig.gteaserHTML||false;turnToSettings.useFirstPartyCookies=turnToSettings.useFlashXHR;turnToSettings.urlPref=turnToConfig.urlPref||"";turnToSettings.eventPartnerData=turnToConfig.eventPartnerData||"";TurnTo.friendsUrl="/myConnections";TurnTo.registerUrl=getUrl("/trackin/trajn?siteKey="+turnToConfig.siteKey+"&v="+encodeURIComponent(TurnTo.version),true);if(!turnToConfig.ttClientUrl){turnToSettings.ttClientUrl=TurnTo.partnerProtocol+"//"+document.location.host+"/tra/tt_client.html"}else{turnToSettings.ttClientUrl=TurnTo.partnerProtocol+"//"+document.location.host+turnToConfig.ttClientUrl}turnToSettings.updateCookieName=turnToConfig.updateCookieName||"TTupd";turnToSettings.authCookieName=turnToConfig.authCookieName||"TTauth";turnToSettings.sessionCookieName=turnToConfig.sessionCookieName||"TTsess";turnToSettings.thirdBlockedCookieName=turnToConfig.thirdBlockedCookieName||"TT3bl";turnToSettings.fbconnectCookieName="TTfbc";turnToSettings.zipOnlyCookie="TTZip";turnToSettings.dialogPosition=turnToConfig.dialogPosition||["center",100];turnToSettings.popupOptions=turnToConfig.popupOptions||"width=760,height=400,location=yes";turnToSettings.popupName=turnToConfig.popupName||"TTlogin";turnToSettings.animationMs=turnToConfig.animationMs||200;turnToSettings.overlayOpacity=turnToConfig.overlayOpacity||"0.2";turnToSettings.matchesHeight=turnToConfig.matchesHeight||450;turnToSettings.useSSL=(typeof turnToConfig.useSSL=="undefined")||turnToConfig.useSSL;turnToSettings.floatingTeaserStyle=turnToConfig.floatingTeaserStyle||0;var cid=(new Date()).getTime();TurnTo.baseEventUrl=getUrl("/webEvent/s.gif?siteKey="+turnToConfig.siteKey+"&v="+encodeURIComponent(TurnTo.version)+"&cid="+cid);TurnTo.eventTeaserShown=0;TurnTo.eventTraShownNotLoggedIn=10;TurnTo.eventTraShownLoggedIn=11;TurnTo.eventFriendsOpen=12;TurnTo.eventFOFsOpen=13;TurnTo.eventZipsOpen=14;TurnTo.eventQaItemteaserClick=124;TurnTo.eventQaQSubmit=125;TurnTo.eventQaQPost=126;TurnTo.matchTypeFriend=1;TurnTo.matchTypeFoaf=2;TurnTo.matchTypeZip=3;TurnTo.facebookTargetId=12;TurnTo.twitterTargetId=13;TurnTo.ppLastFocusIndex=-1;TurnTo.regSourceUnknown=0;TurnTo.regSourceTra=1;TurnTo.regSourceEmbedded=2;TurnTo.regSourceOrderConfPage=3;TurnTo.regSourcePswPte=4;TurnTo.regSourcePswForwardInvite=5;TurnTo.regSourceAskQuestion=6;TurnTo.regSourceAskAnswer=7;TurnTo.staff=0;TurnTo.siteCustomerService=1;TurnTo.siteModerator=2;TurnTo.siteManager=3;TurnTo.getUrl=getUrl;TurnTo.getStaticUrl=getStaticUrl;TurnTo.iframeProxyUrl=getUrl("/tra2/xip_server.html");TurnTo.staticIframeProxyUrl=getStaticUrl("/tra2/xip_server.html");TurnTo.secureIframeProxyUrl=(turnToSettings.useSSL?TurnTo.iframeProxyUrl.replace(/http:/,"https:"):TurnTo.iframeProxyUrl);TurnTo.stylesheetUrl=getStaticUrl((turnToConfig.devMode)?"/tra2/src/tra-src.css":"/tra2/tra.css");TurnTo.logoHTML='<img class="TTlogo" src="'+getStaticUrl("/images/tt_logo_16.png")+'" width="16" height="16" />';TurnTo.traJq=traJq;TurnTo.onload=onload;TurnTo.setCookieIframe=null;TurnTo.setCookie=setCookie;TurnTo.mockDojo=mockDojo;TurnTo.unmockDojo=unmockDojo;TurnTo.sendTheFeed=false;TurnTo.registerFbConnectUser=registerFbConnectUser;TurnTo.initItemsToShow=3;TurnTo.initZipItemsToShow=5;TurnTo.inputFocus=inputFocus;TurnTo.inputLoseFocus=inputLoseFocus;TurnTo.inputUserImage=inputUserImage;TurnTo.getDateString=getDateString;TurnTo.fbButtonSrc=getStaticUrl("/tra2/images/use-facebook.png");TurnTo.fbButtonSrc2=getStaticUrl("/tra2/images/use-facebook.png");TurnTo.getUrl=getUrl;TurnTo.getItems4User=getItems4User;TurnTo.findUserRaveTarget=findUserRaveTarget;TurnTo.getProductImage=getProductImage;TurnTo.getUserPhoto=getUserPhoto;TurnTo.getFacebookUserPhoto=getFacebookUserPhoto;TurnTo.connectWithTwitter=connectWithTwitter;TurnTo.userStatusDisplayLI=userStatusDisplayLI;TurnTo.makePrimaryIdent=makePrimaryIdent;TurnTo.removeIdent=removeIdent;TurnTo.resendEmailConfirmation=resendEmailConfirmation;TurnTo.inputUserImageForUserSetting=inputUserImageForUserSetting;TurnTo.startPostPurchaseFlow=startPostPurchaseFlow;TurnTo.rolloverHideLo=rolloverHideLo;TurnTo.rolloverShowLo=rolloverShowLo;TurnTo.rolloverHideLi=rolloverHideLi;TurnTo.rolloverShowLi=rolloverShowLi;TurnTo.showInlineAnswerInput=showInlineAnswerInput;TurnTo.hideInlineAnswerInput=hideInlineAnswerInput;TurnTo.insertItemTeaser=insertItemTeaser;TurnTo.TT2questionTextFocus=TT2questionTextFocus;TurnTo.TT2questionTextLoseFocus=TT2questionTextLoseFocus;TurnTo.isSiteManager=isSiteManager;TurnTo.isSiteModerator=isSiteModerator;TurnTo.setup=setup;TurnTo.insertGlobalTeaser=insertGlobalTeaser;TurnTo.unsupport=unsupport;TurnTo.floatTeaserSet=false;TEASER_COOKIE_NAME="TURNTO_TEASER_COOKIE";TEASER_SHOWN_COOKIE_NAME="TURNTO_TEASER_SHOWN";LOGGED_IN_COOKIE_NAME="TURNTO_LOGGEDIN";ROLLOVER_TOOLTIP_COOKIE_NAME="TURNTO_ROLLOVER_TOOLTIP";TurnTo.traDataLoaded=false;TurnTo.fteaserCount=0;TurnTo.zip=null;TurnTo.beforeReady=beforeReady;TurnTo.doIteaser=false;var traWindow;var traSubWindow;var traBubbleWindow;var flashCheck;var importSrc;var popular;var toolTipQ;var totalLiMessages=0;var IEversion=0;var IE7or8=false;var instantTeaserClicked=false;var turntoflow;var preLoadedSite=(TurnTo.matchData&&TurnTo.matchData.site);if(TurnTojQuery.browser.msie&&TurnTojQuery.browser.version.substr(0,1)<8){turnToSettings.useSetup=false}TurnTo.params=getParameters();if((TurnTo.params.length>0&&window.location.href.indexOf("#turntodone")==-1)&&(TurnTo.params.length>0&&TurnTo.params.turntoflow)){turntoflow=TurnTo.params.turntoflow}function traJq(str){return TurnTojQuery(str,TurnTo.jQcontext)}function setup(){if(!turnToSettings.useSetup){return}if(turntoflow){instantTeaSpinScreen()}onload()}function beforeReady(){if(TurnTojQuery.browser.msie){return}if(TurnTojQuery("body")[0]&&preLoadedSite){if(turntoflow){instantTeaSpinScreen()}else{checkDoIteaser();displayTeasers(true)}}}function checkDoIteaser(){TurnTo.doIteaser=(TurnTojQuery(".TurnToItemTeaser")[0]!=undefined)&&(typeof TurnToItemSku!="undefined")}var insertItemTeaserDone=false;function insertItemTeaser(){if(!insertItemTeaserDone){TurnTo.doIteaser=true;if(typeof TurnToItemData!="undefined"){loadItemTeaser(TurnToItemData,null)}insertItemTeaserDone=true}}function insertGlobalTeaser(){var gteasers=TurnTojQuery(".TTgteaser");gteasers.each(function(i){var ttLogo='<a class="TTloginLink">'+TurnTo.logoHTML+"</a>";var teaserHTML=turnToSettings.gteaserHTML||((TurnTo.matchData.site.gteaser)?eval(TurnTo.matchData.site.gteaser):ttLogo+' <a class="TTloginLink">See what your friends are buying here</a>');this.innerHTML=teaserHTML});gteasers.addClass("TTcustomEmebedTeaserLO")}function displayTeasers(arg1){console.debug("displayTeasers");var inBeforeReady=arg1||false;if(TurnTo.doIteaser){if(inBeforeReady){insertUI();insertItemTeaser()}else{if(!turnToSettings.useSetup){if(turnToSettings.siteKey=="m1zoXMnglUJjMVWsite"){displayItemTeaser()}else{insertItemTeaser()}}}}turnToSettings.floatingTeaserStyle=(turnToSettings.floatingTeaserStyle>0)?turnToSettings.floatingTeaserStyle:TurnTo.matchData.site.ftid;if(TurnTo.matchData.site.floatTeaser){if(!TurnTo.floatTeaserSet){TurnTo.floatTeaserSet=createFloatingTeaser(TurnTojQuery("body")[0])}}if(TurnTo.floatTeaserSet&&turnToSettings.floatingTeaserStyle!=1){floatTeaserLoggedOut()}turnToSettings.fTeaserFunc.display();if(!turnToSettings.useSetup){var doGteaser=(turnToSettings.showGteaser)||(!TurnTo.matchData.site.floatTeaser);if(doGteaser){insertGlobalTeaser()}}activateTeaserLinks(inBeforeReady)}function activateTeaserLinks(arg1){var inBeforeReady=arg1||false;if(inBeforeReady){var loginLinks=TurnTojQuery(".TTloginLink");loginLinks.each(function(i){this.href="javascript:void(0)";this.onclick=function(e){console.debug(instantTeaserClicked);instantTeaserClicked=true;instantTeaSpinScreen()}});return}TurnTojQuery(".TTshowMatchesLink, .TTloginLink").each(function(i){this.href="javascript:void(0)";this.onclick=function(e){setTeaserCookie();TurnTo.loggedIn?showMatches(e):showLogin()}});turnToSettings.fTeaserFunc.afterOpenLinks();if(instantTeaserClicked&&!closedFromSpinScreen){setTeaserCookie();insertUI();toggleMainWindow("none");TurnTo.loggedIn?showMatches():showLogin()}}function insertHead(){var headID=document.getElementsByTagName("head")[0];var cssNode=document.createElement("link");cssNode.type="text/css";cssNode.rel="stylesheet";cssNode.href=TurnTo.stylesheetUrl;cssNode.media="screen";headID.appendChild(cssNode);if(turnToSettings.siteKey=="eQ4apTWPd21WLuksite"||turnToSettings.siteKey=="uMUh5vkap5LztCnsite"){var siteStylesheetUrl=getStaticUrl("/traServer2/siteCss/"+turnToSettings.siteKey+"/turntosite.css");var cssNode2=document.createElement("link");cssNode2.type="text/css";cssNode2.rel="stylesheet";cssNode2.href=siteStylesheetUrl;cssNode2.media="screen";headID.appendChild(cssNode2)}}var turntoRollo1BoxElement;var showRolloBox=false;var insertUiComplete=false;function insertUI(){if(insertUiComplete){return}if(hasSpinScreen){TurnTojQuery("#TTturntospinner").replaceWith(TurnToHTML.mainscreen)}else{if(turnToSettings.useSetup){TurnTojQuery("#TurnToSetup").append("<div id='TTturntoTra'></div>")}else{var traDiv=document.createElement("div");traDiv.id="TTturntoTra";document.body.appendChild(traDiv)}TurnTojQuery("#TTturntoTra").replaceWith(TurnToHTML.mainscreen)}insertUiParts();resize();traWindow=TurnTojQuery("#TTtraWindow")[0];traSubWindow=TurnTojQuery("#TTtraSubWindow")[0];traBubbleWindow=TurnTojQuery("#TTtraBubbleWindow")[0];traWindow.overlay=TurnTojQuery("#TTtraBackOverlay")[0].style;traSubWindow.overlay=TurnTojQuery("#TTtraInsideOverlay")[0].style;TurnTojQuery("#TTtraWindowClose")[0].onclick=toggleMainWindow;TurnTojQuery("#TTSubWindowClose")[0].onclick=function(e){toggleSubWindow("none")};TurnTojQuery("#TTBubbleWindowClose")[0].onclick=toggleBubbleWindow;TurnTo.jQcontext=TurnTojQuery("#TTtraWindow");toolTipQ=TurnTojQuery("#TTtoolTip");toolTipQ[0].onmouseout=function(e){toolTipQ[0].style.display="none"};insertUiComplete=true}function insertUiParts(){if(TurnTo.loggedIn){insertScreen("#TTtraUserStateMain",TurnToHTML.TTtraLoggedInMain);TurnTojQuery("#TTtraWindowClose")[0].onclick=toggleMainWindow;showZipForm()}else{insertScreen("#TTtraUserStateMain",TurnToHTML.TTtraLoggedOutMain);if(TurnTo.matchData.site.ask){traJq("#TT2loheadercopy").html(TurnTo.customCopy.friendsHeader)}TurnTojQuery("#TTtraWindowClose")[0].onclick=toggleMainWindow;var sl=traJq("#TTsignUpLink3")[0];sl.onclick=function(e){showUserLogin(e)};minimizeFbHeader();if(TurnTo.quirksMode){traJq("#TT2loFacebookLink").hide();traJq("#TT2loConnectLink").hide()}}if(turnToConfig.removeTurnToLogo){traJq("#TTtraFooterLogo").hide()}}function unsupport(){if(navigator.userAgent.indexOf("iPad")>=0){return true}if(TurnTojQuery.browser.msie&&TurnTojQuery.browser.version.substr(0,1)<7){return true}if(turnToSettings.useFlashXHR){if(FlashDetect.major<9){return true}else{if(FlashDetect.major==9&&FlashDetect.minor==0&&FlashDetect.revision<124){return true}}}return false}function onload(){if(TurnTojQuery.browser.msie&&TurnTojQuery.browser.version.substr(0,1)<7){return}IEversion=TurnTojQuery.browser.version.substr(0,1);IE7or8=IEversion==7||IEversion==8;if(TurnTojQuery.browser.msie){TurnTo.quirksMode=document.compatMode!="CSS1Compat"}checkDoIteaser();if(turnToSettings.useFlashXHR){if(FlashDetect.major<9){return}else{if(FlashDetect.major==9&&FlashDetect.minor==0&&FlashDetect.revision<124){return}else{finishOnload()}}}else{finishOnload()}}function finishOnload(){insertHead();checkThirdPartyCookies();var zipCookie=getCookie(turnToSettings.zipOnlyCookie);TurnTo.zip=zipCookie;var visitorParts=TurnToTrack.visitTeaserShown(turnToSettings.useStaticTeaser||(preLoadedSite&&!getCookie(LOGGED_IN_COOKIE_NAME)));setCookie(TEASER_SHOWN_COOKIE_NAME,new Date().getTime(),2592000);if(turntoflow&&preLoadedSite){insertUI();traHookCheck()}if(preLoadedSite){setTimeout(function(){displayTeasers();if(!turntoflow){insertUI()}},0)}else{setTimeout(function(){getTeaserData(function(data,ioArgs){getTeaserDataLoaded(data,ioArgs);insertUI();traHookCheck();displayTeasers()},visitorParts)},0)}if(preLoadedSite&&getCookie(LOGGED_IN_COOKIE_NAME)){getTeaserData(function(data,ioArgs){getTeaserDataLoaded(data,ioArgs)},visitorParts)}checkUnloadReg();if(TurnTo.params.turntocheck=="h"){window.location=getUrl("/userHome/partnerMonitor?done=true&siteId=")+TurnTo.params.turntonexturl+"&status=done&useSetup="+turnToSettings.useSetup}}var traHookCheckDone=false;var hideAskBackBtnOnce=false;function traHookCheck(){if(traHookCheckDone){return}if(!turntoflow){return}var addDone=false;if((TurnTo.params.turntoflow=="fmatch"||TurnTo.params.turntoflow=="ppemail")&&TurnTo.params.turntoeml){TurnTo.pteFlowStarted=true;getItems4Pte(TurnTo.params.turntoeml,TurnTo.params.turntotri,TurnTo.params.turntoiuid,showOrderConfPurchases);addDone=true}else{if(TurnTo.params.turntoflow=="answer"&&TurnTo.params.turntoqid&&TurnTo.params.turntosuid){hideAskBackBtnOnce=true;if(TurnTo.params.turntoqid&&TurnTo.params.turntosuid){if(TurnTo.params.turntoqid.indexOf(".")>0){TurnTo.params.turntoqid=TurnTo.params.turntoqid.replace(/[.]/g,"=")}if(TurnTo.params.turntosuid.indexOf(".")>0){TurnTo.params.turntosuid=TurnTo.params.turntosuid.replace(/[.]/g,"=")}showAnswerWidget(TurnTo.params.turntoqid,TurnTo.params.turntosuid)}addDone=true}else{if((TurnTo.params.turntoflow=="answerview"||TurnTo.params.turntoflow=="replyview")&&TurnTo.params.turntocatitmid){hideAskBackBtnOnce=true;toggleMainWindow("none");showQuestionWidget({catItmId:TurnTo.params.turntocatitmid,iid:"",uid:TurnTo.params.turntouid,hideQuestionInput:true});addDone=true}else{if(TurnTo.params.turntoflow=="answerreply"&&TurnTo.params.turntorid&&TurnTo.params.turntoaid&&TurnTo.params.turntouid){hideAskBackBtnOnce=true;showReplyWidget({rid:TurnTo.params.turntorid,aid:TurnTo.params.turntoaid,uid:TurnTo.params.turntouid});addDone=true}else{if(TurnTo.params.newReg=="y"){if((TurnTo.params.turntoflow=="question"||TurnTo.params.turntoflow=="answer")&&TurnTo.params.turntoflowid){toggleMainWindow("none");showQuestionWidget({catItmId:TurnTo.params.turntoflowid,iid:""})}else{showMatches()}addDone=true}else{if(TurnTo.params.turntoflow=="sitevisitsadmin"){toggleMainWindow("none");showSiteVisitAdminControl();addDone=true}else{if(TurnTo.params.turntoflow=="sitevisitsadminsilent"){TurnToTrack.blockSiteVisitTracking();window.location=decodeURIComponent(TurnTo.params.turntonexturl)}else{if(TurnTo.params.turntoflow=="thankanswer"&&TurnTo.params.turntoaid&&TurnTo.params.turntouid){hideAskBackBtnOnce=true;showThankAnswerWidget({aid:TurnTo.params.turntoaid,uid:TurnTo.params.turntouid});addDone=true}}}}}}}}if(addDone){window.location=window.location+"#turntodone"}traHookCheckDone=true}function minimizeFbHeader(e){var header=traJq("#TT2fbHeader");traJq("#TT2loCloseFbHeaderLink").unbind();traJq("#TT2loFacebookLink").unbind();traJq("#TT2loConnectLink").unbind();header.html("");header.css("cursor","pointer");header.css("height","33px");header.css("background","url("+customCopy.friendFbHeader1+") no-repeat scroll 0pt transparent");if(e){e.stopPropagation()}else{if(window.event){window.event.cancelBubble=true}}header.click(function(e){openFbHeader(e)})}function openFbHeader(){var header=traJq("#TT2fbHeader");header.unbind();header.css("background","url("+customCopy.friendFbHeader2+") no-repeat scroll 0pt transparent");header.css("cursor","auto");header.css("height","216px");header.append("<a id='TT2loCloseFbHeaderLink' href='javascript:void(0)'/>");header.append("<a id='TT2loFacebookLink' href='javascript:void(0)'/>");header.append("<a id='TT2loConnectLink' href='javascript:void(0)' style='font-size:10px;font-weight:bold'>Connect without Facebook</a>");traJq("#TT2loCloseFbHeaderLink").click(function(e){minimizeFbHeader(e)});traJq("#TT2loFacebookLink").click(function(e){connectWithFacebook();minimizeFbHeader(e)});traJq("#TT2loConnectLink").click(function(e){displayImportSourceSelectScreen({hideFacebook:true});minimizeFbHeader(e)})}function UIloggedIn(){totalLiMessages=0;numQuestionWidgetMsg=0;insertScreen("#TTtraDialogFirstName",TurnToHTML.titlebarLoggedIn,null);TurnTojQuery("#TTtraWindowClose")[0].onclick=toggleMainWindow;traJq("#TTaddFriendsHdrLink").show()[0].onclick=function(e){getImportHistory(displayImportSourceSelectScreen)};if(TurnTo.matchData.site.ask){traJq("#TT2myAskLink").show()[0].onclick=function(e){showMyAsk()};traJq("#TT2myAskLinkSeparator").show();var marginLeft=parseInt(traJq(".TT2verticalSeparator").css("margin-left").replace("px",""))-8;var marginRight=parseInt(traJq(".TT2verticalSeparator").css("margin-right").replace("px",""))-8;traJq(".TT2verticalSeparator").css({marginLeft:marginLeft+"px",marginRight:marginRight+"px"})}var mpLink=traJq("#TT2ManagePurchasesLink").show();if(TurnTo.matchData.counts.userItems>0){mpLink[0].onclick=function(e){getItems4User(displayPastPurchases)};mpLink.html("My Purchases ("+TurnTo.matchData.counts.userItems+")")}else{mpLink[0].onclick=function(e){getItems4User(displayPastPurchases)};mpLink.html("My Purchases")}var settingLink=traJq("#TT2userSettingLink").show();settingLink.click(function(){displayUserSetting()});traJq(".TThi")[0].innerHTML='<img style="display:inline;height:16px;float:left;" src="'+TurnTo.getUserPhoto(TurnTo.matchData.user.id,TurnTo.matchData.user.pid)+'" height="16px"/><b style="margin-left:10px;float:left">Welcome, '+TurnTo.matchData.user.firstName+"</b>";traJq(".TTfirstName")[0].innerHTML=TurnTo.matchData.user.firstName;var fcount=TurnTo.matchData.counts.sharingMatches+TurnTo.matchData.counts.askFirstMatches;var ffoafcountPlus=fcount+(TurnTo.matchData.counts.foafCount*1);if(ffoafcountPlus>9){ffoafcountPlus=10}if(TurnTo.matchData.newfbuser){addLIMessage({screen:"newfbuser",count:TurnTo.matchData.newfbuser});traJq("#TT2newfbuserclickhere").click(displayImportSourceSelectScreen);traJq("#TTnewfbuserdismiss").click(dismissFbNewUserMess)}for(var uIdentsIndx=0;uIdentsIndx<TurnTo.matchData.user.uIdents.length;uIdentsIndx++){if(TurnTo.matchData.user.uIdents[uIdentsIndx].type==1){if(!TurnTo.matchData.user.uIdents[uIdentsIndx].confirmed){var id=TurnTo.matchData.user.uIdents[uIdentsIndx].id;var email=TurnTo.matchData.user.uIdents[uIdentsIndx].ident;addLIMessage({screen:"unconfirmedEmailMessage",email:email,id:id});traJq("#TT2resendEmailConfirmation"+id)[0].email=email;traJq("#TT2resendEmailConfirmation"+id)[0].onclick=function(e){resendEmailConfirmation(this.email)};TurnTo.matchData.user.unconfirmedEmail=true}}}if(!TurnTo.matchData.user.hasPhoto){addLIMessage({screen:"noUserPhotoMessage"});traJq("#TT2AddUserPhoto").click(function(){displayUserSetting(function(){insertScreen("#TTSubScreen",TurnToHTML.userSetting,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl});toggleSubWindow("block");insertScreen("#contactInfo",TurnToHTML.userSettingEmailAddresses,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl});var traHeight=TurnTojQuery("#TTmainContent").height();traJq("#TT2userSetting").css({maxHeight:traHeight-50+"px"});if(TurnTojQuery.browser.msie){traJq("#TT2userSetting").css({height:"auto"})}setupUserSettingAction();showPhotoEditor()})})}if(!userHasEmail()){addLIMessage({screen:"addemailmessage"});traJq("#TT2addemailclickhere").click(function(){displayUserSetting();dismissMessage("#TT2addemailMessage")})}updateLogoutLinks(logOut);resize()}function showMyAsk(){xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getAskData4User")+"&cid="+cid,load:function(data,ioArgs){insertScreen("#TTSubScreen",TurnToHTML.myAsk,{questions:data.questions});if(data.questions.length==0){traJq("#TT2myAsk").html("<h3 style='font-weight:bold;font-size:18px;margin-bottom:15px;'>You have not asked or answered any questions</h3>")}traJq(".TT2shareQuestionBtn").click(function(){var qid=this.id.substring(this.id.lastIndexOf("-")+1);showQuestionSharePanel({id:qid,questionText:traJq("#TT2rawQuestionText-"+qid).val(),catImgId:traJq("#TT2catImgId-"+qid).val(),questionSubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})});traJq(".TT2shareAnswerBtn").click(function(){var aid=this.id.substring(this.id.lastIndexOf("-")+1);showAnswerSharePanel({id:aid,answerText:traJq("#TT2rawAnswerText-"+aid).val(),questionText:traJq("#TT2rawQuestionText-"+traJq("TT2qid4answer-"+aid).val()).val(),catImgId:traJq("#TT2catImgId-"+traJq("TT2qid4answer-"+aid).val()).val(),answerSubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})});traJq(".TT2shareReplyBtn").click(function(){var rid=this.id.substring(this.id.lastIndexOf("-")+1);showReplySharePanel({id:rid,replyText:traJq("#TT2rawReplyText-"+rid).val(),answerText:traJq("#TT2rawAnswerText-"+traJq("TT2aid4reply-"+rid).val()).val(),catImgId:traJq("#TT2catImgId-"+traJq("TT2qid4reply-"+rid).val()).val(),replySubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})});traJq(".TT2itemLink").click(function(){var matchLinkRoot=getSessionUrl("/traServer2/logClickEvent","siteKey="+turnToSettings.siteKey+"&v="+TurnTo.version+"&curl="+encodeURIComponent(document.URL));var catItmId=this.id.substring(this.id.lastIndexOf("-")+1);window.location=matchLinkRoot+"&catItemId="+catItmId});toggleSubWindow("block");resizeSubWindow("TT2myAsk")},error:function(data,ioArgs){var message="showMyAsk XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function showSiteVisitAdminControl(){insertScreen("#TTtraLayout",TurnToHTML.siteVisitsAdmin,{blocking:TurnToTrack.isSiteVisitTrackingBlock(),siteDisplayName:TurnTo.matchData.site.siteName});TurnTojQuery(".TT2sectionHeader").hide();traJq("#TT2footerFeedback").hide();traJq("#TTsignUpLink3").hide();traJq("#TT2block").click(function(){TurnToTrack.blockSiteVisitTracking();showSiteVisitAdminControl()});traJq("#TT2clear").click(function(){TurnToTrack.unblockSiteVisitTracking();showSiteVisitAdminControl()});traJq("#TT2close").click(toggleMainWindow)}var closedFromSpinScreen=false;var hasSpinScreen=false;function instantTeaSpinScreen(){if(turnToSettings.useSetup){TurnTojQuery("#TurnToSetup").append("<div id='TTturntospinner' style='display:none'></div>")}else{TurnTojQuery("body").prepend("<div id='TTturntospinner' style='display:none'></div>")}TurnTojQuery("#TTturntospinner").html(TurnToHTML.spinscreen);TurnTojQuery("#TTtraWindow").css({top:TurnTojQuery(document).scrollTop()+6+"px",left:((TurnTojQuery(window).width()/2)-350)+"px"});TurnTojQuery("#TTspinnerClose").click(function(e){closedFromSpinScreen=true;TurnTojQuery("#TTturntospinner").hide()});TurnTojQuery("#TTturntospinner").show();hasSpinScreen=true}function dismissMessage(id){dismissQuestionWidgetMessage(id);if(traJq("#TTLImessageArea")&&traJq("#TTLImessageArea").length>0){if(traJq(id)&&traJq(id).length>0){traJq(id).hide();totalLiMessages--;checkLiMessages()}}}function dismissFbNewUserMess(){dismissMessage("#TTnewfbusermessage");TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/dismissnewfbuser")+"&cid="+cid,load:function(data,ioArgs){console.log("dismissed new fb user")},error:function(data,ioArgs){var message="dismiss new fb user XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function checkLiMessages(){if(totalLiMessages<0){totalLiMessages=0}(totalLiMessages==0)?traJq("#TTLImessageArea").hide():traJq("#TTLImessageArea").show()}function addLIMessage(params){totalLiMessages++;checkLiMessages();traJq("#TTLImessageArea").append(TurnToHTML[params.screen]({params:params}))}function dismissLoggedInItemMessage(params){traJq("#TT2loggedInItemMessageArea"+params.suid).hide();traJq("#TT2loggedInItemMessageArea"+params.suid).html("");traJq("#TT2clearBoth"+params.suid).remove()}var numQuestionWidgetMsg=0;function addQuestionWidgetMessage(params){var msgArea=traJq("#TT2questionWidgetMessageArea");msgArea.append(TurnToHTML[params.screen]({params:params}));if(numQuestionWidgetMsg==0){msgArea.show()}numQuestionWidgetMsg++}function dismissQuestionWidgetMessage(id){var msgArea=traJq("#TT2questionWidgetMessageArea");if(msgArea&&msgArea.length>0){if(traJq(id)&&traJq(id).length>0){traJq(id).hide();numQuestionWidgetMsg--}if(numQuestionWidgetMsg==0){msgArea.hide()}}}function setLoggedInItemMessage(params){traJq("#TT2loggedInItemMessageArea"+params.suid).show();traJq("#TT2loggedInItemMessageArea"+params.suid).before("<div id='TT2clearBoth"+params.suid+"' class='TT2clearBoth'></div>");traJq("#TT2loggedInItemMessageArea"+params.suid).html("<a style='float:right' id='TT2dismissLoggedInItemMessage"+params.suid+"' href='javascript:void(0)'><img src='"+getStaticUrl("/tra2/images/subCloseButton.png")+"' border='0' style='padding-left:2px'/></a>");traJq("#TT2loggedInItemMessageArea"+params.suid).append(TurnToHTML[params.screen]({params:params}));traJq("#TT2dismissLoggedInItemMessage"+params.suid)[0].suid=params.suid;traJq("#TT2dismissLoggedInItemMessage"+params.suid)[0].onclick=function(e){dismissLoggedInItemMessage({suid:this.suid})}}function userStatusDisplayLI(matchRow,zip,suid){var mtype=matchRow.mtype;var stype=matchRow.stype;var s="";if(mtype==1){s+=" a <span class='TT2redbold'>neighbor</span> in zip "+zip}if(mtype==2){s+=" a <span class='TT2redbold'>neighbor</span> near zip "+zip}if(mtype==3){s+=" <span class='TT2redbold'>your friend</span>"}if(mtype==4){s+="a <span class='TT2redbold'>friend of your friend</span> <img style='width: 24px;height: 24px;vertical-align:top;' src='"+getUserPhoto(matchRow.flfs[0].uid,matchRow.flfs[0].pid)+"'/> <b>"+matchRow.flfs[0].fName+" "+matchRow.flfs[0].lName+"</b>"}if(mtype==5){s+=" <span class='TT2redbold'>A friend of yours</span> <input id='TT2seeWho"+suid+"' type='button' class='TT2button' value='see who'/>"}if(mtype!=0&&stype!=0){s+=" and "}if(stype==1){s+=" a <span class='TT2redbold'>repeat customer</span>"}if(stype==2){s+=" a <span class='TT2redbold'>recent customer</span>"}if(stype==3){s+=" a <span class='TT2redbold'>top customer</span> "}return s}function userStatusDisplay(mtype,stype,zip){var s="";if(mtype==1){s+=" a <span class='TT2redbold'>neighbor</span> in zip "+zip}if(stype==1){if(mtype==1){s+=" and "}s+=" <span class='TT2redbold'>repeat customer</span>"}if(stype==2){s+="  just "}s+=" bought";return s}function displayUserSetting(callbackF){TurnTo.photoEditorShow=false;getUserSetting(callbackF?callbackF:function(){insertScreen("#TTSubScreen",TurnToHTML.userSetting,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl});toggleSubWindow("block");insertScreen("#contactInfo",TurnToHTML.userSettingEmailAddresses,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl});var traHeight=TurnTojQuery("#TTmainContent").height();traJq("#TT2userSetting").css({maxHeight:traHeight-50+"px"});if(TurnTojQuery.browser.msie){traJq("#TT2userSetting").css({height:"auto"})}setupUserSettingAction()})}function getUserSetting(callbackF){TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getUserSetting")+"&cid="+cid,load:function(data,ioArgs){TurnTo.matchData.user.setting=data;if(callbackF){callbackF()}},error:function(data,ioArgs){var message="getUserSetting XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function setupUserSettingAction(){traJq("#pwd_change").click(showUserSettingPwdChange);traJq("#pwd_nochange").click(hideUserSettingPwdChange);traJq("#showPhotoEditorLink1").click(showPhotoEditor);traJq("#showPhotoEditorLink2").click(showPhotoEditor);traJq("#closePhotoButton").click(closePhotoEditor);traJq("#updateButton").click(updateUserSetting);traJq("#savePhotoButton").click(saveUserPhoto);traJq("#TT2userSettingFbConnect").click(function(){connectWithFacebook(null,{link:true,onConnectClose:function(){displayUserSetting()}})});traJq("input[name='userImgSelection']").click(function(){var imageType=this.value;if(imageType=="facebook"){traJq("#tmpUserImg").show();traJq("#tmpUserImg")[0].src="";traJq("#tmpUserImg")[0].src=getFacebookUserPhoto()}else{traJq("#tmpUserImg")[0].src="";if(TurnTo.matchData.user.hasPhoto&&TurnTo.matchData.user.pid.indexOf("fb")>0){traJq("#tmpUserImg").hide()}else{traJq("#tmpUserImg").show();traJq("#tmpUserImg")[0].src=getUserPhoto(TurnTo.matchData.user.id,TurnTo.matchData.user.pid)}}})}function updateUserSetting(){TurnTo.error=null;traJq("#updateButton").hide();traJq("#updateLoading").show();var p={};var firstName,lastName;traJq("#profileForm").find("input").each(function(i){if(this.type=="checkbox"&&this.checked){p[this.name]=TurnTojQuery.trim(this.value)}else{if(this.type!="checkbox"){p[this.name]=TurnTojQuery.trim(this.value);if(this.name=="firstName"){firstName=this.value}else{if(this.name=="lastName"){lastName=this.value}}}}});if(TurnTo.photoEditorShow){var imgTypeSelected=traJq("input[name='userImgSelection']:checked").val();if(imgTypeSelected!="facebook"&&TurnTo.userSettingUploadId){p.uploadId=TurnTo.userSettingUploadId;p.imgTypeSelected=imgTypeSelected;TurnTo.userSettingUploadId=null}if(imgTypeSelected=="facebook"){p.imgTypeSelected=imgTypeSelected}}xhr("POST",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/updateUserSetting")+"&cid="+cid,content:p,load:function(data,ioArgs){TurnTo.matchData.user.firstName=firstName;TurnTo.matchData.user.lastName=lastName;TurnTo.matchData.user.pid=data.pid;TurnTo.matchData.user.uIdents=data.uIdents;updateTitleBar();toggleSubWindow("none");traJq("#profileForm").find("input").each(function(i){var email=TurnTojQuery.trim(this.value).toLowerCase();if(this.name.indexOf("newEmail")>=0&&email.length>0){var index=getIdentIndexInUser(email);if(index!=-1){var id=TurnTo.matchData.user.uIdents[index].id;addLIMessage({screen:"unconfirmedEmailMessage",email:email,id:id});traJq("#TT2resendEmailConfirmation"+id)[0].email=email;traJq("#TT2resendEmailConfirmation"+id)[0].onclick=function(){resendEmailConfirmation(this.email)}}}});if(p.imgTypeSelected){dismissMessage("#TT2noUserPhotoMessage")}hideUserSettingPwdChange()},error:function(data,ioArgs){var message="";var errorText="";traJq("#userSettingSuccess").hide();var userSettingErrors=traJq("#userSettingErrors");userSettingErrors.show();if(data.errors){for(var i=0;i<data.errors.length;i++){errorText+="<li>"+data.errors[i]+"</li>"}}else{if(data.error){message="makePrimaryIdent XHR - Status: "+data.status+" Error: "+data.error;errorText="<li>"+data.error+"</li>"}}userSettingErrors.append("<ul>"+errorText+"</ul>");traJq("#updateButton").show();traJq("#updateLoading").hide();console.debug(message)},timeout:turnToSettings.timeout})}function updateTitleBar(){traJq(".TThi")[0].innerHTML='<img style="display:inline;height:16px;float:left;" src="'+TurnTo.getUserPhoto(TurnTo.matchData.user.id,TurnTo.matchData.user.pid)+'" height="16px"/><b style="margin-left:10px;float:left">Welcome, '+TurnTo.matchData.user.firstName+"</b>";traJq(".TTfirstName")[0].innerHTML=TurnTo.matchData.user.firstName}function inputUserImageForUserSetting(inputEl,callback){if(inputEl.value==""){return}TurnTo.userSettingUploadId=(new Date()).getTime();traJq("#profileForm").append('<iframe id="'+TurnTo.userSettingUploadId+'" name="'+TurnTo.userSettingUploadId+'" style="display:none"/>');traJq("#profileForm").attr("target",TurnTo.userSettingUploadId);traJq("#profileForm").attr("action",getUrl("/traServer2/uploadTmpUserImage"));traJq("#profileForm").attr("enctype","multipart/form-data");traJq("#profileForm").attr("encoding","multipart/form-data");traJq("#profileForm").append('<input type="hidden" id="uploadId" name="uploadId" value="'+TurnTo.userSettingUploadId+'"/>');traJq("#profileForm").submit();traJq("#profileForm").removeAttr("action");traJq("#profileForm").removeAttr("enctype");traJq("#profileForm").removeAttr("target");traJq("#tmpUserImg").hide();traJq("#tmpUserImgLoading").show();traJq("#tmpUserImg").load(function(){clearInterval(TurnTo.userSettingCheckImgInterval);traJq("#tmpUserImgLoading").hide();traJq("#tmpUserImg").show();traJq("#uploadId").remove();traJq("#TT2customUserImgRadio").show();traJq("input[name='userImgSelection'][value=custom]").attr("checked","checked");TurnTojQuery("#"+TurnTo.userSettingUploadId).remove()});TurnTo.userSettingCheckImgInterval=setInterval(function(){traJq("#tmpUserImg").attr("src",getUrl("/traServer2/displayTmpUserImage?uploadId="+TurnTo.userSettingUploadId+"&t="+(new Date()).getTime()))},1000)}function saveUserPhoto(){var imgTypeSelected=traJq("input[name='userImgSelection']:checked").val();if(imgTypeSelected=="facebook"||TurnTo.userSettingUploadId){traJq("#userImg").hide();traJq("#userImgLoading").show();traJq("#userImg").load(function(){traJq("#userImg").show();traJq("#userImgLoading").hide();traJq("#TT2userImgFile").val("");updateTitleBar();if(imgTypeSelected=="facebook"){traJq("#tmpUserImg").hide();traJq("#TT2customUserImgRadio").show()}else{traJq("#tmpUserImg").show()}dismissMessage("#TT2noUserPhotoMessage");traJq("#showPhotoEditorLink1").show()});TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/saveUserImage")+"&cid="+cid+"&uploadId="+TurnTo.userSettingUploadId+"&imgTypeSelected="+imgTypeSelected,load:function(data,ioArgs){if(data.pid&&data.pid!="0"){TurnTo.matchData.user.pid=data.pid;TurnTo.matchData.user.hasPhoto=true}traJq("#showPhotoEditorLink2").html("&nbsp;Change Photo");traJq("#showPhotoEditorLink1").show();traJq("#userImg").attr("src",getUserPhoto(TurnTo.matchData.user.id,TurnTo.matchData.user.pid));TurnTo.userSettingUploadId=null},error:function(data,ioArgs){var message="saveUserPhoto XHR - Status: "+data.status+" Error: "+data.error;console.debug(message);TurnTo.userSettingUploadId=null},timeout:turnToSettings.timeout})}closePhotoEditor()}function showPhotoEditor(){TurnTo.photoEditorShow=true;traJq("#showPhotoEditorLink1").hide();traJq("#editPhoto").show();traJq("#editPhotoButtons").show();if(TurnTo.matchData.user.hasPhoto){traJq("#tmpUserImg").show();if(TurnTo.matchData.user.pid.indexOf("fb")>0){traJq("#tmpUserImg")[0].src=getFacebookUserPhoto();traJq("input[name='userImgSelection']")[1].checked=true}else{traJq("#tmpUserImg")[0].src=getUserPhoto(TurnTo.matchData.user.id,TurnTo.matchData.user.pid);traJq("input[name='userImgSelection']")[0].checked=true}}}function closePhotoEditor(){TurnTo.photoEditorShow=false;if(TurnTo.matchData.user.hasPhoto){traJq("#showPhotoEditorLink1").show()}traJq("#editPhoto").hide();traJq("#editPhotoButtons").hide()}function makePrimaryIdent(id,callbackF){TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/makeIdentifierPrimary/"+id)+"&cid="+cid,load:function(data,ioArgs){if(callbackF){callbackF()}else{getUserSetting(function(){insertScreen("#contactInfo",TurnToHTML.userSettingEmailAddresses,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl})})}},error:function(data,ioArgs){var message="makePrimaryIdent XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function removeIdent(id,callbackF){TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/deleteIdentifier/"+id)+"&cid="+cid,load:function(data,ioArgs){if(callbackF){callbackF()}else{getUserSetting(function(){insertScreen("#contactInfo",TurnToHTML.userSettingEmailAddresses,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl})})}},error:function(data,ioArgs){var message="removeIdent XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function disconnectTwitter(){TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/disconnectTwitter")+"&cid="+cid,load:function(data,ioArgs){traJq("#twitterConnected").css({display:"none"});traJq("#twitterNotConnected").css({display:"inline"});TurnTo.matchData.user.setting.twitterIdentifier=null},error:function(data,ioArgs){var message="disconnectTwitter XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function disconnectFacebook(){TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/disconnectFacebook")+"&cid="+cid,load:function(data,ioArgs){traJq("#facebookConnected").css({display:"none"});traJq("#facebookNotConnected").css({display:"inline"});TurnTo.matchData.user.setting.twitterIdentifier=null},error:function(data,ioArgs){var message="disconnectFacebook XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function showUserSettingPwdChange(){traJq("#pwd_change").hide();traJq("#pwd_nochange").show();traJq("#currpasswd").show();traJq("#pwd_1").show();traJq("#pwd_2").show();traJq("#editingPasswd").val(true)}function hideUserSettingPwdChange(){traJq("#pwd_change").show();traJq("#pwd_nochange").hide();traJq("#currpasswd").hide();traJq("#pwd_1").hide();traJq("#pwd_2").hide();traJq("#editingPasswd").val(false)}function displayPastPurchases(notInSubwindow){if(TurnTo.matchData.counts.userItems>0){traJq("#TT2ManagePurchasesLink").html("My Purchases ("+TurnTo.matchData.counts.userItems+")");if(!TurnTo.matchData.user.siteSharing){insertScreen("#TTSubScreen",TurnToHTML.pastPurchases,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl});var tips=traJq("#TT2pastPurchasesTips");if(tips.length>0){tips.click(showPastPurchasesTips)}toggleSubWindow("block");traJq("#TT2managePastPurchaseBtn")[0].onclick=function(e){traJq("#TT2managePastPurchaseBtn").hide();traJq("#TT2managePastPurchaseLoading").show();traJq("#TT2NotManagePastPurchaseLink").hide();updateSharing(true,function(){managePastPurchases()})};traJq("#TT2NotManagePastPurchaseLink")[0].onclick=function(e){updateSharing(false,function(e){toggleSubWindow("none")})};var traHeight=TurnTojQuery("#TTmainContent").height();traJq("#TT2pastPurchases").css({maxHeight:traHeight-130+"px"});if(TurnTojQuery.browser.msie){traJq("#TT2pastPurchases").css({height:"auto"})}}else{managePastPurchases()}}else{traJq("#TT2ManagePurchasesLink").html("Find Your Purchases");displayFindPurchaseEmailInputScreen()}if(notInSubwindow){traSubWindow.overlay.display="none";traJq("#TT2pastPurchases").css({maxHeight:(TurnTojQuery(window).height()-230)+"px"});if(TurnTojQuery.browser.msie){traJq("#TT2pastPurchases").css({height:"auto"})}}}function showPastPurchasesTips(){traJq(".TT2pastPurchasesTipsDesc").show();var pastPur=traJq("#TT2pastPurchases");var originalHeight=pastPur.css("max-height");if(TurnTojQuery.browser.msie){originalHeight=pastPur.css("height")}originalHeight=parseInt(originalHeight);pastPur.css({maxHeight:(originalHeight-100)+"px"});if(TurnTojQuery.browser.msie){pastPur.css({height:(originalHeight-100)+"px"})}traJq("#TT2pastPurchasesTipsDismiss").click(function(){traJq(".TT2pastPurchasesTipsDesc").hide();pastPur.css({maxHeight:(originalHeight)+"px"});if(TurnTojQuery.browser.msie){pastPur.css({height:"auto"})}})}function findUserRaveTarget(targetId){if(TurnTo.matchData.user.raveTargets){for(var i=0;i<TurnTo.matchData.user.raveTargets.length;i++){if(TurnTo.matchData.user.raveTargets[i].id==targetId){return i}}}return -1}function managePastPurchases(postReg,orderConf){var connectedFb=findUserRaveTarget(TurnTo.facebookTargetId)>-1;var connectedTwitter=findUserRaveTarget(TurnTo.twitterTargetId)>-1;var idents;idents=TurnTo.matchData.user?TurnTo.matchData.user.uIdents:null;insertScreen("#TTSubScreen",TurnToHTML.pastPurchasesManagement,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl,connectedFb:connectedFb,connectedTwitter:connectedTwitter,orderConf:orderConf,idents:idents});toggleSubWindow("block");if(!orderConf){traJq("#TT2seeMorePurchases").click(function(){traJq("#TT2doneAddedEmail").hide();traJq("#TT2addtionalEmail").show();traJq("#TT2pastPurchases")[0].scrollTop=traJq("#TT2pastPurchases")[0].scrollTop+40;traJq("#TT2addEmailBtn").click(function(){var email=TurnTojQuery.trim(traJq("#TT2email").val()).toLowerCase();if(email.length==0){return}addEmailAddresses({TTaddEmail_0:email},function(data){if(data.uIdents){TurnTo.matchData.user.uIdents=data.uIdents;traJq("#TTSubDialogErrors").html("");traJq("#TT2addtionalEmail").hide();traJq("#TT2doneAddedEmail").show();traJq("#TT2emailList").append(email+" - Unconfirmed <a ident='"+email+"' style='color:#003399' href='javascript:void(0)'>(Resend confirmation email)</a><br/>");traJq('a[ident="'+email+'"]').click(function(){resendEmailConfirmation(email)});var index=getIdentIndexInUser(email);if(index!=-1){var id=TurnTo.matchData.user.uIdents[index].id;addLIMessage({screen:"unconfirmedEmailMessage",email:email,id:id});traJq("#TT2resendEmailConfirmation"+id)[0].email=email;traJq("#TT2resendEmailConfirmation"+id)[0].onclick=function(){resendEmailConfirmation(this.email)}}}})})})}var commentAreas=traJq(".TT2commentArea");var comments=traJq(".TT2ppComment");var saveButtons=traJq(".TT2ppSaveCommentButton");var editLinks=traJq(".TT2ppEditCommentLink");var delLinks=traJq(".TT2ppDelCommentLink");var checkboxGroup=traJq(".TT2postCheckboxGroup");var stiIds=traJq("input[name=TT2stiIds]");var facebookBoxes=traJq("input[name=TT2facebookBox]");var twitterBoxes=traJq("input[name=TT2twitterBox]");for(var i=0;i<comments.length;i++){comments[i].indexNum=i;comments[i].oldValue=comments[i].value;saveButtons[i].indexNum=i;delLinks[i].indexNum=i;editLinks[i].indexNum=i;facebookBoxes[i].indexNum=i;twitterBoxes[i].indexNum=i;if(TurnTo.matchData.user.items[i].raveComment){delLinks[i].style.display="inline";editLinks[i].style.display="inline";comments[i].style.height="100px";commentAreas[i].style.paddingBottom="18px"}delLinks[i].onclick=function(e){var stiIdElm=stiIds[this.indexNum];deleteComment4UserItem(stiIdElm.value,deleteComment4UserItemCallback)};editLinks[i].onclick=function(e){focusComment(this.indexNum)};saveButtons[i].onclick=function(e){var comment=comments[this.indexNum].value;if(TurnTojQuery.trim(comment).length>0){this.style.display="none";traJq(".TT2submitImgLoading")[this.indexNum].style.display="inline";var postToFacebook=facebookBoxes[this.indexNum].checked;var postToTwitter=twitterBoxes[this.indexNum].checked;var stiIdElm=stiIds[this.indexNum];saveComment4UserItem(stiIdElm.value,comment,postToFacebook,postToTwitter,saveComment4UserItemCallback)}else{}};twitterBoxes[i].onchange=function(e){if(this.checked&&findUserRaveTarget(TurnTo.twitterTargetId)<0){connectWithTwitter(e,{})}};facebookBoxes[i].onchange=function(e){if(this.checked){if(!TurnTo.matchData.user.fbPostToWallGranted){connectWithFacebook(e,{post:true,photo:true,link:true,keepSubwindow:true,noMatchesShow:true,onConnectClose:function(){getFbPostToWallGranted(function(){if(!TurnTo.matchData.user.fbPostToWallGranted){traJq("input[name=TT2facebookBox]").attr("checked",false)}})}})}}};comments[i].onfocus=function(e){focusComment(this.indexNum)};comments[i].onkeyup=function(e){if(this.value.length>500){traJq(".TT2charLimitCount")[this.indexNum].style.color="red";saveButtons[this.indexNum].style.display="none"}else{traJq(".TT2charLimitCount")[this.indexNum].style.color="black";saveButtons[this.indexNum].style.display="inline"}traJq(".TT2charLimitCount")[this.indexNum].innerHTML=500-this.value.length};facebookBoxes[i].onfocus=function(e){focusComment(this.indexNum)};twitterBoxes[i].onfocus=function(e){focusComment(this.indexNum)}}traJq("#TT2ppDoneBtn")[0].onclick=function(e){if(checkUnsaveComments()){if(postReg){if(!TurnTo.matchData.user.isFbConnected){displayPostRegInfo()}else{toggleSubWindow("none")}}else{toggleSubWindow("none")}}};var traHeight=TurnTojQuery("#TTmainContent").height();var height=traHeight>100?traHeight-100:TurnTojQuery(window).height()-150;traJq("#TT2pastPurchases").css({maxHeight:height+"px"});if(TurnTojQuery.browser.msie){traJq("#TT2pastPurchases").css({height:"auto"});if(traJq("#TT2pastPurchases").height()>height){traJq("#TT2pastPurchases").css({height:height+"px"})}}}function displayPostRegInfo(){var q=insertScreen("#TTSubScreen",TurnToHTML.postRegInfo,{matches:TurnTo.matchData,getUrl:getUrl,getStaticUrl:getStaticUrl,fbButtonSrc:getStaticUrl("/tra2/images/use-facebook.png")});toggleSubWindow("block");traJq("#TTpostRegFbConnect").click(function(e){connectWithFacebook(e,{onConnectClose:function(e){if(TurnTo.matchData.user.isFbConnected){toggleSubWindow("none")}},link:true,photo:true,noMatchesShow:true,keepSubwindow:true})});traJq("#TTpostRegFbConnect2").click(function(e){connectWithFacebook(e,{onConnectClose:function(e){if(TurnTo.matchData.user.isFbConnected){toggleSubWindow("none")}},link:true,photo:true,noMatchesShow:true,keepSubwindow:true})});traJq("#TTregForm")[0].onsubmit=function(e){submitPostRegInfo(function(){});return false};if(TurnTo.matchData.siteUser&&TurnTo.matchData.siteUser.fbMatched){traJq("#TT2postRegPhoto").hide()}}function submitPostRegInfo(callbackF){var p={};traJq(".TT2doneSub").hide();traJq("#TTregSubmitLoading").show();p.passwd=traJq("#TTpassword")[0].value;p.repasswd=traJq("#TTrePassword")[0].value;if(TurnTo.lastUploadedImageId){p.uploadId=TurnTo.lastUploadedImageId}var enteredPwd=p.passwd.length>0||p.repasswd.length>0;var enteredPhoto=p.uploadId;if(!enteredPwd&&!enteredPhoto){toggleSubWindow("none");return}else{if(!enteredPwd&&enteredPhoto){TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/saveUserImage")+"&cid="+cid+"&uploadId="+TurnTo.lastUploadedImageId,load:function(data,ioArgs){toggleSubWindow("none")},error:function(data,ioArgs){var message="saveUserImage XHR - Status: "+data.status+" Error: "+data.error;console.debug(message);TurnTo.lastUploadedImageId=null;toggleSubWindow("none")},timeout:turnToSettings.timeout})}else{if(enteredPwd){if(enteredPhoto){p.imgTypeSelected="custom"}p.editingPasswd=true;p.firstName=TurnTo.matchData.user.firstName;p.lastName=TurnTo.matchData.user.lastName;p.id=TurnTo.matchData.user.id;xhr("POST",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/updateUserSetting")+"&cid="+cid,content:p,load:function(data,ioArgs){TurnTo.matchData.user.pid=data.pid;toggleSubWindow("none");if(callbackF){callbackF()}},error:function(data,ioArgs){traJq("#TTregSubmitLoading").hide();traJq("#TTregSubmit").show();traJq("#TTregSkip").show();displayErrorsInSubscreen(data.errors)},timeout:turnToSettings.timeout})}}}}function focusComment(index){var commentAreas=traJq(".TT2commentArea");var comments=traJq(".TT2ppComment");var savedComments=traJq(".TT2ppSavedComment");var saveButtons=traJq(".TT2ppSaveCommentButton");var checkboxGroup=traJq(".TT2postCheckboxGroup");var delLinks=traJq(".TT2ppDelCommentLink");var editLinks=traJq(".TT2ppEditCommentLink");var limitReminders=traJq(".TT2limitReminder");if(TurnTo.ppLastFocusIndex>=0){if(!comments[TurnTo.ppLastFocusIndex].value){checkboxGroup[TurnTo.ppLastFocusIndex].style.display="none";saveButtons[TurnTo.ppLastFocusIndex].style.display="none";comments[TurnTo.ppLastFocusIndex].style.height="20px";delLinks[TurnTo.ppLastFocusIndex].style.display="none";editLinks[TurnTo.ppLastFocusIndex].style.display="none";limitReminders[TurnTo.ppLastFocusIndex].style.display="none";commentAreas[TurnTo.ppLastFocusIndex].style.paddingBottom="5px"}else{if(comments[TurnTo.ppLastFocusIndex].value==comments[TurnTo.ppLastFocusIndex].oldValue){checkboxGroup[TurnTo.ppLastFocusIndex].style.display="none";saveButtons[TurnTo.ppLastFocusIndex].style.display="none";savedComments[TurnTo.ppLastFocusIndex].style.display="block";comments[TurnTo.ppLastFocusIndex].style.height="20px";comments[TurnTo.ppLastFocusIndex].style.display="none";delLinks[TurnTo.ppLastFocusIndex].style.display="inline";editLinks[TurnTo.ppLastFocusIndex].style.display="inline";limitReminders[TurnTo.ppLastFocusIndex].style.display="none";commentAreas[TurnTo.ppLastFocusIndex].style.paddingBottom="18px"}}}TurnTo.ppLastFocusIndex=index;if(index>=0){savedComments[index].style.display="none";comments[index].style.height="100px";comments[index].style.display="inline";comments[index].focus();if(comments[index].value.length<=500){saveButtons[index].style.display="inline"}checkboxGroup[index].style.display="inline";delLinks[index].style.display="none";editLinks[index].style.display="none";limitReminders[index].style.display="inline";commentAreas[index].style.paddingBottom="28px"}}function deleteComment4UserItemCallback(data){var indexNum=getIndexByStiId(data.id);traJq(".TT2commentArea")[indexNum].style.paddingBottom="5px";traJq(".TT2ppSavedComment")[indexNum].style.display="none";traJq(".TT2ppSavedComment")[indexNum].innerHTML="";traJq(".TT2ppComment")[indexNum].value="";traJq(".TT2ppComment")[indexNum].oldValue="";traJq(".TT2ppComment")[indexNum].style.height="20px";traJq(".TT2ppComment")[indexNum].style.display="inline";traJq(".TT2ppComment")[indexNum].onfocus=function(e){focusComment(this.indexNum)};traJq(".TT2ppEditCommentLink")[indexNum].style.display="none";traJq(".TT2ppDelCommentLink")[indexNum].style.display="none"}function saveComment4UserItemCallback(data){var indexNum=getIndexByStiId(data.id);traJq(".TT2submitImgLoading")[indexNum].style.display="none";traJq(".TT2ppComment")[indexNum].style.display="none";traJq(".TT2postCheckboxGroup")[indexNum].style.display="none";traJq(".TT2limitReminder")[indexNum].style.display="none";traJq(".TT2ppSaveCommentButton")[indexNum].style.display="none";traJq(".TT2ppSavedComment")[indexNum].innerHTML='"'+traJq(".TT2ppComment")[indexNum].value+'"';traJq(".TT2ppSavedComment")[indexNum].style.display="block";traJq(".TT2ppDelCommentLink")[indexNum].style.display="inline";traJq(".TT2ppEditCommentLink")[indexNum].style.display="inline";traJq(".TT2ppComment")[indexNum].oldValue=traJq(".TT2ppComment")[indexNum].value;traJq(".TT2ppComment")[indexNum].onblur=function(e){if(this.oldValue==this.value){traJq(".TT2postCheckboxGroup")[this.indexNum].style.display="none";traJq(".TT2ppSaveCommentButton")[this.indexNum].style.display="none";traJq(".TT2ppDelCommentLink")[this.indexNum].style.display="inline";traJq(".TT2ppEditCommentLink")[this.indexNum].style.display="inline";traJq(".TT2ppSavedComment")[this.indexNum].style.display="block";this.style.display="none"}};traJq(".TT2commentArea")[indexNum].style.paddingBottom="18px"}function getIndexByStiId(stiId){var stiIds=traJq("input[name=TT2stiIds]");for(var i=0;i<stiIds.length;i++){if(stiIds[i].value==stiId){return i}}return -1}function saveComment4UserItem(stiId,comment,postToFacebook,postToTwitter,callbackF){comment=stripString(comment);TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/updateComment4UserItem/"+stiId)+"&cid="+cid+"&raveComment="+encodeURIComponent(comment)+"&postToFacebook="+postToFacebook+"&postToTwitter="+postToTwitter,load:function(data,ioArgs){if(callbackF){callbackF(data)}},error:function(data,ioArgs){var message="saveComment4UserItem XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function deleteComment4UserItem(stiId,callbackF){TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/updateComment4UserItem/"+stiId)+"&cid="+cid+"&rave=false",load:function(data,ioArgs){if(callbackF){callbackF(data)}},error:function(data,ioArgs){var message="deleteComment4UserItem XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function finishPastPurchaseManagement(){saveAllUnsavedComments()}function checkUnsaveComments(){var saveButtons=traJq(".TT2ppSaveCommentButton");var hasUnsave=false;saveButtons.each(function(i){if(this.style.display=="inline"){var comment=traJq(".TT2ppComment")[i].value;if(comment.length>0){hasUnsave=true}}});if(hasUnsave){if(confirm('Your comments will not be saved unless you click "Submit". Click "Cancel" to go back and save your comments. Click "OK" to continue without saving.')){return true}return false}else{return true}}function saveAllUnsavedComments(){var saveButtons=traJq(".TT2ppSaveCommentButton");var error;if(error){alert(error)}else{toggleSubWindow("none")}}function updateSharing(share,callbackF){TurnTo.error=null;if(share){TurnTo.newSharePastPurchases=true}xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/updateSharing/")+"&cid="+cid+"&sharing="+share+(TurnTo.matchData.siteUser?"&suid="+TurnTo.matchData.siteUser.id:""),load:function(data,ioArgs){if(callbackF){callbackF(data)}},error:function(data,ioArgs){var message="updateSharing XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function connectWithTwitter(e,options){var svisitParams=TurnToTrack.makeVisitorPartsQS(TurnToTrack.getVisitorParts());var ttTwitterUrl=getUrl("/oauth/auth?return_controller=traServer2&return_action=oauthComplete&error_controller=traServer2&error_action=oauthComplete&consumer=twitter&siteKey="+turnToSettings.siteKey+svisitParams);if(TurnTo.twitterConnectPopupInterval){clearInterval(TurnTo.twitterConnectPopupInterval)}TurnTo.twitterwin=window.open(ttTwitterUrl,"ConnectWithTwitter","height=550,width=650,status=1,toolbar=0,menubar=0,resizable=1,scrollbars=1");if(TurnTo.twitterwin){TurnTo.twitterConnectPopupInterval=setInterval(function(){twitterConnectPopupIntervalFn(options)},500)}stopDefault(e)}function twitterConnectPopupIntervalFn(options){if(TurnTo.twitterwin.closed){clearInterval(TurnTo.twitterConnectPopupInterval);if(options.onConnectClose){options.onConnectClose()}else{getUserRaveTargets(refreshPastPurchaseTwitterConnect)}}}function getUserRaveTargets(callbackF){TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getUserRaveTargets/")+"&cid="+cid,load:function(data,ioArgs){TurnTo.matchData.user.raveTargets=data.raveTargets;if(callbackF){callbackF(data)}},error:function(data,ioArgs){var message="getUserRaveTargets XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function refreshPastPurchaseTwitterConnect(data){var twitterBoxes=traJq("input[name=TT2twitterBox]");var foundTwitterTarget=findUserRaveTarget(TurnTo.twitterTargetId)>-1;for(var i=0;i<twitterBoxes.length;i++){if(findUserRaveTarget(TurnTo.twitterTargetId)>-1){twitterBoxes[i].checked=true}else{twitterBoxes[i].checked=false}}}function refreshPastPurchaseFbConnect(data){var facebookBoxes=traJq("input[name=TT2facebookBox]");var foundFbTarget=findUserRaveTarget(TurnTo.facebookTargetId)>-1;for(var i=0;i<facebookBoxes.length;i++){if(foundFbTarget){facebookBoxes[i].checked=true}else{facebookBoxes[i].checked=false}}}function defaultItemTeaserDisplay(data){var preText=turnToConfig.iteaserPreamble||"<h2>Got questions?</h2>";var text=turnToConfig.iteaserText||"<span>Got Questions? <u>ASK</u> people who bought this</span>";var iteaserhtml='<div id="TT2ILTbox">'+preText+'<div id="TT2ILTbutton-holder"><a class="TT2ILTbutton TurntoItemTeaserClick" href="javascript:void(0)">'+text+"</a></div>";iteaserhtml+='<div id="TT2ILTcount-line">';if(data.counts.q>0||data.counts.a>0||data.counts.c>0){iteaserhtml+='<p><a class="TurntoItemTeaserClick TurnToIteaSee" href="javascript:void(0)">See ';if(data.counts.q>0){iteaserhtml+="<strong>"+data.counts.q+"</strong>"+(data.counts.q>1?" questions":" question");if((data.counts.a+data.counts.c)>0){iteaserhtml+=" | "}}if(data.counts.a>0){iteaserhtml+="<strong>"+data.counts.a+"</strong>"+(data.counts.a>1?" answers":" answer");if(data.counts.c>0){iteaserhtml+=" | "}}if(data.counts.c>0){iteaserhtml+="<strong>"+data.counts.c+"</strong>"+(data.counts.c>1?" comments":" comment")}iteaserhtml+="</a></p>"}iteaserhtml+="</div></div>";return iteaserhtml}function loadItemTeaser(data,ioArgs){if(!TurnTo.doIteaser){return}var iteaserhtml=turnToSettings.iTeaserFunc(data);var iteasers=TurnTojQuery(".TurnToItemTeaser");iteasers.html(iteaserhtml);iteasers.each(function(it){TurnTojQuery(".TurntoItemTeaserClick").click(function(e){setTeaserCookie();toggleMainWindow("none");showQuestionWidget({iteaserClick:true,sku:TurnToItemSku});logWebEvent2(TurnTo.eventQaItemteaserClick,(typeof(TurnToTrack)!="undefined")?"&svid="+TurnToTrack.getSiteVisitId():null)})})}function displayItemTeaser(){if(!TurnTo.doIteaser){return}xhr("GET",{iframeProxyUrl:TurnTo.staticIframeProxyUrl,useStaticHost:true,url:xhrUri("/traServer2/getItemTeaserData/"+turnToConfig.siteKey+"/"+TurnToItemSku),load:loadItemTeaser,error:function(data,ioArgs){TurnTojQuery(".TurnToItemTeaser").hide();return},timeout:turnToSettings.timeout})}function setTeaserCookie(){setCookie(TEASER_COOKIE_NAME,new Date().getTime(),2592000);if(typeof(TurnToTrack)!="undefined"){TurnToTrack.visitTeaserClick()}}function getMatches(loadFunc,nocache,login,options){TurnTo.error=null;var zipCookie=getCookie(turnToSettings.zipOnlyCookie);if(options==null||(options!=null&&!options.noMatchesShow)){insertScreen("#TTtraLayout",'<div id="TT-spinner" style="text-align:center;padding-top:60px;padding-bottom:50px;"></div>')}xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/"+(login&&!TurnTo.matchData.user?"loginGetMatches":("getMatches"+(nocache?"NoCache":""))))+(zipCookie?"&zip="+zipCookie:""),load:function(data,ioArgs){TurnTo.traDataLoaded=true;setLocalLoginCookie(data);loadFunc(data,ioArgs)},error:function(data,ioArgs){var message="getMatches XHR - Status: "+data.status+" Error: "+data.error;if(data.status==401){TurnTo.traDataLoaded=true}else{if(data.status!=406){console.debug(message)}}if(data.status==408){populateMatchesError(data,ioArgs)}else{if(data.status==405){displayImportSourceSelectScreen()}else{setLocalLoginCookie(data);loadFunc(data,ioArgs)}}},timeout:turnToSettings.timeout})}function getTeaserData(loadFunc,visitorParts){TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getTeaserData")+"&cid="+cid+TurnToTrack.makeVisitorPartsQS(visitorParts),load:loadFunc,error:function(data,ioArgs){var message="getTeaserData XHR - Status: "+data.status+" Error: "+data.error;if(data.status==406){return}if(data.status==401){}else{if(data.status!=406){console.debug(message)}}loadFunc(data,ioArgs)},timeout:turnToSettings.timeout})}function getItems4User(callbackF){TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getItems4User")+"&cid="+cid,load:function(data,ioArgs){TurnTo.matchData.user.items=data.items;if(data.items!=null){TurnTo.matchData.counts.userItems=data.items.length}TurnTo.matchData.user.siteSharing=data.siteSharing;TurnTo.matchData.user.raveTargets=data.raveTargets;TurnTo.matchData.allTargets=data.allTargets;callbackF()},error:function(data,ioArgs){var message="getItems4User XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function getMatchesLoaded(data,ioArgs){if(!processResponse(data,ioArgs)){return}populateTra(data,ioArgs,function(){displayTeasers()})}function getTeaserDataLoaded(data,ioArgs){if(data.status==503||data.status==500||data.status==404){return}if(!processResponse(data,ioArgs)){return}setLocalLoginCookie(data)}function processResponse(data,ioArgs){var headers=ioArgs?ioArgs.xhr._responseHeaders:null;if(headers){console.dir(headers);checkLocalUpdates(new Date(headers.Date).getTime())}TurnTo.matchData=data;importSrc=TurnTo.matchData.importSrc;popular=TurnTo.matchData.popular;if(data.status==406){return false}TurnTo.loggedIn=(data.status==200);if(!TurnTo.loggedIn&&data.status!=401){TurnTo.error=data.error;TurnTo.status=data.status}if(TurnTo.loggedIn&&TurnTo.thirdBlocked&&data.sessionId){setLocalCookies(data.authToken,data.tokenValiditySeconds,data.sessionId)}return true}function hasErrors(){if(TurnTo.error){TurnTojQuery(".TTerror").each(function(i){this.innerHTML=TurnTo.error});return true}return false}function checkThirdPartyCookies(){if(turnToSettings.useFirstPartyCookies){setThirdBlocked(true);return}var cName=turnToSettings.thirdBlockedCookieName;var cVal=getCookie(cName);if(cVal==null){setCookie(turnToSettings.thirdBlockedCookieName,false);cVal=false}if(cVal){setThirdBlocked(cVal=="true");return}}function checkFirebug(){var haveCon=(typeof window.console!="undefined");var haveFB=(haveCon&&("dirxml" in window.console));if(!haveFB){var names=["log","debug","info","warn","error","assert","dir","dirxml","group","groupEnd","time","timeEnd","count","trace","profile","profileEnd"];window.console={};for(var i=0;i<names.length;++i){window.console[names[i]]=function(){}}}}function updateLogoutLinks(logoutFn,innerHTML){var logoutLinks=TurnTojQuery(".TTlogoutLink");logoutLinks.each(function(i){this.href="javascript:void(0)";this.onclick=logoutFn;if(innerHTML){this.innerHTML=innerHTML}})}function updateLoggedInTeasers(teaserHTML,onclickFn){if(!onclickFn){onclickFn=showMatches}TurnTojQuery(".TTgteaser").each(function(i){this.innerHTML=teaserHTML});TurnTojQuery(".TTshowMatchesLink").each(function(i){this.href="javascript:void(0)";this.onclick=onclickFn})}function checkLocalUpdates(responseTime){var ttupd=getCookie(turnToSettings.updateCookieName);if(!ttupd){return}else{ttupd=parseInt(ttupd);if(ttupd<responseTime){setCookie(turnToSettings.updateCookieName);return}}xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getSharees"),load:function(data,ioArgs){TurnTojQuery(".TTsharingIcon").each(function(i){var isSharee=false;var sharees=data.sharees;for(var j=0;j<sharees.length;j++){if(this.matchId==sharees[j].id){isSharee=true;break}}this.isSharee=isSharee;setSharingIcon(this)})},error:function(data,ioArgs){var message="getSharees XHR - Status: "+data.status+" Error: "+data.error;if(data.status==401){}else{console.debug(message)}}})}function setThirdBlocked(blocked){TurnTo.thirdBlocked=blocked;if(TurnTo.setCookieIframe){setCookie(turnToSettings.thirdBlockedCookieName,""+blocked);var p=TurnTo.setCookieIframe.parentNode;if(p){p.removeChild(TurnTo.setCookieIframe)}TurnTo.setCookieIframe=null}}function setSharingIcon(elem){elem.href="javascript:void(0)";if(elem.isSharee){elem.className="TTsharingIcon TTisFriendIcon";elem.onclick=null;elem.href=getSessionUrl(TurnTo.friendsUrl,"id="+elem.matchId);elem.target="_blank";elem.title="You share with "+elem.firstName+". Click to manage friends."}else{elem.className="TTsharingIcon TTnotFriendIcon";elem.href=null;elem.target=null;elem.onclick=makeSharingClickFunc(elem);elem.title=elem.firstName+" is sharing with you but you are not sharing with "+elem.firstName+". Click to make "+elem.firstName+" a friend."}}function getUrl(uri,httpOnly){var urlPref=turnToSettings.urlPref||"";var proto=(httpOnly)?"http:":TurnTo.partnerProtocol;return proto+"//"+turnToSettings.host+urlPref+uri}function getStaticUrl(uri){return TurnTo.partnerProtocol+"//"+turnToSettings.staticHost+uri}function getSessionUrl(uri,query,httpOnly){if(httpOnly==null){httpOnly=true}var post="";var sid=getCookie(turnToSettings.sessionCookieName);if(sid){uri="/syncSession"+uri;post+="?js="+sid}var url=getUrl(uri,httpOnly);url+=post;if(query){url+=(sid?"&":"?")+query}return url}function xhrUri(uri){var xu=turnToSettings.urlPref+uri;var sid=getCookie(turnToSettings.sessionCookieName);if(sid){xu+=";jsessionid="+sid}xu+="?siteKey="+turnToSettings.siteKey+"&v="+encodeURIComponent(TurnTo.version)+"&uniq="+(new Date().getTime());return xu}function toggleDisplay(elem,displayStyle){var elemQ=TurnTojQuery(elem);if(typeof displayStyle=="undefined"){displayStyle="block"}if(elem.style.display==displayStyle){elemQ.hide(turnToSettings.animationMs,checkScroll);displayStyle="none"}else{elemQ.show(turnToSettings.animationMs,checkScroll);displayStyle="block"}return displayStyle}function checkScroll(){var postCss={"overflow-y":TurnTo.matchesBox.matchesList.scrollHeight>turnToSettings.matchesHeight?"scroll":"hidden"};if(TurnTojQuery.browser.msie){postCss.height=Math.min(turnToSettings.matchesHeight,TurnTo.matchesBox.matchesList.scrollHeight)+"px"}else{postCss["max-height"]=turnToSettings.matchesHeight+"px"}TurnTo.matchesBox.matchesListQ.css(postCss)}function makeLiZipSubmit(iconElem){return function(e){var oldClassName=this.className;xhr("POST",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/addPostalCode")+"&postcd="+iconElem.value,contentType:"text/text",load:function(response,ioArgs){setCookie(turnToSettings.updateCookieName,new Date().getTime());if(response.message=="invalid"){TurnTojQuery("#TTpostalErrorDiv").show()}if(response.message=="success"){TurnTo.zip=iconElem.value;traJq("#TT2userZip").html(TurnTo.zip);traJq("#TT2zipInput").hide();traJq("#TT2noZipP").hide();traJq("#TT2zipReadyP").show();traJq("#TT2changeZipLink").html("Change Zip");getMatches(getMatchesLoadedOpenPostal,true)}},error:function(data,ioArgs){var message="postalcode XHR - Status: "+data.status+" Error: "+data.error;if(data.status==401){logOut(false)}else{console.debug(message)}iconElem.className=oldClassName},timeout:turnToSettings.timeout});stopDefault(e);return false}}function getMatchesLoadedOpenPostal(data,ioArgs){getMatchesLoaded(data,ioArgs);TurnTojQuery("#TTpostalCodeMatchDetail").show()}var ttGAProxies=[];var ttGACurrentProxy=0;var webEventCount=0;function logWebEvent(event,params){if(!params){params=""}if(TurnTo.matchData!=null){if(TurnTo.matchData.user&&TurnTo.matchData.user.id){params+="&uid="+TurnTo.matchData.user.id}var webevnt=document.createElement("img");webevnt.width="1";webevnt.width="1";webevnt.border="0";webevnt.className="TTWebEvent";var c=webEventCount++;webevnt.id="TTWebEvent"+c;document.body.appendChild(webevnt);var webEventCallElem=TurnTojQuery("#TTWebEvent"+c);webEventCallElem[0].src=TurnTo.baseEventUrl+"&e="+event+(params?params:"")}}function logWebEvent2(event,params){if(typeof(TurnToTrack)!="undefined"&&TurnToTrack.isSiteVisitTrackingBlock()){return}if(!params){params=""}if(TurnTo.matchData&&TurnTo.matchData.user&&TurnTo.matchData.user.id){params+="&uid="+TurnTo.matchData.user.id}var c=webEventCount++;params+="&uniq="+c;console.debug("Logging event "+event+" with params "+params+" and webEventCount "+c);TurnTojQuery("body").append('<img width="1" border="0" class="TTWebEvent" id="TTWebEvent"'+c+'" src="'+TurnTo.baseEventUrl+"&e="+event+params+'">')}function logTraEvent(event,params){params+="&et=tra&url="+document.URL+"&pd="+turnToSettings.eventPartnerData;logWebEvent(event,params)}function showMatches(e){toggleMainWindow("none");if(TurnTo.traDataLoaded){if(traJq(".TT2LoggedInItems").length==0&&traJq(".TT2LoggedOutItems").length==0){TurnTojQuery(".TT2sectionHeader").show();insertScreen("#TTtraLayout",'<div id="TT-spinner" style="text-align:center;padding-top:60px;padding-bottom:50px;"></div>');setTimeout(function(){populateTra(TurnTo.matchData)},100)}return false}if(e){stopDefault(e)}getMatches(populateTra);return false}function showRealLogin(e){TurnTo.jq("#TTcheckFormDiv")[0].style.display="none";TurnTo.jq("#TTloginScreen")[0].style.display="block";TurnTo.jq(TurnTo.loginUsername).focus();stopDefault(e);return false}TurnTo.insertScreen=insertScreen;function insertScreen(id,html,obj){var q=traJq(id);if(q==null){}q[0].innerHTML=obj?html(obj):html;return q}function showLogin(e){toggleMainWindow("none");if(TurnTo.traDataLoaded){if(traJq(".TT2LoggedInItems").length==0&&traJq(".TT2LoggedOutItems").length==0){TurnTojQuery(".TT2sectionHeader").show();insertScreen("#TTtraLayout",'<div id="TT-spinner" style="text-align:center;padding-top:60px;padding-bottom:50px;"></div>');setTimeout(function(){populateTra(TurnTo.matchData)},100)}return}if(TurnTo.loginAtTTInterval){clearInterval(TurnTo.loginAtTTInterval)}stopDefault(e);getMatches(populateTra);if(turnToSettings.fbOpen){connectWithFacebook(null,{onConnectClose:function(){}});turnToSettings.fbOpen=false}}function populateMatchesError(data,ioArgs,callback){if(!processResponse(data,ioArgs)){return}traJq("#TT-spinner").hide();if(traJq("#TT2zipbox")&&traJq("#TT2zipbox").length>0){var zipbox=traJq("#TT2zipbox")[0];zipbox.innerHTML=""}traJq("#TTnetworkTitle").html("");insertScreen("#TTtraLayout",TurnToHTML.traMatchesError)}function populateTra(data,ioArgs,callback){preloadImages();if(!processResponse(data,ioArgs)){return}if(TurnTo.postPurchaseFlowStarted||TurnTo.pteFlowStarted||TurnTo.noSharingPrefExitFlow){closeOrderConfPurchases()}traJq("#TT-spinner").hide();var matchLinkRoot=getSessionUrl("/traServer2/logClickEvent","siteKey="+turnToSettings.siteKey+"&v="+TurnTo.version+"&curl="+encodeURIComponent(document.URL));if(TurnTo.loggedIn){insertScreen("#TTtraUserStateMain",TurnToHTML.TTtraLoggedInMain);if(turnToConfig.removeTurnToLogo){traJq("#TTtraFooterLogo").hide()}showZipForm();traJq("#TTnetworkTitle").append(TurnToHTML.loggedInTopLeft({turnToSettings:turnToSettings,siteName:TurnTo.matchData.site.siteName}));if(TurnTo.matchData.site.ask){traJq("#TT2liheadercopy").html(TurnTo.customCopy.friendsHeader)}var rowsPerPage=12;var numOfPages=parseInt(TurnTo.matchData.matches.length/rowsPerPage)+(TurnTo.matchData.matches.length%rowsPerPage==0?0:1);insertScreen("#TTtraLayout",TurnToHTML.traLoggedInLayout,{matches:TurnTo.matchData.matches,zip:TurnTo.matchData.postalCode,rowsPerPage:rowsPerPage,numOfPages:numOfPages});setupDisplayItemsLayoutNav(traJq(".TT2LoggedInItems"),numOfPages.toFixed());UIloggedIn();traJq(".TtLiSlider").each(function(i){var ul=traJq(this);var c=TurnTojQuery("li",ul).length*-1;if(c==-1){return}ul[0].style.width=((c*-1)*600)+"px";var o=0;var n=ul.parent().find(".TTnext");var p=ul.parent().find(".TTprev");ul.parent().parent()[0].style.height="140px";var a=function(){ul.animate({marginLeft:o*600+"px"},350);(o<0)?p.show():p.hide();(o>c+1)?n.show():n.hide()};p[0].onclick=function(e){o+=1;a()};n[0].onclick=function(e){o-=1;a()};n.show()});setTimeout(rolloverLI,0);for(var row=0;row<TurnTo.matchData.matches.length;row++){var maxcol2display=(TurnTo.quirksMode)?5:TurnTo.matchData.matches[row].items.length;for(var col=0;(col<maxcol2display&&col<TurnTo.matchData.matches[row].items.length);col++){var item=TurnTo.matchData.matches[row].items[col];if(col==0){var seeWho=traJq("#TT2seeWho"+item.suid);if(item.shid&&seeWho.length>0){seeWho[0].suid=item.suid;seeWho[0].shid=item.shid;seeWho[0].onclick=function(){sendForwardMatchInvitationEmail(this.suid,this.shid)}}}var titleLink=traJq("#TT2loggedInItemTitleLink"+row+""+col);titleLink[0].stiId=item.iid;titleLink[0].uId=TurnTo.matchData.matches[row].uid;var title=TurnTojQuery.trim(titleLink.html());titleLink.html(truncate(title,15,"..."));titleLink.click(function(){window.location=matchLinkRoot+"&tiId="+this.stiId+(this.uId?"&uId="+this.uId:"")})}}if(TurnTo.quirksMode){traJq(".TT2liitemBox").each(function(i){var el=traJq(this);el.css({height:"85px",width:"110px"})})}}else{var pgStart=0;var pgEnd=TurnTo.matchData.matches.length;insertScreen("#TTtraLayout",TurnToHTML.traLoggedOutLayout,{matches:TurnTo.matchData.matches,start:pgStart,end:pgEnd,userStatusDisplay:userStatusDisplay,truncate:truncate,showToolTip:(TurnTo.matchData.site.ask&&getCookie(ROLLOVER_TOOLTIP_COOKIE_NAME)!="1")});var allItemsLengthIncludingReg=TurnTo.matchData.matches.length;var itemsPerPage=18;var numOfPages=(parseInt(allItemsLengthIncludingReg/itemsPerPage)+(allItemsLengthIncludingReg%itemsPerPage==0?0:1)).toFixed();setupDisplayItemsLayoutNav(traJq(".TT2LoggedOutItems"),numOfPages);setTimeout(rolloverLO,0);if(!(TurnTojQuery.browser.msie&&TurnTojQuery.browser.version.substr(0,1)<7)){if(showbubble){}}if(TurnTo.quirksMode){setTimeout(function(){traJq("#TT2loFacebookLink").show();traJq("#TT2loConnectLink").show()},10)}}if(callback!=null&&callback!="undefined"){callback()}traJq("#TTtraFooterLogo").click(function(e){window.open(TurnTo.partnerProtocol+"//"+turnToSettings.host)});var feedback=TurnTojQuery("#TT2footerFeedback");feedback.attr("href","mailto:feedback@turnto.com?subject=Feedback on friends widget on "+encodeURIComponent(TurnTo.matchData.site.siteName)+"&body=%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A---------------------------------------------------------------%0ASubmitted from: "+encodeURIComponent(document.location));iehacks()}function showQuestionWidget(match,callback){insertScreen("#TTtraLayout",'<div id="TT-spinner" style="text-align:center;padding-top:60px;padding-bottom:50px;"></div>');TurnTojQuery(".TT2sectionHeader").hide();TurnTojQuery("#TTLImessageArea").hide();numQuestionWidgetMsg=0;traJq("#TT2questionWidgetMessageArea").html("");var feedback=TurnTojQuery("#TT2footerFeedback");feedback.show();feedback.attr("href","mailto:feedback@turnto.com?subject=Feedback on QandA widget on "+encodeURIComponent(TurnTo.matchData.site.siteName)+"&body=%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A---------------------------------------------------------------%0ASubmitted from: "+encodeURIComponent(document.location));getCommentsQuestionsAnswers4CatItem({catItmId:match.catItmId,iid:match.iid,uid:match.uid,sku:match.sku},function(data,ioArgs){insertScreen("#TTtraLayout",TurnToHTML.questionWidget,{match:match,comments:data.comments,questions:data.questions,item:data.catItem,relatedQuestions:data.relatedQuestions});traJq(".TT2shareQuestionBtn").click(function(){var qid=this.id.substring(this.id.lastIndexOf("-")+1);showQuestionSharePanel({id:qid,questionText:traJq("#TT2rawQuestionText-"+qid).val(),catImgId:traJq("#TT2catImgId").val(),questionSubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})});traJq(".TT2shareAnswerBtn").click(function(){var aid=this.id.substring(this.id.lastIndexOf("-")+1);showAnswerSharePanel({id:aid,answerText:traJq("#TT2rawAnswerText-"+aid).val(),questionText:traJq("#TT2rawQuestionText-"+traJq("#TT2qid4answer-"+aid).val()).val(),catImgId:traJq("#TT2catImgId").val(),answerSubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})});traJq(".TT2shareReplyBtn").click(function(){var rid=this.id.substring(this.id.lastIndexOf("-")+1);showReplySharePanel({id:rid,replyText:traJq("#TT2rawReplyText-"+rid).val(),answerText:traJq("#TT2rawAnswerText-"+traJq("#TT2aid4reply-"+rid).val()).val(),catImgId:traJq("#TT2catImgId").val(),replySubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})});traJq(".TT2answerReply").click(function(){this.style.color="#777777";var qarid=this.id.substring(this.id.indexOf("-"));traJq("#TT2answersBlock"+qarid).show();traJq("#TT2answerBtns"+qarid).show()});traJq("#TT2dismissSubmitted").click(function(){traJq("#TT2questionSubmitted").hide()});traJq("#TT2dismissNotConfirmed").click(function(){traJq("#TT2questionSubmittedNotConfirmed").hide()});traJq("#TT2resendConfirmation").click(function(){for(var i=0;i<TurnTo.matchData.user.uIdents.length;i++){if(TurnTo.matchData.user.uIdents[i].type==1&&!TurnTo.matchData.user.uIdents[i].confirmed){resendEmailConfirmation(TurnTo.matchData.user.uIdents[i].ident)}}});traJq("#TT2askQuestionTips").click(function(){traJq("#TT2askQuestionExamples").toggle();traJq("#TT2askQuestionTipsExpandImg").toggle();traJq("#TT2askQuestionTipsShrinkImg").toggle()});if(match.reply){traJq("#TT2replySubmitted").show()}traJq("#TT2dismissReplySubmitted").click(function(){traJq("#TT2replySubmitted").hide()});if(match.hideQuestionInput){traJq("#TT2showQuestionBox").show();traJq("#TT2questionSection").hide();traJq("#TT2whatShouldAsk").hide()}traJq("#TT2showQuestionBoxLink").click(function(){traJq("#TT2showQuestionBox").hide();traJq("#TT2questionSection").show();traJq("#TT2whatShouldAsk").show()});if(!TurnTo.matchData.user){traJq("#TT2twitterBox").hide();traJq("#TT2twitterImg").hide();traJq("#TTsignUpLink3").hide()}else{if(!TurnTo.matchData.user.hasPhoto){addQuestionWidgetMessage({screen:"noUserPhotoMessage"});traJq("#TT2AddUserPhoto").click(function(){displayUserSetting(function(){insertScreen("#TTSubScreen",TurnToHTML.userSetting,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl});toggleSubWindow("block");insertScreen("#contactInfo",TurnToHTML.userSettingEmailAddresses,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl});var traHeight=TurnTojQuery("#TTmainContent").height();traJq("#TT2userSetting").css({maxHeight:traHeight-50+"px"});if(TurnTojQuery.browser.msie){traJq("#TT2userSetting").css({height:"auto"})}setupUserSettingAction();showPhotoEditor()})})}}if(match.iteaserClick||hideAskBackBtnOnce){traJq("#TT2goToMatches").hide();hideAskBackBtnOnce=false}else{traJq("#TT2goToMatches").click(function(){insertScreen("#TTtraLayout",'<div id="TT-spinner" style="text-align:center;padding-top:60px;padding-bottom:50px;"></div>');TurnTojQuery(".TT2sectionHeader").show();setTimeout(function(){if(!TurnTo.traDataLoaded){getMatches(populateTra)}else{populateTra(TurnTo.matchData)}},50);if(!TurnTo.matchData.user){traJq("#TTsignUpLink3").show()}})}if(turnToConfig.removeTurnToLogo){traJq("#TTtraFooterLogo").hide()}else{traJq("#TTtraFooterLogo").click(function(e){window.open(TurnTo.partnerProtocol+"//"+turnToSettings.host)})}setupQuestionSubmit();traJq("#TT2facebookBox").click(function(e){if(this.checked){this.disabled=true;if(!TurnTo.matchData.user||!TurnTo.matchData.user.fbPostToWallGranted){connectWithFacebook(null,{regSource:TurnTo.regSourceAskQuestion,post:true,photo:true,link:(TurnTo.matchData.user?true:false),noMatchesShow:true,onConnectClose:function(){traJq("#TT2facebookBox").removeAttr("disabled");if(TurnTo.matchData.user){getFbPostToWallGranted(function(){if(!TurnTo.matchData.user.fbPostToWallGranted){traJq("input[name=TT2facebookBox]").attr("checked",false)}else{traJq("input[name=TT2facebookBox]").attr("checked",true)}});if(TurnTo.matchData.user&&traJq("#TTtraDialogTitleBar").length>0){change2LiTitleBarFromLo()}}else{traJq("input[name=TT2facebookBox]").attr("checked",false)}}})}else{traJq("#TT2facebookBox").removeAttr("disabled")}if(window.event){window.event.returnValue=true}}});traJq("#TT2twitterBox").click(function(e){if(this.checked){if(!TurnTo.matchData.user){return}this.disabled=true;if(findUserRaveTarget(TurnTo.twitterTargetId)==-1){if(!TurnTo.matchData.user.raveTargets){getUserRaveTargets(function(){var foundTwitterTarget=findUserRaveTarget(TurnTo.twitterTargetId)>-1;if(!foundTwitterTarget){traJq("#TT2twitterBox").attr("disabled",true);connectWithTwitter(null,{onConnectClose:function(){traJq("#TT2twitterBox").removeAttr("disabled");getUserRaveTargets(function(){var foundTwitterTarget=findUserRaveTarget(TurnTo.twitterTargetId)>-1;if(!foundTwitterTarget){traJq("input[name=TT2twitterBox]").attr("checked",false)}else{traJq("input[name=TT2twitterBox]").attr("checked",true)}})}})}else{traJq("input[name=TT2twitterBox]").attr("checked",true);traJq("#TT2twitterBox").removeAttr("disabled")}})}else{connectWithTwitter(null,{onConnectClose:function(){traJq("#TT2twitterBox").removeAttr("disabled");getUserRaveTargets(function(){var foundTwitterTarget=findUserRaveTarget(TurnTo.twitterTargetId)>-1;if(!foundTwitterTarget){traJq("input[name=TT2twitterBox]").attr("checked",false)}else{traJq("input[name=TT2twitterBox]").attr("checked",true)}})}})}}else{traJq("#TT2twitterBox").removeAttr("disabled")}if(window.event){window.event.returnValue=true}}});traJq(".TT2itemLink").click(function(){var matchLinkRoot=getSessionUrl("/traServer2/logClickEvent","siteKey="+turnToSettings.siteKey+"&v="+TurnTo.version+"&curl="+encodeURIComponent(document.URL));var catItmId=this.id.substring(this.id.lastIndexOf("-")+1);window.location=matchLinkRoot+"&catItemId="+catItmId});traJq("textarea[name^=TT2inlineAnswer-]").keyup(function(){var qaid=this.id.substring(this.id.indexOf("-"));var charLeft=2000-this.value.length;if(charLeft<=200){if(charLeft>=0){traJq("#TT2anwerCharCount"+qaid)[0].style.color="black";traJq("#TT2answerBtn"+qaid).show()}else{traJq("#TT2anwerCharCount"+qaid)[0].style.color="red";traJq("#TT2answerBtn"+qaid).hide()}traJq("#TT2anwerCharCount"+qaid).html(charLeft+" characters left")}else{traJq("#TT2anwerCharCount"+qaid).html("");traJq("#TT2answerBtn"+qaid).show()}});traJq(".TT2cancelBtn").click(function(){var qarid=this.id.substring(this.id.indexOf("-"));var reply=qarid.indexOf("-")!=qarid.lastIndexOf("-");traJq("#TT2inlineAnswer"+qarid).val("");inputLoseFocus(traJq("#TT2inlineAnswer"+qarid)[0],hideInlineAnswerInput);traJq("#TT2answerReply"+qarid).css("color","#314C9B");if(reply){traJq("#TT2answersBlock"+qarid).hide();traJq("#TT2answerBtns"+qarid).hide()}traJq("#TT2error"+qarid).html("");traJq("#TT2anwerCharCount"+qarid).html("")});traJq(".TT2answerBtn").click(function(){var qarid=this.id.substring(this.id.indexOf("-")+1);var ids=qarid.split("-");var qid=ids[0];if(ids.length>1){var aid=ids[1];var rid=ids[2];submitAnswerReply(qid,aid,rid)}else{submitInlineAnswer(qid)}});if(callback){callback()}})}function showThankAnswerWidget(cmd){dropLocalLoginCookie();getAnswerData(cmd,function(data,ioArgs){toggleMainWindow("none");traJq("#TT-spinner").hide();traJq(".TT2sectionHeader").hide();if(traJq("#TTtraDialogTitleBar2")&&traJq("#TTtraDialogTitleBar2").length>0){traJq("#TTtraDialogTitleBar2").attr("id","TTtraDialogTitleBar");traJq("#TTtraDialogTitleBar").css("height","32px");TurnTojQuery("#TTtraWindowClose").css("margin-top","-18px")}else{traJq("#TTsignUpLink3").hide()}if(data.question.removed){showQuestionRemovedMsg();return}else{if(data.answer.removed){showAnswerRemovedMsg();return}}insertScreen("#TTtraLayout",TurnToHTML.thankAnswerWidget,{question:data.question,answer:data.answer,catItem:data.catItem});traJq("#TT2footerFeedback").hide();if(turnToConfig.removeTurnToLogo){traJq("#TTtraFooterLogo").hide()}else{traJq("#TTtraFooterLogo").click(function(e){window.open(TurnTo.partnerProtocol+"//"+turnToSettings.host)})}traJq("#TT2answerBtn-"+data.question.id+"-"+data.answer.id+"-"+data.answer.uid).click(function(){submitAnswerReply(traJq("#TT2replyqid").val(),traJq("#TT2replyaid").val(),traJq("#TT2replyuid").val(),function(replyData,replyArgs){showQuestionWidget({catItmId:data.catItem.id,hideQuestionInput:true,reply:true},function(){showReplySharePanel({id:replyData.reply.id,replyText:traJq("#TT2rawReplyText-"+replyData.reply.id).val(),answerText:traJq("#TT2rawAnswerText-"+traJq("#TT2aid4reply-"+replyData.reply.id).val()).val(),catImgId:traJq("#TT2catImgId").val(),replySubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})})})});traJq("textarea[name^=TT2inlineAnswer-]").keyup(function(){var qaid=this.id.substring(this.id.indexOf("-"));var charLeft=2000-this.value.length;if(charLeft<=200){if(charLeft>=0){traJq("#TT2anwerCharCount"+qaid)[0].style.color="black";traJq("#TT2answerBtn"+qaid).show()}else{traJq("#TT2anwerCharCount"+qaid)[0].style.color="red";traJq("#TT2answerBtn"+qaid).hide()}traJq("#TT2anwerCharCount"+qaid).html(charLeft+" characters left")}else{traJq("#TT2anwerCharCount"+qaid).html("");traJq("#TT2answerBtn"+qaid).show()}});traJq(".TT2itemLink").click(function(){var matchLinkRoot=getSessionUrl("/traServer2/logClickEvent","siteKey="+turnToSettings.siteKey+"&v="+TurnTo.version+"&curl="+encodeURIComponent(document.URL));var catItmId=this.id.substring(this.id.lastIndexOf("-")+1);window.location=matchLinkRoot+"&catItemId="+catItmId})})}function getAnswerData(cmd,callbackF){xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getAnswerData")+("&aid="+cmd.aid+"&uid="+cmd.uid),load:function(data,ioArgs){if(data.user){TurnTo.matchData.user=data.user;TurnTo.loggedIn=true;setLocalLoginCookie(data)}if(callbackF){callbackF(data,ioArgs)}},error:function(data,ioArgs){traJq("#error").html(data.error);var message="getAnswerData XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function showReplyWidget(cmd){dropLocalLoginCookie();getReplyData(cmd,function(data,ioArgs){toggleMainWindow("none");traJq("#TT-spinner").hide();traJq(".TT2sectionHeader").hide();if(traJq("#TTtraDialogTitleBar2")&&traJq("#TTtraDialogTitleBar2").length>0){traJq("#TTtraDialogTitleBar2").attr("id","TTtraDialogTitleBar");traJq("#TTtraDialogTitleBar").css("height","32px");TurnTojQuery("#TTtraWindowClose").css("margin-top","-18px")}else{traJq("#TTsignUpLink3").hide()}if(data.question.removed){showQuestionRemovedMsg();return}else{if(data.answer.removed){showAnswerRemovedMsg();return}else{if(data.reply.removed){showReplyRemovedMsg();return}}}insertScreen("#TTtraLayout",TurnToHTML.replyWidget,{question:data.question,answer:data.answer,reply:data.reply,replys:data.replys,catItem:data.catItem});traJq("#TT2footerFeedback").hide();if(turnToConfig.removeTurnToLogo){traJq("#TTtraFooterLogo").hide()}else{traJq("#TTtraFooterLogo").click(function(e){window.open(TurnTo.partnerProtocol+"//"+turnToSettings.host)})}traJq("#TT2answerBtn-"+data.question.id+"-"+data.answer.id+"-"+data.reply.uid).click(function(){submitAnswerReply(traJq("#TT2replyqid").val(),traJq("#TT2replyaid").val(),traJq("#TT2replyuid").val(),function(replyData,replyArgs){showQuestionWidget({catItmId:data.catItem.id,hideQuestionInput:true,reply:true},function(){showReplySharePanel({id:replyData.reply.id,replyText:traJq("#TT2rawReplyText-"+replyData.reply.id).val(),answerText:traJq("#TT2rawAnswerText-"+traJq("#TT2aid4reply-"+replyData.reply.id).val()).val(),catImgId:traJq("#TT2catImgId").val(),replySubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})})})});traJq("textarea[name^=TT2inlineAnswer-]").keyup(function(){var qaid=this.id.substring(this.id.indexOf("-"));var charLeft=2000-this.value.length;if(charLeft<=200){if(charLeft>=0){traJq("#TT2anwerCharCount"+qaid)[0].style.color="black";traJq("#TT2answerBtn"+qaid).show()}else{traJq("#TT2anwerCharCount"+qaid)[0].style.color="red";traJq("#TT2answerBtn"+qaid).hide()}traJq("#TT2anwerCharCount"+qaid).html(charLeft+" characters left")}else{traJq("#TT2anwerCharCount"+qaid).html("");traJq("#TT2answerBtn"+qaid).show()}});traJq(".TT2itemLink").click(function(){var matchLinkRoot=getSessionUrl("/traServer2/logClickEvent","siteKey="+turnToSettings.siteKey+"&v="+TurnTo.version+"&curl="+encodeURIComponent(document.URL));var catItmId=this.id.substring(this.id.lastIndexOf("-")+1);window.location=matchLinkRoot+"&catItemId="+catItmId})})}function getReplyData(cmd,callbackF){xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getReplyData")+("&aid="+cmd.aid+"&rid="+cmd.rid+"&uid="+cmd.uid),load:function(data,ioArgs){if(data.user){TurnTo.matchData.user=data.user;TurnTo.loggedIn=true;setLocalLoginCookie(data)}if(callbackF){callbackF(data,ioArgs)}},error:function(data,ioArgs){traJq("#error").html(data.error);var message="getReplyData XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function submitAnswerReply(qid,aid,rid,callbackF){var qaid=qid+"-"+aid+"-"+rid;var replyText=stripString(traJq("#TT2inlineAnswer-"+qaid).val());var replyToUid=traJq("#TT2replyToUid-"+qaid).val();traJq("#TT2imgLoading-"+qaid).show();traJq("#TT2answerBtn-"+qaid).hide();var data={};data.reply=replyText;data.aid=aid;data.replyToUid=replyToUid;data.notify=traJq("#TT2answerNotify-"+qaid).attr("checked");xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/traServer2/replyAnswer"),postData:toJson(data),contentType:"text/text",load:function(data,ioArgs){if(callbackF){callbackF(data,ioArgs)}else{traJq("#TT2imgLoading-"+qaid).hide();traJq("#TT2answerBtn-"+qaid).show();traJq("#TT2cancelBtn-"+qaid).click();traJq("#TT2lastAnswerblock-"+aid).before(TurnToHTML.replyBlock({qid:qid,aid:aid,showReplyLink:false,reply:data.reply,replyUid:null}));traJq("#TT2shareReplyBtn-"+data.reply.id).click(function(){showReplySharePanel({id:data.reply.id,replyText:traJq("#TT2rawReplyText-"+data.reply.id).val(),answerText:traJq("#TT2rawAnswerText-"+traJq("#TT2aid4reply-"+data.reply.id).val()).val(),catImgId:traJq("#TT2catImgId").val(),replySubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})});showReplySharePanel({id:data.reply.id,replyText:traJq("#TT2rawReplyText-"+data.reply.id).val(),answerText:traJq("#TT2rawAnswerText-"+traJq("#TT2aid4reply-"+data.reply.id).val()).val(),catImgId:traJq("#TT2catImgId").val(),replySubmitted:true,catImgProcessed:traJq("#TT2catImgProcessed").val()});traJq("#TT2replySubmitted-"+data.reply.id).show();traJq("#TT2dismissReplied-"+data.reply.id).click(function(){var rid=this.id.substring(this.id.lastIndexOf("-")+1);traJq("#TT2replySubmitted-"+rid).remove()})}},error:function(data,ioArgs){traJq("#TT2imgLoading-"+qaid).hide();traJq("#TT2answerBtn-"+qaid).show();traJq("#TT2error-"+qaid).html(data.error);var message="subimtAnswerReply XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function isSiteManager(userSiteRoles){if(userSiteRoles==null){return false}for(var i=0;i<userSiteRoles.length;i++){if(userSiteRoles[i]==TurnTo.siteManager){return true}}return false}function isSiteModerator(userSiteRoles){if(userSiteRoles==null){return false}for(var i=0;i<userSiteRoles.length;i++){if(userSiteRoles[i]==TurnTo.siteModerator){return true}}return false}function showInlineAnswerInput(inputElJQ){var id=inputElJQ.attr("id");var qid=id.substring(id.lastIndexOf("-")+1);inputElJQ.height("");inputElJQ.attr("rows","6");traJq("#TT2answerBtns-"+qid).show();traJq("#TT2answersBlock-"+qid).height("");traJq("#TT2answersBlock-"+qid).css("padding","6px 8px 6px 6px")}function hideInlineAnswerInput(inputElJQ){var id=inputElJQ.attr("id");var qid=id.substring(id.lastIndexOf("-")+1);inputElJQ.height("20px");inputElJQ.attr("rows","1");traJq("#TT2answerBtns-"+qid).hide();traJq("#TT2answersBlock-"+qid).height("26px");traJq("#TT2answersBlock-"+qid).css("padding","")}function showRegFlow4InlineAnswer(qid){TurnTojQuery("#TTmainContent").height((TurnTojQuery(window).height()-100)+"px");insertScreen("#TTSubScreen",TurnToHTML.inlineAnswerRegFlow,{catItmId:traJq("#TT2catItmId").val(),iid:traJq("#TT2iid").val(),regSource:qid?TurnTo.regSourceAskAnswer:TurnTo.regSourceAskQuestion});toggleSubWindow("block");resizeSubWindow("TT2questionRegFlow");traJq("#TT2login").click(function(){toggleSubWindow("none");showUserLogin();traJq("#TTfbConnectLogin")[0].onclick=function(e){traJq("#TTloginSubmit").hide();traJq("#TTloginSpinner").show();connectWithFacebook(e,{onConnectClose:function(e){if(TurnTo.loggedIn){if(qid){submitInlineAnswer(qid)}else{submitItemQuestion()}change2LiTitleBarFromLo();toggleBubbleWindow("none")}else{traJq("#TTloginSubmit").show();traJq("#TTloginSpinner").hide()}},photo:true,noMatchesShow:true,regSource:TurnTo.regSourceAskQuestion,keepBubbleWindow:true})};TurnTo.loginForm.onsubmit=function(e){traJq("#TTloginSubmit").hide();traJq("#TTloginSpinner").show();enableLogin(false);if(TurnTo.loginAtTTInterval){clearInterval(TurnTo.loginAtTTInterval)}var auth={};auth.username=traJq("#TTloginUsername")[0].value;auth.password=traJq("#TTloginPassword")[0].value;if(traJq("#TTloginRememberMe").length>0&&traJq("#TTloginRememberMe")[0].checked){auth._spring_security_remember_me="true"}xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/secure/traServer2/loginGetMatches"),content:auth,load:function(data,ioArgs){if(qid){loginOnSubmitAnswer(data,ioArgs,qid)}else{loginOnSubmitQuestion(data,ioArgs)}},error:function(data,ioArgs){console.dir(data);if(data.status==402){TurnTo.loginMessage.innerHTML='<span class="TTred">Your Email and Password did not match.</span>'}else{if(data.status==407){TurnTo.loginMessage.innerHTML='<span class="TTred" style="font-weight:100;font-size:10px;">Your email address is unconfirmed. Please check the email we sent you to finish your registration.</span>'}else{console.debug("doLogin status: ",data.status,data.error);TurnTo.loginMessage.innerHTML="Timeout - please try again"}}TurnTo.loginSubmit.disabled=false;traJq("#TTloginSubmit").show();traJq("#TTloginSpinner").hide()}});stopDefault(e);return false}});var subSpinnerDes="Please wait while we are connecting you to submit the "+(qid?"answer":"question")+"...";traJq("#TT2askDelAuthLink1").click(function(e){connectWithFacebook(e,{onConnectClose:function(e){if(TurnTo.loggedIn){if(qid){submitInlineAnswer(qid)}else{submitItemQuestion()}change2LiTitleBarFromLo()}else{showRegFlow4InlineAnswer(qid)}},photo:true,noMatchesShow:true,regSource:TurnTo.regSourceAskQuestion,keepSubwindow:true,subSpinnerDes:subSpinnerDes,showSubSpinner:true})});traJq(".TT2delAuth").click(function(){traJq(".TT2delAuth").css("background-color","");this.style.backgroundColor="#fefeab";var domain=this.innerHTML.replace(/^\s+|\s+$/g,"");TurnTo.delAuthWin=delAuthImport(domain,TurnTo.regSourceAskQuestion);if(TurnTo.delAuthPopupInterval){clearInterval(TurnTo.delAuthPopupInterval)}TurnTo.delAuthPopupInterval=setInterval(function(){if(TurnTo.delAuthWin.closed){traJq(".TT2delAuth").css("background-color","");insertScreen("#TTSubScreen",TurnToHTML.subSpinScreen,{description:subSpinnerDes});clearInterval(TurnTo.delAuthPopupInterval);getMatchesAfterDelAuthImport(function(data,ioArgs){if(data.user){if(qid){loginOnSubmitAnswer(data,ioArgs,qid)}else{loginOnSubmitQuestion(data,ioArgs)}}else{showRegFlow4InlineAnswer(qid)}},true)}},750)});traJq("#TTregForm")[0].onsubmit=function(e){doSubmitRegForm2(e,function(data,ioArgs){if(qid){loginOnSubmitAnswer(data,ioArgs,qid)}else{loginOnSubmitQuestion(data,ioArgs)}setLocalLoginCookie(data);TurnToTrack.newUserRegistration(TurnTo.matchData.user.id)})};traJq("#TT2noImport").click(function(){traJq("#TT2questionReg").show();traJq("#TT2noImport").html("The old-fashioned way:");traJq("#TT2closeForm").show();resizeSubWindow("TT2questionRegFlow")});traJq("#TT2closeForm").click(function(){traJq("#TT2closeForm").hide();traJq("#TT2noImport").html("I don't use any of these");traJq("#TT2questionReg").hide();resizeSubWindow("TT2questionRegFlow")})}function showRegFlow4Question(qid){TurnTojQuery("#TTmainContent").height((TurnTojQuery(window).height()-100)+"px");insertScreen("#TTSubScreen",TurnToHTML.questionRegFlow,{catItmId:traJq("#TT2catItmId").val(),iid:traJq("#TT2iid").val(),regSource:qid?TurnTo.regSourceAskAnswer:TurnTo.regSourceAskQuestion});toggleSubWindow("block");resizeSubWindow("TT2questionRegFlow");traJq("#TT2login").click(function(){toggleSubWindow("none");showUserLogin();traJq("#TTfbConnectLogin")[0].onclick=function(e){traJq("#TTloginSubmit").hide();traJq("#TTloginSpinner").show();connectWithFacebook(e,{onConnectClose:function(e){if(TurnTo.loggedIn){if(qid){submitInlineAnswer(qid)}else{submitItemQuestion()}change2LiTitleBarFromLo();toggleBubbleWindow("none")}else{traJq("#TTloginSubmit").show();traJq("#TTloginSpinner").hide()}},photo:true,noMatchesShow:true,regSource:TurnTo.regSourceAskQuestion,keepBubbleWindow:true})};TurnTo.loginForm.onsubmit=function(e){traJq("#TTloginSubmit").hide();traJq("#TTloginSpinner").show();enableLogin(false);if(TurnTo.loginAtTTInterval){clearInterval(TurnTo.loginAtTTInterval)}var auth={};auth.username=traJq("#TTloginUsername")[0].value;auth.password=traJq("#TTloginPassword")[0].value;if(traJq("#TTloginRememberMe").length>0&&traJq("#TTloginRememberMe")[0].checked){auth._spring_security_remember_me="true"}xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/secure/traServer2/loginGetMatches"),content:auth,load:function(data,ioArgs){if(qid){loginOnSubmitAnswer(data,ioArgs,qid)}else{loginOnSubmitQuestion(data,ioArgs)}},error:function(data,ioArgs){console.dir(data);if(data.status==402){TurnTo.loginMessage.innerHTML='<span class="TTred">Your Email and Password did not match.</span>'}else{if(data.status==407){TurnTo.loginMessage.innerHTML='<span class="TTred" style="font-weight:100;font-size:10px;">Your email address is unconfirmed. Please check the email we sent you to finish your registration.</span>'}else{console.debug("doLogin status: ",data.status,data.error);TurnTo.loginMessage.innerHTML="Timeout - please try again"}}TurnTo.loginSubmit.disabled=false;traJq("#TTloginSubmit").show();traJq("#TTloginSpinner").hide()}});stopDefault(e);return false}});var subSpinnerDes="Please wait while we are connecting you to submit the "+(qid?"answer":"question")+"...";traJq("#TT2askDelAuthLink1").click(function(e){connectWithFacebook(e,{onConnectClose:function(e){if(TurnTo.loggedIn){if(qid){submitInlineAnswer(qid)}else{submitItemQuestion()}change2LiTitleBarFromLo()}else{showRegFlow4Question(qid)}},photo:true,noMatchesShow:true,regSource:TurnTo.regSourceAskQuestion,keepSubwindow:true,subSpinnerDes:subSpinnerDes,showSubSpinner:true})});traJq(".TT2delAuth").click(function(){traJq(".TT2delAuth").css("background-color","");this.style.backgroundColor="#fefeab";var domain=this.innerHTML.replace(/^\s+|\s+$/g,"");TurnTo.delAuthWin=delAuthImport(domain,TurnTo.regSourceAskQuestion);if(TurnTo.delAuthPopupInterval){clearInterval(TurnTo.delAuthPopupInterval)}TurnTo.delAuthPopupInterval=setInterval(function(){if(TurnTo.delAuthWin.closed){traJq(".TT2delAuth").css("background-color","");insertScreen("#TTSubScreen",TurnToHTML.subSpinScreen,{description:subSpinnerDes});clearInterval(TurnTo.delAuthPopupInterval);getMatchesAfterDelAuthImport(function(data,ioArgs){if(data.user){if(qid){loginOnSubmitAnswer(data,ioArgs,qid)}else{loginOnSubmitQuestion(data,ioArgs)}}else{showRegFlow4Question(qid)}},true)}},750)});traJq("#TTregForm")[0].onsubmit=function(e){doSubmitRegForm2(e,function(data,ioArgs){if(qid){loginOnSubmitAnswer(data,ioArgs,qid)}else{loginOnSubmitQuestion(data,ioArgs)}setLocalLoginCookie(data);TurnToTrack.newUserRegistration(TurnTo.matchData.user.id)})};traJq("#TT2noImport").click(function(){traJq("#TT2questionReg").show();traJq("#TT2noImport").html("The old-fashioned way:");traJq("#TT2closeForm").show();resizeSubWindow("TT2questionRegFlow")});traJq("#TT2closeForm").click(function(){traJq("#TT2closeForm").hide();traJq("#TT2noImport").html("I don't use any of these");traJq("#TT2questionReg").hide();resizeSubWindow("TT2questionRegFlow")})}function showRegFlow4AnonymousQuestion(cmd){var qid=cmd.id;TurnTojQuery("#TTmainContent").height((TurnTojQuery(window).height()-100)+"px");insertScreen("#TTSubScreen",TurnToHTML.questionRegFlow,{catItmId:traJq("#TT2catItmId").val(),iid:traJq("#TT2iid").val(),regSource:TurnTo.regSourceAskQuestion});toggleSubWindow("block");resizeSubWindow("TT2questionRegFlow");traJq("#TT2questionRegRadio1").click(function(){traJq(".TT2questionRegSel1").show();traJq(".TT2questionRegSel2").hide();resizeSubWindow("TT2questionRegFlow")});traJq("#TT2questionRegRadio2").click(function(){traJq(".TT2questionRegSel1").hide();traJq(".TT2questionRegSel2").show();resizeSubWindow("TT2questionRegFlow")});traJq("#TT2questionRegClose").click(function(){toggleSubWindow("none");showQuestionSharePanel(cmd)});traJq("#TT2register").click(function(){traJq("#TT2questionReg").show();traJq("#TT2questionLogin").hide()});traJq("#TT2login").click(function(){traJq("#TT2questionLogin").show();traJq("#TT2questionReg").hide();traJq("#TTloginForgotPwd")[0].onclick=function(e){traJq("#TT2questionForgotPwd").show();traJq("#TT2questionLogin").hide();traJq("#TTresetPwdBtn")[0].onclick=function(e){resetPwd4QuestionLogin()};traJq("#TTforgotPwdForm")[0].onsubmit=function(e){resetPwd4QuestionLogin();return false}};traJq("#TTloginForm")[0].onsubmit=function(e){traJq("#TTloginSubmit").hide();traJq("#TTloginSpinner").show();if(TurnTo.loginAtTTInterval){clearInterval(TurnTo.loginAtTTInterval)}var auth={};auth.username=traJq("#TTloginUsername")[0].value;auth.password=traJq("#TTloginPassword")[0].value;if(traJq("#TTloginRememberMe").length>0&&traJq("#TTloginRememberMe")[0].checked){auth._spring_security_remember_me="true"}xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/secure/traServer2/loginGetMatches"),content:auth,load:function(data,ioArgs){updateQuestionOwner(qid,function(){showQuestionSharePanel(cmd)});loginOnAnonymousQuestion(qid,data,ioArgs)},error:function(data,ioArgs){console.dir(data);if(data.status==402){traJq("#TTloginMessage").html('<span class="TTred">Your Email and Password did not match.</span>')}else{if(data.status==407){traJq("#TTloginMessage").html('<span class="TTred" style="font-weight:100;font-size:10px;">Your email address is unconfirmed. Please check the email we sent you to finish your registration.</span>')}else{console.debug("doLogin status: ",data.status,data.error);traJq("#TTloginMessage").html("Timeout - please try again")}}traJq("#TTloginSubmit")[0].disabled=false;traJq("#TTloginSubmit").show();traJq("#TTloginSpinner").hide()}});stopDefault(e);return false}});var subSpinnerDes="Please wait while we are connecting you to submit the question...";traJq("#TT2askDelAuthLink1").click(function(e){connectWithFacebook(e,{onConnectClose:function(e){if(TurnTo.loggedIn){updateQuestionOwner(qid,function(){showQuestionSharePanel(cmd)});change2LiTitleBarFromLo();toggleSubWindow("none")}else{showRegFlow4AnonymousQuestion(cmd)}},photo:true,noMatchesShow:true,regSource:TurnTo.regSourceAskQuestion,keepSubwindow:true,subSpinnerDes:subSpinnerDes,showSubSpinner:true})});traJq(".TT2delAuth").click(function(){traJq(".TT2delAuth").css("background-color","");this.style.backgroundColor="#fefeab";var domain=this.innerHTML.replace(/^\s+|\s+$/g,"");TurnTo.delAuthWin=delAuthImport(domain,TurnTo.regSourceAskQuestion);if(TurnTo.delAuthPopupInterval){clearInterval(TurnTo.delAuthPopupInterval)}TurnTo.delAuthPopupInterval=setInterval(function(){if(TurnTo.delAuthWin.closed){traJq(".TT2delAuth").css("background-color","");insertScreen("#TTSubScreen",TurnToHTML.subSpinScreen,{description:subSpinnerDes});clearInterval(TurnTo.delAuthPopupInterval);getMatchesAfterDelAuthImport(function(data,ioArgs){if(data.user){updateQuestionOwner(qid,function(){showQuestionSharePanel(cmd)});loginOnAnonymousQuestion(qid,data,ioArgs)}else{showRegFlow4AnonymousQuestion(cmd)}},true)}},750)});traJq("#TTregForm")[0].onsubmit=function(e){doSubmitRegForm2(e,function(data,ioArgs){updateQuestionOwner(qid,function(){showQuestionSharePanel(cmd)});loginOnAnonymousQuestion(qid,data,ioArgs);setLocalLoginCookie(data);TurnToTrack.newUserRegistration(TurnTo.matchData.user.id)})};traJq("#TT2noImport").click(function(){traJq("#TT2questionReg").show();traJq("#TT2noImport").html("The old-fashioned way:");traJq("#TT2closeForm").show();resizeSubWindow("TT2questionRegFlow")});traJq("#TT2closeForm").click(function(){traJq("#TT2closeForm").hide();traJq("#TT2noImport").html("I don't use any of these");traJq("#TT2questionReg").hide();resizeSubWindow("TT2questionRegFlow")})}function resetPwd4QuestionLogin(){TurnTo.error=null;var email=TurnTojQuery.trim(traJq("#TT2forgotLoginUsername")[0].value);if(email.length==0||email==traJq("#TT2forgotLoginUsername").attr("ttInputLabelText")){traJq("#TTresetPwdMessage")[0].innerHTML="Please enter the email address for your account.";return}xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/resetPassword")+"&email="+encodeURIComponent(traJq("#TT2forgotLoginUsername")[0].value),load:function(data,ioArgs){traJq("#TT2questionForgotPwd").hide();traJq("#TT2questionLogin").show();traJq("#TTloginMessage")[0].innerHTML=data.message},error:function(data,ioArgs){var message="resetPassword XHR - Status: "+data.status+" Error: "+data.error;console.debug(message);traJq("#TTresetPwdMessage")[0].innerHTML=data.error},timeout:turnToSettings.timeout})}function updateQuestionOwner(qid,callback){var p={};p.qid=qid;xhr("POST",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/updateQuestionOwner"),postData:toJson(p),contentType:"text/text",load:function(data,ioArgs){if(data.uid){traJq(".TT2youPhotoImg").attr("src",getUserPhoto(TurnTo.matchData.user.id,TurnTo.matchData.user.pid));traJq(".TT2youCheckAnswers").html("We'll email you as soon as someone answers.");if(TurnTo.matchData.user.isPreUser){traJq("#TT2questionSubmitted").hide();traJq("#TT2questionSubmittedNotConfirmed").show()}if(callback){callback(data)}}}})}function loginOnAnonymousQuestion(qid,data,ioArgs){if(!processResponse(data,ioArgs)){return}toggleBubbleWindow("none");toggleSubWindow("none");change2LiTitleBarFromLo()}function loginOnSubmitQuestion(data,ioArgs){if(!processResponse(data,ioArgs)){return}toggleBubbleWindow("none");toggleSubWindow("none");submitItemQuestion();change2LiTitleBarFromLo()}function loginOnSubmitAnswer(data,ioArgs,qid){if(!processResponse(data,ioArgs)){return}toggleBubbleWindow("none");toggleSubWindow("none");submitInlineAnswer(qid);change2LiTitleBarFromLo()}function change2LiTitleBarFromLo(){traJq("#TTtraDialogTitleBar").attr("id","TTtraDialogTitleBar2");traJq("#TTtraDialogTitleBar2").html('<div class="TT2topbarLI" style="padding-top:20px;padding-left:0px;padding-right:10px;width:670px;float:right;"><span id="TTtraDialogTitle" unselectable="on" style="-moz-user-select: none;"></span><a id="TTtraWindowClose" href="javascript:void(0)" role="button" unselectable="on" style="margin-top:-33px;height:25px;width:25px"></a><span id="TTtraDialogFirstName" style=""></span><span class="TT2verticalSeparator">|</span><a id="TTaddFriendsHdrLink" href="javascript:void(0)" class="TTexternalLink" style="font-size:11px;display:none;float:left">Check for more friends</a><span class="TT2verticalSeparator">|</span><a id="TT2userSettingLink" href="javascript:void(0)" class="TTexternalLink" style="font-size:11px;display:none;float:left">Settings</a><span class="TT2verticalSeparator">|</span><a id="TT2ManagePurchasesLink" href="javascript:void(0)" class="TTexternalLink" style="font-size:11px;display:none;float:left"></a><span id="TT2myAskLinkSeparator" class="TT2verticalSeparator">|</span><a id="TT2myAskLink" href="javascript:void(0)" class="TTexternalLink" style="color:#777777;padding-left:10px;float:left;font-size:11px">My Q&A</a></div>');traJq("#TTtraDialogTitleBar2").css("height","");resize();populateLiTitleBar()}function populateLiTitleBar(){insertScreen("#TTtraDialogFirstName",TurnToHTML.titlebarLoggedIn,null);TurnTojQuery("#TTtraWindowClose")[0].onclick=toggleMainWindow;if(TurnTo.matchData.site.ask){traJq("#TT2myAskLink").show()[0].onclick=function(e){showMyAsk()};traJq("#TT2myAskLinkSeparator").show();var marginLeft=parseInt(traJq(".TT2verticalSeparator").css("margin-left").replace("px",""))-8;var marginRight=parseInt(traJq(".TT2verticalSeparator").css("margin-right").replace("px",""))-8;traJq(".TT2verticalSeparator").css({marginLeft:marginLeft+"px",marginRight:marginRight+"px"})}traJq("#TTaddFriendsHdrLink").show()[0].onclick=function(e){getImportHistory(displayImportSourceSelectScreen)};var mpLink=traJq("#TT2ManagePurchasesLink").show();if(TurnTo.matchData.counts.userItems>0){mpLink[0].onclick=function(e){getItems4User(displayPastPurchases)};mpLink.html("My Purchases ("+TurnTo.matchData.counts.userItems+")")}else{mpLink[0].onclick=function(e){getItems4User(displayPastPurchases)};mpLink.html("My Purchases")}var settingLink=traJq("#TT2userSettingLink").show();settingLink.click(function(){displayUserSetting()});var photo=TurnTo.getUserPhoto(TurnTo.matchData.user.id,TurnTo.matchData.user.pid);traJq(".TThi")[0].innerHTML='<img style="display:inline;height:16px;float:left;" src="'+photo+'" height="16px"/><b style="margin-left:10px;float:left">Welcome, '+TurnTo.matchData.user.firstName+"</b>";traJq(".TTfirstName")[0].innerHTML=TurnTo.matchData.user.firstName;updateLogoutLinks(logOut)}function submitItemQuestion(){var rawQuestion=TurnTojQuery.trim(traJq("#TT2questionText").val());var question=stripString(rawQuestion);if(regExpSearchEmail(question)!=-1){traJq("#TT2error").html("Please do not include your email address in your question. You will be able to enter an email on the next page.");return}if(traJq("#TT2questionText").attr("class")=="TTdefaultInput"||question.length==0){traJq("#TT2error").html("Please enter your question below");return}var iid=traJq("#TT2iid").val();var catItmId=traJq("#TT2catItmId").val();traJq("#TT2error").html("");traJq("#TT2questionSubImgLoading").show();traJq("#TT2askOwnersBtn").hide();traJq("#TT2questionText").attr("disabled",true);var data={};data.question=question;data.iid=iid;data.catItmId=catItmId;xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/traServer2/askQuestion4Item"),postData:toJson(data),contentType:"text/text",load:function(data,ioargs){traJq("#TT2questionSubImgLoading").hide();traJq("#TT2askOwnersBtn").show();traJq("#TT2questionText").val("");traJq("#TT2questionText").removeAttr("disabled");var asker={};if(TurnTo.matchData.user){asker.uid=TurnTo.matchData.user.id;asker.pid=TurnTo.matchData.user.pid;asker.fName=TurnTo.matchData.user.firstName;asker.lName=TurnTo.matchData.user.lastName;showQuestionSharePanel({id:data.qid,questionText:rawQuestion,catImgId:traJq("#TT2catImgId").val(),questionSubmitted:true,catImgProcessed:traJq("#TT2catImgProcessed").val()})}else{asker.uid=null;asker.pid=null;asker.fName=null;asker.lName=null;showRegFlow4AnonymousQuestion({id:data.qid,questionText:rawQuestion,catImgId:traJq("#TT2catImgId").val(),questionSubmitted:true,catImgProcessed:traJq("#TT2catImgProcessed").val()})}var questionBlock=traJq("#TT2questionSectionTitle");questionBlock.after(TurnToHTML.questionAnswerBlock({asker:asker,questionText:replaceTrimLineBreaks(question),rawQuestionText:rawQuestion,qid:data.qid,isStaff:data.isStaff}));traJq("#TT2shareQuestionBtn-"+data.qid).click(function(){showQuestionSharePanel({id:data.qid,questionText:traJq("#TT2rawQuestionText-"+data.qid).val(),catImgId:traJq("#TT2catImgId").val(),questionSubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})});if(!questionBlock.is(":visible")){questionBlock.show();traJq(".TT2cqa:first").css("border-bottom","none")}traJq("#TT2answerBtn-"+data.qid).click(function(){var qid=this.id.substring(this.id.lastIndexOf("-")+1);submitInlineAnswer(qid)});traJq("#TT2cancelBtn-"+data.qid).click(function(){var qid=this.id.substring(this.id.lastIndexOf("-")+1);traJq("#TT2inlineAnswer"+qid).val("");inputLoseFocus(traJq("#TT2inlineAnswer"+qid)[0],hideInlineAnswerInput);traJq("#TT2answerReply"+qid).css("color","#314C9B");traJq("#TT2answersBlock"+qid).hide();traJq("#TT2answerBtns"+qid).hide();traJq("#TT2error"+qid).html("");traJq("#TT2anwerCharCount"+qid).html("")});traJq("#TT2questionSection").hide();traJq("#TT2whatShouldAsk").hide();traJq("#TT2showQuestionBox").show();if(TurnTo.matchData.user&&TurnTo.matchData.user.isPreUser){traJq("#TT2questionSubmittedNotConfirmed").show()}else{traJq("#TT2questionSubmitted").show()}traJq("#TT2inlineAnswer-"+data.qid).keyup(function(){var qid=this.id.substring(this.id.lastIndexOf("-")+1);var charLeft=2000-this.value.length;if(charLeft<=200){if(charLeft>=0){traJq("#TT2anwerCharCount-"+qid)[0].style.color="black";traJq("#TT2answerBtn-"+qid).show()}else{traJq("#TT2anwerCharCount-"+qid)[0].style.color="red";traJq("#TT2answerBtn-"+qid).hide()}traJq("#TT2anwerCharCount-"+qid).html(charLeft+" characters left")}else{traJq("#TT2anwerCharCount-"+qid).html("");traJq("#TT2answerBtn-"+qid).show()}})},error:function(data,ioargs){traJq("#TT2questionSubImgLoading").hide();traJq("#TT2askOwnersBtn").show();traJq("#TT2questionText").removeAttr("disabled");var message="submitItemQuestion XHR - Status: "+data.status+" Error: "+data.error;console.debug(message);if(data.error){traJq("#TT2error").html(data.error)}}});logWebEvent2(TurnTo.eventQaQPost,(typeof(TurnToTrack)!="undefined")?"&svid="+TurnToTrack.getSiteVisitId():null);traJq("#TT2anwerCharCount").html("")}function showReplySharePanel(cmd){insertScreen("#TTSubScreen",TurnToHTML.replySharePanel,{replyText:cmd.replyText,answerText:cmd.answerText,catImgId:cmd.catImgId,replySubmitted:cmd.replySubmitted,catImgProcessed:cmd.catImgProcessed});toggleSubWindow("block");if(!TurnTo.matchData.user){traJq("#TT2twitterDiv").hide()}traJq("#TT2shareBtn").click(function(){var replyText=stripString(traJq("#TT2shareTextArea").val());if(replyText.length==0){return}var postFb=traJq("#TT2facebookBox").attr("checked");var postTwitter=traJq("#TT2twitterBox").attr("checked");if(!postFb&&!postTwitter){traJq("#TT2shareError").html("Please select where you want to share this reply.");return}var data={};data.id=cmd.id;data.replyText=replyText;traJq("#TT2shareBtns").hide();traJq("#TT2shareLoading").show();if(postFb){getFacebookPublishPerm({regSource:TurnTo.regSourceAskAnswer,callback:function(){data.postToFacebook=true;if(postTwitter){getTwitterPublishPerm({callback:function(){data.postToTwitter=true;shareReply(data)}})}else{shareReply(data)}}})}if(!postFb&&postTwitter){getTwitterPublishPerm({callback:function(){data.postToTwitter=true;shareReply(data)}})}});traJq("#TT2cancelShareBtn").click(function(){toggleSubWindow("none")})}function showAnswerSharePanel(cmd){insertScreen("#TTSubScreen",TurnToHTML.answerSharePanel,{answerText:cmd.answerText,questionText:cmd.questionText,catImgId:cmd.catImgId,answerSubmitted:cmd.answerSubmitted,catImgProcessed:cmd.catImgProcessed});toggleSubWindow("block");if(!TurnTo.matchData.user){traJq("#TT2twitterDiv").hide()}traJq("#TT2shareBtn").click(function(){var answerText=stripString(traJq("#TT2shareTextArea").val());if(answerText.length==0){return}var postFb=traJq("#TT2facebookBox").attr("checked");var postTwitter=traJq("#TT2twitterBox").attr("checked");if(!postFb&&!postTwitter){traJq("#TT2shareError").html("Please select where you want to share this answer.");return}var data={};data.id=cmd.id;data.answerText=answerText;traJq("#TT2shareBtns").hide();traJq("#TT2shareLoading").show();if(postFb){getFacebookPublishPerm({regSource:TurnTo.regSourceAskAnswer,callback:function(){data.postToFacebook=true;if(postTwitter){getTwitterPublishPerm({callback:function(){data.postToTwitter=true;shareAnswer(data)}})}else{shareAnswer(data)}}})}if(!postFb&&postTwitter){getTwitterPublishPerm({callback:function(){data.postToTwitter=true;shareAnswer(data)}})}});traJq("#TT2cancelShareBtn").click(function(){toggleSubWindow("none")})}function showQuestionSharePanel(cmd){insertScreen("#TTSubScreen",TurnToHTML.questionSharePanel,{questionText:cmd.questionText,catImgId:cmd.catImgId,questionSubmitted:cmd.questionSubmitted,catImgProcessed:cmd.catImgProcessed});toggleSubWindow("block");if(!TurnTo.matchData.user){traJq("#TT2twitterDiv").hide()}traJq("#TT2shareBtn").click(function(){var questionText=stripString(traJq("#TT2shareTextArea").val());if(questionText.length==0){return}var postFb=traJq("#TT2facebookBox").attr("checked");var postTwitter=traJq("#TT2twitterBox").attr("checked");if(!postFb&&!postTwitter){traJq("#TT2shareError").html("Please select where you want to share this question.");return}var data={};data.id=cmd.id;data.questionText=questionText;traJq("#TT2shareBtns").hide();traJq("#TT2shareLoading").show();if(postFb){var wasNotUser=!TurnTo.matchData.user;getFacebookPublishPerm({regSource:TurnTo.regSourceAskQuestion,callback:function(){if(wasNotUser&&TurnTo.matchData.user){traJq("#TT2twitterDiv").show();updateQuestionOwner(cmd.id,function(){data.postToFacebook=true;if(postTwitter){getTwitterPublishPerm({callback:function(){data.postToTwitter=true;shareQuestion(data)}})}else{shareQuestion(data)}})}else{data.postToFacebook=true;if(postTwitter){getTwitterPublishPerm({callback:function(){data.postToTwitter=true;shareQuestion(data)}})}else{shareQuestion(data)}}}})}if(!postFb&&postTwitter){getTwitterPublishPerm({callback:function(){data.postToTwitter=true;shareQuestion(data)}})}});traJq("#TT2cancelShareBtn").click(function(){toggleSubWindow("none")})}function shareReply(data){xhr("POST",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/shareReply"),postData:toJson(data),contentType:"text/text",load:function(data,ioargs){traJq("#TT2shareBtns").show();traJq("#TT2shareLoading").hide();if(data.fbSuccess==false){traJq("#TT2shareError").html("Failed to share the reply to facebook.")}if(data.twitterSuccess==false){traJq("#TT2shareError").html("Failed to share the reply to twitter.")}if(data.success){toggleSubWindow("none")}},error:function(data,ioargs){traJq("#TT2shareError").html("Failed to share the reply.");traJq("#TT2shareBtns").show();traJq("#TT2shareLoading").hide()}})}function shareAnswer(data){xhr("POST",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/shareAnswer"),postData:toJson(data),contentType:"text/text",load:function(data,ioargs){traJq("#TT2shareBtns").show();traJq("#TT2shareLoading").hide();if(data.fbSuccess==false){traJq("#TT2shareError").html("Failed to share the answer to facebook.")}if(data.twitterSuccess==false){traJq("#TT2shareError").html("Failed to share the answer to twitter.")}if(data.success){toggleSubWindow("none")}},error:function(data,ioargs){traJq("#TT2shareError").html("Failed to share the answer.");traJq("#TT2shareBtns").show();traJq("#TT2shareLoading").hide()}})}function shareQuestion(data){xhr("POST",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/shareQuestion"),postData:toJson(data),contentType:"text/text",load:function(data,ioargs){traJq("#TT2shareBtns").show();traJq("#TT2shareLoading").hide();if(data.fbSuccess==false){traJq("#TT2shareError").html("Failed to share the question to facebook.")}if(data.twitterSuccess==false){traJq("#TT2shareError").html("Failed to share the question to twitter.")}if(data.success){toggleSubWindow("none")}},error:function(data,ioargs){traJq("#TT2shareError").html("Failed to share the question.");traJq("#TT2shareBtns").show();traJq("#TT2shareLoading").hide()}})}function getTwitterPublishPerm(cmd){if(traJq("#TT2twitterBox").attr("checked")){if(!TurnTo.matchData.user){cmd.callback();return}traJq("#TT2twitterBox").attr("disabled",true);if(findUserRaveTarget(TurnTo.twitterTargetId)==-1){connectWithTwitter(null,{onConnectClose:function(){traJq("#TT2twitterBox").removeAttr("disabled");getUserRaveTargets(function(){var foundTwitterTarget=findUserRaveTarget(TurnTo.twitterTargetId)>-1;if(!foundTwitterTarget){traJq("input[name=TT2twitterBox]").attr("checked",false)}cmd.callback()})}})}else{traJq("#TT2twitterBox").removeAttr("disabled");cmd.callback()}if(window.event){window.event.returnValue=true}}}function getFacebookPublishPerm(cmd){if(traJq("#TT2facebookBox").attr("checked")){traJq("#TT2facebookBox").attr("disabled",true);if(!TurnTo.matchData.user||!TurnTo.matchData.user.fbPostToWallGranted){connectWithFacebook(null,{regSource:cmd.regSource,post:true,photo:true,link:(TurnTo.matchData.user?true:false),noMatchesShow:true,keepSubwindow:true,onConnectClose:function(){traJq("#TT2facebookBox").removeAttr("disabled");if(TurnTo.matchData.user){getFbPostToWallGranted(function(){if(!TurnTo.matchData.user.fbPostToWallGranted){traJq("input[name=TT2facebookBox]").attr("checked",false)}cmd.callback()});if(TurnTo.matchData.user&&traJq("#TTtraDialogTitleBar").length>0){change2LiTitleBarFromLo()}}else{traJq("input[name=TT2facebookBox]").attr("checked",false);cmd.callback()}}})}else{traJq("#TT2facebookBox").removeAttr("disabled");cmd.callback()}if(window.event){window.event.returnValue=true}}}function showPostToFacebookWindow(cmd){insertScreen("#TTSubScreen",TurnToHTML.postToFacebookPanel,{questionText:cmd.questionText,catImgId:cmd.catImgId});toggleSubWindow("block");traJq("#TT2postToFbBtn").click(function(){traJq("#TT2fbBtns").hide();traJq("#TT2fbLoading").show();var fbQuestionText=stripString(traJq("#TT2postToFbTextArea").val());if(fbQuestionText.length==0){return}var data={};data.id=cmd.id;data.fbQuestionText=fbQuestionText;if(!TurnTo.matchData.user||!TurnTo.matchData.user.fbPostToWallGranted){var wasNotUser=!TurnTo.matchData.user;connectWithFacebook(null,{regSource:TurnTo.regSourceAskQuestion,post:true,photo:true,link:(TurnTo.matchData.user?true:false),noMatchesShow:true,onConnectClose:function(){if(TurnTo.matchData.user){getFbPostToWallGranted(function(){if(!TurnTo.matchData.user.fbPostToWallGranted){return}else{if(wasNotUser){updateQuestionOwner(cmd.id,function(){postQuestionToFacebook(data)})}else{postQuestionToFacebook(data)}}});if(TurnTo.matchData.user&&traJq("#TTtraDialogTitleBar").length>0){change2LiTitleBarFromLo()}}else{return}}})}else{postQuestionToFacebook(data)}});traJq("#TT2cancelToFbBtn").click(function(){toggleSubWindow("none")})}function postQuestionToFacebook(data){xhr("POST",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/postQuestionToFacebook"),postData:toJson(data),contentType:"text/text",load:function(data,ioargs){if(!data.success){traJq("#TT2fbError").html("Failed to post to Facebook.");traJq("#TT2fbBtns").show();traJq("#TT2fbLoading").hide();return}toggleSubWindow("none")},error:function(data,ioargs){traJq("#TT2fbError").html("Failed to post to Facebook.");traJq("#TT2fbBtns").show();traJq("#TT2fbLoading").hide()}})}function showAnswerWidget(qid,suid){dropLocalLoginCookie();traJq(".TT2sectionHeader").hide();xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getQuestion")+"&qid="+qid+"&suid="+suid,load:function(data,ioArgs){toggleMainWindow("none");TurnTo.answerData=data;traJq("#TT-spinner").hide();traJq(".TT2sectionHeader").hide();if(data.logoutUser){TurnTo.matchData.user=null;TurnTo.loggedIn=false}if(data.user){TurnTo.matchData.user=data.user;TurnTo.loggedIn=true}if(data.user||data.logoutUser){setLocalLoginCookie(data)}if(traJq("#TTtraDialogTitleBar2")&&traJq("#TTtraDialogTitleBar2").length>0){traJq("#TTtraDialogTitleBar2").attr("id","TTtraDialogTitleBar");traJq("#TTtraDialogTitleBar").css("height","32px");TurnTojQuery("#TTtraWindowClose").css("margin-top","-18px")}else{traJq("#TTsignUpLink3").hide()}if(data.question.status==0){showQuestionRemovedMsg();return}var item=data.catItem;insertScreen("#TTtraLayout",TurnToHTML.answerWidget,{item:data.catItem,question:data.question,asker:data.asker,answerer:data.answerer});traJq("#TT2footerFeedback").hide();if(turnToConfig.removeTurnToLogo){traJq("#TTtraFooterLogo").hide()}else{traJq("#TTtraFooterLogo").click(function(e){window.open(TurnTo.partnerProtocol+"//"+turnToSettings.host)})}traJq("#TT2answerBtn").click(submitAnswer);traJq("#TT2answerText")[0].onkeyup=function(e){var charLeft=2000-this.value.length;if(charLeft<=200){if(charLeft>=0){traJq(".TT2charLimitCount")[0].style.color="black";traJq("#TT2answerBtn").show()}else{traJq(".TT2charLimitCount")[0].style.color="red";traJq("#TT2answerBtn").hide()}traJq("#TT2anwerCharCount").html(charLeft+" characters left")}else{traJq("#TT2anwerCharCount").html("");traJq("#TT2answerBtn").show()}};traJq(".TT2itemLink").click(function(){var matchLinkRoot=getSessionUrl("/traServer2/logClickEvent","siteKey="+turnToSettings.siteKey+"&v="+TurnTo.version+"&curl="+encodeURIComponent(document.URL));var catItmId=this.id.substring(this.id.lastIndexOf("-")+1);window.location=matchLinkRoot+"&catItemId="+catItmId})},error:function(data,ioArgs){var message="getQuestion XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function showQuestionRemovedMsg(){showMainMessage("Sorry, this question has been removed.","OK","",toggleMainWindow)}function showAnswerRemovedMsg(){showMainMessage("Sorry, this answer has been removed.","OK","",toggleMainWindow)}function showReplyRemovedMsg(){showMainMessage("Sorry, the comment you are replying to was removed.","OK","",toggleMainWindow)}function showMainMessage(message,bttn1Label,bttn2Label,bttn1Callback,bttn2Callback){insertScreen("#TTtraLayout",TurnToHTML.mainMessage,{message:message,bttn1Label:bttn1Label,bttn2Label:bttn2Label});TurnTojQuery(".TT2sectionHeader").hide();traJq("#TT2footerFeedback").hide();traJq("#TTsignUpLink3").hide();if(bttn1Callback){traJq("#TT2bttn1").click(function(){bttn1Callback()})}else{traJq("#TT2bttn1").hide()}if(bttn2Callback){traJq("#TT2bttn2").click(function(){bttn2Callback()})}else{traJq("#TT2bttn2").hide()}}function submitAnswer(){traJq("#TT2imgLoading").show();traJq("#TT2answerBtn").hide();var data={};if(traJq("#TT2answerText").attr("class")=="TTdefaultInput"){traJq("#TT2imgLoading").hide();traJq("#TT2answerBtn").show();return}data.answer=stripString(traJq("#TT2answerText").val());data.qid=TurnTo.answerData.question.id;var doSubmitFn=function(){xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/traServer2/answerQuestion4Item"),postData:toJson(data),contentType:"text/text",load:function(answerData,ioArgs){getMatches(function(data,ioargs){if(!processResponse(data,ioargs)){return}if(!TurnTo.matchData.user.isFbConnected&&(!TurnTo.matchData.user.hasPhoto||TurnTo.matchData.user.hasDefaultPassword)){showAnswerRegFlow(answerData)}else{showQuestionWidget4AnswerFlow(answerData)}},null,null,{noMatchesShow:true})},error:function(data,ioArgs){traJq("#TT2imgLoading").hide();traJq("#TT2answerBtn").show();traJq("#error").html(data.error);var message="answerQuestion4Item XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})};if(TurnTo.matchData.user){doSubmitFn()}else{registerAndLogin(doSubmitFn)}}function submitInlineAnswer(qid){if(!TurnTo.matchData.user){showRegFlow4InlineAnswer(qid);return}else{toggleSubWindow("none")}var answerText=stripString(traJq("#TT2inlineAnswer-"+qid).val());traJq("#TT2imgLoading-"+qid).show();traJq("#TT2answerBtn-"+qid).hide();var data={};data.answer=answerText;data.qid=qid;data.notify=traJq("#TT2answerNotify-"+qid).attr("checked");xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/traServer2/answerQuestion4Item"),postData:toJson(data),contentType:"text/text",load:function(data,ioArgs){traJq("#TT2imgLoading-"+qid).hide();traJq("#TT2answerBtn-"+qid).show();traJq("#TT2inlineAnswer-"+qid).val("");traJq("#TT2error-"+qid).html();inputLoseFocus(traJq("#TT2inlineAnswer-"+qid)[0],TurnTo.hideInlineAnswerInput);var answerDiv=traJq("#TT2answersBlock-"+qid);answerDiv.before(TurnToHTML.answerBlock({answerer:{uid:TurnTo.matchData.user.id,pid:TurnTo.matchData.user.pid,fName:TurnTo.matchData.user.firstName,lName:TurnTo.matchData.user.lastName,isStaff:data.isStaff},answerText:replaceTrimLineBreaks(answerText),rawAnswerText:answerText,aid:data.aid,qid:qid,quid:data.quid,replys:[],showReply:true}));if(TurnTo.matchData.user.isPreUser){traJq("#TT2answerSubmittedNotConfirmed-"+data.aid).show();traJq("#TT2dismissNotConfirmed-"+data.aid).click(function(){var aid=this.id.substring(this.id.lastIndexOf("-")+1);traJq("#TT2answerSubmitted-"+aid).remove();traJq("#TT2answerSubmittedNotConfirmed-"+aid).remove()});traJq("#TT2resendConfirmation-"+data.aid).click(function(){for(var i=0;i<TurnTo.matchData.user.uIdents.length;i++){if(TurnTo.matchData.user.uIdents[i].type==1&&!TurnTo.matchData.user.uIdents[i].confirmed){resendEmailConfirmation(TurnTo.matchData.user.uIdents[i].ident)}}})}else{traJq("#TT2shareAnswerBtn-"+data.aid).click(function(){showAnswerSharePanel({id:data.aid,answerText:traJq("#TT2rawAnswerText-"+data.aid).val(),questionText:traJq("#TT2rawQuestionText-"+traJq("#TT2qid4answer-"+data.aid).val()).val(),catImgId:traJq("#TT2catImgId").val(),answerSubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})});traJq("#TT2answerSubmitted-"+data.aid).show();traJq("#TT2dismissSubmitted-"+data.aid).click(function(){var aid=this.id.substring(this.id.lastIndexOf("-")+1);traJq("#TT2answerSubmitted-"+aid).remove();traJq("#TT2answerSubmittedNotConfirmed-"+aid).remove()});showAnswerSharePanel({id:data.aid,answerText:traJq("#TT2rawAnswerText-"+data.aid).val(),questionText:traJq("#TT2rawQuestionText-"+traJq("#TT2qid4answer-"+data.aid).val()).val(),catImgId:traJq("#TT2catImgId").val(),answerSubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})}},error:function(data,ioArgs){traJq("#TT2imgLoading-"+qid).hide();traJq("#TT2answerBtn-"+qid).show();traJq("#TT2error-"+qid).html(data.error);var message="submitInlineAnswer XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout});traJq("#TT2anwerCharCount-"+qid).html("")}function showAnswerRegFlow(answerData){traJq("#TT2answerPanel").hide();traJq("#TT2answerSubmittedPanelTxt").html('"'+traJq("#TT2answerText").val()+'"');traJq("#TT2answerSubmittedPanel").show();traJq("#TT2answerRegFlow").show();traJq("#TTpostRegFbConnect").click(function(e){connectWithFacebook(e,{onConnectClose:function(e){showQuestionWidget4AnswerFlow(answerData)},link:true,photo:true,noMatchesShow:true})});traJq("#TTpostRegFbConnect2").click(function(e){connectWithFacebook(e,{onConnectClose:function(e){showQuestionWidget4AnswerFlow(answerData)},link:true,photo:true,noMatchesShow:true})});traJq("#TTregForm")[0].onsubmit=function(e){submitReg4Answer(answerData,function(){showQuestionWidget4AnswerFlow(answerData)});return false}}function submitReg4Answer(answerData,callbackF){var p={};traJq(".TT2doneSub").hide();traJq("#TTregSubmitLoading").show();p.passwd=traJq("#TTpassword")[0].value;p.repasswd=traJq("#TTrePassword")[0].value;if(TurnTo.lastUploadedImageId){p.uploadId=TurnTo.lastUploadedImageId}var enteredPwd=p.passwd.length>0||p.repasswd.length>0;var enteredPhoto=p.uploadId;if(!enteredPwd&&!enteredPhoto){showQuestionWidget4AnswerFlow(answerData);return}else{if(!enteredPwd&&enteredPhoto){TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/saveUserImage")+"&cid="+cid+"&uploadId="+TurnTo.lastUploadedImageId,load:function(data,ioArgs){if(data.pid&&data.pid!="0"){TurnTo.matchData.user.hasPhoto=true;TurnTo.matchData.user.pid=data.pid}showQuestionWidget4AnswerFlow(answerData)},error:function(data,ioArgs){var message="saveUserImage XHR - Status: "+data.status+" Error: "+data.error;console.debug(message);TurnTo.lastUploadedImageId=null},timeout:turnToSettings.timeout})}else{if(enteredPwd){if(enteredPhoto){p.imgTypeSelected="custom"}p.editingPasswd=true;p.firstName=TurnTo.matchData.user.firstName;p.lastName=TurnTo.matchData.user.lastName;p.id=TurnTo.matchData.user.id;xhr("POST",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/updateUserSetting")+"&cid="+cid,content:p,load:function(data,ioArgs){if(data.pid&&data.pid!="0"){TurnTo.matchData.user.hasPhoto=true;TurnTo.matchData.user.pid=data.pid}if(callbackF){callbackF()}},error:function(data,ioArgs){traJq("#TTregSubmitLoading").hide();traJq(".TT2doneSub").show();traJq("#TTregSubmit").show();traJq("#TTregSkip").show();displayErrorsInSubscreen(data.errors)},timeout:turnToSettings.timeout})}}}}function TT2questionTextFocus(el){el.css({"font-size":"12px"})}function TT2questionTextLoseFocus(el){el.css({"font-size":"16px"})}function showQuestionWidget4AnswerFlow(answerData){change2LiTitleBarFromLo();var feedback=TurnTojQuery("#TT2footerFeedback");feedback.show();feedback.attr("href","mailto:feedback@turnto.com?subject=Feedback on QandA widget on "+encodeURIComponent(TurnTo.matchData.site.siteName)+"&body=%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A%0A---------------------------------------------------------------%0ASubmitted from: "+encodeURIComponent(document.location));numQuestionWidgetMsg=0;traJq("#TT2questionWidgetMessageArea").html("");getCommentsQuestionsAnswers4CatItem({catItmId:TurnTo.answerData.catItem.id},function(data,ioArgs){insertScreen("#TTtraLayout",TurnToHTML.questionWidget,{match:{},comments:data.comments,questions:data.questions,item:data.catItem,relatedQuestions:data.relatedQuestions});traJq(".TT2shareQuestionBtn").click(function(){var qid=this.id.substring(this.id.lastIndexOf("-")+1);showQuestionSharePanel({id:qid,questionText:traJq("#TT2rawQuestionText-"+qid).val(),catImgId:traJq("#TT2catImgId").val(),questionSubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})});traJq(".TT2shareAnswerBtn").click(function(){var aid=this.id.substring(this.id.lastIndexOf("-")+1);showAnswerSharePanel({id:aid,answerText:traJq("#TT2rawAnswerText-"+aid).val(),questionText:traJq("#TT2rawQuestionText-"+traJq("#TT2qid4answer-"+aid).val()).val(),catImgId:traJq("#TT2catImgId").val(),answerSubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})});traJq(".TT2shareReplyBtn").click(function(){var rid=this.id.substring(this.id.lastIndexOf("-")+1);showReplySharePanel({id:rid,replyText:traJq("#TT2rawReplyText-"+rid).val(),answerText:traJq("#TT2rawAnswerText-"+traJq("#TT2aid4reply-"+rid).val()).val(),catImgId:traJq("#TT2catImgId").val(),replySubmitted:false,catImgProcessed:traJq("#TT2catImgProcessed").val()})});traJq(".TT2answerReply").click(function(){this.style.color="#777777";var qaid=this.id.substring(this.id.indexOf("-"));traJq("#TT2answersBlock"+qaid).show();traJq("#TT2answerBtns"+qaid).show()});traJq("#TT2dismissSubmitted").click(function(){traJq("#TT2questionSubmitted").hide()});traJq("#TT2dismissNotConfirmed").click(function(){traJq("#TT2questionSubmittedNotConfirmed").hide()});traJq("#TT2resendConfirmation").click(function(){for(var i=0;i<TurnTo.matchData.user.uIdents.length;i++){if(TurnTo.matchData.user.uIdents[i].type==1&&!TurnTo.matchData.user.uIdents[i].confirmed){resendEmailConfirmation(TurnTo.matchData.user.uIdents[i].ident)}}});traJq("#TT2askQuestionTips").click(function(){traJq("#TT2askQuestionExamples").toggle();traJq("#TT2askQuestionTipsExpandImg").toggle();traJq("#TT2askQuestionTipsShrinkImg").toggle()});traJq("#TT2showQuestionBoxLink").click(function(){traJq("#TT2showQuestionBox").hide();traJq("#TT2questionSection").show();traJq("#TT2whatShouldAsk").show()});if(TurnTo.matchData.site.askUseCoupon){getAskCoupon4Answer(answerData.aid,function(data){if(data.couponText){insertScreen("#TTSubScreen",TurnToHTML.askCoupon,{couponText:data.couponText});toggleSubWindow("block");traJq("#TTSubWindowClose").hide();traJq("#TT2closeCoupon").click(function(){toggleSubWindow("none");traJq("#TTSubWindowClose").show();showAnswerSharePanel({id:answerData.aid,answerText:answerData.text,questionText:traJq("#TT2rawQuestionText-"+traJq("#TT2qid4answer-"+answerData.aid).val()).val(),catImgId:traJq("#TT2catImgId-"+traJq("#TT2qid4answer-"+answerData.aid).val()).val(),answerSubmitted:true,catImgProcessed:traJq("#TT2catImgProcessed").val()})})}else{showAnswerSharePanel({id:answerData.aid,answerText:answerData.text,questionText:traJq("#TT2rawQuestionText-"+traJq("#TT2qid4answer-"+answerData.aid).val()).val(),catImgId:traJq("#TT2catImgId-"+traJq("#TT2qid4answer-"+answerData.aid).val()).val(),answerSubmitted:true,catImgProcessed:traJq("#TT2catImgProcessed").val()})}})}else{showAnswerSharePanel({id:answerData.aid,answerText:answerData.text,questionText:traJq("#TT2rawQuestionText-"+traJq("#TT2qid4answer-"+answerData.aid).val()).val(),catImgId:traJq("#TT2catImgId-"+traJq("#TT2qid4answer-"+answerData.aid).val()).val(),answerSubmitted:true,catImgProcessed:traJq("#TT2catImgProcessed").val()})}setupQuestionSubmit();if(!TurnTo.matchData.user.hasPhoto){if(TurnTo.answerData&&TurnTo.answerData.answerer&&TurnTo.answerData.answerer.fbMatched){}else{addQuestionWidgetMessage({screen:"noUserPhotoMessage"});traJq("#TT2AddUserPhoto").click(function(){displayUserSetting(function(){insertScreen("#TTSubScreen",TurnToHTML.userSetting,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl});toggleSubWindow("block");insertScreen("#contactInfo",TurnToHTML.userSettingEmailAddresses,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl});var traHeight=TurnTojQuery("#TTmainContent").height();traJq("#TT2userSetting").css({maxHeight:traHeight-50+"px"});if(TurnTojQuery.browser.msie){traJq("#TT2userSetting").css({height:"auto"})}setupUserSettingAction();showPhotoEditor()})})}}if(hideAskBackBtnOnce){hideAskBackBtnOnce=false;traJq("#TT2goToMatches").hide()}traJq("#TT2goToMatches").click(function(){insertScreen("#TTtraLayout",'<div id="TT-spinner" style="text-align:center;padding-top:60px;padding-bottom:50px;"></div>');setTimeout(function(){populateTra(TurnTo.matchData)},50)});var name=TurnTo.answerData.answerer.fName?TurnTo.answerData.answerer.fName:TurnTo.answerData.answerer.email;traJq("#TT2askWigetHeader").html('<span style="margin-left:10px"></span>'+name+", thanks for helping "+TurnTo.answerData.asker.fName+" "+(TurnTo.answerData.asker.lName!=null?TurnTo.answerData.asker.lName:"")+" about <a id='TT2itemLink-title-"+data.catItem.id+"' class='TT2itemLink' href='javascript:void(0)' style='font-size:18px;font-weight:bold;color:#314C9B'>"+data.catItem.title+"</a>");traJq("#TT2questionSection").hide();traJq("#TT2showQuestionBox").show();traJq("#TT2facebookBox").click(function(e){if(this.checked){this.disabled=true;if(!TurnTo.matchData.user.fbPostToWallGranted){connectWithFacebook(null,{post:true,photo:true,link:true,noMatchesShow:true,onConnectClose:function(){traJq("#TT2facebookBox").removeAttr("disabled");if(TurnTo.matchData.user){getFbPostToWallGranted(function(){if(!TurnTo.matchData.user.fbPostToWallGranted){traJq("input[name=TT2facebookBox]").attr("checked",false)}else{traJq("input[name=TT2facebookBox]").attr("checked",true)}});if(TurnTo.matchData.user&&traJq("#TTtraDialogTitleBar").length>0){change2LiTitleBarFromLo()}}else{traJq("input[name=TT2facebookBox]").attr("checked",false)}}})}else{traJq("#TT2facebookBox").removeAttr("disabled")}if(window.event){window.event.returnValue=true}}});traJq("#TT2twitterBox").click(function(e){if(this.checked){if(!TurnTo.matchData.user){return}this.disabled=true;if(findUserRaveTarget(TurnTo.twitterTargetId)==-1){if(!TurnTo.matchData.user.raveTargets){getUserRaveTargets(function(){var foundTwitterTarget=findUserRaveTarget(TurnTo.twitterTargetId)>-1;if(!foundTwitterTarget){connectWithTwitter(null,{onConnectClose:function(){traJq("#TT2twitterBox").removeAttr("disabled");getUserRaveTargets(function(){var foundTwitterTarget=findUserRaveTarget(TurnTo.twitterTargetId)>-1;if(!foundTwitterTarget){traJq("input[name=TT2twitterBox]").attr("checked",false)}else{traJq("input[name=TT2twitterBox]").attr("checked",true)}})}})}else{traJq("input[name=TT2twitterBox]").attr("checked",true);traJq("#TT2twitterBox").removeAttr("disabled")}})}else{connectWithTwitter(null,{onConnectClose:function(){traJq("#TT2twitterBox").removeAttr("disabled");getUserRaveTargets(function(){var foundTwitterTarget=findUserRaveTarget(TurnTo.twitterTargetId)>-1;if(!foundTwitterTarget){traJq("input[name=TT2twitterBox]").attr("checked",false)}else{traJq("input[name=TT2twitterBox]").attr("checked",true)}})}})}}else{traJq("#TT2twitterBox").removeAttr("disabled")}if(window.event){window.event.returnValue=true}}});traJq(".TT2itemLink").click(function(){var matchLinkRoot=getSessionUrl("/traServer2/logClickEvent","siteKey="+turnToSettings.siteKey+"&v="+TurnTo.version+"&curl="+encodeURIComponent(document.URL));var catItmId=this.id.substring(this.id.lastIndexOf("-")+1);window.location=matchLinkRoot+"&catItemId="+catItmId});traJq("textarea[name^=TT2inlineAnswer-]").keyup(function(){var qaid=this.id.substring(this.id.indexOf("-"));var charLeft=2000-this.value.length;if(charLeft<=200){if(charLeft>=0){traJq("#TT2anwerCharCount"+qaid)[0].style.color="black";traJq("#TT2answerBtn"+qaid).show()}else{traJq("#TT2anwerCharCount"+qaid)[0].style.color="red";traJq("#TT2answerBtn"+qaid).hide()}traJq("#TT2anwerCharCount"+qaid).html(charLeft+" characters left")}else{traJq("#TT2anwerCharCount"+qaid).html("");traJq("#TT2answerBtn"+qaid).show()}});traJq(".TT2cancelBtn").click(function(){var qaid=this.id.substring(this.id.indexOf("-"));var reply=qaid.indexOf("-")!=qaid.lastIndexOf("-");traJq("#TT2inlineAnswer"+qaid).val("");inputLoseFocus(traJq("#TT2inlineAnswer"+qaid)[0],hideInlineAnswerInput);traJq("#TT2answerReply"+qaid).css("color","#314C9B");if(reply){traJq("#TT2answersBlock"+qaid).hide();traJq("#TT2answerBtns"+qaid).hide()}traJq("#TT2error"+qaid).html("");traJq("#TT2anwerCharCount"+qaid).html("")});traJq(".TT2answerBtn").click(function(){var qaid=this.id.substring(this.id.indexOf("-")+1);var ids=qaid.split("-");var qid=ids[0];if(ids.length>1){var aid=ids[1];var rid=ids[2];submitAnswerReply(qid,aid,rid)}else{submitInlineAnswer(qid)}})})}function setupQuestionSubmit(){traJq("#TT2askOwnersBtn").click(function(){var qText=TurnTojQuery.trim(traJq("#TT2questionText").val());if(qText.length>0&&qText!=traJq("#TT2questionText").attr("ttinputlabeltext")){logWebEvent2(TurnTo.eventQaQSubmit,(typeof(TurnToTrack)!="undefined")?"&svid="+TurnToTrack.getSiteVisitId():null)}submitItemQuestion()});traJq("#TT2questionText")[0].onkeyup=function(e){var charLeft=2000-this.value.length;if(charLeft<=200){if(charLeft>=0){traJq(".TT2charLimitCount")[0].style.color="black";traJq("#TT2askOwnersBtn").show()}else{traJq(".TT2charLimitCount")[0].style.color="red";traJq("#TT2askOwnersBtn").hide()}traJq("#TT2anwerCharCount").html(charLeft+" characters left")}else{traJq("#TT2anwerCharCount").html("");traJq("#TT2askOwnersBtn").show()}}}function getAskCoupon4Answer(aid,callback){xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getAskCoupon4Answer")+"&aid="+aid,load:function(data,ioArgs){callback(data)}})}function getCommentsQuestionsAnswers4CatItem(cmd,callbackF){xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getCommentsQuestionsAnswers4CatItem")+(cmd.catItmId?"&catitmid="+cmd.catItmId:"")+(cmd.iid?"&iid="+cmd.iid:"")+(cmd.uid?"&uid="+cmd.uid:"")+(cmd.sku?"&sku="+cmd.sku:"")+(!TurnTo.traDataLoaded?"&getuser=true":""),load:function(data,ioArgs){if(data.logoutUser){TurnTo.matchData.user=null;TurnTo.loggedIn=false;displayTeasers()}if(data.user){TurnTo.matchData.user=data.user;TurnTo.matchData.counts=data.counts;TurnTo.loggedIn=true;change2LiTitleBarFromLo()}if(data.user||data.logoutUser){setLocalLoginCookie(data)}if(callbackF){callbackF(data,ioArgs)}},error:function(data,ioArgs){traJq("#error").html(data.error);var message="getCommentsQuestionsAnswers4CatItem XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function truncate(inputText,maxChars,suffix){if(inputText.length<=maxChars){return inputText}return inputText.substring(0,maxChars-suffix.length)+suffix}function setupDisplayItemsLayoutNav(itemsEle,numOfPages){var nextLink=traJq("#TT2nextLink");nextLink.click(function(){changeDisplayItemsLayoutPage(itemsEle,TurnTo.layoutPage+1,numOfPages)});changeDisplayItemsLayoutPage(itemsEle,0,numOfPages)}function changeDisplayItemsLayoutPage(itemsEle,page,numOfPages){if(page>numOfPages-1||page<0){return}itemsEle[page].style.display="block";if(page!=0){var scrollDown=parseFloat(TurnTojQuery("#TTmainContent")[0].style.maxHeight)-50;traJq("#TTmainContent")[0].scrollTop=traJq("#TTmainContent")[0].scrollTop+scrollDown}TurnTo.layoutPage=page;if(page==numOfPages-1){traJq("#TT2nextLink").hide()}else{traJq("#TT2nextLink").show()}}function getProductImage(imgId,size){return TurnTo.partnerProtocol+"//"+turnToSettings.imageStoreBase+"/item/"+turnToSettings.siteKey+"/"+imgId+size+".png"}function getUserPhoto(uid,pid){return TurnTo.partnerProtocol+"//"+turnToSettings.imageStoreBase+"/user/"+(uid==null?0:pid)+".png"}function getFacebookUserPhoto(){if(TurnTo.matchData&&TurnTo.matchData.user&&TurnTo.matchData.user.isFbConnected){return getUrl("/traServer2/fbUserPhoto?uid="+TurnTo.matchData.user.id)}else{if(TurnTo.matchData&&TurnTo.matchData.siteUser&&TurnTo.matchData.siteUser.fbMatched){return getUrl("/traServer2/fbUserPhoto?suid="+TurnTo.matchData.siteUser.id)}else{if(TurnTo.answerData&&TurnTo.answerData.answerer&&TurnTo.answerData.answerer.fbMatched){return getUrl("/traServer2/fbUserPhoto?suid="+TurnTo.answerData.answerer.suid)}}}return getUserPhoto()}function storeSiteUserFbPhoto(suid,callback){var p={};p.suid=suid;xhr("POST",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/storeSiteUserFbPhoto")+"&cid="+cid,content:p,load:function(data,ioArgs){if(data.pid&&data.pid!="0"){TurnTo.matchData.user.pid=data.pid;TurnTo.matchData.user.hasPhoto=true}if(callback){callback()}},timeout:turnToSettings.timeout})}var rolloverTimeout;TurnTo.rolloverTimeout=rolloverTimeout;var GshadeEl;var GbuttonsEl;var GdetailEl;var GhotAreaEl;function rolloverShowLo(){GshadeEl.show();GbuttonsEl.show();GdetailEl.show();GhotAreaEl.show()}function rolloverShowLi(){GdetailEl.show();GhotAreaEl.show()}function rolloverHideLo(){if(GshadeEl){GshadeEl.hide();GbuttonsEl.hide();GdetailEl.hide();GhotAreaEl.hide()}}function rolloverHideLi(){}function rollover_is_child_of(parent,child){if(child!=null){while(child.parentNode){if((child=child.parentNode)==parent){return true}}}return false}function rolloverFixOnMouseOut(parentElement,event,func){if(!event){event=window.event}var current_mouse_target=null;if(event.toElement){current_mouse_target=event.toElement}else{if(event.relatedTarget){current_mouse_target=event.relatedTarget}}if(!rollover_is_child_of(parentElement,current_mouse_target)&&parentElement!=current_mouse_target){func()}}function iehacks(){if(TurnTojQuery.browser.msie){if(IEversion==7){traJq(".TurnToRolloLoAskButton1").css({margin:"65px 0 0 0",height:"81px"});traJq(".TurnTorolloverDetail").css({left:"207px"})}}}function rolloverLO(){TurnTojQuery(".TTitemRollOver").each(function(i){var el=traJq(this);var containerEl=el.parent().parent().parent().parent().parent();var shadeEl=TurnTojQuery(".TTrolloverBack",containerEl);var buttonsEl=TurnTojQuery(".TTrolloverButtons",containerEl);var detailEl=TurnTojQuery(".TurnTorolloverDetail",containerEl);var hotAreaEl=TurnTojQuery(".TTrolloverHotArea",containerEl);var commentBoxEl=TurnTojQuery(".TTbluecommentbox",containerEl);var bigImageEl=TurnTojQuery(".TTloggedOutRollo1Img",containerEl);var turnToRolloLoItemButton1El=TurnTojQuery(".TurnToRolloLoItemButton1",containerEl);var matchindex=turnToRolloLoItemButton1El.attr("matchindex");var match=TurnTo.matchData.matches[matchindex];if(!match.catItmAc){TurnTojQuery(".TurnToRolloLoAskButton1",containerEl).removeClass("TurnToRolloLoAskButton1").addClass("TurnToRolloLoNca").css({height:"126px"});TurnTojQuery(".TurnToRolloLoItemButton1",containerEl).removeClass("TurnToRolloLoItemButton1");TurnTojQuery(".TurnToRolloLoNca").css({height:"137px"});if(TurnTo.quirksMode){TurnTojQuery(".TurnToRolloLoNca").css({margin:"65px 0 0 -2px"})}}else{if(TurnTo.matchData.site.ask&&match.askable){TurnTojQuery(".TurnToRolloLoAskButton1",containerEl).click(function(){var index=this.id.substring(this.id.lastIndexOf("-")+1);var match=TurnTo.matchData.matches[index];showQuestionWidget(match)});if(IEversion==8){TurnTojQuery(".TurnToRolloLoAskButton1").css({height:"74px"})}}else{var turntoNoAskEl=TurnTojQuery(".TurnToRolloLoAskButton1",containerEl).removeClass("TurnToRolloLoAskButton1").addClass("TurnToRolloLoNoAsk");var turntoNoAskItemEl=TurnTojQuery(".TurnToRolloLoItemButton1",containerEl).removeClass("TurnToRolloLoItemButton1").addClass("TurnToRolloLoItemNoAsk");if(IEversion==8){turntoNoAskEl.css({height:"74px"})}else{turntoNoAskEl.css({height:"84px"})}if(IEversion==8){turntoNoAskItemEl.css({height:"64px"})}else{turntoNoAskItemEl.css({height:"58px"})}if(IEversion==7){turntoNoAskItemEl.css({height:"55px","margin-left":"0"})}if(TurnTo.quirksMode){turntoNoAskEl.css({height:"80px",margin:"64px 0 0 -2px"});turntoNoAskItemEl.css({height:"55px","margin-left":"-2px"})}turntoNoAskEl.click(function(){var ii=turnToRolloLoItemButton1El.attr("matchindex");var stiId=TurnTo.matchData.matches[ii].iid;var uId=TurnTo.matchData.matches[ii].uid==null?null:TurnTo.matchData.matches[ii].uid;var matchLinkRoot=getSessionUrl("/traServer2/logClickEvent","siteKey="+turnToSettings.siteKey+"&v="+TurnTo.version+"&curl="+encodeURIComponent(document.URL));window.location=matchLinkRoot+"&tiId="+stiId+(uId?"&uId="+uId:"")})}turnToRolloLoItemButton1El.click(function(){var ii=turnToRolloLoItemButton1El.attr("matchindex");var stiId=TurnTo.matchData.matches[ii].iid;var uId=TurnTo.matchData.matches[ii].uid==null?null:TurnTo.matchData.matches[ii].uid;var matchLinkRoot=getSessionUrl("/traServer2/logClickEvent","siteKey="+turnToSettings.siteKey+"&v="+TurnTo.version+"&curl="+encodeURIComponent(document.URL));window.location=matchLinkRoot+"&tiId="+stiId+(uId?"&uId="+uId:"")})}el[0].onmouseout=function(e){clearTimeout(TurnTo.rolloverTimeout)};el[0].onmouseover=function(e2){clearTimeout(TurnTo.rolloverTimeout);TurnTo.rolloverHideLo();setTimeout(function(e){traJq("#TT2tipBox").hide();setCookie(ROLLOVER_TOOLTIP_COOKIE_NAME,"1",5184000)},10000);bigImageEl.attr("src",bigImageEl.attr("thephoto"));if(match.rave){TurnTojQuery(".TT2commentSpin",commentBoxEl.parent()).show();getComments4itemId(match.iid,match.uid,loadComments4RollOverClosure(commentBoxEl))}if(!e2){e2=window.event}var position=el.position();var offset=el.offset();var h=TurnTojQuery(window).height();var offsetTop=offset.top;if(position.left>250){if(match.catItmAc){if(TurnTo.matchData.site.ask&&match.askable){TurnTojQuery(".TurnToRolloLoAskButton1",containerEl).addClass("TurnToRolloLoAskButton1Right").css({"margin-left":"-4px",width:"211px"});if(IEversion==7){TurnTojQuery(".TurnToRolloLoAskButton1Right").css({"margin-left":"-10px",width:"211px"});TurnTojQuery(".TurnToRolloLoItemButton1").css({"margin-left":"-1px"})}}else{if(IEversion==7){TurnTojQuery(".TurnToRolloLoItemButton1").css({"margin-left":"0px"});TurnTojQuery(".TurnToRolloLoNoAsk",containerEl).addClass("TurnToRolloLoNoAskRight").css({"margin-left":"-9px",width:"211px"})}else{TurnTojQuery(".TurnToRolloLoNoAsk",containerEl).addClass("TurnToRolloLoNoAskRight").css({"margin-left":"-4px",width:"211px"})}if(TurnTo.quirksMode){TurnTojQuery(".TurnToRolloLoItemButton1").css({"margin-left":"0px"});TurnTojQuery(".TurnToRolloLoNoAskRight").css({"margin-left":"-11px",width:"211px"})}}}else{if(IEversion==7){TurnTojQuery(".TurnToRolloLoNca",containerEl).removeClass("TurnToRolloLoNca").addClass("TurnToRolloLoNcaRight").css({"margin-left":"-9px",width:"211px"})}else{TurnTojQuery(".TurnToRolloLoNca",containerEl).removeClass("TurnToRolloLoNca").addClass("TurnToRolloLoNcaRight").css({"margin-left":"-4px",width:"211px"})}if(TurnTo.quirksMode){TurnTojQuery(".TurnToRolloLoNcaRight").css({"margin-left":"-11px",width:"211px"})}}detailEl.css({left:"-240px"});if(IEversion==7){if(match.catItmAc){detailEl.css({left:"-254px"})}else{detailEl.css({left:"-248px"})}}if(IEversion==8){if(match.catItmAc){detailEl.css({left:"-248px"})}else{detailEl.css({left:"-248px"})}}if(TurnTo.quirksMode){if(match.catItmAc){detailEl.css({left:"-250px"})}else{detailEl.css({left:"-248px"})}}detailEl.addClass("TTrolloverShadowR")}else{detailEl.addClass("TTrolloverShadowL");if(IEversion==7){traJq(".TurnToRolloLoItemButton1").css({margin:"0 0 0 0"})}if(IEversion==8){traJq(".TurnToRolloLoItemButton1").css({height:"65px"})}if(TurnTo.quirksMode){detailEl.css({left:"205px"})}}var ttop=-20;if(e2.pageY<250){ttop=60}if(e2.pageY>400){ttop=-75}detailEl.css({top:ttop+"px"});GshadeEl=shadeEl;GbuttonsEl=buttonsEl;GdetailEl=detailEl;GhotAreaEl=hotAreaEl;TurnTo.rolloverTimeout=setTimeout("TurnTo.rolloverShowLo()",1)};hotAreaEl[0].onmouseout=function(e){clearTimeout(TurnTo.rolloverTimeout);rolloverFixOnMouseOut(hotAreaEl[0],e,function(){shadeEl.hide();buttonsEl.hide();detailEl.hide();hotAreaEl.hide()})}})}function rolloverLI(){TurnTojQuery(".TTitemRollOverLi").each(function(i){var el=traJq(this);var containerEl=this;var shadeEl=TurnTojQuery(".TTrolloverBack",containerEl);var buttonsEl=TurnTojQuery(".TTrolloverButtons",containerEl);var detailEl=TurnTojQuery(".TurnTorolloverDetail",containerEl);var hotAreaEl=TurnTojQuery(".TTrolloverHotArea",containerEl);var commentBoxEl=TurnTojQuery(".TTbluecommentbox",containerEl);var bigImageEl=TurnTojQuery(".TTloggedOutRollo1Img",containerEl);var askBttn=TurnTojQuery(".TurnToRolloLiAskButton1",containerEl);var matchId=askBttn.attr("matchId");var row=matchId.substring(matchId.indexOf("-")+1,matchId.lastIndexOf("-"));var col=matchId.substring(matchId.lastIndexOf("-")+1);var match=TurnTo.matchData.matches[row].items[col];if(!match.catItmAc){TurnTojQuery(".TurnToRolloLiAskButton1",containerEl).removeClass("TurnToRolloLiAskButton1").addClass("TurnToRolloLiNca").css({height:"90px"});TurnTojQuery(".TurnToRolloLiItemButton1",containerEl).removeClass("TurnToRolloLiItemButton1")}else{if(TurnTo.matchData.site.ask&&match.askable){askBttn.click(function(){TurnTojQuery(".TT2sectionHeader").hide();TurnTojQuery("#TTLImessageArea").hide();showQuestionWidget(match)})}else{TurnTojQuery(".TurnToRolloLiAskButton1",containerEl).removeClass("TurnToRolloLiAskButton1").addClass("TurnToRolloLiNoAsk")}var turnToRolloLiItemButton1El=TurnTojQuery(".TurnToRolloLiItemButton1",containerEl);var rave=turnToRolloLiItemButton1El.attr("rave")=="true"?true:false;var stiId=turnToRolloLiItemButton1El.attr("matchId");var uId=turnToRolloLiItemButton1El.attr("uid");uId=uId==null?null:uId;turnToRolloLiItemButton1El.click(function(){var matchLinkRoot=getSessionUrl("/traServer2/logClickEvent","siteKey="+turnToSettings.siteKey+"&v="+TurnTo.version+"&curl="+encodeURIComponent(document.URL));window.location=matchLinkRoot+"&tiId="+stiId+(uId?"&uId="+uId:"")})}el[0].onmouseout=function(e){clearTimeout(TurnTo.rolloverTimeout)};el[0].onmouseover=function(e2){clearTimeout(TurnTo.rolloverTimeout);bigImageEl.attr("src",bigImageEl.attr("thephoto"));if(rave){TurnTojQuery(".TT2commentSpin",commentBoxEl.parent()).show();getComments4itemId(stiId,uId,loadComments4RollOverClosure(commentBoxEl))}TurnTo.rolloverHideLi();if(!e2){e2=window.event}var position=el.position();var offset=el.offset();var h=TurnTojQuery(window).height();var offsetTop=offset.top;if(position.left>250){if(match.catItmAc){if(TurnTo.matchData.site.ask&&match.askable){TurnTojQuery(".TurnToRolloLiAskButton1",containerEl).addClass("TurnToRolloLiAskButton1Right")}else{TurnTojQuery(".TurnToRolloLiNoAsk",containerEl).addClass("TurnToRolloLiNoAskRight")}}else{TurnTojQuery(".TurnToRolloLiNca",containerEl).removeClass("TurnToRolloLiNca").addClass("TurnToRolloLiNcaRight")}TurnTojQuery(".TurnToRolloLiItemButton1",containerEl).css({"margin-left":"5px"});detailEl.addClass("TTrolloverShadowR");if(IEversion==8||IEversion==7){detailEl.css({left:"-249px"})}else{detailEl.css({left:"-241px"})}}else{detailEl.addClass("TTrolloverShadowL")}var ttop=-20;if(e2.pageY<250){ttop=10}if(e2.pageY>400){ttop=-70}detailEl.css({top:ttop+"px"});GdetailEl=detailEl;GhotAreaEl=hotAreaEl;TurnTo.rolloverTimeout=setTimeout("TurnTo.rolloverShowLi()",1)};detailEl[0].onmouseover=function(e){clearTimeout(TurnTo.rolloverTimeout);detailEl.hide();hotAreaEl.hide()};el[0].onmouseout=function(e){clearTimeout(TurnTo.rolloverTimeout);rolloverFixOnMouseOut(el[0],e,function(){detailEl.hide();hotAreaEl.hide()})}})}var commentCache={};function getComments4itemId(itemId,uid,loadCommentFunc){loadCommentFunc=(loadCommentFunc)?loadCommentFunc:loadComments;if(commentCache[itemId]==-1){return}if(commentCache[itemId]){loadCommentFunc(commentCache[itemId])}else{commentCache[itemId]=-1;TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getComments4itemId")+"&itemid="+itemId+"&uid="+uid,load:function(data,ioArgs){getCommentsForSkuLoaded(data,ioArgs,loadCommentFunc)},error:function(data,ioArgs){var message="getMatches XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}}function getCommentsForSkuLoaded(data,ioArgs,loadCommentFunc){loadCommentFunc=(loadCommentFunc)?loadCommentFunc:loadComments;if(data.status!=200){TurnTo.error=data.error;TurnTo.status=data.status}if(hasErrors()){return}if(data.comments.length>0){commentCache[data.itemid]=data;loadCommentFunc(data)}}function loadComments(data){if(turntoShowRolloComments){TurnTojQuery("#TTbluecommentbox")[0].className="TTbluebox";TurnTojQuery("#TTloggedOutRollo1name")[0].innerHTML=data.user.first+" "+data.user.last+". chose this because:";var com="";for(var x=0;x<data.comments.length;x++){com+=data.comments[x]}TurnTojQuery("#TTloggedOutRollo1Comments")[0].innerHTML=com;turntoOnMousemove=true;showRolloBox=true}}function loadComments4RollOverClosure(commentsBoxEl){return(function(data){TurnTojQuery(".TTloggedOutRollo1name",commentsBoxEl)[0].innerHTML=data.user.first+" "+data.user.last+". chose this because:";var com="";for(var x=0;x<data.comments.length;x++){com+=data.comments[x]}TurnTojQuery(".TTloggedOutRollo1Comments",commentsBoxEl)[0].innerHTML=com;TurnTojQuery(".TT2commentSpin",commentsBoxEl.parent()).hide();commentsBoxEl.show()})}function loadComments4AskWidget(data){var com="";for(var x=0;x<data.comments.length;x++){com+=data.comments[x]}traJq("#TT2cqaComment").html(com)}function showUserLogin(e){console.log("showLogin");insertScreen("#TTBubbleScreen",TurnToHTML.loginscreen,{fbButtonSrc:getStaticUrl("/tra2/images/use-facebook.png")});traJq("#TTfbConnectLogin")[0].onclick=function(e){connectWithFacebook(e,{onConnectClose:function(){toggleBubbleWindow("none")}})};traJq("#TTloginForgotPwd")[0].onclick=function(e){insertScreen("#TTBubbleScreen",TurnToHTML.forgotPwdScreen,{});traJq("#TTresetPwdBtn")[0].onclick=function(e){resetPassword(e)};traJq("#TTforgotPwdForm")[0].onsubmit=function(e){resetPassword(e);return false}};TurnTo.loginMessage=document.getElementById("TTloginMessage");TurnTo.loginUsername=document.getElementById("TTloginUsername");TurnTo.loginPassword=document.getElementById("TTloginPassword");TurnTo.loginRememberMe=document.getElementById("TTloginRememberMe");TurnTo.loginSubmit=document.getElementById("TTloginSubmit");TurnTo.loginForm=document.getElementById("TTloginForm");TurnTo.loginForm.onsubmit=doLogin;if(TurnTo.loginAtTTInterval){clearInterval(TurnTo.loginAtTTInterval)}toggleBubbleWindow("block");stopDefault(e);return false}function resetPassword(e){TurnTo.error=null;var email=TurnTojQuery.trim(traJq("#TTloginUsername")[0].value);if(email.length==0||email==traJq("#TTloginUsername").attr("ttInputLabelText")){traJq("#TTresetPwdMessage")[0].innerHTML="Please enter the email address for your account.";return}xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/resetPassword")+"&email="+encodeURIComponent(traJq("#TTloginUsername")[0].value),load:function(data,ioArgs){showUserLogin(e);traJq("#TTloginMessage")[0].innerHTML=data.message},error:function(data,ioArgs){var message="resetPassword XHR - Status: "+data.status+" Error: "+data.error;console.debug(message);traJq("#TTresetPwdMessage")[0].innerHTML=data.error},timeout:turnToSettings.timeout})}function clearFbConnectPicture(){var ttfbpicdiv=TurnTojQuery("#TTfbPic");if(ttfbpicdiv){ttfbpicdiv.empty()}window.TTfbPicFrame=null}function registerFbConnectUser(cmd){var cmdStr=decodeURIComponent(cmd);var rsltObj=eval("("+cmdStr+")");if(!rsltObj.connectResult){return}rsltObj.service="facebook";xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/traServer2/createUser"),postData:toJson(rsltObj),contentType:"text/text",load:createUserLoaded})}function enableLogin(flag){var func=flag?doLogin:null;TurnTo.loginSubmit.onclick=func;TurnTo.loginSubmit.disabled=!flag}function doLogin(e){traJq("#TTloginSubmit").hide();traJq("#TTloginSpinner").show();enableLogin(false);if(TurnTo.loginAtTTInterval){clearInterval(TurnTo.loginAtTTInterval)}var auth={};auth.username=traJq("#TTloginUsername")[0].value;auth.password=traJq("#TTloginPassword")[0].value;if(traJq("#TTloginRememberMe").length>0&&traJq("#TTloginRememberMe")[0].checked){auth._spring_security_remember_me="true"}xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/secure/traServer2/loginGetMatches"),content:auth,load:loginGetMatchesLoaded,error:function(data,ioArgs){console.dir(data);if(data.status==402){TurnTo.loginMessage.innerHTML='<span class="TTred">Your Email and Password did not match.</span>'}else{if(data.status==407){TurnTo.loginMessage.innerHTML='<span class="TTred" style="font-weight:100;font-size:10px;">Your email address is unconfirmed. Please check the email we sent you to finish your registration.</span>'}else{TurnTo.loginMessage.innerHTML="Timeout - please try again"}}enableLogin(true);traJq("#TTloginSubmit").show();traJq("#TTloginSpinner").hide()}});stopDefault(e);return false}function createUserLoaded(data,ioargs){if(data.error){TurnTojQuery("#TTcheckFmMessage")[0].innerHTML=data.error;TurnTojQuery(".TTerrorLoginLink").each(function(i){this.href="javascript:void(0)";this.onclick=showRealLogin})}else{loginGetMatchesLoaded(data,ioargs)}}function loginGetMatchesLoaded(data,ioargs){if(data.status==200){toggleBubbleWindow("none");populateTra(data,ioargs,function(){displayTeasers()});setLocalLoginCookie(data)}else{TurnTo.loginMessage.innerHTML="Email and Password did not match";enableLogin(true)}}function setLocalCookies(token,validitySeconds,sessionId){if(token){setCookie(turnToSettings.authCookieName,token,validitySeconds)}setCookie(turnToSettings.sessionCookieName,sessionId)}function setLocalLoginCookie(data){var userLoggedIn=false;if(data){if(data.user){userLoggedIn=true}}else{if(TurnTo.matchData&&TurnTo.matchData.user){userLoggedIn=true}}if(userLoggedIn){setCookie(LOGGED_IN_COOKIE_NAME,true,604800)}else{dropLocalLoginCookie()}}function dropLocalLoginCookie(){var c=getCookie(LOGGED_IN_COOKIE_NAME);if(c){setCookie(LOGGED_IN_COOKIE_NAME)}}function dropLocalCookies(){var cn=turnToSettings.authCookieName;var c=getCookie(cn);if(c){setCookie(cn)}cn=turnToSettings.sessionCookieName;c=getCookie(cn);if(c){setCookie(cn)}dropLocalLoginCookie()}function loginAtTT(){TurnTo.loginWindow=window.open(TurnTo.loginUrl,turnToSettings.popupName,turnToSettings.popupOptions);if(TurnTo.loginAtTTInterval){clearInterval(TurnTo.loginAtTTInterval)}TurnTo.loginComplete=false;TurnTo.loginAtTTInterval=setInterval(loginAtTTInterval,2000);return false}function loginAtTTInterval(){if(TurnTo.loginComplete){clearInterval(TurnTo.loginAtTTInterval);if(TurnTo.thirdBlocked){setLocalCookies(TurnTo.loginToken,TurnTo.loginValiditySeconds,TurnTo.loginSessionId)}TurnTo.loginAtTTInterval=null;getMatches(getMatchesLoaded)}}function logOut(doXhr,dontShowLogin){TurnTo.zip=null;fbLogout();localLogout();insertUiParts();if(typeof doXhr=="undefined"){doXhr=true}if(doXhr){xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/logout"),handleAs:"json",load:function(){getMatches(populateTra)},error:function(data,ioArgs){console.debug("logOut status: ",data.status,data.error)}})}resize();return false}function fbLogout(){if(TurnTo.matchData.user.isFbConnected){window.open(getUrl("/traServer2/logoutFromFacebook"),"fbLogout","scrollbars=no,menubar=no,height=500,width=740,resizable=no,toolbar=no,status=no,location=no").focus()}}function localLogout(){var s=TurnTo.matchData.site;TurnTo.matchData={};TurnTo.matchData.site=s;TurnTo.loggedIn=false;TurnTo.traDataLoaded=true;dropLocalCookies();displayTeasers()}function addFeedPurchaseOrder(data){TurnTo.turnToFeed={sig:randomString(),orderId:data.orderId,email:data.email,partnerData:data.partnerData?data.partnerData:"",postalCode:data.postalCode?data.postalCode:"",firstName:data.firstName?data.firstName:"",lastName:data.lastName?data.lastName:""};TurnTo.turnToFeed.items=[]}function addFeedLineItem(data){TurnTo.turnToFeed.items[TurnTo.turnToFeed.items.length]={title:data.title,url:data.url,lineItemId:(data.lineItemId?data.lineItemId:TurnTo.turnToFeed.items.length+1),partnerData:(data.partnerData?data.partnerData:""),sku:(data.sku?data.sku:""),price:(data.price?data.price:"0.00"),itemImageUrl:(data.itemImageUrl?data.itemImageUrl:"")}}function debugFeed(){var d="";for(var name in TurnTo.turnToFeed){if(name!="items"){d+=name+"="+TurnTo.turnToFeed[name]+" \n"}}for(var x=0;x<TurnTo.turnToFeed.items.length;x++){d+="\nLine item "+(x+1)+"\n";for(var iname in TurnTo.turnToFeed.items[x]){d+=iname+"="+TurnTo.turnToFeed.items[x][iname]+" \n"}}alert(d)}function makeSendConfirmFunc(){return function(e){xhr("POST",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/sendconfirm"),contentType:"text/text",load:function(response,ioArgs){setCookie(turnToSettings.updateCookieName,new Date().getTime());if(response.message=="success"){var tmpEmail=TurnTo.matchData.user.firstName;var mess="Your confirmation e-mail has been resent to "+tmpEmail+'. Please check your email to complete your registration.<div align="center" style="padding-top:20px;"><a id="TTLoginDialogClose">Click here to close this window</a></div>';TurnTojQuery("#TTcheckFormDiv")[0].style.display="none";var elem=TurnTojQuery("#TTgenMessageScreen")[0];elem.style.display="block";elem.innerHTML=mess;var cl=TurnTojQuery("#TTLoginDialogClose")[0];cl.href="javascript:void(0)";cl.onclick=function(){stopDefault(e);return false};logOut(true,true);stopDefault(e)}},error:function(data,ioArgs){var message="postalcode XHR - Status: "+data.status+" Error: "+data.error;if(data.status==401){logOut(false)}else{console.debug(message)}},timeout:turnToSettings.timeout});stopDefault(e);return false}}function checkFTforIe6(){if(TurnTojQuery.browser.msie&&TurnTojQuery.browser.version.substr(0,1)<7){if(TurnTo.matchData!=null){TurnTo.matchData.site.floatTeaser=false}return false}return true}function floatTeaserLoggedIn(count){if(checkFTforIe6()){TurnTojQuery("#TTteaserFloatLink")[0].className="TTshowMatchesLink TTfloatTeaser TTfloatTeaser"+count}}function floatTeaserLoggedOut(){if(TurnTo.floatTeaserSet&&checkFTforIe6()){TurnTojQuery("#TTteaserFloatLink")[0].className="TTloginLink TTfloatTeaser TTfloatTeaser0"}}function createFloatingTeaser(arg1){var hasBody=arg1||false;if(TurnTojQuery.browser.msie&&TurnTojQuery.browser.version.substr(0,1)<7){if(TurnTo.matchData!=null){TurnTo.matchData.site.floatTeaser=false}return false}var headID=document.getElementsByTagName("head")[0];if(turnToSettings.floatingTeaserStyle!=1){var cssUrl=getStaticUrl("/static/fteaser/"+turnToSettings.floatingTeaserStyle+"/styles.css");var cssNode3=document.createElement("link");cssNode3.type="text/css";cssNode3.rel="stylesheet";cssNode3.href=cssUrl;cssNode3.media="screen";headID.appendChild(cssNode3)}if(turnToSettings.useAbsoluteFteaser&&TurnTojQuery.browser.msie){var cssUrl4=getStaticUrl("/static/fteaser/css/absolute.css");var cssNode4=document.createElement("link");cssNode4.type="text/css";cssNode4.rel="stylesheet";cssNode4.href=cssUrl4;cssNode4.media="screen";headID.appendChild(cssNode4)}var fteaser='<div id="TTteaserFloatId"><a id="TTteaserFloatLink" border="0" href="javascript:void(0)"></a></div>';if(turnToSettings.useSetup){TurnTojQuery("#TurnToSetup").append(fteaser)}else{if(hasBody){TurnTojQuery("body").prepend(fteaser)}else{TurnTojQuery("body").append(fteaser)}}return true}function truncateName(name){var len=18;if(name.length>len){var trunc=name.substring(0,len);trunc+="...";return trunc}return name}function setCookie(cName,value,age){var dc=cName+"=";if(typeof value!="undefined"){dc+=value+"; path=/";if(typeof age!="undefined"){if(age!=-1){var d=new Date();d.setTime(d.getTime()+age*1000);dc+="; Expires="+d.toUTCString()}}}else{dc+="; path=/; Expires="+new Date(0).toUTCString()}document.cookie=dc}function getCookie(cName){var c=null;var ac=document.cookie;var start=ac.indexOf(cName+"=");if(start>=0){start+=cName.length+1;var end=ac.indexOf(";",start);if(end==-1){end=ac.length}c=ac.substring(start,end)}return c}function stopDefault(e){if(e){e.preventDefault()}else{if(window.event){window.event.returnValue=false}}}function getDateString(d){var day=d.getDate();var m=d.getMonth()+1;var mS=["January","February","March","April","May","June","July","August","September","October","November","December"][d.getMonth()];var y=d.getFullYear();return mS+" "+day}function checkUnloadReg(){if(!turnToConfig.showExitPopup){return}if(!turnToConfig.exitPopupFrequency){turnToConfig.exitPopupFrequency=100}var freqRes=(Math.floor(Math.random()*100)<turnToConfig.exitPopupFrequency);var blackout=false;if(turnToConfig.exitPopupBlackout){var ttCookie=getCookie(turnToSettings.regPopupCookieName);if(ttCookie){var cookieVal=parseInt(ttCookie);var blackoutSetting=turnToConfig.exitPopupBlackout*3600000;var now=(new Date()).getTime();blackout=(cookieVal+blackoutSetting)>now}}if(freqRes&&!blackout){TurnTojQuery(window).unload(showRegPopup);setCookie(turnToSettings.regPopupCookieName,(new Date()).getTime(),31536000)}}function showRegPopup(){if(TurnTo.matchData&&TurnTo.matchData.user){return}var feedString=(TurnTo.turnToFeed&&TurnTo.turnToFeed.orderId)?"&orderId="+TurnTo.turnToFeed.orderId+"&sig="+TurnTo.turnToFeed.sig:"";feedString="layout=embed&siteKey="+turnToConfig.siteKey+feedString;var uri="http://"+turnToConfig.host+"/trackin/ordrconfpgpop?siteKey="+turnToSettings.siteKey+"&qs="+encodeURIComponent(feedString);var wndw=window.open(uri,"regwin","scrollbars=yes,menubar=no,height=500,width=740,resizable=yes,toolbar=no,status=no,location=no")}function randomString(){var chars="0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";var string_length=10;var randomstring="";for(var i=0;i<string_length;i++){var rnum=Math.floor(Math.random()*chars.length);randomstring+=chars.substring(rnum,rnum+1)}return randomstring}function mockDojo(){TTprevDjConfig=(typeof djConfig=="undefined")?null:djConfig;djConfig={isDebug:turnToConfig.isDebug,xipClientUrl:turnToConfig.xipClientUrl||"/tra/xip_client.html"};TTprevDojo=(typeof dojo=="undefined")?null:dojo;dojo={require:function(){},provide:function(){},io:{iframe:{create:createIframe}},objectToQuery:objectToQuery,extend:dojo_extend};TTprevDojox=(typeof dojox=="undefined")?null:dojox;dojox={io:{proxy:{}}}}function unmockDojo(){if(TTprevDjConfig){djConfig=TTprevDjConfig}if(TTprevDojo){dojo=TTprevDojo}if(TTprevDojox){dojox=TTprevDojox}}function xhr(method,options){if(turnToSettings.useFlashXHR){var protoAndUrl=(options.iframeProxyUrl.match("^https")==null?"http":"http")+"://"+(options.useStaticHost?turnToSettings.staticHost:turnToSettings.host)+options.url+"&curl="+encodeURIComponent(document.URL);var flproxy;if(turnToSettings.useSetup){flproxy=new flensed.flXHR({autoUpdatePlayer:false,appendToId:"TurnToSetup"})}else{flproxy=new flensed.flXHR({autoUpdatePlayer:false})}flproxy.instanceId="myproxy1";flproxy.xmlResponseText=false;flproxy.onreadystatechange=flashXhrFunc(options);flproxy.open(method,protoAndUrl);var authToken=getCookie(turnToSettings.authCookieName);if(authToken){flproxy.setRequestHeader("X-TTauth",authToken)}var sendData=null;var ctype=options.contentType;if(method=="POST"){if(ctype){sendData=options.postData}else{ctype="application/x-www-form-urlencoded";sendData=objectToQuery(options.content)}flproxy.setRequestHeader("Content-Type",ctype)}flproxy.send(sendData)}else{xhrFunc=makeXhrFunc(method,options);xhrFunc()}}function flashXhrFunc(options){return function(xhr){if(xhr.readyState==4){var data1={status:200,error:""};if(options.handleAs&&options.handleAs=="text"){data1.text=xhr.responseText}else{try{data1=eval("("+xhr.responseText+")")}catch(ex){console.debug("JSON eval exception: "+ex);data1={status:500,error:ex}}}if(!data1.status){data1={status:500,error:"status field not returned in JSON"}}if(data1.status==200){options.load(data1,{options:options,xhr:xhr})}else{options.error(data1,{options:options,xhr:xhr})}}}}function makeXhrFunc(method,options){return function(){var facade=new dojox.io.proxy.xip.XhrIframeFacade(options.iframeProxyUrl);facade._method=method;facade._uri=options.url;if(options.headers){facade._requestHeaders=options.headers}var ctype=options.contentType;if(method=="POST"){if(ctype){facade._bodyData=options.postData}else{ctype="application/x-www-form-urlencoded";facade._bodyData=objectToQuery(options.content)}facade.setRequestHeader("Content-Type",ctype)}if(TurnTo.thirdBlocked){facade.setRequestHeader("X-TT3bl","true")}var authToken=getCookie(turnToSettings.authCookieName);if(authToken){facade.setRequestHeader("X-TTauth",authToken)}var stateId=dojox.io.proxy.xip.send(facade);TurnTo.xhrIntervals[stateId]=setInterval(makeXhrIntervalFunc(stateId,facade,options),100)}}function makeXhrIntervalFunc(stateId,xhr,options){var startTime=new Date().getTime();options.timeout=options.timeout||turnToSettings.timeout;return function(){if(xhr.readyState!=4){var currentTime=new Date().getTime();if(currentTime>startTime+options.timeout){xhr.abort();clearInterval(TurnTo.xhrIntervals[stateId]);var message="XHR Timed out";xhr.status=500;xhr.statusText=message;options.error({status:xhr.status,error:message},{options:options,xhr:xhr})}return}clearInterval(TurnTo.xhrIntervals[stateId]);var data={status:200,error:""};if(xhr.status==200){if(options.handleAs&&options.handleAs=="text"){data.text=xhr.responseText}else{try{data=eval("("+xhr.responseText+")")}catch(ex){console.debug("JSON eval exception: "+ex);data={status:500,error:ex}}}if(!data.status){data={status:500,error:"status field not returned in JSON"}}if(data.status==200){options.load(data,{options:options,xhr:xhr})}else{options.error(data,{options:options,xhr:xhr})}}else{options.error({status:xhr.status,error:xhr.statusText},{options:options,xhr:xhr})}}}function ttPlural(word,count){var t=word.replace(/^\s+/,"").replace(/\s+$/,"");if(t=="person"){return((count==1)?" person":" people")}else{if(t=="shop"){return word+((count==1)?"s":"")}else{if(t=="has"){return((count==1)?"has":"have")}else{return word+((count==1)?"":"s")}}}}function createIframe(fname,onloadstr,uri){if(window[fname]){return window[fname]}if(window.frames[fname]){return window.frames[fname]}var cframe=null;var turi=uri;if(!turi){return null}var ifrstr=TurnTojQuery.browser.msie?'<iframe name="'+fname+'" src="'+turi+'" onload="'+onloadstr+'">':"iframe";cframe=document.createElement(ifrstr);with(cframe){name=fname;setAttribute("name",fname);id=fname}var c;if(turnToSettings.useSetup){TurnTojQuery("#TurnToSetup").append(cframe)}else{c=document.body.appendChild(cframe)}window[fname]=cframe;with(cframe.style){if(TurnTojQuery.browser.safari&&TurnTojQuery.browser.version<419.3){position="absolute"}left=top="1px";height=width="1px";visibility="hidden";position="absolute"}if(!TurnTojQuery.browser.msie){setIframeSrc(cframe,turi,true);cframe.onload=new Function(onloadstr)}return cframe}function setIframeSrc(iframe,src,replace){try{if(!replace){if(TurnTojQuery.browser.safari){iframe.location=src}else{frames[iframe.name].location=src}}else{var idoc;if(TurnTojQuery.browser.msie){idoc=iframe.contentWindow.document}else{if(TurnTojQuery.browser.safari){var vs=TurnTojQuery.browser.version.split(".");var safariVersion=vs[0]+"."+((vs[1])?vs[1]:0);if(parseInt(safariVersion)>=419.3){idoc=iframe.contentWindow.document}else{idoc=iframe.document}}else{idoc=iframe.contentWindow}}if(!idoc){iframe.location=src;return}else{idoc.location.replace(src)}}}catch(e){}}function objectToQuery(map){var enc=encodeURIComponent;var pairs=[];var backstop={};for(var name in map){var value=map[name];if(value!=backstop[name]){var assign=enc(name)+"=";if(isArray(value)){for(var i=0;i<value.length;i++){pairs.push(assign+enc(value[i]))}}else{pairs.push(assign+enc(value))}}}return pairs.join("&")}function escapeJsonString(str){return('"'+str.replace(/(["\\])/g,"\\$1")+'"').replace(/[\f]/g,"\\f").replace(/[\b]/g,"\\b").replace(/[\n]/g,"\\n").replace(/[\t]/g,"\\t").replace(/[\r]/g,"\\r")}var toJsonIndentStr="\t";function toJson(it,prettyPrint,_indentStr){if(it===undefined){return"undefined"}var objtype=typeof it;if(objtype=="number"||objtype=="boolean"){return it+""}if(it===null){return"null"}if(isString(it)){return escapeJsonString(it)}if(it.nodeType&&it.cloneNode){return""}var recurse=arguments.callee;var newObj;_indentStr=_indentStr||"";var nextIndent=prettyPrint?_indentStr+toJsonIndentStr:"";if(typeof it.__json__=="function"){newObj=it.__json__();if(it!==newObj){return recurse(newObj,prettyPrint,nextIndent)}}if(typeof it.json=="function"){newObj=it.json();if(it!==newObj){return recurse(newObj,prettyPrint,nextIndent)}}var sep=prettyPrint?" ":"";var newLine=prettyPrint?"\n":"";if(isArray(it)){var res=TurnTojQuery.map(it,function(obj){var val=recurse(obj,prettyPrint,nextIndent);if(typeof val!="string"){val="undefined"}return newLine+nextIndent+val});return"["+res.join(","+sep)+newLine+_indentStr+"]"}if(objtype=="function"){return null}var output=[];for(var key in it){var keyStr;if(typeof key=="number"){keyStr='"'+key+'"'}else{if(typeof key=="string"){keyStr=escapeJsonString(key)}else{continue}}val=recurse(it[key],prettyPrint,nextIndent);if(typeof val!="string"){continue}output.push(newLine+nextIndent+keyStr+":"+sep+val)}return"{"+output.join(","+sep)+newLine+_indentStr+"}"}function isString(it){return typeof it=="string"||it instanceof String}function isArray(it){return it&&it instanceof Array||typeof it=="array"}function dojo_extend(constructor,props){for(var i=1,l=arguments.length;i<l;i++){dojo_mixin(constructor.prototype,arguments[i])}return constructor}function dojo_mixin(obj,props){var tobj={};for(var x in props){if(tobj[x]===undefined||tobj[x]!=props[x]){obj[x]=props[x]}}if(TurnTojQuery.browser.msie&&props){var p=props.toString;if(typeof p=="function"&&p!=obj.toString&&p!=tobj.toString&&p!="\nfunction toString() {\n    [native code]\n}\n"){obj.toString=props.toString}}return obj}function displayBubble(){TurnTojQuery("#TTSubWindowClose")[0].onclick=toggleSubWindow;var q=insertScreen("#TTBubbleScreen",TurnToHTML.bubble,({turnToSettings:turnToSettings,siteName:TurnTo.matchData.site.siteName,importSrc:importSrc,fbButtonSrc:getStaticUrl("/tra2/images/use-facebook.png")}));traJq("#TT2addFriendsLogoutLink3").click(function(){toggleBubbleWindow();displayImportSourceSelectScreen()});traJq("#TT2signUpLink3").click(showUserLogin);traJq("#TTtipSpecialB").click(function(){toggleBubbleWindow();displayWhy2()});traJq("#TTfbConnectB").click(function(e){connectWithFacebook(e,{photo:true,onConnectClose:function(){if(TurnTo.matchData.user){TurnTo.newfbuserExitFlow=true;toggleBubbleWindow()}}})});toggleBubbleWindow("block")}function displayWhy(){var q=insertScreen("#TTSubScreen",TurnToHTML.whyshouldi,({}));TurnTojQuery("#TTSubWindowClose")[0].onclick=function(e){toggleSubWindow();displayBubble()}}function displayWhy2(){var q=insertScreen("#TTSubScreen",TurnToHTML.whyshouldi,({}));toggleSubWindow("block")}function displayReg(regflow){var q=insertScreen("#TTSubScreen",TurnToHTML.regform,{matches:TurnTo.matchData,getUrl:getUrl,getStaticUrl:getStaticUrl,regflow:regflow});toggleSubWindow("block");q.find("#TTregForm")[0].onsubmit=doSubmitRegForm;traJq("#TT2regFromfbConnect").click(function(){connectWithFacebook(null,{photo:true,onConnectClose:function(){TurnTo.newfbuserExitFlow=true;toggleSubWindow("none")}})})}TurnTo.displayImportSourceSelectScreen=displayImportSourceSelectScreen;function displayImportSourceSelectScreen(cmdObj){var fbmessage=traJq("#TTnewfbusermessage");if(fbmessage){dismissFbNewUserMess()}if(typeof cmdObj=="undefined"){cmdObj={showConfirm:false}}if(!TurnTo.matchData.importSrc){xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getImportSources"),load:function(data,ioargs){TurnTo.matchData.importSrc=data.importSrc;var q=insertScreen("#TTSubScreen",TurnToHTML.importSourceSelect,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl,fbButtonSrc:getStaticUrl("/tra2/images/use-facebook.png"),showConfirm:cmdObj.showConfirm,hideFacebook:cmdObj.hideFacebook,headerText:cmdObj.headerText});toggleSubWindow("block");setupImportSrcSelectAction(cmdObj)}})}else{var q=insertScreen("#TTSubScreen",TurnToHTML.importSourceSelect,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl,fbButtonSrc:getStaticUrl("/tra2/images/use-facebook.png"),showConfirm:cmdObj.showConfirm,hideFacebook:cmdObj.hideFacebook,headerText:cmdObj.headerText});toggleSubWindow("block");setupImportSrcSelectAction(cmdObj)}}function setupImportSrcSelectAction(cmdObj){for(var i=0;i<TurnTo.matchData.importSrc.length;i++){var impSrc=TurnTo.matchData.importSrc[i];if(!TurnTo.matchData.user){if(invalidLOImportSource(impSrc)){traJq("#TT2importSrc-"+impSrc.id).hide()}}traJq("#TT2importSrcLink"+impSrc.id)[0].onclick=function(e){removeImportSrcLinksEffect();this.style.backgroundColor="#fefeab";var domain=this.innerHTML.replace(/^\s+|\s+$/g,"");TurnTo.delAuthWin=delAuthImport(domain,TurnTo.regSourceTra);if(!TurnTo.delAuthWin){var value=getValueBaseOnDomain(domain);if(value!=-1){displayImport(value,null)}}else{insertScreen("#TT2ImportSrcForm","<ul id='TTSubDialogErrors'></ul><div id='TTpickone'></div>");if(TurnTo.delAuthPopupInterval){clearInterval(TurnTo.delAuthPopupInterval)}TurnTo.delAuthPopupInterval=setInterval(function(){delAuthPopupIntervalFn()},750)}}}if(!TurnTo.matchData.user){traJq("#TT2NoImportLink")[0].onclick=displayReg}traJq("#TTimportSourceSkip")[0].onclick=(TurnTo.matchData.user&&!TurnTo.matchData.user.isPreUser)||cmdObj.fromRegForm?function(){toggleSubWindow("none")}:displayReg;if(traJq("#TTfbConnect2")[0]){traJq("#TTfbConnect2")[0].onclick=function(e){connectWithFacebook(e,{link:true,onConnectClose:function(){getDelAuthImportMsg(function(data,ioargs){TurnTo.matchData.delAuthImportMsg=data.delAuthImportMsg;getImportHistory(function(){displayImportSourceSelectScreen({showConfirm:true})})})}})}}makeToolTip("#TTtip-whysafe2");makeToolTip("#TTtipWhy");var traHeight=TurnTojQuery("#TTmainContent").height();traJq("#TT2importSourceSelect").css({maxHeight:traHeight-70+"px"});if(TurnTojQuery.browser.msie){traJq("#TT2importSourceSelect").css({height:"auto"})}}function invalidLOImportSource(impSrc){var domain=impSrc.domain;if(domain=="yahoo.com"||domain=="ymail.com"||domain=="rocketmail.com"){return true}return false}function removeImportSrcLinksEffect(){for(var i=0;i<TurnTo.matchData.importSrc.length;i++){traJq("#TT2importSrcLink"+TurnTo.matchData.importSrc[i].id)[0].style.backgroundColor="#fff"}}function getValueBaseOnDomain(domain){for(var i=0;i<TurnTo.matchData.importSrc.length;i++){var impSrc=TurnTo.matchData.importSrc[i];if(impSrc.domain==domain){return i}}return -1}function delAuthImport(domain,regSource){var uri;var name;if(domain=="hotmail.com"||domain=="msn.com"){uri="/liveServicesAuth?consumer="+domain.substring(0,domain.indexOf(".com"));name="LiveServices"}else{if(domain=="gmail.com"){uri="/oauthDataAuth?consumer=google";name="GoogleData"}else{if(domain=="yahoo.com"||domain=="ymail.com"||domain=="rocketmail.com"){uri="/oauthDataAuth?consumer="+domain.substring(0,domain.indexOf(".com"));name="YahooData"}}}if(uri){if(!regSource){regSource=TurnTo.regSourceTra}return window.open(getUrl("/traServer2"+uri+"&siteKey="+turnToSettings.siteKey+(regSource?"&regSource="+regSource:"")),name,"menubar=no,width=850,height=550,toolbar=no,location=no,scrollbars=yes,resizable=yes")}return null}var delAuthImportMsg;function delAuthPopupIntervalFn(){if(TurnTo.delAuthWin.closed){clearInterval(TurnTo.delAuthPopupInterval);getMatchesAfterDelAuthImport(function(data,ioargs){if(!TurnTo.matchData.user){loginGetMatchesLoaded(data,ioargs)}else{getMatchesLoaded(data,ioargs)}getDelAuthImportMsg(function(data,ioargs){TurnTo.matchData.delAuthImportMsg=data.delAuthImportMsg;getImportHistory(function(){toggleSubWindow("none");if(TurnTo.matchData.user&&(TurnTo.matchData.user.isPreUser||TurnTo.matchData.user.hasDefaultPassword)){displayReg()}else{displayImportSourceSelectScreen({showConfirm:true})}})})})}}function getDelAuthImportMsg(callback){xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/traServer2/getDelAuthImportMsg"),load:function(data,ioargs){if(callback){callback(data,ioargs)}},error:function(data,ioargs){displayErrorsInSubscreen(data.errors)}})}function getMatchesAfterDelAuthImport(callback,useCallbackInError){xhr("GET",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/traServer2/getMatchesAfterDelAuthImport"),load:function(data,ioargs){if(callback){callback(data,ioargs)}if(data.user){setLocalLoginCookie(data)}},error:function(data,ioargs){if(useCallbackInError){callback(data,ioargs)}else{if(data.errors){displayErrorsInSubscreen(data.errors)}}}})}function addEmailAddresses(p,callbackF){xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/traServer2/addEmailAddresses"),postData:toJson(p),contentType:"text/text",load:function(data,ioargs){if(data.errors){displayErrorsInSubscreen(data.errors)}else{if(callbackF){callbackF(data)}}},error:function(data,ioargs){if(data.errors){displayErrorsInSubscreen(data.errors)}}})}TurnTo.displayAdditionalEmailInputScreen=displayAdditionalEmailInputScreen;function displayAdditionalEmailInputScreen(){var q=insertScreen("#TTSubScreen",TurnToHTML.addMoreEmails,{matches:TurnTo.matchData,getUrl:getUrl,hasEmail:userHasEmail()});traJq("#TTaddMoreEmailsForm")[0].action="javascript:void()";traJq("#TTaddMoreEmailsForm")[0].onsubmit=function(e){var p={};var dataToSubmit=false;traJq("#TTaddMoreEmailsForm").find("input").each(function(i){if(this.name.indexOf("TTaddEmail_")>-1){clearErrorFromField(this.id);if(this.ttInputClicked){p[this.name]=TurnTojQuery.trim(this.value);if(p[this.name]){dataToSubmit=true}}}});if(dataToSubmit){addEmailAddresses(p,function(data){if(data.uIdents){TurnTo.matchData.user.uIdents=data.uIdents}toggleSubWindow("none")})}else{toggleSubWindow("none")}stopDefault(e)};traJq("#TTaddMoreEmailsForm-moreLn")[0].onclick=function(e){var contnr=traJq("#TTmoreEmails");var currNum=contnr.children().length;contnr.append(TurnToHTML.additionalEmail({i:currNum}));if(currNum>5){traJq("#TTaddMoreEmailsForm-moreLn").hide()}};toggleSubWindow("block")}TurnTo.displayFindPurchaseEmailInputScreen=displayFindPurchaseEmailInputScreen;function displayFindPurchaseEmailInputScreen(){var q=insertScreen("#TTSubScreen",TurnToHTML.findPurchasesEmails,{matches:TurnTo.matchData,getUrl:getUrl,hasEmail:userHasEmail()});traJq("#TTaddMoreEmailsForm")[0].action="javascript:void()";traJq("#TTaddMoreEmailsForm")[0].onsubmit=function(e){var p={};var emails=[];var dataToSubmit=false;traJq("#TTaddMoreEmailsForm").find("input").each(function(i){if(this.name.indexOf("TTaddEmail_")>-1){clearErrorFromField(this.id);if(this.ttInputClicked){p[this.name]=TurnTojQuery.trim(this.value);if(p[this.name]){dataToSubmit=true;emails[emails.length]=this.value}}}});if(dataToSubmit){addEmailAddresses(p,function(data){if(data.uIdents){TurnTo.matchData.user.uIdents=data.uIdents;for(var i=0;i<emails.length;i++){var email=emails[i];var index=getIdentIndexInUser(email);if(index!=-1){var id=TurnTo.matchData.user.uIdents[index].id;addLIMessage({screen:"unconfirmedEmailMessage",email:email,id:id});traJq("#TT2resendEmailConfirmation"+id)[0].email=email;traJq("#TT2resendEmailConfirmation"+id)[0].onclick=function(){resendEmailConfirmation(this.email)}}}}dispalyConfirmEmailsReminderScreenForPurchases(emails)})}else{toggleSubWindow("none")}stopDefault(e)};toggleSubWindow("block")}function sendForwardMatchInvitationEmail(suid,shid){var p={};p.suid=suid;p.shid=shid;xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/traServer2/sendForwardMatchInvitationEmail"),postData:toJson(p),contentType:"text/text",load:function(data,ioargs){setLoggedInItemMessage({screen:"forwardMatchInvitationEmailSentMessage",suid:data.suid})},error:function(data,ioargs){}})}function dispalyConfirmEmailsReminderScreenForPurchases(emails){var q=insertScreen("#TTSubScreen",TurnToHTML.confirmEmailsReminderForPurchases,{emails:emails})}function displayForwardInviteScreen(e,customMessage){var q=insertScreen("#TTSubScreen",TurnToHTML.inviteForwardMatches,{getUrl:getUrl});traJq("#TTInviteForwardMatches")[0].onsubmit=function(e){stopDefault(e);var p={};xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/traServer2/sendForwardMatchesInvitationEmail"),postData:toJson(p),contentType:"text/text",load:function(data,ioargs){var confMsg;if(data.shareeCount==0){confMsg="No invitations have been sent out."}else{if(data.shareeCount==1){confMsg="Great. We've asked this friend if they want to connect with you. We'll let you know when we hear back from them."}else{confMsg="Great. We've asked these "+data.shareeCount+" friends if they want to connect with you. We'll let you know when we hear back from them."}}displayConfirmationScreen({message:confMsg})}})};if(customMessage){var txtMessage=traJq("#TTinviteMessage")[0];inputFocus(txtMessage,function(inputEl){inputEl.addClass("TTinviteTextareaBig").removeClass("TTinviteTextarea")});txtMessage.value=customMessage}stopDefault(e);toggleSubWindow("block")}function displayConfirmationScreen(options){insertScreen("#TTSubScreen",TurnToHTML.confirmationMessageScreen,options);traJq("#confirmationMessageScreenBtn")[0].onclick=function(){toggleSubWindow("none")};toggleSubWindow("block")}function stripString(s){var cs=s.replace(/<\/?[^>]+>/gi,"");cs=TurnTojQuery.trim(cs);return cs}function displayImport(num,options){if(typeof options=="undefined"||!options){options={}}insertScreen("#TT2ImportSrcForm",TurnToHTML.importContactsForm,{importSrc:importSrc[num],getUrl:getUrl,getStaticUrl:getStaticUrl});traJq("#TTimportForm")[0].onsubmit=function(e){doSubmitImportForm(e,options)}}function doSubmitRegForm2(e,callbackF){var p={};if(traJq("#TTfirstName").length>0){if(traJq("#TTfirstName")[0].className=="TTdefaultInput"){p.TTfirstName=traJq("#TTfirstName")[0].ttInputClicked?traJq("#TTfirstName")[0].value:""}else{p.TTfirstName=traJq("#TTfirstName")[0].value}}if(traJq("#TTlastName").length>0){if(traJq("#TTlastName")[0].className=="TTdefaultInput"){p.TTlastName=traJq("#TTlastName")[0].ttInputClicked?traJq("#TTlastName")[0].value:""}else{p.TTlastName=traJq("#TTlastName")[0].value}}p.TTpassword=traJq("#TTpassword")[0].value;p.TTrePassword=traJq("#TTrePassword")[0].value;if(traJq("#TTregSource").length>0){p.regSource=traJq("#TTregSource")[0].value}if(!p.regSource){p.regSource=TurnTo.regSourceTra}if(!TurnTo.matchData.user&&p.regSource!=TurnTo.regSourceAskQuestion){p.TTcaptcha=traJq("#TTcaptcha")[0].ttInputClicked?traJq("#TTcaptcha")[0].value:""}if(traJq("#TTemail").length>0){p.TTemail=traJq("#TTemail")[0].ttInputClicked?traJq("#TTemail")[0].value:""}if(TurnTo.lastUploadedImageId){p.uploadId=TurnTo.lastUploadedImageId}if(traJq("#TT2regCatItmId").length>0){p.regCatItmId=traJq("#TT2regCatItmId").val()}if(traJq("#TT2regIid").length>0){p.regIid=traJq("#TT2regIid").val()}traJq("#TTregSubmit").hide();traJq("#TTregSubmitLoading").show();xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/secure/traServer2/registerFullUser"),postData:toJson(p),contentType:"text/text",load:function(data,ioArgs){if(data.errors){traJq("#TTregSubmitLoading").hide();traJq("#TTregSubmit").show();displayErrorsInSubscreen(data.errors)}else{if(callbackF){callbackF(data,ioArgs)}}}});stopDefault(e)}function doSubmitRegForm(e,callbackF){var p={};if(traJq("#TTfirstName").length>0){if(traJq("#TTfirstName")[0].className=="TTdefaultInput"){p.TTfirstName=traJq("#TTfirstName")[0].ttInputClicked?traJq("#TTfirstName")[0].value:""}else{p.TTfirstName=traJq("#TTfirstName")[0].value}}if(traJq("#TTlastName").length>0){if(traJq("#TTlastName")[0].className=="TTdefaultInput"){p.TTlastName=traJq("#TTlastName")[0].ttInputClicked?traJq("#TTlastName")[0].value:""}else{p.TTlastName=traJq("#TTlastName")[0].value}}p.TTpassword=traJq("#TTpassword")[0].value;p.TTrePassword=traJq("#TTrePassword")[0].value;if(!TurnTo.matchData.user){p.TTcaptcha=traJq("#TTcaptcha")[0].ttInputClicked?traJq("#TTcaptcha")[0].value:""}if(traJq("#TTemail").length>0){p.TTemail=traJq("#TTemail")[0].ttInputClicked?traJq("#TTemail")[0].value:""}if(TurnTo.lastUploadedImageId){p.uploadId=TurnTo.lastUploadedImageId}if(traJq("#TTregSource").length>0){p.regSource=traJq("#TTregSource")[0].value}if(!p.regSource){p.regSource=TurnTo.regSourceTra}traJq("#TTregSubmit").hide();traJq("#TTregSubmitLoading").show();xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/secure/traServer2/registerFullUser"),postData:toJson(p),contentType:"text/text",load:function(data,ioargs){if(data.errors){traJq("#TTregSubmitLoading").hide();traJq("#TTregSubmit").show();displayErrorsInSubscreen(data.errors)}else{toggleSubWindow("none");var delAuthImportMsg=TurnTo.matchData.delAuthImportMsg;var importHistory=TurnTo.matchData.importHistory;loginGetMatchesLoaded(data,ioargs);TurnTo.matchData.delAuthImportMsg=delAuthImportMsg;TurnTo.matchData.importHistory=importHistory;if(!TurnTo.matchData.user.isPreUser){}TurnToTrack.newUserRegistration(TurnTo.matchData.user.id);if(callbackF){callbackF()}else{displayImportSourceSelectScreen({showConfirm:true,fromRegForm:true})}}}});stopDefault(e)}function doSubmitImportForm(e,options){stopDefault(e);traJq("#TTimportSubmit").hide();traJq("#TTimportSubmitLoading").show();var p={};var uNameEl=traJq("#TTimportName")[0];p.username=uNameEl.ttInputClicked?uNameEl.value:"";p.password=traJq("#TTimportPwd")[0].value;p.sourceId=traJq("#TTimportSrc")[0].value;xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/secure/traServer2/importContacts"),postData:toJson(p),contentType:"text/text",load:function(data,ioargs){if(data.errors){traJq("#TTimportSubmitLoading").hide();traJq("#TTimportSubmit").show();displayErrorsInSubscreen(data.errors)}else{if(options.onImportFinished){options.onImportFinished(data,ioargs)}else{if(!TurnTo.matchData.user){loginGetMatchesLoaded(data,ioargs)}else{TurnTo.matchData.delAuthImportMsg=data.delAuthImportMsg;TurnTo.matchData.importHistory=data.importHistory;TurnTo.matchData.user.uIdents=data.uIdents}displayImportSourceSelectScreen({showConfirm:true});if(TurnTo.matchData.user&&(TurnTo.matchData.user.isPreUser||TurnTo.matchData.user.hasDefaultPassword)){displayReg()}}}}})}function resendEmailConfirmation(emailAddr,callbackF){xhr("POST",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/sendconfirm"),contentType:"text/text",load:function(response,ioArgs){if(callbackF){callbackF()}else{if(response.message){displayConfirmationScreen({message:response.message})}}},error:function(data,ioArgs){},postData:toJson({email:emailAddr})})}var TRA_ERROR_MESSAGES=new Array();TRA_ERROR_MESSAGES[3]="This email is already in use.";TRA_ERROR_MESSAGES[5]="Could not complete import.";TRA_ERROR_MESSAGES[6]="Please enter a first name";TRA_ERROR_MESSAGES[7]="Please enter a last name";TRA_ERROR_MESSAGES[8]="Please enter a valid email address";TRA_ERROR_MESSAGES[9]="Please enter a password";TRA_ERROR_MESSAGES[10]="The passwords you entered did not match";TRA_ERROR_MESSAGES[11]="The security code you entered did not match";function displayErrorsInSubscreen(errors){var ssQ=traJq("#TTSubDialogErrors").html("");for(var e=0;e<errors.length;e++){var errR=errors[e];if(!errR.message&&!errR.ecode){ssQ.append("<li>"+errR+"</li>")}else{ssQ.append("<li>"+(errR.message?errR.message:TRA_ERROR_MESSAGES[errR.ecode])+"</li>")}if(errR.field){var errFld=traJq("#"+errR.field);errFld.addClass("TTinputBoxError");if(errFld.attr("ttPasswordPair")){traJq("#"+errFld.attr("ttPasswordPair")).addClass("TTinputBoxError")}}}if(errors){ssQ.show()}}function clearErrorFromField(elId){traJq("#"+elId).removeClass("TTinputBoxError")}function moreLessifier1(displayParams,rowId){if(displayParams.tot>0){displayParams.rowsQ=traJq(rowId)}if(displayParams.tot>displayParams.max){displayParams.moreRowsQ=traJq(TurnToHTML.moreLessBlock);displayParams.moreRowsQ.hide()}}function moreLessifier2(x,q,displayParams){if(x==displayParams.max){var moreQ=traJq(TurnToHTML.moreLink({moreCount:(displayParams.tot-displayParams.max)}));moreQ[0].onclick=function(e){traJq(this).next(".TTmoreLess").show();traJq(this).hide()};displayParams.rowsQ.append(moreQ)}if(x<displayParams.max){displayParams.rowsQ.append(q)}else{displayParams.moreRowsQ.append(q)}}function moreLessifier3(displayParams){if(displayParams.tot>displayParams.max){var lessQ=traJq(TurnToHTML.lessLink);lessQ[0].onclick=function(e){traJq(this).parent().hide();traJq(this).parent().prev(".TTmoreLink").show()};displayParams.moreRowsQ.append(lessQ);displayParams.rowsQ.append(displayParams.moreRowsQ)}}function showZipForm(){if(TurnTo.matchData.user){TurnTo.zip=TurnTo.matchData.user.postalCode}if(traJq("#TT2zipbox")&&traJq("#TT2zipbox").length>0){var zipbox=traJq("#TT2zipbox")[0];zipbox.innerHTML="";zipbox.innerHTML=TurnToHTML.zipForm({turnToSettings:turnToSettings,zcount:"0",pcount:"0",zip:TurnTo.zip});if(TurnTo.zip){traJq("#TT2noZipP").hide();traJq("#TT2zipReadyP").show();traJq("#TT2changeZipLink").html("Change Zip");traJq("#TT2zipInput").hide()}else{traJq("#TT2noZipP").show();traJq("#TT2zipReadyP").hide();traJq("#TT2changeZipLink").html("Enter Zip");traJq("#TT2zipInput").show()}if(TurnTo.matchData.user){traJq("#TTzipForm")[0].onsubmit=makeLiZipSubmit(traJq("#TTzip")[0])}else{traJq("#TTzipForm")[0].onsubmit=submitZip}traJq("#TTzipForm")[0].action="javascript:void()";traJq("#TT2changeZipLink").click(function(){traJq("#TT2zipInput").show()})}}function submitZip(e){var mess=traJq("#TTZipFormMessage")[0];mess.innerHTML="";var p={};var v=traJq("#TTzip")[0];if(!v.ttInputClicked){mess.style.display="block";mess.innerHTML="Please enter your zip code.";return false}else{mess.style.display="none"}p.zip=v.value;TurnTo.zip=p.zip;setCookie(turnToSettings.zipOnlyCookie,p.zip);getMatches(populateTra);traJq("#TT2userZip").html(TurnTo.zip);traJq("#TT2zipInput").hide();traJq("#TT2noZipP").hide();traJq("#TT2zipReadyP").show();traJq("#TT2changeZipLink").html("Change Zip");stopDefault(e);return false}function getZipMatches(zip,nocache){nocache=true;TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getZipMatches")+"&zip="+zip,load:function(data,ioArgs){getZipMatchesLoaded(data,ioArgs)},error:function(data,ioArgs){var message="getZipMatches XHR - Status: "+data.status+" Error: "+data.error;if(data.status==401){}else{}getZipMatchesLoaded(data,ioArgs)},timeout:turnToSettings.timeout})}function getZipMatchesLoaded(data,ioArgs){TurnTo.matchData=data;TurnTo.traDataLoaded=true;if(data.status!=200&&data.status!=401){TurnTo.error=data.error;TurnTo.status=data.status}setCookie(turnToSettings.zipOnlyCookie,TurnTo.matchData.postalCode);if(hasErrors()){return}displayZipMatches()}function resize(){var topOffset=turnToSettings.traTopOffset;TurnTojQuery("#TTtraWindow").css({top:TurnTojQuery(document).scrollTop()+6+"px",left:((TurnTojQuery(window).width()/2)-350)+"px"});var mHei=(TurnTojQuery(window).height()-(TurnTo.loggedIn?130:100));if(mHei<0){mHei=0}TurnTojQuery("#TTmainContent")[0].style.maxHeight=mHei+"px";if(TurnTojQuery.browser.msie){TurnTojQuery("#TTmainContent")[0].style.height=mHei+"px"}}function resizeSubWindow(id){var currentSubWindowHeight=traJq("#"+id).height();var traHeight=TurnTojQuery("#TTmainContent").height();if(currentSubWindowHeight>traHeight-70){traJq("#"+id).css({height:(traHeight-70)+"px"})}else{traJq("#"+id).css({height:"auto"})}}function toggleMainWindow(dis){var t=(dis)?dis:traWindow.style.display;if(t=="none"){resize();TurnTojQuery(".TT2sectionHeader").show();traJq("#TT2footerFeedback").show();traJq("#TTsignUpLink3").show()}if(t=="block"&&TurnTo.loggedIn&&(TurnTo.matchData&&TurnTo.matchData.counts&&TurnTo.matchData.counts.userItems>0)){if(!TurnTo.matchData.user.hasSharingPref){TurnTo.noSharingPrefExitFlow=true;hideMainWindow();traSubWindow.overlay.display="none";getItems4User(function(){displayPastPurchases(true)});return}else{if(TurnTo.newfbuserExitFlow){TurnTo.newfbuserExitFlow=false;hideMainWindow();traSubWindow.overlay.display="none";getItems4User(function(){displayPastPurchases(true)});TurnTo.newfbuserExitMessage=true;return}}}traWindow.style.display=(t=="none")?"block":"none";traWindow.overlay.display=(t=="none")?"block":"none"}function toggleSubWindow(pos){if(pos=="block"){}var current=traSubWindow.style.display;if(pos=="none"||(current=="block"&&pos!="block")){if(TurnTo.postPurchaseFlowStarted||TurnTo.pteFlowStarted||TurnTo.noSharingPrefExitFlow){closeOrderConfPurchases();return}}if(pos=="block"||pos=="none"){if(pos==current){return}else{traSubWindow.overlay.display=pos;traSubWindow.style.display=pos}}else{traSubWindow.style.display=(current=="none")?"block":"none";traSubWindow.overlay.display=(traSubWindow.overlay.display=="none")?"block":"none"}}function replaceTrimLineBreaks(str){return str.replace(/\n{2,}/g,"<br/><br/>").replace(/\n/g,"<br/>")}function toggleBubbleWindow(pos){var current=traBubbleWindow.style.display;if(pos=="block"||pos=="none"){if(pos==current){return}else{traBubbleWindow.style.display=pos;traSubWindow.overlay.display=pos}}else{traBubbleWindow.style.display=(current=="none")?"block":"none";traSubWindow.overlay.display=(current=="none")?"block":"none"}}function display(elem,show,empty){if(empty){traJq(elem)[0].innerHTML=""}if(show){traJq(elem)[0].style.display="block"}else{traJq(elem)[0].style.display="none"}}function makeToolTip(el){var tipTarget=traJq(el);if(tipTarget.length==0){return}tipTarget[0].onmouseover=function(event){toolTipQ[0].innerHTML=traJq("#"+this.id+"-tip")[0].innerHTML;var h=toolTipQ.height();var ol=5;var ot=22;var main=(traJq(this).closest("#TTmainContent")[0])?true:false;if(!main){ot=0}if(toolTipQ[0].style.display!="block"){toolTipQ[0].style.display="block";var left=this.parentNode.offsetLeft+ol;var top=this.parentNode.offsetTop+2;var right="auto";toolTipQ.css({left:left,right:right,top:top,width:"300px"})}}}function connectWithFacebook(e,options){if(typeof options=="undefined"||!options){options={}}var linkParam=options.link?"&link=true":"";var photoParam=options.photo?"&photo=true":"";var postParam=options.post?"&post=true":"";var regSource="&regSource="+(options.regSource?options.regSource:TurnTo.regSourceTra);var svisitParams=TurnToTrack.makeVisitorPartsQS(TurnToTrack.getVisitorParts());var ttFbUrl=getUrl("/traServer2/connectWithFacebook?siteKey="+turnToSettings.siteKey+linkParam+svisitParams+photoParam+postParam+regSource);if(TurnTo.fbConnectPopupInterval){clearInterval(TurnTo.fbConnectPopupInterval)}TurnTo.fbwin=window.open(ttFbUrl,"ConnectWithFacebook","height=550,width=650,status=1,toolbar=0,menubar=0,resizable=1,scrollbars=1");TurnTo.fbConnectPopupInterval=setInterval(function(){fbConnectPopupIntervalFn(options)},500);stopDefault(e)}function fbConnectPopupIntervalFn(options){if(TurnTo.fbwin.closed){if(options.showSubSpinner){insertScreen("#TTSubScreen",TurnToHTML.subSpinScreen,{description:options.subSpinnerDes})}clearInterval(TurnTo.fbConnectPopupInterval);TurnTo.loginBubbleShowing=traBubbleWindow.style.display=="block";if(TurnTo.loginBubbleShowing&&!options.keepBubbleWindow){toggleBubbleWindow("none")}if(!options.keepSubwindow){toggleSubWindow("none")}getMatches(function(data,ioargs){if(!options.noMatchesShow){getMatchesLoaded(data,ioargs)}else{TurnTo.traDataLoaded=false;if(!processResponse(data,ioargs)){return}}if(options.onConnectClose){options.onConnectClose()}else{if(TurnTo.matchData.newfbuser){TurnTo.newfbuserExitFlow=true}}if(!TurnTo.matchData.user&&TurnTo.loginBubbleShowing&&!options.keepBubbleWindow){showUserLogin()}},true,false,options)}}function getFbPostToWallGranted(callback){xhr("GET",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/traServer2/getFbPostToWallGranted"),contentType:"text/text",load:function(data,ioargs){if(data.errors){}else{TurnTo.matchData.user.fbPostToWallGranted=data.fbPostToWallGranted}if(callback){callback()}},error:function(data,ioargs){var message="getFbPostToWallGranted XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)}})}function getImportHistory(callback){xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/traServer2/getImportHistory"),contentType:"text/text",load:function(data,ioargs){if(data.errors){}else{if(data.importHistory){TurnTo.matchData.importHistory=data.importHistory;TurnTo.matchData.totalFriends=data.totalFriends}}if(callback){callback()}}})}function userHasEmail(){if(!TurnTo.matchData||!TurnTo.matchData.user){return false}for(var pi=0;pi<TurnTo.matchData.user.uIdents.length;pi++){if(TurnTo.matchData.user.uIdents[pi].type==1){return true}}return false}function inputFocus(inputEl,callback){if(inputEl.ttInputClicked){return}var inputElJQ=traJq(inputEl);var isPassword=inputElJQ.attr("type")=="password";var passwordPair=inputElJQ.attr("ttPasswordPair");if(!isPassword&&passwordPair){var pf=traJq("#"+passwordPair);inputElJQ.hide();pf.show();pf.focus();if(callback){callback(inputElJQ)}return}inputElJQ.removeClass("TTdefaultInput");inputEl.value="";inputEl.ttInputClicked=true;if(callback){callback(inputElJQ)}}function inputLoseFocus(inputEl,callback){inputEl.value=TurnTojQuery.trim(inputEl.value);if(inputEl.value!=""){return}var inputElJQ=traJq(inputEl);var isPassword=inputElJQ.attr("type")=="password";var passwordPair=inputElJQ.attr("ttPasswordPair");if(isPassword){var textPairEl=traJq("#"+passwordPair);inputElJQ.hide();textPairEl.show();if(callback){callback(inputElJQ)}return}inputEl.value=inputElJQ.attr("ttInputLabelText");inputElJQ.addClass("TTdefaultInput");inputEl.ttInputClicked=false;if(callback){callback(inputElJQ)}}function inputUserImage(inputEl,callback){if(inputEl.value==""){return}if(TurnTo.checkImgInterval){clearInterval(TurnTo.checkImgInterval)}var uploadId=(new Date()).getTime();TurnTo.lastUploadedImageId=uploadId;traJq("#TTregForm").attr("action",getUrl("/traServer2/uploadTmpUserImage?uploadId="+uploadId));traJq("#TTregForm").attr("method","post");traJq("#TTregForm").attr("enctype","multipart/form-data");traJq("#TTregForm").attr("encoding","multipart/form-data");var hiddenIframe=document.createElement("iframe");hiddenIframe.setAttribute("id",uploadId);hiddenIframe.setAttribute("name",uploadId);hiddenIframe.setAttribute("style","display:none");traJq("#TTregForm").append(hiddenIframe);traJq("#TTregForm").attr("target",uploadId);traJq("#TTregForm").submit();traJq("#TT2userImg").hide();traJq("#TT2userImgLoading").show();var submitF=traJq("#TTregForm")[0].onsubmit;traJq("#TTregForm")[0].onsubmit=function(){return false};traJq("#TT2userImg")[0].onload=function(){clearInterval(TurnTo.checkImgInterval);traJq("#"+uploadId).remove();traJq("#TTregForm")[0].onsubmit=submitF;traJq("#TT2userImg").show();traJq("#TT2userImgLoading").hide()};TurnTo.checkImgInterval=setInterval(function(){traJq("#TT2userImg").attr("src",getUrl("/traServer2/displayTmpUserImage?uploadId="+uploadId+"&t="+(new Date()).getTime()))},1000)}function startPostPurchaseFlow(turnToFeed){TurnTo.postPurchaseFlowStarted=true;TurnTo.turnToFeed=turnToFeed;if(!TurnTo.params.turntoflow){TurnTo.params.turntoflow="orderconf"}getItems4Conf(turnToFeed,showOrderConfPurchases)}function getItems4Conf(turnToFeed,callbackF){TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getItems4Conf")+"&cid="+cid+"&sig="+turnToFeed.sig+"&orderId="+turnToFeed.orderId+"&email="+encodeURIComponent(turnToFeed.email),load:function(data,ioArgs){TurnTo.matchData.items4Conf=data.items;TurnTo.matchData.allTargets=data.allTargets;TurnTo.matchData.siteUser=data.siteUser;if(data.user||TurnTo.matchData.user){if(data.user){TurnTo.matchData.user=data.user}TurnTo.matchData.user.items=data.items;TurnTo.matchData.user.raveTargets=data.raveTargets;TurnTo.matchData.user.siteSharing=data.siteSharing}if(data.logoutUser){TurnTo.matchData.user=null}if(callbackF){callbackF()}},error:function(data,ioArgs){var message="getItems4Conf XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function getItems4Pte(eml,tri,iuid,callbackF){TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getItems4Pte")+"&cid="+cid+"&eml="+eml+(tri?"&tri="+tri:"")+(iuid?"&iuid="+iuid:""),load:function(data,ioArgs){TurnTo.matchData.items4Conf=data.items;TurnTo.matchData.siteUser=data.siteUser;TurnTo.matchData.invitor=data.invitor;if(data.user){TurnTo.matchData.user=data.user}if(data.logoutUser){TurnTo.matchData.user=null}if(callbackF){callbackF()}},error:function(data,ioArgs){var message="getItems4Pte XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function showOrderConfPurchases(){if(TurnTo.matchData.items4Conf&&TurnTo.matchData.items4Conf.length>0){toggleMainWindow();insertScreen("#TTSubScreen",TurnToHTML.pastPurchases,{matches:TurnTo.matchData,getStaticUrl:getStaticUrl,getUrl:getUrl});var tips=traJq("#TT2pastPurchasesTips");if(tips.length>0){tips.click(showPastPurchasesTips)}toggleSubWindow("block");if(TurnTo.matchData.user&&TurnTo.matchData.user.siteSharing){managePastPurchases(false,true)}else{traJq("#TT2managePastPurchaseBtn")[0].onclick=function(e){traJq("#TT2managePastPurchaseBtn").hide();traJq("#TT2managePastPurchaseLoading").show();traJq("#TT2NotManagePastPurchaseLink").hide();if(TurnTo.matchData.user){updateSharing(true,function(){managePastPurchases(false,true);traJq("#TT2pastPurchases").css({maxHeight:(TurnTojQuery(window).height()-150)+"px"});if(TurnTojQuery.browser.msie){traJq("#TT2pastPurchases").css({height:"auto"})}})}else{registerAndLogin(function(){updateSharing(true,function(){managePastPurchases(TurnTo.matchData.newUser,true);traJq("#TT2pastPurchases").css({maxHeight:(TurnTojQuery(window).height()-150)+"px"});if(TurnTojQuery.browser.msie){traJq("#TT2pastPurchases").css({height:"auto"})}});setLocalLoginCookie()})}};traJq("#TT2NotManagePastPurchaseLink")[0].onclick=function(e){updateSharing(false,closeOrderConfPurchases)}}hideMainWindow()}}function registerAndLogin(callbackF){TurnTo.error=null;var p={};if(TurnTo.turnToFeed){p.email=TurnTo.turnToFeed.email;p.orderId=TurnTo.turnToFeed.orderId;p.sig=TurnTo.turnToFeed.sig}else{if(TurnTo.answerData){p.email=TurnTo.answerData.answerer.email}else{if((TurnTo.params.turntoflow=="fmatch"||TurnTo.params.turntoflow=="ppemail")&&TurnTo.params.turntoeml){p.eml=TurnTo.params.turntoeml}}}if(TurnTo.params.turntoflow){p.flow=TurnTo.params.turntoflow}xhr("POST",{iframeProxyUrl:TurnTo.secureIframeProxyUrl,url:xhrUri("/traServer2/registerAndLogin"),postData:toJson(p),contentType:"text/text",load:function(data,ioargs){TurnTo.matchData.newUser=data.newUser;TurnTo.matchData.user=data.user;if(data.newUser){TurnToTrack.newUserRegistration(TurnTo.matchData.user.id)}TurnTo.matchData.user.raveTargets=data.raveTargets;TurnTo.matchData.user.items=TurnTo.matchData.items4Conf;callbackF.call()},error:function(data,ioargs){var message="registerAndLogin XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function hideMainWindow(){TurnTojQuery("#TTtraWindow").css({height:TurnTojQuery(window).height()+"px"});traJq("#TTtraSubWindow").css({top:6+"px"});traJq("#TTtraSubWindow")[0].overlay.display="none";traJq("#TTtraUserStateMain").hide();var maxHeight=(TurnTojQuery(window).height()-220);traJq("#TT2pastPurchases").css({maxHeight:maxHeight+"px"});if(TurnTojQuery.browser.msie){traJq("#TT2pastPurchases").css({height:"auto"});if(traJq("#TT2pastPurchases").height()>maxHeight){traJq("#TT2pastPurchases").css({height:maxHeight+"px"})}}}function closeOrderConfPurchases(){if(TurnTo.loggedIn){TurnTo.matchData.user.hasSharingPref=true}TurnTo.postPurchaseFlowStarted=false;TurnTo.pteFlowStarted=false;TurnTo.noSharingPrefExitFlow=false;toggleMainWindow();toggleSubWindow("none");TurnTojQuery("#TTtraWindow").css("height","auto");traJq("#TTtraUserStateMain").show();traJq("#TTtraSubWindow").css({top:75+"px"});traJq("#TT2pastPurchases").css({maxHeight:""});if(TurnTojQuery.browser.msie){traJq("#TT2pastPurchases").css({height:""})}}function getIdentIndexInUser(ident){if(TurnTo.matchData.user&&TurnTo.matchData.user.uIdents){for(var i=0;i<TurnTo.matchData.user.uIdents.length;i++){if(TurnTo.matchData.user.uIdents[i].ident==ident){return i}}}return -1}function getParameters(){var vars=[],name,value;if(window.location.href.indexOf("?")>0){var hashes=window.location.href.slice(window.location.href.indexOf("?")+1).split("&");for(var i=0;i<hashes.length;i++){name=hashes[i].substring(0,hashes[i].indexOf("="));value=hashes[i].substring(hashes[i].indexOf("=")+1);vars.push(name);vars[name]=value}}return vars}function preloadImages(){if(TurnTo.loggedIn){var preloadImage7=new Image();preloadImage7.src=getStaticUrl("/tra2/images/rollover/ask1li.png");var preloadImage8=new Image();preloadImage8.src=getStaticUrl("/tra2/images/rollover/ask1lihover.png");var preloadImage9=new Image();preloadImage9.src=getStaticUrl("/tra2/images/rollover/ask1liright.png");var preloadImage10=new Image();preloadImage10.src=getStaticUrl("/tra2/images/rollover/ask1lihoverright.png");var preloadImage13=new Image();preloadImage13.src=getStaticUrl("/tra2/images/rollover/item1li.png");var preloadImage14=new Image();preloadImage14.src=getStaticUrl("/tra2/images/rollover/item1lihover.png");var preloadImage18=new Image();preloadImage18.src=getStaticUrl("/tra2/images/rollover/noask-li.png");var preloadImage19=new Image();preloadImage19.src=getStaticUrl("/tra2/images/rollover/noask-li-r.png")}else{var preloadImage=new Image();preloadImage.src=customCopy.friendFbHeader1;var preloadImage2=new Image();preloadImage2.src=customCopy.friendFbHeader2;var preloadImage3=new Image();preloadImage3.src=getStaticUrl("/tra2/images/rollover/ask1.png");var preloadImage4=new Image();preloadImage4.src=getStaticUrl("/tra2/images/rollover/ask1hover.png");var preloadImage5=new Image();preloadImage5.src=getStaticUrl("/tra2/images/rollover/ask1right.png");var preloadImage6=new Image();preloadImage6.src=getStaticUrl("/tra2/images/rollover/ask1hoverright.png");var preloadImage11=new Image();preloadImage11.src=getStaticUrl("/tra2/images/rollover/item1.png");var preloadImage12=new Image();preloadImage12.src=getStaticUrl("/tra2/images/rollover/item1hover.png");var preloadImage15=new Image();preloadImage15.src=getStaticUrl("/tra2/images/rollover/noask-item1-hover.png");var preloadImage16=new Image();preloadImage16.src=getStaticUrl("/tra2/images/rollover/noask-item1.png");var preloadImage17=new Image();preloadImage17.src=getStaticUrl("/tra2/images/rollover/noask.png");var preloadImage20=new Image();preloadImage20.src=getStaticUrl("/tra2/images/rollover/noask-r.png")}}function regExpSearchEmail(content){var emailRegEx=/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/im;return content.search(emailRegEx)}function is_child_of(parent,child){if(child!=null){while(child.parentNode){if((child=child.parentNode)==parent){return true}}}return false}function fixOnMouseOut(element,boxElement,event){if(!event){event=window.event}var current_mouse_target=null;if(event.toElement){current_mouse_target=event.toElement}else{if(event.relatedTarget){current_mouse_target=event.relatedTarget}}if(!is_child_of(element,current_mouse_target)&&element!=current_mouse_target){boxElement.style.display="none"}}function makeRaves(jq,linkClass,boxFn,mouseAreaFn,skuIsItemId){jq.find(linkClass).each(function(i){var box=boxFn(this);var mouseArea=mouseAreaFn(this);this.onmouseover=function(e){var sku=box.eq(0).attr("sku");if(sku){traJq(".ttRaveCommentInsert").remove();var comBox=traJq(box).find(".TTraveCommentBox");if(comBox[0]){comBox[0].innerHTML="<div class='ttRaveCommentInsert'></div>";getRaveCommentsForSku(sku,skuIsItemId)}}box.eq(0).show();mouseArea[0].onmouseout=function(e){fixOnMouseOut(mouseArea[0],box[0],e)}}})}function getRaveCommentsForSku(sku,skuIsItemId){TurnTo.error=null;xhr("GET",{iframeProxyUrl:TurnTo.iframeProxyUrl,url:xhrUri("/traServer2/getRaveComments4sku")+"&sku="+sku+"&skuIsId="+skuIsItemId,load:function(data,ioArgs){getRaveCommentsForSkuLoaded(data,ioArgs)},error:function(data,ioArgs){var message="getMatches XHR - Status: "+data.status+" Error: "+data.error;console.debug(message)},timeout:turnToSettings.timeout})}function getRaveCommentsForSkuLoaded(data,ioArgs){if(data.status!=200){TurnTo.error=data.error;TurnTo.status=data.status}if(hasErrors()){return}var com="";for(x=0;x<data.comments.length;x++){if(x>0){com+="<br><br>"}com+="&#147;";com+=data.comments[x];com+=" &#148;"}var b=traJq(".ttRaveCommentInsert")[0];var pb=traJq(b).parent()[0];pb.innerHTML=com;var sku=traJq(pb).closest(".TTraveOuterBox");sku.eq(0).removeAttr("sku")}function doNothing(){doNothing.prototype.display=function(){};doNothing.prototype.afterOpenLinks=function(){}}};TurnTo.init();if(TurnTo.unsupport()){TurnTo.setup=function(){};TurnTo.insertGlobalTeaser=function(){};TurnTo.insertItemTeaser=function(){};TurnTo.mockDojo()}else{if(!turnToSettings.useSetup){TurnTo.beforeReady();TurnTojQuery(document).ready(TurnTo.onload)}TurnTo.mockDojo()}};
dojo.provide("dojox.io.proxy.xip");dojo.require("dojo.io.iframe");dojo.require("dojox.data.dom");dojox.io.proxy.xip={xipClientUrl:((dojo.config||djConfig)["xipClientUrl"])||dojo.moduleUrl("dojox.io.proxy","xip_client.html"),urlLimit:4000,_callbackName:(dojox._scopeName||"dojox")+".io.proxy.xip.fragmentReceived",_state:{},_stateIdCounter:0,_isWebKit:navigator.userAgent.indexOf("WebKit")!=-1,send:function(c){var b=this.xipClientUrl;if(b.split(":")[0].match(/javascript/i)||c._ifpServerUrl.split(":")[0].match(/javascript/i)){return}var a=b.indexOf(":");var d=b.indexOf("/");if(a==-1||d<a){var e=window.location.href;if(d==0){b=e.substring(0,e.indexOf("/",9))+b}else{b=e.substring(0,(e.lastIndexOf("/")+1))+b}}this.fullXipClientUrl=b;if(typeof document.postMessage!="undefined"){document.addEventListener("message",dojo.hitch(this,this.fragmentReceivedEvent),false)}this.send=this._realSend;return this._realSend(c)},_realSend:function(b){var c="XhrIframeProxy"+(this._stateIdCounter++);b._stateId=c;var a=b._ifpServerUrl+"#0:init:id="+c+"&client="+encodeURIComponent(this.fullXipClientUrl)+"&callback="+encodeURIComponent(this._callbackName);this._state[c]={facade:b,stateId:c,clientFrame:dojo.io.iframe.create(c,"",a),isSending:false,serverUrl:b._ifpServerUrl,requestData:null,responseMessage:"",requestParts:[],idCounter:1,partIndex:0,serverWindow:null};return c},receive:function(e,h){var c={};var j=h.split("&");for(var d=0;d<j.length;d++){if(j[d]){var g=j[d].split("=");c[decodeURIComponent(g[0])]=decodeURIComponent(g[1])}}var a=this._state[e];var k=a.facade;k._setResponseHeaders(c.responseHeaders);if(c.status==0||c.status){k.status=parseInt(c.status,10)}if(c.statusText){k.statusText=c.statusText}if(c.responseText){k.responseText=c.responseText;var f=k.getResponseHeader("Content-Type");if(f){var b=f.split(";")[0];if(b.indexOf("application/xml")==0||b.indexOf("text/xml")==0){k.responseXML=dojox.data.dom.createDocument(c.responseText,f)}}}k.readyState=4;this.destroyState(e)},frameLoaded:function(f){var c=this._state[f];var b=c.facade;var e=[];for(var d in b._requestHeaders){e.push(d+": "+b._requestHeaders[d])}var a={uri:b._uri};if(e.length>0){a.requestHeaders=e.join("\r\n")}if(b._method){a.method=b._method}if(b._bodyData){a.data=b._bodyData}this.sendRequest(f,dojo.objectToQuery(a))},destroyState:function(c){var b=this._state[c];if(b){delete this._state[c];var a=b.clientFrame.parentNode;a.removeChild(b.clientFrame);b.clientFrame=null;b=null}},createFacade:function(){if(arguments&&arguments[0]&&arguments[0].iframeProxyUrl){return new dojox.io.proxy.xip.XhrIframeFacade(arguments[0].iframeProxyUrl)}else{return dojox.io.proxy.xip._xhrObjOld.apply(dojo,arguments)}},sendRequest:function(c,a){var b=this._state[c];if(!b.isSending){b.isSending=true;b.requestData=a||"";b.serverWindow=frames[b.stateId];if(!b.serverWindow){b.serverWindow=document.getElementById(b.stateId).contentWindow}if(typeof document.postMessage=="undefined"){if(b.serverWindow.contentWindow){b.serverWindow=b.serverWindow.contentWindow}}this.sendRequestStart(c)}},sendRequestStart:function(h){var e=this._state[h];e.requestParts=[];var c=e.requestData;var g=e.serverUrl.length;var d=this.urlLimit-g;var f=0;while((c.length-f)+g>this.urlLimit){var b=c.substring(f,f+d);var a=b.lastIndexOf("%");if(a==b.length-1||a==b.length-2){b=b.substring(0,a)}e.requestParts.push(b);f+=b.length}e.requestParts.push(c.substring(f,c.length));e.partIndex=0;this.sendRequestPart(h)},sendRequestPart:function(d){var c=this._state[d];if(c.partIndex<c.requestParts.length){var a=c.requestParts[c.partIndex];var b="part";if(c.partIndex+1==c.requestParts.length){b="end"}else{if(c.partIndex==0){b="start"}}this.setServerUrl(d,b,a);c.partIndex++}},setServerUrl:function(e,d,b){var a=this.makeServerUrl(e,d,b);var c=this._state[e];if(this._isWebKit){c.serverWindow.location=a}else{c.serverWindow.location.replace(a)}},makeServerUrl:function(e,d,b){var c=this._state[e];var a=c.serverUrl+"#"+(c.idCounter++)+":"+d;if(b){a+=":"+b}return a},fragmentReceivedEvent:function(a){if(a.uri.split("#")[0]==this.fullXipClientUrl){this.fragmentReceived(a.data)}},fragmentReceived:function(f){var b=f.indexOf("#");var e=f.substring(0,b);var a=f.substring(b+1,f.length);var d=this.unpackMessage(a);var c=this._state[e];switch(d.command){case"loaded":this.frameLoaded(e);break;case"ok":this.sendRequestPart(e);break;case"start":c.responseMessage=""+d.message;this.setServerUrl(e,"ok");break;case"part":c.responseMessage+=d.message;this.setServerUrl(e,"ok");break;case"end":this.setServerUrl(e,"ok");c.responseMessage+=d.message;this.receive(e,c.responseMessage);break}},unpackMessage:function(e){var f=e.split(":");var g=f[1];e=f[2]||"";var c=null;if(g=="init"){var b=e.split("&");c={};for(var d=0;d<b.length;d++){var a=b[d].split("=");c[decodeURIComponent(a[0])]=decodeURIComponent(a[1])}}return{command:g,message:e,config:c}}};dojox.io.proxy.xip._xhrObjOld=dojo._xhrObj;dojo._xhrObj=dojox.io.proxy.xip.createFacade;dojox.io.proxy.xip.XhrIframeFacade=function(a){this._requestHeaders={};this._allResponseHeaders=null;this._responseHeaders={};this._method=null;this._uri=null;this._bodyData=null;this.responseText=null;this.responseXML=null;this.status=null;this.statusText=null;this.readyState=0;this._ifpServerUrl=a;this._stateId=null};dojo.extend(dojox.io.proxy.xip.XhrIframeFacade,{open:function(b,a){this._method=b;this._uri=a;this.readyState=1},setRequestHeader:function(b,a){this._requestHeaders[b]=a},send:function(a){this._bodyData=a;this._stateId=dojox.io.proxy.xip.send(this);this.readyState=2},abort:function(){dojox.io.proxy.xip.destroyState(this._stateId)},getAllResponseHeaders:function(){return this._allResponseHeaders},getResponseHeader:function(a){return this._responseHeaders[a]},_setResponseHeaders:function(d){if(d){this._allResponseHeaders=d;d=d.replace(/\r/g,"");var b=d.split("\n");for(var c=0;c<b.length;c++){if(b[c]){var a=b[c].split(": ");this._responseHeaders[a[0]]=a[1]}}}}});
TurnTo.unmockDojo();
/*
checksum:
eececc66c8b334cbbbe40ee8bd4e909c
bc079fe83c78259e8fa5969012a14a9
22214fe2b8e6c1a70cd41e5c3ae702ad
ba1e79c23a5d305ffecf5526bea9765d
acf6b58860bc62e6d0e0fed84daf7e2c
fe3946de451693ae961a767c794fd1b9
5d2e8f4d950df75fe8c0f3f6c8c9ab32
*/
