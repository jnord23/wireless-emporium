<!--
function eCheckNum(sn) {
	s = sn.value;
	for (var i = 0; i < s.length; i++) {
		ch = s.substring(i, i + 1)
		if (((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") || (ch == " ")  || (ch == ".")  || (ch == "-")   || (ch == "&")  ||  (ch == ",")  ||  (ch == "_")  ||  (ch == "(")  ||  (ch == ")")  ||  ch == "@")) {
			return false;
			theForm.elements[eCheckNum.arguments[i]].focus();
			//(ch >= "0" && ch <= "9") ||
		}
	}
	return true;
}

function CheckNum(theForm) {
	for (var i=1; i<CheckNum.arguments.length; i++)
	if (!eCheckNum(theForm.elements[CheckNum.arguments[i]])) {
		alert("Field entry is not valid");
		theForm.elements[CheckNum.arguments[i]].focus();
		return false;
	}
	return true;
}

function eCheckAlphaNum(sn) {
	s = sn.value;
	for (var i = 0; i < s.length; i++) {
		ch = s.substring(i, i + 1)
		if (!((ch >= "A" && ch <= "Z") || (ch >= "a" && ch <= "z") || (ch == " ")  || (ch == ".")  || (ch == "-")   || (ch == "&")  ||  (ch == ",")  ||  (ch == "_")  ||  (ch == "(")  ||  (ch == ")")  || (ch >= "0" && ch <= "9") || ch == "@")) {
			return false;
			theForm.elements[eCheckAlphaNum.arguments[i]].focus();
		}
	}
	return true;
}

function CheckAlphaNum(theForm) {
	for (var i=1; i<CheckAlphaNum.arguments.length; i++)
	if (!eCheckAlphaNum(theForm.elements[CheckAlphaNum.arguments[i]])) {
		alert("Field entry is not valid");
		theForm.elements[CheckAlphaNum.arguments[i]].focus();
		return false;
	}
	return true;
}

function CheckRequiredFields(theForm) {
	for (i=1; i<CheckRequiredFields.arguments.length; i++)
	if (theForm.elements[CheckRequiredFields.arguments[i]].value=="") {
		alert("This field is required");
		theForm.elements[CheckRequiredFields.arguments[i]].focus();
		return false;
	}
	return true;
}

// * start checking the actual form stuff here
// * replace field and form names below
var bValid;
var f;
var i;
var sString;

function ProcessForm(bFinal) {
	if (!f)
	f = document.thismainForm;
	if (bFinal == "TRUE") {
		CheckSubmit();
	} else {
		document.thismainForm.submit();
	}
}

function CheckValid(strFieldName, strMsg) {
	if (strFieldName == "" || strFieldName == null || strFieldName == "NULL" || strFieldName == " ") {
		if (bValid) {
			alert(strMsg);
			bValid = false;
		}
	}
}

function CheckPassword() {
	sString = new String(f.pword1.value);
	if (sString.length < 4 || sString.length > 16) {
		if (bValid) {
			alert("Your New Password must be between 4 and 16 characters!");
			bValid = false;
		}
	}
	if (f.pword1.value != f.pword2.value) {
		if (bValid) {
			alert("The Password fields must match!");
			bValid = false;
		}
	}
}

function getCheckedRadioButton(radioSet) { 
	for (var i=0; i<radioSet.length; i++)
	if (radioSet[i].checked)
	return i;
	return -1;
}

// validate radio
function CheckRadio(radio, strMsg) {
	if (getCheckedRadioButton(radio) == -1) {
		if (bValid) {
			alert(strMsg);
			bValid = false;
		}
	}
}

function CheckPhone() {
	var sOrigPhone = new String(f.HomePhone.value);
	var sNewPhone = new String();
	var i;
	if (bValid != true)
	//an error was previously detected
	return;
	for (i=0;i < sOrigPhone.length;i++) {
		if (!isNaN(parseInt(sOrigPhone.charAt(i)))) {
			sNewPhone = sNewPhone + sOrigPhone.charAt(i);
		}
	}
	if (sNewPhone.length < 10) {
		bValid = false;
		alert("Your phone number must be at least 10 digits long");
	}
}

function PWKeyUp() {
	var sChar;
	var lLength;
	if(!f)
	f = document.thismainForm;
	sString = new String(f.Password.value);
	lLength = sString.length;
	for (i = 0; i < lLength; i++) {
		sChar = sString.charAt(i);
		if ((sChar >= "0" && sChar <= "9") || (sChar >= "a" && sChar <= "z") || (sChar >= "A" && sChar <= "Z")) {
		} else {
			alert("Your Password must contain only Letters or Numbers!");
			f.Password.value = sString.substring(0, sString.length-1);
		}
	}
}






// New e-mail validation functions added 12/19/2006 by MC.

function CheckEMailOLD() {
	sString = new String(f.email.value);
	if (sString.indexOf("@", 0) == -1) {
		if (bValid) {
			alert("Please include a valid Email Address in the format 'name@domain.xxx'");
			bValid = false;
		}
	}
	if (sString.indexOf(".", 0) == -1) {
		if (bValid) {
			alert("Please include a valid Email Address in the format 'name@domain.xxx'");
			bValid = false;
		}
	}
}

function CheckEMailNEW_alt() {
	var sString = new String(f.email.value);
	var emailaddress = form.email.value;
	var emailFormat = /^\w(\.?[\w-])*@\w(\.?[\w-])*\.[a-zA-Z]{2,6}(\.[a-zA-Z]{2})?$/i;
	// begin LengthCheck function for email field
	// begin email format validation
	if (emailaddress.search(emailFormat) == -1) {
		if (bValid) {
			alert("Please specify your email address in the following format: email@emailaddress.com");
			bValid = false;
		}
	}
}

function CheckEMail() {
	var addr = new String(f.email.value);
	//var invalidChars = '\/\'\\ ";:?!()[]\{\}^|';
	var invalidChars = '\/\'\\ ";:?!()[]\{\}^|,';
	for (i=0; i<invalidChars.length; i++) {
		if (addr.indexOf(invalidChars.charAt(i),0) > -1) {
			if (bValid) {
				alert('E-mail address contains invalid characters');
				bValid = false;
			}
		}
	}
	for (i=0; i<addr.length; i++) {
		if (addr.charCodeAt(i)>127) {
			if (bValid) {
				alert("E-mail address contains non ascii characters.");
				bValid = false;
			}
		}
	}
	var atPos = addr.indexOf('@',0);
	if (atPos == -1) {
		if (bValid) {
			alert('E-mail address must contain an @');
			bValid = false;
		}
	}
	if (atPos == 0) {
		if (bValid) {
			alert('E-mail address must not start with @');
			bValid = false;
		}
	}
	if (addr.indexOf('@', atPos + 1) > - 1) {
		if (bValid) {
			alert('E-mail address must contain only one @');
			bValid = false;
		}
	}
	if (addr.indexOf('.', atPos) == -1) {
		if (bValid) {
			alert('E-mail address must contain a period in the domain name');
			bValid = false;
		}
	}
	if (addr.indexOf('@.',0) != -1) {
		if (bValid) {
			alert('Period must not immediately follow @ in e-mail address');
			bValid = false;
		}
	}
	if (addr.indexOf('.@',0) != -1) {
		if (bValid) {
			alert('Period must not immediately precede @ in e-mail address');
			bValid = false;
		}
	}
	if (addr.indexOf('..',0) != -1) {
		if (bValid) {
			alert('Two periods must not be adjacent in e-mail address');
			bValid = false;
		}
	}
	var suffix = addr.substring(addr.lastIndexOf('.')+1);
	if (suffix.length != 2 && suffix != 'com' && suffix != 'net' && suffix != 'org' && suffix != 'edu' && suffix != 'int' && suffix != 'mil' && suffix != 'gov' & suffix != 'arpa' && suffix != 'biz' && suffix != 'aero' && suffix != 'name' && suffix != 'coop' && suffix != 'info' && suffix != 'pro' && suffix != 'museum') {
		if (bValid) {
			alert('Invalid primary domain in e-mail address');
			bValid = false;
		}
	}
	return true;
}

//-->
