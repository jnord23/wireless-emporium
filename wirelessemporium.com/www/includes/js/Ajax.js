function ajax(newURL,rLoc) {
	var httpRequest;
	if (window.XMLHttpRequest) { // Mozilla, Safari, ...
		httpRequest = new XMLHttpRequest();
		if (httpRequest.overrideMimeType) {
			httpRequest.overrideMimeType('text/xml');
		}
	} else if (window.ActiveXObject) { // IE
		httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
	}
	httpRequest.open('GET', newURL, true);
	httpRequest.onreadystatechange = function(){
		if (httpRequest.readyState == 4) {
			if (httpRequest.status == 200) {
				badRun = 0
				var rVal = httpRequest.responseText
				if (rVal == "") {
					alert("No data available")
				}
				else if (rVal == "refresh") {
					curLoc = window.location
					window.location = curLoc
				}
				else if(document.getElementById(rLoc)){
					document.getElementById(rLoc).innerHTML = rVal
				}
				rVal = null;
			}
			else {
				document.getElementById("testZone").innerHTML = newURL
				//alert("Error performing action")
			}
		}
	};
	httpRequest.send(null);
}