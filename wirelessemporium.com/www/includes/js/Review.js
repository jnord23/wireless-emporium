//implemented intially for WE product reviews
var intMaxEntryDisplay=3

$(document).ready( function(){
	$("#imgShowAll").click( function( objEvent){
		OnClickShowAll()
	});

	$("#imgHide").click( function( objEvent){
		$("#divOutputReviewEntry").hide()
		$("#divButtonHide").hide()
		ShowAllToggleOn()
	});

	if( intEntryCount<=intMaxEntryDisplay) ShowAllToggleOff()

	$("#hdnEntryCount").val( ''+intMaxEntryDisplay)
	$("#divOutputReviewEntry").show()

	ReviewUpdate()
});

function OnClickShowAll(){
	$("#divOutputReviewEntry").show()
	$("#hdnEntryCount").val("-2")
	ReviewUpdate()
	ShowAllToggleOff();
}

function ShowAllToggleOff(){
	$("#divButtonShowAll").hide()
	$("#divButtonDivider").hide()
	$("#divButtonHide").show()
	$("#divReviewBottomButtonPanel").attr("style", "width: 130px;");
}

function ShowAllToggleOn(){
	$("#divButtonDivider").hide()
	$("#divButtonShowAll").show()
	$("#divReviewBottomButtonPanel").attr("style", "width: 99px;");
}

function OnChangeReviewUpdate(){
	if( $('#divButtonHide').is(':hidden') ){
		$("#hdnEntryCount").val( ''+intMaxEntryDisplay)
	}
	ReviewUpdate()
	$("#divOutputReviewEntry").show()
	ResetButtonUi()
	if( intEntryCount<=intMaxEntryDisplay) ShowAllToggleOff()
}

function ResetButtonUi(){
	$("#divButtonShowAll").show()
	$("#divButtonHide").show()
	$("#divButtonDivider").show()
	$("#divReviewBottomButtonPanel").attr("style", "width: 250px;");
}

function ReviewUpdate(){
	$.ajax({
		type: 'POST',
		url: '/framework/data/productdetail/request.asp',
		data: eval( '(' + '{"ItemId":"'+ document.getElementById( "hdnItemId").value +'", "EntryCount":"'+ document.getElementById( "hdnEntryCount").value +'", "rq": "GetReview", "ReviewOrderRating": "'+ document.getElementById( "ReviewOrderRating").value +'", "ReviewOrderDate": "'+ document.getElementById( "ReviewOrderDate").value +'"}' +')'),
		success: function(objJson){
			ShowReviewUpdatePanel( objJson);
		},
		dataType: "json"
	});
}

function ShowReviewUpdatePanel( objJson){
	var strOutput = '';

	if( $("#hdnEntryCount").val()=="-2") intEntryCount=objJson[ "data"].length

	if( objJson[ "data"].length==0){
		document.getElementById( "divReviewBottomButtonPanel").innerHTML = '';
	} 

	for( var intIdx=0; intIdx<objJson[ "data"].length; intIdx++){
		var strHtmlRating = '';
		var intRating = Math.floor( parseFloat( objJson[ "data"][ intIdx]["Rating"])*10);
		var intStarCount = 5;
		while( intRating>0 || intStarCount>0){
			var strImageType = ''
			if( intRating >= 10){
				strImageType = "full"
			} else if( intRating >= 5){
				strImageType = "half"
			} else{
				strImageType = "empty"
			}
			strHtmlRating+=( '<div class="DivCell"><img src="/images/reviews/star-'+ strImageType +'.gif" /></div>')

			intRating -= 10
			--intStarCount;
		}

		strOutput +=
			'<div class="ReviewEntryContainer">'
			+ '<div class="ReviewTitle">'+ objJson[ "data"][ intIdx]["ReviewTitle"] +'</div>'
			+ '<div>'
			+ strHtmlRating
			+ '<div class="DivCell ReviewRating">'+ objJson[ "data"][ intIdx]["Rating"] +' out of 5 stars</div>'
			+ '</div>'
			+'<div class="CleanUp">&nbsp;</div>'
			+'<div>'
				+'<div class="DivCell ReviewPostedBy">Posted by:</div>'
				+'<div class="DivCell ReviewName">'+ objJson[ "data"][ intIdx]["Nickname"] +'</div>'
				+'<div class="DivCell ReviewDate">on '+ objJson[ "data"][ intIdx]["DateTimeEntd"] +'</div>'
			+'</div>'
			+'<div class="CleanUp">&nbsp;</div>'
			+'<div class="ReviewEntry">'+ objJson[ "data"][ intIdx]["Review"] +'</div>'
			+'<div>'
				+'<div class="DivCell ReviewShare">Share this review:</div>'
				+'<div class="DivCell" style="padding-left: 10px;"><a href="http://www.facebook.com/wirelessemporium/" target="_blank"><img src="/images/reviews/facebookIconNew.jpg" alt="Share with Facebook" border="0" /></a></div>'
				+'<div class="DivCell" style="padding-left: 5px;"><a href="http://twitter.com/wirelessemp/" target="_blank"><img src="/images/reviews/twitterIconNew.jpg" alt="Share with Twitter" border="0" /></a></div>'
			+'</div>'
			+'<div class="CleanUp">&nbsp;</div>'
			+'<div class="ReviewContainerBuffer"><img src="/images/spacer.gif" width="1" height="1" /></div>'
			+'</div>'

	}
	document.getElementById( 'divOutputReviewEntry').innerHTML=strOutput
}