

	function showLoginDialog() {
		var randomnumber=Math.floor(Math.random()*100001);
		ajax("/ajax/accountLoginDialog.asp?rn="+randomnumber,'popBox');
		document.getElementById("popBox").style.position = "absolute";
		//document.getElementById("popCover").onclick = function () { closeFloatingBox() }
		//document.getElementById("popBox").onclick = function () { closeFloatingBox() }
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	function closeFloatingBox() {
		//------------------ change it to original -------------------//
		//document.getElementById("popBox").style.position = "fixed";
		//document.getElementById("popCover").onclick = function () {}
		//document.getElementById("popBox").onclick = function () {}
		//------------------------------------------------------------//
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
		document.getElementById("popCover").innerHTML = "";
		document.getElementById("popBox").innerHTML = "";
	}	
	function removeGhost(strF) {
		//var f = document.getElementsByName(strF).item(0).value;
		var f = strF.value
		if (f == 'Email Address' || f == 'Password' || f == 'First Name' || f == 'Last Name' || f == 'Confirm Password') {
			//document.getElementsByName(strF).item(0).value = '';
			strF.value = '';
		}
	}
	function replaceGhost(o) {
		var v = o.value;
		var name = o.name;
		if (v == '') {
			if (name == 'fName') {
				o.value = 'First Name';
			}
			if (name == 'lName') {
				o.value = 'Last Name';
			}
			if (name == 'inputEmail') {
				o.value = 'Email Address';
			}
			if (name == 'email') {
				o.value = 'Email Address';
			}
			if (name == 'password') {
				o.value = 'Password';
			}
			if (name == 'inputPassword') {
				o.value = 'Password';
			}
			if (name == 'passwordConfirm') {
				o.value = 'Confirm Password';
			}
		}
	}
//	function removeGhostPass(obj){
//		var newO=document.createElement('input');
//		newO.setAttribute('type','password');
//		newO.setAttribute('name',obj.getAttribute('name'));
//		newO.setAttribute('id',obj.getAttribute('id'));
//		newO.setAttribute('onblur',obj.getAttribute('onblur'));
//		obj.parentNode.replaceChild(newO,obj);
//		newO.focus();
//	}


	function replaceT(obj){
		//Deprecated!  This name is confusing... Use removeGhostPass
		var newO=document.createElement('input');
		newO.setAttribute('type','password');
		newO.setAttribute('name',obj.getAttribute('name'));
		newO.setAttribute('id',obj.getAttribute('id'));
		newO.setAttribute('onblur',obj.getAttribute('onblur'));
		obj.parentNode.replaceChild(newO,obj);
		newO.focus();
	}
	function passOnfocus(obj) {
		//Convert to Password to provide character masking
		var v = obj.value;
		//alert('"' + v + '"');
		// && obj.type == 'text'
		if ((v == 'Password' || v == 'Confirm Password')) {
			var newO=document.createElement('input');
			newO.setAttribute('type','password');
			newO.setAttribute('name',obj.getAttribute('name'));
			newO.setAttribute('id',obj.getAttribute('id'));
			newO.setAttribute('onblur',obj.getAttribute('onblur'));
			var focusData = obj.getAttribute('onfocus');
			newO.setAttribute('onfocus',obj.getAttribute('onfocus'));
			newO.setAttribute('value',obj.value)//don't copy values
			obj.parentNode.replaceChild(newO,obj);
			//setTimeout(obj.setAttribute('onfocus',focusData), 500);
			//newO.onfocus = focusData;
			removeGhost(newO);
			newO.focus();
		}
	}
	function passOnblur(obj) {
//Broken implementation... ("not_found_err dom exception 8" in Chrome) 2012-10-31
//running(this);thenRunning(this); will break if the first function replaces the dom object, so functions must be combined and reference valid (non-replaced) children
//see passOnfocus() for working implementation

//		var v = obj.value;
//		//alert('"' + v + '"');
//		// && obj.type == 'text'
//		if ((v == 'Password' || v == 'Confirm Password')) {
//			var newO2=document.createElement('input');
//			newO2.setAttribute('type','text');
//			newO2.setAttribute('name',obj.getAttribute('name'));
//			newO2.setAttribute('id',obj.getAttribute('id'));
//			//newO2.setAttribute('onblur',obj.getAttribute('onblur'));
//			//var focusData = obj.getAttribute('onfocus');
//			newO2.setAttribute('value',obj.value)//don't copy values
//			obj.parentNode.replaceChild(newO2,obj);
//			//obj.focus();
//			//setTimeout(obj.setAttribute('onfocus',focusData),500);
//		}
		//Older browsers will not display changed type correctly...
		var v = obj.value;
		if ((v == 'Password' || v == 'Confirm Password')) {
			obj.type = 'text';
		}
			
	}
//	function replaceGhostPass(o) {
//		var v = o.value;
//		var name = o.name;
//		if (v == ''){
//		//Convert to Password to provide character masking
//		var newO=document.createElement('input');
//		newO.setAttribute('type','text');
//		newO.setAttribute('name',o.getAttribute('name'));
//		newO.setAttribute('id',o.getAttribute('id'));
//		newO.setAttribute('onblur',o.getAttribute('onblur'));
//		newO.setAttribute('onfocus',o.getAttribute('onfocus'));
//		newO.setAttribute('value',o.value);
//		o.parentNode.replaceChild(newO,o);
//		newO.focus();
//		}
//	}
	
	function checkToggle(o,checkBoxSurrogateFor){
		o.className = (o.className === 'unchecked' ? 'checked' : 'unchecked');
		var c = document.getElementById(checkBoxSurrogateFor);
		c.checked = (c.checked == '' ? 'checked' : '');
	}
	