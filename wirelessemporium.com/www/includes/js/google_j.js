<!-- /* <[CDATA[ */

// preload images
function preloadImgs() {
  var imgs = ['https://checkout.google.com/seller/accept/images/sc.gif','https://checkout.google.com/seller/accept/images/gl.gif','https://checkout.google.com/seller/accept/images/x.gif','https://checkout.google.com/seller/accept/images/gcb.gif','https://checkout.google.com/seller/accept/images/ht.gif','https://checkout.google.com/seller/accept/images/st.gif'];
  for (var i = 0; i < imgs.length; i++) {
    var img = new Image();
    img.src = imgs[i];
  }
}

// get position of mark (http://blog.firetree.net/2005/07/04/javascript-find-position/)
function findXPos(o) {
  var n = 0;
  if (o.offsetParent) {
    while (1) {
      n += o.offsetLeft;
      if (!o.offsetParent) { break; }
      o = o.offsetParent;
    }
  } else if (o.x) {
    n += o.x;
  }
  return n;
}

function findYPos(o) {
  var n = 0;
  if (o.offsetParent) {
    while (1) {
      n += o.offsetTop;
      if (!o.offsetParent) { break; }
      o = o.offsetParent;
    }
  } else if (o.y) {
    n += o.y;
  }
  return n;
}

function is_mac() {
  var agt = navigator.userAgent.toLowerCase();
  return (agt.indexOf("mac") != -1);
}

// position hidden div depending on position on screen
function setPos(n) {
  var h = document.getElementById('h'+n);
  var i = document.getElementById('i'+n);
  var m = document.getElementById('m'+n);
  var d = document.documentElement;
  var b = document.body;
  
  // position of the mark on the screen (top right corner)
  var x = findXPos(m);
  var y = findYPos(m);
  
  // width and height of the body (code taken from dw_viewport.js from http://www.dyn-web.com/)
  var ww = 0;
  if (window.innerWidth) { ww = window.innerWidth - 18; }
  else if (d && d.clientWidth) { ww = d.clientWidth; }
  else if (b && b.clientWidth) { ww = b.clientWidth; }
  
  var wh = 0;
  if (window.innerHeight) { wh = window.innerHeight - 18; }
  else if (d && d.clientHeight) { wh = d.clientHeight; }
  else if (b && b.clientHeight) { wh = b.clientHeight; }
  
  // scroll x and y of the window (code taken from dw_viewport.js from http://www.dyn-web.com/)
  var sx = 0;
  if (typeof window.pageXOffset == "number") { sx = window.pageXOffset; }
  else if (d && d.scrollLeft) { sx = d.scrollLeft; }
  else if (b && b.scrollLeft) { sx = b.scrollLeft; }
  else if (window.scrollX) { sx = window.scrollX; }

  var sy = 0;    
  if (typeof window.pageYOffset == "number") { sy = window.pageYOffset; }
  else if (d && d.scrollTop) { sy = d.scrollTop; }
  else if (b && b.scrollTop) { sy = b.scrollTop; }
  else if (window.scrollY) { sy = window.scrollY; }
  
  // width and height (including border) of hidden window
  var hw = 326;
  var hh = 144;
  
  // width and height of the mark
  var mw = i.width;
  var mh = i.height;
  
  if (ww - (x - sx + mw) < (x - sx)) {
    h.style.left = (-hw-3) + 'px';
  } else {
    h.style.left = mw + 1 + 'px';
  }
  
  if (wh - (y - sy) < (y - sy + mh)) {
    if (n==1 || n==2) {
      h.style.top = (-hh + mh + 3) + 'px';
      if (is_mac()) { h.style.top = (-hh + mh + 10) + 'px'; }
    } else {
      h.style.top = (-mh + 4) + 'px';
      if (is_mac()) { h.style.top = (-mh + 11) + 'px'; }
    }
  } else {
    if (n == 1) {
      h.style.top = 9 + 'px';
    } else if (n == 2) {
      h.style.top = 7 + 'px';
    } else {
      h.style.top = 0 + 'px';
    }
  }
}

// browser detection
function detectBrowser() {
  // browser detection from http://www.webreference.com/tools/browser/javascript.html  
  // convert all characters to lowercase to simplify testing
  var agt = navigator.userAgent.toLowerCase();
  var appVer = navigator.appVersion.toLowerCase();
  
  var is_minor = parseFloat(appVer);
  var is_major = parseInt(is_minor,10);
  
  var is_opera = (agt.indexOf("opera") != -1);
  var is_opera2 = (agt.indexOf("opera 2") != -1 || agt.indexOf("opera/2") != -1);
  var is_opera3 = (agt.indexOf("opera 3") != -1 || agt.indexOf("opera/3") != -1);
  var is_opera4 = (agt.indexOf("opera 4") != -1 || agt.indexOf("opera/4") != -1);
  var is_opera5 = (agt.indexOf("opera 5") != -1 || agt.indexOf("opera/5") != -1);
  var is_opera6 = (agt.indexOf("opera 6") != -1 || agt.indexOf("opera/6") != -1);
  var is_opera7 = (agt.indexOf("opera 7") != -1 || agt.indexOf("opera/7") != -1);
  var is_opera5up = (is_opera && !is_opera2 && !is_opera3 && !is_opera4);
  var is_opera6up = (is_opera && !is_opera2 && !is_opera3 && !is_opera4 && !is_opera5);
  var is_opera7up = (is_opera && !is_opera2 && !is_opera3 && !is_opera4 && !is_opera5 && !is_opera6);
  
  // IE
  var is_mac = agt.indexOf("mac")!=-1;
  var iePos  = appVer.indexOf('msie');
  if (iePos !=-1) {
    if (is_mac) {
      iePos = agt.indexOf('msie');
      is_minor = parseFloat(agt.substring(iePos+5,agt.indexOf(';',iePos)));
    } else {
      is_minor = parseFloat(appVer.substring(iePos+5,appVer.indexOf(';',iePos)));
    }
     is_major = parseInt(is_minor,10);
  }
  
  var is_safari = ((agt.indexOf('safari')!=-1)&&(agt.indexOf('mac')!=-1))?true:false;  
  var is_camino = (((agt.indexOf('camino')!=-1)||(agt.indexOf('chimera')!=-1))&&(agt.indexOf('mac')!=-1))?true:false;  
  var is_gecko = ((!is_safari)&&(navigator.product)&&(navigator.product.toLowerCase()=="gecko"))?true:false;
  
  var is_fx = ((agt.indexOf('mozilla/5')!=-1) && (agt.indexOf('spoofer')==-1) &&
        (agt.indexOf('compatible')==-1) && (agt.indexOf('opera')==-1) &&
        (agt.indexOf('webtv')==-1) && (agt.indexOf('hotjava')==-1) &&
        (is_gecko) && ((navigator.vendor=="Firefox")||(agt.indexOf('firefox')!=-1)));
        
  var is_moz = ((agt.indexOf('mozilla/5')!=-1) && (agt.indexOf('spoofer')==-1) &&
        (agt.indexOf('compatible')==-1) && (agt.indexOf('opera')==-1) &&
        (agt.indexOf('webtv')==-1) && (agt.indexOf('hotjava')==-1) &&
        (is_gecko) && (!is_fx) && ((navigator.vendor==="")||(navigator.vendor=="Mozilla")||(navigator.vendor=="Debian")));
  
  if ((is_moz)||(is_fx)) {
    var is_moz_ver = (navigator.vendorSub)?navigator.vendorSub:0;
    if(is_fx&&!is_moz_ver) {
      is_moz_ver = agt.indexOf('firefox/');
      is_moz_ver = agt.substring(is_moz_ver+8);
      is_moz_ver = parseFloat(is_moz_ver);
    }
    if(!(is_moz_ver)) {
      is_moz_ver = agt.indexOf('rv:');
      is_moz_ver = agt.substring(is_moz_ver+3);
      var is_paren = is_moz_ver.indexOf(')');
      is_moz_ver = is_moz_ver.substring(0,is_paren);
    }
    is_minor = is_moz_ver;
    is_major = parseInt(is_moz_ver,10);
  }
  
  var is_nav  = ((agt.indexOf('mozilla')!=-1) && (agt.indexOf('spoofer')==-1) &&
        (agt.indexOf('compatible') == -1) && (agt.indexOf('opera')==-1) &&
        (agt.indexOf('webtv')==-1) && (agt.indexOf('hotjava')==-1) &&
        (!is_safari) && (!is_camino) && (!(is_moz)) && (!is_fx));
  
  if ((navigator.vendor)&&((navigator.vendor=="Netscape6")||(navigator.vendor=="Netscape"))&&(is_nav)) {
     is_major = parseInt(navigator.vendorSub,10); 
     is_minor = parseFloat(navigator.vendorSub);
  }
  
  var is_ie = ((iePos!=-1) && (!is_opera) && (!is_safari));
  
  var is_nav7up = (is_nav && is_minor >= 7);
  var is_moz1up = (is_moz && is_minor >= 1);
  var is_ie5up = (is_ie && is_minor >= 5);
  var is_ie6up = (is_ie && is_minor >= 6);
  var is_win = ( (agt.indexOf("win")!=-1) || (agt.indexOf("16bit")!=-1) );
  
  return ((is_win && is_ie5up) || is_fx || is_nav7up || is_moz1up || is_camino || is_safari || is_opera7up);
  
}

// show/hide hidden div
function showHide(n) {
  setPos(n);
  var h = document.getElementById('h'+n);
  h.style.display = (h.style.display == "none") ? "block" : "none";  
}

function showMark(n) {
  if (detectBrowser()) {
    document.images.onload=preloadImgs();
    switch (n) {
      case 1:
        document.write('<div id="google_amark_b" style="width:92px; height:88px;"><div id="m'+n+'" class="m"><a href="javascript:showHide('+n+')"><img src="https://checkout.google.com/seller/accept/images/st.gif" width="92" height="88" id="i'+n+'" alt="Google Checkout Acceptance Mark" />');
        break;
      case 2:
        document.write('<div id="google_amark_b" style="width:182px; height:44px;"><div id="m'+n+'" class="m"><a href="javascript:showHide('+n+')"><img src="https://checkout.google.com/seller/accept/images/ht.gif" width="182" height="44" id="i'+n+'" alt="Google Checkout Acceptance Mark" />');
        break;
      default:
        document.write('<div id="google_amark_b" style="width:72px; height:73px;"><div id="m'+n+'" class="m"><a href="javascript:showHide('+n+')"><img src="https://checkout.google.com/seller/accept/images/sc.gif" width="72" height="73" id="i'+n+'" alt="Google Checkout Acceptance Mark" />');
        break;
    }
    document.write('</a></div><div id="h'+n+'" class="h" style="display:none;"><div id="t"><img src="https://checkout.google.com/seller/accept/images/gl.gif"  width="154" height="28" id="l" alt="Google Checkout logo" /><div id="x"><a href="javascript:showHide('+n+')"><img src="https://checkout.google.com/seller/accept/images/x.gif" width="16" height="16" alt="" /></a></div></div><div id="c"><p>Google Checkout is a fast, secure way to buy from stores across the web.</p><p class="p">When it\'s time to buy, look for the <img src="https://checkout.google.com/seller/accept/images/gcb.gif" width="104" height="19" alt="Google Checkout button image" /> button.</p><p>Use it once and stop creating new accounts every time you buy. <a href="https://checkout.google.com/buyer/tour.html" target="_blank">Learn more</a></p></div></div></div>');
  } else {
    switch (n) {
      case 1:
        document.write('<img src="https://checkout.google.com/seller/accept/images/st.gif" width="92" height="88" alt="Google Checkout Acceptance Mark" />');
        break;
      case 2:
        document.write('<img src="https://checkout.google.com/seller/accept/images/ht.gif" width="182" height="44" alt="Google Checkout Acceptance Mark" />');
        break;
      default:
        document.write('<img src="https://checkout.google.com/seller/accept/images/sc.gif" width="72" height="73" alt="Google Checkout Acceptance Mark" />');
        break;
    }
  }
}

/* ]]> */ //-->
