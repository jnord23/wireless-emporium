	<% if prepStr(pageTitle) = "" then pageTitle = replace(replace(request.ServerVariables("URL"),".asp",""),"/","") %>
    <!-- This site contains information about: cell phone accessories,cheapest cell phone accessories,cell phone faceplate,cell phone covers,cell phone battery,cellular battery,cell phone charger,discount cell phone chargers,hands free kit,hands-free -->
	<meta name="verify-v1" content="kygxtWzRMtPejZFFDjdkwO7wTNu3kxWwO3M/Q6WGJCs=" />
	<meta name="verify-v1" content="JXXhlKPTKULNfZeB9M5Qxp3AW1u4DQRl/PJZ4NLAfEs=" />
	<meta name="msvalidate.01" content="DFF5FF52EAB66FFFC627628486428C9B" />
    <!--#include virtual="/framework/utility/noindex.asp"-->
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="canonical" href="<%=canonicalURL%>"/>

	<script>
        function Open_Popup(theURL,winName,features)	{	window.open(theURL,winName,features);	}
		function toggle(which) 
		{
			if (document.getElementById(which).style.display == '') 
				document.getElementById(which).style.display='none'
			else 
				document.getElementById(which).style.display=''
		}
    </script>
    
<!-- BEGIN: Google Trusted Store -->
<script type="text/javascript">
	var gts = gts || [];
	
	gts.push(["id", "230549"]);
	<%
	trustStoreItemID = ""
	sql = 	"exec sp_googleTrustedItem 0"
	set rsTrust = oConn.execute(sql)
	if not rsTrust.eof then trustStoreItemID = rsTrust("itemid")
	%>
	gts.push(["google_base_offer_id", "<%=trustStoreItemID%>"]);
	gts.push(["google_base_subaccount_id", "8589772"]);
	gts.push(["google_base_country", "US"]);
	gts.push(["google_base_language", "EN"]);
//	gts.push(["gtsContainer","we_google_trusted_badge"]);
	
	(function() {
		var scheme = (("https:" == document.location.protocol) ? "https://" : "http://");
		var gts = document.createElement("script");
		gts.type = "text/javascript";
		gts.async = true;
		gts.src = scheme + "www.googlecommerce.com/trustedstores/gtmp_compiled.js";
		var s = document.getElementsByTagName("script")[0];
		s.parentNode.insertBefore(gts, s);
	})();
</script>
<!-- END: Google Trusted Store -->
<%
select case lcase(topPageName)
	case "top"
	%>
        <!-- AdSense Start -->
        <script src="<%=useHttp%>://www.google.com/jsapi"></script>
        <script type="text/javascript" charset="utf-8">
            google.load('ads.search', '2');
        </script>
        <!-- AdSense End -->    
    <%
	case "product"
	%>
        <script type="text/javascript" charset="utf-8">
	        google.load('ads.search', '2');
        </script>
    <%
		sql	=	"exec [sp_getVideosByItemID] '" & itemid & "'"
'		response.write "<pre>" & sql & "</pre>"
		set rsVideo = oConn.execute(sql)
		if not rsVideo.eof then
			videoURL = rsVideo("video_url")
			if prepStr(videoURL) <> "" then
			%>
			<meta property="og:url" content="<%=canonicalURL%>" />
			<meta property="og:title" content="<%=itemdesc%>" />
			<meta property="og:description" content="<%=itemdesc%>" />
			<meta property="og:type" content="video" />
			<meta property="og:image" content="http://www.wirelessemporium.com/productpics/thumb/<%=itempic%>" />
			<meta property="og:video" content="<%=videoURL%>"/>
			<meta property="og:video:type" content="application/x-shockwave-flash"/>
			<meta property="og:video:width" content="480"/>
			<meta property="og:video:height" content="280"/>
			<meta property="og:site_name" content="Wireless Emporium"/>
			<%
			end if
		end if
end select

if pageName = "customphonecase" then
	%><meta property="og:image" content="http://www.wirelessemporium.com/images/customCase/facebook-custom-case.jpg"/><%
	%><meta property="og:title" content="Customize Your Own Phone Case" /><%
	%><meta property="og:type" content="video" /><%
	%><meta property="og:url" content="http://www.wirelessemporium.com/custom-phone-cases" /><%
	%><meta property="og:description" content="Customize your own phone case at Wireless Emporium" /><%
end if

if pageName = "mycustomcase" and myCustomCaseImg <> "" and myCustomCaseUrl <> "" then
	%><meta property="og:image" content="<%=myCustomCaseImg%>"/><%
	%><meta property="og:title" content="My Custom Case" /><%
	%><meta property="og:type" content="website" /><%
	%><meta property="og:url" content="<%=myCustomCaseUrl%>" /><%
	%><meta property="og:description" content="My custom case looks awesome!" /><%
end if

call printPixel(1)
'response.write session("printPixel")
%>