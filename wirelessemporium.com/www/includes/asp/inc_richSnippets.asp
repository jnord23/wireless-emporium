<%
function getRichSnippets(richSnippetType)
	select case lcase(richSnippetType)
		case "incorp"	'incorporation rich snippets
			select case lcase(REWRITE_URL)
				case "/index.asp","/index_mp.asp"
				%>
				<div itemscope itemtype="http://data-vocabulary.org/Organization"> 
					<span itemprop="name">Wireless Emporium</span> 
					Located at 
					<span itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
					  <span itemprop="street-address">1410 N. Batavia St.</span>, 
					  <span itemprop="locality">Orange</span>, 
					  <span itemprop="region">CA</span>. 
					</span>
					Phone: <span itemprop="tel">800-305-1106</span>. 
					<a href="http://www.wirelessemporium.com" class="url" style="color:#ccc;">http://www.wirelessemporium.com</a> 
				</div>
				<%
				case "/apple-iphone-leather-carrying-cases-pouches.asp"
				%>
				<div itemscope itemtype="http://data-vocabulary.org/Organization"> 
					<span itemprop="name">Wireless Emporium</span> 
					Located at 
					<span itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
					  <span itemprop="street-address">1410 N. Batavia St.</span>, 
					  <span itemprop="locality">Orange</span>, 
					  <span itemprop="region">CA</span>. 
					</span>
					Phone: <span itemprop="tel">800-305-1106</span>. 
					<a href="/apple-iphone-leather-carrying-cases-pouches.asp" class="url" style="color:#ccc;">http://www.wirelessemporium.com</a> 
				</div>
				<%
			end select
	end select
end function
%>