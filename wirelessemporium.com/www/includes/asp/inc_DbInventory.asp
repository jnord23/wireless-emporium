<%
'get all ItemValues (as a nested hash) by ItemId
function GetItemValue( byval strItemId)	
	dim dicReturnValue : set dicReturnValue = CreateObject("Scripting.Dictionary")
	dicReturnValue.CompareMode = 1 ' -- case-insensitive mode
	if isId( strItemId) then
		dim objConn: set objConn=GetNewConn()

		dim strSql: strSql = _
" select " &VbCrLf&_
" tv.[Name] as [ActiveItemValueType]" &VbCrLf&_
" , s.[Site_Id]" &VbCrLf&_
" , tv.[OriginalPrice]" &VbCrLf&_
" , tv.[DiscountPrice]" &VbCrLf&_
" , tv.[DiscountPercent] " &VbCrLf&_
" from XStore s with(nolock) " &VbCrLf&_
" left join( " &VbCrLf&_
" select " &VbCrLf&_
" t.[Name]" &VbCrLf&_
" , v.[SiteId]" &VbCrLf&_
" , v.[OriginalPrice]" &VbCrLf&_
" , v.[DiscountPrice]" &VbCrLf&_
" , v.[DiscountPercent]" &VbCrLf&_
" from ItemValue v with(nolock)" &VbCrLf&_
" inner join ItemValueType t with(nolock) " &VbCrLf&_
" on t.[ItemValueTypeId]=v.[ActiveItemValueTypeId] and t.[IsActive]=1 " &VbCrLf&_
" where v.[IsActive]=1 and v.[ItemId]="& DbId( strItemId) &VbCrLf&_
" ) as tv" &VbCrLf&_
" on tv.[SiteId]=s.[Site_Id]"

		if DBGMODE then response.write "<pre>" & strSql & "</pre>": response.end

		dim objRs: set objRs = Server.CreateObject("ADODB.Recordset")
		objRs.open strSql, objConn, 0, 1 ' adOpenForwardOnly, adLockReadOnly 'assumes existing connection (pooled)

		while not objRs.eof ' stream all values into one string
			dim dicAttribute : set dicAttribute = CreateObject("Scripting.Dictionary")
			dicAttribute("OriginalPrice") = obj2Str( objRs("OriginalPrice").Value)
			dicAttribute("DiscountPrice") = obj2Str( objRs("DiscountPrice").Value)
			dicAttribute("DiscountPercent") = obj2Str( objRs("DiscountPercent").Value)
			dim strActiveItemValueType: strActiveItemValueType = obj2Str( objRs("ActiveItemValueType").Value)
			if not HasLen( strActiveItemValueType) then strActiveItemValueType = "OriginalPrice"
			dicAttribute("ActiveItemValueType") = strActiveItemValueType
			set dicReturnValue( obj2Str( objRs("Site_Id").Value)) = dicAttribute
			objRs.MoveNext
		wend

		objRs.close()
		set objRs=nothing
		call CloseConn( objConn)
	end if

	set GetItemValue = dicReturnValue
end function

'check if Item is master
function IsMasterItem( byval strItemId)
	dim strSql: strSql = _
	" select [Master]" &VbCrLf&_
	" from we_Items with(nolock)" &VbCrLf&_
	" where [ItemId]="& DbId( strItemId)

	IsMasterItem = (DbSingleValue( strSql)="True")
end function

'get list(DynamicArray) of items by PartNumber
function GetItemByPartNumber( byval strPartNumber)
	dim strSql: strSql = _
" select [ItemId]" &VbCrLf&_
" from we_Items with(nolock)" &VbCrLf&_
" where [PartNumber]="& DbStr( strPartNumber)

	set GetItemByPartNumber = DbList( strSql)
end function

 %>