<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<%

function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
end function

dim ReceiptText
ReceiptText = ""

dim sPromoCode
SQL = "SELECT B.PromoCode FROM we_orders A INNER JOIN we_coupons AS B ON A.couponid=B.couponid WHERE A.orderid = '" & nOrderID & "'"
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if not RS.eof then sPromoCode = RS("PromoCode")

'''''''''''''''''' START DISCOUNT ZONE ''''''''''''''''''
if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	SQL = "SELECT * FROM WE_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = prepInt(RS("promoPercent"))
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		FreeItemPartNumber = RS("FreeItemPartNumber")
		setValue = RS("setValue")
		oneTime = RS("oneTime")
	else
		sPromoCode = ""
	end if
end if

'mySession needs to be defined using the sessionID from the time of original purchase
'Otherwise, the promocode lookup from the shopping cart will return 0 results and no discount will be shown when it should be  
'We also can't use the current sessionID because it doesn't match the original purchase sessionID
sqlSession =	"select		s.sessionID--, * " & vbcrlf & _
				"from		we_orders o left join ShoppingCart s " & vbcrlf & _
				"	on		o.orderid = s.purchasedOrderID " & vbcrlf & _
				"where		o.orderid = '" & nOrderId & "'"
set sessionRS = Server.CreateObject("ADODB.Recordset")
set sessionRS = oConn.execute(sqlSession)
mySession = prepInt(sessionRS("sessionID"))
'if session("adminid") = 96 then
'	response.write "{mySession:" & mySession & "}<br />"
'	response.write "{couponid:" & couponid & "}<br />"
'	response.write "{sPromoCode:" & sPromoCode & "}<br />"
'	response.write "{discountTotal:" & discountTotal & "}<br />"
'end if
sPromoCodeSave = sPromoCode 'Save the promo code for printing on the receipt
%><!--#include virtual="/cart/includes/inc_promoFunctions.asp"--><% 'Calculates the discount total as variable discountTotal && sPromoCode could be set to an empty string
'if session("adminid") = 96 then
'	response.write "{mySession:" & mySession & "}<br />"
'	response.write "{couponid:" & couponid & "}<br />"
'	response.write "{sPromoCode:" & sPromoCode & "}<br />"
'	response.write "{discountTotal:" & discountTotal & "}<br />"
'end if
call fOpenConn() 'inc_promoFunctions.asp closes the connection, re-open it so processing can continue
'''''''''''''''''' END DISCOUNT ZONE ''''''''''''''''''

dim sFname, sLname, sEmail, sPhone
dim saddress1, saddress2, sCity, sstate, szip, sCountry
dim sShipAddress, sBillAddress
dim oRsCust, oRsOrd

SQL = "SELECT * FROM we_accounts WHERE accountid = '" & nAccountId & "'"
session("errorSQL") = SQL
set oRsCust = Server.CreateObject("ADODB.Recordset")
oRsCust.open SQL, oConn, 3, 3
if oRsCust.eof then
	ReceiptText = "<h3>Customer account" & nAccountId & " not found!</h3>"
else
	sFname = oRsCust("fname")
	sLname = oRsCust("lname")
	sEmail = oRsCust("email")
	sPhone = oRsCust("phone")
	
	if isnull(sFname) or len(sFname) <= 0 then
		custName			=	"<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>Customer</p>"
		custNameAddress		=	"<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>Customer</p>"
	else
		custName			=	ucase(left(sFname, 1)) & lcase(right(sFname,len(sFname)-1))
		custNameAddress		=	ucase(left(sFname, 1)) & lcase(right(sFname,len(sFname)-1)) & " " & ucase(left(sLname, 1)) & lcase(right(sLname,len(sLname)-1))
	end if
end if

SQL = "SELECT * FROM we_orders WHERE orderid = '" & nOrderId & "'"
session("errorSQL") = SQL
set oRsOrd = Server.CreateObject("ADODB.Recordset")
oRsOrd.open SQL, oConn, 3, 3
if oRsOrd.eof then ReceiptText = "<h3>Order" & nOrderId & " not found!</h3>"

'format the shipping and billing addresses
dim nShippingid
nShippingid = oRsOrd("shippingid")
if nShippingid > 0 then
	strSql = "SELECT * FROM we_addl_shipping_addr WHERE id='" & nShippingid & "'"
	session("errorSQL") = SQL
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open strSql, oConn, 3, 3
	if not RS.eof then
		saddress1 = RS("saddress1")
		saddress2 = RS("saddress2")
		sCity = RS("sCity")
		sstate = RS("sstate")
		szip = RS("szip")
		sCountry = RS("sCountry")
	end if
else
	saddress1 = oRsCust("saddress1")
	saddress2 = oRsCust("saddress2")
	sCity = oRsCust("sCity")
	sstate = oRsCust("sstate")
	szip = oRsCust("szip")
	sCountry = oRsCust("sCountry")
end if

testMsg = ""
if saddress1 = "4040 N. Palm St." then
	SQL = "select cancelled from we_Orders WHERE orderID = '" & nOrderId & "'"
	set checkOrderRS = oConn.execute(SQL)
	
	if not checkOrderRS.EOF then
		if isnull(checkOrderRS("cancelled")) then
			call cancelTestOrder(nOrderId)
		end if
		testMsg = "This is a test order and has been auto canceled"
	else
		testMsg = "Cancel Bypass:" & SQL
	end if
end if

sShipAddress = sAddress1
if sAddress2 <> "" then sShipAddress = sShipAddress & "<br>" & saddress2
sShipAddress = sShipAddress & "<br>" & sCity & ", " & sState & "&nbsp;" & sZip & "<br>" & sCountry

sBillAddress = oRsCust("bAddress1")
if oRsCust("bAddress2") <> "" then sBillAddress = sBillAddress & "<br>" & oRsCust("bAddress2")
sBillAddress = sBillAddress & "<br>" & oRsCust("bCity") & ", " & oRsCust("bState") & "&nbsp;" & oRsCust("bZip") & "<br>" & oRsCust("bCountry")

oRsCust.close
set oRsCust = nothing

if ReceiptText = "" then
	'format order info
	dim sShipType, nOrdershippingfee, nOrderTax, nBuysafeamount, extOrderType, extOrderNumber, nOrderDateTime, itemids
'	nOrderSubTotal = formatNumber(cDbl(oRsOrd("ordersubtotal")),2)
	emailSubTotal = formatNumber(cDbl(oRsOrd("ordersubtotal")),2)
	sShipType = oRsOrd("shiptype")
'	nOrdershippingfee = cDbl(oRsOrd("ordershippingfee"))
	emailShipFee = cDbl(oRsOrd("ordershippingfee"))
'	nOrderTax = oRsOrd("orderTax")
	emailOrderTax = oRsOrd("orderTax")
	nBuysafeamount = oRsOrd("BuySafeAmount")
'	nOrderGrandTotal = formatNumber(cDbl(oRsOrd("ordergrandtotal")),2)
	emailOrderGrandTotal = formatNumber(cDbl(oRsOrd("ordergrandtotal")),2)
	extOrderType = oRsOrd("extOrderType")
	extOrderNumber = oRsOrd("extOrderNumber")
	nOrderDateTime = oRsOrd("orderdatetime")
	'Free Product Offer code added 2/3/2010 by MC
	dim FreeProductOffer
	FreeProductOffer = oRsOrd("FreeProductOffer")
	shopRunnerID = oRsOrd("shopRunnerID")
	
	dim strOrderType
	select case extOrderType
		case 1 : strOrderType = "Paypal<br>" & extOrderNumber
		case 2 : strOrderType = "Google Checkout<br>" & extOrderNumber
		case 3
			strOrderType = "eBillme"
			eBillme = "eBillme"
			eBillmeAccount = extOrderNumber
		case 4 : strOrderType = "Ebay<br>" & extOrderNumber
		case 5 : strOrderType = "AtomicMall<br>" & extOrderNumber
		case 6 : strOrderType = "Amazon<br>" & extOrderNumber
		case 7 : strOrderType = "Buy.Com<br>" & extOrderNumber
		case else : strOrderType = "Credit Card"
	end select
	
	oRsOrd.close
	set oRsOrd = nothing
	
	'===================
	'NEW WEBLOYALTY CODE
	dim RSwl, cctype, country, pcode, strToEncrypt, strEncryptedValue
	SQL = "SELECT A.CCtype, B.bState, B.bZip FROM we_ccinfo A INNER JOIN we_Accounts B ON A.accountid=B.accountid WHERE A.orderid='" & nOrderID & "'"
	session("errorSQL") = SQL
	set RSwl = Server.CreateObject("ADODB.Recordset")
	RSwl.open SQL, oConn, 3, 3
	if not RSwl.eof then
		cctype = RSwl("CCtype")
		if len(RSwl("bState")) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",RSwl("bState")) > 0 then
			country = "CANADA"
		else
			country = "US"
		end if
		pcode = RSwl("bZip")
	end if
	RSwl.close
	set RSwl = nothing
	
	' Put together string ?concatenate all required values
	if incEmail = true then
		dim yesexiturl, noexiturl
		yesexiturl = "https://" & request.ServerVariables("SERVER_NAME") & "/confirm?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderGrandTotal & "&c=" & emailSubTotal
		noexiturl = yesexiturl
		strToEncrypt = "cref" & chr(255) & nOrderID & chr(255) & "cctype" & chr(255) & cctype & chr(255) & "country" & chr(255) & country & chr(255) & "pcode" & chr(255) & pcode & chr(255) & "yesexiturl" & chr(255) & yesexiturl & chr(255) & "noexiturl" & chr(255) & noexiturl
	else
		strToEncrypt = "cref" & chr(255) & nOrderID & chr(255) & "cctype" & chr(255) & cctype & chr(255) & "country" & chr(255) & country & chr(255) & "pcode" & chr(255) & pcode
	end if
	' Encrypt it
	strEncryptedValue = SimpleEncrypt(strToEncrypt)
	'===================
	
	linkDomain	= "https://www.wirelessemporium.com/"
	if instr(request.ServerVariables("SERVER_NAME"), "staging.wirelessemporium.com") > 0 then linkDomain = "http://staging.wirelessemporium.com/"
	self_link = linkDomain & "confirm?o=" & nOrderID & "&a=" & nAccountID & "&dzid=email_TRANS_HEADER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=orderconf&utm_promocode=NA"
	self_link2 = linkDomain & "confirm?o=" & nOrderID & "&a=" & nAccountID & "&dzid=email_TRANS_BODY_2L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=orderconf&utm_promocode=NA"
	
	sSqlDetails = "SELECT isnull(b.postPurchase, 0) postPurchase, d.brandName, e.modelName, A.itemid,A.PartNumber,A.itemdesc,A.price_our,isnull(B.price, 0) price,A.itemPic,c.id,c.musicSkinsID,c.artist + '-' + c.designName as prodDesc,c.price_we,B.quantity, a.subtypeid, " &_
			"CASE " &_
	"	WHEN (b.cogs IS NULL OR b.cogs = 0) THEN a.cogs " &_
	" ELSE b.cogs " &_
	" END cogs " &_
	"FROM we_orderdetails B left join we_items A ON B.itemid = A.itemid left join we_items_musicSkins C on B.itemID = C.id left join we_brands d on a.brandID = d.brandID left join we_models e on a.modelID = e.modelID WHERE B.itemID > 0 and B.orderid = '" & nOrderId & "'"
	
	session("errorSQL") = sSqlDetails
	set oRsOrderDetails = oConn.execute(sSqlDetails)
	
	dim totalDiscount
	totalDiscount = cdbl(0)
	
	if not oRsOrderDetails.eof then
		itemCount = 1
		do until oRsOrderDetails.eof
			if isnull(oRsOrderDetails("itemid")) then
				itemid = oRsOrderDetails("id")
				partnumber = oRsOrderDetails("musicSkinsID")
				itemDesc = oRsOrderDetails("prodDesc")
				qty = cdbl(oRsOrderDetails("quantity"))
				price = cdbl(oRsOrderDetails("price"))
				cogs = cdbl(oRsOrderDetails("cogs"))
				subtotal	=	cdbl(qty * price)
				itemDiscount = cdbl(subtotal * promoPercent)
				totalDiscount = cdbl(totalDiscount + itemDiscount)
				if item_ids <> "" then
					item_ids = item_ids & ","
				end if
				item_ids = item_ids & itemid
			else
				itemid 	= oRsOrderDetails("itemid")
				partnumber = oRsOrderDetails("PartNumber")
				itemDesc = insertDetailsAdv(oRsOrderDetails("itemdesc"),useBrandName,useModelName)
				qty = cdbl(oRsOrderDetails("quantity"))
				price = cdbl(oRsOrderDetails("price"))
				cogs = cdbl(oRsOrderDetails("cogs"))
				subtotal = cdbl(qty * price)

				itemDiscount = 0
				if not oRsOrderDetails("postPurchase") then  itemDiscount = cdbl(subtotal * promoPercent)
				
				totalDiscount = cdbl(totalDiscount + itemDiscount)
				if item_ids <> "" then
					item_ids = item_ids & ","
				end if
				item_ids = item_ids & itemid
			end if
			
			itempic		= oRsOrderDetails("itempic")
			
			itemImgPath = server.MapPath("/productPics/thumb/" & itempic)
			'itemImgPath = "C:\inetpub\wwwroot\productpics\thumb\" & itempic
			set fsThumb = CreateObject("Scripting.FileSystemObject")
			if not fsThumb.FileExists(itemImgPath) then
				useImg = "/productPics/thumb/imagena.jpg"
				DoNotDisplay = 1
			else
				useImg = "/productPics/thumb/" & itempic
			end if
			if incEmail = true then
				productLink = "http://www.wirelessemporium.com/p-" & itemid & "-" & formatSEO(itemDesc) & "?dzid=email_TRANS_BODY_" & itemCount+2 & "L_" & itemid & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=orderconf&utm_promocode=NA"
			else
				productLink = "http://www.wirelessemporium.com/p-" & itemid & "-" & formatSEO(itemDesc)
			end if
			
			strOrderDetails =	strOrderDetails &	"	<tr>" & vbcrlf & _
													"		<td style='border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;'>" & partnumber & "</td>" & vbcrlf & _
													"		<td style='border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;'><a href=""" & productLink & """><img src='https://www.wirelessemporium.com" & useImg & "' height='65' /></a></td>" & vbcrlf & _
													"		<td style='border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;'>" & formatnumber(qty,0) & "</td>" & vbcrlf & _
													"		<td style='border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;'><a href=""" & productLink & """ style=""color:#000000;text-decoration:underline;"">" & itemDesc & "</a></td>" & vbcrlf & _
													"		<td style='border-bottom:1px solid #999999;border-right:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;'>" & formatcurrency(price) & "</td>" & vbcrlf & _
													"		<td style='border-bottom:1px solid #999999;text-align:center;padding:10px 5px;font-size:12px;background:#ffffff;font-family:Arial, Helvetica, sans-serif;'>" & formatcurrency(subtotal) & "</td>" & vbcrlf & _
													"	</tr>"
			weDataString = weDataString & "window.WEDATA.orderData.cartItems.push({partNumber: " & jsStr(partnumber) & ", qty: " & jsStr(qty) & ",price: " & jsStr(price) & ",itemId: " & jsStr(itemid) & ", cogs: " & jsStr(cogs) & ", isPostPurchase: " & jsStr(LCase(oRsOrderDetails("postPurchase"))) & ", discount: " & jsStr(itemDiscount) & "});" & vbcrlf
			
			oRsOrderDetails.movenext
			itemCount = itemCount + 1
		loop
		oRsOrderDetails.close
	end if
	
	if item_ids then
		sqlUpSell = "exec sp_ppUpsell 0, '" & item_ids & "', 0, 30"
		set objRsUpSell = oConn.execute(sqlUpSell)
		if not objRsUpSell.EOF then
			strUpSell	=	"<tr>" & vbcrlf & _
							"	<td style='background-color: #ffffff;' width='640'>" & vbcrlf & _
							"		<table width='640' cellspacing='0' cellpadding='0'>" & vbcrlf & _
							"			<tbody>" & vbcrlf & _
							"				<tr>"
			
			counter = 1
			do until objRsUpSell.EOF
				itemID		= objRsUpSell("itemID")
				itemPic		= objRsUpSell("itemPic")
				itemDesc	= objRsUpSell("itemDesc")
				useItemDesc = replace(itemDesc,"  "," ")
				itemImgPath = server.MapPath("/productPics/thumb/" & itemPic)
				price		= objRsUpSell("price")
				discount	= price * 0.15
				promoPrice	= price - discount
				price_retail= objRsUpSell("price_retail")
				
				itemImgPath = server.MapPath("\productpics\big\" & itempic)
				'itemImgPath = "C:\inetpub\wwwroot\productpics\thumb\" & itempic
				set fsBig = CreateObject("Scripting.FileSystemObject")
				if not fsBig.FileExists(itemImgPath) then
					useImg = "/productPics/big/imagena.jpg"
					DoNotDisplay = 1
				else
					useImg = "/productPics/big/" & itemPic
				end if
				
				if counter = 3 or counter = 6 then
					style = "padding:20px 10px 0 20px;"
				else 
					style = "padding:20px 0 0 20px;"
				end if
				
				if incEmail = true then
					product_link = "http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(useItemDesc) & "?dzid=email_TRANS_RECS_" & counter & "L_" & itemID & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=orderconf&utm_promocode=NA"
				else 
					product_link = "http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(useItemDesc)
				end if
				
				strUpSell 	= strUpSell &	"	<td width='210' style='" & style & "'>" & vbcrlf & _
												"		<table width='190' cellpadding='0' cellspacing='0'>" & vbcrlf & _
												"			<tbody>" & vbcrlf & _
												"				<tr>" & vbcrlf & _
												"					<td width='190' height='190'>" & vbcrlf & _
												"						<a href='" & product_link & "'><img src='https://www.wirelessemporium.com" & useImg & "' style='display:block;' width='190' /></a>" & vbcrlf & _
												"					</td>" & vbcrlf & _
												"				</tr>" & vbcrlf & _
												"				<tr>" & vbcrlf & _
												"					<td width='190' height='60' style=""padding:7px 0px 7px 0px;"" valign=""top"">" & vbcrlf & _
												"						<a href='" & product_link & "' target=""_blank"" style=""font-size:12px; color:#333; font-family: Arial, sans-serif;"">" & useItemDesc & "</a>" & vbcrlf & _
												"					</td>" & vbcrlf & _
												"				</tr>" & vbcrlf & _
												"				<tr>" & vbcrlf & _
												"					<td>" & vbcrlf & _
												"						<span style='color:#999;font-size:12px;color:#ccc;text-decoration:line-through;font-family:Arial, sans-serif;'>Retail Price: " & formatCurrency(price_retail) & "</span><br />" & vbcrlf & _
												"						<span style='color:#999;font-size:12px;color:#ccc;text-decoration:line-through;font-family:Arial, sans-serif;'>Our Price: " & formatCurrency(price) & "</span><br />" & vbcrlf & _
												"					</td>" & vbcrlf & _
												"				</tr>" & vbcrlf & _
												"				<tr>" & vbcrlf & _
												"					<td style='padding:10px 0 0 0;'>" & vbcrlf & _
												"						<span style='color:#000;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;'>Promo Price:</span> <span style='color:#ff6633;font-size:16px;font-weight:bold;font-family:Arial, sans-serif;'>" & formatCurrency(promoPrice) & "</span><br />" & vbcrlf & _
												"						<span style='color:#000;font-size:14px;font-family:Arial, sans-serif;'>You Save:</span> <span style='margin:10px 0 0 0;color:#000;font-size:14px;font-family:Arial, sans-serif;font-weight:bold;'>15% (" & formatCurrency(discount) & ")</span>" & vbcrlf & _
												"					</td>" & vbcrlf & _
												"				</tr>" & vbcrlf & _
												"				<tr>" & vbcrlf & _
												"					<td style='padding:10px 0 0 0;'>" & vbcrlf & _
												"						<table width='160' border='0' cellpadding='0' cellspacing='0'>" & vbcrlf & _
												"							<tbody>" & vbcrlf & _
												"								<tr>" & vbcrlf & _
												"									<td align='center' style='background-color:#ff6633;border-radius:16px;padding:7px 0;'>" & vbcrlf & _
												"										<a href='" & product_link & "' style='color:#fff;font-weight:bold;font-size:14px;font-family:Arial, sans-serif;text-decoration:none;'>SHOP NOW &raquo;</a>" & vbcrlf & _
												"									</td>" & vbcrlf & _
												"								</tr>" & vbcrlf & _
												"							</tbody>" & vbcrlf & _
												"						</table>" & vbcrlf & _
												"					</td>" & vbcrlf & _
												"				</tr>" & vbcrlf & _
												"			</tbody>" & vbcrlf & _
												"		</table>" & vbcrlf & _
												"	</td>"
				
				if counter = 3 then
					strUpSell = strUpSell & "						</tr>" & vbcrlf & _
								"					</tbody>" & vbcrlf & _
								"				</table>" & vbcrlf & _
								"				<br />" & vbcrlf & _
								"			</td>" & vbcrlf & _
								"		</tr>" & vbcrlf & _
								"		<tr>" & vbcrlf & _
								"		  <td style='background-color: #cccccc;' width='640' height='1'></td>" & vbcrlf & _
								"		</tr>" & vbcrlf & _
								"		<tr>" & vbcrlf & _
								"			<td style='background-color: #ffffff;' width='640'>" & vbcrlf & _
								"				<table width='640' cellspacing='0' cellpadding='0'>" & vbcrlf & _
								"					<tbody>" & vbcrlf & _
								"						<tr>"
					counter = counter + 1
				elseif counter = 6 then
					exit do
				else
					counter = counter + 1
				end if
				objRsUpSell.movenext
			loop
			objRsUpsell.close
		end if
		strUpSell		= strUpSell & "						</tr>" & vbcrlf & _
								"					</tbody>" & vbcrlf & _
								"				</table>" & vbcrlf & _
								"				<br />" & vbcrlf & _
								"			</td>" & vbcrlf & _
								"		</tr>"
	end if
	
	
	dzid = request.querystring("dzid")
	
	if incEmail = true or dzid <> "" then
		ReceiptText = readTextFile(server.MapPath("\images\email\template\orderConfirmEmail.htm"))
	else 
		ReceiptText = readTextFile(server.MapPath("\images\email\template\orderConfirmPage.htm"))
	end if
	
	ReceiptText = replace(ReceiptText, "[SELF LINK]", self_link)
	ReceiptText = replace(ReceiptText, "[SELF LINK2]", self_link2)
	ReceiptText = replace(ReceiptText, "[BODY NUM LINK]", itemCount+2)
	ReceiptText = replace(ReceiptText, "[ORDER EMAIL]", sEmail)
	ReceiptText = replace(ReceiptText, "[ORDER DATE]", nOrderDateTime)
	ReceiptText = replace(ReceiptText, "[ORDER ID]", nOrderId)
	ReceiptText = replace(ReceiptText, "[ORDER DETAILS]", strOrderDetails)
	ReceiptText = replace(ReceiptText, "[SHIPPING TYPE]", sShipType)
	ReceiptText = replace(ReceiptText, "[CUSTOMER NAME ADDRESS]", custNameAddress)
	ReceiptText = replace(ReceiptText, "[BILLED TO]", sBillAddress)
	ReceiptText = replace(ReceiptText, "[SHIPPED TO]", sShipAddress)
	ReceiptText = replace(ReceiptText, "[ORDER SUBTOTAL]", formatCurrency(emailSubTotal,2))
	strTaxAmount = ""
	if emailOrderTax > 0 then
		strTaxAmount = strTaxAmount & "								<tr>" & vbcrlf
		strTaxAmount = strTaxAmount & "									<td style=""background:#cccccc;padding:5px 10px;text-align:left;font-family:Arial, Helvetica, sans-serif;font-size:14px;font-weight:bold;"">TAX:</td>" & vbcrlf
		strTaxAmount = strTaxAmount & "									<td style=""background:#cccccc;padding:5px 10px;text-align:right;font-family:Arial, Helvetica, sans-serif;font-size:14px;"">" & formatCurrency(emailOrderTax,2) & "</td>" & vbcrlf
		strTaxAmount = strTaxAmount & "								</tr>" & vbcrlf
	end if
	ReceiptText = replace(ReceiptText, "[TAX AMOUNT]", strTaxAmount)
	ReceiptText = replace(ReceiptText, "[SHIPPING FEE]", formatCurrency(emailShipFee,2))
	strPromoCode = ""
	if sPromoCode <> "" then
		strPromoCode = strPromoCode & "								<tr>" & vbcrlf
		strPromoCode = strPromoCode & "									<td style=""background:#cccccc;padding:5px 10px;text-align:left;font-family:Arial, Helvetica, sans-serif;font-size:14px;font-weight:bold;"">PROMO CODE:</td>" & vbcrlf
		strPromoCode = strPromoCode & "									<td style=""background:#cccccc;padding:5px 10px;text-align:right;font-family:Arial, Helvetica, sans-serif;font-size:14px;"">" & sPromoCode & "</td>" & vbcrlf
		strPromoCode = strPromoCode & "								</tr>" & vbcrlf
	end if
	ReceiptText = replace(ReceiptText, "[PROMO CODE]", strPromoCode)
	ReceiptText = replace(ReceiptText, "[DISCOUNT TOTAL]", formatCurrency(totalDiscount,2))
	ReceiptText = replace(ReceiptText, "[GRAND TOTAL]", formatCurrency(emailOrderGrandTotal,2))
	strPostPurchase = ""
	if (emailOrderGrandTotal - nOrderGrandTotal) > 1 and nOrderGrandTotal > 0 then
		strPostPurchase = strPostPurchase & "				<p style=""margin:0; padding:0 0 20px 0;line-height:22px;"">Thank you for your order! Please remember that you will see two charges on your account that total <b>" & formatCurrency(emailOrderGrandTotal,2) & "</b>. One charge in the amount of <b>" & formatCurrency((emailOrderGrandTotal - nOrderGrandTotal),2) & "</b> and the other in the amount of <b>" & formatCurrency(nOrderGrandTotal,2) & "</b>.</p>" & vbcrlf
	end if
	ReceiptText = replace(ReceiptText, "[POST PURCHASE]", strPostPurchase)
	ReceiptText = replace(ReceiptText, "[UPSELL]", strUpSell)
	ReceiptText = replace(ReceiptText, "[COPYRIGHT YEAR]", Year(Date))
	
	addoubleclick = addoubleclick & "<table width=""25%"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center""><tr><td>" & vbcrlf
	if incEmail = true then
		addoubleclick = addoubleclick & "<a href=""https://one-time-offer.com/sg/gateway.aspx?v=0353A36353&p=9202F21232BE77B7D7F706E72FC3B3930303639F16D6060727&cl=1545"" target=""_blank""><img src=""http://www.wirelessemporium.com/images/checkout/rebate_img.png"" width=""276"" height=""66"" border=""0"" alt=""Click here now to claim your Cash Back Incentive on your next purchase when you enroll in Webloyalty's service. See offer and billing details.""></a>" & vbcrlf
	else
		addoubleclick = addoubleclick & "<SCRIPT language=""JavaScript1.1"" SRC=""https://ad.doubleclick.net/adj/N3446.Wireless_Emporium/B2088600;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?""></SCRIPT>" & vbcrlf
		addoubleclick = addoubleclick & "<NOSCRIPT>" & vbcrlf
		addoubleclick = addoubleclick & "<A HREF=""https://ad.doubleclick.net/jump/N3446.Wireless_Emporium/B2088600;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?"" target=""_blank""><IMG SRC=""https://ad.doubleclick.net/ad/N3446.Wireless_Emporium/B2088600;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?"" BORDER=0 WIDTH=300 HEIGHT=130 ALT=""""></A>" & vbcrlf
		addoubleclick = addoubleclick & "</NOSCRIPT>" & vbcrlf
	end if
	addoubleclick = addoubleclick & "</td></tr></table>" & vbcrlf
	
	
	
	
	ReceiptText = replace(ReceiptText, "[WEB LOYALTY]", addoubleclick)
end if
%>