<%
function addCommaString(base, strAdd)
	if isnull(base) or base = "" then
		addCommaString = strAdd
	else
		addCommaString = base & "," & strAdd
	end if
end function

'============== new version (12/20/2011 by Terry)
function getCrossSellItems(pItemID, byref pStrItems)
	dim nItems : nItems = 0
	
	sql = "select itemid, modelid, typeid, partnumber from we_items where itemid = '" & pItemID & "'"
	session("errorSQL") = sql
	set objRsItem = Server.CreateObject("ADODB.Recordset")
	objRsItem.open sql, oConn, 0, 1	
	
	if objRsItem.eof then
		sql = "select id itemid, modelid, 20 typeid, musicSkinsID partnumber from we_items_musicskins where id = '" & pItemID & "'"
		session("errorSQL") = sql
		set objRsItem = Server.CreateObject("ADODB.Recordset")
		objRsItem.open sql, oConn, 0, 1
	end if
	
	if not objRsItem.eof then
		dim tPartNumber, tModelID, tTypeID, sqlOrderBy
		tPartNumber = objRsItem("partnumber")
		tModelID = objRsItem("modelid")
		tTypeID = objRsItem("typeid")
		set objRsItem = nothing
		
		sql = 	"select	typeid, subtypeid, isnull(method, '%') method, isnull(method2, '%') method2, isnull(method3, '%') method3" & vbcrlf & _
				"	, 	isnull(altMethod, '%') altMethod, isnull(wordintitle, '%') wordintitle, isnull(wordintitle2, '%') wordintitle2" & vbcrlf & _
				"	,	crossselldesc, modelspecific, topselling, mostexpensive, lowestPrice, random, orderNum, itemid" & vbcrlf & _
				"from	v_crossSellMatrix" & vbcrlf & _
				"where	matrixTypeID = '" & tTypeID & "'" & vbcrlf & _
				"	and	prefix = '" & left(tPartNumber, 3) & "'" & vbcrlf & _
				"	and	site_id = 0" & vbcrlf & _
				"	union" & vbcrlf & _
				"	select	typeid, subtypeid, isnull(method, '%') method, isnull(method2, '%') method2, isnull(method3, '%') method3" & vbcrlf & _
				"		, 	isnull(altMethod, '%') altMethod, isnull(wordintitle, '%') wordintitle, isnull(wordintitle2, '%') wordintitle2" & vbcrlf & _
				"		,	crossselldesc, modelspecific, topselling, mostexpensive, lowestPrice, random, 0 orderNum, itemid" & vbcrlf & _
				"	from	CrossSellPlan" & vbcrlf & _
				"	where	id = 69" & vbcrlf & _
				"		and	( select top 1 isnull(partnumber, '') partnumber from we_items with (nolock) where itemid = '" & pItemID & "' ) like 'LC4-MOT-MB81%'" & vbcrlf & _
				 "order by orderNum" & vbcrlf

		session("errorSQL") = sql
		set objRsRules = Server.CreateObject("ADODB.Recordset")
		objRsRules.open sql, oConn, 0, 1
		if objRsRules.eof then 'use default
			sql = 	"select	typeid, subtypeid, isnull(method, '%') method, isnull(method2, '%') method2, isnull(method3, '%') method3" & vbcrlf & _
					"	, 	isnull(altMethod, '%') altMethod, isnull(wordintitle, '%') wordintitle, isnull(wordintitle2, '%') wordintitle2" & vbcrlf & _
					"	,	crossselldesc, modelspecific, topselling, mostexpensive, lowestPrice, random, orderNum, itemid" & vbcrlf & _
					"from	v_crossSellMatrix" & vbcrlf & _
					"where	matrixTypeID = '8'" & vbcrlf & _
					"	and	prefix = 'XXX'" & vbcrlf & _
					"	and	site_id = 0" & vbcrlf & _				
					"order by orderNum" & vbcrlf			
			session("errorSQL") = sql					
			set objRsRules = Server.CreateObject("ADODB.Recordset")
			objRsRules.open sql, oConn, 0, 1
		end if
		
'		response.write "<pre>" & sql & "</pre><br><br>"
				
		do until objRsRules.eof
			checkHandsFreeType = false
			sqlOrderBy = ""
			tOrderNum = objRsRules("orderNum")
			HandsfreeTypes = ""
			musicSkins = 0
			if objRsRules("typeid") = 20 then
				musicSkins = 1
			end if
			if 5 = objRsRules("typeid") or left(objRsRules("method"), 2) = "BT" or left(objRsRules("method"), 2) = "HF" then 
				checkHandsFreeType = true
			end if
			
			if musicSkins = 1 then
				sql = 	"select	top 1 id itemid, rand(cast(cast(newid() as binary(8)) as int)) * id id" & vbcrlf & _
						"from	we_items_musicskins" & vbcrlf & _
						"where 	skip = 0 " & vbcrlf & _
						"	and deleteItem = 0 " & vbcrlf & _
						"	and (artist <> '' and artist is not null) " & vbcrlf & _
						"	and (designname <> '' and designname is not null) " & vbcrlf & _
						"	and deleteItem = 0 " & vbcrlf & _
						"	and modelid = '" & tModelID & "' " & vbcrlf & _
						"order by 2" & vbcrlf
			else
				sql = 	"select	top 1 itemid, rand(cast(cast(newid() as binary(8)) as int)) * itemID id " & vbcrlf & _
						"from 	we_items " & vbcrlf & _
						"where 	hidelive = 0 and inv_qty <> 0 and price_our > 0 " & vbcrlf
					
				if checkHandsFreeType then
					tempSql = 	"select b.brandid, nullif(nullif(b.brandName, 'Universal'), 'Other') brandName, isnull(HandsfreeTypes, -1) HandsfreeTypes " & vbcrlf & _
								"from we_models a left outer join we_brands b on a.brandid = b.brandid where modelid = '" & tModelID & "'"
					session("errorSQL") = tempSql
					set objRsTemp = oConn.execute(tempSql)
					if not objRsTemp.eof then HandsfreeTypes = left(objRsTemp("HandsfreeTypes"),len(objRsTemp("HandsfreeTypes"))-1)
					if len(HandsfreeTypes) > 0 then  sql = sql & "	and	handsfreetype in ('" & replace(HandsfreeTypes,",","','") & "')" & vbcrlf
					if isnull(objRsRules("typeid")) then sql = sql & "	and partnumber like '" & objRsRules("method") & "'" & vbcrlf
					sql = sql & "	and (itemdesc not like '%cable%' and itemdesc not like '%audio adapter%')" & vbcrlf
				elseif not isnull(objRsRules("itemid")) then
					sql = 	sql	&	"	and	itemid = '" & objRsRules("itemid") & "'" & vbcrlf
				elseif not isnull(objRsRules("typeid")) then
					sql = 	sql	&	"	and	typeid = '" & objRsRules("typeid") & "'" & vbcrlf & _
									"	and	itemdesc like '" & objRsRules("wordintitle") & "' and itemdesc like '" & objRsRules("wordintitle2") & "'" & vbcrlf					
				elseif not isnull(objRsRules("subtypeid")) then
					sql = 	sql	&	"	and	subtypeid = '" & objRsRules("subtypeid") & "'" & vbcrlf & _
									"	and	itemdesc like '" & objRsRules("wordintitle") & "' and itemdesc like '" & objRsRules("wordintitle2") & "'" & vbcrlf
				else
					sql = 	sql	&	"	and	partnumber like '" & objRsRules("method") & "' and partnumber like '" & objRsRules("method2") & "' and partnumber like '" & objRsRules("method3") & "'" & vbcrlf & _
									"	and	itemdesc like '" & objRsRules("wordintitle") & "' and itemdesc like '" & objRsRules("wordintitle2") & "'" & vbcrlf
				end if
				
				if objRsRules("modelspecific") and not checkHandsFreeType then sql = sql & "	and modelid = '" & tModelID & "'" & vbcrlf end if
				if objRsRules("topselling") then sqlOrderBy = addCommaString(sqlOrderBy, "numberOfSales desc") end if
				if objRsRules("mostexpensive") then sqlOrderBy = addCommaString(sqlOrderBy, "price_our desc") end if
				if objRsRules("lowestPrice") then sqlOrderBy = addCommaString(sqlOrderBy, "price_our") end if
				if sqlOrderBy <> "" then 
					sql = sql & " order by " & sqlOrderBy 
				else
					sql = sql & " order by 2 "
				end if
			end if
			
'			response.write "<pre>" & sql & "</pre><br><br>"				

			session("errorSQL") = sql
			set objRsInner = oConn.execute(sql)
			if objRsInner.eof then
				if objRsRules("altMethod") <> "%" then
					sql = 	"select	top 1 itemid, rand(cast(cast(newid() as binary(8)) as int)) * itemID id " & vbcrlf & _
							"from 	we_items " & vbcrlf & _
							"where 	hidelive = 0 and inv_qty <> 0 and price_our > 0 " & vbcrlf & _
							"	and	partnumber like '" & objRsRules("altMethod") & "'" & vbcrlf

					if instr(objRsRules("altMethod"), "UNI") = 0 and objRsRules("modelspecific") then sql = sql & "	and modelid = '" & tModelID & "'" & vbcrlf end if
					if objRsRules("topselling") then sqlOrderBy = addCommaString(sqlOrderBy, "numberOfSales desc") end if
					if objRsRules("mostexpensive") then sqlOrderBy = addCommaString(sqlOrderBy, "price_our desc") end if
					if objRsRules("lowestPrice") then sqlOrderBy = addCommaString(sqlOrderBy, "price_our") end if
					if sqlOrderBy <> "" then 
						sql = sql & " order by " & sqlOrderBy 
					else
						sql = sql & " order by 2 "
					end if
					
					session("errorSQL") = sql
					set objRsInner2 = oConn.execute(sql)
					if not objRsInner2.eof then
						pStrItems = addCommaString(pStrItems, objRsInner2("itemid"))
						nItems = nItems + 1
					end if
				end if	
			else
				pStrItems = addCommaString(pStrItems, objRsInner("itemid"))
				nItems = nItems + 1
			end if
			objRsRules.movenext
		loop	
		set objRsRules = nothing	
	end if
	
	pStrItems = replace(pStrItems, " ", "")
	pStrItems = replace(pStrItems, ",,", ",")
end function

function CrossSells(itemID)
	itemID = cStr(itemID)
	if itemID <> "" then
		dim a, b, crossSell(3), defaultItemID(5)
		a = 1
		SQL = "SELECT itemID, modelID, typeID, partNumber FROM we_items WHERE itemID = '" & itemID & "'"
		set RS = oConn.execute(SQL)
		if not RS.eof then
			dim SQLbase, modelID, RS2, RS3
			SQLbase = "SELECT itemID FROM we_items WHERE hidelive = 0 AND inv_qty <> 0 AND price_Our > 0 AND itemID <> '" & RS("itemID") & "'"
			modelID = RS("modelID")
			select case RS("typeID")
				case 1
					SQL = SQLbase & " AND PartNumber LIKE 'SBC-%' AND modelID = '" & modelID & "'"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = SQLbase & " AND PartNumber LIKE 'DCH-%' AND modelID = '" & modelID & "'"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = SQLbase & " AND typeID = 16 ORDER BY price_Our"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					if a < 3 then
						SQL = SQLbase & " AND typeID = 1 AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'TC3-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'CC1-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'LC0-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
				case 2
					if left(RS("PartNumber"),2) = "CC" then
						SQL = SQLbase & " AND PartNumber LIKE 'SCR-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
						SQL = SQLbase & " AND PartNumber LIKE 'ACP-%'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
						SQL = "SELECT HandsfreeTypes FROM we_models WHERE HandsfreeTypes IS NOT NULL AND modelID = '" & modelID & "'"
						set RS3 = oConn.execute(SQL)
						if not RS3.eof then
							SQL = SQLbase & " AND PartNumber LIKE 'BT1-%' AND HandsfreeType IN (" & left(RS3("HandsfreeTypes"),len(RS3("HandsfreeTypes"))-1) & ")"
							set RS2 = oConn.execute(SQL)
							if not RS2.eof then
								crossSell(a) = RS2("itemID")
								a = a + 1
							end if
						end if
						RS3.close
						set RS3 = nothing
						if a < 3 then
							SQL = "SELECT HandsfreeTypes FROM we_models WHERE HandsfreeTypes IS NOT NULL AND modelID = '" & modelID & "'"
							set RS3 = oConn.execute(SQL)
							if not RS3.eof then
								SQL = SQLbase & " AND PartNumber LIKE 'HF1-%' AND HandsfreeType IN (" & left(RS3("HandsfreeTypes"),len(RS3("HandsfreeTypes"))-1) & ")"
								set RS2 = oConn.execute(SQL)
								if not RS2.eof then
									crossSell(a) = RS2("itemID")
									a = a + 1
								end if
							end if
							RS3.close
							set RS3 = nothing
						end if
						if a < 3 then
							SQL = SQLbase & " AND PartNumber LIKE 'BAT-%' AND modelID = '" & modelID & "'"
							set RS2 = oConn.execute(SQL)
							if not RS2.eof then
								crossSell(a) = RS2("itemID")
								a = a + 1
							end if
						end if
					elseif left(RS("PartNumber"),2) = "TC" then
						SQL = SQLbase & " AND PartNumber LIKE 'LC0-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
						SQL = SQLbase & " AND PartNumber LIKE 'MEM-%'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
						SQL = SQLbase & " AND PartNumber LIKE 'BAT-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
						if a < 3 then
							SQL = SQLbase & " AND typeID = 16 ORDER BY price_Our"
							set RS2 = oConn.execute(SQL)
							if not RS2.eof then
								crossSell(a) = RS2("itemID")
								a = a + 1
							end if
						end if
						if a < 3 then
							SQL = SQLbase & " AND PartNumber LIKE 'SPK-%' AND modelID = '" & modelID & "'"
							set RS2 = oConn.execute(SQL)
							if not RS2.eof then
								crossSell(a) = RS2("itemID")
								a = a + 1
							end if
						end if
						if a < 3 then
							SQL = SQLbase & " AND PartNumber LIKE 'DCH-%' AND modelID = '" & modelID & "'"
							set RS2 = oConn.execute(SQL)
							if not RS2.eof then
								crossSell(a) = RS2("itemID")
								a = a + 1
							end if
						end if
						if a < 3 then
							SQL = SQLbase & " AND PartNumber LIKE 'SBC-%' AND modelID = '" & modelID & "'"
							set RS2 = oConn.execute(SQL)
							if not RS2.eof then
								crossSell(a) = RS2("itemID")
								a = a + 1
							end if
						end if
						if a < 3 then
							SQL = SQLbase & " AND typeID = 5 AND modelID = '" & modelID & "'"
							set RS2 = oConn.execute(SQL)
							if not RS2.eof then
								crossSell(a) = RS2("itemID")
								a = a + 1
							end if
						end if
					else
						SQL = SQLbase & " AND PartNumber LIKE 'SCR-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
						SQL = SQLbase & " AND PartNumber LIKE 'ACP-%'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
						SQL = SQLbase & " AND typeID = 16 ORDER BY price_Our"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
						if a < 3 then
							SQL = SQLbase & " AND typeID = 5 AND modelID = '" & modelID & "'"
							set RS2 = oConn.execute(SQL)
							if not RS2.eof then
								crossSell(a) = RS2("itemID")
								a = a + 1
							end if
						end if
						if a < 3 then
							SQL = SQLbase & " AND typeID = 7 AND modelID = '" & modelID & "'"
							set RS2 = oConn.execute(SQL)
							if not RS2.eof then
								crossSell(a) = RS2("itemID")
								a = a + 1
							end if
						end if
					end if
				case 3
					SQL = SQLbase & " AND PartNumber LIKE 'SCR-%' AND modelID = '" & modelID & "'"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = "SELECT HandsfreeTypes FROM we_models WHERE HandsfreeTypes IS NOT NULL AND modelID = '" & modelID & "'"
					set RS3 = oConn.execute(SQL)
					if not RS3.eof then
						SQL = SQLbase & " AND PartNumber LIKE 'BT1-%' AND HandsfreeType IN (" & left(RS3("HandsfreeTypes"),len(RS3("HandsfreeTypes"))-1) & ")"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					RS3.close
					set RS3 = nothing
					SQL = "SELECT HandsfreeTypes FROM we_models WHERE HandsfreeTypes IS NOT NULL AND modelID = '" & modelID & "'"
					set RS3 = oConn.execute(SQL)
					if not RS3.eof then
						SQL = SQLbase & " AND PartNumber LIKE 'HF1-%' AND HandsfreeType IN (" & left(RS3("HandsfreeTypes"),len(RS3("HandsfreeTypes"))-1) & ")"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					RS3.close
					set RS3 = nothing
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'UNI-CHM-%'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'STY-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'LC5-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'LC9-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'CC1-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
				case 5
					SQL = SQLbase & " AND PartNumber LIKE 'BT1-%' ORDER BY price_Our"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = SQLbase & " AND typeID = 7"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = SQLbase & " AND PartNumber LIKE 'ACP-%'"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'DAT-%'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'HST-%'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
				case 6
					SQL = SQLbase & " AND typeID = 7 AND modelID = '" & modelID & "'"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = "SELECT HandsfreeTypes FROM we_models WHERE HandsfreeTypes IS NOT NULL AND modelID = '" & modelID & "'"
					set RS3 = oConn.execute(SQL)
					if not RS3.eof then
						SQL = SQLbase & " AND PartNumber LIKE 'BT1-%' AND HandsfreeType IN (" & left(RS3("HandsfreeTypes"),len(RS3("HandsfreeTypes"))-1) & ")"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					RS3.close
					set RS3 = nothing
					SQL = "SELECT HandsfreeTypes FROM we_models WHERE HandsfreeTypes IS NOT NULL AND modelID = '" & modelID & "'"
					set RS3 = oConn.execute(SQL)
					if not RS3.eof then
						SQL = SQLbase & " AND PartNumber LIKE 'HF1-%' AND HandsfreeType IN (" & left(RS3("HandsfreeTypes"),len(RS3("HandsfreeTypes"))-1) & ")"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					RS3.close
					set RS3 = nothing
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'MEM-%'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'CLP-%'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
				case 7
					if left(RS("PartNumber"),3) = "LC5" then
						SQL = SQLbase & " AND typeID = 3 AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
						SQL = SQLbase & " AND PartNumber LIKE 'UNI-CHM-%'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
						SQL = SQLbase & " AND PartNumber LIKE 'SCR-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
						if a < 3 then
							SQL = SQLbase & " AND PartNumber LIKE 'STY-%' AND modelID = '" & modelID & "'"
							set RS2 = oConn.execute(SQL)
							if not RS2.eof then
								crossSell(a) = RS2("itemID")
								a = a + 1
							end if
						end if
						if a < 3 then
							SQL = "SELECT HandsfreeTypes FROM we_models WHERE HandsfreeTypes IS NOT NULL AND modelID = '" & modelID & "'"
							set RS3 = oConn.execute(SQL)
							if not RS3.eof then
								SQL = SQLbase & " AND PartNumber LIKE 'BT1-%' AND HandsfreeType IN (" & left(RS3("HandsfreeTypes"),len(RS3("HandsfreeTypes"))-1) & ")"
								set RS2 = oConn.execute(SQL)
								if not RS2.eof then
									crossSell(a) = RS2("itemID")
									a = a + 1
								end if
							end if
							RS3.close
							set RS3 = nothing
						end if
						if a < 3 then
							SQL = "SELECT HandsfreeTypes FROM we_models WHERE HandsfreeTypes IS NOT NULL AND modelID = '" & modelID & "'"
							set RS3 = oConn.execute(SQL)
							if not RS3.eof then
								SQL = SQLbase & " AND PartNumber LIKE 'HF1-%' AND HandsfreeType IN (" & left(RS3("HandsfreeTypes"),len(RS3("HandsfreeTypes"))-1) & ")"
								set RS2 = oConn.execute(SQL)
								if not RS2.eof then
									crossSell(a) = RS2("itemID")
									a = a + 1
								end if
							end if
							RS3.close
							set RS3 = nothing
						end if
					else
						SQL = SQLbase & " AND PartNumber LIKE 'SCR-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
						SQL = SQLbase & " AND PartNumber LIKE 'DAT-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
						SQL = SQLbase & " AND PartNumber LIKE 'CC1-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
						if a < 3 then
							SQL = "SELECT HandsfreeTypes FROM we_models WHERE HandsfreeTypes IS NOT NULL AND modelID = '" & modelID & "'"
							set RS3 = oConn.execute(SQL)
							if not RS3.eof then
								SQL = SQLbase & " AND PartNumber LIKE 'BT1-%' AND HandsfreeType IN (" & left(RS3("HandsfreeTypes"),len(RS3("HandsfreeTypes"))-1) & ")"
								set RS2 = oConn.execute(SQL)
								if not RS2.eof then
									crossSell(a) = RS2("itemID")
									a = a + 1
								end if
							end if
							RS3.close
							set RS3 = nothing
						end if
						if a < 3 then
							SQL = SQLbase & " AND PartNumber LIKE 'STY-%' AND modelID = '" & modelID & "'"
							set RS2 = oConn.execute(SQL)
							if not RS2.eof then
								crossSell(a) = RS2("itemID")
								a = a + 1
							end if
						end if
						if a < 3 then
							SQL = SQLbase & " AND PartNumber LIKE 'HST-%' AND modelID = '" & modelID & "'"
							set RS2 = oConn.execute(SQL)
							if not RS2.eof then
								crossSell(a) = RS2("itemID")
								a = a + 1
							end if
						end if
						if a < 3 then
							SQL = SQLbase & " AND PartNumber LIKE 'CLP-%' AND modelID = '" & modelID & "'"
							set RS2 = oConn.execute(SQL)
							if not RS2.eof then
								crossSell(a) = RS2("itemID")
								a = a + 1
							end if
						end if
					end if
				case 12
					SQL = SQLbase & " AND typeID = 7 AND modelID = '" & modelID & "'"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = SQLbase & " AND typeID = 16 ORDER BY price_Our"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = SQLbase & " AND typeID = 3 AND modelID = '" & modelID & "'"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					if a < 3 then
						SQL = SQLbase & " AND typeID = 2 AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
				case 13
					SQL = "SELECT HandsfreeTypes FROM we_models WHERE HandsfreeTypes IS NOT NULL AND modelID = '" & modelID & "'"
					set RS3 = oConn.execute(SQL)
					if not RS3.eof then
						SQL = SQLbase & " AND PartNumber LIKE 'BT1-%' AND HandsfreeType IN (" & left(RS3("HandsfreeTypes"),len(RS3("HandsfreeTypes"))-1) & ")"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					RS3.close
					set RS3 = nothing
					SQL = SQLbase & " AND PartNumber LIKE 'SCR-%' AND modelID = '" & modelID & "'"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = SQLbase & " AND typeID = 7 AND modelID = '" & modelID & "'"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'DCH-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					if a < 3 then
						SQL = SQLbase & " AND typeID = 16 ORDER BY price_Our"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'BAT-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
				case 14
					SQL = SQLbase & " AND typeID = 8"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = SQLbase & " AND PartNumber LIKE 'SPK-%'"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = SQLbase & " AND typeID = 16 ORDER BY price_Our"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
				case 15
					SQL = SQLbase & " AND typeID = 16 ORDER BY price_Our"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = SQLbase & " AND PartNumber LIKE 'BT1-%'"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = SQLbase & " AND PartNumber LIKE 'LC0-%'"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'HF1-%'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
				case 16
					SQL = SQLbase & " AND PartNumber LIKE 'CC1-%' AND modelID = '" & modelID & "'"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = SQLbase & " AND PartNumber LIKE 'SCR-%' AND modelID = '" & modelID & "'"
					set RS2 = oConn.execute(SQL)
					if not RS2.eof then
						crossSell(a) = RS2("itemID")
						a = a + 1
					end if
					SQL = "SELECT HandsfreeTypes FROM we_models WHERE HandsfreeTypes IS NOT NULL AND modelID = '" & modelID & "'"
					set RS3 = oConn.execute(SQL)
					if not RS3.eof then
						SQL = SQLbase & " AND PartNumber LIKE 'BT1-%' AND HandsfreeType IN (" & left(RS3("HandsfreeTypes"),len(RS3("HandsfreeTypes"))-1) & ")"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					RS3.close
					set RS3 = nothing
					if a < 3 then
						SQL = SQLbase & " AND PartNumber LIKE 'STY-%' AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
					if a < 3 then
						SQL = SQLbase & " AND typeID = 7 AND modelID = '" & modelID & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					end if
				case else
					for b = 1 to 3
						SQL = SQLbase & " AND typeID = 8 AND itemID <> '" & crossSell(1) & "' AND itemID <> '" & crossSell(2) & "' AND itemID <> '" & crossSell(3) & "'"
						set RS2 = oConn.execute(SQL)
						if not RS2.eof then
							crossSell(a) = RS2("itemID")
							a = a + 1
						end if
					next
			end select
			defaultItemID(1) = 10848
			defaultItemID(2) = 2385
			defaultItemID(3) = 15387
			defaultItemID(4) = 13252
			defaultItemID(5) = 10695
			do until a >= 4
				for b = 1 to 5
					if crossSell(1) <> defaultItemID(b) and crossSell(2) <> defaultItemID(b) and crossSell(3) <> defaultItemID(b) then
						crossSell(a) = defaultItemID(b)
						exit for
					end if
				next
				a = a + 1
			loop
			CrossSells = crossSell(1) & "," & crossSell(2) & "," & crossSell(3)
			CrossSells = replace(CrossSells,",,",",")
			RS.close
			set RS = nothing
			RS2.close
			set RS2 = nothing
		else
			CrossSells = ""
		end if
	end if
end function
%>
