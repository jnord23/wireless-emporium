<%

call RenderRatingPanel3( dblAvgRating, intRatingCount) 

function RenderRatingPanel3( byval dblAvgRating, byval intRatingCount)
%>
<div style="float:left; padding-left:10px;">
    <div style="float:left; width:120px; text-align:left; font-weight:bold; font-size:13px;">Customer Rating:</div>
    <div style="float:left;">
        <% 
        dim strStarType: strStarType = EMPTY_STRING
        for intIdx = 1 to 5
            if dblAvgRating => intIdx then
                strStarType = "full"
            elseif dblAvgRating => ((intIdx - 1) + .5) then
                strStarType = "half"
            else
                strStarType = "empty"
            end if
        %>
        <div style="float:left;"><a id="anchor" href="#ReviewPanel" style="cursor:pointer;" onclick="OnClickShowAll()"><img src="/images/product/star_<%=strStarType%>.jpg" width="17" height="17" border="0" /></a></div>
        <%
        next
        %>
    </div>
    <div style="float:left; width:150px; text-align:left; font-weight:bold; font-size:12px; padding-left:20px;">(<%=strRatingCount%> Customer Reviews)</div>
    <div style="float:left;"><a href="/user-reviews-b?itemID=<%=itemid%>&partNumber=<%=partnumber%>" class="ProductDetailReadWriteReviewLink" style="text-decoration:none;" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'"><span class="ProductDetailReadWriteReviewLink">Write A Review</span></a></div>
</div>
<%
end function
%>