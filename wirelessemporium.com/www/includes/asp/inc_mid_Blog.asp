<!--#include virtual="/includes/asp/inc_SQLhtml.asp"-->
<tr>
	<td align="center" valign="top" width="800"><img src="/images/home_blog_header.jpg" width="800" height="30" border="0" alt="Recent Blog Entries"></td>
</tr>
<%
call fOpenConn()
SQL = "SELECT TOP 3 A.*, B.nickname, C.b_categoryName" & vbcrlf
SQL = SQL & " FROM b_articles A" & vbcrlf
SQL = SQL & " LEFT JOIN b_user B ON A.b_userid = B.userid" & vbcrlf
SQL = SQL & " LEFT JOIN b_category C ON A.b_categoryid = C.b_categoryid" & vbcrlf
SQL = SQL & " WHERE A.store = 0" & vbcrlf
SQL = SQL & " AND A.b_parentID = 0 AND A.b_categoryid <> 2" & vbcrlf
SQL = SQL & " AND (A.b_articlestatus = 1 OR A.b_articlestatus = 3" & vbcrlf
SQL = SQL & ") ORDER BY A.b_firstPostTime DESC" & vbcrlf

'response.write "<pre>" & SQL & "</pre>"

set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
do until RS.eof
	dim b_firstPostTime, PostMonthName, PostDay, PostYear, b_articleid, b_articleTitle, b_articleContent, nextSpace
	b_firstPostTime = dateValue(RS("b_firstPostTime"))
	PostMonthName = MonthName(month(b_firstPostTime))
	PostDay = day(b_firstPostTime)
	PostYear = year(b_firstPostTime)
	b_articleid = RS("b_articleid")
	b_articleTitle = RS("b_articleTitle")
	b_articleContent = SQLhtml(RS("b_articleContent"))
	if instr(b_articleContent,"<") > 0 then
		b_articleContent = left(b_articleContent,instr(b_articleContent,"<") - 1)
	else
		if len(b_articleContent) > 100 then
			nextSpace = inStr(right(b_articleContent,len(b_articleContent) - 100)," ")
			b_articleContent = left(b_articleContent,100 + nextSpace) & "..."
		end if
	end if
	%>
	<tr>
		<td width="100%" align="center" valign="top" colspan="3">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td width="100%" align="center" valign="top" colspan="3"><img src="/images/home_blog_top.jpg" width="800" height="13" border="0"></td>
				</tr>
				<tr>
					<td width="13" height="100%" align="left" valign="top" bgcolor="#EBEBEB"><img src="/images/spacer.gif" width="13" height="10" border="0"></td>
					<td width="774" align="center" valign="top" bgcolor="#EBEBEB">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td width="14" align="left" valign="top" rowspan="3"><img src="/images/spacer.gif" width="14" height="5" border="0"><img src="/images/blue_square.jpg" width="8" height="8" border="0"></td>
								<td width="760" align="left" valign="top" class="smlText"><a href="/blog/article-<%=b_articleid & "-" & replace(formatSEO(b_articleTitle),"?","")%>.html" class="blog-link"><%=b_articleTitle%></a>&nbsp;&nbsp;&nbsp;&nbsp;Posted on <%=PostMonthName & " " & PostDay & ", " & PostYear%></td>
							</tr>
							<tr><!--<td>bbb</td>-->
								<td align="left" valign="top" class="normalText"><%=b_articleContent%></td>
							</tr>
							<tr>
								<td align="left" valign="top"><a class="blog-full" href="/blog/article-<%=b_articleid & "-" & replace(formatSEO(b_articleTitle),"?","")%>.html">Read the full post &gt;&gt;</a></td>
							</tr>
						</table>
					</td>
					<td width="13" height="100%" align="left" valign="top" bgcolor="#EBEBEB"><img src="/images/spacer.gif" width="13" height="10" border="0"></td>
				</tr>
				<tr>
					<td width="100%" align="center" valign="top" colspan="3"><img src="/images/home_blog_bottom.jpg" width="800" height="13" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><img src="/images/spacer.gif" width="1" height="5" border="0"></td>
	</tr>
	<%
	RS.movenext
loop
RS.close
set RS = nothing
call fCloseConn()
%>
