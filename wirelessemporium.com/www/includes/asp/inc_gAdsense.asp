    <!-- Google AdSense Start -->
	<script type="text/javascript" charset="utf-8">
	var pageOptions = {
	'pubId' : 'pub-1001075386636876',
	<% if len(brandName) > 0 then %>
		<% if len(modelName) > 0 then %>
			<% if len(categoryName) > 0 then %>
	'query' : '<%=brandName%>' + ' ' + '<%=modelName%>' + ' ' + '<%=categoryName%>'
			<% else %>
	'query' : '<%=brandName%>' + ' ' + '<%=modelName%>'
			<% end if %>
		<% else %>
			<% if len(categoryName) > 0 then %>
	'query' : '<%=brandName%>' + ' ' + '<%=categoryName%>'
			<% else %>
	'query' : '<%=brandName%>'
			<% end if %>
		<% end if %>
	};
	<% elseif len(categoryName) > 0 then %>
	'query' : '<%=categoryName%>'
	};
	<% else %>
	'query' : 'Cell Phones'
	};
	<% end if %>
	var adblock1 = {
	'container' : 'adcontainer1',
	'number' : 3,
	'width' : 'auto',
	'lines' : 2,
	'fontFamily' : 'arial',
	'fontSizeTitle' : '14px',
	'fontSizeDescription' : '14px',
	'fontSizeDomainLink' : '14px',
	'linkTarget' : '_blank'
	};
	var adblock2 = {
	'container' : 'adcontainer2',
	'number' : 4,
	'width' : '250px',
	'lines' : 3,
	'fontFamily' : 'arial',
	'fontSizeTitle' : '12px',
	'fontSizeDescription' : '12px',
	'fontSizeDomainLink' : '12px',
	'linkTarget' : '_blank'
	};
	
	<%
	if isnull(googleAds) or len(googleAds) < 1 then googleAds = 0 end if
	if googleAds = 1 then
	%>
		new google.ads.search.Ads(pageOptions, adblock1);
	<% 
	elseif googleAds = 2 then 
	%>
		new google.ads.search.Ads(pageOptions, adblock1, adblock2);
	<% 
	end if 
	%>
	
	</script>
	<!-- Google AdSense Close -->



