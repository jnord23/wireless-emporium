<%
	on error resume next
	if request.querystring("id") <> "" then
		Response.Cookies("vendororder") = request.querystring("id")
		Response.Cookies("vendororder").Expires = date + 45
	elseif request.querystring("utm_source") <> "" then
		Response.Cookies("vendororder") = request.querystring("utm_source")
		Response.Cookies("vendororder").Expires = date + 45
	elseif request.querystring("refer") <> "" then
		Response.Cookies("vendororder") = request.querystring("refer")
		Response.Cookies("vendororder").Expires = date + 45
	elseif request.querystring("clickid") <> "" then
		Response.Cookies("vendororder") = "clickid=" & request.querystring("clickid")
		Response.Cookies("vendororder").Expires = date + 45
	elseif request.querystring("source") <> "" then
		Response.Cookies("vendororder") = request.querystring("source")
		Response.Cookies("vendororder").Expires = date + 45
	end if
	
	csid = request.QueryString("csid")
	if isnull(csid) or len(csid) < 1 or not isnumeric(csid) then csid = 0 else csid = cdbl(csid)
	
	if csid > 0 then
		response.Cookies("mySession") = csid
		response.cookies("mySession").expires = dateAdd("m", 1, now)
	elseif prepInt(request.Cookies("mySession")) = 0 then
		mySession = session.sessionid
		response.cookies("mySession") = mySession
		response.cookies("mySession").expires = dateAdd("m", 1, now)
	else
		mySession = prepInt(request.Cookies("mySession"))
		response.cookies("mySession") = mySession
		response.cookies("mySession").expires = dateAdd("m", 1, now)
	end if
	
	mySession = request.cookies("mySession")
	on error goto 0
	
	dim bSecureHttp : bSecureHttp = false
	dim useHttp : useHttp = "http"
	if Request.ServerVariables("SERVER_PORT_SECURE") = "1" then	
		bSecureHttp	= 	true 
		useHttp		=	"https"
	end if
	
	dim bStaging : bStaging = false
	if instr(request.ServerVariables("HTTP_HOST"),"staging") > 0 then bStaging = true end if

	' for master pages	
	if prepStr(request.QueryString("curPage")) <> "" then
		REWRITE_URL = prepStr(request.QueryString("curPage"))
	else
		REWRITE_URL = Request.ServerVariables("HTTP_X_REWRITE_URL")	
	end if

	if inStr(REWRITE_URL,"?") > 0 then REWRITE_URL = left(REWRITE_URL,inStr(REWRITE_URL,"?")-1) 

	dim canonicalURL : canonicalURL = useHttp & "://" & Request.ServerVariables("SERVER_NAME") & REWRITE_URL
	dim gblQueryString : gblQueryString = ""
	if instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"?") > 0 then
		gblQueryString = mid(request.ServerVariables("HTTP_X_REWRITE_URL"),instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"?") + 1)
	end if
		
	if not isnull(request.Cookies("adminID")) and len(request.Cookies("adminID")) > 0 then
		session("adminID") = request.Cookies("adminID")
		session("adminLvl") = request.Cookies("adminLvl")
		if isnull(request.Cookies("adminNav")) or len(request.Cookies("adminNav")) < 1 then
			session("adminNav") = false
		else
			session("adminNav") = request.Cookies("adminNav")
		end if
	end if
	
	if isnull(session("adminID")) or len(session("adminID")) < 1 then session("adminID") = 0 end if
	if isnull(session("adminLvl")) or len(session("adminLvl")) < 1 then session("adminLvl") = 0 end if
	
	userBrowser = request.ServerVariables("HTTP_USER_AGENT")
	mobileAccess = prepInt(mobileAccess)
	if mobileAccess = 0 then mobileAccess = prepInt(request.QueryString("mobileAccess"))
	if mobileAccess = 0 then mobileAccess = prepInt(session("mobileAccess"))
	if instr(request.ServerVariables("SERVER_NAME"),"m.wireless") > 0 or instr(request.ServerVariables("SERVER_NAME"),"mdev.wireless") > 0 then mobileAccess = 1
	if mobileAccess = 0 then
		'no mobile bypass session found
		if (instr(userBrowser,"Mobile") > 0 or instr(userBrowser,"BlackBerry") > 0 or instr(userBrowser,"Opera Mini") > 0) and instr(userBrowser,"iPad") < 1 then
			'when it is e-mail blast do not send them WE mobile site
'			if lcase(request.querystring("utm_medium")) = "email" then
			if false then
				session("mobileAccess") = 1
			else
				'mobile browser detected
				if mobileAccess = 0 then
					'no mobile bypass in QS - move to mobile site with brand/model/category info
					mobileSiteURL = replace(replace(request.ServerVariables("SERVER_NAME"), "staging.", "mdev."), "www.", "m.")
					if REWRITE_URL = "/" then
						if gblQueryString <> "" then
							call responseRedirect("http://" & mobileSiteURL & "?" & gblQueryString)
						else
							call responseRedirect("http://" & mobileSiteURL)
						end if
					else
						if gblQueryString <> "" then
							call responseRedirect("http://" & mobileSiteURL & REWRITE_URL & ".htm?" & gblQueryString)
						else
							call responseRedirect("http://" & mobileSiteURL & REWRITE_URL & ".htm")
						end if
					end if
				else
					'mobile bypass found in QS - save session
					session("mobileAccess") = 1
				end if			
			end if
		end if
	elseif mobileAccess = 2 then
		session("mobileAccess") = 2
	else
		session("mobileAccess") = 1
	end if
	
	dim gblCustomShippingDate : gblCustomShippingDate = ""
	set rsConfig = oConn.execute("sp_getConfigValueByID 0, 1")	'only custom case shipping for now. needs to be adjusted later for config values.
	if not rsConfig.eof then
		gblCustomShippingDate = rsConfig("configValue")
	end if
	
	dim RandNum
	function stripCode(myCode)
		stripCode = replace(replace(replace(replace(replace(myCode,"'",""),"<",""),">",""),"(",""),")","")
	end function	
	
	dim arrCssDateModified(1)
	arrCssDateModified(0) = getCssDateParam("/includes/css/base.css")
	arrCssDateModified(1) = getCssDateParam("/includes/css/index.css")	
	
	cms_basepage = right(request.ServerVariables("SCRIPT_NAME"), len(request.ServerVariables("SCRIPT_NAME"))-1)
%>	

