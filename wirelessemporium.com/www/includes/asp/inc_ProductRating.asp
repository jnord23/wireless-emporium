<%

call RenderRatingPanel( dblAvgRating, intRatingCount) 

function RenderRatingPanel( byval dblAvgRating, byval intRatingCount) 

%><div class="ProductDetailRatingPanel" style="width:100%; height: 52px; padding-top:3px;">
	<div class="DivCell DivCenterContent" style="width:142px; padding-left:15px">
		<div class="ProductDetailRatingLevelLabel">Customer Rating:</div>
		<div class="CleanUp">&nbsp;</div>
		<div style="width: 88px;" class="DivCenter"><% 
	dim strStarType: strStarType = EMPTY_STRING
    for intIdx = 1 to 5
        if dblAvgRating => intIdx then
            strStarType = "full"
        elseif dblAvgRating => ((intIdx - 1) + .5) then
            strStarType = "half"
        else
            strStarType = "empty"
        end if%>
		<div class="DivCell DivCenter"><a id="anchor" href="#ReviewPanel" style="cursor:pointer;" onclick="OnClickShowAll()"><img src="/images/product/star_<%=strStarType%>.jpg" width="17" height="17" border="0" /></a></div><%

    next%>
		</div>
		<div class="CleanUp">&nbsp;</div>
<%                                                            
            if dblAvgRating > 0 then %>
		<div class="ProductDetailRatingLevel"><%=GetRatingLevel( dblAvgRating)%></div><%
			end if %>
	</div>
	<div class="DivCell DivCenterContent" style="width: 189px; padding-top: 7px;">

		<div class="ProductDetailCustomerReviewCount">(<%=strRatingCount%> Customer Reviews)</div>
		<div style="width:189px;"><% if strRatingCount > 0 then %><a href="#ReviewPanel" style="text-decoration:none;" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" onclick="OnClickShowAll()"><span class="ProductDetailReadWriteReviewLink">Read Reviews</span></a> | <% end if %><a href="/user-reviews-b?itemID=<%=itemid%>&partNumber=<%=partnumber%>" class="ProductDetailReadWriteReviewLink" style="text-decoration:none;" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'"><span class="ProductDetailReadWriteReviewLink">Write A Review</span></a></div>
	</div>
</div><%

end function%>