<%
function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(replace(formatSEO,"---","-"),"--","-"),"?","-"),vbTab,"-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "faceplates-screen-protectors"
		case "bling-kits---charms" : formatSEO = "charms-bling-kits"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "headsets-hands-free-kits"
		case "bluetooth-headsets" : formatSEO = "bluetooth-headsets-headphones"
		case "holsters-belt-clips" : formatSEO = "holsters-holders"
		case "leather-cases" : formatSEO = "cases-pouches-skins"
	end select
end function
		
function nameSEO(val)
	select case val
		case "Antennas/Parts" : nameSEO = "Antennas and Parts"
		case "Hands-Free" : nameSEO = "Hands-Free and Headsets"
		case "Holsters/Belt Clips" : nameSEO = "Holsters/Holders"
		case "Key Pads/Flashing" : nameSEO = "Keypads and Flashing Keypads"
		case "Faceplates" : nameSEO = "Covers and Faceplates"
		case "Full Body Protectors" : nameSEO = "Body Guards/Skins"
		case "Data Cable & Memory" : nameSEO = "Data Cable & Memory Cards"
		case else : nameSEO = val
	end select
end function

function singularSEO(val)
	select case val
		case "Antennas/Parts" : singularSEO = "Antenna"
		case "Holsters/Belt Clips" : singularSEO = "Holster/Holder"
		case "Key Pads/Flashing" : singularSEO = "Keypad"
		case "Data Cable & Memory" : singularSEO = "Data Cable"
		case "Chargers" : singularSEO = "Charger"
		case "Faceplates" : singularSEO = "Faceplate/Screen Protector"
		case "Leather Cases" : singularSEO = "Case"
		case "Full Body Protectors" : singularSEO = "Body Guard/Skin"
		case else : singularSEO = val
	end select
end function

function brandSEO(val)
	val = cStr(val)
	select case val
		case "1" : brandSEO = "utstarcom-cell-phone-accessories.asp"
		case "2" : brandSEO = "sony-ericsson-cell-phone-accessories.asp"
		case "3" : brandSEO = "kyocera-cell-phone-accessories.asp"
		case "4" : brandSEO = "lg-cell-phone-accessories.asp"
		case "5" : brandSEO = "motorola-cell-phone-accessories.asp"
		case "6" : brandSEO = "nextel-cell-phone-accessories.asp"
		case "7" : brandSEO = "nokia-cell-phone-accessories.asp"
		case "8" : brandSEO = "panasonic-cell-phone-accessories.asp"
		case "9" : brandSEO = "samsung-cell-phone-accessories.asp"
		case "10" : brandSEO = "sanyo-cell-phone-accessories.asp"
		case "11" : brandSEO = "siemens-cell-phone-accessories.asp"
		case "12" : brandSEO = "universal-cell-phone-accessories.asp"
		case "13" : brandSEO = "nec-cell-phone-accessories.asp"
		case "14" : brandSEO = "blackberry-cell-phone-accessories.asp"
		case "15" : brandSEO = "other-cell-phone-accessories.asp"
		case "16" : brandSEO = "palm-cell-phone-accessories.asp"
		case "17" : brandSEO = "iphone-accessories.asp"
		case "18" : brandSEO = "pantech-cell-phone-accessories.asp"
		case "19" : brandSEO = "sidekick-cell-phone-accessories.asp"
		case "20" : brandSEO = "htc-cell-phone-accessories.asp"
		case else : brandSEO = "brand.asp?brandid=" & val
	end select
end function

function categorySEO(val)
	val = cStr(val)
	select case val
		case "1" : categorySEO = "cell-phone-batteries.asp"
		case "2" : categorySEO = "cell-phone-chargers.asp"
		case "3" : categorySEO = "cell-phone-covers-faceplates-screen-protectors.asp"
		case "5" : categorySEO = "bluetooth-headsets-handsfree.asp"
		case "6" : categorySEO = "cell-phone-holsters-belt-clips-holders.asp"
		case "7" : categorySEO = "cell-phone-cases.asp"
		case "12" : categorySEO = "cell-phone-antennas.asp"
		case "13" : categorySEO = "cell-phone-data-cables-memory-cards.asp"
		case "18" : categorySEO = "screen-protectors.asp"		
		case "19" : categorySEO = "decal-skin.asp"
		case "20" : categorySEO = "music-skins.asp"
		case else : categorySEO = "category.asp?categoryid=" & val
	end select
end function

function optCarrierName(val)
	select case lcase(val)
		case "at&t / cingular"
			optCarrierName = "AT&T"
		case "sprint / nextel"
			optCarrierName = "Sprint"
		case "verizon wireless"
			optCarrierName = "Verizon"
		case "boost mobile / southern linc"
			optCarrierName = "Boost Mobile"
		case "cricket wireless"
			optCarrierName = "Cricket"
		case "metropcs"
			optCarrierName = "Metro PCS"
		case else
			optCarrierName = val
	end select
end function

function optBrandName(val)
	select case lcase(val)
		case "apple"
			optBrandName = "iPhone"
		case else
			optBrandName = val
	end select
end function
%>
