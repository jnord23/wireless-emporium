<%
function getStates(myState)
	dim RSstate, stateSectionHold
	SQL = "SELECT * FROM WE_States WHERE stateSection <> 'Canada' ORDER BY stateSection, stateName"
	set RSstate = Server.CreateObject("ADODB.Recordset")
	RSstate.open SQL, oConn, 0, 1
	stateSectionHold = RSstate("stateSection")
	response.write "<option value="""">-- Please Select --</option>"
	response.write "<optgroup label=""" & RSstate("stateSection") & """>" & vbcrlf
	do until RSstate.eof
		if RSstate("stateSection") <> stateSectionHold then
			response.write "</optgroup>" & vbcrlf
			response.write "<optgroup label=""" & RSstate("stateSection") & """>" & vbcrlf
		end if
		stateSectionHold = RSstate("stateSection")
		response.write "<option value=""" & RSstate("StateAbbr") & """"
		if Ucase(trim(RSstate("StateAbbr"))) = Ucase(myState) or Ucase(trim(RSstate("StateName"))) = Ucase(myState) then response.write " selected"
		response.write ">" & RSstate("StateName") & "</option>" & vbCrLf
		RSstate.movenext
	loop
	response.write "</optgroup>" & vbcrlf
	SQL = "SELECT * FROM WE_States WHERE stateSection = 'Canada' ORDER BY stateName"
	set RSstate = Server.CreateObject("ADODB.Recordset")
	RSstate.open SQL, oConn, 0, 1
	response.write "<optgroup label=""Canada"">" & vbcrlf
	do until RSstate.eof
		response.write "<option value=""" & RSstate("StateAbbr") & """"
		if Ucase(trim(RSstate("StateAbbr"))) = Ucase(myState) or Ucase(trim(RSstate("StateName"))) = Ucase(myState) then response.write " selected"
		response.write ">" & RSstate("StateName") & "</option>" & vbCrLf
		RSstate.movenext
	loop
	response.write "</optgroup>" & vbcrlf
	set RSstate = nothing
end function
%>
