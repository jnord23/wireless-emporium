<%

call RenderReview( true, ItemId, PartNumber) 

function RenderReview( byval blnRenderFormTag, byval strItemId, byval strPartNumber) 

%>
<script type="text/javascript" src="/includes/js/jquery-1.2.6.pack.js" ></script>
<script type="text/javascript" src="/includes/js/Review.js" ></script>
<link href="/includes/css/Base.css" rel="stylesheet" type="text/css" >
<link href="/includes/css/Review.css" rel="stylesheet" type="text/css" ><%

'dim ItemId: ItemId="15940"
'dim PartNumber: PartNumber="TC1-MOT-00V9-01"

dim ReviewOrderDate: ReviewOrderDate = "Newest"
dim ReviewOrderRating: ReviewOrderRating = "Highest"

' construct url to write reviews
dim plQsWriteReview: set plQsWriteReview = NewQueryString( EMPTY_STRING)
call plQsWriteReview.Add( "ItemId", ItemId)
call plQsWriteReview.Add( "PartNumber", PartNumber)
dim strWriteReviewUrl: strWriteReviewUrl = "/user-reviews-b" & plQsWriteReview.AsQueryString
set plQsWriteReview = nothing


' ping db for avg rating and review count
dim objSqlExecReview: set objSqlExecReview = GetUserRatingStat( ItemId)
dim strHtmlRating: strHtmlRating = EMPTY_STRING
dim intRating: intRating = cint( cdbl( objSqlExecReview.Item( "AvgRating")) * 10)
dim blnShowBeTheFirst: blnShowBeTheFirst = ( cint( objSqlExecReview.Item( "RatingCount"))=0)
%>
<script type="text/javascript">
	var intEntryCount = <%=objSqlExecReview.Item( "RatingCount") %>
</script>
<%
' determine which UI liner to display - stats if ratings exist or link to write review if not
if blnShowBeTheFirst then
	strHtmlDivRatingOrBeTheFirst = "<div class=""DivCell ReviewRating""><a class=""ReviewBeTheFirst"" href="""& strWriteReviewUrl &""">"& replace( "Be the first to review this product!", " ", "&nbsp;") & "</a></div>"
else
	strHtmlDivRatingOrBeTheFirst = "<div class=""DivCell ReviewRating"">"& objSqlExecReview.Item( "AvgRating") &" out of 5 stars</div>"
end if

' construct rating visualization for web display
dim intStarCount: intStarCount = 5
while intRating>0 or intStarCount>0
	dim strImageType: strImageType = EMPTY_STRING
	if intRating >= 10 then
		strImageName = "full"
	elseif intRating >= 5  then
		strImageName = "half"
	else
		strImageName = "empty"
	end if

	strHtmlRating = strHtmlRating + "<div class=""DivCell""><img src=""/images/reviews/star-"& strImageName &".gif"" /></div>"

	intRating = intRating-10
	intStarCount = intStarCount - 1
wend

if blnRenderFormTag then response.Write "<form>"
 %>
<input type="hidden" id="hdnItemId" value="<%=ItemId %>" />
<input type="hidden" id="hdnEntryCount" value="3" />
<table border="0" cellpadding="0" cellspacing="0" width="825">
<tr>
	<td align="right"><img src="/images/reviews/topLeftCorner.jpg" style="border-width:0px;" /></td>
	<td class="ReviewPanelTop"><table border="0" cellpadding="0" cellspacing="0"><tr><td style="background-color: #ffffff;"><div style="padding-left: 7px; padding-right: 7px;"><img src="/images/reviews/reviewsTitle.jpg" style="border-width:0px;" /></div></td></tr></table></td>
	<td align="left"><img src="/images/reviews/topRightCorner.jpg" style="border-width:0px;" /></td>
</tr>
<tr>
	<td class="ReviewPanelLeft"></td>
	<td class="ReviewPanelCenter">

<div class="ReviewEntryHeaderContainer">
	<div class="DivCell" style="width: 310px;"><div class="ReviewAverageRating">Average User Rating:</div>
		<div class="CleanUp">&nbsp;</div>
		<div>
			<div class="DivCell"><%=strHtmlRating %></div>
			<%=strHtmlDivRatingOrBeTheFirst %>
		</div>
		<div class="CleanUp">&nbsp;</div>
		<div style="padding-top: 5px;">
			<div class="DivCell ReviewDividerRight"><a href="<%=strWriteReviewUrl %>" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'"><img src="/images/reviews/writeButton.png" alt="Write a review" border="0" /></a></div>
			<div class="DivCell ReviewShare" style="padding-left: 9px; padding-top: 2px;">Share this product: </div>
			<div class="DivCell" style="padding-left: 10px; padding-top: 2px;"><a href="http://www.facebook.com/wirelessemporium/" target="_blank" rel="me"><img src="/images/reviews/facebookIconNew.jpg" alt="Share with Facebook" border="0" /></a></div>
			<div class="DivCell" style="padding-left: 5px; padding-top: 2px;"><a href="http://twitter.com/wirelessemp/" target="_blank" rel="me"><img src="/images/reviews/twitterIconNew.jpg" alt="Share with Twitter" border="0" /></a></div>
		</div>
	</div>
	<div class="DivCell" style="padding-left: 0px;">
		<div class="DivCell SortReviewBy">Sort Reviews By:</div>
		<div class="CleanUp">&nbsp;</div>
		<div id="divReviewPanelDropdown">
			<div class="DivCell"><select <%=GetHtmlIdName( "ReviewOrderDate") %> class="ReviewOrderDate" onchange="OnChangeReviewUpdate();"><%=GetHtmlDropdownOptionSet( plReviewOrderDate, ReviewOrderDate) %></select></div>
			<div class="DivCell" style="padding-left: 10px;"><select <%=GetHtmlIdName( "ReviewOrderRating") %> class="ReviewOrderRating" onchange="OnChangeReviewUpdate();"><%=GetHtmlDropdownOptionSet( plReviewOrderRating, ReviewOrderRating) %></select></div>
		</div>
	</div>
</div>
<div class="CleanUp"></div>

<div id="divOutputReviewEntry" style="padding-top:10px;"></div>


	</td>
	<td class="ReviewPanelRight"></td>
</tr>
<tr>
	<td align="right"><img src="/images/reviews/boxBottomLeft.jpg" style="border-width:0px;" /></td>
	<td class="ReviewPanelBottom" align="center">
	<div id="divReviewBottomButtonPanel" style="width: 250px">
		<div class="DivCell" id="divButtonShowAll"><img src="/images/reviews/ShowAllReviewsButton.gif" style="border-width:0px;" id="imgShowAll" /></div>
		<div class="DivCell" id="divButtonDivider" style="padding-left: 10px; padding-right: 10px;"><img src="/images/reviews/ButtonDivider.gif" style="border-width:0px;" /></div>
		<div class="DivCell" id="divButtonHide"><img src="/images/reviews/HideReviewsButton.gif" style="border-width:0px;" id="imgHide" /></div>
	</div>
	</td>
	<td align="left"><img src="/images/reviews/boxBottomRight.jpg" style="border-width:0px;" /></td>
</tr>
</table><%
	if blnRenderFormTag then response.Write "</form>"

	set objSqlExecReview = nothing 

end function%>