<%

call RenderReview( true, ItemId, PartNumber, dblAvgRating, cint( strRatingCount))

function RenderReview( byval blnRenderFormTag, byval strItemId, byval strPartNumber, byval dblAvgRating, byval intRatingCount) 

%>
<link href="/includes/css/Review.css" rel="stylesheet" type="text/css" ><%

'dim ItemId: ItemId="15940"
'dim PartNumber: PartNumber="TC1-MOT-00V9-01"

dim ReviewOrderDate: ReviewOrderDate = "Newest"
dim ReviewOrderRating: ReviewOrderRating = "Highest"

' construct url to write reviews
dim plQsWriteReview: set plQsWriteReview = NewQueryString( EMPTY_STRING)
call plQsWriteReview.Add( "ItemId", ItemId)
call plQsWriteReview.Add( "PartNumber", PartNumber)
dim strWriteReviewUrl: strWriteReviewUrl = "/user-reviews-b" & plQsWriteReview.AsQueryString
set plQsWriteReview = nothing


dim strHtmlRating: strHtmlRating = EMPTY_STRING

' ping db for avg rating and review count
'dim objSqlExecReview: set objSqlExecReview = GetUserRatingStat( ItemId)
'dim intRating: intRating = cint( cdbl( objSqlExecReview.Item( "AvgRating")) * 10)
dim blnShowBeTheFirst: blnShowBeTheFirst = ( intRatingCount=0)
%>
<script type="text/javascript">
	var intEntryCount = <%=cstr( intRatingCount) %>
</script>
<%
' determine which UI liner to display - stats if ratings exist or link to write review if not
if blnShowBeTheFirst then
	strHtmlDivRatingOrBeTheFirst = "<div style=""float:left; width:300px;""><a class=""ReviewBeTheFirst"" style=""text-decoration:underline;"" href="""& strWriteReviewUrl &""">"& replace( "Be the first to review this product!", " ", "&nbsp;") & "</a></div>"
else
	strHtmlDivRatingOrBeTheFirst = 	"<div style=""float:left; width:300px; color:#333;"">" & vbcrlf & _
									"	<span itemprop=""review"" itemscope itemtype=""http://data-vocabulary.org/Review-aggregate"">" & vbcrlf & _
                                	"		<span itemprop=""rating"" style=""font-weight:bold; color:#111;"">"& cstr( round(dblAvgRating, 2)) &"</span> out of <span style=""font-weight:bold; color:#111;"">5</span> stars, based on <span itemprop=""count"" style=""font-weight:bold; color:#111;"">" & formatnumber(strRatingCount,0) & "</span> reviews" & vbcrlf & _
									"	</span>" & vbcrlf & _
									"</div>"
end if

' construct rating visualization for web display
dim intRating
if isnull(dblAvgRating) then dblAvgRating = 0.0
intRating = cint( dblAvgRating * 10)
dim intStarCount: intStarCount = 5
while intRating>0 or intStarCount>0
	dim strImageType: strImageType = EMPTY_STRING
	if intRating >= 10 then
		strImageName = "full"
	elseif intRating >= 5  then
		strImageName = "half"
	else
		strImageName = "empty"
	end if

	strHtmlRating = strHtmlRating + "<div class=""DivCell""><img src=""/images/reviews/star-"& strImageName &".gif"" /></div>"

	intRating = intRating-10
	intStarCount = intStarCount - 1
wend

if blnRenderFormTag then response.Write "<form>"
 %>
<input type="hidden" id="hdnItemId" value="<%=ItemId %>" />
<input type="hidden" id="hdnEntryCount" value="3" />
<div class="productReviewZone">
    <div style="width:748px; height:55px; float:left;">
        <div class="selected">PRODUCT REVIEWS</div>
        <div class="last"><!--#include virtual="/includes/asp/product/inc_rewards.asp"--></div>
    </div>
</div>
<div id="reviewMain" style="float:left; width:100%; margin-bottom:30px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td width="100%" style="padding:10px;">
            <div class="ReviewEntryHeaderContainer">
                <div id="reviewHeaderSection">
                    <div class="DivCell" style="width:100%;" align="left"><div class="ReviewAverageRating" style="font-size:18px;">AVERAGE USER RATING:</div>
                        <div class="CleanUp">&nbsp;</div>
                        <div style="padding-top:10px;">
                            <div style="float:left; width:150px;"><%=strHtmlRating2%></div>
                            <%=strHtmlDivRatingOrBeTheFirst%>
                        </div>
                    </div>
                    <div class="DivCell" style="width:50%; padding-top:5px; text-align:left;">
                        <img src="/images/500/WE-BUCKS-PDP-REVIEW.jpg" border="0" />
                    </div>
                    <div class="DivCell" style="width:50%; padding-top:5px; text-align:right;">
						<a href="<%=strWriteReviewUrl%>"><img src="/images/product/review-button.png" border="0" /></a>
                    </div>
                </div>
            </div>
        </td>
    </tr>        
    <% if strRatingCount > 0 then %>
    <tr>
        <td align="left" style="border-top:1px solid #ccc;">
            <div style="width:95%; padding-left:15px; padding-right:15px; ">
            <%
			sql = "select rating, nickname, reviewTitle, convert(varchar(20), datetimeentd, 107) reviewDate, review from we_Reviews where (itemID = " & itemID & " or PartNumbers like '%" & partNumber & "%') and approved = 1 order by datetimeentd desc"
			session("errorSQL") = sql
			Set rs = oConn.execute(sql)
			
			if not rs.eof then
				lap = 0
				do while not rs.EOF
					lap = lap + 1		
					addlStyle = "border-top:1px solid #ccc;"
					if lap = 1 then
						addlStyle = ""
					end if
		
					if lap > 3 then
						addlStyle = addlStyle & " display:none;"
					end if
					%>
					<div id="id_childReview_<%=lap%>" style="float:left; width:100%; padding:10px 0px 10px 0px; <%=addlStyle%>">
						<div style="float:left; width:100%;"><%=getRatingStar(rs("rating"))%></div>
						<div style="float:left; font-size:20px; width:510px; color:#333; padding-top:10px;"><%=rs("reviewTitle")%></div>
						<div style="float:left; color:#555; width:200px; padding-top:10px; text-align:right;"><%=rs("reviewDate")%></div>
						<div style="float:left; width:100%; padding-top:10px; font-size:14px; font-family:Arial, Helvetica, sans-serif;"><%=rs("review")%></div>
						<div style="float:left; width:100%; padding-top:10px; color:#666;"><span style="color:#333; font-weight:bold;">REVIEWED BY</span>&nbsp;&nbsp;<%=rs("nickname")%></div>
					</div>
					<%
					rs.movenext
				loop
			end if
            %>
            </div>
        </td>
    </tr>
    <tr>
        <td style="border-top:1px solid #ccc; padding-top:15px;" align="center">
            <% if strRatingCount > 3 then %>
				<a style="font-size:15px; font-weight:bold; color:#333; cursor:pointer;" id="showHideButton" onclick="showHideReviews()">Show More Reviews</a> &nbsp; 
                <img id="review_arrow" src="/images/product/review-arrow-down.png" border="0" />
            <% end if %>
        </td>
    </tr>
    <% end if %>        	
    </table>
</div>
<%
if blnRenderFormTag then response.Write "</form>"

end function

function getRatingStar(rating)
	dim nRating 
	dim strRatingImg
	nRating = cint(rating)
	strRatingImg = 	"<img src=""/images/product/review-star-2-empty.png"" border=""0"" width=""25"" height=""25"" />" & vbcrlf & _
					"<img src=""/images/product/review-star-2-empty.png"" border=""0"" width=""25"" height=""25"" />" & vbcrlf & _
					"<img src=""/images/product/review-star-2-empty.png"" border=""0"" width=""25"" height=""25"" />" & vbcrlf & _
					"<img src=""/images/product/review-star-2-empty.png"" border=""0"" width=""25"" height=""25"" />" & vbcrlf & _
					"<img src=""/images/product/review-star-2-empty.png"" border=""0"" width=""25"" height=""25"" />"
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""
		for i=1 to 5
			if i <= nRating then 
				strRatingImg = strRatingImg & "<img src=""/images/product/review-star-2-full.png"" border=""0"" width=""25"" height=""25"" style=""margin-right:2px;"" />"
			elseif ((i - 1) + .5) <= nRating then 
				strRatingImg = strRatingImg & "<img src=""/images/product/review-star-2-half.png"" border=""0"" width=""25"" height=""25"" style=""margin-right:2px;"" />"
			else
				strRatingImg = strRatingImg & "<img src=""/images/product/review-star-2-empty.png"" border=""0"" width=""25"" height=""25"" style=""margin-right:2px;"" />"
			end if	
		next	
	end if
	
	getRatingStar = strRatingImg
end function
%>
<% if strRatingCount > 0 then %>
<script language="javascript">
//	setTimeout("ajax('/ajax/showReviews-b?itemID=<%=itemID%>&partNumber=<%=partNumber%>','showReviews')",3000);
	function showHideReviews() {
		if (document.getElementById("showHideButton").innerHTML == "Show More Reviews") {
			strDisplay = "";
			document.getElementById("showHideButton").innerHTML = "Hide Reviews";
			document.getElementById('review_arrow').src = '/images/product/review-arrow-up.png';
		}
		else {
			strDisplay = "none";
			document.getElementById("showHideButton").innerHTML = "Show More Reviews";
			document.getElementById('review_arrow').src = '/images/product/review-arrow-down.png';
		}
		
		for (i=4; i<=<%=strRatingCount%>; i++){
			if (document.getElementById("id_childReview_"+i) != null){
				document.getElementById("id_childReview_"+i).style.display = strDisplay;
			}
		}
		
	}
</script>
<% end if %>