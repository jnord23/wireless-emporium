<%
ugcID = "|"

%>
<style>
	.qna-normalText		{font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left;}
	.qna-normalText2 	{font-size:12px; font-family:Arial, Helvetica, sans-serif; text-align:left;}
</style>
<div style="float:left; width:100%; margin-bottom:15px;">
    <table border="0" align="left" cellspacing="0" cellpadding="0" width="100%">
        <tr style="background:url(/images/product/grey_header_bar.jpg) repeat-x;">
            <td align="left"><img src="/images/product/QandA_header.jpg" border="0" /></td>
            <td align="right"><img src="/images/product/grey_header_right.jpg" border="0" width="12" height="30" /></td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="border-right:1px solid #ccc; border-left:1px solid #ccc;">
                <span class="TurnToItemTeaser"></span>
                <script type="text/javascript">
                    var TurnToItemSku = "<%=itemid%>";
                    document.write(unescape("%3Cscript src='" + document.location.protocol + "//static.www.turnto.com/traServer3/itemjs/" + turnToConfig.siteKey + "/" + TurnToItemSku + "' type='text/javascript'%3E%3C/script%3E"));
                </script>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left" style="border-right:1px solid #ccc; border-left:1px solid #ccc; border-bottom:1px solid #ccc;">
                <%
                if not isnull(arrUGC) then
                %>
                <div style="width:716px; height:400px; padding:10px 20px 0px 10px; overflow:auto;">
                <%
                    numQuestions = cint(0)
                    numAnswers = cint(0)
                    numAns = cint(0)
                    nQuestions = cint(getNumOfContent(3, 1, 0, arrUGC))
                    nComments = cint(getNumOfContent(3, 4, 0, arrUGC))
                    curQID = cint(-1)
                    if nQuestions > 0 then
                    %>
                    <div style="float:left; width:100%; padding:10px 0px 0px 0px; border-top:1px solid #ccc;" class="boldText" align="left">
                        <% if nQuestions > 1 then %>
                            <%=formatnumber(nQuestions,0)%> QUESTIONS FROM THE COMMUNITY        
                        <% else %>
                            <%=formatnumber(nQuestions,0)%> QUESTION FROM THE COMMUNITY
                        <% end if %>
                    </div>    
                    <%
                    end if
                    for i=0 to ubound(arrUGC, 2)
                        qid = cint(arrUGC(0,i))
                        qName = arrUGC(1,i)
                        qContent = arrUGC(2,i)
                        qContentType = arrUGC(3,i)
                        qPostDate = arrUGC(4,i)
                        aid = arrUGC(5,i)
                        aName = arrUGC(6,i)
                        aContent = arrUGC(7,i)
                        aContentType = arrUGC(8,i)
                        aPostDate = arrUGC(9,i)
                        rid = arrUGC(10,i)
                        rName = arrUGC(11,i)
                        rContent = arrUGC(12,i)
                        rContentType = arrUGC(13,i)
                        rPostDate = arrUGC(14,i)
                        
                        if qContentType	<> "4" then
                            if not isnull(qid) and instr(ugcID, "|" & qid & "|") <= 0 then
                                numQuestions = numQuestions + 1
                                ugcID = ugcID & qid & "|"
                            %>
                                <div style="float:left; width:100%; margin-top:10px;">
                                    <div style="float:left; width:566px; padding:15px 0px 5px 0px; font-weight:bold; border-top:1px solid #ccc;" class="qna-normalText">
                                        <div style="float:left; width:20px;" class="qna-normalText"><%=formatnumber(numQuestions,0)%>.)</div>
                                        <div style="float:left; width:536px; text-align:left; padding-left:10px;"><%=qContent%></div>
                                        <div style="float:left; width:536px; color:#666; padding:5px 0px 0px 30px; text-align:left;" class="smlText">Asked by <%=printUserName(qName)%>&nbsp;&nbsp;|&nbsp;&nbsp;<%=qPostDate%></div>
                                    </div>
                                    <div style="float:left; width:130px; padding:15px 0px 5px 0px; color:#3699D4; border-top:1px solid #ccc;" align="right" class="smlText">
                                        <%
                                        numAnswers = cint(formatnumber(getNumOfContent(0, qid, 5, arrUGC),0))
                                        if numAnswers > 1 then 
                                            response.write numAnswers & " Answers<br />"
                                            response.write "<a id=""" & qid & "_id_viewAns"" href=""javascript:void(0)"" onclick=""toggleAnswer(" & qid & "," & numAnswers & ");"" style=""color:#3699D4;"">View all answers</a>"
                                        else
                                            response.write numAnswers & " Answer"
                                        end if
                                        %>
                                    </div>
                                </div>
                            <%
                            end if
                
                            if not isnull(aid) and instr(ugcID, "|" & aid & "|") <= 0 then
                                if qid <> curQID then
                                    curQID = qid
                                    numAns = 0
                                end if
                                numAns = numAns + 1
                                ugcID = ugcID & aid & "|"
                                
                                curAnswerStyle = ""				
                                if numAns > 1 then
                                    curAnswerStyle = "display:none;"
                                %>
                                <div id="ans_<%=qid%>_<%=numAns%>" style="float:left; width:666px; padding:5px 5px 5px 45px; font-size:11px; <%=curAnswerStyle%>" class="qna-normalText2">
                                    <span style="color:#FF6600; font-weight:bold;" class="normalText">Answer</span>&nbsp;&nbsp;<%=aContent%>&nbsp;&nbsp;&nbsp;
                                </div>
                                <%
                                else
                                %>
                                <div style="float:left; width:666px; padding:5px 5px 5px 45px; font-size:11px;" class="qna-normalText2">
                                    <span style="color:#FF6600; font-weight:bold;" class="normalText">Answer</span>&nbsp;&nbsp;<%=aContent%>&nbsp;&nbsp;&nbsp;
                                </div>
                                <%
                                end if
                            end if
                        
                            if not isnull(rid) and instr(ugcID, "|" & rid & "|") <= 0 then
                                ugcID = ugcID & rid & "|"
                                if numAns > 1 then
                                %>
                                <div id="rpy_<%=qid%>_<%=numAns%>" style="float:left; width:646px; padding:5px 5px 5px 65px; font-size:11px; <%=curAnswerStyle%>" class="qna-normalText2">
                                    <span style="color:#3699D4; font-weight:bold;" class="normalText">Reply</span>&nbsp;&nbsp;<%=rContent%>&nbsp;&nbsp;&nbsp;
                                </div>
                                <%
                                else
                                %>
                                <div style="float:left; width:646px; padding:5px 5px 5px 65px; font-size:11px;" class="qna-normalText2">
                                    <span style="color:#3699D4; font-weight:bold;" class="normalText">Reply</span>&nbsp;&nbsp;<%=rContent%>&nbsp;&nbsp;&nbsp;
                                </div>
                                <%
                                end if
                            end if
                        end if
                    next
                
                    if nComments > 0 then
                        if nQuestions > 0 then
                        %>
                        <div style="float:left; width:100%; height:40px;"></div>
                        <%
                        end if
                    %>
                    <div style="float:left; width:100%; padding:10px 0px 10px 0px; border-top:1px solid #ccc; border-bottom:1px solid #ccc;" class="boldText" align="left">
                        <% if nComments > 1 then %>
                            <%=formatnumber(nComments,0)%> COMMENTS FROM THE COMMUNITY        
                        <% else %>
                            <%=formatnumber(nComments,0)%> COMMENT FROM THE COMMUNITY
                        <% end if %>
                    </div>    
                    <%
                    end if
                    for i=0 to ubound(arrUGC, 2)
                        qid = arrUGC(0,i)
                        qName = arrUGC(1,i)
                        qContent = arrUGC(2,i)
                        qContentType = arrUGC(3,i)
                        qPostDate = arrUGC(4,i)
                        aid = arrUGC(5,i)
                        aName = arrUGC(6,i)
                        aContent = arrUGC(7,i)
                        aContentType = arrUGC(8,i)
                        aPostDate = arrUGC(9,i)
                        rid = arrUGC(10,i)
                        rName = arrUGC(11,i)
                        rContent = arrUGC(12,i)
                        rContentType = arrUGC(13,i)
                        rPostDate = arrUGC(14,i)
                
                        if qContentType	= "4" then
                            if not isnull(qid) and instr(ugcID, "|" & qid & "|") <= 0 then
                                numComments = numComments + 1
                                ugcID = ugcID & qid & "|"
                            %>
                                <div style="float:left; width:100%; padding:15px 0px 5px 0px; font-weight:bold;" class="qna-normalText">
                                    <div style="float:left; width:20px;" class="qna-normalText"><%=formatnumber(numComments,0)%>.)</div>
                                    <div style="float:left; width:666px; text-align:left; padding-left:10px;"><%=qContent%></div>
                                    <div style="float:left; width:666px; color:#666; padding-left:30px;" class="smlText">Commented by <%=printUserName(qName)%>&nbsp;&nbsp;|&nbsp;&nbsp;<%=qPostDate%></div>
                                </div>
                            <%
                            end if
                        end if
                    next
                    %>
                </div>
                <%
                end if
                %>
            </td>
        </tr>
    </table>
</div>
<script>
	function toggleAnswer(questionID, numAnswers)
	{
		for (i=0; i<=numAnswers; i++)
		{
			if (document.getElementById("ans_" + questionID + "_" + i) != null) 
			{
				toggle("ans_" + questionID + "_" + i);
			}
		}
		if (document.getElementById(questionID + "_id_viewAns").innerHTML == "View all answers")
			document.getElementById(questionID + "_id_viewAns").innerHTML = "Hide answers";
		else
			document.getElementById(questionID + "_id_viewAns").innerHTML = "View all answers";
	}
</script>
<%
function getNumOfContent(searchIdx, searchVal, dupCheckIdx, byref arr)
	ret = 0
	strDupCheck = "|"
	if isarray(arr) then
		for k=0 to ubound(arr,2)
			if not isnull(searchVal) and not isnull(arr(dupCheckIdx,k)) then
				if cstr(searchVal) = cstr(arr(searchIdx,k)) and instr(strDupCheck, "|" & arr(dupCheckIdx,k) & "|") <= 0 then
					ret = ret + 1
				end if
				strDupCheck = strDupCheck & arr(dupCheckIdx,k) & "|"
			end if
		next
	end if
	getNumOfContent = ret
end function

function printUserName(userName)
	if trim(userName) = "" then
		printUserName = "Anonymous"
	else
		printUserName = userName
	end if
end function
%>