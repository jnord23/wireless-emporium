<!--#include virtual="/includes/asp/product/inc_CrossSells.asp"-->
<div style="float:left; width:100%; margin-bottom:15px;">
    <table border="0" align="left" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="left" valign="top" width="100%">
                <table border="0" align="left" cellspacing="0" cellpadding="0" width="100%">
                    <tr style="background:url(/images/product/grey_header_bar.jpg) repeat-x;">
                        <td align="left"><img src="/images/product/related_left.jpg" border="0" width="196" height="30" /></td>
                        <td align="right"><img src="/images/product/grey_header_right.jpg" border="0" width="12" height="30" /></td>
                    </tr>
                </table>
            </td>
        </tr>      
        <%
        dim strTest, itemIDArray, b
        itemIDArray = split(strItemCheck,",")
        for b = 0 to Ubound(itemIDArray)
        %>
        <tr>
            <td style="border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #CCC; border-bottom-left-radius:15px; border-bottom-right-radius:15px;" width="100%">
                <table border="0" align="center" cellspacing="0" cellpadding="0">
                    <tr>
                        <%
                        strCrossItems = ""
                        call getCrossSellItems(itemIDArray(b), strCrossItems)
                        if strCrossItems = "" then
                            strCrossItems = CrossSells(itemIDArray(b))
                        end if
                        
                        if right(strCrossItems,1) = "," then
                            strCrossItems = left(strCrossItems, len(strCrossItems)-1)
                        end if
                        
    '																				if typeID = 16 and noLeftNav = 1 then
    '																					topRows = 4
    '																				else
    '																					topRows = 3
    '																				end if
                        topRows = 3
                        
                        sql = 	"	select	top " & topRows & " *" & vbcrlf & _
                                "	from	(	" & vbcrlf & _
                                "			select	top 8 itemid, isnull(s.subtypeName, c.typename) typename, a.HandsfreeType, itemdesc, itempic, price_retail, price_our, 0 musicskins, itempic defaultPic, b.rowid" & vbcrlf & _
                                "			from 	we_items a with (nolock) join [dbo].[tfn_StringToColumn]('" & replace(strCrossItems, "'", "") & "', ',') b" & vbcrlf & _
                                "				on	a.itemid = b.rString join we_types c" & vbcrlf & _
                                "				on	a.typeid = c.typeid left outer join we_subtypes s" & vbcrlf & _
                                "				on	a.subtypeid = s.subtypeid" & vbcrlf & _
                                "			where	a.hidelive = 0 and a.inv_qty <> 0 and a.price_our > 0" & vbcrlf & _
                                "			union" & vbcrlf & _
                                "			select	top 8 id itemid, 'Music Skins' typename, 0 HandsFreeType, a.artist + ' ' + a.designName itemDesc, a.image itemPic" & vbcrlf & _
                                "				,	isnull(a.msrp, 0.0) price_retail, isnull(a.price_we, 0.0) price_our, 1 musicskins, a.defaultImg defaultPic, b.rowid" & vbcrlf & _
                                "			from 	we_items_musicskins a with (nolock) join [dbo].[tfn_StringToColumn]('" & replace(strCrossItems, "'", "") & "', ',') b" & vbcrlf & _
                                "				on	a.id = b.rString" & vbcrlf & _
                                "			where 	a.skip = 0 " & vbcrlf & _
                                "				and a.deleteItem = 0 " & vbcrlf & _
                                "				and (a.artist <> '' and a.artist is not null) " & vbcrlf & _
                                "				and (a.designname <> '' and a.designname is not null) " & vbcrlf & _
                                "				and a.deleteItem = 0 " & vbcrlf & _
                                "			) a" & vbcrlf & _
                                "	order by a.rowid" & vbcrlf	
                        session("errorSQL") = SQL
                        set RS = Server.CreateObject("ADODB.Recordset")
                        RS.open SQL, oConn, 0, 1
                        a = 0
                        set ofsCross = Server.CreateObject("Scripting.FileSystemObject")
                        dim singularTypeName
                        do until RS.eof
                            a = a + 1
                            if RS("HandsfreeType") = 2 then
                                singularTypeName = "Bluetooth"
                            else
                                singularTypeName = singularSEO(RS("typeName"))
                            end if
                            itempic = RS("itemPic")
                            defaultPic = RS("defaultPic")
                            musicSkins = RS("musicskins")
                            useImgPath = ""
                            useLink = ""
    
                            if musicSkins = 1 then
                                useLink = "/p-ms-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"
                                if ofsCross.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsSmall/" & itempic)) then
                                    useImgPath = "/productpics/musicSkins/musicSkinsSmall/" & itempic
                                else
                                    if ofsCross.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & defaultPic)) then
                                        useImgPath = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & defaultPic
                                    else
                                        useImgPath = "/productpics/thumb/imagena.jpg"
                                    end if					
                                end if
                            else
                                useLink = "/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"
                                if ofsCross.FileExists(server.mappath("/productpics/thumb/" & itemPic)) then
                                    useImgPath = "/productpics/thumb/" & itemPic
                                else
                                    useImgPath = "/productpics/thumb/imagena.jpg"
                                end if
                            end if
                            %>
                            <td align="center" valign="top" width="33%" height="100%">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
                                    <tr>
                                        <td align="center" valign="middle" rowspan="3" style="padding:5px;">
                                            <a href="<%=useLink%>"><img src="<%=useImgPath%>" width="100" height="100" align="center" border="0" alt="<%=RS("itemDesc")%>"></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" style="padding:2px;">
                                            <p class="related-1" style="margin-bottom:0px;"><%=singularTypeName%></p>
                                            <p style="line-height:14px;margin-top:4px;"><a href="<%=useLink%>" title="<%=RS("itemDesc")%>" class="cellphone2-link"><%=RS("itemDesc")%></a></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="bottom" class="boldText" style="padding:2px;">
                                            <div style="font-size:13px;">Our&nbsp;Price:&nbsp;<span class="pricing-orange2">$<%=formatNumber(RS("price_Our"),2)%>&nbsp;</span></div>
                                            <span class="pricing-gray2">List&nbsp;Price:&nbsp;<s>$<%=formatNumber(prepInt(RS("price_retail")),2)%></s>&nbsp;</span>
                                            <br>
                                            <span class="pricing-gray2">You&nbsp;Save:&nbsp;$<%=formatNumber(prepInt(RS("price_retail")) - RS("price_Our"),2)%>&nbsp;</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <%
                            RS.movenext
                            if a < topRows then
                                %>
                                <td align="center" valign="top" width="1" style="border-right:1px dotted #ccc;">&nbsp;</td>
                                <%
                            end if
                        loop
                        RS.close
                        set RS = nothing
                        %>
                    </tr>
                </table>
            </td>
        </tr>
        <%
        next
        %>
    </table>
</div>
