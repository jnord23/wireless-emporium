<%

call RenderRatingPanel2( dblAvgRating, intRatingCount) 

function RenderRatingPanel2( byval dblAvgRating, byval intRatingCount)
	dim strStarType: strStarType = EMPTY_STRING
%>
<div id="reviewWrapper" style="float:left; padding:5px 0px 0px 10px; width:240px;">
	<div style="float:left;">
        <div style="float:left; width:135px; text-align:left; font-weight:bold; font-size:13px;">Customer Rating:</div>
        <div style="float:left;">
            <% 
            for intIdx = 1 to 5
                if dblAvgRating => intIdx then
                    strStarType = "full"
                elseif dblAvgRating => ((intIdx - 1) + .5) then
                    strStarType = "half"
                else
                    strStarType = "empty"
                end if
            %>
            <div style="float:left;"><a id="anchor" href="#ReviewPanel" style="cursor:pointer;" onclick="OnClickShowAll()"><img src="/images/product/star_<%=strStarType%>.jpg" width="17" height="17" border="0" /></a></div>
            <%
            next
            %>
        </div>
    </div>
    <div id="reviewInnerNew" style="padding-left:10px; float:left; display:none;">
	    <div style="float:left; text-align:left; font-weight:bold; font-size:12px;"><a href="/user-reviews-b?itemID=<%=itemid%>&partNumber=<%=partnumber%>" style="text-decoration:underline;">(<%=strRatingCount%> Customer Reviews)</a></div>
    </div>    
    <div id="reviewInner" style="float:left;">
	    <div style="float:left; text-align:left; font-weight:bold; font-size:12px;">(<%=strRatingCount%> Customer Reviews)</div>
    	<div style="float:left; padding:1px 0px 0px 5px;"><a href="/user-reviews-b?itemID=<%=itemid%>&partNumber=<%=partnumber%>" class="ProductDetailReadWriteReviewLink" style="text-decoration:none;" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'"><span class="ProductDetailReadWriteReviewLink">Write A Review</span></a></div>
    </div>
</div>
<%
end function
%>