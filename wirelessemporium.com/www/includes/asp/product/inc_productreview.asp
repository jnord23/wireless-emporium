<%

call RenderReview( true, ItemId, PartNumber, dblAvgRating, cint( strRatingCount))

function RenderReview( byval blnRenderFormTag, byval strItemId, byval strPartNumber, byval dblAvgRating, byval intRatingCount) 

%>
<link href="/includes/css/Review.css" rel="stylesheet" type="text/css" ><%

'dim ItemId: ItemId="15940"
'dim PartNumber: PartNumber="TC1-MOT-00V9-01"

dim ReviewOrderDate: ReviewOrderDate = "Newest"
dim ReviewOrderRating: ReviewOrderRating = "Highest"

' construct url to write reviews
dim plQsWriteReview: set plQsWriteReview = NewQueryString( EMPTY_STRING)
call plQsWriteReview.Add( "ItemId", ItemId)
call plQsWriteReview.Add( "PartNumber", PartNumber)
dim strWriteReviewUrl: strWriteReviewUrl = "/user-reviews-b" & plQsWriteReview.AsQueryString
set plQsWriteReview = nothing


dim strHtmlRating: strHtmlRating = EMPTY_STRING

' ping db for avg rating and review count
'dim objSqlExecReview: set objSqlExecReview = GetUserRatingStat( ItemId)
'dim intRating: intRating = cint( cdbl( objSqlExecReview.Item( "AvgRating")) * 10)
dim blnShowBeTheFirst: blnShowBeTheFirst = ( intRatingCount=0)
%>
<script type="text/javascript">
	var intEntryCount = <%=cstr( intRatingCount) %>
</script>
<%
' determine which UI liner to display - stats if ratings exist or link to write review if not
if blnShowBeTheFirst then
	strHtmlDivRatingOrBeTheFirst = "<div class=""DivCell ReviewRating""><a class=""ReviewBeTheFirst"" href="""& strWriteReviewUrl &""">"& replace( "Be the first to review this product!", " ", "&nbsp;") & "</a></div>"
else
	strHtmlDivRatingOrBeTheFirst = 	"<div class=""DivCell ReviewRating"">" & vbcrlf & _
									"	<span itemprop=""review"" itemscope itemtype=""http://data-vocabulary.org/Review-aggregate"">" & vbcrlf & _
                                	"		<span itemprop=""rating"">"& cstr( round(dblAvgRating, 2)) &"</span> out of 5 stars, based on <span itemprop=""count"">" & formatnumber(strRatingCount,0) & "</span> reviews" & vbcrlf & _
									"	</span>" & vbcrlf & _
									"</div>"
end if

' construct rating visualization for web display
dim intRating
if isnull(dblAvgRating) then dblAvgRating = 0.0
intRating = cint( dblAvgRating * 10)
dim intStarCount: intStarCount = 5
while intRating>0 or intStarCount>0
	dim strImageType: strImageType = EMPTY_STRING
	if intRating >= 10 then
		strImageName = "full"
	elseif intRating >= 5  then
		strImageName = "half"
	else
		strImageName = "empty"
	end if

	strHtmlRating = strHtmlRating + "<div class=""DivCell""><img src=""/images/reviews/star-"& strImageName &".gif"" /></div>"

	intRating = intRating-10
	intStarCount = intStarCount - 1
wend

if blnRenderFormTag then response.Write "<form>"
 %>
<input type="hidden" id="hdnItemId" value="<%=ItemId %>" />
<input type="hidden" id="hdnEntryCount" value="3" />
<div style="float:left; width:100%; margin-bottom:15px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td width="100%">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
                <tr style="background:url(/images/product/grey_header_bar.jpg) repeat-x;">
                    <td align="left"><img name="ReviewPanel" src="/images/product/rating_review_left.jpg" border="0" width="202" height="30" /></td>
                    <td align="right"><img src="/images/product/grey_header_right.jpg" border="0" width="12" height="30" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="100%" style="border-left:1px solid #ccc; border-right:1px solid #ccc; padding:10px;">
            <div class="ReviewEntryHeaderContainer">
                <div id="reviewHeaderSectionNew" style="display:none;">
                    <div style="float:left; width:100%;">
                        <div style="float:left; width:600px; font-size:20px; color:#666; font-weight:bold; font-family:Verdana, Geneva, sans-serif; text-align:left;">Overall Rating</div>
                        <div style="float:left; width:300px;"><img src="/images/500/WE-BUCKS-PDP-REVIEW.jpg" border="0" /></div>
                    </div>
                    <div style="float:left; width:100%;">
                        <div style="float:left; width:230px; border-right:1px dotted #ccc;">
                            <%
                            for i = 1 to 5
                                starCnt = 6 - i
                                if starCnt = 5 then total = stars5
                                if starCnt = 4 then total = stars4
                                if starCnt = 3 then total = stars3
                                if starCnt = 2 then total = stars2
                                if starCnt = 1 then total = stars1
                                
                                if strRatingCount = 0 then
                                    barPercent = 0
                                else
                                    barPercent = total / strRatingCount * 100
                                end if
                            %>
                            <div style="width:220px; height:20px;">
                                <div style="float:left; color:#666; width:40px;"><%=starCnt%> Star<% if starCnt > 1 then %>s<% end if %></div>
                                <div style="border:1px solid #ccc; width:120px; height:15px; margin-left:15px; float:left; position:relative;">
                                    <% if barPercent > 0 then %>
                                    <div style="width:<%=barPercent%>%; background-color:#002144; height:15px;"></div>
                                    <% end if %>
                                </div>
                                <div style="margin-left:5px; font-size:12px; width:10px; float:left;">(<%=total%>)</div>
                            </div>
                            <%
                            next
                            %>
                        </div>
                        <div style="margin-left:15px; float:left; width:340px;">
                            <div class="DivCell" style="width:100%;" align="left">
                                <div style="font-size:14px; font-weight:bold; color:#333;">Average Customer Review</div>                    
                                <div>
                                    <div class="DivCell"><%=strHtmlRating %></div>
                                    <%=strHtmlDivRatingOrBeTheFirst %>
                                </div>
                                <div class="CleanUp">&nbsp;</div>
                                <div style="padding-top: 5px;">
                                    <div class="DivCell ReviewDividerRight"><a href="<%=strWriteReviewUrl %>" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'"><img src="/images/reviews/writeButton.png" alt="Write a review" border="0" /></a></div>
                                    <div class="DivCell ReviewShare" style="padding-left: 9px; padding-top: 2px;">Share this product: </div>
                                    <div class="DivCell" style="padding-left: 10px; padding-top: 2px;"><a href="http://www.facebook.com/wirelessemporium/" target="_blank" rel="me"><img src="/images/reviews/facebookIconNew.jpg" alt="Share with Facebook" border="0" /></a></div>
                                    <div class="DivCell" style="padding-left: 5px; padding-top: 2px;"><a href="http://twitter.com/wirelessemp/" target="_blank" rel="me"><img src="/images/reviews/twitterIconNew.jpg" alt="Share with Twitter" border="0" /></a></div>
                                </div>
                            </div>
                        </div>
                    </div>            
                </div>
                <div id="reviewHeaderSection">
                    <div class="DivCell" style="width: 340px;" align="left"><div class="ReviewAverageRating">Average User Rating:</div>
                        <div class="CleanUp">&nbsp;</div>
                        <div>
                            <div class="DivCell"><%=strHtmlRating %></div>
                            <%=strHtmlDivRatingOrBeTheFirst %>
                        </div>
                        <div class="CleanUp">&nbsp;</div>
                        <div style="padding-top: 5px;">
                            <div class="DivCell ReviewDividerRight"><a href="<%=strWriteReviewUrl %>" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'"><img src="/images/reviews/writeButton.png" alt="Write a review" border="0" /></a></div>
                            <div class="DivCell ReviewShare" style="padding-left: 9px; padding-top: 2px;">Share this product: </div>
                            <div class="DivCell" style="padding-left: 10px; padding-top: 2px;"><a href="http://www.facebook.com/wirelessemporium/" target="_blank" rel="me"><img src="/images/reviews/facebookIconNew.jpg" alt="Share with Facebook" border="0" /></a></div>
                            <div class="DivCell" style="padding-left: 5px; padding-top: 2px;"><a href="http://twitter.com/wirelessemp/" target="_blank" rel="me"><img src="/images/reviews/twitterIconNew.jpg" alt="Share with Twitter" border="0" /></a></div>
                        </div>
                    </div>
                    <div class="DivCell" style="width:340px; padding-left: 0px;">
                        <div class="DivCell SortReviewBy">Sort Reviews By:</div>
                        <div class="CleanUp">&nbsp;</div>
                        <div id="divReviewPanelDropdown">
                            <div class="DivCell"><select <%=GetHtmlIdName( "ReviewOrderDate") %> class="ReviewOrderDate" onchange="OnChangeReviewUpdate();"><%=GetHtmlDropdownOptionSet( plReviewOrderDate, ReviewOrderDate) %></select></div>
                            <div class="DivCell" style="padding-left: 10px;"><select <%=GetHtmlIdName( "ReviewOrderRating") %> class="ReviewOrderRating" onchange="OnChangeReviewUpdate();"><%=GetHtmlDropdownOptionSet( plReviewOrderRating, ReviewOrderRating) %></select></div>
                        </div>
                    </div>
                    <div class="DivCell" style="width:100%; padding-top:5px; text-align:left;">
                        <img src="/images/500/WE-BUCKS-PDP-REVIEW.jpg" border="0" />
                    </div>
                </div>
            </div>
        </td>
    </tr>        
    <% if strRatingCount > 0 then %>
    <tr>
        <td style="border:1px solid #CCC;" align="left">
            <div id="showReviews"></div>
        </td>
    </tr>
    <tr>
        <td style="border-left:1px solid #ccc; border-bottom:1px solid #ccc; border-right:1px solid #ccc;" align="center">
            <% if strRatingCount > 3 then %>
            <a style="font-size:15px; font-weight:bold; color:#FF3E01; cursor:pointer;" id="showHideButton" onclick="showHideReviews()">Click to Show More Reviews</a>
            <% end if %>
        </td>
    </tr>
    <% else %>
    <tr><td style="border-bottom:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;">&nbsp;</td></tr>
    <% end if %>        	
    </table>
</div>
<%
if blnRenderFormTag then response.Write "</form>"

end function
%>
<% if strRatingCount > 0 then %>
<script language="javascript">
	setTimeout("ajax('/ajax/showReviews?itemID=<%=itemID%>&partNumber=<%=partNumber%>','showReviews')",3000);
	function showHideReviews() {
		if (document.getElementById("showHideButton").innerHTML == "Click to Show More Reviews") {
			strDisplay = "";
			document.getElementById("showHideButton").innerHTML = "Click to Hide Reviews";
		}
		else {
			strDisplay = "none";
			document.getElementById("showHideButton").innerHTML = "Click to Show More Reviews";
		}
		
		for (i=4; i<=<%=strRatingCount%>; i++){
			if (document.getElementById("id_childReview_"+i) != null){
				document.getElementById("id_childReview_"+i).style.display = strDisplay;
			}
		}
		
	}
</script>
<% end if %>