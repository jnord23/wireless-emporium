<%
dim sharingURL : sharingURL = canonicalURL


if instr(sharingURL, "?") > 0  then
    sharingURL = sharingURL & "&"
else
    sharingURL = sharingURL & "?"
end if
pinSharingURL = sharingURL & "dzid=pin_"& Request.Cookies("user")("id")
twitterSharingURL = sharingURL & "dzid=twitter_"& Request.Cookies("user")("id")
emailSharingURL = sharingURL & "dzid=email_"& Request.Cookies("user")("id")
gplusSharingURL = sharingURL & "dzid=gplus_"& Request.Cookies("user")("id")
%>

<div style="float:right;padding-top:4px;" class="share-gplus">
    <g:plusone size="medium" annotation="none" href="<%= gplusSharingURL %>"></g:plusone>
</div>
<div style="float:right; padding:4px 10px 0px 0px;" class="share-pinterest">
    <!-- Pin it -->
    <script type="text/javascript">
        (function() {
            window.PinIt = window.PinIt || { loaded:false };
            if (window.PinIt.loaded) return;
            window.PinIt.loaded = true;
            function async_load(){
                var s = document.createElement("script");
                s.type = "text/javascript";
                s.async = true;
                if (window.location.protocol == "https:")
                    s.src = "https://assets.pinterest.com/js/pinit.js";
                else
                    s.src = "http://assets.pinterest.com/js/pinit.js";
                var x = document.getElementsByTagName("script")[0];
                x.parentNode.insertBefore(s, x);
            }
            if (window.attachEvent)
                window.attachEvent("onload", async_load);
            else
                window.addEventListener("load", async_load, false);
        })();
    </script>
    <a target="_blank" href="http://pinterest.com/pin/create/button/?url=<%=Server.URLEncode(pinSharingURl)%>&media=http://www.wirelessemporium.com<%=itemImgFullPath%>" class="pin-it-button" count-layout="none">
        <img src="/images/product/pinit_pre.jpg" border="0" width="42" height="20" alt="Pinterest" />
    </a>
</div>
<div style="float:right; padding:4px 10px 0px 0px;">
    <div style="position:relative;" class="share-email" onmouseover="document.getElementById('id_500_email2').style.display=''" onmouseout="document.getElementById('id_500_email2').style.display='none'">
        <div id="id_500_email2" style="display:none; position:absolute; bottom:20px; right:-100px;" onmouseover="document.getElementById('id_500_email2').style.display=''" onmouseout="document.getElementById('id_500_email2').style.display='none'">
            <a href="#" onclick="show500Help(); return false;"><img src="/images/500/WE-BUCKS-PDP-HOVER-EMAIL.png" border="0" /></a>
        </div>
        <script type="text/javascript">
            try {
                var fullTitle = document.title;
                var ffImageUrl = "http://www.wirelessemporium.com<%=itemImgFullPath%>";
                var ffProductName = "<%=insertDetails(itemDesc)%>";
                var ffProducturl = "<%=emailSharingURL%>";
                var ffMessage = "Check out this product " + ffProductName + " on Wireless Emporium";
            }
            catch (e) {}
        </script>
        <script type="text/javascript">
            _ffLoyalty.displayWidget("buBY2zrrDT", {
                message: ffMessage,
                url: ffProducturl,
                image_url: ffImageUrl,
                title: fullTitle,
                description: ffProductName
        });
        </script>
    </div>
</div>
<div style="float:right; padding:4px 10px 0px 0px;">
    <div style="position:relative;" class="share-twitter" onmouseover="document.getElementById('id_500_twitter2').style.display=''" onmouseout="document.getElementById('id_500_twitter2').style.display='none'">
        <div id="id_500_twitter2" style="display:none; position:absolute; bottom:20px; right:-100px;" onmouseover="document.getElementById('id_500_twitter2').style.display=''" onmouseout="document.getElementById('id_500_twitter2').style.display='none'">
            <a href="#" onclick="show500Help(); return false;"><img src="/images/500/WE-BUCKS-PDP-HOVER-TWITTER.png" border="0" /></a>
        </div>                                                                
        <script type="text/javascript">
            try {
                var tProductName = "<%=insertDetails(itemDesc)%>";
                if (tProductName.length >= 70) tProductName = tProductName.substring(0,67) + '...'
                var tMessage = "Check out this product [" + tProductName + "] on Wireless Emporium";
                var tProducturl = "<%=twitterSharingURL%>";
            }
            catch (e) {}
        </script>
        <script type="text/javascript">
            _ffLoyalty.displayWidget("TuBYAs1fyg", {
                message: tMessage, url: tProducturl
            });
        </script>
    </div>
</div>
<div style="float:right; padding:0px 10px 0px 0px;"><a href="/member_program"><img src="/images/icons/wePoints.gif" border="0" width="131" height="40" /></a></div>