<%

call RenderReview( true, ItemId, PartNumber, dblAvgRating, cint( strRatingCount))

function RenderReview( byval blnRenderFormTag, byval strItemId, byval strPartNumber, byval dblAvgRating, byval intRatingCount) 

%>
<link href="/includes/css/Review.css" rel="stylesheet" type="text/css" ><%

'dim ItemId: ItemId="15940"
'dim PartNumber: PartNumber="TC1-MOT-00V9-01"

dim ReviewOrderDate: ReviewOrderDate = "Newest"
dim ReviewOrderRating: ReviewOrderRating = "Highest"

' construct url to write reviews
dim plQsWriteReview: set plQsWriteReview = NewQueryString( EMPTY_STRING)
call plQsWriteReview.Add( "ItemId", ItemId)
call plQsWriteReview.Add( "PartNumber", PartNumber)
dim strWriteReviewUrl: strWriteReviewUrl = "/user-reviews-b.asp" & plQsWriteReview.AsQueryString
set plQsWriteReview = nothing


dim strHtmlRating: strHtmlRating = EMPTY_STRING

' ping db for avg rating and review count
'dim objSqlExecReview: set objSqlExecReview = GetUserRatingStat( ItemId)
'dim intRating: intRating = cint( cdbl( objSqlExecReview.Item( "AvgRating")) * 10)
dim blnShowBeTheFirst: blnShowBeTheFirst = ( intRatingCount=0)
%>
<script type="text/javascript">
	var intEntryCount = <%=cstr( intRatingCount) %>
</script>
<%
' determine which UI liner to display - stats if ratings exist or link to write review if not
if blnShowBeTheFirst then
	strHtmlDivRatingOrBeTheFirst = "<div class=""DivCell ReviewRating""><a class=""ReviewBeTheFirst"" href="""& strWriteReviewUrl &""">"& replace( "Be the first to review this product!", " ", "&nbsp;") & "</a></div>"
else
	strHtmlDivRatingOrBeTheFirst = 	"<div class=""DivCell ReviewRating"">" & vbcrlf & _
									"	<span itemprop=""review"" itemscope itemtype=""http://data-vocabulary.org/Review-aggregate"">" & vbcrlf & _
                                	"		<span itemprop=""rating"">"& cstr( round(dblAvgRating, 2)) &"</span> out of 5 stars, based on <span itemprop=""count"">" & formatnumber(strRatingCount,0) & "</span> reviews" & vbcrlf & _
									"	</span>" & vbcrlf & _
									"</div>"
end if

' construct rating visualization for web display
dim intRating
if isnull(dblAvgRating) then dblAvgRating = 0.0
intRating = cint( dblAvgRating * 10)
dim intStarCount: intStarCount = 5
while intRating>0 or intStarCount>0
	dim strImageType: strImageType = EMPTY_STRING
	if intRating >= 10 then
		strImageName = "full"
	elseif intRating >= 5  then
		strImageName = "half"
	else
		strImageName = "empty"
	end if

	strHtmlRating = strHtmlRating + "<div class=""DivCell""><img src=""/images/reviews/star-"& strImageName &".gif"" /></div>"

	intRating = intRating-10
	intStarCount = intStarCount - 1
wend

if blnRenderFormTag then response.Write "<form>"
 %>
<input type="hidden" id="hdnItemId" value="<%=ItemId %>" />
<input type="hidden" id="hdnEntryCount" value="3" />
<div style="float:left; width:100%; margin-bottom:15px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td width="100%" style="padding:10px;">
            <div class="ReviewEntryHeaderContainer">
                <div id="reviewHeaderSection">
                    <div class="DivCell" style="width:50%;" align="left"><div class="ReviewAverageRating">Average User Rating:</div>
                        <div class="CleanUp">&nbsp;</div>
                        <div>
                            <div class="DivCell"><%=strHtmlRating %></div>
                            <%=strHtmlDivRatingOrBeTheFirst %>
                        </div>
                        <div class="CleanUp">&nbsp;</div>
                        <div style="padding-top: 5px;">
                            <div class="DivCell ReviewDividerRight"><a href="<%=strWriteReviewUrl %>" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'"><img src="/images/reviews/writeButton.png" alt="Write a review" border="0" /></a></div>
                            <div class="DivCell ReviewShare" style="padding-left: 9px; padding-top: 2px;">Share this product: </div>
                            <div class="DivCell" style="padding-left: 10px; padding-top: 2px;"><a href="http://www.facebook.com/wirelessemporium/" target="_blank" rel="me"><img src="/images/reviews/facebookIconNew.jpg" alt="Share with Facebook" border="0" /></a></div>
                            <div class="DivCell" style="padding-left: 5px; padding-top: 2px;"><a href="http://twitter.com/wirelessemp/" target="_blank" rel="me"><img src="/images/reviews/twitterIconNew.jpg" alt="Share with Twitter" border="0" /></a></div>
                        </div>
                    </div>
                    <div class="DivCell" style="width:50%;">
                        <div class="DivCell SortReviewBy">Sort Reviews By:</div>
                        <div class="CleanUp">&nbsp;</div>
                        <div id="divReviewPanelDropdown">
                            <div class="DivCell"><select <%=GetHtmlIdName( "ReviewOrderDate") %> class="ReviewOrderDate" onchange="OnChangeReviewUpdate();"><%=GetHtmlDropdownOptionSet( plReviewOrderDate, ReviewOrderDate) %></select></div>
                            <div class="DivCell" style="padding-left: 10px;"><select <%=GetHtmlIdName( "ReviewOrderRating") %> class="ReviewOrderRating" onchange="OnChangeReviewUpdate();"><%=GetHtmlDropdownOptionSet( plReviewOrderRating, ReviewOrderRating) %></select></div>
                        </div>
                    </div>
                    <div class="DivCell" style="width:100%; padding-top:5px; text-align:left;">
                        <img src="/images/500/WE-BUCKS-PDP-REVIEW.jpg" border="0" />
                    </div>
                </div>
            </div>
        </td>
    </tr>        
    <% if strRatingCount > 0 then %>
    <tr>
        <td align="left">
            <div id="showReviews">
				<div style="width:95%; padding-left:15px; padding-right:15px; ">
				<%
                'MG 2013.03.13 Task 1634 -- Display review accross simliar products.
                sql = "EXEC sp_GetFullReview  " & itemID 
                'sql = "select rating, nickname, reviewTitle, convert(varchar(10), datetimeentd, 20) datetimeentd, review from we_Reviews where (itemID = " & itemID & " or PartNumbers like '%" & partNumber & "%') and approved = 1"
                session("errorSQL") = sql
                Set rsReview = oConn.execute(sql)
                
                if not rsReview.eof then
                    lap = 0
                    do while not rsReview.EOF
                        lap = lap + 1		
                        addlStyle = "border-top:1px solid #ccc;"
                        if lap = 1 then
                            addlStyle = ""
                        end if
            
                        if lap > 3 then
                            addlStyle = addlStyle & " display:none;"
                        end if
                        %>
                        <div id="id_childReview_<%=lap%>" style="float:left; width:100%; padding-bottom:15px; <%=addlStyle%>">
                            <div style="color:#666; width:910px; padding-top:10px;" class="smlText"><%=rsReview("nickname")%>&nbsp;&nbsp;|&nbsp;&nbsp;<%=rsReview("datetimeentd")%></div>
                            <div style="font-size:16px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; width:910px; padding-top:10px;"><%=rsReview("reviewTitle")%></div>
                            <div style="width:910px;"><%=getRatingStar(rsReview("rating"))%></div>
                            <div style="width:910px; padding-top:10px; color:#333; line-height:20px; font-size:14px; font-family:Arial, Helvetica, sans-serif;"><%=rsReview("review")%></div>
                        </div>
                        <%
                        rsReview.movenext
                    loop
                end if
                %>
				</div>
            </div>
        </td>
    </tr>
    <tr>
        <td align="center">
            <% if strRatingCount > 3 then %>
            <a style="font-size:15px; font-weight:bold; color:#FF3E01; cursor:pointer; text-decoration:underline;" id="showHideButton" onclick="showHideReviews()">Click to Show More Reviews</a>
            <% end if %>
        </td>
    </tr>
    <% end if %>        	
    </table>
</div>
<%
if blnRenderFormTag then response.Write "</form>"

end function

function getRatingStar(rating)
	dim nRating 
	dim strRatingImg
	nRating = cint(rating)
	strRatingImg = 	"<img src=""/images/product/review_star_empty.jpg"" border=""0"" width=""14"" height=""14"" />" & vbcrlf & _
					"<img src=""/images/product/review_star_empty.jpg"" border=""0"" width=""14"" height=""14"" />" & vbcrlf & _
					"<img src=""/images/product/review_star_empty.jpg"" border=""0"" width=""14"" height=""14"" />" & vbcrlf & _
					"<img src=""/images/product/review_star_empty.jpg"" border=""0"" width=""14"" height=""14"" />" & vbcrlf & _
					"<img src=""/images/product/review_star_empty.jpg"" border=""0"" width=""14"" height=""14"" />"
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""
		for i=1 to 5
			if i <= nRating then 
				strRatingImg = strRatingImg & "<img src=""/images/product/review_star_full.jpg"" border=""0"" width=""14"" height=""14"" />"
			elseif ((i - 1) + .5) <= nRating then 
				strRatingImg = strRatingImg & "<img src=""/images/product/review_star_half.jpg"" border=""0"" width=""14"" height=""14"" />"
			else
				strRatingImg = strRatingImg & "<img src=""/images/product/review_star_empty.jpg"" border=""0"" width=""14"" height=""14"" />"
			end if	
		next	
	end if
	
	getRatingStar = strRatingImg
end function
%>
<% if strRatingCount > 0 then %>
<script language="javascript">
//	setTimeout("ajax('/ajax/showReviews.asp?itemID=<%=itemID%>&partNumber=<%=partNumber%>','showReviews')",3000);
	function showHideReviews() {
		if (document.getElementById("showHideButton").innerHTML == "Click to Show More Reviews") {
			strDisplay = "";
			document.getElementById("showHideButton").innerHTML = "Click to Hide Reviews";
		}
		else {
			strDisplay = "none";
			document.getElementById("showHideButton").innerHTML = "Click to Show More Reviews";
		}
		
		for (i=4; i<=<%=strRatingCount%>; i++){
			if (document.getElementById("id_childReview_"+i) != null){
				document.getElementById("id_childReview_"+i).style.display = strDisplay;
			}
		}
		
	}
</script>
<% end if %>