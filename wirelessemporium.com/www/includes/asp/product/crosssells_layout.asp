<!--#include virtual="/includes/asp/product/inc_CrossSells.asp"-->
<div class="crossSellZone">
    <div style="width:748px; height:55px; float:left;">
        <div class="selected">PRODUCTS YOU'LL ENJOY</div>
        <div class="last"></div>
    </div>
</div>
<div style="float:left; width:100%; margin-bottom:30px;">
    <table border="0" align="left" cellspacing="0" cellpadding="0" width="100%">
        <%
        dim strTest, itemIDArray, b
        itemIDArray = split(strItemCheck,",")
        for b = 0 to Ubound(itemIDArray)
        %>
        <tr>
            <td width="100%">
                <table border="0" align="center" cellspacing="0" cellpadding="0">
                    <tr>
                        <%
                        strCrossItems = ""
                        call getCrossSellItems(itemIDArray(b), strCrossItems)
                        if strCrossItems = "" then
                            strCrossItems = CrossSells(itemIDArray(b))
                        end if
                        
                        if right(strCrossItems,1) = "," then
                            strCrossItems = left(strCrossItems, len(strCrossItems)-1)
                        end if
                        
    '																				if typeID = 16 and noLeftNav = 1 then
    '																					topRows = 4
    '																				else
    '																					topRows = 3
    '																				end if
                        topRows = 4
                        
                        sql = 	"	select	top " & topRows & " *" & vbcrlf & _
                                "	from	(	" & vbcrlf & _
                                "			select	top 8 itemid, isnull(s.subtypeName, c.typename) typename, a.HandsfreeType, itemdesc, itempic, price_retail, price_our, 0 musicskins, itempic defaultPic, b.rowid" & vbcrlf & _
                                "			from 	we_items a with (nolock) join [dbo].[tfn_StringToColumn]('" & replace(strCrossItems, "'", "") & "', ',') b" & vbcrlf & _
                                "				on	a.itemid = b.rString join we_types c" & vbcrlf & _
                                "				on	a.typeid = c.typeid left outer join we_subtypes s" & vbcrlf & _
                                "				on	a.subtypeid = s.subtypeid" & vbcrlf & _
                                "			where	a.hidelive = 0 and a.inv_qty <> 0 and a.price_our > 0" & vbcrlf & _
                                "			union" & vbcrlf & _
                                "			select	top 8 id itemid, 'Music Skins' typename, 0 HandsFreeType, a.artist + ' ' + a.designName itemDesc, a.image itemPic" & vbcrlf & _
                                "				,	isnull(a.msrp, 0.0) price_retail, isnull(a.price_we, 0.0) price_our, 1 musicskins, a.defaultImg defaultPic, b.rowid" & vbcrlf & _
                                "			from 	we_items_musicskins a with (nolock) join [dbo].[tfn_StringToColumn]('" & replace(strCrossItems, "'", "") & "', ',') b" & vbcrlf & _
                                "				on	a.id = b.rString" & vbcrlf & _
                                "			where 	a.skip = 0 " & vbcrlf & _
                                "				and a.deleteItem = 0 " & vbcrlf & _
                                "				and (a.artist <> '' and a.artist is not null) " & vbcrlf & _
                                "				and (a.designname <> '' and a.designname is not null) " & vbcrlf & _
                                "				and a.deleteItem = 0 " & vbcrlf & _
                                "			) a" & vbcrlf & _
                                "	order by a.rowid" & vbcrlf	
                        session("errorSQL") = SQL
                        set RS = Server.CreateObject("ADODB.Recordset")
                        RS.open SQL, oConn, 0, 1
                        a = 0
                        set ofsCross = Server.CreateObject("Scripting.FileSystemObject")
                        dim singularTypeName
                        do until RS.eof
                            a = a + 1
                            if RS("HandsfreeType") = 2 then
                                singularTypeName = "Bluetooth"
                            else
                                singularTypeName = singularSEO(RS("typeName"))
                            end if
                            itempic = RS("itemPic")
                            defaultPic = RS("defaultPic")
                            musicSkins = RS("musicskins")
                            useImgPath = ""
                            useLink = ""
							dispItemDesc = rs("itemDesc")
							if len(dispItemDesc) > 70 then dispItemDesc = left(dispItemDesc, 67) & "..."
    
                            if musicSkins = 1 then
                                useLink = "/p-ms-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"
                                if ofsCross.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsSmall/" & itempic)) then
                                    useImgPath = "/productpics/musicSkins/musicSkinsSmall/" & itempic
                                else
                                    if ofsCross.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & defaultPic)) then
                                        useImgPath = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & defaultPic
                                    else
                                        useImgPath = "/productpics/thumb/imagena.jpg"
                                    end if					
                                end if
                            else
                                useLink = "/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"
                                if ofsCross.FileExists(server.mappath("/productpics/thumb/" & itemPic)) then
                                    useImgPath = "/productpics/thumb/" & itemPic
                                else
                                    useImgPath = "/productpics/thumb/imagena.jpg"
                                end if
                            end if
                            %>
                            <td align="center" valign="top" width="25%" height="100%" style="padding:5px;">
                            	<div style="width:100%; text-align:center;">
	                                <a href="<%=useLink%>"><img src="<%=useImgPath%>" width="100" height="100" align="center" border="0" alt="<%=RS("itemDesc")%>"></a>
                                </div>
                            	<div style="width:100%; padding-top:10px; text-align:center; height:50px; ">
									<a href="<%=useLink%>" title="<%=RS("itemDesc")%>" style="color:#444;"><%=dispItemDesc%></a>
                                </div>
                            	<div style="width:100%; color:#cc0001; font-size:15px; font-weight:bold; text-align:center; padding-top:10px;">
									<%=formatcurrency(RS("price_Our"))%>
                                </div>
                            	<div style="width:100%; color:#666; text-align:center; padding-top:3px;">
									<s>Reg. <%=formatcurrency(prepInt(RS("price_retail")))%></s>
                                </div>
                            	<div style="width:100%; text-align:center; padding-top:10px; ">
                                	<div style="width:120px; height:20px; padding-top:8px; margin-left:25px; background-color:#cc0001; text-align:center;">
									    <a href="<%=useLink%>" title="<%=RS("itemDesc")%>" style="color:#fff; text-decoration:none; font-size:14px; font-weight:bold; color:#fff; ">
                                            SHOP NOW
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <%
                            RS.movenext
                            if a < topRows then
                                %>
                                <td align="center" valign="top" width="1" style="border-right:1px dotted #ccc;">&nbsp;</td>
                                <%
                            end if
                        loop
                        RS.close
                        set RS = nothing
                        %>
                    </tr>
                </table>
            </td>
        </tr>
        <%
        next
        %>
    </table>
</div>
