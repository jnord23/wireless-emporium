<%
on error resume next
'if typeID = 16 and noLeftNav = 1 then
'	maxIndex = 4
'else
'	maxIndex = 3
'end if
maxIndex = 4
for formIndex = 1 to maxIndex
	recentItem = Request.Cookies("RecentView" & formIndex)("id")
	if inStr(recentItem,"/") > 0 then recentItem = left(recentItem,inStr(recentItem,"/")-1)
	if recentItem <> "" and isNumeric(recentItem) then Execute("thisRecentItem" & formIndex & "id" & "=" & recentItem)
next
on error goto 0

if thisRecentItem1ID <> "" or thisRecentItem2ID <> "" or thisRecentItem3ID <> "" or thisRecentItem4ID <> "" then
	conditionalQuery = ""
%>
<div class="recentViewZone">
    <div style="width:748px; height:55px; float:left;">
        <div class="selected">RECENTLY VIEWED</div>
        <div class="last"></div>
    </div>
</div>
<div style="float:left; width:100%; margin-bottom:30px;">
    <table border="0" align="left" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="left" valign="top" width="100%" >
                <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <%
						set fsRecent = CreateObject("Scripting.FileSystemObject")
                        if thisRecentItem1ID <> "" then conditionalQuery = conditionalQuery & " ItemID = " & thisRecentItem1ID
                        if thisRecentItem2ID <> "" then
                            if conditionalQuery <> "" then conditionalQuery = conditionalQuery & " OR "
                            conditionalQuery = conditionalQuery &  " ItemID = " & thisRecentItem2ID
                        end if
                        if thisRecentItem3ID <> "" then
                        if conditionalQuery <> "" then conditionalQuery = conditionalQuery & " OR "
                            conditionalQuery = conditionalQuery & " ItemID = " & thisRecentItem3ID
                        end if
                        if thisRecentItem4ID <> "" then
                        if conditionalQuery <> "" then conditionalQuery = conditionalQuery & " OR "
                            conditionalQuery = conditionalQuery & " ItemID = " & thisRecentItem4ID
                        end if						
    
                        'SQLQuery = "SELECT TOP 3 itemID,itemDesc,itemPic,price_Retail,price_Our,HandsfreeType FROM we_Items WHERE " & conditionalQuery
                        SQLQuery = "SELECT c.brandName, d.modelName, A.itemID, A.itemDesc, A.itemPic, A.price_retail, A.modelID, A.price_Our, A.HandsfreeType, B.typeName FROM we_items A"
                        SQLQuery = SQLQuery & " INNER JOIN we_types B ON A.typeID=B.typeID left join we_brands c on a.brandID = c.brandID left join we_models d on a.modelID = d.modelID"
                        SQLQuery = SQLQuery & " WHERE " & conditionalQuery & " ORDER BY A.numberOfSales DESC"
                        set RS = oConn.execute(SQLQuery)
                        if not RS.eof then
                            a = 0
                            do until RS.eof
                                a = a + 1
                                useBrandName = RS("brandName")
                                useModelName = RS("modelName")
                                
                                if RS("HandsfreeType") = 2 then
                                    singularTypeName = "Bluetooth"
                                else
                                    singularTypeName = singularSEO(RS("typeName"))
                                end if
                                itemdesc = RS("itemdesc")
								dispItemDesc = rs("itemDesc")
								if len(dispItemDesc) > 70 then dispItemDesc = left(dispItemDesc, 67) & "..."
                                %>
                                <td align="center" valign="top" width="177" style="padding:5px;">
                                    <div style="width:100%; text-align:center;">
										<%
                                        itemRecent = RS("itemPic")
                                        itemRecentPath = Server.MapPath("/productpics/big") & "\" & itemRecent
                        
                                        if not fsRecent.FileExists(itemRecentPath) then itemRecent = "imagena.jpg"
                                        %>
                                        <a href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemDesc"))%>.asp"><img src="/productpics/thumb/<%=itemRecent%>" width="100" height="100" align="center" border="0" alt="<%=RS("itemDesc")%>"></a>
                                    </div>
                                    <div style="width:100%; padding-top:10px; text-align:center; height:50px; ">
                                        <a href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemDesc"))%>.asp" title="<%=RS("itemDesc")%>" style="color:#444;"><%=dispItemDesc%></a>
                                    </div>
                                    <div style="width:100%; color:#cc0001; font-size:15px; font-weight:bold; text-align:center; padding-top:10px;">
                                        <%=formatcurrency(RS("price_Our"))%>
                                    </div>
                                    <div style="width:100%; color:#666; text-align:center; padding-top:3px;">
                                        <s>Reg. <%=formatcurrency(prepInt(RS("price_retail")))%></s>
                                    </div>
                                    <div style="width:100%; text-align:center; padding-top:10px; ">
                                        <div style="width:120px; height:20px; padding-top:8px; margin-left:25px; background-color:#cc0001; text-align:center;">
                                            <a href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemDesc"))%>.asp" title="<%=RS("itemDesc")%>" style="color:#fff; text-decoration:none; font-size:14px; font-weight:bold; color:#fff; ">
                                                SHOP NOW
                                            </a>
                                        </div>
                                    </div>
                                </td>
                                <%
                                RS.movenext
                                if a < maxIndex and not RS.eof then
                                    %>
                                    <td align="center" valign="top" width="1" style="border-right:1px dotted #ccc;">&nbsp;</td>
                                    <%
                                end if
                            loop
                        end if
                        RS.close
                        set RS = nothing
						
						if a=1 then 
							response.write "<td width=""177"">&nbsp;</td><td width=""177"">&nbsp;</td><td width=""177"">&nbsp;</td>"
						elseif a=2 then 
							response.write "<td width=""177"">&nbsp;</td><td width=""177"">&nbsp;</td>"
						elseif a=3 then 
							response.write "<td width=""177"">&nbsp;</td>"
						end if
							
                        %>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<%
end if
%>
