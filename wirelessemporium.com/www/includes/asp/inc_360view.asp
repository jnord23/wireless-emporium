<!--<script type="text/javascript" src="/includes/js/swfobject/swfobject.js"></script>-->
<script type="text/javascript" src="/includes/js/jquery/jquery.swfobject.1-1-1.min.js"></script>
<script src="/includes/js/jquery.blockUI.js"></script>
<script>
	/*
	function loadFlash(pathToPlay, itemdesc)
	{
		var playGround = document.getElementById('flashPlayGround')
		var so = new SWFObject(pathToPlay, itemdesc, "600", "600", "9", "#FFFFFF");
		var playerVersion = swfobject.getFlashPlayerVersion(); // returns a JavaScript object 
		var majorVersion = playerVersion.major; // access the major, minor and release version numbers via their respective properties
		if (majorVersion < 9)
		{
			playGround.innerHTML = '<a href="http://www.adobe.com/go/getflashplayer" target="_blank"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" border="0" /></a>';
		}
		else
		{
			so.write("flashPlayGround");
		}
	}
	*/
	function loadFlash(pathToPlay) {
		$('#flashPlayGround').flash(
			{
				swf: pathToPlay
			,	width: 600
			,	height: 600
			}
		);
	}
	
	function close360() {
		$.unblockUI();
	}
	
	function playFlash() {
		$.blockUI.defaults.css = 
		{         
			padding:			0
		,	marginLeft:			-326
		,	marginTop:			-326
		,	width:				'652px'
		,	top:				'50%'
		,	left:				'50%'
		,	textAlign:			'center'
		,	color:				'#000'
		,	border:				'1px solid #ccc'
		,	backgroundColor:	'#fff'
		,	cursor:				'auto'
		};
		$.blockUI.defaults.overlayCSS = { backgroundColor: '#fff', opacity: 0.6 };
		
		$.blockUI({
			fadeIn: 1000
		,	message: $('#div_view360')
		,	timeout: 0
		});	
		loadFlash('<%=flashImgPath%>');		
	}
</script>


<div id="div_view360" style="display:none; width:100%; padding:1px;" align="center">
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
    	<tr>
			<td style="width:650px; height:36px; background-image: url('/images/product/360/360-gray-bar-with-icon.png'); background-repeat: no-repeat;" align="right" >
            	<div style="width:25px; height:25px; margin:7px 7px 0px 0px;"><img style="cursor:pointer;" src="/images/product/360/360-box-closex.png" border="0" onClick="close360();" /></div>&nbsp;
            </td>
		</tr>
        <tr>
        	<td style="width:650px; height:610px;" align="center" valign="middle">
            	<div id="flashPlayGround" style="display:block; width:650px; height:610px;">&nbsp;</div>
            </td>
        </tr>
	</table>
</div>
