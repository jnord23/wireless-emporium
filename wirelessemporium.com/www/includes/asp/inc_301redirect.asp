<%
sub redirectURL(byref pageType, byref id, curUrl, byref param)
	dim strMasterURL : strMasterURL = getMasterURL(pageType, id, param)
	dim querySTR : querySTR = ""
	dim url : url = curUrl
	
	if instr(url,"?") > 0 then 
		querySTR = mid(request.ServerVariables("HTTP_X_REWRITE_URL"),instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"?") + 1)
	end if
	
	if instr(url,"?") > 0 then 
		url = left(request.ServerVariables("HTTP_X_REWRITE_URL"), instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"?") - 1)
	end if

	if "" <> strMasterURL then 
		if url <> strMasterURL then
			if "" <> querySTR then
				dim objQs: set objQs=NewQueryString( querySTR) 'fix infinite loop
				select case( GetPhysicalAbsolutePath())
					case "/brand-model-category":
						objQs.Remove( "BrandID")
						objQs.Remove( "ModelId")
						objQs.Remove( "CategoryId")
				end select

				strMasterURL = strMasterURL & objQs.AsQueryString
			end if
			call PermanentRedirect( strMasterURL)
		end if
	end if
end sub

function getMasterURL(byref pageType, byref id, byref param)
	dim strMasterURL : strMasterURL = ""
	dim tBrandID, tModelID, tCategoryID, tBrandName, tModelName, tCategoryName, tMusicGenreName, tMusicArtistName, tPhoneOnly, tItemDesc, tItemID

	select case lcase(pageType)
		case "p"	'product page
			if isobject(param) then
				tItemID 	= param.Item("x_itemID")
				tItemDesc 	= param.Item("x_itemDesc")
				
				strMasterURL = "/p-" & tItemID & "-" & formatSEO(tItemDesc)
			end if
		case "b"	'brand page
			if isobject(param) then
				tBrandID 	= param.Item("x_brandID")
				tPhoneOnly 	= param.Item("x_phoneOnly")
				select case tBrandID
					case 1 : strMasterURL = "/utstarcom-cell-phone-accessories"
					case 2 : strMasterURL = "/sony-ericsson-cell-phone-accessories"
					case 3 : strMasterURL = "/kyocera-cell-phone-accessories"
					case 4 : strMasterURL = "/lg-cell-phone-accessories"
					case 5 : strMasterURL = "/motorola-cell-phone-accessories"
					case 6 : strMasterURL = "/nextel-cell-phone-accessories"
					case 7 : strMasterURL = "/nokia-cell-phone-accessories"
					case 9 : strMasterURL = "/samsung-cell-phone-accessories"
					case 10 : strMasterURL = "/sanyo-cell-phone-accessories"
					case 11 : strMasterURL = "/siemens-cell-phone-accessories"
					case 14 : strMasterURL = "/blackberry-cell-phone-accessories"
					case 15 : strMasterURL = "/other-brand-cell-phone-accessories"
					case 16 : strMasterURL = "/hp-palm-cell-phone-accessories"
					case 17
						if tPhoneOnly = 1 then
							strMasterURL = "/iphone-accessories"
						else
							strMasterURL = "/ipod-ipad-accessories"							
						end if
					case 18 : strMasterURL = "/pantech-cell-phone-accessories"
					case 19 : strMasterURL = "/sidekick-cell-phone-accessories"
					case 20 : strMasterURL = "/htc-cell-phone-accessories"
					case 28 : strMasterURL = "/casio-cell-phone-accessories"
					case 29 : strMasterURL = "/huawei-cell-phone-accessories"
					case 30 : strMasterURL = "/zte-cell-phone-accessories"
				end select
			end if
		case "c"	'category page
			select case id
				case 1: strMasterURL = "/cell-phone-batteries"
				case 2: strMasterURL = "/cell-phone-chargers-cables"
				case 3: strMasterURL = "/cell-phone-covers-faceplates"
				case 5: strMasterURL = "/bluetooth-headsets-handsfree"
				case 6: strMasterURL = "/cell-phone-holsters-holders-mounts"
				case 7: strMasterURL = "/cell-phone-pouches-carrying-cases"
				case 12: strMasterURL = "/cell-phone-signal-boosters"
				case 13: strMasterURL = "/cell-phone-memory-cards-readers"
				case 15: strMasterURL = "/cell-phones-tablets"
				case 18: strMasterURL = "/cell-phone-screen-protectors-skins"
				case 22: strMasterURL = "/cell-phone-bundle-packs"
				case 1030: strMasterURL = "/cell-phone-design-faceplates-covers"
				case 1040: strMasterURL = "/cell-phone-heavy-duty-hybrid-cases"
				case 1050: strMasterURL = "/cell-phone-rhinestone-bling-cases"
				case 1060: strMasterURL = "/cell-phone-rubberized-hard-covers"
				case 1070: strMasterURL = "/cell-phone-silicone-cases"
				case 1080: strMasterURL = "/cell-phone-tpu-crystal-candy-cases"
				case 1090: strMasterURL = "/cell-phone-replacement-and-extended-batteries"
				case 1100: strMasterURL = "/cell-phone-universal-batteries"
				case 1110: strMasterURL = "/cell-phone-car-chargers"
				case 1120: strMasterURL = "/cell-phone-desktop-cradles-docks"
				case 1130: strMasterURL = "/cell-phone-home-chargers"
				case 1140: strMasterURL = "/cell-phone-spare-battery-chargers"
				case 1150: strMasterURL = "/cell-phone-universal-chargers"
				case 1360: strMasterURL = "/cell-phone-data-cables"
				case 1380: strMasterURL = "/battery-powered-cell-phone-chargers"
				case 1160: strMasterURL = "/cell-phone-horizontal-cases-pouches"
				case 1170: strMasterURL = "/cell-phone-vertical-cases-pouches"				
				case 1180: strMasterURL = "/sleeves-portfolios"								
				case 1190: strMasterURL = "/cell-phone-car-mounts-stands"
				case 1200: strMasterURL = "/cell-phone-holsters"
				case 1220: strMasterURL = "/cell-phone-bluetooth-car-kits"				
				case 1230: strMasterURL = "/cell-phone-bluetooth-headsets"								
				case 1240: strMasterURL = "/speakers"												
				case 1250: strMasterURL = "/cell-phone-wired-headsets"																
				case 1260: strMasterURL = "/decal-skin"
				case 1270: strMasterURL = "/music-skins"
				case 1280: strMasterURL = "/cell-phone-screen-surface-protection"
				case 1290: strMasterURL = "/cell-phone-starter-kit"
				case 1300: strMasterURL = "/cell-phone-power-pack"
			end select
		case "car"
			select case id
				case 2 : strMasterURL = "/car-2-phones-att-cingular"
				case 4 : strMasterURL = "/car-4-phones-sprint-nextel"
				case 5 : strMasterURL = "/car-5-phones-t-mobile"
				case 6 : strMasterURL = "/car-6-phones-verizon"
				case 12 : strMasterURL = "/car-12-phones-unlocked-cell-phones"
			end select
		case "cb"	'category-brand
			if isobject(param) then 
				tBrandID			=	param.Item("x_brandID")
				tCategoryID			=	param.Item("x_categoryID")
				tBrandName			=	param.Item("x_brandName")
				tCategoryName		=	param.Item("x_categoryName")
				if isnumeric(tBrandID) and isnumeric(tCategoryID) and "" <> tBrandName and "" <> tCategoryName then 
					if tBrandID = 17 and tCategoryID = 7 and tBrandName = "apple-iphone" then
						strMasterURL = "/apple-iphone-leather-carrying-cases-pouches"
					else
						strMasterURL = "/sc-" & tCategoryID & "-sb-" & tBrandID & "-" & formatSEO(tBrandName) & "-" & formatSEO(tCategoryName)
					end if
				end if
			end if
		case "bmc"	'brand-model-category page
			if isobject(param) then 
				tBrandID			=	param.Item("x_brandID")
				tModelID			=	param.Item("x_modelID")
				tCategoryID			=	param.Item("x_categoryID")
				tBrandName			=	param.Item("x_brandName")
				tModelName			=	param.Item("x_modelName")
				tCategoryName		=	param.Item("x_categoryName")
				tMusicGenreName		=	param.Item("x_musicGenreName")
				tMusicArtistName	=	param.Item("x_musicArtistName")
				if isnumeric(tBrandID) and isnumeric(tModelID) and isnumeric(tCategoryID) and "" <> tBrandName and "" <> tModelName and "" <> tCategoryName then 
					if 0 = tBrandID and 0 = tModelID and 14 = tCategoryID then
						strMasterURL = "/cell-phone-charms-stickers"
					elseif 15 = tCategoryID then
						strMasterURL = "/sb-" & tBrandID & "-sm-0-sc-" & tCategoryID & "-" & formatSEO(tBrandName) & "-" & formatSEO(tCategoryName)
					elseif 20 = tCategoryID or tCategoryID = 1270 then
						if len(tMusicArtistName) > 0 then
							strMasterURL = "/sb-" & tBrandID & "-sm-" & tModelID & "-scd-1270-" & formatSEO(tCategoryName) & "-" & formatSEO(tMusicGenreName) & "-" & formatSEO(tMusicArtistName)
						else
							strMasterURL = "/sb-" & tBrandID & "-sm-" & tModelID & "-scd-1270-" & formatSEO(tCategoryName) & "-" & formatSEO(tMusicGenreName)
						end if
					else
						strMasterURL = "/sb-" & tBrandID & "-sm-" & tModelID & "-sc-" & tCategoryID & "-" & formatSEO(tCategoryName) & "-" & formatSEO(tBrandName) & "-" & formatSEO(tModelName)
					end if
				end if
			end if
		case "bmc-music-genre"	'musicskins genre select
			if isobject(param) then 
				tBrandID			=	param.Item("x_brandID")
				tModelID			=	param.Item("x_modelID")
				tCategoryID			=	param.Item("x_categoryID")
				tBrandName			=	param.Item("x_brandName")
				tModelName			=	param.Item("x_modelName")
				tCategoryName		=	param.Item("x_categoryName")
				if isnumeric(tBrandID) and isnumeric(tModelID) and isnumeric(tCategoryID) and "" <> tBrandName and "" <> tModelName and "" <> tCategoryName then 
					strMasterURL = "/sb-" & tBrandID & "-sm-" & tModelID & "-sc-1270-" & formatSEO(tBrandName) & "-" & formatSEO(tModelName) & "-" & formatSEO(tCategoryName) & "-genre"
				end if
			end if
		case "bmcd"	'brand-model-category-details page
			if isobject(param) then 
				tBrandID			=	param.Item("x_brandID")
				tModelID			=	param.Item("x_modelID")
				tCategoryID			=	param.Item("x_categoryID")
				tBrandName			=	param.Item("x_brandName")
				tModelName			=	param.Item("x_modelName")
				tCategoryName		=	param.Item("x_categoryName")
				tMusicGenreName		=	param.Item("x_musicGenreName")
				tMusicArtistName	=	param.Item("x_musicArtistName")
				if isnumeric(tBrandID) and isnumeric(tModelID) and isnumeric(tCategoryID) and "" <> tBrandName and "" <> tModelName and "" <> tCategoryName then 
					if 0 = tBrandID and 0 = tModelID and 14 = tCategoryID then
						strMasterURL = "/cell-phone-charms-stickers"
					elseif 20 = tCategoryID or tCategoryID = 1270 then
						if len(tMusicArtistName) > 0 then
							strMasterURL = "/sb-" & tBrandID & "-sm-" & tModelID & "-scd-1270-" & formatSEO(tCategoryName) & "-" & formatSEO(tMusicGenreName) & "-" & formatSEO(tMusicArtistName)
						else
							strMasterURL = "/sb-" & tBrandID & "-sm-" & tModelID & "-scd-1270-" & formatSEO(tCategoryName) & "-" & formatSEO(tMusicGenreName)
						end if
					else
						strMasterURL = "/sb-" & tBrandID & "-sm-" & tModelID & "-scd-" & tCategoryID & "-" & formatSEO(tCategoryName) & "-" & formatSEO(tBrandName) & "-" & formatSEO(tModelName)
					end if
				end if
			end if			
	end select

	getMasterURL = strMasterURL

end function
%>
