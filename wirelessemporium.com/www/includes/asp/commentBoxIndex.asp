<%
recaptcha_public_key	= "6Ld2C74SAAAAAG_CoH_enQM1_uKABFbiftwRnxyW" ' your public key
recaptcha_private_key	= "6Ld2C74SAAAAADoSV8ZEMd2SBztaSvrXBYcs59rQ" ' your private key
%>
<%''' **************** Start Comment Card ********************* %>
						<div style="width:100%; height:34px; border-left:1px solid #CCC; border-right:1px solid #CCC;" align="left">
                            <div class="graybox-middle" style="width:430px; padding-left:10px;">
								<div class="graybox-inner" title="WE COMMENT CARD">SUGGESTION BOX</div>
                            </div>
                        </div>
                        <div style="height:40px; width:420px; text-align:justify; padding:5px;" class="xtrasmlIndexText" align="left">
							We work hard to make sure our site offers the best shopping experience possible. If you have any suggestions 
                            on how we can make things even better - whatever it is, please drop us a note!
                        </div>
                        <form id="frmComment"  name="frmComment">                        
                        <div style="height:15px; width:420px; padding:5px;" class="xtrasmlIndexText" align="left">
                            <div class="xtrasmlIndexText" style="float:left; width:90px; font-weight:bold; margin-right:10px;">Email (optional):</div>
                            <input type="text" name="email" value="" size="35" class="xtrasmlIndexText" style="float:left;" />
						</div>
                        <div style="height:15px; width:420px; padding:5px;" class="xtrasmlIndexText" align="left">
                            <div class="xtrasmlIndexText" style="float:left; width:90px; font-weight:bold; margin-right:10px;">Phone (optional): </div>
                            <input type="text" name="phone" value="" size="35" class="xtrasmlIndexText" style="float:left;" />
						</div>
                        <div align="right" style="width:430px;">
                        	<TEXTAREA rows="3" cols="50" id="textarea1" name="textarea1" style="margin-top:5px;"></TEXTAREA>
						</div>
                        <div align="right" style="width:430px; padding-top:5px;">
	                        <div align="left" style="float:right; width:420px; height:60px; border:1px solid #ccc;">
                            	<div id="recaptcha_image" style="float:left;"></div>
                            	<div style="float:right; padding:35px 2px 0px 0px; font-size:9px;"><a href="javascript:Recaptcha.reload()">Get another<br />CAPTCHA</a></div>
							</div>
                            <div id="commentSubmitHome" title="Submit Comment" onclick="return checkall();" style="float:right; cursor:pointer; margin-top:5px;"></div>
                            <div class="xtrasmlIndexText" style="float:right; font-weight:bold; padding-top:2px; margin-right:5px;">Enter the words above: <input type="text" name="recaptcha_response_field" id="recaptcha_response_field" size="18" /></div>
                        </div>
						</form>
<%''' **************** End Comment Card ********************* %>
<script type="text/javascript" src="/includes/js/recaptcha_ajax.js"></script>
<script language="javascript">
	function showRecaptcha() { 
		var RecaptchaOptions = {theme : 'custom'};
	 	Recaptcha.create('<%=recaptcha_public_key%>', 'recaptcha_image', RecaptchaOptions);	
	}
	
	(function () {
		setTimeout("showRecaptcha()",500)
	}());
	
	<% ''' *********** For Comments Validations -- After validation it will send the request to server using with ajax%>
	var commentLoop = 0;
	var commentValue = '';
	
	function checkall() {	 
	  var GreForm = this.document.frmComment;
		
		if (GreForm.textarea1.value=="") {
			alert("Enter Your Comments");
			GreForm.textarea1.focus();
			return false;
		}
		<%
		useURL = request.ServerVariables("HTTP_X_REWRITE_URL")
		if useURL = "/" then useURL = "index.asp"
		%>
		useURL = escape("<%=useURL%>")
		ajax('/ajax/saveCommentCard.asp?page=' + useURL + '&email=' + document.frmComment.email.value + '&phone=' + document.frmComment.phone.value + '&comment=' + escape(document.frmComment.textarea1.value) + '&re_challenge=' + Recaptcha.get_challenge() + '&re_response=' + Recaptcha.get_response() + '&re_privatekey=<%=recaptcha_private_key%>','dumpZone')
		commentValue = GreForm.textarea1.value
		GreForm.textarea1.value = ""
		
		setTimeout("chkCommentReturn()",500)
		
		return true;
	}
	
	function chkCommentReturn() {
		commentLoop++
		if (commentLoop < 5) {
			if (document.getElementById("dumpZone").innerHTML != "") {
				if (document.getElementById("dumpZone").innerHTML == "wrong recaptcha") {
					document.getElementById('textarea1').value = commentValue;
					alert('You have entered the wrong secret words.');
				}
				else if (document.getElementById("dumpZone").innerHTML == "commentGood") {
					alert("Comment saved");
				}
				else {
					alert("Error saving comment\nPlease resubmit\n" + document.getElementById("dumpZone").innerHTML);
					document.getElementById('textarea1').value = commentValue;
				}
			}
		}
		else {
			alert("Error saving comment\nPlease resubmit");
			document.getElementById('textarea1').value = commentValue;
		}
	}
</script>