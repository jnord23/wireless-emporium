<%
sub ImageHtmlTagWrapper( byval strImageTag, byval strUrl, byval blnSale)
	dim strDivWrapperHtmlStart: strDivWrapperHtmlStart=EMPTY_STRING
	dim strDivIconHtmlStart: strDivIconHtmlStart=EMPTY_STRING
	dim strDivHtmlEnd: strDivHtmlEnd=EMPTY_STRING

	if blnSale then 
		strDivWrapperHtmlStart="<div style=""display:block; position: relative; z-index:0;"">"
		strDivIconHtmlStart="<div style=""display:block; position: absolute; top: -20px; right: 0px; z-index:1;""><a href="""& strUrl &"""><img src=""/images/saleIcon.png"" alt=""Sale Price"" border=""0"" /></a>"
		strDivHtmlEnd="</div></div>"
	end if
	
%><%=strDivWrapperHtmlStart%><a href="<%=strUrl%>"><%=strImageTag%></a><%=strDivIconHtmlStart%><%=strDivHtmlEnd%><%

end sub 

sub ImageHtmlTagWrapper2( byval strImageTag, byval strUrl, byval strSaleImg, byval blnSale)
	dim strDivWrapperHtmlStart: strDivWrapperHtmlStart=EMPTY_STRING
	dim strDivIconHtmlStart: strDivIconHtmlStart=EMPTY_STRING
	dim strDivHtmlEnd: strDivHtmlEnd=EMPTY_STRING

	if blnSale then 
		strDivWrapperHtmlStart="<div style=""display:block; position: relative; z-index:0;"">"
		strDivIconHtmlStart="<div style=""display:block; position: absolute; top: -15px; right: 0px; z-index:1;""><a href="""& strUrl &"""><img src=""" & strSaleImg & """ alt=""Sale Price"" border=""0"" /></a>"
		strDivHtmlEnd="</div></div>"
	end if
	
%><%=strDivWrapperHtmlStart%><a href="<%=strUrl%>"><%=strImageTag%></a><%=strDivIconHtmlStart%><%=strDivHtmlEnd%><%

end sub 


sub ImageHtmlTagWrapperProductDetail( byref pArrImgObj)
	response.write "<div style=""width:300px; display:block; position: relative; z-index:-3;"">"
			
	if isArray(pArrImgObj) then
		for i=0 to ubound(pArrImgObj)
			response.write pArrImgObj(i)
		next
	end if
	
	response.write "</div>"
end sub 


sub YouSaveValueHtmlTagWrapperProductDetail( byval strHtmlText, byval blnSale)
	dim strHtmlStart: strHtmlStart=EMPTY_STRING
	dim strHtmlEnd: strHtmlEnd=EMPTY_STRING

	if blnSale then 
		strHtmlStart="<s>"
		strHtmlEnd="</s>"
	end if
	
%><%=strHtmlStart%><%=strHtmlText%><%=strHtmlEnd%><%
end sub 

'extra join to get the special pricing attributes [knguyen/20110602]
strSqlSaleLeftJoin = 	" LEFT JOIN ( " &VbCrLf&_
						"	select v.[ItemId], t.[Name] as [ActiveItemValueType], v.[OriginalPrice] " &VbCrLf&_
						"	from ItemValue v with(nolock)  " &VbCrLf&_
						"	inner join ItemValueType t with(nolock)  " &VbCrLf&_
						"	on  " &VbCrLf&_
						"		t.[ItemValueTypeId]=v.[ActiveItemValueTypeId]  " &VbCrLf&_
						"		and t.[IsActive]=1  " &VbCrLf&_
						"		and v.[IsActive]=1  " &VbCrLf&_
						"		and v.[SiteId]="& DbId( obj2Str( Application("SiteId"))) &"  " &VbCrLf&_
						" ) tv  " &VbCrLf&_
						" on tv.[ItemId]=c.[ItemId] " &VbCrLf
%>