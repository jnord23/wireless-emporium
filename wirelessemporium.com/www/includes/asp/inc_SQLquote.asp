<%
function SQLquote(strValue)
	strContent = trim(strValue)
	if not isNull(strContent) and strContent <> "" then
		strContent = trim(replace(strContent,chr(34),"&quot;"))
		strContent = replace(strContent,"%26%2343%3B1","+")
		strContent = replace(strContent,"&#43;","+")
		strContent = replace(strContent,Chr(39),"''")
		strContent = replace(strContent,"--","")
		strContent = replace(strContent,"/*","")
		strContent = replace(strContent,"*/","")
		strContent = replace(strContent,"xp_","")
		strContent = trim(replace(strContent,chr(146),"''"))
		strContent = trim(replace(strContent,chr(147),"&quot;"))
		strContent = trim(replace(strContent,chr(148),"&quot;"))
	else
		strContent = strValue
	end if
	SQLquote = strContent
end function
%>