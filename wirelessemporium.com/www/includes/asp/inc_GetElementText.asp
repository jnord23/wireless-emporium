<%
Function GetElementText(Node, Tagname)
	Dim NodeList
	Set NodeList = Node.getElementsByTagname(Tagname)
	If NodeList.Length > 0 Then
		For b = 0 To NodeList.Length - 1
			Set CurrNode = NodeList.nextNode
			GetElementText = CurrNode.text
		Next
	Else
		GetElementText = ""
	End If
	Set NodeList = Nothing
End Function
%>
