<%
function IsValidEmail(emailAddress)
	dim ValidEmail, emailParts, iLoopCounter, emailChar, acceptableChars, last2Chars
	ValidEmail = true
	'acceptableChars are the characters that we will allow in our email
	acceptableChars = "abcdefghijklmnopqrstuvwxyz.-_@"
	emailParts = split(emailAddress, "@")
	'check to make sure that there is only 1 @ and that there are 2 parts
	'remember arrays are zero based
	'Using the UBound function will return the highest element in the array
	'So if it's a valid email the UBound function will return 1, i.e. 0 start
	if UBound(emailParts) <> 1 then
		ValidEmail = false
	else
		'Check the length of each part of the email address
		'first part can be just one character, 2nd part must be atleast 4
		if Len(emailParts(0)) < 1 or Len(emailParts(1)) < 4 then ValidEmail = false
		'check first character on the left part isn't a "." using Left function
		if Left(emailParts(0), 1) = "." then ValidEmail = false
		'check that there is a . in the second part of the email address - .com
		if InStr(emailParts(1), ".") <= 0 then ValidEmail = false
		'We know there's a . now make sure there are 2 characters after the . for valid email
		last2Chars = Right(emailParts(1),2)
		if InStr(last2chars,".") Then ValidEmail = false
		'check that there shouldn't be a _ in the second part of the email address
		if InStr(emailParts(1), "_") > 0 then ValidEmail = false
	end if
	'loop through each character of email
	for iLoopCounter = 1 to Len(emailAddress)
		emailChar = Lcase(Mid(emailAddress, iLoopCounter, 1))
		if InStr(acceptableChars, emailChar) = 0 and not IsNumeric(emailChar) then ValidEmail = false
	next
	'check if there is 2 . in a row
	if InStr(emailAddress, "..") > 0 then ValidEmail = false
	'check if there is @. in a row
	if InStr(emailAddress, "@.") > 0 then ValidEmail = false
	IsValidEmail = ValidEmail
end function
%>
