<%
sub GiftCertificate(nOrderId,giftAmount)
	dim flag, RSgift
	flag = 0
	if nOrderId <> "new" then
		SQL = "SELECT * FROM GiftCertificates WHERE FreeWithOrderNumber = '" & nOrderId & "'"
		set RSgift = Server.CreateObject("ADODB.Recordset")
		RSgift.open SQL, oConn, 3, 3
		if not RSgift.eof then
			GiftCertificateCode = RSgift("GiftCertificateCode")
			giftAmount = RSgift("amount")
		end if
	end if
	if GiftCertificateCode = "" then
		GiftCertificateCode = generateRequestID()
		do until flag = 1
			SQL = "SELECT * FROM GiftCertificates WHERE GiftCertificateCode = '" & GiftCertificateCode & "'"
			set RSgift = Server.CreateObject("ADODB.Recordset")
			RSgift.open SQL, oConn, 3, 3
			if RSgift.eof then
				SQL = "INSERT INTO GiftCertificates (store,GiftCertificateCode,amount,"
				if nOrderId <> "new" then SQL = SQL & "FreeWithOrderNumber,"
				SQL = SQL & "datePurchased) VALUES ("
				SQL = SQL & "0,"
				SQL = SQL & "'" & GiftCertificateCode & "',"
				SQL = SQL & "'" & giftAmount & "',"
				if nOrderId <> "new" then SQL = SQL & "'" & nOrderId & "',"
				SQL = SQL & "'" & now & "')"
				oConn.execute SQL
				flag = 1
			else
				flag = 0
			end if
		loop
	end if
	RSgift.close
	set RSgift = nothing
end sub

function generateRequestID()
	randomize
	dim aLoop, n, strChars
	strChars = ""
	for aLoop = 1 to 16
		n = int(rnd * 26) + 75
		if n >= 91 then
			strChars = strChars & chr(n - 43)
		else
			strChars = strChars & chr(n)
		end if
	next
	strChars = replace(replace(strChars,"0","J"),"O","X")
	generateRequestID = strChars
end function
%>
