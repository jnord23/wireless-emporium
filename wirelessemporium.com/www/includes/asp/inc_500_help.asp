						<div>
                        	<div style="color:#666; padding:20px 0px 0px 20px;" align="left">
                                Now you can earn points for being social by joining our Wireless Emporium Rewards Program. It is very easy to enroll, earn and redeem points for special coupons and gift certificates that will help you save.<br /><br />
								There are many ways to earn points, such as by purchasing a product, emailing a product to a friend, following us on Twitter, referring a friend that purchases, writing a product review, asking and answering questions about a product, tweeting and much more.<br />
                                <a id="id_readMore" href="#" onclick="readMore();return false;" style="color:#ef6527; font-weight:bold; text-decoration:underline;">[Read More]</a>
                            </div>
                            <div class="help_content" id="id_helpcontent" style="display:none; padding-top:20px;">
								<style type="text/css">
                                  .help_content	{text-align:left; font-family:arial; font-size: 12px; padding: 10px; line-height: 18px; color: #666;}
                                  .reward-table{ font-family:arial; font-size: 12px; padding: 10px; line-height: 18px; color: #666; margin: 10px 0px; border-collapse: collapse; background-color: #f2f2f2; }
                                  .reward-td{ padding: 8px; border: 1px solid #ccc; `}
                                  .reward-td strong{ color: #666;}*/
                                  strong{ color: #333;}
                                  hr.reward { margin: 15px 0px !important;}
                                  a.question{ color: #333; text-decoration:underline;}
                                  a.back_to_top{ font-size: 11px;  text-decoration:underline;}
                                  div.answer{ padding: 5px 0px; }
                                </style>
                                <strong style="font-size:14px">WE Bucks Loyalty Program FAQs</strong>
                                  <hr>
                                  <a class="question" href="#1">What is WE Bucks Loyalty Program?</a><br />
                                  <a class="question" href="#2">Who is eligible to join?</a><br />
                                  <a class="question" href="#3">What does it cost?</a><br />
                                  <a class="question" href="#4">How do I enroll?</a><br />
                                  <a class="question" href="#5">How do I earn WEBucks?</a><br />
                                  <a class="question" href="#6">Is there a limit to the number of WEBucks I can earn?</a><br />
                                  <a class="question" href="#7">Do my WEBucks expire?</a><br />
                                  <a class="question" href="#8">What can I redeem my WEBucks for?</a><br />
                                  <a class="question" href="#9">How can I view my WEBucks balance?</a><br />
                                  <a class="question" href="#10">How do I redeem WEBucks?</a><br />
                                  <a class="question" href="#11">Am I able to use WEBucks at checkout?</a><br />
                                  <a class="question" href="#12">Is there a place where I can report a problem or make a suggestion?</a><br />
                                  <hr>
                                  <a name="1"></a>
                                  <strong>What is WE Bucks Loyalty Program?</strong>
                                  <div class="answer">
                                    Our loyalty program allows you to earn points for sharing, purchasing online, reviewing and much more. The points can be redeemed for specific rewards such as coupons and gift cards.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="2"></a>
                                  <strong>Who is eligible to join?</strong>
                                  <div class="answer">
                                    All customers are eligible to enroll in the loyalty program.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="3"></a>
                                  <strong>What does it cost?</strong>
                                  <div class="answer">
                                    Enrollment in the program is completely free.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="4"></a>
                                  <strong>How do I enroll?</strong>
                                  <div class="answer">
                                    To enroll, visit our <a target="_parent" href="/register/member_login.asp">Rewards Page</a> and click on the "Enroll Now" button. You will also have an opportunity to enroll after completing a purchase.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="5"></a>
                                  <strong>How do I earn WEBucks?</strong>
                                  <div class="answer">
                                    You can earn WEbucks in a variety of different ways. Each time you place an online order you accumulate 1 point for each dollar spent on our website. You can also earn points for being social and sharing products with friends. To see the many different ways that points can be earned, check out the Earn WEbucks tab on the Member Page.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="6"></a>
                                  <strong>Is there a limit to the number of WEBucks I can earn?</strong>
                                  <div class="answer">
                                    While some tasks can only be completed once, others, such as tweeting, can be done many times. There are certain limitations on the number of points that can be earn for these actions but don't worry because that limit is reset at the start of each week.
                                    <table style="width:380px" class="reward-table">
                                      <tr>
                                        <td class="reward-td"></td>
                                        <td class="reward-td"><strong>Total # of points that can be earned</strong></td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>Tweet a product</strong></td>
                                        <td class="reward-td">50 points per week</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>Write a review</strong></td>
                                        <td class="reward-td">150 points per week</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>Open an email</strong></td>
                                        <td class="reward-td">25 points per week</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>Send an email</strong></td>
                                        <td class="reward-td">30 points per week</td>
                                      </tr>
                                    </table>
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="7"></a>
                                  <strong>Do my WEBucks expire?</strong>
                                  <div class="answer">
                                    We think that when you earn WEbucks, you should be able to keep them as long as you'd like. That's why, no matter how long you take to redeem them, your WEbucks will be safe and sound in a the world's most secure secret vault. Well maybe not, but they'll be in your account for when you're ready to use them.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="8"></a>
                                  <strong>What can I redeem my WEBucks for?</strong>
                                  <div class="answer">
                                    You can redeem WEbucks for a variety of rewards from exclusive products to discounts and gift cards.
                                    <table style="width:380px" class="reward-table">
                                      <tr>
                                        <td class="reward-td"><strong>50 WEBucks</strong></td>
                                        <td class="reward-td">10% Off Coupon</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>100 WEBucks</strong></td>
                                        <td class="reward-td">15% Off Coupon</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>150 WEBucks</strong></td>
                                        <td class="reward-td">$5 Gift Card</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>300 WEBucks</strong></td>
                                        <td class="reward-td">20% Off Coupon</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>500 WEBucks</strong></td>
                                        <td class="reward-td">25% Off Coupon</td>
                                      </tr>
                                      <tr>
                                        <td class="reward-td"><strong>1000 WEBucks</strong></td>
                                        <td class="reward-td">30% Off Coupon and Free Faceplate*</td>
                                      </tr>
                                    </table>
                                *Free Faceplate reward can be redeemed for any non-OEM faceplate valued at $11.99 or under.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="9"></a>
                                  <strong>How can I view my WEBucks balance?</strong>
                                  <div class="answer">
                                    To view your balance and program activity, login to the member application found here: <a target="_parent" href="/register/member_login.asp">Activity</a> and click on the "Activity" tab. Here you will find a detailed list of all completed activities as well as point totals for each of these activities.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="10"></a>
                                  <strong>How do I redeem WEBucks?</strong>
                                  <div class="answer">
                                    To redeem your points for rewards, login to the member application found here: <a target="_parent" href="/register/member_login.asp">Rewards</a> and click on the "Rewards" tab. Here you will find a list of all program rewards. There will be a "Redeem" button below any reward that you have enough WEbucks to claim. Click on "Redeem" and instructions for redemption will be provided.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="11"></a>
                                  <strong>Am I able to use WEBucks at checkout?</strong>
                                  <div class="answer">
                                    Upon redeeming a reward, you will be provided with a unique coupon code that can be applied at checkout. For the $5 gift card reward, the entire value of the card must be used at once and cannot be carried over to another purchase.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
                                  <hr>
                                  <a name="12"></a>
                                  <strong>Is there a place where I can report a problem or make a suggestion?</strong>
                                  <div class="answer">
                                    Please contact our Customer Happiness department with any questions. They can be reached at 1-800-305-1106.
                                  </div>
                                  <a href="#top" class="back_to_top">Back to top</a>
  


                            </div>
                        </div>
<script>
	function toggle(which) 
	{
		if (document.getElementById(which).style.display == '') 
			document.getElementById(which).style.display='none'
		else 
			document.getElementById(which).style.display=''
	}

	function readMore() {
		toggle('id_helpcontent');
	}
</script>                        