<%
dim mySession, myAccount, sUserfName
on error resume next

csid = request.QueryString("csid")
if isnull(csid) or len(csid) < 1 or not isnumeric(csid) then csid = 0 else csid = cdbl(csid)

if csid > 0 then
	response.Cookies("mySession") = csid
	response.cookies("mySession").expires = dateAdd("m", 1, now)
end if

mySession = request.cookies("mySession")
myAccount = request.cookies("myAccount")
sUserfName = request.cookies("fname")
on error goto 0
%>
