<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<%
dim ReceiptText
ReceiptText = ""

dim sPromoCode
SQL = "SELECT B.PromoCode FROM we_orders A INNER JOIN we_coupons AS B ON A.couponid=B.couponid WHERE A.orderid = '" & nOrderID & "'"
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if not RS.eof then sPromoCode = RS("PromoCode")

'''''''''''''''''' START DISCOUNT ZONE ''''''''''''''''''
if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	SQL = "SELECT * FROM WE_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		FreeItemPartNumber = RS("FreeItemPartNumber")
		setValue = RS("setValue")
		oneTime = RS("oneTime")
	else
		sPromoCode = ""
	end if
end if

'mySession needs to be defined using the sessionID from the time of original purchase
'Otherwise, the promocode lookup from the shopping cart will return 0 results and no discount will be shown when it should be  
'We also can't use the current sessionID because it doesn't match the original purchase sessionID
sqlSession =	"select		s.sessionID--, * " & vbcrlf & _
				"from		we_orders o left join ShoppingCart s " & vbcrlf & _
				"	on		o.orderid = s.purchasedOrderID " & vbcrlf & _
				"where		o.orderid = '" & nOrderId & "'"
set sessionRS = Server.CreateObject("ADODB.Recordset")
set sessionRS = oConn.execute(sqlSession)
mySession = prepInt(sessionRS("sessionID"))
'if session("adminid") = 96 then
'	response.write "{mySession:" & mySession & "}<br />"
'	response.write "{couponid:" & couponid & "}<br />"
'	response.write "{sPromoCode:" & sPromoCode & "}<br />"
'	response.write "{discountTotal:" & discountTotal & "}<br />"
'end if
sPromoCodeSave = sPromoCode 'Save the promo code for printing on the receipt
%><!--#include virtual="/cart/includes/inc_promoFunctions.asp"--><% 'Calculates the discount total as variable discountTotal && sPromoCode could be set to an empty string
'if session("adminid") = 96 then
'	response.write "{mySession:" & mySession & "}<br />"
'	response.write "{couponid:" & couponid & "}<br />"
'	response.write "{sPromoCode:" & sPromoCode & "}<br />"
'	response.write "{discountTotal:" & discountTotal & "}<br />"
'end if
call fOpenConn() 'inc_promoFunctions.asp closes the connection, re-open it so processing can continue
'''''''''''''''''' END DISCOUNT ZONE ''''''''''''''''''

dim sFname, sLname, sEmail, sPhone
dim saddress1, saddress2, sCity, sstate, szip, sCountry
dim sShipAddress, sBillAddress
dim oRsCust, oRsOrd

SQL = "SELECT * FROM we_accounts WHERE accountid = '" & nAccountId & "'"
session("errorSQL") = SQL
set oRsCust = Server.CreateObject("ADODB.Recordset")
oRsCust.open SQL, oConn, 3, 3
if oRsCust.eof then
	ReceiptText = "<h3>Customer account" & nAccountId & " not found!</h3>"
else
	sFname = oRsCust("fname")
	sLname = oRsCust("lname")
	sEmail = oRsCust("email")
	sPhone = oRsCust("phone")
end if

SQL = "SELECT * FROM we_orders WHERE orderid = '" & nOrderId & "'"
session("errorSQL") = SQL
set oRsOrd = Server.CreateObject("ADODB.Recordset")
oRsOrd.open SQL, oConn, 3, 3
if oRsOrd.eof then ReceiptText = "<h3>Order" & nOrderId & " not found!</h3>"

'format the shipping and billing addresses
dim nShippingid
nShippingid = oRsOrd("shippingid")
if nShippingid > 0 then
	strSql = "SELECT * FROM we_addl_shipping_addr WHERE id='" & nShippingid & "'"
	session("errorSQL") = SQL
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open strSql, oConn, 3, 3
	if not RS.eof then
		saddress1 = RS("saddress1")
		saddress2 = RS("saddress2")
		sCity = RS("sCity")
		sstate = RS("sstate")
		szip = RS("szip")
		sCountry = RS("sCountry")
	end if
else
	saddress1 = oRsCust("saddress1")
	saddress2 = oRsCust("saddress2")
	sCity = oRsCust("sCity")
	sstate = oRsCust("sstate")
	szip = oRsCust("szip")
	sCountry = oRsCust("sCountry")
end if

testMsg = ""
if saddress1 = "4040 N. Palm St." then
	SQL = "select cancelled from we_Orders WHERE orderID = '" & nOrderId & "'"
	set checkOrderRS = oConn.execute(SQL)
	
	if not checkOrderRS.EOF then
		if isnull(checkOrderRS("cancelled")) then
			call cancelTestOrder(nOrderId)
		end if
		testMsg = "This is a test order and has been auto canceled"
	else
		testMsg = "Cancel Bypass:" & SQL
	end if
end if

sShipAddress = sAddress1
if sAddress2 <> "" then sShipAddress = sShipAddress & "<br>" & saddress2
sShipAddress = sShipAddress & "<br>" & sCity & ", " & sState & "&nbsp;" & sZip & "<br>" & sCountry

sBillAddress = oRsCust("bAddress1")
if oRsCust("bAddress2") <> "" then sBillAddress = sBillAddress & "<br>" & oRsCust("bAddress2")
sBillAddress = sBillAddress & "<br>" & oRsCust("bCity") & ", " & oRsCust("bState") & "&nbsp;" & oRsCust("bZip") & "<br>" & oRsCust("bCountry")

oRsCust.close
set oRsCust = nothing

if ReceiptText = "" then
	'format order info
	dim sShipType, nOrdershippingfee, nOrderTax, nBuysafeamount, extOrderType, extOrderNumber, nOrderDateTime, itemids
'	nOrderSubTotal = formatNumber(cDbl(oRsOrd("ordersubtotal")),2)
	emailSubTotal = formatNumber(cDbl(oRsOrd("ordersubtotal")),2)
	sShipType = oRsOrd("shiptype")
'	nOrdershippingfee = cDbl(oRsOrd("ordershippingfee"))
	emailShipFee = cDbl(oRsOrd("ordershippingfee"))
'	nOrderTax = oRsOrd("orderTax")
	emailOrderTax = oRsOrd("orderTax")
	nBuysafeamount = oRsOrd("BuySafeAmount")
'	nOrderGrandTotal = formatNumber(cDbl(oRsOrd("ordergrandtotal")),2)
	emailOrderGrandTotal = formatNumber(cDbl(oRsOrd("ordergrandtotal")),2)
	extOrderType = oRsOrd("extOrderType")
	extOrderNumber = oRsOrd("extOrderNumber")
	nOrderDateTime = oRsOrd("orderdatetime")
	'Free Product Offer code added 2/3/2010 by MC
	dim FreeProductOffer
	FreeProductOffer = oRsOrd("FreeProductOffer")
	shopRunnerID = oRsOrd("shopRunnerID")
	
	dim strOrderType
	select case extOrderType
		case 1 : strOrderType = "Paypal<br>" & extOrderNumber
		case 2 : strOrderType = "Google Checkout<br>" & extOrderNumber
		case 3
			strOrderType = "eBillme"
			eBillme = "eBillme"
			eBillmeAccount = extOrderNumber
		case 4 : strOrderType = "Ebay<br>" & extOrderNumber
		case 5 : strOrderType = "AtomicMall<br>" & extOrderNumber
		case 6 : strOrderType = "Amazon<br>" & extOrderNumber
		case 7 : strOrderType = "Buy.Com<br>" & extOrderNumber
		case else : strOrderType = "Credit Card"
	end select
	
	oRsOrd.close
	set oRsOrd = nothing
	
	'===================
	'NEW WEBLOYALTY CODE
	dim RSwl, cctype, country, pcode, strToEncrypt, strEncryptedValue
	SQL = "SELECT A.CCtype, B.bState, B.bZip FROM we_ccinfo A INNER JOIN we_Accounts B ON A.accountid=B.accountid WHERE A.orderid='" & nOrderID & "'"
	session("errorSQL") = SQL
	set RSwl = Server.CreateObject("ADODB.Recordset")
	RSwl.open SQL, oConn, 3, 3
	if not RSwl.eof then
		cctype = RSwl("CCtype")
		if len(RSwl("bState")) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",RSwl("bState")) > 0 then
			country = "CANADA"
		else
			country = "US"
		end if
		pcode = RSwl("bZip")
	end if
	RSwl.close
	set RSwl = nothing
	
	' Put together string ?concatenate all required values
	if incEmail = true then
		dim yesexiturl, noexiturl
		yesexiturl = "https://" & request.ServerVariables("SERVER_NAME") & "/confirm?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderGrandTotal & "&c=" & emailSubTotal
		noexiturl = yesexiturl
		strToEncrypt = "cref" & chr(255) & nOrderID & chr(255) & "cctype" & chr(255) & cctype & chr(255) & "country" & chr(255) & country & chr(255) & "pcode" & chr(255) & pcode & chr(255) & "yesexiturl" & chr(255) & yesexiturl & chr(255) & "noexiturl" & chr(255) & noexiturl
	else
		strToEncrypt = "cref" & chr(255) & nOrderID & chr(255) & "cctype" & chr(255) & cctype & chr(255) & "country" & chr(255) & country & chr(255) & "pcode" & chr(255) & pcode
	end if
	' Encrypt it
	strEncryptedValue = SimpleEncrypt(strToEncrypt)
	'===================
	
	ReceiptText = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">" & vbcrlf
	ReceiptText = ReceiptText & "<html>" & vbcrlf
	ReceiptText = ReceiptText & "<head>" & vbcrlf
	ReceiptText = ReceiptText & "<title>Thank you for your purchase on WirelessEmporium.com</title>" & vbcrlf
	ReceiptText = ReceiptText & "<style type=""text/css"">" & vbcrlf
	ReceiptText = ReceiptText & "<!--" & vbcrlf
	ReceiptText = ReceiptText & ".regText {font-family: Arial,Helvetica,Sans-serif; font-size: 9pt;}" & vbcrlf
	ReceiptText = ReceiptText & ".header {font-family: Arial,Helvetica,Sans-serif; font-size: 16pt;}" & vbcrlf
	ReceiptText = ReceiptText & ".totals {font-family: Arial,Helvetica,Sans-serif; font-size: 11pt;}" & vbcrlf
	ReceiptText = ReceiptText & ".footer a:visited { color: #000000 !important; }" & vbcrlf
	ReceiptText = ReceiptText & ".footer a:hover { color: #000000 !important; }" & vbcrlf
	ReceiptText = ReceiptText & ".footer a:active { color: #000000 !important; }" & vbcrlf
	ReceiptText = ReceiptText & ".header1 a:visited { color: #000000 !important; }" & vbcrlf
	ReceiptText = ReceiptText & ".header1 a:hover { color: #000000 !important; }" & vbcrlf
	ReceiptText = ReceiptText & ".header1 a:active { color: #000000 !important; }" & vbcrlf
	ReceiptText = ReceiptText & "a:visited { color: #0099FF !important; }" & vbcrlf
	ReceiptText = ReceiptText & "a:hover { color: #0099FF !important; }" & vbcrlf
	ReceiptText = ReceiptText & "a:active { color: #0099FF !important; }" & vbcrlf
	ReceiptText = ReceiptText & "-->" & vbcrlf
	ReceiptText = ReceiptText & "</style>" & vbcrlf
	ReceiptText = ReceiptText & "</head>" & vbcrlf
	ReceiptText = ReceiptText & "<body class=""regText"">" & vbcrlf
	
	if session("mailError") <> "" then ReceiptText = ReceiptText & "<p>" & session("mailError") & "</p>" & vbcrlf
	
	ReceiptText = ReceiptText & "<table width=""900"" cellpadding=""8"" cellspacing=""0"" align=""center"" class=""regText"">" & vbcrlf
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "<td width=""200""><a href=""http://" & request.ServerVariables("SERVER_NAME") & "/""><img src=""//www.wirelessemporium.com/images/WElogo.gif"" border=""0"" alt=""WirelessEmporium.com"" hspace=""12"" align=""absmiddle""></a></td>" & vbcrlf
	ReceiptText = ReceiptText & "<td class=""header"" align=""left""><b>Wireless Emporium Order Confirmation</b></td>" & vbcrlf
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	
	if testMsg <> "" then
		ReceiptText = ReceiptText & "<tr>" & vbcrlf
		ReceiptText = ReceiptText & "<td colspan=""2"" align=""center"">" & testMsg & "</td>" & vbcrlf
		ReceiptText = ReceiptText & "</tr>" & vbcrlf
	end if
	
	ReceiptText = ReceiptText & "</table>" & vbcrlf
	ReceiptText = ReceiptText & "<table width=""950"" cellpadding=""5"" cellspacing=""1"" align=""center"" class=""regText"">" & vbcrlf
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "<td colspan=""2""><p><b>Thank you for your purchase on <a href=""http://" & request.ServerVariables("SERVER_NAME") & "/"">WirelessEmporium.com</a>.</b><br>Here are the details of your order:</td>" & vbcrlf
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	
	'Gift Certificate
	'if emailSubTotal > 10 then
	'	dim GiftCertificateCode, giftAmount
	'	GiftCertificateCode = ""
	'	giftAmount = 5
	'	call GiftCertificate(nOrderId,giftAmount)
	'	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	'	ReceiptText = ReceiptText & "<td colspan=""2""><p><b>You have earned a " & formatCurrency(giftAmount) & " Gift Certificate! Your Gift Certificate number is:<br>" & GiftCertificateCode & "</b></p></td>" & vbcrlf
	'	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	'end if
	
	'START eBillme section
	if eBillme = "eBillme" then
		eBillmeAccountArray = split(eBillmeAccount," | ")
		if Ubound(eBillmeAccountArray) > 0 then
			eBillmeAccount = eBillmeAccountArray(1)
			ReceiptText = ReceiptText & "<tr><td colspan=""2"">" & vbcrlf & _
			"<table cellspacing=""0"" border=""0"" cellpadding=""0"" align=""center"" width=""720"" style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"">" & vbcrlf & _
			"  <tr>" & vbcrlf & _
			"	<td background=""https://content.wupaygateway.com/wupay/confirm/images/header.png"" height=""37"" valign=""middle"" style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"" colspan=""2""><table cellspacing=""0"" border=""0"" cellpadding=""0"" width=""720"">" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td class=""header1"" style=""font-size: 12px;  font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;"" width=""24""> </td>" & vbcrlf & _
			"		<td class=""header1"" style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;"" width=""221""><strong>WU&reg; Pay&trade; - Your Bill </strong></td>" & vbcrlf & _
			"		<td class=""header1"" style=""font-size: 12px;  font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;"" width=""454""><div align=""right""><a target=""_blank"" href=""http://livechat.boldchat.com/aid/8900659474086935388/bc.chat?cwdid=8262818297912230602"" style=""text-decoration: underline; color: #000000;"">Live Chat</a> | <a href=""http://www.westernunion.com/wupay/learn/faq"" style=""text-decoration: underline; color: #000000;"" target=""_blank"">FAQ</a></div></td>" & vbcrlf & _
			"		<td class=""header1"" style=""font-size: 12px;  font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;"" width=""21""> </td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	</table>" & vbcrlf & _
			"   </td>" & vbcrlf & _
			"  </tr>" & vbcrlf & _
			"  <tr>" & vbcrlf & _
			"	<td class=""cells"" valign=""top"" style=""font-size: 12px; padding: 3px 9px 8px 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"" width=""500""><table cellspacing=""0"" border=""0"" cellpadding=""2"" align=""center"" width=""90%"">" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;""><span class=""title"" style=""font-size: 23px; padding-top: 8px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"">This is your Bill</span>" & vbcrlf & _
			"		  <p class=""blueheader"" style=""font-weight: bold; font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #00000;"">" & sFname & " " & sLname & ",</p>" & vbcrlf & _
			"		  <p class=""para"" style=""font-size: 12px; line-height: 14px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;""> Thank you for using WU&reg; Pay as your payment method. Please note:<br> " & vbcrlf & _
			"		  A copy of this bill will be emailed to <strong>" & sEmail & "</strong></p></td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	</table>" & vbcrlf & _
			"	</td>" & vbcrlf & _
			"	<td class=""logos"" valign=""bottom"" style=""font-size: 12px; padding: 8px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; text-align: center;"" width=""220""><div align=""center""><img src=""https://content.wupaygateway.com/wupay/confirm/images/wupay-logo.png"" alt=""WU&lt;sup&gt;®&lt;/sup&gt; Pay logo"" width=""149""><br>" & vbcrlf & _
			"	  <br>" & vbcrlf & _
			"	  </div>      " & vbcrlf & _
			"	  <div align=""left""><span class=""style31"" style=""font-size: 13px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #00000;""><span class=""blueheader"" style=""font-weight: bold; font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #00000;"">When will my order ship?</span></span><span class=""blueheader"" style=""font-weight: bold; font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #00000;""><br>" & vbcrlf & _
			"	</span><span class=""sidebarsmall"" style=""font-size: 11px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000;"">Once payment is received,  WU&reg; Pay will authorize your  to ship your order.</span></div></td>" & vbcrlf & _
			"  </tr>" & vbcrlf & _
			"  <tr>" & vbcrlf & _
			"	<td style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"" colspan=""2""><img src=""https://content.wupaygateway.com/wupay/confirm/images/dividers1top.png"" height=""3"" alt=""divider top"" width=""720""></td>" & vbcrlf & _
			"  </tr>" & vbcrlf & _
			"  <tr>" & vbcrlf & _
			"	<td class=""divider"" style=""padding-bottom: 5px; font-size: 15px; background: #ededed; padding-top: 5px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000; padding-left: 24px;"" colspan=""2""><div align=""left"">To " & vbcrlf & _
			"		complete your order please pay WU&reg; Pay" & vbcrlf & _
			"		" & formatCurrency(emailOrderGrandTotal,2) & "</div></td>" & vbcrlf & _
			"  </tr>" & vbcrlf & _
			"  <tr>" & vbcrlf & _
			"	<td style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"" colspan=""2""><img src=""https://content.wupaygateway.com/wupay/confirm/images/dividers1bottom.png"" height=""3"" alt=""divider bottom"" width=""720""></td>" & vbcrlf & _
			"  </tr>" & vbcrlf & _
			"  <tr>" & vbcrlf & _
			"	<td class=""cells"" style=""font-size: 12px; padding: 3px 9px 8px 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"" width=""500""><table cellspacing=""0"" border=""0"" cellpadding=""0"" align=""center"" width=""90%"">" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td valign=""bottom"" style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"" width=""85%""><strong>Go to your bank's website, and pay WU&reg; Pay.</strong> <br>" & vbcrlf & _
			"			<br>" & vbcrlf & _
			"			<strong>First time users:</strong> You will require the following information to <br>" & vbcrlf & _
			"			setup WU&reg; Pay as a payee:</td>" & vbcrlf & _
			"		<td valign=""bottom"" style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"" width=""15%""><div align=""right""><a href=""JavaScript:window.print();"" class=""style26"" style=""font-size: 10px; color: #0099FF;""><img src=""https://content.wupaygateway.com/wupay/confirm/images/print.png"" border=""0"" height=""16"" alt=""print"" align=""absmiddle"" width=""16"">print bill</a></div></td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	</table>" & vbcrlf & _
			"	  <br>" & vbcrlf & _
			"	  <div align=""right""><a href=""JavaScript:window.print();"" class=""style26"" style=""font-size: 10px; color: #0099FF;"">" & vbcrlf & _
			"	</a></div>    " & vbcrlf & _
			"	<table cellspacing=""0"" border=""0"" cellpadding=""7"" align=""center"" width=""90%"">" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><div class=""style30"" align=""right"" style=""color: #000000;"">" & vbcrlf & _
			"			<div align=""right""><span class=""bank"" style=""font-size: 13px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"">Payee Name:</span><br>" & vbcrlf & _
			"				<span class=""weeno"" style=""font-size: 10px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;"">Do not include merchant name</span></div>" & vbcrlf & _
			"		</div></td>" & vbcrlf & _
			"		<td bgcolor=""#f1f9fe"" class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><strong>WU Pay</strong></td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;"" width=""50%""><div class=""style30"" align=""right"" style=""color: #000000;"">" & vbcrlf & _
			"			<div align=""right""><span class=""bank"" style=""font-size: 13px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"">Your Account Number:</span><br>" & vbcrlf & _
			"			  <span class=""weeno"" style=""font-size: 10px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;"">Please enter as shown</span></div>" & vbcrlf & _
			"		</div></td>" & vbcrlf & _
			"		<td bgcolor=""#f1f9fe"" class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;"" width=""50%""><strong>" & eBillmeAccount & "</strong></td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><div class=""style30"" align=""right"" style=""color: #000000;"">" & vbcrlf & _
			"			<div class=""bank"" align=""right"" style=""font-size: 13px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"">Payee Address:</div>" & vbcrlf & _
			"		</div></td>" & vbcrlf & _
			"		<td bgcolor=""#f1f9fe"" class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><strong>P.O. Box 635808<br>" & vbcrlf & _
			"		  Cincinnati, OH</strong></td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><div class=""style30"" align=""right"" style=""color: #000000;"">" & vbcrlf & _
			"			<div align=""right""><span class=""bank"" style=""font-size: 13px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"">Payee ZIP:</span><br>" & vbcrlf & _
			"				<span class=""weeno"" style=""font-size: 10px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;"">Please enter all 9 digits</span></div>" & vbcrlf & _
			"		</div></td>" & vbcrlf & _
			"		<td bgcolor=""#f1f9fe"" class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><strong>45263-5808</strong></td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><div class=""style30"" align=""right"" style=""color: #000000;"">" & vbcrlf & _
			"			<div class=""bank"" align=""right"" style=""font-size: 13px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"">Payee Phone Number:</div>" & vbcrlf & _
			"		</div></td>" & vbcrlf & _
			"		<td bgcolor=""#f1f9fe"" class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><strong>1.888.899.6633</strong></td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	</table>" & vbcrlf & _
			"	<br>" & vbcrlf & _
			"	</td>" & vbcrlf & _
			"	<td class=""logos"" style=""font-size: 12px; padding: 8px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; text-align: center;""><p class=""blueheader"" align=""left"" style=""font-weight: bold; font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #00000;"">How to ensure the fastest <br>" & vbcrlf & _
			"  payment processing:</p>" & vbcrlf & _
			"	  <p class=""sidebarsmall"" align=""left"" style=""font-size: 11px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000;""><strong>Enter all information accurately:</strong><br>" & vbcrlf & _
			"		 Enter WU&reg; Pay as the Payee Name<br>" & vbcrlf & _
			"		 Enter your WU&reg; Pay account number<br>" & vbcrlf & _
			"		 Enter WU&reg; Pay's complete address<br>" & vbcrlf & _
			"		 Enter WU&reg; Pay's 9 digit ZIP</p>" & vbcrlf & _
			"	  <p class=""sidebarsmall"" align=""left"" style=""font-size: 11px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000;""><strong class=""sidebarsmall"" style=""font-size: 11px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000;"">Useful tip:</strong><br>" & vbcrlf & _
			"		Use 'cut and paste' to enter payee information correctly.</p>" & vbcrlf & _
			"	  <p class=""sidebarsmall"" align=""left"" style=""font-size: 11px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000;""><strong><a href=""http://www.westernunion.com/wupay/learn/bill-pay"" style=""color: #0099FF;"" target=""_blank"">Read step by step instructions</a></strong> <br>" & vbcrlf & _
			"	  on how to set up WU&reg; Pay as a payee</p>" & vbcrlf & _
			"	  <p class=""sidebarsmall"" align=""left"" style=""font-size: 11px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000;""><strong><a href=""http://www.westernunion.com/wupay/redirect.php?http%3A%2F%2Flivechat.boldchat.com%2Faid%2F8900659474086935388%2Fbc.chat%3Fcwdid%3D8262818297912230602"" style=""color: #0099FF;"" target=""_blank"">Still need help?</a></strong><br>" & vbcrlf & _
			"	  </p>" & vbcrlf & _
			"  </td></tr>" & vbcrlf & _
			"  <tr>" & vbcrlf & _
			"	<td bgcolor=""#007ec7"" class=""footer"" valign=""top"" style=""font-size: 12px; background: no-repeat; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #FFFFFF; vertical-align: top;"" colspan=""2""><strong><img src=""https://content.wupaygateway.com/wupay/confirm/images/footer.png"" height=""11"" alt=""footer1"" width=""720""><br>" & vbcrlf & _
			"	</strong>" & vbcrlf & _
			"  </td>" & vbcrlf & _
			"  </tr>" & vbcrlf & _
			"</table>" & vbcrlf & _
			"</td></tr>" & vbcrlf

'============ OLD eBillMe		
'			ReceiptText = ReceiptText & "<tr><td colspan=""2""><table width=""600"" border=""0"" cellpadding=""5"" cellspacing=""0"" align=""center"" style=""border: 1px solid black;"">" & vbcrlf
'			ReceiptText = ReceiptText & "<tr><td colspan=""3"" bgcolor=""#006699"" height=""25""><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #FFFFFF; font-weight: bold"">To receive your order:</p></td></tr>" & vbcrlf
'			ReceiptText = ReceiptText & "<tr><td colspan=""2""><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">Go to your bank's website, setup and pay eBillme " & formatCurrency(emailOrderGrandTotal,2) & "</p>" & vbcrlf
'			ReceiptText = ReceiptText & "<p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal""><em>First time users may require the following information:</em></p></td>" & vbcrlf
'			ReceiptText = ReceiptText & "<td rowspan=""5"" align=""center"" width=""150""><p align=""center""><img src=""https://" & request.ServerVariables("SERVER_NAME") & "/cart/process/eBillme/images/eBillme-tag_logo2.jpg"" width=""147"" height=""47"" border=""0"" alt=""eBillme - My Bank, My Way""></p>" & vbcrlf
'			ReceiptText = ReceiptText & "<p align=""center""><a href=""https://www.ebillme.com/index.php/learnmore2/wirelessemporium"" target=""_blank""><img src=""https://" & request.ServerVariables("SERVER_NAME") & "/cart/process/eBillme/images/learnmore-button.jpg"" width=""103"" height=""38"" border=""0"" alt=""Need Help?""></a><br>" & vbcrlf
'			ReceiptText = ReceiptText & "<a href=""https://www.ebillme.com/index.php/learnmore2/wirelessemporium"" class=""regText"" target=""_blank"">Need Help?</a></p></td></tr>" & vbcrlf
'			ReceiptText = ReceiptText & "<tr><td><p style=""text-align:right; font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal"">Account&nbsp;Number:</p></td>" & vbcrlf
'			ReceiptText = ReceiptText & "<td width=""350""><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">" & eBillmeAccount & "</p></td></tr>" & vbcrlf
'			ReceiptText = ReceiptText & "<tr><td><p style=""text-align:right; font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal"">Payee&nbsp;Name:</p></td>" & vbcrlf
'			ReceiptText = ReceiptText & "<td><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">eBillme</p></td></tr>" & vbcrlf
'			ReceiptText = ReceiptText & "<tr><td><p style=""text-align:right; font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal"">Payee&nbsp;Address:</p></td>" & vbcrlf
'			ReceiptText = ReceiptText & "<td><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">PO&nbsp;Box:&nbsp;635808<br>Cincinnati,&nbsp;OH&nbsp;45263-5808</p></td></tr>" & vbcrlf
'			ReceiptText = ReceiptText & "<tr><td><p style=""text-align:right; font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal"">Payee&nbsp;Phone&nbsp;Number:</p></td>" & vbcrlf
'			ReceiptText = ReceiptText & "<td><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">1.866.365.6632</p></td></tr>" & vbcrlf
'			ReceiptText = ReceiptText & "</table></td></tr>" & vbcrlf
		end if
	end if
	'END eBillme section
	
	'format customer info
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" colspan=""2"" bgcolor=""#EAEAEA"" class=""header""><p>&nbsp;&nbsp;&nbsp;Customer Information:</p></td>" & vbcrlf
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" colspan=""2"">" & vbcrlf
	ReceiptText = ReceiptText & "<table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"" class=""regText"">" & vbcrlf
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b>Name:</b><br>" & sFname & " " & sLname & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top""><b>Paid By:</b><br>" & strOrderType & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3"">&nbsp;</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b>Email Address:</b><br><a href=""mailto:" & sEmail & """>" & sEmail & "</a>" & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "<td><b>Phone:</b><br>" & sPhone & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3"">&nbsp;</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b>Billing Address:</b><br>" & sBillAddress & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "<td>" & vbcrlf
	ReceiptText = ReceiptText & "	<b>Shipping Address:</b><br>" & sShipAddress & vbcrlf
	ReceiptText = ReceiptText & "	<br /><br />" & vbcrlf	
	ReceiptText = ReceiptText & "	<div style=""color:#f00; font-size:11px;"">**Please review your shipping address. <br />If it is incorrect please contact us immediately at <a href=""mailto:updates@wirelessemporium.com"">updates@wirelessemporium.com</a>**</div>" & vbcrlf
	ReceiptText = ReceiptText & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3"">&nbsp;</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "</table>" & vbcrlf
	ReceiptText = ReceiptText & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	
	'format order information
	dim oRsOrderDetails, sSqlDetails, thisSubtotal, mySubtotal, nCount, nID, nPartNumber, nQty, nPrice
	mySubtotal = 0
	nCount = 1
	if not isNumeric(emailOrderTax) or emailOrderTax = "" or isNull(emailOrderTax) then emailOrderTax = "0"
	emailOrderTax = cDbl(emailOrderTax)
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" colspan=""2"" bgcolor=""#EAEAEA"" class=""header""><p>&nbsp;&nbsp;&nbsp;Order Details:</p></td>" & vbcrlf
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" colspan=""2"">" & vbcrlf
	ReceiptText = ReceiptText & "<table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"" class=""regText"">" & vbcrlf
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b>Order ID:</b></td>" & vbcrlf
	ReceiptText = ReceiptText & "<td>" & nOrderId & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3""><br><hr></td></tr>" & vbcrlf
	
	' Item Details
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b>Items:</b></td>" & vbcrlf
	ReceiptText = ReceiptText & "<td>" & vbcrlf
	sSqlDetails = "SELECT d.brandName, e.modelName, A.itemid,A.PartNumber,A.itemdesc,A.price_our,A.itemPic,c.id,c.musicSkinsID,c.artist + '-' + c.designName as prodDesc,c.price_we,B.quantity, a.subtypeid FROM we_orderdetails B left join we_items A ON B.itemid = A.itemid left join we_items_musicSkins C on B.itemID = C.id left join we_brands d on a.brandID = d.brandID left join we_models e on a.modelID = e.modelID WHERE B.itemID > 0 and B.orderid = '" & nOrderId & "'"
	session("errorSQL") = sSqlDetails
	set oRsOrderDetails = oConn.execute(sSqlDetails)
	if not oRsOrderDetails.eof then
		do until oRsOrderDetails.eof
			jsIdCounter = jsIdCounter + 1
			if inStr(oRsOrderDetails("PartNumber"),"WCD-") then ' is Custom Case
				customImgOnly = oRsOrderDetails("itemPic")
				customImgUrl = "http://www.wirelessemporium.com/productpics/big/" & oRsOrderDetails("itemPic")
				customPageUrl = "http://" & request.ServerVariables("HTTP_HOST") & "/custom-case-" & oRsOrderDetails("itemid")
				'Styles are located in confirm
				'Open "divSocContainer" Div
				shareBuilder = "<div class=""divSocContainer"">"
					'"Share" Button
					shareBuilder = shareBuilder & "<div onclick=""document.getElementById('customPop_" & jsIdCounter & "').style.display='block'"" class=""divShare"">Share!</div>"
					'Open "Menu" Div
					shareBuilder = shareBuilder & "<div id=""customPop_" & jsIdCounter & """ class=""divMenu"">"
						'"Close" Button
						shareBuilder = shareBuilder & "<div onclick=""document.getElementById('customPop_" & jsIdCounter & "').style.display='none'"" class=""divClose"">Close</div>"
						'"Twitter" Div
						shareBuilder = shareBuilder & "<div class=""divTwitter""><a href=""https://twitter.com/share"" class=""twitter-share-button"" data-url=""" & customPageUrl & """ data-text=""My custom case is on its way"" data-via=""WirelessEmp"" data-count=""none"">Tweet</a></div>"
						'"Facebook" Div
						shareBuilder = shareBuilder & "<div class=""divFacebook""><div id=""fb-root""></div><div class=""fb-like"" data-href=""" & customPageUrl & """ data-send=""false"" data-layout=""button_count"" data-width=""50"" data-show-faces=""false"" data-font=""verdana""></div></div>"
						'"Pinterest" Div
						shareBuilder = shareBuilder & "<div class=""divPinterest""><a href=""http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.wirelessemporium.com%2Fcustom-phone-cases&media=http%3A%2F%2Fwww.wirelessemporium.com%2Fproductpics%2Fbig%2F" & customImgOnly & "&description=I%20customized%20my%20phone%20cover%20at%20WirelessEmporium.com"" class=""pin-it-button"" count-layout=""none""><img border=""0"" src=""//assets.pinterest.com/images/PinExt.png"" title=""Pin It"" /></a></div>"
					'Close "Menu" Div
					shareBuilder = shareBuilder & "</div>"
				'Close "divSocContainer" Div
				shareBuilder = shareBuilder & "</div>"
				

				if socialShareOn = true then 'Only shown in needed pages, such as confirm; will not function properly in email 
					ReceiptText = ReceiptText & shareBuilder
				else
					'ReceiptText = ReceiptText 'Do Nothing
				end if
			end if
			
			
			
			if isnull(oRsOrderDetails("itemid")) then
				nID = oRsOrderDetails("id")
				nPartNumber = oRsOrderDetails("musicSkinsID")
				nDesc = oRsOrderDetails("prodDesc")
				nQty = oRsOrderDetails("quantity")
				nPrice = oRsOrderDetails("price_we")
			else
				useBrandName = oRsOrderDetails("brandName")
				useModelName = oRsOrderDetails("modelName")
				nID = oRsOrderDetails("itemid")
				nPartNumber = oRsOrderDetails("PartNumber")
				partNumber = nPartNumber
				nDesc = insertDetailsAdv(oRsOrderDetails("itemdesc"),useBrandName,useModelName)
				nQty = oRsOrderDetails("quantity")
				nPrice = oRsOrderDetails("price_our")
				if itemids = "" then
					itemids = nID
				else
					itemids = itemids & "," & nID
				end if
			end if
			ReceiptText = ReceiptText & nCount & ": <a href=""http://" & request.ServerVariables("SERVER_NAME") & "/p-" & nID & "-" & formatSEO(nDesc) & """>" & nDesc & "</a> [" & nPartNumber & "]"
			ReceiptText = ReceiptText & "<br>&nbsp;&nbsp;&nbsp;Quantity: " & nQty & " | Price: " & formatCurrency(prepInt(nPrice),2)
			for MYcount = 1 to nQty
				MYnID = MYnID & nID & ","
			next
			thisSubtotal = formatNumber(cDbl(nQty) * cDbl(prepInt(nPrice)),2)
			ReceiptText = ReceiptText & "&nbsp;|&nbsp;&nbsp;Subtotal: " & thisSubtotal & "<br />" & vbcrlf

			if isDropShip(nPartNumber) then
				ReceiptText = ReceiptText & "&nbsp;&nbsp;&nbsp;<strong>This Item Will Ship Separately</strong><br />" & vbcrlf
			end if
			
			if inStr(oRsOrderDetails("PartNumber"),"WCD-") or prepInt(oRsOrderDetails("subtypeid")) = 1031 then
				ReceiptText = ReceiptText & "&nbsp;&nbsp;&nbsp;<strong>Please allow up to " & gblCustomShippingDate & " business days for production</strong><br /><br />" & vbcrlf
			else
				ReceiptText = ReceiptText & "<br />" & vbcrlf
			end if
			ReceiptText = ReceiptText & " <script> " & vbcrlf
			ReceiptText = ReceiptText &	" window.WEDATA.orderData.cartItems.push({" & vbcrlf 
			ReceiptText = ReceiptText & " partNumber: '" & nPartNumber & "'," & vbcrlf
			ReceiptText = ReceiptText & " qty: " & nQty & ", " & vbcrlf
			ReceiptText = ReceiptText &	" price: " & nPrice & ", " & vbcrlf
			ReceiptText = ReceiptText & " itemId: " & nID & vbcrlf 
			ReceiptText = ReceiptText &	" });" & vbcrlf
			ReceiptText = ReceiptText &	"</script>" & vbcrlf

			
			nCount = nCount + 1
			nTotQty = nTotQty + nQty
			mySubtotal = mySubtotal + thisSubtotal
			oRsOrderDetails.movenext
		loop
	end if
	oRsOrderDetails.close
	set oRsOrderDetails = nothing
	ReceiptText = ReceiptText & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3""><br><hr></td></tr>" & vbcrlf
	
	'Free Product Offer code added 2/3/2010 by MC
	'Modified for AdLucent promo 2/24/2010 by MC
	select case FreeProductOffer
		case "10848"
			if emailOrderGrandTotal - emailShipFee - emailOrderTax >= 30 then
				ReceiptText = ReceiptText & "<tr>" & vbcrlf
				ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
				ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b>FREE GIFT:</b></td>" & vbcrlf
				ReceiptText = ReceiptText & "<td>Universal PDA & Cell Phone Screen Protector</td>" & vbcrlf
				ReceiptText = ReceiptText & "</tr>" & vbcrlf
				ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3""><br><hr></td></tr>" & vbcrlf
			end if
		case "21124"
			if emailOrderGrandTotal - emailShipFee - emailOrderTax >= 30 and emailOrderGrandTotal - emailShipFee - emailOrderTax >= 49.99 then
				ReceiptText = ReceiptText & "<tr>" & vbcrlf
				ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
				ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b>FREE GIFT:</b></td>" & vbcrlf
				ReceiptText = ReceiptText & "<td>USB Car Charger Adapter</td>" & vbcrlf
				ReceiptText = ReceiptText & "</tr>" & vbcrlf
				ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3""><br><hr></td></tr>" & vbcrlf
			end if
		case "36800"
			if emailOrderGrandTotal - emailShipFee - emailOrderTax >= 50 then
				ReceiptText = ReceiptText & "<tr>" & vbcrlf
				ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
				ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b>FREE GIFT:</b></td>" & vbcrlf
				ReceiptText = ReceiptText & "<td>Universal 2-in-1 Car/Home Charger</td>" & vbcrlf
				ReceiptText = ReceiptText & "</tr>" & vbcrlf
				ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3""><br><hr></td></tr>" & vbcrlf
			end if
	end select

	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b>Order Subtotal:</b></td>" & vbcrlf
	ReceiptText = ReceiptText & "<td>" & formatCurrency(emailSubTotal,2) & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3""><br><hr></td></tr>" & vbcrlf
	
	if discountTotal > 0 then 'changed from sPromoCode to discountTotal because sPromoCode is wiped out by promoFunctions 'discountTotal <> "" then
		ReceiptText = ReceiptText & "<tr>" & vbcrlf
		ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
		ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b>Discount (" & sPromoCode & "):</b></td>" & vbcrlf
		ReceiptText = ReceiptText & "<td>(" & formatcurrency(discountTotal) & ")</td>" & vbcrlf 'No longer show promo code sPromoCode 'sPromoCode is set to "" by promoFunctions
		ReceiptText = ReceiptText & "</tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3""><br><hr></td></tr>" & vbcrlf
	end if
	
	'if emailOrderGrandTotal - emailSubTotal - emailOrderTax - emailShipFee < 0 then
	if emailOrderGrandTotal - mySubtotal - emailOrderTax - emailShipFee - nBuysafeamount < 0 then
		ReceiptText = ReceiptText & "<tr>" & vbcrlf
		ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
		ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b>Discount:</b></td>" & vbcrlf
		'ReceiptText = ReceiptText & "<td>" & formatCurrency(emailOrderGrandTotal - emailSubTotal - emailOrderTax - emailShipFee,2) & "</td>" & vbcrlf
		ReceiptText = ReceiptText & "<td>" & formatCurrency(emailOrderGrandTotal - mySubtotal - emailOrderTax - emailShipFee - nBuysafeamount,2) & "</td>" & vbcrlf
		'ReceiptText = ReceiptText & emailOrderGrandTotal & " - " & mySubtotal & " - " & emailOrderTax & " - " & emailShipFee & "</td>" & vbcrlf
		ReceiptText = ReceiptText & "</tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3""><br><hr></td></tr>" & vbcrlf
	end if
	
	if emailOrderTax > 0 then
		ReceiptText = ReceiptText & "<tr>" & vbcrlf
		ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
		ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b>CA Resident Tax (" & Application("taxDisplay") & "%):</b></td>" & vbcrlf
		ReceiptText = ReceiptText & "<td>" & formatCurrency(emailOrderTax,2) & "</td>" & vbcrlf
		ReceiptText = ReceiptText & "</tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3""><br><hr></td></tr>" & vbcrlf
	end if
	
	if nBuysafeamount > 0 then
		ReceiptText = ReceiptText & "<tr>" & vbcrlf
		ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
		ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b><a href=""http://www.buysafe.com/questions"" target=""_blank"">buySAFE&nbsp;Bond&nbsp;Guarantee</a>:</b></td>" & vbcrlf
		ReceiptText = ReceiptText & "<td>" & formatCurrency(nBuysafeamount,2) & "</td>" & vbcrlf
		ReceiptText = ReceiptText & "</tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3""><br><hr></td></tr>" & vbcrlf
	end if
	
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b>Shipping Type:</b></td>" & vbcrlf
	ReceiptText = ReceiptText & "<td>" & sShipType & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3""><br><hr></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
	ReceiptText = ReceiptText & "<td valign=""top"" width=""350""><b>Shipping & Handling Fee:</b></td>" & vbcrlf
	if prepStr(shopRunnerID) <> "" then
		if prepInt(emailReceipt) = 1 then
	ReceiptText = ReceiptText & "<td>Free ShopRunner 2-Day</td>" & vbcrlf
		else
	ReceiptText = ReceiptText & "<td><div name=""sr_cartSummaryDiv""></div></td>" & vbcrlf
		end if
	else
	ReceiptText = ReceiptText & "<td>" & formatCurrency(emailShipFee,2) & "</td>" & vbcrlf
	end if
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	
	ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3""><br><hr size=""12""></td></tr>" & vbcrlf

	if (emailOrderGrandTotal - nOrderGrandTotal) > 1 and nOrderGrandTotal > 0 then
		ReceiptText = ReceiptText & "<tr>" & vbcrlf
		ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
		ReceiptText = ReceiptText & "<td valign=""top"" width=""350"" class=""totals""><b>Grand Total:</b></td>" & vbcrlf
		ReceiptText = ReceiptText & "<td>" & formatCurrency(emailOrderGrandTotal,2) & "</td>" & vbcrlf
		ReceiptText = ReceiptText & "</tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr>" & vbcrlf
		ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
		ReceiptText = ReceiptText & "<td valign=""top"" width=""350"" class=""totals"">&nbsp;</td>" & vbcrlf
		ReceiptText = ReceiptText & "<td style=""padding:5px;"" align=""left""><div style=""padding:5px; background-color:#f2f2f2; color:#000; font-weight:bold;"">" & _
									"Thank you for your order! Please remember that you will see two charges on your account that total " & formatCurrency(emailOrderGrandTotal,2) & ". " & _
									"One charge in the amount of " & formatCurrency((emailOrderGrandTotal - nOrderGrandTotal),2) & " and the other in the amount of " & formatCurrency(nOrderGrandTotal,2) & _
									"</div></td></tr>" & vbcrlf
	else
		ReceiptText = ReceiptText & "<tr>" & vbcrlf
		ReceiptText = ReceiptText & "<td valign=""top"" width=""50"">&nbsp;</td>" & vbcrlf
		ReceiptText = ReceiptText & "<td valign=""top"" width=""350"" class=""totals""><b>Grand Total:</b></td>" & vbcrlf
		ReceiptText = ReceiptText & "<td>" & formatCurrency(emailOrderGrandTotal,2) & "</td>" & vbcrlf
		ReceiptText = ReceiptText & "</tr>" & vbcrlf
	end if

	ReceiptText = ReceiptText & "<tr><td valign=""top"" colspan=""3""><br><hr size=""12""></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	if prepStr(shopRunnerID) <> "" then
		ReceiptText = ReceiptText & "<td align=""center"" colspan=""3""><div name=""sr_checkoutSRItemsPageDiv""></div></td>" & vbcrlf
	else
		ReceiptText = ReceiptText & "<td align=""center"" colspan=""3""><div name=""sr_checkoutPageDiv""></div></td>" & vbcrlf
	end if
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	
	ReceiptText = ReceiptText & "</table>" & vbcrlf
	ReceiptText = ReceiptText & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	
	if InvoiceType <> "Admin" then
		ReceiptText = ReceiptText & "<tr><td>" & vbcrlf
		
		'ShopRunner
		if shopRunnerID = "" then
			ReceiptText = 	ReceiptText & "<p align=""left"">Next time get FREE 2-Day Shipping from WirelessEmporium.com and other " &_
							"great retailers with your ShopRunner membership. Sign up for Fast " &_
							"Free Shipping at https://" & request.ServerVariables("SERVER_NAME") & "/shoprunner.</p>" & vbcrlf
		else
			ReceiptText = 	ReceiptText & "<p align=""left"">Thanks for using FREE 2-Day Shipping by ShopRunner!<br />" &_
							"For returns, visit https://www.shoprunner.com/returns to print your" &_
							"pre-paid return label. Don't forget, invite a Friend to join.</p>" & vbcrlf
		end if
		
		' THANK YOU
		ReceiptText = ReceiptText & "<table width=""100%"" cellpadding=""5"" cellspacing=""0"" border=""0"" align=""center""><tr><td colspan=""2"" class=""regText"">" & vbcrlf
		ReceiptText = ReceiptText & "<p align=""left"">To ensure delivery of our emails to your inbox, please add service@WirelessEmporium.com to your Address Book. Thank You." & vbcrlf
		ReceiptText = ReceiptText & "<p align=""left"">We appreciate your business. Upon shipment out of our warehouse, <strong>you will receive an order shipment confirmation email</strong>. If you have any additional questions regarding your order, please email our customer service department at: <a href='mailto:service@wirelessemporium.com'>service@wirelessemporium.com</a>, and be sure to include your full name and order I.D. number in your correspondence.</p>" & vbcrlf
		
		' WEBLOYALTY
		ReceiptText = ReceiptText & "<table width=""25%"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center""><tr><td>" & vbcrlf
		if incEmail = true then
			ReceiptText = ReceiptText & "<a href=""https://one-time-offer.com/sg/gateway.aspx?v=0353A36353&p=9202F21232BE77B7D7F706E72FC3B3930303639F16D6060727&cl=1545"" target=""_blank""><img src=""http://www.wirelessemporium.com/images/cart/300x130_Webloyalty_banner.gif"" width=""300"" height=""130"" border=""0"" alt=""Click here now to claim your Cash Back Incentive on your next purchase when you enroll in Webloyalty's service. See offer and billing details.""></a>" & vbcrlf
		else
			ReceiptText = ReceiptText & "<SCRIPT language=""JavaScript1.1"" SRC=""https://ad.doubleclick.net/adj/N3446.Wireless_Emporium/B2088600;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?""></SCRIPT>" & vbcrlf
			ReceiptText = ReceiptText & "<NOSCRIPT>" & vbcrlf
			ReceiptText = ReceiptText & "<A HREF=""https://ad.doubleclick.net/jump/N3446.Wireless_Emporium/B2088600;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?"" target=""_blank""><IMG SRC=""https://ad.doubleclick.net/ad/N3446.Wireless_Emporium/B2088600;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?"" BORDER=0 WIDTH=300 HEIGHT=130 ALT=""""></A>" & vbcrlf
			ReceiptText = ReceiptText & "</NOSCRIPT>" & vbcrlf
		end if
		ReceiptText = ReceiptText & "</td></tr></table>" & vbcrlf
		
		' QUESTIONS (GRAY BOX)
		ReceiptText = ReceiptText & "<table width=""100%"" cellpadding=""5"" cellspacing=""5"" border=""0"" align=""center"" bgcolor=""#EAEAEA""><tr><td width=""45%"" align=""left"" valign=""top"" class=""regText"">" & vbcrlf
		ReceiptText = ReceiptText & "<table width=""100%"" cellpadding=""5"" cellspacing=""5"" border=""0"" align=""center"">" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td colspan=""2"" class=""regText"">Questions about your order or our service?<br>Visit our <a href=""http://" & request.ServerVariables("SERVER_NAME") & "/faq"">Frequently Asked Questions</a> Page</td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td colspan=""2"" class=""regText""><a href=""http://" & request.ServerVariables("SERVER_NAME") & "/orderstatus?OrderID=" & nOrderId & """><b>Check Your Order Status Online</b></a></td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td colspan=""2"" class=""regText""><b>Please visit us again soon!</b> - <a href=""http://" & request.ServerVariables("SERVER_NAME") & "/"">WirelessEmporium.com</a></td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "</td></tr></table>" & vbcrlf
		
		' TURNTO
		ReceiptText = ReceiptText & "<td width=""55%"" align=""right"" valign=""top"" class=""regText"">" & vbcrlf
		if incEmail = true then
			ReceiptText = ReceiptText & "&nbsp;" & vbcrlf
		else
			ReceiptText = ReceiptText & "<p align=""center"" style=""font-size:10pt;""><a href=""http://www.turnto.com/trackin/ordrconfpg?siteKey=W1a86usaEcbMZXlsite&linkKey=ripUIyMa42&qs=dm%3Dy""><img src=""http://www.wirelessemporium.com/images/turnto-banner.jpg"" width=""404"" height=""100"" border=""0"" alt=""Add your name to our trusted reference list.""></a></p>" & vbcrlf
			ReceiptText = ReceiptText & "<p align=""justify"" style=""font-size:10pt;""><div align=""justify"">When your friends visit WirelessEmporium.com, can we show them that you know about some of our products in case they need advice? It only takes a few seconds. You don't have to review, rate, comment, tweet or blast your friends in any way. As a thank you, <a style=""font-size:10pt;"" href=""http://www.turnto.com/trackin/ordrconfpg?siteKey=W1a86usaEcbMZXlsite&linkKey=ripUIyMa42&qs=dm%3Dy"">you'll get 10% off your next order</a>.</div></p>" & vbcrlf
		end if
		ReceiptText = ReceiptText & "</td></tr></table>" & vbcrlf
		
		ReceiptText = ReceiptText & "<br>" & vbcrlf
		
		' SQUARETRADE BANNER
		ReceiptText = ReceiptText & "<p align=""center""><a href=""http://www.squaretrade.com/pages/cellphone-landing3?ccode=bs_war_vc_052:wirelessemporium"" target=""_blank""><img src=""http://www.wirelessemporium.com/Squaretrade/banner_728x90.jpg"" border=""0"" width=""728"" height=""90"" alt=""SquareTrade - Warranties that make sense!""></a></p>" & vbcrlf
		
		ReceiptText = ReceiptText & "</td></tr></table>" & vbcrlf
	end if
end if




%>
