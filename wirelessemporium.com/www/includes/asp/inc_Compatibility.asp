<%
function CompatibilityList(thisPart,format)
	SQL = "SELECT B.modelName, B.modelImg, C.brandName FROM ((we_Items A INNER JOIN we_models B ON A.modelID=B.modelID)"
	SQL = SQL & " INNER JOIN we_brands C ON A.brandID=C.brandID)"
	SQL = SQL & " INNER JOIN we_types D ON A.typeID=D.typeID"
	SQL = SQL & " WHERE A.PartNumber='" & thisPart & "'"
	SQL = SQL & " AND A.HideLive = 0"
	SQL = SQL & " ORDER BY C.brandName, B.modelName"
	set RScompat = oConn.execute(SQL)
	
	dim holdBrandName
	holdBrandName = "9999"
	do until RScompat.eof
		if RScompat("brandName") <> holdBrandName then
			if not isNull(CompatibilityList) and len(CompatibilityList) > 2 then CompatibilityList = left(CompatibilityList,len(CompatibilityList)-2)
			if format = "display" then
				CompatibilityList = CompatibilityList & "<br><b>" & RScompat("brandName") & "</b><br>" & vbcrlf
			else
				if not isNull(CompatibilityList) and len(CompatibilityList) > 2 then CompatibilityList = CompatibilityList & ". "
				CompatibilityList = CompatibilityList & RScompat("brandName") & ": "
			end if
			holdBrandName = RScompat("brandName")
		end if
		CompatibilityList = CompatibilityList & RScompat("modelName") & ", "
		RScompat.movenext
	loop
	RScompat.close
	set RScompat = nothing
	if not isNull(CompatibilityList) and len(CompatibilityList) > 2 then CompatibilityList = left(CompatibilityList,len(CompatibilityList)-2)
end function
%>
