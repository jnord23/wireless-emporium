<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_Webloyalty.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/asp/inc_getStatesAdmin.asp"-->
<!-- METADATA TYPE="typelib" NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/admin/formatNotes.asp"-->
<script src="/includes/js/validate_new.js"></script>
<script>
function fnopennotes(orderid) 
{
	var url='ordernotes.asp?orderid='+orderid;
	window.open(url,'ordernote','left=0,top=0,width=800,height=300,resizable=1,locationbar=0,menubar=0,toolbar=0,scrollbars=1');
}

function RTS(siteid,orderid,accountid)
{
	ajax('/ajax/ajaxRTS?siteid=' + siteid + '&orderid=' + orderid + '&accountid=' + accountid,'dumpZone');
	alert('Instruction email sent to the customer');
}

function resendConfirmEmail(siteid,orderid,accountid)
{
	ajax('/ajax/ajaxResendConfirmEmail?siteid=' + siteid + '&orderid=' + orderid + '&accountid=' + accountid,'dumpZone');
	alert('email resent to the customer');
}

function toggleTable(chkID, tblID)
{
	var isChecked = document.getElementById(chkID).checked;
	
	if (isChecked) document.getElementById(tblID).style.display = "";
	else document.getElementById(tblID).style.display = "none";
}

function toggleTable2(tblID)
{
	var isDiaplay = document.getElementById(tblID).style.display;
	
	if ("none" == isDiaplay) document.getElementById(tblID).style.display = "";
	else document.getElementById(tblID).style.display = "none";
}

function doReship(oForm)
{
	if ("" == oForm.cbReshipReason.value) 
	{
		alert("please select reship reason.");
		return false;
	} else if (isNaN(oForm.txtQty.value))
	{
		alert("QTY is not numeric format.");
		return false;
	} else if (eval(oForm.txtQty.value) > 50)
	{
		alert("QTY cannot exceed 50.");
		return false;
	}
	
	oForm.hidSubmit.value = "Y";

	return true;
}

function cancelUpdateCustInfo()
{
	showHide('btnEditCustInfo','btnUpdateCustInfo');
	showHide('curName','updateName');
	showHide('curEmailAddress','updateEmailAddress');
	showHide('curBillAddress','updateBillAddress');
	showHide('curShipAddress','updateShipAddress');
}

function editCustInfo()
{
	showHide('btnUpdateCustInfo','btnEditCustInfo');
	showHide('updateName','curName');
	showHide('updateEmailAddress','curEmailAddress');	
	showHide('updateBillAddress','curBillAddress');		
	showHide('updateShipAddress','curShipAddress');			
}

function cancelUpdateOrderInfo()
{
	showHide('curShipType','updateShipType');
}

function editOrderInfo()
{
	showHide('updateShipType','curShipType');
}

function updateOrderInfo(accountid, orderid)
{
	if (("" == accountid) || ("" == orderid))	{alert("AccountID or OrderID was not provided.");return false;}

	var	newShipType	= escape(TrimStrings(document.frmOrderInfo.newShipType.value));

	bValid = true;
	CheckValidNEW(newShipType, "New Shipping Type is a required field!");

	if (bValid) document.frmOrderInfo.submit();
}

function TrimStrings(stringToTrim)
{
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function updateCustInfo(accountid, orderid)
{
	if (("" == accountid) || ("" == orderid))	{alert("AccountID or OrderID was not provided.");return false;}

	var	fname 		= escape(TrimStrings(document.frmCustInfo.newFName.value));
	var	lname 		= escape(TrimStrings(document.frmCustInfo.newLName.value));
	var	email 		= escape(TrimStrings(document.frmCustInfo.newEmailAddress.value));

	var	bAddr1		= escape(TrimStrings(document.frmCustInfo.newBAddress1.value));
	var	bAddr2		= escape(TrimStrings(document.frmCustInfo.newBAddress2.value));
	var	bCity		= escape(TrimStrings(document.frmCustInfo.newBCity.value));
	var	bState		= escape(TrimStrings(document.frmCustInfo.newBState.value));
	var	bZip		= escape(TrimStrings(document.frmCustInfo.newBZip.value));
	var	bCountry	= escape(TrimStrings(document.frmCustInfo.newBCountry.value));

	var	sAddr1		= escape(TrimStrings(document.frmCustInfo.newSAddress1.value));
	var	sAddr2		= escape(TrimStrings(document.frmCustInfo.newSAddress2.value));
	var	sCity		= escape(TrimStrings(document.frmCustInfo.newSCity.value));
	var	sState		= escape(TrimStrings(document.frmCustInfo.newSState.value));
	var	sZip		= escape(TrimStrings(document.frmCustInfo.newSZip.value));
	var	sCountry	= escape(TrimStrings(document.frmCustInfo.newSCountry.value));
	var param		= "";

	bValid = true;
	CheckValidNEW(fname, "Your First Name is a required field!");
	CheckValidNEW(lname, "Your Last Name is a required field!");
	CheckEMailNEW(email);

	CheckValidNEW(bAddr1, "Your Billing Address is a required field!");
	CheckValidNEW(bCity, "Your Billing City is a required field!");	
	CheckValidNEW(bState, "Your Billing State is a required field!");	
	CheckValidNEW(bZip, "Your Billing ZipCode is a required field!");

	CheckValidNEW(sAddr1, "Your Shipping Address is a required field!");
	CheckValidNEW(sCity, "Your Shipping City is a required field!");	
	CheckValidNEW(sState, "Your Shipping State is a required field!");	
	CheckValidNEW(sZip, "Your Shipping ZipCode is a required field!");	

	if (bValid) document.frmCustInfo.submit();
}

window.onload = function()
{
	//do nothing for now
}
</script>
<%
'Dummy Subs to prevent inc_promoFunctions.asp from wigging out and we don't want to modify it because it is included in so many other pages (2012-11-14)
sub fOpenConn
end sub
sub fCloseConn
end sub
%>
<%
response.buffer = true
response.expires = -1

InvoiceType = "Admin"
newName = SQLQuote(request.QueryString("newName"))
if isnull(nOrderID) or len(nOrderID) < 1 then nOrderID = SQLQuote(request.QueryString("nOrderID")) end if
if isnull(nAccountId) or len(nAccountId) < 1 then nAccountId = SQLQuote(request.QueryString("nAccountId")) end if

dim retCustMsg : retCustMsg = request("retCustMsg")
'response.write "[request.QueryString:" & request.QueryString & "]<br>"
'response.write "[request.Form:" & request.Form & "]<br>"

dim sPromoCode
writeSomething = 0

txtPartNumber = request("txtPartNumber")
cbReshipReason = request("cbReshipReason")
txtQty = request("txtQty")
hidSubmit = request("hidSubmit")
reshipNote = request("reshipNote")
doNotSendReshipEmail = prepInt(request("chkDoNotSendEmail"))

sql = "select reshipDesc from xreshipreason order by 1"
session("errorSQL") = sql
arrReship = getDbRows(sql)

'====================== update reship records =====================
Dim retReship : retReship = -1
dim retReshipMsg : retReshipMsg = ""
if "Y" = hidSubmit and "" <> txtPartNumber and "" <> cbReshipReason and isnumeric(txtQty) then
	session("errorSQL") = "exec we_ReshipItem"
	
	Dim cmd
	Set cmd = Server.CreateObject("ADODB.Command")
	Set cmd.ActiveConnection = oConn
	cmd.CommandText = "we_ReshipItem"
	cmd.CommandType = adCmdStoredProc 

	cmd.Parameters.Append cmd.CreateParameter("ret", adInteger, adParamReturnValue)
	cmd.Parameters.Append cmd.CreateParameter("p_orderid", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("p_partnumber", adVarChar, adParamInput, 100)
	cmd.Parameters.Append cmd.CreateParameter("p_reshipreason", adVarChar, adParamInput, 100)
	cmd.Parameters.Append cmd.CreateParameter("p_qty", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("p_modifier", adVarChar, adParamInput, 50)
	cmd.Parameters.Append cmd.CreateParameter("p_note", adVarChar, adParamInput, 4000)
	cmd.Parameters.Append cmd.CreateParameter("p_doNotSendEmail", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("p_out_msg", adVarChar, adParamOutput, 500)

	cmd("p_orderid")		=	nOrderID
	cmd("p_partnumber")		=	txtPartNumber
	cmd("p_reshipreason")	=	cbReshipReason
	cmd("p_qty")			=	txtQty
	cmd("p_modifier")		=	Request.Cookies("username")
	cmd("p_note")			=	reshipNote
	cmd("p_doNotSendEmail")	=	doNotSendReshipEmail

	cmd.Execute

	retReshipMsg	=	cmd("p_out_msg")
	retReship		= 	cmd("ret")			
end if 

SQL = "SELECT * FROM we_orders WHERE orderid = '" & nOrderId & "'"
set oRsOrd = Server.CreateObject("ADODB.Recordset")
oRsOrd.open SQL, oConn, 0, 1

if oRsOrd.eof then
	SQL = replace(SQL, "we_orders", "we_orders_historical")
	oRsOrd.close
	set oRsOrd = nothing
	set oRsOrd = Server.CreateObject("ADODB.Recordset")
	oRsOrd.open SQL, oConn, 0, 1
end if	

if oRsOrd.eof then
	writeSomething = 1
	response.Write("<h3>Order" & nOrderId & " not found!</h3>")
	response.end
end if

dim siteURL, lnkLogo, siteColor, siteDesc, site_id, useStore, eBillmeURL, pathPDF, pathTXT

set objRsStore = oConn.execute("select * from xstore where site_id = '" & oRsOrd("store") & "'")
if not objRsStore.eof then
	site_id		=	oRsOrd("store")
	siteURL		=	objRsStore("site_url")
	siteLogo	=	objRsStore("logo_big")
	lnkLogo		=	objRsStore("logo_big2")
	siteColor	=	objRsStore("color")
	siteDesc	=	objRsStore("longDesc")
	useStore	=	objRsStore("shortDesc")
	eBillmeURL	=	objRsStore("ebillme_url")
	pathPDF		=	objRsStore("salesOrderFilePathPDF")
	pathTXT		=	objRsStore("salesOrderFilePathTXT")	
end if
objRsStore.close
set objRsStore = nothing

sql	=	"select	configValue from we_config where configName = 'Free ItemID' and siteid = " & site_id
set rsFreeItem = oConn.execute(sql)
if not rsFreeItem.eof then strFreeItemID = rsFreeItem("configValue")
	
transactionID = 0
pp_transactionID = 0
'mySession needs to be defined using the sessionID from the time of original purchase
'Otherwise, the promocode lookup from the shopping cart will return 0 results and no discount will be shown when it should be  
'We also can't use the current sessionID because it doesn't match the original purchase sessionID
sqlSession =	"select		s.sessionID, o.* " & vbcrlf & _
				"from		we_orders o left join ShoppingCart s " & vbcrlf & _
				"	on		o.orderid = s.purchasedOrderID " & vbcrlf & _
				"where		o.orderid = '" & nOrderId & "'"
set sessionRS = oConn.execute(sqlSession)
mySession = prepInt(sessionRS("sessionID"))
transactionID = prepInt(sessionRS("transactionID"))
pp_transactionID = prepInt(sessionRS("pp_transactionID"))

sql	=	"select b.promoCode, b.couponDesc" & vbcrlf & _
		"from	we_orders a join v_coupons b" & vbcrlf & _
		"	on	a.couponid = b.couponid and a.store = b.site_id" & vbcrlf & _
		"where	a.orderid = '" & nOrderID & "'"
set rsPromo = oConn.execute(sql)
if not rsPromo.eof then 
	sPromoCode = rsPromo("promoCode")
	sPromoDesc = rsPromo("couponDesc")
end if	

sPromoCodeSave = sPromoCode 'Save the promo code for printing on the receipt

call fOpenConn() 'inc_promoFunctions.asp closes the connection, re-open it so processing can continue
'''''''''''''''''' END DISCOUNT ZONE ''''''''''''''''''

dim sFname, sLname, sEmail, sPhone
dim saddress1, saddress2, sCity, sstate, szip, sCountry
dim baddress1, baddress2, bCity, bstate, bzip, bCountry
dim sShipAddress, sBillAddress
dim oRsCust, oRsOrd

SQL = "SELECT * FROM " & useStore & "_accounts WHERE accountid = '" & nAccountId & "'"
set oRsCust = Server.CreateObject("ADODB.Recordset")
oRsCust.open SQL, oConn, 0, 1
if oRsCust.eof then
	writeSomething = 1
	response.Write("<h3>Customer account" & nAccountId & " not found!</h3>")
else
	sFname = oRsCust("fname")
	sLname = oRsCust("lname")
	sEmail = oRsCust("email")
	sPhone = oRsCust("phone")
end if

if request.form("cancelEmailSubmitted") <> "" then
	retCustMsg = "Order cancelled."
	if request.form("chkSendEmail") = "Y" then
		retCustMsg = retCustMsg & vbCrLf & "- Cancellation email sent."
		cdo_from = "sales@wirelessemporium.com"
		cdo_to = sEmail
		select case site_id
			case 0 : cdo_from = "sales@wirelessemporium.com"
			case 1 : cdo_from = "sales@cellphoneaccents.com"
			case 2 : cdo_from = "sales@cellularoutfitter.com"
			case 3 : cdo_from = "sales@phonesale.com"
			case 10 : cdo_from = "sales@tabletmall.com"
		end select
		cdo_subject = "Your order has been cancelled."
		cdo_body = "<p><img src=""" & siteLogo & """>" & "</p>"
		cdo_body = cdo_body & "<p>Hello " & sFname & " " & sLname & "<br /><br />Thank you for contacting us.<br /><br />"
		cdo_body = cdo_body & "Per your request, we have cancelled order number " & nOrderID & " and transaction has been reversed to the original method of payment.<br /><br />"
		cdo_body = cdo_body & "If you have any additional questions, please contact us for further assistance.<br /><br />"
		cdo_body = cdo_body & "Regards,<br /><br />"
		cdo_body = cdo_body & siteDesc & "</p>"
		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
	end if
	
	bStockItem = false
	if request.form("chkRestock") = "Y" then 
		retCustMsg = retCustMsg & vbCrLf & "- Restock Qty."
		bStockItem = true
	end if
	
	call doCancelOrder(nOrderID, bStockItem)
	
	sql	=	"	select	id, orderid, convert(varchar(8000), ordernotes) ordernotes	" & vbcrlf & _
			"	from	we_ordernotes" & vbcrlf & _
			"	where	orderid = '" & nOrderID & "'" & vbcrlf
			
	set objRsNotes = oConn.execute(sql)
	
	if not objRsNotes.EOF then
		curOrderNotes 	= 	objRsNotes("ordernotes")
		strMsg			=	retCustMsg
		AddNotes = SQLquote(curOrderNotes) & vbcrlf & formatNotes(strMsg)

		sql = 	"	update	we_ordernotes	" & vbcrlf & _
				"	set		ordernotes = '" & AddNotes & "'" & vbcrlf & _
				"	where	orderid = '" & nOrderID & "'" & vbcrlf		

		session("errorSQL") = sql		
		oConn.execute(sql)
	else
		strMsg		=	retCustMsg
		AddNotes 	= 	formatNotes(strMsg)
		sql 		= 	"	insert into we_ordernotes(orderid, ordernotes) values('" & nOrderID & "', '" & AddNotes & "') "
		session("errorSQL") = sql		
		oConn.execute(sql)
	end if
	objRsNotes = null
		
end if







dim pdfFileName, txtFileName
pdfFileName = oRsOrd("processPDF")
txtFileName = oRsOrd("processTXT")

'format the shipping and billing addresses
dim nShippingid
nShippingid = oRsOrd("shippingid")
if nShippingid > 0 then
	strSql = "SELECT * FROM we_addl_shipping_addr WHERE id='" & nShippingid & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open strSql, oConn, 0, 1
	if not RS.eof then
		saddress1 = RS("saddress1")
		saddress2 = RS("saddress2")
		sCity = RS("sCity")
		sstate = RS("sstate")
		szip = RS("szip")
		sCountry = RS("sCountry")
	end if
	
	if saddress1 = "" or isnull(saddress1) then
		saddress1 = oRsCust("saddress1")
		saddress2 = oRsCust("saddress2")
		sCity = oRsCust("sCity")
		sstate = oRsCust("sstate")
		szip = oRsCust("szip")
		sCountry = oRsCust("sCountry")	
	end if
else
	saddress1 = oRsCust("saddress1")
	saddress2 = oRsCust("saddress2")
	sCity = oRsCust("sCity")
	sstate = oRsCust("sstate")
	szip = oRsCust("szip")
	sCountry = oRsCust("sCountry")
end if

sShipAddress = sAddress1
if sAddress2 <> "" then sShipAddress = sShipAddress & "<br>" & saddress2
sShipAddress = sShipAddress & "<br>" & sCity & ", " & sState & "&nbsp;" & sZip & "<br>" & sCountry

baddress1 	= oRsCust("bAddress1")
baddress2 	= oRsCust("bAddress2")
bCity		= oRsCust("bCity")
bstate		= oRsCust("bState")
bzip		= oRsCust("bZip")
bCountry	= oRsCust("bCountry")

sBillAddress = baddress1
if baddress2 <> "" then sBillAddress = sBillAddress & "<br>" & baddress2
sBillAddress = sBillAddress & "<br>" & bCity & ", " & bstate & "&nbsp;" & bzip & "<br>" & bCountry

oRsCust.close
set oRsCust = nothing

if writeSomething = 0 then
	'format order info
	dim sShipType, nOrdershippingfee, nOrderTax, nBuysafeamount, extOrderType, extOrderNumber, nOrderDateTime
	nOrderSubTotal = formatNumber(cDbl(oRsOrd("ordersubtotal")),2)
	sShipType = oRsOrd("shiptype")
	nOrdershippingfee = cDbl(oRsOrd("ordershippingfee"))
	nOrderTax = oRsOrd("orderTax")
	nBuysafeamount = oRsOrd("BuySafeAmount")
	nOrderGrandTotal = formatNumber(cDbl(oRsOrd("ordergrandtotal")),2)
	extOrderType = oRsOrd("extOrderType")
	extOrderNumber = oRsOrd("extOrderNumber")
	nOrderDateTime = oRsOrd("orderdatetime")
	'Free Product Offer code added 2/3/2010 by MC
	dim FreeProductOffer
	FreeProductOffer = oRsOrd("FreeProductOffer")
	
	dim strOrderType
	select case extOrderType
		case 1 : strOrderType = "Paypal<br>" & extOrderNumber
		case 2 : strOrderType = "Google Checkout<br>" & extOrderNumber
		case 3
			strOrderType = "eBillme"
			eBillme = "eBillme"
			eBillmeAccount = extOrderNumber
		case 4 : strOrderType = "Ebay<br>" & extOrderNumber
		case 5 : strOrderType = "AtomicMall<br>" & extOrderNumber
		case 6 : strOrderType = "Amazon<br>" & extOrderNumber
		case 7 : strOrderType = "Buy.Com<br>" & extOrderNumber
		case 8 : strOrderType = "Sears<br>" & extOrderNumber
		case 9 : strOrderType = "Bestbuy<br>" & extOrderNumber
		case else : strOrderType = "Credit Card"
	end select
	
	oRsOrd.close
	set oRsOrd = nothing
	
	'===================
	'NEW WEBLOYALTY CODE
	dim RSwl, cctype, country, pcode, strToEncrypt, strEncryptedValue
	SQL = "SELECT A.CCtype, B.bState, B.bZip FROM we_ccinfo A INNER JOIN we_accounts B ON A.accountid=B.accountid WHERE A.orderid='" & nOrderID & "'"
	set RSwl = Server.CreateObject("ADODB.Recordset")
	RSwl.open SQL, oConn, 0, 1
	if not RSwl.eof then
		cctype = RSwl("CCtype")
		if len(RSwl("bState")) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",RSwl("bState")) > 0 then
			country = "CANADA"
		else
			country = "US"
		end if
		pcode = RSwl("bZip")
	end if
	RSwl.close
	set RSwl = nothing
	
	' Put together string ?concatenate all required values
	if incEmail = true then
		dim yesexiturl, noexiturl
		yesexiturl = "https://www.wirelessemporium.com/confirm.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderGrandTotal & "&c=" & nOrderSubTotal
		noexiturl = yesexiturl
		strToEncrypt = "cref" & chr(255) & nOrderID & chr(255) & "cctype" & chr(255) & cctype & chr(255) & "country" & chr(255) & country & chr(255) & "pcode" & chr(255) & pcode & chr(255) & "yesexiturl" & chr(255) & yesexiturl & chr(255) & "noexiturl" & chr(255) & noexiturl
	else
		strToEncrypt = "cref" & chr(255) & nOrderID & chr(255) & "cctype" & chr(255) & cctype & chr(255) & "country" & chr(255) & country & chr(255) & "pcode" & chr(255) & pcode
	end if
	' Encrypt it
	strEncryptedValue = SimpleEncrypt(strToEncrypt)
	'===================
%>
<% if session("mailError") <> "" then response.Write("<p>" & session("mailError") & "</p>") %>
<table width="1000" cellpadding="8" cellspacing="0" align="center" class="regText">
	<tr>
    	<td width="200"><a href="<%=siteURL%>"><img src="<%=lnkLogo%>" border="0" alt="<%=siteDesc & ".com"%>" hspace="12" align="absmiddle"></a></td>
        <td class="header" align="left"><b><%=siteDesc%> Order Confirmation</b></td>
    </tr>
</table>
<table width="950" cellpadding="5" cellspacing="1" align="center" class="regText" border="0"> 
	<tr>
    	<td width="500">
        	<p>
            <b>Thank you for your purchase on <a href="<%=siteURL%>"><%=siteDesc & ".com"%></a>.</b><br>
            Here are the details of your order:
            </p>
        </td>
    	<td width="450" align="right">
        <%
		dim fso : set fso = CreateObject("Scripting.FileSystemObject")
		
		if len(pdfFileName) > 1 or len(txtFileName) > 1 then
		%>
			<table width="100%" cellpadding="2" cellspacing="0" align="center" class="regText" border="1" style="border-collapse:collapse;"> 
            <%
			pdfFullPath = pathPDF & "\" & pdfFileName
			txtFullPath = pathTXT & "\" & txtFileName
			if len(pdfFullPath) > 1 then
				if fso.FileExists(pdfFullPath) then
			%>
            	<tr>
                	<td><%=pdfFileName%></td>
                	<td><a target="_blank" href="<%=replace(replace(lcase(pdfFullPath), "c:\inetpub\wwwroot\wirelessemporium.com\www", ""), "\", "/")%>">Download</a></td>
				</tr>
			<%
				end if
				pdfFullPath = replace(lcase(pdfFullPath), "\salesorders", "")
				if fso.FileExists(pdfFullPath) then
			%>
            	<tr>
                	<td><%=pdfFileName%></td>
                	<td><a target="_blank" href="<%=replace(replace(lcase(pdfFullPath), "c:\inetpub\wwwroot\wirelessemporium.com\www", ""), "\", "/")%>">Download</a></td>
				</tr>
			<%
				end if
			end if
			
			if len(txtFullPath) > 1 then
				if fso.FileExists(txtFullPath) then
			%>
            	<tr>
                	<td><%=txtFileName%></td>
                	<td><a target="_blank" href="<%=replace(replace(lcase(txtFullPath), "c:\inetpub\wwwroot\wirelessemporium.com\www", ""), "\", "/")%>">Download</a></td>
				</tr>
			<%
				end if
				txtFullPath = replace(lcase(txtFullPath), "\salesorders", "")
				if fso.FileExists(txtFullPath) then
			%>
            	<tr>
                	<td><%=txtFileName%></td>
                	<td><a target="_blank" href="<%=replace(replace(lcase(txtFullPath), "c:\inetpub\wwwroot\wirelessemporium.com\www", ""), "\", "/")%>">Download</a></td>
				</tr>
			<%
				end if
			end if
			%>
            </table>
		<%
		end if
		%>
        </td>		
    </tr>
    <tr>
    	<td></td>
        <td align="right">
        <%if transactionID > 0 then%>
        Authorize.net TransactionID: <%=transactionID%><br />
        <%end if%>
        <%if pp_transactionID > 0 then%>
        Authorize.net PostPurchase TransactionID: <%=pp_transactionID%>
        <%end if%>
        </td>
    </tr>
    <%
	if len(retCustMsg) > 0 then
	%>
    <tr>
    	<td width="100%">
	        <div id="divMsg" style="border:1px solid red; font-size:16px; width:100%; margin:5px 0px 5px 0px; padding:5px;"><b><%=retCustMsg%></b></div>
        </td>
    </tr>
    <%
	end if
	   
    'format customer info
    %>
    <tr>
        <td valign="top" colspan="2" bgcolor="#EAEAEA" class="header">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="regText">
				<tr>
					<td width="150"><p>&nbsp;&nbsp;&nbsp;Customer Information:</p></td>
					<td width="450">&nbsp;</td>
					<td align="right"><a href="javascript:resendConfirmEmail('<%=site_id%>','<%=nOrderID%>','<%=nAccountID%>')">Resend Confirmation</a></td>
					<td align="right" style="position:relative;">
                    	<a href="javascript:toggle('cancelBox');">Cancel this order</a>
                        <div id="cancelBox" style="width:250px; padding:5px; display:none; position:absolute; top:30px; right:0px; background-color:#fff; border:2px solid #666; color:#333;">
                        	<form name="frmCancelOrder" method="post" action="/admin/view_invoice_b.asp">
                        	<div style="padding-top:5px;" align="right"><a href="javascript:toggle('cancelBox');">Close</a></div>
                            <div style="padding-top:5px;" align="left"><input type="checkbox" name="chkSendEmail" value="Y" checked="checked" /> Send Cancellation Email To The Customer</div>
                            <div style="padding-top:5px;" align="left"><input type="checkbox" name="chkRestock" value="Y" checked="checked" /> Restock</div>
                            <div style="padding-top:5px;" align="center"><input type="submit" name="cancelEmailSubmitted" value="SUBMIT" /></div>
                            <input type="hidden" name="orderID" value="<%=nOrderId%>" />
                            </form>
                        </div>
					</td>
					<td align="right">
						<div id="btnEditCustInfo"><a style="cursor:pointer; color:#00F" onclick="editCustInfo();">(Edit)</a></div>
						<div id="btnUpdateCustInfo" style="display:none;">
							<a style="cursor:pointer; color:#F00" onclick="updateCustInfo('<%=nAccountId%>', '<%=nOrderID%>');"><b>SAVE</b></a> | 
							<a style="cursor:pointer; color:gray" onclick="cancelUpdateCustInfo();">CANCEL</a>							
						</div>
					</td>
				</tr>
			</table>
		</td>
    </tr>
	<form name="frmCustInfo" method="POST" action="/admin/adjustInvoice.asp">	
		<input type="hidden" name="updateType" value="custInfo" />
		<input type="hidden" name="accountid" value="<%=nAccountID%>" />
		<input type="hidden" name="orderid" value="<%=nOrderID%>" />
		<input type="hidden" name="storeid" value="<%=site_id%>" />
    <tr>
        <td valign="top" colspan="2">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="regText">
                <tr>
                    <td valign="top" width="50">&nbsp;</td>
                    <td valign="top" width="350" nowrap="nowrap">
                    	<b>Name:</b><br>
						<div id="curName"><%=sFname%>&nbsp;<%=sLname%>&nbsp;</div>
                        <div id="updateName" style="display:none;">
							<input type="text" name="newFName" value="<%=sFname%>" size="10"/>&nbsp;
							<input type="text" name="newLName" value="<%=sLname%>" size="10"/>							
						</div>
                    </td>
                    <td valign="top"><b>Paid By:</b><br><%=strOrderType%></td>
                </tr>
                <tr>
                    <td valign="top" colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="50">&nbsp;</td>
                    <td valign="top" width="350">
						<b>Email Address:</b><br>
						<div id="curEmailAddress"><a href="mailto:<%=sEmail%>"><%=sEmail%></a></div>
						<div id="updateEmailAddress" style="display:none;"><input type="text" name="newEmailAddress" value="<%=sEmail%>" size="30"/></div>
					</td>
                    <td><b>Phone:</b><br><%=sPhone%></td>
                </tr>
                <tr>
                    <td valign="top" colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" width="50">&nbsp;</td>
                    <td valign="top" width="350">
						<b>Billing Address:</b><br>
						<div id="curBillAddress"><%=sBillAddress%></div>
						<div id="updateBillAddress" style="display:none;">
				            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="regText">
								<tr><td height="25" width="100">Address1:</td><td><input type="text" name="newBAddress1" value="<%=bAddress1%>" size="20"/></td></tr>	
								<tr><td height="25" width="100">Address2:</td><td><input type="text" name="newBAddress2" value="<%=bAddress2%>" size="20"/></td></tr>
								<tr><td height="25" width="100">City:</td><td><input type="text" name="newBCity" value="<%=bCity%>" size="10"/></td></tr>
								<tr><td height="25" width="100">State:</td><td><select name="newBState"><%getStates(bState)%></select></td></tr>
								<tr><td height="25" width="100">Zipcode:</td><td><input type="text" name="newBZip" value="<%=bZip%>" size="5"/></td></tr>
								<tr><td height="25" width="100">Country:</td><td><input type="text" name="newBCountry" value="<%=bCountry%>" size="5"/></td></tr>
							</table>
						</div>
					</td>
                    <td>
						<b>Shipping Address:</b><br>
						<div id="curShipAddress"><%=sShipAddress%></div>
						<div id="updateShipAddress" style="display:none;">
				            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="regText">
								<tr><td height="25" width="100">Address1:</td><td><input type="text" name="newSAddress1" value="<%=sAddress1%>" size="20"/></td></tr>	
								<tr><td height="25" width="100">Address2:</td><td><input type="text" name="newSAddress2" value="<%=sAddress2%>" size="20"/></td></tr>
								<tr><td height="25" width="100">City:</td><td><input type="text" name="newSCity" value="<%=sCity%>" size="10"/></td></tr>
								<tr><td height="25" width="100">State:</td><td><select name="newSState"><%getStates(sState)%></select></td></tr>
								<tr><td height="25" width="100">Zipcode:</td><td><input type="text" name="newSZip" value="<%=sZip%>" size="5"/></td></tr>
								<tr><td height="25" width="100">Country:</td><td><input type="text" name="newSCountry" value="<%=sCountry%>" size="5"/></td></tr>
							</table>						
						</div>
                        <a href="javascript:RTS('<%=site_id%>','<%=nOrderID%>','<%=nAccountID%>')">RTS</a>
					</td>
                </tr>
                <tr>
                    <td valign="top" colspan="3">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
	</form>				
	<%
	'format order information
	dim oRsOrderDetails, sSqlDetails, thisSubtotal, mySubtotal, nCount, nID, nPartNumber, nQty, nPrice, productLink, isReshipOrder
	isReshipOrder = false
	mySubtotal = 0
	nCount = 1
	
	if not isNumeric(nOrderTax) or nOrderTax = "" or isNull(nOrderTax) then nOrderTax = "0" end if
	nOrderTax = cDbl(nOrderTax)
    %>
	<tr>
    	<td valign="top" colspan="2" bgcolor="#EAEAEA" class="header"><p>&nbsp;&nbsp;&nbsp;Order Details:</p></td>
    </tr>
    <tr>
    	<td valign="top" colspan="2">
        	<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="regText">
            	<tr>
                	<td valign="top" width="50">&nbsp;</td>
					<td valign="top" width="350">Order ID: <b><%=nOrderId%></b></td>
                    <td width="600">&nbsp;</td>
                </tr>
            	<tr>
                	<td valign="top" width="50">&nbsp;</td>
					<td colspan="2">
					<%
					sqlOrderDetail =	"	select	distinct a.orderdatetime, a.scandate, a.approved, a.cancelled, a.extordertype, a.phoneorder " & vbcrlf & _
										"		,	isnull(convert(varchar(max), b.ordernotes), '') ordernotes, a.thub_posted_to_accounting processed, a.thub_posted_date  " & vbcrlf & _
										"		,	a.parentOrderID, isnull(r.rmaType, 0) rmaType " & vbcrlf & _
										"	from	we_orders a left outer join we_ordernotes b  " & vbcrlf & _
										"		on	a.orderid = b.orderid left outer join we_rma r " & vbcrlf & _
										"		on	a.orderid = r.childOrderID  " & vbcrlf & _
										"	where	a.orderid = '" & nOrderId & "' " & vbCRLF
					
					dim arrOrderDetail : arrOrderDetail = getDbRows(sqlOrderDetail)
					if isnull(arrOrderDetail) then
						sqlOrderDetail = replace(sqlOrderDetail, "we_orders", "we_orders_historical")
						arrOrderDetail = getDbRows(sqlOrderDetail)
					end if
					
					if not isnull(arrOrderDetail) then 
						select case arrOrderDetail(4,0)
							case 1 : OrderType = "Paypal"
							case 2 : OrderType = "Google"
							case 3 : OrderType = "eBillme"
							case 4 : OrderType = "Ebay"
							case 5 : OrderType = "AtomicMall"
							case 6 : OrderType = "Amazon"
							case 7 : OrderType = "Buy.Com"
							case 8 : OrderType = "Sears"
							case 10 : OrderType = "Newegg"
							case else : OrderType = "Credit Card"
						end select
						
						if cint(arrOrderDetail(10,0)) = 8 then
						%>
						<div style="font-size:16px; border:1px solid red; padding:5px; margin:5px 0px 5px 0px; width:100%;">
							This order is <b>Back-Order</b>, <a href="/admin/view_invoice_b.asp?accountid=<%=nAccountId%>&orderId=<%=arrOrderDetail(9,0)%>">click here</a> to see original order.                        
                        </div>                        
                        <%						
						elseif not isnull(arrOrderDetail(9,0)) then
							isReshipOrder = true
						%>
                        <div style="font-size:16px; border:1px solid red; padding:5px; margin:5px 0px 5px 0px; width:100%;">
							This order is <b>RESHIPPED ORDER</b>, <a href="/admin/view_invoice_b.asp?accountid=<%=nAccountId%>&orderId=<%=arrOrderDetail(9,0)%>">click here</a> to see original order.                        
                        </div>
                        <%
						end if
					%>
			        	<table id="id_tblOrderDetail" width="100%" border="2" cellpadding="2" cellspacing="2" align="center" class="regText" style="border-color:#FEFEFE; border-collapse:collapse;">
							<tr bgcolor="#CCCCCC">
								<td align="center"><b>Order Date</b></td>
								<td align="center"><b>Scan Date</b></td>		
								<td align="center"><b>Apvd.</b></td>
								<td align="center"><b>Cncld.</b></td>		
								<td align="center"><b>Order Type</b></td>
								<td align="center"><b>Phone Order</b></td>
								<td align="center"><b>Notes</b></td>
								<td align="center"><b>Processed</b></td>	
							</tr>
							<tr>
								<td align="center"><%=arrOrderDetail(0,0)%></td>
								<td align="center"><%=nullif(arrOrderDetail(1,0), "N/A", arrOrderDetail(1,0))%></td>
								<td align="center"><%=trueif(arrOrderDetail(2,0), "YES", "NO")%></td>
								<td align="center"><%=trueif(arrOrderDetail(3,0), "YES", "NO")%></td>
								<td align="center"><%=OrderType%></td>
								<td align="center"><%=nullif(arrOrderDetail(5,0), "NO", "YES")%></td>
								<td align="center" title="<%if arrOrderDetail(6,0) = "" then response.write("No notes entered") else response.write(Left(Replace(arrOrderDetail(6,0),"""",""),100)) end if%>"><a href="#" onClick="fnopennotes('<%=nOrderId%>');"><img src='/images/Notes<%if arrOrderDetail(6,0) = "" then response.write "_none"%>.gif' border=0></a></td>
								<td align="center">					
									<%=nullif(arrOrderDetail(7,0), "NO", "YES")%>
									<%=nullif(arrOrderDetail(8,0), "", "&nbsp;(" & arrOrderDetail(8,0) & ")")%>
								</td>
							</tr>
						</table>	
					<%
					else
						response.write "<font color=""red"">No order detail found</font>"
					end if 
					%>				
					</td>
                </tr>				
                <tr>
                	<td valign="top" colspan="3" width="1000"><br><hr></td>
                </tr>
				<% ' Item Details %>
                <tr><td><br /></td></tr>
                <tr>
                	<td valign="top" width="50">&nbsp;</td>
                    <td valign="top" width="250"><b>Items:</b></td>
                    <td valign="top" width="650">
			        	<table width="650" border="0" cellpadding="0" cellspacing="0" align="center" class="regText">
                    <%

					sSqlDetails = 	"exec sp_adminReceipt " & nOrderId
					'response.write "<pre>" & sSqlDetails & "</pre>"
					'response.end
					session("errorSQL") = sSqlDetails
					dim arrDetails : arrDetails = getDbRows(sSqlDetails)
					if not isnull(arrDetails) then 
						itemTotal = 0
						for nRow=0 to ubound(arrDetails, 2)
							if not arrDetails(5, nRow) and not arrDetails(33, nRow) then 
					%>
							<tr>
								<td width="100%" align="left" style="padding-bottom:30px;">
					<%
								if isnull(arrDetails(0, nRow)) then	'musicSkins
									nID 			= arrDetails(19, nRow)
									nPartNumber 	= arrDetails(20, nRow)
									nTypeID			= arrDetails(21, nRow)
									strItemDesc		= arrDetails(25, nRow) & " " & arrDetails(26, nRow) & " " & arrDetails(24, nRow) & " music skin"
									select case site_id
										case "0" : nPrice	= arrDetails(22, nRow)	'we
										case "2" : nPrice	= arrDetails(31, nRow)	'co
										case "10" : nPrice = getPricingTM(arrDetails(33, nRow), arrDetails(22, nRow))
										case else : nPrice	= arrDetails(22, nRow)
									end select									
									productLink 	= "<a target=""_blank"" href=""http://www.wirelessemporium.com/p-ms-" & nID & "-" & formatSEO(strItemDesc) & ">" & strItemDesc & "</a> [" & nPartNumber & "]<br>"
									nQty 			= arrDetails(4, nRow)								
								else
									nID 			= arrDetails(0, nRow)
									nPartNumber 	= arrDetails(1, nRow)
									nTypeID			= arrDetails(15, nRow)
									useBrandName 	= arrDetails(25, nRow)
									useModelName 	= arrDetails(26, nRow)
		
									select case site_id
										case "1" : strItemDesc	= arrDetails(16, nRow)	'ca
										case "2" : strItemDesc	= arrDetails(14, nRow)	'co
										case "3" : strItemDesc	= arrDetails(17, nRow)	'ps
										case "10" : strItemDesc	= arrDetails(32, nRow)	'tm
										case else : strItemDesc	= arrDetails(2, nRow)	'else
									end select
									
									partNumber = nPartNumber
									strItemDesc = insertDetailsAdv(strItemDesc,useBrandName,useModelName)
									
									select case site_id
										case "0" : nPrice	= arrDetails(3, nRow)	'we
										case "1" : nPrice	= arrDetails(10, nRow)	'ca
										case "2" : nPrice 	= arrDetails(11, nRow)	'co
										case "3" : nPrice 	= arrDetails(12, nRow)	'ps
										case "10" : nPrice 	= getPricingTM(arrDetails(13, nRow), arrDetails(3, nRow))
									end select
									
									select case site_id
										case "0" : productLink = "<a target=""_blank"" href=""http://www.wirelessemporium.com/p-" & nID & "-" & formatSEO(strItemDesc) & """>" & strItemDesc & "</a> [" & nPartNumber & "]<br>"
										case "1" : productLink = "<a target=""_blank"" href=""http://www.cellphoneaccents.com/p-" & nID & "-" & formatSEO(strItemDesc) & ".html"">" & strItemDesc & "</a> [" & nPartNumber & "]<br>"
										case "2" : productLink = "<a target=""_blank"" href=""http://www.cellularoutfitter.com/p-" & nID & "-" & formatSEO(strItemDesc) & ".html"">" & strItemDesc & "</a> [" & nPartNumber & "]<br>"
										case "3" : productLink = "<a target=""_blank"" href=""http://www.phonesale.com/" & formatSEO(arrDetails(18, nRow)) & "/p-" & nID+300001 & "-tc-" & nTypeID & "-" & formatSEO(strItemDesc) & ".html"">" & strItemDesc & "</a> [" & nPartNumber & "]<br>"
										case "10" : productLink = "<a target=""_blank"" href=""http://www.tabletmall.com/" & formatSEO(strItemDesc) & "-p-" & nID & """>" & strItemDesc & "</a> [" & nPartNumber & "]<br>"
									end select							
		
									nQty 			= arrDetails(4, nRow)
									postPurchase	= arrDetails(34, nRow)
								end if
								wePic			= arrDetails(35, nRow)
								coPic			= arrDetails(36, nRow)
								invQty			= arrDetails(37, nRow)
								voOrderID		= arrDetails(38, nRow)
								voVendorCode	= arrDetails(39, nRow)
								voOrderAmt		= arrDetails(40, nRow)
								voProcessDate	= arrDetails(41, nRow)
								Cogs			= arrDetails(42, nRow)
								AvgShip			= arrDetails(43, nRow)
								
								if instr(strFreeItemID, nID) > 0 then
									usePrice = formatCurrency(0)
									nPrice = 0
								else	
									if isnumeric(nPrice) then
										usePrice = formatCurrency(nPrice,2)
									else
										usePrice = formatCurrency(0,2)
										nPrice = 0
									end if
								end if
					%>
						<%=nCount%>: 
						<%if postPurchase then%>
                        <div style="display:inline-block; background-color:#eaeff5; border-radius:5px; padding:5px; font-weight:bold;">Post Purchase Item</div>
                        <%elseif instr(strFreeItemID, nID) > 0 then%>
                        <div style="display:inline-block; background-color:#930; color:#fff; border-radius:5px; padding:5px; font-weight:bold;">Free Promo Item</div>
                        <%end if%>
                        
						<%=productLink%>
            			&nbsp;&nbsp;&nbsp;Quantity: <%=nQty%> | Price: <%=usePrice%>
					<%
							for MYcount = 1 to nQty
								MYnID = MYnID & nID & ","
							next
							thisSubtotal = formatNumber(cDbl(nQty) * cDbl(nPrice),2)
							if not isnull(voOrderID) then
								dayBump = 1
								availableDate = formatDateTime(cdate(voProcessDate) + cint(AvgShip) + dayBump,1)
								if instr(availableDate,"Saturday") > 0 then dayBump = 3
								if instr(availableDate,"Sunday") > 0 then dayBump = 2
								availableDate = formatDateTime(cdate(voProcessDate) + cint(AvgShip) + dayBump,1)
							end if
					%>
                    <br><div id="subTotalBox" style="position:relative;">
                    	<div style="position:absolute; top:-30px; left:-100px; z-index:20;"><img src="/productPics/thumb/<%=wePic%>" border="0" /></div>
                        <% if Cogs > 3.99 then %>
                        <div style="position:absolute; top:-30px; left:-180px; font-size:16px; font-weight:bold; color:#000; z-index:20;">RETURN</div>
                        <% else %>
                        <div style="position:absolute; top:-30px; left:-180px; font-size:16px; font-weight:bold; color:#0C0; z-index:20;">REPLACE</div>
                        <% end if %>
                        <% if invQty < 4 then %>
                        <div style="position:absolute; top:0px; left:180px; width:400px; height:30px; border:1px solid #000; padding:2px; z-index:20;">
                        	<div style="display:table; border-bottom:1px solid #000; width:100%;">
                            	<div style="float:left; width:50px; border-right:1px solid #000;">On Order</div>
                                <div style="float:left; width:225px; border-right:1px solid #000; margin-left:10px;">Available On About</div>
                                <div style="float:left; margin-left:10px;">Order #</div>
                            </div>
                            <% if not isnull(voOrderID) then %>
                            <div style="display:table; width:100%;">
                            	<div style="float:left; width:50px; border-right:1px solid #000;"><%=voOrderAmt%></div>
                                <div style="float:left; width:225px; border-right:1px solid #000; margin-left:10px;"><%=availableDate%> (<%=cint(AvgShip)%>)</div>
                                <div style="float:left; margin-left:10px;"><%=voOrderID%></div>
                            </div>
                            <% else %>
                            <div style="display:table; width:100%; text-align:center; font-size:12px; font-weight:bold;">Item is not currently on order</div>
                            <% end if %>
                        </div>
                        <% end if %>
                        <div style="position:absolute; top:0px; left:-180px; z-index:<%=500-nCount%>;">
                        	<form name="frmReship" action="/admin/view_invoice_b.asp" method="post">					
                                <input type="hidden" name="orderId" value="<%=nOrderID%>" />
                                <input type="hidden" name="hidSubmit" value="" />
                                &nbsp;&nbsp;&nbsp; <b>Reship</b> <input type="checkbox" id="id_chkReship_<%=nCount%>" name="chkReship" onclick="javascript:toggleTable('id_chkReship_<%=nCount%>','id_tblReship_<%=nCount%>');" /> <br>
                                <table id="id_tblReship_<%=nCount%>" width="500" border="1" cellpadding="2" cellspacing="0" align="left" class="regText" style="display:none; border-color:#000; border-collapse:collapse; background-color:#EAEAEA;">
                                    <tr>
                                        <td width="100" align="right"><b>PartNumber:</b></td>
                                        <td align="left"><input type="text" name="txtPartNumber" value="<%=nPartNumber%>" size="15"/></td>
                                    </tr>
                                    <tr>
                                        <td width="100" align="right"><b>Reship Reason:</b></td>
                                        <td align="left">
                                            <select name="cbReshipReason">
                                                <option value=""></option>
                                                <%
                                                for i=0 to ubound(arrReship, 2)
                                                %>
                                                <option value="<%=arrReship(0,i)%>"><%=arrReship(0,i)%></option>
                                                <%	
                                                next
                                                %>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100" align="right"><b>Additional Note:</b></td>
                                        <td align="left"><textarea name="reshipNote" cols="40" rows="3"></textarea></td>
                                    </tr>								
                                    <tr>
                                        <td width="100" align="right"><b>Qty:</b></td>
                                        <td align="left">
                                            <div class="left padding5 margin-v"><input type="text" name="txtQty" value="1" size="2"/></div>
                                            <div class="right roundBorder padding5 margin-v" style="background-color:#fff;"><input id="id_doNotSendEmail" type="checkbox" name="chkDoNotSendEmail" value="1" /><label for="id_doNotSendEmail">DO NOT SEND AN EMAIL WHEN RESHIP</label></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center"><input type="submit" name="myAction" value="Reship Now" onclick="return doReship(this.form);"/></td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                        &nbsp;&nbsp;&nbsp;Subtotal: <%=thisSubtotal%>
                    </div>
                    <br>&nbsp;&nbsp;&nbsp;<a href="/admin/productNotes.asp?partNumber=<%=nPartNumber%>" target="_blank"><img src='/images/Notes<%if arrDetails(9,0) = "" then response.write "_none"%>.gif' border=0></a>
						<br />
                    <%
							nCount = nCount + 1
							nTotQty = nTotQty + nQty
							mySubtotal = mySubtotal + thisSubtotal
							itemTotal = itemTotal + (cDbl(nQty) * cDbl(nPrice))
					%>
								</td>
							</tr>
					<%				
							end if			
						next
					end if
					%>
						</table>
                    </td>
                </tr>
            	<tr>
			        <td valign="top" colspan="3" width="950"><br><hr></td>
			    </tr>	
				<tr>
                	<td valign="top" width="50">&nbsp;</td>
                    <td colspan="2">
					<b>BackOrder Fulfillment History:</b>
					<%
					if not isnull(arrDetails) then 'display reshiped items
					%>
						<table width="100%" border="2" cellpadding="2" cellspacing="2" align="left" class="regText" style="border-color:#FEFEFE; border-collapse:collapse;">					
							<tr bgcolor="#CCCCCC">
								<td align="center" width="70" height="25"><b>BackOrder OrderID</b></td>
								<td align="center" width="150"><b>PartNumber</b></td>
								<td align="center" width="310"><b>ItemDesc</b></td>
								<td align="center" width="150"><b>Fulfillment Date</b></td>
								<td align="center" width="100"><b>Modifier</b></td>
								<td align="center"><b>QTY</b></td>
							</tr>
					<%	dim bgColor, txtColor
					
						for nRow=0 to ubound(arrDetails, 2)
							if arrDetails(33, nRow) then
								backOrderOrderID	= 	arrDetails(27, nRow)
								parentOrderID	=	arrDetails(28, nRow)
								orderDetailID	=	arrDetails(30, nRow)
								
								if isnull(arrDetails(0, nRow)) then	'musicSkins
									nID 			= arrDetails(19, nRow)
									nPartNumber 	= arrDetails(20, nRow)
									strItemDesc		= arrDetails(25, nRow) & " " & arrDetails(26, nRow) & " " & arrDetails(24, nRow) & " music skin"
									nPrice			= arrDetails(22, nRow)
								else
									nID 			= arrDetails(0, nRow)
									nPartNumber 	= arrDetails(1, nRow)
		
									select case site_id
										case "1" : strItemDesc	= arrDetails(16, nRow)	'ca
										case "2" : strItemDesc	= arrDetails(14, nRow)	'co
										case "3" : strItemDesc	= arrDetails(17, nRow)	'ps
										case else : strItemDesc	= arrDetails(2, nRow)	'else
									end select
									
									select case site_id
										case "0" : nPrice	= arrDetails(3, nRow)	'we
										case "1" : nPrice	= arrDetails(10, nRow)	'ca
										case "2" : nPrice 	= arrDetails(11, nRow)	'co
										case "3" : nPrice 	= arrDetails(12, nRow)	'ps
										case "10" : nPrice 	= arrDetails(13, nRow)	'er
									end select
								end if
									
'								call reshipColorPicker(arrReshipReason, arrDetails(6, nRow), bgColor, txtColor)
					%>
							<tr>
								<td align="center" height="30"><a href="/admin/view_invoice_b.asp?accountid=<%=nAccountId%>&orderId=<%=backOrderOrderID%>"><%=backOrderOrderID%></a></td>
								<td align="center"><%=nPartNumber%></td>
								<td align="center"><%=strItemDesc%></td>
								<td align="center"><%=arrDetails(7, nRow)%></td>
								<td align="center"><%=arrDetails(8, nRow)%></td>
								<td align="center"><%=formatnumber(arrDetails(4, nRow), 0)%></td>
							</tr>
					<%
							end if 
						next
					%>	
						</table>
					<%	
					end if
					%>
					</td>
                </tr>				
            	<tr>
			        <td valign="top" colspan="3"><br><hr></td>
			    </tr>                			
            	<tr>
                	<td valign="top" width="50">&nbsp;</td>
                    <td colspan="2"><%if "Y" = hidSubmit and -1 = retReship then response.write "<font color=""red"">" & retReshipMsg & "</font><br>" end if%>
					<b>Reship History:</b>
					<%
					if not isnull(arrDetails) then 'display reshiped items
					%>
						<table width="100%" border="2" cellpadding="2" cellspacing="2" align="left" class="regText" style="border-color:#FEFEFE; border-collapse:collapse;">					
							<tr bgcolor="#CCCCCC">
								<td align="center" width="70" height="25"><b>Reship OrderID</b></td>
								<td align="center" width="95"><b>PartNumber</b></td>
								<td align="center" width="210"><b>ItemDesc</b></td>
								<td align="center" width="170"><b>Reship Reason</b></td>
								<td align="center" width="110"><b>Reship Date</b></td>
								<td align="center" width="60"><b>Modifier</b></td>
								<td align="center"><b>QTY</b></td>								
								<td align="center"><b>Note</b></td>
								<td align="center"><b>Action</b></td>
							</tr>
					<%
						for nRow=0 to ubound(arrDetails, 2)
							if arrDetails(5, nRow) then
								reshipOrderID	= 	arrDetails(27, nRow)
								parentOrderID	=	arrDetails(28, nRow)
								orderDetailID	=	arrDetails(30, nRow)
								
								if isnull(arrDetails(0, nRow)) then	'musicSkins
									nID 			= arrDetails(19, nRow)
									nPartNumber 	= arrDetails(20, nRow)
									strItemDesc		= arrDetails(25, nRow) & " " & arrDetails(26, nRow) & " " & arrDetails(24, nRow) & " music skin"
									nPrice			= arrDetails(22, nRow)
								else
									nID 			= arrDetails(0, nRow)
									nPartNumber 	= arrDetails(1, nRow)
		
									select case site_id
										case "1" : strItemDesc	= arrDetails(16, nRow)	'ca
										case "2" : strItemDesc	= arrDetails(14, nRow)	'co
										case "3" : strItemDesc	= arrDetails(17, nRow)	'ps
										case else : strItemDesc	= arrDetails(2, nRow)	'else
									end select
									
									select case site_id
										case "0" : nPrice	= arrDetails(3, nRow)	'we
										case "1" : nPrice	= arrDetails(10, nRow)	'ca
										case "2" : nPrice 	= arrDetails(11, nRow)	'co
										case "3" : nPrice 	= arrDetails(12, nRow)	'ps
										case "10" : nPrice 	= arrDetails(13, nRow)	'er
									end select
								end if
									
'								call reshipColorPicker(arrReshipReason, arrDetails(6, nRow), bgColor, txtColor)
					%>
							<tr>
								<td align="center" height="30"><a href="/admin/view_invoice_b.asp?accountid=<%=nAccountId%>&orderId=<%=reshipOrderID%>"><%=reshipOrderID%></a></td>
								<td align="center"><%=nPartNumber%></td>
								<td align="center"><%=strItemDesc%></td>
<!--								<td align="center" style="background-color:<%=bgColor%>; color:<%=txtColor%>;"><b><%=arrDetails(6, nRow)%></b></td>-->
								<td align="center"><b><%=arrDetails(6, nRow)%></b></td>
								<td align="center"><%=arrDetails(7, nRow)%></td>
								<td align="center"><%=arrDetails(8, nRow)%></td>
								<td align="center"><%=formatnumber(arrDetails(4, nRow), 0)%></td>
								<td align="left"><%=arrDetails(9, nRow)%></td>
								<td align="center">
                                <%
								'reship can be cancelled only when reship order has not been processed yet
								if isnull(arrDetails(29, nRow)) and not isReshipOrder and not isnull(parentOrderID) then
                                %>
                                	<a href="/admin/deleteReshipItem.asp?accountid=<%=nAccountId%>&orderId=<%=nOrderID%>&reshipOrderID=<%=reshipOrderID%>&orderDetailID=<%=orderDetailID%>&partNumber=<%=nPartNumber%>&delQty=<%=arrDetails(4, nRow)%>">
                                    	<img src="/images/btn_delete.gif" border="0" />
                                    </a>
								</td>
                                <%
								end if
                                %>
							</tr>
					<%
							end if 
						next
					%>	
						</table>
					<%	
					end if
					%>
					</td>
                </tr>				
            	<tr>
			        <td valign="top" colspan="3"><br><hr></td>
			    </tr>
                <tr>
                    <td valign="top" width="50">&nbsp;</td>
                    <td valign="top" width="350"><b>Order Subtotal:</b></td>
                    <td><%=formatCurrency(nOrderSubTotal,2)%></td>
                </tr>
                <tr>
                    <td valign="top" colspan="3"><br><hr></td>
                </tr>
                <%if sPromoCode <> "" then%>
                <tr>
                    <td valign="top" width="50">&nbsp;</td>
                    <td valign="top" width="350"><b>Coupon Used (<%=sPromoCode%>)<b>:</b></td>
                    <td valign="top"><%=sPromoDesc%></td>
                </tr>
                <tr>
                    <td valign="top" colspan="3"><br><hr></td>
                </tr>
                <%end if%>
                <%if itemTotal > (nOrderGrandTotal - nOrderTax - nOrdershippingfee) then%>
                <tr>
                    <td valign="top" width="50">&nbsp;</td>
                    <td valign="top" width="350"><b>Discount Amount:</b></td>
                    <td valign="top"><%=formatcurrency((nOrderGrandTotal - nOrderTax - nOrdershippingfee) - itemTotal,2)%></td>
                </tr>
                <tr>
                    <td valign="top" colspan="3"><br><hr></td>
				</tr>
				<%end if%>
                <%
                if nOrderTax > 0 then
                %>
                <tr>
                    <td valign="top" width="50">&nbsp;</td>
                    <td valign="top" width="350"><b>CA Resident Tax (<%=Application("taxDisplay")%>%):</b></td>
                    <td><%=formatCurrency(nOrderTax,2)%></td>
                </tr>
                <tr>
                    <td valign="top" colspan="3"><br><hr></td>
                </tr>
                <%
                end if
                
                if nBuysafeamount > 0 then
                %>
                <tr>
                    <td valign="top" width="50">&nbsp;</td>
                    <td valign="top" width="350"><b><a href="http://www.buysafe.com/questions" target="_blank">buySAFE&nbsp;Bond&nbsp;Guarantee</a>:</b></td>
                    <td><%=formatCurrency(nBuysafeamount,2)%></td>
                </tr>
                <tr>
                    <td valign="top" colspan="3"><br><hr></td>
                </tr>
                <%
                end if
                %>
				<form name="frmOrderInfo" action="/admin/adjustInvoice.asp" method="POST">
					<input type="hidden" name="updateType" value="orderInfo" />
					<input type="hidden" name="accountid" value="<%=nAccountID%>" />
					<input type="hidden" name="orderid" value="<%=nOrderID%>" />
					<input type="hidden" name="curShipType" value="<%=sShipType%>" />
					<input type="hidden" name="storeid" value="<%=site_id%>" />
                <tr>
                    <td valign="top" width="50">&nbsp;</td>
                    <td valign="top" width="350"><b>Shipping Type:</b></td>
                    <td>
						<div id="curShipType"><%=sShipType%>&nbsp;<a style="cursor:pointer; color:#00F" onclick="editOrderInfo();">(Edit)</a></div>
						<div id="updateShipType" style="display:none;">
							<select name="newShipType">
								<option value="First Class" <%if "First Class" = sShipType then %>selected<% end if%>>First Class</option>
								<option value="First Class Int'l" <%if "First Class Int'l" = sShipType then %>selected<% end if%>>First Class Int'l</option>
								<option value="UPS 2nd Day Air" <%if "UPS 2nd Day Air" = sShipType then %>selected<% end if%>>UPS 2nd Day Air</option>
								<option value="UPS 3 Day Select" <%if "UPS 3 Day Select" = sShipType then %>selected<% end if%>>UPS 3 Day Select</option>
								<option value="UPS Ground" <%if "UPS Ground" = sShipType then %>selected<% end if%>>UPS Ground</option>
								<option value="USPS Express" <%if "USPS Express" = sShipType then %>selected<% end if%>>USPS Express</option>
								<option value="USPS Priority" <%if "USPS Priority" = sShipType then %>selected<% end if%>>USPS Priority</option>
								<option value="USPS Priority Int'l" <%if "USPS Priority Int'l" = sShipType then %>selected<% end if%>>USPS Priority Int'l</option>
							</select>&nbsp;
							<a style="cursor:pointer; color:#F00" onclick="updateOrderInfo('<%=nAccountId%>', '<%=nOrderID%>');"><b>SAVE</b></a> | 
							<a style="cursor:pointer; color:gray" onclick="cancelUpdateOrderInfo();">CANCEL</a>
						</div>
					</td>
                </tr>
				</form>
                <tr>
                    <td valign="top" colspan="3"><br><hr></td>
                </tr>
                <tr>
                    <td valign="top" width="50">&nbsp;</td>
                    <td valign="top" width="350"><b>Shipping & Handling Fee:</b></td>
                    <td><%=formatCurrency(nOrdershippingfee,2)%></td>
                </tr>
                <tr>
                    <td valign="top" colspan="3"><br><hr size="12"></td>
                </tr>
                <tr>
                    <td valign="top" width="50">&nbsp;</td>
                    <td valign="top" width="350" class="totals"><b>Grand Total:</b></td>
                    <td><%=formatCurrency(nOrderGrandTotal,2)%></td>
                </tr>
            </table>
		</td>
	</tr>
</table>
<% 
end if

sub doCancelOrder(pOrderID, bRestock)
	SQL = "UPDATE we_Orders SET cancelled = 1 WHERE orderID = '" & pOrderID & "'"
	oConn.execute SQL
	
	if bRestock then
		SQL = "SELECT itemid, partnumber, quantity FROM we_Orderdetails WHERE orderID = '" & pOrderID & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 0, 1
		do until RS.eof
			SQL = "SELECT PartNumber, itemKit_NEW FROM we_Items WHERE itemID = '" & RS("itemID") & "'"
			set RS2 = Server.CreateObject("ADODB.Recordset")
			RS2.open SQL, oConn, 0, 1
			if not RS2.eof then
				if isnull(RS2("itemKit_NEW")) then
					SQL = "SELECT itemID, inv_qty FROM we_Items WHERE PartNumber = '" & RS2("PartNumber") & "' AND master = 1"
					set RS3 = Server.CreateObject("ADODB.Recordset")
					RS3.open SQL, oConn, 0, 1
					if not RS3.eof then
						SQL = "UPDATE we_Items SET NumberOfSales = NumberOfSales - " & RS("quantity") & " WHERE itemID = '" & RS("itemID") & "'"
						oConn.execute SQL
						
						On Error Resume Next
						sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & RS3("itemID") & "," & RS3("inv_qty") & "," & RS("quantity") & "," & pOrderID & "," & session("adminID") & ",'Cancel Order','" & now & "')"
						session("errorSQL") = sql
						oConn.execute(sql)
						On Error Goto 0
						
						SQL = "UPDATE we_Items SET inv_qty = inv_qty + " & RS("quantity") & " WHERE itemID = '" & RS3("itemID") & "'"
						oConn.execute SQL
					end if
				else
					SQL = "SELECT b.itemID,a.typeID,a.partNumber,b.inv_qty FROM we_items a left join we_items b on a.partNumber = b.partNumber and b.master = 1 WHERE a.itemID IN (" & RS2("itemKit_NEW") & ")"
					set RS3 = Server.CreateObject("ADODB.Recordset")
					RS3.open SQL, oConn, 0, 1
					if not RS3.EOF then
						SQL = "UPDATE we_Items SET NumberOfSales = NumberOfSales - " & RS("quantity") & " WHERE itemID = '" & RS("itemID") & "'"
						oConn.execute SQL
						do while not RS3.EOF
							On Error Resume Next
							sql = "insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & RS3("itemID") & "," & RS3("inv_qty") & "," & RS("quantity") & "," & pOrderID & "," & session("adminID") & ",'Cancel Kit Order','" & now & "')"
							session("errorSQL") = sql
							oConn.execute(sql)
							On Error Goto 0
							
							SQL = "UPDATE we_Items SET inv_qty = inv_qty + " & RS("quantity") & " WHERE itemID = '" & RS3("itemID") & "'"
							oConn.execute SQL
							RS3.movenext
						loop
					end if
				end if
			end if
			RS.movenext
		loop
	
		'UPDATE we_itemsSiteReady table
		sql = 	"select	distinct b.modelid" & vbcrlf & _
				"from	we_orderdetails a join we_items b" & vbcrlf & _
				"	on	a.itemid = b.itemid" & vbcrlf & _
				"where	orderid = '" & pOrderID & "'"
		session("errorSQL") = sql
		set rsSiteReady = oConn.execute(sql)
		if not rsSiteReady.eof then
			do until rsSiteReady.eof
'				response.write rsSiteReady("modelid") & "<br>"
				oConn.execute("sp_createProductListByModelID " & rsSiteReady("modelid"))
				rsSiteReady.movenext
			loop
		end if		
	end if
end sub
%>