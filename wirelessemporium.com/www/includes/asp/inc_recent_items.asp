<%
on error resume next
if typeID = 16 and noLeftNav = 1 then
	maxIndex = 4
else
	maxIndex = 3
end if
for formIndex = 1 to maxIndex
	recentItem = Request.Cookies("RecentItem" & formIndex)("id")
	if inStr(recentItem,"/") > 0 then recentItem = left(recentItem,inStr(recentItem,"/")-1)
	if recentItem <> "" and isNumeric(recentItem) then Execute("thisRecentItem" & formIndex & "id" & "=" & recentItem)
next
on error goto 0

if thisRecentItem1ID <> "" or thisRecentItem2ID <> "" or thisRecentItem3ID <> "" then
	conditionalQuery = ""
	%>
													<tr>
														<td align="left" valign="top" width="100%" style="padding-bottom:20px;">
															<table border="0" align="left" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td align="left" valign="top" width="100%" style="padding-top:20px;">
                                                                        <table border="0" align="left" cellspacing="0" cellpadding="0" width="100%">
																			<tr style="background:url(/images/product/grey_header_bar.jpg) repeat-x;">
                                                                                <td><img src="/images/product/recently_left.jpg" border="0" width="281" height="30" /></td>
                                                                                <td align="right"><img src="/images/product/grey_header_right.jpg" border="0" width="12" height="30" /></td>
																			</tr>
																		</table>
                                                                    </td>
																</tr>
																<tr>
																	<td align="left" valign="top" width="100%" style="border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #CCC; border-bottom-left-radius:15px; border-bottom-right-radius:15px;">
																		<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
																			<tr>
																				<%
																				if thisRecentItem1ID <> "" then conditionalQuery = conditionalQuery & " ItemID = " & thisRecentItem1ID
																				if thisRecentItem2ID <> "" then
																					if conditionalQuery <> "" then conditionalQuery = conditionalQuery & " OR "
																					conditionalQuery = conditionalQuery &  " ItemID = " & thisRecentItem2ID
																				end if
																				if thisRecentItem3ID <> "" then
																				if conditionalQuery <> "" then conditionalQuery = conditionalQuery & " OR "
																					conditionalQuery = conditionalQuery & " ItemID = " & thisRecentItem3ID
																				end if

																				'SQLQuery = "SELECT TOP 3 itemID,itemDesc,itemPic,price_Retail,price_Our,HandsfreeType FROM we_Items WHERE " & conditionalQuery
																				SQLQuery = "SELECT c.brandName, d.modelName, A.itemID, A.itemDesc, A.itemPic, A.price_retail, A.modelID, A.price_Our, A.HandsfreeType, B.typeName FROM we_items A"
																				SQLQuery = SQLQuery & " INNER JOIN we_types B ON A.typeID=B.typeID left join we_brands c on a.brandID = c.brandID left join we_models d on a.modelID = d.modelID"
																				SQLQuery = SQLQuery & " WHERE " & conditionalQuery & " ORDER BY A.numberOfSales DESC"
																				set RS = oConn.execute(SQLQuery)
																				if not RS.eof then
																					a = 0
																					do until RS.eof
																						a = a + 1
																						useBrandName = RS("brandName")
																						useModelName = RS("modelName")
																						
																						if RS("HandsfreeType") = 2 then
																							singularTypeName = "Bluetooth"
																						else
																							singularTypeName = singularSEO(RS("typeName"))
																						end if
																						itemdesc = RS("itemdesc")
																						%>
																						<td align="center" valign="top" width="246" height="100%">
																							<table border="0" cellspacing="0" cellpadding="0" width="246" height="100%">
																								<tr>
																									<td align="center" valign="middle" width="110" rowspan="3" style="padding:5px;">
																									<%
																									dim fsRecent, itemRecent, itemRecentPath
																									set fsRecent = CreateObject("Scripting.FileSystemObject")
																									itemRecent = RS("itemPic")
																									itemRecentPath = Server.MapPath("/productpics/big") & "\" & itemRecent
																					
																									if not fsRecent.FileExists(itemRecentPath) then
																										itemRecent = "imagena.jpg"
																									end if 
																									%>
																										<a href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemDesc"))%>.asp"><img src="/productpics/thumb/<%=itemRecent%>" width="100" height="100" align="center" border="0" alt="<%=RS("itemDesc")%>"></a>
																									</td>
																								</tr>
																								<tr>
																									<td align="left" valign="top" width="132" style="padding:2px;">
																										<br><p style="line-height:14px;margin-top:4px;"><a class="cellphone2-link" href="/p-<%=RS("itemid") & "-" & formatSEO(insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName))%>.asp" title="<%=insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName)%>"><%=insertDetailsAdv(RS("itemDesc"),useBrandName,useModelName)%></a></p>
																									</td>
																								</tr>
																								<tr>
																									<td align="right" valign="bottom" class="boldText" width="132" style="padding:2px;">
																										Our&nbsp;Price:&nbsp;<span class="pricing-orange2">$<%=formatNumber(RS("price_Our"),2)%>&nbsp;</span>
																										<br>
																										<span class="pricing-gray2">List&nbsp;Price:&nbsp;<s>$<%=formatNumber(prepInt(RS("price_retail")),2)%></s>&nbsp;</span>
																										<br>
																										<span class="pricing-gray2">You&nbsp;Save:&nbsp;$<%=formatNumber(prepInt(RS("price_retail")) - RS("price_Our"),2)%>&nbsp;</span>
																									</td>
																								</tr>
																							</table>
																						</td>
																						<%
																						RS.movenext
																						if a < maxIndex and not RS.eof then
																							%>
																							<td align="center" valign="top" width="1" style="border-right:1px dotted #ccc;">&nbsp;</td>
																							<%
																						end if
																					loop
																				end if
																				RS.close
																				set RS = nothing
																				%>
																			</tr>
																		</table>
                                                                    </td>
																</tr>
															</table>
														</td>
													</tr>
	<%
end if
%>
