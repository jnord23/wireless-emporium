<%
FUNCTION CDOSend(strTo,strFrom,strSubject,strBody)
	on error resume next
	Dim objErrMail
	Set objErrMail = Server.CreateObject("CDO.Message")
	With objErrMail
		.From = strFrom
		.To = strTo
		.Subject = strSubject
		.HTMLBody = CStr("" & strBody)
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Item("urn:schemas:httpmail:sender") = "sales@wirelessemporium.com"
		.Configuration.Fields.Update
		.Send
	End With
	Set objErrMail = nothing
	on error goto 0
END FUNCTION

FUNCTION CDOSendTest(strTo,strFrom,strSubject,strBody)
	Dim objErrMail
	Set objErrMail = Server.CreateObject("CDO.Message")
	With objErrMail
		.From = strFrom
		.To = strTo
		.Subject = strSubject
		.HTMLBody = CStr("" & strBody)
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Item("urn:schemas:httpmail:sender") = "sales@wirelessemporium.com"
		.Configuration.Fields.Update
		.Send
	End With
	Set objErrMail = nothing
END FUNCTION
%>
