﻿<%
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.Open Session("ConnectionString")
	
	sql = "select * from we_brandText where brandID = " & brandID
	session("errorSQL") = SQL
	set brandTxtRS = Server.CreateObject("ADODB.Recordset")
	brandTxtRS.open SQL, oConn, 0, 1
	
	if not brandTxtRS.EOF then
		SEtitle = brandTxtRS("seTitle")
		SEdescription = brandTxtRS("seDescription")
		SEkeywords = brandTxtRS("seKeywords")
		titleText = brandTxtRS("titleText")
		topText = brandTxtRS("topText")
		bottomText = brandTxtRS("bottomText")
		brandStaticText = brandTxtRS("brandStaticText")
	end if
	phoneTitle = "Phone"
	cellPhoneTitle = "Cell Phone"
	
	on error resume next
	if request.querystring("id") <> "" then
		Response.Cookies("vendororder") = request.querystring("id")
		Response.Cookies("vendororder").Expires = date + 45
	elseif request.querystring("utm_source") <> "" then
		Response.Cookies("vendororder") = request.querystring("utm_source")
		Response.Cookies("vendororder").Expires = date + 45
	elseif request.querystring("refer") <> "" then
		Response.Cookies("vendororder") = request.querystring("refer")
		Response.Cookies("vendororder").Expires = date + 45
	elseif request.querystring("clickid") <> "" then
		Response.Cookies("vendororder") = "clickid=" & request.querystring("clickid")
		Response.Cookies("vendororder").Expires = date + 45
	elseif request.querystring("source") <> "" then
		Response.Cookies("vendororder") = request.querystring("source")
		Response.Cookies("vendororder").Expires = date + 45
	end if
	on error goto 0
	
	if isnull(session("adminID")) or len(session("adminID")) < 1 then session("adminID") = 0
	if isnull(session("adminLvl")) or len(session("adminLvl")) < 1 then session("adminLvl") = 0
	
	userBrowser = request.ServerVariables("HTTP_USER_AGENT")
	mobileAccess = request.QueryString("mobileAccess")
	if isnull(mobileAccess) or len(mobileAccess) < 1 then mobileAccess = ""
	
	if isnull(session("mobileAccess")) or len(session("mobileAccess")) < 1 then
		'no mobile bypass (allows mobile users to access standard site)
		if (instr(userBrowser,"Mobile") > 0 or instr(userBrowser,"BlackBerry") > 0) and instr(userBrowser,"iPad") < 1 then
			'detected a mobile browser
			if mobileAccess = "" then
				'no mobile bypass in QS - move to mobile site
				additionalQS = mid(request.ServerVariables("HTTP_X_REWRITE_URL"),instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"?") + 1)
				if len(additionalQS) > 0 then
					response.Redirect("http://m.wirelessemporium.com?" & additionalQS)
				else
					response.Redirect("http://m.wirelessemporium.com")
				end if
			else
				'mobile bypass found in QS - save session
				session("mobileAccess") = 1
			end if
		end if
	end if
	
	function ds(strValue)
		strContent = trim(strValue)
		if not isNull(strContent) and strContent <> "" then
			strContent = trim(replace(strContent,"'","''"))
			strContent = trim(replace(strContent,chr(34),"''''"))
			strContent = replace(strContent,"%26%2343%3B1","+")
			strContent = replace(strContent,"&#43;","+")
			strContent = replace(strContent,Chr(39),"''")
			strContent = replace(strContent,";","")
			strContent = replace(strContent,"--","")
			strContent = replace(strContent,"/*","")
			strContent = replace(strContent,"*/","")
			strContent = replace(strContent,"xp_","")
			strContent = trim(replace(strContent,chr(146),"''"))
			strContent = trim(replace(strContent,chr(147),"''"))
			strContent = trim(replace(strContent,chr(148),"''"))
		else
			strContent = strValue
		end if
		ds = strContent
	end function
	
	function formatSEO(val)
		if not isNull(val) then
			formatSEO = lCase(trim(replace(val,"&trade;","-")))
			formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
			formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),":","-"),"+","-")
			formatSEO = replace(formatSEO,"---","-")
			formatSEO = replace(formatSEO,"--","-")
		else
			formatSEO = ""
		end if
		select case formatSEO
			case "sidekick" : formatSEO = "t-mobile-sidekick"
			case "at-t---cingular" : formatSEO = "att-cingular"
			case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
			case "sprint---nextel" : formatSEO = "sprint-nextel"
			case "u-s--cellular" : formatSEO = "us-cellular"
			case "antennas-parts" : formatSEO = "antennas"
			case "faceplates" : formatSEO = "faceplates-skins"
			case "verizon" : formatSEO = "verizon-wireless"
			case "unlocked" : formatSEO = "unlocked"
			case "audiovox" : formatSEO = "utstarcom"
			case "data-cable-memory" : formatSEO = "data-cables-memory-cards"
			case "hands-free" : formatSEO = "hands-free-bluetooth-headsets"
			case "holsters-belt-clips" : formatSEO = "holsters-holders"
			case "leather-cases" : formatSEO = "cases-and-covers"
			case "full-body-protectors" : formatSEO = "full-body-skins"
		end select
		formatSEO = replace(formatSEO,"---","-")
		formatSEO = replace(formatSEO,"--","-")
	end function
	
	'Normal Page Start
	set fso = CreateObject("Scripting.FileSystemObject")
	Set Img = Server.CreateObject("Persits.Jpeg")
	set Jpeg = Server.CreateObject("Persits.Jpeg")
	Jpeg.Quality = 60
	Jpeg.Interpolation = 1
	
	sql	=	"select	c.orderNum, a.modelid, a.modelname, a.modelimg, a.international, b.brandname, b.brandimg " & vbcrlf & _
			"from we_models a with (nolock) join we_brands b with (nolock) " & vbcrlf & _
			"on a.brandid = b.brandid " & vbcrlf & _
			"left join we_popPhones c on a.modelid = c.modelID " & vbcrlf & _
			"where a.modelname is not null and a.hidelive = 0 and a.brandid = '" & brandid & "' " & vbcrlf & _
			"and a.modelname not like 'all model%'" & vbcrlf
			
	if brandID = 17 and phoneOnly = 1 then
		SQL = SQL & " and a.modelName not like '%iPod%' and a.modelName not like '%iPad%'" & vbcrlf
	elseif brandID = 17 and phoneOnly = 0 then
		phoneTitle = "Product"
		cellPhoneTitle = ""
		SQL = SQL & " and a.modelName not like '%iPhone%'" & vbcrlf
	end if
	
	if cbCarrier <> "" then
		fileExt = "_" & cbCarrier
		showPopPhones = 0
		sql	= sql & " and a.carriercode like '%' + '" & cbCarrier & "' + ',%'" & vbcrlf
	end if
	
	if cbSort <> "" then
		fileExt = fileExt & "_" & cbSort
		if noSort = 0 then showPopPhones = 0 else showPopPhones = 1
		select case ucase(cbSort)
			case "AZ"
				sqlOrder = " order by orderNum desc, international, modelname"
			case "ZA"
				sqlOrder = " order by orderNum desc, international, modelname desc"	
			case "NO"
				sqlOrder = " order by orderNum desc, international, releaseyear desc, releasequarter desc, modelname"		
			case "ON"
				sqlOrder = " order by orderNum desc, international, releaseyear, releasequarter, modelname"
		end select
	end if
	
	sql	= sql & sqlOrder
	session("errorSQL") = SQL
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	
	if RS.eof then response.redirect("/index.asp")
	
	a = 0
	topProds = ""
	selectProds = ""
	displayProds = ""
	do while not RS.EOF
		a = a + 1
		orderNum = RS("orderNum")
		modelID = RS("modelid")
		modelName = RS("modelname")
		modelImg = RS("modelImg")
		international = RS("international")
		if a = 1 then
			brandName = RS("brandName")
			brandImg = RS("brandImg")
		end if
		rs.movenext
		if a <= maxModels then
			if not isnull(orderNum) then
				if showPopPhones = 1 then
					topProds = topProds & orderNum & "*" & modelID & "*" & modelName & "*" & modelImg & "#"
				else
					displayProds = displayProds & modelID & "*" & modelName & "*" & modelImg & "*" & international & "#"
				end if
			else
				displayProds = displayProds & modelID & "*" & modelName & "*" & modelImg & "*" & international & "#"
			end if
		end if
		selectProds = selectProds & modelID & "*" & modelName & "#"
	loop
	
	if topProds <> "" then topProds = left(topProds,len(topProds)-1)
	if displayProds <> "" then displayProds = left(displayProds,len(displayProds)-1)
	if selectProds <> "" then selectProds = left(selectProds,len(selectProds)-1)
	
	if showPopPhones = 1 then
		popItemImgPath = formatSEO(brandName) & "_pop.jpg"
	end if
	
	if maxModels > 40 then
		if brandID = 17 and phoneOnly = 1 then
			itemImgPath = "applePhones_all" & fileExt & ".jpg"
		else
			itemImgPath = formatSEO(brandName) & "_all" & fileExt & ".jpg"
		end if
	else
		itemImgPath = formatSEO(brandName) & "_p" & viewPage & fileExt & ".jpg"
	end if
	
	if SEtitle = "" then SEtitle = "Cell Phone Accessories for " & brandName & ": Discount Accessories from Wireless Emporium, " & brandName & " cell phone Accessories, " & brandName & " Mobile phone Accessories, " & brandName & " Wireless phone Accessories"
	if SEdescription = "" then SEdescription = "Discount Accessories for " & brandName & " and " & brandName & " Wireless Phone Accessories at Wireless Emporium. Your source for wholesale " & brandName & " cell phone Accessories, wholesale " & brandName & " cellular Accessories, discount " & brandName & " cell phone Accessories and cheap " & brandName & " cell phone Accessories"
	if SEkeywords = "" then SEkeywords = "Discount Accessories for " & brandName & "," & brandName & " Accessories," & brandName & " cell phone Accessories," & brandName & " cell phone Accessories," & brandName & " discount cellular Accessories," & brandName & " mobile phone Accessories," & brandName & " wireless Accessories," & brandName & " cellular telephone Accessories," & brandName & " wholesale cellular Accessories," & brandName & " cellular Accessories," & brandName & " wireless phone Accessories," & brandName & " wholesale cell phone Accessories," & brandName & " cellular phone Accessories,wholesale " & brandName & " wireless Accessories,wholesale " & brandName & " cellular phone Accessories,discount " & brandName & " cell phone Accessories,cool " & brandName & " cell phone Accessories"
	if titleText = "" then titleText = "Discount " & brandName & " Cell Phone Chargers, Batteries &amp; Other " & brandName & " Accessories"
	
	dim strBreadcrumb
	if brandID = 17 and phoneOnly = 0 then
		strBreadcrumb = brandName & " iPod/iPad Accessories"
		session("breadcrumb_brand") = brandName & " iPod/iPad "
	else
		strBreadcrumb = brandName & " Cell Phone Accessories"
		session("breadcrumb_brand") = brandName
	end if
	
	if topProds = "" then showPopPhones = 0
	if showPopPhones = 1 then
		session("errorSQL") = "fileExists: /images/stitch/" & popItemImgPath
		if not fso.FileExists(Server.MapPath("/images/stitch/" & popItemImgPath)) then
			'create single product image
			buildArray = split(topProds,"#")
			imgWidth = 70 * (ubound(buildArray) + 1)
			session("errorSQL") = "70 * (" & ubound(buildArray) + 1 & ") = " & imgWidth
			session("errorSQL2") = "topProds:" & topProds
			Jpeg.New imgWidth, 112, &HFFFFFF
			imgX = 0
			imgY = 0
			imgCnt = 0
			for i = 0 to ubound(buildArray)
				curBuildArray = split(buildArray(i),"*")
				if curBuildArray(3) <> "" then
					if not fso.FileExists(Server.MapPath("/productPics/models/thumbs/") & "\" & curBuildArray(3)) then
						inner_itemImgPath = Server.MapPath("/productPics/models/thumbs/") & "\" & curBuildArray(3)
						inner_itemImgPath2 = Server.MapPath("/productPics/models/") & "\" & curBuildArray(3)
						if not fso.FileExists(inner_itemImgPath) then
							Jpeg.Open inner_itemImgPath2
							Jpeg.Height = 112
							Jpeg.Width = 70
							Jpeg.Save inner_itemImgPath
						end if
					end if
					Img.Open Server.MapPath("/productPics/models/thumbs/") & "\" & curBuildArray(3)
					Jpeg.Canvas.DrawImage X + (imgX)/2, Y + (imgY)/2, Img
					imgX = imgX + 140
				end if
			next
			Jpeg.Save Server.MapPath("/images/stitch/" & popItemImgPath)
		end if
	end if
	
	session("errorSQL2") = "fileExists: /images/stitch/" & itemImgPath
	if not fso.FileExists(Server.MapPath("/images/stitch/" & itemImgPath)) then
		'create single product image
		buildArray = split(displayProds,"#")
		imgWidth = 70 * (ubound(buildArray) + 1)
		Jpeg.New imgWidth, 112, &HFFFFFF
		imgX = 0
		imgY = 0
		for i = 0 to ubound(buildArray)
			curBuildArray = split(buildArray(i),"*")
			if curBuildArray(2) <> "" then
				if not fso.FileExists(Server.MapPath("/productPics/models/thumbs/") & "\" & curBuildArray(2)) then
					session("errorSQL") = "1:" & Server.MapPath("/productPics/models/thumbs/") & "\" & curBuildArray(2)
					session("errorSQL2") = "2:" & Server.MapPath("/productPics/models/") & "\" & curBuildArray(2) & "<br>Array:" & buildArray(i)
					inner_itemImgPath = Server.MapPath("/productPics/models/thumbs/") & "\" & curBuildArray(2)
					inner_itemImgPath2 = Server.MapPath("/productPics/models/") & "\" & curBuildArray(2)
					if not fso.FileExists(inner_itemImgPath) then
						Jpeg.Open inner_itemImgPath2
						Jpeg.Height = 112
						Jpeg.Width = 70
						Jpeg.Save inner_itemImgPath
					end if
				end if
				Img.Open Server.MapPath("/productPics/models/thumbs/") & "\" & curBuildArray(2)
				Jpeg.Canvas.DrawImage X + (imgX)/2, Y + (imgY)/2, Img
				imgX = imgX + 140
			end if
		next
		session("errorSQL") = itemImgPath
		session("errorSQL2") = "2 is:" & itemImgPath
		Jpeg.Save Server.MapPath("/images/stitch/" & itemImgPath)
	end if
	
	session("breadcrumb_model") = ""
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%=brandName%> Cell Phone Accessories – <%=brandName%> Cell Phone Covers Cases Batteries And More</title>
	<meta name="Description" content="<%=SEdescription%>" />
	<meta name="Keywords" content="<%=SEkeywords%>" />
	<meta name="robots" content="index,follow" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- This site contains information about: cell phone accessories,cheapest cell phone accessories,cell phone faceplate,cell phone covers,cell phone battery,cellular battery,cell phone charger,discount cell phone chargers,hands free kit,hands-free -->
	<meta name="verify-v1" content="kygxtWzRMtPejZFFDjdkwO7wTNu3kxWwO3M/Q6WGJCs=" />
	<meta name="verify-v1" content="JXXhlKPTKULNfZeB9M5Qxp3AW1u4DQRl/PJZ4NLAfEs=" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <style type="text/css">
		body { font-family: Arial, Helvetica, sans-serif; }
		.brandTitle { font-size: 18pt; color: #<%=h1color%>; font-weight: normal; margin-left:11px; margin-bottom:1px; }
		.headerTxt { font-size: 10px; color: #000000; font-weight: bold; }
		.xtrasmlIndexText { font-size: 8pt; color: #666666; line-height: 14px; }
		a.xtrasmlIndexLink { font-size: 8pt; font-weight: normal; text-decoration: none; color: #CC3300; }
		a.topnav { font-size: 10pt; font-weight: bold; text-decoration: none; color: #FFFFFF; }
		.signLink { font-size: 9pt; font-weight: normal; color: #666666; text-decoration: none; }
		a.sign-link { font-size: 9pt; font-weight: bold; color: #666666; text-decoration: none; }
		a.category { font-size: 10pt; font-weight: bold; color: #000000; text-decoration: none; margin-bottom: 0px; }
		a.product-description4 { font-size: 10pt; font-weight: normal; color: #000000; text-decoration: none; }
		.pricing-gray { font-size: 10pt; color: #999999; font-weight: normal; }
		.pricing-orange { font-size: 12pt; color: #FF6633; font-weight: bold; }
		.TopModelLink { font-size: 9pt; font-weight: normal; color: #999999; text-decoration: none; }
		a.TopModelLink { font-size: 9pt; font-weight: bold; color: #999999; text-decoration: none; }
		a.bottom-links { font-size: 8pt; font-weight: bold; color: #000000; text-decoration: none; }
		a.bottom-accessory-links { font-size: 8pt; font-weight: normal; color: #000000; text-decoration: none; }
		.bottom-text { font-size: 8pt; font-weight: bold; color: #000000; }
		.bottomText { font-size: 8pt; color: #333333; font-weight: normal; letter-spacing:-1px; }
		a.bottomText-link { font-size: 8pt; font-weight: normal; color: #CC3300; text-decoration: none; letter-spacing:-1px; }
		a.leftnav { font-size: 11pt; font-weight: bold; text-decoration: none; color: #666666; line-height: 22px; }
		a.blog-link { font-size: 11pt; font-weight: bold; text-decoration: none; color: #6699CC; }
		a.blog-full { font-size: 9pt; font-weight: bold; text-decoration: underline; color: #3366AA; }
		.smlText { font-size: 8pt; }
		a.cellphone-font2 { font-size: 10pt; font-weight: bold; color: #000000; text-decoration: none; }
		a.breadcrumb { font-size: 8pt; color: #FF6600; text-decoration: none; }
		.breadcrumbFinal { font-size: 8pt; color: #000; }
		a.paginationOff { font-weight:bold; font-size:18px; color:#666; cursor:pointer; }
		a.paginationOff:hover { font-weight:bold; font-size:18px; color:#03C; text-decoration:underline; cursor:pointer; }
		
		/*
		Copyright (c) 2008, Yahoo! Inc. All rights reserved.
		Code licensed under the BSD License:
		http://developer.yahoo.net/yui/license.txt
		version: 2.6.0
		*/
		#nxt-ac-container{position:absolute;width:20em;z-index:9050;}
		#nxt-ac-container .yui-ac-content{position:absolute;width:100%;border:1px solid #808080;background:#fff;overflow:hidden;z-index:9050;}
		#nxt-ac-container .yui-ac-content ul{margin:0;padding:0;width:100%;text-align:left;font-family:arial;}
		#nxt-ac-container .yui-ac-content li{margin:0;padding:2px 5px;cursor:default;white-space:nowrap;}
		#nxt-ac-container .yui-ac-content li.yui-ac-highlight{background:#426FD9;color:#FFF;}
		
		body #google_amark_b, body #google_amark_b * { text-align:left !important; padding:0 !important; margin:0 !important; border:0 !important; position:relative !important; font-weight:normal !important; text-decoration:none !important; font-size:11px !important; font-family:Arial, sans-serif !important; background:#fff !important; float:none !important; }
		#google_amark_b .m img, #google_amark_b #t img, #google_amark_b #x a { display:block !important; }
		#google_amark_b .h { position:absolute !important; width:325px !important; border:1px solid #ccc !important; }
		#google_amark_b #t { padding:6px 0 1px 0px !important; }
		#google_amark_b #l { left:10px !important; }
		#google_amark_b #x { position:absolute !important; right:0px !important; top:0px !important; border-left:1px solid #ccc !important; border-bottom:1px solid #ccc !important; }
		#google_amark_b #c { padding:5px 10px 10px 10px !important; color:#676767 !important; border-top:1px solid #ccc !important;  }
		#google_amark_b #c p { padding:5px 0 0 0 !important; }
		#google_amark_b #c p.p { padding:0 !important; }
		#google_amark_b #c p.p img { top: 5px !important; }
		#google_amark_b #c p a:link, #google_amark_b #c p a:visited { color:#0000CC !important; text-decoration:underline !important; }
		
		<%
		'Popular models on page
		if showPopPhones = 1 then
			useX = 0
			allProdArray = split(topProds,"#")
			for i = 0 to ubound(allProdArray)
				curProdArray = split(allProdArray(i),"*")
				if curProdArray(3) <> "" then
		%>
		#popPhonePic<%=i+1%> { width: 70px; height: 112px; background: url(/images/stitch/<%=popItemImgPath%>) <%=useX%>px 0px no-repeat; }
		<%
					useX = useX - 70
				end if
			next
		end if
		%>
		<%
		'Other models on page
		useX = 0
		allProdArray = split(displayProds,"#")
		for i = 0 to ubound(allProdArray)
			curProdArray = split(allProdArray(i),"*")
			if curProdArray(2) <> "" then
		%>
		#PhonePic<%=i+1%> { width: 70px; height: 112px; background: url(/images/stitch/<%=itemImgPath%>) <%=useX%>px 0px no-repeat; }
		<%
				useX = useX - 70
			end if
		next
		%>
	</style>
</head>
<body>
<!-- Google AdSense Start -->
<script type="text/javascript" charset="utf-8">
var pageOptions = {
'pubId' : 'pub-1001075386636876',
<% if len(brandName) > 0 then %>
	<% if len(modelName) > 0 then %>
		<% if len(categoryName) > 0 then %>
'query' : '<%=brandName%>' + ' ' + '<%=modelName%>' + ' ' + '<%=categoryName%>'
		<% else %>
'query' : '<%=brandName%>' + ' ' + '<%=modelName%>'
		<% end if %>
	<% else %>
		<% if len(categoryName) > 0 then %>
'query' : '<%=brandName%>' + ' ' + '<%=categoryName%>'
		<% else %>
'query' : '<%=brandName%>'
		<% end if %>
	<% end if %>
};
<% elseif len(categoryName) > 0 then %>
'query' : '<%=categoryName%>'
};
<% else %>
'query' : 'Cell Phones'
};
<% end if %>
var adblock1 = {
'container' : 'adcontainer1',
'number' : 3,
'width' : 'auto',
'lines' : 2,
'fontFamily' : 'arial',
'fontSizeTitle' : '14px',
'fontSizeDescription' : '14px',
'fontSizeDomainLink' : '14px',
'linkTarget' : '_blank'
};
var adblock2 = {
'container' : 'adcontainer2',
'number' : 4,
'width' : '250px',
'lines' : 3,
'fontFamily' : 'arial',
'fontSizeTitle' : '12px',
'fontSizeDescription' : '12px',
'fontSizeDomainLink' : '12px',
'linkTarget' : '_blank'
};
<%
if isnull(googleAds) or len(googleAds) < 1 then googleAds = 0
if googleAds = 1 then
%>
//new google.ads.search.Ads(pageOptions, adblock1);
<% elseif googleAds = 2 then %>
//new google.ads.search.Ads(pageOptions, adblock1, adblock2);
<% end if %>
</script>
<!-- Google AdSense Close -->
<table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
	<% if not isnull(session("adminID")) and len(session("adminID")) > 0 then %>
    	<% if session("adminNav") then %>
    <tr bgcolor="#FF0000"><td style="font-weight:bold; color:#FFF; font-size:12px; padding:5px;">Admin Options: <a href="/admin/menu.asp" style="color:#FFF;">Admin Menu</a><%=otherAdminOptions%></td></tr>
    	<% end if %>
	<% end if %>
	<tr>
    	<td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left">
                        <%
                        if month(date) = 12 and day(date) < 27 then
                            holidayLogo = 1
                        elseif (month(date) = 12 and day(date) > 26) or (month(date) = 1 and day(date) < 7) then
                            holidayLogo = 2
                        else
                            holidayLogo = 0
                        end if
                        if holidayLogo = 0 then
                        %>
                        <a href="/"><img src="/images/we_logo_sm.gif" width="450" height="69" border="0" alt="Cell Phone Accessories &#8211; Wireless Emporium" /></a>
                        <% elseif holidayLogo = 1 then %>
                        <a href="/"><img src="/images/we_xmas_logo.jpg" width="475" height="69" border="0" alt="Cell Phone Accessories &#8211; Wireless Emporium" /></a>
                        <% elseif holidayLogo = 2 then %>
                        <a href="/"><img src="/images/we_ny_logo.jpg" border="0" alt="Cell Phone Accessories &#8211; Wireless Emporium" /></a>
                        <% end if %>
                    </td>
                    <td width="545" align="right" valign="top">
                        <table width="530" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="100%" align="right" class="signLink2" colspan="3">
									<% if myAccount <> "" then %>
									<a href="<%=useLink%>/register/default.asp?pf=modify" class="sign-link"><b>Welcome, <%=sUserfName%></b></a><span style="color:#999;">&nbsp;&bull;&nbsp;</span><a href="/register/logout.asp" class="sign-link">Log&nbsp;Out</a><span style="color:#999;">&nbsp;&bull;&nbsp;</span><a href="/register/logout.asp?s=not" class="sign-link">I'm not <%=sUserfName%></a><span style="color:#999;">&nbsp;&bull;&nbsp;</span>
									<a href="/faq.asp" rel="nofollow" class="sign-link">Help</a>
									<% else %>
									<a href="<%=useLink%>/register/login.asp?pf=register" class="sign-link">Sign In</a><span style="color:#999;">&nbsp;&bull;&nbsp;</span><a href="/faq.asp" rel="nofollow" class="sign-link">Help</a>
									<% end if %>
									<%
									if cart <> 1 then
										if miniTotalQuantity > 0 and miniSubTotal > 0 then
									%>
									<span style="color:#999;">&nbsp;&bull;&nbsp;</span><a href="/cart/basket.asp" rel="nofollow" class="sign-link2">Shopping Bag (<%=miniTotalQuantity%>)</a>
                                    <%
                                    	end if
                                    end if
									%>
								</td>
							</tr>
							<tr>
								<td width="100%" align="right">
									<div id="japan" style="float:left;" onclick="window.open('/helpJapan.asp','HelpJapan')"></div>
									<div style="float:left; margin-top:10px;"><script type="text/javascript">var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");document.write(unescape("%3Cscript src='" + lhnJsHost + "www.livehelpnow.net/lhn/scripts/lhnvisitor.aspx?div=&amp;zimg=426&amp;lhnid=3410&amp;iv=&amp;iwidth=115&amp;iheight=35&amp;zzwindow=2636&amp;d=0&amp;custom1=&amp;custom2=&amp;custom3=' type='text/javascript'%3E%3C/script%3E"));</script></div>
								</td>
								<td width="5">&nbsp;&nbsp;</td>
								<td>
									<table width="100" border="0" cellpadding="0" cellspacing="0" title="Click to Verify - This site chose VeriSign Trust Seal to promote trust online with consumers." >
										<tr>
                                        	<td width="100%" align="center" valign="top"><a href="javascript:Open_Popup('https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.WIRELESSEMPORIUM.COM&lang=en','VeriSign','width=560,height=500,location=yes,status=yes,resizable=yes,scrollbars=yes');"><img src="/images/verisign.jpg" width="80" height="45" border="0" alt="VeriSign Secured Site" /></a></td>
                                        </tr>
									</table>
								</td>
							</tr>
						</table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="100%" height="34" align="center" style="background-image: url('/images/bg_cat_header2.jpg'); background-repeat: repeat; background-position: center bottom;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr>
                    <td width="126" align="center"><a href="/unlocked-cell-phones.asp" class="topnav" title="Unlocked Cell Phones">Unlocked&nbsp;Phones</a></td>
                    <td width="7" align="center"><span style="color:#fff;">&nbsp;&bull;&nbsp;</span></td>
                    <td width="158" align="center"><a href="/cell-phone-covers-faceplates-screen-protectors.asp" class="topnav" title="Cell Phone Faceplates - Protectors">Faceplates/Protectors</a></td>
                    <td width="7" align="center"><span style="color:#fff;">&nbsp;&bull;&nbsp;</span></td>
                    <td width="70" align="center"><a href="/cell-phone-batteries.asp" class="topnav" title="Cell Phone Batteries">Batteries</a></td>
                    <td width="7" align="center"><span style="color:#fff;">&nbsp;&bull;&nbsp;</span></td>
                    <td width="70" align="center"><a href="/cell-phone-chargers.asp" class="topnav" title="Cell Phone Chargers">Chargers</a></td>
                    <td width="7" align="center"><span style="color:#fff;">&nbsp;&bull;&nbsp;</span></td>
                    <td width="50" align="center"><a href="/cell-phone-cases.asp" class="topnav" title="Cell Phone Cases">Cases</a></td>
                    <td width="7" align="center"><span style="color:#fff;">&nbsp;&bull;&nbsp;</span></td>
                    <td width="72" align="center"><a href="/bluetooth-headsets-handsfree.asp" class="topnav" title="Bluetooth Headset">Headsets</a></td>
                    <td width="7" align="center"><span style="color:#fff;">&nbsp;&bull;&nbsp;</span></td>
                    <td width="122" align="center"><a href="/cell-phone-holsters-belt-clips-holders.asp" class="topnav" title="Cell Phone Holsters - Holders">Holsters/Holders</a></td>
                    <td width="7" align="center"><span style="color:#fff;">&nbsp;&bull;&nbsp;</span></td>
                    <td width="90" align="center"><a href="/cell-phone-data-cables-memory-cards.asp" class="topnav" title="Cell Phone Data Cables">Data&nbsp;Cables</a></td>
                    <td width="7" align="center"><span style="color:#fff;">&nbsp;&bull;&nbsp;</span></td>
                    <td width="114" align="center"><a href="/cell-phone-antennas.asp" class="topnav" title="Cell Phone Antennas - Parts">Antennas/Parts</a></td>
                    <td width="7" align="center"><span style="color:#fff;">&nbsp;&bull;&nbsp;</span></td>
                    <td width="65" align="center"><a href="/cell-phone-charms-bling-kits.asp" class="topnav" title="Bling Kits - Cell Phone Charms">Charms</a></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="100%" height="35" align="left" style="background-image: url('/images/bg_search2.jpg'); background-repeat: repeat; background-position: center top;">
        	<form name="Mainsearch" method="get" action="/search/search.asp" id="nxt-ac-form">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                    <td width="100%" align="left" class="search">
                        <input type="hidden" name="from" id="Hidden1" />
                        <input type="hidden" name="FrmSearchWords" id="FrmSearchWords" />
                        &nbsp;<input type="text" id="nxt-ac-searchbox" autocomplete="off" name="keywords" value="Find Your Product" onFocus="this.value='';" class="nicetextbox" style="color: #999999;" size="40" maxlength="60" />&nbsp;<input type="image" src="/images/search_button2.jpg" alt="GO" />
                    </td>
                    <td nowrap="nowrap" style="padding-top:1px;">
                    	<table border="0" cellpadding="0" cellspacing="0">
                        	<tr>
                            	<td nowrap="nowrap" style="text-align:right; font-size:9px; color:#333; font-weight:bold; padding-right:20px;">MON - FRI 8AM - 5PM PST<br /><span style="font-size:16px;">ORDER NOW! 800-305-1106</span></td>
                                <td nowrap="nowrap" style="background-color:#cc0000; color:#FFF; font-size:16px; font-weight:bold; padding:2px;"><div style="border:1px solid #FFF; height:17px; padding:5px 10px 5px 10px;">*FREE SHIPPING ON EVERY ORDER!</div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
	<tr>
		<td>
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td valign="top">
                    	<div style="width:200px; vertical-align:top; padding-top:10px;">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td width="100%" align="center" valign="top" bgcolor="#EBEBEB">
									<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr><td valign="top" align="center"><img src="/images/leftnav/header_shop_brands.jpg" width="185" height="36" border="0" alt="Shop Brands" /></td></tr>
										<tr><td align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/iphone-accessories.asp" class="leftnav" title="iPhone Accessories">iPHONE</a></td></tr>
                                        <tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/ipod-ipad-accessories.asp" class="leftnav" title="iPod/iPad Accessories">iPOD / iPAD</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/blackberry-cell-phone-accessories.asp" class="leftnav" title="BlackBerry Accessories">BLACKBERRY</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/htc-cell-phone-accessories.asp" class="leftnav" title="HTC Accessories">HTC</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/kyocera-cell-phone-accessories.asp" class="leftnav" title="Kyocera Accessories">KYOCERA</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/lg-cell-phone-accessories.asp" class="leftnav" title="LG Accessories">LG</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/motorola-cell-phone-accessories.asp" class="leftnav" title="Motorola Accessories">MOTOROLA</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/nextel-cell-phone-accessories.asp" class="leftnav" title="Nextel Accessories">NEXTEL</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/nokia-cell-phone-accessories.asp" class="leftnav" title="Nokia Accessories">NOKIA</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/palm-cell-phone-accessories.asp" class="leftnav" title="Palm Accessories">PALM</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/pantech-cell-phone-accessories.asp" class="leftnav" title="Pantech Accessories">PANTECH</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/samsung-cell-phone-accessories.asp" class="leftnav" title="Samsung Accessories">SAMSUNG</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/sanyo-cell-phone-accessories.asp" class="leftnav" title="Sanyo Accessories">SANYO</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/sidekick-cell-phone-accessories.asp" class="leftnav" title="Sidekick Accessories">SIDEKICK</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/siemens-cell-phone-accessories.asp" class="leftnav" title="Siemens Accessories">SIEMENS</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/sony-ericsson-cell-phone-accessories.asp" class="leftnav" title="Sony Ericsson Accessories">SONY ERICSSON</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/utstarcom-cell-phone-accessories.asp" class="leftnav" title="UTStarcom Accessories">UTSTARCOM</a></td></tr>
										<tr><td valign="top" align="left"><span style="color:#f2661b; font-size:24px;">&nbsp;&bull;&nbsp;</span><a href="/other-cell-phone-accessories.asp" class="leftnav" title="Other Brand Name Cell Phone Accessories">...OTHER</a></td></tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="left" valign="top" bgcolor="#EBEBEB">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr><td valign="top" align="center"><img src="/images/leftnav/header_shop_carriers.jpg" width="185" height="36" border="0" alt="Shop Carriers" /></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px; padding-left:7px;">&nbsp;&bull;&nbsp;</span><a href="/car-2-att-cingular-accessories.asp" class="leftnav" title="AT&amp;T / Cingular Accessories">AT&amp;T / CINGULAR</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px; padding-left:7px;">&nbsp;&bull;&nbsp;</span><a href="/car-4-sprint-nextel-accessories.asp" class="leftnav" title="Sprint / Nextel Accessories">SPRINT / NEXTEL</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px; padding-left:7px;">&nbsp;&bull;&nbsp;</span><a href="/car-5-t-mobile-accessories.asp" class="leftnav" title="T-Mobile Accessories">T-MOBILE</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px; padding-left:7px;">&nbsp;&bull;&nbsp;</span><a href="/car-6-verizon-accessories.asp" class="leftnav" title="Verizon Accessories">VERIZON</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px; padding-left:7px;">&nbsp;&bull;&nbsp;</span><a href="/car-1-alltel-accessories.asp" class="leftnav" title="Alltel Accessories">ALLTEL</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px; padding-left:7px;">&nbsp;&bull;&nbsp;</span><a href="/car-8-boost-mobile-southern-linc-accessories.asp" class="leftnav" title="Boost Mobile / Southern Linc Accessories">BOOST MOBILE</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px; padding-left:7px;">&nbsp;&bull;&nbsp;</span><a href="/car-9-cricket-accessories.asp" class="leftnav" title="Cricket Accessories">CRICKET</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px; padding-left:7px;">&nbsp;&bull;&nbsp;</span><a href="/car-3-metro-pcs-accessories.asp" class="leftnav" title="Metro PCS Accessories">METRO PCS</a></td></tr>
										<tr><td valign="top" align="left" style="border-bottom:1px solid #CCC;"><span style="color:#f2661b; font-size:24px; padding-left:7px;">&nbsp;&bull;&nbsp;</span><a href="/car-7-prepaid-accessories.asp" class="leftnav" title="Prepaid Cell Phone Accessories">PREPAID</a></td></tr>
										<tr><td valign="top" align="left"><span style="color:#f2661b; font-size:24px; padding-left:7px;">&nbsp;&bull;&nbsp;</span><a href="/car-11-us-cellular-accessories.asp" class="leftnav" title="U.S. Cellular Accessories">U.S. CELLULAR</a></td></tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" align="center" style="padding-top:10px;">
									<div style="width:180px; height:76px; background-image:url(/images/leftnav/connectwithus.jpg); padding-top:28px;">
										<a href="http://www.facebook.com/wirelessemporium/" target="_blank"><img src="/images/leftnav/icon_fb.jpg" width="45" height="45" border="0" alt="Connect to us on Facebook!" /></a>
                                        <a href="http://twitter.com/wirelessemp/" target="_blank"><img src="/images/leftnav/icon_tw.jpg" width="45" height="45" border="0" alt="Connect to us on Twitter!" /></a>
                                        <a href="http://www.myspace.com/wirelessemporium" target="_blank"><img src="/images/leftnav/icon_ms.jpg" width="45" height="45" border="0" alt="Connect to us on MySpace!" /></a>
									</div>
                                </td>
                            </tr>
							<tr>
								<td width="100%" valign="top" align="center">
									<a href="/blog"><img src="/images/leftnav/WE_blog_button.jpg" width="177" height="50" border="0" alt="Wireless Emporium Blog" /></a>
								</td>
							</tr>
							<tr>
								<td valign="top" align="center">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td width="83" align="center">
												<img src="/images/remoteImages/sc.gif" width="72" height="73" alt="Google Checkout Acceptance Mark" />
											</td>
											<td width="107" align="center">
												<img src="/images/leftnav/cc.jpg" width="107" height="71" border="0" alt="Wireless Emporium Accepts Visa, MasterCard, Discover, and American Express" />
											</td>
										</tr>
										<tr>
											<td align="center">
												<a href="https://www.ebillme.com/index.php/learnmore2/wirelessemporium" target="_blank"><img src="/images/leftnav/paypal.jpg" width="83" height="33" border="0" alt="Paypal" /></a>
											</td>
											<td align="center">
												<a href="https://www.ebillme.com/index.php/learnmore2/wirelessemporium" target="_blank"><img src="/images/leftnav/ebillme.jpg" width="97" height="33" border="0" alt="eBillme - My Bank, My Way" /></a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" align="center">
									<a href="/shipping-intl.asp"><img src="/images/leftnav/bongo.png" width="169" height="46" border="0" alt="bongo - No Hassle International Shipping" /></a>
								</td>
							</tr>
						</table>
                        </div>
                    </td>
                    <td style="padding-left:5px;" valign="top">