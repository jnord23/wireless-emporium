<%
	if isnull(noAsp) then noAsp = false
	if noAsp then
		if instr(request.ServerVariables("HTTP_X_REWRITE_URL"),".asp") > 0 and instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"_mp.asp") < 1 then
			response.Status = "410 Gone"
			response.End()
		end if
	end if
	
	saveEmail()
	
	dim imgSrv : imgSrv = "//www.wirelessemporium.com"
	dim jsSrv : jsSrv = "//www.wirelessemporium.com"
	
	pageStart = time
	
	if SEtitle = "" then SEtitle = "Buy Cell Phone Accessories from WirelessEmporium ?Cell phone Covers, Faceplates, Charms, Chargers, Batteries - Deep Discounts and Free Shipping"
	if SEdescription = "" then SEdescription = "The #1 Name in Cell Phone Accessories Online - Wireless Emporium. Discount Cell Phone Accessories at Prices up to 75% Off. Buy Wholesale Cellular Accessories, discount wireless accessories and cheap cell phone accessories."
	if SEkeywords = "" then SEkeywords = "cheap cell phone accessories,cell phone accessories,discount cellular accessories,mobile phone accessories,wireless accessories,cellular telephone accessories,wholesale cellular accessories,cellular accessories,wireless phone accessories,wholesale cell phone accessories,cellular phone accessories,wholesale wireless accessories,wholesale cellular phone accessories,discount cell phone accessories,cool cell phone accessories"
	
	siteId = 0
	cmsData(request.ServerVariables("HTTP_X_REWRITE_URL"))
	metaArray = split(session("metaTags"),"##")
	SEtitle = metaArray(0)
	SEdescription = metaArray(1)
	SEkeywords = metaArray(2)
	SeTopText = metaArray(3)
	SeBottomText = metaArray(4)
	
	noLeftNav = 1
	
	canonicalURL = ""
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><%=SEtitle%></title>
    <meta name="Description" content="<%=SEdescription%>">
    <meta name="Keywords" content="<%=SEkeywords%>">
    <% if prepStr(request.QueryString("curPage")) <> "" then %>
    <meta name="alternate" content="http://m.wirelessemporium.com<%=prepStr(request.QueryString("curPage")) & ".htm"%>">
	<% else %>
    <meta name="alternate" content="http://m.wirelessemporium.com<%=request.ServerVariables("HTTP_X_REWRITE_URL") & ".htm"%>">
    <% end if %>
    <!--#include virtual="/includes/asp/inc_HeadTagEnd.asp"-->
</head>
<% if pageName <> "basket" then %>
<div id="popUpBG" style="height:100%; display:none; width:100%; background-color:#999; position:fixed; left:0px; top:0px; z-index:100; opacity:0.7; filter:alpha(opacity=70);">&nbsp;</div>
<div id="popUpBox" style="height:1500px; display:none; width:100%; position:fixed; left:0px; right:0px; z-index:101; text-align:center; padding-top:300px;">
	<a onclick="document.getElementById('popUpBG').style.display = 'none'; document.getElementById('popUpBox').style.display = 'none'" style="cursor:pointer;">
    	<img id="welcomeBack" src="/images/buttons/welcomeBack.jpg" border="0" />
    </a>
</div>
<% end if %>
<!--#include virtual="/includes/asp/inc_BodyTagStart.asp"-->
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/checkout/mvt_topNav2.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/checkout/mvt_itemImage2.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/checkout/mvt_stepPos3.css" />
<script type="text/javascript" src="/cart/includes/jsCombo.js"></script>
<script src="<%=jsSrv%>/includes/js/ga_social_tracking.js"></script>
<link rel="stylesheet" type="text/css" href="/includes/css/base.css<%=arrCssDateModified(0)%>" />
<link rel="stylesheet" type="text/css" href="/includes/css/index.css<%=arrCssDateModified(1)%>" />
<!-- Load Twitter JS-API asynchronously -->
<script>
	(function(){
	var twitterWidgets = document.createElement('script');
	twitterWidgets.type = 'text/javascript';
	twitterWidgets.async = true;
	twitterWidgets.src = '//platform.twitter.com/widgets.js';
	
	// Setup a callback to track once the script loads.
	twitterWidgets.onload = _ga.trackTwitter;
	
	document.getElementsByTagName('head')[0].appendChild(twitterWidgets);
	})();
</script>
<!-- Load Google JS-API asynchronously -->
<script type="text/javascript">
  (function() {
	var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	po.src = '//apis.google.com/js/plusone.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<!--#include virtual="/includes/template/topHTML.asp"-->