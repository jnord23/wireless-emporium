<% if request.ServerVariables("HTTPS") = "off" then useHttp = "http" else useHttp = "https" %>
<!-- Async GoogleAnalytics Start -->
<script async type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1311669-1']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_trackPageLoadTime']); 

  (function(){
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
  var _gaq = _gaq || [];
   _gaq.push(['_setAccount', 'UA-38929999-1']);
   _gaq.push(['_setDomainName', 'wirelessemporium.com']);
   _gaq.push(['_trackPageview']);
 
  (function() {
     var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
     ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
   })();
</script>
<!-- Google Analytics Social Button Tracking -->
<!-- <script type="text/javascript" src="<%=useHttp%>://app.tabpress.com/js/ga_social_tracking.js"></script> -->
<!-- GoogleAnalytics End -->