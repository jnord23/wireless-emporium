<% if prepInt(cart) = 0 then %>
<div id="buySafe" style="position:fixed; width:100%; height:100%;">
	<div id="buySafeLogo" style="position:absolute; z-index:999; bottom:0px; right:0px;" onmouseover="buySafeRollover(); document.getElementById('buySafePopUp').style.display=''" onmouseout="document.getElementById('buySafePopUp').style.display='none'"><a href="https://www.buysafe.com/Web/Seal/VerifySeal.aspx?MPHASH=yEvFyUYD61Qg5T7dnPqnsBxICp3zIBrZwF%2BXbrTim3ltTNRphySTAsBmnbPtsBPxDb0nS2WHRPdkSCcwgM0D4w%3D%3D" target="_blank" title="BuySafe Verification"><img src="/images/icons/buySafe_off.png" border="0" /></a></div>
    <div id="buySafePopUp" style="position:absolute; z-index:999; bottom:65px; right:77px; display:none;" onmouseover="document.getElementById('buySafePopUp').style.display=''" onmouseout="document.getElementById('buySafePopUp').style.display='none'"></div>
</div>
<script language="javascript">function buySafeRollover() { if (document.getElementById("buySafePopUp").innerHTML == "") { document.getElementById("buySafePopUp").innerHTML = "<a href='http://www.buysafe.com/buyer_benefits/bond_short.html' target='_blank' title='BuySafe Benefits'><img src='/images/icons/buySafe_on.png' border='0' /></a>" } }</script>
<% else %>
<script src="https://seal.buysafe.com/private/rollover/rollover.js" type="text/javascript" language="javascript" charset="utf-8"></script>
<% end if %>
<div style="position:relative;">
<table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-top:5px;" id="siteTop">
	<tr><td id="adminBar"></td></tr>
    <script>utmx_section("Top Banner")</script>
    <tr>
    	<td width="100%" align="center">
			<div id="siteTop-Bar" style="height:22px; width:100%;" align="center">
            	<div style="width:980px;">
                	<div id="siteTop-Div"></div>
                    <div class="siteTopText" style="width:250px;" align="center"><a rel="nofollow" class="sign-link" style="cursor:pointer;" onclick="document.getElementById('enterMobileNumber').style.display=''; ajax('/ajax/mShopper.asp','mShopperDataCell')" title="Exclusive Smartphone Offers">Exclusive Offers Via SmartPhone >></a></div>
                    <div id="siteTop-Div"></div>
                    <div id="siteTop-CartBar" style="padding-left:10px; float:right;" align="center">
                        <a href="/cart/basket.asp" title="Shopping Cart"><div class="siteTopText" style="width:100%;" align="right">0 Item(s) $0.00&nbsp;&nbsp;</div></a>
                    </div>                    
                    <a href="/cart/basket.asp" title="Shopping Cart"><div id="siteTop-Cart" style="float:right;"></div></a>
                    <div id="siteTop-Div" style="float:right;"></div>                                                
                    <a href="/faq.asp" title="Help Center" style="text-decoration:none;"><div class="siteTopText" style="width:100px; float:right;" align="center">Help Center</div></a>
                    <div id="siteTop-Div" style="float:right;"></div>
				</div>
            </div>
            <div style="display:none; position:absolute; top:30px; left:0px; background-color:#FFF; border:2px groove #333; height:420px; width:420px; overflow:auto; z-index:50;" id="enterMobileNumber">
                <table border="0" cellpadding="5" cellspacing="0" style="background-color:#FFF;" width="250">
                    <tr>
                        <td width="100%" style="background-color:#000; color:#FFF; font-weight:bold; font-size:12px;">Get Exclusive Mobile Offers</td>
                        <td style="background-color:#000;" align="right"><a onclick="document.getElementById('enterMobileNumber').style.display='none'" style="cursor:pointer; color:#FFF; font-size:12px; text-decoration:underline;">Close</a></td>
                    </tr>
                    <tr><td colspan="2"><div id="mShopperWeLogo"></div></td></tr>
                    <tr>
                        <td colspan="2" id="mShopperDataCell"></td>
                    </tr>
                </table>
            </div>
        </td>
	</tr>    
    <tr>
    	<td width="100%" align="center">
            <div style="width:980px; height:60px; padding:7px 0px 7px 0px;">
                <div style="width:395px; float:left;" align="left">
                	<% if date < cdate("01/17/2012") then %>
                    <a href="/"><img src="/images/we_logo_2012.jpg" title="Wireless Emporium Inc." alt="Wireless Emporium Inc." border="0" /></a>
                    <% else %>
                    <a href="/"><div id="logo" title="Wireless Emporium Inc."></div></a>
                    <% end if %>
                </div>
            </div>
        </td>
    </tr>
    <% if varientTopNav = 1 then %>
    <tr>
    	<td width="100%" align="center">
            <!--#include virtual="/includes/template/topNav.asp"-->
        </td>
    </tr>
    <%
	end if
	
	if varientTopBanner = 2 then
	%>
    <tr>
    	<td width="100%" align="center">
            <img src="/images/varients/Free-Same-Day-Shipping-Banner.jpg" border="0" />
        </td>
    </tr>
    <%
	end if
	
	if REWRITE_URL <> "/seasonalpicks.asp" and REWRITE_URL <> "/cybermonday.asp" then
		if date > cdate("11/24/2011") and date < cdate("11/28/2011") then
	%>
    <tr>
    	<td width="100%" align="center">
		    <table cellpadding="0" cellspacing="0" border="0" width="980"><tr><td align="center" style="padding-bottom:3px; border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc;"><a href="/seasonalpicks.asp"><img src="/images/holiday/weHolidayTextBar.jpg" border="0" /></a></td></tr></table>
		</td>
	</tr>
    <%
		elseif date > cdate("11/27/2011") and date < cdate("12/01/2011") then
	%>
    <tr>
    	<td width="100%" align="center">
		    <table cellpadding="0" cellspacing="0" border="0" width="980"><tr><td align="center" style="padding-bottom:3px; border-left:1px solid #ccc; border-right:1px solid #ccc;"><a href="/cybermonday.asp"><img src="/images/holiday/Door-Buster-Banner2.jpg" border="0" /></a></td></tr></table>
		</td>
	</tr>    
    <%
		elseif topPageName <> "index" and date > cdate("11/30/2011") and date < cdate("12/13/2011") then
	%>
    <tr>
    	<td width="100%" align="center">
		    <table cellpadding="0" cellspacing="0" border="0" width="980">
            	<tr>
                	<td align="center" style="border-left:1px solid #ccc; border-right:1px solid #ccc;">
                    	<div style="position:relative; width:950px; border-bottom:2px solid #ccc;">
                            <img src="/images/holiday/holidayTopBar2.gif" border="0" style="cursor:pointer;" onclick="toggle('holidayText');" />
							<div id="holidayText" style="font-size:13px; padding:10px; position:absolute; background-color:#fff; left:130px; top:3px; border:2px solid #666; display:none; width:700px; height:75px; text-align:left;">
                            	<div>
                                	WirelessEmporium.com's 12 Days of Holiday Madness Giveaway! From December 1st- December 12th we are going to give away one order per day. Place an order and you are automatically entered - it's that easy. 
                                	We will be announcing the winners on <a href="http://www.Facebook.com/WirelessEmporium" target="_blank">www.Facebook.com/WirelessEmporium</a> every day. So order today and make sure to check out our Facebook page to see if you are the lucky winner.
	                                Please see our Store Policy page for additional information.
                                </div>
                                <div width="100%" align="right"><a href="#" onclick="toggle('holidayText');">Close Window</a></div>
                            </div>                            
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>    
    <%
		elseif topPageName <> "index" and date > cdate("12/12/2011") and date < cdate("12/19/2011") then
	%>
    <tr>
    	<td width="100%" align="center">
		    <table cellpadding="0" cellspacing="0" border="0" width="980">
            	<tr>
                	<td align="center" style="border-left:1px solid #ccc; border-right:1px solid #ccc;">
                    	<div style="position:relative; width:950px; border-bottom:2px solid #ccc;">
                            <img src="/images/mainbanners/holidayTopBar3.gif" border="0" />                        
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>    
    <%
		end if
	end if
	%>
    </noscript>
    <tr>
    	<td width="100%" align="center">