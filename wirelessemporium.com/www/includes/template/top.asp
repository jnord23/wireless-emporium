<%
	if isnull(noAsp) or noAsp = "" then noAsp = true
	curPage = prepStr(request.QueryString("curPage"))
	
	if noAsp then
		if instr(request.ServerVariables("HTTP_X_REWRITE_URL"),".asp") > 0 and instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"_mp.asp") < 1 then
			response.Status = "410 Gone"
			response.End()
		end if
	end if
	
	saveEmail()
	
	topPageName = "top"
	
	if bSecureHttp then 
		response.redirect("http://" & request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL")) 
	end if
	dim pageStart : pageStart = time
	dim extraWidgets : extraWidgets = "yes"
	
	function drawStitchModelBoxByArray(pArr, pStitchImg)
		timestamp = getFileLastModified(pStitchImg)
		if timestamp <> "" then pStitchImg = pStitchImg & "?v=" & timestamp
	%>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
            <%
            useX = 0 : a = 0
            if not isnull(pArr) then
                for nRows=0 to ubound(pArr,2)
                    a = a + 1
					modelid = cint(pArr(1,nRows))
					modelName = replace(pArr(2,nRows), "/", " / ")
					brandid = cint(pArr(8,nRows))
					brandName = replace(pArr(7,nRows), "/", " / ")
					altText = modelName & " " & nameSEO(categoryName)
					catLinkFix = "-sc-"
					if categoryid > 999 then catLinkFix = "-scd-" end if
					strLink = "/sb-" & brandID & "-sm-" & modelID & catLinkFix & categoryID & "-" & formatSEO(categoryName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)
					if categoryid = 1270 then
						strLink = "/sb-" & brandID & "-sm-" & modelID & "-sc-" & categoryID & "-music-skins-genre"
					end if
	            %>
                <td width="20%" valign="top" style="border-bottom:1px solid #CCC;<% if a < 5 then %> border-right:1px solid #CCC;<% end if %> padding:10px 5px 5px 5px;">
                    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                        <tr>
                            <td align="center" valign="top">
                                <a href="<%=strLink%>" title="<%=altText%>"><div style="width: 70px; height: 112px; background: url(<%=pStitchImg%>) <%=useX%>px 0px no-repeat; cursor:pointer;" title="<%=altText%>"></div></a><br />
                                <a class="cellphone2-link" href="<%=strLink%>" title="<%=altText%>"><%=altText%></a>
							</td>
                        </tr>
                    </table>
                </td>
    	        <%
                    if a = 5 then
                        response.write "</tr><tr>" & vbcrlf
                        a = 0
                    end if
                    useX = useX - 70
                next					
            end if
            %>
            </tr>
        </table>    
    <%
	end function
	
	curPage = prepStr(request.QueryString("curPage"))
	if curPage <> "" then useURL = curPage else useURL = request.ServerVariables("HTTP_X_REWRITE_URL")
	siteId = 0
'	cmsData(useURL)
'	metaArray = split(session("metaTags"),"##")
'	SEtitle = metaArray(0)
'	SEdescription = metaArray(1)
'	SEkeywords = metaArray(2)
'	SeTopText = metaArray(3)
'	SeBottomText = metaArray(4)
	
	metaString = ""
	call getCmsData(0,cms_basepage,useURL,metaString)
	if instr(metaString, "!##!") > 0 then
		metaArray = split(metaString,"!##!")
		SEtitle = metaArray(0)
		SEdescription = metaArray(1)
		SEkeywords = metaArray(2)
		SeTopText = metaArray(3)
		SeBottomText = metaArray(4)
		SeH1 = metaArray(5)	
	end if	
	
	if SEtitle = "" then SEtitle = "Buy Cell Phone Accessories from WirelessEmporium ?Cell phone Covers, Faceplates, Charms, Chargers, Batteries - Deep Discounts and Free Shipping"
	if SEdescription = "" then SEdescription = "The #1 Name in Cell Phone Accessories Online - Wireless Emporium. Discount Cell Phone Accessories at Prices up to 75% Off. Buy Wholesale Cellular Accessories, discount wireless accessories and cheap cell phone accessories."
	if SEkeywords = "" then SEkeywords = "cheap cell phone accessories,cell phone accessories,discount cellular accessories,mobile phone accessories,wireless accessories,cellular telephone accessories,wholesale cellular accessories,cellular accessories,wireless phone accessories,wholesale cell phone accessories,cellular phone accessories,wholesale wireless accessories,wholesale cellular phone accessories,discount cell phone accessories,cool cell phone accessories"	
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><%=SEtitle%></title>
    <meta name="Description" content="<%=SEdescription%>">
    <meta name="Keywords" content="<%=SEkeywords%>">
    <% if prepStr(request.QueryString("curPage")) <> "" then %>
    <meta name="alternate" content="http://m.wirelessemporium.com<%=prepStr(request.QueryString("curPage")) & ".htm"%>">
	<% else %>
    <meta name="alternate" content="http://m.wirelessemporium.com<%=request.ServerVariables("HTTP_X_REWRITE_URL") & ".htm"%>">
    <% end if %>
    <% if prepInt(noIndex) = 1 then %><meta name="robots" content="noindex, follow"><% end if %>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
    <link rel="stylesheet" type="text/css" href="/includes/css/base.css<%=arrCssDateModified(0)%>" />
    <!--#include virtual="/includes/asp/inc_HeadTagEnd.asp"-->
</head>
<!--#include virtual="/includes/asp/inc_BodyTagStart.asp"-->
	<!--#include virtual="/includes/template/topHTML.asp"-->