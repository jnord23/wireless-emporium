<%
dim preStrands : preStrands = 0
%>
<div itemscope itemtype="http://data-vocabulary.org/Product"> <!-- rich snippet starts -->
	<span itemprop="condition" content="new"></span>
	<span itemprop="color" content="<%=productColor%>"></span>
	<meta itemprop="currency" content="USD" />
	<span itemprop="price" content="<%=price_our%>"></span>
	<span itemprop="brand" content="<%=brandName%>"></span>
	<span itemprop="model" content="<%=modelName%>"></span>

    <div class="tb cWidth">
        <!-- Start Non Promo Banners -->
		<% if typeid = 16 then %>
        <div id="normalPdpBanner" class="tb mCenter" style="display:none;"><img src="/images/banners/we-free-sameday-shipping2.jpg" border="0" width="748" height="68" /></div>
        <%else%>
        <div id="normalPdpBanner" class="tb mCenter" style="display:none;"><img src="/images/banners/freeShippingAllProducts.jpg" border="0" width="748" height="68" /></div>
        <% end if %>
        <!-- End Non Promo Banners -->
        <!-- Start Promo Banner -->
        <div id="promoPdpBanner" class="tb mCenter" onclick="showSpecialOffer()" style="cursor:pointer;"><img src="/images/deals/headset/offerBanner.jpg" border="0" width="748" height="68" /></div>
        <!-- End Promo Banner -->
        <div class="tb cWidth allBreadCrumbs breadCrumbs">
            <div class="fl bcBox"><a href="javascript:history(-1);" class="breadCrumbs">&lt;&lt;&nbsp;Back</a></div>
            <div class="fl bcBox">|</div>
            <div class="fl bcBoxLarge">
                <%if typeID = 16 then%>
                <a class="breadCrumbs" href="/unlocked-phones">Unlocked Cell Phones</a>&nbsp;&#183;&nbsp;<a class="breadCrumbs" href="/cp-sb-<%=brandID & "-" & formatSEO(brandName)%>-unlocked-cell-phones"><%=brandName%> Unlocked Cell Phones</a>
                <%else%>
                    <%if musicSkins = 1 then%>
                        <a class="breadCrumbs" href="/">HOME</a>&nbsp;&#183;&nbsp;<a class="breadCrumbs" href="/<%=formatSEO(brandName)%>-phone-accessories"><%=brandName%> Accessories</a>&nbsp;&#183;&nbsp;<a class="breadCrumbs" href="<%=modelLink%>"><%=brandName & " " & modelName%> Accessories</a>&nbsp;&#183;&nbsp;<a class="breadCrumbs" href="/sb-<%=brandID & "-sm-" & modelID & "-sc-" & typeID & "-" & formatSEO(categoryName) & "-genre"%>"><%=brandName & " " & modelName & " " & nameSEO(categoryName)%></a>
                    <%elseif isTablet then%>	
                        <a class="breadCrumbs" href="/tablet-ereader-accessories">Tablet Accessories</a>&nbsp;&#183;&nbsp;<a class="breadCrumbs" href="<%=modelLink%>"><%=brandName & " " & modelName%> Accessories</a>&nbsp;&#183;&nbsp;<a class="breadCrumbs" href="/sb-<%=brandID & "-sm-" & modelID & "-sc-" & typeID & "-" & formatSEO(categoryName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>"><%=brandName & " " & modelName & " " & nameSEO(categoryName)%></a>
                    <%else%>
                        <a class="breadCrumbs" href="/">HOME</a>&nbsp;&#183;&nbsp;<a class="breadCrumbs" href="/<%=formatSEO(brandName)%>-phone-accessories"><%=brandName%> Accessories</a>&nbsp;&#183;&nbsp;<a class="breadCrumbs" href="<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>-accessories"><%=brandName & " " & modelName%> Accessories</a>&nbsp;&#183;&nbsp;<a class="breadCrumbs" href="/sb-<%=brandID & "-sm-" & modelID & "-scd-" & subTypeID & "-" & formatSEO(strSubtypename) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>"><%=brandName & " " & modelName & " " & strSubtypename%></a>
                    <% end if %>
                <%end if%>
            </div>
        </div>
        <div class="tb topContentArea">
            <div class="fl prodImgDetails">
                <div class="tb mCenter starBox">
                    <div class="fl stars">
                        <%
                        for i = 1 to 5
                            if round(dblAvgRating) >= i then
                        %>
                        <div class="fl fullStar"></div>
                        <%
                            else
                        %>
                        <div class="fl emptyStar"></div>
                        <%
                            end if
                        next
                        %>
                    </div>
                    <div class="fl reviewCnt">(<%=reviewCnt%> Reviews)</div>
                    <div class="fl reviewLinks">
                        <div class="fl reviewLink1"><a class="reviewLink" href="#prodTabs" onclick="changeTab('reviewTab','reviewDetails');">Read Reviews</a></div>
                        <div class="fl reviewLink2">|</div>
                        <div class="fl reviewLink3"><a class="reviewLink" href="/user-reviews-b?itemID=<%=itemid%>&partNumber=<%=partnumber%>">Write Review</a></div>
                    </div>
                </div>
                <div id="fullSizedImg" class="tb prodImg mCenter"><%=replace(replace(objItemImgFull,"/product/360/360-clicktoview-button.png","/blank.gif"),"360 image","")%></div>
                <%
                if numAvailColors > 1 then
                %>
                <div class="ffl" align="center" style="padding-top:10px;">
                    <div style="display:inline-block; font-style:italic;" align="center">CURRENT COLOR:&nbsp;</div>
                    <%=generateColorPickerText(arrColors, colorSlaves, colorid, "font-weight:bold; text-align:left;")%>
                </div>
                <div class="ffl" align="center" style="padding-bottom:10px;"><%=generateColorPicker(arrColors, itemid, colorSlaves, colorid, numAvailColors, blnIsNotOriginalPrice)%></div>
                <div style="display:none;" id="preloading_deck"></div>
                <%
                end if
                %>
                <div class="tb imgOptions mCenter">
                    <a href="#" onclick="showFloatingZoomImage();"><div class="fl zoomLink"></div></a>
                    <%=strVideo%>
                    <% if hasFlashFile then %><a href="#" onclick="playFlash();"><div class="fl link360"></div></a><% end if %>
                </div>
                <div id="imgAlt-location" class="tb mCenter altImgBox">
                    <div class="fl altImg"><a style="cursor:pointer;" onclick="fnPreviewImage('<%=itemImgFullPath%>')"><img src="<%=replace(itemImgFullPath,"/big/","/icon/")%>" border="0" width="40" height="40" /></a></div>
                    <%
                    for iCount = 0 to 7
                        path = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".jpg")
                        src = replace(path,".jpg","_thumb.jpg")
                        if fs.FileExists(Server.MapPath(path)) and fs.FileExists(Server.MapPath(src)) then						
                    %>
                    <div class="fl altImg"><a style="cursor:pointer;" onclick="fnPreviewImage('<%=path%>')"><img src="<%=src%>" border="0" width="40" height="40" /></a></div>
                    <%
                        end if
                    next
                    %>
                </div>
                <% if customize then %>
                <div class="tb mCenter customizeText">Customizing this product?<br />Please allow up to <%=gblCustomShippingDate%> business days for<br />production of custom cases prior to shipping time.</div>
                <% end if %>
            </div>
            <div class="fl prodTxtDetails">
                <div class="tb itemTitle">
                    <h1 id="id_h1" class="itemTitleH1"><%=itemDesc%></h1><br />
                    <div id="itemIdDisplay" class="fl">Item ID: <%=itemID%></div>
                </div>
                <div class="fl priceBox">
                    <div id="mvt_pricing1" style="padding:10px 0px 10px 0px;">
                        <div class="tb wePrice"><%=formatCurrency(price_our,2)%></div>
                        <div class="tb retailPrice">Retail Price: <%=formatCurrency(price_retail,2)%></div>
                        <div class="tb saveValue">You Save <%=formatpercent((price_retail-price_our)/price_retail,0)%> (<%=formatCurrency(price_retail-price_our,2)%>)</div>
                    </div>
                </div>
                <div class="fl shipBox">
                    <div class="tb shipIcon"></div>
                    <% if prepInt(masterInvQty) > 0 or not bFinalOutofStock then %>
                    <div id="mvt_instock2" style="color:#222;">
                        <div class="fl" style="margin:0px 5px 0px 5px;"><img src="/images/product/stock.png" border="0" /></div>
                        <div class="fl" style="width:175px; ">
                            <div class="tb" style="font-size:14px; color:#339900; font-weight:bold;">This item is In Stock!</div>
                            <div class="tb"><%=gblShipoutDate%></div>
                        </div>
                    </div>
                    <%end if%>
                </div>
                <div class="tb valueProps">
                    <div class="tb" id="mvt_badge2">
                        <% if typeID <> 16 then %>
                        <img src="/images/product/item-replacement.png" border="0" />
                        <% else %>
                        <img src="/images/product/phone-badge.png" border="0" />
                        <% end if %>
                    </div>
                </div>
                <% if prepInt(masterInvQty) > 0 or not bFinalOutofStock then %>
                <div class="tb addToCartBox">
                    <form name="frmSubmit" action="/cart/item_add" method="post" style="margin:0px; padding:0px;">
                    <div class="fl qtyText">Qty:</div>
                    <div class="fl qtyBox"><input type="text" name="qty" value="1" size="3" maxlength="3" /></div>
                    <!-- Start No Promo Add to Cart -->
                    <div id="fcAddBttn" class="fl addToCartBttn" style="display:none;">
                        <a href="javascript:br_cartTracking('add', '<%=itemid%>', '<%=productColor%>', '<%=replace(itemDesc, "'", "\'")%>', '<%=price_our%>'); popUpCart_add(<%=itemID%>,document.frmSubmit.qty.value,null);">
                        	<img src="/images/buttons/addToCart_bigRed.gif" title="Add To Cart" border="0" />
						</a>
                    </div>
                    <!-- End No Promo Add to Cart -->
                    <!-- Start With Promo Add to Cart -->
                    <div id="fcAddBttnPromo" class="fl addToCartBttn">
                        <a href="javascript:br_cartTracking('add', '<%=itemid%>', '<%=productColor%>', '<%=replace(itemDesc, "'", "\'")%>', '<%=price_our%>'); popUpCartWithPromo_add(<%=itemID%>,document.frmSubmit.qty.value,null);">
                        	<img src="/images/buttons/addToCart_bigRed.gif" title="Add To Cart" border="0" />
						</a>
                    </div>
                    <div id="btnAddToCart" class="fl addToCartBttn">
						<input type="image" src="/images/buttons/addToCart_bigRed.gif" alt="Add To Cart" onclick="br_cartTracking('add', '<%=itemid%>', '<%=productColor%>', '<%=replace(itemDesc, "'", "\'")%>', '<%=price_our%>')" />
                    </div>
                    <!-- Start With Promo Add to Cart -->
					<input type="hidden" name="prodid" value="<%=itemID%>" />
                    </form>
                    <div id="promoFreeGiftMsg" class="fl" style="margin-top:15px;"><img src="/images/deals/headset/pdpTag.jpg" border="0" width="370" height="54" /></div>
                </div>
                <span itemprop="availability" content="in_stock"></span>
                <% else %>
                <div class="tb mCenter outOfStock">This Item is Currently<br />Out of Stock<br />Please Check Back Soon</div>
                <span itemprop="availability" content="out_of_stock"></span>
                <% end if %>
                <% if prepStr(bullet1) <> "" then %>
                <div class="tb featureTitle"></div>
                <div id="mvt_features2">
                    <div class="tb featureList">
                        <ul style="margin-left:-15px; line-height:20px; font-size:15px; color:#333;">
                        <%
                        bulletList = bullet1 & "##" & bullet2 & "##" & bullet3 & "##" & bullet4 & "##" & bullet5 & "##" & bullet6 & "##" & bullet7 & "##" & bullet8 & "##" & bullet9 & "##" & bullet10
                        bulletListArray = split(bulletList,"##")
                        for i = 0 to ubound(bulletListArray)
                            if prepStr(bulletListArray(i)) <> "" then
                            %>
                            <li><%=bulletListArray(i)%></li>
                            <%
                            end if
                        next
                        %>
                        </ul>
                    </div>
                </div>
                <% end if %>
                <div class="tb socialBox">
                    <!--#include virtual="/includes/asp/product/inc_rewards2.asp"-->
                </div>
            </div>
        </div>
        <div class="tb mCenter" style="height:20px;"></div>
    </div>
</div> <!-- rich snippet end -->


<% if not recommendRS.eof and preStrands = 1 then %>
<div class="tb titleBar">
	<div class="fl titleIconRecommend"></div>
    <div class="fl titleMainBar">Recommended Popular Items</div>
    <div class="fl titleRight"></div>
</div>
<div class="fl prevItem"><input type="image" src="/images/buttons/bigPrevBlue.gif" width="21" height="103" onmouseup="recScroll(-1)" /></div>
<div class="fl recommenedProducts">
	<div id="recScrollBox" class="recommendedProductList">
    	<table border="0" cellpadding="0" cellspacing="0">
        	<tr>
				<%
				recProdCnt = 0
                do while not recommendRS.EOF
					recProdCnt = recProdCnt + 1
                %>
                <td class="recCell" onclick="window.location='/p-<%=recommendRS("itemID")%>-<%=formatSEO(recommendRS("itemDesc"))%>'">
                	<div class="recPic mCenter"><a href="/p-<%=recommendRS("itemID")%>-<%=formatSEO(recommendRS("itemDesc"))%>"><img src="/productpics/thumb/<%=recommendRS("itemPic")%>" border="0" width="100" height="100" /></a></div>
                    <div class="recDesc"><%=recommendRS("itemDesc")%></div>
                    <div class="recRetail">Reg. <%=formatCurrency(recommendRS("price_retail"),2)%></div>
                    <div class="recPrice"><%=formatCurrency(recommendRS("price_our"),2)%></div>
                </td>
                <%
                    recommendRS.movenext
                loop
                %>
            </tr>
        </table>
    </div>
</div>
<div class="fl nextItem"><input type="image" src="/images/buttons/bigNextBlue.gif" width="21" height="103" onmouseup="recScroll(1)" /></div>
<% else %>
<div class="tb titleBar">
	<div class="fl titleIconRecent"></div>
    <div class="fl titleMainBar">
        <div class="fl" style="font-size:inherit;">People Who Purchased This Also Purchased</div>
        <div class="fr backToTopText" onclick="window.scrollTo(0,0)">Back To Top</div>
        <div class="fr backToTopIcon" onclick="window.scrollTo(0,0)"></div>
    </div>
    <div class="fl titleRight"></div>
</div>
<div class="strandsRecs" tpl="PROD-4" item="<%=itemID%>"></div>
<div class="tb titleBar">
	<div class="fl titleIconRecommend"></div>
    <div class="fl titleMainBar">People Who Viewed This Also Viewed</div>
    <div class="fl titleRight"></div>
</div>
<div class="strandsRecs" tpl="PROD-1" item="<%=itemID%>"></div>

<% end if %>
<!-- product overview tabs -->
<div id="prodTabs" class="tb titleBar">
	<div class="fl titleIconTabs"></div>
    <div class="fl titleMainBar">
        <div class="activeDetailTab" id="overviewTab" onclick="changeTab('overviewTab','prodDetails');" style="left:0px;">Product Overview</div>
        <div class="inactiveDetailTab" id="compatTab" onclick="changeTab('compatTab','compBody');">Compatibility</div>
        <div class="inactiveDetailTab" id="reviewTab" onclick="changeTab('reviewTab','reviewDetails');">Reviews</div>
        <div class="inactiveDetailTab" id="qaTab" onclick="changeTab('qaTab','qaDetails');">Q&amp;A</div>
        <div class="fr backToTopText" onclick="window.scrollTo(0,0)">Back To Top</div>
        <div class="fr backToTopIcon" onclick="window.scrollTo(0,0)"></div>
    </div>
    <div class="fl titleRight"></div>
</div>
<div id="prodDetails" class="fl tabContent">
	<%=updateHTML(strItemLongDetail)%>
    <%
    if typeID = 16 then
		response.write "<div style=""font-size:14px; font-weight:bold;"">PHONE FEATURES:</div>" & vbcrlf
		if BULLET1 <> "" or BULLET2 <> "" or BULLET3 <> "" or BULLET4 <> "" or BULLET5 <> "" or BULLET6 <> "" or BULLET7 <> "" or BULLET8 <> "" or BULLET9 <> "" or BULLET10 <> "" then
			response.write "<ul style=""font-size:14px; margin-top:0px;"">" & vbcrlf
			if BULLET1 <> "" then response.write "<li>" & insertDetails(BULLET1) & "</li>" & vbcrlf
			if BULLET2 <> "" then response.write "<li>" & insertDetails(BULLET2) & "</li>" & vbcrlf
			if BULLET3 <> "" then response.write "<li>" & insertDetails(BULLET3) & "</li>" & vbcrlf
			if BULLET4 <> "" then response.write "<li>" & insertDetails(BULLET4) & "</li>" & vbcrlf
			if BULLET5 <> "" then response.write "<li>" & insertDetails(BULLET5) & "</li>" & vbcrlf
			if BULLET6 <> "" then response.write "<li>" & insertDetails(BULLET6) & "</li>" & vbcrlf
			if BULLET7 <> "" then response.write "<li>" & insertDetails(BULLET7) & "</li>" & vbcrlf
			if BULLET8 <> "" then response.write "<li>" & insertDetails(BULLET8) & "</li>" & vbcrlf
			if BULLET9 <> "" then response.write "<li>" & insertDetails(BULLET9) & "</li>" & vbcrlf
			if BULLET10 <> "" then response.write "<li>" & insertDetails(BULLET10) & "</li>" & vbcrlf
			response.write "</ul>" & vbcrlf
		end if
		if COMPATIBILITY <> "" then
			response.write "<p><div style=""font-size:14px; font-weight:bold;"">COMPATIBILITY:</div>" & COMPATIBILITY & "</p>" & vbcrlf
		end if
		%>
		<p><div style="font-size:14px; font-weight:bold;"> WHAT IS INCLUDED: </div><%=PackageContents%></p>
		<p><em>Wireless Emporium offers direct replacement and unlocked Cell/PDA/Smart Phones to you WITHOUT ANY CONTRACTS! Order your replacement or unlocked Cell/PDA/Smart Phones today and we'll ship it out to your home or business for FREE!</em></p>
		<p style="font-size:14px; font-weight:bold;"> Click Here: ** <a href="javascript:Open_Popup('/phone_return_policy','PhoneReturnPolicy','width=550,height=650,scrollbars=no');" style="font-size:14px;font-weight:bold;color:#CC3300;text-decoration:none;">Cell Phone Return Policy</a> **</p>
		<%
	elseif typeID = 1 then
		sql = "exec sp_batteryGraphData " & modelID & ", 0"
		session("errorSQL") = sql
		set graphRS = oConn.execute(sql)
		
		if not graphRS.EOF then
			dim talkTime : talkTime = prepInt(graphRS("usageTime"))
			dim oemBatCap : oemBatCap = prepInt(graphRS("oemCapacity"))
			dim mahPerHour : mahPerHour = oemBatCap / talkTime
	%>
    <style>
		.bcTitleText			{ font-size:16px; margin:15px 0px 60px 0px; }
		.bcFullGraph			{ display:table; }
		.bcHrs					{ float:left; }
		.bcMeasurement1			{ margin-top:35px; font-weight:bold; font-size:10px; width:22px; text-align:left; display:table;}
		.bcMeasurement2			{ margin-top:63px; font-weight:bold; font-size:10px; width:22px; text-align:left; display:table;}
		.bcGraphGraphics		{ float:left; }
		.bcHeaders				{ display:table; }
		.bcInactiveHeader		{ width:160px; background-color:#000; color:#FFF; padding:5px 0px; line-height:16px; float:left; text-align:center; position:relative; }
		.bcRecommendTop			{ position:absolute; width:162px; height:50px; background-image:url(/images/backgrounds/batteryCapacityRecommendTop.gif); top:-50px; }
		.bcRecommendHeader		{ position:absolute; bottom:0px; background-image:url(/images/backgrounds/batteryCapacityRecommendHeader.gif); width:162px; left:-1px; padding:5px 0px; line-height:16px; text-align:center; z-index:2; }
		.bcRightBorder			{ border-right:1px solid #FFF; }
		.bcBoldTitle			{ font-size:16px; }
		.bcGraph				{ display:table; border-bottom:1px solid #CCC; margin-bottom:20px; }
		.bcGraphColumnHolder1	{ height:296px; width:159px; background-image:url(/images/backgrounds/batteryCapacityGraph_inactive.gif); float:left; border-left:1px solid #CCC; position:relative; z-index:1; }
		.bcGraphColumnHolder2	{ height:296px; width:160px; background-image:url(/images/backgrounds/batteryCapacityGraph_inactive.gif); float:left; border-left:1px solid #CCC; position:relative; z-index:1; }
		.lastInLine				{ border-right:1px solid #CCC; }
		.bcGraphColumn			{ width:119px; background-color:#CCC; position:absolute; bottom:0px; left:20px; text-align:center; font-size:12px; z-index:3; }
		.columnHeader			{ font-weight:bold; font-size:16px; }
		p						{ line-height:16px; border:0px; margin:10px 0px 5px 0px; }
		.bcCurItem				{ box-shadow: 0px 0px 15px 0px #888888; z-index:2; }
		.bcCurItemFooter		{ background-color:#FFF; border-left:1px solid #CCC; border-bottom:1px solid #CCC; border-right:1px solid #CCC; position:absolute; bottom:-11px; left:-1px; width:159px; height:10px; border-bottom-left-radius:5px; border-bottom-right-radius:5px; box-shadow: 0px 0px 15px 0px #888888; z-index:1 }
		.bcShadowBlock			{ background-color:#FFF; position:absolute; bottom:0px; height:20px; width:159px; z-index:2; }
		.bcCurItemFooter_active	{ background-color:#14457b; border-left:1px solid #CCC; border-bottom:1px solid #CCC; border-right:1px solid #CCC; position:absolute; bottom:-11px; left:-1px; width:160px; height:10px; border-bottom-left-radius:5px; border-bottom-right-radius:5px; box-shadow: 0px 0px 15px 0px #888888; z-index:1 }
		.bcShadowBlock_active	{ background-color:#14457b; position:absolute; bottom:0px; height:20px; width:160px; z-index:2; }
		.activeGraphBG			{ background-image:url(/images/backgrounds/batteryCapacityGraph_active.gif); }
		.gStars					{ display:table; }
		.gStarBox				{ float:left; width:160px; }
		.gStarDisplay			{ margin:0px auto; display:table; font-size:14px; }
		span.full				{ width:18px; height:18px; padding-right:1px; background:url("/images/reviews/stars.png") 0 -36px no-repeat; display:inline-block; }
		span.half				{ width:18px; height:18px; padding-right:1px; background:url("/images/reviews/stars.png") 0 -18px no-repeat; display:inline-block; }
		span.empty				{ width:18px; height:18px; padding-right:1px; background:url("/images/reviews/stars.png") 0 0 no-repeat; display:inline-block; }
		.gButtons				{ display:table; margin:10px 0px; }
		.gButton				{ float:left; width:160px; text-align:center; }
	</style>
    <div class="bcTitleText">Usage time: see how this battery stacks up against the rest.</div>
    <div class="bcFullGraph">
    	<div class="bcHrs">
        	<div class="bcMeasurement1">32</div>
            <div class="bcMeasurement2">24</div>
            <div class="bcMeasurement2">16</div>
            <div class="bcMeasurement2">8</div>
            <div class="bcMeasurement2">0</div>
        </div>
        <div class="bcGraphGraphics">
            <div class="bcHeaders">
                <%
                dim graphLap : graphLap = 0
                dim gProdDetails(3)
				dim recommendedID : recommendedID = 0
				dim recommendedPrice : recommendedPrice = 100
                do while not graphRS.EOF
                    gProdDetails(graphLap) = graphRS("itemID") & "#&#" & graphRS("itemDesc") & "#&#" & graphRS("batteryCapacity") & "#&#" & graphRS("price_our") & "#&#" & graphRS("usageTime") & "#&#" & graphRS("oemCapacity") & "#&#" & graphRS("reviews") & "#&#" & graphRS("stars")
					
					gArray = split(gProdDetails(graphLap),"#&#")
                    gPrice = prepInt(gArray(3))
					
					if gPrice < recommendedPrice then
						recommendedID = graphLap
						recommendedPrice = gPrice
					end if
					
					graphLap = graphLap + 1
					graphRS.movenext
				loop
				
				graphLap = 0
				for iGraph = 0 to ubound(gProdDetails)
                    gArray = split(gProdDetails(graphLap),"#&#")
                    gPrice = gArray(3)
					
					if gPrice < recommendedPrice then
						recommendedID = graphLap
						recommendedPrice = recommendedPrice
					end if
                    
                    graphLap = graphLap + 1
                    if graphLap = 1 then
                %>
                <div class="bcInactiveHeader bcRightBorder">
                	<strong class="bcBoldTitle">OEM</strong><br /><%=formatCurrency(gPrice)%>
                    <% if recommendedID = iGraph then %>
                    <div class="bcRecommendTop"></div>
                    <div class="bcRecommendHeader"><strong class="bcBoldTitle">Plus</strong><br /><%=formatCurrency(gPrice)%></div>
                    <% end if %>
                </div>
                <%
                    elseif graphLap = 2 then
                %>
                <div class="bcInactiveHeader bcRightBorder">
                	<strong class="bcBoldTitle">Prime</strong><br /><%=formatCurrency(gPrice)%>
                    <% if recommendedID = iGraph then %>
                    <div class="bcRecommendTop"></div>
                    <div class="bcRecommendHeader"><strong class="bcBoldTitle">Plus</strong><br /><%=formatCurrency(gPrice)%></div>
                    <% end if %>
                </div>
                <%
                    elseif graphLap = 3 then
                %>
                <div class="bcInactiveHeader bcRightBorder">
                    <strong class="bcBoldTitle">Plus</strong><br /><%=formatCurrency(gPrice)%>
                    <% if recommendedID = iGraph then %>
                    <div class="bcRecommendTop"></div>
                    <div class="bcRecommendHeader"><strong class="bcBoldTitle">Plus</strong><br /><%=formatCurrency(gPrice)%></div>
                    <% end if %>
                </div>
                <%
                    elseif graphLap = 4 then
                %>
                <div class="bcInactiveHeader">
                	<strong class="bcBoldTitle">Value</strong><br /><%=formatCurrency(gPrice)%>
                    <% if recommendedID = iGraph then %>
                    <div class="bcRecommendTop"></div>
                    <div class="bcRecommendHeader"><strong class="bcBoldTitle">Plus</strong><br /><%=formatCurrency(gPrice)%></div>
                    <% end if %>
                </div>
                <%
                    end if
                next
                %>
            </div>
            <div class="bcGraph">
                <%
                graphLap = 0
                dim pxPerHour : pxPerHour = 9.1875
                for i = 0 to 3
                    gArray = split(gProdDetails(graphLap),"#&#")
                    gItemID = prepInt(gArray(0))
                    gItemDesc = prepVal(gArray(1))
                    gBatteryCapacity = prepInt(gArray(2))
                    gPrice = prepInt(gArray(3))
                    
                    graphLap = graphLap + 1
                    
                    if graphLap = 1 or graphLap = 4 then
                        useClass1 = "bcGraphColumnHolder1"
                    else
                        useClass1 = "bcGraphColumnHolder2"
                    end if
                    if itemID = gItemID then useClass1 = useClass1 & " bcCurItem"
                    if graphLap = 4 then useClass1 = useClass1 & " lastInLine"
					
					if recommendedID = i then useClass1 = useClass1 & " activeGraphBG"
					
                    newTalkTime = cint(gBatteryCapacity / mahPerHour)
                %>
                <div class="<%=useClass1%>">
                    <div class="bcGraphColumn" style="height:<%=pxPerHour*newTalkTime%>px;">
                        <p class="columnHeader"><%=newTalkTime%> Hours</p>
                        Talk Time
                    </div>
                    <% if itemID = gItemID and recommendedID = i then %>
                    <div class="bcShadowBlock_active"></div>
                    <div class="bcCurItemFooter_active"></div>
                    <% elseif itemID = gItemID then %>
                    <div class="bcShadowBlock"></div>
                    <div class="bcCurItemFooter"></div>
                    <% end if %>
                </div>
                <%
                next
                %>
            </div>
            <div class="gStars">
                <%
                for gRows = 0 to 3
                    gArray = split(gProdDetails(gRows),"#&#")
                    if prepInt(gArray(6)) > 0 then
                %>
                <div class="gStarBox">
                    <div class="gStarDisplay"><%=getRatingAvgStarBig(prepInt(gArray(7)))%> (<%=prepInt(gArray(6))%>)</div>
                </div>
                <%
                    end if
                next
                %>
            </div>
            <div class="gButtons">
                <%
                for gRows = 0 to 3
                    gArray = split(gProdDetails(gRows),"#&#")
                    if prepInt(gArray(0)) = itemID then
                %>
                <div class="gButton">
                    <form name="graphSubmit" action="/cart/item_add" method="post" style="margin:0px; padding:0px;">
                        <input type="hidden" name="qty" value="1" />
                        <input type="hidden" name="prodid" value="<%=itemID%>" />
                        <input type="image" src="/images/buttons/batteryGraph/buyNow.gif" border="0" width="141" height="43" alt="Add To Cart" onclick="br_cartTracking('add', '<%=itemid%>', '<%=productColor%>', '<%=replace(itemDesc, "'", "\'")%>', '<%=price_our%>')" />
                    </form>
                </div>
                <%
                    else
                %>
                <div class="gButton"><a href="/p-<%=gArray(0)%>-<%=formatSEO(gArray(1))%>"><img src="/images/buttons/batteryGraph/viewDetails.gif" border="0" width="141" height="43" /></a></div>
                <%
                    end if
                next
                %>
            </div>
        </div>
    </div>
    <%
		end if
	end if
	%>
    <%
	if customize and instr(strItemLongDetail,"/productTabs/custom_1.jpg") < 1 then
	%>
	<div align=center>
	<a href=/custom/?aspid=<%=itemID%>><img src="/images/productTabs/custom_1.jpg" border="0"></a>
	</div>
	<br />
	Customize your device with your own distinctive flair! An amazing gift for any <%=brandName & " " & modelName%> owner, WirelessEmporium's cases can be fully personalized with any pictures and text. Transform this impact-resistant case into a work of art with a design that is vibrantly printed in full color using advanced ECO-UV inks. 100% satisfaction guaranteed!
	<br /><br />
	<div align=center>
	<img src="/images/productTabs/pdp-hori-div.png">
	<br /><br />
	<iframe width=640 height=360 src=http://www.youtube.com/embed/epi2_d2PhUA?feature=player_detailpage frameborder=0 allowfullscreen></iframe>
	<br /><br />
	<img src="/images/productTabs/pdp-hori-div.png">
	<br /><br />
	<img src="/images/productTabs/custom-pdp-graphic.png">
	</div>
	<% end if %>
</div>
<div id="compBody" class="fl tabContentInactive"></div>
<div id="reviewDetails" class="fl tabContentInactive">
	<!--#include virtual="/includes/asp/product/productreview3.asp"-->
</div>
<div id="qaDetails" class="fl tabContentInactive">
    <%
        ugcID = "|"
        function getNumOfContent(searchIdx, searchVal, dupCheckIdx, byref arr)
	        ret = 0
	        strDupCheck = "|"
	        if isarray(arr) then
		        for k=0 to ubound(arr,2)
			        if not isnull(searchVal) and not isnull(arr(dupCheckIdx,k)) then
				        if cstr(searchVal) = cstr(arr(searchIdx,k)) and instr(strDupCheck, "|" & arr(dupCheckIdx,k) & "|") <= 0 then
					        ret = ret + 1
				        end if
				        strDupCheck = strDupCheck & arr(dupCheckIdx,k) & "|"
			        end if
		        next
	        end if
	        getNumOfContent = ret
        end function

        function printUserName(userName)
	        if trim(userName) = "" then
		        printUserName = "Anonymous"
	        else
		        printUserName = userName
	        end if
        end function
    %>
    <table border="0" align="left" cellspacing="0" cellpadding="0" width="100%">
        <tr>
    	    <td align="center" colspan="2" style="border-right:1px solid #ccc; border-left:1px solid #ccc;">
			    <span class="TurnToItemTeaser"></span>
			    <script type="text/javascript">
			        var TurnToItemSku = "<%=itemid%>";
			        document.write(unescape("%3Cscript src='" + document.location.protocol + "//static.www.turnto.com/traServer3/itemjs/" + turnToConfig.siteKey + "/" + TurnToItemSku + "' type='text/javascript'%3E%3C/script%3E"));
                </script>
            </td>
        </tr>
        <tr>
    	    <td colspan="2" align="left" style="padding-left:20px; border-right:1px solid #ccc; border-left:1px solid #ccc; border-bottom:1px solid #ccc;">
			    <%
                if not isnull(arrUGC) then
			    %>
			    <div class="qnaWrapper">
                <%
                    numQuestions = cint(0)
                    numAnswers = cint(0)
                    numAns = cint(0)
                    nQuestions = cint(getNumOfContent(3, 1, 0, arrUGC))
                    nComments = cint(getNumOfContent(3, 4, 0, arrUGC))
                    curQID = cint(-1)
                    if nQuestions > 0 then
                    %>
                    <div style="float:left; width:95%; padding:10px 5px 0px 5px; border-top:1px solid #ccc;" class="boldText" align="left">
                        <% if nQuestions > 1 then %>
                            <%=formatnumber(nQuestions,0)%> QUESTIONS FROM THE COMMUNITY        
                        <% else %>
                            <%=formatnumber(nQuestions,0)%> QUESTION FROM THE COMMUNITY
                        <% end if %>
                    </div>    
                    <%
                    end if
                    for i=0 to ubound(arrUGC, 2)
                        qid = cint(arrUGC(0,i))
                        qName = arrUGC(1,i)
                        qContent = arrUGC(2,i)
                        qContentType = arrUGC(3,i)
                        qPostDate = arrUGC(4,i)
                        aid = arrUGC(5,i)
                        aName = arrUGC(6,i)
                        aContent = arrUGC(7,i)
                        aContentType = arrUGC(8,i)
                        aPostDate = arrUGC(9,i)
                        rid = arrUGC(10,i)
                        rName = arrUGC(11,i)
                        rContent = arrUGC(12,i)
                        rContentType = arrUGC(13,i)
                        rPostDate = arrUGC(14,i)
                    
                        if qContentType	<> "4" then
                            if not isnull(qid) and instr(ugcID, "|" & qid & "|") <= 0 then
                                numQuestions = numQuestions + 1
                                ugcID = ugcID & qid & "|"
                            %>
                                <div style="float:left; width:100%; height:10px;"></div>
                                <div style="float:left; width:70%; padding:15px 5px 5px 35px; font-weight:bold; position: relative; z-index:1; border-top:1px solid #ccc;" class="qna-normalText">
                                    <div style="width:20px; height:20px; position:absolute; top:15px; left:0px; z-index:2;" class="qna-normalText"><%=formatnumber(numQuestions,0)%>.)</div>
                                    <%=qContent%><br />
                                    <div style="color:#666; padding-top:5px;" class="smlText">Asked by <%=printUserName(qName)%>&nbsp;&nbsp;|&nbsp;&nbsp;<%=qPostDate%></div>
                                </div>
                                <div style="float:left; width:20%; padding:15px 5px 5px 5px; color:#3699D4; border-top:1px solid #ccc;" align="right" class="smlText">
                                    <%
                                    numAnswers = cint(formatnumber(getNumOfContent(0, qid, 5, arrUGC),0))
                                    if numAnswers > 1 then 
                                        response.write numAnswers & " Answers<br />"
                                        response.write "<a id=""" & qid & "_id_viewAns"" href=""javascript:void(0)"" onclick=""toggleAnswer(" & qid & "," & numAnswers & ");"" style=""color:#3699D4;"">View all answers</a>"
                                    else
                                        response.write numAnswers & " Answer"
                                    end if
                                    %>
                                </div>
                            <%
                            end if
            
                            if not isnull(aid) and instr(ugcID, "|" & aid & "|") <= 0 then
                                if qid <> curQID then
                                    curQID = qid
                                    numAns = 0
                                end if
                                numAns = numAns + 1
                                ugcID = ugcID & aid & "|"
                            
                                curAnswerStyle = ""				
                                if numAns > 1 then
                                    curAnswerStyle = "display:none;"
                                %>
                                <div id="ans_<%=qid%>_<%=numAns%>" style="float:left; width:90%; padding:5px 5px 5px 45px; font-size:11px; <%=curAnswerStyle%>" class="qna-normalText2">
                                    <span style="color:#FF6600; font-weight:bold;" class="normalText">Answer</span>&nbsp;&nbsp;<%=aContent%>&nbsp;&nbsp;&nbsp;
                                </div>
                                <%
                                else
                                %>
                                <div style="float:left; width:90%; padding:5px 5px 5px 45px; font-size:11px;" class="qna-normalText2">
                                    <span style="color:#FF6600; font-weight:bold;" class="normalText">Answer</span>&nbsp;&nbsp;<%=aContent%>&nbsp;&nbsp;&nbsp;
                                </div>
                                <%
                                end if
                            end if
                    
                            if not isnull(rid) and instr(ugcID, "|" & rid & "|") <= 0 then
                                ugcID = ugcID & rid & "|"
                                if numAns > 1 then
                                %>
                                <div id="rpy_<%=qid%>_<%=numAns%>" style="float:left; width:85%; padding:5px 5px 5px 65px; font-size:11px; <%=curAnswerStyle%>" class="qna-normalText2">
                                    <span style="color:#3699D4; font-weight:bold;" class="normalText">Reply</span>&nbsp;&nbsp;<%=rContent%>&nbsp;&nbsp;&nbsp;
                                </div>
                                <%
                                else
                                %>
                                <div style="float:left; width:85%; padding:5px 5px 5px 65px; font-size:11px;" class="qna-normalText2">
                                    <span style="color:#3699D4; font-weight:bold;" class="normalText">Reply</span>&nbsp;&nbsp;<%=rContent%>&nbsp;&nbsp;&nbsp;
                                </div>
                                <%
                                end if
                            end if
                        end if
                    next
            
                    if nComments > 0 then
                        if nQuestions > 0 then
                        %>
                        <div style="float:left; width:100%; height:40px;"></div>
                        <%
                        end if
                    %>
                    <div style="float:left; width:95%; padding:10px 5px 10px 5px; border-top:1px solid #ccc; border-bottom:1px solid #ccc;" class="boldText" align="left">
                        <% if nComments > 1 then %>
                            <%=formatnumber(nComments,0)%> COMMENTS FROM THE COMMUNITY        
                        <% else %>
                            <%=formatnumber(nComments,0)%> COMMENT FROM THE COMMUNITY
                        <% end if %>
                    </div>    
                    <%
                    end if
                    for i=0 to ubound(arrUGC, 2)
                        qid = arrUGC(0,i)
                        qName = arrUGC(1,i)
                        qContent = arrUGC(2,i)
                        qContentType = arrUGC(3,i)
                        qPostDate = arrUGC(4,i)
                        aid = arrUGC(5,i)
                        aName = arrUGC(6,i)
                        aContent = arrUGC(7,i)
                        aContentType = arrUGC(8,i)
                        aPostDate = arrUGC(9,i)
                        rid = arrUGC(10,i)
                        rName = arrUGC(11,i)
                        rContent = arrUGC(12,i)
                        rContentType = arrUGC(13,i)
                        rPostDate = arrUGC(14,i)
            
                        if qContentType	= "4" then
                            if not isnull(qid) and instr(ugcID, "|" & qid & "|") <= 0 then
                                numComments = numComments + 1
                                ugcID = ugcID & qid & "|"
                            %>
                                <div style="float:left; width:90%; padding:15px 5px 5px 35px; font-weight:bold; position: relative; z-index:1;" class="qna-normalText">
                                    <div style="width:20px; height:20px; position:absolute; top:15px; left:0px; z-index:2;" class="qna-normalText"><%=formatnumber(numComments,0)%>.)</div>
                                    <%=qContent%><br />
                                    <span style="color:#666;" class="smlText">Commented by <%=printUserName(qName)%>&nbsp;&nbsp;|&nbsp;&nbsp;<%=qPostDate%></span>
                                </div>
                            <%
                            end if
                        end if
                    next
				    %>
                </div>
                <%
                end if
                %>
            </td>
        </tr>
    </table>
</div>
<!-- Recently viewed items -->
<% if showRecent = 1 and preStrands = 1 then %>
<div class="tb titleBar">
	<div class="fl titleIconRecent"></div>
    <div class="fl titleMainBar">
        <div class="fl" style="font-size:inherit;">Recently Viewed Items</div>
        <div class="fr backToTopText" onclick="window.scrollTo(0,0)">Back To Top</div>
        <div class="fr backToTopIcon" onclick="window.scrollTo(0,0)"></div>
    </div>
    <div class="fl titleRight"></div>
</div>
<div class="fl prevItem"><input type="image" src="/images/buttons/bigPrevBlue.gif" width="21" height="103" onmouseup="rvScroll(-1)" /></div>
<div class="fl recommenedProducts">
	<div id="rvScrollBox" class="recommendedProductList">
    	<table border="0" cellpadding="0" cellspacing="0">
        	<tr>
				<%
				rvProdCnt = 0
                do while not recentRS.EOF
					if not isnull(recentRS("price_our")) then
						rvProdCnt = rvProdCnt + 1
                %>
                <td class="recCell" onclick="window.location='/p-<%=recentRS("itemID")%>-<%=formatSEO(recentRS("itemDesc"))%>'">
                	<div class="recPic mCenter"><a href="/p-<%=recentRS("itemID")%>-<%=formatSEO(recentRS("itemDesc"))%>"><img src="/productpics/thumb/<%=recentRS("itemPic")%>" border="0" width="100" height="100" /></a></div>
                    <div class="recDesc"><%=recentRS("itemDesc")%></div>
                    <div class="recRetail">Reg. <%=formatCurrency(recentRS("price_retail"),2)%></div>
                    <div class="recPrice"><%=formatCurrency(recentRS("price_our"),2)%></div>
                </td>
                <%
					end if
                    recentRS.movenext
                loop
                %>
            </tr>
        </table>
    </div>
</div>
<div class="fl nextItem"><input type="image" src="/images/buttons/bigNextBlue.gif" width="21" height="103" onmouseup="rvScroll(1)" /></div>
<% end if %>
<div class="tb titleBar">
	<div class="fl titleIconExclusiveOffer"></div>
    <div class="fl titleMainBar">
        <div class="fl" style="font-size:inherit;">Exclusive Partner Offer</div>
        <div class="fr backToTopText" onclick="window.scrollTo(0,0)">Back To Top</div>
        <div class="fr backToTopIcon" onclick="window.scrollTo(0,0)"></div>
    </div>
    <div class="fl titleRight"></div>
</div>
<div class="tb mCenter srBox">
    <div class="mCenter srInnerBox" name="sr_productDetailDiv"></div>
</div>
<div class="tb titleBar">
	<div class="fl titleIconShareThoughts"></div>
    <div class="fl titleMainBar">
        <div class="fl" style="font-size:inherit;">Share Your Thoughts With Us</div>
        <div class="fr backToTopText" onclick="window.scrollTo(0,0)">Back To Top</div>
        <div class="fr backToTopIcon" onclick="window.scrollTo(0,0)"></div>
    </div>
    <div class="fl titleRight"></div>
</div>
<div class="tb commentDiv"><!--#include virtual="/includes/asp/product/inc_commentBox.asp"--></div>
<div id="imgLarge-location-pool" style="display:none;"></div>