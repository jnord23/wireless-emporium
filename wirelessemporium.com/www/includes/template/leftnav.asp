<%
'sql	=	"select	15 brandid, '...OTHER' brandName, 0 phoneOnly, 99 leftNavOrder_WE union" & vbcrlf & _
'		"select	brandid, brandName, 0 phoneOnly, leftNavOrder_WE " & vbcrlf & _
'		"from	we_brands" & vbcrlf & _
'		"where	brandid in (1,2,3,4,5,6,7,9,10,11,14,16,17,18,19,20)" & vbcrlf & _
'		"order by leftNavOrder_WE" & vbcrlf
		
sql	=	"exec accessoryFinder"
set objRsTopBrand = oConn.execute(sql)
%>
<script language="javascript">
	function onFinder(finderType, o) {
		if (finderType == "b") {
			ajax('/ajax/accessoryFinder.asp?brandid=' + o.value, 'cbModelBox');
			ajax('/ajax/accessoryFinder.asp?modelid=-1', 'cbCatBox');
		}
		else if (finderType == "m") {
			ajax('/ajax/accessoryFinder.asp?modelid=' + o.value, 'cbCatBox');			
		} 
	}
	
	function onFinder2(finderType, o) {
		if (finderType == "b") {
			ajax('/ajax/accessoryFinder.asp?hzBox=1&brandid=' + o.value, 'cbModelBox2');
			ajax('/ajax/accessoryFinder.asp?hzBox=1&modelid=-1', 'cbCatBox2');
		}
		else if (finderType == "m") {
			ajax('/ajax/accessoryFinder.asp?hzBox=1&modelid=' + o.value, 'cbCatBox2');			
		} 
	}
	
	function jumpTo() {
		var oForm = document.getElementById('id_frmFinder');
		var brandid = oForm.cbBrand.value;
		var modelid = oForm.cbModel.value;
		var categoryid = oForm.cbCat.value;
		var brandName = ""
		var modelName = ""
		var categoryName = ""
		var strLink = ""
		
		if (brandid != "" && modelid != "" && categoryid != "") {
			brandName = oForm.cbBrand.options[oForm.cbBrand.selectedIndex].text;
			modelName = oForm.cbModel.options[oForm.cbModel.selectedIndex].text;
			categoryName = oForm.cbCat.options[oForm.cbCat.selectedIndex].text;
			strLink = "sb-" + brandid + "-sm-" + modelid + "-sc-" + categoryid + "-cell-phones-" + categoryName + "-" + brandName + "-" + modelName;
			window.location = "/" + formatSEO(strLink);
		}
		else if (brandid != "" && modelid != "") {
			brandName = oForm.cbBrand.options[oForm.cbBrand.selectedIndex].text;
			modelName = oForm.cbModel.options[oForm.cbModel.selectedIndex].text;
			strLink = brandName + "-" + modelName + "-accessories";
			window.location = "/" + formatSEO(strLink);
		}
		else if (brandid != "") {
			brandName = oForm.cbBrand.options[oForm.cbBrand.selectedIndex].text;
			strLink = brandName + "-phone-accessories";
			window.location = "/" + formatSEO(strLink);
		}
//		trackClick("AccessoryFinder", strLink, strLink);
	}
	
	function jumpTo2() {
		var brandid = document.frmFinder2.cbBrand.value;
		var modelid = document.frmFinder2.cbModel.value;
		var categoryid = document.frmFinder2.cbCat.value;
		var brandName = ""
		var modelName = ""
		var categoryName = ""
		var strLink = ""
		
		if (brandid != "" && modelid != "" && categoryid != "") {
			brandName = document.frmFinder2.cbBrand.options[document.frmFinder2.cbBrand.selectedIndex].text;
			modelName = document.frmFinder2.cbModel.options[document.frmFinder2.cbModel.selectedIndex].text;
			categoryName = document.frmFinder2.cbCat.options[document.frmFinder2.cbCat.selectedIndex].text;
			strLink = "sb-" + brandid + "-sm-" + modelid + "-sc-" + categoryid + "-cell-phones-" + categoryName + "-" + brandName + "-" + modelName;
			window.location = "/" + formatSEO(strLink);
		}
		else if (brandid != "" && modelid != "") {
			brandName = document.frmFinder2.cbBrand.options[document.frmFinder2.cbBrand.selectedIndex].text;
			modelName = document.frmFinder2.cbModel.options[document.frmFinder2.cbModel.selectedIndex].text;
			strLink = brandName + "-" + modelName + "-accessories";
			window.location = "/" + strLink;
		}
//		trackClick("AccessoryFinder", strLink, strLink);
	}
</script>
	<!--
	<style>
    #customPhone {
		width:200px;
		height:119px;
		margin-bottom:10px;
		background:url(/images/customCase/weCustoms.png);
		cursor:pointer;
	}
    </style>
	<a href="/customized-phone-cases" title="Customize Your Case.  Easily add your own images, photos and text for $19.99.  Start here."><div id="customPhone"></div></a>
    -->
	<div id="mvt_accessoryFinder">
        <div id="leftNav-AccessoryTop"></div>
        <div id="leftNav-AccessoryMiddle" style="width:200px;">
            <form id="id_frmFinder" name="frmFinder" style="padding:10px 0px 0px 10px;">
                <select name="cbBrand" style="width:175px;" onchange="onFinder('b',this);">
                    <option value="">Select Your Brand</option>
                    <optgroup label="Top Brands">
                        <option value="9">Samsung</option>
                        <option value="17">Apple</option>
                        <option value="4">LG</option>
                        <option value="30">ZTE</option>
                        <option value="5">Motorola</option>
                        <option value="7">Nokia</option>
                    </optgroup>
                    <%
					lastLabel = ""
                    do until objRsTopBrand.eof
						if objRsTopBrand("groupLabel") <> lastLabel then
							if lastLabel <> "" then response.write "</optgroup>"
							response.write "<optgroup label=""" & objRsTopBrand("groupLabel") & """>"
							lastLabel = objRsTopBrand("groupLabel")
						end if
                        leftNavBrandid = objRsTopBrand("brandid")
                        leftNavBrandName = replace(objRsTopBrand("brandName"), "/", " / ")
    	                %>
	                    <option value="<%=leftNavBrandid%>"><%=leftNavBrandName%></option>
        	            <%
                        objRsTopBrand.movenext
                    loop
                    %>
                    </optgroup>
                </select>
                <div id="cbModelBox" style="margin-top:15px;">
                <select name="cbModel" style="width:175px;" onchange="onFinder('m',this);">
                    <option value="">Select Phone Model</option>
                </select>
                </div>
                <div id="cbCatBox" style="margin-top:15px;">
                <select name="cbCat" style="width:175px;">
                    <option value="">Select Category</option>
                </select>
                </div>
            </form>
        </div>
        <div id="leftNav-AccessoryBottom" style="margin-bottom:10px;" onclick="jumpTo();"></div>
    </div>

    <div id="mvt_buyRiskFree" onclick="freeReturnShippingPopup(0)"></div>
	<% if pageName = "bmcd" or pageName = "bmc-b" then %>
		<% if pageName = "bmcd" then %>
		<div id="mvt_bmcd_leftNav1">
			<div>
			<%
			'subtypeid, subtypename, subtypeimg, typeid, typename
			'topNavWidth, topLast, noDiv, typeStart, typeEnd
			'b.cnt, a.typeNameSEO_WE, a.subTypeNameSEO_WE, a.typeLinkStyle, a.topNavText
			'a.topNavImg, a.type_stitch_pos, a.subtype_stitch_pos
			'response.write sql & "<br />"
			
			arrCurCategories = filter2DRS(arrTopNav, 3, cstr(parentTypeID))

			lap = 0
			for nRow=0 to ubound(arrCurCategories, 2)
				strCategoryName = arrCurCategories(4, nRow)
				strCategoryNameSEO = arrCurCategories(11, nRow)
				strSubcategoryName = arrCurCategories(1, nRow)
				strSubcategoryNameSEO = arrCurCategories(12, nRow)
				strTypeID = cint(arrCurCategories(3, nRow))
				strSubtypeID = cint(arrCurCategories(0, nRow))
				isAvailable = arrCurCategories(20, nRow)
				if isAvailable then
					lap = lap + 1
					if lap = 1 then
				%>
				<div style="background:url(/images/leftnav/leftnav-header-background1.jpg) no-repeat; width:200px; height:46px;" title="<%=strCategoryNameSEO%>">
					<a href="javascript:void(0)" style="cursor:default; color:#fff; width:200px; height:46px; text-align:center; font-weight:bold; font-size:14px; display:table-cell; vertical-align:middle; text-decoration:none;" title="<%=strCategoryNameSEO%>">
						<%=ucase(strCategoryName)%>
					</a>
				</div>
				<%
					end if

					targetBlank = ""
					if strSubtypeID = 1394 then
						strLink = "http://www.cellstores.com/mobile/?r=wirelessemporium2"
						targetBlank = "target=""_blank"""
					elseif modelid = 0 then
						strLink = "/" & formatSEO(strSubcategoryNameSEO)
					else
						strLink = "/sb-" & brandid & "-sm-" & modelid & "-scd-" & strSubtypeID & "-" & formatSEO(strSubcategoryName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)
					end if
					
					if strSubtypeID <> 1064 then
						if nRow = ubound(arrCurCategories, 2) then
				%>
				<div id="leftNav-BrandBottom"><a href="<%=strLink%>" <%=targetBlank%> class="leftNavLink2" title="<%=strSubcategoryName%>"><%=strSubcategoryName%></a></div>
				<%
						else
				%>
				<div id="leftNav-BrandMiddle"><a href="<%=strLink%>" <%=targetBlank%> class="leftNavLink2" title="<%=strSubcategoryName%>"><%=strSubcategoryName%></a></div>
				<%
						end if
					end if
				end if
			next
			%>
			</div>
		</div>
		<% else %>
		<% if parentTypeID <> 15 and parentTypeID <> 16 then %>
		<div class="type-filter">
			<div class="header">FILTER BY TYPE</div>
			<div class="filters">
			<form name="typeFilter" id="typeFilter">
			<%
			hidSubCatSelected = prepStr(request.form("hidSubCatSelected"))
			if parentTypeID = 15 then 
				parentTypeID = 16
			end if

			arrCurCategories = filter2DRS(arrTopNav, 3, cstr(parentTypeID))
			tooltip = ""
			lap = 0
			allSubTypes = ""
			for nRow=0 to ubound(arrCurCategories, 2)
				strCategoryName = arrCurCategories(4, nRow)
				strCategoryNameSEO = arrCurCategories(11, nRow)
				strSubcategoryName = arrCurCategories(1, nRow)
				strSubcategoryNameSEO = arrCurCategories(12, nRow)
				strTypeID = cint(arrCurCategories(3, nRow))
				strSubtypeID = cint(arrCurCategories(0, nRow))
				isAvailable = arrCurCategories(20, nRow)
				bmcFilterText = arrCurCategories(21, nRow)
				if strSubcategoryName = "Design Faceplates-Covers" then
					strSubcategoryName = "Design Faceplates- Covers"
				end if
				
				if isAvailable and strSubtypeID <> "1371" then
					lap = lap + 1
					if nRow = ubound(arrCurCategories, 2) then useClass = "bmcdFilterTypeRowBottom" else useClass = "bmcdFilterTypeRow"
					if len(strSubcategoryName) >= 24 then useTxtClass = "tall-filter" else useTxtClass = "filter"
				%>
				<div class="<%=useTxtClass%>">
					<div class="chkbox">
						<input type="checkbox" id="f<%=lap%>" name="subtypes[]" value="<%=strSubtypeID%>" <%if instr(hidSubCatSelected, "," & strSubtypeID & ",") > 0 then%>checked<%end if%> onclick="subTypeFilter();" />
						<label for="f<%=lap%>"><span><%=strSubcategoryName%></span></label>
					</div>
					<a href="javascript:void(0); return false;" id="qf<%=lap%>" class="tooltip"><div class="qmark"></div></a>
				</div>
				<%
					if allSubTypes <> "" then
						allSubTypes = allSubTypes & ","
					end if
					
					allSubTypes = allSubTypes &  "'" & strSubtypeID & "'"					
					
					current_tooltip = ""
					current_tooltip = current_tooltip & "<div id='tqf" & lap & "' class='tooltip-show' style='display:none;'>"
					current_tooltip = current_tooltip & "<div class='tip'></div>"
					current_tooltip = current_tooltip & "<div class='ttcontent'>"
					current_tooltip = current_tooltip & "<div class='image'><img src='/images/bmcdb/icons/" & strSubtypeID & ".png' /></div>"
					current_tooltip = current_tooltip & "<div class='text'>" & bmcFilterText & "</div>"
					current_tooltip = current_tooltip & "</div>"
					current_tooltip = current_tooltip & "</div>"
					
					tooltip = tooltip & current_tooltip
				end if
			next
			%>
			</form>
			</div>
		</div>
		<% end if %>
	<% end if %>
    <div style="float:left;">
        <div style="margin-top:10px;">
        <%
		arrOtherCategories = filter2DRS(arrTopNav, 8, "1")
		lap = 0
		for nRow=0 to ubound(arrOtherCategories, 2)
			if not isnull(arrOtherCategories(18, nRow)) then
				strCategoryName = arrOtherCategories(4, nRow)
				strCategoryNameSEO = arrOtherCategories(11, nRow)
				strTypeID = cint(arrOtherCategories(3, nRow))
					
				if cstr(parentTypeID) <> cstr(arrOtherCategories(3, nRow)) and cstr(strTypeID) <> "24" then
					lap = lap + 1
					if lap = 1 then
					%>
					<div style="background:url(/images/leftnav/leftnav-header-background1.jpg) no-repeat; width:200px; height:46px;" title="OTHER CATEGORIES">
						<a href="javascript:void(0)" style="cursor:default; color:#fff; width:200px; height:46px; text-align:center; font-weight:bold; font-size:14px; display:table-cell; vertical-align:middle; text-decoration:none;" title="OTHER CATEGORIES">
							OTHER CATEGORIES
						</a>
					</div>
					<%
					end if
	
					if strTypeID = 8 then
						strLink = "/" & formatSEO(strCategoryNameSEO)
					elseif strTypeID = 15 then
						strLink = "/sb-" & brandid & "-sm-0-sc-" & strTypeID & "-" & formatSEO(strCategoryName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)
					elseif modelid = 0 then
						strLink = "/" & formatSEO(strCategoryNameSEO)
					else
						strLink = "/sb-" & brandid & "-sm-" & modelid & "-sc-" & strTypeID & "-" & formatSEO(strCategoryName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)
					end if
					strLink = replace(strLink,"/cell-phone","/phone")
					if nRow = ubound(arrOtherCategories, 2) then
					%>
					<div id="leftNav-BrandBottom">
						<a href="<%=strLink%>" class="leftNavLink2" title="<%=strCategoryNameSEO%>">
							<%=strCategoryName%>
						</a>
					</div>
					<%
					else
					%>
					<div id="leftNav-BrandMiddle">
						<a href="<%=strLink%>" class="leftNavLink2" title="<%=strCategoryNameSEO%>">
							<%=strCategoryName%>
						</a>
					</div>
					<%
					end if
				end if
			end if
		next
		%>
        </div>
    </div>
	<% else%>
	<div style="float:left;">
        <div id="leftNav-BrandTop"></div>
        <div id="leftNav-BrandMiddle"><a href="/apple-iphone-accessories" class="leftNavLink" title="iPhone Accessories">iPHONE</a></div>
        <div id="leftNav-BrandMiddle"><a href="/apple-ipod-ipad-accessories" class="leftNavLink" title="iPod/iPad Accessories">iPOD / iPAD</a></div>
        <div id="leftNav-BrandMiddle"><a href="/blackberry-phone-accessories" class="leftNavLink" title="BlackBerry Accessories">BLACKBERRY</a></div>
        <div id="leftNav-BrandMiddle"><a href="/casio-phone-accessories" class="leftNavLink" title="Casio Accessories">CASIO</a></div>
        <div id="leftNav-BrandMiddle"><a href="/hp-palm-phone-accessories" class="leftNavLink" title="HP/Palm Accessories">HP / PALM</a></div>
        <div id="leftNav-BrandMiddle"><a href="/htc-phone-accessories" class="leftNavLink" title="HTC Accessories">HTC</a></div>
        <div id="leftNav-BrandMiddle"><a href="/huawei-phone-accessories" class="leftNavLink" title="Huawei Accessories">HUAWEI</a></div>
        <div id="leftNav-BrandMiddle"><a href="/kyocera-phone-accessories" class="leftNavLink" title="Kyocera Accessories">KYOCERA</a></div>
        <div id="leftNav-BrandMiddle"><a href="/lg-phone-accessories" class="leftNavLink" title="LG Accessories">LG</a></div>
        <div id="leftNav-BrandMiddle"><a href="/motorola-phone-accessories" class="leftNavLink" title="Motorola Accessories">MOTOROLA</a></div>
        <div id="leftNav-BrandMiddle"><a href="/nokia-phone-accessories" class="leftNavLink" title="Nokia Accessories">NOKIA</a></div>
        <div id="leftNav-BrandMiddle"><a href="/pantech-phone-accessories" class="leftNavLink" title="Pantech Accessories">PANTECH</a></div>
        <div id="leftNav-BrandMiddle"><a href="/samsung-phone-accessories" class="leftNavLink" title="Samsung Accessories">SAMSUNG</a></div>
        <div id="leftNav-BrandMiddle"><a href="/sony-ericsson-phone-accessories" class="leftNavLink" title="Sony Ericsson Accessories">SONY ERICSSON</a></div>
        <div id="leftNav-BrandMiddle"><a href="/zte-phone-accessories" class="leftNavLink" title="ZTE Accessories">ZTE</a></div>
        <div id="leftNav-BrandMiddle"><a href="/tablet-ereader-accessories" class="leftNavLink" title="Tablets eReaders Accessories">Tablets / eReaders</a></div>
        <div id="leftNav-BrandBottom"><a href="/other-phone-accessories" class="leftNavLink" title="Other Brand Name Cell Phone Accessories">...OTHER</a></div>
	</div>    
    <% end if%>
    <%
	if pageName = "product-b.asp" then
		if isobject(recentRS) then
			if not recentRS.EOF then
	%>
    <a href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=13198065" target="_blank" style="text-decoration:none;"><div id="leftNav-BBB" style="margin-top:10px;float:left;"></div></a>
    <div class="strandsRecs" tpl="prod_1" item="<%=itemID%>"></div>
    <%
			else
	%>
    <a href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=13198065" target="_blank" style="text-decoration:none;"><div id="leftNav-BBB" style="margin-top:10px;float:left;"></div></a>
    <!--
    <div style="padding-left:15px;float:left;"><a href="http://www.cellstores.com/template/simpleautocontent.aspx?pageid=9729&referringdomain=WirelessEmporium2&refcode1=&refcode2=&agent=&eid=&zipcode=" target="_blank"><div id="leftNav-phonePlanTop"></div></a></div>
    <div style="padding-left:15px;float:left;"><a href="http://www.cellstores.com/mobile/?r=wirelessemporium2" target="_blank"><div id="leftNav-phonePlanBottom"></div></a></div>
    -->
    <%
			end if
		else
	%>
    <a href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=13198065" target="_blank" style="text-decoration:none;"><div id="leftNav-BBB" style="margin-top:10px;float:left;"></div></a>
    <!--
    <div style="padding-left:15px;float:left;"><a href="http://www.cellstores.com/template/simpleautocontent.aspx?pageid=9729&referringdomain=WirelessEmporium2&refcode1=&refcode2=&agent=&eid=&zipcode=" target="_blank"><div id="leftNav-phonePlanTop"></div></a></div>
    <div style="padding-left:15px;float:left;"><a href="http://www.cellstores.com/mobile/?r=wirelessemporium2" target="_blank"><div id="leftNav-phonePlanBottom"></div></a></div>
    -->
    <%
		end if
	%>
    <% else %>
    <a href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=13198065" target="_blank" style="text-decoration:none;"><div id="leftNav-BBB" style="float:left;"></div></a>
    <!--
    <div style="padding-left:15px;float:left;"><a href="http://www.cellstores.com/template/simpleautocontent.aspx?pageid=9729&referringdomain=WirelessEmporium2&refcode1=&refcode2=&agent=&eid=&zipcode=" target="_blank"><div id="leftNav-phonePlanTop"></div></a></div>
    <div style="padding-left:15px;float:left;"><a href="http://www.cellstores.com/mobile/?r=wirelessemporium2" target="_blank"><div id="leftNav-phonePlanBottom"></div></a></div>
    -->
    <% end if %>
    <div id="mvt_srRunnerLeftBottom" onclick="sr_$.learn('small banner')"></div>