<%
	if isnull(noAsp) or noAsp = "" then noAsp = True
	if noAsp then
		if instr(request.ServerVariables("HTTP_X_REWRITE_URL"),".asp") > 0 and instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"_mp.asp") < 1 then
			response.Status = "410 Gone"
			response.End()
		end if
	end if
	
	saveEmail()
	
	topPageName = "product"

	siteId = 0
	'cmsData(request.ServerVariables("HTTP_X_REWRITE_URL"))
	'metaArray = split(session("metaTags"),"##")
	'SEtitle = metaArray(0)
	'SEdescription = metaArray(1)
	'SEkeywords = metaArray(2)
	'SeTopText = metaArray(3)
	'SeBottomText = metaArray(4)
	
	curPage = prepStr(request.QueryString("curPage"))
	if curPage <> "" then useURL = curPage else useURL = request.ServerVariables("HTTP_X_REWRITE_URL") end if
	
	metaString = ""
	call getCmsData(0,cms_basepage,useURL,metaString)
	if instr(metaString, "!##!") > 0 then
		metaArray = split(metaString,"!##!")
		SEtitle = metaArray(0)
		SEdescription = metaArray(1)
		SEkeywords = metaArray(2)
		SeTopText = metaArray(3)
		SeBottomText = metaArray(4)
		SeH1 = metaArray(5)	
	end if
	
	
	if bSecureHttp then response.redirect("http://" & request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL")) end if
	pageStart = time
	
	if SEtitle = "" then SEtitle = "Buy Cell Phone Accessories from WirelessEmporium ?Cell phone Covers, Faceplates, Charms, Chargers, Batteries - Deep Discounts and Free Shipping"
	if SEdescription = "" then SEdescription = "The #1 Name in Cell Phone Accessories Online - Wireless Emporium. Discount Cell Phone Accessories at Prices up to 75% Off. Buy Wholesale Cellular Accessories, discount wireless accessories and cheap cell phone accessories."
	if SEkeywords = "" then SEkeywords = "cheap cell phone accessories,cell phone accessories,discount cellular accessories,mobile phone accessories,wireless accessories,cellular telephone accessories,wholesale cellular accessories,cellular accessories,wireless phone accessories,wholesale cell phone accessories,cellular phone accessories,wholesale wireless accessories,wholesale cellular phone accessories,discount cell phone accessories,cool cell phone accessories"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><%=SEtitle%></title>
    <meta name="Description" content="<%=SEdescription%>">
    <meta name="Keywords" content="<%=SEkeywords%>">
    <% if prepInt(noIndex) = 1 then %><META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"><% end if %>
    <% if prepStr(request.QueryString("curPage")) <> "" then %>
    <meta name="alternate" content="http://m.wirelessemporium.com<%=prepStr(request.QueryString("curPage")) & ".htm"%>">
	<% else %>
    <meta name="alternate" content="http://m.wirelessemporium.com<%=request.ServerVariables("HTTP_X_REWRITE_URL") & ".htm"%>">
    <% end if %>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
    <link rel="stylesheet" type="text/css" href="/includes/css/base.css<%=arrCssDateModified(0)%>" />
    <!--#include virtual="/includes/asp/inc_HeadTagEnd.asp"-->
</head>
<!--#include virtual="/includes/asp/inc_BodyTagStart.asp"-->
<!--#include virtual="/includes/template/topHTML.asp"-->