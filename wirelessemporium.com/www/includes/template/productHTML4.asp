<div class="pdpMain">
	<div class="breadCrumbs">
		<%if typeID = 16 then%>
        <a class="breadCrumb" href="/unlocked-cell-phones.asp">Unlocked Cell Phones</a>&nbsp;&rsaquo;&nbsp;<a class="breadCrumb" href="/cp-sb-<%=brandID & "-" & formatSEO(brandName)%>-unlocked-cell-phones.asp"><%=brandName%> Unlocked Cell Phones</a>
        <%else%>
            <%if musicSkins = 1 then%>
                <a class="breadCrumb" href="/">Home</a>&nbsp;&rsaquo;&nbsp;
                <a class="breadCrumb" href="/<%=brandSEO(brandID)%>"><%=brandName%> Accessories</a>&nbsp;&rsaquo;&nbsp;
                <a class="breadCrumb" href="<%=modelLink%>"><%=brandName & " " & modelName%> Accessories</a>&nbsp;&rsaquo;&nbsp;
                <a class="breadCrumb" href="/sb-<%=brandID & "-sm-" & modelID & "-sc-" & typeID & "-" & formatSEO(categoryName) & "-genre"%>.asp"><%=brandName & " " & modelName & " " & nameSEO(categoryName)%></a>
            <%elseif isTablet then%>	
                <a class="breadCrumb" href="/tablet-ereader-accessories.asp">Tablet Accessories</a>&nbsp;&rsaquo;&nbsp;
                <a class="breadCrumb" href="<%=modelLink%>"><%=brandName & " " & modelName%> Accessories</a>&nbsp;&rsaquo;&nbsp;
                <a class="breadCrumb" href="/sb-<%=brandID & "-sm-" & modelID & "-sc-" & typeID & "-" & formatSEO(categoryName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp"><%=brandName & " " & modelName & " " & nameSEO(categoryName)%></a>
            <%else%>
                <a class="breadCrumb" href="/">Home</a>&nbsp;&rsaquo;&nbsp;
                <a class="breadCrumb" href="/<%=brandSEO(brandID)%>"><%=brandName%> Accessories</a>&nbsp;&rsaquo;&nbsp;
                <a class="breadCrumb" href="accessories-for-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>.asp"><%=brandName & " " & modelName%> Accessories</a>&nbsp;&rsaquo;&nbsp;
                <a class="breadCrumb" href="/sb-<%=brandID & "-sm-" & modelID & "-scd-" & subTypeID & "-" & formatSEO(categoryName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp"><%=brandName & " " & modelName & " " & nameSEO(categoryName)%></a>
            <% end if %>
        <%end if%>
        &nbsp;&rsaquo;&nbsp;<%=strTypename%>
    </div>
    <div class="ffl">
    	<div class="pdpMainLeft">
			<div id="imgLarge-location" class="ffl" align="center" style="cursor:pointer;" <%=strZoom%>><%=objItemImgFull%></div>        
			<div class="ffl" align="center" style="padding-top:10px;"><%=objZoom%></div>
			<div class="ffl" align="center" style="padding-top:10px;"><%=objCustomize%></div>
			<%
			if numAvailColors > 1 then
			%>
			<div class="ffl" align="center" style="padding-top:10px;">
				<div style="display:inline-block; font-style:italic;" align="center">CURRENT COLOR:&nbsp;</div>
				<%=generateColorPickerText(arrColors, colorSlaves, colorid, "font-weight:bold; text-align:left;")%>
			</div>
			<div class="ffl" align="center" style="padding-bottom:10px;"><%=generateColorPicker(arrColors, itemid, colorSlaves, colorid, numAvailColors, blnIsNotOriginalPrice)%></div>
			<div style="display:none;" id="preloading_deck"></div>
			<%
			end if
			%>
            <div id="imgLarge-location-pool" style="display:none;"><%=replace(objItemImgFull, "itemprop=""image""", "")%></div>
			<div class="ffl" align="center" style="padding-top:10px;">
                <div style="display:inline-block; padding-right:20px;"><%=strVideo%></div>
                <div style="display:inline-block;"><%=strFlash%></div>
                <div class="clr"></div>
			</div>
			<div id="slides1" class="ffl" align="center" style="padding-top:15px; position:relative;">
                <div id="altviewArrowLeft" class="fl clickable btn-arrow-left" style="position:absolute; top:22px; left:40px; display:none;"></div>
                <div id="altviewArrowRight" class="fl clickable btn-arrow-right" style="position:absolute; top:22px; right:40px; display:none;"></div>
                <div id="altviewInner" class="fl slides_container" style="display:none; width:280px; padding-left:10px; margin-left:75px;">
                    <div class="fl slide" style="width:280px; height:70px;">
					<%
					nLap = 0
					a = 0
					if nAltViews > 0 then
						for i=0 to ubound(arrAltViews)
							nLap = nLap + 1
							a = a + 1					
						%>
						<div class="fl item" style="text-align:center; width:70px;">
                        	<%=arrAltViews(i)%>
                        </div>
                        <%
							if ((a mod 4) = 0) and (a < 8) then
								response.write "</div>" & vbcrlf & "<div class=""fl slide"" style=""width:280px; height:70px;"">"
							end if						
						next
					end if
					if nLap > 4 then arrPagination(0) = true
					%>
                    </div>
                </div>        
            </div>            
        </div>
		<form name="frmSubmit" action="/cart/item_add.asp" method="post" style="margin:0px; padding:0px;">
        <!-- rich snippets mark up -->
        <div itemscope itemtype="http://data-vocabulary.org/Product">
        	<!-- pdpMainRight -->
            <div class="pdpMainRight">
                <div class="ffl">
                    <div class="fl">
                        <%
                        for i = 1 to 5
                            if dblAvgRating > i then
                        %>
                        <div class="fl fullStar"></div>
                        <%
                            else
                        %>
                        <div class="fl emptyStar"></div>
                        <%
                            end if
                        next
                        %>
                    </div>
                    <div class="fl" style="color:#336699; padding-left:20px;">(<%=strRatingCount%> Customer Reviews)</div>
                    <div class="fl" style="color:#333; padding:0px 10px 0px 10px;"> | </div>
                    <div class="fl"><a href="/user-reviews-b.asp?itemID=<%=itemid%>&partNumber=<%=partnumber%>" style="color:#336699; text-decoration:none;">Write A Review</a></div>
                </div>
                <div class="ffl" style="padding:5px 0px 10px 0px;">
                    <span itemprop="name"><h1 id="id_h1" class="product"><%=itemDesc%></h1></span>
                </div>
                <div id="part_item_number" class="ffl" style="font-size:14px;">
					<div class="fl" style="font-size:11px;">Part #: <%=partNumber%></div>
					<div class="fl"> &nbsp; &nbsp; </div>
					<div class="fl" style="font-size:11px;">Item ID: <%=itemid%></div>
                </div>
                <div class="AddToCartBox">
					<%if not bFinalOutofStock then%>
                    <div class="fl" style="width:180px;">
                        <div class="ffl" style="color:#555; font-size:14px;">List Price: <del><%=formatCurrency(prepInt(price_Retail))%></del></div>
                        <div class="ffl ourPrice" style="padding:10px 0px 10px 0px;">
                        	<span>Our Price: <%=formatCurrency(price_Our)%></span>
                        	<span style="color:#900; font-size:11px;">(Save <%=formatpercent((price_Retail-price_Our)/price_Retail,0)%>)</span>
						</div>
                        <div class="ffl">
                        	<div class="fl" style="font-size:18px;">w/FREE SHIPPING</div>
                            <div class="fl" style="padding:4px 0px 0px 3px; position:relative;" onmouseover="document.getElementById('id_freeshipping').style.display=''" onmouseout="document.getElementById('id_freeshipping').style.display='none'">
                            	<img src="/images/product/question-mark.png" border="0" />
                                <div id="id_freeshipping" onmouseover="document.getElementById('id_freeshipping').style.display=''" onmouseout="document.getElementById('id_freeshipping').style.display='none'" class="floatingValuePropBox" style="display:none; top:10px; left:12px; ">
									Wireless Emporium strives to provide our customers with a great shopping experience and part of that experience is to offer free shipping on all orders. Regardless of the size of your order we will always offer you <strong>free fast shipping</strong>.
									<br /><br />
									<strong>Place your order before 3pm EST and it will ship from our warehouse that same day.</strong>
                                </div>
							</div>
						</div>
                    </div>
                    <div class="fl" style="width:50px; padding-top:10px;">
                        <div class="ffl" style="text-align:center; color:#222; font-weight:bold;">Qty:</div>
                        <div class="ffl" style="text-align:center; padding-top:5px;"><input class="qtyBox" type="text" name="qty" value="1" maxlength="4" /></div>
                    </div>
                    <div class="fr" style="padding-top:15px;">
                        <input type="image" src="/images/buttons/addToCartGrayBg.gif" onclick="return addToCart();" border="0" alt="Add to Cart">
                    </div>
                    <div class="horiBar"></div>
                    <div class="ffl">
                        <div class="fl"><img src="/images/icons/greenCheckGrayBg.gif" border="0" /></div>
                        <div class="fl stockPresentation" style="padding-top:1px;">
                            <span style="padding-left:5px; color:#649900; font-weight:bold;">In Stock! </span>
                            <span style="font-size:12px;"><%=gblShipoutDate%></span>
                        </div>
                    </div>
                    <%else%>
					<div class="ffl" style="text-align:center; font-weight:bold; font-size:16px; color:#F00;"><br />THIS ITEM IS CURRENTLY<br />OUT OF STOCK<br /><br /></div>
                    <%end if%>
                </div>
                <div class="ffl productFeatures">
                    <div class="fl title">Product Features</div>
                    <div class="ffl list">                
                        <ul>
                        <%for i = 0 to ubound(featureListArray)%>
                            <%if prepStr(featureListArray(i)) <> "" then%>
                            <li><%=featureListArray(i)%></li>
                            <%end if%>
                        <%next%>
                        </ul>
                    </div>
					<div class="ffl">
                    <%
					if customize then
					%>
					<div class="ffl" style="padding:5px 0px 0px 5px; border-top:1px solid #ccc; margin-top:5px;">
						<div class="ffl" style="text-align:left; font-size:12px; padding-left:5px; font-weight:bold;">Customizing this product? Please allow up to <%=gblCustomShippingDate%> business days for production of custom cases prior to shipping time.</div>
					</div>
					<%
					elseif subTypeID = 1031 then
					%>
					<div class="ffl" style="padding:5px 0px 0px 5px; border-top:1px solid #ccc; margin-top:5px;">
						<div class="ffl" style="text-align:left; font-size:12px; padding-left:5px; font-weight:bold;">Please allow up to <%=gblCustomShippingDate%> business days for production prior to shipping time.</div>
					</div>
					<%
					end if
					%>
                    </div>
                </div>
            </div>
        	<!--// pdpMainRight -->
        </div>
        <!--// rich snippets mark up -->
		<input type="hidden" name="musicSkins" value="<%=musicSkins%>" />
    	<input type="hidden" name="prodid" value="<%=itemID%>">
        </form>
    </div>
    <div class="ffl" style="padding-top:20px; margin-bottom:100px;">
    	<div class="prodDescTabs">
        	<div class="left">&nbsp;</div>
        	<div class="middle">
            	<div id="tabHeader1" class="on" style="width:100px;" onclick="onTab(1)">Description</div>
            	<div id="tabHeader2" class="off" style="width:110px;" onclick="onTab(2)">Compatibility</div>
            	<div id="tabHeader3" class="off" style="width:155px;" onclick="onTab(3)">Ratings &amp; Reviews</div>
            	<div id="tabHeader4" class="off" style="width:125px;" onclick="onTab(4)">Customer Q&amp;A</div>
            	<div id="tabHeader5" class="off" style="width:155px;" onclick="onTab(5)">Warranty &amp; Returns</div>
            </div>
        	<div class="right">
            	<!--#include virtual="/includes/asp/product/inc_rewards3.asp"-->
            </div>
        </div>
        <div class="prodContent">
	       	<!-- product description -->
		    <div id="tab1" class="desc">
            	<%if typeid = 16 then%>
            	<div style="position:absolute; top:0px; left:750px; z-index:5;">
                	<img src="/images/product/freeshippingicon_phone.gif" border="0" alt="free shipping on every order" onmouseover="document.getElementById('id_valuePropPhone').style.display=''" onmouseout="document.getElementById('id_valuePropPhone').style.display='none'" />
                    <div style="width:100%; position:relative;">
		            	<div style="position:absolute; top:-100px; left:-335px;">
							<div id="id_valuePropPhone" onmouseover="document.getElementById('id_valuePropPhone').style.display=''" onmouseout="document.getElementById('id_valuePropPhone').style.display='none'" class="floatingValuePropBox" style="display:none; bottom:-100px; left:12px;">
								Wireless Emporium strives to provide our customers with a great shopping experience and part of that experience is to offer free shipping on all orders. Regardless of the size of your order we will always offer you <strong>free fast shipping</strong>.
								<br /><br />
								<strong>Place your order before 3pm EST and it will ship from our warehouse that same day.</strong>
							</div>
                        </div>
                    </div>
				</div>
                <%else%>
            	<div style="position:absolute; top:0px; left:750px; z-index:5;">
                	<img src="/images/product/freeshippingicon.gif" border="0" alt="free shipping on every order, EZ No-Hassle return policy" onmouseover="document.getElementById('id_valuePropAccessory').style.display=''" onmouseout="document.getElementById('id_valuePropAccessory').style.display='none'" />
                    <div style="width:100%; position:relative;">
		            	<div style="position:absolute; top:-100px; left:-335px;">
							<div id="id_valuePropAccessory" onmouseover="document.getElementById('id_valuePropAccessory').style.display=''" onmouseout="document.getElementById('id_valuePropAccessory').style.display='none'" class="floatingValuePropBox" style="display:none; bottom:0px; left:12px;">
								Wireless Emporium strives to provide our customers with a great shopping experience and part of that experience is to offer free shipping on all orders. Regardless of the size of your order we will always offer you <strong>free fast shipping</strong>.
								<br /><br />
								<strong>Place your order before 3pm EST and it will ship from our warehouse that same day.</strong>
                                <br /><br />
                                <strong>Returns:</strong> if you have any reason to return you item, you may return it to us within 90 days for a full refund of your item price. This is to ensure that you, the customer, will always be happy with your WirelessEmporium.com experience.<br /><br />
                                Unlocked phones have a separate return policy. Please see our return policy page for additional details.<br /><br />
                                <a href="/store.asp" target="_blank">Please click here to view our complete return policy</a><br />
                                <br /><br />
                                <strong>Warranty:</strong> all items are covered by our industry leading 365 day warranty policy. If your item break or malfunction within 1 year of your purchase date, we will replace it free of charge!<br /><br />
                                Unlocked phones have a separate warranty policy. Please see our warranty policy page for additional details.<br /><br />
                                <a href="/store.asp" target="_blank">Please click here to view our complete warranty policy</a>
							</div>
                        </div>
                    </div>
				</div>
                <%end if%>
                <div id="prodDescription">
				<%
                if musicSkins = 1 then
                    if fs.FileExists(Server.MapPath(itemSamplePath & "/" & sampleImg)) then
                        session("errorSQL") = "openPic:" & Server.MapPath(itemSamplePath & "/" & sampleImg)
                        jpeg.Open Server.MapPath(itemSamplePath & "/" & sampleImg)
                        if jpeg.OriginalWidth > 300 then
                            jpeg.Width = 300
                            jpeg.Height = jpeg.OriginalHeight * 300 / jpeg.OriginalWidth
                            jpeg.Save Server.MapPath(itemSamplePath & "/" & sampleImg)
                        end if
                %>
                <div style="float:left; margin:0px 20px 15px 20px;"><img src="<%=itemSamplePath & "/" & sampleImg%>" border="0" /></div>
                <%
                    end if
                %>
                <%=brandName%>&nbsp;<%=modelName%> Music Skin <%=insertDetails(itemDesc)%> - There is no better way to personalize your phone than with 
                a Music Skin. Music skins are a thin, "sticker-like" covering made from a patented 3M material that wraps around your 
                entire phone, helping to prevent scratches and dust from marring the look of your phone. Easy to apply and non-permanent, 
                Music Skins are custom cut to fit your phone and add virtually no bulk at all to your phone. Even with your phone wrapped 
                in a Music Skin you can use it in conjunction with any case, holster or charging dock.
                <br /><br />
                <ul>
                    <li>Thin material wraps around your <%=brandName%>&nbsp;<%=modelName%> without adding any bulk</li>
                    <li>Provides basic layer of protection from scratches and dust</li>
                    <li>Premium grade vinyl material and high gloss finish for a stunning look</li>
                    <li>Custom cut and non-permanent</li>
                    <li>NOT REUSABLE</li>
                </ul>
                <br /><br />
                <img src="/images/musicskin/detail.jpg" border="0" width="380" />
                <img src="/images/musicskin/detail2.jpg" border="0" width="380" />
                <br /><br />
                YouTube Video = <a href="http://www.youtube.com/watch?v=Ri3LhsUcqZs" target="_blank">http://www.youtube.com/watch?v=Ri3LhsUcqZs</a>
                <br /><br />
                <iframe title="YouTube video player" width="640" height="390" src="http://www.youtube.com/embed/Ri3LhsUcqZs" frameborder="0" allowfullscreen></iframe>
                <br /><br />
                <%
                else
                    if instr(strItemLongDetail,"Buy this quality") > 0 then
                        useLongDesc = left(strItemLongDetail,instr(strItemLongDetail,"Buy this quality")-1)
                    elseif instr(strItemLongDetail,"Remember - WirelessEmporium.com") > 0 then
                        useLongDesc = left(strItemLongDetail,instr(strItemLongDetail,"Remember - WirelessEmporium.com")-1)
                    else
                        useLongDesc = strItemLongDetail
                    end if
                    response.write "<span itemprop=""description""><p style=""font-size:14px; margin-top:0px;"">" & insertDetails(useLongDesc) & "</p></span>" & vbcrlf
                end if
                if typeID = 16 then
                    response.write "<div style=""font-size:14px; font-weight:bold;"">PHONE FEATURES:</div>" & vbcrlf
                    if BULLET1 <> "" or BULLET2 <> "" or BULLET3 <> "" or BULLET4 <> "" or BULLET5 <> "" or BULLET6 <> "" or BULLET7 <> "" or BULLET8 <> "" or BULLET9 <> "" or BULLET10 <> "" then
                        response.write "<ul style=""font-size:14px; margin-top:0px;"">" & vbcrlf
                        if BULLET1 <> "" then response.write "<li>" & insertDetails(BULLET1) & "</li>" & vbcrlf
                        if BULLET2 <> "" then response.write "<li>" & insertDetails(BULLET2) & "</li>" & vbcrlf
                        if BULLET3 <> "" then response.write "<li>" & insertDetails(BULLET3) & "</li>" & vbcrlf
                        if BULLET4 <> "" then response.write "<li>" & insertDetails(BULLET4) & "</li>" & vbcrlf
                        if BULLET5 <> "" then response.write "<li>" & insertDetails(BULLET5) & "</li>" & vbcrlf
                        if BULLET6 <> "" then response.write "<li>" & insertDetails(BULLET6) & "</li>" & vbcrlf
                        if BULLET7 <> "" then response.write "<li>" & insertDetails(BULLET7) & "</li>" & vbcrlf
                        if BULLET8 <> "" then response.write "<li>" & insertDetails(BULLET8) & "</li>" & vbcrlf
                        if BULLET9 <> "" then response.write "<li>" & insertDetails(BULLET9) & "</li>" & vbcrlf
                        if BULLET10 <> "" then response.write "<li>" & insertDetails(BULLET10) & "</li>" & vbcrlf
                        response.write "</ul>" & vbcrlf
                    end if
                    if COMPATIBILITY <> "" then
                        response.write "<p><div style=""font-size:14px; font-weight:bold;"">COMPATIBILITY:</div>" & COMPATIBILITY & "</p>" & vbcrlf
                    end if
                    %>
                    <p><div style="font-size:14px; font-weight:bold;"> WHAT IS INCLUDED: </div><%=PackageContents%></p>
                    <p><em>Wireless Emporium offers direct replacement and unlocked Cell/PDA/Smart Phones to you WITHOUT ANY CONTRACTS! Order your replacement or unlocked Cell/PDA/Smart Phones today and we'll ship it out to your home or business for FREE!</em></p>
                    <p style="font-size:14px; font-weight:bold;"> Click Here: ** <a href="javascript:Open_Popup('/phone_return_policy.asp','PhoneReturnPolicy','width=550,height=650,scrollbars=no');" style="font-size:14px;font-weight:bold;color:#CC3300;text-decoration:none;">Cell Phone Return Policy</a> **</p>
                    <%
                else
                    if typeID = 1 or typeID = 2 or typeID = 6 or typeID = 7 then
                        'response.write "<h5><i>All factory-direct Wireless Emporium Cell Phone " & nameSEO(categoryName) & " are manufactured to the highest ISO 9001:2000 certified quality standards.</i></h5>" & vbcrlf 'Removed for Penguin update
                    end if
                end if
                if not isnull(itemDimentions) and itemDimentions <> "" then
                    response.write "<p style=""font-size:14px; margin-top:0px;"">Product Dimensions: <span style=""font-size:14px; font-weight:bold;"">" & itemDimentions & "</span></p>" & vbcrlf
                end if
                
                if typeID = 18 then
                %>
                <div>
                    <iframe width='640' height='360' src='http://www.youtube.com/embed/N0JZfqNk0Vk?rel=0&showinfo=1&modestbranding=1&wmode=opaque' frameborder='0' allowfullscreen></iframe>
                </div>
                <div style="padding:15px 0px 15px 0px; cursor:pointer;" onclick="showScreenProtectorInfo();">
                    <img src="/images/product/New-button-screenprotector-infographic.png" border="0" width="710" />
                </div>
                <%
                end if
                if typeID = 1 then
                %>
                <div style="">
                    <iframe width='640' height='360' src='http://www.youtube.com/embed/R7e5niCL0kE?rel=0&showinfo=1&modestbranding=1&wmode=opaque' frameborder='0' allowfullscreen></iframe>
                </div>
                <%
                end if
                %>
                <%
                if customize and instr(strItemLongDetail,"/productTabs/custom_1.jpg") < 1 then
                %>
                <div align=center>
                <a href=/custom/?aspid=<%=itemID%>><img src="/images/productTabs/custom_1.jpg" border="0"></a>
                </div>
                <br />
                Customize your device with your own distinctive flair! An amazing gift for any <%=brandName & " " & modelName%> owner, WirelessEmporium's cases can be fully personalized with any pictures and text. Transform this impact-resistant case into a work of art with a design that is vibrantly printed in full color using advanced ECO-UV inks. 100% satisfaction guaranteed!
                <br /><br />
                <div align=center>
                <img src="/images/productTabs/pdp-hori-div.png">
                <br /><br />
                <iframe width=640 height=360 src=http://www.youtube.com/embed/epi2_d2PhUA?feature=player_detailpage frameborder=0 allowfullscreen></iframe>
                <br /><br />
                <img src="/images/productTabs/pdp-hori-div.png">
                <br /><br />
                <img src="/images/productTabs/custom-pdp-graphic.png">
                </div>
                <% end if %>
                <%
                if download_URL <> "" then response.write "<p><a href=""" & download_URL & """ target=""_blank"">" & download_TEXT & "</a></p>" & vbcrlf																										
                %>
                </div>
            	<div class="cls"></div>
            </div> 
	       	<!--// product description -->
            <!-- product compatibility -->
            <div id="tab2" class="comp" style="display:none;">
                <div id="compBody" class="ffl"></div>
            </div>
            <!--// product compatibility -->
            <!-- ratings & reviews -->
            <div id="tab3" class="review" style="display:none;">
           		<!--#include virtual="/includes/asp/product/inc_productreview3.asp"-->
            </div>
            <!--// ratings & reviews -->
            <!-- Q&A -->
            <div id="tab4" class="qna" style="display:none;">
           		<!--#include virtual="/includes/asp/product/inc_qna3.asp"-->
            </div>
            <!--// Q&A -->
            <!-- Q&A -->
            <div id="tab5" class="warranty" style="display:none;">
	            <strong>Returns:</strong> if you have any reason to return you item, you may return it to us within 90 days for a full refund of your item price. This is to ensure that you, the customer, will always be happy with your WirelessEmporium.com experience.<br /><br />
				Unlocked phones have a separate return policy. Please see our return policy page for additional details.<br /><br />
                <a href="/store.asp" target="_blank">Please click here to view our complete return policy</a><br />
                <br /><br /><br />
				<strong>Warranty:</strong> all items are covered by our industry leading 365 day warranty policy. If your item break or malfunction within 1 year of your purchase date, we will replace it free of charge!<br /><br />
				Unlocked phones have a separate warranty policy. Please see our warranty policy page for additional details.<br /><br />
                <a href="/store.asp" target="_blank">Please click here to view our complete warranty policy</a><br />
            </div>
            <!--// Q&A -->
        </div>
    </div>
	<%
    sql = 	"select	top 20 a.subtypeid, a.subtypename, b.itemid, b.itemdesc, b.price_retail, b.price_our, b.itempic" & vbcrlf & _
            "	,	(	select	count(*)" & vbcrlf & _
            "		from	we_reviews" & vbcrlf & _
            "		where	approved = 1 and partnumbers in ( select partnumbers from we_reviews where itemid = b.itemid)) reviewCount" & vbcrlf & _
            "	,	isnull((	select	avg(rating) " & vbcrlf & _
            "					from	we_reviews" & vbcrlf & _
            "					where	approved = 1 and partnumbers in ( select partnumbers from we_reviews where itemid = b.itemid)), 0) avgReview" & vbcrlf & _
            "from	we_subtypes a cross apply " & vbcrlf & _
            "		(	select	top 3 b.itemid, b.itemdesc, b.price_retail, b.price_our, b.itempic" & vbcrlf & _
            "			from	we_items b" & vbcrlf & _
            "			where	a.subtypeid = b.subtypeid" & vbcrlf & _
            "				and	b.hidelive = 0 and b.inv_qty <> 0" & vbcrlf & _
            "				and	b.modelid in ( select modelid from we_items where itemid = '" & itemid & "' )" & vbcrlf & _
            "				and (select top 1 inv_qty from we_Items where partNumber = b.partNumber and master = 1 order by inv_qty desc) > 0" & vbcrlf & _
            "				and b.itempic is not null and b.itempic <> ''" & vbcrlf & _
            "				and b.partnumber not like 'WCD%'" & vbcrlf & _
            "			order by b.numberOfSales desc	) b"
'		response.write "<pre>" & sql & "</pre>"
	set rs = oConn.execute(sql)
	if not rs.eof then
    %>
    <div class="ffl" style="padding-top:20px; border-top:1px solid #ccc;">
	    <div class="ffl" style="font-size:20px;"> &nbsp; Customers Who Have Your Phone Model Also Bought</div>
		<div id="slides2" class="ffl" align="center" style="padding-top:15px; position:relative;">
            <div class="fl clickable btn-arrow-left" style="position:absolute; top:100px; left:30px;"></div>
            <div class="fl clickable btn-arrow-right" style="position:absolute; top:100px; right:30px;"></div>
            <div class="fl slides_container" style="display:none; width:750px; margin-left:120px;">
                <div class="fl slide" style="width:750px; height:240px;">
                <%
                nLap = 0
                a = 0
				do until rs.eof
					nLap = nLap + 1
					a = a + 1
					tItemid = rs("itemid")
					altText = rs("itemdesc")
					itemdesc = replace(rs("itemDesc"), "/", " / ")
					if len(itemdesc) > 53 then itemdesc = left(itemdesc, 50) & "..."
					price_our = formatcurrency(rs("price_our"),2)
					price_retail = formatcurrency(rs("price_retail"),2)
					productLink = "/p-" & tItemid & "-" & formatSEO(altText) & ".asp"
					itempic = rs("itempic")
					reviewCount = prepInt(rs("reviewCount"))
					avgReview = prepInt(rs("avgReview"))
				%>
                    <div class="fl item" style="text-align:center; width:160px; padding:10px;">
                         <div class="ffl" style="padding-top:2px; text-align:center;">
                            <a href="<%=productLink%>" title="<%=altText%>"><img src="/productpics/thumb/<%=itemPic%>" width="100" height="100" border="0" alt="<%=altText%>" title="<%=altText%>" /></a>
                        </div>
                        <div class="ffl" style="padding:5px 0px 0px 0px; height:50px; text-align:left;"><a href="<%=productLink%>" title="<%=altText%>" style="font-size:13px; color:#333; font-weight:bold;"><%=itemdesc%></a></div>
                        <div class="ffl" style="padding:5px 0px 0px 0px;">
                        	<div class="fl" style="color:#777; text-decoration:line-through; padding-right:10px;"><%=price_retail%></div>
                        	<div class="fl" style="color:#ff6600; font-weight:bold;"><%=price_our%></div>
                        </div>
                        <div class="ffl" style="padding:5px 0px 0px 0px;">
                        	<div class="fl" style="color:#777; padding-right:10px;">FREE SHIPPING</div>
                        	<div class="fl"><a href="<%=productLink%>" title="<%=altText%>" style="font-size:11px; color:#336699">See Details</a></div>
                        </div>
                        <div class="ffl" style="padding:5px 0px 0px 0px;">
                        	<div class="fl" style="padding-right:10px;"><%=getRatingStar(avgReview)%></div>
                        	<div class="fl">(<span style="color:#336699;"><%=reviewCount%></span>)</div>
                        </div>
                    </div>
                <%
					if ((a mod 4) = 0) and (a < 20) then
						response.write "</div>" & vbcrlf & "<div class=""fl slide"" style=""width:750px; height:240px;"">"
					end if						
					rs.movenext
				loop
                if nLap > 4 then arrPagination(1) = true
                %>
                </div>
            </div>        
        </div>        
    </div>
    <%end if%>
	<div class="ffl" style="margin-top:20px; padding-top:20px; border-top:1px solid #ccc;">
	    <div class="ffl" style="font-size:20px;"> &nbsp; Your Recently Viewed Products</div>
        <%
		sql = 	"select	top 20 c.rowid, a.itemid, a.itemdesc, a.itempic, a.price_our, a.price_retail" & vbcrlf & _
				"	,	(	select	count(*)" & vbcrlf & _
				"			from	we_reviews" & vbcrlf & _
				"			where	approved = 1 and partnumbers in ( select partnumbers from we_reviews where itemid = a.itemid)) reviewCount" & vbcrlf & _
				"	,	isnull((	select	avg(rating) " & vbcrlf & _
				"					from	we_reviews" & vbcrlf & _
				"					where	approved = 1 and partnumbers in ( select partnumbers from co_reviews where itemid = a.itemid)), 0) avgReview" & vbcrlf & _
				"from	we_items a join [dbo].[tfn_StringToColumn]('" & strRecentViews & "', ',') c" & vbcrlf & _
				"	on	a.itemid = c.rString " & vbcrlf & _
				"group by c.rowid, a.itemid, a.itemdesc, a.itempic, a.price_our, a.price_retail" & vbcrlf & _
				"order by c.rowid"
		%>
		<div id="slides3" class="ffl" align="center" style="padding-top:15px; position:relative;">
            <div class="fl clickable btn-arrow-left" style="position:absolute; top:100px; left:30px;"></div>
            <div class="fl clickable btn-arrow-right" style="position:absolute; top:100px; right:30px;"></div>
            <div class="fl slides_container" style="display:none; width:750px; margin-left:120px;">
                <div class="fl slide" style="width:750px; height:240px;">
                <%
				set rs = oConn.execute(sql)
                nLap = 0
                a = 0
				do until rs.eof
					nLap = nLap + 1
					a = a + 1
					tItemid = rs("itemid")
					altText = rs("itemdesc")
					itemdesc = replace(rs("itemDesc"), "/", " / ")
					if len(itemdesc) > 53 then itemdesc = left(itemdesc, 50) & "..."
					price_our = formatcurrency(rs("price_our"),2)
					price_retail = formatcurrency(rs("price_retail"),2)
					productLink = "/p-" & tItemid & "-" & formatSEO(altText) & ".asp"
					itempic = rs("itempic")
					reviewCount = prepInt(rs("reviewCount"))
					avgReview = prepInt(rs("avgReview"))
				%>
                    <div class="fl item" style="text-align:center; width:160px; padding:10px;">
                         <div class="ffl" style="padding-top:2px; text-align:center;">
                            <a href="<%=productLink%>" title="<%=altText%>"><img src="/productpics/thumb/<%=itemPic%>" width="100" height="100" border="0" alt="<%=altText%>" title="<%=altText%>" /></a>
                        </div>
                        <div class="ffl" style="padding:5px 0px 0px 0px; height:50px; text-align:left;"><a href="<%=productLink%>" title="<%=altText%>" style="font-size:13px; color:#333; font-weight:bold;"><%=itemdesc%></a></div>
                        <div class="ffl" style="padding:5px 0px 0px 0px;">
                        	<div class="fl" style="color:#777; text-decoration:line-through; padding-right:10px;"><%=price_retail%></div>
                        	<div class="fl" style="color:#ff6600; font-weight:bold;"><%=price_our%></div>
                        </div>
                        <div class="ffl" style="padding:5px 0px 0px 0px;">
                        	<div class="fl" style="color:#777; padding-right:10px;">FREE SHIPPING</div>
                        	<div class="fl"><a href="<%=productLink%>" title="<%=altText%>" style="font-size:11px; color:#336699">See Details</a></div>
                        </div>
                        <div class="ffl" style="padding:5px 0px 0px 0px;">
                        	<div class="fl" style="padding-right:10px;"><%=getRatingStar(avgReview)%></div>
                        	<div class="fl">(<span style="color:#336699;"><%=reviewCount%></span>)</div>
                        </div>
                    </div>
                <%
					if ((a mod 4) = 0) and (a < 20) then
						response.write "</div>" & vbcrlf & "<div class=""fl slide"" style=""width:750px; height:240px;"">"
					end if						
					rs.movenext
				loop
                if nLap > 4 then arrPagination(1) = true
                %>
                </div>
            </div>        
        </div>        
    </div>
	<% if not isDropship(partnumber) then %>
	<div class="ffl" style="margin-top:20px; padding-top:20px; border-top:1px solid #ccc;">
	    <div class="ffl" style="font-size:20px;"> &nbsp; Exclusive Partner Offer</div>
        <div class="fl" style="padding-left:350px;"><div name="sr_productDetailDiv"></div></div>
	</div>
	<% end if %>
	<div class="ffl" style="margin-top:20px; padding-top:20px; border-top:1px solid #ccc;">
	    <div class="ffl" style="font-size:20px;"> &nbsp; Questions Or Comments? </div>
	</div>
</div>
<script>
	function onTab(idx) {
		for (i=1; i<=5; i++) {
			header = document.getElementById('tabHeader'+i);
			content = document.getElementById('tab'+i);
			if ((header != null)&&(content != null)) {
				header.className = 'off';
				content.style.display = 'none';
			}
		}

		header = document.getElementById('tabHeader'+idx);
		content = document.getElementById('tab'+idx);
		if ((header != null)&&(content != null)) {
			header.className = 'on';
			content.style.display = '';
		}
	}
</script>