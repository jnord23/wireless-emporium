﻿<%
	if isnull(noAsp) or noAsp = "" then noAsp = True
	if noAsp then
		if instr(request.ServerVariables("HTTP_X_REWRITE_URL"),".asp") > 0 and instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"_mp.asp") < 1 then
			response.Status = "410 Gone"
			response.End()
		end if
	end if
	
	saveEmail()
	
	topPageName = "index"
	
	siteId = 0
'	cmsData(request.ServerVariables("HTTP_X_REWRITE_URL"))
'	metaArray = split(session("metaTags"),"##")
'	SEtitle = metaArray(0)
'	SEdescription = metaArray(1)
'	SEkeywords = metaArray(2)
'	SeTopText = metaArray(3)
'	SeBottomText = metaArray(4)
'	SeH1 = metaArray(5)	

	curPage = prepStr(request.QueryString("curPage"))
'	response.write "request.QueryString(""curPage""):" & request.QueryString("curPage") & "<br>"
'	response.write "request.ServerVariables(""HTTP_X_REWRITE_URL""):" & request.ServerVariables("HTTP_X_REWRITE_URL") & "<br>"
	if curPage <> "" then useURL = curPage else useURL = request.ServerVariables("HTTP_X_REWRITE_URL") end if
'	response.write "useURL:" & useURL & "<br>"
	
	metaString = ""
	call getCmsData(0,cms_basepage,useURL,metaString)
	if instr(metaString, "!##!") > 0 then
		metaArray = split(metaString,"!##!")
		SEtitle = metaArray(0)
		SEdescription = metaArray(1)
		SEkeywords = metaArray(2)
		SeTopText = metaArray(3)
		SeBottomText = metaArray(4)
		SeH1 = metaArray(5)	
	end if
	
	if request.ServerVariables("HTTPS") = "on" then response.redirect("http://" & request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL"))	
	
	canonicalURL = "http://www.wirelessemporium.com"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%=SEtitle%></title>
	<meta name="Description" content="<%=SEdescription%>" />
	<meta name="Keywords" content="<%=SEkeywords%>" />
	<meta name="robots" content="index,follow" />
    <% if prepStr(request.QueryString("curPage")) <> "" then %>
    <meta name="alternate" content="http://m.wirelessemporium.com<%=prepStr(request.QueryString("curPage")) & ".htm"%>">
	<% else %>
    <meta name="alternate" content="http://m.wirelessemporium.com<%=request.ServerVariables("HTTP_X_REWRITE_URL") & ".htm"%>">
    <% end if %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta property="twitter:account_id" content="40093639" />
	<link rel="stylesheet" type="text/css" href="/includes/css/base.css<%=arrCssDateModified(0)%>" />
    <link rel="stylesheet" type="text/css" href="/includes/css/index.css<%=arrCssDateModified(1)%>" />
	<!--#include virtual="/includes/asp/inc_HeadTagEnd.asp"-->
</head>
<!--#include virtual="/includes/asp/inc_BodyTagStart.asp"-->
<!--#include virtual="/includes/template/topHTML.asp"-->