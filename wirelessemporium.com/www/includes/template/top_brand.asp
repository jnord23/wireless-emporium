<%
	if isnull(noAsp) or noAsp = "" then noAsp = True
	if noAsp then
		if instr(request.ServerVariables("HTTP_X_REWRITE_URL"),".asp") > 0 and instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"_mp.asp") < 1 then
			response.Status = "410 Gone"
			response.End()
		end if
	end if
	
	saveEmail()
	
	topPageName = "brand"
	
	if request.ServerVariables("HTTPS") = "on" then response.redirect("http://" & request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL"))
	
	function drawStitchModelBoxByArray(pArr, pStitchImg)
		timestamp = getFileLastModified(pStitchImg)
		if timestamp <> "" then pStitchImg = pStitchImg & "?v=" & timestamp
	%>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
            <%
            useX = 0 : lap = 0 : a = 0
			useCarrierLogo = ",1301,1259,1258,"
            if not isnull(pArr) then
                for nRows=0 to ubound(pArr,2)
                    a = a + 1
                    lap = lap + 1
                    modelID = cint(pArr(0,nRows))
                    modelName = pArr(1,nRows)
					modelName = replace(modelName,"/"," / ")
					dbBrandName = pArr(6,nRows)
                    modelImg = pArr(2,nRows)
					linkName = pArr(10,nRows)
					
'					if isnull(linkName) then
						useLink = formatSEO(dbBrandName) & "-" & formatSEO(replace(modelName,"+","plussign"))
'					else
'						useLink = formatSEO(linkName)
'					end if
                    
                    if brandid = 17 then
                        altTag = modelName & " Accessories"
                    else
                        altTag = dbBrandName & " " & modelName & " Accessories"
                    end if
					useLink = replace(useLink,"other-","")
					useLink = replace(useLink,"ipod-ipad-","apple-")
					useLink = replace(useLink, "t-mobile-sidekick-", "sidekick-")
					
					if brandid = 15 then
						if left(dbBrandName, 5) = "Other" then
	                        useAnchorText = modelName & " Accessories"
						else
	                        useAnchorText = dbBrandName & " " & modelName & " Accessories"
						end if
                    else
                        useAnchorText = modelName & " Accessories"
                    end if					
	            %>
                <td width="20%" valign="top" style="border-bottom:1px solid #CCC;<% if a < 5 then %> border-right:1px solid #CCC;<% end if %> padding:10px 5px 5px 5px;">
                    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                        <tr>
                            <td align="center" valign="top">
                                 <a href="/<%=formatSEO(useLink & "-accessories")%>" title="<%=altTag%>">
                                	<div style="position:relative; width: 70px; height: 112px; background: url(<%=pStitchImg%>) <%=useX%>px 0px no-repeat; cursor:pointer;" title="<%=altTag%>">
									<%if instr(useCarrierLogo, "," & modelid & ",") > 0 then%>
                                    	<div style="position:absolute; bottom:0px; left:-25px;"><img src="/images/brands/b_carrierLogo_<%=modelid%>.png" border="0" /></div>
                                    <%end if%>
									</div>
								</a>
                                <br />
                                <a class="cellphone2-link" href="/<%=formatSEO(useLink & "-accessories")%>" title="<%=altTag%>"><%=useAnchorText%></a>
                            </td>
                        </tr>
                    </table>
                </td>
    	        <%
                    if a = 5 then
                        response.write "</tr><tr>" & vbcrlf
                        a = 0
                    end if
                    useX = useX - 70
                next					
            end if
            %>
            </tr>
        </table>    
    <%
	end function
	
	curPage = prepStr(request.QueryString("curPage"))
	if curPage <> "" then useURL = curPage else useURL = request.ServerVariables("HTTP_X_REWRITE_URL") end if
	
	siteId = 0
'	cmsData(useURL)
'	metaArray = split(session("metaTags"),"##")
'	SEtitle = metaArray(0)
'	SEdescription = metaArray(1)
'	SEkeywords = metaArray(2)
'	SeTopText = metaArray(3)
'	SeBottomText = metaArray(4)
'	SeH1 = metaArray(5)	

	metaString = ""
	call getCmsData(0,cms_basepage,useURL,metaString)
	if instr(metaString, "!##!") > 0 then
		metaArray = split(metaString,"!##!")
		SEtitle = metaArray(0)
		SEdescription = metaArray(1)
		SEkeywords = metaArray(2)
		SeTopText = metaArray(3)
		SeBottomText = metaArray(4)
		SeH1 = metaArray(5)	
	end if
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%=SEtitle%></title>
	<meta name="Description" content="<%=SEdescription%>" />
	<meta name="Keywords" content="<%=SEkeywords%>" />
	<meta name="robots" content="index,follow" />
    <% if prepStr(request.QueryString("curPage")) <> "" then %>
    <meta name="alternate" content="http://m.wirelessemporium.com<%=prepStr(request.QueryString("curPage")) & ".htm"%>">
	<% else %>
    <meta name="alternate" content="http://m.wirelessemporium.com<%=request.ServerVariables("HTTP_X_REWRITE_URL") & ".htm"%>">
    <% end if %>
	<!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
	<link rel="stylesheet" type="text/css" href="/includes/css/base.css<%=arrCssDateModified(0)%>" />
	<!--#include virtual="/includes/asp/inc_HeadTagEnd.asp"-->    
</head>
<!--#include virtual="/includes/asp/inc_BodyTagStart.asp"-->
	<!--#include virtual="/includes/template/topHTML.asp"-->