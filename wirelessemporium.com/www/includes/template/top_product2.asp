<%
pageStart = time
on error resume next
if request.querystring("id") <> "" then
	Response.Cookies("vendororder") = request.querystring("id")
	Response.Cookies("vendororder").Expires = date + 45
elseif request.querystring("utm_source") <> "" then
	Response.Cookies("vendororder") = request.querystring("utm_source")
	Response.Cookies("vendororder").Expires = date + 45
elseif request.querystring("refer") <> "" then
	Response.Cookies("vendororder") = request.querystring("refer")
	Response.Cookies("vendororder").Expires = date + 45
elseif request.querystring("clickid") <> "" then
	Response.Cookies("vendororder") = "clickid=" & request.querystring("clickid")
	Response.Cookies("vendororder").Expires = date + 45
elseif request.querystring("source") <> "" then
	Response.Cookies("vendororder") = request.querystring("source")
	Response.Cookies("vendororder").Expires = date + 45
end if
on error goto 0

if instr(request.ServerVariables("HTTP_HOST"),"staging") > 0 then
	extraWidgets = "yes"
else
	extraWidgets = "yes"
end if

if isnull(session("adminID")) or len(session("adminID")) < 1 then session("adminID") = 0
if isnull(session("adminLvl")) or len(session("adminLvl")) < 1 then session("adminLvl") = 0

call fOpenConn()
userBrowser = request.ServerVariables("HTTP_USER_AGENT")
mobileAccess = request.QueryString("mobileAccess")
if isnull(mobileAccess) or len(mobileAccess) < 1 then mobileAccess = ""

if isnull(session("mobileAccess")) or len(session("mobileAccess")) < 1 then
	if (instr(userBrowser,"Mobile") > 0 or instr(userBrowser,"BlackBerry") > 0) and instr(userBrowser,"iPad") < 1 then
		if mobileAccess = "" then
			additionalQS = mid(request.ServerVariables("HTTP_X_REWRITE_URL"),instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"?") + 1)
			if len(additionalQS) > 0 then
				response.Redirect("http://m.wirelessemporium.com?i=" & itemID & "&" & additionalQS)
			else
				response.Redirect("http://m.wirelessemporium.com?i=" & itemID)
			end if
		else
			session("mobileAccess") = 1
		end if
	end if
end if

dim RandNum
function stripCode(myCode)
	stripCode = replace(replace(replace(replace(replace(myCode,"'",""),"<",""),">",""),"(",""),")","")
end function

if SEtitle = "" then SEtitle = "Buy Cell Phone Accessories from WirelessEmporium � Cell phone Covers, Faceplates, Charms, Chargers, Batteries - Deep Discounts and Free Shipping"
if SEdescription = "" then SEdescription = "The #1 Name in Cell Phone Accessories Online - Wireless Emporium. Discount Cell Phone Accessories at Prices up to 75% Off. Buy Wholesale Cellular Accessories, discount wireless accessories and cheap cell phone accessories."
if SEkeywords = "" then SEkeywords = "cheap cell phone accessories,cell phone accessories,discount cellular accessories,mobile phone accessories,wireless accessories,cellular telephone accessories,wholesale cellular accessories,cellular accessories,wireless phone accessories,wholesale cell phone accessories,cellular phone accessories,wholesale wireless accessories,wholesale cellular phone accessories,discount cell phone accessories,cool cell phone accessories"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=SEtitle%></title>
<meta name="Description" content="<%=SEdescription%>">
<meta name="Keywords" content="<%=SEkeywords%>">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="/includes/css/style.css" type="text/css">
<link rel="stylesheet" href="/includes/css/shadetabs.css" type="text/css">
<script type="text/javascript" src="/includes/js/dropdown.js"></script>
<script type="text/javascript" src="/includes/js/tabcontent.js"></script>
<% if extraWidgets = "yes" then %>
<script src="https://seal.buysafe.com/private/rollover/rollover.js" type="text/javascript" language="javascript" charset="utf-8"></script>
<% end if %>
<script language="javascript" type="text/javascript">
function Open_Popup(theURL,winName,features) {
	window.open(theURL,winName,features);
}
</script>
<script src="http://www.google.com/jsapi"></script>
<script type="text/javascript" charset="utf-8">
google.load('ads.search', '2');
</script>


<script>
function addToCart()	{	doSubmit();					}
function doSubmit()		{	return true;				}
</script>


<script src="https://seal.buysafe.com/private/rollover/rollover.js" type="text/javascript" language="javascript" charset="utf-8"></script>
<!--#include virtual="/includes/template/inc_HeadTagEnd.asp"-->
</head>

<body>
<!-- Google AdSense Start -->
<script type="text/javascript" charset="utf-8">
var pageOptions = {
'pubId' : 'pub-1001075386636876',
<% if len(brandName) > 0 then %>
	<% if len(modelName) > 0 then %>
		<% if len(categoryName) > 0 then %>
'query' : '<%=brandName%>' + ' ' + '<%=modelName%>' + ' ' + '<%=categoryName%>'
		<% else %>
'query' : '<%=brandName%>' + ' ' + '<%=modelName%>'
		<% end if %>
	<% else %>
		<% if len(categoryName) > 0 then %>
'query' : '<%=brandName%>' + ' ' + '<%=categoryName%>'
		<% else %>
'query' : '<%=brandName%>'
		<% end if %>
	<% end if %>
};
<% elseif len(categoryName) > 0 then %>
'query' : '<%=categoryName%>'
};
<% else %>
'query' : 'Cell Phones'
};
<% end if %>
var adblock1 = {
'container' : 'adcontainer1',
'number' : 3,
'width' : 'auto',
'lines' : 2,
'fontFamily' : 'arial',
'fontSizeTitle' : '14px',
'fontSizeDescription' : '14px',
'fontSizeDomainLink' : '14px',
'linkTarget' : '_blank'
};
var adblock2 = {
'container' : 'adcontainer2',
'number' : 4,
'width' : '250px',
'lines' : 3,
'fontFamily' : 'arial',
'fontSizeTitle' : '12px',
'fontSizeDescription' : '12px',
'fontSizeDomainLink' : '12px',
'linkTarget' : '_blank'
};
<%
if isnull(googleAds) or len(googleAds) < 1 then googleAds = 0
if googleAds = 1 then
%>
new google.ads.search.Ads(pageOptions, adblock1);
<% elseif googleAds = 2 then %>
new google.ads.search.Ads(pageOptions, adblock1, adblock2);
<% end if %>
</script>
<!-- Google AdSense Close -->
<!-- ClickTale Top part -->
<script type="text/javascript">
var WRInitTime=(new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->
<% if extraWidgets = "yes" then %>
<!-- TurnTo Code Start -->
<span id="TurnToSetup"></span>
<script type="text/javascript">TurnTo.setup();</script>
<span class="TurnToItemTeaser"></span>
<script type="text/javascript">
var TurnToItemSku = "<%=itemID%>";
document.write(unescape("%3Cscript src='" + document.location.protocol + "//" + turnToConfig.staticHost + "/traServer2/itemjs/" + turnToConfig.siteKey + "/" + TurnToItemSku + "' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">TurnTo.insertItemTeaser();</script>
<!-- TurnTo Code End -->
<% end if %>
<table width="1000" border="1" cellspacing="0" cellpadding="0" align="center">
	<% if not isnull(session("adminID")) and len(session("adminID")) > 0 then %>
    	<% if session("adminNav") then %>
    <tr bgcolor="#FF0000"><td style="font-weight:bold; color:#FFF; font-size:12px; padding:5px;">Admin Options: <a href="/admin/menu.asp" style="color:#FFF;">Admin Menu</a><%=otherAdminOptions%></td></tr>
    	<% end if %>
	<% end if %>
	<tr>
		<td width="100%" valign="top" align="left">
			<table width="100%" border="1" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="left">
						<table width="100%" border="1" cellspacing="0" cellpadding="0">
							<tr>
								<td width="455" align="left" class="top-link" valign="middle">
									<%
									if month(date) = 12 and day(date) < 27 then
										holidayLogo = 1
									elseif (month(date) = 12 and day(date) > 26) or (month(date) = 1 and day(date) < 7) then
										holidayLogo = 2
									else
										holidayLogo = 0
									end if
									if holidayLogo = 0 then
									%>
                                    <a href="/"><img src="/images/WE_logo.jpg" width="450" height="69" border="1" align="absbottom" alt="Cell Phone Accessories &#8211; Wireless Emporium"></a>
                                    <% elseif holidayLogo = 1 then %>
                                    <a href="/"><img src="/images/we_xmas_logo.jpg" width="475" height="69" border="1" align="absbottom" alt="Cell Phone Accessories &#8211; Wireless Emporium"></a>
                                    <% elseif holidayLogo = 2 then %>
                                    <a href="/"><img src="/images/we_ny_logo.jpg" border="1" align="absbottom" alt="Cell Phone Accessories &#8211; Wireless Emporium"></a>
                                    <% end if %>
								</td>
								<td width="545" align="right" valign="top">
									<%=minicart%>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="100%" height="34" valign="middle" align="center" style="background-image: url('/images/bg_cat_header2.jpg'); background-repeat: repeat; background-position: center bottom;">
						<table width="100%" border="1" cellspacing="0" cellpadding="0">
							<tr>
								<td width="126" align="center" valign="middle"><a href="/unlocked-cell-phones.asp" class="topnav" title="Unlocked Cell Phones">Unlocked&nbsp;Phones</a></td>
								<td width="7" align="center" valign="middle"><img src="/images/dot_white.jpg" width="7" height="7" border="1"></td>
								<td width="158" align="center" valign="middle"><a href="/cell-phone-covers-faceplates-screen-protectors.asp" class="topnav" title="Cell Phone Faceplates - Protectors">Faceplates/Protectors</a></td>
								<td width="7" align="center" valign="middle"><img src="/images/dot_white.jpg" width="7" height="7" border="1"></td>
								<td width="70" align="center" valign="middle"><a href="/cell-phone-batteries.asp" class="topnav" title="Cell Phone Batteries">Batteries</a></td>
								<td width="7" align="center" valign="middle"><img src="/images/dot_white.jpg" width="7" height="7" border="1"></td>
								<td width="70" align="center" valign="middle"><a href="/cell-phone-chargers.asp" class="topnav" title="Cell Phone Chargers">Chargers</a></td>
								<td width="7" align="center" valign="middle"><img src="/images/dot_white.jpg" width="7" height="7" border="1"></td>
								<td width="50" align="center" valign="middle"><a href="/cell-phone-cases.asp" class="topnav" title="Cell Phone Cases">Cases</a></td>
								<td width="7" align="center" valign="middle"><img src="/images/dot_white.jpg" width="7" height="7" border="1"></td>
								<td width="72" align="center" valign="middle"><a href="/bluetooth-headsets-handsfree.asp" class="topnav" title="Bluetooth Headset">Headsets</a></td>
								<td width="7" align="center" valign="middle"><img src="/images/dot_white.jpg" width="7" height="7" border="1"></td>
								<td width="122" align="center" valign="middle"><a href="/cell-phone-holsters-belt-clips-holders.asp" class="topnav" title="Cell Phone Holsters - Holders">Holsters/Holders</a></td>
								<td width="7" align="center" valign="middle"><img src="/images/dot_white.jpg" width="7" height="7" border="1"></td>
								<td width="90" align="center" valign="middle"><a href="/cell-phone-data-cables-memory-cards.asp" class="topnav" title="Cell Phone Data Cables">Data&nbsp;Cables</a></td>
								<td width="7" align="center" valign="middle"><img src="/images/dot_white.jpg" width="7" height="7" border="1"></td>
								<td width="114" align="center" valign="middle"><a href="/cell-phone-antennas.asp" class="topnav" title="Cell Phone Antennas - Parts">Antennas/Parts</a></td>
								<td width="7" align="center" valign="middle"><img src="/images/dot_white.jpg" width="7" height="7" border="1"></td>
								<td width="65" align="center" valign="middle"><a href="/cell-phone-charms-bling-kits.asp" class="topnav" title="Bling Kits - Cell Phone Charms">Charms</a></td>
							</tr>
						</table>
					</td>
				</tr>
				<form name="Mainsearch" method="get" action="/search/search.asp" id="nxt-ac-form">
				<tr>
					<td width="100%" height="35" valign="middle" align="left" style="background-image: url('/images/bg_search2.jpg'); background-repeat: repeat; background-position: center top;">
						<table width="100%" border="1" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td width="400" valign="middle" align="left" class="search">
									<input type="hidden" name="from" id="Hidden1">
									<input type="hidden" name="FrmSearchWords" id="FrmSearchWords">
									&nbsp;&nbsp;<input type="text" id="nxt-ac-searchbox" autocomplete="off" name="keywords" value="Find Your Product" onFocus="this.value='';" class="nicetextbox" style="color: #999999;" size="40" maxlength="60">&nbsp;&nbsp;<input type="image" src="/images/search_button2.jpg" align="absmiddle" width="30" height="19" border="1" alt="GO">
								</td>
								<td width="600" valign="middle" align="right">
									<%=tollfree%>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" valign="top" width="100%" height="10"><img src="/images/spacer.gif" width="10" height="10" border="1"></td>
	</tr>
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="0" width="1000">
				<tr>
