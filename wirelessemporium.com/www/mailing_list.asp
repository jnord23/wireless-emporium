<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <%
    if request.form("submitted") = "1" then
        dim cdo_to, cdo_from, cdo_subject, cdo_body
        cdo_to = request.form("email")
        cdo_from = "sales@WirelessEmporium.com"
        cdo_subject = "Thank you for signing up with WirelessEmporium.com!"
        if IsValidEmail(cdo_to) = true then
            'call fOpenConn()
            'email = SQLquote(request.form("email"))
            'SQL = "DELETE FROM we_emailopt WHERE email='" & email & "'"
            ''response.write "<p>" & SQL & "</p>" & vbcrlf
            'oConn.execute SQL
            'SQL = "INSERT INTO we_emailopt_out (email,DateTimeEntd) VALUES ('" & email & "','" & now & "')"
            ''response.write "<p>" & SQL & "</p>" & vbcrlf
            'oConn.execute SQL
            'call fCloseConn()
            
            cdo_body = "<p><img src=""http://www.wirelessemporium.com/images/WElogo.gif"">" & "</p>" & vbcrlf
            cdo_body = cdo_body & "<p><b>Name: </b>" & request.form("name") & "</p>" & vbcrlf
            cdo_body = cdo_body & "<p><a href=""http://www.wirelessemporium.com/mailing_list_add.asp"">Click here</a> to confirm your registration.</p>" & vbcrlf
            cdo_body = cdo_body & "<p><b>Date/Time Sent: </b>" & now & "</p>" & vbcrlf
            cdo_body = cdo_body & "<p>&nbsp;</p>" & vbcrlf
            CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
            %>
            <p><b><br><br>Thank you for signing up with WirelessEmporium.com!</b></p>
            <p><b>Your privacy is important to us. You will soon be receiving an e-mail with a link to click on to verify that you wish to join our mailing list.</b></p>
            <p><b><br><br>WirelessEmporium.com</b></p>
            <%
        else
            %>
            <p><b><br><br>The e-mail address you entered was not valid.</b></p>
            <p><b>Please <a href="javascript:history.back();">go back</a> and try again.</b></p>
            <p><b><br><br>WirelessEmporium.com</b></p>
            <%
        end if
    else
        %>
        <script language="javascript1.2">
        function reset() {
            document.contactus.name.value="";
            document.contactus.email.value="";
            document.contactus.subject.value="";
            document.contactus.phonenumber.value="";
            document.contactus.product.value="";
            document.contactus.orderno.value="";
            document.contactus.question.value="";
        }
        function validateEmail(email) {
            var splitted = email.match("^(.+)@(.+)$");
            if (splitted == null) return false;
            if (splitted[1] != null) {
                var regexp_user=/^\"?[\w-_\.]*\"?$/;
                if (splitted[1].match(regexp_user) == null) return false;
            }
            if (splitted[2] != null) {
                var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
                if (splitted[2].match(regexp_domain) == null) {
                    var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
                    if (splitted[2].match(regexp_ip) == null) return false;
                }
                return true;
            }
            return false;
        }
        function validate() {
            if (document.contactus.name.value.length==0) {
                alert("Please Enter Your Name");
                return false;
            }
            if (document.contactus.subject.value=="Select here...") {
                alert("Please Select the Type of Inquiry");
                return false;
            }
            if (document.contactus.question.value.length==0) {
                alert("Please Enter Question");
                return false;
            }
            if (document.contactus.email.value.length==0) {
                alert("Please Enter Email Address");
                return false;
            }
            if (!validateEmail(document.contactus.email.value)) { 
                alert("Please Enter a Valid Email Address");
                return false;
            }
            document.contactus.submit()
            return false;
        }
        </script>
        <tr>
            <td colspan="2" align="center"><h1>Subscribe to Wireless Emporium E-Mails</h1></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <form action="mailing_list.asp" method="post">
                    <p><input type="text" name="email" size="50" value="<%=request.querystring("strEmail")%>"></p>
                    <p><input type="submit" name="submitted" value="Subscribe Now!"></p>
                </form>
            </td>
        </tr>
        <%
    end if
    %>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->