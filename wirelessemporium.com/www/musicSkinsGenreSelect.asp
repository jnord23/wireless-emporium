<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
	response.buffer = true

	brandID = request.querystring("brandID")
	modelID = request.querystring("modelID")
	categoryID = request.querystring("categoryID")
	design = request.querystring("design")
	musicSkins = request.querystring("musicSkins")

	if isnull(brandID) or len(brandID) < 1 then brandID = 0 end if 
	if isnull(modelID) or len(modelID) < 1 then modelID = 0 end if
	if isnull(categoryID) or len(categoryID) < 1 then categoryID = 0 end if 
	if isnull(design) or len(design) < 1 then design = 0 end if
	if isnull(musicSkins) or len(musicSkins) < 1 then musicSkins = 0 end if

	call fOpenConn()
	sql = "select distinct a.genre, a.brand, b.modelName, b.modelImg from we_items_musicSkins a join we_models b on a.modelID = b.modelID where a.skip = 0 and a.deleteItem = 0 and b.modelID = " & modelID & " and a.genre <> 'Screen Protectors' order by a.genre"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	noSkins = 0
	if rs.EOF then
		sql = "select a.brandName as brand, b.modelName, b.modelImg from we_models b join we_brands a on b.brandID = a.brandID where b.modelID = '" & modelID & "'"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
		
		response.Status = "301 Moved Permanently"
		response.AddHeader "Location", "http://www.wirelessemporium.com"
		response.end
	end if
	
	modelName = RS("modelName")
	modelImg = RS("modelImg")
	brandName = RS("brand")
	categoryName = "Music Skins"
	
	'=========================================================================================
	Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
		oParam.CompareMode = vbTextCompare
		oParam.Add "x_brandID", brandID
		oParam.Add "x_modelID", modelID
		oParam.Add "x_categoryID", categoryID
		oParam.Add "x_brandName", brandName
		oParam.Add "x_modelName", modelName
		oParam.Add "x_categoryName", "Music Skins"	
	call redirectURL("bmc-music-genre", "", request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
	'=========================================================================================
	
	SEtitle 		= "Premium " & brandName & " " & modelName & " Music Skins | Wireless Emporium"
	SEdescription 	= "Shop for Premium " & brandName & " " & modelName & " Music Skins with Music, TV, Movie and all other themes at factory-direct prices at Wireless Emporium."
	SEkeywords 		= brandName & " " & modelName & " music skins, " & modelName & " music skins, " & modelName & " skins, " & brandName & " skins, cell phone skins, phone skins"
	h1				= brandName & " " & modelName & " Music Skins"	
%>
<!--#include virtual="/includes/template/top.asp"-->
<%
	topText = "Go bold or go sleek with your " & brandName & " " & modelName & " by choosing from hundreds of styles of <a class=""topText-link"" href=""http://www.wirelessemporium.com/sc-20-sb-" & brandID & "-" & formatSEO(RS("brand")) & "-" & formatSEO(categoryName) & ".asp"" title=""" & brandName & " " & modelName & " music skins"">" & brandName & " music skins</a> at Wireless Emporium. These " & brandName & " " & modelName & " music skins are easy to assemble and are as simple as a sticker to install."
	topText = topText & "Because we offer our cell phone faceplates and screen protectors at discount prices, you can buy multiple " & brandName & " " & modelName & " faceplates and change them as often as your mood! Choose the color or design that best suits your personality from our wide selection of " & brandName & " " & modelName & " <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a> and cell phone faceplates below and protect your phone with style! " & brandName & " " & modelName & " faceplates and covers are an essential addition to your mobile device, as it is one of the most effective ways to both protect the look of your phone and enhance it, all while retaining total functionality."
	bottomText = "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">Since 2001, Wireless Emporium has been offering high-quality cell <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Discount Phone Accessories"">discount phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best cell phone faceplates and covers for " & brandName & " " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
	
	dim modelLink, phoneOnly
	if modelID = 940 then
		modelLink = "/ipod-ipad-accessories.asp"
	else
		modelLink = "/T-" & modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
	end if
	
	if instr(modelName, "iPhone") > 0 then
		phoneOnly = 1
	else
		phoneOnly = 0
	end if
	
	strBreadcrumb = brandName & " " & modelName & " " & nameSEO(categoryName)
	
	if brandid = 17 and phoneOnly = 1 then
		brandCrumb 	= "<a class=""breadcrumb"" href=""/iphone-accessories.asp"">Apple iPhone Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " Accessories</a>"	
	elseif brandid 	= 17 and phoneOnly = 0 then
		topText = replace(replace(lcase(topText), "apple cell phone", brandName & " " & modelName), "cell phone", "")
		SEtitle = replace(replace(lcase(SEtitle), "apple cell phone", brandName & " " & modelName), "cell phone", "")
		SEdescription = replace(replace(lcase(SEdescription), "apple cell phone", brandName & " " & modelName), "cell phone", "")
		SEkeywords = replace(replace(lcase(SEkeywords), "apple cell phone", brandName & " " & modelName), "cell phone", "")
	
		brandCrumb 	= "<a class=""breadcrumb"" href=""/ipod-ipad-accessories.asp"">Apple iPod/iPad Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " Accessories</a>"
	else
		brandCrumb 	= "<a class=""breadcrumb"" href=""/" & brandSEO(brandID) & """>" & brandName & " Cell Phone Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " Cell Phone Accessories</a>"	
	end if
	
	if h1 <> "" then
		strH1 = h1
	elseif strH1 = "" then
		strH1 = brandName & " " & modelName & " " & nameSEO(categoryName)
	end if
	
%>
<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <%
            if (categoryid = "8" or categoryid = "14") and modelid = "0" then
                %>
                <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;Cell Phone <%=nameSEO(categoryName)%>
                <%
            else
                %>
                <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<%=brandCrumb%>&nbsp;&gt;&nbsp;<%=modelCrumb%>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
                <%
            end if
            %>
        </td>
    </tr>
    <tr>
        <td width="100%" align="center" valign="top">
            <img src="/images/brandmodelheaders/header_<%=brandID%>.jpg" width="748" height="30" border="0" alt="<%=brandName & " " & modelName%> Accessories">
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" valign="top">
            <table width="748" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="150" height="100" align="center" valign="middle">
						<img src="/productpics/models/<%=modelImg%>" width="80" border="0" alt="<%=brandName & " " & modelName & " " & nameSEO(categoryName)%>">
                    </td>
                    <td width="5" align="center" valign="middle">
                        <img src="/images/spacer.gif" width="1" height="5" border="0">
                    </td>
                    <td width="595" align="left" valign="top">
                        <h1 class="brandModelCat"><%=strH1%></h1>
                        <p class="topText"><%=topText%></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="padding-top:40px;">
            <%
            if noSkins = 1 then
            %>
            <span style="font-size:16px; font-weight:bold; color:#F00;">No Music Skins Available For This Phone Model</span>
            <%
            else
                curGenres = ""
                do while not rs.EOF
                    if instr(rs("genre"),",") > 0 then
                        genreArray = split(rs("genre"),",")
                        for i = 0 to ubound(genreArray)
                            if curGenres = "" then
                                curGenres = genreArray(i)
                            else
                                if instr(trim(curGenres),trim(genreArray(i))) < 1 then curGenres = curGenres & "," & genreArray(i)
                            end if
                        next
                    else
                        if curGenres = "" then
                            curGenres = rs("genre")
                        else
                            if instr(trim(curGenres),trim(rs("genre"))) < 1 then curGenres = curGenres & "," & rs("genre")
                        end if
                    end if
                    rs.movenext
                loop
                genreArray = split(curGenres,",")
                session("curGenres") = curGenres
            %>
            <table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td colspan="5" align="left" style="padding-bottom:10px;">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-weight:bold; font-size:16px; padding-right:20px;" nowrap="nowrap">Select Music Skins Genre:</td>
                                <td>
                                    <select name="musicGenre" onchange="bmc(this.value)">
                                        <option value="">Select Genre</option>
                                        <% for i = 0 to ubound(genreArray) %>
                                        <option value="<%=formatSEO(trim(genreArray(i)))%>"><%=trim(genreArray(i))%></option>
                                        <% next %>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%
                totalGenre = 0
                do while totalGenre <= ubound(genreArray)
                %>
                <tr>
                    <%
                    for i = 1 to 3
                        if totalGenre > ubound(genreArray) then
                    %>
                    <td style="padding-bottom:10px;">&nbsp;</td>
                    <%
                        else
                            imgName = replace(genreArray(totalGenre),"&","")
                            imgName = replace(imgName,"/Movies","")
                    %>
                    <td align="center" style="padding-bottom:10px;">
                        <a href="/sb-<%=brandID%>-sm-<%=modelID%>-scd-<%=categoryID%>-music-skins-<%=formatSEO(trim(genreArray(totalGenre)))%>.asp"><img src="/images/buttons/ms__<%=trim(imgName)%>.jpg" border="0" width="240" /></a><br />
                        <a href="/sb-<%=brandID%>-sm-<%=modelID%>-scd-<%=categoryID%>-music-skins-<%=formatSEO(trim(genreArray(totalGenre)))%>.asp" style="font-weight:bold; font-size:16px;"><%=genreArray(totalGenre)%></a>
                    </td>
                    <%
                        end if
                        totalGenre = totalGenre + 1							
                    next
                    %>
                </tr>
                <%
                loop
                %>
            </table>
            <%
            end if
            %>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->
<script language="javascript">
	function bmc(val) 
	{
		var useVal = val.replace(" ","-");
		
		if ("" != useVal) window.location = '/sb-<%=brandID%>-sm-<%=modelID%>-scd-<%=categoryID%>-music-skins-' + useVal + '.asp';
	}
</script>