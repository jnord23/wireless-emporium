<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
response.buffer = true
if request.ServerVariables("HTTP_X_REWRITE_URL") = "/index.asp" then PermanentRedirect "/"

dim pageTitle : pageTitle = "Home"
dim SEtitle, SEdescription, SEkeywords
SEtitle = "Cell Phone Accessories Cell Phone Covers Cases Batteries | iPhone BlackBerry Motorola Samsung LG"
SEdescription = "The #1 Name in Cell Phone Accessories Online - Wireless Emporium. Find all cell phone accessories like cases, covers, chargers and batteries for popular brands like iPhone, BlackBerry, Motorola, HTC, Samsung and LG."
SEkeywords = "cell phone accessories, cheap cell phone accessories, cellphone accessories, discount cell phone accessories, mobile phone accessories, mobile accessories, cellular accessories, cell phone covers, cell phone chargers, cell phone cases, cell phone batteries, iphone accessories, iphone cases, iphone covers, iphone batteries, iphone chargers, blackberry accessories, blackberry chargers, blackberry cases, blackberry batteries, Motorola accessories, Motorola chargers, Motorola cases, Motorola covers, LG covers, LG accessories, LG cases, LG batteries, Samsung accessories, Samsung cases, Samsung covers, Samsung batteries"

nItemDescMaxLength = 52
noCommentBox = true

topCats = "Covers &amp; Faceplates##Chargers &amp; Cables##Screen Protectors &amp; Skins##Pouches &amp; Carrying Cases##Batteries##Holsters, Holders &amp; Mounts##Custom Cases##Cell Phones &amp; Tablets"
topCatArray = split(topCats,"##")
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<link href="/includes/css/mvt_searchBar1.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_homeBanner1.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_largeBanners2.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_topCats2.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt_miniBanners3.css" rel="stylesheet" type="text/css">
<table border="0" bordercolor="#FF0000" cellpadding="0" cellspacing="0" width="100%">
    <tr id="newCatButtons">
    	<td>
        	<!--#include virtual="/Framework/seoContent.asp"-->
            <div style="width:748px; height:30px;">
                <div id="topBar-topCategories-left" style="width:38px;"></div>
                <div id="topBar-middle" style="width:700px;">
                	<div style="padding-top:7px;"><h1 style="color:#fff; font-size:14px; font-weight:bold;"><%=SeH1%></h1></div>
                </div>
                <div id="topBar-right"></div>            
            </div>
            <div style="width:726px; height:96px; border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc; background-color:#e4e4e4; padding:10px;">
            	<%
				itemCnt = 0
				for i = 0 to ubound(topCatArray)
					catName = topCatArray(i)
					itemCnt = itemCnt + 1
					if itemCnt < 4 then
						boxSpacing = 1
					else
						boxSpacing = 0
						itemCnt = 0
					end if
					
					if catName = "Cell Phones &amp; Tablets" then
						useLink = "/" & replace(formatSEO(catName),"-amp","")
					elseif catName = "Handsfree Bluetooth" then
						useLink = "/bluetooth-headsets-handsfree"
					elseif catName = "Custom Cases" then
						useLink = "/customized-phone-cases"
					else
						useLink = "/phone-" & replace(formatSEO(catName),"-amp","")
					end if
					useLink = replace(useLink,"cell-phones","phones")
				%>
                <div style="float:left; text-align:center; background:url(/images/buttons/button-cat-background.jpg); width:180px; height:33px; padding-top:15px; font-weight:bold; margin:0px <%=boxSpacing%>px 5px 0px;"><a href="<%=useLink%>" style="color:#000;"><%=catName%></a></div>
                <%
				next
				%>
            </div>
        </td>
    </tr>
    <% if Application("shopRunner") = 1 then %>
    <tr>
    	<td width="100%" style="padding-top:15px;">
        	<div id="mvt_freeReturnMain">
                <span class="normal">Enjoy</span> <span class="boldOrange">Free Same Day Shipping</span> <span class="normal">and</span> <span class="boldOrange">Free 90-Day Return Shipping</span> 
                &nbsp; <a href="javascript:void(0)" onclick="freeReturnShippingPopup(0)">LEARN MORE</a>
                <div id="mvt_btnLearnMore" onclick="freeReturnShippingPopup(0)"></div>
			</div>
		</td>
    </tr>
    <% end if %>
	<tr>
    	<td width="100%" style="padding-top:15px;" id="featuredProducts"></td>
    </tr>
    <tr>
    	<td id="miniBanners_lower" style="padding:10px 0px 10px 0px;">
        	<div style="float:left;"><a href="/faq"><img src="/images/mainbanners/set1-minibanner1.jpg" border="0" /></a></div>
            <div style="float:left; padding-left:6px;"><a href="/customized-phone-cases"><img src="/images/mainbanners/custom-hombanner-mini1.jpg" border="0" /></a></div>
            <%'<div style="float:left; padding-left:6px;"><a href="/sb-17-sm-1267-scd-1030-design-faceplates-covers-apple-iphone-4s.asp"><img src="/images/mainbanners/set1-minibanner2.jpg" border="0" /></a></div>%>
            <div style="float:left; padding-left:6px;"><a href="http://www.cellstores.com/mobile/?r=wirelessemporium2" target="_blank"><img src="/images/mainbanners/set1-minibanner3.jpg" border="0" /></a></div>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
	<tr>
    	<td>
			<div style="float:left; width:440px; margin-right:15px;">
	        	<%
				'<div style="width:430px; font-size:15px; font-weight:bold; color:#494949; border:1px solid #ccc; background-color:#ebebeb; text-align:left; height:25px; padding:10px 0px 0px 10px">WIRELESS EMPORIUM</div>
                '<div style="width:420px; font-size:10px; color:#666666; padding:5px 5px 0px 5px;">
                '	<strong>Wireless Emporium</strong> is the Leader in <h1 class="xtrasmlIndexText" title="Cell Phone Accessories">Cell Phone Accessories</h1>. 
                '    We offer 1000s of accessories for all brands - 
                '    <a href="/blackberry-cell-phone-accessories.asp" class="xtrasmlIndexLink" title="BlackBerry Accessories">BlackBerry Accessories</a>, 
                '    <a href="/iphone-accessories.asp" class="xtrasmlIndexLink" title="iPhone Accessories">iPhone Accessories</a>, 
                '    <a href="/motorola-cell-phone-accessories.asp" class="xtrasmlIndexLink" title="Motorola Accessories">Motorola Accessories</a>,																 
                '    <a href="/lg-cell-phone-accessories.asp" class="xtrasmlIndexLink" title="LG Accessories">LG Accessories</a>, 
                '    <a href="/samsung-cell-phone-accessories.asp" class="xtrasmlIndexLink" title="Samsung Accessories">Samsung Accessories</a>, 
                '    <a href="/htc-cell-phone-accessories.asp" class="xtrasmlIndexLink" title="HTC Accessories">HTC Accessories</a> 
                '    &amp; more. All products, from 
                '    <a href="/cell-phone-covers-faceplates-screen-protectors.asp" class="xtrasmlIndexLink" title="cell phone covers">cell phone covers</a>, 
                '    <a href="/cell-phone-chargers.asp" class="xtrasmlIndexLink" title="cell phone chargers">cell phone chargers</a>, 
                '    <a href="/cell-phone-batteries.asp" class="xtrasmlIndexLink" title="cell phone batteries">cell phone batteries</a> to 
                '    <a href="/bluetooth-headsets-handsfree.asp" class="xtrasmlIndexLink" title="Bluetooth headsets">Bluetooth headsets</a>, 
                '    come with a 100% satisfaction guarantee. Discount Accessories + FREE SHIPPING = 
                '    The Best TOTAL Price Online, Every Day!
                '    <div id="spacer" style="height:10px;width:100%;"></div>
                '</div>
				%>
				<%
                '<div style="width:430px; margin-top:20px; font-size:15px; font-weight:bold; color:#494949; border:1px solid #ccc; background-color:#ebebeb; text-align:left; height:25px; padding:10px 0px 0px 10px">CELL PHONE ACCESSORIES</div>
                '<div style="width:420px; margin-bottom:20px; font-size:10px; color:#666666; padding:5px 5px 0px 5px;">
                '	These days, phones act as our social hubs, providing us with the ability to connect with friends and family through multiple media outlets. 
                '    Wireless Emporium knows how important it is to personalize and protect your phone, which is why we offer the largest assortment of cell phone 
                '    accessories anywhere. With our phones doing so much more than just making calls, they tend to use up battery life quickly. Make sure your 
                '    phone has the juice to get you through your busy day. Optimize the performance of your phone with our huge selection of cell phone batteries, 
                '    cell phone chargers, and cell phone holsters. You can also make a statement and show off your personal style. We have thousands of 
                '    <a href="/cell-phone-covers-faceplates-screen-protectors.asp" class="xtrasmlIndexLink" title="cell phone covers">cell phone covers</a>, 
                '    <a href="/cell-phone-cases.asp" class="xtrasmlIndexLink" title="cell phone cases">cell phone cases</a> 
                '    and cell phone charms to add a little color to your handheld device. Our full line of cheap cell phone accessories 
                '    aren't just priced well below retail prices, everything we sell is made from high quality materials and is guaranteed to meet the most 
                '    stringent of industry standards. Regardless of what brand or model phone you have, Wireless Emporium has the largest selection of accessories 
                '    to fit your needs. Whether it be a car charger for your Blackberry, a new phone case for your iPhone or a phone cover to protect that new 
                '    Samsung phone, we have it. We are a leading resource for phone accessories and 
                '    <a href="/unlocked-cell-phones.asp" class="xtrasmlIndexLink" title="unlocked cell phones">unlocked cell phones</a> 
                '    for every major brand of phone in the market, like HTC, LG, Nokia, Motorola and more. Shop with peace of mind, knowing that all of our 
                '    products are backed by our 100% satisfaction guarantee and come with free shipping.  Our commitment to customer service is the reason why we 
                '    have cultivated relationships with so many of our customers, and it's why you should choose Wireless Emporium for all of your cellular needs.
                '</div>
				%>
                <!--#include virtual="/includes/asp/commentBoxIndex.asp"-->
            </div>
        </td>
    </tr>
</table>
<script type="text/javascript">
    window.WEDATA.pageType = 'homepage';
</script>
<script language="javascript">var isMasterPage = 1</script>
<!--#include virtual="/includes/template/bottom_index.asp"-->