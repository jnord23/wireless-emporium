<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
response.buffer = true
dim basePageName : basePageName = mid(request.ServerVariables("SCRIPT_NAME"),2)
dim brandid
brandid = request.querystring("brandid")
if brandid = "" or not isNumeric(brandid) then
	call PermanentRedirect("/")
end if
strSort = prepStr(request("strSort"))

usePages = 1
curPageNum = ds(request.QueryString("page"))
if isnull(curPageNum) or len(curPageNum) < 1 then curPageNum = 1
showAll = prepStr(request.QueryString("show"))

if showAll <> "" then
	productsPerPage = 2000
	pagenationNumber = 40
else
	productsPerPage = 40
	pagenationNumber = 40	
end if

call fOpenConn()
SQL = "SELECT A.*, d.alwaysInStock, B.brandName, C.modelName, C.modelImg FROM we_items A INNER JOIN we_brands B ON A.brandID = B.brandID INNER JOIN we_models C ON A.modelID = C.modelID left join we_pnDetails d on a.partNumber = d.partNumber"
SQL = SQL & " WHERE A.typeid = '16' AND price_our is not null"
SQL = SQL & " AND A.hideLive = 0"
SQL = SQL & " AND (A.inv_qty <> 0 or d.alwaysInStock = 1)"
if brandid <> 12 then
	SQL = SQL & " AND A.brandID = '" & brandid & "'"
end if

select case strSort
	case "pop"
		sql = sql & " order by numberOfSales desc, flag1 desc, itemID desc"
	case "new"
		sql = sql & " order by itemID desc"
	case "low"
		sql = sql & " order by price_our, flag1 desc, itemID desc"		
	case "high"
		sql = sql & " order by price_our desc, flag1 desc, itemID desc"
	case else
		sql = sql & " order by ReleaseYear desc, ReleaseQuarter desc, itemDesc"
end select

'session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
else
	productCount = 0
	if not RS.eof then
		lap = -1
		do while not RS.EOF
			lap = lap + 1
			productCount = productCount + 1
			RS.movenext
		loop

		totalPages = round(productCount * 1.0 / pagenationNumber)
		
		RS.movefirst
	end if

	if curPageNum > 1 then
		loopPage = 1
		pagesSkipped = 0
		productsSkipped = 0
		do while cdbl(loopPage) < cdbl(curPageNum)
			pagesSkipped = pagesSkipped + 1
			for i = 1 to productsPerPage
				productsSkipped = productsSkipped + 1
				if rs.eof then
					exit for
				end if
				RS.movenext
			next
			loopPage = loopPage + 1
		loop
	end if	
end if

dim brandName, modelName, modelImg
if brandid = 12 then
	brandName = "All"
	modelName = ""
else
	brandName = RS("brandName")
	modelName = RS("modelName")
end if


dim SEtitle, SEdescription, SEkeywords, topText, bottomText
select case brandid
	case "1"
		topText = "Audiovox, one of the most reliable names in cell phones, offers a full line of devices that will suit everyone's needs. Wireless Emporium takes quality a step further by offering unlocked Audiovox <a class=""topText-link"" href=""http://www.wirelessemporium.com/unlocked-cell-phones.asp"" title=""Audiovox Cell Phones"">cell phones</a>, which lets you use the phone you love on the network you want. Our contract-free unlocked cell phones give you the freedom you've always wanted when picking the perfect phone."
		topText = topText & "<br><br>It's not always easy picking a phone that's right for you. Cell phones aren't just cell phones anymore; functionality has expanded to the point where they can be portable computers, capable of giving you directions when you're lost, being a portal to the Internet, and even allowing you to watch TV on the go. Wireless Emporium is committed to making your next cell phone purchase a simple one. We carry cell phones from all major and minor brands and each product page displays a wealth of information including key features and network compatibility. Best of all, we offer free shipping on every order!"
		bottomText = "Also check out our fine range of <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Audiovox Cell Phone Accessories"">Cell Phone Accessories</a>."
	case "2"
		topText = "Sony Ericsson, one of the most reliable names in cell phones, offers a full line of devices that will suit everyone's needs. Wireless Emporium takes quality a step further by offering unlocked Sony Ericsson cell phones without a contract, which lets you use the phone you love on the network you want. Our contract-free <a class=""topText-link"" href=""http://www.wirelessemporium.com/unlocked-cell-phones.asp"" title=""Unlocked Sony Ericsson Cell Phones"">unlocked cell phones</a> give you the freedom you've always wanted when picking the perfect phone."
		topText = topText & "<br><br>It's not always easy picking a phone that's right for you. Cell phones aren't just cell phones anymore; functionality has expanded to the point where they can be portable computers, capable of giving you directions when you're lost, being a portal to the Internet, and even allowing you to watch TV on the go. Wireless Emporium is committed to making your next cell phone purchase a simple one. We carry cell phones from all major and minor brands and each product page displays a wealth of information including key features and network compatibility. Best of all, we offer free shipping on every order!"
		bottomText = "Also check out our fine range of <a class=""topText-link"" href=""http://www.wirelessemporium.com/sony-ericsson-cell-phone-accessories.asp"" title=""Unlocked Sony Ericsson Cell Phone Accessories"">Sony Ericsson Accessories</a>!"
	case "3"
		topText = "Kyocera Qualcomm, one of the most reliable names in cell phones, offers a full line of devices that will suit everyone's needs. Wireless Emporium takes quality a step further by offering unlocked Kyocera Qualcomm cell phones, which lets you use the phone you love on the network you want. Our contract-free <a class=""topText-link"" href=""http://www.wirelessemporium.com/unlocked-cell-phones.asp"" title=""Unlocked Kyocera Qualcomm Cell Phones"">unlocked cell phones</a> give you the freedom you've always wanted when picking the perfect phone."
		topText = topText & "<br><br>It's not always easy picking a phone that's right for you. Cell phones aren't just cell phones anymore; functionality has expanded to the point where they can be portable computers, capable of giving you directions when you're lost, being a portal to the Internet, and even allowing you to watch TV on the go. Wireless Emporium is committed to making your next cell phone purchase a simple one. We carry cell phones from all major and minor brands and each product page displays a wealth of information including key features and network compatibility. Best of all, we offer free shipping on every order!"
		bottomText = "Also check out our range of the best <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Kyocera Qualcomm Cell Phone Accessories"">cell phone accessories</a>!"
	case "4"
		topText = "When it comes to cell phones, LG certainly knows its customers. Also in the business of producing electronic displays, you can bet that each LG cell phone features a great display with stunning resolution. Add to that a user-friendly interface and a wide range of functionality and you've got the makings of a great cell phone that will work hard to keep you connected to everything that's important."
		topText = topText & "<br><br>Despite an LG cell phone's vast functionality, a cell phone is useless without reliable service. A lack of options is inherent when purchasing a phone from a service provider and a multi-year contract is even more limiting. That's why Wireless Emporium offers the latest <a class=""topText-link"" href=""http://www.wirelessemporium.com/unlocked-cell-phones.asp"" title=""Unlocked LG Phones"">unlocked cell phones</a> for you to choose from. With an unlocked LG smartphone, you can get the phone you love and use it on a network you want."
		bottomText = "Also check out our fine range of <a class=""topText-link"" href=""http://www.wirelessemporium.com/lg-cell-phone-accessories.asp"" title=""Unlocked LG Cell Phone Accessories"">LG Cell Phone Accessories</a>!"
	case "5"
		topText = "Motorola is undoubtedly one of the oldest and most credible names in the cell phone industry today. Having been one of the pioneers in wireless phones, Motorola continues to make big breakthroughs and game-changing moves.  The Motorola RAZR made huge waves when it first came out with its super-slim design and unique keypad. But the MotoRAZR is locked to certain carriers, limiting its usability."
		topText = topText & "<br><br>Wireless Emporium offers the largest selection of <a class=""topText-link"" href=""http://www.wirelessemporium.com/cp-sb-5-motorola-unlocked-cell-phones.asp"" title=""Unlocked Motorola Cell Phones"">unlocked cell phones</a> to be used on any GSM network. With an unlocked phone, you can use the phone you love on the network you want. Say goodbye to the contractual commitment accompanied with buying a cell phone from service providers and get your unlocked cell phone today! Ordering from Wireless Emporium is easy and gives you the freedom you've always wanted when shopping for a phone."
		bottomText = "Check out our fine range of <a class=""topText-link"" href=""http://www.wirelessemporium.com/motorola-cell-phone-accessories.asp"" title=""Unlocked Motorola Cell Phone Accessories"">Motorola Cell Phone Accessories</a> as well!"
	case "6"
		topText = "Nextel, one of the most reliable names in cell phones, offers a full line of devices that will suit everyone's needs. Wireless Emporium takes quality a step further by offering Nextel cell phones without a contract, which lets you use the phone you love on the network you want. Our contract-free unlocked cell phones give you the freedom you've always wanted when picking the perfect phone."
		topText = topText & "<br><br>It's not always easy picking a phone that's right for you. <a class=""topText-link"" href=""http://www.wirelessemporium.com/unlocked-cell-phones.asp"" title=""Unlocked Nextel Cell Phones"">Cell phones</a> aren't just cell phones anymore; functionality has expanded to the point where they can be portable computers, capable of giving you directions when you're lost, being a portal to the Internet, and even allowing you to watch TV on the go. Wireless Emporium is committed to making your next cell phone purchase a simple one. We carry cell phones from all major and minor brands and each product page displays a wealth of information including key features and network compatibility. Best of all, we offer free shipping on every order!"
	case "7"
		topText = "Nokia, one of the most reliable names in <a class=""topText-link"" href=""http://www.wirelessemporium.com/unlocked-cell-phones.asp"" title=""Unlocked Nokia Cell Phones"">cell phones</a>, offers a full line of devices that will suit everyone's needs. Wireless Emporium takes quality a step further by offering unlocked Nokia cell phones without a contract, which lets you use the phone you love on the network you want. Our contract-free unlocked cell phones give you the freedom you've always wanted when picking the perfect phone."
		topText = topText & "<br><br>It's not always easy picking a phone that's right for you. Cell phones aren't just cell phones anymore; functionality has expanded to the point where they can be portable computers, capable of giving you directions when you're lost, being a portal to the Internet, and even allowing you to watch TV on the go. Wireless Emporium is committed to making your next cell phone purchase a simple one. We carry cell phones from all major and minor brands and each product page displays a wealth of information including key features and network compatibility. Best of all, we offer free shipping on every order!"
		bottomText = "Check out our fine range of <a class=""topText-link"" href=""http://www.wirelessemporium.com/nokia-cell-phone-accessories.asp"" title=""Unlocked Nokia Cell Phone Accessories"">Nokia Accessories</a> as well!"
	case "9"
		topText = "Samsung is one of the leading names in the cell phone industry. Known for making quality handsets that cater to a wide range of consumers, Samsung continues to push the envelope and provide feature-packed cell phones that keep the masses happy. Whether you're looking for a phone just to make calls or a smartphone that plays music, records video, and keeps you connected to your favorite social networking sites, Samsung is sure to have something that fits your needs."
		topText = topText & "<br><br>Wireless Emporium offers the largest selection of unlocked Samsung cell phones to be used on any GSM network. With an <a class=""topText-link"" href=""http://www.wirelessemporium.com/unlocked-cell-phones.asp"" title=""Unlocked Samsung Cell Phone"">unlocked cell phone</a>, you can use the phone you love on the network you want. Say goodbye to the contractual commitment accompanied with buying a cell phone from service providers and get your unlocked cell phone today! Ordering from Wireless Emporium is easy and gives you the freedom you've always wanted when shopping for a phone."
		bottomText = "Also check out our fine range of <a class=""topText-link"" href=""http://www.wirelessemporium.com/samsung-cell-phone-accessories.asp"" title=""Unlocked Samsung Cell Phone Accessories"">Samsung Cell Phone Accessories</a>!"
	case "14"
		topText = "Blackberry has been an innovator in smartphones since it debuted its first handheld in the late 1990s. Since then, Blackberry has made huge strides in giving users exactly what they want. If you're looking for a phone with a full QWERTY keyboard for easy text input and a trackball or optical trackpad for simplified navigation, Blackberry is sure to have a Cell phone that suits your needs."
		topText = topText & "<br><br>While many popular models like the Blackberry Curve, Blackberry Bold, Blackberry Tour, and Blackberry Storm are bound by service providers, users can break the chains with an <a class=""topText-link"" href=""http://www.wirelessemporium.com/unlocked-cell-phones.asp"" title=""Unlocked BlackBerry Cell Phone"">unlocked cell phone</a>. By purchasing an unlocked Blackberry smartphone, everyone can enjoy the Blackberry they want on the network they choose. Wireless Emporium offers the latest Blackberry phones without the commitment of contracts, allowing everyone to stay connected with a world famous Blackberry smartphone. With free shipping on every order, this deal is just too good to pass up."
		bottomText = "Also check out our range of <a class=""topText-link"" href=""http://www.wirelessemporium.com/blackberry-cell-phone-accessories.asp"" title=""BlackBerry Cell Phone Accessories"">BlackBerry Accessories</a>!"
	case "15"
		topText = "It's not always easy picking a phone that's right for you. <a class=""topText-link"" href=""http://www.wirelessemporium.com/unlocked-cell-phones.asp"" title=""Cell Phones"">Cell phones</a> aren't just cell phones anymore; functionality has expanded to the point where they can be portable computers, capable of giving you directions when you're lost, being a portal to the Internet, and even allowing you to watch TV on the go. Wireless Emporium is committed to making your next cell phone purchase a simple one. We carry cell phones from all major and minor brands and each product page displays a wealth of information including key features and network compatibility. Best of all, we offer free shipping on every order!"
		topText = topText & "<br><br>Simply choose one of the phones below to get started!"
	case "16"
		topText = "Palm, one of the most reliable names in cell phones, offers a full line of devices that will suit everyone's needs. Wireless Emporium takes quality a step further by offering unlocked Palm cell phones without a contract, which lets you use the phone you love on the network you want. Our contract-free <a class=""topText-link"" href=""http://www.wirelessemporium.com/unlocked-cell-phones.asp"" title=""Unlocked Cell Phones"">unlocked cell phones</a> give you the freedom you've always wanted when picking the perfect phone."
		topText = topText & "<br><br>It's not always easy picking a phone that's right for you. Cell phones aren't just cell phones anymore; functionality has expanded to the point where they can be portable computers, capable of giving you directions when you're lost, being a portal to the Internet, and even allowing you to watch TV on the go. Wireless Emporium is committed to making your next cell phone purchase a simple one. We carry cell phones from all major and minor brands and each product page displays a wealth of information including key features and network compatibility. Best of all, we offer free shipping on every order!"
	case "17"
		topText = "There's no doubt that Apple has made quite a name for itself in the world of <a class=""topText-link"" href=""http://www.wirelessemporium.com/unlocked-cell-phones.asp"" title=""Cell Phones"">cell phones</a>. Considered a leader in design, usability, and customer satisfaction, Apple has created new trends and broke records with the sale of its Apple iPhone. Never before has expanding a phone's functionality been so easy; users can choose from thousands of applications and easily download them to their phone with the use of the App Store. Unlimited functionality and a truly user-friendly interface have catapulted the iPhone into a class of its own."
		topText = topText & "<br><br>Initially exclusive to AT&T, the iPhone was limited by service providers. However, with an unlocked iPhone, subscribers of any GSM network can use this must-have phone with the network of their choice. At Wireless Emporium, we offer the latest iPhone 3G and iPhone 3GS without service provider restriction or the commitment of contracts. Now everyone can enjoy what millions all ready do. With free shipping on every order, this deal is just too good to pass up."
		bottomText = "Also check out our latest range of <a class=""topText-link"" href=""http://www.wirelessemporium.com/iphone-accessories.asp"" title=""iPhone Accessories"">iPhone Accessories</a>."
	case "18"
		topText = "Pantech, one of the most reliable names in cell phones, offers a full line of devices that will suit everyone's needs. Wireless Emporium takes quality a step further by offering unlocked Pantech cell phones without a contract, which lets you use the phone you love on the network you want. Our contract-free unlocked cell phones give you the freedom you've always wanted when picking the perfect phone."
		topText = topText & "<br><br>It's not always easy picking a phone that's right for you. Cell phones aren't just cell phones anymore; functionality has expanded to the point where they can be portable computers, capable of giving you directions when you're lost, being a portal to the Internet, and even allowing you to watch TV on the go. Wireless Emporium is committed to making your next cell phone purchase a simple one. We carry <a class=""topText-link"" href=""http://www.wirelessemporium.com/unlocked-cell-phones.asp"" title=""Unlocked Pantech Phones"">unlocked cell phones</a> from all major and minor brands and each product page displays a wealth of information including key features and network compatibility. Best of all, we offer free shipping on every order!"
		bottomText = "Check out our fine range of <a class=""topText-link"" href=""http://www.wirelessemporium.com/pantech-cell-phone-accessories.asp"" title=""Unlocked Pantech Cell Phone Accessories"">Pantech Accessories</a> as well!"
	case "19"
		topText = "The T-Mobile Sidekick brand has long been associated with multimedia and multitasking on all levels. Staying current on changing demands, Sidekick phones continue to give users what they want with one device that does it all�and does it all well. Easy access to music, social networking sites, and messages make a Sidekick a communicator's device of choice."
		topText = topText & "<br><br>Wireless Emporium is committed to offering the best Sidekick <a class=""topText-link"" href=""http://www.wirelessemporium.com/unlocked-cell-phones.asp"" title=""Sidekick Cell Phones"">cell phones</a>, including the Sidekick 2008 and Sidekick LX 2009. Find these and other Sidekick phones below!"
	case "20"
		topText = "When it comes to cell phones, HTC knows exactly what users want. With a complete line of cell phones for nearly all carriers, including Verizon, Sprint, T-Mobile, and AT&T, HTC has really made a name for itself by producing quality smartphones that are easy to use. Known best for their intuitive touch screen phones, HTC phones makes staying connected to those you love easy and truly hassle-free."
		topText = topText & "<br><br>But what is an HTC cell phone if you can't use it on the network you want? Wireless Emporium offers the latest unlocked HTC cell phones without the commitment of contracts, which means you get to use the phone you love on the network that's best for you. So what are you waiting for? Go ahead and get that <a class=""topText-link"" href=""http://www.wirelessemporium.com/unlocked-cell-phones.asp"" title=""Unlocked HTC Smartphone"">unlocked smartphone</a> you've always wanted. We'll even ship it to you for free!"
		bottomText = "Also check out our fine range of <a class=""topText-link"" href=""http://www.wirelessemporium.com/htc-cell-phone-accessories.asp"" title=""HTC Cell Phone Accessories"">HTC Accessories</a>!"
end select

if SEtitle = "" then SEtitle = "Buy Discounted Cell Phones: iPhone, Blackberry & HTC | WirelessEmporium"
if SEdescription = "" then SEdescription = "Get amazing Discounts on Cell Phones like the iPhone, Blackberry, HTC, Samsung, Motorola & more at WirelessEmporium � the online Cell Phone Store."
if SEkeywords = "" then SEkeywords = "cell phones, discounted cell phones, discount on cell phones, unlocked cell phone, cell phones for sale."

'todo: need to consolidate Metatags with we_XXXText
dim strH1: strH1 = "Cell Phones"
dim strH2: strH2 = EMPTY_STRING
dim strAltText: strAltText = EMPTY_STRING
dim strBreadcrumb: strBreadcrumb = brandName & " Cell Phones"

'set input values to extract page meta-values
dim dicReplaceAttribute: set dicReplaceAttribute = CreateObject("Scripting.Dictionary")
dicReplaceAttribute( "BrandName") = brandName

'set known product attributes to replace out template placeholders
dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
dicSeoAttributeInput( "BrandId") = brandID

call LookupSeoAttributes()

'SEtitle = "Buy Discounted Cell Phones: iPhone, Blackberry & HTC | WirelessEmporium"
'SEdescription = "Get amazing Discounts on Cell Phones like the iPhone, Blackberry, HTC, Samsung, Motorola & more at WirelessEmporium � the online Cell Phone Store."
'SEkeywords = "cell phones, discounted cell phones, discount on cell phones, unlocked cell phone, cell phones for sale."
%>
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/unlocked-cell-phones.asp">Unlocked Cell Phones</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle" height="65" style="background:url(/images/UnlockedPhones/banners/headers_<%=formatSEO(brandName)%>.jpg) no-repeat; background-size:745px;">
            <style>
                h1 {
                    font-size: 24px;
                    <%
                    if brandid = 5 or brandid = 7 or brandid = 14 then
                        %>
                        color: #CCCCCC;
                        <%
                    else
                        %>
                        color: #333333;
                        <%
                    end if
                    %>
                    font-weight: normal;
                    font-family: Tahoma, Trebuchet MS, Arial, Helvetica, sans-serif;
                    margin-bottom:10px;
                    margin-left:50px;
                }
            </style>
            <h1><%=strH1%></h1>
        </td>
    </tr>
    <tr><td width="100%" align="left" valign="top"><%=bottomText%></td></tr>
	<tr><td style="font-size:1px;border-bottom:1px solid #ccc;" align="left">&nbsp;</td></tr>
    <tr>
    	<td width="100%" style="padding-top:10px;">
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
                	<td width="70px" style="color:#494949; font-weight:bold;">&nbsp;Sort By:</td>
                    <td style="padding-top:5px;">
                    	<form name="frmSort" method="post">
                        	<input type="image" <%if strSort = "pop" then%> src="/images/button-most-pop2.jpg" <%else%> src="/images/button-most-pop1.jpg" onmouseover="this.src='/images/button-most-pop2.jpg'" onmouseout="this.src='/images/button-most-pop1.jpg'" <%end if%> onclick="return fnSort('pop');" border="0" />
                        	<input type="image" <%if strSort = "new" then%> src="/images/button-new2.jpg" <%else%> src="/images/button-new1.jpg" onmouseover="this.src='/images/button-new2.jpg'" onmouseout="this.src='/images/button-new1.jpg'" <%end if%> onclick="return fnSort('new');" border="0" />
                        	<input type="image" <%if strSort = "low" then%> src="/images/button-lowest2.jpg" <%else%> src="/images/button-lowest1.jpg" onmouseover="this.src='/images/button-lowest2.jpg'" onmouseout="this.src='/images/button-lowest1.jpg'" <%end if%> onclick="return fnSort('low');" border="0" />
                        	<input type="image" <%if strSort = "high" then%> src="/images/button-highest2.jpg" <%else%> src="/images/button-highest1.jpg" onmouseover="this.src='/images/button-highest2.jpg'" onmouseout="this.src='/images/button-highest1.jpg'" <%end if%> onclick="return fnSort('high');" border="0" />
                        </form>
                    </td>
					<td align="right">
                    	<div style="float:right;">
						<%
                        if usePages = 1 then
							nStartPos = cdbl(curPageNum) - 2
							nLap = 0
							if nStartPos < 1 then nStartPos = 1 end if
							if nStartPos > 3 then
							%>
                                <div class="pageOff" align="center"><a href="<%=REWRITE_URL%>?strSort=<%=strSort%>" class="pageOff">1</a></div>
                                <div style="float:left; height:5px; padding-top:14px; color:#494949;"> ...&nbsp;</div>
                            <%
							end if
                            for x = nStartPos to totalPages
								nLap = nLap + 1
                                strLinkToPage = REWRITE_URL & "?page=" & x & "&strSort=" & strSort
                                if x = 1 then strLinkToPage = REWRITE_URL & "?strSort=" & strSort end if
                                
                                if x = cdbl(curPageNum) and showAll <> "all" then
                                %>
                                <div class="pageOn" align="center"><a href="<%=strLinkToPage%>" class="pageOn"><%=x%></a></div>
                                <%
                                else
                                %>
                                <div class="pageOff" align="center"><a href="<%=strLinkToPage%>" class="pageOff"><%=x%></a></div>
                                <%
                                end if
								
								if nLap = totalPages or nLap = 4 then
									exit for
								end if
                            next
							if totalPages > 1 then
								if cdbl(curPageNum) + 1 > totalPages then
									strLinkToPage = REWRITE_URL & "?page=" & totalPages & "&strSort=" & strSort
								else
									strLinkToPage = REWRITE_URL & "?page=" & cdbl(curPageNum) + 1 & "&strSort=" & strSort
								end if
							%>
                            <div style="float:left; height:5px; padding-top:14px; color:#494949;"> ...&nbsp;</div>
                            <div class="pageNext" align="center">
                            	<a href="<%=strLinkToPage%>" class="pageNext">Next</a>
							</div>
    	                        <%
								if showAll = "all" then
								%>
							<div class="pageOff" align="center" style="width:50px;"><a href="<%=REWRITE_URL%>?show=all&strSort=<%=strSort%>" class="pageOn">View All</a></div>
	                            <%
								else
								%>
							<div class="pageOff" align="center" style="width:50px;"><a href="<%=REWRITE_URL%>?show=all&strSort=<%=strSort%>" class="pageOff">View All</a></div>                                
	                            <%
								end if
							end if
                        end if
                        %>
                    	</div>
                    </td>                    
				</tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="bottom" width="100%" height="20"><img src="/images/UnlockedPhones/gray_bar.jpg" width="745" height="5" border="0"></td>
    </tr>
    <tr>
        <td>
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <%
                    a = 0
                    dim DoNotDisplay, RSkit
                    dim altText
                    do until RS.eof
                        DoNotDisplay = 0
                        if not isNull(RS("ItemKit_NEW")) then
                            SQL = "SELECT inv_qty FROM we_Items WHERE itemID IN (" & RS("ItemKit_NEW") & ")"
                            set RSkit = Server.CreateObject("ADODB.recordset")
                            RSkit.open SQL, oConn, 0, 1
                            do until RSkit.eof
                                if RSkit("inv_qty") < 1 then DoNotDisplay = 1
                                RSkit.movenext
                            loop
                            RSkit.close
                            set RSkit = nothing
                        end if
                        if DoNotDisplay = 0 then
                            altText = brandName & " " & modelName & " " & singularSEO("Unlocked Cell Phones") & " ?" & RS("itemDesc")
                            response.write "<td align=""center"" valign=""top"" width=""180"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""180"" height=""100%"">" & vbcrlf
                            response.write "<tr><td align=""center"" valign=""middle"" width=""180"" height=""154""><a href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp""><img src=""/productpics/thumb/" & RS("itemPic") & """ border=""0"" alt=""" & altText & """></a></td></tr>" & vbcrlf
                            response.write "<tr><td align=""center"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
                            response.write "<tr><td align=""center"" valign=""top""><a class=""cellphone-link"" href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"" title=""" & altText & """>" & RS("itemDesc") & "</a></td></tr>" & vbcrlf
                            response.write "<tr><td align=""center"" valign=""top"" height=""8""><img src=""/images/spacer.gif"" width=""1"" height=""8""></td></tr>" & vbcrlf
                            response.write "<tr><td align=""center"" valign=""bottom""><span class=""boldText"">Our&nbsp;Price:&nbsp;</span><span class=""pricing-orange2"">" & formatCurrency(RS("price_Our")) & "</span><br><span class=""pricing-gray2"">List&nbsp;Price:&nbsp;<s>" & formatCurrency(RS("price_Retail")) & "</s></span><br><span class=""pricing-gray2"">You&nbsp;Save:&nbsp;" & formatCurrency(RS("price_Retail") - RS("price_Our")) & "</span></td></tr>" & vbcrlf
                            response.write "</table></td>" & vbcrlf
                            a = a + 1
                            if a = 4 then
                                response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""745"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
                                a = 0
                            else
                                response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                            end if
                        end if
                        RS.movenext
                    loop
                    if a = 1 then
                        response.write "<td width=""180"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""180"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""180"">&nbsp;</td>" & vbcrlf
                    elseif a = 2 then
                        response.write "<td width=""180"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""180"">&nbsp;</td>" & vbcrlf
                    elseif a = 3 then
                        response.write "<td width=""180"">&nbsp;</td>" & vbcrlf
                    end if
                    %>
                </tr>
            </table>
        </td>
    </tr>
	<tr><td style="font-size:1px;border-bottom:1px solid #ccc;" align="left">&nbsp;</td></tr>
    <tr>
    	<td width="100%" style="padding:10px 0px 10px 0px; border-bottom:1px solid #ccc;">
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
                	<td width="70px" style="color:#494949; font-weight:bold;">&nbsp;Sort By:</td>
                    <td style="padding-top:5px;">
                    	<form name="frmSort" method="post">
                        	<input type="image" <%if strSort = "pop" then%> src="/images/button-most-pop2.jpg" <%else%> src="/images/button-most-pop1.jpg" onmouseover="this.src='/images/button-most-pop2.jpg'" onmouseout="this.src='/images/button-most-pop1.jpg'" <%end if%> onclick="return fnSort('pop');" border="0" />
                        	<input type="image" <%if strSort = "new" then%> src="/images/button-new2.jpg" <%else%> src="/images/button-new1.jpg" onmouseover="this.src='/images/button-new2.jpg'" onmouseout="this.src='/images/button-new1.jpg'" <%end if%> onclick="return fnSort('new');" border="0" />
                        	<input type="image" <%if strSort = "low" then%> src="/images/button-lowest2.jpg" <%else%> src="/images/button-lowest1.jpg" onmouseover="this.src='/images/button-lowest2.jpg'" onmouseout="this.src='/images/button-lowest1.jpg'" <%end if%> onclick="return fnSort('low');" border="0" />
                        	<input type="image" <%if strSort = "high" then%> src="/images/button-highest2.jpg" <%else%> src="/images/button-highest1.jpg" onmouseover="this.src='/images/button-highest2.jpg'" onmouseout="this.src='/images/button-highest1.jpg'" <%end if%> onclick="return fnSort('high');" border="0" />
                        </form>
                    </td>
					<td align="right">
                    	<div style="float:right;">
						<%
                        if usePages = 1 then
							nStartPos = cdbl(curPageNum) - 2
							nLap = 0
							if nStartPos < 1 then nStartPos = 1 end if
							if nStartPos > 3 then
							%>
                                <div class="pageOff" align="center"><a href="<%=REWRITE_URL%>?strSort=<%=strSort%>" class="pageOff">1</a></div>
                                <div style="float:left; height:5px; padding-top:14px; color:#494949;"> ...&nbsp;</div>
                            <%
							end if
                            for x = nStartPos to totalPages
								nLap = nLap + 1
                                strLinkToPage = REWRITE_URL & "?page=" & x & "&strSort=" & strSort
                                if x = 1 then strLinkToPage = REWRITE_URL & "?strSort=" & strSort end if
                                
                                if x = cdbl(curPageNum) and showAll <> "all" then
                                %>
                                <div class="pageOn" align="center"><a href="<%=strLinkToPage%>" class="pageOn"><%=x%></a></div>
                                <%
                                else
                                %>
                                <div class="pageOff" align="center"><a href="<%=strLinkToPage%>" class="pageOff"><%=x%></a></div>
                                <%
                                end if
								
								if nLap = totalPages or nLap = 4 then
									exit for
								end if
                            next
							if totalPages > 1 then
								if cdbl(curPageNum) + 1 > totalPages then
									strLinkToPage = REWRITE_URL & "?page=" & totalPages & "&strSort=" & strSort
								else
									strLinkToPage = REWRITE_URL & "?page=" & cdbl(curPageNum) + 1 & "&strSort=" & strSort
								end if
							%>
                            <div style="float:left; height:5px; padding-top:14px; color:#494949;"> ...&nbsp;</div>
                            <div class="pageNext" align="center">
                            	<a href="<%=strLinkToPage%>" class="pageNext">Next</a>
							</div>
    	                        <%
								if showAll = "all" then
								%>
							<div class="pageOff" align="center" style="width:50px;"><a href="<%=REWRITE_URL%>?show=all&strSort=<%=strSort%>" class="pageOn">View All</a></div>
	                            <%
								else
								%>
							<div class="pageOff" align="center" style="width:50px;"><a href="<%=REWRITE_URL%>?show=all&strSort=<%=strSort%>" class="pageOff">View All</a></div>                                
	                            <%
								end if
							end if
                        end if
                        %>
                    	</div>
                    </td>                    
				</tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%" class="topText" style="padding-top:10px;">
			<%=topText%>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->
<script>
	function fnSort(sortOption)
	{
		strLoc = '<%=REWRITE_URL & "?strSort="%>' + sortOption;

		<% if cdbl(curPageNum) > 1 then %>
			strLoc = strLoc + '&page=' + <%=cdbl(curPageNum)%>;
		<% end if %>
		
		<% if display = "showall" then %>
			strLoc = strLoc + '&disp=' + <%=display%>;
		<% end if %>

		window.location = strLoc;
		return false;
	}
</script>