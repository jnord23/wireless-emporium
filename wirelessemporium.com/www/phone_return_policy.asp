<html>
<head>
<title></title>
<link href="/includes/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="10" class="smlText">
	<tr>
		<td width="100%" valign="top">
			<table width="500" border="0" cellspacing="1" cellpadding="10" class="smlText" align="center">
				<tr>
					<td align="center" bgcolor="#EAEAEA"><strong>** CELL PHONE RETURN POLICY **</strong></td>
				</tr>
				<tr>
					<td bgcolor="#EAEAEA">
						<div align="justify">
							<p>All return requests must be made within <strong>7 days</strong> of receiving the item. All returned merchandise must be unregistered, in complete original manufacturer's packaging, same condition as sold, with all literature, accessories, instructions, blank warranty cards and documentation. All Phones must be returned in &quot;Like-New Condition&quot;, show no signs of use and must have less than <strong>25 minutes</strong> in total cumulative talk time.</p>
							<p>If an item is defective it may only be exchanged with an equivalent product if we are unable to supply the same item or if the item cannot be repaired. Any return shipping cost is the responsibility of the customer. All shipping and handling fees are non-refundable. This includes all refused and unaccepted packages.</p>
							<p>All NON-DEFECTIVE returns will be subject to a 20% restocking fee.</p>
							<p>For refunds allow 3-4 business days for the refund to post to your account from the time the refund is issued.</p>
							<p>All returns must be accompanied by RMA number. To obtain an RMA number, please send an e-mail to: returns@wirelessemporium.com</p>
							<p>Be sure to include the following information:</p>
							<ul style="margin-top:0px;">
								<li>Order Number</li>
								<li>Date of original purchase</li>
								<li>Reason for request for RMA</li>
								<li>E-mail address</li>
								<li>Phone number</li>
							</ul>
						</div>
					</td>
				</tr>
				<tr>
					<td align="center" bgcolor="#EAEAEA">
						<form><input type="button" onClick="window.close();" value="Close Window" class="smlText"></form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
