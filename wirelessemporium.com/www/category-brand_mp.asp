<%
response.Write("testing2")
response.End()
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
response.buffer = true
set fso = CreateObject("Scripting.FileSystemObject")

dim pageName : pageName = "category-brand"
dim phoneOnly
dim brandid : brandid = prepInt(request.querystring("brandid"))
dim categoryid : categoryid = prepInt(request.querystring("categoryid"))
dim cbModel : cbModel = ds(request("cbModel"))
dim cbSort : cbSort = ds(request("cbSort"))
dim curPage : curPage = prepStr(request.QueryString("curPage"))

if brandid = 0 then
	if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
		response.Write("Error 1")
		response.End()
	else
		call PermanentRedirect("/")
	end if
end if
if categoryid = 0 then
	if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
		response.Write("Error 2")
		response.End()
	else
		call PermanentRedirect("/")
	end if
end if
if not isnull(session("qSort")) then
	qSort = session("qSort")
	session("qSort") = null
end if
isUniversal = false

response.Cookies("saveBrandID") = brandID
response.Cookies("saveModelID") = 0
response.Cookies("saveItemID") = 0

stitchImgPath = "/images/stitch/catBrands/"
	
if instr(curPage, "apple-iphone") > 0 then
	phoneOnly = 1
else
	phoneOnly = 0
end if

leftGoogleAd = 1
googleAds = 1
if categoryid = 20 then categoryid = 1270 end if

sql = "exec sp_categoryData " & categoryid
session("errorSQL") = SQL
set RS = oConn.execute(sql)

strSubTypeID = ""
if RS.eof then
	if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
		response.Write("Error 3")
		response.End()
	else
		call PermanentRedirect("/")
	end if
else
	categoryName = RS("typeName")
	parentTypeID = RS("typeid")
	isUniversal = RS("hasUniversal")
	do until RS.eof
		strSubTypeID = strSubTypeID & RS("subtypeid") & ","
		RS.movenext
	loop
	strSubTypeID = left(strSubTypeID, len(strSubTypeID) - 1)
end if
if strSubTypeID = "" then strSubTypeID = "9999999" end if

showIntPhones = request.Form("showIntPhones")
if isnull(showIntPhones) or len(showIntPhones) < 1 then showIntPhones = 0 end if
if isnull(cbSort) or len(cbSort) < 1 then
	noSort = 1
	cbSort = "AZ"
end if
if categoryID = 3 and brandID = 17 then 'is iPhone
	cbSort = "ZA" '"ZA" gets the job done just fine for now
end if
if isnull(qSort) or len(qSort) < 1 then qSort = ""
if qSort <> "" then cbSort = qSort

cellPhoneTitle = "Cell Phone"		
strSqlAnd = ""
if brandID = 17 and phoneOnly = 1 then
	cellPhoneTitle = "iPhone"
elseif brandID = 17 and phoneOnly = 0 then
	cellPhoneTitle = "iPod / iPad"
	phoneTitle = "Product"
end if



sql = "exec sp_modelsByCatAndBrand " & categoryID & "," & brandID & ",'" & categoryName & "'," & phoneOnly & ",'" & cbSort & "'," & noSort
session("errorSQL") = SQL
'response.write "<pre>" & sql & "</pre>"
'response.end
arrRS = getDbRows(sql)

arrPopPhones = filter2DRS(arrRS, 4, true)
arrOldPhones = filter2DRS(arrRS, 5, true)	
arrIntPhones = filter2DRS(arrRS, 6, true)
arrDisplayPhones = filter2DRS(arrRS, 10, true)
arrSortedRS = sort2DRS(arrRS, 2)

if isnull(arrRS) then
	if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
		response.Write("Error 4")
		response.End()
	else
		call PermanentRedirect("/")
	end if
end if

if ubound(arrPopPhones,2) >= 0 then showPopPhones = 1 else showPopPhones = 0 end if
if ubound(arrOldPhones,2) >= 0 then showOldPhones = 1 else showOldPhones = 0 end if
if ubound(arrIntPhones,2) >= 0 then showIntPhones = 1 else showIntPhones = 0 end if
if ubound(arrDisplayPhones,2) >= 0 then showRegularPhones = 1 else showRegularPhones = 0 end if

if brandName = "" then brandName = arrRS(7,0) end if

phoneList = ""
if not isnull(arrSortedRS) then
	for i=0 to ubound(arrSortedRS,2)
		if phoneList = "" then
			phoneList = """" & arrSortedRS(2,i) & " " & nameSEO(categoryName) & """"
		else
			phoneList = phoneList & ", """ & arrSortedRS(2,i) & " " & nameSEO(categoryName) & """"
		end if
	next
end if

sql	= "exec sp_allCarriers"
session("errorSQL") = sql
set objRsCarrier = oConn.execute(sql)

a = 0
maxModels = 999

if showPopPhones = 1 then
	popItemImgPath = formatSEO(brandName) & "_pop_" & categoryid & ".jpg"
end if

if brandID = 17 and phoneOnly = 1 then
	itemImgPath = "applePhones_all" & fileExt & "_" & categoryid & ".jpg"
else
	itemImgPath = formatSEO(brandName) & "_all" & fileExt & "_" & categoryid & ".jpg"
	oldItemImgPath = formatSEO(brandName) & "_old" & fileExt & "_" & categoryid & ".jpg"
	intItemImgPath = formatSEO(brandName) & "_int" & fileExt & "_" & categoryid & ".jpg"
end if

dim strBreadcrumb

if brandid = 17 and phoneOnly = 1 then
	headerImgID = 17
	if h1 = "" then h1 = brandName & " iPhone " & nameSEO(categoryName)
	if strBreadcrumb = "" then strBreadcrumb = "Cell Phone " & nameSEO(categoryName) & " for " & brandName & " iPhone"
	if strCatCrumb = "" then strCatCrumb = nameSEO(categoryName)	
elseif brandid = 17 and phoneOnly = 0 then
	headerImgID = 17
	if h1 = "" then h1 = brandName & " iPod/iPad " & nameSEO(categoryName)
	if strBreadcrumb = "" then strBreadcrumb = nameSEO(categoryName) & " for " & brandName & " iPod/iPad"
	if strCatCrumb = "" then strCatCrumb = nameSEO(categoryName)
	
	if topText = "" then topText = replace(lcase(topText), "cell phone", "iPod/iPad")
	if altText = "" then altText = replace(lcase(altText), "cell phone", "iPod/iPad")
else
	headerImgID = 20
	if h1 = "" then h1 = brandName & " Cell Phone " & nameSEO(categoryName)
	if strBreadcrumb = "" then strBreadcrumb = "Cell Phone " & nameSEO(categoryName) & " for " & brandName
	if strCatCrumb = "" then strCatCrumb = "Cell Phone " & nameSEO(categoryName)
end if

if altText = "" then altText = nameSEO(categoryName) & " for " & brandName & " Model"

'todo: need to consolidate Metatags with we_XXXText
dim strH1: strH1 = h1
dim strH2: strH2 = EMPTY_STRING
dim strAltText: strAltText = EMPTY_STRING
	
if showPopPhones = 1 then
	imgStitchPath = Server.MapPath("/images/stitch/catBrands/" & popItemImgPath)
	call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrPopPhones, 3)	
end if
if showOldPhones = 1 then
	imgStitchPath = Server.MapPath("/images/stitch/catBrands/" & oldItemImgPath)
	call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrOldPhones, 3)		
end if
if showIntPhones = 1 then
	imgStitchPath = Server.MapPath("/images/stitch/catBrands/" & intItemImgPath)
	call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrIntPhones, 3)			
end if
if showRegularPhones = 1 then
	imgStitchPath = Server.MapPath("/images/stitch/catBrands/" & itemImgPath)
	call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrDisplayPhones, 3)
end if
%>
<!--#include virtual="/includes/template/top.asp"-->
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script>
	$(document).ready(function() {
		$("input#id_Search").autocomplete({source: [<%=phoneList%>]	});
		$("input#id_Search").autocomplete( "option", "position", { my : "right top", at: "right bottom" } );
		$("input#id_Search").bind( "autocompleteselect", function(event, ui) { jumpto(); });
	});
</script>
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="/phone-<%=formatSEO(categoryName)%>"><%=strCatCrumb%></a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
        </td>
    </tr>
    <tr>
        <td width="100%" align="center" valign="top">
            <img src="/images/brandmodelheaders/header_<%=brandID%>.jpg" width="748" height="30" border="0" alt="<%=brandName &" "& modelName &" "&categoryName %>">
        </td>
    </tr>
	<tr>
        <td width="100%" align="center" valign="top">
            <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="200" align="left"><img src="/images/cats/we_header_<%=parentTypeID%>.jpg" border="0" alt="<%=strH1%>" title="<%=strH1%>" /></td>                
                	<td width="*" style="padding-left:10px;">
                        <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                            <tr>                    
                            	<td valign="top" align="left">
	                                <h1 class="catBrand"><%=strH1%></h1>
                                </td>
							</tr>
                            <tr>                    
                            	<td valign="top" align="left" class="topText">
									<%
									if prepStr(SeTopText) <> "" then topText = SeTopText
									response.Write(topText)
									%>
                                </td>
							</tr>
						</table>
                    </td>
                </tr>
			</table>
        </td>
    </tr>    
    <tr>
    	<td style="padding-top:15px;">
			<form name="frmSearch" method="post">        
            <div class="graybox-middle" style="width:726px; border-left:1px solid #999; border-right:1px solid #999; padding:0px 10px 0px 10px;">
            	<div style="float:right; padding-top:2px;"><img src="/images/WE-seach-button.jpg" border="0" onclick="jumpto(); return false;" style="cursor:pointer;" /></div>
            	<div style="float:right; padding:8px 10px 0px 10px;"><input type="text" id="id_Search" name="txtSearch" value="Search" onclick="this.value='';" /></div>
                <div class="graybox-inner" title="Choose from the following" align="right">
                    <select name="cbModel" style="float:right; width:400px;" onChange="if(this.value != ''){window.location=this.value;}">
                        <option value="">Choose from the following</option>
                        <%
                        for nRows=0 to ubound(arrSortedRS, 2)
                            tModelid = cint(arrSortedRS(1,nRows))
                            tModelName = arrSortedRS(2,nRows)
                            tBrandid = cint(arrSortedRS(8,nRows))
                            tBrandName = arrRS(7,nRows)
                            tAnchorText = tModelName & " " & nameSEO(categoryName)
                            catLinkFix = "-sc-"
                            if categoryid > 999 then catLinkFix = "-scd-" end if
                            strLink = "/sb-" & tBrandid & "-sm-" & tModelid & catLinkFix & categoryID & "-" & formatSEO(categoryName) & "-" & formatSEO(tBrandName) & "-" & formatSEO(tModelName) & ".asp"

                            if categoryid = 1270 then
                                strLink = "/sb-" & tBrandid & "-sm-" & tModelid & "-sc-" & categoryID & "-music-skins-genre.asp"
                            end if
                            %>
                            <option value="<%=strLink%>"><%=tAnchorText%></option>                                                                                                    
                            <%
                        next
                        %>
                    </select>
                	<span style="font-weight:bold; float:right;">Jump to:&nbsp;&nbsp;</span>
				</div>
            </div>
			</form>            
        </td>
    </tr>
    <%
	if showPopPhones = 1 then
	%>
    <tr>
        <td width="100%" style="padding-top:5px;">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="background:url(/images/brands/gray-header-right.gif) repeat-x;">
            	<tr>
                	<td align="left" valign="top">
                    	<div style="background:url(/images/brands/gray-header-left.jpg) no-repeat; width:415px; height:30px; color:#fff; font-size:14px; padding:4px 0px 0px 10px;">TOP <%=ucase(replace(brandName, "/", " / ") & " " & cellPhoneTitle)%> MODELS</div>
					</td>
                </tr>
			</table>
        </td>
	</tr>
    <tr>
        <td width="100%">
			<%=drawStitchModelBoxByArray(arrPopPhones, popItemImgPath)%>
        </td>
	</tr>
    <%
	end if
	
	if showRegularPhones = 1 then
	%>
    <tr>
        <td width="100%" style="padding-top:15px;">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="background:url(/images/brands/gray-header-right.gif) repeat-x;">
            	<tr>
                	<td align="left" valign="top">
                    	<div style="background:url(/images/brands/gray-header-left.jpg) no-repeat; width:415px; height:30px; color:#fff; font-size:14px; padding:4px 0px 0px 10px;">
						<% if showPopPhones = 1 and brandid <> 15 then %>
                        OTHER <%=ucase(replace(brandName, "/", " / ") & " " & cellPhoneTitle)%> MODELS
						<% else %>
                        <%=ucase(replace(brandName, "/", " / ") & " " & cellPhoneTitle)%> MODELS
                        <% end if %>
                        </div>
					</td>
                </tr>
			</table>
        </td>
	</tr>
    <tr>
        <td width="100%">
			<%=drawStitchModelBoxByArray(arrDisplayPhones, itemImgPath)%>
        </td>
	</tr>
    <%
	end if
	
	if showOldPhones = 1 then
	%>
    <tr>
        <td width="100%" style="padding-top:5px; cursor:pointer;" onclick="toggleModels('olderModels','id_bottom_arrow_old');" title="CLICK TO VIEW MORE <%=ucase(brandName)%> MODELS">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
            	<tr>
                	<td width="10px"><img src="/images/brands/bottom-header-left.jpg" border="0" style="cursor:pointer;" /></a>
					</td>
                    <td style="background:url(/images/brands/bottom-header-center.jpg) repeat-x; padding-top:10px; font-size:18px; font-weight:bold; color:#494949;" align="right" valign="top" width="*">
						CLICK TO VIEW MORE <%=ucase(brandName)%> MODELS 
                    </td>
                    <td style="background:url(/images/brands/bottom-header-center.jpg) repeat-x; width:180px; padding-left:10px;" align="left" valign="top">
                    	<img id="id_bottom_arrow_old" src="/images/brands/down-arrow.jpg" border="0" style="margin-top:10px; cursor:pointer;" />
                    </td>
                    <td width="10px"><img src="/images/brands/bottom-header-right.jpg" border="0" style="cursor:pointer;" /></td>
                </tr>
			</table>
        </td>
	</tr>
    <tr>
        <td width="100%">
			<div id="olderModels" style="display:none;">
				<%=drawStitchModelBoxByArray(arrOldPhones, oldItemImgPath)%>
            </div>
        </td>
	</tr>
    <%
	end if

	if showIntPhones = 1 then
	%>
    <tr>
        <td width="100%" style="padding-top:5px; cursor:pointer;" onclick="toggleModels('intModels','id_bottom_arrow_int');" title="CLICK TO VIEW MORE <%=ucase(brandName)%> MODELS">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
            	<tr>
                	<td width="10px"><img src="/images/brands/bottom-header-left.jpg" border="0" style="cursor:pointer;" /></a>
					</td>
                    <td style="background:url(/images/brands/bottom-header-center.gif) repeat-x; padding-top:11px; font-size:18px; font-weight:bold; color:#494949;" align="right" valign="top" width="*">
						CLICK TO VIEW INTERNATIONAL <%=ucase(brandName)%> MODELS 
                    </td>
                    <td style="background:url(/images/brands/bottom-header-center.gif) repeat-x; width:140px; padding-left:10px;" align="left" valign="top">
                    	<div style="padding-top:10px;"><img id="id_bottom_arrow_int" src="/images/brands/down-arrow.jpg" border="0" style="margin-top:10px; cursor:pointer;" /></div>
                    </td>
                    <td width="10px"><img src="/images/brands/bottom-header-right.jpg" border="0" style="cursor:pointer;" /></td>
                </tr>
			</table>
        </td>
	</tr>
    <tr>
        <td width="100%">
			<div id="intModels" style="display:none;">
				<%=drawStitchModelBoxByArray(arrIntPhones, intItemImgPath)%>
            </div>
        </td>
	</tr>
    <%
	end if
	%>                
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
    <tr><td align="center" class="bottomText"><%=bottomText%></td></tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->
<script>
	function jumpto()
	{
		modelname = document.frmSearch.txtSearch.value;
		var x=document.frmSearch.cbModel;
		for (i=0;i<x.length;i++)
			if (x.options[i].text == modelname) window.location = x.options[i].value;
	}
	
	function toggleModels(which, idArrow)
	{
		toggle(which);
		
		if (document.getElementById(which).style.display == '') 
			document.getElementById(idArrow).src = "/images/brands/up-arrow.jpg"
		else 
			document.getElementById(idArrow).src = "/images/brands/down-arrow.jpg"
	}
</script>