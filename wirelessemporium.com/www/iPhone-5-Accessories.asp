<%
	response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<%
	call PermanentRedirect("/accessories-for-apple-iphone-5.asp")
	'sql = ""
	'session("errorSQL") = sql
	'set	RS = oConn.execute(SQL)
%>

<style>
body {
    /*font-family: "Lucida Grande", Arial, Helvetica, sans-serif;*/
}

#diviPhone5Rumors h2, #diviPhone5Rumors h3, #diviPhone5Rumors dt {
    color:slategray;
}



#diviPhone5Rumors dt, #diviPhone5Rumors dd {
    margin-bottom: 1.0em;
	/*margin-left: 1.5em;*/
}

#diviPhone5Rumors dt {
    font-weight: bold;
	font-size:1.25em;
}



#diviPhone5Rumors dl dd dt {
    color: #000;
    margin-top: 1.0em;
    font-weight: inherit;
    font-style: italic;
}

.divPhotoR {
	margin-top:2.5em;
	margin-left:2.0em;
	margin-bottom:1.5em;
	min-width:1px;
	min-height:1px;
	float:right;
}

.firstPhoto {
	padding-top:0.0em;
}

.nonFirstPhoto {
	margin-top:0.5em;
}

#diviPhone5Rumors .italic {
    font-style: italic;
}

#diviPhone5Rumors .empty {
	display:none;
}

#diviPhone5Rumors img {
	border:none;
}
</style>

<div id="diviPhone5Rumors">
<div id="divBreadCrumbs" class="breadcrumbFinal">
    <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;
    <a class="breadcrumb" href="/iphone-accessories.asp">Apple iPhone Accessories</a>&nbsp;&gt;&nbsp;
    Apple iPhone 5 Rumors
</div>
<div id="divBrandModelHeader">
	<img src="/images/brandmodelheaders/header_17.jpg" width="745" height="30" border="0" alt=" iPhone 4S Accessories">
</div>
    <h1 class="brandModel">
        Apple iPhone 5 Accessories
    </h1>

    <div class="divPhotoR firstPhoto">
        <img src="/images/iPhone5Rumors/iPhone-5-Back.jpg" alt="iPhone 5" width="360" height="195" />
    </div>

    <p>
        The Apple iPhone 5 is likely one of the only phones in the world that can dominate
        headlines without even so much a picture of what it'll actually look like. In fact,
        rumors are swirling about its form factor, specs and release date and the facts
        seem to be changing every day. One thing that you can count on, however, is that
        <a href="http://www.wirelessemporium.com">Wireless Emporium</a> is busy stocking 
        its shelves with the largest selection of <a href="/iphone-accessories.asp">iPhone 5 accessories</a>, 
        cases, covers and more so that you can be fully equipped with the
        best accessories once the iPhone 5 hits the market. With free fast shipping, a worry-free
        90 day return policy and an unmatched one year warranty, be sure to visit this page
        for updates!</p>
    
    <h3>
        EVERYTHING (WE THINK) WE KNOW ABOUT THE iPHONE 5
    </h3>
    
    <dl>
        <dt class="italic">When is the iPhone 5 coming out? What are the new features? What is the iPhone 5
            price? Will it be under Sprint, Verizon, or AT&T?</dt>
        <dd>
            Everyone is dying to know the details about Apple's upcoming phone. We have rounded
            up the rumors, predictions, and speculations regarding the next-generation iPhone.
            We warn you though to take everything with a grain of salt. Nothing is official
            until the company actually launches the much-awaited device.
        </dd>
    </dl>
    <dl>
        <dt>
            The Form Factor
        </dt>
        <dd>
            The next-generation iPhone will no doubt feature more advanced hardware technology
            than its predecessor, the iPhone 4S. Everyone is speculating wildly on what it will
            look like - the first rumor was a metal unibody made of exotic material named LiquidMetal.
            This guess was actually fairly close to the truth, considering Apple's acquisition
            of the rights to the material in 2010. Leaked photos, however suggest otherwise.
            Nothing is official at this moment, but here are a few new features that the high-end
            device might have:
            <dl>
                <dt>
                    Different design
                </dt>
                <dd>
                    The iPhone 5 may not have a LiquidMetal unibody, but leaked photos suggest it might
                    have a 100% aluminum back. <a href="http://www.macrumors.com/2012/05/29/claimed-rear-shell-with-sides-for-next-generation-iphone-surfaces/" target="_blank">MacRumors</a> 
                    have rounded up the images that show what the next iPhone 5 might
                    look like. The basic black and white versions are inevitable, but the backs panels
                    are now also two-toned. <a href="http://www.mobilefun.co.uk/blog/2012/06/iphone-5-case-image-leaks-confirm-final-design/" target="_blank">MobileFun</a>'s 
                    report reveals that the next-generation iPhone measures 7.6
                    mm thin, 123.83 mm high, and 58.57 mm wide.
              </dd>
                <dt>
                 	<div class="divPhotoR nonFirstPhoto">
                        <img src="/images/iPhone5Rumors/iPhone-5-Larger-Screen.jpg" alt="iPhone 5" width="360" height="298" />
					</div>
                    Thinner, taller touchscreen
                </dt>
                <dd>
                    <a href="http://online.wsj.com/article/SB10001424052702303754904577532121136436182.html" target="_blank">The Wall Street Journal</a> 
                    reports that the iPhone 5 will have a different
                    touchscreen technology that reduces thickness and improves image quality. The technology
                    integrates the touch sensors with the liquid crystal display, making the screen
                    thinner. The integration also saves Apple's production costs, as they no longer
                    have to have separate suppliers for each component. 
                    <a href="http://news.cnet.com/8301-13579_3-57435236-37/apple-orders-4-inch-iphone-screens-says-report/?tag=mncol;txt" target="_blank">CNET</a>
                    has reported that screen size will increase from 3.5 inches to 4 or
                    more. The iPhone 5 will, however, increase only in height. <a href target="_blank""http://9to5mac.com/2012/07/10/claimed-iphone-5-engineering-samples-show-physical-mockup-of-rumored-next-generation-iphone-design/#more-199749">9to5mac</a>'s 
                    leaked photo seems to support this speculation as well.
          </dd>
                <dt>
                    Magnetic, mini dock port
                </dt>
                <dd>
					<a href="http://techcrunch.com/2012/06/20/confirmed-the-new-iphone-will-have-a-19-pin-mini-connector/?fb_comment_id=fbc_10151072215241349_24489343_10151073522436349#ffc02d19c" target="_blank">TechCrunch</a> 
                    has confirmed that the next-generation iPhone will feature a
                    19-pin connector instead of the standard 30. The dock connector is as small as the
                    micro USB, as Apple plans to make its mobile devices as thin as possible. The same
                    report speculates that the connectors will be magnetic, just like the MagSafe connectors
                    used on MacBooks.
              </dd>
                <dt>
                    New spots for ports
                </dt>
                <dd>
                    Aside from the bigger speakers, microphone and the other internal hardware differences,
                    the iPhone 5's ports have also changed their locations. <a href="http://www.mobilefun.co.uk/blog/2012/06/iphone-5-case-image-leaks-confirm-final-design/" target="_blank">MobileFun</a>'s 
                    sourced images show that the audio port is now at the bottom.
                    9to5mac's leaked photos, on the other hand, show the FaceTime camera is located
                    at the center, on top of the earpiece.
                </dd>
            </dl>
            <dt>
                The Features
            </dt>
            <dd>
                <dl>
                    <dt>
                        Lightning-fast LTE
                    </dt>
                    <dd>
                        Apple's competitors are all featuring 4G/LTE technologies on their smartphones.
                        The iPhone is actually lagging behind in this category. The company's latest iPad
                        model, however, already has the super fast connectivity feature. It is only logical
                        for the iPhone to have this high-speed data support standard.
                    </dd>
                    <dt>
                        Convenient NFC
                    </dt>
                    <dd>
                        Flagship phones especially have this smartphone feature standard. Although we can
                        all be content with Wi-Fi connectivity, an additional way of transferring data and
                        making transactions would not hurt. A lot of phones already have this and Apple
                        may decide to include it on the iPhone 5's feature set.
                    </dd>
                </dl>
            </dd>
            <dt>The Functionality </dt>
            <dd>
                <dl>
                    <dt>
                 	<div class="divPhotoR nonFirstPhoto">
                        <img src="/images/iPhone5Rumors/iPhone-5-Production-Faceplate.jpg" alt="iPhone 5" width="360" height="327" />
					</div>
                        New nano-SIM card standard
                    </dt>
                    <dd>
                        The company paved the way for Micro-SIM card support with its iPhone 4S. It is,
                        thus, not surprising if Apple did have a nano-SIM support on their next-generation
                        iPhone. <a href="http://reviews.cnet.com/8301-19512_7-57473318-233/euro-carriers-stocking-nano-sim-cards-ahead-of-iphone-5-launch/" target="_blank">CNET</a>
                         also reports that European carriers are already stocking nano-SIM cards
                        in anticipation of Apple's autumn iPhone launch. The question would be whether the
                        other mobile phone companies will adapt to the new standard.
                  </dd>
                    <dt>
                        Improved processing speed
                    </dt>
                    <dd>
                        The iPhone 5 will definitely feature improved processing speed. It is not yet determined,
                        however, whether it will have a quad-core processor. Pertinent rumors point out
                        that the next-generation iPhone will have a dual-core instead. Whatever type of
                        chip the company will use, it better work smoothly. 9to5mac has reported that the
                        A5 chip is overheating, which caused a delay in iPhone's former expected June launch.
                    </dd>
                    <dt>
                        Introducing the iOS 6
                    </dt>
                    <dd>
                        If the speculated hardware modifications turn out to be relatively minor, it is
                        the iOS 6's slew of software features that will entice the iPhone's next generation
                        of users. With Apple's new operating system, you can use your iPhone as an electronic
                        pass for plane travel. You can now use Siri while you steer the wheel. Safari, social
                        networking integrations, and photo sharing also feature new functionalities. Of
                        all the speculations, this would most likely turn out to be true.
                    </dd>
                </dl>
            </dd>
            <dt>
                The Foresight
            </dt>
            <dd>
                <dl>
                    <dt class="empty"></dt>
                    <dd>
                        Apple seems to be breaking its long-standing traditions. From the event locations,
                        to the product naming, to the launch dates, everything has changed. We expect the
                        same with Apple's upcoming phone. In the end, it all makes for a sensible business
                        strategy.
                    </dd>
                    <dt>
                        No more number scheme
                    </dt>
                    <dd>
                        Although everyone is calling it 'iPhone 5,' we are almost certain that the company
                        will simply call it the 'iPhone'. Everyone was expecting the latest iPad model to
                        be dubbed as iPad 3. Apple, however, surprised us by dropping the number scheme.
                        The next-generation iPhone will most likely seal its iconic status by simple being
                        what it is ??an iPhone.
                    </dd>
                    <dt>
                        Arriving in autumn
                    </dt>
                    <dd>
                        Everyone is asking when the iPhone 5 is coming. If you ask us, we think it will
                        be in September or October. The iOS 6 Developer Beta 3 expires on September and
                        it will be timely to introduce the new software version along with the latest handset.
                        Apple's June-to-October transition last year translated to record sales. Autumn
                        is always a lucrative season because of the holiday demand for a new device. We
                        are sure Apple will not have a hard time repeating its performance from last year.
                    </dd>
                </dl>
            </dd>
        </dd>
    </dl>
</div>

<!--#include virtual="/includes/template/bottom.asp"-->
