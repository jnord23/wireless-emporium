<html>
<head>
<title>WirelessEmporium Spring Savings</title>
<%
dim baseUrl : baseUrl = "http://www.wirelessemporium.com"
%>
<base href="<%=baseUrl%>">
</head>
<%
dim imgLink 		: 	imgLink 		= 	baseUrl & "/images/email/blast/2011-04-20"
dim strParam 		: 	strParam 		= 	"utm_source=Email&utm_medium=Blast&utm_campaign=WESPRING10"
dim topNavStyle		:	topNavStyle		=	"font-family: Arial, Helvetica, sans-serif; font-size:14px; font-weight: bold; color:#787878; text-decoration: none;"
%>
<body>
<table width="710" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td width="100%" style="padding-bottom:5px;" align="right">
			<a href="<%=imgLink%>/blast.asp" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 9px; color: #777777;">Can't view this?</a>
		</td>
	</tr>
	<tr>
		<td width="100%" style="padding-bottom:5px;" align="left">
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td width="540" align="left">
						<a href="<%=baseUrl%>/?<%=strParam%>" target="_blank" style="text-decoration:none;">
							<img src="<%=imgLink%>/we_logo.jpg" border="0" width="445" height="66" />
						</a>
					</td>
					<td width="170" right="right">
						<a href="http://www.mshopper.net/mshopperv2/wap/MerchantOptIn.jsp?merchantId=537&optinsource=pcsite" target="_blank" style="text-decoration:none;">
							<img src="<%=imgLink%>/cellPhoneOffers.jpg" border="0" width="170" height="66" />
						</a>					
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%" style="padding:5px 0px 5px 0px; border-bottom:2px solid #ccc; border-top:2px solid #ccc;" align="left">
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">			
				<tr>
					<td align="center"><a target="_blank" href="http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp?<%=strParam%>" style="<%=topNavStyle%>">FACEPLATES</a></td>
					<td align="center"><a target="_blank" href="http://www.wirelessemporium.com/cell-phone-batteries.asp?<%=strParam%>" style="<%=topNavStyle%>">BATTERIES</a></td>
					<td align="center"><a target="_blank" href="http://www.wirelessemporium.com/cell-phone-chargers.asp?<%=strParam%>" style="<%=topNavStyle%>">CHARGERS</a></td>
					<td align="center"><a target="_blank" href="http://www.wirelessemporium.com/cell-phone-cases.asp?<%=strParam%>" style="<%=topNavStyle%>">CASES</a></td>
					<td align="center"><a target="_blank" href="http://www.wirelessemporium.com/bluetooth-headsets-handsfree.asp?<%=strParam%>" style="<%=topNavStyle%>">HEADSETS</a></td>
					<td align="center"><a target="_blank" href="http://www.wirelessemporium.com/cell-phone-holsters-belt-clips-holders.asp?<%=strParam%>" style="<%=topNavStyle%>">HOLSTERS/HOLDERS</a></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%" style="padding-top:10px;">
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr><td width="100%" colspan="5"><a target="_blank" href="<%=baseUrl%>/?<%=strParam%>" style="text-decoration:none;"><img src="<%=imgLink%>/promoTop.jpg" border="0" width="710" height="194" style="display:block;" /></a></td></tr>
				<tr>
					<td width="228" height="50"><a target="_blank" href="<%=baseUrl%>/?<%=strParam%>" style="text-decoration:none;"><img src="<%=imgLink%>/promoMid_1.jpg" border="0" width="228" height="50" style="display:block;" /></a></td>
					<td width="195" height="50">
						<table width="195" border="0" align="center" cellpadding="0" cellspacing="0">
							<tr><td width="195" height="14"><img src="<%=imgLink%>/button1_Top_a1.jpg" border="0" width="195" height="14" style="display:block;"/></td></tr>
							<tr><td valign="bottom" align="center" width="195" height="26" style="padding:0px 0px 0px 0px; background-color:#F77132; font-family: Arial, Helvetica, sans-serif; color:#FFFFFF; font-weight:bold; font-size:20px; letter-spacing:2px;">WESPRING10</td></tr>
							<tr><td width="195" height="10"><img src="<%=imgLink%>/button1_Bot_a1.jpg" border="0" width="195" height="10" style="display:block;"/></td></tr>
						</table>					
					</td>
					<td width="5" height="50">
						<table width="5" border="0" align="center" cellpadding="0" cellspacing="0">
							<tr><td width="5" height="14"><img src="<%=imgLink%>/button1_Top_a2.jpg" border="0" width="5" height="14" style="display:block;"/></td></tr>
							<tr><td width="5" height="26"><img src="<%=imgLink%>/button1_Mid_a2.jpg" border="0" width="5" height="26" style="display:block;"/></td></tr>
							<tr><td width="5" height="10"><img src="<%=imgLink%>/button1_Bot_a3.jpg" border="0" width="5" height="10" style="display:block;"/></td></tr>
						</table>
					</td>
					<td width="63" height="50"><img src="<%=imgLink%>/promoMid_or.jpg" border="0" width="63" height="50" style="display:block;" /></td>
					<td width="219" height="50"><a target="_blank" href="http://www.facebook.com/wirelessemporium/?<%=strParam%>" style="text-decoration:none;" rel="me"><img src="<%=imgLink%>/promoMid_last.jpg" border="0" width="219" height="50" style="display:block;" /></a></td>
				</tr>
				<tr><td width="100%" colspan="5"><a target="_blank" href="<%=baseUrl%>/?<%=strParam%>" style="text-decoration:none;"><img src="<%=imgLink%>/promoBottom.jpg" border="0" width="710" height="46" style="display:block;" /></a></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="710" height="33">
			<img src="<%=imgLink%>/essentialSpringBuys_section.jpg" border="0" width="710" height="33" style="display:block;" />
		</td>
	</tr>
	<tr>
		<td width="710">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="239" height="318"><a target="_blank" href="http://www.wirelessemporium.com/p-77028-3feet-universal-smartphone-pda-tablet-stand--lime-green-.asp?<%=strParam%>" style="text-decoration:none;"><img src="<%=imgLink%>/product1.jpg" border="0" width="239" height="318" style="display:block;" /></a></td>
					<td width="234" height="318"><a target="_blank" href="http://www.wirelessemporium.com/p-ms-1088932-john-lennon--new-york-city.asp?<%=strParam%>" style="text-decoration:none;"><img src="<%=imgLink%>/product2.jpg" border="0" width="234" height="318" style="display:block;" /></a></td>
					<td width="237" height="318"><a target="_blank" href="http://www.wirelessemporium.com/p-76972-aqua-skin-waterproof-full-body-protective-film-for-apple-iphone-4-verizon--2-pack-.asp?<%=strParam%>" style="text-decoration:none;"><img src="<%=imgLink%>/product3.jpg" border="0" width="237" height="318" style="display:block;" /></a></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="710" height="13" style="padding-top:20px;"><img src="<%=imgLink%>/connectWithUs_section.jpg" border="0" width="710" height="13" style="display:block;" /></td>
	</tr>	
	<tr>
		<td width="710" style="padding-bottom:5px;">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="239" height="125"><a target="_blank" href="http://www.facebook.com/wirelessemporium/?<%=strParam%>" style="text-decoration:none;" rel="me"><img src="<%=imgLink%>/facebook.jpg" border="0" width="239" height="125" style="display:block;" /></a></td>
					<td width="234" height="125"><a target="_blank" href="http://twitter.com/wirelessemp/?<%=strParam%>" style="text-decoration:none;"><img src="<%=imgLink%>/twitter.jpg" border="0" width="234" height="125" style="display:block;" /></a></td>
					<td width="237" height="125"><a target="_blank" href="http://www.wirelessemporium.com/blog/?<%=strParam%>" style="text-decoration:none;"><img src="<%=imgLink%>/blog.jpg" border="0" width="237" height="125" style="display:block;" /></a></td>
				</tr>
			</table>
		</td>
	</tr>	
	<tr>
		<td width="710" align="left" style="padding-bottom:5px; font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; font-weight:normal; text-decoration:none;">
			This email correspondence has been sent to you as you have indicated you would like to receive special, exclusive offers from WirelessEmporium.Com<br> 
			If this message has been sent in error, or you would no longer like to receive any more periodic offers from WirelessEmporium.Com, you may <a href="http://www.wirelessEmporium.com/unsubscribe.asp?strEmail=webmaster@wirelessemporium.com&<%=strParam%>" style="text-decoration:underline; font-size:10px;">click here</a> to unsubscribe.
		</td>
	</tr>
</table>
</body>
</html>