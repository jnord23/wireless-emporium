<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_Email_Validation.asp"-->
<%
pageTitle = "product.asp"

'Retrieve form values
cSubmitted = prepStr(request.form("submitted"))
cName = prepStr(request.form("name"))
cEmail = prepStr(request.form("email"))
cSubject = prepStr(request.form("subject"))
cPhone = prepStr(request.form("phonenumber"))
cProduct = prepStr(request.form("product"))
cOrderNo = prepStr(request.form("orderno"))
cQuestion =prepStr(request.form("question"))

verifyHuman = replace(prepStr(request("verifyHuman"))," ","")
verifyHumanCode = prepStr(request("verifyHumanCode"))

%>
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;Contact Us
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle" width="100%">
            <h1>Contact Us</h1>
        </td>
    </tr>
    <tr>
        <td width="748" align="center">
            <table border="0" cellspacing="0" cellpadding="0" width="748" align="center">
                <tr>
                    <td valign="top" class="breadcrumb" width="100%" align="center">
                        <%
						
						'Check existing verification code
						
						if verifyHuman = "" then verifyHuman = 0
						if verifyHumanCode = "" then verifyHumanCode = 0
						
						if left(verifyHumanCode,1) < 4 then
							vh_letter = "a"
						elseif left(verifyHumanCode,1) < 7 then
							vh_letter = "b"
						else
							vh_letter = "b"
						end if
						if right(verifyHumanCode,1) < 4 then
							vh_num = 1
						elseif left(verifyHumanCode,1) < 7 then
							vh_num = 2
						else
							vh_num = 3
						end if
						
						requiredCode = vh_letter & vh_num

                        if cSubmitted = "1" and verifyHuman = requiredCode then
												
							dim cdo_to, cdo_from, cdo_subject, cdo_body
							select case cSubject 
								case "Returns / Exchanges / Request RMA" : cdo_to = "returns@WirelessEmporium.com"
								case "Marketing Inquiries" : cdo_to = "marketing@WirelessEmporium.com"
								case "General Inquiries" : cdo_to = "sales@WirelessEmporium.com"
								case else : cdo_to = "service@WirelessEmporium.com"
							end select
							cdo_from = prepStr(cEmail)
							if IsValidEmail(cdo_from) = true then
								cdo_subject = "Contact Us"
								cdo_body = "<p><img src=""http://www.wirelessemporium.com/images/WElogo.gif"">" & "</p>" & vbcrlf
								cdo_body = cdo_body & "<p><b>Name: </b>" & cName & "</p>" & vbcrlf
								cdo_body = cdo_body & "<p><b>Phone: </b>" & cPhone & "</p>" & vbcrlf
								cdo_body = cdo_body & "<p><b>Type of Inquiry: </b><font color=""#FF0000"">" & cSubject & "</font></p>" & vbcrlf
								cdo_body = cdo_body & "<p><b>Order #: </b>" & cOrderNo & "</p>" & vbcrlf
								cdo_body = cdo_body & "<p><b>Product: </b>" & cProduct & "</p>" & vbcrlf
								cdo_body = cdo_body & "<p><b>Question: </b>" & replace(cQuestion,vbcrlf,"<br>") & "</p>" & vbcrlf
								cdo_body = cdo_body & "<p><b>Date/Time Sent: </b>" & now & "</p>" & vbcrlf
								cdo_body = cdo_body & "<p>&nbsp;</p><p>" & Request.ServerVariables("REMOTE_ADDR") & "</p>" & vbcrlf
								CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
								%>
								<p><b><br><br>Thank you for contacting WirelessEmporium.com!</b></p>
								<p><b>A Representative from the appropriate department will contact you promptly. If your inquiry is sent between normal business hours (8am-5pm PST M-F) expect an answer in the next few hours or within one business day.</b></p>
								<p><b><br><br>WirelessEmporium.com</b></p>
								<%
							else
								%>
								<p><b><br><br>The e-mail address you entered was not valid.</b></p>
								<p><b>Please <a href="javascript:history.back();">go back</a> and try again.</b></p>
								<p><b><br><br>WirelessEmporium.com</b></p>
								<%							
							end if
						else ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
							if not verifyHuman = requiredCode and cSubmitted = "1" then
								Message = "<p style=""color:red;font-weight:bold"">Wrong Verification Code</p>"
							end if
							
							'Create new verification code
							randomize
							verifyHumanCode = int(rnd*61565161)+1
							if left(verifyHumanCode,1) < 4 then
								vh_letter = "a"
							elseif left(verifyHumanCode,1) < 7 then
								vh_letter = "b"
							else
								vh_letter = "b"
							end if
							if right(verifyHumanCode,1) < 4 then
								vh_num = 1
							elseif left(verifyHumanCode,1) < 7 then
								vh_num = 2
							else
								vh_num = 3
							end if
							
							set fso = CreateObject("Scripting.FileSystemObject")
							Set Jpeg = Server.CreateObject("Persits.Jpeg")
							Set vhImg = Server.CreateObject("Persits.Jpeg")
							Jpeg.Quality = 100
							Jpeg.Interpolation = 1
							
							Jpeg.New 60, 30, &HFFFFFF
							vhImg.Open server.MapPath("/images/verifyHuman/" & vh_letter & ".gif")
							Jpeg.Canvas.DrawImage 0, 0, vhImg
							vhImg.Open server.MapPath("/images/verifyHuman/" & vh_num & ".gif")
							Jpeg.Canvas.DrawImage 30, 0, vhImg
							if not fso.fileExists(server.MapPath("/images/verifyHuman/results/" & verifyHumanCode & ".jpg")) then
								Jpeg.Save server.MapPath("/images/verifyHuman/results/" & verifyHumanCode & ".jpg")
							end if
							
								%>
								<script language="javascript1.2">
								function reset() {
									document.contactus.name.value="";
									document.contactus.email.value="";
									document.contactus.verifyHuman.value="";
									document.contactus.subject.value="";
									document.contactus.phonenumber.value="";
									document.contactus.product.value="";
									document.contactus.orderno.value="";
									document.contactus.question.value="";
								}
								function validateEmail(email) {
									var splitted = email.match("^(.+)@(.+)$");
									if (splitted == null) return false;
									if (splitted[1] != null) {
										var regexp_user=/^\"?[\w-_\.]*\"?$/;
										if (splitted[1].match(regexp_user) == null) return false;
									}
									if (splitted[2] != null) {
										var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
										if (splitted[2].match(regexp_domain) == null) {
											var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
											if (splitted[2].match(regexp_ip) == null) return false;
										}
										return true;
									}
									return false;
								}
								function validate() {
									if (document.contactus.name.value.length==0) {
										alert("Please Enter Your Name");
										return false;
									}
									if (document.contactus.subject.value=="Select here...") {
										alert("Please Select the Type of Inquiry");
										return false;
									}
									if (document.contactus.question.value.length==0) {
										alert("Please Enter Question");
										return false;
									}
									if (document.contactus.email.value.length==0) {
										alert("Please Enter Email Address");
										return false;
									}
									if (document.contactus.verifyHuman.value.length==0) {
										alert("Please Enter Verification Code");
										return false;
									}
									if (!validateEmail(document.contactus.email.value)) { 
										alert("Please Enter a Valid Email Address");
										return false;
									}
									document.contactus.submit()
									return false;
								}
								</script>
                                
								<table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td align="left" valign="top" class="static-content-font">
											<p align="justify">
												Have a question or comment about anything? Our Associates are standing by to assist.
												Please contact us by providing the pertinent information below and your inquiry will be
												addressed within one business day or less.
											</p>
										</td>
									</tr>
									<tr>
										<td align="left" valign="top" class="static-content-font">
											<p>&nbsp;</p>
                                            <%=Message%>
										</td>
									</tr>
									<tr>
										<td>
											<table width="400" border="0" align="left" cellspacing="0">
												<form onSubmit="return validate();" name="contactus" method="post" action="/contactus">
												<input type="hidden" name="submitted" value="1">
												<tr>
													<td align="left" class="content-bold-blue" width="120"><font size="2">Name:</font><font color="#FF0000">*</font></td>
													<td width="16" align="right">&nbsp;</td>
													<td>&nbsp;<input type="text" name="name" size="20" maxlength="100" value="<%=cName%>" autocomplete="off"></td>
												</tr>
												<tr>
													<td height="26" align="left" class="content-bold-blue"><font size="2">Email:</font><font color="#FF0000">*</font></td>
													<td width="16" align="right">&nbsp;</td>
													<td>&nbsp;<input type="text" name="email" size="20" maxlength="100" value="<%=cEmail%>" autocomplete="off"></td>
												</tr>
												<tr>
													<td align="left" class="content-bold-blue"><font size="2">Type of Inquiry:</font><font color="#FF0000">*</font></td>
													<td width="16" align="right">&nbsp;</td>
													<td>
														<select name="subject">
															<option value="" selected>Select here...</option>
															<option value="Sales Inquiries"<% if "Sales Inquiries" = cSubject then response.write " selected=""selected"" " %>>Sales Inquiries</option>
															<option value="Returns / Exchanges / Request RMA"<% if "Returns / Exchanges / Request RMA" = cSubject then response.write " selected=""selected"" " %>>Returns / Exchanges / Request RMA</option>
															<option value="Billing Inquiries"<% if "Billing Inquiries" = cSubject then response.write " selected=""selected"" " %>>Billing Inquiries</option>
															<option value="Marketing Inquiries"<% if "Marketing Inquiries" = cSubject then response.write " selected=""selected"" " %>>Marketing Inquiries</option>
															<option value="Other Customer Service Inquiries"<% if "Other Customer Service Inquiries" = cSubject then response.write " selected=""selected"" " %>>Other Customer Service Inquiries</option>
															<option value="General Inquiries"<% if "General Inquiries" = cSubject then response.write " selected=""selected"" " %>>General Inquiries</option>
														</select>
													</td>
												</tr>
												<tr>
													<td align="left" class="content-bold-blue"><font size="2">Phone #:</font></td>
													<td width="16" align="right">&nbsp;</td>
													<td>&nbsp;<input type="text" name="phonenumber" size="20" maxlength="20" value="<%=cPhone%>" autocomplete="off"></td>
												</tr>
												<tr>
													<td align="left" class="content-bold-blue"><font size="2">Product :</font></td>
													<td width="16" align="right">&nbsp;</td>
													<td>&nbsp;<input type="text" name="product" size="20" maxlength="20" value="<%=cProduct%>" autocomplete="off"></td>
												</tr>
												<tr>
													<td align="left" class="content-bold-blue"><font size="2">Order #:</font></td>
													<td width="16" align="right">&nbsp;</td>
													<td>&nbsp;<input type="text" name="orderno" size="8" maxlength="8" value="<%=cOrderNo%>" autocomplete="off"></td>
												</tr>
												<tr>
													<td align="left" valign="top" class="content-bold-blue"><font size="2">Question:</font><font color="#FF0000">*</font></td>
													<td width="16" align="right" valign="top">&nbsp;</td>
													<td>&nbsp;<textarea name="question" cols="25" rows="4" autocomplete="off"><%=cQuestion%></textarea></td>
												</tr>
												<tr>
												  <td align="left" class="content-bold-blue">&nbsp;</td>
												  <td align="right">&nbsp;</td>
												  <td>&nbsp;<img src='/images/verifyHuman/results/<%=verifyHumanCode%>.jpg' border='0' /></td>
												</tr>
												<tr>
												  <td align="left" class="content-bold-blue"><font size="2">Verification Code:</font></td>
												  <td align="right">&nbsp;</td>
												  <td>&nbsp;<input type="text" name="verifyHuman" value="" size="2" maxlength="2" /><input type="hidden" name="verifyHumanCode" value="<%=verifyHumanCode%>" autocomplete="off" /></td>
												</tr>
												<tr>
													<td colspan="3" align="center">
														<br><input type="submit" name="submit" value="Click Here To Send">
														<br>To ensure delivery of our emails to your inbox, please add service@WirelessEmporium.com to your Address Book. Thank You.
													</td>
												</tr>
												<tr>
													<td colspan="3" align="left">
														<br><b>To check the status of your order, please <a href="/orderstatus">click here</a>.</b>
													</td>
												</tr>
												</form>
											</table>
										</td>
									</tr>
								</table>
								<%
							end if
                        %>
                        <p>&nbsp;</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->