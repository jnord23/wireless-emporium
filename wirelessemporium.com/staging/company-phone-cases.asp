<%
	pageName = "CompanyPhoneCases"
	
	response.buffer = true
	noSitePadding = true
	noleftnav = 1
	nocommentbox = true
	mobileAccess = 1
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<%
dim strName, strCName, strPhone, strEmail
strName = prepStr(request.form("txtName"))
strCName = prepStr(request.form("txtCName"))
strPhone = prepStr(request.form("txtPhoneNumber"))
strEmail = prepStr(request.form("txtEmail"))

if strName <> "" and strCName <> "" and strPhone <> "" and strEmail <> "" then
	strBody = 	"Name: " & strName & "<br>" & vbcrlf & _
				"Company Name: " & strCName & "<br>" & vbcrlf & _
				"Phone Number: " & strPhone & "<br>" & vbcrlf & _
				"Email Address: " & strEmail & "<br>"
				
	set objErrMail = Server.CreateObject("CDO.Message")
	with objErrMail
		.From = "sales@wirelessemporium.com"
		.To = "ruben@wirelessemporium.com,wemarketing@wirelessemporium.com,wemerch@wirelessemporium.com,wedev@wirelessemporium.com,eugene@wirelessemporium.com,tony@wirelessemporium.com"
'		.To = "terry@wirelessemporium.com,ruben@wirelessemporium.com,wemarketing@wirelessemporium.com"
		.Subject = "Custom Business/Group Quotes"
		.HTMLBody = strBody
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Item("urn:schemas:httpmail:sender") = "sales@wirelessemporium.com"
		.Configuration.Fields.Update
		.Send
	end with
%>
<div style="float:left; width:958px; text-align:center; height:250px;">
	<div style="float:left; width:100%; font-weight:bold; font-size:16px; color:#333; text-align:center; padding-top:30px;">Thank you! Your request has been submitted!</div>
	<div style="float:left; width:100%; text-align:center; text-align:center; padding-top:30px;"><a href="/" style="text-decoration:underline; font-weight:bold; font-size:16px;">Continue Shopping</a></div>
</div>
<!-- Google Code for Lead Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1045494101;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "ors0COXwwAMQ1fLD8gM";
var google_conversion_value = 20;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1045494101/?value=20&amp;label=ors0COXwwAMQ1fLD8gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<%	
else
%>
<style>
#closeButton {
	text-transform:uppercase;
	position:absolute;
	right:-20px;
	top:0px;
	width:34px;
	height:34px;
	background:#2C2C2C;
	text-align:center;
	border-radius:20px;
	font:bold 16px/34px Verdana, Geneva, sans-serif;
	border:3px solid #FFF;
	cursor:pointer;
	color:#FFF;
	z-index:100;
	-webkit-box-shadow: -2px 2px 4px 0px #000; /* Saf3-4, iOS 4.0.2 - 4.2, Android 2.3+ */
	box-shadow: -2px 2px 4px 0px #000; /* Opera 10.5, IE9, FF4+, Chrome 6+, iOS 5 */
}
</style>
<script>
	function quoteSubmit() {
		var name = document.frmQuote.txtName.value;
		var companyname = document.frmQuote.txtCName.value;
		var phonenumber = document.frmQuote.txtPhoneNumber.value;
		var email = document.frmQuote.txtEmail.value;
		
		if (name == '') {
			alert('Name is required!');
			return false;
		}

		if (companyname == '') {
			alert('Company Name is required!');
			return false;
		}
		
		if (phonenumber == '') {
			alert('Phone Number is required!');
			return false;
		}
		
		if (email == '') {
			alert('Email is required!');
			return false;
		}
		
		return true;
	}

	function popupSubmit() {
		ajax("/ajax/ajaxCompanyCustomCase.asp",'popBox');
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	function closeFloatingBox() {
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
	}	
</script>
<div style="float:left; width:100%;">
	<div class="left" style="background:url(/images/custom/corp/main-banner1.jpg) no-repeat; width:978px; height:342px; position:relative;">
    	<div style="position:absolute; display:inline-block; top:210px; right:100px;">
        	<a href="javascript:popupSubmit();"><img src="/images/custom/corp/button-free-quote.jpg" border="0" alt="Get a Free Quote" /></a>
		</div>
    	<div style="position:absolute; display:inline-block; top:285px; right:150px; text-align:left;">
        	<span style="font-size:32px; font-weight:bold; font-style:italic; color:#666;">1-888-503-9790</span>
		</div>
    </div>
	<div class="left" style="width:958px; background-color:#e8e8e8; padding:30px 0px 30px 20px;">
    	<div class="left" style="width:528px; font-size:28px; color:#629A41;">Special discounted bulk rates available.</div>
    	<div class="left" style="width:430px; font-size:14px; color:#629A41; padding-top:10px;">- For more information please contact Us at 1-888-503-9790</div>
    </div>
	<div class="left" style="width:100%; padding:20px 0px 20px 0px;">
    	<div class="left" style="width:22%; height:280px; padding:12px 12px 12px 20px; border-right:1px solid #ccc;">
        	<div style="width:100%; float:left; text-align:center;"><img src="/images/custom/corp/icon-1.jpg" border="0" /></div>
        	<div style="width:100%; float:left; text-align:center; padding-top:10px;"><img src="/images/custom/corp/title-brand.jpg" border="0" /></div>            
        	<div style="width:100%; float:left; text-align:left; padding-top:10px; color:#555; line-height:18px;">
	            Our custom corporate cases are the perfect opportunity to showcase your company! Use your phone cover as a marketing opportunity to highlight your company's logo and brand. Utilize your phone as valuable real estate to advertise and promote your brand. 
            </div>
        </div>
    	<div class="left" style="width:22%; height:280px; padding:12px; border-right:1px solid #ccc;">
        	<div style="width:100%; float:left; text-align:center;"><img src="/images/custom/corp/icon-2.jpg" border="0" /></div>
        	<div style="width:100%; float:left; text-align:center; padding-top:10px;"><img src="/images/custom/corp/title-protection.jpg" border="0" /></div>            
        	<div style="width:100%; float:left; text-align:left; padding-top:10px; color:#555; line-height:18px;">
	            Our custom cases provide the quality impact-resistance protection that is guaranteed with all Wireless Emporium cases. Made from premium polycarbonate materials, our cases are guaranteed to protect your mobile device against scratches, bumps and drops. 
            </div>
        </div>
    	<div class="left" style="width:22%; height:280px; padding:12px; border-right:1px solid #ccc;">
        	<div style="width:100%; float:left; text-align:center;"><img src="/images/custom/corp/icon-3.jpg" border="0" /></div>
        	<div style="width:100%; float:left; text-align:center; padding-top:10px;"><img src="/images/custom/corp/title-perfect.jpg" border="0" /></div>            
        	<div style="width:100%; float:left; text-align:left; padding-top:10px; color:#555; line-height:18px;">
	            The cases are custom printed for each order, ensuring optimum quality control. Your design is printed directly on the cases, not stickered on, using innovative UV printing capabilities. This allows for us to produce your company logo in striking full-color images on a wide variety of cell phone cases.
            </div>
        </div>        
    	<div class="left" style="width:22%; height:280px; padding:12px;">
        	<div style="width:100%; float:left; text-align:center;"><img src="/images/custom/corp/icon-4.jpg" border="0" /></div>
        	<div style="width:100%; float:left; text-align:center; padding-top:10px;"><img src="/images/custom/corp/title-contact.jpg" border="0" /></div>            
        	<div style="width:100%; float:left; text-align:left; padding-top:10px; color:#555; line-height:18px;">
	            Have further questions? Need help with your design? We have a dedicated design team on-staff to assist with your every need. <br />Just call us at: <strong>1-888-503-9790</strong>
            </div>
        </div>        
    </div>
    <div class="left" style="margin-left:4px; background:url(/images/custom/corp/learntoheaderx.gif) repeat-x; height:62px; padding:20px 0px 0px 20px; width:950px; border-bottom:1px solid #e2e2e2; font-size:36px; color:#777; text-align:left;">
    	Learn how to work it?
    </div>
    <div class="left" style="width:100%; padding:20px 0px 20px 0px;">
    	<div class="left" style="width:55%; text-align:center;">
			<iframe width="480" height="270" src="http://www.youtube.com/embed/epi2_d2PhUA?rel=0&showinfo=0&modestbranding=1&wmode=opaque" frameborder="0" allowfullscreen></iframe>
        </div>
    	<div class="left" style="width:35%; padding-left:15px;">
	    	<div class="left" style="width:100%; text-align:left;"><img src="/images/custom/corp/title-amazing3d.jpg" border="0" /></div>
	    	<div class="left" style="margin-top:20px; width:100%; text-align:left; color:#666;">
	            How does our printer work? How quickly can we print your order? Check out this video to see how our printing process works, from beginning to end. 
		    	<div class="left" style="margin-top:20px; width:100%;">
			    	<div class="left" style="width:25px; text-align:left;"><img src="/images/custom/corp/checkmark.jpg" border="0" /></div>
			    	<div class="left" style="width:90%; text-align:left; color:#666;">Utilizes our innovative UV printer</div>
                </div>
		    	<div class="left" style="margin-top:10px; width:100%;">
			    	<div class="left" style="width:25px; text-align:left;"><img src="/images/custom/corp/checkmark.jpg" border="0" /></div>
			    	<div class="left" style="width:90%; text-align:left; color:#666;">Custom printed with each specific order</div>
                </div>
		    	<div class="left" style="margin-top:10px; width:100%;">
			    	<div class="left" style="width:25px; text-align:left;"><img src="/images/custom/corp/checkmark.jpg" border="0" /></div>
			    	<div class="left" style="width:90%; text-align:left; color:#666;">Crisp and stunning image of your company's logo</div>
                </div>
            </div>
        </div>
    </div>
</div>
<%
end if	
%>
<!--#include virtual="/includes/template/bottom.asp"-->
