<%
	response.buffer = true
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.Open Session("ConnectionString")
	
	partNumber = request.QueryString("partNumber")
	
	if not isnull(partNumber) and len(partNumber) > 0 then
		sql =	"exec sp_interviewTestProductDetails '" & partNumber & "'"
		set	productDetailsRS = oConn.execute(sql)
		
		if not productDetailsRS.EOF then
			itemPic = productDetailsRS("itemPic")
			itemDesc = productDetailsRS("itemDesc")
			price_our = formatCurrency(productDetailsRS("price_our"))
			bullet1 = productDetailsRS("bullet1")
			bullet2 = productDetailsRS("bullet2")
			bullet3 = productDetailsRS("bullet3")
			bullet4 = productDetailsRS("bullet4")
%>
<div class="fullContainer">
	<div class="fl bigImg"><img src="/productpics/big/<%=itemPic%>"></div>
    <div class="completeProdDetails">
    	<div class="fl md_row">
			<div class="fl md_title">PartNumber:</div>
			<div class="md_value"><%=partNumber%></div>
        </div>
        <div class="fl md_row">
			<div class="fl md_title">Name:</div>
			<div class="md_value"><%=itemDesc%></div>
        </div>
        <div class="fl md_row">
			<div class="fl md_title">Price:</div>
			<div class="md_value"><%=price_our%></div>
        </div>
        <div class="fl md_row">
			<div class="fl md_title">Details:</div>
			<div class="md_value">
				<ul>
                	<li><%=bullet1%></li>
                    <li><%=bullet2%></li>
                    <li><%=bullet3%></li>
                    <li><%=bullet4%></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<%
		else
			response.Write("Error pulling product details")
		end if
		
		response.End()
	end if
	
	sql =	"exec sp_interviewTestProductList"
	set	productListRS = oConn.execute(sql)
%>
<!DOCTYPE html>
<html>
<head>
	<title>WE Interview Test</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <style type="text/css">
		body {
			font-family:Verdana, Geneva, sans-serif;
		}
		
		.weLogo {
			width: 475px;
			height: 70px;
			background: url(http://www.wirelessemporium.com/productPics/stitch/brand_9.jpg) 0px 0px;
			margin:10px 0px 10px 0px;
			border-bottom:2px #000 dotted;
		}
		
		.productDetails {
			padding:5px;
			border:1px solid #000;
			margin:0px 5px 5px 0px;
			width:150px;
			cursor:pointer;
		}
		
		.productDetails:hover {
			background-color:#666;
			color:#FFF;
		}
		
		.quickDetail {
			font-size:9px;
			margin-top:3px;
		}
		
		.productImage {
			width:100px;
			height:100px;
		}
		
		.fullContainer {
			width:620px;
			height:300px;
		}
		
		.bigImg {
			width:300px;
			height:300px;
		}
		
		.completeProdDetails {
			width:300px;
			float:right;
			height:300px;
		}
		
		.md_row {
			width:300px;
			margin-bottom:5px;
		}
		
		.fl {
			float:left;
		}
		
		.md_title {
			font-weight:bold;
			font-size:12px;
			width:200px;
		}
		
		.md_value {
			font-size:11px;
			padding-left:10px;
			width:190px;
		}
	</style>
</head>
<body>
<div>
	<div class="weLogo"></div>
    <div id="fullProductDetails"></div>
    <div>
		<%
        do while not productListRS.EOF
			itemPic = productListRS("itemPic")
			partNumber = productListRS("partNumber")
			prodType = productListRS("typeName")
			vendorCode = productListRS("vendorCode")
			vendorPartNumber = productListRS("vendorPN")
        %>
        <div class="fl productDetails" onClick="fullDetails('<%=partNumber%>')">
        	<div class="productImage"><img src="/productpics/thumb/<%=itemPic%>"></div>
            <div class="quickDetail"><%=partNumber%></div>
            <div class="quickDetail"><%=prodType%></div>
            <div class="quickDetail"><%=vendorCode%> (<%=vendorPartNumber%>)</div>
        </div>
        <%
			productListRS.movenext
        loop
        %>
</div>
</body>
</html>
<script language="javascript">
	function fullDetails(partNumber) {
		//alert('test');
		ajax('/interviewTest.asp?partNumber=' + partNumber,'fullProductDetails')
	}
	
	function ajax(newURL,rLoc) {
		if (newURL.indexOf("?") > 0) {
			newURL = newURL + '&ajaxID=' + Math.floor(Math.random()*999999)
			
		}
		else {
			newURL = newURL + '?ajaxID=' + Math.floor(Math.random()*999999)
			//alert(newURL);
		}
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
			
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					var rVal = httpRequest.responseText
					alert(rVal);
					if (rVal == "") {
						//No data available
					}
					else if(document.getElementById(rLoc)) {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				}
			}
		};
		httpRequest.send(null);
	}
</script>