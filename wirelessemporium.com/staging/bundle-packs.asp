<%
	response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	call fOpenConn()

	sql = "select * from we_items where typeID = 22 and hideLive = 0 and inv_qty <> 0 order by flag1 desc"
	session("errorSQL") = sql
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1

%>
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <% if rs.EOF then %>
    <tr><td align="center" style="font-weight:bold; font-size:24px; padding-top:100px;">No Bundles Currently Available<br />Please Check Back Soon</td></tr>
    <%
    else
        do while not rs.EOF
    %>
    <tr>
        <%
        for i = 1 to 4
            if rs.EOF then
        %>
        <td>&nbsp;</td>
        <%
            else
        %>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="192" height="100%">
                <tr><td align="center" valign="middle" width="192" height="154"><a href="/p-<%=RS("itemid")%>-<%=formatSEO(RS("itemDesc"))%>.asp" title="<%=rs("itemDesc")%>"><img src="/productpics/thumb/<%=rs("itemPic")%>" border="0"></a></td></tr>
                <tr><td align="center" valign="top" height="5"><img src="/images/spacer.gif" width="1" height="5"></td></tr>
                <% if RS("inv_qty") = 0 then %>
                <tr><td align="center" valign="top" height="90" rowspan="2"><img src="/images/sold_out.png" width="85"></td></tr>
                <% else %>
                <tr><td align="center" valign="top" height="50"><a class="product-description5" href="/p-<%=RS("itemid")%>-<%=formatSEO(RS("itemDesc"))%>.asp" title="<%=rs("itemDesc")%>"><%=rs("itemDesc")%></a></td></tr>
                <tr>
                    <td align="center" valign="top" height="40">
                        <span class="boldText">Our&nbsp;Price:&nbsp;</span><span class="pricing-orange2"><%=formatCurrency(RS("price_Our"))%></span><br>
                        <span class="pricing-gray2">List&nbsp;Price:&nbsp;<s><%=formatCurrency(RS("price_Retail"))%></s></span>
                    </td>
                </tr>
                <% end if %>
            </table>
        </td>
        <%
                rs.movenext
            end if
        next
        %>
    </tr>
    <%
        loop
    end if

	call fCloseConn()
    %>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->