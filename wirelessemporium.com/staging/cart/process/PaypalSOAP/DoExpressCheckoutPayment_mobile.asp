<%
dim useHttps : useHttps = 1
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

function weEmail(weSubject,weBody,toEmail)
	set autoEmail = Server.CreateObject("CDO.Message")
	with autoEmail
		.From = "support@wirelessemporium.com"
		.To = toEmail
		.Subject = weSubject
		.HTMLBody = weBody
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Item("urn:schemas:httpmail:sender") = "support@wirelessemporium.com"
		.Configuration.Fields.Update
		.Send
	end with
end function

dim HTTP_REFERER
HTTP_REFERER = request.servervariables("HTTP_REFERER")
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_OnSale.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/cart/process/PaypalSOAP/Constants.asp"-->
<!--#include virtual="/includes/asp/inc_Webloyalty.asp"-->
<%
dim token, payerID, INVNUM
INVNUM = request.form("INVNUM")
token = request.form("token")
payerID = request.form("PayerID")

if INVNUM = "" or token = "" or payerID = "" then
	response.redirect "PaypalError.asp?ec=1001"
	response.end
end if

dim nShipType, intlShipping, strShiptype
nShipType = request.form("shiptype")
intlShipping = request.form("intlShipping")
select case nShipType
	case "2"
		if intlShipping = "1" then
			strShiptype = "USPS Priority Int'l"
		else
			'strShiptype = "USPS Priority Mail (2-3 days)"
			strShiptype = "USPS Priority"
		end if
	case "3" : strShiptype = "USPS Express"
	case "4" : strShiptype = "UPS Ground"
	case "5" : strShiptype = "UPS 3 Day Select"
	case "6" : strShiptype = "UPS 2nd Day Air"
	case else
		if intlShipping = "1" then
			strShiptype = "First Class Int'l"
		else
			strShiptype = "First Class"
		end if
		if nShipRate = "" then nShipRate = 0
end select

dim TAXAMT, SHIPPINGAMT, grandTotal, paymentAmount, nTotalQuantity
TAXAMT = request.form("TAXAMT")
SHIPPINGAMT = request.form("shipcost" & nShipType)
paymentAmount = request.form("paymentAmount")
nTotalQuantity = request.form("nTotalQuantity")
grandTotal = request.Form("grandTotal")

'dim fname, lname, sAddress1, sAddress2, sCity, sState, sZip, sCountry
fname = request.form("fname")
lname = request.form("lname")
sAddress1 = request.form("sAddress1")
sAddress2 = request.form("sAddress2")
sCity = request.form("sCity")
sState = request.form("sState")
sZip = request.form("sZip")
sCountry = request.form("SHIPTOCOUNTRYCODE")
sEmail = request.form("EMAIL")

dim SoapStr
SoapStr = "<?xml version=""1.0"" encoding=""utf-8"" ?>" & vbcrlf
SoapStr = SoapStr & "<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" & vbcrlf
SoapStr = SoapStr & "	<soap:Header>" & vbcrlf
SoapStr = SoapStr & "		<RequesterCredentials xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
SoapStr = SoapStr & "			<Credentials xmlns=""urn:ebay:apis:eBLBaseComponents"">" & vbcrlf
SoapStr = SoapStr & "				<Username>" & API_USERNAME & "</Username>" & vbcrlf
SoapStr = SoapStr & "				<Password>" & API_PASSWORD & "</Password>" & vbcrlf
SoapStr = SoapStr & "				<Signature>" & API_SIGNATURE & "</Signature>" & vbcrlf
SoapStr = SoapStr & "			</Credentials>" & vbcrlf
SoapStr = SoapStr & "		</RequesterCredentials>" & vbcrlf
SoapStr = SoapStr & "	</soap:Header>" & vbcrlf
SoapStr = SoapStr & "	<soap:Body>" & vbcrlf
SoapStr = SoapStr & "		<DoExpressCheckoutPaymentReq xmlns=""urn:ebay:api:PayPalAPI"">" & vbcrlf
SoapStr = SoapStr & "			<DoExpressCheckoutPaymentRequest>" & vbcrlf
SoapStr = SoapStr & "				<Version xmlns=""urn:ebay:apis:eBLBaseComponents"">" & API_VERSION & "</Version>" & vbcrlf
SoapStr = SoapStr & "				<DoExpressCheckoutPaymentRequestDetails xmlns=""urn:ebay:apis:eBLBaseComponents"">" & vbcrlf
SoapStr = SoapStr & "					<PaymentAction>Authorization</PaymentAction>" & vbcrlf
SoapStr = SoapStr & "					<Token>" & token & "</Token>" & vbcrlf
SoapStr = SoapStr & "					<PayerID>" & payerID & "</PayerID>" & vbcrlf
SoapStr = SoapStr & "					<PaymentDetails>" & vbcrlf
SoapStr = SoapStr & "						<OrderTotal currencyID=""USD"">" & grandTotal & "</OrderTotal>" & vbcrlf
SoapStr = SoapStr & "						<ItemTotal currencyID=""USD"">" & paymentAmount & "</ItemTotal>" & vbcrlf
SoapStr = SoapStr & "						<TaxTotal currencyID=""USD"">" & TAXAMT & "</TaxTotal>" & vbcrlf
SoapStr = SoapStr & "						<ShippingTotal currencyID=""USD"">" & SHIPPINGAMT & "</ShippingTotal>" & vbcrlf
SoapStr = SoapStr & "						<ShipToAddress>" & vbcrlf
SoapStr = SoapStr & "							<Name>" & fname & " " & lname & "</Name>" & vbcrlf
SoapStr = SoapStr & "							<Street1>" & sAddress1 & "</Street1>" & vbcrlf
SoapStr = SoapStr & "							<Street2>" & sAddress2 & "</Street2>" & vbcrlf
SoapStr = SoapStr & "							<CityName>" & sCity & "</CityName>" & vbcrlf
SoapStr = SoapStr & "							<StateOrProvince>" & sState & "</StateOrProvince>" & vbcrlf
SoapStr = SoapStr & "							<Country>" & sCountry & "</Country>" & vbcrlf
SoapStr = SoapStr & "							<PostalCode>" & sZip & "</PostalCode>" & vbcrlf
SoapStr = SoapStr & "						</ShipToAddress>" & vbcrlf
SoapStr = SoapStr & "					</PaymentDetails>" & vbcrlf
SoapStr = SoapStr & "				</DoExpressCheckoutPaymentRequestDetails>" & vbcrlf
SoapStr = SoapStr & "			</DoExpressCheckoutPaymentRequest>" & vbcrlf
SoapStr = SoapStr & "		</DoExpressCheckoutPaymentReq>" & vbcrlf
SoapStr = SoapStr & "	</soap:Body>" & vbcrlf
SoapStr = SoapStr & "</soap:Envelope>" & vbcrlf

'CREATE OBJECTS
dim objXMLDOC, objXMLDOM, oNode, strError
set objXMLDOC = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
set objXMLDOM = Server.CreateObject("Msxml2.DomDocument")
set oNode = Server.CreateObject("Microsoft.XMLDOM")

'MAKE THE CALL
objXMLDOC.open "POST", API_ENDPOINT, False
objXMLDOC.setRequestHeader "Content-Type", "text/xml"
session("errorSQL") = SoapStr
objXMLDOC.send(SoapStr)
objXMLDOM.async = false
objXMLDOM.LoadXML objXMLDOC.responseText

'PROCESS THE CALL
if objXMLDOM.parseError.errorCode <> 0 then
	strError = strError & "<h3>Parser error found.</h3>"
else
	'BEGIN CHECK FOR ERROR FROM PAYPAL
	set oNode = objXMLDOM.getElementsByTagName("Ack")
	if (not oNode is nothing) then
		session("errorSQL") = objXMLDOC.responseText
		if oNode.item(0).text = "Failure" then
			set oNode = objXMLDOM.getElementsByTagName("ShortMessage")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
			set oNode = objXMLDOM.getElementsByTagName("LongMessage")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
			set oNode = objXMLDOM.getElementsByTagName("ErrorCode")
			if (not oNode is nothing) then strError = strError & "<h3>" & oNode.item(0).text & "</h3>" & vbcrlf
		end if
	else
		strError = strError & "<h3>No data received.</h3>"
	end if
end if

if strError <> "" then
	weEmail "PayPal Error",strError & "<br />" & session("errorSQL"),"jonathan@wirelessemporium.com"
	response.redirect "PaypalError.asp?ec=1002&strError=" & strError
	response.end
else
	set oNode = objXMLDOM.getElementsByTagName("BillingAgreementID")
	if (not oNode is nothing) then
		BillingAgreementID = oNode.item(0).text
	end if

	' UPDATE DB WITH APPROVAL, SHIPPING, ETC.
	call fOpenConn()
	SQL = "UPDATE we_orders SET"
	SQL = SQL & " approved = -1,"
	SQL = SQL & " emailSent = null,"
	SQL = SQL & " extOrderNumber = '" & request.form("CorrelationID") & "',"
	SQL = SQL & " ordershippingfee = '" & formatNumber(SHIPPINGAMT,2) & "',"
	SQL = SQL & " orderTax = '" & TAXAMT & "',"
	SQL = SQL & " ordergrandtotal = '" & grandTotal & "',"
	SQL = SQL & " shiptype = '" & SQLquote(strShiptype) & "'"
	SQL = SQL & " WHERE orderid='" & INVNUM & "'"
	'response.write "<p>" & SQL & "</p>" & vbcrlf
	oConn.execute(SQL)
	
	SQL = "SELECT a.itemID, a.quantity, b.ItemKit_NEW FROM we_orderdetails a left join we_items b on a.itemID = b.itemID WHERE a.orderid = '" & INVNUM & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	dim nProdIdCheck, Quantity, KIT, decreaseSql
	do until RS.eof
		nProdIdCheck = RS("itemid")
		KIT = RS("ItemKit_NEW")
		nProdQuantity = RS("quantity")
		orderNumber = INVNUM
		
		sql = "SELECT a.PartNumber, b.itemID as masterID, b.inv_qty FROM we_items a left join we_items b on a.partNumber = b.partNumber and b.master = 1 WHERE a.itemID = '" & nProdIdCheck & "'"
		session("errorSQL") = sql
		set itemRS = oConn.execute(SQL)
		
		if not itemRS.EOF then
			nPartNumber = itemRS("PartNumber")
			masterID = itemRS("masterID")
			
			sql = "select inv_qty from we_Items where partNumber = '" & nPartNumber & "' and master = 1"
			session("errorSQL") = sql
			set invRS = oConn.execute(sql)
			
			if not invRS.EOF then
				curQty = prepInt(invRS("inv_qty"))
				if curQty > 0 then
					curQty = curQty - nProdQuantity
					'adjust inventory
					sql = "UPDATE we_Items set inv_qty = " & curQty &  " where PartNumber = '" & nPartNumber & "' AND master = 1"
					session("errorSQL") = sql
					oConn.execute(sql)
					'save inventory adjustment
					On Error Resume Next
					sql = "if not (select count(*) from we_invRecord where itemID = '" & masterID & "' and orderID = '" & OrderNumber & "') > 0 insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & masterID & "," & itemRS("inv_qty") & "," & nProdQuantity & "," & OrderNumber & ",0,'WE PayPal Customer Mobile Order','" & now & "')"
					session("errorSQL") = sql
					oConn.execute(sql)
					On Error Goto 0
					'set number of sales for select item
					sqlQuery = "UPDATE we_items SET numberOfSales = numberOfSales + " & nProdQuantity & " WHERE itemID = '" & nProdIdCheck & "'"
					oConn.Execute(SQL)
				end if
			end if
		end if
		RS.movenext
	loop
	
	SQL = "SELECT A.orderid, A.accountid, A.ordersubtotal, A.ordergrandtotal, B.fname, B.email FROM we_orders A INNER JOIN we_accounts B ON A.accountid=B.accountid WHERE A.orderid = '" & INVNUM & "'"
	'response.write "<p>" & SQL & "</p>" & vbcrlf
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	
	dim strRedirect
	if not RS.eof then
		'dim nAccountId, sPromoCode
		nAccountId = RS("accountID")
		sPromoCode = request.form("sPromoCode")
		
		SQL = "UPDATE we_accounts SET"
		SQL = SQL & " fname = '" & SQLquote(fname) & "',"
		SQL = SQL & " lname = '" & SQLquote(lname) & "',"
		SQL = SQL & " sAddress1 = '" & SQLquote(sAddress1) & "',"
		SQL = SQL & " sAddress2 = '" & SQLquote(sAddress2) & "',"
		SQL = SQL & " sCity = '" & SQLquote(sCity) & "',"
		SQL = SQL & " sState = '" & SQLquote(sState) & "',"
		SQL = SQL & " BillingAgreementID = '" & BillingAgreementID & "'"
		'SQL = SQL & " sZip = '" & SQLquote(sZip) & "',"
		'SQL = SQL & " sCountry = '" & SQLquote(sCountry) & "'"
		SQL = SQL & " WHERE accountid = '" & nAccountId & "'"
		'response.write "<p>" & SQL & "</p>" & vbcrlf
		oConn.execute SQL
		
		if request.form("chkOptMail") = "Y" then
			oConn.execute("sp_ModifyEmailOpt '" & SQLquote(RS("fname")) & "','" & SQLquote(RS("email")) & "','Y'")
		end if
		
		mobileSite = 1
		
		strRedirect = "https://m.wirelessemporium.com/cart/complete.asp?a=" &  RS("accountID") & "&o=" & RS("orderID") & "&d=" & RS("ordergrandtotal") & "&c=" & RS("ordersubtotal") & "&mobileOrder=1&pp=Y"
		'strRedirect = "http://staging.wirelessemporium.com/cart/complete.asp?a=" & RS("accountID") & "&o=" & RS("orderID") & "&d=" & RS("ordergrandtotal") & "&c=" & RS("ordersubtotal")
		
		nOrderId = INVNUM
		%>
		<!--include virtual="/includes/asp/inc_receipt.asp"-->
		<%
	else
		' RECORD NOT FOUND error message
		response.redirect "PaypalError.asp?ec=1003"
		response.end
	end if
	
	RS.close
	set RS = nothing
	call fCloseConn()
	
	if strRedirect <> "" then
		'CLEAN UP EVERYTHING
		set objXMLDOC = nothing
		set objXMLDOM = nothing
		set oNode = nothing
	
		response.redirect strRedirect
		response.end
	end if
end if

'CLEAN UP EVERYTHING
set objXMLDOC = nothing
set objXMLDOM = nothing
set oNode = nothing

function XMLencode(str)
	XMLencode = replace(replace(replace(replace(replace(str,"&","&amp;"),"<","&lt;"),">","&gt;"),"'","&apos;"),chr(34),"&quot;")
end function
%>
