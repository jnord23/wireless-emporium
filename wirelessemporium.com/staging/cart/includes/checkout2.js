	document.orderForm.email.focus();
	
	document.orderForm.subTotal.value = document.totalDetails.subTotal.value;
	document.orderForm.sWeight.value = document.totalDetails.totalWeight.value;
	document.orderForm.itemCnt.value = document.totalDetails.itemCnt.value;
	document.orderForm.grandTotal.value = document.totalDetails.grandTotal.value;
	document.orderForm.ssl_ProdIdCount.value = document.totalDetails.itemCnt.value;
	document.getElementById("additionalHiddenFields").innerHTML = document.getElementById("sourceHidenFields").innerHTML;
	
	var blockSubmit = "";
	var baseShippingOptions = document.getElementById("shippingOptions").innerHTML;
	var subTotal = parseFloat(document.orderForm.subTotal.value);
	var maxLap = 0;
	var mySession = document.orderForm.mySession.value;
	var firstErrorField = "";
	var testimonialNum = 1;
	var curWindowPos = 0;
	var scrollBeyond = 0;
	
	var rightContentSize = document.getElementById("floatingRightPane").offsetHeight;
	var leftContentSize = document.getElementById("leftContentArea").offsetHeight;
	
	var blockSlide = 1;
	if (rightContentSize > leftContentSize) { blockSlide = 1; }
	
	function activateBilling(checked) {
		if (checked) {
			document.getElementById("billingInfo").style.display = '';
		}
		else {
			document.getElementById("billingInfo").style.display = 'none';
		}
	}
	
	if (blockSlide == 0) {
		setTimeout("adjustRightDetails()",500)
	}
	else {
		fullSize = document.getElementById("allFormData").offsetHeight;
		document.getElementById("allFormData").style.height = rightContentSize + "px";
	}
	
	function adjustRightDetails() {
		var topBuffer = 315;
		if (window.scrollY == undefined) {
			scrollBeyond = document.body.parentNode.scrollTop - 315;
		}
		else {
			scrollBeyond = window.scrollY - 315;
		}
		if (scrollBeyond > 0) {
			setTimeout("slideRightPane(" + scrollBeyond + ")",20);
		}
		else {
			scrollBeyond = 0;
			var obj = document.getElementById("floatingRightPane");
			var objTop = parseInt(obj.style.top);
			if (objTop > 0) {
				setTimeout("slideRightPane(0)",20);
			}
		}
	}
	
	function slideRightPane(moveAmt) {
		//document.getElementById("testSpot").innerHTML = moveAmt
		var obj = document.getElementById("floatingRightPane");
		if (rightContentSize < leftContentSize) {
			maxPosition = leftContentSize - rightContentSize;
			var objTop = parseInt(obj.style.top);
			if (isNaN(objTop)) { objTop = 0; }
			if (moveAmt > maxPosition) { moveAmt = maxPosition; }
			if (objTop > moveAmt) {
				var newTop = objTop - 5;
				if (newTop < moveAmt) { newTop = moveAmt; }
				obj.style.top = newTop + "px";
				if (newTop > moveAmt) { setTimeout("slideRightPane(" + moveAmt + ")",20); }
			}
			else {
				var newTop = objTop + 5;
				if (newTop > moveAmt) { newTop = moveAmt; }
				obj.style.top = newTop + "px";
				if (newTop < moveAmt) { setTimeout("slideRightPane(" + moveAmt + ")",20); }
			}
		}
	}
	
	function userScrolled() {
		if (blockSlide == 0) {
			if (window.scrollY == undefined) {
				var testWindowPos = document.body.parentNode.scrollTop - 315;
			}
			else {
				var testWindowPos = window.scrollY - 315;
			}
			if (testWindowPos < 0) { testWindowPos = 0; }
			if (curWindowPos != testWindowPos) {
				curWindowPos = testWindowPos;
				adjustRightDetails()
			}
		}
		setTimeout("userScrolled()",500)
	}
	
	setTimeout("userScrolled()",500)
	
	function openFields() {
		if (validEmailAddress()) {
			document.getElementById("ccFormBlocker").style.display = 'none';
			document.orderForm.fname.focus();
			window.scroll(0,500);
		}
		else {
			testField('email','Please enter a valid email address');
			document.orderForm.email.focus();
		}
	}
	
	function validEmailAddress() {
		var email = document.orderForm.email.value;
		var atSym = email.indexOf("@");
		var dotSym = email.indexOf(".");
		if (atSym > 0 && dotSym > atSym) { return true; } else { return false; }
	}
	
	function clearField(field,dir,defaultVal) {
		if (document.getElementById("ccFormBlocker").style.display == '') {
			document.orderForm.email.select();
		}
		else {
			var obj = document.getElementById(field)
			if (dir == "f") {
				if (obj.value == defaultVal) {
					obj.className = obj.className.replace(' bogusEntry','');
					obj.value = '';
					obj.focus();
				}
			}
			else {
				if (obj.value == '') {
					obj.className = obj.className + ' bogusEntry';
					obj.value = defaultVal;
				}
			}
		}
	}
	
	function verifyFormData() {
		blockSubmit = "";
		firstErrorField = "";
		if (validEmailAddress()) {
			if (document.getElementById("ccFormBlocker").style.display == '') {
				openFields()
				return false;
			}
			else {
				testField('email','Please enter a valid email address');
				testField('fname','Please enter a valid first name','First Name');
				testField('lname','Please enter a valid last name','Last Name');
				testField('sAddress1','Please enter a valid address','123 Aloha Ln. Apt. #1');
				testField('sCity','Please enter a valid city','');
				testField('sState','Please enter a valid state','');
				testField('sZip','Please enter a valid zip code','12345');
				testField('phoneNumber','Please enter a valid phone number','1234567890');
				
				if (document.orderForm.sameaddress.checked) {
					testField('bFname','Please enter a valid first name','First Name');
					testField('bLname','Please enter a valid last name','Last Name');
					testField('bAddress1','Please enter a valid address','321 Aloha Ln. Unit B');
					testField('bCity','Please enter a valid city','');
					testField('bState','Please enter a valid state','');
					testField('bZip','Please enter a valid zip code','12345');
				}
				else {
					document.orderForm.bFname.value = document.orderForm.fname.value;
					document.orderForm.bLname.value = document.orderForm.lname.value;
					document.orderForm.bAddress1.value = document.orderForm.sAddress1.value;
					document.orderForm.bCity.value = document.orderForm.sCity.value;
					document.orderForm.bState.selectedIndex = document.orderForm.sState.selectedIndex;
					document.orderForm.bZip.value = document.orderForm.sZip.value;
				}
				
				testField('cc_cardOwner','Please enter a valid name on card','First & Last Name');
				testField('cardNum','Please enter a valid card number','1234123412341234');
				testField('secCode','Please enter a valid security code','000');
				testField('cc_month','Select valid Month','');
				testField('cc_year','Select valid year','');
				if (blockSubmit == "yes") { 
					if (firstErrorField != "") { eval("document.orderForm." + firstErrorField + ".focus()"); }
					return false; 
				}
				else {
					document.getElementById("submitTop").innerHTML = 'Placing Order<br />Please Wait...'
					document.getElementById("submitBottom").innerHTML = 'Placing Order<br />Please Wait...'
					document.orderForm.action = "/framework/processing.asp";
					document.orderForm.submit();
					return true;
				}
			}
		}
		else {
			fieldError('email','Please enter a valid email address');
			return false;
		}
	}
	
	function fieldError(field,errMsg) {
		if (firstErrorField == "") { firstErrorField = field }
		if (field == "cc_month" || field == "cc_year") {
			document.getElementById(field).className = "errorFieldSelect";
		}
		else {
			document.getElementById(field).className = "errorField";
		}
		document.getElementById(field + "Error").innerHTML = errMsg;
	}
	
	function testField(field,errMsg,dafaultValue) {
		var obj = document.getElementById(field);
		var curClass = obj.className;
		var curVal = eval("document.orderForm." + field + ".value");
		
		if (specialCharTest(curVal)) {
			fieldError(field,errMsg);
			blockSubmit = "yes";
		}
		else {
			if (document.getElementById(field).className == "errorField" || document.getElementById(field).className == "errorFieldSelect") {
				if (curVal == dafaultValue) {
					if (curClass.indexOf("Select") > -1) {
						document.getElementById(field).className = "checkoutEntryFormSelect bogusEntry";
					}
					else {
						document.getElementById(field).className = "checkoutEntryFormField bogusEntry";
					}
				}
				else {
					if (curClass.indexOf("Select") > -1) {
						document.getElementById(field).className = "checkoutEntryFormSelect";
					}
					else {
						document.getElementById(field).className = "checkoutEntryFormField";
					}
				}
				document.getElementById(field + "Error").innerHTML = "";
				curClass = obj.className;
			}
			if (curClass.indexOf(" bogusEntry") > 0 || curVal == "") {
				fieldError(field,errMsg);
				blockSubmit = "yes";
			}
		}
	}
	
	function getNewShipping() {
		document.getElementById('dumpZone').innerHTML = "";	
		zipcode 			=	document.orderForm.sZip.value;
		curShippingType		=	document.orderForm.shiptype.value;
		totalWeight			=	document.orderForm.sWeight.value;
		itemCnt				=	document.orderForm.itemCnt.value;
	
		updateURL = '/ajax/shippingDetails2.asp?zip=' + zipcode + '&weight=' + totalWeight + '&shiptype=' + curShippingType + '&nTotalQuantity=' + itemCnt + '&nSubTotal=' + subTotal + '&useFunction=upscode2';
		ajax(updateURL,'dumpZone');
		setTimeout("moreShippingOptions()",200);
	}
	
	function moreShippingOptions() {
		var dumpZone = document.getElementById("dumpZone").innerHTML;
		if (dumpZone == "") {
			setTimeout("moreShippingOptions()",200);
		}
		else {
			document.getElementById("shippingOptions").innerHTML = baseShippingOptions + dumpZone;
		}
	}
	
	function setActiveShip(shipOption) {
		for(i=0;i<7;i++) {
			if (i == 1) { i++; }
			if (document.getElementById("ship" + i + "Box") != null) {
				activeClass = document.getElementById("ship" + i + "Box").className;
				if (activeClass.indexOf(" inactiveShip") < 0) {
					document.getElementById("ship" + i + "Box").className = activeClass.replace(' activeShip',' inactiveShip');
				}
			}
		}
		activeClass = document.getElementById("ship" + shipOption + "Box").className;
		if (activeClass.indexOf(" activeShip") < 0) {
			document.getElementById("ship" + shipOption + "Box").className = activeClass.replace(' inactiveShip',' activeShip');
		}
		var newShipAmt = parseFloat(eval("document.orderForm.shipcost" + shipOption + ".value"));
		document.orderForm.shippingTotal.value = newShipAmt.toFixed(2);
		var curTax = parseFloat(document.orderForm.nCATax.value);
		document.orderForm.grandTotal.value = (subTotal + curTax + newShipAmt).toFixed(2);
		document.getElementById("shippingDiv").innerHTML = "$" + newShipAmt.toFixed(2);
		document.getElementById("grandTotalDiv").innerHTML = "$" + (subTotal + curTax + newShipAmt).toFixed(2);
		document.orderForm.shiptypeID.value = shipOption;
		if (shipOption == 0) {
			document.getElementById("curArrivalDate").innerHTML = document.totalDetails.arrivalDate4.value;
		}
		if (shipOption == 2) {
			document.getElementById("curArrivalDate").innerHTML = document.totalDetails.arrivalDate2.value;
		}
		if (shipOption == 3) {
			document.getElementById("curArrivalDate").innerHTML = document.totalDetails.arrivalDate1.value;
		}
		if (shipOption == 4) {
			document.getElementById("curArrivalDate").innerHTML = document.totalDetails.arrivalDate3.value;
		}
		if (shipOption == 5) {
			document.getElementById("curArrivalDate").innerHTML = document.totalDetails.arrivalDate3.value;
		}
		if (shipOption == 6) {
			document.getElementById("curArrivalDate").innerHTML = document.totalDetails.arrivalDate2.value;
		}
	}
	
	function updateItem(itemID) {
		activeForm = eval("document.itemForm_" + itemID);
		activeForm.submit();
	}
	
	function removeItem(itemID) {
		activeForm = eval("document.itemForm_" + itemID);
		activeForm.qty.value = 0;
		activeForm.submit();
	}
	
	function getCityState(zipCode,which) {
		maxLap = 0;
		document.getElementById("cityStateDump").innerHTML = ""
		ajax('/ajax/getCityState.asp?zipcode=' + zipCode,'cityStateDump')
		setTimeout("checkCityStateResult('" + which + "')",500)
	}
	
	function checkCityStateResult(which) {
		var result = document.getElementById("cityStateDump").innerHTML
		if (which == "bill") {
			document.getElementById("billCity").style.display = '';
			document.getElementById("billState").style.display = '';
		}
		else {
			document.getElementById("shipCity").style.display = '';
			document.getElementById("shipState").style.display = '';
		}
		if (result == "") {
			if (maxLap < 5) { setTimeout("checkCityStateResult()",500) }
			maxLap++
		}
		else {
			if (result != "none") {
				resultArray = result.split("##")
				cityName = resultArray[0]
				stateCode = resultArray[1]
				if (which == "bill") {
					document.orderForm.bCity.value = cityName;
					var stateOptions = document.orderForm.bState.length
					for (i=0;i<stateOptions;i++) {
						if (document.orderForm.bState.options[i].value == stateCode) {
							document.orderForm.bState.selectedIndex = i
						}
					}
				}
				else {
					document.orderForm.sCity.value = cityName;
					var stateOptions = document.orderForm.sState.length
					for (i=0;i<stateOptions;i++) {
						if (document.orderForm.sState.options[i].value == stateCode) {
							document.orderForm.sState.selectedIndex = i
							checkTax(stateCode)
						}
					}
				}
			}
		}
	}
	
	function checkTax(stateCode) {
		var taxValue = document.orderForm.taxValue.value;
		if (stateCode == "CA") {
			var taxAmt = parseFloat(document.orderForm.subTotal.value) * taxValue;
			var shipCost = document.orderForm.shippingTotal.value;
			var newTotal = parseFloat(document.orderForm.subTotal.value) + parseFloat(shipCost) + parseFloat(taxAmt);
			newTotal = newTotal.toFixed(2);
			taxAmt = taxAmt.toFixed(2);
			
			document.orderForm.nCATax.value = taxAmt;
			document.getElementById("taxDiv").innerHTML = "$" + taxAmt;
			document.orderForm.grandTotal = newTotal;
			document.getElementById("grandTotalDiv").innerHTML = "$" + newTotal;
		}
		else {
			var taxAmt = 0;
			var shipCost = document.orderForm.shippingTotal.value;
			var newTotal = parseFloat(document.orderForm.subTotal.value) + parseFloat(shipCost) + parseFloat(taxAmt);
			newTotal = newTotal.toFixed(2);
			taxAmt = taxAmt.toFixed(2);
			
			document.orderForm.nCATax.value = taxAmt;
			document.getElementById("taxDiv").innerHTML = "$" + taxAmt;
			document.orderForm.grandTotal = newTotal;
			document.getElementById("grandTotalDiv").innerHTML = "$" + newTotal;
		}
	}
	
	function subPayPal() {
		if (validEmailAddress()) {
			document.PaypalForm.submit();
		}
		else {
			testField('email','Please enter a valid email address');
			document.orderForm.email.focus();
		}
	}
	
	function getCustomerData(email) {
		ajax('/ajax/customerData.asp?email=' + email,'customerInfo')
		setTimeout("displayData(document.getElementById('customerInfo').innerHTML)",2000)
		ajax('/ajax/remarketing.asp?email=' + email + '&mySession=' + mySession + '&siteID=0','dumpZone')
	}
	
	function displayData(dataReturn) {
		if (dataReturn != "no data" && dataReturn != "") {
			openFields()
			document.getElementById("popUpBG").style.display = ''
			document.getElementById("popUpBox").style.display = ''
			var dataArray = dataReturn.split("@@")
			document.orderForm.fname.value = dataArray[0]
			document.getElementById("fname").className = document.getElementById("fname").className.replace(' bogusEntry','');
			document.orderForm.lname.value = dataArray[1]
			document.getElementById("lname").className = document.getElementById("lname").className.replace(' bogusEntry','');
			document.orderForm.sAddress1.value = dataArray[2] + " " + dataArray[3]
			document.getElementById("sAddress1").className = document.getElementById("sAddress1").className.replace(' bogusEntry','');
			document.orderForm.sCity.value = dataArray[4]
			if (document.orderForm.sState.selectedIndex == 0) {
				for (i=0;i<50;i++) {
					if (document.orderForm.sState.options[i].value == dataArray[5]) {
						document.orderForm.sState.selectedIndex = i
						checkTax(dataArray[5])
					}
				}
			}
			document.orderForm.sZip.value = dataArray[6]
			document.getElementById("sZip").className = document.getElementById("sZip").className.replace(' bogusEntry','');
			getNewShipping();
			getCityState(dataArray[6],'')
			document.orderForm.phoneNumber.value = dataArray[7]
			document.getElementById("phoneNumber").className = document.getElementById("phoneNumber").className.replace(' bogusEntry','');
			
			if (dataArray[2] != dataArray[8] || dataArray[6] != dataArray[12]) {
				document.orderForm.sameaddress.checked = true
				activateBilling(document.orderForm.sameaddress.checked);
				document.orderForm.bFname.value = dataArray[0]
				document.orderForm.bLname.value = dataArray[1]
				document.orderForm.bAddress1.value = dataArray[8] + " " + dataArray[9]
				document.orderForm.bCity.value = dataArray[10]
				for (i=0;i<50;i++) {
					if (document.orderForm.bState.options[i].value == dataArray[11]) {
						document.orderForm.bState.selectedIndex = i
					}
				}
				document.orderForm.bZip.value = dataArray[12]
				getCityState(dataArray[12],'bill')
			}
			document.orderForm.accountid.value = dataArray[13]
			document.orderForm.parentAcctID.value = dataArray[14]
		}
	}
	
	function learnMore() {
		if (document.getElementById("secCodeGraphicBox").style.display == '') {
			document.getElementById("secCodeGraphicBox").style.display = 'none';
		}
		else {
			document.getElementById("secCodeGraphicBox").style.display = '';
		}
	}
	
	function specialCharTest(testValue) {
		var iChars = "~`!$%^&*+=[]\\\';,/{}|\":<>?";
		var errorFound = 0;
		for (var i = 0; i < testValue.length; i++) {
			if (iChars.indexOf(testValue.charAt(i)) != -1) {
				errorFound = 1;
			}
		}
		if (errorFound == 0) {
			return false;
		} 
		else {
			return true;
		}
	}
	
	function testimonalChg(num) {
		document.getElementById("testimonialNum_" + testimonialNum).style.display = 'none';
		testimonialNum = testimonialNum + parseInt(num);
		if (testimonialNum < 1) { testimonialNum = 3; }
		if (testimonialNum > 3) { testimonialNum = 1; }
		document.getElementById("testimonialNum_" + testimonialNum).style.display = '';
	}
	
	function optionSelected(curID) {
		var curClass = document.getElementById(curID).className;
		document.getElementById(curID).className = curClass.replace(' bogusEntry','');
	}