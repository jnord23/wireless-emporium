<%
dim discountTotal
discountTotal = 0

'Free Bluetooth
if FreeBluetooth = 1 and nSubTotal - FreeBluetoothPrice >= FreeBluetoothMin then
	discountTotal = FreeBluetoothPrice
	nSubTotal = round(nSubTotal - FreeBluetoothPrice,2)
	if discountTotal > 0 then NoMorePromos = 1
end if

'Category Sale
if CategorySale = 1 then
	discountTotal = round(CategorySaleTotal * CategorySalePercent,2)
	nSubTotal = round(nSubTotal - (CategorySaleTotal * CategorySalePercent),2)
	if discountTotal > 0 then NoMorePromos = 1
end if

'Same Model Sale
if SameModelSale = 1 then
	dim aCount, bCount, cCount, holdModelID, SameModelSaleDiscount, myItemPrices, myItemArray
	bCount = 0
	SameModelSaleDiscount = 0
	myItemPrices = ""
'	call fOpenConn()
'	SQL = "SELECT A.itemID, A.qty, B.modelID, left(B.PartNumber,3) AS PartLeft3, B.price_Our FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID" & vbcrlf
'	SQL = SQL & " WHERE A.store = 0 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)" & vbcrlf
'	SQL = SQL & " AND (		left(B.PartNumber,3) IN ('" & replace(mid(SameModelTypeID,2,len(SameModelTypeID)-2),"|","','") & "')" & vbcrlf
'	SQL = SQL & "			OR" & vbcrlf
'	SQL = SQL & "			B.PartNumber IN ('" & replace(mid(SameModelTypeID2,14,len(SameModelTypeID2)-14),"|","','") & "')	)" & vbcrlf
'	SQL = SQL & " AND B.NoDiscount = 0" & vbcrlf
'	SQL = SQL & " ORDER BY B.price_Our"
	
	sql	=	"select	a.itemid, a.qty, b.modelid, left(b.partnumber,3) as partleft3" & vbcrlf & _
			"	,	case when b.partnumber like 'WCD%' and len(b.partnumber) > 10 then" & vbcrlf & _
			"				(select top 1 isnull(price_our, 0) + b.price_our from we_items where itemid = substring(b.partnumber, charindex('-', b.partnumber)+1, charindex('-', b.partnumber, 6) - charindex('-', b.partnumber)-1))" & vbcrlf & _
			"			else" & vbcrlf & _
			"				b.price_our" & vbcrlf & _
			"		end price_our" & vbcrlf & _
			"from	shoppingcart a join we_items b " & vbcrlf & _
			"	on	a.itemid=b.itemid" & vbcrlf & _
			"where	a.store = 0 and a.sessionid = '" & mySession & "' and (a.purchasedorderid is null or a.purchasedorderid = 0)" & vbcrlf & _
			"		and (	left(b.partnumber,3) in ('" & replace(mid(SameModelTypeID,2,len(SameModelTypeID)-2),"|","','") & "')" & vbcrlf & _
			"				or" & vbcrlf & _
			"				b.partnumber in ('" & replace(mid(SameModelTypeID2,14,len(SameModelTypeID2)-14),"|","','") & "'))" & vbcrlf & _
			"	and b.nodiscount = 0" & vbcrlf & _
			"order by b.price_our"
	
	set RS = Server.CreateObject("ADODB.Recordset")
'	response.write "<pre>" & sql & "</pre>"
	RS.open SQL, oConn, 0, 1
	totalQty = 0
	if not RS.eof then
		holdModelID = RS("modelID")
		do until RS.eof
			totalQty = totalQty + RS("qty")
			' SECTION COMMENTED OUT TO TEMPORARILY GET RID OF MODEL-SPECIFIC CRITERIA
			' UNCOMMENT THIS TO GET BACK TO NORMAL MODEL SALE
			'if RS("modelID") <> holdModelID then
				'myItemArray = split(left(myItemPrices,len(myItemPrices) - 1),",")
				'if uBound(myItemArray) > 0 then
					'for cCount = 0 to int(bCount / 2) - 1
						'SameModelSaleDiscount = SameModelSaleDiscount + (myItemArray(cCount) * SameModelSalePercent)
						'response.write "<p>SameModelSaleDiscount = " & SameModelSaleDiscount & " | " & myItemArray(cCount) & " | bCount = " & bCount & " | " & int(bCount / 2) - 1 & "</p>"
					'next
				'end if
				'bCount = 0
				'myItemPrices = ""
				'holdModelID = RS("modelID")
			'end if
			for aCount = 1 to RS("qty")
				myItemPrices = myItemPrices & RS("price_Our") & ","
				bCount = bCount + 1
			next
			RS.movenext
		loop
		myItemArray = split(left(myItemPrices,len(myItemPrices) - 1),",")
		if uBound(myItemArray) > 0 then
			for cCount = 0 to int(bCount / 2) - 1
				SameModelSaleDiscount = SameModelSaleDiscount + (myItemArray(cCount) * SameModelSalePercent)
				'response.write "<p>SameModelSaleDiscount = " & SameModelSaleDiscount & " | " & myItemArray(cCount) & " | bCount = " & bCount & " | " & int(bCount / 2) - 1 & "</p>"
			next
		end if
	end if
	RS.close
	set RS = nothing
'	call fCloseConn()
	discountTotal = SameModelSaleDiscount
	nSubTotal = round(nSubTotal - SameModelSaleDiscount,2)
	if discountTotal > 0 then NoMorePromos = 1
end if

' APPLY COUPON DISCOUNT
if couponid <> 0 then
	if NoMorePromos = 0 then
		if oneTime then
			discountTotal = setValue
		elseif nSubTotal >= promoMin then
'			call fOpenConn()
			SQL = "SELECT A.itemID, A.qty, B.modelID, B.typeID, isnull(b.subtypeid, 0) subtypeid, B.PartNumber, B.itemDesc, B.itemPic, B.HandsfreeType, B.price_Our, B.price_retail, B.itemWeight FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID "
			SQL = SQL & " WHERE A.store = 0 AND A.sessionID = '" & mySession & "' "
			if nOrderId = "" then 'inc_promoFunctions.asp is being called by the cart or checkout pages
				SQL = SQL & "AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0) "
				'response.write "null"
				'response.write nOrderId
			else 'inc_promoFunctions.asp is being called by a report for an order that has been assigned an ID
				SQL = SQL & "AND A.purchasedOrderID = '" & nOrderId & "' "
				'response.write "not null"
				'response.write nOrderId
			end if
			SQL = SQL & " AND A.customCost is null"
			SQL = SQL & " AND B.NoDiscount = 0"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 0, 1
			do until RS.eof
				nProdIdCheck = RS("itemID")
				nProdQuantity = RS("qty")
				if cStr(typeID) <> "0" or BOGO = true then
					if cStr(RS("typeID")) = "16" then
						if inStr(couponDesc,"(*This coupon code cannot be applied to cell phone purchases)") = 0 then
							couponDesc = couponDesc & "<br>(*This coupon code cannot be applied to cell phone purchases)"
						end if
					else
						dim typeIDarray
						typeIDarray = split(typeID,",")
						for a = 0 to uBound(typeIDarray)
							if excludeBluetooth = true and RS("HandsfreeType") = 2 then
								if inStr(couponDesc,"(*Bluetooth products excluded)") = 0 then
									couponDesc = couponDesc & "<br>(*Bluetooth products excluded)"
								end if
							else
								if cStr(RS("typeID")) = cStr(typeIDarray(a)) or cStr(RS("subtypeid")) = cStr(typeIDarray(a)) or cStr(typeIDarray(a)) = "99" then
									if BOGO = true then
										discountTotal = discountTotal + (RS("price_Our") * int(nProdQuantity/2) * promoPercent)
									else
										discountTotal = discountTotal + (RS("price_Our") * nProdQuantity * promoPercent)
									end if
								else
									SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
									SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_Our > 0)"
									SQL = SQL & " AND (R.typeid = '" & cStr(typeIDarray(a)) & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
									SQL = SQL & " AND I.NoDiscount = 0"
									'response.write "<p>cStr(RS(typeID)) = " & cStr(RS("typeID")) & " | SQL = " & SQL & "</p>"
									set RSrelated = Server.CreateObject("ADODB.Recordset")
									RSrelated.open SQL, oConn, 0, 1
									if not RSrelated.eof then
										if BOGO = true then
											discountTotal = discountTotal + (RS("price_Our") * int(nProdQuantity/2) * promoPercent)
										else
											discountTotal = discountTotal + (RS("price_Our") * nProdQuantity * promoPercent)
										end if
									end if
									RSrelated.close
									set RSrelated = nothing
								end if
							end if
						next
					end if
				elseif not isNull(FreeItemPartNumber) then
					if FreeItemPartNumber = RS("PartNumber") and FreeProductWithPhonePrice = 0 and PhonePurchased = 1 then
						FreeProductWithPhonePrice = RS("price_Our")
					end if
				elseif (		(cBool(couponid = 55815) or cBool(couponid = 56517) or cBool(couponid = 70314)) and inStr(left(lcase(RS("PartNumber")),4),"wcd-") = 0		) then 'WECUSTOM25 | WECUSTOM10: Coupon code but no applicable partNumber
				'elseif cBool(cStr(couponid) = "55815") and inStr(left(lcase(RS("PartNumber")),4),"wcd-") > 0 then 'WECUSTOM25
				'elseif (cBool(couponid = 55815) and inStr(left(lcase(RS("PartNumber")),4),"wcd-") > 0) or (cBool(couponid = 56517) and inStr(left(lcase(RS("PartNumber")),4),"wcd-") > 0) then 'WECUSTOM25
				'****save****elseif not (		inStr(left(lcase(RS("PartNumber")),4),"wcd-") > 0  and (cBool(couponid = 55815) or cBool(couponid = 56517))		) then 'WECUSTOM25 or WECUSTOM10
					'response.write "{cBool(couponid = 55815):" & cBool(couponid = 55815) & "}"
					'response.write "{couponid  is 55815:" & cStr((couponid) = "55815") & "}"
					'response.write "{couponid  is 55815:" & cInt(couponid) = 55815 & "}"
					'response.write "{:inStr(left(lcase(RS(""PartNumber"")),4),""wcd-""))" & cStr(RS("PartNumber")) & cStr(inStr(left(lcase(RS("PartNumber")),4),"wcd-") > 0) & "}"
					'''BadInstrUse'''response.write "{please:" & Cstr(		not (cBool(cStr(couponid) = "55815") and cBool(inStr(left(lcase(RS("PartNumber")),4),"wcd-")))		) & "}"
					''response.write "{please:" & Cstr(		not (cBool(cStr(couponid) = "55815") and inStr(left(lcase(RS("PartNumber")),4),"wcd-") > 0 )		) & "}"
					'response.write "<br />"
					'response.write "here"
					'response.end
					couponDesc = couponDesc & "<br>(*This coupon code can only be applied to Custom Case Designs)"
				elseif cStr(RS("typeID")) = "16" then
					if inStr(couponDesc,"(*This coupon code cannot be applied to cell phone purchases)") = 0 then
						couponDesc = couponDesc & "<br>(*This coupon code cannot be applied to cell phone purchases)"
					end if
				else
					if excludeBluetooth = true and RS("HandsfreeType") = 2 then
						if inStr(couponDesc,"(*Bluetooth products excluded)") = 0 then
							couponDesc = couponDesc & "<br>(*Bluetooth products excluded)"
						end if
					else
						discountTotal = discountTotal + (promoPercent * (nProdQuantity * RS("price_Our")))
					end if
				end if
				RS.movenext
			loop
			RS.close
			set RS = nothing
'			call fCloseConn()
			if FreeProductWithPhonePrice > 0 then discountTotal = FreeProductWithPhonePrice
			session("errorSQL") = nSubTotal & " - " & discountTotal
			if discountTotal > 0 then
				nSubTotal = round(nSubTotal - discountTotal,2)
			else
				'Now that all discounts have been added together, if no discount could be applied, the coupon is rejected.  Another coupon entry could be attempted.
				sPromoCode = ""
'				session("promocode") = ""
				response.Cookies("promocode") = ""
				response.Cookies("promocode").Expires = DateAdd("d", -1, now)
			end if
		else
			couponDesc = "Coupon code cannot be applied to this order - minimum order value not reached."
		end if
	else
		couponDesc = "Coupon code cannot be applied to this order - another discount has already been applied."
	end if
end if

dim sGiftCert
if request.form("gift") <> "" then
	sGiftCert = SQLquote(request.form("gift"))
else
	sGiftCert = session("gift")
end if
if sGiftCert <> "" then
	'GIFT CERTIFICATE CODE SUBMITTED, CHECK IF IT EXISTS
'	call fOpenConn()
	SQL = "SELECT * FROM GiftCertificates WHERE GiftCertificateCode = '" & sGiftCert & "' AND dateUsed IS NULL AND store = 0"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	dim nGiftCertAmount
	if not RS.eof then
		nGiftCertAmount = RS("amount")
	else
		nGiftCertAmount = 0
	end if
	if nSubTotal - nGiftCertAmount < 0 then nGiftCertAmount = nSubTotal
'	call fCloseConn()
end if

discountTotal = discountTotal
%>
