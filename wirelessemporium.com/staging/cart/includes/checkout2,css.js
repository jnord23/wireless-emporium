	.checkoutTitleBar 				{ padding-bottom:5px; width:800px; margin:30px auto; border-bottom:1px solid #CCC; }
	.checkoutTitleText 				{ font-size:28px; margin-right:15px; font-weight:bold; }
	.checkoutTitleLock 				{ margin-right:130px; padding-top:5px; }
	.checkoutTitleInactiveBubble	{ background-color:#CCC; width:25px; height:23px; padding-top:2px; text-align:center; border-radius:15px; color:#FFF; font-size:16px; }
	.checkoutTitleInactiveText		{ color:#CCC; margin:2px 10px 0px 5px; font-size:16px; font-weight:bold; }
	.checkoutTitleInactiveText2		{ color:#CCC; margin:2px 0px 0px 5px; font-size:16px; font-weight:bold; }
	.checkoutTitleActiveBubble		{ background-color:#ff6600; width:25px; height:23px; padding-top:2px; font-weight:bold; text-align:center; border-radius:15px; color:#FFF; font-size:16px; }
	.checkoutTitleActiveText		{ color:#000; font-weight:bold; font-size:16px; margin:2px 10px 0px 5px; }
	
	.checkoutContentMain			{ width:800px; margin:0px auto; position:relative; }
	.checkoutContentLeft			{ width:450px; margin-right:30px; }
	.ccBlocker						{ width:450px; height:840px; background-color:#fff; position:absolute; top:191px; left:0px; opacity:0.6; filter:alpha(opacity=60); }
	.payWithCC						{ cursor:pointer; }
	
	.checkoutEntryBox				{ width:448px; border:1px solid #CCC; }
	.checkoutEntryBoxNext			{ width:448px; border:1px solid #CCC; margin-top:15px; }
	.checkoutEntryTitle				{ background-color:#999; font-weight:bold; border-bottom:1px solid #CCC; width:428px; color:#fff; padding:10px; }
	.checkoutEntryTitleText			{ font-size:18px; }
	.checkoutEntryRequired			{ font-size:12px; font-style:italic; font-weight:bold; margin-top:3px; }
	.checkoutEntryContent			{ background-color:#f7f7f7; padding:20px; width:408px; }
	.checkoutEntryText				{ font-weight:bold; font-size:14px; margin:6px 20px 0px 0px; width:120px; text-align:right; }
	.checkoutEntryFormField			{ width:260px; height:28px; border:1px solid #CCC; }
	.checkoutEntryFormFieldSml		{ width:127px; height:28px; border:1px solid #CCC; }
	.checkoutEntryFormSelect		{ width:225px; height:28px; }
	.checkoutEntryLine				{ width:100%; }
	.checkoutEntryNextLine			{ width:100%; margin-top:15px; }
	.bogusEntry						{ color:#CCC; }
	
	.billingOption					{ text-align:center; font-size:14px; }
	.checkoutEntryFormSelect		{ width:127px; height:28px; border:1px solid #CCC; padding-top:4px; }
	.paymentCcMonth					{ margin-right:10px; }
	.ccImg							{ margin-left:140px; }
	.secCodeLearn					{ color:#006699; text-decoration:underline; cursor:pointer; }
	.secCodeLink					{ margin:7px 0px 0px 10px; }
	.secure128						{ border:1px solid #CCC; padding:5px 24px; width:400px; height:33px; margin-top:15px; }
	.grayLock						{ margin-top:10px; }
	.secure128Text					{ margin:10px 0px 0px 10px; font-size:14px; }
	.shipRadio						{ padding-right:5px; }
	.shipTitle						{ font-size:14px; width:170px; padding-top:2px; text-align:left; }
	.shipTime						{ font-size:14px; width:130px; padding-top:2px; }
	.shipAmt						{ font-size:14px; text-align:right; width:60px; padding-top:2px; }
	.activeShip						{ border:1px solid #CCC; background-color:#ffffcc; padding:2px 3px 4px 3px; }
	.inactiveShip					{ padding:2px 3px 4px 3px; }
	
	.checkoutContentRight			{ position:relative; }
	.floatingRightDetails			{ position:absolute; top:0px; left:0px; }
	.placeOrderTop					{ margin-bottom:15px; text-align:center; width:318px; }
	.orderSummaryBox				{ width:318px; border:1px solid #CCC; }
	.orderSummaryTitle				{ background-color:#999; font-weight:bold; border-bottom:1px solid #CCC; width:298px; color:#fff; padding:10px; }
	.orderSummaryContent			{ width:318px; }
	
	.itemRow						{ border-bottom:1px solid #CCC; width:278px; padding:20px; }
	.orderSummaryItemPic			{ width:100px; height:100px; }
	.orderSummaryItemDesc			{ width:165px; margin-left:10px; }
	.orderSummaryItemName			{ font-weight:bold; font-size:12px; margin-bottom:10px; line-height:16px; }
	.orderSummaryItemPrice			{ margin-bottom:10px; }
	.priceTitleBold					{ font-weight:bold; width:90px; text-align:right; margin-right:10px; padding-top:4px; }
	.priceAmtBold					{ font-weight:bold; font-size:18px; color:#cc0000; text-align:right; width:60px; }
	.priceAmtBoldMulti				{ font-weight:bold; font-size:12px; text-align:right; width:60px; padding-top:4px; }
	.priceTitle						{ width:90px; text-align:right; margin-right:10px; }
	.priceAmt						{ text-align:right; width:60px; text-decoration:line-through; }
	.orderSummaryStock				{ width:100%; padding-top:10px; }
	.orderSummaryStockLine			{ font-size:11px; margin-left:5px; }
	
	.orderSummaryQtyLine			{ margin-top:10px; }
	.qtyTitle						{ font-weight:bold; font-size:16px; margin-right:10px; padding-top:4px; }
	.qtyField						{ height:28px; border:1px solid #CCC; width:35px; text-align:center; }
	.osUpdateQty					{ color:#006699; text-decoration:underline; font-size:14px; cursor:pointer; margin-left:25px; padding-top:8px; }
	.osSplitQty						{ margin:0px 10px; font-size:16px; padding-top:4px; }
	.osRemoveQty					{ color:#006699; text-decoration:underline; font-size:14px; cursor:pointer; padding-top:8px; }
	
	.orderTotals					{ border-bottom:1px solid #CCC; width:278px; padding:20px 20px 0px 20px; }
	.orderTotalRow					{ margin-bottom:10px; }
	.orderTotalTitle				{ width:200px; text-align:right; font-size:14px; }
	.orderTotalAmt					{ width:75px; text-align:right; font-weight:bold; font-size:14px; }
	.orderTotalAmtRed				{ width:75px; text-align:right; color:#cc0000; font-weight:bold; font-size:14px; }
	
	.grandTotalRow					{ width:278px; padding:20px; }
	.grandTotalTitle				{ font-weight:bold; font-size:16px; width:200px; text-align:right; }
	.grandTotalAmt					{ font-weight:bold; font-size:16px; color:#cc0000; width:75px; text-align:right; }
	
	.greenShipBox					{ border:2px solid #a2bb97; background-color:#e3fadf; color:#457730; padding:8px; width:300px; line-height:20px; font-size:12px; text-align:left; margin-top:10px; }
	
	.bottomPlaceOrder				{ width:269px; height:52px; margin:20px auto; }
	.bottomTrustBox					{ width:800px; margin:30px auto; border-top:1px solid #CCC; padding-top:20px; }
	.btb_needHelp					{ font-weight:bold; font-size:16px; margin-bottom:10px; }
	.btb_number						{ font-size:14px; }
	.bottomLinks					{ width:100%; }
	.linkBreak						{ margin:0px 5px; }
	.copywriteLine					{ width:100%; margin-top:10px; }
	
	.fieldBox						{ position:relative; }
	.fieldBoxError					{ position:absolute; right:0px; top:-15px; color:#ff7700; }
	.errorField						{ border:1px solid #ff7700; background:url(/images/checkout/icons/errorIcon.gif) no-repeat right; background-color:#ffeac8; width:260px; height:28px; }