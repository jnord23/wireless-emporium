<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_301Redirect.asp"-->
<%
	response.buffer = true
	
	pageName = "brand-sub"
	brandid = 9
	subbrandid = prepInt(request.QueryString("subBrandID"))
	cbSort = prepStr(request.form("cbSort"))

	if cbSort = "" then cbSort = "MP"
	select case ucase(cbSort)
		case "MP"
			sqlOrder = " order by numberOfSales desc"
		case "N"
			sqlOrder = " order by a.releaseyear desc, a.releasequarter desc, a.modelname"
		case "O"
			sqlOrder = " order by a.releaseyear, a.releasequarter, a.modelname"	
		case "AZ"
			sqlOrder = " order by a.modelname"
		case "ZA"
			sqlOrder = " order by a.modelname desc"
	end select

	set fs = CreateObject("Scripting.FileSystemObject")
	
	response.Cookies("saveBrandID") = brandID
	response.Cookies("saveModelID") = 0
	response.Cookies("saveItemID") = 0
	
	leftGoogleAd = 1
	googleAds = 1
	lap = 0
	
'	sql	=	"select	a.modelid, a.modelname, a.modelimg, a.carriercode" & vbcrlf & _
'			"	,	a.topModel, a.oldModel, a.international, b.brandname, b.brandimg " & vbcrlf & _
'			"	,	c.subBrandName_WE" & vbcrlf & _
'			"from 	we_models a join we_brands b" & vbcrlf & _
'			"	on	a.brandid = b.brandid join we_subbrands c" & vbcrlf & _
'			"	on	a.subbrandid = c.subbrandid" & vbcrlf & _
'			"where 	a.hidelive = 0" & vbcrlf & _
'			"	and	a.isTablet = 0" & vbcrlf & _
'			"	and	c.subbrandid = '" & subbrandid & "'" & vbcrlf & _
'			"	and	a.international <> 1 " & vbcrlf & _

	sql	=	"select	a.modelid, a.modelname, a.modelimg, a.carriercode" & vbcrlf & _
			"	,	a.topModel, a.oldModel, a.international, b.brandname, b.brandimg " & vbcrlf & _
			"	,	c.subBrandName_WE, a.releaseyear, a.releasequarter, isnull(sum(e.quantity), 0) numberOfSales" & vbcrlf & _
			"from 	we_models a join we_brands b" & vbcrlf & _
			"	on	a.brandid = b.brandid join we_subbrands c" & vbcrlf & _
			"	on	a.subbrandid = c.subbrandid join we_items d" & vbcrlf & _
			"	on	a.modelid = d.modelid left outer join we_orderdetails e" & vbcrlf & _
			"	on	d.itemid = e.itemid and e.entryDate >= dateadd(dd, -14, getdate())" & vbcrlf & _
			"where 	a.hidelive = 0" & vbcrlf & _
			"	and	a.isTablet = 0" & vbcrlf & _
			"	and	c.subbrandid = '" & subbrandid & "'" & vbcrlf & _
			"	and	a.international <> 1 " & vbcrlf & _
			"	and	d.hidelive = 0 and d.inv_qty <> 0" & vbcrlf & _
			"group by a.modelid, a.modelname, a.modelimg, a.carriercode" & vbcrlf & _
			"	,	a.topModel, a.oldModel, a.international, b.brandname, b.brandimg " & vbcrlf & _
			"	,	c.subBrandName_WE, a.releaseyear, a.releasequarter" & vbcrlf & _
			sqlOrder
			
	session("errorSQL") = sql
	arrPhones = getDbRows(sql)

	if isnull(arrPhones) then response.redirect("/") end if
	
	sql	=	"select	carriername, carriercode" & vbcrlf & _
			"from	we_carriers" & vbcrlf & _
			"where	carriercode in ('VW','CG','SN','TM')" & vbcrlf & _
			"order by case when carriercode = 'VW' then 1 " & vbcrlf & _
			"			when carriercode = 'CG' then 2" & vbcrlf & _
			"			when carriercode = 'SN' then 3" & vbcrlf & _
			"			when carriercode = 'TM' then 4" & vbcrlf & _
			"			else 5" & vbcrlf & _
			"		end"
	session("errorSQL") = sql
	arrCarriers = getDbRows(sql)			

	SEtitle = "Samsung Galaxy Accessories: Galaxy Accessories, Covers, Cases & Batteries"
	SEdescription = "Looking for Samsung Galaxy accessories? Wireless Emporium carries the best Galaxy Accessories like Covers, Cases, Batteries, Chargers & more all with Free Shipping!"
	SEkeywords = "galaxy accessories, galaxy s accessories, galaxy s2 accessories, galaxy covers, galaxy cases"
	strH1 = "Galaxy Smartphone Accessories"
	strH2 = "Galaxy cases and covers"
	
	session("breadcrumb_model") = ""
%>
<!--#include virtual="/includes/template/top_brand.asp"-->

<script src="/includes/js/jquery-1.6.2.min.js"></script>    
<link rel="stylesheet" type="text/css" href="/includes/css/slides.css?v=20120530" />    
<script src="/includes/js/slides/slides.min.jquery.js"></script>
<script>
window.WEDATA.pageType = 'subBrand';
window.WEDATA.pageData = {
	brand: '<%= brandName %>',
	brandId: '<%= brandId %>',
	subBrand: '<%= subBrandName %>',
	subBrandId: '<%= subBrandId %>'
};

	$(function(){
		$('#latest').slides({
			preload: true,
			preloadImage: '/images/preloading.gif',
			generatePagination: false,
			prev: 'big-arrow-left',
			next: 'big-arrow-right'
		});

		$('#topselling').slides({
			preload: true,
			preloadImage: '/images/preloading.gif',
			generatePagination: false,
			prev: 'big-arrow-left',
			next: 'big-arrow-right'
		});		
	});
</script>   
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
	<tr><td class="breadcrumbFinal"><a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;Galaxy Accessories</td></tr>
    <tr>
    	<td width="100%" style="padding:15px 0px 0px 5px;">
			<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
            	<tr>
                	<td width="100%" align="left" valign="top" style="padding:10px 0px 10px 0px;">
                    	<div style="float:left; width:500px; height:230px;">
                          	<div>
                            	<div style="float:left; height:40px;"><img src="/images/brand-sub/<%=subBrandID%>/logo.gif" border="0" width="106" height="35" alt="<%=strH1%>" title="<%=strH1%>" /></div>
                            	<div style="height:40px;"><h1 style="padding-left:10px; font-size:24px; color:#343434; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;"><%=strH1%></h1></div>
                            </div>
                            <div style="font-size:15px; color:#666666; line-height:150%; padding-top:10px;">Experience the premium entertainment and innovative features that make Samsung Galaxy mobile devices truly brilliant.</div>
                            <div style="padding-top:30px;">
                            	<div><img src="/images/brand-sub/<%=subBrandID%>/select-your-phone-below.png" border="0" width="434" title="Select Your Samsung Galaxy Phone Below" alt="Select Your Samsung Galaxy Phone Below" /></div>
                                <div style="padding-top:5px; font-size:14px; color:#343434; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;"> 
                                	<div style="float:left; padding:3px 10px 0px 0px;">Sort By:</div>
                                    <div style="float:left;">
										<form name="frmSort" method="post">
                                            <select name="cbSort" onChange="document.frmSort.submit();">
                                                <option value="MP" <%if cbSort = "MP" then%> selected <%end if%>>Most Popular</option>
                                                <option value="N" <%if cbSort = "N" then%> selected <%end if%>>Newest</option>
                                                <option value="O" <%if cbSort = "O" then%> selected <%end if%>>Oldest</option>
                                                <option value="AZ" <%if cbSort = "AZ" then%> selected <%end if%>>A-Z</option>
                                                <option value="ZA" <%if cbSort = "ZA" then%> selected <%end if%>>Z-A</option>
                                            </select>
                                        </form>
                                    </div>
                                </div>
							</div>
                        </div>
                    	<div style="height:230px;"><img src="/images/brand-sub/<%=subBrandID%>/banner_right.png" border="0" width="230" height="198" alt="<%=strH1%>" title="<%=strH1%>" /></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td width="100%">
			<table border="0" align="left" cellspacing="0" cellpadding="0" width="100%">
			<%
            for i=0 to ubound(arrCarriers,2)
                carriername = optCarrierName(arrCarriers(0,i))
                carriercode = arrCarriers(1,i)
                strHeader = carriername & " Samsung Galaxy Smartphones"
                %>
            	<tr>
                	<td width="100%" align="left" valign="top" style="<%if i=0 then%>border-top:1px solid #ccc;<%end if%> border-bottom:1px solid #ccc;">
						<table border="0" align="left" cellspacing="0" cellpadding="0" width="100%">
                        	<tr>
                            	<td align="left" valign="middle" width="30%" style="padding:5px;"><img src="/images/brand-sub/carrier_logo_<%=carriercode%>.png" border="0" alt="<%=strHeader%>" title="<%=strHeader%>"/></td>
                                <td align="left" valign="middle" width="70%" style="font-size:24px; font-style:italic; color:#343434; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;"><%=strHeader%></td>
                            </tr>
						</table>
					</td>
				</tr>
                <tr>
					<td width="100%" align="left" valign="top" style="border-bottom:1px solid #ccc;">
						<table border="0" align="left" cellspacing="0" cellpadding="0" width="100%">
                        	<tr>
							<%
                            lap = 0
                            for j=0 to ubound(arrPhones,2)
                                if instr(arrPhones(3,j), carriercode) > 0 then
                                    lap = lap + 1
                
                                    if lap > 5 then 
                                        lap = 1
                                        response.write "</tr><tr><td colspan=""5"" style=""border-top:1px solid #ccc; height:1px;""></td></tr><tr>"
                                    end if
                                    
                                    modelid = arrPhones(0,j)
                                    modelimg = arrPhones(2,j)
                                    brandName = arrPhones(7,j)
                                    modelName = arrPhones(1,j)
                                    altTag = replace(modelName,"/"," / ") & " accessories"
                                    %>
                                    <td style="<%if lap < 5 then%>border-right:1px solid #ccc;<%end if%> width:138px; padding:5px;" valign="top">
                                        <div style="width:138px;" align="center"><a href="/<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>-accessories" title="<%=altTag%>"><img src="/productPics/models/thumbs/<%=modelimg%>" border="0" alt="<%=altTag%>" title="<%=altTag%>" /></a></div>
                                        <div style="padding-top:5px; width:138px;" align="center"><a class="cellphone2-link" href="/<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>-accessories" title="<%=altTag%>"><%=replace(modelName,"/"," / ") & " Accessories"%></a></div>
                                    </td>
                                    <%
                                end if
                            next
							if lap = 4 then response.write "<td style=""width:138px; padding:5px;"">&nbsp;</td>"
							if lap = 3 then response.write "<td style=""width:138px; padding:5px;"">&nbsp;</td><td style=""width:138px; padding:5px;"">&nbsp;</td>"
							if lap = 2 then response.write "<td style=""width:138px; padding:5px;"">&nbsp;</td><td style=""width:138px; padding:5px;"">&nbsp;</td><td style=""width:138px; padding:5px;"">&nbsp;</td>"
							if lap = 1 then response.write "<td style=""width:138px; padding:5px;"">&nbsp;</td><td style=""width:138px; padding:5px;"">&nbsp;</td><td style=""width:138px; padding:5px;"">&nbsp;</td><td style=""width:138px; padding:5px;"">&nbsp;</td>"
                            %>
                            </tr>
						</table>
					</td>
				</tr>
			<%
            next
			strHeader = "Other Samsung Galaxy Smartphones"
            %>
            	<tr>
                	<td width="100%" align="left" valign="top" style="<%if i=0 then%>border-top:1px solid #ccc;<%end if%> border-bottom:1px solid #ccc;">
						<table border="0" align="left" cellspacing="0" cellpadding="0" width="100%">
                        	<tr>
                            	<td align="left" valign="middle" width="30%" style="padding:5px;"><img src="/images/brand-sub/carrier_logo_other.png" border="0" alt="<%=strHeader%>" title="<%=strHeader%>"/></td>
                                <td align="left" valign="middle" width="70%" style="font-size:24px; font-style:italic; color:#343434; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;"><%=strHeader%></td>
                            </tr>
						</table>
					</td>
				</tr>
                <tr>
					<td width="100%" align="left" valign="top" style="border-bottom:1px solid #ccc;">
						<table border="0" align="left" cellspacing="0" cellpadding="0" width="100%">
                        	<tr>
							<%
                            lap = 0
                            for j=0 to ubound(arrPhones,2)
                                if instr(arrPhones(3,j), "AL,") > 0 or instr(arrPhones(3,j), "MP,") > 0 or instr(arrPhones(3,j), "PP,") > 0 or instr(arrPhones(3,j), "BM,") > 0 or instr(arrPhones(3,j), "CK,") > 0 or instr(arrPhones(3,j), "US,") > 0 or instr(arrPhones(3,j), "UN,") > 0 then
                                    lap = lap + 1
                
                                    if lap > 5 then 
                                        lap = 1
                                        response.write "</tr><tr><td colspan=""5"" style=""border-top:1px solid #ccc; height:1px;""></td></tr><tr>"
                                    end if
                                    
                                    modelid = arrPhones(0,j)
                                    modelimg = arrPhones(2,j)
                                    brandName = arrPhones(7,j)
                                    modelName = arrPhones(1,j)
                                    altTag = replace(modelName,"/"," / ") & " accessories"
                                    %>
                                    <td style="<%if lap < 5 then%>border-right:1px solid #ccc;<%end if%> width:138px; padding:5px;" valign="top">
                                        <div style="width:138px;" align="center"><a href="/<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>-accessories" title="<%=altTag%>"><img src="/productPics/models/thumbs/<%=modelimg%>" border="0" alt="<%=altTag%>" title="<%=altTag%>" /></a></div>
                                        <div style="padding-top:5px; width:138px;" align="center"><a class="cellphone2-link" href="/<%=formatSEO(brandName) & "-" & formatSEO(modelName)%>-accessories" title="<%=altTag%>"><%=replace(modelName,"/"," / ") & " Accessories"%></a></div>
                                    </td>
                                    <%
                                end if
                            next
							if lap = 4 then response.write "<td style=""width:138px; padding:5px;"">&nbsp;</td>"
							if lap = 3 then response.write "<td style=""width:138px; padding:5px;"">&nbsp;</td><td style=""width:138px; padding:5px;"">&nbsp;</td>"
							if lap = 2 then response.write "<td style=""width:138px; padding:5px;"">&nbsp;</td><td style=""width:138px; padding:5px;"">&nbsp;</td><td style=""width:138px; padding:5px;"">&nbsp;</td>"
							if lap = 1 then response.write "<td style=""width:138px; padding:5px;"">&nbsp;</td><td style=""width:138px; padding:5px;"">&nbsp;</td><td style=""width:138px; padding:5px;"">&nbsp;</td><td style=""width:138px; padding:5px;"">&nbsp;</td>"
                            %>
                            </tr>
						</table>
					</td>
				</tr>                
			</table>
        </td>
	</tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td align="center" style="padding-bottom:20px;"><a href="/samsung-phone-accessories" title="View All <%=strH1%>"><img src="/images/brand-sub/<%=subBrandID%>/view-all-model.png" border="0" alt="View All <%=strH1%>" title="View All <%=strH1%>" /></a></td></tr>
    <%
	sql	=	"select	b.typeid, b.itemid, b.itempic, b.itemdesc, b.price_retail, b.price_our" & vbcrlf & _
			"from	(" & vbcrlf & _
			"		select	a.subtypeid, max(a.itemid) itemid" & vbcrlf & _
			"		from	we_items a with (nolock) join we_models b" & vbcrlf & _
			"			on	a.modelid = b.modelid" & vbcrlf & _
			"		where	a.hidelive = 0" & vbcrlf & _
			"			and	a.inv_qty <> 0" & vbcrlf & _
			"			and	b.subBrandid = 1" & vbcrlf & _
			"			and	a.itempic is not null and a.itempic <> ''" & vbcrlf & _
			"			and	a.subtypeid is not null" & vbcrlf & _
			"			and	b.isTablet = 0" & vbcrlf & _
			"		group by a.subtypeid" & vbcrlf & _
			"		) a join we_items b" & vbcrlf & _
			"	on	a.itemid = b.itemid"
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1	
	%>
    <tr>
    	<td width="100%" class="iphone-header-box" align="left">
			&nbsp;&nbsp;<h2 style="font-size:18px; font-weight:bold; color:#fff;">Latest Samsung Galaxy Accessories</h2>
		</td>
	</tr>
    <tr>
        <td align="left" width="100%">
            <div style="width:748px; height:230px; position:relative;">
                <div id="latest">
                    <div class="big-arrow-left" style="margin-top:50px;"></div>
                    <div style="float:left; width:678px; height:220px; padding:5px;">                        
                        <div class="slides_container">
                            <div class="slide">
							<%
							a = 0
							do until rs.eof
								a = a + 1
								itemid = rs("itemid")
								altText = rs("itemdesc")
								itemdesc = rs("itemDesc")
								if len(itemdesc) > 63 then itemdesc = left(itemdesc, 60) & "..."
								price_retail = formatcurrency(rs("price_retail"),2)
								price_our = formatcurrency(rs("price_our"),2)
								productLink = "/p-" & itemid & "-" & formatSEO(itemdesc)
								itempic = rs("itempic")
								%>
                                <div class="item" align="center">
                                    <div style="width:149px; padding-top:2px;" align="center">
                                    	<a href="<%=productLink%>" title="<%=altText%>"><div style="background: url(/productpics/thumb/<%=itemPic%>) 0px -0px no-repeat; width:100px; height:100px;" title="<%=altText%>"></div></a>
                                    </div>
                                    <div style="width:129px; padding:5px 10px 0px 10px;" align="left"><a href="<%=productLink%>" class="cellphone-link" title="<%=altText%>"><h3 style="font-size:12px;"><%=itemdesc%></h3></a></div>
                                    <div style="width:129px; padding:5px 10px 0px 10px;" align="left" class="index-price-retail">Was <del><%=price_retail%></del></div>
                                    <div style="width:129px; padding:0px 10px 0px 10px;" align="left" class="index-price-our">Our Price <%=price_our%></div>
                                    <div style="width:129px; padding:0px 10px 0px 10px;" align="left"><b>Save <%=formatPercent((price_Retail - price_Our) / price_Retail,0)%></b></div>
                                </div>
                                <%
								if ((a mod 4) = 0) and (a < 12) then
									response.write "</div>" & vbcrlf & "<div class=""slide"">"
								end if
								if a >= 12 then exit do
								rs.movenext
							loop
							%>
                            </div>
                        </div>
                    </div>
                    <div class="big-arrow-right" style="margin-top:50px;"></div>
                </div>
            </div>
        
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <%
	sql	=	"select	top 12 x.itemid, x.itempic, x.itemdesc, x.price_retail, x.price_our" & vbcrlf & _
			"from	we_subtypes a cross apply" & vbcrlf & _
			"		(	select	top 1 b.itemid, b.itempic, b.itemdesc, b.price_retail, b.price_our" & vbcrlf & _
			"			from	we_items b join we_models c" & vbcrlf & _
			"				on	b.modelid = c.modelid" & vbcrlf & _
			"			where	b.subtypeid = a.subtypeid" & vbcrlf & _
			"				and	c.subbrandid = 1" & vbcrlf & _
			"				and	b.itempic is not null and b.itempic <> '' and b.price_our > 0" & vbcrlf & _
			"				and	c.isTablet = 0" & vbcrlf & _
			"			order by b.numberOfSales desc	) x" & vbcrlf & _
			"where	a.hidelive = 0"
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1	
	%>
    <tr>
    	<td width="100%" class="iphone-header-box" style="font-size:18px; font-weight:bold; color:#fff;" align="left">
			&nbsp;&nbsp;<h2 style="font-size:18px; font-weight:bold; color:#fff;">Top Selling Samsung Galaxy Accessories</h2>
		</td>
	</tr>
    <tr>
        <td align="left" width="100%">
            <div style="width:748px; height:230px; position:relative;">
                <div id="topselling">
                    <div class="big-arrow-left" style="margin-top:50px;"></div>
                    <div style="float:left; width:678px; height:220px; padding:5px;">                        
                        <div class="slides_container">
                            <div class="slide">
							<%
							a = 0
							do until rs.eof
								a = a + 1							
								itemid = rs("itemid")
								altText = rs("itemdesc")
								itemdesc = rs("itemDesc")
								if len(itemdesc) > 63 then itemdesc = left(itemdesc, 60) & "..."								
								price_retail = formatcurrency(rs("price_retail"),2)
								price_our = formatcurrency(rs("price_our"),2)
								productLink = "/p-" & itemid & "-" & formatSEO(itemdesc)
								itempic = rs("itempic")
								%>
                                <div class="item" align="center">
                                    <div style="width:149px; padding-top:2px;" align="center">
                                    	<a href="<%=productLink%>" title="<%=altText%>"><div style="background: url(/productpics/thumb/<%=itemPic%>) 0px -0px no-repeat; width:100px; height:100px;" title="<%=altText%>"></div></a>
                                    </div>
                                    <div style="width:129px; padding:5px 10px 0px 10px;" align="left"><a href="<%=productLink%>" class="cellphone-link" title="<%=altText%>"><h3 style="font-size:12px;"><%=itemdesc%></h3></a></div>
                                    <div style="width:129px; padding:5px 10px 0px 10px;" align="left" class="index-price-retail">Was <del><%=price_retail%></del></div>
                                    <div style="width:129px; padding:0px 10px 0px 10px;" align="left" class="index-price-our">Our Price <%=price_our%></div>
                                    <div style="width:129px; padding:0px 10px 0px 10px;" align="left"><b>Save <%=formatPercent((price_Retail - price_Our) / price_Retail,0)%></b></div>
                                </div>
                                <%
								if ((a mod 4) = 0) and (a < 12) then
									response.write "</div><div class=""slide"">"
								end if
								rs.movenext
							loop
								%>
                            </div>
                        </div>
                    </div>
                    <div class="big-arrow-right" style="margin-top:50px;"></div>
                </div>
            </div>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <%
	'==== get 3 latest items per each models, 12 total
	sql = 	"select	top 4 app_link, app_genre, app_img, app_desc" & vbcrlf & _
			"from	we_apps" & vbcrlf & _
			"where	subbrandid = '" & subbrandid & "'" & vbcrlf & _
			"order by id" & vbcrlf
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1	
	%>    
    <tr>
    	<td width="100%" class="iphone-header-box" style="font-size:18px; font-weight:bold; color:#fff;" align="left">
			&nbsp;&nbsp;<h2 style="font-size:18px; font-weight:bold; color:#fff;">Essential Android Apps</h2>
		</td>
	</tr>
    <tr>
        <td align="left">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                <tr>
				<%
                do until rs.eof
                    %>
                    <td align="left" style="padding:5px;">
                    	<div style="float:left;">
                        	<a target="_blank" href="<%=rs("app_link")%>" title="iPhone Apps: <%=rs("app_desc")%>">
                        		<img src="/images/brand-sub/<%=rs("app_img")%>" border="0" alt="Apps: <%=rs("app_desc")%>" title="Apps: <%=rs("app_desc")%>" />
                            </a>
						</div>
                        <div style="float:left; padding-left:3px;">
                        	<span style="font-size:13px; font-weight:bold;"><%=rs("app_desc")%></span><br />
                            <span style="color:#868686;"><%=rs("app_genre")%></span><br />
                            <a target="_blank" href="<%=rs("app_link")%>" title="Apps: <%=rs("app_desc")%>" style="color:#1D7EA9;">Download the app</a>
                        </div>
                    </td>
                    <%
                    rs.movenext
                loop
                %>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
    	<td width="100%" class="header-box" style="font-size:18px; font-weight:bold; color:#fff;" align="left">
			&nbsp;&nbsp;<h3 style="font-size:18px; font-weight:bold; color:#fff;">Frequently Samsung Galaxy Questions</h3>
		</td>
	</tr>
    <tr>
    	<td width="100%" align="left">
            <table border="0" align="center" cellspacing="0" cellpadding="5" width="100%">
                <tr>
                    <td align="left" width="100%" valign="top" style="padding:15px 5px 5px 5px;">
						<h3 style="color:#000; font-weight:bold;">Which US Mobile phone carriers provide service for Samsung Galaxy phones?</h3><br />
                        <div style="text-align:justify;">
                            Each of the "big four" US carriers provide service for their specific Samsung Galaxy phones: T-Mobile, Verizon, Sprint & AT&T. <br />
                            Also, no-contract providers such as Straight Talk, Metro PCS, Boost Mobile and US Cellular have begun to offer their own flavors of the popular Samsung Galaxy phone.
                        </div>
						<br />
						<h3 style="color:#000; font-weight:bold;">Are there differences between the different Samsung Galaxy smartphones?</h3><br />
                        <div style="text-align:justify;">
							Yes, there are differences between the different Samsung Galaxy S models. Each model has slight differences for each carrier. For example, the Samsung Galaxy S II for T-Mobile is called the "Hercules" or "T989" while the Samsung Galaxy S II for Sprint is called the "Epic 4G Touch." Differences will include slightly different processor speeds, differences in data support (LTE vs. HSPA+, for example), minor differences in size and different flavors of the Android OS. 
                        </div>
						<br />
						<h3 style="color:#000; font-weight:bold;">Would accessories for one Samsung Galaxy phone work for the other?</h3><br />
                        <div style="text-align:justify;">
							There is a wide range of common Samsung Galaxy accessories, such as leather cases, pouches, chargers and more that can fit all Galaxy phone models. However, do keep in mind that certain Galaxy S accessories such as skins, faceplates, covers and cases will have to be specific to your carrier and model. For example, the Galaxy S II Skyrocket for AT&T has a different location for its buttons, camera and camera flash. As always, Wireless Emporium stocks items for all Samsung Galaxy phone models so use our easy to navigate interface to find the model specific to you. 
                        </div>
						<br />
						<h3 style="color:#000; font-weight:bold;">What is the Samsung Galaxy S III and when is the Galaxy S 3 coming out?</h3><br />
                        <div style="text-align:justify;">
							Due to the overwhelming success of the Galaxy S line, Samsung is already hard at work at developing the Samsung Galaxy S III line. It is positioned to be Samsung's new flagship phone line and features best in class performance, a massive 4.8 inch super AMOLED screen and support for the fastest 4G and LTE mobile data networks. The release date quoted by Samsung is estimated to be June 20th, 2012. Don't worry - Wireless Emporium will be well prepared with the largest selection of Samsung Galaxy S III accessories for your new phone! 
                        </div>
                    </td>
				</tr>
			</table>
		</td>
	</tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<%
function getCarrier(pCarrierCode)
	ret = ""
	if instr(pCarrierCode,",") > 0 then
		arrCodes = split(pCarrierCode,",")
		for i = 0 to ubound(arrCodes)
			if arrCodes(i) = "VW" then	
				ret = "Verizon"
			elseif arrCodes(i) = "AL" then 		
				ret = "Alltel"
			elseif arrCodes(i) = "CG" then	
				ret = "AT&T / Cingular"
			elseif arrCodes(i) = "MP" then	
				ret = "Metro PCS"
			elseif arrCodes(i) = "SN" then	
				ret = "Sprint / Nextel"
			elseif arrCodes(i) = "TM" then	
				ret = "T-Mobile"
			elseif arrCodes(i) = "PP" then	
				ret = "Prepaid"
'			elseif arrCodes(i) = "BM" then	
'				ret = "Boost Mobile / Southern Linc"
			elseif arrCodes(i) = "BM" then	
				ret = "Boost Mobile"
			elseif arrCodes(i) = "CK" then	
				ret = "Cricket"
			elseif arrCodes(i) = "US" then	
				ret = "U.S. Cellular"
			elseif arrCodes(i) = "UN" then	
				ret = "Unlocked"
			end if
			
			if ret <> "" then
				imagePath = "/images/brand-sub/carrier_" & arrCodes(i) & ".png"
				paddingTop = "5px"
				select case arrCodes(i)
					case "VW" : paddingTop = "1px"
					case "TM" : paddingTop = "8px"
					case "MP" : paddingTop = "8px"
					case "SN" : paddingTop = "2px"
				end select

				if fs.FileExists(server.MapPath(imagePath)) then 
					ret = "<div style=""padding-top:" & paddingTop & ";""><img src=""" & imagePath & """ border=""0"" /></div>"
				else
					ret = "<div style=""padding-top:" & paddingTop & ";"">" & ret & "</div>"
				end if
				exit for
			end if
		next
	end if
	getCarrier = ret
end function	
%>
<!--#include virtual="/includes/template/bottom_brand.asp"-->
