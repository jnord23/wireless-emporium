<%
set oConn = CreateObject("ADODB.Connection")
oConn.Open "DSN=WEDB;Uid=webApps;Pwd=6E7j22^9Gt94d#;"

reportDate = dateAdd("D",-1,date)

sql	=	"select	a.shortDesc" & vbcrlf & _
		"	,	case when a.mobileSite = 0 then 'Desktop' else 'Mobile' end siteType" & vbcrlf & _
		"	,	a.orderDesc" & vbcrlf & _
		"	,	a.isPhoneOrder" & vbcrlf & _
		"	,	count(*) orderCount, sum(cast(a.ordergrandtotal as money)) orderGrandTotal,	isnull(sum(cast(a.ordergrandtotal as money)) / nullif(count(*), 0), 0) orderAvg" & vbcrlf & _
		"	,	sum(a.ppOrder) ppOrderCount, sum(a.pp_orderGrandTotal) ppGrandTotal, isnull(sum(cast(a.pp_ordergrandtotal as money)) / nullif(sum(a.ppOrder), 0), 0) ppOrderAvg" & vbcrlf & _
		"	,	sum(a.cogs) cogs" & vbcrlf & _
		"from	(" & vbcrlf & _
		"		select	x.shortDesc, a.mobileSite, a.orderid, a.ordergrandtotal" & vbcrlf & _
		"			,	case when a.pp_orderGrandTotal > 0 then 1 else 0 end ppOrder" & vbcrlf & _
		"			,	isnull(a.pp_orderGrandTotal, 0) pp_orderGrandTotal" & vbcrlf & _
		"			,	isnull(e.orderDesc, 'Credit Card') orderDesc" & vbcrlf & _
		"			,	case when f.orderid is not null then 'Phone' else '' end isPhoneOrder" & vbcrlf & _
		"			,	count(*) lineItems, sum(b.quantity) qty, sum(b.cogs * b.quantity) cogs" & vbcrlf & _
		"		from	xstore x with (nolock) left outer join we_orders a with (nolock) " & vbcrlf & _
		"			on	x.site_id = a.store join we_orderdetails b with (nolock)" & vbcrlf & _
		"			on	a.orderid = b.orderid join we_items c with (nolock)" & vbcrlf & _
		"			on	b.itemid = c.itemid join v_accounts d with (nolock)" & vbcrlf & _
		"			on	a.accountid = d.accountid and a.store = d.site_id left outer join xordertype e with (nolock)" & vbcrlf & _
		"			on	a.extordertype = e.typeid left outer join we_phoneOrders f with (nolock)" & vbcrlf & _
		"			on	a.orderid = f.orderid" & vbcrlf & _
		"		where	a.orderdatetime >= convert(varchar(10), dateadd(d, -1, getdate()), 20) and a.orderdatetime < convert(varchar(10), getdate(), 20)" & vbcrlf & _
		"			and	a.approved = 1 and (a.cancelled = 0 or a.cancelled is null)" & vbcrlf & _
		"			and	a.parentorderid is null" & vbcrlf & _
		"			and	d.email not like '%@wirelessemporium.com'" & vbcrlf & _
		"			and	b.itemid not in (0,1)" & vbcrlf & _
		"		group by	x.shortDesc, a.mobileSite, a.orderid, a.ordergrandtotal" & vbcrlf & _
		"				,	case when a.pp_orderGrandTotal > 0 then 1 else 0 end" & vbcrlf & _
		"				,	isnull(a.pp_orderGrandTotal, 0)" & vbcrlf & _
		"				,	isnull(e.orderDesc, 'Credit Card')" & vbcrlf & _
		"				,	case when f.orderid is not null then 'Phone' else '' end" & vbcrlf & _
		"		) a" & vbcrlf & _
		"group by	a.shortDesc" & vbcrlf & _
		"		,	case when a.mobileSite = 0 then 'Desktop' else 'Mobile' end" & vbcrlf & _
		"		,	a.orderDesc" & vbcrlf & _
		"		,	a.isPhoneOrder" & vbcrlf & _
		"order by 1, 2, 3, 4"
set rs = CreateObject("ADODB.Recordset")
rs.open sql, oConn, 3, 3

sHtml = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Strict//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"">" & vbcrlf & _
		"<html xmlns=""http://www.w3.org/1999/xhtml"">" & vbcrlf & _
		"<head>" & vbcrlf & _
		"<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />" & vbcrlf & _
		"<meta name=""viewport"" content=""width=device-width, initial-scale=1.0"" />" & vbcrlf & _
		"<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,600,700,300,800' rel='stylesheet' type='text/css'>" & vbcrlf & _
		"<title>Daily Order Statistics</title>" & vbcrlf & _
		"</head>" & vbcrlf & _
		"<body style=""margin: 0; padding: 0; width: 700px; -webkit-text-size-adjust: none;"">" & vbcrlf & _
		"<div style=""padding:10px 0px 10px 0px; font-family: 'Open Sans', sans-serif; font-weight:600;"">Daily Order Statistics for " & reportDate & "</div>" & vbcrlf & _
		"<table width=""100%"" border=""1"" cellspacing=""0"" cellpadding=""5"" style=""font-size:11px; border-collapse:collapse; font-family: 'Open Sans', sans-serif; font-weight:600;"">" & vbcrlf & _
		"  	<tr>" & vbcrlf & _
		"  		<td align=""left"" style=""background-color:#333; color:#fff; font-weight:bold;"" rowspan=""2"">Sites</td>"& vbcrlf & _
		"  		<td align=""left"" style=""background-color:#333; color:#fff; font-weight:bold;"" rowspan=""2"">Site Type</td>"& vbcrlf & _
		"  		<td align=""left"" style=""background-color:#333; color:#fff; font-weight:bold;"" rowspan=""2"">Payment Type</td>"& vbcrlf & _
		"  		<td align=""left"" style=""background-color:#333; color:#fff; font-weight:bold;"" rowspan=""2"">isPhoneOrder</td>"& vbcrlf & _
		"  		<td align=""center"" style=""background-color:#333; color:#fff; font-weight:bold;"" colspan=""3"">OrderTotal</td>"& vbcrlf & _		
		"  		<td align=""center"" style=""background-color:#333; color:#fff; font-weight:bold;"" colspan=""3"">PostPurchase OrderTotal</td>"& vbcrlf & _
		"  		<td align=""center"" style=""background-color:#333; color:#fff; font-weight:bold;"" rowspan=""2"">Cogs</td>"& vbcrlf & _
		"	</tr>"& vbcrlf & _
		"  	<tr>" & vbcrlf & _
		"  		<td align=""right"" style=""background-color:#333; color:#fff; font-weight:bold;"">Count</td>"& vbcrlf & _
		"  		<td align=""right"" style=""background-color:#333; color:#fff; font-weight:bold;"">Amount</td>"& vbcrlf & _
		"  		<td align=""right"" style=""background-color:#333; color:#fff; font-weight:bold;"">Avg.</td>"& vbcrlf & _
		"  		<td align=""right"" style=""background-color:#333; color:#fff; font-weight:bold;"">Count</td>"& vbcrlf & _
		"  		<td align=""right"" style=""background-color:#333; color:#fff; font-weight:bold;"">Amount</td>"& vbcrlf & _
		"  		<td align=""right"" style=""background-color:#333; color:#fff; font-weight:bold;"">Avg.</td>"& vbcrlf & _
		"	</tr>"& vbcrlf		

lastSiteName = ""
lastSiteType = ""
lastOrderDesc = ""
lastPhoneOrder = "N/A"

' 0: grandtotal, 1:siteTotal, 2:siteTypeTotal
dim arrGrandTotal(2)
dim arrOrderCount(2)
dim arrPPGrandTotal(2)
dim arrPPOrderCount(2)
dim arrCogs(2)

if not rs.eof then
	nCount = 0
	do until rs.eof
		nCount = nCount + 1
		curSiteName = rs("shortDesc")
		curSiteType = rs("siteType")
		curOrderDesc = rs("orderDesc")
		curPhoneOrder = rs("isPhoneOrder")
		orderCount = rs("orderCount")
		orderGrandTotal = rs("orderGrandTotal")
		orderAvg = rs("orderAvg")
		ppOrderCount = rs("ppOrderCount")
		ppGrandTotal = rs("ppGrandTotal")
		ppOrderAvg = rs("ppOrderAvg")
		cogs = rs("cogs")
		
		arrOrderCount(0) = arrOrderCount(0) + orderCount
		arrGrandTotal(0) = arrGrandTotal(0) + orderGrandTotal
		arrPPOrderCount(0) = arrPPOrderCount(0) + ppOrderCount
		arrPPGrandTotal(0) = arrPPGrandTotal(0) + ppGrandTotal		
		arrCogs(0) = arrCogs(0) + cogs

		if lastSiteName <> "" and curSiteName <> lastSiteName then
			sHtml = sHtml & "  	<tr>" & vbcrlf & _
							"  		<td align=""left"" style=""background-color:#eee; font-weight:bold;"">" & lastSiteName & "</td>"& vbcrlf & _
							"  		<td align=""left"" style=""background-color:#eee; font-weight:bold;"">Sub Total</td>"& vbcrlf & _
							"  		<td align=""left"" style=""background-color:#eee; font-weight:bold;""></td>"& vbcrlf & _
							"  		<td align=""left"" style=""background-color:#eee; font-weight:bold;""></td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#eee; font-weight:bold;"">" & formatnumber(arrOrderCount(1), 0) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#eee; font-weight:bold;"">" & formatcurrency(arrGrandTotal(1), 2) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#eee; font-weight:bold;"">" & formatcurrency(arrGrandTotal(1)/arrOrderCount(1), 2) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#eee; font-weight:bold;"">" & formatnumber(arrPPOrderCount(1), 0) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#eee; font-weight:bold;"">" & formatcurrency(arrPPGrandTotal(1), 2) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#eee; font-weight:bold;"">" & formatcurrency(arrPPGrandTotal(1)/arrPPOrderCount(1), 2) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#eee; font-weight:bold;"">" & formatcurrency(arrCogs(1), 2) & "</td>"& vbcrlf & _
							"	</tr>"& vbcrlf
			arrOrderCount(1) = 0
			arrGrandTotal(1) = 0
			arrPPOrderCount(1) = 0
			arrPPGrandTotal(1) = 0
			arrCogs(1) = 0
		end if

		arrOrderCount(1) = arrOrderCount(1) + orderCount
		arrGrandTotal(1) = arrGrandTotal(1) + orderGrandTotal
		arrPPOrderCount(1) = arrPPOrderCount(1) + ppOrderCount
		arrPPGrandTotal(1) = arrPPGrandTotal(1) + ppGrandTotal		
		arrCogs(1) = arrCogs(1) + cogs
		
		sHtml = sHtml & "  	<tr>" & vbcrlf & _
						"  		<td align=""left"">" & curSiteName & "</td>"& vbcrlf & _
						"  		<td align=""left"">" & curSiteType & "</td>"& vbcrlf & _
						"  		<td align=""left"">" & curOrderDesc & "</td>"& vbcrlf & _
						"  		<td align=""left"">" & curPhoneOrder & "</td>"& vbcrlf & _
						"  		<td align=""right"">" & formatnumber(orderCount, 0) & "</td>"& vbcrlf & _		
						"  		<td align=""right"">" & formatcurrency(orderGrandTotal, 2) & "</td>"& vbcrlf & _		
						"  		<td align=""right"">" & formatcurrency(orderAvg, 2) & "</td>"& vbcrlf & _
						"  		<td align=""right"">" & formatnumber(ppOrderCount, 0) & "</td>"& vbcrlf & _
						"  		<td align=""right"">" & formatcurrency(ppGrandTotal, 2) & "</td>"& vbcrlf & _
						"  		<td align=""right"">" & formatcurrency(ppOrderAvg, 2) & "</td>"& vbcrlf & _
						"  		<td align=""right"">" & formatcurrency(cogs, 2) & "</td>"& vbcrlf & _
						"	</tr>"& vbcrlf

		lastSiteName = curSiteName
		lastSiteType = curSiteType
		lastOrderDesc = curOrderDesc
		lastPhoneOrder = curPhoneOrder
		
		if nCount = rs.recordcount then
			sHtml = sHtml & "  	<tr>" & vbcrlf & _
							"  		<td align=""left"" style=""background-color:#eee; font-weight:bold;"">" & lastSiteName & "</td>"& vbcrlf & _
							"  		<td align=""left"" style=""background-color:#eee; font-weight:bold;"">Sub Total</td>"& vbcrlf & _
							"  		<td align=""left"" style=""background-color:#eee; font-weight:bold;""></td>"& vbcrlf & _
							"  		<td align=""left"" style=""background-color:#eee; font-weight:bold;""></td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#eee; font-weight:bold;"">" & formatnumber(arrOrderCount(1), 0) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#eee; font-weight:bold;"">" & formatcurrency(arrGrandTotal(1), 2) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#eee; font-weight:bold;"">" & formatcurrency(arrGrandTotal(1)/arrOrderCount(1), 2) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#eee; font-weight:bold;"">" & formatnumber(arrPPOrderCount(1), 0) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#eee; font-weight:bold;"">" & formatcurrency(arrPPGrandTotal(1), 2) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#eee; font-weight:bold;"">" & formatcurrency(arrPPGrandTotal(1)/arrPPOrderCount(1), 2) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#eee; font-weight:bold;"">" & formatcurrency(arrCogs(1), 2) & "</td>"& vbcrlf & _
							"	</tr>"& vbcrlf
		
			sHtml = sHtml & "  	<tr>" & vbcrlf & _
							"  		<td align=""left"" style=""background-color:#ccc; font-weight:bold;"">Grand Total</td>"& vbcrlf & _
							"  		<td align=""left"" style=""background-color:#ccc;""></td>"& vbcrlf & _
							"  		<td align=""left"" style=""background-color:#ccc;""></td>"& vbcrlf & _
							"  		<td align=""left"" style=""background-color:#ccc;""></td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#ccc; font-weight:bold;"">" & formatnumber(arrOrderCount(0), 0) & "</td>"& vbcrlf & _		
							"  		<td align=""right"" style=""background-color:#ccc; font-weight:bold;"">" & formatcurrency(arrGrandTotal(0), 2) & "</td>"& vbcrlf & _		
							"  		<td align=""right"" style=""background-color:#ccc; font-weight:bold;"">" & formatcurrency(arrGrandTotal(0)/arrOrderCount(0), 2) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#ccc; font-weight:bold;"">" & formatnumber(arrPPOrderCount(0), 0) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#ccc; font-weight:bold;"">" & formatcurrency(arrPPGrandTotal(0), 2) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#ccc; font-weight:bold;"">" & formatcurrency(arrPPGrandTotal(0)/arrPPOrderCount(0), 2) & "</td>"& vbcrlf & _
							"  		<td align=""right"" style=""background-color:#ccc; font-weight:bold;"">" & formatcurrency(arrCogs(0), 2) & "</td>"& vbcrlf & _
							"	</tr>"& vbcrlf
		
		end if
		rs.movenext
	loop
else

end if		

sHtml = sHtml & "</table></body></html>"

response.write sHtml

%>
