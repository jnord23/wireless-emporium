<!DOCTYPE html>
<html>
<head>
</head>
<body>
<script src="//code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="//bizsolutions.strands.com/sbsstatic/js/sbsLib-1.0.min.js"></script>
<div>
    <div><a href="javascript:refreshWidget()">refresh Widget</a></div>
    <div id="strands" class="strandsRecs" tpl="misc_3" item="145880"></div>                
</div>
</body>
</html>

<script>
	window.onload = function() {
		try{
			SBS.Recs.setRenderer(strands_rendering, "misc_3");
			SBS.Worker.go("Kx7AmRMCrW");
		} catch (e) {
			alert(e.message);
		};
	}
	
	function strands_rendering(rec_info) {
		var rec = rec_info.recommendations;
		var lap = 0;
		var content = "";

		lap = 0;
		for(i=0;i<=rec.length-1;i++){
//			alert(JSON.stringify(rec[i]));
			var itemid = rec[i]["itemId"];
			var itemPic = rec[i]["metadata"]["picture"];
			var price_retail = rec[i]["metadata"]["price"];
			var price = rec[i]["metadata"]["properties"]["saleprice"];
			var itemDesc = rec[i]["metadata"]["name"];
			if (itemDesc.length > 50) itemDesc = itemDesc.substring(0,46) + '...';
			if (!isNaN(price_retail) && !isNaN(price)) {
				if (freeItems.indexOf(itemid + ",") == -1) {
					lap = lap + 1;
					content = content + "<div style='width:160px; padding:0px 0px 0px 5px; border:1px solid #ccc;'>";
					content = content + "	<div><img src='" + itemPic + "' border='0' width='100' /></div>";
					content = content + "	<div style='width:100%; padding-top:10px; font-size:14px; font-weight:bold; color:#333;' align='center'>" + itemDesc + "</div>";
					content = content + "	<div style='width:100%; padding-top:10px; font-size:13px; color:#333;' align='center'>ItemID: " + itemid + "</div>";
					content = content + "	<div style='width:100%; padding-top:10px;' align='center'><span style='color:#c00; font-weight:bold; font-size:16px;'>$" + price + "</span> &nbsp; <span style='text-decoration:line-through; font-size:13px;'>$" + price_retail + "</span></div>";
					content = content + "</div>";
				}
			}
		}
		$("#strands").html(content);
	}
	
	function refreshWidget() {
		document.getElementById("strands").setAttribute('item','102964');
		try {
			SBS.Recs.setRenderer(strands_rendering, "misc_3");
			SBS.Worker.update();
		} catch (e) {};
	}
</script>
