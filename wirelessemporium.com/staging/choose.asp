<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<%
response.buffer = true

dim categoryID, category_URL, carrierID
categoryID = request.querystring("typeid")
category_URL = categorySEO(categoryID)
carrierID = request.querystring("carrierid")
if carrierID <> "" then category_URL = category_URL & "?carrierid=" & carrierID

if category_URL <> "" then
	call PermanentRedirect(category_URL)
else
	call PermanentRedirect("/")
end if
%>
