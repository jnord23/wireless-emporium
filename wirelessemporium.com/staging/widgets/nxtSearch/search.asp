<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
noLeftNav = 1
pageName = "nextopia search"
noCommentBox = true
%>
<!--#include virtual="/includes/template/top.asp"-->
<script>
window.WEDATA.pageType = "search";
</script>
<div style="clear:both;"></div>
<div id="nxt-refines" style="float:left; width:200px;"></div>
<div id="nxt-content" style="float:left; width:748px; padding-left:10px;">
	<div style="width:100%; text-align:center;"><img src="//d2brc35ftammet.cloudfront.net/img/loading-grey.gif" /></div>
</div>
<div style="clear:both; height:100px;"></div>
<script>
	var nxtBtnTimer;
	
	function getNxtUrl(hrefString) {
		var ret = hrefString;
		var searchKeyword = "NULL";
		if (getQsVars("")["keywords"] != undefined) searchKeyword = getQsVars("")["keywords"];
		var itemid = hrefString.substring(hrefString.indexOf("/p-")+3);
		itemid = itemid.substring(0,itemid.indexOf("-"));
		var tracking = "IZID=NXTPIA_" + itemid + "_" + searchKeyword + "&utm_source=Nextopia&utm_medium=INT"
						
		if (hrefString.indexOf("?") != -1) {
			ret = hrefString.substring(0, hrefString.indexOf("?")) + "?" + tracking;
		} else {
			ret = hrefString + "?" + tracking;
		}
		
		return ret;
	}
	
	function nxtButtonSwap() {
		$('.prod-name').each(function(index) {
			try {
				var hrefString = $(this).attr("href").toString();
//				if (hrefString.indexOf("?IZID") == -1)
					$(this).attr("href",getNxtUrl(hrefString));

			} catch (e) {}
		});		

		$('.nxt_image_wrapper').each(function(index) {
			try {
				var hrefString = $(this).find('a').attr("href").toString();
//				if (hrefString.indexOf("?IZID") == -1) 
					$(this).find('a').attr("href",getNxtUrl(hrefString));
				
			} catch (e) {}
		});		
		
		clearTimeout(nxtBtnTimer);
		nxtBtnTimer = setTimeout('nxtButtonSwap()', 1000);
	}
	
	$(document).ready(function() {
		nxtButtonSwap();
	});
</script>
<!--#include virtual="/includes/template/bottom.asp"-->