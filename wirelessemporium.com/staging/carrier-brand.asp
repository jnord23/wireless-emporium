<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
response.buffer = true
dim carrierid
carrierid = request.querystring("carrierid")
if carrierid = "" or not isNumeric(carrierid) then
	call PermanentRedirect("/")
end if

call fOpenConn()
SQL = "SELECT * FROM we_Carriers WHERE id = '" & carrierid & "'"
'session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
else
	dim carrierName, carrierCode, carrierImg
	carrierName = RS("carrierName")
	carrierCode = RS("carrierCode")
	carrierImg = RS("carrierImg")
end if

dim brandid
brandid = request.querystring("brandid")
if brandid = "" or not isNumeric(brandid) then
	call PermanentRedirect("/")
end if

displayIntLink = 0
showIntPhones = request.Form("showIntPhones")
if isnull(showIntPhones) or len(showIntPhones) < 1 then showIntPhones = 0

SQL = "SELECT DISTINCT A.*, C.* FROM (we_models A INNER JOIN we_items B ON A.modelID = B.modelID)"
SQL = SQL & " INNER JOIN we_brands C ON A.brandid = C.brandid"
SQL = SQL & " WHERE B.brandid = '" & brandid & "'"
SQL = SQL & " AND B.hideLive = 0 AND patindex('%All %',A.modelname) = 0"
SQL = SQL & " AND A.carrierCode LIKE '%" & carrierCode & "%'"
SQL = SQL & " ORDER BY a.international, A.temp DESC"
'session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
end if

dim brandName, brandImg
brandName = RS("brandName")
brandImg = RS("brandImg")

dim SEtitle, SEdescription, SEkeywords, topText, bottomText, RStypes
select case brandid
	case "1"
'		SEtitle = "Buy Discount Audiovox Cell Phone Accessories Mobile Chargers, Batteries, Antenna, Holsters, Faceplates, Cases"
'		SEdescription = "The best place to buy the latest Audiovox accessories for your phones, including cases, batteries, chargers, antennas, faceplates and more. Buy Online!"
'		SEkeywords = "audiovox cell phone accessory, audiovox mobile phone accessories, audiovox cell phone battery, audiovox charger, audiovox leather case, audiovox cell phone, audiovox cellular phone accessory, cell phone parts, wireless accessories, wireless phone accessories, bluetooth cell phone headsets, cell phone antennas, cell phone belt clips, cell phone gear, cell phone keypads, cellphone keypads"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your " & brandName & " brand cell phone. Choose from 1000's of quality " & brandName & " accessories including leather cases, pouches, batteries, <a class=""topText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""topText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""topText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality " & brandName & " wireless accessories online!"
		topText = topText & "<br><br>With our discount <a class=""topText-link"" href=""http://www.wirelessemporium.com/"">" & brandName & " cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your " & brandName & " mobile phone!"
	case "2"
'		SEtitle = "Discount Sony Ericsson Cell Phone Batteries, Headsets, Holsters, Cases, Chargers, Antenna, Faceplates, Mobile Accessories"
'		SEdescription = "We offer only the best quality Sony Ericsson cell phone accessories at unbelievable rates. Products include discount batteries, headsets, holsters, chargers, antenna, cases, faceplates & much more!!"
'		SEkeywords = "sony ericsson accessory, sony ericsson handphone, sony ericsson faceplates, sony ericsson battery, sony ericsson cell phone battery, sony ericsson charger, sony ericsson phone charger, sony ericsson case, sony ericsson holster"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your " & brandName & " brand cell phone. Choose from 1000's of quality " & brandName & " accessories including leather cases, pouches, batteries, <a class=""topText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""topText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""topText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality " & brandName & " wireless accessories online!"
		topText = topText & "<br><br>With our discount <a class=""topText-link"" href=""http://www.wirelessemporium.com/"">" & brandName & " cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your " & brandName & " mobile phone!"
	case "3"
'		SEtitle = "Buy Discount Kyocera Qualcomm Cell Phone Accessories, Mobile Chargers, Keypads, Batteries, Antenna, Holsters, Faceplates, Cases, Covers"
'		SEdescription = "Make your phone your own personal statement with our Kyocera / Qualcomm cell phone accessories including covers, keypads, chargers, batteries, antenna, holsters, faceplates, cases and so much more."
'		SEkeywords = "kyocera cell phone accessory, kyocera mobile phone accessories, kyocera battery, kyocera cell phone battery, kyocera phone cover, kyocera cell phone cover, kyocera faceplates, kyocera cell phone faceplates, mobile phone headsets, mobile phone chargers, mobile phone cases, mobile phone batteries, mobile phone accessories, mobile handsfree, faceplates for cell phones"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your " & brandName & " brand cell phone. Choose from 1000's of quality " & brandName & " accessories including leather cases, pouches, batteries, <a class=""topText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""topText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""topText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality " & brandName & " wireless accessories online!"
		topText = topText & "<br><br>With our discount <a class=""topText-link"" href=""http://www.wirelessemporium.com/"">" & brandName & " cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your " & brandName & " mobile phone!"
	case "4"
'		SEtitle = "LG Cell Phone Accessories: LG Voyager Accessories | LG Shine Accessories"
'		SEdescription = "Get great deals on LG cell phone accessories like LG batteries, cases, covers and much more at discounted prices only at Wireless Emporium ?the #1 cell phone accessory store online."
'		SEkeywords = "lg accessories, lg cell phone accessories, lg voyager covers, lg shine case, lg cell phone cases, lg cell phone covers, lg cell phone batteries"
'		h1 = "LG&nbsp;Cell&nbsp;Phone&nbsp;Accessories"
		titleText = "Discount LG Cell Phone Chargers, Batteries &amp; Other LG Cell Phone Accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct LG cell phone accessories for your LG brand cell phone. Choose from 1000's of quality LG cell phone accessories including leather cases, pouches, LG cell phone batteries, LG cell phone cases, LG cell phone covers, <a class=""topText-link"" href=""http://www.wirelessemporium.com/p-4532-replacement-lithium-ion-battery-for-lg-vx3200.asp"" title=""LG Battery"">LG batteries</a>, the best <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Bluetooth Headset"">Bluetooth headsets</a>, and much more. Get great deals on <a class=""topText-link"" href=""http://www.wirelessemporium.com/p-17330-lg-shine-cu720-premium-vertical-leather-pouch.asp"" title=""LG Shine Case"">LG Shine Cases</a>, LG Voyager Covers, LG Shine phone covers and many other LG cell phone accessories. Wireless Emporium is also the largest seller of LG <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-cases.asp"" title=""Cell Phone Cases"">cell phone cases</a>, LG <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a> and cell phone faceplates to personalize and accentuate your phone. No one sells more quality LG wireless accessories online!"
		topText = topText & "<br><br>Wireless Emporium is itself the epitome of quality and reliability, and its top class cell phone accessories like LG voyager covers are just a simple example of the credibility of its products."
		topText = topText & "<br><br>With our discount LG cell phone accessories, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your LG mobile phone!"
		topText = topText & "<br><br>Check out our wide range of world class LG accessories at Wireless Emporium like <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-595-cell-accessories-lg-env2-vx9100.asp"" title=""LG enV2 Accessories"">LG enV2 Accessories</a>, <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-756-cell-accessories-lg-env3-vx9200.asp"" title=""LG enV3 Accessories"">LG enV3 accessories</a>, <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-715-cell-accessories-lg-xenon-gr500.asp"" title=""LG Xenon Accessories"">LG Xenon accessories</a> and many more!"
		bottomText = "Wireless Emporium is the Number One Online Resource for cell phone accessories and the largest seller of cell phone cases, cell phone covers, <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-chargers.asp"" title=""Cell Phone Chargers"">cell phone chargers</a>, cell phone batteries and other accessories to personalize, optimize and protect your phone."
	case "5"
'		SEtitle = "Motorola Cell Phone Accessories: Motorola Q Cell Phone Cases, Batteries, Faceplates"
'		SEdescription = "Get the best deals on Motorola cell phone accessories like Motorola batteries, cases, faceplates and much more at discounted prices only at Wireless Emporium ?the #1 cell phone accessory store online."
'		SEkeywords = "motorola accessories, motorola cell phone accessories, motorola cell phone cases, motorola batteries, motorola faceplates, motorola q accessories"
		h1 = "Motorola&nbsp;Cell&nbsp;Phone&nbsp;Accessories"
		titleText = "Discount Motorola Cell Phone Chargers, Batteries &amp; Other Motorola Cell Phone Accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories "
		topText = topText & "for your Motorola brand cell phone. Choose from 1000's of quality Motorola accessories including "
		topText = topText & "Motorola cell phone cases, leather cases, pouches, Motorola batteries, Motorola faceplates, "
		topText = topText & "<a class=""topText-link"" href=""http://www.wirelessemporium.com/products/motorola-v220-antennas-antenna-stub-for-motorola-v220.asp"" title=""Motorola Antenna"">Motorola antenna</a>, "
		topText = topText & "Bluetooth headsets, all kinds of chargers and much more. "
		topText = topText & "Wireless Emporium is also the largest seller of cell phone covers, "
		topText = topText & "<a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-charms-bling-kits.asp"" title=""Cell Phone Charms"">cell phone charms</a> and "
		topText = topText & "cell phone faceplates to personalize and accentuate your phone. No one sells more quality HTC wireless accessories online!"
		topText = topText & "<br><br>Buy <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-500-accessories-motorola-w385.asp"" title=""Motorola w385 Accessories"">Motorola w385 accessories</a>, "
		topText = topText & "Motorola V3 Razr accessories and accessories for many other models right here ?at Wireless Emporium. "
		topText = topText & "With our discount <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Motorola Accessories"">Motorola accessories</a>, "
		topText = topText & "rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Motorola mobile phone!"
	case "6"
'		SEtitle = "Discount Nextel Cell Phone Accessories, Mobile Batteries, Cases, Antenna, Holsters, Faceplates, Hands Free & Headsets, Chargers"
'		SEdescription = "If you're looking for quality, Nextel cell phone accessories, checkout our huge selection factory-direct Nextel accessories. Includes batteries, cases, faceplates, chargers & many more."
'		SEkeywords = "nextel cell phone accessory, nextel mobile phone accessories, nextel battery, nextel cell phone battery, nextel phone charger, case i870 leather nextel, nextel faceplates, mobile phone headsets, mobile phone chargers, mobile phone cases, mobile phone batteries, mobile phone accessories, mobile handsfree, faceplates for cell phones, discount cell phone accessories, cellular phone headsets, cellular phone accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your " & brandName & " brand cell phone. Choose from 1000's of quality " & brandName & " accessories including leather cases, pouches, batteries, <a class=""topText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""topText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""topText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality " & brandName & " wireless accessories online!"
		topText = topText & "<br><br>With our discount <a class=""topText-link"" href=""http://www.wirelessemporium.com/"">" & brandName & " cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your " & brandName & " mobile phone!"
	case "7"
'		SEtitle = "Nokia Cell Phone Accessories: Nokia 6085 Case, Cables, Nokia Phone Chargers"
'		SEdescription = "Buy Nokia cell phone accessories like Nokia batteries, cases, faceplates and much more at discounted prices only at Wireless Emporium ?the #1 cell phone accessory store online."
'		SEkeywords = "nokia accessories, nokia cell phone accessories, nokia 6085 case, nokia cables, nokia phone chargers, nokia phones"
'		h1 = "Nokia&nbsp;Cell&nbsp;Phone&nbsp;Accessories"
		titleText = "Discount Nokia Cell Phone Chargers, Batteries &amp; Other Nokia Accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories "
		topText = topText & "for your Nokia brand cell phone. Choose from 1000's of quality Nokia accessories including "
		topText = topText & "leather cases, pouches, batteries, Bluetooth headsets, "
		topText = topText & "<a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-batteries.asp3"" title=""Nokia Cables"">Nokia Cables</a>, all kinds of "
		topText = topText & "<a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Nokia Phone Chargers"">Nokia phone chargers</a> and much more. "
		topText = topText & "Wireless Emporium is also the largest seller of "
		topText = topText & "cell phone covers, cell phone faceplates &amp; "
		topText = topText & "<a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-charms-bling-kits.asp"" title=""Cell Phone Bling"">cell phone bling</a> kits "
		topText = topText & "to personalize and accentuate your phone. No one sells more quality Nokia wireless accessories online!"
		topText = topText & "<br><br>With our discount <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Nokia Cell Phone Accessories"">Nokia cell phone accessories</a>, "
		topText = topText & "rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Nokia mobile phone!"
	case "8"
'		SEtitle = "Discount Panasonic Cell Phone Accessories, Mobile Chargers, Batteries, Antenna, Holsters, Faceplates, Covers, Keypads"
'		SEdescription = "Wireless Emporium offers latest Panasonic accessories for your cell phones, including cases, batteries, chargers, antenna, keypads, faceplates and more. Buy Online."
'		SEkeywords = "panasonic cell phone accessory, panasonic mobile phone accessories, panasonic battery pack, panasonic battery charger, panasonic phone battery, panasonic phone part, panasonic headset, mobile phone accessories, mobile handsfree, faceplates for cell phones"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your " & brandName & " brand cell phone. Choose from 1000's of quality " & brandName & " accessories including leather cases, pouches, batteries, <a class=""topText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""topText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""topText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality " & brandName & " wireless accessories online!"
		topText = topText & "<br><br>With our discount <a class=""topText-link"" href=""http://www.wirelessemporium.com/"">" & brandName & " cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your " & brandName & " mobile phone!"
	case "9"
'		SEtitle = "Samsung Cell Phone Accessories: Samsung Eternity, Behold Accessories"
'		SEdescription = "Samsung cell phone accessories at Wireless Emporium: Samsung Behold Accessories and many more like cell phone cases and covers at discounted prices!"
'		SEkeywords = "samsung accessories, samsung cell phone accessories, samsung eternity cell phone accessories, samsung behold accessories"
'		h1 = "Samsung&nbsp;Cell&nbsp;Phone&nbsp;Accessories"
		titleText = "Discount Samsung Cell Phone Chargers, Batteries &amp; Other Samsung Accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your Samsung brand cell phone. Choose from 1000's of quality Samsung accessories including Samsung Behold accessories, <a class=""topText-link"" href=""http://www.wirelessemporium.com/p-5396-900-mah-extended-li-ion-battery-for-samsung-c225.asp"" title=""Samsung Phone Battery"">Samsung phone batteries</a>, <a class=""topText-link"" href=""http://www.wirelessemporium.com/sc-7-sb-9-samsung-cases-pouches-skins.asp"" title=""Samsung Eternity Leather Cases"">Samsung eternity leather cases</a>, pouches, batteries, Bluetooth headsets, all kinds of chargers and much more. Wireless Emporium is also the largest seller of Samsung <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a> and Samsung cell phone faceplates to personalize and accentuate your phone. No one sells more quality Samsung wireless accessories online!"
		topText = topText & "<br><br>Wireless Emporium is itself the epitome of quality and reliability, and its top class cell phone accessories like Samsung Behold Accessories are just a simple example of the credibility of its products."
		topText = topText & "<br><br>With our discount <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Samsung Cell Phone Accessories"">Samsung cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Samsung mobile phone!"
		bottomText = "Wireless Emporium is the Number One Online Resource for cell phone accessories and the largest seller of cell phone cases, cell phone covers, cell phone chargers, <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-batteries.asp"" title=""Cell Phone Batteries"">cell phone batteries</a> and other accessories to personalize, optimize and protect your phone."
	case "10"
'		SEtitle = "Buy Sanyo Cell Phone Accessories Mobile Chargers, Batteries, Holsters, Faceplates, Cases, Antenna, Covers, Keypads Online"
'		SEdescription = "Wireless Emporium offers latest Sanyo cell phone accessories for your phones, including cases, batteries, chargers, holsters, antennas, faceplates and more. Buy Online."
'		SEkeywords = "sanyo cell phone accessory, sanyo mobile phone accessories, sanyo cell phone battery, sanyo battery charger, sanyo leather case, faceplates for sanyo, mobile phone holsters, mobile phone headsets, mobile phone chargers, mobile phone cases, mobile phone batteries, mobile phone accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your " & brandName & " brand cell phone. Choose from 1000's of quality " & brandName & " accessories including leather cases, pouches, batteries, <a class=""topText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""topText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""topText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality " & brandName & " wireless accessories online!"
		topText = topText & "<br><br>With our discount <a class=""topText-link"" href=""http://www.wirelessemporium.com/"">" & brandName & " cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your " & brandName & " mobile phone!"
	case "11"
'		SEtitle = "Discount Siemens Cell Phone Accessories Mobile Chargers, Batteries, Antenna, Holsters, Faceplates, Covers, Keypads"
'		SEdescription = "Best place to buy discount accessories such as batteries, chargers, Holsters, Belt Clips, Leather Cases, Antennas, Handsfree Headsets, & more online! We pride ourselves on giving you a complete line of cell phone devices to accentuate your Siemens cell phone."
'		SEkeywords = "accessory cell phone siemens, accessory mobile siemens, accessories cell phone siemens, siemens cell phone battery, siemens battery, part siemens, siemens faceplates, mobile phone holsters, mobile phone headsets, mobile phone chargers, mobile phone cases, mobile phone batteries, mobile phone accessories, mobile handsfree, faceplates for cell phones"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your " & brandName & " brand cell phone. Choose from 1000's of quality " & brandName & " accessories including leather cases, pouches, batteries, <a class=""topText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""topText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""topText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality " & brandName & " wireless accessories online!"
		topText = topText & "<br><br>With our discount <a class=""topText-link"" href=""http://www.wirelessemporium.com/"">" & brandName & " cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your " & brandName & " mobile phone!"
	case "14"
'		SEtitle = "BlackBerry Accessories: BlackBerry Holster, Battery & Charger"
'		SEdescription = "Buy BlackBerry accessories like BlackBerry holsters, faceplates, chargers and much more at discounted prices only at Wireless Emporium ?the #1 cell phone accessory store online."
'		SEkeywords = "blackberry accessories, blackberry holster, blackberry battery, blackberry charger, blackberry faceplates"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your " & brandName & " brand cell phone. Choose from 1000's of quality " & brandName & " accessories including leather cases, pouches, batteries, <a class=""topText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""topText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""topText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality " & brandName & " wireless accessories online!"
		topText = topText & "<br><br>With our discount <a class=""topText-link"" href=""http://www.wirelessemporium.com/"">" & brandName & " cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your " & brandName & " mobile phone!"
	case "16"
'		SEtitle = "Buy Quality Palmone Cell Phone Accessories, Batteries, Antenna, Holsters, Faceplates, Cases, Keypads, Covers, Mobile Chargers"
'		SEdescription = "Buy quality Palmone cell phone accessories, at discounted rates. Products include chargers, batteries, keypads, covers, faceplates & more. Buy Online!!"
'		SEkeywords = "palmone accessory, palmone cell phoneaccessories, palmone mobile phone accessory, wireless phone headsets, mobile phone holsters, mobile phone headsets, mobile phone chargers, mobile phone cases, mobile phone batteries, mobile phone accessories, mobile handsfree, faceplates for cell phones"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your " & brandName & " brand cell phone. Choose from 1000's of quality " & brandName & " accessories including leather cases, pouches, batteries, <a class=""topText-link"" href=""/match.asp?typeID=5&brandID=" & brandIDval & """>Bluetooth headsets</a>, all kinds of chargers and much more. Wireless Emporium is also the largest seller of <a class=""topText-link"" href=""/cell-phone-covers-faceplates-screen-protectors.asp"">cell phone covers</a>, and <a class=""topText-link"" href=""/sc-3-sb-" & brandIDval & "-" & formatSEO(brandName) & "-faceplates-screen-protectors.asp"">cell phone faceplates</a> to personalize and accentuate your phone. No one sells more quality " & brandName & " wireless accessories online!"
		topText = topText & "<br><br>With our discount <a class=""topText-link"" href=""http://www.wirelessemporium.com/"">" & brandName & " cell phone accessories</a>, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your " & brandName & " mobile phone!"
	case "17"
'		SEtitle = "iPhone Accessories | iPhone Cases | iPod Accessories | iPod Touch Accessories | WirelessEmporium.com"
'		SEdescription = "Buy iPhone accessories from Wireless Emporium - the #1 store online for cell phone accessories like iPhone covers, headsets, leather cases and much more."
'		SEkeywords = "iphone accessories, iphone covers, iphone headset, iphone leather case, iphone accessory, iphone 3g accessories"
'		h1 = "iPhone Accessories - iPod Accessories - iPod Touch Accessories"
		titleText = "Discount iPhone cases, iphone chargers, iPhone face plates, iPhone holsters and clips and many other iPhone Accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct iPhone accessories for your Apple iPhone & iPod accessories for your iPod. The iPhone is THE killer next generation device from Apple to change the way cell phone users operate and Wireless Emporium is poised to offer the very latest and greatest iPhone accessories for you at prices up to 75% off retail! Choose from 1000s of quality iPhone accessories including protective iPhone cases, pouches, batteries, Bluetooth headsets, <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-charms-bling-kits.asp"" title=""Cell Phone Charms"">cell phone charms</a>, iPhone chargers and much more. Buy the best <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Bluetooth Headset"">Bluetooth headset</a>, protective iPhone leather cases, <a class=""topText-link"" href=""http://www.wirelessemporium.com/iphone-accessories.asp"" title=""iPhone Headset"">iPhone headset</a>, iPhone covers and many other quality iPhone accessories ?only at Wireless Emporium. As an invaluable communication tool, it's important to protect your investment with the right iPhone case, and Wireless Emporium has you covered with the largest selection of factory-direct iPhone cases in various styles and colors. Have an iPod too? Every iPod user needs to be equipped with the very best iPod accessories including iPod chargers, iPod speakers, iPod headphones and more!"
		topText = topText & "<br><br>Wireless Emporium is the #1 online resource for iPod Touch accessories and the largest seller of iPod accessories like leather cases, hands-free headsets and other accessories to personalize, optimize and protect your iPod."
		topText = topText & "<br><br>No one sells more quality iPod accessories and <a class=""topText-link"" href=""http://www.wirelessemporium.com/iphone-accessories.asp"" title=""iPhone Accessories"">iPhone accessories</a> online! iPod accessories and iPhone cellular accessories, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your iPod and iPhone!"
		bottomText = "Wireless Emporium is the Number One Online Resource for <a class=""topText-link"" href=""http://www.wirelessemporium.com"" title=""Cell Phone Accessories"">cell phone accessories</a> and the largest seller of <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-cases.asp"" title=""Cell Phone Cases"">cell phone cases</a>, cell phone covers, cell phone chargers, cell phone batteries and other accessories to personalize, optimize and protect your phone."
	case "18"
'		SEtitle = "Pantech Accessories: Pantech Duo & Matrix Accessories | Matrix Cases"
'		SEdescription = "Buy Pantech Cell Phone Accessories - Pantech Duo Accessories and Pantech Matrix Accessories like cases and much more at discounted prices only at Wireless Emporium."
'		SEkeywords = "pantech accessories, pantech cell phone accessories, pantech duo, pantech matrix accessories, pantech matrix cases, pantech data cable, pantech matrix accessories"
'		h1 = "Pantech&nbsp;Cell&nbsp;Phone&nbsp;Accessories"
		titleText = "Discount Pantech Cell Phone Chargers, Batteries &amp; Other Pantech Accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your Pantech brand cell phone. Choose from 1000s of quality Pantech accessories including leather cases, pouches, batteries, Bluetooth headsets, Pantech Matrix accessories like <a class=""topText-link"" href=""http://www.wirelessemporium.com/p-22435-large-neoprene-pouch-for-pantech-matrix-c740.asp"" title=""Pantech Matrix Cases"">Pantech Matrix Cases</a>, all kinds of chargers like <a class=""topText-link"" href=""http://www.wirelessemporium.com/p-22425-pantech-matrix-c740-car-charger.asp"" title=""Pantech Matrix Car Charger"">Pantech Matrix car charger</a>, Pantech Duo accessories and much more. Wireless Emporium is also the largest seller of <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a>, cell phone faceplates &amp; <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-charms-bling-kits.asp"" title=""Cell Phone Charms"">cell phone charms</a> to personalize and accentuate your phone. Wireless Emporium is itself the epitome of quality and reliability, and its top class cell phone accessories like Pantech Matrix and Pantech Duo accessories are just a simple example of the credibility of its products. No one sells more quality Pantech wireless accessories online! With our discount Pantech cell phone accessories, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Pantech mobile phone!"
		bottomText = "Wireless Emporium is the Number One Online Resource for cell phone accessories and the largest seller of cell phone cases, cell phone covers, cell phone chargers, <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-batteries.asp"" title=""Cell Phone Batteries"">cell phone batteries</a> and other accessories to personalize, optimize and protect your phone."
	case "19"
'		SEtitle = "Sidekick LX Accessories, Sidekick 2008 Battery & Sidekick Faceplates"
'		SEdescription = "Buy Sidekick cell phone accessories like Sidekick batteries, cases, faceplates and much more at discounted prices only at Wireless Emporium ?the #1 cell phone accessory store online."
'		SEkeywords = "sidekick LX accessories, sidekick 2008 accessories, sidekick faceplates, sidekick slide cases, sidekick 2008 battery"
'		h1 = "Sidekick&nbsp;2008&nbsp;&amp;&nbsp;LX&nbsp;Accessories"
		titleText = "Discount Sidekick Cell Phone Chargers, Batteries &amp; Other Sidekick Accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories "
		topText = topText & "for your Sidekick brand cell phone. Choose from 1000's of quality Sidekick accessories including leather cases, pouches, "
		topText = topText & "batteries, Bluetooth headsets, all kinds of chargers and much more. "
		topText = topText & "Wireless Emporium is also the largest seller of cell phone covers, cell phone faceplates &amp; "
		topText = topText & "<a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-charms-bling-kits.asp"" title=""Cell Phone Charms"">cell phone charms</a> "
		topText = topText & "to personalize and accentuate your phone. "
		topText = topText & "<a class=""topText-link"" href=""http://www.wirelessemporium.com/T-619-accessories-t-mobile-sidekick-2008.asp"" title=""Sidekick 2008 Accessories"">Sidekick 2008 accessories</a>, "
		topText = topText & "<a class=""topText-link"" href=""http://www.wirelessemporium.com/T-534-accessories-t-mobile-sidekick-lx.asp"" title=""Sidekick LX Accessories"">Sidekick LX accessories</a> like "
		topText = topText & "<a class=""topText-link"" href=""http://www.wirelessemporium.com/p-18406-sidekick-lx-silicone-case--hot-pink-.asp"" title=""Sidekick LX Cases"">Sidekick LX cases</a> "
		topText = topText & "&amp; much more at extremely discounted prices. "
		topText = topText & "No one sells more quality Pantech wireless accessories online!"
		topText = topText & "<br><br>With our discount Sidekick cell phone accessories, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your Sidekick mobile phone!"
	case "20"
'		SEtitle = "HTC Accessories: HTC Touch Pro Accessories, Touch Case, HTC Headset, HTC Diamond Accessories"
'		SEdescription = "Buy HTC cell phone accessories like HTC Touch Pro Accessories, batteries, cases, faceplates and much more at discounted prices only at Wireless Emporium!"
'		SEkeywords = "htc accessories, htc cell phone accessories, htc touch case, htc headset, htc touch pro accessories, htc diamond accessories"
'		h1 = "HTC&nbsp;Accessories"
		titleText = "Discount HTC Cell Phone Chargers, Batteries &amp; Other HTC Accessories"
		topText = "Wireless Emporium offers the largest selection of factory-direct cell phone accessories for your HTC brand cell phone. Choose from 1000s of quality HTC accessories including HTC Touch Pro accessories, leather cases, pouches, batteries, Bluetooth headsets, all kinds of chargers and much more. Wireless Emporium is also the largest seller of HTC cell phone covers, HTC <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-charms-bling-kits.asp"" title=""Cell Phone Charms"">cell phone charms</a> &amp; HTC cell phone faceplates to personalize and accentuate your phone. No one sells more quality HTC wireless accessories online! Wireless Emporium is itself the epitome of quality and reliability, and its top class cell phone accessories like HTC Touch Pro Accessories are just a simple example of the credibility of its products. Buy the best <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Bluetooth Headset"">Bluetooth headset</a>, <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-537-cell-accessories-htc-touch-verizon-xv6900.asp"" title=""HTC Touch Case"">HTC touch case</a>, <a class=""topText-link"" href=""http://www.wirelessemporium.com/p-17655-oem-htc-stereo-hands-free-headset-emc220.asp"" title=""HTC Headset"">HTC headset</a> and many other HTC Diamond accessories &amp; HTC touch accessories. With our discount HTC cell phone accessories, rapid processing, fast FREE SHIPPING and 100% satisfaction guarantee, there is no better place to accessorize your HTC mobile phone!"
		bottomText = "Wireless Emporium is the Number One Online Resource for cell phone accessories and the largest seller of cell phone cases, <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a>, cell phone chargers, cell phone batteries and other accessories to personalize, optimize and protect your phone."
end select

if SEtitle 			= "" 	then SEtitle = "Buy " & optCarrierName(carrierName) & " " & optBrandName(brandName) & " Accessories | Wireless Emporium"
if SEdescription 	= "" 	then SEdescription = "Buy the latest " & optCarrierName(carrierName) & " " & optBrandName(brandName) & " Accessories from Wireless Emporium ?the #1 store for Cell Phone Accessories."
if SEkeywords 		= "" 	then SEkeywords = optCarrierName(carrierName) & " " & optBrandName(brandName) & " accessories, " & optBrandName(brandName) & " accessories, " & optCarrierName(carrierName) & " accessories, " & optCarrierName(carrierName) & " " & optBrandName(brandName) & " phone cases, " & optCarrierName(carrierName) & " " & optBrandName(brandName) & " phone covers, " & optCarrierName(carrierName) & " " & optBrandName(brandName) & " phone accessories"
if titleText 		= "" 	then titleText = "Discount " & optBrandName(brandName) & " Cell Phone Chargers, Batteries &amp; Other " & optBrandName(brandName) & " Accessories"
if h1 				= "" 	then h1 = optCarrierName(carrierName) & " " & optBrandName(brandName) & " Accessories"

dim oRsTypes
dim strBreadcrumb
strBreadcrumb = optCarrierName(carrierName) & " " & optBrandName(brandName) & " Accessories"
%>
<!--#include virtual="/includes/template/top.asp"-->
<script>
window.WEDATA.pageType = "carrierBrand";
window.WEDATA.pageData = {
	carrier: '<%= carrierName %>',
	carrierId: '<%= carrierId %>',
	brand: '<%= brandName %>',
	brandId: '<%= brandId %> '
};
</script>

<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="/car-<%=carrierid & "-" & formatSEO(carrierName)%>"><%=optCarrierName(carrierName)%> Accessories</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
        </td>
    </tr>
    <tr>
        <td valign="top" width="100%" class="smltext" background="/images/newbrands/banner_<%=formatSEO(brandName)%>.jpg" style="background-repeat:no-repeat;">
            <p><img src="/images/spacer.gif" width="1" height="110" border="0"></p>
            <h1><%=h1%></h1>
            <p align="justify">
                <strong><%=titleText%></strong>
                <br>
                <%=topText%>
            </p>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%">
            <table cellspacing="0" cellpadding="0" border="0" class="thin-border" width="500" align="center">
                <tr>
                    <td align="center" valign="middle">
                        <img src="/productpics/carriers/<%=carrierCode%>/logo.jpg" alt="<%=carrierName%> - Available <%=brandName%> Cell Phone Models" width="176" height="105" border="0"><br>
                        <img src="/images/brand_icons/<%=formatSEO(brandName)%>.gif" align="center" border="0" alt="<%=brandName%>"><br>
                    </td>
                    <td class="smlText" valign="middle">
                        <p align="justify">
                            Here are the specific <%=brandName%> phone models that are offered by <%=carrierName%>.
                            If you do not find your model here, <a class="topText-link" href="/<%=brandSEO(brandID)%>">CLICK HERE</a>
                            to shop and select cell phone accessories from all <%=requestedbrand%> phones.
                        </p>
                    </td>
                    <td class="smlText" valign="middle">
                        <img src="/images/spacer.gif" width="30" height="1" border="0">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%">
            <br>
            <form>
                Please Select Your Phone Model&nbsp;&nbsp;&nbsp;
                <select name="selectModel" onChange="if(this.value != ''){window.location=this.value;}">
                    <option value="">Select from this list or click on an image below</option>
                    <%
                    do until RS.eof
                    %>
                    <option value="/<%=formatSEO(RS("brandName")) & "-" & formatSEO(RS("modelName"))%>-accessories"><%=RS("brandName")%> <%=RS("modelName")%></option>
                    <%
                        RS.movenext
                    loop
                    RS.movefirst
                    %>
                </select>
            </form>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%">
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <%
                    a = 0
                    intPhones = 0
                    do until RS.eof
                        if rs("international") and intPhones = 0 then
                    %>
                </tr>
                <tr>
                    <td id="intPhones" align="left" style="font-size:18px; font-weight:bold;" colspan="5" nowrap="nowrap">
                        <form id="formInt" name="formInt" method="post">
                            International Phones (<a href="#" onclick="formInt.submit()">Hide International Phones</a>)
                            <input type="hidden" name="showIntPhones" value="0" />
                        </form>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="5"><br><img src="/images/grayline_800.jpg" width="800" height="2" border="0"><br><br></td>
                </tr>
                <tr>
                    <%
                            intPhones = 1
                        end if
                        modelID = RS("modelID")
                        modelName = RS("modelName")
                        modelImg = RS("modelImg")
                        if brandID = "17" then
                            altText = modelName
                        elseif brandID = "4" then
                            altText = "LG Accessories: LG " & modelName & " Accessories"
                        elseif brandID = "18" then
                            altText = "Pantech Accessories: Pantech " & modelName & " Accessories"
                        elseif brandID = "9" then
                            altText = "Samsung Cell Phone Accessories: Samsung " & modelName & " Accessories"
                        elseif brandID = "20" then
                            altText = "HTC Accessories: HTC " & modelName & " Accessories"
                        else
                            altText = brandName & " " & modelName
                        end if
                        %>
                        <td align="center" valign="top" width="200">
                            <table border="0" cellspacing="0" cellpadding="0" width="200">
                                <tr>
                                    <td align="center" valign="top" width="200">
                                        <table border="0" cellspacing="0" cellpadding="0" width="200">
                                            <tr>
                                                <td align="center" valign="middle">
                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td align="left" valign="bottom" height="18" colspan="2"><img src="/images/spacer.gif" width="10" height="1"></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" valign="top" height="150" width="100">
                                                                <%if modelID = 940 then%>
                                                                <a class="cellphone-font" href="/apple-ipad-accessories"><img src="/productPics/models/<%=modelImg%>" border="0" alt="<%=altText%> Accessories"></a>
                                                                <%else%>
                                                                <a href="<%=formatSEO(brandName) & "-" & formatSEO(modelName)%>-accessories"><img src="/productPics/models/<%=modelImg%>" border="0" alt="<%=altText%> Accessories"></a>
                                                                <%end if%>
                                                            </td>
                                                            <td align="center" valign="top" width="5"><img src="/images/spacer.gif" width="5" height="1" border="0"></td>
                                                            <td align="center" valign="top" width="95">
                                                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                    <tr>
                                                                        <td align="left" valign="top" class="cellphone-category-font">
                                                                            <%if modelID = 940 then%>
                                                                            <a class="cellphone-font" href="/apple-ipad-accessories" title="<%=altText%> Accessories"><%=replace(modelName,"/"," / ")%></a>
                                                                            <%else%>
                                                                            <a class="cellphone-font" href="/<%=formatSEO(brandName) & "-" & formatSEO(modelName)%>-accessories" title="<%=altText%> Accessories"><%=replace(modelName,"/"," / ")%></a>
                                                                            <%end if%>
                                                                            <br>
                                                                            <%
                                                                            response.write "<a class=""cellphone-category-font"" href=""/hf-sm-" & modelID & "-" & formatSEO(brandName) & "-" & formatSEO(modelName) & "-bluetooth-headsets-headphones"" title=""Hands-Free Kits &amp; Bluetooth Headsets for " & altText & """>Handsfree</a><br>" & vbcrlf
                                                                            SQL = "SELECT DISTINCT A.typeid,B.typename FROM we_items A INNER JOIN we_types B ON A.typeid = B.typeid WHERE A.modelid = " & modelID & " AND A.inv_qty <> 0 AND A.hideLive = 0"
                                                                            set RStypes = Server.CreateObject("ADODB.Recordset")
                                                                            RStypes.open SQL, oConn, 0, 1
                                                                            do until RStypes.eof
                                                                                response.write "<a class=""cellphone-category-font"" href=""/sb-" & brandID & "-sm-" & modelID & "-sc-" & RStypes("typeid") & "-" & formatSEO(RStypes("typename")) & "-" & formatSEO(brandname) & "-" & formatSEO(modelName) & " title=""" & nameSEO(RStypes("typename")) & " for " & altText & """>" & RStypes("typename") & "</a><br>" & vbcrlf
                                                                                RStypes.movenext
                                                                            loop
                                                                            %>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="middle"><img src="/images/spacer.gif" width="1" height="6"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <%
                        RS.movenext
                        if not rs.EOF then
                            if showIntPhones = 0 and rs("international") then
                                displayIntLink = 1
                                do while not rs.EOF
                                    rs.movenext
                                loop
                            elseif rs("international") and a > 0 and intPhones = 0 then
                                response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""5""><br><img src=""/images/grayline_800.jpg"" width=""800"" height=""2"" border=""0""><br><br></td></tr><tr>" & vbcrlf
                                a = 0
                            end if
                        end if
                        a = a + 1
                        if a = 4 then
                            response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""4""><br><img src=""/images/grayline_800.jpg"" width=""800"" height=""2"" border=""0""><br><br></td></tr><tr>" & vbcrlf
                            a = 0
                        end if
                    loop
                    if a = 1 then
                        response.write "<td width=""200"">&nbsp;</td><td width=""200"">&nbsp;</td><td width=""200"">&nbsp;</td>" & vbcrlf
                    elseif a = 2 then
                        response.write "<td width=""200"">&nbsp;</td><td width=""200"">&nbsp;</td>" & vbcrlf
                    elseif a = 3 then
                        response.write "<td width=""200"">&nbsp;</td>" & vbcrlf
                    end if
                    %>
                </tr>
            </table>
        </td>
    </tr>
    <% if displayIntLink = 1 then %>
    <tr>
        <td align="center" valign="top" colspan="5"><br><img src="/images/grayline_800.jpg" width="800" height="2" border="0"><br><br></td>
    </tr>
    <tr>
        <td align="left">
            <form id="formInt" name="formInt" method="post" action="/car-<%=carrierID%>-b-<%=brandID%>-<%=formatSEO(carrierName)%>-<%=formatSEO(brandName)%>-phone-accessories#intPhones">
                <a href="#" onclick="formInt.submit()" style="font-size:18px; font-weight:bold;">Show International Phones</a>
                <input type="hidden" name="showIntPhones" value="1" />
            </form>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" colspan="5"><br><img src="/images/grayline_800.jpg" width="800" height="2" border="0"><br><br></td>
    </tr>
    <% end if %>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->