<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
response.buffer = true
googleAds = 1
dim carrierid : carrierid = prepInt(request.querystring("carrierid"))
dim cpass : cpass = prepInt(request.QueryString("cpass"))
dim categoryid : categoryid = prepInt(request.querystring("categoryID"))

noProducts = false
pageName = "category"

if carrierid > 0 then
	dim URL
	SQL = "exec sp_carrierData " & carrierID
	set RS = oConn.execute(sql)
	if not RS.eof then URL = "/car-" & carrierID & "-" & formatSEO(RS("carrierName")) & "-accessories"
	RS.close
	set RS = nothing
	if URL = "" then
		call PermanentRedirect("/")
	else
		call PermanentRedirect(URL)
		response.end
	end if
end if

if categoryid = 0 then
	call PermanentRedirect("/")	
elseif categoryid = 1310 or categoryid = 1320 then
	call PermanentRedirect("/phone-dust-plugs-charms")
end if


dim categoryName, parentTypeID, categoryNameSEO
if categoryid > 999 then
	pageName = "subcategory"
else
	pageName = "category"
end if	
sql = "exec sp_categoryData " & categoryid
session("errorSQL") = SQL
set RS = oConn.execute(sql)

strSubTypeID = ""
if not RS.eof then
	parentCatNameSEO = RS("parentTypeNameSEO")
	parentCatName = RS("parentTypeName")
	categoryName = RS("typeName")
	parentTypeID = RS("typeid")
	categoryNameSEO = RS("typeNameSEO")
	do until RS.eof
		strSubTypeID = strSubTypeID & RS("subtypeid") & ","
		RS.movenext
	loop
	strSubTypeID = left(strSubTypeID, len(strSubTypeID) - 1)
end if
if strSubTypeID = "" then strSubTypeID = "9999999" end if

if cpass = 1 then
	sql = "exec sp_cpassData"
else
	sql = "exec sp_brandsByCategory '" & strSubTypeID & "'"
end if
'response.write "<pre>" & sql & "</pre>"
arrRS = getDbRows(sql)
if isnull(arrRS) then
	noProducts = true
end if

if "" = strBreadcrumb then
	strBreadcrumb = "Cell Phone " & nameSEO(categoryName)
end if
if parentTypeID = 2 or parentTypeID = 18 or parentTypeID = 19 or parentTypeID = 20 then
	isNewHeader = true
end if
%>
<!--#include virtual="/includes/template/top.asp"-->
<script>
	window.WEDATA.pageType = 'category';
	window.WEDATA.categoryData = {
		categoryId: <%=categoryid %>,
		categoryName: '<%=nameSEO(categoryName)%>'
	};
</script>

<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
        	<%
			if categoryid > 1000 then
			%>
			<a class="breadcrumb" href="/">HOME</a>
			<%
				if parentTypeID = 5 then 
					response.write "Mobile Hands Free Kits"
				else
					response.write parentCatNameSEO
				end if
				%>
			</a>&nbsp;&gt;&nbsp;
			<%=strBreadcrumb%>
            <%
			else
			%>
            <a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
            <%
			end if
			%>
        </td>
    </tr>
	<%
	if prepStr(SeTopText) <> "" then topText = SeTopText
	if isNewHeader then
	%>
    <tr>
        <td width="100%" align="center" valign="top" style="padding-top:5px;">
			<div style="border-top:2px solid #b9b9b9;border-bottom:2px solid #b9b9b9; width:745px; height:83px; float:left;">
				<div style="float:left; width:<%=headerImgWidth%>px; height:83px;">
					<img src="/images/categories/<%=categoryid%>/catLogo.jpg" width="<%=headerImgWidth%>" height="<%=headerImgHeight%>" border="0" title="Cell Phone <%=nameSEO(categoryName)%>" />				
				</div>
				<div style="float:left; width:<%=735 - headerImgWidth%>px; color:#4B4B4B; text-align:left; padding:3px 0px 0px 10px; font-size:11px;">
					<% 
					if H1tag <> "" then 
						if h1Style <> "" then
							response.write "<h1 style=""" & h1Style & """>" & H1tag & "</h1> &nbsp; "
						else
							response.write "<h1>" & H1tag & "</h1> &nbsp; "
						end if
					end if 
					%>
					<%=topText%>
				</div>
			</div>
        </td>
    </tr>	
	<%
	else
	%>
    <tr>
        <td width="100%" align="center" valign="top">
            <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                <tr>
                	<td width="*" style="padding-right:10px;">
                        <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                            <tr>                    
                            	<td style="border-bottom:1px solid #ccc; height:60px;" valign="bottom" align="left">
	                                <h1 class="category"><%=H1tag%></h1>
                                </td>
							</tr>
                            <tr>                    
                            	<td valign="top" align="left" style="height:60px;">
									<h2 class="category"><%=H2tag%></h2>
                                	<%=topText%>
                                </td>
							</tr>
						</table>
                    </td>
                    <td width="200" align="right"><img src="/images/cats/we_header_<%=parentTypeID%>.jpg" border="0" alt="<%=H1tag%>" title="<%=H1tag%>" /></td>
                </tr>
			</table>
        </td>
    </tr>
	<%
	end if

	if noProducts then
	%>
    <tr>
        <td align="center" valign="top" width="100%" style="padding-top:20px;">
        	<h3>No Products Matched Your Query!</h3>
        </td>
	</tr>
    <%
	else
	%>    
    <tr>
    	<td style="padding-top:15px;">
            <div class="graybox-middle" style="width:726px; border-left:1px solid #999; border-right:1px solid #999; padding:0px 10px 0px 10px;">
                <div class="graybox-inner" title="Choose from the following" align="right">
                	<form name="frmSearch" method="post" style="float:right;">
                        <select name="cbModel" onChange="if(this.value != ''){window.location=this.value;}">
                            <option value="">Choose from the following</option>
                            <%
                            for nRows=0 to ubound(arrRS, 2)
                                brandid = cint(arrRS(1,nRows))
                                brandName = replace(arrRS(0,nRows), "/", " / ")
                                anchorText = brandName & " " & nameSEO(categoryName)
                                if brandid = 17 then
                                %>
                                <option value="/sc-<%=categoryID & "-sb-" & brandid & "-" & formatSEO(brandName) & "-iphone-" & formatSEO(categoryName)%>">
                                    <%=brandName & " iPhone " & nameSEO(categoryName)%>
                                </option>
									<%
                                    if categoryid <> 15 then
                                    %>
                                    <option value="/sc-<%=categoryID & "-sb-" & brandid & "-" & formatSEO(brandName) & "-ipod-ipad-" & formatSEO(categoryName)%>">
                                        <%=brandName & " iPod/iPad " & nameSEO(categoryName)%>
                                    </option>
                                <%
									end if
                                else
                                %>
                                <option value="/sc-<%=categoryid%>-sb-<%=brandid%>-<%=formatSEO(anchorText)%>"><%=anchorText%></option>
                                <%
                                end if
                            next
                            %>
                        </select>
					</form>
                	<span style="font-weight:bold; float:right;">Brands:&nbsp;&nbsp;</span>
				</div>
            </div>
        </td>
    </tr>
	<tr>
        <td width="100%" align="center" style="padding:5px 0px 15px 0px;">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="background:url(/images/cats/gray-header-right.jpg) repeat-x;">
            	<tr>
                	<td style="background:url(/images/cats/gray-header-left.jpg) no-repeat; width:330px; height:30px; color:#fff; font-size:14px; padding:4px 0px 0px 10px;" align="left" valign="top">
						<b>SELECT YOUR BRAND</b>
					</td>
                </tr>
			</table>        
        </td>
	</tr>
	<tr>
        <td width="100%" align="center" style="padding-bottom:20px;">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="684">
                <tr>
                
                    <%
                    dim others : others = 0
                    a = 0
					for nRows=0 to ubound(arrRS, 2)
						brandid = cint(arrRS(1,nRows))
						brandName = replace(arrRS(0,nRows), "/", " / ")
						strLink = "/sc-" & categoryID & "-sb-" & brandid & "-" & formatSEO(brandName) & "{-}" & formatSEO(categoryName)
						altText = brandName & "{}" '& nameSEO(categoryName) 'Removed to prevent duplicate content penalty
						
						if categoryid = 15 then
							strLink = "/sb-" & brandid & "-sm-0-sc-" & categoryid & "-" & formatSEO(brandName) & "-cell-phones-tablets"
							altText = brandName & "{}" '& nameSEO(categoryName) 'Removed to prevent duplicate content penalty
						end if
						
                        if brandid = 15 then
							if categoryid = 15 then
								otherLink = "/sb-" & brandid & "-sm-0-sc-" & categoryid & "-" & formatSEO(brandName) & "{-}" & formatSEO(categoryName)
								otherText = brandName & "{}" & nameSEO(categoryName)
							else
								otherLink = "/sc-" & categoryID & "-sb-" & brandid & "-" & formatSEO(brandName) & "{-}" & formatSEO(categoryName)
								otherText = brandName & "{}" & nameSEO(categoryName)
							end if							
                            others = 1
                        else
							if brandid = 17 then
								if categoryid = 7 then
							%>
								<td width="171" height="75" align="center" valign="top" style="padding:20px 10px 20px 10px; border-bottom:1px solid #ccc;">
                                    <a href="/apple-iphone-leather-carrying-cases-pouches" style="text-decoration:none;" title="<%=replace(altText, "{}", " iPhone ")%>">
										<img src="/images/cats/we_cat_brand_<%=brandid%>_1.jpg" border="0" title="<%=replace(altText, "{}", " iPhone ")%>" alt="<%=replace(altText, "{}", " iPhone ")%>" />
									</a><br>
									<h2><a href="/apple-iphone-leather-carrying-cases-pouches" title="Cell Phone <%=nameSEO(categoryName) & ": " & replace(altText, "{}", " iPhone ")%>" class="cellphone3-link"><%=replace(altText, "{}", " iPhone ")%></a></h2>
								</td>
								<%
								else
								%>
								<td width="171" height="75" align="center" valign="top" style="padding:20px 10px 20px 10px; border-bottom:1px solid #ccc;">
                                    <a href="<%=replace(strLink, "{-}", "-iphone-")%>" style="text-decoration:none;" title="<%=replace(altText, "{}", " iPhone ")%>">
										<img src="/images/cats/we_cat_brand_<%=brandid%>_1.jpg" border="0" title="<%=replace(altText, "{}", " iPhone ")%>" alt="<%=replace(altText, "{}", " iPhone ")%>" />
									</a><br>
									<h2><a href="<%=replace(strLink, "{-}", "-iphone-")%>" title="Cell Phone <%=nameSEO(categoryName) & ": " & replace(altText, "{}", " iPhone ")%>" class="cellphone3-link"><%=replace(altText, "{}", " iPhone ")%></a></h2>
								</td>
                                <%
								end if
								%>         
                                <% if categoryid <> 15 and categoryid <> 1031 then %>
								<td width="171" height="75" align="center" valign="top" style="padding:20px 10px 20px 10px; border-bottom:1px solid #ccc;">
                                    <a href="<%=replace(strLink, "{-}", "-ipod-ipad-")%>" style="text-decoration:none;" title="<%=replace(altText, "{}", " iPod/iPad ")%>">
										<img src="/images/cats/we_cat_brand_<%=brandid%>_0.jpg" border="0" title="<%=replace(altText, "{}", " iPod/iPad ")%>" alt="<%=replace(altText, "{}", " iPod/iPad ")%>" />
									</a><br>
									<h2><a href="<%=replace(strLink, "{-}", "-ipod-ipad-")%>" title="<%=replace(altText, "{}", " iPod/iPad ")%>" class="cellphone3-link"><%=replace(altText, "{}", " iPod/iPad ")%></a></h2>
								</td>
							<%
		                            a = a + 1
								end if
							else
                            %>
								<td width="171" height="75" align="center" valign="top" style="padding:20px 10px 20px 10px; border-bottom:1px solid #ccc;">
									<a href="<%=replace(strLink, "{-}", "-")%>" title="<%=replace(altText, "{}", " ")%>"><img src="/images/cats/we_cat_brand_<%=brandid%>.jpg" border="0" title="<%=replace(altText, "{}", " ")%>" alt="<%=replace(altText, "{}", " ")%>"></a>
									<br>
									<h2><a href="<%=replace(strLink, "{-}", "-")%>" title="Cell Phone <%=nameSEO(categoryName) & ": " & replace(altText, "{}", " Cell Phone ")%>" class="cellphone3-link"><%=replace(altText, "{}", " ")%></a></h2>
								</td>
                            <%
							end if
                            a = a + 1
                        end if
                        if a = 4 then
                            response.write "</tr><tr>" & vbcrlf
                            a = 0
                        end if
                    next
                    if others = 1 then
                        %>
                        <td width="171" height="75" align="center" valign="top" style="padding:20px 10px 20px 10px; border-bottom:1px solid #ccc;">
                            <a href="<%=replace(otherLink, "{-}", "-")%>" style="text-decoration:none;"><img src="/images/cats/we_cat_brand_15.jpg" border="0" alt="<%=replace(otherText, "{}", " ")%>"></a>
                            <br>
                            <h2><a href="<%=replace(otherLink, "{-}", "-")%>" title="Cell Phone <%=nameSEO(categoryName) & ": " & replace(otherText, "{}", " Cell Phone ")%>" class="cellphone3-link">Other Cell Phone <%=nameSEO(categoryName)%></a></h2>
                        </td>
                        <%
                        a = a + 1
                        if a = 4 then
                            response.write "</tr><tr>" & vbcrlf
                            a = 0
                        end if
                    end if
                    %>
                </tr>
            </table>
        </td>
    </tr>
    <% if categoryid = 7 then %>
    <tr>
	    <td align="center"><%'Youtube Video http://www.youtube.com/watch?v=wYR1ruVE8tk %>
	        <iframe width="468" height="263" src="http://www.youtube.com/embed/wYR1ruVE8tk?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;wmode=opaque" frameborder="0" allowfullscreen></iframe>
        </td>
    </tr>
    <% end if %>
    <!--#include virtual="/includes/asp/inc_bottomCategory.asp"-->
    <%
	end if
	%>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
    <tr>
        <td width="100%" align="center" valign="top" style="padding-bottom:10px;">
            <img src="/images/hline_2.jpg" width="100%" height="8" border="0">
        </td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->