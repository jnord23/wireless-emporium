<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
	noAsp = true
	call fOpenConn()
	pageName = "brandTablet"
	response.buffer = true
	googleAds = 1

	sql	=	"select	a.brandid, a.brandname, a.brandimg" & vbcrlf & _
			"	,	b.modelid, b.modelname, b.modelimg, b.temp, b.carrierCode" & vbcrlf & _
			"	,	b.releaseYear, b.releasequarter" & vbcrlf & _
			"from	we_brands a join we_models b" & vbcrlf & _
			"	on	a.brandid = b.brandid" & vbcrlf & _
			"where	a.brandType >= 1 and b.isTablet = 1" & vbcrlf & _
			"	and	b.hidelive = 0" & vbcrlf & _
			"order by b.temp desc, b.releaseyear desc, b.releaseQuarter desc, a.brandName" & vbcrlf

'	response.write "<pre>" & sql & "</pre>"
	session("errorSQL") = sql			
	set objRsTablet = oConn.execute(sql)
	
'	session("breadcrumb_brand") = ""
	
	SEtitle = "Buy Tablet Accessories from WirelessEmporium - Tablet Covers, Faceplates, Charms, Chargers, Batteries - Deep Discounts and Free Shipping"
	SEdescription = "The #1 Name in Tablet Accessories Online - Wireless Emporium. Discount Tablet Accessories at Prices up to 75% Off. Buy Wholesale Tablet Accessories, discount tablet accessories and cheap tablet accessories."
	SEkeywords = "cheap Tablet accessories,tablet accessories,e-reader accessories,ereader accessories,discount tablet accessories,wholesale tablet accessories,wireless accessories,cool tablet accessories"	
%>
<!--#include virtual="/includes/template/top.asp"-->
<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumb" style="padding-top:7px;">
            <a class="breadcrumb" href="/">HOME</a>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%" style="border-top:1px solid #ccc; border-bottom:1px solid #ccc;">
        	<div style="float:left; width:270px; padding:20px 0px 0px 10px;" align="left">
            	<h1 class="tablet">Tablet Accessories</h1><br />
                <h2 class="tablet">Tablet Cases, Covers & Batteries</h2>
            </div>
        	<div style="float:left; color:#2D2D2D; width:310px; padding:10px 10px 0px 10px; line-height:170%; font-size:11px;" align="left">
				<p>Tablets and E-Readers are relatively new, but are devices that make our everyday lives so much easier.  More portable than a laptop and more powerful than a smartphone, these devices are the perfect blend.  Protect your investment and enhance its performance with our full line of tablet accessories.</p>
			</div>
        	<div style="float:left; width:130px; padding-top:10px;">
				<img src="/images/brands/tablet_header.png" border="0" alt="Tablet Accessories" title="Tablet Accessories" />
            </div>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%" style="padding-top:10px;">
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>        
			<%
			set fs = Server.CreateObject("Scripting.FileSystemObject")			
			if not objRsTablet.eof then
				a = 0			
				do until objRsTablet.eof
					a = a + 1
					modelID 		= objRsTablet("modelid")
					modelName 		= objRsTablet("modelName")
					brandName 		= objRsTablet("brandName")
					brandName		= replace(brandName, "Other", "")
					modelImg 		= objRsTablet("modelimg")
					modelImgPath 	= "/productPics/models/thumbs/" & modelImg
					if not fs.FileExists(server.mappath(modelImgPath)) then
						modelImgPath = "/productPics/models/" & modelImg
					end if
					
					altTag 			= brandName & " " & modelName & " Accessories"
					if prepStr(brandName) = "" then brandName = "Other"
					modelLink 		= "/" & formatSEO(brandName & "-" & modelName) & "-accessories"
					if modelID = 940 then
						modelLink = "/apple-ipad-accessories"
					end if
					

					%>
                        <td width="19%" valign="top" style="border-bottom:1px solid #CCC;<% if a < 5 then %> border-right:1px solid #CCC;<% end if %> padding:10px 5px 5px 5px;">
                            <table border="0" cellpadding="0" cellspacing="0" align="center">
                                <tr>
                                    <td align="center" valign="top" style="height:150px;">
										<a href="<%=modelLink%>" title="<%=altTag%>"><img src="<%=modelImgPath%>" border="0" height="100" /></a><br />
                                        <a class="cellphone2-link" href="<%=modelLink%>" title="<%=altTag%>"><%=replace(brandName & " " & modelName,"/"," / ") & "&nbsp;Accessories"%></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
					<%
                    if a = 5 then
                        response.write "</tr><tr>" & vbcrlf
                        a = 0
                    end if
					objRsTablet.movenext
				loop
			end if
			%>
                </tr>
            </table>                            
        </td>
    </tr>    
    <tr>
        <td width="100%" align="center" valign="top" style="padding-top:30px;">
            <div align="justify" class="topText">
            	<p>When Apple introduced the iPad, it took the world by storm.  Since then several other tablet computers have made their way to the market, each with the capability to make our daily tasks easier and more enjoyable.  These devices aren't cheap however, and need to be properly cared for.  That's why we here at Wireless Emporium carry an extensive line of <a href="/tablet-ereader-accessories" class="bottomtext-link" title="tablet accessories">tablet accessories</a>.  The giant display on your tablet is the hub of everything you do.  A <a href="/screen-protectors" class="bottomtext-link" title="tablet screen protector">tablet screen protector</a> will prevent scratches and dings from damaging your screen and ruining your user experience.  We also carry a variety of tablet cases that not only protect your device, but can double as a convenient carrying case.
				<br><br>
				Get the most out of your tablet is also important.  A tablet keyboard or a <a href="/bluetooth-headsets-handsfree" class="bottomtext-link" title="bluetooth headsets">Bluetooth device</a> will help you be more productive as you navigate your to do list.  A spare tablet charger will help you stay powered up when you're away from home, whether you are in the car or at the office.  We even have numerous <a href="/cell-phone-data-cables-memory-cards" class="bottomlink-text" title="data cables">data cables</a> to keep your device synced up with your personal computer or other electronic devices.  As will all the accessories on our site, all of our tablet accessories are backed by our 90 day no hassle return policy, a 1 year warranty and come with free shipping.  Our knowledgeable customer service staff is also here to help you find the tablet accessory that's right for you.
                </p>
			</div>
        </td>
    </tr>	
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<%
call fCloseConn()
%>
<!--#include virtual="/includes/template/bottom.asp"-->