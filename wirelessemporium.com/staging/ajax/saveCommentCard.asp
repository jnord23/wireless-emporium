<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	
	call getDBConn(oConn)
	
'	sql = 	"insert into _mcafee_monitor(pageName, ipaddress, referrer, formString, queryString)" & vbcrlf & _
'			"values('/ajax/saveCommentCard.asp', '" & replace(Request.ServerVariables("REMOTE_ADDR"), "'", "''") & "', '" & replace(request.ServerVariables("HTTP_REFERER"), "'", "''") & "', '" & replace(request.form, "'", "''") & "', '" & replace(request.QueryString, "'", "''") & "')"
'	oConn.execute(sql)
	
	FUNCTION CDOSend(strTo,strFrom,strSubject,strBody)
		Dim objErrMail
		Set objErrMail = Server.CreateObject("CDO.Message")
		With objErrMail
			.From = strFrom
			.To = strTo
			.Subject = strSubject
			.HTMLBody = CStr("" & strBody)
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Item("urn:schemas:httpmail:sender") = "sales@wirelessemporium.com"
			.Configuration.Fields.Update
			.Send
		End With
		Set objErrMail = nothing
	END FUNCTION
	
	itemID = prepInt(request.QueryString("itemID"))
	email = prepStr(request.QueryString("email"))
	phone = prepStr(request.QueryString("phone"))
	comment = prepStr(request.QueryString("comment"))
	page = prepStr(request.QueryString("page"))
	recaptcha_challenge_field = prepStr(request("re_challenge"))
	recaptcha_response_field = prepStr(request("re_response"))
	recaptcha_privatekey = prepStr(request("re_privatekey"))
	
	recap_server_response = recaptcha_confirm(recaptcha_privatekey,recaptcha_challenge_field,recaptcha_response_field)

	remoteIPAddress = Request.ServerVariables("HTTP_X_FORWARDED_FOR")

	if "" <> recap_server_response then
		response.write "wrong recaptcha"
	elseif comment = "" then
		response.Write("No Comment Found")
	else
		session("errorSQL") = "comment:" & comment
		session("errorSQL2") = "qsVal:" & request.QueryString("comment")
		sql = ""
		fullComment = ""
		if cdbl(itemID) > 0 then
			fullComment = "New Comment on WE (" & request.ServerVariables("HTTP_USER_AGENT") & ") : <a href='http://www.wirelessemporium.com/p-" & itemID & "-commented-product.asp'>http://www.wirelessemporium.com/p-" & itemID & "-commented-product.asp</a>"
			if email <> "" then fullComment = fullComment & "<br />Email:&nbsp;" & email
			if phone <> "" then fullComment = fullComment & "<br />Phone:&nbsp;" & phone
			fullComment = fullComment & "<br />" & comment & "<br /><br />" & remoteIPAddress
			sql = "insert into we_commentCard (itemID,email,phone,comment,IP) values(" & itemID & ",'" & email & "','" & phone & "','" & comment & "','" & remoteIPAddress & "')"
		elseif page <> "" then
			fullComment = "New Comment on WE (" & request.ServerVariables("HTTP_USER_AGENT") & ") : <a href='http://www.wirelessemporium.com/" & page & "'>http://www.wirelessemporium.com/" & page & "</a>"
			if email <> "" then fullComment = fullComment & "<br />Email:&nbsp;" & email
			if phone <> "" then fullComment = fullComment & "<br />Phone:&nbsp;" & phone
			fullComment = fullComment & "<br />" & comment & "<br /><br />" & remoteIPAddress
			sql = "insert into we_commentCard (page,email,phone,comment,IP) values('" & page & "','" & email & "','" & phone & "','" & comment & "','" & remoteIPAddress & "')"
		end if
		
		if sql <> "" then
			session("errorSQL") = sql
			oConn.execute(sql)
			
			if left(comment,8) = "Testing!" then
				CDOSend "jon@wirelessemporium.com;terry@wirelessemporium.com","comments@wirelessemporium.com","WE - Comment Card",fullComment
			else
				CDOSend "edwin@wirelessemporium.com;philippe@wirelessemporium.com;sherwood@wirelessemporium.com;tony@wirelessemporium.com;ruben@wirelessemporium.com;jon@wirelessemporium.com;eugene@wirelessemporium.com;terry@wirelessemporium.com;wemarketing@wirelessemporium.com;wemerch@wirelessemporium.com;matt@wirelessemporium.com;muhammad@wirelessemporium.com","comments@wirelessemporium.com","WE - Comment Card",fullComment
			end if
			response.Write("commentGood")
		else
			response.Write("Insert command not found")
		end if
	end if
	
	call CloseConn(oConn)
	
	function recaptcha_confirm(recaptcha_private_key,rechallenge,reresponse)
		Dim VarString
			VarString = _
			"privatekey=" & recaptcha_private_key & _
			"&remoteip=" & Request.ServerVariables("REMOTE_ADDR") & _
			"&challenge=" & rechallenge & _
			"&response=" & reresponse
		
		Dim objXmlHttp
		Set objXmlHttp = Server.CreateObject("Msxml2.ServerXMLHTTP")
			objXmlHttp.open "POST", "http://www.google.com/recaptcha/api/verify", False
			objXmlHttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
			objXmlHttp.send VarString

		Dim ResponseString
			ResponseString = split(objXmlHttp.responseText, vblf)

		Set objXmlHttp = Nothing
		
		if ResponseString(0) = "true" then
			'They answered correctly
			recaptcha_confirm = ""
		else
			'They answered incorrectly
			recaptcha_confirm = ResponseString(1)
		end if
	end function
	
%>