<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	
%><!--#include virtual="/includes/account/account_base.asp"--><%	

	dim thisUser, pageTitle, header
	
	call getDBConn(oConn)
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%	
	dim loginStatus : loginStatus = prepInt(request.QueryString("loginStatus"))
	dim myCartDetails : myCartDetails = prepInt(request.QueryString("myCartDetails"))
	dim featuredProducts : featuredProducts = prepInt(request.QueryString("featuredProducts"))
	dim adminBar : adminBar = prepInt(request("adminBar"))
	adminBar = 0
	
	dim curPage : curPage = prepStr(request.QueryString("curPage"))
	dim brandID : brandID = prepInt(request("brandID"))
	dim modelID : modelID = prepInt(request("modelID"))
	dim categoryID : categoryID = prepInt(request("categoryID"))
	dim typeID : typeID = prepInt(request.QueryString("typeID"))
	dim itemID : itemID = prepInt(request("itemID"))
	dim basePageName : basePageName = prepStr(request.QueryString("basePageName"))
	dim brandModel : brandModel = prepStr(request("brandModel"))
	dim productUpdateType : productUpdateType = prepInt(request.QueryString("productUpdateType"))
	dim musicSkins : musicSkins = prepInt(request.QueryString("musicSkins"))
	
	dim SEtitle : SEtitle = prepStr(request.Form("SEtitle"))
	dim SEdescription : SEdescription = prepStr(request.Form("SEdescription"))
	dim SEkeywords : SEkeywords = prepStr(request.Form("SEkeywords"))
	dim SeTopText : SeTopText = prepStr(request.Form("SeTopText"))
	dim SeBottomText : SeBottomText = prepStr(request.Form("SeBottomText"))
	dim SeH1 : SeH1 = prepStr(request.Form("SeH1"))
	dim updatePage : updatePage = prepStr(request.Form("updatePage"))
	dim defaultTextPage : defaultTextPage = prepStr(request.Form("defaultTextPage"))
	dim useGeneric : useGeneric = prepInt(request.Form("useGeneric"))
	dim cms_basepage : cms_basepage = prepStr(request("cms_basepage"))
	dim cms_rewriteURL : cms_rewriteURL = prepStr(request("cms_rewriteURL"))

	if curPage = "" then curPage = cms_rewriteURL
	
	if loginStatus = 1 then	
		response.write "<div>"
		if isLoggedIn then
'			if session("accountEmail") = "doldding82@gmail.com" or session("accountEmail") = "doldding82@hotmail.com" or session("accountEmail") = "terry@wirelessemporium.com" then
		%>
        <a href="/account/logout" title="Logout"><div id="siteTop-Text" style="padding:6px 0px 0px 0px; font-size:13px; font-weight:bold;">Logout</div></a>
        <div id="siteTop-Blackdiv"></div>
        <a href="/account/account_details" title="My Account"><div id="siteTop-Text" style="padding:6px 0px 0px 5px; font-size:13px; font-weight:bold;">My Account</div></a>
        <div id="siteTop-Blackdiv"></div>
        <%
'			end if
		else
		%>
	    <a href="/account/login" title="Rewards Program">
			<div id="siteTop-RewardsProgram"></div>
			<div id="siteTop-Text" style="float:left; padding:6px 0px 0px 5px;">Rewards Program</div>
		</a>
        <div id="siteTop-Blackdiv"></div>
        <%
		end if
		response.write "</div>"
	elseif myCartDetails = 1 then
		mySession = prepInt(request.cookies("mySession"))
		
		sql = 	"select isnull(sum(a.qty), 0) totalQty, isnull(sum(isnull(a.customCost,b.price_our) * a.qty), 0) totalPrice1, isnull(sum(c.price_we * a.qty), 0) totalPrice2 " & vbcrlf & _
				"from shoppingcart a left join we_items b on a.itemid=b.itemid left join we_items_musicSkins c on a.itemID = c.id " & vbcrlf & _
				"where a.store = 0 and a.sessionid = '" & mySession & "' and (a.purchasedorderid is null or a.purchasedorderid = 0)" & vbcrlf
		session("errorSQL") = sql
		set objRsMiniCart = oConn.execute(sql)
		
		if objRsMiniCart.EOF then
			miniTotalQuantity = 0
			miniSubTotal = 0
		else
			miniTotalQuantity = formatnumber(objRsMiniCart("totalQty"), 0)
			miniSubTotal = formatcurrency(objRsMiniCart("totalPrice1") + objRsMiniCart("totalPrice2"))
		end if
%>
<div class="siteTopText" style="width:100%; padding-top:6px;" align="right"><%=miniTotalQuantity%> Item(s)<!-- <%=miniSubTotal%>-->&nbsp;&nbsp;</div>
<%
	elseif featuredProducts = 1 then
		saveBrandID = prepInt(request.Cookies("saveBrandID"))
		saveModelID = prepInt(request.Cookies("saveModelID"))
		saveItemID = prepInt(request.Cookies("saveItemID"))

		secID = "topBar-featuredPro-left"
		title = "Featured Products"
		
		sql = "exec sp_recommendedItemsForUserCookie 0," & prepInt(saveBrandID) & "," & prepInt(saveModelID) & "," & prepInt(saveItemID) & ",10"
		session("errorSQL") = sql
        set arrFeatured = oConn.execute(sql)
		
		if arrFeatured.EOF then
			response.Cookies("saveBrandID") = ""
			response.Cookies("saveModelID") = ""
			response.Cookies("saveItemID") = ""
			
			sql	=	"	select	top 10 d.brandName, e.modelName, b.we_url, (select top 1 cast(itemID as varchar(8)) + '##' + itemDesc from we_Items where partNumber = b.partNumber order by itemID desc) as itemdesc, b.itempic, b.price_retail, b.price_our, b.handsfreetype, c.typename " &_
					"	from	we_recommends a join we_items b " &_
					"		on	a.itemid=b.itemid join we_types c " &_
					"		on	b.typeid=c.typeid left join we_brands d on b.brandID = d.brandID left join we_models e on b.modelID = e.modelID" &_
					"	where	b.inv_qty <> 0 and b.hidelive = 0 and a.site_id = 0" &_
					"	order by a.sortorder"
			session("errorSQL") = sql
	        set arrFeatured = oConn.execute(sql)
		end if
%>
<div style="width:748px; height:30px;">
    <div id="<%=secID%>" title="<%=title%>"></div>
    <div id="topBar-middle" style="width:413px;"></div>
    <div id="topBar-right"></div>            
</div>
<div style="width:746px; height:528px; border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc;">
    <%
    nRows = 0
    totalItems = 0
	nItemDescMaxLength = 50
    set fso = CreateObject("Scripting.FileSystemObject")
    do while not arrFeatured.EOF
        nRows = nRows + 1
        totalItems = totalItems + 1
        useBrandName = arrFeatured("brandName")
        useModelName = arrFeatured("modelName")
        prodDetails = split(arrFeatured("itemDesc"),"##")
        itemID = prodDetails(0)
        itemDesc = prodDetails(1)
        altText = itemDesc
        price_retail = formatcurrency(prepInt(arrFeatured("price_retail")),2)
        price_our = formatcurrency(prepInt(arrFeatured("price_our")),2)
        productLink = "/p-" & itemid & "-" & formatSEO(itemDesc)
        if len(itemdesc) > nItemDescMaxLength then
            nextSpace = inStr(right(itemdesc, len(itemdesc) - nItemDescMaxLength), " ")
            itemdesc = left(itemdesc, nItemDescMaxLength + nextSpace) & "..."
        end if
        itempic = arrFeatured("itemPic")
		if instr(request.ServerVariables("SERVER_NAME"),"staging.") < 1 then
	        if fso.fileExists(server.MapPath("/productpics/thumb/" & itemPic)) then
    	        imageLink = "/productpics/thumb/" & itemPic
        	else
            	imageLink = "/productpics/thumb/imagena.jpg"
	        end if
		else
			imageLink = "/productpics/thumb/" & itemPic
		end if
    %>
    <div style="float:left; width:131px; height:244px; padding:15px 5px 5px 5px; border-bottom:1px solid #ebebeb; margin-left:2px;">
        <div style="width:100%; height:120px;" align="center">
            <% if prodRow = 3 then %>
            <% call ImageHtmlTagWrapper2("<img src=""/productpics/thumb/" & itempic & """ border=""0"" title=""" & altText & """ width=""100"" height=""100"" />",  productLink, "/images/saleIcon_index.png", true) %>
            <% else %>
            <a href="<%=productLink%>" title="<%=altText%>"><img src="<%=imageLink%>" border="0" title="<%=altText%>" /></a>
            <% end if %>
        </div>
        <div style="width:100%; height:48px; padding-top:10px;" align="left"><a href="<%=productLink%>" class="cellphone-link" title="<%=altText%>"><%=itemdesc%></a></div>
        <div style="width:100%; padding-top:10px;" align="left" class="index-price-retail">Was <del><%=price_retail%></del></div>
        <div style="width:100%;" align="left" class="index-price-our">Our Price <%=price_our%></div>
        <% if price_retail <> 0 then %>
        <div style="width:100%;" align="left"><b>Save <%=formatPercent((price_Retail - price_Our) / price_Retail,0)%></b></div>
        <% end if %>
    </div>
    <% if nRows < 5 then %><div style="float:left; width:5px; height:220px; margin-top:20px; border-right:1px solid #ebebeb;"></div><% end if %>
    <%
        arrFeatured.movenext
        if nRows = 5 then nRows = 0
        if totalItems = 10 then
            exit do
        elseif arrFeatured.EOF then
            sql	=	"	select	top " & 10-totalItems & " d.brandName, e.modelName, b.we_url, (select top 1 cast(itemID as varchar(8)) + '##' + itemDesc from we_Items where partNumber = b.partNumber order by itemID desc) as itemdesc, b.itempic, b.price_retail, b.price_our, b.handsfreetype, c.typename " &_
                    "	from	we_recommends a join we_items b " &_
                    "		on	a.itemid=b.itemid join we_types c " &_
                    "		on	b.typeid=c.typeid left join we_brands d on b.brandID = d.brandID left join we_models e on b.modelID = e.modelID" &_
                    "	where	b.inv_qty <> 0 and b.hidelive = 0 and a.site_id = 0" &_
                    "	order by a.sortorder"
            session("errorSQL") = sql
            set arrFeatured = oConn.execute(sql)
        end if
    loop
    %>
</div>
<%
	elseif adminBar = 1 then
		adminID = prepInt(session("adminID"))
		if adminID > 0 then
			sql = "select adminNav, marketingLvl from we_adminUsers where adminID = " & adminID
			session("errorSQL") = sql
			set adminRS = oConn.execute(sql)
			
			navBar = adminRS("adminNav")
			marketingLvl = adminRS("marketingLvl")
			if navBar then
				set fso = CreateObject("Scripting.FileSystemObject")
%>
	<div id="topSelect" style="width:100%; height:28px; background-color:#900; color:#FFF; font-weight:bold; font-size:12px; position:relative;">
        <div style="float:left; padding:5px 20px 0px 20px;">Admin Controls:</div>
        <div style="float:left; padding:5px 20px 0px 20px;"><a href="/admin" style="color:#FFF; font-weight:bold; font-size:12px;">Admin Menu</a><%=session("otherAdminOptions")%></div>
		        <%
				if marketingLvl > 2 then
					cms_siteid = 0
					cms_rewriteURL = curPage
					cms_basepage = cms_basepage
					cms_post = "/ajax/dynamicupdate.asp"
					%>
               		<!--#include virtual="/Framework/Utility/cms.asp"-->
    	            <%
				end if
				%>
    </div>
<%
				session("otherAdminOptions") = ""
			end if
		end if
	end if
	
	call CloseConn(oConn)
%>