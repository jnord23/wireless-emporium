<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	dim thisUser, pageTitle, header
	dim removeID : removeID = prepInt(request.QueryString("removeID"))
	dim addID : addID = prepInt(request.QueryString("addID"))
	dim addQty : addQty = prepInt(request.QueryString("addQty"))
	dim mySession : mySession = prepInt(request.cookies("mySession"))
	dim promo : promo = prepInt(request.QueryString("promo"))
	dim itemCnt : itemCnt = 0
	dim itemAmt : itemAmt = 0
	dim itemLap : itemLap = 0
	dim sWeight : sWeight = 0
	dim updateCartID : updateCartID = prepInt(request.QueryString("updateCartID"))
	dim updateQty : updateQty = prepInt(request.QueryString("updateQty"))
	dim customCost : customCost = request.QueryString("customCost")
	dim sPromoCode : sPromoCode = prepStr(request.Cookies("promocode"))

	if sPromoCode <> "" then
		'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber, couponid
		couponid = 0
		SQL = "select * from we_coupons where promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
		session("errorSQL") = SQL
		set rsPromo = Server.CreateObject("ADODB.Recordset")
		rsPromo.open SQL, oConn, 0, 1
		if not rsPromo.eof then
			couponid = rsPromo("couponid")
			promoMin = rsPromo("promoMin")
			promoPercent = rsPromo("promoPercent")
			typeID = rsPromo("typeID")
			excludeBluetooth = rsPromo("excludeBluetooth")
			BOGO = rsPromo("BOGO")
			couponDesc = rsPromo("couponDesc")
			FreeItemPartNumber = rsPromo("FreeItemPartNumber")
			setValue = rsPromo("setValue")
			oneTime = rsPromo("oneTime")
		else
			promoError = "Promo Code Not Found"
			sPromoCode = ""
		end if
	end if

	if customCost = "" or isnull(customCost) then customCost = "null" else customCost = prepStr(customCost)
	if not isnumeric(customCost) then customCost = "null"
	
	if updateCartID > 0 then
		if updateQty < 1 then
			sql = "delete from shoppingCart where id = " & updateCartID
		else
			sql = "update shoppingCart set qty = " & updateQty & " where id = " & updateCartID
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "select * from ShoppingCart where store = 0 and sessionID = '" & mySession & "' and lockQty = 0 and purchasedOrderID is null"
		session("errorSQl") = sql
		set testRS = oConn.execute(sql)
		
		if testRS.EOF then
			sql = "delete from ShoppingCart where store = 0 and sessionID = '" & mySession & "' and purchasedOrderID is null"
			session("errorSQl") = sql
			oConn.execute(sql)
		end if
	end if
%>
<div class="tb fcTopRow">
	<% if addID > 0 and addQty > 0 then %>
	<div class="fl fcAddedCheck"></div>
    <div class="fl fcAddedText">Item added to cart!</div>
    <% end if %>
    <div class="fr fcCloseCheck" onclick="popUpCart()"></div>
    <div class="fr fcCloseText" onclick="popUpCart()">Close</div>
</div>
<%
	sql = "exec sp_dropdownCart " & mySession & "," & removeID & "," & addID & "," & addQty & "," & promo & ",'" & request.ServerVariables("REMOTE_ADDR") & "', " & customCost
'	response.write sql
	session("errorSQL") = sql
	set fCartRS = oConn.execute(sql)

	do while not fCartRS.EOF
		itemLap = itemLap + 1
		fcID = prepInt(fCartRS("id"))
		fcItemID = prepInt(fCartRS("itemid"))
		fcQty = prepInt(fCartRS("qty"))
		fcPic = fCartRS("itemPic")
		fcDesc = prepStr(fCartRS("itemDesc"))
		fcPrice = prepInt(fCartRS("price_our"))
		fcRetail = prepInt(fCartRS("price_retail"))
		fcCustomCost = fCartRS("customCost")
		fcLocked = fCartRS("lockQty")
		sWeight = sWeight + fCartRS("itemWeight")
%>
<div class="tb fcItem">
	<div class="fl fcImg"><img src="/productpics/thumb/<%=fcPic%>" border="0" width="100" height="100" /></div>
    <div class="fl">
    	<div class="tb fcDesc"><%=fcDesc%></div>
        <div class="tb fcPriceRow">
        	<% if isnull(fcCustomCost) then %>
			<div class="fl fcPriceTitle">Our Price:</div>
			<div class="fl fcPrice"><%=formatCurrency(fcPrice,2)%></div>
            <%
			else
				fcPrice = fcCustomCost
			%>
            <div class="fl fcPriceTitle">Promo Price:</div>
            	<% if prepInt(fcCustomCost) = 0 then %>
            	<div class="fl fcPrice">FREE</div>
				<% else %>
				<div class="fl fcPrice"><%=formatCurrency(prepInt(fcCustomCost),2)%></div>
            	<% end if %>
            <% end if %>
        </div>
        <div class="tb fcPriceRow">
			<div class="fl fcRetailTitle">Retail Price:</div>
			<div class="fl fcRetail"><%=formatCurrency(fcRetail,2)%></div>
        </div>
    </div>
    <div class="tb qtyRow">
    	<div class="fl qtyTitle">Qty:</div>
        <% if fcLocked then %>
        <div class="fl qtyAmt2"><%=fcQty%></div>
        <% else %>
        <div class="fl qtyAmtAdj"><input id="qtyAdj_<%=fcID%>" type="text" style="width:20px;" value="<%=fcQty%>" /></div>
        <div class="fl fcRemove qtyAdjBttn" onclick="updateCartQty(<%=fcID%>)">UPDATE</div>
        <% end if %>
        <div id="popCartRemove" class="fl fcRemove" onclick="br_cartTracking('remove', '<%=fcItemID%>', '', '<%=replace(fcDesc, "'", "\'")%>', ''); popUpCart_remove(<%=fcID%>)" style="display:none;">REMOVE</div>
        <div id="popCartRemoveWithPromo" class="fl fcRemove" onclick="br_cartTracking('remove', '<%=fcItemID%>', '', '<%=replace(fcDesc, "'", "\'")%>', ''); popUpCartWithPromo_remove(<%=fcID%>)">REMOVE</div>
    </div>
</div>
<%
		PPhtml = PPhtml & 	"<input type=""hidden"" name=""L_NUMBER" & itemLap-1 & """ value=""" & fcItemID & """>" & vbcrlf & _
							"<input type=""hidden"" name=""L_NAME" & itemLap-1 & """ value=""" & fcDesc & """>" & vbcrlf & _
							"<input type=""hidden"" name=""L_QTY" & itemLap-1 & """ value=""" & fcQty & """>" & vbcrlf & _
							"<input type=""hidden"" name=""L_AMT" & itemLap-1 & """ value=""" & fcPrice & """>" & vbcrlf
		
		fCartRS.movenext
		itemCnt = itemCnt + fcQty
		itemAmt = itemAmt + (fcPrice * fcQty)
		if itemLap = 1 and not fCartRS.EOF and addID > 0 and addQty > 0 then
%>
<div class="tb otherItemSlot">Other items in your cart:</div>
<%
		end if
	loop

	dim nSubTotal : nSubTotal = itemAmt
	%>
	<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
	<%
	if sPromoCode = "" then
        if discountTotal > 0 then
            if instr(discountTotal,".") > 0 then discountTotal = left(discountTotal,instr(discountTotal,".")+2)
            itemLap = itemLap + 1
			PPhtml = PPhtml & 	"<input type=""hidden"" name=""L_NUMBER" & itemLap-1 & """ value=""DISCOUNT"">" & vbcrlf & _
								"<input type=""hidden"" name=""L_NAME" & itemLap-1 & """ value=""Promo Discount"">" & vbcrlf & _
								"<input type=""hidden"" name=""L_QTY" & itemLap-1 & """ value=""1"">" & vbcrlf & _
								"<input type=""hidden"" name=""L_AMT" & itemLap-1 & """ value=""" & discountTotal * -1 & """>" & vbcrlf
        end if
    else
        response.Cookies("promocode") = sPromoCode
        if discountTotal = 0 then
            if isnumeric(promoPercent) then discountTotal = discountAmt * promoPercent
            if discountTotal < 0 then discountTotal = discountTotal * -1
        end if
        if instr(discountTotal,".") > 0 then discountTotal = left(discountTotal,instr(discountTotal,".")+2)
        if discountTotal > 0 then
            itemLap = itemLap + 1
			PPhtml = PPhtml & 	"<input type=""hidden"" name=""L_NUMBER" & itemLap-1 & """ value=""DISCOUNT"">" & vbcrlf & _
								"<input type=""hidden"" name=""L_NAME" & itemLap-1 & """ value=""" & sPromoCode & """>" & vbcrlf & _
								"<input type=""hidden"" name=""L_QTY" & itemLap-1 & """ value=""1"">" & vbcrlf & _
								"<input type=""hidden"" name=""L_AMT" & itemLap-1 & """ value=""" & discountTotal * -1 & """>" & vbcrlf
        end if
    end if

	dim nDiscountedCartTotal : nDiscountedCartTotal = itemAmt - discountTotal
		
	call CloseConn(oConn)
%>
<div class="tb subTotalRow">
	<div class="fl fcSubtotalTitle">Subtotal (<%=itemCnt%> items):</div>
    <div class="fl fcSubtotalAmt"><%=formatCurrency(itemAmt)%></div>
</div>

<%if discountTotal > 0 then%>
<div class="tb subTotalRow">
	<div class="fl fcSubtotalTitle">Discount<%if sPromoCode <> "" then%><%=" ("&ucase(sPromoCode)&")"%><%end if%>:</div>
    <div class="fl itemTotalAmt"><%=formatCurrency(discountTotal*-1)%></div>
</div>
<%end if%>

<div class="tb subTotalRow">
    <div class="fl fcSubtotalTitle2">Shipping:</div>
    <div class="fl fcShipAmt">FREE!</div>
</div>

<%if discountTotal > 0 then%>
<div class="tb itemTotalRow">
	<div class="fl itemTotalTitle">Discounted Item Total:</div>
    <div class="fl itemTotalAmt"><%=formatCurrency(nDiscountedCartTotal)%></div>
</div>
<%else%>
<div class="tb itemTotalRow">
	<div class="fl itemTotalTitle">Item Total:</div>
    <div class="fl itemTotalAmt"><%=formatCurrency(itemAmt)%></div>
</div>
<%end if%>

<% if itemCnt > 0 then %>
<div id="fcCheckoutOpt2" class="tb fcCheckoutRow">
	<div id="hidePaypal">
    	<div class="ffl" align="left"><a onclick="document.fcPaypalForm.submit()" style="cursor:pointer;"><img src="/images/checkout/paypal_cta_checkout_floating.png" border="0" /></a></div>
        <div class="ffl" style="font-size:16px; font-weight:bold; color:#333; padding:5px 0px 5px 0px; text-align:center;">- Or -</div>
    </div>
	<% if instr(request.ServerVariables("SERVER_NAME"),"staging.") > 0 then %>
    <div class="fl fcCheckout2" onclick="window.location='/cart/checkout'"></div>
	<% else %>
    <div class="fl fcCheckout2" onclick="window.location='https://www.wirelessemporium.com/cart/checkout'"></div>
    <% end if %>
   	<div class="fr fcViewCartLink" onclick="window.location='/cart/basket'">VIEW CART</div>
</div>
<form name="fcPaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder">
    <input type="hidden" name="numItems" value="<%=itemLap%>">
    <input type="hidden" name="paymentAmount" value="<%=formatNumber(nDiscountedCartTotal,2)%>">
    <input type="hidden" name="ItemsAndWeight" value="<%=cStr(itemLap) & "|" & cStr(sWeight)%>">
    <input type="hidden" name="promo" value="<%=sPromoCode%>">
    <%=PPhtml%>
</form>
<% end if %>