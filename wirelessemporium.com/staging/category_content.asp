<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
response.buffer = true
googleAds = 1
dim carrierid
carrierid = request.querystring("carrierid")
cpass = request.QueryString("cpass")
noProducts = false
pageName = "category"

'response.write "request.QueryString:" & request.QueryString & "<br>"
if isnull(cpass) or len(cpass) < 1 or not isnumeric(cpass) then cpass = 0

if carrierid <> "" then
	if not isNumeric(categoryid) then
		call PermanentRedirect("/?ec=102")
	else
		dim URL
		call fOpenConn()
		SQL = "SELECT * FROM we_Carriers WHERE id='" & carrierID & "'"
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 0, 1
		if not RS.eof then URL = "/car-" & carrierID & "-" & formatSEO(RS("carrierName")) & "-accessories"
		RS.close
		set RS = nothing
		call fCloseConn()
		if URL = "" then
			call PermanentRedirect("/?ec=203")
		else
			call PermanentRedirect(URL)
			response.end
		end if
	end if
end if

dim categoryid : categoryid = prepInt(request.querystring("categoryID"))
dim categoryName : categoryName = prepStr(request.querystring("categoryName"))

dim title : title = prepStr(request.QueryString("title"))
if title <> "" then
	title = replace(title,"phone-","")
	call fOpenConn()
	sql = "select subtypeID from we_subTypes where nameSEO_WE like '%" & replace(title,"-","%") & "%'"
	set subTypeRS = oConn.execute(sql)
	if not subTypeRS.EOF then
		categoryid = subTypeRS("subtypeID")
	end if
	call fCloseConn()
end if

if categoryid = 0 then
	sql = "select typeID from we_types where dbo.formatSEO(typeName_WE) = '" & categoryName & "'"
	session("errorSQL") = sql
	set typeRS = oConn.execute(sql)
	
	if typeRS.EOF then
		sql = "select subtypeID from we_subTypes where dbo.formatSEO(nameSEO_WE) like '%" & categoryName & "%'"
		session("errorSQL") = sql
		set typeRS = oConn.execute(sql)
		
		if typeRS.EOF then
			response.Write("can't find: " & categoryName)
			response.End()
		else
			categoryid = typeRS("subtypeID")
		end if
	else
		categoryid = typeRS("typeID")
	end if
end if

if categoryid = "" or not isNumeric(categoryid) then
	call PermanentRedirect("/?ec=304")	
elseif categoryid = 1310 or categoryid = 1320 then
	call PermanentRedirect("/phone-charms-stickers")
end if


dim parentTypeID, categoryNameSEO
call fOpenConn()
if categoryid > 999 then
	pageName = "subcategory"
	sql = "SELECT typename parentTypeName, subTypeName typename, typeid, subtypeid, subTypeNameSEO_WE typeNameSEO, typeNameSEO_WE parentTypeNameSEO from v_subTypeMatrix WHERE subtypeid = '" & categoryid & "'"
else
	pageName = "category"
	sql = "SELECT typename ParentTypeName, typename, typeid, subtypeid, typeNameSEO_WE typeNameSEO, typeNameSEO_WE parentTypeNameSEO from v_subTypeMatrix WHERE typeid = '" & categoryid & "'"
end if	
'response.write sql & "<br>"
'session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

strSubTypeID = ""
if not RS.eof then
	parentCatNameSEO = RS("parentTypeNameSEO")
	parentCatName = RS("parentTypeName")
	categoryName = RS("typeName")
	parentTypeID = RS("typeid")
	categoryNameSEO = RS("typeNameSEO")
	do until RS.eof
		strSubTypeID = strSubTypeID & RS("subtypeid") & ","
		RS.movenext
	loop
	strSubTypeID = left(strSubTypeID, len(strSubTypeID) - 1)
end if
if strSubTypeID = "" then strSubTypeID = "9999999" end if

if cpass = 1 then
	sql = "SELECT brandName, brandID FROM we_brands where brandid not in (12,8) and brandType < 2 order by brandName"
else
	if parentTypeID = 5 then
		sql	=	"	select	distinct x.brandName, x.brandid" & vbcrlf & _
				"	from	(	select	a.brandid, a.brandName, '''' + replace(handsfreetypes, ',', ''',''') + '''' handsfreetypes" & vbcrlf & _
				"				from	we_brands a join we_models b" & vbcrlf & _
				"					on	a.brandid = b.brandid" & vbcrlf & _
				"				where	a.brandtype < 2 and a.brandid not in (12,8) " & vbcrlf & _
				"					and	b.handsfreetypes is not null	) x join " & vbcrlf & _
				"			(	select	distinct '''' + convert(varchar(10), handsfreetype) + '''' handsfreetype" & vbcrlf & _
				"				from	we_items" & vbcrlf & _
				"				where	hidelive = 0 " & vbcrlf & _
				"					and subtypeid in (" & strSubTypeID & ")	) y" & vbcrlf & _
				"		on	x.handsfreetypes like '%' + y.handsfreetype + ',%'" & vbcrlf & _
				"	order by brandname " & vbcrlf				
	else
		sql	=	"	select	distinct a.brandname, a.brandid" & vbcrlf & _
				"	from	we_brands a join we_items c " & vbcrlf & _
				"		on	a.brandid = c.brandid" & vbcrlf & _
				"	where	c.hidelive = 0 and c.inv_qty <> 0 and c.ghost = 0 and c.price_our > 0" & vbcrlf & _
				"		and	a.brandid not in (12,8)" & vbcrlf & _
				"		and a.brandtype < 2 " & vbcrlf & _
				"		and c.subtypeid in (" & strSubTypeID & ")" & vbcrlf & _
				"	union	" & vbcrlf & _
				"	select	distinct a.brandname, a.brandid" & vbcrlf & _
				"	from	we_subRelatedItems t join we_brands a " & vbcrlf & _
				"		on	t.brandid = a.brandid join we_items c " & vbcrlf & _
				"		on	t.itemid = c.itemid " & vbcrlf & _
				"	where	c.hidelive = 0 and c.inv_qty <> 0 and c.ghost = 0 and c.price_our > 0" & vbcrlf & _
				"		and	a.brandid not in (12,8)" & vbcrlf & _
				"		and a.brandtype < 2 " & vbcrlf & _
				"		and t.subtypeid in (" & strSubTypeID & ")" & vbcrlf & _
				"	order by brandname " & vbcrlf
	end if
end if
'response.write "<pre>" & sql & "</pre>"
arrRS = getDbRows(sql)
if isnull(arrRS) then
	noProducts = true
end if

dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
dicSeoAttributeInput( "CategoryId") = parentTypeID
dicSeoAttributeInput( "SubCategoryId") = categoryID
dim dicContentEventText: set dicContentEventText = GetContentEventText( dicSeoAttributeInput)

if "" = strBreadcrumb then
	strBreadcrumb = "Cell Phone " & nameSEO(categoryName)
end if
if parentTypeID = 2 or parentTypeID = 18 or parentTypeID = 19 or parentTypeID = 20 then
	isNewHeader = true
end if
%>
<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
        	<%
			if categoryid > 1000 then
			%>
			<a class="breadcrumb" href="/newHome/">HOME</a>
			<%
				if parentTypeID = 5 then 
					response.write "Mobile Hands Free Kits"
				else
					response.write parentCatNameSEO
				end if
				%>
			</a>&nbsp;&gt;&nbsp;
			<%=strBreadcrumb%>
            <%
			else
			%>
            <a class="breadcrumb" href="/newHome/">HOME</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
            <%
			end if
			%>
        </td>
    </tr>
	<%
	if prepStr(SeTopText) <> "" then topText = SeTopText
	if isNewHeader then
	%>
    <tr>
        <td width="100%" align="center" valign="top" style="padding-top:5px;">
			<div style="width:100%; border-top:2px solid #b9b9b9;border-bottom:2px solid #b9b9b9; width:745px; float:left;">
				<div style="float:left;">
					<img src="/images/categories/<%=categoryid%>/catLogo.jpg" width="<%=headerImgWidth%>" height="<%=headerImgHeight%>" border="0" title="Cell Phone <%=nameSEO(categoryName)%>" />				
				</div>
				<div style="float:left; width:<%=735 - headerImgWidth%>px; color:#4B4B4B; text-align:left; padding:3px 0px 0px 10px; font-size:11px;">
					<% 
					if H1tag <> "" then 
						if h1Style <> "" then
							response.write "<h1 style=""" & h1Style & """>" & H1tag & "</h1> &nbsp; "
						else
							response.write "<h1>" & H1tag & "</h1> &nbsp; "
						end if
					end if 
					%>
					<%=topText%>
				</div>
			</div>
        </td>
    </tr>	
	<%
	else
	%>
    <tr>
        <td width="100%" align="center" valign="top">
            <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                <tr>
                	<td width="*" style="padding-right:10px;">
                        <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                            <tr>                    
                            	<td style="border-bottom:1px solid #ccc; height:60px;" valign="bottom" align="left">
	                                <h1 class="category"><%=H1tag%></h1>
                                </td>
							</tr>
                            <tr>                    
                            	<td valign="top" align="left" style="height:60px; color:#444; padding-top:5px;">
									<!--<h2 class="category"><%=H2tag%></h2>-->
                                	<%=topText%>
                                </td>
							</tr>
						</table>
                    </td>
                    <td width="200" align="right"><img src="/images/cats/we_header_<%=parentTypeID%>.jpg" border="0" alt="<%=H1tag%>" title="<%=H1tag%>" /></td>
                </tr>
			</table>
        </td>
    </tr>
	<%
	end if

	if noProducts then
	%>
    <tr>
        <td align="center" valign="top" width="100%" style="padding-top:20px;">
        	<h3>No Products Matched Your Query!</h3>
        </td>
	</tr>
    <%
	else
	%>    
    <tr>
    	<td style="padding-top:15px;">
            <div class="graybox-middle" style="width:726px; border-left:1px solid #999; border-right:1px solid #999; padding:0px 10px 0px 10px;">
                <div class="graybox-inner" title="Choose from the following" align="right">
                	<form name="frmSearch" method="post" style="float:right;">
                        <select name="cbModel" onChange="if(this.value != ''){window.location=this.value;}">
                            <option value="">Choose from the following</option>
                            <%
                            for nRows=0 to ubound(arrRS, 2)
                                brandid = cint(arrRS(1,nRows))
                                brandName = replace(arrRS(0,nRows), "/", " / ")
                                anchorText = brandName & " " & nameSEO(categoryName)
                                if brandid = 17 then
                                %>
                                <option value="/sc-<%=categoryID & "-sb-" & brandid & "-" & formatSEO(brandName) & "-iphone-" & formatSEO(categoryName)%>">
                                    <%=brandName & " iPhone " & nameSEO(categoryName)%>
                                </option>
									<%
                                    if categoryid <> 15 then
                                    %>
                                    <option value="/sc-<%=categoryID & "-sb-" & brandid & "-" & formatSEO(brandName) & "-ipod-ipad-" & formatSEO(categoryName)%>">
                                        <%=brandName & " iPod/iPad " & nameSEO(categoryName)%>
                                    </option>
                                <%
									end if
                                else
                                %>
                                <option value="/sc-<%=categoryid%>-sb-<%=brandid%>-<%=formatSEO(anchorText)%>"><%=anchorText%></option>
                                <%
                                end if
                            next
                            %>
                        </select>
					</form>
                	<span style="font-weight:bold; float:right;">Brands:&nbsp;&nbsp;</span>
				</div>
            </div>
        </td>
    </tr>
	<tr>
        <td width="100%" align="center" style="padding:5px 0px 15px 0px;">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="background:url(/images/cats/gray-header-right.jpg) repeat-x;">
            	<tr>
                	<td style="background:url(/images/cats/gray-header-left.jpg) no-repeat; width:330px; height:30px; color:#fff; font-size:14px; padding:4px 0px 0px 10px;" align="left" valign="top">
						<b>SELECT YOUR BRAND</b>
					</td>
                </tr>
			</table>        
        </td>
	</tr>
	<tr>
        <td width="100%" align="center" style="padding-bottom:20px;">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="684">
                <tr>
                
                    <%
                    dim others : others = 0
                    a = 0
					for nRows=0 to ubound(arrRS, 2)
						brandid = cint(arrRS(1,nRows))
						brandName = replace(arrRS(0,nRows), "/", " / ")
						strLink = "#!/sc-" & categoryID & "-sb-" & brandid & "-" & formatSEO(brandName) & "{-}" & formatSEO(categoryName)
						altText = brandName & "{}" '& nameSEO(categoryName) 'Removed to prevent duplicate content penalty
						
						if categoryid = 15 then
							strLink = "#!/sb-" & brandid & "-sm-0-sc-" & categoryid & "-" & formatSEO(brandName) & "-cell-phones-tablets"
							altText = brandName & "{}" '& nameSEO(categoryName) 'Removed to prevent duplicate content penalty
						end if
						
                        if brandid = 15 then
							if categoryid = 15 then
								otherLink = "#!/sb-" & brandid & "-sm-0-sc-" & categoryid & "-" & formatSEO(brandName) & "{-}" & formatSEO(categoryName)
								otherText = brandName & "{}" & nameSEO(categoryName)
							else
								otherLink = "#!/sc-" & categoryID & "-sb-" & brandid & "-" & formatSEO(brandName) & "{-}" & formatSEO(categoryName)
								otherText = brandName & "{}" & nameSEO(categoryName)
							end if							
                            others = 1
                        else
							if brandid = 17 then
							%>
								<td width="171" height="75" align="center" valign="top" style="padding:20px 10px 20px 10px; border-bottom:1px solid #ccc;">
                                    <a href="<%=replace(strLink, "{-}", "-iphone-")%>" style="text-decoration:none;" title="<%=replace(altText, "{}", " iPhone ")%>">
										<img src="/images/cats/we_cat_brand_<%=brandid%>_1.jpg" border="0" title="<%=replace(altText, "{}", " iPhone ")%>" alt="<%=replace(altText, "{}", " iPhone ")%>" />
									</a><br>
									<h2><a href="<%=replace(strLink, "{-}", "-iphone-")%>" title="Cell Phone <%=nameSEO(categoryName) & ": " & replace(altText, "{}", " iPhone ")%>" class="cellphone3-link"><%=replace(altText, "{}", " iPhone ")%></a></h2>
								</td>       
                                <% if categoryid <> 15 and categoryid <> 1031 then %>
								<td width="171" height="75" align="center" valign="top" style="padding:20px 10px 20px 10px; border-bottom:1px solid #ccc;">
                                    <a href="<%=replace(strLink, "{-}", "-ipod-ipad-")%>" style="text-decoration:none;" title="<%=replace(altText, "{}", " iPod/iPad ")%>">
										<img src="/images/cats/we_cat_brand_<%=brandid%>_0.jpg" border="0" title="<%=replace(altText, "{}", " iPod/iPad ")%>" alt="<%=replace(altText, "{}", " iPod/iPad ")%>" />
									</a><br>
									<h2><a href="<%=replace(strLink, "{-}", "-ipod-ipad-")%>" title="<%=replace(altText, "{}", " iPod/iPad ")%>" class="cellphone3-link"><%=replace(altText, "{}", " iPod/iPad ")%></a></h2>
								</td>
							<%
		                            a = a + 1
								end if
							else
                            %>
								<td width="171" height="75" align="center" valign="top" style="padding:20px 10px 20px 10px; border-bottom:1px solid #ccc;">
									<a href="<%=replace(strLink, "{-}", "-")%>" title="<%=replace(altText, "{}", " ")%>"><img src="/images/cats/we_cat_brand_<%=brandid%>.jpg" border="0" title="<%=replace(altText, "{}", " ")%>" alt="<%=replace(altText, "{}", " ")%>"></a>
									<br>
									<h2><a href="<%=replace(strLink, "{-}", "-")%>" title="Cell Phone <%=nameSEO(categoryName) & ": " & replace(altText, "{}", " Cell Phone ")%>" class="cellphone3-link"><%=replace(altText, "{}", " ")%></a></h2>
								</td>
                            <%
							end if
                            a = a + 1
                        end if
                        if a = 4 then
                            response.write "</tr><tr>" & vbcrlf
                            a = 0
                        end if
                    next
                    if others = 1 then
                        %>
                        <td width="171" height="75" align="center" valign="top" style="padding:20px 10px 20px 10px; border-bottom:1px solid #ccc;">
                            <a href="<%=replace(otherLink, "{-}", "-")%>" style="text-decoration:none;"><img src="/images/cats/we_cat_brand_15.jpg" border="0" alt="<%=replace(otherText, "{}", " ")%>"></a>
                            <br>
                            <h2><a href="<%=replace(otherLink, "{-}", "-")%>" title="Cell Phone <%=nameSEO(categoryName) & ": " & replace(otherText, "{}", " Cell Phone ")%>" class="cellphone3-link">Other Cell Phone <%=nameSEO(categoryName)%></a></h2>
                        </td>
                        <%
                        a = a + 1
                        if a = 4 then
                            response.write "</tr><tr>" & vbcrlf
                            a = 0
                        end if
                    end if
                    %>
                </tr>
            </table>
        </td>
    </tr>
    <% if categoryid = 7 then %>
    <tr>
	    <td align="center"><%'Youtube Video http://www.youtube.com/watch?v=wYR1ruVE8tk %>
	        <iframe width="468" height="263" src="http://www.youtube.com/embed/wYR1ruVE8tk?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;wmode=opaque" frameborder="0" allowfullscreen></iframe>
        </td>
    </tr>
    <% end if %>
    <!--#include virtual="/includes/asp/inc_bottomCategory.asp"-->
    <%
    '<tr>
    '	<td style="padding-top:30px;">
    '        <div class="graybox-middle" style="width:726px; border-left:1px solid #999; border-right:1px solid #999; padding:0px 10px 0px 10px;">
	'			<div class="graybox-inner" title="<%=categoryNameSEO$>">:)<%=ucase(categoryNameSEO)$></div>			
    '        </div>
    '    </td>
    '</tr>
    %>
    <%
	end if
	%>
	<tr><td width="100%" align="center" valign="top" class="bottomText" style="padding:10px 0px 10px 0px;"><%=sebottomText%></td></tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
    <tr>
        <td width="100%" align="center" valign="top" style="padding-bottom:10px;">
            <img src="/images/hline_2.jpg" width="100%" height="8" border="0">
        </td>
    </tr>
</table>
<%
call fCloseConn()
%>