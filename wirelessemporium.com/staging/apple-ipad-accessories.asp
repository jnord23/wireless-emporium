<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
dim modelID
modelID = 940
call fOpenConn()
sql	=	"select	distinct a.modelid, a.modelname, a.modelimg, c.brandid, c.brandname, a.isTablet" & vbcrlf & _
		"	,	isnull(v.orderNum, d.orderNum) orderNum, v.typeid, v.typename" & vbcrlf & _
		"from	we_models a join we_brands c " & vbcrlf & _
		"	on	a.brandid = c.brandid join we_items b " & vbcrlf & _
		"	on	a.modelid = b.modelid join we_types d " & vbcrlf & _
		"	on	b.typeid = d.typeid join v_subtypeMatrix v" & vbcrlf & _
		"	on	b.subtypeid = v.subtypeid " & vbcrlf & _
		"where	a.modelid = '" & modelid & "'" & vbcrlf & _ 
		"	--and d.typeid not in ()" & vbcrlf & _
		"	and b.hidelive = 0 and b.inv_qty <> 0 and b.price_our > 0 " & vbcrlf & _
		"order by orderNum desc	" & vbcrlf
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

dim brandName, brandID, modelName, modelImg
brandName = RS("brandName")
brandID = RS("brandID")
modelName = RS("modelName")
modelImg = RS("modelImg")

dim SEtitle, SEdescription, SEkeywords, topText, bottomText, RSmeta
SEtitle = "iPad Accessories: iPad Headset, iPad Screen Protector, iPad 3G Car Charger"
SEdescription = "Get the best deals on iPad accessories from Wireless Emporium - the #1 store online for cell phone accessories like iPad headsets, screen protectors, car chargers and much more."
SEkeywords = "iPad accessories, iPad 3G accessories, iPad screen protector, iPad headset, iPad 3g car charger, iPad charger, iPad 3g charger"

altText = "Apple iPad Accessories: iPad TYPES"
topText = "The original Apple iPad introduced the world to an entirely new type of mobile device.  The tablet computer was born that day, and has captured the world's imagination.  Despite being the first of its kind, the iPad remains one of the most powerful tablet devices on the market today, and has inspired a host of unique <a href=""http://www.wirelessemporium.com/apple-ipad-accessories.asp"" class=""toptext-link"" title=""iPad accessories"">iPad accessories</a> to go with it."
bottomText = "When the iPad was introduced, it was a device that was more portable than a laptop, but it wasn't as easy as putting it in your pocket like a cell phone.  A host of iPad accessories soon hit the market, including <a href=""http://www.wirelessemporium.com/sb-17-sm-940-sc-7-cases-pouches-skins-apple-ipad.asp"" class=""bottomlink-text"" title=""iPad cases"">iPad cases</a> that doubled as iPad stands.  <a href=""http://www.wirelessemporium.com/sb-17-sm-940-sc-8-other-gear-apple-ipad.asp"" class=""toptext-link"" title=""bluetooth keyboards"">Bluetooth keyboards</a> made it easier to use the iPad for business and educational purposes.  In general, portability became even easier, and usability shot through the roof.  As more and more people rushed out to buy the iPad, and they became more ingrained in daily lives, the need to protect the devices from wear and tear became clear."
bottomText = bottomText & "<br /><br /><a href=""http://www.wirelessemporium.com/sb-17-sm-940-sc-18-screen-protectors-apple-ipad.asp"" class=""bottomtext-link"" title=""iPad screen protectors"">iPad screen protectors</a> are a must have to guard the iPad's display from scratches, dust and fingerprints."
bottomText = bottomText & "  An iPad case or <a href=""http://www.wirelessemporium.com/sb-17-sm-940-sc-3-faceplates-screen-protectors-apple-ipad.asp"" class=""bottomtext-link"" class=""iPad faceplates"">iPad faceplate</a> is a perfect compliment, protecting the body of the iPad from all sorts of damage.  "
bottomText = bottomText & "There are even designer covers and <a href=""http://www.wirelessemporium.com/sb-17-sm-940-sc-19-decal-skin-apple-ipad.asp"" class=""bottomtext-link"" title=""iPad decal skins"">iPad decal skins</a> that allow individuals to add some personal flair to their devices.  Power users never have to worry about their devices running out of battery life either.  There are several <a href=""http://www.wirelessemporium.com/sb-17-sm-940-sc-2-chargers-apple-ipad.asp"" class+""bottomtext-link"" title=""iPad chargers"">iPad chargers</a> for the car, office or home that will keep you powered up no matter where you go.  And there are even iPad docking stations that not only power up your device but sync it with your home computer.  No matter what your needs are, Wireless Emporium has the largest selection of iPad accessories anywhere."

dim strBreadcrumb
'if session("breadcrumb_brand") <> "" then brandName = session("breadcrumb_brand") end if 
strBreadcrumb = brandName & " " & modelName & " Accessories"
'session("breadcrumb_model") = modelName

%>
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_SEOtext_model.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="/iphone-accessories.asp"><%=brandName%> Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="padding-top:5px;">
            <h1>Apple iPad Accessories</h1>
        </td>
    </tr>
    <tr>
        <td width="748" height="200" align="left" valign="top" style="background-image: url('/images/WE_ipad_banner.jpg'); background-width:730px; background-repeat: no-repeat; background-position: center top;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                    <td align="center" valign="top" width="300" class="topText">&nbsp;</td>
                    <td align="center" valign="top" width="438" class="topText" style="padding-top:45px;"><%=modelText%></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%">
            <img src="/images/spacer.gif" width="1" height="10" border="0">
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%">
            <img src="/images/categories/ipad/hline.jpg" width="748" height="7" border="0">
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%">
            <img src="/images/spacer.gif" width="1" height="10" border="0">
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="748">
                <tr>
                    <%
                    a = 0
                    do until RS.eof
						thisType = nameSEO(RS("typeName"))
						altText = RS("modelname") & " "  & thisType
					%>
						<td align="center" valign="top" width="180"><table border="0" cellspacing="0" cellpadding="0" width="119"><tr><td align="center" valign="middle"><img src="/images/spacer.gif" width="1" height="6" border="0"></td></tr>
						<tr><td align="center" valign="middle"><table border="0" cellspacing="0" cellpadding="0" width="100"><tr><td align="center" valign="bottom" height="150"><a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-<%=RS("typeid")%>-<%=formatSEO(RS("typename"))%>-<%=formatSEO(brandname)%>-<%=formatSEO(modelName)%>.asp"><img src="/images/categories/we_cat_tablet-<%=RS("typeid")%>.jpg" border="0" alt="<%=altText%>" title="<%=altText%>"></a></td></tr>
						<tr><td align="center" valign="middle"><img src="/images/spacer.gif" width="1" height="6" border="0"></td></tr>
						<tr><td align="center" valign="middle"><a class="cellphone2-link" href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-<%=RS("typeid")%>-<%=formatSEO(RS("typename"))%>-<%=formatSEO(brandname)%>-<%=formatSEO(modelName)%>.asp" title="<%=altText%>"><%=altText%></a></td></tr></table></td></tr></table></td>
                        <%
                        RS.movenext
                        a = a + 1
                        if a = 4 then
                            response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""4"">&nbsp;</td></tr><tr>" & vbcrlf
                            a = 0
                        end if
                    loop
                    if a = 1 then
                        response.write "<td width=""180"">&nbsp;</td><td width=""180"">&nbsp;</td><td width=""180"">&nbsp;</td>" & vbcrlf
                    elseif a = 2 then
                        response.write "<td width=""180"">&nbsp;</td><td width=""180"">&nbsp;</td>" & vbcrlf
                    elseif a = 3 then
                        response.write "<td width=""180"">&nbsp;</td>" & vbcrlf
                    end if
                    %>
                </tr>
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td width="100%" align="center" valign="top" height="40">&nbsp;</td></tr>							
    <tr>
        <td width="100%" align="center" valign="top">
            <img src="/images/hline_dot.jpg" width="740" height="8" border="0">
        </td>
    </tr>
    <tr><td width="100%" align="center" valign="top">&nbsp;</td></tr>
    <tr>
        <td align="center" valign="top">
            <h3>Related <%=brandName%> Cell Phone Models</h3>
            <table border="0" cellspacing="10" cellpadding="0" width="100%">
                <tr>
                    <%
                    dim strRelatedSQL
                    strRelatedSQL	=	"select	top 5 modelid, modelName, modelImg " & vbcrlf & _
                                        "from	we_models " & vbcrlf & _
                                        "where	brandid = " & brandID & vbcrlf & _
                                        "	and	hidelive = 0 " & vbcrlf & _
                                        "	and	modelid <> " & modelID & vbcrlf & _
                                        "order by releaseyear desc, releasequarter desc	" & vbcrlf
                                
                    set RS2 = Server.CreateObject("ADODB.Recordset")
                    RS2.open strRelatedSQL, oConn, 0, 1
                                        
                    do until RS2.eof
                        modelID = RS2("modelID")
                        modelName = RS2("modelName")
                        modelImg = RS2("modelImg")

                        if brandid = "17" then
                            altTag = modelName & " Accessories"
                        else
                            altTag = brandName & " " & modelName & " Accessories"
                        end if

                        RS2.movenext
                        %>
                        <td width="149" align="center" valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr>
                                    <td width="100%" height="150" align="center" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" align="center">
                                            <tr>
                                                <td>
                                                    <%if modelID = 940 then%>
                                                        <a href="/apple-ipad-accessories.asp" title="<%=altTag%>"><img src="/productPics/models/<%=modelImg%>" border="0" alt="<%=replace(altTag,"Accessories","accessories")%>"></a><br />																			
                                                        <a class="cellphone-font" href="/apple-ipad-accessories.asp" title="<%=altTag%>"><%=replace(modelName,"/"," / ") & "<br>Accessories"%></a>
                                                    <%else%>
                                                        <a href="/T-<%=modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp" title="<%=altTag%>"><img src="/productPics/models/<%=modelImg%>" border="0" alt="<%=replace(altTag,"Accessories","accessories")%>"></a><br />																			
                                                        <a class="cellphone-link" href="/T-<%=modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp" title="<%=altTag%>"><%=replace(modelName,"/"," / ") & "<br>Accessories"%></a>
                                                    <%end if%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" valign="middle"><img src="/images/spacer.gif" width="1" height="6"></td>
                                </tr>
                            </table>
                        </td>
                        <%
                    loop
                    RS2.close
                    set RS2 = nothing
                    %>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">&nbsp;</td>
    </tr>
    <!--#include virtual="/includes/asp/inc_SEOtext_brand.asp"-->
    <!--#include virtual="/includes/asp/inc_SEOtext_store.asp"-->
    <tr>
        <td align="left" valign="top">&nbsp;</td>
    </tr>
    <tr>
        <td align="center">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" class="topText">
                        <p style="margin-top:0px;margin-bottom:6px;"><%=brandText%></p>
                        <p><%=topText%></p>
                        <p><%=bottomText%></p>
                        <p><%=storeText%></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->