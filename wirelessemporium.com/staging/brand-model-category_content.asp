<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
dim noAsp : noAsp = true
response.buffer = true

pageName = "bmc"
dim basePageName : basePageName = "BMC"

dim productListingPage : productListingPage = 1
googleAds = 1
leftGoogleAd = 1
nItemDescMaxLength = 85

dim modelid, categoryid, isTablet
isTablet = false

brandID = prepInt(request.querystring("brandID"))
modelID = prepInt(request.querystring("modelID"))
categoryID = prepInt(request.querystring("categoryID"))

response.Cookies("saveBrandID") = 0
response.Cookies("saveModelID") = modelID
response.Cookies("saveItemID") = 0

musicSkins = request.querystring("musicSkins")
musicSkinGenre = request.querystring("musicSkinGenre")
musicSkinArtistID = request.querystring("musicSkinArtist")

if instr(musicSkins,",") > 0 then
	musicSkinsArray = split(musicSkins,",")
	musicSkins = musicSkinsArray(0)
end if
if instr(musicSkinGenre,",") > 0 then
	musicSkinGenreArray = split(musicSkinGenre,",")
	musicSkinGenre = musicSkinGenreArray(0)
end if
session("errorSQL2") = musicSkins

if isnull(brandID) or len(brandID) < 1 then brandID = 0
if isnull(modelID) or len(modelID) < 1 then modelID = 0
if isnull(categoryID) or len(categoryID) < 1 then categoryID = 0
if isnull(musicSkins) or len(musicSkins) < 1 then musicSkins = 0
if musicSkinGenre = "r-b" then musicSkinGenre = "R&B"
if musicSkinGenre = "tv-movies" then musicSkinGenre = "TV/Movies"
if musicSkinGenre = "screen-protectors" then musicSkinGenre = "Screen Protectors"
if len(musicSkinGenre) > 0 then
	musicSkinGenre = ucase(left(musicSkinGenre,1)) & right(musicSkinGenre,len(musicSkinGenre)-1)
end if
if instr(REWRITE_URL, "apple-iphone") > 0 then
	phoneOnly = 1
else
	phoneOnly = 0
end if

if (brandID = 0 or modelID = 0) and (categoryID <> 8 and categoryid <> 15) then call PermanentRedirect("/?ec=bmc9001")
'if categoryid > 999 then
'	call PermanentRedirect(replace(REWRITE_URL, "-sc-", "-scd-"))
'end if

call fOpenConn()
sql = "exec sp_brandModelNamesByModelID " & modelid
session("errorSQL") = sql
set nameRS = oConn.execute(sql)
if not nameRS.eof then
	modelName = nameRS("modelname")
	modelImg = nameRS("modelimg")
	brandName = nameRS("brandName")
	isTablet = nameRS("isTablet")
	if not isnull(nameRS("HandsfreeTypes")) then HandsfreeTypes = left(nameRS("HandsfreeTypes"),len(nameRS("HandsfreeTypes"))-1)
end if
nameRS = null

if brandName = "" then
	sql = "exec sp_brandDetailsByBrandID " & brandid
	set rsTemp = oConn.execute(sql)
	if not rsTemp.eof then brandName = rsTemp("brandName")
end if
if modelImg = "" then modelImg = "mini-phones.jpg" end if

if categoryid = 3 or categoryid = 7 then
	numRowItems = 5
else
	numRowItems = 10
end if

if prepStr(HandsfreeTypes) = "" then HandsfreeTypes = "1"
sql = "exec sp_subcategoriesByBMC " & brandID & ", " & modelID & ", " & categoryid & ", '" & HandsfreeTypes & "'"
session("errorSQL") = sql
set rs = oConn.execute(sql)

noProducts = false
if rs.EOF then
	noProducts = true

	sql = "exec sp_modelDetailsByID " & modelid
	session("errorSQL") = SQL
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		modelName = RS("modelname")
		modelImg = RS("modelimg")
	end if
else
	categoryName = rs("typeName")
	subCategoryName = rs("subtypeName")
	subCategoryNameSEO = rs("subTypeNameSEO_WE")
	subCatTopText = rs("subCatTopText")
end if

'=========================================================================================
Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
	oParam.CompareMode = vbTextCompare
	oParam.Add "x_brandID", brandID
	oParam.Add "x_modelID", modelID
	oParam.Add "x_categoryID", categoryID
	oParam.Add "x_brandName", brandName
	oParam.Add "x_modelName", modelName
	oParam.Add "x_categoryName", categoryName	
	oParam.Add "x_musicGenreName", musicSkinGenre
	if "0" <> musicSkinArtistID and len(musicSkinArtistID) > 0 then
		oParam.Add "x_musicArtistName", "artist-" & musicSkinArtistID & "-" & musicSkinArtistName
	end if

'call redirectURL("bmc", "", request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
'=========================================================================================

dim SEtitle, SEdescription, SEkeywords, h1, topText, bottomText

dim modelLink
if modelID = 940 then
	modelLink = "/apple-ipod-ipad-accessories"
else
	modelLink = "/" & formatSEO(brandName) & "-" & formatSEO(modelName) & "-accessories"
end if

dim strBreadcrumb
if strBreadcrumb = "" then strBreadcrumb = brandName & "&nbsp;" & modelName & "&nbsp;" & nameSEO(categoryName)

'todo: need to consolidate Metatags with we_XXXText
dim strH1: strH1 = EMPTY_STRING
dim strH2: strH2 = EMPTY_STRING
dim strAltText: strAltText = EMPTY_STRING

dim strTypeToken : strTypeToken = "Cell Phone"
if isTablet then strTypeToken = "Tablet"

modelLink = "/" & formatSEO(brandName & " " & modelName) & "-accessories"

if isTablet then
	modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & " " & modelName & " " & strTypeToken & " Accessories</a>"	
else
	if brandid = 17 and phoneOnly = 1 then
		brandCrumb 	= "<a class=""breadcrumb"" href=""/apple-iphone-accessories"">Apple iPhone Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & "&nbsp;" & modelName & " Accessories</a>"	
	elseif brandid 	= 17 and phoneOnly = 0 then
		topText = replace(replace(topText, "apple cell phone", brandName & "&nbsp;" & modelName), strTypeToken, "")
		SEtitle = replace(replace(SEtitle, "apple cell phone", brandName & "&nbsp;" & modelName), strTypeToken, "")
		SEdescription = replace(replace(SEdescription, "apple cell phone", brandName & "&nbsp;" & modelName), strTypeToken, "")
		SEkeywords = replace(replace(SEkeywords, "apple cell phone", brandName & "&nbsp;" & modelName), strTypeToken, "")
	
		brandCrumb 	= "<a class=""breadcrumb"" href=""/apple-ipod-ipad-accessories"">Apple iPod/iPad Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & "&nbsp;" & modelName & " Accessories</a>"
	else
		brandCrumb 	= "<a class=""breadcrumb"" href=""/" & formatSEO(brandName) & "-phone-accessories"">" & brandName & " " & strTypeToken & " Accessories</a>"
		modelCrumb 	= "<a class=""breadcrumb"" href=""" & modelLink & """>" & brandName & "&nbsp;" & modelName & "&nbsp;" & strTypeToken & " Accessories</a>"	
	end if
end if

if h1 <> "" then
	strH1 = h1
elseif strH1 = "" then
	strH1 = brandName & " " & modelName & " " & nameSEO(categoryName)
end if

session("otherAdminOptions") = ", <a style='color:#ffffff;' href='/admin/db_update_models.asp?ModelID=" & modelID & "&BrandID=" & brandID & "&searchType=Model&submitType=Edit&submitModel=Search'>Edit " & modelName & "</a>"
%>
<% extraWidgets = "no" %>
<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
        	<%
			if categoryid = 8 then
			%>
			<a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;Other Accessories
            <%
			elseif modelid = 0 then
			%>
            <a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;<%=brandCrumb%>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
            <%
			else
			%>
			<a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;<%=brandCrumb%>&nbsp;&gt;&nbsp;<%=modelCrumb%>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>            
            <%
			end if
			%>
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" valign="top" style="padding:10px 0px 10px 0px;" title="<%=strH1%>">
            <table width="748" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="150" align="center" valign="middle" style="padding-right:5px;">
						<img src="/productpics/models/<%=modelImg%>" width="80" border="0" alt="<%=strH1%>" title="<%=strH1%>">
                    </td>
                    <td width="383" align="left">
						 <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                         <%
						 if categoryid = 8 then
						 %>
                            <tr>                    
                            	<td style="height:120px;" valign="middle" align="left">
	                                <div style="font-size:24px; font-weight:bold;"><%=categoryName%></div>
                                    <div id="CustomerContent"><%=SeTopText%></div>
                                </td>
							</tr>
                         <%
						 elseif categoryid = 15 then
						 %>
                            <tr>                    
                            	<td style="height:120px;" valign="middle" align="left">
	                                <div style="font-size:24px; font-weight:bold;"><%=brandName%>&nbsp;<%=categoryName%></div>
                                    <div id="CustomerContent"><%=SeTopText%></div>
                                </td>
							</tr>
                         <%
						 else
						 %>
                            <tr>                    
                            	<td style="border-bottom:1px solid #ccc; height:60px;" valign="bottom" align="left">
	                                <div style="font-size:24px; font-weight:bold;"><%=brandName & " " & modelName%></div>
                                </td>
							</tr>
                            <tr><td valign="top" align="left" style="height:60px;"><h2 style="font-size:24px; font-weight:bold;"><%=categoryName%></h2>
                                    <div id="CustomerContent"><%=SeTopText%></div>
                            </td></tr>
                         <%
						 end if
						 %>
						</table>
                    </td>
                    <td style="padding-left:10px;">
                    <img src="/images/cats/we_header_<%=categoryid%>.jpg" border="0" alt="<%=strH1%>" title="<%=strH1%>" />
					</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
            <div style="float:left;">
            	<table width="726px" border="0" cellspacing="0" cellpadding="0" class="graybox-middle" style="border-left:1px solid #999; border-right:1px solid #999; padding:0px 10px 0px 10px;">
                	<tr>
                    	<td align="right">
	                        <div class="graybox-inner" style="display:inline;">Jump to:</div>
							<div style="display:inline;" id="jumpToOptions"></div>                      
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" valign="top">
        	<%
			a = 0
			if categoryID = 3 or categoryID = 7 then
				sql	=	"exec sp_customFaceplatesAvailable " & modelid
				session("errorSQL") = sql
				set rsCust = oConn.execute(sql)
				if rsCust("cnt") > 0 then
					a = a + 1
					subCategoryName = "Customize Your Case"		
					subCatLink = "#!/sb-" & brandid & "-sm-" & modelid & "-scd-2000-" & formatSEO(lcase(subCategoryName)) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)
										
			%>
            <div style="float:left; width:150px; padding:10px; margin-left:10px; border-bottom:1px solid #CCC; border-right:1px dotted #CCC;">
                <div style="float:left; width:130px; height:140px; text-align:center;">
                	<a href="<%=subCatLink%>" title="<%=brandName & " " & modelName & " " & subCategoryName%>">
                    	<img src="/images/bmc/Customize-faceplate-sub-cat-image2.jpg" border="0" height="100" />
					</a>
				</div>
                <div style="float:left; width:130px; padding-top:10px; font-weight:bold; height:50px; overflow:hidden; text-align:center;">
                	<a href="<%=subCatLink%>" class="cellphone-link" title="<%=brandName & " " & modelName & " " & subCategoryName%>">
						<%=subCategoryName%>
					</a>
				</div>
            </div>        
            <%
				end if
			end if

			hasDustPlug = false
			if not noProducts then
				lap = 0 : lastTypeID = 0 : lastSubTypeID = 0
				set fsThumb = CreateObject("Scripting.FileSystemObject")
				lastPic = ""
				do while not rs.EOF
					a = a + 1
					lap = lap + 1
					showSubCat = 1
					
					curSubTypeID = rs("subtypeid")
					subCategoryName = rs("subtypeName")
					subCategoryNameSEO = rs("subtypeNameSEO_WE")
					if curSubTypeID = 1310 then hasDustPlug = true
					if lastSubCategoryName = "" then lastSubCategoryName = rs("subtypeName") end if
					itemPic = rs("itemPic")
					if isnull(itemPic) then showSubCat = 0
					if categoryid = 8 then
						itemPic2 = rs("itemPic2")
						if isnull(itemPic) then itemPic = itemPic2
					end if
					'if lastPic <> itemPic then
						lastPic = itemPic
						musicSkins = rs("isMusicSkins")
						msDefaultImg = rs("defaultImg")
	
						'====================================================================== find appropriate product images
						if musicSkins = 1 then
							itemImgPath = Server.MapPath("/productpics/musicSkins/musicSkinsLarge") & "\" & itemPic
							itemImgPath2 = Server.MapPath("/productpics/musicSkins/musicSkinsSmall") & "\" & itemPic
							useImgPath = "/productpics/musicSkins/musicSkinsSmall/" & itemPic
						else
							itemImgPath = Server.MapPath("/productpics/thumb") & "\" & itemPic
							itemImgPath2 = Server.MapPath("/productpics/thumb") & "\" & itemPic
							useImgPath = "/productpics/thumb/" & itemPic
						end if
						
						if not fsThumb.FileExists(itemImgPath) then
							'response.Write("itemImgPath:" & itemImgPath & "<br>")
							if musicSkins = 1 then
								if isnull(msDefaultImg) then
									itemPic = "imagena.jpg"
									useImgPath = "/productpics/thumb/imagena.jpg"
								else
									skipRest = 0
									if fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & msDefaultImg)) then
										setDefault = msDefaultImg
									elseif fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & replace(msDefaultImg," ","-"))) then
										setDefault = replace(msDefaultImg," ","-")
									else
										setDefault = msDefaultImg
									end if
									if skipRest = 0 then
										chkPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/thumbs/" & setDefault)
										if not fsThumb.FileExists(chkPath) then
											musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)
											if fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)) then
												musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & setDefault)
											elseif fsThumb.FileExists(Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & replace(setDefault," ","-"))) then
												musicSkinsDefaultPath = Server.MapPath("/productpics/musicSkins/musicSkinsDefault/" & replace(setDefault," ","-"))
											else
												itemPic = "imagena.jpg"
												useImgPath = "/productpics/thumb/imagena.jpg"
												skipRest = 1
											end if
											if skipRest = 0 then
												session("errorSQL") = musicSkinsDefaultPath
												Jpeg.Open musicSkinsDefaultPath
												Jpeg.Width = 100
												Jpeg.Height = Jpeg.OriginalHeight * 100 / Jpeg.OriginalWidth
												Jpeg.Save chkPath
											end if
										end if
										if skipRest = 0 then
											itemPic = setDefault
											useImgPath = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & setDefault
										end if
									end if
								end if
							else
								itemPic = "imagena.jpg"
								useImgPath = "/productpics/thumb/imagena.jpg"
							end if
						elseif musicSkins = 1 and not fsThumb.FileExists(itemImgPath2) then
							Jpeg.Open itemImgPath
							if Jpeg.OriginalHeight > Jpeg.OriginalWidth then
								Jpeg.Height = 100
								Jpeg.Width = Jpeg.OriginalWidth * 100 / Jpeg.OriginalHeight
							else
								Jpeg.Width = 100
								Jpeg.Height = Jpeg.OriginalHeight * 100 / Jpeg.OriginalWidth
							end if
							Jpeg.Save itemImgPath2
						end if		
						'====================================================================== find appropriate product images
						if musicSkins = 1 then
							subCatLink = "#!/sb-" & brandid & "-sm-" & modelid & "-sc-" & curSubTypeID & "-music-skins-genre"
						elseif categoryID = 8 then
							subCatLink = "#!/sb-0-sm-0-scd-" & curSubTypeID & "-" & formatSEO(subCategoryName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)
						else
							subCatLink = "#!/sb-" & brandid & "-sm-" & modelid & "-scd-" & curSubTypeID & "-" & formatSEO(subCategoryName) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)
						end if
						selectOptions = selectOptions & subCatLink & "##" & subCategoryName & "##"
						
						useStaticImage = "1110##1130##1140##1150##1360##"
						if instr(useStaticImage, curSubTypeID & "##") > 0 then useImgPath = "/images/bmc/subcat-" & curSubTypeID & ".jpg"
						
						if showSubCat = 1 then
						%>
						<div style="float:left; width:150px; padding:10px; margin-left:10px; border-bottom:1px solid #CCC;<% if a <= 4 then %> border-right:1px dotted #CCC;<% end if %>">
							<div style="float:left; width:130px; height:140px; text-align:center;"><a href="<%=subCatLink%>" title="<%=brandName & " " & modelName & " " & subCategoryName%>"><img src="<%=useImgPath%>" border="0" /></a></div>
							<div style="float:left; width:130px; padding-top:10px; font-weight:bold; height:50px; overflow:hidden; text-align:center;"><a href="<%=subCatLink%>" class="cellphone-link" title="<%=brandName & " " & modelName & " " & subCategoryName%>"><%=subCategoryName%></a></div>
						</div>
						<%
							if a = 4 then a = 0
						end if
						do while curSubTypeID = rs("subtypeID")
							rs.movenext
							if rs.EOF then exit do
						loop
						if rs.EOF then exit do
					'else
					'	rs.movenext
					'end if
				loop
				
				if categoryid = 8 and not hasDustPlug then
				%>
                <div style="float:left; width:150px; padding:10px; margin-left:10px; border-bottom:1px solid #CCC;<% if a <= 4 then %> border-right:1px dotted #CCC;<% end if %>">
                    <div style="float:left; width:130px; height:140px; text-align:center;"><a href="/phone-dust-plugs-charms" title="<%=brandName & " " & modelName & " Dust Plugs &amp; Charms"%>"><img src="/productpics/thumb/cell-phone-charms-pink-diamond-heart-w-pink-stone.jpg" border="0" /></a></div>
                    <div style="float:left; width:130px; padding-top:10px; font-weight:bold; height:50px; overflow:hidden; text-align:center;"><a href="/phone-dust-plugs-charms" class="cellphone-link" title="<%=brandName & " " & modelName & " Dust Plugs &amp; Charms"%>">Dust Plugs &amp; Charms</a></div>
                </div>
                <%
				end if
				'Youtube Video Section
				%>
                <% if modelID = 1412 and categoryID = 3 then 'iPhone 5 Cases Video %>
                <div style="float:left;clear:both;text-align:center;width:100%;margin-top:2.0em;">
	                <iframe width="468" height="263" src="http://www.youtube.com/embed/3Z9ppoPmx5M?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;wmode=opaque" frameborder="0" allowfullscreen></iframe>
                </div>
                <% end if %>
                <% if (modelID = 1267 or modelID = 968 or modelID = 1120) and categoryID = 3 then 'iPhone 4s, iPhone 4, iPhone 4S Cases Video %>
                <div style="float:left;clear:both;text-align:center;width:100%;margin-top:2.0em;">
	                <iframe width="468" height="263" src="http://www.youtube.com/embed/6Ek6VOILwI0?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;wmode=opaque" frameborder="0" allowfullscreen></iframe>
                </div>               
                <% end if %>
                <% if modelID = 1390 and categoryID = 3 then 'Samsung Galaxy S3 Cases Video %>
                <div style="float:left;clear:both;text-align:center;width:100%;margin-top:2.0em;">
	                <iframe width="468" height="263" src="http://www.youtube.com/embed/1SktTFlmApw?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;wmode=opaque" frameborder="0" allowfullscreen></iframe>
                </div>               
                <% end if %>
                <%
			else
				response.write "<div style=""width:700px; padding:20px;"" align=""center""><h3>No Products Matched Your Query!</h3></div>"
			end if
			%>          
        </td>
    </tr>
    <%
    sql	= "exec sp_getCatsByModel " & modelID & ", 0"
	session("errorSQL") = sql
'	response.write "<pre>" & sql & "</pre>"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	a = 0
	if not RS.eof then
	%>
    <!-- other accessories start -->
    <tr>
    	<td style="padding-top:50px;">
            <div class="graybox-middle" style="width:726px; border-left:1px solid #999; border-right:1px solid #999; padding:0px 10px 0px 10px;">
                <div class="graybox-inner" title="OTHER <%=ucase(brandName) & " " & ucase(modelName)%> ACCESSORIES" align="center">
                	<h3 class="brandModel">OTHER <%=ucase(brandName) & " " & ucase(modelName)%> ACCESSORIES</h3>
				</div>
            </div>
        </td>
    </tr>
    <tr>
    	<td>
            <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                <tr>    
    
    <%
		brandName = replace(brandName, "/", " / ")
		modelName = replace(modelName, "/", " / ")					
		do until RS.eof
			thisType = replace(RS("typeName"), "/", " / ")
			curTypeID = rs("typeid")
			if curTypeID = 16 then curTypeID = 15
			if curTypeID <> categoryID then
				strLink = replace(replace(replace(replace(replace(replace(rs("urlStructure"), "{brandid}", brandid), "{modelid}", modelid), "{typeid}", rs("typeid")), "{brandname}", formatSEO(brandName)), "{modelname}", formatSEO(modelName)), "{typename}", formatSEO(thisType))
				strLink = replace(strLink,".asp","")
				altText = modelName & " " & thisType
				%>
						<td align="center" valign="top" width="165" style="padding:15px 5px 30px 5px;">
							<table border="0" cellspacing="0" cellpadding="0" width="100%">
								<tr><td align="center"><a href="<%=strLink%>" title="<%=altText%>"><img src="/images/categories/we_cat_<%=rs("typeid")%>.jpg" width="165" border="0" alt="<%=altText%>" title="<%=altText%>" /></a></td></tr>
								<tr><td align="center" style="padding-top:10px;"><a href="<%=strLink%>" title="<%=altText%>" class="cellphone3-link"><%=altText%></a></td></tr>                            
							</table>
						</td>
				<%
				a = a + 1
				if a = 4 then
					response.write "</tr><tr>" & vbcrlf
					a = 0
				end if
			end if
			RS.movenext
		loop
		%>
                </tr>
            </table>
        </td>
    </tr>
    <!-- other accessories end -->
    <tr>
    	<td style="padding:20px 0px 20px 0px;">
        	<%
			turnToItemID = "BMC" & brandid & modelid & categoryid
			%>
           	<!--#include virtual="/includes/asp/inc_turnto.asp"-->
        </td>
	</tr>
	<%
	end if
	%>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
    <tr><td align="center" class="topText">
    	<h1 class="brandModelCat2">
		<%
			if seH1 = "" then
				response.write strH1
			else
				response.write seH1
			end if
		%>
        </h1></td></tr>
    <tr><td align="center" class="bottomText" style="font-size:12px;"><%=SeBottomText%></td></tr>
</table>
<div id="selectOptionHolder" style="display:none;">
	<%
	if prepStr(selectOptions) <> "" then
		selectOptions = left(selectOptions,len(selectOptions)-2)
		selectOptionsArray = split(selectOptions,"##")
	%>
	<form name="frmSearch" method="post" style="display:inline;">
    <select name="cbItem" onChange="if(this.value != ''){window.location=this.value;}">
        <option value="">Choose from the following</option>
        	<%
			for i = 0 to ubound(selectOptionsArray)
				useLink = selectOptionsArray(i)
				i = i + 1
				useName = selectOptionsArray(i)
			%>
	        <option value="<%=useLink%>" title="<%=formatSEO(useName)%>"><%=useName%></option>
            <%
			next
			%>
    </select>
    <%
	end if
	%>
</form>
</div>
<script>
document.getElementById("jumpToOptions").innerHTML = document.getElementById("selectOptionHolder").innerHTML
function viewMore(subtypeid)
{
	nextLink = document.getElementById('id_link_'+subtypeid).href;
	if (nextLink != "") window.location = nextLink;
}

function preloading(pItemID, selectedColorID)
{
	ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmc&uType=pre&itemid=' + pItemID + '&selectedColorID=' + selectedColorID, 'preloading_deck_' + pItemID);		
}

function changeProduct(pItemID, selectedColorID, numColors, selectedIDX, onSale)
{
	curObj = null;
	for(i=0; i < numColors; i++) 
	{
		curObj = 'div_colorOuterBox_' + pItemID + '_' + i;
		if (document.getElementById(curObj) != null) document.getElementById(curObj).className = 'colorBoxUnselected';
	}
	curObj = 'div_colorOuterBox_' + pItemID + '_' + selectedIDX;
	document.getElementById(curObj).className = 'colorBoxSelected';
	
	ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmc&uType=img&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&onsale=' + onSale + '&adminOption=<%=prepInt(session("adminID"))%>', 'id_productImage_'+pItemID);
	ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmc&uType=link&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&itemMaxLen=<%=nItemDescMaxLength%>&adminOption=<%=prepInt(session("adminID"))%>', 'id_productLink_'+pItemID);
	ajax('/ajax/ajaxSwapProductColor.asp?uPage=bmc&uType=price&itemid=' + pItemID + '&selectedColorID=' + selectedColorID + '&adminOption=<%=prepInt(session("adminID"))%>', 'id_productPricing_'+pItemID);
}
</script>