<%header = 0%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
dim nAccountID, nOrderID, InvoiceType
nOrderID = request("orderID")
InvoiceType = "Admin"
dim stockDetail : stockDetail = prepInt(request.QueryString("stockDetail"))

if stockDetail > 0 then
	sql =	"exec sp_invoiceStockDetail " & stockDetail
	session("errorSQL") = sql
	set stockRS = oConn.execute(sql)
	
	if stockRS.EOF then
%>
<div style="width:100%; height:30px; font-weight:bold; color:#900; font-size:12px; text-align:center;">NOT A VALID ITEMID!</div>
<%
		response.End()
	end if
	
	invQty			= stockRS("inv_qty")
	voOrderID		= stockRS("orderID")
	voOrderAmt		= stockRS("orderAmt")
	voProcessDate	= stockRS("processOrderDate")
	AvgShip			= stockRS("avgShip")
	
	if not isnull(voProcessDate) then
		dayBump = 1
		availableDate = formatDateTime(cdate(voProcessDate) + cint(AvgShip) + dayBump,1)
		if instr(availableDate,"Saturday") > 0 then dayBump = 3
		if instr(availableDate,"Sunday") > 0 then dayBump = 2
		availableDate = formatDateTime(cdate(voProcessDate) + cint(AvgShip) + dayBump,1)
		if cdate(voProcessDate) + cint(AvgShip) + dayBump < now then availableDate = "Product Unavailable"
	else
		availableDate = "Unprocessed Order"
	end if
	
	if invQty < 4 then
	%>
    <div style="width:100%; height:32px; border:1px solid #000;">
        <div style="display:table; border-bottom:1px solid #000; width:100%;">
            <div style="float:left; width:60px; border-right:1px solid #000; padding-left:2px;">On Order</div>
            <div style="float:left; width:225px; border-right:1px solid #000; margin-left:10px;">Available On About</div>
            <div style="float:left; margin-left:10px;">Order #</div>
        </div>
        <% if not isnull(voOrderID) then %>
        <div style="display:table; width:100%;">
            <div style="float:left; width:60px; border-right:1px solid #000; padding-left:2px;"><%=voOrderAmt%></div>
            <div style="float:left; width:225px; border-right:1px solid #000; margin-left:10px;"><%=availableDate%></div>
            <div style="float:left; margin-left:10px;"><%=voOrderID%></div>
        </div>
        <% else %>
        <div style="display:table; width:100%; text-align:center; font-size:12px; font-weight:bold; padding:2px;">Item is not currently on order</div>
        <% end if %>
    </div>
    <% else %>
    <div style="width:100%; height:30px; font-weight:bold; font-size:12px; text-align:center;">Ready to ship</div>
	<%
	end if
	response.End()
end if

if nOrderID > 0 then
	dim sCheckSql, oRsCheck
	nAccountID = 0
	sCheckSql = "SELECT accountID FROM we_Orders WHERE orderID = '" & nOrderID & "'"
	set oRsCheck = oConn.execute(sCheckSql)

	if oRsCheck.eof then
		sCheckSql = replace(sCheckSql, "we_Orders", "we_Orders_historical")
		set oRsCheck = oConn.execute(sCheckSql)
	end if		

	if not oRsCheck.eof then nAccountID = oRsCheck("accountID")
	oRsCheck.close
	set oRsCheck = nothing
	
	if nAccountID > 0 then
		%>
        <div id="orderConDiv">
		<!--#include virtual="/includes/asp/inc_receiptAdmin.asp"-->
        </div>
		<%
	else
		response.redirect "view_invoice.asp?error=noreturn"
		response.end
	end if
else
	%>
	<form name="form1" method="post" action="view_invoice.asp">
		<table width="250" border="0" align="center" cellpadding="1" cellspacing="0">
			<tr bgcolor="#FF6600">
				<td align="center" nowrap>
					<p><font face="Verdana" size="2" color="#FFFFFF"><b>
					<%if request("error") = "noreturn" then response.write "No data to match your request<br>"%>
					</b></font></p>
				</td>
			</tr>
			<tr>
				<td bgcolor="#000066">
					<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0" bgcolor="#EAEAEA">
						<tr bgcolor="#FF6600">
							<td width="28%"><p><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>Account ID:</b></font></p></td>
							<td width="72%"><input type="text" name="accountID" id="accountID"></td>
						</tr>
						<tr bgcolor="#FF6600">
							<td width="28%"><p><b><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Order ID:</font></b></p></td>
							<td width="72%"><input type="text" name="orderID" id="orderID"></td>
						</tr>
						<tr align="center" bgcolor="#FF6600">
							<td colspan="2"><input type="submit" name="Submit" value="View Invoice"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	<%
end if
%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function ajax(newURL,rLoc) {
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						//nothing
					}
					else if(document.getElementById(rLoc)) {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				}
				else {
					alert("Error performing action")
				}
			}
		};
		httpRequest.send(null);
	}
	
	function showHide(showDiv,hideDiv) {
		document.getElementById(hideDiv).style.display = 'none';
		document.getElementById(showDiv).style.display = '';
	}
	
	function stockDetail(itemID) {
		ajax('/admin/view_invoice.asp?stockDetail=' + itemID,'stockDetailBox')
	}
</script>
