<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
strSDate	=	prepStr(request("txtSDate"))
strEDate	=	prepStr(request("txtEDate"))
siteid		=	request("cbStore")
adminid		=	prepInt(request("cbAdmin"))
mobilesite	=	prepInt(request("cbMobile"))

if not isdate(strEDate) then
	sql	=	"select	convert(varchar(10), max(a.orderdatetime), 20) dateRecent from we_orders a join we_phoneorders b on a.orderid = b.orderid"
	session("errorSQL") = sql
	set rsDate = oConn.execute(sql)
	if rsDate.eof then 
		strEDate = date
	else
		strEDate = cdate(rsDate("dateRecent"))
	end if
end if	

if not isdate(strSDate) then
	strSDate = strEDate - 14
end if

strSDate = cdate(strsDate)
strEDate = cdate(strEDate)

filename	=	"PhoneOrderReport2_" & Year(now) & Month(now) & Day(now) & Hour(now) & Minute(now) & Second(now) & ".xls"

Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition","attachment;filename="""&filename&""""

sql	=	"select	a.store, convert(varchar(16), a.orderdatetime, 20) orderdatetime, a.orderid, a.accountid, b.specialnotes, c.fname + ' ' + c.lname adminName, d.shortDesc, d.color siteColor, e.email, e.phone, f.promoCode" & vbcrlf & _
		"	,	isnull(cast(a.ordergrandtotal as money), 0) orderGrandTotal, isnull(cast(a.ordersubtotal as money), 0) orderSubTotal, isnull(cast(a.ordershippingfee as money), 0) orderShippingFee, isnull(a.orderTax, 0) orderTax" & vbcrlf & _
		"	,	isnull(b.shippingDiscount, 0) shippingdiscount, isnull(b.itemDiscount, 0) itemDiscount" & vbcrlf & _
		"	,	a.shiptype, a.approved, isnull(a.cancelled, 0) cancelled, a.transactionID, a.mobilesite" & vbcrlf & _
		"from	we_orders a join we_phoneorders b" & vbcrlf & _
		"	on	a.orderid = b.orderid join we_adminUsers c" & vbcrlf & _
		"	on	b.adminid = c.adminid join xstore d" & vbcrlf & _
		"	on	a.store = d.site_id join v_accounts e" & vbcrlf & _
		"	on	a.accountid = e.accountid and a.store = e.site_id left outer join v_coupons f" & vbcrlf & _
		"	on	a.couponid = f.couponid and a.store = f.site_id " & vbcrlf & _
		"where	a.orderdatetime >= '" & strSDate & "' and a.orderdatetime < '" & (strEDate+1) & "'" & vbcrlf
if siteid <> "" then
	sql = sql & "	and	d.site_id = " & siteid & vbcrlf
end if
if adminid > 0 then
	sql = sql & "	and	b.adminid = " & adminid & vbcrlf
end if
if mobilesite > 0 then
	sql = sql & "	and	a.mobileSite = 1" & vbcrlf
end if
sql = sql & "order by 2 desc"
session("errorSQL") = sql

set rs = CreateObject("ADODB.Recordset")
rs.open sql, oConn, 0, 1

if rs.eof then
	response.write "No records found"
else
	lap = -1
	response.write "Store" & vbTab & "Order ID" & vbTab & "OrderDateTime" & vbTab & "Customer Email" & vbTab & "Customer Phone" & vbTab & "OrderGrandTotal" & vbTab & "DiscountTotal" & vbTab & "Discount(Promo)" & vbTab
	response.write "Discount(shipping)" & vbTab & "Discount(Item)" & vbTab & "shiptype" & vbTab & "approved / cancelled" & vbTab & "Mobile" & vbTab & "adminUserName" & vbNewLine
	do until rs.eof
		lap = lap + 1
		nDiscountTotal = cdbl(0)
		nPromtionDiscount = (rs("orderSubTotal") + rs("orderShippingFee") + rs("orderTax")) - rs("orderGrandTotal")
		nDiscountTotal = nPromtionDiscount + rs("shippingdiscount") + rs("itemDiscount")
		response.write rs("shortDesc") & vbTab
		response.write rs("orderid") & vbTab
		response.write rs("orderdatetime") & vbTab
		response.write rs("email") & vbTab
		response.write rs("phone") & vbTab
		response.write formatcurrency(rs("orderGrandTotal")) & vbTab
		response.write formatcurrency(nDiscountTotal*(-1)) & vbTab
		response.write formatcurrency(nPromtionDiscount*(-1)) & vbTab
		response.write formatcurrency(rs("shippingdiscount")*(-1)) & vbTab
		response.write formatcurrency(rs("itemDiscount")*(-1)) & vbTab
		response.write rs("shiptype") & vbTab
		if rs("approved") then 
			response.write "Yes"
		else
			response.write "No"
		end if
		response.write " / "
		if rs("cancelled") then 
			response.write "Yes" & vbTab
		else
			response.write "No" & vbTab
		end if
		if rs("mobilesite") then 
			response.write "Yes" & vbTab
		else
			response.write "No" & vbTab
		end if		
		response.write rs("adminName") & vbNewLine
																		
		rs.movenext
	loop
end if
%>