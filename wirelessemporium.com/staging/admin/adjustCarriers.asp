<%
	response.buffer = true
	response.expires = -1

	thisUser = Request.Cookies("username")
	pageTitle = "Ajust Carriers"
	
	brand = prepStr(request.QueryString("brand"))
	showModel = prepInt(request.QueryString("showModel"))
	modelID = prepInt(request.QueryString("modelID"))
	carrierCode = prepStr(request.QueryString("carrierCode"))
	carrierAction = prepStr(request.QueryString("carrierAction"))
	phoneCode = prepStr(request.QueryString("phoneCode"))
	
	if brand = "" then header = 1 else header = 0
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<%
	if brand = "" then
		sql = "select brandID, brandName from we_brands order by brandName"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 3, 3
%>
<table border="0" cellpadding="3" cellpadding="0" width="85%" align="center">
	<tr><td><hr color="#000000" /></td></tr>
    <tr><td align="left" style="font-weight:bold; font-size:18px;">Adjust Product Carriers</td></tr>
    <tr><td><hr color="#000000" /></td></tr>
    <tr>
    	<td align="center">
        	<table border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td>Select Brand:</td>
                    <td>
                    	<select name="brand" onchange="getData(this.value)">
                        	<option value=""></option>
                            <% do while not rs.EOF %>
                        	<option value="<%=rs("brandID")%>"><%=rs("brandName")%></option>
                            <%
								rs.movenext
							loop
							%>
                        </select>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td id="resultPane"></td>
    </tr>
</table>
<%
	elseif carrierCode <> "" then
		if carrierAction = "true" then
			sql = "update we_models set carrierCode = carrierCode + '" & carrierCode & ",' where modelID = " & modelID
		else
			sql = "update we_models set carrierCode = replace(carrierCode,'" & carrierCode & ",','') where modelID = " & modelID
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
		response.End()
	elseif phoneCode <> "" then
		if carrierAction = "true" then
			sql = "update we_models set phoneCode = phoneCode + '" & phoneCode & ",' where modelID = " & modelID
		else
			sql = "update we_models set phoneCode = replace(phoneCode,'" & phoneCode & ",','') where modelID = " & modelID
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
		response.End()
	elseif showModel > 0 then
		sql = "select * from we_models where modelID = " & showModel & " and brandID = " & brand & " and carrierCode is not null and carrierCode <> ',' order by modelName"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 3, 3
%>
<%=rs("modelName")%><br />
<img src="/productpics/models/<%=rs("modelImg")%>" border="0" /><br />
<a href="/admin/db_update_models.asp?ModelID=<%=rs("modelID")%>&BrandID=<%=brand%>&searchType=Model&submitType=Edit&submitModel=Search" target="_blank">Edit Model Details</a><br />
<img src="/images/blank.gif" border="0" width="250" height="1" />
<%
		response.End()
	else
		sql = "select * from we_models where brandID = " & brand & " order by modelName"
		'sql = "select * from we_models where brandID = " & brand & " and carrierCode is not null and carrierCode <> ',' order by modelName"
		session("errorSQL") = sql
		Set rs = oConn.execute(sql)
%>
<table border="0" cellpadding="3" cellpadding="0" align="center">
    <tr>
    	<td valign="top" align="center" id="showModelDetails">
        	<%=rs("modelName")%><br />
            <img src="/productpics/models/<%=rs("modelImg")%>" border="0" /><br />
            <a href="/admin/db_update_models.asp?ModelID=<%=rs("modelID")%>&BrandID=<%=brand%>&searchType=Model&submitType=Edit&submitModel=Search" target="_blank">Edit Model Details</a><br />
            <img src="/images/blank.gif" border="0" width="250" height="1" />
        </td>
        <td valign="top">
            <table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" align="center">
                <tr bgcolor="#333333" style="font-weight:bold; color:#FFF; font-size:10px;">
                    <td nowrap="nowrap">Model Name</td>
                    <td>Alltel</td>
                    <td align="center" nowrap="nowrap">AT&amp;T<br />Cingular</td>
                    <td align="center" nowrap="nowrap">Boost Mobile<br />Southern Linc</td>
                    <td>Cricket</td>
                    <td nowrap="nowrap">Metro PCS</td>
                    <td>Prepaid</td>
                    <td align="center" nowrap="nowrap">Sprint<br />Nextel</td>
                    <td nowrap="nowrap">T-Mobile</td>
                    <td nowrap="nowrap">U.S. Cellular</td>
                    <td>Unlocked</td>
                    <td>Verizon</td>
                    <td bgcolor="#0000FF">Flip</td>
                    <td bgcolor="#0000FF">Qwerty</td>
                    <td bgcolor="#0000FF">Slider</td>
                    <td bgcolor="#0000FF">Touch</td>
                </tr>
                <%
                bgColor = "#ffffff"
				lap = 0
                do while not rs.EOF
					lap = lap + 1
					modelID = rs("modelID")
					modelName = rs("modelName")
					carrierCode = rs("carrierCode")
					phoneCode = rs("phoneCode")
                %>
                <tr bgcolor="<%=bgColor%>">
                    <td><a style="cursor:pointer; color:#930;" onclick="ajax('/admin/adjustCarriers.asp?brand=<%=brand%>&showModel=<%=modelID%>','showModelDetails')"><%=modelName%></a></td>
                    <td align="center" title="Alltel"><input type="checkbox" name="activeCarrier" value="1" onclick="updateCarrier(<%=brand%>,<%=modelID%>,'AL',this.checked)"<% if instr(ucase(carrierCode),"AL") > 0 then %> checked="checked"<% end if %> /></td>
                    <td align="center" title="AT&amp;T"><input type="checkbox" name="activeCarrier" value="1" onclick="updateCarrier(<%=brand%>,<%=modelID%>,'CG',this.checked)"<% if instr(ucase(carrierCode),"CG") > 0 then %> checked="checked"<% end if %> /></td>
                    <td align="center" title="Boost Mobile"><input type="checkbox" name="activeCarrier" value="1" onclick="updateCarrier(<%=brand%>,<%=modelID%>,'BM',this.checked)"<% if instr(ucase(carrierCode),"BM") > 0 then %> checked="checked"<% end if %> /></td>
                    <td align="center" title="Cricket"><input type="checkbox" name="activeCarrier" value="1" onclick="updateCarrier(<%=brand%>,<%=modelID%>,'CK',this.checked)"<% if instr(ucase(carrierCode),"CK") > 0 then %> checked="checked"<% end if %> /></td>
                    <td align="center" title="Metro PCS"><input type="checkbox" name="activeCarrier" value="1" onclick="updateCarrier(<%=brand%>,<%=modelID%>,'MP',this.checked)"<% if instr(ucase(carrierCode),"MP") > 0 then %> checked="checked"<% end if %> /></td>
                    <td align="center" title="Prepaid"><input type="checkbox" name="activeCarrier" value="1" onclick="updateCarrier(<%=brand%>,<%=modelID%>,'PP',this.checked)"<% if instr(ucase(carrierCode),"PP") > 0 then %> checked="checked"<% end if %> /></td>
                    <td align="center" title="Sprint"><input type="checkbox" name="activeCarrier" value="1" onclick="updateCarrier(<%=brand%>,<%=modelID%>,'SN',this.checked)"<% if instr(ucase(carrierCode),"SN") > 0 then %> checked="checked"<% end if %> /></td>
                    <td align="center" title="T-Mobile"><input type="checkbox" name="activeCarrier" value="1" onclick="updateCarrier(<%=brand%>,<%=modelID%>,'TM',this.checked)"<% if instr(ucase(carrierCode),"TM") > 0 then %> checked="checked"<% end if %> /></td>
                    <td align="center" title="U.S. Cellular"><input type="checkbox" name="activeCarrier" value="1" onclick="updateCarrier(<%=brand%>,<%=modelID%>,'US',this.checked)"<% if instr(ucase(carrierCode),"US") > 0 then %> checked="checked"<% end if %> /></td>
                    <td align="center" title="Unlocked"><input type="checkbox" name="activeCarrier" value="1" onclick="updateCarrier(<%=brand%>,<%=modelID%>,'UN',this.checked)"<% if instr(ucase(carrierCode),"UN") > 0 then %> checked="checked"<% end if %> /></td>
                    <td align="center" title="Verizon"><input type="checkbox" name="activeCarrier" value="1" onclick="updateCarrier(<%=brand%>,<%=modelID%>,'VW',this.checked)"<% if instr(ucase(carrierCode),"VW") > 0 then %> checked="checked"<% end if %> /></td>
                    <td align="center" title="PhoneType-Flip"><input type="checkbox" name="PhoneType" value="1" onclick="updatePhoneType(<%=brand%>,<%=modelID%>,'Flip',this.checked)"<% if instr(ucase(phoneCode),"FLIP") > 0 then %> checked="checked"<% end if %> /></td>
                    <td align="center" title="PhoneType-Qwerty"><input type="checkbox" name="PhoneType" value="1" onclick="updatePhoneType(<%=brand%>,<%=modelID%>,'Qwerty',this.checked)"<% if instr(ucase(phoneCode),"QWERTY") > 0 then %> checked="checked"<% end if %> /></td>
                    <td align="center" title="PhoneType-Slider"><input type="checkbox" name="PhoneType" value="1" onclick="updatePhoneType(<%=brand%>,<%=modelID%>,'Slider',this.checked)"<% if instr(ucase(phoneCode),"SLIDER") > 0 then %> checked="checked"<% end if %> /></td>
                    <td align="center" title="PhoneType-Touch Screen"><input type="checkbox" name="PhoneType" value="1" onclick="updatePhoneType(<%=brand%>,<%=modelID%>,'Touch',this.checked)"<% if instr(ucase(phoneCode),"TOUCH") > 0 then %> checked="checked"<% end if %> /></td>
                </tr>
                <%
                    rs.movenext
                    if bgColor = "#ffffff" then bgColor = "#f0f0f0" else bgColor = "#ffffff"
                loop
                %>
                <tr bgcolor="#333333"><td align="right" colspan="16" style="font-weight:bold; color:#FFF; font-size:10px;"><%=lap%> Total Products</td></tr>
            </table>
        </td>
    </tr>
</table>
<%
		response.End()
	end if
%>
<div id="dataOnly" style="visibility:;"></div>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">
	function getData(myVal){
		if (myVal == "") {
			document.getElementById("resultPane").innerHTML = ""
		}
		else {
			ajax('/admin/adjustCarriers.asp?brand=' + myVal,'resultPane')
		}
	}
	
	function updateCarrier(brand,modelID,carrier,action) {
		ajax('/admin/adjustCarriers.asp?brand=' + brand + '&modelID=' + modelID + '&carrierCode=' + carrier + '&carrierAction=' + action,'dataOnly')
	}
	
	function updatePhoneType(brand,modelID,phoneCode,action) {
		ajax('/admin/adjustCarriers.asp?brand=' + brand + '&modelID=' + modelID + '&phoneCode=' + phoneCode + '&carrierAction=' + action,'dataOnly')
	}
	
	function ajax(newURL,rLoc) {
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200)
				{
					badRun = 0
					var rVal = httpRequest.responseText
					if(document.getElementById(rLoc)) {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				} else {
					badRun++
					if (badRun == 5) {
						badRun = 0
						alert("Error performing action")
					}
					else {
						setTimeout("ajax(newURL,rLoc)",500)
					}
				}
			}
		};
		httpRequest.send(null);
	}
</script>
