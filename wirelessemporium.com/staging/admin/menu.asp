<%
dim thisUser
pageTitle = "Main Menu"
header = 1
thisUser = lcase(Request.Cookies("username"))

session("adminID") = request.Cookies("adminID")

dim myLinks : myLinks = prepStr(request.Cookies("myLinks"))
dim linkDate : linkDate = prepStr(request.Cookies("linkDate"))
if instr(myLinks,"%") > 0 then
	response.Cookies("myLinks") = replace(replace(replace(replace(replace(myLinks,"%2F","/"),"%2E","."),"%2C",","),"%5F","_"),"%3F","?")
	myLinks = prepStr(request.Cookies("myLinks"))
end if
if myLinks <> "" then
	response.Cookies("myLinks").expires = date + 45
	response.Cookies("linkDate").expires = date + 45
	dim linkArray : linkArray = split(myLinks,",")
	response.Write("got it:" & myLinks)
else
	response.Cookies("linkDate") = date
	response.Cookies("linkDate").expires = date + 45
end if

function arrayVal(searchVal,arrayObj)
	returnVal = -1
	if myLinks <> "" then
		for i = 0 to ubound(arrayObj)
			if arrayObj(i) = searchVal then returnVal = 1
		next
		arrayVal = returnVal
	end if
end function

response.Write("<!-- sessionName:" & session("username") & " -->")
response.Write("<!-- cookieName:" & Request.Cookies("username") & " -->")
response.Write("<!-- productLvl:" & session("productLvl") & " -->")
response.Write("<!-- orderLvl:" & session("orderLvl") & " -->")
response.Write("<!-- marketingLvl:" & session("marketingLvl") & " -->")
%>
<!--#include virtual="/includes/admin/admin_top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
dim pass1 : pass1 = prepStr(request.Form("pass1"))

if pass1 <> "" then
	sql = "update we_adminUsers set password = '" & pass1 & "' where adminID = " & adminID
	session("errorSQL") = sql
	oConn.execute(sql)
end if

sql = "select fname, department, password from we_adminUsers where adminID = " & adminID
session("errorSQL") = sql
set adminRS = oConn.execute(sql)

if adminID = 54 then
	sql = "select top 15 count(*) as numberOfSales, b.partNumber, c.inv_qty, c.itemID from we_orders a left join we_orderdetails b on a.orderid = b.orderid join we_Items c on b.partNumber = c.PartNumber and c.master = 1 and c.inv_qty > 0 where a.store = 0 and a.approved = 1 and a.cancelled is null and a.parentOrderID is null and a.orderdatetime > '" & date-30 & "' and c.hideLive = 0 group by b.partNumber, c.inv_qty, c.itemID order by 1 desc"
	session("errorSQL") = sql
	set weRecommendsRS = oConn.execute(sql)
	
	if not weRecommendsRS.EOF then
		sql = "delete from we_recommends where site_ID = 0"
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	dim recLap : recLap = 0
	do while not weRecommendsRS.EOF
		recLap = recLap + 1
		sql = "insert into we_recommends (itemID,partNumber,sortOrder,site_id) values(" & weRecommendsRS("itemID") & ",'" & weRecommendsRS("partNumber") & "'," & recLap & ",0)"
		session("errorSQL") = sql
		oConn.execute(sql)
		weRecommendsRS.movenext
	loop
	
	sql = "select b.partNumber, a.id from we_Reviews a join we_Items b on a.itemID = b.itemID where PartNumbers = ''"
	session("errorSQL") = sql
	set reviewsRS = oConn.execute(sql)
	
	do while not reviewsRS.EOF
		sql = "update we_Reviews set partnumbers = '" & reviewsRS("partnumber") & "' where id = " & reviewsRS("id")
		session("errorSQL") = sql
		oConn.execute(sql)
		
		reviewsRS.movenext
	loop
	
	sql = "select (select itemID from we_Items where PartNumber = a.PartNumber and master = 1) as existingMaster, a.itemID, a.inv_qty, a.partNumber from we_Items a where a.inv_qty > 0 and a.master = 0 order by partNumber"
	session("errorSQL") = sql
	set masterFixRS = oConn.execute(sql)
	
	curPN = ""
	do while not masterFixRS.EOF
		if curPN <> masterFixRS("partNumber") then
			existingMaster = masterFixRS("existingMaster")
			curItemID = masterFixRS("itemID")
			curInv = masterFixRS("inv_qty")
			curPN = masterFixRS("partNumber")
			if isnull(masterFixRS("existingMaster")) then
				sql = "update we_Items set master = 1 where itemID = " & curItemID
				session("errorSQL") = sql
				oConn.execute(sql)
				
				sql = "update we_Items set inv_qty = -1 where master = 0 and partNumber = '" & curPN & "'"
				session("errorSQL") = sql
				oConn.execute(sql)
			else
				sql = "update we_Items set inv_qty = inv_qty + " & curInv & " where itemID = " & existingMaster
				session("errorSQL") = sql
				oConn.execute(sql)
				
				sql = "insert into we_invRecord (itemID,inv_qty,orderQty,adjustQty,orderID,adminID,notes) values(" & existingMaster & ",0,0,0,0,54,'MasterFix for " & curPN & "')"
				session("errorSQL") = sql
				oConn.execute(sql)
				
				sql = "update we_Items set inv_qty = -1 where master = 0 and partNumber = '" & curPN & "'"
				session("errorSQL") = sql
				oConn.execute(sql)
			end if
		end if
		masterFixRS.movenext
	loop
	
	sql = "select distinct a.itemID, b.PartNumber, (select top 1 COGS from we_Items where partNumber = b.PartNumber and master = 1) as cogs from we_orderdetails a join we_Items b on a.itemid = b.itemID where a.itemid <> 98804 and a.entryDate > '" & date-1 & "' and (select top 1 COGS from we_Items where partNumber = b.PartNumber and master = 1) is null"
	session("errorSQL") = sql
	set noCogsRS = oConn.execute(sql)
	
	dim pnList : pnList = ""
	do while not noCogsRS.EOF
		pnList = pnList & noCogsRS("partNumber") & "<br />"
		noCogsRS.movenext
	loop
	if pnList <> "" then
		CDOSend "jon@wirelessemporium.com, sherwood@wirelessemporium.com, steven@wirelessemporium.com","noCogs@wirelessemporium.com","No Cogs Found!","Part numbers recently sold without Cogs:<br />" & pnList
	end if
end if

if isnull(session("productLvl")) or len(session("productLvl")) < 1 then
	sql = "select productLvl, orderLvl, marketingLvl from we_adminUsers where adminID = " & session("adminID")
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	if not rs.EOF then
		session("productLvl") = rs("productLvl")
		session("orderLvl") = rs("orderLvl")
		session("marketingLvl") = rs("marketingLvl")
	end if
end if

if request("submitted") = "1" then
	SQL = "UPDATE we_tollfree SET active=" & request("tollfree")
	Set RS = oConn.execute(SQL)
end if
SQL = "SELECT active FROM we_tollfree"
set RS = oConn.execute(SQL)
if not RS.eof then
	active = RS("active")
else
	active = 0
end if
set RS = nothing
%>
<style>
	.linkVanishing { opacity:0.4;filter:alpha(opacity=40); }
	.linkGone { display:none; }
</style>
<% if instr(adminRS("password"),"7895") > 0 or instr(adminRS("password"),"9513") > 0 then %>
<table width="752" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr bgcolor="#FF0000" style="color:#FFF;">
    	<td align="center">
        	<form name="newPassForm" action="/admin/menu.asp" method="post" onsubmit="return(testPass())">
            <div style="width:300px; height:150px; text-align:center;">
            	<div style="text-align:center;">
                	PLEASE ENTER A NEW PASSWORD<br />
                    <span style="font-size:9px;">must be 7 characters in length</span>
                </div>
                <div style="padding-top:10px;"><input type="password" name="pass1" value="" /></div>
                <div style="padding-top:10px;"><input type="password" name="pass2" value="" /></div>
                <div style="padding-top:10px;"><input type="submit" name="mySub" value="Update Password" /></div>
            </div>
            </form>
        </td>
    </tr>
</table>
<% end if %>

<% if "Philippines" = adminRS("department") and "Joseph" = adminRS("fname") then %>
	<center>
	<a onclick="cookieLink(this.href)" href="/admin/report_OrdersStats.asp">Daily Order Statistics - WE</a><br />
	<a onclick="cookieLink(this.href)" href="/admin/report_OrdersStats_CO.asp">Daily Order Statistics - CO</a><br />
	<a onclick="cookieLink(this.href)" href="/admin/report_OrdersStats_CA.asp">Daily Order Statistics - CA</a><br />
	<a onclick="cookieLink(this.href)" href="/admin/report_OrdersStats_PS.asp">Daily Order Statistics - PS</a><br />
	<a onclick="cookieLink(this.href)" href="/admin/report_OrdersStats_TM.asp">Daily Order Statistics - TM</a><br />
    </center>
<% else %>
<form name="frmTollFree" action="menu.asp" method="post">
<table width="752" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr bgcolor="#CCCCCC">
		<td width="100%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" valign="top" bgcolor="#CCCCCC" class="smlText">
						&nbsp;Toll-Free&nbsp;#:&nbsp;
						<input type="radio" name="tollfree" value="-1" onClick="document.frmTollFree.submit();"<%if active = -1 then response.write " checked"%>>Show&nbsp;
						<input type="radio" name="tollfree" value="0" onClick="document.frmTollFree.submit();"<%if active = 0 then response.write " checked"%>>Hide
						<input type="hidden" name="submitted" value="1">
					</td>
					<td align="right" valign="top" bgcolor="#CCCCCC" class="normalText">
						<a onclick="cookieLink(this.href)" href="/admin/WE-ee.asp">WE Employee Page</a>&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%" valign="middle">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="45%" valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<% if session("adminID") = 54 or session("adminID") = 62 or session("adminID") = 22 or session("adminID") = 96 then %>
                            <tr>
                            	<td>
                                	<strong><br>Beta Testing:</strong>
                                    <ol class="smlText" style="margin-top:2px;">
                                        <li<% if arrayVal("/admin/mainbanners.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/mainbanners.asp">Update Banners</a></li>
                                        <li<% if arrayVal("/admin/musicSkins_remove.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/musicSkins_remove.asp">Music Skins Remove</a></li>
                                        <li<% if arrayVal("/admin/db_update_qty_partnumber.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_qty_partnumber.asp">Update QTY with PartNumber</a></li>
                                        <li<% if arrayVal("/admin/adminUsers",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/adminUsers">Admin Users</a></li>
                                        <li<% if arrayVal("/admin/deleteCompFiles.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/deleteCompFiles.asp">Delete Compressed Pages</a></li>
                                        <li<% if arrayVal("/admin/clearOldCustomCaseFiles.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/clearOldCustomCaseFiles.asp">Clean Custom Case Files</a></li>
                                        <li<% if arrayVal("/admin/killProducts.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/killProducts.asp">Kill Products</a></li>
                                    </ol>                                    
                                </td>
                            </tr>
                            <% end if %>
                            <tr>
								<td width="350" valign="top" class="normalText">
									<p>
										<strong><br>Admin Tasks:</strong>
										<ol class="smlText" style="margin-top:2px;">
                                        	<li<% if arrayVal("/admin/db_search_brands.asp?searchID=2",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_search_brands.asp?searchID=2">Update Known Product Issues</a></li>
											<li<% if arrayVal("/admin/view_invoice.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/view_invoice.asp">Create an Invoice</a></li>
											<li<% if arrayVal("/admin/custlist.asp?display=none",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/custlist.asp?display=none">Customer Look Up</a></li>
											<li<% if arrayVal("/admin/custBlockIp.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/custBlockIp.asp">Unblock IP Address</a></li>
											<li<% if arrayVal("/admin/CompatibleProducts.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/CompatibleProducts.asp">Compatible Products Search</a></li>
											<li<% if arrayVal("/admin/db_search_brands.asp?searchID=6",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_search_brands.asp?searchID=6">Faceplate Search</a></li>
											<li<% if arrayVal("/admin/faceplates.asp?charms=1",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/faceplates.asp?charms=1">Charms Search</a></li>
											<li<% if arrayVal("/admin/CustomerService.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/CustomerService.asp">Customer Service Reference</a></li>
											<li<% if arrayVal("/admin/db_update_promo_notes.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_promo_notes.asp">Notes on WE Promotions</a></li>
                                            <li<% if arrayVal("/admin/productLookup.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/productLookup.asp">Product Look Up</a></li>
										</ol>
									</p>
								</td>
							</tr>
							<tr>
								<td bgcolor="#E95516" height="2" style="font-size:1px;"><img src="/images/spacer.gif" width="2" height="2" border="0"></td>
							</tr>
                            <tr><td width="350" valign="top" class="normalText"><strong><br>Database Updates:</strong></td></tr>
                            <tr>
                            	<td width="350" valign="top">
                                	<%
									selectedTab = "FF0"
									if session("productLvl") > 0 then
									%>
                                    <div style="float:left; background-color:#333; cursor:pointer; font-size:12px; font-weight:bold; border-right:1px solid #fff; padding:0px 5px 0px 5px; color:#<%=selectedTab%>;" onclick="menuTab('products')" id="productsTab">Products</div>
									<%
										selectedTab = "FFF"
									end if
									%>
                                    <% if session("orderLvl") > 0 then %>
                                    <div style="float:left; background-color:#333; cursor:pointer; font-size:12px; font-weight:bold; border-right:1px solid #fff; padding:0px 5px 0px 5px; color:#<%=selectedTab%>;" onclick="menuTab('orders')" id="ordersTab">Orders</div>
									<%
										selectedTab = "FFF"
									end if
									%>
                                    <% if session("marketingLvl") > 0 then %><div style="float:left; background-color:#333; cursor:pointer; font-size:12px; font-weight:bold; padding:0px 5px 0px 5px; color:#<%=selectedTab%>;" onclick="menuTab('marketing')" id="marketingTab">Marketing</div><% end if %>
                                </td>
                            </tr>
							<tr>
								<td width="350" valign="top" class="normalText">
									<% if session("productLvl") > 0 then %>
                                    <div id="products">
                                    	<ol class="smlText" style="margin-top:2px;">
                                        	<li<% if arrayVal("/admin/vendorPartNumbers_enter.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/vendorPartNumbers_enter.asp">Vendor Enter Order/Part Numbers</a></li>
                                            <li<% if arrayVal("/admin/adjustCarriers.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/adjustCarriers.asp">Adjust Model Carriers</a></li>
                                            <li<% if arrayVal("/admin/db_search_relatedItems.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_search_relatedItems.asp">Add/Delete Related Items</a></li>
											<li<% if arrayVal("/admin/db_search_mansubtypes.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_search_mansubtypes.asp">Manipulate Subtypes</a></li>
											<li<% if arrayVal("/admin/db_update_color_matrix.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_color_matrix.asp">Update Color Matrix</a></li>
											<li<% if arrayVal("/admin/db_update_newarrivals.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_newarrivals.asp">Update New Arrivals</a></li>
											<li<% if arrayVal("/admin/db_update_we_recommends.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_we_recommends.asp">Update WE Recommends</a></li>
                                            <li<% if arrayVal("/admin/db_search_brands.asp?searchID=8",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_search_brands.asp?searchID=8">Update WE Prices</a></li>
											<li<% if arrayVal("/admin/db_update_phones.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_phones.asp">Update Cell Phone Prices</a></li>
                                            <li<% if arrayVal("/admin/db_search_brands.asp?searchID=9",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_search_brands.asp?searchID=9">Update Flags</a></li>
											<li<% if arrayVal("/admin/db_search_brands.asp?searchID=10",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_search_brands.asp?searchID=10">Update Temp Values</a></li>
											<li<% if arrayVal("/admin/productReplace.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/productReplace.asp">Update Item Descriptions / Details</a></li>
                                            <li<% if arrayVal("/admin/missingCogs.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/missingCogs.asp">Enter Missing COGS</a></li>
                                            <li<% if arrayVal("/admin/db_search_models_types.asp?searchID=3",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_search_models_types.asp?searchID=3">Update Part Numbers</a></li>
                                            <li<% if arrayVal("/admin/db_search_brands.asp?searchID=4",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_search_brands.asp?searchID=4">Add/Edit Products</a></li>
											<li<% if arrayVal("/admin/db_search_brands.asp?searchID=5",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_search_brands.asp?searchID=5">Add/Edit Models</a></li>
                                            <li<% if arrayVal("/admin/createSlave.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/createSlave.asp">Create Slave</a></li>
                                            <li<% if arrayVal("/admin/reverseSlaveMaker.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/reverseSlaveMaker.asp">Reverse Slave Maker</a></li>
                                            <li<% if arrayVal("/admin/update_itemPictures.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/update_itemPictures.asp">Copy Master Pics/Desc/Price</a></li>
                                            <li<% if arrayVal("/admin/uploadDescArt.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/uploadDescArt.asp">Upload Product Description Art</a></li>
                                            <li<% if arrayVal("/admin/db_search_brands.asp?searchID=1",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_search_brands.asp?searchID=1">Update Product Details</a></li>
                                            <li<% if arrayVal("/admin/vendorList.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/vendorList.asp">Vendor List</a></li>
                                            <li<% if arrayVal("/admin/missingImages.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/missingImages.asp">Product Missing Images</a></li>
                                            <li<% if arrayVal("/admin/productQuickCreate.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/productQuickCreate.asp">Product Quick Create</a></li>
                                            <% if session("productLvl") > 1 then %>
                                            <li<% if arrayVal("/admin/zeroInventory.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/zeroInventory.asp">Zero Inventory</a></li>
                                            <li<% if arrayVal("/admin/inventoryAdjust.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/inventoryAdjust.asp">Inventory Adjust</a></li>
                                            <li<% if arrayVal("/admin/amazonProducts.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/amazonProducts.asp">Update Amazon Products</a></li>
                                            <li<% if arrayVal("/admin/pushChanges.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/pushChanges.asp">Push Changes Live</a></li>
                                            <li<% if arrayVal("/admin/db_update_images.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_images.asp">Update Website Images</a></li>
                                            <li<% if arrayVal("/admin/db_update_videos.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_videos.asp">Upload Video URL</a></li>
                                            <li<% if arrayVal("/admin/uploadProductDetails.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/uploadProductDetails.asp">Update Product Details (quick upload)</a></li>
                                            <li<% if arrayVal("/admin/customDesigns.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/customDesigns.asp">Custom Designs</a></li>
                                            <li<% if arrayVal("/admin/LightningDeals",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/LightningDeals">Lightning Deal Dashboard</a></li>
                                            <li<% if arrayVal("/admin/copylightningdeal.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/copylightningdeal.asp">Lightning Deal Project Duplicator</a></li>
                                            <li<% if arrayVal("/admin/featuredartist.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/featuredartist.asp">Featured Artists</a></li>
                                            <% end if %>
                                        </ol>
                                    </div>
                                    <%
									end if
									if session("orderLvl") > 0 then
									%>
                                    <div id="orders"<% if session("productLvl") > 0 then %> style="display:none;"<% end if %>>
                                    	<ol class="smlText" style="margin-top:2px;">
                                        	<li<% if arrayVal("/admin/db_update_order_status.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_order_status.asp">Update Order Status</a></li>
                                            <li<% if arrayVal("/admin/db_update_cancel_TEST_ORDERS.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_cancel_TEST_ORDERS.asp">Cancel TEST ORDERS</a></li>
                                            <% if session("orderLvl") > 1 then %>
                                            <li<% if arrayVal("/admin/db_search_rma.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_search_rma.asp">PROCESS BACK-ORDERS</a></li>
                                            <li<% if arrayVal("/admin/phoneorder.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" target="_blank" href="/admin/phoneorder.asp">Phone Order</a></li>
                                            <% end if %>
											<% if session("orderLvl") > 2 then %>
                                            <li<% if arrayVal("/admin/ProcessOrders.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/ProcessOrders.asp">PROCESS ORDERS</a></li>                                            
                                        	<li<% if arrayVal("/admin/uploadTrackingNums.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/uploadTrackingNums.asp">Upload Tracking Numbers</a></li>
                                        	<li<% if arrayVal("/admin/uploadmassivetrackingnum.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/uploadmassivetrackingnum.asp">New Upload Tracking Numbers</a></li>
                                            <li<% if arrayVal("/admin/db_update_order_cancellations.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_order_cancellations.asp">Update Order Cancellations</a></li>
                                            <li<% if arrayVal("/admin/db_update_ebillme_orders.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_ebillme_orders.asp">Update eBillme Orders</a></li>
                                            <li<% if arrayVal("/admin/uploadAtomicMall.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/uploadAtomicMall.asp">Upload AtomicMall Orders</a></li>
											<li<% if arrayVal("/admin/uploadAmazon.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/uploadAmazon.asp">Upload Amazon</a></li>
											<li<% if arrayVal("/admin/upload_UPS_MI.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/upload_UPS_MI.asp">Upload UPS Mail Innovations</a></li>
                                            <li<% if arrayVal("/admin/vendorOrders.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/vendorOrders.asp">Vendor Process Orders</a></li>
                                            <li<% if arrayVal("/admin/vendorReceiveOrders.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/vendorReceiveOrders.asp">Vendor Receive Orders</a></li>
                                            <li<% if arrayVal("/admin/vendorAdjustOrders.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/vendorAdjustOrders.asp">Vendor Adjust Orders</a></li>
                                            <li<% if arrayVal("/admin/uploadNewgistics.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/uploadNewgistics.asp">Upload Newgistics</a></li>
                                            <li<% if arrayVal("/admin/report_rmaReport.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_rmaReport.asp">RMA Return Report</a></li>
                                            <!--<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/uploadBuycom.asp">Upload Buy.com Orders</a></li>
											<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/uploadBuyComSKUs.asp">Upload Buy.com SKUs</a></li>
											<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_search_brands.asp?searchID=7">Update Buy.com Prices</a></li>
                                            <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/uploadEbayInventory.asp">Upload Ebay Inventory</a></li>-->
                                            <% end if %>
                                        </ol>
                                    </div>
                                    <%
									end if
									if session("marketingLvl") > 0 then
									%>
                                    <div id="marketing"<% if session("productLvl") > 0 or session("orderLvl") > 0 then %> style="display:none;"<% end if %>>
                                    	<ol class="smlText" style="margin-top:2px;">
                                        	<li<% if arrayVal("/admin/user_reviews.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/user_reviews.asp">Update User Reviews(New)</a></li>
                                        	<li<% if arrayVal("/admin/db_update_user_reviews.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_user_reviews.asp">Update User Reviews</a></li>
                                            <% if session("marketingLvl") > 1 then %>
                                        	<li<% if arrayVal("/admin/update_mainMenu.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/update_mainMenu.asp">Update PhoneSale Menus</a></li>
                                            <li<% if arrayVal("/admin/db_update_PressReleases.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_PressReleases.asp">Update Press Releases</a></li>
                                            <li<% if arrayVal("/admin/db_update_coupons.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_coupons.asp">Add/Edit Coupons</a></li>
											<li<% if arrayVal("/admin/db_update_links.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_links.asp">Add/Edit Links</a></li>
                                            <li<% if arrayVal("/admin/db_update_faq.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_faq.asp">Update FAQs</a></li>
                                            <li<% if arrayVal("/admin/topProducts.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/topProducts.asp">Top Phone Models</a></li>
                                            <li<% if arrayVal("/admin/productUrlMatch.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/productUrlMatch.asp">Product Url Match</a></li>
                                            <li<% if arrayVal("/admin/amazonProducts.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/amazonProducts.asp">Update Amazon Products</a></li>
                                            <% end if %>
											<% if session("marketingLvl") > 2 then %>
                                            <li<% if arrayVal("/admin/db_update_cancel_TEST_ORDERS.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_update_cancel_TEST_ORDERS.asp">Cancel TEST ORDERS</a></li>
                                            <% end if %>
                                            <% if session("marketingLvl") > 3 then %>
                                            <li<% if arrayVal("/admin/createCoupon.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/createCoupon.asp">One Time Coupons</a></li>
                                            <li<% if arrayVal("/admin/manPixels.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/manPixels.asp">Pixel Manipulation</a></li>
                                            <li<% if arrayVal("/admin/mvt.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/mvt.asp">In-House MVT</a></li>
                                            <li<% if arrayVal("/admin/adwords_editor.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/adwords_editor.asp">Adwords Editor</a></li>
                                            <li<% if arrayVal("/admin/PPC.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/PPC.asp">PPC</a></li>
                                            <li<% if arrayVal("/admin/customCaseContest.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/customCaseContest.asp">Custom Case Contest</a></li>
                                            <li<% if arrayVal("/admin/sysconfig.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/sysconfig.asp">Sites Configuration</a></li>
                                            <li<% if arrayVal("/admin/emailLists.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/emailLists.asp">Email List Management</a></li>
                                            <% end if %>
                                        </ol>
                                    </div>
                                    <% end if %>
								</td>
							</tr>
                            <tr>
                                <td bgcolor="#E95516" height="2" style="font-size:1px;"><img src="/images/spacer.gif" width="2" height="2" border="0"></td>
                            </tr>
                            <tr>
                                <td width="350" valign="top" class="normalText">
                                    <p>
                                        <strong><br>Drop-Shipper:</strong>
                                        <ul class="smlText" style="margin-top:2px;">
                                            <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>>Upload Tracking Number:</li>
                                            <ol class="smlText" style="margin-top:2px;">
                                                <li<% if arrayVal("/admin/upload_dropship_tracking.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/upload_dropship_tracking.asp">Upload Tracking Number</a></li>
                                            </ol>
                                            <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>>Cellphone-Mate:</li>
                                            <ol class="smlText" style="margin-top:2px;">
                                                <li<% if arrayVal("/admin/CellphoneMate_transmit_orders.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/CellphoneMate_transmit_orders.asp">Transmit Cellphone-Mate Orders</a></li>
                                            </ol>
                                            <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>>Decalskin:</li>
                                            <ol class="smlText" style="margin-top:2px;">
                                                <li<% if arrayVal("/admin/Decalskin_transmit_orders.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/Decalskin_transmit_orders.asp">Transmit Decalskin Orders</a></li>
                                                <li<% if arrayVal("/admin/Decalskin_upload_items.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/Decalskin_upload_items.asp">Upload Decalskin Items</a></li>
                                                <!--<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/Decalskin_upload_images.asp">Upload Decalskin Images</a></li>-->
                                                <li<% if arrayVal("/admin/Decalskin_upload_images_batch.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/Decalskin_upload_images_batch.asp">Upload Decalskin Images - Batch</a></li>
                                            </ol>
                                            <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>>Mobile Line:</li>
                                            <ol class="smlText" style="margin-top:2px;">
                                                <li<% if arrayVal("/admin/MobileLine_transmit_orders.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/MobileLine_transmit_orders.asp">Transmit Mobile Line Orders</a></li>
                                                <li<% if arrayVal("/admin/mobileLineXLS.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/mobileLineXLS.asp">Add/Update Mobile Line Products</a></li>
                                                <li<% if arrayVal("/admin/mobileLinePriceAdjust.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/mobileLinePriceAdjust.asp">Mobile Line Price Adjust</a></li>
                                            </ol>
                                            <!--<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>>MobileLine (OLD):</li>
                                            <ol class="smlText" style="margin-top:2px;">
                                                <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/MobileLine_UPDATE_items.asp">UPDATE MobileLine Items</a></li>
                                                <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/MobileLine_upload_items.asp">Upload MobileLine Items</a></li>
                                                <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/MobileLine_upload_images_batch.asp">Upload MobileLine Images - Batch</a></li>
                                            </ol>-->
                                            <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>>MusicSkins:</li>
                                            <ol class="smlText" style="margin-top:2px;">
                                            	<li<% if arrayVal("/admin/musicSkins_Transmit.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/musicSkins_Transmit.asp">Music Skins Transmit Orders</a></li>
                                                <li<% if arrayVal("/admin/musicSkins_assign.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/musicSkins_assign.asp">Assign MusicSkin Products</a></li>
                                                <li<% if arrayVal("/admin/musicSkins_upload.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/musicSkins_upload.asp">Create New MusicSkin Products</a></li>
                                            </ol>
                                            <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>>Hypercel:</li>
                                            <ol class="smlText" style="margin-top:2px;">
                                            	<li<% if arrayVal("/admin/hypercel_Transmit.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/hypercel_Transmit.asp">Transmit Orders</a></li>
                                                <li<% if arrayVal("/admin/hypercellDownload.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/hypercellDownload.asp">Download/Update Products</a></li>
                                                <li<% if arrayVal("/admin/hypercellAssign.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/hypercellAssign.asp">Assign Products</a></li>
                                            </ol>
                                            <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>>Simplexity:</li>
                                            <ol class="smlText" style="margin-top:2px;">
                                            	<li<% if arrayVal("/admin/simplexityXML.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/simplexityXML.asp">Get New XML</a></li>
                                                <li<% if arrayVal("/admin/simplexityAssign.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/simplexityAssign.asp">Assign Products</a></li>
                                            </ol>
                                        </ul>
                                    </p>
                                </td>
                            </tr>
						</table>
					</td>
					<td width="2" bgcolor="#E95516" style="font-size:1px;"><img src="/images/spacer.gif" width="2" height="2" border="0"></td>
					<td width="55%" valign="top" style="padding-left:10px;">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr><td width="350" valign="top" class="normalText"><strong><br>Reports:</strong></td></tr>
                            <tr>
                            	<td width="350" valign="top">
                                	<%
									selectedTab = "FF0"
									%>
                                    <div style="float:left; background-color:#333; cursor:pointer; font-size:12px; font-weight:bold; border-right:1px solid #fff; padding:0px 5px 0px 5px; color:#<%=selectedTab%>;" onclick="reportMenuTab('sales')" id="salesTab">Sales</div>
									<%
									selectedTab = "FFF"
									%>
                                    <div style="float:left; background-color:#333; cursor:pointer; font-size:12px; font-weight:bold; border-right:1px solid #fff; padding:0px 5px 0px 5px; color:#<%=selectedTab%>;" onclick="reportMenuTab('inventory')" id="inventoryTab">Inventory</div>
									<div style="float:left; background-color:#333; cursor:pointer; font-size:12px; font-weight:bold; padding:0px 5px 0px 5px; color:#<%=selectedTab%>;" onclick="reportMenuTab('other')" id="otherTab">Other</div>
                                </td>
                            </tr>
                            <tr>
								<td width="400" valign="top" class="normalText">
									<div id="sales">
                                    	<ol class="smlText" style="margin-top:2px;">
                                            <li<% if arrayVal("/admin/salesReportTotals.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/salesReportTotals.asp">Sales Report Totals (all sites)</a></li>
                                            <li<% if arrayVal("/admin/report_Sales.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Sales.asp">Sales Report</a></li>
                                            <li<% if arrayVal("/admin/report_Sales2.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Sales2.asp">Sales Report 2</a></li>
                                            <li<% if arrayVal("/admin/report_ItemSales.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_ItemSales.asp">Item Sales Report</a></li>
                                            <li<% if arrayVal("/admin/report_ItemSales_Top10.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_ItemSales_Top10.asp">Top 10 Item Sales Report</a></li>
                                            <li<% if arrayVal("/admin/db_search_brands.asp?searchID=11",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/db_search_brands.asp?searchID=11">Top Item Sales Report By Model</a></li>
                                            <li<% if arrayVal("/admin/report_OrdersStats.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_OrdersStats.asp">Daily Order Statistics</a></li>
                                            <li<% if arrayVal("/admin/report_OrdersStats_ALL.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_OrdersStats_ALL.asp">Daily Order Statistics for ALL Sites</a></li>
                                            <li<% if arrayVal("/admin/report_Questionable_Orders.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Questionable_Orders.asp">Questionable Orders Report</a></li>
                                            <li<% if arrayVal("/admin/report_Whales.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Whales.asp">Large Order Report</a></li>
                                            <li<% if arrayVal("/admin/report_SalesTax.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_SalesTax.asp">Sales Tax Report</a></li>
                                            <li<% if arrayVal("/admin/report_Orders.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Orders.asp">Orders Report</a></li>
                                            <li<% if arrayVal("/admin/report_eBillme_WE_orders.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_eBillme_WE_orders.asp">WE Orders CSV Report for eBillme</a></li>
                                            <li<% if arrayVal("/admin/report_PartSales.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_PartSales.asp">Non-Processed Sales by Part Number Report</a></li>
                                            <li<% if arrayVal("/admin/report_ItemLastPurchasedDate.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_ItemLastPurchasedDate.asp">Report the Last Date an Item Was Purchased</a></li>
                                            <li<% if arrayVal("/admin/report_Refunds.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Refunds.asp">Refunds Report</a></li>
											<li<% if arrayVal("/admin/report_Unprocessed_Orders.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Unprocessed_Orders.asp">Unprocessed Orders Report</a></li>
                                            <li<% if arrayVal("/admin/report_Cart_Abandonments.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Cart_Abandonments.asp">Cart Abandonment Report</a></li>
                                            <li<% if arrayVal("/admin/report_reship.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_reship.asp">Reship History</a></li>
                                            <li<% if arrayVal("/admin/customerEarnReport.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/customerEarnReport.asp">Customer Earn Report</a></li>
                                            <li<% if arrayVal("/admin/report_Tracking.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Tracking.asp">Checkout Tracking</a></li>
                                            <li<% if arrayVal("/admin/detailedOrderCnt.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/detailedOrderCnt.asp">Daily Order Count</a></li>
                                            <li<% if arrayVal("/admin/salesByAge.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/salesByAge.asp">Sales By Model Year</a></li>
                                            <li<% if arrayVal("/admin/repeatCustomerReport",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/repeatCustomerReport">Repeat Customer Report</a></li>
                                            <li<% if arrayVal("/admin/DailyOrderStatisticsRange",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/DailyOrderStatisticsRange">Daily Order Statistics Range</a></li>
                                            <li<% if arrayVal("/admin/report_outOfStock.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_outOfStock.asp">Out of Stock Report</a></li>
                                            <li<% if arrayVal("/admin/topPhones.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/topPhones.asp">Top Selling Phones</a></li>
                                            <li<% if arrayVal("/admin/topModels.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/topModels.asp">Top Selling Models (Accessories)</a></li>
                                            <li<% if arrayVal("/admin/soldOnSite.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/soldOnSite.asp">PS Sales Breakdown</a></li>
                                            <li<% if arrayVal("/admin/advancedSalesReport.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/advancedSalesReport.asp">Advanced Sales Report</a></li>
                                            <li<% if arrayVal("/admin/activeSales.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/activeSales.asp">Active Sales Report</a></li>
                                            <li<% if arrayVal("/admin/mobileOrders.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/mobileOrders.asp">Mobile Sales</a></li>
                                        </ol>
                                    </div>
                                    <div id="inventory" style="display:none;">
                                    	<ol class="smlText" style="margin-top:2px;">
											<li<% if arrayVal("/admin/report_Inventory.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Inventory.asp">Inventory Report</a></li>
                                            <li<% if arrayVal("/admin/report_Inventory_Brand_Model.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Inventory_Brand_Model.asp">Generate Inventory Report by Brand/Type/Model</a></li>
                                            <li<% if arrayVal("/admin/report_new_item_images.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_new_item_images.asp">New Item Image Report</a></li>
                                            <li<% if arrayVal("/admin/report_newItems.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_newItems.asp">New Items Report</a></li>
                                            <li<% if arrayVal("/admin/report_Amazon.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Amazon.asp">Amazon Inventory/COGS Report</a></li>
                                            <li<% if arrayVal("/admin/faceplatePurchaseReport.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/faceplatePurchaseReport.asp">Product Purchase Report</a></li>
                                            <li<% if arrayVal("/admin/report_amazon_cogs.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_amazon_cogs.asp">Amazon Cogs Report</a></li>
                                            <li<% if arrayVal("/admin/TotalCogs",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/TotalCogs">Total COGS</a></li>
                                            <li<% if arrayVal("/admin/bunkerItems.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/bunkerItems.asp">Bunker Items</a></li>
                                            <li<% if arrayVal("/admin/report_rmaUpdate.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_rmaUpdate.asp">RMA Return Report</a></li>
                                            <li<% if arrayVal("/admin/nonSellingItems.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/nonSellingItems.asp">Non Selling Items</a></li>
                                        </ol>
                                    </div>
                                    <div id="other" style="display:none;">
                                    	<ol class="smlText" style="margin-top:2px;">
											<li<% if arrayVal("/admin/report_CouponStats.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_CouponStats.asp">Coupon Statistics</a></li>
                                            <li<% if arrayVal("/admin/report_Email_Addresses_2.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Email_Addresses_2.asp">E-Mail Address Export</a></li>
                                            <li<% if arrayVal("/admin/report_email_opt_outs.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_email_opt_outs.asp">E-Mail Opt-Out Report</a></li>
                                            <li<% if arrayVal("/admin/report_email_opt_ins.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_email_opt_ins.asp">E-Mail Opt-In Report</a></li>
                                            <li<% if arrayVal("/admin/report_ebillme_promos.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_ebillme_promos.asp">eBillme Promo Code Usage Report</a></li>
                                            <li<% if arrayVal("/admin/report_Image_Names.asp.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Image_Names.asp.asp">Find Image File Names by Part Number</a></li>
                                            <li<% if arrayVal("/admin/report_HandsfreeTypes.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_HandsfreeTypes.asp">Find Items by Handsfree Type</a></li>
                                            <li<% if arrayVal("/admin/report_models_handsfree_types.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_models_handsfree_types.asp">Look Up Handsfree Types for a Specific Model</a></li>
                                            <li<% if arrayVal("/admin/report_UPS_addl_addrs.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_UPS_addl_addrs.asp">WE UPS Orders w/Alternate Addresses To Be Shipped</a></li>
                                            <li<% if arrayVal("/admin/report_tempTXT.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_tempTXT.asp">Uploads to /Admin/tempTXT Directory</a></li>
                                            <li<% if arrayVal("/admin/report_PhoneOrders.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_PhoneOrders.asp">WE Phone Orders</a></li>
                                            <li<% if arrayVal("/admin/report_PhoneOrders2.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_PhoneOrders2.asp">WE Phone Orders2</a></li>
                                            <li<% if arrayVal("/admin/report_UPScost.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_UPScost.asp">UPS Shipping Fee Report</a></li>
                                            <li<% if arrayVal("/admin/report_noConfirmSent.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_noConfirmSent.asp">Orders with No ConfirmSent</a></li>
                                            <li<% if arrayVal("/admin/report_backordered_items.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_backordered_items.asp">Back-Ordered Items Report</a></li>
                                            <li<% if arrayVal("/admin/report_Phones.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Phones.asp">Phone Report</a></li>
											<li<% if arrayVal("/admin/report_Models.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Models.asp">Models Report</a></li>
                                            <li<% if arrayVal("/admin/report_Buy_Tracking.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Buy_Tracking.asp">Buy.com Tracking Number Report</a></li>
											<li<% if arrayVal("/admin/report_Amazon_Tracking.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_Amazon_Tracking.asp">Amazon Tracking Number Report</a></li>
                                            <li<% if arrayVal("/admin/report_COGS.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/report_COGS.asp">COGS Report</a></li>
                                            <li<% if arrayVal("/admin/vendorCodes.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/vendorCodes.asp">Current Vendor Codes</a></li>
                                            <li<% if arrayVal("/admin/siteArchitecture.asp",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/siteArchitecture.asp">Site Architecture</a></li>
                                        </ol>
                                    </div>
								</td>
							</tr>
							<tr>
								<td bgcolor="#E95516" height="2" style="font-size:1px;"><img src="/images/spacer.gif" width="2" height="2" border="0"></td>
							</tr>
							<tr>
								<td width="400" valign="top" class="normalText">
									<blockquote>
										<p>
											<strong><br>Product Lists:</strong>
											<ol class="smlText" style="margin-top:2px;">
											<!--
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_csv_Amazon.asp">Amazon (qty only)</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_Amazon.asp">Amazon (full list)</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_Amazon_Ads.asp">Amazon Ads</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_AtomicMall.asp">Atomic Mall</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_Become.asp">Become</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_Bing.asp">Bing</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_Buy.asp">Buy.com</a></li>
												<%if thisUser = "YRL_tony" or thisUser = "rosemariep" or thisUser = "kristenr" or thisUser = "jnord" or thisUser = "armandol" or thisUser = "mariog" or thisUser = "terryk" then%>
													<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_Buy_INV.asp">Buy.com Inventory</a></li>
												<%end if%>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_CJ.asp">Commission Junction</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_csv_Fetchback.asp">Fetchback</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_Googlebase_WE.asp">Googlebase</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_nextag.asp">NexTag</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_nextag_store.asp">NexTag Store</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_Pepperjam.asp">Pepperjam</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_pricegrabber.asp">Price Grabber</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_PriceManager.asp">Price Manager</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_csv_pricerunner.asp">Pricerunner</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_pronto.asp">Pronto</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_shop.asp">Shop.com</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_csv_shopping.asp">Shopping.com</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_Shopzilla.asp">Shopzilla</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_Smarter.asp">Smarter</a></li>
												<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_generic_WE.asp">Generic WE Product List</a></li>
                                                <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_tab_Treepodia.asp">Treepodia Product List</a></li>
                                                <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_csv_sears.asp">Sears Product List</a></li>
											-->	
                                                <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/datafeeds.asp">Links to the DataFeeds</a></li>
                                                <li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/amazonInventory.asp">Amazon Inventory</a></li>
												<!--<li<% if arrayVal("",linkArray) = -1 then %> class="linkVanishing"<% end if %>><a onclick="cookieLink(this.href)" href="/admin/product_csv_Amazon.asp">WE - Amazon (qty only)</a></li>-->
											</ol>
										</p>
									</blockquote>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#CCCCCC">
		<td valign="middle" bgcolor="#CCCCCC">&nbsp;</td>
	</tr>
</table>
</form>
<%end if%>
<!--#include virtual="/includes/admin/admin_bottom.asp"-->
<script language="javascript">	
	function menuTab(which) {
		<% if session("productLvl") > 0 then %>
		document.getElementById("products").style.display = 'none'
		document.getElementById("productsTab").style.color = '#fff'
		<%
		end if
		if session("orderLvl") > 0 then
		%>
		document.getElementById("orders").style.display = 'none'
		document.getElementById("ordersTab").style.color = '#fff'
		<%
		end if
		if session("marketingLvl") > 0 then
		%>
		document.getElementById("marketing").style.display = 'none'
		document.getElementById("marketingTab").style.color = '#fff'
		<% end if %>
		document.getElementById(which).style.display = ''
		document.getElementById(which + "Tab").style.color = '#FF0'
	}
	function reportMenuTab(which) {
		document.getElementById("sales").style.display = 'none'
		document.getElementById("salesTab").style.color = '#fff'
		document.getElementById("inventory").style.display = 'none'
		document.getElementById("inventoryTab").style.color = '#fff'
		document.getElementById("other").style.display = 'none'
		document.getElementById("otherTab").style.color = '#fff'
		document.getElementById(which).style.display = ''
		document.getElementById(which + "Tab").style.color = '#FF0'
	}
	
	function cookieLink(hrefVal) {
		hrefVal = hrefVal.replace("http://staging.wirelessemporium.com","");
		hrefVal = hrefVal.replace("http://www.wirelessemporium.com","");
		
		var myCookies = document.cookie;
		if (myCookies != "") {
			var cookieArray = myCookies.split(";");
			var matchFound = 0;
			for (i=0;i<cookieArray.length;i++) {
				cookieName = cookieArray[i].substring(cookieArray[i],cookieArray[i].indexOf("="));
				if (cookieName == "myLinks") {
					fullLinkString = cookieArray[i].substring(cookieArray[i].indexOf("=")+1);
					if (fullLinkString != hrefVal) {
						if (fullLinkString == "") {
							document.cookie = "myLinks=" + hrefVal
						}
						else {
							document.cookie = "myLinks=" + fullLinkString + "," + hrefVal
						}
					}
				}
			}
		}
		else {
			document.cookie = "myLinks=" + hrefVal;
		}
	}
</script>