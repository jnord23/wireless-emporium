<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
response.buffer = true
'response.write request.QueryString & "<br>"
set fso = CreateObject("Scripting.FileSystemObject")

dim pageName : pageName = "category-brand"
dim brandid, categoryid, phoneOnly
brandid = request.querystring("brandid")
if brandid = "" or not isNumeric(brandid) then
	call PermanentRedirect("/?ec=102")
end if
response.Cookies("saveBrandID") = brandID
response.Cookies("saveModelID") = 0
response.Cookies("saveItemID") = 0
categoryid = request.querystring("categoryid")
if categoryid = "" or not isNumeric(categoryid) then
	call PermanentRedirect("/?ec=203")
end if
cbModel = ds(request("cbModel"))
cbSort = ds(request("cbSort"))
if not isnull(session("qSort")) then
	qSort = session("qSort")
	session("qSort") = null
end if
isUniversal = false

stitchImgPath = "/images/stitch/catBrands/"
	
if instr(Request.ServerVariables("HTTP_X_REWRITE_URL"), "apple-iphone") > 0 then
	phoneOnly = 1
	'response.Status = "301 Moved Permanently"
	'response.AddHeader "Location", "/index.asp"
	'response.write Request.ServerVariables("HTTP_X_REWRITE_URL")	
else
	phoneOnly = 0
	'response.Status = "301 Moved Permanently"
	'response.AddHeader "Location", "/index.asp"
	'response.write Request.ServerVariables("HTTP_X_REWRITE_URL")	
end if

leftGoogleAd = 1
googleAds = 1

if categoryid = 20 then categoryid = 1270 end if

call fOpenConn()
if categoryid > 999 then
	sql = "SELECT subTypeName typename, subtypeid, typeid, hasUniversal from v_subTypeMatrix WHERE subtypeid = '" & categoryid & "'"
else
	sql = "SELECT typename, typeid, subtypeid, hasUniversal from v_subTypeMatrix WHERE typeid = '" & categoryid & "'"
end if	
'session("errorSQL") = SQL
strSubTypeID = ""
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/?ec=405")
else
	categoryName = RS("typeName")
	parentTypeID = RS("typeid")
	isUniversal = RS("hasUniversal")
	do until RS.eof
		strSubTypeID = strSubTypeID & RS("subtypeid") & ","
		RS.movenext
	loop
	strSubTypeID = left(strSubTypeID, len(strSubTypeID) - 1)
end if
if strSubTypeID = "" then strSubTypeID = "9999999" end if

showIntPhones = request.Form("showIntPhones")
if isnull(showIntPhones) or len(showIntPhones) < 1 then showIntPhones = 0 end if
if isnull(cbSort) or len(cbSort) < 1 then
	noSort = 1
	cbSort = "AZ"
end if
if categoryID = 3 and brandID = 17 then 'is iPhone
	'cbSort = "NO" 'This causes errors with GetRows in Static.asp... 
	cbSort = "ZA" '"ZA" gets the job done just fine for now
end if
if isnull(qSort) or len(qSort) < 1 then qSort = ""
if qSort <> "" then cbSort = qSort
sqlOrder = " order by topModel desc, international, modelname"


cellPhoneTitle = "Cell Phone"		
strSqlAnd = ""
if brandID = 17 and phoneOnly = 1 then
	cellPhoneTitle = "iPhone"
	strSqlAnd = " and modelName not like '%iPod%' and modelName not like '%iPad%'" & vbcrlf
elseif brandID = 17 and phoneOnly = 0 then
	cellPhoneTitle = "iPod / iPad"
	phoneTitle = "Product"
	strSqlAnd = " and modelName not like '%iPhone%'" & vbcrlf
end if


if isUniversal then
	sql	=	"select	distinct '" & categoryName & "' catName" & vbcrlf & _
			"	, 	a.modelid, a.modelname, a.modelimg, a.topModel, a.oldModel, a.international, b.brandname, b.brandid, b.brandimg " & vbcrlf & _
			"	,	cast(case when a.topModel <> 1 and a.oldModel <> 1 and a.international <> 1 then 1 else 0 end as bit) displayModel" & vbcrlf & _
			"from 	we_models a with (nolock) join we_brands b with (nolock) " & vbcrlf & _
			"	on 	a.brandid = b.brandid join we_items c with (nolock)" & vbcrlf & _
			"	on	a.modelid = c.modelid " & vbcrlf & _
			"where	a.hideLive = 0 and a.modelname is not null and c.hidelive = 0 and c.price_our > 0 and c.inv_qty <> 0 and a.brandid = '" & brandid & "' " & vbcrlf & _
			"	and a.modelname not like 'all model%'" & vbcrlf & _
			"	and b.brandType < 2" & vbcrlf & _
			"	and b.brandid not in (12, 8)" & vbcrlf & _
			strSqlAnd
else 
	if parentTypeID = 5 then 'handsfree
		sql	=	"select	distinct '" & categoryName & "' catName" & vbcrlf & _
				"	, 	x.modelid, x.modelname, x.modelimg, x.topModel, x.oldModel, x.international, x.brandname, x.brandid, x.brandimg, x.displayModel" & vbcrlf & _
				"from	(	select	b.modelid, b.modelname, b.modelimg, b.topModel, b.oldModel, b.international, a.brandname, a.brandid, a.brandimg" & vbcrlf & _
				"				,	cast(case when b.topModel <> 1 and b.oldModel <> 1 and b.international <> 1 then 1 else 0 end as bit) displayModel" & vbcrlf & _
				"				,	'''' + replace(handsfreetypes, ',', ''',''') + '''' handsfreetypes" & vbcrlf & _
				"			from	we_brands a join we_models b" & vbcrlf & _
				"				on	a.brandid = b.brandid" & vbcrlf & _
				"			where	b.hideLive = 0 and a.brandtype < 2 and a.brandid not in (12, 8)" & vbcrlf & _
				"				and	b.handsfreetypes is not null	" & vbcrlf & _
				"				and	a.brandid = '" & brandid & "'	) x join " & vbcrlf & _
				"		(	select	distinct '''' + convert(varchar(10), handsfreetype) + '''' handsfreetype" & vbcrlf & _
				"			from	we_items" & vbcrlf & _
				"			where	hidelive = 0 and inv_qty <> 0" & vbcrlf & _
				"				and	subtypeid in (" & strSubTypeID & ") ) y" & vbcrlf & _
				"	on	x.handsfreetypes like '%' + y.handsfreetype + ',%'" & vbcrlf & _
				strSqlAnd
	elseif categoryid = 1270 then 'music skins
		sql	=	"select	distinct '" & categoryName & "' catName" & vbcrlf & _
				"	, 	a.modelid, a.modelname, a.modelimg, a.topModel, a.oldModel, a.international, b.brandname, b.brandid, b.brandimg " & vbcrlf & _
				"	,	cast(case when a.topModel <> 1 and a.oldModel <> 1 and a.international <> 1 then 1 else 0 end as bit) displayModel" & vbcrlf & _
				"from 	we_models a with (nolock) join we_brands b with (nolock) " & vbcrlf & _
				"	on 	a.brandid = b.brandid join we_items_musicskins c with (nolock)" & vbcrlf & _
				"	on	a.modelid = c.modelid " & vbcrlf & _
				"where	a.hideLive = 0 and a.modelname is not null and a.brandid = '" & brandid & "' " & vbcrlf & _
				"	and a.modelname not like 'all model%'" & vbcrlf & _
				"	and b.brandType < 2" & vbcrlf & _
				"	and b.brandid not in (12, 8)" & vbcrlf & _
				"	and	c.skip = 0 " & vbcrlf & _
				"	and c.deleteItem = 0 " & vbcrlf & _
				"	and (c.artist <> '' and c.artist is not null) " & vbcrlf & _
				"	and (c.designname <> '' and c.designname is not null) " & vbcrlf & _
				strSqlAnd
	else
		sql	=	"select	distinct '" & categoryName & "' catName" & vbcrlf & _
				"	, 	a.modelid, a.modelname, a.modelimg, a.topModel, a.oldModel, a.international, b.brandname, b.brandid, b.brandimg " & vbcrlf & _
				"	,	cast(case when a.topModel <> 1 and a.oldModel <> 1 and a.international <> 1 then 1 else 0 end as bit) displayModel" & vbcrlf & _
				"from 	we_models a with (nolock) join we_brands b with (nolock) " & vbcrlf & _
				"	on 	a.brandid = b.brandid join we_items c with (nolock)" & vbcrlf & _
				"	on	a.modelid = c.modelid " & vbcrlf & _
				"where	a.hideLive = 0 and a.modelname is not null and c.hidelive = 0 and c.price_our > 0 and c.inv_qty <> 0 and c.ghost = 0 and a.brandid = '" & brandid & "' " & vbcrlf & _
				"	and a.modelname not like 'all model%'" & vbcrlf & _
				"	and b.brandid not in (12, 8)" & vbcrlf & _
				"	and b.brandType < 2" & vbcrlf & _
				"	and	c.subtypeid in (" & strSubTypeID & ")" & vbcrlf & _
				strSqlAnd & vbcrlf & _
				"union" & vbcrlf & _
				"select	distinct '" & categoryName & "' catName" & vbcrlf & _
				"	, 	a.modelid, a.modelname, a.modelimg, a.topModel, a.oldModel, a.international, b.brandname, b.brandid, b.brandimg " & vbcrlf & _
				"	,	cast(case when a.topModel <> 1 and a.oldModel <> 1 and a.international <> 1 then 1 else 0 end as bit) displayModel" & vbcrlf & _
				"from 	we_subRelatedItems t join we_models a with (nolock) " & vbcrlf & _
				"	on	t.modelid = a.modelid join we_brands b with (nolock) " & vbcrlf & _
				"	on 	t.brandid = b.brandid join we_items c with (nolock)" & vbcrlf & _
				"	on	t.itemid = c.itemid " & vbcrlf & _
				"where	a.hideLive = 0 and a.modelname is not null and c.hidelive = 0 and c.price_our > 0 and c.inv_qty <> 0 and c.ghost = 0 and t.brandid = '" & brandid & "' " & vbcrlf & _
				"	and a.modelname not like 'all model%'" & vbcrlf & _
				"	and b.brandid not in (12, 8)" & vbcrlf & _
				"	and b.brandType < 2" & vbcrlf & _
				"	and	t.subtypeid in (" & strSubTypeID & ")" & vbcrlf & _				
				strSqlAnd
	end if
end if

if cbSort <> "" then
	if cbSort = "AZ" and noSort = 0 then
		fileExt = fileExt & "_" & cbSort & "S"
	else
		fileExt = fileExt & "_" & cbSort
	end if
	'fileExt = fileExt & "_" & cbSort
	if noSort = 0 then showPopPhones = 0
	select case ucase(cbSort)
		case "AZ"
			if noSort = 1 then
				sqlOrder = " order by topModel desc, oldModel, international, modelname"
			else
				sqlOrder = " order by oldModel, international, modelname"
			end if
		case "ZA"
			sqlOrder = " order by oldModel, international, modelname desc"	
		case "NO"
			sqlOrder = " order by oldModel, international, releaseyear desc, releasequarter desc, modelname"		
		case "ON"
			sqlOrder = " order by oldModel, international, releaseyear, releasequarter, modelname"
	end select
end if

sql	= sql & sqlOrder
session("errorSQL") = SQL
'response.write "<pre>" & sql & "</pre>"
'response.end
arrRS = getDbRows(sql)
if isnull(arrRS) then response.redirect("/") end if

if brandID = 17 and phoneOnly = 1 then 'no popphone for iphones
	arrPopPhones = null
	arrOldPhones = filter2DRS(arrRS, 5, true)	
	arrIntPhones = filter2DRS(arrRS, 6, true)
	arrDisplayPhones = arrRS
	arrSortedRS = sort2DRS(arrRS, 2)	
else
	arrPopPhones = filter2DRS(arrRS, 4, true)	
	arrOldPhones = filter2DRS(arrRS, 5, true)	
	arrIntPhones = filter2DRS(arrRS, 6, true)
	arrDisplayPhones = filter2DRS(arrRS, 10, true)
	arrSortedRS = sort2DRS(arrRS, 2)
end if

if not isnull(arrPopPhones) then if ubound(arrPopPhones,2) >= 0 then showPopPhones = 1 else showPopPhones = 0 end if end if
if not isnull(arrOldPhones) then if ubound(arrOldPhones,2) >= 0 then showOldPhones = 1 else showOldPhones = 0 end if end if
if not isnull(arrIntPhones) then if ubound(arrIntPhones,2) >= 0 then showIntPhones = 1 else showIntPhones = 0 end if end if
if not isnull(arrDisplayPhones) then if ubound(arrDisplayPhones,2) >= 0 then showRegularPhones = 1 else showRegularPhones = 0 end if end if

if brandName = "" then brandName = arrRS(7,0) end if

phoneList = ""
if not isnull(arrSortedRS) then
	for i=0 to ubound(arrSortedRS,2)
		if phoneList = "" then
			phoneList = """" & arrSortedRS(2,i) & " " & nameSEO(categoryName) & """"
		else
			phoneList = phoneList & ", """ & arrSortedRS(2,i) & " " & nameSEO(categoryName) & """"
		end if
	next
end if

'=========================================================================================
'Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
'	oParam.CompareMode = vbTextCompare
'	oParam.Add "x_brandID", brandID
'	oParam.Add "x_categoryID", categoryID
'	if brandid = 17 and phoneOnly = 1 then
'		oParam.Add "x_brandName", "apple-iphone"
'	elseif brandid = 17 and phoneOnly = 0 then
'		oParam.Add "x_brandName", "apple-ipod-ipad"
'	else
'		oParam.Add "x_brandName", brandName	
'	end if
'	oParam.Add "x_categoryName", categoryName	
'call redirectURL("cb", "", request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
'=========================================================================================
sql	= "select * from we_carriers order by listOrder, carriername"
session("errorSQL") = sql
set objRsCarrier = Server.CreateObject("ADODB.Recordset")
objRsCarrier.open sql, oConn, 0, 1

a = 0
maxModels = 999


if showPopPhones = 1 then
	popItemImgPath = formatSEO(brandName) & "_pop_" & categoryid & ".jpg"
end if

if brandID = 17 and phoneOnly = 1 then
	itemImgPath = "applePhones_all" & fileExt & "_" & categoryid & ".jpg"
else
	itemImgPath = formatSEO(brandName) & "_all" & fileExt & "_" & categoryid & ".jpg"
	oldItemImgPath = formatSEO(brandName) & "_old" & fileExt & "_" & categoryid & ".jpg"
	intItemImgPath = formatSEO(brandName) & "_int" & fileExt & "_" & categoryid & ".jpg"
end if

if bStaging then popItemImgPath = "staging_" & popItemImgPath
if bStaging then itemImgPath = "staging_" & itemImgPath
if bStaging then oldItemImgPath = "staging_" & oldItemImgPath
if bStaging then intItemImgPath = "staging_" & intItemImgPath

dim SEtitle, SEdescription, SEkeywords, topText, bottomText, altText
if categoryid = "1" then
	topText = "No matter what model " & brandName & " phone you use, Wireless Emporium is sure to have the largest selection of batteries to keep you powered up. We pride ourselves on giving our shoppers an incredible selection of accessories, which is why we carry OEM batteries, aftermarket batteries, extended life batteries, and even premium batteries in some cases. More than just an accessory, a spare or replacement battery is an essential item to keep your " & brandName & " phone on top of its game."
	bottomText = "Since 2001, Wireless Emporium has been offering high-quality cell phone accessories to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best cellular batteries for " & brandName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
elseif categoryid = "2" then
	if brandid = "14" then
		SEtitle = "BlackBerry Accessories: BlackBerry Cell Phone Chargers | Discount Chargers"
		SEdescription = "Wireless Emporium is the #1 source for factory direct BlackBerry accessories like BlackBerry cell phone chargers at Discounted Rates!! Order Now!!"
		SEkeywords = "Discount Chargers, cheap cell phone Chargers, discount cellular Chargers, mobile phone Chargers, wireless Chargers, cellular telephone Chargers, wholesale cellular Chargers, cellular Chargers, wireless phone Chargers, wholesale cell phone Chargers, cellular phone Chargers, wholesale wireless Chargers, wholesale cellular phone Chargers, discount cell phone Chargers, cool cell phone Chargers"
		bottomText = "Wireless Emporium is the Number one Online Resource for cell phone accessories and the largest seller of cell phone cases, cell phone covers, cell phone chargers, cell phone batteries and other accessories to personalize, optimize and protect your phone."
		altText = "BlackBerry Accessories: BlackBerry Model Chargers"
	else
		bottomText = "Since 2001, Wireless Emporium has been offering high-quality cell phone accessories to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best " & brandName & " cell phone chargers for " & brandName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
	end if
	topText = "Huge selection and low prices is what Wireless Emporium is all about. With the largest selection of " & brandName & " accessories on the web, we are confident in saying that we carry the car charger or travel charger for your " & brandName & " phone that you need to keep your phone powered up and working right. We offer OEM and aftermarket travel chargers. We have heavy duty and retractable cord car chargers- all of which are designed and guaranteed to work with your " & brandName & " phone."
elseif categoryid = "3" then
	bottomText = "Wireless Emporium has more Faceplates and Screen Protectors to choose from for all " & brandName & " phones than anywhere else on the web. Personalize and protect your " & brandName & " phone with accessories for every taste. We pride ourselves on giving you a complete line of quality cell phone accessories for you to accentuate, personalize and protect your mobile phone. And of course, all of our " & brandName & " Faceplates and Screen Protectors come with our ironclad 100% satisfaction guarantee."
elseif categoryid = "5" then
	topText = "Wireless Emporium offers the largest selection and the best prices on all " & brandName & " accessories including the widest array of " & brandName & " phone hands-free devices to accessorize your phone. With " & brandName & " Bluetooth headsets and hands-free cell phone headsets becoming more of a necessity for " & brandName & " cell phone owners, we are constantly updating our catalog of hands-free devices to offer you a full line of affordable and feature-rich headsets. "
	topText = topText & "With our Discount bluetooth headsets starting at under $20 and hands-free headsets starting at under $10, you're bound to find a hands-free solution that works for you and your wallet. We pride ourselves on giving you a complete line of quality cell phone accessories for you to enhance, accentuate and personalize your mobile phone. And of course, all of our " & brandName & " hands-free come with our ironclad 100% satisfaction guarantee."
	bottomText = "Since 2001, Wireless Emporium has been offering high-quality cell phone accessories to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best " & brandName & " Bluetooth headsets and hands-free headsets for " & brandName & " cell phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
elseif categoryid = "6" then
	topText = "Wireless Emporium offers the largest selection and the best prices on all " & brandName & " accessories including the widest array of " & brandName & " phone holsters/belt clips to accessorize your phone. We pride ourselves on giving you a complete line of quality cell phone accessories for you to enhance, accentuate and personalize your mobile phone. "
	topText = topText & "And of course, all of our " & brandName & " holsters/belt clips come with our ironclad 100% satisfaction guarantee. We make it our duty to offer a complete line of " & brandName & " cell phone holsters & belt clips for your " & brandName & " phone. Our " & brandName & " holsters & belt clips are made of durable plastic that has passed rigorous quality tests! Complete with an adjustable belt clip that locks in six different positions, you can clip this " & brandName & " holster to clothes or car visor for easy access to your phone."
	bottomText = "Since 2001, Wireless Emporium has been offering high-quality cell phone accessories to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best " & brandName & " cell phone holsters & belt clips for your " & brandName & " phone at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
elseif categoryid = "7" then
	topText = "Wireless Emporium offers the largest selection and the best prices on all " & brandName & " accessories including the widest array of " & brandName & " phone leather cases to accessorize your phone. We pride ourselves on giving you a complete line of cheap cell phone accessories for you to enhance, accentuate and personalize your mobile phone. "
	topText = topText & "And of course, all of our " & brandName & " leather cases come with our ironclad 100% satisfaction guarantee. " & brandName & " cell phone cases are essential to keeping your " & brandName & " cell phone in good shape. A quality " & brandName & " cell phone case will help keep unwanted damage away and prolong the life of your cell phone. Wireless Emporium offers the largest selection of " & brandName & " cell phone cases at heavily discounted prices because it's what our loyal customers expect from us. We only sell quality cell phone cases that have passed stringent testing and are guaranteed to withstand the rigors of everyday life."
	bottomText = "Since 2001, Wireless Emporium has been offering high-quality cell phone accessories to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best " & brandName & " cases for " & brandName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
elseif categoryid = "20" then
	if brandid = 17 and phoneOnly = 1 then
		SEtitle 		= 	"iPhone Music Skins: Music, TV, Movie Themed Skins for iPhones"
		SEdescription	=	"Buy Premium iPhone Music Skins at WirelessEmporium! We carry a large selection of Music, TV, Movies & other themed skins for all iPhone phones."
		SEkeywords 		= 	"iPhone music skins, music skins, cell phone skins, skins for cell phones, phone skins, mobile phone skins"
		h1				=	"iPhone Music Skins"
		strBreadcrumb	=	"iPhone Music Skins"
	elseif brandid = 17 and phoneOnly = 0 then
		SEtitle 		= 	"iPod / iPad Music Skins: Music, TV, Movie Themed Skins for iPods/iPads"
		SEdescription	=	"Buy Premium iPod / iPad Music Skins at WirelessEmporium! We carry a large selection of Music, TV, Movies & other themed skins for all iPod / iPad phones."
		SEkeywords 		= 	"iPod / iPad music skins, music skins, cell phone skins, skins for cell phones, phone skins, mobile phone skins"
		h1				=	"iPod / iPad Music Skins"	
		strBreadcrumb	=	"iPod / iPad Music Skins"	
	else
		SEtitle 		= 	brandName & " Music Skins: Music, TV, Movie Themed Skins for " & brandName & " Phones"
		SEdescription	=	"Buy Premium " & brandName & " Music Skins at WirelessEmporium! We carry a large selection of Music, TV, Movies & other themed skins for all " & brandName & " phones."
		SEkeywords 		= 	brandName & " music skins, music skins, cell phone skins, skins for cell phones, phone skins, mobile phone skins"
		h1				=	brandName & " Music Skins"	
	end if

	strCatCrumb		=	"Music Skins"	
end if

dim strBreadcrumb

if brandid = 17 and phoneOnly = 1 then
	headerImgID = 17
	if h1 = "" then h1 = brandName & " iPhone " & nameSEO(categoryName)
	if strBreadcrumb = "" then strBreadcrumb = "Cell Phone " & nameSEO(categoryName) & " for " & brandName & " iPhone"
	if strCatCrumb = "" then strCatCrumb = nameSEO(categoryName)	
elseif brandid = 17 and phoneOnly = 0 then
	headerImgID = 17
	if h1 = "" then h1 = brandName & " iPod/iPad " & nameSEO(categoryName)
	if strBreadcrumb = "" then strBreadcrumb = nameSEO(categoryName) & " for " & brandName & " iPod/iPad"
	if strCatCrumb = "" then strCatCrumb = nameSEO(categoryName)
		
	if SEtitle = "" then SEtitle = replace(lcase(SEtitle), "cell phone", "iPod/iPad")
	if SEdescription = "" then SEdescription = replace(lcase(SEdescription), "cell phone", "iPod/iPad")
	if SEkeywords = "" then SEkeywords = replace(lcase(SEkeywords), "cell phone", "iPod/iPad")
	if topText = "" then topText = replace(lcase(topText), "cell phone", "iPod/iPad")
	if altText = "" then altText = replace(lcase(altText), "cell phone", "iPod/iPad")
else
	headerImgID = 20
	if h1 = "" then h1 = brandName & " Cell Phone " & nameSEO(categoryName)
	if strBreadcrumb = "" then strBreadcrumb = "Cell Phone " & nameSEO(categoryName) & " for " & brandName
	if strCatCrumb = "" then strCatCrumb = "Cell Phone " & nameSEO(categoryName)
end if

if SEtitle = "" then SEtitle = "Cell Phone " & nameSEO(categoryName) & ": Discount " & nameSEO(categoryName) & " from Wireless Emporium, cell phone " & nameSEO(categoryName) & ", Mobile phone " & nameSEO(categoryName) & ", Wireless phone " & nameSEO(categoryName)
if SEdescription = "" then SEdescription = "Discount " & nameSEO(categoryName) & " and Wireless Phone " & nameSEO(categoryName) & " at Wireless Emporium. Your source for wholesale cell phone " & nameSEO(categoryName) & ", wholesale cellular " & nameSEO(categoryName) & ", discount cell phone " & nameSEO(categoryName) & " and cheap cell phone " & nameSEO(categoryName)
if SEkeywords = "" then SEkeywords = "Discount " & nameSEO(categoryName) & ", cheap cell phone " & nameSEO(categoryName) & ", discount cellular " & nameSEO(categoryName) & ", mobile phone " & nameSEO(categoryName) & ", wireless " & nameSEO(categoryName) & ", cellular telephone " & nameSEO(categoryName) & ", wholesale cellular " & nameSEO(categoryName) & ", cellular " & nameSEO(categoryName) & ", wireless phone " & nameSEO(categoryName) & ", wholesale cell phone " & nameSEO(categoryName) & ", cellular phone " & nameSEO(categoryName) & ", wholesale wireless " & nameSEO(categoryName) & ", wholesale cellular phone " & nameSEO(categoryName) & ", discount cell phone " & nameSEO(categoryName) & ", cool cell phone " & nameSEO(categoryName)
if topText = "" then topText = "Wireless Emporium offers the largest selection and the best prices on all " & brandName & " accessories including the widest array of " & brandName & " phone " & nameSEO(categoryName) & " to accessorize your phone. We pride ourselves on giving you a complete line of quality cell phone accessories for you to enhance, accentuate and personalize your mobile phone. And of course, all of our " & brandName & " " & nameSEO(categoryName) & " come with our ironclad 100% satisfaction guarantee."
if altText = "" then altText = nameSEO(categoryName) & " for " & brandName & " Model"

'todo: need to consolidate Metatags with we_XXXText
dim strH1: strH1 = h1
dim strH2: strH2 = EMPTY_STRING
dim strAltText: strAltText = EMPTY_STRING

'set input values to extract page meta-values
dim dicReplaceAttribute: set dicReplaceAttribute = CreateObject("Scripting.Dictionary")
dicReplaceAttribute( "BrandName") = brandName
dicReplaceAttribute( "CategoryName") = categoryName

'set known product attributes to replace out template placeholders
dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
dicSeoAttributeInput( "BrandId") = brandID
dicSeoAttributeInput( "CategoryId") = categoryID

'get content event text (initially installed to load pixels) [knguyen/20110520]
'todo: deploy to other wrappers as needed
dim dicContentEventText: set dicContentEventText = GetContentEventText( dicSeoAttributeInput)

call LookupSeoAttributes()


set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
'dim strBrandAccessoryLandingUrl: strBrandAccessoryLandingUrl = EMPTY_STRING
dicSeoAttributeInput( "BrandAccessoryLandingUrl") = brandSEO( brandID)

topText = ApplyPageAttribute( topText, dicSeoAttributeInput)
set dicSeoAttributeInput = nothing
	
if showPopPhones = 1 then
	imgStitchPath = Server.MapPath("/images/stitch/catBrands/" & popItemImgPath)
	call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrPopPhones, 3)	
end if
if showOldPhones = 1 then
	imgStitchPath = Server.MapPath("/images/stitch/catBrands/" & oldItemImgPath)
	call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrOldPhones, 3)		
end if
if showIntPhones = 1 then
	imgStitchPath = Server.MapPath("/images/stitch/catBrands/" & intItemImgPath)
	call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrIntPhones, 3)			
end if
if showRegularPhones = 1 then
	imgStitchPath = Server.MapPath("/images/stitch/catBrands/" & itemImgPath)
	call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrDisplayPhones, 3)
end if
'response.write ubound(arrPopPhones, 2) & "<br>"
'response.write ubound(arrOldPhones, 2) & "<br>"
'response.write ubound(arrIntPhones, 2) & "<br>"
'response.write ubound(arrDisplayPhones, 2) & "<br>"
%>

<!--#include virtual="/includes/template/top.asp"-->
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script>
window.WEDATA.pageType = 'categoryBrand';
window.WEDATA.pageData = {
	brand: '<%= brandName %>',
	brandId: '<%= brandId %>',
	category: '<%=categoryName%>',
	categoryId: '<%= categoryId%>'

};

	$(document).ready(function() {
		$("input#id_Search").autocomplete({source: [<%=phoneList%>]	});
		$("input#id_Search").autocomplete( "option", "position", { my : "right top", at: "right bottom" } );
		$("input#id_Search").bind( "autocompleteselect", function(event, ui) { jumpto(); });
	});
</script>
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/">HOME</a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="/phone-<%=formatSEO(categoryName)%>"><%=strCatCrumb%></a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
        </td>
    </tr>
    <tr>
        <td width="100%" align="center" valign="top">
            <img src="/images/brandmodelheaders/header_<%=brandID%>.jpg" width="748" height="30" border="0" alt="<%=brandName &" "& modelName &" "&categoryName %>">
        </td>
    </tr>
	<tr>
        <td width="100%" align="center" valign="top">
            <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="200" align="left"><img src="/images/cats/we_header_<%=parentTypeID%>.jpg" border="0" alt="<%=strH1%>" title="<%=strH1%>" /></td>                
                	<td width="*" style="padding-left:10px;">
                        <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                            <tr>                    
                            	<td valign="top" align="left">
	                                <h1 class="catBrand"><%=seH1%></h1>
                                </td>
							</tr>
                            <tr>                    
                            	<td valign="top" align="left" class="topText">
									<%
									if prepStr(SeTopText) <> "" then topText = SeTopText
									response.Write(topText)
									%>
                                </td>
							</tr>
						</table>
                    </td>
                </tr>
			</table>
        </td>
    </tr>    
    <tr>
    	<td style="padding-top:15px;">
			<form name="frmSearch" method="post">        
            <div class="graybox-middle" style="width:726px; border-left:1px solid #999; border-right:1px solid #999; padding:0px 10px 0px 10px;">
            	<div style="float:right; padding-top:2px;"><img src="/images/WE-seach-button.jpg" border="0" onclick="jumpto(); return false;" style="cursor:pointer;" /></div>
            	<div style="float:right; padding:8px 10px 0px 10px;"><input type="text" id="id_Search" name="txtSearch" value="Search" onclick="this.value='';" /></div>
                <div class="graybox-inner" title="Choose from the following" align="right">
                    <select name="cbModel" style="float:right; width:400px;" onChange="if(this.value != ''){window.location=this.value;}">
                        <option value="">Choose from the following</option>
                        <%
                        for nRows=0 to ubound(arrSortedRS, 2)
                            tModelid = cint(arrSortedRS(1,nRows))
                            tModelName = arrSortedRS(2,nRows)
                            tBrandid = cint(arrSortedRS(8,nRows))
                            tBrandName = arrRS(7,nRows)
                            tAnchorText = tModelName & " " & nameSEO(categoryName)
                            catLinkFix = "-sc-"
                            if categoryid > 999 then catLinkFix = "-scd-" end if
                            strLink = "/sb-" & tBrandid & "-sm-" & tModelid & catLinkFix & categoryID & "-" & formatSEO(categoryName) & "-" & formatSEO(tBrandName) & "-" & formatSEO(tModelName)

                            if categoryid = 1270 then
                                strLink = "/sb-" & tBrandid & "-sm-" & tModelid & "-sc-" & categoryID & "-music-skins-genre"
                            end if
                            %>
                            <option value="<%=strLink%>"><%=tAnchorText%></option>                                                                                                    
                            <%
                        next
                        %>
                    </select>
                	<span style="font-weight:bold; float:right;">Jump to:&nbsp;&nbsp;</span>
				</div>
            </div>
			</form>            
        </td>
    </tr>
    <%
	if showPopPhones = 1 then
	%>
    <tr>
        <td width="100%" style="padding-top:5px;">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="background:url(/images/brands/gray-header-right.jpg) repeat-x;">
            	<tr>
                	<td style="background:url(/images/brands/gray-header-left.jpg) no-repeat; width:330px; height:30px; color:#fff; font-size:14px; padding:4px 0px 0px 10px;" align="left" valign="top">
                    	TOP <%=ucase(replace(brandName, "/", " / ") & " " & cellPhoneTitle)%> MODELS
					</td>
                </tr>
			</table>
        </td>
	</tr>
    <tr>
        <td width="100%">
			<%=drawStitchModelBoxByArray(arrPopPhones, stitchImgPath & popItemImgPath)%>
        </td>
	</tr>
    <%
	end if
	
	if showRegularPhones = 1 then
	%>
    <tr>
        <td width="100%" style="padding-top:15px;">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="background:url(/images/brands/gray-header-right.jpg) repeat-x;">
            	<tr>
                	<td style="background:url(/images/brands/gray-header-left.jpg) no-repeat; width:330px; height:30px; color:#fff; font-size:14px; padding:4px 0px 0px 10px;" align="left" valign="top">
                    	<% if showPopPhones = 1 and brandid <> 15 then %>
                        OTHER <%=ucase(replace(brandName, "/", " / ") & " " & cellPhoneTitle)%> MODELS
						<% else %>
                        <%=ucase(replace(brandName, "/", " / ") & " " & cellPhoneTitle)%> MODELS
                        <% end if %>
					</td>
                </tr>
			</table>
        </td>
	</tr>
    <tr>
        <td width="100%">
			<%=drawStitchModelBoxByArray(arrDisplayPhones, stitchImgPath & itemImgPath)%>
        </td>
	</tr>
    <%
	end if
	
	if showOldPhones = 1 then
	%>
    <tr>
        <td width="100%" style="padding-top:5px; cursor:pointer;" onclick="toggleModels('olderModels','id_bottom_arrow_old');" title="CLICK TO VIEW MORE <%=ucase(brandName)%> MODELS">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
            	<tr>
                	<td width="10px"><img src="/images/brands/bottom-header-left.jpg" border="0" style="cursor:pointer;" /></a>
					</td>
                    <td style="background:url(/images/brands/bottom-header-center.jpg) repeat-x; padding-top:10px; font-size:18px; font-weight:bold; color:#494949;" align="right" valign="top" width="*">
						CLICK TO VIEW MORE <%=ucase(brandName)%> MODELS 
                    </td>
                    <td style="background:url(/images/brands/bottom-header-center.jpg) repeat-x; width:180px; padding-left:10px;" align="left" valign="top">
                    	<img id="id_bottom_arrow_old" src="/images/brands/down-arrow.jpg" border="0" style="margin-top:10px; cursor:pointer;" />
                    </td>
                    <td width="10px"><img src="/images/brands/bottom-header-right.jpg" border="0" style="cursor:pointer;" /></td>
                </tr>
			</table>
        </td>
	</tr>
    <tr>
        <td width="100%">
			<div id="olderModels" style="display:none;">
				<%=drawStitchModelBoxByArray(arrOldPhones, stitchImgPath & oldItemImgPath)%>
            </div>
        </td>
	</tr>
    <%
	end if

	if showIntPhones = 1 then
	%>
    <tr>
        <td width="100%" style="padding-top:5px; cursor:pointer;" onclick="toggleModels('intModels','id_bottom_arrow_int');" title="CLICK TO VIEW MORE <%=ucase(brandName)%> MODELS">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
            	<tr>
                	<td width="10px"><img src="/images/brands/bottom-header-left.jpg" border="0" style="cursor:pointer;" /></a>
					</td>
                    <td style="background:url(/images/brands/bottom-header-center.jpg) repeat-x; padding-top:11px; font-size:18px; font-weight:bold; color:#494949;" align="right" valign="top" width="*">
						CLICK TO VIEW INTERNATIONAL <%=ucase(brandName)%> MODELS 
                    </td>
                    <td style="background:url(/images/brands/bottom-header-center.jpg) repeat-x; width:140px; padding-left:10px;" align="left" valign="top">
                    	<img id="id_bottom_arrow_int" src="/images/brands/down-arrow.jpg" border="0" style="margin-top:10px; cursor:pointer;" />
                    </td>
                    <td width="10px"><img src="/images/brands/bottom-header-right.jpg" border="0" style="cursor:pointer;" /></td>
                </tr>
			</table>
        </td>
	</tr>
    <tr>
        <td width="100%">
			<div id="intModels" style="display:none;">
				<%=drawStitchModelBoxByArray(arrIntPhones, stitchImgPath & intItemImgPath)%>
            </div>
        </td>
	</tr>
    <%
	end if
	%>                
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
    <tr><td align="center" class="bottomText"><%=bottomText%></td></tr>
    <tr><td align="center" class="bottomText" style="padding:10px 0px 10px 0px;"><%=seBottomText%></td></tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->
<script>
	function jumpto()
	{
		modelname = document.frmSearch.txtSearch.value;
		var x=document.frmSearch.cbModel;
		for (i=0;i<x.length;i++)
			if (x.options[i].text == modelname) window.location = x.options[i].value;
	}
	
	function toggleModels(which, idArrow)
	{
		toggle(which);
		
		if (document.getElementById(which).style.display == '') 
			document.getElementById(idArrow).src = "/images/brands/up-arrow.jpg"
		else 
			document.getElementById(idArrow).src = "/images/brands/down-arrow.jpg"
	}
</script>
<!--
<script language="javascript">
	function chgCarrier() {
		document.topSearchForm.viewPage.value = 1
		document.topSearchForm.submit()
	}
	function chgOrder() {
		document.topSearchForm.viewPage.value = 1
		document.topSearchForm.submit()
	}
	function displayHiddenPhones(which) {
		if (document.getElementById(which).style.display == '') {
			document.getElementById(which).style.display='none'
		}
		else {
			document.getElementById(which).style.display=''
		}
	}
</script>
-->