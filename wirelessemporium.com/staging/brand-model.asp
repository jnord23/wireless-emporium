<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
response.buffer = true
dim pageName : pageName = "brand-model"
dim basePageName : basePageName = "Brand-Model"
dim modelid, brandName, modelName, modelName2, brandModel
modelid = prepInt(request.querystring("modelid"))
if instr(request.QueryString("brandModel"),"--") > 0 then
	brandModel = prepStr(replace(request.QueryString("brandModel"),"--","-"))
else
	brandModel = prepStr(request.QueryString("brandModel"))
end if

if brandModel <> "" then
	curURL = request.ServerVariables("HTTP_X_REWRITE_URL")
	curURL = replace(replace(replace(replace(curURL,"/accessories-for-",""),"-"," "),".asp",""), "'", "''")
	
	useBrandModel = replace(brandModel,"-"," ")
	sql = "select a.modelID from we_models a left join we_Brands b on a.brandID = b.brandID where replace(replace(b.brandName,'-',' '),'/',' ') + ' ' + replace(replace(a.modelName,'-',' '),'/',' ') like '" & useBrandModel & "%'"
	session("errorSQL") = sql
	set modelIdRS = oConn.execute(sql)
	
	if modelIdRS.EOF then
		do while modelIdRS.EOF
			session("errorSQL") = "useBrandModel:" & useBrandModel
			removedWord = replace(mid(useBrandModel,instrrev(useBrandModel," "))," ","")
			useBrandModel = left(useBrandModel,instrrev(useBrandModel," ")-1)
			if removedWord <> "" then
				sql = "select a.modelID from we_models a left join we_Brands b on a.brandID = b.brandID where replace(replace(b.brandName,'-',' '),'/',' ') + ' ' + replace(replace(a.modelName,'-',' '),'/',' ') like '" & useBrandModel & "%' and replace(replace(b.brandName,'-',' '),'/',' ') + ' ' + replace(replace(a.modelName,'-',' '),'/',' ') like '%" & removedWord & "%'"
			else
				sql = "select a.modelID from we_models a left join we_Brands b on a.brandID = b.brandID where replace(replace(b.brandName,'-',' '),'/',' ') + ' ' + replace(replace(a.modelName,'-',' '),'/',' ') like '" & useBrandModel & "%'"
			end if
			session("errorSQL") = sql
			set modelIdRS = oConn.execute(sql)
			
			if instrrev(useBrandModel," ") < 1 then exit do
		loop
		
		if modelIdRS.EOF then
			if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
				response.Write("bounce to home1 (nonMP)")
				response.End()
			else
				PermanentRedirect("/")
			end if
		else
			modelid = modelIdRS("modelID")
			sql = "update we_models set linkName = '" & curURL & "' where modelID = " & modelID
			session("errorSQL") = sql
			oConn.execute(sql)
		end if
	else
		modelid = modelIdRS("modelID")
		sql = "update we_models set linkName = '" & curURL & "' where modelID = " & modelID
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
else
	if modelid = 0 then
		if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
			response.Write("bounce to home2 (nonMP)")
			response.End()
		else
			PermanentRedirect("/")
		end if
	else
		sql = "select brandName, modelName from we_models a left join we_brands b on a.brandID = b.brandID where modelID = " & modelID
		session("errorSQL") = sql
		set modelRS = oConn.execute(sql)
		
		useLink = formatSEO(modelRS("brandName")) & "-" & formatSEO(modelRS("modelName"))
		
		PermanentRedirect("/accessories-for-" & useLink & ".asp")
	end if
end if

saveModelID = modelid

response.Cookies("saveBrandID") = 0
response.Cookies("saveModelID") = modelID
response.Cookies("saveItemID") = 0

leftGoogleAd = 1
googleAds = 1
isTablet = false

dim brandID, modelImg
dim SEtitle, SEdescription, SEkeywords, topText, bottomText

call fOpenConn()

sql	=	"select	distinct a.modelid, a.modelname, a.modelimg, a.temp, c.brandid, c.brandname" & vbcrlf & _
		"	,	a.isTablet, v.typeid, v.typeName, v.orderNum" & vbcrlf & _
		"	,	'sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}.asp' urlStructure	" & vbcrlf & _
		"from	we_models a join we_brands c " & vbcrlf & _
		"	on	a.brandid = c.brandid join we_items b " & vbcrlf & _
		"	on	a.modelid = b.modelid join we_types d " & vbcrlf & _
		"	on	b.typeid = d.typeid join v_subtypeMatrix v" & vbcrlf & _
		"	on	b.subtypeid = v.subtypeid" & vbcrlf & _
		"where	a.modelid = '" & modelid & "' " & vbcrlf & _
		"	and d.typeid not in (5) and b.hidelive = 0 and (b.inv_qty <> 0 or (select alwaysInStock from we_pnDetails where PartNumber = b.partNumber) = 1) and b.price_our > 0 " & vbcrlf & _
		"union " & vbcrlf & _
		"select	distinct a.modelid, a.modelname, a.modelimg, a.temp, c.brandid, c.brandname" & vbcrlf & _
		"	,	a.isTablet, v.typeID, v.typeName, v.orderNum	" & vbcrlf & _
		"	,	'sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}.asp' urlStructure	" & vbcrlf & _
		"from	we_models a join we_brands c" & vbcrlf & _
		"	on	a.brandid = c.brandid join v_typeMatrix v" & vbcrlf & _
		"	on	v.typeID in (5, 8)" & vbcrlf & _
		"where	a.modelid = '" & modelid & "'" & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct a.modelid, a.modelname, a.modelimg, a.temp, c.brandid, c.brandname" & vbcrlf & _
		"	,	a.isTablet, v.typeID, v.typeName, v.orderNum" & vbcrlf & _
		"	,	'sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}.asp' urlStructure	" & vbcrlf & _
		"from	we_models a join we_brands c" & vbcrlf & _
		"	on	a.brandid = c.brandid join we_subrelateditems s" & vbcrlf & _
		"	on	a.modelid = s.modelid join v_typeMatrix v" & vbcrlf & _
		"	on	s.subtypeid = v.origSubTypeId" & vbcrlf & _
		"where	s.modelid = '" & modelid & "'" & vbcrlf & _
		"order by v.orderNum desc" & vbcrlf
'response.write "<pre>" & sql & "</pre>"
session("errorSQL") = SQL
set RS = oConn.execute(sql)

if not RS.eof then
	brandName = RS("brandName")
	brandID = prepInt(RS("brandID"))
	modelName = RS("modelName")
	modelImg = RS("modelImg")
	isTablet = RS("isTablet")
	if brandID = 17 then useBrandImg = 17 else useBrandImg = 20

	select case modelid
		case "662"
			h1 = "We Have the HTC Herman/Touch Pro CDMA Cell Phone Accessories You Need!"
			altText = "HTC Cell Phone Accessories: HTC Herman/Touch Pro CDMA Cell Phone TYPES"
			topText = "Wireless Emporium is the one-stop shop for HTC Herman/Touch Pro CDMA cell phone accessories! When it comes to <a class=""topText-link"" href=""http://www.wirelessemporium.com"" title=""cell phone accessories"">cell phone accessories</a>, Wireless Emporium has exactly what you need at all the right prices. We sell quality factory-direct HTC Herman/Touch Pro CDMA cell phone accessories for your cell phone at prices you won't see anywhere else on the web! If it's convenience you're looking for, we've got all your HTC Herman/Touch Pro CDMA cell phone essentials here on our site, and we'll ship them to you for free!"
			bottomText2 = "We pride ourselves on having the latest accessories for nearly all cell phone models. Browse our full line of HTC accessories that includes a variety of HTC Touch Pro accessories. Pick up an HTC Touch case today and experience what true quality means. Our selection of HTC Touch Pro accessories consists of only high quality cell phone accessories and offered at discounted prices; check out our HTC Touch case to get a better idea. And like our other HTC Touch Pro accessories, every HTC Touch case comes with a satisfaction guarantee."
			bottomText2 = bottomText2 & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Since 2001, Wireless Emporium has been offering high-quality accessories to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best accessories at the best value. Shipping and superior customer service is free with every order and offered every day!"
			bottomText = "Wireless Emporium is the Number one Online Resource for cell phone accessories and the largest seller of cell phone cases, <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a>, cell phone chargers, cell phone batteries and other accessories to personalize, optimize and protect your phone."
		case "987"
			SEtitle = "Samsung Captivate Accessories (i897) ?Buy Exclusive Samsung Galaxy S Accessories"
			SEdescription = "Get the latest Samsung Captivate Accessories (i897) at Wireless Emporium. Buy Samsung Captivate Covers, Cases, Batteries, Chargers & Bluetooth Headsets at Discounted Rates!"
			SEkeywords = "samsung captivate accessories, accessories for samsung captivate, samsung captivate cases, samsung captivate covers, samsung captivate chargers, samsung captivate faceplates, samsung galaxy s accessories"
			h1 = "Samsung Captivate Accessories (i897)"
			altText = "Samsung Captivate TYPES"
			topText = "Wireless Emporium is the #1 source for factory-direct Samsung Captivate i897 phone accessories. We offer the best Samsung Captivate accessories at the best prices available anywhere, and are constantly updating our inventory of Samsung Captivate i897 cell phone accessories to reflect the latest in technology and convenience. Since 2001, Wireless Emporium has made finding the right Samsung Captivate i897 phone accessories at the right price easy. You won't find a better selection of accessories for this exciting phone from the Samsung Galaxy S family at better prices anywhere online or at your local retailer."
			bottomText2 = "Thousands of customers have found the Samsung Captivate i897 cellphone accessories they need at Wireless Emporium. Our reputation for cost-plus pricing, easy ordering, and fast FREE shipping on all our Samsung Captivate i897 cell phone accessories is unmatched in the industry. When you want the latest and greatest in Samsung Captivate accessories, you'll simply find no better value than at Wireless Emporium. Shop our Galaxy S accessories for the Captivate phone below!"
			bottomText2 = bottomText2 & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Wireless Emporium is itself the epitome of quality and reliability, and its top class cell phone accessories like <a class=""topText-link"" href=""http://www.wirelessemporium.com/sb-9-sm-987-sc-7-cases-pouches-skins-samsung-captivate-i897.asp"" title=""Samsung Captivate Cases"">Samsung Captivate cases</a> are just a simple example of the credibility of its products."
		case "988"
			SEtitle = "Samsung Vibrant Accessories ?Buy Samsung Galaxy S Accessories for the Vibrant T959"
			SEdescription = "Searching for the latest Samsung Vibrant Accessories? Look no further than Wireless Emporium. We carry the best discounted Samsung Vibrant Accessories all with Free Shipping!"
			SEkeywords = "samsung vibrant accessories, accessories for samsung vibrant, samsung vibrant cases, samsung vibrant covers, samsung vibrant chargers, samsung vibrant faceplates, samsung galaxy s accessories"
			h1 = "Samsung Vibrant Accessories (T959)"
			altText = "Samsung Vibrant TYPES"
			topText = "Wireless Emporium is the #1 source for factory-direct Samsung Vibrant accessories. We offer the best Samsung Vibrant T959 wireless phone accessories at the best prices available anywhere, and are constantly updating our inventory of Samsung Vibrant T959 cell phone accessories to reflect the latest in technology and convenience. Since 2001, Wireless Emporium has made finding the right Samsung Galaxy S accessories for the Vibrant phone at the right price easy. You won't find a better selection of accessories at better prices anywhere online or at your local retailer."
			bottomText2 = "Thousands of customers have found the Samsung Vibrant accessories they need at Wireless Emporium. Our reputation for cost-plus pricing, easy ordering, and fast FREE shipping on all our Samsung Vibrant T959 cell phone accessories is unmatched in the industry. When you want the latest and greatest in Samsung Vibrant T959 accessories, you'll simply find no better value than at Wireless Emporium. Shop our Samsung Vibrant T959 accessories below!"
			bottomText2 = bottomText2 & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Wireless Emporium is itself the epitome of quality and reliability, and its top class cell phone accessories like Samsung Vibrant T959 covers are just a simple example of the credibility of its products."
			bottomText = "Wireless Emporium is the Number one Online Resource for cell phone accessories and the largest seller of <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-cases.asp"" title=""Cell Phone Cases"">cell phone cases</a>, cell phone covers, cell phone chargers, cell phone batteries and other accessories to personalize, optimize and protect your phone."
		case "1009"
			SEtitle = "Samsung Epic 4G ?Buy only the best Samsung Galaxy S Accessories for the Epic 4G"
			SEdescription = "Find the latest Samsung Epic 4G Accessories at Wireless Emporium ?Your one-stop-shop for Samsung Galaxy S Accessories for the Epic 4G and all other Galaxy S Phones"
			SEkeywords = "samsung epic 4g accessories, accessories for samsung epic 4g, samsung epic 4g cases, samsung epic 4g covers, samsung epic 4g chargers, samsung epic 4g faceplates, samsung galaxy s accessories"
			h1 = "Samsung Epic 4G Accessories"
			altText = "Samsung Epic 4G TYPES"
			topText = "Wireless Emporium is the #1 source for factory-direct Samsung Epic 4G accessories. We offer the best Samsung Epic 4G wireless phone accessories at the best prices available anywhere, and are constantly updating our inventory of Samsung Epic 4G cell phone accessories to reflect the latest in technology and convenience. Since 2001, Wireless Emporium has made finding the right Samsung Epic 4G phone accessories at the right price easy. You won't find a better selection of accessories at better prices anywhere online or at your local retailer."
			bottomText2 = "Thousands of customers have found the Samsung Epic 4G accessories they need at Wireless Emporium. Our reputation for cost-plus pricing, easy ordering, and fast FREE shipping on all our Samsung Epic 4G cell phone accessories is unmatched in the industry. When you want the latest and greatest in Samsung Epic 4G accessories, you'll simply find no better value than at Wireless Emporium. Shop our Samsung Galaxy S accessories for the Epic 4G phone below!"
			bottomText2 = bottomText2 & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Wireless Emporium is itself the epitome of quality and reliability, and its top class cell phone accessories like Samsung Epic 4G covers are just a simple example of the credibility of its products."
			bottomText = "Wireless Emporium is the Number one Online Resource for cell phone accessories and the largest seller of cell phone cases, cell phone covers, cell phone chargers, <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-batteries.asp"" title=""Cell Phone Batteries"">cell phone batteries</a> and other accessories to personalize, optimize and protect your phone."
		case "1015"
			SEtitle = "Samsung Fascinate Accessories (i500) ?Get the Latest Samsung Galaxy S Accessories"
			SEdescription = "You can find the best Samsung Fascinate Accessories at WirelessEmporium. Get Great deals on Samsung Fascinate Covers, Cases, Faceplates, Batteries and more with Free Shipping!"
			SEkeywords = "samsung fascinate accessories, accessories for samsung fascinate, samsung fascinate cases, samsung fascinate covers, samsung fascinate chargers, samsung fascinate faceplates, samsung galaxy s accessories"
			h1 = "Samsung Fascinate Accessories (i500)"
			altText = "Samsung Fascinate TYPES"
			topText = "Wireless Emporium is the #1 source for factory-direct Samsung Fascinate accessories. We offer the best Samsung Fascinate i500 wireless phone accessories at the best prices available anywhere, and are constantly updating our inventory of Samsung Fascinate i500 cell phone accessories to reflect the latest in technology and convenience. Since 2001, Wireless Emporium has made finding the right Samsung Fascinate i500 phone accessories at the right price easy. You won't find a better selection of Samsung Galaxy S accessories for the Fascinate phone at better prices anywhere online or at your local retailer."
			bottomText2 = "Thousands of customers have found the Samsung Fascinate accessories they need at Wireless Emporium. Our reputation for cost-plus pricing, easy ordering, and fast FREE shipping on all our Samsung Fascinate i500 cell phone accessories is unmatched in the industry. When you want the latest and greatest in Samsung Fascinate i500 accessories, you'll simply find no better value than at Wireless Emporium. Shop our Samsung Fascinate i500 accessories below!"
			bottomText2 = bottomText2 & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Wireless Emporium is itself the epitome of quality and reliability, and its top class cell phone accessories like <a class=""topText-link"" href=""http://www.wirelessemporium.com/sb-9-sm-1015-sc-7-cases-pouches-skins-samsung-fascinate-i500-.asp"" title=""Samsung Fascinate Covers"">Samsung Fascinate covers</a> are just a simple example of the credibility of its products."
			bottomText = "Wireless Emporium is the Number one Online Resource for cell phone accessories and the largest seller of cell phone cases, <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a>, cell phone chargers, cell phone batteries and other accessories to personalize, optimize and protect your phone."
		case "1105"
			topText = "Samsung was the first company to challenge Apple in the tablet space.  The Galaxy Tab is an extension of their popular line of Galaxy S smartphones, and was the first Android tablet to hit the market.  Smaller than the iPad, the Galaxy tab is the standard by which all subsequent Android tablets has been judged against."
			bottomText = "The Samsung Galaxy tab is a life made easy convenience machine.  Its ability to multitask and perform a variety of functions makes is why it's such a popular device.  It is also why you should protect that device with the great selection of <a href=""http://www.wirelessemporium.com/T-1105-cell-accessories-samsung-galaxy-tab.asp"" class=""bottomtext-link"" title=""Galaxy Tab accessories"">Samsung Galaxy Tab accessories</a> available at Wireless Emporium.  We carry accessories designed to protect your device, enhance its performancne and even one that let you demonstrate your personality.  There really isn't any need to shop around.  Our prices, coupled with free shipping means that you won't find a better value anywhere on the web."
			bottomText = bottomText & "<br><br>Feel free to browse our selection of Galaxy Tab screen protectors.  Not only do they shield your Galaxy Tab from scratches, but they come in a variety of styles too.  Our <a href=http://www.wirelessemporium.com/sb-9-sm-1105-sc-3-faceplates-screen-protectors-samsung-galaxy-tab.asp"" class=""bottomtext-link"" title=""Galaxy Tab faceplates"">Galaxy Tab faceplates</a> and <a href=""http://www.wirelessemporium.com/sb-9-sm-1105-sc-19-decal-skin-samsung-galaxy-tab.asp"" class=""bottomtext-link"" title=""Galaxy Tab decal skins"">Galaxy Tab decal skins</a> are a great way to add both a layer of protection for your device while also expressing a little personality too.  "
			bottomText = bottomText & "With a huge selection of colors and designs to choose from, we have something for everyone.  Our Samsung Galaxy Tab leather cases are a great look for a business professional or a student and help you carry your device around without damaging it.  We also have a huge selection of <a href=""http://www.wirelessemporium.com/sb-9-sm-1105-sc-2-chargers-samsung-galaxy-tab.asp"" class=""bottomtext-link"" title=""Galaxy Tab chargers"">Galaxy Tab chargers</a> to make sure that you are fully powered up, no matter where you are.  All of our Galaxy Tab accessories are guaranteed to meet the highest industry standards and are backed by a 1 year warranty."					
		case "1133"
			topText = "It's hard to improve upon one of the most innovative devices to come into our lives, but that is exactly what Apple did when they introduced the iPad 2.  Beefed up specs and a more ergonomic design have made this tablet device a best seller. Our selection of <a href=""http://www.wirelessemporium.com/T-1133-cell-accessories-apple-ipad-2.asp"" class=""toptext-link"" title=""iPad 2 accessories"">iPad 2 accessories</a> will ensure that you get the most out of your device."
			bottomText = "You're iPad 2 can do so much.  It is a research dynamo, a media hub, and a general convenience machine.  But it is only useful so long as it's working properly.  Our line of iPad 2 accessories will ensure that your device not only maintains it fresh from the box look but also can enhance its performance.  From an aesthetic perspective, our <a href=http://www.wirelessemporium.com/sb-17-sm-1133-sc-19-decal-skin-apple-ipad-2.asp"" class=""bottomtext-link"" class=""iPad 2 decal skins"">iPad 2 decal skins</a> will give your device a unique and personalized look, while protecting the body of your iPad 2 from scratches and dust.  For a little more advanced protection, we carry several different <a href=""http://www.wirelessemporium.com/sb-17-sm-1133-sc-7-cases-pouches-skins-apple-ipad-2.asp"" class=""bottomtext-link"" title=""iPad 2 cases"">iPad 2 cases</a>, all designed to add a protective layer that can repel more dangerous threats to your tablet."
			bottomText = bottomText & "<br /><br />For your convenience, we have several iPad 2 carrying cases to help you get your tablet from point A to point B without damaging it.  An <a href=""http://www.wirelessemporium.com/hf-sm-1133-apple-ipad-2-bluetooth-headsets-headphones.asp"" class=""bottomtext-link"" title=""bluetooth devices"">iPad 2 bluetooth keyboard</a> is a great way to improve productivity for students and business professionals alike."
			bottomText = bottomText & "	And our <a href=""http://www.wirelessemporium.com/sb-17-sm-1133-sc-13-data-cable-memory-apple-ipad-2.asp"" class=""bottomtext-link"" class=""iPad 2 data cables"">iPad 2 data cables</a> helps to keep all your devices connected.  We also have multiple <a href=""http://www.wirelessemporium.com/sb-17-sm-1133-sc-2-chargers-apple-ipad-2.asp"" class=""bottomtext-link"" title=""iPad 2 Chargers"">iPad 2 chargers</a> for the home, office or car so you can have the peace of mind that your device will always be ready to go when you are.  All of our iPad 2 accessories include free shipping and a 1 year warranty.  Wireless Emporium also offers a 90 day EZ return policy, so you know you can shop with confidence."
		case "1207"
			topText = "E-readers have revolutionized the way we read books.  Compact and completely portable, these amazing devices have the capacity to hold thousands of books.  And their e-ink technology means that you can read these devices in direct sunlight without any glare. The Amazon Kindle 3G is one of the premier e-readers available today.  Protect your device with our Amazon Kindle 3G accessories</a>"
			bottomText = "As great as the Amazon Kindle 3G is, it is virtually worthless if it should crack or get damaged.  It is important to protect your Amazon Kindle 3G with durable, high quality <a href=""http://www.wirelessemporium.com/tablet-ereader-accessories.asp"" class=""bottomtext-link"" title=""E-reader accessories"">e-reader accessories</a> from Wireless Emporium.  A <a href=""http://www.wirelessemporium.com/sb-21-sm-1207-sc-3-faceplates-screen-protectors-amazon-kindle-3g.asp""> class=""bottomtext-link"" title =""Kindle 3G cases"">Kindle 3G case</a> or Kindle 3G faceplate is a great way to protect your e-reader from picking up excessive damage from drops, scratches, dust and any other every day wear and tear.  "
			bottomText = bottomText & "We also carry a number of <a href=""http://www.wirelessemporium.com/hf-sm-1207-amazon-kindle-3g-bluetooth-headsets-headphones.asp"" class=""bottomtext-link"" title=""Kindle 3G hands free headsets"">hands free headsets</a> and Bluetooth devices that easily pair with your Amazon Kindle 3G and make your life that much easier.  All of the Amazon Kindle 3G accessories sold at Wireless Emporium come with free shipping and a 90 day no hassle return policy."
		case "1191"
			topText = "As Amazon's follow up to their wildly popular Kindle, the Amazon Kindle 2 has continued to lead the way in the E-reader category.  Amazingly lightweight and ulta portable, this device may be small, but it packs a powerful punch.  Be sure that you get the most out of your Amazon Kindle 2 with our huge selection of Kindle 2 accessories."
			bottomText = "The Amazon Kindle 2 utilizes a technology called e-ink, which can be read in direct sunlight without any glare.  But that technology is useless if your Kindle 2's screen is scratched up.  A <a href=""http://www.wirelessemporium.com/sb-21-sm-1191-sc-18-screen-protectors-amazon-kindle-2.asp"" class=""bottomtext-link"" title=""Kindle 2 screen protectors"">Kindle 2 screen protector</a> is a great way to ensure that your display remains free from damage that can occur through every day use.  You can also protect the body of your device with a <a href=""http://www.wirelessemporium.com/sb-21-sm-1191-sc-19-decal-skin-amazon-kindle-2.asp"" class=""bottomtext-link"" title=""Kindle 2 decal skins"">Kindle 2 decal skin</a>.  Not only do they help prevent scratches and keep dust from getting into your device and damaging internal components, they look great.  Personalize your Kindle 2.  We carry decal skins for any personality.  "
			bottomText = bottomText & "We also carry a number of hands free headsets and Bluetooth devices designed to keep your Kindle 2 connected and make use that much more convenient.  Like everything sold at Wireless Emporium, all of our Amazon Kindle 2 accessores are backed by a 1 year warrantly and a 90 day no hassle return policy."
		case "1197"
			topText = "Not to be left out, Barnes & Noble released an E-reader of their own called the Nook.  Featuring e-ink technology, similar to that of the Kindle, the Nook also has a vibrant color display.  The combination lends itself to a very friendly user experience, which is why this is one of the most popular e-readers you will find anywhere.  Our full line of Nook accessories will keep you device protected and looking great for less."
			bottomText = "You love your Barnes & Noble Nook.  You love it so much that you find yourself constantly using it.  If that is the case, it is imperative that you preserve your device with Nook accessories.  Wireless Emporium guarantees to have the best prices anywhere, and a great selection on everything from Nook screen protectors to <a href=""http://www.wirelessemporium.com/sb-23-sm-1197-sc-7-cases-pouches-skins-barnes-noble-nook.asp"" class=""bottomtext-link"" title=""Nook carrying cases"">Nook carrying cases</a>.  Our screen protectors shield your Nook from scratches, dust and fingerprints that can have a severely adverse effect on your enjoyment of your e-reader.  Our Nook cases don't only protect your device from drop and shock damage, our leather carrying cases provide a classy and convenient way to bring your Nook from point A to point B.  "
			bottomText = bottomText & "If it is personalization you are looking for, a <a href=""http://www.wirelessemporium.com/sb-23-sm-1197-sc-19-decal-skin-barnes-noble-nook.asp"" class=""bottomtext-link"" title=""Nook decal skins"">Nook decal skin</a> is a great choice.  Not only do we have thousands of designs to choose from, but these decal skins are a great way to prevent your Nook from getting scratched up as well as keep dust from getting inside the device.  All of our Nook accessories are backed by a 1 year warranty and come with free shipping."
		case "1206"
			topText = "The Blackberry Playbook is a great fusion of business prowess and mass consumer usability.  Running on Blackberry's proprietary OS, this much anticipated device is a strong competitor to Apple's iPad and the various Android tablets on the market.  Even though this is one of the more recent competitors in the tablet space, we carry several Blackberry Playbook accessories for you to choose from."
			bottomText = "A device as high powered as the Blackberry Playbook deserves a level of protection above and beyond what is offered right out of the box.  As great as the Playbook is, it isn't indestructible.  It is important to protect it with a <a href=""http://www.wirelessemporium.com/sb-14-sm-1206-sc-3-faceplates-screen-protectors-blackberry-playbook.asp"" class=""bottomtext-link"" title=""Playbook faceplates"">Playbook faceplate</a> to ensure that every day use and minor accidents don't cause any permanent damage that could render your investment worthless.  You need to guard that display from any damage as well.  That is where a Blackberry Playbook screen protector comes into play.  No need to fret over scratches, dust or fingerprints so long as your Playbook is sporting one of our screen protectors.  "
			bottomText = bottomText & "Our full line of <a href=""http://www.wirelessemporium.com/hf-sm-1206-blackberry-playbook-bluetooth-headsets-headphones.asp"" class=""bottomtext-link"" title=""Bluetooth headsets"">Bluetooth headsets</a> and hands free devices help make your user experience more enjoyable and convenient.   All of our accessories are backed by a 1 year warranty, 90 day no hassle return policy and free shipping."
		case else
			h1 = "We Have the " & brandName & " " & modelName & " Cell Phone Accessories You Need!"
			altText = modelName & " TYPES"
			topText = "Wireless Emporium is the #1 source for factory-direct " & brandName & " " & modelName & " phone accessories. We offer the best " & brandName & " " & modelName & " wireless phone accessories at the best prices available anywhere, and are constantly updating our inventory of " & brandName & " " & modelName & " cell phone accessories to reflect the latest in technology and convenience. Since 2001, Wireless Emporium has made finding the right " & brandName & " " & modelName & " phone accessories at the right price easy. You won't find a better selection of accessories at better prices anywhere online or at your local retailer."
			bottomText2 = "Thousands of customers have found the " & brandName & " " & modelName & " cellphone accessories they need at Wireless Emporium. Our reputation for cost-plus pricing, easy ordering, and fast FREE shipping on all our " & brandName & " " & modelName & " cell phone accessories is unmatched in the industry. When you want the latest and greatest in " & brandName & " " & modelName & " accessories, you'll simply find no better value than at Wireless Emporium. Shop our " & brandName & " " & modelName & " accessories below!"
			bottomText2 = bottomText2 & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Wireless Emporium is itself the epitome of quality and reliability, and its top class cell phone accessories like " & brandName & " " & modelName & " covers are just a simple example of the credibility of its products."
			bottomText = "Wireless Emporium is the Number one Online Resource for cell phone accessories and the largest seller of cell phone cases, <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a>, cell phone chargers, cell phone batteries and other accessories to personalize, optimize and protect your phone."
	end select
	
	'consolidate bottomText
	if bottomText <> "" then bottomText = "<p>"& bottomText &"</p>"
	if bottomText2 <> "" then bottomText = "<p>"& bottomText2 &"</p>"& bottomText
	
	if SEtitle = "" or SEdescription = "" or SEkeywords = "" then
		SQL = "SELECT * FROM we_MetaTags WHERE modelid = '" & modelid & "' AND typeid IS NULL ORDER BY id DESC"
	'	response.write "SQL2 : " & SQL & "<br>"
		dim RSmeta
		set RSmeta = Server.CreateObject("ADODB.Recordset")
		RSmeta.open SQL, oConn, 0, 1
		if not RSmeta.eof then
			if SEtitle = "" then SEtitle = RSmeta("title")
			if SEdescription = "" then SEdescription = RSmeta("description")
			if SEkeywords = "" then SEkeywords = RSmeta("keywords")
		else
			if SEtitle = "" then SEtitle = "Discount " & brandName & " " & modelName & " Cell Phone Accessories, Mobile Phone Batteries, Chargers, Faceplate, Holsters / Belt Clips, Leather Cases, Bluetooth Hands-Free Headsets"
			if SEdescription = "" then SEdescription = "Wireless Emporium is the #1 source for factory-direct " & brandName & " " & modelName & " Cell Phone Accessories. Buy batteries, chargers, holsters/ belt clips, leather cases & Bluetooth hands-free headsets for " & brandName & " cell phones @ discounted rates!!"
			if SEkeywords = "" then SEkeywords = brandName & " cell phone accessory, " & brandName & " mobile phone accessories, battery " & brandName & ", battery cell " & brandName & " phone, battery cellular " & brandName & " phone, battery charger " & brandName & ", " & brandName & " cell phone charger, " & brandName & " face plate, " & brandName & " leather case, " & brandName & " holsters, " & brandName & " antennas"
		end if
	end if
else
	sql	=	"select	a.modelid, a.modelname, a.modelimg, c.brandid, c.brandname, a.isTablet" & vbcrlf & _
			"from	we_models a join we_brands c " & vbcrlf & _
			"	on	a.brandid = c.brandid" & vbcrlf & _
			"where	a.modelid = '" & modelid & "' " & vbcrlf

	session("errorSQL") = SQL
'	response.write sql
	
	set objRsName = Server.CreateObject("ADODB.Recordset")
	objRsName.open sql, oConn, 0, 1
	
	if not objRsName.eof then
		brandName = objRsName("brandName")
		brandID = objRsName("brandID")
		modelName = objRsName("modelName")
		modelImg = objRsName("modelImg")
		isTablet = objRsName("isTablet")
	end if
	
	objRsName = null
end if

'if session("breadcrumb_brand") <> "" then brandName = session("breadcrumb_brand") end if 
'session("breadcrumb_model") = modelName

session("otherAdminOptions") = ", <a style='color:#ffffff;' href='/admin/db_update_models.asp?ModelID=" & modelID & "&BrandID=" & brandID & "&searchType=Model&submitType=Edit&submitModel=Search'>Edit " & modelName & "</a>"

'todo: need to consolidate Metatags with we_XXXText
dim strH1: strH1 = brandName & " " & modelName & " Accessories"
dim strH2: strH2 = EMPTY_STRING
dim strAltText: strAltText = altText & " Accessories"
dim strBreadcrumb : strBreadcrumb = brandName & " " & modelName & " Accessories"
dim strBrandBreadCrumb

if brandID = 17 and instr(lcase(modelName), "iphone") > 0 then
	strBrandBreadCrumb = brandName & " iPhone Accessories"
elseif brandID = 17 and instr(lcase(modelName), "iphone") <= 0 then
	strBrandBreadCrumb = brandName & " iPod/iPad Accessories"
else
	strBrandBreadCrumb = brandName & " Cell Phone Accessories"
end if

'set input values to extract page meta-values
dim dicReplaceAttribute: set dicReplaceAttribute = CreateObject("Scripting.Dictionary")
dicReplaceAttribute( "BrandName") = brandName
dicReplaceAttribute( "ModelName") = modelName

'set known product attributes to replace out template placeholders
dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
dicSeoAttributeInput( "BrandId") = brandID
dicSeoAttributeInput( "ModelId") = modelID

call LookupSeoAttributes()

if isTablet then
	strTypeToken = "Tablet"
else
	strTypeToken = "Cell Phone"
end if


%>
<!--#include virtual="/includes/template/top.asp"-->
<script>utmx_section("accessoryFinder")</script>
<link href="/includes/css/mvt_accessoryFinder1.css" rel="stylesheet" type="text/css">
</noscript>
<script>utmx_section("banner")</script>
<link href="/includes/css/mvt_banner1.css" rel="stylesheet" type="text/css">
</noscript>
<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
    	<td>
        	<div id="banner1"><img src="/images/banners/we-free-sameday-shipping2.jpg" border="0" /></div>
            <div id="banner2"><img src="/images/banners/we-90day-sat2.jpg" border="0" /></div>
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" valign="top" class="breadcrumbFinal">
        	<%
			if isTablet then
			%>
				<a class="breadcrumb" href="/tablet-ereader-accessories.asp">Tablet Accessories</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
            <%
			else
			%>
				<a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;
				<a class="breadcrumb" href="/<%=brandSEO(brandID)%>"><%=strBrandBreadCrumb%></a>&nbsp;&gt;&nbsp;
				<%=strBreadcrumb%>
            <%
			end if
			%>
        </td>
    </tr>
    <tr>
        <td width="100%" align="center" valign="top">
            <img src="/images/brandmodelheaders/header_<%=brandID%>.jpg" width="745" height="30" border="0" alt="<%=strBreadCrumb_brand & " " & modelName%> Accessories">
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" valign="top">
            <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="150" height="100" align="center" valign="middle">
                        <img src="/productPics/models/<%=modelImg%>" width="80" border="0" alt="<%=strAltText%>">
                    </td>
                    <td width="5" align="center" valign="middle">
                        <img src="/images/spacer.gif" width="1" height="5" border="0">
                    </td>
                    <td width="585" align="left" valign="top">
                        <h1 class="brandModel"><%=strH1%></h1>
                        <div id="CustomerContent"><%=SeTopText%></div>
                        <%'<p class="topText"><%=topText$></p>%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr id="horz_accessoryFinder">
    	<td>
        	<form id="id_frmFinder2" name="frmFinder2" style="padding:0px; margin:0px;">
            <div style="background-image:url(/images/backgrounds/accessorySearch_bg_noLine.jpg); width:100%; height:87px; margin:10px 0px 10px 0px;">
            	<div style="background-image:url(/images/backgrounds/accessorySearch_bg_line.jpg); width:730px; height:87px; margin-left:auto; margin-right:auto;">
                	<div style="float:left; padding-top:5px;"><img src="/images/icons/accessorySearch_icon.png" border="0" /></div>
                    <div style="float:left; padding:5px 0px 0px 10px; font-size:18px; color:#FFF; font-weight:bold; height:19px;">Accessory Finder</div>
                    <div style="float:left; padding:11px 0px 0px 20px; font-size:12px; color:#FFF; font-style:italic; width:500px; height:19px;">Find Accessories More Efficiently</div>
                    <div style="float:left; padding:20px 0px 0px 10px;">
                    	<select name="cbBrand" style="width:175px;" onchange="onFinder2('b',this);">
                            <option value="">Your Phone Brand</option>
                            <%
							objRsTopBrand.movefirst
                            do until objRsTopBrand.eof
                                leftNavBrandid = prepInt(objRsTopBrand("brandid"))
                                leftNavBrandName = replace(objRsTopBrand("brandName"), "/", " / ")
                            %>
                            <option value="<%=leftNavBrandid%>"<% if prepInt(leftNavBrandid) = prepInt(brandID) then %> selected="selected"<% end if %>><%=leftNavBrandName%></option>
                            <%
                                objRsTopBrand.movenext
                            loop
                            %>
                        </select>
                    </div>
                    <div style="float:left; padding:20px 0px 0px 10px;" id="cbModelBox2">
                    	<select name="cbModel" style="width:175px;" onchange="onFinder2('m',this);">
                            <option value="">Your Phone Model</option>
                        </select>
                    </div>
                    <div style="float:left; padding:20px 0px 0px 10px;" id="cbCatBox2">
                    	<select name="cbCat" style="width:175px;">
                            <option value="">Accessory Category</option>
                        </select>
                    </div>
                    <div style="float:left; padding:15px 0px 0px 20px; cursor:pointer;" onclick="jumpTo2()"><img src="/images/buttons/accessorySearch_button.png" border="0" /></div>
                </div>
            </div>
            </form>
        </td>
    </tr>
    <%
	if RS.eof then
	%>
    <tr>
        <td width="100%" align="center" valign="top" style="padding:30px; font-size:30px; color:#FF6100;">
        	The product you are searching for may be unavailable at this time
        </td>
    </tr>    
    <%
	else
	%>
    <tr>
        <td width="100%" align="left" valign="top" style="padding-top:20px;">
            <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                <tr>
                    <%
                    a = 0
					sql	=	"select	count(*) cnt" & vbcrlf & _
							"from	we_ItemsExtendedData a join we_items b" & vbcrlf & _
							"	on	a.partnumber = b.partnumber join v_subtypematrix v" & vbcrlf & _
							"	on	b.subtypeid = v.subtypeid" & vbcrlf & _
							"where	b.modelid = '" & modelid & "' and v.typeid = '3'" & vbcrlf & _
							"	and	a.customize = 1"
					session("errorSQL") = sql
					set rsCustom = oConn.execute(sql)
					if rsCustom("cnt") > 0 then
						strLink = "/sb-" & brandid & "-sm-" & modelid & "-scd-2000-custom-covers-faceplates-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
						altText = modelName & " Custom Cases"
						%>
                        <td align="center" valign="top" width="165" style="padding:15px 5px 30px 5px;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr><td align="center"><a href="<%=strLink%>" title="<%=altText%>"><img src="/images/WE-category-buttons-custom.jpg" width="165" border="0" alt="<%=altText%>" title="<%=altText%>" /></a></td></tr>
                                <tr><td align="center" style="padding-top:10px;"><a href="<%=strLink%>" title="<%=altText%>" class="cellphone3-link"><%=altText%></a></td></tr>                            
                            </table>
                        </td>
                        <%
						a = a + 1
					end if

					set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
					dim strBrandModelCategoryUrl: strBrandModelCategoryUrl = EMPTY_STRING

					brandName = replace(brandName, "/", " / ")
					modelName = replace(modelName, "/", " / ")					
                    do until RS.eof
						thisType = replace(RS("typeName"), "/", " / ")
						'sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}.asp
						strLink = replace(replace(replace(replace(replace(replace(rs("urlStructure"), "{brandid}", brandid), "{modelid}", modelid), "{typeid}", rs("typeid")), "{brandname}", formatSEO(brandName)), "{modelname}", formatSEO(modelName)), "{typename}", formatSEO(thisType))
						altText = modelName & " " & thisType
                    %>
                    <td align="center" valign="top" width="165" style="padding:15px 5px 30px 5px;">
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        	<tr><td align="center"><a href="<%=strLink%>" title="<%=altText%>"><img src="/images/categories/we_cat_<%=rs("typeid")%>.jpg" width="165" border="0" alt="<%=altText%>" title="<%=altText%>" /></a></td></tr>
                        	<tr><td align="center" style="padding-top:10px;"><a href="<%=strLink%>" title="<%=altText%>" class="cellphone3-link"><%=altText%></a></td></tr>                            
                        </table>
                    </td>
                    <%
                        RS.movenext
                        a = a + 1
                        if a = 4 then
                            response.write "</tr><tr>" & vbcrlf
                            a = 0
                        end if
                    loop
					
					bottomText = ApplyPageAttribute( bottomText, dicSeoAttributeInput)
					set dicSeoAttributeInput = nothing
                    %>
                </tr>
            </table>
        </td>
    </tr>
    <%
	if not isTablet then 
	%>
    <tr>
        <td align="center" valign="top" style="padding-top:20px;">
            <div class="graybox-middle" style="width:726px; border-left:1px solid #999; border-right:1px solid #999; padding:0px 10px 0px 10px;">
                <div class="graybox-inner" title="RELATED <%=ucase(brandName)%><%=" " & ucase(strTypeToken)%> MODELS" align="center">
                	<h3 class="brandModel">RELATED <%=ucase(brandName)%><%=" " & ucase(strTypeToken)%> MODELS</h3>
				</div>
            </div>
            <table border="0" cellspacing="10" cellpadding="0" width="100%">
                <tr>
                    <%
                    dim strRelatedSQL
                    strRelatedSQL	=	"select	top 5 modelid, modelName, modelImg " & vbcrlf & _
                                        "from	we_models " & vbcrlf & _
                                        "where	brandid = '" & brandID & "'" & vbcrlf & _
                                        "	and	hidelive = 0 " & vbcrlf & _
                                        "	and	modelid <> '" & modelID & "'" & vbcrlf & _
                                        "order by releaseyear desc, releasequarter desc	" & vbcrlf
                                
                    set RS2 = Server.CreateObject("ADODB.Recordset")
                    RS2.open strRelatedSQL, oConn, 0, 1
                                        
                    do until RS2.eof
                        modelID = RS2("modelID")
                        modelName = RS2("modelName")
                        modelImg = RS2("modelImg")

                        if brandid = "17" then
                            altTag = modelName & " Accessories"
                        else
                            altTag = brandName & " " & modelName & " Accessories"
                        end if

                        RS2.movenext
                        %>
                        <td width="160" align="center" valign="top" style="padding-top:5px;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr>
                                    <td width="100%" height="150" align="center" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" align="center">
                                            <tr>
                                                <!--
                                                <%'if session("adminID") > 0 and lap > 1 then %>
                                                <td><a href="/admin/adjPopPhones.asp?upOrderNum=<%=modelID%>&brandID=<%=brandID%>&curOrderNum=<%=orderNum%>&phoneOnly=<%=phoneOnly%>" style="font-size:24px; font-family:Arial; font-weight:bold; text-decoration:none;">&lt;</a></td>
                                                <%'end if %>
                                                -->
                                                <td align="center">
                                                    <%if modelID = 940 then%>
                                                        <a href="/apple-ipad-accessories.asp" title="<%=altTag%>"><img src="/productPics/models/<%=modelImg%>" border="0" alt="<%=replace(altTag,"Accessories","accessories")%>"></a><br /><br />
                                                        <a class="cellphone-link" href="/apple-ipad-accessories.asp" title="<%=altTag%>"><%=replace(modelName,"/"," / ") & "&nbsp;Accessories"%></a>
                                                    <%else%>
                                                        <a href="/T-<%=modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp" title="<%=altTag%>"><img src="/productPics/models/<%=modelImg%>" border="0" alt="<%=replace(altTag,"Accessories","accessories")%>"></a><br /><br />
                                                        <a class="cellphone-link" href="/T-<%=modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp" title="<%=altTag%>"><%=replace(modelName,"/"," / ") & "&nbsp;Accessories"%></a>
                                                    <%end if%>
                                                </td>
                                                <!--
                                                <%' if session("adminID") > 0 and not rs2.EOF then %>
                                                <td><a href="/admin/adjPopPhones.asp?downOrderNum=<%=modelID%>&brandID=<%=brandID%>&curOrderNum=<%=orderNum%>" style="font-size:24px; font-family:Arial; font-weight:bold; text-decoration:none;">&gt;</a></td>
                                                <%' end if %>
                                                -->
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!--
                                <%' if session("adminID") > 0 then %>
                                <tr>
                                    <td align="center" id="popOp_<%=modelID%>">
                                        <a href="/admin/adjPopPhones.asp?removeModelID=<%=modelID%>&brandID=<%=brandID%>&curOrderNum=<%=orderNum%>&phoneOnly=<%=phoneOnly%>">Remove From<br />Popular Phones</a>
                                    </td>
                                </tr>
                                <%' end if %>
                                -->
                                <tr>
                                    <td colspan="2" align="center" valign="middle"><img src="/images/spacer.gif" width="1" height="6"></td>
                                </tr>
                            </table>
                        </td>
                        <%
                    loop
                    RS2.close
                    set RS2 = nothing
                    %>
                </tr>
            </table>
        </td>
    </tr>
	<%
	end if
	
	if saveModelID = 1412 then	'	Saved Model is iPhone 5 (modelID 1412)
    %>
    <tr>
        <td align="center" valign="top" style="padding-top:20px;">
			<style>
                #diviPhone5Rumors h2, #diviPhone5Rumors h3, #diviPhone5Rumors dt { color:slategray; }
                #diviPhone5Rumors dt, #diviPhone5Rumors dd { margin-bottom: 1.0em; /*margin-left: 1.5em;*/ }
                #diviPhone5Rumors dt { font-weight: bold; font-size:1.25em; }
                #diviPhone5Rumors dl dd dt { color: #000; margin-top: 1.0em; font-weight: inherit; font-style: italic; }
                
                .divPhotoR { margin-top:2.5em; margin-left:2.0em; margin-bottom:1.5em; min-width:1px; min-height:1px; float:right; }
                .firstPhoto { padding-top:0.0em; }
                .nonFirstPhoto { margin-top:0.5em; }
                #diviPhone5Rumors .italic { font-style: italic; }
                #diviPhone5Rumors .empty { display:none; }
                #diviPhone5Rumors img { border:none; }
            </style>        
            <div class="graybox-middle" style="width:726px; border-left:1px solid #999; border-right:1px solid #999; padding:0px 10px 0px 10px;">
                <div class="graybox-inner" title="Click To Read the Latest iPhone 5 Rumors" align="center">
                	<a href="javascript:toggle('diviPhone5Rumors');"><h3 class="brandModel" style="font-weight:bold;">Click To Read the Latest iPhone 5 Rumors</h3></a>
				</div>
            </div>
            <div id="diviPhone5Rumors" style="display:none; padding-top:20px; text-align:left;">
                <div class="divPhotoR firstPhoto">
                    <img src="/images/iPhone5Rumors/iPhone-5-Back.jpg" alt="iPhone 5" width="360" height="195" />
                </div>
                <p>
                	<br />
                    The Apple iPhone 5 is likely one of the only phones in the world that can dominate
                    headlines without even so much a picture of what it'll actually look like. In fact,
                    rumors are swirling about its form factor, specs and release date and the facts
                    seem to be changing every day. One thing that you can count on, however, is that
                    <a href="http://www.wirelessemporium.com">Wireless Emporium</a> is busy stocking 
                    its shelves with the largest selection of <a href="/iphone-accessories.asp">iPhone 5 accessories</a>, 
                    cases, covers and more so that you can be fully equipped with the
                    best accessories once the iPhone 5 hits the market. With free fast shipping, a worry-free
                    90 day return policy and an unmatched one year warranty, be sure to visit this page
                    for updates!</p>
                
                <h3>
                    EVERYTHING (WE THINK) WE KNOW ABOUT THE iPHONE 5
                </h3>
                
                <dl>
                    <dt class="italic">When is the iPhone 5 coming out? What are the new features? What is the iPhone 5
                        price? Will it be under Sprint, Verizon, or AT&T?</dt>
                    <dd>
                        Everyone is dying to know the details about Apple's upcoming phone. We have rounded
                        up the rumors, predictions, and speculations regarding the next-generation iPhone.
                        We warn you though to take everything with a grain of salt. Nothing is official
                        until the company actually launches the much-awaited device.
                    </dd>
                </dl>
                <dl>
                    <dt>
                        The Form Factor
                    </dt>
                    <dd>
                        The next-generation iPhone will no doubt feature more advanced hardware technology
                        than its predecessor, the iPhone 4S. Everyone is speculating wildly on what it will
                        look like - the first rumor was a metal unibody made of exotic material named LiquidMetal.
                        This guess was actually fairly close to the truth, considering Apple's acquisition
                        of the rights to the material in 2010. Leaked photos, however suggest otherwise.
                        Nothing is official at this moment, but here are a few new features that the high-end
                        device might have:
                        <dl>
                            <dt>
                                Different design
                            </dt>
                            <dd>
                                The iPhone 5 may not have a LiquidMetal unibody, but leaked photos suggest it might
                                have a 100% aluminum back. <a href="http://www.macrumors.com/2012/05/29/claimed-rear-shell-with-sides-for-next-generation-iphone-surfaces/" target="_blank">MacRumors</a> 
                                have rounded up the images that show what the next iPhone 5 might
                                look like. The basic black and white versions are inevitable, but the backs panels
                                are now also two-toned. <a href="http://www.mobilefun.co.uk/blog/2012/06/iphone-5-case-image-leaks-confirm-final-design/" target="_blank">MobileFun</a>'s 
                                report reveals that the next-generation iPhone measures 7.6
                                mm thin, 123.83 mm high, and 58.57 mm wide.
                          </dd>
                            <dt>
                                <div class="divPhotoR nonFirstPhoto">
                                    <img src="/images/iPhone5Rumors/iPhone-5-Larger-Screen.jpg" alt="iPhone 5" width="360" height="298" />
                                </div>
                                Thinner, taller touchscreen
                            </dt>
                            <dd>
                                <a href="http://online.wsj.com/article/SB10001424052702303754904577532121136436182.html" target="_blank">The Wall Street Journal</a> 
                                reports that the iPhone 5 will have a different
                                touchscreen technology that reduces thickness and improves image quality. The technology
                                integrates the touch sensors with the liquid crystal display, making the screen
                                thinner. The integration also saves Apple's production costs, as they no longer
                                have to have separate suppliers for each component. 
                                <a href="http://news.cnet.com/8301-13579_3-57435236-37/apple-orders-4-inch-iphone-screens-says-report/?tag=mncol;txt" target="_blank">CNET</a>
                                has reported that screen size will increase from 3.5 inches to 4 or
                                more. The iPhone 5 will, however, increase only in height. <a href target="_blank""http://9to5mac.com/2012/07/10/claimed-iphone-5-engineering-samples-show-physical-mockup-of-rumored-next-generation-iphone-design/#more-199749">9to5mac</a>'s 
                                leaked photo seems to support this speculation as well.
                      </dd>
                            <dt>
                                Magnetic, mini dock port
                            </dt>
                            <dd>
                                <a href="http://techcrunch.com/2012/06/20/confirmed-the-new-iphone-will-have-a-19-pin-mini-connector/?fb_comment_id=fbc_10151072215241349_24489343_10151073522436349#ffc02d19c" target="_blank">TechCrunch</a> 
                                has confirmed that the next-generation iPhone will feature a
                                19-pin connector instead of the standard 30. The dock connector is as small as the
                                micro USB, as Apple plans to make its mobile devices as thin as possible. The same
                                report speculates that the connectors will be magnetic, just like the MagSafe connectors
                                used on MacBooks.
                          </dd>
                            <dt>
                                New spots for ports
                            </dt>
                            <dd>
                                Aside from the bigger speakers, microphone and the other internal hardware differences,
                                the iPhone 5's ports have also changed their locations. <a href="http://www.mobilefun.co.uk/blog/2012/06/iphone-5-case-image-leaks-confirm-final-design/" target="_blank">MobileFun</a>'s 
                                sourced images show that the audio port is now at the bottom.
                                9to5mac's leaked photos, on the other hand, show the FaceTime camera is located
                                at the center, on top of the earpiece.
                            </dd>
                        </dl>
                        <dt>
                            The Features
                        </dt>
                        <dd>
                            <dl>
                                <dt>
                                    Lightning-fast LTE
                                </dt>
                                <dd>
                                    Apple's competitors are all featuring 4G/LTE technologies on their smartphones.
                                    The iPhone is actually lagging behind in this category. The company's latest iPad
                                    model, however, already has the super fast connectivity feature. It is only logical
                                    for the iPhone to have this high-speed data support standard.
                                </dd>
                                <dt>
                                    Convenient NFC
                                </dt>
                                <dd>
                                    Flagship phones especially have this smartphone feature standard. Although we can
                                    all be content with Wi-Fi connectivity, an additional way of transferring data and
                                    making transactions would not hurt. A lot of phones already have this and Apple
                                    may decide to include it on the iPhone 5's feature set.
                                </dd>
                            </dl>
                        </dd>
                        <dt>The Functionality </dt>
                        <dd>
                            <dl>
                                <dt>
                                <div class="divPhotoR nonFirstPhoto">
                                    <img src="/images/iPhone5Rumors/iPhone-5-Production-Faceplate.jpg" alt="iPhone 5" width="360" height="327" />
                                </div>
                                    New nano-SIM card standard
                                </dt>
                                <dd>
                                    The company paved the way for Micro-SIM card support with its iPhone 4S. It is,
                                    thus, not surprising if Apple did have a nano-SIM support on their next-generation
                                    iPhone. <a href="http://reviews.cnet.com/8301-19512_7-57473318-233/euro-carriers-stocking-nano-sim-cards-ahead-of-iphone-5-launch/" target="_blank">CNET</a>
                                     also reports that European carriers are already stocking nano-SIM cards
                                    in anticipation of Apple's autumn iPhone launch. The question would be whether the
                                    other mobile phone companies will adapt to the new standard.
                              </dd>
                                <dt>
                                    Improved processing speed
                                </dt>
                                <dd>
                                    The iPhone 5 will definitely feature improved processing speed. It is not yet determined,
                                    however, whether it will have a quad-core processor. Pertinent rumors point out
                                    that the next-generation iPhone will have a dual-core instead. Whatever type of
                                    chip the company will use, it better work smoothly. 9to5mac has reported that the
                                    A5 chip is overheating, which caused a delay in iPhone's former expected June launch.
                                </dd>
                                <dt>
                                    Introducing the iOS 6
                                </dt>
                                <dd>
                                    If the speculated hardware modifications turn out to be relatively minor, it is
                                    the iOS 6's slew of software features that will entice the iPhone's next generation
                                    of users. With Apple's new operating system, you can use your iPhone as an electronic
                                    pass for plane travel. You can now use Siri while you steer the wheel. Safari, social
                                    networking integrations, and photo sharing also feature new functionalities. Of
                                    all the speculations, this would most likely turn out to be true.
                                </dd>
                            </dl>
                        </dd>
                        <dt>
                            The Foresight
                        </dt>
                        <dd>
                            <dl>
                                <dt class="empty"></dt>
                                <dd>
                                    Apple seems to be breaking its long-standing traditions. From the event locations,
                                    to the product naming, to the launch dates, everything has changed. We expect the
                                    same with Apple's upcoming phone. In the end, it all makes for a sensible business
                                    strategy.
                                </dd>
                                <dt>
                                    No more number scheme
                                </dt>
                                <dd>
                                    Although everyone is calling it 'iPhone 5,' we are almost certain that the company
                                    will simply call it the 'iPhone'. Everyone was expecting the latest iPad model to
                                    be dubbed as iPad 3. Apple, however, surprised us by dropping the number scheme.
                                    The next-generation iPhone will most likely seal its iconic status by simple being
                                    what it is – an iPhone.
                                </dd>
                                <dt>
                                    Arriving in autumn
                                </dt>
                                <dd>
                                    Everyone is asking when the iPhone 5 is coming. If you ask us, we think it will
                                    be in September or October. The iOS 6 Developer Beta 3 expires on September and
                                    it will be timely to introduce the new software version along with the latest handset.
                                    Apple's June-to-October transition last year translated to record sales. Autumn
                                    is always a lucrative season because of the holiday demand for a new device. We
                                    are sure Apple will not have a hard time repeating its performance from last year.
                                </dd>
                            </dl>
                        </dd>
                    </dd>
                </dl>
			</div>
        </td>
    </tr>
    <%
	end if
	%>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
    <%
    '<tr><td class="bottomText"><%=bottomText$></td></tr>
    %>
    <tr>
        <td align="center" valign="top" width="100%">
            <img src="/images/brandmodelheaders/hline_bottom.jpg" width="740" height="8" border="0">
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">&nbsp;</td>
    </tr>
    <%
	end if
    %>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->
<script language="javascript">
	ajax('/ajax/accessoryFinder.asp?hzBox=1&brandid=<%=brandID%>&selectModelid=<%=saveModelID%>', 'cbModelBox2')
	ajax('/ajax/accessoryFinder.asp?hzBox=1&modelid=<%=saveModelID%>', 'cbCatBox2')
</script>