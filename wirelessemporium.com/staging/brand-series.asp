<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
response.buffer = true
dim seriesid
seriesid = request.querystring("seriesid")
if seriesid = "" or not isNumeric(seriesid) then
	call PermanentRedirect("/")
end if

call fOpenConn()
SQL = "SELECT A.*, C.brandName, C.brandImg FROM we_ModelSeries A INNER JOIN we_brands C ON A.brandid = C.brandid WHERE A.id = '" & seriesid & "'"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
end if

dim modelSeriesName, modelSeriesIDs, brandName, brandID, brandImg
modelSeriesName = RS("modelSeriesName")
modelSeriesIDs = RS("modelSeriesIDs")
brandName = RS("brandName")
brandID = RS("brandID")
brandImg = RS("brandImg")

SQL = "SELECT DISTINCT A.*, C.* FROM (we_models A INNER JOIN we_items B ON A.modelID = B.modelID)"
SQL = SQL & " INNER JOIN we_brands C ON A.brandid = C.brandid"
SQL = SQL & " WHERE A.modelid IN (" & modelSeriesIDs & ")"
SQL = SQL & " AND B.hideLive = 0 AND patindex('%All %',A.modelname) = 0"
SQL = SQL & " AND B.inv_qty <> 0"
if seriesid = 9 then
	SQL = SQL & " ORDER BY A.modelname"
else
	SQL = SQL & " ORDER BY A.temp DESC"
end if
'session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
end if

dim SEtitle, SEdescription, SEkeywords, topText, holdModelName, RStypes

select case seriesid
	case "1"
		SEtitle = "BlackBerry Curve 8330/8310/8320/8300 Accessories: Faceplates, Cases, Covers & More!"
		SEdescription = "Grab the BEST DEALS online at WirelessEmporium on BlackBerry Curve Accessories like BlackBerry Faceplates, Cases, Covers and more for your Blackberry Curve Phone!"
		SEkeywords = "blackberry curve accessories, blackberry curve 8330 accessories, blackberry curve 8330 covers, blackberry curve 8310 accessories, blackberry curve faceplates, blackberry curve cases"
		topText = "Wireless Emporium is the leading name in <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""BlackBerry Cell Phone Accessories"">cell phone accessories</a>. We specialize in offering quality products for Blackberry Curve smartphones, including the Blackberry Curve 8300 series and Blackberry Curve 8900. We are constantly expanding our selection and offering the very best and latest in Blackberry Curve chargers, Blackberry Curve faceplates, and Blackberry Curve batteries. Since 2001, Wireless Emporium has made finding the right Blackberry Curve phone accessories at the right price easy. You won't find a better selection of accessories at better prices anywhere online or at your local retailer."
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Thousands of customers have found the Blackberry Curve cell phone accessories they need at Wireless Emporium. Our reputation for cost-plus pricing, easy ordering, and fast FREE shipping on all our <a class=""topText-link"" href=""http://www.wirelessemporium.com/blackberry-cell-phone-accessories.asp"" title=""BlackBerry Curve Accessories"">Blackberry Accessories</a> is unmatched in the industry. When you want the latest and greatest in accessories, you'll simply find no better value than at Wireless Emporium. With hundreds of Blackberry Curve faceplates and multiple options for Blackberry Curve cases, this is the only place you'll have to visit to get everything you need for your phone."
	case "2"
		SEtitle = "Motorola Razr Cell Phone Accessories, Motorola Razr Antennas, Batteries, Chargers, Faceplates, Belt Clip & Holsters"
		SEdescription = "Motorola Razr Cell Phone Accessories - Choose from our extensive selection of cell phone accessories for your Motorola Razr cell phone which includes antennas, batteries, chargers, faceplates, belt clips, holsters and more."
		topText = "Wireless Emporium is the leading name in cell phone accessories. We specialize in offering quality products for Motorola RAZR cell phones, including the Motorola V3 RAZR and Motorola RAZR V3m. We are constantly expanding our selection and offering the very best and latest in Motorola RAZR chargers, Motorola RAZR faceplates, and Motorola RAZR batteries. Since 2001, Wireless Emporium has made finding the right Motorola RAZR phone accessories at the right price easy. You won't find a better selection of accessories at better prices anywhere online or at your local retailer."
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Thousands of customers have found the Motorola RAZR cell phone accessories they need at Wireless Emporium. Our reputation for cost-plus pricing, easy ordering, and fast FREE shipping on all our cell phone accessories is unmatched in the industry. When you want the latest and greatest in accessories, you'll simply find no better value than at Wireless Emporium. With hundreds of Motorola RAZR faceplates and multiple options for Motorola RAZR cases, this is the only place you'll have to visit to get everything you need for your phone."
	case "3"
		SEtitle = "Discount Motorola KRZR Cell Phone Accessories, Batteries, Chargers, Data Cable, Leather Cases - Wireless Emporium"
		SEdescription = "Motorola KRZR Cell Phone Accessories - Get the right Motorola KRZR accessories at Wireless Emporium. Buy Motorola batteries, chargers, data cable, leather cases and hands-free at discount prices."
		topText = "Wireless Emporium is the leading name in cell phone accessories. We specialize in offering quality products for Motorola KRZR cell phones, including the Motorola KRZR K1 and Motorola KRZR K1m. We are constantly expanding our selection and offering the very best and latest in Motorola KRZR chargers, Motorola KRZR faceplates, and Motorola KRZR batteries. Since 2001, Wireless Emporium has made finding the right Motorola KRZR phone accessories at the right price easy. You won't find a better selection of accessories at better prices anywhere online or at your local retailer."
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Thousands of customers have found the Motorola KRZR cell phone accessories they need at Wireless Emporium. Our reputation for cost-plus pricing, easy ordering, and fast FREE shipping on all our cell phone accessories is unmatched in the industry. When you want the latest and greatest in accessories, you'll simply find no better value than at Wireless Emporium. With hundreds of Motorola KRZR faceplates and multiple options for Motorola KRZR cases, this is the only place you'll have to visit to get everything you need for your phone."
	case "4"
		SEtitle = "Discount LG enV Cell Phone Accessories, LG enV Batteries, LG enV Chargers, LG enV Belt Clips, LG enV Leather Cases & Hands Free"
		SEdescription = "LG enV Cell Phone Accessories - Wireless Emporium is the #1 source for factory-direct LG enV cell phone accessories. We offer high quality LG enV batteries, chargers, belt clips, leather cases and more at competitive rates!"
		topText = "Wireless Emporium is the leading name in cell phone accessories. We specialize in offering quality products for LG enV cell phones, including the LG enV Touch VX11000 and LG env3 VX9200. We are constantly expanding our selection and offering the very best and latest in LG enV chargers, LG enV faceplates, and LG enV batteries. Since 2001, Wireless Emporium has made finding the right LG enV phone accessories at the right price easy. You won't find a better selection of accessories at better prices anywhere online or at your local retailer."
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Thousands of customers have found the LG enV cell phone accessories they need at Wireless Emporium. Our reputation for cost-plus pricing, easy ordering, and fast FREE shipping on all our cell phone accessories is unmatched in the industry. When you want the latest and greatest in accessories, you'll simply find no better value than at Wireless Emporium. With hundreds of LG enV faceplates and multiple options for LG enV cases, this is the only place you'll have to visit to get everything you need for your phone."
	case "5"
		SEtitle = "Discount LG Rumor Cell Phone Accessories, LG Rumor Batteries, LG Rumor Chargers, Data Cable, Leather Cases, Hands Free ?Wireless Emporium"
		SEdescription = "LG Rumor Cell Phone Accessories - Wireless Emporium is the #1 source for factory direct LG Rumor Accessories like LG Rumor faceplates & LG Rumor covers at Discounted Rates!"
		topText = "Wireless Emporium is the leading name in cell phone accessories. We specialize in offering quality products for LG Rumor cell phones, including the LG Rumor LX260 and LG Rumor2 LX265. We are constantly expanding our selection and offering the very best and latest in LG Rumor chargers, LG Rumor faceplates, and LG Rumor batteries. Since 2001, Wireless Emporium has made finding the right LG Rumor phone accessories at the right price easy. You won't find a better selection of accessories at better prices anywhere online or at your local retailer."
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Thousands of customers have found the LG Rumor cell phone accessories they need at Wireless Emporium. Our reputation for cost-plus pricing, easy ordering, and fast FREE shipping on all our cell phone accessories is unmatched in the industry. When you want the latest and greatest in accessories, you'll simply find no better value than at Wireless Emporium. With hundreds of LG Rumor faceplates and multiple options for LG Rumor cases, this is the only place you'll have to visit to get everything you need for your phone."
	case "6"
		SEtitle = "Discount LG Chocolate Cell Phone Accessories, Mobile Phone Batteries, Chargers, Faceplate, Holsters / Belt Clips, Leather Cases, Bluetooth Hands-Free Headsets"
		SEdescription = "LG Chocolate Cell Phone Accessories - Wireless Emporium is the #1 source for factory-direct LG Chocolate Cell Phone Accessories. Buy batteries, chargers, holsters/ belt clips, leather cases & Bluetooth hands-free headsets for LG cell phones @ discounted rates!"
		topText = "Wireless Emporium is the leading name in cell phone accessories. We specialize in offering quality products for LG Chocolate cell phones, including the LG Chocolate 3 VX8560 and LG Chocolate Touch VX8575. We are constantly expanding our selection and offering the very best and latest in LG Chocolate chargers, LG Chocolate faceplates, and LG Chocolate batteries. Since 2001, Wireless Emporium has made finding the right LG Chocolate phone accessories at the right price easy. You won't find a better selection of accessories at better prices anywhere online or at your local retailer."
		topText = topText & "</p><p style=""margin-top:0px;margin-bottom:6px;"">Thousands of customers have found the LG Chocolate cell phone accessories they need at Wireless Emporium. Our reputation for cost-plus pricing, easy ordering, and fast FREE shipping on all our cell phone accessories is unmatched in the industry. When you want the latest and greatest in accessories, you'll simply find no better value than at Wireless Emporium. With hundreds of LG Chocolate faceplates and multiple options for LG Chocolate cases, this is the only place you'll have to visit to get everything you need for your phone."
	case "9"
		SEtitle = "Android Cell Phone Accessories: Charger, Faceplates, Cases, & More!"
		SEdescription = "Get the best deals on Android accessories from Wireless Emporium. Find Android charger, Android screen protectors, Android cases, and more!"
		SEkeywords = "Android accessories, android covers, android screen protectors, android chargers, android batteries, android data cables, android cell phones"
		topText = "You've heard all about it: the Android operating system is here and it's taking the cell phone community by storm. The world's first open source mobile OS is all about functionality. Whether you've picked up the Google Nexus One or the HTC Droid Incredible for Verizon, Android users have the world at their fingertips with a huge catalog of thousands of applications, and the options will only continue to grow."
		topText = topText & "</p><p style=""color:#FFFFFF;margin-top:0px;margin-bottom:6px;"">Regardless of which Android phone you have, you're going to need accessories. Protect your phone's touch screen with screen protectors made specifically for your device. Find <a class=""topText-link"" href=""http://wirelessemporium.com/sb-5-sm-876-sc-7-cases-pouches-skins-motorola-droid-a855.asp"" title=""Motorola Droid Cases"">Motorola Droid cases</a> and <a class=""topText-link"" href=""http://wirelessemporium.com/sb-20-sm-806-sc-3-faceplates-screen-protectors-htc-t-mobile-mytouch-3g-magic.asp"" title=""HTC myTouch 3G Faceplates"">HTC myTouch 3G faceplates</a> at the lowest prices online."
		topText = topText & " Find everything you need including batteries, belt clips, data cables, chargers, and even the cell phones themselves ?all in one convenient place. Getting cell phone accessories has never been easier when you order from Wireless Emporium! Order today and have your Android accessories shipped to you for free!"
end select

if SEkeywords = "" then
	SEkeywords = brandName & " accessories, " & brandName & " " & modelSeriesName & " accessories, " & brandName & " " & modelSeriesName & " car charger, " & brandName & " " & modelSeriesName & " faceplates, " & brandName & " " & modelSeriesName & " cases, "
	holdModelName = "99999999"
	do until RS.eof
		if RS("modelName") <> holdModelName then
			SEkeywords = SEkeywords & brandName & " " & RS("modelName") & " accessories, "
			SEkeywords = SEkeywords & brandName & " " & modelSeriesName & " " & RS("modelName") & " accessories, "
			holdModelName = RS("modelName")
		end if
		RS.movenext
	loop
	SEkeywords = left(SEkeywords,len(SEkeywords)-2)
	RS.movefirst
end if

dim strBreadcrumb
if seriesid = 9 then
	strBreadcrumb = "Android Accessories"
else
	strBreadcrumb = brandName & " " & modelSeriesName & " Accessories"
end if
%>
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <style>
                h1 {
                    font-size: 18pt;
                    color: #000000;
                    font-weight: normal;
                    font-family: Tahoma, Trebuchet MS, Arial, Helvetica, sans-serif;
                    margin-bottom:0px;
                }
            </style>
            <h1>
                <%
                if seriesid = 9 then
                    response.write "Android Cell Phone Accessories" & vbcrlf
                else
                    response.write brandName & " " & modelSeriesName & " Accessories" & vbcrlf
                end if
                %>
            </h1>
        </td>
    </tr>
    <tr>
        <%
        if seriesid = 9 then
            %>
            <td width="100%" height="215" align="left" valign="top" style="background-image: url('/images/android_banner.jpg'); background-repeat: no-repeat; background-width:740; background-height:200; background-position: center top;">
                <table border="0" cellspacing="0" cellpadding="10" width="500">
                    <tr>
                        <td align="left" valign="top" width="100%" class="topText">
                            <p style="color:#FFFFFF;margin-top:0px;margin-bottom:6px;"><%=topText%></p>
                        </td>
                    </tr>
                </table>
            </td>
            <%
        else
            %>
            <td align="left" valign="top">
                <p class="topText" style="margin-top:0px;margin-bottom:6px;"><%=topText%></p>
            </td>
            <%
        end if
        %>
    </tr>
    <tr>
        <td align="left" valign="top">
            <img src="/images/spacer.gif" width="1" height="10" border="0">
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%">
            <form>
                Please Select Your Phone Model&nbsp;&nbsp;&nbsp;
                <select name="selectModel" onChange="if(this.value != ''){window.location=this.value;}">
                    <option value="">Select from this list or click on an image below</option>
                    <%
                    do until RS.eof
                        if RS("modelID") = 940 then
                            response.write "<option value=""/apple-ipad-accessories.asp"">" & RS("modelName") & "</option>" & vbcrlf
                        else
                            response.write "<option value=""/T-" & RS("modelID") & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(RS("modelName")) & ".asp"">" & RS("modelName") & "</option>" & vbcrlf
                        end if
                        RS.movenext
                    loop
                    RS.movefirst
                    %>
                </select>
            </form>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%">
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <%
                    a = 0
                    do until RS.eof
                        modelID = RS("modelID")
                        modelName = RS("modelName")
                        modelImg = RS("modelImg")
                        %>
                        <td align="center" valign="top" width="150">
                            <table border="0" cellspacing="0" cellpadding="0" width="150">
                                <tr>
                                    <td align="center" valign="top" width="150">
                                        <table border="0" cellspacing="0" cellpadding="0" width="150">
                                            <tr>
                                                <td align="center" valign="middle">
                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td align="left" valign="bottom" height="18" colspan="2"><img src="/images/spacer.gif" width="10" height="1" border="0"></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" valign="top" height="150" width="100">
                                                                <%
                                                                if modelID = 940 then
                                                                    %>
                                                                    <a href="/apple-ipad-accessories.asp"><img src="/productPics/models/<%=modelImg%>" border="0" alt="<%=altText%> Cell Phone Accessories"></a>
                                                                    <%
                                                                else
                                                                    %>
                                                                    <a href="/T-<%=modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp"><img src="/productPics/models/<%=modelImg%>" border="0" alt="<%=altText%> Accessories" width="80"></a>
                                                                    <%
                                                                end if
                                                                %>
                                                            </td>
                                                            <td align="center" valign="top" width="5"><img src="/images/spacer.gif" width="5" height="1" border="0"></td>
                                                            <td align="center" valign="top" width="95">
                                                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                    <tr>
                                                                        <td align="left" valign="top" class="cellphone-category-font">
                                                                            <%
                                                                            if modelID = 940 then
                                                                                %>
                                                                                <a class="cellphone-font" href="/apple-ipad-accessories.asp" title="<%=altText%> Accessories"><%=replace(modelName,"/"," / ")%></a>
                                                                                <%
                                                                            else
                                                                                %>
                                                                                <a class="cellphone-font" href="/T-<%=modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>.asp" title="<%=altText%> Accessories"><%=replace(modelName,"/"," / ")%></a>
                                                                                <%
                                                                            end if
                                                                            %>
                                                                            <br>
                                                                            <%
                                                                            SQL = "SELECT DISTINCT A.typeid, A.brandid, B.typename, C.brandName FROM we_items A INNER JOIN we_types B ON A.typeid = B.typeid INNER JOIN we_brands C ON A.brandid = C.brandid WHERE A.modelid = " & modelID & " AND A.inv_qty <> 0 AND A.hideLive = 0"
                                                                            set RStypes = Server.CreateObject("ADODB.Recordset")
                                                                            RStypes.open SQL, oConn, 0, 1
                                                                            dim aCount
                                                                            aCount = 0
                                                                            do until RStypes.eof
                                                                                select case RStypes("brandID")
                                                                                    case "4" : altText = "LG Accessories: LG " & modelName & " Accessories"
                                                                                    case "9" : altText = "Samsung Cell Phone Accessories: Samsung " & modelName & " Accessories"
                                                                                    case "17" : altText = modelName
                                                                                    case "18" : altText = "Pantech Accessories: Pantech " & modelName & " Accessories"
                                                                                    case "20" : altText = "HTC Accessories: HTC " & modelName & " Accessories"
                                                                                    case else : altText = RStypes("brandName") & " " & modelName
                                                                                end select
                                                                                if RStypes("typeid") <> 17 then
                                                                                    if aCount = 0 then
                                                                                        response.write "<a class=""cellphone-category-font"" href=""/hf-sm-" & modelID & "-" & formatSEO(RStypes("brandName")) & "-" & formatSEO(modelName) & "-bluetooth-headsets-headphones.asp"" title=""Hands-Free Kits &amp; Bluetooth Headsets for " & altText & """>Handsfree</a><br>" & vbcrlf
                                                                                    end if
                                                                                    aCount = aCount + 1
                                                                                    response.write "<a class=""cellphone-category-font"" href=""/sb-" & RStypes("brandID") & "-sm-" & modelID & "-sc-" & RStypes("typeid") & "-" & formatSEO(RStypes("typename")) & "-" & formatSEO(RStypes("brandname")) & "-" & formatSEO(modelName) & ".asp"" title=""" & nameSEO(RStypes("typename")) & " for " & altText & """>" & RStypes("typename") & "</a><br>" & vbcrlf
                                                                                end if
                                                                                RStypes.movenext
                                                                            loop
                                                                            %>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="middle"><img src="/images/spacer.gif" width="1" height="6" border="0"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <%
                        RS.movenext
                        a = a + 1
                        if a = 4 then
                            response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""4""><br><img src=""/images/grayline_800.jpg"" width=""748"" height=""2"" border=""0""><br><br></td></tr><tr>" & vbcrlf
                            a = 0
                        end if
                    loop
                    if a = 1 then
                        response.write "<td width=""150"">&nbsp;</td><td width=""150"">&nbsp;</td><td width=""150"">&nbsp;</td>" & vbcrlf
                    elseif a = 2 then
                        response.write "<td width=""150"">&nbsp;</td><td width=""150"">&nbsp;</td>" & vbcrlf
                    elseif a = 3 then
                        response.write "<td width=""150"">&nbsp;</td>" & vbcrlf
                    end if
                    %>
                </tr>
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%" colspan="4">
            <br><img src="/images/orangeline_800.jpg" width="748" height="2" border="0"><br><br>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
    <tr>
        <td align="left" valign="top">&nbsp;</td>
    </tr>
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->