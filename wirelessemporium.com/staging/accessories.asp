<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
response.buffer = true
dim brandid
brandid = request.querystring("brandid")
if brandid = "" or not isNumeric(brandid) then
	call PermanentRedirect("/")
end if

call fOpenConn()
sql = "select top 50 a.price_our, a.price_retail, a.itemID, a.itemDesc, a.itemPic, a.modelID, b.brandName, b.brandImg from we_items a left join we_brands b on a.brandID = b.brandID where a.typeID <> 16 and a.brandID = " & brandID & " and a.inv_qty <> 0 and a.price_our is not null and a.price_our > 0 order by a.numberOfSales desc"
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
rowCnt = rs.recordcount
if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
end if

dim brandName, brandImg
brandName = RS("brandName")
brandImg = RS("brandImg")

dim SEtitle, SEdescription, SEkeywords
SEtitle = "Top " & brandName & " Accessories"
SEdescription = "WirelessEmporium.com is the #1 store online for " & brandName & " Cell Phone Accessories, offering the best deals on " & brandName & " chargers, batteries, faceplates, cases & much more."
SEkeywords = brandName & " cell phone accessories, " & brandName & " accessories, " & brandName & " cell phone accessory, " & brandName & " cases, " & brandName & " batteries, " & brandName & " chargers"

dim oRsTypes
%>
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal">
            <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;
            <span class="top-sublink-pink"><%=brandName%> Cell Phone Accessories</span>
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle" width="100%">
            <h1><%=SEtitle%></h1>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" width="100%">
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <%
                do while not rs.EOF
                %>
                <tr>
                    <%
                    for i = 0 to 3
                        if rs.EOF then
                    %>
                    <td style="border-top:1px solid #cccccc;">&nbsp;</td>
                    <%
                        else
                            modelID = rs("modelID")
                    %>
                    <td align="center" style="<% if i < 3 then %>border-right:1px dashed #cccccc; <% end if %>border-top:1px solid #cccccc;">
                        <table border="0" cellspacing="0" cellpadding="0" width="170" height="100%">
                            <tr>
                                <td align="center" valign="middle" width="170" height="154"><a href="/p-<%=rs("itemID")%>-<%=formatSEO(rs("itemDesc"))%>.asp"><img src="/productpics/thumb/<%=rs("itemPic")%>" border="0" alt="<%=altText%>"></a></td>
                            </tr>
                            <tr><td align="center" valign="top" height="5"><img src="/images/spacer.gif" width="1" height="5"></td></tr>
                            <tr>
                                <td align="center" valign="top"><a class="cellphone-link" href="/p-<%=rs("itemID")%>-<%=formatSEO(rs("itemDesc"))%>.asp" title="<%=altText%>"><%=rs("itemDesc")%></a></td>
                            </tr>
                            <tr>
                                <td align="center" valign="bottom">
                                    <span class="boldText">Our&nbsp;Price:&nbsp;</span><span class="pricing-orange2"><%=formatCurrency(rs("price_Our"))%>&nbsp;</span><br>
                                    <span class="pricing-gray2">List&nbsp;Price:&nbsp;<s><%=formatCurrency(rs("price_Retail"))%></s></span><br>
                                </td>
                            </tr>

                        </table>
                    </td>
                    <%
                            rs.movenext
                        end if
                    next
                    %>
                <% loop	%>
            </table>
        </td>
    </tr>
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->