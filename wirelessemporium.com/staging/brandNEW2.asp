<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_301Redirect.asp"-->
<%
	response.buffer = true
	response.write "!"
	response.end
	pageName = "brandNew2"
	brandid = ds(request.querystring("brandid"))
	phoneOnly = ds(request.QueryString("phoneOnly"))
	showIntPhones = ds(request.Form("showIntPhones"))
	cbModel = ds(request("cbModel"))
	cbSort = ds(request("cbSort"))
	cbCarrier = ds(request("cbCarrier"))
	viewPage = ds(request.form("viewPage"))
	maxModels = ds(request.form("maxModels"))
	if not isnull(session("qSort")) then
		qSort = session("qSort")
		session("qSort") = null
	end if
	stitchImgPath = "/images/stitch/brands/"
	
	response.Cookies("saveBrandID") = brandID
	response.Cookies("saveModelID") = 0
	response.Cookies("saveItemID") = 0

	noSort = 0
	if brandid = "" or not isNumeric(brandid) then call PermanentRedirect("/")
	if phoneOnly = "" or not isNumeric(phoneOnly) then phoneOnly = 0
	if isnull(qSort) or len(qSort) < 1 then qSort = ""
	if qSort <> "" then cbSort = qSort
	if isnull(showIntPhones) or len(showIntPhones) < 1 then showIntPhones = 0
	if isnull(cbSort) or len(cbSort) < 1 then
		noSort = 1
		cbSort = "AZ"
	end if
	if isnull(viewPage) or len(viewPage) < 1 then viewPage = 1
	if isnull(maxModels) or len(maxModels) < 1 then maxModels = 40

	leftGoogleAd = 1
	displayIntLink = 0
	displayHideIntLink = 0
	googleAds = 1
	lap = 0
	sqlOrder = " order by orderNum desc, oldModel, international, modelname"
	if viewPage = 1 then showPopPhones = 1 else showPopPhones = 0
	
	maxModels = 4000
	showPopPhones = 1
	
	'=========================================================================================
	Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
		oParam.CompareMode = vbTextCompare
		oParam.Add "x_brandID", brandID
		oParam.Add "x_phoneOnly", phoneOnly
	call redirectURL("b", "", request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
	'=========================================================================================
	
	'Normal Page Start
	set fso = CreateObject("Scripting.FileSystemObject")
	Set Jpeg = Server.CreateObject("Persits.Jpeg")
	Jpeg.Quality = 100
	Jpeg.Interpolation = 1
	
	sql	=	"select	a.modelid, a.modelname, a.modelimg, a.topModel, a.oldModel, a.international, b.brandname, b.brandimg " & vbcrlf & _
			"	,	cast(case when a.topModel <> 1 and a.oldModel <> 1 and a.international <> 1 then 1 else 0 end as bit) displayModel, (select count(*) from we_models where brandID = " & brandID & " and (oldModel = 1 or international = 1) and isTablet = 0) as otherModels, a.linkName " & vbcrlf & _
			"from 	we_models a with (nolock) join we_brands b with (nolock) " & vbcrlf & _
			"	on 	a.brandid = b.brandid " & vbcrlf & _
			"where 	a.modelname is not null and a.hidelive = 0 and a.brandid = '" & brandid & "' " & vbcrlf & _
			"	and a.modelname not like 'all model%'" & vbcrlf
			
	if brandID = 17 and phoneOnly = 1 then
		brandName = "iPhone"
		cellPhoneTitle = "Cell Phone"
		SQL = SQL & " and a.modelName not like '%iPod%' and a.modelName not like '%iPad%'" & vbcrlf
	elseif brandID = 17 and phoneOnly = 0 then
		brandName = "iPod/iPad"
		phoneTitle = "Product"
		cellPhoneTitle = ""
		SQL = SQL & " and a.modelName not like '%iPhone%'" & vbcrlf
	else
		cellPhoneTitle = "Cell Phone"
		SQL = SQL & " and a.isTablet = 0" & vbcrlf
	end if
	
	if cbCarrier <> "" then
		fileExt = "_" & cbCarrier
		showPopPhones = 0
		sql	= sql & " and a.carriercode like '%' + '" & cbCarrier & "' + ',%'" & vbcrlf
	end if
	
	if cbSort <> "" then
		if cbSort = "AZ" and noSort = 0 then
			fileExt = fileExt & "_" & cbSort & "S"
		else
			fileExt = fileExt & "_" & cbSort
		end if
		if noSort = 0 then showPopPhones = 0 else showPopPhones = 1
		select case ucase(cbSort)
			case "AZ"
				if noSort = 1 then
					sqlOrder = " order by topModel, oldModel, international, modelname"
				else
					sqlOrder = " order by oldModel, international, modelname"
				end if
			case "ZA"
				sqlOrder = " order by oldModel, international, modelname desc"	
			case "NO"
				sqlOrder = " order by oldModel, international, releaseyear desc, releasequarter desc, modelname"		
			case "ON"
				sqlOrder = " order by oldModel, international, releaseyear, releasequarter, modelname"
		end select
	end if
	
	sql	= sql & sqlOrder
	session("errorSQL") = SQL
'	response.write "<pre>" & sql & "</pre>"
	arrRS = getDbRows(sql)

	arrPopPhones = filter2DRS(arrRS, 3, true)
	arrOldPhones = filter2DRS(arrRS, 4, true)	
	arrIntPhones = filter2DRS(arrRS, 5, true)
	arrDisplayPhones = filter2DRS(arrRS, 8, true)
	arrSortedRS = sort2DRS(arrRS, 1)
	
	if isnull(arrRS) then response.redirect("/") end if
	
	if ubound(arrPopPhones,2) >= 0 then showPopPhones = 1 else showPopPhones = 0 end if
	if ubound(arrOldPhones,2) >= 0 then showOldPhones = 1 else showOldPhones = 0 end if
	if ubound(arrIntPhones,2) >= 0 then showIntPhones = 1 else showIntPhones = 0 end if
	
	if brandName = "" then brandName = arrDisplayPhones(6,0) end if
	
	phoneList = ""
	if not isnull(arrRS) then
		for i=0 to ubound(arrRS,2)
			if phoneList = "" then
				phoneList = """" & arrRS(1,i) & """"
			else
				phoneList = phoneList & ", """ & arrRS(1,i) & """"
			end if
		next
	end if
	
	sql	= "select * from we_carriers order by listOrder, carriername"
	session("errorSQL") = sql
	set objRsCarrier = Server.CreateObject("ADODB.Recordset")
	objRsCarrier.open sql, oConn, 0, 1

	sql = "select * from we_brandText where brandID = '" & brandID & "'"
	session("errorSQL") = SQL
	set brandTxtRS = Server.CreateObject("ADODB.Recordset")
	brandTxtRS.open SQL, oConn, 0, 1
	dim strH2: strH2=""
	if not brandTxtRS.EOF then
		SEtitle = brandTxtRS("seTitle")
		SEdescription = brandTxtRS("seDescription")
		SEkeywords = brandTxtRS("seKeywords")
		strH2 = brandTxtRS("titleText")
		if instr(strH2,"#brandName#") > 0 then strH2 = replace(brandTxtRS("titleText"),"#brandName#",brandName)
		if len(strH2) > 80 then strH2 = mid(left(strH2,80),1,instrrev(strH2," ")) 'legacy H2 truncation logic [knguyen/20110519]
		
		topText = brandTxtRS("topText")
		bottomText = brandTxtRS("bottomText")
		brandStaticText = brandTxtRS("brandStaticText")
	end if
	phoneTitle = "Phone"

	if showPopPhones = 1 then
		popItemImgPath = formatSEO(brandName) & "_pop.jpg"
	end if
	
	if maxModels > 40 then
		if brandID = 17 and phoneOnly = 1 then
			itemImgPath = "applePhones_all" & fileExt & ".jpg"
		else
			itemImgPath = formatSEO(brandName) & "_all" & fileExt & ".jpg"
			oldItemImgPath = formatSEO(brandName) & "_old" & fileExt & ".jpg"
			intItemImgPath = formatSEO(brandName) & "_int" & fileExt & ".jpg"
		end if
	else
		itemImgPath = formatSEO(brandName) & "_p" & viewPage & fileExt & ".jpg"
	end if
	
	if SEtitle = "" then SEtitle = "Cell Phone Accessories for " & brandName & ": Discount Accessories from Wireless Emporium, " & brandName & " cell phone Accessories, " & brandName & " Mobile phone Accessories, " & brandName & " Wireless phone Accessories"
	if SEdescription = "" then SEdescription = "Discount Accessories for " & brandName & " and " & brandName & " Wireless Phone Accessories at Wireless Emporium. Your source for wholesale " & brandName & " cell phone Accessories, wholesale " & brandName & " cellular Accessories, discount " & brandName & " cell phone Accessories and cheap " & brandName & " cell phone Accessories"
	if SEkeywords = "" then SEkeywords = "Discount Accessories for " & brandName & "," & brandName & " Accessories," & brandName & " cell phone Accessories," & brandName & " cell phone Accessories," & brandName & " discount cellular Accessories," & brandName & " mobile phone Accessories," & brandName & " wireless Accessories," & brandName & " cellular telephone Accessories," & brandName & " wholesale cellular Accessories," & brandName & " cellular Accessories," & brandName & " wireless phone Accessories," & brandName & " wholesale cell phone Accessories," & brandName & " cellular phone Accessories,wholesale " & brandName & " wireless Accessories,wholesale " & brandName & " cellular phone Accessories,discount " & brandName & " cell phone Accessories,cool " & brandName & " cell phone Accessories"
	if strH2 = "" then strH2 = "Discount " & brandName & " Cell Phone Chargers, Batteries &amp; Other " & brandName & " Accessories"
	
	dim strBreadcrumb
	if brandID = 17 and phoneOnly = 0 then
		strBreadcrumb = "iPod/iPad Accessories"
	elseif brandID = 17 and phoneOnly = 1 then
		strBreadcrumb = "iPhone Accessories"	
	else
		strBreadcrumb = brandName & " Accessories"
	end if

		
	'todo: need to consolidate Metatags with we_XXXText
	dim strH1, strAltText
	strH1=brandName&" Accessories"
	'set input values to extract page meta-values
	dim dicReplaceAttribute: set dicReplaceAttribute = CreateObject("Scripting.Dictionary")
	dicReplaceAttribute( "BrandName") = brandName
	
	'set known product attributes to replace out template placeholders
	dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
	dicSeoAttributeInput( "BrandId") = brandID
	dicSeoAttributeInput( "IsPhone") = phoneOnly

	'??? this logic is taken from earlier revision that was hardcoded and overwrote the original database look up [knguyen/20110519]
	SEtitle = brandName & " Cell Phone Accessories - " & brandName & " Cell Phone Covers Cases Batteries And More"

	'legacy rule-based logic for H1
	if brandName = "Apple" then
		if phoneOnly = 0 then strH1="iPod/iPad Accessories" else strH1="iPhone Accessories" end if
	else
		strH1=brandName & " Accessories"
	end if

	'legacy rule-based logic for H2
	if brandName = "Apple" then
		if phoneOnly = 0 then strH1="iPod/iPad Accessories" else strH1="iPhone Accessories" end if
	else
		strH1=brandName & " Accessories"
	end if
	
	call LookupSeoAttributes()

	
	if showPopPhones = 1 then
		imgStitchPath = Server.MapPath("/images/stitch/brands/" & popItemImgPath)
		call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrPopPhones, 2)	
	end if
	if showOldPhones = 1 then
		imgStitchPath = Server.MapPath("/images/stitch/brands/" & oldItemImgPath)
		call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrOldPhones, 2)		
	end if
	if showIntPhones = 1 then
		imgStitchPath = Server.MapPath("/images/stitch/brands/" & intItemImgPath)
		call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrIntPhones, 2)			
	end if

	imgStitchPath = Server.MapPath("/images/stitch/brands/" & itemImgPath)
	call createStitchByArray(70, 112, server.MapPath("/productPics/models/thumbs/"), server.MapPath("/productPics/models/"), imgStitchPath, arrDisplayPhones, 2)
	
	session("breadcrumb_model") = ""
	
	if not fso.fileExists(server.MapPath("/images/brandheaders/we_brand_header_" & brandid & "_small.jpg")) then
		Jpeg.Open server.MapPath("/images/brandheaders/we_brand_header_" & brandid & ".jpg")
		Jpeg.Width = 100
        Jpeg.Height = Jpeg.OriginalHeight * 100 / Jpeg.OriginalWidth
		Jpeg.Save server.MapPath("/images/brandheaders/we_brand_header_" & brandid & "_small.jpg")
	end if
%>
<!--#include virtual="/includes/template/top_brand.asp"-->
<script>
window.WEDATA.pageType = 'brand';
window.WEDATA.pageData = {
	brand: '<%= brandName %>',
	brandId: '<%= brandId %>'
};
</script>

<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
	<tr>
    	<td class="breadcrumbFinal">
        	<a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
        </td>
    </tr>
    <tr>
        <td width="100%">
        	<div style="float:left">
            	<div><h1 class="brand"><%=strH1%></h1></div>
                <div><h2 class="brand"><%=strH2%></h2></div>
            </div>
            <div style="float:right;"><img src="/images/brandheaders/we_brand_header_<%=brandid%>_small.jpg" border="0" /></div>
        </td>
    </tr>
    <tr>
        <td width="100%">
			<form name="topSearchForm" method="post">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="background:url(/images/brands/sort-by-background-middle.jpg) repeat-x;">
            	<tr>
                	<td align="left"><img src="/images/brands/sort-by-background-left.jpg" border="0" /></td>
                    <td align="center" style="padding:3px 0px 0px 10px;">
                        <select name="cbModel" onChange="if(this.value != ''){window.location=this.value;}">
                            <option value="">Search By Phone Name</option>
                            <%
                            for nRows=0 to ubound(arrSortedRS, 2)
                                if 940 = cint(arrSortedRS(0,nRows)) then
                                %>
                                <option value="/apple-ipad-accessories.asp"><%=arrSortedRS(1,nRows)%></option>                                                                        
                                <%
                                else
                                %>
                                <option value="/T-<%=arrSortedRS(0,nRows)%>-cell-accessories-<%=formatSEO(brandName)%>-<%=formatSEO(arrSortedRS(1,nRows))%>.asp"><%=arrSortedRS(1,nRows)%></option>                                    
                                <%
                                end if
                            next
                            %>
                        </select>
                    </td>
                    <td align="center"><div style="font-size:14px; font-weight:bold; color:#666;">-&nbsp;OR&nbsp;-</div></td>
                    <td align="center" style="padding-top:3px;">
                    	<div style="position:relative;">
                        	<input type="text" name="txtSearch" value="Search" onclick="searchHelpSelect()" onkeyup="searchHelp(this.value)" onblur="searchHelpHide()" />
                            <div style="position:absolute; bottom:0px; right:0px;" id="searchHelpBox"></div>
                        </div>
                    </td>
                    <td align="right"><a href=""><img src="/images/brands/sort-by-background-right.jpg" border="0" onclick="jumpto();return false;" /></a></td>
                </tr>
			</table>
			</form>            
        </td>
	</tr>
    <%
	if showPopPhones = 1 then
	%>
    <tr>
        <td width="100%" style="padding-top:5px;">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="background:url(/images/brands/gray-header-right.jpg) repeat-x;">
            	<tr>
                	<td style="background:url(/images/brands/gray-header-left.jpg) no-repeat; width:330px; height:30px; color:#fff; font-size:14px; padding:4px 0px 0px 10px;" align="left" valign="top">
                    	TOP <%=ucase(replace(brandName, "/", " / ") & " " & cellPhoneTitle)%> MODELS
					</td>
                </tr>
			</table>
        </td>
	</tr>
    <tr>
        <td width="100%">
			<%=drawStitchModelBoxByArray(arrPopPhones, popItemImgPath)%>
        </td>
	</tr>
    <%
	end if
	%>
    <tr>
        <td width="100%" style="padding-top:15px;">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="background:url(/images/brands/gray-header-right.jpg) repeat-x;">
            	<tr>
                	<td style="background:url(/images/brands/gray-header-left.jpg) no-repeat; width:330px; height:30px; color:#fff; font-size:14px; padding:4px 0px 0px 10px;" align="left" valign="top">
                    	<% if showPopPhones = 1 and brandid <> 15 then %>
                        OTHER <%=ucase(replace(brandName, "/", " / ") & " " & cellPhoneTitle)%> MODELS
						<% else %>
                        <%=ucase(replace(brandName, "/", " / ") & " " & cellPhoneTitle)%> MODELS
                        <% end if %>
					</td>
                </tr>
			</table>
        </td>
	</tr>
    <tr>
        <td width="100%">
			<%=drawStitchModelBoxByArray(arrDisplayPhones, itemImgPath)%>
        </td>
	</tr>
    <%
	if showOldPhones = 1 then
	%>
    <tr>
        <td width="100%" style="padding-top:5px; cursor:pointer;" onclick="toggleModels('olderModels','id_bottom_arrow_old');" title="CLICK TO VIEW MORE <%=ucase(brandName)%> MODELS">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
            	<tr>
                	<td width="10px"><img src="/images/brands/bottom-header-left.jpg" border="0" style="cursor:pointer;" /></a>
					</td>
                    <td style="background:url(/images/brands/bottom-header-center.jpg) repeat-x; padding-top:10px; font-size:18px; font-weight:bold; color:#494949;" align="right" valign="top" width="*">
						CLICK TO VIEW MORE <%=ucase(brandName)%> MODELS 
                    </td>
                    <td style="background:url(/images/brands/bottom-header-center.jpg) repeat-x; width:180px; padding-left:10px;" align="left" valign="top">
                    	<img id="id_bottom_arrow_old" src="/images/brands/down-arrow.jpg" border="0" style="margin-top:10px; cursor:pointer;" />
                    </td>
                    <td width="10px"><img src="/images/brands/bottom-header-right.jpg" border="0" style="cursor:pointer;" /></td>
                </tr>
			</table>
        </td>
	</tr>
    <tr>
        <td width="100%">
			<div id="olderModels" style="display:none;">
				<%=drawStitchModelBoxByArray(arrOldPhones, oldItemImgPath)%>
            </div>
        </td>
	</tr>
    <%
	end if

	if showIntPhones = 1 then
	%>
    <tr>
        <td width="100%" style="padding-top:5px; cursor:pointer;" onclick="toggleModels('intModels','id_bottom_arrow_int');" title="CLICK TO VIEW MORE <%=ucase(brandName)%> MODELS">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
            	<tr>
                	<td width="10px"><img src="/images/brands/bottom-header-left.jpg" border="0" style="cursor:pointer;" /></a>
					</td>
                    <td style="background:url(/images/brands/bottom-header-center.jpg) repeat-x; padding-top:11px; font-size:18px; font-weight:bold; color:#494949;" align="right" valign="top" width="*">
						CLICK TO VIEW INTERNATIONAL <%=ucase(brandName)%> MODELS 
                    </td>
                    <td style="background:url(/images/brands/bottom-header-center.jpg) repeat-x; width:140px; padding-left:10px;" align="left" valign="top">
                    	<img id="id_bottom_arrow_int" src="/images/brands/down-arrow.jpg" border="0" style="margin-top:10px; cursor:pointer;" />
                    </td>
                    <td width="10px"><img src="/images/brands/bottom-header-right.jpg" border="0" style="cursor:pointer;" /></td>
                </tr>
			</table>
        </td>
	</tr>
    <tr>
        <td width="100%">
			<div id="intModels" style="display:none;">
				<%=drawStitchModelBoxByArray(arrIntPhones, intItemImgPath)%>
            </div>
        </td>
	</tr>
    <%
	end if
	%>        
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
	<!--#include virtual="/includes/asp/inc_SEOtext_brand.asp"-->
    <!--#include virtual="/includes/asp/inc_SEOtext_store.asp"-->
    
    <tr>
    	<td valign="top" width="100%" align="left">
            <div class="graybox-middle" style="width:726px; border-left:1px solid #999; border-right:1px solid #999; padding:0px 10px 0px 10px;">
                <div class="graybox-inner" title="<%=ucase(replace(brandName, "/", " / ") & " " & cellPhoneTitle & " Accessories")%>"><%=ucase(replace(brandName, "/", " / ") & " " & cellPhoneTitle & " Accessories")%></div>
            </div>
        </td>
    </tr>
    <tr><td align="left" valign="top" width="100%" class="bottomText" style="padding-top:10px;"><%=brandStaticText%></td></tr>
    <tr><td align="left" valign="top" width="100%" class="bottomText"><br /><%=replace( replace( Obj2Str( bottomText), "{StoreText}", storeText), "{BrandText}", brandText)%></td></tr>
    <% if topText <> "" then %>
    <tr>
    	<td class="bottomText" style="padding-top:10px;"><%=topText%></td>
    </tr>
    <% end if %>
</table>
<!--#include virtual="/includes/template/bottom_brand.asp"-->
<script>
	function jumpto()
	{
		modelname = document.topSearchForm.txtSearch.value;
		var x=document.topSearchForm.cbModel;
		for (i=0;i<x.length;i++)
			if (x.options[i].text == modelname) window.location = x.options[i].value;
	}

	function toggleModels(which, idArrow)
	{
		toggle(which);
		
		if (document.getElementById(which).style.display == '') 
			document.getElementById(idArrow).src = "/images/brands/up-arrow.jpg"
		else 
			document.getElementById(idArrow).src = "/images/brands/down-arrow.jpg"
	}
</script>