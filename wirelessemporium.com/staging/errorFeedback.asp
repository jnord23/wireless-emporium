<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_Email_Validation.asp"-->

<html>
<head>
<META NAME="ROBOTS" CONTENT="NOINDEX">
<title>Error Feedback Form</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/includes/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>

<%
if request.form("callingURL") <> "" then
	dim cdo_to, cdo_from, cdo_subject, cdo_body
	cdo_to = "ruben@WirelessEmporium.com"
	'cdo_to = "webmaster@WirelessEmporium.com"
	cdo_from = request.form("email")
	if IsValidEmail(cdo_from) = true then
		cdo_subject = "WE Page Error Form"
		cdo_body = "<p><img src=""http://www.wirelessemporium.com/images/WElogo.gif"">" & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>URL: </b>" & request.form("callingURL") & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Error Type: </b>" & request.form("ErrorType") & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Details: </b>" & request.form("contentFeedback") & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Date/Time Sent: </b>" & now & "</p>" & vbcrlf
		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		%>
		<p><b><br><br>Thank you for contacting WirelessEmporium.com!</b></p>
		<p><b>We appreciate your taking the time to report this problem. Each error reported by our customers helps us in our efforts to make the WirelessEmporium.com website the best it can be.</b></p>
		<p><b><br><br>WirelessEmporium.com</b></p>
		<%
	else
		%>
		<p><b><br><br>The e-mail address you entered was not valid.</b></p>
		<p><b>Please <a href="javascript:history.back();">go back</a> and try again.</b></p>
		<p><b><br><br>WirelessEmporium.com</b></p>
		<%
	end if
else
	%>
	<script language="javascript1.2">
	function reset() {
		document.contactus.name.value="";
		document.contactus.email.value="";
		document.contactus.subject.value="";
		document.contactus.phonenumber.value="";
		document.contactus.product.value="";
		document.contactus.orderno.value="";
		document.contactus.question.value="";
	}
	
	function validateEmail(email) {
		var splitted = email.match("^(.+)@(.+)$");
		if (splitted == null) return false;
		if (splitted[1] != null) {
			var regexp_user=/^\"?[\w-_\.]*\"?$/;
			if (splitted[1].match(regexp_user) == null) return false;
		}
		if (splitted[2] != null) {
			var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
			if (splitted[2].match(regexp_domain) == null) {
				var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
				if (splitted[2].match(regexp_ip) == null) return false;
			}
			return true;
		}
		return false;
	}
	
	function validate() {
		var total = 0;
		for (var i=0; i < document.contactus.ErrorType.length; i++) {
			if (document.contactus.ErrorType[i].checked) {
				total = 1;
			}
		}
		if (total == 0) {
			alert("Please Select At Least One Error Type");
			return false;
		}
		if (document.contactus.email.value.length==0) {
			alert("Please Enter Email Address");
			return false;
		}
		if (!validateEmail(document.contactus.email.value)) { 
			alert("Please Enter a Valid Email Address");
			return false;
		}
		document.contactus.submit()
		return false;
	}
	</script>
	
	<div style="border:1px solid #000000; position:relative;">
		<p><img src="/images/WE_logo.jpg" width="450" height="69" border="0" alt="WirelessEmporium.com - The #1 Name in Cell Phone Accessories"></p>
		<form onSubmit="return validate();" style="padding:0px; margin:0px;" action="errorFeedback.asp" name="contactus" method="post">
			<input type="hidden" name="callingURL" value="">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="background-color:#f5f5f5;">
				<tr>
					<td colspan="3"><h2 style="display:inline">Error Type</h2></td>
				</tr>
				<tr>
					<td width="38%"><input name="ErrorType" type="checkbox" value="Product Information">&nbsp;Product&nbsp;Description</td>
					<td width="35%"><input name="ErrorType" type="checkbox" value="Product Image">&nbsp;Product&nbsp;Image</td>
					<td width="27%"><input name="ErrorType" type="checkbox" value="Broken Link">&nbsp;Broken&nbsp;Link</td>
				</tr>
				<tr>
					<td><input name="ErrorType" type="checkbox" value="Missing Info">&nbsp;Missing&nbsp;Information</td>
					<td><input name="ErrorType" type="checkbox" value="Spelling/Grammar">&nbsp;Spelling/Grammar</td>
					<td><input name="ErrorType" type="checkbox" value="Other">&nbsp;Other</td>
				</tr>
			</table>
			<p>Details:<br><textarea name="contentFeedback" style="width:415px; height:55px;"></textarea></p>
			<p>Email Address:<br><input name="email" type="text" size="65"></p>
			<p align="center"><input type="submit" name="submit" value="Click Here To Send"></p>
		</form>
	</div>
	
	<script language="javascript">
		var a = window.opener.location;
		document.contactus.callingURL.value = a;
	</script>
	<%
end if
%>

<p align="center"><a href="javascript:window.close();">Close Window</a>&nbsp;&nbsp;</p>

</body>
</html>

