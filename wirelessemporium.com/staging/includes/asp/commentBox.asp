<%
recaptcha_public_key	= "6Ld2C74SAAAAAG_CoH_enQM1_uKABFbiftwRnxyW" ' your public key
recaptcha_private_key	= "6Ld2C74SAAAAADoSV8ZEMd2SBztaSvrXBYcs59rQ" ' your private key
%>
<%''' **************** Start Comment Card ********************* %>
<form id="frmComment"  name="frmComment">
<div style="width:800px;">
    <div style="float:left; width:182px; height:200px; vertical-align:top;"><img src="/images/icons/WE_comment_image.png" alt="" border="0" width="182" height="216" /></div>
    <div style="float:left; width:576px; height:320px; background-color:#ebebeb; border:1px solid #999; padding:5px 20px 0px 20px;">
        <div style="float:left; width:576px; font-size:14px;">
            <span style="font-size:18px; font-weight:bold;">SUGGESTION BOX</span><br />
            We work hard to make sure our site offers the best shopping experience possible. If you have any 
            suggestions on how we can make things even better - whatever it is, please drop us a note!
        </div>
        <div style="float:left; width:576px; margin-top:10px;" align="left">
            <div style="float:left; width:140px; text-align:right; font-weight:bold;">Email (optional):</div>
            <div style="float:left; width:420px; text-align:left; padding-left:10px;"><input type="text" name="email" value="" size="32" /></div>
        </div>
        <div style="float:left; width:576px; margin-top:10px;" align="left">
            <div style="float:left; width:140px; text-align:right; font-weight:bold;">Phone (optional):</div>
            <div style="float:left; width:420px; text-align:left; padding-left:10px;"><input type="text" name="phone" value="" size="32" /></div>
        </div>
        <div style="float:left; width:576px; margin-top:10px;" align="left"><TEXTAREA style="width:560px; height:45px;" id="textarea1" name="textarea1"></TEXTAREA></div>
        <div style="float:left; margin-top:5px; width:440px; background-color:#fff;"><div id="div_recaptcha"></div></div>
        <div style="float:right; padding-top:40px;"><img src="/images/buttons/WE_submit.png" alt="" onclick="return checkall();" border="0" width="100" height="26" style="cursor:pointer;" /></div>
    </div>
</div>
</form>
<%''' **************** End Comment Card ********************* %>
<script type="text/javascript" src="/includes/js/recaptcha_ajax.js"></script>
<script language="javascript">
	function showRecaptcha() { 
		 Recaptcha.create("<%=recaptcha_public_key%>", 'div_recaptcha', {theme: "clean"});
	}

	(function () {
		setTimeout("showRecaptcha()",500)
	}());
	
</script>
<script language="javascript">
	<% ''' *********** For Comments Validations -- After validation it will send the request to server using with ajax%>
	var commentLoop = 0;
	var commentValue = '';
	function ajax_cb(newURL,rLoc) {
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						alert("No data available")
					}
					else if (rVal == "refresh") {
						curLoc = window.location
						window.location = curLoc
					}
					else if(document.getElementById(rLoc)) {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				}
				else {
					document.getElementById("testZone").innerHTML = newURL
					alert("Error performing action")
				}
			}
		};
		httpRequest.send(null);
	}
	
	function checkall() {	 
	  var GreForm = this.document.frmComment;
		
		if (GreForm.textarea1.value=="") {
			alert("Enter Your Comments");
			GreForm.textarea1.focus();
			return false;
		}
		<%
		useURL = request.ServerVariables("HTTP_X_REWRITE_URL")
		if useURL = "/" then useURL = "index.asp"
		%>
		useURL = escape("<%=useURL%>")
		ajax_cb('/ajax/saveCommentCard.asp?page=' + useURL + '&email=' + document.frmComment.email.value + '&phone=' + document.frmComment.phone.value + '&comment=' + escape(document.frmComment.textarea1.value) + '&re_challenge=' + Recaptcha.get_challenge() + '&re_response=' + Recaptcha.get_response() + '&re_privatekey=<%=recaptcha_private_key%>','dumpZone')
		commentValue = GreForm.textarea1.value
		GreForm.textarea1.value = ""
		
		setTimeout("chkCommentReturn()",500)
		
		return true;
	}
	
	function chkCommentReturn() {
		commentLoop++
		if (commentLoop < 5) {
			if (document.getElementById("dumpZone").innerHTML != "") {
				if (document.getElementById("dumpZone").innerHTML == "wrong recaptcha") {
					document.getElementById('textarea1').value = commentValue;
					alert('You have entered the wrong secret words.');
				}				
				else if (document.getElementById("dumpZone").innerHTML == "commentGood") {
					alert("Comment saved")
				}
				else {
					alert("Error saving comment\nPlease resubmit\n" + document.getElementById("dumpZone").innerHTML)
					document.getElementById('textarea1').value = commentValue
				}
			}
		}
		else {
			alert("Error saving comment\nPlease resubmit")
			document.getElementById('textarea1').value = commentValue
		}
	}
</script>