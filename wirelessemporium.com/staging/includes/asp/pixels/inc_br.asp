<%
dim br_acctID : br_acctID = 5159
dim br_ptype	'"<search, category, product, thematic, other>"
dim br_catid : br_catid = categoryid
dim br_cat : br_cat = ""
dim br_productid : br_productid = ""
dim br_productName : br_productName = ""
dim br_oos : br_oos = "other"
dim br_listprice : br_listprice = ""
dim br_conv : br_conv = 0
dim br_catName : br_catName = categoryName

if br_catName = "" then br_catName = catName
if br_catid = "" then br_catid = typeID

if isnull(pageName) then pageName = ""

select case lcase(pageName)
	case "product.asp", "product"
		br_ptype = "product"
		br_cat = brandName & "|" & modelName & "|" & br_catName
		br_productid = itemid
		br_productName = itemdesc

		if bFinalOutofStock then 
			br_oos = "outofstock"
		else
			br_oos = "ok"
		end if
		
		br_listprice = price_our
	case "category"
		br_ptype = "category"
		br_cat = br_catName
	case "category-brand"
		br_ptype = "category"	
		br_cat = br_catName & "|" & brandName
	case "brandNew2", "brands"
		br_ptype = "category"	
		br_catid = brandid
		br_cat = brandName
	case "brand-model", "brandmodel"
		br_ptype = "category"	
		br_catid = modelid
		br_cat = brandName & "|" & modelName
	case "bmc"
		br_ptype = "category"
		br_cat = brandName & "|" & modelName & "|" & br_catName		
	case "bmcd"
		br_ptype = "category"
		br_cat = brandName & "|" & modelName & "|" & br_catName		
	case "search"
		br_ptype = "search"
	case "complete.asp"
		br_ptype = "other"
		br_conv = 1
	case else
		br_ptype = "other"
end select
%>
	<!-- BloomSurface tracking code.  Place at foot of page. -->
	<script type="text/javascript">
		var br_data = {};
		br_data.acct_id = "<%=br_acctID%>";
		br_data.ptype = "<%=br_ptype%>";
		br_data.cat_id = "<%=br_catid%>";
		br_data.cat = "<%=br_cat%>";
		br_data.prod_id = "<%=br_productid%>";
		br_data.prod_name = "<%=br_productName%>";
		br_data.pstatus= "<%=br_oos%>";
		br_data.sku = "<%=br_productid%>";
		br_data.price = "<%=br_listprice%>";
		br_data.search_term = "";
		br_data.sale_price = "";
		br_data.is_conversion = "<%=br_conv%>";
		br_data.basket_value = "<%=nOrderGrandTotal%>";
		br_data.order_id ="<%=nOrderID%>";
		br_data.basket = {
			'items': [<%=br_conv_items%>]
		};

		(function() {
			var brtrk = document.createElement('script');
			brtrk.type = 'text/javascript';
			brtrk.async = true;
			brtrk.src = 'https:' == document.location.protocol ? "https://cdns.brsrvr.com/v1/br-trk-<%=br_acctID%>.js" : "http://cdn.brcdn.com/v1/br-trk-<%=br_acctID%>.js";
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(brtrk, s);
		})();
	</script>
