<% if request.ServerVariables("HTTPS") = "off" then useHttp = "http" else useHttp = "https" %>
<!-- Async GoogleAnalytics Start -->
<script async type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1311669-1']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_trackPageLoadTime']); 

  (function(){
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Google Analytics Social Button Tracking -->
<!-- <script type="text/javascript" src="<%=useHttp%>://app.tabpress.com/js/ga_social_tracking.js"></script> -->
<!-- GoogleAnalytics End -->

<% 	if instr(request.ServerVariables("SERVER_NAME"), "staging") > 0  then %>
<script src="//assets.adobedtm.com/d0f694a37a1061afe4e18245479cdeaed180db2f/satelliteLib-5ee061b19b32dea49ae509a9092fc999c889172f-staging.js"></script>
<% else %>
<script src="//assets.adobedtm.com/d0f694a37a1061afe4e18245479cdeaed180db2f/satelliteLib-5ee061b19b32dea49ae509a9092fc999c889172f.js"></script>
<% end if %>