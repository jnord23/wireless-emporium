<%="<!-- srToken:" & session("sr_token") & " -->"%>
<%
	if prepStr(request.QueryString("utm_promocode")) <> "" then 
		session("promocode") = prepStr(request.QueryString("utm_promocode"))
		response.Cookies("promocode") = prepStr(request.QueryString("utm_promocode"))
		Response.Cookies("promocode").Expires = DateAdd("d", 1, now)
	end if
	if prepStr(request.QueryString("utm_email")) <> "" then	response.Cookies("myEmail") = prepStr(request.QueryString("utm_email"))
	
	if prepStr(request.Cookies("srLogin")) = "" then
		if prepStr(session("sr_token")) <> "" then response.Cookies("srLogin") = session("sr_token")
	end if
%>
<link rel="stylesheet" type="text/css" href="/includes/css/account_loginMenu.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/heartbleed/on.css" />
<style>.helpfulLink {text-decoration:none;} .helpfulLink div{float:right;text-align:center;/*min-width:65px;*/padding-left:1.25em;padding-right:1.25em;}</style>
<script>
	
	
	$(document).ready(function(){
		function setCookie(cname,cvalue)
		{
			document.cookie = cname + "=" + cvalue + ";"
		}
		
		function getCookie(cname)
		{
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) 
		  {
		  var c = ca[i].trim();
		  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		  }
		return "";
		}
		
		var heartbleed_cookie = getCookie("heartbleed");
		if(!heartbleed_cookie) {
			$("#heartbleed-bar").show();
		} else {
			$("#heartbleed-bar").hide();
		}
		
		$(".heartbleed-close").click(function(){
			$("#heartbleed-bar").hide();
			setCookie("heartbleed", 1);
		});
	});
</script>
<div id="popCover" style="height:100%; display:none; width:100%; background-color:#333; position:fixed; left:0px; top:0px; z-index:3000; opacity:0.6; filter:alpha(opacity=60);" onclick="closeLightBox()">&nbsp;</div>
<div id="popBox" style="height:2000px; display:none; position:fixed; width:100%; left:0px; top:0px; z-index:3001; text-align:center;"></div>
<div style="position:relative;">
<table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%" id="siteTop">
	<tr><td id="adminBar"></td></tr>
    <tr>
    	<td width="100%" align="center" id="si:<%=prepInt(request.cookies("mySession"))%>">
        	<div id="heartbleed-bar" style="display:none;">
				<div class="heartbleed-main">
					<div class="heart-green"></div>
					<div class="heartbleed-text">Worried about Heartbleed? Don't be. We're making sure you're safe while you shop!</div>
					<a href="https://filippo.io/Heartbleed/#wirelessemporium.com" target="_blank"><div class="heartbleed-check">See For Yourself &raquo;</div></a>
				</div>
				<div class="heartbleed-close"></div>
			</div>
			<div id="siteTop-Blackbar" style="height:28px; width:100%;" align="center">
            	<div style="width:980px;">
                	<div class="left" style="float:left; width:500px;">
                        <div id="siteTop-OrderNowPhone" style="margin-top:2px;"></div>
                        <div id="siteTop-Text" style="padding:6px 0px 0px 5px; text-decoration:none;">Order Now 1-800-305-1106</div>
                        <!--
						<div id="siteTop-Blackdiv"></div>
						<div class="left">
							<script type="text/javascript">
                                var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                                var lhnid = 3410;
                                var lhnwindow = '';
                                var lhnImage = "<div id='siteTop-Livechat' style='margin-top:2px; '></div>";
                                document.write(unescape("%3Cscript src='/framework/utility/liveHelp.js' type='text/javascript'%3E%3C/script%3E"));
                            </script>                        
                        </div>
                        <div class="left">
							<script type="text/javascript">
                                var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                                var lhnid = 3410;
                                var lhnwindow = '';
                                var lhnImage = "<div id='siteTop-Text' style='padding:6px 0px 0px 5px;'>Live Chat</div>";
                                document.write(unescape("%3Cscript src='/framework/utility/liveHelp.js' type='text/javascript'%3E%3C/script%3E"));
                            </script>
                        </div>
						-->
                        <div id="siteTop-Blackdiv"></div>
	                    <div id="account_nav"></div>
                    </div>
                	<div style="float:right; width:480px; position:relative;">
                    	<!-- Start: floating cart option -->
                        <div id="navCart2" class="fr">
                            <div id="dropdownCartBox" class="fcMainBox" style="display:none;">Loading Cart Items...</div>
                            <div id="fcArrowImg" class="fr fcArrow" onclick="popUpCart()"><img src="<%=imgSrv%>/images/floatingCart/top-arrow.png" border="0" /></div>
                            <div id="siteTop-CartBar2" style="padding-left:10px; float:right; cursor:pointer;" onclick="popUpCart()" title="Shopping Cart">
                                <div class="siteTopText" style="width:100%; padding-top:6px;">0 Item(s) $0.00&nbsp;&nbsp;</div>
                            </div>
							<div id="siteTop-Cart2" style="float:right; cursor:pointer;" onclick="popUpCart()" title="Shopping Cart"></div>
                        </div>
                        <div id="siteTop-Text" style="float:right; height:22px; padding:6px 20px 0px 20px; position:relative;" onmouseover="document.getElementById('id_helpcenter').style.display=''" onmouseout="document.getElementById('id_helpcenter').style.display='none'">
                        	<a href="/faq" title="Help Center" style="color:#fff; font-size:13px; font-weight:bold;">Help</a>
							<div id="id_helpcenter" style="display:none; padding:10px 10px 10px 15px; float:left; z-index:2000; position:absolute; top:22px; left:-100px; background:url(<%=imgSrv%>/images/template/top/help-bg.png) no-repeat; width:308px; height:297px;" onmouseover="document.getElementById('id_helpcenter').style.display=''" onmouseout="document.getElementById('id_helpcenter').style.display='none'">
								<div style="float:left; width:90%; padding-top:15px; color:#333; font-size:18px; font-weight:bold; text-align:center;">We're here to help you<br />find what you need:</div>
								<div id="siteTop-HelpCenterBar"></div>
                                <div style="float:left; cursor:pointer;" onclick="location.href='/orderstatus'">
								    <div id="siteTop-ImgOrder"></div>
                                    <div style="float:left; width:190px; padding:18px 0px 0px 10px;">
                                        <div style="float:left; font-size:14px; font-weight:bold; color:#333;">WHERE'S MY ORDER?</div>
                                        <div class="left">
                                        	<div style="float:left; font-size:14px; color:#666;">Check order status</div>
                                            <div style="float:left; padding:4px 0px 0px 5px;">
	                                            <div id="siteTop-OrangeArrows"></div>
                                            </div>
                                        </div>
                                    </div>                                
                                </div>
								<div id="siteTop-HelpCenterBar"></div>
                                <div style="float:left; cursor:pointer;" onclick="location.href='/faq'">
								    <div id="siteTop-ImgFaq"></div>
                                    <div style="float:left; width:190px; padding:18px 0px 0px 10px;">
                                        <div style="float:left; font-size:14px; font-weight:bold; color:#333;">FREQUENT QUESTIONS</div>
                                        <div class="left">
                                        	<div style="float:left; font-size:14px; color:#666;">The answers you need</div>
                                            <div style="float:left; padding:4px 0px 0px 5px;">
	                                            <div id="siteTop-OrangeArrows"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<div id="siteTop-HelpCenterBar"></div>
                            </div>
                        </div>
						<div id="siteTop-Blackdiv" style="float:right; margin:0px 0px 0px 20px;"></div>
						<div id="siteTop-Text" style="float:right; height:22px; padding:6px 0px 0px 20px; position:relative;">
							<a href="/orderstatus" style="color:#fff; font-size:13px; font-weight:bold;">Order Status</a>
						</div>
                        <div id="siteTop-Blackdiv" style="float:right; margin:0px 0px 0px 20px;"></div>
                    </div>
                </div>
            </div>
        </td>
	</tr>
    <!--
    <tr>
    	<td width="100%" align="center">
			<div id="siteTop-Bar" style="height:22px; width:100%;" align="center">
            	<div style="width:980px;">
                    <div id="account_nav"></div>
                    <div id="siteTop-CartBar" style="padding-left:10px; float:right;" align="center">
                    <a href="/cart/basket.asp" title="Shopping Cart"><div class="siteTopText" style="width:100%;" align="right">0 Item(s) $0.00&nbsp;&nbsp;</div></a>
                    </div>                    
                    <a href="/cart/basket.asp" title="Shopping Cart"><div id="siteTop-Cart" style="float:right;"></div></a>
                    <div id="siteTop-Div" style="float:right;"></div>                                                
                    <a href="/faq.asp" title="Help Center" style="text-decoration:none;"><div class="siteTopText" style="width:100px; float:right;" align="center">Help Center</div></a>
                    <div id="siteTop-Div" style="float:right;"></div>
                    
                    <a href="/faq.asp" title="Home" class="helpfulLink"><div class="siteTopText">FAQs</div></a>
                    <div id="siteTop-Div" style="float:right;"></div>
                    
                    <a href="/orderstatus.asp" title="Home" class="helpfulLink"><div class="siteTopText">Order Status</div></a>
                    <div id="siteTop-Div" style="float:right;"></div>
                    
            	</div>
            </div>
        </td>
	</tr>    
    -->
    <tr>
    	<td width="100%" align="center">
            <div style="width:980px; height:60px; padding:7px 0px 7px 0px;">
                <div style="width:395px; float:left;" align="left">
                	<% if month(date) = 1 and day(date) < 22 then %>
                    <a href="/"><img src="<%=imgSrv%>/images/holidayLogos/newYears.jpg" title="Wireless Emporium Inc." alt="Wireless Emporium Inc." border="0" /></a>
                    <% elseif month(date) = 10 then %>
                    <a href="/"><img src="<%=imgSrv%>/images/holidayLogos/halloween.png" title="Wireless Emporium Inc." alt="Wireless Emporium Inc." border="0" /></a>
					<% elseif (month(date) = 11 and day(date) > 22) or (month(date) = 12 and day(date) < 26) then %>
                    <a href="/"><img src="<%=imgSrv%>/images/holidayLogos/xmas.jpg" title="Wireless Emporium Inc." alt="Wireless Emporium Inc." border="0" /></a>
					<% elseif month(date) = 12 or (month(date) = 1 and day(date) = 1) then %>
                    <a href="/"><img src="<%=imgSrv%>/images/holidayLogos/newYears.jpg" title="Wireless Emporium Inc." alt="Wireless Emporium Inc." border="0" /></a>
					<% elseif month(date) = 11 then %>
                    <a href="/"><img src="<%=imgSrv%>/images/holidayLogos/we-thanksgiving.png" title="Wireless Emporium Inc." alt="Wireless Emporium Inc." border="0" /></a>
					<% else %>
                    <a href="/"><div id="logo" title="Wireless Emporium Inc."></div></a>
                    <% end if %>
                </div>
                <div style="width:575px; float:left;" align="right">
                    <div style="float:right; padding:5px 0px 0px 5px;" title="Click to Verify - This site chose VeriSign Trust Seal to promote trust online with consumers">
                        <a rel="nofollow" href="javascript:Open_Popup('https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.WIRELESSEMPORIUM.COM&lang=en','VeriSign','width=560,height=500,location=yes,status=yes,resizable=yes,scrollbars=yes');">
                            <div id="verisignLogo"></div>
                        </a>
                    </div>
                    <div style="float:right; padding:15px 0px 10px 0px;">
                    	<!-- START SCANALERT CODE -->
                        <% if prepInt(cart) = 1 then %>
                        <a rel="nofollow" target="_blank" href="https://www.mcafeesecure.com/RatingVerify?ref=www.wirelessemporium.com"><img width="65" height="37" border="0" src="https://images.scanalert.com/meter/www.wirelessemporium.com/63.gif" alt="McAfee SECURE sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="alert('Copying Prohibited by Law - McAfee SECURE is a Trademark of McAfee, Inc.'); return false;"></a>
						<% else %>
                        <a rel="nofollow" target="_blank" href="https://www.mcafeesecure.com/RatingVerify?ref=www.wirelessemporium.com"><img width="65" height="37" border="0" src="//images.scanalert.com/meter/www.wirelessemporium.com/63.gif" alt="McAfee SECURE sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="alert('Copying Prohibited by Law - McAfee SECURE is a Trademark of McAfee, Inc.'); return false;"></a>
                        <% end if %>
                        <!-- END SCANALERT CODE --> 
                    </div>
                    <% if prepInt(varientTopBanner) = 0 or prepInt(varientTopBanner) = 1 then %>
	                    <% if false then %>
                    <div style="float:right; margin:8px 10px 0px 0px;" title="Free Priority Shipping!">
                        <div style="font-size:23px; font-weight:bold; color:#cc0000; padding-bottom:5px; border-bottom:1px solid #999; text-align:center;">Free Priority Shipping!</div>
                        <div style="color:#999; font-size:14px; width:283px; text-align:center; font-stretch:expanded;">FOR ALL ORDERS OVER $30</div>
                    </div>
                        <% else %>
					<!--
                    <div id="mvt_freeShippingBanner" style="float:right; margin:8px 10px 0px 0px;" title="Free Shipping on Every Order">
                        <div style="font-size:23px; font-weight:bold; color:#cc0000; padding-bottom:5px; border-bottom:1px solid #999;">Free Same Day Shipping!</div>
                        <div style="color:#999; font-size:14px; width:283px; text-align:center; font-stretch:expanded;">90-DAY NO-HASSLE RETURNS</div>
                    </div>
                    -->
                    <div id="mvt_freeReturnShippingBanner" title="Free Same Day Shipping plus Free Return Shipping" onclick="freeReturnShippingPopup(0)"></div>
                        <% end if %>
					<% end if %>
                </div>
            </div>
        </td>
    </tr>
    <% if prepInt(varientTopNav) = 0 then %>
    <tr>
    	<td width="100%" align="center">
            <!--#include virtual="/includes/template/topNav.asp"-->
        </td>
    </tr>
    <%
	end if
	
	if prepInt(varientTopBanner) = 2 then
	%>
    <tr>
    	<!--<td id="mvt_checkoutBanner" width="100%" align="center"><img src="<%=imgSrv%>/images/varients/Free-Same-Day-Shipping-Banner.gif" border="0" /></td>-->
        <td id="mvt_freeReturnShippingCheckoutBanner" width="100%" align="center"><img src="<%=imgSrv%>/images/freereturnshipping/checkout-top-banner.png" border="0" /></td>
	</tr>
    <%
	elseif prepInt(varientTopBanner) = 3 then
	%>
    <tr>
    	<!--<td id="mvt_checkoutBanner" width="100%" align="center"><img src="<%=imgSrv%>/images/checkout/top-banner.png" border="0" /></td>-->
        <td id="mvt_freeReturnShippingCheckoutBanner" width="100%" align="center"><img src="<%=imgSrv%>/images/freereturnshipping/checkout-top-banner.png" border="0" /></td>
	</tr>
    <%
	end if
	
	if REWRITE_URL <> "/seasonalpicks" and REWRITE_URL <> "/cybermonday" then
		if date > cdate("11/24/2011") and date < cdate("11/28/2011") then
	%>
    <tr>
    	<td width="100%" align="center">
		    <table cellpadding="0" cellspacing="0" border="0" width="980"><tr><td align="center" style="padding-bottom:3px; border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc;"><a href="/seasonalpicks"><img src="<%=imgSrv%>/images/holiday/weHolidayTextBar.jpg" border="0" /></a></td></tr></table>
		</td>
	</tr>
    <%
		elseif date > cdate("11/27/2011") and date < cdate("12/01/2011") then
	%>
    <tr>
    	<td width="100%" align="center">
		    <table cellpadding="0" cellspacing="0" border="0" width="980"><tr><td align="center" style="padding-bottom:3px; border-left:1px solid #ccc; border-right:1px solid #ccc;"><a href="/cybermonday"><img src="<%=imgSrv%>/images/holiday/Door-Buster-Banner2.jpg" border="0" /></a></td></tr></table>
		</td>
	</tr>    
    <%
		elseif topPageName <> "index" and date > cdate("11/30/2011") and date < cdate("12/13/2011") then
	%>
    <tr>
    	<td width="100%" align="center">
		    <table cellpadding="0" cellspacing="0" border="0" width="980">
            	<tr>
                	<td align="center" style="border-left:1px solid #ccc; border-right:1px solid #ccc;">
                    	<div style="position:relative; width:950px; border-bottom:2px solid #ccc;">
                            <img src="<%=imgSrv%>/images/holiday/holidayTopBar2.gif" border="0" style="cursor:pointer;" onclick="toggle('holidayText');" />
							<div id="holidayText" style="font-size:13px; padding:10px; position:absolute; background-color:#fff; left:130px; top:3px; border:2px solid #666; display:none; width:700px; height:75px; text-align:left;">
                            	<div>
                                	WirelessEmporium.com's 12 Days of Holiday Madness Giveaway! From December 1st- December 12th we are going to give away one order per day. Place an order and you are automatically entered - it's that easy. 
                                	We will be announcing the winners on <a href="http://www.Facebook.com/WirelessEmporium" target="_blank" rel="me">www.Facebook.com/WirelessEmporium</a> every day. So order today and make sure to check out our Facebook page to see if you are the lucky winner.
	                                Please see our Store Policy page for additional information.
                                </div>
                                <div width="100%" align="right"><a href="#" onclick="toggle('holidayText');">Close Window</a></div>
                            </div>                            
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>    
    <%
		elseif topPageName <> "index" and date > cdate("12/12/2011") and date < cdate("12/19/2011") then
	%>
    <tr>
    	<td width="100%" align="center">
		    <table cellpadding="0" cellspacing="0" border="0" width="980">
            	<tr>
                	<td align="center" style="border-left:1px solid #ccc; border-right:1px solid #ccc;">
                    	<div style="position:relative; width:950px; border-bottom:2px solid #ccc;">
                            <img src="<%=imgSrv%>/images/mainbanners/holidayTopBar3.gif" border="0" />                        
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>    
    <%
		end if
	end if
	%>
    <tr>
    	<td width="100%" align="center">
            <table cellpadding="0" cellspacing="0" border="0" width="980">
            	<tr>
                <%
				if prepInt(noLeftNav) = 1 or prepint(iv_leftNav) = 1 then
				%>
                    <td style="width:980px; <%if not noSitePadding then%>padding:10px;<%end if%> border-left:1px solid #ccc; border-right:1px solid #ccc;" valign="top" align="left">
                <%
				else
					if pageTitle = "product" then
				%>
                	<td id="leftNavBox" style="width:200px; padding:10px 0px 10px 10px; border-left:1px solid #ccc;" valign="top" align="left">
			            <!--#include virtual="/includes/template/leftNav.asp"-->
                    </td>
                    <td id="contentBox" style="width:780px; padding:10px; border-right:1px solid #ccc;" valign="top" align="left">
                <%
					else
				%>
                    <td style="width:200px; padding:10px 0px 10px 10px; border-left:1px solid #ccc;" valign="top" align="left">
			            <!--#include virtual="/includes/template/leftNav.asp"-->
                    </td>
                    <td style="width:780px; padding:10px; border-right:1px solid #ccc;" valign="top" align="left">
                <%
					end if
				end if
				%>
