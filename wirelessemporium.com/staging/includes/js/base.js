var emphasizePaypal = 0;

function fbtPopDetails(dir,lap) {
	if (dir == 1) {
		document.getElementById("fbtPop_" + lap).style.display = '';
	}
	else {
		document.getElementById("fbtPop_" + lap).style.display = 'none';
	}
}

function filterType(lap) {
	if (document.getElementById("fbtCbox_" + lap).className == "fl bmcdFilterCheckboxOff") {
		document.getElementById("fbtCbox_" + lap).className = "fl bmcdFilterCheckboxOn";
		document.getElementById("fbtText_" + lap).className = document.getElementById("fbtText_" + lap).className + "Active";
	}
	else {
		document.getElementById("fbtCbox_" + lap).className = "fl bmcdFilterCheckboxOff";
		document.getElementById("fbtText_" + lap).className = document.getElementById("fbtText_" + lap).className.replace('Active','');
	}
}

function showPromoDetails() {	
	ajax("/ajax/julyPromo.asp",'popBox');
	document.getElementById("popCover").style.display = "";
	document.getElementById("popBox").style.display = "";
}

function closePromoDetails() {			
	document.getElementById("popCover").style.display = "none";
	document.getElementById("popBox").style.display = "none";

	document.getElementById("popCover").innerHTML = "";
	document.getElementById("popBox").innerHTML = "";
}

function popUpCart() {
	if (document.getElementById("dropdownCartBox").style.display == 'none') {
		if (emphasizePaypal == 1) {
			location.href = '/cart/basket';
		} else {
			ajax('/ajax/dropdownCart.asp','dropdownCartBox');
			document.getElementById("dropdownCartBox").style.display = '';
		}
	}
	else {
		document.getElementById("dropdownCartBox").style.display = 'none'
		document.getElementById("dropdownCartBox").innerHTML = ''
	}		
}

function popUpCart_remove(cartItemID) {
	if (emphasizePaypal == 1) {
		document.getElementById('dumpZone').innerHTML = '';
		ajax('/ajax/dropdownCart.asp?removeID=' + cartItemID,'dumpZone');
		checkCartUpdate();
	} else {
		ajax('/ajax/dropdownCart.asp?removeID=' + cartItemID,'dropdownCartBox');
		document.getElementById("dropdownCartBox").style.display = ''
		setTimeout("ajax('/ajax/dynamicUpdate.asp?myCartDetails=1','siteTop-CartBar2')",1200);		
	}
}

function popUpCartWithPromo_remove(cartItemID) {
	if (emphasizePaypal == 1) {
		document.getElementById('dumpZone').innerHTML = '';
		ajax('/ajax/dropdownCart.asp?removeID=' + cartItemID + '&promo=1','dumpZone');
		checkCartUpdate();
	} else {
		ajax('/ajax/dropdownCart.asp?removeID=' + cartItemID + '&promo=1','dropdownCartBox');
		document.getElementById("dropdownCartBox").style.display = '';
		setTimeout("ajax('/ajax/dynamicUpdate.asp?myCartDetails=1','siteTop-CartBar2')",1200);
	}
}

function popUpCart_add(itemID,itemQty,customCost) {
	window.scroll(0,0);
	
	var useItemID = itemID;
	if (typeof document.frmSubmit !== 'undefined') useItemID = document.frmSubmit.prodid.value;
	
	if (typeof StrandsTrack=="undefined"){ StrandsTrack=[]; }
	StrandsTrack.push({event:"addshoppingcart",item:itemID} );
	
	if (emphasizePaypal == 1) {
		document.getElementById('dumpZone').innerHTML = '';
		ajax('/ajax/dropdownCart.asp?addID=' + useItemID + '&addQty=' + itemQty + '&customCost=' + customCost,'dumpZone');
		checkCartUpdate();
	} else {
		ajax('/ajax/dropdownCart.asp?addID=' + useItemID + '&addQty=' + itemQty + '&customCost=' + customCost,'dropdownCartBox');
		document.getElementById("dropdownCartBox").style.display = ''
		setTimeout("ajax('/ajax/dynamicUpdate.asp?myCartDetails=1','siteTop-CartBar2')",1200);
	}
}

function popUpCartWithPromo_add(itemID,itemQty,customCost) {
	window.scroll(0,0);
	jQuery(window).trigger('addToCart');

	var useItemID = itemID;
	if (typeof document.frmSubmit !== 'undefined') useItemID = document.frmSubmit.prodid.value;
	
	if (typeof StrandsTrack=="undefined"){ StrandsTrack=[]; }
	StrandsTrack.push({event:"addshoppingcart",item:itemID} );
	
	if (emphasizePaypal == 1) {
		document.getElementById('dumpZone').innerHTML = '';
		ajax('/ajax/dropdownCart.asp?addID=' + useItemID + '&addQty=' + itemQty + '&promo=1&customCost=' + customCost,'dumpZone');
		checkCartUpdate();
	} else {
		ajax('/ajax/dropdownCart.asp?addID=' + useItemID + '&addQty=' + itemQty + '&promo=1&customCost=' + customCost,'dropdownCartBox');
		document.getElementById("dropdownCartBox").style.display = '';
		setTimeout("ajax('/ajax/dynamicUpdate.asp?myCartDetails=1','siteTop-CartBar2')",1200);
	}
}

function updateCartQty(cartItemID) {
	var newQty = document.getElementById("qtyAdj_" + cartItemID).value;

	if (emphasizePaypal == 1) {
		document.getElementById('dumpZone').innerHTML = '';
		ajax('/ajax/dropdownCart.asp?updateCartID=' + cartItemID + '&updateQty=' + newQty,'dumpZone');
		checkCartUpdate();
	} else {
		ajax('/ajax/dropdownCart.asp?updateCartID=' + cartItemID + '&updateQty=' + newQty,'dropdownCartBox');
		document.getElementById("dropdownCartBox").style.display = '';
		setTimeout("ajax('/ajax/dynamicUpdate.asp?myCartDetails=1','siteTop-CartBar2')",1200);
	}	
}

function checkCartUpdate() {
	if ("" == document.getElementById('dumpZone').innerHTML)  {
		setTimeout('checkCartUpdate()', 100);
	} else {
		location.href = '/cart/basket';
	}
}

// bloomreach add/remove cart tracking
function br_cartTracking(tType, itemid, color, prodName, price) {
    try
    {
        if (tType == 'add') {
            BrTrk.getTracker().logEvent('Cart', tType, {'prod_id': itemid, 'sku': itemid, 'prod_color': color, 'prod_name': prodName}, {'price' : price});
         } else {
            BrTrk.getTracker().logEvent('Cart', tType, {'prod_id': itemid, 'sku': itemid, 'prod_color': color, 'prod_name': prodName});
        }
        
     }
    catch(e) {}    
}

function getSelectedRadioValue(buttonGroup) {
    var i = getSelectedRadio(buttonGroup);
   if (i == -1) {
      return "";
   } else {
      if (buttonGroup[i]) { // Make sure the button group is an array (not just one button)
         return buttonGroup[i].value;
      } else { // The button group is just the one button, and it is checked
         return buttonGroup.value;
      }
   }
}

function getSelectedRadio(buttonGroup) {
   if (buttonGroup[0]) {
      for (var i=0; i<buttonGroup.length; i++) {
         if (buttonGroup[i].checked) {
            return i
         }
      }
   } else {
      if (buttonGroup.checked) { return 0; } // if the one button is checked, return zero
   }
   return -1;
}

function formatSEO(s){
	var output = s.replace(/[^a-zA-Z0-9]/g,' ').replace(/\s+/g,"-").toLowerCase();
	/* remove first dash */
	if(output.charAt(0) == '-') output = output.substring(1);
	/* remove last dash */
	var last = output.length-1;
	if(output.charAt(last) == '-') output = output.substring(0, last);
	
	return output;
}

function freeReturnShippingPopup(action) {
	if (action == 0) {
		document.getElementById('popBox').innerHTML = '';
		ajax('/ajax/freereturnshipping.asp','popBox');
		document.getElementById('popCover').style.display = '';
		document.getElementById('popBox').style.display = '';
	} else {
		closeLightBox();
	}
	
	return false;
}

function closeLightBox() {
	document.getElementById('popCover').style.display = 'none';
	document.getElementById('popBox').style.display = 'none';
	document.getElementById('popBox').innerHTML = 'none';
}

function getQsVars(pUrl)
{
	var vars = [], hash, hashes;
	if (pUrl != "") {
		hashes = pUrl.slice(pUrl.indexOf('?') + 1).split('&');
	} else {
		hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	}
	if (hashes != undefined) {
		for(var i = 0; i < hashes.length; i++)
		{
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
	}
	return vars;
}