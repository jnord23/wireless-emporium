<%
' APPLY COUPON DISCOUNT
NoMorePromos = 0

if couponid > 0 then
	if NoMorePromos = 0 then
		if oneTime then
			nDiscountTotal = setValue
		elseif nSubTotal >= promoMin then
			sql = 	"select a.itemid, a.qty, b.modelid, b.typeid, b.partnumber, b.itemdesc, b.itempic, b.handsfreetype, b.price_our, b.price_retail, b.price_ca, b.price_co, b.price_ps, b.price_er price_tm, b.itemweight" & vbcrlf & _
					"from 	shoppingcart a join we_items b on a.itemid = b.itemid" & vbcrlf & _
					"where 	a.store = '" & siteid & "' and a.adminid = '" & adminID & "' and (a.purchasedorderid is null or a.purchasedorderid = 0)" & vbcrlf & _
					" 	and a.customcost is null" & vbcrlf & _
					" 	and b.nodiscount = 0"
			session("errorSQL") = sql
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 0, 1
			do until RS.eof
				select case siteid
					case 0 : couponPrice = cdbl(rs("price_our"))
					case 1 : couponPrice = cdbl(rs("price_ca"))
					case 2 : couponPrice = cdbl(rs("price_co"))
					case 3 : couponPrice = cdbl(rs("price_ps"))
					case 10 : couponPrice = cdbl(getPricingTM(rs("price_tm"), rs("price_our")))
				end select			
				nProdIdCheck = RS("itemID")
				nProdQuantity = RS("qty")
				if cStr(cTypeID) <> "0" or BOGO = true then
					if cStr(RS("typeID")) = "16" then
						if inStr(couponDesc,"(*This coupon code cannot be applied to cell phone purchases)") = 0 then
							couponDesc = couponDesc & "<br>(*This coupon code cannot be applied to cell phone purchases)"
						end if
					else
						dim typeIDarray
						typeIDarray = split(cTypeID,",")
						for a = 0 to uBound(typeIDarray)
							if excludeBluetooth = true and RS("HandsfreeType") = 2 then
								if inStr(couponDesc,"(*Bluetooth products excluded)") = 0 then
									couponDesc = couponDesc & "<br>(*Bluetooth products excluded)"
								end if
							else
								if cStr(RS("typeID")) = cStr(typeIDarray(a)) or cStr(typeIDarray(a)) = "99" then
									if BOGO = true then
										nDiscountTotal = nDiscountTotal + (couponPrice * int(nProdQuantity/2) * promoPercent)
									else
										nDiscountTotal = nDiscountTotal + (couponPrice * nProdQuantity * promoPercent)
									end if
								else
									SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
									SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_Our > 0)"
									SQL = SQL & " AND (R.typeid = '" & cStr(typeIDarray(a)) & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
									SQL = SQL & " AND I.NoDiscount = 0"
									'response.write "<p>cStr(RS(typeID)) = " & cStr(RS("typeID")) & " | SQL = " & SQL & "</p>"
									set RSrelated = Server.CreateObject("ADODB.Recordset")
									RSrelated.open SQL, oConn, 0, 1
									if not RSrelated.eof then
										if BOGO = true then
											nDiscountTotal = nDiscountTotal + (couponPrice * int(nProdQuantity/2) * promoPercent)
										else
											nDiscountTotal = nDiscountTotal + (couponPrice * nProdQuantity * promoPercent)
										end if
									end if
									RSrelated.close
									set RSrelated = nothing
								end if
							end if
						next
					end if
				elseif not isNull(FreeItemPartNumber) then
					if FreeItemPartNumber = RS("PartNumber") and FreeProductWithPhonePrice = 0 and PhonePurchased = 1 then
						FreeProductWithPhonePrice = couponPrice
					end if
				elseif cStr(RS("typeID")) = "16" then
					if inStr(couponDesc,"(*This coupon code cannot be applied to cell phone purchases)") = 0 then
						couponDesc = couponDesc & "<br>(*This coupon code cannot be applied to cell phone purchases)"
					end if
				else
					if excludeBluetooth = true and RS("HandsfreeType") = 2 then
						if inStr(couponDesc,"(*Bluetooth products excluded)") = 0 then
							couponDesc = couponDesc & "<br>(*Bluetooth products excluded)"
						end if
					else
						nDiscountTotal = nDiscountTotal + (promoPercent * (nProdQuantity * couponPrice))
					end if
				end if
				RS.movenext
			loop
			RS.close
			set RS = nothing
			if FreeProductWithPhonePrice > 0 then nDiscountTotal = FreeProductWithPhonePrice
			session("errorSQL") = nSubTotal & " - " & nDiscountTotal
'			if nDiscountTotal > 0 then
'				nSubTotal = round(nSubTotal - nDiscountTotal,2)
'			end if
		else
			couponDesc = "Coupon code cannot be applied to this order - minimum order value not reached."
		end if
	else
		couponDesc = "Coupon code cannot be applied to this order - another discount has already been applied."
	end if
end if
%>
