// JavaScript Document<script>
<!--
function isObject(o)			{	return (o && "object" == typeof o);				}   

function isAllChecked(objChk) 
{
	if (isObject(objChk))
	{
		if ("undefined" != typeof(objChk.length))
		{
			for (i=0; i<objChk.length; i++)	
				if (false == objChk[i].checked)
					return false;
		}
		else
		{
			if (false == objChk.checked)	
				return false;
		}

		return true;
	} 
}

function isAllUnChecked(objChk) 
{
	if (isObject(objChk))
	{
		if ("undefined" != typeof(objChk.length))
		{
			for (i=0; i<objChk.length; i++)	
				if (true == objChk[i].checked)
					return false;
		}
		else
		{
			if (true == objChk.checked)	
				return false;
		}

		return true;
	} 
}

function setAllCheckbox(objChk, check) 
{
	if (isObject(objChk))
	{
		if ("undefined" != typeof(objChk.length))
			for (i=0; i<objChk.length; i++)	objChk[i].checked = check;
		else
			objChk.checked = check;
	} 
}
//-->