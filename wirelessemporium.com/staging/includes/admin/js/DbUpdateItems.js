var strSiteId=''

$(document).ready(function(){
	$("input[field=DiscountValue]").keyup( function( objEvent){
		if( document.getElementById( 'DiscountOption'+strSiteId).value=='%off')
			RefreshDiscountPercentPrice( strSiteId)
		else
			document.getElementById('Price'+strSiteId).value=document.getElementById('DiscountValue'+strSiteId).value
	});
});

function CalculateDiscountedPrice( strDiscountPercent, strOriginalPrice){
	return Math.round((100.0-strDiscountPercent)*strOriginalPrice)/100.00
}

function RefreshDiscountPercentPrice( strSiteId){
	document.getElementById('Price'+strSiteId).value=CalculateDiscountedPrice( document.getElementById('DiscountValue'+strSiteId).value, document.getElementById('OriginalPrice'+strSiteId).value)
}

function OnChangeDiscountOption( objDiscountOption, strSiteId){
	document.getElementById('divDiscountValue'+strSiteId).style.display=( objDiscountOption.value=='NoSale'? 'none': 'block')
	if( objDiscountOption.value=='%off'){ 
		document.getElementById('DiscountValue'+strSiteId).value=document.getElementById('DiscountPercent'+strSiteId).value		
		document.getElementById('Price'+strSiteId).value=CalculateDiscountedPrice( document.getElementById('DiscountPercent'+strSiteId).value, document.getElementById('OriginalPrice'+strSiteId).value)
		document.getElementById('Price'+strSiteId).disabled=true;
	} else if( objDiscountOption.value=='$'){ 
		document.getElementById('DiscountValue'+strSiteId).value=document.getElementById('DiscountPrice'+strSiteId).value
		document.getElementById('Price'+strSiteId).value=document.getElementById('DiscountPrice'+strSiteId).value
		document.getElementById('Price'+strSiteId).disabled=true;
	} else{
		document.getElementById('Price'+strSiteId).value=document.getElementById('OriginalPrice'+strSiteId).value
		document.getElementById('Price'+strSiteId).disabled=false;
	}
}
