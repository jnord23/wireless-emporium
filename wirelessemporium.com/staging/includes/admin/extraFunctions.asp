<%
sql = "select saveDate from x_saveTheDate where title = 'customLowStock'"
session("errorSQL") = sql
set lowStockRS = oConn.execute(sql)

if lowStockRS.EOF then
	customLowStock = date-1
	sql = "insert into x_saveTheDate (title,saveDate) values('customLowStock','" & customLowStock & "')"
	session("errorSQL") = sql
	oConn.execute(sql)
else
	customLowStock = cdate(lowStockRS("saveDate"))
end if

if customLowStock < date then
	sql = "exec sp_customProductLowStock"
	session("errorSQL") = sql
	set lowStockRS = oConn.execute(sql)
	
	if not lowStockRS.EOF then
		do while not lowStockRS.EOF
			lsItemID = lowStockRS("itemID")
			lsPartNumber = lowStockRS("partNumber")
			lsInvQty = lowStockRS("inv_qty")
			
			strBody = strBody & "<div style='margin:10px 0px; color:#f00;'>Partnumber: " & lsPartNumber & " (" & lsInvQty & " remaining inventory)</div>"
			
			lowStockRS.movenext
		loop
		
		Dim objErrMail : Set objErrMail = Server.CreateObject("CDO.Message")
		With objErrMail
			.From = "Purchasing <wepurchasing@wirelessemporium.com>"
			.To = "wepurchasing@wirelessemporium.com"
			'.To = "jon@wirelessemporium.com"
			.Subject = "Custom Product(s) Low on Stock"
			.HTMLBody = strBody
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Item("urn:schemas:httpmail:sender") = "sales@wirelessemporium.com"
			.Configuration.Fields.Update
			if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
				response.Write("<!-- don't send on staging -->")
			else
				.Send
			end if
			
			sql = "update x_saveTheDate set saveDate = '" & date & "' where title = 'customLowStock'"
			session("errorSQL") = sql
			oConn.execute(sql)
		End With
		Set objErrMail = nothing
		
		strBody = ""
	end if
end if
%>