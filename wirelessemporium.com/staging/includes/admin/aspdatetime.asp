<%
	'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		'DATE TIME LIBRARY
	'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	Function fnstrleadzerodt(passval)
		passval=Trim("" & passval)
		If len(passval)=1 Then
			fnstrleadzerodt = "0" & passval
		Else
			fnstrleadzerodt = passval
		End If
	End Function
	Function fnRetDateFormat(dtval,dtformat)
		Dim dt,mon,yr
		Dim hh,nn,ampm, timeval
		fnRetDateFormat=""
		dtval = Trim("" & dtval)
		If isDate(dtval) Then
			dtval = Cdate(dtval)
			dt=DatePart("d",dtval)
			mon=DatePart("m",dtval)
			yr=DatePart("yyyy",dtval)
			Select Case dtformat
				Case "m/d/yyyy": fnRetDateFormat = mon & "/" & dt & "/" & yr
				Case "mm/dd/yyyy": fnRetDateFormat = fnstrleadzerodt(mon) & "/" & fnstrleadzerodt(dt) & "/" & yr
									'Response.Write "<BR>dt-->" & dt
				Case "hh:mm ampm":	timeval = FormatDateTime(dtval,3)
									'hh=fnstrleadzerodt(Hour(timeval))
									'nn=fnstrleadzerodt(Minute(timeval))
									fnRetDateFormat=Mid(timeval,1,InstrRev(timeval,":")-1)
									'fnRetDateFormat = hh & ":" & nn
									If Instr(timeval,"AM") > 0 Then
										fnRetDateFormat=fnRetDateFormat & " AM"
									Else
										fnRetDateFormat=fnRetDateFormat & " PM"
									End If
			End Select
		End If
	End Function
	Function fnretAddedDate(dtval,addto,addval)
			'Returns New Date after addition of passed values
			'Used in common/jsPayTerms.asp
			addval=Trim("" & addval)
			If Not isNumeric(addval) Then
				addval=0
			Else
				addval = Cdbl("0" & addval)
			End If
			If isDate(dtval) then
				fnretAddedDate = DateAdd(addto,addval,dtval)
			End If
	End Function
	Function fnstrDateOrBlank(strpassdt)
		'FUNCTION USED TO PASS BLANK VALUE IF ENTERED DATE IS BLANK/NOT PROPER DATE
		'RETURNS DATE IF DATE IS PASSED ELSE BLANKS
		Dim passdt
		passdt = Trim("" & strpassdt)
		fnstrDateOrBlank=""
		If (isDate(passdt) And passdt <> "") Then
			fnstrDateOrBlank=cDate(passdt)
		End If
	End Function


Function fnStrJs(strpass)
	'WHEN ASP ASSIGNS SOME VALUE TO A JAVASCRIPT FUNCTION
	'THEN SPECIAL CHARACTERS NEEDS TO BE PRECEEDED BY \
	On Error Resume Next
	strpass=Trim("" & strpass)
	fnStrJs=Replace(strpass,"\","\\")
	fnStrJs=Replace(fnStrJs,"(","\(")
	fnStrJs=Replace(fnStrJs,")","\)")
	fnStrJs=Replace(fnStrJs,"'","\'")
	fnStrJs=Replace(fnStrJs,chr(13)," ")
	fnStrJs=Replace(fnStrJs,chr(10)," ")
	'fnStrJs=Replace(fnStrJs,"<","\<)")
	'fnStrJs=Replace(fnStrJs,">","\>)")
End Function

Function fnstrBkToOrig(strPass)
	fnstrBkToOrig=Trim("" & strPass)
	fnstrBkToOrig=Replace(fnstrBkToOrig,"\\","\")
	fnstrBkToOrig= Replace(fnstrBkToOrig,"\'","'")
	fnstrBkToOrig= Replace(fnstrBkToOrig,"\(","(")
	fnstrBkToOrig=Replace(fnstrBkToOrig,"\)",")")
End Function


Function fnblnDeleteFile(pgname,strpath)
'FUNCTION USED TO DELETE FILES FROM SERVER.
'CALLED FROM ADMIN SECTION TO DELETE THE STAFF/PRODUCT IMAGES
	On Error Resume Next
		Dim objFSO
		Set objFSO = Server.CreateObject("SCRIPTING.FILESYSTEMOBJECT")
		objFSO.DeleteFile strpath,true
		If Err.number <> 0 Then
			If sendmymail("msharma@celetron.com","msharma@celetron.com","Error while deleting file in Page : " & pgname,"File that was being deleted :" & strpath & "<BR>" & "Page Name : " & pgname)="" Then
				fnblnDeleteFile=False
			End If
		Else
			fnblnDeleteFile=True
		End If
End Function

Function fnblnFileExists(strpath)
'FUNCTION THAT RETURNS TRUE IF A FILE EXISTS IN THE SPECIFIED PATH
	On Error Resume Next
		fnblnFileExists=False
		Dim objFSO
		Set objFSO = Server.CreateObject("SCRIPTING.FILESYSTEMOBJECT")
		fnblnFileExists=objFSO.FileExists(strpath)
		Set objFSO = Nothing
End Function

Function fnstrQs(strpass)
	'To replace the single quotes with two single quotes
	On Error Resume Next
	strpass="" & strpass
  	strpass=Replace(strpass,"'","''")
  	fnstrQs=strpass
End Function

Function fnValOrNull(strPass)
	Dim strTemp
	strTemp = strPass
	strTemp = Trim("" & strTemp)
	If strTemp = "" Then
		fnValOrNull=NULL
	Else
		fnValOrNull=strTemp
	End If
End Function

	 'USED IN BUSINESS OBJECT CASES
	 Sub boGenerateDropDowns(rsBO,keyfield,valuefield)
		'On Error Resume Next
 			Dim strArrFlds, strText, i
			strArrFlds=Split(valuefield,"|")
			While not rsBO.EOF
				For i=0 To UBound(strArrFlds)
					If i <> 0 Then strText = strText & " - "
					strText=strText & rsBO(strArrFlds(i))
				Next
				If keyfield<> "" Then
						Response.Write("<option value=" & fnstrJs(rsBO(keyfield)) & ">" & strText & "</option>")
				Else
						Response.Write("<option>" & strText & "</option>")
				End If
				rsBO.MoveNext
			Wend
			rsBO.Close
			Set rsBO = Nothing
	 End Sub

Function TISPL_fnReturnTotalPages(tmpTotalRecs,tmpPageSize)
		'Returns Total Pages
		On Error Resume Next
		TISPL_fnReturnTotalPages = 1
		'Response.Write "<BR>" & tmpTotalRecs & " : " & tmpPageSize
		'Response.end
		If (tmpTotalRecs > 0) And (tmpPageSize > 0) Then
			TISPL_fnReturnTotalPages = Fix(tmpTotalRecs \ tmpPageSize)
			If (tmpTotalRecs Mod tmpPageSize) > 0 Then TISPL_fnReturnTotalPages = TISPL_fnReturnTotalPages + 1
		End If
End Function
%>