<%
function UPScode_all(site_id,sZip,sWeight,shiptype,nTotalQuantity,Paypal,Google)
	strPostalCode = "92867"
	if site_id = 1 or site_id = 3 then
		strPostalCode = "92835"	
	end if
	if len(sZip) = 5 and isNumeric(sZip) then
		myXML = myXML & "<?xml version=" & chr(34) & "1.0" & chr(34) & "?>"
		myXML = myXML & "<AccessRequest xml:lang=" & chr(34) & "en-US" & chr(34) & ">"
		myXML = myXML & "	<AccessLicenseNumber>DBFAE5E2A07B0DA0</AccessLicenseNumber>"
		myXML = myXML & "	<UserId>wirelessemporium</UserId>"
		myXML = myXML & "	<Password>bears1986</Password>"
		myXML = myXML & "</AccessRequest>"
		myXML = myXML & "<?xml version=" & chr(34) & "1.0" & chr(34) & "?>"
		myXML = myXML & "<RatingServiceSelectionRequest xml:lang=" & chr(34) & "en-US" & chr(34) & ">"
		myXML = myXML & "	<Request>"
		myXML = myXML & "		<TransactionReference>"
		myXML = myXML & "			<CustomerContext>Rating and Service</CustomerContext>"
		myXML = myXML & "			<XpciVersion>1.0001</XpciVersion>"
		myXML = myXML & "		</TransactionReference>"
		myXML = myXML & "		<RequestAction>Rate</RequestAction>"
		myXML = myXML & "		<RequestOption>shop</RequestOption>"
		myXML = myXML & "	</Request>"
		myXML = myXML & "	<PickupType>"
		myXML = myXML & "		<Code>01</Code>"
		myXML = myXML & "	</PickupType>"
		myXML = myXML & "	<Shipment>"
		myXML = myXML & "		<Shipper>"
		myXML = myXML & "			<Address>"
		myXML = myXML & "				<PostalCode>" & strPostalCode & "</PostalCode>"
		myXML = myXML & "			</Address>"
		myXML = myXML & "		</Shipper>"
		myXML = myXML & "		<ShipTo>"
		myXML = myXML & "			<Address>"
		myXML = myXML & "				<PostalCode>" & sZip & "</PostalCode>"
		myXML = myXML & "			</Address>"
		myXML = myXML & "		</ShipTo>"
		myXML = myXML & "		<Service>"
		myXML = myXML & "			<Code>11</Code>"
		myXML = myXML & "		</Service>"
		myXML = myXML & "		<Package>"
		myXML = myXML & "			<PackagingType>"
		myXML = myXML & "				<Code>02</Code>"
		myXML = myXML & "				<Description>Package</Description>"
		myXML = myXML & "			</PackagingType>"
		myXML = myXML & "			<Description>Rate Shopping</Description>"
		myXML = myXML & "			<PackageWeight>"
		myXML = myXML & "				<Weight>" & sWeight & "</Weight>"
		myXML = myXML & "			</PackageWeight>"
		myXML = myXML & "		</Package>"
		myXML = myXML & "		<ShipmentServiceOptions/>"
		myXML = myXML & "	</Shipment>"
		myXML = myXML & "</RatingServiceSelectionRequest>"
		
		on error resume next
		
		dim XmlHttp
		Set XmlHttp = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
		xmlHttp.open "POST", "https://wwwcie.ups.com/ups.app/xml/Rate", False
		xmlHttp.send(myXML)
		
		dim xmlDoc, RootNode, NodeList
		Set xmlDoc = Server.CreateObject("Msxml2.DOMDocument.3.0")
		xmlDoc.async = false
		xmlDoc.loadXml xmlHttp.responseText
		Set RootNode = xmlDoc.documentElement
		Set NodeList = RootNode.selectNodes("RatedShipment")
		UPScode = ""
		if NodeList.Length > 0 then
			dim Code, Price
			for a = 0 to NodeList.Length - 1
				Code = GetElementText(NodeList(a),"Service/Code")
				Price = GetElementText(NodeList(a),"RatedPackage/TotalCharges/MonetaryValue")
				select case site_id
					case 0: Price = cDbl(Price) + 1.4
					case 1: Price = Price + 5.00
					case 2:	Price = (Price + 1.99) + (2.99 * nTotalQuantity)
					case 3: Price = Price + (1.99 * nTotalQuantity)
					case 10: Price = cDbl(Price) + 3.4
				end select
				if Google = 0 then
					select case site_id
						case 2	'CO
							select case code
								case "02": UPScode = UPScode & "9@@" & Price & "@@UPS 2nd Day Air##"		'shiptype 9 - UPS 2nd Day Air
								case "03": UPScode = UPScode & "7@@" & Price & "@@UPS Ground##"				'shiptype 7 - UPS Ground							
								case "12": UPScode = UPScode & "8@@" & Price & "@@UPS 3 Day Select##"		'shiptype 8 - UPS 3 Day Select
							end select						
						case else
							select case code
								case "02": UPScode = UPScode & "6@@" & Price & "@@UPS 2nd Day Air##"		'shiptype 6 - UPS 2nd Day Air
								case "03": UPScode = UPScode & "4@@" & Price & "@@UPS Ground##"				'shiptype 4 - UPS Ground							
								case "12": UPScode = UPScode & "5@@" & Price & "@@UPS 3 Day Select##"		'shiptype 5 - UPS 3 Day Select
							end select						
					end select
				else	'for Google Checkout
					select case code
						case "02": UPScode = UPScode & ShippingType(Code) & "|" & Price & "|"		'shiptype 9 - UPS 2nd Day Air
						case "03": UPScode = UPScode & ShippingType(Code) & "|" & Price & "|"		'shiptype 7 - UPS Ground
						case "12": UPScode = UPScode & ShippingType(Code) & "|" & Price & "|"		'shiptype 8 - UPS 3 Day Select
					end select
				end if
			next
		end if
		
		on error goto 0
	end if
	UPScode_all = UPScode
end function

Function GetElementText(Node, Tagname)
	Dim NodeList, CurrNode, b
	Set NodeList = Node.getElementsByTagname(Tagname)
	If NodeList.Length > 0 Then
		For b = 0 To NodeList.Length - 1
			Set CurrNode = NodeList.nextNode
			GetElementText = CurrNode.text
		Next
	Else
		GetElementText = ""
	End If
	Set NodeList = Nothing
End Function

Function ShippingType(code)
	select case code
		case "01" : ShippingType = "UPS Next Day Air"
		case "02" : ShippingType = "UPS 2nd Day Air"
		case "03" : ShippingType = "UPS Ground"
		case "07" : ShippingType = "UPS Worldwide Express"
		case "08" : ShippingType = "UPS Worldwide Expedited"
		case "11" : ShippingType = "UPS Standard"
		case "12" : ShippingType = "UPS 3 Day Select"
		case "13" : ShippingType = "UPS Next Day Air Saver"
		case "14" : ShippingType = "UPS Next Day Air Early A.M."
		case "54" : ShippingType = "UPS Worldwide Express Plus"
		case "59" : ShippingType = "UPS 2nd Day Air A.M."
	end select
End Function

Function PackageType(code)
	select case code
		case "01" : PackageType = "UPS letter/ UPS Express Envelope"
		case "02" : PackageType = "Package"
		case "03" : PackageType = "UPS Tube"
		case "04" : PackageType = "UPS Pak"
		case "21" : PackageType = "UPS Express Box"
		case "24" : PackageType = "UPS 25Kg Box"
		case "25" : PackageType = "UPS 10Kg Box"
	end select
End Function
%>
