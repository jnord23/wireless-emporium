FAQ for PhoneSale.com
<ul>
	<li>
		Why do service providers offer locked phones?
		<ul>
			<li>
				Most service providers don't make money by selling cell phones; they make money by selling service. This is why they offer heavily discounted but locked cell phones in conjunction with two-year contracts. By providing you with a locked cell phone, service providers restrict you from using your phone on different networks and from experiencing the true potential of your phone. Additionally, you have to pay steep penalty fees to switch networks or cancel service if you move.
			</li>
		</ul>
	</li>
	<li>
		What is an unlocked phone?
		<ul>
			<li>
				An unlocked phone is a cell phone that is not restricted to only one carrier. Some carriers have exclusive contracts with phone companies which restricts the use of certain phones to one specific carrier. But by unlocking cell phones, you have the freedom to use the phone you want on the network that works best for you. 
			</li>
		</ul>
	</li>
	<li>
		What is a "locked" phone?
		<ul>
			<li>
				A locked phone is a device that can only be used with one provider; its usability is �locked� to only one network. Locked phones will not accept other carriers' SIM cards, including international SIMs. 
			</li>
		</ul>
	</li>
	<li>
		Which phones can be unlocked?
		<ul>
			<li>
				Nearly any GSM cell phone can be unlocked and used on the network of your choice. GSM cell phones are devices that use SIM (Subscriber Identity Module) cards, including most AT&T and T-Mobile phones. If you have any questions about whether or not a phone can be unlocked, please contact us <link to �Contact Us� page>.  
			</li>
		</ul>
	</li>
	<li>
		Which phones cannot be unlocked?
		<ul>
			<li>
				There are two types of cell phones in the US�GSM and CDMA phones. CDMA phones, those used on Verizon, Sprint, and MetroPCS networks, do not use SIM cards and therefore cannot be unlocked to use on different networks at this time. If you have any questions about whether or not a phone can be unlocked, please contact us <link to �Contact Us� page>.  
			</li>
		</ul>
	</li>
	<li>
		How can I activate an unlocked phone?
		<ul>
			<li>
				With an unlocked cell phone, you don't have to worry about activation because it is ready to go. Just pop in your current SIM card and turn the cell phone on. You'll be up and running instantly. 
			</li>
		</ul>
	</li>
	<li>
		Which carriers can use unlocked phones?
		<ul>
			<li>
				You will be able to use your unlocked cell phone with any network that utilizes SIM cards. Not only does that include two of the major US carriers, AT&T and T-Mobile, but also dozens of networks outside the US, making it easy for you to travel abroad and use your phone without having to pay expensive roaming or international fees. 
			</li>
		</ul>
	</li>
	<li>
		Will my provider charge me for using an unlocked phone?
		<ul>
			<li>
				No. More than anything, service providers are concerned about selling you service and will not penalize you for using a phone they didn't provide. 
			</li>
		</ul>
	</li>
	<li>
		Why do unlocked phones cost more?
		<ul>
			<li>
				Carriers often get big discounts from manufacturers because they're big distributors and are willing to pass those savings onto customers as long as they lock in service contracts. Because unlocked cell phones are not accompanied by the commitment of contracts, they typically cost more. But don't think that just because a carrier offers you a phone for �free� that it's actually free�they make back the money they lost on the phone by selling you a two-year service contract and charging you an activation fee.
			</li>
		</ul>
	</li>
	<li>
		Can I use an unlocked phone with Verizon/Sprint/Metro PCS service?
		<ul>
			<li>
				Verizon, Sprint, and MetroPCS are CDMA networks and do not use SIM cards therefore unlocked phones will not work on these networks. 
			</li>
		</ul>
	</li>
	<li>
		Can I use a Sprint phone with Verizon service (vice versa)?
		<ul>
			<li>
				No. Sprint and Verizon restrict the use of their phones to their networks and there is currently no way to �unlock� their phones. 
			</li>
		</ul>
	</li>
	<li>
		Which phones can be used internationally?
		<ul>
			<li>
				The majority of international networks are GSM networks, which means that most unlocked phones will be useable in countries that have GSM networks. Some restrictions apply, including frequency (below).
			</li>
		</ul>
	</li>
	<li>
		What is dual band, tri band, quad band? Which one do I need?
		<ul>
			<li>
				Dual, tri, and quad bands refer to frequency bands. These bands are the frequencies in which cell phones are able to transmit data in various areas and countries. Though there are more than four frequencies used worldwide, some frequencies are more popular than others. For example, 900 MHz is the most commonly used frequency band and the 1900 MHz band will improve services in countries that use 900 MHz. 
			</li>
			<li>
				Which type of phone you need depends on where you will be using it. A dual band phone is typically sufficient for service in most countries. Luckily, tri and quad band phones are becoming the standard, making unlocked GSM phones all the more useful. For a complete list of which frequency a country uses, visit http://www.gsmworld.com/roaming/gsminfo/index.shtml.
			</li>
		</ul>
	</li>
	<li>
		Why are cell phones on PhoneSale.com cheaper than the retail price at my local store?
		<ul>
			<li>
				PhoneSale.com utilizes a multi-million dollar budget of purchasing power and has direct relations with mass distributors and manufacturers of cell phone handsets. We bypass the "middleman" and are able to pass the channel savings directly to our customers. Also, as a direct-to-consumer retailer, PhoneSale.com does not deal with carriers or service contracts and is not encumbered by carrier or manufacturer restrictions.
			</li>
		</ul>
	</li>
</ul>
