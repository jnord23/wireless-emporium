<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
listingType = "C"
carrierid = prepInt(request.querystring("carrierid"))
brandid = prepInt(request.querystring("brandid"))

response.buffer = true
productListingPage = 1
nProductsPerPage = 24

strSort = prepStr(request("strSort"))
if strSort = "" then strSort = "pop"

dim carrierName, categoryName, brandName

if left(REWRITE_URL, 5) = "/car-" then
	if carrierid = 0 then call PermanentRedirect("/")
	
	SQL = "SELECT carriername FROM we_carriers with (nolock) WHERE id = '" & carrierid & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	
	if RS.eof then call PermanentRedirect("/")
	
	carrierName = RS("carrierName")
else
	if brandid = 0 then call PermanentRedirect("/")
	
	SQL = "SELECT brandName FROM we_brands with (nolock) WHERE brandid = '" & brandid & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	
	if RS.eof then call PermanentRedirect("/")
	
	brandName = rs("brandName")
	
	if left(REWRITE_URL, 7) = "/cp-sb-" then
		call PermanentRedirect("/sb-" & brandid & "-sm-0-scd-1020-cell-phones-" & formatSEO(brandName))
	end if

	listingType = "B"
end if

categoryName = "Cell Phones"

sql	=	"select	itemid, brandid, modelid, typeid, subtypeid, carrierid, itemdesc, itempic, flag1, price_retail" & vbcrlf & _
		"	,	price_our, inv_qty, itemkit_new, hotdeal, hidelive, numberofsales, isnull(b.reviewCnt, 0) reviewCnt, isnull(b.reviewAvg, 0) reviewAvg" & vbcrlf & _
		"from	we_items a with (nolock) left join (	select	partNumber, count(*) as reviewCnt, convert(int,avg(starRating)) as reviewAvg " & vbcrlf & _
		"												from	XReview ai with (nolock) join we_items bi with (nolock) on ai.itemID = bi.itemID where siteID = 0 and isApproved = 1" & vbcrlf & _
		"												group by partNumber" & vbcrlf & _
		"											) b " & vbcrlf & _
		"	on	a.partNumber = b.partNumber" & vbcrlf & _
		"where	a.typeid = 16" & vbcrlf & _
		"	and a.hidelive = 0" & vbcrlf & _
		"	and a.inv_qty <> 0"	 & vbcrlf

if carrierid > 0 then
	if carrierid = 2 or carrierid = 5 then
		sql	= sql & "	and	a.carrierid in (" & carrierid & ", 12) " & vbcrlf
	else
		sql	= sql & "	and	a.carrierid = " & carrierid & " " & vbcrlf
	end if
end if

if brandid > 0 then
	sql	= sql & "	and	a.brandid = " & brandid & " " & vbcrlf
end if

select case strSort
	case "pop"
		sql = sql & " order by numberOfSales desc, flag1 desc, itemID desc"
	case "new"
		sql = sql & " order by itemID desc"
	case "low"
		sql = sql & " order by price_our, flag1 desc, itemID desc"		
	case "high"
		sql = sql & " order by price_our desc, flag1 desc, itemID desc"
	case "review"
		sql = sql & " order by reviewCnt desc, reviewAvg desc, flag1 desc, itemid desc"		
	case else
		sql = sql & " order by flag1 desc, itemID desc"
end select

'response.write "<pre>" & sql & "</pre>"

set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.eof then call PermanentRedirect("/")

nProducts = 0
do until rs.eof
	nProducts = nProducts + 1
	rs.movenext
loop
rs.movefirst

dim Unlocked
if carrierid = 6 then
	Unlocked = ""
else
	Unlocked = "Unlocked "
end if

dim SEtitle, SEdescription, SEkeywords
if brandName <> "" then
	SEtitle = Unlocked & "Cell Phones for " & brandName 
	SEdescription = "Wireless Emporium offers the best " & Unlocked & "Cell Phones including " & Unlocked & " GSM cell phones & CDMA cell phones for all major brands such as LG, Samsung, Nokia and many more, at low discounted prices!"
	SEkeywords = Unlocked & "cell phones, " & Unlocked & "phones, " & Unlocked & "gsm cell phones, " & Unlocked & "cdma cell phones, " & Unlocked & "lg cell phones, " & Unlocked & "nokia cell phones"

	strH1 = brandName & " "& Unlocked &" Cell Phones"
	strBreadcrumb = brandName & " " & Unlocked  & "Cell Phones"	
else
	SEtitle = Unlocked & "Cell Phones: " & Unlocked & "GSM LG, Samsung & Nokia Cell Phones"
	SEdescription = "Wireless Emporium offers the best " & Unlocked & "Cell Phones including " & Unlocked & "GSM cell phones & CDMA cell phones for all major brands such as LG, Samsung, Nokia and many more, at low discounted prices!"
	SEkeywords = Unlocked & "cell phones, " & Unlocked & "phones, " & Unlocked & "gsm cell phones, " & Unlocked & "cdma cell phones, " & Unlocked & "lg cell phones, " & Unlocked & "nokia cell phones"

	strH1 = carrierName & " "& Unlocked &" Cell Phones"
	strBreadcrumb = carrierName & " " & Unlocked  & "Cell Phones"		
end if

if seH1 <> "" then strH1 = seH1

strH1 = replace(strH1, "Unlocked Unlocked", "Unlocked")
strBreadcrumb = replace(strBreadcrumb, "Unlocked Unlocked", "Unlocked")

lastPage = getPageNum(nProducts, nProductsPerPage)
if nProducts <= nProductsPerPage then
	lastProduct = nProducts
else
	lastProduct = nProductsPerPage
end if
%>
<!--#include virtual="/includes/template/top.asp"-->
<script>
window.WEDATA.pageType = "carrier";
window.WEDATA.pageData = {
    carrier: '<%= carrierName %>',
    carrierId: '<%= carrierId %>'
};
</script>

<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%" class="unlocked">
    <tr>
        <td align="left" valign="top" width="100%" style="font-size:14px; color:#333; font-weight:bold;" >
            <a class="siteBlue" style="font-size:14px; font-weight:normal;" href="/">HOME</a>
            &nbsp;<span class="siteBlue">&rsaquo;</span>&nbsp;
            <a class="siteBlue" style="font-size:14px; font-weight:normal;" href="/unlocked-phones"><%=Unlocked%> Cell Phones</a>
            &nbsp;<span class="siteBlue">&rsaquo;</span>&nbsp;
			<%=strBreadcrumb%>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%" style="padding-top:8px;">
        	<div class="fl">
            <%if listingType = "C" then%>
            	<img src="/images/unlockedphones/banners/banner_logo_<%=carrierid%>.png" border="0" alt="<%=strH1%>" />
            <%else%>
            	<div style="background:url(/images/unlockedphones/banners/brand_left.png) no-repeat; width:200px; height:141px;">
                	<div style="padding:30px 0px 0px 50px;"><img src="/images/brandheaders/we_brand_header_<%=brandid%>_small.jpg" border="0" width="100" height="41"></div>
                </div>
            <%end if%>
			</div>
        	<div class="fl" style="background:url(/images/unlockedphones/banners/right_banner.jpg) no-repeat; width:548px; height:141px; text-align:center;">
            	<div style="padding-top:35px; font-size:24px; color:#fff; font-weight:bold; text-transform:uppercase; letter-spacing:2px;">FREE EXPRESS SHIPPING</div>
            	<div style="padding-top:20px; font-size:24px; color:#fff; font-weight:bold; text-transform:uppercase; letter-spacing:2px;">NO CONTRACTS NEEDED</div>
            </div>
        </td>
    </tr>
    <tr>
        <td id="topPages" align="left" valign="top" width="100%" style="padding-top:15px;">
        	<div class="fl">
            	<div class="fl pageRange" style="color:#333; font-size:16px; font-weight:bold;">1-<%=lastProduct%></div>
                <div class="fl" style="color:#333; font-size:16px; padding-left:4px;">
	                of <strong><%=nProducts%></strong> Products (<a href="javascript:void(0)" class="showAll siteBlue" style="font-size:16px;">Show All</a>)
                </div>
            </div>
        	<div class="pages">
            	<div class="on">1</div>
            	<%for i=2 to lastPage%>
				<div class="off"><%=i%></div>
                <%next%>
            </div>
        </td>
	</tr>
    <tr><td align="left" valign="top" width="100%" style="padding-top:8px; border-bottom:1px solid #ccc;"></td></tr>
    <tr>
        <td align="left" valign="top" width="100%" style="padding-top:8px;">
        	<div class="fl" style="color:#333; font-size:16px; font-weight:bold; color:#333;">Sort By:</div>
            <div class="fl" style="padding:3px 0px 0px 25px;"><a href="javascript:void(0)" <%if strSort = "pop" then%>class="sortingOn"<%else%>class="sortingOff"<%end if%> title="pop">Bestsellers</a></div>
            <div class="fl" style="padding:3px 0px 0px 25px;"><a href="javascript:void(0)" <%if strSort = "review" then%>class="sortingOn"<%else%>class="sortingOff"<%end if%> title="review">Top Rated</a></div>
            <div class="fl" style="padding:3px 0px 0px 25px;"><a href="javascript:void(0)" <%if strSort = "new" then%>class="sortingOn"<%else%>class="sortingOff"<%end if%> title="new">Newest</a></div>
            <div class="fl" style="padding:3px 0px 0px 25px;"><a href="javascript:void(0)" <%if strSort = "low" then%>class="sortingOn"<%else%>class="sortingOff"<%end if%> title="low">Price: Low to High</a></div>
            <div class="fl" style="padding:3px 0px 0px 25px;"><a href="javascript:void(0)" <%if strSort = "high" then%>class="sortingOn"<%else%>class="sortingOff"<%end if%> title="high">Price: High to Low</a></div>
        </td>
	</tr>
    <tr>
        <td id="products" align="center" valign="top" width="100%">
        <%
		lap = 0
		do until rs.eof
			lap = lap + 1
			itemid = rs("itemid")
			itempic = rs("itempic")
			itemdesc = rs("itemdesc")
			price = rs("price_our")
			priceRetail = rs("price_retail")
			reviewCnt = rs("reviewCnt")
			reviewAvg = rs("reviewAvg")
			itemCarrierID = rs("carrierid")
			curPageNum = getPageNum(lap, nProductsPerPage)
			%>
        	<div class="<%if curPageNum > 1 then%>hidden<%end if%> page<%=curPageNum%> item">
            	<div class="carrierLogo">
					<a href="/p-<%=itemid%>-<%=formatSEO(itemdesc)%>" title="<%=itemdesc%>">
                	<img src="/images/unlockedphones/banners/logo_<%=itemCarrierID%>.png" border="0" />
                    </a>
				</div>
            	<div class="fl">
	                <a href="/p-<%=itemid%>-<%=formatSEO(itemdesc)%>" title="<%=itemdesc%>">
                	<img src="/productpics/big/<%=itempic%>" border="0" width="150" alt="<%=itemdesc%>" />
                    </a>
				</div>
                <div class="title">
	                <a href="/p-<%=itemid%>-<%=formatSEO(itemdesc)%>" title="<%=itemdesc%>"><%=itemdesc%></a>
				</div>
                <div class="ffl" style="padding-top:8px;">
                	<div class="fl"><%=getReviewStar(reviewAvg)%></div>
                	<div class="review">(<%=reviewCnt%> reviews)</div>
				</div>
                <div class="ffl" style="padding-top:8px;">
                	<div class="priceRetail"><%=priceRetail%></div>
                	<div class="price"><%=price%></div>
                </div>
                <div class="freeShipping">Free Express Shipping!</div>
                <div class="ffl" style="padding-top:8px; text-align:left;">
                    <!-- Start No Promo Add to Cart -->
                    <div style="display:none;">
                        <a href="javascript:br_cartTracking('add', '<%=itemid%>', '', '<%=replace(itemDesc, "'", "\'")%>', '<%=price%>'); popUpCart_add(<%=itemid%>,1,null);"><img src="/images/unlockedphones/btn_add.png" title="Add To Cart" border="0" /></a>
                    </div>
                    <!-- End No Promo Add to Cart -->
                    <!-- Start With Promo Add to Cart -->
                    <a href="javascript:br_cartTracking('add', '<%=itemid%>', '', '<%=replace(itemDesc, "'", "\'")%>', '<%=price%>'); popUpCartWithPromo_add(<%=itemid%>,1,null);"><img src="/images/unlockedphones/btn_add.png" title="Add To Cart" border="0" /></a>
				</div>
            </div>
    	    <%
			rs.movenext
		loop
		%>
        </td>
	</tr>
    <tr><td align="left" valign="top" width="100%" style="padding-top:15px; border-bottom:1px solid #ccc;"></td></tr>
    <tr>
        <td align="left" valign="top" width="100%" style="padding-top:15px;">
        	<div class="fl">
            	<div class="fl pageRange" style="color:#333; font-size:16px; font-weight:bold;">1-<%=lastProduct%></div>
                <div class="fl" style="color:#333; font-size:16px; padding-left:4px;">
	                of <strong><%=nProducts%></strong> Products (<a href="javascript:void(0)" class="showAll siteBlue" style="font-size:16px;">Show All</a>)
                </div>
            </div>
        	<div class="pages">
            	<div class="on">1</div>
            	<%for i=2 to lastPage%>
				<div class="off"><%=i%></div>
                <%next%>
            </div>
        </td>
	</tr>
    <tr>
    	<td style="padding:30px 0px 30px 0px;">
        	<h1><%=strH1%></h1>
            <br />
        	<p><%=seTopText%></p>
        </td>
    </tr>
    
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<div style="display:none;"><img src="/images/loading4.gif" border="0" /></div>
<script>
	var	nProducts = <%=nProducts%>;
	var nProductsPerPage = <%=nProductsPerPage%>;
	
	var pageClicked = function() {
		var clicked = $(this).html();
		
		$(".pageRange").each(function () {
			if (clicked == "Show All") {
				$(this).html(nProducts);
			} else {
				var start = (nProductsPerPage*clicked)-nProductsPerPage+1;
				var end = nProductsPerPage*clicked;
				if (end > nProducts) end = nProducts;
				$(this).html(start + '-' + end);
			}
		});
		
		$(".pages").children().each(function () {
			if (clicked == "Show All") {
				var current = $(this).html();

				if ($('.page' + current).hasClass('hidden')) {
					$('.page' + current).removeClass('hidden');
				}
				
				if (current == 1) {
					$(this).attr('class', 'on');
				} else {
					$(this).attr('class', 'off');
					$(this).addClass('hidden');
				}
			} else {
				var current = $(this).html();
				if (current == clicked) {
					$(this).attr('class', 'on');
					if ($('.page' + current).hasClass('hidden')) {
						$('.page' + current).removeClass('hidden');
					}
				} else {
					$(this).attr('class', 'off');
					if (!$('.page' + current).hasClass('hidden')) {
						$('.page' + current).addClass('hidden');
					}
				}
			}
		});
		
		$('html,body').animate({ scrollTop: $("#topPages").offset().top}, 'slow');
	}
	
	var fnSort = function() {
		$('#products').html('<img src="/images/loading4.gif" border="0" />');
		window.location = '<%=REWRITE_URL%>?strSort=' + $(this).attr("title");
	}
	
	$(document).ready(function() {
		$(".pages").children().click(pageClicked);
		$(".showAll").click(pageClicked);
		$(".sortingOn").click(fnSort);
		$(".sortingOff").click(fnSort);
	});
</script>
<!--#include virtual="/includes/template/bottom.asp"-->
<%
function getPageNum(nTotalProducts, nProductPerPage)
	getPageNum = int((nTotalProducts / nProductPerPage) + 0.99999)
end function

function getReviewStar(reviewAvg)
	temp = ""
	for i = 1 to 5
		if reviewAvg => i then useStar = "on" else useStar = "off"
		temp = temp & "<img src='/images/ratingStar_" & useStar & ".gif' border='0' />"
	next
	getReviewStar = temp
end function
%>
