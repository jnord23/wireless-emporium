<%
'TEST CUSTOMER PROCESS IN CHECKOUT
on error resume next
response.Cookies("checkoutPage") = 0
response.Cookies("checkoutPage").expires = now
on error goto 0
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_Webloyalty.asp"-->
<!--#include virtual="/includes/asp/inc_GiftCertificate.asp"-->
<%


dim UserIPAddress
	UserIPAddress = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
if UserIPAddress = "" then
  UserIPAddress = Request.ServerVariables("REMOTE_ADDR")
end if
dim isInternalUser : isInternalUser = false

if UserIpAddress = "66.159.49.66" then
	isInternalUser = true
end if

dim nAccountID, nOrderID, nOrderAmount, nOrderTotal
nAccountID = request.querystring("a")
nOrderID = request.querystring("o")
nOrderAmount = request.querystring("d")
nOrderGrandTotal = request.querystring("ppd")
nOrderTotal = 0
usePostPurchase = request.querystring("pp")
reSellerID = 9340

'set known product attributes retrieve appropriate content events
'dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
'dim dicContentEventText: set dicContentEventText = GetContentEventText( dicSeoAttributeInput)

call fOpenConn()


SQL = "SELECT o.*, ot.orderDesc orderTypeDesc FROM we_orders o with (nolock) left join xOrderType ot with (nolock) on o.extOrderType = ot.typeId WHERE o.orderid = '" & nOrderID & "' AND o.accountid = '" & nAccountID & "'"
SQL = SQL & " AND (o.approved = 1 OR o.extOrderType = 3 OR o.extOrderType = 2)"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/?ec=112")
elseif RS("extOrderType") = 3 then
	' Go ahead and display the confirmation page even though the order has not been approved by eBillme.
end if
extOrderType = rs("extOrderType")
extOrderTypeDesc = rs("orderTypeDesc")
auth_transactionid = rs("transactionID")

if (extOrderTypeDesc = "" or extOrderTypeDesc = "0" or isNull(extOrderTypeDesc)) then
  extOrderTypeDesc = "CC"
end if
'if session("adminid") <> "0" then socialShareOn = true
socialShareOn = true
weDataString = ""
weDataString = weDataString & "window.WEDATA = { pageType: 'orderConfirmation', internalUser: "& jsStr(LCase(isInternalUser)) &", storName: 'WirelessEmporium.com', orderData: { cartItems: []	}, account: {} };" & vbcrlf


%>
<!--#include virtual="/includes/asp/inc_receipt_new.asp"-->
<%

dim accountMasterId
accountMasterId = nAccountId
' look for the parentId of the current account
sql = "select parentId from " & getAccountsTable() & " " &_
		"where email = '" & sEmail & "' " &_
		"and accountId = " & nAccountId & " " &_
		"order by dateEntered " 'dateEntered just in case there are multiple records returned... which should NEVER happen
		'TODO: Check for more than 1 record returned and handle with an email to developers with debug information
		set rs = oConn.execute(sql)
		if (not rs.eof or rs.RecordCount > 0) and not isNull(rs("parentId")) then accountMasterId = rs("parentId")

Response.Cookies("user")("id") = accountMasterId
Response.Cookies("user")("email") = sEmail
Response.Cookies("user").Expires = DateAdd("yyyy",5,Now())

weDataString = weDataString & "window.WEDATA.account.email = "& jsStr(sEmail) & ";" & vbcrlf
weDataString = weDataString & "window.WEDATA.account.id = "& jsStr(accountMasterId) & ";" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.orderNumber = "& jsStr(nOrderId) & ";" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.accountId = "& jsStr(nAccountId) & ";" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.totalDiscount = "& jsStr(totalDiscount) & ";" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.taxAmount = "& jsStr(emailOrderTax) & ";" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.shippingAmount = "& jsStr(emailShipFee) & ";" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.grandTotal = "& jsStr(emailOrderGrandTotal) & ";" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.subTotal = "& jsStr(emailSubTotal) & ";" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.couponId = "& jsStr(couponIda) & ";" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.promoCode = "& jsStr(spromoCodea) & ";" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.orderType = "& jsStr(extOrderTypeDesc) & ";" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.shipping = {state: "& jsStr(sstate) & ", city: "& jsStr(sCity) &", zip: "& jsStr(sZip) &"};" & vbcrlf


satelliteTag = "<script src='//assets.adobedtm.com/d0f694a37a1061afe4e18245479cdeaed180db2f/satelliteLib-5ee061b19b32dea49ae509a9092fc999c889172f.js'></script>"
if instr(request.ServerVariables("SERVER_NAME"), "staging") > 0  then
satelliteTag = "<script src='//assets.adobedtm.com/d0f694a37a1061afe4e18245479cdeaed180db2f/satelliteLib-5ee061b19b32dea49ae509a9092fc999c889172f-staging.js'></script>"
end if
ReceiptText = replace(ReceiptText, "[SATELLITE TAG]", satelliteTag)
ReceiptText = replace(ReceiptText, "[WEDATA]", "<script>" & weDataString & "</script>")


%>

<%=ReceiptText%>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

<script>
	$('.divMenu').delay(5000).fadeOut('slow');
</script>
<style>
.divSocContainer {
	position:relative;
}
.divShare {
	width:80px;
	height:30px;
	background:#ff6600;
	float:right;
	-moz-border-radius:8px;
	border-radius:8px;
	text-align:center;
	font-weight:bold;
	line-height:30px;
	position:relative;
	color:#FFF;
	cursor:pointer;
}
.divMenu {
	width:280px;
	height:29px;
	background:#FFF;
	border:1px solid darkgray;
	-moz-border-radius:8px;
	border-radius:8px;
	position:absolute;
	right:0px;
	top:0px;
	/*display:none;*/
	cursor:default;	
}
.divClose {
	color:#FFF;
	background:darkgray;
	cursor:pointer;
	float:right;
	width:80px;
	height:30px;
	border-radius:8px;
	margin-right:-1px;
	margin-top:-1px;
	text-align:center;
	line-height:30px;
	font-weight:bold;
}
.divTwitter {
	width:62px;
	float:left;
	margin-right:8px;
	margin-left:8px;
	margin-top:4px;
}
.divFacebook {
	width:49px;
	overflow:hidden;
	float:left;
	margin-right:10px;
	margin-top:4px;
}
.divPinterest {
	width:60px;
	float:left;
	margin-top:4px;
}
</style>
<% 'Facebook Button %>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>                    
<% 'Twitter Button %>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>                    
<% 'Pinterest Button %>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
<%


'----------------------------------------------
'Tracking for troubleshooting the checkout page
'Determine SessionID
if request.Cookies("mySession") <> "" then
    thisHereSession = request.Cookies("mySession")
elseif request.QueryString("passSessionID") <> "" then
    thisHereSession = request.QueryString("passSessionID")
else
    thisHereSession = 0
end if
thisHereHost = request.ServerVariables("HTTP_HOST")
%>
<script src="/framework/userinterface/js/ajax.js"></script>
<script>
function trackingLoad() {
	ajax('/ajax/ajaxTracking.asp?sessionID=<%=thisHereSession%>&visitedConfirm=1','trackingZone');
}

setTimeout("trackingLoad()",1000)
</script>
<div id="trackingZone"<% if thisHereHost <> "staging.wirelessemporium.com" then %> style="display:none;"<% end if %>><%=request.ServerVariables("SCRIPT_NAME")%></div>
<%
'----------------------------------------------



session.abandon

dim vendororder
on error resume next
vendororder = Request.Cookies("vendororder")
on error goto 0
' If this is an Affiliate Referral from Nextag, add the Survey code
if vendororder = "nextag" then
	response.write "<link rel=""stylesheet"" href=""https://merchants.nextag.com/serv/main/buyer/dhtmlpopup/dhtmlwindow.css"" type=""text/css"" />" & vbcrlf
	response.write "<script type=""text/javascript"">" & vbcrlf
	response.write "var seller_id = 189395;" & vbcrlf
	response.write "//popup_left and popup_top default to the center of browser window if commented" & vbcrlf
	response.write "//var popup_left=50;" & vbcrlf
	response.write "//var popup_top=50;" & vbcrlf
	response.write "//var popup_width=345; // default 345" & vbcrlf
	response.write "//var popup_height=205; // default 205" & vbcrlf
	response.write "//var popup_resize=0; // default 0" & vbcrlf
	response.write "document.write('<'+ 'script type=""text/javascript"" src=""https://merchants.nextag.com/seller/review/popup_include.js""><\/script>'); </script>" & vbcrlf
elseif vendororder = "pricegrabber" then
%>
<script type="text/javascript">
<!--// Adjust the values of popup_pos_x, popup_pos_y to change the location of the popup layer on your confirmation page
popup_pos_x=600;
popup_pos_y=300;
// fill in the order number below
popup_order_number = '';
// fill in the email address below
popup_email = '';
//-->
if (navigator.appName.indexOf("Microsoft") == -1 ) {self.blur();}
wl_popup.resizeTo(screen.availWidth * .92, screen.availHeight * .92);
</script>
<!-- PriceGrabber Merchant Evaluation Code -->
<script type="text/javascript" charset="UTF-8" src="https://www.pricegrabber.com/rating_merchrevpopjs.php?retid=2008"></script>
<noscript><a href="http://www.pricegrabber.com/rating_merchrev.php?retid=2008" target=_blank>
<img src="https://images.pricegrabber.com/images/mr_noprize.jpg" border="0" width="272" height="238" alt="Merchant Evaluation"></a></noscript>
<!-- End PriceGrabber Code -->
<%
end if

'destroys everything in the session
dim sessionA(300), sessionB, sessionC, sessionI
For Each sessionI in Session.Contents
	sessionA(sessionB) = sessionI
	sessionB = sessionB + 1
Next
for sessionC = 0 to sessionB - 1
	if sessionA(sessionC) <> "oConn" and sessionA(sessionC) <> "Blocked" and sessionA(sessionC) <> "fname" and sessionA(sessionC) <> "lname" and sessionA(sessionC) <> "accountid" and sessionA(sessionC) <> "validUser" then
		Session.Contents.Remove(sessionA(sessionC))
	end if
next

' MCNOTE!!!  Get rid of this and replace with something that gets rid of all cookies including FreeItem

'zero-out all cart cookies
' START basket grab
dim basketitem
for basketitem = 1 to 100
	if Request.Cookies("itemID(" & basketitem & ")") <> "" and Request.Cookies("itemID(" & basketitem & ")") <> "0" then
		Response.Cookies("itemID(" & basketitem & ")") = 0
		Response.Cookies("itemQty(" & basketitem & ")") = 0
		Response.Cookies("relatedID(" & basketitem & ")") = 0
	end if
next
' END basket grab

'call OutputPixel( dicContentEventText) '--thank you pixel goes here

'set dicSeoAttributeInput = nothing
'set dicContentEventText = nothing
%>
<div id="testZone"></div>

<script src="/includes/js/edutl.js"></script>
<script language="javascript" src="/includes/js/shoprunner_init.js"></script>

<%'500 Friends%>
<script src="https://d3aa0ztdn3oibi.cloudfront.net/javascripts/ff.loyalty.widget.js" type="text/javascript"></script>
<script>
_ffLoyalty.initialize("<%=WE_500_ACCID%>");
</script>
<%
call printPixel(3)
set fs = Server.CreateObject("Scripting.FileSystemObject")
str500Products = "["
sql = "SELECT d.brandName, e.modelName, A.itemid,A.PartNumber,A.itemdesc,A.price_our,A.itempic,c.id,c.musicSkinsID,c.artist + '-' + c.designName as prodDesc,c.price_we,c.defaultImg,c.image,B.quantity FROM we_orderdetails B left join we_items A ON B.itemid = A.itemid left join we_items_musicSkins C on B.itemID = C.id left join we_brands d on a.brandID = d.brandID left join we_models e on a.modelID = e.modelID WHERE B.itemID > 0 and B.orderid = '" & nOrderId & "'"
session("errorSQL") = sql
set rs = oConn.execute(sql)
if not rs.eof then
	lap = 0
	do until rs.eof
		lap = lap + 1
		if isnull(rs("itemid")) then
			productpageToken = "pm"
			nID = rs("id")
			nPartNumber = rs("musicSkinsID")
			nDesc = rs("prodDesc")
			nQty = rs("quantity")
			nPrice = rs("price_we")

			picBase = "/productpics/musicSkins/musicSkinsSmall/"
			itempic = rs("image")
			if not fs.FileExists(server.mappath(picBase & itempic)) then
				picBase = "/productpics/musicSkins/musicSkinsDefault/thumbs/"
				itemPic = rs("defaultimg")
			end if
			nItempic = picBase & itempic
		else
			productpageToken = "p"
			useBrandName = rs("brandName")
			useModelName = rs("modelName")
			nID = rs("itemid")
			nPartNumber = rs("PartNumber")
			partNumber = nPartNumber
			nDesc = insertDetailsAdv(rs("itemdesc"),useBrandName,useModelName)
			nQty = rs("quantity")
			nPrice = rs("price_our")
			nItempic = "/productpics/thumb/" & rs("itempic")
		end if

		if itemids = "" then 
			itemids = nID
		else
			itemids = itemids & "," & nID
		end if

		if lap = 1 then
			str500Products = str500Products & "{'name':'" & nDesc & "', 'url':'http://www.wirelessemporium.com/" & productpageToken & "-" & nID & "-" & formatSEO(nDesc) & ".asp', 'description':'" & nDesc & "','image_url':'https://www.wirelessemporium.com" & nItempic & "'}"
		else
			str500Products = str500Products & ",{'name':'" & nDesc & "', 'url':'http://www.wirelessemporium.com/" & productpageToken & "-" & nID & "-" & formatSEO(nDesc) & ".asp', 'description':'" & nDesc & "','image_url':'https://www.wirelessemporium.com" & nItempic & "'}"
		end if
		if lap >= 5 then exit do
		rs.movenext
	loop
end if
str500Products = str500Products & "]"

if usePostPurchase = "Y" and (prepInt(extOrderType) = 0 or prepInt(extOrderType) = 1) then
%>
	<%'Reseller Ratings Styles%>
    <link rel="stylesheet" href="https://www.resellerratings.com/images/js/dhtml_survey.css" type="text/css" />
    <script type="text/javascript">
        seller_id = <%=reSellerID%>;
        rrAutoShow = false;
        document.write('<'+ 'script type="text/javascript" src="https://www.resellerratings.com/images/js/popup_include.js"><\/script>');
    </script>

	<script>
		function extraPopups() {
			nRnd = Math.floor(Math.random()*999999);

			if ((nRnd % 2)==1) {
				_ffLoyalty.displayPostPurchaseWidget("cZix2UbnE5", {'email': '<%=sEmail%>', 'event_id': '<%=nOrderID%>', 'value': '<%=round(nOrderAmount)%>', 'products': <%=str500Products%>, 'referral_tracking': true});
			} else {
				var seller_id = <%=reSellerID%>;
				rrAutoShow = true;
				__rr_showPopup();
			}
		}
		
        window.onload = function() { 
            pp_doPopup();
        }
    
        function doSubmit() {
            frm = document.frmPostPurchase;
            if (CurrencyFormatted(frm.addlGrandTotal.value) == "0.00") {
                alert("Plesae select the items");
                return false;
            }
            return true;
        }
        
        function pp_closePopup() {
            document.getElementById("popCover").style.display = "none";
            document.getElementById("popBox").style.display = "none";
            document.getElementById("popBox").innerHTML = "";
			extraPopups();
        }
        
        function pp_doPopup() {
            document.getElementById("popCover").style.display = "";
            document.getElementById("popBox").style.display = "";
            document.getElementById("popBox").innerHTML = '<div style="width:700px; background-color:#fff; margin-left:auto; margin-right:auto; margin-top:100px;"><img src="/images/loading4.gif" border="0" /></div>';
            ajax('/ajax/postPurchase?itemids=<%=itemids%>&orderid=<%=nOrderID%>&transid=<%=auth_transactionid%>&extordertype=<%=prepInt(extOrderType)%>','popBox');
        }
        
        function updatePPTotal() {
            frm = document.frmPostPurchase;
            nCount = frm.hidCount.value;
            taxTotal = frm.taxTotal.value;
            
            var addlTotal = parseFloat(0);
            var addlTax = parseFloat(0);
            var addlShipping = parseFloat(0);
            var itemTotal = parseFloat(0);
            var retailTotal = parseFloat(0);
            var itemSave = parseFloat(0);
            var qtyTotal = parseFloat(0);
    
            for (i=1; i<= nCount; i++) {
                itemChecked = document.getElementById('id_chkItem'+i).checked;
                price_co = document.getElementById('id_itemPrice'+i).value;
                price_retail = document.getElementById('id_itemRetail'+i).value;
                qty = document.getElementById('id_qty'+i).value;			
                
                if (itemChecked) {
                    if ((!isNaN(qty))&&(qty != '')&&(qty != 0)) {		
                        itemTotal = parseFloat(itemTotal) + parseFloat(price_co * qty);
                        retailTotal = parseFloat(retailTotal) + parseFloat(price_retail * qty);
                        itemSave = parseFloat(itemSave) + (parseFloat(retailTotal) - parseFloat(itemTotal));
                        qtyTotal = parseInt(qtyTotal) + parseInt(qty);
                    }
                }
            }
            
            if (parseFloat(taxTotal) > 0) {
                addlTax = parseFloat(itemTotal * <%=Application("taxMath")%>);
            }
            
            addlShipping = parseFloat(0);
            addlTotal = parseFloat(itemTotal) + parseFloat(addlShipping) + parseFloat(addlTax);
    
            document.getElementById('addlSave').innerHTML = CurrencyFormatted(itemSave);
            document.getElementById('addlTotal').innerHTML = CurrencyFormatted(itemTotal);
            document.getElementById('addlTaxTotal').innerHTML = CurrencyFormatted(addlTax);
            document.getElementById('addlGrandTotal').innerHTML = CurrencyFormatted(addlTotal);
            
            frm.addlGrandTotal.value = CurrencyFormatted(addlTotal);
        }
        
        function CurrencyFormatted(amount) {
            var i = parseFloat(amount);
            if(isNaN(i)) { i = 0.00; }
            var minus = '';
            if(i < 0) { minus = '-'; }
            i = Math.abs(i);
            i = Math.round(i * 100) / 100;
            s = new String(i);
            if(s.indexOf('.') < 0) { s += '.00'; }
            if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
            s = minus + s;
            return s;
        }		
    </script>
<%
else
	'get random number
	randomize
	rndPop = Int(Rnd * 100) + 1
	if rndPop < 51 then
%>
        <script type="text/javascript">
	        seller_id = <%=reSellerID%>;
	        document.write('<'+ 'script type="text/javascript" src="https://www.resellerratings.com/images/js/popup_include.js"><\/script>');
        </script>
<%
	else '500 friends
%>
		<script>
			_ffLoyalty.displayPostPurchaseWidget("cZix2UbnE5", {'email': '<%=sEmail%>', 'event_id': '<%=nOrderID%>', 'value': '<%=round(nOrderAmount)%>', 'products': <%=str500Products%>, 'referral_tracking': true});
		</script>
<%
	end if '500 friends end
end if 'postpurchase end

on error resume next

sql = 	"select	isnull(a.cancelled, 0) cancelled, b.bAddress1" & vbcrlf & _
		"from	we_orders a join v_accounts b" & vbcrlf & _
		"	on	a.accountid = b.accountid and a.store = b.site_id" & vbcrlf & _
		"where	a.orderID = '" & nOrderID & "' and a.store = 0"
set rsGoogle = oConn.execute(sql)		
		
if not rsGoogle.eof then
	if instr(request.ServerVariables("SERVER_NAME"), "staging.") = 0 and rsGoogle("bAddress1") <> "4040 N. Palm St." then
	%>
		<!-- BEGIN: Google Trusted Store -->
		<script type="text/javascript">
			var gts = gts || [];
			
			gts.push(["id", "230549"]);
			<%
			trustStoreItemID = ""
			sql = 	"exec sp_googleTrustedItem 0"
			set rsTrust = oConn.execute(sql)
			if not rsTrust.eof then trustStoreItemID = rsTrust("itemid")
			%>
			gts.push(["google_base_offer_id", "<%=trustStoreItemID%>"]);
			gts.push(["google_base_subaccount_id", "8589772"]);
			gts.push(["google_base_country", "US"]);
			gts.push(["google_base_language", "EN"]);
			
			(function() {
				var scheme = (("https:" == document.location.protocol) ? "https://" : "http://");
				var gts = document.createElement("script");
				gts.type = "text/javascript";
				gts.async = true;
				gts.src = scheme + "www.googlecommerce.com/trustedstores/gtmp_compiled.js";
				var s = document.getElementsByTagName("script")[0];
				s.parentNode.insertBefore(gts, s);
			})();
		</script>
		<!-- END: Google Trusted Store -->
		
		<%
		sql = 	"select	a.orderid" & vbcrlf & _
				"	,	isnull(sum(case when c.partnumber like 'WCD-%' then 1 else 0 end), 0) custom" & vbcrlf & _
				"	,	isnull(sum(case when d.typeid = 16 then 1 else 0 end), 0) phone" & vbcrlf & _
				"	,	isnull(sum(case when d.vendor in ('CM', 'DS', 'MLD') then 1 else 0 end), 0) dropship" & vbcrlf & _
				"from	we_orders a join v_accounts b" & vbcrlf & _
				"	on	a.accountid = b.accountid and a.store = b.site_id join we_orderdetails c" & vbcrlf & _
				"	on	a.orderid = c.orderid join we_items d" & vbcrlf & _
				"	on	c.itemid = d.itemid" & vbcrlf & _
				"where	a.orderid = '" & nOrderID & "'" & vbcrlf & _
				"	and	(a.extordertype is null or a.extordertype not in (4,5,6,7,8,9))" & vbcrlf & _
				"group by a.orderid"
	'	response.write "<pre>" & sql & "</pre>"
		set rsCheck = oConn.execute(sql)
		
		if not rsCheck.eof then
			if rsCheck("custom") > 0 then
				estShipDate = date+10
			elseif rsCheck("phone") > 0 then
				estShipDate = date+7
			elseif rsCheck("dropship") > 0 then
				estShipDate = date+10
			else
				estShipDate = date+5
			end if
	
			strEstShipDate = year(estShipDate) & "-" & right("00" & month(estShipDate),2) & "-" & right("00" & day(estShipDate),2)
			%>
			<!-- START Trusted Stores Order -->
			<div id="gts-order" style="display:none;">
				<!-- start order and merchant information -->
				<span id="gts-o-id"><%=nOrderID%></span>
				<span id="gts-o-domain">www.wirelessemporium.com</span>
				<span id="gts-o-email"><%=sEmail%></span>
				<span id="gts-o-country">US</span>
				<span id="gts-o-currency">USD</span>
				<span id="gts-o-total"><%=nOrderAmount%></span>
				<span id="gts-o-discounts"><%=round(prepInt(nOrderGrandTotal) - prepInt(mySubtotal) - prepInt(nOrderTax) - prepInt(nOrdershippingfee) - prepInt(nBuysafeamount),2)%></span>
				<span id="gts-o-shipping-total"><%=nOrdershippingfee%></span>
				<span id="gts-o-tax-total"><%=nOrderTax%></span>
				<span id="gts-o-est-ship-date"><%=strEstShipDate%></span>
				<%if rsCheck("custom") > 0 or rsCheck("dropship") > 0 then%>
				<span id="gts-o-has-preorder">Y</span>            
				<%else%>
				<span id="gts-o-has-preorder">N</span>
				<%end if%>
				<span id="gts-o-has-digital">N</span>
				<!-- end order and merchant information -->
				
				<!-- start repeated item specific information -->
				<!-- item example: this area repeated for each item in the order -->
			<%
		'	sql = "SELECT d.brandName, e.modelName, A.itemid,A.PartNumber,A.itemdesc,A.price_our,A.itempic,c.id,c.musicSkinsID,c.artist + '-' + c.designName as prodDesc,c.price_we,c.defaultImg,c.image,B.quantity FROM we_orderdetails B left join we_items A ON B.itemid = A.itemid left join we_items_musicSkins C on B.itemID = C.id left join we_brands d on a.brandID = d.brandID left join we_models e on a.modelID = e.modelID WHERE B.itemID > 0 and B.orderid = '" & nOrderId & "'"
			sql	=	"select	d.brandname, e.modelname, a.itemid, a.partnumber, a.itemdesc, a.price_our, a.itempic, b.quantity " & vbcrlf & _
					"from	we_orderdetails b left join we_items a " & vbcrlf & _
					"	on	b.itemid = a.itemid left join we_brands d " & vbcrlf & _
					"	on	a.brandid = d.brandid left join we_models e " & vbcrlf & _
					"	on	a.modelid = e.modelid " & vbcrlf & _
					"where	b.itemid > 0 and b.orderid = '" & nOrderID & "'"
	'		response.write "<pre>" & sql & "</pre>"
			set rs = oConn.execute(sql)
			if not rs.eof then
				lap = 0
				do until rs.eof
					lap = lap + 1
		'			if isnull(rs("itemid")) then
		'				productpageToken = "pm"
		'				nID = rs("id")
		'				nPartNumber = rs("musicSkinsID")
		'				nDesc = rs("prodDesc")
		'				nQty = rs("quantity")
		'				nPrice = rs("price_we")
		'
		'				picBase = "/productpics/musicSkins/musicSkinsSmall/"
		'				itempic = rs("image")
		'				if not fs.FileExists(server.mappath(picBase & itempic)) then
		'					picBase = "/productpics/musicSkins/musicSkinsDefault/thumbs/"
		'					itemPic = rs("defaultimg")
		'				end if
		'				nItempic = picBase & itempic
		'			else
						productpageToken = "p"
						useBrandName = rs("brandName")
						useModelName = rs("modelName")
						nID = rs("itemid")
						nPartNumber = rs("PartNumber")
						partNumber = nPartNumber
						nDesc = insertDetailsAdv(rs("itemdesc"),useBrandName,useModelName)
						nQty = rs("quantity")
						nPrice = rs("price_our")
						nItempic = "/productpics/thumb/" & rs("itempic")
		'			end if
					%>
					<span class="gts-item">
						<span class="gts-i-name"><%=nDesc%></span>
						<span class="gts-i-price"><%=nPrice%></span>
						<span class="gts-i-quantity"><%=nQty%></span>
						<span class="gts-i-prodsearch-id"><%=nID%></span>
						<span class="gts-i-prodsearch-store-id">8589772</span>
						<span class="gts-i-prodsearch-country">US</span>
						<span class="gts-i-prodsearch-language">EN</span>
					</span>
					<%
					rs.movenext
				loop
			end if
			%>
			</div>
			<!-- END Trusted Stores -->
	<%
		end if
	end if
end if	
on error goto 0
call CloseConn(oConn)
%>
<script type="text/javascript">_satellite.pageBottom();</script>
<div id="popCover" style="height:100%; display:none; width:100%; background-color:#333; position:fixed; left:0px; top:0px; z-index:3000; opacity:0.6; filter:alpha(opacity=60);">&nbsp;</div>
<div id="popBox" style="height:2000px; display:none; width:100%; position:absolute; left:0px; top:0px; z-index:3001; margin:auto; text-align:center;"></div>
</body>
</html>