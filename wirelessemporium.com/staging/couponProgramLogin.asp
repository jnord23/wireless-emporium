<%
	response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
    <%
	if not isnull(session("userMsg")) then
		userMsg  = session("userMsg")
		session("userMsg") = null
	%>
    <tr><td align="center" style="font-weight:bold; color:#F00;"><%=userMsg%></td></tr>
    <%
	end if
	%>
    <tr>
        <td style="font-size:12px; color:#999;">
        	<div style="float:left; padding:30px 0px 0px 60px;">
	        	<h1 style="color:#666; font-size:36px; padding:0px; margin:0px;">Coupon Program Login</h1>
    	        Login to share your coupon code with others - Check your code performance - Cash in your code
            </div>
            <div style="float:right; padding:10px 20px 0px 0px;"><img src="/images/icons/couponProgramHeader.jpg" border="0" /></div>
        </td>
    </tr>
    <tr>
    	<td valign="top">
        	<form action="/couponProgram.asp" method="post">
            <div style="height:194px; width:594px; background-image:url(/images/backgrounds/couponProgramLogin.gif); margin-left:auto; margin-right:auto; margin-top:20px;">
            	<div style="font-weight:bold; font-size:18px; color:#ccc; border-bottom:2px groove #333; text-align:center; padding:15px 10px 0px 10px; height:30px; width:530px; margin-left:auto; margin-right:auto;">Coupon Program Account Management Login:</div>
                <div style="display:block; width:100%; height: 40px;">
                    <div style="float:left; height:40px; width:200px; padding:10px 0px 0px 40px;">
                        <div style="float:left; padding:5px 5px 0px 0px;"><img src="/images/icons/couponProgramArrow.gif" border="0" /></div>
                        <div style="float:left; color:#ccc; font-size:12px;">Enter your coupon code:</div><br />
                        <div style="float:left; padding-top:5px;"><input type="text" name="couponCode" value="" size="20" /></div><br />
                        <div style="float:left; padding-top:15px;"><input type="image" src="/images/buttons/couponProgramLoginBttn.jpg" border="0" /></div>
                    </div>
                    <div style="float:right; height:40px; width:200px; padding:10px 40px 0px 0px; color:#CCC; font-size:14px;">
                        <span style="font-weight:bold;">Have Questions?</span><br />
                        Send an email to<br />
                        <a href="mailto:coupon@wirelessemporium.com" style="color:#CCC; text-decoration:none;">coupon@wirelessemporium.com</a>
                    </div>
                </div>
            </div>
            </form>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->
