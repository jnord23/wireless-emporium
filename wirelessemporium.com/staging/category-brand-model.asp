<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_minicart.asp"-->
<!--#include virtual="/includes/asp/inc_tollfree.asp"-->
<%
response.buffer = true
dim productListingPage
productListingPage = 1
googleAds = 1

dim modelid, categoryid
modelid = request.querystring("modelid")
if modelid = "" or not isNumeric(modelid) then
	call PermanentRedirect("/")
end if
categoryid = request.querystring("categoryid")
if categoryid = "" or not isNumeric(categoryid) then
	call PermanentRedirect("/")
end if
brandID = request.querystring("brandID")
if brandID = "" or not isNumeric(brandID) then
	call PermanentRedirect("/")
end if

'if brandID = 7 or brandID = 10 or brandID = 19 or brandID = 11 or brandID = 2 or brandID = 1 or brandID = 15 or brandID = 3 or brandID = 4 or brandID = 6 or brandID = 9 or brandID = 16 or brandID = 18 then
if categoryid <> 14 then
	call fOpenConn()
	sql = "select a.modelName, b.brandName, c.typeName from we_models a join we_brands b on b.brandID = '" & brandID & "' join we_types c on c.typeID = '" & categoryid & "' where modelID = '" & modelID & "'"
	session("errorSQL") = sql
'	set rs = Server.CreateObject("ADODB.Recordset")
'	rs.open SQL, oConn, 3, 3
	set	rs = oConn.execute(SQL)
	
	if not rs.eof then
		call PermanentRedirect("/sb-" & brandID & "-sm-" & modelID & "-sc-" & categoryid & "-" & formatSEO(RS("typename")) & "-" & formatSEO(rs("brandname")) & "-" & formatSEO(rs("modelName")) & ".asp")
	else
		call PermanentRedirect("/")
	end if
	call fCloseConn()

end if

dim REWRITE_URL, design
REWRITE_URL = Request.ServerVariables("HTTP_X_REWRITE_URL")
if inStr(Request.ServerVariables("HTTP_X_REWRITE_URL"),"?") > 0 then REWRITE_URL = left(Request.ServerVariables("HTTP_X_REWRITE_URL"),inStr(Request.ServerVariables("HTTP_X_REWRITE_URL"),"?")-1)
design = request.querystring("design")

leftGoogleAd = 1

call fOpenConn()
if modelid <> "0" then
	SQL = "SELECT B.modelName, B.modelImg, B.excludePouches, B.includeNFL, B.includeExtraItem, C.brandID, C.brandName"
	SQL = SQL & " FROM we_models B INNER JOIN we_brands C ON B.brandid = C.brandid"
	SQL = SQL & " WHERE modelid = '" & modelid & "'"
else
	SQL = "SELECT TOP 1 'UNIVERSAL' AS modelName, 'UNIVERSAL.JPG' AS modelImg, 0 AS excludePouches, 0 AS includeNFL, NULL AS includeExtraItem, 0 AS brandID, '' AS brandName"
	SQL = SQL & " FROM we_models"
end if
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
else
	dim brandName, brandID, modelName, modelImg, excludePouches, includeNFL, includeExtraItem
	brandName = RS("brandName")
	brandID = RS("brandID")
	modelName = RS("modelName")
	modelImg = RS("modelImg")
	excludePouches = RS("excludePouches")
	includeNFL = RS("includeNFL")
	includeExtraItem = RS("includeExtraItem")
end if

if modelid <> "0" then
	SQL = "SELECT A.itemID, A.brandID, A.modelID, A.typeID, A.subtypeID, A.itemDesc, A.itemPic, A.flag1, A.price_Retail, A.price_Our, isnull(A.inv_qty, 0) inv_qty, A.ItemKit_NEW, A.hotDeal, A.hideLive,"
	SQL = SQL & " C.brandID, C.brandName, D.typeName"
	SQL = SQL & " FROM ((we_items A"
	SQL = SQL & " INNER JOIN we_models B ON A.modelID = B.modelID)"
	SQL = SQL & " INNER JOIN we_brands C ON A.brandid = C.brandid)"
	SQL = SQL & " INNER JOIN we_types D ON A.typeid = D.typeid"
else
	SQL = "SELECT A.itemID, A.brandID, A.modelID, A.typeID, A.subtypeID, A.itemDesc, A.itemPic, A.flag1, A.price_Retail, A.price_Our, isnull(A.inv_qty, 0) inv_qty, A.ItemKit_NEW, A.hotDeal, A.hideLive,"
	SQL = SQL & " 0 AS brandID, 'UNIVERSAL' AS brandName, D.typeName"
	SQL = SQL & " FROM we_items A INNER JOIN we_types D ON A.typeid = D.typeid"
end if
if categoryid = "12" then
	SQL = SQL & " WHERE A.vendor = 'CM' OR (A.modelid = '" & modelid & "' AND A.typeid = '" & categoryid & "' AND A.hideLive = 0 AND A.inv_qty <> 0 AND A.price_Our > 0)"
else
	SQL = SQL & " WHERE A.modelid = '" & modelid & "'"
	SQL = SQL & " AND A.typeid = '" & categoryid & "'"
	SQL = SQL & " AND A.hideLive = 0"
	SQL = SQL & " AND A.inv_qty <> 0"
	if categoryid <> "5" and modelid <> "0" then SQL = SQL & " AND patindex('%All %',B.modelname) = 0"
	SQL = SQL & " AND A.price_Our > 0"
end if
SQL = SQL & " ORDER BY A.flag1 DESC"
session("errorSQL") = sql
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.eof then
	call fCloseConn()
	call PermanentRedirect("/")
else
	dim categoryName
	categoryName = RS("typeName")
end if

SQL = "SELECT A.itemID, A.brandID, A.modelID, A.typeID, A.subtypeID, A.itemDesc, A.itemPic, A.flag1, A.price_Retail, A.price_Our, A.inv_qty, A.ItemKit_NEW, A.hotDeal, A.hideLive, A.DesignType, "
if modelid <> "0" then
	SQL = SQL & "B.modelName, B.modelImg, B.excludePouches, B.includeNFL, B.includeExtraItem, C.brandID, C.brandName, D.typeName"
else
	SQL = SQL & "'UNIVERSAL' AS modelName, 'UNIVERSAL.JPG' AS modelImg, 0 AS excludePouches, 0 AS includeNFL, NULL AS includeExtraItem, 0 AS brandID, 'UNIVERSAL' AS brandName, D.typeName"
end if
if modelid <> "0" then
	SQL = SQL & " FROM we_Items A"
	SQL = SQL & " INNER JOIN we_models B ON A.modelID = B.modelID"
	SQL = SQL & " INNER JOIN we_brands C ON A.brandid = C.brandid"
	SQL = SQL & " INNER JOIN we_types D ON A.typeid = D.typeid"
else
	SQL = SQL & " FROM we_items A INNER JOIN we_types D ON A.typeid = D.typeid"
end if
SQL = SQL & " WHERE A.itemid IN"
SQL = SQL & " (SELECT itemID FROM we_items W"
SQL = SQL & " WHERE (W.Sports=0 AND W.typeID='" & categoryid & "' AND W.modelID='" & modelid & "' AND (W.inv_qty <> 0 OR W.ItemKit_NEW IS NOT NULL)) AND W.price_Our > 0"
SQL = SQL & " UNION ALL"
SQL = SQL & " SELECT R.itemid FROM we_items I INNER JOIN we_relatedItems R ON R.itemid = I.itemid"
SQL = SQL & " WHERE (I.Sports=0 AND R.typeid='" & categoryid & "' AND R.modelid='" & modelid & "' AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL)) AND I.price_Our > 0)"
SQL = SQL & " AND A.hideLive = 0 AND (A.inv_qty <> 0 OR A.ItemKit_NEW IS NOT NULL)"
SQL = SQL & " ORDER BY A.flag1 DESC"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 2

if RS.eof and categoryid <> "12" then
	call fCloseConn()
	call PermanentRedirect("/")
end if

dim SEtitle, SEdescription, SEkeywords, h1, topText
topText = ""

if categoryid = "8" then
	SEtitle = "Cell Phone Charms & Bling Kits: Crystal Charms, Phone Pets Charms at Wholesale Prices"
	SEdescription = "Get the best deals on cell phone charms & bling kits like crystal charms, phone pets charms at WirelessEmporium.com - The #1 store online for cell phone accessories at wholesale prices."
	SEkeywords = "cell phone charms, cell phone bling, cell bling, crystal cell phone charms, bling kits, cell phone bling kits"
	topText = "GET NOTICED every time you raise your phone to your ear! Bling your cell phone with popular <a class=""topText-link"" href=""/p-7679-pink-and-clear-rhinestone-crystals--300-count-peel--n--stick-.asp"" title=""Cell Phone Bling"">Cell Phone Bling</a> Kits & <a class=""topText-link"" href=""http://www.wirelessemporium.com/p-18634-crystal-charms---heart-in-heart.asp"" title=""Cell Phone Charms"">Cell Phone Charms</a> AT WHOLESALE COST! Cell phone bling kits have grown in popularity and are used by cell phone users and famous stars across the world. They are a quick and easy way to personalize your cell phone. Best of all, when you buy your bling kit at WirelessEmporium.com, you know you're getting the BEST DEAL ONLINE ?GUARANTEED!</p>"
	topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">We offer a wide range of cell phone charms like crystal charms & phone pets charms. Apart from cell phone charms & bling, we offer a complete line of accessories for your cell phone at WHOLESALE PRICES! Buy Phone Pets Charms like the pig, black cat, mouse or yellow, pink, blue <a class=""topText-link"" href=""/p-7676-purple-and-clear-rhinestone-crystals--300-count-peel--n--stick-.asp"" title=""Crystal Cell Phone Charms"">crystal cell phone charms</a> at wholesale prices!"
elseif categoryid = "14" then
	SEtitle = "Cell Phone Charms & Stickers: Stylish Cell Phone Bling Kits at Wireless Emporium"
	SEdescription = "Buy Fashionable Cell Phone Charms & Stickers at Wireless Emporium. We are the biggest store for Cell Phone Bling Kits & Jewels at Wholesale Prices and Free Shipping!"
	SEkeywords = "cell phone charms, cell phone Stickers, cute cell phone charms, cell phone charms wholesale, cell phone jewels, flashing cell phone charms"
	topText = "Let your phone shine with cell phone charms! Choose from dozens of cute and cuddly, bright and shiny charms for your cell phone. WirelessEmporium.com is the biggest cell phone accessory store online. We have a huge cell phone bling & charm collection including many other <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Cell Phone Accessories and Charms"">cell phone accessories</a> for all cell phone brands including Nokia, Samsung, Motorola, LG, Apple & many more.</p>"
	topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">Buy Cellphone Blings & Charms to accessorize your phone at WirelessEmporium.com! Bling your phone with a wide array of bling kits and bling accessories. Tag your phone with a cute, colorful personalized charm. Pay prices that you simply won't believe and get FREE SHIPPING to boot!"
end if

dim modelLink
if modelID = 940 then
	modelLink = "http://www.wirelessemporium.com/apple-ipad-accessories.asp"
else
	modelLink = "http://www.wirelessemporium.com/T-" & modelID & "-cell-accessories-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".asp"
end if

if modelid = "987" or modelid = "988" or modelid = "1009" or modelid = "1015" then
	SEtitle = "Samsung " & modelName & " " & nameSEO(categoryName) & " ?Buy Exclusive Samsung " & modelName & " Accessories"
	SEdescription = "Buy Quality Samsung " & modelName & " " & nameSEO(categoryName) & " ?WirelessEmporium carries your favorite Samsung " & modelName & " " & singularSEO(categoryName) & " at discounted rates and Free Shipping!"
	SEkeywords = "samsung " & modelName & " " & nameSEO(categoryName) & ", samsung " & modelName & " " & singularSEO(categoryName) & ", " & modelName & " " & singularSEO(categoryName) & ", samsung accessories, samsung " & modelName & " cell phone " & nameSEO(categoryName) & ", " & nameSEO(categoryName) & " for samsung " & modelName
	h1 = "Samsung " & modelName & " " & nameSEO(categoryName)
	select case categoryid
		case "6"
			topText = "Choose from a wide array of our quality Samsung " & modelName & " holsters and belt clips to safely and easily secure your Samsung " & modelName & " phone. We offer Samsung " & modelName & " belt holsters at prices you won't find anywhere else! Protect your Samsung " & modelName & " cell phone with our low priced Samsung " & modelName & " cell phone holsters! They clip easily to belts, purses, pockets, and protect your phone through your active lifestyle. An adjustable belt clip that rotates and locks in six positions makes it easy for you to adjust your phone the way you need it. Our Samsung " & modelName & " cell phone belt clips are stylish, too! Thousands of customers trust us to offer high-quality Samsung " & modelName & " holsters at the lowest possible prices, and we're dedicated to satisfying the needs of all our customers! Browse through our Samsung " & modelName & " cell phone belt clip selection today.</p>"
			topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">Since 2001, Wireless Emporium has been offering wholesale <a class=""topText-link"" href=""http://www.wirelessemporium.com"" title=""Cell Phone Accessories"">cell phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-" & modelid & "-cell-accessories-samsung-" & formatSEO(modelName) & ".asp"" title=""Samsung " & modelName & " Accessories"">Samsung " & modelName & " accessories</a> like holsters & belt clips for your Samsung " & modelName & " phone at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
		case "7"
			topText = "Choose from a wide array of our quality Samsung " & modelName & " leather cases to safely and easily secure your Samsung " & modelName & " phone. We offer prices you won't find anywhere else! Protect your Samsung " & modelName & " cell with our low priced Samsung " & modelName & " cases! They clip easily to belts, purses, pockets, and protect your phone through your active lifestyle. Our Samsung leather cases are stylish, too! Browse through our <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-" & modelid & "-cell-accessories-samsung-" & formatSEO(modelName) & ".asp"" title=""Samsung " & modelName & " Accessories"">Samsung " & modelName & " Accessories</a> selection today. Buy a quality Samsung " & modelName & " cell phone leather case today and we'll ship it to your home or business for free! Wireless Emporium guarantees your satisfaction on all of our quality Samsung leather cases. As the leading name in wholesale <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Cell Phone Accessories"">cell phone accessories</a>, we know a thing or two about Samsung " & modelName & " cases.</p>"
			topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">See for yourself why countless customers trust us to offer the best Samsung " & modelName & " leather cases for Samsung " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day! All of the quality <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-cases.asp"" title=""Cell Phone Cases"">cell phone cases</a> we offer are guaranteed to have passed tough quality tests and our customer service is sure to please. So don't wait! Buy your Samsung " & modelName & " cell phone case from Wireless Emporium today!"
		case "13"
			topText = "WirelessEmporium is the largest store online for Samsung " & modelName & " Data Cable & Memory & other <a class=""topText-link"" href=""http://www.wirelessemporium.com/T-" & modelid & "-cell-accessories-samsung-" & formatSEO(modelName) & ".asp"" title=""Samsung " & modelName & " Accessories"">Samsung " & modelName & " Accessories</a> at the lowest prices.</p>"
			topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">The best in Samsung " & modelName & " Data Cable & Memory at prices up to 80% off retail. Couple this with our WirelessEmporium.com Happy-Shopper 30-day Guarantee and fast FREE SHIPPING, and you realize there's no better place to find your Samsung " & modelName & " Data Cable & Memory!"
		case "17"
			topText = "WirelessEmporium is the largest store online for Samsung " & modelName & " Body Guards/Skins & other Samsung " & modelName & " Cell Phone Accessories at the lowest prices.</p>"
			topText = topText & "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">The best in Samsung " & modelName & " Body Guards/Skins at prices up to 80% off retail. Couple this with our WirelessEmporium.com Happy-Shopper 30-day Guarantee and fast FREE SHIPPING, and you realize there's no better place to find your Samsung " & modelName & " Body Guards/Skins!"
	end select
else
	if categoryid = "1" then
		topText = "Wireless Emporium has everything you need for your " & brandName & " " & modelName & " phone, including a wide selection of <a class=""topText-link"" href=""/sc-1-sb-" & brandID & "-" & brandName & "-batteries.asp"" title=""" & brandName & " Cell Phone Batteries"">batteries for " & brandName & " phones</a>. If you need a spare battery because your current " & brandName & " " & modelName & " phone is burning through battery power too fast, or you need to replace on old battery that isn�t holding it�s charge anymore, we have a " & brandName & " " & modelName & " <a class=""topText-link"" href=""cell-phone-batteries.asp"" title=""Cell Phone Batteries"">battery</a> that will fit your needs guaranteed."
		topText = topText & "Every " & brandName & " " & modelName & " cell phone battery we sell is quality-tested and guaranteed for flawless performance."
		bottomText = "Don't pay outrageous retail prices for the same " & brandName & " " & modelName & " cell phone batteries you can find here at Wireless Emporium. We provide discount " & brandName & " " & modelName & " battery priced at up to 65% off retail! All cell phone batteries are guaranteed to perform or your money back. Since 2001, Wireless Emporium has been offering <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Discount " & brandName & " " & modelName & " Accessories"">discount cell phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best cell phone batteries for " & brandName & " " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
	elseif categoryid = "2" then
		topText = "Wireless Emporium is the online leader in <a class=""topText-link"" href=""/sc-2-sb-" & brandID & "-" & brandName & "-chargers.asp"" title=""" & brandName & " Cell Phone Batteries"">" & brandName & " cell phone chargers</a>. Because of that, we guarantee to have the widest selection of car chargers and travel chargers for your " & brandName & " " & modelName & " phone. If you are constantly on the go and need to keep your " & brandName & " " & modelName & " powered up or you just need a spare <a class=""topText-link"" href=""/cell-phone-chargers.asp"" title=""Cell Phone Chargers"">cell phone charger</a> for your home or office, Wireless Emporium is the place to be for the essential accessories you need."
		bottomText = "We provide thousands of businesses and consumers discount <a class=""topText-link"" href=""" & modelLink & """ title=""" & brandName & " " & modelName & " Chargers"">" & brandName & " " & modelName & " accessory</a> solutions that give your phone flawless performance time after time at prices up to 50% OFF retail we even ship for FREE! We can help you find the " & brandName & " " & modelName & " charger you need to extend the talk and standby times for your " & brandName & " " & modelName & " mobile phone. All Wireless Emporium " & brandName & " " & modelName & " chargers are guaranteed to perform or your money back. "
		bottomText = bottomText & "Look through our selection of " & brandName & " " & modelName & " chargers today. See for yourself why countless customers trust us to offer the best " & brandName & " " & modelName & " cell phone chargers for " & brandName & " " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
	elseif categoryid = "3" then
		topText = "Go bold or go sleek with your " & brandName & " " & modelName & " by choosing from hundreds of styles of <a class=""topText-link"" href=""http://www.wirelessemporium.com/sc-3-sb-" & brandID & "-" & formatSEO(RS("brandName")) & "-" & formatSEO(categoryName) & ".asp"" title=""" & brandName & " " & modelName & " Faceplates"">" & brandName & " cell phone faceplates</a> and screen protectors at Wireless Emporium. These " & brandName & " " & modelName & " faceplates are easy to assemble and our " & brandName & " " & modelName & " screen protectors are as simple as a sticker to install."
		topText = topText & "Because we offer our cell phone faceplates and screen protectors at discount prices, you can buy multiple " & brandName & " " & modelName & " faceplates and change them as often as your mood! Choose the color or design that best suits your personality from our wide selection of " & brandName & " " & modelName & " <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-covers-faceplates-screen-protectors.asp"" title=""Cell Phone Covers"">cell phone covers</a> and cell phone faceplates below and protect your phone with style! " & brandName & " " & modelName & " faceplates and covers are an essential addition to your mobile device, as it is one of the most effective ways to both protect the look of your phone and enhance it, all while retaining total functionality."
		bottomText = "<p class=""topText"" style=""margin-top:0px;margin-bottom:6px;"">Since 2001, Wireless Emporium has been offering high-quality cell <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Discount Phone Accessories"">discount phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best cell phone faceplates and covers for " & brandName & " " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
	elseif categoryid = "6" then
		topText = "Choose from a wide array of our quality " & brandName & " " & modelName & " cell phone holsters and belt clips to safely and easily secure your " & brandName & " " & modelName & " phone. We offer " & brandName & " " & modelName & " belt holsters at prices you won't find anywhere else! Protect your " & brandName & " " & modelName & " cell phone with our low priced " & brandName & " " & modelName & " holsters! They clip easily to belts, purses, pockets, and protect your phone through your active lifestyle. An adjustable belt clip that rotates and locks in six positions makes it easy for you to adjust your phone the way you need it. Our " & brandName & " " & modelName & " <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-holsters-belt-clips-holders.asp"" title=""" & brandName & " " & modelName & " Cell Phone Belt Clips"">cell phone belt clips</a> are stylish, too! "
		topText = topText & "Thousands of customers trust us to offer high-quality holsters at the lowest possible prices, and we're dedicated to satisfying the needs of all our customers! Browse through our " & brandName & " " & modelName & " cell phone belt clip selection today."
		bottomText = "Since 2001, Wireless Emporium has been offering <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Wholesale " & brandName & " " & modelName & " Cell Phone Accessories"">wholesale cell phone accessories</a> to first-time shoppers and loyal customers. See for yourself why countless customers trust us to offer the best <a class=""topText-link"" href=""" & modelLink & """ title=""" & brandName & " " & modelName & " Accessories"">" & brandName & " " & modelName & " accessories</a> like holsters & belt clips for your " & brandName & " " & modelName & " phone at the lowest prices. Shipping and superior customer service is free with every order and offered every day!"
	elseif categoryid = "7" then
		topText = "Choose from a wide array of our quality " & brandName & " " & modelName & " leather cases to safely and easily secure your " & brandName & " " & modelName & " phone. We offer prices you won't find anywhere else! Protect your " & brandName & " " & modelName & " cell with our low priced " & brandName & " " & modelName & " leather cases! They clip easily to belts, purses, pockets, and protect your phone through your active lifestyle. Our " & brandName & " " & modelName & " leather cases are stylish, too! Browse through our <a class=""topText-link"" href=""" & modelLink & """ title=""" & brandName & " " & modelName & " Accessories"">" & brandName & " " & modelName & " Accessories</a> cases selection today. "
		topText = topText & "Buy a quality " & brandName & " " & modelName & " cell phone leather cases today and we'll ship it to your home or business for free! Wireless Emporium guarantees your satisfaction on all of our quality <a class=""topText-link"" href=""http://www.wirelessemporium.com/sc-3-sb-" & brandID & "-" & formatSEO(RS("brandName")) & "-" & formatSEO(categoryName) & ".asp"" title=""" & brandName & " " & modelName & " Leather Case"">" & brandName & " leather cases</a>. As the leading name in <a class=""topText-link"" href=""http://www.wirelessemporium.com/"" title=""Wholesale " & brandName & " " & modelName & " Cell Phone Accessories"">wholesale cell phone accessories</a>, we know a thing or two about " & brandName & " " & modelName & " cases."
		bottomText = "See for yourself why countless customers trust us to offer the best " & brandName & " " & modelName & " cases for " & brandName & " " & modelName & " phones at the lowest prices. Shipping and superior customer service is free with every order and offered every day! All of the <a class=""topText-link"" href=""http://www.wirelessemporium.com/cell-phone-cases.asp"" title=""Quality " & brandName & " " & modelName & " Cases"">quality cell phone cases</a> we offer are guaranteed to have passed tough quality tests and our customer service is sure to please. So don't wait! Buy your " & brandName & " " & modelName & " cell phone case from Wireless Emporium today!"
	end if
end if

if SEtitle = "" or SEdescription = "" or SEkeywords = "" then
	SQL = "SELECT title, description, keywords FROM we_MetaTags WHERE brandID = '" & brandID & "' AND modelid = '" & modelid & "' AND typeID = '" & categoryid & "'"
	dim RSmeta
	set RSmeta = Server.CreateObject("ADODB.Recordset")
	RSmeta.open SQL, oConn, 0, 1
	if not RSmeta.eof then
		if SEtitle = "" then SEtitle = RSmeta("title")
		if SEdescription = "" then SEdescription = RSmeta("description")
		if SEkeywords = "" then SEkeywords = RSmeta("keywords")
	else
		if SEtitle = "" then SEtitle = nameSEO(categoryName) & " for " & brandName & " " & modelName & " ?" & nameSEO(categoryName) & " for " & brandName & " " & modelName & " Cell Phones ?Accessories for " & brandName & " " & modelName
		if SEdescription = "" then SEdescription = nameSEO(categoryName) & " for " & brandName & " " & modelName & " ?WirelessEmporium is the largest store online for " & brandName & " " & modelName & " Cell Phone " & nameSEO(categoryName) & " & other Cell Phone Accessories for " & brandName & " " & modelName & " at the lowest prices."
		if SEkeywords = "" then SEkeywords = "Accessories for " & brandName & " " & modelName & ", Cell Phone Accessories for " & brandName & " " & modelName & ", " & brandName & " " & modelName & " " & nameSEO(categoryName) & ", " & brandName & " " & modelName & " Cell Phone " & nameSEO(categoryName) & ", " & nameSEO(categoryName) & " for " & brandName & " " & modelName & ", " & nameSEO(categoryName) & " for " & brandName & " " & modelName & " Cell Phones"
	end if
end if
if topText = "" then topText = "WirelessEmporium is the largest store online for " & brandName & " " & modelName & " Cell Phone " & nameSEO(categoryName) & " & other Cell Phone Accessories for " & brandName & " " & modelName & " at the lowest prices. Couple this with our WirelessEmporium.com Happy-Shopper 30-day Guarantee and fast FREE SHIPPING, and you realize there's no better place to find your " & brandName & " " & modelName & " " & nameSEO(categoryName) & "!"

'session("breadcrumb_model") = ""
'session("breadcrumb_brand") = ""

dim strBreadcrumb
strBreadcrumb = "Cell Phone " & nameSEO(categoryName) & " for " & brandName & " " & modelName
%>
<!--#include virtual="/includes/template/top.asp"-->
<table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal" style="padding-top:7px;">
            <%
            if (categoryid = "8" or categoryid = "14") and modelid = "0" then
                %>
                <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;Cell Phone <%=nameSEO(categoryName)%>
                <%
            else
                %>
                <a class="breadcrumb" href="/index.asp">Cell Phone Accessories</a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="/<%=categorySEO(categoryID)%>">Cell Phone <%=nameSEO(categoryName)%></a>&nbsp;&gt;&nbsp;<a class="breadcrumb" href="/sc-<%=categoryID & "-sb-" & brandID & "-" & formatSEO(brandName) & "-" & formatSEO(categoryName)%>.asp"><%=brandName & " Cell Phone " & nameSEO(categoryName)%></a>&nbsp;&gt;&nbsp;<%=strBreadcrumb%>
                <%
            end if
            %>
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" valign="top">
            <table width="748" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="150" height="100" align="center" valign="middle">
                        <%
                        if categoryid = "8" and modelid = "0" then
                            %>
                            <img src="/images/cats/cat-other-gear.jpg" width="80" border="0" alt="<%=brandName & " " & modelName & " " & nameSEO(categoryName)%>">
                            <%
                        elseif categoryid = "14" and modelid = "0" then
                            %>
                            <img src="/images/cats/cat-charms-bling-kits.jpg" width="80" border="0" alt="<%=brandName & " " & modelName & " Charms & Stickers"%>">
                            <%
                        else
                            %>
                            <img src="/productpics/models/<%=modelImg%>" width="80" border="0" alt="<%=brandName & " " & modelName & " " & nameSEO(categoryName)%>">
                            <%
                        end if
                        %>
                    </td>
                    <td width="5" align="center" valign="middle">
                        <img src="/images/spacer.gif" width="1" height="5" border="0">
                    </td>
                    <td width="645" align="left" valign="top">
                        <style>
                            h1 {
                                font-size: 18pt;
                                color: #000000;
                                font-weight: bold;
                                font-family: Arial, Helvetica, sans-serif;
                            }
                        </style>
                        <%
                        if categoryid = "14" then
                            response.write "<h1>Cell Phone Charms & Stickers</h1>"
                        elseif categoryid = "8" then
                            response.write "<h1>" & nameSEO(categoryName) & " for All Phone Models</h1>"
                        elseif h1 <> "" then
                            response.write "<h1>" & h1 & "</h1>"
                        else
                            response.write "<h1>" & brandName & " " & modelName & " " & nameSEO(categoryName) & "</h1>"
                        end if
                        %>
                        <p><%=topText%></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <%if categoryid = "3" or categoryid = "7" then%>
    <tr>
        <td width="100%" align="left" valign="top">
            <table width="748" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <%
                    dim DesignName(10), DesignCount(10), dCount, a
                    DesignName(1) = "Body Glove"
                    DesignName(2) = "Case-Mate"
                    DesignName(3) = "Dragonfly"
                    DesignName(4) = "Ed Hardy"
                    DesignName(5) = "Incipio"
                    DesignName(6) = "Otterbox"
                    DesignName(7) = "Seidio"
                    DesignName(8) = "Solid Cases"
                    DesignName(9) = "Design Cases"
                    DesignName(10) = "Silicone & Candy Cases"
                    dCount = 0
                    a = 0
                    do until RS.eof
                        for dCount = 1 to 10
                            DesignCount(RS("DesignType")) = DesignCount(RS("DesignType")) + 1
                        next
                        RS.movenext
                    loop
                    RS.movefirst
                    for dCount = 1 to 10
                        if DesignCount(dCount) > 0 then
                            if a = 5 then
                                response.write "</tr><tr><td colspan=""5""><img src=""/images/spacer.gif"" width=""1"" height=""10"" border=""0""></td></tr><tr>" & vbcrlf
                                a = 0
                            end if
                            response.write "<td width=""160"" align=""center"" valign=""top""><a href=""" & REWRITE_URL & "?design=" & dCount & """><img src=""/images/DesignTypes/fp_brands_" & dCount & ".jpg"" width=""145"" height=""92"" border=""0"" alt=""" & DesignName(dCount) & """></a></td>" & vbcrlf
                            a = a + 1
                        end if
                    next
                    if a = 5 then
                        response.write "</tr><tr><td colspan=""5""><img src=""/images/spacer.gif"" width=""1"" height=""10"" border=""0""></td></tr><tr>" & vbcrlf
                        a = 0
                    end if
                    response.write "<td width=""160"" align=""center"" valign=""top""><a href=""" & REWRITE_URL & """><img src=""/images/DesignTypes/fp_brands_all.jpg"" width=""145"" height=""92"" border=""0"" alt=""Show All""></a></td>" & vbcrlf
                    a = a + 1
                    if a = 1 then
                        response.write "<td width=""160"">&nbsp;</td><td width=""160"">&nbsp;</td><td width=""160"">&nbsp;</td><td width=""160"">&nbsp;</td>" & vbcrlf
                    elseif a = 2 then
                        response.write "<td width=""160"">&nbsp;</td><td width=""160"">&nbsp;</td><td width=""160"">&nbsp;</td>" & vbcrlf
                    elseif a = 3 then
                        response.write "<td width=""160"">&nbsp;</td><td width=""160"">&nbsp;</td>" & vbcrlf
                    elseif a = 4 then
                        response.write "<td width=""160"">&nbsp;</td>" & vbcrlf
                    end if
                    %>
                </tr>
            </table>
        </td>
    </tr>
    <%end if%>
    <tr>
        <td width="100%" align="left" valign="top">
            <table width="746" border="0" align="center" cellspacing="0" cellpadding="0">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="100%" align="center" valign="top">
                        <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" valign="top" colspan="7"><img src="/images/line-hori.jpg" width="746" height="5" border="0"></td>
                            </tr>
                            <tr>
                                <%
                                dim altText, aCount, startCount, endCount, RSextra, itemDescEtc
                                a = 0
                                
                                if categoryid = 3 or categoryid = 7 then
                                    SQL = "SELECT MIN(price_Retail) AS minPrice_Retail, MIN(price_Our) AS minPrice_Our FROM we_items WHERE hidelive = 0 AND inv_qty > 0 AND typeid = 17 AND modelID = '" & modelID & "'"
                                    set RSextra = Server.CreateObject("ADODB.Recordset")
                                    RSextra.open SQL, oConn, 3, 3
                                    if not RSextra.eof then
                                        if RSextra("minPrice_Retail") > 0 and RSextra("minPrice_Our") > 0 then
                                            response.write "<td align=""center"" valign=""top"" width=""172"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""172"" height=""100%"">" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""middle"" width=""172"" height=""154""><a href=""/sc-17-sb-" & brandID & "-sm-" & modelID & "-full-body-protectors-for-" & formatSEO(brandname) & "-" & formatSEO(modelName) & ".asp""><img src=""/images/GadgetGuards/we_decal.jpg"" border=""0"" alt=""Gadget Guards & Decal Skins""></a></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""top"" height=""50""><a class=""cellphone-link"" href=""/sc-17-sb-" & brandID & "-sm-" & modelID & "-full-body-protectors-for-" & formatSEO(brandname) & "-" & formatSEO(modelName) & ".asp"" title=""Gadget Guards & Decal Skins"">GADGET GUARDS & DECAL SKINS</a><br></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""bottom"" height=""40""><span class=""boldText"">Our&nbsp;Price:&nbsp;</span><span class=""pricing-orange2"">" & formatCurrency(RSextra("minPrice_Our")) & "&nbsp;</span><br><span class=""pricing-gray2"">Starting&nbsp;At:&nbsp;<s>" & formatCurrency(RSextra("minPrice_Retail")) & "</s></span><br><a href=""/sc-17-sb-" & brandID & "-sm-" & modelID & "-full-body-protectors-for-" & formatSEO(brandname) & "-" & formatSEO(modelName) & ".asp"" class=""product-description3""><img src=""/images/link_arrow.gif"" border=""0"" align=""abs-middle"">&nbsp;&nbsp;<u><i>Click Here For Our Complete Lineup</i></u></a></td></tr>" & vbcrlf
                                            response.write "</table></td>" & vbcrlf
                                            response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                                            a = 1
                                        end if
                                    end if
                                end if
                                
                                if categoryid = 7 and (design = "" or not isNumeric(design)) then
                                    dim boxHeader(6), imgSrc(6)
                                    if excludePouches = false then
                                        boxHeader(1) = "MLB OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                                        boxHeader(2) = "NCAA OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                                        boxHeader(3) = "DISNEY OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                                        boxHeader(4) = "BLING UNIVERSAL CELL PHONE POUCHES"
                                        imgSrc(1) = "LC-MLB-thumb.jpg"
                                        imgSrc(2) = "LC-NCAA-Thumb.jpg"
                                        imgSrc(3) = "LC-Disney-Thumb.jpg"
                                        imgSrc(4) = "LC-Blingpouch-Thumb.jpg"
                                        startCount = 1
                                        endCount = 4
                                    elseif includeNFL = true then
                                        boxHeader(5) = "NFL OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                                        imgSrc(5) = "LC-NFL-Thumb.jpg"
                                        startCount = 5
                                        endCount = 6
                                        boxHeader(6) = "NBA OFFICIALLY LICENSED UNIVERSAL CELL PHONE POUCH"
                                        imgSrc(6) = "LC-NBA-Thumb.jpg"
                                    end if
                                    if startCount > 0 and endCount > 0 then
                                        for aCount = startCount to endCount
                                            SQL = "SELECT TOP 1 price_Retail, price_Our FROM we_items WHERE hidelive=0 AND inv_qty > 0 AND Sports = '" & aCount & "'"
                                            set RSextra = Server.CreateObject("ADODB.Recordset")
                                            RSextra.open SQL, oConn, 3, 3
                                            if not RSextra.eof then
                                                altText = boxHeader(aCount)
                                                response.write "<td align=""center"" valign=""top"" width=""172"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""172"" height=""100%"">" & vbcrlf
                                                response.write "<tr><td align=""center"" valign=""middle"" width=""172"" height=""154""><a href=""/pouches-" & aCount & "-" & formatSEO(boxHeader(aCount)) & ".asp""><img src=""/productpics/thumb/" & imgSrc(aCount) & """ border=""0"" alt=""" & altText & """></a></td></tr>" & vbcrlf
                                                response.write "<tr><td align=""center"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
                                                response.write "<tr><td align=""center"" valign=""top"" height=""50""><a class=""cellphone-link"" href=""/pouches-" & aCount & "-" & formatSEO(boxHeader(aCount)) & ".asp"" title=""" & altText & """>" & boxHeader(aCount) & "</a></td></tr>" & vbcrlf
                                                response.write "<tr><td align=""center"" valign=""bottom"" height=""40""><span class=""boldText"">Our&nbsp;Price:&nbsp;</span><span class=""pricing-orange2"">" & formatCurrency(RSextra("price_Our")) & "&nbsp;</span><br><span class=""pricing-gray2"">List&nbsp;Price:&nbsp;<s>" & formatCurrency(RSextra("price_Retail")) & "</s></span><br><a href=""/pouches-" & aCount & "-" & formatSEO(boxHeader(aCount)) & ".asp"" class=""product-description3""><img src=""/images/link_arrow.gif"" border=""0"" align=""abs-middle"">&nbsp;&nbsp;<u><i>Click Here For Our Complete Lineup</i></u></a></td></tr>" & vbcrlf
                                                response.write "</table></td>" & vbcrlf
                                                a = a + 1
                                                if a = 4 then
                                                    response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""746"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
                                                    a = 0
                                                else
                                                    response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                                                end if
                                            end if
                                            RSextra.close
                                            set RSextra = nothing
                                        next
                                    end if
                                elseif categoryid = 2 then
                                    if not isNull(includeExtraItem) and includeExtraItem > 0 then
                                        SQL = "SELECT itemID, itemDesc, itemPic, price_Retail, price_Our FROM we_items WHERE itemid = '" & includeExtraItem & "'"
                                        set RSextra = Server.CreateObject("ADODB.Recordset")
                                        RSextra.open SQL, oConn, 3, 3
                                        do until RSextra.eof
                                            altText = singularSEO(categoryName) & " for " & brandName & " " & modelName & " ?" & RSextra("itemDesc")
                                            if len(RSextra("itemDesc")) >= 85 then
                                                itemDescEtc = left(RSextra("itemDesc"),80) & "..."
                                            else
                                                itemDescEtc = RSextra("itemDesc")
                                            end if
                                            response.write "<td align=""center"" valign=""top"" width=""172"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""172"" height=""100%"">" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""middle"" width=""172"" height=""154""><a href=""/p-" & RSextra("itemid") & "-" & formatSEO(RSextra("itemDesc")) & ".asp""><img src=""/productpics/thumb/" & RSextra("itemPic") & """ border=""0"" alt=""" & altText & """></a></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""top"" height=""50""><a class=""cellphone-link"" href=""/p-" & RSextra("itemid") & "-" & formatSEO(RSextra("itemDesc")) & ".asp"" title=""" & altText & """>" & itemDescEtc & "</a></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""bottom"" height=""40""><span class=""boldText"">Our&nbsp;Price:&nbsp;</span><span class=""pricing-orange2"">" & formatCurrency(RSextra("price_Our")) & "</span><br><span class=""pricing-gray2"">List&nbsp;Price:&nbsp;<s>" & formatCurrency(RSextra("price_Retail")) & "</s></span></td></tr>" & vbcrlf
                                            response.write "</table></td>" & vbcrlf
                                            a = a + 1
                                            if a = 4 then
                                                response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""746"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
                                                a = 0
                                            else
                                                response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                                            end if
                                            RSextra.movenext
                                        loop
                                        RSextra.close
                                        set RSextra = nothing
                                    end if
                                elseif categoryid = 12 then
                                    SQL = "SELECT itemID, itemDesc, itemPic, price_Retail, price_Our FROM we_items WHERE vendor = 'CM'"
                                    set RSextra = Server.CreateObject("ADODB.Recordset")
                                    RSextra.open SQL, oConn, 3, 3
                                    do until RSextra.eof
                                        altText = singularSEO(categoryName) & " for " & brandName & " " & modelName & " ?" & RSextra("itemDesc")
                                        if len(RSextra("itemDesc")) >= 85 then
                                            itemDescEtc = left(RSextra("itemDesc"),80) & "..."
                                        else
                                            itemDescEtc = RSextra("itemDesc")
                                        end if
                                        response.write "<td align=""center"" valign=""top"" width=""172"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""172"" height=""100%"">" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""middle"" width=""172"" height=""154""><a href=""/p-" & RSextra("itemid") & "-" & formatSEO(RSextra("itemDesc")) & ".asp""><img src=""/productpics/thumb/" & RSextra("itemPic") & """ border=""0"" alt=""" & altText & """></a></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""top"" height=""50""><a class=""cellphone-link"" href=""/p-" & RSextra("itemid") & "-" & formatSEO(RSextra("itemDesc")) & ".asp"" title=""" & altText & """>" & itemDescEtc & "</a></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""bottom"" height=""40""><span class=""boldText"">Our&nbsp;Price:&nbsp;</span><span class=""pricing-orange2"">" & formatCurrency(RSextra("price_Our")) & "</span><br><span class=""pricing-gray2"">List&nbsp;Price:&nbsp;<s>" & formatCurrency(RSextra("price_Retail")) & "</s></span></td></tr>" & vbcrlf
                                        response.write "</table></td>" & vbcrlf
                                        a = a + 1
                                        if a = 4 then
                                            response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""746"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
                                            a = 0
                                        else
                                            response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                                        end if
                                        RSextra.movenext
                                    loop
                                    RSextra.close
                                    set RSextra = nothing
                                end if
                                
                                dim DoNotDisplay, RSkit
                                do until RS.eof
                                    DoNotDisplay = 0
                                    if not isNull(RS("ItemKit_NEW")) then
                                        SQL = "SELECT inv_qty FROM we_Items WHERE itemID IN (" & RS("ItemKit_NEW") & ")"
                                        set RSkit = Server.CreateObject("ADODB.recordset")
                                        RSkit.open SQL, oConn, 3, 3
                                        do until RSkit.eof
                                            if RSkit("inv_qty") < 1 then DoNotDisplay = 1
                                            RSkit.movenext
                                        loop
                                        RSkit.close
                                        set RSkit = nothing
                                    end if
                                    
                                    if design <> "" and isNumeric(design) and design > 0 and design <= 10 then
                                        if cStr(RS("DesignType")) <> design then DoNotDisplay = 1
                                    end if
                                    
                                    if DoNotDisplay = 0 then
                                        altText = singularSEO(categoryName) & " for " & brandName & " " & modelName & " ?" & RS("itemDesc")
                                        if categoryid = "14" and modelid = "0" then altText = "Cell Phone Charms & Bling : " & altText
                                        if len(RS("itemDesc")) >= 85 then
                                            itemDescEtc = left(RS("itemDesc"),80) & "..."
                                        else
                                            itemDescEtc = RS("itemDesc")
                                        end if
                                        response.write "<td align=""center"" valign=""top"" width=""172"" height=""100%""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""172"" height=""100%"">" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""middle"" width=""172"" height=""154""><a href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp""><img src=""/productpics/thumb/" & RS("itemPic") & """ border=""0"" alt=""" & altText & """></a></td></tr>" & vbcrlf
                                        response.write "<tr><td align=""center"" valign=""top"" height=""5""><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf

                                        if 0 = RS("inv_qty") then
                                            response.write "<tr><td align=""center"" valign=""top"" height=""90"" rowspan=""2""><img src=""/images/sold_out.png"" width=""85""></td></tr>" & vbcrlf
                                        else
                                            response.write "<tr><td align=""center"" valign=""top"" height=""50""><a class=""cellphone-link"" href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc")) & ".asp"" title=""" & altText & """>" & itemDescEtc & "</a></td></tr>" & vbcrlf
                                            response.write "<tr><td align=""center"" valign=""bottom"" height=""40""><span class=""boldText"">Our&nbsp;Price:&nbsp;</span><span class=""pricing-orange2"">" & formatCurrency(RS("price_Our")) & "</span><br><span class=""pricing-gray2"">List&nbsp;Price:&nbsp;<s>" & formatCurrency(RS("price_Retail")) & "</s></span></td></tr>" & vbcrlf
                                        end if 
                                    
                                        response.write "</table></td>" & vbcrlf
                                        a = a + 1
                                        if a = 4 then
                                            response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""7""><img src=""/images/line-hori.jpg"" width=""746"" height=""5"" border=""0""></td></tr><tr>" & vbcrlf
                                            a = 0
                                        else
                                            response.write "<td width=""10"" align=""right"" valign=""middle""><img src=""/images/line-vert.jpg"" width=""5"" height=""242"" border=""0""></td>" & vbcrlf
                                        end if
                                    end if
                                    RS.movenext
                                loop
                                if a = 1 then
                                    response.write "<td width=""172"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""172"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""172"">&nbsp;</td>" & vbcrlf
                                elseif a = 2 then
                                    response.write "<td width=""172"">&nbsp;</td><td width=""10"" align=""right"" valign=""middle""><img src=""/images/spacer.gif"" width=""10"" height=""242"" border=""0""></td><td width=""172"">&nbsp;</td>" & vbcrlf
                                elseif a = 3 then
                                    response.write "<td width=""172"">&nbsp;</td>" & vbcrlf
                                end if
                                %>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
    <tr>
        <td align="center" class="bottomText"><%=bottomText%>
        </td>
    </tr>
</table>
<% call fCloseConn() %>
<!--#include virtual="/includes/template/bottom.asp"-->