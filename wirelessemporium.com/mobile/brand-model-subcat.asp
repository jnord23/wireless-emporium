<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "brandModel"
%>
<!--#include virtual="/template/top.asp"-->
<%	
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim categoryID : categoryID = prepInt(request.QueryString("categoryID"))
	
	sql =	"select a.subtypeid, a.subTypeNameSEO_WE, b.qty, b.itemPic " &_
			"from v_subtypeMatrix a " &_
				"outer apply (" &_
					"select SUM(inv_qty) as qty, (select top 1 itemPic from we_Items where modelID = 1325 and subtypeID = a.subtypeid and hidelive = 0 and inv_qty > 0) as itemPic " &_
					"from we_Items " &_
					"where modelID = 1325 and subtypeID = a.subtypeid and hidelive = 0" &_
				") b " &_
			"where a.typeID = 3 and b.qty > 0"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
%>
<div id="curModelDetails">
	<div id="modelPic"><img src="/productPics/models/thumbs/<%=modelImg%>" /></div>
    <div id="modelText">
    	<div id="currentlyViewing">CURRENTLY VIEWING CATEGORIES FOR:</div>
        <div id="modelName"><%=modelName%></div>
        <a href="/?reset=1" style="text-decoration:none;">
        <div id="changeDeviceBox">
        	<div id="redX"></div>
            <div id="tapToChange">Tap to change device.</div>
        </div>
        </a>
    </div>
</div>
<div id="mainProductBox">
	<%
	prodCnt = 0
	if rs.EOF then
%>
	<div id="noProds">No Products Found<br />For This Model</div>
<%
	end if
	
	do while not rs.EOF
		catID = rs("subtypeid")
		catName = rs("subTypeNameSEO_WE")
		itemPic = rs("itemPic")
	%>
    <a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-<%=catID%>-<%=formatSEO(catName)%>-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>.asp" style="text-decoration:none;" title="<%=itemDesc%>">
	<div id="singleProdBox">
    	<div id="prodPic"><img src="/productpics/thumb/<%=itemPic%>" border="0" /></div>
        <div id="prodDesc"><%=catName%></div>
    </div>
    </a>
    <%
		rs.movenext
	loop
	%>
</div>
<!--#include virtual="/template/bottom.asp"-->