<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "brandModel"
	basePageName = "Brand-Model"
	
	if instr(request.QueryString("brandModel"),"--") > 0 then
		brandModel = prepStr(replace(request.QueryString("brandModel"),"--","-"))
	else
		brandModel = prepStr(request.QueryString("brandModel"))
	end if
	
	if brandModel <> "" then
		curURL = request.ServerVariables("HTTP_X_REWRITE_URL")
		curURL = replace(replace(replace(replace(curURL,"/accessories-for-",""),"-"," "),".asp",""), "'", "''")
		sql = "select modelID from we_models where linkName = '" & curURL & "'"
		session("errorSQL") = sql
		set linkNameRS = oConn.execute(sql)
		
		if linkNameRS.EOF then
			useBrandModel = replace(brandModel,"-"," ")
			sql = "select a.modelID from we_models a left join we_Brands b on a.brandID = b.brandID where replace(replace(b.brandName,'-',' '),'/',' ') + ' ' + replace(replace(a.modelName,'-',' '),'/',' ') like '" & useBrandModel & "%'"
			session("errorSQL") = sql
			set modelIdRS = oConn.execute(sql)
			
			if modelIdRS.EOF then
				do while modelIdRS.EOF
					session("errorSQL") = "useBrandModel:" & useBrandModel
					removedWord = replace(mid(useBrandModel,instrrev(useBrandModel," "))," ","")
					useBrandModel = left(useBrandModel,instrrev(useBrandModel," ")-1)
					if removedWord <> "" then
						sql = "select a.modelID from we_models a left join we_Brands b on a.brandID = b.brandID where replace(replace(b.brandName,'-',' '),'/',' ') + ' ' + replace(replace(a.modelName,'-',' '),'/',' ') like '" & useBrandModel & "%' and replace(replace(b.brandName,'-',' '),'/',' ') + ' ' + replace(replace(a.modelName,'-',' '),'/',' ') like '%" & removedWord & "%'"
					else
						sql = "select a.modelID from we_models a left join we_Brands b on a.brandID = b.brandID where replace(replace(b.brandName,'-',' '),'/',' ') + ' ' + replace(replace(a.modelName,'-',' '),'/',' ') like '" & useBrandModel & "%'"
					end if
					session("errorSQL") = sql
					set modelIdRS = oConn.execute(sql)
					
					if instrrev(useBrandModel," ") < 1 then exit do
				loop
				
				if modelIdRS.EOF then
					if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
						response.Write("bounce to home1")
						response.End()
					else
						PermanentRedirect("/")
					end if
				else
					modelid = modelIdRS("modelID")
					sql = "update we_models set linkName = '" & curURL & "' where modelID = " & modelID
					session("errorSQL") = sql
					oConn.execute(sql)
				end if
			else
				modelid = modelIdRS("modelID")
				sql = "update we_models set linkName = '" & curURL & "' where modelID = " & modelID
				session("errorSQL") = sql
				oConn.execute(sql)
			end if
		else
			modelid = linkNameRS("modelID")
		end if
	end if
	
	if prepInt(modelID) = 0 then
		response.Write("missing modelID")
		response.End()
	end if
	
	sql	=	"select	distinct a.modelid, a.modelname, a.modelimg, a.temp, c.brandid, c.brandname" & vbcrlf & _
			"	,	a.isTablet, v.typeid, v.typeName, v.orderNum" & vbcrlf & _
			"	,	'sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}.htm' urlStructure	" & vbcrlf & _
			"from	we_models a join we_brands c " & vbcrlf & _
			"	on	a.brandid = c.brandid join we_items b " & vbcrlf & _
			"	on	a.modelid = b.modelid join we_types d " & vbcrlf & _
			"	on	b.typeid = d.typeid join v_subtypeMatrix v" & vbcrlf & _
			"	on	b.subtypeid = v.subtypeid" & vbcrlf & _
			"where	a.modelid = '" & modelid & "' " & vbcrlf & _
			"	and d.typeid not in (4,5,8,9,12,15,16,20,22,23,24,25) and b.hidelive = 0 and (b.inv_qty <> 0 or (select alwaysInStock from we_pnDetails where PartNumber = b.partNumber) = 1) and b.price_our > 0 " & vbcrlf & _
			"union " & vbcrlf & _
			"select	distinct a.modelid, a.modelname, a.modelimg, a.temp, c.brandid, c.brandname" & vbcrlf & _
			"	,	a.isTablet, v.typeID, v.typeName, v.orderNum	" & vbcrlf & _
			"	,	'sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}.htm' urlStructure	" & vbcrlf & _
			"from	we_models a join we_brands c" & vbcrlf & _
			"	on	a.brandid = c.brandid join v_typeMatrix v" & vbcrlf & _
			"	on	v.typeID in (5, 8)" & vbcrlf & _
			"where	a.modelid = '" & modelid & "'" & vbcrlf & _
			"union" & vbcrlf & _
			"select	distinct a.modelid, a.modelname, a.modelimg, a.temp, c.brandid, c.brandname" & vbcrlf & _
			"	,	a.isTablet, v.typeID, v.typeName, v.orderNum" & vbcrlf & _
			"	,	'sb-{brandid}-sm-{modelid}-sc-{typeid}-{typename}-{brandname}-{modelname}.htm' urlStructure	" & vbcrlf & _
			"from	we_models a join we_brands c" & vbcrlf & _
			"	on	a.brandid = c.brandid join we_subrelateditems s" & vbcrlf & _
			"	on	a.modelid = s.modelid join v_typeMatrix v" & vbcrlf & _
			"	on	s.subtypeid = v.origSubTypeId" & vbcrlf & _
			"where	s.modelid = '" & modelid & "'" & vbcrlf & _
			"order by v.orderNum desc" & vbcrlf
	
	'response.write "<pre>" & sql & "</pre>"
	session("errorSQL") = SQL
	set RS = oConn.execute(sql)
	
	if RS.EOF then
		PermanentRedirect("/")
	else
		modelImg = RS("modelImg")
		modelName = RS("modelName")
		brandID = rs("brandID")
		brandName = rs("brandName")
	end if
%>
<!--#include virtual="/template/top.asp"-->
<div id="main">
    <div id="curModelDetails">
        <div id="modelPic"><img src="/productPics/models/thumbs/<%=modelImg%>" /></div>
        <div id="modelText">
            <div id="currentlyViewing">CURRENTLY VIEWING CATEGORIES FOR:</div>
            <div id="modelName"><%=modelName%></div>
            <a href="/?reset=1" style="text-decoration:none;">
            <div id="changeDeviceBox">
                <div id="redX"></div>
                <div id="tapToChange">Tap to change device.</div>
            </div>
            </a>
        </div>
    </div>
    <div id="mainProductBox">
        <%
        prodCnt = 0
        if rs.EOF then
    %>
        <div id="noProds">No Products Found<br />For This Model</div>
    <%
        end if
        
        do while not rs.EOF
            catID = rs("typeID")
            catName = rs("typeName")
        %>
        <a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-<%=catID%>-<%=formatSEO(catName)%>-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>.htm" style="text-decoration:none;" title="<%=itemDesc%>">
        <div id="singleProdBox">
            <div id="prodPic"><img src="/images/mobile/categories/<%=catID%>.jpg" border="0" /></div>
            <div id="prodDesc"><%=catName%></div>
        </div>
        </a>
        <%
            rs.movenext
        loop
        %>
    </div>
    <div style="clear:both; padding:5px 0px 5px 0px; width:100%; background-color:#efefef;" align="center">
        <a href="javascript:location.href='/search/search.htm'+BR.mobile.getTrendingUrl();">
            <div id="br_btnTrending"></div>
        </a>
    </div>
</div>
<script>
window.WEDATA.pageType = 'brandModel';
window.WEDATA.pageData = {
	brand: <%= jsStr(brandName) %>,
	brandId: <%= jsStr(brandId) %>,
	model: <%= jsStr(modelName) %>,
	modelId: <%= jsStr(modelId) %>
}
</script>
<!--#include virtual="/template/bottom.asp"-->