<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "Home"
	
	dim reset : reset = prepInt(request.QueryString("reset"))
	if reset = 1 then
		response.Cookies("mobileBrandID") = 0
		response.Cookies("mobileModelID") = 0
	end if
%>
<!--#include virtual="/template/top.asp"-->
<%	
	sql = "select brandID, brandName from we_brands where phoneSale = 1 order by brandName"
	session("errorSQL") = sql
	set brandRS = oConn.execute(sql)
%>
<form id="basicForm" name="prodSelectForm" onsubmit="return(chkForm())" method="post" action="/bmc.htm">
<div id="homeSelectBox">
	<div id="selectBoxContainer">
		<select id="actualSelectBox" name="brandID" onchange="newBrandSelected()">
			<option value="">Choose A Device Brand</option>
			<%
			do while not brandRS.EOF
			%>
			<option value="<%=brandRS("brandID")%>"><%=brandRS("brandName")%></option>
			<%
				brandRS.movenext
			loop
			%>
		</select>
	</div>
</div>
<div id="homeSelectBox">
	<div id="selectBoxContainer2">
		<select id="actualSelectBox2" name="modelID" disabled="disabled">
			<option value="">Choose A Device Model</option>
		</select>
	</div>
</div>
<div id="homeSelectBox">
	<div id="selectBoxContainer3">
		<select id="actualSelectBox2" name="typeID" disabled="disabled">
			<option value="">Choose A Product Type</option>
		</select>
	</div>
</div>
</form>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">
	function newTypeSelected(typeID) {
		if (typeID != "") {
			document.prodSelectForm.submit()
		}
	}
	
	function newBrandSelected() {
		var brandID = document.prodSelectForm.brandID.options[document.prodSelectForm.brandID.selectedIndex].value
		if (brandID == "") {
			document.getElementById("selectBoxContainer2").innerHTML = "<select id='actualSelectBox2' name='modelID' disabled='disabled'><option value=''>Choose A Device Model</option></select>"
			document.getElementById("selectBoxContainer3").innerHTML = "<select id='actualSelectBox2' name='typeID' disabled='disabled'><option value=''>Choose A Product Type</option></select>"
		}
		else {
			ajax('/ajax/homepageSelect.htm?brandSelect=1&brandID=' + brandID,'selectBoxContainer2')
		}
		document.getElementById("selectBoxContainer3").innerHTML = "<select id='actualSelectBox2' name='typeID' disabled='disabled'><option value=''>Choose A Product Type</option></select>"
	}
	
	function newModelSelected() {
		var modelID = document.prodSelectForm.modelID.options[document.prodSelectForm.modelID.selectedIndex].value
		if (modelID == "") {
			document.getElementById("selectBoxContainer3").innerHTML = "<select id='actualSelectBox2' name='typeID' disabled='disabled'><option value=''>Choose A Product Type</option></select>"
		}
		else {
			ajax('/ajax/homepageSelect.htm?modelSelect=1&modelID=' + modelID,'selectBoxContainer3')
			scroll(100,0)
		}
	}
	
	function chkForm() {
		var brandID = document.prodSelectForm.brandID.options[document.prodSelectForm.brandID.selectedIndex].value
		var modelID = document.prodSelectForm.modelID.options[document.prodSelectForm.modelID.selectedIndex].value
		var typeID = document.prodSelectForm.typeID.options[document.prodSelectForm.typeID.selectedIndex].value
		
		if (brandID == "") {
			alert("You must select a brand, model and product type")
			return false
		}
		else if (modelID == "") {
			alert("You must select a model and product type")
			return false
		}
		else if (typeID == "") {
			alert("You must select a product type")
			return false
		}
		else {
			return true
		}
	}
	
	<% if prepInt(request.Cookies("mobileBrandID")) > 0 then %>
	for (i=0;i<document.prodSelectForm.brandID.length;i++) {
		if (document.prodSelectForm.brandID.options[i].value == <%=prepInt(request.Cookies("mobileBrandID"))%>) {
			document.prodSelectForm.brandID.selectedIndex = i
			newBrandSelected()
			setTimeout("setModleByCookie()",500)
		}
	}
	
	function setModleByCookie() {
		for (i=0;i<document.prodSelectForm.modelID.length;i++) {
			if (document.prodSelectForm.modelID.options[i].value == <%=prepInt(request.Cookies("mobileModelID"))%>) {
				document.prodSelectForm.modelID.selectedIndex = i
				newModelSelected()
			}
		}
	}
	<% end if %>
	if(typeof(Storage)!=="undefined") {
		localStorage.brandID = 17
		alert("we have local storage:" + localStorage.brandID)
	}
</script>