<%
	response.buffer = true
	response.expires = now
	response.ExpiresAbsolute = now
	response.CacheControl = "private"
	
	saveEmail()

	dim UserIPAddress
		UserIPAddress = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
	if UserIPAddress = "" then
	  UserIPAddress = Request.ServerVariables("REMOTE_ADDR")
	end if
	dim isInternalUser : isInternalUser = false

	if UserIpAddress = "66.159.49.66" then
		isInternalUser = true
	end if	

	dim currentPageURL : currentPageURL = request.ServerVariables("HTTP_X_REWRITE_URL")
	if instr(currentPageURL,".asp") > 0 then response.Redirect(replace(currentPageURL,".asp",""))
	if instr(currentPageURL,".htm") < 1 then
		if request.ServerVariables("URL") <> "/index.asp" and request.ServerVariables("URL") <> "/" then
			if instr(currentPageURL,"?") > 0 then
				currentPageURL_array = split(currentPageURL,"?")
				response.Redirect(currentPageURL_array(0) & ".htm?" & currentPageURL_array(1))
			else
				response.Redirect(currentPageURL & ".htm")
			end if
		end if
	elseif instr(currentPageURL,"/.htm") > 0 then
		response.Redirect(replace(currentPageURL,".htm",""))
	elseif instr(currentPageURL,"?") > 0 then
		if instr(currentPageURL,".htm") > instr(currentPageURL,"?") then
			currentPageURL = replace(currentPageURL,".htm","")
			currentPageURL_array = split(currentPageURL,"?")
			response.Redirect(currentPageURL_array(0) & ".htm?" & currentPageURL_array(1))
		end if
	end if
	
	mobilePhoneNumber = "1-888-506-2797"
	mobilePhoneText = "Order Now"
	dim fullSiteLink : fullSiteLink = "http://www.wirelessemporium.com" & replace(request.ServerVariables("HTTP_X_REWRITE_URL"),".htm","")
	showSearch = 1
	valPropBanner = 0
	gpsLoc = 0
	showTabs = 1
	if pageName = "Basket2" then
		mobilePhoneText = "Need Help? Call"
		showSearch = 0
		valPropBanner = 1
		gpsLoc = 1
		showTabs = 0
	end if
	
'	if prepInt(securePage) = 1 then
'		if request.ServerVariables("HTTPS") = "off" then
'			response.redirect("https://" & request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL"))
'		end if
'	else
'		if request.ServerVariables("HTTPS") = "on" then
'			response.redirect("http://" & request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL"))
'		end if
'	end if
	
	mySession = prepInt(request.cookies("mySession"))
		
	sql = 	"select isnull(sum(a.qty), 0) totalQty " & vbcrlf & _
			"from shoppingcart a " & vbcrlf & _
			"where a.store = 0 and a.sessionid = '" & mySession & "' and (a.purchasedorderid is null or a.purchasedorderid = 0)"
	session("errorSQL") = sql
	set objRsMiniCart = getRS(sql)
	
	if objRsMiniCart.EOF or mySession = 0 then
		miniTotalQuantity = 0 & " Items"
	else
		miniTotalQuantity = prepInt(objRsMiniCart("totalQty"))
		if miniTotalQuantity = 1 then
			miniTotalQuantity = miniTotalQuantity & " Item"
		else
			miniTotalQuantity = miniTotalQuantity & " Items"
		end if
	end if
	
	useMetaPage = replace(request.ServerVariables("HTTP_X_REWRITE_URL"),".htm",".asp")
	useMetaPage = replace(useMetaPage,".asp","")
	if pageName = "Basket" or pageName = "Basket2" then useMetaPage = "/cart/basket.asp"
	
	sql = "select title, description, keywords from MetaTags where siteID = 0 and PhysicalAbsolutePath = '" & useMetaPage & "'"
	session("errorSQL") = sql
	set metaRS = getRS(sql)
	
	if not metaRS.EOF then
		metaTitle = metaRS("title")
		metaDesc = metaRS("description")
		metaKeywords = metaRS("keywords")
	else
		sql = "select title, description, keywords from MetaTags where siteID = 0 and brandID = " & prepInt(brandID) & " and modelID is null and typeID is null"
'		response.write "2:" & sql & "<br>"
		if prepInt(modelID) > 0 then sql = replace(sql,"modelID is null","modelID = " & modelID)
		if prepInt(typeID) > 0 then sql = replace(sql,"typeID is null","typeID = " & typeID)
		saveSQL = sql
		session("errorSQL") = sql
		set metaRS = getRS(sql)
		
		if not metaRS.EOF then
			metaTitle = metaRS("title")
			metaDesc = metaRS("description")
			metaKeywords = metaRS("keywords")
		else
			sql = "select seTitle as title, seDescription as description, seKeywords as keywords from metaTags_default where siteID = 0 and basePage = '" & basePageName & "'"
'			response.write "3:" & sql & "<br>"
			session("errorSQL") = sql
			set metaRS = getRS(sql)
			
			if not metaRS.EOF then
				metaTitle = updateGenericMeta(metaRS("title"))
				metaDesc = updateGenericMeta(metaRS("description"))
				metaKeywords = updateGenericMeta(metaRS("keywords"))
			end if
		end if
	end if
	
	if prepStr(request.QueryString("utm_promocode")) <> "" then
		session("promocode") = prepStr(request.QueryString("utm_promocode"))
	end if
	if prepStr(request.QueryString("utm_email")) <> "" then	response.Cookies("myEmail") = prepStr(request.QueryString("utm_email"))
	
	canonicalURL = "http://www.wirelessemporium.com" & replace(request.ServerVariables("HTTP_X_REWRITE_URL"),".htm","")
	if prepVal(setCanonical) <> "" then
		canonicalURL = "http://www.wirelessemporium.com" & setCanonical
	end if
%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><%=metaTitle%></title>
    <meta name="Description" content="<%=metaDesc%>">
    <meta name="Keywords" content="<%=metaKeywords%>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=0"/>
    <meta name="format-detection" content="telephone=no">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300,600,700' rel='stylesheet' type='text/css'>
    <link id="baseCss" href="/template/css/base.css" rel="stylesheet" type="text/css">
    <link rel="canonical" href="<%=canonicalURL%>" />
    <% if pageName = "Home" then %><link href="/template/css/home.css" rel="stylesheet" type="text/css"><% end if %>
    <% if pageName = "BMC" then %><link href="/template/css/bmc.css" rel="stylesheet" type="text/css"><% end if %>
    <% if pageName = "Product" then %><link href="/template/css/product.css" rel="stylesheet" type="text/css"><% end if %>
    <% if pageName = "Basket" or pageName = "Basket2" then %><link href="/template/css/basket.css" rel="stylesheet" type="text/css"><% end if %>
    <% if instr(pageName,"Checkout") > 0 or pageName = "Confirm" then %><link href="/template/css/checkout.css" rel="stylesheet" type="text/css"><% end if %>
    <% if pageName = "Contact Us" then %><link href="/template/css/contact.css" rel="stylesheet" type="text/css"><% end if %>
    <% if pageName = "brands" then %><link href="/template/css/brand.css" rel="stylesheet" type="text/css"><% end if %>
    <% if pageName = "brandModel" then %><link href="/template/css/brandModel.css" rel="stylesheet" type="text/css"><% end if %>
	<script language="javascript">var myIP = '<%=request.ServerVariables("REMOTE_ADDR")%>';</script>
	<!--<script type="text/javascript" src="//www.wirelessemporium.com/mvt/mvtResults.js"></script>-->
    <!--<script type="text/javascript" src="//www.wirelessemporium.com/mvt/mvtMain.js"></script>-->
    <script language="javascript">
        //var uc = Math.floor(Math.random() * 999999999)
        //var mvtScript = document.createElement('script'); mvtScript.type = 'text/javascript';
        //mvtScript.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + '?jsID=' + uc;
        //var mvtClose = document.getElementsByTagName('script')[0]; mvtClose.parentNode.insertBefore(mvtScript, mvtClose);
    </script>

    <!-- BR implementation start -->
    <link href="/template/css/brsearch.css" rel="stylesheet" type="text/css">
    <script src="//code.jquery.com/jquery-1.9.1.min.js"></script>    
    <!--<script language="javascript" src="http://d1tpmtwl6eat2g.cloudfront.net/brm/wirelessemporium_com/br-mob-PREVIEW-ONLY.js"></script>-->
    <!--<script language="javascript" src="//docs.brmob.net/static/mob1/api_v1/docs/master/br-mob.js"></script>-->
    <script language="javascript" src="http://d1tpmtwl6eat2g.cloudfront.net/brm/wirelessemporium_com/br-mob.js"></script>
    <!-- BR implementatino end -->
	<script type="text/javascript">
	if(typeof window.WEDATA == 'undefined'){
		window.WEDATA = {
			storeName: 'CellularOutfitter.com',
			isMobile: 'true',
			internalUser: <%= jsStr(LCase(isInternalUser)) %>,
			pageType: <%= jsStr(replace(replace(request.ServerVariables("URL"),".asp",""),"/","")) %>,
			account: {
				email: <%= jsStr(Request.Cookies("user")("email")) %>,
				id: <%= jsStr(Request.Cookies("user")("id")) %>
			}
		};
	}
	</script>
    <% 	if instr(request.ServerVariables("SERVER_NAME"), "mdev") > 0  then %>
		<script src="//assets.adobedtm.com/d0f694a37a1061afe4e18245479cdeaed180db2f/satelliteLib-58fa2284a294781e2d10628d798d6b7dc4abe026-staging.js"></script>	
	<% else %>
		<script src="//assets.adobedtm.com/d0f694a37a1061afe4e18245479cdeaed180db2f/satelliteLib-58fa2284a294781e2d10628d798d6b7dc4abe026.js"></script>
	<% end if %>
    <%call printPIxel(1)%>
</head>
<body onscroll="userScrolled()">
<% if instr(request.ServerVariables("SERVER_NAME"),"mdev.") > 0 then response.Write(request.ServerVariables("LOCAL_ADDR") & "<br>") %>
<div id="mbox-global"></div>
<div id="mbox-page"></div>
<div id="popCover" style="height:100%; display:none; width:100%; background-color:#333; position:fixed; left:0px; top:0px; z-index:3000; opacity:0.6; filter:alpha(opacity=60);">&nbsp;</div>
<div id="popBox" style="height:2000px; display:none; width:100%; position:fixed; left:0px; top:0px; z-index:3001; text-align:center;"></div>
    <%call printPIxel(2)%>
<div id="wholeSite" class="<%=Request.ServerVariables("remote_addr")%>">
	<% if valPropBanner = 0 then %>
    <div id="freeShipTop">
        <div id="freeText" style="padding:5px;">Fast, FREE Shipping <span style="color:#FFF;">|</span> Easy 90-Day Returns</div>
    </div>
    <div id="logoRow">
        <div id="logoContainer">
            <div id="weLogo" style="width:261px;"><a href="/"><img src="/images/mobile/retina/home/we-m-logo.png" border="0" width="251" height="32" alt="Wireless Emporium Mobile Site" style="margin:10px auto 0;" /></a></div>
            <div id="cartIcon" onclick="window.location='/basket.htm'" style="width:58px;">
                <div style="background:url(/images/mobile/retina/home/we-m-shoppingcart.png) no-repeat;background-size:20px 20px; width:20px;height:20px;position:relative;margin:10px auto 0;"></div>
                <div id="inCart"><%=miniTotalQuantity%></div>
            </div>
        </div>
    </div>
    <% else %>
    <a href="/">
    <div id="bannerRow">
	    <div id="valPropBanner1"></div>
    </div>
    </a>
    <% end if %>
    <div id="searchBar">
        <div id="searchContainer">
            <div id="orderByPhone"><%=mobilePhoneText%>&nbsp;<a href="tel:<%=mobilePhoneNumber%>" id="csNumber"><%=mobilePhoneNumber%></a></div>
            <% if showSearch = 1 then %>
            <div id="searchButton"><a href="/search/search.htm"><img src="/images/mobile/retina/checkout/we-m-button-search2.png" border="0" width="44" height="33" alt="Product Search Link" /></a></div>
            <% end if %>
        </div>
    </div>
    <% if gpsLoc = 2 then %>
    <div id="gpsBar">
    	<div id="gpsMsg" onclick="navigator.geolocation.getCurrentPosition(GetLocation)"><img src="/images/mobile/buttons/useGPS.jpg" border="0" width="320" height="40" alt="Submit GPS Location" /></div>
    </div>
    <% end if %>
    <div style="clear:both;"></div>
    <% if pageName = "Home" or showTabs = 0 then %>
    <div id="adSpot" style="display:none;"><img src="/images/mobile/mobileAd.jpg" border="0" width="320" height="120" alt="Advertisement" /></div>
    <% elseif instr(pageName,"Checkout") > 0 then %>
    <div id="breadCrumbs">
        <div id="breadCrumbText"><a href="/" id="breadCrumbLink">Home</a></div>
        <div id="breadCrumbBreak"><img src="/images/mobile/breadCrumbBreak.gif" border="0" width="15" height="40" alt="Breadcrumb Break" /></div>
        <div id="breadCrumbText">
        	<div id="checkoutStepBox"><span style="font-weight:bold;">Checkout:</span> Step</div>
            <div id="checkoutStepImgBox">
            	<% if pageName = "Checkout1" then %><img src="/images/mobile/icons/step1.gif" border="0" width="20" height="20" alt="Checkout Step 1" /><% end if %>
                <% if pageName = "Checkout2" then %><img src="/images/mobile/retina/checkout/we-m-steps-2.png" border="0" width="20" height="20" alt="Checkout Step 2" /><% end if %>
                <% if pageName = "Checkout3" then %><img src="/images/mobile/retina/payment/we-m-steps-3.png" border="0" width="20" height="20" alt="Checkout Step 3" /><% end if %>
            </div>
            <div id="checkoutStepBox">of</div>
            <div id="checkoutStepImgBox"><img src="/images/mobile/retina/checkout/we-m-steps-3.png" border="0" width="20" height="20" alt="Total Checkout Steps" /></div>
        </div>
        <div id="breadCrumbBreak"><img src="/images/mobile/breadCrumbBreak.gif" border="0" width="15" height="40" alt="Breadcrumb Break" /></div>
    </div>
	<% else %>
    <div id="breadCrumbs" style="float:left;">
        <div id="breadCrumbText"><a href="/" id="breadCrumbLink">Home</a></div>
        <div id="breadCrumbBreak"><img src="/images/mobile/breadCrumbBreak.gif" border="0" width="15" height="40" alt="Breadcrumb Break" /></div>
        <div id="breadCrumbText"><a href="javascript:history.go(-1)" id="breadCrumbLink">Previous Page</a></div>
        <div id="breadCrumbBreak"><img src="/images/mobile/breadCrumbBreak.gif" border="0" width="15" height="40" alt="Breadcrumb Break" /></div>
    </div>
    <% end if %>
    <!--#include virtual="/template/frmSearch.asp"-->    
    <div style="clear:both;"></div>
    