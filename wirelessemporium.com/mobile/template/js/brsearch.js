	///**
	// * CONFIDENTIAL AND PROPRIETARY SOURCE CODE OF BLOOMREACH, INC.
	// *
	// * Copyright (C) 2013 BloomReach, Inc. All Rights Reserved.
	// *
	// * Use of this Source Code is subject to the terms of the applicable license
	// * agreement from BloomReach, Inc.
	// *
	// * The copyright notice(s) in this Source Code does not indicate actual or
	// * intended publication of this Source Code.
	// */
	
	/**
	 * This is an example implementation of the BloomMobile Autosuggest and Search plug-ins for DebShops.  This
	 * implementation avoids using jQuery Mobile.
	 *
	 * Author: william@bloomreach.com
	 */

	var curLoc = window.location.toString().toLowerCase();

	var BR = BR || {};
	BR.mobile = BR.mobile || {};
	BR.mobile.debshops = BR.mobile.debshops || {};
	
	/**
	 * #### BloomMobile Autosuggest Implementation for DebShops
	 */
	;(function($, window, document) {
	
	var self = this,
		location = window.location;
	
	  /* Default configuration */
	self.autosuggestConfig = {
	  accountId: 5159,
	  authKey: '',
	  domainKey: 'wirelessemporium_com',
	  environment: 'prod',
	  searchButton: '#br_searchBtn',
	  /*clearButton: '#brm-clear-btn',*/
	  resultContainer: '.brm-autosuggest-menu',
	  searchCallback: function(searchUrl, searchData) {
//		window._gaq = window._gaq || [];
//		window._gaq.push(['_setCustomVar', 1, '2013-04-16 Search', 'Test', 1]);
		location.hash = searchUrl.substring(searchUrl.indexOf('#') + 1);
	  }
	};
	
	self.addMltBtn = function() {
	  setTimeout(function() { // just to be safe
		var fMlt = function() {
		  var $this = $(this);
	
		  if (!$this.find('.mlt').length) {
	
			var titleElem = $this.find('#prodDesc');
			var pidElem = $this.find('#prodID');
			if (titleElem.length && pidElem.length) {
			  var title = encodeURIComponent(titleElem.text());
			  var pid = pidElem.text();
	
			  if (title && pid) {
//				var $mltBtn = $('<button class="mlt"></button>');
				var url = '/search/search.htm?q=' + pid + '#brm-search?request_type=mlt&brand=WE&title=' + title + '&pid=' + pid;
				var $mltBtn = $('<a href="' + url + '"><div class="mlt"></div></a>');
				  
				/*
				var $mltBtn = $('<button class="mlt"></button>');
				$mltBtn.click(function(event) {
				  window.location.hash = 'brm-search?request_type=mlt&brand=WE&title=' + title + '&pid=' + pid;
				  event.preventDefault();
				});
				*/
				$this.append($mltBtn);
			  }
			}
		  }
		}
		
		$('div.singleProdBox').each(fMlt);
		$('div.singleProdBox2').each(fMlt);
	  }, 100);
	};
	
	/* Initialize the plug-in */
	$(document).ready(function() {
//	  $('#br_searchBox').attr('autocomplete', 'off').wrap('<span id="brm-clear-wrapper" />').after('<a href id="brm-clear-btn">X</a>').brm_suggest(self.autosuggestConfig);
	  $('#br_searchBox').attr('autocomplete', 'off').brm_suggest(self.autosuggestConfig);
	  //if (location.hash.indexOf('brm-search') !== 1) {
		self.addMltBtn();
		$(document).ajaxComplete(self.addMltBtn); // for "view more" button
	 // }
	});
	
	}).apply(BR.mobile.debshops, [jQuery, window, document]);
	
	
	/**
	 * #### BloomMobile Search Implementation for DebShops
	 */
	;(function($, window, document) {
	
	var self = this,
		location = window.location,
		F = BR.mobile.apiFields;
	
	/* Render the entire page of search results. */
	self.renderResultsPage = function(results) {
		
	  window.scrollTo(0, 0); // scroll to top
	  brmLoadingContainer.hide();
	
	  var resultsContainer = $('#brm-results');
	  if (!resultsContainer.length) {
		resultsContainer = $('<div id="brm-results"></div>');
		brmSearchContainer.append(resultsContainer);
	  }
	  resultsContainer.html('');
	  
	  /* Breadcrumb */
	  var searchName = results.breadcrumbs[0].name;
	  
	  var loadBannerHot ="";
	  if (searchName === 'trending'){
		loadBannerHot = '<img style="width:320px;" src="/images/mobile/retina/whats-hot/we-m-banner-whatshot.png"><div style="margin:2px 0px 2px 0px;" class="tooltipBR">';
	  } else if (searchName === 'Just For You'){
		loadBannerHot = '<img style="width:320px;" src="/images/mobile/retina/just-for-you/we-m-banner-justforu.png"><div style="margin:2px 0px 2px 0px;" class="tooltipBR">';
	  }
	  else
		loadBannerHot = '<div class="tooltipBR">';
	  
	  var loadBanner = $('<div class="clear"></div>'+loadBannerHot+'</div>');
	  resultsContainer.append(loadBanner);
	
	  self.renderBreadcrumb(results.resultType, searchName, resultsContainer);
	
	  /* Keep the name of the search in the search box */
	  $('#br_searchBox').val(searchName);
	
	  /* Example Trending Link */
	  //self.renderTrending(resultsContainer);
	
	  /* Filters */
	  var filtersContainer = $('#brm-filters');
	  if (!filtersContainer.length) {
		filtersContainer = $('<div id="brm-filters"></div>');
		resultsContainer.append(filtersContainer);
	  }
	  self.renderFilters(results.availableFilters, results.currentFilters, filtersContainer);
	
	  /* Products */
	  var productGrid = $('<div></div>', { class: 'productgridpage productlisting' });
	  resultsContainer.append($('<div></div>', { class: 'productgrid' }).append(productGrid));
	  self.renderProducts(results.products, productGrid, results.brTrkData);
	  if (results.products.length == 0) {
		  resultsContainer.append($('<div class="clear" style="padding-bottom:5px;">No results found for "' + searchName + '"</div>'));
	  }
	
	  /* View More Button */
	  if (results.loadMore) {
		var loadMoreButton = $('<div class="clear"></div><a href="#" class="viewmore" style="margin-bottom: 20px;" onclick="return false;"><div class="bottomButtonGraphic">View More</div></a>');
		loadMoreButton.on('click', function(event) {
		  brmSearchContainer.brm_search('getNextPage', function(results) {
			self.renderProducts(results.products, productGrid, results.brTrkData);
			self.trackPageviewBrTrk(searchName, results.brTrkData);

			if (!results.loadMore) {
			  loadMoreButton.hide();
			}
		  });
		  event.preventDefault();
		  return false;
		});
		resultsContainer.append(loadMoreButton);
	  }
	
	  /* More Like This Header */
	  if (results.mltProduct) {
		self.renderMltHeader(results.mltProduct, productGrid, results.brTrkData);
	  }
	
	  /* Fire analytics pixels. */
	  if (suppressPageviewOnce) {
		suppressPageviewOnce = false;
	  } else {
//		window._gaq = window._gaq || [];
//		window._gaq.push(['_trackPageview', location.pathname + location.search + location.hash]);
		self.trackPageviewBrTrk(searchName, results.brTrkData);
	  }
	
	  brmSearchContainer.trigger('brm-rendered');

	};
	
	/* Render breadcrumbs. */
	self.renderBreadcrumb = function(resultType, breadcrumbText, container) {
	  var prefixText = 'Your Search results for ';
	  if (resultType === BR.mobile.requestType.MLT) { // Currently resultType is the same as requestType
		prefixText = 'More Like ';
	  }
	  container.append($([
						   '<div class="breadcrumb">',
							 '<div>',
							   '<!--<a href="/">Home</a>-->',
							   '<!--<span> > </span>-->',
							   '<span>', prefixText, '</span>',
							   '<a href="', location.href, '">', breadcrumbText, '</a>',
							 '</div>',
						   '</div>'
						 ].join('')));
	};
	
	/* Render the product list */
	self.renderProducts = function(products, productGrid, brTrkData) {
	  for (var i = 0; i < products.length; i++) {
		self.renderProduct(products[i], productGrid, brTrkData);
	  }

	  productGrid.append('<div class="clear"></div>');
	
	  $('.productgridpage button').off('click', self.handleMoreLikeThis);
	  $('.productgridpage button').on('click', self.handleMoreLikeThis);
	  $('.swatch-image').click(self.handleSwatch);
	};
	
	
	
	
	/* Render an individual product */
	self.renderProduct = function(product, productGrid, brTrkData) {
		var thumb_image = product.thumb_image;
		var big_image = thumb_image.replace("productpics/thumb", "productpics/big");
		var productTitle = product.title.length > 47 ? product.title.substr(0,44) + '...': product.title;
		var productURL = product.url;
		if (productURL.indexOf('www.') != -1) {
			if (curLoc.indexOf('m.wireless') != -1) {
				productURL = productURL.replace('www.', 'm.');
			} else {
				productURL = productURL.replace('www.', 'mdev.');
			}
			if (productURL.indexOf('?') != -1) {
				productURL = productURL.replace('?', '.htm?');
			} else {
				productURL = productURL + ".htm";
			}
		}
		
		var brInternalTracking = getInternalTracking(product, productURL);
		productURL = productURL + brInternalTracking;
		
		var productCell = $([
							'<a href="', productURL ,'" title="', product.title, '">',
								'<div class="product">',
									'<div class="brand_model">', product.brand, '</div>',
									'<div class="image">',
										'<img class="thumb-image" src="', big_image, '" border="0" width="100" />',
									'</div>',
									'<div class="name">', productTitle, '</div>',
									'<div class="pricing">',
										'<div class="price">',
											self.renderProductPrice(product.price, product.sale_price),
										'</div>',
									'</div>',
									self.renderPromotion(product.promotions),
									'<button class="mlt" data-href="', product.mltUrl, '"></button>',
								  '<div class="clear"></div>',
								'</div>',
							'</a>'
						  ].join(''));
		productCell.data('brTrkData', brTrkData);
		productGrid.append(productCell);
	};
	
	/* Render the More Like This product header on a More Like This search results page */
	self.renderMltHeader = function(product, productGrid, brTrkData) {
		var product_thumb = product.thumb_image;
		var product_big = product_thumb.replace("productpics/thumb", "productpics/big");
		
		var productTitle = product.title.length > 66 ? product.title.substr(0,63) + '...': product.title;
		
		var productURL = product.url;
		if (productURL.indexOf('www.') != -1) {
			if (curLoc.indexOf('m.wireless') != -1) {
				productURL = productURL.replace('www.', 'm.');
			} else {
				productURL = productURL.replace('www.', 'mdev.');
			}
			if (productURL.indexOf('?') != -1) {
				productURL = productURL.replace('?', '.htm?');
			} else {
				productURL = productURL + ".htm";
			}
		}

		var brInternalTracking = getInternalTracking(product, productURL);
		productURL = productURL + brInternalTracking;
						
		var mltHeader = $([
						  '<a href="', productURL ,'" title="', product.title, '" class="mlt-header">',
							'<div class="producttile" align="center">',
								'<div class="tileBox">',
									'<div class="imgBox">',
										'<img src="', product_big, '" border="0" width="100" />',
									'</div>',
									'<div class="productdetail">',
									  'Items similar to:<br>',
									  '<div class="name">', productTitle, '</div>',
									  '<div class="pricing">',
										'<div class="price">',
										  self.renderProductPrice(product.price, product.sale_price),
										'</div>',
									  '</div>',
									'</div>',
								'</div>',
							'</div>',
						  '</a>',
						  '<div class="clear"></div>',
						].join(''));
	  mltHeader.data('brTrkData', brTrkData);
	  productGrid.prepend(mltHeader);
	};
	
	/* Render product price */
	self.renderProductPrice = function(price, salePrice) {
	  if (price && salePrice && price != salePrice) {
		return '<div class="discountprice"><div class="standardprice">$' + price.toFixed(2) + '</div><div class="salesprice">$' + salePrice.toFixed(2) + '</div></div>';
	  } else {
		return '<div class="salesprice">$' + price.toFixed(2) + '</div>';
	  }
	};
	
	/* Render product promotion */
	self.renderPromotion = function(promotion) {
	  var renderedHTML = '';
	  if (promotion) {
		renderedHTML = '<div class="promo"><div class="promotionalMessage">' + promotion + '</div></div>';
	  }
	  return renderedHTML;
	};
	
	/* Render product swatch */
	self.renderSwatch = function(variants, currThumb) {
	  var renderedHTMLs = [];
	  if (variants) {
		if (variants.length > 1) {
		  renderedHTMLs.push(['<div class="icons_more"><img alt="more colors" src="http://demandware.edgesuite.net/aahi_prd/on/demandware.static/Sites-DebShops-Site/Sites-DebShops-Library/default//images/flags/more_colors.png" /><\/div>'].join(''));
		}
	/*  for (var i = 0, l = variants.length; i < l; i++) {
		  var variant = variants[i];
		  if (variant.sku_swatch_images && variant.sku_swatch_images.length) {
			var color = variant.sku_color_group;
			var swatch = variant.sku_swatch_images[0];
			var thumb = '';
			if (variant.sku_thumb_images && variant.sku_thumb_images.length) {
			  thumb = variant.sku_thumb_images[0];
			}
			var selected = thumb === currThumb ? 'selected': '';
			renderedHTMLs.push(['<img alt="', color, '" class="swatch-image ', selected, '" src="', swatch, '" data-sku-thumb-image="', thumb, '" />'].join(''));
		  }
		}*/
	  }
	  return renderedHTMLs.join('');
	};
	
	/* Event handler for More Like This button */
	self.handleMoreLikeThis = function(event) {
	  var href = $(event.target).data('href');
	  if (href) {
		location.hash = href;
	  }
	  return false;
	};
	
	/* Event handler for Swatch */
	self.handleSwatch = function(event) {
	  event.preventDefault();
	  var $this = $(this);
	  $this.siblings().removeClass('selected');
	  $this.addClass('selected');
	  var thumbImage = $this.data('sku-thumb-image');
	  if (thumbImage) {
		$this.parents('.producttile').find('.thumb-image').attr('src', thumbImage);
	  }
	};
	
	/* Render the group of filters */
	self.renderFilters = function(availableFilters, currentFilters, filtersContainer) {
	  filtersContainer.html('');
	
	  var controlGroup = $('<fieldset style="text-align: center"></fieldset>');
	  filtersContainer.append(controlGroup);
	
	  for (var i = 0; i < availableFilters.length; i++) {
		self.renderFilter(availableFilters[i], currentFilters, controlGroup);
	  }
	
	  filtersContainer.trigger('create');
	  filtersContainer.off('change');
	  filtersContainer.on('change', function(event) {
		location.hash = $(event.target[event.target.selectedIndex]).data('url');
	  });
	};
	
	/* Raw Filter names */
	self.filterNames = {
	  size: 'sizes',
	  category: 'category',
	  color: 'color_groups'
	};
	
	/* display names for filters */
	self.filterDisplayNames = {
	  'color_group': 'color'   // remove after migration
	};
	self.filterDisplayNames[self.filterNames.color] = 'color';
	self.filterDisplayNames[self.filterNames.size] = 'size';
	
	/* Sorting comparators */
	self.predefinedSizeOrder = {
	  'xs': 1,
	  's': 2,
	  's/m': 3,
	  'm': 4,
	  'm/l': 5,
	  'l': 6,
	  'l/x': 7,
	  'xl': 8,
	  '1x': 9,
	  '2x': 10,
	  '3x': 11,
	  'a': 12,
	  'b': 13,
	  'c': 14,
	  'd': 15
	};
	self.productComparator = {
	  alphabetical: function(a, b) {
		a = a.label;
		b = b.label;
		if (!a) {
		  return !b ? 0 : -1;
		} else if (!b) {
		  return 1;
		} else {
		  return a > b ? 1 : (a < b ? -1 : 0);
		}
	  }
	};
	self.productComparator[self.filterNames.size] = function(a, b) {
	  a = a.label;
	  b = b.label;
	  if (!a) {
		return !b ? 0 : -1;
	  } else if (!b) {
		return 1;
	  } else if (typeof self.predefinedSizeOrder[a] !== 'undefined') {
		return typeof self.predefinedSizeOrder[b] !== 'undefined' ? self.predefinedSizeOrder[a] - self.predefinedSizeOrder[b] : -1;
	  } else if (typeof self.predefinedSizeOrder[b] !== 'undefined') {
		return 1;
	  } else {
		var floatA = parseFloat(a);
		var floatB = parseFloat(b);
		var floatDiff = floatA - floatB;
		if ((isNaN(floatA) && isNaN(floatB)) || floatDiff === 0) {
		  return a > b ? 1 : (a < b ? -1 : 0);
		} else {
		  return isNaN(floatA) ? 1 : (isNaN(floatB) ? -1 : floatDiff);
		}
	  }
	};
	self.productComparator[self.filterNames.category] = function(a, b) {
	  var aLabel = a.label;
	  var bLabel = b.label;
	  var alwaysLast = 'clearance';
	  if (!aLabel) {
		return !bLabel ? 0 : -1;
	  } else if (!bLabel) {
		return 1;
	  } else if (aLabel.toLowerCase() === alwaysLast) {
		return bLabel.toLowerCase() === alwaysLast ? 0 : 1;
	  } else if (bLabel.toLowerCase() === alwaysLast) {
		return -1;
	  } else {
		var aCount = a.count;
		var bCount = b.count;
		return aCount > bCount ? -1 : (aCount < bCount ? 1 : 0);
	  }
	};
	
	/* Render an individual filter */
	self.renderFilter = function(filter, currentFilters, container) {
	  var displayName = self.filterDisplayNames[filter.name] || filter.name;
	  displayName = displayName[0].toUpperCase() + displayName.substr(1);
	
	  if (currentFilters && currentFilters[filter.name]) {
		var filterContainer = $('<span></span>', {'class': 'filter'});
		filterContainer.append(displayName + ': ' + currentFilters[filter.name].value + ' ');
		var clearButton = $('<a href="' + currentFilters[filter.name].removalUrl + '">[X]</a>');
		filterContainer.append(clearButton);
		container.append(filterContainer);
		
		clearButton.on('click', function(event) {
		  location.hash = currentFilters[filter.name].removalUrl;
		  event.preventDefault();
		  return false;
		});
		
	  } else if (filter.name !== self.filterNames.size || (currentFilters && currentFilters[self.filterNames.category])) {
		var renderedHTML = [
							 '<select>',
							 '<option value="">', displayName, '</option>'
						   ];
		var options = filter.options.sort(self.productComparator[filter.name] || self.productComparator.alphabetical);
		for (var i = 0; i < options.length; i++) {
		  var option = options[i];
		  if (filter.name !== self.filterNames.category || option.level === 1) { // Only category has a level attribute
			renderedHTML.push(['<option value="', option.value, '" data-url="', option.url, '">', option.label, ' (', option.count, ')', '</option>'].join(''));
		  }
		}
		renderedHTML.push('</select>');
		container.append($(renderedHTML.join('')));
	  }
	};
	
	/* Render the trending link.  This is an examprendle of how to add a link to BloomMobile's Trending feature. */
	self.renderTrending = function(container) {
	  var trendingLink = $('<a href="' + BR.mobile.getTrendingUrl() + '" class="brm-trending">Example Trending Link</a>');
	  trendingLink.on('click', function(event) {
		var href = $(event.target).attr('href');
		if (href) {
		  location.hash = href;
		}
		return false;
	  });
	  container.append(trendingLink);
	};
	
	/* Stores the state of the original page before searching, so that it can be restored on back button. */
	self.originalState = {};
	
	/* Custom HTML5 page transition for BloomMobile Search */
	var onHashChangeDisabled = false,
		onPopStateDisabled = false,
		hashChangeTimeout = 200,
		hashChangeEnableTimer;
	
//	var brmLoadingContainer = $('<div class="brm-dim"><div id="brm-loading" class="brm-loading-background"></div></div>');
	var brmLoadingContainer = $('<div id="preLoading"></div>');
	self.changePage = function(event) {
	  if (onHashChangeDisabled) {
		return;
	  }
	
	  if (location.hash.indexOf('brm-search') === 1) {
		/* Store the baseURI of the document before we change it with replaceState */
		var $head = $('head'),
			$base = $head.children('base');
		if (!$base.length) {
		  $('<base>', {href: location.pathname + location.search}).prependTo($head);
		}
	
		/* Show/Insert lading sign */
		if (!self.originalState.results) {
		  self.originalState.results = $('#main');
		  if (self.originalState.results.length == 0) self.originalState.results = $('#mainProductBox');
		  brmLoadingContainer.insertAfter(self.originalState.results);
		}
		brmLoadingContainer.show();
	
		/* Update the address bar. */
		var hashUrl = location.hash.substr(1);
		history.replaceState(null, null, self.generateFullUrl(BR.mobile.getHashUrlParams(hashUrl), hashUrl));
	
		/* Search. */
		self.showBrmSearch();
	  } else {
		self.hideBrmSearch();
		self.originalState = {};
	  }
	};
	$(window).on('hashchange', self.changePage);
	
	/* Handle back and forward buttons */
	$(window).on('popstate', function(event) {
	  if (onPopStateDisabled) {
		return;
	  }
	
	  // if we get two pop states in under hashChangeTimeout
	  // make sure to clear any timer set for the previous change
	  clearTimeout( hashChangeEnableTimer );
	
	  // make sure to enable hash handling for the changePage call
	  onHashChangeDisabled = false;
	  self.changePage(event);
	
	  // prevent any hashchange in the next hashChangeTimeout
	  onHashChangeDisabled = true;
	
	  // re-enable hashchange handling after swallowing a possible hashchange
	  // event that comes on all popstates courtesy of browsers like Android
	  hashChangeEnableTimer = setTimeout(function() {
		onHashChangeDisabled = false;
	  }, hashChangeTimeout);
	});
	
	/* Generate a URL similar to existing search URLs using data from BloomMobile hash parameters */
	self.generateFullUrl = function(hashParameters, hashUrl) {
	  var url = '/search/search.htm?q=' + (hashParameters.q || hashParameters.pid);
	
	  if (hashParameters.fq) {
		var fq_params = hashParameters.fq;
		if (typeof fq_params === 'string') {
		  fq_params = [fq_params];
		}
		for (var i = 0; i < fq_params.length; i++) {
		  var filter = fq_params[i].split(':');
		  url += '&f_' + encodeURIComponent(filter[0]) + '=' + encodeURIComponent(filter[1]);
		}
	  }
	
	  return url + '#' + hashUrl;
	};
	
	/* Use the BloomMobile Search plug-in to perform a search based on the URL and show the results. */
	self.showBrmSearch = function() {
	  /*
	   * When the results have been rendered:
	   * 
	   * - Show the plug-in's rendered results.
	   * - Hide the original results.
	   */
	  brmSearchContainer.one('brm-rendered', function(event) {
		var resultsContainer = $('#brm-results');
	
		resultsContainer.insertBefore(self.originalState.results);
		self.originalState.results.hide();
	  });
	
	  /* Call the search plug-in.  The 'search' method performs a search based on the location in the address bar. */
	  brmSearchContainer.brm_search('search');
	};
	
	/* Restore the page back to what it was before the search plug-in modified it. */
	self.hideBrmSearch = function() {
	  var resultsContainer = $('#brm-results');
	
	  if (self.originalState.results) {
		location.reload();
	  }
	};

	self.trackPageviewBrTrk = function(searchTerm, br_data) {
	  br_data = $.extend({}, br_data);
	
	  br_data.acct_id = "5159";
	  br_data.ptype = "search";
	  br_data.cat = "";
	  br_data.prod_id = "";
	  br_data.prod_name = "";
	  br_data.search_term = searchTerm;
	  br_data.is_conversion = "0";
	
	  br_data.basket_value = "0.00";
	  br_data.order_id ="";
	
	  try {
		if (BrTrk) {
		  var tracker = BrTrk.getTracker(0.2, br_data);
		  tracker.updateBrData(br_data);
		  tracker.logPageView();
		}
	  } catch(err) {
		console.error(err);
	  }
	};
	
	/* Default configuration */
	self.searchConfig = {
	  accountId: 5159,
	  authKey: '',
	  domainKey: 'wirelessemporium_com',
	  environment: 'prod',
	  renderFunction: self.renderResultsPage,
	  fieldNames: [F.pid, F.title, F.brand, F.price, F.sale_price, F.promotions, F.thumb_image, F.sku_thumb_images, F.sku_swatch_images, F.sku_color_group, F.url],
	  variants_by: F.color_group
	};
	
	/* Initialize the plug-in */
	var brmSearchContainer = $('<div id="brm-search"></div>');
	$(document).ready(function() {
		
	  $(document.body).append(brmSearchContainer);
	  brmSearchContainer.brm_search(self.searchConfig);
	});
	
	/* Code to redirect url to brm search if a user in test group lands to control search page from outside */
	var controlSearchUrls = [ '/search/search.htm?q=',
							  '/search/search.htm?q='
	];
	
	var getControlQuery = function() {
	  var regex = new RegExp('q=([^\?\&#]*)'),
		  match = regex.exec(location.href);
	  if (match) {
		return match[1];
	  }
	  return '';
	};
	
	
	$(document).ready(function(){
	  BR.mobile.loadBRSearchIfTestGroup(self.searchConfig.accountId, controlSearchUrls, getControlQuery, function(searchUrl) {
		location.hash = searchUrl;
	  });
	});
	
	/*
	 * This block of code runs once if the visitor lands on a BloomMobile URL.
	 * - Hide momentary flash of search results from server before BloomMobile search results are rendered.
	 * - Trigger a popstate event to kick off the search.
	 * - Suppress the GA pageview triggered by BloomMobile Search, since the page is already firing one.
	 */
	if (location.hash.indexOf('brm-search') === 1 || BR.mobile.isBRSearchRedirect(self.searchConfig.accountId, controlSearchUrls)) {
	  $(document.head).prepend($('<style></style>', {type: 'text/css'}).html('#main {display: none;}'));
	  $(document).ready(function() {
		$(window).triggerHandler('popstate');
		// Chrome fires an extraneous popstate on page load.  Squash it before it triggers a duplicate search.
		setTimeout(function() {onPopStateDisabled = true;}, 0);
	  });
	
	  $(window).on('load', function() {
		setTimeout(function() {
		  onPopStateDisabled = false;
		}, hashChangeTimeout);
	  });
	
	  var suppressPageviewOnce = true;
	}
	
	/*
	 * Additional cookie data for tracking pixels.
	 */
	window.BrTrkConfig = window.BrTrkConfig || {};
	window.BrTrkConfig.pixelLogCallback = function(brData) {
	  // _br_me will be deprecated in the future.  It can be removed once the transition to _br_mec is complete.
	  if ($.cookie('_br_me')) {
		brData['_br_me'] = $.cookie('_br_me');
	  }
	  if ($.cookie('_br_mec')) {
		brData['_br_mec'] = $.cookie('_br_mec');
	  }
	};
	
	}).apply(BR.mobile.debshops, [jQuery, window, document]);
	
	
	/**
	 * #### BloomMobile More Like This Widget Implementation for DebShops
	 */
	;(function($, window, document) {
	
	var self = this,
		mltWidgetContainer = $('<div></div>');
	
	/* Default configuration */
	self.mltConfig = {
	  searchConfig: self.searchConfig,
	  numResults: 4,
	  renderFunction: function(results) {
		var productGrid = $('<div></div>', { 'class': 'productgridpage' });
		mltWidgetContainer.append($('<div class="panel open"><div class="toggle"></div></div>', { 'class': 'productgrid' }).append(productGrid));
		
//		var productGrid2=($('<div class="panel open"><div class="toggle2">More Like This</div></div>').append(productGrid));
		var productGrid2=($('<div class="panel open"><div class="toggle2"></div></div>').append(productGrid));
		mltWidgetContainer.append($('<div class="description accordion"><div class="panel open"></div>', { 'class': 'productgrid' }).append(productGrid2));
		
		self.renderProducts(results.products, productGrid, results.brTrkData);
//		productGrid.append($('<a></a>', {'href': results.viewMore,  'class': 'viewmore'}).text('View More'));
		productGrid.append($('<a></a>', {'href': results.viewMore,  'class': 'viewmore'}).append($('<div></div>', { 'class': 'bottomButtonGraphic' }).text('View More')));
	  }
	};
	
	
	
	/* Initialize the plug-in */
	$(document).ready(function() {
	  /* Make sure to do this only on a product page */
	  
//	  var productTitle = $.trim($('div.prod-title > h4').first().text()),
	  var productTitle = $('#br-prod-title').text(),
		  productDisplay = $('#brm-mlt-widget');
//		  alert('productTitle: ' + productTitle + '\r\nproductDisplay: ' + productDisplay.length);
	  if (productTitle && productDisplay.length) {
		var product = {title: productTitle},
//			productID = $.trim($('.productid').first().text().replace(/(style# |SKU: )/g, ''));
			productID = $('#br-prodid').text();
		if (productID) {
		  product.pid = productID;
		}
		self.mltConfig.product = product;
	
		/* Insert MLT widget container */
		productDisplay.append(mltWidgetContainer);
	
		mltWidgetContainer.brm_mlt(self.mltConfig);
	  }
	});
	
	
	
	
	}).apply(BR.mobile.debshops, [jQuery, window, document]);


function getUrlVars(pUrl)
{
	var vars = [], hash, hashes;
//	hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	if (pUrl.indexOf("#brm-search?") != -1) hashes = pUrl.substring(pUrl.indexOf("#brm-search?")+12).split('&');
	if (hashes != undefined) {
		for(var i = 0; i < hashes.length; i++)
		{
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
	}
	return vars;
}

function getInternalTracking(oProduct, pProductURL)
{
//	alert(curLoc + '\r\n' + location.hash);
	var arrUrlVars = getUrlVars(location.hash);
	var ret = "";
	if (arrUrlVars["request_type"] != undefined) {
		ret = "IZID=INT_";
		if (arrUrlVars["request_type"] == "jfy") {
			ret = ret + "JFY_NULL_" + oProduct.pid + "&utm_source=BR&utm_medium=INT&keywords=NULL";
		} else if (arrUrlVars["request_type"] == "mlt") {
			ret = ret + "MLT_" + arrUrlVars["title"] + "_" + oProduct.pid + "&utm_source=BR&utm_medium=INT&keywords=" + arrUrlVars["title"];
		} else if (arrUrlVars["request_type"] == "search") {
			if (arrUrlVars["search_type"] != undefined) {
				if (arrUrlVars["search_type"] == "trending") {
					ret = ret + "WH_NULL_" + oProduct.pid + "&utm_source=BR&utm_medium=INT&keywords=NULL";
				} else if (arrUrlVars["search_type"] == "keyword") {
					ret = ret + "SQ_" + arrUrlVars["q"] + "_" + oProduct.pid + "&utm_source=BR&utm_medium=INT&keywords=" + arrUrlVars["q"];
				}
			}
		} 
	}
	
	if ((pProductURL.indexOf(".htm?") != -1)&&(ret != "")) {
		ret = "&" + ret;
	} else {
		ret = "?" + ret;
	}
	
	return ret;
}
