	var scrollUpdate = 11
	var nextGrowStage = 200
	var activeDot = 0
	
	var winLoc = window.location.toString();
	winLoc = winLoc.toLowerCase();
	
	if (useHttps == 1 && winLoc.indexOf("https:") < 0) {
		window.location = winLoc.replace("http:","https:")
	}
	else if (useHttps == 0 && winLoc.indexOf("http:") < 0) {
		window.location = winLoc.replace("https:","http:")
	}

	function showHide(show,hide) {
		if (document.getElementById(show) != null) document.getElementById(show).style.display = 'block';
		if (document.getElementById(hide) != null) document.getElementById(hide).style.display = 'none';
	}
		
	function clearDefault() {
		if (document.searchForm.searchTextBox.value == "Search by Keyword(s) or Item#") {
			document.searchForm.searchTextBox.value = ""
		}		
	}
	
	function searchClick() {
		if (document.searchForm.searchTextBox.value == "Search by Keyword(s) or Item#" || document.searchForm.searchTextBox.value == "") {
			alert("Please enter search keywords first")
			document.searchForm.searchTextBox.focus()
			return false
		}
		else {
			return true
		}
	}
	
	function userScrolled() {
		if (pageName == "BMC") {
//			if (document.body.scrollTop > nextGrowStage) {
			if ($(document).scrollTop() > nextGrowStage) {	
				nextGrowStage = nextGrowStage + 200
				for (i=0;i<4;i++) {
					document.getElementById("singleProdBox_" + scrollUpdate).setAttribute("class", "singleProdBox")
					document.getElementById("singleProdBox_" + scrollUpdate).innerHTML = document.getElementById("singleProdBox_" + scrollUpdate).innerHTML.replace("tempimg","img")
					scrollUpdate++
				}
			}
		}
	}
	
	function GetLocation(location) {
		alert(location.coords.latitude);
		alert(location.coords.longitude);
	}
	
	function showFullDesc() {
		if (document.getElementById("showDesc").className == 'fl prodTitleContract') {
			document.getElementById("shortDesc").style.display = '';
			document.getElementById("fullDesc").style.display = 'none';
			document.getElementById("showDesc").className = 'fl prodTitleExpand';
		}
		else {
			document.getElementById("shortDesc").style.display = 'none';
			document.getElementById("fullDesc").style.display = '';
			document.getElementById("showDesc").className = 'fl prodTitleContract';
		}
	}
	
	function showMoreReviews(totalReviews) {
		for (i=1;i<=totalReviews;i++) {
			document.getElementById("reviewCnt_" + i).className = 'tb singleReview';
		}
		document.getElementById("moreReviewButton").style.display = 'none';
	}
	
	function detailExpand(which) {
		if (document.getElementById(which).className == "fr detailExpand") {
			document.getElementById(which).className = "fr detailContract"
			if (which == "detailButton") {
				document.getElementById("prodDetails").style.display = '';
			}
			else {
				document.getElementById("prodOverview").style.display = '';
			}
		}
		else {
			document.getElementById(which).className = "fr detailExpand"
			if (which == "detailButton") {
				document.getElementById("prodDetails").style.display = 'none';
			}
			else {
				document.getElementById("prodOverview").style.display = 'none';
			}
		}
	}
	
	function addToCart_ap(itemID) {
		eval("document.ap_" + itemID + ".submit()");
	}
	
	function swipePrimary(obj) {
		if (obj.scrollLeft < 250) {
			document.getElementById("imgDot_" + activeDot).className = "fl imgDotInactive"
			document.getElementById("imgDot_0").className = "fl imgDot"
			activeDot = 0
		}
		else if (obj.scrollLeft > 250 && obj.scrollLeft < 550) {
			document.getElementById("imgDot_" + activeDot).className = "fl imgDotInactive"
			document.getElementById("imgDot_1").className = "fl imgDot"
			activeDot = 1
		}
		else if (obj.scrollLeft > 550 && obj.scrollLeft < 850) {
			document.getElementById("imgDot_" + activeDot).className = "fl imgDotInactive"
			document.getElementById("imgDot_2").className = "fl imgDot"
			activeDot = 2
		}
		else if (obj.scrollLeft > 850 && obj.scrollLeft < 1150) {
			document.getElementById("imgDot_" + activeDot).className = "fl imgDotInactive"
			document.getElementById("imgDot_3").className = "fl imgDot"
			activeDot = 3
		}
		else if (obj.scrollLeft > 1150 && obj.scrollLeft < 1450) {
			document.getElementById("imgDot_" + activeDot).className = "fl imgDotInactive"
			document.getElementById("imgDot_4").className = "fl imgDot"
			activeDot = 4
		}
		else if (obj.scrollLeft > 1450 && obj.scrollLeft < 1750) {
			document.getElementById("imgDot_" + activeDot).className = "fl imgDotInactive"
			document.getElementById("imgDot_5").className = "fl imgDot"
			activeDot = 5
		}
		else if (obj.scrollLeft > 1750 && obj.scrollLeft < 2050) {
			document.getElementById("imgDot_" + activeDot).className = "fl imgDotInactive"
			document.getElementById("imgDot_6").className = "fl imgDot"
			activeDot = 6
		}
		else if (obj.scrollLeft > 2050 && obj.scrollLeft < 2350) {
			document.getElementById("imgDot_" + activeDot).className = "fl imgDotInactive"
			document.getElementById("imgDot_7").className = "fl imgDot"
			activeDot = 7
		}
		else if (obj.scrollLeft > 2350) {
			document.getElementById("imgDot_" + activeDot).className = "fl imgDotInactive"
			document.getElementById("imgDot_8").className = "fl imgDot"
			activeDot = 8
		}
	}
	
	function showFloatingZoomImage(itemid) {
		viewPortH = $(window).height(); //get browser's viewport height.
		ajax("/ajax/ajaxZoomImage.asp?itemid=" + itemid + "&screenH="+viewPortH,'popBox');
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	
	function closeFloatingBox() {	
		//------------------ change it to original -------------------//
		document.getElementById("popBox").style.position = "fixed";
		document.getElementById("popCover").onclick = function () {}
		document.getElementById("popBox").onclick = function () {}
		//------------------------------------------------------------//
				
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
	
		document.getElementById("popCover").innerHTML = "";
		document.getElementById("popBox").innerHTML = "";
	}
	
	// bloomreach add/remove cart tracking
	function br_cartTracking(tType, itemid, color, prodName, price) {
		try
		{
			if (tType == 'add') {
				BrTrk.getTracker().logEvent('Cart', tType, {'prod_id': itemid, 'sku': itemid, 'prod_color': color, 'prod_name': prodName}, {'price' : price});
			 } else {
				BrTrk.getTracker().logEvent('Cart', tType, {'prod_id': itemid, 'sku': itemid, 'prod_color': color, 'prod_name': prodName});
			}
			return false;
		 }
		catch(e) {}    
	}	
	
	/*email acquisition widget*/
	function closeEmailWidget(widgetid) {
		showHide(null,widgetid);
		showHide(null,'fade');
	}
	
	function showEmailWidget() {
		showHide('overlay1', null);
		showHide('fade', null);
	}	

	function closeEmailIcon() {
		showHide(null, 'floating15');
		ajax('/ajax/newsletter.asp?semail=closewidget','dumpZone');
	}
	
	function addNewsletter(email) {
		document.getElementById('overlay2').innerHTML = '';
		ajax('/ajax/newsletter.asp?semail=' + email,'overlay2');
		jQuery(window).trigger('newsletterSignup', ['overlay', email]);
		showHide('overlay2', null);
		showHide(null,'overlay1');
	}	