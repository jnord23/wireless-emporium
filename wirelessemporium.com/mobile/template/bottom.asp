	<div class="CallToAction">WHY SHOP WITH US?</div>
    <div class="ctaDownArrow"></div>
    <div class="whyShopItem">
        <div class="fl whyShopCheckmark"><img src="/images/mobile/icons/whyShopCheckmark.gif" /></div>
        <div class="fl whyShopText">Over 1,000,000 Orders Shipped!</div>
    </div>
    <div class="whyShopItem">
        <div class="fl whyShopCheckmark"><img src="/images/mobile/icons/whyShopCheckmark.gif" /></div>
        <div class="fl whyShopText">All Orders Ship Within 24 Hours</div>
    </div>
    <div class="whyShopItem">
        <div class="fl whyShopCheckmark"><img src="/images/mobile/icons/whyShopCheckmark.gif" /></div>
        <div class="fl whyShopText">90-Day EZ No-Hassle Return Policy</div>
    </div>
    <div class="whyShopItem">
        <div class="fl whyShopCheckmark"><img src="/images/mobile/icons/whyShopCheckmark.gif" /></div>
        <div class="fl whyShopText">Safe &amp; Secure SSL Encrypted Checkout</div>
    </div>
    <div class="safeShopping"><img src="/images/mobile/icons/safeShopping.jpg" /></div>
    <%if pageName = "Basket" then%>
    <div style="font-size:11px; padding-top:15px; text-align:center; color:#666;">*Cart ID: <%=mySession%></div>
    <%end if%>
    <div id="lowerBox">
        <div id="socializeText">Want to socialize with us?</div>
        <div id="socialIcons">
        	<div id="socialIcon"><a href="http://twitter.com/#!/wirelessemp/"><img src="/images/mobile/icons/twitter.gif" border="0" width="29" height="28" alt="Visit Our Twitter Page" /></a></div>
            <div id="socialIcon"><a href="http://www.facebook.com/WirelessEmporium"><img src="/images/mobile/icons/facebook.gif" border="0" width="29" height="28" alt="Visit Our Facebook Page" /></a></div>
            <div id="socialIcon"><a href="https://pinterest.com/wirelessemp/"><img src="/images/mobile/icons/pintrest.gif" border="0" width="29" height="28" alt="Visit Our Pintrest Page" /></a></div>
            <div id="socialIcon"><a href="https://plus.google.com/113217319312103488691?rel=author"><img src="/images/mobile/icons/googlePlus.gif" border="0" width="29" height="28" alt="Visit Our Google Page" /></a></div>
        </div>
        <div id="lowerSpacer"></div>
        <div id="footerText"><a href="/faq.htm" id="freeText">Help</a> | <a href="<%=fullSiteLink%>?mobileAccess=2" id="freeText">View Full Site</a> | <a href="tel:<%=mobilePhoneNumber%>" id="freeText"><%=mobilePhoneNumber%></a></div>
        <div id="footerText"><a href="/privacy.htm" id="bottomLinks">Privacy Policy</a> | <a href="/terms.htm" id="bottomLinks">Terms and Conditions</a> | <a href="/contact.htm" id="bottomLinks">Contact Us</a></div>
        <div id="footerText">&copy; 2002-<%=year(date)%> Wireless Emporium, Inc.</div>
    </div>
</div>
<%if prepInt(request.cookies("welcomeCouponUsed")) = 0 and instr(lcase(pageName), "checkout") = 0 and pageName <> "Basket" and pageName <> "Confirm" then%>
<div id="floating15" class="discount-container">
    <div class="discount">
        <div class="save" onclick="showEmailWidget()"></div>
        <input type="button" class="get-discount" value="GET DISCOUNT" onclick="showEmailWidget()" />
        <a href="javascript:closeEmailIcon()" id="close"><div class="close"></div></a>
    </div>
</div>
<%end if%>
<div id="overlay1">
    <div class="btn-close" onclick="closeEmailWidget('overlay1')"></div>
    <div class="modal-content">
        <p class="discount-title">Want to save 15% OFF today's order?</p>
        <p class="discount-subtitle">Enter your email address below.</p>
        <p style="text-align:center;"><input id="id_modal_email" class="modal-email" type="text" name="email" value="Enter your email for your custom coupon link!" onfocus="this.value='';" /></p>
        <p style="text-align:center;">
            <input type="button" class="get-discount-modal" value="GET DISCOUNT" onclick="addNewsletter(document.getElementById('id_modal_email').value); _gaq.push(['_trackEvent', 'Email Signup', 'Submission', 'Mobile', 'Widget']);" />
        </p>
    </div>
</div>
<div id="overlay2"></div>
<div id="fade"></div>

    <%call printPIxel(3)%>
    <!--#include virtual="/includes/asp/pixels/inc_br.asp"-->
    <%call CloseConn(oConn)%>
</body>
</html>
<div id="dumpZone" style="display:none;"></div>
<div id="testZone"></div>
<script type="text/javascript">
	<%if prepStr(request.QueryString("utm_newsletter")) <> "" then%>
	setTimeout(function(){ 
		showEmailWidget();
		addNewsletter('<%=prepStr(request.QueryString("utm_newsletter"))%>'); 
	}, 500);
	<%end if%>

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-1311669-2']);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
  
	var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-38929999-1']);
		_gaq.push(['_setDomainName', 'wirelessemporium.com']);
		_gaq.push(['_trackPageview']);
 
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
<script language="javascript" src="/Framework/UserInterface/Js/ajax_noDynamicLoad.js"></script>
<script language="javascript">
	var pageName = "<%=pageName%>";
	var useHttps = <%=prepInt(securePage)%>;
</script>
<script language="javascript" src="/template/js/base.js"></script>
