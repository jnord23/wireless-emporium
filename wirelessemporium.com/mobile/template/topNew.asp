<%
	response.buffer = true
	response.expires = now
	response.ExpiresAbsolute = now
	response.CacheControl = "private"
	
	if prepInt(securePage) = 1 then
		if request.ServerVariables("HTTPS") = "off" then
			response.redirect("https://" & request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL"))
		end if
	else
		if request.ServerVariables("HTTPS") = "on" then
			response.redirect("http://" & request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL"))
		end if
	end if
	
	mySession = prepInt(request.cookies("mySession"))
		
	sql = 	"select isnull(sum(a.qty), 0) totalQty " & vbcrlf & _
			"from shoppingcart a " & vbcrlf & _
			"where a.store = 0 and a.sessionid = '" & mySession & "' and (a.purchasedorderid is null or a.purchasedorderid = 0)"
	session("errorSQL") = sql
	set objRsMiniCart = oConn.execute(sql)
	
	if objRsMiniCart.EOF then
		miniTotalQuantity = 0 & " Items"
	else
		miniTotalQuantity = prepInt(objRsMiniCart("totalQty"))
		if miniTotalQuantity = 1 then
			miniTotalQuantity = miniTotalQuantity & " Item"
		else
			miniTotalQuantity = miniTotalQuantity & " Items"
		end if
	end if
%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html>
<head>
  <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
  <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="format-detection" content="telephone=no">
  <title>Wireless Emporium Mobile Site - <%=pageName%></title>
  <link href="/template/css/base.css" rel="stylesheet" type="text/css">
  <% if pageName = "Home" then %><link href="/template/css/home.css" rel="stylesheet" type="text/css"><% end if %>
  <% if pageName = "BMC" then %><link href="/template/css/bmc.css" rel="stylesheet" type="text/css"><% end if %>
  <% if pageName = "Product" then %><link href="/template/css/product.css" rel="stylesheet" type="text/css"><% end if %>
  <% if pageName = "Basket" then %><link href="/template/css/basket.css" rel="stylesheet" type="text/css"><% end if %>
  <% if instr(pageName,"Checkout") > 0 or pageName = "Confirm" then %><link href="/template/css/checkout.css" rel="stylesheet" type="text/css"><% end if %>
  <% if pageName = "Contact Us" then %><link href="/template/css/contact.css" rel="stylesheet" type="text/css"><% end if %>
</head>
<body onscroll="userScrolled()">
<div id="wholeSite">
    <div id="freeShipTop">
        <div id="freeText" style="padding:5px;">Fast, FREE Shipping <span style="color:#FFF;">|</span> Easy 90-Day Returns</div>
    </div>
    <div id="logoRow">
        <div id="logoContainer">
            <div id="weLogo"><a href="/"><img src="/images/mobile/mobileWeLogo.jpg" border="0" /></a></div>
            <div id="cartIcon" onclick="window.location='/basket.htm'">
                <img src="/images/mobile/cartIcon.gif" border="0" />
                <div id="inCart"><%=miniTotalQuantity%></div>
            </div>
        </div>
    </div>
    <div id="searchBar">
        <div id="searchContainer">
            <div id="orderByPhone">Order Now <a href="tel:<%=mobilePhoneNumber%>" id="csNumber"><%=mobilePhoneNumber%></a></div>
            <div id="searchButton"><a href="/search.htm"><img src="/images/mobile/weMobileSearchButton.jpg" border="0" /></a></div>
        </div>
    </div>
    <% if pageName = "Home" then %>
    <div id="adSpot" style="display:none;"><img src="/images/mobile/mobileAd.jpg" border="0" width="320" height="120" /></div>
    <% elseif instr(pageName,"Checkout") > 0 then %>
    <div id="breadCrumbs">
        <div id="breadCrumbText"><a href="/" id="breadCrumbLink">Home</a></div>
        <div id="breadCrumbBreak"><img src="/images/mobile/breadCrumbBreak.gif" border="0" width="15" height="40" /></div>
        <div id="breadCrumbText">
        	<div id="checkoutStepBox"><span style="font-weight:bold;">Checkout:</span> Step</div>
            <div id="checkoutStepImgBox">
            	<% if pageName = "Checkout1" then %><img src="/images/mobile/icons/step1.gif" border="0" /><% end if %>
                <% if pageName = "Checkout2" then %><img src="/images/mobile/icons/step2.gif" border="0" /><% end if %>
                <% if pageName = "Checkout3" then %><img src="/images/mobile/icons/step3.gif" border="0" /><% end if %>
            </div>
            <div id="checkoutStepBox">of</div>
            <div id="checkoutStepImgBox"><img src="/images/mobile/icons/of3.gif" border="0" /></div>
        </div>
        <div id="breadCrumbBreak"><img src="/images/mobile/breadCrumbBreak.gif" border="0" width="15" height="40" /></div>
    </div>
	<% else %>
    <div id="breadCrumbs">
        <div id="breadCrumbText"><a href="/" id="breadCrumbLink">Home</a></div>
        <div id="breadCrumbBreak"><img src="/images/mobile/breadCrumbBreak.gif" border="0" width="15" height="40" /></div>
        <div id="breadCrumbText"><a href="javascript:history.go(-1)" id="breadCrumbLink">Previous Page</a></div>
        <div id="breadCrumbBreak"><img src="/images/mobile/breadCrumbBreak.gif" border="0" width="15" height="40" /></div>
    </div>
    <% end if %>