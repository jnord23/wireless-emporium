<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "Contact Us"
	
	dim email : email = prepStr(request.Form("email"))
	dim fname : fname = prepStr(request.Form("fname"))
	dim lname : lname = prepStr(request.Form("lname"))
	dim phone : phone = prepStr(request.Form("phone"))
	dim subject : subject = prepStr(request.Form("subject"))
	dim product : product = prepStr(request.Form("product"))
	dim orderNumber : orderNumber = prepStr(request.Form("orderNumber"))
	dim question : question = prepStr(request.Form("question"))
	dim showThanks :  showThanks = 0
	
	if email <> "" then
		function CDOSend(strTo,strFrom,strSubject,strBody)
			on error resume next
			Dim objErrMail
			Set objErrMail = Server.CreateObject("CDO.Message")
			With objErrMail
				.From = strFrom
				.To = strTo
				.Subject = strSubject
				.HTMLBody = CStr("" & strBody)
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
				.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
				.Configuration.Fields.Item("urn:schemas:httpmail:sender") = "sales@wirelessemporium.com"
				.Configuration.Fields.Update
				.Send
			End With
			Set objErrMail = nothing
			on error goto 0
		end function
		
		dim customerName : customerName = fname & " " & lname
		dim cdo_to, cdo_from, cdo_subject, cdo_body
		select case subject 
			case "Returns / Exchanges / Request RMA" : cdo_to = "returns@WirelessEmporium.com"
			case "Marketing Inquiries" : cdo_to = "marketing@WirelessEmporium.com"
			case "General Inquiries" : cdo_to = "sales@WirelessEmporium.com"
			case else : cdo_to = "service@WirelessEmporium.com"
		end select
		
		cdo_to = "jnord23@hotmail.com"
		cdo_subject = "Contact Us"
		cdo_body = "<p><img src=""http://www.wirelessemporium.com/images/WElogo.gif"">" & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Name: </b>" & customerName & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Phone: </b>" & phone & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Type of Inquiry: </b><font color=""#FF0000"">" & subject & "</font></p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Order #: </b>" & orderNumber & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Product: </b>" & product & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Question: </b>" & replace(question,vbcrlf,"<br>") & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Date/Time Sent: </b>" & now & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>&nbsp;</p><p>" & Request.ServerVariables("REMOTE_ADDR") & "</p>" & vbcrlf
		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		showThanks = 1
	end if
%>
<!--#include virtual="/template/top.asp"-->
<div id="contactBackground">
	<% if showThanks = 1 then %>
    <div id="contactBox">
	    <p><b>Thank you for contacting WirelessEmporium.com!</b></p>
    	<p><b>A Representative from the appropriate department will contact you promptly. If your inquiry is sent between normal business hours (8am-5pm PST M-F) expect an answer in the next few hours or within one business day.</b></p>
	    <p><b>WirelessEmporium.com</b></p>
    </div>
    <% end if %>
    <div id="contactBox">
    	<div id="contactBaseData">
	        <div id="contactIcon"><img src="/images/mobile/icons/contactCall.gif" /></div>
    	    <div id="contactTextArea">
        		<div id="contactTextThin">Call Toll Free:</div>
            	<div id="contactTextThick"><a href="tel:<%=mobilePhoneNumber%>" id="freeText"><%=mobilePhoneNumber%></a></div>
	        </div>
        </div>
    </div>
    <form name="emailForm" method="post" action="/contact.asp" onsubmit="validateQuestionForm()">
    <div id="contactBox">
    	<div id="contactBaseData" style="margin-bottom:10px;" onclick="showQuestionForm()">
	        <div id="contactIcon"><img src="/images/mobile/icons/contactEmail.gif" /></div>
    	    <div id="contactTextArea">
        		<div id="contactTextThin">Email Us:</div>
            	<div id="contactTextThick">24 hours a day.</div>
	        </div>
        </div>
        <div id="emailFormDetails" style="display:none;">
            <div id="dataEntryRow">
                <div id="entryTitle">
                    <div id="entryTitleStar"><img src="/images/mobile/icons/requiredField.gif" border="0" /></div>
                    <div id="entryTitleText">Email:</div>
                </div>
                <div id="entryField"><input id="dataEntryInput" type="text" name="email" value="" onblur="checkEmail(this.value)" /></div>
            </div>
            <div id="dataEntryRow">
                <div id="entryTitle">
                    <div id="entryTitleStar"><img src="/images/mobile/icons/requiredField.gif" border="0" /></div>
                    <div id="entryTitleText">First Name:</div>
                </div>
                <div id="entryField"><input id="dataEntryInput" type="text" name="fname" value="" /></div>
            </div>
            <div id="dataEntryRow">
                <div id="entryTitle">
                    <div id="entryTitleStar"><img src="/images/mobile/icons/requiredField.gif" border="0" /></div>
                    <div id="entryTitleText">Last Name:</div>
                </div>
                <div id="entryField"><input id="dataEntryInput" type="text" name="lname" value="" /></div>
            </div>
            <div id="dataEntryRow">
                <div id="entryTitle">
                    <div id="entryTitleStar"><img src="/images/mobile/icons/requiredField.gif" border="0" /></div>
                    <div id="entryTitleText">Phone:</div>
                </div>
                <div id="entryField"><input id="dataEntryInput" type="text" name="phone" value="" /></div>
            </div>
            <div id="dataEntryRow">
                <div id="entryTitle">
                    <div id="entryTitleStar"><img src="/images/mobile/icons/requiredField.gif" border="0" /></div>
                    <div id="entryTitleText">Type of Inquiry:</div>
                </div>
                <div id="entryField">
                    <select name="subject" id="dataEntryInput">
                        <option value="" selected></option>
                        <option value="Sales Inquiries">Sales Inquiries</option>
                        <option value="Returns / Exchanges / Request RMA">Returns / Exchanges / Request RMA</option>
                        <option value="Billing Inquiries">Billing Inquiries</option>
                        <option value="Marketing Inquiries">Marketing Inquiries</option>
                        <option value="Other Customer Service Inquiries">Other Customer Service Inquiries</option>
                        <option value="General Inquiries">General Inquiries</option>
                    </select>
                </div>
            </div>
            <div id="dataEntryRow">
                <div id="entryTitle">
                    <div id="entryTitleStar"></div>
                    <div id="entryTitleText">Product:</div>
                </div>
                <div id="entryField"><input id="dataEntryInput" type="text" name="product" value="" /></div>
            </div>
            <div id="dataEntryRow">
                <div id="entryTitle">
                    <div id="entryTitleStar"></div>
                    <div id="entryTitleText">Order Number:</div>
                </div>
                <div id="entryField"><input id="dataEntryInput" type="text" name="orderNumber" value="" /></div>
            </div>
            <div id="dataEntryRowQuestion">
                <div id="entryTitle">
                    <div id="entryTitleStar"><img src="/images/mobile/icons/requiredField.gif" border="0" /></div>
                    <div id="entryTitleText">Question:</div>
                </div>
                <div id="entryArea"><textarea id="dataEntryArea" name="question" rows="4" cols="28"></textarea></div>
            </div>
            <div id="questionButton"><input type="image" src="/images/mobile/buttons/submitQuestion.gif" border="0" /></div>
        </div>
    </div>
    </form>
    <div id="contactBox">
    	<div id="contactBaseData">
	        <div id="contactIcon"><img src="/images/mobile/icons/contactHours.gif" /></div>
    	    <div id="contactTextArea">
        		<div id="contactTextThin">Hours of Operation:</div>
            	<div id="contactTextThick">M-F 8am-5pm (PST).</div>
	        </div>
        </div>
    </div>
</div>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">
	function showQuestionForm() {
		if (document.getElementById("emailFormDetails").style.display == '') {
			document.getElementById("emailFormDetails").style.display = 'none'
		}
		else {
			document.getElementById("emailFormDetails").style.display = ''
		}
	}
	function validateQuestionForm() {
		
	}
</script>