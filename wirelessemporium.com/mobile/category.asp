<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "Home"
	
	dim categoryName : categoryName = prepStr(request.QueryString("categoryName"))
%>
<!--#include virtual="/template/top.asp"-->
<%	
	sql = "select * from we_types where SOUNDEX(typeName_WE) = SOUNDEX('" & categoryName & "')"
	session("errorSQL") = sql
	set catRS = oConn.execute(sql)
	
	if not catRS.EOF then
		categoryID = catRS("typeID")
		categoryName = catRS("typeName_WE")
	end if
	
	sql = "select brandID, brandName from we_brands where phoneSale = 1 and other = 0 order by brandName"
	session("errorSQL") = sql
	set brandRS = oConn.execute(sql)
%>
<div class="tb mCenter" style="background-color:#fff;">
	<div class="fl catImg"><img src="/images/mobile/categories/<%=categoryID%>.jpg" border="0" /></div>
    <div class="fl catDetails">
    	<div class="curView">Currently Viewing:</div>
        <div class="curCat"><%=categoryName%></div>
        <div>
        	<a href="/?reset=1" style="text-decoration:none;">
            <div class="tb" id="changeBrandBox">
                <div id="redX"></div>
                <div id="tapToChange">Tap to change category.</div>
            </div>
            </a>
        </div>
    </div>
</div>
<div class="selectByBrand">
    <div class="CallToAction">SELECT A DEVICE BRAND BELOW:</div>
    <div class="ctaDownArrow"></div>
    <div class="allBrandImgs">
    	<%
		do while not brandRS.EOF
			brandID = brandRS("brandID")
			brandName = brandRS("brandName")
			brandName = replace(brandName,"/Palm","")
			brandName = replace(brandName,"/Qualcomm","")
			brandName = replace(brandName," Ericsson","")
		%>
        <div class="fl brandButton" onclick="window.location='/sc-<%=categoryID%>-sb-<%=brandID%>-<%=formatSEO(brandName)%>-<%=formatSEO(categoryName)%>.htm'">
        	<div class="brandImg"><img src="/images/mobile/homeBrands/<%=lcase(brandName)%>.png" /></div>
            <div class="brandText"><a class="brandLink" href="/sc-<%=categoryID%>-sb-<%=brandID%>-<%=formatSEO(brandName)%>-<%=formatSEO(categoryName)%>.htm"><%=brandName%></a></div>
        </div>
        <%
			brandRS.movenext
		loop
		brandRS.movefirst
		%>
        <div class="fl brandButton" onclick="window.location='/other-cell-phone-accessories.htm'">
        	<div class="brandImg"><img src="/images/mobile/homeBrands/others.png" /></div>
            <div class="brandText"><a class="brandLink" href="/other-cell-phone-accessories.htm">Others</a></div>
        </div>
    </div>
</div>
<script>
	window.WEDATA.pageType = 'category';
	window.WEDATA.categoryData = {
		categoryId: <%=categoryid %>,
		categoryName: '<%=nameSEO(categoryName)%>'
	};
</script>

<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">
	var mobileBrandID = <%=prepInt(request.Cookies("mobileBrandID"))%>
	var mobileModelID = <%=prepInt(request.Cookies("mobileModelID"))%>
</script>
<script language="javascript" src="/template/js/index.js"></script>