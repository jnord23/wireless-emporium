<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	dim totalEntries : totalEntries = 0
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	
	if fso.fileExists(server.MapPath("/sitemap.xml")) then
		fso.deleteFile(server.MapPath("/sitemap.xml"))
	end if
	
	set tfile = fso.createTextFile(server.MapPath("/sitemap.xml"))
	tfile.writeLine("<?xml version=""1.0"" encoding=""UTF-8"" ?>")
	tfile.writeLine("<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:mobile=""http://www.google.com/schemas/sitemap-mobile/1.0"">")
	
	tfile.writeLine("<url>")
	tfile.writeLine("	<loc>http://m.wirelessemporium.com/</loc>")
	tfile.writeLine("	<mobile:mobile/>")
	tfile.writeLine("	<changefreq>always</changefreq>")
	tfile.writeLine("	<priority>1.0</priority>")
	tfile.writeLine("</url>")
	
	totalEntries = 1
	
	sql = "select brandName from we_brands where phoneSale = 1"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	do while not rs.EOF
		tfile.writeLine("<url>")
        tfile.writeLine("	<loc>http://m.wirelessemporium.com/" & formatSEO(rs("brandName")) & "-cell-phone-accessories.htm</loc>")
        tfile.writeLine("	<mobile:mobile/>")
		tfile.writeLine("	<changefreq>always</changefreq>")
		tfile.writeLine("	<priority>1.0</priority>")
    	tfile.writeLine("</url>")
		rs.movenext
		totalEntries = totalEntries + 1
	loop
	
	sql = "select a.modelID, a.modelName, b.brandID, b.brandName from we_models a left join we_brands b on a.brandID = b.brandID where mobileReady = 1"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	sql = "select typeID, typeNameSEO_WE as typeName, subtypeid, subTypeNameSEO_WE from v_subtypeMatrix where typeID not in (4,8,9,12,15,16,20,22,23,24,25) order by typeNameSEO_WE"
	session("errorSQL") = sql
	set typeRS = oConn.execute(sql)
	
	typeList = ""
	subTypeList = ""
	do while not typeRS.EOF
		catID = typeRS("typeID")
		catName = typeRS("typeName")
		
		typeList = typeList & catID & "##" & catName & "@@"
		do while catName = typeRS("typeName")
			subCatID = typeRS("subtypeid")
			subCatName = typeRS("subTypeNameSEO_WE")
			subTypeList = subTypeList & subCatID & "##" & subCatName & "@@"
			typeRS.movenext
			if typeRS.EOF then exit do
		loop
	loop
	typeList = left(typeList,len(typeList)-2)
	typeArray = split(typeList,"@@")
	subTypeList = left(subTypeList,len(subTypeList)-2)
	subTypeArray = split(subTypeList,"@@")
	
	do while not rs.EOF
		modelID = rs("modelID")
		modelName = rs("modelName")
		brandID = rs("brandID")
		brandName = rs("brandName")
		
		tfile.writeLine("<url>")
        tfile.writeLine("	<loc>http://m.wirelessemporium.com/accessories-for-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".htm</loc>")
        tfile.writeLine("	<mobile:mobile/>")
		tfile.writeLine("	<changefreq>always</changefreq>")
		tfile.writeLine("	<priority>1.0</priority>")
    	tfile.writeLine("</url>")
		totalEntries = totalEntries + 1
		
		for i = 0 to ubound(typeArray)
			curType = split(typeArray(i),"##")
			
			tfile.writeLine("<url>")
			tfile.writeLine("	<loc>http://m.wirelessemporium.com/sb-" & brandID & "-sm-" & modelID & "-sc-" & curType(0) & "-" & formatSEO(curType(1)) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".htm</loc>")
			tfile.writeLine("	<mobile:mobile/>")
			tfile.writeLine("	<changefreq>always</changefreq>")
			tfile.writeLine("	<priority>1.0</priority>")
			tfile.writeLine("</url>")
			totalEntries = totalEntries + 1
		next
		
		for i = 0 to ubound(subTypeArray)
			curType = split(subTypeArray(i),"##")
			
			tfile.writeLine("<url>")
			tfile.writeLine("	<loc>http://m.wirelessemporium.com/sb-" & brandID & "-sm-" & modelID & "-scd-" & curType(0) & "-" & formatSEO(curType(1)) & "-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".htm</loc>")
			tfile.writeLine("	<mobile:mobile/>")
			tfile.writeLine("	<changefreq>always</changefreq>")
			tfile.writeLine("	<priority>1.0</priority>")
			tfile.writeLine("</url>")
			totalEntries = totalEntries + 1
		next
		
		sql = "select itemID, itemDesc from we_Items where modelID = " & modelID & " and hideLive = 0 and typeID not in (4,8,9,12,15,16,20,22,23,24,25)"
		session("errorSQL") = sql
		set prodRS = oConn.execute(sql)
		
		do while not prodRS.EOF
			tfile.writeLine("<url>")
			tfile.writeLine("	<loc>http://m.wirelessemporium.com/p-" & prodRS("itemID") & "-" & formatSEO(prodRS("itemDesc")) & ".htm</loc>")
			tfile.writeLine("	<mobile:mobile/>")
			tfile.writeLine("	<changefreq>always</changefreq>")
			tfile.writeLine("	<priority>1.0</priority>")
			tfile.writeLine("</url>")
			prodRS.movenext
			totalEntries = totalEntries + 1
		loop
		
		rs.movenext
	loop
	
	tfile.writeLine("</urlset>")
	tfile.close	
	tfile = null
	response.Write("XML file completed: " & totalEntries)
%>