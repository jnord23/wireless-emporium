<%
	response.buffer = true
	response.expires = -1
	server.ScriptTimeout = 30000
%>
<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/asp/inc_Compatibility.asp"-->
<%
	dim thisUser, pageTitle, header
	thisUser = Request.Cookies("admin")("username")
	baseCrumb = "<a style='font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?r=1','mobileContent',1)>Main Menu&nbsp;&gt;</a>"
	
	sql = "insert into we_mobileTrackingPages (sessionID,page) values('" & request.Cookies("mySession") & "','" & request.ServerVariables("URL") & "?" & request.ServerVariables("QUERY_STRING") & "')"
	session("errorSQL") = sql
	oConn.execute(sql)

	productID = prepStr(request.QueryString("p"))
	productType = prepStr(request.QueryString("t"))
	menuOption = prepStr(request.QueryString("mm"))
	refresh = prepStr(request.QueryString("r"))
	itemID = prepStr(request.QueryString("i"))
	itemDetails = prepStr(request.QueryString("d"))
	accessoryType = prepStr(request.QueryString("at"))
	brandID = prepStr(request.QueryString("b"))
	brandName = prepStr(request.QueryString("bn"))
	search = prepStr(request.QueryString("search"))
	carrierCode = prepStr(request.QueryString("c"))
	carrierName = prepStr(request.QueryString("cn"))
	preType = prepStr(request.QueryString("preType"))
	login = prepStr(request.QueryString("login"))
	email = prepStr(request.QueryString("email"))
	backBttn = prepStr(request.QueryString("backBttn"))
	zoom = prepStr(request.QueryString("z"))
	moreProduct = prepStr(request.QueryString("mp"))
	msGenre = prepStr(request.QueryString("msGenre"))
	msArtist = prepStr(request.QueryString("msArtist"))
	artistPage = prepStr(request.QueryString("artistPage"))
	musicSkinItem = prepStr(request.QueryString("musicSkinItem"))
	sampleImg = prepStr(request.QueryString("sampleImg"))
	contactUs = prepStr(request.QueryString("contactUs"))
	sortBy = prepStr(request.QueryString("sb"))
	remindItemID = prepStr(request.QueryString("remindItemID"))
	
	if isnull(productID) or len(productID) < 1 then productID = 0
	if isnull(productType) or len(productType) < 1 then productType = 0
	if isnull(menuOption) or len(menuOption) < 1 then menuOption = 0
	if isnull(refresh) or len(refresh) < 1 then refresh = 0
	if isnull(itemID) or len(itemID) < 1 then itemID = 0
	if isnull(itemDetails) or len(itemDetails) < 1 then itemDetails = 0
	if isnull(accessoryType) or len(accessoryType) < 1 then accessoryType = ""
	if isnull(brandID) or len(brandID) < 1 then brandID = 0
	if isnull(brandName) or len(brandName) < 1 then brandName = ""
	if isnull(search) or len(search) < 1 then search = ""
	if isnull(carrierCode) or len(carrierCode) < 1 then carrierCode = ""
	if isnull(carrierName) or len(carrierName) < 1 then carrierName = ""
	if isnull(preType) or len(preType) < 1 then preType = 0
	if isnull(login) or len(login) < 1 then login = 0
	if isnull(email) or len(email) < 1 then email = ""
	if isnull(backBttn) or len(backBttn) < 1 then backBttn = 0
	if isnull(zoom) or len(zoom) < 1 then zoom = ""
	if isnull(moreProduct) or len(moreProduct) < 1 then moreProduct = 0
	if isnull(msGenre) or len(msGenre) < 1 then msGenre = ""
	if isnull(msArtist) or len(msArtist) < 1 then msArtist = ""
	if isnull(artistPage) or len(artistPage) < 1 then artistPage = 1
	if isnull(musicSkinItem) or len(musicSkinItem) < 1 then musicSkinItem = 0
	if isnull(sampleImg) or len(sampleImg) < 1 then sampleImg = ""
	if isnull(contactUs) or len(contactUs) < 1 then contactUs = 0
	if isnull(sortBy) or len(sortBy) < 1 then sortBy = "9 desc"
	if isnull(remindItemID) or len(remindItemID) < 1 then remindItemID = 0
	
	if productType > 0 and productID = 0 then 
		call CloseConn(oConn)
		response.Redirect("/ajax/mobileContent.asp?mm=1&preType=" & productType)
	end if
	productDetails = 0
	showProducts = 0
	
	if msGenre = "R" then msGenre = "R&B"
	if moreProduct = 1 then prodsToShow = 101 else prodsToShow = 11
	
	if backBttn = 0 then
		curPage = "/ajax/mobileContent.asp?msGenre=" & msGenre & "&msArtist=" & msArtist & "&p=" & productID & "&t=" & productType & "&mm=" & menuOption & "&r=" & refresh & "&i=" & itemID & "&d=" & itemDetails & "&at=" & accessoryType & "&b=" & brandID & "&bn=" & brandName & "&search=" & search & "&c=" & carrierCode & "&cn=" & carrierName & "&preType=" & preType & "&login=" & login & "&email=" & email
		if session("prevPage2") <> session("prevPage1") then session("prevPage3") = replace(session("prevPage2"),"&backBttn=1","")
		if session("prevPage1") <> session("curPage") then session("prevPage2") = replace(session("prevPage1"),"&backBttn=1","")
		if session("curPage") <> curPage then session("prevPage1") = replace(session("curPage"),"&backBttn=1","")
		session("curPage") = replace(curPage,"&backBttn=1","")
	else
		session("curPage") = replace(session("curPage"),"&backBttn=1","")
		session("prevPage1") = replace(session("prevPage1"),"&backBttn=1","")
		session("prevPage2") = replace(session("prevPage2"),"&backBttn=1","")
		session("prevPage3") = replace(session("prevPage3"),"&backBttn=1","")
	end if
	
	if zoom <> "" then
		set fso = CreateObject("Scripting.FileSystemObject")
		if musicSkinItem = 1 then
			if fso.fileExists(server.MapPath("/productpics/musicSkins/musicSkinsLarge/" & zoom)) then
				useImg = "/productpics/musicSkins/musicSkinsLarge/" & zoom
			else
				useImg = "/productpics/musicSkins/musicSkinsDefault/" & zoom
			end if
		else
			useImg = "/productPics/big/" & zoom
		end if
%>
<img src="<%=useImg%>" border="0" />
<%
		if sampleImg <> "" then
%>
<br /><img src="/productpics/musicSkins/musicSkinsSample/<%=sampleImg%>" border="0" />
<%
		end if
		call CloseConn(oConn)
		response.End()
	elseif remindItemID > 0 then
		sql = "select itemDesc, price_our from we_items where itemID = " & remindItemID
		session("errorSQL")
		set rs = oConn.execute(sql)
		
		itemDesc = rs("itemDesc")
		price_our = rs("price_our")
		
		rs = null
%>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
    	<td bgcolor="#ebebeb" style="padding:10px;" align="left">
        	<div style="font-weight:bold; font-size:18px;">Remind Me Later</div>
            <div style="font-size:18px; color:#666; margin-top:10px;"><%=itemDesc%></div>
            <div style="font-weight:bold; font-size:16px; color:#F00; margin-top:10px;"><%=formatCurrency(price_our,2)%></div>
        </td>
    </tr>
    <tr>
    	<td style="padding:10px;" align="left">
        	<form name="remindForm" style="margin:0px;" method="post" action="/?i=<%=remindItemID%>">
        	<div style="font-size:18px;">Enter Email Address:</div>
            <div style="margin-top:5px;"><input type="text" name="email" value="" size="42" style="font-size:14px;" /></div>
            <div style="margin-top:5px; font-size:11px; color:#666;">(e.g. jdoe@aol.com)</div>
            <div style="margin-top:5px; font-size:18px;">Type a Message:</div>
            <div style="margin-top:5px;"><textarea name="message" rows="4" cols="34" style="font-size:14px;">A reminder from Wireless Emporium to review a product</textarea></div>
            <div style="margin-top:10px; float:left; width:300px;">
            	<div style="float:left; cursor:pointer;" onclick="ajax('/ajax/mobileContent.asp?i=<%=remindItemID%>','mobileContent',1)"><img src="/images/mobile/buttons/back.png" border="0" /></div>
                <div style="float:right; cursor:pointer;" onclick="document.remindForm.submit()"><img src="/images/mobile/buttons/send.png" border="0" /></div>
            </div>
            </form>
        </td>
    </tr>
</table>
<%
		call CloseConn(oConn)
		response.End()
	elseif login = 1 then
%>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr bgcolor="#ebebeb"><td align="left" style="color:#000000; font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%>&nbsp;Login</td></tr>
    <tr>
    	<td align="center" style="padding-top:10px;">
        	<form name="frmLogin" method="post" action="/mobile/">
            <table border="0" cellpadding="0" cellpadding="0">
            	<tr>
                	<td style="font-weight:bold;" align="left">Email:</td>
                    <td><input type="text" name="login" value="<%=email%>" size="23" /></td>
                </tr>
                <tr>
                	<td style="font-weight:bold;" align="left">Password:</td>
                    <td><input type="password" name="loginPW" value="" size="23" /></td>
                </tr>
                <tr>
                	<td colspan="2">
                    	<table border="0" cellpadding="0" cellpadding="0">
                            <tr>
                                <td align="left"><a href="#" style="color:ff6633;">*Forgot your password?</a></td>
                                <td align="right" style="padding-left:5px;"><input type="image" onClick="frmLogin.submit();" src="/images/register/button_login.jpg" border="0" alt="Login Now" width="122" height="22"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr><td><hr color="ff6633" /></td>
    <tr><td align="left" style="font-size:14px; font-weight:bold; color:#000000; padding-left:20px;">Create New Account</td>
    <tr><td><hr color="ff6633" /></td>
    <tr><td align="center" style="padding-bottom:10px; padding-top:10px;"><a href="/register/default.asp?basket="><img src="/images/register/button_account.jpg" border="0" alt="Create Account" width="122" height="22"></a></td></tr>
</table>
<%
		call CloseConn(oConn)
		response.End()
	elseif contactUs = 1 then
%>
<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%" style="padding:0px 10px 0px 10px;">
    <tr>
        <td align="left"><h1>Contact Us</h1></td>
    </tr>
    <tr>
        <td align="center">
        	<form onSubmit="return validateContactUs();" name="contactus" method="post" action="/?r=1&saveContactUs=1">
        	<div style="text-align:left; margin-bottom:20px;">
            	Have a question or comment about anything? Our Associates are standing by to assist.
                Please contact us by providing the pertinent information below and your inquiry will be
                addressed within one business day or less.
            </div>
            <div style="float:left; width:100%; margin-bottom:10px;">
            	<div style="float:left; width:100px; text-align:right; margin-right:10px;">Name:<span style='color:#FF0000;'>*</span></div>
                <div style="float:left;"><input type="text" name="name" size="20" maxlength="100" value=""></div>
            </div>
            <div style="float:left; width:100%; margin-bottom:10px;">
            	<div style="float:left; width:100px; text-align:right; margin-right:10px;">Email:<span style='color:#FF0000;'>*</span></div>
                <div style="float:left;"><input type="text" name="email" size="20" maxlength="100" value=""></div>
            </div>
            <div style="float:left; width:100%; margin-bottom:10px;">
            	<div style="float:left; width:100px; text-align:right; margin-right:10px;">Type of Inquiry:<span style='color:#FF0000;'>*</span></div>
                <div style="float:left;">
                	<select name="subject">
                        <option value="" selected>Select here...</option>
                        <option value="Sales Inquiries">Sales Inquiries</option>
                        <option value="Returns / Exchanges / Request RMA">Returns / Exchanges / Request RMA</option>
                        <option value="Billing Inquiries">Billing Inquiries</option>
                        <option value="Marketing Inquiries">Marketing Inquiries</option>
                        <option value="Other Customer Service Inquiries">Other Customer Service Inquiries</option>
                        <option value="General Inquiries">General Inquiries</option>
                    </select>
                </div>
            </div>
            <div style="float:left; width:100%; margin-bottom:10px;">
            	<div style="float:left; width:100px; text-align:right; margin-right:10px;">Phone #:</div>
                <div style="float:left;"><input type="text" name="phonenumber" size="20" maxlength="100" value=""></div>
            </div>
            <div style="float:left; width:100%; margin-bottom:10px;">
            	<div style="float:left; width:100px; text-align:right; margin-right:10px;">Product:</div>
                <div style="float:left;"><input type="text" name="product" size="20" maxlength="100" value=""></div>
            </div>
            <div style="float:left; width:100%; margin-bottom:10px;">
            	<div style="float:left; width:100px; text-align:right; margin-right:10px;">Order #:</div>
                <div style="float:left;"><input type="text" name="orderno" size="20" maxlength="20" value=""></div>
            </div>
            <div style="float:left; width:100%; margin-bottom:10px;">
            	<div style="float:left; width:100px; text-align:right; margin-right:10px;">Question:<span style='color:#FF0000;'>*</span></div>
                <div style="float:left;"><textarea name="question" cols="25" rows="4"></textarea></div>
            </div>
            <div>
            	<input type="submit" name="submit" value="Click Here To Send">
                <input type="hidden" name="submitted" value="1">
            </div>
            <div style="margin:10px 0px 10px 0px;">
            	To ensure delivery of our emails to your inbox, please add<br />service@WirelessEmporium.com to your Address Book. Thank You.
            </div>
            </form>
        </td>
    </tr>
</table>
<%
		call CloseConn(oConn)
		response.End()
	end if
	
	if search <> "" then
		sql = "select top 10 itemID, itemDesc, typeID from we_items where itemDesc like '%" & search & "%' or itemID like '%" & search & "%' or partnumber like '%" & search & "%' order by numberOfSales desc"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 3, 3
%>
<table border="0" cellpadding="3" cellpadding="0">
<%
		do while not rs.EOF
%>
	<tr><td align="left" onclick="document.getElementById('nxt-ac-container').innerHTML='';ajax('/ajax/mobileContent.asp?i=<%=rs("itemID")%>&t=<%=rs("typeID")%>','mobileContent',1)"><%=rs("itemDesc")%></td></tr>
<%
			rs.movenext
		loop
%>
</table>
<%
		call CloseConn(oConn)
		response.End()
	end if
	
	if refresh = 1 then
		userBrowser = request.ServerVariables("HTTP_USER_AGENT")
		session.Contents.Remove("crumbLvl1")
		session.Contents.Remove("crumbLvl2")
		session.Contents.Remove("crumbLvl3")
		session.Contents.Remove("crumbLvl4")
		
		itemID = 0

		if itemID = 0 then
			sql = "select a.modelID, b.modelName from we_mobileBrowsers a join we_models b on a.modelID = b.modelID where a.userBrowser = '" & userBrowser & "'"
			session("errorSQL") = sql
			Set phoneIdRS = Server.CreateObject("ADODB.Recordset")
			phoneIdRS.open sql, oConn, 3, 3
			
			if phoneIdRS.EOF then
				'nothing yet
			else
				if isnull(phoneIdRS("modelID")) then
					if instr(userBrowser,"BlackBerry") > 0 then
						browserID = replace(userBrowser,"BlackBerry","")
						browserID = left(browserID,instr(browserID,"/") - 1)
						sql = "execute sp_updateMobileBrowser 14,'" & browserID & "','" & userBrowser & "'"
						session("errorSQL") = sql
						Set phoneIdRS = Server.CreateObject("ADODB.Recordset")
						phoneIdRS.open sql, oConn, 3, 3
						if not isnull(phoneIdRS("modelID")) then
							itemID = phoneIdRS("modelID")
						end if
						modelName = "BlackBerry " & browserID
					elseif instr(userBrowser,"Android") > 0 then
						browserArray = split(userBrowser,";")
						browserID = trim(left(browserArray(4),instr(browserArray(4),"Build") - 1))
						sql = "execute sp_updateMobileBrowser 0,'" & browserID & "','" & userBrowser & "'"
						session("errorSQL") = sql
						Set phoneIdRS = Server.CreateObject("ADODB.Recordset")
						phoneIdRS.open sql, oConn, 3, 3
						if not isnull(phoneIdRS("modelID")) then
							itemID = phoneIdRS("modelID")
							modelName = phoneIdRS("modelName")
						end if
					end if
				else
					itemID = phoneIdRS("modelID")
					modelName = phoneIdRS("modelName")
				end if
			end if
		end if
		
		sql = "select * from we_models where modelID = " & itemID
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 3, 3
%>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <% if itemID > 0 then %>
    <tr onclick="ajax('/ajax/mobileContent.asp?p=<%=itemID%>','mobileContent',1)">
        <td align="right" style="padding-top:10px; padding-left:20px;"><img src="/images/mobile/buttons/we_m_button_small.png" border="0" /></td>
        <td width="100%" align="left" style="padding-top:10px;">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr><td align="left" style="font-size:18px; color:#ff6633; padding-left:20px;"><%=modelName%></td></tr>
                <tr><td align="left" style="color:#666; font-size:10px; padding-left:20px;">Accessories For Your Phone</td></tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="2" style="border-bottom:1px solid #cccccc;"></td></tr>
    <% end if %>
    <tr onclick="ajax('/ajax/mobileContent.asp?mm=1','mobileContent',1)">
        <td align="right" style="padding-top:10px;"><img src="/images/mobile/buttons/we_m_button_small.png" border="0" /></td>
        <td align="left" style="padding-top:10px;">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr><td align="left" style="font-size:18px; color:#ff6633; padding-left:20px;">BRANDS</td></tr>
                <tr><td align="left" style="color:#666; font-size:10px; padding-left:20px;">Apple | BlackBerry | HTC | more &gt;&gt;</td></tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="2" style="border-bottom:1px solid #cccccc;"></td></tr>
    <tr onclick="ajax('/ajax/mobileContent.asp?mm=2','mobileContent',1)">
        <td align="right" style="padding-top:10px;"><img src="/images/mobile/buttons/we_m_button_small.png" border="0" /></td>
        <td align="left" style="padding-top:10px;">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr><td align="left" style="font-size:18px; color:#ff6633; padding-left:20px;">CARRIERS</td></tr>
                <tr><td align="left" style="color:#666; font-size:10px; padding-left:20px;">AT&amp;T | Sprint | T-Mobile | Verizon | more &gt;&gt;</td></tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="2" style="border-bottom:1px solid #cccccc;"></td></tr>
    <tr onclick="ajax('/ajax/mobileContent.asp?mm=3','mobileContent',1)">
        <td align="right" style="padding-top:10px;"><img src="/images/mobile/buttons/we_m_button_small.png" border="0" /></td>
        <td align="left" style="padding-top:10px;">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr><td align="left" style="font-size:18px; color:#ff6633; padding-left:20px;">ACCESSORY TYPE</td></tr>
                <tr><td align="left" style="color:#666; font-size:10px; padding-left:20px;">Faceplates | Chargers | Cases | more &gt;&gt;</td></tr>
            </table>
        </td>
    </tr>
</table>
<%
		call CloseConn(oConn)
		response.End()
	end if
	
	if itemID > 0 then
		'showing a single item for purchase
		sql = "select a.*, (select inv_qty from we_items where partNumber = a.partNumber and master = 1) as masterQty from we_items a where a.itemID = " & itemID
	elseif productType > 0 and productID > 0 then
		'Show one type of accessory for a specific phone
		showProducts = 1
		if productType = 5 then
			sql = "select top " & prodsToShow & " a.*, b.modelName, b.modelImg, 'Hands-Free' as typeName from we_items a left join we_models b on a.modelID = b.modelID left join we_items d on a.partNumber = d.partNumber and d.master = 1 where a.HandsfreeType in ('2','3') and a.typeid = '5' and a.hideLive = 0 and a.price_Our is not null and a.price_Our > 0 and d.inv_qty > 0 order by a.numberofsales desc"
		elseif productType = 14 then
			sql = "select top " & prodsToShow & " a.*, b.modelName, b.modelImg, 'Bling Kits & Charms' as typeName from we_items a left join we_models b on a.modelID = b.modelID left join we_items d on a.partNumber = d.partNumber and d.master = 1 where a.typeid = '14' and a.hideLive = 0 and a.price_Our is not null and a.price_Our > 0 and d.inv_qty > 0 order by a.numberofsales desc"
		elseif productType = 20 then
			if msGenre = "" then
				sql = "select distinct genre from we_items_musicSkins where genre is not null and genre not like '%,%' and genre not like 'Screen Protectors' and modelID = " & productID & " order by genre"
			elseif msArtist = "" then
				'sql = "select distinct top 10 artist, (select top 1 image + '##' + defaultImg from we_items_musicSkins where artist = a.artist) as artistImg from we_items_musicSkins a where genre like '%" & msGenre & "%' and modelID = " & productID & " order by artist"
				sql = "select distinct artist, image, defaultImg from we_items_musicSkins where genre like '%" & msGenre & "%' and modelID = " & productID & " and artist is not null order by artist"
			else
				sql = "select a.*, b.modelName, b.modelImg, c.typeName from we_items_musicSkins a left join we_models b on a.modelID = b.modelID left join we_types c on 20 = c.typeID where a.modelID = " & productID & " and a.price_WE is not null and a.price_WE > 0 and artist = '" & msArtist & "' order by a.artist, a.designName"
			end if
		else
			if prodsToShow = 11 then
				sql = 	"select * from (select top " & prodsToShow & " a.itemID,a.itemPic,a.itemDesc,a.price_retail,a.price_our, b.modelName, b.modelImg, c.typeName, " &_
						"a.numberofsales from we_items a left join we_models b on a.modelID = b.modelID left join we_types c on a.typeID = c.typeID left join we_items d " &_
						"on a.partNumber = d.partNumber and d.master = 1 where a.modelID = " & productID & " and a.typeID = " & productType & " and a.hideLive = 0 and " &_
						"d.inv_qty > 0 and a.price_Our is not null and a.price_Our > 0 order by " & sortBy & ") as x"
				sql = sql & " union "
				sql = sql & "select * from (select top " & prodsToShow & " b.itemID,b.itemPic,b.itemDesc,b.price_retail,b.price_our, c.modelName, c.modelImg, " &_
							"d.typeName, b.numberofsales from we_relatedItems a left join we_items b on a.itemID = b.itemID left join we_models c on a.modelID = c.modelID " &_
							"left join we_types d on a.typeID = d.typeID left join we_items e on b.partNumber = e.partNumber and e.master = 1 where " &_
							"a.modelID = " & productID & " and a.typeID = " & productType & " and b.hideLive = 0 and e.inv_qty > 0 and b.price_Our is not null and " &_
							"b.price_Our > 0 order by " & sortBy & ") as y " &_
							"order by " & sortBy
			else
				sql = 	"select a.itemID,a.itemPic,a.itemDesc,a.price_retail,a.price_our, b.modelName, b.modelImg, c.typeName, " &_
						"a.numberofsales from we_items a left join we_models b on a.modelID = b.modelID left join we_types c on a.typeID = c.typeID left join we_items d " &_
						"on a.partNumber = d.partNumber and d.master = 1 where a.modelID = " & productID & " and a.typeID = " & productType & " and a.hideLive = 0 and " &_
						"d.inv_qty > 0 and a.price_Our is not null and a.price_Our > 0"
				sql = sql & " union "
				sql = sql & "select b.itemID,b.itemPic,b.itemDesc,b.price_retail,b.price_our, c.modelName, c.modelImg, " &_
							"d.typeName, b.numberofsales from we_relatedItems a left join we_items b on a.itemID = b.itemID left join we_models c on a.modelID = c.modelID " &_
							"left join we_types d on a.typeID = d.typeID left join we_items e on b.partNumber = e.partNumber and e.master = 1 where " &_
							"a.modelID = " & productID & " and a.typeID = " & productType & " and b.hideLive = 0 and e.inv_qty > 0 and b.price_Our is not null and " &_
							"b.price_Our > 0 " &_
							"order by " & sortBy
			end if
			'sql = "select top " & prodsToShow & " a.*, b.modelName, b.modelImg, c.typeName from we_items a left join we_models b on a.modelID = b.modelID left join we_types c on a.typeID = c.typeID left join we_items d on a.partNumber = d.partNumber and d.master = 1 where a.modelID = " & productID & " and a.typeID = " & productType & " and a.hideLive = 0 and d.inv_qty > 0 and a.price_Our is not null and a.price_Our > 0 order by a.numberofsales desc"
		end if
	elseif productID > 0 then
		'Show types of available accessories for a specific phone
		productDetails = 1
		sql = "select distinct (select count(*) from we_items_musicSkins where modelID = c.modelID) as musicSkins, a.typeID, b.typeName, b.typeImg, c.modelID, c.modelName, c.modelImg, d.brandName, a.brandID from we_items a left join we_types b on a.typeID = b.typeID left join we_models c on a.modelID = c.modelID left join we_brands d on a.brandID = d.brandID where b.typeID <> 5 and c.modelID = " & productID
	elseif menuOption = 1 then
		'Show all the brands supported on the site
		sql = "select brandID, brandName, brandImg from we_brands where brandType < 2 order by brandName"
	elseif menuOption = 2 then
		'Show all the carriers supported on the site
		sql = "select carrierCode, carrierName, carrierImg from we_carriers where id <> 12 order by carrierName"
	elseif menuOption = 3 then
		'Show all the accessory types supported on the site
		sql = "select typeID, typeName, typeImg from we_types where typeID <> 15 and typeID <> 9 and typeID <> 21 order by typeName"
	elseif carrierCode <> "" then
		'Show all the phone models supported by a specific carrier
		sql = "select top 21 a.modelID, a.modelName, a.modelImg, sum(b.numberOfSales) as qtySold from we_models a join we_items b on a.modelID = b.modelID where b.hideLive = 0 and a.carrierCode like '%" & carrierCode & ",%' group by a.modelID, a.modelName, a.modelImg order by qtySold desc"
	else
		'Show all the phone models supported by a specific brand
		'sql = "select distinct a.modelID, a.modelName, a.modelImg from we_models a join we_items b on a.modelID = b.modelID where b.hideLive = 0 and a.brandID = " & brandID & " order by a.modelID desc"
		sql = "select c.modelID as modelID, c.modelName, d.modelImg, c.sumQty from temp_TopModels c join we_models d on c.modelID = d.modelID where c.brandID = " & brandID & " and c.store = 0 union select distinct a.modelID as modelID, a.modelName, a.modelImg, '0' as sumQty from we_models a join we_items b on a.modelID = b.modelID where a.modelName not in (select modelName from temp_TopModels where brandID = " & brandID & " and store = 0) and b.hideLive = 0 and a.brandID = " & brandID & " order by c.modelName, modelID desc"
	end if
	sql = replace(sql,"top 101","")
	
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	if itemID > 0 and rs.EOF then
		musicSkinItem = 1
		sql = "select b.brandName, c.modelName, a.* from we_items_musicSkins a left join we_brands b on a.brandID = b.brandID left join we_models c on a.modelID = c.modelID where id = " & itemID
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
	end if
	
	if itemDetails > 0 then
		if itemDetails = 1 then response.Write(rs("itemLongDetail"))
		if itemDetails = 2 then
%>
<ul>
	<%
	for i = 1 to 10
		if not isnull(rs("bullet" & i)) then
			if len(rs("bullet" & i)) > 0 then
	%>
	<li style="line-height:20px;"><%=rs("bullet" & i)%></li>
    <%
			end if
		end if
	next
	%>
</ul>
<%
		end if
		if itemDetails = 3 then response.Write(CompatibilityList(rs("PartNumber"),"display"))
		call CloseConn(oConn)
		response.End()
	end if
%>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family:Arial, Helvetica, sans-serif;">
	<%
	if itemID > 0 then
		if musicSkinItem = 1 then
			set fso = CreateObject("Scripting.FileSystemObject")
			brandName = rs("brandName")
			modelName = rs("modelName")
			itemDesc = rs("artist") & " " & rs("designName")
			msrp = rs("msrp")
			price_we = rs("price_WE")
			sampleImg = rs("sampleImg")
			if isnull(rs("image")) or rs("image") = "" then
				useImg = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & rs("defaultImg")
				itemPic = rs("defaultImg")
			else
				if fso.fileExists(server.MapPath("/productpics/musicSkins/musicSkinsSmall/" & rs("image"))) then
					useImg = "/productpics/musicSkins/musicSkinsSmall/" & rs("image")
					itemPic = rs("image")
				else
					useImg = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & rs("defaultImg")
					itemPic = rs("defaultImg")
				end if
			end if
			if isnull(sampleImg) or len(sampleImg) < 1 then
				if not isnull(rs("defaultImg")) and len(rs("defaultImg")) > 0 then
					sampleImg = replace(rs("defaultImg"),"artwork-thumbnail","sample-devices")
				else
					sampleImg = ""
				end if
			end if
		else
			itemDesc = rs("itemDesc")
			itemPic = rs("itemPic")
			msrp = rs("price_retail")
			price_we = rs("price_our")
			useImg = "/productPics/thumb/" & rs("itemPic")
			partNumber = rs("partNumber")
			NoDiscount = rs("noDiscount")
			masterQty = rs("masterQty")
			
			dim showOtherVersions : showOtherVersions = 0
			if instr(itemDesc,"(") > 0 then
				sql = "select itemID, itemPic, itemDesc from we_items where itemDesc like '%" & left(itemDesc,instr(itemDesc,"(")) & "%' and itemID <> " & itemID
				session("errorSQL") = sql
				set otherVersionsRS = oConn.execute(sql)
				if not otherVersionsRS.EOF then
					showOtherVersions = 1
					removePartName = left(itemDesc,instr(itemDesc,"("))
				end if
			end if
		end if
		
		dim reviewAvgRating : reviewAvgRating = "0.0"
		dim reviewRatingCount : reviewRatingCount = 0
	
		if showOtherVersions = 1 then
			sql = "select isnull( avg( a.Rating), 0.0) as AvgRating, count( a.Rating) as RatingCount from we_reviews a with(nolock) left join we_items b on a.itemID = b.itemID where a.itemID = " & itemID & " or b.itemDesc like '%" & removePartName & "%'"
		else
			sql = "select isnull( avg( Rating), 0.0) as AvgRating, count( Rating) as RatingCount from we_reviews with(nolock) where itemID = " & itemID
		end if
		session("errorSQL") = sql
		set reviewRS = oConn.Execute(sql)
	
		if not reviewRS.EOF then
			reviewAvgRating = reviewRS("AvgRating")
			reviewRatingCount = reviewRS("RatingCount")
		end if
	
		reviewRS = null
	%>
    <tr bgcolor="#ebebeb"><td align="left" style="font-weight:bold; font-size:14px; color:#000000; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%><%=session("crumbLvl1")%><%=session("crumbLvl2")%><%=session("crumbLvl3")%><%=session("crumbLvl4")%></td></tr>
    <tr>
    	<td align="center">
        	<table border="0" cellpadding="2" cellspacing="2" style="padding-left:20px; padding-right:20px" width="100%">
            	<tr>
                	<td align="left" style="font-weight:bold; font-size:12px;"><%=itemDesc%></td>
                </tr>
                <tr>
                	<td>
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                        	<tr>
                            	<td align="center" onclick="ajax('/ajax/mobileContent.asp?z=<%=itemPic%>&musicSkinItem=<%=musicSkinItem%>&sampleImg=<%=sampleImg%>','mobileContent',1)"><img src="<%=useImg%>" border="0" /><br />Click to Zoom</td>
                                <td>
                                	<table border="0" cellpadding="3" cellpadding="0" width="100%">
                                        <tr>
                                            <td align="left" style="font-size:12px;" colspan="2"><img src="/images/product/bullet.gif" border="0" />&nbsp;Ships within 24/48 hours</td>
                                        </tr>
                                        <tr>
                                        	<td align="left" colspan="2" style="font-weight:bold; color:#F00; font-size:16px;">FREE SHIPPING</td>
                                        </tr>
                                        <tr>
                                        	<td colspan="2" style="border-top:2px solid #999;">
                                            	<div style="float:left; width:145px;">
                                                	<div style="float:left; width:145px;">
                                                    	<div style="font-weight:bold; font-size:13px; width:100%; text-align:center; margin-bottom:10px;">Customer Rating:</div>
                                                        <div style="width:100%; text-align:center;">
                                                        	<%
															for i = 1 to 5
																if reviewAvgRating => i then
																	strStarType = "full"
																elseif reviewAvgRating => ((i - 1) + .5) then
																	strStarType = "half"
																else
																	strStarType = "empty"
																end if
															%>
                                                            <img src="/images/product/star_<%=strStarType%>.jpg" width="17" height="17" border="0" />
                                                            <%
															next
                                                            %>
                                                        </div>
                                                    </div>
                                                    <div style="float:left; width:145px;;">
                                                    	<div style="font-weight:bold; font-size:12px; width:100%; text-align:center; margin-bottom:10px;">(<%=reviewRatingCount%> Customer Reviews)</div>
                                                        <div style="font-weight:bold; width:100%; text-align:center;"><a href="#ReviewPanel" style="font-size:10px; color:#ff6633;">Read All Reviews</a></div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr><td colspan="2"><hr color="#999999" /></td></tr>
                                        <tr>
                                            <td align="right" style="font-weight:bold; font-size:12px;">Retail Price:</td>
                                            <td align="right" style="font-size:12px; text-decoration:line-through;"><%=formatCurrency(msrp,2)%></td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="font-weight:bold; font-size:12px;">Our Price:</td>
                                            <td align="right" style="font-size:12px;"><%=formatCurrency(price_we,2)%></td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="font-weight:bold; font-size:12px;">Shipping:</td>
                                            <td align="right" style="font-size:12px; color:#ff6633;">FREE</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right" style="font-weight:bold; font-size:16px">Total Price: <span style="color:#F00"><%=formatCurrency(price_we,2)%></span></td>
                                        </tr>
                                        <%
										specialOffer = 0
										validProd = split("FP1|FP2|FP3|FP4|FP5|FP6","|")
										for i = 0 to ubound(validProd)
											if instr(partNumber,validProd(i)) > 0 then specialOffer = 1
										next
										if NoDiscount then specialOffer = 0
										if specialOffer = 1 then
										%>
                                        <tr><td colspan="2" align="right"><img src="/images/product/fp_minipromo1_white.jpg" border="0" alt="Buy 1, Get 1 50% Off!"></td></tr>
                                        <%
										end if
										%>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                            	<td align="center" colspan="2">
                                	<div style="text-align:left;"><iframe src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fm.wirelessemporium.com%2Fmobile%2F%3Fi%3D<%=itemID%>&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe></div>
                                    <% if showOtherVersions = 1 then %>
                                    <div style="border-top:1px solid #000; margin-top:5px; padding-top:5px;">
                                    	<div style="font-weight:bold; float:left; padding-top:3px;">Other Styles:</div>
                                        <div style="float:left; padding-left:5px;">
                                        	<select name="otherColors" onchange="ajax('/ajax/mobileContent.asp?i=' + this.value,'mobileContent',1)">
                                            	<option value="">Select Style</option>
                                                <%
												do while not otherVersionsRS.EOF
													dim ovItemID : ovItemID = otherVersionsRS("itemID")
													dim ovItemDesc : ovItemDesc = otherVersionsRS("itemDesc")
													ovItemDesc = replace(replace(ovItemDesc,removePartName,""),")","")
												%>
												<option value="<%=ovItemID%>"><%=ovItemDesc%></option>
												<%
													otherVersionsRS.movenext
												loop
												%>
                                            </select>
                                        </div>
                                    </div>
                                    <% end if %>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">
                                	<% if masterQty > 0 then %>
                                    <form method="post" action="/mobile/">
                                    <table border="0" cellpadding="2" cellspacing="2">
                                        <tr>
                                            <td><img src="/images/product/seal_90day_white.jpg" border="0" width="50" /></td>
                                            <td style="font-size:12px;">Quantity:</td>
                                            <td><input type="text" name="qty" value="1" size="4" /></td>
                                            <td>&nbsp;&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="right">
                                                <input type="hidden" name="itemID" value="<%=itemID%>" />
                                                <input type="image" src="/images/mobile/buttons/button_addtocart.png" border="0" alt="Add to Cart" align="absmiddle"><br />
                                                <img src="/images/mobile/buttons/button_remindmelater.png" border="0" alt="Remind Me Later" style="cursor:pointer;" onclick="ajax('/ajax/mobileContent.asp?remindItemID=<%=itemID%>','mobileContent',1)" />
                                            </td>
                                        </tr>
                                    </table>
                                    </form>
                                    <% else %>
                                    <div style="width:290px; text-align:center; color:#F00; font-weight:bold; font-size:16px; padding-bottom:20px;">Product Currently<br />Out of Stock</div>
                                    <% end if %>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td align="right" valign="bottom" style="padding-right:20px; background-image:url(/images/mobile/backgrounds/trustBg.gif); background-repeat:repeat-x;"><img src="/images/mobile/trust_icons.jpg" /></td></tr>
    <tr>
    	<td align="center">
        	<% if musicSkinItem = 1 then %>
            <div style="text-align:left; padding:5px 10px 0px 10px;">
            <%=brandName%>&nbsp;<%=modelName%> Music Skins ??<%=itemDesc%> ??There is no better way to personalize your phone than with a Music Skin. 
            Music skins are a thin, "sticker-like" covering made from a patented 3M material that wraps around your entire phone, helping to prevent scratches 
            and dust from marring the look of your phone. Easy to apply and non-permanent, Music Skins are custom cut to fit your phone and add virtually no 
            bulk at all to your phone. Even with your phone wrapped in a Music Skin you can use it in conjunction with any case, holster or charging dock. 
			<br /><br />
            <ul>
            	<li>Thin material wraps around your <%=brandName%>&nbsp;<%=modelName%> without adding any bulk</li>
            	<li>Provides basic layer of protection from scratches and dust</li>
            	<li>Premium grade vinyl material and high gloss finish for a stunning look</li>
            	<li>Custom cut and non-permanent</li>
            	<li>NOT REUSABLE</li>
            </ul>
            </div>
            <% else %>
        	<table border="0" cellpadding="2" cellspacing="2" style="padding-left:20px; padding-right:20px" width="90%">
                <tr><td style="font-weight:bold; font-size:14px;" nowrap="nowrap"><a onclick="ajax('/ajax/mobileContent.asp?i=<%=itemID%>&d=1','prodDetails',1)">Description</a>&nbsp;|&nbsp;<a onclick="ajax('/ajax/mobileContent.asp?i=<%=itemID%>&d=2','prodDetails',1)">Features</a>&nbsp;|&nbsp;<a onclick="ajax('/ajax/mobileContent.asp?i=<%=itemID%>&d=3','prodDetails',1)">Compatibility</a></td></tr>
                <tr><td align="left" style="font-size:12px;" id="prodDetails"><%=rs("itemLongDetail")%></td></tr>
            </table>
            <% end if %>
        </td>
    </tr>
    <tr>
    	<td id="ReviewPanel">
			<div style="margin-top:10px; border-top:1px solid #000; border-bottom:1px solid #000; padding:5px; font-weight:bold; font-size:14px;">Customer Product Reviews</div>
			<%
				if showOtherVersions = 1 then
					sql = "select top 10 a.rating, a.nickname, a.reviewTitle, a.review, a.dateTimeEntd from we_reviews a with (nolock) left join we_items b with (nolock) on a.itemID = b.itemID where (a.itemID = " & itemID & " or b.itemDesc like '%" & removePartName & "%') and a.approved = 1 order by a.rating desc, a.dateTimeEntd desc"
				else
					sql = "select top 10 rating, nickname, reviewTitle, review, dateTimeEntd from we_reviews where itemID = " & itemID & " and approved = 1 order by rating desc, dateTimeEntd desc"
				end if
				set reviewRS = oConn.execute(sql)
				
				dim reviewLap : reviewLap = 0
				do while not reviewRS.EOF
					reviewLap = reviewLap + 1
					curRating = reviewRS("rating")
					curNickname = reviewRS("nickname")
					curTitle = reviewRS("reviewTitle")
					curReview = reviewRS("review")
					curDateTimeEntd = reviewRS("dateTimeEntd")
			%>
            <div style="float:left; width:80%; margin-left:20px;<% if reviewLap > 1 then %> border-top:1px solid #000;<% end if %> margin-top:10px; padding-top:10px;">
            	<div style="font-weight:bold; font-size:16px;"><%=curTitle%></div>
                <div style="font-size:12px;">
                	<%
					for i = 1 to 5
						if curRating => i then
							strStarType = "full"
						elseif curRating => ((i - 1) + .5) then
							strStarType = "half"
						else
							strStarType = "empty"
						end if
					%>
					<img src="/images/product/star_<%=strStarType%>.jpg" width="17" height="17" border="0" />
					<%
					next
					%>
                </div>
                <div style="font-size:12px; color:#CCC;">Posted By: <span style="color:#000;"><%=curNickname%></span> on <%=curDateTimeEntd%></div>
                <div style="font-size:12px;"><%=curReview%></div>
            </div>
            <%
					reviewRS.movenext
				loop
			%>
        </td>
    </tr>
    <tr><td><div style="margin-top:10px; background-color:#999; color:#FFF; padding:2px 0px 2px 5px;">Direct Link: <%="http://" & request.ServerVariables("SERVER_NAME") & "?i=" & itemID%></div></td></tr>
	<%
	elseif showProducts = 1 then
		if rs.EOF then
			sql = "select typeName from we_types where typeID = " & productType
			session("errorSQL") = sql
			Set rs = Server.CreateObject("ADODB.Recordset")
			rs.open sql, oConn, 3, 3
	%>
    <tr bgcolor="#ebebeb"><td align="left" style="font-weight:bold; font-size:14px; color:#000000; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%><%=session("crumbLvl1")%><%=session("crumbLvl2")%><%=session("crumbLvl3")%>&nbsp;<%=rs("typeName")%></td></tr>
    <tr><td align="center" style="padding-top:50px; padding-bottom:50px; color:#F00; font-weight:bold;">No <%=rs("typeName")%> Available For This Phone Model</td></tr>
    <%
		elseif productType = 20 and msGenre = "" and msArtist = "" then
		%>
    <tr><td align="center" style="font-weight:bold; font-size:16px; padding-top:10px;">Select Music Skins Genre</td></tr>
    <tr>
        <td align="center" style="width:260px; padding-top:10px;">
        	<%
			do while not rs.EOF
			%>
            <div onclick="ajax('/ajax/mobileContent.asp?p=<%=productID%>&t=<%=productType%>&msGenre=<%=rs("genre")%>','mobileContent',1)">
            	<img src="/images/buttons/ms__<%=replace(replace(rs("genre"),"&",""),"TV/Movies","TV")%>.jpg" border="0" width="260" height="100" />
            </div>
            <%
				rs.movenext
			loop
			%>
        </td>
    </tr>
        <%
		elseif productType = 20 and msArtist = "" then
		%>
    <tr><td align="center" style="font-weight:bold; font-size:16px; padding-top:10px;">Select <%=msGenre%> Artist</td></tr>
    <tr>
        <td align="center" style="padding-top:10px;">
        	<div style="width:260px;">
				<%
				for x = 2 to artistPage
					for z = 1 to 40
						msArtist = rs("artist")
						do while msArtist = rs("artist")
							rs.movenext
							if rs.EOF then exit do
						loop
					next
				next
                set fso = CreateObject("Scripting.FileSystemObject")
				artistLap = 0
                do while not rs.EOF
					if productID = 945 and msGenre = "Fashion" then
						'response.Write("3:" & sql)
						'response.End()
					end if
					artistLap = artistLap + 1
                %>
                <div style="width:260px; float:left; border-bottom:1px solid #CCC; padding:5px 0px 5px 0px; text-align:center;">
                    <%
                    for i = 0 to 1
                        if rs.EOF then
                    %>
                    <div style="width:130px; float:left; height:100px;">&nbsp;</div>
                    <%
                        else
                            msArtist = rs("artist")
                            if isnull(rs("image")) or rs("image") = "" then
                                useImg = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & rs("defaultImg")
                            else
                                if fso.fileExists(server.MapPath("/productpics/musicSkins/musicSkinsSmall/" & rs("image"))) then
                                    useImg = "/productpics/musicSkins/musicSkinsSmall/" & rs("image")
                                else
                                    useImg = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & rs("defaultImg")
                                end if
                            end if
							if not isnull(msGenre) and len(msGenre) > 0 then
								useMsGenre = server.URLEncode(msGenre)
							else
								useMsGenre = ""
							end if
							if not isnull(msArtist) and len(msArtist) > 0 then
								useMsArtist = server.URLEncode(msArtist)
							else
								useMsArtist = ""
							end if
                    %>
                    <div style="width:125px; float:left; height:100px; padding-top:5px; font-size:11px;<% if i = 0 then %> border-right:1px solid #CCC;<% end if %>" onclick="ajax('/ajax/mobileContent.asp?p=<%=productID%>&t=<%=productType%>&msGenre=<%=useMsGenre%>&msArtist=<%=useMsArtist%>','mobileContent',1)">
                        <img src="<%=useImg%>" border="0" /><br />
                        <%=msArtist%>
                    </div>
                    <%
                            do while msArtist = rs("artist")
                                rs.movenext
                                if rs.EOF then exit do
                            loop
                        end if
                    next
                    %>
                </div>
                <%
					if artistLap = 20 then exit do
                loop
                %>
                <% if not rs.EOF or artistPage > 1 then %>
                <div style="width:260px; float:left; text-align:center; padding:10px;">
                    <%
					for x = 1 to (artistPage - 1)
					%>
                    <div onclick="ajax('/ajax/mobileContent.asp?p=<%=productID%>&t=<%=productType%>&msGenre=<%=msGenre%>&artistPage=<%=x%>','mobileContent',1)" style="width:50px; height:50px; border:1px solid #000; float:left; margin:0px 10px 10px 0px; font-size:11px;">
                        View Page<br />
                        <span style="font-size:16px;"><%=x%></span>
                    </div>
                    <%
					next
					%>
                    <div style="width:50px; height:50px; border:1px solid #000; float:left; margin:0px 10px 10px 0px; font-size:11px; background-color:#000; color:#FFF;">
                        View Page<br />
                        <span style="font-size:16px;"><%=artistPage%></span>
                    </div>
					<%
                    artistLap = 0
                    do while not rs.EOF
                        artistLap = artistLap + 1
                        msArtist = rs("artist")
                        if artistLap = 1 then
                            artistPage = artistPage + 1
                    %>
                    <div onclick="ajax('/ajax/mobileContent.asp?p=<%=productID%>&t=<%=productType%>&msGenre=<%=msGenre%>&artistPage=<%=artistPage%>','mobileContent',1)" style="width:50px; height:50px; border:1px solid #000; float:left; margin:0px 10px 10px 0px; font-size:11px;">
                        View Page<br />
                        <span style="font-size:16px;"><%=artistPage%></span>
                    </div>
                    <%
                        end if
                        
                        do while msArtist = rs("artist")
                            rs.movenext
                            if rs.EOF then exit do
                        loop
                        if artistLap = 40 then artistLap = 0
                    loop
                    %>
                </div>
                <% end if %>
            </div>
        </td>
    </tr>
        <%
		else
			if isnull(session("crumbLvl3")) then session("crumbLvl3") = "<a style='font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?p=" & productID & "','mobileContent',1)>&nbsp;" & rs("modelName") & "&nbsp;&gt;</a>"
			session("crumbLvl4") = "<a style='font-weight:bold; font-size:14px; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?p=" & productID & "&t=" & productType & "','mobileContent',1)>&nbsp; " & rs("typeName") & "&nbsp;&gt;</a>"
	%>
    <tr bgcolor="#ebebeb"><td align="left" style="font-weight:bold; font-size:14px; color:#000000; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%><%=session("crumbLvl1")%><%=session("crumbLvl2")%><%=session("crumbLvl3")%>&nbsp;<%=rs("typeName")%></td></tr>
    <tr>
    	<td style="padding:10px 0px 10px 5px; border-bottom:1px solid #000;">
        	<div style="float:left; margin-right:10px;">Sort Products By:</div>
            <div style="float:left;">
            	<select name="sortOption" onchange="resortProducts(this.value,<%=productID%>,<%=productType%>,<%=moreProduct%>)">
                	<option value="9 desc">Most Popular</option>
                    <option value="3"<% if sortBy = "3" then %> selected="selected"<% end if %>>Name A-Z</option>
                    <option value="3 desc"<% if sortBy = "3 desc" then %> selected="selected"<% end if %>>Name Z-A</option>
                    <option value="5"<% if sortBy = "5" then %> selected="selected"<% end if %>>Price Low to High</option>
                    <option value="5 desc"<% if sortBy = "5 desc" then %> selected="selected"<% end if %>>Price High to Low</option>
                </select>
            </div>
        </td>
    </tr>
	<%
			lap = 0
			set fso = CreateObject("Scripting.FileSystemObject")
			do while not rs.EOF
				lap = lap + 1
	%>
    <tr>
    	<%
		if productType = 20 then
			if fso.fileExists(server.MapPath("/productpics/musicSkins/musicSkinsSmall/" & rs("image"))) then
				useImg = "/productpics/musicSkins/musicSkinsSmall/" & rs("image")
			else
				useImg = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & rs("defaultImg")
			end if
		%>
        <td align="center" onclick="ajax('/ajax/mobileContent.asp?p=<%=productID%>&t=<%=productType%>&i=<%=rs("id")%>&at=<%=accessoryType%>','mobileContent',1)">
        	<table border="0" cellpadding="2" cellspacing="2" width="100%">
            	<tr>
                	<td valign="top" rowspan="3"><img src="<%=useImg%>" /></td>
                    <td valign="top" width="100%" align="left" style="font-size:16px;"><%=rs("artist")%> <%=rs("designName")%></td>
                </tr>
                <tr><td align="left" style="font-size:12px; font-weight:bold; color:#999">Retail Price: <%=formatCurrency(rs("msrp"),2)%></td></tr>
                <tr><td align="left" style="font-size:16px; font-weight:bold; color:#000">Our Price: <%=formatCurrency(rs("price_WE"),2)%></td></tr>
                <tr><td colspan="2"><hr color="#ff6633" /></td></tr>
            </table>
        </td>
        <%
		else
			if fso.fileExists(server.MapPath("/productPics/thumb/" & rs("itemPic"))) then
				useImg = "/productPics/thumb/" & rs("itemPic")
			else
				useImg = "/productPics/thumb/imagena.jpg"
			end if
		%>
    	<td align="center" onclick="ajax('/ajax/mobileContent.asp?p=<%=productID%>&t=<%=productType%>&i=<%=rs("itemID")%>&at=<%=accessoryType%>','mobileContent',1)">
        	<table border="0" cellpadding="2" cellspacing="2" width="100%">
            	<tr>
                	<td valign="top" rowspan="3"><img src="<%=useImg%>" border="0" width="100" height="100" /></td>
                    <td valign="top" width="100%" align="left" style="font-size:16px;"><%=rs("itemDesc")%></td>
                </tr>
                <tr><td align="left" style="font-size:12px; font-weight:bold; color:#999">Retail Price: <%=formatCurrency(rs("price_retail"),2)%></td></tr>
                <tr><td align="left" style="font-size:16px; font-weight:bold; color:#000">Our Price: <%=formatCurrency(rs("price_our"),2)%></td></tr>
                <tr><td colspan="2"><hr color="#ff6633" /></td></tr>
            </table>
        </td>
        <% end if %>
    </tr>
    <%
				rs.movenext
				if lap = (prodsToShow - 1) then exit do
			loop
			if not rs.EOF and lap < 90 then
	%>
    <tr><td align="center"><input type="button" name="myAction" value="Show More Products" onclick="ajax('/ajax/mobileContent.asp?p=<%=productID%>&t=<%=productType%>&msGenre=<%=msGenre%>&mp=1&sb=<%=sortBy%>','mobileContent',1)" /></td></tr>
    <tr><td><br /></td></tr>
    <tr><td><br /></td></tr>
    <%
			end if
		end if
	%>
    <tr><td><div style="margin-top:10px; background-color:#999; color:#FFF; padding:2px 0px 2px 5px;">Direct Link: <%="http://" & request.ServerVariables("SERVER_NAME") & "?m=" & productID & "&c=" & productType%></div></td></tr>
	<%
	elseif productDetails = 1 then
		if isnull(session("crumbLvl1")) or len(session("crumbLvl1")) < 1 then session("crumbLvl1") = "<a style='font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?b=" & rs("brandID") & "&bn=" & rs("brandName") & "','mobileContent',1)>&nbsp;" & rs("brandName") & "&nbsp;&gt;</a>"
		session("crumbLvl3") = "<a style='font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?p=" & productID & "','mobileContent',1)>&nbsp;" & rs("modelName") & "&nbsp;&gt;</a>"
	%>
    <tr bgcolor="#ebebeb"><td align="left" style="color:#000000; font-weight:bold; font-size:14px; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%><%=session("crumbLvl1")%><%=session("crumbLvl2")%>&nbsp;<%=rs("modelName")%></td></tr>
    <tr>
    	<td align="center">
        	<table border="0" cellpadding="2" cellspacing="2" width="100%">
            	<tr>
                    <td valign="top" onclick="ajax('/ajax/mobileContent.asp?p=<%=rs("modelID")%>&t=5','mobileContent',1)">
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                        	<tr>
                            	<td><img src="/images/mobile/accessories/handsfree.jpg" border="0" /></td>
                                <td style="font-size:18px; color:#ff6633; padding-left:20px;" nowrap="nowrap">HANDS-FREE</td>
                                <td align="right" width="100%"><img src="/images/mobile/arrows.png" border="0" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td colspan="2" style="border-bottom:1px solid #cccccc;"></td></tr>
				<%
				lap = 1
				musicSkins = 0
				do while not rs.EOF
					musicSkins = rs("musicSkins")
					select case rs("typeID")
						case "1" : showType = "BATTERIES"
						case "2" : showType = "CHARGERS"
						case "3" : showType = "FACEPLATES"
						case "4" : showType = "KEYPADS"
						case "5" : showType = "HANDS-FREE"
						case "6" : showType = "HOLSTERS"
						case "7" : showType = "LEATHER CASES"
						case "8" : showType = "OTHER GEAR"
						case "12" : showType = "ANTENNAS &amp; PARTS"
						case "13" : showType = "CABLES &amp; MEMORY"
						case "14" : showType = "CHARMS"
						case "16" : showType = "CELL PHONES"
						case "17" : showType = "BODY GUARDS/SKINS"
						case "18" : showType = "Screen Protectors"
						case "19" : showType = "Decal Skins"
						case "20" : showType = "Music Skins"
						case "22" : showType = "Bundle Packs"
					end select
				%>
            	<tr>
                    <td valign="top" onclick="ajax('/ajax/mobileContent.asp?p=<%=rs("modelID")%>&t=<%=rs("typeID")%>','mobileContent',1)">
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                        	<tr>
                            	<td><img src="/images/mobile/accessories/<%=rs("typeImg")%>" border="0" /></td>
                                <td style="font-size:18px; color:#ff6633; padding-left:20px;" nowrap="nowrap"><%=ucase(showType)%></td>
                                <td align="right" width="100%"><img src="/images/mobile/arrows.png" border="0" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td style="border-bottom:1px solid #cccccc;"></td></tr>
                <%
					rs.movenext
				loop
				if musicSkins > 0 then
				%>
                <tr>
                    <td valign="top" onclick="ajax('/ajax/mobileContent.asp?p=<%=productID%>&t=20','mobileContent',1)">
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                        	<tr>
                            	<td><img src="/images/mobile/accessories/Musicskin.png" border="0" /></td>
                                <td style="font-size:18px; color:#ff6633; padding-left:20px;" nowrap="nowrap">MUSIC SKINS</td>
                                <td align="right" width="100%"><img src="/images/mobile/arrows.png" border="0" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td style="border-bottom:1px solid #cccccc;"></td></tr>
                <%
				end if
				%>
            </table>
        </td>
    </tr>
    <%
	elseif menuOption = 3 then
		session("crumbLvl1") = "<a style='font-weight:bold; font-size:14px; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?mm=2','mobileContent',1)>&nbsp;Accessories&nbsp;&gt;</a>"
	%>
    <tr bgcolor="#ebebeb"><td align="left" style="color:#000000; font-weight:bold; font-size:14px; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%>&nbsp;Accessories</td></tr>
    <tr>
    	<td align="center">
        	<table border="0" bordercolor="#00FF00" cellpadding="3" cellspacing="0" width="100%">
				<%
				lap = 1
				do while not rs.EOF
					select case rs("typeID")
						case "1" : showType = "BATTERIES"
						case "2" : showType = "CHARGERS"
						case "3" : showType = "FACEPLATES"
						case "4" : showType = "KEYPADS"
						case "5" : showType = "HANDS-FREE"
						case "6" : showType = "HOLSTERS"
						case "7" : showType = "LEATHER CASES"
						case "8" : showType = "OTHER GEAR"
						case "12" : showType = "ANTENNAS &amp; PARTS"
						case "13" : showType = "CABLES &amp; MEMORY"
						case "14" : showType = "CHARMS"
						case "16" : showType = "CELL PHONES"
						case "17" : showType = "BODY GUARDS/SKINS"
						case "18" : showType = "Screen Protectors"
						case "19" : showType = "Decal Skins"
						case "20" : showType = "Music Skins"
						case "22" : showType = "Bundle Packs"
					end select
					if rs("typeID") = 14 or rs("typeID") = 5 then
				%>
            	<tr>
                    <td valign="top" onclick="ajax('/ajax/mobileContent.asp?p=1&t=<%=rs("typeID")%>','mobileContent',1)">
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                        	<tr>
                            	<td><img src="/images/mobile/accessories/<%=rs("typeImg")%>" border="0" /></td>
                                <td style="font-size:18px; color:#ff6633; padding-left:20px;" nowrap="nowrap"><%=ucase(showType)%></td>
                                <td align="right" width="100%"><img src="/images/mobile/arrows.png" border="0" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td colspan="2" style="border-bottom:1px solid #cccccc;"></td></tr>
                <%
					else
				%>
            	<tr>
                    <td valign="top" onclick="ajax('/ajax/mobileContent.asp?mm=1&preType=<%=rs("typeID")%>','mobileContent',1)">
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                        	<tr>
                            	<td><img src="/images/mobile/accessories/<%=rs("typeImg")%>" border="0" /></td>
                                <td style="font-size:18px; color:#ff6633; padding-left:20px;" nowrap="nowrap"><%=ucase(showType)%></td>
                                <td align="right" width="100%"><img src="/images/mobile/arrows.png" border="0" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td colspan="2" style="border-bottom:1px solid #cccccc;"></td></tr>
                <%
					end if
					rs.movenext
				loop
				%>
            </table>
        </td>
    </tr>
    <%
	elseif menuOption = 2 then
		session("crumbLvl1") = "<a style='font-weight:bold; font-size:14px; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?mm=2','mobileContent',1)>&nbsp;Carriers&nbsp;&gt;</a>"
	%>
    <tr bgcolor="#ebebeb"><td align="left" style="color:#000000; font-weight:bold; font-size:14px; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%>&nbsp;Carriers</td></tr>
    <tr>
    	<td align="center">
        	<table border="0" bordercolor="#00FF00" cellpadding="3" cellspacing="0" width="300">
				<tr><td>&nbsp;</td></tr>
				<%
				set fso = CreateObject("Scripting.FileSystemObject")
				do while not rs.EOF
				%>
                <tr>
                <%
					for i = 0 to 1
						if rs.EOF then
				%>
                	<td>&nbsp;</td>
                <%
						else
							displayName = rs("carrierName")
							displayName = replace(replace(displayName," ","-"),"/","-")
							displayImg = rs("carrierImg")
						
							if fso.fileExists(server.MapPath("/images/mobile/carriers/" & displayImg)) then
				%>
                    <td width="33%" onclick="ajax('/ajax/mobileContent.asp?c=<%=rs("carrierCode")%>&cn=<%=server.URLEncode(rs("carrierName"))%>','mobileContent',1)">
                    	<img src="/images/mobile/carriers/<%=displayImg%>" border="0" />
                    </td>
                <%
							else
				%>
                    <td width="33%" onclick="ajax('/ajax/mobileContent.asp?c=<%=rs("carrierCode")%>&cn=<%=server.URLEncode(rs("carrierName"))%>','mobileContent',1)" align="center">
                    	<table border="0" bordercolor="#FF0000" cellpadding="0" cellspacing="0">
                        	<tr><td><img src="/images/blank.gif" border="0" width="93" height="1" /></td></tr>
                            <tr>
                            	<td align="center">
                                	<table border="0" cellpadding="0" cellspacing="0" style="background-image:url(/images/mobile/brands/blankBrand.jpg); background-repeat:no-repeat;" width="100%">
                                    	<tr>
                                        	<td><img src="/images/blank.gif" border="0" width="1" height="81" /></td>
                                            <td align="center" style="font-size:12px; font-weight:bold;"><%=replace(rs("carrierName"),"/","<br />")%></td>
                                            <td><img src="/images/blank.gif" border="0" width="1" height="81" /></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td><img src="/images/blank.gif" border="0" width="93" height="1" /></td></tr>
                        </table>
                    </td>
                <%
							end if
							rs.movenext
						end if
                    next
                %>
                </tr>
                <%
				loop
				%>
            </table>
        </td>
    </tr>
	<%
	elseif carrierCode <> "" then
		tempCrumb = "<a style='font-weight:bold; font-size:14px; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?mm=2','mobileContent',1)>&nbsp;Carriers&nbsp;&gt;</a>"
		session("crumbLvl2") = "<a style='font-weight:bold; font-size:14px; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?c=" & carrierCode & "&cn=" & server.URLEncode(carrierName) & "','mobileContent',1)>&nbsp;" & carrierName & "&nbsp;&gt;</a>"
	%>
    <tr bgcolor="#ebebeb"><td nowrap="nowrap" align="left" style="font-weight:bold; font-size:14px; color:#000000; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%><%=tempCrumb%>&nbsp;<%=carrierName%></td></tr>
    <tr>
    	<td align="center">
        	<table border="0" cellpadding="2" cellspacing="2">
            	<% do while not rs.EOF %>
                <tr>
                	<%
					for i = 0 to 1
						if rs.EOF then
					%>
                    <td style="border-bottom:1px solid #CCC;">&nbsp;</td>
                    <%
						else
					%>
                	<td align="center" onclick="ajax('/ajax/mobileContent.asp?p=<%=rs("modelID")%>','mobileContent',1)" style="border-bottom:1px solid #CCC;">
                        <div style="margin:5px 0px 5px 0px;">
                        <table border="0" cellpadding="3" cellspacing="0"<% if i < 1 then %> style="border-right:1px solid #CCC;"<% end if %> width="150">
                        	<tr>
                            	<td align="center" style="font-size:12px;"><%=replace(rs("modelName"),"/"," ")%></td>
                            </tr>
                            <tr>
                            	<td align="center"><img src="/productPics/models/<%=rs("modelImg")%>" width="50" alt="<%=rs("modelName")%>" /></td>
                            </tr>
                        </table>
                        </div>
                    </td>
                    <%
							rs.movenext
						end if
					next
					%>
                </tr>
                <% loop %>
            </table>
        </td>
    </tr>
    <%
	elseif brandID > 0 then
		session("crumbLvl2") = "<a style='font-weight:bold; font-size:14px; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?b=" & brandID & "&bn=" & brandName & "','mobileContent',1)>&nbsp;" & brandName & "&nbsp;&gt;</a>"
	%>
    <tr bgcolor="#ebebeb"><td nowrap="nowrap" align="left" style="font-weight:bold; font-size:14px; color:#000000; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%><%=session("crumbLvl1")%>&nbsp;<%=brandName%></td></tr>
    <tr>
    	<td align="center">
        	<table border="0" cellpadding="0" cellspacing="0">
            	<% do while not rs.EOF %>
                <tr>
                	<%
					for i = 0 to 1
						if rs.EOF then
					%>
                    <td style="border-bottom:1px solid #CCC;">&nbsp;</td>
                    <%
						else
					%>
                	<td align="center" onclick="ajax('/ajax/mobileContent.asp?p=<%=rs("modelID")%>&t=<%=preType%>','mobileContent',1)" style="border-bottom:1px solid #CCC;">
                    	<div style="margin:5px 0px 5px 0px;">
                        <table border="0" cellpadding="3" cellspacing="0"<% if i < 1 then %> style="border-right:1px solid #CCC;"<% end if %> width="150">
                        	<tr>
                            	<td align="center" style="font-size:12px;"><%=replace(rs("modelName"),"/"," ")%></td>
                            </tr>
                            <tr>
                            	<td align="center"><img src="/productPics/models/<%=rs("modelImg")%>" width="50" /></td>
                            </tr>
                        </table>
                        </div>
                    </td>
                    <%
							rs.movenext
						end if
					next
					%>
                </tr>
                <% loop %>
            </table>
        </td>
    </tr>
    <%
	else
		session("crumbLvl1") = "<a style='font-weight:bold; font-size:14px; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?mm=1','mobileContent',1)>&nbsp;Brands&nbsp;&gt;</a>"
	%>
    <tr bgcolor="#ebebeb"><td align="left" style="color:#000000; font-weight:bold; font-size:14px; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%>&nbsp;Brands</td></tr>
    <tr>
    	<td align="center">
        	<table border="0" bordercolor="#00FF00" cellpadding="3" cellspacing="0">
				<tr><td>&nbsp;</td></tr>
				<%
				set fso = CreateObject("Scripting.FileSystemObject")
				do while not rs.EOF
				%>
                <tr>
                <%
					for i = 0 to 2
						if rs.EOF then
				%>
                	<td>&nbsp;</td>
                <%
						else
							displayName = rs("brandName")
							brandImg = rs("brandImg")
							displayName = replace(replace(displayName," ","-"),"/","-")
							if not isnull(brandImg) and len(brandImg) > 0 then
								useBrandImg = replace(replace(brandImg,".gif",".jpg"),".png",".jpg")
							else
								useBrandImg = "123aaa"
							end if
						
							if fso.fileExists(server.MapPath("/images/mobile/brands/" & useBrandImg)) then
				%>
                    <td width="33%" onclick="ajax('/ajax/mobileContent.asp?b=<%=rs("brandID")%>&bn=<%=server.URLEncode(rs("brandName"))%>&preType=<%=preType%>','mobileContent',1)">
                    	<img src="/images/mobile/brands/<%=replace(replace(rs("brandImg"),".gif",".jpg"),".png",".jpg")%>" border="0" />
                    </td>
                <%
							else
				%>
                    <td width="33%" onclick="ajax('/ajax/mobileContent.asp?b=<%=rs("brandID")%>&bn=<%=server.URLEncode(rs("brandName"))%>&preType=<%=preType%>','mobileContent',1)" align="center">
                    	<table border="0" bordercolor="#FF0000" cellpadding="0" cellspacing="0">
                        	<tr><td><img src="/images/blank.gif" border="0" width="93" height="1" /></td></tr>
                            <tr>
                            	<td align="center">
                                	<table border="0" cellpadding="0" cellspacing="0" style="background-image:url(/images/mobile/brands/blankBrand.jpg); background-repeat:no-repeat;" width="100%">
                                    	<tr>
                                        	<td><img src="/images/blank.gif" border="0" width="1" height="81" /></td>
                                            <td align="center" style="font-size:12px; font-weight:bold;"><%=replace(rs("brandName"),"/","<br />")%></td>
                                            <td><img src="/images/blank.gif" border="0" width="1" height="81" /></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td><img src="/images/blank.gif" border="0" width="93" height="1" /></td></tr>
                        </table>
                    </td>
                <%
							end if
							rs.movenext
						end if
                    next
                %>
                </tr>
                <%
				loop
				%>
            </table>
        </td>
    </tr>
    <% end if %>
</table>
<%
	call CloseConn(oConn)
%>
<!-- Google Analytics Start -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1311669-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Google Analytics End -->