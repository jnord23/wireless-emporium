<%
	response.buffer = true
	response.expires = now
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	dim brandSelect : brandSelect = prepInt(request.QueryString("brandSelect"))
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim modelSelect : modelSelect = prepInt(request.QueryString("modelSelect"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	
	if brandSelect = 1 then
		sql = "select modelID, modelName from we_models where brandID = " & brandID & " and mobileReady = 1 and isPhone = 1 and oldModel = 0 order by modelName"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
%>
<select id="actualSelectBox" name="modelID" onchange="newModelSelected()">
	<option value="">Choose A Device Model</option>
    <%
	dim modelList : modelList = ""
	do while not rs.EOF
	%>
    <option value="<%=rs("modelID")%>"><%=rs("modelName")%></option>
    <%
		modelList = modelList & rs("modelID") & "@@" & formatSEO(rs("modelName")) & "##"
		rs.movenext
	loop
	rs = null
	%>
</select>
<%
		modelList = left(modelList,len(modelList)-2)
		modelArray = split(modelList,"##")
		for i = 0 to ubound(modelArray)
			detailArray = split(modelArray(i),"@@")
%>
<input type="hidden" name="model_<%=detailArray(0)%>" value="<%=detailArray(1)%>" />
<%
		next
	end if
	
	if modelSelect = 1 then
		sql = "select typeID, typeName from we_types where typeID in (select distinct typeID from we_Items where modelID = " & modelID & " and hideLive = 0 and ghost = 0) and typeID not in (4,8,9,12,15,16,20,22,23,24,25) order by typeName"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
%>
<select id="actualSelectBox" name="typeID" onchange="newTypeSelected(this.value)">
	<option value="">Choose A Product Type</option>
    <%
	dim catList : catList = ""
	do while not rs.EOF
	%>
    <option value="<%=rs("typeID")%>"><%=rs("typeName")%></option>
    <%
		catList = catList & rs("typeID") & "@@" & formatSEO(rs("typeName")) & "##"
		rs.movenext
	loop
	rs = null
	%>
</select>
<%
		catList = left(catList,len(catList)-2)
		catArray = split(catList,"##")
		for i = 0 to ubound(catArray)
			detailArray = split(catArray(i),"@@")
%>
<input type="hidden" name="cat_<%=detailArray(0)%>" value="<%=detailArray(1)%>" />
<%
		next
	end if
	
	call CloseConn(oConn)
%>