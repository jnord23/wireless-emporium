<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<%
	response.buffer = true
	response.expires = -1
	response.ExpiresAbsolute = Now() - 1
	response.CacheControl = "no-cache"
	Session.Timeout = 20
	
	if isnull(session("acctID")) or len(session("acctID")) < 1 or session("acctID") = 0 then response.Redirect("/mobile/?a=")
	
	response.Cookies("checkoutPage") = 1
	response.Cookies("checkoutPage").expires = date + 1
	
	sql = "insert into we_mobileTrackingPages (sessionID,page) values('" & request.Cookies("mySession") & "','" & request.ServerVariables("URL") & "?" & request.ServerVariables("QUERY_STRING") & "')"
	session("errorSQL") = sql
	oConn.execute(sql)
	
	function fOpenConn
		'nothing
	end function
	function fCloseConn
		'nothing
	end function
	
	curPage = "Checkout"
	userMsg = ""
	ajaxRedir = ""
	
	shiptype = SQLquote(request.Form("shiptype"))
	shipAmt = SQLquote(request.Form("shipcost" & shiptype))
	fname = SQLquote(request.Form("fname"))
	lname = SQLquote(request.Form("lname"))
	phone = SQLquote(request.Form("phoneNumber"))
	b_addr1 = SQLquote(request.Form("bAddress1"))
	b_addr2 = SQLquote(request.Form("bAddress2"))
	b_city = SQLquote(request.Form("bCity"))
	b_state = SQLquote(request.Form("bState"))
	b_zip = SQLquote(request.Form("bZip"))
	
	if isnull(shiptype) or len(shiptype) < 1 then shiptype = 0
	if isnull(shipAmt) or len(shipAmt) < 1 then shipAmt = 0
	if isnull(fname) or len(fname) < 1 then fname = ""
	if isnull(lname) or len(lname) < 1 then lname = ""
	if isnull(phone) or len(phone) < 1 then phone = ""
	if isnull(b_addr1) or len(b_addr1) < 1 then b_addr1 = ""
	if isnull(b_addr2) or len(b_addr2) < 1 then b_addr2 = ""
	if isnull(b_city) or len(b_city) < 1 then b_city = ""
	if isnull(b_state) or len(b_state) < 1 then b_state = ""
	if isnull(b_zip) or len(b_zip) < 1 then b_zip = ""
	
	dim updateAcct : updateAcct = 0
	if b_addr1 <> "" then
		if isnumeric(b_zip) then
			for i = len(b_zip) to 4
				b_zip = "0" & b_zip
			next
		end if
		sql = "update we_accounts set fname = '" & fname & "', lname = '" & lname & "', phone = '" & phone & "', bAddress1 = '" & b_addr1 & "', bAddress2 = '" & b_addr2 & "', bCity = '" & b_city & "', bState = '" & b_state & "', bZip = '" & b_zip & "' where accountID = " & session("acctID")
		updateAcct = 1
		session("errorSQL") = sql
		oConn.execute(sql)
		if instr(fname," ") > 0 then
			session("customer") = left(fname,instr(fname," "))
		else
			session("customer") = fname
		end if
	end if
	
	tax = 0
	
	if isnull(request.Cookies("mySession")) or len(request.Cookies("mySession")) < 1 then
		response.Cookies("mySession") = session.SessionID
		response.Cookies("mySession").expires = date + 30
		useSessionID = session.SessionID
	else
		useSessionID = request.Cookies("mySession")
	end if
	
	if isnull(session("acctID")) or len(session("acctID")) < 1 then	response.Redirect("https://m.wirelessemporium.com/mobile/login.asp")
	
	sql = "select sum(qty) as cartItems from shoppingCart where sessionID = " & useSessionID & " or accountID = " & session("acctID")
	session("errorSQL") = sql
	Set cartRS = Server.CreateObject("ADODB.Recordset")
	cartRS.open sql, oConn, 0, 1
	cartItems = cartRS("cartItems")
	cartRS = null
	
	sql = "select 0 as musicSkins, b.partNumber, b.noDiscount, a.qty, a.itemID, b.itemDesc, b.price_our from shoppingCart a join we_items b on a.itemID = b.itemID where a.sessionID = " & useSessionID & " or a.accountID = " & session("acctID")
	sql = sql & " union "
	sql = sql & "select 1 as musicSkins, '' as partNumber, cast(0 as bit) as noDiscount, a.qty, a.itemID, b.artist + ' ' + b.designName as itemDesc, b.price_we as price_our from shoppingCart a join we_items_musicSkins b on a.itemID = b.id where a.sessionID = " & useSessionID & " or a.accountID = " & session("acctID")
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 0, 1
	
	sql = "select fname, lname, bAddress1, bAddress2, bCity, bState, bZip, sAddress1, sAddress2, sCity, sState, sZip, phone, email from we_accounts where accountID = " & session("acctID")
	session("errorSQL") = sql
	Set billAddrRS = Server.CreateObject("ADODB.Recordset")
	billAddrRS.open sql, oConn, 0, 1
	
	if not billAddrRS.EOF then
		fname = billAddrRS("fname")
		lname = billAddrRS("lname")
		bAddress1 = billAddrRS("bAddress1")
		bAddress2 = billAddrRS("bAddress2")
		bCity = billAddrRS("bCity")
		bState = billAddrRS("bState")
		bZip = billAddrRS("bZip")
		sAddress1 = billAddrRS("sAddress1")
		sAddress2 = billAddrRS("sAddress2")
		sCity = billAddrRS("sCity")
		sState = billAddrRS("sState")
		sZip = billAddrRS("sZip")
		phone = billAddrRS("phone")
		email = billAddrRS("email")
	end if
	
	baseCrumb = "<a style='font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?r=1','mobileContent',1)>Main Menu&nbsp;&gt;</a>"
%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html>
<head>
	<title>Wireless Emporium Mobile Site - <%=curPage%></title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <script type="text/javascript" src="/includes/js/acCombo.js"></script>
	<script type="text/javascript" src="/includes/js/autocomplete-min.js"></script>
    <style type="text/css">
	body{font:13px/1.231 arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;margin:0;padding:0;}
	select,input,button,textarea,button{font:99% arial,helvetica,clean,sans-serif;}
	table{font-size:inherit;font:100%;}
	pre,code,kbd,samp,tt{font-family:monospace;*font-size:108%;line-height:100%;}
	.yui-skin-sam .yui-ac{position:relative;font-family:arial;font-size:100%;}
	.yui-skin-sam .yui-ac-input{position:absolute;width:100%;}
	.yui-skin-sam .yui-ac-container{position:absolute;top:1.6em;width:100%; font-size:20px;}
	.yui-skin-sam .yui-ac-content{position:absolute;width:100%;border:1px solid #808080;background:#fff;overflow:hidden;z-index:9050;}
	.yui-skin-sam .yui-ac-shadow{position:absolute;margin:.3em;width:100%;background:#000;-moz-opacity:.10;opacity:.10;filter:alpha(opacity=10);z-index:9049;}
	.yui-skin-sam .yui-ac iframe{opacity:0;filter:alpha(opacity=0);padding-right:.3em;padding-bottom:.3em;}
	.yui-skin-sam .yui-ac-content ul{margin:0;padding:0;width:100%;}
	.yui-skin-sam .yui-ac-content li{margin:0;padding:2px 5px;cursor:default;white-space:nowrap;list-style:none;zoom:1;}
	.yui-skin-sam .yui-ac-content li.yui-ac-prehighlight{background:#B3D4FF;}
	.yui-skin-sam .yui-ac-content li.yui-ac-highlight{background:#cccccc;color:#FFF;}
	#myAutoComplete {
	    width:15em; /* set width here or else widget will expand to fit its container */
    	padding-bottom:2em;
	}
	a { color:#ff6633; text-decoration:none; }
	a:hover { color:#ff6633; text-decoration:underline; }
    </style>
</head>
<body class="yui-skin-sam" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" style="width:100%; text-align:center">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr><td align="left" style="border-top:2px solid #666666; border-bottom:2px solid #666666; padding-top:0px;"><a href="/?r=1"><img src="/images/mobile/WE_logo_r2.png" border="0" /></a></td></tr>
    <tr bgcolor="#666666">
    	<td>
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td align="left" style="padding-left:10px;"><img src="/images/mobile/cart.jpg" border="0" /></td>
                    <td id="viewCart" align="left" width="100%" style="font-weight:bold; color:#FFF; padding-left:5px;"<% if cartItems > 0 then %> onclick="ajax('/ajax/mobileCheckout.asp','mobileContent',1)"<% end if %>>
                    	<% if cartItems > 0 then %>
                        <%=cartItems%> Item(s)
                        <% else %>
                        No Items
                        <% end if %>
                    </td>
                    <td align="right" style="padding-right:10px; color:#FFF" nowrap="nowrap">
                    	<%
						noLogin = 1
						if noLogin = 1 then
						%>
                        1.800.305.1106
                        <%
						else
							if isnull(session("customer")) or len(session("customer")) < 1 then
						%>
                        <a href="https://m.wirelessemporium.com/mobile/login.asp?mm=1" style="color:#FFF; font-weight:bold;">Login</a>
                        <% 	else %>
                        Welcome <%=session("customer")%>&nbsp;(<a href="http://m.wirelessemporium.com/mobile/?logout=1" style="color:#FFF;">Logout</a>)
                        <% 	end if
						end if
						%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td align="left" style="background-image:url(/images/mobile/searchBG.gif); background-repeat:repeat-x">
            <form name="searchForm" method="get" action="/mobile/">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td><img src="/images/blank.gif" border="0" width="5" height="30" /></td>
                    <td valign="top" style="padding-top:5px;">
                    	<div id="myAutoComplete">
		                <input id="myInput" name="searchString" type="text" value="Find Your Product" onfocus="testContent(this.value)">
        		        <div id="myContainer"></div>
                        </div>
                    </td>
                    <td><img src="/images/blank.gif" border="0" width="10" height="30" /></td>
                    <td align="right" valign="top" style="padding-top:6px; padding-right:8px;"><input type="image" src="/images/mobile/buttons/button_search.png" align="absmiddle" border="0" alt="GO"></td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <% if userMsg <> "" then %>
    <tr bgcolor="#FF0000"><td align="center" style="font-weight:bold; color:#FFF;"><%=userMsg%></td></tr>
    <% end if %>
    <tr bgcolor="#ebebeb"><td align="left" style="color:#000000; font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%>&nbsp;Checkout</td></tr>
    <tr>
    	<td align="left" style="color:#ff6633; font-size:24px; font-weight:bold; padding-bottom:5px; padding-top:10px; padding-left:5px;">Checkout Step 3 of 3 -</td>
    </tr>
    <tr><td style="border-bottom:1px solid #ff6633;"></td></tr>
    <tr>
    	<td id="mobileContent" align="center" style="padding-left:10px; padding-right:10px;">
        	<form method="post" name="checkoutForm" action="https://m.wirelessemporium.com/cart/process/processing.asp<%=session("additionalQS")%>">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr><td style="color:#FFF;">acct:<%=session("acctID")%>;<%=updateAcct%></td></tr>
                <tr>
                	<td style="font-weight:bold; font-size:18px; color:#ff6633; border-bottom:1px solid #ff6633;" align="left">Cart Items <a href="#" style="font-size:12px;" onclick="ajax('/ajax/mobileCheckout.asp','mobileContent',1)">(Edit Cart Items)</a></td>
                </tr>
                <tr>
                	<td align="center">
                    	<table border="0" cellpadding="3" cellspacing="0">
                        	<tr bgcolor="#ff6633" style="font-weight:bold; color:#FFF">
                            	<td align="left">Item</td>
                                <td align="left">Qty</td>
                                <td align="left">Subtotal</td>
                            </tr>
                            <%
							bgColor = "#ffffff"
							itemTotal = 0
							totalRetail = 0
							lap = 0
							specialOffer = 0
							faceplateDiscountCnt = 0
							validProd = split("FP1|FP2|FP3|FP4|FP5|FP6","|")
							do while not rs.EOF
								lap = lap + 1
								partNumber = rs("partNumber")
								for i = 0 to ubound(validProd)
									if instr(partNumber,validProd(i)) > 0 then
										if not rs("noDiscount") then specialOffer = 1
									end if
								next
								if specialOffer = 1 then
									for i = 1 to cdbl(rs("qty"))
										faceplateDiscountCnt = faceplateDiscountCnt + 1
										if faceplateDiscountCnt = 2 then
											itemTotal = itemTotal + (rs("price_our")/2)
											faceplateDiscountCnt = 0
										else
											itemTotal = itemTotal + rs("price_our")
										end if
									next
								else
									itemTotal = itemTotal + (rs("qty") * rs("price_our"))
								end if
								totalRetail = totalRetail + (rs("price_our") * rs("qty"))
								specialOffer = 0
							%>
                            <tr bgcolor="<%=bgColor%>">
                            	<td align="left"><%=rs("itemDesc")%></td>
                                <td align="right"><%=rs("qty")%></td>
                                <td align="right">
									<%=rs("qty") * rs("price_our")%>
                                    <input type="hidden" name="ssl_item_number_<%=lap%>" value="<%=rs("itemID")%>">
									<input type="hidden" name="ssl_item_qty_<%=lap%>" value="<%=rs("qty")%>">
                                    <input type="hidden" name="ssl_musicSkins_<%=lap%>" value="<%=rs("musicSkins")%>">
                                </td>
                            </tr>
                            <%
								if bState = "CA" then tax = itemTotal * .0775
								rs.movenext
								if bgColor = "#ffffff" then bgColor = "#f0f0f0" else bgColor = "#ffffff"
							loop
							grandTotal = itemTotal + tax + shipAmt
							%>
                            <tr>
                            	<td colspan="2" align="right" style="font-weight:bold; border-top:1px solid #999999;">Tax</td>
                                <td align="right" style="border-top:1px solid #999999;"><%=formatCurrency(tax,2)%></td>
                            </tr>
                            <tr>
                            	<td colspan="2" align="right" style="font-weight:bold;">Shipping</td>
                                <td align="right"><%=formatCurrency(shipAmt,2)%></td>
                            </tr>
                            <% if totalRetail > itemTotal then %>
                            <tr>
                            	<td colspan="2" align="right" style="font-weight:bold;">Total Before Discount</td>
                                <td align="right"><%=formatCurrency(totalRetail + tax + shipAmt,2)%></td>
                            </tr>
                            <tr>
                            	<td colspan="2" align="right" style="font-weight:bold;">Sale Discount</td>
                                <td align="right"><%=formatCurrency(totalRetail - itemTotal,2)%></td>
                            </tr>
                            <% end if %>
                            <tr>
                            	<td colspan="2" align="right" style="font-weight:bold;">Grand Total</td>
                                <td align="right"><%=formatCurrency(grandTotal,2)%></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td style="font-weight:bold; font-size:18px; color:#ff6633; border-bottom:1px solid #ff6633;" align="left">Credit Card Info</td>
                </tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td>
                    	<table border="0" cellpadding="2" cellspacing="0" align="center">
                            <tr><td colspan="2" align="center"><img src="/images/verisign.jpg" border="0" /></td></tr>
                            <tr>
                                <td valign="top" align="center" colspan="2"><img src="/images/cart/we_cards.gif" alt="We accept Visa, MasterCard, American Express and Discover" border="0" align="absbottom"></td>
                            </tr>
                            <tr>
                                <td align="right" nowrap="nowrap" style="font-weight:bold;">Card Type:</td>
                                <td align="left">
                                    <select name="cc_cardType">
                                        <option value=""></option>
                                        <option value="VISA">VISA</option>
                                        <option value="MC">MASTER CARD</option>
                                        <option value="AMEX">AMERICAN EXPRESS</option>
                                        <option value="DISC">DISCOVER</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td align="right" nowrap="nowrap" style="font-weight:bold;">Card Holder Name:</td><td align="left"><input name="cc_cardOwner" type="text" size="16"></td></tr>
                            <tr><td align="right" nowrap="nowrap" style="font-weight:bold;">Credit Card #:</td><td align="left"><input name="cardNum" type="text" id="cardNum" size="16" maxlength="16"></td></tr>
                            <tr><td align="right">&nbsp;</td><td style="font-size:9px;">no spaces/dashes<br>example: 1234123412341234</td></tr>
                            <tr><td align="right" nowrap="nowrap" style="font-weight:bold;">Security Code:</td><td align="left"><input name="secCode" type="text" id="secCode" size="4" maxlength="4">  <a href="/cart/cvv2help.asp" target="_blank">(What is CVV?)</a></td></tr>
                            <tr>
                                <td align="right" nowrap="nowrap" style="font-weight:bold;">Expiration Date:</td>
                                <td align="left">
                                    <select name="cc_month">
                                        <option value="01">01 (January)</option>
                                        <option value="02">02 (February)</option>
                                        <option value="03">03 (March)</option>
                                        <option value="04">04 (April)</option>
                                        <option value="05">05 (May)</option>
                                        <option value="06">06 (June)</option>
                                        <option value="07">07 (July)</option>
                                        <option value="08">08 (August)</option>
                                        <option value="09">09 (September)</option>
                                        <option value="10">10 (October)</option>
                                        <option value="11">11 (November)</option>
                                        <option value="12">12 (December)</option>
                                    </select>
                                    <select name="cc_year">
                                        <%
                                        dim countYear
                                        for countYear = 0 to 14
                                            response.write "<option value=""" & right(cStr(year(date) + countYear),2) & """>" & cStr(year(date) + countYear) & "</option>" & vbcrlf
                                        next
                                        %>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td style="font-weight:bold; font-size:18px; color:#ff6633; border-bottom:1px solid #ff6633;" align="left">Billing Address</td>
                </tr>
                <tr><td><br /></td></tr>
                <tr>
                	<td>
                    	<table border="0" cellpadding="3" cellspacing="0">
                            <tr>
                                <td align="right" style="font-weight:bold;">First Name:</td>
                                <td style="color:#F00;" align="left"><input type="text" name="fname" value="<%=fname%>" size="18" />&nbsp;*</td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">Last Name:</td>
                                <td style="color:#F00;" align="left"><input type="text" name="lname" value="<%=lname%>" size="18" />&nbsp;*</td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">Phone:</td>
                                <td style="color:#F00;" align="left"><input type="text" name="phoneNumber" value="<%=phone%>" size="18" />&nbsp;*</td>
                            </tr>
                            <tr><td><br /></td></tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">Address 1:</td>
                                <td style="color:#F00;" align="left"><input type="text" name="bAddress1" value="<%=bAddress1%>" size="18" />&nbsp;*</td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">Address 2:</td>
                                <td style="color:#F00;" align="left"><input type="text" name="bAddress2" value="<%=bAddress2%>" size="18" />&nbsp;*</td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">City:</td>
                                <td style="color:#F00;" align="left"><input type="text" name="bCity" value="<%=bCity%>" size="18" />&nbsp;*</td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">State:</td>
                                <td style="color:#F00;" align="left">
                                    <select name="bState">
                                        <%getStates(bState)%>
                                    </select>
                                    &nbsp;*
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight:bold;">Zip/Postal Code:</td>
                                <td style="color:#F00;" align="left"><input type="text" name="bZip" value="<%=bZip%>" size="18" />&nbsp;*</td>
                            </tr>
                            <tr>
                            	<td colspan="2" align="right"><input type="button" name="myAction" value="Update Billing Address" onclick="document.checkoutForm.action='https://m.wirelessemporium.com/mobile/checkout.asp';document.checkoutForm.submit()" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td style="border-bottom:1px solid #ff6633;"><br /></td></tr>
                <tr>
                	<td>
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                        	<tr>
                            	<td style="padding-top:10px;"><img src="/images/verisign.jpg" border="0" /></td>
                                <td align="right" style="padding-top:10px;" width="100%">
                                	<input type="hidden" name="sAddress1" value="<%=sAddress1%>" />
                                    <input type="hidden" name="sAddress2" value="<%=sAddress2%>" />
                                    <input type="hidden" name="sCity" value="<%=sCity%>" />
                                    <input type="hidden" name="sState" value="<%=sState%>" />
                                    <input type="hidden" name="sZip" value="<%=sZip%>" />
                                    <input type="hidden" name="email" value="<%=email%>" />
                                    <input type="hidden" name="shippingTotal" value="<%=shipAmt%>" />
                                    <input type="hidden" name="shiptype" value="<%=shiptype%>" />
                                    <input type="hidden" name="subTotal" value="<%=itemTotal%>" />
                                    <input type="hidden" name="grandTotal" value="<%=grandTotal%>" />
                                    <input type="hidden" name="accountid" value="<%=session("acctID")%>" />
                                    <input type="hidden" name="ssl_ProdIdCount" value="<%=cartItems%>" />
                                    <input type="hidden" name="nShippingID_mobile" value="<%=shiptype%>" />
                                    <input type="hidden" name="mobile" value="1" />
                                    
                                    <input type="image" src="/images/mobile/buttons/button_compcheckout.png" name="myAction" onclick="return(chkForm())" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td><br /></td></tr>
            </table>
            </form>
        </td>
    </tr>
    <tr bgcolor="#ebebeb">
    	<td>
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td nowrap="nowrap" style="padding-left:5px;"><a style="text-decoration:underline;">Contact Us</a></td>
                    <td style="color:#666; font-weight:bold; padding-left:10px; padding-right:10px;">|</td>
                    <td nowrap="nowrap"><a style="text-decoration:underline;">RSS</a></td>
                    <td style="color:#666; font-weight:bold; padding-left:10px; padding-right:10px;">|</td>
                    <td nowrap="nowrap"><a href="/?ms=full" style="text-decoration:underline;">Full Site</a></td>
                	<td align="right" width="100%"><img src="/images/mobile/we_logo_little.png" border="0" /></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div id="testZone"></div>
</body>
</html>
<%
	oConn.Close
	set oConn = nothing
%>
<script language="javascript" src="/includes/js/productKeywords.js"></script>
<script type="text/javascript">
	function chkForm() {
		if (document.checkoutForm.fname.value == "") { alert("Please enter your first name");document.checkoutForm.fname.focus();return false }
		else if (document.checkoutForm.lname.value == "") { alert("Please enter your last name");document.checkoutForm.lname.focus();return false }
		else if (document.checkoutForm.phoneNumber.value == "") { alert("Please enter your phone number");document.checkoutForm.phoneNumber.focus();return false }
		else if (document.checkoutForm.bAddress1.value == "") { alert("Please enter your billing address");document.checkoutForm.addr1.focus();return false }
		else if (document.checkoutForm.bCity.value == "") { alert("Please enter your billing city");document.checkoutForm.city.focus();return false }
		else if (document.checkoutForm.bState.value == "") { alert("Please enter your billing state");document.checkoutForm.state.focus();return false }
		else if (document.checkoutForm.bZip.value == "") { alert("Please enter your billing zip/postal code");document.checkoutForm.zip.focus();return false }
		else if (document.checkoutForm.cc_cardType.value == "") { alert("Please enter your credit card type");document.checkoutForm.cc_cardType.focus();return false }
		else if (document.checkoutForm.cc_cardOwner.value == "") { alert("Please enter the card holder's name");document.checkoutForm.cc_cardOwner.focus();return false }
		else if (document.checkoutForm.cardNum.value == "") { alert("Please enter the credit card number");document.checkoutForm.cardNum.focus();return false }
		else if (document.checkoutForm.secCode.value == "") { alert("Please enter your security code");document.checkoutForm.secCode.focus();return false }
		else if (document.checkoutForm.cc_month.value == "") { alert("Please enter your expiration month");document.checkoutForm.cc_month.focus();return false }
		else if (document.checkoutForm.cc_year.value == "") { alert("Please enter your expiration year");document.checkoutForm.cc_year.focus();return false }
		else { document.checkoutForm.submit() }
	}
	
	function testContent(val) {
		if (val == "Find Your Product") {
			document.searchForm.searchString.value = ""
		}
	}
	
	YAHOO.example.BasicLocal = function() {
		var oDS = new YAHOO.util.LocalDataSource(YAHOO.example.Data.arrayProducts);
		var oAC = new YAHOO.widget.AutoComplete("myInput", "myContainer", oDS);
		oAC.prehighlightClassName = "yui-ac-prehighlight";
		oAC.useShadow = false;
		oAC.maxResultsDisplayed = 3;
		
		return {
			oDS: oDS,
			oAC: oAC
		};
	}();
	
	function ajax(newURL,rLoc,loading) {
		if (newURL.indexOf("?") > 0) {
			newURL = newURL + '&ajaxID=' + Math.floor(Math.random()*999999)
		}
		else {
			newURL = newURL + '?ajaxID=' + Math.floor(Math.random()*999999)
		}
		
		if (loading == 1) {
			document.getElementById(rLoc).innerHTML = "<table width='100%'><tr><td align='center' style=background-image:url(/images/mobile/loading.gif);background-repeat:no-repeat;background-position:center;><img src='/images/blank.gif' border='0' width='1' height='60' /><br /><hr color='#ff6633' /><br /><img src='/images/blank.gif' border='0' width='1' height='40' /><br /><div align='center' style='font-weight:bold; font-size:18px;'>Loading Content...</div><hr color='#ff6633' /><br /><img src='/images/blank.gif' border='0' width='1' height='100' /></td></tr></table>"
		}
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						window.location = "/mobile/?r=1"
					}
					else if(document.getElementById(rLoc)) {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				}
				else {
					//alert("Error performing action")
				}
			}
		};
		httpRequest.send(null);
	}
</script>
<!-- Google Analytics Start -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1311669-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Google Analytics End -->