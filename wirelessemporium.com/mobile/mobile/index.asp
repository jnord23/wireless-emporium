<%
	if isnull(session("ready4Back")) or len(session("ready4Back")) < 1 then response.Redirect("/")
%>
<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<%
	response.buffer = true
	response.expires = -1
	Session.Timeout = 20
	
	userBrowser = request.ServerVariables("HTTP_USER_AGENT")
	displayItemID = SQLquote(request.QueryString("i"))
	displayBrandID = SQLquote(request.QueryString("b"))
	displayModelID = SQLquote(request.QueryString("m"))
	displayCategoryID = SQLquote(request.QueryString("c"))
	
	if isnull(displayItemID) or len(displayItemID) < 1 or not isnumeric(displayItemID) then displayItemID = 0
	if isnull(displayBrandID) or len(displayBrandID) < 1 or not isnumeric(displayBrandID) then displayBrandID = 0
	if isnull(displayModelID) or len(displayModelID) < 1 or not isnumeric(displayModelID) then displayModelID = 0
	if isnull(displayCategoryID) or len(displayCategoryID) < 1 or not isnumeric(displayCategoryID) then displayCategoryID = 0
	
	searchResults = 0
	
	sql = "execute sp_mobileBrowser '" & userBrowser & "'"
	session("errorSQL") = sql
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.open sql, oConn, 3, 3
	
	browserID = rs("browserID")
	
	itemID = 0
	curPage = "Home"
	userMsg = ""
	ajaxRedir = ""
	
	if len(session("userMsg")) > 0 then
		userMsg = session("userMsg")
		session("userMsg") = null
	end if
	
	if itemID = 0 then
		sql = "select a.modelID, b.modelName from we_mobileBrowsers a join we_models b on a.modelID = b.modelID where a.userBrowser = '" & userBrowser & "'"
		session("errorSQL") = sql
		Set phoneIdRS = Server.CreateObject("ADODB.Recordset")
		phoneIdRS.open sql, oConn, 3, 3
		
		if phoneIdRS.EOF then
			'nothing yet
		else
			if isnull(phoneIdRS("modelID")) then
				if instr(userBrowser,"BlackBerry") > 0 then
					browserID = replace(userBrowser,"BlackBerry","")
					browserID = left(browserID,instr(browserID,"/") - 1)
					sql = "execute sp_updateMobileBrowser 14,'" & browserID & "','" & userBrowser & "'"
					session("errorSQL") = sql
					Set phoneIdRS = Server.CreateObject("ADODB.Recordset")
					phoneIdRS.open sql, oConn, 3, 3
					if not isnull(phoneIdRS("modelID")) then
						itemID = phoneIdRS("modelID")
					end if
					modelName = "BlackBerry " & browserID
				elseif instr(userBrowser,"Android") > 0 then
					browserArray = split(userBrowser,";")
					browserID = trim(left(browserArray(4),instr(browserArray(4),"Build") - 1))
					sql = "execute sp_updateMobileBrowser 0,'" & browserID & "','" & userBrowser & "'"
					session("errorSQL") = sql
					Set phoneIdRS = Server.CreateObject("ADODB.Recordset")
					phoneIdRS.open sql, oConn, 3, 3
					if not isnull(phoneIdRS("modelID")) then
						itemID = phoneIdRS("modelID")
						modelName = phoneIdRS("modelName")
					end if
				end if
			else
				itemID = phoneIdRS("modelID")
				modelName = phoneIdRS("modelName")
			end if
		end if
	end if
	
	searchString = SQLquote(lcase(request.QueryString("searchString")))
	login = SQLquote(request.Form("login"))
	loginPW = SQLquote(request.Form("loginPW"))
	logout = SQLquote(request.QueryString("logout"))
	reset = SQLquote(lcase(request.QueryString("r")))
	purchaseQty = SQLquote(request.Form("qty"))
	purchaseItemID = SQLquote(request.Form("itemID"))
	
	if isnull(searchString) or len(searchString) < 1 then searchString = ""
	if isnull(login) or len(login) < 1 then login = ""
	if isnull(loginPW) or len(loginPW) < 1 then loginPW = ""
	if isnull(logout) or len(logout) < 1 or not isnumeric(logout) then logout = 0
	if isnull(reset) or len(reset) < 1 or not isnumeric(reset) then reset = 0
	if isnull(purchaseQty) or len(purchaseQty) < 1 or not isnumeric(purchaseQty) then purchaseQty = 0
	if isnull(purchaseItemID) or len(purchaseItemID) < 1 or not isnumeric(purchaseItemID) then purchaseItemID = 0
	
	if len(request.Cookies("mySession")) > 0 then
		useSessionID = request.Cookies("mySession")
	else
		response.Cookies("mySession") = session.SessionID
		response.Cookies("mySession").expires = date + 30
		useSessionID = session.SessionID
	end if
	
	if login <> "" then
		sql = "select accountid, fname, lname, pword from we_accounts where email = '" & login & "'"
		session("errorSQL") = sql
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 3, 3
		if rs.EOF then
			userMsg = "NO ACCOUNT EXISTS WITH THAT EMAIL ADDRESS"
			ajaxRedir = "/ajax/mobileContent.asp?login=1"
		else
			if loginPW <> rs("pword") then
				userMsg = "PASSWORD DOES NOT MATCH THE EMAIL ADDRESS"
				ajaxRedir = "/ajax/mobileContent.asp?login=1&email=" & login
			else
				if instr(rs("fname")," ") > 0 then
					session("customer") = left(rs("fname"),instr(rs("fname")," "))
				else
					session("customer") = rs("fname")
				end if
				session("acctID") = rs("accountid")
			end if
		end if
	elseif logout = 1 then
		session.Contents.Remove("customer")
		session.Contents.Remove("acctID")
	end if
	
	if isnull(session("acctID")) or len(session("acctID")) < 1 then session("acctID") = 0
	
	if purchaseQty > 0 then
		sql = "select * from shoppingCart where itemID = " & purchaseItemID & " and (sessionID = " & useSessionID & " or accountID = " & session("acctID") & ")"
		session("errorSQL") = sql
		Set cartRS = Server.CreateObject("ADODB.Recordset")
		cartRS.open sql, oConn, 3, 3
		
		if cartRS.EOF then
			if session("acctID") > 0 then
				sql = "insert into shoppingCart (store,sessionID,accountID,itemID,qty,dateEntd,mobileOrder,ipAddress) values(0," & useSessionID & "," & session("acctID") & "," & purchaseItemID & "," & purchaseQty & ",'" & now & "',1,'" & request.ServerVariables("REMOTE_ADDR") & "')"
			else
				sql = "insert into shoppingCart (store,sessionID,itemID,qty,dateEntd,mobileOrder,ipAddress) values(0," & useSessionID & "," & purchaseItemID & "," & purchaseQty & ",'" & now & "',1,'" & request.ServerVariables("REMOTE_ADDR") & "')"
			end if
		else
			sql = "update shoppingCart set qty = qty + " & purchaseQty & " where itemID = " & purchaseItemID & " and (sessionID = " & useSessionID & " or accountID = " & session("acctID") & ")"
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	if searchString <> "" then
		searchResults = 1
		searchArray = split(searchString," ")
		search4 = ""
		for i = 0 to ubound(searchArray)
			if search4 = "" then
				search4 = searchArray(i)
			else
				search4 = search4 & "%" & searchArray(i)
			end if
		next
		sql = "select top 101 b.carrierCode, a.itemID, a.itemPic, a.itemDesc from we_items a left join we_models b on a.modelID = b.modelID where a.hideLive = 0 and (select top 1 inv_qty from we_items where partNumber = a.partNumber and master = 1) > 0 and price_our is not null and price_our > 0 and itemDesc like '%" & search4 & "%' order by a.numberOfSales desc"
		session("errorSQL") = sql
		Set searchRS = oConn.execute(sql)
		
		if searchRS.EOF then
			searchString = replace(searchString,"tmobile","t-mobile")
			searchString = replace(searchString,"att","at&t")
			searchString = replace(searchString,"us cellular","u.s. cellular")
			
			searchArray = split(searchString," ")
			brandID = 0
			typeID = 0
			modelID = 0
			details = ""
			for i = 0 to ubound(searchArray)
				sql = "select * from we_brands where brandName like '%" & searchArray(i) & "%'"
				session("errorSQL") = sql
				Set rs = oConn.execute(sql)
				
				if rs.EOF or brandID > 0 then
					sql = "select * from we_types where typeName like '%" & searchArray(i) & "%'"
					session("errorSQL") = sql
					Set rs = oConn.execute(sql)
					
					if rs.EOF or typeID > 0 then
						sql = "select modelID from we_models where modelName like '%" & searchArray(i) & "%'"
						session("errorSQL") = sql
						Set rs = oConn.execute(sql)
						
						if rs.EOF or modelID > 0 then
							details = details & " " & searchArray(i)
						else
							if rs.recordcount > 1 and i < ubound(searchArray) then
								sql = "select modelID from we_models where modelName like '%" & searchArray(i) & " " & searchArray(i+1) & "%'"
								session("errorSQL") = sql
								Set rs2 = oConn.execute(sql)
								
								if rs2.EOF then
									modelID = rs("modelID")
								else
									i = i + 1
									modelID = rs2("modelID")
								end if
							else
								modelID = rs("modelID")
							end if
						end if
					else
						if rs.recordcount > 1 and i < ubound(searchArray) then
							sql = "select typeID from we_types where typeName like '%" & searchArray(i) & " " & searchArray(i+1) & "%'"
							session("errorSQL") = sql
							Set rs2 = oConn.execute(sql)
							
							if rs2.EOF then
								typeID = rs("typeID")
							else
								i = i + 1
								typeID = rs2("typeID")
							end if
						else
							typeID = rs("typeID")
						end if
					end if
				else
					if rs.recordcount > 1 and i < ubound(searchArray) then
						sql = "select brandID from we_brands where brandName like '%" & searchArray(i) & " " & searchArray(i+1) & "%'"
						session("errorSQL") = sql
						Set rs2 = oConn.execute(sql)
						
						if rs2.EOF then
							brand = rs("brandID")
						else
							i = i + 1
							brand = rs2("brandID")
						end if
					else
						brand = rs("brandID")
					end if
				end if
			next
			
			myWhere = ""
			if brandID > 0 then myWhere = " and a.brandID = " & brandID
			if typeID > 0 then
				if myWhere = "" then myWhere = " and a.typeID = " & typeID else myWhere = myWhere & " and a.typeID = " & typeID
			end if
			if modelID > 0 then
				if myWhere = "" then myWhere = " and a.modelID = " & modelID else myWhere = myWhere & " and a.modelID = " & modelID
			end if
			if details <> "" then
				detailArray = split(details," ")
				for x = 0 to ubound(detailArray)
					if myWhere = "" then
						myWhere = " and (a.itemDesc like '%" & modelID & "%'"
					elseif x > 0 then
						myWhere = myWhere & " or a.itemDesc like '%" & modelID & "%'"
					else
						myWhere = myWhere & " and (a.itemDesc like '%" & modelID & "%'"
					end if
				next
				myWhere = myWhere & ")"
			end if
			
			sql = "select top 10 itemID, itemDesc, typeID from we_items" & myWhere & " order by numberOfSales desc"
			sql = "select top 101 b.carrierCode, a.itemID, a.itemPic, a.itemDesc from we_items a left join we_models b on a.modelID = b.modelID where a.hideLive = 0 and price_our is not null and price_our > 0" & myWhere & " order by a.numberOfSales desc"
			session("errorSQL") = sql
			Set searchRS = oConn.execute(sql)
		end if
	end if
	
	if session("acctID") > 0 then
		sql = "select sum(qty) as cartItems from shoppingCart where sessionID = " & useSessionID & " or accountID = " & session("acctID")
	else
		sql = "select sum(qty) as cartItems from shoppingCart where sessionID = " & useSessionID
	end if
	session("errorSQL") = sql
	Set cartRS = oConn.execute(sql)
	cartItems = cartRS("cartItems")
	cartRS = null
	
	breadCrumbs = "Main Menu"
%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html>
<head>
	<title>Wireless Emporium Mobile Site - <%=curPage%></title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <script type="text/javascript" src="/includes/js/acCombo.js"></script>
	<script type="text/javascript" src="/includes/js/autocomplete-min.js"></script>
    <style type="text/css">
	body{font:13px/1.231 arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;margin:0;padding:0;}
	select,input,button,textarea,button{font:99% arial,helvetica,clean,sans-serif;}
	table{font-size:inherit;font:100%;}
	pre,code,kbd,samp,tt{font-family:monospace;*font-size:108%;line-height:100%;}
	.yui-skin-sam .yui-ac{position:relative;font-family:arial;font-size:100%;}
	.yui-skin-sam .yui-ac-input{position:absolute;width:100%;}
	.yui-skin-sam .yui-ac-container{position:absolute;top:1.6em;width:100%; font-size:20px;}
	.yui-skin-sam .yui-ac-content{position:absolute;width:100%;border:1px solid #808080;background:#fff;overflow:hidden;z-index:9050;}
	.yui-skin-sam .yui-ac-shadow{position:absolute;margin:.3em;width:100%;background:#000;-moz-opacity:.10;opacity:.10;filter:alpha(opacity=10);z-index:9049;}
	.yui-skin-sam .yui-ac iframe{opacity:0;filter:alpha(opacity=0);padding-right:.3em;padding-bottom:.3em;}
	.yui-skin-sam .yui-ac-content ul{margin:0;padding:0;width:100%;}
	.yui-skin-sam .yui-ac-content li{margin:0;padding:2px 5px;cursor:default;white-space:nowrap;list-style:none;zoom:1;}
	.yui-skin-sam .yui-ac-content li.yui-ac-prehighlight{background:#B3D4FF;}
	.yui-skin-sam .yui-ac-content li.yui-ac-highlight{background:#cccccc;color:#FFF;}
	#myAutoComplete {
	    width:15em; /* set width here or else widget will expand to fit its container */
    	padding-bottom:2em;
	}
	a { color:#ff6633; text-decoration:none; }
	a:hover { color:#ff6633; text-decoration:underline; }
    </style>
</head>
<body class="yui-skin-sam" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" style="width:100%; text-align:center">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr><td align="left" style="border-top:2px solid #666666; border-bottom:2px solid #666666; padding-top:0px;"><a href="/?r=1"><img src="/images/mobile/WE_logo_r2.png" border="0" /></a></td></tr>
    <tr bgcolor="#666666">
    	<td>
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td align="left" style="padding-left:10px;"><img src="/images/mobile/cart.jpg" border="0" /></td>
                    <td id="viewCart" align="left" width="100%" style="font-weight:bold; color:#FFF; padding-left:5px;"<% if cartItems > 0 then %> onclick="ajax('/ajax/mobileCheckout.asp','mobileContent',1)"<% end if %>>
                    	<% if cartItems > 0 then %>
                        <%=cartItems%> Item(s)
                        <% else %>
                        No Items
                        <% end if %>
                    </td>
                    <td align="right" style="padding-right:10px; color:#FFF" nowrap="nowrap">
                    	<%
						noLogin = 1
						if noLogin = 1 then
						%>
                        <a href="tel:18003051106" style="color:#FFF;">1.800.305.1106</a>
                        <%
						else
							if isnull(session("customer")) or len(session("customer")) < 1 then
						%>
                        <a href="https://m.wirelessemporium.com/mobile/login.asp?mm=1" style="color:#FFF; font-weight:bold;">Login</a>
                        <% 	else %>
                        Welcome <%=session("customer")%>&nbsp;(<a href="http://m.wirelessemporium.com/mobile/?logout=1" style="color:#FFF;">Logout</a>)
                        <% 	end if
						end if
						%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td align="left" style="background-image:url(/images/mobile/searchBG.gif); background-repeat:repeat-x">
            <form name="searchForm" method="get" action="/mobile/" onsubmit="document.getElementById('mobileContent').innerHTML='<br><br>Searching For Products...<br><br>'">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td><img src="/images/blank.gif" border="0" width="5" height="30" /></td>
                    <td valign="top" style="padding-top:5px;">
                    	<div id="myAutoComplete">
		                <input id="myInput" name="searchString" type="text" value="Find Your Product" onfocus="testContent(this.value)">
        		        <div id="myContainer"></div>
                        </div>
                    </td>
                    <td><img src="/images/blank.gif" border="0" width="10" height="30" /></td>
                    <td align="right" valign="top" style="padding-top:6px; padding-right:8px;"><input type="image" src="/images/mobile/buttons/button_search.png" align="absmiddle" border="0" alt="GO"></td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <% if userMsg <> "" then %>
    <tr bgcolor="#FF0000"><td align="center" style="font-weight:bold; color:#FFF;"><%=userMsg%></td></tr>
    <% end if %>
    <tr>
    	<td id="mobileContent" align="center">
        	<% if searchResults = 1 or (displayItemID = 0 and displayBrandID = 0 and (isnull(session("prevPage1")) or len(session("prevPage1")) < 1)) then %>
				<% if searchResults = 0 then %>
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr bgcolor="#ebebeb"><td align="left" style="color:#000000; font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=breadCrumbs%></td></tr>
                <tr>
                	<td align="center" width="100%">
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                        	<% if itemID > 0 then %>
                            <tr onclick="ajax('/ajax/mobileContent.asp?p=<%=itemID%>','mobileContent',1)">
                            	<td align="right" style="padding-top:10px; padding-left:20px;"><img src="/images/mobile/buttons/we_m_button_small.png" border="0" /></td>
                                <td width="100%" align="left" style="padding-top:10px;">
                                	<table border="0" cellpadding="0" cellspacing="0">
                                    	<tr><td align="left" style="font-size:18px; color:#ff6633; padding-left:20px;"><%=modelName%></td></tr>
                                        <tr><td align="left" style="color:#666; font-size:10px; padding-left:20px;">Accessories For Your Phone</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td colspan="2" style="border-bottom:1px solid #cccccc;"></td></tr>
                            <% end if %>
                            <tr onclick="ajax('/ajax/mobileContent.asp?mm=1','mobileContent',1)">
                            	<td align="right" style="padding-top:10px;"><img src="/images/mobile/buttons/we_m_button_small.png" border="0" /></td>
                                <td align="left" style="padding-top:10px;">
                                	<table border="0" cellpadding="0" cellspacing="0">
                                    	<tr><td align="left" style="font-size:18px; color:#ff6633; padding-left:20px;">BRANDS</td></tr>
                                        <tr><td align="left" style="color:#666; font-size:10px; padding-left:20px;">Apple | BlackBerry | HTC | more &gt;&gt;</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td colspan="2" style="border-bottom:1px solid #cccccc;"></td></tr>
                            <tr onclick="ajax('/ajax/mobileContent.asp?mm=2','mobileContent',1)">
                            	<td align="right" style="padding-top:10px;"><img src="/images/mobile/buttons/we_m_button_small.png" border="0" /></td>
                                <td align="left" style="padding-top:10px;">
                                	<table border="0" cellpadding="0" cellspacing="0">
                                    	<tr><td align="left" style="font-size:18px; color:#ff6633; padding-left:20px;">CARRIERS</td></tr>
                                        <tr><td align="left" style="color:#666; font-size:10px; padding-left:20px;">AT&amp;T | Sprint | T-Mobile | Verizon | more &gt;&gt;</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td colspan="2" style="border-bottom:1px solid #cccccc;"></td></tr>
                            <tr onclick="ajax('/ajax/mobileContent.asp?mm=3','mobileContent',1)">
                            	<td align="right" style="padding-top:10px;"><img src="/images/mobile/buttons/we_m_button_small.png" border="0" /></td>
                                <td align="left" style="padding-top:10px;">
                                	<table border="0" cellpadding="0" cellspacing="0">
                                    	<tr><td align="left" style="font-size:18px; color:#ff6633; padding-left:20px;">ACCESSORY TYPE</td></tr>
                                        <tr><td align="left" style="color:#666; font-size:10px; padding-left:20px;">Faceplates | Chargers | Cases | more &gt;&gt;</td></tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
				<%
                else
                    baseCrumb = "<a style='font-weight:bold; font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#ff6633' onclick=ajax('/ajax/mobileContent.asp?r=1','mobileContent',1)>Main Menu&nbsp;&gt;</a>"
                    
                    if searchRS.EOF then
                %>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family:Arial, Helvetica, sans-serif;">
                <tr bgcolor="#ebebeb"><td align="left" style="font-weight:bold; font-size:14px; color:#000000; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%></td></tr>
                <tr><td align="center" style="padding-top:20px; padding-bottom:20px; color:#F00; font-weight:bold;">No Products Found<br />Please Try Another Search Or Return To Main Menu</td></tr>
            </table>
				<%	else %>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family:Arial, Helvetica, sans-serif;">
                <tr bgcolor="#ebebeb"><td align="left" style="font-weight:bold; font-size:14px; color:#000000; padding-left:5px; padding-bottom:2px; padding-top:2px;"><%=baseCrumb%></td></tr>
                <tr>
                	<td align="center">
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            				<%
							searchLap = 0
							do while not searchRS.EOF
								searchLap = searchLap + 1
							%>
            				<tr onclick="ajax('/ajax/mobileContent.asp?i=<%=searchRS("itemID")%>','mobileContent',1)">
                            	<td style="padding-bottom:5px; padding-top:5px; border-bottom:1px solid #cccccc;"><img src="/productPics/thumb/<%=searchRS("itemPic")%>" border="0" /></td>
                                <td style="padding-bottom:5px; padding-top:5px; border-bottom:1px solid #cccccc;" valign="top" width="100%"><%=searchRS("itemDesc")%></td>
                            </tr>
				            <%
								searchRS.movenext
								if searchLap = 100 and not searchRS.EOF then
							%>
                            <tr><td colspan="2" align="center" style="font-weight:bold; font-size:16px; color:#F00; padding:10px 0px 10px 0px;">Too many search results<br />Please be more specific</td></tr>
                            <%
									exit do
								end if
							loop
							%>
            			</table>
                    </td>
                </tr>
            </table>
            <%
					end if
				end if
			end if
			%>
        </td>
    </tr>
    <tr bgcolor="#ebebeb">
    	<td>
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td nowrap="nowrap" style="padding-left:5px;"><a onclick="ajax('/ajax/mobileContent.asp?contactUs=1','mobileContent',1)" style="text-decoration:underline;">Contact Us</a></td>
                    <td style="color:#666; font-weight:bold; padding-left:10px; padding-right:10px;">|</td>
                    <!--<td nowrap="nowrap"><a style="text-decoration:underline;">RSS</a></td>
                    <td style="color:#666; font-weight:bold; padding-left:10px; padding-right:10px;">|</td>-->
                    <td nowrap="nowrap"><a href="/?ms=full" style="text-decoration:underline;">Full Site.</a></td>
                	<td align="right" width="100%"><img src="/images/mobile/we_logo_little.png" border="0" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td align="center">
        	<div style="width:300px; height:1px;">
    	        <div id="banner2"><img src="/images/mobile/banners/90-banner2.png" border="0" width="300" height="85" /></div>
            </div>
        </td>
    </tr>
</table>
<div id="testZone"></div>
</body>
</html>
<%
	oConn.Close
	set oConn = nothing
%>
<script language="javascript" src="/includes/js/productKeywords.js"></script>
<script type="text/javascript">
	var curAd = 1
	//setTimeout("rotateAds()",5000)
	function rotateAds() {
		oldAd = curAd
		curAd++
		if (curAd > 2) curAd = 1
		document.getElementById("banner" + oldAd).style.display = 'none'
		document.getElementById("banner" + curAd).style.display = ''
		setTimeout("rotateAds()",5000)
	}
	
	function testContent(val) {
		if (val == "Find Your Product") {
			document.searchForm.searchString.value = ""
		}
	}
	
	YAHOO.example.BasicLocal = function() {
		var oDS = new YAHOO.util.LocalDataSource(YAHOO.example.Data.arrayProducts);
		var oAC = new YAHOO.widget.AutoComplete("myInput", "myContainer", oDS);
		oAC.prehighlightClassName = "yui-ac-prehighlight";
		oAC.useShadow = false;
		oAC.maxResultsDisplayed = 3;
		
		return {
			oDS: oDS,
			oAC: oAC
		};
	}();
	
	function ajax(newURL,rLoc,loading) {
		if (newURL.indexOf("?") > 0) {
			newURL = newURL + '&ajaxID=' + Math.floor(Math.random()*999999)
		}
		else {
			newURL = newURL + '?ajaxID=' + Math.floor(Math.random()*999999)
		}
		if (loading == 1) {
			document.getElementById(rLoc).innerHTML = "<table width='100%'><tr><td align='center' style=background-image:url(/images/mobile/loading.gif);background-repeat:no-repeat;background-position:center;><img src='/images/blank.gif' border='0' width='1' height='60' /><br /><hr color='#ff6633' /><br /><img src='/images/blank.gif' border='0' width='1' height='40' /><br /><div align='center' style='font-weight:bold; font-size:18px;'>Loading Content...</div><hr color='#ff6633' /><br /><img src='/images/blank.gif' border='0' width='1' height='100' /></td></tr></table>"
		}
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						window.location = "/mobile/?r=1"
					}
					else if(document.getElementById(rLoc)) {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				}
				else {
					//alert("Error performing action")
				}
			}
		};
		httpRequest.send(null);
	}
	
	<%
	if displayItemID > 0 then
	%>
	ajax('/ajax/mobileContent.asp?i=<%=displayItemID%>','mobileContent',1)
	<%
	elseif displayCategoryID > 0 then
	%>
	ajax('/ajax/mobileContent.asp?p=<%=displayModelID%>&t=<%=displayCategoryID%>','mobileContent',1)
	<%
	elseif displayModelID > 0 then
	%>
	ajax('/ajax/mobileContent.asp?p=<%=displayModelID%>','mobileContent',1)
	<%
	elseif purchaseQty > 0 then
	%>
	ajax('/ajax/mobileCheckout.asp','mobileContent',1)
	<%
	elseif searchString = "" then
		session.Contents.Remove("curPage")
		if not isnull(session("prevPage1")) and len(session("prevPage1")) > 0 then
			session("curPage") = session("prevPage1") & "&backBttn=1"
			if isnull(session("prevPage2")) or len(session("prevPage2")) < 1 then session.Contents.Remove("prevPage1") else session("prevPage1") = replace(session("prevPage2"),"&backBttn=1","")
			if isnull(session("prevPage3")) or len(session("prevPage3")) < 1 then session.Contents.Remove("prevPage2") else session("prevPage2") = replace(session("prevPage3"),"&backBttn=1","")
			session.Contents.Remove("prevPage3")		
	%>
	ajax('<%=session("curPage")%>','mobileContent',1)
	<% 	elseif ajaxRedir <> "" then %>
	ajax('<%=ajaxRedir%>','mobileContent',1)
	<% 	end if %>
	<% end if %>
	function validateEmail(email) {
		var splitted = email.match("^(.+)@(.+)$");
		if (splitted == null) return false;
		if (splitted[1] != null) {
			var regexp_user=/^\"?[\w-_\.]*\"?$/;
			if (splitted[1].match(regexp_user) == null) return false;
		}
		if (splitted[2] != null) {
			var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
			if (splitted[2].match(regexp_domain) == null) {
				var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
				if (splitted[2].match(regexp_ip) == null) return false;
			}
			return true;
		}
		return false;
	}
	function validateContactUs() {
		if (document.contactus.name.value.length==0) {
			alert("Please Enter Your Name");
			return false;
		}
		if (document.contactus.subject.value=="Select here...") {
			alert("Please Select the Type of Inquiry");
			return false;
		}
		if (document.contactus.question.value.length==0) {
			alert("Please Enter Question");
			return false;
		}
		if (document.contactus.email.value.length==0) {
			alert("Please Enter Email Address");
			return false;
		}
		if (!validateEmail(document.contactus.email.value)) { 
			alert("Please Enter a Valid Email Address");
			return false;
		}
		document.contactus.submit()
		return false;
	}
	
	function resortProducts(sortBy,productID,productType,moreProduct) {
		ajax('/ajax/mobileContent.asp?p=' + productID + '&t=' + productType + '&mp=' + moreProduct + '&sb=' + sortBy,'mobileContent',1)
	}
</script>
<!-- Google Analytics Start -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1311669-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Google Analytics End -->