<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
	response.buffer = true
	response.expires = now+1
	response.ExpiresAbsolute = now+1
	response.CacheControl = "private"
%>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
  <title>Wireless Emporium Mobile Site - Home</title>
</head>
<body>
	<style type="text/css">
        body { font-family:Verdana, Geneva, sans-serif; font-size:12px; margin:0px; padding:0px; }
        #freeShipTop { padding:5px; width:100%; text-align:center; color:#FFF; font-size:12px; background-color:#696969; font-weight:bold; }
        #freeText { color:#fe9b35; }
        #logoRow { height:57px; width:100%; text-align:center; }
        #logoContainer { height:57px; width:320px; margin-left:auto; margin-right:auto; }
        #weLogo { float:left; border-right:1px dotted #CCC; height:57px; }
        #cartIcon { float:left; position:relative; height:57px; }
        #inCart { position:absolute; bottom:0px; width:100%; text-align:center; color:#9a9a9a; font-size:12px; padding-bottom:5px; }
        #searchBar { height:40px; width:100%; background:-moz-linear-gradient(top,#fe9900,#ff6701); }
        #searchContainer { height:40px; width:320px; margin-left:auto; margin-right:auto; }
        #searchInput { float:left; margin-top:5px; padding:7px 0px 0px 5px; height:23px; width:260px; background-image:url(/images/mobile/weMobileSearchBackground.gif); }
        #searchButton { float:left; padding:3px 0px 0px 2px; }
        #searchTextBox { color:#999; border:0px; width:250px; }
        #breadCrumbs { height:40px; background:-moz-linear-gradient(top,#fff,#ececec); width:100%; }
        #breadCrumbText { float:left; padding:8px 0px 0px 10px; }
        #breadCrumbLink { color:#006599; font-weight:bold; text-decoration:none; }
        #breadCrumbBreak { float:left; padding-left:10px; }
        #adSpot { width:100%; text-align:center; }
        #customerService { height:30px; background:-moz-linear-gradient(top,#fff,#ececec); width:100%; border-bottom:1px solid #ccc; text-align:center; color:#666; padding-top:10px; font-weight:bold; }
        #homeSelectBox { background-image:url(/images/mobile/selectButton.jpg); background-repeat:no-repeat; background-position:center; text-align:center; margin-top:15px; width:100%; height:52px; }
        #homeShopNow { text-align:center; margin-top:15px; width:100%; height:52px; }
        #selectBoxContainer { width:280px; overflow:hidden; margin-left:auto; margin-right:auto; }
        #actualSelectBox { width:302px; height:50px; padding-top:12px; border:0px; color:#0e6f9c; background-color:transparent; font-weight:bold; font-size:14px; text-align:center; }
        #actualSelectBox2 { width:302px; height:50px; padding-top:12px; border:0px; color:#999; background-color:transparent; font-weight:bold; font-size:14px; text-align:center; }
        #basicForm { margin:0px; padding:0px; }
        #lowerBox { background-image:url(/images/mobile/grayNoiseBackground.gif); width:100%; height:209px; margin-top:20px; }
        #socializeText { color:#FFF; font-weight:bold; font-size:16px; width:100%; text-align:center; padding-top:10px; }
    </style>
    <div id="freeShipTop">Get fast, <span id="freeText">FREE</span> shipping on all orders!</div>
    <div id="logoRow">
        <div id="logoContainer">
            <div id="weLogo"><a href="/"><img src="/images/mobile/mobileWeLogo.jpg" border="0" width="261" height="57" /></a></div>
            <div id="cartIcon">
                <img src="/images/mobile/cartIcon.gif" border="0" width="58" height="57" />
                <div id="inCart">1 Item</div>
            </div>
        </div>
    </div>
    <div id="searchBar">
        <div id="searchContainer">
            <form id="basicForm" action="/search.asp" method="post">
            <div id="searchInput"><input id="searchTextBox" type="text" name="search" value="Search by Keyword(s) or Item#" /></div>
            <div id="searchButton"><input type="image" src="/images/mobile/weMobileSearchButton.jpg" border="0" /></div>
            </form>
        </div>
    </div>
    <div id="breadCrumbs">
        <div id="breadCrumbText"><a href="/" id="breadCrumbLink">Home</a></div>
        <div id="breadCrumbBreak"><img src="/images/mobile/breadCrumbBreak.gif" border="0" /></div>
    </div>
    <%
        sql = "select typeID, typeName from we_types order by typeName"
        session("errorSQL") = sql
        set typeRS = oConn.execute(sql)
        
        sql = "select brandID, brandName from we_brands order by brandName"
        session("errorSQL") = sql
        set brandRS = oConn.execute(sql)
    %>
    <div id="adSpot"><img src="/images/mobile/mobileAd.jpg" border="0" /></div>
    <div id="customerService">Customer Service <%=mobilePhoneNumber%></div>
    <form id="basicForm" name="prodSelectForm">
    <div id="homeSelectBox">
        <div id="selectBoxContainer">
            <select id="actualSelectBox" name="typeID">
                <option value="">Choose A Product Type</option>
                <%
                do while not typeRS.EOF
                %>
                <option value="<%=typeRS("typeID")%>"><%=typeRS("typeName")%></option>
                <%
                    typeRS.movenext
                loop
                %>
            </select>
        </div>
    </div>
    <div id="homeSelectBox">
        <div id="selectBoxContainer">
            <select id="actualSelectBox" name="brandID">
                <option value="">Choose A Device Brand</option>
                <%
                do while not brandRS.EOF
                %>
                <option value="<%=brandRS("brandID")%>"><%=brandRS("brandName")%></option>
                <%
                    brandRS.movenext
                loop
                %>
            </select>
        </div>
    </div>
    <div id="homeSelectBox">
        <div id="selectBoxContainer">
            <select id="actualSelectBox2" name="modelID" disabled="disabled">
                <option value="">Choose A Device Model</option>
            </select>
        </div>
    </div>
    <div id="homeShopNow"><input alt="Show Now!" type="image" src="/images/mobile/buttons/shopNow.jpg" border="0" /></div>
    </form>
    <div id="lowerBox">
        <div id="socializeText">Want to socialize with us?</div>
    </div>
</body>
</html>