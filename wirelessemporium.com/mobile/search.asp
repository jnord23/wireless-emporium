<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "BMC"
%>
<!--#include virtual="/template/top.asp"-->
<%	
	dim myKeywords : myKeywords = prepStr(request.Form("myKeywords"))
	dim ogKeywords : ogKeywords = myKeywords
	dim performSearch : performSearch = 0
	
	brandID = 0
	modelID = 0
	typeID = 0
	foundSomething = 0
	if myKeywords <> "" then
		sql = "select brandID from we_brands where brandName = '" & myKeywords & "'"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		if not rs.EOF then
			brandID = rs("brandID")
			myKeywords = ""
		end if
		
		if myKeywords <> "" then
			sql = "select modelID from we_models where modelName = '" & myKeywords & "'"
			session("errorSQL") = sql
			set rs = oConn.execute(sql)
			
			if not rs.EOF then
				modelID = rs("modelID")
				myKeywords = ""
			end if
		end if
		
		if myKeywords <> "" then
			sql = "select typeID from we_types where typeName like '%" & myKeywords & "%' or typeName_WE like '%" & myKeywords & "%'"
			session("errorSQL") = sql
			set rs = oConn.execute(sql)
			
			if not rs.EOF then
				typeID = rs("typeID")
				myKeywords = ""
			end if
		end if
		
		if instr(myKeywords," ") > 0 then
			keyArray = split(myKeywords," ")
			for i = 0 to ubound(keyArray)
				if prepStr(keyArray(i)) <> "" then
					sql = "select brandID from we_brands where brandName = '" & keyArray(i) & "'"
					session("errorSQL") = sql
					set rs = oConn.execute(sql)
					
					if not rs.EOF then
						foundSomething = 1
						brandName = keyArray(i)
						brandID = rs("brandID")
						for x = i to ubound(keyArray)
							useBrandName = brandName & " " & keyArray(x)
							sql = "select brandID from we_brands where brandName = '" & useBrandName & "'"
							session("errorSQL") = sql
							set rs = oConn.execute(sql)
							if not rs.EOF then
								brandName = useBrandName
								brandID = rs("brandID")
							end if
						next
						myKeywords = replace(myKeywords,brandName,"")
						i = ubound(keyArray)
					end if
				end if
			next
			keyArray = split(myKeywords," ")
			for i = 0 to ubound(keyArray)
				if prepStr(keyArray(i)) <> "" then
					sql = "select modelID from we_models where modelName = '" & keyArray(i) & "'"
					session("errorSQL") = sql
					set rs = oConn.execute(sql)
					
					if not rs.EOF then
						foundSomething = 2
						modelName = keyArray(i)
						modelID = rs("modelID")
						for x = i to ubound(keyArray)
							useModelName = modelName & " " & keyArray(x)
							sql = "select modelID from we_models where modelName = '" & useModelName & "'"
							session("errorSQL") = sql
							set rs = oConn.execute(sql)
							if not rs.EOF then
								modelName = useModelName
								modelID = rs("modelID")
							end if
						next
						myKeywords = replace(myKeywords,modelName,"")
						i = ubound(keyArray)
					end if
				end if
			next
			keyArray = split(myKeywords," ")
			for i = 0 to ubound(keyArray)
				if prepStr(keyArray(i)) <> "" then
					sql = "select typeID from we_types where typeName like '%" & keyArray(i) & "%' or typeName_WE like '%" & keyArray(i) & "%'"
					session("errorSQL") = sql
					set rs = oConn.execute(sql)
					
					if not rs.EOF then
						foundSomething = 3
						typeID = rs("typeID")
						myKeywords = replace(myKeywords,keyArray(i),"")
						i = ubound(keyArray)
					end if
				end if
			next
		end if
		
		sql =	"select top 50 a.itemID, b.itemPic, b.itemDesc, c.brandName, d.modelName, b.price_our, 0 as avgRating " &_
				"from we_ItemsSiteReady a " &_
					"left join we_Items b on a.itemID = b.itemID " &_
					"left join we_brands c on b.brandID = c.brandID " &_
					"left join we_models d on b.modelID = d.modelID "
		if myKeywords <> "" then
				sql =	sql & "where b.itemDesc like '%" & trim(myKeywords) & "%' and b.hideLive = 0 and b.ghost = 0 and (a.inv_qty > 0 or a.alwaysInStock = 1) or b.partNumber like '%" & trim(myKeywords) & "%'"
		else
				sql =	sql & "where b.hideLive = 0 and b.ghost = 0 and (a.inv_qty > 0 or a.alwaysInStock = 1) "
		end if
		if brandID > 0 then sql = replace(sql,"where","where b.brandID = " & brandID & " and")
		if modelID > 0 then sql = replace(sql,"where","where b.modelID = " & modelID & " and")
		if typeID > 0 then sql = replace(sql,"where","where b.typeID = " & typeID & " and")
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		performSearch = 1
		
		if rs.EOF then resultsFound = 0 else resultsFound = 1
		sql = "if (select count(*) from we_searchQueries where siteID = 0 and mobile = 1 and keywords = '" & prepStr(ogKeywords) & "') > 0 update we_searchQueries set searchAttempts = searchAttempts + 1, resultsFound = " & resultsFound & ", sqlCommand = '" & prepStr(sql) & "' where siteID = 0 and mobile = 1 and keywords = '" & prepStr(ogKeywords) & "' else insert into we_searchQueries (siteID,mobile,keywords,sqlCommand,resultsFound) values(0,1,'" & prepStr(ogKeywords) & "','" & prepStr(sql) & "'," & resultsFound & ")"
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
%>
<form id="basicForm" name="searchForm" action="/search.htm" method="post" onsubmit="return(searchClick())">
<div id="searchBoxRow">
	<div id="searchInput"><input id="searchTextBox" type="text" name="myKeywords" value="Search by Keyword(s) or Item#" onfocus="clearDefault()" /></div>
	<div id="searchSubmit"><input id="searchSubmitButton" type="submit" value="Search" /></div>
</div>
</form>
<%
	if performSearch = 1 then
%>
<div id="mainProductBox">
	<%
	prodCnt = 0
	if rs.EOF then
%>
	<div id="noProds">No Product Found<br />Please adjust your filters</div>
<%
	end if
	
	do while not rs.EOF
		prodCnt = prodCnt + 1
		itemID = rs("itemID")
		itemPic = rs("itemPic")
		itemDesc = prepStr(rs("itemDesc"))
		brandName = prepStr(rs("brandName"))
		modelName = prepStr(rs("modelName"))
		itemDesc = replace(itemDesc,brandName,"")
		itemDesc = trim(replace(itemDesc,modelName,""))
		itemPrice = formatCurrency(prepInt(rs("price_our")),2)
		avgRating = rs("avgRating")
		
		if prodCnt < 11 then
			useClass = "singleProdBox"
			useImg = "img"
		else
			useClass = "singleProdBox2"
			useImg = "tempimg"
		end if
	%>
    <a href="/product.asp?itemID=<%=itemID%>" style="text-decoration:none;" title="<%=itemDesc%>">
	<div id="singleProdBox_<%=prodCnt%>" class="<%=useClass%>">
    	<div id="prodBrandModel" title="Product for <%=brandName & " " & modelName%>"><%=brandName & " " & modelName%></div>
        <div id="prodPic"><<%=useImg%> src="/productpics/thumb/<%=itemPic%>" /></div>
        <div id="prodDesc"><%=itemDesc%></div>
        <div id="prodPrice"><%=itemPrice%></div>
        <% if prepInt(avgRating) > 0 then %>
        <div id="prodRating">
        	<%
			for i = 1 to 5
				if avgRating => i then useStar = "on" else useStar = "off"
				response.Write("<img src='/images/mobile/ratingStar_" & useStar & ".gif' border='0' />")
			next
			%>
        </div>
		<% else %>
        <div id="prodRating"></div>
        <% end if %>
    </div>
    </a>
    <%
		rs.movenext
	loop
	%>
</div>
	<%
	end if
	%>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">	
	function showFilterOptions() {
		if (document.getElementById("filterBoxes").className == "filterOptions") {
			document.getElementById("filterBoxes").setAttribute("class", "filterOptionsHidden")
		}
		else {
			document.getElementById("filterBoxes").setAttribute("class", "filterOptions")
		}
	}
	
	function removeTypeFilter() {
		document.prodSelectForm.filterSubtype.selectedIndex = 0
		document.prodSelectForm.submit()
	}
	
	function removePriceFilter() {
		document.prodSelectForm.filterPrice.selectedIndex = 0
		document.prodSelectForm.submit()
	}
</script>