<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "Basket"
	
	dim itemID : itemID = prepInt(request.Form("itemID"))
	dim qty : qty = prepInt(request.Form("qty"))
	dim acctID : acctID = prepInt(session("acctID"))
	
	if prepInt(request.Cookies("mySession")) = 0 then
		response.Cookies("mySession") = session.SessionID
		response.Cookies("mySession").expires = date + 30
		useSessionID = session.SessionID
		newID = useSessionID
	else
		useSessionID = prepInt(request.Cookies("mySession"))
		newID = 0
	end if
	
	if itemID > 0 then
		if qty = 0 then
			sql = "delete from shoppingCart where itemID = " & itemID & " and sessionID = " & useSessionID
		else
			sql =	"if (select count(*) from shoppingCart where itemID = " & itemID & " and sessionID = " & useSessionID & ") > 0 " &_
						"update shoppingCart " &_
						"set qty = " & qty & " " &_
						"where itemID = " & itemID & " and sessionID = " & useSessionID & " " &_
					"else " &_
						"insert into shoppingCart (" &_
							"store,sessionID,accountID,itemID,qty,mobileOrder" &_
						") values(" &_
							"0," & useSessionID & "," & acctID & "," & itemID & "," & qty & ",1" &_
						")"
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	sql =	"select c.totalItems, a.qty, b.itemID, b.itemPic, b.itemDesc, b.price_our " &_
			"from shoppingCart a " &_
				"left join we_Items b on a.itemID = b.itemID " &_
				"outer apply (" &_
					"select sum(qty) as totalItems, sessionID " &_
					"from shoppingCart " &_
					"where sessionID = " & useSessionID & " " &_
					"group by sessionID" &_
				") c " &_
			"where a.sessionID = " & useSessionID
	session("errorSQL") = sql
	Set rs = oConn.execute(sql)
	
	if rs.EOF then
		itemCnt = "0 Items"
	else
		if rs("totalItems") <> 1 then
			itemCnt = rs("totalItems") & " Items"
		else
			itemCnt = rs("totalItems") & " Item"
		end if
	end if
%>
<!--#include virtual="/template/top.asp"-->
<div id="qtyInCartRow">
   	<div id="qtyInCartText">You have <span id="orangeText"><%=itemCnt%></span> in your cart:</div>
</div>
<div id="productContainer">
	<%
	if rs.EOF then
	%>
    <div id="itemBox_noItems">You Currently Have<br />No Items In Your Cart</div>
    <%
	end if
	
	subtotal = 0
	do while not rs.EOF
		itemID = prepInt(rs("itemID"))
		itemPic = rs("itemPic")
		itemDesc = prepStr(rs("itemDesc"))
		qty = prepInt(rs("qty"))
		itemPrice = prepInt(rs("price_our"))
	%>
    <div id="itemBox">
    	<div id="itemPic" onclick="window.location='/product.htm?itemID=<%=itemID%>'"><img src="/productPics/thumb/<%=itemPic%>" border="0" /></div>
        <div id="itemDetails">
        	<div id="itemDesc" onclick="window.location='/product.htm?itemID=<%=itemID%>'"><%=itemDesc%></div>
            <form name="qtyAdjustForm_<%=itemID%>" method="post" id="basicForm">
            <div id="qtyDetails">
            	<div id="qtyText">Qty.</div>
                <div id="qtyValue">
                	<input type="text" id="qtyInput" name="qty" value="<%=qty%>" size="3" maxlength="3" />
                    <input type="hidden" name="itemID" value="<%=itemID%>" />
                </div>
                <div id="qtyButton"><input type="image" src="/images/mobile/buttons/updateQty.jpg" /></div>
            </div>
            </form>
            <% if qty = 1 then %>
            <div id="itemPrice">Item Price: <%=formatCurrency(itemPrice,2)%></div>
            <% else %>
            <div id="itemPriceSmall">Item Price: <%=formatCurrency(itemPrice,2)%></div>
            <div id="itemPrice">Item Total: <%=formatCurrency((itemPrice * qty),2)%></div>
            <% end if %>
            <div id="removeRow" onclick="removeItem(<%=itemID%>)">
            	<div id="removeImg"><img src="/images/mobile/removeX.gif" border="0" /></div>
                <div id="removeText">Remove Item</div>
            </div>
        </div>
    </div>
    <%
		subtotal = subtotal + (itemPrice * qty)
		rs.movenext
	loop
	%>
    <div id="totalBox">
    	<div id="subTotalRow">
        	<div id="subtotalText">Subtotal:</div>
            <div id="subtotalAmt"><%=formatCurrency(prepInt(subtotal),2)%></div>
        </div>
        <div id="subTotalRow">
        	<div id="shipText">Shipping:</div>
            <div id="shipAmt">FREE</div>
        </div>
        <div id="totalRow">
        	<div id="totalText">Estimated Total:</div>
            <div id="totalAmt"><%=formatCurrency(prepInt(subtotal),2)%></div>
        </div>
    </div>
</div>
<div id="proceedButtons"><a href="/checkout_step1.htm"><img src="/images/mobile/buttons/secureCheckout.jpg" border="0" /></a></div>
<div id="proceedButtons"><a href="/"><img src="/images/mobile/buttons/continueShopping.jpg" border="0" /></a></div>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">
	function removeItem(itemID) {
		eval("document.qtyAdjustForm_" + itemID + ".qty.value = 0")
		eval("document.qtyAdjustForm_" + itemID + ".submit()")
	}
</script>