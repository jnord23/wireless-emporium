<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<%
	dim pageName : pageName = "Confirm"
	dim useSessionID : useSessionID = prepInt(request.Cookies("mySession"))
	dim securePage : securePage = 1
	if instr(request.ServerVariables("SERVER_NAME"), "mdev.") > 0 then securePage = 0
	
	dim orderID : orderID = prepInt(request.QueryString("o"))
	dim accountID : accountID = prepInt(request.QueryString("a"))
	dim orderTotal : orderTotal = prepInt(request.QueryString("d"))
	usePostPurchase = request.querystring("pp")
	
	dim accountMasterId : accountMasterId = accountId
	' look for the parentId of the current account
	sql = "select parentId from we_accounts " &_
        "where email = '" & email & "' " &_
        "and accountId = " & accountid & " " &_
        "order by dateEntered " 'dateEntered just in case there are multiple records returned... which should NEVER happen
        'TODO: Check for more than 1 record returned and handle with an email to developers with debug information
        set rs = oConn.execute(sql)
		if (not rs.eof or rs.RecordCount > 0) and not isNull(rs("parentId")) then accountMasterId = rs("parentId")

	sql =	"select isnull(b.postPurchase,0) postPurchase, c.fname, c.lname, c.email, c.phone, c.bAddress1, c.bAddress2, c.bCity, c.bState, c.bZip, a.transactionID, a.extOrderType, ot.orderDesc orderTypeDesc, " & _
			"c.sAddress1, c.sAddress2, c.sCity, c.sState, c.sZip, d.itemDesc, b.itemid, b.quantity, d.price_our, " & _
			"a.ordersubtotal, a.ordershippingfee, a.orderTax, a.ordergrandtotal, a.pp_ordergrandtotal, isnull(e.promoCode, '') promoCode, isnull(e.promoPercent, 0) promoPercent, " & _
			"CASE " &_
			"	WHEN (b.cogs IS NULL OR b.cogs = 0) THEN d.cogs " &_
			" ELSE b.cogs " &_
			" END cogs " &_
			"from we_Orders a with (nolock) " & _
				"left join xorderType ot with (nolock) on a.extOrderType = ot.typeId " &_
				"join we_orderDetails b with (nolock) on a.orderID = b.orderID " & _
				"join we_accounts c with (nolock) on a.accountID = c.accountID " & _
				"left join we_Items d with (nolock) on b.itemID = d.itemID " & _
				"left join we_coupons e with (nolock) on a.couponid = e.couponid " & _
			"where a.orderID = " & orderID
	session("errorSQL") = sql
	set rs = oConn.execute(sql)

	if rs.eof then response.Redirect "/"
	
	dim fname : fname = prepStr(rs("fname"))
	dim lname : lname = prepStr(rs("lname"))
	dim email : email = prepStr(rs("email"))
	dim phone : phone = prepStr(rs("phone"))
	dim bAddress1 : bAddress1 = prepStr(rs("bAddress1"))
	dim bAddress2 : bAddress2 = prepStr(rs("bAddress2"))
	dim bCity : bCity = prepStr(rs("bCity"))
	dim bState : bState = prepStr(rs("bState"))
	dim bZip : bZip = prepStr(rs("bZip"))
	dim sAddress1 : sAddress1 = prepStr(rs("sAddress1"))
	dim sAddress2 : sAddress2 = prepStr(rs("sAddress2"))
	dim sCity : sCity = prepStr(rs("sCity"))
	dim sState : sState = prepStr(rs("sState"))
	dim sZip : sZip = prepStr(rs("sZip"))
	dim subTotal : subTotal = prepInt(rs("ordersubtotal"))
	dim shippingAmt : shippingAmt = prepInt(rs("ordershippingfee"))
	dim tax : tax = prepInt(rs("orderTax"))
	dim grandTotal : grandTotal = prepInt(rs("ordergrandtotal"))
	dim pp_grandTotal : pp_grandTotal = prepInt(rs("pp_ordergrandtotal"))	
	dim promoCode : promoCode = rs("promoCode")
    dim promoPercent : promoPercent = rs("promoPercent")
	dim auth_transactionid : auth_transactionid = rs("transactionID")
	dim extOrderType : extOrderType = rs("extOrderType")
	dim extOrderTypeDesc : extOrderTypeDesc = rs("orderTypeDesc")
	
	if bAddress2 <> "" then bAddress1 = bAddress1 & ", " & bAddress2
	if sAddress2 <> "" then sAddress1 = sAddress1 & ", " & sAddress2
%>
<!--#include virtual="/template/top.asp"-->
<%
	Response.Cookies("user")("id") = accountMasterId
	Response.Cookies("user")("email") = email
	Response.Cookies("user").Expires = DateAdd("yyyy",5,Now())

if (extOrderTypeDesc = "" or extOrderTypeDesc = "0" or isNull(extOrderTypeDesc)) then
		extOrderTypeDesc = "CC"
	end if

totalDiscount = 0
%>
<script>
window.WEDATA.pageType = 'orderConfirmation';
window.WEDATA.account.email = <%= jsStr(email) %>;
window.WEDATA.account.id = <%= jsStr(accountMasterId) %>;
window.WEDATA.orderData = {
    cartItems: [],
    orderNumber: <%= jsStr(orderId) %>,
    orderType: <%= jsStr(extOrderTypeDesc) %>,
    accountId: <%= jsStr(accountid) %>,
    taxAmount: <%= jsStr(tax) %>,
    grandTotal: <%= jsStr(grandtotal) %>,
    subTotal: <%= jsStr(subtotal) %>,
    shippingAmount: <%= jsStr(shippingAmt) %>,
    promoCode: <%= jsStr(promoCode) %>,
    shipping: {
        state: <%= jsStr(sState) %>,
        city: <%= jsStr(sCity) %>,
        zip: <%= jsStr(sZip) %>
    }
};

</script>
<div id="secureRow">
   	<div id="secureImg"><img src="/images/mobile/icons/secureIcon.gif" /></div>
    <div id="secureText">Customer Information</div>
</div>
<div id="orderReviewContainer">
    <div id="orderReviewBox">
    	<div id="orderReviewRowMsg">
        	<div id="thankYouText">Thank you for your purchase at</div>
            <div id="thankYouTextOrange">WirelessEmporium.com</div>
            <div id="thankYouText">Here are the details of your order:</div>
        </div>
    	<div id="custInfoRow">
        	<div class="custInfoTitle">Name:</div>
            <div class="custInfoValue"><%=fname & " " & lname%></div>
        </div>
        <div id="custInfoRow">
        	<div class="custInfoTitle">Email:</div>
            <div class="custInfoValue"><%=email%></div>
        </div>
        <div id="custInfoRow">
        	<div class="custInfoTitle">Phone:</div>
            <div class="custInfoValue"><%=phone%></div>
        </div>
        <div id="custInfoRow">
        	<div class="custInfoTitle">Billing Address:</div>
            <div class="custInfoValue">
				<%=bAddress1%><br />
                <%=bCity & ", " & bState & " " & bZip%>
            </div>
        </div>
        <div id="custInfoRow">
        	<div class="custInfoTitle">Shipping Address:</div>
            <div class="custInfoValue">
				<%=sAddress1%><br />
                <%=sCity & ", " & sState & " " & sZip%>
            </div>
        </div>
    </div>
</div>
<div id="alertContainer">
	<div id="alertIcon"><img src="/images/mobile/icons/alertIcon.gif" border="0" /></div>
    <div id="alertMsg">
    	<div id="alertValue">Please review your shipping address.</div>
        <div id="alertValue">If it is incorrect, contact us immediately at</div>
        <div id="alertValue"><a id="alertEmail" href="mailto:updates@wirelessemporium.com">updates@wirelessemporium.com</a></div>
    </div>
</div>
<div id="secureRow">
   	<div id="secureImg"><img src="/images/mobile/icons/secureIcon.gif" /></div>
    <div id="secureText">Order Details</div>
</div>
<div id="orderReviewContainer">
    <div id="orderReviewBox">
        <div id="orderReviewRowMsg">
            <div class="custInfoTitle">OrderID:</div>
            <div class="custInfoValue"><%=orderID%></div>
            <div class="custInfoTitle" style="margin-top:10px;">Your Items:</div>
        </div>
		<%
        prodLap = 1
        do while not rs.EOF
            itemDesc = prepStr(rs("itemDesc"))
            qty = prepInt(rs("quantity"))
            itemPrice = prepInt(rs("price_our"))
            itemCogs = prepInt(rs("cogs"))
            itemPostPurchase = rs("postPurchase")
            itemDiscount = (qty * itemPrice) * promoPercent
            totalDiscount = totalDiscount + itemDiscount
        %>
        <div id="orderReviewRowProd">
            <div id="itemDesc"><%=itemDesc%></div>
            <div id="qtyDetails">
                <div id="qtyText">Qty.</div>
                <div id="qtyValue"><%=qty%></div>
            </div>
            <% if qty = 1 then %>
            <div id="itemPrice">Item Price: <%=formatCurrency(itemPrice,2)%></div>
            <% else %>
            <div id="itemPriceSmall">Item Price: <%=formatCurrency(itemPrice,2)%></div>
            <div id="itemPrice">Item Total: <%=formatCurrency((itemPrice * qty),2)%></div>
            <% end if %>
        </div>

        <script>
            window.WEDATA.orderData.cartItems.push({
                itemId: <%= jsStr(rs("itemid")) %>,
                qty: <%= jsStr(qty) %>,
                price: <%= jsStr(itemPrice) %>,
                cogs: <%= jsStr(itemCogs) %>,
                discount: <%= jsStr(itemDiscount) %>,
                isPostPurchase: <%= jsStr(lcase(itemPostPurchase)) %>
            });
        </script>

        <%
			if itemids = "" then
				itemids = rs("itemid")
			else
				itemids = itemids & "," & rs("itemid")
			end if		
			
            rs.movenext
            prodLap = prodLap + 1
        loop
        %>
        <script> window.WEDATA.orderData.totalDiscount = <%= jsStr(totalDiscount) %>; </script>
    </div>
	<div id="orderReviewBox">
    	<div id="orderReviewRow">
        	<div class="orderReviewText">Subtotal:</div>
            <div class="orderReviewValue"><%=formatCurrency(subtotal,2)%></div>
        </div>
        <div id="orderReviewRow">
        	<div id="taxTotal" class="orderReviewText">Tax:</div>
            <div id="taxValue" class="orderReviewValue">
				<%=formatCurrency(tax,2)%>
            </div>
        </div>
		<%if promoCode <> "" then%>
        <div id="orderReviewRow">
        	<div class="orderReviewText">Promo Code:</div>
            <div class="orderReviewValue"><%=promoCode%></div>
        </div>
        <%end if%>
        <div id="orderReviewRow">
        	<div class="orderReviewText">Shipping:</div>
            <div id="shippingValue" class="orderReviewValueRed">
            	<% if shippingAmt = 0 then shippingAmt = "FREE" %>
				<%=shippingAmt%>
            </div>
        </div>
        <div id="orderReviewRow2">
        	<div id="orderReviewTextWhite">Grand Total:</div>
            <div id="grandTotalValue" class="orderReviewValueOrange">
				<%=formatCurrency(grandTotal,2)%>
            </div>
        </div>
        <%if pp_grandTotal > 0 then%>
        <div id="orderReviewRow" style="display:table; padding:5px 0px 5px 0px;">
            Thank you for your order! Please remember that you will see two charges on your account that total <b><%=formatCurrency(grandTotal,2)%></b>. One charge in the amount of <b><%=formatCurrency(grandTotal-pp_grandTotal,2)%></b> and the other in the amount of <b><%=formatCurrency(pp_grandTotal,2)%></b>
        </div>
        <%end if%>
    </div>
</div>
<div id="thanksText">
	We appreciate your business! You'll receive an Order Shipment Confirmation email upon shipment out of our warehouse.
    If you have any questions regarding your order, please email our customer service department at:
</div>
<div id="serviceEmailLink"><a href="mailto:service@wirelessemporium.com" id="serviceLink">service@wirelessemporium.com</a></div>
<%
session.abandon

'=================== post purchase widget start
if usePostPurchase = "Y" and isnull(extOrderType) then
	usePromoPercent = 0
	currentTotal = 0.0
	currentTax = 0.0
	if prepStr(itemids) = "" then itemids = "146466"
	
	if instr(request.ServerVariables("SERVER_NAME"),"mdev") > 0 then	
		actionURL = "/framework/postProcessing.htm"
	else
		actionURL = "https://m.wirelessemporium.com/framework/postProcessing.htm"
	end if
%>
    <div id="thank-you">
        <form name="frmPostPurchase" action="<%=actionURL%>" method="post">
        <div class="btn-close2"></div>
        <div class="modal-thank-you">
            <div class="top">
                <h1>Thanks for your purchase!</h1>
                <p>Congrats! Your order is complete. However, we did notice that your order is missing some of our most popular items as rated by our customers.</p>
                <%
                sql	=	"select	top 1 promoDate, promoPercent" & vbcrlf & _
                        "from	postpurchasepromo" & vbcrlf & _
                        "where	siteid = 0" & vbcrlf & _
                        "order by 1 desc"
                set rs = oConn.execute(sql)
                if not rs.eof then usePromoPercent = rs("promoPercent")
                
                if usePromoPercent > 0 then
                %>
                <p>For <span class="orange">One Time Only</span>, get an additional <span class="orange"><u><%=usePromoPercent%>% OFF</u></span> any of the items below.</p>
                <%
                else
                %>
                Save even more money by instantly adding additional value packed accessories. Below is a list of our most popular related products as chosen by our customers. <br />Just click the <span style="font-weight:bold; color:#ff6600;">ADD TO MY ORDER</span> button below to grab some of our best-selling items! Supplies are limited.
                <%
                end if
                %>            
            </div>
            <%
            sql = "sp_ppUpsell 0, '" & itemids & "', " & usePromoPercent & ", 60"
            set rs = oConn.execute(sql)
            lap = 0
            do until rs.eof
                lap = lap + 1
                itemdesc = rs("itemdesc")
                if len(itemdesc) > 100 then itemdesc = left(itemdesc, 97) & "..."
                price_retail = rs("price_retail")
                price_our = rs("price")
            %>
            <div class="pp-product">
                <div class="product-image"><img src="/productpics/thumb/<%=rs("itempic")%>" border="0" /></div>
                <div class="product-details">
                    <div class="add-this">
                        <input id="id_chkItem<%=lap%>" type="checkbox" name="chkItem<%=lap%>" value="<%=rs("itemid")%>" onclick="updatePPTotal();" />
                        <label for="id_chkItem<%=lap%>">Add this to my order</label>
                    </div>
                    <div class="details"><%=itemdesc%></div>
                    <div class="our-price">Our Price: <span class="orange"><%=formatcurrency(price_our)%></span></div>
                    <div class="list-price">List Price: <span class="strike"><%=formatcurrency(price_retail)%></span></div>
                </div>
                <div class="quantity">
                    <input type="tel" name="chkQty<%=lap%>" value="1" id="id_qty<%=lap%>" onkeyup="updatePPTotal();" />
                    <label for="id_qty<%=lap%>">Qty.</label>
                </div>
                <input id="id_itemPrice<%=lap%>" type="hidden" name="itemPrice<%=lap%>" value="<%=price_our%>" />
                <input id="id_itemRetail<%=lap%>" type="hidden" name="itemRetail<%=lap%>" value="<%=price_retail%>" />
            </div>
            <%
                rs.movenext
            loop
            %>
            <div class="bottom">
                <p>Please add the selected items to my order now for an additional:</p>
                <div class="total">$<span id="addlTotal">0.0</span>* (Save $<span id="addlSave">0.0</span>)</div>
                <div class="add-to-order"><input type="submit" value="" onclick="return doSubmit();" /></div>
                <div class="no-thanks"><input type="button" value="" id="no-thanks" /></div>
                <%
                curTax = 0
                curGrandTotal = 0
                sql = 	"select	isnull(ordersubtotal, 0) ordersubtotal, isnull(ordershippingfee, 0) ordershippingfee, isnull(orderTax, 0) orderTax, isnull(ordergrandtotal, 0) ordergrandtotal, accountid" & vbcrlf & _
                        "from	we_orders" & vbcrlf & _
                        "where	orderid = '" & orderid & "'"
                set rs = oConn.execute(sql)
                if not rs.eof then
                    curSubTotal = prepInt(rs("ordersubtotal"))
                    curShippingFee = prepInt(rs("ordershippingfee"))
                    curTax = prepInt(rs("orderTax"))
                    curGrandTotal = prepInt(rs("ordergrandtotal"))
                    accountid = prepInt(rs("accountid"))
                end if
                %>            
                <div class="note">
                    *Items will be added to your current order. You will see a 2nd charge for $<span id="addlGrandTotal">0.00</span> on your credit card.<br />
                    Total includes $<span id="addlTaxTotal">0.00</span> in sales tax for California residents. This is a WirelessEmporium.com offer.
                </div>
            </div>
        </div>
        <input type="hidden" name="addlGrandTotal" value="" />
        <input type="hidden" name="grandTotal" value="<%=curGrandTotal%>" />
        <input type="hidden" name="taxTotal" value="<%=curTax%>" />
        <input type="hidden" name="hidCount" value="<%=lap%>" />
        <input type="hidden" name="orderid" value="<%=orderid%>" />
        <input type="hidden" name="transid" value="<%=auth_transactionid%>" />
        </form>
    </div>

	<script>
		$(document).ready(function(){	
			$("#fade").show();
			$("#thank-you").show();
			
			$(".btn-close2, #no-thanks").click(function(){
				$("#thank-you").hide();
				$("#fade").hide();
			});
		});
		
        function doSubmit() {
            frm = document.frmPostPurchase;
            if (CurrencyFormatted(frm.addlGrandTotal.value) == "0.00") {
                alert("Plesae select the items");
                return false;
            }
            return true;
        }
        
        function updatePPTotal() {
            frm = document.frmPostPurchase;
            nCount = frm.hidCount.value;
            taxTotal = frm.taxTotal.value;
            
            var addlTotal = parseFloat(0);
            var addlTax = parseFloat(0);
            var addlShipping = parseFloat(0);
            var itemTotal = parseFloat(0);
            var retailTotal = parseFloat(0);
            var itemSave = parseFloat(0);
            var qtyTotal = parseFloat(0);
    
            for (i=1; i<= nCount; i++) {
                itemChecked = document.getElementById('id_chkItem'+i).checked;
                price_co = document.getElementById('id_itemPrice'+i).value;
                price_retail = document.getElementById('id_itemRetail'+i).value;
                qty = document.getElementById('id_qty'+i).value;			
                
                if (itemChecked) {
                    if ((!isNaN(qty))&&(qty != '')&&(qty != 0)) {		
                        itemTotal = parseFloat(itemTotal) + parseFloat(price_co * qty);
                        retailTotal = parseFloat(retailTotal) + parseFloat(price_retail * qty);
                        itemSave = parseFloat(itemSave) + (parseFloat(retailTotal) - parseFloat(itemTotal));
                        qtyTotal = parseInt(qtyTotal) + parseInt(qty);
                    }
                }
            }
            
            if (parseFloat(taxTotal) > 0) {
                addlTax = parseFloat(itemTotal * <%=Application("taxMath")%>);
            }
            
            addlShipping = parseFloat(0);
            addlTotal = parseFloat(itemTotal) + parseFloat(addlShipping) + parseFloat(addlTax);
    
            document.getElementById('addlSave').innerHTML = CurrencyFormatted(itemSave);
            document.getElementById('addlTotal').innerHTML = CurrencyFormatted(itemTotal);
            document.getElementById('addlTaxTotal').innerHTML = CurrencyFormatted(addlTax);
            document.getElementById('addlGrandTotal').innerHTML = CurrencyFormatted(addlTotal);
            
            frm.addlGrandTotal.value = CurrencyFormatted(addlTotal);
        }
        
        function CurrencyFormatted(amount) {
            var i = parseFloat(amount);
            if(isNaN(i)) { i = 0.00; }
            var minus = '';
            if(i < 0) { minus = '-'; }
            i = Math.abs(i);
            i = Math.round(i * 100) / 100;
            s = new String(i);
            if(s.indexOf('.') < 0) { s += '.00'; }
            if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
            s = minus + s;
            return s;
        }		
    </script>
<%end if%>
<!--#include virtual="/template/bottom.asp"-->