<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "Basket"
	
	dim itemID : itemID = prepInt(request.Form("itemID"))
	dim qty : qty = prepInt(request.Form("qty"))
	dim acctID : acctID = prepInt(session("acctID"))
	
	if prepInt(request.Cookies("mySession")) = 0 then
		response.Cookies("mySession") = session.SessionID
		response.Cookies("mySession").expires = date + 30
		useSessionID = session.SessionID
		newID = useSessionID
	else
		useSessionID = prepInt(request.Cookies("mySession"))
		newID = 0
	end if
	
	if itemID > 0 then
		if qty = 0 then
			sql = "delete from shoppingCart where itemID = " & itemID & " and sessionID = " & useSessionID
		else
			sql =	"if (select count(*) from shoppingCart where itemID = " & itemID & " and sessionID = " & useSessionID & ") > 0 " &_
						"update shoppingCart " &_
						"set qty = " & qty & " " &_
						"where itemID = " & itemID & " and sessionID = " & useSessionID & " " &_
					"else " &_
						"insert into shoppingCart (" &_
							"store,sessionID,accountID,itemID,qty,mobileOrder" &_
						") values(" &_
							"0," & useSessionID & "," & acctID & "," & itemID & "," & qty & ",1" &_
						")"
		end if
		session("errorSQL") = sql
		oConn.execute(sql)
	end if
	
	sql =	"select c.totalItems, a.qty, b.itemID, b.itemPic, b.itemDesc, b.price_our " &_
			"from shoppingCart a " &_
				"left join we_Items b on a.itemID = b.itemID " &_
				"outer apply (" &_
					"select sum(qty) as totalItems, sessionID " &_
					"from shoppingCart " &_
					"where sessionID = " & useSessionID & " " &_
					"group by sessionID" &_
				") c " &_
			"where a.sessionID = " & useSessionID
	session("errorSQL") = sql
	Set rs = oConn.execute(sql)
	
	if rs.EOF then
		itemCnt = "0 Items"
	else
		if rs("totalItems") <> 1 then
			itemCnt = rs("totalItems") & " Items"
		else
			itemCnt = rs("totalItems") & " Item"
		end if
	end if
%>
<!--#include virtual="/template/top.asp"-->
<div id="qtyInCartRow">
   	<div id="qtyInCartText">You have <span id="orangeText"><%=itemCnt%></span> in your cart:</div>
</div>
<div id="productContainer">
	<div style="padding:0px 0px 5px 0px; text-align:left;"><a href="/" style="text-decoration:none; color:#333; font-weight:bold;">&lsaquo; Continue Shopping</a></div>
	<%
	if rs.EOF then
	%>
    <div id="itemBox_noItems">You Currently Have<br />No Items In Your Cart</div>
    <%
	end if
	
	subtotal = 0
	do while not rs.EOF
		itemID = prepInt(rs("itemID"))
		itemPic = rs("itemPic")
		itemDesc = prepStr(rs("itemDesc"))
		qty = prepInt(rs("qty"))
		itemPrice = prepInt(rs("price_our"))
	%>
    <div id="itemBox">
    	<div id="itemPic" onclick="window.location='/product.htm?itemID=<%=itemID%>'"><img src="/productPics/thumb/<%=itemPic%>" border="0" /></div>
        <div id="itemDetails">
        	<div id="itemDesc" onclick="window.location='/product.htm?itemID=<%=itemID%>'"><%=itemDesc%></div>
            <form name="qtyAdjustForm_<%=itemID%>" method="post" id="basicForm">
            <div id="qtyDetails">
            	<div id="qtyText">Qty.</div>
                <div id="qtyValue">
                	<input type="text" id="qtyInput" name="qty" value="<%=qty%>" size="3" maxlength="3" />
                    <input type="hidden" name="itemID" value="<%=itemID%>" />
                </div>
                <div id="qtyButton"><input type="image" src="/images/mobile/buttons/updateQty.jpg" /></div>
            </div>
            </form>
            <% if qty = 1 then %>
            <div id="itemPrice">Item Price: <%=formatCurrency(itemPrice,2)%></div>
            <% else %>
            <div id="itemPriceSmall">Item Price: <%=formatCurrency(itemPrice,2)%></div>
            <div id="itemPrice">Item Total: <%=formatCurrency((itemPrice * qty),2)%></div>
            <% end if %>
            <div id="removeRow" onclick="removeItem(<%=itemID%>)">
            	<div id="removeImg"><img src="/images/mobile/removeX.gif" border="0" /></div>
                <div id="removeText">Remove Item</div>
            </div>
        </div>
    </div>
    <%
		subtotal = subtotal + (itemPrice * qty)
		rs.movenext
	loop
	%>
    <div id="totalBox">
    	<div id="subTotalRow">
        	<div id="subtotalText">Subtotal:</div>
            <div id="subtotalAmt"><%=formatCurrency(prepInt(subtotal),2)%></div>
        </div>
        <div id="subTotalRow">
        	<div id="shipText">Shipping:</div>
            <div id="shipAmt">FREE</div>
        </div>
        <div id="totalRow">
        	<div id="totalText">Estimated Total:</div>
            <div id="totalAmt"><%=formatCurrency(prepInt(subtotal),2)%></div>
        </div>
    </div>
</div>
<%
	sql =	"select c.totalItems, c.totalWeight, a.qty, b.itemID, b.itemPic, b.itemDesc, b.price_our " &_
			"from shoppingCart a " &_
				"left join we_Items b on a.itemID = b.itemID " &_
				"outer apply (" &_
					"select sum(qty) as totalItems, sum(itemWeight * ia.qty) as totalWeight, sessionID " &_
					"from shoppingCart ia " &_
						"left join we_Items ib on ia.itemID = ib.itemID " &_
					"where sessionID = " & useSessionID & " " &_
					"group by sessionID" &_
				") c " &_
			"where a.sessionID = " & useSessionID
	session("errorSQL") = sql
	Set rs = oConn.execute(sql)
	
	if rs.EOF or useSessionID = 0 then
		response.Redirect("/")
	else
		itemCnt = rs("totalItems")
		totalWeight = rs("totalWeight")
		
		itemDetails = ""
		orderAmt = 0
		do while not rs.EOF
			qty = rs("qty")
			itemID = rs("itemID")
			itemDesc = rs("itemDesc")
			itemPrice = rs("price_our")
			
			itemDetails = itemDetails & qty & "##"
			itemDetails = itemDetails & itemID & "##"
			itemDetails = itemDetails & itemDesc & "##"
			itemDetails = itemDetails & itemPrice & "@@"
			orderAmt = orderAmt + (qty * itemPrice)
			rs.movenext
		loop
		if len(itemDetails) > 0 then itemDetails = left(itemDetails,len(itemDetails)-2)
		itemDetailArray = split(itemDetails,"@@")
	end if
%>
<div id="checkoutOption">
	<form method="post" action="/checkout_step2.htm">
    	<input type="hidden" name="subtotal" value="<%=orderAmt%>" />
	    <input type="image" src="/images/mobile/buttons/creditCardCheckout.jpg" border="0" />
    </form>
</div>
<div id="checkoutOption"><img src="/images/mobile/buttons/creditCards.jpg" border="0" /></div>
<div id="otherOptions">
	<div id="checkoutOption" style="display:none;">
    	<form name="googleForm" method="post" action="/cart/process/google/CartProcessing.htm">
            <input type="hidden" name="promo" value="">
            <input type="hidden" name="analyticsdata" value="">
            <input type="hidden" name="sWeight" value="<%=totalWeight%>">
            <input type="hidden" name="mobileOrder" value="1" />
            <%
			for i = 0 to ubound(itemDetailArray)
				singleItemArray = split(itemDetailArray(i),"##")
				qty = singleItemArray(0)
				itemID = singleItemArray(1)
			%>
            <input type="hidden" name="itemID_<%=i+1%>" value="<%=itemID%>">
            <input type="hidden" name="itemQty_<%=i+1%>" value="<%=qty%>">
            <% next %>
            <input type="image" src="/images/mobile/buttons/googleCheckout.jpg" border="0" />
        </form>
    </div>
	<div id="checkoutOption">
    	<form name="PaypalForm" method="post" action="/cart/process/PaypalSOAP/ReviewOrder.htm" style="margin:0px;">
            <%
			itemLap = 0
			for i = 0 to ubound(itemDetailArray)
				singleItemArray = split(itemDetailArray(i),"##")
				qty = singleItemArray(0)
				itemID = singleItemArray(1)
				itemDesc = singleItemArray(2)
				itemPrice = singleItemArray(3)
			%>
			<input type="hidden" name="L_NUMBER<%=itemLap%>" value="<%=itemID%>">
            <input type="hidden" name="L_NAME<%=itemLap%>" value="<%=itemDesc%>">
            <input type="hidden" name="L_QTY<%=itemLap%>" value="<%=qty%>">
            <input type="hidden" name="L_AMT<%=itemLap%>" value="<%=itemPrice%>">
            <%
				itemLap = itemLap + 1
			next
			%>
            <input type="hidden" name="numItems" value="<%=itemCnt%>">
		    <input type="hidden" name="paymentAmount" value="<%=orderAmt%>">
		    <input type="hidden" name="ItemsAndWeight" value="<%=itemCnt%>|<%=totalWeight%>">
		    <input type="hidden" name="mobileOrder" value="1">
            <input type="image" src="/images/mobile/buttons/payPalCheckout.jpg" border="0" />
		</form>
    </div>
</div>
<!--
<div id="proceedButtons"><a href="/checkout_step1.htm"><img src="/images/mobile/buttons/secureCheckout.jpg" border="0" /></a></div>
<div id="proceedButtons"><a href="/"><img src="/images/mobile/buttons/continueShopping.jpg" border="0" /></a></div>
-->
<div style="height:10px;"></div>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">
	function removeItem(itemID) {
		eval("document.qtyAdjustForm_" + itemID + ".qty.value = 0")
		eval("document.qtyAdjustForm_" + itemID + ".submit()")
	}
</script>