<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "Product"
	basePageName = "product.asp"
	
	dim itemID : itemID = prepInt(request.QueryString("itemID"))
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	
	sql =	"select d.brandID, c.modelID, e.typeID, e.typeName, d.brandName, c.modelName, c.modelImg, a.itemID, a.itemPic, a.itemDesc, a.itemLongDetail, b.inv_qty, a.partNumber,  " &_
			"a.price_our, a.price_retail, f.avgRating, f.totalReviews, a.bullet1, a.bullet2, a.bullet3, a.bullet4, a.bullet5, a.bullet6, a.bullet7, a.bullet8, a.bullet9, a.bullet10 " &_
			"from we_Items a " &_
				"left join we_Items b on a.partNumber = b.partNumber and b.master = 1 " &_
				"left join we_models c on a.modelID = c.modelID " &_
				"left join we_brands d on d.brandID = b.brandID " &_
				"left join we_types e on a.typeID = e.typeID " &_
				"outer apply (select PartNumbers, avg(rating) as avgRating, COUNT(*) as totalReviews from we_Reviews where PartNumbers = a.PartNumber and approved = 1 group by PartNumbers) f " &_
			"where a.hideLive = 0 and a.ghost = 0 and a.itemID = " & itemID
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	if rs.EOF then response.Redirect("/")
	
	dim brandID : brandID = prepInt(request.Cookies("mobileBrandID"))
	dim modelID : modelID = prepInt(request.Cookies("mobileModelID"))
	if brandID = 0 or modelID = 0 then
		brandID = prepInt(rs("brandID"))
		modelID = prepInt(rs("modelID"))
	end if
	dim typeID : typeID = prepInt(rs("typeID"))
	dim categoryName : categoryName = prepStr(rs("typeName"))
	breadCrumb = 1
	
	sql =	"select top 2 d.brandName, c.modelName, c.modelImg, a.itemID, a.itemPic, a.itemDesc, a.price_our, e.avgRating, e.totalReviews " &_
			"from we_Items a " &_
				"left join we_Items b on a.partNumber = b.partNumber and b.master = 1 " &_
				"left join we_models c on a.modelID = c.modelID " &_
				"left join we_brands d on d.brandID = " & brandID & " " &_
				"outer apply (select PartNumbers, avg(rating) as avgRating, COUNT(*) as totalReviews from we_Reviews where PartNumbers = a.PartNumber and approved = 1 group by PartNumbers) e " &_
			"where a.hideLive = 0 and a.ghost = 0 and b.inv_qty > 0 and a.modelID = " & modelID & " and a.typeID = 3 and a.itemID <> " & itemID
	session("errorSQL") = sql
	set relatedRS = oConn.execute(sql)
	
	dim priceRetail : priceRetail = prepInt(rs("price_retail"))
	dim priceWE : priceWE = prepInt(rs("price_our"))
	dim itemPic : itemPic = rs("itemPic")
	dim avgRating : avgRating = prepInt(rs("avgRating"))
	dim totalReviews : totalReviews = prepInt(rs("totalReviews"))
	dim itemDesc : itemDesc = prepStr(rs("itemDesc"))
	dim itemLongDetail : itemLongDetail = prepStr(rs("itemLongDetail"))
	dim inv_qty : inv_qty = prepInt(rs("inv_qty"))
	dim partNumber : partNumber = prepStr(rs("partNumber"))
	deHtml = 0
	do while instr(itemLongDetail,"<") > 0
		deHtml = deHtml + 1
		if instr(itemLongDetail,"<") > 1 then
			session("errorSQL") = itemLongDetail
			if instr(itemLongDetail,">") > 0 then
				tempVal = mid(itemLongDetail,1,instr(itemLongDetail,"<")-1)
				tempVal = tempVal & mid(itemLongDetail,instr(itemLongDetail,">")+1)
			else
				tempVal = replace(itemLongDetail,"<","&lt;")
			end if
			itemLongDetail = tempVal
		else
			tempVal = mid(itemLongDetail,instr(itemLongDetail,">")+1)
			itemLongDetail = tempVal
		end if
	loop
	dim bullet1 : bullet1 = prepStr(rs("bullet1"))
	dim bullet2 : bullet2 = prepStr(rs("bullet2"))
	dim bullet3 : bullet3 = prepStr(rs("bullet3"))
	dim bullet4 : bullet4 = prepStr(rs("bullet4"))
	dim bullet5 : bullet5 = prepStr(rs("bullet5"))
	dim bullet6 : bullet6 = prepStr(rs("bullet6"))
	dim bullet7 : bullet7 = prepStr(rs("bullet7"))
	dim bullet8 : bullet8 = prepStr(rs("bullet8"))
	dim bullet9 : bullet9 = prepStr(rs("bullet9"))
	dim bullet10 : bullet10 = prepStr(rs("bullet10"))
	
	savePerc = priceWE / priceRetail
	savePerc = 1 - savePerc
	
	sql = "select * from we_reviews where partNumbers = '" & partNumber & "' and approved = 1"
	session("errorSQL") = sql
	set reviewRS = oConn.execute(sql)
	
	'Review Counts
	sql = "exec sp_pullProductReviewsByPartNumber '" & partNumber & "'"
	session("errorSQL") = sql
	set rsReviewCnt = oConn.execute(sql)
	
	if rsReviewCnt.EOF then
		dblAvgRating = 0
		strRatingCount = 0
	else
		dblAvgRating = rsReviewCnt("avgRating")
		strRatingCount = rsReviewCnt("reviewCnt")
	end if
	
	gblShipoutDate = "Order today and this item will ship out within 24 hours."
	shipoutCutoff = cdate(Date & " 12:00:00 PM")
	if now < shipoutCutoff then
		nextMin = datediff("n", now, shipoutCutoff)
		if cint(nextMin/60) = 0 then
			gblShipoutDate = "Order within the next <b>" & cint(nextMin mod 60) & " min</b> and this item ships out today."
		else
			gblShipoutDate = "Order within the next <b>" & cint(nextMin/60) & " hrs</b> and <b>" & cint(nextMin mod 60) & " min</b> and your item ships out today."
		end if
	end if
%>
<!--#include virtual="/template/top.asp"-->
<!-- rich snippets mark up -->
<div itemscope itemtype="http://data-vocabulary.org/Product">
	<div class="prodDescBox_b">
        <span itemprop="name"><div class="prodDesc_b"><%=itemDesc%></div></span>
    </div>
    <% if inv_qty > 0 then %>
    <div class="addToCart_b">
    	<form name="addToCart" method="post" action="/basket.htm">
        <div class="addToCart_b">
        	<input type="image" src="/images/mobile/buttons/addToCart_02.png" />
        	<input type="hidden" name="qty" value="1" />
	        <input type="hidden" name="itemID" value="<%=itemID%>" />
        </div>
        </form>
    </div>
    <% end if %>
    <div class="ratingInfo_b" onclick="readReviews()">
		<% if avgRating > 0 then %>
        <div class="ratingStars_b">
            <%
            for i = 1 to 5
                if avgRating => i then useStar = "on" else useStar = "off"
                response.Write("<img src='/images/mobile/ratingStar_" & useStar & ".gif' border='0' />")
            next
            %>
        </div>
        <div class="readReviews_b">(Read <%=totalReviews%> Reviews)</div>
        <% end if %>
    </div>
    <div class="prodPic_b"><img id="fullSized" itemprop="image" src="/productpics/big/<%=itemPic%>" border="0" /></div>
    <div id="altImgRow">
        <div id="altImgBox" onclick="document.getElementById('fullSized').src='/productpics/big/<%=itemPic%>'"><img src="/productpics/icon/<%=itempic%>" border="0" width="40" /></div>
        <%
        for iCount = 0 to 2
            path = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".jpg")
            thumbPath = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & "_thumb.jpg")
            if fso.FileExists(Server.MapPath(thumbPath)) then
        %>
        <div id="altImgBox" onclick="document.getElementById('fullSized').src='<%=path%>'"><img src="<%=thumbPath%>" border="0" /></div>
        <%
            end if
        next
        %>
    </div>
    <span itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer">
	    <meta itemprop="currency" content="USD" />
        <div class="priceRating_b tb">
        	<div class="priceTitle fl tp3">Our Price:</div>
            <div class="priceValueOur fl">$<span itemprop="price"><%=formatnumber(priceWE,2)%></span></div>
        </div>
        <div class="priceRating_b tb">
            <div class="priceTitle fl">Retail Price:</div>
            <div class="priceValueRetail fl"><%=formatCurrency(priceRetail,2)%></div>
        </div>
        <div class="priceRating_b tb">
            <div class="priceTitle bl fl">You Save:</div>
            <div class="priceSave fl"><%=formatCurrency(priceRetail-priceWE,2)%> (<%=formatPercent(savePerc,0)%>)</div>
        </div>
	</span>
    <div class="tb inStockRow">
    	<% if inv_qty > 0 then %>
    	<div class="tb mCenter inStockBox">
            <div class="fl inStockIcon"></div>
            <div class="fl inStockMsg">
                <div class="tb inStockTxt">THIS ITEM IS IN STOCK!</div>
                <div class="tb sameDayTxt"><%=gblShipoutDate%></div>
            </div>
        </div>
        <form name="addToCart" method="post" action="/basket.htm">
        <div class="addToCart_b">
        	<input type="image" src="/images/mobile/buttons/addToCart_02.png" />
        	<input type="hidden" name="qty" value="1" />
	        <input type="hidden" name="itemID" value="<%=itemID%>" />
        </div>
        </form>
        <% else %>
        <div class="outOfStockTxt">This items is currently out of stock</div>
        <% end if %>
    </div>
    <div id="valuePropRow">
        <div id="valueBox">
            <div id="valueCheckmark"><img src="/images/mobile/icons/checkmark_02.gif" border="0" /></div>
            <div id="valueText">Fast, FREE Shipping!</div>
        </div>
    </div>
    <div id="valuePropRow">
        <div id="valueBox">
            <div id="valueCheckmark"><img src="/images/mobile/icons/checkmark_02.gif" border="0" /></div>
            <div id="valueText">90-Day No-Hassle Returns</div>
        </div>
    </div>
    <div id="valuePropRow">
        <div id="valueBox">
            <div id="valueCheckmark"><img src="/images/mobile/icons/checkmark_02.gif" border="0" /></div>
            <div id="valueText">365-Day Free Item Replacement</div>
        </div>
    </div>
    <div id="valuePropRow">
        <div id="valueBox">
            <div id="valueCheckmark"><img src="/images/mobile/icons/checkmark_02.gif" border="0" /></div>
            <div id="valueText">100% Secure Shopping Guarantee</div>
        </div>
    </div>
    <div id="tabContainer">
        <div id="tabRow">
            <div id="detailsTab" class="detailsTab_b_off" onclick="showTab('detailsTab')">Details</div>
            <div id="featuresTab" class="featuresTab_b_on" onclick="showTab('featuresTab')">Features</div>
            <% if not reviewRS.EOF then %><div id="reviewsTab" class="reviewsTab_b_off" onclick="showTab('reviewsTab')">Reviews</div><% end if %>
        </div>
        <span itemprop="description">
	        <div id="detailsTabContent" class="detailsTabContent_off"><%=itemLongDetail%></div>
        </span>
        <div id="featuresTabContent" class="featuresTabContent_on">
            <ul style="line-height:20px;">
                <li><%=bullet1%></li>
                <% if bullet2 <> "" then %><li><%=bullet2%></li><% end if %>
                <% if bullet3 <> "" then %><li><%=bullet3%></li><% end if %>
                <% if bullet4 <> "" then %><li><%=bullet4%></li><% end if %>
                <% if bullet5 <> "" then %><li><%=bullet5%></li><% end if %>
                <% if bullet6 <> "" then %><li><%=bullet6%></li><% end if %>
                <% if bullet7 <> "" then %><li><%=bullet7%></li><% end if %>
                <% if bullet8 <> "" then %><li><%=bullet8%></li><% end if %>
                <% if bullet9 <> "" then %><li><%=bullet9%></li><% end if %>
                <% if bullet10 <> "" then %><li><%=bullet10%></li><% end if %>
            </ul>
        </div>
        <div id="reviewsTabContent" class="reviewsTabContent_off">
            <%		
            rowID = 1
            do while not reviewRS.EOF
                nickname = reviewRS("nickname")
                rating = reviewRS("rating")
                reviewTitle = reviewRS("reviewTitle")
                reviewText = reviewRS("review")
            %>
            <div id="reviewBox<%=rowID%>">
                <div id="reviewerAndStars">
                    <div id="reviewer">Review By: <%=nickname%></div>
                    <div id="reviewStars">
                        <%
                        for i = 1 to 5
                            if avgRating => i then useStar = "on" else useStar = "off"
                            response.Write("<img src='/images/mobile/ratingStar_" & useStar & ".gif' border='0' />")
                        next
                        %>
                    </div>
                </div>
                <div id="reviewTitleRow">
                    <div id="reviewTitle"><%=reviewTitle%></div>
                </div>
                <div id="reviewText"><%=reviewText%></div>
            </div>
            <%
                reviewRS.movenext
                if rowID = 1 then rowID = 2 else rowID = 1
            loop
            %>
        </div>
    </div>
	<span style="display:none;" itemprop="review" itemscope itemtype="http://data-vocabulary.org/Review-aggregate">
		<span itemprop="rating"><%=round(prepInt(dblAvgRating), 2)%></span> out of 5 stars, based on <span itemprop="count"><%=formatnumber(strRatingCount,0)%></span> reviews
	</span>
    <div class="itemPartNumber">Part #: <%=partnumber%> | Item ID: <%=itemID%></div>
    <% if not relatedRS.EOF then %>
    <div id="valuePropRow">
        <div id="valueBox">
            <div id="valueCheckmark"><img src="/images/mobile/icons/heart.gif" border="0" width="26" height="26" /></div>
            <div id="valueText">Other Products You'll Love</div>
        </div>
    </div>
    <div id="mainProductBox">
        <div id="refProdctBox">
            <%
            do while not relatedRS.EOF
                itemID = relatedRS("itemID")
                itemPic = relatedRS("itemPic")
                itemDesc = relatedRS("itemDesc")
                itemDesc = replace(itemDesc,brandName,"")
                itemDesc = trim(replace(itemDesc,modelName,""))
                itemPrice = formatCurrency(prepInt(relatedRS("price_our")),2)
                avgRating = relatedRS("avgRating")
            %>
            <a href="/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.htm" style="text-decoration:none;" title="<%=itemDesc%>">
            <div id="singleProdBox_<%=prodCnt%>" class="singleProdBox">
                <div id="refPic"><img src="/productpics/thumb/<%=itemPic%>" /></div>
                <div id="refDesc"><%=itemDesc%></div>
                <div id="refPrice"><%=itemPrice%></div>
                <% if prepInt(avgRating) > 0 then %>
                <div id="refRating">
                    <%
                    for i = 1 to 5
                        if avgRating => i then useStar = "on" else useStar = "off"
                        response.Write("<img src='/images/mobile/ratingStar_" & useStar & ".gif' border='0' />")
                    next
                    %>
                </div>
                <% else %>
                <div id="refRating"></div>
                <% end if %>
            </div>
            </a>
            <%
                relatedRS.movenext
            loop
            %>
        </div>
    </div>
    <% end if %>
</div>
<!--// rich snippets mark up -->
<div class="backToTopArrow" onclick="window.scroll(0,0)"></div>
<div class="backToTopTxt" onclick="window.scroll(0,0)">BACK TO TOP</div>
<div class="bottomButtonGraphic" onclick="history.go(-1)">Return To Previous Page</div>
<div class="bottomButtonGraphic" onclick="window.location='/?reset=1'">Change Your Device</div>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">
	var curTab = "featuresTab"
	
	window.scroll(0,160)
	
	function showTab(tab) {
		document.getElementById(curTab).setAttribute("class", curTab + "_b_off")
		document.getElementById(tab).setAttribute("class", tab + "_b_on")
		document.getElementById(curTab + 'Content').setAttribute("class", curTab + "Content_off")
		document.getElementById(tab + 'Content').setAttribute("class", tab + "Content_on")
		curTab = tab
	}
	
	function readReviews() {
		showTab('reviewsTab')
		window.location = '#reviewsTab'
	}
</script>