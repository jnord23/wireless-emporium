<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	dim pageName : pageName = "idmyphone"
%>
<!--#include virtual="/template/top.asp"-->
<div id="main">
    <div class="mCenter" style="color:#333; font-size:18px; font-family:Arial, Helvetica, sans-serif;" align="center">
	<%
	uAgnt = replace(request.ServerVariables("HTTP_USER_AGENT"), "'", "''")
	sql = "exec mobileRedirectByUA '" & uAgnt & "', 1"
	session("errorSQL") = sql
	set rsRedirectUA = oConn.execute(sql)
	if not rsRedirectUA.eof then
		UABrandID = rsRedirectUA("brandid")
		UABrandName = rsRedirectUA("brandName")
		UAModelID = prepInt(rsRedirectUA("modelid"))
		UAModelName = rsRedirectUA("modelName")
		UAModelImg = rsRedirectUA("modelImg")		
		UAPhoneTypeID = rsRedirectUA("phoneTypeID")
		UAPhoneTypeNme = rsRedirectUA("phoneTypeName")
		UAIsApple = rsRedirectUA("isApple")
		if UAModelID > 0 and UABrandID > 0 then
'			response.write "UAModelID:" & UAModelID & "<br>"
'			response.write "UABrandID:" & UABrandID & "<br>"
'			response.write "UAModelImg:" & UAModelImg & "<br>"
		%>
		<div id="detectHeader" class="tb pt10">We've detected that you are using the</div>
		<div class="tb pt10 bold" style="font-size:24px;"><%=UABrandName%>&nbsp;<%=UAModelName%></div>
        <div class="tb pt10">
	        <img src="/productPics/models/<%=UAModelImg%>" width="70" alt="<%=UABrandName & " " & UAModelName%>" />
        </div>
		<div class="tb pt10">Popular items as picked by customers:</div>
		<div class="tb pt10 strandsRecs" tpl="CTGY-2" dfilter="modelname::<%=UAModelName%>" ></div>
        <%
		else
		%>
	    <div class="tb ptb10">We were unable to detect your phone device.</div>
        <%
		end if
	else	
	%>
    <div class="tb ptb10">We were unable to detect your phone device.</div>
    <%
	end if
	%>
    </div>
    <div class="clr ptb10">&nbsp;</div>
</div>
<script type="text/javascript" src="//bizsolutions.strands.com/sbsstatic/js/sbsLib-1.0.min.js"></script>
<script>
	window.onload = function() { 
		strands();

		$('html, body').animate({
			scrollTop: $("#main").offset().top
		}, 500);
	}
	
	function strands_rendering(rec_info) {
		var rec = rec_info.recommendations;
		var content = "";
		var lap = 0;
		for(i=0;i<=rec.length-1;i++){
//			alert(JSON.stringify(rec[i]));			
			var price_our = rec[i]["metadata"]["properties"]["saleprice"];
			var itemDesc = rec[i]["metadata"]["name"];
			if (itemDesc.length > 50) itemDesc = itemDesc.substring(0,46) + '...';
			var itempic = (rec[i]["metadata"]["picture"]).replace("/thumb/", "/big/");
			if (!isNaN(price_our)) {
				lap = i + 1;
				content = content + "<div style='float:left; width:100px; padding:30px 5px 0px 0px;'>";
				content = content + "	<div style='float:left; width:100%; text-align:center; cursor:pointer;' onclick=\"location.href='" + rec[i]["metadata"]["link"] + "'\"><img src='" + itempic + "' border='0' width='100' height='100' /></div>";
				content = content + "	<div style='float:left; width:100%; padding-top:5px; height:85px; font-weight:bold; font-size:13px; color:#333; cursor:pointer;' title='" + rec[i]["metadata"]["name"] + "' onclick=\"location.href='" + rec[i]["metadata"]["link"] + "'\">" + itemDesc + "</div>";
				content = content + "	<div style='float:left; width:100%; padding-top:5px; color:#ca0702; font-weight:bold; font-size:13px;'>$" + price_our + "</div>";
				content = content + "</div>";
				if (lap == 3) break;
			}
		}
		$(".strandsRecs").html(content);
	}
	
	function strands() {
		try{ 
			SBS.Recs.setRenderer(strands_rendering, "CTGY-2");
			SBS.Worker.go("PLOSodl5QC"); 
		} catch (e){}	
	}
</script>
<!--#include virtual="/template/bottom.asp"-->
