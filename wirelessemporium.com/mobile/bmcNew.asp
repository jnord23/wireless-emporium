<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "BMC"
%>
<!--#include virtual="/template/top.asp"-->
<%	
	dim brandID : brandID = prepInt(request("brandID"))
	dim modelID : modelID = prepInt(request("modelID"))
	dim typeID : typeID = prepInt(request("typeID"))
	dim sortBy : sortBy = prepStr(request.Form("sortBy"))
	dim filterSubtype : filterSubtype = prepInt(request.Form("filterSubtype"))
	dim filterPrice : filterPrice = prepInt(request.Form("filterPrice"))
	dim design : design = prepStr(request.Form("design"))
	
	response.Cookies("mobileBrandID") = brandID
	response.Cookies("mobileBrandID").Expires = now+30
	response.Cookies("mobileModelID") = modelID
	response.Cookies("mobileModelID").Expires = now+30
	
	if brandID = 0 or modelID = 0 or typeID = 0 then response.Redirect("/")
	
	set fso = CreateObject("Scripting.FileSystemObject")
	
	sql =	"select top 40 a.numberOfSales, d.brandName, c.modelName, c.modelImg, a.itemID, a.itemPic, a.itemDesc, a.price_our, e.avgRating " &_
			"from we_Items a " &_
				"left join we_Items b on a.partNumber = b.partNumber and b.master = 1 " &_
				"left join we_models c on a.modelID = c.modelID " &_
				"left join we_brands d on d.brandID = " & brandID & " " &_
				"outer apply (select PartNumbers, avg(rating) as avgRating from we_Reviews where PartNumbers = a.PartNumber and approved = 1 group by PartNumbers) e " &_
			"where a.hideLive = 0 and a.ghost = 0 and b.inv_qty > 0 and a.modelID = " & modelID & " and a.typeID = " & typeID & " " &_
			"union " &_
			"select a2.numberOfSales, d.brandName, c.modelName, c.modelImg, a.itemID, a2.itemPic, a2.itemDesc, a2.price_our, e.avgRating " &_
			"from we_relatedItems a " &_
				"join we_Items a2 on a.itemID = a2.itemID " &_
				"left join we_Items b on a2.partNumber = b.partNumber and b.master = 1 " &_
				"left join we_models c on a.modelID = c.modelID " &_
				"left join we_brands d on d.brandID = " & brandID & " " &_
				"outer apply (select PartNumbers, avg(rating) as avgRating from we_Reviews where PartNumbers = a2.PartNumber and approved = 1 group by PartNumbers) e " &_
			"where a2.hideLive = 0 and a2.ghost = 0 and b.inv_qty > 0 and a.modelID = " & modelID & " and a.typeID = " & typeID
	if filterSubtype > 0 then
		sql = replace(sql,"where a.hideLive = 0","where a.hideLive = 0 and a.subtypeID = " & filterSubtype)
		sql = replace(sql,"where a2.hideLive = 0","where a2.hideLive = 0 and a2.subtypeID = " & filterSubtype)
	end if
	if design <> "" then
		if instr(design,"##") > 0 then
			designArray = split(design,"##")
			designList = ""
			for i = 0 to ubound(designArray)
				if designList = "" then
					designList = "(a.itemDesc like '%" & designArray(i) & "%'"
				else
					designList = designList & " or a.itemDesc like '%" & designArray(i) & "%'"
				end if
			next
			designList = designList & ")"
			sql = replace(sql,"where a.hideLive = 0","where a.hideLive = 0 and " & designList)
			sql = replace(sql,"where a2.hideLive = 0","where a2.hideLive = 0 and " & replace(designList,"a.","a2."))
		else
			sql = replace(sql,"where a.hideLive = 0","where a.hideLive = 0 and a.itemDesc like '%" & design & "%'")
			sql = replace(sql,"where a2.hideLive = 0","where a2.hideLive = 0 and a2.itemDesc like '%" & design & "%'")
		end if
	end if
	
	if filterPrice > 0 then
		if filterPrice = 10 then
			price1 = 0
			price2 = 9.99
			showFilterPrice = "Less than $10"
		end if
		if filterPrice = 19.99 then
			price1 = 10
			price2 = 19.99
			showFilterPrice = "$10 to $19.99"
		end if
		if filterPrice = 39.99 then
			price1 = 20
			price2 = 39.99
			showFilterPrice = "$20 to $39.99"
		end if
		if filterPrice = 79.99 then
			price1 = 40
			price2 = 79.99
			showFilterPrice = "$40 to $79.99"
		end if
		if filterPrice = 99.99 then
			price1 = 80
			price2 = 99.99
			showFilterPrice = "$80 to $99.99"
		end if
		if filterPrice = 100 then
			price1 = 100
			price2 = 2000
			showFilterPrice = "$100 and Up"
		end if
		sql = replace(sql,"where a.hideLive = 0","where a.hideLive = 0 and a.price_our >= " & price1 & " and a.price_our <= " & price2)
		sql = replace(sql,"where a2.hideLive = 0","where a2.hideLive = 0 and a2.price_our >= " & price1 & " and a2.price_our <= " & price2)
	end if
	
	if sortBy = "low" then
		sql = sql & " order by price_our"
	elseif sortBy = "high" then
		sql = sql & " order by price_our desc"
	elseif sortBy = "new" then
		sql = sql & " order by a.itemID desc"
	elseif sortBy = "rating" then
		sql = sql & " order by e.avgRating desc, a.itemID desc"
	else
		sql = sql & " order by a.numberOfSales desc, a.itemID desc"
	end if
	
	'response.Write("<br>" & sql & "<br>")
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	if rs.EOF then
		sql = "select a.brandName, b.modelName, b.modelImg from we_brands a left join we_models b on b.modelID = " & modelID & " where a.brandID = " & brandID
		session("errorSQL") = session("errorSQL") & "##NEXT SQL##" & sql
		set rs = oConn.execute(sql)
		
		brandName = rs("brandName")
		modelName = rs("modelName")
		modelImg = rs("modelImg")
		
		rs.movenext
	else
		brandName = rs("brandName")
		modelName = rs("modelName")
		modelImg = rs("modelImg")
	end if
%>
<div id="curModelDetails">
	<div id="modelPic"><img src="/productPics/models/thumbs/<%=modelImg%>" /></div>
    <div id="modelText">
    	<div id="currentlyViewing">CURRENTLY VIEWING PRODUCTS FOR:</div>
        <div id="modelName"><%=modelName%></div>
        <a href="/?reset=1" style="text-decoration:none;">
        <div id="changeDeviceBox">
        	<div id="redX"></div>
            <div id="tapToChange">Tap to change device.</div>
        </div>
        </a>
    </div>
</div>
<form id="basicForm" name="prodSelectForm" method="post" action="/bmcNew.htm">
<div id="sortFilterBar">
	<div id="topResultCnt"></div>
    <div id="filterButton" onclick="showFilterOptions()"></div>
    <div id="sortButton">
    	<select name="sortBy" class="sortBySelect" onchange="document.prodSelectForm.submit()">
        	<option value="">Sort By</option>
            <option value="pop">Most Popluar</option>
            <option value="new">Newest</option>
            <option value="low">Lowest Price</option>
            <option value="high">Highest Price</option>
            <option value="rating">Rating</option>
        </select>
		<input type="hidden" name="brandID" value="<%=brandID%>" />
        <input type="hidden" name="modelID" value="<%=modelID%>" />
        <input type="hidden" name="typeID" value="<%=typeID%>" />
    </div>
</div>
<div id="filterBoxes" class="filterOptionsHidden">
	<div id="filterType">One Click Filters</div>
	<%
	if typeID = 3 then
	%>
    <div id="popDesignRow">
    	<div id="DesignBox" onclick="selectDesign('hearts## love ')">
        	<div id="designImg"><img src="/images/mobile/buttons/filterHearts.jpg" border="0" /></div>
            <div id="designText">Hearts/Love</div>
        </div>
        <div id="DesignBox" onclick="selectDesign('butterfl')">
        	<div id="designImg"><img src="/images/mobile/buttons/filterButterflies.jpg" border="0" /></div>
            <div id="designText">Butterflies</div>
        </div>
        <div id="DesignBox" onclick="selectDesign('flower##blossom')">
        	<div id="designImg"><img src="/images/mobile/buttons/filterFlowers.jpg" border="0" /></div>
            <div id="designText">Flowers</div>
        </div>
    </div>
    <div id="popDesignRow">
    	<div id="DesignBox" onclick="selectDesign('zebra')">
        	<div id="designImg"><img src="/images/mobile/buttons/filterZebra.jpg" border="0" /></div>
            <div id="designText">Zebra</div>
        </div>
        <div id="DesignBox" onclick="selectDesign('checker')">
        	<div id="designImg"><img src="/images/mobile/buttons/filterChecker.jpg" border="0" /></div>
            <div id="designText">Checkered</div>
        </div>
        <div id="DesignBox" onclick="selectDesign('skull')">
        	<div id="designImg"><img src="/images/mobile/buttons/filterSkulls.jpg" border="0" /></div>
            <div id="designText">Skulls</div>
        </div>
    </div>
    <%
	end if
	%>
    <div id="filterType">Multi-Select Filters</div>
    <%
	sql = "select subtypeid, subtypeName from v_subtypeMatrix where typeID = " & typeID
	session("errorSQL") = session("errorSQL") & "##NEXT SQL##" & sql
	set subtypeRS = oConn.execute(sql)
	
	if not subtypeRS.EOF then
    %>
    <div id="homeSelectBox">
        <div id="selectBoxContainer">
            <select id="actualSelectBox" name="filterSubtype">
                <option value="">Filter By Type</option>
                <%			
                do while not subtypeRS.EOF
					if filterSubtype = subtypeRS("subtypeid") then showFilterType = subtypeRS("subtypeName")
                %>
                <option value="<%=subtypeRS("subtypeid")%>"<% if filterSubtype = subtypeRS("subtypeid") then %> selected="selected"<% end if %>><%=subtypeRS("subtypeName")%></option>
                <%
                    subtypeRS.movenext
                loop
                %>
            </select>
        </div>
    </div>
    <%
	end if
	subtypeRS = null
    %>
    <div id="homeSelectBox">
        <div id="selectBoxContainer">
            <select id="actualSelectBox" name="filterPrice">
                <option value="">Filter By Price</option>
                <option value="10"<% if filterPrice = 10 then %> selected="selected"<% end if %>>less than $10</option>
                <option value="19.99"<% if filterPrice = 19.99 then %> selected="selected"<% end if %>>$10 to $19.99</option>
                <option value="39.99"<% if filterPrice = 39.99 then %> selected="selected"<% end if %>>$20 to $39.99</option>
                <option value="79.99"<% if filterPrice = 79.99 then %> selected="selected"<% end if %>>$40 to $79.99</option>
                <option value="99.99"<% if filterPrice = 99.99 then %> selected="selected"<% end if %>>$80 to $99.99</option>
                <option value="100"<% if filterPrice = 100 then %> selected="selected"<% end if %>>$100 and Up</option>
            </select>
        </div>
    </div>
	<div id="homeShopNow">
    	<input type="hidden" name="design" value="" />
        <input alt="Show Now!" type="image" src="/images/mobile/buttons/filterNow.gif" border="0" />
    </div>
</div>
</form>
<% if filterSubtype > 0 then %>
<div id="filterBar" onclick="removeTypeFilter()">
    <div id="filterSet" style="padding:5px;">Filter Type: <span style="color:#FFF;"><%=showFilterType%></span></div>
    <div id="filterX">X</div>
</div>
<% end if %>
<% if filterPrice > 0 then %>
<div id="filterBar" onclick="removePriceFilter()">
    <div id="filterSet" style="padding:5px;">Filter Price: <span style="color:#FFF;"><%=showFilterPrice%></span></div>
    <div id="filterX">X</div>
</div>
<% end if %>
<div id="mainProductBox">
	<%
	prodCnt = 0
	if rs.EOF then
%>
	<div id="noProds">No Product Found<br />Please adjust your filters</div>
<%
	end if
	
	do while not rs.EOF
		itemID = rs("itemID")
		itemPic = rs("itemPic")
		itemDesc = rs("itemDesc")
		itemDesc = replace(itemDesc,brandName,"")
		itemDesc = trim(replace(itemDesc,modelName,""))
		itemPrice = formatCurrency(prepInt(rs("price_our")),2)
		avgRating = rs("avgRating")
		
		if fso.fileExists(server.MapPath("/productpics/thumb/" & itemPic)) then
			prodCnt = prodCnt + 1
			if prodCnt < 11 then
				useClass = "singleProdBox"
				useImg = "img"
			else
				useClass = "singleProdBox2"
				useImg = "tempimg"
			end if
	%>
    <a href="/product.htm?itemID=<%=itemID%>" style="text-decoration:none;" title="<%=itemDesc%>">
	<div id="singleProdBox_<%=prodCnt%>" class="<%=useClass%>">
    	<div id="prodPic"><<%=useImg%> src="/productpics/thumb/<%=itemPic%>" /></div>
        <div id="prodDesc"><%=itemDesc%></div>
        <div id="prodPrice"><%=itemPrice%></div>
        <% if prepInt(avgRating) > 0 then %>
        <div id="prodRating">
        	<%
			for i = 1 to 5
				if avgRating => i then useStar = "on" else useStar = "off"
				response.Write("<img src='/images/mobile/ratingStar_" & useStar & ".gif' border='0' />")
			next
			%>
        </div>
		<% else %>
        <div id="prodRating"></div>
        <% end if %>
    </div>
    </a>
    <%
		end if
		rs.movenext
	loop
	%>
</div>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">
	//document.getElementById("topResultCnt").innerHTML = "<%=prodCnt%> results."
	
	function selectDesign(design) {
		document.prodSelectForm.design.value = design
		document.prodSelectForm.submit()
	}
	
	function showFilterOptions() {
		if (document.getElementById("filterBoxes").className == "filterOptions") {
			document.getElementById("filterBoxes").setAttribute("class", "filterOptionsHidden")
		}
		else {
			document.getElementById("filterBoxes").setAttribute("class", "filterOptions")
			window.scroll(0,330)
		}
	}
	
	function removeTypeFilter() {
		document.prodSelectForm.filterSubtype.selectedIndex = 0
		document.prodSelectForm.submit()
	}
	
	function removePriceFilter() {
		document.prodSelectForm.filterPrice.selectedIndex = 0
		document.prodSelectForm.submit()
	}
	
	//document.getElementById("baseCss").href = '/template/css/base2.css'
	
	//navigator.geolocation.getCurrentPosition(GetLocation);
	function GetLocation(location) {
		alert(location.coords.latitude);
		alert(location.coords.longitude);
		alert(location.coords.heading);
		alert(location.coords.speed);
		alert(location.coords.altitude);
	}
</script>