<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_getStates.asp"-->
<%
	dim pageName : pageName = "Checkout2"
	dim useSessionID : useSessionID = prepInt(request.Cookies("mySession"))
	dim csID : csID = prepInt(request.QueryString("csID"))
	dim securePage : securePage = 1
	weDataStr = ""
	if instr(request.ServerVariables("SERVER_NAME"), "mdev.") > 0 then securePage = 0
	
	if csID > 0 then useSessionID = csID
	mySession = useSessionID
	
	sql =	"update shoppingCart set step2 = 1 where sessionID = " & useSessionID
	session("errorSQL") = sql
	oConn.execute(sql)
	
	sql =	"select c.totalItems, c.totalWeight, a.qty, b.itemID, b.itemPic, b.itemDesc, b.price_our " &_
			"from shoppingCart a " &_
				"left join we_Items b on a.itemID = b.itemID " &_
				"outer apply (" &_
					"select sum(qty) as totalItems, sum(itemWeight * ia.qty) as totalWeight, sessionID " &_
					"from shoppingCart ia " &_
						"left join we_Items ib on ia.itemID = ib.itemID " &_
					"where sessionID = " & useSessionID & " and ia.purchasedorderid is null " &_
					"group by sessionID" &_
				") c " &_
			"where a.sessionID = " & useSessionID & " and a.purchasedorderid is null"
	session("errorSQL") = sql
	Set rs = oConn.execute(sql)
	
	if rs.EOF or useSessionID = 0 then
		response.Redirect("/")
	else
		itemCnt = rs("totalItems")
		totalWeight = rs("totalWeight")
		
		itemDetails = ""
		orderAmt = 0
		do while not rs.EOF
			qty = rs("qty")
			itemID = rs("itemID")
			itemDesc = rs("itemDesc")
			itemPrice = rs("price_our")
            weDataStr = weDataStr & "window.WEDATA.pageData.cartItems.push({ itemId: "& jsStr(itemID) &", qty: "& jsStr(qty) &", price: "& jsStr(itemPrice) &"});"
            
			itemDetails = itemDetails & qty & "##"
			itemDetails = itemDetails & itemID & "##"
			itemDetails = itemDetails & itemDesc & "##"
			itemDetails = itemDetails & itemPrice & "@@"
			orderAmt = orderAmt + (qty * itemPrice)
			rs.movenext
		loop
		if len(itemDetails) > 0 then itemDetails = left(itemDetails,len(itemDetails)-2)
		itemDetailArray = split(itemDetails,"@@")
	end if
	nSubTotal = orderAmt
%>
<!--#include virtual="/template/promoCalc.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/template/top.asp"-->
<%
	discountTotal = prepInt(discountTotal)
	discountedCartTotal = orderAmt - discountTotal
%>
<div id="secureRow">
   	<div id="secureImg"><img src="/images/mobile/retina/checkout/we-m-icon-lock.png" height="23" /></div>
    <div id="secureText">Shipping Information</div>
</div>
<form name="checkoutForm" method="post" action="/checkout_step3.htm" onsubmit="return(verifyForm())">
<div id="entryFormContainer">
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">Email:</div>
        </div>
        <div id="entryField"><input id="dataEntryInput" type="text" name="email" value="" onblur="checkEmail(this.value, <%=mySession%>)" /></div>
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">First Name:</div>
        </div>
        <div id="entryField"><input id="dataEntryInput" type="text" name="fname" value="" /></div>
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">Last Name:</div>
        </div>
        <div id="entryField"><input id="dataEntryInput" type="text" name="lname" value="" /></div>
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">Phone:</div>
        </div>
        <div id="entryField"><input id="dataEntryInput" type="tel" name="phone" value="" /></div>
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">Address 1:</div>
        </div>
        <div id="entryField"><input id="dataEntryInput" type="text" name="sAddress1" value="" /></div>
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"></div>
            <div id="entryTitleText">Address 2:</div>
        </div>
        <div id="entryField"><input id="dataEntryInput" type="text" name="sAddress2" value="" /></div>
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">City:</div>
        </div>
        <div id="entryField"><input id="dataEntryInput" type="text" name="sCity" value="" /></div>
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">State:</div>
        </div>
        <div id="entryField">
            <select id="dataEntryInput" name="sState" onchange="changeTax(this.value)">
                <option value=""></option>
                <%getStates(bState)%>
            </select>
        </div>
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">Zip / Postal Code:</div>
        </div>
        <div id="entryField"><input id="dataEntryInput" type="text" name="sZip" value="" onblur="getNewShipping()" /></div>
    </div>
</div>
<div id="checkBoxRow1">
	<div id="checkmarkIcon">
    	<img src="/images/mobile/retina/checkout/we-m-button-checkbox-on.png" width="32" border="0" id="emailMeIcon" onclick="clickCheck('emailMe')" />
        <input type="hidden" name="emailMe" value="1" />
    </div>
    <div id="checkmarkText">Please notify me via email of exclusive periodic coupon code offers.</div>
</div>
<div id="checkBoxRow2">
	<div id="checkmarkIcon"><img src="/images/mobile/retina/checkout/we-m-button-checkbox-off.png" width="32" border="0" id="billingAddrIcon" onclick="clickCheck('billingAddr')" /></div>
    <div id="checkmarkText">Check if billing address is different than the shipping address.</div>
</div>
<div id="billingAddrForm" style="display:none;">
	<div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">Address 1:</div>
        </div>
        <div id="entryField"><input id="dataEntryInput" type="text" name="bAddress1" value="" /></div>
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"></div>
            <div id="entryTitleText">Address 2:</div>
        </div>
        <div id="entryField"><input id="dataEntryInput" type="text" name="bAddress2" value="" /></div>
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">City:</div>
        </div>
        <div id="entryField"><input id="dataEntryInput" type="text" name="bCity" value="" /></div>
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">State:</div>
        </div>
        <div id="entryField">
            <select id="dataEntryInput" name="bState">
                <option value=""></option>
                <%getStates(bState)%>
            </select>
        </div>
    </div>
    <div id="dataEntryRow">
        <div id="entryTitle">
            <div id="entryTitleStar"><img src="/images/mobile/retina/checkout/we-m-icon-astric.png" width="9" border="0" /></div>
            <div id="entryTitleText">Zip / Postal Code:</div>
        </div>
        <div id="entryField"><input id="dataEntryInput" type="text" name="bZip" value="" /></div>
    </div>
</div>
<div id="secureRow">
   	<div id="secureImg"><img src="/images/mobile/retina/checkout/we-m-icon-lock.png" height="23" /></div>
    <div id="secureText">Shipping Method</div>
</div>
<div id="checkBoxRow1">
	<div id="checkmarkIcon">
    	<img src="/images/mobile/retina/checkout/we-m-button-checkbox-on.png" width="32" border="0" id="shipping0Icon" onclick="clickShip('shipping0')" />
        <input type="hidden" name="shipping0Price" value="0" />
    </div>
    <div id="checkmarkText">
    	<div id="boldOrange">FREE SHIPPING - USPS First Class</div>
        <div id="arrivesIn">Arrives in 4-10 buisiness days.</div>
    </div>
</div>
<div id="checkBoxRow2">
	<div id="checkmarkIcon">
    	<img src="/images/mobile/retina/checkout/we-m-button-checkbox-off.png" width="32" border="0" id="shipping2Icon" onclick="clickShip('shipping2')" />
        <input type="hidden" name="shipping2Price" value="6.99" />
    </div>
    <div id="checkmarkText">
    	<div id="boldOrange">$6.99 - USPS Priority Mail</div>
        <div id="arrivesIn">Arrives in 2-4 buisiness days.</div>
    </div>
</div>
<div id="checkBoxRow1">
	<div id="checkmarkIcon">
    	<img src="/images/mobile/retina/checkout/we-m-button-checkbox-off.png" width="32" border="0" id="shipping3Icon" onclick="clickShip('shipping3')" />
        <input type="hidden" name="shipping3Price" value="24.99" />
    </div>
    <div id="checkmarkText">
    	<div id="boldOrange">$24.99 - USPS Express Mail</div>
        <div id="arrivesIn">Arrives in 1-2 buisiness days.</div>
    </div>
</div>
<div id="otherShippingOptions">
	<div id="waitingForZip">Please Enter Your Zip / Postal Code<br />to see the other shipping options</div>
</div>
<div id="secureRow">
   	<div id="secureImg"><img src="/images/mobile/retina/checkout/we-m-icon-lock.png" height="23" /></div>
    <div id="secureText">Order Review</div>
</div>
<div id="orderReviewContainer">
	<div id="orderReviewBox">
    	<div id="orderReviewRow">
        	<div class="orderReviewText">Subtotal:</div>
            <div class="orderReviewValue">
				<%=formatCurrency(orderAmt,2)%>
                <input type="hidden" name="subtotal" value="<%=orderAmt%>" />
            </div>
        </div>
        <div id="orderReviewRow">
        	<div id="taxTotal" class="orderReviewText">Tax:</div>
            <div id="taxValue" class="orderReviewValue">
				<%=formatCurrency(0,2)%>
                <input type="hidden" name="tax" value="0" />
            </div>
        </div>
		<%
		if sPromoCode <> "" then
			session("promocode") = sPromoCode
		%>
        <div id="orderReviewRow">
        	<div class="orderReviewText">Discount:</div>
            <div class="orderReviewValueRed"><%=formatcurrency(0-discountTotal,2)%></div>
        </div>
        <input type="hidden" name="sPromoCode" value="<%=sPromoCode%>">
        <input type="hidden" name="nPromo" value="<%=couponid%>">
		<%end if%>
        <div id="orderReviewRow">
        	<div class="orderReviewText">Shipping:</div>
            <div id="shippingValue" class="orderReviewValueRed">
            	FREE
                <input type="hidden" name="shiptype" value="0" />
                <input type="hidden" name="shippingAmt" value="0" />
            </div>
        </div>
        <div id="orderReviewRow2">
        	<div id="orderReviewTextWhite">Grand Total:</div>
            <div id="grandTotalValue" class="orderReviewValueOrange">
				<%=formatCurrency(discountedCartTotal,2)%>
                <input type="hidden" name="grandTotal" value="<%=discountedCartTotal%>" />
            </div>
        </div>
    </div>
</div>
<div id="continueCheckout"><input type="image" src="/images/mobile/retina/checkout/we-m-button-continuecheckout.png" width="282" border="0" /></div>
</form>
<div id="emailReturn" style="display:none;"></div>
<script>
window.WEDATA.pageType = 'checkout2';
window.WEDATA.pageData = {
    discountTotal: <%= jsStr(discountTotal) %>,
    subTotal: <%= jsStr(nSubTotal) %>,
    cartItems: []
};
<%= weDataStr %>
</script>

<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">
	var orderAmt = <%=prepInt(orderAmt)%>;
	var discountedCartTotal = <%=prepInt(discountedCartTotal)%>;
	var totalItems = <%=prepInt(itemCnt)%>;
	var totalWeight = <%=prepInt(totalWeight)%>;
	var curShip = "shipping0"
	
	function clickCheck(which) {
		if (document.getElementById(which + "Icon").src == 'https://m.wirelessemporium.com/images/mobile/retina/checkout/we-m-button-checkbox-on.png') {
			document.getElementById(which + "Icon").src = '/images/mobile/retina/checkout/we-m-button-checkbox-off.png'
			if (which == "billingAddr") {
				document.getElementById("billingAddrForm").style.display = 'none'
			}
			else {
				eval("document.checkoutForm." + which + ".value = 0")
			}
		}
		else {
			document.getElementById(which + "Icon").src = '/images/mobile/retina/checkout/we-m-button-checkbox-on.png'
			if (which == "billingAddr") {
				document.getElementById("billingAddrForm").style.display = ''
			}
			else {
				eval("document.checkoutForm." + which + ".value = 1")
			}
		}
	}
	
	function clickShip(which) {
		if (document.getElementById(which + "Icon").src != 'https://m.wirelessemporium.com/images/mobile/retina/checkout/we-m-button-checkbox-on.png') {
			document.getElementById(which + "Icon").src = '/images/mobile/retina/checkout/we-m-button-checkbox-on.png'
			document.getElementById(curShip + "Icon").src = '/images/mobile/retina/checkout/we-m-button-checkbox-off.png'
			newShip = eval("document.checkoutForm." + which + "Price.value")
			shipType = which.replace('shipping','')
			if (newShip == 0) {
				document.getElementById("shippingValue").innerHTML = 'FREE<input type="hidden" name="shiptype" value="' + shipType + '" /><input type="hidden" name="shippingAmt" value="' + newShip + '" />'
			}
			else {
				document.getElementById("shippingValue").innerHTML = '$' + CurrencyFormatted(newShip) + '<input type="hidden" name="shiptype" value="' + shipType + '" /><input type="hidden" name="shippingAmt" value="' + newShip + '" />'
			}
			curShip = which
			updateGrandTotal()
		}
	}
	
	function checkEmail(email,mySession) {
		if (email.indexOf("@") > 0) {
			ajax('/ajax/customerData.asp?email=' + email,'emailReturn');
			setTimeout("displayData(document.getElementById('emailReturn').innerHTML)",1500);
			ajax('/ajax/remarketing.asp?email=' + email + '&mySession=' + mySession + '&siteID=0','dumpZone');
		}
	}
	
	function changeTax(state) {
		var nCATax = CurrencyFormatted(orderAmt * <%=Application("taxMath")%>);
	
		if (state == "CA") {
			document.getElementById("taxTotal").innerHTML = 'Tax <span id="taxMsg">(<%=Application("taxDisplay")%>% Tax for CA shipments)</span>:'
			document.getElementById("taxValue").innerHTML = '$' + nCATax + '<input type="hidden" name="tax" value="' + nCATax + '" />'
		} else {
			document.getElementById("taxTotal").innerHTML = 'Tax:'
			document.getElementById("taxValue").innerHTML = '$0.00<input type="hidden" name="tax" value="0" />'
		}
		updateGrandTotal();
	}
	
	function CurrencyFormatted(amount) {
		var i = parseFloat(amount);
		if(isNaN(i)) { i = 0.00; }
		var minus = '';
		if(i < 0) { minus = '-'; }
		i = Math.abs(i);
		i = parseInt(i * 100);
		i = i / 100;
		s = new String(i);
		if(s.indexOf('.') < 0) { s += '.00'; }
		if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
		s = minus + s;
		return s;
	}
	
	function updateGrandTotal() {
		curTax = document.checkoutForm.tax.value;
		curShipping = document.checkoutForm.shippingAmt.value;
		grandTotal = parseFloat(curTax) + parseFloat(curShipping) + parseFloat(discountedCartTotal);
		document.getElementById("grandTotalValue").innerHTML = '$' + CurrencyFormatted(grandTotal) + '<input type="hidden" name="grandTotal" value="' + grandTotal + '" />';
	}
	
	function displayData(dataReturn) {
		if (dataReturn != "no data" && dataReturn != "") {
			var dataArray = dataReturn.split("@@")
			if (document.checkoutForm.fname.value == "") { document.checkoutForm.fname.value = dataArray[0] }
			if (document.checkoutForm.lname.value == "") { document.checkoutForm.lname.value = dataArray[1] }
			if (document.checkoutForm.sAddress1.value == "") { document.checkoutForm.sAddress1.value = dataArray[2] }
			if (document.checkoutForm.sAddress2.value == "") { document.checkoutForm.sAddress2.value = dataArray[3] }
			if (document.checkoutForm.sCity.value == "") { document.checkoutForm.sCity.value = dataArray[4] }
			if (document.checkoutForm.sState.selectedIndex == 0) {
				for (i=0;i<50;i++) {
					if (document.checkoutForm.sState.options[i].value == dataArray[5]) {
						document.checkoutForm.sState.selectedIndex = i
						changeTax(dataArray[5])
					}
				}
			}
			if (document.checkoutForm.sZip.value == "") {
				document.checkoutForm.sZip.value = dataArray[6]
				getNewShipping();
			}
			if (document.checkoutForm.phone.value == "") { document.checkoutForm.phone.value = dataArray[7] }
			getNewShipping()
		}
	}
	
	function getNewShipping() {
		zipcode = document.checkoutForm.sZip.value;
		curShippingTotal = 0
		curShippingType = 0	
		ajax('/ajax/shippingDetails.asp?zip=' + zipcode + '&weight=' + totalWeight + '&shiptype=' + curShippingType + '&nTotalQuantity=' + totalItems + '&nSubTotal=' + orderAmt + '&useFunction=upscode2&newMobile=1','otherShippingOptions');
	}
	
	function verifyForm() {
		if (document.checkoutForm.email.value == "") {
			alert("You must enter your email address")
			document.checkoutForm.email.focus()
			return false
		}
		else if (document.checkoutForm.fname.value == "") {
			alert("You must enter your first name")
			document.checkoutForm.fname.focus()
			return false
		}
		else if (document.checkoutForm.lname.value == "") {
			alert("You must enter your last name")
			document.checkoutForm.lname.focus()
			return false
		}
		else if (document.checkoutForm.phone.value == "") {
			alert("You must enter your phone number")
			document.checkoutForm.phone.focus()
			return false
		}
		else if (document.checkoutForm.sAddress1.value == "") {
			alert("You must enter your address")
			document.checkoutForm.sAddress1.focus()
			return false
		}
		else if (document.checkoutForm.sCity.value == "") {
			alert("You must enter your city")
			document.checkoutForm.sCity.focus()
			return false
		}
		else if (document.checkoutForm.sState.selectedIndex == 0) {
			alert("You must enter your state")
			document.checkoutForm.sState.focus()
			return false
		}
		else if (document.checkoutForm.sZip.value == "") {
			alert("You must enter your zip/postal code")
			document.checkoutForm.sZip.focus()
			return false
		}
		else {
			if (document.getElementById("billingAddrForm").style.display = '') {
				if (document.checkoutForm.bAddress1.value == "") {
					alert("You must enter your billing address")
					document.checkoutForm.bAddress1.focus()
					return false
				}
				else if (document.checkoutForm.bCity.value == "") {
					alert("You must enter your billing city")
					document.checkoutForm.bCity.focus()
					return false
				}
				else if (document.checkoutForm.bState.selectedIndex == 0) {
					alert("You must enter your billing state")
					document.checkoutForm.bState.focus()
					return false
				}
				else if (document.checkoutForm.bZip.value == "") {
					alert("You must enter your billing zip/postal code")
					document.checkoutForm.bZip.focus()
					return false
				}
				else {
					return true
				}
			}
			else {
				return true
			}
		}
	}
</script>