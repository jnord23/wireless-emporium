<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
	response.buffer = true
	response.expires = -1
	response.ExpiresAbsolute = Now() - 1
	response.CacheControl = "no-cache"
	
	if request.querystring("utm_source") = "AdMob" then
		session("adTracking") = "AdMob"
		session("additionalQS") = "?utm_source=AdMob&utm_medium=" & request.querystring("utm_medium") & "&utm_campaign=" & request.querystring("utm_campaign")
	end if
	
	if isnull(request.Cookies("mySession")) or len(request.Cookies("mySession")) < 1 then response.Cookies("mySession") = session.SessionID
	
	function jnord(msg)
		if session("acctID") = "575054" then
			response.Write("<table border='0' width='100%' style='font-size:8pt;'><tr><td>TEST:" & msg & "</td></tr></table>")
			response.End()
		end if
	end function
	
	r = SQLquote(request.QueryString("r"))
	itemID = SQLquote(request.QueryString("i"))
	brandID = SQLquote(request.QueryString("b"))
	modelID = SQLquote(request.QueryString("m"))
	categoryID = SQLquote(request.QueryString("c"))
	mainSite = SQLquote(request.QueryString("ms"))
	saveContactUs = SQLquote(request.QueryString("saveContactUs"))
	email = SQLquote(request.Form("email"))
	message = SQLquote(request.Form("message"))
	
	if isnull(r) or len(r) < 1 or not isnumeric(r) then r = 0
	if isnull(itemID) or len(itemID) < 1 or not isnumeric(itemID) then itemID = 0
	if isnull(brandID) or len(brandID) < 1 or not isnumeric(brandID) then brandID = 0
	if isnull(modelID) or len(modelID) < 1 or not isnumeric(modelID) then modelID = 0
	if isnull(categoryID) or len(categoryID) < 1 or not isnumeric(categoryID) then categoryID = 0
	if isnull(mainSite) or len(mainSite) < 1 then mainSite = ""
	if isnull(saveContactUs) or len(saveContactUs) < 1 or not isnumeric(saveContactUs) then saveContactUs = 0
	if isnull(email) or len(email) < 1 then email = ""
	if isnull(message) or len(message) < 1 then message = ""
	
	remindEmailSent = 0	
	if saveContactUs = 1 then
		cuName = SQLquote(request.Form("name"))
		cuEmail = SQLquote(request.Form("email"))
		cuSubect = SQLquote(request.Form("subject"))
		cuPhone = SQLquote(request.Form("phonenumber"))
		cuProduct = SQLquote(request.Form("product"))
		cuOrderNum = SQLquote(request.Form("orderno"))
		cuQuestion = SQLquote(request.Form("question"))
		
		dim cdo_to, cdo_from, cdo_subject, cdo_body
		select case cuSubect
			case "Returns / Exchanges / Request RMA" : cdo_to = "returns@WirelessEmporium.com"
			case "Marketing Inquiries" : cdo_to = "marketing@WirelessEmporium.com"
			case "General Inquiries" : cdo_to = "sales@WirelessEmporium.com"
			case else : cdo_to = "service@WirelessEmporium.com"
		end select
		cdo_from = "contactUsMobile@wirelessemporium.com"
		'cdo_to = "jon@wirelessemporium.com"
		
		cdo_subject = "Contact Us - Mobile Site"
		cdo_body = "<p><img src=""http://www.wirelessemporium.com/images/WElogo.gif"">" & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Name: </b>" & cuName & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Email: </b><a href='mailto:" & cuEmail & "'>" & cuEmail & "</a></p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Phone: </b>" & cuPhone & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Type of Inquiry: </b><font color=""#FF0000"">" & cuSubect & "</font></p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Order #: </b>" & cuOrderNum & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Product: </b>" & cuProduct & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Question: </b>" & replace(cuQuestion,vbcrlf,"<br>") & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Date/Time Sent: </b>" & now & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>&nbsp;</p><p>" & Request.ServerVariables("REMOTE_ADDR") & "</p>" & vbcrlf
		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
	elseif email <> "" then
		sql = "select itemDesc from we_items where itemID = " & itemID
		session("errorSQL") = sql
		set remindRS = oConn.execute(sql)
		
		itemDesc = remindRS("itemDesc")
		remindRS = null
		
		cdo_to = email
		cdo_from = "remindMe@wirelessemporium.com"
		'cdo_to = "jon@wirelessemporium.com"
		
		cdo_subject = "Product Reminder - Mobile Site"
		cdo_body = "<p><img src=""http://www.wirelessemporium.com/images/WElogo.gif"">" & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>This is a product reminder email that was sent from the WirelessEmproium.com mobile website. Below is a message that was entered and a link to the product.</p>" & vbcrlf
		cdo_body = cdo_body & "<p>" & message & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>Product Link: <a href='http://www.wirelessemporium.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".asp'>http://www.wirelessemproium.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".asp</a></p>"
		
		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		
		remindEmailSent = 1
	end if
	
	if mainSite <> "" then
		if mainSite = "full" then
			sql = "update we_mobileTracking set mainSite = 1 where sessionID = '" & request.Cookies("mySession") & "'"
			session("errorSQL") = sql
			oConn.execute(sql)
			
			response.Redirect("http://www.wirelessemporium.com/?mobileAccess=full")
		end if
	end if
	
	sql = "execute sp_mobileTracking '" & request.ServerVariables("REMOTE_ADDR") & "','" & request.ServerVariables("LOCAL_ADDR") & "','" & request.ServerVariables("HTTP_USER_AGENT") & "','" & request.Cookies("mySession") & "','" & session.SessionID & "'"
	session("errorSQL") = sql
	oConn.execute(sql)
	
	session("ready4Back") = 1
	
	if r = 1 then
		session.Contents.Remove("crumbLvl1")
		session.Contents.Remove("crumbLvl2")
		session.Contents.Remove("crumbLvl3")
		session.Contents.Remove("crumbLvl4")
		
		session.Contents.Remove("prevPage3")
		session.Contents.Remove("prevPage2")
		session.Contents.Remove("prevPage1")
		session.Contents.Remove("curPage")
		response.Redirect("/")
	end if
%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Mobile Forward</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=UTF-8">
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="EXPIRES" CONTENT="0">
    <META HTTP-EQUIV="REFRESH" CONTENT="1;URL=/mobile/?i=<%=itemID%>&b=<%=brandID%>&m=<%=modelID%>&c=<%=categoryID%>">
</head>
<style type="text/css">
	body{font:13px/1.231 arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;margin:0;padding:0;}
</style>
<body onload="window.location='/mobile/?i=<%=itemID%>&b=<%=brandID%>&m=<%=modelID%>&c=<%=categoryID%>'">
<table border="0" cellpadding="0" cellspacing="0" width="100%" onclick="moveFwd()">
	<tr><td><img src="/images/blank.gif" border="0" width="1" height="100" /></td></tr>
    <tr>
    	<td>
            <table border="0" cellpadding="3" cellspacing="0" width="100%" align="center" style="background-image:url(/images/mobile/loading.gif);background-repeat:no-repeat;background-position:center;">
                <tr><td><img src="/images/blank.gif" border="0" width="1" height="100" /></td></tr>
                <tr><td align="center">Loading WE Mobile...</td></tr>
                <tr><td align="center" style="font-size:9px;">If you are not redirected please tap or refresh this page</td></tr>
            </table>
        </td>
    </tr>
    <tr><td><img src="/images/blank.gif" border="0" width="1" height="100" /></td></tr>
</table>
</body>
</html>
<script language="javascript">
	<% if remindEmailSent = 1 then %>
	alert("Reminder email sent")
	<% end if %>
	<% if saveContactUs = 1 then %>
	alert("Contact Us Email Sent to WirelessEmporium.com")
	<% end if %>
	function moveFwd() {
		window.location='/mobile/?i=<%=itemID%>&b=<%=brandID%>&m=<%=modelID%>&c=<%=categoryID%>'
	}
	setTimeout("window.location='/mobile/?i=<%=itemID%>&b=<%=brandID%>&m=<%=modelID%>&c=<%=categoryID%>'",500)
</script>
<!-- Google Analytics Start -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1311669-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Google Analytics End -->