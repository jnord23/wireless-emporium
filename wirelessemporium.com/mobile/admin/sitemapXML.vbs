set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getLiveConn()

dim fs, file, path, nFileIdx, fileBaseName, folderName, nRows
nRows = 0
nFileIdx = 1
folderName = "C:\inetpub\wwwroot\wirelessemporium.com\mobile"
fileBaseName = "sitemap"
set fs = CreateObject("Scripting.FileSystemObject")

path = folderName & "\" & fileBaseName & nFileIdx & ".xml"
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if
set file = fs.CreateTextFile(path)
file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
file.WriteLine "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">"
file.WriteLine "<url>"
file.WriteLine vbtab & "<loc>http://m.wirelessemporium.com/</loc>"
file.WriteLine vbtab & "<priority>0.90</priority>"
file.WriteLine "</url>"

dim temp(100)
temp(1) = "/apple-phone-accessories.htm"
temp(2) = "/blackberry-phone-accessories.htm"
temp(3) = "/casio-phone-accessories.htm"
temp(4) = "/hp-phone-accessories.htm"
temp(5) = "/htc-phone-accessories.htm"
temp(6) = "/huawei-phone-accessories.htm"
temp(7) = "/kyocera-phone-accessories.htm"
temp(8) = "/lg-phone-accessories.htm"
temp(9) = "/motorola-phone-accessories.htm"
temp(10) = "/nokia-phone-accessories.htm"
temp(11) = "/pantech-phone-accessories.htm"
temp(12) = "/samsung-phone-accessories.htm"
temp(13) = "/sony-phone-accessories.htm"
temp(14) = "/zte-phone-accessories.htm"
temp(15) = "/other-phone-accessories.htm"

dim a, URL
for a = 1 to 15
	URL = temp(a)
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://m.wirelessemporium.com" & URL & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"
next




'============================================== BRAND-MODEL
dim strBrandModelSQL, objRsBrandModel
strBrandModelSQL = 	"select	distinct a.modelid, b.brandid, b.brandname, isnull(a.modelname, '') modelname, a.linkName" & vbcrlf & _
					"from 	we_models a with (nolock) join we_brands b with (nolock) " & vbcrlf & _
					"	on 	a.brandid = b.brandid join we_items c with (nolock)" & vbcrlf & _
					"	on	a.modelid = c.modelid" & vbcrlf & _
					"where 	a.modelname is not null and a.hidelive = 0" & vbcrlf & _
					"	and a.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
					"	and (a.modelname not like 'all model%' or a.modelname not like 'universal%')" & vbcrlf & _
					"	and	c.hidelive = 0 and c.inv_qty <> 0 and c.price_our > 0" & vbcrlf
					
set objRsBrandModel	= oConn.execute(strBrandModelSQL)

do until objRsBrandModel.EOF
	url = formatSEO(objRsBrandModel("brandName")) & "-" & formatSEO(objRsBrandModel("modelName")) & "-accessories.htm"
	
	call fWriteFile(url)
	objRsBrandModel.movenext
loop
set objRsBrandModel = nothing
'============================================== BRAND-MODEL END	


'============================================== brand-model-category
sql	=	"select	distinct a.typeid, a.typeName, c.brandid, c.brandname, m.modelid, m.modelname" & vbcrlf & _
		"from	v_subtypematrix a with (nolock) join we_items b with (nolock)" & vbcrlf & _
		"	on	a.subtypeid = b.subtypeid join we_brands c with (nolock)" & vbcrlf & _
		"	on	b.brandid = c.brandid join we_models m with (nolock)" & vbcrlf & _
		"	on	b.modelid = m.modelid" & vbcrlf & _
		"where	a.typeid not in (5,8,16)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0 " & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct a.typeid, a.typeName, c.brandid, c.brandname, m.modelid, m.modelname" & vbcrlf & _
		"from	v_subtypematrix a with (nolock) join we_subRelatedItems t with (nolock)" & vbcrlf & _
		"	on	a.subtypeid = t.subtypeid join we_brands c with (nolock)" & vbcrlf & _
		"	on	t.brandid = c.brandid join we_items b with (nolock)" & vbcrlf & _
		"	on	t.itemid = b.itemid join we_models m with (nolock)" & vbcrlf & _
		"	on	t.modelid = m.modelid" & vbcrlf & _
		"where	a.typeid not in (5,8,16)" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"	and b.hidelive = 0" & vbcrlf & _
		"	and b.inv_qty <> 0" & vbcrlf & _
		"	and b.price_our > 0" & vbcrlf & _
		"union" & vbcrlf & _
		"select	distinct 5 typeid, 'Handsfree Bluetooth' typename, c.brandid, c.brandname, m.modelid, m.modelname" & vbcrlf & _
		"from	we_brands c with (nolock) join we_models m with (nolock)" & vbcrlf & _
		"	on	c.brandid = m.brandid" & vbcrlf & _
		"where	m.handsfreetypes is not null" & vbcrlf & _
		"	and	c.brandid in (1,2,3,4,5,6,7,9,10,11,14,15,16,17,18,19,20)" & vbcrlf & _
		"order by 1, 3" & vbcrlf
		
Set RS = CreateObject("ADODB.Recordset")	
set RS = oConn.execute(sql)	
do until RS.EOF
	url = "sb-" & RS("brandID") & "-sm-" & RS("modelID") & "-sc-" & RS("typeID") & "-" & formatSEO(RS("typeName")) & "-" & formatSEO(RS("brandName")) & "-" & formatSEO(RS("modelName")) & ".htm"
	
	call fWriteFile(url)
	RS.movenext
loop
set RS = nothing
'============================================== brand-model-category end

'============================================== product page START
dim pSQL, objRsProduct
pSQL	=	"select	a.itemid, a.itemdesc " & vbcrlf & _
			"from	we_items a with (nolock) join we_models b with (nolock) " & vbcrlf & _
			"	on	a.modelid = b.modelid join (select distinct itemid from we_itemssiteready with (nolock)) c" & vbcrlf & _			
			"	on	a.itemid = c.itemid" & vbcrlf & _
			"where	a.hidelive = 0 and a.inv_qty <> 0  " & vbcrlf & _
			"	and a.price_our > 0 " & vbcrlf & _
			"	and b.hidelive = 0 " & vbcrlf & _			
			"order by 1"
			
set objRsProduct = oConn.execute(pSQL)

do until objRsProduct.EOF
	url = "p-" & objRsProduct("itemID") & "-" & formatSEO(objRsProduct("itemDesc")) & ".htm"

	call fWriteFile(url)

	objRsProduct.movenext
loop
set objRsProduct = nothing
'============================================== product page END

file.WriteLine "</urlset>"
file.close()
set file = nothing


'============================================== index site map START
path = folderName & "\" & fileBaseName & ".xml"
'If the file already exists, delete it.
if fs.FileExists(path) then
	fs.DeleteFile(path)
end if
set file = fs.CreateTextFile(path)
file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
file.WriteLine "<sitemapindex xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"">"

for i=1 to nFileIdx
	file.WriteLine "<sitemap>"


	file.WriteLine vbtab & "<loc>http://m.wirelessemporium.com/" & fileBaseName & i & ".xml</loc>"
	file.WriteLine vbtab & "<lastmod>" & RFC3339(now) & "</lastmod>"
	file.WriteLine "</sitemap>"
next

file.WriteLine "</sitemapindex>"
file.close()
set file = nothing
'============================================== index site map END

		

sub fWriteFile(pStrUrl)
	nRows = nRows + 1
	
	if nRows > 45000 then
		nFileIdx = nFileIdx + 1
		nRows = 0
		file.WriteLine "</urlset>"
		file.close()
		set file = nothing
		
		path = folderName & "\" & fileBaseName & nFileIdx & ".xml"
		if fs.FileExists(path) then
			fs.DeleteFile(path)
		end if
		set file = fs.CreateTextFile(path)
		file.WriteLine "<?xml version=""1.0"" encoding=""UTF-8""?>"
		file.WriteLine "<urlset xmlns=""http://www.sitemaps.org/schemas/sitemap/0.9"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.google.com/schemas/sitemap/0.84 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"">"
	end if
	
	file.WriteLine "<url>"
	file.WriteLine vbtab & "<loc>http://m.wirelessemporium.com/" & pStrUrl & "</loc>"
	file.WriteLine vbtab & "<priority>0.90</priority>"
	file.WriteLine "</url>"	
end sub

function RFC3339(myDate)
	dim myDays, myMonth, myYear, myHours, myMonths, mySeconds
	myDate = dateAdd("h",8,myDate)
	myDays = zeroPad(day(myDate),2)
	myMonth = zeroPad(month(myDate),2)
	myYear = year(myDate)
	myHours = zeroPad(hour(myDate),2)
	myMinutes = zeroPad(minute(myDate),2)
	mySeconds = zeroPad(second(myDate),2)
	RFC3339 = myYear & "-" & myMonth & "-" & myDays & "T" & myHours & ":" & myMinutes & ":" & mySeconds & "Z"
end function
 
function zeroPad(m,t)
	zeroPad = String(t-Len(m),"0") & m
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(replace(formatSEO,"?","-"), "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
end function

call CloseConn(oConn)
sub CloseConn( byref objConn)
	if IsObject( objConn) then 
		if not objConn is nothing then
			objConn.Close()
			set objConn = nothing
			objConn = null
		end if
	end if
	
	if IsObject(weUtil) then 
		if not weUtil is nothing then
			set weUtil = nothing
			weUtil = null
		end if
	end if	
end sub