<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
'Option Explicit
noCommentBox = true
Const lngMaxFormBytes = 200

forceErrorData = 1

Dim objASPError, blnErrorWritten, strServername, strServerIP, strRemoteIP
Dim strMethod, lngPos, datNow, strQueryString, strURL

Set objASPError = Server.GetLastError

dim pageTitle
pageTitle = "The page cannot be displayed"
%>
<!--#include virtual="/template/top.asp"-->
<table border="0" cellspacing="0" cellpadding="0" width="300" align="center">
    <tr>
        <td align="left" valign="top" width="100%" class="breadcrumbFinal"><img src="/images/spacer.gif" width="1" height="10"></td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <% if forceErrorData = 1 then %>
            <table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">Category:</td>
                    <td align="left"><%=objASPError.Category%></td>
                </tr>
                <tr>
                    <td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">ASPCode:</td>
                    <td align="left"><%=objASPError.ASPCode%></td>
                </tr>
                <tr>
                    <td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">ASPDescription:</td>
                    <td align="left"><%=objASPError.ASPDescription%></td>
                </tr>
                <tr>
                    <td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">Number:</td>
                    <td align="left"><%=objASPError.Number%></td>
                </tr>
                <tr>
                    <td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">Description:</td>
                    <td align="left"><%=objASPError.Description%></td>
                </tr>
                <tr>
                    <td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">File:</td>
                    <td align="left"><%=objASPError.File%></td>
                </tr>
                <tr>
                    <td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">Source:</td>
                    <td align="left"><%=objASPError.Source%></td>
                </tr>
                <tr>
                    <td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">Column:</td>
                    <td align="left"><%=objASPError.Column%></td>
                </tr>
                <tr>
                    <td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">Line:</td>
                    <td align="left"><%=objASPError.Line%></td>
                </tr>
                <tr>
                    <td align="right" style="font-weight:bold" nowrap="nowrap" valign="top">errorSQL:</td>
                    <td align="left"><%=session("errorSQL")%></td>
                </tr>
            </table>
            <%
                response.End()
            end if
            %>
            <%if inStr(objASPError.Description,"Communication link failure") > 0 or inStr(objASPError.Description,"[DBNETLIB]") > 0 then%>
                <p style="font-size:28px;color:#000000;font-weight:bold;font-family:Verdana,Arial,Helvetica,sans-serif;">Website Temporarily Down</p>
                <h4>
                    To place an order via telephone, call us at:<br>
                    (888) 725-7575<br>
                    (Available Mon-Fri 9am-5pm Pacific Time)<br>
                </h4>
                <p>There is a problem with the web server.</p>
                <hr>
                <p>Our sincere apologies for any inconvenience this may have caused.</p>
                <hr>
                <p>Contact the <a href="mailto:webmaster@wirelessemporium.com?subject=From%20Website%20Down%20Page">Webmaster</a> if you wish to report any other information concerning this URL address.</p>
            <%else%>
                <p style="font-size:28px;color:#000000;font-weight:bold;font-family:Verdana,Arial,Helvetica,sans-serif;">Website Error</p>
                <h4>
                    To place an order via telephone, call us at:<br>
                    (888) 725-7575<br>
                    (Available Mon-Fri 9am-5pm Pacific Time)
                </h4>
                <hr>
                <p>There is a problem with the page you are trying to reach and it cannot be displayed.</p>
                <hr>
                <p>An e-mail has been sent to the <a href="mailto:webmaster@wirelessemporium.com?subject=From%20ASP%20Error%20Page">Webmaster</a> about this problem.</p>
                <p>Our sincere apologies for any inconvenience this may have caused.</p>
                <hr>
                <p>Contact the <a href="mailto:webmaster@wirelessemporium.com?subject=From%20ASP%20Error%20Page">Webmaster</a> if you wish to report any other information concerning this URL address.</p>
            <%end if%>
            <p>&nbsp;</p>
        </td>
    </tr>
</table>
<!--#include virtual="/template/bottom.asp"-->
<!-- Google Analytics Start -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1311669-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Google Analytics End -->