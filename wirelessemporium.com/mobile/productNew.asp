<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/Framework/IncludeTemplate/BasePage.asp"-->
<%
	pageName = "Product"
	
	dim itemID : itemID = prepInt(request.QueryString("itemID"))
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	
	sql =	"select d.brandID, c.modelID, e.typeID, e.typeName, d.brandName, c.modelName, c.modelImg, a.itemID, a.itemPic, a.itemDesc, a.itemLongDetail, b.inv_qty, a.partNumber,  " &_
			"a.price_our, a.price_retail, f.avgRating, a.bullet1, a.bullet2, a.bullet3, a.bullet4, a.bullet5, a.bullet6, a.bullet7, a.bullet8, a.bullet9, a.bullet10 " &_
			"from we_Items a " &_
				"left join we_Items b on a.partNumber = b.partNumber and b.master = 1 " &_
				"left join we_models c on a.modelID = c.modelID " &_
				"left join we_brands d on d.brandID = b.brandID " &_
				"left join we_types e on a.typeID = e.typeID " &_
				"outer apply (select PartNumbers, avg(rating) as avgRating from we_Reviews where PartNumbers = a.PartNumber and approved = 1 group by PartNumbers) f " &_
			"where a.hideLive = 0 and a.ghost = 0 and a.itemID = " & itemID
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	if rs.EOF then response.Redirect("/")
	
	dim brandID : brandID = prepInt(request.Cookies("mobileBrandID"))
	dim modelID : modelID = prepInt(request.Cookies("mobileModelID"))
	if brandID = 0 or modelID = 0 then
		brandID = prepInt(rs("brandID"))
		modelID = prepInt(rs("modelID"))
	end if
	dim typeID : typeID = prepInt(rs("typeID"))
	dim categoryName : categoryName = prepStr(rs("typeName"))
	breadCrumb = 1
	
	sql =	"select top 2 d.brandName, c.modelName, c.modelImg, a.itemID, a.itemPic, a.itemDesc, a.price_our, e.avgRating " &_
			"from we_Items a " &_
				"left join we_Items b on a.partNumber = b.partNumber and b.master = 1 " &_
				"left join we_models c on a.modelID = c.modelID " &_
				"left join we_brands d on d.brandID = " & brandID & " " &_
				"outer apply (select PartNumbers, avg(rating) as avgRating from we_Reviews where PartNumbers = a.PartNumber and approved = 1 group by PartNumbers) e " &_
			"where a.hideLive = 0 and a.ghost = 0 and b.inv_qty > 0 and a.modelID = " & modelID & " and a.typeID = 3 and a.itemID <> " & itemID
	session("errorSQL") = sql
	set relatedRS = oConn.execute(sql)
%>
<!--#include virtual="/template/top.asp"-->
<%	
	dim priceRetail : priceRetail = prepInt(rs("price_retail"))
	dim priceWE : priceWE = prepInt(rs("price_our"))
	dim itemPic : itemPic = rs("itemPic")
	dim avgRating : avgRating = prepInt(rs("avgRating"))
	dim itemDesc : itemDesc = prepStr(rs("itemDesc"))
	dim itemLongDetail : itemLongDetail = prepStr(rs("itemLongDetail"))
	dim inv_qty : inv_qty = prepInt(rs("inv_qty"))
	dim partNumber : partNumber = prepStr(rs("partNumber"))
	deHtml = 0
	do while instr(itemLongDetail,"<") > 0
		deHtml = deHtml + 1
		if instr(itemLongDetail,"<") > 1 then
			tempVal = mid(itemLongDetail,1,instr(itemLongDetail,"<")-1)
			tempVal = tempVal & mid(itemLongDetail,instr(itemLongDetail,">")+1)
			itemLongDetail = tempVal
		else
			tempVal = mid(itemLongDetail,instr(itemLongDetail,">")+1)
			itemLongDetail = tempVal
		end if
	loop
	dim bullet1 : bullet1 = prepStr(rs("bullet1"))
	dim bullet2 : bullet2 = prepStr(rs("bullet2"))
	dim bullet3 : bullet3 = prepStr(rs("bullet3"))
	dim bullet4 : bullet4 = prepStr(rs("bullet4"))
	dim bullet5 : bullet5 = prepStr(rs("bullet5"))
	dim bullet6 : bullet6 = prepStr(rs("bullet6"))
	dim bullet7 : bullet7 = prepStr(rs("bullet7"))
	dim bullet8 : bullet8 = prepStr(rs("bullet8"))
	dim bullet9 : bullet9 = prepStr(rs("bullet9"))
	dim bullet10 : bullet10 = prepStr(rs("bullet10"))
	
	myModelID = 0
	userAgent = request.ServerVariables("HTTP_USER_AGENT")
	userAgentArray = split(userAgent,";")
	for i = 0 to ubound(userAgentArray)
		if instr(userAgentArray(i),"Build") > 0 then
			userAgent = trim(left(userAgentArray(i),instr(userAgentArray(i),"Build")-2))
			
			sql =	"select a.modelID " &_
					"from we_models a " &_
						"left join we_brands b on a.brandID = b.brandID "
					"where b.brandName + '-' + a.modelName like '%" & userAgent & "%'"
			session("errorSQL") = sql
			set modelRS = oConn.execute(sql)
			
			if not modelRS.EOF then
				myModelID = prepInt(modelRS("modelID"))
			else
				sql = "select modelID from we_models where modelName like '%" & userAgent & "%'"
				session("errorSQL") = sql
				set modelRS = oConn.execute(sql)
			
				if not modelRS.EOF then myModelID = prepInt(modelRS("modelID"))
			end if
		end if
	next
%>
<div>Agent:<%=userAgent%> (<%=myModelID%>)</div>
<div style="padding-top:10px;">Host:<%=request.ServerVariables("HTTP_HOST")%></div>
<div style="padding-top:10px;">Conn:<%=request.ServerVariables("HTTP_CONNECTION")%></div>
<div id="priceRating">
	<div id="prices">
		<div id="regPrice">Reg: <span style="text-decoration:line-through;"><%=priceRetail%></span></div>
    	<div id="wePrice">Sale: <%=priceRetail%></div>
    </div>
    <div id="ratingInfo" onclick="readReviews()">
    	<% if avgRating > 0 then %>
        <div id="ratingStars">
        	<%
			for i = 1 to 5
				if avgRating => i then useStar = "on" else useStar = "off"
				response.Write("<img src='/images/mobile/ratingStar_" & useStar & ".gif' border='0' />")
			next
			%>
        </div>
        <div id="readReviews">Read All Reviews</div>
        <% end if %>
    </div>
</div>
<div id="prodPic"><img id="fullSized" src="/productpics/big/<%=itemPic%>" border="0" /></div>
<div id="altImgRow">
	<div id="altImgBox" onclick="document.getElementById('fullSized').src='/productpics/big/<%=itemPic%>'"><img src="/productpics/icon/<%=itempic%>" border="0" width="40" /></div>
	<%
    for iCount = 0 to 2
        path = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & ".jpg")
        thumbPath = "/productpics/AltViews/" & replace(itempic,".jpg","-" & iCount & "_thumb.jpg")
        if fso.FileExists(Server.MapPath(thumbPath)) then
    %>
    <div id="altImgBox" onclick="document.getElementById('fullSized').src='<%=path%>'"><img src="<%=thumbPath%>" border="0" /></div>
    <%
        end if
    next
    %>
</div>
<div id="prodDescBox">
	<div id="prodDesc"><%=itemDesc%></div>
</div>
<% if inv_qty > 0 then %>
<form name="addToCart" method="post" action="/basket.htm">
<div id="addToCartRow">
	<input type="hidden" name="qty" value="1" />
    <input type="hidden" name="itemID" value="<%=itemID%>" />
    <input type="image" src="/images/mobile/buttons/addToCart.jpg" />
</div>
</form>
<% else %>
<div id="prodDescBox">
	<div id="outOfStock">Currently Out of Stock</div>
</div>
<% end if %>
<div id="valuePropRow">
	<div id="valueBox">
		<div id="valueCheckmark"><img src="/images/mobile/checkmark.gif" border="0" /></div>
    	<div id="valueText">Fast, FREE Shipping!</div>
    </div>
</div>
<div id="valuePropRow">
	<div id="valueBox">
		<div id="valueCheckmark"><img src="/images/mobile/checkmark.gif" border="0" /></div>
    	<div id="valueText">90-Day No-Hassle Returns</div>
    </div>
</div>
<div id="valuePropRow">
	<div id="valueBox">
		<div id="valueCheckmark"><img src="/images/mobile/checkmark.gif" border="0" /></div>
    	<div id="valueText">100% Secure Shopping Guarantee</div>
    </div>
</div>
<div id="tabContainer">
    <div id="tabRow">
        <div id="detailsTab" class="detailsTab_on" onclick="showTab('detailsTab')">Details</div>
        <div id="featuresTab" class="featuresTab_off" onclick="showTab('featuresTab')">Features</div>
        <div id="reviewsTab" class="reviewsTab_off" onclick="showTab('reviewsTab')">Reviews</div>
    </div>
    <div id="detailsTabContent" class="detailsTabContent_on"><%=itemLongDetail%></div>
    <div id="featuresTabContent" class="featuresTabContent_off">
        <ul style="line-height:20px;">
            <li><%=bullet1%></li>
            <% if bullet2 <> "" then %><li><%=bullet2%></li><% end if %>
            <% if bullet3 <> "" then %><li><%=bullet3%></li><% end if %>
            <% if bullet4 <> "" then %><li><%=bullet4%></li><% end if %>
            <% if bullet5 <> "" then %><li><%=bullet5%></li><% end if %>
            <% if bullet6 <> "" then %><li><%=bullet6%></li><% end if %>
            <% if bullet7 <> "" then %><li><%=bullet7%></li><% end if %>
            <% if bullet8 <> "" then %><li><%=bullet8%></li><% end if %>
            <% if bullet9 <> "" then %><li><%=bullet9%></li><% end if %>
            <% if bullet10 <> "" then %><li><%=bullet10%></li><% end if %>
        </ul>
    </div>
    <div id="reviewsTabContent" class="reviewsTabContent_off">
    	<%
		sql = "select * from we_reviews where partNumbers = '" & partNumber & "'"
		session("errorSQL") = sql
		set reviewRS = oConn.execute(sql)
		
		rowID = 1
		do while not reviewRS.EOF
			nickname = reviewRS("nickname")
			rating = reviewRS("rating")
			reviewTitle = reviewRS("reviewTitle")
			reviewText = reviewRS("review")
		%>
        <div id="reviewBox<%=rowID%>">
        	<div id="reviewer">Review By: <%=nickname%></div>
            <div id="reviewTitleRow">
	            <div id="reviewTitle"><%=reviewTitle%></div>
                <div id="reviewStars">
                	<%
					for i = 1 to 5
						if avgRating => i then useStar = "on" else useStar = "off"
						response.Write("<img src='/images/mobile/ratingStar_" & useStar & ".gif' border='0' />")
					next
					%>
                </div>
            </div>
			<div id="reviewText"><%=reviewText%></div>
        </div>
        <%
			reviewRS.movenext
			if rowID = 1 then rowID = 2 else rowID = 1
		loop
		%>
    </div>
</div>
<% if not relatedRS.EOF then %>
<div id="valuePropRow">
	<div id="valueBox">
		<div id="valueCheckmark"><img src="/images/mobile/heart.gif" border="0" /></div>
    	<div id="valueText">Other Products You'll Love</div>
    </div>
</div>
<div id="mainProductBox">
	<div id="refProdctBox">
		<%
        do while not relatedRS.EOF
            itemID = relatedRS("itemID")
            itemPic = relatedRS("itemPic")
            itemDesc = relatedRS("itemDesc")
            itemDesc = replace(itemDesc,brandName,"")
            itemDesc = trim(replace(itemDesc,modelName,""))
            itemPrice = formatCurrency(prepInt(relatedRS("price_our")),2)
            avgRating = relatedRS("avgRating")
        %>
        <a href="/product.htm?itemID=<%=itemID%>" style="text-decoration:none;" title="<%=itemDesc%>">
        <div id="singleProdBox_<%=prodCnt%>" class="singleProdBox">
            <div id="refPic"><img src="/productpics/thumb/<%=itemPic%>" /></div>
            <div id="refDesc"><%=itemDesc%></div>
            <div id="refPrice"><%=itemPrice%></div>
            <% if prepInt(avgRating) > 0 then %>
            <div id="refRating">
                <%
                for i = 1 to 5
                    if avgRating => i then useStar = "on" else useStar = "off"
                    response.Write("<img src='/images/mobile/ratingStar_" & useStar & ".gif' border='0' />")
                next
                %>
            </div>
            <% else %>
            <div id="refRating"></div>
            <% end if %>
        </div>
        </a>
        <%
            relatedRS.movenext
        loop
        %>
    </div>
</div>
<% end if %>
<div id="bottumButton"><a href="javascript:history.go(-1)"><img src="/images/mobile/buttons/previousPage.jpg" border="0" /></a></div>
<div id="bottumButton"><a href="/?reset=1"><img src="/images/mobile/buttons/changeDevice.jpg" border="0" /></a></div>
<!--#include virtual="/template/bottom.asp"-->
<script language="javascript">
	var curTab = "detailsTab"
	var curScroll = 200
	
	window.scroll(0,200)
	if (window.DeviceOrientationEvent) {
	  window.addEventListener('deviceorientation', capture_orientation, false);
	} else {
	  alert("no motion found")
	}
	
	function capture_orientation (event) {
		var alpha = parseInt(event.alpha);
		var beta = parseInt(event.beta);
		var gamma = parseInt(event.gamma);
		
		if (beta > 40) {
			if (curScroll > 1) {
				curScroll = curScroll - 5
				window.scroll(0,curScroll)
			}
		}
		else if (beta < 20) {
			curScroll = curScroll + 5
			window.scroll(0,curScroll)
		}
		document.getElementById("testZone").innerHTML = 'Orientation - Alpha: '+alpha+', Beta: '+beta+', Gamma: '+gamma
	}
	
	function showTab(tab) {
		document.getElementById(curTab).setAttribute("class", curTab + "_off")
		document.getElementById(tab).setAttribute("class", tab + "_on")
		document.getElementById(curTab + 'Content').setAttribute("class", curTab + "Content_off")
		document.getElementById(tab + 'Content').setAttribute("class", tab + "Content_on")
		curTab = tab
	}
	
	function readReviews() {
		showTab('reviewsTab')
		window.location = '#reviewsTab'
	}
</script>