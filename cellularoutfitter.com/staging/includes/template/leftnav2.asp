<%
if curPageName = "BMC-b" then
	if categoryID = 18 then useCatID = 999 else useCatID = categoryID
	
	if useCatID = 16 then
		sql = 	"select	distinct c.id subid, c.carriername subName" & vbcrlf & _
				"from	we_items a join we_carriers c" & vbcrlf & _
				"	on	a.carrierid = c.id left outer join we_pndetails d" & vbcrlf & _
				"	on	a.partnumber = d.partnumber" & vbcrlf & _
				"where	a.typeid = '16' " & vbcrlf & _
				"	and	a.brandid = '" & brandid & "'" & vbcrlf & _
				"	and a.hidelive = 0 " & vbcrlf & _
				"	and (a.inv_qty <> 0 or d.alwaysinstock = 1) " & vbcrlf & _
				"	and a.price_co > 0 	" & vbcrlf & _
				"	and	c.id in (1,2,3,4,5,6,7,8,9,11)" & vbcrlf & _
				"order by subid"
	else
		sql =	"select b.subTypeID subid, b.subTypeName subName " & vbcrlf & _
				"from we_typeMatrix a " & vbcrlf & _
					"left join we_subTypes b on a.origSubTypeID = b.subTypeID " & vbcrlf & _
				"where b.hidelive = 0 and b.subtypeid not in (1064,1031) and (maskTypeID = " & prepInt(useCatID) & ")"
	end if
	if customFilter = 1 then sql = replace(sql,"(maskTypeID","(b.subtypeid = 1284 or maskTypeID")
	session("errorSQL") = sql
	set rsSub = oConn.execute(sql)
	
	sql = "select color, colorCodes from XProductColors order by color"
	session("errorSQL") = sql
	set colorRS = oConn.execute(sql)
%>
<div style="float:left; width:190px;">
	<div id="mvt_myPhone" style="float:left; margin:10px 0px 0px 2px; width:185px;">
		<div style="float:left; width:100%;" title="Current Device" ><img src="/images/bmc/header-currently-viewing.jpg" border="0" width="185" height="38" /></div>
		<div style="float:left; width:183px; position:relative; border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc; border-bottom-right-radius:5px; border-bottom-left-radius:5px; text-align:center; padding-top:5px;">
			<div style="position:absolute; top:2px; right:2px;">
				<%
				lnkBrands = ""
				if brandid > 0 then lnkBrand = "/b-" & brandid & "-" & formatSEO(brandName) & "-cell-phone-accessories.html"
				%>
				<a href="<%=lnkBrands%>"><img src="/images/bmc/x-button.jpg" border="0" width="27" height="27" /></a>
			</div>
			<div style="text-align:center; width:100%;">
				<%
				if OtherGear then
					%>
					<img src="/images/types/CO_CAT_banner_other-gear.jpg" height="100" border="0" alt="<%=brandName & " " & modelName & " " & categoryName%>">
					<%
				elseif Bling then
					%>
					<img src="/images/types/CO_CAT_banner_charms.jpg" border="0" alt="<%=brandName & " " & modelName & " " & categoryName%>">
					<%
				else
					%>
					<img src="/modelpics/<%=modelImg%>" border="0" height="106" alt="<%=brandName & " " & modelName & " " & categoryName%>">
					<%
				end if								
				%>
			</div>
			<div style="text-align:center; width:95%; padding:10px 0px 10px 5px;"><h1 style="color:#666; font-size:12px; font-weight:normal;"><%=strH1%></h1></div>
		</div>
	</div>
	<div style="float:left; margin-top:10px;"><img src="/images/leftFilter/header-filter-by.jpg" border="0" title="Filter Menu" /></div>
	<div style="float:left; background-color:#ebebeb; border-bottom-left-radius:10px; border-bottom-right-radius:10px; width:190px;">
		<form name="filterForm" action="<%=request.ServerVariables("HTTP_X_REWRITE_URL")%>" method="post" style="padding:0px; margin:0px;">
		<div class="tb filterMainBox">
			<div class="tb topFilterBox">
				<%
				if not rsSub.EOF then
					subNameHeader = "Style Type"
					if useCatID = 16 then subNameHeader = "Carriers"
				%>
				<div class="tb filterTitleRow">
					<div class="fl filterIcon"></div>
					<div class="fl filterTitle"><%=subNameHeader%></div>
				</div>
				<%
					do while not rsSub.EOF
						subID = prepInt(rsSub("subID"))
						subName = rsSub("subName")
						subName = replace(subName,"& Hybrid","")
						subName = replace(subName," Cases","")
						subName = replace(subName," Covers","")
						subName = replace(subName,"-Covers","")
				%>
				<div class="tb filterOptRow">
					<div class="fl filterCheckbox"><input type="checkbox" id="<%=subID%>" name="styleType" value="<%=subID%>" onclick="updateFilter()"<% if carrierid = subID then %> checked="checked"<% end if %> /></div>
					<div class="fl filterOptText"><a class="filterOptLink" onclick="selectFilter(<%=subID%>)"><%=subName%></a></div>
				</div>
				<%
						rsSub.movenext
					loop
				end if
				%>
			</div>
			<div class="tb topFilterBox">
				<div class="tb filterTitleRow">
					<div class="fl filterIcon"></div>
					<div class="fl filterTitle">Price</div>
				</div>
				<div class="tb filterOptRow">
					<div class="fl"><input type="checkbox" id="price_0" name="priceRange" value="4" onclick="updateFilter()" /></div>
					<div class="fl filterOptText"><a class="filterOptLink" onclick="selectFilter('price_0')">$0 to $4.99</a></div>
				</div>
				<div class="tb filterOptRow">
					<div class="fl"><input type="checkbox" id="price_5" name="priceRange" value="5" onclick="updateFilter()" /></div>
					<div class="fl filterOptText"><a class="filterOptLink" onclick="selectFilter('price_5')">$5 to $9.99</a></div>
				</div>
				<div class="tb filterOptRow">
					<div class="fl"><input type="checkbox" id="price_10" name="priceRange" value="10" onclick="updateFilter()" /></div>
					<div class="fl filterOptText"><a class="filterOptLink" onclick="selectFilter('price_10')">$10 to $19.99</a></div>
				</div>
				<div class="tb filterOptRow">
					<div class="fl"><input type="checkbox" id="price_20" name="priceRange" value="20" onclick="updateFilter()" /></div>
					<div class="fl filterOptText"><a class="filterOptLink" onclick="selectFilter('price_20')">$20 to $29.99</a></div>
				</div>
				<div class="tb filterOptRow">
					<div class="fl"><input type="checkbox" id="price_30" name="priceRange" value="30" onclick="updateFilter()" /></div>
					<div class="fl filterOptText"><a class="filterOptLink" onclick="selectFilter('price_30')">$30 and Up</a></div>
				</div>
			</div>
            <div class="tb">
				<div class="tb filterTitleRow">
					<div class="fl filterIcon"></div>
					<div class="fl filterTitle">Color</div>
				</div>
				<%
				do while not colorRS.EOF
					color = colorRS("color")
					hexColor = colorRS("colorCodes")
				%>
				<div class="tb filterOptRow">
					<div class="fl"><input type="checkbox" id="color_<%=color%>" name="colorSelect" value="<%=color%>" onclick="updateFilter()" /></div>
					<div class="fl colorBox" style="background-color:<%=hexColor%>;" onclick="selectFilter('color_<%=color%>')"></div>
                    <div class="fl filterOptText"><a class="filterOptLink" onclick="selectFilter('color_<%=color%>')"><%=color%></a></div>
				</div>
				<%
					colorRS.movenext
				loop
				%>
			</div>
		</div>
		</form>
	</div>
	<div style="float:left; border-bottom:1px solid #CCC; margin:10px 0px 10px 0px; width:100%;"></div>
	<div style="float:left; background-color:#fff; border:1px solid #CCC; border-radius:10px; width:190px;">
		<div style="float:left; margin:10px 0px 0px 20px; width:143px; text-align:center; border-bottom:1px solid #CCC; padding-bottom:10px;"><img src="/images/leftFilter/header-buy-confidence.jpg" border="0" /></div>
		<div style="float:left; margin-top:10px; width:100%; text-align:center; font-size:11px; color:#999;">
			All of your information is<br />
			secured and transmitted<br />
			utilizing the highest level<br />
			of Secure Sockets<br />
			Layer (SSL) encryption
		</div>
		<div style="float:left; margin-top:10px; width:100%; text-align:center;"><img src="/images/leftFilter/seal-truste.jpg" border="0" /></div>
		<div style="float:left; margin-top:10px; width:100%; text-align:center;"><img src="/images/leftFilter/seal-bbb.jpg" border="0" /></div>
		<div style="float:left; margin-top:10px; width:100%; text-align:center;"><img src="/images/leftFilter/seal-verisign.jpg" border="0" /></div>
		<div style="float:left; margin:10px 0px 20px 0px; width:100%; text-align:center;"><img src="/images/leftFilter/seal-mcafee.jpg" border="0" /></div>
	</div>
</div>
<% else %>
	<% if noLeftSide = 0 then %>
		<% arrowSize = 14 %>
<div style="width:185px; padding-top:10px;">
	<div id="newLeftNav">
		<div id="blueSearchBox">
			<form method="get" action="/search/search.asp" style="margin:0px; padding:0px;">
			<div style="float:left; margin:15px 0px 0px 10px;">
				<div style="float:left; width:135px; background-color:#fff; height:20px; padding-left:5px; border-top-left-radius:5px; border-bottom-left-radius:5px;">
					<input type="text" name="keywords" value="Search" onclick="this.value=''" style="color:#999; width:130px; border:none;" />
				</div>
				<div style="float:left; width:20px; background-color:#fff; height:17px; padding-top:3px; border-top-right-radius:5px; border-bottom-right-radius:5px;">
					<input type="image" name="btnSubmit" src="/images/template/search-icon.jpg" />
				</div>
			</div>
			</form>
		</div>
		<div id="leftBrandNav" style="float:left; margin-top:5px;">
			<div id="blueShopByBrand"></div>
			<div id="leftBrandWarpper">
				<div id="leftBrandBox" onclick="window.location='/b-17-apple-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-17-apple-cell-phone-accessories.html" title="Apple">Apple</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-14-blackberry-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-14-blackberry-cell-phone-accessories.html" title="BlackBerry">BlackBerry</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-28-casio-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-28-casio-cell-phone-accessories.html" title="Casio">Casio</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-20-htc-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-20-htc-cell-phone-accessories.html" title="HTC">HTC</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-29-huawei-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-29-huawei-cell-phone-accessories.html" title="Huawei">Huawei</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-3-kyocera-qualcomm-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-3-kyocera-qualcomm-cell-phone-accessories.html" title="Kyocera">Kyocera</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-4-lg-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-4-lg-cell-phone-accessories.html" title="LG">LG</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-5-motorola-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-5-motorola-cell-phone-accessories.html" title="Motorola">Motorola</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-6-nextel-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-6-nextel-cell-phone-accessories.html" title="Nextel">Nextel</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-7-nokia-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-7-nokia-cell-phone-accessories.html" title="Nokia">Nokia</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-16-palm-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-16-palm-cell-phone-accessories.html" title="Palm">Palm</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-18-pantech-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-18-pantech-cell-phone-accessories.html" title="Pantech">Pantech</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-9-samsung-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-9-samsung-cell-phone-accessories.html" title="Samsung">Samsung</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-10-sanyo-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-10-sanyo-cell-phone-accessories.html" title="Sanyo">Sanyo</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-19-t-mobile-sidekick-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-19-t-mobile-sidekick-cell-phone-accessories.html" title="Sidekick">Sidekick</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-2-sony-ericsson-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-2-sony-ericsson-cell-phone-accessories.html" title="Sony Ericsson">Sony Ericsson</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-30-zte-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-30-zte-cell-phone-accessories.html" title="ZTE">ZTE</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-15-other-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-15-other-cell-phone-accessories.html" title="Other Brands">Other Brands</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>
				</div>
			</div>
		</div>
        <div style="float:left; width:100%; text-align:center; padding:10px 0px 10px 0px; border-bottom:1px solid #ccc;">
			<a rel="Nofollow" href="/custom/"><img src="/images/custom/leftNav2.jpg" border="0" /></a>
		</div>
		<div style="float:left; width:100%; text-align:center; padding:10px 0px 10px 0px; border-bottom:1px solid #ccc;">
			<a target="_blank" rel="Nofollow" href="https://www.mcafeesecure.com/RatingVerify?ref=www.cellularoutfitter.com"><img width="115" height="32" border="0" src="//images.scanalert.com/meter/www.cellularoutfitter.com/12.gif" alt="McAfee SECURE sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="alert('Copying Prohibited by Law - McAfee SECURE is a Trademark of McAfee, Inc.'); return false;"></a>
		</div>
		<% if request.ServerVariables("URL") <> "/index.asp" then %>
		<div style="width:100%; float:left; padding:5px 0px 5px 0px;">
			<div align="center">
				<p align="center"><a href="/privacypolicy.html" target="_blank"><div id="truste"></div></a></p>
				<p align="center"><a rel="Nofollow" href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=100097908&source=ctc" target="_blank"><div id="bbb"></div></a></p>
				<!-- START GOOGLE CHECKOUT CODE -->
				<script type="text/javascript" src="/includes/js/j.js"></script>
				<script type="text/javascript">showMark(3);</script>
				<noscript><div id="googleChkOutImg"></div></noscript>
				<!-- END GOOGLE CHECKOUT CODE -->
				<p align="center"><div id="inc5000"></div></p>
				<p align="center"><a rel="Nofollow" href="http://www.shopwiki.com/store-252661" target="_blank" title="CellularOutfitter.com is a ShopWiki Approved Store"><div id="shopWiki"></div></a></p>
			</div>
		</div>
		<% else %>
		<div style="width:100%; float:left; padding:5px 0px 5px 0px; text-align:center;">
			<script type="text/javascript">
			<!--
				google_ad_client = "ca-pub-1001075386636876";
				/* CO Skyscraper */
				google_ad_slot = "2604906734";
				google_ad_width = 160;
				google_ad_height = 600;
			//-->
			</script>
			<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
		</div>
		<% end if %>
		<% if leftGoogleAd = 1 then %>
		<div id="adcontainer2"></div>
		<% end if %>
	</div>
</div>
	<% end if 'noLeftSide = 0%>
<% end if 'curPageName = "BMC_new"%>