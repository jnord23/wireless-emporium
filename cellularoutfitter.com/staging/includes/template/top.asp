<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
curPage = request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL")
if instr(curPage,"216.139.235.93") > 0 then
	response.Status = "301 Moved Permanently"
	response.AddHeader "Location", "http://" & replace(curPage,"216.139.235.93","www.cellularOutfitter.com")
	response.end
end if

saveEmail()

betaView = prepInt(request.Cookies("betaView"))

on error resume next
if request.querystring("id") <> "" then
	Response.Cookies("vendororder") = request.querystring("id")
	Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
elseif request.querystring("refer") <> "" then
	response.Write("refer:" & request.QueryString("refer") & "<br>")
	Response.Cookies("vendororder") = request.querystring("refer")
	Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
elseif request.querystring("source") <> "" then
	Response.Cookies("vendororder") = request.querystring("source")
	Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
elseif request.querystring("utm_source") <> "" then
	Response.Cookies("vendororder") = request.querystring("utm_source")
	Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)	
elseif request.querystring("clickid") <> "" then
	Response.Cookies("vendororder") = "clickid=" & request.querystring("clickid")
	Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
end if
on error goto 0

dim relatedItems(10), NumRecords, a, b
NumRecords = 0
for a = 1 to 10
	relatedItems(a) = 999999
next

if SEtitle = "" then SEtitle = "Wholesale Cell Phone Accessories ?Wholesale Cellular Phone Accessories ?CellularOutfitter.com"
if SEdescription = "" then SEdescription = "Buy Cheap Cell Phone Accessories at Wholesale Prices to the Public. Find the latest Discount Cellular Phone Accessories for Motorola, LG, Nokia, Samsung phones and more. Cellular Accessories at the Lowest Prices Online Guaranteed."
if SEkeywords = "" then SEkeywords = "cheap cell phone accessories, cell phone accessories, discount cellular phone accessories, cellular phone accessories"
if pageTitle = "Home" then SEtitle = "Wholesale Cell Phone Accessories | Wholesale Cell Phones | iPhone, Blackberry, HTC, Samsung & LG"

REWRITE_URL = Request.ServerVariables("HTTP_X_REWRITE_URL")
if inStr(Request.ServerVariables("HTTP_X_REWRITE_URL"),"?") > 0 then 
	REWRITE_URL = left(Request.ServerVariables("HTTP_X_REWRITE_URL"),inStr(Request.ServerVariables("HTTP_X_REWRITE_URL"),"?")-1)
end if

if Request.ServerVariables("HTTPS") = "on" then
	canonicalURL = "https://" & Request.ServerVariables("SERVER_NAME") & REWRITE_URL
else
	canonicalURL = "http://" & Request.ServerVariables("SERVER_NAME") & REWRITE_URL
end if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><%=SEtitle%></title>
<meta name="Description" content="<%=SEdescription%>">
<meta name="Keywords" content="<%=SEkeywords%>">
<link rel="canonical" href="<%=lcase(canonicalURL)%>" />
<%if pageTitle = "Home" then%>
<meta name="robots" content="index,follow">
<meta name="alternate" content="http://m.cellularoutfitter.com<%=request.ServerVariables("HTTP_X_REWRITE_URL")%>">
<!-- Cell Phone Accessories at CellularOutfitter.com: Motorola Cell Phone Accessories, Nokia Cell Phone Accessories, LG Cell Phone Accessories, Samsung Cell Phone Accessories -->
<meta name="verify-v1" content="KDSemcIPOLKERRNLdtodRPug14ThOrk3fAAECAAzt/U=" />
<meta name="verify-v1" content="7oJ6qXmQ/MOO9Wics9IknbH0gaHI9z5Tl3dH82NBhO8=" />
<%end if%>
<link rel="stylesheet" type="text/css" href="/includes/css/styleCO.css<%=getCssDateParam("/includes/css/styleCO.css")%>" />
<link rel="shortcut icon" href="https://www.cellularoutfitter.com/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="https://www.cellularoutfitter.com/images/favicon.ico" type="image/x-icon">
<script src="https://seal.buysafe.com/private/rollover/rollover.js" type="text/javascript" language="javascript" charset="utf-8"></script>
<script language="javascript">
function onSearchbox()
{
	if ("Keyword/Brand" == document.frmNxtAc.keywords.value) document.frmNxtAc.keywords.value = "";
	else if ("" == document.frmNxtAc.keywords.value) document.frmNxtAc.keywords.value = "Keyword/Brand";
}

function addbookmark() {
	var IE = navigator.appName.match(/(Microsoft Internet Explorer)/gi),
	NS = navigator.appName.match(/(Netscape)/gi),
	OP = navigator.appName.match(/(Opera)/gi),
	BK = document.getElementById('bookmark');
	BK.onmouseout = function() {
		window.status = '';
	}
	if(IE && document.uniqueID) {
		var bookmarkurl = "/";
		var bookmarktitle = "Cellular Outfitter";
		window.external.AddFavorite(bookmarkurl,bookmarktitle);
	}
	else if(OP || IE && !document.uniqueID) {
		alert('Your browser requires that you\nPress Ctrl & D to Bookmark this page.');
	}
	else if(NS) {
		alert('Your browser requires that you\nPress Ctrl & D to Bookmark this page.');
	}
	else { BK.innerHTML = '' }
}
</script>
<!-- AdSense Start -->
<script src="http://www.google.com/jsapi"></script>
<script type="text/javascript" charset="utf-8">
google.load('ads.search', '2');
</script>
<!-- AdSense End -->
<!--#include virtual="/includes/template/inc_HeadTagEnd.asp"-->
</head>

<body class="body">
	<% call printPIxel(2) %>
<!-- Google AdSense Start -->
<script type="text/javascript" charset="utf-8">
var pageOptions = {
'pubId' : 'pub-1001075386636876',
<% if len(brandName) > 0 then %>
	<% if len(modelName) > 0 then %>
		<% if len(categoryName) > 0 then %>
'query' : '<%=brandName%>' + ' ' + '<%=modelName%>' + ' ' + '<%=categoryName%>'
		<% else %>
'query' : '<%=brandName%>' + ' ' + '<%=modelName%>'
		<% end if %>
	<% else %>
		<% if len(categoryName) > 0 then %>
'query' : '<%=brandName%>' + ' ' + '<%=categoryName%>'
		<% else %>
'query' : '<%=brandName%>'
		<% end if %>
	<% end if %>
};
<% elseif len(categoryName) > 0 then %>
'query' : '<%=categoryName%>'
};
<% else %>
'query' : 'Cell Phones'
};
<% end if %>
var adblock1 = {
'container' : 'adcontainer1',
'number' : 3,
'width' : 'auto',
'lines' : 2,
'fontFamily' : 'arial',
'fontSizeTitle' : '14px',
'fontSizeDescription' : '14px',
'fontSizeDomainLink' : '14px',
'linkTarget' : '_blank'
};
var adblock2 = {
'container' : 'adcontainer2',
'number' : 4,
'width' : '200px',
'lines' : 3,
'fontFamily' : 'arial',
'fontSizeTitle' : '12px',
'fontSizeDescription' : '12px',
'fontSizeDomainLink' : '12px',
'linkTarget' : '_blank'
};
new google.ads.search.Ads(pageOptions, adblock1, adblock2);
</script>
<!-- Google AdSense Close -->
<!--#include virtual="/includes/template/topHTML.asp"-->