<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	if isnull(noLeftSide) or len(noLeftSide) < 1 then noLeftSide = 0
	curPage = request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL")
	if instr(curPage,"216.139.235.93") > 0 then
		response.Status = "301 Moved Permanently"
		response.AddHeader "Location", "http://" & replace(curPage,"216.139.235.93","www.cellularOutfitter.com")
		response.end
	end if

	on error resume next
	if request.querystring("id") <> "" then
		Response.Cookies("vendororder") = request.querystring("id")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
	elseif request.querystring("refer") <> "" then
		Response.Cookies("vendororder") = request.querystring("refer")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
	elseif request.querystring("source") <> "" then
		Response.Cookies("vendororder") = request.querystring("source")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
	elseif request.querystring("utm_source") <> "" then
		Response.Cookies("vendororder") = request.querystring("utm_source")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)		
	elseif request.querystring("clickid") <> "" then
		Response.Cookies("vendororder") = "clickid=" & request.querystring("clickid")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
	end if
	on error goto 0

	if Request.ServerVariables("SERVER_PORT_SECURE") = "1" then
		strHTTP = "https"
	else
		strHTTP = "http"
	end if
	
	if pageTitle = "Brand" then
		dim sql, rs
		sql = "select seTitle, seDescription, seKeywords, topText, bottomText from co_brandText where brandID = " & brandID
		session("errorSQL") = sql
		set rs = Server.CreateObject("ADODB.Recordset")
		rs.open sql, oConn, 0, 1
		
		'if RS.eof then response.redirect("/?ec=201")
		
		if not rs.EOF then
			SEtitle = rs("seTitle")
			SEdescription = rs("seDescription")
			SEkeywords = rs("seKeywords")
			topText = rs("topText")
			if not isnull(rs("bottomText")) and len(rs("bottomText")) > 0 then bottomText = rs("bottomText")
		end if
	end if

	function ds(strValue)
		strContent = trim(strValue)
		if not isNull(strContent) and strContent <> "" then
			strContent = trim(replace(strContent,"'","''"))
			strContent = trim(replace(strContent,chr(34),"''''"))
			strContent = replace(strContent,"%26%2343%3B1","+")
			strContent = replace(strContent,"&#43;","+")
			strContent = replace(strContent,Chr(39),"''")
			strContent = replace(strContent,";","")
			'strContent = replace(strContent,"--","")
			strContent = replace(strContent,"/*","")
			strContent = replace(strContent,"*/","")
			strContent = replace(strContent,"xp_","")
			strContent = trim(replace(strContent,chr(146),"''"))
			strContent = trim(replace(strContent,chr(147),"''"))
			strContent = trim(replace(strContent,chr(148),"''"))
		else
			strContent = strValue
		end if
		ds = strContent
	end function
	
	REWRITE_URL = Request.ServerVariables("HTTP_X_REWRITE_URL")
	if inStr(Request.ServerVariables("HTTP_X_REWRITE_URL"),"?") > 0 then 
		REWRITE_URL = left(Request.ServerVariables("HTTP_X_REWRITE_URL"),inStr(Request.ServerVariables("HTTP_X_REWRITE_URL"),"?")-1)
	end if
	
	if Request.ServerVariables("HTTPS") = "on" then
		canonicalURL = "https://" & Request.ServerVariables("SERVER_NAME") & REWRITE_URL
	else
		canonicalURL = "http://" & Request.ServerVariables("SERVER_NAME") & REWRITE_URL
	end if	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title><%=SEtitle%></title>
	<meta name="Description" content="<%=SEdescription%>">
	<meta name="Keywords" content="<%=SEkeywords%>">
    <% if pageTitle = "Home" then %>
    <link rel="canonical" href="http://www.cellularoutfitter.com" />
	<% else %>
	<link rel="canonical" href="<%=canonicalURL%>" />    
	<% end if %>
	<meta name="robots" content="index,follow">
	<meta name="verify-v1" content="KDSemcIPOLKERRNLdtodRPug14ThOrk3fAAECAAzt/U=" />
	<meta name="verify-v1" content="7oJ6qXmQ/MOO9Wics9IknbH0gaHI9z5Tl3dH82NBhO8=" />
    <meta name="msvalidate.01" content="DFF5FF52EAB66FFFC627628486428C9B" />
	<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/includes/css/styleCO.css" />
    <link rel="stylesheet" type="text/css" href="/includes/css/stitch.css" />
    <!--#include virtual="/includes/template/inc_HeadTagEnd.asp"-->
</head>
<body class="body">
	<% call printPIxel(2) %>
<!--#include virtual="/includes/template/topHTML-new.asp"-->