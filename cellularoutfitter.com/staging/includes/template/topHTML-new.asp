<% if prepInt(cart) = 0 then %>
<div id="buySafe" style="position:fixed; width:100%; height:100%;">
	<div id="buySafeLogo" style="position:absolute; z-index:999; bottom:0px; left:0px;" onmouseover="buySafeRollover(); document.getElementById('buySafePopUp').style.display=''" onmouseout="document.getElementById('buySafePopUp').style.display='none'"><a href="https://www.buysafe.com/Web/Seal/VerifySeal.aspx?MPHASH=DUSrEUQiRGGi3%2BXwd0f4uWb9FK%2BkUtZQSWeu1jXBHU0AoJDBf32vZBlAMv7Q%2BIwIG5t19fce1L2cREvPnzJWrQ%3D%3D" target="_blank" title="BuySafe Verification"><img src="/images/icons/buySafe_off.png" border="0" /></a></div>
    <div id="buySafePopUp" style="position:absolute; z-index:999; bottom:65px; left:77px; display:none;" onmouseover="document.getElementById('buySafePopUp').style.display=''" onmouseout="document.getElementById('buySafePopUp').style.display='none'"></div>
</div>
<script language="javascript">function buySafeRollover() { if (document.getElementById("buySafePopUp").innerHTML == "") { document.getElementById("buySafePopUp").innerHTML = "<a href='http://www.buysafe.com/buyer_benefits/bond_short.html' target='_blank' title='BuySafe Benefits'><img src='/images/icons/buySafe_on.png' border='0' /></a>" } }</script>
<% else %>
<script src="https://seal.buysafe.com/private/rollover/rollover.js" type="text/javascript" language="javascript" charset="utf-8"></script>
<% end if %>
<div style="position:relative;">
	<%
	sql = "select SUM(qty) as myQty, sum(b.price_CO * a.qty) as myTotal from ShoppingCart a left join we_Items b on a.itemID = b.itemID where store = 2 and sessionID = " & prepInt(request.cookies("mySession")) & " and dateCheckout is null"
	session("errorSQL") = sql
	set myCartRS = oConn.execute(sql)
	
	if myCartRS.EOF then
		myQty = 0
		myTotal = 0
	else
		myQty = prepInt(myCartRS("myQty"))
		myTotal = prepInt(myCartRS("myTotal"))
	end if
	%>
    <div style="width:100%; height:28px; background-image:url(/images/backgrounds/top-bar-dblue.png);">
        <div style="width:1040px; height:28px; margin-left:auto; margin-right:auto;">
            <div style="float:right; background-image:url(/images/backgrounds/top-bar-green.png); padding:0px 20px 0px 20px; height:28px;">
                <a href="/cart/basket.asp">
                <div style="float:left"><img src="/images/icons/top-bar-green-carticon.png" border="0" /></div>
                <div style="float:left; color:#FFF; font-weight:bold; font-size:10px; padding:7px 10px 0px 10px;"><%=myQty%> Items - <%=formatCurrency(myTotal,2)%></div>
                <div style="float:left"><img src="/images/icons/top-bar-green-downarrow.png" border="0" /></div>
                </a>
            </div>
            <div style="float:right;"><img src="/images/backgrounds/top-bar-divider-dblue.png" border="0" /></div>
            <div style="float:right; font-weight:bold; padding:7px 20px 0px 20px;"><a href="/contact-us.html" style="color:#FFF; font-size:10px;">Contact Us</a></div>
            <div style="float:right;"><img src="/images/backgrounds/top-bar-divider-dblue.png" border="0" /></div>
            <div style="float:right; font-weight:bold; padding:7px 20px 0px 20px;"><a href="/faq.html" style="color:#FFF; font-size:10px;">FAQs</a></div>
            <div style="float:right;"><img src="/images/backgrounds/top-bar-divider-dblue.png" border="0" /></div>
            <div style="float:right; font-weight:bold; padding:7px 20px 0px 20px;"><a href="/track-your-order.html" style="color:#FFF; font-size:10px;">Order Status</a></div>
            <div style="float:right;"><img src="/images/backgrounds/top-bar-divider-dblue.png" border="0" /></div>
            <div style="float:right;"><script type="text/javascript" src="/includes/js/liveHelp-new.js"></script></div>
        </div>
    </div>
<table width="1020" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF" style="border-left:1px solid #ccc; border-right:1px solid #ccc;">
    <tr>
        <td valign="top" style="padding:10px 10px 0px 10px;">
            <div style="float:left; width:1020px;">
            	<div style="float:left; width:330px; height:80px;">
                	<%
					if date < cdate("01/17/2012") then
						holidayLogo = 1
					else
						holidayLogo = 0
					end if
					if holidayLogo = 0 then
					%>
						<a href="/"><img src="/images/logos/CO-logo.png" border="0" title="CellularOutfitter.com" /></a>
					<% else %>
						<a href="/"><img src="/images/co_logo_2012.jpg" alt="CellularOutfitter.com" title="CellularOutfitter.com" border="0" /></a>
					<% end if %>
                </div>
                <div style="float:right; width:670px; height:80px;">
                	<div style="float:left; width:670px; height:54px; padding-top:20px;">
                        <div style="float:right; padding-left:10px;">
                        	<a href="javascript:Open_Popup('<%=strHTTP%>://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.CELLULAROUTFITTER.COM&lang=en','VeriSign','width=560,height=500,location=yes,status=yes,resizable=yes,scrollbars=yes');"><div id="verisignLogo" title="CELLULAR OUTFITTER, INC. has been verified by Verisign"></div></a>
                        </div>
                        <div style="float:right; padding-left:10px;"><img src="/images/icons/minibanner-LOW.png" border="0" /></div>
                        <div style="float:right; padding-left:10px;"><img src="/images/icons/minibanner-110.png" border="0" /></div>
                        <div style="float:right; width:45px; padding-top:15px; overflow:hidden;">
                            <div id="fb-root"></div>
							<script src="http://connect.facebook.net/en_US/all.js#appId=194209997302295&amp;xfbml=1"></script>
                            <fb:like href="<%=canonicalURL%>" send="false" layout="button_count" width="50" show_faces="false" font=""></fb:like>
                        </div>
                        <div style="float:right; overflow:hidden; width:40px; padding-top:15px;">
                        	<g:plusone size="medium" count="false" href="<%=canonicalURL%>"></g:plusone>
						</div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding:0px 10px 0px 10px;">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="/images/top/menuBg.gif">
                <tr>
                    <td width="91" align="center"><a href="/c-12-cell-phone-antennas.html" class="cat-font2" title="Cell Phone Antennas">Antennas</a></td>
                    <td align="center"><div id="mainMenuBr"></div></td>
                    <td width="91" align="center"><a href="/c-1-cell-phone-batteries.html" title="Cell Phone Batteries" class="cat-font2">Batteries</a></td>
                    <td align="center"><div id="mainMenuBr"></div></td>
                    <td width="91" align="center"><a href="/bluetooth.html" title="Cell Phone Bluetooth Headsets" class="cat-font2">Bluetooth</a></td>
                    <td align="center"><div id="mainMenuBr"></div></td>					
                    <td width="91" align="center"><a href="/c-7-cell-phone-cases-pouches.html" title="Cell Phone Cases & Pouches" class="cat-font2">Cases &amp; Pouches</a></td>					
                    <td align="center"><div id="mainMenuBr"></div></td>
                    <td width="91" align="center"><a href="/c-2-cell-phone-chargers.html" title="Cell Phone Chargers" class="cat-font2">Chargers</a></td>					
                    <td align="center"><div id="mainMenuBr"></div></td>
                    <td width="91" align="center"><a href="/c-3-cell-phone-covers-screen-guards.html" title="Cell Phone Covers & Screen Guards" class="cat-font2">Covers &amp; Guards</a></td>
                    <td align="center"><div id="mainMenuBr"></div></td>					
                    <td width="91" align="center"><a href="/c-13-cell-phone-data-cables-memory-cards.html" title="Cell Phone Data Cable & Memory Cards" class="cat-font2">Data Cable<br>Memory</a></td>
                    <td align="center"><div id="mainMenuBr"></div></td>					
                    <td width="91" align="center"><a href="/c-5-cell-phone-hands-free-kits-bluetooth-headsets.html" title="Cell Phone Hands-Free Kits & Headsets" class="cat-font2">Hands-Free</a></td>
                    <td align="center"><div id="mainMenuBr"></div></td>
                    <td width="91" align="center"><a href="/c-6-cell-phone-holsters-car-mounts.html" title="Cell Phone Holsters & Car Mounts" class="cat-font2">Holsters &amp;<br>Car Mounts</a></td>
                    <td align="center"><div id="mainMenuBr"></div></td>															
                    <td width="91" align="center"><a href="/wholesale-cell-phones.html" title="Wholesale Cell Phones" class="cat-font2">Wholesale<br>Cell Phones</a></td>
                    <td align="center"><div id="mainMenuBr"></div></td>															
                    <td width="91" align="center"><a href="/c-20-cell-phone-music-skins.html" title="Cell Phone Music Skins" class="cat-font2">Music<br>Skins</a></td>
                    <td align="center"><div id="mainMenuBr"></div></td>															
                    <td width="91" align="center"><a href="/c-18-cell-phone-screen-protectors.html" title="Cell Phone Screen Protectors" class="cat-font2">Screen Protectors</a></td>
                </tr>
            </table>
        </td>
    </tr>
    <%
	if date > cdate("11/24/2011") and date < cdate("11/30/2011") then
		if pageTitle <> "Home" and pageTitle <> "checkout" and pageTitle <> "basket" then
    %>
    <tr><td align="center"><img src="/images/holiday/CO_text.jpg" border="0" alt="Get Massive Discounts Today Only: 50% Off Chargers and 25% Off Covers" title="Get Massive Discounts Today Only: 50% Off Chargers and 25% Off Covers" /></td></tr>
    <%
		end if
	end if
	%>
    <tr>
        <td style="padding:0px 10px 0px 10px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top">
                    	<% if noLeftSide = 0 then %>
                    	<% arrowSize = 14 %>
                    	<div style="width:185px;">
                        <table width="185" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding-top:10px;">
                                	<div style="width:185px; height:80px; background-color:#dedede; padding:3px;">
                                        <form method="get" action="/search/search.asp" name="frmNxtAc" id="nxt-ac-form">
                                        <div style="border:2px solid #FFF; height:70px; background-color:#dedede; padding:5px 0px 0px 5px; width:175px;">
                                            <div style="float:left; font-weight:bold; width:165px; font-size:12px;">SEARCH</div>
                                            <div style="float:left; margin-top:5px; padding:0px 5px 1px 0px; background-color:#FFF;"><img src="/images/icons/search-icon.png" border="0" /></div>
                                            <div style="float:left; font-weight:bold; padding-top:5px;"><input id="nxt-ac-searchbox" type="text" autocomplete="off" name="keywords" value="" size="19" class="inputbox" style="border:none; font-size:14px;"></div>
                                            <div style="float:right; font-weight:bold; padding:5px 6px 0px 0px;"><a href="#" onclick="document.frmNxtAc.submit()"><img src="/images/buttons/search-gobutton.png" border="0" /></a></div>
                                        </div>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:10px;"><img src="/images/banners/header-shopbrand.png" border="0" /></td>
                            </tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-17-apple-cell-phone-accessories.html" title="Apple">Apple</a></td></tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-14-blackberry-cell-phone-accessories.html" title="BlackBerry">BlackBerry</a></td></tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-20-htc-cell-phone-accessories.html" title="HTC">HTC</a></td></tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-4-lg-cell-phone-accessories.html" title="LG">LG</a></td></tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-5-motorola-cell-phone-accessories.html" title="Motorola">Motorola</a></td></tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-6-nextel-cell-phone-accessories.html" title="Nextel">Nextel</a></td></tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-7-nokia-cell-phone-accessories.html" title="Nokia">Nokia</a></td></tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-16-palm-cell-phone-accessories.html" title="Palm">Palm</a></td></tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-18-pantech-cell-phone-accessories.html" title="Pantech">Pantech</a></td></tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-9-samsung-cell-phone-accessories.html" title="Samsung">Samsung</a></td></tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-10-sanyo-cell-phone-accessories.html" title="Sanyo">Sanyo</a></td></tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-19-t-mobile-sidekick-cell-phone-accessories.html" title="Sidekick">Sidekick</a></td></tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-11-siemens-cell-phone-accessories.html" title="Siemens">Siemens</a></td></tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-2-sony-ericsson-cell-phone-accessories.html" title="Sony Ericsson">Sony Ericsson</a></td></tr>
                            <tr><td style="padding:5px 0px 5px 10px;"><span style='color:#66cc66; font-size:<%=arrowSize%>px;'>&raquo;</span> <a class="left_nav" href="/b-15-other-cell-phone-accessories.html" title="Other Brands">Other Brands</a></td></tr>
							<% if request.ServerVariables("URL") <> "/index.asp" and request.ServerVariables("URL") <> "/index-new.asp" then %>
                            <tr>
                                <td colspan="3" align="center" valign="top" style="padding:10px 0px 10px 0px;">
                                    <div style="border-top:1px solid #CCC; padding-top:5px;">
                                        <p align="center"><a href="/privacypolicy.html" target="_blank"><div id="truste"></div></a></p>
                                        <p align="center"><a href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=100097908&source=ctc" target="_blank"><div id="bbb"></div></a></p>
                                        <!-- START GOOGLE CHECKOUT CODE -->
                                        <script type="text/javascript" src="/includes/js/j.js"></script>
                                        <script type="text/javascript">showMark(3);</script>
                                        <noscript><div id="googleChkOutImg"></div></noscript>
                                        <!-- END GOOGLE CHECKOUT CODE -->
                                        <p align="center"><div id="inc5000"></div></p>
                                        <p align="center"><a href="http://www.shopwiki.com/store-252661" target="_blank" title="CellularOutfitter.com is a ShopWiki Approved Store"><div id="shopWiki"></div></a></p>
                                    </div>
                                </td>
                            </tr>
                            <% else %>
                            <tr>
                            	<td colspan="3" align="center" style="padding-top:20px;">
                                	<script type="text/javascript"><!--
									google_ad_client = "ca-pub-1001075386636876";
									/* CO Skyscraper */
									google_ad_slot = "2604906734";
									google_ad_width = 160;
									google_ad_height = 600;
									//-->
									</script>
									<script type="text/javascript"
									src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
									</script>
                                </td>
                            </tr>
							<% end if %>
                            <% if leftGoogleAd = 1 then %>
                            <!--#include virtual="/includes/asp/inc_GoogleAdSense2.asp"-->
                            <% end if %>
                        </table>
                        </div>
                        <% end if %>
                    </td>
                    <td width="100%" valign="top" style="padding-left:15px;">