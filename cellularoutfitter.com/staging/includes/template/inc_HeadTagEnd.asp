<%
dim UserIPAddress
	UserIPAddress = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
if UserIPAddress = "" then
  UserIPAddress = Request.ServerVariables("REMOTE_ADDR")
end if
dim isInternalUser : isInternalUser = false

if UserIpAddress = "66.159.49.66" then
	isInternalUser = true
end if
%>
<%
mobileDetect()
if request.ServerVariables("HTTPS") = "off" then useHttp = "http" else useHttp = "https"
%>
<!--#include virtual="/framework/utility/noindex.asp"-->
<% call printPixel(1) %>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/json3/3.3.1/json3.min.js"></script>

<!-- GoogleAnalytics Start -->
<script type="text/javascript">
	var _gaq = _gaq || [];
	var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
	_gaq.push(['_require', 'inpage_linkid', pluginUrl]);
  	_gaq.push(['_setAccount', 'UA-1097464-1']);

  	<% if prepStr(mvtGaq) <> "" then %>
	_gaq.push(['_setCustomVar',1,'mvtVarient','<%=mvtGaq%>',2]);
	<% else %>
	//mvtGAQ
	<% end if %>
	
	_gaq.push(['_trackPageview']);
	_gaq.push(['_trackPageLoadTime']); 
	<% if showGoogleAnalyticsLoggedIn then 'this variable is set to true on /index_account.asp %>_gaq.push(['_setCustomVar',1,'LoggedInUsers','1',2]);<% end if %>
	
	(function(){
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
//		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>

<%if instr(request.ServerVariables("SERVER_NAME"), "staging.") = 0 then%>
<!-- BEGIN: Google Trusted Store -->
<script type="text/javascript">
	var gts = gts || [];
	
	gts.push(["id", "2884"]);
	<%
	trustStoreItemID = ""
	sql = 	"exec sp_googleTrustedItem 2"
	set rsTrust = oConn.execute(sql)
	if not rsTrust.eof then trustStoreItemID = rsTrust("itemid")
	%>
	gts.push(["google_base_offer_id", "<%=trustStoreItemID%>"]);
	gts.push(["google_base_subaccount_id", "139792"]);
	gts.push(["google_base_country", "US"]);
	gts.push(["google_base_language", "EN"]);
//	gts.push(["gtsContainer","we_google_trusted_badge"]);
	
	(function() {
		var scheme = (("https:" == document.location.protocol) ? "https://" : "http://");
		var gts = document.createElement("script");
		gts.type = "text/javascript";
		gts.async = true;
		gts.src = scheme + "www.googlecommerce.com/trustedstores/gtmp_compiled.js";
		var s = document.getElementsByTagName("script")[0];
		s.parentNode.insertBefore(gts, s);
	})();
</script>
<!-- END: Google Trusted Store -->
<%end if%>

<script type="text/javascript">
if(typeof window.WEDATA == 'undefined'){
	window.WEDATA = {
		storeName: 'CellularOutfitter.com',
		internalUser: <%= jsStr(LCase(isInternalUser)) %>,
		pageType: <%= jsStr(replace(replace(request.ServerVariables("URL"),".asp",""),"/","")) %>,
		account: {
			email: <%= jsStr(Request.Cookies("user")("email")) %>,
			id: <%= jsStr(Request.Cookies("user")("id")) %>
		}


	};
}
</script>

<% if instr(request.ServerVariables("SERVER_NAME"), "staging") > 0  then %>
<script src="//assets.adobedtm.com/d0f694a37a1061afe4e18245479cdeaed180db2f/satelliteLib-6c61417c205e2f84dfcdbf654877e85bba16c2d8-staging.js"></script>
<% else %>
<script src="//assets.adobedtm.com/d0f694a37a1061afe4e18245479cdeaed180db2f/satelliteLib-6c61417c205e2f84dfcdbf654877e85bba16c2d8.js"></script>
<% end if %>