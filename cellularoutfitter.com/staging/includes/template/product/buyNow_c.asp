<%
useTime = now
totalSeconds = datediff("s",useTime,date+1)
totalMinutes = totalSeconds/60
if cint(totalMinutes) > totalMinutes then totalMinutes = cint(totalMinutes)-1 else totalMinutes = cint(totalMinutes)
totalSeconds = totalSeconds - (totalMinutes*60)
totalHours = totalMinutes/60
if cint(totalHours) > totalHours then totalHours = cint(totalHours)-1 else totalHours = cint(totalHours)
totalMinutes = totalMinutes - (totalHours*60)
%>
<div class="tb buyNowDetailRow">
	<div class="fl itemBuyNowTitle" style="padding-top:7px;">Retail Price:</div>
    <div class="fl itemRetailValue"><%=formatCurrency(price_retail,2)%></div>
</div>
<div class="tb buyNowDetailRow sitewideSale">
	<div class="fl itemBuyNowTitle2">Wholesale Price:</div>
    <div class="fl"><%=formatCurrency(price_co,2)%></div>
</div>
<div class="tb buyNowDetailRow">
	<div class="fl itemBuyNowTitle">You Save:</div>
    <div class="fl itemSaveValue"><%=formatCurrency(price_retail - price_CO,2)%> (<%=formatPercent((price_retail - price_CO) / price_retail,0)%>)</div>
</div>
<% if prodQty < 1 and not alwaysInStock then %>
<div class="tb" style="font-weight:bold; color:#F00; padding-bottom:10px;">Temporarily Out of Stock</div>
<% else %>
<div class="tb saleTimeLeft">
	<div class="fl clockIcon"></div>
    <div class="fl saleTimeText">
    	<span style="font-weight:bold; font-style:italic;">Hurry:</span> Only <span style="color:#cc0000; font-weight:bold;"><%=totalHours%> hours</span> 
        left to get the lowest pricing in store history. Order now!
    </div>
</div>
<form name="popSubmit" action="/cart/item_add.asp" method="post" style="margin:0px; padding:0px;">
<div class="tb buyNowDetailRow">
	<div class="fl itemBuyNowQty">Quantity:</div>
    <div class="fl qtyBox">
    	<input name="qty" type="text" value="1" class="qtyField" />
        <input type="hidden" name="prodid" value="<%=itemID%>" />
    </div>
    <div class="fl"><input type="image" src="/images/buttons/addToCart_greyGreen.gif" border="0" width="275" height="50" /></div>
</div>
</form>
<% end if %>
<div class="tb" style="border-top:1px dotted #CCC; padding:10px 0px 10px 0px; width:420px;">
	<% if useUvp <> "" then %>
    <div class="tb productCheckRow">
        <div style="float:left; width:17px;"><img src="/images/icons/check.png" border="0" width="17" height="14" /></div>
        <div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#333;"><%=useUvp%></div>
    </div>
    <% end if %>
    <div class="tb productCheckRow">
	    <div style="float:left; width:17px;"><img src="/images/icons/check.png" border="0" width="17" height="14" /></div>
    	<div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#333;">Leaves Our US Warehouse Within 24 Hours</div>
    </div>
    <div class="tb productCheckRow">
	    <div style="float:left; width:17px;"><img src="/images/icons/check.png" border="0" width="17" height="14" /></div>
	    <div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#333;">Lowest Prices Online Guaranteed</div>
    </div>
    <div class="tb productCheckRow">
	    <div style="float:left; width:17px;"><img src="/images/icons/check.png" border="0" width="17" height="14" /></div>
    	<div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#333;">
            Unconditional 90 Day Return Policy
            <% if customize then %>
            &nbsp;*<br /><span style="color:#999; font-size:10px;">* except for customized items</span>
            <% end if %>
        </div>
    </div>
</div>
<div class="tb paymentOptions"></div>