function ajax(newURL,rLoc) {
    var httpRequest;
    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
        httpRequest = new XMLHttpRequest();
        if (httpRequest.overrideMimeType) {
            httpRequest.overrideMimeType('text/xml');
        }
    } else if (window.ActiveXObject) { // IE
        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }
    httpRequest.open('GET', newURL, true);
    httpRequest.onreadystatechange = function(){
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                badRun = 0
                var rVal = httpRequest.responseText
                if (rVal == "") {
                    alert("No data available")
                }
                else if (rVal == "refresh") {
                    curLoc = window.location
                    window.location = curLoc
                }
                else if (rVal == "wrong recaptcha") {
                    Recaptcha.reload();
                    document.getElementById('recaptcha_msg').innerHTML = '<span style="color:red;"><b>You have entered the wrong secret words.</b></span>';
                }
                else {
                    correctAnswer = true;
                    if(document.getElementById(rLoc)) {
                        document.getElementById(rLoc).innerHTML = rVal;
                    }
                }
                rVal = null;
            }
            else {
                //document.getElementById("testZone").innerHTML = newURL
                alert("Error performing action");
            }
        }
    };
    httpRequest.send(null);
}

function closeReviewPopup() {
	document.getElementById('popCover').style.display = 'none';
	document.getElementById('popBox').style.display = 'none';
	document.getElementById('popBox').innerHTML = '';
}

function formatSEO(s){
	var output = s.replace(/[^a-zA-Z0-9]/g,' ').replace(/\s+/g,"-").toLowerCase();
	/* remove first dash */
	if(output.charAt(0) == '-') output = output.substring(1);
	/* remove last dash */
	var last = output.length-1;
	if(output.charAt(last) == '-') output = output.substring(0, last);
	
	return output;
}

function checkEmail(email) {
	email = email.trim();
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	
	if (!filter.test(email)) {
		return false;
	}
	return true;
}
