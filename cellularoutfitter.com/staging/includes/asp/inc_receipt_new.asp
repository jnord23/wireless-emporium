<%

function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
end function

dim ReceiptText
ReceiptText = ""

dim sPromoCode
SQL = "SELECT B.PromoCode FROM we_orders A INNER JOIN CO_coupons AS B ON A.couponid=B.couponid WHERE A.orderid = '" & nOrderID & "'"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.EOF then
	SQL = replace(lcase(SQL),"we_orders","we_orders_historical")
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
end if

if not RS.eof then sPromoCode = RS("PromoCode")

dim sFname, sLname, sEmail, sPhone
dim saddress1, saddress2, sCity, sstate, szip, sCountry
dim sShipAddress, sBillAddress
dim oRsCust, oRsOrd

SQL = "SELECT * FROM CO_accounts WHERE accountid = '" & nAccountId & "'"
set oRsCust = Server.CreateObject("ADODB.Recordset")
oRsCust.open SQL, oConn, 3, 3
if oRsCust.eof then
	ReceiptText = "<h3>Customer account" & nAccountId & " not found!</h3>"
else
	sFname = oRsCust("fname")
	sLname = oRsCust("lname")
	sEmail = oRsCust("email")
	sPhone = oRsCust("phone")
end if

SQL = "SELECT * FROM we_orders WHERE orderid = '" & nOrderId & "'"
set oRsOrd = Server.CreateObject("ADODB.Recordset")
oRsOrd.open SQL, oConn, 0, 1

if oRsOrd.EOF then
	SQL = replace(lcase(SQL),"we_orders","we_orders_historical")
	set oRsOrd = Server.CreateObject("ADODB.Recordset")
	oRsOrd.open SQL, oConn, 0, 1
end if

if oRsOrd.eof then ReceiptText = "<h3>Order" & nOrderId & " not found!</h3>"

strShippingMethod = oRsOrd("shiptype")

'format the shipping and billing addresses
dim nShippingid
nShippingid = oRsOrd("shippingid")
if nShippingid > 0 then
	strSql = "SELECT * FROM we_addl_shipping_addr WHERE id='" & nShippingid & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open strSql, oConn, 0, 1
	if not RS.eof then
		saddress1 = RS("saddress1")
		saddress2 = RS("saddress2")
		sCity = RS("sCity")
		sstate = RS("sstate")
		szip = RS("szip")
		sCountry = RS("sCountry")
	end if
else
	saddress1 = oRsCust("saddress1")
	saddress2 = oRsCust("saddress2")
	sCity = oRsCust("sCity")
	sstate = oRsCust("sstate")
	szip = oRsCust("szip")
	sCountry = oRsCust("sCountry")
end if

testMsg = ""
if saddress1 = "4040 N. Palm St." then
	SQL = "select cancelled from we_orders where orderID = '" & nOrderId & "'"
	set checkOrderRS = oConn.execute(SQL)
	
	if not checkOrderRS.EOF then
		if isnull(checkOrderRS("cancelled")) then
			call cancelTestOrder(nOrderId)
		end if
		testMsg = "This is a test order and has been auto canceled"
	else
		testMsg = "Cancel Bypass:" & SQL
	end if
end if

sShipAddress = "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & sAddress1 & "</p>" & vbcrlf
if sAddress2 <> "" then 
	sShipAddress = sShipAddress & "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & sAddress2 & "</p>" & vbcrlf
end if
sShipAddress = sShipAddress & "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & sCity & ", " & sState & "&nbsp;" & sZip & "&nbsp;" & sCountry & "</p>" & vbcrlf

sBillAddress = "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & oRsCust("bAddress1") & "</p>" & vbcrlf
if oRsCust("bAddress2") <> "" then 
	sBillAddress = sBillAddress & "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & oRsCust("bAddress2") & "</p>" & vbcrlf
end if
sBillAddress = sBillAddress & "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & oRsCust("bCity") & ", " & oRsCust("bState") & "&nbsp;" & oRsCust("bZip") & "&nbsp;" & oRsCust("bCountry") & "</p>" & vbcrlf




oRsCust.close
set oRsCust = nothing

if ReceiptText = "" then
	'format order info
	'dim nOrderSubTotal
'	nOrderSubTotal = oRsOrd("ordersubtotal")
	emailSubTotal = oRsOrd("ordersubtotal")
	sShipType = oRsOrd("shiptype")
'	nShipFee = oRsOrd("ordershippingfee")
	emailShipFee = oRsOrd("ordershippingfee")
	if prepInt(nShipFee) = 0 then nShipFee = emailShipFee
'	nOrderTax = oRsOrd("orderTax")
	emailOrderTax = oRsOrd("orderTax")
	if prepInt(nOrderTax) = 0 then nOrderTax = emailOrderTax
	nBuysafeamount = oRsOrd("BuySafeAmount")
'	nOrderGrandTotal = oRsOrd("ordergrandtotal")
	emailOrderGrandTotal = oRsOrd("ordergrandtotal")
	extOrderType = oRsOrd("extOrderType")
	extOrderNumber = oRsOrd("extOrderNumber")
	nOrderDateTime = oRsOrd("orderdatetime")
	
	dim strOrderType
	select case extOrderType
		case 1 : strOrderType = "Paypal<br>" & extOrderNumber
		case 2 : strOrderType = "Google Checkout<br>" & extOrderNumber
		case 3
			strOrderType = "eBillme"
			eBillme = "eBillme"
			eBillmeAccount = extOrderNumber
		case else : strOrderType = "Credit Card"
	end select
	
	oRsOrd.close
	set oRsOrd = nothing
	
	'===================
	'NEW WEBLOYALTY CODE
	dim RSwl, cctype, country, pcode, strToEncrypt, strEncryptedValue
	SQL = "SELECT A.CCtype, B.bState, B.bZip FROM we_ccinfo A INNER JOIN CO_Accounts B ON A.accountid = B.accountid WHERE A.orderid='" & nOrderID & "'"
	set RSwl = Server.CreateObject("ADODB.Recordset")
	RSwl.open SQL, oConn, 0, 1
	if not RSwl.eof then
		cctype = RSwl("CCtype")
		if len(RSwl("bState")) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",RSwl("bState")) > 0 then
			country = "CANADA"
		else
			country = "US"
		end if
		pcode = RSwl("bZip")
	end if
	RSwl.close
	set RSwl = nothing
	
	' Put together string ?concatenate all required values
	if incEmail = true then
		dim yesexiturl, noexiturl
		yesexiturl = "https://www.cellularoutfitter.com/confirm.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & emailOrderGrandTotal & "&c=" & emailSubTotal
		noexiturl = yesexiturl
		strToEncrypt = "cref" & chr(255) & nOrderID & chr(255) & "cctype" & chr(255) & cctype & chr(255) & "country" & chr(255) & country & chr(255) & "pcode" & chr(255) & pcode & chr(255) & "yesexiturl" & chr(255) & yesexiturl & chr(255) & "noexiturl" & chr(255) & noexiturl
	else
		strToEncrypt = "cref" & chr(255) & nOrderID & chr(255) & "cctype" & chr(255) & cctype & chr(255) & "country" & chr(255) & country & chr(255) & "pcode" & chr(255) & pcode
	end if
	' Encrypt it
	strEncryptedValue = SimpleEncrypt(strToEncrypt)
	'===================
	
'	linkStagingDomain	= "http://staging.cellularoutfitter.com/"
'	if nOrderGrandTotal then
'		self_link = linkStagingDomain & "confirm.asp?o=" & nOrderID & "&a=" & nAccountID & "&d=" & nOrderAmount & "&ppd=" & nOrderGrandTotal & "&dzid=email_TRANS_HEADER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=orderconf&utm_promocode=NA"
'	else
'		self_link = linkStagingDomain & "confirm.asp?o=" & nOrderID & "&a=" & nAccountID & "&d=" & nOrderAmount & "&pp=" & usePostPurchase & "&dzid=email_TRANS_HEADER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=orderconf&utm_promocode=NA"
'	end if

	linkDomain	= "https://www.cellularoutfitter.com/"
	if instr(request.ServerVariables("SERVER_NAME"), "staging.cellularoutfitter.com") > 0 then linkDomain = "http://staging.cellularoutfitter.com/"
	self_link = linkDomain & "confirm.asp?o=" & nOrderID & "&a=" & nAccountID & "&dzid=email_TRANS_HEADER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=orderconf&utm_promocode=NA"
	
	'get orderdetails
	dim oRsOrderDetails
'	SQL = 	"SELECT c.brandName, d.modelName, A.itemid,A.PartNumber,A.itemDesc_CO,A.itemPic_CO,isnull(b.price, A.price_CO) price_co,B.quantity FROM we_items A INNER JOIN we_orderdetails B ON A.itemid = B.itemid left join we_brands c on a.brandID = c.brandID left join we_models d on a.modelID = d.modelID WHERE B.orderid = '" & nOrderId & "'" &_
'			" union " &_
'			"SELECT '' as brandName, '' as modelName, A.id as itemid,A.musicSkinsID as PartNumber,A.artist + ' ' + a.designName as itemDesc_CO,A.defaultImg as itemPic_CO,A.price_CO,B.quantity FROM we_items_musicSkins A INNER JOIN we_orderdetails B ON A.id = B.itemid WHERE B.orderid = '" & nOrderId & "'"
	sql	=	"select	c.brandname, d.modelname, a.itemid,a.partnumber,a.itemdesc_co,a.itempic_co,isnull(b.price, a.price_co) price_co,b.quantity" & vbcrlf & _
			"	,	case when e.id is not null then 1 else 0 end promoItem," & vbcrlf & _
			" CASE " &_
			" WHEN (b.cogs IS NULL OR b.cogs = 0) THEN a.cogs "&_
			" ELSE b.cogs " &_
			" END cogs " &_
			"from	we_items a inner join we_orderdetails b " & vbcrlf & _
			"	on	a.itemid = b.itemid left join we_brands c " & vbcrlf & _
			"	on	a.brandid = c.brandid left join we_models d " & vbcrlf & _
			"	on	a.modelid = d.modelid left outer join we_config e" & vbcrlf & _
			"	on	b.itemid = e.configValue and e.siteid = 2 and e.configName = 'Free ItemID'" & vbcrlf & _
			"where	b.orderid = '" & nOrderId & "' " & vbcrlf & _
			"union " & vbcrlf & _
			"select '' as brandname, '' as modelname, a.id as itemid,a.musicskinsid as partnumber,a.artist + ' ' + a.designname as itemdesc_co,a.defaultimg as itempic_co,a.price_co,b.quantity, 0 promoItem," & vbcrlf & _
			" CASE " &_
			" WHEN (b.cogs IS NULL OR b.cogs = 0) THEN a.cogs "&_
			" ELSE b.cogs " &_
			" END cogs " &_
			"from	we_items_musicskins a inner join we_orderdetails b " & vbcrlf & _
			"	on	a.id = b.itemid " & vbcrlf & _
			"where	b.orderid = '" & nOrderId & "'"
	'response.write SQL
	set oRsOrderDetails = Server.CreateObject("ADODB.Recordset")
	oRsOrderDetails.open SQL, oConn, 3, 3
	
	if oRsOrderDetails.EOF then
		SQL = replace(lcase(SQL),"we_orders","we_orders_historical")
		SQL = replace(SQL,"we_orderdetails","we_orderdetails_historical")
		set oRsOrderDetails = Server.CreateObject("ADODB.Recordset")
		oRsOrderDetails.open SQL, oConn, 3, 3
	end if
	
	strOrderDetails = ""
	if not oRsOrderDetails.eof then
		dim nCount, nID, nPartNumber, nQty, nPrice, itemdesc_CO, thisSubtotal, itemids, itemSubTotal
		itemSubTotal = cdbl(0)
		nCount = 1
		do until oRsOrderDetails.eof
			brandName = oRsOrderDetails("brandName")
			modelName = oRsOrderDetails("modelName")
			nID = oRsOrderDetails("itemid")
			nPartNumber = oRsOrderDetails("PartNumber")
			partNumber = nPartNumber
			nQty = cdbl(oRsOrderDetails("quantity"))
			nPrice = cdbl(oRsOrderDetails("price_CO"))
			subTotal = cdbl(nQty * nPrice)
			cogs = cdbl(oRsOrderDetails("cogs"))
			itemSubTotal = itemSubTotal + subTotal
			itemdesc_CO = insertDetails(oRsOrderDetails("itemDesc_CO"))
			itempic = oRsOrderDetails("itemPic_CO")
			promoItem = oRsOrderDetails("promoItem")
'			itemImgPath = "C:\inetpub\wwwroot\productpics_co\thumb\" & itempic
			itemImgPath = server.MapPath("\productpics\thumb\" & itempic)
			
			set fsThumb = CreateObject("Scripting.FileSystemObject")
			if not fsThumb.FileExists(itemImgPath) then
				useImg = "/productPics/thumb/imagena.jpg"
				DoNotDisplay = 1
			else
				useImg = "/productPics/thumb/" & itempic
			end if
			
			if incEmail = true then
				product_link = "http://www.cellularoutfitter.com/p-" & nID & "-" & formatSEO(itemdesc_CO) & ".html?dzid=email_TRANS_BODY_" & nCount & "L_" & nID & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=orderconf&utm_promocode=NA"
			else
				product_link = "http://www.cellularoutfitter.com/p-" & nID & "-" & formatSEO(itemdesc_CO) & ".html"
			end if
			
			strOrderDetails = strOrderDetails & "<tr>" & vbcrlf & _
												"	<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;text-align:left;vertical-align:top;padding:10px 5px;border-left:1px solid #ccc;border-bottom:1px solid #ccc;border-right:1px solid #ccc;'>" & nPartNumber & "</td>" & vbcrlf & _
												"	<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;text-align:center;'><a href=""" & product_link & """><img src='https://www.cellularoutfitter.com" & useImg & "' height='65' /></a></td>" & vbcrlf & _
												"	<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;text-align:center;'>" & nQty & "</td>" & vbcrlf & _
												"	<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;'><a href=""" & product_link & """ style=""color:#000000;"">" & itemdesc_CO & "</a></td>" & vbcrlf & _
												"	<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;text-align:center;'>" & formatcurrency(nPrice) & "</td>" & vbcrlf & _
												"	<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;text-align:center;'>" & formatcurrency(subTotal) & "</td>" & vbcrlf & _
												"</tr>" & vbcrlf
			nCount = nCount + 1
			nTotQty = nTotQty + nQty
			
			weDataString = weDataString & "window.WEDATA.orderData.cartItems.push({partNumber: '" & nPartNumber & "', qty: " & nQty & ",price: " & nPrice & ",itemId: " & nID & ", cogs: " & cogs & "});" & vbcrlf
			
			if promoItem = 0 then
				if itemids = "" then
					itemids = nID
				else
					itemids = itemids & "," & nID
				end if
			end if
			oRsOrderDetails.movenext
		loop
	end if
	
	email_num_link = nCount
	
	if itemids then
		sqlUpSell = "exec sp_ppUpsell 2, '" & itemids & "', 0, 30"
		set objRsUpSell = oConn.execute(sqlUpSell)
		nCount = 1
		if not objRsUpSell.EOF then
			strUpSell 	=	"<table width='558' cellpadding='0' cellspacing='0' style='margin:20px 40px;border:2px solid #cccccc;'>" & vbcrlf & _
							"	<tr>" & vbcrlf & _
							"		<td style='padding:1px;'>" & vbcrlf & _
							"			<table width='554' cellpadding='0' cellspacing='0' style='border:1px solid #cccccc;'>" & vbcrlf & _
							"				<tr>" & vbcrlf & _
							"					<td style='font-family:Verdana, Helvetica, sans-serif;font-size:13px;line-height:20px;text-align:center;padding:15px;'>" & vbcrlf & _
							"						WE THOUGHT YOU MIGHT BE INTERESTED IN THESE GREAT PRODUCTS.<br />" & vbcrlf & _
							"						THEY ARE HIGHLY RECOMMENDED BY CUSTOMERS LIKE YOU." & vbcrlf & _
							"					</td>" & vbcrlf & _
							"				</tr>" & vbcrlf & _
							"			</table>" & vbcrlf & _
							"		</td>" & vbcrlf & _
							"	</tr>" & vbcrlf & _
							"</table>" & vbcrlf & _
							"<table width='640' cellpadding='0' cellspacing='0' style='padding:10px 0;border-bottom:2px solid #cccccc;'>" & vbcrlf & _
							"	<tr>"
			counter = 1
			do until objRsUpSell.EOF
				itemID		= objRsUpSell("itemID")
				itemPic		= objRsUpSell("itemPic")
				itemDesc	= objRsUpSell("itemDesc")
				useItemDesc = replace(itemDesc,"  "," ")
'				itemImgPath = "C:\inetpub\wwwroot\productpics_co\big\" & itempic
				itemImgPath = server.MapPath("\productpics\big\" & itempic)
				price		= objRsUpSell("price")
				price_retail= objRsUpSell("price_retail")
				
				set fsThumb = CreateObject("Scripting.FileSystemObject")
				if not fsThumb.FileExists(itemImgPath) then
					useImg = "/productPics/big/imagena.jpg"
					DoNotDisplay = 1
				else
					useImg = "/productPics/big/" & itemPic
				end if
				
				if incEmail = true then
					product_link = "http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(useItemDesc) & ".html?dzid=email_TRANS_RECS_" & nCount & "L_" & itemID & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=orderconf&utm_promocode=NA"
				else
					product_link = "http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(useItemDesc) & ".html"
				end if
				
				if counter < 4 then 
					strUpSell 	= strUpSell &	"	<td style='border-right:2px solid #cccccc;width:138px;padding:10px;text-align:center;vertical-align:top;'>" & vbcrlf & _
												"		<a href='" & product_link & "'><img src='https://www.cellularoutfitter.com" & useImg & "' height='132' /></a>" & vbcrlf & _
												"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;line-height:18px;color:#006596;font-weight:bold;font-size:12px;text-align:left;height:90px;'><a href='" & product_link & "' style='color:#006596;font-weight:bold;font-size:12px;'>" & itemDesc & "</a></p>" & vbcrlf & _
												"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;font-size:12px;text-align:left;color:#cccccc;text-decoration:line-through;'>Retail Price: " & formatCurrency(price_retail) & "</p>" & vbcrlf & _
												"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;margin-top:10px;font-size:16px;font-weight:bold;text-align:left;color:#000000;'>Our Price:<br />" & formatCurrency(price) & "</p>" & vbcrlf & _
												"	</td>"
				else 
					strUpSell 	= strUpSell &	"	<td style='width:138px;padding:10px;text-align:center;vertical-align:top;'>" & vbcrlf & _
											"		<a href='" & product_link & "'><img src='https://www.cellularoutfitter.com" & useImg & "' height='132' /></a>" & vbcrlf & _
											"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;line-height:18px;color:#006596;font-weight:bold;font-size:12px;text-align:left;height:90px;'><a href='" & product_link & "' style='color:#006596;font-weight:bold;font-size:12px;'>" & itemDesc & "</a></p>" & vbcrlf & _
											"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;font-size:12px;text-align:left;color:#cccccc;text-decoration:line-through;'>Retail Price: " & formatCurrency(price_retail) & "</p>" & vbcrlf & _
											"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;margin-top:10px;font-size:16px;font-weight:bold;text-align:left;color:#000000;'>Our Price:<br />" & formatCurrency(price) & "</p>" & vbcrlf & _
											"	</td>"
				end if
				
				if counter = 4 then
					exit do
				else
					counter = counter + 1
				end if
				nCount = nCount + 1
				objRsUpSell.movenext
			loop
		end if
		strUpSell		= strUpSell & "</tr>" & vbcrlf & _
						"</table>"
	end if
	
	
	oRsOrderDetails.close
	set oRsOrderDetails = nothing
	if isnull(sFname) or len(sFname) <= 0 then
		custName			=	"<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>Customer</p>"
		custNameAddress		=	"<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>Customer</p>"
	else
		custName			=	ucase(left(sFname, 1)) & lcase(right(sFname,len(sFname)-1))
		custNameAddress		=	"<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & ucase(left(sFname, 1)) & lcase(right(sFname,len(sFname)-1)) & " " & ucase(left(sLname, 1)) & lcase(right(sLname,len(sLname)-1)) & "</p>"
	end if
	
	if sPhone <> "" then
		strTelephone			= "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>T: " & sPhone & "</p>"
	else
		strTelephone			= ""
	end if
	
	strOrderTax = ""
	if emailOrderTax > 0 then
		strOrderTax = strOrderTax &		"<tr>" & vbcrlf
		strOrderTax = strOrderTax &		"	<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;width:154px;text-align:right;border-bottom:1px solid #cccccc;"">Tax:</td>" & vbcrlf
		strOrderTax = strOrderTax &		"	<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;text-align:right;padding-right:20px;border-bottom:1px solid #cccccc;"">" & formatCurrency(emailOrderTax,2) & "</td>" & vbcrlf
		strOrderTax = strOrderTax &		"</tr>" & vbcrlf
	end if
	
	strPromoCode = ""
	if sPromoCode <> "" then
		strPromoCode = strPromoCode &	"<tr>" & vbcrlf
		strPromoCode = strPromoCode &	"	<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;width:154px;text-align:right;border-bottom:1px solid #cccccc;"">PromoCode:</td>" & vbcrlf
		strPromoCode = strPromoCode &	"	<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;text-align:right;padding-right:20px;border-bottom:1px solid #cccccc;"">" & sPromoCode & "</td>" & vbcrlf
		strPromoCode = strPromoCode &	"</tr>" & vbcrlf
	end if	
	
	strDiscount = ""
	if (cdbl(emailOrderGrandTotal) - (cdbl(emailSubTotal)+cdbl(emailShipFee)+cdbl(emailOrderTax))) < 0 then
		strDiscount = strDiscount &		"<tr>" & vbcrlf
		strDiscount = strDiscount &		"	<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;width:154px;text-align:right;border-bottom:1px solid #cccccc;"">Discount:</td>" & vbcrlf
		strDiscount = strDiscount &		"	<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;text-align:right;padding-right:20px;border-bottom:1px solid #cccccc;"">" & formatCurrency((cdbl(emailOrderGrandTotal) - (cdbl(emailSubTotal)+cdbl(emailShipFee)+cdbl(emailOrderTax))),2) & "</td>" & vbcrlf
		strDiscount = strDiscount &		"</tr>" & vbcrlf
	end if
	
	strPostPurchase = ""
	if (emailOrderGrandTotal - nOrderGrandTotal) > 1 and nOrderGrandTotal > 0 then
		strPostPurchase = strPostPurchase &		"<table width=""263"" cellpadding=""5"" cellspacing=""0"" style=""padding-top:10px;"">" & vbcrlf
		strPostPurchase = strPostPurchase &		"	<tr>" & vbcrlf
		strPostPurchase = strPostPurchase &		"		<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;padding-top:5px;width:154px;text-align:center;color:#000000;font-weight:normal;"">" & vbcrlf
		strPostPurchase = strPostPurchase &		"			Thank you for your order! Please remember that you will see two charges on your account that total <b>" & formatCurrency(emailOrderGrandTotal,2) & "</b>. One charge in the amount of <b>" & formatCurrency((emailOrderGrandTotal - nOrderGrandTotal),2) & "</b> and the other in the amount of <b>" & formatCurrency(nOrderGrandTotal,2) & "</b>." & vbcrlf
		strPostPurchase = strPostPurchase &		"		</td>" & vbcrlf
		strPostPurchase = strPostPurchase &		"	</tr>" & vbcrlf
		strPostPurchase = strPostPurchase &		"</table>" & vbcrlf
	end if
	
	dzid = request.querystring("dzid") 
	
	if incEmail = true or dzid <> "" then
		ReceiptText = readTextFile(server.MapPath("\images\email\template\orderConfirmEmail.htm"))
	else
		ReceiptText = readTextFile(server.MapPath("\images\email\template\orderConfirmPage.htm"))
	end if
	
	addoubleclick = ""
	' WEBLOYALTY
	addoubleclick = addoubleclick & "<table width=""25%"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" style=""padding:10px 0;""><tr><td>" & vbcrlf
	if incEmail = true then
		addoubleclick = addoubleclick & "<a href=""https://one-time-offer.com/sg/gateway.aspx?v=0353A36353&p=92D2E21232BE77B7D7F706E72FC3C3830303639F16D6060727&cl=1545"" target=""_blank""><img src=""http://www.cellularoutfitter.com/images/checkout/rebate_img.png"" width=""276"" height=""66"" border=""0"" alt=""Click here now to claim your Cash Back Incentive on your next purchase when you enroll in Webloyalty's service. See offer and billing details.""></a>" & vbcrlf
	else
		addoubleclick = addoubleclick & "<SCRIPT language=""JavaScript1.1"" SRC=""https://ad.doubleclick.net/adj/N3446.Wireless_Emporium/B3113366;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?""></SCRIPT>" & vbcrlf
		addoubleclick = addoubleclick & "<NOSCRIPT>" & vbcrlf
		addoubleclick = addoubleclick & "<A HREF=""https://ad.doubleclick.net/jump/N3446.Wireless_Emporium/B3113366;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?"" target=""_blank""><IMG SRC=""https://ad.doubleclick.net/ad/N3446.Wireless_Emporium/B3113366;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?"" BORDER=0 WIDTH=300 HEIGHT=130 ALT=""""></A>" & vbcrlf
		addoubleclick = addoubleclick & "</NOSCRIPT>" & vbcrlf
	end if
	addoubleclick = addoubleclick & "</td></tr></table>" & vbcrlf
	
	
	ReceiptText = replace(ReceiptText, "[SELF LINK]", self_link)
	ReceiptText = replace(ReceiptText, "[ORDER EMAIL]", sEmail)
	ReceiptText = replace(ReceiptText, "[ORDER DATE]", nOrderDateTime)
	ReceiptText = replace(ReceiptText, "[ORDER ID]", nOrderId)
	ReceiptText = replace(ReceiptText, "[SHIPPING METHOD]", strShippingMethod)
	ReceiptText = replace(ReceiptText, "[ORDER DETAILS]", strOrderDetails)
	ReceiptText = replace(ReceiptText, "[CUSTOMER NAME ADDRESS]", custNameAddress)
	ReceiptText = replace(ReceiptText, "[BILLED TO]", sBillAddress)
	ReceiptText = replace(ReceiptText, "[SHIPPED TO]", sShipAddress)
	ReceiptText = replace(ReceiptText, "[TELEPHONE]", strTelephone)
	ReceiptText = replace(ReceiptText, "[ORDER SUBTOTAL]", formatCurrency(emailSubTotal,2))
	ReceiptText = replace(ReceiptText, "[ORDER TAX]", strOrderTax)
	ReceiptText = replace(ReceiptText, "[SHIPPING FEE]", formatCurrency(emailShipFee,2))
	ReceiptText = replace(ReceiptText, "[PROMO CODE]", strPromoCode)
	ReceiptText = replace(ReceiptText, "[DISCOUNT]", strDiscount)
	ReceiptText = replace(ReceiptText, "[GRAND TOTAL]", formatCurrency(emailOrderGrandTotal,2))
	ReceiptText = replace(ReceiptText, "[POST PURCHASE]", strPostPurchase)
	ReceiptText = replace(ReceiptText, "[UPSELL]", strUpSell)
	ReceiptText = replace(ReceiptText, "[COPYRIGHT YEAR]", Year(Date))
	ReceiptText = replace(ReceiptText, "[EMAIL NUM LINK]", email_num_link)
	ReceiptText = replace(ReceiptText, "[WEB LOYALTY]", addoubleclick)
end if
%>