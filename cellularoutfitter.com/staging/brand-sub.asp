<%
	response.buffer = true
	response.expires = 70
	response.CacheControl = "public"

	pageTitle = "BrandSub"
	
	dim subBrandID : subBrandID = prepInt(request.querystring("subBrandID"))
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
	dim fs : set fs = CreateObject("Scripting.FileSystemObject")
	if subBrandID = 0 then PermanentRedirect("/")
	leftGoogleAd = 1
	
	sql = 	"select	b.brandid, a.modelid, a.modelname, a.modelimg, a.carriercode" & vbcrlf & _
			"	,	a.topModel, a.oldModel, a.international, b.brandname, b.brandimg " & vbcrlf & _
			"	,	c.subBrandName_WE, a.releaseyear, a.releasequarter" & vbcrlf & _
			"from 	we_models a join we_brands b" & vbcrlf & _
			"	on	a.brandid = b.brandid join we_subbrands c" & vbcrlf & _
			"	on	a.subbrandid = c.subbrandid " & vbcrlf & _
			"where 	a.hidelive = 0" & vbcrlf & _
			"	and	a.isTablet = 0" & vbcrlf & _
			"	and	c.subbrandid = '" & subBrandID & "'" & vbcrlf & _
			"	and	a.international <> 1" & vbcrlf & _
			"order by a.topModel desc, a.releaseyear desc, a.releasequarter desc"
	session("errorSQL") = sql			
	arrPhones = getDbRows(sql)
	
	if isnull(arrPhones) then response.redirect("/?ec=202")
	
	brandid = cint(arrPhones(0,0))
	brandName = arrPhones(8,0)
	subBrandName = arrPhones(10,0)
	
	response.Cookies("myBrand") = brandid
	response.Cookies("myModel") = 0	
	
	dim strBreadcrumb, headerImgAltText, strH1
	strH1 = brandName & " " & subBrandName & " Accessories"
	strBreadcrumb = brandName & " " & subBrandName & " Accessories"
	headerImgAltText = brandName & " " & subBrandName & " Accessories"
	
	SEtitle = brandName & " " & subBrandName & " Accessories: " & subBrandName & " Accessories, Covers, Cases & Batteries"
	SEdescription = "Looking for " & brandName & " " & subBrandName & " accessories? Cellphone Accents carries the best Galaxy Accessories like Covers, Cases, Batteries, Chargers!"
	SEkeywords = "galaxy s3 accessories, galaxy accessories, galaxy s accessories, galaxy s2 accessories, galaxy covers, galaxy cases"	
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<script src="/framework/userinterface/js/jquery/jquery-1.6.2.min.js"></script>    
<link rel="stylesheet" type="text/css" href="/includes/css/slides.css" />    
<script src="/framework/userinterface/js/slides/slides.min.jquery.js"></script>
<script>
window.WEDATA.pageType = 'subBrand';
window.WEDATA.pageData = {
	brand: '<%= brandName %>',
	brandId: '<%= brandId %>'
	subBrand: '<%= subBrandName %>',
	subBrandId: '<%= subBrandId %>'
};
</script>

<script>
	$(function(){
		$('#slides1').slides({preload: true, preloadImage: '/images/preloading.gif', generatePagination: true, /*play: 4000, pause: 3000, hoverPause: true,*/ prev: 'blue-arrow-left', next: 'blue-arrow-right'});
		$('#slides2').slides({preload: true, preloadImage: '/images/preloading.gif', generatePagination: true, /*play: 4000, pause: 3000, hoverPause: true,*/ prev: 'blue-arrow-left', next: 'blue-arrow-right'});
	});	
</script>
<style>
	a.carrier_header	{text-decoration:underline; }
	a.carrier_header_on	{text-decoration:none; color:#333; }
</style>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td style="border-bottom:1px solid #CCC;">
            <a class="top-sublink-gray" href="/">Cell Phone Accessories&nbsp;>&nbsp;</a>
            <span class="top-sublink-blue"><%=strBreadcrumb%></span>
        </td>
    </tr>
    <tr>
    	<td width="100%" align="left" valign="bottom" style="background:url(/images/brand-sub/<%=subBrandID%>/logo.jpg) no-repeat; width:822px; height:219px;">
        	<div style="width:450px; padding:0px 0px 30px 30px;"> 
            	<div style="color:#00aeed;">Accessories - Cases / Chargers / Holsters</div>
            	<div style="padding-top:5px; font-size:12px; color:#666;">Samsung Galaxy series has set and redefined the smartphone standard since day one. Enhance your experience with out rumble-ready Galaxy Accessories.</div>
			</div>
        </td>
    </tr>
    <tr>
    	<td style="padding:15px 0px 5px 0px;">
        	<div style="float:left;"><img src="/images/backgrounds/gray-bar-left.png" border="0" width="22" height="64" /></div>
            <div style="float:left; height:64px; width:765px; background-image:url(/images/backgrounds/gray-bar-background.png)">
            	<div style="float:left; margin-top:20px; padding-left:10px; font-size:24px; font-weight:normal;">Select Your&nbsp;<%=brandName%>&nbsp;<%=subBrandName%>&nbsp;Phone</div>
                <div style="float:right; margin-top:25px; padding-left:10px;">
                	<select name="selectModel" onChange="if(this.value != ''){window.location=this.value;}" style="width:250px; color:#333;">
                        <option value="">--Select from this list--</option>
                        <%
						for i=0 to ubound(arrPhones,2)
							modelID = arrPhones(1,i)
                            modelName = arrPhones(2,i)
							%>
                            <option value="<%="/m-" & modelID & "-" & formatSEO(brandName) & "-" & formatSEO(modelName) & "-cell-phone-accessories.html"%>"><%=modelName%></option>
                            <%
						next
                        %>
                    </select>
                </div>
            </div>
            <div style="float:left;"><img src="/images/backgrounds/gray-bar-right.png" border="0" width="22" height="64" /></div>
        </td>
    </tr>
	<tr>
    	<td style="padding:10px; border-top:1px solid #ccc; border-bottom:1px solid #ccc;">
        	<%
			sql	=	"select	distinct o.groupOrder" & vbcrlf & _
					"	,	convert(varchar(100), substring(	(	select	(',' + i.carrierCode)" & vbcrlf & _
					"												from	we_carriers i" & vbcrlf & _
					"												where	o.groupOrder = i.groupOrder" & vbcrlf & _
					"												order by i.groupOrder, i.id" & vbcrlf & _
					"												for xml path('')" & vbcrlf & _
					"											), 2, 1000)) carriercode" & vbcrlf & _
					"	,	convert(varchar(100), substring(	(	select	(',' + i.carriername)" & vbcrlf & _
					"												from	we_carriers i" & vbcrlf & _
					"												where	o.groupOrder = i.groupOrder" & vbcrlf & _
					"												order by i.groupOrder, i.id" & vbcrlf & _
					"												for xml path('')" & vbcrlf & _
					"											), 2, 1000)) carriername" & vbcrlf & _
					"from	we_carriers o" & vbcrlf & _
					"order by o.groupOrder"
			arrCarriers = getDbRows(sql)
			%>
        	<div style="float:left; font-weight:bold; font-size:11px; color:#333; margin-right:15px;">Sort By:</div>
            <%
			arrMaxLength = ubound(arrCarriers,2)
			for i=0 to arrMaxLength
				groupID = cint(arrCarriers(0,i))
				carriercode = arrCarriers(1,i)
				carrierName = arrCarriers(2,i)
				if groupID = 5 then carrierName = "Others"
			%>
            <div style="float:left; margin-right:15px;"><a href="javascript:togglePhones(<%=i%>,<%=(arrMaxLength+1)%>);" id="id_carrier_header_<%=i%>" class="carrier_header" title="<%=carrierName%>"><%=carrierName%></a></div>
            <div style="float:left; height:12px; width:0px; border-right:1px solid #ccc; margin:2px 15px 0px 0px;"></div>
			<%
			next
			%>
            <div style="float:left; font-weight:bold; font-size:11px; color:#333;">
            	<a href="javascript:togglePhones(<%=i%>,<%=(arrMaxLength+1)%>);" id="id_carrier_header_<%=i%>" class="carrier_header_on" title="View All <%=subBrandName%> Phones">View All&nbsp;<%=subBrandName%>&nbsp;Phones</a>
			</div>
        </td>
    </tr>
	<tr>
        <td>
        	<%
			for i=0 to ubound(arrCarriers,2)
				carriercode = arrCarriers(1,i)
			%>
            <div id="id_phonelist_<%=i%>" style="display:none;">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <%
                        lap = 0
                        for j = 0 to ubound(arrPhones,2)
							phoneCarriercode = arrPhones(4,j)
							if lookupPhoneCarrier(phoneCarriercode, carriercode) then
								lap = lap + 1
								modelid = arrPhones(1,j)
								modelName = arrPhones(2,j)
								modelpic = arrPhones(3,j)
								altText = brandName & " " & subBrandName & " Cell Phone Accessories: " & brandName & " " & modelName & " Accessories"
								modelLink = "/m-" & modelid & "-" & formatSEO(brandName) & "-" & formatSEO(modelName) & "-cell-phone-accessories.html"
                        %>
                        <td valign="top" style="<% if lap < 5 then %>border-right:1px solid #CCC;<% end if %> border-bottom:1px solid #ccc;">
                            <div style="width:160px; margin:10px 0px 10px 0px;">
                                <div style="margin-left:40px;">
                                    <a href="<%=modelLink%>" title="<%=altText%>">
                                        <img src="/modelPics/thumbs/<%=modelpic%>" border="0" width="70" height="112" alt="<%=altText%>" title="<%=altText%>">
                                    </a>
                                </div>
                                <div style="text-align:center; margin:5px; width:150px;">
                                    <a style="color:#2d5183; text-decoration:underline; font-size:12px;" href="<%=modelLink%>" title="<%=altText%>"><%=modelName%></a>
                                </div>
                            </div>
                        </td>
                        <%
								if lap >= 5 then
									response.write "</tr><tr>"
									lap = 0
								end if
							end if
                        next
                        %>
                    </tr>
                </table>
            </div>
            <%
			next
			%>
            <div id="id_phonelist_<%=i%>">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <%
                        lap = 0
                        for j = 0 to ubound(arrPhones,2)
							phoneCarriercode = arrPhones(4,j)

							lap = lap + 1
							modelid = arrPhones(1,j)
							modelName = arrPhones(2,j)
							modelpic = arrPhones(3,j)
							altText = brandName & " " & subBrandName & " Cell Phone Accessories: " & brandName & " " & modelName & " Accessories"
							modelLink = "/m-" & modelid & "-" & formatSEO(brandName) & "-" & formatSEO(modelName) & "-cell-phone-accessories.html"
                        %>
                        <td valign="top" style="<% if lap < 5 then %>border-right:1px solid #CCC;<% end if %> border-bottom:1px solid #ccc;">
                            <div style="width:160px; margin:10px 0px 10px 0px;">
                                <div style="margin-left:40px;">
                                    <a href="<%=modelLink%>" title="<%=altText%>">
                                        <img src="/modelPics/thumbs/<%=modelpic%>" border="0" width="70" height="112" alt="<%=altText%>" title="<%=altText%>">
                                    </a>
                                </div>
                                <div style="text-align:center; margin:5px; width:150px;">
                                    <a style="color:#2d5183; text-decoration:underline; font-size:12px;" href="<%=modelLink%>" title="<%=altText%>"><%=modelName%></a>
                                </div>
                            </div>
                        </td>
                        <%
							if lap >= 5 then
								response.write "</tr><tr>"
								lap = 0
							end if
                        next
                        %>
                    </tr>
                </table>
            </div>
        </td>
    </tr>    
    <% if brandID = 9 then %>
    <tr>
        <td align="center" style="padding-top:20px;">
            <div style="border:1px solid #ccc; background-color:#f2f2f2; border-radius:10px; padding:10px 0px 10px 0px; width:800px; color:#2d5183; font-weight:bold;">
            	Can't Find What You're Looking For? - <a href="/b-<%=brandid%>-<%=brandName%>-cell-phone-accessories.html" style="font-size:14px; text-decoration:underline;">View More&nbsp;<%=brandName%>&nbsp;Phones</a>
			</div>
        </td>
    </tr>
    <% end if %> 
    <tr>
    	<td align="left" valign="top" width="100%" style="padding-top:20px;">
			<div id="slides1" style="position:relative; z-index:1; width:810px; height:280px;">
            	<div id="slides1_header" style="background:url(/images/brand-sub/header-bg.jpg) repeat-x; width:810px; height:70px; font-size:24px; padding:20px 0px 0px 15px;">
                	New&nbsp;<%=brandName%>&nbsp;<%=subBrandName%>&nbsp;Accessories
                </div>
				<div class="blue-arrow-left" style="right:150px; top:25px;"></div>
                <div class="blue-arrow-right" style="right:10px; top:25px;"></div>
				<div id="new_accessories" class="slides_container" style="width:800px; padding:5px;">
                    <div class="slide" style="width:800px; height:220px;">
                        <%
                        sql = 	"select	top 30 x.itemid, x.itemdesc_co, x.itempic_co, x.price_retail, x.price_co" & vbcrlf & _
								"	,	rand(cast(cast(newid() as binary(8)) as int)) * x.itemid randomID" & vbcrlf & _
								"from	we_subtypes o with (nolock) cross apply" & vbcrlf & _
								"		(	" & vbcrlf & _
								"			select	top 2 b.itemid, b.itemdesc_co, b.itempic_co, b.price_retail, b.price_co" & vbcrlf & _
								"			from	we_models a join we_items b" & vbcrlf & _
								"				on	a.modelid = b.modelid" & vbcrlf & _
								"			where	a.hidelive = 0 and a.isTablet = 0 and a.subbrandid = '" & subbrandid & "' and a.international <> 1 " & vbcrlf & _
								"				and b.typeid <> 16 and b.hidelive = 0 and b.inv_qty <> 0 and b.price_co > 0 and (b.itempic_co <> '' and b.itempic_co is not null)" & vbcrlf & _
								"				and	o.subtypeid = b.subtypeid" & vbcrlf & _
								"			order by b.itemid desc		" & vbcrlf & _
								"		) x" & vbcrlf & _
								"order by randomID"
'						response.write "<pre>" & sql & "</pre>"
                        set rs = oConn.execute(sql)
                        a = 0
                        do until rs.eof
							itempic = rs("itempic_co")
							if fs.fileExists(server.MapPath("/productpics/thumb/" & itempic)) then
								a = a + 1
								itemid = rs("itemid")
								altText = rs("itemdesc_co")
								itemdesc = replace(rs("itemDesc_co"), "/", " / ")
								if len(itemdesc) > 48 then itemdesc = left(itemdesc, 45) & "..."
								price_retail = formatcurrency(rs("price_retail"),2)
								price_co = formatcurrency(rs("price_co"),2)
								productLink = "/p-" & itemid & "-" & formatSEO(itemdesc) & ".html"
                            %>
                            <div class="item" align="center" style="width:180px; height:220px; padding:0px 10px 0px 10px;">
                                <div style="width:180px; padding-top:2px;" align="center">
                                    <a href="<%=productLink%>" title="<%=altText%>"><div style="background: url(/productpics/thumb/<%=itemPic%>) no-repeat; width:100px; height:100px;" title="<%=altText%>"></div></a>
                                </div>
                                <div style="width:180px; padding:5px 0px 5px 0px;" align="center"><a href="<%=productLink%>" title="<%=altText%>" style="font-size:12px; color:#666; text-decoration:underline;"><%=itemdesc%></a></div>
                                <div style="width:180px; font-size:20px; font-weight:bold;" align="center"><%=price_co%></div>
                                <div style="width:180px; padding-top:5px; color:#2676BF; font-size:11px;" align="center">(<%=formatPercent((price_Retail - price_co) / price_Retail,0)%> OFF Retail Price: <del><%=price_retail%></del>)</div>
                            </div>
                            <%
								if ((a mod 4) = 0) and (a < 20) then
									response.write "</div>" & vbcrlf & "<div class=""slide"">"
								end if
								if a = 20 then exit do
							end if
                            rs.movenext
                        loop
                        %>
                    </div>
                </div>                
            </div>        
        </td>
    </tr>
    <tr>
    	<td align="left" valign="top" width="100%" style="padding-top:60px;">
			<div id="slides2" style="position:relative; z-index:1; width:810px; height:280px;">
            	<div id="slides2_header" style="background:url(/images/brand-sub/header-bg.jpg) repeat-x; width:810px; height:70px; font-size:24px; padding:20px 0px 0px 15px;">
                	Top Selling&nbsp;<%=brandName%>&nbsp;<%=subBrandName%>&nbsp;Accessories
                </div>
				<div class="blue-arrow-left" style="right:150px; top:25px;"></div>
                <div class="blue-arrow-right" style="right:10px; top:25px;"></div>
				<div id="topselling_accessories" class="slides_container" style="width:800px; padding:5px;">
                    <div class="slide" style="width:800px; height:220px;">
                        <%
                        sql = 	"select	top 20 x.itemid, x.itemdesc_co, x.itempic_co, x.price_retail, x.price_co" & vbcrlf & _
								"	,	rand(cast(cast(newid() as binary(8)) as int)) * x.itemid randomID" & vbcrlf & _
								"from	we_subtypes o with (nolock) cross apply" & vbcrlf & _
								"		(	" & vbcrlf & _
								"			select	top 2 b.itemid, b.itemdesc_co, b.itempic_co, b.price_retail, b.price_co, b.numberOfSales" & vbcrlf & _
								"			from	we_models a join we_items b" & vbcrlf & _
								"				on	a.modelid = b.modelid" & vbcrlf & _
								"			where	b.typeid <> 16 and a.hidelive = 0 and a.isTablet = 0 and a.subbrandid = '" & subbrandid & "' and a.international <> 1 " & vbcrlf & _
								"				and b.hidelive = 0 and b.inv_qty <> 0 and b.price_co > 0 and (b.itempic_co <> '' and b.itempic_co is not null)" & vbcrlf & _
								"				and	o.subtypeid = b.subtypeid" & vbcrlf & _
								"			order by b.numberOfSales desc		" & vbcrlf & _
								"		) x" & vbcrlf & _
								"order by randomID"
                        set rs = oConn.execute(sql)
                        a = 0
                        do until rs.eof
                            a = a + 1
                            itemid = rs("itemid")
                            altText = rs("itemdesc_co")
                            itemdesc = replace(rs("itemDesc_co"), "/", " / ")
                            if len(itemdesc) > 48 then itemdesc = left(itemdesc, 45) & "..."
                            price_retail = formatcurrency(rs("price_retail"),2)
                            price_co = formatcurrency(rs("price_co"),2)
                            productLink = "/p-" & itemid & "-" & formatSEO(itemdesc) & ".html"
                            itempic = rs("itempic_co")
                            %>
                            <div class="item" align="center" style="width:180px; height:220px; padding:0px 10px 0px 10px;">
                                <div style="width:180px; padding-top:2px;" align="center">
                                    <a href="<%=productLink%>" title="<%=altText%>"><div style="background: url(/productpics/thumb/<%=itemPic%>) no-repeat; width:100px; height:100px;" title="<%=altText%>"></div></a>
                                </div>
                                <div style="width:180px; padding:5px 0px 5px 0px;" align="center"><a href="<%=productLink%>" title="<%=altText%>" style="font-size:12px; color:#666; text-decoration:underline;"><%=itemdesc%></a></div>
                                <div style="width:180px; font-size:20px; font-weight:bold;" align="center"><%=price_co%></div>
                                <div style="width:180px; padding-top:5px; color:#2676BF; font-size:11px;" align="center">(<%=formatPercent((price_Retail - price_co) / price_Retail,0)%> OFF Retail Price: <del><%=price_retail%></del>)</div>
                            </div>
                            <%
                            if ((a mod 4) = 0) and (a < 20) then
                                response.write "</div>" & vbcrlf & "<div class=""slide"">"
                            end if
                            rs.movenext
                        loop
                        %>
                    </div>
                </div>                
            </div>        
        </td>
    </tr>
    <tr><td style="padding-top:40px;">&nbsp;</td></tr>
    <tr>
    	<td style="padding:0px 0px 10px 0px; border-bottom:1px solid #ccc;">
			<table border="0" cellspacing="10" cellpadding="0">
            	<tr>
                	<td rowspan="4" style="width:55%; border-right:1px solid #ccc; padding-right:5px;" align="left" valign="top">
                    	<div style="background:url(/images/brand-sub/header-bg2.jpg) repeat-x; width:100%; height:38px; padding-top:15px;" align="center"><h1 style="margin:0px; font-weight:bold; font-size:18px; color:#000;">Accessories for Samsung Galaxy Phones</h1></div>
                        <p style="font-size:12px; color:#666;">
                        	<%=seBottomText%>
                        </p>
                    </td>
                    <td style="width:45%;" align="center" valign="top">
                    	<div style="background:url(/images/brand-sub/header-bg2.jpg) repeat-x; width:100%; height:38px; padding-top:15px;">
	                        <h3 style="margin:0px; font-weight:bold; font-size:18px;">Top&nbsp;<%=brandName%>&nbsp;<%=subBrandName%>&nbsp;Phones</h3>
                        </div>
                    </td>
                </tr>
                <%
				sql	=	"select	top 3 a.modelid, a.modelname, a.modelimg, isnull(sum(c.quantity), 0) numRecentSales" & vbcrlf & _
						"from	we_models a join we_items b" & vbcrlf & _
						"	on	a.modelid = b.modelid left outer join we_orderdetails c" & vbcrlf & _
						"	on	b.itemid = c.itemid and c.entryDate >= dateadd(dd, -14, getdate())" & vbcrlf & _
						"where	a.hidelive = 0 and a.isTablet = 0 and a.subbrandid = 1 and a.international <> 1 " & vbcrlf & _
						"	and b.hidelive = 0 and b.inv_qty <> 0 and b.price_co > 0" & vbcrlf & _
						"group by a.modelid, a.modelname, a.modelimg" & vbcrlf & _
						"order by numRecentSales desc"
				set rsTopPhones = oConn.execute(sql)
				lap = 0
				do until rsTopPhones.eof
					lap = lap + 1
					modelid = rsTopPhones("modelid")
					modelname = rsTopPhones("modelname")
					modelpic = rsTopPhones("modelimg")
					link = "/m-" & modelid & "-" & formatSEO(brandName) & "-" & formatSEO(modelname) & "-cell-phone-accessories.html"
					altText = brandName & " " & modelname
				%>
            	<tr>
                    <td style="<%if lap<3 then%>border-bottom:1px solid #ccc;<%end if%>" align="left" valign="top">
						<table border="0" cellspacing="10" cellpadding="0">
                        	<tr>
                            	<td width="220" align="left" valign="top">
                                	<a href="<%=link%>" title="<%=altText%>" style="text-decoration:none;"><h3 style="margin:0px; font-weight:normal; font-size:16px; color:#000;"><%=brandName & " " & rsTopPhones("modelname")%></h3></a><br />
                                    <%
									sql = 	"select	top 3 b.typeid, b.typename_co, sum(a.numberOfSales) numSales" & vbcrlf & _
											"from	we_items a join we_types b" & vbcrlf & _
											"	on	a.typeid = b.typeid" & vbcrlf & _
											"where	a.modelid = '" & modelid & "'" & vbcrlf & _
											"group by b.typeid, b.typename_co" & vbcrlf & _
											"order by numSales desc"
									set rsTemp = oConn.execute(sql)
									do until rsTemp.eof
										strLink = "/sb-" & brandid & "-sm-" & modelid & "-sc-" & rsTemp("typeid") & "-" & formatSEO(brandName) & "-" & formatSEO(modelname) & "-" & rsTemp("typename_co") & ".html"
									%>
		                            <span style="font-size:9px;">>></span> <a href="<%=strLink%>" title="<%=rsTemp("typename_co") & " for " & altText%>" style="text-decoration:underline; font-weight:normal;"><%=rsTemp("typename_co")%></a><br>
                                    <%
										rsTemp.movenext
									loop
									%>
		                            <a href="<%=link%>" title="<%=altText%>"><img src="/images/brand-sub/button-more-acc.jpg" border="0" alt="<%=altText%>" title="<%=altText%>" style="margin-top:5px;" /></a>
								</td>
                                <td width="80" align="center">
									<a href="<%=link%>" title="<%=altText%>"><img src="/modelPics/thumbs/<%=modelpic%>" border="0" alt="<%=altText%>" title="<%=altText%>" /></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>                
                <%
					rsTopPhones.movenext
				loop
				%>
            </table>
        </td>
    </tr>
	<% if seBottomText <> "" then %>
<!--    <tr>
        <td colspan="4" align="left" class="static-content-font" style="padding:20px 0px 20px 0px;"><div style="border:1px solid #CCC; padding:10px;"><%=seBottomText%></div></td>
    </tr>
    -->
    <% end if %>
    <tr><td style="padding-top:10px;">&nbsp;</td></tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<script>
	function jumpto()
	{
		modelname = document.frmSearch.txtSearch.value;
		var x=document.frmSearch.selectModel;
		for (i=0;i<x.length;i++)
			if (x.options[i].text == modelname) window.location = x.options[i].value;
	}
	
	function togglePhones(show,maxLength)
	{
		for (i=0; i<=maxLength; i++) 
		{
			document.getElementById('id_phonelist_'+i).style.display = 'none';
			document.getElementById('id_carrier_header_'+i).className = 'carrier_header';
		}
		
		document.getElementById('id_phonelist_'+show).style.display = '';
		document.getElementById('id_carrier_header_'+show).className = 'carrier_header_on';
	}
</script>
<%
function lookupPhoneCarrier(pCarrierHeaders,pCarriercodes)
	ret = false
	if pCarriercodes <> "" and pCarrierHeaders <> "" then
		arrCarrierCodes = split(pCarriercodes, ",")
		for k=0 to ubound(arrCarrierCodes)
			if trim(arrCarrierCodes(k)) <> "" then
				if instr(pCarrierHeaders, trim(arrCarrierCodes(k))) > 0 then
					ret = true
					exit for
				end if
			end if
		next
	end if
	lookupPhoneCarrier = ret
end function
%>
<!--#include virtual="/includes/template/bottom_index.asp"-->
