<%
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

'TEST CUSTOMER PROCESS IN CHECKOUT
response.Cookies("checkoutPage") = 0
response.Cookies("checkoutPage").expires = now

Session.Contents.Remove("nvpResArray")
Session.Contents.Remove("token")
Session.Contents.Remove("currencyCodeType")
Session.Contents.Remove("paymentAmount")
Session.Contents.Remove("PaymentType")
Session.Contents.Remove("PayerID")


SEtitle = "Cheap Cell Phone Accessories - Cellular Phone Accessories - CellularOutfitter.com"
SEdescription = "Buy cheap Cell Phone Accessories at Wholesale Prices to the Public.  Find the latest Discount Cellular Phone Accessories for Motorola, LG, Nokia, Samsung phones and more.  Cellular Accessories at the Lowest Prices Online Guaranteed."
SEkeywords = "cheap cell phone accessories, cell phone accessories, discount cellular phone accessories, cellular phone accessories"
pageTitle = "basket2"
%>

<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/cart/includes/inc_makeNum.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<!--#include virtual="/includes/asp/inc_getMySession.asp"-->

<%
htmBasketRows = ""
nSubTotal = 0
EmptyBasket = false

dim removePromo : removePromo = prepInt(request.QueryString("removePromo"))
if removePromo = 1 then session("promocode") = null

' START basket grab
dim sItemName, sRetailPrice, sItemPrice, sItemPic, nProdIdCheck, nProdQuantity, nProdIdCount, nProdTotalQuantity
dim sWeight, strItemCheck, strQty, FreeProductInCart
dim SquaretradeItemName, SquaretradeItemPrice, SquaretradeItemCondition
nProdTotalQuantity = cint(0)
sWeight = 0
strItemCheck = ""
strQty = ""
FreeProductInCart = 0
call fOpenConn()
sql = 	"SELECT e.alwaysInStock, c.brandName, d.modelName, (select top 1 inv_qty from we_items where partNumber = b.partNumber order by master desc, inv_qty desc) as inv_qty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, " &_
		"B.itemDesc_CO, B.itemPic_CO, b.itemPic, B.price_CO, B.price_retail, B.Condition, B.itemWeight, B.NoDiscount FROM ShoppingCart A INNER JOIN we_items B ON A.itemID = " &_
		"B.itemID left join we_brands c on b.brandID = c.brandID left join we_models d on b.modelID = d.modelID left join we_pnDetails e on b.partNumber = e.partNumber WHERE A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)" &_
		" union " &_
		"SELECT cast('True' as bit) as alwaysInStock, '' as brandName, '' as modelName, 100 as inv_qty, A.itemID, A.qty, B.modelID, 3 as typeID, B.musicSkinsID, B.artist + ' ' + B.designName, B.image as itemPic_co, '' as itemPic, B.price_CO, " &_
		"B.msrp as price_retail, 1 as Condition, 1 as itemWeight, 1 as NoDiscount FROM ShoppingCart A INNER JOIN we_items_musicskins B ON A.itemID=B.id WHERE " &_
		"A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
do until RS.eof
	if isnull(rs("alwaysInStock")) then alwaysInStock = False else alwaysInStock = rs("alwaysInStock")
	brandName = RS("brandName")
	modelName = RS("modelName")
	noDiscount = RS("nodiscount")
	nProdIdCheck = RS("itemID")
	nProdQuantity = RS("qty")
	nProdTotalQuantity = nProdTotalQuantity + RS("qty")
	nProdPartNumber = RS("partNumber")
	partNumber = nProdPartNumber
	strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
	strQty = strQty & cStr(nProdQuantity)& ","  ' QTY string for each item, this is for buySafe Use
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		sItemName = insertDetails(RS("itemDesc_CO"))
		sItemPic = RS("itemPic_CO")
		baseItemPic = RS("itemPic")
		sItemPrice = RS("price_CO")
		sRetailPrice = RS("price_retail")
		'Free Bluetooth
		if FreeBluetooth = 1 then
			if cStr(nProdIdCheck) = FreeBluetoothItemID then
				FreeBluetoothPrice = sItemPrice
			end if
		end if
		'Free Product With Phone Purchase
		'if RS("typeID") = 16 then
		'	PhonePurchased = 1
		'	SquaretradeItemName = sItemName
		'	SquaretradeItemPrice = sItemPrice
		'	SquaretradeItemCondition = RS("Condition")
		'end if
		'Category Sale
		if CategorySale = 1 then
			if RS("typeID") = CategorySaleTypeID and RS("NoDiscount") = 0 then
				CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
			else
				SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
				SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_CO > 0)"
				SQL = SQL & " AND (R.typeid = '" & CategorySaleTypeID & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
				SQL = SQL & " AND I.NoDiscount = 0"
				dim RSrelated
				set RSrelated = Server.CreateObject("ADODB.Recordset")
				RSrelated.open SQL, oConn, 0, 1
				if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				RSrelated.close
				set RSrelated = nothing
			end if
		end if
		
		'Free Product Offer code added 2/1/2010 by MC
		if inStr(RS("PartNumber"),"scr-") > 0 then FreeProductInCart = 1
		'session("errorSQL") = "nItemTotal = " & nItemTotal & " | nProdIdCheck = " & nProdIdCheck & " | sItemPrice = " & sItemPrice & " | nProdQuantity = " & nProdQuantity
		
		if instr(RS("PartNumber"),"PHN-") < 1 and RS("nodiscount") = 0 and ucase(left(RS("PartNumber"), 2)) <> "BT" then 
			specialAmt = specialAmt + (sItemPrice * nProdQuantity)
		end if
		
		if rs("inv_qty") > 0 or alwaysInStock then
			nSubTotal = nSubTotal + (sItemPrice * nProdQuantity)
			nItemTotal = nItemTotal + (sItemPrice * nProdQuantity)
			sWeight = sWeight + (cDbl(RS("itemWeight")) * nProdQuantity)
			if nProdIdCheck => 1000000 then
				sql = "select preferredImg, image, defaultImg from we_items_musicSkins where id = " & nProdIdCheck
				session("errorSQL") = sql
				set rs = oConn.execute(sql)
				if not rs.EOF then
					preferredImg = rs("preferredImg")
					itempic_CO = rs("image")
					defaultImg = rs("defaultImg")
					if isnull(preferredImg) then																					
						if hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsLarge", itempic_CO) then
							sql = "update we_items_musicSkins set preferredImg = 1 where id = " & itemID
							session("errorSQL") = sql
							oConn.execute(sql)
							useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
						elseif hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault", defaultImg) then
							sql = "update we_items_musicSkins set preferredImg = 2 where id = " & itemID
							session("errorSQL") = sql
							oConn.execute(sql)
							useImg = "http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault/" & defaultImg
						else
							useImg = "/productPics/thumb/imagena.jpg"
						end if
					elseif preferredImg = 1 then
						useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
					elseif preferredImg = 2 then
						useImg = "/productpics/musicSkins/musicSkinsDefault/" & defaultImg
					else
						useImg = "/productPics/thumb/imagena.jpg"
					end if
				end if
			else
				if instr(nProdPartNumber,"DEC-SKN") > 0 then
					useImg = "/productpics/decalSkins/thumb/" & baseItemPic
				else
					useImg = "/productpics/icon/" & sItemPic
				end if
			end if
			htmBasketRows = htmBasketRows & fWriteBasketRow(nProdIdCheck,sItemName,sItemPrice,cStr(nProdQuantity),sRetailPrice,useImg)
			
			WEhtml2 = WEhtml2 & "<input type=""hidden"" name=""itemID_" & nProdIdCount & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""itemQty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
			PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & sItemName & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & nProdQuantity & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & sItemPrice & """>" & vbcrlf
		else
			sql = "delete from ShoppingCart where itemID = " & nProdIdCheck & " and (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
			session("errorSQL") = sql
			oConn.execute SQL
			htmBasketRows = htmBasketRows & fWriteErrorBasketRow(nProdIdCheck,sItemName,sItemPrice,cStr(nProdQuantity),sRetailPrice,sItemPic)
		end if
	end if
	RS.movenext
loop
RS.close
set RS = nothing
call fCloseConn()
' END basket grab

'GET COUPON VALUES
dim sPromoCode
if request.form("promo") <> "" then
	sPromoCode = SQLquote(request.form("promo"))
else
	sPromoCode = session("promocode")
end if

'sPromoCode = ""
'session("promocode") = ""

if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	call fOpenConn()
	SQL = "SELECT * FROM CO_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		'FreeItemPartNumber = RS("FreeItemPartNumber")
	else
		sPromoCode = ""
	end if
	call fCloseConn()
end if
' END GET COUPON VALUES

if sWeight / 16 < 1 then
	sWeight = 1
else
	sWeight = sWeight / 16
end if

if strItemCheck <> "" then strItemCheck = left(strItemCheck,len(strItemCheck)-1)

if nProdIdCount = 0 then
	htmBasketRows = htmBasketRows & "<tr><td colspan=""6""><i><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Basket is currently empty.<br><br></i></td></tr>" & vbcrlf
	EmptyBasket = true
end if
%>

<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/includes/template/top_cart.asp"-->

								<td width="100%" align="center" valign="top">
									<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
										<%
										if len(session("userMsg")) > 0 and not isnull(session("userMsg")) then
										%>
										<tr><td align="center" style="font-weight:bold; color:#C30; font-size:14px; padding:10px 0px 10px 0px;"><%=session("userMsg")%></td></tr>
										<%
											session("userMsg") = null
										end if
										%>
										<tr>
											<td width="100%" valign="top" align="center" style="padding-top:15px;">
                                            	<img src="/images/cart/110-banner.jpg" border="0" width="980" height="78" />
                                            </td>
										</tr>
										<tr>
											<td width="100%" valign="top" align="center" style="border-top:1px dotted #000; padding:10px 0px 5px 0px;">
                                            	<img src="/images/cart/lock.png" border="0" />&nbsp;
                                            	<img src="/images/cart/steps_cart_on.jpg" border="0" />&nbsp;
                                            	<img src="/images/cart/steps_arrow.jpg" border="0" />&nbsp;
                                            	<img src="/images/cart/steps_order_off.jpg" border="0" />&nbsp;
                                            	<img src="/images/cart/steps_arrow.jpg" border="0" />&nbsp;
                                            	<img src="/images/cart/steps_receipt.jpg" border="0" />
                                            </td>
										</tr>
                                        <tr>
											<td width="100%" valign="top" align="center" style="border:1px solid #999999;">
                                            	<!-- basket start -->
                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                	<tr bgcolor="#cccccc" height="25px">
                                                    	<td align="center" style="font-size: 12px; font-weight: bold; color:#333;" colspan="2" width="435">DESCRIPTION</td>
                                                    	<td align="center" style="font-size: 12px; font-weight: bold; color:#333;">RETAIL PRICE</td>
                                                    	<td align="center" style="font-size: 12px; font-weight: bold; color:#333;">YOUR PRICE</td>
                                                    	<td align="center" style="font-size: 12px; font-weight: bold; color:#333;">QUANTITY</td>
														<td align="center" style="font-size: 12px; font-weight: bold; color:#333;">SUBTOTAL</td>
                                                    </tr>
                                                    <%=htmBasketRows%>
										<%
										if not EmptyBasket then
										%>
													<%
													if sPromoCode <> "" then specialAmt = 0 end if
													if date > cdate("9/30/2011") then specialAmt = 0 end if

													spend2Get = 0
													if spend2Get = 1 then													
														if 30-specialAmt <= 0 then
															nSubTotal = nSubTotal - 5
															nProdIdCount = nProdIdCount + 1
															PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""0""><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""Discount""><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""1""><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""-5"">" & vbcrlf
													%>
                                                    <tr>
                                                        <td width="75" align="center">&nbsp;</td>
                                                        <td width="360" align="left" valign="middle" style="font-size:13px; font-weight:bold; padding:5px;">$5 Discount</td>
                                                        <td align="center" valign="middle" style="font-size:13px; font-weight:bold;">&nbsp;</td>
                                                        <td align="center" valign="middle" class="red-price-small">-$5.00</td>
                                                        <td align="center" valign="middle" style="font-size:13px; font-weight:bold;">1</td>
                                                        <td align="center" valign="middle" style="font-size:13px; font-weight:bold;">-$5.00</td>
                                                    </tr>
													<%
														else
													%>
													<tr bgcolor="#017cc2">
                                                    	<td align="center" colspan="6">
                                                        	<table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                            	<tr>
                                                                	<td align="center" style="font-weight:bold; color:#fff; padding:5px; font-size:20px;">Spend $<%=30-specialAmt%> more to take an extra $5 off your order</td>
                                                                </tr>
                                                                <tr>
                                                                	<td align="right" style="color:#fff; font-size:10px; padding-right:20px;">
                                                                        *Phones, Bluetooth Headsets, and OEM products are not eligible for the $5 Off Orders Over $30 Promotion<br />
                                                                        Promotions cannot be combined with any additional promotions or other discounts                                                                    
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
													</tr>
													<%
														end if
													end if														

													if spendThisMuch = 1 and prepStr(sPromoCode) = "" then
														if nSubTotal => stm_total then
															call fOpenConn()
															session("stmFreeItem") = 1
															sql = "select b.brandName, c.modelName, a.* from we_items a left join we_brands b on a.brandID = b.brandID left join we_models c on a.modelID = c.modelID where itemID = " & stm_freeItem
															set freeItemRS = Server.CreateObject("ADODB.Recordset")
															freeItemRS.open sql, oConn, 0, 1
															
															brandName = freeItemRS("brandName")
															modelName = freeItemRS("modelName")
															partNumber = freeItemRS("partNumber")
															itemDesc = insertDetails(freeItemRS("itemDesc"))
													%>
                                                    <tr style="height:60px;">
                                                        <td width="75" align="center" style="border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;"><a href="/p-<%=freeItemRS("itemID")%>-<%=formatSEO(itemDesc)%>.html"><img src="/productpics/icon/<%=freeItemRS("itemPic")%>" width="45" height="45" border="0"></a></td>
                                                        <td width="360" align="left" valign="middle" style="padding-left:10px; border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;"><a class="product-cart" href="/p-<%=freeItemRS("itemID")%>-<%=formatSEO(itemDesc)%>.html"><%=itemDesc%></a></td>
                                                        <td align="center" valign="middle" style="font-size:13px; font-weight:bold; border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;"><s><%=formatCurrency(freeItemRS("price_retail"))%></s></td>
                                                        <td align="center" valign="middle" class="red-price-small" style="border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;">FREE</td>
                                                        <td align="center" valign="middle" style="font-size:13px; font-weight:bold; border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;">1</td>
                                                        <td align="center" valign="middle" style="font-size:13px; font-weight:bold; border-bottom:1px solid #eaeaea;">FREE</td>
                                                    </tr>
													<%
															call fCloseConn()
														else
															session("stmFreeItem") = null
													%>
                                                    <tr>
                                                    	<td align="center" colspan="6">
                                                        	<table border="0" cellspacing="0" cellpadding="0">
                                                            	<tr>
                                                                	<td align="center" style="font-weight:bold; color:#999; padding:5px; font-size:18px;">Spend <span style="color:#017cc2;"><%=formatcurrency(30-nSubTotal,2)%></span> more to get a free screen protector</td>
                                                                </tr>
                                                                <tr>
                                                                	<td align="center" style="color:#999; font-size:10px; padding-bottom:3px;">
                                                                        *Promotions cannot be combined with any additional promotions or other discounts                                                                    
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
													</tr>
                                                    <%
														end if
													else
														session("stmFreeItem") = null
													end if  

													if isNumeric(nSubTotal) then
														strItemTotal = formatCurrency(nSubTotal)
													else
														strItemTotal = formatCurrency(0)
													end if

													if sPromoCode = "" then
													%>
                                                    <tr bgcolor="#e1e1e1">
                                                    	<td colspan="6" align="left" style="padding-left:5px; height:40px;">
															<form action="/cart/basket.asp" method="post" style="margin:0px;">
															<table border="0" width="100%" cellpadding="0" cellspacing="0" id="promoCodeBtn">
                                                            	<tr>
																	<td style="font-size:10px; cursor:pointer; color:#666;" align="left" onclick="showPromo();" nowrap="nowrap">Enter Promo Code</td>                                                                
                                                                </tr>
                                                            </table>
                                                        	<table id="promoCode" border="0" width="100%" cellpadding="0" cellspacing="0" style="display:none;">
                                                            	<tr>
                                                                	<td width="240px" class="cart-text" align="left" style="vertical-align:middle;">
                                                                        Enter Promotion code here (if applicable):
                                                                    </td>
                                                                    <td width="150px">
																		<input type="text" name="promo" value="<%=sPromoCode%>">
                                                                    </td>
                                                                	<td width="*" align="left">
																		<input type="image" onClick="this.submit();" src="/images/cart/button_submit.jpg" width="84" height="27" border="0" alt="Submit" align="absbottom">
                                                                    </td>
                                                                </tr>
                                                            </table>
															</form>
                                                        </td>
                                                    </tr>
													<%
													else
														session("promocode") = sPromoCode
														strItemTotal = formatCurrency(nSubTotal)
													%>
                                                    <tr bgcolor="#DDDDDD" height="40px">
                                                    	<td colspan="3" align="left" style="padding-left:5px;" class="font-size:13px; font-weight:bold;">
                                                        	Promotions:&nbsp;<span style="font-weight: normal;"><%=couponDesc%></span>
                                                            &nbsp;<a href="/cart/basket.asp?removePromo=1" style="font-weight:normal; color:#F00; font-size:11px;">(Remove coupon)</a>
                                                        </td>
                                                    	<td colspan="3" align="right" class="font-size:13px; font-weight:bold;" style="padding-right:20px;"><span class="cart-text">Discounted Subtotal</span><br /><%=strItemTotal%></td>
                                                    </tr>
													<%
													end if
													%>
                                                    <tr>
                                                        <td colspan="3" align="left" style="padding-left:10px;">
                                                        	<div style="float:left; padding:10px 15px 0px 0px;"><a href="/privacypolicy.html" target="_blank" style="text-decoration:none;"><img src="/images/cart/truste.jpg" border="0" width="141" height="45" /></a></div>
                                                        	<div style="float:left; padding:9px 15px 0px 0px;"><a href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=100097908&source=ctc" target="_blank" style="text-decoration:none;"><img src="/images/cart/bbb.jpg" border="0" width="114" height="46" /></a></div>
                                                        	<div style="float:left; padding:0px 15px 0px 0px;"><img src="/images/cart/google.jpg" border="0" width="62" height="63" /></div>
                                                        	<div style="float:left; padding:0px 15px 0px 0px;"><img src="/images/cart/5000.jpg" border="0" width="84" height="61" /></div>
                                                        </td>
                                                        <td colspan="3" align="right" style="padding:15px;">
                                                        	<%
															if discountTotal > 0.0 then
															%>
                                                            <div style="font-size: 20px; font-weight: bold; color:#900;">Discount:&nbsp;&nbsp;<%=formatcurrency(discountTotal)%></div>
                                                            <%
															end if
															%>
                                                        	<div style="font-size: 22px; font-weight: bold;">Item Total:&nbsp;&nbsp;<%=strItemTotal%></div>
                                                            <div style="font-size: 11px; font-weight: bold; padding-top:5px;">
																Enter your shipping zip:<br />
                                                                <form name="shippingZip" onsubmit="return CheckSubmit('CO');" style="margin:2px 0px 3px 0px;">
                                                                    <img src="/images/cart/zip_arrow.jpg" border="0" />
                                                                    <input type="text" size="10" maxlength="10" name="shipZip" value="">
                                                                </form>
                                                            </div>
														</td>
                                                    </tr>
                                        <%
										end if
										%>                                                    
												</table>
                                            </td>
                                        </tr>
                                        <tr>
                                        	<td width="100%" style="padding-top:15px;">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                	<tr>
                                                    	<td valign="top" align="left">
                                                            <table border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" align="left">
                                                                    	<img src="/images/cart/happy-customer-banner.jpg" border="0" width="192" height="51" />
                                                                    </td>
                                                                    <td valign="top" align="left">
                                                                    	<div>
	                                                                        <div style="float:left; padding-right:5px;"><img src="/images/cart/green-check.jpg" border="0" width="16" height="16" /></div>
	                                                                        <div style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#666;">SSL Encrypted For Safe & Secure Transactions</div>
                                                                        </div>
                                                                    	<div>
	                                                                        <div style="float:left; padding-right:5px;"><img src="/images/cart/green-check.jpg" border="0" width="16" height="16" /></div>
	                                                                        <div style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#666;">All Orders Ship Within 1 Business Day</div>
                                                                        </div>
                                                                    	<div>
	                                                                        <div style="float:left; padding-right:5px;"><img src="/images/cart/green-check.jpg" border="0" width="16" height="16" /></div>
	                                                                        <div style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#666;">Unconditional 1-Year Warranty</div>
                                                                        </div>
                                                                    </td>
																</tr>
                                                                <tr>
                                                                    <td valign="top" align="left" colspan="2">
                                                                    	<a href="/"><img src="/images/cart/cont-shopping-button.jpg" border="0" width="186" height="34" alt="continue shopping" title="continue shopping" /></a>
                                                                    </td>
																</tr>
															</table>
                                                        </td>
                                                        <td valign="top" align="right">
                                                            <table border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" align="right">
                                                                        <form name="CheckoutForm" method="post" action="/cart/basket.asp">
                                                                            <input type="hidden" name="promo" value="<%=sPromoCode%>">
                                                                            <input type="hidden" name="sWeight" value="<%=cStr(sWeight)%>">
                                                                            <input type="hidden" name="strItemCheck" value="<%=strItemCheck%>">
                                                                            <input type="hidden" name="shipZip" value="">
                                                                            <input type="hidden" name="buysafeamount" value="0">
                                                                            <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
                                                                            <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
                                                                            <%=WEhtml%>
                                                                            <input type="image" name="cartContinue" onClick="return CheckSubmit('CO');" src="/images/cart/checkout-button.jpg" width="389" height="57" border="0" alt="Continue Checkout">
                                                                        </form>
                                                                    </td>
																</tr>
                                                                <tr>
                                                                    <td valign="top" align="right">
                                                                        <div style="float:right; margin-left:15px; width:110px; height:11px; font-size:1px; border-bottom:1px solid #ccc;">&nbsp;</div>
                                                                        <div style="float:right; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; color:#333;">Or Checkout With</div>
                                                                        <div style="float:right; margin-right:15px; width:110px; height:11px; font-size:1px; border-bottom:1px solid #ccc;">&nbsp;</div>
                                                                    </td>
																</tr>
                                                                <tr>
                                                                    <td valign="top" align="right" style="padding-top:10px;">
                                                                        <form name="GoogleForm" method="POST" action="/cart/process/google/CartProcessing.asp">
                                                                            <input type="image" name="Checkout" onClick="return CheckSubmit('GC');" alt="Google Checkout" src="https://checkout.google.com/buttons/checkout.gif?merchant_id=468324454609889&w=168&h=44&style=white&variant=text&loc=en_US" height="44" width="168">
                                                                            <input type="hidden" name="promocode" value="<%=sPromoCode%>">
                                                                            <input type="hidden" name="analyticsdata" value="">
                                                                            <input type="hidden" name="shipZip" value="">
                                                                            <input type="hidden" name="sWeight" value="<%=cStr(sWeight)%>">
                                                                            <input type="hidden" name="buysafeamount" value="0">
                                                                            <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
                                                                            <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
                                                                            <%=WEhtml%>
                                                                        </form>
                                                                    </td>
																</tr>
                                                                <tr>
                                                                    <td valign="top" align="right" style="padding-top:10px;">
                                                                        <form name="PaypalForm" action="/cart/basket.asp" method="post">
                                                                            <% if session("stmFreeItem") = 1 then nProdIdCount = nProdIdCount + 1 %>
                                                                            <input type="hidden" name="promo" value="<%=sPromoCode%>">
                                                                            <input type="hidden" name="ItemsAndWeight" value="<%=cStr(nProdIdCount) & "|" & cStr(sWeight)%>">
                                                                            <input type="hidden" name="paymentAmount" value="<%=nSubTotal%>">
                                                                            <input type="hidden" name="shipZip" value="">
                                                                            <input type="hidden" name="buysafeamount" value="0">
                                                                            <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
                                                                            <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
                                                                            <% if session("stmFreeItem") = 1 then PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & stm_freeItem & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & stm_freeItemDesc & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""1""><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""0"">" & vbcrlf %>
                                                                            <%=PPhtml%>
                                                                            <input type="image" name="cartPaypal" onClick="return CheckSubmit('PP');" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif">
                                                                        </form>
                                                                    </td>
																</tr>
															</table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
									</table>
								</td>
<!--#include virtual="/includes/template/bottom_cart.asp"-->
<%
function fWriteBasketRow(nProdId,sItemName,sItemPrice,nQty,sRetailPrice,sitempic_CO)
	'this will write out the row for the basket
	dim sTemp
	sTemp	=	"<form name=""frmEditDelete" & nProdId & """ action=""/cart/item_edit.asp"" method=""post"" style=""margin:0px;"">" & vbcrlf & _
				"	<tr height=""70px"">" & vbcrlf & _
				"		<td width=""75"" align=""center"" style=""border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;""><a href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html""><img src=""" & sitempic_CO & """ width=""45"" height=""45"" border=""0""></a></td>" & vbcrlf & _
				"		<td align=""left"" valign=""middle"" style=""padding-left:10px; border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;""><a class=""product-cart"" href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html"">" & sItemName & "</a></td>" & vbcrlf & _
				"		<td align=""center"" valign=""middle"" style=""font-size:13px; font-weight:bold; border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;""><s>" & formatCurrency(cDbl(sRetailPrice)) & "</s></td>" & vbcrlf & _
				"		<td align=""center"" valign=""middle"" class=""red-price-small"" style=""border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;"">" & formatCurrency(cDbl(sItemPrice)) & "</td>" & vbcrlf & _
				"		<td align=""center"" valign=""middle"" style=""border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;"">" & vbcrlf & _
				"			<input type=""hidden"" name=""prodid"" value=""" & nProdId & """>" & vbcrlf & _
				"			<input type=""hidden"" name=""pagetype"" value=""edit"">" & vbcrlf & _
				"			<input type=""text"" name=""qty"" size=""1"" maxlength=""4"" value=""" & nQty & """><br />" & vbcrlf & _
				"			<a style=""font-size:12px;color:#000000; font-weight:normal;"" href=""javascript:EditDeleteItem('frmEditDelete" & nProdId & "','edit');""><b>Update</b></a> | " & vbcrlf & _
				"			<a style=""font-size:12px;color:#000000; font-weight:normal;"" href=""javascript:EditDeleteItem('frmEditDelete" & nProdId & "','delete');""><b>Delete</b></a>" & vbcrlf & _
				"		</td>" & vbcrlf & _
				"		<td align=""center"" valign=""middle"" style=""font-size:13px; font-weight:bold; border-bottom:1px solid #eaeaea;"">" & formatCurrency((cDbl(sItemPrice) * cDbl(nQty))) & "</td>" & vbcrlf & _
				"	</tr>" & vbcrlf & _
				"</form>" & vbcrlf
	
	fWriteBasketRow = sTemp
end function

function fWriteErrorBasketRow(nProdId,sItemName,sItemPrice,nQty,sRetailPrice,sitempic_CO)
	'this will write out the row for the basket
	dim sErrorTemp
	
	sErrorTemp	=	"<form name=""frmEditDelete" & nProdId & """ action=""/cart/item_edit.asp"" method=""post"" style=""margin:0px;"">" & vbcrlf & _
					"	<tr height=""70px"">" & vbcrlf & _
					"		<td width=""75"" align=""center""><a href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html""><img src=""/productpics/icon/" & sitempic_CO & """ width=""45"" height=""45"" border=""0""></a></td>" & vbcrlf & _
					"		<td align=""left"" valign=""middle"" style=""padding-left:10px;""><a class=""product-cart"" href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html"">" & sItemName & "</a></td>" & vbcrlf & _
					"		<td align=""center"" valign=""middle"" style=""font-size:13px; font-weight:bold;""><s>" & formatCurrency(cDbl(sRetailPrice)) & "</s></td>" & vbcrlf & _
					"		<td align=""center"" valign=""middle"" class=""red-price-small"">" & formatCurrency(cDbl(sItemPrice)) & "</td>" & vbcrlf & _
					"		<td align=""center"" valign=""middle"" style=""font-size:13px; font-weight:bold;"" colspan=""2"">This product is currently unavailable</td>" & vbcrlf & _
					"	</tr>" & vbcrlf & _
					"</form>" & vbcrlf
					
'	sErrorTemp = "<form name=""frmEditDelete" & nProdId & """ action=""/cart/item_edit.asp"" method=""post"">" & vbcrlf
'	sErrorTemp = sErrorTemp & "<tr><td width=""100%"" align=""center"" valign=""middle"" height=""100""><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr>" & vbcrlf
'	sErrorTemp = sErrorTemp & "<td width=""75"" align=""center""><a href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html""><img src=""/productpics/icon/" & sitempic_CO & """ width=""45"" height=""45"" border=""0""></a></td>" & vbcrlf
'	sErrorTemp = sErrorTemp & "<td width=""360"" align=""left"" valign=""top""><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0"">" & vbcrlf
'	sErrorTemp = sErrorTemp & "<tr><td valign=""top"" colspan=""2""><a class=""product-cart"" href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html"">" & sItemName & "</a></td></tr>" & vbcrlf
'	sErrorTemp = sErrorTemp & "<tr><td valign=""top"" width=""50%"" class=""cart-text"">Retail&nbsp;Price:&nbsp;&nbsp;<s>" & formatCurrency(cDbl(sRetailPrice)) & "</s></td><td valign=""top"" width=""50%"" class=""red-price-small"">Your&nbsp;Price:&nbsp;&nbsp;" & formatCurrency(cDbl(sItemPrice)) & "</td></tr>" & vbcrlf
'	sErrorTemp = sErrorTemp & "</table></td>" & vbcrlf
'	sErrorTemp = sErrorTemp & "<td width=""90"" align=""right"" valign=""top"" class=""cart-text"" colspan=""3"">" & vbcrlf
'	sErrorTemp = sErrorTemp & "This product is currently unavailable</td>" & vbcrlf
'	sErrorTemp = sErrorTemp & "</tr></table></td></tr>" & vbcrlf
'	sErrorTemp = sErrorTemp & "<tr><td bgcolor=""#CCCCCC""><img src=""/images/spacer.gif"" width=""700"" height=""1"" border=""0""></td></tr>" & vbcrlf
'	sErrorTemp = sErrorTemp & "</form>" & vbcrlf

	fWriteErrorBasketRow = sErrorTemp
end function

function hasImageOnRemote(pRemoteURL, pFileName)
	dim isExists : isExists = false
	dim oXMLHTTP : set oXMLHTTP = server.CreateObject("Msxml2.ServerXMLHTTP")
	dim strTemp : strTemp = pRemoteURL & "/" & pFileName
	
	oXMLHTTP.Open "HEAD", strTemp, False	'communicating without responseBody, only looks up file header
	oXMLHTTP.Send
	
	if (oXMLHTTP.readyState = "4") and (oXMLHTTP.status = "200") and (instr(oXMLHTTP.getResponseHeader("Content-Type"), "image") > 0) then
		isExists = true
	end if

	set oXMLHTTP = Nothing
	
	hasImageOnRemote = isExists
	
end function
%>

<script language="JavaScript">
function EditDeleteItem(nForm,thisAction) {
	var frmName = eval('document.' + nForm);
	if (isNaN(frmName.qty.value)) {
		alert("Please enter a valid number for Quantity.");
	} else {
		if (thisAction == 'delete') {
			frmName.action = "/cart/item_delete.asp";
			//frmName.action = "http://staging.cellularoutfitter.com/cart/item_delete.asp";
		} else {
			frmName.action = "/cart/item_edit.asp";
			//frmName.action = "http://staging.cellularoutfitter.com/cart/item_edit.asp";
		}
		frmName.pagetype.value = thisAction;
		frmName.submit();
	}
}
</script>

<script language="javascript">
	function showPromo() {
		document.getElementById("promoCodeBtn").style.display = 'none'
		document.getElementById("promoCode").style.display = ''
	}
	
function CheckSubmit(pymt) {
	bValid = true;
	CheckValidNEW(document.shippingZip.shipZip.value, "Your Shipping Zip is a required field!");
	if (bValid) {
		if (pymt == 'PP') {
			document.PaypalForm.shipZip.value = document.shippingZip.shipZip.value;
			document.PaypalForm.action = '/cart/process/PaypalSOAP/ReviewOrder.asp';
			//document.PaypalForm.action = '/cart/process/PaypalSOAP/ReviewOrder.asp';
			document.PaypalForm.submit();
		} else if (pymt == 'GC') {
			document.GoogleForm.shipZip.value = document.shippingZip.shipZip.value;
			document.GoogleForm.submit();
		} else {
			<%
			if crossSell <> 0 then
				if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
					response.write "document.CheckoutForm.action = '/cart/checkout.asp';"
				else
					response.write "document.CheckoutForm.action = 'https://www.cellularoutfitter.com/cart/checkout.asp';"
				end if
			else
				if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
					response.write "document.CheckoutForm.action = '/cart/checkout.asp';"
				else
					response.write "document.CheckoutForm.action = 'https://www.cellularoutfitter.com/cart/checkout.asp';"
				end if
			end if
			%>
			document.CheckoutForm.shipZip.value = document.shippingZip.shipZip.value;
			document.CheckoutForm.submit();
		}
	}
	return false;
}
</script>