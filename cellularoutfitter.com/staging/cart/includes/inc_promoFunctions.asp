<%
dim discountTotal

'Free Bluetooth
if FreeBluetooth = 1 and nSubTotal - FreeBluetoothPrice >= FreeBluetoothMin then
	discountTotal = FreeBluetoothPrice
	nSubTotal = round(nSubTotal - FreeBluetoothPrice,2)
	if discountTotal > 0 then NoMorePromos = 1
end if

'Category Sale
if CategorySale = 1 then
	discountTotal = round(CategorySaleTotal * CategorySalePercent,2)
	nSubTotal = round(nSubTotal - (CategorySaleTotal * CategorySalePercent),2)
	if discountTotal > 0 then NoMorePromos = 1
end if

'Same Model Sale
if SameModelSale = 1 then
	dim aCount, bCount, cCount, holdModelID, SameModelSaleDiscount, myItemPrices, myItemArray
	bCount = 0
	SameModelSaleDiscount = 0
	myItemPrices = ""
	call fOpenConn()
	SQL = "SELECT A.itemID, A.qty, B.modelID, left(isnull(B.PartNumber, ''),3) AS PartLeft3, B.price_CO FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID"
	SQL = SQL & " WHERE A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
	SQL = SQL & " AND left(B.PartNumber,3) IN ('" & replace(mid(SameModelTypeID,2,len(SameModelTypeID)-2),"|","','") & "')"
	SQL = SQL & " AND B.NoDiscount = 0"
	SQL = SQL & " ORDER BY B.modelID, B.price_CO"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		holdModelID = RS("modelID")
		do until RS.eof
			' SECTION COMMENTED OUT TO TEMPORARILY GET RID OF MODEL-SPECIFIC CRITERIA
			' UNCOMMENT THIS TO GET BACK TO NORMAL MODEL SALE
			'if RS("modelID") <> holdModelID then
			'	myItemArray = split(left(myItemPrices,len(myItemPrices) - 1),",")
			'	if uBound(myItemArray) > 0 then
			'		for cCount = 0 to int(bCount / 2) - 1
			'			SameModelSaleDiscount = SameModelSaleDiscount + (myItemArray(cCount) * SameModelSalePercent)
			'			'response.write "<p>SameModelSaleDiscount = " & SameModelSaleDiscount & " | " & myItemArray(cCount) & " | bCount = " & bCount & " | " & int(bCount / 2) - 1 & "</p>"
			'		next
			'	end if
			'	bCount = 0
			'	myItemPrices = ""
			'	holdModelID = RS("modelID")
			'end if
			for aCount = 1 to RS("qty")
				myItemPrices = myItemPrices & RS("price_CO") & ","
				bCount = bCount + 1
			next
			RS.movenext
		loop
		myItemArray = split(left(myItemPrices,len(myItemPrices) - 1),",")
		if uBound(myItemArray) > 0 then
			for cCount = 0 to int(bCount / 2) - 1
				SameModelSaleDiscount = SameModelSaleDiscount + (myItemArray(cCount) * SameModelSalePercent)
				'response.write "<p>SameModelSaleDiscount = " & SameModelSaleDiscount & " | " & myItemArray(cCount) & " | bCount = " & bCount & " | " & int(bCount / 2) - 1 & "</p>"
			next
		end if
	end if
	RS.close
	set RS = nothing
	call fCloseConn()
	discountTotal = SameModelSaleDiscount
	nSubTotal = round(nSubTotal - SameModelSaleDiscount,2)
	if discountTotal > 0 then NoMorePromos = 1
end if


if holidaySale = 1 then
	holidayDiscount = cdbl(0)
	call fOpenConn()
	SQL = "SELECT A.qty, left(isnull(B.PartNumber, ''),3) AS PartLeft3, B.price_CO, b.typeid FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID"
	SQL = SQL & " WHERE A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
	SQL = SQL & " AND B.NoDiscount = 0"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		do until RS.eof
			if instr(holidaySalePartNum(0), "|" & rs("PartLeft3") & "|") > 0 then	'covers
				holidayDiscount = holidayDiscount + ((rs("qty") * rs("price_co")) * holidaySalePercent(0))
			end if
			if instr(holidaySalePartNum(1), "|" & rs("PartLeft3") & "|") > 0 and rs("typeid") = 2 then	'chargers
				holidayDiscount = holidayDiscount + ((rs("qty") * rs("price_co")) * holidaySalePercent(1))
			end if
			RS.movenext
		loop
	end if
	RS.close
	set RS = nothing
	call fCloseConn()
	discountTotal = holidayDiscount
	nSubTotal = round(nSubTotal - holidayDiscount,2)
	if discountTotal > 0 then NoMorePromos = 1
end if

' free shipping
if discountTotal > 0 and freeshipping = 1 then
 
	if nSubTotal >= freeshippingMin then
		shipcost0 = "0" 
	end if
	
end if

' APPLY COUPON DISCOUNT
if couponid <> 0 then
	if NoMorePromos = 0 then
		if nSubTotal >= promoMin then
			call fOpenConn()
			SQL = "SELECT A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, B.itemDesc_CO, B.itemPic, B.HandsfreeType, B.price_CO, A.customCost, B.price_retail, B.itemWeight FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID"
			SQL = SQL & " WHERE A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
			SQL = SQL & " AND B.NoDiscount = 0"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 0, 1
			do until RS.eof
				nProdIdCheck = RS("itemID")
				nProdQuantity = RS("qty")
				usePrice = RS("price_CO")
				if not isnull(RS("customCost")) then usePrice = RS("customCost")
				if typeID <> "0" or BOGO = true then
					if cStr(RS("typeID")) = "16" then
						if inStr(couponDesc,"(*This coupon code cannot be applied to cell phone purchases)") = 0 then
							couponDesc = couponDesc & "<br>(*This coupon code cannot be applied to cell phone purchases)"
						end if
					else
						dim typeIDarray
						typeIDarray = split(typeID,",")
						for a = 0 to uBound(typeIDarray)
							if excludeBluetooth = true and RS("HandsfreeType") = 2 then
								if inStr(couponDesc,"(*Bluetooth products excluded)") = 0 then
									couponDesc = couponDesc & "<br>(*Bluetooth products excluded)"
								end if
							else
								if cStr(RS("typeID")) = cStr(typeIDarray(a)) or cStr(typeIDarray(a)) = "99" then
									if BOGO = true then
										discountTotal = discountTotal + (usePrice * int(nProdQuantity/2) * promoPercent)
										'Response.Write("<p>discountTotal 1: "&discountTotal)
									else
										discountTotal = discountTotal + (usePrice * nProdQuantity * promoPercent)
										'Response.Write("<p>discountTotal 2: "&discountTotal)
									end if
								else
									SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
									SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_CO > 0)"
									SQL = SQL & " AND (R.typeid = '" & cStr(typeIDarray(a)) & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
									SQL = SQL & " AND I.NoDiscount = 0"
									set RSrelated = Server.CreateObject("ADODB.Recordset")
									RSrelated.open SQL, oConn, 3, 3
									if not RSrelated.eof then
										if BOGO = true then
											discountTotal = discountTotal + (usePrice * int(nProdQuantity/2) * promoPercent)
											'Response.Write("<p>discountTotal 3: "&discountTotal)
										else
											discountTotal = discountTotal + (usePrice * nProdQuantity * promoPercent)
											'Response.Write("<p>discountTotal 4: "&discountTotal)
										end if
									end if
									RSrelated.close
									set RSrelated = nothing
								end if
							end if
							
						next
					end if
				'elseif not isNull(FreeItemPartNumber) then  ' there is no this filed in CA_coupon Table
				'	if FreeItemPartNumber = RS("PartNumber") and FreeProductWithPhonePrice = 0 and PhonePurchased = 1 then
				'		FreeProductWithPhonePrice = usePrice
				'	end if
				
				elseif cStr(RS("typeID")) = "16" then
					if inStr(couponDesc,"(*This coupon code cannot be applied to cell phone purchases 2)") = 0 then
						couponDesc = couponDesc & "<br>(*This coupon code cannot be applied to cell phone purchases)"
					end if
				else
					if excludeBluetooth = true and RS("HandsfreeType") = 2 then
						if inStr(couponDesc,"(*Bluetooth products excluded)") = 0 then
							couponDesc = couponDesc & "<br>(*Bluetooth products excluded)"
						end if
					else
						discountTotal = discountTotal + (promoPercent * (nProdQuantity * usePrice))
					end if
				end if
				RS.movenext
			loop
			RS.close
			set RS = nothing
			call fCloseConn()
			if FreeProductWithPhonePrice > 0 then discountTotal = FreeProductWithPhonePrice
			nSubTotal = round(nSubTotal - discountTotal,2)
			
			'Response.Write("<br>nSubTotal 6: "&nSubTotal)
		else
			couponDesc = "Coupon code cannot be applied to this order - minimum order value not reached."
		end if
	else
		couponDesc = "Coupon code cannot be applied to this order - another discount has already been applied."
	end if
end if

dim sGiftCert
if request.form("gift") <> "" then
	sGiftCert = SQLquote(request.form("gift"))
else
	sGiftCert = session("gift")
end if
if sGiftCert <> "" then
	'GIFT CERTIFICATE CODE SUBMITTED, CHECK IF IT EXISTS
	call fOpenConn()
	SQL = "SELECT * FROM GiftCertificates WHERE GiftCertificateCode = '" & sGiftCert & "' AND dateUsed IS NULL AND store = 2"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	dim nGiftCertAmount
	if not RS.eof then
		nGiftCertAmount = RS("amount")
	else
		nGiftCertAmount = 0
	end if
	if nSubTotal - nGiftCertAmount < 0 then nGiftCertAmount = nSubTotal
	call fCloseConn()
end if

discountTotal = round(discountTotal,2)
'Response.Write("discountTotal 7:"& discountTotal)
%>
