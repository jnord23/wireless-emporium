<%
dim vendororder, itemQS
on error resume next
vendororder = Request.Cookies("vendororder")
on error goto 0

'db-driven pixel [knguyen/20110520]
'todo: consolidate all hardcoded pixels into actual db rules
'dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
'dim dicContentEventText: set dicContentEventText = GetContentEventText( dicSeoAttributeInput)
'call OutputPixel( dicContentEventText) '--sends out the db-driven pixel

call printPixel(3)

if vendororder <> "" then
	allItems_br  = split(allItems,",")
	allQty_br  = split(allQty,",")
	allPrice_br  = split(allPrice,",")
	select case left(vendororder,7)
		case "shopzil"
			' Shopzilla
			response.write "<script language=""JavaScript"">" & vbcrlf
			response.write "<!--" & vbcrlf
			response.write "var mid            = '157952';" & vbcrlf
			response.write "var cust_type      = '1';" & vbcrlf
			response.write "var order_value    = " & nOrderGrandTotal & ";" & vbcrlf
			response.write "var order_id       = " & nOrderId & ";" & vbcrlf
			response.write "var units_ordered  = " & nTotQty & ";" & vbcrlf
			response.write "//-->" & vbcrlf
			response.write "</script>" & vbcrlf
			response.write "<script language=""javascript"" src=""https://www.shopzilla.com/css/roi_tracker.js""></script>" & vbcrlf
		case "shoppin"
			' Shopping.com
			response.write "<script language=""JavaScript"">" & vbcrlf
			response.write "var merchant_id = '429701'" & ";" & vbcrlf
			response.write "var order_id = " & nOrderId & ";" & vbcrlf
			response.write "var order_amt = " & nOrderGrandTotal & ";" & vbcrlf
			'response.write "//var category_id = 'PUT_YOUR_DATA_HERE'" & ";" & vbcrlf
			'response.write "//var category_name = 'PUT_YOUR_DATA_HERE'" & ";" & vbcrlf
			response.write "var product_id = '" & codes & "';" & vbcrlf
			response.write "var product_name = '" & items & "';" & vbcrlf
			response.write "</script>" & vbcrlf
			response.write "<script language=""JavaScript"" src=""https://stat.DealTime.com/ROI/ROI.js?mid=429701""></script>" & vbcrlf
		case "pricegr"
			'PriceGrabber.com CTS Item Detail Tracking
			response.write "<img src=""https://www.pricegrabber.com/conversion.php?retid=12199" & strPriceGrabber & """>" & vbcrlf
	end select
else
	'show the Fetchback code if there is no "vendororder" cookie value - added by MC 10/29/2010
	response.write "<iframe src='https://pixel.fetchback.com/serve/fb/pdc?cat=&name=success&sid=1241&crv=" & nOrderSubTotal & "&oid=" & nOrderId & "' scrolling='no' width='1' height='1' marginheight='0' marginwidth='0' frameborder='0'></iframe>" & vbcrlf
end if
%>

<!-- br start -->
<!--#include virtual="/includes/asp/pixels/inc_br.asp"-->
<!-- br end -->
<!-- granify start -->
<!--#include virtual="/includes/asp/pixels/inc_granify.asp"-->
<!-- granify end -->
<!-- channel advisor start -->
<!--#include virtual="/includes/asp/pixels/inc_channeladvisor.asp"-->
<!-- channel advisor end -->