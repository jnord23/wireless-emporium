<html>
<head>
	<title>Verification Number (CVV2) Help</title>
</head>
<body>
<table cellspacing="0" cellpadding="1" width="100%" bgColor="#000000" border="0">
	<tr>
		<td>
			<table cellspacing="1" cellpadding="1" width="100%" bgColor="#ffffff" border="0">
				<tr>
					<td class="siteNav5td" vAlign="top"><font class="contbold">What is CVV2 (or Security Code) ?</font></td>
				</tr>
				<tr>
					<td valign="top">
						<p align="Justify"><font class="contsmall">CVV2 is a security measure we require for all
						transactions. Since a CVV2 number is listed on your credit card, but is not
						stored anywhere, the only way to know the correct CVV2 number for your credit
						card is to physically have possession of the card itself. All VISA, MasterCard
						and American Express cards made in America in the past 5 years or so have a
						CVV2 number.</font></p>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="siteNav5td" valign="top">
						<font class="contbold">How to find your CVV2 (or Security Code) number?</font>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<p align="Justify"><font class="contsmall">On a Visa or MasterCard, please turn your card
						over and look in the signature strip. You will find (either the entire 16-digit
						string of your card number, OR just the last 4 digits), followed by a space,
						followed by a 3-digit number. That 3-digit number is your CVV2 number. (See
						below) On American Express Cards, the CVV2 number is a 4-digit number that
						appears above the end of your card number. (See below)</font></p>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td valign="top">
						<table cellspacing="1" cellpadding="3" border="0">
							<tr>
								<td class="siteNav5td" colspan="2"><font class="contbold">Visa &amp; MasterCard :</font></td>
							</tr>
							<tr>
								<td><img src="http://www.cellularoutfitter.com/images/cart/visa_cvv2.gif"></td>
								<td>
									<p align="Justify"><font class="contsmall">This number is printed on your MasterCard &amp; Visa cards in
									the signature area of the back of the card. (it is the last 3 digits AFTER the
									credit card number in the signature area of the card). IF YOU CANNOT READ YOUR
									CVV2 NUMBER, YOU WILL HAVE TO CONTACT THE ISSUING CREDITOR.</font></p>
								</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td class="siteNav5td" colspan="2"><font class="contbold">American Express :</font></td>
							</tr>
							<tr>
								<td><img src="http://www.cellularoutfitter.com/images/cart/amex_cvv2.gif"></td>
								<td>
									<p align="Justify"><font class="contsmall">American Express cards show the CVV2 printed above and to the
									right of the imprinted card number on the front of the card.</font></p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="top" align="middle">
						<font class="contbold"><a onClick="window.close(); return false;" href="">Close this window.</a></font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
