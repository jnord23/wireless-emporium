<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	'if instr(lcase(request.ServerVariables("HTTP_REFERER")),"://www.cellularoutfitter.com") < 1 and instr(lcase(request.ServerVariables("HTTP_REFERER")),"://staging.cellularoutfitter.com") < 1 then response.Redirect("/?ec=562")
	
	dim promoItem : promoItem = prepInt(request.Form("promoItemID")) 'set to 0 if no promo is active
	dim includePromo : includePromo = prepInt(request.Form("includePromo"))
	
	csid = request.QueryString("csid")
	if isnull(csid) or len(csid) < 1 or not isnumeric(csid) then csid = 0 else csid = cdbl(csid)
	
	if csid > 0 then
		response.Cookies("mySession") = csid
		response.cookies("mySession").expires = dateAdd("m", 1, now)
	end if
	
	dim mySession, myAccount
	mySession = request.cookies("mySession")
	myAccount = request.cookies("myAccount")
	if mySession = "" then
		mySession = session.sessionid
		response.cookies("mySession") = mySession
		response.cookies("mySession").expires = dateAdd("m", 1, now)
	end if
	
	'To be called for each item added to cart
	function AddItemToCart(strItemID, strQty, strItemPrice)
		if strQty < 1 then strQty = 1
		if strQty > 25500 then strQty = 25500
		if strItemID > 0 then
			if strItemID => 1000000 then
				SQL = "select cast('True' as bit) as alwaysInStock, 100 as inv_qty, b.id, b.itemID, b.qty from we_items_musicSkins a left join ShoppingCart b on a.id = b.itemID and sessionID = '" & mySession & "' and (purchasedOrderID IS NULL OR purchasedOrderID = 0) where a.id = " & strItemID
			else
				SQL = 	"select c.alwaysInStock, (select top 1 inv_qty from we_items where partNumber = a.partNumber and master = 1) as inv_qty, b.id, b.itemID, b.qty " &_
						"from we_items a " &_
							"left join ShoppingCart b on a.itemID = b.itemID and sessionID = '" & mySession & "' and b.lockQty = 0 and (purchasedOrderID IS NULL OR purchasedOrderID = 0) " &_
							"left join we_pnDetails c on a.partNumber = c.partNumber " &_
						"where a.itemID = " & strItemID & " and a.hideLive = 0"
			end if
			session("errorSQL") = SQL
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 0, 1
			
			if not RS.eof then
				if isnull(rs("alwaysInStock")) then alwaysInStock = False else alwaysInStock = rs("alwaysInStock")
				if rs("inv_qty") > 0 or alwaysInStock then
					if not isnull(rs("id")) then
						if not isnull(strItemPrice) then
							SQL = "UPDATE ShoppingCart SET qty = qty + " & strQty & ", customCost = " & strItemPrice & " WHERE id = '" & RS("id") & "'"
						else
							SQL = "UPDATE ShoppingCart SET qty = qty + " & strQty & ", customCost = null WHERE id = '" & RS("id") & "'"
						end if 
					else
						if isnull(strItemPrice) then strItemPrice = "null"
						
						SQL = "INSERT INTO ShoppingCart (store,sessionID,accountID,itemID,qty,dateEntd,customCost,ipAddress) VALUES (2,"
						SQL = SQL & "'" & mySession & "', "
						if myAccount <> "" then
							SQL = SQL & "'" & myAccount & "', "
						else
							SQL = SQL & "null, "
						end if
						SQL = SQL & "'" & strItemID & "', "
						SQL = SQL & "'" & strQty & "', "
						SQL = SQL & "'" & now & "', "
						SQL = SQL & strItemPrice & ",'" & request.ServerVariables("REMOTE_ADDR") & "')"		
					end if
				else
					sql = "delete from ShoppingCart where itemID = " & strItemID & " and (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
					session("userMsg") = "OUT OF STOCK NOTICE: THE SELECTED PRODUCT IS NOT CURRENTLY AVAILABLE"
				end if
				session("errorSQL") = sql
				oConn.execute SQL
				
				if includePromo = 1 then
					if instr(sql,"INSERT INTO ShoppingCart") > 0 and promoItem > 0 then
						sql =	"if (select inv_qty from we_Items where itemID = " & promoItem & ") > 0 " &_
									"if (select count(*) from shoppingCart where store = 2 and sessionID = '" & mySession & "' and itemID = " & promoItem & " and purchasedOrderID is null) = 0 " &_
										"insert into ShoppingCart " &_
											"(store,sessionID,itemID,qty,customCost,lockQty,ipAddress) " &_
											"values " &_
											"(2,'" & mySession & "'," & promoItem & ",1,0,1,'" & request.ServerVariables("REMOTE_ADDR") & "')"
						session("errorSQL") = sql
						oConn.execute SQL
					end if
				end if
			else
				session("userMsg") = "OUT OF STOCK NOTICE: THE SELECTED PRODUCT IS NOT CURRENTLY AVAILABLE"
			end if
			RS.close
			set RS = nothing
		end if
	
	end function

	dim itemID, qty, itemPrice, addsp_ItemID, addsp_ItemPrice, addsp_ChkBx
	'Main Product Prep
	itemID = prepInt(request("prodid"))
	qty = prepInt(request("qty"))
	itemPrice = request.form("itemPrice")
	addsp_ItemID = prepInt(request.form("addsp_prodid"))
	'Screen Protector Prep
	addsp_qty = 1 'Qty is hard-coded to 1, cause it's only a checkbox and qty can't be selected
	addsp_ItemPrice = ds(request.form("addsp_price"))
	addsp_ChkBx = prepStr(request.form("addsp_ChkBx"))
	
	'Catch empties or null values in form submission
	if isnull(itemPrice) or len(itemPrice) < 1 or not isnumeric(itemPrice) then itemPrice = null
	if isnull(addsp_ItemPrice) or len(addsp_ItemPrice) < 1 or not isnumeric(addsp_ItemPrice) then addsp_ItemPrice = null

	'Add the main product to cart
	AddItemToCart itemID, qty, itemPrice
	
	'Add the screen protector to cart, if chosen
	if addsp_ChkBx = "addsp_Checked" then 'Value passed from form's input type checkbox is addsp_Checked
		AddItemToCart addsp_ItemID, addsp_qty, addsp_ItemPrice
	end if
	
	'Now that products are added, show customer his/her shopping basket
	response.redirect "/cart/basket.html"
%>