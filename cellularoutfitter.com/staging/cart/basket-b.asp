<%
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

'TEST CUSTOMER PROCESS IN CHECKOUT
response.Cookies("checkoutPage") = 0
response.Cookies("checkoutPage").expires = now

Session.Contents.Remove("nvpResArray")
Session.Contents.Remove("token")
Session.Contents.Remove("currencyCodeType")
Session.Contents.Remove("paymentAmount")
Session.Contents.Remove("PaymentType")
Session.Contents.Remove("PayerID")


SEtitle = "Cheap Cell Phone Accessories - Cellular Phone Accessories - CellularOutfitter.com"
SEdescription = "Buy cheap Cell Phone Accessories at Wholesale Prices to the Public.  Find the latest Discount Cellular Phone Accessories for Motorola, LG, Nokia, Samsung phones and more.  Cellular Accessories at the Lowest Prices Online Guaranteed."
SEkeywords = "cheap cell phone accessories, cell phone accessories, discount cellular phone accessories, cellular phone accessories"
pageTitle = "basket2"
%>

<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/cart/includes/inc_makeNum.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<!--#include virtual="/includes/asp/inc_getMySession.asp"-->

<%
secureURL = ""
if instr(request.ServerVariables("SERVER_NAME"), "www.cellularoutfitter.com") > 0 then secureURL = "https://www.cellularoutfitter.com"

htmBasketRows = ""
htmBasketRows2 = ""
nSubTotal = 0
nRetailTotal = 0
EmptyBasket = false
gblShipoutDateDesc = "Leaves our California warehouse <span style=""font-weight:bold; font-style:italic;"">today</span> if ordered by 3pm PST."
shipoutCutoff = cdate(Date & " 03:00:00 PM")
if Now > shipoutCutoff then gblShipoutDateDesc = "Ships from our warehouse <br />within <span style=""font-weight:bold; font-style:italic;"">24 hours</span>."

dim removePromo : removePromo = prepInt(request.QueryString("removePromo"))
if removePromo = 1 then session("promocode") = null

' START basket grab
dim sItemName, sRetailPrice, sItemPrice, sItemPic, nProdIdCheck, nProdQuantity, nProdIdCount, nProdTotalQuantity
dim sWeight, strItemCheck, strQty, FreeProductInCart
dim SquaretradeItemName, SquaretradeItemPrice, SquaretradeItemCondition
nProdTotalQuantity = cint(0)
sWeight = 0
strItemCheck = ""
strQty = ""
FreeProductInCart = 0
call fOpenConn()
sql = 	"SELECT e.alwaysInStock, c.brandName, d.modelName, (select top 1 inv_qty from we_items where partNumber = b.partNumber order by master desc, inv_qty desc) as inv_qty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, " & vbcrlf & _
		"B.itemDesc_CO, B.itemPic_CO, b.itemPic, B.price_CO, B.price_retail, B.Condition, B.itemWeight, B.NoDiscount, nullif(a.customcost, 0) customcost FROM ShoppingCart A INNER JOIN we_items B ON A.itemID = " & vbcrlf & _
		"B.itemID left join we_brands c on b.brandID = c.brandID left join we_models d on b.modelID = d.modelID left join we_pnDetails e on b.partNumber = e.partNumber WHERE A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)" & vbcrlf & _
		" union " & vbcrlf & _
		"SELECT cast('True' as bit) as alwaysInStock, '' as brandName, '' as modelName, 100 as inv_qty, A.itemID, A.qty, B.modelID, 3 as typeID, B.musicSkinsID, B.artist + ' ' + B.designName, B.image as itemPic_co, '' as itemPic, B.price_CO, " & vbcrlf & _
		"B.msrp as price_retail, 1 as Condition, 1 as itemWeight, 1 as NoDiscount, nullif(a.customcost, 0) customcost FROM ShoppingCart A INNER JOIN we_items_musicskins B ON A.itemID=B.id WHERE " & vbcrlf & _
		"A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
do until RS.eof
	if isnull(rs("alwaysInStock")) then alwaysInStock = False else alwaysInStock = rs("alwaysInStock")
	brandName = RS("brandName")
	modelName = RS("modelName")
	noDiscount = RS("nodiscount")
	nProdIdCheck = RS("itemID")
	nProdQuantity = RS("qty")
	nProdTotalQuantity = nProdTotalQuantity + RS("qty")
	nTypeID = prepInt(RS("typeID"))
	nProdPartNumber = RS("partNumber")
	partNumber = nProdPartNumber
	strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
	strQty = strQty & cStr(nProdQuantity)& ","  ' QTY string for each item, this is for buySafe Use
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		sItemName = insertDetails(RS("itemDesc_CO"))
		sItemPic = RS("itemPic_CO")
		baseItemPic = RS("itemPic")
		sItemPrice = RS("price_CO")
		if not isnull(rs("customCost")) then sItemPrice = rs("customCost")
		sRetailPrice = RS("price_retail")
		'Free Bluetooth
		if FreeBluetooth = 1 then
			if cStr(nProdIdCheck) = FreeBluetoothItemID then
				FreeBluetoothPrice = sItemPrice
			end if
		end if
		'Free Product With Phone Purchase
		'if nTypeID = 16 then
		'	PhonePurchased = 1
		'	SquaretradeItemName = sItemName
		'	SquaretradeItemPrice = sItemPrice
		'	SquaretradeItemCondition = RS("Condition")
		'end if
		'Category Sale
		if CategorySale = 1 then
			if nTypeID = CategorySaleTypeID and RS("NoDiscount") = 0 then
				CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
			else
				SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
				SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_CO > 0)"
				SQL = SQL & " AND (R.typeid = '" & CategorySaleTypeID & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
				SQL = SQL & " AND I.NoDiscount = 0"
				dim RSrelated
				set RSrelated = Server.CreateObject("ADODB.Recordset")
				RSrelated.open SQL, oConn, 0, 1
				if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				RSrelated.close
				set RSrelated = nothing
			end if
		end if
		'Free Product Offer code added 2/1/2010 by MC
		if inStr(RS("PartNumber"),"scr-") > 0 then FreeProductInCart = 1
		'session("errorSQL") = "nItemTotal = " & nItemTotal & " | nProdIdCheck = " & nProdIdCheck & " | sItemPrice = " & sItemPrice & " | nProdQuantity = " & nProdQuantity
		
		if instr(RS("PartNumber"),"PHN-") < 1 and RS("nodiscount") = 0 and ucase(left(RS("PartNumber"), 2)) <> "BT" then 
			specialAmt = specialAmt + (sItemPrice * nProdQuantity)
		end if
		
		if rs("inv_qty") > 0 or alwaysInStock then
			nSubTotal = nSubTotal + (sItemPrice * nProdQuantity)
			nItemTotal = nItemTotal + (sItemPrice * nProdQuantity)
			nRetailTotal = nRetailTotal + (sRetailPrice * nProdQuantity)
			sWeight = sWeight + (cDbl(RS("itemWeight")) * nProdQuantity)
			if nProdIdCheck => 1000000 then
				sql = "select preferredImg, image, defaultImg from we_items_musicSkins where id = " & nProdIdCheck
				session("errorSQL") = sql
				set rs = oConn.execute(sql)
				if not rs.EOF then
					preferredImg = rs("preferredImg")
					itempic_CO = rs("image")
					defaultImg = rs("defaultImg")
					if isnull(preferredImg) then																					
						if hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsLarge", itempic_CO) then
							sql = "update we_items_musicSkins set preferredImg = 1 where id = " & itemID
							session("errorSQL") = sql
							oConn.execute(sql)
							useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
						elseif hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault", defaultImg) then
							sql = "update we_items_musicSkins set preferredImg = 2 where id = " & itemID
							session("errorSQL") = sql
							oConn.execute(sql)
							useImg = "http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault/" & defaultImg
						else
							useImg = "/productPics/thumb/imagena.jpg"
						end if
					elseif preferredImg = 1 then
						useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
					elseif preferredImg = 2 then
						useImg = "/productpics/musicSkins/musicSkinsDefault/" & defaultImg
					else
						useImg = "/productPics/thumb/imagena.jpg"
					end if
				end if
			else
				if instr(nProdPartNumber,"DEC-SKN") > 0 then
					useImg = "/productpics/decalSkins/thumb/" & baseItemPic
				else
'					useImg = "/productpics/icon/" & sItemPic
					useImg = "/productpics/thumb/" & sItemPic
				end if
			end if
'			htmBasketRows = htmBasketRows & fWriteBasketRow(nProdIdCheck,sItemName,sItemPrice,cStr(nProdQuantity),sRetailPrice,useImg)
			htmBasketRows2 = htmBasketRows2 & fWriteBasketRow2(nProdIdCheck,sItemName,sItemPrice,cStr(nProdQuantity),sRetailPrice,useImg,nTypeID)			
			
			WEhtml2 = WEhtml2 & "<input type=""hidden"" name=""itemID_" & nProdIdCount & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""itemQty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
			PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & sItemName & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & nProdQuantity & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & sItemPrice & """>" & vbcrlf
		else
			sql = "delete from ShoppingCart where itemID = " & nProdIdCheck & " and (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
			session("errorSQL") = sql
			oConn.execute SQL
			htmBasketRows = htmBasketRows & fWriteErrorBasketRow(nProdIdCheck,sItemName,sItemPrice,cStr(nProdQuantity),sRetailPrice,sItemPic)
		end if
	end if
	RS.movenext
loop
RS.close
set RS = nothing
call fCloseConn()
' END basket grab

'GET COUPON VALUES
dim sPromoCode
if request.form("promo") <> "" then
	sPromoCode = SQLquote(request.form("promo"))
else
	sPromoCode = session("promocode")
end if

'sPromoCode = ""
'session("promocode") = ""

if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	call fOpenConn()
	SQL = "SELECT * FROM CO_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		'FreeItemPartNumber = RS("FreeItemPartNumber")
	else
		sPromoCode = ""
	end if
	call fCloseConn()
end if
' END GET COUPON VALUES

if sWeight / 16 < 1 then
	sWeight = 1
else
	sWeight = sWeight / 16
end if

if strItemCheck <> "" then strItemCheck = left(strItemCheck,len(strItemCheck)-1)

if nProdIdCount = 0 then
	htmBasketRows = htmBasketRows & "<tr><td colspan=""6""><i><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Basket is currently empty.<br><br></i></td></tr>" & vbcrlf
	htmBasketRows2 = htmBasketRows2 & "<div style=""width:100%; float:left; padding:25px 0px 25px 0px; text-align:center;"">Basket is currently empty.</div>" & vbcrlf	
	EmptyBasket = true
end if
%>

<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/includes/template/top_cart.asp"-->
<link href="/includes/css/mvt/basket/slides.css" rel="stylesheet" type="text/css">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="/framework/userinterface/js/slides/slides.min.jquery.js"></script>
<link href="/includes/css/mvt/basket/doublecheckout1.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt/basket/instock1.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt/basket/lineDiscount1.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt/basket/totalDiscount1.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt/basket/bottomSlides1.css" rel="stylesheet" type="text/css">
<link href="/includes/css/mvt/basket/valueProps1.css" rel="stylesheet" type="text/css">
<td width="100%" align="center" valign="top">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
        <%
        if len(session("userMsg")) > 0 and not isnull(session("userMsg")) then
        %>
        <tr><td align="center" style="font-weight:bold; color:#C30; font-size:14px; padding:10px 0px 10px 0px;"><%=session("userMsg")%></td></tr>
        <%
            session("userMsg") = null
        end if
        %>
        <tr>
            <td width="100%" valign="top" align="center" style="padding-top:15px;">
                <img src="/images/cart/110-banner.jpg" border="0" width="980" height="78" usemap="#secureBanner" />
                <map name="secureBanner">
                    <area shape="rect" coords="0,0,562,78" href="javascript:openLowPricePopup();" alt="110% LOW PRICE GUARANTEE">
                    <area shape="rect" coords="563,0,980,78" href="javascript:openReturnPopup();" alt="90-DAY RETURN POLICY">
                </map>
            </td>
        </tr>
        <tr>
            <td width="100%" valign="top" align="center" style="border-top:1px dotted #000; border-bottom:1px dotted #000; padding:3px 0px 3px 0px;">
				<img src="/images/cart/step-1.jpg" />
            </td>
        </tr>
        <tr>
            <td width="100%" valign="top" align="left" style="padding:15px 0px 15px 0px;">
            	<div style="float:left; font-size:26px; color:#333; padding-top:10px;">
                	You have <span style="font-style:italic; color:#3a983a; font-weight:bold;">
                    <%if nProdTotalQuantity = 1 then%> 
	                    1 item
                    <%else%>
                    	<%=nProdTotalQuantity%> items
                    <%end if%>
                    </span> in your cart.
                </div>
            	<div id="mvt_doublecheckout" style="float:right;">
                    <form name="CheckoutForm" method="post" action="<%=secureURL%>/cart/checkout.asp">
                        <input type="hidden" name="promo" value="<%=sPromoCode%>">
                        <input type="hidden" name="sWeight" value="<%=cStr(sWeight)%>">
                        <input type="hidden" name="strItemCheck" value="<%=strItemCheck%>">
                        <input type="hidden" name="shipZip" value="">
                        <input type="hidden" name="buysafeamount" value="0">
                        <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
                        <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
                        <%=WEhtml%>
                        <input type="image" name="cartContinue" src="/images/basket/cta.png" border="0" alt="Continue Checkout">
                    </form>
                </div>
            </td>
        </tr>
        <tr>
        	<td width="100%" valign="top" align="center" id="mvt_cartNew" style="padding-bottom:10px;">
            	<div style="float:left; width:100%; background:url(/images/cart/headerbar-blue.jpg) repeat-x; height:23px; font-size:12px; color:#fff; padding-top:5px; border-top-left-radius:5px; border-top-right-radius:5px;">
                	<div style="float:left; width:120px; text-align:center; font-weight:bold; font-size:15px;">Item</div>
                	<div style="float:left; width:320px; text-align:left; font-weight:bold; font-size:15px;">Product Description</div>
                	<div style="float:left; width:220px; text-align:center; font-weight:bold; font-size:15px;">Shipping & Availability</div>
                	<div style="float:left; width:70px; text-align:center; font-weight:bold; font-size:15px;">Qty</div>
                	<div style="float:left; width:130px; text-align:center; font-weight:bold; font-size:15px;">Item Price</div>
                	<div style="float:left; width:100px; text-align:center; font-weight:bold; font-size:15px;">Total</div>
                </div>
                <div style="float:left; width:978px; border-left:1px solid #ccc; border-right:1px solid #ccc;">
				<%=htmBasketRows2%>
				<%
				if not EmptyBasket then
				    if isNumeric(nSubTotal) then
                        strItemTotal = formatCurrency(nSubTotal)
                    else
                        strItemTotal = formatCurrency(0)
                    end if
					
					if sPromoCode <> "" then
                        session("promocode") = sPromoCode
                        strItemTotal = formatCurrency(nSubTotal)					
					%>
                    <div style="width:978px; float:left; padding:5px;">
                        <div style="width:968px; float:left; padding:20px 0px 20px 0px;">
                            <div style="float:left; width:548px; text-align:left; padding-left:20px; color:#333;">
                                <b>Promotions:&nbsp;</b><%=couponDesc%>&nbsp;<a href="/cart/basket-b.asp?removePromo=1" style="color:#F00; font-size:11px;">(Remove coupon)</a>
                            </div>
                            <div style="float:left; width:380px; padding-right:20px; text-align:right;">
                                <div style="float:left; width:180px; text-align:right; color:#900; font-size:18px;">
                                    Discount:
                                </div>
                                <div style="float:left; width:200px; text-align:right; color:#900; font-size:18px;">
									<%=formatcurrency(discountTotal)%>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div style="width:978px; float:left; border-bottom:1px solid #ccc;"></div>
	                <%
					end if
                end if
				%>
                </div>
                
				<%if not EmptyBasket then%>
                <div style="float:left; width:978px; background-color:#f2f2f2; border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc;">
                	<div style="float:left; width:537px; padding:20px;">
                        <form action="/cart/basket-b.asp" method="post" style="margin:0px;">
                        <div id="promoCodeBtn2" style="float:left; font-size:12px; cursor:pointer; color:#666; text-align:left; padding-top:6px;" onclick="showPromo2();">Have a coupon code?</div>
                        <div id="promoCode2" style="float:left; display:none;">
                            <div style="float:left; padding-top:6px;" class="cart-text">Enter Promotion code here (if applicable):</div>
                            <div style="float:left; padding:3px 0px 0px 5px;"><input type="text" name="promo" value="<%=sPromoCode%>"></div>
                            <div style="float:left; padding:0px 0px 0px 5px;"><input type="image" onClick="this.submit();" src="/images/cart/button_submit.jpg" width="84" height="27" border="0" alt="Submit" align="absbottom"></div>
                        </div>
                        </form>                    
                    </div>
                	<div style="float:left; width:400px; background-color:#fff; border-left:1px solid #ccc;">
                    	<div style="float:left; width:100%; padding:15px 0px 15px 0px;">
                            <div style="float:left; width:180px; text-align:right; color:#333; font-size:18px;">
                            	Subtotal:
                            </div>
                            <div style="float:left; width:200px; text-align:right; color:#333; font-size:18px; padding:0px 20px 0px 0px;">
                            	<%=strItemTotal%>
                            </div>
                        </div>
                    	<div style="float:left; width:100%; background-color:#333; padding:10px 0px 10px 0px;">
                            <div style="float:left; width:180px; text-align:right; color:#fff; font-size:20px; font-weight:bold;">
                            	Order Total:
                            </div>
                            <div style="float:left; width:200px; text-align:right; color:#fff; font-size:20px; font-weight:bold; padding:0px 20px 0px 0px;">
                            	<%=strItemTotal%><br />
                                <span id="mvt_totalDiscount" style="font-size:12px; font-style:italic; font-weight:normal;">Total Savings: <%=formatcurrency(nRetailTotal-strItemTotal)%></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="float:left; width:100%; border-bottom:1px solid #ccc;">
	                <div style="float:left; width:570px;">
                        <div id="mvt_valueProp1" style="float:left; width:370px; margin:20px 0px 0px 80px; border:1px solid #ccc; border-radius:5px; padding:10px;">
                        	<div style="float:left; width:300px;">
                                <div style="float:left; width:100%; padding-top:5px;">
                                    <div style="float:left; padding:3px 10px 0px 0px;"><img src="/images/basket/mini-checkmark.png" border="0" /></div>
                                    <div style="float:left; color:#333; font-weight:bold; font-size:14px;">Over 2 Million Satisfied Customers</div>
                                </div>
                                <div style="float:left; width:100%; padding-top:5px;">
                                    <div style="float:left; padding:3px 10px 0px 0px;"><img src="/images/basket/mini-checkmark.png" border="0" /></div>
                                    <div style="float:left; color:#333; font-weight:bold; font-size:14px;">All Orders Ship Within 24 Hours</div>
                                </div>
                                <div style="float:left; width:100%; padding-top:5px;">
                                    <div style="float:left; padding:3px 10px 0px 0px;"><img src="/images/basket/mini-checkmark.png" border="0" /></div>
                                    <div style="float:left; color:#333; font-weight:bold; font-size:14px;">Unconditional 1-Year Warranty</div>
                                </div>
                                <div style="float:left; width:100%; padding-top:5px;">
                                    <div style="float:left; padding:3px 10px 0px 0px;"><img src="/images/basket/mini-checkmark.png" border="0" /></div>
                                    <div style="float:left; color:#333; font-weight:bold; font-size:14px;">Safe & Secure 100% SSL Encyption</div>
                                </div>
                            </div>
                        	<div style="float:left; width:70px; padding-top:10px;">
                                <div style="float:left; width:100%;">
                                    <!-- START SCANALERT CODE -->
                                    <a target="_blank" href="https://www.mcafeesecure.com/RatingVerify?ref=www.cellularoutfitter.com"><img width="65" height="37" border="0" src="//images.scanalert.com/meter/www.cellularoutfitter.com/63.gif" alt="McAfee Secure sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="alert('Copying Prohibited by Law - McAfee Secure is a Trademark of McAfee, Inc.'); return false;"></a>
                                    <!-- END SCANALERT CODE -->                                    
                                </div>
                                <div style="float:left; width:100%;">
                                    <a href="javascript:Open_Popup('https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.CELLULAROUTFITTER.COM&lang=en','VeriSign','width=560,height=500,location=yes,status=yes,resizable=yes,scrollbars=yes');">
                                        <div id="verisignLogo" style="float:right;" title="CELLULAR OUTFITTER, INC. has been verified by Verisign"></div>
                                    </a>
                                </div>
                            </div>
                        </div>
						<div id="mvt_valueProp2" style="float:left; width:100%; padding-top:20px;">
                        	<div style="float:left; width:200px;"><img src="/images/cart/happy-customer-banner.jpg" border="0" width="192" height="51" /></div>
                        	<div style="float:left; width:350px; padding-left:20px;">
								<div style="float:left; width:100%;">
                                    <div style="float:left; padding-right:5px;"><img src="/images/cart/green-check.jpg" border="0" width="16" height="16" /></div>
                                    <div style="float:left; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#666;">SSL Encrypted For Safe & Secure Transactions</div>
                                </div>
                                <div style="float:left; width:100%;">
                                    <div style="float:left; padding-right:5px;"><img src="/images/cart/green-check.jpg" border="0" width="16" height="16" /></div>
                                    <div style="float:left; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#666;">All Orders Ship Within 1 Business Day</div>
                                </div>
                                <div style="float:left; width:100%;">
                                    <div style="float:left; padding-right:5px;"><img src="/images/cart/green-check.jpg" border="0" width="16" height="16" /></div>
                                    <div style="float:left; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#666;">Unconditional 1-Year Warranty</div>
                                </div>
                            </div>
                        </div>                        
                    </div>
	                <div style="float:left; width:400px; padding:20px 10px 20px 0px;">
                    	<div style="float:left; width:100%; text-align:right;">
                            <form name="CheckoutForm" method="post" action="<%=secureURL%>/cart/checkout.asp">
                                <input type="hidden" name="promo" value="<%=sPromoCode%>">
                                <input type="hidden" name="sWeight" value="<%=cStr(sWeight)%>">
                                <input type="hidden" name="strItemCheck" value="<%=strItemCheck%>">
                                <input type="hidden" name="shipZip" value="">
                                <input type="hidden" name="buysafeamount" value="0">
                                <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
                                <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
                                <%=WEhtml%>
                                <input type="image" name="cartContinue" src="/images/basket/cta.png" border="0" alt="Continue Checkout">
                            </form>
                        </div>
						<div style="float:left; width:100%; text-align:center; color:#333; padding:10px 0px 10px 0px; font-weight:bold;">- or -</div>
						<div style="float:left; width:100%;">
                            <div style="float:left; width:50%;">
                                <form name="PaypalForm" action="<%=secureURL%>/cart/process/PaypalSOAP/ReviewOrder.asp" method="post">
                                    <% if session("stmFreeItem") = 1 then nProdIdCount = nProdIdCount + 1 %>
                                    <input type="hidden" name="promo" value="<%=sPromoCode%>">
                                    <input type="hidden" name="ItemsAndWeight" value="<%=cStr(nProdIdCount) & "|" & cStr(sWeight)%>">
                                    <input type="hidden" name="paymentAmount" value="<%=nSubTotal%>">
                                    <input type="hidden" name="shipZip" value="">
                                    <input type="hidden" name="buysafeamount" value="0">
                                    <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
                                    <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
                                    <% if session("stmFreeItem") = 1 then PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & stm_freeItem & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & stm_freeItemDesc & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""1""><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""0"">" & vbcrlf %>
                                    <%=PPhtml%>
                                    <!--<input type="image" name="cartPaypal" onClick="return CheckSubmit(this.form,'PP');" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif">-->
                                    <input type="image" name="cartPaypal" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif">
                                </form>
                            </div>
                            <div style="float:left; width:50%;">
                                <form name="GoogleForm" method="POST" action="<%=secureURL%>/cart/process/google/CartProcessing.asp">
                                    <!--<input type="image" name="Checkout" onClick="return CheckSubmit(this.form,'GC');" alt="Google Checkout" src="https://checkout.google.com/buttons/checkout.gif?merchant_id=468324454609889&w=168&h=44&style=white&variant=text&loc=en_US" height="44" width="168">-->
                                    <input type="image" name="Checkout" alt="Google Checkout" src="https://checkout.google.com/buttons/checkout.gif?merchant_id=468324454609889&w=168&h=44&style=white&variant=text&loc=en_US" height="44" width="168">
                                    <input type="hidden" name="promocode" value="<%=sPromoCode%>">
                                    <input type="hidden" name="analyticsdata" value="">
                                    <input type="hidden" name="shipZip" value="">
                                    <input type="hidden" name="sWeight" value="<%=cStr(sWeight)%>">
                                    <input type="hidden" name="buysafeamount" value="0">
                                    <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
                                    <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
                                    <%=WEhtml%>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="float:left; width:100%; text-align:left; padding-top:10px;">
                	<a href="/" style="color:#3c537c; font-size:13px; font-weight:bold; text-decoration:none;">&laquo; Browse more items</a>
                </div>
                <div id="mvt_slides" style="float:left; width:100%;">
                <%
					sql = 	"select	rand(cast(cast(newid() as binary(8)) as int)) * a.itemid id" & vbcrlf & _
							"	,	a.itemid, a.itempic_co, a.itemdesc_co, a.price_retail, a.price_co, a.partnumber, round(a.reviewRating / a.reviewCnt, 2) avgReview " & vbcrlf & _
							"from	(" & vbcrlf & _
							"		select	top 30 b.itemid, b.itempic_co, b.itemdesc_co, b.price_retail, b.price_co, b.partnumber, count(*) reviewCnt, isnull(sum(rating), 0) reviewRating" & vbcrlf & _
							"		from	we_orderdetails a join we_items b" & vbcrlf & _
							"			on	a.itemid = b.itemid left outer join co_reviews c" & vbcrlf & _
							"			on	b.partnumber = c.partnumbers and c.approved = 1 and c.rating >= 3" & vbcrlf & _
							"		where	a.orderid in (	select	distinct top 100 orderid" & vbcrlf & _
							"								from	we_orderdetails" & vbcrlf & _
							"								where	itemid in (" & strItemCheck & ")" & vbcrlf & _
							"								order by orderid desc )" & vbcrlf & _
							"			and	b.hidelive = 0" & vbcrlf & _
							"			and	b.inv_qty <> 0" & vbcrlf & _
							"			and	b.price_co > 0" & vbcrlf & _
							"			and	b.itemid not in (" & strItemCheck & ")" & vbcrlf & _
							"		group by b.itemid, b.itempic_co, b.itemdesc_co, b.price_retail, b.price_co, b.partnumber" & vbcrlf & _
							"		order by 1 desc" & vbcrlf & _
							"		) a" & vbcrlf & _
							"order by 1"
					set rsCross = Server.CreateObject("ADODB.Recordset")
					rsCross.open sql, oConn, 3, 3
'					set rsCross = oConn.execute(sql)
					bPagination = false
					if not rsCross.eof then
					%>
					<div style="float:left; width:100%; text-align:left; color:#3c537c; padding-top:20px;">
						<div style="float:left; padding-right:10px;"><img src="/images/basket/cross-sell-arrow.png" border="0" /></div>
						<div style="float:left; padding-top:3px; font-weight:bold; font-style:italic;">Customers Who Purchased Items In Your Cart Also Bought...</div>
					</div>
					<div id="slides_crosssell" style="float:left; width:980px; padding-top:30px; position:relative;">
						<div class="slides_container" style="float:left; margin-left:40px; width:900px; height:280px;">
							<div class="slide" style="width:100%; float:left;">
							<%
							set fs = CreateObject("Scripting.FileSystemObject")
							lapTotal = rsCross.recordcount
							lap = 0
							do until rsCross.eof
								lap = lap + 1
								crossItemID = rsCross("itemid")
								itemdesc = rsCross("itemdesc_co")
								if len(itemdesc) > 67 then itemdesc = left(itemdesc, 64) & "..."
	
								itempic_co = rsCross("itempic_co")
								itemImgPath = server.MapPath("/productPics/thumb/" & itempic_co)
								if not fs.FileExists(itemImgPath) then
									useImg = "/productPics/thumb/imagena.jpg"
								else
									useImg = "/productPics/thumb/" & itempic_co
								end if
								
								productLink = "p-" & crossItemID & "-" & formatSEO(rsCross("itemdesc_co")) & ".html"
							%>
								<form name="frmCross<%=lap%>" action="/cart/item_add.asp" method="post" style="margin:0px; padding:0px;">
								<input type="hidden" name="qty" value="1">
								<input type="hidden" name="prodid" value="<%=crossItemID%>">
								<div class="item" style="float:left; width:170px; padding:5px;">
									<div style="float:left; width:100%; text-align:center;">
										<a href="/<%=productLink%>" target="_blank" title="<%=rsCross("itemdesc_co")%>"><img src="<%=useImg%>" border="0" width="100" alt="<%=rsCross("itemdesc_co")%>" /></a>
									</div>
									<div style="float:left; width:100%; text-align:left; padding-top:5px; height:45px;">
										<a href="/<%=productLink%>" target="_blank" style="font-size:12px; color:#3c537c;" title="<%=rsCross("itemdesc_co")%>"><%=itemdesc%></a>
									</div>
									<div style="float:left; width:100%; text-align:left; padding-top:5px;">
										<%=getRatingAvgStar(rsCross("avgReview"))%>
									</div>
									<div style="float:left; width:100%; text-align:left; padding-top:5px; color:#900; font-size:18px;">
										<%=formatcurrency(rsCross("price_co"))%>
									</div>
									<div style="float:left; width:100%; text-align:left; padding-top:5px; color:#666; font-size:12px;">
										Retail: <del><%=formatcurrency(rsCross("price_retail"))%></del>
									</div>
									<div style="float:left; width:100%; text-align:left; padding-top:5px; color:#666; font-size:12px;">
										<input type="image" src="/images/basket/mini-cta.png" border="0" width="92" height="21" />
									</div>
								</div>
								</form>
							<%
								if (lap mod 5) = 0 and lap <> lapTotal then
									response.write "</div><div class=""slide"" style=""width:100%; float:left;"">"
									bPagination = true
								end if
								rsCross.movenext
							loop
							%>
							</div>
						</div>
						<div class="slides-prev" onmousedown='this.style.backgroundImage="url(/images/basket/arrow_left_down.png)"' onmouseup='this.style.backgroundImage="url(/images/basket/arrow_left_hover.png)"' onmouseout='this.style.backgroundImage="url(/images/basket/arrow_left.png)"' onmouseover='this.style.backgroundImage="url(/images/basket/arrow_left_hover.png)"'></div>
						<div class="slides-next" onmousedown='this.style.backgroundImage="url(/images/basket/arrow_right_down.png)"' onmouseup='this.style.backgroundImage="url(/images/basket/arrow_right_hover.png)"' onmouseout='this.style.backgroundImage="url(/images/basket/arrow_right.png)"' onmouseover='this.style.backgroundImage="url(/images/basket/arrow_right_hover.png)"'></div>
					</div>
					<div style="display:none;">
						<img src="/images/basket/arrow_left.png" border="0" />
						<img src="/images/basket/arrow_left_down.png" border="0" />
						<img src="/images/basket/arrow_left_hover.png" border="0" />
						<img src="/images/basket/arrow_right_down.png" border="0" />
						<img src="/images/basket/arrow_right.png" border="0" />
						<img src="/images/basket/arrow_right_hover.png" border="0" />
					</div>
	                <%end if%>
				</div>
                <%end if%>
            </td>
        </tr>
    </table>
</td>
<div id="lowPricePopup" style="display:none;">
    <div style="width:600px; background-color:#fff; text-align:center; margin-left:auto; margin-right:auto; margin-top:150px; position:relative;">
        <div style="position:absolute; top:5px; right:5px; color:#666; font-weight:bold; cursor:pointer;" onclick="closeLowPricePopup()">X</div>
        <div style="border-bottom:1px solid #ccc; background-color:#f2f2f2; height:100px;">
		    <div style="float:left; padding:15px;">
            	<img src="/images/basket/shield.png" border="0" />
            </div>
		    <div style="padding:35px 0px 0px 10px; float:left; font-size:30px; color:#333; font-weight:bold;">110% LOW PRICE GUARANTEE</div>
        </div>
        <div style="clear:both;"></div>
        <div style="width:100%; font-size:12px; color:#333;">
        	<div style="padding:25px; text-align:left; line-height:17px;">
	            CellularOutfitter.com GUARANTEES the lowest price of any major online retailer. We're so confident in our ability to provide the lowest possible price of any online retailer, that we will credit you the difference if you can find a cheaper price online.
				<br /><br />
                <span style="font-weight:bold; color:#000;">HERE's HOW IT WORKS:</span>
                <br /><br />
                If you have found a price (product + shipping + tax) lower than ours on a competing site, please email a screen shot of the checkout page to: priceguarantee@cellularoutfitter.com, or, you can fax a printout to:
				<br /><br />
                <span style="font-style:italic;">(714) 278-1932</span><br />
                <span style="font-style:italic;">attn: "CellularOutfitter.com Price Guarantee"</span>
				<br /><br />
				We will credit you the difference! This offer is effective a full 14 days after your purchase date so you can be assured you are receiving the BEST value anywhere.
				<br /><br />
				It's as simple as that! No gimmicks, no tricks. It's just another way that the team here at CellularOutfitter.com is dedicated to providing the best possible shopping experience anywhere!
                <br /><br />
                <span style="font-weight:bold; color:#000;">PLEASE NOTE: </span>Due to the volatility of the online auction marketplace, we DO NOT match pricing on auction sites such as eBay, Overstock, Half.com, Yahoo! Auctions, Amazon.com auctions, etc. The competing site and offer must be recognized by what CellularOutfitter.com deems a valid, established online U.S. merchant in good standing and shall not include micro-affiliate, gateway, non-domestic (USA) sites or any other variation thereof and of which shall be determined at the sole discretion of CellularOutfitter.com.
				<br /><Br />
                <span style="font-weight:bold; color:#000;">ALSO NOTE: </span>Our price matching guarantee applies to cellular accessories only. This guarantee does not apply to cellular phones.
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>
<div id="returnPopup" style="display:none;">
    <div style="width:600px; background-color:#fff; text-align:center; margin-left:auto; margin-right:auto; margin-top:150px; position:relative;">
        <div style="position:absolute; top:5px; right:5px; color:#666; font-weight:bold; cursor:pointer;" onclick="closeReturnPopup()">X</div>
        <div style="border-bottom:1px solid #ccc; background-color:#f2f2f2; height:100px;">
		    <div style="float:left; padding:15px;">
            	<img src="/images/basket/shield.png" border="0" />
            </div>
		    <div style="padding:35px 0px 0px 10px; float:left; font-size:30px; color:#333; font-weight:bold;">90-DAY RETURNS</div>
        </div>
        <div style="clear:both;"></div>
        <div style="width:100%; font-size:12px; color:#333;">
        	<div style="padding:25px; text-align:left; line-height:17px; height:450px; overflow:auto;">
				<span style="font-weight:bold; color:#000;">Return/Warranty Policy:</span>
                <br />
				If you are not happy with the any product, you may return the item for an exchange or refund of the purchase price (minus shipping) within ninety (90) days of receipt. We also offer a ONE YEAR MANUFACTURER WARRANTY on all items. If your item is defective, for up to a full year, simply return the item to CellularOutfitter.com for a prompt exchange. 
				<br /><br />
				All returns must be accompanied by a Cellular Outfitter RMA number. To obtain an RMA number, please send an e-mail to: <a href="mailto:returns@CellularOutfitter.com">returns@CellularOutfitter.com </a>
				<br /><br />
				The information below must be included for any return:
                <br />
				&nbsp; - Order Number<br />
                &nbsp; - Date of original purchase<br />
                &nbsp; - Reason for request for RMA<br />
                &nbsp; - Your e-mail address<br />
				<br /><br />	
				No returns, refunds or exchanges will be processed without an RMA number. 
				<br /><br />
				<span style="font-weight:bold; color:#000;">CELL PHONE RETURN POLICY:</span><br />
				All return requests must be made within 7 days of receiving the item. All returned merchandise must be unregistered, in complete original manufacturer's packaging, same condition as sold, with all literature, accessories, instructions, blank warranty cards and documentation. All Phones must be returned in "Like-New Condition", show no signs of use and must have less than 25 minutes in total cumulative talk time. 
				<br /><br />
				If an item is defective it may only be exchanged with an equivalent product if we are unable to supply the same item or if the item cannot be repaired. Any return shipping cost is the responsibility of the customer. All shipping and handling fees are non-refundable. This includes all refused and unaccepted packages. 
				<br /><br />
				All NON-DEFECTIVE cell phone returns will be subject to a 20% restocking fee. 
				<br /><br />
				For refunds allow 3-4 business days for the refund to post to your account from the time the refund is issued. 
				<br /><br />
				<span style="font-weight:bold; color:#000;">Non-Defective Returns:</span><br />
				NON-DEFECTIVE RETURNS OF ACCESSORIES ARE SUBJECT TO A 15% RE-STOCKING FEE. NON-DEFECTIVE RETURNS OF PHONES ARE SUBJECT TO A 20% RE-STOCKING FEE. Such returns will be for store credit or refund at the customer's request. Refunds are only available for order within 30 days of original order date for accessories and 7 days for phones. Refunds will be issued for the current published price of the product(s) minus a 15% or 20% re-stocking fee. 
				<br /><br />
				<span style="font-weight:bold; color:#000;">NON-DEFECTIVE BLUETOOTH AND WIRED HEADSET RETURNS:</span><br /> 
				NON-DEFECTIVE BLUETOOTH AND WIRED HEADSET RETURNS MAY BE SUBJECT TO A 15% RE-STOCKING FEE and such returns will be for store credit or refund (at the customer's request). Refunds are only available within 90 days of original order date. Please contact us at <a href="mailto:returns@cellularoutfitter.com">returns@cellularoutfitter.com</a> if you have questions about which products are returnable and which products may be subject to a restocking fee. 
				<br /><br />
				Customers must contact the headset manufacturer for all defective/warranty issues on headsets that have had the packaging opened. For your convenience, you can contact us at <a href="mailto:returns@CellularOutfitter.com">returns@CellularOutfitter.com</a> for the manufacturers' contact information.
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>

<!--#include virtual="/includes/template/bottom_cart.asp"-->
<%
function getRatingAvgStar(rating)
	dim nRating 
	dim strRatingImg
	nRating = cdbl(rating)
	strRatingImg = 	"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />"
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""	
		for i=1 to 5
			if nRating => i then
				strRatingImg = strRatingImg & "<img src=""/images/review/greenStarFull.gif"" border=""0"" width=""14"" height=""13"" /> "
			elseif nRating => ((i - 1) + .1) then
				strRatingImg = strRatingImg & "<img src=""/images/review/greenStarHalf.gif"" border=""0"" width=""8"" height=""13"" /> "				
			else
				strRatingImg = strRatingImg & "<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" /> "
			end if
		next		
	end if
	
	getRatingAvgStar = strRatingImg
end function

function fWriteBasketRow2(nProdId,sItemName,sItemPrice,nQty,sRetailPrice,sitempic_CO,sTypeID)
	categoryDesc = ""
	dim arrValues(2)
	select case sTypeID
		case 3 
			arrValues(0) = "Protects your pricey device!"
			arrValues(1) = "Shields your phone from scratches, dings and bumps"
			arrValues(2) = "Crafted to perfectly fit your phone"
		case 7 
			arrValues(0) = "Classy, timeless design"
			arrValues(1) = "Provides heavy duty protection"
			arrValues(2) = "Designed to perfectly fit your phone"
		case 5 
			arrValues(0) = "Extend the functionality of your device"
			arrValues(1) = "Hands-free operation for maximum convenience"
			arrValues(2) = "90 Day Worry Free Return Policy"
		case 2 
			arrValues(0) = "Keep your phone powered up while on the go"
			arrValues(1) = "Built in over charge protection in all devices"
			arrValues(2) = "Save up to 80% off MSRP on our high quality items"
		case 1 
			arrValues(0) = "Guaranteed to work upon delivery"
			arrValues(1) = "Extend the life of your mobile device"
			arrValues(2) = "Long lasting & 1 Year Warranty"
		case 6 
			arrValues(0) = "Keep your phone handy at all times"
			arrValues(1) = "High quality construction ensures lasting use"
			arrValues(2) = "1 Year Warranty on all items"
		case 18 
			arrValues(0) = "Easy, bubble-free application"
			arrValues(1) = "Protects your screen from scratches and other damage"
			arrValues(2) = "Retains full touch screen responsiveness"
		case 19,20 
			arrValues(0) = "Cut to perfectly fit your phone"
			arrValues(1) = "High quality 3M material"
			arrValues(2) = "Ultra-thin yet durable material"
		case 8
			arrValues(0) = "Extend the functionality of your device"
			arrValues(1) = "90 Day Worry Free Return Policy"
			arrValues(2) = "1 Year Warranty on all items"
		case 16
			arrValues(0) = "Save up to 80% retail prices"
			arrValues(1) = "Unlocked phones provide worldwide use"
			arrValues(2) = "Personalized technical support included"
	end select

	categoryDesc = 	"<div style=""float:left; width:100%;"">" & vbcrlf
	for nCnt = 0 to ubound(arrValues)
		categoryDesc = categoryDesc & "	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf
		categoryDesc = categoryDesc & "	<div style=""float:left; width:270px;"">" & arrValues(nCnt) & "</div>" & vbcrlf
		categoryDesc = categoryDesc & "</div><div style=""float:left; width:100%;"">" & vbcrlf
	next	
	categoryDesc = categoryDesc & "</div>"

	strTemp = 	"<div style=""width:978px; float:left; padding:5px;"">" & vbcrlf & _
				"	<div style=""width:968px; float:left; padding-top:10px;"">" & vbcrlf & _
				"		<div style=""float:left; width:100px; padding:0px 10px 10px 10px;"">" & vbcrlf & _
				"			<a href=""/p-" & nProdIdCheck & "-" & formatSEO(sItemName) & ".html""><img src=""" & sitempic_CO & """ border=""0"" alt=""" & sItemName & """ width=""100"" height=""100"" /></a>" & vbcrlf & _
				"		</div>" & vbcrlf & _
				"		<div style=""float:left; width:305px; padding:0px 10px 0px 0px; text-align:left;"">" & vbcrlf & _
				"			<div style=""padding-bottom:10px;""><a style=""font-size:14px; color:#333; font-weight:bold;"" href=""/p-" & nProdIdCheck & "-" & formatSEO(sItemName) & ".html"">" & sItemName & "</a></div>" & vbcrlf & _
				"			<div style=""font-size:12px; color:#555; font-weight:normal;"" class=""mvt_cartProdDetail"">" & categoryDesc & "</div>" & vbcrlf & _
				"		</div>" & vbcrlf & _
				"		<div style=""float:left; width:220px; text-align:center;"">" & vbcrlf & _
				"			<div class=""mvt_instock"" style=""float:left; width:100%;"">" & vbcrlf & _
				"				<div style=""float:left; padding-left:10px;""><img src=""/images/basket/qty-checkmark.png"" border=""0"" /></div>" & vbcrlf & _
				"				<div style=""float:left; padding:4px 0px 0px 5px; font-size:13px; color:#333;"">This product is <span style=""color:#3a983a; font-weight:bold;"">in stock</span>.</div>" & vbcrlf & _
				"			</div>" & vbcrlf & _
				"			<div style=""float:left; width:198px; margin:10px 0px 0px 5px; padding:5px; font-size:12px; color:#555; border:1px solid #ccc; -moz-box-shadow: 1px 2px 5px #666; -webkit-box-shadow: 1px 2px 5px #666; box-shadow: 1px 2px 5px #666; border-radius:5px;"">" & gblShipoutDateDesc & vbcrlf & _
				"			</div>" & vbcrlf & _
				"		</div>" & vbcrlf & _
				"		<div style=""float:left; width:70px; text-align:center;"">" & vbcrlf & _
				"			<form name=""frmEditDelete_" & nProdId & """ action=""/cart/item_edit.asp"" method=""post"" style=""margin:0px;"">" & vbcrlf & _
				"				<input type=""hidden"" name=""prodid"" value=""" & nProdId & """>" & vbcrlf & _
				"				<input type=""hidden"" name=""pagetype"" value=""edit"">" & vbcrlf & _
				"				<input type=""text"" name=""qty"" size=""1"" maxlength=""4"" style=""border:1px solid #ccc; padding:5px; border-radius:5px; text-align:center;"" value=""" & nQty & """><br /><br />" & vbcrlf & _
				"				<a style=""font-size:11px; color:#3f597e; font-weight:normal; text-decoration:underline;"" href=""javascript:EditDeleteItem('frmEditDelete_" & nProdId & "','edit');""><b>Update</b></a><br />" & vbcrlf & _
				"				<a style=""font-size:11px; color:#3f597e; font-weight:normal; text-decoration:underline;"" href=""javascript:EditDeleteItem('frmEditDelete_" & nProdId & "','delete');""><b>Delete</b></a>" & vbcrlf & _
				"			</form>" & vbcrlf & _
				"		</div>" & vbcrlf & _
				"		<div style=""float:left; width:130px; text-align:center; font-size:12px;"">" & vbcrlf & _
				"			<div style=""padding-top:10px; font-size:14px;"">" & formatCurrency(cDbl(sItemPrice))& "</div>" & vbcrlf & _
				"			<div style=""padding-top:5px; font-size:11px; color:#999;"">Retail: " & formatCurrency(cDbl(sRetailPrice)) & "</div>" & vbcrlf & _
				"			<div class=""mvt_lineDiscount"" style=""padding-top:5px; font-style:italic; font-weight:bold; color:#cb0000;"">You Saved " & formatCurrency(cDbl(sRetailPrice) - cDbl(sItemPrice)) & "</div>" & vbcrlf & _
				"		</div>" & vbcrlf & _
				"		<div style=""float:left; width:100px; text-align:center; font-weight:bold;"">" & formatCurrency((cDbl(sItemPrice) * cDbl(nQty))) & "</div>" & vbcrlf & _
				"	</div>" & vbcrlf & _
				"</div>" & vbcrlf & _
				"<div style=""width:978px; float:left; border-bottom:1px solid #ccc;""></div>" & vbcrlf				
	
	fWriteBasketRow2 = strTemp
end function

function fWriteBasketRow(nProdId,sItemName,sItemPrice,nQty,sRetailPrice,sitempic_CO)
	'this will write out the row for the basket
	dim sTemp
	sTemp	=	"<form name=""frmEditDelete" & nProdId & """ action=""/cart/item_edit.asp"" method=""post"" style=""margin:0px;"">" & vbcrlf & _
				"	<tr height=""70px"">" & vbcrlf & _
				"		<td width=""75"" align=""center"" style=""border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;""><a href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html""><img src=""" & sitempic_CO & """ width=""45"" height=""45"" border=""0""></a></td>" & vbcrlf & _
				"		<td align=""left"" valign=""middle"" style=""padding-left:10px; border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;""><a class=""product-cart"" href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html"">" & sItemName & "</a></td>" & vbcrlf & _
				"		<td align=""center"" valign=""middle"" style=""font-size:13px; font-weight:bold; border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;""><s>" & formatCurrency(cDbl(sRetailPrice)) & "</s></td>" & vbcrlf & _
				"		<td align=""center"" valign=""middle"" class=""red-price-small"" style=""border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;"">" & formatCurrency(cDbl(sItemPrice)) & "</td>" & vbcrlf & _
				"		<td align=""center"" valign=""middle"" style=""border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;"">" & vbcrlf & _
				"			<input type=""hidden"" name=""prodid"" value=""" & nProdId & """>" & vbcrlf & _
				"			<input type=""hidden"" name=""pagetype"" value=""edit"">" & vbcrlf & _
				"			<input type=""text"" name=""qty"" size=""1"" maxlength=""4"" value=""" & nQty & """><br />" & vbcrlf & _
				"			<a style=""font-size:12px;color:#000000; font-weight:normal;"" href=""javascript:EditDeleteItem('frmEditDelete" & nProdId & "','edit');""><b>Update</b></a> | " & vbcrlf & _
				"			<a style=""font-size:12px;color:#000000; font-weight:normal;"" href=""javascript:EditDeleteItem('frmEditDelete" & nProdId & "','delete');""><b>Delete</b></a>" & vbcrlf & _
				"		</td>" & vbcrlf & _
				"		<td align=""center"" valign=""middle"" style=""font-size:13px; font-weight:bold; border-bottom:1px solid #eaeaea;"">" & formatCurrency((cDbl(sItemPrice) * cDbl(nQty))) & "</td>" & vbcrlf & _
				"	</tr>" & vbcrlf & _
				"</form>" & vbcrlf
	
	fWriteBasketRow = sTemp
end function

function fWriteErrorBasketRow(nProdId,sItemName,sItemPrice,nQty,sRetailPrice,sitempic_CO)
	'this will write out the row for the basket
	dim sErrorTemp
	
	sErrorTemp	=	"<form name=""frmEditDelete" & nProdId & """ action=""/cart/item_edit.asp"" method=""post"" style=""margin:0px;"">" & vbcrlf & _
					"	<tr height=""70px"">" & vbcrlf & _
					"		<td width=""75"" align=""center""><a href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html""><img src=""/productpics/icon/" & sitempic_CO & """ width=""45"" height=""45"" border=""0""></a></td>" & vbcrlf & _
					"		<td align=""left"" valign=""middle"" style=""padding-left:10px;""><a class=""product-cart"" href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html"">" & sItemName & "</a></td>" & vbcrlf & _
					"		<td align=""center"" valign=""middle"" style=""font-size:13px; font-weight:bold;""><s>" & formatCurrency(cDbl(sRetailPrice)) & "</s></td>" & vbcrlf & _
					"		<td align=""center"" valign=""middle"" class=""red-price-small"">" & formatCurrency(cDbl(sItemPrice)) & "</td>" & vbcrlf & _
					"		<td align=""center"" valign=""middle"" style=""font-size:13px; font-weight:bold;"" colspan=""2"">This product is currently unavailable</td>" & vbcrlf & _
					"	</tr>" & vbcrlf & _
					"</form>" & vbcrlf
					
	fWriteErrorBasketRow = sErrorTemp
end function

function hasImageOnRemote(pRemoteURL, pFileName)
	dim isExists : isExists = false
	dim oXMLHTTP : set oXMLHTTP = server.CreateObject("Msxml2.ServerXMLHTTP")
	dim strTemp : strTemp = pRemoteURL & "/" & pFileName
	
	oXMLHTTP.Open "HEAD", strTemp, False	'communicating without responseBody, only looks up file header
	oXMLHTTP.Send
	
	if (oXMLHTTP.readyState = "4") and (oXMLHTTP.status = "200") and (instr(oXMLHTTP.getResponseHeader("Content-Type"), "image") > 0) then
		isExists = true
	end if

	set oXMLHTTP = Nothing
	
	hasImageOnRemote = isExists
	
end function
%>

<script>
	$(function(){
		<% if bPagination then %>
			$('#slides_crosssell').slides({preload: true, preloadImage: '/images/preloading.gif', generatePagination: true, prev: 'slides-prev', next: 'slides-next'});
		<% else %>
			$('#slides_crosssell').slides({preload: true, preloadImage: '/images/preloading.gif', generatePagination: false /*, prev: 'slides-prev', next: 'slides-next'*/});
		<% end if %>
	});	
	
	function openLowPricePopup() {
		document.getElementById("popBox").innerHTML = document.getElementById('lowPricePopup').innerHTML;
		
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	
	function closeLowPricePopup() {
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
	}	
	
	function openReturnPopup() {
		document.getElementById("popBox").innerHTML = document.getElementById('returnPopup').innerHTML;
		
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	
	function closeReturnPopup() {
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
	}		
</script>

<script language="JavaScript">
function EditDeleteItem(nForm,thisAction) {
	var frmName = eval('document.' + nForm);
	if (isNaN(frmName.qty.value)) {
		alert("Please enter a valid number for Quantity.");
	} else {
		if (thisAction == 'delete') {
			frmName.action = "/cart/item_delete.asp";
			//frmName.action = "http://staging.cellularoutfitter.com/cart/item_delete.asp";
		} else {
			frmName.action = "/cart/item_edit.asp";
			//frmName.action = "http://staging.cellularoutfitter.com/cart/item_edit.asp";
		}
		frmName.pagetype.value = thisAction;
		frmName.submit();
	}
}
</script>

<script language="javascript">
	function showPromo2() {
		document.getElementById("promoCodeBtn2").style.display = 'none'
		document.getElementById("promoCode2").style.display = ''
	}
	
	function showPromo() {
		document.getElementById("promoCodeBtn").style.display = 'none'
		document.getElementById("promoCode").style.display = ''
	}
	
function CheckSubmit(pymt) {
	/*
	bValid = true;
	CheckValidNEW(document.shippingZip.shipZip.value, "Your Shipping Zip is a required field!");
	if (bValid) {
		if (pymt == 'PP') {
			document.PaypalForm.shipZip.value = document.shippingZip.shipZip.value;
			document.PaypalForm.action = '/cart/process/PaypalSOAP/ReviewOrder.asp';
			//document.PaypalForm.action = '/cart/process/PaypalSOAP/ReviewOrder.asp';
			document.PaypalForm.submit();
		} else if (pymt == 'GC') {
			document.GoogleForm.shipZip.value = document.shippingZip.shipZip.value;
			document.GoogleForm.submit();
		} else {
			<%
'			if crossSell <> 0 then
'				if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
'					response.write "document.CheckoutForm.action = '/cart/checkout.asp';"
'				else
'					response.write "document.CheckoutForm.action = 'https://www.cellularoutfitter.com/cart/checkout.asp';"
'				end if
'			else
'				if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
'					response.write "document.CheckoutForm.action = '/cart/checkout.asp';"
'				else
'					response.write "document.CheckoutForm.action = 'https://www.cellularoutfitter.com/cart/checkout.asp';"
'				end if
'			end if
			%>
			document.CheckoutForm.shipZip.value = document.shippingZip.shipZip.value;
			document.CheckoutForm.submit();
		}
	}
	return false;
	*/
}
</script>