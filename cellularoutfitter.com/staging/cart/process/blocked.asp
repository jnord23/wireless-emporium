<html>
<head>
<link rel="stylesheet" href="https://www.cellularoutfitter.com/includes/css/styleCO.css" type="text/css">
</head>

<body bgcolor="#ffffff" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" link="#666666" vlink="#666666" alink="#ff6600">

<br><br>
<table cellspacing="1" cellpadding="1" width="75%" border="1" align="center" class="smlText">
	<tr>
		<td align="center"><a href="http://www.cellularoutfitter.com"><img src="http://www.cellularoutfitter.com/images/CellularOutfitter.jpg" alt="CellularOutfitter.com" width="292" height="52" border="0"></a></td>
	</tr>
	<tr>
		<td align="center">
			<strong>
				<br>
				Due to security reasons, we are unable to process your order online at this time.
				<br><br>
				Please contact us at (714) 278-1930
				<br>
				and an Associate will be happy to process your order immediately over the phone.
				<br><br>
				Thank you for shopping with Cellular Outfitter!
				<br><br>
				<a href="mailto:sales@cellularoutfitter.com">sales@cellularoutfitter.com</a>
				<br><br>
			</strong>
		</td>
	</tr>
</table>

</body>
</html>
