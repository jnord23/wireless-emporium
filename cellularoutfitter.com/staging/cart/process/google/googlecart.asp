<%
'******************************************************************************
' Copyright (C) 2006 Google Inc.
'  
' Licensed under the Apache License, Version 2.0 (the "License");
' you may not use this file except in compliance with the License.
' You may obtain a copy of the License at
'  
'      http://www.apache.org/licenses/LICENSE-2.0
'  
' Unless required by applicable law or agreed to in writing, software
' distributed under the License is distributed on an "AS IS" BASIS,
' WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
' See the License for the specific language governing permissions and
' limitations under the License.
'******************************************************************************


'******************************************************************************
' Class ParameterizedUrls is the object representation of a 
'     <parameterized-urls> used in the <merchant-checkout-flow-support>.
'******************************************************************************
Class ParameterizedUrls
    Public url
	Public total
End Class


'******************************************************************************
' Class Cart is the object representation of a <checkout-shopping-cart>.
' It has methods to create and post a shopping cart to Google Checkout.
'******************************************************************************
Class Cart
	Public MerchantCalculationsUrl
	Public AcceptMerchantCoupons
	Public AcceptGiftCertificates
	Public MerchantCalculatedTax
    Public CartExpiration
    Public MerchantPrivateData
    Public EditCartUrl
    Public ContinueShoppingUrl
    Public RequestBuyerPhoneNumber
	Public RequestInitialAuthDetails

	Public ItemArr()
	Public ShippingArr()
	Public PURLArr()
	Public DefaultTaxRuleArr()
	Public AlternateTaxTableArr()

    Private ItemIndex
	Private ShippingIndex
	Private PURLIndex
	Private DefaultTaxRuleIndex
	Private AlternateTaxTableIndex

    Private Sub Class_Initialize()
		AcceptMerchantCoupons = false
		AcceptGiftCertificates = false
		MerchantCalculatedTax = false
    	RequestBuyerPhoneNumber = false
		RequestInitialAuthDetails = false

		ItemIndex = -1
		ShippingIndex = -1
		PURLIndex = -1
		DefaultTaxRuleIndex = -1
		AlternateTaxTableIndex = -1	
	End Sub

    '**************************************************************************
	' Adds an item to the cart.
	'
	' Input:    Item    Object: Class Item
    '**************************************************************************
    Public Sub AddItem(Item)
        ItemIndex = ItemIndex + 1
		ReDim Preserve ItemArr(ItemIndex)
		Set ItemArr(ItemIndex) = Item
    End Sub

	'**************************************************************************
	' Adds a shipping method to the cart.
	'
	' Input:    Shipping    Object: Class FlatRateShipping, or
	'                               Class MerchantCalculatedShipping, or
	'                               Class Pickup
    '**************************************************************************
    Public Sub AddShipping(Shipping)
        ShippingIndex = ShippingIndex + 1
		ReDim Preserve ShippingArr(ShippingIndex)
		Set ShippingArr(ShippingIndex) = Shipping
    End Sub

	'**************************************************************************
	' Adds a Parameterized Url to the cart.
	'
	' Input:    Url, Total
    '**************************************************************************
    Public Sub AddParameterizedUrl(PURL)
        PURLIndex = PURLIndex + 1
		ReDim Preserve PURLArr(PURLIndex)
		Set PURLArr(PURLIndex) = PURL
    End Sub

    '**************************************************************************
	' Adds a defalt tax rule to the default tax table.
	'
	' Input:    TaxRule    Object: Class DefaultTaxRule
    '**************************************************************************
	Public Sub AddDefaultTaxRule(TaxRule)
        DefaultTaxRuleIndex = DefaultTaxRuleIndex + 1
		ReDim Preserve DefaultTaxRuleArr(DefaultTaxRuleIndex)
		Set DefaultTaxRuleArr(DefaultTaxRuleIndex) = TaxRule
	End Sub

    '**************************************************************************
	' Adds an alternate tax table to the alternate tax tables.
	'
	' Input:    TaxTable    Object: Class AlternateTaxTable
    '**************************************************************************
	Public Sub AddAlternateTaxTable(TaxTable)
		AlternateTaxTableIndex = AlternateTaxTableIndex + 1
		ReDim Preserve AlternateTaxTableArr(AlternateTaxTableIndex)
		Set AlternateTaxTableArr(AlternateTaxTableIndex) = TaxTable
	End Sub

    '**************************************************************************
	' Returns the <checkout-shopping-cart> XML in plain text format.
    '**************************************************************************
    Public Property Get GetXml()
		Dim Xml, Attr
		Dim i, Item, TaxRule, TaxTable, Shipping, State, ZipPattern

		Set Xml = new XmlBuilder

		Attr = Xml.Attribute("xmlns", SchemaUri)
		Xml.Push "checkout-shopping-cart", Attr
		Xml.Push "shopping-cart", ""

		If CartExpiration <> "" Then
			Xml.Push "cart-expiration", ""
			Xml.AddElement "good-until-date", CartExpiration, ""
			Xml.Pop "cart-expiration"
		End If

		Xml.Push "items", ""
		For Each Item In ItemArr
			Xml.Push "item", ""
			Xml.AddElement "item-name", Item.Name, ""
			Xml.AddElement "item-description", Item.Description, ""
			Attr = Xml.Attribute("currency", MerchantCurrency)
			Xml.AddElement "unit-price", Item.UnitPrice, Attr
			Xml.AddElement "quantity", Item.Quantity, ""
			If Item.MerchantPrivateItemData <> "" Then
				Xml.AddXmlElement "merchant-private-item-data", Item.MerchantPrivateItemData, ""
			End If
			If Item.MerchantItemId <> "" Then
				Xml.AddElement "merchant-item-id", Item.MerchantItemId, ""
			End If
			If Item.TaxTableSelector <> "" Then
				Xml.AddElement "tax-table-selector", Item.TaxTableSelector, ""
			End If
			Xml.Pop "item"
		Next
		Xml.Pop "items"

		If MerchantPrivateData <> "" Then
			Xml.AddXmlElement "merchant-private-data", MerchantPrivateData, ""
		End If

		Xml.Pop "shopping-cart"
		Xml.Push "checkout-flow-support", ""
		Xml.Push "merchant-checkout-flow-support", ""

		If EditCartUrl <> "" Then
			Xml.AddElement "edit-cart-url", EditCartUrl, ""
		End If

		If ContinueShoppingUrl <> "" Then
			Xml.AddElement "continue-shopping-url", ContinueShoppingUrl, ""
		End If

		If RequestBuyerPhoneNumber Then
			Xml.AddElement "request-buyer-phone-number", "true", ""
		End If

		If MerchantCalculationsUrl <> "" Then
			Xml.Push "merchant-calculations", ""
			Xml.AddElement "merchant-calculations-url", MerchantCalculationsUrl, ""
			If AcceptMerchantCoupons Then
				Xml.AddElement "accept-merchant-coupons", "true", ""
			End If
			If AcceptGiftCertificates Then
				Xml.AddElement "accept-gift-certificates", "true", ""
			End If
			Xml.Pop "merchant-calculations"
		End If

		If ShippingIndex >= 0 Then 
			Xml.Push "shipping-methods", ""
			
			For Each Shipping In ShippingArr
				Attr = Xml.Attribute("name", Shipping.Name)
				Xml.Push Shipping.ShippingType, Attr
				Attr = Xml.Attribute("currency", MerchantCurrency)
				Xml.AddElement "price", Shipping.Price, Attr
				
				If Shipping.ShippingType = "flat-rate-shipping" Or _
					Shipping.ShippingType = "merchant-calculated-shipping" Then
					Dim AllowedRestrictions, ExcludedRestrictions

					AllowedRestrictions = Shipping.AllowedCountryArea <> "" Or  _
											UBound(Shipping.AllowedStates) Or  _
											UBound(Shipping.AllowedZipPatterns)

					ExcludedRestrictions = Shipping.ExcludedCountryArea <> "" Or  _
											UBound(Shipping.ExcludedStates) >= 0 Or  _
											UBound(Shipping.ExcludedZipPatterns) >= 0

					If AllowedRestrictions Or ExcludedRestrictions Then
						Xml.Push "shipping-restrictions", ""
						If AllowedRestrictions Then
							Xml.Push "allowed-areas", ""
							If Shipping.AllowedCountryArea <> "" Then
								if Shipping.AllowedCountryArea = "CA" then
									Xml.Push "postal-area", ""
									Xml.AddElement "country-code", "CA", ""
									Xml.Pop "postal-area"
								else
									Attr = Xml.Attribute("country-area", Shipping.AllowedCountryArea)
									Xml.EmptyElement "us-country-area", Attr
									
									For Each State In Shipping.AllowedStates
										Xml.Push "us-state-area", ""
										Xml.AddElement "state", State, ""
										Xml.Pop "us-state-area"
									Next
		
									For Each ZipPattern In Shipping.AllowedZipPatterns
										Xml.Push "us-zip-area", ""
										Xml.AddElement "zip-pattern", ZipPattern, ""
										Xml.Pop "us-zip-area"
									Next
								end if
							End If

							Xml.Pop "allowed-areas"
						End If

						If ExcludedRestrictions Then
							Xml.Push "excluded-areas", ""
							If Shipping.ExcludedCountryArea <> "" Then
								Attr = Xml.Attribute("country-area", Shipping.ExcludedCountryArea)
								Xml.EmptyElement "us-country-area", Attr
							End If

							For Each State In Shipping.ExcludedStates
								Xml.Push "us-state-area", ""
								Xml.AddElement "state", State, ""
								Xml.Pop "us-state-area"
							Next

							For Each ZipPattern In Shipping.ExcludedZipPatterns
								Xml.Push "us-zip-area", ""
								Xml.AddElement "zip-pattern", ZipPattern, ""
								Xml.Pop "us-zip-area"
							Next

							Xml.Pop "excluded-areas"
						End If
						Xml.Pop "shipping-restrictions"
					End If
				End If
				Xml.Pop Shipping.ShippingType
			Next
			Xml.Pop "shipping-methods"
		End If

		If PURLIndex >= 0 Then 
			Xml.Push "parameterized-urls", ""
			For Each PURL In PURLArr
				Attr = Xml.Attribute("url", PURL.url)
				Xml.Push "parameterized-url", Attr
				Xml.Push "parameters", ""
					Attrs = Xml.Attribute("name", "orderid")
					Attrs = Attrs & " " & Xml.Attribute("type", "order-id")
					Xml.MCEmptyElement "url-parameter", Attrs
					Attrs = Xml.Attribute("name", "customerid")
					Attrs = Attrs & " " & Xml.Attribute("type", "buyer-id")
					Xml.MCEmptyElement "url-parameter", Attrs
					if PURL.total <> "" then
						Attrs = Xml.Attribute("name", PURL.total)
						Attrs = Attrs & " " & Xml.Attribute("type", "order-subtotal")
						Xml.MCEmptyElement "url-parameter", Attrs
					end if
				Xml.Pop "parameters"
				Xml.Pop "parameterized-url"
			Next
			Xml.Pop "parameterized-urls"
		End If

		If DefaultTaxRuleIndex >= 0 Or AlternateTaxTableIndex >= 0 Then
			If MerchantCalculatedTax Then
				Attr = Xml.Attribute("merchant-calculated", "true")
				Xml.Push "tax-tables", Attr
			Else
				Xml.Push "tax-tables", ""
			End If

			If DefaultTaxRuleIndex >= 0 Then
				Xml.Push "default-tax-table", ""
				Xml.Push "tax-rules", ""
				For Each TaxRule In DefaultTaxRuleArr
					If TaxRule.CountryArea <> "" Then
						Xml.Push "default-tax-rule", ""
						If TaxRule.ShippingTaxed Then
							Xml.AddElement "shipping-taxed", "true", ""
						End If
						Xml.AddElement "rate", TaxRule.Rate, ""
						Xml.Push "tax-area", ""
						Attr = Xml.Attribute("country-area", TaxRule.CountryArea)
						Xml.EmptyElement "us-country-area", Attr
						Xml.Pop "tax-area"
						Xml.Pop "default-tax-rule"
					End If

					For Each State In TaxRule.States
						Xml.Push "default-tax-rule", ""
						If TaxRule.ShippingTaxed Then
							Xml.AddElement "shipping-taxed", "true", ""
						End If
						Xml.AddElement "rate", TaxRule.Rate, ""
						Xml.Push "tax-area", ""
						Xml.Push "us-state-area", ""
						Xml.AddElement "state", State, ""
						Xml.Pop "us-state-area"
						Xml.Pop "tax-area"
						Xml.Pop "default-tax-rule"
					Next

					For Each ZipPattern In TaxRule.ZipPatterns
						Xml.Push "default-tax-rule", ""
						If TaxRule.ShippingTaxed Then
							Xml.AddElement "shipping-taxed", "true", ""
						End If
						Xml.AddElement "rate", TaxRule.Rate, ""
						Xml.Push "tax-area", ""
						Xml.Push "us-zip-area", ""
						Xml.AddElement "zip-pattern", ZipPattern, ""
						Xml.Pop "us-zip-area"
						Xml.Pop "tax-area"
						Xml.Pop "default-tax-rule"
					Next
				Next
				Xml.Pop "tax-rules"
				Xml.Pop "default-tax-table"
			End If

			If AlternateTaxTableIndex >= 0 Then
				Xml.Push "alternate-tax-tables", ""

				For Each TaxTable In AlternateTaxTableArr
					Attr = Xml.Attribute("name", TaxTable.Name)
					If TaxTable.Standalone Then
						Attr = Attr & " " & Xml.Attribute("standalone", "true")
					End If
					Xml.Push "alternate-tax-table", Attr
					Xml.Push "alternate-tax-rules", ""

					For Each TaxRule In TaxTable.AlternateTaxRuleArr
						If TaxRule.CountryArea <> "" Then
							Xml.Push "alternate-tax-rule", ""
							Xml.AddElement "rate", TaxRule.Rate, ""
							Xml.Push "tax-area", ""
							Attr = Xml.Attribute("country-area", TaxRule.CountryArea)
							Xml.EmptyElement "us-country-area", Attr
							Xml.Pop "tax-area"
							Xml.Pop "alternate-tax-rule"
						End If

						For Each State In TaxRule.States
							Xml.Push "alternate-tax-rule", ""
							Xml.AddElement "rate", TaxRule.Rate, ""
							Xml.Push "tax-area", ""
							Xml.Push "us-state-area", ""
							Xml.AddElement "state", State, ""
							Xml.Pop "us-state-area"
							Xml.Pop "tax-area"
							Xml.Pop "alternate-tax-rule"
						Next

						For Each ZipPattern In TaxRule.ZipPatterns
							Xml.Push "alternate-tax-rule", ""
							Xml.AddElement "rate", TaxRule.Rate, ""
							Xml.Push "tax-area", ""
							Xml.Push "us-zip-area", ""
							Xml.AddElement "zip-pattern", ZipPattern, ""
							Xml.Pop "us-zip-area"
							Xml.Pop "tax-area"
							Xml.Pop "alternate-tax-rule"
						Next
					Next
					Xml.Pop "alternate-tax-rules"
					Xml.Pop "alternate-tax-table"
				Next
				Xml.Pop "alternate-tax-tables"
			End If
			Xml.Pop "tax-tables"
		End If

		Xml.Pop "merchant-checkout-flow-support"
		Xml.Pop "checkout-flow-support"

		If RequestInitialAuthDetails Then
			Xml.Push "order-processing-support", ""
			Xml.AddElement "request-initial-auth-details", "true", ""
			Xml.Pop "order-processing-support"
		End If

		Xml.Pop "checkout-shopping-cart"

		GetXml = Xml.GetXml
		Set Xml = Nothing
    End Property

    '**************************************************************************
	' Posts the cart to Google Checkout and redirects the user to 
	'     the Google Checkout page accordingly.
    '**************************************************************************
	Public Sub PostCartToGoogle()
		Dim ResponseXml
		ResponseXml = SendRequest(GetXml, PostUrl)
		ProcessSynchronousResponse(ResponseXml)
	End Sub

    '**************************************************************************
	' Diagnoses the shopping cart XML by posting to the Diagnose URL.
	' It displays the response in the browser.
    '**************************************************************************
	Public Sub DiagnoseXml()
		Dim ResponseXml
		ResponseXml = SendRequest(GetXml, DiagnoseUrl)
		ProcessSynchronousResponse(ResponseXml)
	End Sub

    '**************************************************************************
	' Processes the synchronous response received from Google Checkout 
	'     when a cart is posted.
    '**************************************************************************
	Public Sub ProcessSynchronousResponse(ResponseXml)
		Dim ResponseNode, RootTag
		Set ResponseNode = GetRootNode(ResponseXml)
		RootTag = ResponseNode.Tagname

		Select Case RootTag
			Case "request-received"
				DisplayResponse ResponseXml
			Case "error"
				DisplayResponse ResponseXml
			Case "diagnosis"
				DisplayResponse ResponseXml
			Case "checkout-redirect"
				ProcessCheckoutRedirect ResponseXml
			Case Else
		End Select 
	End Sub

    '**************************************************************************
	' Displays the response in the browser.
    '**************************************************************************
	Public Sub DisplayResponse(ResponseXml)
		Response.Write "<pre>"
		Response.Write Server.HTMLEncode(ResponseXml)
		Response.Write "</pre>"
	End Sub

    '**************************************************************************
	' Redirects the user to the Google Checkout page when a cart is 
	'     successfully posted.
    '**************************************************************************
	Public Sub ProcessCheckoutRedirect(ResponseXml)
		Dim ResponseNode, RedirectUrl
		Set ResponseNode = GetRootNode(ResponseXml)
		RedirectUrl = GetElementText(ResponseNode, "redirect-url")
		Response.Redirect RedirectUrl
		Set ResponseNode = Nothing
	End Sub

	Private Sub Class_Terminate()
		Erase ItemArr
		Erase ShippingArr
		Erase DefaultTaxRuleArr
		Erase AlternateTaxTableArr
	End Sub
End Class


'******************************************************************************
' Class Item is the object representation of an <item>.
'******************************************************************************
Class Item
    Public Name
    Public Description
    Public Quantity
    Public UnitPrice
    Public MerchantItemId
    Public TaxTableSelector
    Public MerchantPrivateItemData

    '**************************************************************************
	' Parses through an <item> node and makes the Item object ready to be used.
    '**************************************************************************
	Public Sub ParseItem(ItemNode)
		Name = GetElementText(ItemNode, "item-name")
		Description = GetElementText(ItemNode, "item-description")
		Quantity = GetElementText(ItemNode, "quantity")
		UnitPrice = GetElementText(ItemNode, "unit-price")
		MerchantItemId = GetElementText(ItemNode, "merchant-item-id")
		TaxTableSelector = GetElementText(ItemNode, "tax-table-selector")

		Dim MerchantPrivateItemDataNL
		Set MerchantPrivateItemDataNL = ItemNode.getElementsByTagname("merchant-private-item-data")
		If MerchantPrivateItemDataNL.Length > 0 Then Set MerchantPrivateItemData = MerchantPrivateItemDataNL(0)
		Set MerchantPrivateItemDataNL = Nothing
	End Sub
End Class


'******************************************************************************
' The DisplayButton function displays a Google Checkout button.
' When a buyer clicks on the button, it will invoke the CartProcessingPage
'
' Input:    ButtonSize    "LARGE" OR "MEDIUM" OR "SMALL"
'           Enabled       true - displays a Google Checkout button
'                         false - displays a disabled Google Checkout button
'******************************************************************************
Sub DisplayButton(ButtonSize, Enabled) 
	Dim ButtonWidth, ButtonHeight, ButtonStyle, ButtonVariant, ButtonLocale, ButtonSrc, ButtonGif

	ButtonSize = UCase(ButtonSize)
	Select Case ButtonSize
		Case "LARGE"
			ButtonWidth = "180"
			ButtonHeight = "46"
		Case "MEDIUM"
			ButtonWidth = "168"
			ButtonHeight = "44"
		Case "SMALL"
			ButtonWidth = "160"
			ButtonHeight = "43"
		Case Else
	End Select 

	If Enabled = True Then
		ButtonVariant = "text"
	ElseIf Enabled = False Then
		ButtonVariant = "disabled"
	End If

	EnvType = UCase(EnvType)
	If EnvType = "SANDBOX" Then
		ButtonGif = "https://sandbox.google.com/checkout/buttons/checkout.gif"
	ElseIf EnvType = "PRODUCTION" Then
		ButtonGif = "https://checkout.google.com/buttons/checkout.gif"
	End If

	ButtonStyle = "white"
	ButtonLocale = "en_US"
	ButtonSrc = ButtonGif & _
				"?merchant_id=" & MerchantId & _
				"&w=" & ButtonWidth & _
				"&h=" & ButtonHeight & _
				"&style=" & ButtonStyle & _
				"&variant=" & ButtonVariant & _
				"&loc=" & ButtonLocale

	If Enabled = True Then
		Response.Write VbCrLf & "<form method=""POST"" action=""" & CartProcessingPage & """>" & VbCrLf
		Response.Write "  <input type=""image"" name=""Checkout"" alt=""Checkout"" src=""" & ButtonSrc & """ height=""" & ButtonHeight & """ width=""" & ButtonWidth & """>" & VbCrLf
		Response.Write "</form>" & VbCrLf
	ElseIf Enabled = False Then
		Response.Write VbCrLf & "<img alt=""Checkout"" src=""" & ButtonSrc & """ height=""" & ButtonHeight & """ width=""" & ButtonWidth & """ />" & VbCrLf
	End If
End Sub


%>