<!--#include virtual="/cart/process/google/googleglobal.asp"-->
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/includes/asp/inc_Webloyalty.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
' Retrieve the XML received
Dim ResponseXml, biData, nIndex
biData = Request.BinaryRead(Request.TotalBytes)
For nIndex = 1 To LenB(biData)
	ResponseXml = ResponseXml & Chr(AscB(MidB(biData,nIndex,1)))
Next

LogMessage ResponseXml

Dim MyOrder
Set MyOrder = New Order

Dim ResponseNode, RootTag
Set ResponseNode = GetRootNode(ResponseXml)
RootTag = ResponseNode.Tagname

Select Case RootTag
	Case "new-order-notification"
		ProcessNewOrderNotification ResponseXml
	Case "order-state-change-notification"
		ProcessOrderStateChangeNotification ResponseXml
	Case Else
End Select 

' Handle a new-order-notifiation message
Sub ProcessNewOrderNotification(ResponseXml)
	CALL fOpenConn()
	
	Dim thisThing
	Dim SQL, RS
	
	Dim couponcode
	
	Dim MyNewOrder
	Set MyNewOrder = New NewOrderNotification
	MyNewOrder.ParseNotification ResponseXml
	
	SQL = "SELECT * FROM we_Orders WHERE store = 2 AND extOrderType = 2 AND extOrderNumber = '" & MyNewOrder.GoogleOrderNumber & "'"
	session("errorSQL") = SQL
	Set RS = oConn.execute(SQL)
	if RS.EOF then
		' Don't do anything if an order already exists in the WE database with this GoogleOrderNumber
		Dim BillingAddress
		Set BillingAddress = MyNewOrder.BuyerBillingAddress
		SQL = "SET NOCOUNT ON; "
		SQL = SQL & "INSERT INTO CO_Accounts (fname,lname,bAddress1,bAddress2,bCity,bState,bZip,bCountry,email,phone,sAddress1,sAddress2,sCity,sState,sZip,sCountry,hearFrom,dateEntered) VALUES ("
		Dim Fname, Lname, ContactNameArray, a
		ContactNameArray = split(BillingAddress.ContactName, " ")
		Fname = ContactNameArray(0)
		if Ubound(ContactNameArray) > 0 then
			for a = 1 to Ubound(ContactNameArray)
				if a > 1 then Lname = Lname & " "
				Lname = Lname & ContactNameArray(a)
			next
		end if
		SQL = SQL & "'" & SQLquote(Fname) & "', "
		SQL = SQL & "'" & SQLquote(Lname) & "', "
		SQL = SQL & "'" & SQLquote(BillingAddress.Address1) & "', "
		SQL = SQL & "'" & SQLquote(BillingAddress.Address2) & "', "
		SQL = SQL & "'" & SQLquote(BillingAddress.City) & "', "
		SQL = SQL & "'" & SQLquote(BillingAddress.Region) & "', "
		SQL = SQL & "'" & SQLquote(BillingAddress.PostalCode) & "', "
		SQL = SQL & "'" & SQLquote(BillingAddress.CountryCode) & "', "
		SQL = SQL & "'" & SQLquote(BillingAddress.Email) & "', "
		SQL = SQL & "'" & SQLquote(BillingAddress.Phone) & "', "
		
		Dim ShippingAddress
		Set ShippingAddress = MyNewOrder.BuyerShippingAddress
		DoSomethingWith ShippingAddress.ContactName
		SQL = SQL & "'" & SQLquote(ShippingAddress.Address1) & "', "
		SQL = SQL & "'" & SQLquote(ShippingAddress.Address2) & "', "
		SQL = SQL & "'" & SQLquote(ShippingAddress.City) & "', "
		SQL = SQL & "'" & SQLquote(ShippingAddress.Region) & "', "
		SQL = SQL & "'" & SQLquote(ShippingAddress.PostalCode) & "', "
		if len(SQLquote(ShippingAddress.Region)) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",SQLquote(ShippingAddress.Region)) > 0 then
			SQL = SQL & "'CANADA', "
		else
			SQL = SQL & "'', "
		end if
		
		SQL = SQL & "'Google Checkout', "
		SQL = SQL & "'" & now & "'); "
		SQL = SQL & "SELECT @@IDENTITY AS newAccountID; "
		SQL = SQL & "SET NOCOUNT OFF;"
		session("errorSQL") = SQL
		Set RS = oConn.execute(SQL)
		Dim newAccountID
		if not RS.eof then newAccountID = RS("newAccountID")
		
		if MyNewOrder.MarketingEmailAllowed = "true" then
			oConn.execute("sp_ModifyEmailOpt_CO '" & SQLquote(Fname) & "','" & SQLquote(BillingAddress.Email) & "','Y'")
		end if
		
		Set BillingAddress = Nothing
		Set ShippingAddress = Nothing
		
		DoSomethingWith MyNewOrder.TimeStamp
		DoSomethingWith MyNewOrder.BuyerId
		DoSomethingWith MyNewOrder.MerCalcSuccessful
		DoSomethingWith MyNewOrder.FulfillmentOrderState
		DoSomethingWith MyNewOrder.FinancialOrderState
		
		thisThing = thisThing & "<p>SQL = " & SQL & "</p>"
		
		SQL = "SET NOCOUNT ON; "
		SQL = SQL & "INSERT INTO we_Orders (store,accountid,extOrderType,extOrderNumber,ordersubtotal,ordershippingfee,orderTax,ordergrandtotal,shiptype,orderdatetime) VALUES ("
		SQL = SQL & "'2', "
		SQL = SQL & "'" & newAccountID & "', "
		SQL = SQL & "'2', "
		SQL = SQL & "'" & MyNewOrder.GoogleOrderNumber & "', "
		SQL = SQL & "'" & MyNewOrder.OrderTotal - MyNewOrder.AdjustmentTotal & "', "
		SQL = SQL & "'" & MyNewOrder.ShippingCost & "', "
		SQL = SQL & "'" & MyNewOrder.TaxTotal & "', "
		SQL = SQL & "'" & MyNewOrder.OrderTotal & "', "
		Dim shiptype
		if inStr(MyNewOrder.ShippingName,"Priority Int'l") > 0 then
			shiptype = "USPS Priority Int''l"
		elseif inStr(MyNewOrder.ShippingName,"Priority") > 0 then
			shiptype = "USPS Priority"
		elseif inStr(MyNewOrder.ShippingName,"Express") > 0 then
			shiptype = "USPS Express"
		elseif inStr(MyNewOrder.ShippingName,"First Class Int'l") > 0 then
			shiptype = "First Class Int''l"
		elseif inStr(MyNewOrder.ShippingName,"UPS Ground") > 0 then
			shiptype = "UPS Ground"
		elseif inStr(MyNewOrder.ShippingName,"UPS 3 Day Select") > 0 then
			shiptype = "UPS 3 Day Select"
		elseif inStr(MyNewOrder.ShippingName,"UPS 2nd Day Air") > 0 then
			shiptype = "UPS 2nd Day Air"
		else
			shiptype = "First Class"
		end if
		SQL = SQL & "'" & shiptype & "', "
		SQL = SQL & "'" & now & "'); "
		SQL = SQL & "SELECT @@IDENTITY AS newOrderID; "
		SQL = SQL & "SET NOCOUNT OFF;"
		session("errorSQL") = SQL
		set RS = oConn.execute(SQL)
		dim newOrderID
		if not RS.eof then newOrderID = RS("newOrderID")
		
		thisThing = thisThing & "<p>SQL = " & SQL & "</p>"
		
		'======save buysafe data to DB
		if isObject(MyNewOrder.MerchantPrivateData) then
			dim MyPrivateData2, BuySafeInfo
			set MyPrivateData2 = MyNewOrder.MerchantPrivateData
			BuySafeInfo = GetElementText(MyPrivateData2, "buysafeinfo")
			set MyPrivateData2 = nothing
			dim updateRS2
			BuySafeArray = split(trim(BuySafeInfo),",")
			BuySafeCartId = BuySafeArray(0)
			BuySafeAmount = BuySafeArray(1)
			WantsBondValue = BuySafeArray(2)
			if isNull(BuySafeAmount) or BuySafeAmount = "" then BuySafeAmount = 0
			SQL = "UPDATE we_orders SET"
			SQL = SQL & " ordersubtotal = '" & MyNewOrder.OrderTotal - MyNewOrder.AdjustmentTotal - BuySafeAmount & "', "
			SQL = SQL & " BuySafeCartId = '" & BuySafeCartId & "', "
			SQL = SQL & " BuySafeAmount = '" & BuySafeAmount & "'"
			SQL = SQL & " WHERE orderid = '" & newOrderID & "'"
			set updateRS2 = oConn.execute(SQL)
			thisThing = thisThing & "<p>SQL = " & SQL & "</p>"
		end if
		'=======end save buysafe to DB
		
		dim MyItem, strItems
		strItems = ""
		For Each MyItem In MyNewOrder.ItemArr
			DoSomethingWith MyItem.Name
			DoSomethingWith MyItem.Description
			DoSomethingWith MyItem.UnitPrice
			if MyItem.MerchantItemId <> "" then
				SQL = "INSERT INTO We_Orderdetails (orderid,itemid,quantity) VALUES ("
				SQL = SQL & "'" & newOrderID & "', "
				SQL = SQL & "'" & MyItem.MerchantItemId & "', "
				SQL = SQL & "'" & MyItem.Quantity & "')"
				oConn.execute(SQL)
				thisThing = thisThing & "<p>SQL = " & SQL & "</p>"
				strItems = strItems & MyItem.MerchantItemId & ","
				' START Update Inventory
				dim decreaseSql, nProdIdCheck, nPartNumber, KIT
				nProdIdCheck = MyItem.MerchantItemId
				' Updates inventory quantity for all items with the same Part Number as the item purchased
				' If the quantity would be adjusted to less than zero, then it is set at zero
				' The query only affects items whose initial quantity is greater than zero (no -1 quantity items are included)
				SQL = "SELECT PartNumber, ItemKit_NEW, (select inv_qty from we_items where partnumber = a.partnumber and master = 1) as inv_qty FROM we_items a WHERE itemID = '" & nProdIdCheck & "'"
				set RS = oConn.execute(SQL)
				KIT = null
				if not RS.eof then
					nPartNumber = RS("PartNumber")
					KIT = RS("ItemKit_NEW")
					decreaseSql = "UPDATE we_items SET inv_qty = CASE WHEN (inv_qty - " & MyItem.Quantity & " > 0) THEN inv_qty - " & MyItem.Quantity & " ELSE 0 END"
					if isNull(KIT) then
						decreaseSql = decreaseSql & " WHERE PartNumber = '" & nPartNumber & "' AND inv_qty > 0 AND inv_qty IS NOT NULL"
					else
						decreaseSql = decreaseSql & " WHERE itemID IN (" & KIT & ")"
					end if
					thisThing = thisThing & "<p>decreaseSql = " & decreaseSql & "</p>"
					oConn.execute(decreaseSql)
					On Error Resume Next
					if isNull(KIT) then
						sql = "if not (select count(*) from we_invRecord where itemID = " & nProdIdCheck & " and orderID = " & newOrderID & ") > 0 insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & nProdIdCheck & "," & RS("inv_qty") & "," & MyItem.Quantity & "," & newOrderID & ",0,'CO Google Customer Order','" & now & "')"
						session("errorSQL") = sql
						oConn.execute(sql)
					else
						sql = "select itemID, inv_qty from we_items where itemID IN (" & KIT & ")"
						session("errorSQL") = sql
						set	kitRS = oConn.execute(SQL)
						
						do while not kitRS.EOF
							sql = "if not (select count(*) from we_invRecord where itemID = " & kitRS("itemID") & " and orderID = " & newOrderID & ") > 0 insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & kitRS("itemID") & "," & kitRS("inv_qty") & "," & MyItem.Quantity & "," & newOrderID & ",0,'CO Google Customer Order','" & now & "')"
							session("errorSQL") = sql
							oConn.execute(sql)
							kitRS.movenext
						loop
					end if
					On Error Goto 0
				end if
				' END Update Inventory
				
				' START Update Number of Sales
				'SQL = "UPDATE we_Items SET numberOfSales = numberOfSales + " & MyItem.Quantity & " WHERE itemID = '" & nProdIdCheck & "'"
				if isNull(KIT) then
					sqlQuery = "UPDATE we_items SET numberOfSales = numberOfSales + " & MyItem.Quantity & " WHERE itemID = '" & nProdIdCheck & "'"
				else
					sqlQuery = "UPDATE we_items SET numberOfSales = numberOfSales + " & MyItem.Quantity & " WHERE itemID IN (" & KIT & ")"
				end if
				thisThing = thisThing & "<p>sqlQuery = " & sqlQuery & "</p>"
				oConn.Execute(SQL)
				' END Update Number of Sales
				
			elseif MyItem.Name = "DISCOUNT" then
				thisThing = thisThing & "<p>DISCOUNT = " & formatCurrency(MyItem.UnitPrice * -1) & "</p>"
				thisThing = thisThing & "<p>COUPON CODE = " & MyItem.Description & "</p>"
				couponcode = MyItem.Description
			end if
			
			If IsObject(MyItem.MerchantPrivateItemData) Then
				Dim MyPrivateItemData
				Set MyPrivateItemData = MyItem.MerchantPrivateItemData
				' Retrieve <color> and <weight> values
				DoSomethingWith GetElementText(MyPrivateItemData, "color")
				DoSomethingWith GetElementText(MyPrivateItemData, "weight")
				' Or get the raw <merchant-private-item-data> XML
				' DoSomethingWith MyPrivateItemData.xml
				Set MyPrivateItemData = Nothing
			End If
		Next
		
		if couponcode <> "" then
			SQL = "SELECT couponid FROM CO_coupons WHERE promoCode='" & couponcode & "'"
			Set RS = oConn.execute(SQL)
			if not RS.EOF then
				Dim couponid
				couponid = RS("couponid")
			end if
			SQL = "UPDATE We_Orders SET couponid='" & couponid & "' WHERE store = 2 AND extOrderType = 2 AND extOrderNumber = '" & MyNewOrder.GoogleOrderNumber & "'"
			oConn.execute(SQL)
		end if
		
		if isObject(MyNewOrder.MerchantPrivateData) then
			dim MyPrivateData, mySession, sGiftCert
			set MyPrivateData = MyNewOrder.MerchantPrivateData
			mySession = GetElementText(MyPrivateData, "session-id")
			sGiftCert = GetElementText(MyPrivateData, "cart-id")
			' update ShoppingCart table
			SQL = "UPDATE ShoppingCart SET purchasedOrderID = '" & newOrderID & "' WHERE store = 2 AND sessionID = '" & mySession & "' AND (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
			SQL = SQL & " AND itemID IN (" & left(strItems,len(strItems)-1) & ")"
			thisThing = thisThing & "<p>SQL = " & SQL & "</p>"
			session("errorSQL") = SQL
			oConn.execute SQL
			' update GiftCertificates table
			'if sGiftCert <> "" then
			'	SQL = "UPDATE GiftCertificates SET UsedOnOrderNumber = '" & newOrderID & "', dateUsed = '" & now & "' WHERE GiftCertificateCode = '" & sGiftCert & "'"
			'	thisThing = thisThing & "<p>SQL = " & SQL & "</p>"
			'	session("errorSQL") = SQL
			'	oConn.execute SQL
			'end if
			set MyPrivateData = nothing
		end if
		
		thisThing = thisThing & "<p>" & ResponseXml & "</p>"
		dim strTo,strFrom,strSubject,strBody
		strTo = "webmaster@cellularoutfitter.com"
		strFrom = "support@cellularoutfitter.com"
		strSubject = "CO processNewOrderNotification - Google"
		strBody = thisThing
		'CDOSend strTo,strFrom,strSubject,strBody
	end if
	call fCloseConn()
	'session.sessionid.abandon
End Sub

' Handle a order-state-change-notification message
Sub ProcessOrderStateChangeNotification(ResponseXml)
	CALL fOpenConn()
	
	Dim MyOrderState
	Set MyOrderState = New OrderStateChangeNotification
	MyOrderState.ParseNotification ResponseXml
	
	Dim strTo,strFrom,strSubject,strBody
	Dim thisThing
	
	Dim SQL, RS
	
	DoSomethingWith MyOrderState.TimeStamp
	DoSomethingWith MyOrderState.GoogleOrderNumber
	DoSomethingWith MyOrderState.NewFulfillmentOrderState
	DoSomethingWith MyOrderState.NewFinancialOrderState
	DoSomethingWith MyOrderState.PreviousFulfillmentOrderState
	DoSomethingWith MyOrderState.PreviousFinancialOrderState
	
	Dim GoogleOrderNumber, Amount, Reason, Comment
	Dim MerchantOrderNumber, Carrier, TrackingNumber, Mesage
	
	GoogleOrderNumber = MyOrderState.GoogleOrderNumber
	
	SQL = "SELECT * FROM we_Orders WHERE store = 2 AND extOrderType = 2 AND extOrderNumber = '" & GoogleOrderNumber & "' AND Approved=1"
	session("errorSQL") = SQL
	set RS = oConn.execute(SQL)
	if not RS.eof then
		if MyOrderState.NewFinancialOrderState = "CHARGED" then
			' MyOrder.AddTrackingData GoogleOrderNumber, Carrier, TrackingNumber
			' MyOrder.SendOrderCommand
			
			if MyOrderState.NewFulfillmentOrderState <> "DELIVERED" then
				MerchantOrderNumber = RS("orderID")
				MyOrder.AddMerchantOrderNumber GoogleOrderNumber, MerchantOrderNumber
				MyOrder.SendOrderCommand
				
				MyOrder.DeliverOrder GoogleOrderNumber, "USPS", "", False
				MyOrder.SendOrderCommand
				
				MyOrder.ArchiveOrder GoogleOrderNumber
				MyOrder.SendOrderCommand
				
				thisThing = "<p>" & ResponseXml & "</p>"
				strTo = "webmaster@cellularoutfitter.com"
				strFrom = "support@cellularoutfitter.com"
				strSubject = "CHARGED"
				strBody = thisThing
				'CDOSend strTo,strFrom,strSubject,strBody
			end if
		end if
	else
		if MyOrderState.NewFinancialOrderState = "CHARGEABLE" then
			' Don't do anything if an order already exists in the WE database with this GoogleOrderNumber and has been APPROVED
			' REVIEW THIS CODE AND TEST!!!
			SQL = "SELECT orderID, accountID, BuySafeAmount, BuySafeCartID FROM we_Orders WHERE store = 2 AND extOrderType = 2 AND extOrderNumber='" & GoogleOrderNumber & "'"
			' Is this the way we want to order them?
			SQL = SQL & " ORDER BY orderID DESC"
			set RS = Server.CreateObject("ADODB.Recordset")
			RS.open SQL, oConn, 3, 3
			if RS.recordcount > 0 then
				nOrderID = RS("orderID")
				nAccountId = RS("accountID")
				if not isNull(RS("BuySafeAmount")) then
					BuySafeAmount = RS("BuySafeAmount")
					ShoppingCartId = RS("BuySafeCartID")
				else
					BuySafeAmount = 0
					ShoppingCartId = null
				end if
				SQL = "UPDATE we_Orders SET Approved = -1 WHERE orderID = '" & nOrderID & "'"
				oConn.execute(SQL)
				
				' update ShoppingCart table
				'SQL = "UPDATE ShoppingCart SET purchasedOrderID = '" & RS("orderID") & "' WHERE sessionID = '" & mySession & "' AND (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
				'SQL = "UPDATE ShoppingCart SET purchasedOrderID = '" & RS("orderID") & "' WHERE purchasedOrderID IS NULL OR purchasedOrderID = 0"
				'SQL = SQL & " AND itemID IN (" & strItems & ")"
				'session("errorSQL") = SQL
				'oConn.execute SQL
				' update GiftCertificates table
				if sGiftCert <> "" then
					SQL = "UPDATE GiftCertificates SET UsedOnOrderNumber = '" & RS("orderID") & "', dateUsed = '" & now & "' WHERE GiftCertificateCode = '" & sGiftCert & "'"
					'oConn.execute SQL
				end if
				thisThing = thisThing & "<p>SQL = " & SQL & "</p>"
				
				thisThing = thisThing & "<p>CHARGEABLE!</p>"
				thisThing = thisThing & "<p>" & ResponseXml & "</p>"
				strTo = "webmaster@cellularoutfitter.com"
				strFrom = "support@cellularoutfitter.com"
				strSubject = "ProcessOrderStateChangeNotification"
				strBody = thisThing
				'CDOSend strTo,strFrom,strSubject,strBody
				
				dim thisEmail, incEmail
				incEmail = true
				
				'========BuySafe SetShoppingCartCheckOut 4/14/2010===========
				'if BuySafeAmount > 0 then
					dim BuySafeExtOrderType
					BuySafeExtOrderType = 2
					%>
					<!--#include virtual="/cart/BuySafe/SetShoppingCartCheckOut.asp"-->
					<%
				'end if
				'========End BuySafe SetShoppingCartCheckOut=======
				%>
				<!--#include virtual="/includes/asp/inc_receipt.asp"-->
				<%
				
				for thisEmail = 1 to 2
					if thisEmail = 1 then
						sFromName = "Automatic E-Mail from CellularOutfitter.com"
						sFromAddress = "sales@CellularOutfitter.com"
						sAddRecipient1 = sEmail
						sSubject = "Cellular Outfitter GOOGLE CHECKOUT Order Confirmation"
					else
						sFromName = "Automatic E-Mail from CellularOutfitter.com"
						sFromAddress = "sales@CellularOutfitter.com"
						sSubject = "A New GOOGLE CHECKOUT Order from Cellular Outfitter"
					end if
					cdo_from = sFromName & "<" & sFromAddress & ">"
					cdo_subject = sSubject
					cdo_body = ReceiptText
					on error resume next
						if thisEmail = 1 then
							' to Customer
							cdo_to = "" & sAddRecipient1 & ""
							'cdo_to = "webmaster@cellularoutfitter.com"
							CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
							if Err.Number <> 0 then
								session("mailError") = "<p><b>Your order is being fulfilled and shipped to the shipping address you provided.</b></p>"
								session("mailError") = session("mailError") & "<p>However, when attempting to e-mail your Order Confirmation to <b>" & cdo_to & "</b>, "
								session("mailError") = session("mailError") & "we received the following error from your e-mail provider:<br>" & Err.Description & "</p>"
								session("mailError") = session("mailError") & "<p>It is possible that you may not receive any e-mails from us (such as Shipping Confirmation, etc.) "
								session("mailError") = session("mailError") & "if we continue to encounter problems with this e-mail address.</p>"
								session("mailError") = session("mailError") & "<p>Please be assured, however, that your order will be fulfilled and shipped.</p>"
							else
								strSQL = "UPDATE we_orders SET emailSent = 'yes' WHERE orderid = '" & nOrderID & "'"
								oConn.execute strSQL
							end if
						else
							' to Cellular Outfitter recipients
							cdo_to = "shipping@wirelessemporium.com"
							'cdo_to = "webmaster@cellularoutfitter.com"
							CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
						end if
					on error goto 0
				next
				
				'CALL fSendConfirmEmail("sales",GoogleOrderNumber,MyOrderState.NewFinancialOrderState)
				'CALL fSendConfirmEmail("user",GoogleOrderNumber,MyOrderState.NewFinancialOrderState)
			end if
		end if
	end if
	call fCloseConn()
End Sub


'Set MyOrder = Nothing


' DoSomethingWith is a dummy function that demonstrates how you can access 
'     each variable from a notification/callback class once it's parsed through.
' Any calls to this function should be removed from the code.
Sub DoSomethingWith(Variable)
  ' Do nothing 
End Sub

function XMLencode(str)
	XMLencode = replace(replace(replace(replace(replace(str,"&","&amp;"),"<","&lt;"),">","&gt;"),"'","&apos;"),chr(34),"&quot;")
end function
%>
