<%
dim MyCart, MyItem
set MyCart = New Cart
%>

<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/cart/includes/inc_makeNum.asp"-->
<!--#include virtual="/includes/asp/inc_GetMySession.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<!--#include virtual="/cart/process/google/googleglobal.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->
<%
'GET COUPON VALUES
dim sPromoCode
if request.form("promocode") <> "" then
	sPromoCode = SQLquote(request.form("promocode"))
else
	sPromoCode = session("promo")
end if
if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	call fOpenConn()
	SQL = "SELECT * FROM CO_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 3, 3
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		'FreeItemPartNumber = RS("FreeItemPartNumber")
	else
		sPromoCode = ""
	end if
	call fCloseConn()
end if

dim sItemName, sRetailPrice, sItemPrice, sItemPic, nProdIdCheck, nProdQuantity, nProdIdCount
dim strItemCheck, WEhtml, PPhtml
sWeight = 0
strItemCheck = ""
call fOpenConn()
SQL = "SELECT c.brandName, d.modelName, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, B.itemDesc_CO, B.itemPic, B.price_CO, B.price_retail, B.Condition, B.itemWeight, B.NoDiscount"
SQL = SQL & " FROM ShoppingCart A INNER JOIN we_items B ON A.itemID=B.itemID left join we_brands c on b.brandID = c.brandID left join we_models d on b.modelID = d.modelID"
SQL = SQL & " WHERE A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
do until RS.eof
	brandName = RS("brandName")
	modelName = RS("modelName")
	PartNumber = RS("PartNumber")
	nProdIdCheck = RS("itemID")
	itemDesc = insertDetails(RS("itemDesc_CO"))
	nProdQuantity = RS("qty")
	strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		sItemPrice = RS("price_CO")
		set MyItem = New Item
		MyItem.Name = itemDesc
		MyItem.Description = itemDesc
		MyItem.Quantity = nProdQuantity
		MyItem.UnitPrice = sItemPrice
		MyItem.MerchantItemId = nProdIdCheck
		MyCart.AddItem MyItem
		set MyItem = nothing
		'Free Bluetooth
		if FreeBluetooth = 1 then
			if cStr(nProdIdCheck) = FreeBluetoothItemID then
				FreeBluetoothPrice = sItemPrice
			end if
		end if
		'Free Product With Phone Purchase
		if RS("typeID") = 16 then PhonePurchased = 1
		'Category Sale
		if CategorySale = 1 then
			if RS("typeID") = CategorySaleTypeID and RS("NoDiscount") = 0 then
				CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
			else
				SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
				SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_CO > 0)"
				SQL = SQL & " AND (R.typeid = '" & CategorySaleTypeID & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
				SQL = SQL & " AND I.NoDiscount = 0"
				dim RSrelated
				set RSrelated = Server.CreateObject("ADODB.Recordset")
				RSrelated.open SQL, oConn, 3, 3
				if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				RSrelated.close
				set RSrelated = nothing
			end if
		end if
		nItemTotal = nItemTotal + (sItemPrice * nProdQuantity)
		sWeight = sWeight + (cDbl(RS("itemWeight")) * nProdQuantity)
		nTotalQuantity = nTotalQuantity + RS("qty")
	end if
	RS.movenext
loop
if session("stmFreeItem") = 1 then
	nProdIdCount = nProdIdCount + 1
	sItemPrice = 0
	set MyItem = New Item
	MyItem.Name = stm_freeItemDesc
	MyItem.Description = stm_freeItemDesc
	MyItem.Quantity = 1
	MyItem.UnitPrice = 0
	MyItem.MerchantItemId = stm_freeItem
	MyCart.AddItem MyItem
	set MyItem = nothing
end if

RS.close
set RS = nothing
call fCloseConn()

dim nSubTotal
nSubTotal = nItemTotal

if sWeight / 16 < 1 then
	sWeight = 1
else
	sWeight = sWeight / 16
end if

if strItemCheck <> "" then strItemCheck = left(strItemCheck,len(strItemCheck)-1)

if nProdIdCount = 0 then
	response.write "<h2>Your session has expired. <a href=""/"">Click here</a> to start again.</h2>"
	response.end
end if

%>
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<%

if discountTotal > 0 then
	set MyItem = New Item
	MyItem.Name = "DISCOUNT"
	MyItem.Description = sPromoCode
	MyItem.Quantity = 1
	MyItem.UnitPrice = discountTotal * -1
	MyCart.AddItem MyItem
	set MyItem = nothing
end if

if nGiftCertAmount > 0 then
	set MyItem = New Item
	MyItem.Name = "Gift Certificate"
	MyItem.Description = GiftCertificateCode
	MyItem.Quantity = 1
	MyItem.UnitPrice = nGiftCertAmount * -1
	MyCart.AddItem MyItem
	set MyItem = nothing
end if

nSubTotal = nItemTotal

' Set optional parameters
' Specify an expiration date for the order and build <shopping-cart>
Dim myExpDate, myMonth, myDay
myMonth = cStr(month(date))
if month(date) < 10 then myMonth = "0" & myMonth
myDay = cStr(day(date))
if day(date) < 10 then myDay = "0" & myDay
myExpDate = cStr(year(date) + 1) & "-" & myMonth & "-" & myDay & "T23:59:59"

' BuySafe data
dim buysafeinfo, buysafeamount
buysafeinfo = request.form("buysafecartID")
buysafeamount = request.form("buysafeamount")
if isNull(BuySafeAmount) or BuySafeAmount = "" then BuySafeAmount = 0
buysafeinfo = buysafeinfo & "," & buysafeamount & "," & request.form("WantsBondField")
on error resume next
	'clear buySAFE ShoppingCartId
	Response.Cookies("WEbuySAFE")("ShoppingCartId") = ""
on error goto 0

MyCart.CartExpiration = myExpDate
MyCart.MerchantPrivateData = "<session-id>" & mySession & "</session-id><cart-id>" & sGiftCert & "</cart-id><buysafeinfo>" & buysafeinfo & "</buysafeinfo>"
MyCart.EditCartUrl = "http://www.cellularoutfitter.com/cart/basket.asp"
MyCart.ContinueShoppingUrl = "http://www.cellularoutfitter.com/cart/process/google/continue.asp"
MyCart.RequestBuyerPhoneNumber = True
MyCart.RequestInitialAuthDetails = True

' Add shipping methods
' Note: You cannot mix merchant-calculated-shipping methods with other shipping methods (flat-rate-shipping or pickup) in the same cart.
Dim MyShipping

' Add a flat-rate-shipping method
Set MyShipping = New FlatRateShipping
MyShipping.Name = "First Class"
'MyShipping.Price = 5.99
MyShipping.Price = 4.00 + (1.99 * nTotalQuantity)
MyShipping.AllowedCountryArea = "ALL"
MyCart.AddShipping MyShipping
Set MyShipping = Nothing

' Add a flat-rate-shipping method
Set MyShipping = New FlatRateShipping
MyShipping.Name = "USPS Priority Mail (2-3 days)"
'MyShipping.Price = 9.99
MyShipping.Price = 8.00 + (1.99 * nTotalQuantity)
MyShipping.AllowedCountryArea = "ALL"
MyCart.AddShipping MyShipping
Set MyShipping = Nothing

' Add a flat-rate-shipping method
Set MyShipping = New FlatRateShipping
MyShipping.Name = "USPS Express Mail (1-2 days)"
'MyShipping.Price = 22.99
MyShipping.Price = 21.00 + (1.99 * nTotalQuantity)
MyShipping.AllowedCountryArea = "ALL"
MyCart.AddShipping MyShipping
Set MyShipping = Nothing


'*** UNCOMMENTED THESE - CHECK TO SEE IF INTERNATIONAL SHIPPING IS BEING ALLOWED IN SETTINGS RIGHT NOW ***
' Add an international flat-rate-shipping method
'Set MyShipping = New FlatRateShipping
'MyShipping.Name = "USPS First Class Int'l (8-12 business days)"
'MyShipping.Price = 5.00 + (2.99 * nTotalQuantity)
'MyShipping.AllowedCountryArea = "CA"
'MyCart.AddShipping MyShipping
'Set MyShipping = Nothing

' Add an international flat-rate-shipping method
'Set MyShipping = New FlatRateShipping
'MyShipping.Name = "USPS Priority Int'l (3-8 business days)"
'MyShipping.Price = 12.00 + (2.99 * nTotalQuantity)
'MyShipping.AllowedCountryArea = "CA"
'MyCart.AddShipping MyShipping
'Set MyShipping = Nothing
'*********************************************************************************************************


'*** NEW UPS SHIPPING ***
dim sZip, sWeight, thisUPS, a
sZip = request.form("shipZip")
sWeight = request.form("sWeight")
thisUPS = UPScode(sZip,sWeight,shiptype,nTotalQuantity,0,1)
if thisUPS <> "" then
	thisUPS = left(thisUPS,len(thisUPS)-1)
	UPSarray = split(thisUPS,"|")
	for a = 0 to uBound(UPSarray)-1 step 2
		' Add a flat-rate-shipping method
		Set MyShipping = New FlatRateShipping
		MyShipping.Name = UPSarray(a)
		MyShipping.Price = UPSarray(a + 1)
		MyShipping.AllowedCountryArea = "ALL"
		MyCart.AddShipping MyShipping
		Set MyShipping = Nothing
	next
end if
'************************


' Define default tax rules
Dim MyDefaultTaxRule

Set MyDefaultTaxRule = New DefaultTaxRule
MyDefaultTaxRule.Rate = Application("taxMath")
MyDefaultTaxRule.States = Array("CA")
MyCart.AddDefaultTaxRule(MyDefaultTaxRule)
Set MyDefaultTaxRule = Nothing

Set MyDefaultTaxRule = New DefaultTaxRule
MyDefaultTaxRule.Rate = 0
MyDefaultTaxRule.CountryArea = "ALL"
MyCart.AddDefaultTaxRule(MyDefaultTaxRule)
Set MyDefaultTaxRule = Nothing

' Add buysafebond 4/14/2010
if request.form("WantsBondField") = "true" then
	set MyItem = New Item
	MyItem.Name = "buySAFE Bond"
	MyItem.Description = "buySAFE Bond " & formatCurrency(buysafeamount)
	MyItem.Quantity = 1
	MyItem.UnitPrice = buysafeamount
	MyItem.TaxTableSelector = "nontaxitem"
	MyCart.AddItem MyItem
end if
' End buysafebond

' Add parameterized-urls for tracking pixels
Dim MyURLS

' Add a parameterized URL for Google AdWords
Set MyURLS = New ParameterizedUrls
MyURLS.url = server.htmlencode("https://www.googleadservices.com/pagead/conversion/1061947743/imp.gif?label=purchase&script=0")
MyURLS.total = "value"
MyCart.AddParameterizedUrl MyURLS
Set MyURLS = Nothing

' Display <checkout-shopping-cart> XML
'Response.Write MyCart.GetXml
'response.end

' Diagnose <checkout-shopping-cart> XML
' MyCart.DiagnoseXml

'on error resume next
'	'clear buySAFE ShoppingCartId
'	Response.Cookies("CObuySAFE")("ShoppingCartId") = ""
'on error goto 0

' Post Cart to Google Checkout 
MyCart.PostCartToGoogle

Set MyCart = Nothing
%>
