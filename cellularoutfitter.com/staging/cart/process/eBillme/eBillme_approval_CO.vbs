dim oConn
set oConn = CreateObject("ADODB.Connection")
oConn.ConnectionString = "DSN=WEDB01;Uid=wireless_user;Pwd=c3llph0n3;"
oConn.ConnectionTimeout = 1000		'seconds
oConn.Open

const SEB_PAYEE = "VPvNJXzrKmuB05GpkSAT4g=="
const SEB_USERNAME = "p0u#Ce11@dM1n"
const SEB_PASSWORD = "z3#G67YZe2fE"
dim seb, r, REFERENCE_ID, extOrderNumber
set seb = CreateObject("SeB.SeBImpl")
r = seb.getPaymentDetails(SEB_PAYEE, SEB_USERNAME, SEB_PASSWORD)

Set xmlDoc = CreateObject("Msxml2.DOMDocument.3.0")
xmlDoc.async = false
xmlDoc.loadXml r

Set RootNode = xmlDoc.documentElement

Set PaymentNode = RootNode.selectNodes("collection/payment")
for a = 0 to PaymentNode.Length - 1
	nOrderId = GetElementText(PaymentNode(a),"ordernumber")
	PaymentStatus = GetElementText(PaymentNode(a),"paymentstatus")
	bEmail = GetElementText(PaymentNode(a),"buyerdetails/email")
	if PaymentStatus = "F" then
		SQL = "SELECT orderid FROM we_orders WHERE orderid='" & nOrderId & "'"
		Set RS = CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 3, 3
		if not RS.eof then
			SQL = "UPDATE we_orders SET approved=-1, emailSent='yes' WHERE orderid='" & nOrderId & "'"
			oConn.execute SQL
			
			SQL = "SELECT * FROM we_orderdetails WHERE orderid='" & nOrderId & "'"
			Set RSinv = CreateObject("ADODB.Recordset")
			RSinv.open SQL, oConn, 3, 3
			do until RSinv.eof
				' START Update Inventory
				dim decreaseSql, nProdIdCheck, nProdQuantity, nPartNumber, KIT
				nProdIdCheck = RSinv("itemid")
				nProdQuantity = RSinv("quantity")
				' Updates inventory quantity for all items with the same Part Number as the item purchased
				' If the quantity would be adjusted to less than zero, then it is set at zero
				' The query only affects items whose initial quantity is greater than zero (no -1 quantity items are included)
				SQL = "SELECT PartNumber, ItemKit_NEW, (select inv_qty from we_items where partnumber = a.partnumber and master = 1) as inv_qty FROM we_items a WHERE itemID = '" & nProdIdCheck & "'"
				set RS = oConn.execute(SQL)
				KIT = null
				if not RS.eof then
					nPartNumber = RS("PartNumber")
					KIT = RS("ItemKit_NEW")
					decreaseSql = "UPDATE we_items SET inv_qty = CASE WHEN (inv_qty - " & nProdQuantity & " > 0) THEN inv_qty - " & nProdQuantity & " ELSE 0 END"
					if isNull(KIT) then
						decreaseSql = decreaseSql & " WHERE PartNumber = '" & nPartNumber & "' AND inv_qty > 0 AND inv_qty IS NOT NULL"
					else
						decreaseSql = decreaseSql & " WHERE itemID IN (" & KIT & ")"
					end if
					oConn.execute(decreaseSql)
					On Error Resume Next
					if isNull(KIT) then
						sql = "if not (select count(*) from we_invRecord where itemID = " & nProdIdCheck & " and orderID = " & nOrderId & ") > 0 insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & nProdIdCheck & "," & RS("inv_qty") & "," & nProdQuantity & "," & nOrderId & ",0,'CO eBillMe Customer Order','" & now & "')"
						session("errorSQL") = sql
						oConn.execute(sql)
					else
						sql = "select itemID, inv_qty from we_items where itemID IN (" & KIT & ")"
						session("errorSQL") = sql
						set	kitRS = oConn.execute(SQL)
						
						do while not kitRS.EOF
							sql = "if not (select count(*) from we_invRecord where itemID = " & kitRS("itemID") & " and orderID = " & nOrderId & ") > 0 insert into we_invRecord (itemID,inv_qty,orderQty,orderID,adminID,notes,editDate) values(" & kitRS("itemID") & "," & kitRS("inv_qty") & "," & nProdQuantity & "," & nOrderId & ",0,'CO eBillMe Customer Order','" & now & "')"
							session("errorSQL") = sql
							oConn.execute(sql)
							kitRS.movenext
						loop
					end if
				end if
				' END Update Inventory
				
				' START Update Number of Sales
				'SQL = "UPDATE we_Items SET numberOfSales = numberOfSales + " & nProdQuantity & " WHERE itemID = '" & nProdIdCheck & "'"
				if isNull(KIT) then
					sqlQuery = "UPDATE we_items SET numberOfSales = numberOfSales + " & nProdQuantity & " WHERE itemID = '" & nProdIdCheck & "'"
				else
					sqlQuery = "UPDATE we_items SET numberOfSales = numberOfSales + " & nProdQuantity & " WHERE itemID IN (" & KIT & ")"
				end if
				oConn.Execute(SQL)
				' END Update Number of Sales
				RSinv.movenext
			loop
		else
			cdo_body = "CO Order Number " & nOrderId & " not found for eBillme order!"
			cdo_to = "webmaster@cellularoutfitter.com"
			cdo_from = "support@cellularoutfitter.com"
			cdo_subject = "eBillme Order Not Found!"
			CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
		end if
	end if
next

set seb = nothing

function GetElementText(Node, Tagname)
	Dim NodeList
	Set NodeList = Node.getElementsByTagname(Tagname)
	If NodeList.Length > 0 Then
		For b = 0 To NodeList.Length - 1
			Set CurrNode = NodeList.nextNode
			GetElementText = CurrNode.text
		Next
	Else
		GetElementText = ""
	End If
	Set NodeList = Nothing
end function

function CDOSend(strTo,strFrom,strSubject,strBody)
	Dim objErrMail
	Set objErrMail = CreateObject("CDO.Message")
	With objErrMail
		.From = strFrom
		.To = strTo
		'.bcc = "webmaster@cellularoutfitter.com"
		.Subject = strSubject
		.HTMLBody = CStr("" & strBody)
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Update
		.Send
	End With
	Set objErrMail = nothing
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "covers-screen-guards"
		case "bling-kits---charms" : formatSEO = "bling-kits-charms"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "hands-free-kits-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO = "holsters-car-mounts"
		case "leather-cases" : formatSEO = "cases-pouches"
		case "cell-phones" : formatSEO = "wholesale-cell-phone"
		case "full-body-protectors" : formatSEO = "invisible-film-protectors"
	end select
end function
