<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_Webloyalty.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%

dim UserIPAddress
	UserIPAddress = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
if UserIPAddress = "" then
  UserIPAddress = Request.ServerVariables("REMOTE_ADDR")
end if
dim isInternalUser : isInternalUser = false

if UserIpAddress = "66.159.49.66" then
	isInternalUser = true
end if

dim nAccountID, nOrderID, nOrderAmount, nOrderTotal
nAccountID = request.querystring("a")
nOrderID = request.querystring("o")
'nOrderAmount = request.querystring("d")
'nOrderGrandTotal = request.querystring("ppd")	'
nOrderTotal = 0
usePostPurchase = request.querystring("pp")
reSellerID = 18705

'set known product attributes retrieve appropriate content events
dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
dim dicContentEventText: set dicContentEventText = GetContentEventText( dicSeoAttributeInput)

dim RS, SQL
SQL = "SELECT orderGrandTotal, isnull(pp_orderGrandTotal, 0) pp_orderGrandTotal, extOrderType, transactionID from we_orders with (nolock) where orderid = '" & nOrderID & "' AND accountid = '" & nAccountID & "'"
SQL = SQL & " AND (approved = 1 OR extOrderType = 3)"
set RS = oConn.execute(SQL)
if RS.eof then
	call fCloseConn()
	response.redirect("http://www.cellularoutfitter.com/")
	response.end
elseif RS("extOrderType") = 3 then
	' Go ahead and display the confirmation page even though the order has not been approved by eBillme.
end if

nOrderAmount = rs("orderGrandTotal")
nOrderGrandTotal = rs("pp_orderGrandTotal")
extOrderType = rs("extOrderType")
auth_transactionid = rs("transactionID")

weDataString = ""

weDataString = weDataString & "window.WEDATA = { pageType: 'orderConfirmation', internalUser: '"& LCase(isInternalUser) &"', storName: 'WirelessEmporium.com', orderData: { cartItems: []	}, account: {} };" & vbcrlf

%>
<!--#include virtual="/includes/asp/inc_receipt_new.asp"-->
<%
dim accountMasterId : accountMasterId = nAccountId

' look for the parentId of the current account
sql = "select parentId from co_accounts " &_
		"where email = '" & sEmail & "' " &_
		"and accountId = " & nAccountId & " " &_
		"order by dateEntered " 'dateEntered just in case there are multiple records returned... which should NEVER happen
		'TODO: Check for more than 1 record returned and handle with an email to developers with debug information
		set rs = oConn.execute(sql)
		if (not rs.eof or rs.RecordCount > 0) and not isNull(rs("parentId")) then accountMasterId = rs("parentId")
		
Response.Cookies("user")("id") = accountMasterId
Response.Cookies("user")("email") = sEmail
Response.Cookies("user").Expires = DateAdd("yyyy",5,Now())


weDataString = weDataString & "window.WEDATA.account.email = '"& sEmail & "';" & vbcrlf
weDataString = weDataString & "window.WEDATA.account.id = '"& accountMasterId & "';" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.orderNumber = '"& nOrderId & "';" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.accountId = '"& nAccountId & "';" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.totalDiscount = '"& totalDiscount & "';" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.taxAmount = '"& emailOrderTax & "';" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.shippingAmount = '"& emailShipFee & "';" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.grandTotal = '"& emailOrderGrandTotal & "';" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.subTotal = '"& subtotal & "';" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.couponId = '"& couponId & "';" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.promoCode = '"& spromoCode & "';" & vbcrlf
weDataString = weDataString & "window.WEDATA.orderData.shipping = {state: '"& sstate & "', city: '"& sCity &"', zip: '"& sZip &"'};" & vbcrlf

satelliteTag = "<script src='//assets.adobedtm.com/d0f694a37a1061afe4e18245479cdeaed180db2f/satelliteLib-6c61417c205e2f84dfcdbf654877e85bba16c2d8.js'></script>"

if instr(request.ServerVariables("SERVER_NAME"), "staging") > 0  then
	satelliteTag = "<script src='//assets.adobedtm.com/d0f694a37a1061afe4e18245479cdeaed180db2f/satelliteLib-6c61417c205e2f84dfcdbf654877e85bba16c2d8-staging.js'></script>"
end if

ReceiptText = replace(ReceiptText, "[WEDATA]", "<script>"&weDataString&"</script>")
ReceiptText = replace(ReceiptText, "[SATELLITE TAG]", satelliteTag)


response.write ReceiptText
'session.abandon

dim vendororder
on error resume next
vendororder = Request.Cookies("vendororder")
on error goto 0
' If this is an Affiliate Referral from Nextag, add the Survey code
if vendororder = "nextag" then
	response.write "<link rel=""stylesheet"" href=""https://merchants.nextag.com/serv/main/buyer/dhtmlpopup/dhtmlwindow.css"" type=""text/css"" />" & vbcrlf
	response.write "<script type=""text/javascript"">" & vbcrlf
	response.write "var seller_id = 3380399;" & vbcrlf
	response.write "//popup_left and popup_top default to the center of browser window if commented" & vbcrlf
	response.write "//var popup_left=50;" & vbcrlf
	response.write "//var popup_top=50;" & vbcrlf
	response.write "//var popup_width=345; // default 345" & vbcrlf
	response.write "//var popup_height=205; // default 205" & vbcrlf
	response.write "//var popup_resize=0; // default 0" & vbcrlf
	response.write "document.write('<'+ 'script type=""text/javascript"" src=""https://merchants.nextag.com/seller/review/popup_include.js""><\/script>'); </script>" & vbcrlf
else
'	response.write "<!-- Shopping.com survey -->"
'	response.write "<script type=""text/javascript"" language=""JavaScript"" src=""https://www.shopping.com/xMSJ?pt=js&mid=429701&lid=1"">"
'	response.write "</" & "script>"

'	response.write "<!-- reseller rating -->" & vbcrlf
'	response.write "<link rel=""stylesheet"" href=""https://www.resellerratings.com/images/js/dhtml_survey.css"" type=""text/css"" />"
'	response.write "<script type=""text/javascript"">" & vbcrlf
'	response.write "(function(w) {" & vbcrlf
'	response.write "	w.seller_id =  18705;" & vbcrlf
'	response.write "	// Ensure that the value is quoted, please pass the customer's email address " & vbcrlf
'	response.write "	w.__rr_email_pass = """ & sEmail & """; " & vbcrlf
'	response.write "	var scr = document.createElement(" & chr(39) & "script" & chr(39) & ");" & vbcrlf
'	response.write "	scr.type = " & chr(39) & "text/javascript" & chr(39) & ";" & vbcrlf
'	response.write "	scr.async = true;" & vbcrlf
'	response.write "	scr.src = ""https://www.resellerratings.com/popup/include/popup.js"";" & vbcrlf
'	response.write "	var s = document.getElementsByTagName(" & chr(39) & "script" & chr(39) & ")[0];" & vbcrlf
'	response.write "	s.parentNode.insertBefore(scr, s);" & vbcrlf
'	response.write "})(window);" & vbcrlf
'	response.write "</" & "script>"
end if

'zero-out all cart cookies
' START basket grab
dim basketitem
for basketitem = 1 to 100
	if Request.Cookies("itemID(" & basketitem & ")") <> "" and Request.Cookies("itemID(" & basketitem & ")") <> "0" then
		Response.Cookies("itemID(" & basketitem & ")") = 0
		Response.Cookies("itemQty(" & basketitem & ")") = 0
		Response.Cookies("relatedID(" & basketitem & ")") = 0
	end if
next
' END basket grab

call OutputPixel( dicContentEventText) '--thank you pixel goes here

set dicSeoAttributeInput = nothing
set dicContentEventText = nothing
%>
<script src="//code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="/framework/userinterface/js/ajax.js" ></script>
<script type="text/javascript" src="//bizsolutions.strands.com/sbsstatic/js/sbsLib-1.0.min.js"></script>

<%if usePostPurchase = "Y" and (prepInt(extOrderType) = 0 or prepInt(extOrderType) = 1) then%>
	<script>
        window.onload = function() { 
			if ($('#ppWidgetControl').css('display') == 'none') {
				pp_strands();
			} else {
				pp_doPopup();
			}
        }

		function getRatingAvgStar(rating) {
			var nRating = rating;
			var strRatingImg = "<img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> <img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> <img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> <img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> <img src='/images/review/grayStar.gif' border='0' width='14' height='13' />";

			if (!isNaN(nRating)) {
				if ((parseFloat(nRating) > 0)||(parseFloat(nRating) <=5)) {
					strRatingImg = "";
					for (nCnt=1; nCnt<=5; nCnt++) {
						if (parseFloat(nRating) >= parseFloat(nCnt)) {
							strRatingImg = strRatingImg + "<img src='/images/review/greenStarFull.gif' border='0' width='14' height='13' /> ";
						} else if (parseFloat(nRating) >= parseFloat((nCnt - 1) + 0.1)) {
							strRatingImg = strRatingImg + "<img src='/images/review/greenStarHalf.gif' border='0' width='8' height='13' /> ";
						} else {
							strRatingImg = strRatingImg + "<img src='/images/review/grayStar.gif' border='0' width='14' height='13' /> ";
						}
					}
				}
			}

			return strRatingImg;
		}

		
		function strands_rendering(rec_info) {
			var usePromoPercent = document.getElementById('usePromoPercent').innerHTML;
			var rec = rec_info.recommendations;
			var content = "";
			var lap = 0;
			for(i=0;i<=rec.length-1;i++){
//				alert(JSON.stringify(rec[i]));
				var price_retail = rec[i]["metadata"]["price"];
				var price_co = rec[i]["metadata"]["properties"]["saleprice"];
				var itemDesc = rec[i]["metadata"]["name"];
				if (itemDesc.length > 65) itemDesc = itemDesc.substring(0,61) + '...';
				
				if (!isNaN(price_retail) && !isNaN(price_co)) {
					lap = i + 1;
					price_co = CurrencyFormatted(price_co * ((100-usePromoPercent)/100));
					var savePercent = Math.ceil((price_retail - price_co) / price_retail * 100.0);
					content = content + "<div style='float:left; width:167px; padding:30px 5px 0px 0px;'>";
					content = content + "	<div style='float:left; width:100%; text-align:center;'><img src='" + rec[i]["metadata"]["picture"] + "' border='0' /></div>";
					content = content + "	<div style='float:left; width:162px; margin-top:5px; padding:5px 0px 5px 0px; background-color:#eaeff5; border-radius:5px;'>";
					content = content + "		<div style='float:left;'><input id='id_chkItem" + lap + "' type='checkbox' name='chkItem" + lap + "' value='" + rec[i]["itemId"] + "' onclick='updatePPTotal();' /></div>";
					content = content + "		<div style='float:left; padding:2px 0px 0px 5px; color:#444; font-weight:bold; font-size:11px;'><label style='cursor:pointer;' for='id_chkItem" + lap + "'>Add This To My Order</label></div>";
					content = content + "	</div>";
					content = content + "	<div style='float:left; width:100%; padding-top:5px; color:#333; font-weight:bold; font-size:12px;'><input id='id_qty" + lap + "' type='text' name='chkQty" + lap + "' maxlength='3' value='1' style='width:40px; height:25px; padding:5px; color:#333; text-align:center; border-radius:3px; border:1px solid #ccc; -moz-box-shadow: inset 0 0 1px 1px #eee; -webkit-box-shadow: inset 0 0 1px 1px #eee; box-shadow: inset 0 0 1px 1px #eee;' onkeyup='updatePPTotal();' /> &nbsp; Quantity</div>";
					content = content + "	<div style='float:left; width:100%; padding-top:5px; font-size:12px; color:#333; height:45px;' title='" + rec[i]["metadata"]["name"] + "'>" + itemDesc + "</div>";
					content = content + "	<div style='float:left; width:100%; padding-top:5px;'>" + getRatingAvgStar(rec[i]["rating"]) + "</div>";
					content = content + "	<div style='float:left; width:100%; padding-top:5px; color:#333; font-size:11px; text-decoration:line-through;'>$" + price_retail + "</div>";
					content = content + "	<div style='float:left; width:100%; padding-top:5px; color:#336699; font-weight:bold; font-size:13px;'>";
					content = content + "		$" + price_co + "&nbsp;";
					content = content + "		(Save " + savePercent + "%)";
					content = content + "	</div>";
					content = content + "	<input id='id_itemPrice" + lap + "' type='hidden' name='itemPrice" + lap + "' value='" + price_co + "' />";
					content = content + "	<input id='id_itemRetail" + lap + "' type='hidden' name='itemRetail" + lap + "' value='" + price_retail + "' />";
					content = content + "</div>";
				}
			}
			document.frmPostPurchase.hidCount.value = lap;
			$(".strandsRecs").html(content);
		}
		
		function pp_strands() {
            document.getElementById("popCover").style.display = "";
            document.getElementById("popBox").style.display = "";
            document.getElementById("popBox").innerHTML = '';
            ajax('/ajax/postPurchase.asp?itemids=<%=itemids%>&orderid=<%=nOrderID%>&transid=<%=auth_transactionid%>&pType=strands&extordertype=<%=prepInt(extOrderType)%>','popBox');
			checkUpdate();
		}
				
		function checkUpdate() {
			if (document.getElementById("popBox").innerHTML == "") {
				setTimeout('checkUpdate()', 300);
			} else {
				try{ 
					SBS.Recs.setRenderer(strands_rendering, "misc_2");
					SBS.Worker.go("Kx7AmRMCrW"); 
				} catch (e){}
			}
		}
    
        function doSubmit() {
            frm = document.frmPostPurchase;
            if (CurrencyFormatted(frm.addlGrandTotal.value) == "0.00") {
                alert("Plesae select the items");
                return false;
            }
            document.getElementById('pp_bottomBox').style.display = 'none';
            document.getElementById('pp_bottomBoxText').style.display = '';
            return true;
        }
        
        function pp_closePopup() {
            document.getElementById("popCover").style.display = "none";
            document.getElementById("popBox").style.display = "none";
            document.getElementById("popBox").innerHTML = "";

			resellerRating(window);
        }
        
        function pp_doPopup() {
            document.getElementById("popCover").style.display = "";
            document.getElementById("popBox").style.display = "";
            document.getElementById("popBox").innerHTML = '<div style="width:700px; background-color:#fff; margin-left:auto; margin-right:auto; margin-top:100px;"><img src="/images/checkout/loading4.gif" border="0" /></div>';
            ajax('/ajax/postPurchase.asp?itemids=<%=itemids%>&orderid=<%=nOrderID%>&transid=<%=auth_transactionid%>&extordertype=<%=prepInt(extOrderType)%>','popBox');
        }
        
        function updatePPTotal() {
            frm = document.frmPostPurchase;
            nCount = frm.hidCount.value;
            taxTotal = frm.taxTotal.value;
            
            var addlTotal = parseFloat(0);
            var addlTax = parseFloat(0);
            var addlShipping = parseFloat(0);
            var itemTotal = parseFloat(0);
            var retailTotal = parseFloat(0);
            var itemSave = parseFloat(0);
            var qtyTotal = parseFloat(0);
    
            for (i=1; i<= nCount; i++) {
                itemChecked = document.getElementById('id_chkItem'+i).checked;
                price_co = document.getElementById('id_itemPrice'+i).value;
                price_retail = document.getElementById('id_itemRetail'+i).value;
                qty = document.getElementById('id_qty'+i).value;			
                
                if (itemChecked) {
                    if ((!isNaN(qty))&&(qty != '')&&(qty != 0)) {		
                        itemTotal = parseFloat(itemTotal) + parseFloat(price_co * qty);
                        retailTotal = parseFloat(retailTotal) + parseFloat(price_retail * qty);
                        itemSave = parseFloat(itemSave) + (parseFloat(retailTotal) - parseFloat(itemTotal));
                        qtyTotal = parseInt(qtyTotal) + parseInt(qty);
                    }
                }
            }
            
            if (parseFloat(taxTotal) > 0) {
                addlTax = parseFloat(itemTotal * <%=Application("taxMath")%>);
            }
            
            addlShipping = parseFloat(qtyTotal * 1.99);
            addlTotal = parseFloat(itemTotal) + parseFloat(addlShipping) + parseFloat(addlTax);
    
            document.getElementById('addlSave').innerHTML = CurrencyFormatted(itemSave);
            document.getElementById('addlTotal').innerHTML = CurrencyFormatted(itemTotal);
            document.getElementById('addlTaxTotal').innerHTML = CurrencyFormatted(addlTax);
            document.getElementById('addlGrandTotal').innerHTML = CurrencyFormatted(addlTotal);
            
            frm.addlGrandTotal.value = CurrencyFormatted(addlTotal);
        }
        
        function CurrencyFormatted(amount) {
            var i = parseFloat(amount);
            if(isNaN(i)) { i = 0.00; }
            var minus = '';
            if(i < 0) { minus = '-'; }
            i = Math.abs(i);
            i = Math.round(i * 100) / 100;
            s = new String(i);
            if(s.indexOf('.') < 0) { s += '.00'; }
            if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
            s = minus + s;
            return s;
        }		
    </script>
<%else%>
	<script type="text/javascript">
		setTimeout("resellerRating(window)",50);
	</script>
<%end if%>

<script>
	function resellerRating(w) {
	    __rr_autoEnroll = false;
        w.seller_id = <%=reSellerID%>;
        w.__rr_email_pass = "<%=sEmail%>"; // Ensure that the value is quoted
        w.__rr_inv = "<%=nOrderID%>"; // It is recommended to quote the invoice number
        var scr = document.createElement('script');
        scr.type = 'text/javascript';
        scr.async = true;
        scr.src = "//www.resellerratings.com/popup/include/popup.js";
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(scr, s);
	}
</script>
<!-- Begin Curebit integration code -->
<script type="text/javascript" src="https://www.curebit.com/public/cellular-outfitter/purchases/create.js?v=0.2&p[order_number]=<%=nOrderID%>&p[subtotal]=<%=nOrderSubTotal%>&p[customer_email]=<%=replace(sEmail,"@","%40")%>&p[order_date]=<%=year(date)%>-<%=month(date)%>-<%=day(date)%>%252003%3A18%3A08<%=cureBitProducts%>" ></script>
<!-- End Curebit integration code -->
<%
on error resume next
sql = 	"select	isnull(a.cancelled, 0) cancelled, b.bAddress1" & vbcrlf & _
		"from	we_orders a join v_accounts b" & vbcrlf & _
		"	on	a.accountid = b.accountid and a.store = b.site_id" & vbcrlf & _
		"where	a.orderID = '" & nOrderID & "' and a.store = 2"
set rsGoogle = oConn.execute(sql)

if not rsGoogle.eof then
	if (instr(request.ServerVariables("SERVER_NAME"), "staging.") = 0 and rsGoogle("bAddress1") <> "4040 N. Palm St.") or prepInt(request.querystring("tobp")) = 1 then
	%>
		<!-- BEGIN: Google Trusted Store -->
		<script type="text/javascript">
			var gts = gts || [];
			
			gts.push(["id", "2884"]);
			<%
			trustStoreItemID = ""
			sql = 	"exec sp_googleTrustedItem 2"
			set rsTrust = oConn.execute(sql)
			if not rsTrust.eof then trustStoreItemID = rsTrust("itemid")
			%>
			gts.push(["google_base_offer_id", "<%=trustStoreItemID%>"]);
			gts.push(["google_base_subaccount_id", "139792"]);
			gts.push(["google_base_country", "US"]);
			gts.push(["google_base_language", "EN"]);
		//	gts.push(["gtsContainer","we_google_trusted_badge"]);
			
			(function() {
				var scheme = (("https:" == document.location.protocol) ? "https://" : "http://");
				var gts = document.createElement("script");
				gts.type = "text/javascript";
				gts.async = true;
				gts.src = scheme + "www.googlecommerce.com/trustedstores/gtmp_compiled.js";
				var s = document.getElementsByTagName("script")[0];
				s.parentNode.insertBefore(gts, s);
			})();
		</script>
		<!-- END: Google Trusted Store -->
		
		<%
		sql = 	"select	a.orderid" & vbcrlf & _
				"	,	isnull(sum(case when c.partnumber like 'WCD-%' then 1 else 0 end), 0) custom" & vbcrlf & _
				"	,	isnull(sum(case when d.typeid = 16 then 1 else 0 end), 0) phone" & vbcrlf & _
				"	,	isnull(sum(case when d.vendor in ('CM', 'DS', 'MLD') then 1 else 0 end), 0) dropship" & vbcrlf & _
				"from	we_orders a join v_accounts b" & vbcrlf & _
				"	on	a.accountid = b.accountid and a.store = b.site_id join we_orderdetails c" & vbcrlf & _
				"	on	a.orderid = c.orderid join we_items d" & vbcrlf & _
				"	on	c.itemid = d.itemid" & vbcrlf & _
				"where	a.orderid = '" & nOrderID & "'" & vbcrlf & _
				"	and	(a.extordertype is null or a.extordertype not in (4,5,6,7,8,9))" & vbcrlf & _
				"group by a.orderid"
	'	response.write "<pre>" & sql & "</pre>"
		set rsCheck = oConn.execute(sql)
		
		if not rsCheck.eof then
			if rsCheck("custom") > 0 then
				estShipDate = date+10
			elseif rsCheck("phone") > 0 then
				estShipDate = date+7
			elseif rsCheck("dropship") > 0 then
				estShipDate = date+10
			else
				estShipDate = date+5
			end if
	
			strEstShipDate = year(estShipDate) & "-" & right("00" & month(estShipDate),2) & "-" & right("00" & day(estShipDate),2)
			%>
			<!-- START Trusted Stores Order -->
			<div id="gts-order" style="display:none;">
				<!-- start order and merchant information -->
				<span id="gts-o-id"><%=nOrderID%></span>
				<span id="gts-o-domain">www.cellularoutfitter.com</span>
				<span id="gts-o-email"><%=sEmail%></span>
				<span id="gts-o-country">US</span>
				<span id="gts-o-currency">USD</span>
				<span id="gts-o-total"><%=nOrderAmount%></span>
				<span id="gts-o-discounts"><%=round(prepInt(emailOrderGrandTotal) - prepInt(emailSubTotal) - prepInt(emailShipFee) - prepInt(emailOrderTax),2)%></span>
				<span id="gts-o-shipping-total"><%=emailShipFee%></span>
				<span id="gts-o-tax-total"><%=emailOrderTax%></span>
				<span id="gts-o-est-ship-date"><%=strEstShipDate%></span>
				<%if rsCheck("custom") > 0 or rsCheck("dropship") > 0 then%>
				<span id="gts-o-has-preorder">Y</span>            
				<%else%>
				<span id="gts-o-has-preorder">N</span>
				<%end if%>
				<span id="gts-o-has-digital">N</span>
				<!-- end order and merchant information -->
				
				<!-- start repeated item specific information -->
				<!-- item example: this area repeated for each item in the order -->
			<%
		'	sql = "SELECT d.brandName, e.modelName, A.itemid,A.PartNumber,A.itemdesc,A.price_our,A.itempic,c.id,c.musicSkinsID,c.artist + '-' + c.designName as prodDesc,c.price_we,c.defaultImg,c.image,B.quantity FROM we_orderdetails B left join we_items A ON B.itemid = A.itemid left join we_items_musicSkins C on B.itemID = C.id left join we_brands d on a.brandID = d.brandID left join we_models e on a.modelID = e.modelID WHERE B.itemID > 0 and B.orderid = '" & nOrderId & "'"
			sql	=	"select	d.brandname, e.modelname, a.itemid, a.partnumber, a.itemdesc, a.price_our, a.itempic, b.quantity " & vbcrlf & _
					"from	we_orderdetails b left join we_items a " & vbcrlf & _
					"	on	b.itemid = a.itemid left join we_brands d " & vbcrlf & _
					"	on	a.brandid = d.brandid left join we_models e " & vbcrlf & _
					"	on	a.modelid = e.modelid " & vbcrlf & _
					"where	b.itemid > 0 and b.orderid = '" & nOrderID & "'"
	'		response.write "<pre>" & sql & "</pre>"
			set rs = oConn.execute(sql)
			if not rs.eof then
				lap = 0
				do until rs.eof
					lap = lap + 1
					productpageToken = "p"
					useBrandName = rs("brandName")
					useModelName = rs("modelName")
					nID = rs("itemid")
					nPartNumber = rs("PartNumber")
					partNumber = nPartNumber
					nDesc = rs("itemdesc")
					nQty = rs("quantity")
					nPrice = rs("price_our")
					nItempic = "/productpics/thumb/" & rs("itempic")
					%>
					<span class="gts-item">
						<span class="gts-i-name"><%=nDesc%></span>
						<span class="gts-i-price"><%=nPrice%></span>
						<span class="gts-i-quantity"><%=nQty%></span>
						<span class="gts-i-prodsearch-id"><%=nID%></span>
						<span class="gts-i-prodsearch-store-id">139792</span>
						<span class="gts-i-prodsearch-country">US</span>
						<span class="gts-i-prodsearch-language">EN</span>
					</span>
					<%
					rs.movenext
				loop
			end if
			%>
			</div>
			<!-- END Trusted Stores -->
	<%
		end if
	end if
end if
on error goto 0
%>
<script>_satellite.pageBottom();</script>
<div id="popCover" style="height:100%; display:none; width:100%; background-color:#333; position:fixed; left:0px; top:0px; z-index:3000; opacity:0.6; filter:alpha(opacity=60);">&nbsp;</div>
<div id="popBox" style="height:2000px; display:none; width:100%; position:absolute; left:0px; top:0px; z-index:3001; margin:auto; text-align:center;"></div>
<%
	call printPixel(3)
	call CloseConn(oConn)
%>
</body>
</html>