<%
dim fso : set fso = CreateObject("Scripting.FileSystemObject")
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_GetMySession.asp"-->
<%
dim semail, brandID, modelID, typeID, itemID
semail = prepStr(request.QueryString("semail"))
brandID = prepInt(request.QueryString("brandID"))
modelID = prepInt(request.QueryString("modelID"))
typeID = prepInt(request.QueryString("typeID"))
itemID = prepInt(request.QueryString("itemID"))
dim newsletterMsg : newsletterMsg = ""

bSubscribed = false

if semail <> "" then
	if semail = "checkCouponUsed" then
		if prepInt(request.cookies("welcomeCouponUsed")) = 0 then
			if prepInt(session("widgetCollapsed")) = 0 then
				response.write "show_expand"
			else
				response.write "show_collapse"
			end if
		else
			response.write "hide"
		end if
		call CloseConn(oConn)
		response.end
	elseif semail = "collapsed" then
		session("widgetCollapsed") = mySession
		response.write "collapsed"
		call CloseConn(oConn)
		response.end
	elseif semail = "expanded" then
		session("widgetCollapsed") = ""
		response.write "expanded"
		call CloseConn(oConn)
		response.end
	elseif instr(trim(semail), "@") > 0 and instr(trim(semail), " ") = 0 and instr(trim(semail), ".@") = 0 then
		if isValidEmail(semail) then									
			sql = "exec sp_emailSub 2,'" & semail & "',''," & brandID & "," & modelID & "," & typeID & "," & itemID
			session("errorSQL") = sql
			set emailRS = oConn.execute(sql)
			
			if emailRS("returnValue") = "duplicate" then
				newsletterTitle = "Oops, Looks Like You've<br />Already Subscribed"
				newsletterMsg = "Thank you for your interest in joining our email program. Our records indicate that you are already registered " &_
								"making you ineligible for this offer. Don't worry, the stylus pen can still be yours! Place any order today and " &_
								"it will automatically be included in your order, absolutely free! " &_
								"<br /><br />" &_
								"Sincerely,<br />" &_                                                    
								"CellularOutfitter.com"
			else
				dim cdo_to, cdo_from, cdo_subject, cdo_body
				
				sql = "exec sp_ppUpsell 2, 102964, 0, 30"
				session("errorSQL") = sql
				set recommendRS = oConn.execute(sql) 
				strUpSell = ""
				
				if not recommendRS.EOF then
					strUpSell = strUpSell & "		<table width=""370"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf
					strUpSell = strUpSell & "			<tr><td>&nbsp;</td></tr>" & vbcrlf
					strUpSell = strUpSell & "			<tr>" & vbcrlf
					counter = 1
					do until recommendRS.EOF
						itemID		= recommendRS("itemID")
						itemPic		= recommendRS("itemPic")
						itemDesc	= recommendRS("itemDesc")
						useItemDesc = replace(itemDesc,"  "," ")
						itemImgPath = server.MapPath("/productPics/thumb/" & itemPic)
						itemImgPath = "C:\inetpub\wwwroot\productpics_co\thumb\" & itemPic
						price		= recommendRS("price")
						price_retail= cdbl(recommendRS("price_retail"))
						promo_price	= cdbl(price - (price * .25))
						productLink = "http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".html?dzid=email_TRANS_RECS_" & counter & "L_" & itemID & "_WELCOME25&utm_source=internal&utm_medium=email&utm_campaign=Welcome1&utm_content=welcome&utm_promocode=WELCOME25"
						
						set fsThumb = CreateObject("Scripting.FileSystemObject")
						if not fsThumb.FileExists(itemImgPath) then
							useImg = "http://www.cellularoutfitter.com/productPics/big/imagena.jpg"
							DoNotDisplay = 1
						else
							useImg = "http://www.cellularoutfitter.com/productPics/big/" & itemPic
						end if
						
						strUpSell = strUpSell & "			<td width=""174"" align=""center"" valign=""top"" style=""background-color: #ffffff; color: #006699; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 150%; margin: 0; padding:5px"" >" & vbcrlf
						strUpSell = strUpSell & "				<a href=""" & productLink & """ target=""_blank"" style=""text-decoration:none; "">" & vbcrlf
						strUpSell = strUpSell & "					<img src=""" & useImg & """ width=""140"" border=""0"" style=""display:block;padding-bottom:10px;""  />" & vbcrlf
						strUpSell = strUpSell & "					<p style=""color:#333333; font:bold 12px Verdana, Geneva, sans-serif; line-height:18px; min-height:72px; margin:0; text-align:left; padding:0 0 10px 10px;height: 90px;"">" & itemDesc & "</p>" & vbcrlf
						strUpSell = strUpSell & "				</a>" & vbcrlf                                    
						strUpSell = strUpSell & "				<p style=""color:#cccccc; font:normal 11px Verdana, Geneva, sans-serif; line-height:14px; margin:0; text-align:left; padding-left:10px;"">" & vbcrlf
						strUpSell = strUpSell & "					Retail Price: <span style=""font:normal Verdana, Geneva, sans-serif; text-decoration:line-through "">" & formatCurrency(price_retail) & "</span>" & vbcrlf
						strUpSell = strUpSell & "				</p>" & vbcrlf
						strUpSell = strUpSell & "				<p style=""color:#cccccc; font:normal 11px Verdana, Geneva, sans-serif; line-height:14px; margin:0; text-align:left; padding-left:10px;"">" & vbcrlf
						strUpSell = strUpSell & "					Our Price: " & formatCurrency(price) & "" & vbcrlf
						strUpSell = strUpSell & "				</p>" & vbcrlf
						strUpSell = strUpSell & "				<p style=""color:#333333; text-decoration:none; font-size:12px; padding-left:10px; text-align:left;"">" & vbcrlf
						strUpSell = strUpSell & "					<strong >Promo Price:</strong><span style=""color:#ce0002; font-weight:normal""> " & formatCurrency(promo_price) & "</span>" & vbcrlf
						strUpSell = strUpSell & "				</p>" & vbcrlf
						strUpSell = strUpSell & "				<a href=""" & productLink & """ target=""_blank"">" & vbcrlf
						strUpSell = strUpSell & "					<table width=""150"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf
						strUpSell = strUpSell & "						<tr>" & vbcrlf
						strUpSell = strUpSell & "							<td style=""text-align:center;font-weight:normal;color:#ffffff;font-size:14px;line-height:18px;background:#33cc66;border:1px solid #009966;border-radius:20px;padding:5px 10px;"">BUY NOW &raquo;</td>" & vbcrlf
						strUpSell = strUpSell & "						</tr>" & vbcrlf
						strUpSell = strUpSell & "					</table>" & vbcrlf
						strUpSell = strUpSell & "				</a>" & vbcrlf
						strUpSell = strUpSell & "			</td>" & vbcrlf
						
						if counter = 1 or counter = 3 then
							strUpSell = strUpSell & "		<td width=""2""><img src=""http://www.cellularoutfitter.com/images/subscribe/v_divider.jpg"" width=""1"" height=""320""  border=""0"" style=""display:block;"" /></td>" & vbcrlf
						elseif counter = 2 then
							strUpSell = strUpSell & "	</tr>" & vbcrlf
							strUpSell = strUpSell & "	<tr>" & vbcrlf
							strUpSell = strUpSell & "		<td width=""174""  style=""border-bottom:1px solid #cccccc;padding:5px 0;""></td>" & vbcrlf
							strUpSell = strUpSell & "		<td width=""2"" style=""border-bottom:1px solid #cccccc;padding:5px 0;""></td>" & vbcrlf
							strUpSell = strUpSell & "		<td width=""174""  style=""border-bottom:1px solid #cccccc;padding:5px 0;""></td>" & vbcrlf
							strUpSell = strUpSell & "	</tr>" & vbcrlf
							strUpSell = strUpSell & "	<tr>" & vbcrlf
						else 
							strUpSell = strUpSell & "	</tr>" & vbcrlf
							strUpSell = strUpSell & "</table>" & vbcrlf
							exit do
						end if
						
						recommendRS.movenext
						counter = counter + 1
					loop
				end if
				
				strTemplate = readTextFile("C:\inetpub\wwwroot\cellularoutfitter.com\www\images\email\template\co-subscribe.htm")
				strTemplate = replace(strTemplate, "[UPSELL]", strUpSell)
				strTemplate = replace(strTemplate, "[EXPIRY]", DateAdd("d", 7, Date()))
				
				cdo_body = strTemplate
				
				if semail = "jnord@test.com" then semail = "jon@wirelessemporium.com"
				cdo_to = semail
				cdo_from = "support@CellularOutfitter.com"
				cdo_subject = "Welcome and Prepare Yourself for Serious Savings"
				
				CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
				
				newsletterTitle = "Check Your Inbox!"
				newsletterMsg = "Thanks for joining! As promised, we've sent your 25% off coupon code to the email address you have provided." &_
                    			"<br /><br />" &_
                    			"And guess what? There are more deals to come!" &_
                    			"<br /><br />" &_
                    			"Now that you are a member, you'll be the first to know about exclusive sales, special discounts, and insider information before anyone else! But keep it a secret, because this is just for our fans. Be sure to check your inbox, because you never know when something special will show up!"
			end if
			session("widgetCollapsed") = mySession
			response.cookies("welcomeCouponUsed") = mySession
			response.cookies("welcomeCouponUsed").expires = now+30
			bSubscribed = true
		else
			newsletterMsg = "Please enter a valid email address and try again. (" & semail & ")"
		end if
	else
		newsletterMsg = "Please make sure you enter a valid email address and try again. (" & semail & ")"
	end if
else
	newsletterMsg = "Email error, please double check that you have entered a valid email. (" & semail & ")"
end if
call CloseConn(oConn)
%>
<div class="promoBoxMain" onclick="closeReviewPopup(); <%if bSubscribed then%>emailOfferDown();<%end if%>">
    <div class="tb closeWinRow">
        <div class="fr closeWin"></div>
    </div>
    <div class="promoTitle"><%=newsletterTitle%></div>
    <div class="promoDetails">
        <%=newsletterMsg%>
    </div>
    <div class="newsRegCTA"></div>
</div>
<%

Function isValidEmail(emailAddress)
	'Declare variables 
	Dim ValidEmail, emailParts, iLoopCounter, emailChar, acceptableChars, last2Chars 
	ValidEmail = True 'set the default result to True
	'acceptableChars are the characters that we will allow in our email
	acceptableChars="abcdefghijklmnopqrstuvwxyz.-_@"
	'use the Split function to create an array with the @ as the separator
	'so if your email was test@tester.com the email would be split into an array
	'with the first array element holding "test" and the second "tester.com"
	emailParts = Split(emailAddress, "@")
	'check to make sure that there is only 1 @ and that there are 2 parts
	'remember arrays are zero based
	'Using the UBound function will return the highest element in the array
	'So if it's a valid email the UBound function will return 1, i.e. 0 start
	If UBound(emailParts) <> 1 Then
	ValidEmail = false
	Else 
	'Check the length of each part of the email address
	'first part can be just one character, 2nd part must be atleast 4
	If Len(emailParts(0))<1 OR Len(emailParts(1))<4 Then
	ValidEmail = false
	End If
	'check first character on the left part isn't a "." using Left function
	If Left(emailParts(0), 1)="." Then
	ValidEmail = false
	End If 
	'check that there is a . in the second part of the email address - .com
	If InStr(emailParts(1), ".") <= 0 Then
	ValidEmail = false 
	End If 
	'We know there's a . now make sure there are 2 characters after the . for valid email
	last2Chars=Right(emailParts(1),2)
	If InStr(last2chars,".") Then
	ValidEmail = false
	End If
	'check that there shouldn't be a _ in the second part of the email address 
	If InStr(emailParts(1), "_") >0 Then
	ValidEmail = false 
	End If 
	End If
	'loop through each character of email
	For iLoopCounter = 1 to Len(emailAddress)
	'Use Lcase & Mid functions, Mid function used to return each individual character
	'in the email, and then Lcase converts it into lowercase
	emailChar = Lcase(Mid(emailAddress, iLoopCounter, 1)) 
	'Check if the emailAddress characters are acceptable
	If InStr(acceptableChars, emailChar) = 0 and Not IsNumeric(emailChar) Then
	ValidEmail = false
	End if
	Next
	'check if there is 2 . in a row
	If InStr(emailAddress, "..") > 0 Then
	ValidEmail=false
	End If
	'check if there is @. in a row
	If InStr(emailAddress, "@.") > 0 Then
	ValidEmail=false
	End If 
	isValidEmail=ValidEmail
End function

function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")		
	else
		formatSEO = ""
	end if
	formatSEO = replace(formatSEO, "---", "-")
	formatSEO = replace(formatSEO, "--", "-")	
end function
%>