<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	dim curVal : curVal = prepStr(request.QueryString("curVal"))
	dim lap : lap = 0
	dim skipSingle : skipSingle = 0
	dim advWhere : advWhere = ""
	
	if curVal = "" then 
		call CloseConn(oConn)
		response.End()
	end if
	
	sql = "select distinct top 5 a.brandName as primName, b.modelName as secName, b.releaseYear, b.releaseQuarter from we_brands a left join we_models b on a.brandID = b.brandID left join we_Items c on b.modelID = c.modelID where c.price_co > 0 and c.price_co is not null and a.brandName like '" & curVal & "%' order by b.releaseYear desc, b.releaseQuarter desc"
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	if rs.EOF then
		sql = "select distinct top 5 a.modelName as primName, c.typeName as secName, a.releaseYear, a.releaseQuarter from we_Models a left join we_Items b on a.modelID = b.modelID left join we_Types c on b.typeID = c.typeID where b.price_co > 0 and b.price_co is not null and modelName like '" & curVal & "%' order by a.releaseYear desc, a.releaseQuarter desc"
		session("errorSQL") = sql
		set rs = oConn.execute(sql)
		
		if rs.EOF then
			skipSingle = 1
			sql = "select distinct top 5 itemDesc as primName, '' as secName from we_Items where itemDesc like '%" & curVal & "%'"
			session("errorSQL") = sql
			set rs = oConn.execute(sql)
			
			if rs.EOF then
				skipSingle = 1
				curArray = split(curVal," ")
				for i = 0 to ubound(curArray)
					if advWhere = "" then 
						advWhere = "itemDesc like '%" & curArray(i) & "%'"
					else
						advWhere = advWhere & " and itemDesc like '%" & curArray(i) & "%'"
					end if
				next
				sql = "select distinct top 5 itemDesc as primName, '' as secName from we_Items where " & advWhere
				session("errorSQL") = sql
				set rs = oConn.execute(sql)
			end if
		end if
	end if
	
	if rs.EOF then
%>&nbsp;<%
	else
		do while not rs.EOF
			lap = lap + 1
			if lap = 1 and skipSingle = 0 then
%>
<div id="searchRow_<%=lap%>" onclick="document.frmNxtAc.keywords.value='<%=rs("primName")%>';document.frmNxtAc.submit();" style="cursor:pointer; width:200px;"><%=rs("primName")%></div>
<%			
				lap = lap + 1
			end if
%>
<div id="searchRow_<%=lap%>" onclick="document.frmNxtAc.keywords.value='<%=rs("primName") & " " & rs("secName")%>';document.frmNxtAc.submit();" style="cursor:pointer; width:200px;"><%=rs("primName") & " " & rs("secName")%></div>
<%
			rs.movenext
		loop
	end if

	call CloseConn(oConn)
%>