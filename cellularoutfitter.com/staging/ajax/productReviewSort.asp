<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!-- METADATA TYPE="typelib" NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<%
	itemid = prepInt(request.queryString("itemid"))
	strSort = prepStr(request.queryString("strSort"))
	strCanonical = prepStr(request.queryString("canonicalURL"))
	
	sql = "exec getReviews 2, " & itemid & ", '" & strSort & "'"
	session("errorSQL") = sql
	set objRsReviews = oConn.execute(sql)
	
	if not objRsReviews.EOF then
		do while not objRsReviews.EOF
			reviewID = objRsReviews("id")
			reviewTitle = objRsReviews("reviewTitle")
			reviewerNickName = objRsReviews("reviewerNickName")
			reviewerLocation = objRsReviews("reviewerLocation")
			reviewerAge = objRsReviews("reviewerAge")
			reviewerGender = objRsReviews("reviewerGender")
			reviewerType = objRsReviews("reviewerType")
			starRating = objRsReviews("starRating")
			ownershipPeriod = objRsReviews("ownershipPeriod")
			entryDate = objRsReviews("entryDate2")

			isRecommend = objRsReviews("isRecommend")
			isProPrice = objRsReviews("isProPrice")
			isProDesign = objRsReviews("isProDesign")
			isProWeight = objRsReviews("isProWeight")
			isProProtection = objRsReviews("isProProtection")
			isConFit = objRsReviews("isConFit")
			isConRobust = objRsReviews("isConRobust")
			isConMaintenance = objRsReviews("isConMaintenance")
			isConWeight = objRsReviews("isConWeight")
			thumbsUpCount = objRsReviews("thumbsUpCount")
			thumbsDownCount = objRsReviews("thumbsDownCount")
			
			otherCon = objRsReviews("otherCon")
			otherPro = objRsReviews("otherPro")
			reviewBody = objRsReviews("reviewBody")
			%>
			<div style="display:table; border-bottom:1px solid #ccc; margin:10px 0px 10px 0px; padding-bottom:10px;">
				<div style="float:left; width:250px;">
					<div style="float:left; background-color:#f7f7f7; width:230px; padding:10px;">
						<div style="float:left; width:100%;">
							<div style="float:left; padding-top:2px;"><img src="/images/product/review/ico-head.png" border="0" /></div>
							<div style="float:left; padding-left:5px;">
								<div style="font-weight:bold; color:#034076;"><%=reviewerNickName%></div>
								<div><%=reviewerLocation%></div>
							</div>
						</div>
						<div style="float:left; width:100%; padding-top:15px;">
							<%if isRecommend then%>
							<div style="float:left;"><img src="/images/product/review/recommend-check.png" border="0" alt="I recommend this!" /></div>
							<div style="float:left; padding-left:3px; font-style:italic;"><span style="font-size:15px; font-weight:bold;">Yes,</span> I recommend this!</div>
							<%else%>
							<div style="float:left;"><img src="/images/product/review/recommend-x.png" border="0" /></div>
							<div style="float:left; padding-left:3px; font-style:italic;"><span style="font-size:15px; font-weight:bold;">No,</span> I don't recommend this!</div>
							<%end if%>
						</div>
						<div style="float:left; width:100%; padding-top:15px;">
							<div>
								<strong>Age:</strong> 
								<%if not isnull(reviewerAge) then%>
									<%=reviewerAge%>
								<%else%>
									N/A
								<%end if%>
							</div>
							<div style="padding-top:7px;">
								<strong>Gender:</strong> 
								<%
								if not isnull(reviewerGender) then
									if reviewerGender = "M" then
										response.write "Male"
									else
										response.write "Female"
									end if
								else
								%>
									N/A
								<%end if%>
							</div>
							<div style="padding-top:7px;">
								<strong>Description:</strong> 
								<%
								if not isnull(reviewerType) then
									select case ucase(reviewerType)
										case "STU" : response.write "Student"
										case "BO" : response.write "Business Owner"
										case "TE" : response.write "Technology Enthusiast"
										case "PA" : response.write "Phone Addict"
										case "CPU" : response.write "Casual Phone User"
									end select
								else
								%>
									N/A
								<%end if%>
							</div>
							<div style="padding-top:7px;">
								<strong>Ownership:</strong> 
								<%
								if not isnull(ownershipPeriod) then
									select case cint(ownershipPeriod)
										case 1 : response.write "2 ~ 4 Weeks"
										case 2 : response.write "1 ~ 2 Months"
										case 3 : response.write "3 ~ 12 Months"
										case 4 : response.write "1 year +"
									end select
								else
								%>
									N/A
								<%end if%>
							</div>
						</div>
						<div style="float:left; width:100%; padding-top:15px; border-bottom:1px solid #ccc;"></div>
						<div style="float:left; width:100%; padding-top:15px;">
							<div>
								<strong>Pros:</strong> 
								<%
								strPros = ""
								if isProPrice then strPros = "Great Price, "
								if isProDesign then strPros = strPros & "Beautifully Designed, "
								if isProWeight then strPros = strPros & "Lightweight, "
								if isProProtection then strPros = strPros & "Protects Contents, "
								if strPros <> "" then response.write left(strPros, len(strPros)-2)
								if not isnull(otherPro) then 
									if strPros <> "" then response.write "<br>"
									response.write "<span style=""font-style:italic;"">""" & otherPro & """</span>"
								end if
								%>
							</div>
							<div style="padding-top:7px;">
								<strong>Cons:</strong>
								<%
								strCons = ""
								if isConFit then strCons = "Doesn't Fit Well, "
								if isConRobust then strCons = strCons & "Scratches Easily, "
								if isConMaintenance then strCons = strCons & "Difficult To Clean, "
								if isConWeight then strCons = strCons & "Too Bulky/Heavy, "
								if strCons <> "" then response.write left(strCons, len(strCons)-2)
								if not isnull(otherCon) then 
									if strCons <> "" then response.write "<br>"
									response.write "<span style=""font-style:italic;"">""" & otherCon & """</span>"
								end if
								%>
							</div>
						</div>
					</div>
                    <!--
					<div style="float:left; width:100%; padding-top:10px;">
						<div style="float:left; font-weight:bold; padding:5px 0px 0px 10px;">Share:</div>
						<div style="float:left; padding-left:10px;"><img src="/images/product/review/share-facebook.png" border="0" /></div>
						<div style="float:left; padding-left:10px;">
	                        <a style="vertical-align:bottom;" href="http://twitter.com/share" data-url="<%=canonicalURL%>" class="twitter-share-button" data-count="none" data-via="CellOutfitter">Tweet</a>
                        	<img src="/images/product/review/share-twitter.png" border="0" />
						</div>
					</div>
                    -->
				</div>
				<div style="float:left; margin-left:20px; width:470px;">
					<div style="float:left; width:100%;">
						<div style="float:left;"><%=getRatingAvgStarBig(starRating)%></div>
						<div style="float:left; padding-left:10px; font-size:16px; font-weight:bold;"><%=formatnumber(starRating,1)%></div>
					</div>
					<div style="float:left; width:100%; font-size:18px; padding-top:3px;"><%=reviewTitle%></div>
					<div style="float:left; width:100%; font-size:12px; padding-top:3px; color:#999; font-style:italic;">Posted on <%=entryDate%></div>
					<div style="float:left; width:100%; padding-top:10px; line-height:22px; font-size:14px;"><%=reviewBody%></div>
					<div style="float:left; width:100%; padding-top:20px;">
						<div style="float:left; font-weight:bold; padding-top:7px;">Was this review helpful?</div>
						<div style="float:left; padding-left:10px;"><img src="/images/product/review/ico-thumbs-up.png" border="0" style="cursor:pointer;" onclick="updateThumbs(<%=reviewID%>,1,<%=thumbsUpCount%>)" /></div>
						<div style="float:left; font-weight:bold; padding:7px 0px 0px 3px;" id="thumbsUpCount<%=reviewID%>"><%=thumbsUpCount%></div>
						<div style="float:left; padding-left:10px;"><img src="/images/product/review/ico-thumbs-down.png" border="0" style="cursor:pointer;" onclick="updateThumbs(<%=reviewID%>,0,<%=thumbsDownCount%>)" /></div>
						<div style="float:left; font-weight:bold; padding:7px 0px 0px 3px;"  id="thumbsDownCount<%=reviewID%>"><%=thumbsDownCount%></div>
					</div>
				</div>
			</div>
			<%
			objRsReviews.movenext
		loop
	end if
	
	call CloseConn(oConn)
		
	function getRatingAvgStarBig(rating)
		dim nRating 
		dim strRatingImg
		nRating = cdbl(rating)
		strRatingImg = 	"<img src=""/images/product/review/star-empty.png"" border=""0"" />" & vbcrlf & _
						"<img src=""/images/product/review/star-empty.png"" border=""0"" />" & vbcrlf & _
						"<img src=""/images/product/review/star-empty.png"" border=""0"" />" & vbcrlf & _
						"<img src=""/images/product/review/star-empty.png"" border=""0"" />" & vbcrlf & _
						"<img src=""/images/product/review/star-empty.png"" border=""0"" />" & vbcrlf
		
		if nRating > 0 or nRating <=5 then
			strRatingImg = ""	
			for i=1 to 5
				if nRating => i then
					strRatingImg = strRatingImg & "<img src=""/images/product/review/star-full.png"" border=""0"" /> "
				elseif nRating => ((i - 1) + .1) then
					strRatingImg = strRatingImg & "<img src=""/images/product/review/star-full.png"" border=""0"" /> "				
				else
					strRatingImg = strRatingImg & "<img src=""/images/product/review/star-empty.png"" border=""0"" /> "
				end if
			next		
		end if
		
		getRatingAvgStarBig = strRatingImg
	end function	
%>
