<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	response.buffer = true
	response.expires = -1
	
	sql = 	"select top 5 b.itemID, a.orderid, a.orderdatetime, b.itemDesc_CO, c.fname, c.lname " &_
			"from we_Orders a " &_
				"outer apply ( " &_
					"select top 1 ib.itemDesc_CO, ia.itemID " &_
					"from we_orderdetails ia " &_
						"left join we_Items ib on ia.itemid = ib.itemID " &_
					"where orderid = a.orderID" &_
				") b " &_
				"outer apply ( " &_
					"select top 1 ia.fname, ia.lname " &_
					"from CO_accounts ia " &_
					"where ia.accountid = a.accountid " &_
				") c " &_
			"where approved = 1 and cancelled is null and store = 2 " &_
			"order by orderid desc"
	session("errorSQL") = sql
	set recentOrdersRS = oConn.execute(sql)
%>
<div style="float:left; width:170px; height:29px; font-weight:bold; color:#FFF; padding-top:5px; margin-bottom:10px; text-align:center; background-image:url(/images/backgrounds/livePurch.gif)">Live Purchases</div>
<% do while not recentOrdersRS.EOF %>
<div style="float:left; width:170px; font-size:12px; padding-bottom:5px; margin-bottom:10px; color:#666; font-size:11px; color:#666; border-bottom:1px solid #999;">
	<em>(<%=recentOrdersRS("orderdatetime")%>)</em><br />
	<strong><%=recentOrdersRS("fname") & " " & left(recentOrdersRS("lname"),1) & "."%> purchased</strong><br />
	<a href="/p-<%=recentOrdersRS("itemID")%>-<%=formatSEO(recentOrdersRS("itemDesc_CO"))%>.html" style="font-size:11px; color:#3399cc;"><%=recentOrdersRS("itemDesc_CO")%></a>
</div>
<%
	recentOrdersRS.movenext
loop

call CloseConn(oConn)
%>