<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	buffer = true
	
	brandID = prepStr(request.QueryString("brandID"))
	modelID = prepStr(request.QueryString("modelID"))
	typeID = prepStr(request.QueryString("typeID"))
		
	'Determine the type of pull
	if typeID = 5 then 'Bluetooth headsets
		sql = 	"select top 8 a.price_Retail, a.price_CO, a.itemPic_CO, a.itemDesc_CO, a.itemID " &_
				"from	we_items a " &_
				"left join we_Items b on a.partNumber = b.partNumber and b.master = 1 " &_
				"where	a.HandsfreeType in (2,3) " &_
				"and b.inv_qty > 0 and a.price_CO is not null and a.price_CO > 0 and a.hideLive = 0 " &_
				"order by a.numberOfSales desc"
	elseif typeID = 999999 then 'Other
		sql = 	"select top 8 a.price_Retail, a.price_CO, a.itemPic_CO, a.itemDesc_CO, a.itemID " &_
				"from we_Items a  " &_
				"left join we_Items b on a.partNumber = b.partNumber and b.master = 1 " &_
				"where b.inv_qty > 0 and a.price_CO is not null and a.price_CO > 0 " &_
				"and a.hideLive = 0 and ((a.brandID = 17 and a.modelID = 968) or (a.brandID = 0 and a.modelID = 0)) and (a.typeID = 13 or a.typeID = 12 or a.typeID = 21 or a.typeID = 8) " &_
				"order by a.numberOfSales desc "
	else 'Everything that's not bluetooth or other will use this
		sql = 	"select top 8 a.price_Retail, a.price_CO, a.itemPic_CO, a.itemDesc_CO, a.itemID " &_
				"from we_Items a  " &_
					"left join we_Items b on a.partNumber = b.partNumber and b.master = 1 " &_
				"where b.inv_qty > 0 and a.price_CO is not null and a.price_CO > 0 " &_
				"and a.hideLive = 0 and a.brandID = " & brandID & " " &_
				"and a.modelID = " & modelID & " and a.typeID = " & typeID & " " &_
				"order by a.numberOfSales desc"
		'sql = "select top 10 * from we_Items order by numberOfSales desc"
	end if
	
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
    
	if rs.eof then %>
    <div><p>No matching accessories could be located for your phone.  Please select another category.</p></div>
    <% 
	end if
		
	while not rs.eof 
	%>
	<div class="accountSuggestedProduct">
		<div class="photo">
			<a href="/p-<%=rs("itemID")%>-<%=formatSEO(rs("itemDesc_CO"))%>-.html"><img src="/productPics/thumb/<%=rs("itemPic_CO")%>" width="100" height="100" /></a>
		</div>
		<div class="link"><a href="/p-<%=rs("itemID")%>-<%=formatSEO(rs("itemDesc_CO"))%>-.html"><%=rs("itemDesc_CO")%></a></div>
		<div class="price"><%=FormatCurrency(rs("price_CO"))%></div ><% pctOff = Int(100-((rs("price_CO")/rs("price_Retail"))*100)) %>
		<div class="savings">(<%=pctOff%>% OFF Retail Price: <del><%=FormatCurrency(rs("price_Retail"))%>)</del></div >
	</div>
	<% rs.movenext %>
	<% wend %>
	<div style="clear:both;"></div>
<%
call CloseConn(oConn)
%>