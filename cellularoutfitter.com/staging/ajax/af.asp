<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/includes/asp/inc_BasePage.asp"--><%	
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
	
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim categoryID : categoryID = prepInt(request.QueryString("categoryID"))
	dim catReset : catReset = prepInt(request.QueryString("catReset"))
	dim subLinkRequest : subLinkRequest = prepInt(request.QueryString("subLinkRequest"))
	dim largeField : largeField = prepInt(request.QueryString("largeField"))
	
	if largeField = 1 then
		useClass = "accessoryLargeSelect"
		useFunc = "af_Select2"
	else
		useClass = "accessorySelect"
		useFunc = "af_Select"
	end if
	
	if subLinkRequest = 1 then
		if categoryID > 0 then
			sql =	"select a.brandName, b.modelName, c.typeName_CO " &_
					"from we_brands a " &_
						"join we_models b on b.modelID = " & modelID & " " &_
						"join we_types c on c.typeID = " & categoryID & " " &_
					"where a.brandID = " & brandID
			session("errorSQL") = sql
			set linkDetailsRS = oConn.execute(sql)
			response.Write("/sb-" & brandID & "-sm-" & modelID & "-sc-" & categoryID & "-" & formatSEO(linkDetailsRS("brandName")) & "-" & formatSEO(linkDetailsRS("modelName")) & "-" & formatSEO(linkDetailsRS("typeName_CO")) & ".html")
		elseif modelID > 0 then
			sql =	"select a.brandName, b.modelName " &_
					"from we_brands a " &_
						"join we_models b on b.modelID = " & modelID & " " &_
					"where a.brandID = " & brandID
			session("errorSQL") = sql
			set linkDetailsRS = oConn.execute(sql)
			response.Write("/m-" & modelID & "-" & formatSEO(linkDetailsRS("brandName")) & "-" & formatSEO(linkDetailsRS("modelName")) & "-cell-phone-accessories.html")
		else
			sql =	"select a.brandName " &_
					"from we_brands a " &_
					"where a.brandID = " & brandID
			session("errorSQL") = sql
			set linkDetailsRS = oConn.execute(sql)
			response.Write("/b-" & brandID & "-" & formatSEO(linkDetailsRS("brandName")) & "-cell-phone-accessories.html")
		end if
		call CloseConn(oConn)
		response.End()
	end if
	
	if brandID > 0 then
		sql	=	"exec sp_accessoryFinder_byBrand " & brandid
		set objRs = oConn.execute(sql)
%>
<select name="modelID" class="<%=useClass%>" onchange="<%=useFunc%>('m',this.value)">
    <option value="">2. Phone Model</option>
    <%
	do until objRs.eof
		if objRS("menuCat") <> modelCategory then
			if modelCategory <> "" then
				response.Write("</optgroup>")
			end if
			modelCategory = objRS("menuCat")
			response.Write("<optgroup label=""" & modelCategory & """>")
		end if
	%>
		<option value="<%=objRs("modelid")%>"<% if selectModelid = prepInt(objRs("modelid")) then %> selected="selected"<% end if %>><%=objRs("modelname")%></option>
	<%
		objRs.movenext
	loop
	%>
</select>
<%
	elseif modelID > 0 then
		sql =	"select distinct a.typeID, b.typeName_CO " &_
				"from we_itemsSiteReady a " &_
					"join we_types b on a.typeID = b.typeID " &_
				"where modelID = " & modelID & " and a.typeID not in (19,22) " &_
				"	and	(a.inv_qty > 0 or a.alwaysInStock = 1) " &_				
				"order by typeName_CO"
		session("errorSQL") = sql
		set categoryRS = oConn.execute(sql)
%>
<select name="categoryID" class="<%=useClass%>">
    <option value="">3. Category</option>
    <%
	do while not categoryRS.EOF
	%>
    <option value="<%=categoryRS("typeID")%>"><%=categoryRS("typeName_CO")%></option>
    <%
		categoryRS.movenext
	loop
	%>
</select>
<%
	elseif catReset = 1 then
%>
<select name="categoryID" class="<%=useClass%>">
    <option value="">3. Category</option>
    <option value="">Select Model First</option>
</select>
<%
	end if
	
	call CloseConn(oConn)
%>