<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/includes/asp/inc_BasePage.asp"--><%	
%><!--#include virtual="/cart/UPSxml.asp"--><%	

	dim thisUser, pageTitle, header
	
	selectBox = prepInt(request.QueryString("selectBox"))
	sZip = prepStr(request.QueryString("zip"))
	sWeight = prepStr(request.QueryString("weight"))
	shiptype = prepStr(request.QueryString("shiptype"))
	nTotalQuantity = prepInt(request.QueryString("nTotalQuantity"))
	nSubTotal = prepInt(request.QueryString("nSubTotal"))
	
	if instr(sZip,"-") > 0 then
		zipArray = split(sZip,"-")
		useZip = zipArray(0)
	else
		useZip = sZip
	end if
	
	do while len(useZip) < 5
		useZip = "0" & useZip
	loop
	
	on error resume next
	if selectBox = 1 then
		if useZip <> "" then response.write UPScodeSelectBox(useZip,sWeight,shiptype,nTotalQuantity,0,0)
	else
		if useZip <> "" then response.write UPScode(useZip,sWeight,shiptype,nTotalQuantity,0,0)
	end if
	on error goto 0
	
	call CloseConn(oConn)
%>