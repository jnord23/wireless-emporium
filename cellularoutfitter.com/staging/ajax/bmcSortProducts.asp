<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	response.buffer = true
	response.expires = -1
	
	dim brandid : brandid = prepInt(request.querystring("brandID"))
	dim modelid : modelid = prepInt(request.querystring("modelID"))
	dim categoryid : categoryid = prepInt(request.querystring("categoryid"))
	dim carrierid : carrierid = prepInt(request.querystring("carrierid"))
	
	dim musicSkins : musicSkins = prepInt(request.QueryString("musicSkins"))
	dim musicSkinGenre : musicSkinGenre = prepStr(request.QueryString("musicSkinGenre"))
	dim musicSkinArtistID : musicSkinArtistID = prepInt(request.QueryString("musicSkinArtist"))
	
	dim sortBy : sortBy = prepStr(request.querystring("sb"))
	
	strSubTypeID = ""
	if categoryid = 1270 then musicSkins = 1
	if categoryid > 999 then
		sql = 	"select	nameSEO_CO typename, subtypeid, typeid from v_subtypematrix_co where subtypeid = '" & categoryid & "'"
	else
		sql	=	"select	typename typename, subtypeid, typeid from v_subtypematrix_co where typeid = '" & categoryid & "'"
	end if
	session("errorSQL") = sql
	set catRS = oConn.execute(sql)
	if not catRS.eof then
		categoryName = catRS("typename")
		parentTypeID = catRS("typeid")
		do until catRS.eof
			strSubTypeID = strSubTypeID & catRS("subtypeid") & ","
			catRS.movenext
		loop
		strSubTypeID = left(strSubTypeID, len(strSubTypeID) - 1)
	end if
	if strSubTypeID = "" then strSubTypeID = "9999999" end if
	catRS = null
	
	if musicSkins = 1 then
		useModelID = modelID
		sql = "update we_items_musicSkins set price_CO = '8.00' where price_WE = '12.99' and price_CO is null"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		sql = "update we_items_musicSkins set price_CO = '10.00' where price_WE = '17.99' and price_CO is null"
		session("errorSQL") = sql
		oConn.execute(sql)
		
		Set Jpeg = Server.CreateObject("Persits.Jpeg")
		Jpeg.Quality = 100
		Jpeg.Interpolation = 1
	
		sql = 	"	select	1 as hasQty, cast('False' as bit) as customize, 0 onSale, cast(1 as bit) as alwaysInStock, 100 as inv_qty, 'Music Skins' as typeName, null as ItemKit_NEW, '' as itemPic, a.musicSkinsID, a.musicSkinsID as partNumber, a.defaultImg, a.preferredImg, a.id as itemID, a.brandID	" & vbcrlf & _
				"		, 	a.brand as brandName, a.artist + ' - ' + a.designName as itemDesc_CO, c.brandname" & vbcrlf & _
				"		, 	a.image as itempic_CO, a.price_co, a.msrp as price_retail, a.genre, b.modelID, b.modelName, b.modelImg, b.excludePouches, 20 typeid, 1270 subtypeid" & vbcrlf & _
				"		, 	b.includeNFL, b.includeExtraItem, e.artist, 'OriginalPrice' as [ActiveItemValueType], 1 noDiscount, 100 reviewcnt, 0 as reviewAvg, cast(0 as bit) hotDeal, cast(0 as bit) hideLive, 999 flag1 " & vbcrlf & _
				"	from 	we_items_musicSkins a join we_models b " & vbcrlf & _
				"		on 	a.modelID = b.modelID join we_brands c" & vbcrlf & _
				"		on 	a.brandid = c.brandid left outer join we_items_musicskins_artist e" & vbcrlf & _
				"		on 	a.artist = e.artist" & vbcrlf & _
				"	where 	a.skip = 0 " & vbcrlf & _
				"		and a.deleteItem = 0 " & vbcrlf & _
				"		and (a.artist <> '' and a.artist is not null) " & vbcrlf & _
				"		and (a.designname <> '' and a.designname is not null) " & vbcrlf & _
				"		and b.modelID = '" & useModelID & "' " & vbcrlf & _
				"		and a.genre like '%" & musicSkinGenre & "%'" & vbcrlf
		if "0" <> musicSkinArtistID and len(musicSkinArtistID) > 0 then
			sql = sql & "		and e.id = '" & musicSkinArtistID & "'" & vbcrlf
		end if
		sql = sql & "	order by a.artist, a.genre"
	elseif categoryid = 5 then
		sql = "select HandsfreeTypes from we_Models where modelID = " & modelID
		session("errorSQL") = sql
		set hfRS = oConn.execute(sql)
		
		if hfRS.EOF then
			modelHF = 2
		else
			modelHF = prepStr(hfRS("HandsfreeTypes"))
			if modelHF <> "" then
				if right(modelHF,1) = "," then modelHF = left(modelHF,len(modelHF)-1)
			else
				modelHF = 2
			end if
		end if
		
		sql = 		"select	case when a.inv_qty > 0 then 1 else 0 end as hasQty, cast('False' as bit) as customize, cast (( case when iv.OriginalPrice > a.price_CO then 1 else 0 end) as bit) as onSale, cast(1 as bit) as alwaysInStock, " & vbcrlf &_
					"	a.itemID, a.brandID, a.modelID, a.typeID, a.subtypeID, a.itemDesc_CO, a.itempic_CO, a.partNumber, a.itempic, " & vbcrlf &_
					"	a.flag1, a.price_Retail, a.price_CO, a.inv_qty, ItemKit_NEW, hotDeal, hideLive, noDiscount,  " & vbcrlf &_
					"	( " & vbcrlf &_
					"		select	COUNT(*) " & vbcrlf &_
					"		from	CO_Reviews " & vbcrlf &_
					"		where	approved = 1 and (itemID = a.itemID or PartNumbers = a.PartNumber) " & vbcrlf &_
					"	) as reviewCnt, " & vbcrlf &_
					"	( " & vbcrlf &_
					"		select		avg(rating) " & vbcrlf &_
					"		from		co_reviews " & vbcrlf &_
					"		where		approved = 1 and (itemid = a.itemid or partnumbers = a.partnumber) " & vbcrlf &_
					"	) as reviewAvg " & vbcrlf &_
					"from	we_Items a left join ItemValue iv  " & vbcrlf &_
					"		on	iv.itemId = a.ItemId and iv.SiteId = 2 " & vbcrlf &_
					"where	price_CO > 0 and typeID = 5 and HandsfreeType in (" & modelHF & ") and brandID = 12  " & vbcrlf &_
					"		and hideLive = 0 and ghost = 0 and a.inv_qty > 0 "
	elseif categoryid = 8 then	'exact same sql but just exclude modelID from the query stmt.
		sql =	"select	case when a.inv_qty > 0 then 1 else 0 end as hasQty, cast('False' as bit) as customize, cast (( case when iv.OriginalPrice > a.price_CO then 1 else 0 end) as bit) as onSale, pnd.alwaysInStock, a.itemid, a.brandid, a.modelid, a.typeid, a.subtypeid, a.itemdesc_co, a.itempic_co, a.partnumber, a.itempic, a.flag1, a.price_retail, a.price_co, a.numberOfSales " & vbcrlf &_
				"	,	case when (select top 1 inv_qty from we_items where partnumber = a.partnumber and master = 1 order by inv_qty desc) > 0 then 1 else 0 end as inv_qty " & vbcrlf &_
				"	,	itemkit_new, hotdeal, hidelive, nodiscount " & vbcrlf &_
				"	,	(	select count(*) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = a.partnumber)) as reviewcnt, (	select avg(rating) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = a.partnumber)) as reviewAvg, c.subTypeOrderNum  " & vbcrlf &_
				"from	we_items a join v_subtypeMatrix_co c " & vbcrlf &_
				"	on	a.subtypeid = c.subtypeid left join we_pnDetails pnd  " & vbcrlf &_
				"	on	a.partNumber = pnd.partNumber left join ItemValue iv  " & vbcrlf &_
				"	on	iv.itemId = a.ItemId and iv.SiteId = 2 " & vbcrlf &_
				"where	a.sports = 0 and c.subtypeid in (" & strSubTypeID & ") and price_co > 0 and hidelive = 0 and ghost = 0  " & vbcrlf &_
				"union  " & vbcrlf &_
				"select	case when b.inv_qty > 0 then 1 else 0 end as hasQty, cast('False' as bit) as customize, cast (( case when iv.OriginalPrice > b.price_CO then 1 else 0 end) as bit) as onSale, pnd.alwaysInStock, b.itemid, b.brandid, b.modelid, b.typeid, b.subtypeid, b.itemdesc_co, b.itempic_co, b.partnumber, b.itempic, b.flag1, b.price_retail, b.price_co, b.numberOfSales  " & vbcrlf &_
				"	,	case when (select top 1 inv_qty from we_items where partnumber = b.partnumber and master = 1 order by inv_qty desc) > 0 then 1 else 0 end as inv_qty " & vbcrlf &_
				"	,	itemkit_new, hotdeal, hidelive, nodiscount " & vbcrlf &_
				"	,	(	select count(*) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = b.partnumber)) as reviewcnt, (	select avg(rating) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = b.partnumber)) as reviewAvg, c.subTypeOrderNum  " & vbcrlf &_
				"from	we_subrelateditems a join we_items b  " & vbcrlf &_
				"	on	a.itemid = b.itemid join v_subtypeMatrix_co c " & vbcrlf &_
				"	on	b.subtypeid = c.subtypeid left join we_pnDetails pnd  " & vbcrlf &_
				"	on	b.partNumber = pnd.partNumber left join ItemValue iv  " & vbcrlf &_
				"	on	iv.itemId = b.ItemId and iv.SiteId = 2 " & vbcrlf &_
				"where	b.price_co > 0 and b.hidelive = 0 and b.ghost = 0 and a.subtypeid in (" & strSubTypeID & ") " & vbcrlf &_
				"union " & vbcrlf &_
				"select	case when b.inv_qty > 0 then 1 else 0 end as hasQty, distinct cast('False' as bit) as customize, cast (( case when iv.OriginalPrice > b.price_CO then 1 else 0 end) as bit) as onSale, pnd.alwaysInStock, b.itemid, b.brandid, b.modelid, b.typeid, b.subtypeid, b.itemdesc_co, b.itempic_co, b.partnumber, b.itempic, b.flag1, b.price_retail, b.price_co, b.numberOfSales  " & vbcrlf &_
				"	,	case when (select top 1 inv_qty from we_items where partnumber = b.partnumber and master = 1 order by inv_qty desc) > 0 then 1 else 0 end as inv_qty " & vbcrlf &_
				"	,	itemkit_new, hotdeal, hidelive, nodiscount " & vbcrlf &_
				"	,	(	select count(*) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = b.partnumber)) as reviewcnt " & vbcrlf &_
				"	,	(	select avg(rating) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = b.partnumber)) as reviewAvg " & vbcrlf &_
				"	,	c.subTypeOrderNum  " & vbcrlf &_
				"from	we_relateditems a join we_items b  " & vbcrlf &_
				"	on	a.itemid = b.itemid left outer join v_subtypeMatrix_co c " & vbcrlf &_
				"	on	b.subtypeid = c.subtypeid left join we_pnDetails pnd  " & vbcrlf &_
				"	on	b.partNumber = pnd.partNumber left join ItemValue iv  " & vbcrlf &_
				"	on	iv.itemId = b.ItemId and iv.SiteId = 2 " & vbcrlf &_
				"where	b.price_co > 0 and b.hidelive = 0 and b.ghost = 0 and a.typeid = '" & categoryid & "' "
	elseif categoryid = 16 then	
		sql	=	"select	case when a.inv_qty > 0 then 1 else 0 end as hasQty, cast('False' as bit) as customize, cast (( case when iv.OriginalPrice > a.price_co then 1 else 0 end) as bit) as onSale, pnd.alwaysInStock, a.itemid, a.brandid, a.modelid, a.typeid, a.subtypeid, a.carrierid, a.itemdesc_co, a.itempic_co, a.partnumber, a.itempic, a.flag1, a.price_retail, a.price_co, a.numberOfSales " & vbcrlf & _
				"	,	case when (select top 1 inv_qty from we_items where partnumber = a.partnumber and master = 1 order by inv_qty desc) > 0 then 1 else 0 end as inv_qty" & vbcrlf & _
				"	,	itemkit_new, hotdeal, hidelive, nodiscount" & vbcrlf & _
				"	,	(	select count(*) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = a.partnumber)) as reviewcnt, (	select avg(rating) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = a.partnumber)) as reviewAvg, c.subTypeOrderNum " & vbcrlf & _
				"from	we_items a join v_subtypeMatrix_co c" & vbcrlf & _
				"	on	a.subtypeid = c.subtypeid" & vbcrlf & _
				"	left join we_pnDetails pnd on a.partNumber = pnd.partNumber " & vbcrlf & _
				"	left join ItemValue iv on iv.itemId = a.ItemId and iv.SiteId = 2" & vbcrlf & _
				"where	a.sports = 0 and c.subtypeid in (" & strSubTypeID & ") and a.brandid = '" & brandid & "' and price_co > 0 and hidelive = 0 and ghost = 0 " & vbcrlf & _
				"union " & vbcrlf & _
				"select	case when b.inv_qty > 0 then 1 else 0 end as hasQty, cast('False' as bit) as customize, cast (( case when iv.OriginalPrice > b.price_co then 1 else 0 end) as bit) as onSale, pnd.alwaysInStock, b.itemid, b.brandid, b.modelid, b.typeid, b.subtypeid, b.carrierid, b.itemdesc_co, b.itempic_co, b.partnumber, b.itempic, b.flag1, b.price_retail, b.price_co, b.numberOfSales " & vbcrlf & _
				"	,	case when (select top 1 inv_qty from we_items where partnumber = b.partnumber and master = 1 order by inv_qty desc) > 0 then 1 else 0 end as inv_qty" & vbcrlf & _
				"	,	itemkit_new, hotdeal, hidelive, nodiscount" & vbcrlf & _
				"	,	(	select count(*) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = b.partnumber)) as reviewcnt, (	select avg(rating) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = b.partnumber)) as reviewAvg, c.subTypeOrderNum " & vbcrlf & _
				"from	we_subrelateditems a join we_items b " & vbcrlf & _
				"	on	a.itemid = b.itemid join v_subtypeMatrix_co c" & vbcrlf & _
				"	on	b.subtypeid = c.subtypeid" & vbcrlf & _
				"	left join we_pnDetails pnd on b.partNumber = pnd.partNumber " & vbcrlf & _
				"	left join ItemValue iv on iv.itemId = b.ItemId and iv.SiteId = 2" & vbcrlf & _
				"where	b.price_co > 0 and b.hidelive = 0 and b.ghost = 0 and a.brandid = '" & brandid & "' and a.subtypeid in (" & strSubTypeID & ")" & vbcrlf & _
				"union" & vbcrlf & _
				"select	case when b.inv_qty > 0 then 1 else 0 end as hasQty, cast('False' as bit) as customize, cast (( case when iv.OriginalPrice > b.price_co then 1 else 0 end) as bit) as onSale, pnd.alwaysInStock, b.itemid, b.brandid, b.modelid, b.typeid, b.subtypeid, b.carrierid, b.itemdesc_co, b.itempic_co, b.partnumber, b.itempic, b.flag1, b.price_retail, b.price_co, b.numberOfSales " & vbcrlf & _
				"	,	case when (select top 1 inv_qty from we_items where partnumber = b.partnumber and master = 1 order by inv_qty desc) > 0 then 1 else 0 end as inv_qty" & vbcrlf & _
				"	,	itemkit_new, hotdeal, hidelive, nodiscount" & vbcrlf & _
				"	,	(	select count(*) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = b.partnumber)) as reviewcnt, (	select avg(rating) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = b.partnumber)) as reviewAvg, c.subTypeOrderNum " & vbcrlf & _
				"from	we_relateditems a join we_items b " & vbcrlf & _
				"	on	a.itemid = b.itemid left outer join v_subtypeMatrix_co c" & vbcrlf & _
				"	on	b.subtypeid = c.subtypeid" & vbcrlf & _
				"	left join we_pnDetails pnd on b.partNumber = pnd.partNumber " & vbcrlf & _
				"	left join ItemValue iv on iv.itemId = b.ItemId and iv.SiteId = 2" & vbcrlf & _
				"where	b.price_co > 0 and b.hidelive = 0 and b.ghost = 0 and a.brandid = '" & brandid & "' and a.typeid = '" & categoryid & "' "
	elseif categoryid = 24 then
		sql =	"select case when a.inv_qty > 0 then 1 else 0 end as hasQty, b.customize, cast (( case when iv.OriginalPrice > a.price_co then 1 else 0 end) as bit) as onSale, cast('False' as bit) as alwaysInStock, (	select count(*) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = a.partnumber)) as reviewcnt, (	select avg(rating) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = a.partnumber)) as reviewAvg, a.* " & vbcrlf & _
				"from we_Items a " & vbcrlf & _
					"left join we_ItemsExtendedData b on a.partNumber = b.partNumber " & vbcrlf & _
					"left join ItemValue iv on iv.itemId = a.ItemId and iv.SiteId = 2 " & vbcrlf & _
				"where b.customize = 1 and a.modelID = " & modelID & " and a.price_co > 0 and a.hideLive = 0 and a.ghost = 0"
	else
		sql	=	"select	case when a.inv_qty > 0 then 1 else 0 end as hasQty, f.customize, cast (( case when iv.OriginalPrice > a.price_co then 1 else 0 end) as bit) as onSale, pnd.alwaysInStock, a.itemid, a.brandid, a.modelid, a.typeid, a.subtypeid, a.itemdesc_co, a.itempic_co, a.partnumber, a.itempic, a.flag1, a.price_retail, a.price_co, a.numberOfSales " & vbcrlf & _
				"	,	case when (select top 1 inv_qty from we_items where partnumber = a.partnumber and master = 1 order by inv_qty desc) > 0 then 1 else 0 end as inv_qty" & vbcrlf & _
				"	,	itemkit_new, hotdeal, hidelive, nodiscount" & vbcrlf & _
				"	,	(	select count(*) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = a.partnumber)) as reviewcnt, (	select avg(rating) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = a.partnumber)) as reviewAvg, c.subTypeOrderNum " & vbcrlf & _
				"from	we_items a join v_subtypeMatrix_co c" & vbcrlf & _
				"	on	a.subtypeid = c.subtypeid" & vbcrlf & _
				"	left join we_pnDetails pnd on a.partNumber = pnd.partNumber " & vbcrlf & _
				"	left join ItemValue iv on iv.itemId = a.ItemId and iv.SiteId = 2" & vbcrlf & _
				"	left JOIN we_ItemsExtendedData f ON A.partnumber = f.partnumber " & vbcrlf &_
				"where	a.sports = 0 and c.subtypeid in (" & strSubTypeID & ") and a.modelid = '" & modelID & "' and price_co > 0 and hidelive = 0 and ghost = 0 " & vbcrlf & _
				"union " & vbcrlf & _
				"select	case when b.inv_qty > 0 then 1 else 0 end as hasQty, f.customize, cast (( case when iv.OriginalPrice > b.price_co then 1 else 0 end) as bit) as onSale, pnd.alwaysInStock, b.itemid, b.brandid, b.modelid, b.typeid, b.subtypeid, b.itemdesc_co, b.itempic_co, b.partnumber, b.itempic, b.flag1, b.price_retail, b.price_co, b.numberOfSales " & vbcrlf & _
				"	,	case when (select top 1 inv_qty from we_items where partnumber = b.partnumber and master = 1 order by inv_qty desc) > 0 then 1 else 0 end as inv_qty" & vbcrlf & _
				"	,	itemkit_new, hotdeal, hidelive, nodiscount" & vbcrlf & _
				"	,	(	select count(*) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = b.partnumber)) as reviewcnt, (	select avg(rating) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = b.partnumber)) as reviewAvg, c.subTypeOrderNum " & vbcrlf & _
				"from	we_subrelateditems a join we_items b " & vbcrlf & _
				"	on	a.itemid = b.itemid join v_subtypeMatrix_co c" & vbcrlf & _
				"	on	b.subtypeid = c.subtypeid" & vbcrlf & _
				"	left join we_pnDetails pnd on b.partNumber = pnd.partNumber " & vbcrlf & _
				"	left join ItemValue iv on iv.itemId = b.ItemId and iv.SiteId = 2" & vbcrlf & _
				"	left JOIN we_ItemsExtendedData f ON b.partnumber = f.partnumber " & vbcrlf &_
				"where	b.price_co > 0 and b.hidelive = 0 and b.ghost = 0 and a.modelid = '" & modelID & "' and a.subtypeid in (" & strSubTypeID & ")" & vbcrlf & _
				"union" & vbcrlf & _
				"select	case when b.inv_qty > 0 then 1 else 0 end as hasQty, f.customize, cast (( case when iv.OriginalPrice > b.price_co then 1 else 0 end) as bit) as onSale, pnd.alwaysInStock, b.itemid, b.brandid, b.modelid, b.typeid, b.subtypeid, b.itemdesc_co, b.itempic_co, b.partnumber, b.itempic, b.flag1, b.price_retail, b.price_co, b.numberOfSales " & vbcrlf & _
				"	,	case when (select top 1 inv_qty from we_items where partnumber = b.partnumber and master = 1 order by inv_qty desc) > 0 then 1 else 0 end as inv_qty" & vbcrlf & _
				"	,	itemkit_new, hotdeal, hidelive, nodiscount" & vbcrlf & _
				"	,	(	select count(*) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = b.partnumber)) as reviewcnt, (	select avg(rating) from co_reviews where approved = 1 and (itemid = a.itemid or partnumbers = b.partnumber)) as reviewAvg, c.subTypeOrderNum " & vbcrlf & _
				"from	we_relateditems a join we_items b " & vbcrlf & _
				"	on	a.itemid = b.itemid left outer join v_subtypeMatrix_co c" & vbcrlf & _
				"	on	b.subtypeid = c.subtypeid" & vbcrlf & _
				"	left join we_pnDetails pnd on b.partNumber = pnd.partNumber " & vbcrlf & _
				"	left join ItemValue iv on iv.itemId = b.ItemId and iv.SiteId = 2" & vbcrlf & _
				"	left JOIN we_ItemsExtendedData f ON b.partnumber = f.partnumber " & vbcrlf &_
				"where	b.price_co > 0 and b.hidelive = 0 and b.ghost = 0 and a.modelid = '" & modelID & "' and a.typeid = '" & categoryid & "' "
	end if
	
	if musicSkins = 0 then
		if sortBy = "" or sortBy = "lh" then sql = sql & "ORDER BY hasQty desc, A.price_CO"
		if sortBy = "hl" then sql = sql & "ORDER BY hasQty desc, A.price_CO desc"
		if sortBy = "bs" then sql = sql & "ORDER BY hasQty desc, numberOfSales desc"
	end if
	
	if sortBy = "hl" then sortBy = "high"
	if sortBy = "lh" then sortBy = "low"
	if sortBy = "bs" then sortBy = "pop"
	
	if categoryID = 5 then
		sql = "exec sp_bmcdProducts " & modelID & ", 5, '" & sortBy & "', 2, 0, 0, 5"
	elseif categoryID = 8 then
		sql = "exec sp_bmcdProducts " & modelID & ", 8, '" & sortBy & "', 2, 0, 0, 8"
	elseif categoryID = 16 then
		sql = "exec sp_bmcdProducts	0, " & categoryID & ", '" & sortBy & "', 2, 0, " & brandID & ", 0"
	else
		sql = "exec sp_bmcdProducts	" & modelID & ", " & categoryID & ", '" & sortBy & "', 2, 0, " & brandID & ", 0"
	end if
	
	session("errorSQL") = SQL
'	response.write "<pre>" & sql & "</pre>"
	set rs = oConn.execute(sql)
	
	noProducts = 0
	if RS.EOF then noProducts = 1
%>
<div id="noProducts" class="alertBoxHidden">
    No products match your current filter settings<br />
    Please try to adjust you filter to see more products
</div>
<%
	dim altText, DoNotDisplay, RSkit, RSextra
	a = 0
	
	set fsThumb = CreateObject("Scripting.FileSystemObject")
	curID = 0
	visCnt = 0
	useClass = "bmc_productBox"
	do until RS.eof
		visCnt = visCnt + 1
		DoNotDisplay = 0
		outOfStock = false
		
		customize = rs("customize")
		if isnull(customize) then customize = false
		alwaysInStock = rs("alwaysInStock")
		if isnull(alwaysInStock) then alwaysInStock = false

		filterTypeID = prepInt(rs("subtypeID"))
		if categoryid = 16 then filterTypeID = prepInt(rs("carrierid"))
		
		flag1 = prepInt(rs("flag1"))
		hotDeail = rs("hotDeal")
		reviewCnt = prepInt(rs("reviews"))
		reviewAvg = prepInt(rs("reviewAvg"))
		noDiscount = rs("noDiscount")
		
		itemID = prepInt(rs("itemID"))
		typeID = prepInt(rs("typeID"))
		itemDesc_CO = rs("itemDesc_CO")
		itemPic_CO = rs("itempic_CO")
		partNumber = prepStr(rs("partNumber"))
		price_Retail = prepInt(rs("price_Retail"))
		price_CO = prepInt(rs("price_CO"))
		invQty = prepInt(rs("inv_qty"))
		ItemKit_NEW = rs("ItemKit_NEW")
		onSale = rs("onSale")
		
		if invQty = 0 and not alwaysInStock then
			outOfStock = true
			if reviewCnt < 1 then DoNotDisplay = 1
		end if
		if curID = itemID then
			DoNotDisplay = 1
		else
			curID = itemID
		end if
		curPN = partNumber
		partNumber = curPN
		useItemDesc = itemDesc_CO
		if not isNull(ItemKit_NEW) then
			SQL = "SELECT inv_qty FROM we_Items WHERE itemID IN (" & ItemKit_NEW & ")"
			set RSkit = Server.CreateObject("ADODB.recordset")
			RSkit.open SQL, oConn, 0, 1
			do until RSkit.eof
				if RSkit("inv_qty") < 1 then DoNotDisplay = 1
				RSkit.movenext
			loop
			RSkit.close
			set RSkit = nothing
		end if
		
		'if ((categoryid = 3 or categoryid = 7) and typeid = 19) then DoNotDisplay = 1 end if 'skip decal skins
		 
		itemImgPath = server.MapPath("/productPics/thumb/" & itempic_CO)
		if not fsThumb.FileExists(itemImgPath) then
			useImg = "/productPics/thumb/imagena.jpg"
			DoNotDisplay = 1
		else
			useImg = "/productPics/thumb/" & itempic_CO
		end if
		if musicSkins = 1 then
			DoNotDisplay = 0
			if isnull(RS("preferredImg")) then																					
				if hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsSmall", itempic_CO) then
					sql = "update we_items_musicSkins set preferredImg = 1 where id = " & itemID
					session("errorSQL") = sql
					oConn.execute(sql)
					useImg = "/productpics/musicSkins/musicSkinsSmall/" & itempic_CO
				elseif hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault/thumbs", RS("defaultImg")) then
					sql = "update we_items_musicSkins set preferredImg = 2 where id = " & itemID
					session("errorSQL") = sql
					oConn.execute(sql)
					useImg = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & RS("defaultImg")
				else
					useImg = "/productPics/thumb/imagena.jpg"
					'DoNotDisplay = 1
				end if
			elseif RS("preferredImg") = 1 then
				useImg = "/productpics/musicSkins/musicSkinsSmall/" & itempic_CO
			elseif RS("preferredImg") = 2 then
				useImg = "/productpics/musicSkins/musicSkinsDefault/thumbs/" & RS("defaultImg")
			else
				useImg = "/productPics/thumb/imagena.jpg"
				'DoNotDisplay = 1
			end if
		elseif instr(curPN,"DEC-SKN") > 0 then
			useImg = "/productpics/decalSkins/thumb/" & itempic	
		end if
		
		if DoNotDisplay = 0 then
			picLap = picLap + 1
			itemDesc_CO = cleanMP(replace(itemDesc_CO,"  "," "))
			useItemDesc = cleanMP(replace(useItemDesc,"  "," "))
			if brandID = "2" and modelid = "510" and categoryid = "3" then
				altText = "Sony Ericsson W580i Covers"
			elseif brandID = "14" and modelid = "599" and categoryid = "3" then
				altText = "Blackberry Curve 8330 Covers"
			else
				altText = brandName & " " & modelName & " " & singularSEO(categoryName) & ": " & itemDesc_CO
			end if
			if Bling = true then altText = "Cell Phone Charms & Bling : " & altText
			if useClass = "bmc_productBox fl" then showDivs = showDivs & picLap & ","
	%>
	<div id="itemListID_<%=picLap%>" class="<%=useClass%>">
		<div class="bmc_productPic fl" id="picDiv_<%=picLap%>" title="<%=altText%>">
			<% if onSale then%>
				<div class="bmc_productOnSale onSale"></div>
			<% else %>
				<div class="bmc_productOnSale"></div>
			<% end if %>
			<a class="static-content-font-link" href="/p-<%=itemid & "-" & formatSEO(useItemDesc)%>.html" title="<%=altText%>"<% if prepInt(hrefNoFollow) = 1 then %> rel="nofollow"<% end if %>><tempimg id="prodPic" src="<%=useImg%>" border="0" class="clickable"></a>
			<% if customize then %><div class="customBadge"><img src="/images/custom/co-customize-sm.png" border="0" width="70" height="70" /></div><% end if %>
		</div>
		<div class="bmc_productTitle fl" title="<%=altText%>"><a class="static-content-font-link" href="/p-<%=itemid & "-" & formatSEO(useItemDesc)%>.html" title="<%=altText%>"<% if prepInt(hrefNoFollow) = 1 then %> rel="nofollow"<% end if %>><%=useItemDesc%></a></div>
		<div class="bmc_rating fl"><% if reviewAvg > 0 then %><%=getRatingAvgStar(reviewAvg)%><% end if %></div>
	<% if outOfStock then %>
		<div class="bmc_outOfStock fl">OUT OF STOCK</div>
	<% else %>
		<div class="bmc_pricing_old">
			<div class="bmc_retailPrice fl">Retail Price: <s><%=formatCurrency(price_retail)%></s></div>
				<%
				dim holidayPricing : holidayPricing = 0
				if holidaySale = 1 then
					partNumber3 = ""
					if not isnull(curPN) then partNumber3 = left(curPN, 3) end if
					if instr(holidaySalePartNum(0), "|" & partNumber3 & "|") > 0 then
						holidayPricing = price_CO - (price_CO * holidaySalePercent(0))
					elseif instr(holidaySalePartNum(1), "|" & partNumber3 & "|") > 0 then
						holidayPricing = price_CO - (price_CO * holidaySalePercent(1))
					end if
				end if
			
				if holidayPricing > 0 and not noDiscount then
				%>
			<div class="bmc_wholesale1 fl">Wholesale Price: <span class="red-price-small"><s><%=formatCurrency(price_CO)%></s></span></div>
			<div class="bmc_holidayPrice fl">You Pay: <span class="red-price-small"><%=formatCurrency(holidayPricing)%></span></div>
				<% else %>
			<div class="bmc_wholesale2 fl">Wholesale Price: <span class="red-price-small"><%=formatCurrency(price_CO)%></span></div>
				<% end if %>
		</div>
		<div class="bmc_pricing_new">
			<div style="float:left; width:100%; text-align:center; padding-top:10px; font-size:18px; color:#333; font-weight:bold;"><%=formatCurrency(price_CO,2)%></div>
			<div style="float:left; width:100%; text-align:center; padding-top:5px; font-size:12px; color:#3C599B;">(<%=formatPercent((price_retail - price_CO) / price_retail,0)%> OFF Retail Price: <del><%=formatcurrency(price_retail,2)%></del>)</div>
		</div>
	<% end if %>
		<div class="bmc_moreInfo fl"><a href="/p-<%=itemid & "-" & formatSEO(useItemDesc)%>.html"<% if prepInt(hrefNoFollow) = 1 then %> rel="nofollow"<% end if %>><img src="/images/moreInfo.png" width="94" height="20" border="0" alt="More Info"></a></div>
		<div class="bmc_addtocart">
			<form action="/cart/item_add.asp" method="post" style="margin:0px; padding:0px;">
				<input name="qty" type="hidden" value="1" size="2">
				<input name="prodid" type="hidden" value="<%=itemID%>">
				<input type="image" src="/images/bmc/button-addcart.jpg" border="0" alt="Add To Cart" />
			</form>
		</div>
		<div id="subType_<%=picLap%>" style="display:none;"><%=filterTypeID%></div>
		<div id="price_<%=picLap%>" style="display:none;"><%=price_CO%></div>
		<div id="custom_<%=picLap%>" style="display:none;"><%=customize%></div>
	</div>
	<%
		end if
		RS.movenext
		if visCnt = productsPerPage then useClass = "bmc_productBox2 fl"
		if usePages = 5 then
			if picLap = productsPerPage then exit do
		end if
	loop

	call CloseConn(oConn)
	
	function getRatingAvgStar(rating)
		dim nRating 
		dim strRatingImg
		nRating = cdbl(rating)
		strRatingImg = 	"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
						"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
						"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
						"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
						"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />"
		
		if nRating > 0 or nRating <=5 then
			strRatingImg = ""	
			for i=1 to 5
				if nRating => i then
					strRatingImg = strRatingImg & "<img src=""/images/review/greenStarFull.gif"" border=""0"" width=""14"" height=""13"" /> "
				elseif nRating => ((i - 1) + .1) then
					strRatingImg = strRatingImg & "<img src=""/images/review/greenStarHalf.gif"" border=""0"" width=""8"" height=""13"" /> "				
				else
					strRatingImg = strRatingImg & "<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" /> "
				end if
			next		
		end if
		
		getRatingAvgStar = strRatingImg
	end function
%>