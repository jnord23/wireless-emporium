<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!-- METADATA TYPE="typelib" NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<%
	reviewID = prepInt(request.queryString("reviewID"))
	isThumbsUp = prepInt(request.queryString("isThumbsUp"))
	isThumbsDown = 1
	if isThumbsUp = 1 then isThumbsDown = 0
	
	Dim cmd
	Set cmd = Server.CreateObject("ADODB.Command")
	Set cmd.ActiveConnection = oConn
	cmd.CommandText = "updateXReview_Thumbs"
	cmd.CommandType = adCmdStoredProc 

	cmd.Parameters.Append cmd.CreateParameter("id", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("isThumbsUp", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("isThumbsDown", adInteger, adParamInput)

	cmd("id")			=	reviewID
	cmd("isThumbsUp")	=	isThumbsUp
	cmd("isThumbsDown")	=	isThumbsDown
	
	cmd.Execute	
	
	response.write "updated"
	
	call CloseConn(oConn)
%>
