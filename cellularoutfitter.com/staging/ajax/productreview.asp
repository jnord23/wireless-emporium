<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
itemid 			= 	prepInt(request.QueryString("itemid"))
screenHeight	=	prepInt(request.QueryString("screenH"))
newHeightSize	=	700
if screenHeight <= 800 and screenHeight <> 0 then
	newHeightSize = screenHeight - 150
end if

call CloseConn(oConn)
%>
<div style="width:800px; text-align:left; font-size:15px; margin-left:auto; margin-right:auto; margin-top:100px; position:relative; border:1px solid #ccc; background-color:#fff; color:#333; border-radius:5px; font-family:Arial, Helvetica, sans-serif; -moz-box-shadow: 0px 0px 5px 5px #333; -webkit-box-shadow: 0px 0px 5px 5px #333; box-shadow: 0px 0px 5px 5px #333;">
	<div style="position:absolute; top:-15px; right:-10px; cursor:pointer;" onclick="closeReviewPopup()"><img src="/images/icon-lightbox-close.png" border="0" /></div>
	<div style="margin:10px 0px 10px 0px; padding:10px 30px 10px 10px; width:760px; height:<%=newHeightSize%>px; overflow:auto;">
        <form name="frmProductReview" method="post">
        <div style="padding-top:5px; font-size:36px; color:#0F6F88; text-align:center;">Write a review of this product!</div>    
        <div style="padding-top:10px; line-height:22px;">
            Thank you for sharing your thoughts about one of our products! Please focus on the product performance and quality. The more information you provide, the more useful your review will be to others. Your review may be posted on CellularOutfitter.com
        </div>    
        <div style="padding-top:10px; text-align:right;">
            <span style="color:#900; font-size:20px;">*</span> = Required
        </div>
        <div style="border-radius:5px; background-color:#f7f7f7; padding:10px; width:740px; margin-top:15px;">
            <div style="font-weight:bold; font-size:17px;">Product Rating:</div>
            <div style="border-top:1px dashed #ccc; margin-top:5px;"></div>
            <div style="padding-top:10px; display:table;">
                <div style="float:left; width:250px;">Click stars to rate product <span style="color:#900; font-size:20px;">*</span></div>
                <div style="float:left; width:150px;">
                    <img id="reviewStar1" src="/images/product/review/star-full.png" border="0" style="cursor:pointer;" onClick="rateReview(1)" />
                    <img id="reviewStar2" src="/images/product/review/star-full.png" border="0" style="cursor:pointer;" onClick="rateReview(2)" />
                    <img id="reviewStar3" src="/images/product/review/star-full.png" border="0" style="cursor:pointer;" onClick="rateReview(3)" />
                    <img id="reviewStar4" src="/images/product/review/star-full.png" border="0" style="cursor:pointer;" onClick="rateReview(4)" />
                    <img id="reviewStar5" src="/images/product/review/star-full.png" border="0" style="cursor:pointer;" onClick="rateReview(5)" />
                </div>
                <div style="float:left; width:200px;"><span id="reviewStarDesc1" style="font-weight:bold;">5 Stars</span> &nbsp; &nbsp; <span id="reviewStarDesc2">Excellent</span></div>
                <input id="id_hidStar" type="hidden" name="hidStar" value="5" />
            </div>
            <div style="padding-top:10px; display:table;">
                <div style="float:left; width:250px;">Do you recommend this product? <span style="color:#900; font-size:20px;">*</span></div>
                <div style="float:left; font-size:14px;">
                    <input type="radio" name="rdoRecommend" value="1" checked /> YES &nbsp; &nbsp;
                    <input type="radio" name="rdoRecommend" value="0" /> NO
                </div>
            </div>
        </div>
        <div style="border-radius:5px; background-color:#f7f7f7; padding:10px; width:740px; margin-top:15px;">
            <div style="font-weight:bold; font-size:17px;">Pros:</div>
            <div style="border-top:1px dashed #ccc; margin-top:5px;"></div>
            <div style="padding-top:10px; display:table;">
                <div style="float:left; width:250px;">Check all that apply:</div>
                <div style="float:left; width:200px;">
                    <div><input type="checkbox" name="chkPros1" value="1" /> Great Price</div>
                    <div style="padding-top:5px;"><input type="checkbox" name="chkPros2" value="1" /> Beautifully Designed</div>
                    <div style="padding-top:5px;"><input type="checkbox" name="chkPros3" value="1" /> Functional</div>
                    <div style="padding-top:5px;"><input type="checkbox" name="chkPros4" value="1" /> Convenient</div>
                </div>
                <div style="float:left;">
                    <div>Add your own:</div>
                    <div style="padding-top:5px;"><textarea name="txtPros" cols="30" rows="2"></textarea></div>
                </div>
            </div>
        </div>
        <div style="border-radius:5px; background-color:#f7f7f7; padding:10px; width:740px; margin-top:15px;">
            <div style="font-weight:bold; font-size:17px;">Cons:</div>
            <div style="border-top:1px dashed #ccc; margin-top:5px;"></div>
            <div style="padding-top:10px; display:table;">
                <div style="float:left; width:250px;">Check all that apply:</div>
                <div style="float:left; width:200px;">
                    <div><input type="checkbox" name="chkCons1" value="1" /> Poor Value</div>
                    <div style="padding-top:5px;"><input type="checkbox" name="chkCons2" value="1" /> Doesn't Fit</div>
                    <div style="padding-top:5px;"><input type="checkbox" name="chkCons3" value="1" /> Not What I Expected</div>
                    <div style="padding-top:5px;"><input type="checkbox" name="chkCons4" value="1" /> Doesn't Work Well</div>
                </div>
                <div style="float:left;">
                    <div>Add your own:</div>
                    <div style="padding-top:5px;"><textarea name="txtCons" cols="30" rows="2"></textarea></div>
                </div>
            </div>
        </div>
        <div style="border-radius:5px; background-color:#f7f7f7; padding:10px; width:740px; margin-top:15px;">
            <div style="font-weight:bold; font-size:17px;">Your Review:</div>
            <div style="border-top:1px dashed #ccc; margin-top:5px;"></div>
            <div style="padding-top:10px; display:table;">
                <div style="float:left; width:250px;">Email: <span style="color:#900; font-size:20px;">*</span></div>
                <div style="float:left;">
                	<div><input type="text" class="" name="txtEmail" style="width:400px;" maxlength="100" /></div>
                    <div id="msgEmail" style="color:#cd0000; display:none;">Please enter the valid email address!</div>
				</div>
            </div>
            <div style="padding-top:10px; display:table;">
                <div style="float:left; width:250px;">Review title: <span style="color:#900; font-size:20px;">*</span></div>
                <div style="float:left;">
                	<div><input type="text" name="txtReviewTitle" style="width:400px;" maxlength="95" /></div>
                    <div id="msgReviewTitle" style="color:#cd0000; display:none;">Please enter review title!</div>
				</div>
            </div>
            <div style="padding-top:10px; display:table;">
                <div style="float:left; width:250px;">Review: <span style="color:#900; font-size:20px;">*</span></div>
                <div style="float:left;">
                	<div><textarea name="txtReview" style="width:400px; height:200px;"></textarea></div>
                    <div id="msgReview" style="color:#cd0000; display:none;">Please enter review!</div>
				</div>
            </div>
        </div>
        <div style="border-radius:5px; background-color:#f7f7f7; padding:10px; width:740px; margin-top:15px;">
            <div style="font-weight:bold; font-size:17px;">Your Information:</div>
            <div style="border-top:1px dashed #ccc; margin-top:5px;"></div>
            <div style="padding-top:10px; display:table;">
                <div style="float:left; width:250px;">Nickname: <span style="color:#900; font-size:20px;">*</span></div>
                <div style="float:left;">
                	<div><input type="text" name="txtNickname" style="width:400px;" maxlength="50" /></div>
                    <div id="msgNickname" style="color:#cd0000; display:none;">Please enter nickname!</div>
				</div>
            </div>
            <div style="padding-top:10px; display:table;">
                <div style="float:left; width:250px;">Location:</div>
                <div style="float:left;"><input type="text" name="txtLocation" style="width:400px; color:#999;" maxlength="50" /></div>
                <div style="clear:both;"></div>
                <div style="float:left; padding-left:250px; font-size:12px; font-style:italic; color:#888;">City Name, State</div>
            </div>
            <div style="padding-top:10px; display:table;">
                <div style="float:left; width:250px;">Age:</div>
                <div style="float:left;">
                    <select name="cbAge" style="width:200px;">
                        <option value="0">Please Select</option>
                        <option value="1">17 & under</option>
                        <option value="2">18 - 25</option>
                        <option value="3">26 - 35</option>
                        <option value="4">36 - 45</option>
                        <option value="5">46 - 55</option>
                        <option value="6">56+</option>
                    </select>
                </div>
            </div>
            <div style="padding-top:10px; display:table;">
                <div style="float:left; width:250px;">Gender:</div>
                <div style="float:left;">
                    <select name="cbGender" style="width:200px;">
                        <option value="">Please Select</option>
                        <option value="M">Male</option>
                        <option value="F">Female</option>
                    </select>
                </div>
            </div>
            <div style="padding-top:10px; display:table;">
                <div style="float:left; width:250px;">Ownership:</div>
                <div style="float:left;">
                    <select name="cbOwnership" style="width:200px;">
                        <option value="0">Please Select</option>
                    	<option value="1">2 ~ 4 Weeks</option>
                    	<option value="2">1 ~ 2 Months</option>
                    	<option value="3">3 ~ 12 Months</option>
                    	<option value="4">1 year +</option>
                    </select>
                </div>
            </div>
            <div style="padding-top:10px; display:table;">
                <div style="float:left; width:250px;">Describe yourself:</div>
                <div style="float:left;">
                    <div><input type="radio" name="rdoJob" value="STU" checked /> Student</div>
                    <div style="padding-top:5px;"><input type="radio" name="rdoJob" value="BO" /> Business Owner</div>
                    <div style="padding-top:5px;"><input type="radio" name="rdoJob" value="TE" /> Technology Enthusiast</div>
                    <div style="padding-top:5px;"><input type="radio" name="rdoJob" value="PA" /> Phone Addict</div>
                    <div style="padding-top:5px;"><input type="radio" name="rdoJob" value="CPU" /> Casual Phone User</div>
                </div>
            </div>
        </div>
        <div style="margin:15px 0px 30px 0px; text-align:center;">
        	<img src="/images/product/review/cta-submit-review.png" border="0" alt="Submit Review" onClick="validateReview()" style="cursor:pointer;" />
        </div>
        </form>
    </div>
</div>