<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Get 20&#37; Off Heavy-Duty Phone Cases. Limited Time Offer.</title>
    <style type="text/css">
        .ReadMsgBody {
        	width: 100%;
        }
        .ExternalClass {
        	width: 100%;
        }
    </style>
</head>

<body style="margin: 0; padding: 0; width: 100%; -webkit-text-size-adjust: none !important;	-ms-text-resize: none !important;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="background-color: #f7f7f7; -webkit-text-size-adjust: none;" width="100%" align="center">
                <!-- Content -->

                <table style="margin: 0;" width="640" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 2.0;text-align:center;" width="640" height="40" align="left" valign="middle">
                            Having trouble viewing this email?
                            <a style="color: #006699;" href="{{browser_link}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_code}}&utm_promocode={{promo_code}}&utm_email={{{email_address}}}" target="_blank">Click here</a>.
                    </tr>
                    <tr>
                        <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px;" width="640" height="30" align="center" valign="middle">
							Save <b>25%</b> On Cell Phone Accessories - Use Coupon Code <b>WELCOME25</b> @ Checkout! Expires {{expDate}}.
						</td>
                    </tr>
                </table>
                <table width="640" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="background-color: #ffffff; color: #999999; font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 175%;" width="240" height="80" align="center" valign="middle">
                            <a href="http://www.cellularoutfitter.com/?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_code}}&utm_promocode={{promo_code}}&utm_email={{{email_address}}}" target="_blank">
                                <img style="display: block;" src="http://www.cellularoutfitter.com/images/email/blast/template/images/co-logo.gif" width="220" height="45" alt="CellularOutfitter.com" border="0" />
                            </a>
                            <b>WHOLESALE PRICES</b> ON ACCESSORIES</td>
                        <td style="background-color: #ffffff;" width="390" align="right" valign="middle">
                            <a href="http://www.cellularoutfitter.com/?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_code}}&utm_promocode={{promo_code}}&utm_email={{{email_address}}}" target="_blank">
                                <img src="http://www.cellularoutfitter.com/images/email/blast/template/images/co-values.gif" width="300" height="38" alt="110% Low Price Guarantee | 90-Day Return Policy" border="0" />
                            </a>
                        </td>
                        <td style="background-color: #ffffff;" width="10"></td>
                    </tr>
                </table>
                <table width="640" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="45" align="center" valign="middle">
                            <a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=3}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_code}}&utm_promocode={{promo_code}}&utm_email={{{email_address}}}" target="_blank">Covers &amp; Skins</a>
                        </td>
                        <td style="background-color: #124f80;" width="1" height="45"></td>
                        <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="45" align="center" valign="middle">
                            <a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=7}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_code}}&utm_promocode={{promo_code}}&utm_email={{{email_address}}}" target="_blank">Cases &amp; Pouches</a>
                        </td>
                        <td style="background-color: #124f80;" width="1" height="45"></td>
                        <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="45" align="center" valign="middle">
                            <a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=18}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_code}}&utm_promocode={{promo_code}}&utm_email={{{email_address}}}" target="_blank">Screen Protectors</a>
                        </td>
                        <td style="background-color: #124f80;" width="1" height="45"></td>
                        <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="45" align="center" valign="middle">
                            <a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=24}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_code}}&utm_promocode={{promo_code}}&utm_email={{{email_address}}}" target="_blank">Custom Covers</a>
                        </td>
                        <td style="background-color: #124f80;" width="1" height="45"></td>
                        <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="45" align="center" valign="middle">
                            <a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=2}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_code}}&utm_promocode={{promo_code}}&utm_email={{{email_address}}}" target="_blank">Chargers Cables</a>
                        </td>
                        <td style="background-color: #124f80;" width="1" height="45"></td>
                        <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="45" align="center" valign="middle">
                            <a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=5}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_code}}&utm_promocode={{promo_code}}&utm_email={{{email_address}}}" target="_blank">Bluetooth Heasdsets</a>
                        </td>
                        <td style="background-color: #124f80;" width="1" height="45"></td>
                        <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="79" height="45" align="center" valign="middle">
                            <a style="color: #ffffff; text-decoration: none;" href="{{bmcd_link;typeID=6}}?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_code}}&utm_promocode={{promo_code}}&utm_email={{{email_address}}}" target="_blank">Holsters Mounts</a>
                        </td>
                        <td style="background-color: #124f80;" width="1" height="45"></td>
                        <td style="background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;" width="80" height="45" align="center" valign="middle">
                            <a style="color: #ffffff; text-decoration: none;" href="http://www.cellularoutfitter.com/sb-0-sm-0-sc-8-cell-phone-other-accessories.html?utm_source=streamsend&utm_medium=email2&utm_campaign={{campaign_code}}&utm_promocode={{promo_code}}&utm_email={{{email_address}}}" target="_blank">Other Accessories</a>
                        </td>
                    </tr>
                </table>
				<table width="640" border="0" cellspacing="0" cellpadding="0" style="background-color:#ffffff;">
					<tr>
						<td style="width:640px;height:200px;padding:0;margin:0;">
							<div style="width:640px;height:200px;position:relative;float;left;"><img src="http://www.cellularoutfitter.com/images/confirm-email/CO-shipping-confirmation-banner.jpg" width="640" height="200" style="margin:0;padding:0;" /></div>
						</td>
					</tr>
					<tr>
						<td width="640">
							<p style="margin:20px 10px 0 10px; font-family:Verdana, Helvetica, sans-serif;font-size:14px;text-align:center;">Your order <b>XXXXXXX</b> has shipped.</p>
							<p style="margin:10px 10px 0 10px; font-family:Verdana, Helvetica, sans-serif;font-size:14px;text-align:center;">Tracking <span style="color:#0099ff;font-weight:bold;text-decoration:underline">#90DSFBS7SDF9V</span></p>
							<p style="margin:10px 10px 0 10px; font-family:Verdana, Helvetica, sans-serif;font-size:11px;text-align:center;font-weight:bold;">We've carefully packed your merchandise and sent it along it's way. Expect to see it soon.</p>
							<p style="margin:10px 10px 0 10px; font-family:Verdana, Helvetica, sans-serif;font-size:11px;text-align:center;font-weight:bold;">We appreciate your business.</p>
							<table width="620" border="0" cellpadding="0" cellspacing="0" style="margin:10px;">
								<thead>
									<tr>
										<td style="padding:5px;text-align:left;border-top-left-radius:5px;background:#486084;color:#ffffff;font-weight:bold;">Item #</td>
										<td style="padding:5px;background:#486084;color:#ffffff;font-weight:bold;width:76px;">Product</td>
										<td style="padding:5px;background:#486084;color:#ffffff;font-weight:bold;">Qty</td>
										<td style="padding:5px;background:#486084;color:#ffffff;font-weight:bold;">Description</td>
										<td style="padding:5px;background:#486084;color:#ffffff;font-weight:bold;">Price</td>
										<td style="padding:5px;border-top-right-radius:5px;background:#486084;color:#ffffff;font-weight:bold;">Total</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="font-size:12px;text-align:left;vertical-align:top;padding:10px 5px;border-left:1px solid #ccc;border-bottom:1px solid #ccc;border-right:1px solid #ccc;">FLP-SAM-SGS4-S10</td>
										<td style="font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;"><img src="http://www.cellularoutfitter.com/images/confirm-email/product.png" width="66" height="65" /></td>
										<td style="font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;">1</td>
										<td style="font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;">Samsung Galaxy S4 S-View Flip (Black)</td>
										<td style="font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;">$9.99</td>
										<td style="font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;">$9.99</td>
									</tr>
								</tbody>
							</table>
							<h2 style="text-align:left;padding:5px;margin:5px;color:#006596;font-weight:bold;font-size:16px;border-bottom:1px solid #cccccc;">Invoice Details</h2>
							<p style="margin:0 0 10px 10px;text-align:left;font-size:12px;color:#7d7d7d;">Order ID: XXXXXXXXXX</p>
							<p style="margin:0 0 10px 10px;text-align:left;font-size:12px;color:#7d7d7d;">Shipping Method: USPS First Class (4-10 business days)</p>
							<p style="margin:0 0 10px 10px;text-align:left;font-size:12px;color:#7d7d7d;">Track Your Package: 90DSFBS7SDF9V</p>
							<table width="640" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td style="width:315px;padding:10px 5px 10px 0;text-align:left;">
										<h2 style="text-align:left;padding:5px;margin:5px;color:#006596;font-weight:bold;font-size:16px;border-bottom:1px solid #cccccc;">Billing Address</h2>
										<p style="margin:0 0 10px 10px;text-align:left;font-size:12px;color:#7d7d7d;">Mykala Ly</p>
										<p style="margin:0 0 10px 10px;text-align:left;font-size:12px;color:#7d7d7d;">12345 Brodard Rd.</p>
										<p style="margin:0 0 10px 10px;text-align:left;font-size:12px;color:#7d7d7d;">Palo Alto, California, 94306</p>
										<p style="margin:0 0 10px 10px;text-align:left;font-size:12px;color:#7d7d7d;">United States</p>
										<p style="margin:0 0 10px 10px;text-align:left;font-size:12px;color:#7d7d7d;">T: 650-555-4321</p>
									</td>
									<td style="width:315px;padding:10px 0px 10px 5px;text-align:left;">
										<h2 style="text-align:left;padding:5px;margin:5px;color:#006596;font-weight:bold;font-size:16px;border-bottom:1px solid #cccccc;">Shipping Address</h2>
										<p style="margin:0 0 10px 10px;text-align:left;font-size:12px;color:#7d7d7d;">Mykala Ly</p>
										<p style="margin:0 0 10px 10px;text-align:left;font-size:12px;color:#7d7d7d;">12345 Brodard Rd.</p>
										<p style="margin:0 0 10px 10px;text-align:left;font-size:12px;color:#7d7d7d;">Palo Alto, California, 94306</p>
										<p style="margin:0 0 10px 10px;text-align:left;font-size:12px;color:#7d7d7d;">United States</p>
										<p style="margin:0 0 10px 10px;text-align:left;font-size:12px;color:#7d7d7d;">T: 650-555-4321</p>
									</td>
								</tr>
							</table>
							<table width="630" border="0" cellpadding="0" cellspacing="0" style="margin:5px;">
								<tr>
									<td style="width:305px;padding:5px;">
										<table width="263" cellpadding="20" cellspacing="0" style="height:215px;">
											<tr>
												<td style="border:1px solid #cccccc;background:#f7f7f7;margin:5px;">
													<p style="margin:0 0 10px 0;font-weight:bold;text-align:left;font-size:14px;color:#006596;">Save 25% OFF Your Next Order By Subscribing To Our Email Newsletter</p>
													<p style="margin:0;line-height:18px;text-align:left;font-size:12px;color:#7d7d7d;">You'll also receive exclusive access to special promotions, exciting content, and insider information before anyone else has a chance to see them.</p>
													<table width="263" border="0" cellpadding="0" cellspacing="0" style="height:34px;border-spacing:0;margin-top:10px;">
														<tr>
															<td style="width:28px;height:34px;text-align:left;border-spacing:0;">
																<div style="position:relative;float:left;width:28px;"><img src="http://www.cellularoutfitter.com/images/confirm-email/mail-icon.png" width="28" height="34" /></div>
																<div style="position:relative;float:left;"><input type="text" style="position:relative;float;left;margin:0;padding:5px;border-top:1px solid #cccccc;border-bottom:1px solid #cccccc;height:22px;border-left:0;border-right:0;font-family:'Times New Roman', sans-serif;font-weight:bold;width:167px;" value="Enter your email..." /></div>
																<div style="position:relative;float:left;"><input type="submit" value="SIGN UP" style="font-family:Tahoma, Helvetica, sans-serif;margin:0;height:34px;border-left:0;border-top:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;background:#26af61;color:#ffffff;font-weight:normal;padding:5px;border-bottom-right-radius:5px;border-top-right-radius:5px;cursor:pointer;" /></div>
															</td>
															<td></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td style="305px;padding:5px;">
										<table width="263" cellpadding="20" cellspacing="0" style="height:215px;">
											<tr>
												<td style="border:1px solid #cccccc;background:#f7f7f7;margin:5px;font-size:12px;color:#7d7d7d;">
													<table width="263" cellpadding="5" cellspacing="0">
														<tr>
															<td style="width:154px;text-align:right;border-bottom:1px solid #cccccc;">Order Subtotal:</td>
															<td style="text-align:right;padding-right:20px;border-bottom:1px solid #cccccc;">$9.99</td>
														</tr>
														<tr>
															<td style="width:154px;text-align:right;border-bottom:1px solid #cccccc;">Tax:</td>
															<td style="text-align:right;padding-right:20px;border-bottom:1px solid #cccccc;">$0.00</td>
														</tr>
														<tr>
															<td style="width:154px;text-align:right;border-bottom:1px solid #cccccc;">Shipping Fee:</td>
															<td style="text-align:right;padding-right:20px;border-bottom:1px solid #cccccc;">$5.99</td>
														</tr>
														<tr>
															<td style="width:154px;text-align:right;border-bottom:1px solid #cccccc;">Discount:</td>
															<td style="text-align:right;padding-right:20px;border-bottom:1px solid #cccccc;">(-0.00)</td>
														</tr>
													</table>
													<table width="263" cellpadding="5" cellspacing="0" style="margin-top:1px;">
														<tr>
															<td style="padding-top:5px;width:154px;text-align:right;border-top:1px solid #cccccc;color:#000000;font-weight:bold;">Grand Total:</td>
															<td style="padding-top:5px;padding-right:20px;text-align:right;border-top:1px solid #cccccc;color:#000000;font-weight:bold;">$15.98</td>
														</tr>
													</table>
													<table width="263" cellpadding="5" cellspacing="0">
														<tr>
															<td style="padding-top:10px;width:154px;text-align:right;color:#000000;font-weight:bold;">{Additional Charges}</td>
															<td style="padding-top:10px;padding-right:20px;text-align:right;color:#000000;font-weight:bold;">{XX.XX}</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<table width="558" cellpadding="0" cellspacing="0" style="margin:20px 40px;border:2px solid #cccccc;">
								<tr>
									<td style="padding:1px;">
										<table width="554" cellpadding="0" cellspacing="0" style="border:1px solid #cccccc;">
											<tr>
												<td style="text-align:center;padding:20px;">
													WE THOUGHT YOU MIGHT BE INTERESTED IN THESE GREAT PRODUCTS.<br />
													THEY ARE HIGHLY RECOMMENDED BY CUSTOMERS LIKE YOU.
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<table width="640" cellpadding="0" cellspacing="0">
								<tr>
									<td style="border-right:2px solid #cccccc;width:138px;padding:10px;">
										<img src="http://www.cellularoutfitter.com/images/confirm-email/related-product.png" width="132" height="132" />
										<p style="line-height:18px;color:#006596;font-weight:bold;font-size:12px;text-align:left;"><a href="#" style="color:#006596;font-weight:bold;font-size:12px;">Universal 500 mAh Spare Battery Charger w/LCD Display (White w/Black Trim)</a></p>
										<p style="font-size:12px;margin-top:10px;text-align:left;color:#cccccc;text-decoration:line-through;">Retail Price: $14.99</p>
										<p style="margin-top:10px;font-size:16px;font-weight:bold;text-align:left;color:#000000;">Our Price:<br />$3.99</p>
									</td>
									<td style="border-right:2px solid #cccccc;width:138px;padding:10px;">
										<img src="http://www.cellularoutfitter.com/images/confirm-email/related-product.png" width="132" height="132" />
										<p style="line-height:18px;color:#006596;font-weight:bold;font-size:12px;text-align:left;"><a href="#" style="color:#006596;font-weight:bold;font-size:12px;">Universal 500 mAh Spare Battery Charger w/LCD Display (White w/Black Trim)</a></p>
										<p style="font-size:12px;margin-top:10px;text-align:left;color:#cccccc;text-decoration:line-through;">Retail Price: $14.99</p>
										<p style="margin-top:10px;font-size:16px;font-weight:bold;text-align:left;color:#000000;">Our Price:<br />$3.99</p>
									</td>
									<td style="border-right:2px solid #cccccc;width:138px;padding:10px;">
										<img src="http://www.cellularoutfitter.com/images/confirm-email/related-product.png" width="132" height="132" />
										<p style="line-height:18px;color:#006596;font-weight:bold;font-size:12px;text-align:left;"><a href="#" style="color:#006596;font-weight:bold;font-size:12px;">Universal 500 mAh Spare Battery Charger w/LCD Display (White w/Black Trim)</a></p>
										<p style="font-size:12px;margin-top:10px;text-align:left;color:#cccccc;text-decoration:line-through;">Retail Price: $14.99</p>
										<p style="margin-top:10px;font-size:16px;font-weight:bold;text-align:left;color:#000000;">Our Price:<br />$3.99</p>
									</td>
									<td style="width:138px;padding:10px;">
										<img src="http://www.cellularoutfitter.com/images/confirm-email/related-product.png" width="132" height="132" />
										<p style="line-height:18px;color:#006596;font-weight:bold;font-size:12px;text-align:left;"><a href="#" style="color:#006596;font-weight:bold;font-size:12px;">Universal 500 mAh Spare Battery Charger w/LCD Display (White w/Black Trim)</a></p>
										<p style="font-size:12px;margin-top:10px;text-align:left;color:#cccccc;text-decoration:line-through;">Retail Price: $14.99</p>
										<p style="margin-top:10px;font-size:16px;font-weight:bold;text-align:left;color:#000000;">Our Price:<br />$3.99</p>
									</td>
								</tr>
							</table>
							<table width="640" cellpadding="0" cellspacing="0">
								<tr>
									<td style="padding:10px;font-size:11px;text-align:center;background-color:#ebebeb;border-top:2px solid #cccccc;font-family:Verdana, Helvetica, sans-serif;">
										<p style="font-weight:bold;line-height:18px;">Would You Like To Receive Special Coupons & Opportunities To Win Sweepstakes And Prizes?</p>
										<p style="font-weight:bold;line-height:18px;">It's Easy! Simply Join Us On Social Media.</p>
										<p style="text-align:center;">
											<a href="#"><img src="http://www.cellularoutfitter.com/images/confirm-email/facebook.png" width="35" height="35" style="margin:10px;" /></a>
											<a href="#"><img src="http://www.cellularoutfitter.com/images/confirm-email/twitter.png" width="35" height="35" style="margin:10px;" /></a>
											<a href="#"><img src="http://www.cellularoutfitter.com/images/confirm-email/google-plus.png" width="35" height="35" style="margin:10px;" /></a>
											<a href="#"><img src="http://www.cellularoutfitter.com/images/confirm-email/pinterest.png" width="35" height="35" style="margin:10px;" /></a>
										</p>
										<p style="line-height:18px;">If you have any questions, comments, or concerns, please <a href="#" style="color:#006596;font-weight:bold;text-decoration:underline;">contact us</a>.</p>
										<p style="line-height:18px;">Our customer service hours are Monday through Friday 6:00 A.M. - 6:00 P.M. Pacific Standard Time.</p>
										<p style="line-height:18px;">Offer excludes Bluetooth headsets, custom cases, phones and select OEM products.</p>
										<p style="line-height:18px;">This email correspondence has been sent to you as you have indicated you would like to receive special, exclusive offers from CellularOutfitter.com. If this message has been sent in error, or you would no longer like to receive any more periodic offers from CellularOutfitter.com, you may <a href="#" style="color:#006596;font-weight:bold;text-decoration:underline;">unsubscribe</a>.</p>
										<p style="line-height:18px;"><a href="#" style="color:#006596;font-weight:bold;text-decoration:underline;">Privacy Policy</a> | <a href="#" style="color:#006596;font-weight:bold;text-decoration:underline;">Terms & Conditions</a> | <a href="#" style="color:#006596;font-weight:bold;text-decoration:underline;">Contact Us</a></p>
										<p style="line-height:18px;">1410 N. Batavia St. | Orange, CA 92867</p>
										<p style="line-height:18px;">&copy; 2002-2014 CellularOutfitter.com</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>