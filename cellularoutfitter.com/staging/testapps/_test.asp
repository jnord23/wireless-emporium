<%
set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getStagingConn()

bTestMode = true

startDate	=	date - 5
endDate		=	date

sql	=	"exec [getShipConfirmTest] '2/7/2014', '2/7/2014'"
set objRsOrders = oConn.execute(sql)

strTemplate = readTextFile("C:\inetpub\wwwroot\cellularoutfitter.com\www\images\email\template\shippingConfirmEmail.htm")
dup_strTemplate = strTemplate

lastOrderID 	= 	-1
curOrderID		=	-1
strOrderDetails	=	""
numItem			=	0

if objRsOrders.state = 1 then
	'recordset loop(by store)
	do until objRsOrders is nothing
		do until objRsOrders.eof
			curOrderID	=	objRsOrders("orderid")
			site_id		=	objRsOrders("site_id")
			if lastOrderID <> curOrderID then
				numItem = 0
				if strOrderDetails <> "" then
					strTemplate = swapAndSend(strTemplate, logoLink, headerText, custName, custNameAddress, topText, strShipAddress, strBillAddress, lastOrderID, lastAccountID, strOrderDetails, orderSubTotalAmt, telephone, strTelephone, site_id, shoprunnerid, trackLink, promoCode, discountAmt, taxAmt, shippingFeeAmt, shipMethod, shippedBy, strTracking, orderGrandTotalAmt, storeName, cdo_from, cdo_to, cdo_subject)

'					if bTestMode then
'						cdo_to = "terry@wirelessemporium.com"
'						call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "")
'					else
'						call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "shipping@wirelessemporium.com")
'					end if
					response.write cdo_to & "<br>" & strTemplate & "<br><br>"
	
					strTemplate = dup_strTemplate	'refresh the template for an another e-mail.			
				end if

				strOrderDetails	= ""
				item_ids = ""
			end if
			
			'========================================== SET REPLACEMENT TEXTS ================================================
			lastAccountID 		=	objRsOrders("accountid")
			lastOrderID 		= 	curOrderID
			fname				=	objRsOrders("fname")
			lname				=	objRsOrders("lname")
			if isnull(fname) or len(fname) <= 0 then
				custName			=	"<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>Customer</p>"
			else
				custName			=	ucase(left(fname, 1)) & lcase(right(fname,len(fname)-1))
				custNameAddress		=	"<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & ucase(left(fname, 1)) & lcase(right(fname,len(fname)-1)) & " " & ucase(left(lname, 1)) & lcase(right(lname,len(lname)-1)) & "</p>"
			end if
			email				=	objRsOrders("email")
			cdo_to				=	email
			
			storeName			=	objRsOrders("longDesc")
			topText				=	"Thank you for ordering from " & storeName & ".com! We would like to notify you that your order has been SHIPPED to:"			
			trackLink			=	objRsOrders("track_link")	'shipping track
			arr_track 			=	split(trackLink, "=")
			for each x in arr_track
				trackNum		=	x
			next				
			shipMethod			=	objRsOrders("shiptype")
			shippedBy			=	objRsOrders("shipped_by")
			promoCode			=	objRsOrders("promoCode")
			shoprunnerid		=	objRsOrders("shoprunnerid")
						
			orderSubTotalAmt	=	cdbl(objRsOrders("ordersubtotal"))
			orderGrandTotalAmt	=	cdbl(objRsOrders("ordergrandtotal"))
			taxAmt				=	cdbl(objRsOrders("orderTax"))
			discountAmt			=	cdbl(objRsOrders("discount"))
			shippingFeeAmt		=	cdbl(objRsOrders("ordershippingfee"))
			cdo_from			=	storeName & "<sales@" & Lcase(storeName) & ".com>"
			strOrder			=	"Order"
			if objRsOrders("reshipType") = 2 then strOrder = "Backorder"
			
			telephone			= objRsOrders("phone")
			
			strTelephone		= "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>T: " & telephone & "</p>"
			
			strBillAddress = formatAddress(objRsOrders("bAddress1"),objRsOrders("bAddress2"),objRsOrders("bCity"),objRsOrders("bState"),objRsOrders("bZip"))
			strShipAddress = formatAddress(objRsOrders("sAddress1"),objRsOrders("sAddress2"),objRsOrders("sCity"),objRsOrders("sState"),objRsOrders("sZip"))			
			
			select case site_id
				case 0
					headerText	=	strOrder & " #" & curOrderID & " Successfully Shipped from WirelessEmporium.com"
					logoLink	=	"http://www.wirelessemporium.com/images/global/we_logo_solo.gif"
				case 1
					headerText	=	"Your CellphoneAccents.com " & strOrder & " #" & curOrderID & " is on the way!"
					logoLink	=	"http://www.cellphoneaccents.com/images/logo.jpg"
				case 2
					headerText	=	"Shipment Status from CellularOutfitter.com " & strOrder & " #" & curOrderID
					logoLink	=	"http://www.cellularoutfitter.com/images/CellularOutfitter_STANDARD.jpg"
				case 3
					headerText	=	"Phonesale.com " & strOrder & " #" & curOrderID & " - Shipment Status"
					logoLink	=	"http://www.phonesale.com/images/phonesale.jpg"
				case 10
					headerText	=	"Your TabletMall.com " & strOrder & " #" & curOrderID & " - Shipment Status"
					logoLink	=	"http://www.tabletmall.com/images/logo.gif"					
			end select

			cdo_subject	=	headerText			
			'=================================================================================================================
			
			'============================================== ORDER DETAIL TABLE ===============================================
			if instr(email, "@") > 0 and len(custName) > 0 then
				numItem = numItem + 1
				itemid		=	objRsOrders("itemid")
				if item_ids <> "" then
					item_ids = item_ids & ","
				end if
				item_ids = item_ids & itemid
				
				qty			=	cdbl(objRsOrders("quantity"))
				price		=	cdbl(objRsOrders("price"))
				subtotal	=	cdbl(qty * price)
				itemDesc 	=	objRsOrders("itemdesc")
				partnumber	=	objRsOrders("partnumber")
				itempic		= objRsOrders("itempic")
				
				productLink = "http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".html?dzid=email_transactional_ORDERREVIEW_" & numItem & "L_" & itemid & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=Shipping&utm_content=Confirmation&utm_promocode=NA"
				
'				itemImgPath = server.MapPath("/productPics/thumb/" & itempic)
				itemImgPath = "C:\inetpub\wwwroot\productpics_co\thumb\" & itempic
				
				set fsThumb = CreateObject("Scripting.FileSystemObject")
				if not fsThumb.FileExists(itemImgPath) then
					useImg = "/productPics/thumb/imagena.jpg"
					DoNotDisplay = 1
				else
					useImg = "/productPics/thumb/" & itempic
				end if
														
				strOrderDetails =   strOrderDetails &	"			        		<tr>" & vbcrlf & _
														"								<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;text-align:left;vertical-align:top;padding:10px 5px;border-left:1px solid #ccc;border-bottom:1px solid #ccc;border-right:1px solid #ccc;'>" & partnumber & "</td>" & vbcrlf & _
														"								<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;text-align:center;'><a href=""" & productLink & """><img src='http://www.cellularoutfitter.com" & useImg & "' height='65' /></a></td>" & vbcrlf & _
														"								<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;text-align:center;'>" & formatnumber(qty,0) & "</td>" & vbcrlf & _
														"								<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;'><a href=""" & productLink & """ style=""color:#333333; font-family:Tahoma, Helvetica, sans-serif;font-size:12px;"">" & itemDesc & "</a></td>" & vbcrlf & _
														"								<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;text-align:center;'>" & formatcurrency(price) & "</td>" & vbcrlf & _
														"								<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;text-align:center;'>" & formatcurrency(subtotal) & "</td>" & vbcrlf & _
														"							</tr>" & vbcrlf
			end if
			'=================================================================================================================		
			objRsOrders.movenext
		loop
		set objRsOrders = objRsOrders.nextrecordset
	loop
	
	if numItem > 0 then
		if strOrderDetails <> "" then
			strTemplate = swapAndSend(strTemplate, logoLink, headerText, custName, custNameAddress, topText, strShipAddress, strBillAddress, lastOrderID, lastAccountID, strOrderDetails, orderSubTotalAmt, telephone, strTelephone, site_id, shoprunnerid, trackLink, promoCode, discountAmt, taxAmt, shippingFeeAmt, shipMethod, shippedBy, strTracking, orderGrandTotalAmt, storeName, cdo_from, cdo_to, cdo_subject)

'			if bTestMode then
'				cdo_to = "terry@wirelessemporium.com"
'				call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "")
'			else
'				call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "terry@wirelessemporium.com,ruben@wirelessemporium.com,wemarketing@wirelessemporium.com,shipping@wirelessemporium.com")
'			end if
			response.write cdo_to & "<br>" & strTemplate & "<br><br>"
		
			strTemplate	= dup_strTemplate	'refresh the template for an another e-mail.
		end if
	end if
end if

function swapAndSend(strTemplate, logoLink, headerText, custName, custNameAddress, topText, strShipAddress, strBillAddress, lastOrderID, lastAccountID, strOrderDetails, orderSubTotalAmt, telephone, strTelephone, site_id, shoprunnerid, trackLink, promoCode, discountAmt, taxAmt, shippingFeeAmt, shipMethod, shippedBy, strTracking, orderGrandTotalAmt, storeName, cdo_from, cdo_to, cdo_subject)
	strTemplate = replace(strTemplate, "[EMAIL]", cdo_to)
	strTemplate = replace(strTemplate, "[EMAIL_SIGNUP]", "http://www.cellularoutfitter.com?dzid=email_transactional_EMAILSIGNUP_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Shipping&utm_content=Confirmation&utm_promocode=NA&utm_newsletter="&cdo_to)
	strTemplate = replace(strTemplate, "[LOGO IMG LINK]", logoLink)
	strTemplate = replace(strTemplate, "[HEADER TEXT]", headerText)
	strTemplate = replace(strTemplate, "[CUSTOMER NAME]", custName)
	strTemplate = replace(strTemplate, "[CUSTOMER NAME ADDRESS]", custNameAddress)
	strTemplate = replace(strTemplate, "[TOP TEXT]", topText)
	strTemplate = replace(strTemplate, "[SHIPPED TO]", strShipAddress)
	strTemplate = replace(strTemplate, "[BILLED TO]", strBillAddress)
	strTemplate = replace(strTemplate, "[ORDER ID]", lastOrderID)					
	strTemplate = replace(strTemplate, "[ORDER DETAILS]", strOrderDetails)
	strTemplate = replace(strTemplate, "[ORDER SUBTOTAL]", formatcurrency(orderSubTotalAmt))
	strTemplate = replace(strTemplate, "[EMAIL_PAGE]", "http://www.cellularoutfitter.com/images/email/confirm/shipping-confirm.html?a=" & lastAccountID & "&o=" & lastOrderID & "&dzid=email_transactional_HEADER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Shipping&utm_content=Confirmation&utm_promocode=NA")
	
	if telephone <> "" then
		strTemplate = replace(strTemplate, "[TELEPHONE]", strTelephone)
	else 
		strTemplate = replace(strTemplate, "[TELEPHONE]", "")
	end if
	
	if site_id = 0 then
		if shoprunnerid <> "" then
			strTemplate = replace(strTemplate, "[SR]", "Thanks for using FREE 2-Day Shipping by ShopRunner! For returns, visit https://www.shoprunner.com/returns to print your pre-paid return label. Don't forget, invite a Friend to join.")
		else
			strTemplate = replace(strTemplate, "[SR]", "Next time get FREE 2-Day Shipping from Wireless Emporium and other great retailers with your ShopRunner membership. Visit https://www.wirelessemporium.com/shoprunner to sign up for a FREE 30-Day Trial")
	'						strTemplate = replace(strTemplate, "[SR]", "")
		end if
	else
		strTemplate = replace(strTemplate, "[SR]", "")
	end if
	
	arr_track 			=	split(trackLink, "=")
	for each x in arr_track
		trackNum		=	x
	next				
	
	if promoCode <> "" then
		strTemp = 	"            	<tr>" & vbcrlf & _
					"					<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>PROMO CODE:</b></td>" & vbcrlf & _
					"					<td width=""80%"" align=""left"" valign=""top"">" & promoCode & "</td>" & vbcrlf & _
					"				</tr>" & vbcrlf
					
		strTemplate = replace(strTemplate, "[PROMO CODE]", strTemp)
	else
		strTemplate = replace(strTemplate, "[PROMO CODE]", "")
	end if
	
	if discountAmt > 0 then			
		strTemp =	"(-" & formatcurrency(discountAmt) & ")"
		strTemplate = replace(strTemplate, "[DISCOUNT]", strTemp)
	else
		strTemplate = replace(strTemplate, "[DISCOUNT]", "")
	end if
	
	strTemplate = replace(strTemplate, "[BUY SAFE]", "")
	
	strTemplate = replace(strTemplate, "[TAX AMOUNT]", formatcurrency(taxAmt))
	strTemplate = replace(strTemplate, "[SHIPPING FEE]", formatcurrency(shippingFeeAmt))
	if shoprunnerid <> "" then
		strTemplate = replace(strTemplate, "[SHIPPING TYPE]", "ShopRunner, 2-Day Shipping - FREE")
		strTemplate = replace(strTemplate, "[TR TRACKING]", "")
	else
		if shipMethod = "First Class" then
			strTemplate = replace(strTemplate, "[SHIPPING TYPE]", shipMethod & " (3-10 Business Days)")
		else 
			strTemplate = replace(strTemplate, "[SHIPPING TYPE]", shipMethod)
		end if
		if shippedBy = "UPS-MI"	then
			strMemo = "(Tracking may take 24-48 hours to appear after your mail piece has shipped.)"
		else
			strMemo = ""
		end if
		
		strTracking = "<a href='" & trackLink & "' target='_blank' style='color:#0099ff;font-weight:bold;text-decoration:underline;'>#" & trackNum & "</a>"
		
		strTemplate = replace(strTemplate, "[TR TRACKING]", strTracking)
	end if
	strTemplate = replace(strTemplate, "[GRAND TOTAL]", formatcurrency(orderGrandTotalAmt))
	strTemplate = replace(strTemplate, "[STORE NAME]", storeName)
	
	strUpSell = ""
	if item_ids then strUpSell = getCrossSellProducts(item_ids)
	strTemplate = replace(strTemplate, "[UPSELL]", strUpSell)
	
	swapAndSend = strTemplate
end function

function formatAddress(sAdd1,sAdd2,sCity,sState,sZip)
	dim strTemp
	strTemp = "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & sAdd1 & "</p>" & vbcrlf
	if sAdd2 <> "" then 
		strTemp = strTemp & "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & sAdd2 & "</p>" & vbcrlf
	end if
	strTemp = strTemp & "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & sCity & ", " & sState & "&nbsp;" & sZip & "</p>" & vbcrlf
	formatAddress = strTemp
end function

function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
end function


sub cdosend(strFrom, strTo, strSubject, strBody, strBcc)
	if instr(trim(strTo), "@") > 0 and instr(trim(strTo), " ") = 0 and isValidEmail(strTo) then
		set objEmail = CreateObject("CDO.Message")
		with objEmail
			.From = strFrom
			.To = strTo
			.Bcc = strBcc
			.Subject = strSubject
			.HTMLBody = strBody
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Update
			.Send
		end with
	end if
end sub

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")		
	else
		formatSEO = ""
	end if
	formatSEO = replace(formatSEO, "---", "-")
	formatSEO = replace(formatSEO, "--", "-")	
end function

function getCrossSellProducts(item_ids)
	sqlUpSell = "exec sp_ppUpsell 2, '" & item_ids & "', 0, 30"
	set objRsUpSell = oConn.execute(sqlUpSell)
	if not objRsUpSell.EOF then
		strUpSell 	=	"<table width='558' cellpadding='0' cellspacing='0' style='margin:20px 40px;border:2px solid #cccccc;'>" & vbcrlf & _
						"	<tr>" & vbcrlf & _
						"		<td style='padding:1px;'>" & vbcrlf & _
						"			<table width='554' cellpadding='0' cellspacing='0' style='border:1px solid #cccccc;'>" & vbcrlf & _
						"				<tr>" & vbcrlf & _
						"					<td style='font-family:Verdana, Helvetica, sans-serif;font-size:13px;line-height:20px;text-align:center;padding:15px;'>" & vbcrlf & _
						"						WE THOUGHT YOU MIGHT BE INTERESTED IN THESE GREAT PRODUCTS.<br />" & vbcrlf & _
						"						THEY ARE HIGHLY RECOMMENDED BY CUSTOMERS LIKE YOU." & vbcrlf & _
						"					</td>" & vbcrlf & _
						"				</tr>" & vbcrlf & _
						"			</table>" & vbcrlf & _
						"		</td>" & vbcrlf & _
						"	</tr>" & vbcrlf & _
						"</table>" & vbcrlf & _
						"<table width='640' cellpadding='0' cellspacing='0' style='padding:10px 0;'>" & vbcrlf & _
						"	<tr>"
		counter = 1
		do until objRsUpSell.EOF
			itemID		= objRsUpSell("itemID")
			itemPic		= objRsUpSell("itemPic")
			itemDesc	= objRsUpSell("itemDesc")
			useItemDesc = replace(itemDesc,"  "," ")
'						itemImgPath = server.MapPath("/productPics/thumb/" & itemPic)
			itemImgPath = "C:\inetpub\wwwroot\productpics_co\thumb\" & itempic
			price		= objRsUpSell("price")
			price_retail= objRsUpSell("price_retail")
			productLink = "http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".html?dzid=email_transactional_RECOMMENDATIONS_" & counter & "L_" & itemID & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=Shipping&utm_content=Confirmation&utm_promocode=NA"
			
			set fsThumb = CreateObject("Scripting.FileSystemObject")
			if not fsThumb.FileExists(itemImgPath) then
				useImg = "/productPics/big/imagena.jpg"
				DoNotDisplay = 1
			else
				useImg = "/productPics/big/" & itemPic
			end if
			
			if counter < 4 then 
				strUpSell 	= strUpSell &	"	<td style='border-right:2px solid #cccccc;width:138px;padding:10px;' align=""center"">" & vbcrlf & _
											"		<a href='" & productLink & "'><img src='http://www.cellularoutfitter.com" & useImg & "' width='132' and height='132' /></a>" & vbcrlf & _
											"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;line-height:18px;color:#006596;font-weight:bold;font-size:12px;text-align:left;'><a href='" & productLink & "' style='color:#006596;font-weight:bold;font-size:12px;'>" & itemDesc & "</a></p>" & vbcrlf & _
											"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;font-size:12px;text-align:left;color:#cccccc;text-decoration:line-through;'>Retail Price: " & formatCurrency(price_retail) & "</p>" & vbcrlf & _
											"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;margin-top:10px;font-size:16px;font-weight:bold;text-align:left;color:#000000;'>Our Price:<br />" & formatCurrency(price) & "</p>" & vbcrlf & _
											"	</td>"
			else 
				strUpSell 	= strUpSell &	"	<td style='width:138px;padding:10px;' align=""center"">" & vbcrlf & _
										"		<a href='" & productLink & "'><img src='http://www.cellularoutfitter.com" & useImg & "' width='132' and height='132' /></a>" & vbcrlf & _
										"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;line-height:18px;color:#006596;font-weight:bold;font-size:12px;text-align:left;'><a href='" & productLink & "' style='color:#006596;font-weight:bold;font-size:12px;'>" & itemDesc & "</a></p>" & vbcrlf & _
										"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;font-size:12px;text-align:left;color:#cccccc;text-decoration:line-through;'>Retail Price: " & formatCurrency(price_retail) & "</p>" & vbcrlf & _
										"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;margin-top:10px;font-size:16px;font-weight:bold;text-align:left;color:#000000;'>Our Price:<br />" & formatCurrency(price) & "</p>" & vbcrlf & _
										"	</td>"
			end if
			
			if counter = 4 then
				exit do
			else
				counter = counter + 1
			end if
			objRsUpSell.movenext
		loop
	end if
	strUpSell		= strUpSell & "</tr>" & vbcrlf & _
					"</table>"
	getCrossSellProducts = strUpSell
end function

Function isValidEmail(emailAddress)
	'Declare variables 
	Dim ValidEmail, emailParts, iLoopCounter, emailChar, acceptableChars, last2Chars 
	ValidEmail = True 'set the default result to True
	'acceptableChars are the characters that we will allow in our email
	acceptableChars="abcdefghijklmnopqrstuvwxyz.-_@"
	'use the Split function to create an array with the @ as the separator
	'so if your email was test@tester.com the email would be split into an array
	'with the first array element holding "test" and the second "tester.com"
	emailParts = Split(emailAddress, "@")
	'check to make sure that there is only 1 @ and that there are 2 parts
	'remember arrays are zero based
	'Using the UBound function will return the highest element in the array
	'So if it's a valid email the UBound function will return 1, i.e. 0 start
	If UBound(emailParts) <> 1 Then
	ValidEmail = false
	Else 
	'Check the length of each part of the email address
	'first part can be just one character, 2nd part must be atleast 4
	If Len(emailParts(0))<1 OR Len(emailParts(1))<4 Then
	ValidEmail = false
	End If
	'check first character on the left part isn't a "." using Left function
	If Left(emailParts(0), 1)="." Then
	ValidEmail = false
	End If 
	'check that there is a . in the second part of the email address - .com
	If InStr(emailParts(1), ".") <= 0 Then
	ValidEmail = false 
	End If 
	'We know there's a . now make sure there are 2 characters after the . for valid email
	last2Chars=Right(emailParts(1),2)
	If InStr(last2chars,".") Then
	ValidEmail = false
	End If
	'check that there shouldn't be a _ in the second part of the email address 
	If InStr(emailParts(1), "_") >0 Then
	ValidEmail = false 
	End If 
	End If
	'loop through each character of email
	For iLoopCounter = 1 to Len(emailAddress)
	'Use Lcase & Mid functions, Mid function used to return each individual character
	'in the email, and then Lcase converts it into lowercase
	emailChar = Lcase(Mid(emailAddress, iLoopCounter, 1)) 
	'Check if the emailAddress characters are acceptable
	If InStr(acceptableChars, emailChar) = 0 and Not IsNumeric(emailChar) Then
	ValidEmail = false
	End if
	Next
	'check if there is 2 . in a row
	If InStr(emailAddress, "..") > 0 Then
	ValidEmail=false
	End If
	'check if there is @. in a row
	If InStr(emailAddress, "@.") > 0 Then
	ValidEmail=false
	End If 
	isValidEmail=ValidEmail
End function
%>