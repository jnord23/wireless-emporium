<%
set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getStagingConn()

bTestMode = true

sql	=	"exec [getShipConfirm] '2/7/2014', '2/7/2014', '2'"
set objRsOrders = oConn.execute(sql)

lastEmail = ""
curEmail = ""
lastOrderID 	= 	-1
curOrderID		=	-1
numItem = 0

if objRsOrders.state = 1 then
	'recordset loop(by store)
	do until objRsOrders is nothing
		do until objRsOrders.eof
			site_id			=	objRsOrders("site_id")
			curEmail = objRsOrders("email")
			curOrderID		=	objRsOrders("orderid")
			
			if lastEmail <> curEmail and lastEmail <> "" then
				
				strTemplate = swapAndSend(site_id, item_ids, lastAccountID, lastOrderID)
				response.write lastEmail & "<br />"
				response.write strTemplate & "<br><br>"
				strTemplate 	= 	dup_strTemplate	'refresh the template for an another e-mail.
				
				item_ids = ""
			end if
			
			'========================================== SET REPLACEMENT TEXTS ================================================
			lastAccountID 		=	objRsOrders("accountid")
			lastOrderID 		= 	curOrderID
			lastEmail			=	curEmail
			fname				=	objRsOrders("fname")
			lname				=	objRsOrders("lname")
			cdo_to				=	curEmail
			cdo_from			=	storeName & "<sales@" & Lcase(storeName) & ".com>"
			
			if isnull(fname) or len(fname) <= 0 then
				custName			=	"<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>Customer</p>"
			else
				custName			=	ucase(left(fname, 1)) & lcase(right(fname,len(fname)-1))
				custNameAddress		=	ucase(left(fname, 1)) & lcase(right(fname,len(fname)-1)) & " " & ucase(left(lname, 1)) & lcase(right(lname,len(lname)-1))
			end if
			
			
			'============================================== ORDER DETAIL TABLE ===============================================
			if instr(curEmail, "@") > 0 and len(custName) > 0 then
				itemid		=	objRsOrders("itemid")
				if item_ids <> "" then
					item_ids = item_ids & ","
				end if
				item_ids = item_ids & itemid				
				itempic		= objRsOrders("itempic")
				
				numItem = numItem + 1
			end if
			objRsOrders.movenext
		loop
		set objRsOrders = objRsOrders.nextrecordset
	loop
	
	if numItem > 0 then
		'strOrderDetails	=	strOrderDetails	&	"			        	</table>"
		
		strTemplate = swapAndSend(site_id, item_ids, lastAccountID, lastOrderID)
		
		'send an e-mail
'			call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "")
		response.write lastEmail & "<br />"
		response.write strTemplate & "<br><br>"
	
		strTemplate 	= 	dup_strTemplate	'refresh the template for an another e-mail.
	end if
end if



function swapAndSend(site_id, item_ids, lastAccountID, lastOrderID)
	if item_ids then strUpSell = getCrossSellProducts(site_id, item_ids)
	strTemplate = readTextFile("C:\inetpub\wwwroot\cellularoutfitter.com\www\images\email\template\co-subscribe-batch.htm")
	strTemplate = replace(strTemplate, "[ACCOUNT ID]", lastAccountID)
	strTemplate = replace(strTemplate, "[ORDER ID]", lastOrderID)
	strTemplate = replace(strTemplate, "[UPSELL]", strUpSell)
	
	swapAndSend = strTemplate
end function

function getCrossSellProducts(site_id, item_ids)
	sqlUpSell = "exec sp_ppUpsell " & site_id & ", '" & item_ids & "', 0, 30"
	session("errorSQL") = sqlUpSell
	set recommendRS = oConn.execute(sqlUpSell) 

	strUpSell = ""

	if not recommendRS.EOF then
		strUpSell = strUpSell & "		<table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf
		strUpSell = strUpSell & "			<tr>" & vbcrlf
		strUpSell = strUpSell & "				<td style=""background-color: #ffffff;"" width=""20"">&nbsp;</td>" & vbcrlf
		counter = 1
		do until recommendRS.EOF
			itemID		= recommendRS("itemID")
			itemPic		= recommendRS("itemPic")
			itemDesc	= recommendRS("itemDesc")
			useItemDesc = replace(itemDesc,"  "," ")
			itemImgPath = "C:\inetpub\wwwroot\productpics_co\thumb\" & itemPic
			price		= cdbl(recommendRS("price"))
			price_retail= cdbl(recommendRS("price_retail"))
			price_diff	= cdbl(price_retail - price)
			diff_percent	= cdbl(price_diff * 100 / price_retail) 

			productLink = "http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".html?dzid=email_transactional_RECOMMENDATIONS_" & counter & "L_" & itemID & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=Welcome2&utm_content=Confirmation&utm_promocode=WELCOME2"
			
			set fsThumb = CreateObject("Scripting.FileSystemObject")
			if not fsThumb.FileExists(itemImgPath) then
				useImg = "/productPics/big/imagena.jpg"
				DoNotDisplay = 1
			else
				useImg = "/productPics/big/" & itemPic
			end if
			
			strUpSell = strUpSell & "				<td style=""background-color: #ffffff; color: #006699; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 150%; margin: 0;"" width=""200"" align=""center"" valign=""top"" >" & vbcrlf
			strUpSell = strUpSell & "					<a href=""" & productLink & """ target=""_blank""><img style=""display: block;"" src=""" & useImg & """ alt="""" width=""180"" border=""0"" /></a>" & vbcrlf
			strUpSell = strUpSell & "					<p style=""color:#006697; font:normal 11px Verdana, Geneva, sans-serif; line-height:1.5; margin:0; width:180px; height:80px;"">" & vbcrlf
			strUpSell = strUpSell & "						<a style=""color:#006697;"" href=""" & productLink & """ target=""_blank"">" & itemDesc & "</a>" & vbcrlf
			strUpSell = strUpSell & "					</p>" & vbcrlf
			strUpSell = strUpSell & "					<br />" & vbcrlf
			strUpSell = strUpSell & "					<p style=""color:#006697; font:normal 12px Verdana, Geneva, sans-serif; line-height:1.1; margin:0; padding-bottom:10px; "">" & vbcrlf
			strUpSell = strUpSell & "						<span style=""font-size:11px; color:#cccccc;"">Retail Price: <span style=""font:normal Verdana, Geneva, sans-serif; text-decoration:line-through "">" & formatCurrency(price_retail) & "</span></span>" & vbcrlf
			strUpSell = strUpSell & "						<br />" & vbcrlf
			strUpSell = strUpSell & "						<strong style=""color:#000; font-size:14px"">Our Price: " & formatCurrency(price) & "</strong>" & vbcrlf
			strUpSell = strUpSell & "						<br /><br />" & vbcrlf
			strUpSell = strUpSell & "						<a href=""" & productLink & """ style=""color:#2dcc70; text-decoration:none; font-size:12px; ""><strong>YOU SAVE " & formatCurrency(price_diff) & " (" & round(diff_percent) & "%)</strong></a>" & vbcrlf
			strUpSell = strUpSell & "					</p>" & vbcrlf
			strUpSell = strUpSell & "					<a href=""" & productLink & """ target=""_blank"">" & vbcrlf
			strUpSell = strUpSell & "						<table style=""padding-bottom:60px;"" width=""150"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf
			strUpSell = strUpSell & "							<tr>" & vbcrlf
			strUpSell = strUpSell & "								<td style=""text-align:center;font-weight:normal;color:#ffffff;font-size:14px;line-height:18px;background:#33cc66;border:1px solid #009966;border-radius:20px;padding:5px 10px;"">BUY NOW &raquo;</td>" & vbcrlf
			strUpSell = strUpSell & "							</tr>" & vbcrlf
			strUpSell = strUpSell & "						</table>" & vbcrlf
			strUpSell = strUpSell & "					</a>" & vbcrlf
			strUpSell = strUpSell & "				</td>" & vbcrlf
			
			if counter = 3 then
				strUpSell = strUpSell & "			<td style=""background-color: #ffffff;"" width=""20"">&nbsp;</td>" & vbcrlf
				strUpSell = strUpSell & "		</tr>" & vbcrlf
				strUpSell = strUpSell & "		<tr>" & vbcrlf
				strUpSell = strUpSell & "			<td style=""background-color: #ffffff;"" width=""20"">&nbsp;</td>" & vbcrlf
			elseif counter = 6 then
				strUpSell = strUpSell & "			<td style=""background-color: #ffffff;"" width=""20"">&nbsp;</td>" & vbcrlf
				strUpSell = strUpSell & "		</tr>" & vbcrlf
				strUpSell = strUpSell & "	</table>" & vbcrlf
				
				exit do
			end if
			
			recommendRS.movenext
			counter = counter + 1
		loop
	end if
	getCrossSellProducts = strUpSell
end function

function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")		
	else
		formatSEO = ""
	end if
	formatSEO = replace(formatSEO, "---", "-")
	formatSEO = replace(formatSEO, "--", "-")	
end function

%>