strTemplate = replace(strTemplate, "[LOGO IMG LINK]", logoLink)
strTemplate = replace(strTemplate, "[HEADER TEXT]", headerText)
strTemplate = replace(strTemplate, "[CUSTOMER NAME]", custName)
strTemplate = replace(strTemplate, "[CUSTOMER NAME ADDRESS]", custNameAddress)
strTemplate = replace(strTemplate, "[TOP TEXT]", topText)
strTemplate = replace(strTemplate, "[SHIPPED TO]", strShipAddress)
strTemplate = replace(strTemplate, "[BILLED TO]", strBillAddress)
strTemplate = replace(strTemplate, "[ORDER ID]", lastOrderID)					
strTemplate = replace(strTemplate, "[ORDER DETAILS]", strOrderDetails)
strTemplate = replace(strTemplate, "[ORDER SUBTOTAL]", formatcurrency(orderSubTotalAmt))
strTemplate = replace(strTemplate, "[EMAIL_PAGE]", "http://www.cellularoutfitter.com/images/email/confirm/shipping-confirm.html?a=" & lastAccountID & "&o=" & lastOrderID & "&dzid=email_transactional_HEADER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Shipping&utm_content=Confirmation&utm_promocode=NA")

if telephone <> "" then
    strTemplate = replace(strTemplate, "[TELEPHONE]", strTelephone)
else 
    strTemplate = replace(strTemplate, "[TELEPHONE]", "")
end if

if site_id = 0 then
    if shoprunnerid <> "" then
        strTemplate = replace(strTemplate, "[SR]", "Thanks for using FREE 2-Day Shipping by ShopRunner! For returns, visit https://www.shoprunner.com/returns to print your pre-paid return label. Don't forget, invite a Friend to join.")
    else
        strTemplate = replace(strTemplate, "[SR]", "Next time get FREE 2-Day Shipping from Wireless Emporium and other great retailers with your ShopRunner membership. Visit https://www.wirelessemporium.com/shoprunner to sign up for a FREE 30-Day Trial")
'						strTemplate = replace(strTemplate, "[SR]", "")
    end if
else
    strTemplate = replace(strTemplate, "[SR]", "")
end if

if promoCode <> "" then
    strTemp = 	"            	<tr>" & vbcrlf & _
                "					<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>PROMO CODE:</b></td>" & vbcrlf & _
                "					<td width=""80%"" align=""left"" valign=""top"">" & promoCode & "</td>" & vbcrlf & _
                "				</tr>" & vbcrlf
                
    strTemplate = replace(strTemplate, "[PROMO CODE]", strTemp)
else
    strTemplate = replace(strTemplate, "[PROMO CODE]", "")					
end if

if discountAmt > 0 then				
    strTemp =	"(-" & formatcurrency(discountAmt) & ")"
    strTemplate = replace(strTemplate, "[DISCOUNT]", strTemp)
else
    strTemplate = replace(strTemplate, "[DISCOUNT]", "")
end if

strTemplate = replace(strTemplate, "[BUY SAFE]", "")

strTemplate = replace(strTemplate, "[TAX AMOUNT]", formatcurrency(taxAmt))
strTemplate = replace(strTemplate, "[SHIPPING FEE]", formatcurrency(shippingFeeAmt))
if shoprunnerid <> "" then
    strTemplate = replace(strTemplate, "[SHIPPING TYPE]", "ShopRunner, 2-Day Shipping - FREE")
    strTemplate = replace(strTemplate, "[TR TRACKING]", "")
else
    if shipMethod = "First Class" then
        strTemplate = replace(strTemplate, "[SHIPPING TYPE]", shipMethod & " (3-10 Business Days)")
    else 
        strTemplate = replace(strTemplate, "[SHIPPING TYPE]", shipMethod)
    end if
    if shippedBy = "UPS-MI"	then
        strMemo = "(Tracking may take 24-48 hours to appear after your mail piece has shipped.)"
    else
        strMemo = ""
    end if
    'strTracking = 	"            	<tr>" & vbcrlf & _
    '				"               	<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>SHIPMENT TRACKING:</b></td>" & vbcrlf & _
    '				"					<td width=""80%"" align=""left"" valign=""top""><a href=""" & trackLink & """ target=""_blank""><b>Click Here</b></a>&nbsp;" & strMemo & "</td>" & vbcrlf & _
    '				"				</tr>"
    
    strTracking = "<a href='" & trackLink & "' target='_blank' style='color:#0099ff;font-weight:bold;text-decoration:underline;'>#" & trackNum & "</a>"
    
    strTemplate = replace(strTemplate, "[TR TRACKING]", strTracking)
end if
strTemplate = replace(strTemplate, "[GRAND TOTAL]", formatcurrency(orderGrandTotalAmt))
strTemplate = replace(strTemplate, "[STORE NAME]", storeName)

strUpSell = ""
if item_ids then strUpSell = getCrossSellProducts(item_ids)
strTemplate = replace(strTemplate, "[UPSELL]", strUpSell)

'send an e-mail
if bTestMode = true then
    cdo_to = "terry@wirelessemporium.com"
end if
'					call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "")
response.write cdo_to & "<br>" & strTemplate & "<br><br>"

strTemplate 	= 	dup_strTemplate	'refresh the template for an another e-mail.	