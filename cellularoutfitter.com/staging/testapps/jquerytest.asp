
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Samsung Galaxy S5 Trident Kraken A.M.S. Case w/Built-in Screen Protector (Blue/Black)</title>
	<meta name="Description" content="CellularOutfitter.com is the #1 store online offering discounted cell phone accessories like Samsung Galaxy S5 Trident Kraken A.M.S. Case w/Built-in Screen Protector (Blue/Black) & other premium accessories.">
	<meta name="Keywords" content="">
    <meta name="alternate" content="http://m.cellularoutfitter.com/p-320857-samsung-galaxy-s3-anti-glare-screen-protector-film.html">
	<link rel="canonical" href="http://staging.cellularoutfitter.com/p-320857-samsung-galaxy-s5-trident-kraken-a-m-s-case-w-built-in-screen-protector-blue-black-.html" />
	<meta name="verify-v1" content="KDSemcIPOLKERRNLdtodRPug14ThOrk3fAAECAAzt/U=" />
	<meta name="verify-v1" content="7oJ6qXmQ/MOO9Wics9IknbH0gaHI9z5Tl3dH82NBhO8=" />
    <meta name="msvalidate.01" content="DFF5FF52EAB66FFFC627628486428C9B" />
	<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/includes/css/styleCO.css?v=20140430205550" />
    
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<!-- Load Google JS-API asynchronously -->
    <script type="text/javascript">
      (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = '//apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
      })();
    </script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38672929-1']);
  _gaq.push(['_setDomainName', 'cellularoutfitter.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!-- Start Visual Website Optimizer Code -->
<script type='text/javascript'>
var _vis_opt_account_id = 43368;
var _vis_opt_protocol = (('https:' == document.location.protocol) ? 'https://' : 'http://');
document.write('<s' + 'cript src="' + _vis_opt_protocol + 
'dev.visualwebsiteoptimizer.com/deploy/js_visitor_settings.php?v=1&a='+_vis_opt_account_id+'&url='
+encodeURIComponent(document.URL)+'&random='+Math.random()+'" type="text/javascript">' + '<\/s' + 'cript>');
</script>

<script type='text/javascript'>
if(typeof(_vis_opt_settings_loaded) == "boolean") { document.write('<s' + 'cript src="' + _vis_opt_protocol + 
'd5phz18u4wuww.cloudfront.net/vis_opt.js" type="text/javascript">' + '<\/s' + 'cript>'); }
// if your site already has jQuery 1.4.2, replace vis_opt.js with vis_opt_no_jquery.js above
</script>

<script type='text/javascript'>
if(typeof(_vis_opt_settings_loaded) == "boolean" && typeof(_vis_opt_top_initialize) == "function") {
        _vis_opt_top_initialize(); vwo_$(document).ready(function() { _vis_opt_bottom_initialize(); });
}
</script>
<!-- End Visual Website Optimizer Code -->

<link rel="stylesheet" type="text/css" href="/includes/css/tt.css" />

<script type="text/javascript">
    var turnToConfig = {
      siteKey: "yNDzxNjrFYIMmkHsite",
      setupType: "overlay"
    };

    (function() {
        var tt = document.createElement('script'); tt.type = 'text/javascript'; tt.async = true;
        tt.src = document.location.protocol + "//static.www.turnto.com/traServer3/trajs/" + turnToConfig.siteKey + "/tra.js";
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tt, s);
    })();
</script>



<script src="//code.jquery.com/jquery-latest.min.js"></script>

<script type="text/javascript">
var GRANIFY_SITE_ID = 747;
 
(function(e,t,n){e=e+"?id="+t;window.Granify=n;n._stack=[];n.s_v=2;n.init=function(e,t,r){function i(e,t){e[t]=function(){Granify._stack.push([t].concat(Array.prototype.slice.call(arguments,0)))}}var s=n;h=["on","addTag","trackPageView","trackCart","trackOrder"];for(u=0;u<h.length;u++)i(s,h[u])};n.init();var r,i,s,o=document.createElement("iframe");o.src="javascript:false";o.title="";o.role="presentation";(o.frameElement||o).style.cssText="width: 0; height: 0; border: 0";s=document.getElementsByTagName("script");s=s[s.length-1];s.parentNode.insertBefore(o,s);try{i=o.contentWindow.document}catch(u){r=document.domain;o.src="javascript:var d=document.open();d.domain='"+r+"';void(0);";i=o.contentWindow.document}i.open()._l=function(){var t=this.createElement("script");if(r)this.domain=r;t.id="js-iframe-async";t.src=e;this.body.appendChild(t)};i.write('<body onload="document._l();">');i.close()})("//cdn.granify.com/assets/javascript.js",GRANIFY_SITE_ID,[])
</script>



<!-- GoogleAnalytics Start -->
<script type="text/javascript">
	var _gaq = _gaq || [];
	var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
	_gaq.push(['_require', 'inpage_linkid', pluginUrl]);
  	_gaq.push(['_setAccount', 'UA-1097464-1']);

  	
	//mvtGAQ
	
	
	_gaq.push(['_trackPageview']);
	_gaq.push(['_trackPageLoadTime']); 
	
	
	(function(){
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
//		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>


<script type="text/javascript">
if(typeof window.WEDATA == 'undefined'){
	window.WEDATA = {
		storeName: 'CellularOutfitter.com',
		internalUser: 'true'
	};
}
</script>

<script src="//assets.adobedtm.com/d0f694a37a1061afe4e18245479cdeaed180db2f/satelliteLib-6c61417c205e2f84dfcdbf654877e85bba16c2d8-staging.js"></script>

    <META NAME="ROBOTS" CONTENT="INDEX, FOLLOW">
    
</head>
<body class="body">
	<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0013/7223.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>

<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0013/7223.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>

<script>
if(!OPNET_ARXS){
   var OPNET_ARXS={startJS:Number(new Date()),
   clientId:'1BFC6109EB7229A4',appId:322788,
   collector:'eue.collect-opnet.com',
   sv:'0301'};
   (function(){
      var w=window,l=w.addEventListener,m=w.attachEvent,
      d=document,s='script',t='load',o=OPNET_ARXS,
      z='-0b2c4d73f58414c86c7384150be8ca44',
      p=('onpagehide' in w),e=p?'pageshow':t,
      j=d.createElement(s),x=d.getElementsByTagName(s)[0],
      h=function(y){o.ldJS=new Date();o.per=y?y.persisted:null;},
      i=function(){o.ld=1;};o.cookie=d.cookie;d.cookie=
      '_op_aixPageId=0; path=/; expires='+(new Date(0)).toGMTString();
      o.cookieAfterDelete=d.cookie;j.async=1;
      j.src=(('https:'==d.location.protocol)?
      'https://953c27ce3b34cfb8cc56'+z+'.ssl':
      'http://fb3f316d487bcc59f7ec'+z+'.r88')+
      '.cf1.rackcdn.com/opnet_browsermetrix.c.js';
      x.parentNode.insertBefore(j,x);
      if(l){l(e,h,false);if(p){l(t,i,false);}}else if(m)
      {m('on'+e,h);if(p){m('on'+t,i);}}
   })();}
</script>

<!-- Google Tag Manager All Pages -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-W27RFB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W27RFB');</script>
<!-- End Google Tag Manager -->


<script>
	function onFinder(finderType, o) {
		if (finderType == "b") {
			ajax('/ajax/accessoryFinder.asp?brandid=' + o.value, 'cbModelBox');
			ajax('/ajax/accessoryFinder.asp?modelid=-1', 'cbCatBox');
		}
		else if (finderType == "m") {
			ajax('/ajax/accessoryFinder.asp?modelid=' + o.value, 'cbCatBox');			
		} 
	}
	
	function onFinder2(finderType, o) {
		if (finderType == "b") {
			ajax('/ajax/accessoryFinder.asp?version=b&brandid=' + o.value, 'cbModelBox2');
			ajax('/ajax/accessoryFinder.asp?version=b&modelid=-1', 'cbCatBox2');
		}
		else if (finderType == "m") {
			ajax('/ajax/accessoryFinder.asp?version=b&modelid=' + o.value, 'cbCatBox2');			
		} 
	}
	
	function jumpTo() {
		var brandid = document.frmFinder.cbBrand.value;
		var modelid = document.frmFinder.cbModel.value;
		var categoryid = document.frmFinder.cbCat.value;
		var brandName = ""
		var modelName = ""
		var categoryName = ""
		var strLink = ""
		
		if (brandid != "" && modelid != "" && categoryid != "") {
			brandName = document.frmFinder.cbBrand.options[document.frmFinder.cbBrand.selectedIndex].text;
			modelName = document.frmFinder.cbModel.options[document.frmFinder.cbModel.selectedIndex].text;
			categoryName = document.frmFinder.cbCat.options[document.frmFinder.cbCat.selectedIndex].text;
			strLink = "/sb-" + brandid + "-sm-" + modelid + "-sc-" + categoryid + "-" + brandName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + modelName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + categoryName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + ".html"
			window.location = strLink;
		}
		else if (brandid != "" && modelid != "") {
			brandName = document.frmFinder.cbBrand.options[document.frmFinder.cbBrand.selectedIndex].text;
			modelName = document.frmFinder.cbModel.options[document.frmFinder.cbModel.selectedIndex].text;
			strLink = "/m-" + modelid + "-" + brandName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + modelName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-cell-phone-accessories.html"
			window.location = strLink;
		}
	}
	
	function jumpTo2() {
		var brandid = document.frmFinder2.cbBrand.value;
		var modelid = document.frmFinder2.cbModel.value;
		var categoryid = document.frmFinder2.cbCat.value;
		var brandName = ""
		var modelName = ""
		var categoryName = ""
		var strLink = ""
		
		if (brandid != "" && modelid != "" && categoryid != "") {
			brandName = document.frmFinder2.cbBrand.options[document.frmFinder2.cbBrand.selectedIndex].text;
			modelName = document.frmFinder2.cbModel.options[document.frmFinder2.cbModel.selectedIndex].text;
			categoryName = document.frmFinder2.cbCat.options[document.frmFinder2.cbCat.selectedIndex].text;
			strLink = "/sb-" + brandid + "-sm-" + modelid + "-sc-" + categoryid + "-" + brandName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + modelName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + categoryName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + ".html"
			window.location = strLink;
		}
		else if (brandid != "" && modelid != "") {
			brandName = document.frmFinder2.cbBrand.options[document.frmFinder2.cbBrand.selectedIndex].text;
			modelName = document.frmFinder2.cbModel.options[document.frmFinder2.cbModel.selectedIndex].text;
			strLink = "/m-" + modelid + "-" + brandName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + modelName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-cell-phone-accessories.html"
			window.location = strLink;
		}
	}
	
	function af_Select(which,useID) {
		if (which == "b") {
			ajax('/ajax/af.asp?brandID=' + useID,'af_model');
			ajax('/ajax/af.asp?catReset=1','af_category');
		}
		if (which == "m") { ajax('/ajax/af.asp?modelID=' + useID,'af_category'); }
	}
	
	var af_limit = 0;
	function af_submit() {
		var brandID = document.af_form.brandID.value;
		var modelID = document.af_form.modelID.value;
		var categoryID = document.af_form.categoryID.value;
		
		if (brandID == "") {
			alert("You must select at least a brand first");
		}
		else {
			document.getElementById("dumpZone").innerHTML = "";
			ajax('/ajax/af.asp?subLinkRequest=1&brandID=' + brandID + '&modelID=' + modelID + '&categoryID=' + categoryID,'dumpZone');
			setTimeout("af_linkCheck()",200);
		}
	}
	
	function af_Select2(which,useID) {
		if (which == "b") {
			ajax('/ajax/af.asp?largeField=1&brandID=' + useID,'af_model2');
			ajax('/ajax/af.asp?largeField=1&catReset=1','af_category2');
		}
		if (which == "m") { ajax('/ajax/af.asp?largeField=1&modelID=' + useID,'af_category2'); }
	}
	
	function af_submit2() {
		var brandID = document.af_form2.brandID.value;
		var modelID = document.af_form2.modelID.value;
		var categoryID = document.af_form2.categoryID.value;
		
		if (brandID == "") {
			alert("You must select at least a brand first");
		}
		else {
			document.getElementById("dumpZone").innerHTML = "";
			ajax('/ajax/af.asp?subLinkRequest=1&brandID=' + brandID + '&modelID=' + modelID + '&categoryID=' + categoryID,'dumpZone');
			setTimeout("af_linkCheck()",200);
		}
	}
	
	function af_linkCheck() {
		if (document.getElementById("dumpZone").innerHTML != "") {
			window.location = document.getElementById("dumpZone").innerHTML;
		}
		else {
			af_limit++;
			if (af_limit < 5) { setTimeout("af_linkCheck()",200); }
		}
	}
	
	function emailSubmitFieldChk(dir) {
		if (dir == "f") {
			if (document.es_form.semail.value == "Enter email address") {
				document.es_form.semail.value = "";
			}
		}
		else {
			if (document.es_form.semail.value == "") {
				document.es_form.semail.value = "Enter email address";
			}
		}
	}
	
	function emailSubmitFieldChk2(dir) {
		if (dir == "f") {
			if (document.es_form2.semail.value == "Enter your email address and save!") {
				document.es_form2.semail.value = "";
			}
		}
		else {
			if (document.es_form2.semail.value == "") {
				document.es_form2.semail.value = "Enter your email address and save!";
			}
		}
	}
	
	function emailSubmitFieldChk3(dir) {
		if (dir == "f") {
			if (document.es_formBanner.semail.value == "Enter your email address...") {
				document.es_formBanner.semail.value = "";
			}
		}
		else {
			if (document.es_formBanner.semail.value == "") {
				document.es_formBanner.semail.value = "Enter your email address...";
			}
		}
	}
	
	$(document).ready(function(){
		function setCookie(cname,cvalue)
		{
			document.cookie = cname + "=" + cvalue + ";"
		}
		
		function getCookie(cname)
		{
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) 
		  {
		  var c = ca[i].trim();
		  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		  }
		return "";
		}
		
		var heartbleed_cookie = getCookie("heartbleed");
		if(!heartbleed_cookie) {
			$("#heartbleed-bar").show();
		} else {
			$("#heartbleed-bar").hide();
		}
		
		$(".heartbleed-close").click(function(){
			$("#heartbleed-bar").hide();
			setCookie("heartbleed", 1);
		});
	});
</script>
<!-- promocode:  -->
<link rel="stylesheet" type="text/css" href="/includes/css/account_loginMenu.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/sitewide/bVersion.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/heartbleed/on.css" />
Server IP: 192.168.114.3
<link href='//fonts.googleapis.com/css?family=Open+Sans:600,400|Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
<div id="popCover" onclick="closeReviewPopup()" style="height:100%; display:none; width:100%; background-color:#333; position:fixed; left:0px; top:0px; z-index:3000; opacity:0.6; filter:alpha(opacity=60);">&nbsp;</div>
<div id="popBox" style="display:none; position:fixed; left:20%; right:20%; top:0px; z-index:3001; margin:auto; text-align:center;"></div>
<div id="topSelect"></div>
<div id="heartbleed-bar" style="display:none;">
	<div class="heartbleed-main">
		<div class="heart-gray"></div>
		<div class="heartbleed-text">Worried about Heartbleed? Don't be. We're making sure you're safe while you shop! <a href="https://filippo.io/Heartbleed/#cellularoutfitter.com" target="_blank">See For Yourself &raquo;</a></div>
	</div>
	<div class="heartbleed-close"></div>
</div>
<div id="siteTopBar" style="width:100%; height:28px; background-image:url(/images/backgrounds/top-bar-dblue.png);">
	<div style="width:1040px; height:28px; margin-left:auto; margin-right:auto;">
        <div style="float:right; background-image:url(/images/backgrounds/top-bar-green.png); padding:0px 20px 0px 20px; height:28px;">
            <a href="/cart/basket.asp">
            <div style="float:left" id="cartIcon"></div>
            <div id="siteTop-CartBar" style="float:left; color:#FFF; font-weight:bold; font-size:10px; padding:7px 10px 0px 10px;">0 Items - $0.00</div>
            <div style="float:left"><div id="cardDownArrow"></div></div>
            </a>
        </div>
        <div id="account_nav"></div>
        <div style="float:right;"><div id="topMenuBr"></div></div>
        <div style="float:right; font-weight:bold; padding:7px 20px 0px 20px;"><a href="/contact-us.html" style="color:#FFF; font-size:10px;">Contact Us</a></div>
        <div style="float:right;"><div id="topMenuBr"></div></div>
        <div style="float:right; font-weight:bold; padding:7px 20px 0px 20px;"><a href="/faq.html" style="color:#FFF; font-size:10px;">Help</a></div>
        <div style="float:right;"><div id="topMenuBr"></div></div>
        <div style="float:right; font-weight:bold; padding:7px 20px 0px 20px;"><a href="/track-your-order.html" style="color:#FFF; font-size:10px;">Order Status</a></div>
		<!--
        <div style="float:right;"><div id="topMenuBr"></div></div>
        <div style="float:right; height:28px; overflow:hidden;">
            <script type="text/javascript">
                var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                var lhnid = 3410;
                var lhnwindow = 6755;
                var lhnImage = "<div id='lhnchatimg' title='Live Help'></div>";
                document.write(unescape("%3Cscript src='/framework/utility/liveHelp.js' type='text/javascript'%3E%3C/script%3E"));
            </script>
        </div>
        -->
	</div>
</div>
<div id="floatingTab_email" class="emailFloatingTab" style="bottom:-75px; display:none;">
	<div class="tb floatingTabText">
    	<div class="fl">Get <span style="color:#f1c40f; font-weight:bold; font-size:15px;">25% OFF</span> today's order, instantly.</div>
        <div class="fr minEmailOffer" onclick="showEmailOfferTab()"><div id="floatingTabValue" class="fr minEmailBar">+</div></div>
    </div>
    <div class="tb floatingEmailBox">
    	<form name="es_form2" method="post" onsubmit="return(addNewsletter(document.es_form2.semail.value,'Popup Widget'));" style="margin:0px; padding:0px;">
        <div class="fl">
	        <div class="fl floatingEmailFieldBox">
    	        <input type="text" name="semail" value="Enter your email address and save!" class="esField2" onfocus="emailSubmitFieldChk2('f')" onblur="emailSubmitFieldChk2('b')" />
        	</div>
	        <div class="fl floatingEmailSubmitBttn" onclick="addNewsletter(document.es_form2.semail.value,'Popup Widget');">SUBMIT</div>
        </div>
        </form>
    </div>
    <div id="emailDumpZone" style="display:none;"></div>
</div>
<table width="1020" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF" style="border-left:1px solid #ccc; border-right:1px solid #ccc;">
    <tr>
        <td id="siteTopBar2" valign="top" style="padding:5px 10px 0px 10px;">
            <div id="logoLine_a" style="float:left; width:1020px;">
            	<div style="float:left; width:250px; height:60px;">
                	
                    <a href="/"><div id="logo" title="CellularOutfitter.com"></div></a>
                    
					
                </div>
                <div style="float:right; width:750px; padding-top:5px;">
                	<div style="float:left; width:750px;">
                    	<div id="mvt_newTopHeader1" style="margin-left:10px; float:right;">
                            <div style="float:right; padding-left:5px;">
                                <a href="javascript:Open_Popup('https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.CELLULAROUTFITTER.COM&lang=en','VeriSign','width=560,height=500,location=yes,status=yes,resizable=yes,scrollbars=yes');"><div id="verisignLogo" title="CELLULAR OUTFITTER, INC. has been verified by Verisign"></div></a>
                            </div>
                        	<a href="/shipping-policy.html"><div id="topHeaderUVP1_2" style="border-right:2px dotted #ccc; padding-right:5px; margin-right:5px;"></div></a>
                        	<a href="/lowest-price.html"><div id="topHeaderUVP1_1" style="border-right:2px dotted #ccc; padding-right:5px; margin-right:5px;"></div></a>
                        </div>
                        <div class="fr topSearchMainBox">
                        	<form name="topSearchForm" method="get" action="/widgets/nxtSearch/search.asp" style="margin:0px; padding:0px;">
                            <div class="fl topSearchBox">
                            	<div class="fl topSearchMag"></div>
                                <div class="fl topSearchFieldBox">
	                                <input type="text" name="search" value="What are you looking for?" style="border: 0px; background-color: transparent; color: #999; width: 180px; font-family: 'Open Sans', sans-serif; font-weight: 600;" class="searchInputBig" onfocus="this.value='';" />
    	                            <input type="hidden" name="sitesearch" value="Y2" />
                                </div>
                                <div class="fr topSearchSubmit" onclick="document.topSearchForm.submit()">Go!</div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding:5px 10px 0px 10px;">
        	<div id="topNav">
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:80px;">
					<a href="/c-3-cell-phone-covers-and-skins.html" title="Cell Phone Covers & Gel Skins" class="cat-font2">Covers & Gel Skins</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:90px;">
					<a href="/c-7-cell-phone-cases-pouches.html" title="Cell Phone Cases & Pouches" class="cat-font2">Cases & Pouches</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:14px 5px 0px 5px; width:110px;">
					<a href="/c-18-cell-phone-screen-protectors.html" title="Cell Phone Screen Protectors" class="cat-font2">Screen Protectors</a>
                </div>
                <!--
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:80px;">
					<a href="/c-24-cell-phone-custom-cases.html" title="Cell Phone Custom Cases" class="cat-font2">Custom<br />Cases</a>
                </div>
                -->
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:100px;">
					<a href="/c-2-cell-phone-chargers-and-data-cables.html" title="Cell Phone Chargers & Data Cables" class="cat-font2">Chargers & Data Cables</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:14px 5px 0px 5px; width:90px;">
					<a href="/c-1-cell-phone-batteries.html" title="Cell Phone Batteries" class="cat-font2">Batteries</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:100px;">
					<a href="/c-5-cell-phone-hands-free-kits-bluetooth-headsets.html" title="Cell Phone Hands-Free Kits & Headsets" class="cat-font2">Bluetooth & Hands-Free</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:90px;">
					<a href="/c-6-cell-phone-holsters-car-mounts.html" title="Cell Phone Holsters & Car Mounts" class="cat-font2">Holsters & Car Mounts</a>
                </div>
                <div id="topNav-Div"></div>
				<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:100px;">
					<a href="/c-8-cell-phone-other-accessories.html" title="Cell Phone Other Accessories" class="cat-font2">Other Accessories</a>
                </div>
                <div id="topNav-Div"></div>
				<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:110px;">
					<a href="/wholesale-cell-phones.html" title="Wholesale Cell Phones" class="cat-font2">Wholesale Cell Phones</a>
                </div>
            </div>
            <div id="topAccessoryFinder_alt2">
            	<form name="af_form" method="post" onsubmit="return(false)" style="margin:0px; padding:0px;">
            	<div class="fl smallAccessoryFinderText">Find Accessories:</div>
                <div class="fl accessoryFinderField">
                	<select name="brandID" class="accessorySelect" onchange="af_Select('b',this.value)">
                    	<option value="">1. Phone Brand</option>
                        <optgroup label="Top Brands">
                            <option value="9">Samsung</option>
                            <option value="17">Apple</option>
                            <option value="4">LG</option>
                            <option value="30">ZTE</option>
                            <option value="5">Motorola</option>
                            <option value="7">Nokia</option>
                        </optgroup>
                        <optgroup label="Device Brands">
                        
                        <option value="17">Apple</option>
                        
                        <option value="14">BlackBerry</option>
                        
                        <option value="28">Casio</option>
                        
                        <option value="16">HP/Palm</option>
                        
                        <option value="20">HTC</option>
                        
                        <option value="29">Huawei</option>
                        
                        <option value="3">Kyocera/Qualcomm</option>
                        
                        <option value="4">LG</option>
                        
                        <option value="5">Motorola</option>
                        
                        <option value="6">Nextel</option>
                        
                        <option value="7">Nokia</option>
                        
                        <option value="18">Pantech</option>
                        
                        <option value="9">Samsung</option>
                        
                        <option value="10">Sanyo</option>
                        
                        <option value="19">Sidekick</option>
                        
                        <option value="11">Siemens</option>
                        
                        <option value="2">Sony Ericsson</option>
                        
                        <option value="1">UTStarcom</option>
                        
                        <option value="30">ZTE</option>
                        
                        </optgroup>
                    </select>
                </div>
                <div id="af_model" class="fl accessoryFinderField">
                	<select name="modelID" class="accessorySelect">
                    	<option value="">2. Phone Model</option>
                        <option value="">Select Brand First</option>
                    </select>
                </div>
                <div id="af_category" class="fl accessoryFinderField">
                	<select name="categoryID" class="accessorySelect">
                    	<option value="">3. Category</option>
                        <option value="">Select Model First</option>
                    </select>
                </div>
                <div class="fl accessoryFinderSubmit" onclick="af_submit()"></div>
                </form>
                <div class="fl emailSubmitBox">
                	<div class="tb emailSubmitLrgTxt">Get <span style="color:#f1c40f; font-weight:bold;">25% OFF</span></div>
                    <div class="tb emailSubmitSmlTxt">TODAY'S ORDER, INSTANTLY.</div>
                </div>
                <form name="es_form" method="post" onsubmit="return(addNewsletter(document.es_form.semail.value,'Top Widget'));" style="margin:0px; padding:0px;">
                <div class="fl accessoryFinderField">
                	<input type="text" name="semail" value="Enter email address" class="esField" onfocus="emailSubmitFieldChk('f')" onblur="emailSubmitFieldChk('b')" />
                </div>
                <div class="fl emailSubmitBttn" onclick="addNewsletter(document.es_form.semail.value,'Top Widget');"></div>
                </form>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding:0px 10px 0px 10px;">
			
<link rel="stylesheet" type="text/css" href="/includes/css/pdp.css" />

    <link rel="stylesheet" href="/includes/css/colorbox.css" />
    <script src="/includes/js/jquery.colorbox.js"></script>
    <script>
        $(document).ready(function(){
			try {
	            $(".linkVideo").colorbox({iframe:true, innerWidth:"60%", innerHeight:"60%", rel:'linkVideo'});
			} catch(e) {
//				alert('lightbox message:' + e.message);
			}
            if (document.getElementById('id_linkVideo') != null) document.getElementById('id_linkVideo').style.display = '';
        });
    </script>

<div>
    <div id="promoBanner1" style="margin:10px 0px 10px 0px; cursor:pointer;" onclick="checkPromo('yes')"><img src="/images/banners/penPromo.jpg" border="0" /></div>
    <!--<div id="promoBanner2" style="margin:10px 0px 10px 0px; cursor:pointer;" onclick="checkPromo('yes')"><img src="/images/banners/" border="0" /></div>-->
</div>
<!-- rich snippets-->
<div itemscope itemtype="http://data-vocabulary.org/Product">
    <span itemprop="condition" content="new"></span>
    <span itemprop="color" content=""></span>
    <span itemprop="price" content="37.99"></span>
    <span itemprop="brand" content="Samsung"></span>
    <span itemprop="model" content="Galaxy S5"></span>
    
<table width="100%" border="0" bordercolor="#FF0000" cellspacing="0" cellpadding="0" align="center">
	<tr>
	<td class="top-sublink-gray">
    	<div>
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" style="float:left;"><a class="top-sublink-gray" href="http://www.CellularOutfitter.com/" itemprop="url"><span itemprop="title">Home</span></a>&nbsp;&rsaquo;&nbsp;</div>
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" style="float:left;"><a class="top-sublink-gray" href="http://www.cellularoutfitter.com/b-9-samsung-cell-phone-accessories.html" itemprop="url"><span itemprop="title">Samsung Cell Phone Accessories</span></a>&nbsp;&rsaquo;&nbsp;</div>
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" style="float:left;"><a class="top-sublink-gray" href="http://www.cellularoutfitter.com/m-1674-samsung-galaxy-s5-cell-phone-accessories.html" itemprop="url"><span itemprop="title">Samsung Galaxy S5 Cell Phone Accessories</span></a>&nbsp;&rsaquo;&nbsp;</div>
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" style="float:left;"><a class="top-sublink-gray" href="http://www.cellularoutfitter.com/sb-9-sm-1674-sc-3-samsung-galaxy-s5-covers-screen-guards-cell-phone-accessories.html" itemprop="url"><span itemprop="title">Samsung Galaxy S5 Covers &amp; Screen Guards</span></a>&nbsp;&rsaquo;&nbsp;</div>
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" style="float:left;"><span class="top-sublink-blue" itemprop="title">Samsung Galaxy S5 Trident Kraken A.M.S. Case w/Built-in Screen Protector (Blue/Black)</span></div>        
        </div>
		
	</td>
</tr>

    <tr>
    	<td style="padding-top:10px; width:900px;" valign="top">
            <div style="width:300px; float:left;">
            	<div style="float:left;position:relative" id="mainProdPic_3">
                	<img onclick="showZoom();" style="cursor:pointer;" itemprop="image" src="http://www.cellularoutfitter.com//productPics/big/samsung-galaxy-s5-trident-kraken-a-m-s-case-w-built-in-screen-protector-blue-black-21729.jpg" alt="Samsung Galaxy S5 Trident Kraken A.M.S. Case w/Built-in Screen Protector (Blue/Black)" border="0" id="imgLarge" height="300" /><div align="center" style="padding-top:5px;"><a href="javascript:showZoom();"><img src="/images/product/co-pdp-zoom-icon.png" border="0" /></a></div></div>
                <div style="width:300px; height:70px; padding-top:10px; float:left;">
                	
                            <a class="linkVideo" href="http://www.youtube.com/embed/hG96OlqeIL0?rel=0&amp;wmode=transparent" title="Trident Kraken A.M.S. Series Overview" style="float:left; border:1px solid #CCC; margin:0px 3px 3px 0px;"><img src="/images/pdp/ico-video.png" width="60" height="60" /></a>
                            
					<div style="float:left; cursor:pointer; border:1px solid #CCC; margin:0px 3px 3px 0px;" onmouseover="fnPreviewImage('/productPics/big/samsung-galaxy-s5-trident-kraken-a-m-s-case-w-built-in-screen-protector-blue-black-21729.jpg')"><img src="/productPics/alts/thumbs/samsung-galaxy-s5-trident-kraken-a-m-s-case-w-built-in-screen-protector-blue-black-21729.jpg" width="60" height="60" /></div>
                    
                    <div style="float:left; cursor:pointer; border:1px solid #CCC; margin:0px 3px 3px 0px;" onmouseover="fnPreviewImage('/altPics/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-0.jpg')"><img src="/productPics/alts/thumbs/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-0.jpg" width="60" height="60" /></div>
                    
                    <div style="float:left; cursor:pointer; border:1px solid #CCC; margin:0px 3px 3px 0px;" onmouseover="fnPreviewImage('/altPics/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-1.jpg')"><img src="/productPics/alts/thumbs/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-1.jpg" width="60" height="60" /></div>
                    
                    <div style="float:left; cursor:pointer; border:1px solid #CCC; margin:0px 3px 3px 0px;" onmouseover="fnPreviewImage('/altPics/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-2.jpg')"><img src="/productPics/alts/thumbs/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-2.jpg" width="60" height="60" /></div>
                    
                    <div style="float:left; cursor:pointer; border:1px solid #CCC; margin:0px 3px 3px 0px;" onmouseover="fnPreviewImage('/altPics/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-3.jpg')"><img src="/productPics/alts/thumbs/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-3.jpg" width="60" height="60" /></div>
                    
                    <div style="float:left; cursor:pointer; border:1px solid #CCC; margin:0px 3px 3px 0px;" onmouseover="fnPreviewImage('/altPics/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-4.jpg')"><img src="/productPics/alts/thumbs/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-4.jpg" width="60" height="60" /></div>
                    
                    <div style="float:left; cursor:pointer; border:1px solid #CCC; margin:0px 3px 3px 0px;" onmouseover="fnPreviewImage('/altPics/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-5.jpg')"><img src="/productPics/alts/thumbs/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-5.jpg" width="60" height="60" /></div>
                    
                </div>
            </div>
            <div id="buyNow1" style="float:left; margin-left:20px; width:420px; padding-left:20px;">
            	<div id="itemDesc_1" style="padding-bottom:10px;"><span itemprop="name"><h1>Samsung Galaxy S5 Trident Kraken A.M.S. Case w/Built-in Screen Protector (Blue/Black)</h1></span></div>
                <div class="tb" style="color:#999; font-size:12px; padding-bottom:10px;">
                    <div style="float:left;">Item ID: 320857</div>
                </div>                
                <!-- rich snippet review -->
                <span itemprop="review" itemscope itemtype="http://data-vocabulary.org/Review-aggregate">
                <div style="padding-bottom:10px; border-bottom:1px dotted #CCC;">
                	<img src="/images/review/grayStar.gif" border="0" width="14" height="13" /> <img src="/images/review/grayStar.gif" border="0" width="14" height="13" /> <img src="/images/review/grayStar.gif" border="0" width="14" height="13" /> <img src="/images/review/grayStar.gif" border="0" width="14" height="13" /> <img src="/images/review/grayStar.gif" border="0" width="14" height="13" />  &nbsp; 0.0 &nbsp; (0 Reviews)<br><a style="font-size:11px; text-decoration:underline;" href="#" onclick="showReviewPopup()">Be the first to review this product!</a>
                </div>
                </span>
                <!--// rich snippet review -->
                <!-- rich snippet product details -->
                <span itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer">
                <meta itemprop="currency" content="USD" />
                
<div class="tb buyNowDetailRow">
	<div class="fl itemBuyNowTitle" style="padding-top:7px;">Retail Price:</div>
    <div class="fl itemRetailValue">$49.95</div>
</div>
<div class="tb buyNowDetailRow sitewideSale">
	<div class="fl itemBuyNowTitle2">Last Day Sitewide Sale:</div>
    <div class="fl">$37.99</div>
</div>
<div class="tb buyNowDetailRow">
	<div class="fl itemBuyNowTitle">You Save:</div>
    <div class="fl itemSaveValue">$11.96 (24%)</div>
</div>
<div id="addItemForm1">
	
    <div class="tb" style="font-weight:bold; color:#F00; padding-bottom:10px;">Temporarily Out of Stock</div>
    <span itemprop="availability" content="out_of_stock"></span>
    
    <div style="cursor:pointer; margin:0px auto 10px auto; width:375px;" onclick="checkPromo('yes')">
        <img id="penPromoSmall" src="/images/banners/penPromoSmall.jpg" border="0" />
    </div>
    
    <!-- Add Screen Protector: 3,-1 -->
    
    </form>
</div>
<div id="addItemForm2">
	
    <div class="tb" style="font-weight:bold; color:#F00; padding-bottom:10px;">Temporarily Out of Stock</div>
    <span itemprop="availability" content="out_of_stock"></span>
    
    <div style="cursor:pointer; margin:0px auto 10px auto; width:375px;" onclick="checkPromo('yes')">
        <img id="stickyPromoSmall" src="/images/banners/stickyPromoSmall.jpg" border="0" />
    </div>
    
    <!-- Add Screen Protector: 3,-1 -->
    
    </form>
</div>
<div class="tb" style="border-top:1px dotted #CCC; padding:10px 0px 10px 0px; width:420px;">
	
    <div class="tb productCheckRow">
        <div style="float:left; width:17px;"><img src="/images/icons/check.png" border="0" width="17" height="14" /></div>
        <div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#333;">Shields your phone from scratches, dings and bumps</div>
    </div>
    
    <div class="tb productCheckRow">
	    <div style="float:left; width:17px;"><img src="/images/icons/check.png" border="0" width="17" height="14" /></div>
    	<div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#333;">Leaves Our US Warehouse Within 24 Hours</div>
    </div>
    <div class="tb productCheckRow">
	    <div style="float:left; width:17px;"><img src="/images/icons/check.png" border="0" width="17" height="14" /></div>
	    <div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#333;">Lowest Prices Online Guaranteed</div>
    </div>
    <div class="tb productCheckRow">
	    <div style="float:left; width:17px;"><img src="/images/icons/check.png" border="0" width="17" height="14" /></div>
    	<div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#333;">
            
	            Unconditional 90 Day Return Policy
    	        
        </div>
    </div>
</div>
<div class="tb paymentOptions"></div>
                </span>
            </div>
            
            <div id="relatedItems_new" style="float:left; width:600px;">
                <div class="strandsRecs fl" tpl="PROD-4" item="320857"></div>
                <div class="strandsRecs fl" tpl="prod_1" item="320857"></div>
            </div>
            <div id="detailTabs" style="float:left; border:1px solid #ccc; margin-top:57px; position:relative; width:780px;">
            	<div style="position:absolute; top:-27px; left:-1px;" id="prodTabs">
                	<div style="float:left;"><img id="tabHeaderLeft1" src="/images/backgrounds/productTab_on_left.gif" border="0" width="28" height="27" /></div>
                    <div id="tabHeader1" class="prodTab_on" onclick="togglePdpTab(1,4)">Product Description</div>
                    <div style="float:left;"><img id="tabHeaderRight1" src="/images/backgrounds/productTab_on_right.gif" border="0" width="28" height="27" /></div>
                    
                    <div style="float:left;"><img id="tabHeaderLeft2" src="/images/backgrounds/productTab_off_left.gif" border="0" width="28" height="27" /></div>
                    <div id="tabHeader2" class="prodTab_off" onclick="togglePdpTab(2,4)">Compatibility</div>
                    <div style="float:left;"><img id="tabHeaderRight2" src="/images/backgrounds/productTab_off_right.gif" border="0" width="28" height="27" /></div>
                    
                    <div style="float:left;"><img id="tabHeaderLeft3" src="/images/backgrounds/productTab_off_left.gif" border="0" width="28" height="27" /></div>
                    <div id="tabHeader3" class="prodTab_off" onclick="togglePdpTab(3,4)">Reviews</div>
                    <div style="float:left;"><img id="tabHeaderRight3" src="/images/backgrounds/productTab_off_right.gif" border="0" width="28" height="27" /></div>
                    
                    <div style="float:left;"><img id="tabHeaderLeft4" src="/images/backgrounds/productTab_off_left.gif" border="0" width="28" height="27" /></div>
                    <div id="tabHeader4" class="prodTab_off" onclick="togglePdpTab(4,4)">Q &amp; A</div>
                    <div style="float:left;"><img id="tabHeaderRight4" src="/images/backgrounds/productTab_off_right.gif" border="0" width="28" height="27" /></div>                    
                </div>
                <div style="padding:10px; color:#333; font-size:12px;" id="tabBody1">
					<div><span itemprop="description">Meet the case that adapts to your lifestyle. Kraken A.M.S. by Trident is a heavy-duty three-in-one protective option featuring a shock-absorbing silicone layer, hardened polycarbonate exterior shell and detachable aluminum media stand. Provides the option to attach a variety of interchangeable accessories (sold separately). Includes self-adhesive screen protector.</span></div>
                    
                    <div style="padding-top:5px;">&bull;<span style="padding-left:10px;">Samsung Galaxy S5 Trident Kraken A.M.S Case</span></div>
                    
                    <div style="padding-top:5px;">&bull;<span style="padding-left:10px;">Adapts to your lifestyle!</span></div>
                    
                    <div style="padding-top:5px;">&bull;<span style="padding-left:10px;">Heavy-duty three-in-one protective option</span></div>
                    
                    <div style="padding-top:5px;">&bull;<span style="padding-left:10px;">Shock-absorbing silicone layer</span></div>
                    
                    <div style="padding-top:5px;">&bull;<span style="padding-left:10px;">Solid polycarbonate shell</span></div>
                    
                    <div style="padding-top:5px;">&bull;<span style="padding-left:10px;">Detachable aluminum media stand</span></div>
                    
                    <div style="padding-top:5px;">&bull;<span style="padding-left:10px;">Includes screen protector</span></div>
                    
                    <div style="padding-top:5px;">&bull;<span style="padding-left:10px;">Accessorize with A.M.S. attachments (sold separately)</span></div>
                    
                    <div style="padding-top:5px;">&bull;<span style="padding-left:10px;">Color: Blue/Black</span></div>
                    
                    <div style="padding-top:5px;">&bull;<span style="padding-left:10px;">Manufacturer Part No.: KN-SSGXS5-BL000</span></div>
                    					
                    <div id="contentText" style="padding-top:5px;"></div>
                </div>
                <div style="padding:10px; color:#333; font-size:12px; display:none; overflow:auto; height:280px;" id="tabBody2">
					<div style='float:left; width:200px; font-size:11px; padding-right:15px; background-color:#fff; height:15px;'>Samsung Galaxy S5</div>
                </div>
                <div style="padding:10px; color:#333; font-size:12px; display:none; overflow:auto; height:280px;" id="tabBody3">
				
				    <div style="color:#222; font-size:13px;">
                        <div style="font-size:18px;">Samsung Galaxy S5 Trident Kraken A.M.S. Case w/Built-in Screen Protector (Blue/Black)</div>
                        <div style="display:table; padding-top:5px;">
                            <div style="float:left;"><img src="/images/product/review/star-empty.png" border="0" /> <img src="/images/product/review/star-empty.png" border="0" /> <img src="/images/product/review/star-empty.png" border="0" /> <img src="/images/product/review/star-empty.png" border="0" /> <img src="/images/product/review/star-empty.png" border="0" /> </div>
                            <div style="float:left; padding-left:10px; font-size:16px; font-weight:bold;">0.0</div>
                            <div style="float:left; padding:2px 0px 0px 10px;">(based on 0 reviews)</div>
                        </div>
                        <div style="display:table; margin:20px 0px 20px 0px;">
                            <div style="float:left; padding:0px 0px 0px 40px; width:329px; border-right:1px solid #ccc;">
                                <div style="float:left; width:100%; font-size:16px; font-weight:bold;">Overall Rating Totals:</div>
                                <div style="float:left; width:100%; padding-top:10px;">
                                	
	                            	<div style="float:left; width:100%; padding-top:3px;">
                                        <div style="float:left; color:#034076; text-decoration:underline;">5 star</div>
                                        <div style="float:left; margin-left:5px; width:200px;">
	                                        <div style="float:left; background-color:#e6e6e6; width:100%; height:15px;"></div>
                                        </div>
                                        <div style="float:left; margin-left:5px;">0</div>
                                    </div>
                                    
	                            	<div style="float:left; width:100%; padding-top:3px;">
                                        <div style="float:left; color:#034076; text-decoration:underline;">4 star</div>
                                        <div style="float:left; margin-left:5px; width:200px;">
	                                        <div style="float:left; background-color:#e6e6e6; width:100%; height:15px;"></div>
                                        </div>
                                        <div style="float:left; margin-left:5px;">0</div>
                                    </div>
                                    
	                            	<div style="float:left; width:100%; padding-top:3px;">
                                        <div style="float:left; color:#034076; text-decoration:underline;">3 star</div>
                                        <div style="float:left; margin-left:5px; width:200px;">
	                                        <div style="float:left; background-color:#e6e6e6; width:100%; height:15px;"></div>
                                        </div>
                                        <div style="float:left; margin-left:5px;">0</div>
                                    </div>
                                    
	                            	<div style="float:left; width:100%; padding-top:3px;">
                                        <div style="float:left; color:#034076; text-decoration:underline;">2 star</div>
                                        <div style="float:left; margin-left:5px; width:200px;">
	                                        <div style="float:left; background-color:#e6e6e6; width:100%; height:15px;"></div>
                                        </div>
                                        <div style="float:left; margin-left:5px;">0</div>
                                    </div>
                                    
	                            	<div style="float:left; width:100%; padding-top:3px;">
                                        <div style="float:left; color:#034076; text-decoration:underline;">1 star</div>
                                        <div style="float:left; margin-left:5px; width:200px;">
	                                        <div style="float:left; background-color:#e6e6e6; width:100%; height:15px;"></div>
                                        </div>
                                        <div style="float:left; margin-left:5px;">0</div>
                                    </div>
                                    
								</div>
                            </div>
                            <div style="float:left; width:320px; padding:0px 0px 0px 50px;">
                                <div style="float:left; width:100%; font-size:16px; font-weight:bold; padding-top:20px;">Be the first to review this product!</div>
                                <div style="float:left; width:100%; padding-top:10px;">
                                    <img src="/images/product/review/cta-write-review.png" border="0" alt="write a review" style="cursor:pointer;" onclick="showReviewPopup()" />
                                </div>
                            </div>
                        </div>
					</div>
				
                </div>
				<div style="padding:10px; color:#333; font-size:12px; display:none;" id="tabBody4">
					<script type="text/javascript">
                        var TurnToItemSku = "320857";
                        document.write(unescape("%3Cscript src='" + document.location.protocol + "//static.www.turnto.com/sitedata/" + turnToConfig.siteKey + "/" + TurnToItemSku + "/d/itemjs' type='text/javascript'%3E%3C/script%3E"));
                    </script>
                	<table border="0" width="100%"><tr><td align="center"><span class="TurnToItemTeaser"></span></td></tr></table>
                    <div>
						                    
                    </div>
                </div>
            </div>
            <div id="trustLogos" style="float:left; margin:20px 0px 10px 0px; text-align:center; width:780px;"><img src="/images/icons/trust.jpg" border="0" /></div>
			<div id="googleAd" style="float:left; margin:20px 0px 20px 0px; text-align:center; width:780px;">
            	<script type="text/javascript">
					<!--
					google_ad_client = "ca-pub-1001075386636876";
					/* CO CA Bottom */
					google_ad_slot = "7359681427";
					google_ad_width = 728;
					google_ad_height = 90;
					//-->
				</script>
				<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
            </div>
        </td>
        <td valign="top">
        	<div style="float:right; width:200px; background-color:#e5e5e5; padding:10px; border-radius:10px;" id="rightSideTestimonial1">
            	<div style="float:left; width:180px; padding:10px; background-color:#fff; border:1px solid #c3c3c3; border-radius:10px;">
                	<div style="float:left; width:170px; padding-bottom:10px; font-weight:bold; color:#002b55; font-size:13px; text-align:center;">Your Recommendations:</div>
                    <div class="fl strandsRecs" tpl="PROD-2" style="margin-left:25px;" item="320857"></div>
                </div>
            </div>
            <div style="float:right; width:200px; background-color:#e5e5e5; margin-top:10px; padding:10px; border-radius:10px;" id="rightSideTestimonial1">
            	<div style="float:left; width:180px; padding:10px; background-color:#f9f9f9; border:1px solid #c3c3c3; border-radius:10px;">
                	<div style="float:left; width:170px; padding-bottom:10px;"><img src="/images/icons/testimonial-header.jpg" border="0" /></div>
                    
					<div style="float:left; width:19px; padding-right:10px;"><img src="/images/icons/yellow-star.jpg" border="0" /></div>
                    <div style="float:left; width:140px; font-size:12px; padding-bottom:10px; color:#666;">
                    	"I love how simple it is to buy on this site. I just click on my phone model, click on the accessories I want to buy, then wait! So much simpler than going out to the mall and walking around, and way more affordable too!"
                    </div>
                    <div style="float:left; width:160px; font-size:12px; color:#999;">
                    	<span style="font-size:16px; font-weight:bold; color:#3178b6">Jennifer</span><br />
                        from Jackson MS
                    </div>
                    
                        <div style="float:left; padding:10px 0px 10px 0px; width:170px;"><hr color="#CCCCCC" /></div>
                        
					<div style="float:left; width:19px; padding-right:10px;"><img src="/images/icons/yellow-star.jpg" border="0" /></div>
                    <div style="float:left; width:140px; font-size:12px; padding-bottom:10px; color:#666;">
                    	"My experience with Cellular Outfitters was exceptional. It was quick to order and I received it promptly. Thank you!"
                    </div>
                    <div style="float:left; width:160px; font-size:12px; color:#999;">
                    	<span style="font-size:16px; font-weight:bold; color:#3178b6">Don</span><br />
                        from Dallas TX
                    </div>
                    
                        <div style="float:left; padding:10px 0px 10px 0px; width:170px;"><hr color="#CCCCCC" /></div>
                        
					<div style="float:left; width:19px; padding-right:10px;"><img src="/images/icons/yellow-star.jpg" border="0" /></div>
                    <div style="float:left; width:140px; font-size:12px; padding-bottom:10px; color:#666;">
                    	"I have a rather old phone and never needed a new battery until recently. I thought no one would carry such an old phone model's battery, but I was very pleased to see that Cellular Outfitter had them at a very affordable price!"
                    </div>
                    <div style="float:left; width:160px; font-size:12px; color:#999;">
                    	<span style="font-size:16px; font-weight:bold; color:#3178b6">Paul</span><br />
                        from Austin TX
                    </div>
                    
                </div>
            </div>
            <!--
            <div style="float:right; width:200px; background-color:#e5e5e5; margin-top:20px; padding:10px; border-radius:10px;" id="rightSideLivePurchase">
            	<div id="livePurchaseDiv" style="float:left; width:180px; padding:10px; background-color:#f9f9f9; border:1px solid #c3c3c3; border-radius:10px;"></div>
            </div>
            -->
        </td>
    </tr>
</table>
</div>

<!--// rich snippets-->

	<div id="id_zoomimage" style="display:none;">
		<div style="display:inline-block; width:900px; padding:3px; border:3px solid #444; background-color:#FFF; margin-left:auto; margin-right:auto; margin-top:30px; border-radius:5px; -moz-box-shadow: 1px 5px 5px #333; -webkit-box-shadow: 1px 5px 5px #333; box-shadow: 5px 5px 5px #333;">
			<div style="float:left; width:100%; position:relative;">
				<div style="position:absolute; top:-15px; right:-15px;"><a href="javascript:closeZoom();"><img src="/images/product/close-button.png" border="0" /></a></div>    
			</div>
			<div style="float:left; text-align:center; width:100%;">
				<div style="float:left; width:800px; text-align:center;">
                
					<img id="id_img_zoom" src="/productpics/big/zoom/samsung-galaxy-s5-trident-kraken-a-m-s-case-w-built-in-screen-protector-blue-black-21729.jpg" border="0" />
                
                	
				</div>
				<div style="float:left; width:80px; text-align:center; padding:10px;">
	                <div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onmouseover="fnSwapZoomImage('/productpics/big/zoom/samsung-galaxy-s5-trident-kraken-a-m-s-case-w-built-in-screen-protector-blue-black-21729.jpg');"><img id="id_altimg_zoom" src="/productpics/big/zoom/samsung-galaxy-s5-trident-kraken-a-m-s-case-w-built-in-screen-protector-blue-black-21729.jpg" width="60" height="60" border="0" /></div>
					
                        <div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onmouseover="fnSwapZoomImage('/altPics/zoom/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-0.jpg');"><img id="id_altimg_zoom0" src="/altPics/zoom/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-0.jpg" width="60" height="60" border="0" /></div>
                        
                        <div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onmouseover="fnSwapZoomImage('/altPics/zoom/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-1.jpg');"><img id="id_altimg_zoom1" src="/altPics/zoom/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-1.jpg" width="60" height="60" border="0" /></div>
                        
                        <div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onmouseover="fnSwapZoomImage('/altPics/zoom/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-2.jpg');"><img id="id_altimg_zoom2" src="/altPics/zoom/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-2.jpg" width="60" height="60" border="0" /></div>
                        
                        <div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onmouseover="fnSwapZoomImage('/altPics/zoom/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-3.jpg');"><img id="id_altimg_zoom3" src="/altPics/zoom/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-3.jpg" width="60" height="60" border="0" /></div>
                        
                        <div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onmouseover="fnSwapZoomImage('/altPics/zoom/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-4.jpg');"><img id="id_altimg_zoom4" src="/altPics/zoom/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-4.jpg" width="60" height="60" border="0" /></div>
                        
                        <div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onmouseover="fnSwapZoomImage('/altPics/zoom/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-5.jpg');"><img id="id_altimg_zoom5" src="/altPics/zoom/trident-kraken-a-m-s-case-w-built-in-screen-protector-for-samsung-galaxy-s5-blue-black-21729-5.jpg" width="60" height="60" border="0" /></div>
                                    
                </div>
			</div>
		</div>
	</div>
	
	<script>
		function closeZoom() {
			document.getElementById("popCover").style.display = "none";
			document.getElementById("popBox").style.display = "none";
			document.getElementById("popBox").innerHTML = "";
		}
		
		function showZoom() {
			document.getElementById("popBox").innerHTML = document.getElementById("id_zoomimage").innerHTML;		
			document.getElementById("popCover").style.display = "";
			document.getElementById("popBox").style.display = "";
		}
		
		function fnSwapZoomImage(imgSrc) {
			document.getElementById('id_img_zoom').src = imgSrc;
		}
		
	</script>
<script type="text/javascript">
	window.WEDATA.pageType = 'product';
	window.WEDATA.productData = {
		manufacturer: 'Samsung',
		partNumber: 'TRI-SAM-SGS5-KR17',
		modelId: 1674,
		modelName: 'Galaxy S5',
		itemId: 320857,
		typeId: 3,
		categoryName: 'Faceplates'
	};

</script>



        </td>
    </tr>
	
    <tr>
    	<td id="commentCard" style="padding:20px 0px 40px 0px; width:100%;" align="center">
	        
<form id="frmComment" name="frmComment" style="margin:0px; padding:0px;">
<div style="width:700px; height:300px; border-top:2px solid #5871a7;">
    <div style="width:658px; height:298px; background-color:#ebebeb; border:1px solid #999; padding:10px 20px 0px 20px;">
        <div style="font-size:20px; font-weight:bold; color:#3C599B; text-align:left;">Comment Card</div>
        <div style="font-size:12px; color:#444; padding-top:10px; text-align:left;">
            We want to hear from you! We work hard to make sure our site offers the best shopping experience possible. 
            If you have any suggestions on how we can make things even better - whatever it is, please drop us a note!
        </div>
        <div style="height:30px; margin-top:20px;">
        	<div style="float:left; font-size:15px; font-weight:bold; color:#3C599B; width:150px;">Email (optional):</div>
            <div style="float:left; padding-left:10px;"><input type="text" name="email" value="" size="32" /></div>
        </div>
        <div style="height:30px; margin-top:10px;">
        	<div style="float:left; font-size:15px; font-weight:bold; color:#3C599B; width:150px;">Phone (optional):</div>
            <div style="float:left; padding-left:10px;"><input type="text" name="phone" value="" size="32" /></div>
        </div>
        <div style="margin-top:10px; float:right; width:650px; text-align:right;"><TEXTAREA rows="3" cols="65" style="width:490px;" id="textarea1" name="textarea1" onfocus="clearDefaultTxt(this)">Add a comment...</TEXTAREA></div>
        <div style="float:right; margin-top:10px;"><img src="/images/buttons/commentSubmit.png" alt="" onclick="return checkall();" border="0" width="115" height="23" style="cursor:pointer;" /></div>
    </div>
</div>
</form>

<script language="javascript">
	
	var commentLoop = 0
	function clearDefaultTxt(curField) {
		if (curField.value == "Add a comment...") {
			curField.value = ""
		}
	}
	function ajax_cb(newURL,rLoc) {
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						alert("No data available")
					}
					else if (rVal == "refresh") {
						curLoc = window.location
						window.location = curLoc
					}
					else {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				}
				else {
					document.getElementById("testZone").innerHTML = newURL
				}
			}
		};
		httpRequest.send(null);
	}
	
	function checkall() {	 
		var GreForm = this.document.frmComment;
		
		if (GreForm.textarea1.value=="" || GreForm.textarea1.value=="Add a comment...") {
			alert("Enter Your Comments");
			GreForm.textarea1.focus();
			return false;
		}
		
		useURL = escape("/p-320857-samsung-galaxy-s3-anti-glare-screen-protector-film.html")
		ajax_cb('/ajax/saveCommentCard.asp?page=' + useURL + '&email=' + document.frmComment.email.value + '&phone=' + document.frmComment.phone.value + '&comment=' + escape(document.frmComment.textarea1.value),'dumpZone')
		var commentValue = GreForm.textarea1.value
		GreForm.textarea1.value = ""
		
		setTimeout("chkCommentReturn()",500)
		
		return true;
	}
	
	function chkCommentReturn() {
		commentLoop++
		if (commentLoop < 5) {
			if (document.getElementById("dumpZone").innerHTML != "") {
				if (document.getElementById("dumpZone").innerHTML == "commentGood") {
					alert("Comment saved")
				}
				else {
					alert("Error saving comment\nPlease resubmit\n" + document.getElementById("dumpZone").innerHTML)
					document.getElementById('textarea1').value = commentValue
				}
			}
		}
		else {
			alert("Error saving comment\nPlease resubmit")
			document.getElementById('textarea1').value = commentValue
		}
	}
</script>
        </td>
    </tr>
	
</table>

<table cellpadding="0" cellspacing="0" width="100%" align="center" id="siteBottom">
	<tr>
    	<td width="100%" align="center" style="background-color:#222;">
			<div id="footer_darkBand" style="color:#fff; width:1020px; text-align:left;">
            	<div style="float:left; padding:5px 0px 5px 0px;">
                    <div style="float:left;"><img src="/images/template/newsletter-icon.jpg" border="0" /></div>
                    <div style="float:left; color:#fff; font-size:16px; font-weight:bold; padding:7px 0px 0px 5px;">JOIN OUR NEWSLETTER</div>
                    <div style="float:left; margin:3px 0px 0px 20px;">
                        <form name="newsletter2" method="post" onsubmit="addNewsletter(document.newsletter2.semail.value,'Bottom Widget');return(false);">
                        <div style="float:left; width:160px; margin-top:1px; padding:6px 0px 0px 3px; border-radius:5px; background-color:#fff; height:20px;"><input id="newsEmail" name="semail" type="text" class="inputbox" style="width:150px; border:none;" value="Email Address" onclick="this.value='';"></div>
                        <div style="float:left; margin-left:5px;"><input type="image" src="/images/template/newsletter-signup.jpg" border="0" alt="Go!" onclick="addNewsletter(document.newsletter2.semail.value,'Bottom Widget');" /></div>
                        </form>                    
                    </div>
                </div>
            	<div style="float:right; padding-top:5px;">
                    <div style="float:left; color:#fff; font-size:16px; font-weight:bold; padding:7px 10px 0px 0px;">FOLLOW US ON</div>
                	<div style="float:left; padding-right:3px;">
						<a href="//plus.google.com/106042856978446012087?prsrc=3" rel="publisher" target="_blank" style="text-decoration:none;">
							<img src="//ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
						</a>
                    </div>
                    <div style="float:left; padding-right:3px;"><a href="http://www.facebook.com/cellularoutfitter" target="_blank" title="CellularOutfitter Facebook"><img src="/images/template/sm-facebook-icon.jpg" border="0" alt="CellularOutfitter Facebook" /></a></div>
                    <div style="float:left; padding-right:3px;"><a href="http://twitter.com/celloutfitter" target="_blank" title="CellularOutfitter Twitter"><img src="/images/template/sm-twitter-icon.jpg" border="0" alt="CellularOutfitter Twitter" /></a></div>
                    <!--<div style="float:left; padding-right:3px;"><a href="http://www.cellularoutfitter.com/blog/" target="_blank" title="CellularOutfitter Blog"><img src="/images/template/sm-blog-icon.jpg" border="0" alt="CellularOutfitter Blog" /></a></div>-->
                </div>
            </div>
        </td>
    </tr>
	<tr>
    	<td width="100%" align="center" style="background-color:#444; padding:20px 0px 20px 0px;">
			<div id="footer_grayBox" style="color:#fff; width:1020px; text-align:left;">
            	<div style="float:left; width:250px;">
                	<div style="float:left; width:100%; color:#ccc; font-size:16px; font-weight:bold; border-bottom:2px solid #ccc; padding-bottom:2px; margin-bottom:10px;">COMPANY INFORMATION</div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/" class="footerLink" title="Cellularoutfitter.com">Home</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/about-us.html" class="footerLink" title="about us">About Us</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/shipping-policy.html" class="footerLink" title="shipping & store policy">Shipping & Store Policy</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/track-your-order.html" class="footerLink" title="track your order">Order Status</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/faq.html" class="footerLink" title="faq">FAQ</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/contact-us.html" class="footerLink" title="contact us">Contact Us</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="https://www.pepperjamnetwork.com/affiliate/registration.php?refid=17199" class="footerLink" title="affiliate program">Affiliate Program</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/site-map.html" style="color:#ccc; font-size:12px;" title="site map">Site Map</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/termsofuse.html" class="footerLink" title="terms of use">Terms Of Use</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/privacypolicy.html" class="footerLink" title="privacy policy">Privacy Policy</a></div>
                	<!--<div style="float:left; width:100%; margin-bottom:3px;"><a href="/blog/" class="footerLink" title="blog">Blog</a></div>-->
                </div>
            	<div style="float:left; width:450px; margin-left:35px;">
                	<div style="float:left; width:100%; color:#ccc; font-size:16px; font-weight:bold; border-bottom:2px solid #ccc; padding-bottom:2px; margin-bottom:10px;">BRANDS</div>
                	<div style="float:left; width:225px;">
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-17-apple-cell-phone-accessories.html" class="footerLink" title="Apple Accessories">Apple Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-14-blackberry-cell-phone-accessories.html" class="footerLink" title="Blackberry Accessories">Blackberry Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-20-htc-cell-phone-accessories.html" class="footerLink" title="HTC Accessories">HTC Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-29-huawei-cell-phone-accessories.html" class="footerLink" title="Huawei Accessories">Huawei Accessories</a></div>                        
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-4-lg-cell-phone-accessories.html" class="footerLink" title="LG Accessories">LG Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-6-nextel-cell-phone-accessories.html" class="footerLink" title="Nextel Accessories">Nextel Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-7-nokia-cell-phone-accessories.html" class="footerLink" title="Nokia Accessories">Nokia Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-16-palm-cell-phone-accessories.html" class="footerLink" title="Palm Accessories">Palm Accessories</a></div>
                    </div>
                	<div style="float:left; width:225px;">
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-18-pantech-cell-phone-accessories.html" class="footerLink" title="Pantech Accessories">Pantech Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-9-samsung-cell-phone-accessories.html" class="footerLink" title="Samsung Accessories">Samsung Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-10-sanyo-cell-phone-accessories.html" class="footerLink" title="Sanyo Accessories">Sanyo Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-19-t-mobile-sidekick-cell-phone-accessories.html" class="footerLink" title="Sidekick Accessories">Sidekick Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-11-siemens-cell-phone-accessories.html" class="footerLink" title="Siemens Accessories">Siemens Accessories</a></div>                        
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-2-sony-ericsson-cell-phone-accessories.html" class="footerLink" title="Sony Ericsson Accessories">Sony Ericsson Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-30-zte-cell-phone-accessories.html" class="footerLink" title="ZTE Accessories">ZTE Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/samsung-galaxy-accessories.html" class="footerLink" title="Samsung Galaxy Accessories">Samsung Galaxy Accessories</a></div>
                    </div>
                </div>
            	<div style="float:left; width:250px; margin-left:35px;">
                	<div style="float:left; width:100%; color:#ccc; font-size:16px; font-weight:bold; border-bottom:2px solid #ccc; padding-bottom:2px; margin-bottom:10px;">PAYMENT OPTIONS</div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><img src="/images/template/payments-icons2.jpg" border="0" /></div>
                </div>
                <div style="display:none;"><a href="https://plus.google.com/106042856978446012087?rel=author" target="_blank" style="display:none;" rel="me">&nbsp;</a></div>
                <div style="float:left; width:100%; padding:10px 0px 10px 0px; color:#ccc; text-align:center; font-size:12px;">&copy;2014 All rights reserved. CellularOutfitter.com</div>
            </div>
            <div id="footer_grayBox_b" class="footerLowerLinks">
            	<div class="tb">
                    <div class="fl footerRow1">
                        <div class="tb footerTitle">COMPANY INFO</div>
                        <div class="tb footerLinkBox"><a href="/about-us.html" class="footerLink" title="about us">About Us</a></div>
                        <div class="tb footerLinkBox"><a href="/contact-us.html" class="footerLink" title="contact us">Contact Us</a></div>
                        <div class="tb footerLinkBox"><a href="https://www.pepperjamnetwork.com/affiliate/registration.php?refid=17199" class="footerLink" title="affiliate program">Affiliate Program</a></div>
                        <div class="tb footerTitleBottom">SERVICE &amp; SUPPORT</div>
                        <div class="tb footerLinkBox"><a href="/track-your-order.html" class="footerLink" title="track your order">Order Status</a></div>
                        <div class="tb footerLinkBox"><a href="/shipping-policy.html" class="footerLink" title="shipping & store policy">Shipping & Store Policy</a></div>
                        <div class="tb footerLinkBox"><a href="/faq.html" class="footerLink" title="faq">FAQ</a></div>
                    </div>
                    <div class="fl footerRow2">
                        <div class="tb footerTitle">SHOP BY PHONE BRAND</div>
                        <div class="fl brandLinks">
                            <div class="tb footerLinkBox"><a href="/b-17-apple-cell-phone-accessories.html" class="footerLink" title="Apple Accessories">Apple Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-14-blackberry-cell-phone-accessories.html" class="footerLink" title="Blackberry Accessories">Blackberry Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-20-htc-cell-phone-accessories.html" class="footerLink" title="HTC Accessories">HTC Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-29-huawei-cell-phone-accessories.html" class="footerLink" title="Huawei Accessories">Huawei Accessories</a></div>                        
                            <div class="tb footerLinkBox"><a href="/b-4-lg-cell-phone-accessories.html" class="footerLink" title="LG Accessories">LG Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-6-nextel-cell-phone-accessories.html" class="footerLink" title="Nextel Accessories">Nextel Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-7-nokia-cell-phone-accessories.html" class="footerLink" title="Nokia Accessories">Nokia Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-16-palm-cell-phone-accessories.html" class="footerLink" title="Palm Accessories">Palm Accessories</a></div>
                        </div>
                        <div class="fl brandLinks">
                            <div class="tb footerLinkBox"><a href="/b-18-pantech-cell-phone-accessories.html" class="footerLink" title="Pantech Accessories">Pantech Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-9-samsung-cell-phone-accessories.html" class="footerLink" title="Samsung Accessories">Samsung Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-10-sanyo-cell-phone-accessories.html" class="footerLink" title="Sanyo Accessories">Sanyo Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-19-t-mobile-sidekick-cell-phone-accessories.html" class="footerLink" title="Sidekick Accessories">Sidekick Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-11-siemens-cell-phone-accessories.html" class="footerLink" title="Siemens Accessories">Siemens Accessories</a></div>                        
                            <div class="tb footerLinkBox"><a href="/b-2-sony-ericsson-cell-phone-accessories.html" class="footerLink" title="Sony Ericsson Accessories">Sony Ericsson Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-30-zte-cell-phone-accessories.html" class="footerLink" title="ZTE Accessories">ZTE Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/samsung-galaxy-accessories.html" class="footerLink" title="Samsung Galaxy Accessories">Samsung Galaxy Accessories</a></div>
                        </div>
                    </div>
                    <div class="fl footerRow3">
                        <div class="tb footerTitle">PAYMENT OPTIONS</div>
                        <div class="tb paymentOptionsB"></div>
                        <div class="tb footerTitleBottomLock">SHOP WITH CONFIDENCE</div>
                        <div class="tb">
                            <div class="fl confidence1">
                                <div class="tb" style="padding-top:20px;"><a target="_blank" href="https://www.mcafeesecure.com/RatingVerify?ref=www.cellularoutfitter.com"><img width="115" height="32" border="0" src="//images.scanalert.com/meter/www.cellularoutfitter.com/12.gif" alt="McAfee SECURE sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="alert('Copying Prohibited by Law - McAfee SECURE is a Trademark of McAfee, Inc.'); return false;"></a></div>
                            </div>
                            <div class="fl confidence2">
                                <a href="javascript:Open_Popup('http://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.CELLULAROUTFITTER.COM&lang=en','VeriSign','width=560,height=500,location=yes,status=yes,resizable=yes,scrollbars=yes');"><div id="verisignLogo" style="margin:20px auto;" title="CELLULAR OUTFITTER, INC. has been verified by Verisign"></div></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tb veryBottomLinks">
                	<div class="fl americanFlag"></div>
                    <div class="fl copywriteTxt">&copy;2013 CELLULAROUTFITTER.COM. ALL RIGHTS RESERVED.</div>
                    <div class="fl vbl_a"><a href="/termsofuse.html" class="footerLink" title="terms of use">Terms Of Use</a></div>
                    <div class="fl vbl_a"><a href="/privacypolicy.html" class="footerLink" title="privacy policy">Privacy Policy</a></div>
                    <div class="fl vbl_b"><a href="/site-map.html" style="color:#ccc; font-size:12px;" title="site map">Site Map</a></div>
                </div>
            </div>
        </td>
	</tr>
</table>
<link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
<div id="promoDetailBox" style="display:none;" onclick="closeReviewPopup()">
	<div id="promoPop1" class="tb containerA">
		<div class="tb promoBoxMain">
			<div class="tb closeWinRow">
				<div class="fr closeWin" onclick="closeReviewPopup()"></div>
			</div>
			<div class="promoTitle">LIMITED TIME OFFER!</div>
			<div class="promoDetails">
				Place an order today and receive a <strong>FREE GIFT</strong>. We are giving away a Ballpoint Pen &amp; Stylus Combo (valued at $7.99) 
				with each order until we run out. This is a limited time offer, so order today to get your free gift. Supplies are 
				running out fast!
			</div>
			<div class="tb promoBottom">
				<div class="fl itemPic"></div>
				<div class="fl promoPoints">
					<div class="tb promoPointRow">
						<div class="fl promoCheckmark"></div>
						<div class="fl promoPointTxt">The ultimate in convenience and versatility - a pen AND stylus in one!</div>
					</div>
					<div class="tb promoPointRow">
						<div class="fl promoCheckmark"></div>
						<div class="fl promoPointTxt">Perfect for navigating on touch screen devices of all shapes and sizes.</div>
					</div>
					<div class="tb promoPointRow">
						<div class="fl promoCheckmark"></div>
						<div class="fl promoPointTxt">Sleek and super smooth twisting barrel with black ink.</div>
					</div>
					<div class="promoCTA" onclick="closeReviewPopup()"></div>
					<div class="promoRestrictions">* LIMIT ONE PER HOUSEHOLD WHILE SUPPLIES LAST.</div>
				</div>
			</div>
		</div>
	</div>
    <div id="promoPop2" class="tb containerA">
		<div class="tb promoBoxMain2">
			<div class="tb closeWinRow">
				<div class="fr closeWin" onclick="closeReviewPopup()"></div>
			</div>
			<div class="promoTitle">LIMITED TIME OFFER!</div>
			<div class="promoDetails2">
				<span class="promoDetails2Ex1">Place an order today and receive a <span class="promoDetails2Small">FREE GIFT</span>. We are giving away one Non-Slip Car</span><br />
                <span class="promoDetails2Ex2">Pad (valued at $4) with every order, but only while supplies last. This is a limited time</span><br />
                <span class="promoDetails2Ex3">offer, so order now to receive yours before they run out!</span>
			</div>
			<div class="tb promoBottom">
				<div class="fl itemPic2"></div>
				<div class="fl promoPoints2">
					<div class="tb promoPointRow2">
						<div class="fl promoCheckmark"></div>
						<div class="fl promoPointTxt">Keeps your cell phone and possessions<br />safe and within reach at all times</div>
					</div>
					<div class="tb promoPointRow2">
						<div class="fl promoCheckmark"></div>
						<div class="fl promoPointTxt">Attach to your vehicle's dashboard,<br />windows, or virtually any smooth surface!</div>
					</div>
					<div class="tb promoPointRow2">
						<div class="fl promoCheckmarkSL"></div>
						<div class="fl promoPointTxt">Non-magnetic, anti-slip technology</div>
					</div>
					<div class="promoCTA2" onclick="closeReviewPopup()"></div>
					<div class="promoRestrictions">* LIMIT ONE PER HOUSEHOLD WHILE SUPPLIES LAST.</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="testZone"></div>
<div id="dumpZone"></div>

<script type="text/javascript">
	if (typeof StrandsTrack=="undefined"){ StrandsTrack=[]; }
	StrandsTrack.push({event:"visited", item: "320857"});  
</script>

<script type="text/javascript">
	if (typeof StrandsTrack=="undefined"){ StrandsTrack=[]; }
	StrandsTrack.push({event:"userlogged", user: "956628179"});
</script>

<!-- Strands Library -->
<script type="text/javascript" src="//bizsolutions.strands.com/sbsstatic/js/sbsLib-1.0.min.js"></script>
<script type="text/javascript">
	try{
		SBS.Worker.go("Kx7AmRMCrW");
	}
	catch (e){};
</script>
<!-- Google Code for Main Tag 12-14-2011 -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1061947743;
var google_conversion_label = "HcUxCM2j6gMQ35Kw-gM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1061947743/?value=0&label=HcUxCM2j6gMQ35Kw-gM&guid=ON&script=0"/>
</div>
</noscript>


<script type="text/javascript">
var google_tag_params = {
ecomm_prodid: '320857', 
ecomm_pagetype: '1674',
ecomm_totalvalue: ''
};
</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = "XXXXXXXXXX";
var google_conversion_label = "YYYYYYYYYY";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/XXXXXXXXXX/?value=0&label=YYYYYYYYYY&guid=ON&script=0"/>
</div>
</noscript>

<!-- Nextopia Code Auto Complete -->
<script type="text/javascript" src="//nxtcfm.s3.amazonaws.com/9b1c36137711f5324260883e42567b60-ac.js"></script>
<!-- End Nextopia Auto Complete -->

<!-- Google Code for Remarketing Tag 3-3-2014 -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
var google_tag_params = {
ecomm_prodid: '320857',
ecomm_pagetype: '1674',
ecomm_totalvalue: '',
};
</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1061947743;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1061947743/?value=0&guid=ON&script=0"/>
</div>
</noscript>



<script type="text/javascript">
if(typeof _satellite != 'undefined'){
	_satellite.pageBottom();
}
</script>


		<!-- BloomSurface tracking code.  Place at foot of page. -->
		<script type="text/javascript">
			var br_data = {};
			br_data.acct_id = "5160";
			br_data.ptype = "product";
			br_data.cat_id = "3";
			br_data.cat = "Samsung|Galaxy S5|Faceplates";
			br_data.prod_id = "320857";
			br_data.prod_name = "Samsung Galaxy S5 Trident Kraken A.M.S. Case w/Built-in Screen Protector (Blue/Black)";
			br_data.pstatus= "ok";
			br_data.sku = "320857";
			br_data.price = "37.99";
			br_data.search_term = "";
			br_data.sale_price = "";
			br_data.is_conversion = "0";
			br_data.basket_value = "";
			br_data.order_id ="";
			br_data.basket = {
				'items': []
			};
	
			(function() {
				var brtrk = document.createElement('script');
				brtrk.type = 'text/javascript';
				brtrk.async = true;
				brtrk.src = 'https:' == document.location.protocol ? "https://cdns.brsrvr.com/v1/br-trk-5160.js" : "http://cdn.brcdn.com/v1/br-trk-5160.js";
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(brtrk, s);
			})();
	</script>

	<script type="text/javascript">
		try {
			Granify.trackPageView({
				page_type: "product"
			});
		}
		catch (e) {}	
    </script>

    
<script language="javascript" src="/frameWork/Utility/base.js"></script>
<script>
	function addNewsletter(semail,loc) {
		_gaq.push(['_trackEvent', 'Email Signup', 'Submission', 'Desktop', loc]);
		if(typeof(_vis_opt_top_initialize) == "function") {
			// Code for Custom Goal: Email Sign-Up
			 _vis_opt_goal_conversion(204);
			// uncomment the following line to introduce a half second pause to ensure goal always registers with the server
			// _vis_opt_pause(500);
		}
		ajax('/ajax/newsletter.asp?semail=' + semail + '&brandID=9&modelID=1674&typeID=3&itemID=320857','popBox');
		document.getElementById("popBox").style.position = "absolute";
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
		jQuery(window).trigger('newsletterSignup', [loc, semail]);
		return false;
	}
</script>
<script type="text/javascript" src="/includes/js/jsCombo_index.js?v=20140110"></script>
<script type="text/javascript" src="/includes/js/km.js"></script>
<script language="javascript">
	//noAA:0
	//sip: 192.168.114.3
	function closeReviewPopup() {
		document.getElementById('popCover').style.display = 'none';
		document.getElementById('popBox').style.display = 'none';
		document.getElementById('popBox').innerHTML = '';
	}
	
	function showPromoDetails() {
		document.getElementById('promoZone').innerHTML = "";
		ajax('/ajax/showPromoPopup.asp','promoZone');
		checkPromo('no');
	}
	
	function checkPromo(force)
	{
		if (force == "yes") {
			document.getElementById('popBox').innerHTML = document.getElementById('promoDetailBox').innerHTML;
			document.getElementById('popCover').style.display = '';
			document.getElementById('popBox').style.display = '';
		}
		else {
			if ("" == document.getElementById('promoZone').innerHTML) setTimeout('checkPromo()', 50);
			else if ("0" == document.getElementById('promoZone').innerHTML) {
				document.getElementById('popBox').innerHTML = document.getElementById('promoDetailBox').innerHTML;
				document.getElementById('popCover').style.display = '';
				document.getElementById('popBox').style.display = '';
			}
		}
	}

	
	window.onload = function() { showPromoDetails(); }
	
</script>
<div id="testZone"></div>
<div id="dumpZone" style="display:none;"></div>
<div id="promoZone" style="display:none;"></div>
</body>
</html>
<!--IsObject(objConn):False
IsObject(weUtil):False
-->
<script language="javascript">
	var useHttps = 0;
	
	function nreset() {
		document.newsletter.semail.value="";
	}
	function nvalidateEmail(email) {
		var splitted = email.match("^(.+)@(.+)$");
		if (splitted == null) return false;
		if (splitted[1] != null) {
			var regexp_user=/^\"?[\w-_\.]*\"?$/;
			if (splitted[1].match(regexp_user) == null) return false;
		}
		if (splitted[2] != null) {
			var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
			if (splitted[2].match(regexp_domain) == null) {
				var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
				if (splitted[2].match(regexp_ip) == null) return false;
			}
			return true;
		}
		return false;
	}
	function nvalidate() {
		if (document.newsletter.semail.value.length==0) {
			alert("please enter your email address");
			document.newsletter.semail.focus();
			return false;
		}
		if (!nvalidateEmail(document.newsletter.semail.value)) {
			alert("please enter your valid email address"); 
			document.newsletter.semail.focus();
			return false; 
		}
		else document.newsletter.submit()
		return true;
	}
</script>
<script language="javascript" src="/includes/js/sslCheck.js"></script>
<script>
	//ajax('/ajax/productDetails.asp?itemID=320857&modelID=1674&typeID=3&latDesign=1&strands=1','relatedItems_new')
	var itemID = 320857;
	var runSalesTimer = 1;
	
	if (runSalesTimer == 1) {
		setTimeout("changeSalesTime()",1000)
	}
	
	function strandsAddToCart(formName) {
		if (typeof StrandsTrack=="undefined"){ StrandsTrack=[]; }
		StrandsTrack.push({event:"addshoppingcart",item:itemID} );
		eval("document." + formName + ".submit()");
	}
	
	function changeSalesTime() {
		var saleHours = parseInt(document.getElementById("saleHours").innerHTML);
		var saleMinutes = parseInt(document.getElementById("saleMinutes").innerHTML);
		var saleSeconds = parseInt(document.getElementById("saleSeconds").innerHTML);
		
		if (saleSeconds > 0) {
			saleSeconds--
			if (saleSeconds < 10) {
				document.getElementById("saleSeconds").innerHTML = "0" + saleSeconds
			}
			else {
				document.getElementById("saleSeconds").innerHTML = saleSeconds
			}
			setTimeout("changeSalesTime()",1000)
		}
		else if (saleMinutes > 0) {
			saleMinutes--
			if (saleMinutes < 10) {
				document.getElementById("saleMinutes").innerHTML = "0" + saleMinutes
			}
			else {
				document.getElementById("saleMinutes").innerHTML = saleMinutes
			}
			document.getElementById("saleSeconds").innerHTML = 59
			setTimeout("changeSalesTime()",1000)
		}
		else if (saleHours > 0) {
			saleHours--
			if (saleHours < 10) {
				document.getElementById("saleHours").innerHTML = "0" + saleHours
			}
			else {
				document.getElementById("saleHours").innerHTML = saleHours
			}
			document.getElementById("saleMinutes").innerHTML = 59
			document.getElementById("saleSeconds").innerHTML = 59
			setTimeout("changeSalesTime()",1000)
		}
	}
	
	var	correctAnswer = false;
	function showReviewAll()
	{
		var objMoreReview = document.getElementById('tblMoreReview');
		var	objArrow = document.getElementById('id_img_reviewArrow');
		
		if ("none" == tblMoreReview.style.display)
		{
			objMoreReview.style.display = '';
			document.getElementById('a_showmore').innerHTML = "hide more..";
		}
		else 
		{
			objMoreReview.style.display = 'none';
			document.getElementById('a_showmore').innerHTML = "show more..";			
		}	
	}

	function showReviewPopup() {
		try {
			viewPortH = $(window).height(); //get browser's viewport height.
		}
		catch(err) {
			viewPortH = 700;
		}		
		ajax('/ajax/productreview.asp?screenH='+viewPortH+'&itemid=320857','popBox');
		document.getElementById('popCover').style.display = '';
		document.getElementById('popBox').style.display = '';
	}
	
	
	
	function rateReview(id) {
		for(i=1;i<6;i++) {
			if (i <= id)
				document.getElementById('reviewStar'+i).src='/images/product/review/star-full.png';
			else
				document.getElementById('reviewStar'+i).src='/images/product/review/star-empty.png';
		}
		
		document.getElementById('id_hidStar').value = id;

		switch(id)
		{
		case 1:
			document.getElementById('reviewStarDesc1').innerHTML = '1 Star';
			document.getElementById('reviewStarDesc2').innerHTML = 'Poor';
			break;
		case 2:
			document.getElementById('reviewStarDesc1').innerHTML = '2 Stars';
			document.getElementById('reviewStarDesc2').innerHTML = 'Fair';
			break;
		case 3:
			document.getElementById('reviewStarDesc1').innerHTML = '3 Stars';
			document.getElementById('reviewStarDesc2').innerHTML = 'Good';
			break;
		case 4:
			document.getElementById('reviewStarDesc1').innerHTML = '4 Stars';
			document.getElementById('reviewStarDesc2').innerHTML = 'Very Good';
			break;
		case 5:
			document.getElementById('reviewStarDesc1').innerHTML = '5 Stars';
			document.getElementById('reviewStarDesc2').innerHTML = 'Excellent';
			break;
		default:
			document.getElementById('reviewStarDesc1').innerHTML = '5 Stars';
			document.getElementById('reviewStarDesc2').innerHTML = 'Excellent';
			break;
		}
	}	
	
	function validateReview() {
		var frm = document.frmProductReview;
		
		frm.txtEmail.className = "";
		frm.txtReviewTitle.className = "";
		frm.txtReview.className = "";
		frm.txtNickname.className = "";
		document.getElementById('msgEmail').style.display = 'none';
		document.getElementById('msgReviewTitle').style.display = 'none';
		document.getElementById('msgReview').style.display = 'none';
		document.getElementById('msgNickname').style.display = 'none';
		
		var reviewerEmail = frm.txtEmail.value;
		var reviewerNickName = frm.txtNickname.value;
		var reviewerLocation = frm.txtLocation.value;
		var reviewerAge = frm.cbAge.value;
		var reviewerGender = frm.cbGender.value;
		var reviewerType = getSelectedRadioValue(frm.rdoJob);
		var orderID = 0;
		var itemID = 320857;
		var starRating = frm.hidStar.value;
		var ownershipPeriod = frm.cbOwnership.value;
		var reviewTitle = frm.txtReviewTitle.value;
		var reviewBody = frm.txtReview.value;
		var isRecommend = getSelectedRadioValue(frm.rdoRecommend);
		var otherPro = frm.txtPros.value;
		var otherCon = frm.txtCons.value;

		var isProPrice = 0;
		if (frm.chkPros1.checked) isProPrice = 1;
		var isProDesign = 0;
		if (frm.chkPros2.checked) isProDesign = 1;
		var isProWeight = 0;
		if (frm.chkPros3.checked) isProWeight = 1;
		var isProProtection = 0;
		if (frm.chkPros4.checked) isProProtection = 1;

		var isConFit = 0;
		if (frm.chkCons1.checked) isConFit = 1;
		var isConRobust = 0;
		if (frm.chkCons2.checked) isConRobust = 1;
		var isConMaintenance = 0;
		if (frm.chkCons3.checked) isConMaintenance = 1;
		var isConWeight = 0;
		if (frm.chkCons4.checked) isConWeight = 1;

		if (!validateEmail(reviewerEmail)) { 
			document.getElementById('msgEmail').style.display = '';
			frm.txtEmail.className = "alertField";
			frm.txtEmail.focus();
			return false;
		}

		if (reviewTitle == '') { 
			document.getElementById('msgReviewTitle').style.display = '';
			frm.txtReviewTitle.className = "alertField";
			frm.txtReviewTitle.focus();
			return false;
		}

		if (reviewBody == '') { 
			document.getElementById('msgReview').style.display = '';
			frm.txtReview.className = "alertField";
			frm.txtReview.focus();
			return false;
		}

		if (reviewerNickName == '') { 
			document.getElementById('msgNickname').style.display = '';
			frm.txtNickname.className = "alertField";
			frm.txtNickname.focus();
			return false;
		}

		var updateURL	=	"/ajax/productReviewUpdate.asp";
		var updateParam	=	"siteID=2";
		updateParam	=	updateParam + "&reviewerEmail=" + reviewerEmail;
		updateParam	=	updateParam + "&reviewerNickName=" + escape(reviewerNickName);
		updateParam	=	updateParam + "&reviewerLocation=" + escape(reviewerLocation);
		updateParam	=	updateParam + "&reviewerAge=" + reviewerAge;
		updateParam	=	updateParam + "&reviewerGender=" + reviewerGender;
		updateParam	=	updateParam + "&reviewerType=" + reviewerType;
		updateParam	=	updateParam + "&orderID=" + orderID;
		updateParam	=	updateParam + "&itemID=" + itemID;
		updateParam	=	updateParam + "&starRating=" + starRating;
		updateParam	=	updateParam + "&ownershipPeriod=" + ownershipPeriod;
		updateParam	=	updateParam + "&reviewTitle=" + escape(reviewTitle);
		updateParam	=	updateParam + "&reviewBody=" + escape(reviewBody);
		updateParam	=	updateParam + "&isRecommend=" + isRecommend;
		updateParam	=	updateParam + "&isProPrice=" + isProPrice;
		updateParam	=	updateParam + "&isProDesign=" + isProDesign;
		updateParam	=	updateParam + "&isProWeight=" + isProWeight;
		updateParam	=	updateParam + "&isProProtection=" + isProProtection;
		updateParam	=	updateParam + "&otherPro=" + escape(otherPro);
		updateParam	=	updateParam + "&isConFit=" + isConFit;
		updateParam	=	updateParam + "&isConRobust=" + isConRobust;
		updateParam	=	updateParam + "&isConMaintenance=" + isConMaintenance;
		updateParam	=	updateParam + "&isConWeight=" + isConWeight;
		updateParam	=	updateParam + "&otherCon=" + escape(otherCon);

		ajaxPost(updateURL, updateParam, 'popBox');
	}
	
	var strThumbs = ",";
	function updateThumbs(reviewID,isThumbsUp,curCount)	{
		if ((isThumbsUp == 1)&&(strThumbs.search(","+reviewID+",") == -1)) {
			strThumbs = strThumbs + reviewID + ",";
			ajax('/ajax/productReviewThumbs.asp?reviewID='+reviewID+'&isThumbsUp='+isThumbsUp,'dumpZone');
			document.getElementById('thumbsUpCount'+reviewID).innerHTML = parseInt(curCount) + 1;
		}
		
		if ((isThumbsUp == 0)&&(strThumbs.search(","+reviewID+",") == -1)) {
			strThumbs = strThumbs + reviewID + ",";
			ajax('/ajax/productReviewThumbs.asp?reviewID='+reviewID+'&isThumbsUp='+isThumbsUp,'dumpZone');
			document.getElementById('thumbsDownCount'+reviewID).innerHTML = parseInt(curCount) + 1;
		}
	}
	
	function onReviewSort(strSort) {
		ajax('/ajax/productReviewSort.asp?itemid=320857&strSort='+strSort+'&canonicalURL=http%3A%2F%2Fstaging%2Ecellularoutfitter%2Ecom%2Fp%2D320857%2Dsamsung%2Dgalaxy%2Ds5%2Dtrident%2Dkraken%2Da%2Dm%2Ds%2Dcase%2Dw%2Dbuilt%2Din%2Dscreen%2Dprotector%2Dblue%2Dblack%2D%2Ehtml','reviewBody');
	}
	
	//setTimeout("ajax('/ajax/livePurch.asp','livePurchaseDiv')",1000)
</script>
<script language="javascript" src="/includes/js/productPage.js?v=20131003"></script>
<!-- Begin Curebit product integration code -->
  <script type="text/javascript" src="http://www.curebit.com/javascripts/api/all-0.2.js"></script>
  <script type="text/javascript">

    curebit.init({site_id: 'cellular-outfitter'});

    var products = [{
          url:	'http://www.cellularoutfitter.com/p-320857-samsung-galaxy-s3-anti-glare-screen-protector-film.html', /*REQUIRED VARIABLE */
          
		  image_url: 'http://www.cellularoutfitter.com/productPics/big/samsung-galaxy-s5-trident-kraken-a-m-s-case-w-built-in-screen-protector-blue-black-21729.jpg', /* REQUIRED VARIABLE */
		  
          title:	'Samsung Galaxy S5 Trident Kraken A.M.S. Case w/Built-in Screen Protector (Blue/Black)', /* REQUIRED VARIABLE */
          product_id: 'CO321107', /* REQUIRED VARIABLE */
          price:	'37.99' /* OPTIONAL VARIABLE */
      },
      {
          url:	'http://www.cellularoutfitter.com/p-320857-samsung-galaxy-s3-anti-glare-screen-protector-film.html', /*REQUIRED VARIABLE */
          
		  image_url: 'http://www.cellularoutfitter.com/productPics/big/samsung-galaxy-s5-trident-kraken-a-m-s-case-w-built-in-screen-protector-blue-black-21729.jpg', /* REQUIRED VARIABLE */
		  
          title:	'Samsung Galaxy S5 Trident Kraken A.M.S. Case w/Built-in Screen Protector (Blue/Black)', /* REQUIRED VARIABLE */
          product_id: 'CO321107', /* REQUIRED VARIABLE */
          price:	'37.99' /* OPTIONAL VARIABLE */
      }];

    curebit.register_products(products);

  </script>

<!-- End Curebit product integration code -->
<script language="javascript">
	var myPartNumber = 'TRI-SAM-SGS5-KR17'
	var modelID = 1674
	var typeID = 3
</script>
<!-- mp -->