<%
set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getStagingConn()

sql	=	"exec [getShipConfirmTest] '2/7/2014', '2/7/2014'"
set objRsOrders = oConn.execute(sql)

strTemplate = readTextFile("C:\inetpub\wwwroot\wirelessemporium.com\www\admin\scheduled_tasks\shipment_confirm_email_template.html")
dup_strTemplate = strTemplate

lastOrderID 	= 	-1
curOrderID		=	-1
strOrderDetails	=	""
numItem			=	0

if objRsOrders.state = 1 then
	'recordset loop(by store)
	do until objRsOrders is nothing
		do until objRsOrders.eof
			curOrderID	=	objRsOrders("orderid")
			site_id		=	objRsOrders("site_id")
			if lastOrderID <> curOrderID then
				numItem = 0
				if strOrderDetails <> "" then
					strOrderDetails	=	strOrderDetails	&	"			        	</table>"
					strTemplate = replace(strTemplate, "[LOGO IMG LINK]", logoLink)
					strTemplate = replace(strTemplate, "[HEADER TEXT]", headerText)
					strTemplate = replace(strTemplate, "[CUSTOMER NAME]", custName)
					strTemplate = replace(strTemplate, "[TOP TEXT]", topText)
					strTemplate = replace(strTemplate, "[SHIPPED TO]", strShipAddress)
					strTemplate = replace(strTemplate, "[ORDER ID]", lastOrderID)					
					strTemplate = replace(strTemplate, "[ORDER DETAILS]", strOrderDetails)
					strTemplate = replace(strTemplate, "[ORDER SUBTOTAL]", formatcurrency(orderSubTotalAmt))
					if site_id = 0 then
						if shoprunnerid <> "" then
							strTemplate = replace(strTemplate, "[SR]", "Thanks for using FREE 2-Day Shipping by ShopRunner! For returns, visit https://www.shoprunner.com/returns to print your pre-paid return label. Don't forget, invite a Friend to join.")
						else
							strTemplate = replace(strTemplate, "[SR]", "Next time get FREE 2-Day Shipping from Wireless Emporium and other great retailers with your ShopRunner membership. Visit https://www.wirelessemporium.com/shoprunner to sign up for a FREE 30-Day Trial")
	'						strTemplate = replace(strTemplate, "[SR]", "")
						end if
					else
						strTemplate = replace(strTemplate, "[SR]", "")
					end if
					
					if promoCode <> "" then
						strTemp = 	"            	<tr>" & vbcrlf & _
									"					<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>PROMO CODE:</b></td>" & vbcrlf & _
									"					<td width=""80%"" align=""left"" valign=""top"">" & promoCode & "</td>" & vbcrlf & _
									"				</tr>" & vbcrlf
									
						strTemplate = replace(strTemplate, "[PROMO CODE]", strTemp)
					else
						strTemplate = replace(strTemplate, "[PROMO CODE]", "")					
					end if

					if discountAmt > 0 then
						strTemp = 	"				<tr>" & vbcrlf & _
									"					<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>DISCOUNT:</b></td>" & vbcrlf & _
									"					<td width=""80%"" align=""left"" valign=""top"">(" & formatcurrency(discountAmt) & ")</td>" & vbcrlf & _
									"				</tr>" & vbcrlf					
						strTemplate = replace(strTemplate, "[DISCOUNT]", strTemp)
					else
						strTemplate = replace(strTemplate, "[DISCOUNT]", "")
					end if

					if buySafeAmt > 0 then
						strTemp = 	"				<tr>" & vbcrlf & _
									"					<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>buySAFE Bond:</b></td>" & vbcrlf & _
									"					<td width=""80%"" align=""left"" valign=""top"">" & formatcurrency(buySafeAmt) & "</td>" & vbcrlf & _
									"				</tr>" & vbcrlf					
						strTemplate = replace(strTemplate, "[BUY SAFE]", strTemp)
					else
						strTemplate = replace(strTemplate, "[BUY SAFE]", "")
					end if		
					
					strTemplate = replace(strTemplate, "[TAX AMOUNT]", formatcurrency(taxAmt))
					strTemplate = replace(strTemplate, "[SHIPPING FEE]", formatcurrency(shippingFeeAmt))
					if shoprunnerid <> "" then
						strTemplate = replace(strTemplate, "[SHIPPING TYPE]", "ShopRunner, 2-Day Shipping - FREE")
						strTemplate = replace(strTemplate, "[TR TRACKING]", "")
					else
						if shipMethod = "First Class" then
							strTemplate = replace(strTemplate, "[SHIPPING TYPE]", shipMethod & " (3-10 Business Days)")
						else 
							strTemplate = replace(strTemplate, "[SHIPPING TYPE]", shipMethod)
						end if
						if shippedBy = "UPS-MI"	then
							strMemo = "(Tracking may take 24-48 hours to appear after your mail piece has shipped.)"
						else
							strMemo = ""
						end if
						strTracking = 	"            	<tr>" & vbcrlf & _
										"               	<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>SHIPMENT TRACKING:</b></td>" & vbcrlf & _
										"					<td width=""80%"" align=""left"" valign=""top""><a href=""" & trackLink & """ target=""_blank""><b>Click Here</b></a>&nbsp;" & strMemo & "</td>" & vbcrlf & _
										"				</tr>"
						strTemplate = replace(strTemplate, "[TR TRACKING]", strTracking)
					end if
					strTemplate = replace(strTemplate, "[GRAND TOTAL]", formatcurrency(orderGrandTotalAmt))
					strTemplate = replace(strTemplate, "[STORE NAME]", storeName)

					'send an e-mail
'					call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "")
					response.write strTemplate & "<br><br>"
				
					strTemplate 	= 	dup_strTemplate	'refresh the template for an another e-mail.				
				end if

				strOrderDetails	=	"			        	<table width=""100%"" border=""1"" cellspacing=""1"" cellpadding=""5"" style=""font-family:Arial; font-size:11px; border-collapse:collapse;"">" & vbcrlf & _
									"			        		<tr>" & vbcrlf & _
									"								<td align=""center"" valign=""top"" width=""70%"" bgcolor=""#ededed""><b>Item Description</b></td>" & vbcrlf & _
									"								<td align=""center"" valign=""top"" width=""10%"" bgcolor=""#ededed""><b>Quantity</b></td>" & vbcrlf & _
									"								<td align=""center"" valign=""top"" width=""10%"" bgcolor=""#ededed""><b>Price</b></td>" & vbcrlf & _
									"								<td align=""center"" valign=""top"" width=""10%"" bgcolor=""#ededed""><b>SubTotal</b></td>" & vbcrlf & _
									"							</tr>" & vbcrlf				
			end if
			
			'========================================== SET REPLACEMENT TEXTS ================================================
			lastOrderID 		= 	curOrderID
			fname				=	objRsOrders("fname")
			if isnull(fname) or len(fname) <= 0 then
				custName			=	"Customer"
			else
				custName			=	ucase(left(fname, 1)) & lcase(right(fname,len(fname)-1))
			end if
			email				=	objRsOrders("email")
			cdo_to				=	email
			
			storeName			=	objRsOrders("longDesc")
			topText				=	"Thank you for ordering from " & storeName & ".com! We would like to notify you that your order has been SHIPPED to:"			
			trackLink			=	objRsOrders("track_link")	'shipping track
			shipMethod			=	objRsOrders("shiptype")
			shippedBy			=	objRsOrders("shipped_by")
			promoCode			=	objRsOrders("promoCode")
			shoprunnerid		=	objRsOrders("shoprunnerid")
						
			orderSubTotalAmt	=	cdbl(objRsOrders("ordersubtotal"))
			orderGrandTotalAmt	=	cdbl(objRsOrders("ordergrandtotal"))
			taxAmt				=	cdbl(objRsOrders("orderTax"))
			discountAmt			=	cdbl(objRsOrders("discount"))
			buySafeAmt			=	cdbl(objRsOrders("BuySafeAmount"))
			shippingFeeAmt		=	cdbl(objRsOrders("ordershippingfee"))
			cdo_from			=	storeName & "<sales@" & Lcase(storeName) & ".com>"
			strOrder			=	"Order"
			if objRsOrders("reshipType") = 2 then strOrder = "Backorder"
			
			if objRsOrders("shippingid") > 0 then
				strShipAddress = formatAddress(objRsOrders("ssAddress1"),objRsOrders("ssAddress2"),objRsOrders("ssCity"),objRsOrders("ssState"),objRsOrders("ssZip"))
			else
				strShipAddress = formatAddress(objRsOrders("sAddress1"),objRsOrders("sAddress2"),objRsOrders("sCity"),objRsOrders("sState"),objRsOrders("sZip"))			
			end if
			
			select case site_id
				case 0
					headerText	=	strOrder & " #" & curOrderID & " Successfully Shipped from WirelessEmporium.com"
					logoLink	=	"http://www.wirelessemporium.com/images/global/we_logo_solo.gif"
				case 1
					headerText	=	"Your CellphoneAccents.com " & strOrder & " #" & curOrderID & " is on the way!"
					logoLink	=	"http://www.cellphoneaccents.com/images/logo.jpg"
				case 2
					headerText	=	"Shipment Status from CellularOutfitter.com " & strOrder & " #" & curOrderID
					logoLink	=	"http://www.cellularoutfitter.com/images/CellularOutfitter_STANDARD.jpg"
				case 3
					headerText	=	"Phonesale.com " & strOrder & " #" & curOrderID & " - Shipment Status"
					logoLink	=	"http://www.phonesale.com/images/phonesale.jpg"
				case 10
					headerText	=	"Your TabletMall.com " & strOrder & " #" & curOrderID & " - Shipment Status"
					logoLink	=	"http://www.tabletmall.com/images/logo.gif"					
			end select

			cdo_subject	=	headerText			
			'=================================================================================================================
			
			'============================================== ORDER DETAIL TABLE ===============================================
			if instr(email, "@") > 0 and len(custName) > 0 then
				numItem = numItem + 1
				
				select case site_id
					case 0
						if objRsOrders("vendor") = "MS" then
							productLink = "http://www.wirelessemporium.com/p-ms-" & objRsOrders("itemid") & "-" & formatSEO(objRsOrders("itemdesc"))
						else
							productLink = "http://www.wirelessemporium.com/p-" & objRsOrders("itemid") & "-" & formatSEO(objRsOrders("itemdesc"))
						end if
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"" style=""font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#FB5908; text-decoration:none;"">" & objRsOrders("itemdesc") & "</a>"
					case 1
						productLink = "http://www.cellphoneaccents.com/p-" & objRsOrders("itemid") & "-" & formatSEO(objRsOrders("itemdesc")) & ".html"
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"" style=""font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#601D90; text-decoration:none;"">" & objRsOrders("itemdesc") & "</a>"
					case 2
						productLink = "http://www.cellularoutfitter.com/p-" & objRsOrders("itemid") & "-" & formatSEO(objRsOrders("itemdesc")) & ".html"
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"" style=""font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#0099CC; text-decoration:none;"">" & objRsOrders("itemdesc") & "</a>"					
					case 3
						if objRsOrders("typeid") = 16 then
							productLink = "http://www.phonesale.com/" & formatSEO(objRsOrders("brandname")) & "/cell-phones/p-" & objRsOrders("itemid")+300001 & "-tc-" & objRsOrders("typeid") & "-" & formatSEO(objRsOrders("itemdesc")) & ".html"
						else
							productLink = "http://www.phonesale.com/accessories/" & formatSEO(objRsOrders("typename")) & "/p-" & objRsOrders("itemid")+300001 & "-tc-" & objRsOrders("typeid") & "-" & formatSEO(objRsOrders("itemdesc")) & ".html"
						end if
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"" style=""font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#9B1D21; text-decoration:none;"">" & objRsOrders("itemdesc") & "</a>"
					case 10
						productLink = "http://www.tabletmall.com/" & formatSEO(objRsOrders("itemdesc")) & "-p-" & objRsOrders("itemid")
						itemDesc = 	"<a href=""" & productLink & """ target=""_blank"" style=""font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#357043; text-decoration:none;"">" & objRsOrders("itemdesc") & "</a>"
				end select

				qty			=	cdbl(objRsOrders("quantity"))
				price		=	cdbl(objRsOrders("price"))
				subtotal	=	cdbl(qty * price)
				
				strOrderDetails	=	strOrderDetails	&	"			        		<tr>" & vbcrlf & _
														"								<td align=""left"" valign=""top"" width=""70%""><b>" & numItem & ":</b> " & itemDesc & "</td>" & vbcrlf & _
														"								<td align=""right"" valign=""top"" width=""10%"">" & formatnumber(qty,0) & "</td>" & vbcrlf & _
														"								<td align=""right"" valign=""top"" width=""10%"">" & formatcurrency(price) & "</td>" & vbcrlf & _
														"								<td align=""right"" valign=""top"" width=""10%"">" & formatcurrency(subtotal) & "</td>" & vbcrlf & _
														"							</tr>" & vbcrlf
			end if
			'=================================================================================================================
			
			objRsOrders.movenext
		loop
		
		set objRsOrders = objRsOrders.nextrecordset
	loop
	
	if numItem > 0 then
		if strOrderDetails <> "" then
			strOrderDetails	=	strOrderDetails	&	"			        	</table>"
			strTemplate = replace(strTemplate, "[LOGO IMG LINK]", logoLink)
			strTemplate = replace(strTemplate, "[HEADER TEXT]", headerText)
			strTemplate = replace(strTemplate, "[CUSTOMER NAME]", custName)
			strTemplate = replace(strTemplate, "[TOP TEXT]", topText)
			strTemplate = replace(strTemplate, "[SHIPPED TO]", strShipAddress)
			strTemplate = replace(strTemplate, "[ORDER ID]", lastOrderID)					
			strTemplate = replace(strTemplate, "[ORDER DETAILS]", strOrderDetails)
			strTemplate = replace(strTemplate, "[ORDER SUBTOTAL]", formatcurrency(orderSubTotalAmt))
			if shoprunnerid <> "" then
				strTemplate = replace(strTemplate, "[SR]", "Thanks for using FREE 2-Day Shipping by ShopRunner! For returns, visit https://www.shoprunner.com/returns to print your pre-paid return label. Don't forget, invite a Friend to join.")
			else
				strTemplate = replace(strTemplate, "[SR]", "Next time get FREE 2-Day Shipping from Wireless Emporium and other great retailers with your ShopRunner membership. Visit https://www.wirelessemporium.com/shoprunner to sign up for a FREE 30-Day Trial")
'				strTemplate = replace(strTemplate, "[SR]", "")
			end if			
			
			if promoCode <> "" then
				strTemp = 	"            	<tr>" & vbcrlf & _
							"					<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>PROMO CODE:</b></td>" & vbcrlf & _
							"					<td width=""80%"" align=""left"" valign=""top"">" & promoCode & "</td>" & vbcrlf & _
							"				</tr>" & vbcrlf
							
				strTemplate = replace(strTemplate, "[PROMO CODE]", strTemp)
			else
				strTemplate = replace(strTemplate, "[PROMO CODE]", "")
			end if
	
			if discountAmt > 0 then
				strTemp = 	"				<tr>" & vbcrlf & _
							"					<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>DISCOUNT:</b></td>" & vbcrlf & _
							"					<td width=""80%"" align=""left"" valign=""top"">(" & formatcurrency(discountAmt) & ")</td>" & vbcrlf & _
							"				</tr>" & vbcrlf					
				strTemplate = replace(strTemplate, "[DISCOUNT]", strTemp)
			else
				strTemplate = replace(strTemplate, "[DISCOUNT]", "")
			end if
	
			if buySafeAmt > 0 then
				strTemp = 	"				<tr>" & vbcrlf & _
							"					<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>buySAFE Bond:</b></td>" & vbcrlf & _
							"					<td width=""80%"" align=""left"" valign=""top"">" & formatcurrency(buySafeAmt) & "</td>" & vbcrlf & _
							"				</tr>" & vbcrlf					
				strTemplate = replace(strTemplate, "[BUY SAFE]", strTemp)
			else
				strTemplate = replace(strTemplate, "[BUY SAFE]", "")
			end if			
			
			if shippedBy = "UPS-MI"	then
				strTemplate = replace(strTemplate, "[MEMO]", "(Tracking may take 24-48 hours to appear after your mail piece has shipped.)")
			else
				strTemplate = replace(strTemplate, "[MEMO]", "")
			end if					
	
			strTemplate = replace(strTemplate, "[TAX AMOUNT]", formatcurrency(taxAmt))
			strTemplate = replace(strTemplate, "[SHIPPING FEE]", formatcurrency(shippingFeeAmt))
			if shoprunnerid <> "" then
				strTemplate = replace(strTemplate, "[SHIPPING TYPE]", "ShopRunner, 2-Day Shipping - FREE")
				strTemplate = replace(strTemplate, "[TR TRACKING]", "")
			else
				if shipMethod = "First Class" then
					strTemplate = replace(strTemplate, "[SHIPPING TYPE]", shipMethod & " (3-10 Business Days)")
				else 
					strTemplate = replace(strTemplate, "[SHIPPING TYPE]", shipMethod)
				end if
				if shippedBy = "UPS-MI"	then
					strMemo = "(Tracking may take 24-48 hours to appear after your mail piece has shipped.)"
				else
					strMemo = ""
				end if
				strTracking = 	"            	<tr>" & vbcrlf & _
								"               	<td width=""20%"" align=""left"" valign=""top"" bgcolor=""#eaeaea""><b>SHIPMENT TRACKING:</b></td>" & vbcrlf & _
                				"					<td width=""80%"" align=""left"" valign=""top""><a href=""" & trackLink & """ target=""_blank""><b>Click Here</b></a>&nbsp;" & strMemo & "</td>" & vbcrlf & _
								"				</tr>"
				strTemplate = replace(strTemplate, "[TR TRACKING]", strTracking)
			end if
			strTemplate = replace(strTemplate, "[GRAND TOTAL]", formatcurrency(orderGrandTotalAmt))
			strTemplate = replace(strTemplate, "[STORE NAME]", storeName)

			'send an e-mail
'			call cdosend(cdo_from, cdo_to, cdo_subject, strTemplate, "")
			response.write strTemplate & "<br><br>"
		
			strTemplate 	= 	dup_strTemplate	'refresh the template for an another e-mail.
		end if
	end if
end if

function formatAddress(sAdd1,sAdd2,sCity,sState,sZip)
	dim strTemp
	strTemp = sAdd1
	if sAdd2 <> "" then 
		strTemp = strTemp & "<br />" & sAdd2 
	end if
	strTemp = strTemp & "<br />" & sCity & ", " & sState & "&nbsp;" & sZip & "<br />" & vbcrlf
	formatAddress = strTemp
end function

function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
end function


sub cdosend(strFrom, strTo, strSubject, strBody, strBcc)
	if instr(trim(strTo), "@") > 0 and instr(trim(strTo), " ") = 0 then
		set objEmail = CreateObject("CDO.Message")
		with objEmail
			.From = strFrom
			.To = strTo
			.Bcc = strBcc
			.Subject = strSubject
			.HTMLBody = strBody
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Update
			.Send
		end with
	end if
end sub

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")		
	else
		formatSEO = ""
	end if
	formatSEO = replace(formatSEO, "---", "-")
	formatSEO = replace(formatSEO, "--", "-")	
end function

Function isValidEmail(emailAddress)
	'Declare variables 
	Dim ValidEmail, emailParts, iLoopCounter, emailChar, acceptableChars, last2Chars 
	ValidEmail = True 'set the default result to True
	'acceptableChars are the characters that we will allow in our email
	acceptableChars="abcdefghijklmnopqrstuvwxyz.-_@"
	'use the Split function to create an array with the @ as the separator
	'so if your email was test@tester.com the email would be split into an array
	'with the first array element holding "test" and the second "tester.com"
	emailParts = Split(emailAddress, "@")
	'check to make sure that there is only 1 @ and that there are 2 parts
	'remember arrays are zero based
	'Using the UBound function will return the highest element in the array
	'So if it's a valid email the UBound function will return 1, i.e. 0 start
	If UBound(emailParts) <> 1 Then
	ValidEmail = false
	Else 
	'Check the length of each part of the email address
	'first part can be just one character, 2nd part must be atleast 4
	If Len(emailParts(0))<1 OR Len(emailParts(1))<4 Then
	ValidEmail = false
	End If
	'check first character on the left part isn't a "." using Left function
	If Left(emailParts(0), 1)="." Then
	ValidEmail = false
	End If 
	'check that there is a . in the second part of the email address - .com
	If InStr(emailParts(1), ".") <= 0 Then
	ValidEmail = false 
	End If 
	'We know there's a . now make sure there are 2 characters after the . for valid email
	last2Chars=Right(emailParts(1),2)
	If InStr(last2chars,".") Then
	ValidEmail = false
	End If
	'check that there shouldn't be a _ in the second part of the email address 
	If InStr(emailParts(1), "_") >0 Then
	ValidEmail = false 
	End If 
	End If
	'loop through each character of email
	For iLoopCounter = 1 to Len(emailAddress)
	'Use Lcase & Mid functions, Mid function used to return each individual character
	'in the email, and then Lcase converts it into lowercase
	emailChar = Lcase(Mid(emailAddress, iLoopCounter, 1)) 
	'Check if the emailAddress characters are acceptable
	If InStr(acceptableChars, emailChar) = 0 and Not IsNumeric(emailChar) Then
	ValidEmail = false
	End if
	Next
	'check if there is 2 . in a row
	If InStr(emailAddress, "..") > 0 Then
	ValidEmail=false
	End If
	'check if there is @. in a row
	If InStr(emailAddress, "@.") > 0 Then
	ValidEmail=false
	End If 
	isValidEmail=ValidEmail
End function
%>