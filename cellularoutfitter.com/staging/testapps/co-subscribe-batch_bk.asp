<%
set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getStagingConn()

bTestMode = true

sql = "exec sp_ppUpsell 2, 102964, 0, 30"
session("errorSQL") = sql
set recommendRS = oConn.execute(sql) 

strUpSell = ""

if not recommendRS.EOF then
	strUpSell = strUpSell & "		<table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf
	strUpSell = strUpSell & "			<tr>" & vbcrlf
	strUpSell = strUpSell & "				<td style=""background-color: #ffffff;"" width=""20"">&nbsp;</td>" & vbcrlf
	counter = 1
	do until recommendRS.EOF
		itemID		= recommendRS("itemID")
		itemPic		= recommendRS("itemPic")
		itemDesc	= recommendRS("itemDesc")
		useItemDesc = replace(itemDesc,"  "," ")
		itemImgPath = "C:\inetpub\wwwroot\productpics_co\thumb\" & itemPic
		price		= cdbl(recommendRS("price"))
		price_retail= cdbl(recommendRS("price_retail"))
		price_diff	= cdbl(price_retail - price)
		diff_percent	= cdbl(price_diff * 100 / price_retail) 

		productLink = "http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".html?dzid=email_transactional_RECOMMENDATIONS_" & counter & "L_" & itemID & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=Shipping&utm_content=Confirmation&utm_promocode=NA"
		
		set fsThumb = CreateObject("Scripting.FileSystemObject")
		if not fsThumb.FileExists(itemImgPath) then
			useImg = "/productPics/big/imagena.jpg"
			DoNotDisplay = 1
		else
			useImg = "/productPics/big/" & itemPic
		end if
		
		strUpSell = strUpSell & "				<td style=""background-color: #ffffff; color: #006699; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 150%; margin: 0;"" width=""200"" align=""center"" valign=""top"" >" & vbcrlf
		strUpSell = strUpSell & "					<a href=""" & productLink & """ target=""_blank""><img style=""display: block;"" src=""" & useImg & """ alt="""" width=""180"" border=""0"" /></a>" & vbcrlf
		strUpSell = strUpSell & "					<p style=""color:#006697; font:normal 11px Verdana, Geneva, sans-serif; line-height:1.5; margin:0; width:180px;"">" & vbcrlf
		strUpSell = strUpSell & "						<a style=""color:#006697;"" href=""" & productLink & """ target=""_blank"">" & itemDesc & "</a>" & vbcrlf
		strUpSell = strUpSell & "					</p>" & vbcrlf
		strUpSell = strUpSell & "					<br />" & vbcrlf
		strUpSell = strUpSell & "					<p style=""color:#006697; font:normal 12px Verdana, Geneva, sans-serif; line-height:1.1; margin:0; padding-bottom:60px "">" & vbcrlf
		strUpSell = strUpSell & "						<span style=""font-size:11px; color:#cccccc;"">Retail Price: <span style=""font:normal Verdana, Geneva, sans-serif; text-decoration:line-through "">" & formatCurrency(price_retail) & "</span></span>" & vbcrlf
		strUpSell = strUpSell & "						<br />" & vbcrlf
		strUpSell = strUpSell & "						<strong style=""color:#000; font-size:14px"">Our Price: " & formatCurrency(price) & "</strong>" & vbcrlf
		strUpSell = strUpSell & "						<br /><br />" & vbcrlf
		strUpSell = strUpSell & "						<a href=""" & productLink & """ style=""color:#26af61; text-decoration:none; font-size:12px; ""><strong>YOU SAVE " & formatCurrency(price_diff) & " (" & round(diff_percent) & "%)</strong></a>" & vbcrlf
		strUpSell = strUpSell & "					</p>" & vbcrlf
		strUpSell = strUpSell & "				</td>" & vbcrlf
		
		if counter = 3 then
			strUpSell = strUpSell & "			<td style=""background-color: #ffffff;"" width=""20"">&nbsp;</td>" & vbcrlf
			strUpSell = strUpSell & "		</tr>" & vbcrlf
			strUpSell = strUpSell & "		<tr>" & vbcrlf
			strUpSell = strUpSell & "			<td style=""background-color: #ffffff;"" width=""20"">&nbsp;</td>" & vbcrlf
		elseif counter = 6 then
			strUpSell = strUpSell & "			<td style=""background-color: #ffffff;"" width=""20"">&nbsp;</td>" & vbcrlf
			strUpSell = strUpSell & "		</tr>" & vbcrlf
			strUpSell = strUpSell & "	</table>" & vbcrlf
			
			exit do
		end if
		
		recommendRS.movenext
		counter = counter + 1
	loop
end if

strTemplate = readTextFile("C:\inetpub\wwwroot\cellularoutfitter.com\www\images\email\template\co-subscribe-batch.htm")
strTemplate = replace(strTemplate, "[UPSELL]", strUpSell)

response.write strTemplate

function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")		
	else
		formatSEO = ""
	end if
	formatSEO = replace(formatSEO, "---", "-")
	formatSEO = replace(formatSEO, "--", "-")	
end function

%>