<%
set weUtil = CreateObject("weUtil.DBConn")
set oConn = weUtil.getStagingConn()

bTestMode = true

sql = "exec sp_ppUpsell 2, 102964, 0, 30"
session("errorSQL") = sql
set recommendRS = oConn.execute(sql) 

strUpSell = ""

if not recommendRS.EOF then
	strUpSell = strUpSell & "		<table width=""370"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf
	strUpSell = strUpSell & "			<tr><td>&nbsp;</td></tr>" & vbcrlf
	strUpSell = strUpSell & "			<tr>" & vbcrlf
	counter = 1
	do until recommendRS.EOF
		itemID		= recommendRS("itemID")
		itemPic		= recommendRS("itemPic")
		itemDesc	= recommendRS("itemDesc")
		useItemDesc = replace(itemDesc,"  "," ")
		itemImgPath = server.MapPath("/productPics/thumb/" & itemPic)
		itemImgPath = "C:\inetpub\wwwroot\productpics_co\thumb\" & itemPic
		price		= recommendRS("price")
		price_retail= cdbl(recommendRS("price_retail"))
		promo_price	= cdbl(price - (price * .25))
		productLink = "http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(itemDesc) & ".html?dzid=email_transactional_RECOMMENDATIONS_" & counter & "L_" & itemID & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=Welcome1&utm_content=Confirmation&utm_promocode=WELCOME25"
		
		set fsThumb = CreateObject("Scripting.FileSystemObject")
		if not fsThumb.FileExists(itemImgPath) then
			useImg = "/productPics/big/imagena.jpg"
			DoNotDisplay = 1
		else
			useImg = "/productPics/big/" & itemPic
		end if
		
		strUpSell = strUpSell & "			<td width=""174"" align=""center"" valign=""top"" style=""background-color: #ffffff; color: #006699; font-family: Verdana, Geneva, sans-serif; font-size: 14px; font-weight: bold; line-height: 150%; margin: 0; padding:5px"" >" & vbcrlf
		strUpSell = strUpSell & "				<a href=""" & productLink & """ target=""_blank"" style=""text-decoration:none; "">" & vbcrlf
		strUpSell = strUpSell & "					<img src=""" & useImg & """ width=""140"" border=""0"" style=""display:block;padding-bottom:10px;""  />" & vbcrlf
		strUpSell = strUpSell & "					<p style=""color:#333333; font:bold 12px Verdana, Geneva, sans-serif; line-height:18px; min-height:72px; margin:0; text-align:left; padding:0 0 10px 10px;height: 90px;"">" & itemDesc & "</p>" & vbcrlf
		strUpSell = strUpSell & "				</a>" & vbcrlf                                    
		strUpSell = strUpSell & "				<p style=""color:#cccccc; font:normal 11px Verdana, Geneva, sans-serif; line-height:14px; margin:0; text-align:left; padding-left:10px;"">" & vbcrlf
		strUpSell = strUpSell & "					Retail Price: <span style=""font:normal Verdana, Geneva, sans-serif; text-decoration:line-through "">" & formatCurrency(price_retail) & "</span>" & vbcrlf
		strUpSell = strUpSell & "				</p>" & vbcrlf
		strUpSell = strUpSell & "				<p style=""color:#cccccc; font:normal 11px Verdana, Geneva, sans-serif; line-height:14px; margin:0; text-align:left; padding-left:10px;"">" & vbcrlf
		strUpSell = strUpSell & "					Our Price: " & formatCurrency(price) & "" & vbcrlf
		strUpSell = strUpSell & "				</p>" & vbcrlf
		strUpSell = strUpSell & "				<p style=""color:#333333; text-decoration:none; font-size:12px; padding-left:10px; text-align:left;"">" & vbcrlf
		strUpSell = strUpSell & "					<strong >Promo Price:</strong><span style=""color:#ce0002; font-weight:normal""> " & formatCurrency(promo_price) & "</span>" & vbcrlf
		strUpSell = strUpSell & "				</p>" & vbcrlf
		strUpSell = strUpSell & "				<a href=""" & productLink & """ target=""_blank"">" & vbcrlf
		strUpSell = strUpSell & "					<table width=""150"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf
		strUpSell = strUpSell & "						<tr>" & vbcrlf
		strUpSell = strUpSell & "							<td style=""text-align:center;font-weight:normal;color:#ffffff;font-size:14px;line-height:18px;background:#33cc66;border:1px solid #009966;border-radius:20px;padding:5px 10px;"">BUY NOW &raquo;</td>" & vbcrlf
		strUpSell = strUpSell & "						</tr>" & vbcrlf
		strUpSell = strUpSell & "					</table>" & vbcrlf
		strUpSell = strUpSell & "				</a>" & vbcrlf
		strUpSell = strUpSell & "			</td>" & vbcrlf
		
		if counter = 1 or counter = 3 then
			strUpSell = strUpSell & "		<td width=""2""><img src=""http://www.cellularoutfitter.com/images/subscribe/v_divider.jpg"" width=""1"" height=""320""  border=""0"" style=""display:block;"" /></td>" & vbcrlf
		elseif counter = 2 then
			strUpSell = strUpSell & "	</tr>" & vbcrlf
			strUpSell = strUpSell & "	<tr>" & vbcrlf
			strUpSell = strUpSell & "		<td width=""174""  style=""border-bottom:1px solid #cccccc;padding:5px 0;""></td>" & vbcrlf
			strUpSell = strUpSell & "		<td width=""2"" style=""border-bottom:1px solid #cccccc;padding:5px 0;""></td>" & vbcrlf
			strUpSell = strUpSell & "		<td width=""174""  style=""border-bottom:1px solid #cccccc;padding:5px 0;""></td>" & vbcrlf
			strUpSell = strUpSell & "	</tr>" & vbcrlf
			strUpSell = strUpSell & "	<tr>" & vbcrlf
		else 
			strUpSell = strUpSell & "	</tr>" & vbcrlf
            strUpSell = strUpSell & "</table>" & vbcrlf
			exit do
		end if
		
		recommendRS.movenext
		counter = counter + 1
	loop
end if

strTemplate = readTextFile("C:\inetpub\wwwroot\cellularoutfitter.com\www\images\email\template\co-subscribe.htm")
strTemplate = replace(strTemplate, "[UPSELL]", strUpSell)
strTemplate = replace(strTemplate, "[EXPIRY]", DateAdd("d", 7, Date()))
response.write strTemplate

function readTextFile(fileName)
	Dim strContents	:	strContents	=	""

	If "" <> fileName Then 
		Const ForReading = 1, ForWriting = 2, ForAppending = 3
		Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

		Dim FSO, Filepath
		set FSO = createObject("Scripting.FileSystemObject")
		Filepath = fileName

		if FSO.FileExists(Filepath) Then
			Dim TextStream
			Set TextStream = FSO.OpenTextFile(Filepath, ForReading, False, TristateUseDefault)

			' Read file in one hit
			strContents = TextStream.ReadAll

			TextStream.Close
			Set TextStream = nothing
		End If

		Set FSO = Nothing
	End If 

	readTextFile	=	strContents
end function

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")		
	else
		formatSEO = ""
	end if
	formatSEO = replace(formatSEO, "---", "-")
	formatSEO = replace(formatSEO, "--", "-")	
end function

%>