<%
dim ReceiptText
ReceiptText = ""

dim sPromoCode
SQL = "SELECT B.PromoCode FROM we_orders A INNER JOIN CO_coupons AS B ON A.couponid=B.couponid WHERE A.orderid = '" & nOrderID & "'"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.EOF then
	SQL = replace(lcase(SQL),"we_orders","we_orders_historical")
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
end if

if not RS.eof then sPromoCode = RS("PromoCode")

dim sFname, sLname, sEmail, sPhone
dim saddress1, saddress2, sCity, sstate, szip, sCountry
dim sShipAddress, sBillAddress
dim oRsCust, oRsOrd

SQL = "SELECT * FROM CO_accounts WHERE accountid = '" & nAccountId & "'"
set oRsCust = Server.CreateObject("ADODB.Recordset")
oRsCust.open SQL, oConn, 3, 3
if oRsCust.eof then
	ReceiptText = "<h3>Customer account" & nAccountId & " not found!</h3>"
else
	sFname = oRsCust("fname")
	sLname = oRsCust("lname")
	sEmail = oRsCust("email")
	sPhone = oRsCust("phone")
end if

SQL = "SELECT * FROM we_orders WHERE orderid = '" & nOrderId & "'"
set oRsOrd = Server.CreateObject("ADODB.Recordset")
oRsOrd.open SQL, oConn, 0, 1

if oRsOrd.EOF then
	SQL = replace(lcase(SQL),"we_orders","we_orders_historical")
	set oRsOrd = Server.CreateObject("ADODB.Recordset")
	oRsOrd.open SQL, oConn, 0, 1
end if

if oRsOrd.eof then ReceiptText = "<h3>Order" & nOrderId & " not found!</h3>"

'format the shipping and billing addresses
dim nShippingid
nShippingid = oRsOrd("shippingid")
if nShippingid > 0 then
	strSql = "SELECT * FROM we_addl_shipping_addr WHERE id='" & nShippingid & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open strSql, oConn, 0, 1
	if not RS.eof then
		saddress1 = RS("saddress1")
		saddress2 = RS("saddress2")
		sCity = RS("sCity")
		sstate = RS("sstate")
		szip = RS("szip")
		sCountry = RS("sCountry")
	end if
else
	saddress1 = oRsCust("saddress1")
	saddress2 = oRsCust("saddress2")
	sCity = oRsCust("sCity")
	sstate = oRsCust("sstate")
	szip = oRsCust("szip")
	sCountry = oRsCust("sCountry")
end if

sShipAddress = "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & sAddress1 & "</p>" & vbcrlf
if sAddress2 <> "" then 
	sShipAddress = sShipAddress & "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & sAddress2 & "</p>" & vbcrlf
end if
sShipAddress = sShipAddress & "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & sCity & ", " & sState & "&nbsp;" & sZip & "&nbsp;" & sCountry & "</p>" & vbcrlf

sBillAddress = "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & oRsCust("bAddress1") & "</p>" & vbcrlf
if oRsCust("bAddress2") <> "" then 
	sBillAddress = sBillAddress & "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & oRsCust("bAddress2") & "</p>" & vbcrlf
end if
sBillAddress = sBillAddress & "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & oRsCust("bCity") & ", " & oRsCust("bState") & "&nbsp;" & oRsCust("bZip") & "&nbsp;" & oRsCust("bCountry") & "</p>" & vbcrlf




oRsCust.close
set oRsCust = nothing

if ReceiptText = "" then
	'format order info
	'dim nOrderSubTotal
'	nOrderSubTotal = oRsOrd("ordersubtotal")
	emailSubTotal = oRsOrd("ordersubtotal")
	sShipType = oRsOrd("shiptype")
'	nShipFee = oRsOrd("ordershippingfee")
	emailShipFee = oRsOrd("ordershippingfee")
	if prepInt(nShipFee) = 0 then nShipFee = emailShipFee
'	nOrderTax = oRsOrd("orderTax")
	emailOrderTax = oRsOrd("orderTax")
	if prepInt(nOrderTax) = 0 then nOrderTax = emailOrderTax
	nBuysafeamount = oRsOrd("BuySafeAmount")
'	nOrderGrandTotal = oRsOrd("ordergrandtotal")
	emailOrderGrandTotal = oRsOrd("ordergrandtotal")
	extOrderType = oRsOrd("extOrderType")
	extOrderNumber = oRsOrd("extOrderNumber")
	nOrderDateTime = oRsOrd("orderdatetime")
	
	dim strOrderType
	select case extOrderType
		case 1 : strOrderType = "Paypal<br>" & extOrderNumber
		case 2 : strOrderType = "Google Checkout<br>" & extOrderNumber
		case 3
			strOrderType = "eBillme"
			eBillme = "eBillme"
			eBillmeAccount = extOrderNumber
		case else : strOrderType = "Credit Card"
	end select
	
	oRsOrd.close
	set oRsOrd = nothing
	
	'===================
	'NEW WEBLOYALTY CODE
	dim RSwl, cctype, country, pcode, strToEncrypt, strEncryptedValue
	SQL = "SELECT A.CCtype, B.bState, B.bZip FROM we_ccinfo A INNER JOIN CO_Accounts B ON A.accountid = B.accountid WHERE A.orderid='" & nOrderID & "'"
	set RSwl = Server.CreateObject("ADODB.Recordset")
	RSwl.open SQL, oConn, 0, 1
	if not RSwl.eof then
		cctype = RSwl("CCtype")
		if len(RSwl("bState")) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",RSwl("bState")) > 0 then
			country = "CANADA"
		else
			country = "US"
		end if
		pcode = RSwl("bZip")
	end if
	RSwl.close
	set RSwl = nothing
	
	' Put together string ?concatenate all required values
	if incEmail = true then
		dim yesexiturl, noexiturl
		yesexiturl = "https://www.cellularoutfitter.com/confirm.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & emailOrderGrandTotal & "&c=" & emailSubTotal
		noexiturl = yesexiturl
		strToEncrypt = "cref" & chr(255) & nOrderID & chr(255) & "cctype" & chr(255) & cctype & chr(255) & "country" & chr(255) & country & chr(255) & "pcode" & chr(255) & pcode & chr(255) & "yesexiturl" & chr(255) & yesexiturl & chr(255) & "noexiturl" & chr(255) & noexiturl
	else
		strToEncrypt = "cref" & chr(255) & nOrderID & chr(255) & "cctype" & chr(255) & cctype & chr(255) & "country" & chr(255) & country & chr(255) & "pcode" & chr(255) & pcode
	end if
	' Encrypt it
	strEncryptedValue = SimpleEncrypt(strToEncrypt)
	'===================
	
'	linkStagingDomain	= "http://staging.cellularoutfitter.com/"
'	if nOrderGrandTotal then
'		self_link = linkStagingDomain & "confirm.asp?o=" & nOrderID & "&a=" & nAccountID & "&d=" & nOrderAmount & "&ppd=" & nOrderGrandTotal & "&dzid=email_transactional_HEADER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"
'	else
'		self_link = linkStagingDomain & "confirm.asp?o=" & nOrderID & "&a=" & nAccountID & "&d=" & nOrderAmount & "&pp=" & usePostPurchase & "&dzid=email_transactional_HEADER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"
'	end if

	linkDomain	= "https://www.cellularoutfitter.com/"
	if instr(request.ServerVariables("SERVER_NAME"), "staging.cellularoutfitter.com") > 0 then linkDomain = "http://staging.cellularoutfitter.com/"
	self_link = linkDomain & "confirm.asp?o=" & nOrderID & "&a=" & nAccountID & "&dzid=email_transactional_HEADER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"
    
	ReceiptText = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN""	""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
	ReceiptText = ReceiptText & "<html xmlns=""http://www.w3.org/1999/xhtml"">" & vbcrlf
	ReceiptText = ReceiptText & "<head>" & vbcrlf
	ReceiptText = ReceiptText & "<head>" & vbcrlf
    ReceiptText = ReceiptText & "<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />" & vbcrlf
    ReceiptText = ReceiptText & "<meta name=""viewport"" content=""width=device-width, initial-scale=1.0"" />" & vbcrlf
    ReceiptText = ReceiptText & "<title>Your Order Has Been Confirmed. Thanks For Shopping With Cellular Outfitter.</title>" & vbcrlf
    ReceiptText = ReceiptText & "<style type=""text/css"">" & vbcrlf
    ReceiptText = ReceiptText & "    .ReadMsgBody {" & vbcrlf
    ReceiptText = ReceiptText & "    	width: 100%;" & vbcrlf
    ReceiptText = ReceiptText & "    }" & vbcrlf
    ReceiptText = ReceiptText & "    .ExternalClass {" & vbcrlf
    ReceiptText = ReceiptText & "    	width: 100%;" & vbcrlf
    ReceiptText = ReceiptText & "    }" & vbcrlf
    ReceiptText = ReceiptText & "</style>" & vbcrlf
	ReceiptText = ReceiptText & "</head>" & vbcrlf
	ReceiptText = ReceiptText & "<body style=""margin: 0; padding: 0; width: 100%; -webkit-text-size-adjust: none !important;	-ms-text-resize: none !important;"">" & vbcrlf
    ReceiptText = ReceiptText & "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf
    ReceiptText = ReceiptText & "    <tr>" & vbcrlf
    ReceiptText = ReceiptText & "        <td style=""background-color: #f7f7f7; -webkit-text-size-adjust: none;"" width=""100%"" align=""center"">" & vbcrlf
    ReceiptText = ReceiptText & "            <table style=""margin: 0;"" width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf
	ReceiptText = ReceiptText & "			 	 <tr>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""color: #333333; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 2.0;text-align:center;"" width=""640"" height=""40"" align=""left"" valign=""middle"">" & vbcrlf
    ReceiptText = ReceiptText & "                        Having trouble viewing this email?" & vbcrlf
    ReceiptText = ReceiptText & "                        <a style=""color: #006699;"" href=""" & self_link & """ target=""_blank"">Click here</a>." & vbcrlf
    ReceiptText = ReceiptText & "                </tr>" & vbcrlf
	ReceiptText = ReceiptText & "                <tr>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px;"" width=""640"" height=""30"" align=""center"" valign=""middle"">" & vbcrlf
	ReceiptText = ReceiptText & "						Your Order Has Been Confirmed. Thanks For Shopping With Cellular Outfitter." & vbcrlf
	ReceiptText = ReceiptText & "					</td>" & vbcrlf
    ReceiptText = ReceiptText & "                </tr>" & vbcrlf
    ReceiptText = ReceiptText & "            </table>" & vbcrlf
    ReceiptText = ReceiptText & "            <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf
    ReceiptText = ReceiptText & "                <tr>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #ffffff; color: #999999; font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 175%;"" width=""240"" height=""80"" align=""center"" valign=""middle"">" & vbcrlf
    ReceiptText = ReceiptText & "                        <a href=""http://www.cellularoutfitter.com/?dzid=email_transactional_HEADER_2L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" target=""_blank"">" & vbcrlf
    ReceiptText = ReceiptText & "                            <img style=""display: block;"" src=""http://www.cellularoutfitter.com/images/email/blast/template/images/co-logo.gif"" width=""220"" height=""45"" alt=""CellularOutfitter.com"" border=""0"" />" & vbcrlf
    ReceiptText = ReceiptText & "                        </a>" & vbcrlf
    ReceiptText = ReceiptText & "                        <b>WHOLESALE PRICES</b> ON ACCESSORIES</td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #ffffff;"" width=""390"" align=""right"" valign=""middle"">" & vbcrlf
    ReceiptText = ReceiptText & "                        <a href=""http://www.cellularoutfitter.com/?dzid=email_transactional_HEADER_3L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" target=""_blank"">" & vbcrlf
    ReceiptText = ReceiptText & "                            <img src=""http://www.cellularoutfitter.com/images/email/blast/template/images/co-values.gif"" width=""300"" height=""38"" alt=""110% Low Price Guarantee | 90-Day Return Policy"" border=""0"" />" & vbcrlf
    ReceiptText = ReceiptText & "                        </a>" & vbcrlf
    ReceiptText = ReceiptText & "                    </td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #ffffff;"" width=""10""></td>" & vbcrlf
    ReceiptText = ReceiptText & "                </tr>" & vbcrlf
    ReceiptText = ReceiptText & "            </table>" & vbcrlf
    ReceiptText = ReceiptText & "            <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbcrlf
    ReceiptText = ReceiptText & "                <tr>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;"" width=""79"" height=""45"" align=""center"" valign=""middle"">" & vbcrlf
    ReceiptText = ReceiptText & "                        <a style=""color: #ffffff; text-decoration: none;"" href=""http://www.cellularoutfitter.com/c-3-cell-phone-covers-and-gel-skins.html?dzid=email_transactional_NAV_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" target=""_blank"">Covers &amp; Skins</a>" & vbcrlf
    ReceiptText = ReceiptText & "                    </td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #124f80;"" width=""1"" height=""45""></td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;"" width=""79"" height=""45"" align=""center"" valign=""middle"">" & vbcrlf
    ReceiptText = ReceiptText & "                        <a style=""color: #ffffff; text-decoration: none;"" href=""http://www.cellularoutfitter.com/c-7-cell-phone-cases-and-pouches.html?dzid=email_transactional_NAV_2L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" target=""_blank"">Cases &amp; Pouches</a>" & vbcrlf
    ReceiptText = ReceiptText & "                    </td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #124f80;"" width=""1"" height=""45""></td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;"" width=""79"" height=""45"" align=""center"" valign=""middle"">" & vbcrlf
    ReceiptText = ReceiptText & "                        <a style=""color: #ffffff; text-decoration: none;"" href=""http://www.cellularoutfitter.com/c-18-cell-phone-screen-protectors.html?dzid=email_transactional_NAV_3L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" target=""_blank"">Screen Protectors</a>" & vbcrlf
    ReceiptText = ReceiptText & "                    </td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #124f80;"" width=""1"" height=""45""></td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;"" width=""79"" height=""45"" align=""center"" valign=""middle"">" & vbcrlf
    ReceiptText = ReceiptText & "                        <a style=""color: #ffffff; text-decoration: none;"" href=""http://www.cellularoutfitter.com/c-24-cell-phone-custom-cases.html?dzid=email_transactional_NAV_4L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" target=""_blank"">Custom Covers</a>" & vbcrlf
    ReceiptText = ReceiptText & "                    </td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #124f80;"" width=""1"" height=""45""></td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;"" width=""79"" height=""45"" align=""center"" valign=""middle"">" & vbcrlf
    ReceiptText = ReceiptText & "                        <a style=""color: #ffffff; text-decoration: none;"" href=""http://www.cellularoutfitter.com/c-2-cell-phone-chargers-and-data-cables.html?dzid=email_transactional_NAV_5L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" target=""_blank"">Chargers Cables</a>" & vbcrlf
    ReceiptText = ReceiptText & "                    </td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #124f80;"" width=""1"" height=""45""></td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;"" width=""79"" height=""45"" align=""center"" valign=""middle"">" & vbcrlf
    ReceiptText = ReceiptText & "                        <a style=""color: #ffffff; text-decoration: none;"" href=""http://www.cellularoutfitter.com/c-5-cell-phone-bluetooth-and-hands-free.html?dzid=email_transactional_NAV_6L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" target=""_blank"">Bluetooth Headsets</a>" & vbcrlf
    ReceiptText = ReceiptText & "                    </td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #124f80;"" width=""1"" height=""45""></td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;"" width=""79"" height=""45"" align=""center"" valign=""middle"">" & vbcrlf
    ReceiptText = ReceiptText & "                        <a style=""color: #ffffff; text-decoration: none;"" href=""http://www.cellularoutfitter.com/c-6-cell-phone-holsters-and-car-mounts.html?dzid=email_transactional_NAV_7L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" target=""_blank"">Holsters Mounts</a>" & vbcrlf
    ReceiptText = ReceiptText & "                    </td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #124f80;"" width=""1"" height=""45""></td>" & vbcrlf
    ReceiptText = ReceiptText & "                    <td style=""background-color: #2980b9; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 11px; font-weight: bold;"" width=""80"" height=""45"" align=""center"" valign=""middle"">" & vbcrlf
    ReceiptText = ReceiptText & "                        <a style=""color: #ffffff; text-decoration: none;"" href=""http://www.cellularoutfitter.com/sb-0-sm-0-sc-8-cell-phone-other-accessories.html?dzid=email_transactional_NAV_8L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" target=""_blank"">Other Accessories</a>" & vbcrlf
    ReceiptText = ReceiptText & "                    </td>" & vbcrlf
    ReceiptText = ReceiptText & "                </tr>" & vbcrlf
    ReceiptText = ReceiptText & "            </table>" & vbcrlf
	ReceiptText = ReceiptText & "			 <table width=""640"" border=""0"" cellspacing=""0"" cellpadding=""0"" style=""background-color:#ffffff;"">" & vbcrlf
	ReceiptText = ReceiptText & "				<tr>" & vbcrlf
	ReceiptText = ReceiptText & "					<td style=""width:640px;height:200px;padding:0;margin:0;"">" & vbcrlf
	ReceiptText = ReceiptText & "						<div style=""width:640px;height:200px;position:relative;float;left;""><img src=""http://www.cellularoutfitter.com/images/order-email/order-email-banner.jpg"" width=""640"" height=""200"" style=""margin:0;padding:0;"" /></div>" & vbcrlf
	ReceiptText = ReceiptText & "					</td>" & vbcrlf
	ReceiptText = ReceiptText & "				</tr>" & vbcrlf
	ReceiptText = ReceiptText & "				<tr>" & vbcrlf
	ReceiptText = ReceiptText & "					<td width=""640"">" & vbcrlf
	ReceiptText = ReceiptText & "						<p style=""margin:0;padding:20px 10px 0 10px; font-family:Tahoma, Helvetica, sans-serif;font-size:12px;text-align:center;"">Thank you for shopping with Cellular Outfitter.</p>" & vbcrlf
	ReceiptText = ReceiptText & "						<p style=""margin:0;padding:10px 10px 0 10px; font-family:Tahoma, Helvetica, sans-serif;font-size:12px;text-align:center;"">The order number for your order placed on " & dateValue(nOrderDateTime) & " is " & nOrderID & "</p>" & vbcrlf
	ReceiptText = ReceiptText & "						<p style=""margin:0;padding:10px 10px 0 10px; font-family:Tahoma, Helvetica, sans-serif;font-size:12px;text-align:center;"">Your merchandise will ship within 24 hours.*</p>" & vbcrlf
	ReceiptText = ReceiptText & "						<p style=""margin:0;padding:10px 10px 0 10px; font-family:Tahoma, Helvetica, sans-serif;font-size:12px;text-align:center;"">We will email you a shipping confirmation when your order is shipped.</p>" & vbcrlf
	ReceiptText = ReceiptText & "						<table width=""640"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""padding:20px 10px;"">" & vbcrlf
	ReceiptText = ReceiptText & "							<thead>" & vbcrlf
	ReceiptText = ReceiptText & "								<tr>" & vbcrlf
	ReceiptText = ReceiptText & "									<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:14px;padding:5px;text-align:left;border-top-left-radius:5px;background:#486084;color:#ffffff;font-weight:bold;"">Item #</td>" & vbcrlf
	ReceiptText = ReceiptText & "									<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:14px;padding:5px;background:#486084;color:#ffffff;font-weight:bold;width:76px;"">Product</td>" & vbcrlf
	ReceiptText = ReceiptText & "									<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:14px;padding:5px;background:#486084;color:#ffffff;font-weight:bold;text-align:center;"">Qty</td>" & vbcrlf
	ReceiptText = ReceiptText & "									<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:14px;padding:5px;background:#486084;color:#ffffff;font-weight:bold;width:246px;"">Description</td>" & vbcrlf
	ReceiptText = ReceiptText & "									<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:14px;padding:5px;background:#486084;color:#ffffff;font-weight:bold;text-align:center;"">Price</td>" & vbcrlf
	ReceiptText = ReceiptText & "									<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:14px;padding:5px;border-top-right-radius:5px;background:#486084;color:#ffffff;font-weight:bold;text-align:center;"">Total</td>" & vbcrlf
	ReceiptText = ReceiptText & "								</tr>" & vbcrlf
	ReceiptText = ReceiptText & "							</thead>" & vbcrlf
	ReceiptText = ReceiptText & "							<tbody>" & vbcrlf
	'get orderdetails
	dim oRsOrderDetails
	SQL = 	"SELECT c.brandName, d.modelName, A.itemid,A.PartNumber,A.itemDesc_CO,A.itemPic_CO,isnull(b.price, A.price_CO) price_co,B.quantity FROM we_items A INNER JOIN we_orderdetails B ON A.itemid = B.itemid left join we_brands c on a.brandID = c.brandID left join we_models d on a.modelID = d.modelID WHERE B.orderid = '" & nOrderId & "'" &_
			" union " &_
			"SELECT '' as brandName, '' as modelName, A.id as itemid,A.musicSkinsID as PartNumber,A.artist + ' ' + a.designName as itemDesc_CO,A.defaultImg as itemPic_CO,A.price_CO,B.quantity FROM we_items_musicSkins A INNER JOIN we_orderdetails B ON A.id = B.itemid WHERE B.orderid = '" & nOrderId & "'"
	'response.write SQL
	set oRsOrderDetails = Server.CreateObject("ADODB.Recordset")
	oRsOrderDetails.open SQL, oConn, 3, 3
	
	if oRsOrderDetails.EOF then
		SQL = replace(lcase(SQL),"we_orders","we_orders_historical")
		SQL = replace(SQL,"we_orderdetails","we_orderdetails_historical")
		set oRsOrderDetails = Server.CreateObject("ADODB.Recordset")
		oRsOrderDetails.open SQL, oConn, 3, 3
	end if
	
	if not oRsOrderDetails.eof then
		dim nCount, nID, nPartNumber, nQty, nPrice, itemdesc_CO, thisSubtotal, itemids, itemSubTotal
		itemSubTotal = cdbl(0)
		nCount = 1
		do until oRsOrderDetails.eof
			brandName = oRsOrderDetails("brandName")
			modelName = oRsOrderDetails("modelName")
			nID = oRsOrderDetails("itemid")
			nPartNumber = oRsOrderDetails("PartNumber")
			partNumber = nPartNumber
			nQty = cdbl(oRsOrderDetails("quantity"))
			nPrice = cdbl(oRsOrderDetails("price_CO"))
			subTotal = cdbl(nQty * nPrice)
			itemSubTotal = itemSubTotal + subTotal
			itemdesc_CO = insertDetails(oRsOrderDetails("itemDesc_CO"))
			itempic = oRsOrderDetails("itemPic_CO")
			
			itemImgPath = "C:\inetpub\wwwroot\productpics_co\thumb\" & itempic
			
			set fsThumb = CreateObject("Scripting.FileSystemObject")
			if not fsThumb.FileExists(itemImgPath) then
				useImg = "/productPics/thumb/imagena.jpg"
				DoNotDisplay = 1
			else
				useImg = "/productPics/thumb/" & itempic
			end if
			
			product_link = "http://www.cellularoutfitter.com/p-" & nID & "-" & formatSEO(itemdesc_CO) & ".html?dzid=email_transactional_ORDERREVIEW_" & nCount & "L_" & nID & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"
			
			
			ReceiptText = ReceiptText & "			        		<tr>" & vbcrlf & _
										"								<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;text-align:left;vertical-align:top;padding:10px 5px;border-left:1px solid #ccc;border-bottom:1px solid #ccc;border-right:1px solid #ccc;'>" & nPartNumber & "</td>" & vbcrlf & _
										"								<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;text-align:center;'><a href=""" & product_link & """><img src='http://www.cellularoutfitter.com" & useImg & "' height='65' /></a></td>" & vbcrlf & _
										"								<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;text-align:center;'>" & nQty & "</td>" & vbcrlf & _
										"								<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;'><a href=""" & product_link & """ style=""color:#000000;"">" & itemdesc_CO & "</a></td>" & vbcrlf & _
										"								<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;text-align:center;'>" & formatcurrency(nPrice) & "</td>" & vbcrlf & _
										"								<td style='font-family:Tahoma, Helvetica, sans-serif;font-size:12px;vertical-align:top;padding:10px 5px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;text-align:center;'>" & formatcurrency(subTotal) & "</td>" & vbcrlf & _
										"							</tr>" & vbcrlf
			nCount = nCount + 1
			nTotQty = nTotQty + nQty
			
			if itemids = "" then
				itemids = nID
			else
				itemids = itemids & "," & nID
			end if
			oRsOrderDetails.movenext
		loop
	end if
	
	if itemids then
		sqlUpSell = "exec sp_ppUpsell 2, '" & itemids & "', 0, 30"
		set objRsUpSell = oConn.execute(sqlUpSell)
		nCount = 1
		if not objRsUpSell.EOF then
			strUpSell 	=	"<table width='558' cellpadding='0' cellspacing='0' style='margin:20px 40px;border:2px solid #cccccc;'>" & vbcrlf & _
							"	<tr>" & vbcrlf & _
							"		<td style='padding:1px;'>" & vbcrlf & _
							"			<table width='554' cellpadding='0' cellspacing='0' style='border:1px solid #cccccc;'>" & vbcrlf & _
							"				<tr>" & vbcrlf & _
							"					<td style='font-family:Verdana, Helvetica, sans-serif;font-size:13px;line-height:20px;text-align:center;padding:15px;'>" & vbcrlf & _
							"						WE THOUGHT YOU MIGHT BE INTERESTED IN THESE GREAT PRODUCTS.<br />" & vbcrlf & _
							"						THEY ARE HIGHLY RECOMMENDED BY CUSTOMERS LIKE YOU." & vbcrlf & _
							"					</td>" & vbcrlf & _
							"				</tr>" & vbcrlf & _
							"			</table>" & vbcrlf & _
							"		</td>" & vbcrlf & _
							"	</tr>" & vbcrlf & _
							"</table>" & vbcrlf & _
							"<table width='640' cellpadding='0' cellspacing='0' style='padding:10px 0;'>" & vbcrlf & _
							"	<tr>"
			counter = 1
			do until objRsUpSell.EOF
				itemID		= objRsUpSell("itemID")
				itemPic		= objRsUpSell("itemPic")
				itemDesc	= objRsUpSell("itemDesc")
				useItemDesc = replace(itemDesc,"  "," ")
				'itemImgPath = server.MapPath("/productPics/thumb/" & itemPic)
				itemImgPath = "C:\inetpub\wwwroot\productpics_co\big\" & itempic
				price		= objRsUpSell("price")
				price_retail= objRsUpSell("price_retail")
				
				set fsThumb = CreateObject("Scripting.FileSystemObject")
				if not fsThumb.FileExists(itemImgPath) then
					useImg = "/productPics/big/imagena.jpg"
					DoNotDisplay = 1
				else
					useImg = "/productPics/big/" & itemPic
				end if
				
				product_link = "http://www.cellularoutfitter.com/p-" & itemID & "-" & formatSEO(useItemDesc) & ".html?dzid=email_transactional_RECOMMENDATIONS_" & nCount & "L_" & itemID & "_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"
				
				if counter < 4 then 
					strUpSell 	= strUpSell &	"	<td style='border-right:2px solid #cccccc;width:138px;padding:10px;text-align:center;'>" & vbcrlf & _
												"		<a href='" & product_link & "'><img src='http://www.cellularoutfitter.com" & useImg & "' height='132' /></a>" & vbcrlf & _
												"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;line-height:18px;color:#006596;font-weight:bold;font-size:12px;text-align:left;'><a href='" & product_link & "' style='color:#006596;font-weight:bold;font-size:12px;'>" & itemDesc & "</a></p>" & vbcrlf & _
												"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;font-size:12px;text-align:left;color:#cccccc;text-decoration:line-through;'>Retail Price: " & formatCurrency(price_retail) & "</p>" & vbcrlf & _
												"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;margin-top:10px;font-size:16px;font-weight:bold;text-align:left;color:#000000;'>Our Price:<br />" & formatCurrency(price) & "</p>" & vbcrlf & _
												"	</td>"
				else 
					strUpSell 	= strUpSell &	"	<td style='width:138px;padding:10px;text-align:center;'>" & vbcrlf & _
											"		<a href='" & product_link & "'><img src='http://www.cellularoutfitter.com" & useImg & "' height='132' /></a>" & vbcrlf & _
											"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;line-height:18px;color:#006596;font-weight:bold;font-size:12px;text-align:left;'><a href='" & product_link & "' style='color:#006596;font-weight:bold;font-size:12px;'>" & itemDesc & "</a></p>" & vbcrlf & _
											"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;font-size:12px;text-align:left;color:#cccccc;text-decoration:line-through;'>Retail Price: " & formatCurrency(price_retail) & "</p>" & vbcrlf & _
											"		<p style='padding:10px 0 0;margin:0;font-family:Tahoma, Helvetica, sans-serif;margin-top:10px;font-size:16px;font-weight:bold;text-align:left;color:#000000;'>Our Price:<br />" & formatCurrency(price) & "</p>" & vbcrlf & _
											"	</td>"
				end if
				
				if counter = 4 then
					exit do
				else
					counter = counter + 1
				end if
				nCount = nCount + 1
				objRsUpSell.movenext
			loop
		end if
		strUpSell		= strUpSell & "</tr>" & vbcrlf & _
						"</table>"
	end if
	
	
	oRsOrderDetails.close
	set oRsOrderDetails = nothing
	if isnull(sFname) or len(sFname) <= 0 then
		custName			=	"<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>Customer</p>"
		custNameAddress		=	"<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>Customer</p>"
	else
		custName			=	ucase(left(sFname, 1)) & lcase(right(sFname,len(sFname)-1))
		custNameAddress		=	"<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>" & ucase(left(sFname, 1)) & lcase(right(sFname,len(sFname)-1)) & " " & ucase(left(sLname, 1)) & lcase(right(sLname,len(sLname)-1)) & "</p>"
	end if
	
	if sPhone <> "" then
		strTelephone			= "<p style='font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;'>T: " & sPhone & "</p>"
	else
		strTelephone			= ""
	end if
	
	ReceiptText = ReceiptText & "							</tbody>" & vbcrlf
	ReceiptText = ReceiptText & "						</table>" & vbcrlf
	ReceiptText = ReceiptText & "						<table width=""640"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""padding:5px;"">" & vbcrlf
	ReceiptText = ReceiptText & "							<tr>" & vbcrlf
	ReceiptText = ReceiptText & "								<td>" & vbcrlf
	ReceiptText = ReceiptText & "									<h2 style=""font-family:Tahoma, Helvetica, sans-serif;text-align:left;padding:10px 10px 5px 10px;margin:0;color:#006596;font-weight:bold;font-size:16px;border-bottom:1px solid #cccccc;"">Invoice Details</h2>" & vbcrlf
	ReceiptText = ReceiptText & "									<p style=""font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;"">Order ID: " & nOrderId & "</p>" & vbcrlf
	ReceiptText = ReceiptText & "									<p style=""font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;text-align:left;font-size:11px;color:#7d7d7d;"">Shipping Method: " & sShipType & "</p>" & vbcrlf
	ReceiptText = ReceiptText & "									<table width=""630"" border=""0"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf
	ReceiptText = ReceiptText & "										<tr>" & vbcrlf
	ReceiptText = ReceiptText & "											<td style=""width:315px;padding:10px 5px 0 0;text-align:left;vertical-align:top;"">" & vbcrlf
	ReceiptText = ReceiptText & "												<h2 style=""font-family:Tahoma, Helvetica, sans-serif;text-align:left;padding:10px 10px 5px 10px;margin:0;color:#006596;font-weight:bold;font-size:16px;border-bottom:1px solid #cccccc;"">Billing Address</h2>" & vbcrlf
	ReceiptText = ReceiptText & 												custNameAddress & vbcrlf
	ReceiptText = ReceiptText & 												sBillAddress & vbcrlf
	ReceiptText = ReceiptText & 												strTelephone & vbcrlf
	ReceiptText = ReceiptText & "											</td>" & vbcrlf
	ReceiptText = ReceiptText & "											<td style=""width:315px;padding:10px 0px 0 5px;text-align:left;"">" & vbcrlf
	ReceiptText = ReceiptText & "												<h2 style=""font-family:Tahoma, Helvetica, sans-serif;text-align:left;padding:10px 10px 5px 10px;margin:0;color:#006596;font-weight:bold;font-size:16px;border-bottom:1px solid #cccccc;"">Shipping Address**</h2>" & vbcrlf
	ReceiptText = ReceiptText & 												custNameAddress & vbcrlf
	ReceiptText = ReceiptText & 												sShipAddress & vbcrlf
	ReceiptText = ReceiptText & 												strTelephone & vbcrlf
	ReceiptText = ReceiptText & "												<p style=""font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:10px 0 0 10px;margin:20px 0 0 0;text-align:left;font-size:11px;color:#7d7d7d;font-style:italic;"">**If the shipping address written above is incorrect,<br />please contact our customer service team as soon as<br />possible at 1-888-876-3137</p>" & vbcrlf
	ReceiptText = ReceiptText & "											</td>" & vbcrlf
	ReceiptText = ReceiptText & "										</tr>" & vbcrlf
	ReceiptText = ReceiptText & "									</table>" & vbcrlf
	ReceiptText = ReceiptText & "								</td>" & vbcrlf
	ReceiptText = ReceiptText & "							</tr>" & vbcrlf
	ReceiptText = ReceiptText & "						</table>" & vbcrlf
	ReceiptText = ReceiptText & "						<table width=""630"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""padding:5px;"">" & vbcrlf
	ReceiptText = ReceiptText & "							<tr>" & vbcrlf
	ReceiptText = ReceiptText & "								<td style=""width:305px;padding:5px;"" valign=""top"">" & vbcrlf
	ReceiptText = ReceiptText & "									<table width=""263"" height=""260"" cellpadding=""20"" cellspacing=""0"">" & vbcrlf
	ReceiptText = ReceiptText & "										<tr>" & vbcrlf
	ReceiptText = ReceiptText & "											<td style=""border:1px solid #cccccc;background:#f7f7f7;margin:0;padding:20px;"">" & vbcrlf
	ReceiptText = ReceiptText & "												<p style=""font-family:Tahoma, Helvetica, sans-serif;margin:0;padding:0 0 10px 0;font-weight:bold;text-align:left;font-size:14px;color:#006596;"">Save 25% OFF Your Next Order By Subscribing To Our Email Newsletter</p>" & vbcrlf
	ReceiptText = ReceiptText & "												<p style=""font-family:Tahoma, Helvetica, sans-serif;margin:0;line-height:18px;text-align:left;font-size:11px;color:#7d7d7d;"">You'll also receive exclusive access to special promotions, exciting content, and insider information before anyone else has a chance to see them.</p>" & vbcrlf
	ReceiptText = ReceiptText & "												<table width=""263"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""height:34px;border-spacing:0;padding:10px 0 0 0;"">" & vbcrlf
	ReceiptText = ReceiptText & "													<tr>" & vbcrlf
	ReceiptText = ReceiptText & "														<td width=""28"" style=""width:28px;height:34px;text-align:left;border-spacing:0;"">" & vbcrlf
	ReceiptText = ReceiptText & "															<img src=""http://www.cellularoutfitter.com/images/confirm-email/mail-icon.png"" width=""28"" height=""34"" style=""position:relative;float:left;"" />" & vbcrlf
	ReceiptText = ReceiptText & "														</td>" & vbcrlf
	ReceiptText = ReceiptText & "														<td width=""177"" style=""height:32px; border-top:1px solid #cccccc;border-bottom:1px solid #cccccc;"">" & vbcrlf
	ReceiptText = ReceiptText & "															<input type=""text"" value=""" & sEmail & """ style=""position:relative;float:left;margin:0;padding:5px;height:22px;border:0px;font-family:'Times New Roman', sans-serif;font-weight:bold;width:167px;background:#ffffff;font-size:12px;overflow:hidden;"" />" & vbcrlf
	ReceiptText = ReceiptText & "														</td>" & vbcrlf
	ReceiptText = ReceiptText & "														<td width=""58"">" & vbcrlf
	ReceiptText = ReceiptText & "															<div style=""font-family: Tahoma, Helvetica, sans-serif;margin: 0;height: 22px;border-left: 0;border-top: 1px solid #cccccc;border-right: 1px solid #cccccc;border-bottom: 1px solid #cccccc;background: #26af61;color: #ffffff;font-weight: normal;padding: 5px;border-bottom-right-radius: 5px;border-top-right-radius: 5px;cursor: pointer;font-size: 13px;width: 48px;line-height: 22px;text-decoration: none;""><a href=""http://www.cellularoutfitter.com/?dzid=email_transactional_EMAILSIGNUP_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA&utm_newsletter=" & sEmail & """ style=""color:#ffffff;text-decoration:none;"">SIGN UP</a></div>" & vbcrlf														
	ReceiptText = ReceiptText & "														</td>" & vbcrlf
	ReceiptText = ReceiptText & "													</tr>" & vbcrlf
	ReceiptText = ReceiptText & "												</table>" & vbcrlf
	ReceiptText = ReceiptText & "											</td>" & vbcrlf
	ReceiptText = ReceiptText & "										</tr>" & vbcrlf
	ReceiptText = ReceiptText & "									</table>" & vbcrlf
	ReceiptText = ReceiptText & "								</td>" & vbcrlf
	ReceiptText = ReceiptText & "								<td style=""width:305px;padding:5px;"" valign=""top"">" & vbcrlf
	ReceiptText = ReceiptText & "									<table width=""263"" height=""260"" cellpadding=""20"" cellspacing=""0"">" & vbcrlf
	ReceiptText = ReceiptText & "										<tr>" & vbcrlf
	ReceiptText = ReceiptText & "											<td style=""border:1px solid #cccccc;background:#f7f7f7;margin:5px;font-size:12px;color:#7d7d7d;"">" & vbcrlf
	ReceiptText = ReceiptText & "												<table width=""263"" cellpadding=""5"" cellspacing=""0"">" & vbcrlf
	ReceiptText = ReceiptText & "													<tr>" & vbcrlf
	ReceiptText = ReceiptText & "														<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;width:154px;text-align:right;border-bottom:1px solid #cccccc;"">Order Subtotal:</td>" & vbcrlf
	ReceiptText = ReceiptText & "														<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;text-align:right;padding-right:20px;border-bottom:1px solid #cccccc;"">" & formatCurrency(emailSubTotal,2) & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "													</tr>" & vbcrlf
	if emailOrderTax > 0 then
		ReceiptText = ReceiptText & "												<tr>" & vbcrlf
		ReceiptText = ReceiptText & "													<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;width:154px;text-align:right;border-bottom:1px solid #cccccc;"">Tax:</td>" & vbcrlf
		ReceiptText = ReceiptText & "													<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;text-align:right;padding-right:20px;border-bottom:1px solid #cccccc;"">" & formatCurrency(emailOrderTax,2) & "</td>" & vbcrlf
		ReceiptText = ReceiptText & "												</tr>" & vbcrlf
	end if
	ReceiptText = ReceiptText & "													<tr>" & vbcrlf
	ReceiptText = ReceiptText & "														<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;width:154px;text-align:right;border-bottom:1px solid #cccccc;"">Shipping Fee:</td>" & vbcrlf
	ReceiptText = ReceiptText & "														<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;text-align:right;padding-right:20px;border-bottom:1px solid #cccccc;"">" & formatCurrency(emailShipFee,2) & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "													</tr>" & vbcrlf
	if sPromoCode <> "" then
		ReceiptText = ReceiptText & "												<tr>" & vbcrlf
		ReceiptText = ReceiptText & "													<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;width:154px;text-align:right;border-bottom:1px solid #cccccc;"">PromoCode:</td>" & vbcrlf
		ReceiptText = ReceiptText & "													<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;text-align:right;padding-right:20px;border-bottom:1px solid #cccccc;"">" & sPromoCode & "</td>" & vbcrlf
		ReceiptText = ReceiptText & "												</tr>" & vbcrlf
	end if	
	if (emailSubTotal - itemSubTotal) < 0 then
		ReceiptText = ReceiptText & "												<tr>" & vbcrlf
		ReceiptText = ReceiptText & "													<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;width:154px;text-align:right;border-bottom:1px solid #cccccc;"">Discount:</td>" & vbcrlf
		ReceiptText = ReceiptText & "													<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;text-align:right;padding-right:20px;border-bottom:1px solid #cccccc;"">" & formatCurrency(emailSubTotal - itemSubTotal,2) & "</td>" & vbcrlf
		ReceiptText = ReceiptText & "												</tr>" & vbcrlf
	end if
	ReceiptText = ReceiptText & "												</table>" & vbcrlf
	ReceiptText = ReceiptText & "												<table width=""263"" cellpadding=""5"" cellspacing=""0"" style=""margin-top:1px;"">" & vbcrlf
	ReceiptText = ReceiptText & "													<tr>" & vbcrlf
	ReceiptText = ReceiptText & "														<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;padding-top:5px;width:154px;text-align:right;border-top:1px solid #cccccc;color:#000000;font-weight:bold;"">Grand Total:</td>" & vbcrlf
	ReceiptText = ReceiptText & "														<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;padding-top:5px;padding-right:20px;text-align:right;border-top:1px solid #cccccc;color:#000000;font-weight:bold;"">" & formatCurrency(emailOrderGrandTotal,2) & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "													</tr>" & vbcrlf
	ReceiptText = ReceiptText & "												</table>" & vbcrlf
	if (emailOrderGrandTotal - nOrderGrandTotal) > 1 and nOrderGrandTotal > 0 then
		ReceiptText = ReceiptText & "											<table width=""263"" cellpadding=""5"" cellspacing=""0"" style=""padding-top:10px;"">" & vbcrlf
		ReceiptText = ReceiptText & "												<tr>" & vbcrlf
		ReceiptText = ReceiptText & "													<td style=""font-family:Tahoma, Helvetica, sans-serif;font-size:11px;padding-top:5px;width:154px;text-align:center;color:#000000;font-weight:normal;"">" & vbcrlf
		ReceiptText = ReceiptText & "														Thank you for your order! Please remember that you will see two charges on your account that total <b>" & formatCurrency(emailOrderGrandTotal,2) & "</b>. One charge in the amount of <b>" & formatCurrency((emailOrderGrandTotal - nOrderGrandTotal),2) & "</b> and the other in the amount of <b>" & formatCurrency(nOrderGrandTotal,2) & "</b>." & vbcrlf
		ReceiptText = ReceiptText & "													</td>" & vbcrlf
		ReceiptText = ReceiptText & "												</tr>" & vbcrlf
		ReceiptText = ReceiptText & "											</table>" & vbcrlf
	end if
	ReceiptText = ReceiptText & "											</td>" & vbcrlf
	ReceiptText = ReceiptText & "										</tr>" & vbcrlf
	ReceiptText = ReceiptText & "									</table>" & vbcrlf
	ReceiptText = ReceiptText & "								</td>" & vbcrlf
	ReceiptText = ReceiptText & "							</tr>" & vbcrlf
	ReceiptText = ReceiptText & "						</table>" & vbcrlf
	ReceiptText = ReceiptText & 						strUpsell & vbcrlf	
	ReceiptText = ReceiptText & "						<table width=""640"" cellpadding=""0"" cellspacing=""0"">" & vbcrlf
	ReceiptText = ReceiptText & "							<tr>" & vbcrlf
	ReceiptText = ReceiptText & "								<td style=""padding:10px;font-size:11px;text-align:center;background-color:#ebebeb;border-top:2px solid #cccccc;font-family:Verdana, Helvetica, sans-serif;"">" & vbcrlf
	ReceiptText = ReceiptText & "									<p style=""margin:0;padding:10px 0 0;font-weight:bold;line-height:18px;"">Would You Like To Receive Special Coupons & Opportunities To Win Sweepstakes And Prizes?</p>" & vbcrlf
	ReceiptText = ReceiptText & "									<p style=""margin:0;padding:10px 0 0;font-weight:bold;line-height:18px;"">It's Easy! Simply Join Us On Social Media.</p>" & vbcrlf
	ReceiptText = ReceiptText & "									<p style=""text-align:center;margin:0;padding:10px 0;"">" & vbcrlf
	ReceiptText = ReceiptText & "										<a href=""https://www.facebook.com/cellularoutfitter""><img src=""http://www.cellularoutfitter.com/images/confirm-email/facebook.png"" width=""35"" height=""35"" style=""margin:10px;"" /></a>" & vbcrlf
	ReceiptText = ReceiptText & "										<a href=""https://twitter.com/celloutfitter""><img src=""http://www.cellularoutfitter.com/images/confirm-email/twitter.png"" width=""35"" height=""35"" style=""margin:10px;"" /></a>" & vbcrlf
	ReceiptText = ReceiptText & "										<a href=""https://plus.google.com/+Cellularoutfitter""><img src=""http://www.cellularoutfitter.com/images/confirm-email/google-plus.png"" width=""35"" height=""35"" style=""margin:10px;"" /></a>" & vbcrlf
	ReceiptText = ReceiptText & "										<a href=""http://www.pinterest.com/cellularoutfit""><img src=""http://www.cellularoutfitter.com/images/confirm-email/pinterest.png"" width=""35"" height=""35"" style=""margin:10px;"" /></a>" & vbcrlf
	ReceiptText = ReceiptText & "									</p>" & vbcrlf
	ReceiptText = ReceiptText & "									<p style=""margin:0;padding:10px 0 0;line-height:18px;"">If you have any questions, comments, or concerns, please <a href=""http://www.cellularoutfitter.com/contact-us.html?dzid=email_transactional_FOOTER_1L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" style=""color:#006596;font-weight:bold;text-decoration:underline;"">contact us</a>.</p>" & vbcrlf
	ReceiptText = ReceiptText & "									<p style=""margin:0;padding:10px 0 0;line-height:18px;"">Our customer service hours are Monday through Friday 6:00 A.M. - 6:00 P.M. Pacific Standard Time.</p>" & vbcrlf
	ReceiptText = ReceiptText & "									<p style=""margin:0;padding:10px 0 0;line-height:18px;"">This email correspondence has been sent to you as you have indicated you would like to receive special, exclusive offers from CellularOutfitter.com. If this message has been sent in error, or you would no longer like to receive any more periodic offers from CellularOutfitter.com, you may <a href=""http://www.cellularoutfitter.com/unsubscribe.html?dzid=email_transactional_FOOTER_2L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" style=""color:#006596;font-weight:bold;text-decoration:underline;"">unsubscribe</a>.</p>" & vbcrlf
	ReceiptText = ReceiptText & "									<p style=""margin:0;padding:10px 0 0;line-height:18px;""><a href=""http://www.cellularoutfitter.com/privacypolicy.html?dzid=email_transactional_FOOTER_3L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" style=""color:#006596;font-weight:bold;text-decoration:underline;"">Privacy Policy</a> | <a href=""http://www.cellularoutfitter.com/termsofuse.html?dzid=email_transactional_FOOTER_4L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" style=""color:#006596;font-weight:bold;text-decoration:underline;"">Terms & Conditions</a> | <a href=""http://www.cellularoutfitter.com/contact-us.html?dzid=email_transactional_FOOTER_5L_NULL_NONE&utm_source=internal&utm_medium=email&utm_campaign=Order&utm_content=Confirmation&utm_promocode=NA"" style=""color:#006596;font-weight:bold;text-decoration:underline;"">Contact Us</a></p>" & vbcrlf
	ReceiptText = ReceiptText & "									<p style=""margin:0;padding:10px 0 0;line-height:18px;"">1410 N. Batavia St. | Orange, CA 92867</p>" & vbcrlf
	ReceiptText = ReceiptText & "									<p style=""margin:0;padding:10px 0 0;line-height:18px;"">&copy; 2002-" & Year(Date) & " CellularOutfitter.com</p>" & vbcrlf
	ReceiptText = ReceiptText & "								</td>" & vbcrlf
	ReceiptText = ReceiptText & "							</tr>" & vbcrlf
	ReceiptText = ReceiptText & "						</table>" & vbcrlf
	ReceiptText = ReceiptText & "					</td>" & vbcrlf
	ReceiptText = ReceiptText & "				</tr>" & vbcrlf
	ReceiptText = ReceiptText & "			</table>" & vbcrlf
	ReceiptText = ReceiptText & "		</td>" & vbcrlf
	ReceiptText = ReceiptText & "	</tr>" & vbcrlf
	ReceiptText = ReceiptText & "</table>" & vbcrlf
end if
%>