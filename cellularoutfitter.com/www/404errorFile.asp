<%
'Causes Google to detect Crawl Errors
'Response.Clear()
'Response.Status = "404 Not Found"

'Soft 404
Response.Clear
Response.Status = "200 OK"

pageTitle = "File not Found � Error 404"
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<div style="padding:3.0em">
									<p>
										Sorry, but that product is no longer available.
									</p>
									<table width="300" border="0" cellspacing="0" cellpadding="0" bgcolor="#E32627" align="center">
										<form method="get" action="http://www.cellularoutfitter.com/search/search.asp">
										<tr>
											<td align="center" style="font-face:Arial,Helvetica;color:#ffffff;font-weight:bold;">Find it FAST with <em>POWER SEARCH</em>:</td>
										</tr>
										<tr>
											<td><img src="http://www.cellularoutfitter.com/images/spacer.gif" width="1" height="4"></td>
										</tr>
										<tr>
											<td align="center"><input name="query" type="text" class="inputbox" size="24"></td>
										</tr>
										<tr>
											<td><img src="http://www.cellularoutfitter.com/images/spacer.gif" width="1" height="4"></td>
										</tr>
										<tr>
											<td align="right"><input type="image" src="http://www.cellularoutfitter.com/images/search-button.gif" width="37" height="23" border="0"></td>
										</tr>
										</form>
									</table>
									<p>
										At CellularOutfitter.com, we are continually updating our vast inventory, so the product you are searching for may be unavailable at this time, or its name may have changed.
									</p>
									<p>
										Please use our convenient browse or search features to quickly find the product you need.
									</p>
									<p>
										If you still cannot find what you are looking for, please feel free to e-mail us at
										<a href="mailto:Support@CellularOutfitter.com">Support@CellularOutfitter.com</a>
									</p>
</div>
<!--#include virtual="/includes/template/bottom.asp"-->
