<%
pageTitle = "Contact Us"
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<!--#include virtual="/includes/asp/inc_Email_Validation.asp"-->
<table border="0" cellspacing="0" cellpadding="0" width="775">
    <tr>
        <td class="top-sublink-gray">
            <a class="top-sublink-gray" href="http://www.CellularOutfitter.com/">Cell Phone Accessories</a>&nbsp;>&nbsp;
            <span class="top-sublink-blue"><%=pageTitle%></span>
        </td>
    </tr>
    <tr>
        <td>
            <div style="float:left;"><img src="/images/banners/CO-header-left-contact.png" border="0" width="82" height="78" /></div>
            <div style="float:left; background-image:url(/images/banners/CO-header-middle.png); width:650px; height:58px; font-size:28px; font-weight:bold; color:#069; padding:20px 0px 0px 10px;">Contact Us</div>
            <div style="float:left;"><img src="/images/banners/CO-header-right.png" border="0" width="16" height="78" /></div>
        </td>
    </tr>
    <tr>
        <td valign="top" class="top-sublink-gray" width="541">
            <%
            if request.form("submitted") = "1" then
                dim cdo_to, cdo_from, cdo_subject, cdo_body
				subject = request.form("subject")
				orderID = prepStr(request.form("orderno"))
				if subject = "Where's My Stuff?" then
					cdo_to = "status@cellularoutfitter.com"
				elseif subject = "Problem With an Order" then
					cdo_to = "support@cellularoutfitter.com"
				elseif subject = "Cancel Items or Orders" then
					cdo_to = "support@cellularoutfitter.com"
				elseif subject = "Returns and Refunds" then
					cdo_to = "returns@cellularoutfitter.com"
				elseif subject = "Shipping Rates & Information" then
					cdo_to = "support@cellularoutfitter.com"
				elseif subject = "Product Questions" then
					cdo_to = "sales@cellularoutfitter.com"
				elseif subject = "Dealer Pricing Quotes" then
					cdo_to = "sales@cellularoutfitter.com"
				end if
				if orderID <> "" then
					useSubject = subject & " Order#" & orderID
				else
					useSubject = subject
				end if
                cdo_from = prepStr(request.form("email"))
                if IsValidEmail(cdo_from) = true then
                    cdo_subject = useSubject
                    cdo_body = "<p>Name: " & prepStr(request.form("name")) & "</p>" & vbcrlf
                    cdo_body = cdo_body & "<p>Phone: " & prepStr(request.form("phonenumber")) & "</p>" & vbcrlf
                    cdo_body = cdo_body & "<p>Type of Inquiry: " & prepStr(request.form("subject")) & "</p>" & vbcrlf
                    cdo_body = cdo_body & "<p>Order #: " & prepStr(request.form("orderno")) & "</p>" & vbcrlf
                    cdo_body = cdo_body & "<p>Product: " & prepStr(request.form("product")) & "</p>" & vbcrlf
                    cdo_body = cdo_body & "<p>Question: " & prepStr(replace(request.form("question"),vbcrlf,"<br>")) & "</p>" & vbcrlf
                    cdo_body = cdo_body & "<p>Date/Time Sent: " & now & "</p>" & vbcrlf
                    cdo_body = cdo_body & "<table width='500' border='0' cellspacing='3' cellpadding='3' align='center'>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td height='50' colspan='2'>&nbsp;</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='500'><table width='500' border='0' cellspacing='0' cellpadding='0'>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='207' height='50' align='center'><img src='/images/coLogo.gif'></td><td width='293'>&nbsp;</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td colspan='2'>&nbsp;</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td><table width='400' cellspacing='0' cellpadding='0' align='center'>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='100' align='left'>Name </td><td width='10'> : </td><td> " & prepStr(request.form("name")) & "</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='100' align='left'>E-Mail : </td><td width='10'> : </td><td> " & prepStr(request.form("email")) & "</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='100' align='left'>Type of Inquiry  </td><td width='10'> : </td><td> " & prepStr(request.form("subject")) & "</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='100' align='left'>Phone : </td><td width='10'> : </td><td> " & prepStr(request.form("phonenumber")) & "</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='100' align='left'>Product : </td><td width='10'> : </td><td> " & prepStr(request.form("product")) & "</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='100' align='left'>Order #  </td><td width='10'> : </td><td> " & prepStr(request.form("orderno")) & "</td></tr>" & vbcrlf
                    cdo_body = cdo_body & "<tr><td width='100' align='left'>Question : </td><td width='10'> : </td><td> " & prepStr(request.form("question")) & "</td>" & vbcrlf
                    cdo_body = cdo_body & "</tr></table></td>" & vbcrlf
                    cdo_body = cdo_body & "</tr></table></td>" & vbcrlf
                    cdo_body = cdo_body & "</tr></table>" & vbcrlf
'                    CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
                    CDOSend2 cdo_to,cdo_from,cdo_subject,cdo_body,"",""
'                    CDOSend2 cdo_to,cdo_from,cdo_subject,cdo_body,"","ruben@wirelessemporium.com"
                    %>
                    <p>
                        <strong>
                            <br><br>
                            Thank you for contacting CellularOutfitter.com!
                        </strong>
                    </p>
                    <p>
                        <strong>
                            A Representative from the appropriate department will contact you promptly.
                            If your inquiry is sent between normal business hours (8am-5pm PST M-F)
                            expect an answer in the next few hours or within one business day.
                        </strong>
                    </p>
                    <p>
                        <strong>
                            <br><br>
                            CellularOutfitter.com
                        </strong>
                    </p>
                    <%
                else
                    %>
                    <p>
                        <strong>
                            <br><br>
                            The e-mail address you entered was not valid.
                        </strong>
                    </p>
                    <p>
                        <strong>
                            Please <a href="javascript:history.back();">go back</a> and try again.
                        </strong>
                    </p>
                    <p>
                        <strong>
                            <br><br>
                            CellularOutfitter.com
                        </strong>
                    </p>
                    <%
                end if
            else
                %>
                <script language="javascript1.2">
                function reset() {
                    document.contactus.name.value="";
                    document.contactus.email.value="";
                    document.contactus.subject.value="";
                    document.contactus.phonenumber.value="";
                    document.contactus.product.value="";
                    document.contactus.orderno.value="";
                    document.contactus.question.value="";
                }
                
                function validateEmail(email) {
                    var splitted = email.match("^(.+)@(.+)$");
                    if (splitted == null) return false;
                    if (splitted[1] != null) {
                        var regexp_user=/^\"?[\w-_\.]*\"?$/;
                        if (splitted[1].match(regexp_user) == null) return false;
                    }
                    if (splitted[2] != null) {
                        var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
                        if (splitted[2].match(regexp_domain) == null) {
                            var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
                            if (splitted[2].match(regexp_ip) == null) return false;
                        }
                        return true;
                    }
                    return false;
                }
                
                function validate() {
                    if (document.contactus.name.value.length==0) {
                        alert("Please Enter Name");
                        return false;
                    }
                    if (document.contactus.subject.value=="Select here...") {
                        alert("Please select the type of Inquiry");
                        return false;
                    }
                    if (document.contactus.question.value.length==0) {
                        alert("Please Enter Question");
                        return false;
                    }
                    if (document.contactus.email.value.length==0) {
                        alert("Please Enter Email Address");
                        return false;
                    }
                    if (!validateEmail(document.contactus.email.value)) { 
                        alert("please enter valid Email address");
                        return false;
                    }
                    document.contactus.submit()
                    return false;
                }
                </script>
                
                <table width="700" border="0" align="left" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="100%" align="left" valign="top" class="static-content-font">
                            <p class="content-bold-blue" style="font-weight:bold; font-size:16px; color:#000;">
                                You have a question? We surely have the answer.
                            </p>
                            Have questions or comments regarding your order, products, usage, returns or anything else? 
                            Our team of Customer Service Associates at CellularOutfitter.com are standing by to assist 
                            in any way to ensure the highest level of service possible. Please contact us by providing 
                            the pertinent information below and your inquiry will be addressed IMMEDIATELY*.
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" align="left" valign="top" class="static-content-font">
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" align="left" valign="top">
                            <table width="100%" border="0" align="left" cellspacing="0">
                                <form onSubmit="return validate();" name="contactus" method="post">
                                <input type="hidden" name="submitted" value="1">
                                <tr>
                                    <td width="130" align="left" class="content-bold-blue"><font size="2">Your Name:</font><font color="#FF0000">*</font></td>
                                    <td width="20" align="right">&nbsp;</td>
                                    <td width="370">&nbsp;<input type="text" name="name" size="20" maxlength="100" value=""></td>
                                </tr>
                                <tr>
                                    <td align="left" class="content-bold-blue"><font size="2">Your Email:</font><font color="#FF0000">*</font></td>
                                    <td align="right">&nbsp;</td>
                                    <td>&nbsp;<input type="text" name="email" size="20" maxlength="100" value=""></td>
                                </tr>
                                <tr>
                                    <td align="left" class="content-bold-blue"><font size="2">Type of Inquiry:</font><font color="#FF0000">*</font></td>
                                    <td align="right">&nbsp;</td>
                                    <td>
                                        <select name="subject">
                                            <option selected>Select here...</option>
                                            <option>Where's My Stuff?</option>
                                            <option>Problem With an Order</option>
                                            <option>Cancel Items or Orders</option>
                                            <option>Returns and Refunds</option>
                                            <option>Shipping Rates & Information</option>
                                            <option>Product Questions</option>
                                            <option>Dealer Pricing Quotes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="content-bold-blue"><font size="2">Phone Number:</font></td>
                                    <td align="right">&nbsp;</td>
                                    <td>&nbsp;<input type="text" name="phonenumber" size="20" maxlength="20" value=""></td>
                                </tr>
                                <tr>
                                    <td align="left" class="content-bold-blue"><font size="2">Order Number:</font></td>
                                    <td align="right">&nbsp;</td>
                                    <td>&nbsp;<input type="text" name="orderno" size="8" maxlength="8" value=""></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="content-bold-blue"><font size="2">Question:</font><font color="#FF0000">*</font></td>
                                    <td align="right" valign="top">&nbsp;</td>
                                    <td>&nbsp;<textarea name="question" cols="45" rows="4"></textarea></td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="left" valign="top" class="static-content-font">
                                        <div style="padding:20px 0px 20px 0px; width:700px; text-align:center;"><input type="image" src="/images/buttons/button-submit.png" align="top" border="0" name="submit" alt="Click Here To Send"></div>
                                        <div style="width:700px; height:25px;">
                                            <div style="float:left; background-color:#ebebeb; border:1px solid #999; width:320px; padding:7px; height:80px; line-height:150%;">
                                                To ensure delivery of our emails to your inbox, please add support@CellularOutfitter.com to your Address Book.
                                                <br>Thank You.
                                            </div>
                                            <div style="float:left; background-color:#ebebeb; border:1px solid #999; width:320px; padding:7px; margin-left:20px; height:80px;">
                                                * Normal business hours are 8am-5pm PST M-F. If you contact us outside of normal business hours we will be sure 
                                                to address your inquiry within the next business day. <br />                                                                       
                                                If you would like to place an order by phone, <br /> please call 800-871-6926.
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </form>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom" style="font-size:11px;">
                        </td>
                    </tr>
                </table>
                <%
            end if
            %>
        </td>
    </tr>
</table>
<!--#include virtual="/includes/template/bottom.asp"-->
