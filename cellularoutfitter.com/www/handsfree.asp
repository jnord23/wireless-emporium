<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
dim brandID, brandName, modelName, modelImg
dim modelid
modelid = request.querystring("modelid")
session("curModelID") = modelID
if modelid = "" or not isNumeric(modelid) then
	response.redirect("/")
	response.end
end if

dim pageTitle
pageTitle = itemDesc_CO

dim unavailable, HandsfreeTypes
unavailable = 0

call fOpenConn()
dim SQL, RS
SQL = "SELECT A.*, b.brandid, B.brandName FROM we_models A INNER JOIN we_brands B ON A.brandID=B.brandID WHERE A.modelid = '" & modelid & "'"
'session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if RS.eof then
	unavailable = 1
else
	if isNull(RS("HandsfreeTypes")) then
		unavailable = 1
	else
		brandID = RS("brandid")
		brandName = RS("brandName")
		modelName = RS("modelName")
		modelImg = RS("modelImg")
		
		
		'=========================================================================================
		Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
			oParam.CompareMode = vbTextCompare
			oParam.Add "x_brandName", brandName
			oParam.Add "x_modelName", modelName
			oParam.Add "x_brandID", brandID
			oParam.Add "x_modelID", modelid

		call redirectURL("hf", modelid, request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
		'=========================================================================================		
		
		HandsfreeTypes = left(RS("HandsfreeTypes"),len(RS("HandsfreeTypes"))-1)
		
		SQL = "SELECT A.*, B.modelName, B.modelImg FROM we_items A INNER JOIN we_models B ON A.modelID = B.modelID"
		SQL = SQL & " WHERE A.HandsfreeType IN ('" & replace(HandsfreeTypes,",","','") & "')"
		SQL = SQL & " AND A.typeid = '5'"
		SQL = SQL & " AND A.hideLive = 0"
		SQL = SQL & " AND A.inv_qty <> 0"
		SQL = SQL & " AND A.price_CO IS NOT NULL AND A.price_CO > 0"
		SQL = SQL & " ORDER BY A.flag1 DESC"
		'session("errorSQL") = SQL
		'response.write "<p>" & SQL & "</p>" & vbcrlf
		set RS = Server.CreateObject("ADODB.Recordset")
		RS.open SQL, oConn, 0, 1
		if RS.eof then unavailable = 1
	end if
end if

dim categoryName
categoryName = "Hands-Free Kits &amp; Bluetooth Headsets"

if unavailable = 1 then
	call fCloseConn()
	server.transfer("404error.asp")
	response.end
end if

SEtitle = brandName & " " & modelName & " " & categoryName & " at Wholesale Prices"
SEdescription = "CellularOutfitter.com is the largest store online for discounted " & brandName & " " & modelName & " " & categoryName & " & other " & brandName & " " & modelName & " accessories at wholesale prices."
SEkeywords = brandName & " " & modelName & " Accessories, " & brandName & " " & modelName & " Cell Phone Accessories, " & brandName & " " & modelName & " " & categoryName & ", " & brandName & " " & modelName & " Cell Phone " & categoryName

dim strBreadcrumb
strBreadcrumb = brandName & " " & modelName & " " & categoryName
%>

<!--#include virtual="/includes/template/top.asp"-->
								<td width="5">&nbsp;</td>
								<td width="775" valign="top">
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td class="top-sublink-gray">
												<a class="top-sublink-gray" href="/">Cell Phone Accessories</a>&nbsp;>&nbsp;
												<a class="top-sublink-gray" href="/b-<%=brandID & "-" & formatSEO(brandName)%>-cell-phone-accessories.html"><%=brandName%> Cell Phone Accessories</a>&nbsp;>&nbsp;
												<a class="top-sublink-gray" href="/m-<%=modelID & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>-cell-phone-accessories.html"><%=brandName & " " & modelName%> Cell Phone Accessories</a>&nbsp;>&nbsp;
												<span class="top-sublink-blue"><%=brandName & " " & modelName & " " & categoryName%></span>
											</td>
										</tr>
										<tr>
											<td><img src="/images/spacer.gif" width="1" height="10" border="0"></td>
										</tr>
										<tr>
											<td>
												<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
													<tr>
														<td width="200" align="center" valign="top"><img src="/modelpics/<%=modelImg%>" border="0" alt="<%=brandName & " " & modelName & " " & categoryName%>"></td>
														<td width="575" align="center" valign="top">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" class="cate-pro-border">
																<tr>
																	<td>
																		<p class="top-text-grey">
																			You've selected <b><%=brandName & " " & modelName%></b> <%=categoryName%>. Find the right <%=categoryName%> for your <b><%=brandName & " " & modelName%></b> at wholesale prices. No membership required! We offer the <a href="/lowest-price.html">lowest prices online guaranteed</a>.
																		</p>
																		<p class="top-text-grey">
																			CellularOutfitter.com is your one-stop shop for the best cell phone accessories. Buy <%=brandName & " " & modelName & " " & categoryName%> at wholesale prices &amp; accessorize your phone today! We stand behind the quality of our products with an iron-clad 100% Quality Assurance Guarantee. We promise you will be amazed with each and every purchase you make. These are the best <%=brandName & " " & modelName & " " & categoryName%>, so go ahead and buy them right away!
																		</p>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td valign="middle" width="100%" height="68" style="background-image: url('/images/CO_header.jpg'); background-repeat: no-repeat; background-position: center bottom;">
												<h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=brandName & " " & modelName & " " & categoryName%></h1>
											</td>
										</tr>
										<tr>
											<td><img src="/images/spacer.gif" width="1" height="10" border="0"></td>
										</tr>
										<tr>
											<td align="left" valign="top">
												<table border="0" align="center" cellspacing="0" cellpadding="0" width="752">
													<tr>
														<%
														a = 0
														set fso = CreateObject("Scripting.FileSystemObject")
														do until RS.eof
															if fso.fileExists(server.MapPath("/productPics/thumb/" & RS("itempic_CO"))) then
																response.write "<td align=""center"" valign=""top"" width=""170""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""170""><tr><td align=""center"" valign=""middle"" class=""cate-pro-border"" width=""170"" height=""154"">" & vbcrlf
																response.write "<a href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc_CO")) & ".html""><img src=""/productPics/thumb/" & RS("itempic_CO") & """ border=""0"" alt=""" & RS("itemDesc_CO") & """></a></td></tr>" & vbcrlf
																response.write "<tr><td><img src=""/images/spacer.gif"" width=""1"" height=""5""></td></tr>" & vbcrlf
																response.write "<tr><td align=""left"" valign=""bottom"" height=""45""><a class=""pro-title-link"" href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc_CO")) & ".html"" title=""" & RS("itemDesc_CO") & """>" & RS("itemDesc_CO") & "</a></td></tr>" & vbcrlf
																response.write "<tr><td align=""left""><img src=""/images/spacer.gif"" width=""1"" height=""8""></td></tr>" & vbcrlf
																response.write "<tr><td align=""left"" class=""cat-font"">Retail Price : <s>" & formatCurrency(RS("price_retail")) & "</s></td></tr>" & vbcrlf
																response.write "<tr><td align=""left""><img src=""/images/spacer.gif"" width=""1"" height=""3""></td></tr>" & vbcrlf
																response.write "<tr><td align=""left"" class=""nevi-font-bold"">Wholesale Price : <span class=""red-price-small"">" & formatCurrency(RS("price_CO")) & "</span></td></tr>" & vbcrlf
																response.write "<tr><td align=""center"" valign=""bottom"" style=""padding-top:5px;""><a href=""/p-" & RS("itemid") & "-" & formatSEO(RS("itemDesc_CO")) & ".html""><img src=""/images/moreInfo.png"" width=""94"" height=""20"" border=""0"" alt=""More Info""></a></td></tr>" & vbcrlf
																response.write "<tr><td valign=""top"">&nbsp;</td></tr></table></td>" & vbcrlf
																RS.movenext
																a = a + 1
																if a = 4 then
																	response.write "</tr><tr>" & vbcrlf
																	a = 0
																end if
															else
																RS.movenext
															end if
														loop
														fso = null
														if a = 1 then
															response.write "<td width=""170"">&nbsp;</td><td width=""170"">&nbsp;</td><td width=""170"">&nbsp;</td>" & vbcrlf
														elseif a = 2 then
															response.write "<td width=""170"">&nbsp;</td><td width=""170"">&nbsp;</td>" & vbcrlf
														elseif a = 3 then
															response.write "<td width=""170"">&nbsp;</td>" & vbcrlf
														end if
														%>
													</tr>
													<tr>
														<td valign=top>&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>
										<!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
										<tr>
											<td>&nbsp;</td>
										</tr>
									</table>
								</td>
<%
call fCloseConn()
%>
<!--#include virtual="/includes/template/bottom.asp"-->