# /robots.txt file for http://www.cellularoutfitter.com/
# mail webmaster@cellularoutfitter.com with questions or comments

# subdirectory lockouts for regular "www" folder

User-agent: ahrefs
Disallow: /
User-agent: majestic12
Disallow: /
User-agent: yandex
Disallow: /

Crawl-delay: 20

User-agent: *
Disallow: /account/
Disallow: /admin/
Disallow: /ajax/
Disallow: /App_Data/
Disallow: /App_Readme/
Disallow: /aspnet_client/
Disallow: /bin/
Disallow: /cart/
Disallow: /compressedPages/
Disallow: /Content/
Disallow: /images/
Disallow: /includes/
Disallow: /Scripts/
Disallow: /ssl/
Disallow: /productpics/
Disallow: /tempCSV/
Disallow: /Views/
Disallow: /confirm.asp
Crawl-delay: 20

User-agent: Googlebot-Image
Allow: /productpics/
Crawl-delay: 20

User-agent: bingbot
Allow: /productpics/
Crawl-delay: 20

Sitemap: http://www.cellularoutfitter.com/sitemap.xml