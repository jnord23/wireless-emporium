<%
pageTitle = "Track Your Order"
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<link rel="stylesheet" type="text/css" href="/includes/css/orderstatus.css" />
<link rel="stylesheet" type="text/css" media="print" href="/includes/css/orderstatusPrint.css" />
<%
	txtorderid = prepInt(request.form("order-number"))
	email = prepStr(request.form("email"))
	zipcode = prepStr(request.form("zip"))
	submitted = prepStr(request.form("submitted"))
	strMsg = ""

	BOItemCount = 0
	reshipItemCount = 0
	
	if submitted = "Y" then
		sql	=	"exec getCustomerOrderInfo 2, " & txtorderid & ", '" & email & "', '" & zipcode & "'"
		set rs = oConn.execute(sql)
		if not rs.eof then
			orderid = rs("orderid")
			fname = rs("fname")
			lname = rs("lname")
			email = rs("email")
			bAddress = rs("baddress1") & " " & rs("baddress2")
			bCity = rs("bCity")
			bState = rs("bState")
			bZip = rs("bzip")
			sAddress = rs("saddress1") & " " & rs("saddress2")
			sCity = rs("sCity")
			sState = rs("sState")
			sZip = rs("szip")
			scandate = rs("scandate")
			rmastatus = rs("rmastatus")
			trackingnum = rs("trackingNum")
			shiptype = replace(rs("shiptype"), " - FREE", "")
		else
			strMsg = "<div style=""color:#f00; font-size:16px;"">We cannot find your order in our system!</div>"
		end if
	end if
%>
<script>
	if(typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, ''); 
		}
	}

	$(document).ready(function(){
		$("#zip").focus(function(){
			if($(this).val() == "Enter your zip code") {
				$(this).val("");
			}
		});
		
		$("#email").focus(function(){
			if($(this).val() == "Enter your email address") {
				$(this).val("");
			}
		});
		
		$("#order-number").focus(function(){
			if($(this).val() == "Enter your order number") {
				$(this).val("");
			}
		});
		
		$("#zip").focusout(function(){
			if($(this).val() == "") {
				$(this).val("Enter your zip code");
			}
		});
		
		$("#email").focusout(function(){
			if($(this).val() == "") {
				$(this).val("Enter your email address");
			}
		});
		
		$("#order-number").focusout(function(){
			if($(this).val() == "") {
				$(this).val("Enter your order number");
			}
		});
		
		$("#track-your-order").click(function(){					
			$(".error-msg").remove();
			$("#zip, #order-number, #email").removeAttr("class");
			if($("#order-number").val() != "Enter your order number") {
				if (!checkOrderID($("#order-number").val())) {
					$("#order-number").attr("class", "error");
					$("#order-number").after("<span class=\"error-msg\">PLEASE ENTER A VALID ORDER NUMBER.</span>");
					return false;
				} else {
					$("#order-number").removeAttr("class");
					$(".error-msg").remove();
					
					document.frmTrack.submitted.value = "Y";
					return true;
				}	
			} else {
				if (!checkZipCode($("#zip").val())) {
					$("#zip").attr("class", "error");
					$("#zip").after("<span class=\"error-msg\">PLEASE ENTER A VALID ZIP CODE.</span>");
					return false;
				} else {
					$("#zip").removeAttr("class");
					$(".error-msg").remove();
				}
					
				if(!checkEmail($("#email").val())) {
					$("#email").attr("class", "error");
					$("#email").after("<span class=\"error-msg\">PLEASE ENTER A VALID EMAIL ADDRESS.</span>");
					return false;
				} else {
					$("#email").removeAttr("class");
					$(".error-msg").remove();
				}
				document.frmTrack.submitted.value = "Y";
				return true;
			}
		});
		
		function checkOrderID(orderid) {
			orderid = orderid.trim(); 
			if (isNaN(orderid)||(orderid=='')) return false;
			return true;
		}
		
		function checkZipCode(zip) {
			zip = zip.trim(); 
			if (isNaN(zip)||(zip=='')) return false;
			return true;
		}
		
		function checkEmail(email) {
			email = email.trim();
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			
			if (!filter.test(email)) {
				return false;
			}
			return true;
		}
	});
	
	function sendContactUs() {
		txtContactEmail = document.frmContactus.txtContactEmail.value;
		txtMessage = document.frmContactus.txtMessage.value;

		if (!checkEmail(txtContactEmail)) {
			alert('Please enter a valid email address');
			return false;
		}

		if ((txtMessage=='')||(txtMessage=='Enter your message')) {
			alert('Please enter your message');
			return false;
		}

		url = '/ajax/getContactusPopup.asp?orderid=<%=orderid%>&txtContactEmail=' + escape(txtContactEmail) + '&txtMessage=' + escape(txtMessage) + '&submitted=Y'
		ajax(url,'popBox');

		return true;
	}
	
	function doContactus() {
		document.getElementById("popBox").innerHTML = '';
		ajax('/ajax/getContactusPopup.asp?orderid=<%=orderid%>&txtContactEmail=<%=email%>','popBox');
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	
	function closeContactUsPopup() {
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
	}
</script>
<div style="width:815px; font-size:12px; padding-top:5px;">
	<div id="breadCrumb" style="width:100%; padding:0px 0px 5px 0px; border-bottom:1px solid #ccc; text-align:left; color:#333;"><a style="color:#333;" href="/index.asp">Home</a>&nbsp;/&nbsp;Order Status</div>
    <%if strMsg <> "" then%>
	<div style="width:100%; padding:10px 0px 10px 0px; border-bottom:1px solid #ccc; color:#333; font-size:16px; text-align:center;"><%=strMsg%></div>
    <%end if%>
	<div style="width:100%; padding-top:10px;">
        <div style="width:100%; background:url(/images/orderstatus/Header-BlueBar-Big.png) repeat-x; height:170px; border-top-left-radius:5px; border-top-right-radius:5px; position:relative;">
        	<div id="ribbon" style="position:absolute; top:-5px; left:20px;"><img src="/images/orderstatus/Ribbon.png" border="0" /></div>
			<%if orderid <> "" then%>
            <div style="padding:50px 0px 0px 200px; text-align:left; font-weight:bold; font-size:40px; color:#fff;">Details for Order #<%=orderid%></div>
            <%else%>
            <div style="padding:50px 0px 0px 200px; text-align:left; font-weight:bold; font-size:40px; color:#fff;">Check Your Order Status</div>
            <%end if%>
            <div style="padding-left:200px; text-align:left; font-size:22px; color:#fff;">Order Information is updated in real-time.</div>
        </div>
    </div>
    <%if submitted = "Y" and orderid <> "" then%>
    <div class="darkGreenRoundBox" style="float:left; width:778px; padding:15px; text-align:left; margin:30px 0px 10px 0px;">
    <%if isnull(scandate) then%>
        <div style="float:left; padding-right:20px;"><img src="/images/orderstatus/CheckMark-MidGreen-Bar.png" border="0" /></div>
        <div style="float:left; color:#fff; font-size:20px;">Your order will be shipped via <%=shiptype%>.</div>
    <%else%>
        <%if BOItemCount <> 0 and (BOItemCount-reshipItemCount) > 0 then%>
            <div style="float:left; padding-right:20px;"><img src="/images/orderstatus/OrderPartiallyShipped-Icon.png" border="0" /></div>
            <div style="float:left; color:#fff; font-size:20px;">Your order was <i>partially</i> shipped on <strong><%=scandate%> PST.</strong> via <%=shiptype%>.</div>                
        <%else%>
            <div style="float:left; padding-right:20px;"><img src="/images/orderstatus/CheckMark-MidGreen-Bar.png" border="0" /></div>
            <div style="float:left; color:#fff; font-size:20px;">Your order was shipped on <strong><%=scandate%> PST.</strong> via <%=shiptype%>.</div>
        <%end if%>
    <%end if%>
    </div>
    <%end if%>
	<div style="width:795px; margin-top:10px; padding:10px; background-color:#f7f7f7; display:table;">
    <%if submitted = "Y" and orderid <> "" then%>
        <div style="width:100%; padding:10px 0px 20px 0px;" align="center">
            <div style="float:left; width:100%; padding:20px 0px 20px 0px;">
                <div style="float:left; font-size:28px; font-weight:bold; color:#333; padding-left:20px;">Your Order Details:</div>
                <div style="float:right;">
                	<%if not isnull(scandate) then%>
                    <div style="float:right;">
                        <div style="float:left; padding-right:3px;"><a href="" target="_blank"><img src="/images/orderstatus/icon-tracking-location.png" border="0" /></a></div>
                        <div style="float:left;"><a href="<%=getTrackingLink(0, orderid, trackingnum, scandate)%>" target="_blank" style="color:#333; font-size:14px; text-decoration:none;">Track this order</a></div>
                    </div>
                    <%end if%>
                    <div style="float:right; padding-right:15px;">
                        <div style="float:left; padding-right:3px;"><a href="" target="_blank"><img src="/images/orderstatus/icon-print.png" border="0" /></a></div>
                        <div style="float:left;"><a href="javascript:window.print()" style="color:#333; font-size:14px; text-decoration:none;">Print this order</a></div>
                    </div>
                </div>
            </div>
            <div style="float:left; width:100%; padding:20px 0px 20px 0px;">
                <div style="float:left; width:10%; text-align:left;">&nbsp;</div>
                <div style="float:left; width:40%; text-align:left; color:#333; font-size:16px; line-height:22px;">
                	<strong>Shipping to:</strong><br />
                    <%=fname & " " & lname%><br />
                    <%=sAddress%><br />
                    <%=sCity%>, <%=sState%>&nbsp;<%=sZip%>
                </div>
                <div style="float:left; width:10%; text-align:left;">&nbsp;</div>
                <div style="float:left; width:40%; text-align:left; color:#333; font-size:16px; line-height:22px;">
                	<strong>Billing to:</strong><br />
                    <%=fname & " " & lname%><br />
                    <%=bAddress%><br />
                    <%=bCity%>, <%=bState%>&nbsp;<%=bZip%>
                </div>
            </div>
            <div style="float:left; width:100%; padding-top:20px;">
            	<div style="float:left; width:100%; border-bottom:1px solid #ccc; padding:10px 0px 10px 0px;">
                    <div style="float:left; width:335px; color:#333; font-size:16px; font-weight:bold; text-align:left;"> &nbsp; &nbsp; Item</div>
                    <div style="float:left; width:200px; color:#333; font-size:16px; font-weight:bold; text-align:left;">Status</div>
                    <div style="float:left; width:150px; color:#333; font-size:16px; font-weight:bold; text-align:center;">Quantity</div>
                    <div style="float:left; width:100px; color:#333; font-size:16px; font-weight:bold; text-align:right; padding-right:8px;">Total</div>
                </div>
			<%
			set rs = rs.nextrecordset
			if not rs.eof then
				do until rs.eof
					itempic = rs("itempic_co")
					itemdesc = rs("itemdesc_co")
					quantity = rs("quantity")
					price_our = rs("price_co")
					boID = rs("backOrderID")
					boCancelDate = rs("backorderCancelDate")
					boProcessDate = rs("backorderProcessDate")
					boQty = rs("backorderQty")
					boChildOrderID = rs("childOrderID")
					if not isnull(rs("parentOrderID")) then
						boTrackingNum = rs("trackingnum")
					end if
				%>
				<div style="float:left; width:100%; border-bottom:1px solid #ccc; padding:10px 0px 10px 0px;">
					<div style="float:left; width:335px; text-align:left;">
						<div style="float:left; width:100px; margin-right:15px;"><img src="/productpics/thumb/<%=itempic%>" border="0" width="100" /></div>
						<div style="float:left; width:205px; margin-right:15px; color:#333; font-size:14px; font-weight:normal;"><%=itemdesc%></div>
					</div>
					<div style="float:left; width:200px; color:#333; font-size:14px; font-weight:normal; text-align:left;">
					<%if not isnull(scandate) then%>
						<%if not isnull(boID) then%>
							<div style="float:left; padding:5px; background:url(/images/orderstatus/Backordered-Element.png) no-repeat; width:193px; height:34px;">
								<div style="padding:4px 0px 0px 40px; font-weight:bold; font-size:15px; padding-right:5px; color:#fff;">Backordered item.</div>
							</div>
							<%if not isnull(boCancelDate) then%>
							<div style="float:left; width:100%; padding-top:10px; text-align:left; color:#cd0000; font-size:15px;">
								This backorder cancelled on <%=boCancelDate%> PST.
							</div>
							<%elseif not isnull(boProcessDate) and boTrackingNum <> "" then%>
							<div style="float:left; width:100%; padding-top:10px; text-align:left; color:#0268ce; font-size:15px;">
								This backorder processed on <%=boProcessDate%> PST.
							</div>
							<div style="float:left; width:100%; padding-top:10px; text-align:left; color:#0268ce; font-size:15px;">
								<div style="float:left; padding-right:3px;"><a href="<%=getTrackingLink(0, boChildOrderID, boTrackingNum, boProcessDate)%>" target="_blank"><img src="/images/orderstatus/icon-tracking-location.png" border="0" /></a></div>
								<div style="float:left;"><a href="<%=getTrackingLink(0, boChildOrderID, boTrackingNum, boProcessDate)%>" target="_blank" style="color:#ff6600; font-size:14px; text-decoration:none;">Track this backorder item</a></div>
							</div>
							<%end if%>
							<div style="float:left; width:100%; padding-top:10px;">
								<div style="float:left; padding:3px 5px 0px 0px;"><a href="javascript:javascript:doContactus()"><img src="/images/orderstatus/ContactUsAboutThis-Icon.png" border="0" /></a></div>
								<div style="float:left;"><a href="javascript:javascript:doContactus()" style="color:#046590; font-size:15px; text-decoration:underline;">Contact us about this.</a></div>
							</div>
						<%else%>
							<div style="float:left; padding:5px; background:url(/images/orderstatus/ThisOrderShipped-CheckBar.png) no-repeat; width:193px; height:34px;">
								<div style="padding:4px 0px 0px 40px; font-weight:bold; font-size:15px; padding-right:5px; color:#fff;">This item shipped!</div>
							</div>
						<%end if%>
					<%else%>
						<%if not isnull(boID) then%>
							<div style="float:left; padding:5px; background:url(/images/orderstatus/Backordered-Element.png) no-repeat; width:193px; height:34px;">
								<div style="padding:4px 0px 0px 40px; font-weight:bold; font-size:15px; padding-right:5px; color:#fff;">Backordered item.</div>
							</div>
							<div style="float:left; width:100%; padding-top:10px;">
								<div style="float:left; padding:3px 5px 0px 0px;"><a href="javascript:doContactus()"><img src="/images/orderstatus/ContactUsAboutThis-Icon.png" border="0" /></a></div>
								<div style="float:left;"><a href="javascript:doContactus()" style="color:#046590; font-size:15px; text-decoration:underline;">Contact us about this.</a></div>
							</div>
						<%else%>
							<div style="float:left; padding:5px; background:url(/images/orderstatus/ThisOrderShipped-CheckBar.png) no-repeat; width:193px; height:34px;">
								<div style="padding:4px 0px 0px 40px; font-weight:bold; font-size:15px; padding-right:5px; color:#fff;">Ready to ship!</div>
							</div>
						<%end if%>                    
					<%end if%>
					</div>
					<div style="float:left; width:150px; color:#333; font-size:14px; font-weight:bold; text-align:center;">
						<%=formatnumber(quantity, 0)%><br />
						<%if boQty > 0 then%>
						(Backordered: <%=boQty%>)
						<%end if%>
					</div>
					<div style="float:left; width:100px; color:#333; font-size:14px; font-weight:bold; text-align:right; padding-right:8px;"><%=formatcurrency(quantity*price_our)%></div>
				</div>
				<%
					rs.movenext
				loop
			end if
            %>
            </div>
            <div style="clear:both; height:30px;"></div>
            <div style="width:697px; height:80px; background:url(/images/orderstatus/OrderHasntShippedInfo-Bar.png) no-repeat;">
            	<div style="font-size:16px; font-weight:bold; color:#fff; padding:30px 0px 0px 50px;">If your order hasn't arrived within 4-10 business days, please <a href="javascript:doContactus();" style="color:#5fb6fd; font-size:16px; font-weight:bold;">contact us</a>.</div>
            </div>
            <div style="clear:both; height:20px;"></div>
			<div id="helpcenterHeader" style="width:100%;" align="center">
            	<div style="background:url(/images/orderstatus/HelpCenter-Bar.png) no-repeat; width:258px; height:33px; font-weight:bold; font-size:24px; padding-top:10px;">Help Center</div>
            </div>
			<div id="helpcenterBody" style="width:100%; background-color:#fff; display:table; padding-bottom:10px;">
            	<div style="width:100%; text-align:left; padding:10px 0px 10px 0px; text-align:center; color:#222;">If you have any questions or concerns regarding your order, please contact our Customer Support Team.</div>
            	<div style="width:100%; text-align:left;">
                	<div style="float:left; padding:30px 10px 10px 15px;"><img src="/images/orderstatus/Phone-Icon.png" border="0" /></div>
                	<div style="float:left; padding:30px 0px 0px 0px; border-right:1px solid #ccc; width:200px; height:70px;">
                    	<div style="font-weight:bold; font-size:18px;">1-800-871-6926</div>
                    	<div>Available Monday - Friday 8 am to 5 pm PST.</div>
                    </div>
                	<div style="float:left; padding:30px 10px 10px 10px;"><img src="/images/orderstatus/Email-Icon.png" border="0" /></div>
                	<div style="float:left; padding:30px 0px 0px 0px; border-right:1px solid #ccc; width:200px; height:70px;">
                    	<div style="font-weight:bold; font-size:18px;">Want to email us?</div>
                    	<div>Send us an email anytime 24/7.<br /><a href="javascript:doContactus();">Email us here.</a></div>
                    </div>
                	<div style="float:left; padding:30px 10px 10px 10px;">
                        <div>
                            <script type="text/javascript">
                                var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                                var lhnid = 3410;
                                var lhnwindow = 6755;
                                var lhnImage = "<img src='/images/orderstatus/LiveChat-Icon.png' border='0' />";
                                document.write(unescape("%3Cscript src='/framework/utility/liveHelp.js' type='text/javascript'%3E%3C/script%3E"));
                            </script>
                        </div>
					</div>
                	<div style="float:left; padding:30px 0px 0px 0px; width:200px; height:70px;">
                    	<div style="font-weight:bold; font-size:18px;">Live Chat Help</div>
                    	<div>Need real-time support?</div>
                        <div>
                            <script type="text/javascript">
                                var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                                var lhnid = 3410;
                                var lhnwindow = 6755;
                                var lhnImage = "<div>Chat with us</div>";
                                document.write(unescape("%3Cscript src='/framework/utility/liveHelp.js' type='text/javascript'%3E%3C/script%3E"));
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <%else%>
		<!-- track you order submit start -->
    	<div style="width:100%; padding-top:20px;" align="center">
	        <form name="frmTrack" method="POST">
            <div class="form">
                <div class="track-zip-email">
                    <div class="track-title">Track by Zip Code and Email Address:</div>
                    <div class="field-label">Zip Code: <span class="asterisk">*</span></div>
                    <div class="field">
                        <input type="text" name="zip" id="zip" value="Enter your zip code" />
                    </div>
                    <div class="field-label">Email Address: <span class="asterisk">*</span></div>
                    <div class="field">
                        <input type="text" name="email" id="email" value="Enter your email address" />
                    </div>
                </div>
                <div class="track-order-number">
                    <div class="track-title">Track by Order Number:</div>
                    <div class="field-label">Order Number: <span class="asterisk">*</span></div>
                    <div class="field">
                        <input type="text" name="order-number" id="order-number" value="Enter your order number" />
                    </div>
                </div>
                <div class="submit-container">
                    <input type="submit" id="track-your-order" class="track-your-order" value="TRACK YOUR ORDER" />
                </div>
                <div class="divider"><div class="subdivider"></div></div>
                <div class="shipping-notes">All shipment and arrival times are approximate.<br />Cellular Outfitter does not expressly warrant or guarantee arrival dates.</div>
                <div class="info">If your order hasn't arrived within 4-10 business days, please <a href="javascript:doContactus();" style="font-size:16px;">contact us</a>.</div>
                <div class="shipping-logos">
                    <div class="label">Our Shipping Providers:</div>
                    <div class="ups"></div>
                    <div class="usps"></div>
                </div>
                <div class="help-center"></div>
            </div>		
            
            <div style="clear:both; height:20px;"></div>
			<div id="helpcenterHeader" style="width:100%;" align="center">
            	<div style="background:url(/images/orderstatus/HelpCenter-Bar.png) no-repeat; width:258px; height:33px; font-weight:bold; font-size:24px; padding-top:10px;">Help Center</div>
            </div>
			<div id="helpcenterBody" style="width:100%; background-color:#fff; display:table; padding-bottom:10px;">
            	<div style="width:100%; text-align:left; padding:10px 0px 10px 0px; text-align:center; color:#222;">If you have any questions or concerns regarding your order, please contact our Customer Support Team.</div>
            	<div style="width:100%; text-align:left;">
                	<div style="float:left; padding:30px 10px 10px 15px;"><img src="/images/orderstatus/Phone-Icon.png" border="0" /></div>
                	<div style="float:left; padding:30px 0px 0px 0px; border-right:1px solid #ccc; width:200px; height:70px;">
                    	<div style="font-weight:bold; font-size:18px;">1-800-871-6926</div>
                    	<div>Available Monday - Friday 8 am to 5 pm PST.</div>
                    </div>
                	<div style="float:left; padding:30px 10px 10px 10px;"><img src="/images/orderstatus/Email-Icon.png" border="0" /></div>
                	<div style="float:left; padding:30px 0px 0px 0px; border-right:1px solid #ccc; width:200px; height:70px;">
                    	<div style="font-weight:bold; font-size:18px;">Want to email us?</div>
                    	<div>Send us an email anytime 24/7.<br /><a href="javascript:doContactus();">Email us here.</a></div>
                    </div>
                	<div style="float:left; padding:30px 10px 10px 10px;">
                        <div>
                            <script type="text/javascript">
                                var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                                var lhnid = 3410;
                                var lhnwindow = 6755;
                                var lhnImage = "<img src='/images/orderstatus/LiveChat-Icon.png' border='0' />";
                                document.write(unescape("%3Cscript src='/framework/utility/liveHelp.js' type='text/javascript'%3E%3C/script%3E"));
                            </script>
                        </div>
					</div>
                	<div style="float:left; padding:30px 0px 0px 0px; width:200px; height:70px;">
                    	<div style="font-weight:bold; font-size:18px;">Live Chat Help</div>
                    	<div>Need real-time support?</div>
                        <div>
                            <script type="text/javascript">
                                var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                                var lhnid = 3410;
                                var lhnwindow = 6755;
                                var lhnImage = "<div>Chat with us</div>";
                                document.write(unescape("%3Cscript src='/framework/utility/liveHelp.js' type='text/javascript'%3E%3C/script%3E"));
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear:both;"></div>
            <input type="hidden" name="submitted" value="" />
    	    </form>
        </div>    
    <%end if%>
    </div>
</div>
<%
function getTrackingLink(siteid, ordernum, trackingnum, scandate)
	strTrackingLink = ""
	strTrackingNum = ""
	numTrackingOption = 0
	
	if left(trackingnum,2) = "1Z" then
		strTrackingNum = trackingnum
		numTrackingOption = 1
	elseif left(trackingnum,2) = "EO" or left(trackingnum,2) = "91" or left(trackingnum,2) = "CJ" or left(trackingnum,2) = "LN" then
		strTrackingNum = trackingnum
		numTrackingOption = 2
	elseif left(trackingnum,2) = "94" then
		strTrackingNum = trackingnum
		numTrackingOption = 5
	elseif left(trackingnum,2) = "04" or left(trackingnum,1) = "9" then
		strTrackingNum = trackingnum
		numTrackingOption = 3
	elseif left(trackingnum,1) = "4" then
		strTrackingNum = trackingnum
		numTrackingOption = 5
	elseif not isnull(scandate) then
		if siteid = "0" or siteid = "2" then
			strTrackingNum = "WE" & ordernum
			numTrackingOption = 3
		else	'MI
			strTrackingNum = "WE" & ordernum
			numTrackingOption = 4
		end if
	elseif left(trackingnum,2) = "D1" then
		strTrackingNum = trackingnum
		numTrackingOption = 7
	end if
	
	select case numTrackingOption
		case 1 : strTrackingLink = "http://wwwapps.ups.com/WebTracking/OnlineTool?InquiryNumber1=" & strTrackingNum
		case 2 : strTrackingLink = "https://tools.usps.com/go/TrackConfirmAction!execute.action?formattedLabel=" & strTrackingNum
		case 3 : strTrackingLink = "http://webtrack.dhlglobalmail.com/?trackingnumber=" & strTrackingNum
		case 4 : strTrackingLink = "http://www.ups-mi.net/packageID/PackageID.aspx?PID=" & strTrackingNum
		case 5 : strTrackingLink = "https://tools.usps.com/go/TrackConfirmAction!execute.action?formattedLabel=" & strTrackingNum
		case 6 : strTrackingLink = "http://www.shipmentmanager.com/Portal.aspx?MerchantID=" & newgisticsID & "&TargetPageID=38&TrackingKey=BID&TrackingIdentifier=" & strTrackingNum
		case 7 : strTrackingLink = "http://www.ontrac.com/trackingres.asp?tracking_number=" & strTrackingNum
	end select
	
	getTrackingLink = strTrackingLink
end function
%>
    
<!--#include virtual="/includes/template/bottom.asp"-->
