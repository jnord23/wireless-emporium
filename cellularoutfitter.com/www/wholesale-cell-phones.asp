<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
siteId = 2

dim categoryid
dim carrierid
carrierid = request.querystring("carrierid")
categoryid = 16
if carrierid <> "" then
	dim URL
	call fOpenConn()
	SQL = "SELECT * FROM we_Carriers WHERE id='" & carrierID & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then URL = "/car-" & carrierID & "-" & formatSEO(RS("carrierName")) & "-accessories.asp"
	RS.close
	set RS = nothing
	call fCloseConn()
	if URL = "" then
		response.redirect("/index.asp")
		response.end
	else
		response.Status = "301 Moved Permanently"
		response.AddHeader "Location", URL
		response.end
	end if
end if

call fOpenConn()
SQL = "SELECT DISTINCT A.brandName, A.brandID FROM we_brands A INNER JOIN we_items B ON A.brandID = B.brandID"
SQL = SQL & " WHERE B.typeid = '" & categoryid & "'"
SQL = SQL & " AND B.hideLive = 0"
SQL = SQL & " AND B.inv_qty <> 0"
SQL = SQL & " AND B.price_CO IS NOT NULL AND B.price_CO > 0"
SQL = SQL & " AND A.brandName <> 'Universal'"
SQL = SQL & " ORDER BY A.brandName"
'session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if RS.eof then
	call fCloseConn()
	response.redirect("/index.asp")
	response.end
end if

dim SEtitle, SEdescription, SEkeywords, topText, bottomText, catText, H1
sql	=	"select	seTitle, seDescription, seKeywords, topText, bottomText, text1, h1tag" & vbcrlf & _
		"from	xcategorytext" & vbcrlf & _
		"where	site_id = 2" & vbcrlf & _
		"	and	categoryID = '" & categoryid & "'" & vbcrlf 
session("errorSQL") = sql
set objRsSEO = oConn.execute(sql)

if not objRsSEO.eof then
	SEtitle			=	objRsSEO("seTitle")
	SEdescription	=	objRsSEO("seDescription")
	SEkeywords		=	objRsSEO("seKeywords")
	topText			=	objRsSEO("topText")
	bottomText		=	objRsSEO("bottomText")
	catText			=	objRsSEO("text1")
	H1				=	objRsSEO("h1tag")
end if
objRsSEO = null

dim strBreadcrumb, headerImgAltText
strBreadcrumb = "Wholesale Cell Phones"
headerImgAltText = "Wholesale Cell Phones"
%>

<!--#include virtual="/includes/template/top_index.asp"-->
<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td class="top-sublink-gray">
            <a class="top-sublink-gray" href="http://www.CellularOutfitter.com/">Cell Phone Accessories</a>&nbsp;>&nbsp;
            <span class="top-sublink-blue">Wholesale Cell Phones</span>
        </td>
    </tr>
    <tr>
        <td width="100%" style="padding-top:5px;" title="<%=headerImgAltText%>">
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td style="border-bottom:1px solid #ccc; padding-bottom:5px;"><h1><%=seH1%></h1></td>
                    <td rowspan="2" valign="middle" align="right" width="200"><img src="/images/types/CO_CAT_banner_wholesale-cell-phone.jpg" border="0" width="190" height="119" alt="<%=headerImgAltText%>" title="<%=headerImgAltText%>"></td>
                </tr>
                <tr>
                    <td>
                    	<%
						if prepStr(SeTopText) <> "" then topText = SeTopText
						%>
                        <p class="static-content-font" style="text-align:justify;"><%=topText%></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="100%" align="center" style="padding:20px 0px 20px 0px;">
            <div style="width:750px; height:39px;" title="Please Select Your Phone Brand">
                <div style="float:left; height:39;">
                    <img src="/images/types/border_left.jpg" width="11" height="39" border="0" />
                </div>
                <div style="float:left; height:39px; background: url('/images/types/border_middle.jpg') repeat-x; width:728px; font-size:22px; padding-top:5px;" align="center">
                    <strong>Please Select Your Phone Brand</strong>
                </div>
                <div style="float:left; height:39;">
                    <img src="/images/types/border_right.jpg" width="11" height="39" border="0" />												
                </div>
            </div>	
        </td>
    </tr>
    <tr>
        <td align="center" width="100%">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <%
                    a = 0
                    dim altText, others
                    others = 0
                    do until RS.eof
						if RS("brandID") = 15 then
							others = 1
						else
	                        altText = RS("brandName") & catText
                        %>
                        <td align="center" valign="top" width="190">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td align="center" valign="middle">
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td align="center" valign="middle"><a href="/cp-sb-<%=RS("brandID") & "-" & formatSEO(RS("brandName"))%>-wholesale-cell-phones.html" title="<%=altText%>"><img src="http://www.cellularoutfitter.com/images/brands/CO_CATpage_buttons_<%=formatSEO(RS("brandName"))%>.jpg" border="0" width="165" height="63" alt="<%=altText%>" title="<%=altText%>"></a></td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="bottom">
                                                    <a href="/cp-sb-<%=RS("brandID") & "-" & formatSEO(RS("brandName"))%>-wholesale-cell-phones.html" title="<%=altText%>" class="cellphone-category-font-link"><%=RS("brandName") & catText%></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <%
    	                    a = a + 1
						end if

                        RS.movenext
						
                        if a = 4 then
                            response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""4""><img src=""http://www.cellularoutfitter.com/images/spacer.gif"" width=""5"" height=""5""></td></tr><tr>" & vbcrlf
                            a = 0
                        end if
                    loop
                    
                    if others = 1 then
                        %>
                        <td align="center" valign="top" width="190">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td align="center" valign="middle">
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td align="center" valign="middle"><a href="/cp-sb-15-other-wholesale-cell-phones.html"><img src="http://www.cellularoutfitter.com/images/brands/CO_CATpage_buttons_other.jpg" border="0" width="165" height="63" alt="Wholesale Cell Phones for Other Brands"></a></td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="bottom">
                                                    <a href="/cp-sb-15-other-wholesale-cell-phones.html" title="Wholesale Cell Phones for Other Brands" class="cellphone-category-font-link">Other<%=catText%></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <%
                        a = a + 1
                        if a = 4 then
                            response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""4""><img src=""http://www.cellularoutfitter.com/images/spacer.gif"" width=""5"" height=""5""></td></tr><tr>" & vbcrlf
                            a = 0
                        end if
                    end if
                    
                    if a = 1 then
                        response.write "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" & vbcrlf
                    elseif a = 2 then
                        response.write "<td>&nbsp;</td><td>&nbsp;</td>" & vbcrlf
                    elseif a = 3 then
                        response.write "<td>&nbsp;</td>" & vbcrlf
                    end if
                    %>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">&nbsp;</td>
    </tr>
    <%if seBottomText <> "" then%>
        <tr>
            <td align="center" style="padding-bottom:40px;">
                <p>&nbsp;</p>
                <table border="0" cellspacing="0" cellpadding="2" class="cate-pro-border">
                    <tr>
                        <td align="left" valign="top" class="static-content-font">
                            <%=seBottomText%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    <%end if%>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<%
call fCloseConn()
%>

<!--#include virtual="/includes/template/bottom.asp"-->
