<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
dim categoryPage : categoryPage = 1
dim categoryid : categoryid = prepInt(request.querystring("categoryid"))
dim cpass : cpass = 0
if categoryid = 0 then PermanentRedirect("/")

'call redirectURL("c", categoryid, request.ServerVariables("HTTP_X_REWRITE_URL"), "")

dim pageTitle
pageTitle = itemDesc_CO

call fOpenConn()
SQL = "SELECT typeName_CO typeName FROM we_types WHERE typeid = " & categoryid
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if RS.eof then
	call fCloseConn()
	response.redirect("/")
	response.end
end if
dim categoryName
categoryName = RS("typeName")

if cpass = 1 then
	sql	=	"select	brandname, brandid" & vbcrlf & _
			"from	we_brands" & vbcrlf & _
			"where	brandType < 2" & vbcrlf & _
			"	and	brandid not in (12)" & vbcrlf & _
			"order by brandname"
else
	if categoryid = 5 then
		sql	=	"select	distinct x.brandid, x.brandName" & vbcrlf & _
				"from	(	select	a.brandid, a.brandName, '''' + replace(handsfreetypes, ',', ''',''') + '''' handsfreetypes" & vbcrlf & _
				"			from	we_brands a join we_models b" & vbcrlf & _
				"				on	a.brandid = b.brandid" & vbcrlf & _
				"			where	a.brandtype < 2 and a.brandid not in (12,8) " & vbcrlf & _
				"				and	b.handsfreetypes is not null	) x join " & vbcrlf & _
				"		(	select	distinct '''' + convert(varchar(10), handsfreetype) + '''' handsfreetype" & vbcrlf & _
				"			from	we_items" & vbcrlf & _
				"			where	hidelive = 0 " & vbcrlf & _
				"				and	inv_qty <> 0" & vbcrlf & _
				"				and	typeid = 5	) y" & vbcrlf & _
				"	on	x.handsfreetypes like '%' + y.handsfreetype + ',%'" & vbcrlf & _
				"order by brandname "
	else
		if categoryid = 19 then 'vinyl skins
			sql	=	"select	distinct a.brandname, a.brandid" & vbcrlf & _
					"from	we_brands a join we_items b " & vbcrlf & _
					"	on	a.brandid = b.brandid join v_subtypeMatrix_co c" & vbcrlf & _
					"	on	b.subtypeid = c.subtypeid" & vbcrlf & _
					"where	c.typeid = '19' and b.hidelive = 0 " & vbcrlf & _
					"	and a.brandid not in (12) " & vbcrlf & _
					"	and a.brandtype < 2" & vbcrlf & _
					"union" & vbcrlf & _
					"select	distinct b.brandName, b.brandID" & vbcrlf & _
					"from	we_items_musicskins a join we_brands b" & vbcrlf & _
					"	on	a.brandid = b.brandid" & vbcrlf & _
					"where	a.skip = 0 and a.deleteItem = 0 and a.artist <> '' and a.artist is not null and a.designname <> '' and a.designname is not null	" & vbcrlf & _
					"	and b.brandid not in (12) " & vbcrlf & _
					"	and b.brandtype < 2" & vbcrlf & _
					"order by brandname"
		else
			sql	= 	"select	distinct a.brandname, a.brandid" & vbcrlf & _
					"from	we_brands a join we_items b " & vbcrlf & _
					"	on	a.brandid = b.brandid join v_subtypeMatrix_co c" & vbcrlf & _
					"	on	b.subtypeid = c.subtypeid" & vbcrlf & _
					"where	c.typeid = '" & categoryid & "' and b.hidelive = 0 " & vbcrlf & _
					"	and a.brandid not in (12) " & vbcrlf & _
					"	and a.brandtype < 2 " & vbcrlf & _
					"order by a.brandname "
		end if
	end if
end if
session("errorSQL") = SQL
'response.write "<pre>" & sql & "</pre>"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if RS.eof then
	call fCloseConn()
	response.redirect("/")
	response.end
end if

dim SEtitle, SEdescription, SEkeywords, topText, bottomText, catText, H1

sql	=	"select	seTitle, seDescription, seKeywords, topText, bottomText, text1, h1tag" & vbcrlf & _
		"from	xcategorytext" & vbcrlf & _
		"where	site_id = 2" & vbcrlf & _
		"	and	categoryID = '" & categoryid & "'"
session("errorSQL") = sql
set objRsSEO = oConn.execute(sql)

if not objRsSEO.eof then
	SEtitle			=	objRsSEO("seTitle")
	SEdescription	=	objRsSEO("seDescription")
	SEkeywords		=	objRsSEO("seKeywords")
	topText			=	objRsSEO("topText")
	bottomText		=	objRsSEO("bottomText")
	catText			=	objRsSEO("text1")
	H1				=	objRsSEO("h1tag")
end if
objRsSEO = null

if H1 = "" then H1 = "Cell Phone " & categoryName

dim strBreadcrumb, headerImgAltText
strBreadcrumb = H1
headerImgAltText = H1

H1 = "Cell Phone " & categoryName

	if categoryID = 3 then
		brandAlt = 2
	else
		brandAlt = 0
	end if
	if categoryID = 3 then
		abCode = "2380736971"
	end if
%>

<!--#include virtual="/includes/template/top_index.asp"-->
<style>
	h1 { font-family: Arial, Helvetica, sans-serif; font-size: 22px; color: #3399CC; margin: 0px; padding: 0px; }
</style>
									<table border="0" cellspacing="0" cellpadding="0" width="775">
										<tr>
											<td class="top-sublink-gray">
												<a class="top-sublink-gray" href="/">Cell Phone Accessories</a>&nbsp;>&nbsp;
												<span class="top-sublink-blue">Cell Phone <%=categoryName%></span>
											</td>
										</tr>
                                        <tr><td><h1 style="color:#000;"><%=H1%></h1></td></tr>
										<tr><td><img src="/images/banners/categories/<%=categoryID%>.png" border="0" /></td></tr>
										<tr>
											<td width="100%" align="center" style="padding:5px 0px 20px 0px;">
												<div style="width:750px; height:39px;" title="Please Select Your Phone Brand">
													<div style="float:left; height:39;">
														<img src="/images/types/border_left.jpg" width="11" height="39" border="0" />
													</div>
													<div style="float:left; height:39px; background: url('/images/types/border_middle.jpg') repeat-x; width:728px; font-size:22px; padding-top:5px;" align="center">
														<strong>Please Select Your Phone Brand</strong>
													</div>
													<div style="float:left; height:39;">
														<img src="/images/types/border_right.jpg" width="11" height="39" border="0" />												
													</div>
												</div>	
											</td>
										</tr>
										<tr>
											<td align="center" width="100%">
												<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<%
														a = 0
														dim altText
														do until RS.eof
															brandName = prepStr(rs("brandName"))
															brandID = prepInt(rs("brandID"))
															altText = brandName & " " & categoryName
															%>
															<td align="center" valign="top" width="190">
																<table border="0" cellspacing="0" cellpadding="0" width="100%">
																	<tr>
																		<td align="center" valign="middle">
																			<table border="0" cellspacing="0" cellpadding="0" width="100%">
																				<tr>
																					<td align="center" valign="middle"><a href="/sc-<%=categoryID & "-sb-" & brandID & "-cell-phone-" & formatSEO(categoryName) & "-for-" & formatSEO(brandName)%>.html"><img src="/images/buttons/brands/<%=brandID%>.png" border="0" width="165" height="63" alt="<%=altText%>" title="<%=altText%>"></a></td>
																				</tr>
																				<tr>
																					<td align="center" valign="bottom"><a href="/sc-<%=categoryID & "-sb-" & brandID & "-cell-phone-" & formatSEO(categoryName) & "-for-" & formatSEO(brandName)%>.html" title="<%=altText%>" class="cellphone-category-font-link" style="padding:0px 1px 0px 1px;"><%=brandName & " " & categoryName%></a></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
															<%
															RS.movenext
															a = a + 1
															if a = 4 then
																response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""4""><img src=""/images/spacer.gif"" width=""5"" height=""5""></td></tr><tr>" & vbcrlf
																a = 0
															end if
														loop
														if a = 1 then
															response.write "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" & vbcrlf
														elseif a = 2 then
															response.write "<td>&nbsp;</td><td>&nbsp;</td>" & vbcrlf
														elseif a = 3 then
															response.write "<td>&nbsp;</td>" & vbcrlf
														end if
														%>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td align="center" valign="top">&nbsp;</td>
										</tr>
										<%if bottomText <> "" then%>
											<tr>
												<td align="center">
													<p>&nbsp;</p>
													<table border="0" cellspacing="0" cellpadding="2" class="cate-pro-border">
														<tr>
															<td align="left" valign="top" class="static-content-font">
																<%=bottomText%>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										<%end if%>
										<!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
									</table>
<%
call fCloseConn()
%>

<!--#include virtual="/includes/template/bottom.asp"-->
