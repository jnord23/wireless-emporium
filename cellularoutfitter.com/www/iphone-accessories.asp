<%
response.Status = "301 Moved Permanently"
response.AddHeader "Location", "/b-17-apple-cell-phone-accessories.html"
response.end

response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
dim brandPage
brandPage = 1

dim pageTitle
pageTitle = itemDesc_CO

call fOpenConn()
SQL = "SELECT distinct A.[ModelId], A.[ModelName], A.[ModelImg], A.temp, C.[BrandId], C.[BrandName], D.[TypeId], D.[TypeName] FROM ((we_models A with(nolock) INNER JOIN we_items B with(nolock) ON A.modelID = B.modelID)"
SQL = SQL & " INNER JOIN we_brands C with(nolock) ON A.brandid = C.brandid)"
SQL = SQL & " INNER JOIN we_types D with(nolock) ON B.typeid = D.typeid"
SQL = SQL & " WHERE B.modelid = 968"
SQL = SQL & " AND B.hideLive = 0"
SQL = SQL & " AND B.inv_qty <> 0"
SQL = SQL & " AND B.price_CO > 0 AND D.TypeId < 16"
SQL = SQL & " ORDER BY A.temp DESC"
'session("errorSQL") = SQL
set RS = oConn.execute( SQL)

if RS.eof then
	call fCloseConn()
	response.redirect("/")
	response.end
end if

dim brandName, brandID, modelName, modelImg
brandName = RS("brandName")
brandID = RS("brandID")
modelName = RS("modelName")
modelImg = RS("modelImg")

dim SEtitle, SEdescription, SEkeywords, topText, bottomText, h1, h2

SEtitle = "Apple iPhone 4 Accessories � Wholesale Apple iPhone 4 Cell Phone Accessories"
SEdescription = "CellularOutfitter.com offers a huge collection of wholesale Apple iPhone 4 Accessories � Buy Apple iPhone 4 Cell Phone Accessories like Covers, Bling Kits, Bluetooth Headsets, Chargers and many other accessories at wholesale prices."
SEkeywords = "Apple iPhone 4 Accessories, Apple iPhone 4 Cell Phone Accessories, Apple iPhone 4 Chargers, Apple iPhone 4 Bluetooth Headsets, Apple iPhone 4 Covers"

dim strBreadcrumb
strBreadcrumb = "Apple iPhone 4 Cell Phone Accessories"

topText = _
"<p class=""top-text-red"" style=""padding-left: 10px; margin-left: 10px;"">" &VbCrLf&_
"The iPhone 4 is one of the best phones on the market ... except for that pesky death grip issue. CellularOutfitter is here to save the day with a selection of guards and cases, all suited to resolve the antenna issues. From sleek jet black to Ed Hardy and everything in between, CellularOutfitter is here to ensure you get the best out of your iPhone 4." &VbCrLf&_
"</p>" &VbCrLf&_
"<p class=""top-text-grey"">" &VbCrLf&_
"Find Apple iPhone 4 accessories at cheap, discount prices to the public. Our products fit your budget and lifestyle." &VbCrLf&_
"Cellular Outfitter offers premium quality factory-direct cell phone accessories at prices substantially lower than retail. In fact, you pay what retailers pay on all iPhone accessories! Our quality-guaranteed products are backed by a full 1-yr manufacturer warranty." &VbCrLf&_
"</p>" &VbCrLf&_
"<p class=""top-text-grey"">" &VbCrLf&_
"At Cellular Outfitter, you will find the best Apple iPhone 4 cell phone accessories for your phone at wholesale prices. We offer many innovative products you simply won't find at a typical retailer." &VbCrLf&_
"From iPhone covers to iPhone cases, Cellular Outfitter can make sure your iPhone is protected not only from drops and dings but also from scratches caused by everyday use. We also carry discounted items for the Apple iPhone 4 such as Covers, Chargers, Bluetooth Headsets, Bling Kits, Charms, and Cases � all at wholesale prices." &VbCrLf&_
"</p>"

h1 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Apple iPhone 4 Cell Phone Accessories"
'todo: need to consolidate Metatags with we_XXXText
dim strH1: strH1 = h1
dim strH2: strH2 = EMPTY_STRING
dim strAltText: strAltText = EMPTY_STRING

'set input values to extract page meta-values
dim dicReplaceAttribute: set dicReplaceAttribute = CreateObject("Scripting.Dictionary")
dicReplaceAttribute( "BrandName") = brandName
dicReplaceAttribute( "ModelName") = modelName

'set known product attributes to replace out template placeholders
dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
dicSeoAttributeInput( "BrandId") = brandID
dicSeoAttributeInput( "ModelId") = modelID

'get content event text (initially installed to load pixels) [knguyen/20110520]
'todo: deploy to other wrappers as needed
dim dicContentEventText: set dicContentEventText = GetContentEventText( dicSeoAttributeInput)

'call LookupSeoAttributes()

%>
<!--#include virtual="/includes/template/top_index.asp"-->
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td class="top-sublink-gray">
												<a class="top-sublink-gray" href="/">Cell Phone Accessories</a>&nbsp;>&nbsp;
												<a class="top-sublink-gray" href="/b-17-apple-cell-phone-accessories.html">Apple Cell Phone Accessories</a>&nbsp;>&nbsp;
												<span class="top-sublink-blue"><%=strBreadcrumb%></span>
											</td>
										</tr>
										<tr>
											<td class="top-sublink-gray"><img src="/images/spacer.gif" width="1" height="10"></td>
										</tr>
										<tr>
											<td align="left" width="775" height="68" style="background-image:url(/images/iphone/CO_header_applestore.jpg); background-repeat:no-repeat;">
												<style>
													h1 {
														color: #FFFFFF;
													}
												</style>
												<h1><%=strH1%></h1>
											</td>
										</tr>
										<tr>
											<td class="top-sublink-gray"><img src="/images/spacer.gif" width="1" height="10"></td>
										</tr>
										<tr>
											<td>
												<table border="0" cellspacing="0" cellpadding="0" width="752">
													<tr>
														<td align="center" valign="middle" width="206"><img src="/modelpics/iphone-3g.jpg" border="0" alt="<%=brandName & " " & modelName%> Cell Phone Accessories"></td>
														<td valign="top" width="558">
															<table border="0" cellspacing="0" cellpadding="5" width="530" class="cate-pro-border">
																<tr>
																	<td>
																		<%=topText%>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td align="left" valign="top">
												<table border="0" align="center" cellspacing="0" cellpadding="0" width="752">
													<tr>
														<%
														response.write "<td align=""center"" valign=""top"" width=""188""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""119""><tr><td align=""center"" valign=""middle"" class=""cellphone-top-table""><img src=""/images/spacer.gif"" width=""1"" height=""6"" border=""0""></td></tr>" & vbcrlf
														response.write "<tr><td align=""center"" valign=""middle"" class=""cellphone-middle-table""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""100""><tr><td align=""center"" valign=""bottom"" class=""cellphone-font"" height=""25""><a class=""cellphone-category-font"" href=""/hf-sm-968-apple-iphone-4-hands-free-kits-bluetooth-headsets.html"" title=""Apple iPhone 4 Hands-Free Kits &amp; Bluetooth Headsets"">Handsfree</a></td></tr>" & vbcrlf
														response.write "<tr><td ""center"" valign=""middle"" height=""150""><a href=""/hf-sm-968-apple-iphone-4-hands-free-kits-bluetooth-headsets.html""><img src=""/images/iphone/handsfree.jpg"" width=""90"" border=""0"" alt=""Apple iPhone 4 Hands-Free Kits &amp; Bluetooth Headsets""></a></td></tr></table></td></tr>" & vbcrlf
														response.write "<tr><td align=""center"" valign=""middle"" class=""cellphone-bottom-table""><img src=""/images/spacer.gif"" width=""1"" height=""6"" border=""0""></td></tr></table></td>" & vbcrlf
														a = 1
														do until RS.eof
															response.write "<td align=""center"" valign=""top"" width=""188""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""119""><tr><td align=""center"" valign=""middle"" class=""cellphone-top-table""><img src=""/images/spacer.gif"" width=""1"" height=""6"" border=""0""></td></tr>" & vbcrlf
															response.write "<tr><td align=""center"" valign=""middle"" class=""cellphone-middle-table""><table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""100""><tr><td align=""center"" valign=""bottom"" class=""cellphone-font"" height=""25""><a class=""cellphone-category-font"" href=""/sb-17-sm-968-sc-" & RS("typeid") & "-apple-iphone-4-" & formatSEO(RS("typename")) & ".html"" title=""Apple iPhone 4 " & replace(RS("typeName"),"Antennas/Parts","Stylus Pens") & """>" & replace(RS("typeName"),"Antennas/Parts","Stylus Pens") & "</a></td></tr>" & vbcrlf
															response.write "<tr><td ""center"" valign=""middle"" height=""150""><a href=""/sb-17-sm-968-sc-" & RS("typeid") & "-apple-iphone-4-" & formatSEO(RS("typename")) & ".html""><img src=""/images/iphone/" & formatSEO(RS("typeName")) & ".jpg"" width=""90"" border=""0"" alt=""Apple iPhone 4 " & RS("typeName") & """></a></td></tr></table></td></tr>" & vbcrlf
															response.write "<tr><td align=""center"" valign=""middle"" class=""cellphone-bottom-table""><img src=""/images/spacer.gif"" width=""1"" height=""6"" border=""0""></td></tr></table></td>" & vbcrlf
															RS.movenext
															a = a + 1
															if a = 4 then
																response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""4"">&nbsp;</td></tr><tr>" & vbcrlf
																a = 0
															end if
														loop
														%>
                                                        <td align="center" valign="top" width="188">
                                                        	<div style="width:119px; margin:0px 34px 0px 34px;">
                                                            	<div style="padding-top:6px;"><a class="cellphone-category-font" href="/sb-17-sm-968-sc-20-music-skins-genres.html" title="Apple iPhone 4 Music Skins">Music Skins</a></div>
                                                                <div style="padding-top:15px;"><a href="/sb-17-sm-968-sc-20-music-skins-genres.html"><img src="/images/iphone/musicSkins.jpg" width="90" border="0" alt="Apple iPhone 4 Music Skins"></a></div>
                                                            </div>
                                                        </td>
                                                        <%
														a = a + 1
														if a = 1 then
															response.write "<td width=""188"">&nbsp;</td><td width=""188"">&nbsp;</td><td width=""188"">&nbsp;</td>" & vbcrlf
														elseif a = 2 then
															response.write "<td width=""188"">&nbsp;</td><td width=""188"">&nbsp;</td>" & vbcrlf
														elseif a = 3 then
															response.write "<td width=""188"">&nbsp;</td>" & vbcrlf
														end if
														%>
													</tr>
													<tr>
														<td align="center" valign="top">&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>
										<%if bottomText <> "" then%>
										<tr>
											<td align="center">
												<p>&nbsp;</p>
												<table border="0" cellspacing="0" cellpadding="2" class="thin-border">
													<tr>
														<td align="left" valign="top" class="contain">
															<p class="static-content-font"><%=bottomText%></p>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<%end if%>
										<!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
									</table>
<%
call fCloseConn()
%>

<!--#include virtual="/includes/template/bottom.asp"-->
