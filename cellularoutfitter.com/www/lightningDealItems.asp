<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	response.buffer = true
	noLeftNav = 0
%>
<!--#include virtual="/includes/template/top.asp"-->
<style>
.quarter {
	width:24%;
	display:inline-block;
}
</style>
<%
categoryID = request.QueryString("categoryID")

if categoryID <> "" then
	sql =	"select		ci.id, c.name, a.* " & vbcrlf & _
			"from		lightningDeal_CategoryItems ci left join lightningDeal_Categories c" & vbcrlf & _
			"	on		c.id = ci.categoryID left join we_Items a " & vbcrlf & _
			"	on		ci.itemID = a.itemid " & vbcrlf & _
			"where		ci.categoryID = " & categoryID & " " & vbclrf & _
			"	and		ci.isActive = 1 " & vbcrlf & _
			"order by	ci.sortOrder "
	session("errorSQL") = sql
	'sql = "select * from sys.tables where name like 'lightning%'"
	
	set rs = oConn.execute(sql)
	
	'do while not rs.eof
	'	response.write(rs("name") & "<br />")
	'	rs.movenext
	'loop
	
	'response.end
	
%>
<div>
<h1><%=rs("name")%></h1>
</div>
<% i = 1 %>
<% if not rs.eof then %>
<% do while not rs.eof %>
<%
	id = rs("id")
	itemID = rs("itemID")
	itemDesc = rs("itemDesc_CO")
	itemPic = rs("itemPic_CO")
	itemPartNumber = rs("partNumber")
	itemPriceOur = rs("price_CO")
	itemPriceRetail = rs("price_Retail")
%>


<%'The following code is copied from brand-model-category_mp.asp (2012-11-28)
  'If another holiday sale needs to use these custom category items, the following block (or a more current copy of it) should be used %>

<%'    <div id="itemListID_<%=picLap$>" class="<%=useClass$>">
'        <div class="bmc_productPic" id="picDiv_<%=picLap$>" title="<%=altText$>" id="imgDiv_<%=picLap$>"><% if onSale then$><div class="bmc_productOnSale onSale"></div><% else $><div class="bmc_productOnSale"></div><% 'end if $><tempimg id="prodPic" src="<%=useImg$>" border="0" onclick="window.location='/p-<%=itemid & "-" & formatSEO(useItemDesc)$>.html'" class="clickable"></div>
'        <div class="bmc_productTitle" title="<%=altText$>"><a class="static-content-font-link" href="/p-<%=itemid & "-" & formatSEO(useItemDesc)$>.html" title="<%=altText$>"><%=useItemDesc$></a></div>
'        <div class="bmc_rating"><% if reviewAvg > 0 then $><%=getRatingAvgStar(reviewAvg)$><% end if $></div>
'        <% if outOfStock then $>
'        <div class="bmc_outOfStock">OUT OF STOCK</div>
'<% else $>
'        <div class="bmc_retailPrice">Retail Price: <s><%=formatCurrency(price_retail)$></s></div>
'            <%
'            dim holidayPricing : holidayPricing = 0
'            if holidaySale = 1 then
'                partNumber3 = ""
'                if not isnull(curPN) then partNumber3 = left(curPN, 3) end if
'                if instr(holidaySalePartNum(0), "|" & partNumber3 & "|") > 0 then
'                    holidayPricing = price_CO - (price_CO * holidaySalePercent(0))
'                elseif instr(holidaySalePartNum(1), "|" & partNumber3 & "|") > 0 then
'                    holidayPricing = price_CO - (price_CO * holidaySalePercent(1))
'                end if
'            end if
'        
'            if holidayPricing > 0 and not noDiscount then
'            $>
'        <div class="bmc_wholesale1">Wholesale Price: <span class="red-price-small"><s><%=formatCurrency(price_CO)$></s></span></div>
'        <div class="bmc_holidayPrice">You Pay: <span class="red-price-small"><%=formatCurrency(holidayPricing)$></span></div>
'            <% else $>
'        <div class="bmc_wholesale2">Wholesale Price: <span class="red-price-small"><%=formatCurrency(price_CO)$></span></div>
'            <% end if $>
'<% end if $>
'        <div class="bmc_moreInfo"><a href="/p-<%=itemid & "-" & formatSEO(useItemDesc)$>.html"><img src="/images/moreInfo.png" width="94" height="20" border="0" alt="More Info"></a></div>
'        <div id="subType_<%=picLap$>" style="display:none;"><%=subtypeID$></div>
'        <div id="price_<%=picLap$>" style="display:none;"><%=price_CO$></div>
'    </div>    
%>



<div class="quarter">
                        <div class="bmc_productPic" id="picDiv_<%=i%>" title="<%=itemDesc%>">
                        	<div id="id_productImage_135708" style="position:relative;">
								
                                <img src="/productpics/thumb/<%=itemPic%>" border="0" width="100" height="100" title="<%=itemDesc%>" onclick="window.location='/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.asp'" class="clickable">
                                <br><span style="color:red; font-size:10px;"><%=itemPartNumber%></span><br><span style="font-size:10px;"><%=itemID%></span>
                            </div>
                        </div>
                        
                        <div class="product-bmc-details" align="center">
                            <div id="id_productLink_<%=itemID%>" class="product-bmc-desc">
                                <a class="cellphone-link" href="/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.asp" title="<%=itemDesc%>" alt="<%=itemDesc%>"><%=itemDesc%></a>
                            </div>
                        	                                                    
                        </div>
                        <div style="float:left; width:100%; padding-top:5px;" align="center">
                            <span class="boldText">Our&nbsp;Price:&nbsp;</span>
                            <span class="pricing-orange2">$<%=itemPriceOur%></span><br>
                            <span class="pricing-gray2">List&nbsp;Price:&nbsp;<s>$<%=itemPriceRetail%></s></span>
                        </div>
                        
</div>
<% i = i + 1 %>
<% rs.movenext %>
<% loop %>
<% end if %>


<% else %>
<h1>No items found.</h1>
<% end if %>
<!--#include virtual="/includes/template/bottom.asp"-->
<% fCloseConn %>
