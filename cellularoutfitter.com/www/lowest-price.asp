<%
pageTitle = "Lowest Price Online - Guaranteed"
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td class="top-sublink-gray">
												<a class="top-sublink-gray" href="/">Cell Phone Accessories</a>&nbsp;>&nbsp;
												<span class="top-sublink-blue"><%=pageTitle%></span>
											</td>
										</tr>
										<tr>
											<td width="100%" align="center" valign="top">
												<img src="/images/CO_pricematch_banner.jpg" width="775" height="130" border="0" alt="Price Match Guarantee">
											</td>
										</tr>
										<tr>
											<td width="100%" align="center" valign="top">
												<table width="700" border="0" cellspacing="0" cellpadding="0" align="center">
													<tr>
														<td width="100%" valign="top" class="static-content-font">
															<p>
																<img src="/images/checkmark2.jpg" width="26" height="26" border="0">
																Lowest Prices anywhere, GUARANTEED!!!<br>
																<img src="/images/checkmark2.jpg" width="26" height="26" border="0">
																Huge inventory with over 30,000 items In-Stock!<br>
																<img src="/images/checkmark2.jpg" width="26" height="26" border="0">
																Up to 80% off of retail pricing<br>
                                                                <img src="/images/checkmark2.jpg" width="26" height="26" border="0">
																Complete Satisfaction Guarantee!<br>
															</p>
															<p>
																CellularOutfitter.com GUARANTEES the lowest price of any major online retailer.
																We're so confident in our ability to provide the lowest possible price of any online retailer,
																that we will credit you the difference if you can find a cheaper price online.
															</p>
															<p>
																Here's how it works:
															</p>
															<p>
																If you have found a price (product + shipping + tax) lower than ours on a competing site,
																please email a screen shot of the checkout page to:
																<a href="mailto:priceguarantee@cellularoutfitter.com">priceguarantee@cellularoutfitter.com</a>,
																or, you can fax a printout to:
															</p>
															<p>
																(714) 278-1932<br>
																attn: "CellularOutfitter.com Price Guarantee"
															</p>
															<p>
																We will credit you the difference!
																This offer is effective a full 14 days after your purchase date so you can be assured you
																are receiving the BEST value anywhere.
															</p>
															<p>
																It's as simple as that! No gimmicks, no tricks.
																It's just another way that the team here at CellularOutfitter.com is dedicated to providing
																the best possible shopping experience anywhere!
															</p>
															<p class="mc-text">
																<i>* PLEASE NOTE: Due to the volatility of the online auction marketplace,
																we DO NOT match pricing on auction sites such as eBay, Overstock, Half.com,
																Yahoo! Auctions, Amazon.com auctions, etc.
																The competing site and offer must be recognized by what CellularOutfitter.com deems a
																valid, established online U.S. merchant in good standing and shall not include
																micro-affiliate, gateway, non-domestic (USA) sites or any other variation thereof and
																of which shall be determined at the sole discretion of CellularOutfitter.com.</i>
															</p>
															<p class="mc-text">
																<i>** ALSO NOTE: Our price matching guarantee applies to cellular accessories only.
																This guarantee does not apply to cellular phones.</i>
															</p>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>

<!--#include virtual="/includes/template/bottom.asp"-->
