<%
	response.buffer = true

	pageTitle = "Brand_b"
	
	dim brandid : brandID = prepInt(request.querystring("brandid"))
	dim cbSort : cbSort = prepStr(request.Form("cbSort"))
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
	set fso = CreateObject("Scripting.FileSystemObject")
	Set Img = Server.CreateObject("Persits.Jpeg")
	set Jpeg = Server.CreateObject("Persits.Jpeg")
	Jpeg.Quality = 60
	Jpeg.Interpolation = 1

	stitchPath = "/images/stitch/brands/"

	if brandID = 0 then PermanentRedirect("/")
	if cbSort = "" then cbSort = "NO"
	
	response.Cookies("myBrand") = brandID
	response.Cookies("myModel") = 0
	
	leftGoogleAd = 1
	
	'call redirectURL("b", brandid, request.ServerVariables("HTTP_X_REWRITE_URL"), "")

'	sql = "exec sp_modelsByBrand " & brandID & ",'AZ','',null"
	sql = "exec sp_modelsByBrand2 " & brandID & ",'AZ','',null"
	session("errorSQL") = sql
	set listRS = oConn.execute(sql)
		
'	sql = "exec sp_modelsByBrand " & brandID & ",'" & cbSort & "','',null"
	sql = "exec sp_modelsByBrand2 " & brandID & ",'" & cbSort & "','',null"
'	response.write sql
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	if RS.eof then 
		call responseRedirect("/?ec=202")
	end if
	
	dim brandName, brandImg, lap, productList, numTopModels
	numTopModels = 0
	productList = ""
	lap = 0
	do while not RS.EOF
		lap = lap + 1
		if lap = 1 then
			brandName = RS("brandName")
			brandImg = RS("brandImg")
		end if
		productList = productList & RS("modelID") & "*" & RS("modelName") & "*" & RS("modelImg") & "*" & RS("topModel") & "#"
		if RS("topModel") then numTopModels = numTopModels + 1
		RS.movenext
	loop
	
	productArray = split(productList,"#")
	itemImgPath = formatSEO(brandName) & "_" & cbSort & "_all.jpg"
	if instr(request.ServerVariables("SERVER_NAME"), "staging") > 0 then itemImgPath = "staging_" & itemImgPath

	'### Start test of stitch date ###
	if fso.FileExists(Server.MapPath(stitchPath & itemImgPath)) then
		set fsTemp = fso.GetFile(Server.MapPath(stitchPath & itemImgPath))
		createDate = fsTemp.DateLastModified
		if datediff("h",createDate,now) > 1 then
			fso.deleteFile(Server.MapPath(stitchPath & itemImgPath))
'			response.Write("<!-- pop file deleted -->")
		else
'			response.Write("<!-- pop file valid -->")
		end if
	end if
	'### End test of stitch date ###
	if not fso.FileExists(Server.MapPath(stitchPath & itemImgPath)) then
		'create single product image
		imgWidth = 70 * (lap)
		Jpeg.New imgWidth, 112, &HFFFFFF
		imgX = 0
		imgY = 0
		for i = 0 to (ubound(productArray) - 1)
			curProductArray = split(productArray(i),"*")
			useNA = 0
			inner_itemImgPath = Server.MapPath("/modelPics/thumbs") & "\" & curProductArray(2)
			inner_itemImgPath2 = Server.MapPath("/modelPics") & "\" & curProductArray(2)
			if not fso.FileExists(inner_itemImgPath) then
				if not fso.FileExists(inner_itemImgPath2) then
					useNA = 1
				else
					session("errorSQL") = "inner_itemImgPath2:" & inner_itemImgPath2 & "<br>"
					Jpeg.Open inner_itemImgPath2
					Jpeg.Height = 112
					Jpeg.Width = 70
					Jpeg.Save inner_itemImgPath
				end if
			end if
			if useNA = 1 then
				Img.Open Server.MapPath("/modelPics/thumbs/modelna.jpg")
				Jpeg.Canvas.DrawImage X + (imgX)/2, Y + (imgY)/2, Img
				imgX = imgX + 140
			elseif not isnull(curProductArray(2)) and len(curProductArray(2)) > 0 then
				Img.Open Server.MapPath("/modelPics/thumbs/") & "\" & curProductArray(2)
				Jpeg.Canvas.DrawImage X + (imgX)/2, Y + (imgY)/2, Img
				imgX = imgX + 140
			end if
		next
		Jpeg.Save Server.MapPath(stitchPath & itemImgPath)
	end if
	
	dim strBreadcrumb, headerImgAltText
	strBreadcrumb = brandName & " Cell Phone Accessories"
	headerImgAltText = brandName & " Cell Phone Accessories"
	
	chkFilePath = Server.MapPath(stitchPath & itemImgPath)
	imgWidth = ubound(productArray) * 70
	if fso.FileExists(chkFilePath) then
		Jpeg.Open chkFilePath
		if cint(imgWidth) <> cint(Jpeg.Width) and imgWidth > 0 then
			fso.DeleteFile(chkFilePath)
			call responseRedirect("http://" & request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL"))
		end if
	end if
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<script>
window.WEDATA.pageType = 'brand';
window.WEDATA.pageData = {
	brand: '<%= brandName %>',
	brandId: '<%= brandId %>'
};
</script>

<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td style="padding:0px 20px; border-bottom:1px solid #CCC;">
            <a class="top-sublink-gray" href="/">Cell Phone Accessories&nbsp;>&nbsp;</a>
            <span class="top-sublink-blue"><%=brandName%> Cell Phone Accessories</span>
        </td>
    </tr>
    <tr>
        <td width="100%" align="center" style="padding:5px 0px 5px 0px;" title="<%=headerImgAltText%>">
        	<table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td align="left" width="214"><img src="/images/logos/<%=brandID%>.png" border="0" width="214" height="74" /></td>
                    <td align="center" width="*">
                    	<div><h1 style="color:#000; font-size:24px; margin:0px; padding:0px;"><%=seH1%></h1></div>
                    	<div style="padding-top:10px;">Select A Phone Model Below To Shop</div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <%if numTopModels > 0 then%>
    <tr><td style="font-size:24px; padding:10px 0px 10px 0px;">Top <%=brandName%> Models</td></tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr><% call drawModels(productArray, itemImgPath, true) %></tr>
            </table>
        </td>
    </tr>
    <%end if%>
    <tr>
    	<td style="padding:20px 0px 20px 20px;">
        	<div style="float:left;"><img src="/images/backgrounds/gray-bar-left.png" border="0" width="22" height="64" /></div>
            <div style="float:left; height:64px; width:735px; background-image:url(/images/backgrounds/gray-bar-background.png)">
            	<div style="float:left;"><img src="/images/icons/gray-bar-bluedot2.png" border="0" width="26" height="64" /></div>
            	<div style="float:left; margin-top:27px; font-weight:bold; padding-left:10px; font-size:11px;">Select Your Cell Phone Model Below.</div>
                <div style="float:left; margin-top:25px; padding-left:20px;">
                	<form name="frmSort" method="post">
                        <select name="cbSort" onChange="this.form.submit();">
                            <option value="AZ" <%if cbSort = "AZ" then %>selected<% end if%>>Sort By A to Z</option>
                            <option value="ZA" <%if cbSort = "ZA" then %>selected<% end if%>>Sort By Z to A</option>
                            <option value="NO" <%if cbSort = "NO" then %>selected<% end if%>>Sort By Newest to Oldest</option>
                            <option value="ON" <%if cbSort = "ON" then %>selected<% end if%>>Sort By Oldest to Newest</option>
                        </select>                                                	
                    </form>
                </div>
                <div style="float:left; padding-left:10px;"><img src="/images/backgrounds/gray-bar-or.png" border="0" width="44" height="64" /></div>
                <div style="float:left; margin-top:25px; padding-left:10px;">
                	<select name="selectModel" onChange="if(this.value != ''){window.location=this.value;}" style="width:150px;">
                        <option value="">Select from this list</option>
                        <%
                        do while not listRS.EOF
                            modelID = listRS("modelID")
                            modelName = listRS("modelName")
                        %>
                        <option value="<%="/m-" & modelID & "-" & formatSEO(brandName) & "-" & formatSEO(modelName) & "-cell-phone-accessories.html"%>"><%=modelName%></option>
                        <%
							listRS.movenext
                        loop
                        %>
                    </select>
                </div>
            </div>
            <div style="float:left;"><img src="/images/backgrounds/gray-bar-right.png" border="0" width="22" height="64" /></div>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr><% call drawModels(productArray, itemImgPath, false) %></tr>
            </table>
        </td>
    </tr>
    <% if brandID = 9 then %>
    <tr>
        <td colspan="4" align="center" style="padding-top:10px;">
            <div style="border:1px solid #999; background-color:#ebebeb; border-radius:15px; padding:10px 0px 10px 0px; width:500px; font-weight:bold;">Can't Find What You're Looking For? - <a href="mailto:support@cellularoutfitter.com" style="font-size:14px; text-decoration:underline;">Click Here</a></div>
        </td>
    </tr>
    <% end if %>
    <tr><td align="left" class="static-content-font" style="padding-top:10px;"><%=SeTopText%></td></tr>
    <tr><td class="static-content-font" style="padding:10px 0px 50px 0px; font-size:10px;"><%=SeBottomText%></td></tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<script>
	function jumpto()
	{
		modelname = document.frmSearch.txtSearch.value;
		var x=document.frmSearch.selectModel;
		for (i=0;i<x.length;i++)
			if (x.options[i].text == modelname) window.location = x.options[i].value;
	}
</script>

<%
	sub drawModels(byref arrProduct, imgStitch, showTopModel)
		Set objFile = fso.GetFile(Server.MapPath(stitchPath & imgStitch))
		md = replace(replace(replace(objFile.DateLastModified," ",""),":",""),"/","")
						
		useCarrierLogo = ",1301,1259,1258,"
		a = 0
		row = 1
		useX = 0

		for i = 0 to (ubound(arrProduct) - 1)
			curProductArray = split(arrProduct(i),"*")
			altText = brandName & " Cell Phone Accessories: " & brandName & " " & curProductArray(1) & " Accessories"
			bTopModel = curProductArray(3)
			if showTopModel then	'only top models
				if bTopModel then
					a = a + 1
				%>
				<td valign="top" style="<% if a < 5 then %>border-right:1px solid #CCC;<% end if %> border-bottom:1px solid #ccc;">
					<div style="width:150px; margin:10px 0px 10px 0px;">
						<div style="margin-left:40px;">
							<a href="/m-<%=curProductArray(0) & "-" & formatSEO(brandName) & "-" & formatSEO(curProductArray(1))%>-cell-phone-accessories.html">
								<div id="PhonePic<%=a%>" style="position:relative; width: 70px; height: 112px; background: url(<%=stitchPath & imgStitch%>?md=<%=md%>) <%=useX%>px 0px no-repeat; cursor:pointer;" title="<%=altText%>">
								<%if instr(useCarrierLogo, "," & curProductArray(0) & ",") > 0 then%>
									<div style="position:absolute; bottom:0px; left:-25px;"><img src="/images/brands/b_carrierLogo_<%=curProductArray(0)%>.png" border="0" /></div>
								<%end if%>
								</div>
							</a>
						</div>
						<div style="text-align:center; margin:5px 5px 0px 5px; width:140px;"><a style="color:#2d5183; text-decoration:underline; font-size:12px;" href="/m-<%=curProductArray(0) & "-" & formatSEO(brandName) & "-" & formatSEO(curProductArray(1))%>-cell-phone-accessories.html" title="<%=brandName & " " & curProductArray(1)%> Cell Phone Accessories"><%=curProductArray(1)%></a></div>
					</div>
				</td>
				<%
				end if
			else			' except for top models
				if not bTopModel then
					a = a + 1
				%>
				<td valign="top" style="<% if a < 5 then %>border-right:1px solid #CCC;<% end if %> border-bottom:1px solid #ccc;">
					<div style="width:150px; margin:10px 0px 10px 0px;">
						<div style="margin-left:40px;">
							<a href="/m-<%=curProductArray(0) & "-" & formatSEO(brandName) & "-" & formatSEO(curProductArray(1))%>-cell-phone-accessories.html">
								<div id="PhonePic<%=a%>" style="position:relative; width: 70px; height: 112px; background: url(<%=stitchPath & imgStitch%>?md=<%=md%>) <%=useX%>px 0px no-repeat; cursor:pointer;" title="<%=altText%>">
								<%if instr(useCarrierLogo, "," & curProductArray(0) & ",") > 0 then%>
									<div style="position:absolute; bottom:0px; left:-25px;"><img src="/images/brands/b_carrierLogo_<%=curProductArray(0)%>.png" border="0" /></div>
								<%end if%>
								</div>
							</a>
						</div>
						<div style="text-align:center; margin:5px 5px 0px 5px; width:140px;"><a style="color:#2d5183; text-decoration:underline; font-size:12px;" href="/m-<%=curProductArray(0) & "-" & formatSEO(brandName) & "-" & formatSEO(curProductArray(1))%>-cell-phone-accessories.html" title="<%=brandName & " " & curProductArray(1)%> Cell Phone Accessories"><%=curProductArray(1)%></a></div>
					</div>
				</td>
				<%
				end if
			end if
			
			useX = useX - 70
			if a = 5 then
				response.write "</tr><tr>"
				a = 0
				row = row + 1
			end if
		next	
	end sub
%>

<!--#include virtual="/includes/template/bottom_index.asp"-->