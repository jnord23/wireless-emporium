// JavaScript Document

function km_addToCart(itemCat, itemDesc) {
	_kmq.push(['record', 'added to cart', {'product category':itemCat, 'product name':itemDesc}]);
	
	return true;
}

function km_commented(url) {
	_kmq.push(['record', 'commented', {'content type':url}]);
	
	return true;
}

function km_newsletter(email) {
	_kmq.push(['record', 'subscribed to newsletter', {'customer email':email}]);
	
	return true;
}

