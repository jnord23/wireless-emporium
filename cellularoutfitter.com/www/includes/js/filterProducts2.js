	var cntPerPage = perPage;
	var myCurPage = 1;
	var activeProds = 0;
	var filterItemCnt = 0;
	var newPageTotal = 0;

	// Set default (on page load)
	setTimeout("updateFilter()",1000)

	function prevPage() {
		if ((myCurPage-1) > 0) {
			showPage(myCurPage-1)
		}
	}
	
	function nextPage() {
		if (parseInt(newPageTotal) < newPageTotal) { newPageTotal = parseInt(newPageTotal) + 1 }
		if ((myCurPage+1) <= newPageTotal) {
			showPage(myCurPage+1)
		}
	}
	
	function slidePageNumbers(pageNum) {
		if ((pageNum - 3) > 1) {
			document.getElementById("preDots").className = "fl extraPages";
			for (i=2;i<=(pageNum - 3);i++) {
				document.getElementById("pageLinkBox_" + i).className = "paginationLink1 hide";
			}
			for (i=(pageNum - 2);i<=(pageNum + 2);i++) {
				if (i <= totalPages) {
					document.getElementById("pageLinkBox_" + i).className = "paginationLink1";
				}
			}
			for (i=(pageNum+3);i<=totalPages;i++) {
				document.getElementById("pageLinkBox_" + i).className = "paginationLink1 hide";
			}
		}
		else {
			for (i=2;i<=6;i++) {
				if (i <= totalPages) {
					document.getElementById("pageLinkBox_" + i).className = "paginationLink1";
				}
			}
			for (i=7;i<=totalPages;i++) {
				document.getElementById("pageLinkBox_" + i).className = "paginationLink1 hide";
			}
			if (document.getElementById("preDots") != null) { document.getElementById("preDots").className = "fl extraPages hide"; }
		}
		
		if (document.getElementById("postDots") != null) {
			if ((pageNum + 2) >= totalPages) {
				document.getElementById("postDots").className = "fl extraPages hide";
			}
			else {
				document.getElementById("postDots").className = "fl extraPages";
			}
		}
	}
	
	function selectFilter(subID) {
		if (document.getElementById(subID).checked == true) {
			document.getElementById(subID).checked = false;
		}
		else {
			document.getElementById(subID).checked = true;
		}
		updateFilter()
	}
	
	// Update visible content
	function showPage(pageNum) {
		myCurPage = pageNum
		filterItemCnt = totalItems
		
		slidePageNumbers(pageNum)
		if (pageNum == 0) { perPage = totalItems } else { perPage = cntPerPage }
		// Blank all current items out
		for (i=1;i<=totalItems;i++) {
			document.getElementById("itemListID_" + i).className = "bmc_productBox2 fl";
		}
		
		if (pageNum > 1) {
			document.getElementById("paginationPreviousPage").className = "fl previousActive";
		}
		else {
			document.getElementById("paginationPreviousPage").className = "fl previousInactive";
		}
		
		if (pageNum == totalPages) {
			document.getElementById("paginationNextPage").className = "fl nextInactive";
		}
		else {
			document.getElementById("paginationNextPage").className = "fl nextActive";
		}

		// Set pagination
		if (curPage == 0) { curPage = 1 }

		document.getElementById("pageLink_" + curPage).className = "inactivePageNum2";
		
		if (pageNum > 0) {
			document.getElementById("pageLink_" + pageNum).className = "activePageNum2";
		}
		// Scroll to top
		window.scroll(0,20)
		
		// Get items to show
		if (pageNum == 0) { nowShow = perPage } else { nowShow = pageNum * perPage }

		curPage = pageNum
		
		if (useFilter == "" && useFilter2 == "" && useFilter3 == "") {
			document.getElementById("noProducts").className = "alertBoxHidden";
			for (i=(nowShow-(perPage-1));i<=nowShow;i++) {
				if (document.getElementById("itemListID_" + i) != undefined) {
					document.getElementById("itemListID_" + i).className = "bmc_productBox fl";
					curProduct = document.getElementById("picDiv_" + i).innerHTML
					document.getElementById("picDiv_" + i).innerHTML = curProduct.replace(/<tempimg/gi,'<img')
				}
			}

			if (pageNum == 0) { 
				updatePagination(1) 
			} 
			else { 
				newPageTotal = totalItems/perPage
				updatePagination(newPageTotal)
			}
		}
		else {
			filterItemCnt = 0
			if (pageNum == 0) {
				lastProductNum = totalItems
				firstProductNum = 0
			}
			else {
				lastProductNum = pageNum * perPage
				firstProductNum = lastProductNum - perPage
			}
			for (i=1;i<=totalItems;i++) {
				checkSub = document.getElementById("subType_" + i).innerHTML
				checkPrice = document.getElementById("price_" + i).innerHTML
				checkCustom = document.getElementById("custom_" + i).innerHTML
				itemDesc = document.getElementById("itemDesc_" + i).innerHTML
				colorMatch = 0
				
				if (checkPrice < 5) { checkPrice = 4 }
				if (checkPrice >= 5 && checkPrice < 10) { checkPrice = 5 }
				if (checkPrice >= 10 && checkPrice < 20) { checkPrice = 10 }
				if (checkPrice >= 20 && checkPrice < 30) { checkPrice = 20 }
				if (checkPrice >= 30) { checkPrice = 30 }
				
				if (useFilter3 != "") {
					splitResults = useFilter3.split("##");
					for(x = 0; x < splitResults.length; x++){
						if (splitResults[x] != "") {
							if (itemDesc.indexOf(splitResults[x]) >= 0) {
								colorMatch = 1
							}
						}
					}
				}
				
				if (useFilter != "") {
					if (useFilter.indexOf('##' + checkSub + '##') >= 0) {
						// Found an item that matches the array
						if (useFilter2 == "" || useFilter2.indexOf('##' + checkPrice + '##') >= 0) {
							if (useFilter3 == "" || colorMatch == 1) {
								filterItemCnt++
								if (filterItemCnt > firstProductNum && filterItemCnt <= lastProductNum) {
									document.getElementById("itemListID_" + i).className = "bmc_productBox fl";
									curProduct = document.getElementById("picDiv_" + i).innerHTML
									document.getElementById("picDiv_" + i).innerHTML = curProduct.replace(/<tempimg/gi,'<img')
								}
							}
						}
					}
					if (useFilter == "##1284##") {
						if (checkCustom == "True") {
							filterItemCnt++
							if (filterItemCnt > firstProductNum && filterItemCnt <= lastProductNum) {
								document.getElementById("itemListID_" + i).className = "bmc_productBox fl";
								curProduct = document.getElementById("picDiv_" + i).innerHTML
								document.getElementById("picDiv_" + i).innerHTML = curProduct.replace(/<tempimg/gi,'<img')
							}
						}
					}
				}
				else if (useFilter2 != "") {
					if (useFilter2.indexOf('##' + checkPrice + '##') >= 0) {
						if (useFilter3 == "" || colorMatch == 1) {
							filterItemCnt++
							if (filterItemCnt > firstProductNum && filterItemCnt <= lastProductNum) {
								document.getElementById("itemListID_" + i).className = "bmc_productBox fl";
								curProduct = document.getElementById("picDiv_" + i).innerHTML
								document.getElementById("picDiv_" + i).innerHTML = curProduct.replace(/<tempimg/gi,'<img')
							}
						}
					}
				}
				else if (useFilter3 != "") {
					if (colorMatch == 1) {
						filterItemCnt++
						if (filterItemCnt > firstProductNum && filterItemCnt <= lastProductNum) {
							document.getElementById("itemListID_" + i).className = "bmc_productBox fl";
							curProduct = document.getElementById("picDiv_" + i).innerHTML
							document.getElementById("picDiv_" + i).innerHTML = curProduct.replace(/<tempimg/gi,'<img')
						}
					}
				}
			}
			if (filterItemCnt == 0) {
				document.getElementById("noProducts").className = "alertBox";
			}
			else {
				document.getElementById("noProducts").className = "alertBoxHidden";
			}
			newPageTotal = filterItemCnt/perPage
			updatePagination(newPageTotal)
		}
		// Update current page
	}
	
	function updatePagination(newCnt) {
		var endingNum = parseInt(document.getElementById("endingProdNum").innerHTML);
		if (parseInt(newCnt) < newCnt) { newCnt = parseInt(newCnt) + 1 }
		document.getElementById("totalItemCnt").innerHTML = filterItemCnt;
		if (document.getElementById("postDots") != null) {
			if (newCnt < 7) { document.getElementById("postDots").className = "hide" } else { document.getElementById("postDots").className = "fl extraPages" }
		}
		if (newCnt == 1 || curPage == newCnt) { document.getElementById("paginationNextPage").className = "fl nextInactive" } else { document.getElementById("paginationNextPage").className = "fl nextActive" }
		if (curPage == 1) { document.getElementById("endingProdNum").innerHTML = perPage }
		endingNum = parseInt(document.getElementById("endingProdNum").innerHTML);
		if (endingNum > filterItemCnt) { document.getElementById("endingProdNum").innerHTML = filterItemCnt } else { document.getElementById("endingProdNum").innerHTML = perPage }
		if (document.getElementById("maxPageNum") != null) { document.getElementById("maxPageNum").innerHTML = newCnt }
		for(i=1;i<=totalPages;i++) {
			if (i > newCnt) {
				if (document.getElementById("pageLinkBox_" + i) != null) {
					document.getElementById("pageLinkBox_" + i).className = "paginationLink2";
				}
			}
			else {
				if (document.getElementById("pageLinkBox_" + i) != null) {
					if (document.getElementById("pageLinkBox_" + i).className == "paginationLink1 hide") {
						document.getElementById("pageLinkBox_" + i).className = "paginationLink1 hide";
					}
					else {
						document.getElementById("pageLinkBox_" + i).className = "paginationLink1";
					}
				}
			}
		}
		if (newCnt > 0) {
			linkData = document.getElementById("lowerPagination").innerHTML.replace(/pageLink/g,'upperLink')
			linkData = linkData.replace(/pageLinkBox/g,'upperLinkBox')
			linkData = linkData.replace(/paginationPreviousPage/g,'copyOf_paginationPreviousPage')
			linkData = linkData.replace(/paginationNextPage/g,'copyOf_paginationNextPage')
			linkData = linkData.replace(/preDots/g,'copyOf_preDots')
			linkData = linkData.replace(/postDots/g,'copyOf_postDots')
			linkData = linkData.replace(/totalItemCnt/g,'copyOf_totalItemCnt')
			linkData = linkData.replace(/startingProdNum/g,'copyOf_startingProdNum')
			linkData = linkData.replace(/endingProdNum/g,'copyOf_endingProdNum')
			linkData = linkData.replace(/maxPageNum/g,'copyOf_maxPageNum')
			
			document.getElementById("upperPagination").innerHTML = linkData
		}
		else {
			document.getElementById("upperPagination").innerHTML = ""
		}
	}
	
	// Update filter settings
	function updateFilter() {
		useFilter = "##"
		useFilter2 = "##"
		useFilter3 = "##"
		for(i=0;i<document.filterForm.elements.length;i++) {
			if (document.filterForm.elements[i].checked == true) {
				if (document.filterForm.elements[i].name == "styleType") {
					useFilter = useFilter + document.filterForm.elements[i].value + '##'
				}
				else if (document.filterForm.elements[i].name == "colorSelect") {
					useFilter3 = useFilter3 + document.filterForm.elements[i].value + '##'
				}
				else {
					useFilter2 = useFilter2 + document.filterForm.elements[i].value + '##'
				}
			}
		}
		if (useFilter == "##") { useFilter = "" }
		if (useFilter2 == "##") { useFilter2 = "" }
		if (useFilter3 == "##") { useFilter3 = "" }
		showPage(1)
	}
	
	function sortProduct(brandID,modelID,categoryID,sb) {
		document.getElementById("allProducts").innerHTML = "<div class='alertBox'>Sorting Products<br />Please Wait</div>"
		document.getElementById("testZone").innerHTML = '/ajax/bmcSortProducts.asp?brandID=' + brandID + '&modelID=' + modelID + '&categoryID=' + categoryID + '&sb=' + sb
		ajax('/ajax/bmcSortProducts.asp?brandID=' + brandID + '&modelID=' + modelID + '&categoryID=' + categoryID + '&sb=' + sb,'allProducts')
		setTimeout("delayUpdate()",300)
	}
	
	function delayUpdate() {
		if (document.getElementById("allProducts").innerHTML.indexOf("Sorting Products") > 0) {
			setTimeout("delayUpdate()",300)
		}
		else {
			setTimeout("updateFilter()",500)
		}
	}
	
	function itemsPerPage(amount) {
		if (amount == 0) {
			showPage(0);
		}
		else {
			cntPerPage = amount;
			showPage(1);
		}
	}	