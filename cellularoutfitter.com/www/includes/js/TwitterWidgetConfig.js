new TWTR.Widget({
  version: 2,
  type: 'profile',
  rpp: 4,
  interval: 6000,
  width: 186,
  height: 350,
  theme: {
    shell: {
      background: '#1364ab',
      color: '#7bff00'
    },
    tweets: {
      background: '#ffffff',
      color: '#000000',
      links: '#026c99'
    }
  },
  features: {
    scrollbar: true,
    loop: false,
    live: true,
    hashtags: true,
    timestamp: true,
    avatars: true,
    behavior: 'all'
  }
}).render().setUser('CellOutfitter').start();