var curTab = 1

function chgBanner(imgLink,imgLoc,cell) {
	if (curTab != cell) {
		document.getElementById("cell-" + curTab).className = "inactiveBigTab"
		document.getElementById("cell-" + cell).className = "activeBigTab"
		curTab = cell
		if (imgLink.substring(1,4) == "http") {
			document.getElementById("curBanner").innerHTML = '<a href="' + imgLink + '" target="_blank" id="link1"><img src="/images/mainbanner/' + imgLoc + '" border="0" id="imgLarge" width="627" height="275"></a>'
		}
		else {
			document.getElementById("curBanner").innerHTML = '<a href="' + imgLink + '" id="link1"><img src="/images/mainbanner/' + imgLoc + '" border="0" id="imgLarge" width="627" height="275"></a>'
		}
	}
}

function onSearchbox(dir) {
	if (dir == 0) {
		if (document.frmNxtAc.keywords.value == "") {
			document.frmNxtAc.keywords.value = "Keyword/Brand"
		}
	}
	else {
		if (document.frmNxtAc.keywords.value == "Keyword/Brand") {
			document.frmNxtAc.keywords.value = ""
		}
	}
}

var rowSelect = 0

function getKey(e) {
	if (!e) var e = window.event;
	// e gives access to the event in all browsers
	curKey = e.keyCode
	findSearchMatch(curKey)
}

function findSearchMatch(curKey) {
	curVal = document.frmNxtAc.keywords.value
	
	if (curKey == 40) {
		rowSelect++
		if (rowSelect > 6) {
			rowSelect = 6
		}
		else {
			//document.getElementById("curRowNum").innerHTML = "rowSelect:" + rowSelect
			document.frmNxtAc.keywords.value = document.getElementById("searchRow_" + rowSelect).innerHTML
		}
	}
	else if (curKey == 38) {
		rowSelect--
		if (rowSelect < 1) {
			rowSelect = 0
		}
		else {
			//document.getElementById("curRowNum").innerHTML = "rowSelect:" + rowSelect
			document.frmNxtAc.keywords.value = document.getElementById("searchRow_" + rowSelect).innerHTML
		}
	}
	else {
		rowSelect = 0
		if (curVal != "") {
			ajax('/ajax/searchHelp.asp?curVal=' + curVal,'searchResults')
			setTimeout("testSearchReturn()",500)
		}
		else {
			document.getElementById("searchResults").style.display = 'none'
		}
	}
}

function testSearchReturn() {
	if (document.getElementById("searchResults").innerHTML == "&nbsp;") {
		document.getElementById("searchResults").style.display = 'none'
	}
	else {
		document.getElementById("searchResults").style.display = ''
	}
}

function hideSearchHelp() {
	rowSelect = 0
	setTimeout("document.getElementById('searchResults').style.display = 'none'",500)
}

function showMetaTags() {
	if (document.getElementById('metaTagBox').style.display == '') {
		document.getElementById('metaTagBox').style.display = 'none'
	}
	else {
		document.getElementById('defaultMetaTags').style.display = 'none'
		document.getElementById('metaTagBox').style.display = ''
	}
}

function showDefaultMetaTags() {
	if (document.getElementById('defaultMetaTags').style.display == '') {
		document.getElementById('defaultMetaTags').style.display = 'none'
	}
	else {
		document.getElementById('metaTagBox').style.display = 'none'
		document.getElementById('defaultMetaTags').style.display = ''
	}
}

/*
ajax('/ajax/accountMenu.asp','account_nav');
ajax('/ajax/accountMenu_b.asp','account_nav_b');
*/

function addNewsletter(semail) {
	if(typeof(_vis_opt_top_initialize) == "function") {
    	// Code for Custom Goal: Email Sign-Up
	     _vis_opt_goal_conversion(204);
	    // uncomment the following line to introduce a half second pause to ensure goal always registers with the server
	    // _vis_opt_pause(500);
    }
	
	ajax("/ajax/newsletter.asp?semail=" + semail,'popBox');
	document.getElementById("popBox").style.position = "absolute";
	document.getElementById("popCover").style.display = "";
	document.getElementById("popBox").style.display = "";
	jQuery(window).trigger('newsletterSigup');
}

function showLoginDialog() {
	var randomnumber=Math.floor(Math.random()*100001);
	ajax("/ajax/accountLoginDialog.asp?rn="+randomnumber,'popBox');
	document.getElementById("popBox").style.position = "absolute";
	//document.getElementById("popCover").onclick = function () { closeFloatingBox() }
	//document.getElementById("popBox").onclick = function () { closeFloatingBox() }
	document.getElementById("popCover").style.display = "";
	document.getElementById("popBox").style.display = "";
}
function closeFloatingBox() {
	//------------------ change it to original -------------------//
	//document.getElementById("popBox").style.position = "fixed";
	//document.getElementById("popCover").onclick = function () {}
	//document.getElementById("popBox").onclick = function () {}
	//------------------------------------------------------------//
	document.getElementById("popCover").style.display = "none";
	document.getElementById("popBox").style.display = "none";
	document.getElementById("popCover").innerHTML = "";
	document.getElementById("popBox").innerHTML = "";
}	
function removeGhost(strF) {
	//var f = document.getElementsByName(strF).item(0).value;
	var f = strF.value
	if (f == 'Email Address' || f == 'Password') {
		//document.getElementsByName(strF).item(0).value = '';
		strF.value = '';
	}
}
function replaceT(obj){
	var newO=document.createElement('input');
	newO.setAttribute('type','password');
	newO.setAttribute('name',obj.getAttribute('name'));
	newO.setAttribute('id',obj.getAttribute('id'));
	obj.parentNode.replaceChild(newO,obj);
	newO.focus();
}

function closeReviewPopup() {
	document.getElementById('popCover').style.display = 'none';
	document.getElementById('popBox').style.display = 'none';
	document.getElementById('popBox').innerHTML = '';
}

function showEmailOfferTab() {
	var obj = document.getElementById("floatingTab_email");
	var curPos = obj.style.bottom;
	curPos = parseInt(curPos.replace("px",""));
	if (curPos == -75) {
		document.getElementById("floatingTabValue").innerHTML = "-"
		setTimeout("emailOfferUp()",20);
	}
	else {
		document.getElementById("floatingTabValue").innerHTML = "+"
		setTimeout("emailOfferDown()",20);
	}
}

function emailOfferUp() {
	var obj = document.getElementById("floatingTab_email");
	var curPos = obj.style.bottom;
	curPos = parseInt(curPos.replace("px",""));
	curPos = curPos + 5;
	if (curPos <= 0) {
		obj.style.bottom = curPos + "px";
		setTimeout("emailOfferUp()",20);
	}
	ajax('/ajax/newsletter.asp?semail=expanded','emailDumpZone');
}

function emailOfferDown() {
	var obj = document.getElementById("floatingTab_email");
	var curPos = obj.style.bottom;
	curPos = parseInt(curPos.replace("px",""));
	curPos = curPos - 5;
	if (curPos >= -75) {
		obj.style.bottom = curPos + "px";
		setTimeout("emailOfferDown()",20);
	}
	ajax('/ajax/newsletter.asp?semail=collapsed','emailDumpZone');
}

/* email widget additional codes */
if(document.getElementById('emailDumpZone')){
	document.getElementById('emailDumpZone').innerHTML = '';
}
ajax('/ajax/newsletter.asp?semail=checkCouponUsed','emailDumpZone');
checkWidgetExpand('checkCouponUsed');

function checkWidgetExpand(checkType)
{
	dumpZone = "";
	if(document.getElementById("emailDumpZone")){
		dumpZone = document.getElementById('emailDumpZone').innerHTML;
	}
	if (checkType == "checkCouponUsed") {
		if ("" == dumpZone) setTimeout('checkWidgetExpand(\'checkCouponUsed\')', 50);
		else if ("" != dumpZone) {
			if (dumpZone == "show_expand") {
				document.getElementById('floatingTab_email').style.display = '';
				showEmailOfferTab();
			} else if (dumpZone == "show_collapse") {
				document.getElementById('floatingTab_email').style.display = '';
			}
		}
	}
}