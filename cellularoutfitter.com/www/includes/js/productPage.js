	function plusOneBox(dir) {
		if (dir == 1) {
			document.getElementById('plusOneDetails').style.display = ''
		}
		else {
			document.getElementById('plusOneDetails').style.display = 'none'
		}
	}
	
	function fnPreviewImage(src) {
		var p = document.getElementById('imgLarge');
		p.src = src;
	}
	
	function togglePdpTab(showIdx, nTabs) {
		for (i=1; i<=nTabs; i++) {
			document.getElementById("tabHeaderLeft"+i).src = '/images/backgrounds/productTab_off_left.gif';
			document.getElementById("tabHeader"+i).className = 'prodTab_off';
			document.getElementById("tabHeaderRight"+i).src = '/images/backgrounds/productTab_off_right.gif';
			document.getElementById("tabBody"+i).style.display = 'none';
		}
		document.getElementById("tabHeaderLeft"+showIdx).src = '/images/backgrounds/productTab_on_left.gif';
		document.getElementById("tabHeader"+showIdx).className = 'prodTab_on';
		document.getElementById("tabHeaderRight"+showIdx).src = '/images/backgrounds/productTab_on_right.gif';
		document.getElementById("tabBody"+showIdx).style.display = '';
	}
	
	function showDetails() {
		document.getElementById("prodDescLeft").src = '/images/backgrounds/productTab_on_left.gif'
		document.getElementById("prodDescDiv").className = 'prodTab_on'
		document.getElementById("prodDescRight").src = '/images/backgrounds/productTab_on_right.gif'
		
		document.getElementById("prodCompLeft").src = '/images/backgrounds/productTab_off_left.gif'
		document.getElementById("prodCompDiv").className = 'prodTab_off'
		document.getElementById("prodCompRight").src = '/images/backgrounds/productTab_off_right.gif'
		
		document.getElementById("prodReviewLeft").src = '/images/backgrounds/productTab_off_left.gif'
		document.getElementById("prodReviewDiv").className = 'prodTab_off'
		document.getElementById("prodReviewRight").src = '/images/backgrounds/productTab_off_right.gif'
		
		document.getElementById("prodComp").style.display = 'none'
		document.getElementById("prodReview").style.display = 'none'
		document.getElementById("prodDetails").style.display = ''
	}
	
	function showComp() {
		document.getElementById("prodCompLeft").src = '/images/backgrounds/productTab_on_left.gif'
		document.getElementById("prodCompDiv").className = 'prodTab_on'
		document.getElementById("prodCompRight").src = '/images/backgrounds/productTab_on_right.gif'
		
		document.getElementById("prodDescLeft").src = '/images/backgrounds/productTab_off_left.gif'
		document.getElementById("prodDescDiv").className = 'prodTab_off'
		document.getElementById("prodDescRight").src = '/images/backgrounds/productTab_off_right.gif'
		
		document.getElementById("prodReviewLeft").src = '/images/backgrounds/productTab_off_left.gif'
		document.getElementById("prodReviewDiv").className = 'prodTab_off'
		document.getElementById("prodReviewRight").src = '/images/backgrounds/productTab_off_right.gif'
		
		document.getElementById("prodDetails").style.display = 'none'
		document.getElementById("prodReview").style.display = 'none'
		document.getElementById("prodComp").style.display = ''
	}
	
	function showReview() {
		document.getElementById("prodReviewLeft").src = '/images/backgrounds/productTab_on_left.gif'
		document.getElementById("prodReviewDiv").className = 'prodTab_on'
		document.getElementById("prodReviewRight").src = '/images/backgrounds/productTab_on_right.gif'
		
		document.getElementById("prodDescLeft").src = '/images/backgrounds/productTab_off_left.gif'
		document.getElementById("prodDescDiv").className = 'prodTab_off'
		document.getElementById("prodDescRight").src = '/images/backgrounds/productTab_off_right.gif'
		
		document.getElementById("prodCompLeft").src = '/images/backgrounds/productTab_off_left.gif'
		document.getElementById("prodCompDiv").className = 'prodTab_off'
		document.getElementById("prodCompRight").src = '/images/backgrounds/productTab_off_right.gif'
		
		document.getElementById("prodDetails").style.display = 'none'
		document.getElementById("prodComp").style.display = 'none'
		document.getElementById("prodReview").style.display = ''
	}