	var cntPerPage = 40;

	// Set default (on page load)
	setTimeout("updateFilter()",1000)

	// Update visible content
	function showPage(pageNum) {
		if (pageNum == 0) { perPage = totalItems } else { perPage = cntPerPage }
		// Blank all current items out
		for (i=1;i<=totalItems;i++) {
			document.getElementById("itemListID_" + i).className = "bmc_productBox2 fl";
		}

		// Set pagination
		if (curPage == 0) { curPage = 1 }

		document.getElementById("pageLink_" + curPage).className = "inactivePageNum2";
		
		if (pageNum > 0) {
			document.getElementById("pageLink_" + pageNum).className = "activePageNum2";
		}
		// Scroll to top
		window.scroll(0,20)
		
		// Get items to show
		if (pageNum == 0) { nowShow = perPage } else { nowShow = pageNum * perPage }

		if (useFilter == "" && useFilter2 == "") {
			document.getElementById("noProducts").className = "alertBoxHidden";
			for (i=(nowShow-(perPage-1));i<=nowShow;i++) {
				if (document.getElementById("itemListID_" + i) != undefined) {
					document.getElementById("itemListID_" + i).className = "bmc_productBox fl";
					curProduct = document.getElementById("picDiv_" + i).innerHTML
					document.getElementById("picDiv_" + i).innerHTML = curProduct.replace(/<tempimg/gi,'<img')
				}
			}

			if (pageNum == 0) { 
				updatePagination(1) 
			} 
			else { 
				newPageTotal = totalItems/perPage
				updatePagination(newPageTotal)
			}
		}
		else {
			filterItemCnt = 0
			if (pageNum == 0) {
				lastProductNum = totalItems
				firstProductNum = 0
			}
			else {
				lastProductNum = pageNum * perPage
				firstProductNum = lastProductNum - perPage
			}
			for (i=1;i<=totalItems;i++) {
				checkSub = document.getElementById("subType_" + i).innerHTML
				checkPrice = document.getElementById("price_" + i).innerHTML
				checkCustom = document.getElementById("custom_" + i).innerHTML
				if (checkPrice < 5) { checkPrice = 4 }
				if (checkPrice >= 5 && checkPrice < 10) { checkPrice = 5 }
				if (checkPrice >= 10 && checkPrice < 20) { checkPrice = 10 }
				if (checkPrice >= 20 && checkPrice < 30) { checkPrice = 20 }
				if (checkPrice >= 30) { checkPrice = 30 }
				if (useFilter != "") {
					if (useFilter.indexOf('##' + checkSub + '##') >= 0) {
						// Found an item that matches the array
						if (useFilter2 == "" || useFilter2.indexOf('##' + checkPrice + '##') >= 0) {
							filterItemCnt++
							if (filterItemCnt > firstProductNum && filterItemCnt <= lastProductNum) {
								document.getElementById("itemListID_" + i).className = "bmc_productBox fl";
								curProduct = document.getElementById("picDiv_" + i).innerHTML
								document.getElementById("picDiv_" + i).innerHTML = curProduct.replace(/<tempimg/gi,'<img')
							}
						}
					}
					if (useFilter == "##1284##") {
						if (checkCustom == "True") {
							filterItemCnt++
							if (filterItemCnt > firstProductNum && filterItemCnt <= lastProductNum) {
								document.getElementById("itemListID_" + i).className = "bmc_productBox fl";
								curProduct = document.getElementById("picDiv_" + i).innerHTML
								document.getElementById("picDiv_" + i).innerHTML = curProduct.replace(/<tempimg/gi,'<img')
							}
						}
					}
				}
				else {
					if (useFilter2.indexOf('##' + checkPrice + '##') >= 0) {
						// Found an item that matches the array
						filterItemCnt++
						if (filterItemCnt > firstProductNum && filterItemCnt <= lastProductNum) {
							document.getElementById("itemListID_" + i).className = "bmc_productBox fl";
							curProduct = document.getElementById("picDiv_" + i).innerHTML
							document.getElementById("picDiv_" + i).innerHTML = curProduct.replace(/<tempimg/gi,'<img')
						}
					}
				}
			}
			if (filterItemCnt == 0) {
				document.getElementById("noProducts").className = "alertBox";
			}
			else {
				document.getElementById("noProducts").className = "alertBoxHidden";
			}
			newPageTotal = filterItemCnt/perPage
			updatePagination(newPageTotal)
		}
		// Update current page
		curPage = pageNum
	}
	
	function updatePagination(newCnt) {
		if (parseInt(newCnt) < newCnt) { newCnt = parseInt(newCnt) + 1 }
		for(i=1;i<=totalPages;i++) {
			if (i > newCnt) {
				document.getElementById("pageLinkBox_" + i).className = "paginationLink2";
			}
			else {
				document.getElementById("pageLinkBox_" + i).className = "paginationLink1";
			}
		}
		if (newCnt > 0) {
			linkData = document.getElementById("lowerPagination").innerHTML.replace(/pageLink/g,'upperLink')
			linkData = linkData.replace(/pageLinkBox/g,'upperLinkBox')
			document.getElementById("upperPagination").innerHTML = linkData
		}
		else {
			document.getElementById("upperPagination").innerHTML = ""
		}
	}
	
	// Update filter settings
	function updateFilter() {
		useFilter = "##"
		useFilter2 = "##"
		for(i=0;i<document.filterForm.elements.length;i++) {
			if (document.filterForm.elements[i].checked == true) {
				if (document.filterForm.elements[i].name == "styleType") {
					useFilter = useFilter + document.filterForm.elements[i].value + '##'
				}
				else {
					useFilter2 = useFilter2 + document.filterForm.elements[i].value + '##'
				}
			}
		}
		if (useFilter == "##") { useFilter = "" }
		if (useFilter2 == "##") { useFilter2 = "" }
		showPage(1)
	}
	
	function sortProduct(brandID,modelID,categoryID,sb) {
		document.getElementById("allProducts").innerHTML = "<div class='alertBox'>Sorting Products<br />Please Wait</div>"
		document.getElementById("testZone").innerHTML = '/ajax/bmcSortProducts.asp?brandID=' + brandID + '&modelID=' + modelID + '&categoryID=' + categoryID + '&sb=' + sb
		ajax('/ajax/bmcSortProducts.asp?brandID=' + brandID + '&modelID=' + modelID + '&categoryID=' + categoryID + '&sb=' + sb,'allProducts')
		setTimeout("delayUpdate()",300)
	}
	
	function delayUpdate() {
		if (document.getElementById("allProducts").innerHTML.indexOf("Sorting Products") > 0) {
			setTimeout("delayUpdate()",300)
		}
		else {
			setTimeout("updateFilter()",500)
		}
	}
	
	function itemsPerPage(amount) {
		if (amount == 0) {
			showPage(0);
		}
		else {
			cntPerPage = amount;
			showPage(1);
		}
	}	