$(document).ready( function () {
	
	
	
	$("#divButtonShowToggle").click( function() {
		$("#divExpandedDetails").toggleClass("hideBorder").slideToggle("medium", function() {
			$(this).toggleClass("hideBorder");
		});
		var text = $('#divButtonShowToggle').text() == 'Show Details' ? 'Hide Details' : 'Show Details';
		$(this).text(text);
	}).addClass("clickable");

	$("#divExpandedDetails").toggle(); //Hides Expanded Details section after the page has loaded
	$(".upcomingItemHoverContainer").toggle();
	$(".upcomingItemContainer").each(function (idx) {
		$(this).hover( function () {
			$("#upcomingItemHoverContainer_"+eval(idx + 1)).toggle();
		} );
	});
	//Update portions of page periodically
	setInterval( function () {
		jQuery.ajax({
			type: "GET",
			url: page,
			data: null,
			dataType: "html",
			success: function (data, textStatus, XMLHttpRequest) {
					//udpate multiple sections; create array of targets; forreach target update innerHTML
					$.each(['#forceRefresh','#divRandom'], function (index, value) {
						$(value).html($(data).find(value));
					});
					if ($('#forceRefresh').text() == 'True') {
						location.reload(true); 
					};
				},
			cache: false
		});
	}, 10000);
	
});

function showReviews() {
	if ($('#divExpandedDetails').is(':not(:visible)')) {
		$('#divButtonShowToggle').click();
	}
	$('html, body').animate({
         scrollTop: $("#divProductReviews").offset().top
     }, 2000);
}

var Timer;
var TotalSeconds;
function CreateTimer(TimerID, Time) {
        Timer = document.getElementById(TimerID);
        TotalSeconds = Time;
        
        UpdateTimer()
        window.setTimeout("Tick()", 1000);
}
function Tick() {
        TotalSeconds -= 1;
        UpdateTimer()
        window.setTimeout("Tick()", 1000);
}
function Tick() {
        if (TotalSeconds <= 0) {
                //alert("Time's up!");
				location.reload(true); 
                return;
        }

        TotalSeconds -= 1;
        UpdateTimer()
        window.setTimeout("Tick()", 1000);
}
function UpdateTimer() {
        var Seconds = TotalSeconds;
        
        var Days = Math.floor(Seconds / 86400);
        Seconds -= Days * 86400;

        var Hours = Math.floor(Seconds / 3600);
        Seconds -= Hours * (3600);

        var Minutes = Math.floor(Seconds / 60);
        Seconds -= Minutes * (60);

		//Target Format: 1h 54m 14s
        //var TimeStr = ((Days > 0) ? Days + " days " : "") + '<span class="digit">' + LeadingZero(Hours) + '</span><span class="colon">:</span><span class="digit">' + LeadingZero(Minutes) + '</span><span class="colon">:</span><span class="digit">' + LeadingZero(Seconds) + '</span>';
        var TimeStr = '<span class="digit">' + LeadingZero(Hours) + '</span><span class="colon">:</span><span class="digit">' + LeadingZero(Minutes) + '</span><span class="colon">:</span><span class="digit">' + LeadingZero(Seconds) + '</span>';
		var TimeStrSimple = ((Days > 0) ? Days + " days " : "") + Hours + "h " + Minutes + "m " + Seconds + "s";

        Timer.innerHTML = TimeStr;
		$(".divSecondaryTimer").text(TimeStrSimple);
}
function LeadingZero(Time) {

        return (Time < 10) ? "0" + Time : + Time;

}

var Timer2;
var TotalSeconds2;
function CreateTimer2(Timer2ID, Time) {
        Timer2 = document.getElementById(Timer2ID);
        TotalSeconds2 = Time;
        
        UpdateTimer2()
        window.setTimeout("Tick2()", 1000);
}
function Tick2() {
        TotalSeconds2 -= 1;
        UpdateTimer2()
        window.setTimeout("Tick2()", 1000);
}
function Tick2() {
        if (TotalSeconds2 <= 0) {
                //alert("Time's up!");
				location.reload(true); 
                return;
        }

        TotalSeconds2 -= 1;
        UpdateTimer2()
        window.setTimeout("Tick2()", 1000);
}
function UpdateTimer2() {
        var Seconds = TotalSeconds2;
        
        var Days = Math.floor(Seconds / 86400);
        Seconds -= Days * 86400;

        var Hours = Math.floor(Seconds / 3600);
        Seconds -= Hours * (3600);

        var Minutes = Math.floor(Seconds / 60);
        Seconds -= Minutes * (60);

		//Target Format: 1h 54m 14s
        var TimeStr = ((Days > 0) ? Days + " days " : "") + Hours + "h " + Minutes + "m " + Seconds + "s";

        Timer2.innerHTML = TimeStr;
}

var Timer3;
var TotalSeconds3;
function CreateTimer3(Timer3ID, Time) {
        Timer3 = document.getElementById(Timer3ID);
        TotalSeconds3 = Time;
        
        UpdateTimer3()
        window.setTimeout("Tick3()", 1000);
}
function Tick3() {
        TotalSeconds3 -= 1;
        UpdateTimer3()
        window.setTimeout("Tick3()", 1000);
}
function Tick3() {
        if (TotalSeconds3 <= 0) {
                //alert("Time's up!");
				location.reload(true); 
                return;
        }

        TotalSeconds3 -= 1;
        UpdateTimer3()
        window.setTimeout("Tick3()", 1000);
}
function UpdateTimer3() {
        var Seconds = TotalSeconds3;
        
        var Days = Math.floor(Seconds / 86400);
        Seconds -= Days * 86400;

        var Hours = Math.floor(Seconds / 3600);
        Seconds -= Hours * (3600);

        var Minutes = Math.floor(Seconds / 60);
        Seconds -= Minutes * (60);

		//Target Format: 1h 54m 14s
        var TimeStr = ((Days > 0) ? Days + " days " : "") + Hours + "h " + Minutes + "m " + Seconds + "s";

        Timer3.innerHTML = TimeStr;
}



/*	jQuery Plugins	*/

