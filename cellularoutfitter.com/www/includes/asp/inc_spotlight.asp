							<tr>
								<td>
                                    <table width="775" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td><img src="images/productHighlight.jpg" border="0" alt="Product Highlight"></td>
                                            <td width="620">
                                                <table width="615" border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tr>
                                                        <%
                                                        dim myItemDesc_CO
                                                        set RS = Server.CreateObject("ADODB.Recordset")
                                                        SQL = "SELECT A.*, B.itemdesc_CO, B.itempic_CO, B.price_retail, B.price_CO FROM CO_spotlight A INNER JOIN we_items B ON A.itemid = B.itemid WHERE B.hidelive = 0 AND B.inv_qty <> 0 ORDER BY sortOrder"
                                                        RS.open SQL, oConn, 3, 3
                                                        pricing = ""
														do until RS.eof
                                                            if len(RS("itemdesc_CO")) > 50 then
                                                                myItemDesc_CO = left(RS("itemdesc_CO"),50) & "..."
                                                            else
                                                                myItemDesc_CO = RS("itemdesc_CO")
                                                            end if
															pricing = pricing & "<span style='color:#999; text-decoration:line-through; font-size:12px;'>" & formatCurrency(RS("price_retail")) & "</span>&nbsp;&nbsp;<span style='color:#2468bb; font-size:14px; font-weight:bold;'>" & formatCurrency(RS("price_CO")) & "</span>|"
                                                            %>
                                                            <td width="150" align="center" valign="top" style="padding-left:20px">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                                                    <tr>
                                                                        <td align="center" valign="top"><a href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemdesc_CO"))%>.html"><img src="/productPics/thumb/<%=RS("itempic_CO")%>" alt="<%=RS("itemdesc_CO")%>" width="100" height="100" border="0"></a></td>
                                                                    </tr>
                                                                    <tr><td align="center" valign="top"><img src="/images/spacer.gif" width="1" height="20"></td></tr>
                                                                    <tr>
                                                                        <td align="left" valign="top"><a class="itemDesc-small" href="/p-<%=RS("itemid") & "-" & formatSEO(RS("itemdesc_CO"))%>.html" title="<%=RS("itemdesc_CO")%>"><%=myItemDesc_CO%></a></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <%
                                                            RS.movenext
                                                        loop
                                                        RS.close
                                                        set RS = nothing
                                                        %>
                                                    </tr>
                                                    <%
													pricingArray = split(pricing,"|")
													%>
                                                    <tr>
                                                    <%
													for i = 0 to ubound(pricingArray)
													%>
                                                    	<td align="left" style="padding-left:20px"><%=pricingArray(i)%></td>
                                                    <%
													next
													%>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
								</td>
							</tr>
							<tr>
								<td height="5"><img src="/images/spacer.gif" width="1" height="5" border="0"></td>
							</tr>
							<tr>
								<td>
									<table width="774" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td width="232" align="left" valign="top"><img src="images/header_new_Arrivals.gif" width="232" height="41" /></td>
											<td width="39" align="center" valign="middle">&nbsp;</td>
											<td width="232" align="center" valign="top"><img src="images/header_top_selling_items.gif" width="232" height="41" /></td>
											<td width="39" align="center" valign="middle">&nbsp;</td>
											<td width="232" align="right" valign="top"><img src="images/header_top_selling_models.gif" width="232" height="41" /></td>
										</tr>
										<tr>
											<td height="10" align="center" valign="top" colspan="5"><img src="/images/spacer.gif" width="1" height="10" border="0"></td>
										</tr>
										<tr>
											<td valign="top">
												<%
												SQL = "SELECT TOP 5 A.itemid, A.itemDesc_CO FROM we_items A INNER JOIN CO_newarrival B ON A.itemID = B.itemID"
												SQL = SQL & " WHERE A.inv_qty <> 0 AND A.hideLive = 0"
												SQL = SQL & " ORDER BY A.DateTimeEntd DESC"
												set rsTop = Server.CreateObject("ADODB.Recordset")
												rsTop.open SQL, oConn, 3, 3
												response.write "<ol class=""topsale"">" & vbcrlf
												do until rsTop.eof
													response.write "<li><a class=""topsale-link"" href=""/p-" & rsTop("itemid") & "-" & formatSEO(rsTop("itemDesc_CO")) & ".html"" title=""" & altText & """>" & rsTop("itemDesc_CO") & "</a></li>" & vbcrlf
													rsTop.movenext
												loop
												response.write "</ol>" & vbcrlf
												%>
											</td>
											<td align="center" valign="middle">&nbsp;</td>
											<td valign="top">
												<%
												SQL = "SELECT TOP 5 A.itemid, A.itemDesc_CO FROM we_items A INNER JOIN temp_ItemSales_Top10 B ON A.itemID = B.itemID"
												SQL = SQL & " WHERE A.inv_qty <> 0 AND A.hideLive = 0"
												SQL = SQL & " AND B.store = 2 ORDER BY B.quantity DESC"
												set rsTop = Server.CreateObject("ADODB.Recordset")
												rsTop.open SQL, oConn, 3, 3
												response.write "<ol class=""topsale"">" & vbcrlf
												do until rsTop.eof
													response.write "<li><a class=""topsale-link"" href=""/p-" & rsTop("itemid") & "-" & formatSEO(rsTop("itemDesc_CO")) & ".html"" title=""" & altText & """>" & rsTop("itemDesc_CO") & "</a></li>" & vbcrlf
													rsTop.movenext
												loop
												response.write "</ol>" & vbcrlf
												%>
											</td>
											<td align="center" valign="middle">&nbsp;</td>
											<td valign="top">
												<%
												SQL = "SELECT TOP 5 * FROM temp_TopModels WHERE store = 2 ORDER BY SumQty DESC"
												set rsTop = Server.CreateObject("ADODB.recordset")
												rsTop.open SQL, oConn, 3, 3
												response.write "<ol class=""topsale"">" & vbcrlf
												do until rsTop.eof
													response.write "<li><a class=""topsale-link"" href=""/m-" & rsTop("modelID") & "-" & formatSEO(rsTop("brandName")) & "-" & formatSEO(rsTop("modelName")) & "-cell-phone-accessories.html"" title=""" & rsTop("brandName") & " " & rsTop("modelName") & " Cell Phone Accessories"">" & rsTop("brandName") & " " & rsTop("modelName") & "</a>" & vbcrlf
													rsTop.movenext
												loop
												response.write "</ol>" & vbcrlf
												rsTop.close
												set rsTop = nothing
												%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
