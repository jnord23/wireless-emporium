<%
useHttp = "http"
if Request.ServerVariables("SERVER_PORT_SECURE") = "1" then	useHttp = "https"
%>
<script src="/includes/js/ga_social_tracking.js"></script>

<!-- Load Facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=659959624029466";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<!-- Load Twitter JS-API asynchronously -->
<script>
	(function(){
	var twitterWidgets = document.createElement('script');
	twitterWidgets.type = 'text/javascript';
	twitterWidgets.async = true;
	twitterWidgets.src = '<%=useHttp%>://platform.twitter.com/widgets.js';
	
	// Setup a callback to track once the script loads.
	twitterWidgets.onload = _ga.trackTwitter;
	
	document.getElementsByTagName('head')[0].appendChild(twitterWidgets);
	})();
</script>
<div class="fr socialLinksHeader">
    <div style="float:right; width:47px; padding-top:15px; overflow:hidden;">
        <fb:like href="<%=canonicalURL%>" send="false" layout="button_count" width="47" show_faces="false" font="arial"></fb:like>
    </div>
    <div style="float:right; overflow:hidden; width:40px; padding-top:15px;">
        <g:plusone size="medium" count="false" href="<%=canonicalURL%>"></g:plusone>
    </div>
</div>