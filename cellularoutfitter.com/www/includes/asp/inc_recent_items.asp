<%
on error resume next
for formIndex = 1 to 3
	recentItem = Request.Cookies("RecentItem" & formIndex)("id")
	if inStr(recentItem,"/") > 0 then recentItem = left(recentItem,inStr(recentItem,"/")-1)
	if recentItem <> "" and isNumeric(recentItem) then Execute("thisRecentItem" & formIndex & "id" & "=" & recentItem)
next
on error goto 0

if thisRecentItem1ID <> "" or thisRecentItem2ID <> "" or thisRecentItem3ID <> "" then
	conditionalQuery = ""
	%>
<table border="0" bordercolor="#CCCCCC" bgcolor="#ededed" cellpadding="0" cellspacing="0" width="100%" style="border:1px solid #999;">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
				<tr><td><img src="/images/spacer.gif" width="1" height="10" border="0"></td></tr>
                <tr><td align="left" colspan="7"><img src="/images/product/recentlyViewed.jpg" border="0" /></td></tr>
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<%
								if thisRecentItem1ID <> "" then conditionalQuery = conditionalQuery & " ItemID = " & thisRecentItem1ID
								if thisRecentItem2ID <> "" then
									if conditionalQuery <> "" then conditionalQuery = conditionalQuery & " OR "
									conditionalQuery = conditionalQuery &  " ItemID = " & thisRecentItem2ID
								end if
								if thisRecentItem3ID <> "" then
								if conditionalQuery <> "" then conditionalQuery = conditionalQuery & " OR "
									conditionalQuery = conditionalQuery & " ItemID = " & thisRecentItem3ID
								end if
								call fOpenConn()
								SQLQuery = "SELECT TOP 3 b.brandName, c.modelName, a.partNumber,a.itemPic,a.itemID,a.itemDesc_CO,a.itempic_CO,a.price_Retail,a.price_CO FROM we_Items a left join we_brands b on a.brandID = b.brandID left join we_models c on a.modelID = c.modelID WHERE " & conditionalQuery
								'session("errorSQL") = SQLQuery
								set RS = oConn.execute(SQLQuery)
								if not RS.eof then
									a = 0
									do until RS.eof
										a = a + 1
										brandName = RS("brandName")
										modelName = RS("modelName")
										partNumber = RS("partNumber")
										itemPic = RS("itemPic")
										itemdesc = insertDetails(RS("itemdesc_CO"))
										if instr(partNumber,"DEC-SKN") > 0 then
											useImg = "/productpics/decalSkins/thumb/" & itemPic
										else
											useImg = "/productpics/thumb/" & RS("itempic_CO")
										end if
								%>
                                <td width="33%" align="center" valign="middle" style="cursor:pointer;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td width="100%" align="center" valign="middle">
                                            	<table bgcolor="#FFFFFF" border="0" cellpadding="3" cellspacing="0" style="border-bottom:1px solid #cccccc; border-left:1px solid #cccccc; border-right:1px solid #cccccc; border-top:1px solid #cccccc;">
                                                	<tr><td><img src="/images/blank.gif" border="0" width="130" height="15" /></td></tr>
                                                	<tr><td align="center"><a href="/p-<%=RS("itemid") & "-" & formatSEO(itemdesc)%>.html"><img src="<%=useImg%>" width="100" height="100" align="center" border="0" alt="<%=itemdesc%>"></a></td></tr>
                                                    <tr><td><img src="/images/blank.gif" border="0" width="130" height="15" /></td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr><td><img src="/images/blank.gif" border="0" width="1" height="5" /></td></tr>
                                        <tr><td align="center" valign="top" class="contain-reale-pro"><a href="/p-<%=RS("itemid") & "-" & formatSEO(itemdesc)%>.html" class="product-cart"><%=itemdesc%></a></td></tr>
                                        <tr><td><img src="/images/blank.gif" border="0" width="1" height="10" /></td></tr>
                                        <tr><td align="center" style="font-weight:bold;">Wholesale Price:&nbsp;<span style="color:#F00"><%=formatCurrency(RS("price_CO"))%></span></td></tr>
                                    </table>
                                </td>
								<%
										RS.movenext
									loop
									for i = a to 2
								%>
                                <td width="33%">&nbsp;</td>
                                <%
									next
								end if
								RS.close
								set RS = nothing
								call fCloseConn()
								%>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="100%" align="center" valign="top">
						<img src="/images/spacer.gif" width="1" height="5" border="0">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
	<%
end if
%>
