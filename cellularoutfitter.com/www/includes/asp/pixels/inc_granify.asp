<%
	gf_pageName = pageTitle
	if prepStr(gf_pageName) = "" then gf_pageName = prepStr(pageName)
	
	select case lcase(gf_pageName)
		case "home"
			gf_pageName = "home"	
		case "product.asp", "product"
			gf_pageName = "product"
		case "category"
			gf_pageName = "collection"
		case "category-brand"
			gf_pageName = "collection"
		case "brandNew2", "brands"
			gf_pageName = "collection"
			br_cat = brandName
		case "brand-model", "brandmodel"
			gf_pageName = "collection"
		case "bmc", "bmc_b"
			gf_pageName = "collection"
		case "bmcd"
			gf_pageName = "collection"
		case "nextopia search", "search"
			gf_pageName = "search"
		case "complete.asp"
			gf_pageName = "receipt"
		case "basket", "basket2"
			gf_pageName = "cart"
		case else
			gf_pageName = "other"
	end select
	%>
	<script type="text/javascript">
		try {
			Granify.trackPageView({
				page_type: "<%=gf_pageName%>"
			});
		}
		catch (e) {}	
    </script>

    <%if gf_pageName = "cart" then%>
    	<script type="text/javascript">
			try {
				Granify.trackCart({
					items: [
					<%=granifyItems%>
					]
				});
			}
			catch (e) {}
		</script>
    <%end if%>

    <%
	if gf_pageName = "receipt" then
		gf_discount = (cdbl(emailSubTotal)+cdbl(emailShipFee)+cdbl(emailOrderTax)) - cdbl(emailOrderGrandTotal)
		gf_orderDateTime = year(date) & "-" & right("00" & month(date), 2) & "-" & right("00" & day(date), 2) & " " & right("00" & hour(time), 2) & ":" & right("00" & minute(time), 2) & ":" & right("00" & second(time), 2)
	%>
		<%' data pulled from inc_receipt_new.asp%>
        <script type="text/javascript">
            try {
                Granify.trackOrder({
                    "created_at": "<%=gf_orderDateTime%>",
                    "currency": "USD",
                    "total_line_items_price": <%=formatnumber(emailSubTotal,2)%>,
                    "total_discounts": <%=formatnumber(gf_discount,2)%>,
                    "subtotal_price": <%=formatnumber(emailSubTotal-gf_discount,2)%>,
                    "total_tax": <%=formatnumber(emailOrderTax,2)%>,
                    "total_price": <%=formatnumber(emailOrderGrandTotal,2)%>,
                    <%if emailOrderTax > 0 then%>
                    "taxes_included": true,
                    <%else%>
                    "taxes_included": false,
                    <%end if%>
                    "order_number": "<%=nOrderID%>",
                    "financial_status": "paid",
                    "discount_codes": [{ "code": "<%=sPromoCode%>", "amount": <%=formatnumber(gf_discount,2)%> }],
                    "customer": {
                      "id": <%=nAccountID%>, 
                      "created_at": "<%=gf_orderDateTime%>",
                      "email": "<%=customerEmail%>",
                      "first_name": "<%=sFname%>",
                      "last_name": "<%=sLname%>"
                    },
                    "line_items": [
                    <%=granifyItems%>
                    ]
                });
            }
            catch (e) {}
        </script>
    <%end if%>
