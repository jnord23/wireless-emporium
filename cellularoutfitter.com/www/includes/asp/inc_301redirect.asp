<%
sub redirectURL(byref pageType, byref id, curUrl, byref param)
	dim strMasterURL : strMasterURL = getMasterURL(pageType, id, param)
	dim querySTR : querySTR = ""
	dim url : url = curUrl
	
	if instr(url,"?") > 0 then 
		querySTR = mid(request.ServerVariables("HTTP_X_REWRITE_URL"),instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"?") + 1)
	end if
	
	if instr(url,"?") > 0 then 
		url = left(request.ServerVariables("HTTP_X_REWRITE_URL"), instr(request.ServerVariables("HTTP_X_REWRITE_URL"),"?") - 1)
	end if

	if "" <> strMasterURL then 
		if url <> strMasterURL then
			if "" <> querySTR then
				strMasterURL = strMasterURL & "?" & querySTR
			end if

			response.Status = "301 Moved Permanently"
			response.AddHeader "Location", strMasterURL
			response.end
		end if
	end if
end sub


function getMasterURL(byref pageType, byref id, byref param)
	dim strMasterURL : strMasterURL = ""
	dim tBrandID, tModelID, tCategoryID, tBrandName, tModelName, tCategoryName
	
	select case lcase(pageType)
		case "b"	'brand page
			select case id
				case 2 : strMasterURL = "/b-2-sony-ericsson-cell-phone-accessories.html"
				case 4 : strMasterURL = "/b-4-lg-cell-phone-accessories.html"
				case 5 : strMasterURL = "/b-5-motorola-cell-phone-accessories.html"
				case 6 : strMasterURL = "/b-6-nextel-cell-phone-accessories.html"
				case 7 : strMasterURL = "/b-7-nokia-cell-phone-accessories.html"
				case 9 : strMasterURL = "/b-9-samsung-cell-phone-accessories.html"
				case 10 : strMasterURL = "/b-10-sanyo-cell-phone-accessories.html"
				case 11 : strMasterURL = "/b-11-siemens-cell-phone-accessories.html"
				case 14 : strMasterURL = "/b-14-blackberry-cell-phone-accessories.html"
				case 15 : strMasterURL = "/b-15-other-cell-phone-accessories.html"
				case 16 : strMasterURL = "/b-16-palm-cell-phone-accessories.html"
				case 17 : strMasterURL = "/b-17-apple-cell-phone-accessories.html"
				case 18 : strMasterURL = "/b-18-pantech-cell-phone-accessories.html"
				case 19 : strMasterURL = "/b-19-t-mobile-sidekick-cell-phone-accessories.html"
				case 20 : strMasterURL = "/b-20-htc-cell-phone-accessories.html"																
			end select
		case "c"	'category page
			select case id
				case 1 : strMasterURL = "/c-1-cell-phone-batteries.html"
				case 2 : strMasterURL = "/c-2-cell-phone-chargers-and-data-cables.html"
				case 3 : strMasterURL = "/c-3-cell-phone-covers-and-gel-skins.html"
				case 5 : strMasterURL = "/c-5-cell-phone-bluetooth-and-hands-free.html"
				case 6 : strMasterURL = "/c-6-cell-phone-holsters-and-car-mounts.html"
				case 7 : strMasterURL = "/c-7-cell-phone-cases-and-pouches.html"
				case 8 : strMasterURL = "/sb-0-sm-0-sc-8-cell-phone-other-accessories.html"
				case 12 : strMasterURL = "/sb-0-sm-0-sc-8-cell-phone-other-accessories.html"
				case 13 : strMasterURL = "/sb-0-sm-0-sc-8-cell-phone-other-accessories.html"
				case 14 : strMasterURL = "/sb-0-sm-0-sc-8-cell-phone-other-accessories.html"
				case 16 : strMasterURL = "/wholesale-cell-phones.html"
				case 18 : strMasterURL = "/c-18-cell-phone-screen-protectors.html"
				case 19 : strMasterURL = "/c-19-cell-phone-vinyl-skins.html"
				case 20 : strMasterURL = "/c-19-cell-phone-vinyl-skins.html"				
				case 99 : strMasterURL = "/c-5-cell-phone-bluetooth-and-hands-free.html"
				case 100 : strMasterURL = "/wholesale-cell-phones.html"
			end select
		case "car"	'carrier page
			select case id
				case 1 : strMasterURL = "/car-1-alltel-cell-phone-accessories.html"
				case 2 : strMasterURL = "/car-2-att-cingular-cell-phone-accessories.html"
				case 3 : strMasterURL = "/car-3-metro-pcs-cell-phone-accessories.html"
				case 4 : strMasterURL = "/car-4-sprint-nextel-cell-phone-accessories.html"
				case 5 : strMasterURL = "/car-5-t-mobile-cell-phone-accessories.html"
				case 6 : strMasterURL = "/car-6-verizon-cell-phone-accessories.html"
				case 7 : strMasterURL = "/car-7-prepaid-cell-phone-accessories.html"
				case 8 : strMasterURL = "/car-8-boost-mobile-southern-linc-cell-phone-accessories.html"
				case 9 : strMasterURL = "/car-9-cricket-cell-phone-accessories.html"
				case 11 : strMasterURL = "/car-11-us-cellular-cell-phone-accessories.html"
			end select
		case "carp"	'carrier phone page
			select case id
				case 1 : strMasterURL = "/car-1-alltel-cell-phones.html"
				case 2 : strMasterURL = "/car-2-att-cingular-cell-phones.html"
				case 3 : strMasterURL = "/car-3-metro-pcs-cell-phones.html"
				case 4 : strMasterURL = "/car-4-sprint-nextel-cell-phones.html"
				case 5 : strMasterURL = "/car-5-t-mobile-cell-phones.html"
				case 6 : strMasterURL = "/car-6-verizon-cell-phones.html"
				case 7 : strMasterURL = "/car-7-prepaid-cell-phones.html"
				case 8 : strMasterURL = "/car-8-boost-mobile-southern-linc-cell-phones.html"
				case 9 : strMasterURL = "/car-9-cricket-cell-phones.html"
				case 11 : strMasterURL = "/car-11-us-cellular-cell-phones.html"
			end select			
		case "cb"	'category-brand page
			if isobject(param) then 
				tCategoryName	=	param.Item("x_categoryName")
				tBrandName		=	param.Item("x_brandName")
				tCategoryID		=	param.Item("x_categoryID")
				tBrandID		=	param.Item("x_brandID")
				if "" <> tCategoryName and "" <> tBrandName and isnumeric(tCategoryID) and isnumeric(tBrandID) then 
					tCategoryName = replace(replace(tCategoryName, "&amp;", "and"), "&", "and")
					strMasterURL = "/sc-" & tCategoryID & "-sb-" & tBrandID & "-cell-phone-" & formatSEO(tCategoryName) & "-for-" & formatSEO(tBrandName) & ".html"
				end if	
			end if					
		case "m"	'brand-model page
			if isobject(param) then 
				tBrandName	=	param.Item("x_brandName")
				tModelName	=	param.Item("x_modelName")
				if "" <> tBrandName and "" <> tModelName then 
					strMasterURL = "/m-" & id & "-" & formatSEO(tBrandName) & "-" & formatSEO(tModelName) & "-cell-phone-accessories.html"
				end if	
			end if			
'		case "p"	'product page
		case "bmc"	'brand-model-category page
			if isobject(param) then 
				tBrandID			=	param.Item("x_brandID")
				tModelID			=	param.Item("x_modelID")
				tCategoryID			=	param.Item("x_categoryID")
				tBrandName			=	param.Item("x_brandName")
				tModelName			=	param.Item("x_modelName")
				tCategoryName		=	replace(replace(param.Item("x_categoryName"), "&amp;", "and"), "&", "and")
				tMusicGenreName		=	param.Item("x_musicGenreName")
				tMusicArtistName	=	param.Item("x_musicArtistName")
				if isnumeric(tBrandID) and isnumeric(tModelID) and isnumeric(tCategoryID) and "" <> tBrandName and "" <> tModelName and "" <> tCategoryName then 
					tCategoryName = replace(replace(tCategoryName, "&amp;", "and"), "&", "and")
					if 8 = tCategoryID then
						strMasterURL = "/sb-0-sm-0-sc-8-cell-phone-other-accessories.html"
					elseif 14 = tCategoryID then
						strMasterURL = "/sb-0-sm-0-sc-8-cell-phone-other-accessories.html"
					elseif 16 = tCategoryID then
						strMasterURL = "/cp-sb-" & tBrandID & "-" & formatSEO(tBrandName) & "-wholesale-cell-phones.html"
					elseif 20 = tCategoryID or tCategoryID = 1270 then
						if len(tMusicArtistName) > 0 then
							strMasterURL = "/sb-" & tBrandID & "-sm-" & tModelID & "-sc-1270-" & formatSEO(tCategoryName) & "-" & formatSEO(tMusicGenreName) & "-" & formatSEO(tMusicArtistName) & ".html"
						else
							strMasterURL = "/sb-" & tBrandID & "-sm-" & tModelID & "-sc-1270-" & formatSEO(tCategoryName) & "-" & formatSEO(tMusicGenreName) & ".html"
						end if
					else
						strMasterURL = "/sb-" & tBrandID & "-sm-" & tModelID & "-sc-" & tCategoryID & "-" & formatSEO(tBrandName) & "-" & formatSEO(tModelName) & "-" & formatSEO(tCategoryName) & ".html"
					end if
				end if
			end if			
		case "cbm"	'category-brand-model page
			if isobject(param) then 
				tBrandID		=	param.Item("x_brandID")
				tModelID		=	param.Item("x_modelID")
				tCategoryID		=	param.Item("x_categoryID")
				tBrandName		=	param.Item("x_brandName")
				tModelName		=	param.Item("x_modelName")
				tCategoryName	=	param.Item("x_categoryName")
				if isnumeric(tBrandID) and isnumeric(tModelID) and isnumeric(tCategoryID) and "" <> tBrandName and "" <> tModelName and "" <> tCategoryName then 
					if 8 = tCategoryID then
						strMasterURL = "/sb-0-sm-0-sc-8-cell-phone-other-accessories.html"
					elseif 14 = tCategoryID then
						strMasterURL = "/sb-0-sm-0-sc-8-cell-phone-other-accessories.html"
					else
						strMasterURL = "/sb-" & tBrandID & "-sm-" & tModelID & "-sc-" & tCategoryID & "-" & formatSEO(tBrandName) & "-" & formatSEO(tModelName) & "-" & formatSEO(tCategoryName) & ".html"
					end if
				end if
			end if
		case "pb"	'phone-brand page
			if isobject(param) then 
				tBrandID		=	param.Item("x_brandID")
				tBrandName		=	param.Item("x_brandName")
				if isnumeric(tBrandID) and "" <> tBrandName then 
					strMasterURL = "/cp-sb-" & tBrandID & "-" & formatSEO(tBrandName) & "-wholesale-cell-phones.html"
				end if
			end if
		case "hf"	'hands-free page
			if isobject(param) then 
				tBrandID		=	param.Item("x_brandID")
				tModelID		=	param.Item("x_modelID")
				tCategoryID		=	5
				tBrandName		=	param.Item("x_brandName")
				tModelName		=	param.Item("x_modelName")
				tCategoryName	=	"Blutooth and Hands-Free"
				if isnumeric(tBrandID) and isnumeric(tModelID) and isnumeric(tCategoryID) and "" <> tBrandName and "" <> tModelName and "" <> tCategoryName then 
					strMasterURL = "/sb-" & tBrandID & "-sm-" & tModelID & "-sc-" & tCategoryID & "-" & formatSEO(tBrandName) & "-" & formatSEO(tModelName) & "-" & formatSEO(tCategoryName) & ".html"
				end if
			end if	
	end select

	getMasterURL = strMasterURL



end function
%>
