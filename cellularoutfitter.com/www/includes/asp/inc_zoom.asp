<%
	useItemPic_co = itempic_co
	sql = "select isnull((select top 1 itemPic_co from we_items where partnumber = a.partnumber and master = 1 ), a.itemPic_co) itempic_co from we_items a where a.itemid = '" & itemid & "'"
	set rs = oConn.execute(sql)
	if not rs.eof then useItemPic_co = rs("itempic_co")

	zoomPath = "/productpics/big/zoom/" & useItemPic_co
%>
	<div id="id_zoomimage" style="display:none;">
		<div style="display:inline-block; width:900px; padding:3px; border:3px solid #444; background-color:#FFF; margin-left:auto; margin-right:auto; margin-top:30px; border-radius:5px; -moz-box-shadow: 1px 5px 5px #333; -webkit-box-shadow: 1px 5px 5px #333; box-shadow: 5px 5px 5px #333;">
			<div style="float:left; width:100%; position:relative;">
				<div style="position:absolute; top:-15px; right:-15px;"><a href="javascript:closeZoom();"><img src="/images/product/close-button.png" border="0" /></a></div>    
			</div>
			<div style="float:left; text-align:center; width:100%;">
				<div style="float:left; width:800px; text-align:center;">
                <%
				if fso.FileExists(Server.MapPath(zoomPath)) then
				%>
					<img id="id_img_zoom" src="<%=zoomPath%>" border="0" />
                <%
				else
					zoomPath = replace(zoomPath, "/productpics/big/zoom", "/productpics/big")
				%>
					<img id="id_img_zoom" src="<%=zoomPath%>" border="0" />
                <%
				end if
				%>
                	
				</div>
				<div style="float:left; width:80px; text-align:center; padding:10px;">
	                <div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onmouseover="fnSwapZoomImage('<%=zoomPath%>');"><img id="id_altimg_zoom" src="<%=zoomPath%>" width="60" height="60" border="0" /></div>
					<%
                    for i = 0 to 7
                        path = "/altPics/zoom/" & replace(itempic_we,".jpg","-" & i & ".jpg")
                        if fso.FileExists(Server.MapPath(path)) then
                        %>
                        <div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onmouseover="fnSwapZoomImage('<%=path%>');"><img id="id_altimg_zoom<%=i%>" src="<%=path%>" width="60" height="60" border="0" /></div>
                        <%
						else
							path = replace(path, "/altPics/zoom", "/altPics")
							if fso.FileExists(Server.MapPath(path)) then
                        %>
                        <div style="float:left; margin:3px; border:1px solid #ccc; padding:5px; cursor:pointer;" onmouseover="fnSwapZoomImage('<%=path%>');"><img id="id_altimg_zoom<%=i%>" src="<%=path%>" width="60" height="60" border="0" /></div>
                        <%
							end if
                        end if
                    next
                    %>            
                </div>
			</div>
		</div>
	</div>
	
	<script>
		function closeZoom() {
			document.getElementById("popCover").style.display = "none";
			document.getElementById("popBox").style.display = "none";
			document.getElementById("popBox").innerHTML = "";
		}
		
		function showZoom() {
			document.getElementById("popBox").innerHTML = document.getElementById("id_zoomimage").innerHTML;		
			document.getElementById("popCover").style.display = "";
			document.getElementById("popBox").style.display = "";
		}
		
		function fnSwapZoomImage(imgSrc) {
			document.getElementById('id_img_zoom').src = imgSrc;
		}
		
	</script>