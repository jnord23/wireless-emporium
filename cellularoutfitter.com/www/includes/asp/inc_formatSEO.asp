<%
function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(replace(replace(replace(formatSEO, "?", "-"),chr(225),"a"),chr(252),"u"),chr(246),"o")
		formatSEO = replace(replace(formatSEO, "---", "-"), "--", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "covers-screen-guards"
		case "bling-kits---charms" : formatSEO = "bling-kits-charms"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "hands-free-kits-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO = "holsters-car-mounts"
		case "leather-cases" : formatSEO = "cases-pouches"
		case "cell-phones" : formatSEO = "wholesale-cell-phone"
		case "full-body-protectors" : formatSEO = "invisible-film-protectors"
	end select
end function

function nameSEO(val)
	select case val
		case "Antennas/Parts" : nameSEO = "Antennas"
		case "Faceplates" : nameSEO = "Covers &amp; Screen Guards"
		case "Data Cable & Memory" : nameSEO = "Data Cables &amp; Memory Cards"
		case "Hands-Free" : nameSEO = "Hands-Free Kits &amp; Bluetooth Headsets"
		case "Holsters/Belt Clips" : nameSEO = "Holsters / Car Mounts"
		case "Leather Cases" : nameSEO = "Cases &amp; Pouches"
		case "Cell Phones" : nameSEO = "Wholesale Unlocked Cell Phones"
		case "Full Body Protectors" : nameSEO = "Invisible Film Protectors"
		case else : nameSEO = val
	end select
end function

function singularSEO(val)
	select case val
		case "Batteries" : singularSEO = "Battery"
		case "Chargers" : singularSEO = "Charger"
		case "Faceplates" : singularSEO = "Cover &amp; Screen Guard"
		case "Hands-Free" : singularSEO = "Hands Free / Bluetooth Headset"
		case "Holsters/Belt Clips" : singularSEO = "Holster / Car Mount"
		case "Leather Cases" : singularSEO = "Case / Pouch"
		case "Antennas/Parts" : singularSEO = "Antenna"
		case "Data Cable & Memory" : singularSEO = "Data Cable / Memory Card"
		case "Bling Kits & Charms" : singularSEO = "Bling Kit / Charm"
		case "Cell Phones" : singularSEO = "Wholesale Unlocked Cell Phone"
		case "Full Body Protectors" : singularSEO = "Invisible Film Protector"
		case else : singularSEO = val
	end select
end function
%>
