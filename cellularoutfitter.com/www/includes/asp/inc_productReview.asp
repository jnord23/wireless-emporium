<!--#include virtual="/includes/asp/inc_googleRecaptcha.asp"-->
<%
	nRow = 0
	bMoreTable = false
	if not objRsReviews.eof then
	%>
	<div style="height:10px; font-size:1px;"></div>
	<table border="0" bordercolor="#CCCCCC" bgcolor="#ededed" width="100%" cellpadding="3" cellspacing="0" class="product-review" style="border:1px solid #999;">
		<tr>
			<td style="margin-top:10px;">
				<div style="float:left; width:250px; padding-top:3px; height:25px;" align="left">
					<img src="/images/review/productReviews.jpg" border="0" width="151" height="20" />
				</div>
				<div style="float:left; width:400px; padding-top:3px; height:25px;" align="right">
					<%=getRatingAvgStar(avgRating) & " &nbsp; Average Rating: " & formatnumber(avgRating,1) & " &nbsp;(" & formatnumber(objRsReviews.recordcount,0) & " Reviews)"%> &nbsp; &nbsp; &nbsp; &nbsp; 
					<b><a style="font-size:11px; color:#000000; cursor:pointer; text-decoration:underline;" onclick="showPopup('writeReview');" name="a_writeareview">Write a review</a></b>		
				</div>
				<table id="tblReview" width="100%" border="0" cellspacing="0" cellpadding="10" class="product-review">
		<%
		do until objRsReviews.eof
			nRow = nRow + 1
			if nRow > 3 and not bMoreTable then
				bMoreTable = true
		%>
				</table>
				<table id="tblMoreReview" width="100%" border="0" cellspacing="0" cellpadding="10" class="product-review" style="display:none;">
		<%
			end if
		%>
					<tr>
						<td align="left" style="text-align:justify; border-top:2px solid #ccc; border-right:2px solid #ccc;" width="440px">
							<span style="font-size:18px; font-weight:bold;"><%=objRsReviews("reviewTitle")%></span><br />
							<%=getRatingStar(objRsReviews("rating")) & " &nbsp; &nbsp; " & formatnumber(objRsReviews("rating"),1)%><br /><br />
							<%=objRsReviews("review")%>
						</td>
						<td align="left" width="*" style="border-top:2px solid #ccc;" valign="top">
							Posted by <b><%=objRsReviews("nickname")%></b><br />
							<%=objRsReviews("dateTimeEntd")%><br /><br />
							Share this review: &nbsp;
							<a href="http://www.facebook.com/cellularoutfitter" target="_blank"><img src="/images/review/facebookShare.jpg" border="0" width="6" height="11" /></a>&nbsp; &nbsp; 
							<a href="http://twitter.com/celloutfitter" target="_blank"><img src="/images/review/twitterShare.jpg" border="0" width="6" height="11" /></a>
						</td>
					</tr>
		<%
			objRsReviews.movenext
		loop		
		%>
				</table>
		<%
		if objRsReviews.recordcount > 3 then
		%>
				<table id="tblReview" width="100%" border="0" cellspacing="0" cellpadding="0" class="product-review">		
					<tr>
						<td align="left" width="460px">&nbsp;</td>
						<td align="right" width="*" style="border-left:2px solid #ccc;">
							<a id="a_showmore" style="cursor:pointer; font-size:10px;" onclick="showReviewAll();">show more..</a>
						</td>
					</tr>
				</table>	
		<%
		end if
		%>				
			</td>
		</tr>
	</table>
<%	
	end if
	objRsReviews.close
	set objRsReviews = nothing
%>

<div id="writeReview" style="display:none; position:absolute; width:600px; height:500px; border:3px groove #5F99CB; z-index:99;">
<table bgcolor="#FFFFFF" border="0" cellpadding="10" cellspacing="0" width="100%" class="product-review" height="500px">
	<tr>
		<td style="background: url(/images/top/menuBg.gif) repeat-x top left; height:40px;" valign="top">
			<div style="float:left; font-weight:bold; font-size:15px; width:540px; color:#ffffff;" align="left">
				<img src="/images/review/checkmark.jpg" border="0" width="19" height="15" />&nbsp;
				CellularOutfitter.com would like for you to write a review on this product
			</div>
			<div style="float:left; width:40px;" align="right">
				<a onclick="closePopup();" style="cursor:pointer; color:#FFF; font-size:14px">
					<img src="/images/review/x_close.jpg" border="0" width="23" height="23" />
				</a>			
			</div>
		</td>
    </tr>
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="product-review">
				<tr>
					<td align="left" valign="top">
						<div id="responseBack" style="width:100%; vertical-align:top;">
							<form name="frmReview">						
							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="product-review">
								<tr>
									<td colspan="2">
										Review Title: <font color="red">*</font><br />
										<input type="text" size="50" name="txtReviewTitle" />
										<br /><br />
										Describe your experience using this product. <font color="red">*</font><br />
										<textarea name="txtDescribe" cols="67" rows="6"></textarea>
										<br /><br />
									</td>
								</tr>
								<tr>
									<td align="left" style="padding-bottom:20px;">
										Your email: <input type="text" size="25" name="txtEmail" /> <font color="red">*</font>
									</td>
									<td align="right" style="padding-bottom:20px;">
										Your nickname: <input type="text" size="25" name="txtNickName" /> <font color="red">*</font>
									</td>
								</tr>
								<tr height="150px">
									<td valign="top">
										<b>Rate the product overall:</b> <font color="red">*</font><br />
										<input type="radio" name="rdoRating" value="1">
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										<img src="/images/review/grayStar.gif" border="0" width="14" height="13" />
										<img src="/images/review/grayStar.gif" border="0" width="14" height="13" />
										<img src="/images/review/grayStar.gif" border="0" width="14" height="13" />
										<img src="/images/review/grayStar.gif" border="0" width="14" height="13" />
										&nbsp;<b>Poor</b>&nbsp;&nbsp;&nbsp;<br />
										
										<input type="radio" name="rdoRating" value="2">
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										<img src="/images/review/grayStar.gif" border="0" width="14" height="13" />
										<img src="/images/review/grayStar.gif" border="0" width="14" height="13" />
										<img src="/images/review/grayStar.gif" border="0" width="14" height="13" />										
										&nbsp;<b>Fair</b>&nbsp;&nbsp;&nbsp;<br />
										
										<input type="radio" name="rdoRating" value="3">
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										<img src="/images/review/grayStar.gif" border="0" width="14" height="13" />
										<img src="/images/review/grayStar.gif" border="0" width="14" height="13" />
										&nbsp;<b>Good</b>&nbsp;&nbsp;&nbsp;<br />
																				
										<input type="radio" name="rdoRating" value="4">
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										<img src="/images/review/grayStar.gif" border="0" width="14" height="13" />
										&nbsp;<b>Very&nbsp;Good</b>&nbsp;&nbsp;&nbsp;<br />
										
										<input type="radio" name="rdoRating" value="5">
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										<img src="/images/review/greenStarFull.gif" border="0" width="14" height="13" />
										&nbsp;<b>Excellent</b>										
									</td>
									<td valign="top">
										<div id="recaptcha_div" style="height:130px;"></div>
										<div id="recaptcha_msg"></div>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<input type="image" src="/images/review/coSubmit.jpg" border="0" onClick="return validate();" />									
									</td>
								</tr>
							</table>
							<input type="hidden" name="hidPartNumber" value="<%=myPartNumber%>" />
							<input type="hidden" name="hidItemID" value="<%=itemid%>" />							
							</form>
						</div>
					</td>
				</tr>				
			</table>		
		</td>
	</tr>
</table>
</div>




<script>
	var	correctAnswer = false;
	function showReviewAll()
	{
		var objMoreReview = document.getElementById('tblMoreReview');
		var	objArrow = document.getElementById('id_img_reviewArrow');
		
		if ("none" == tblMoreReview.style.display)
		{
			objMoreReview.style.display = '';
			document.getElementById('a_showmore').innerHTML = "hide more..";
		}
		else 
		{
			objMoreReview.style.display = 'none';
			document.getElementById('a_showmore').innerHTML = "show more..";			
		}	
	}
	
	function closePopup()
	{
		Recaptcha.destroy();
		document.getElementById('writeReview').style.display='none';
	}
	
	function showPopup(id)
	{
		var objPopup = document.getElementById(id);
		var top = document.body.scrollTop
			? document.body.scrollTop
			: (window.pageYOffset
				? window.pageYOffset
				: (document.body.parentElement
					? document.body.parentElement.scrollTop
					: 0
				)
			);
			
		var	left = Math.round((parseInt(document.documentElement.clientWidth)/2)-(parseInt(objPopup.style.width)/2));
		if (left <= 0) left = 400;
		
		objPopup.style.top = (top+100) + "px";
		objPopup.style.left = left + "px";
		objPopup.style.display = 'block';
				
		if (!correctAnswer) 
		{
			showRecaptcha('recaptcha_div');
			document.frmReview.txtReviewTitle.focus();
		}
	}
	
	<% if showReviewPopup then %>
	setTimeout("showPopup('writeReview');",2000)
	<% end if %>
	
	function validateEmail(email) 
	{
		var splitted = email.match("^(.+)@(.+)$");
		if (splitted == null) return false;
		if (splitted[1] != null) {
			var regexp_user=/^\"?[\w-_\.]*\"?$/;
			if (splitted[1].match(regexp_user) == null) return false;
		}
		if (splitted[2] != null) {
			var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
			if (splitted[2].match(regexp_domain) == null) {
				var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
				if (splitted[2].match(regexp_ip) == null) return false;
			}
			return true;
		}
		return false;
	}
	
	function validate() 
	{
		var title = document.frmReview.txtReviewTitle.value;
		var nickname = document.frmReview.txtNickName.value;
		var desc = document.frmReview.txtDescribe.value;
		var email = document.frmReview.txtEmail.value;
		var arrRating = document.frmReview.rdoRating;
		var	itemid = document.frmReview.hidItemID.value;
		var	partnumber = document.frmReview.hidPartNumber.value;
		var rating = -1;
		
		for (var i=0; i<arrRating.length; i++) if (arrRating[i].checked) rating = arrRating[i].value;
		
		if (rating == -1) {
			alert("Please rate the product overall");
			return false;
		}
		if (title.length==0) {
			alert("Please enter a title for your review");
			return false;
		}
		if (nickname.length==0) {
			alert("Please enter a nickname which will appear on your review");
			return false;
		}
		if (desc.length==0) {
			alert("Please describe your experience using this product");
			return false;
		}
		if (email.length==0) {
			alert("Please enter the email address associated with your order");
			return false;
		}
		if (!validateEmail(email)) { 
			alert("Please enter a valid email address");
			return false;
		}
		
		var	updateURL;
		updateURL	=				"email=" + email + "&title=" + escape(title) + "&desc=" + escape(desc) + "&nickname=" + escape(nickname) + "&rating=" + rating;
		updateURL	=	updateURL + "&itemid=" + itemid + "&partnumber=" + escape(partnumber);
		updateURL	=	updateURL + "&re_challenge=" + Recaptcha.get_challenge() + "&re_response=" + Recaptcha.get_response() + "&re_privatekey=<%=recapPrivateKey%>";		
		updateURL	=	"/ajax/updateProductReview.asp?" + updateURL;
		ajax(updateURL,'responseBack');
		
		return false;
	}

	/*
	function ajax2(newURL,rLoc,parameters) {
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState==4){
				if (httpRequest.status==200 || window.location.href.indexOf("http")==-1){
					document.getElementById(rLoc).innerHTML=httpRequest.responseText
				}
				else {
					alert("An error has occured making the request");
				}
			}
		};
		
		httpRequest.open('POST', newURL, true);
		httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		httpRequest.send(parameters);
	}
	*/	
		
	function ajax(newURL,rLoc) {
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						alert("No data available")					
					}
					else if (rVal == "refresh") {
						curLoc = window.location
						window.location = curLoc
					}
					else if (rVal == "wrong recaptcha") {
						Recaptcha.reload();
						document.getElementById('recaptcha_msg').innerHTML = '<span style="color:red;"><b>You have entered the wrong secret words.</b></span>';
					}
					else {
						correctAnswer = true;
						if(document.getElementById(rLoc)) {
							document.getElementById(rLoc).innerHTML = rVal;
						}
					}
					rVal = null;
				}
				else {
					//document.getElementById("testZone").innerHTML = newURL
				}
			}
		};
		httpRequest.send(null);
	}	
</script>
<%
function getRatingStar(rating)
	dim nRating 
	dim strRatingImg
	nRating = cint(rating)
	strRatingImg = 	"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />"
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""
		for i=1 to 5
			if i <= nRating then 
				strRatingImg = strRatingImg & "<img src=""/images/review/greenStarFull.gif"" border=""0"" width=""14"" height=""13"" /> "
			else
				strRatingImg = strRatingImg & "<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" /> "
			end if	
		next	
	end if
	
	getRatingStar = strRatingImg
end function

function getRatingAvgStar(rating)
	dim nRating 
	dim strRatingImg
	nRating = cdbl(rating)
	strRatingImg = 	"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />"
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""	
		for i=1 to 5
			if nRating => i then
				strRatingImg = strRatingImg & "<img src=""/images/review/greenStarFull.gif"" border=""0"" width=""14"" height=""13"" /> "
			elseif nRating => ((i - 1) + .1) then
				strRatingImg = strRatingImg & "<img src=""/images/review/greenStarHalf.gif"" border=""0"" width=""8"" height=""13"" /> "				
			else
				strRatingImg = strRatingImg & "<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" /> "
			end if
		next		
	end if
	
	getRatingAvgStar = strRatingImg
end function
%>