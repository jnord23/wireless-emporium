<%
dim ReceiptText
ReceiptText = ""

dim sPromoCode
SQL = "SELECT B.PromoCode FROM we_orders A INNER JOIN CO_coupons AS B ON A.couponid=B.couponid WHERE A.orderid = '" & nOrderID & "'"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

if RS.EOF then
	SQL = replace(lcase(SQL),"we_orders","we_orders_historical")
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
end if

if not RS.eof then sPromoCode = RS("PromoCode")

dim sFname, sLname, sEmail, sPhone
dim saddress1, saddress2, sCity, sstate, szip, sCountry
dim sShipAddress, sBillAddress
dim oRsCust, oRsOrd

SQL = "SELECT * FROM CO_accounts WHERE accountid = '" & nAccountId & "'"
set oRsCust = Server.CreateObject("ADODB.Recordset")
oRsCust.open SQL, oConn, 3, 3
if oRsCust.eof then
	ReceiptText = "<h3>Customer account" & nAccountId & " not found!</h3>"
else
	sFname = oRsCust("fname")
	sLname = oRsCust("lname")
	sEmail = oRsCust("email")
	sPhone = oRsCust("phone")
end if

SQL = "SELECT * FROM we_orders WHERE orderid = '" & nOrderId & "'"
set oRsOrd = Server.CreateObject("ADODB.Recordset")
oRsOrd.open SQL, oConn, 0, 1

if oRsOrd.EOF then
	SQL = replace(lcase(SQL),"we_orders","we_orders_historical")
	set oRsOrd = Server.CreateObject("ADODB.Recordset")
	oRsOrd.open SQL, oConn, 0, 1
end if

if oRsOrd.eof then ReceiptText = "<h3>Order" & nOrderId & " not found!</h3>"

'format the shipping and billing addresses
dim nShippingid
nShippingid = oRsOrd("shippingid")
if nShippingid > 0 then
	strSql = "SELECT * FROM we_addl_shipping_addr WHERE id='" & nShippingid & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open strSql, oConn, 0, 1
	if not RS.eof then
		saddress1 = RS("saddress1")
		saddress2 = RS("saddress2")
		sCity = RS("sCity")
		sstate = RS("sstate")
		szip = RS("szip")
		sCountry = RS("sCountry")
	end if
else
	saddress1 = oRsCust("saddress1")
	saddress2 = oRsCust("saddress2")
	sCity = oRsCust("sCity")
	sstate = oRsCust("sstate")
	szip = oRsCust("szip")
	sCountry = oRsCust("sCountry")
end if

testMsg = ""
if saddress1 = "4040 N. Palm St." then
	SQL = "select cancelled from we_Orders WHERE orderID = '" & nOrderId & "'"
	set checkOrderRS = oConn.execute(SQL)
	
	if not checkOrderRS.EOF then
		if isnull(checkOrderRS("cancelled")) then
			call cancelTestOrder(nOrderId)
		end if
		testMsg = "This is a test order and has been auto canceled"
	else
		testMsg = "Cancel Bypass:" & SQL
	end if
end if

sShipAddress = sAddress1
if sAddress2 <> "" then sShipAddress = sShipAddress & "<br>" & saddress2
sShipAddress = sShipAddress & "<br>" & sCity & ", " & sState & "&nbsp;" & sZip & "<br>" & sCountry

sBillAddress = oRsCust("bAddress1")
if oRsCust("bAddress2") <> "" then sBillAddress = sBillAddress & "<br>" & oRsCust("bAddress2")
sBillAddress = sBillAddress & "<br>" & oRsCust("bCity") & ", " & oRsCust("bState") & "&nbsp;" & oRsCust("bZip") & "<br>" & oRsCust("bCountry")

oRsCust.close
set oRsCust = nothing

if ReceiptText = "" then
	'format order info
	'dim nOrderSubTotal
'	nOrderSubTotal = oRsOrd("ordersubtotal")
	emailSubTotal = oRsOrd("ordersubtotal")
	sShipType = oRsOrd("shiptype")
'	nShipFee = oRsOrd("ordershippingfee")
	emailShipFee = oRsOrd("ordershippingfee")
	if prepInt(nShipFee) = 0 then nShipFee = emailShipFee
'	nOrderTax = oRsOrd("orderTax")
	emailOrderTax = oRsOrd("orderTax")
	if prepInt(nOrderTax) = 0 then nOrderTax = emailOrderTax
	nBuysafeamount = oRsOrd("BuySafeAmount")
'	nOrderGrandTotal = oRsOrd("ordergrandtotal")
	emailOrderGrandTotal = oRsOrd("ordergrandtotal")
	extOrderType = oRsOrd("extOrderType")
	extOrderNumber = oRsOrd("extOrderNumber")
	nOrderDateTime = oRsOrd("orderdatetime")
	
	dim strOrderType
	select case extOrderType
		case 1 : strOrderType = "Paypal<br>" & extOrderNumber
		case 2 : strOrderType = "Google Checkout<br>" & extOrderNumber
		case 3
			strOrderType = "eBillme"
			eBillme = "eBillme"
			eBillmeAccount = extOrderNumber
		case else : strOrderType = "Credit Card"
	end select
	
	oRsOrd.close
	set oRsOrd = nothing
	
	'===================
	'NEW WEBLOYALTY CODE
	dim RSwl, cctype, country, pcode, strToEncrypt, strEncryptedValue
	SQL = "SELECT A.CCtype, B.bState, B.bZip FROM we_ccinfo A INNER JOIN CO_Accounts B ON A.accountid = B.accountid WHERE A.orderid='" & nOrderID & "'"
	set RSwl = Server.CreateObject("ADODB.Recordset")
	RSwl.open SQL, oConn, 0, 1
	if not RSwl.eof then
		cctype = RSwl("CCtype")
		if len(RSwl("bState")) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",RSwl("bState")) > 0 then
			country = "CANADA"
		else
			country = "US"
		end if
		pcode = RSwl("bZip")
	end if
	RSwl.close
	set RSwl = nothing
	
	' Put together string ?concatenate all required values
	if incEmail = true then
		dim yesexiturl, noexiturl
		yesexiturl = "https://www.cellularoutfitter.com/confirm.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & emailOrderGrandTotal & "&c=" & emailSubTotal
		noexiturl = yesexiturl
		strToEncrypt = "cref" & chr(255) & nOrderID & chr(255) & "cctype" & chr(255) & cctype & chr(255) & "country" & chr(255) & country & chr(255) & "pcode" & chr(255) & pcode & chr(255) & "yesexiturl" & chr(255) & yesexiturl & chr(255) & "noexiturl" & chr(255) & noexiturl
	else
		strToEncrypt = "cref" & chr(255) & nOrderID & chr(255) & "cctype" & chr(255) & cctype & chr(255) & "country" & chr(255) & country & chr(255) & "pcode" & chr(255) & pcode
	end if
	' Encrypt it
	strEncryptedValue = SimpleEncrypt(strToEncrypt)
	'===================
    ReceiptText = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN""	""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
	ReceiptText = ReceiptText & "<head>" & vbcrlf
	ReceiptText = ReceiptText & "<title>Thank you for your purchase on CellularOutfitter.com</title>" & vbcrlf

	ReceiptText = ReceiptText & "<style type=""text/css"">" & vbcrlf
	ReceiptText = ReceiptText & "<!--" & vbcrlf
	ReceiptText = ReceiptText & ".regText {font-family: Arial; font-size: 9pt; font-weight: normal; color: #000000;}" & vbcrlf
	ReceiptText = ReceiptText & ".totalsText {font-family: Arial, Helvetica, sans-serif; font-size: 10pt; font-weight: bold; color: #000000;}" & vbcrlf
	ReceiptText = ReceiptText & ".blue-text {font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #3399CC;}" & vbcrlf
	ReceiptText = ReceiptText & ".black-text {font-family: Arial, Helvetica, sans-serif; font-size: 16pt; font-weight: normal; color: #000000;}" & vbcrlf
	ReceiptText = ReceiptText & ".gray-text {font-family: Arial, Helvetica, sans-serif; font-size: 16pt; font-weight: normal; color: #999999;}" & vbcrlf
	ReceiptText = ReceiptText & ".borderBox {font-family: Arial; font-size: 9pt; font-weight: normal; color: #000000; border-style: solid; border-width: 1px; border-color: #999999; background-color: #EEEEEE;}" & vbcrlf
	ReceiptText = ReceiptText & "-->" & vbcrlf
	ReceiptText = ReceiptText & "</style>" & vbcrlf
    ReceiptText = ReceiptText & "</head>" & vbcrlf
	ReceiptText = ReceiptText & "<body>" & vbcrlf
	
	ReceiptText = ReceiptText & "<table width=""800"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"">" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center""><a href=""http://www.CellularOutfitter.com/""><img src=""https://www.cellularoutfitter.com/images/coLogo.gif"" width=""320"" height=""90"" border=""0"" alt=""CellularOutfitter.com - Wholesale Prices to the Public""></a></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""100%"" height=""75"" align=""center"" bgcolor=""#EEEEEE""><p class=""blue-text""><span style=""font-size: 16pt;"">Thank you for your purchase on CellularOutfitter.com.</span><br><span style=""font-size: 12pt;"">Please print and keep a copy of this receipt for your records.</span></td></tr>" & vbcrlf
	
	if testMsg <> "" then
		ReceiptText = ReceiptText & "<tr>" & vbcrlf
		ReceiptText = ReceiptText & "<td align=""center"">" & testMsg & "</td>" & vbcrlf
		ReceiptText = ReceiptText & "</tr>" & vbcrlf
	end if
	
	ReceiptText = ReceiptText & "<tr><td width=""100%"" height=""30"" align=""center""><img src=""https://www.cellularoutfitter.com/images/hori_line.jpg"" width=""800"" height=""7"" border=""0""></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""100%"" height=""50"" align=""right""><p class=""black-text""><span style=""font-weight: normal;"">Order&nbsp;Date:&nbsp;&nbsp;" & dateValue(nOrderDateTime) & "</span><br><span style=""font-weight: bold;"">Order&nbsp;ID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & nOrderId & "</span></td></tr>" & vbcrlf
	
	'START eBillme section
	if eBillme = "eBillme" then
		eBillmeAccountArray = split(eBillmeAccount," | ")
		if Ubound(eBillmeAccountArray) > 0 then
			eBillmeAccount = eBillmeAccountArray(1)
			ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center"" style=""padding:20px 0px 20px 0px;"">" & vbcrlf & _
			"<table cellspacing=""0"" border=""0"" cellpadding=""0"" align=""center"" width=""720"" style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"">" & vbcrlf & _
			"  <tr>" & vbcrlf & _
			"	<td background=""https://content.wupaygateway.com/wupay/confirm/images/header.png"" height=""37"" valign=""middle"" style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"" colspan=""2""><table cellspacing=""0"" border=""0"" cellpadding=""0"" width=""720"">" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td class=""header1"" style=""font-size: 12px;  font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;"" width=""24""> </td>" & vbcrlf & _
			"		<td class=""header1"" style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;"" width=""221""><strong>WU&reg; Pay&trade; - Your Bill </strong></td>" & vbcrlf & _
			"		<td class=""header1"" style=""font-size: 12px;  font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;"" width=""454""><div align=""right""><a target=""_blank"" href=""http://livechat.boldchat.com/aid/8900659474086935388/bc.chat?cwdid=8262818297912230602"" style=""text-decoration: underline; color: #000000;"">Live Chat</a> | <a href=""http://www.westernunion.com/wupay/learn/faq"" style=""text-decoration: underline; color: #000000;"" target=""_blank"">FAQ</a></div></td>" & vbcrlf & _
			"		<td class=""header1"" style=""font-size: 12px;  font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;"" width=""21""> </td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	</table>" & vbcrlf & _
			"   </td>" & vbcrlf & _
			"  </tr>" & vbcrlf & _
			"  <tr>" & vbcrlf & _
			"	<td class=""cells"" valign=""top"" style=""font-size: 12px; padding: 3px 9px 8px 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"" width=""500""><table cellspacing=""0"" border=""0"" cellpadding=""2"" align=""center"" width=""90%"">" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;""><span class=""title"" style=""font-size: 23px; padding-top: 8px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"">This is your Bill</span>" & vbcrlf & _
			"		  <p class=""blueheader"" style=""font-weight: bold; font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #00000;"">" & sFname & " " & sLname & ",</p>" & vbcrlf & _
			"		  <p class=""para"" style=""font-size: 12px; line-height: 14px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;""> Thank you for using WU&reg; Pay as your payment method. Please note:<br> " & vbcrlf & _
			"		  A copy of this bill will be emailed to <strong>" & sEmail & "</strong></p></td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	</table>" & vbcrlf & _
			"	</td>" & vbcrlf & _
			"	<td class=""logos"" valign=""bottom"" style=""font-size: 12px; padding: 8px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; text-align: center;"" width=""220""><div align=""center""><img src=""https://content.wupaygateway.com/wupay/confirm/images/wupay-logo.png"" alt=""WU&lt;sup&gt;��&lt;/sup&gt; Pay logo"" width=""149""><br>" & vbcrlf & _
			"	  <br>" & vbcrlf & _
			"	  </div>      " & vbcrlf & _
			"	  <div align=""left""><span class=""style31"" style=""font-size: 13px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #00000;""><span class=""blueheader"" style=""font-weight: bold; font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #00000;"">When will my order ship?</span></span><span class=""blueheader"" style=""font-weight: bold; font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #00000;""><br>" & vbcrlf & _
			"	</span><span class=""sidebarsmall"" style=""font-size: 11px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000;"">Once payment is received,  WU&reg; Pay will authorize your  to ship your order.</span></div></td>" & vbcrlf & _
			"  </tr>" & vbcrlf & _
			"  <tr>" & vbcrlf & _
			"	<td style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"" colspan=""2""><img src=""https://content.wupaygateway.com/wupay/confirm/images/dividers1top.png"" height=""3"" alt=""divider top"" width=""720""></td>" & vbcrlf & _
			"  </tr>" & vbcrlf & _
			"  <tr>" & vbcrlf & _
			"	<td class=""divider"" style=""padding-bottom: 5px; font-size: 15px; background: #ededed; padding-top: 5px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000; padding-left: 24px;"" colspan=""2""><div align=""left"">To " & vbcrlf & _
			"		complete your order please pay WU&reg; Pay" & vbcrlf & _
			"		" & formatCurrency(emailOrderGrandTotal,2) & "</div></td>" & vbcrlf & _
			"  </tr>" & vbcrlf & _
			"  <tr>" & vbcrlf & _
			"	<td style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"" colspan=""2""><img src=""https://content.wupaygateway.com/wupay/confirm/images/dividers1bottom.png"" height=""3"" alt=""divider bottom"" width=""720""></td>" & vbcrlf & _
			"  </tr>" & vbcrlf & _
			"  <tr>" & vbcrlf & _
			"	<td class=""cells"" style=""font-size: 12px; padding: 3px 9px 8px 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"" width=""500""><table cellspacing=""0"" border=""0"" cellpadding=""0"" align=""center"" width=""90%"">" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td valign=""bottom"" style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"" width=""85%""><strong>Go to your bank's website, and pay WU&reg; Pay.</strong> <br>" & vbcrlf & _
			"			<br>" & vbcrlf & _
			"			<strong>First time users:</strong> You will require the following information to <br>" & vbcrlf & _
			"			setup WU&reg; Pay as a payee:</td>" & vbcrlf & _
			"		<td valign=""bottom"" style=""font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"" width=""15%""><div align=""right""><a href=""JavaScript:window.print();"" class=""style26"" style=""font-size: 10px; color: #0099FF;""><img src=""https://content.wupaygateway.com/wupay/confirm/images/print.png"" border=""0"" height=""16"" alt=""print"" align=""absmiddle"" width=""16"">print bill</a></div></td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	</table>" & vbcrlf & _
			"	  <br>" & vbcrlf & _
			"	  <div align=""right""><a href=""JavaScript:window.print();"" class=""style26"" style=""font-size: 10px; color: #0099FF;"">" & vbcrlf & _
			"	</a></div>    " & vbcrlf & _
			"	<table cellspacing=""0"" border=""0"" cellpadding=""7"" align=""center"" width=""90%"">" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><div class=""style30"" align=""right"" style=""color: #000000;"">" & vbcrlf & _
			"			<div align=""right""><span class=""bank"" style=""font-size: 13px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"">Payee Name:</span><br>" & vbcrlf & _
			"				<span class=""weeno"" style=""font-size: 10px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;"">Do not include merchant name</span></div>" & vbcrlf & _
			"		</div></td>" & vbcrlf & _
			"		<td bgcolor=""#f1f9fe"" class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><strong>WU Pay</strong></td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;"" width=""50%""><div class=""style30"" align=""right"" style=""color: #000000;"">" & vbcrlf & _
			"			<div align=""right""><span class=""bank"" style=""font-size: 13px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"">Your Account Number:</span><br>" & vbcrlf & _
			"			  <span class=""weeno"" style=""font-size: 10px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;"">Please enter as shown</span></div>" & vbcrlf & _
			"		</div></td>" & vbcrlf & _
			"		<td bgcolor=""#f1f9fe"" class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;"" width=""50%""><strong>" & eBillmeAccount & "</strong></td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><div class=""style30"" align=""right"" style=""color: #000000;"">" & vbcrlf & _
			"			<div class=""bank"" align=""right"" style=""font-size: 13px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"">Payee Address:</div>" & vbcrlf & _
			"		</div></td>" & vbcrlf & _
			"		<td bgcolor=""#f1f9fe"" class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><strong>P.O. Box 635808<br>" & vbcrlf & _
			"		  Cincinnati, OH</strong></td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><div class=""style30"" align=""right"" style=""color: #000000;"">" & vbcrlf & _
			"			<div align=""right""><span class=""bank"" style=""font-size: 13px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"">Payee ZIP:</span><br>" & vbcrlf & _
			"				<span class=""weeno"" style=""font-size: 10px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000000;"">Please enter all 9 digits</span></div>" & vbcrlf & _
			"		</div></td>" & vbcrlf & _
			"		<td bgcolor=""#f1f9fe"" class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><strong>45263-5808</strong></td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	  <tr>" & vbcrlf & _
			"		<td class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><div class=""style30"" align=""right"" style=""color: #000000;"">" & vbcrlf & _
			"			<div class=""bank"" align=""right"" style=""font-size: 13px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;"">Payee Phone Number:</div>" & vbcrlf & _
			"		</div></td>" & vbcrlf & _
			"		<td bgcolor=""#f1f9fe"" class=""payeecells"" valign=""top"" style=""font-size: 12px; border-bottom-width: thin; border-bottom-style: dotted; padding: 3px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; border-bottom-color: #CCCCCC;""><strong>1.888.899.6633</strong></td>" & vbcrlf & _
			"	  </tr>" & vbcrlf & _
			"	</table>" & vbcrlf & _
			"	<br>" & vbcrlf & _
			"	</td>" & vbcrlf & _
			"	<td class=""logos"" style=""font-size: 12px; padding: 8px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; text-align: center;""><p class=""blueheader"" align=""left"" style=""font-weight: bold; font-size: 12px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #00000;"">How to ensure the fastest <br>" & vbcrlf & _
			"  payment processing:</p>" & vbcrlf & _
			"	  <p class=""sidebarsmall"" align=""left"" style=""font-size: 11px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000;""><strong>Enter all information accurately:</strong><br>" & vbcrlf & _
			"		 Enter WU&reg; Pay as the Payee Name<br>" & vbcrlf & _
			"		 Enter your WU&reg; Pay account number<br>" & vbcrlf & _
			"		 Enter WU&reg; Pay's complete address<br>" & vbcrlf & _
			"		 Enter WU&reg; Pay's 9 digit ZIP</p>" & vbcrlf & _
			"	  <p class=""sidebarsmall"" align=""left"" style=""font-size: 11px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000;""><strong class=""sidebarsmall"" style=""font-size: 11px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000;"">Useful tip:</strong><br>" & vbcrlf & _
			"		Use 'cut and paste' to enter payee information correctly.</p>" & vbcrlf & _
			"	  <p class=""sidebarsmall"" align=""left"" style=""font-size: 11px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000;""><strong><a href=""http://www.westernunion.com/wupay/learn/bill-pay"" style=""color: #0099FF;"" target=""_blank"">Read step by step instructions</a></strong> <br>" & vbcrlf & _
			"	  on how to set up WU&reg; Pay as a payee</p>" & vbcrlf & _
			"	  <p class=""sidebarsmall"" align=""left"" style=""font-size: 11px; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #000;""><strong><a href=""http://www.westernunion.com/wupay/redirect.php?http%3A%2F%2Flivechat.boldchat.com%2Faid%2F8900659474086935388%2Fbc.chat%3Fcwdid%3D8262818297912230602"" style=""color: #0099FF;"" target=""_blank"">Still need help?</a></strong><br>" & vbcrlf & _
			"	  </p>" & vbcrlf & _
			"  </td></tr>" & vbcrlf & _
			"  <tr>" & vbcrlf & _
			"	<td bgcolor=""#007ec7"" class=""footer"" valign=""top"" style=""font-size: 12px; background: no-repeat; font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #FFFFFF; vertical-align: top;"" colspan=""2""><strong><img src=""https://content.wupaygateway.com/wupay/confirm/images/footer.png"" height=""11"" alt=""footer1"" width=""720""><br>" & vbcrlf & _
			"	</strong>" & vbcrlf & _
			"  </td>" & vbcrlf & _
			"  </tr>" & vbcrlf & _
			"</table>" & vbcrlf & _
			"</td></tr>" & vbcrlf			
'			ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center""><p>&nbsp;</p>" & vbcrlf
'			ReceiptText = ReceiptText & "<table width=""600"" border=""0"" cellpadding=""5"" cellspacing=""0"" align=""center"" style=""border: 1px solid black;"">" & vbcrlf
'			ReceiptText = ReceiptText & "<tr><td colspan=""3"" bgcolor=""#006699"" height=""25""><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #FFFFFF; font-weight: bold"">To receive your order:</p></td></tr>" & vbcrlf
'			ReceiptText = ReceiptText & "<tr><td colspan=""2""><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">Go to your bank's website, setup and pay eBillme " & formatCurrency(emailOrderGrandTotal) & "</p>" & vbcrlf
'			ReceiptText = ReceiptText & "<p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal""><em>First time users may require the following information:</em></p></td>" & vbcrlf
'			ReceiptText = ReceiptText & "<td rowspan=""5"" align=""center"" width=""150"" class=""regText""><p align=""center""><img src=""https://www.cellularoutfitter.com/cart/process/eBillme/images/eBillme-tag_logo2.jpg"" width=""147"" height=""47"" border=""0"" alt=""eBillme - My Bank, My Way""></p>" & vbcrlf
'			ReceiptText = ReceiptText & "<p align=""center"" class=""mc-text""><a href=""https://www.ebillme.com/index.php/learnmore2/cellularoutfittercom"" target=""_blank""><img src=""https://www.cellularoutfitter.com/cart/process/eBillme/images/learnmore-button.jpg"" width=""103"" height=""38"" border=""0"" alt=""Need Help?""></a><br>" & vbcrlf
'			ReceiptText = ReceiptText & "<a href=""https://www.ebillme.com/index.php/learnmore2/cellularoutfittercom"" target=""_blank"">Need Help?</a></p></td></tr>" & vbcrlf
'			ReceiptText = ReceiptText & "<tr><td><p style=""text-align:right; font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal"">Account&nbsp;Number:</p></td>" & vbcrlf
'			ReceiptText = ReceiptText & "<td width=""350""><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">" & eBillmeAccount & "</p></td></tr>" & vbcrlf
'			ReceiptText = ReceiptText & "<tr><td><p style=""text-align:right; font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal"">Payee&nbsp;Name:</p></td>" & vbcrlf
'			ReceiptText = ReceiptText & "<td><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">eBillme</p></td></tr>" & vbcrlf
'			ReceiptText = ReceiptText & "<tr><td><p style=""text-align:right; font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal"">Payee&nbsp;Address:</p></td>" & vbcrlf
'			ReceiptText = ReceiptText & "<td><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">PO&nbsp;Box:&nbsp;635808<br>Cincinnati,&nbsp;OH&nbsp;45263-5808</p></td></tr>" & vbcrlf
'			ReceiptText = ReceiptText & "<tr><td><p style=""text-align:right; font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal"">Payee&nbsp;Phone&nbsp;Number:</p></td>" & vbcrlf
'			ReceiptText = ReceiptText & "<td><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">1.866.365.6632</p></td></tr>" & vbcrlf
'			ReceiptText = ReceiptText & "</table>" & vbcrlf
'			ReceiptText = ReceiptText & "<p>&nbsp;</p></td></tr>" & vbcrlf
		end if
	end if
	'END eBillme section
	
	ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""left""><p class=""gray-text"">CUSTOMER INFORMATION:</p></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center"" valign=""top""><table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"" class=""borderBox"">" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td colspan=""7"">&nbsp;</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr>" & vbcrlf
	ReceiptText = ReceiptText & "	<td width=""70"" align=""right"" valign=""top""><b>Name:<br>Email:<br>Phone:<br>Paid&nbsp;By:</b></td>" & vbcrlf
	ReceiptText = ReceiptText & "	<td width=""30"">&nbsp;</td>" & vbcrlf
	ReceiptText = ReceiptText & "	<td width=""200"" align=""left"" valign=""top"">" & sFname & " " & sLname & "<br>" & sEmail & "<br>" & sPhone & "<br>" & strOrderType & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "	<td width=""30"">&nbsp;</td>" & vbcrlf
	ReceiptText = ReceiptText & "	<td colspan=""3"" width=""470"" align=""right"" valign=""top"">" & vbcrlf
	ReceiptText = ReceiptText & "		<table border=""0"" cellpadding=""0"" cellspacing=""0"" class=""borderBox"" style=""border:none;"">" & vbcrlf
	ReceiptText = ReceiptText & "			<tr>" & vbcrlf
	ReceiptText = ReceiptText & "				<td width=""120"" align=""right"" valign=""top""><b>Shipping Address:</b></td>" & vbcrlf
	ReceiptText = ReceiptText & "				<td width=""30"">&nbsp;</td>" & vbcrlf
	ReceiptText = ReceiptText & "				<td width=""320"" align=""left"" valign=""top"">" & sShipAddress & "</td>" & vbcrlf
	ReceiptText = ReceiptText & "			</tr>" & vbcrlf
	ReceiptText = ReceiptText & "			<tr>" & vbcrlf
	ReceiptText = ReceiptText & "				<td width=""100%"" colspan=""3"" style=""padding:1px 0px 3px 20px; color:#f00; font-size:11px;"" align=""left"">" & vbcrlf
	ReceiptText = ReceiptText & "					**Please review your shipping address. <br />If it is incorrect please contact us immediately at <a href=""mailto:updates@cellularoutfitter.com"">updates@cellularoutfitter.com</a>**" & vbcrlf
	ReceiptText = ReceiptText & "				</td>" & vbcrlf
	ReceiptText = ReceiptText & "			</tr>" & vbcrlf
	ReceiptText = ReceiptText & "		</table>" & vbcrlf
	ReceiptText = ReceiptText & "	</td>" & vbcrlf
	ReceiptText = ReceiptText & "</tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td align=""right"" valign=""top"">&nbsp;</td><td>&nbsp;</td><td align=""left"" valign=""top"">&nbsp;</td><td>&nbsp;</td><td align=""right"" valign=""top"" width=""120""><b>Billing Address:</b></td><td width=""30"">&nbsp;</td><td width=""320"" align=""left"" valign=""top"">" & sBillAddress & "</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td colspan=""7"">&nbsp;</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "</table></td></tr>" & vbcrlf

	if not isNumeric(emailOrderTax) or emailOrderTax = "" or isNull(emailOrderTax) then emailOrderTax = "0"
	emailOrderTax = cDbl(emailOrderTax)
	ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""left""><p>&nbsp;</p><p class=""gray-text"">ORDER DETAILS:</p></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center"" valign=""top""><table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"" class=""borderBox"">" & vbcrlf
	
	'get orderdetails
	ReceiptText = ReceiptText & "<tr><td height=""30"" bgcolor=""#DDDDDD"" class=""totalsText"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ITEMS:</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center"" valign=""top""><table width=""780"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"" class=""regText"">" & vbcrlf
	dim oRsOrderDetails
	SQL = 	"SELECT c.brandName, d.modelName, A.itemid,A.PartNumber,A.itemDesc_CO,isnull(b.price, A.price_CO) price_co,B.quantity FROM we_items A INNER JOIN we_orderdetails B ON A.itemid = B.itemid left join we_brands c on a.brandID = c.brandID left join we_models d on a.modelID = d.modelID WHERE B.orderid = '" & nOrderId & "'" &_
			" union " &_
			"SELECT '' as brandName, '' as modelName, A.id as itemid,A.musicSkinsID as PartNumber,A.artist + ' ' + a.designName as itemDesc_CO,A.price_CO,B.quantity FROM we_items_musicSkins A INNER JOIN we_orderdetails B ON A.id = B.itemid WHERE B.orderid = '" & nOrderId & "'"
	set oRsOrderDetails = Server.CreateObject("ADODB.Recordset")
	oRsOrderDetails.open SQL, oConn, 3, 3
	
	if oRsOrderDetails.EOF then
		SQL = replace(lcase(SQL),"we_orders","we_orders_historical")
		SQL = replace(SQL,"we_orderdetails","we_orderdetails_historical")
		set oRsOrderDetails = Server.CreateObject("ADODB.Recordset")
		oRsOrderDetails.open SQL, oConn, 3, 3
	end if
	
	if not oRsOrderDetails.eof then
		dim nCount, nID, nPartNumber, nQty, nPrice, itemdesc_CO, thisSubtotal, itemids
		nCount = 1
		do until oRsOrderDetails.eof
			brandName = oRsOrderDetails("brandName")
			modelName = oRsOrderDetails("modelName")
			nID = oRsOrderDetails("itemid")
			nPartNumber = oRsOrderDetails("PartNumber")
			partNumber = nPartNumber
			nQty = oRsOrderDetails("quantity")
			nPrice = oRsOrderDetails("price_CO")
			itemdesc_CO = insertDetails(oRsOrderDetails("itemDesc_CO"))
			ReceiptText = ReceiptText & "<tr>" & vbcrlf
			if InvoiceType <> "Admin" then
				ReceiptText = ReceiptText & "<td width=""400"" height=""30"" align=""left""><b>" & nCount & ": </b>" & itemdesc_CO & " [" & nPartNumber & "]</td>" & vbcrlf
			else
				ReceiptText = ReceiptText & "<td width=""400"" height=""30"" align=""left""><b>" & nCount & ": </b><a href=""http://www.cellularoutfitter.com/product.asp?itemid=" & nID & """ target=""_blank"">" & itemdesc_CO & "</a> [" & nPartNumber & "]</td>" & vbcrlf
			end if
			ReceiptText = ReceiptText & "<td width=""120"" height=""30"" align=""center"">Price: " & FormatCurrency(nPrice,2) & "</td>" & vbcrlf
			ReceiptText = ReceiptText & "<td width=""120"" height=""30"" align=""center"">Quantity: " & nQty & "</td>" & vbcrlf
			thisSubtotal = formatCurrency(cDbl(nQty) * cDbl(nPrice),2)
			ReceiptText = ReceiptText & "<td width=""160"" height=""30"" align=""right"">&nbsp;&nbsp;&nbsp;Subtotal: " & thisSubtotal & "</td>" & vbcrlf
			ReceiptText = ReceiptText & "</tr>" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""4""><img src=""https://www.cellularoutfitter.com/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
			nCount = nCount + 1
			nTotQty = nTotQty + nQty
			
			if itemids = "" then
				itemids = nID
			else
				itemids = itemids & "," & nID
			end if
			oRsOrderDetails.movenext
		loop
	end if
	oRsOrderDetails.close
	set oRsOrderDetails = nothing
	ReceiptText = ReceiptText & "</table></td></tr>" & vbcrlf
	
	' TOTALS
	ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center"" valign=""top""><table width=""780"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"" class=""totalsText"">" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">Order Total:</td><td width=""80"" align=""right"">" & formatCurrency(emailSubTotal,2) & "</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""2""><img src=""https://www.cellularoutfitter.com/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
	if sPromoCode <> "" then
		ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">Promo Code:</td><td width=""80"" align=""right"">" & sPromoCode & "</td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""2""><img src=""https://www.cellularoutfitter.com/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
	end if
	if round(emailOrderGrandTotal - emailSubTotal - emailOrderTax - emailShipFee,2) < 0 then
		ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">Discount:</td><td width=""80"" align=""right"">" & formatCurrency(emailOrderGrandTotal - emailSubTotal - emailOrderTax - emailShipFee,2) & "</td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""2""><img src=""https://www.cellularoutfitter.com/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
	end if
	if emailOrderTax > 0 then
		ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">CA Resident Tax (" & Application("taxDisplay") & "%):</td><td width=""80"" align=""right"">" & formatCurrency(emailOrderTax,2) & "</td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""2""><img src=""https://www.cellularoutfitter.com/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
	end if
	if nBuysafeamount > 0 then
		ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right""><a href=""http://www.buysafe.com/questions"" target=""_blank"">buySAFE&nbsp;Bond&nbsp;Guarantee</a>:</td><td width=""80"" align=""right"">" & formatCurrency(nBuysafeamount,2) & "</td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""2""><img src=""https://www.cellularoutfitter.com/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
	end if
	ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">Shipping Type:</td><td width=""80"" align=""right"">" & sShipType & "</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""2""><img src=""https://www.cellularoutfitter.com/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">Shipping & Handling Fee:</td><td width=""80"" align=""right"">" & formatCurrency(emailShipFee,2) & "</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""2""><img src=""https://www.cellularoutfitter.com/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
	if (emailOrderGrandTotal - nOrderGrandTotal) > 1 and nOrderGrandTotal > 0 then
		ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">Grand Total:</td><td width=""80"" align=""right"">" & formatCurrency(emailOrderGrandTotal,2) & "</td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td style=""padding:5px;"" align=""left"" colspan=""2""><div style=""padding:5px; background-color:#fff; color:#000; font-weight:bold;"">" & _
									"Thank you for your order! Please remember that you will see two charges on your account that total " & formatCurrency(emailOrderGrandTotal,2) & ". " & _
									"One charge in the amount of " & formatCurrency((emailOrderGrandTotal - nOrderGrandTotal),2) & " and the other in the amount of " & formatCurrency(nOrderGrandTotal,2) & _
									"</td></tr>" & vbcrlf
	else
		ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">Grand Total:</td><td width=""80"" align=""right"">" & formatCurrency(emailOrderGrandTotal,2) & "</td></tr>" & vbcrlf
	end if
	ReceiptText = ReceiptText & "</table></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "</table></td></tr>" & vbcrlf
	
	if InvoiceType <> "Admin" then
		' WEBLOYALTY BANNER
		if incEmail = true then
			ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""left"" valign=""top""><a href=""https://one-time-offer.com/sg/gateway.aspx?v=0353A36353&p=92D2E21232BE77B7D7F706E72FC3C3830303639F16D6060727&cl=1545"" target=""_blank""><img src=""https://www.cellularoutfitter.com/images/cart/300x130_Webloyalty_banner.gif"" width=""300"" height=""130"" border=""0"" alt=""Click here now to claim your Cash Back Incentive on your next purchase when you enroll in Webloyalty's service. See offer and billing details.""></a></td></tr>" & vbcrlf
		else
			ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center"" valign=""top""><table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"">" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td align=""center"" valign=""top"" class=""regText"">&nbsp;</td></tr>" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td class=""regText"">" & vbcrlf
			ReceiptText = ReceiptText & "<p>Your order is complete. As a savvy shopper, you just received the best total price on the Internet. In fact, we are so confident of our low prices that we will match the price of any retailer and credit you the difference! Just send an email with a screenshot of the checkout page to: <a href=""mailto:priceguarantee@cellularoutfitter.com"">priceguarantee@cellularoutfitter.com</a>. More details about our guarantee can be found <a href=""http://www.cellularoutfitter.com/lowest-price.html"">here</a>.</p><br />" & vbcrlf
			ReceiptText = ReceiptText & "</td></tr>" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td class=""regText"">" & vbcrlf
			ReceiptText = ReceiptText & "<p>To ensure delivery of our emails to your inbox, please add support@CellularOutfitter.com to your Address Book. Thank You.</p>" & vbcrlf
			ReceiptText = ReceiptText & "<p>We appreciate your business. Upon shipment out of our warehouse, <strong>you will receive an order shipment confirmation email</strong>. If you have any additional questions regarding your order, please email our customer service department at: <a href='mailto:support@CellularOutfitter.com'>support@CellularOutfitter.com</a>, and be sure to include your full name and order I.D. number in your correspondence.</p>" & vbcrlf
			ReceiptText = ReceiptText & "</td></tr>" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td align=""left"" valign=""top"" class=""regText"">" & vbcrlf
			ReceiptText = ReceiptText & "<SCRIPT language=""JavaScript1.1"" SRC=""https://ad.doubleclick.net/adj/N3446.Wireless_Emporium/B3113366;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?""></SCRIPT>" & vbcrlf
			ReceiptText = ReceiptText & "<NOSCRIPT>" & vbcrlf
			ReceiptText = ReceiptText & "<A HREF=""https://ad.doubleclick.net/jump/N3446.Wireless_Emporium/B3113366;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?""><IMG SRC=""https://ad.doubleclick.net/ad/N3446.Wireless_Emporium/B3113366;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?"" BORDER=0 WIDTH=300 HEIGHT=130 ALT=""""></A>" & vbcrlf
			ReceiptText = ReceiptText & "</NOSCRIPT>" & vbcrlf
			ReceiptText = ReceiptText & "</td></tr>" & vbcrlf
			ReceiptText = ReceiptText & "</table></td></tr>" & vbcrlf
		end if
		
		' SQUARETRADE BANNER
'		ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center""><br><br><a href=""http://www.squaretrade.com/pages/cellphone-landing3?ccode=bs_war_vc_052:cellularoutfitter"" target=""_blank""><img src=""https://www.cellularoutfitter.com/images/Squaretrade/banner_728x90.jpg"" border=""0"" width=""728"" height=""90"" alt=""SquareTrade - Warranties that make sense!""></a></td></tr>" & vbcrlf
		
		' QUESTIONS
		ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""left""><p>&nbsp;</p><p class=""gray-text"">QUESTIONS ABOUT:</p></td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""left"" valign=""top""><table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"" class=""borderBox"">" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td align=""left"" class=""regText"">&nbsp;</td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td align=""left"" class=""regText"" style=""padding:5px;"">" & vbcrlf
		ReceiptText = ReceiptText & "<p>Questions about your order or our service? Visit our <a href=""http://www.CellularOutfitter.com/faq.asp"">Frequently Asked Questions</a> Page.<br>" & vbcrlf
		ReceiptText = ReceiptText & "<a href=""http://www.CellularOutfitter.com/track-your-order.html""><b>Track Your Order Online</b></a></p>" & vbcrlf
		ReceiptText = ReceiptText & "<p><b>Please visit us again soon!</b> - <a href=""http://www.CellularOutfitter.com/"">CellularOutfitter.com</a></p>" & vbcrlf
		ReceiptText = ReceiptText & "</td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td align=""left"" class=""regText"">&nbsp;</td></tr></table></td></tr>" & vbcrlf
	else
		ReceiptText = ReceiptText & "<tr><td align=""center"" valign=""top"" class=""regText""><p>&nbsp;</p><form><input type=""button"" onclick=""window.close();"" value=""Close""></form></td></tr>" & vbcrlf
	end if
	
	ReceiptText = ReceiptText & "</table>" & vbcrlf
end if
%>
