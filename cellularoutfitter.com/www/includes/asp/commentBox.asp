<%''' **************** Start Comment Card ********************* %>
<form id="frmComment" name="frmComment" style="margin:0px; padding:0px;">
<div style="width:700px; height:300px; border-top:2px solid #5871a7;">
    <div style="width:658px; height:298px; background-color:#ebebeb; border:1px solid #999; padding:10px 20px 0px 20px;">
        <div style="font-size:20px; font-weight:bold; color:#3C599B; text-align:left;">Comment Card</div>
        <div style="font-size:12px; color:#444; padding-top:10px; text-align:left;">
            We want to hear from you! We work hard to make sure our site offers the best shopping experience possible. 
            If you have any suggestions on how we can make things even better - whatever it is, please drop us a note!
        </div>
        <div style="height:30px; margin-top:20px;">
        	<div style="float:left; font-size:15px; font-weight:bold; color:#3C599B; width:150px;">Email (optional):</div>
            <div style="float:left; padding-left:10px;"><input type="text" name="email" value="" size="32" /></div>
        </div>
        <div style="height:30px; margin-top:10px;">
        	<div style="float:left; font-size:15px; font-weight:bold; color:#3C599B; width:150px;">Phone (optional):</div>
            <div style="float:left; padding-left:10px;"><input type="text" name="phone" value="" size="32" /></div>
        </div>
        <div style="margin-top:10px; float:right; width:650px; text-align:right;"><TEXTAREA rows="3" cols="65" style="width:490px;" id="textarea1" name="textarea1" onfocus="clearDefaultTxt(this)">Add a comment...</TEXTAREA></div>
        <div style="float:right; margin-top:10px;"><img src="/images/buttons/commentSubmit.png" alt="" onclick="return checkall();" border="0" width="115" height="23" style="cursor:pointer;" /></div>
    </div>
</div>
</form>
<%''' **************** End Comment Card ********************* %>
<script language="javascript">
	<% ''' *********** For Comments Validations -- After validation it will send the request to server using with ajax%>
	var commentLoop = 0
	function clearDefaultTxt(curField) {
		if (curField.value == "Add a comment...") {
			curField.value = ""
		}
	}
	function ajax_cb(newURL,rLoc) {
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
			if (httpRequest.overrideMimeType) {
				httpRequest.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpRequest.open('GET', newURL, true);
		httpRequest.onreadystatechange = function(){
			if (httpRequest.readyState == 4) {
				if (httpRequest.status == 200) {
					badRun = 0
					var rVal = httpRequest.responseText
					if (rVal == "") {
						alert("No data available")
					}
					else if (rVal == "refresh") {
						curLoc = window.location
						window.location = curLoc
					}
					else if(document.getElementById(rLoc)) {
						document.getElementById(rLoc).innerHTML = rVal
					}
					rVal = null;
				}
				else {
					document.getElementById("testZone").innerHTML = newURL
				}
			}
		};
		httpRequest.send(null);
	}
	
	function checkall() {	 
		var GreForm = this.document.frmComment;
		
		if (GreForm.textarea1.value=="" || GreForm.textarea1.value=="Add a comment...") {
			alert("Enter Your Comments");
			GreForm.textarea1.focus();
			return false;
		}
		<%
		useURL = request.ServerVariables("HTTP_X_REWRITE_URL")
		if useURL = "/" then useURL = "index.asp"
		%>
		useURL = escape("<%=useURL%>")
		ajax_cb('/ajax/saveCommentCard.asp?page=' + useURL + '&email=' + document.frmComment.email.value + '&phone=' + document.frmComment.phone.value + '&comment=' + escape(document.frmComment.textarea1.value),'dumpZone')
		var commentValue = GreForm.textarea1.value
		GreForm.textarea1.value = ""
		
		setTimeout("chkCommentReturn()",500)
		
		return true;
	}
	
	function chkCommentReturn() {
		commentLoop++
		if (commentLoop < 5) {
			if (document.getElementById("dumpZone").innerHTML != "") {
				if (document.getElementById("dumpZone").innerHTML == "commentGood") {
					alert("Comment saved")
				}
				else {
					alert("Error saving comment\nPlease resubmit\n" + document.getElementById("dumpZone").innerHTML)
					document.getElementById('textarea1').value = commentValue
				}
			}
		}
		else {
			alert("Error saving comment\nPlease resubmit")
			document.getElementById('textarea1').value = commentValue
		}
	}
</script>