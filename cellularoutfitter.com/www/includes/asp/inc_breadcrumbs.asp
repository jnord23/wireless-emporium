<tr>
	<td class="top-sublink-gray">
    	<div>
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" style="float:left;"><a class="top-sublink-gray" href="http://www.CellularOutfitter.com/" itemprop="url"><span itemprop="title">Home</span></a>&nbsp;&rsaquo;&nbsp;</div>
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" style="float:left;"><a class="top-sublink-gray" href="http://www.cellularoutfitter.com/b-<%=brandID & "-" & formatSEO(brandName)%>-cell-phone-accessories.html" itemprop="url"><span itemprop="title"><%=brandName%> Cell Phone Accessories</span></a>&nbsp;&rsaquo;&nbsp;</div>
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" style="float:left;"><a class="top-sublink-gray" href="http://www.cellularoutfitter.com/m-<%=modelID & "-" & formatSEO(brandName) & "-" & formatSEO(modelName)%>-cell-phone-accessories.html" itemprop="url"><span itemprop="title"><%=brandName & " " & modelName%> Cell Phone Accessories</span></a>&nbsp;&rsaquo;&nbsp;</div>
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" style="float:left;"><a class="top-sublink-gray" href="http://www.cellularoutfitter.com/sb-<%=brandID & "-sm-" & modelID & "-sc-" & typeID & "-" & formatSEO(brandName) & "-" & formatSEO(modelName) & "-" & formatSEO(categoryName)%>-cell-phone-accessories.html" itemprop="url"><span itemprop="title"><%=brandName & " " & modelName & " " & nameSEO(categoryName)%></span></a>&nbsp;&rsaquo;&nbsp;</div>
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" style="float:left;"><span class="top-sublink-blue" itemprop="title"><%=itemDesc_CO%></span></div>        
        </div>
		<%
'		dim bottomBreadcrumb
'		bottomBreadcrumb = "<a class=""top-sublink-gray"" href=""http://www.CellularOutfitter.com/"">Cell Phone Accessories</a>&nbsp;>&nbsp;"
'		if typeID = 16 then
'			bottomBreadcrumb = bottomBreadcrumb & "<a class=""top-sublink-gray"" href=""http://www.cellularoutfitter.com/wholesale-cell-phones.html"">" & nameSEO(categoryName) & "</a>&nbsp;>&nbsp;"
'			bottomBreadcrumb = bottomBreadcrumb & "<a class=""top-sublink-gray"" href=""http://www.cellularoutfitter.com/cp-sb-" & brandID & "-" & formatSEO(categoryName) & ".html>" & brandName & " " & nameSEO(categoryName) & "</a>&nbsp;>&nbsp;"
'		else
'			bottomBreadcrumb = bottomBreadcrumb & "<a class=""top-sublink-gray"" href=""http://www.cellularoutfitter.com/c-" & typeID & "-cell-phone-" & formatSEO(categoryName) & ".html"">Cell Phone " & nameSEO(categoryName) & "</a>&nbsp;>&nbsp;"
'			bottomBreadcrumb = bottomBreadcrumb & "<a class=""top-sublink-gray"" href=""http://www.cellularoutfitter.com/sc-" & typeID & "-sb-" & brandID & "-cell-phone-" & formatSEO(categoryName) & "-for-" & formatSEO(brandName) & ".html"">Cell Phone " & nameSEO(categoryName) & " for " & brandName & "</a>&nbsp;>&nbsp;"
'			bottomBreadcrumb = bottomBreadcrumb & "<a class=""top-sublink-gray"" href=""http://www.cellularoutfitter.com/sc-" & typeID & "-sb-" & brandID & "-sm-" & modelID & "-cell-phone-" & formatSEO(categoryName) & "-for-" & formatSEO(brandName) & "-" & formatSEO(modelName) & ".html"">Cell Phone " & nameSEO(categoryName) & " for " & brandName & " " & modelName & "</a>&nbsp;>&nbsp;"
'		end if
'		bottomBreadcrumb = bottomBreadcrumb & "<span class=""top-sublink-blue"">" & itemDesc_CO & "</span>"
		%>
	</td>
</tr>
