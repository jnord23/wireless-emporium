<%
function CDOSend(strTo,strFrom,strSubject,strBody)
	dim objErrMail
	set objErrMail = Server.CreateObject("CDO.Message")
	with objErrMail
		.From = strFrom
		.To = strTo
		.Subject = strSubject
'		.cc = "ruben@wirelessemporium.com"
		.HTMLBody = CStr("" & strBody)
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Item("urn:schemas:httpmail:sender") = "sales@cellularoutfitter.com"
		.Configuration.Fields.Update
		.Send
	end with
	set objErrMail = nothing
end function

function CDOSend2(strTo,strFrom,strSubject,strBody,strCC,strBCC)
	dim objErrMail
	set objErrMail = Server.CreateObject("CDO.Message")
	with objErrMail
		.From = strFrom
		.To = strTo
		.Subject = strSubject
		.cc = strCC
		.bcc = strBCC
		.HTMLBody = CStr("" & strBody)
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		.Configuration.Fields.Item("urn:schemas:httpmail:sender") = "sales@cellularoutfitter.com"
		.Configuration.Fields.Update
		.Send
	end with
	set objErrMail = nothing
end function
%>


