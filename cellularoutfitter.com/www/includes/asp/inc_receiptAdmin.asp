<%
dim ReceiptText
ReceiptText = ""

useStore = session("useStore")
useStoreID = session("useStoreID")
session("useStore") = null
session("useStoreID") = null

if isnull(useStoreID) or len(useStoreID) < 1 then useStoreID = 2 else useStoreID = cdbl(useStoreID)

if useStoreID = 0 then
	useLogo = "we_logo.jpg"
	storeName = "WirelessEmporium.com"
	useStore = "we"
end if
if useStoreID = 1 then
	useLogo = "ca_logo.jpg"
	storeName = "CellphoneAccents.com"
	useStore = "ca"
end if
if useStoreID = 2 then
	useLogo = "CellularOutfitter.jpg"
	storeName = "CellularOutfitter.com"
	useStore = "co"
end if
if useStoreID = 3 then
	useLogo = "ps_logo.jpg"
	storeName = "PhoneSale.com"
	useStore = "ps"
end if

session("errorSQL2") = "useStoreID:" & useStoreID & "<br>useLogo:" & useLogo & "<br>storeName:" & storeName

dim sPromoCode
SQL = "SELECT B.PromoCode FROM we_orders A INNER JOIN " & useStore & "_coupons AS B ON A.couponid=B.couponid WHERE A.orderid = '" & nOrderID & "'"
session("errorSQL") = sql
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
if not RS.eof then sPromoCode = RS("PromoCode")

dim sFname, sLname, sEmail, sPhone
dim saddress1, saddress2, sCity, sstate, szip, sCountry
dim sShipAddress, sBillAddress
dim oRsCust, oRsOrd
dim pathPDF, pathTXT

SQL = "SELECT * FROM " & useStore & "_accounts WHERE accountid = '" & nAccountId & "'"
set oRsCust = Server.CreateObject("ADODB.Recordset")
oRsCust.open SQL, oConn, 3, 3
if oRsCust.eof then
	ReceiptText = "<h3>Customer account" & nAccountId & " not found!</h3>"
else
	sFname = oRsCust("fname")
	sLname = oRsCust("lname")
	sEmail = oRsCust("email")
	sPhone = oRsCust("phone")
end if

SQL = "SELECT * FROM we_orders WHERE orderid = '" & nOrderId & "'"
set oRsOrd = Server.CreateObject("ADODB.Recordset")
oRsOrd.open SQL, oConn, 3, 3

if oRsOrd.EOF then
	oRsOrd.close
	set oRsOrd = nothing
	set oRsOrd = Server.CreateObject("ADODB.Recordset")

	SQL = replace(lcase(SQL),"we_orders","we_orders_historical")
	oRsOrd.open SQL, oConn, 3, 3
end if

if oRsOrd.eof then ReceiptText = "<h3>Order" & nOrderId & " not found!</h3>"

'format the shipping and billing addresses
dim nShippingid
nShippingid = oRsOrd("shippingid")
if nShippingid > 0 then
	strSql = "SELECT * FROM we_addl_shipping_addr WHERE id='" & nShippingid & "'"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open strSql, oConn, 3, 3
	if not RS.eof then
		saddress1 = RS("saddress1")
		saddress2 = RS("saddress2")
		sCity = RS("sCity")
		sstate = RS("sstate")
		szip = RS("szip")
		sCountry = RS("sCountry")
	end if
else
	saddress1 = oRsCust("saddress1")
	saddress2 = oRsCust("saddress2")
	sCity = oRsCust("sCity")
	sstate = oRsCust("sstate")
	szip = oRsCust("szip")
	sCountry = oRsCust("sCountry")
end if

sShipAddress = sAddress1
if sAddress2 <> "" then sShipAddress = sShipAddress & "<br>" & saddress2
sShipAddress = sShipAddress & "<br>" & sCity & ", " & sState & "&nbsp;" & sZip & "<br>" & sCountry

sBillAddress = oRsCust("bAddress1")
if oRsCust("bAddress2") <> "" then sBillAddress = sBillAddress & "<br>" & oRsCust("bAddress2")
sBillAddress = sBillAddress & "<br>" & oRsCust("bCity") & ", " & oRsCust("bState") & "&nbsp;" & oRsCust("bZip") & "<br>" & oRsCust("bCountry")

oRsCust.close
set oRsCust = nothing

if ReceiptText = "" then
	'format order info
	'dim nOrderSubTotal
	dim sShipType, nShipFee, nOrderTax, nBuysafeamount, extOrderType, extOrderNumber, nOrderDateTime
	nOrderSubTotal = oRsOrd("ordersubtotal")
	sShipType = oRsOrd("shiptype")
	nShipFee = oRsOrd("ordershippingfee")
	nOrderTax = oRsOrd("orderTax")
	nBuysafeamount = oRsOrd("BuySafeAmount")
	nOrderGrandTotal = oRsOrd("ordergrandtotal")
	extOrderType = oRsOrd("extOrderType")
	extOrderNumber = oRsOrd("extOrderNumber")
	nOrderDateTime = oRsOrd("orderdatetime")

	pathPDF = oRsOrd("processPDF")
	pathTXT = oRsOrd("processTXT")

	dim sFileTo
	sFileTo = ""
	
	if len(pathPDF) > 1 or len(pathTXT) > 1 then
		sFileTo = "File to "
		if len(pathPDF) > 1 then 
			sFileTo = sFileTo & "<a href='/admin/tempPDF/" & pathPDF & "' target='_blank'>PDF</a>"
		end if
		if len(pathPDF) > 1 then 
			sFileTo = sFileTo & ",&nbsp;<a href='/admin/tempTXT/" & pathTXT & "' target='_blank'>TXT</a>"
		end if		
	end if

	'sFileTo = "File to "
	'sFileTo = sFileTo & "<a href='/admin/tempPDF/" & "_terrytest_WE_SalesOrders_2-2-2011_PRIO.pdf" & "' target='_blank'>PDF</a>"
	'sFileTo = sFileTo & ",&nbsp;<a href='/admin/tempTXT/" & "_terrytest_WE_2-2-2011_PRIO.txt" & "' target='_blank'>TXT</a>"
	
	dim strOrderType
	select case extOrderType
		case 1 : strOrderType = "Paypal<br>" & extOrderNumber
		case 2 : strOrderType = "Google Checkout<br>" & extOrderNumber
		case 3
			strOrderType = "eBillme"
			eBillme = "eBillme"
			eBillmeAccount = extOrderNumber
		case else : strOrderType = "Credit Card"
	end select
	
	oRsOrd.close
	set oRsOrd = nothing
	
	'===================
	'NEW WEBLOYALTY CODE
	dim RSwl, cctype, country, pcode, strToEncrypt, strEncryptedValue
	SQL = "SELECT A.CCtype, B.bState, B.bZip FROM we_ccinfo A INNER JOIN " & useStore & "_Accounts B ON A.accountid = B.accountid WHERE A.orderid='" & nOrderID & "'"
	set RSwl = Server.CreateObject("ADODB.Recordset")
	RSwl.open SQL, oConn, 3, 3
	if not RSwl.eof then
		cctype = RSwl("CCtype")
		if len(RSwl("bState")) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",RSwl("bState")) > 0 then
			country = "CANADA"
		else
			country = "US"
		end if
		pcode = RSwl("bZip")
	end if
	RSwl.close
	set RSwl = nothing
	
	' Put together string � concatenate all required values
	if incEmail = true then
		dim yesexiturl, noexiturl
		yesexiturl = "https://www." & storeName & "/confirm.asp?a=" & nAccountID & "&o=" & nOrderID & "&d=" & nOrderGrandTotal & "&c=" & nOrderSubTotal
		noexiturl = yesexiturl
		strToEncrypt = "cref" & chr(255) & nOrderID & chr(255) & "cctype" & chr(255) & cctype & chr(255) & "country" & chr(255) & country & chr(255) & "pcode" & chr(255) & pcode & chr(255) & "yesexiturl" & chr(255) & yesexiturl & chr(255) & "noexiturl" & chr(255) & noexiturl
	else
		strToEncrypt = "cref" & chr(255) & nOrderID & chr(255) & "cctype" & chr(255) & cctype & chr(255) & "country" & chr(255) & country & chr(255) & "pcode" & chr(255) & pcode
	end if
	' Encrypt it
	strEncryptedValue = SimpleEncrypt(strToEncrypt)
	'===================
	
	ReceiptText = "<html>"
	ReceiptText = ReceiptText & "<head>" & vbcrlf
	ReceiptText = ReceiptText & "<title>Thank you for your purchase on " & storeName & "</title>" & vbcrlf
	ReceiptText = ReceiptText & "<style type=""text/css"">" & vbcrlf
	ReceiptText = ReceiptText & "<!--" & vbcrlf
	ReceiptText = ReceiptText & ".regText {font-family: Arial; font-size: 9pt; font-weight: normal; color: #000000;}" & vbcrlf
	ReceiptText = ReceiptText & ".totalsText {font-family: Arial, Helvetica, sans-serif; font-size: 10pt; font-weight: bold; color: #000000;}" & vbcrlf
	ReceiptText = ReceiptText & ".blue-text {font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #3399CC;}" & vbcrlf
	ReceiptText = ReceiptText & ".black-text {font-family: Arial, Helvetica, sans-serif; font-size: 16pt; font-weight: normal; color: #000000;}" & vbcrlf
	ReceiptText = ReceiptText & ".gray-text {font-family: Arial, Helvetica, sans-serif; font-size: 16pt; font-weight: normal; color: #999999;}" & vbcrlf
	ReceiptText = ReceiptText & ".borderBox {font-family: Arial; font-size: 9pt; font-weight: normal; color: #000000; border-style: solid; border-width: 1px; border-color: #999999; background-color: #EEEEEE;}" & vbcrlf
	ReceiptText = ReceiptText & "-->" & vbcrlf
	ReceiptText = ReceiptText & "</style>" & vbcrlf
	ReceiptText = ReceiptText & "</head>" & vbcrlf
	ReceiptText = ReceiptText & "<body>" & vbcrlf
	
	ReceiptText = ReceiptText & "<table width=""800"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"">" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center""><a href=""http://www." & storeName & "/""><img src=""https://www." & storeName & "/images/" & useLogo & """ border=""0"" alt=""" & storeName & " - Wholesale Prices to the Public""></a></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""100%"" height=""75"" align=""center"" bgcolor=""#EEEEEE""><p class=""blue-text""><span style=""font-size: 16pt;"">Thank you for your purchase on " & storeName & ".</span><br><span style=""font-size: 12pt;"">Please print and keep a copy of this receipt for your records.</span></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""100%"" height=""30"" align=""center""><img src=""https://www." & storeName & "/images/hori_line.jpg"" width=""800"" height=""7"" border=""0""></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""100%"" height=""50"" align=""right"">" & sFileTo & "<p class=""black-text""><span style=""font-weight: normal;"">Order&nbsp;Date:&nbsp;&nbsp;" & dateValue(nOrderDateTime) & "</span><br><span style=""font-weight: bold;"">Order&nbsp;ID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & nOrderId & "</span></td></tr>" & vbcrlf
	
	'START eBillme section
	if eBillme = "eBillme" then
		eBillmeAccountArray = split(eBillmeAccount," | ")
		if Ubound(eBillmeAccountArray) > 0 then
			eBillmeAccount = eBillmeAccountArray(1)
			ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center""><p>&nbsp;</p>" & vbcrlf
			ReceiptText = ReceiptText & "<table width=""600"" border=""0"" cellpadding=""5"" cellspacing=""0"" align=""center"" style=""border: 1px solid black;"">" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td colspan=""3"" bgcolor=""#006699"" height=""25""><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #FFFFFF; font-weight: bold"">To receive your order:</p></td></tr>" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td colspan=""2""><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">Go to your bank's website, setup and pay eBillme " & formatCurrency(nOrderGrandTotal) & "</p>" & vbcrlf
			ReceiptText = ReceiptText & "<p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal""><em>First time users may require the following information:</em></p></td>" & vbcrlf
			ReceiptText = ReceiptText & "<td rowspan=""5"" align=""center"" width=""150"" class=""regText""><p align=""center""><img src=""https://www." & storeName & "/cart/process/eBillme/images/eBillme-tag_logo2.jpg"" width=""147"" height=""47"" border=""0"" alt=""eBillme - My Bank, My Way""></p>" & vbcrlf
			ReceiptText = ReceiptText & "<p align=""center"" class=""mc-text""><a href=""https://www.ebillme.com/index.php/learnmore2/cellularoutfittercom"" target=""_blank""><img src=""https://www." & storeName & "/cart/process/eBillme/images/learnmore-button.jpg"" width=""103"" height=""38"" border=""0"" alt=""Need Help?""></a><br>" & vbcrlf
			ReceiptText = ReceiptText & "<a href=""https://www.ebillme.com/index.php/learnmore2/cellularoutfittercom"" target=""_blank"">Need Help?</a></p></td></tr>" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td><p style=""text-align:right; font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal"">Account&nbsp;Number:</p></td>" & vbcrlf
			ReceiptText = ReceiptText & "<td width=""350""><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">" & eBillmeAccount & "</p></td></tr>" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td><p style=""text-align:right; font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal"">Payee&nbsp;Name:</p></td>" & vbcrlf
			ReceiptText = ReceiptText & "<td><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">eBillme</p></td></tr>" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td><p style=""text-align:right; font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal"">Payee&nbsp;Address:</p></td>" & vbcrlf
			ReceiptText = ReceiptText & "<td><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">PO&nbsp;Box:&nbsp;635808<br>Cincinnati,&nbsp;OH&nbsp;45263-5808</p></td></tr>" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td><p style=""text-align:right; font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #AAAAAA; font-weight: normal"">Payee&nbsp;Phone&nbsp;Number:</p></td>" & vbcrlf
			ReceiptText = ReceiptText & "<td><p style=""font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: bold"">1.866.365.6632</p></td></tr>" & vbcrlf
			ReceiptText = ReceiptText & "</table>" & vbcrlf
			ReceiptText = ReceiptText & "<p>&nbsp;</p></td></tr>" & vbcrlf
		end if
	end if
	'END eBillme section
	
	ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""left""><p class=""gray-text"">CUSTOMER INFORMATION:</p></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center"" valign=""top""><table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"" class=""borderBox"">" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td colspan=""7"">&nbsp;</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""70"" align=""right"" valign=""top""><b>Name:<br>Email:<br>Phone:<br>Paid&nbsp;By:</b></td><td width=""30"">&nbsp;</td><td width=""200"" align=""left"" valign=""top"">" & sFname & " " & sLname & "<br>" & sEmail & "<br>" & sPhone & "<br>" & strOrderType & "</td><td width=""30"">&nbsp;</td><td width=""120"" align=""right"" valign=""top""><b>Shipping Address:</b></td><td width=""30"">&nbsp;</td><td width=""320"" align=""left"" valign=""top"">" & sShipAddress & "</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td align=""right"" valign=""top"">&nbsp;</td><td>&nbsp;</td><td align=""left"" valign=""top"">&nbsp;</td><td>&nbsp;</td><td align=""right"" valign=""top""><b>Billing Address:</b></td><td>&nbsp;</td><td align=""left"" valign=""top"">" & sBillAddress & "</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td colspan=""7"">&nbsp;</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "</table></td></tr>" & vbcrlf
	
	if not isNumeric(nOrderTax) or nOrderTax = "" or isNull(nOrderTax) then nOrderTax = "0"
	nOrderTax = cDbl(nOrderTax)
	ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""left""><p>&nbsp;</p><p class=""gray-text"">ORDER DETAILS:</p></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center"" valign=""top""><table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"" class=""borderBox"">" & vbcrlf
	
	'get orderdetails
	ReceiptText = ReceiptText & "<tr><td height=""30"" bgcolor=""#DDDDDD"" class=""totalsText"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ITEMS:</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center"" valign=""top""><table width=""780"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"" class=""regText"">" & vbcrlf
	dim oRsOrderDetails
	SQL = "SELECT A.itemid,A.PartNumber,A.itemDesc_CO,A.price_CO,B.quantity FROM we_items A INNER JOIN we_orderdetails B ON A.itemid = B.itemid WHERE B.orderid = '" & nOrderId & "'"
	set oRsOrderDetails = Server.CreateObject("ADODB.Recordset")
	oRsOrderDetails.open SQL, oConn, 3, 3
	
	if oRsOrderDetails.eof then
		oRsOrderDetails.close
		set oRsOrderDetails = nothing
		set oRsOrderDetails = Server.CreateObject("ADODB.Recordset")
		
		SQL = replace(lcase(SQL),"we_orderdetails","we_orderdetails_historical")
		
		oRsOrderDetails.open SQL, oConn, 3, 3
	end if
	
	if not oRsOrderDetails.eof then
		dim nCount, nID, nPartNumber, nQty, nPrice, itemdesc_CO, thisSubtotal
		nCount = 1
		do until oRsOrderDetails.eof
			nID = oRsOrderDetails("itemid")
			nPartNumber = oRsOrderDetails("PartNumber")
			nQty = oRsOrderDetails("quantity")
			nPrice = oRsOrderDetails("price_CO")
			itemdesc_CO = oRsOrderDetails("itemDesc_CO")
			ReceiptText = ReceiptText & "<tr>" & vbcrlf
			if InvoiceType <> "Admin" then
				ReceiptText = ReceiptText & "<td width=""400"" height=""30"" align=""left""><b>" & nCount & ": </b>" & itemdesc_CO & " [" & nPartNumber & "]</td>" & vbcrlf
			else
				ReceiptText = ReceiptText & "<td width=""400"" height=""30"" align=""left""><b>" & nCount & ": </b><a href=""http://www." & storeName & "/product.asp?itemid=" & nID & """ target=""_blank"">" & itemdesc_CO & "</a> [" & nPartNumber & "]</td>" & vbcrlf
			end if
			ReceiptText = ReceiptText & "<td width=""120"" height=""30"" align=""center"">Price: " & FormatCurrency(nPrice,2) & "</td>" & vbcrlf
			ReceiptText = ReceiptText & "<td width=""120"" height=""30"" align=""center"">Quantity: " & nQty & "</td>" & vbcrlf
			thisSubtotal = formatCurrency(cDbl(nQty) * cDbl(nPrice),2)
			ReceiptText = ReceiptText & "<td width=""160"" height=""30"" align=""right"">&nbsp;&nbsp;&nbsp;Subtotal: " & thisSubtotal & "</td>" & vbcrlf
			ReceiptText = ReceiptText & "</tr>" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""4""><img src=""https://www." & storeName & "/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
			nCount = nCount + 1
			nTotQty = nTotQty + nQty
			oRsOrderDetails.movenext
		loop
	end if
	oRsOrderDetails.close
	set oRsOrderDetails = nothing
	ReceiptText = ReceiptText & "</table></td></tr>" & vbcrlf
	
	' TOTALS
	ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center"" valign=""top""><table width=""780"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"" class=""totalsText"">" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">Order Total:</td><td width=""80"" align=""right"">" & formatCurrency(nOrderSubTotal,2) & "</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""2""><img src=""https://www." & storeName & "/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
	if sPromoCode <> "" then
		ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">Promo Code:</td><td width=""80"" align=""right"">" & sPromoCode & "</td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""2""><img src=""https://www." & storeName & "/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
	end if
	if round(nOrderGrandTotal - nOrderSubTotal - nOrderTax - nShipFee,2) < 0 then
		ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">Discount:</td><td width=""80"" align=""right"">" & formatCurrency(nOrderGrandTotal - nOrderSubTotal - nOrderTax - nShipFee,2) & "</td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""2""><img src=""https://www." & storeName & "/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
	end if
	if nOrderTax > 0 then
		ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">CA Resident Tax (7.75%):</td><td width=""80"" align=""right"">" & formatCurrency(nOrderTax,2) & "</td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""2""><img src=""https://www." & storeName & "/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
	end if
	if nBuysafeamount > 0 then
		ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right""><a href=""http://www.buysafe.com/questions"" target=""_blank"">buySAFE&nbsp;Bond&nbsp;Guarantee</a>:</td><td width=""80"" align=""right"">" & formatCurrency(nBuysafeamount,2) & "</td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""2""><img src=""https://www." & storeName & "/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
	end if
	ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">Shipping Type:</td><td width=""80"" align=""right"">" & sShipType & "</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""2""><img src=""https://www." & storeName & "/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">Shipping & Handling Fee:</td><td width=""80"" align=""right"">" & formatCurrency(nShipFee,2) & "</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td bgcolor=""#999999"" colspan=""2""><img src=""https://www." & storeName & "/images/spacer.gif"" width=""780"" height=""1"" border=""0""></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "<tr><td width=""700"" height=""30"" align=""right"">Grand Total:</td><td width=""80"" align=""right"">" & formatCurrency(nOrderGrandTotal,2) & "</td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "</table></td></tr>" & vbcrlf
	ReceiptText = ReceiptText & "</table></td></tr>" & vbcrlf
	
	if InvoiceType <> "Admin" then
		' WEBLOYALTY BANNER
		if incEmail = true then
			ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""left"" valign=""top""><a href=""https://one-time-offer.com/sg/gateway.aspx?v=0353A36353&p=92D2E21232BE77B7D7F706E72FC3C3830303639F16D6060727&cl=1545"" target=""_blank""><img src=""https://www." & storeName & "/images/cart/300x130_Webloyalty_banner.gif"" width=""300"" height=""130"" border=""0"" alt=""Click here now to claim your Cash Back Incentive on your next purchase when you enroll in Webloyalty's service. See offer and billing details.""></a></td></tr>" & vbcrlf
		else
			ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center"" valign=""top""><table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"">" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td align=""center"" valign=""top"" class=""regText"">&nbsp;</td></tr>" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td align=""left"" valign=""top"" class=""regText"">" & vbcrlf
			ReceiptText = ReceiptText & "<SCRIPT language=""JavaScript1.1"" SRC=""https://ad.doubleclick.net/adj/N3446.Wireless_Emporium/B3113366;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?""></SCRIPT>" & vbcrlf
			ReceiptText = ReceiptText & "<NOSCRIPT>" & vbcrlf
			ReceiptText = ReceiptText & "<A HREF=""https://ad.doubleclick.net/jump/N3446.Wireless_Emporium/B3113366;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?""><IMG SRC=""https://ad.doubleclick.net/ad/N3446.Wireless_Emporium/B3113366;q=" & strEncryptedValue & ";sz=300x130;ord=" & myTime & "?"" BORDER=0 WIDTH=300 HEIGHT=130 ALT=""""></A>" & vbcrlf
			ReceiptText = ReceiptText & "</NOSCRIPT>" & vbcrlf
			ReceiptText = ReceiptText & "</td></tr>" & vbcrlf
			ReceiptText = ReceiptText & "<tr><td class=""regText"">" & vbcrlf
			ReceiptText = ReceiptText & "<p>To ensure delivery of our emails to your inbox, please add support@" & storeName & " to your Address Book. Thank You.</p>" & vbcrlf
			ReceiptText = ReceiptText & "<p>We appreciate your business. Upon shipment out of our warehouse, <strong>you will receive an order shipment confirmation email</strong>. If you have any additional questions regarding your order, please email our customer service department at: <a href='mailto:support@" & storeName & "'>support@" & storeName & "</a>, and be sure to include your full name and order I.D. number in your correspondence.</p>" & vbcrlf
			ReceiptText = ReceiptText & "</td></tr>" & vbcrlf
			ReceiptText = ReceiptText & "</table></td></tr>" & vbcrlf
		end if
		
		' SQUARETRADE BANNER
		ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""center""><br><br><a href=""http://www.squaretrade.com/pages/cellphone-landing3?ccode=bs_war_vc_052:cellularoutfitter"" target=""_blank""><img src=""https://www." & storeName & "/images/Squaretrade/banner_728x90.jpg"" border=""0"" width=""728"" height=""90"" alt=""SquareTrade - Warranties that make sense!""></a></td></tr>" & vbcrlf
		
		' QUESTIONS
		ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""left""><p>&nbsp;</p><p class=""gray-text"">QUESTIONS ABOUT:</p></td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td width=""100%"" align=""left"" valign=""top""><table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"" class=""borderBox"">" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td align=""left"" class=""regText"">&nbsp;</td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td align=""left"" class=""regText"">" & vbcrlf
		ReceiptText = ReceiptText & "<p>Questions about your order or our service? Visit our <a href=""http://www." & storeName & "/faq.asp"">Frequently Asked Questions</a> Page.<br>" & vbcrlf
		ReceiptText = ReceiptText & "<a href=""http://www." & storeName & "/track-your-order.html""><b>Track Your Order Online</b></a></p>" & vbcrlf
		ReceiptText = ReceiptText & "<p><b>Please visit us again soon!</b> - <a href=""http://www." & storeName & "/"">" & storeName & "</a></p>" & vbcrlf
		ReceiptText = ReceiptText & "</td></tr>" & vbcrlf
		ReceiptText = ReceiptText & "<tr><td align=""left"" class=""regText"">&nbsp;</td></tr></table></td></tr>" & vbcrlf
	else
		ReceiptText = ReceiptText & "<tr><td align=""center"" valign=""top"" class=""regText""><p>&nbsp;</p><form><input type=""button"" onclick=""window.close();"" value=""Close""></form></td></tr>" & vbcrlf
	end if
	
	ReceiptText = ReceiptText & "</table>" & vbcrlf
end if
%>
