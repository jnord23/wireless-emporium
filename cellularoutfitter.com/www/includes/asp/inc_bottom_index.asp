							<tr>
                            	<td>
									<%
                                    if myModel > 0 then
                                        sql = 	"select top 10 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO, c.modelID from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 1 and c.modelID = " & myModel & " group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO, c.modelID " &_
                                                "union " &_
                                                "select top 10 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO, 0 as modelID from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 1 and b.orderdatetime > '" & date-60 & "' and c.brandID = 20 group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by c.modelID desc, 2 desc"
                                    elseif myBrand > 0 then
                                        sql = "select top 10 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 1 and b.orderdatetime > '" & date-60 & "' and c.brandID = " & myBrand & " group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by 2 desc"
                                    else
                                        sql = "select top 10 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 1 and b.orderdatetime > '" & date-60 & "' group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by 2 desc"
                                    end if
                                    session("errorSQL") = sql
                                    set topProds = oConn.execute(sql)
                                    
                                    if topProds.EOF then
                                        myBrand = 0
                                        myModel = 0
                                        sql = "select top 10 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 1 and b.orderdatetime > '" & date-30 & "' group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by 2 desc"
                                        session("errorSQL") = sql
                                        set topProds = oConn.execute(sql)
                                    end if
                                    %>
                                    <div style="font-weight:bold; color:#1372c6; font-size:16px; padding:10px 0px 5px 0px; float:left; width:750px;">
                                        <% if myBrand = 0 and myModel = 0 then %>Top Selling Products<% else %>Recommended Products<% end if %>
                                    </div>
                                    <div style="float:left; width:750px; margin-bottom:10px; margin-left:45px; margin-right:auto;">
                                        <%
                                        prodLap = 0
                                        totalProdCnt = 0
                                        do while not topProds.EOF
                                            prodLap = prodLap + 1
                                            totalProdCnt = totalProdCnt + 1
                                            itemID = prepInt(topProds("itemID"))
                                            itemDesc_CO = prepStr(topProds("itemDesc_CO"))
                                            price_CO = prepInt(topProds("price_CO"))
                                            itemPic_CO = topProds("itemPic_CO")
                                            if len(itemDesc_CO) > 50 then showName = left(itemDesc_CO,50) & "..." else showName = itemDesc_CO
                                        %>
                                        <div title="<%=itemDesc_CO%>" style="float:left; width:135px; height:180px; padding:10px 5px 0px 5px; <% if prodLap < 5 then %>border-right:1px dotted #999; <% end if %>border-bottom:1px solid #999;">
                                            <div style="float:left; width:130px; text-align:center;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html"><img src="/productpics/thumb/<%=itemPic_CO%>" border="0" /></a></div>
                                            <div style="float:left; width:130px; text-align:left; height:50px; padding-top:5px;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html" style="color:#000;"><%=showName%></a></div>
                                            <div style="float:left; width:130px; text-align:left; padding-top:10px;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html" style="color:#1372c6; font-weight:bold;"><%=formatCurrency(price_CO,2)%></a></div>
                                        </div>
                                        <%
                                            topProds.movenext
                                            if prodLap = 5 then prodLap = 0
                                            if totalProdCnt = 10 then exit do
                                        loop
                                        %>
                                    </div>
								</td>
                            </tr>
                            <tr>
								<td width="100%" align="center" style="padding-bottom:30px;">
									<%
									sql	=	"select	top 3 b_articleID, b_categoryid, b_tagid, b_userID, b_parentID" & vbcrlf & _
											"	,	b_articleTitle, convert(varchar(8000), b_articleContent) b_articleContent" & vbcrlf & _
											"	,	convert(varchar(8000), dbo.[fn_stripHTML](convert(varchar(8000), b_articleContent))) strip_articleContent" & vbcrlf & _
											"	, 	b_articleStatus, b_firstPostTime" & vbcrlf & _
											"	,	b_lastEditTime" & vbcrlf & _
											"from	b_Articles" & vbcrlf & _
											"where	store = 2" & vbcrlf & _
											"	and	(b_articleStatus = 1 or b_articleStatus = 3)" & vbcrlf & _
											"order by b_lastEditTime desc" & vbcrlf
'									response.write "<pre>" & sql & "</pre>"
									session("errorSQL") = sql
									set objRsNewsTicker = oConn.execute(sql)
									%>
									<div style="width:775px; height:35px; vertical-align:middle; background-color:#eeeff4; margin-bottom:2px;">
										<div style="float:left; background-color:#eeeff4; font-weight:bold; font-size:18px; padding:6px 0px 0px 30px;">
											News Ticker
										</div>
										<div style="float:left; background-color:#eeeff4; padding:10px 0px 0px 10px;">
											<img src="/images/mainbottom/blog_dot_active.jpg" border="0">
											<img src="/images/mainbottom/blog_dot_inactive.jpg" border="0">
											<img src="/images/mainbottom/blog_dot_inactive.jpg" border="0">
											<img src="/images/mainbottom/blog_dot_inactive.jpg" border="0">
											<img src="/images/mainbottom/blog_dot_inactive.jpg" border="0">
										</div>
									</div>
									<%
									nRowCnt = 0
									if not objRsNewsTicker.eof then
										response.write "<div style=""width:775px;"">"
										do until objRsNewsTicker.eof
											strContent = ""
											nRowCnt = nRowCnt + 1
											if (nRowCnt mod 4) = 0 then
												response.write "</div><div style=""width:775px; display:none;"">"
											end if
											%>
											<div style="width:100%; background-color:#eeeff4; margin-bottom:2px;" align="center">
												<div style="width:95%; background-color:#eeeff4; color:#2d5183; font-size:20px; padding-top:10px" align="left"><%=objRsNewsTicker("b_articleTitle")%>
												</div>
												<div style="width:95%; background-color:#eeeff4; font-size:12px; padding:5px 0px 5px 0px; text-align:justify;">
											<%
'												strContent = stripHTML(objRsNewsTicker("b_articleContent"))
												strContent = objRsNewsTicker("strip_articleContent")
												strContent = replace(strContent, "?", "'")
												if len(strContent) > 470 then 
													strContent = left(strContent, 470) & "..." 
												end if
												strContent = strContent & " &nbsp; &nbsp; <a style=""color:#4da5d3;"" target=""_blank"" href=""http://www.cellularoutfitter.com/blog/article-" & objRsNewsTicker("b_articleID") & "-" & formatSEO(objRsNewsTicker("b_articleTitle")) & ".html"">Read More</a>"
												
												response.write strContent
											%>
												</div>
											</div>	
											<%
											objRsNewsTicker.movenext
										loop
										response.write "</div>"
									end if	
									objRsNewsTicker = null
									%>
								</td>
							</tr>
                            <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
							<tr>
                                <td align="center" style="padding:10px;">
									<div style="width:100%" >
										<div style="float: left;width:28%;padding-top: 7px;"><a href="/privacypolicy.html" target="_blank"><div id="truste"></div></a></div>
										<div style="float: left;width:17%;padding-top: 7px;"><a href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=100097908&source=ctc" target="_blank"><div id="bbb"></div></a></div>
										<div style="float: left;width:17%"><script type="text/javascript" src="/includes/js/j.js"></script>
                                    <script type="text/javascript">showMark(3);</script>
                                    <noscript><div id="googleChkOutImg"></div></noscript>
                                    <!-- END GOOGLE CHECKOUT CODE --></div>
										<div style="float: left;width:17%"><div id="inc5000"></div></div>
										<div style="float: left;width:17%"><a href="http://www.shopwiki.com/store-252661" target="_blank" title="CellularOutfitter.com is a ShopWiki Approved Store"><div id="shopWiki"></div></a></div>
									</div><% if false then %>
                                    <p align="center"></p>
                                    <p align="center"><a href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=100097908&source=ctc" target="_blank"><div id="bbb"></div></a></p>
                                    <!-- START GOOGLE CHECKOUT CODE -->
                                    
                                    <p align="center"><div id="inc5000"></div></p>
                                    <p align="center"><a href="http://www.shopwiki.com/store-252661" target="_blank" title="CellularOutfitter.com is a ShopWiki Approved Store"><div id="shopWiki"></div></a></p><% end if %>
                                </td>
                            </tr>