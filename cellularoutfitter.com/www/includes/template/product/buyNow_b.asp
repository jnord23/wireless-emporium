<%
useTime = now
totalSeconds = datediff("s",useTime,date+1)
totalMinutes = totalSeconds/60
if cint(totalMinutes) > totalMinutes then totalMinutes = cint(totalMinutes)-1 else totalMinutes = cint(totalMinutes)
totalSeconds = totalSeconds - (totalMinutes*60)
totalHours = totalMinutes/60
if cint(totalHours) > totalHours then totalHours = cint(totalHours)-1 else totalHours = cint(totalHours)
totalMinutes = totalMinutes - (totalHours*60)

if len(totalHours) < 2 then totalHours = "0" & totalHours
if len(totalMinutes) < 2 then totalMinutes = "0" & totalMinutes
if len(totalSeconds) < 2 then totalSeconds = "0" & totalSeconds
%>
<div class="tb buyNowDetailRow">
	<div class="fl itemBuyNowTitle" style="padding-top:7px;">Retail Price:</div>
    <div class="fl itemRetailValue"><%=formatCurrency(price_retail,2)%></div>
</div>
<div class="tb buyNowDetailRow sitewideSale">
	<div class="fl itemBuyNowTitle2">Last Day Sitewide Sale:</div>
    <div class="fl"><%=formatCurrency(price_co,2)%></div>
</div>
<div class="tb buyNowDetailRow">
	<div class="fl itemBuyNowTitle">You Save:</div>
    <div class="fl itemSaveValue"><%=formatCurrency(price_retail - price_CO,2)%> (<%=formatPercent((price_retail - price_CO) / price_retail,0)%>)</div>
</div>
<div id="addItemForm1">
	<% if (prodQty < 1 and not alwaysInStock) or (price_CO = 0 or isnull(price_CO)) then %>
    <div class="tb" style="font-weight:bold; color:#F00; padding-bottom:10px;">Temporarily Out of Stock</div>
    <span itemprop="availability" content="out_of_stock"></span>
    <% else %>
    <div class="tb buyNowDetailRow">
        <div class="tb">
            <div class="fl itemBuyNowTitle" style="padding-top:3px;">Deal Expires In:</div>
            <div id="saleHours" class="fl saleCountdown"><%=totalHours%></div>
            <div id="saleMinutes" class="fl saleCountdown"><%=totalMinutes%></div>
            <div id="saleSeconds" class="fl saleCountdown"><%=totalSeconds%></div>
        </div>
        <div class="tb countdownTitles">
            <div class="fl saleCountdownTitle">HRS</div>
            <div class="fl saleCountdownTitle">MIN</div>
            <div class="fl saleCountdownTitle">SEC</div>
        </div>
    </div>
    <form name="popSubmit" action="/cart/item_add.asp" method="post" style="margin:0px; padding:0px;">
    <div class="tb buyNowDetailRow">
        <div class="fl itemBuyNowQty">Quantity:</div>
        <div id="qtyBox1" class="fl qtyBox">
            <input name="qty" type="text" value="1" class="qtyField" />
            <input type="hidden" name="prodid" value="<%=itemID%>" />
            <input type="hidden" name="includePromo" value="1" />
            <input type="hidden" name="promoItemID" value="259959" />
        </div>
        <div class="fl" style="cursor:pointer;"><img src="/images/buttons/addToCart_greyGreen.gif" border="0" width="275" height="50" onclick="strandsAddToCart('popSubmit'); br_cartTracking('add', '<%=itemid%>', '<%=productColor%>', '<%=replace(itemDesc_co, "'", "\'")%>', '<%=price_co%>');" /></div>
    </div>
    <span itemprop="availability" content="in_stock"></span>
    <% end if %>
    <div style="cursor:pointer; margin:0px auto 10px auto; width:375px;" onclick="checkPromo('yes')">
        <img id="penPromoSmall" src="/images/banners/penPromoSmall.jpg" border="0" />
    </div>
    <% if typeID = 3 and addsp_itemID > 0 then %>
    <div style="border-bottom:1px dotted #CCC;height:1px;"></div>
    <div id="AddScreenProtection" style="background:#EFEFEF;border:#D6D6D6 1px solid; padding:10px;color:#666666;margin-top:4px;margin-bottom:4px;">
        <!--
        <%=addsp_itemID%>,<%=addsp_itemDesc_CO%>,<%=addsp_itemPic_CO%>,<%=addsp_price_co%>
        -->
        <% addspHeight = "60px" %>
        <div id="aspTitle" style="text-transform:uppercase;font-weight:bold;font-size:12px;text-decoration:underline;margin-bottom:10px;">Add Screen Protection</div>
        <div id="aspContainer" style="position:relative;">
            <div id="aspchkBox" style="height=<%=addspHeight%>;float:left;padding-top:15px;"><input type="hidden" name="addsp_prodid" value="<%=addsp_itemID%>" /><input type="hidden" name="addsp_price" value="<%=addsp_price_co%>" /><input type="checkbox" name="addsp_ChkBx" value="addsp_Checked" /></div>
            <div id="aspThumb" style="height=<%=addspHeight%>;float:left"><img src="/productPics/icon/<%=addsp_itemPic_CO%>" style="border:#000 1px solid;margin-right:10px;margin-left:10px;"></div>
            <div id="aspDesc" style="height=<%=addspHeight%>;float:left;font-size:12px;"><%=prepStr(addsp_itemDesc_CO)%>
                <div id="aspPrice" style="font-weight:bold;font-size:12px;"><%=formatCurrency(addsp_price_co,2)%></div>
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>
    <% else %>
    <!-- Add Screen Protector: <%=typeID%>,<%=addsp_itemID%> -->
    <% end if %>
    </form>
</div>
<!-- Alt promo item start -->
<div id="addItemForm2" style="display:none;">
	<% if (prodQty < 1 and not alwaysInStock) or (price_CO = 0 or isnull(price_CO)) then %>
    <div class="tb" style="font-weight:bold; color:#F00; padding-bottom:10px;">Temporarily Out of Stock</div>
    <span itemprop="availability" content="out_of_stock"></span>
    <% else %>
    <div class="tb buyNowDetailRow">
        <div class="tb">
            <div class="fl itemBuyNowTitle" style="padding-top:3px;">Deal Expires In:</div>
            <div id="saleHours" class="fl saleCountdown"><%=totalHours%></div>
            <div id="saleMinutes" class="fl saleCountdown"><%=totalMinutes%></div>
            <div id="saleSeconds" class="fl saleCountdown"><%=totalSeconds%></div>
        </div>
        <div class="tb countdownTitles">
            <div class="fl saleCountdownTitle">HRS</div>
            <div class="fl saleCountdownTitle">MIN</div>
            <div class="fl saleCountdownTitle">SEC</div>
        </div>
    </div>
    <form name="popSubmit2" action="/cart/item_add.asp" method="post" style="margin:0px; padding:0px;">
    <div class="tb buyNowDetailRow">
        <div class="fl itemBuyNowQty">Quantity:</div>
        <div id="qtyBox1" class="fl qtyBox">
            <input name="qty" type="text" value="1" class="qtyField" />
            <input type="hidden" name="prodid" value="<%=itemID%>" />
            <input type="hidden" name="includePromo" value="1" />
            <input type="hidden" name="promoItemID" value="321981" />
        </div>
        <div class="fl" style="cursor:pointer;"><img src="/images/buttons/addToCart_greyGreen.gif" border="0" width="275" height="50" onclick="strandsAddToCart('popSubmit2'); br_cartTracking('add', '<%=itemid%>', '<%=productColor%>', '<%=replace(itemDesc_co, "'", "\'")%>', '<%=price_co%>');" /></div>
    </div>
    <span itemprop="availability" content="in_stock"></span>
    <% end if %>
    <div style="cursor:pointer; margin:0px auto 10px auto; width:375px;" onclick="checkPromo('yes')">
        <img id="stickyPromoSmall" src="/images/banners/stickyPromoSmall.jpg" border="0" />
    </div>
    <% if typeID = 3 and addsp_itemID > 0 then %>
    <div style="border-bottom:1px dotted #CCC;height:1px;"></div>
    <div id="AddScreenProtection" style="background:#EFEFEF;border:#D6D6D6 1px solid; padding:10px;color:#666666;margin-top:4px;margin-bottom:4px;">
        <!--
        <%=addsp_itemID%>,<%=addsp_itemDesc_CO%>,<%=addsp_itemPic_CO%>,<%=addsp_price_co%>
        -->
        <% addspHeight = "60px" %>
        <div id="aspTitle" style="text-transform:uppercase;font-weight:bold;font-size:12px;text-decoration:underline;margin-bottom:10px;">Add Screen Protection</div>
        <div id="aspContainer" style="position:relative;">
            <div id="aspchkBox" style="height=<%=addspHeight%>;float:left;padding-top:15px;"><input type="hidden" name="addsp_prodid" value="<%=addsp_itemID%>" /><input type="hidden" name="addsp_price" value="<%=addsp_price_co%>" /><input type="checkbox" name="addsp_ChkBx" value="addsp_Checked" /></div>
            <div id="aspThumb" style="height=<%=addspHeight%>;float:left"><img src="/productPics/icon/<%=addsp_itemPic_CO%>" style="border:#000 1px solid;margin-right:10px;margin-left:10px;"></div>
            <div id="aspDesc" style="height=<%=addspHeight%>;float:left;font-size:12px;"><%=prepStr(addsp_itemDesc_CO)%>
                <div id="aspPrice" style="font-weight:bold;font-size:12px;"><%=formatCurrency(addsp_price_co,2)%></div>
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>
    <% else %>
    <!-- Add Screen Protector: <%=typeID%>,<%=addsp_itemID%> -->
    <% end if %>
    </form>
</div>
<!-- Alt promo item end -->
<div class="tb" style="border-top:1px dotted #CCC; padding:10px 0px 10px 0px; width:420px;">
	<% if useUvp <> "" then %>
    <div class="tb productCheckRow">
        <div style="float:left; width:17px;"><img src="/images/icons/check.png" border="0" width="17" height="14" /></div>
        <div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#333;"><%=useUvp%></div>
    </div>
    <% end if %>
    <div class="tb productCheckRow">
	    <div style="float:left; width:17px;"><img src="/images/icons/check.png" border="0" width="17" height="14" /></div>
    	<div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#333;">Leaves Our US Warehouse Within 24 Hours</div>
    </div>
    <div class="tb productCheckRow">
	    <div style="float:left; width:17px;"><img src="/images/icons/check.png" border="0" width="17" height="14" /></div>
	    <div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#333;">Lowest Prices Online Guaranteed</div>
    </div>
    <div class="tb productCheckRow">
	    <div style="float:left; width:17px;"><img src="/images/icons/check.png" border="0" width="17" height="14" /></div>
    	<div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#333;">
            <% if isUnlockedPhone = 1 then %>
            	Ready to use with any new or existing service plan
            <% else %>
	            Unconditional 90 Day Return Policy
    	        <% if customize then %>
        	    &nbsp;*<br /><span style="color:#999; font-size:10px;">* except for customized items</span>
            	<% end if %>
            <% end if %>
        </div>
    </div>
</div>
<div class="tb paymentOptions"></div>