<!--#include virtual="/includes/template/bottomHTML.asp"-->
<%
call CloseConn(oConn)
%>
<script language="javascript">
	var useHttps = <%=prepInt(useHttps)%>;
	
	function nreset() {
		document.newsletter.semail.value="";
	}
	function nvalidateEmail(email) {
		var splitted = email.match("^(.+)@(.+)$");
		if (splitted == null) return false;
		if (splitted[1] != null) {
			var regexp_user=/^\"?[\w-_\.]*\"?$/;
			if (splitted[1].match(regexp_user) == null) return false;
		}
		if (splitted[2] != null) {
			var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
			if (splitted[2].match(regexp_domain) == null) {
				var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
				if (splitted[2].match(regexp_ip) == null) return false;
			}
			return true;
		}
		return false;
	}
	function nvalidate() {
		if (document.newsletter.semail.value.length==0) {
			alert("please enter your email address");
			document.newsletter.semail.focus();
			return false;
		}
		if (!nvalidateEmail(document.newsletter.semail.value)) {
			alert("please enter your valid email address"); 
			document.newsletter.semail.focus();
			return false; 
		}
		else document.newsletter.submit()
		return true;
	}
</script>
<script language="javascript" src="/includes/js/sslCheck.js"></script>