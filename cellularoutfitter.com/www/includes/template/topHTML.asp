<%
if prepStr(request.QueryString("utm_promocode")) <> "" then
	session("promocode") = prepStr(request.QueryString("utm_promocode"))
	response.Cookies("promocode") = prepStr(request.QueryString("utm_promocode"))
end if
mySession = prepInt(request.cookies("mySession"))

if prepStr(session("promocode")) <> "" then usePromoCode = session("promocode")
if prepStr(request.Cookies("promocode")) <> "" then usePromoCode = request.Cookies("promocode")

sql = 	"select isnull(sum(a.qty), 0) totalQty, isnull(sum(b.price_co * a.qty), 0) totalPrice1, isnull(sum(c.price_co * a.qty), 0) totalPrice2 " & vbcrlf & _
		"from shoppingcart a left join we_items b on a.itemid=b.itemid left join we_items_musicSkins c on a.itemID = c.id " & vbcrlf & _
		"where a.store = 2 and a.sessionid = '" & mySession & "' and (a.purchasedorderid is null or a.purchasedorderid = 0)" & vbcrlf

call fOpenConn()
set objRsMiniCart = oConn.execute(sql)

if objRsMiniCart.EOF then
	miniTotalQuantity = 0
	miniSubTotal = 0
else
	miniTotalQuantity = formatnumber(objRsMiniCart("totalQty"), 0)
	miniSubTotal = formatcurrency(objRsMiniCart("totalPrice1") + objRsMiniCart("totalPrice2"))
end if

dim brandList : brandList = ""
sql = 	"select	brandid, brandName, 0 phoneOnly" & vbcrlf & _
		"from	we_brands" & vbcrlf & _
		"where	brandid in (1,2,3,4,5,6,7,9,10,11,14,16,17,18,19,20,28,29,30)" & vbcrlf & _
		"order by brandname"
set rsBrandFinder = oConn.execute(sql)
do until rsBrandFinder.eof
	brandList = brandList & rsBrandFinder("brandID") & "##" & rsBrandFinder("brandName") & "##"
	rsBrandFinder.movenext
loop
brandList = left(brandList,len(brandList)-2)
brandListArray = split(brandList,"##")

dim modelList : modelList = ""
if prepInt(brandID) > 0 then
	sql = 	"select	modelid, modelName" & vbcrlf & _
			"from	we_models" & vbcrlf & _
			"where	brandid = " & brandID & " " & vbcrlf & _
			"order by modelName"
	set rsModelFinder = oConn.execute(sql)
	do until rsModelFinder.eof
		modelList = modelList & rsModelFinder("modelid") & "##" & rsModelFinder("modelName") & "##"
		rsModelFinder.movenext
	loop
	modelList = left(modelList,len(modelList)-2)
end if
modelListArray = split(modelList,"##")

dim categoryList : categoryList = ""
if prepInt(modelID) > 0 then
	sql = 	"select distinct a.typeID, b.typeName_co " & vbcrlf & _
			"from we_ItemsSiteReady a " & vbcrlf & _
				"left join we_Types b on a.typeID = b.typeID " & vbcrlf & _
			"where modelID = " & modelID & " " & vbcrlf & _
			"order by b.typeName_co"
	set rsCatFinder = oConn.execute(sql)
	do until rsCatFinder.eof
		categoryList = categoryList & rsCatFinder("typeID") & "##" & rsCatFinder("typeName_co") & "##"
		rsCatFinder.movenext
	loop
	categoryList = left(categoryList,len(categoryList)-2)
end if
categoryListArray = split(categoryList,"##")
%>
<div id="mbox-global"></div>
<div id="mbox-page"></div>
<script>
	function onFinder(finderType, o) {
		if (finderType == "b") {
			ajax('/ajax/accessoryFinder.asp?brandid=' + o.value, 'cbModelBox');
			ajax('/ajax/accessoryFinder.asp?modelid=-1', 'cbCatBox');
		}
		else if (finderType == "m") {
			ajax('/ajax/accessoryFinder.asp?modelid=' + o.value, 'cbCatBox');			
		} 
	}
	
	function onFinder2(finderType, o) {
		if (finderType == "b") {
			ajax('/ajax/accessoryFinder.asp?version=b&brandid=' + o.value, 'cbModelBox2');
			ajax('/ajax/accessoryFinder.asp?version=b&modelid=-1', 'cbCatBox2');
		}
		else if (finderType == "m") {
			ajax('/ajax/accessoryFinder.asp?version=b&modelid=' + o.value, 'cbCatBox2');			
		} 
	}
	
	function jumpTo() {
		var brandid = document.frmFinder.cbBrand.value;
		var modelid = document.frmFinder.cbModel.value;
		var categoryid = document.frmFinder.cbCat.value;
		var brandName = ""
		var modelName = ""
		var categoryName = ""
		var strLink = ""
		
		if (brandid != "" && modelid != "" && categoryid != "") {
			brandName = document.frmFinder.cbBrand.options[document.frmFinder.cbBrand.selectedIndex].text;
			modelName = document.frmFinder.cbModel.options[document.frmFinder.cbModel.selectedIndex].text;
			categoryName = document.frmFinder.cbCat.options[document.frmFinder.cbCat.selectedIndex].text;
			strLink = "/sb-" + brandid + "-sm-" + modelid + "-sc-" + categoryid + "-" + brandName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + modelName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + categoryName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + ".html"
			window.location = strLink;
		}
		else if (brandid != "" && modelid != "") {
			brandName = document.frmFinder.cbBrand.options[document.frmFinder.cbBrand.selectedIndex].text;
			modelName = document.frmFinder.cbModel.options[document.frmFinder.cbModel.selectedIndex].text;
			strLink = "/m-" + modelid + "-" + brandName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + modelName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-cell-phone-accessories.html"
			window.location = strLink;
		}
	}
	
	function jumpTo2() {
		var brandid = document.frmFinder2.cbBrand.value;
		var modelid = document.frmFinder2.cbModel.value;
		var categoryid = document.frmFinder2.cbCat.value;
		var brandName = ""
		var modelName = ""
		var categoryName = ""
		var strLink = ""
		
		if (brandid != "" && modelid != "" && categoryid != "") {
			brandName = document.frmFinder2.cbBrand.options[document.frmFinder2.cbBrand.selectedIndex].text;
			modelName = document.frmFinder2.cbModel.options[document.frmFinder2.cbModel.selectedIndex].text;
			categoryName = document.frmFinder2.cbCat.options[document.frmFinder2.cbCat.selectedIndex].text;
			strLink = "/sb-" + brandid + "-sm-" + modelid + "-sc-" + categoryid + "-" + brandName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + modelName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + categoryName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + ".html"
			window.location = strLink;
		}
		else if (brandid != "" && modelid != "") {
			brandName = document.frmFinder2.cbBrand.options[document.frmFinder2.cbBrand.selectedIndex].text;
			modelName = document.frmFinder2.cbModel.options[document.frmFinder2.cbModel.selectedIndex].text;
			strLink = "/m-" + modelid + "-" + brandName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + modelName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-cell-phone-accessories.html"
			window.location = strLink;
		}
	}
	
	function af_Select(which,useID) {
		if (which == "b") {
			ajax('/ajax/af.asp?brandID=' + useID,'af_model');
			ajax('/ajax/af.asp?catReset=1','af_category');
		}
		if (which == "m") { ajax('/ajax/af.asp?modelID=' + useID,'af_category'); }
	}
	
	var af_limit = 0;
	function af_submit() {
		var brandID = document.af_form.brandID.value;
		var modelID = document.af_form.modelID.value;
		var categoryID = document.af_form.categoryID.value;
		
		if (brandID == "") {
			alert("You must select at least a brand first");
		}
		else {
			document.getElementById("dumpZone").innerHTML = "";
			ajax('/ajax/af.asp?subLinkRequest=1&brandID=' + brandID + '&modelID=' + modelID + '&categoryID=' + categoryID,'dumpZone');
			setTimeout("af_linkCheck()",200);
		}
	}
	
	function af_Select2(which,useID) {
		if (which == "b") {
			ajax('/ajax/af.asp?largeField=1&brandID=' + useID,'af_model2');
			ajax('/ajax/af.asp?largeField=1&catReset=1','af_category2');
		}
		if (which == "m") { ajax('/ajax/af.asp?largeField=1&modelID=' + useID,'af_category2'); }
	}
	
	function af_submit2() {
		var brandID = document.af_form2.brandID.value;
		var modelID = document.af_form2.modelID.value;
		var categoryID = document.af_form2.categoryID.value;
		
		if (brandID == "") {
			alert("You must select at least a brand first");
		}
		else {
			document.getElementById("dumpZone").innerHTML = "";
			ajax('/ajax/af.asp?subLinkRequest=1&brandID=' + brandID + '&modelID=' + modelID + '&categoryID=' + categoryID,'dumpZone');
			setTimeout("af_linkCheck()",200);
		}
	}
	
	function af_linkCheck() {
		if (document.getElementById("dumpZone").innerHTML != "") {
			window.location = document.getElementById("dumpZone").innerHTML;
		}
		else {
			af_limit++;
			if (af_limit < 5) { setTimeout("af_linkCheck()",200); }
		}
	}
	
	function emailSubmitFieldChk(dir) {
		if (dir == "f") {
			if (document.es_form.semail.value == "Enter email address") {
				document.es_form.semail.value = "";
			}
		}
		else {
			if (document.es_form.semail.value == "") {
				document.es_form.semail.value = "Enter email address";
			}
		}
	}
	
	function emailSubmitFieldChk2(dir) {
		if (dir == "f") {
			if (document.es_form2.semail.value == "Enter your email address and save!") {
				document.es_form2.semail.value = "";
			}
		}
		else {
			if (document.es_form2.semail.value == "") {
				document.es_form2.semail.value = "Enter your email address and save!";
			}
		}
	}
</script>
<!-- promocode: <%=usePromoCode%> -->
<link rel="stylesheet" type="text/css" href="/includes/css/account_loginMenu.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/sitewide/bVersion.css" />
<% if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then %>Server IP: <%=request.ServerVariables("LOCAL_ADDR")%><% end if %>
<link href='//fonts.googleapis.com/css?family=Open+Sans:600,400|Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
<div id="popCover" onclick="closeReviewPopup()" style="height:100%; display:none; width:100%; background-color:#333; position:fixed; left:0px; top:0px; z-index:3000; opacity:0.6; filter:alpha(opacity=60);">&nbsp;</div>
<div id="popBox" style="display:none; position:fixed; left:20%; right:20%; top:0px; z-index:3001; margin:auto; text-align:center;"></div>
<div id="topSelect"></div>
<div id="siteTopBar" style="width:100%; height:28px; background-image:url(/images/backgrounds/top-bar-dblue.png);">
	<div style="width:1040px; height:28px; margin-left:auto; margin-right:auto;">
        <div style="float:right; background-image:url(/images/backgrounds/top-bar-green.png); padding:0px 20px 0px 20px; height:28px;">
            <a href="/cart/basket.asp">
            <div style="float:left" id="cartIcon"></div>
            <div id="siteTop-CartBar" style="float:left; color:#FFF; font-weight:bold; font-size:10px; padding:7px 10px 0px 10px;"><%=miniTotalQuantity%> Items - <%=formatCurrency(miniSubTotal,2)%></div>
            <div style="float:left"><div id="cardDownArrow"></div></div>
            </a>
        </div>
        <div id="account_nav"></div>
        <div style="float:right;"><div id="topMenuBr"></div></div>
        <div style="float:right; font-weight:bold; padding:7px 20px 0px 20px;"><a href="/contact-us.html" style="color:#FFF; font-size:10px;">Contact Us</a></div>
        <div style="float:right;"><div id="topMenuBr"></div></div>
        <div style="float:right; font-weight:bold; padding:7px 20px 0px 20px;"><a href="/faq.html" style="color:#FFF; font-size:10px;">Help</a></div>
        <div style="float:right;"><div id="topMenuBr"></div></div>
        <div style="float:right; font-weight:bold; padding:7px 20px 0px 20px;"><a href="/track-your-order.html" style="color:#FFF; font-size:10px;">Order Status</a></div>
		<!--
        <div style="float:right;"><div id="topMenuBr"></div></div>
        <div style="float:right; height:28px; overflow:hidden;">
            <script type="text/javascript">
                var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                var lhnid = 3410;
                var lhnwindow = 6755;
                var lhnImage = "<div id='lhnchatimg' title='Live Help'></div>";
                document.write(unescape("%3Cscript src='/framework/utility/liveHelp.js' type='text/javascript'%3E%3C/script%3E"));
            </script>
        </div>
        -->
	</div>
</div>
<div id="floatingTab_email" class="emailFloatingTab" style="bottom:-75px; display:none;">
	<div class="tb floatingTabText">
    	<div class="fl">Get <span style="color:#f1c40f; font-weight:bold; font-size:15px;">25% OFF</span> today's order, instantly.</div>
        <div class="fr minEmailOffer" onclick="showEmailOfferTab()"><div id="floatingTabValue" class="fr minEmailBar">+</div></div>
    </div>
    <div class="tb floatingEmailBox">
    	<form name="es_form2" method="post" onsubmit="return(addNewsletter(document.es_form2.semail.value,'Popup Widget'));" style="margin:0px; padding:0px;">
        <div class="fl">
	        <div class="fl floatingEmailFieldBox">
    	        <input type="text" name="semail" value="Enter your email address and save!" class="esField2" onfocus="emailSubmitFieldChk2('f')" onblur="emailSubmitFieldChk2('b')" />
        	</div>
	        <div class="fl floatingEmailSubmitBttn" onclick="addNewsletter(document.es_form2.semail.value,'Popup Widget');">SUBMIT</div>
        </div>
        </form>
    </div>
    <div id="emailDumpZone" style="display:none;"></div>
</div>
<table width="1020" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF" style="border-left:1px solid #ccc; border-right:1px solid #ccc;">
    <tr>
        <td id="siteTopBar2" valign="top" style="padding:5px 10px 0px 10px;">
            <div id="logoLine_a" style="float:left; width:1020px;">
            	<div style="float:left; width:250px; height:60px;">
                	<%
					if (month(date) = 11 and day(date) > 22) or (month(date) = 12 and day(date) < 26) then
					%>
                    <a href="/"><img src="/images/holidayLogos/co-xmas.png" alt="CellularOutfitter.com" title="CellularOutfitter.com" border="0" /></a>
                    <%
					elseif month(date) = 12 or (month(date) = 1 and day(date) = 1) then
					%>
                    <a href="/"><img src="/images/holidayLogos/co-new-years-logo.gif" alt="CellularOutfitter.com" title="CellularOutfitter.com" border="0" /></a>
                    <%
					else
					%>
                    <a href="/"><div id="logo" title="CellularOutfitter.com"></div></a>
                    <%
					end if
					%>
					
                </div>
                <div style="float:right; width:750px; padding-top:5px;">
                	<div style="float:left; width:750px;">
                    	<div id="mvt_newTopHeader1" style="margin-left:10px; float:right;">
                            <div style="float:right; padding-left:5px;">
                                <a href="javascript:Open_Popup('https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.CELLULAROUTFITTER.COM&lang=en','VeriSign','width=560,height=500,location=yes,status=yes,resizable=yes,scrollbars=yes');"><div id="verisignLogo" title="CELLULAR OUTFITTER, INC. has been verified by Verisign"></div></a>
                            </div>
                        	<a href="/shipping-policy.html"><div id="topHeaderUVP1_2" style="border-right:2px dotted #ccc; padding-right:5px; margin-right:5px;"></div></a>
                        	<a href="/lowest-price.html"><div id="topHeaderUVP1_1" style="border-right:2px dotted #ccc; padding-right:5px; margin-right:5px;"></div></a>
                        </div>
                        <div class="fr topSearchMainBox">
                        	<form name="topSearchForm" method="get" action="/widgets/nxtSearch/search.asp" style="margin:0px; padding:0px;">
                            <div class="fl topSearchBox">
                            	<div class="fl topSearchMag"></div>
                                <div class="fl topSearchFieldBox">
	                                <input type="text" name="search" value="What are you looking for?" style="border: 0px; background-color: transparent; color: #999; width: 180px; font-family: 'Open Sans', sans-serif; font-weight: 600;" class="searchInputBig" onfocus="this.value='';" />
    	                            <input type="hidden" name="sitesearch" value="Y2" />
                                </div>
                                <div class="fr topSearchSubmit" onclick="document.topSearchForm.submit()">Go!</div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding:5px 10px 0px 10px;">
        	<div id="topNav">
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:80px;">
					<a href="/c-3-cell-phone-covers-and-skins.html" title="Cell Phone Covers & Gel Skins" class="cat-font2">Covers & Gel Skins</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:90px;">
					<a href="/c-7-cell-phone-cases-pouches.html" title="Cell Phone Cases & Pouches" class="cat-font2">Cases & Pouches</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:14px 5px 0px 5px; width:110px;">
					<a href="/c-18-cell-phone-screen-protectors.html" title="Cell Phone Screen Protectors" class="cat-font2">Screen Protectors</a>
                </div>
                <!--
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:80px;">
					<a href="/c-24-cell-phone-custom-cases.html" title="Cell Phone Custom Cases" class="cat-font2">Custom<br />Cases</a>
                </div>
                -->
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:100px;">
					<a href="/c-2-cell-phone-chargers-and-data-cables.html" title="Cell Phone Chargers & Data Cables" class="cat-font2">Chargers & Data Cables</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:14px 5px 0px 5px; width:90px;">
					<a href="/c-1-cell-phone-batteries.html" title="Cell Phone Batteries" class="cat-font2">Batteries</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:100px;">
					<a href="/c-5-cell-phone-hands-free-kits-bluetooth-headsets.html" title="Cell Phone Hands-Free Kits & Headsets" class="cat-font2">Bluetooth & Hands-Free</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:90px;">
					<a href="/c-6-cell-phone-holsters-car-mounts.html" title="Cell Phone Holsters & Car Mounts" class="cat-font2">Holsters & Car Mounts</a>
                </div>
                <div id="topNav-Div"></div>
				<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:100px;">
					<a href="/c-8-cell-phone-other-accessories.html" title="Cell Phone Other Accessories" class="cat-font2">Other Accessories</a>
                </div>
                <div id="topNav-Div"></div>
				<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:110px;">
					<a href="/wholesale-cell-phones.html" title="Wholesale Cell Phones" class="cat-font2">Wholesale Cell Phones</a>
                </div>
            </div>
            <div id="topAccessoryFinder_alt2">
            	<form name="af_form" method="post" onsubmit="return(false)" style="margin:0px; padding:0px;">
            	<div class="fl smallAccessoryFinderText">Find Accessories:</div>
                <div class="fl accessoryFinderField">
                	<select name="brandID" class="accessorySelect" onchange="af_Select('b',this.value)">
                    	<option value="">1. Phone Brand</option>
                        <optgroup label="Top Brands">
                            <option value="9">Samsung</option>
                            <option value="17">Apple</option>
                            <option value="4">LG</option>
                            <option value="30">ZTE</option>
                            <option value="5">Motorola</option>
                            <option value="7">Nokia</option>
                        </optgroup>
                        <optgroup label="Device Brands">
                        <%
						rsBrandFinder.movefirst
						do while not rsBrandFinder.EOF
						%>
                        <option value="<%=rsBrandFinder("brandID")%>"><%=rsBrandFinder("brandName")%></option>
                        <%
							rsBrandFinder.movenext
						loop
						%>
                        </optgroup>
                    </select>
                </div>
                <div id="af_model" class="fl accessoryFinderField">
                	<select name="modelID" class="accessorySelect">
                    	<option value="">2. Phone Model</option>
                        <option value="">Select Brand First</option>
                    </select>
                </div>
                <div id="af_category" class="fl accessoryFinderField">
                	<select name="categoryID" class="accessorySelect">
                    	<option value="">3. Category</option>
                        <option value="">Select Model First</option>
                    </select>
                </div>
                <div class="fl accessoryFinderSubmit" onclick="af_submit()"></div>
                </form>
                <div class="fl emailSubmitBox">
                	<div class="tb emailSubmitLrgTxt">Get <span style="color:#f1c40f; font-weight:bold;">25% OFF</span></div>
                    <div class="tb emailSubmitSmlTxt">TODAY'S ORDER, INSTANTLY.</div>
                </div>
                <form name="es_form" method="post" onsubmit="return(addNewsletter(document.es_form.semail.value,'Top Widget'));" style="margin:0px; padding:0px;">
                <div class="fl accessoryFinderField">
                	<input type="text" name="semail" value="Enter email address" class="esField" onfocus="emailSubmitFieldChk('f')" onblur="emailSubmitFieldChk('b')" />
                </div>
                <div class="fl emailSubmitBttn" onclick="addNewsletter(document.es_form.semail.value,'Top Widget');"></div>
                </form>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding:0px 10px 0px 10px;">
			<%if now >= cdate("11/29/2013") and now < cdate("12/2/2013") then %>
            <div><img src="/images/banners/blackFriday_CO_banner2.png" border="0" alt="Black Friday Deals" /></div>
			<%elseif now >= cdate("12/2/2013") and now < cdate("12/3/2013") then%>
            <div><img src="/images/banners/cyberMonday_CO_banner2.png" border="0" alt="Cyber Monday Deals" /></div>
			<%end if%>
        	<%
			if isnull(noLeftSide) or len(noLeftSide) < 1 or not isnumeric(noLeftSide) then noLeftSide = 0
			if noLeftSide = 0 then
			%>
            <div id="sideNav" style="float:left;">
            	<% if prepInt(useLeftNav2) = 1 then %>
                <!--#include virtual="/includes/template/leftnav2.asp"-->
				<% else %>
				<!--#include virtual="/includes/template/leftnav.asp"-->
                <% end if %>
            </div>
            <div id="mainContent" style="padding-left:15px; float:left; width:800px;">
            <% end if %>