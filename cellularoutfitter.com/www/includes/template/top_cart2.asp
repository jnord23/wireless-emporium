<%
'dim cart : cart = 1
curPage = request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL")
if instr(curPage,"216.139.235.93") > 0 then
	response.Status = "301 Moved Permanently"
	response.AddHeader "Location", "http://" & replace(curPage,"216.139.235.93","www.cellularOutfitter.com")
	response.end
end if

betaView = request.Cookies("betaView")

dim relatedItems(10), NumRecords, a, b
NumRecords = 0
for a = 1 to 10
	relatedItems(a) = 999999
next

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(formatSEO, "?", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "covers-screen-guards"
		case "bling-kits---charms" : formatSEO = "bling-kits-charms"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "hands-free-kits-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO = "holsters-car-mounts"
		case "leather-cases" : formatSEO = "cases-pouches"
		case "cell-phones" : formatSEO = "wholesale-cell-phone"
		case "full-body-protectors" : formatSEO = "invisible-film-protectors"
	end select
end function

function nameSEO(val)
	select case val
		case "Antennas/Parts" : nameSEO = "Antennas"
		case "Faceplates" : nameSEO = "Covers &amp; Screen Guards"
		case "Data Cable & Memory" : nameSEO = "Data Cables &amp; Memory Cards"
		case "Hands-Free" : nameSEO = "Hands-Free Kits &amp; Bluetooth Headsets"
		case "Holsters/Belt Clips" : nameSEO = "Holsters / Car Mounts"
		case "Leather Cases" : nameSEO = "Cases &amp; Pouches"
		case "Cell Phones" : nameSEO = "Wholesale Unlocked Cell Phones"
		case "Full Body Protectors" : nameSEO = "Invisible Film Protectors"
		case else : nameSEO = val
	end select
end function

function singularSEO(val)
	select case val
		case "Batteries" : singularSEO = "Battery"
		case "Chargers" : singularSEO = "Charger"
		case "Faceplates" : singularSEO = "Cover &amp; Screen Guard"
		case "Hands-Free" : singularSEO = "Hands Free / Bluetooth Headset"
		case "Holsters/Belt Clips" : singularSEO = "Holster / Car Mount"
		case "Leather Cases" : singularSEO = "Case / Pouch"
		case "Antennas/Parts" : singularSEO = "Antenna"
		case "Data Cable & Memory" : singularSEO = "Data Cable / Memory Card"
		case "Bling Kits & Charms" : singularSEO = "Bling Kit / Charm"
		case "Cell Phones" : singularSEO = "Wholesale Unlocked Cell Phone"
		case "Full Body Protectors" : singularSEO = "Invisible Film Protector"
		case else : singularSEO = val
	end select
end function

if SEtitle = "" then SEtitle = "Wholesale Cell Phone Accessories ?Wholesale Cellular Phone Accessories ?CellularOutfitter.com"
if SEdescription = "" then SEdescription = "Buy Cheap Cell Phone Accessories at Wholesale Prices to the Public. Find the latest Discount Cellular Phone Accessories for Motorola, LG, Nokia, Samsung phones and more. Cellular Accessories at the Lowest Prices Online Guaranteed."
if SEkeywords = "" then SEkeywords = "cheap cell phone accessories, cell phone accessories, discount cellular phone accessories, cellular phone accessories"
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title><%=SEtitle%></title>
    <!-- Google Website Optimizer Tracking Script -->
	<script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['gwo._setAccount', 'UA-1097464-2']);
      _gaq.push(['gwo._trackPageview', '/2828325546/test']);
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>
    <!-- End of Google Website Optimizer Tracking Script -->
    <meta name="Description" content="<%=SEdescription%>">
    <meta name="Keywords" content="<%=SEkeywords%>">
    <%if pageTitle = "Home" then%>
    <meta name="robots" content="index,follow">
    <!-- Cell Phone Accessories at CellularOutfitter.com: Motorola Cell Phone Accessories, Nokia Cell Phone Accessories, LG Cell Phone Accessories, Samsung Cell Phone Accessories -->
    <meta name="verify-v1" content="KDSemcIPOLKERRNLdtodRPug14ThOrk3fAAECAAzt/U=" />
    <meta name="verify-v1" content="7oJ6qXmQ/MOO9Wics9IknbH0gaHI9z5Tl3dH82NBhO8=" />
    <%end if%>
    <link href="/includes/css/styleCO.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <style>
        #Promotions {display:none;}
        
        body #google_amark_b, body #google_amark_b * { text-align:left !important; padding:0 !important; margin:0 !important; border:0 !important; position:relative !important; font-weight:normal !important; text-decoration:none !important; font-size:11px !important; font-family:Arial, sans-serif !important; background:#fff !important; float:none !important; }
        #google_amark_b .m img, #google_amark_b #t img, #google_amark_b #x a { display:block !important; }
        #google_amark_b .h { position:absolute !important; width:325px !important; border:1px solid #ccc !important; }
        #google_amark_b #t { padding:6px 0 1px 0px !important; }
        #google_amark_b #l { left:10px !important; }
        #google_amark_b #x { position:absolute !important; right:0px !important; top:0px !important; border-left:1px solid #ccc !important; border-bottom:1px solid #ccc !important; }
        #google_amark_b #c { padding:5px 10px 10px 10px !important; color:#676767 !important; border-top:1px solid #ccc !important;  }
        #google_amark_b #c p { padding:5px 0 0 0 !important; }
        #google_amark_b #c p.p { padding:0 !important; }
        #google_amark_b #c p.p img { top: 5px !important; }
        #google_amark_b #c p a:link, #google_amark_b #c p a:visited { color:#0000CC !important; text-decoration:underline !important; }
    </style>
    <script src="/cart/includes/validate_new.js"></script>
    <script language="javascript">
    function addbookmark() {
        var IE = navigator.appName.match(/(Microsoft Internet Explorer)/gi),
        NS = navigator.appName.match(/(Netscape)/gi),
        OP = navigator.appName.match(/(Opera)/gi),
        BK = document.getElementById('bookmark');
        BK.onmouseout = function() {
            window.status = '';
        }
        if(IE && document.uniqueID) {
            var bookmarkurl = "http://www.CellularOutfitter.com/";
            var bookmarktitle = "Cellular Outfitter";
            window.external.AddFavorite(bookmarkurl,bookmarktitle);
        }
        else if(OP || IE && !document.uniqueID) {
            alert('Your browser requires that you\nPress Ctrl & D to Bookmark this page.');
        }
        else if(NS) {
            alert('Your browser requires that you\nPress Ctrl & D to Bookmark this page.');
        }
        else { BK.innerHTML = '' }
    }
    function Open_Popup(theURL,winName,features) { //v2.0
        window.open(theURL,winName,features);
    }
    function showPromoInfo() {
        document.getElementById("Promotions").style.display = "block";
    }
    </script>
    <!--#include virtual="/includes/template/inc_HeadTagEnd.asp"-->
</head>
<%
if strBody = "" then strBody = "<body class=""body"">"
response.write strBody & vbcrlf
%>
<% call printPIxel(2) %>
<div style="position:relative;">
<table width="1020" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
	<tr>
		<td width="20" valign="top" class="sideleft_border"></td>
		<td width="980" valign="top" align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td width="100%" valign="top" align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td width="310">
                                	<%
									if month(date) = 12 and day(date) < 27 then
										holidayLogo = 1
									else
										holidayLogo = 0
									end if
									if holidayLogo = 0 then
									%>
                                    <a href="/"><img src="/images/coLogo2.gif" alt="CellularOutfitter.com" border="0"></a>
                                    <% else %>
                                    <a href="/"><img src="/images/Co_logo_xmas.jpg" alt="CellularOutfitter.com" width="350" height="90" border="0"></a>
                                    <% end if %>
                                </td>
								<td width="20">&nbsp;</td>
								<td width="540" align="right" valign="middle">&nbsp;</td>
								<td width="10">&nbsp;</td>
								<td width="90" align="center" valign="bottom">
									<!-- START SCANALERT CODE -->
									<a target="_blank" href="https://www.mcafeesecure.com/RatingVerify?ref=www.cellularoutfitter.com"><img width="65" height="37" border="0" src="//images.scanalert.com/meter/www.cellularoutfitter.com/63.gif" alt="McAfee Secure sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="alert('Copying Prohibited by Law - McAfee Secure is a Trademark of McAfee, Inc.'); return false;"></a>
									<!-- END SCANALERT CODE -->
									<br>
									<a href="javascript:Open_Popup('https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.CELLULAROUTFITTER.COM&lang=en','VeriSign','width=560,height=500,location=yes,status=yes,resizable=yes,scrollbars=yes');"><img src="/images/seal_co.jpg" width="60" height="30" border="0" alt="VeriSign Secured Site"></a>
								</td>
								<td width="10" align="center" valign="bottom">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="100%" valign="top" align="center">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
