<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<%
	if isnull(noLeftSide) or len(noLeftSide) < 1 then noLeftSide = 0
	curPage = request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL")
	if instr(curPage,"216.139.235.93") > 0 then
		response.Status = "301 Moved Permanently"
		response.AddHeader "Location", "http://" & replace(curPage,"216.139.235.93","www.cellularOutfitter.com")
		response.end
	end if
	
	saveEmail()

	on error resume next
	if request.querystring("id") <> "" then
		Response.Cookies("vendororder") = request.querystring("id")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
	elseif request.querystring("refer") <> "" then
		Response.Cookies("vendororder") = request.querystring("refer")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
	elseif request.querystring("source") <> "" then
		Response.Cookies("vendororder") = request.querystring("source")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
	elseif request.querystring("utm_source") <> "" then
		Response.Cookies("vendororder") = request.querystring("utm_source")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)		
	elseif request.querystring("clickid") <> "" then
		Response.Cookies("vendororder") = "clickid=" & request.querystring("clickid")
		Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
	end if
	on error goto 0
	
	'Rel-Canonical
	showCan = true ' TODO: Should only be turned on where needed, or pages that should be indexed will no longer be indexed
	
	canProduct = "/p-" & itemID & "-" & formatSEO(itemDesc_CO) & ".html" ' These variables are already set on product_mp.asp
	
	if Request.ServerVariables("SERVER_PORT_SECURE") = "1" then
		strHTTP = "https"
	else
		strHTTP = "http"
	end if
	
	REWRITE_URL = Request.ServerVariables("HTTP_X_REWRITE_URL")
	if inStr(Request.ServerVariables("HTTP_X_REWRITE_URL"),"?") > 0 then 
		REWRITE_URL = left(Request.ServerVariables("HTTP_X_REWRITE_URL"),inStr(Request.ServerVariables("HTTP_X_REWRITE_URL"),"?")-1)
	end if
	
	if pageTitle = "Home" then REWRITE_URL = ""
	if pageTitle = "product.asp" and canProduct <> "" then REWRITE_URL = canProduct
	
	canonicalURL = strHTTP & "://" & Request.ServerVariables("SERVER_NAME") & REWRITE_URL



	cms_rewriteURL = prepStr(request.QueryString("curURL"))
	if cms_rewriteURL <> "" then useURL = cms_rewriteURL else useURL = request.ServerVariables("HTTP_X_REWRITE_URL")
	if instr(cms_basepage,"brand-model-category-") > 0 then cms_basepage = "brand-model-category.asp"
	metaString = ""
	call getCmsData(2,cms_basepage,useURL,metaString)
	if instr(metaString, "!##!") > 0 then
		metaArray = split(metaString,"!##!")
		SEtitle = metaArray(0)
		SEdescription = metaArray(1)
		SEkeywords = metaArray(2)
		SeTopText = metaArray(3)
		SeBottomText = metaArray(4)
		SeH1 = metaArray(5)
	end if
	
	if prepVal(setCanonical) <> "" then
		showCan = true
		canonicalURL = "http://" & request.ServerVariables("SERVER_NAME") & setCanonical
	end if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title><%=SEtitle%></title>
	<meta name="Description" content="<%=SEdescription%>">
	<meta name="Keywords" content="<%=SEkeywords%>">
    <meta name="alternate" content="http://m.cellularoutfitter.com<%=request.ServerVariables("HTTP_X_REWRITE_URL")%>">
	<% if showCan then %><link rel="canonical" href="<%=lcase(canonicalURL)%>" /><% end if %>
	<meta name="verify-v1" content="KDSemcIPOLKERRNLdtodRPug14ThOrk3fAAECAAzt/U=" />
	<meta name="verify-v1" content="7oJ6qXmQ/MOO9Wics9IknbH0gaHI9z5Tl3dH82NBhO8=" />
    <meta name="msvalidate.01" content="DFF5FF52EAB66FFFC627628486428C9B" />
	<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/includes/css/styleCO.css<%=getCssDateParam("/includes/css/styleCO.css")%>" />
    <!--#include virtual="/includes/template/inc_HeadTagEnd.asp"-->
	<% if prepInt(noindex) = 1 and prepInt(nofollow) = 0 then %>
    <META NAME="ROBOTS" CONTENT="NOINDEX">
    <% elseif prepInt(noindex) = 1 and prepInt(nofollow) = 1 then %>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <% elseif prepInt(noindex) = 0 and prepInt(nofollow) = 1 then %>
    <META NAME="ROBOTS" CONTENT="NOFOLLOW">
    <% else %>
    <META NAME="ROBOTS" CONTENT="INDEX, FOLLOW">
    <% end if %>
</head>
<body class="body">
	<% call printPIxel(2) %>
<!--#include virtual="/includes/template/topHTML.asp"-->