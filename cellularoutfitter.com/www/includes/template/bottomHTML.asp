            <%
			if isnull(noLeftSide) or len(noLeftSide) < 1 or not isnumeric(noLeftSide) then noLeftSide = 0
			if noLeftSide = 0 then
			%>
            </div>
            <% end if %>
        </td>
    </tr>
	<% if not noCommentBox then %>
    <tr>
    	<td id="commentCard" style="padding:20px 0px 40px 0px; width:100%;" align="center">
	        <!--#include virtual="/includes/asp/commentBox.asp"-->
        </td>
    </tr>
	<% end if %>
</table>

<table cellpadding="0" cellspacing="0" width="100%" align="center" id="siteBottom">
	<tr>
    	<td width="100%" align="center" style="background-color:#222;">
			<div id="footer_darkBand" style="color:#fff; width:1020px; text-align:left;">
            	<div style="float:left; padding:5px 0px 5px 0px;">
                    <div style="float:left;"><img src="/images/template/newsletter-icon.jpg" border="0" /></div>
                    <div style="float:left; color:#fff; font-size:16px; font-weight:bold; padding:7px 0px 0px 5px;">JOIN OUR NEWSLETTER</div>
                    <div style="float:left; margin:3px 0px 0px 20px;">
                        <form name="newsletter2" method="post" onsubmit="addNewsletter(document.newsletter2.semail.value,'Bottom Widget');return(false);">
                        <div style="float:left; width:160px; margin-top:1px; padding:6px 0px 0px 3px; border-radius:5px; background-color:#fff; height:20px;"><input id="newsEmail" name="semail" type="text" class="inputbox" style="width:150px; border:none;" value="Email Address" onclick="this.value='';"></div>
                        <div style="float:left; margin-left:5px;"><input type="image" src="/images/template/newsletter-signup.jpg" border="0" alt="Go!" onclick="addNewsletter(document.newsletter2.semail.value,'Bottom Widget');" /></div>
                        </form>                    
                    </div>
                </div>
            	<div style="float:right; padding-top:5px;">
                    <div style="float:left; color:#fff; font-size:16px; font-weight:bold; padding:7px 10px 0px 0px;">FOLLOW US ON</div>
                	<div style="float:left; padding-right:3px;">
						<a href="//plus.google.com/106042856978446012087?prsrc=3" rel="publisher" target="_blank" style="text-decoration:none;">
							<img src="//ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
						</a>
                    </div>
                    <div style="float:left; padding-right:3px;"><a href="http://www.facebook.com/cellularoutfitter" target="_blank" title="CellularOutfitter Facebook"><img src="/images/template/sm-facebook-icon.jpg" border="0" alt="CellularOutfitter Facebook" /></a></div>
                    <div style="float:left; padding-right:3px;"><a href="http://twitter.com/celloutfitter" target="_blank" title="CellularOutfitter Twitter"><img src="/images/template/sm-twitter-icon.jpg" border="0" alt="CellularOutfitter Twitter" /></a></div>
                    <!--<div style="float:left; padding-right:3px;"><a href="http://www.cellularoutfitter.com/blog/" target="_blank" title="CellularOutfitter Blog"><img src="/images/template/sm-blog-icon.jpg" border="0" alt="CellularOutfitter Blog" /></a></div>-->
                </div>
            </div>
        </td>
    </tr>
	<tr>
    	<td width="100%" align="center" style="background-color:#444; padding:20px 0px 20px 0px;">
			<div id="footer_grayBox" style="color:#fff; width:1020px; text-align:left;">
            	<div style="float:left; width:250px;">
                	<div style="float:left; width:100%; color:#ccc; font-size:16px; font-weight:bold; border-bottom:2px solid #ccc; padding-bottom:2px; margin-bottom:10px;">COMPANY INFORMATION</div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/" class="footerLink" title="Cellularoutfitter.com">Home</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/about-us.html" class="footerLink" title="about us">About Us</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/shipping-policy.html" class="footerLink" title="shipping & store policy">Shipping & Store Policy</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/track-your-order.html" class="footerLink" title="track your order">Order Status</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/faq.html" class="footerLink" title="faq">FAQ</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/contact-us.html" class="footerLink" title="contact us">Contact Us</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="https://www.pepperjamnetwork.com/affiliate/registration.php?refid=17199" class="footerLink" title="affiliate program">Affiliate Program</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/site-map.html" style="color:#ccc; font-size:12px;" title="site map">Site Map</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/termsofuse.html" class="footerLink" title="terms of use">Terms Of Use</a></div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><a href="/privacypolicy.html" class="footerLink" title="privacy policy">Privacy Policy</a></div>
                	<!--<div style="float:left; width:100%; margin-bottom:3px;"><a href="/blog/" class="footerLink" title="blog">Blog</a></div>-->
                </div>
            	<div style="float:left; width:450px; margin-left:35px;">
                	<div style="float:left; width:100%; color:#ccc; font-size:16px; font-weight:bold; border-bottom:2px solid #ccc; padding-bottom:2px; margin-bottom:10px;">BRANDS</div>
                	<div style="float:left; width:225px;">
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-17-apple-cell-phone-accessories.html" class="footerLink" title="Apple Accessories">Apple Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-14-blackberry-cell-phone-accessories.html" class="footerLink" title="Blackberry Accessories">Blackberry Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-20-htc-cell-phone-accessories.html" class="footerLink" title="HTC Accessories">HTC Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-29-huawei-cell-phone-accessories.html" class="footerLink" title="Huawei Accessories">Huawei Accessories</a></div>                        
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-4-lg-cell-phone-accessories.html" class="footerLink" title="LG Accessories">LG Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-6-nextel-cell-phone-accessories.html" class="footerLink" title="Nextel Accessories">Nextel Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-7-nokia-cell-phone-accessories.html" class="footerLink" title="Nokia Accessories">Nokia Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-16-palm-cell-phone-accessories.html" class="footerLink" title="Palm Accessories">Palm Accessories</a></div>
                    </div>
                	<div style="float:left; width:225px;">
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-18-pantech-cell-phone-accessories.html" class="footerLink" title="Pantech Accessories">Pantech Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-9-samsung-cell-phone-accessories.html" class="footerLink" title="Samsung Accessories">Samsung Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-10-sanyo-cell-phone-accessories.html" class="footerLink" title="Sanyo Accessories">Sanyo Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-19-t-mobile-sidekick-cell-phone-accessories.html" class="footerLink" title="Sidekick Accessories">Sidekick Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-11-siemens-cell-phone-accessories.html" class="footerLink" title="Siemens Accessories">Siemens Accessories</a></div>                        
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-2-sony-ericsson-cell-phone-accessories.html" class="footerLink" title="Sony Ericsson Accessories">Sony Ericsson Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/b-30-zte-cell-phone-accessories.html" class="footerLink" title="ZTE Accessories">ZTE Accessories</a></div>
						<div style="float:left; width:100%; margin-bottom:3px;"><a href="/samsung-galaxy-accessories.html" class="footerLink" title="Samsung Galaxy Accessories">Samsung Galaxy Accessories</a></div>
                    </div>
                </div>
            	<div style="float:left; width:250px; margin-left:35px;">
                	<div style="float:left; width:100%; color:#ccc; font-size:16px; font-weight:bold; border-bottom:2px solid #ccc; padding-bottom:2px; margin-bottom:10px;">PAYMENT OPTIONS</div>
                	<div style="float:left; width:100%; margin-bottom:3px;"><img src="/images/template/payments-icons2.jpg" border="0" /></div>
                </div>
                <div style="display:none;"><a href="https://plus.google.com/106042856978446012087?rel=author" target="_blank" style="display:none;" rel="me">&nbsp;</a></div>
                <div style="float:left; width:100%; padding:10px 0px 10px 0px; color:#ccc; text-align:center; font-size:12px;">&copy;<%=year(date)%> All rights reserved. CellularOutfitter.com</div>
            </div>
            <div id="footer_grayBox_b" class="footerLowerLinks">
            	<div class="tb">
                    <div class="fl footerRow1">
                        <div class="tb footerTitle">COMPANY INFO</div>
                        <div class="tb footerLinkBox"><a href="/about-us.html" class="footerLink" title="about us">About Us</a></div>
                        <div class="tb footerLinkBox"><a href="/contact-us.html" class="footerLink" title="contact us">Contact Us</a></div>
                        <div class="tb footerLinkBox"><a href="https://www.pepperjamnetwork.com/affiliate/registration.php?refid=17199" class="footerLink" title="affiliate program">Affiliate Program</a></div>
                        <div class="tb footerTitleBottom">SERVICE &amp; SUPPORT</div>
                        <div class="tb footerLinkBox"><a href="/track-your-order.html" class="footerLink" title="track your order">Order Status</a></div>
                        <div class="tb footerLinkBox"><a href="/shipping-policy.html" class="footerLink" title="shipping & store policy">Shipping & Store Policy</a></div>
                        <div class="tb footerLinkBox"><a href="/faq.html" class="footerLink" title="faq">FAQ</a></div>
                    </div>
                    <div class="fl footerRow2">
                        <div class="tb footerTitle">SHOP BY PHONE BRAND</div>
                        <div class="fl brandLinks">
                            <div class="tb footerLinkBox"><a href="/b-17-apple-cell-phone-accessories.html" class="footerLink" title="Apple Accessories">Apple Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-14-blackberry-cell-phone-accessories.html" class="footerLink" title="Blackberry Accessories">Blackberry Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-20-htc-cell-phone-accessories.html" class="footerLink" title="HTC Accessories">HTC Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-29-huawei-cell-phone-accessories.html" class="footerLink" title="Huawei Accessories">Huawei Accessories</a></div>                        
                            <div class="tb footerLinkBox"><a href="/b-4-lg-cell-phone-accessories.html" class="footerLink" title="LG Accessories">LG Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-6-nextel-cell-phone-accessories.html" class="footerLink" title="Nextel Accessories">Nextel Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-7-nokia-cell-phone-accessories.html" class="footerLink" title="Nokia Accessories">Nokia Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-16-palm-cell-phone-accessories.html" class="footerLink" title="Palm Accessories">Palm Accessories</a></div>
                        </div>
                        <div class="fl brandLinks">
                            <div class="tb footerLinkBox"><a href="/b-18-pantech-cell-phone-accessories.html" class="footerLink" title="Pantech Accessories">Pantech Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-9-samsung-cell-phone-accessories.html" class="footerLink" title="Samsung Accessories">Samsung Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-10-sanyo-cell-phone-accessories.html" class="footerLink" title="Sanyo Accessories">Sanyo Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-19-t-mobile-sidekick-cell-phone-accessories.html" class="footerLink" title="Sidekick Accessories">Sidekick Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-11-siemens-cell-phone-accessories.html" class="footerLink" title="Siemens Accessories">Siemens Accessories</a></div>                        
                            <div class="tb footerLinkBox"><a href="/b-2-sony-ericsson-cell-phone-accessories.html" class="footerLink" title="Sony Ericsson Accessories">Sony Ericsson Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/b-30-zte-cell-phone-accessories.html" class="footerLink" title="ZTE Accessories">ZTE Accessories</a></div>
                            <div class="tb footerLinkBox"><a href="/samsung-galaxy-accessories.html" class="footerLink" title="Samsung Galaxy Accessories">Samsung Galaxy Accessories</a></div>
                        </div>
                    </div>
                    <div class="fl footerRow3">
                        <div class="tb footerTitle">PAYMENT OPTIONS</div>
                        <div class="tb paymentOptionsB"></div>
                        <div class="tb footerTitleBottomLock">SHOP WITH CONFIDENCE</div>
                        <div class="tb">
                            <div class="fl confidence1">
                                <div class="tb" style="padding-top:20px;"><a target="_blank" href="https://www.mcafeesecure.com/RatingVerify?ref=www.cellularoutfitter.com"><img width="115" height="32" border="0" src="//images.scanalert.com/meter/www.cellularoutfitter.com/12.gif" alt="McAfee SECURE sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="alert('Copying Prohibited by Law - McAfee SECURE is a Trademark of McAfee, Inc.'); return false;"></a></div>
                            </div>
                            <div class="fl confidence2">
                                <a href="javascript:Open_Popup('<%=strHTTP%>://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.CELLULAROUTFITTER.COM&lang=en','VeriSign','width=560,height=500,location=yes,status=yes,resizable=yes,scrollbars=yes');"><div id="verisignLogo" style="margin:20px auto;" title="CELLULAR OUTFITTER, INC. has been verified by Verisign"></div></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tb veryBottomLinks">
                	<div class="fl americanFlag"></div>
                    <div class="fl copywriteTxt">&copy;2013 CELLULAROUTFITTER.COM. ALL RIGHTS RESERVED.</div>
                    <div class="fl vbl_a"><a href="/termsofuse.html" class="footerLink" title="terms of use">Terms Of Use</a></div>
                    <div class="fl vbl_a"><a href="/privacypolicy.html" class="footerLink" title="privacy policy">Privacy Policy</a></div>
                    <div class="fl vbl_b"><a href="/site-map.html" style="color:#ccc; font-size:12px;" title="site map">Site Map</a></div>
                </div>
            </div>
        </td>
	</tr>
</table>
<link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
<div id="promoDetailBox" style="display:none;" onclick="closeReviewPopup()">
	<div id="promoPop1" class="tb containerA">
		<div class="tb promoBoxMain">
			<div class="tb closeWinRow">
				<div class="fr closeWin" onclick="closeReviewPopup()"></div>
			</div>
			<div class="promoTitle">LIMITED TIME OFFER!</div>
			<div class="promoDetails">
				Place an order today and receive a <strong>FREE GIFT</strong>. We are giving away a Ballpoint Pen &amp; Stylus Combo (valued at $7.99) 
				with each order until we run out. This is a limited time offer, so order today to get your free gift. Supplies are 
				running out fast!
			</div>
			<div class="tb promoBottom">
				<div class="fl itemPic"></div>
				<div class="fl promoPoints">
					<div class="tb promoPointRow">
						<div class="fl promoCheckmark"></div>
						<div class="fl promoPointTxt">The ultimate in convenience and versatility - a pen AND stylus in one!</div>
					</div>
					<div class="tb promoPointRow">
						<div class="fl promoCheckmark"></div>
						<div class="fl promoPointTxt">Perfect for navigating on touch screen devices of all shapes and sizes.</div>
					</div>
					<div class="tb promoPointRow">
						<div class="fl promoCheckmark"></div>
						<div class="fl promoPointTxt">Sleek and super smooth twisting barrel with black ink.</div>
					</div>
					<div class="promoCTA" onclick="closeReviewPopup()"></div>
					<div class="promoRestrictions">* LIMIT ONE PER HOUSEHOLD WHILE SUPPLIES LAST.</div>
				</div>
			</div>
		</div>
	</div>
    <div id="promoPop2" class="tb containerA" style="display:none;">
		<div class="tb promoBoxMain2">
			<div class="tb closeWinRow">
				<div class="fr closeWin" onclick="closeReviewPopup()"></div>
			</div>
			<div class="promoTitle">LIMITED TIME OFFER!</div>
			<div class="promoDetails2">
				<span class="promoDetails2Ex1">Place an order today and receive a <span class="promoDetails2Small">FREE GIFT</span>. We are giving away one Non-Slip Car</span><br />
                <span class="promoDetails2Ex2">Pad (valued at $4) with every order, but only while supplies last. This is a limited time</span><br />
                <span class="promoDetails2Ex3">offer, so order now to receive yours before they run out!</span>
			</div>
			<div class="tb promoBottom">
				<div class="fl itemPic2"></div>
				<div class="fl promoPoints2">
					<div class="tb promoPointRow2">
						<div class="fl promoCheckmark"></div>
						<div class="fl promoPointTxt">Keeps your cell phone and possessions<br />safe and within reach at all times</div>
					</div>
					<div class="tb promoPointRow2">
						<div class="fl promoCheckmark"></div>
						<div class="fl promoPointTxt">Attach to your vehicle's dashboard,<br />windows, or virtually any smooth surface!</div>
					</div>
					<div class="tb promoPointRow2">
						<div class="fl promoCheckmarkSL"></div>
						<div class="fl promoPointTxt">Non-magnetic, anti-slip technology</div>
					</div>
					<div class="promoCTA2" onclick="closeReviewPopup()"></div>
					<div class="promoRestrictions">* LIMIT ONE PER HOUSEHOLD WHILE SUPPLIES LAST.</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="testZone"></div>
<div id="dumpZone"></div>
<%
if prepInt(request.cookies("mySession")) = 0 then
	mySession = session.sessionid
	response.cookies("mySession") = mySession
	response.cookies("mySession").expires = dateAdd("m", 1, now)
else
	mySession = prepInt(request.cookies("mySession"))
end if
%>
<% if prepInt(itemID) > 0 then %>
<script type="text/javascript">
	if (typeof StrandsTrack=="undefined"){ StrandsTrack=[]; }
	StrandsTrack.push({event:"visited", item: "<%=prepInt(itemID)%>"});  
</script>
<% end if %>
<% if prepInt(request.cookies("mySession")) > 0 then %>
<script type="text/javascript">
	if (typeof StrandsTrack=="undefined"){ StrandsTrack=[]; }
	StrandsTrack.push({event:"userlogged", user: "<%=prepInt(request.cookies("mySession"))%>"});
</script>
<% end if %>
<% if prepStr(request.QueryString("search")) <> "" then %>
<script type="text/javascript">
	if (typeof StrandsTrack=="undefined"){StrandsTrack=[];}
	StrandsTrack.push({event:"searched",searchstring: "<%=prepStr(request.QueryString("search"))%>"});
</script>
<% end if %>
<!-- Strands Library -->
<script type="text/javascript" src="//bizsolutions.strands.com/sbsstatic/js/sbsLib-1.0.min.js"></script>
<script type="text/javascript">
	try{
		SBS.Worker.go("Kx7AmRMCrW");
	}
	catch (e){};
</script>
<!--#include virtual="/includes/asp/inc_pixels.asp"-->
<script language="javascript" src="/frameWork/Utility/base.js"></script>
<script>
	function addNewsletter(semail,loc) {
		_gaq.push(['_trackEvent', 'Email Signup', 'Submission', 'Desktop', loc]);
		if(typeof(_vis_opt_top_initialize) == "function") {
			// Code for Custom Goal: Email Sign-Up
			 _vis_opt_goal_conversion(204);
			// uncomment the following line to introduce a half second pause to ensure goal always registers with the server
			// _vis_opt_pause(500);
		}
		ajax('/ajax/newsletter.asp?semail=' + semail + '&brandID=<%=prepInt(brandID)%>&modelID=<%=prepInt(modelID)%>&typeID=<%=prepInt(typeID)%>&itemID=<%=prepInt(itemID)%>','popBox');
		document.getElementById("popBox").style.position = "absolute";
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
		return false;
	}
</script>
<script type="text/javascript" src="/includes/js/jsCombo_index.js?v=20140110"></script>
<script type="text/javascript" src="/includes/js/km.js"></script>
<script language="javascript">
	//noAA:<%=prepInt(session("adminID"))%>
	//sip: <%=request.ServerVariables("LOCAL_ADDR")%>
	function closeReviewPopup() {
		document.getElementById('popCover').style.display = 'none';
		document.getElementById('popBox').style.display = 'none';
		document.getElementById('popBox').innerHTML = '';
	}
	
	function showPromoDetails() {
		document.getElementById('promoZone').innerHTML = "";
		ajax('/ajax/showPromoPopup.asp','promoZone');
		checkPromo('no');
	}
	
	function checkPromo(force)
	{
		if (force == "yes") {
			document.getElementById('popBox').innerHTML = document.getElementById('promoDetailBox').innerHTML;
			document.getElementById('popCover').style.display = '';
			document.getElementById('popBox').style.display = '';
		}
		else {
			if ("" == document.getElementById('promoZone').innerHTML) setTimeout('checkPromo()', 50);
			else if ("0" == document.getElementById('promoZone').innerHTML) {
				document.getElementById('popBox').innerHTML = document.getElementById('promoDetailBox').innerHTML;
				document.getElementById('popCover').style.display = '';
				document.getElementById('popBox').style.display = '';
			}
		}
	}

	<%if instr(request.ServerVariables("URL"),"index_mp.asp") < 1 then%>
	window.onload = function() { showPromoDetails(); }
	<%end if%>
</script>
<div id="testZone"></div>
<div id="dumpZone" style="display:none;"></div>
<div id="promoZone" style="display:none;"></div>
</body>
</html>
