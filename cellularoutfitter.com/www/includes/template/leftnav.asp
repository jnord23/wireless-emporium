<%
if curPageName = "BMC-b" then
	if categoryID = 18 then useCatID = 999 else useCatID = categoryID
	
	if useCatID = 16 then
		sql = 	"select	distinct c.id subid, c.carriername subName" & vbcrlf & _
				"from	we_items a join we_carriers c" & vbcrlf & _
				"	on	a.carrierid = c.id left outer join we_pndetails d" & vbcrlf & _
				"	on	a.partnumber = d.partnumber" & vbcrlf & _
				"where	a.typeid = '16' " & vbcrlf & _
				"	and	a.brandid = '" & brandid & "'" & vbcrlf & _
				"	and a.hidelive = 0 " & vbcrlf & _
				"	and (a.inv_qty <> 0 or d.alwaysinstock = 1) " & vbcrlf & _
				"	and a.price_co > 0 	" & vbcrlf & _
				"	and	c.id in (1,2,3,4,5,6,7,8,9,11)" & vbcrlf & _
				"order by subid"
	else
		sql =	"select b.subTypeID subid, b.subTypeName subName " & vbcrlf & _
				"from we_typeMatrix a " & vbcrlf & _
					"left join we_subTypes b on a.origSubTypeID = b.subTypeID " & vbcrlf & _
				"where b.hidelive = 0 and b.subtypeid not in (1064,1031) and (maskTypeID = " & prepInt(useCatID) & ")"
	end if
	if customFilter = 1 then sql = replace(sql,"(maskTypeID","(b.subtypeid = 1284 or maskTypeID")
	session("errorSQL") = sql
	set rsSub = oConn.execute(sql)
%>
<div style="float:left; width:190px;">
	<div style="float:left; margin-top:10px;"><img src="/images/leftFilter/header-filter-by.jpg" border="0" title="Filter Menu" /></div>
	<div style="float:left; background-color:#ebebeb; border-bottom-left-radius:10px; border-bottom-right-radius:10px; width:190px;">
		<form name="filterForm" action="<%=request.ServerVariables("HTTP_X_REWRITE_URL")%>" method="post" style="padding:0px; margin:0px;">
		<div style="float:left; padding-bottom:10px; margin:0px 0px 10px 13px; width:163px; background-color:#FFF; border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc; border-bottom-left-radius:10px; border-bottom-right-radius:10px;">
			<div id="mvt_leftFilter1">
				<%
				if not rsSub.EOF then
					subNameHeader = "Style Type"
					if useCatID = 16 then subNameHeader = "Carriers"
				%>
				<div class="filter-heading clearfix">
					<div class="filter-heading-arrow"><img src="/images/leftFilter/arrow.jpg" border="0" /></div>
					<div class="filter-heading-name"><%=subNameHeader%></div>
				</div>
				<%
					do while not rsSub.EOF
						subID = prepInt(rsSub("subID"))
						subName = rsSub("subName")
						subName = replace(subName,"& Hybrid","")
				%>
				<div class="filter-item clearfix">
					<div class="filter-item-checkbox"><input type="checkbox" name="styleType" data-filter-name="<%=subName%>" value="<%=subID%>" onclick="updateFilter()"<% if carrierid = subID then %> checked="checked"<% end if %> /></div>
					<div class="filter-item-name"><%=subName%></div>
				</div>
				<%
						rsSub.movenext
					loop
				end if
				%>
			</div>
			<div id="mvt_leftFilter2">
				<div class="filter-heading clearfix">
					<div class="filter-heading-arrow"><img src="/images/leftFilter/arrow.jpg" border="0" /></div>
					<div class="filter-heading-name">Sort By</div>
				</div>
				<div id="sortOptions" class="filter-item">
					<%
					curPage = request.ServerVariables("HTTP_X_REWRITE_URL")
					if instr(curPage,"?") > 0 then curPage = left(curPage,instr(curPage,"?")-1)
					%>
					<select name="sortBy" onchange="sortProduct(<%=brandID%>,<%=modelID%>,<%=categoryID%>,this.value)">
						<option value="hl"<% if sortBy = "hl" then %> selected="selected"<% end if %>>Price High to Low</option>
						<option value="lh"<% if sortBy = "" or sortBy = "lh" then %> selected="selected"<% end if %>>Price Low to High</option>
						<option value="bs"<% if sortBy = "bs" then %> selected="selected"<% end if %>>Best Selling</option>
					</select>
				</div>
			</div>
			<div id="mvt_leftFilter3">
				<div class="filter-heading clearfix">
					<div class="filter-heading-arrow"><img src="/images/leftFilter/arrow.jpg" border="0" /></div>
					<div class="filter-heading-name">Price Range</div>
				</div>
				<div class="filter-item clearfix">
					<div class="filter-item-checkbox"><input type="checkbox" name="priceRange" data-filter-name="Up to $4.99" value="4" onclick="updateFilter()" /></div>
					<div class="filter-item-name">Up to $4.99</div>
				</div>
				<div class="filter-item clearfix">
					<div class="filter-item-checkbox"><input type="checkbox" name="priceRange" data-filter-name="$5 to $9.99" value="5" onclick="updateFilter()" /></div>
					<div class="filter-item-name">$5 to $9.99</div>
				</div>
				<div class="filter-item clearfix">
					<div class="filter-item-checkbox"><input type="checkbox" name="priceRange" data-filter-name="$10 to $19.99" value="10" onclick="updateFilter()" /></div>
					<div class="filter-item-name">$10 to $19.99</div>
				</div>
				<div class="filter-item clearfix">
					<div class="filter-item-checkbox"><input type="checkbox" name="priceRange" data-filter-name="$20 to $29.99" value="20" onclick="updateFilter()" /></div>
					<div class="filter-item-name">$20 to $29.99</div>
				</div>
				<div class="filter-item clearfix">
					<div class="filter-item-checkbox"><input type="checkbox" name="priceRange" data-filter-name="$30 and Up" value="30" onclick="updateFilter()" /></div>
					<div class="filter-item-name">$30 and Up</div>
				</div>
			</div>
		</div>
		</form>
	</div>
	<div style="float:left; border-bottom:1px solid #CCC; margin:10px 0px 10px 0px; width:100%;"></div>
	<div style="float:left; background-color:#fff; border:1px solid #CCC; border-radius:10px; width:190px;">
		<div style="float:left; margin:10px 0px 0px 20px; width:143px; text-align:center; border-bottom:1px solid #CCC; padding-bottom:10px;"><img src="/images/leftFilter/header-buy-confidence.jpg" border="0" /></div>
		<div style="float:left; margin-top:10px; width:100%; text-align:center; font-size:11px; color:#999;">
			All of your information is<br />
			secured and transmitted<br />
			utilizing the highest level<br />
			of Secure Sockets<br />
			Layer (SSL) encryption
		</div>
		<div style="float:left; margin-top:10px; width:100%; text-align:center;"><img src="/images/leftFilter/seal-truste.jpg" border="0" /></div>
		<div style="float:left; margin-top:10px; width:100%; text-align:center;"><img src="/images/leftFilter/seal-bbb.jpg" border="0" /></div>
		<div style="float:left; margin-top:10px; width:100%; text-align:center;"><img src="/images/leftFilter/seal-verisign.jpg" border="0" /></div>
		<div style="float:left; margin:10px 0px 20px 0px; width:100%; text-align:center;"><img src="/images/leftFilter/seal-mcafee.jpg" border="0" /></div>
	</div>
</div>
<% else %>
	<% if noLeftSide = 0 then %>
		<% arrowSize = 14 %>
<div style="width:185px; padding-top:10px;">
	<div id="newLeftNav">
		<div id="blueSearchBox">
			<form method="get" action="/search/search.asp" style="margin:0px; padding:0px;">
			<div style="float:left; margin:15px 0px 0px 10px;">
				<div style="float:left; width:135px; background-color:#fff; height:20px; padding-left:5px; border-top-left-radius:5px; border-bottom-left-radius:5px;">
					<input type="text" name="keywords" value="Search" onclick="this.value=''" style="color:#999; width:130px; border:none;" />
				</div>
				<div style="float:left; width:20px; background-color:#fff; height:17px; padding-top:3px; border-top-right-radius:5px; border-bottom-right-radius:5px;">
					<input type="image" name="btnSubmit" src="/images/template/search-icon.jpg" />
				</div>
			</div>
			</form>
		</div>
		<div id="leftBrandNav" style="float:left; margin-top:5px;">
			<div id="blueShopByBrand"></div>
			<div id="leftBrandWarpper">
				<div id="leftBrandBox" onclick="window.location='/b-17-apple-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-17-apple-cell-phone-accessories.html" title="Apple">Apple</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-14-blackberry-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-14-blackberry-cell-phone-accessories.html" title="BlackBerry">BlackBerry</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-28-casio-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-28-casio-cell-phone-accessories.html" title="Casio">Casio</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-20-htc-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-20-htc-cell-phone-accessories.html" title="HTC">HTC</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-29-huawei-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-29-huawei-cell-phone-accessories.html" title="Huawei">Huawei</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-3-kyocera-qualcomm-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-3-kyocera-qualcomm-cell-phone-accessories.html" title="Kyocera">Kyocera</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-4-lg-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-4-lg-cell-phone-accessories.html" title="LG">LG</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-5-motorola-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-5-motorola-cell-phone-accessories.html" title="Motorola">Motorola</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-6-nextel-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-6-nextel-cell-phone-accessories.html" title="Nextel">Nextel</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-7-nokia-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-7-nokia-cell-phone-accessories.html" title="Nokia">Nokia</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-16-palm-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-16-palm-cell-phone-accessories.html" title="Palm">Palm</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-18-pantech-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-18-pantech-cell-phone-accessories.html" title="Pantech">Pantech</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>                            	
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-9-samsung-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-9-samsung-cell-phone-accessories.html" title="Samsung">Samsung</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-10-sanyo-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-10-sanyo-cell-phone-accessories.html" title="Sanyo">Sanyo</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-19-t-mobile-sidekick-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-19-t-mobile-sidekick-cell-phone-accessories.html" title="Sidekick">Sidekick</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-2-sony-ericsson-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-2-sony-ericsson-cell-phone-accessories.html" title="Sony Ericsson">Sony Ericsson</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-30-zte-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-30-zte-cell-phone-accessories.html" title="ZTE">ZTE</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>
				</div>
				<div id="leftBrandDiv"></div>
				<div id="leftBrandBox" onclick="window.location='/b-15-other-cell-phone-accessories.html'">
					<div style="float:left; padding-left:5px;"><a class="left_nav" href="/b-15-other-cell-phone-accessories.html" title="Other Brands">Other Brands</a></div>
					<div style="float:right;"><div id='leftNavArrow'></div></div>
				</div>
			</div>
		</div>
        <!--
        <div style="float:left; width:100%; text-align:center; padding:10px 0px 10px 0px; border-bottom:1px solid #ccc;">
			<a href="/custom-phone-covers"><img src="/images/custom/leftNav2.jpg" border="0" /></a>
		</div>
        -->
		<div style="float:left; width:100%; text-align:center; padding:10px 0px 10px 0px; border-bottom:1px solid #ccc;">
			<a target="_blank" href="https://www.mcafeesecure.com/RatingVerify?ref=www.cellularoutfitter.com"><img width="115" height="32" border="0" src="//images.scanalert.com/meter/www.cellularoutfitter.com/12.gif" alt="McAfee SECURE sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="alert('Copying Prohibited by Law - McAfee SECURE is a Trademark of McAfee, Inc.'); return false;"></a>
		</div>
		<div style="width:100%; float:left; padding:5px 0px 5px 0px; text-align:center;">
			<script type="text/javascript">
			<!--
				google_ad_client = "ca-pub-1001075386636876";
				/* CO Skyscraper */
				google_ad_slot = "2604906734";
				google_ad_width = 160;
				google_ad_height = 600;
			//-->
			</script>
			<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
		</div>
		<% if leftGoogleAd = 1 then %>
		<div id="adcontainer2"></div>
		<% end if %>
	</div>
</div>
	<% end if 'noLeftSide = 0%>
<% end if 'curPageName = "BMC_new"%>