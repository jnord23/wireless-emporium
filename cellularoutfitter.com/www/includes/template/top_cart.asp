<%
'dim cart : cart = 1
curPage = request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL")
if instr(curPage,"216.139.235.93") > 0 then
	response.Status = "301 Moved Permanently"
	response.AddHeader "Location", "http://" & replace(curPage,"216.139.235.93","www.cellularOutfitter.com")
	response.end
end if

saveEmail()

betaView = request.Cookies("betaView")

dim relatedItems(10), NumRecords, a, b
NumRecords = 0
for a = 1 to 10
	relatedItems(a) = 999999
next

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(formatSEO, "?", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "covers-screen-guards"
		case "bling-kits---charms" : formatSEO = "bling-kits-charms"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "hands-free-kits-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO = "holsters-car-mounts"
		case "leather-cases" : formatSEO = "cases-pouches"
		case "cell-phones" : formatSEO = "wholesale-cell-phone"
		case "full-body-protectors" : formatSEO = "invisible-film-protectors"
	end select
end function

function nameSEO(val)
	select case val
		case "Antennas/Parts" : nameSEO = "Antennas"
		case "Faceplates" : nameSEO = "Covers &amp; Screen Guards"
		case "Data Cable & Memory" : nameSEO = "Data Cables &amp; Memory Cards"
		case "Hands-Free" : nameSEO = "Hands-Free Kits &amp; Bluetooth Headsets"
		case "Holsters/Belt Clips" : nameSEO = "Holsters / Car Mounts"
		case "Leather Cases" : nameSEO = "Cases &amp; Pouches"
		case "Cell Phones" : nameSEO = "Wholesale Unlocked Cell Phones"
		case "Full Body Protectors" : nameSEO = "Invisible Film Protectors"
		case else : nameSEO = val
	end select
end function

function singularSEO(val)
	select case val
		case "Batteries" : singularSEO = "Battery"
		case "Chargers" : singularSEO = "Charger"
		case "Faceplates" : singularSEO = "Cover &amp; Screen Guard"
		case "Hands-Free" : singularSEO = "Hands Free / Bluetooth Headset"
		case "Holsters/Belt Clips" : singularSEO = "Holster / Car Mount"
		case "Leather Cases" : singularSEO = "Case / Pouch"
		case "Antennas/Parts" : singularSEO = "Antenna"
		case "Data Cable & Memory" : singularSEO = "Data Cable / Memory Card"
		case "Bling Kits & Charms" : singularSEO = "Bling Kit / Charm"
		case "Cell Phones" : singularSEO = "Wholesale Unlocked Cell Phone"
		case "Full Body Protectors" : singularSEO = "Invisible Film Protector"
		case else : singularSEO = val
	end select
end function

if SEtitle = "" then SEtitle = "Wholesale Cell Phone Accessories ?Wholesale Cellular Phone Accessories ?CellularOutfitter.com"
if SEdescription = "" then SEdescription = "Buy Cheap Cell Phone Accessories at Wholesale Prices to the Public. Find the latest Discount Cellular Phone Accessories for Motorola, LG, Nokia, Samsung phones and more. Cellular Accessories at the Lowest Prices Online Guaranteed."
if SEkeywords = "" then SEkeywords = "cheap cell phone accessories, cell phone accessories, discount cellular phone accessories, cellular phone accessories"
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><%=SEtitle%></title>
    <meta name="Description" content="<%=SEdescription%>">
    <meta name="Keywords" content="<%=SEkeywords%>">
    <meta name="alternate" content="http://m.cellularoutfitter.com<%=request.ServerVariables("HTTP_X_REWRITE_URL")%>">
    <% if prepInt(noIndex) = 1 then %>
    <meta name="robots" content="noindex,nofollow">
    <% end if %>
	<%if pageTitle = "Home" then%>
    <meta name="robots" content="index,follow">
    <!-- Cell Phone Accessories at CellularOutfitter.com: Motorola Cell Phone Accessories, Nokia Cell Phone Accessories, LG Cell Phone Accessories, Samsung Cell Phone Accessories -->
    <meta name="verify-v1" content="KDSemcIPOLKERRNLdtodRPug14ThOrk3fAAECAAzt/U=" />
    <meta name="verify-v1" content="7oJ6qXmQ/MOO9Wics9IknbH0gaHI9z5Tl3dH82NBhO8=" />
    <%end if%>
    <link href="/includes/css/styleCO.css<%=getCssDateParam("/includes/css/styleCO.css")%>" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <style>
        #Promotions {display:none;}
        
        body #google_amark_b, body #google_amark_b * { text-align:left !important; padding:0 !important; margin:0 !important; border:0 !important; position:relative !important; font-weight:normal !important; text-decoration:none !important; font-size:11px !important; font-family:Arial, sans-serif !important; background:#fff !important; float:none !important; }
        #google_amark_b .m img, #google_amark_b #t img, #google_amark_b #x a { display:block !important; }
        #google_amark_b .h { position:absolute !important; width:325px !important; border:1px solid #ccc !important; }
        #google_amark_b #t { padding:6px 0 1px 0px !important; }
        #google_amark_b #l { left:10px !important; }
        #google_amark_b #x { position:absolute !important; right:0px !important; top:0px !important; border-left:1px solid #ccc !important; border-bottom:1px solid #ccc !important; }
        #google_amark_b #c { padding:5px 10px 10px 10px !important; color:#676767 !important; border-top:1px solid #ccc !important;  }
        #google_amark_b #c p { padding:5px 0 0 0 !important; }
        #google_amark_b #c p.p { padding:0 !important; }
        #google_amark_b #c p.p img { top: 5px !important; }
        #google_amark_b #c p a:link, #google_amark_b #c p a:visited { color:#0000CC !important; text-decoration:underline !important; }
    </style>
    <script src="/cart/includes/validate_new.js"></script>
    <script src="//seal.buysafe.com/private/rollover/rollover.js" type="text/javascript" language="javascript" charset="utf-8"></script>
    <script language="javascript">
    function addbookmark() {
        var IE = navigator.appName.match(/(Microsoft Internet Explorer)/gi),
        NS = navigator.appName.match(/(Netscape)/gi),
        OP = navigator.appName.match(/(Opera)/gi),
        BK = document.getElementById('bookmark');
        BK.onmouseout = function() {
            window.status = '';
        }
        if(IE && document.uniqueID) {
            var bookmarkurl = "http://www.CellularOutfitter.com/";
            var bookmarktitle = "Cellular Outfitter";
            window.external.AddFavorite(bookmarkurl,bookmarktitle);
        }
        else if(OP || IE && !document.uniqueID) {
            alert('Your browser requires that you\nPress Ctrl & D to Bookmark this page.');
        }
        else if(NS) {
            alert('Your browser requires that you\nPress Ctrl & D to Bookmark this page.');
        }
        else { BK.innerHTML = '' }
    }
    function showPromoInfo() {
        document.getElementById("Promotions").style.display = "block";
    }
    </script>
    
    <link href="/includes/css/mvt_basket/mvt_topnav2.css" rel="stylesheet" type="text/css">
    <link href="/includes/css/mvt_basket/mvt_livechat2.css" rel="stylesheet" type="text/css">
    <link href="/includes/css/mvt_basket/mvt_cart2.css" rel="stylesheet" type="text/css">
    <link href="/includes/css/mvt_basket/mvt_shipdate1.css" rel="stylesheet" type="text/css">
    <link href="/includes/css/mvt_basket/mvt_cartProdDetail2.css" rel="stylesheet" type="text/css">

    <!--#include virtual="/includes/template/inc_HeadTagEnd.asp"-->
</head>
<%
if strBody = "" then strBody = "<body class=""body"">"
response.write strBody & vbcrlf
%>
<% call printPIxel(2) %>
<div id="mbox-global"></div>
<div id="mbox-page"></div>
<div id="popCover" style="height:100%; display:none; width:100%; background-color:#333; position:fixed; left:0px; top:0px; z-index:3000; opacity:0.9; filter:alpha(opacity=90);">&nbsp;</div>
<div id="popBox" style="height:3000px; display:none; width:100%; position:fixed; left:0px; top:0px; z-index:3001; text-align:center;"></div>
<div style="position:relative;">
<table width="1020" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
	<tr>
		<td width="20" valign="top" class="sideleft_border"></td>
		<td width="980" valign="top" align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td width="100%" valign="top" align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td width="310">
                                	<%
									if month(date) = 12 and day(date) < 27 then
										holidayLogo = 1
									else
										holidayLogo = 0
									end if
									if holidayLogo = 0 then
									%>
                                    <a href="/"><img src="/images/coLogo2.gif" alt="CellularOutfitter.com" border="0"></a>
                                    <% else %>
                                    <a href="/"><img src="/images/Co_logo_xmas.jpg" alt="CellularOutfitter.com" width="350" height="90" border="0"></a>
                                    <% end if %>
                                </td>
								<td width="20">&nbsp;</td>
								<td width="470" align="right" valign="bottom">
                                	<div id="mvt_livechat">
										<div style="float:right;">
											<a href="#" onclick="OpenLHNChat();return false;">
	                                            <img src="/images/cart/minibanner-livechat.jpg" border="0" />
											</a>
                                            <script>
												function OpenLHNChat()
												{
													jQuery(window).trigger('liveChat.initiated');
													var bLHNOnline=0;
													var wleft = (screen.width - 580-32) / 2;
													var wtop = (screen.height - 420-96) / 2;
													var sScrollbars=(bLHNOnline==0)?"yes":"no";
													
													window.open('<%=useHttp%>://www.livehelpnow.net/lhn/livechatvisitor.aspx?zzwindow=6755&lhnid=3410&d=' + 0,'lhnchat','left=' + wleft + ',top=' + wtop + ',width=580,height=435,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=' + sScrollbars + ',copyhistory=no,resizable=yes');
												}
											</script>
										</div>
	                                	<div style="float:right;"><img src="/images/cart/minibanner-number.jpg" border="0" /></div>
                                    </div>
                                </td>
								<td width="10">&nbsp;</td>
								<td width="160" align="right" valign="bottom">
                                	<div style="float:right;">
										<a href="javascript:Open_Popup('https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.CELLULAROUTFITTER.COM&lang=en','VeriSign','width=560,height=500,location=yes,status=yes,resizable=yes,scrollbars=yes');">
	                                        <div id="verisignLogo" style="float:right;" title="CELLULAR OUTFITTER, INC. has been verified by Verisign"></div>
                                        </a>                                    
                                    </div>                                
                                	<div style="float:right; margin-right:10px; padding-top:8px;">
                                        <!-- START SCANALERT CODE -->
                                        <a target="_blank" href="https://www.mcafeesecure.com/RatingVerify?ref=www.cellularoutfitter.com"><img width="65" height="37" border="0" src="//images.scanalert.com/meter/www.cellularoutfitter.com/63.gif" alt="McAfee Secure sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="alert('Copying Prohibited by Law - McAfee Secure is a Trademark of McAfee, Inc.'); return false;"></a>
                                        <!-- END SCANALERT CODE -->                                    
                                    </div>
								</td>
								<td width="10" align="center" valign="bottom">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
                <tr>
                	<td style="padding-top:10px;" id="mvt_topNav">
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="/images/top/menuBg.gif">
                            <tr>
                                <td width="70" align="center" style="padding:0px 15px 0px 15px;"><a href="http://www.cellularoutfitter.com/c-3-cell-phone-covers-and-skins.html" title="Cell Phone Covers & Gel Skins" class="cat-font2">Covers & Gel Skins</a></td>
                                <td align="center"><div id="mainMenuBr"></div></td>
                                <td width="100" align="center"><a href="http://www.cellularoutfitter.com/c-7-cell-phone-cases-pouches.html" title="Cell Phone Cases & Pouches" class="cat-font2">Cases & Pouches</a></td>
                                <td align="center"><div id="mainMenuBr"></div></td>
                                <td width="100" align="center"><a href="http://www.cellularoutfitter.com/c-18-cell-phone-screen-protectors.html" title="Cell Phone Screen Protectors" class="cat-font2">Screen Protectors</a></td>
                                <td align="center"><div id="mainMenuBr"></div></td>
                                <td width="90" align="center" style="padding:0px 5px 0px 5px;"><a href="http://www.cellularoutfitter.com/c-2-cell-phone-chargers-and-data-cables.html" title="Cell Phone Chargers & Data Cables" class="cat-font2">Chargers & Data Cables</a></td>
                                <td align="center"><div id="mainMenuBr"></div></td>
                                <td width="100" align="center"><a href="http://www.cellularoutfitter.com/c-1-cell-phone-batteries.html" title="Cell Phone Batteries" class="cat-font2">Batteries</a></td>
                                <td align="center"><div id="mainMenuBr"></div></td>
                                <td width="100" align="center"><a href="http://www.cellularoutfitter.com/c-5-cell-phone-hands-free-kits-bluetooth-headsets.html" title="Cell Phone Hands-Free Kits & Headsets" class="cat-font2">Bluetooth & Hands-Free</a></td>
                                <td align="center"><div id="mainMenuBr"></div></td>
                                <td width="80" align="center" style="padding:0px 10px 0px 10px;"><a href="http://www.cellularoutfitter.com/c-6-cell-phone-holsters-car-mounts.html" title="Cell Phone Holsters & Car Mounts" class="cat-font2">Holsters & Car Mounts</a></td>
                                <td align="center"><div id="mainMenuBr"></div></td>
                                <td width="100" align="center"><a href="http://www.cellularoutfitter.com/c-8-cell-phone-other-accessories.html" title="Cell Phone Other Accessories" class="cat-font2">Other Accessories</a></td>
                                <td align="center"><div id="mainMenuBr"></div></td>
                                <td width="80" align="center" style="padding:0px 10px 0px 10px;"><a href="http://www.cellularoutfitter.com/wholesale-cell-phones.html" title="Wholesale Cell Phones" class="cat-font2">Wholesale Cell Phones</a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
				<tr>
					<td width="100%" valign="top" align="center">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
