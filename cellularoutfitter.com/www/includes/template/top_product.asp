<%
curPage = request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL")
if instr(curPage,"216.139.235.93") > 0 then
	response.Status = "301 Moved Permanently"
	response.AddHeader "Location", "http://" & replace(curPage,"216.139.235.93","www.cellularOutfitter.com")
	response.end
end if

saveEmail()

betaView = prepInt(request.Cookies("betaView"))

on error resume next
if request.querystring("id") <> "" then
	Response.Cookies("vendororder") = request.querystring("id")
	Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
elseif request.querystring("refer") <> "" then
	Response.Cookies("vendororder") = request.querystring("refer")
	Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
elseif request.querystring("source") <> "" then
	Response.Cookies("vendororder") = request.querystring("source")
	Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
elseif request.querystring("utm_source") <> "" then
	Response.Cookies("vendororder") = request.querystring("utm_source")
	Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)	
elseif request.querystring("clickid") <> "" then
	Response.Cookies("vendororder") = "clickid=" & request.querystring("clickid")
	Response.Cookies("vendororder").Expires = DateAdd("d", 45, now)
end if
on error goto 0

dim relatedItems(10), NumRecords, a, b
NumRecords = 0
for a = 1 to 10
	relatedItems(a) = 999999
next

function formatSEO(val)
	if not isNull(val) then
		formatSEO = lCase(trim(replace(val,"&trade;","-")))
		formatSEO = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(formatSEO," ","-"),"/","-"),".","-"),",","-"),"&","-"),"'","-"),chr(150),"-"),chr(151),"-"),chr(153),"-"),chr(169),"-"),chr(174),"-")
		formatSEO = replace(replace(replace(replace(replace(formatSEO,"(","-"),")","-"),";","-"),"+","-"),":","-")
		formatSEO = replace(formatSEO, "?", "-")
	else
		formatSEO = ""
	end if
	select case formatSEO
		case "sidekick" : formatSEO = "t-mobile-sidekick"
		case "at-t---cingular" : formatSEO = "att-cingular"
		case "boost-mobile---southern-linc" : formatSEO = "boost-mobile-southern-linc"
		case "sprint---nextel" : formatSEO = "sprint-nextel"
		case "u-s--cellular" : formatSEO = "us-cellular"
		case "antennas-parts" : formatSEO = "antennas"
		case "faceplates" : formatSEO = "covers-screen-guards"
		case "bling-kits---charms" : formatSEO = "bling-kits-charms"
		case "data-cable---memory" : formatSEO = "data-cables-memory-cards"
		case "hands-free" : formatSEO = "hands-free-kits-bluetooth-headsets"
		case "holsters-belt-clips" : formatSEO = "holsters-car-mounts"
		case "leather-cases" : formatSEO = "cases-pouches"
		case "cell-phones" : formatSEO = "wholesale-cell-phone"
		case "full-body-protectors" : formatSEO = "invisible-film-protectors"
	end select
end function

function nameSEO(val)
	select case val
		case "Antennas/Parts" : nameSEO = "Antennas"
		case "Faceplates" : nameSEO = "Covers &amp; Screen Guards"
		case "Data Cable & Memory" : nameSEO = "Data Cables &amp; Memory Cards"
		case "Hands-Free" : nameSEO = "Hands-Free Kits &amp; Bluetooth Headsets"
		case "Holsters/Belt Clips" : nameSEO = "Holsters / Car Mounts"
		case "Leather Cases" : nameSEO = "Cases &amp; Pouches"
		case "Cell Phones" : nameSEO = "Wholesale Unlocked Cell Phones"
		case "Full Body Protectors" : nameSEO = "Invisible Film Protectors"
		case else : nameSEO = val
	end select
end function

function singularSEO(val)
	select case val
		case "Batteries" : singularSEO = "Battery"
		case "Chargers" : singularSEO = "Charger"
		case "Faceplates" : singularSEO = "Cover &amp; Screen Guard"
		case "Hands-Free" : singularSEO = "Hands Free / Bluetooth Headset"
		case "Holsters/Belt Clips" : singularSEO = "Holster / Car Mount"
		case "Leather Cases" : singularSEO = "Case / Pouch"
		case "Antennas/Parts" : singularSEO = "Antenna"
		case "Data Cable & Memory" : singularSEO = "Data Cable / Memory Card"
		case "Bling Kits & Charms" : singularSEO = "Bling Kit / Charm"
		case "Cell Phones" : singularSEO = "Wholesale Unlocked Cell Phone"
		case "Full Body Protectors" : singularSEO = "Invisible Film Protector"
		case else : singularSEO = val
	end select
end function

if SEtitle = "" then SEtitle = "Wholesale Cell Phone Accessories ?Wholesale Cellular Phone Accessories ?CellularOutfitter.com"
if SEdescription = "" then SEdescription = "Buy Cheap Cell Phone Accessories at Wholesale Prices to the Public. Find the latest Discount Cellular Phone Accessories for Motorola, LG, Nokia, Samsung phones and more. Cellular Accessories at the Lowest Prices Online Guaranteed."
if SEkeywords = "" then SEkeywords = "cheap cell phone accessories, cell phone accessories, discount cellular phone accessories, cellular phone accessories"

REWRITE_URL = Request.ServerVariables("HTTP_X_REWRITE_URL")
if inStr(Request.ServerVariables("HTTP_X_REWRITE_URL"),"?") > 0 then 
	REWRITE_URL = left(Request.ServerVariables("HTTP_X_REWRITE_URL"),inStr(Request.ServerVariables("HTTP_X_REWRITE_URL"),"?")-1)
end if

if Request.ServerVariables("HTTPS") = "on" then
	canonicalURL = "https://" & Request.ServerVariables("SERVER_NAME") & REWRITE_URL
else
	canonicalURL = "http://" & Request.ServerVariables("SERVER_NAME") & REWRITE_URL
end if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><%=SEtitle%></title>
<meta name="Description" content="<%=SEdescription%>">
<meta name="Keywords" content="<%=SEkeywords%>">
<meta name="alternate" content="http://m.cellularoutfitter.com<%=request.ServerVariables("HTTP_X_REWRITE_URL")%>">
<link rel="canonical" href="<%=lcase(canonicalURL)%>" />
<%if pageTitle = "Home" then%>
<meta name="robots" content="index,follow">
<!-- Cell Phone Accessories at CellularOutfitter.com: Motorola Cell Phone Accessories, Nokia Cell Phone Accessories, LG Cell Phone Accessories, Samsung Cell Phone Accessories -->
<meta name="verify-v1" content="KDSemcIPOLKERRNLdtodRPug14ThOrk3fAAECAAzt/U=" />
<meta name="verify-v1" content="7oJ6qXmQ/MOO9Wics9IknbH0gaHI9z5Tl3dH82NBhO8=" />
<%end if%>
<link href="/includes/css/styleCO.css<%=getCssDateParam("/includes/css/styleCO.css")%>" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="https://www.cellularoutfitter.com/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="https://www.cellularoutfitter.com/images/favicon.ico" type="image/x-icon">
<script src="https://seal.buysafe.com/private/rollover/rollover.js" type="text/javascript" language="javascript" charset="utf-8"></script>
<script language="javascript">
function addbookmark() {
	var IE = navigator.appName.match(/(Microsoft Internet Explorer)/gi),
	NS = navigator.appName.match(/(Netscape)/gi),
	OP = navigator.appName.match(/(Opera)/gi),
	BK = document.getElementById('bookmark');
	BK.onmouseout = function() {
		window.status = '';
	}
	if(IE && document.uniqueID) {
		var bookmarkurl = "/";
		var bookmarktitle = "Cellular Outfitter";
		window.external.AddFavorite(bookmarkurl,bookmarktitle);
	}
	else if(OP || IE && !document.uniqueID) {
		alert('Your browser requires that you\nPress Ctrl & D to Bookmark this page.');
	}
	else if(NS) {
		alert('Your browser requires that you\nPress Ctrl & D to Bookmark this page.');
	}
	else { BK.innerHTML = '' }
}
</script>
<!-- AdSense Start -->
<script src="http://www.google.com/jsapi"></script>
<script type="text/javascript" charset="utf-8">
google.load('ads.search', '2');
</script>
<!-- AdSense End -->
<!--#include virtual="/includes/template/inc_HeadTagEnd.asp"-->
</head>

<body class="body">
	<% call printPIxel(2) %>
<!-- Google AdSense Start -->
<script type="text/javascript" charset="utf-8">
var pageOptions = {
'pubId' : 'pub-1001075386636876',
<% if len(brandName) > 0 then %>
	<% if len(modelName) > 0 then %>
		<% if len(categoryName) > 0 then %>
'query' : '<%=brandName%>' + ' ' + '<%=modelName%>' + ' ' + '<%=categoryName%>'
		<% else %>
'query' : '<%=brandName%>' + ' ' + '<%=modelName%>'
		<% end if %>
	<% else %>
		<% if len(categoryName) > 0 then %>
'query' : '<%=brandName%>' + ' ' + '<%=categoryName%>'
		<% else %>
'query' : '<%=brandName%>'
		<% end if %>
	<% end if %>
};
<% elseif len(categoryName) > 0 then %>
'query' : '<%=categoryName%>'
};
<% else %>
'query' : 'Cell Phones'
};
<% end if %>
var adblock1 = {
'container' : 'adcontainer1',
'number' : 3,
'width' : 'auto',
'lines' : 2,
'fontFamily' : 'arial',
'fontSizeTitle' : '14px',
'fontSizeDescription' : '14px',
'fontSizeDomainLink' : '14px',
'linkTarget' : '_blank'
};
var adblock2 = {
'container' : 'adcontainer2',
'number' : 4,
'width' : '200px',
'lines' : 3,
'fontFamily' : 'arial',
'fontSizeTitle' : '12px',
'fontSizeDescription' : '12px',
'fontSizeDomainLink' : '12px',
'linkTarget' : '_blank'
};
new google.ads.search.Ads(pageOptions, adblock1, adblock2);
</script>
<!-- Google AdSense Close -->
<% homepage = 1 %>
<% if betaView = 0 then %>
<!--#include virtual="/includes/template/topHTML.asp"-->
<% else %>
<!--#include virtual="/includes/template/topHTML-new.asp"-->
<% end if %>