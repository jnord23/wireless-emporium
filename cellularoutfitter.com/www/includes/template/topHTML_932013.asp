<%
mySession = prepInt(request.cookies("mySession"))

sql = 	"select isnull(sum(a.qty), 0) totalQty, isnull(sum(b.price_co * a.qty), 0) totalPrice1, isnull(sum(c.price_co * a.qty), 0) totalPrice2 " & vbcrlf & _
		"from shoppingcart a left join we_items b on a.itemid=b.itemid left join we_items_musicSkins c on a.itemID = c.id " & vbcrlf & _
		"where a.store = 2 and a.sessionid = '" & mySession & "' and (a.purchasedorderid is null or a.purchasedorderid = 0)" & vbcrlf

call fOpenConn()
set objRsMiniCart = oConn.execute(sql)

if objRsMiniCart.EOF then
	miniTotalQuantity = 0
	miniSubTotal = 0
else
	miniTotalQuantity = formatnumber(objRsMiniCart("totalQty"), 0)
	miniSubTotal = formatcurrency(objRsMiniCart("totalPrice1") + objRsMiniCart("totalPrice2"))
end if

dim brandList : brandList = ""
sql = 	"select	brandid, brandName, 0 phoneOnly" & vbcrlf & _
		"from	we_brands" & vbcrlf & _
		"where	brandid in (1,2,3,4,5,6,7,9,10,11,14,16,17,18,19,20,28,29,30)" & vbcrlf & _
		"order by brandname"
set rsBrandFinder = oConn.execute(sql)
do until rsBrandFinder.eof
	brandList = brandList & rsBrandFinder("brandID") & "##" & rsBrandFinder("brandName") & "##"
	rsBrandFinder.movenext
loop
brandList = left(brandList,len(brandList)-2)
brandListArray = split(brandList,"##")

dim modelList : modelList = ""
if prepInt(brandID) > 0 then
	sql = 	"select	modelid, modelName" & vbcrlf & _
			"from	we_models" & vbcrlf & _
			"where	brandid = " & brandID & " " & vbcrlf & _
			"order by modelName"
	set rsModelFinder = oConn.execute(sql)
	do until rsModelFinder.eof
		modelList = modelList & rsModelFinder("modelid") & "##" & rsModelFinder("modelName") & "##"
		rsModelFinder.movenext
	loop
	modelList = left(modelList,len(modelList)-2)
end if
modelListArray = split(modelList,"##")

dim categoryList : categoryList = ""
if prepInt(modelID) > 0 then
	sql = 	"select distinct a.typeID, b.typeName_co " & vbcrlf & _
			"from we_ItemsSiteReady a " & vbcrlf & _
				"left join we_Types b on a.typeID = b.typeID " & vbcrlf & _
			"where modelID = " & modelID & " " & vbcrlf & _
			"order by b.typeName_co"
	set rsCatFinder = oConn.execute(sql)
	do until rsCatFinder.eof
		categoryList = categoryList & rsCatFinder("typeID") & "##" & rsCatFinder("typeName_co") & "##"
		rsCatFinder.movenext
	loop
	categoryList = left(categoryList,len(categoryList)-2)
end if
categoryListArray = split(categoryList,"##")
%>
<script>
	function onFinder(finderType, o) {
		if (finderType == "b") {
			ajax('/ajax/accessoryFinder.asp?brandid=' + o.value, 'cbModelBox');
			ajax('/ajax/accessoryFinder.asp?modelid=-1', 'cbCatBox');
		}
		else if (finderType == "m") {
			ajax('/ajax/accessoryFinder.asp?modelid=' + o.value, 'cbCatBox');			
		} 
	}
	
	function onFinder2(finderType, o) {
		if (finderType == "b") {
			ajax('/ajax/accessoryFinder.asp?version=b&brandid=' + o.value, 'cbModelBox2');
			ajax('/ajax/accessoryFinder.asp?version=b&modelid=-1', 'cbCatBox2');
		}
		else if (finderType == "m") {
			ajax('/ajax/accessoryFinder.asp?version=b&modelid=' + o.value, 'cbCatBox2');			
		} 
	}
	
	function jumpTo() {
		var brandid = document.frmFinder.cbBrand.value;
		var modelid = document.frmFinder.cbModel.value;
		var categoryid = document.frmFinder.cbCat.value;
		var brandName = ""
		var modelName = ""
		var categoryName = ""
		var strLink = ""
		
		if (brandid != "" && modelid != "" && categoryid != "") {
			brandName = document.frmFinder.cbBrand.options[document.frmFinder.cbBrand.selectedIndex].text;
			modelName = document.frmFinder.cbModel.options[document.frmFinder.cbModel.selectedIndex].text;
			categoryName = document.frmFinder.cbCat.options[document.frmFinder.cbCat.selectedIndex].text;
			strLink = "/sb-" + brandid + "-sm-" + modelid + "-sc-" + categoryid + "-" + brandName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + modelName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + categoryName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + ".html"
			window.location = strLink;
		}
		else if (brandid != "" && modelid != "") {
			brandName = document.frmFinder.cbBrand.options[document.frmFinder.cbBrand.selectedIndex].text;
			modelName = document.frmFinder.cbModel.options[document.frmFinder.cbModel.selectedIndex].text;
			strLink = "/m-" + modelid + "-" + brandName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + modelName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-cell-phone-accessories.html"
			window.location = strLink;
		}
	}
	
	function jumpTo2() {
		var brandid = document.frmFinder2.cbBrand.value;
		var modelid = document.frmFinder2.cbModel.value;
		var categoryid = document.frmFinder2.cbCat.value;
		var brandName = ""
		var modelName = ""
		var categoryName = ""
		var strLink = ""
		
		if (brandid != "" && modelid != "" && categoryid != "") {
			brandName = document.frmFinder2.cbBrand.options[document.frmFinder2.cbBrand.selectedIndex].text;
			modelName = document.frmFinder2.cbModel.options[document.frmFinder2.cbModel.selectedIndex].text;
			categoryName = document.frmFinder2.cbCat.options[document.frmFinder2.cbCat.selectedIndex].text;
			strLink = "/sb-" + brandid + "-sm-" + modelid + "-sc-" + categoryid + "-" + brandName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + modelName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + categoryName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + ".html"
			window.location = strLink;
		}
		else if (brandid != "" && modelid != "") {
			brandName = document.frmFinder2.cbBrand.options[document.frmFinder2.cbBrand.selectedIndex].text;
			modelName = document.frmFinder2.cbModel.options[document.frmFinder2.cbModel.selectedIndex].text;
			strLink = "/m-" + modelid + "-" + brandName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-" + modelName.replace(/[^a-zA-Z 0-9]|\s+/g,'-') + "-cell-phone-accessories.html"
			window.location = strLink;
		}
	}
</script>
<link rel="stylesheet" type="text/css" href="/includes/css/account_loginMenu.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:600,400|Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
<div id="popCover" style="height:100%; display:none; width:100%; background-color:#333; position:fixed; left:0px; top:0px; z-index:3000; opacity:0.6; filter:alpha(opacity=60);">&nbsp;</div>
<div id="popBox" style="height:2000px; display:none; width:100%; position:fixed; left:0px; top:0px; z-index:3001; margin:auto; text-align:center;"></div>
<div id="topSelect"></div>
<div id="siteTopBar" style="width:100%; height:28px; background-image:url(/images/backgrounds/top-bar-dblue.png);">
	<div style="width:1040px; height:28px; margin-left:auto; margin-right:auto;">
        <div style="float:right; background-image:url(/images/backgrounds/top-bar-green.png); padding:0px 20px 0px 20px; height:28px;">
            <a href="/cart/basket.asp">
            <div style="float:left" id="cartIcon"></div>
            <div id="siteTop-CartBar" style="float:left; color:#FFF; font-weight:bold; font-size:10px; padding:7px 10px 0px 10px;"><%=miniTotalQuantity%> Items - <%=formatCurrency(miniSubTotal,2)%></div>
            <div style="float:left"><div id="cardDownArrow"></div></div>
            </a>
        </div>
        <div id="account_nav"></div>
        <div style="float:right;"><div id="topMenuBr"></div></div>
        <div style="float:right; font-weight:bold; padding:7px 20px 0px 20px;"><a href="/contact-us.html" style="color:#FFF; font-size:10px;">Contact Us</a></div>
        <div style="float:right;"><div id="topMenuBr"></div></div>
        <div style="float:right; font-weight:bold; padding:7px 20px 0px 20px;"><a href="/faq.html" style="color:#FFF; font-size:10px;">FAQs</a></div>
        <div style="float:right;"><div id="topMenuBr"></div></div>
        <div style="float:right; font-weight:bold; padding:7px 20px 0px 20px;"><a href="/track-your-order.html" style="color:#FFF; font-size:10px;">Order Status</a></div>
        <div style="float:right;"><div id="topMenuBr"></div></div>
        <div style="float:right; height:28px; overflow:hidden;">
            <script type="text/javascript">
                var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                var lhnid = 3410;
                var lhnwindow = 6755;
                var lhnImage = "<div id='lhnchatimg' title='Live Help'></div>";
                document.write(unescape("%3Cscript src='/framework/utility/liveHelp.js' type='text/javascript'%3E%3C/script%3E"));
            </script>
        </div>
	</div>
</div>
<div id="siteTopBar_b" style="width:100%; height:28px; background-color:#124f80;">
	<div style="width:1040px; height:28px; margin-left:auto; margin-right:auto;">
    	<div id="account_nav_b"></div>
        <div class="fr topCartBox">
            <a href="/cart/basket.asp">
            <div class="fl cartIcon_02"></div>
            <div id="siteTop-CartBar" class="topCartText"><%=miniTotalQuantity%> Items - <%=formatCurrency(miniSubTotal,2)%></div>
            </a>
        </div>
        <div class="fr topBarLinkBox"><a href="/faq.html" class="topBarLink">FAQs</a></div>
        <div class="fr topBarLinkBox"><a href="/track-your-order.html" class="topBarLink">Order Status</a></div>
        <div class="fr liveChatBox topBoxLine">
            <script type="text/javascript">
                var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
                var lhnid = 3410;
                var lhnwindow = 6755;
                var lhnImage = "<div class='lhnchatimg_b' title='Live Help'><div class='fl livehelpIcon'></div><div class='fl topBarLink'>Live Chat</div></div>";
                document.write(unescape("%3Cscript src='/framework/utility/liveHelp.js' type='text/javascript'%3E%3C/script%3E"));
            </script>
        </div>
    </div>
</div>
<table width="1020" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF" style="border-left:1px solid #ccc; border-right:1px solid #ccc;">
    <tr>
        <td id="siteTopBar2" valign="top" style="padding:5px 10px 0px 10px;">
            <div id="logoLine_a" style="float:left; width:1020px;">
            	<div style="float:left; width:250px; height:60px;">
                	<%
					if (month(date) = 11 and day(date) > 22) or (month(date) = 12 and day(date) < 26) then
					%>
                    <a href="/"><img src="/images/holidayLogos/co-xmas.png" alt="CellularOutfitter.com" title="CellularOutfitter.com" border="0" /></a>
                    <%
					else
					%>
                    <a href="/"><div id="logo" title="CellularOutfitter.com"></div></a>
                    <%
					end if
					%>
					
                </div>
                <div style="float:right; width:750px; padding-top:5px;">
                	<div style="float:left; width:750px;">
                    	<div id="mvt_newTopHeader1" style="margin-left:10px; float:right;">
                            <div style="float:right; padding-left:5px;">
                                <a href="javascript:Open_Popup('<%=strHTTP%>://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.CELLULAROUTFITTER.COM&lang=en','VeriSign','width=560,height=500,location=yes,status=yes,resizable=yes,scrollbars=yes');"><div id="verisignLogo" title="CELLULAR OUTFITTER, INC. has been verified by Verisign"></div></a>
                            </div>
							<%if now >= cdate("4/9/2013") and now < cdate("4/23/2013") then%>
                        	<a href="/april-deals"><div style="float:right; margin-right:5px; background:url(/images/banners/co_mini_banner_april.jpg) no-repeat; width:176px; height:47px;"></div></a>
                            <%else%>
                        	<div id="topHeaderUVP1_3" style="border-right:2px dotted #ccc; padding-right:5px; margin-right:5px;"></div>                            
                            <%end if%>
                        	<a href="/shipping-policy.html"><div id="topHeaderUVP1_2" style="border-right:2px dotted #ccc; padding-right:5px; margin-right:5px;"></div></a>
                        	<a href="/lowest-price.html"><div id="topHeaderUVP1_1" style="border-right:2px dotted #ccc; padding-right:5px; margin-right:5px;"></div></a>
                        </div>
						<!--#include virtual="/includes/asp/inc_socialboost.asp"-->
                    </div>
                </div>
            </div>
            <div id="logoLine_b" class="fl fullWidth">
            	<a href="/"><div class="fl coLogo_02"></div></a>
                <div class="fl searchBoxFull">
                	<form name="topSearch" method="get" action="/search/search.asp" style="margin:0px; padding:0px;">
                    <div class="fl searchBoxInner">
	                	<div class="fl searchIcon"></div>
    	                <div class="fl searchInputBox">
                        	<input type="text" name="search" value="What are you looking for?" class="searchInput" onfocus="this.value='';" />
                            <input type="hidden" name="sitesearch" value="Y" />
                        </div>
        	            <div class="fr searchGo" onclick="document.topSearch.submit();">GO!</div>
                    </div>
                    </form>
                </div>
                <a href="/lowest-price.html"><div class="fl lowPriceVP_small"></div></a>
                <a href="/shipping-policy.html"><div class="fl returnsVP_small"></div></a>
                <div style="float:right; padding-left:5px; margin-top:15px;">
                	<a href="javascript:Open_Popup('<%=strHTTP%>://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=WWW.CELLULAROUTFITTER.COM&lang=en','VeriSign','width=560,height=500,location=yes,status=yes,resizable=yes,scrollbars=yes');"><div id="verisignLogo" title="CELLULAR OUTFITTER, INC. has been verified by Verisign"></div></a>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding:5px 10px 0px 10px;">
        	<div id="topNav">
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:70px;">
					<a href="/c-3-cell-phone-covers-and-skins.html" title="Cell Phone Covers & Gel Skins" class="cat-font2">Covers & Gel Skins</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:80px;">
					<a href="/c-7-cell-phone-cases-pouches.html" title="Cell Phone Cases & Pouches" class="cat-font2">Cases & Pouches</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:100px;">
					<a href="/c-18-cell-phone-screen-protectors.html" title="Cell Phone Screen Protectors" class="cat-font2">Screen Protectors</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:80px;">
					<a href="/c-24-cell-phone-custom-cases.html" title="Cell Phone Custom Cases" class="cat-font2">Custom<br />Cases</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:90px;">
					<a href="/c-2-cell-phone-chargers-and-data-cables.html" title="Cell Phone Chargers & Data Cables" class="cat-font2">Chargers & Data Cables</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:14px 5px 0px 5px; width:80px;">
					<a href="/c-1-cell-phone-batteries.html" title="Cell Phone Batteries" class="cat-font2">Batteries</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:90px;">
					<a href="/c-5-cell-phone-hands-free-kits-bluetooth-headsets.html" title="Cell Phone Hands-Free Kits & Headsets" class="cat-font2">Bluetooth & Hands-Free</a>
                </div>
                <div id="topNav-Div"></div>
            	<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:80px;">
					<a href="/c-6-cell-phone-holsters-car-mounts.html" title="Cell Phone Holsters & Car Mounts" class="cat-font2">Holsters & Car Mounts</a>
                </div>
                <div id="topNav-Div"></div>
				<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:90px;">
					<a href="/c-8-cell-phone-other-accessories.html" title="Cell Phone Other Accessories" class="cat-font2">Other Accessories</a>
                </div>
                <div id="topNav-Div"></div>
				<div style="float:left; text-align:center; padding:7px 5px 0px 5px; width:100px;">
					<a href="/wholesale-cell-phones.html" title="Wholesale Cell Phones" class="cat-font2">Wholesale Cell Phones</a>
                </div>
            </div>
            <div id="bVersion_topNav" class="topNav_b">
            	<div class="fl topNavBase" style="width:58px;">
					<a href="/c-3-cell-phone-covers-and-skins.html" title="Cell Phone Covers & Gel Skins" class="topNav_b_link">Covers</a>
                </div>
                <div class="fl topNavBase" style="width:40px;">
					<a href="/c-7-cell-phone-cases-pouches.html" title="Cell Phone Cases & Pouches" class="topNav_b_link">Cases</a>
                </div>
                <div class="fl topNavBase" style="width:130px;">
					<a href="/c-18-cell-phone-screen-protectors.html" title="Cell Phone Screen Protectors" class="topNav_b_link">Screen Protectors</a>
                </div>
                <div class="fl topNavBase" style="width:125px;">
					<a href="/c-2-cell-phone-chargers-and-data-cables.html" title="Cell Phone Chargers & Data Cables" class="topNav_b_link">Chargers & Cables</a>
                </div>
                <div class="fl topNavBase" style="width:65px;">
					<a href="/c-1-cell-phone-batteries.html" title="Cell Phone Batteries" class="topNav_b_link">Batteries</a>
                </div>
                <div class="fl topNavBase" style="width:165px;">
					<a href="/c-5-cell-phone-hands-free-kits-bluetooth-headsets.html" title="Cell Phone Hands-Free Kits & Headsets" class="topNav_b_link">Bluetooth & Hands-Free</a>
                </div>
                <div class="fl topNavBase" style="width:135px;">
					<a href="/c-6-cell-phone-holsters-car-mounts.html" title="Cell Phone Holsters & Car Mounts" class="topNav_b_link">Holsters & Mounts</a>
                </div>
                <div class="fl topNavBase" style="width:135px;">
					<a href="/wholesale-cell-phones.html" title="Wholesale Cell Phones" class="topNav_b_link">Wholesale Phones</a>
                </div>
                <div class="fl topNavBase2" style="width:60px;">
					<a href="/c-8-cell-phone-other-accessories.html" title="Cell Phone Other Accessories" class="topNav_b_link">Others</a>
                </div>
            </div>
            <div id="topAccessoryFinder">
            	<form name="frmFinder">
            	<div style="float:left; color:#fff; padding-top:15px; font-size:20px; font-family:Verdana, Geneva, sans-serif; font-weight:bold; text-align:center; width:300px;">FIND YOUR ACCESSORY:</div>
            	<div style="float:left; width:200px; padding-top:20px; text-align:center;">
                	<select name="cbBrand" style="width:180px;" onchange="onFinder('b',this);">
                    	<option value="">1.) Your Phone Brand</option>
                        <%
						for i = 0 to ubound(brandListArray)
							curBrandID = prepInt(brandListArray(i))
							i = i + 1
							curBrandName = brandListArray(i)
						%>
						<option value="<%=curBrandID%>"<% if prepInt(brandID) = curBrandID then %> selected="selected"<% end if %>><%=curBrandName%></option>
						<%
						next
						%>
                        <option value="15"<% if prepInt(brandID) = 15 then %> selected="selected"<% end if %>>Other Brands</option>
                    </select>
                </div>
            	<div id="cbModelBox" style="float:left; width:200px; padding-top:20px; text-align:center;">
                	<select name="cbModel" style="width:180px;" onchange="onFinder('m',this);">
                    	<option value="">2.) Your Phone Model</option>
                        <%
						for i = 0 to ubound(modelListArray)
							curModelID = prepInt(modelListArray(i))
							i = i + 1
							curModelName = modelListArray(i)
						%>
						<option value="<%=curModelID%>"<% if prepInt(modelID) = curModelID then %> selected="selected"<% end if %>><%=curModelName%></option>
						<%
						next
						%>
                    </select>
                </div>
            	<div id="cbCatBox" style="float:left; width:200px; padding-top:20px; text-align:center;">
                	<select name="cbCat" style="width:180px;">
                    	<option value="">3.) Accessory Category</option>
                        <%
						for i = 0 to ubound(categoryListArray)
							curCategoryID = prepInt(categoryListArray(i))
							i = i + 1
							curCategoryName = categoryListArray(i)
						%>
						<option value="<%=curCategoryID%>"<% if prepInt(categoryID) = curCategoryID or prepInt(typeID) = curCategoryID then %> selected="selected"<% end if %>><%=curCategoryName%></option>
						<%
						next
						%>
                    </select>
                </div>
            	<div style="float:left; width:120px; padding-top:18px; text-align:right;">
					<a href="javascript:jumpTo();"><div id="topBtnSearch"></div></a>
                </div>
                </form>
            </div>
            <div id="topAccessoryFinder_b" class="finder_versionB">
            	<form name="frmFinder2">
                <div class="fl finderTxt">Find accessories for your phone:</div>
                <div class="fl finderBox">
                	<div class="fl finderNum">1.</div>
                    <div class="fl">
                    	<select class="finderSelectObj" name="cbBrand" style="width:140px;" onchange="onFinder2('b',this);">
                            <option value="">Your Phone Brand</option>
                            <%
                            for i = 0 to ubound(brandListArray)
								curBrandID = prepInt(brandListArray(i))
								i = i + 1
								curBrandName = brandListArray(i)
                            %>
                            <option value="<%=curBrandID%>"<% if prepInt(brandID) = curBrandID then %> selected="selected"<% end if %>><%=curBrandName%></option>
                            <%
                            next
                            %>
                            <option value="15"<% if prepInt(brandID) = 15 then %> selected="selected"<% end if %>>Other Brands</option>
                        </select>
                    </div>
                </div>
                <div class="fl finderBox">
                	<div class="fl finderNum">2.</div>
                    <div id="cbModelBox2" class="fl">
                    	<select class="finderSelectObj" name="cbModel" style="width:140px;" onchange="onFinder2('m',this);">
                            <option value="">Your Phone Model</option>
                            <%
                            for i = 0 to ubound(modelListArray)
                                curModelID = prepInt(modelListArray(i))
                                i = i + 1
                                curModelName = modelListArray(i)
                            %>
                            <option value="<%=curModelID%>"<% if prepInt(modelID) = curModelID then %> selected="selected"<% end if %>><%=curModelName%></option>
                            <%
                            next
                            %>
                        </select>
                    </div>
                </div>
                <div class="fl finderBox">
                	<div class="fl finderNum">3.</div>
                    <div id="cbCatBox2" class="fl">
                    	<select class="finderSelectObj" name="cbCat" style="width:150px;">
                            <option value="">Accessory Category</option>
                            <%
                            for i = 0 to ubound(categoryListArray)
                                curCategoryID = prepInt(categoryListArray(i))
                                i = i + 1
                                curCategoryName = categoryListArray(i)
                            %>
                            <option value="<%=curCategoryID%>"<% if prepInt(categoryID) = curCategoryID or prepInt(typeID) = curCategoryID then %> selected="selected"<% end if %>><%=curCategoryName%></option>
                            <%
                            next
                            %>
                        </select>
                    </div>
                </div>
                <div class="fl finderBttn" onclick="jumpTo2()">
                	<div class="fl finderBttnTxt">Search</div>
                    <div class="fl finderBttnArrow"></div>
                </div>
                </form>
            </div>
            <form name="bigSearch" method="get" action="/search/search.asp" style="margin:0px; padding:0px;">
            <div id="topAccessoryFinder_alt">
            	<div class="fl bigSearchTxt">Search:</div>
                <div class="fl bigSearchBox">
                	<input type="text" name="search" value="What are you looking for?" class="searchInputBig" onfocus="this.value='';" />
                    <input type="hidden" name="sitesearch" value="Y2" />
                </div>
                <div class="fl bigSearchBttn" onclick="document.bigSearch.submit();"></div>
            </div>
            </form>
        </td>
    </tr>
    <tr>
        <td style="padding:0px 10px 0px 10px;">
        	<%
			if isnull(noLeftSide) or len(noLeftSide) < 1 or not isnumeric(noLeftSide) then noLeftSide = 0
			if noLeftSide = 0 then
			%>
            <div id="sideNav" style="float:left;">
            	<% if prepInt(useLeftNav2) = 1 then %>
                <!--#include virtual="/includes/template/leftnav2.asp"-->
				<% else %>
				<!--#include virtual="/includes/template/leftnav.asp"-->
                <% end if %>
            </div>
            <div id="mainContent" style="padding-left:15px; float:left; width:800px;">
            <% end if %>