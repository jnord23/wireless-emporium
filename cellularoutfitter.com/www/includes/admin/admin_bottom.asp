</body>
</html>
<%
call addTracking(gblTrackingID, 2, "TRACKING END")

if isObject(oConn) then
	oConn.close
	set oConn = nothing
end if
%>
<script>
	var useHttps = <%=prepInt(securePage)%>;
	var winLoc = window.location.toString();
	winLoc = winLoc.toLowerCase();
	
	if (useHttps == 1 && winLoc.indexOf("https:") < 0) {
		window.location = winLoc.replace("http:","https:")
	}
	else if (useHttps == 0 && winLoc.indexOf("http:") < 0) {
		window.location = winLoc.replace("https:","http:")
	}
</script>