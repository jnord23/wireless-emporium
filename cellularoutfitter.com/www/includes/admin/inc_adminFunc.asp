<!-- METADATA TYPE="typelib" NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<%
function getTrackingID()
	Dim cmdTrack
	Set cmdTrack = Server.CreateObject("ADODB.Command")
	Set cmdTrack.ActiveConnection = oConn
	cmdTrack.CommandText = "getnextsequence"
	cmdTrack.CommandType = adCmdStoredProc 
	
	cmdTrack.Parameters.Append cmdTrack.CreateParameter("ret", adInteger, adParamReturnValue)
	cmdTrack.Parameters.Append cmdTrack.CreateParameter("p_sequenceid", adInteger, adParamInput)
	cmdTrack.Parameters.Append cmdTrack.CreateParameter("p_out_nextvalue", adInteger, adParamOutput)
	cmdTrack.Parameters.Append cmdTrack.CreateParameter("p_token", adInteger, adParamInput)
	cmdTrack("p_sequenceid")	= 1
	cmdTrack.Execute

	getTrackingID = cmdTrack("p_out_nextvalue")
end function


sub addTracking(trackingID, storeID, pSql)
	dim tUrl, tUserName, frmPost, frmGet, tPageName
	frmPost	=	""
	frmGet	=	""

	tUrl 		= 	request.ServerVariables("SERVER_NAME") & request.ServerVariables("SCRIPT_NAME")
	tUserName	=	request.Cookies("username")
	
	'======= if request.form is too large it cannot be used
	frmPost 	=	Request.TotalBytes
	frmGet		=	left(request.QueryString, 7999)
	tPageName	=	pageTitle

	if "" = tUserName 		then tUserName = request.Cookies("fname") & " " & request.Cookies("lname") end if
	if "" = trim(tUserName) then tUserName = trim(request.Cookies("admin_CO")("fname")) & " " & trim(request.Cookies("admin_CO")("lname")) end if
	if "" = tPageName 		then tPageName = pageName end if
	if "" = pSql			then pSql = "session(errorSQL):" & session("errorSQL") end if

	if "" <> trackingID and "" <> storeID then

		Dim cmdAdd
		Set cmdAdd = Server.CreateObject("ADODB.Command")
		Set cmdAdd.ActiveConnection = oConn
		cmdAdd.CommandText = "addWebTracking"
		cmdAdd.CommandType = adCmdStoredProc
		
		cmdAdd.Parameters.Append cmdAdd.CreateParameter("ret", adInteger, adParamReturnValue)
		cmdAdd.Parameters.Append cmdAdd.CreateParameter("trackingID", adInteger, adParamInput)
		cmdAdd.Parameters.Append cmdAdd.CreateParameter("store", adInteger, adParamInput)
		cmdAdd.Parameters.Append cmdAdd.CreateParameter("sessionid", adInteger, adParamInput)
		cmdAdd.Parameters.Append cmdAdd.CreateParameter("pageName", adVarChar, adParamInput, 1000)
		cmdAdd.Parameters.Append cmdAdd.CreateParameter("url", adVarChar, adParamInput, 8000)
		cmdAdd.Parameters.Append cmdAdd.CreateParameter("userName", adVarChar, adParamInput, 100)
		cmdAdd.Parameters.Append cmdAdd.CreateParameter("sql", adVarChar, adParamInput, 8000)
		cmdAdd.Parameters.Append cmdAdd.CreateParameter("formData_post", adVarChar, adParamInput, 8000)
		cmdAdd.Parameters.Append cmdAdd.CreateParameter("formData_get", adVarChar, adParamInput, 8000)
		cmdAdd.Parameters.Append cmdAdd.CreateParameter("execTime", adDBTimeStamp, adParamInput)

		cmdAdd("trackingID")	=	trackingID
		cmdAdd("store")			=	storeID
		cmdAdd("sessionid")		=	session.SessionID
		cmdAdd("pageName")		=	tPageName
		cmdAdd("url")			=	tUrl
		cmdAdd("userName")		=	tUserName
		cmdAdd("sql")			=	pSql
		cmdAdd("formData_post")	=	frmPost
		cmdAdd("formData_get")	=	frmGet
		cmdAdd("execTime")		=	now
																		
		cmdAdd.Execute
		
	end if
end sub

function getDbRows(pSql)
	Dim objRs	:	Set objRs = Server.CreateObject("ADODB.RecordSet")
	Dim tRet	:	tRet = NULL
	
	'CursorType : adOpenForwardOnly 0 , LockType adLockReadOnly 1
	objRs.Open pSql, oConn, 0, 1

	If objRs.EOF Then
		tRet = NULL
	Else
		tRet = objRs.GetRows()		
	End If

	objRs.Close : Set objRs = Nothing

	getDbRows = tRet
end function

sub reshipColorPicker(byref arr, reason, byref pBgColor, byref pTxtColor)
	pBgColor	= "#FFFFFF"
	pTxtColor	= "#000000"

	if not isnull(arr) then
		for i=0 to ubound(arr,1)
			if lcase(arr(i,0)) = lcase(reason) then
				pBgColor = arr(i,1)
				pTxtColor = arr(i,2)
				exit for
			end if
		next
	end if

end sub
%>