<!--#include virtual="/includes/asp/inc_basepage.asp"-->
<%
dim myServer
myServer = request.servervariables("SERVER_NAME")

if prepInt(Request.Cookies("admin_CO")("adminID")) > 0 then session("adminID") = prepInt(Request.Cookies("admin_CO")("adminID"))

'ADMINSECURE
if pageTitle = "CO Admin Login" then
	if Request.Cookies("admin_CO")("adminUser") = "True" and Request.Cookies("admin_CO")("adminID") <> "" then
		if Guest = true and GuestAllowed = "" then
			if inStr(myServer,"staging.") > 0 then
				response.redirect "http://staging.cellularoutfitter.com/admin/db_update_links.asp"
			else
				response.redirect "http://www.cellularoutfitter.com/admin/db_update_links.asp"
			end if
		else
			if inStr(myServer,"staging.") > 0 then
				response.redirect "http://staging.cellularoutfitter.com/admin/menu.asp"
			else
				response.redirect "http://www.cellularoutfitter.com/admin/menu.asp"
			end if
		end if
	end if
else
	if skipLogin <> 1 then
		if Request.Cookies("admin_CO")("adminUser") = "True" then
			if Guest = true and GuestAllowed = "" then
				response.redirect "db_update_links.asp"
			elseif Request.Cookies("admin_CO")("adminID") = "" then
				if inStr(myServer,"staging.") > 0 then
					response.redirect "http://staging.cellularoutfitter.com/admin/default.asp?show=mustlogin"
				else
					response.redirect "http://www.cellularoutfitter.com/admin/default.asp?show=mustlogin"
				end if
			end if
		else
			if inStr(myServer,"staging.") > 0 then
				response.redirect "http://staging.cellularoutfitter.com/admin/default.asp?show=mustlogin"
			else
				response.redirect "http://www.cellularoutfitter.com/admin/default.asp?show=mustlogin"
			end if
		end if
	end if
end if
%>
<!--#include virtual="/includes/admin/inc_adminFunc.asp"-->
<%
dim gblTrackingID : gblTrackingID = getTrackingID()
call addTracking(gblTrackingID, 2, "TRACKING START")
%>
<html>
<head>
<title><%="CO ADMIN :: " & pageTitle%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="robots" content="noindex, nofollow">
<link href="http://www.cellularoutfitter.com/includes/css/styleCO.css" rel="stylesheet" type="text/css">
</head>

<%
if strBody <> "" then
	response.write strBody & vbcrlf
elseif pageTitle = "CO Admin Login" then
	response.write "<body onLoad=""document.login.userid.focus();"">" & vbcrlf
else
	response.write "<body>" & vbcrlf
end if
%>

<%if header = 1 then%>
	<table width="85%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td width="120">
				<font face="Verdana, Arial, Helvetica, sans-serif" size="2">
					<a href="http://www.cellularoutfitter.com/admin/menu.asp"><img src="http://www.cellularoutfitter.com/images/CellularOutfitter.jpg" alt="CellularOutfitter.com" width="320" height="90" border="0"></a>
				</font>
                <br />
                <a href="/">Return to Public Site</a>
			</td>
			<td valign="bottom" align="right">
				<font face="Verdana, Arial, Helvetica, sans-serif" size="2">
					<b>:: // Welcome to CO.com Admin Pages<br></b>
				</font>
				<hr noshade width="350" size="1" align="right">
				<font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#FF6600">
					<%
					if pageTitle = "CO Admin Login" then
						%>
						Please Login
						<%
					else
						%>
						<a href="http://www.cellularoutfitter.com/admin/signout.asp">Logout</a>
						<%
					end if
					%>
				</font>
				<br><br>
			</td>
		</tr>
	</table>
<%end if%>
