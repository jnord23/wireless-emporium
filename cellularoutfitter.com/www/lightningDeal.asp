<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	noLeftSide = 1
%>
<!--#include virtual="/includes/template/top.asp"-->
<%
	notFound = "Check back soon."
	forceRefresh = false 'force reload to correct errors, etc 'make this true for a period of time greater than or equal to the refresh period, then turn it back off
	
	projID = prepStr(request.QueryString("id"))
	if prepStr(request.Querystring("bypass")) = "1" then
		bypass = true
	else
		bypass = false
	end if
	
	if projID <> "" then
	
		sql =	"select		* " & vbcrlf &_
				"from		lightningDeal_Projects " & vbcrlf &_
				"where		id = " & projID & " " & vbcrlf &_
				"	--and		releaseDate <= getdate() and expirationDate > getdate() "
		session("errorSQL") = sql
'		response.write "<pre>" & sql & "</pre>"
		set	rs = oConn.execute(SQL)
		
		if not rs.eof then
			
			projName = rs("Name")
			siteID = rs("siteID")
			projEntryDate = rs("entryDate")
			projReleaseDate = rs("releaseDate")
			projExpirationDate = rs("expirationDate")
			projBannerFileName = rs("bannerFileName")
			projHeaderTextTerm1 = rs("headerTextTerm1")
			projHeaderTextDefinition1 = rs("headerTextDefinition1")
			projHeaderTextTerm2 = rs("headerTextTerm2")
			projHeaderTextDefinition2 = rs("headerTextDefinition2")
			projHeaderFrequency = rs("headerFrequency")
			projIsActive = rs("isActive")
			
			isReleaseLive = true
			
		else
		
			isReleaseLive = false
		
		end if
	
	else

		projName = "Demo Project"
		siteID = 0
		projEntryDate = now
		projReleaseDate = now
		projExpirationDate = dateadd("h", 1, now)
		projBannerFileName = "banner.jpg"
		projHeaderTextTerm1 = "On Sale:"
		projHeaderTextDefinition1 = "Cool Universal Gadgets & Great Gift Ideas!"
		projHeaderTextTerm2 = "Clearance Items:"
		projHeaderTextDefinition2 = "Up to <strong>80% Off!</strong>"
		projHeaderFrequency = "Extreme Deals Every 2 Hours."
		projIsActive = true
	
	end if
%>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="/Scripts/jquery-ui-1.8.11.min.js" type="text/javascript"></script>
<script type="text/javascript">var page = '<%=request.ServerVariables("SCRIPT_NAME")&"?"&request.ServerVariables("QUERY_STRING")%>';//Required by lightningDeal.js</script>
<script src="/includes/js/lightningDeal.js" type="text/javascript"></script>
<link href="/includes/css/lightningDeal.css" type="text/css" rel="stylesheet" />
<div id="divSaleContainer">
	<div id="divSaleBanner"><img border="0" src="/images/lightningDeal/<%=projBannerFileName%>" /></div>
    <div class="divSale" id="ProdDetails">
        <div id="divWideStripAtBottom" class="mask bl"></div>
	    <div class="mask" id="divSpotlight">
			<%
            sql	=	"select		top 3 i.id, i.projectID, a.itemID, ISNULL(i.title,a.itemDesc_CO) as itemDesc " & vbcrlf &_
                    "			, a.itemPic_CO, a.itemPic, a.price_CO, a.price_Retail, ISNULL(i.description,a.itemLongDetail) as itemLongDetail, a.partNumber " & vbcrlf &_
                    "			, e.showAnimation, a.inv_qty " & vbcrlf &_
                    "			, i.releaseDate, i.expirationDate " & vbcrlf &_
					"			, ISNULL(i.price,a.price_CO) as price " & vbcrlf &_
                    "			, ISNULL(i.bullet1,a.POINT1) as bullet1 " & vbcrlf &_
                    "			, ISNULL(i.bullet2,a.POINT2) as bullet2 " & vbcrlf &_
                    "			, ISNULL(i.bullet3,a.POINT3) as bullet3 " & vbcrlf &_
                    "			, ISNULL(i.bullet4,a.POINT4) as bullet4 " & vbcrlf &_
                    "			, ISNULL(i.bullet5,a.POINT5) as bullet5 " & vbcrlf &_
                    "			, ISNULL(i.bullet6,a.POINT6) as bullet6 " & vbcrlf &_
                    "			, ISNULL(i.bullet7,a.POINT7) as bullet7 " & vbcrlf &_
                    "			, ISNULL(i.bullet8,a.POINT8) as bullet8 " & vbcrlf &_
                    "			, ISNULL(i.bullet9,a.POINT9) as bullet9 " & vbcrlf &_
                    "			, ISNULL(i.bullet10,a.POINT10) as bullet10 " & vbcrlf &_
                    "from		lightningDeal_Items i left join we_Items a " & vbcrlf &_
                    "		on	i.itemID = a.itemID left join we_ItemsExtendedData e " & vbcrlf &_
					"		on	a.PartNumber = e.partNumber" & vbcrlf &_
                    "where		projectid = " & projID & " and isActive = 1 and " & vbcrlf &_
					"			--releaseDate < getdate() and " & vbcrlf & _
					"			expirationDate > getdate() " & _
                    "order by	releaseDate"
            session("errorSQL") = sql
'			response.write "<pre>" & sql & "</pre>" : response.end
            set	rs = oConn.execute(SQL)

            if not rs.eof then
            
            itemID = rs("itemID")
            itemDesc = rs("itemDesc")
            itemPic = rs("itemPic_CO")
			itempic_WE = rs("itemPic")
            itemPriceOur = rs("price_CO")
            itemPriceRetail = rs("price_Retail")
            itemPriceDeal = rs("price")
            itemBullet1 = rs("bullet1")
            itemBullet2 = rs("bullet2")
            itemBullet3 = rs("bullet3")
            itemBullet4 = rs("bullet4")
            itemBullet5 = rs("bullet5")
            itemBullet6 = rs("bullet6")
            itemBullet7 = rs("bullet7")
            itemBullet8 = rs("bullet8")
            itemBullet9 = rs("bullet9")
            itemBullet10 = rs("bullet10")
			itemLongDetail = rs("itemLongDetail")
			itemReleaseDate = rs("releaseDate")
			itemExpirationDate = rs("expirationDate")
			partNumber = rs("partNumber")
			showAnimation = rs("showAnimation")
			itemQty = rs("inv_qty")
            
            'if not rs.eof then
            'rs.movenext
            'do upcoming items
            'else
            'no upcoming items
            'end if
			
			if now > itemReleaseDate and now < itemExpirationDate then 
				displayMain = true
			end if

            'response.write "displayMain:" & displayMain : response.end
			if displayMain = true then
            %>
            
	        <h1><%=itemDesc%></h1>
            
            <div id="divPhotoContainer">
                    	<div id="divMainPhoto"><img width="300" height="300" border="0" src="/productPics/big/<%=itemPic%>" id="imgLarge" itemprop="image"></div>
                    	<div id="divThumbnails">
<script type="text/javascript">
	var d = 'divMainPhoto';
	var o = '<%=itemPic%>';
	//var o = '/productPics/alts/thumbs/<%=itemPic%>';
	//function fnPreviewImage(imgSrc) {
	//	//var objToDisplay = '<img src="' + imgSrc + '" border="0" width="300" />';
	//	document.getElementById(d).style.backgroundImage = 'url(/productPics/alts/thumbs/' + imgSrc + ')';
	//}
	function fnPreviewImage(src) {
		var p = document.getElementById('imgLarge');
		p.src = src;
	} 	
	function fnDefaultImage() {
		document.getElementById(d).style.backgroundImage = 'url(/productPics/alts/thumbs/' + o + ')';
	}
</script>
<script type="text/javascript">
$(document).ready( function () {
	var dateString = '<%=MonthName(Month(itemExpirationDate)) & " " & Day(itemExpirationDate) & ", " & Year(itemExpirationDate) & " " & FormatDateTime(itemExpirationDate,3)%>';
	var now = new Date();
	var SecDiff = Math.floor((Date.parse(dateString) - now.getTime())/1000);
	CreateTimer("divClockDigits", SecDiff);
});
</script>
<%
''''''''''''''''' Start Thumbnails '''''''''''''''''
dim fso : set fso = CreateObject("Scripting.FileSystemObject")
dim jpeg : set jpeg = Server.CreateObject("Persits.Jpeg")

useImg = "/productPics/big/" & itemPic

%>
                <div style="width:300px; height:70px; padding-top:10px; float:left;">
                	<%
					curPic = mid(useImg,instrrev(useImg,"/")+1)
					if fso.fileExists(server.MapPath(useImg)) then
						'response.write ":)"
						if not fso.fileExists(server.MapPath("/productPics/alts/thumbs/" & curPic)) then
							'response.write ":("
							jpeg.Open Server.MapPath(useImg)
							jpeg.Height = 60
							jpeg.Width = 60
							jpeg.Save Server.MapPath("/productPics/alts/thumbs/" & curPic)
						end if
					end if
					%>
					<div style="float:left; cursor:pointer; border:1px solid #CCC; margin-right:3px;" class="justRound" onmouseover="fnPreviewImage('<%=useImg%>')"><img src="/productPics/alts/thumbs/<%=curPic%>" width="60" height="60" /></div>                    
                    <%
					for i = 0 to 3
						useImg = "/altPics/" & itempic_WE
						altMainPic = replace(replace(useImg,".jpg","-" & i & ".jpg"),"/big/","/altImgs/")
						if fso.fileExists(server.MapPath(altMainPic)) then
							curPic = mid(altMainPic,instrrev(altMainPic,"/")+1)
							if not fso.fileExists(server.MapPath("/productPics/alts/thumbs/" & curPic)) then
								jpeg.Open Server.MapPath(altMainPic)
								jpeg.Height = 60
								jpeg.Width = 60
								jpeg.Save Server.MapPath("/productPics/alts/thumbs/" & curPic)
							end if
					%>
                    <div style="float:left; cursor:pointer; border:1px solid #CCC; margin-right:3px;" class="justRound" onmouseover="fnPreviewImage('<%=altMainPic%>')"><img src="/productPics/alts/thumbs/<%=curPic%>" width="60" height="60" /></div>
                    <%
						end if
					next
					curPic = ""
					if typeID = 17 then curPic = "gg_atl_image1.jpg"
					if typeID = 20 then curPic = "ms_altview1.jpg##ms_altview2.jpg"
					if typeID = 19 then curPic = "CO-DecalSkin2.jpg"
					if curPic <> "" then
						picArray = split(curPic,"##")
						for i = 0 to ubound(picArray)
							curPic = picArray(i)
							if not fso.fileExists(server.MapPath("/productPics/alts/thumbs/" & curPic)) then
								jpeg.Open Server.MapPath("/altPics/" & curPic)
								jpeg.Height = 60
								jpeg.Width = 60
								jpeg.Save Server.MapPath("/productPics/alts/thumbs/" & curPic)
							end if
					%>
                    <div style="float:left; cursor:pointer; border:1px solid #CCC; margin-right:3px;" onmouseover="fnPreviewImage('/altPics/<%=curPic%>')"><img src="/productPics/alts/thumbs/<%=curPic%>" width="60" height="60" /></div>
                    <%
						next
					end if
					%>
                </div>

<%
''''''''''''''''' End Thumbnails '''''''''''''''''
%>
							
							<% 'for i = 1 to 3 $>
                            '<div class="divThumbnailSingle" id="ts<%=i$>"></div>
                            '<% next 
							%>
                        </div>
                        <% if itemQty < 1 then %>
                    	<div id="soldOutHover"><h2>Sorry, This Deal is Sold Out!</h2><div>Next deal available in: <span id="divSecondaryTimer_0" class="divSecondaryTimer"></span></div></div>
                        <% end if %>
                    </div>


			<div id="divRoundedBox">
                <div id="divInvBlow">Inventory Blowout Price:</div>
                <div id="divPriceBlowout"><%=formatcurrency(itemPriceDeal)%></div>
                <div id="divStarRating">
					
					<%
                    'Star Rating / Reviews Average
                    sql =	"select		sum (rating) as ratings, COUNT (*) as rCount, (sum (rating) / COUNT (*)) as avg " & vbcrlf & _
                            "from		we_Reviews " & vbcrlf & _
                            "where		itemID = " & itemID & " " & vbcrlf & _
                            "	and		approved = 1"
                    session("errorSQL") = sql
                    set	st = oConn.execute(SQL)
    
					avgRatings = st("ratings")
					avgCount = st("rCount")
					avgRating = st("avg")
					if isNumeric(avgRating) then
						if (Round(avgRating, 1) - Int(Round(avgRating, 1))) >= 0.5 then halfStar = true else halfStar = false end if
					else
						halfStar = false
					end if

					for i = 1 to 5 
					%><span class="star <%if i <= avgRating then%>full<%else%><%if halfStar and i = (Int(avgRating) + 1) then%>half<%else%>empty<%end if%><%end if%>"></span><% 
					next 
					%> <span class="star sup"><a href="javascript:showReviews();" class="<%if isnull(avgRating) then%>hide<%end if%>">Read Reviews</a></span>
                                    
                </div>
                <div id="divTheNumbers">
                    <div class="third" id="n1">
                        <div class="title">Value:</div>
                        <div class="price"><%=formatcurrency(itemPriceRetail)%></div>
                    </div>
                    <div class="divider"><div>&nbsp;</div><div>&nbsp;</div></div>
                    <div class="third" id="n2">
                        <div class="title">Discount:</div>
                        <div class="price"><%=Round((1-(itemPriceDeal/itemPriceRetail))*100)%>%</div>
                    </div>
                    <div class="divider"><div>&nbsp;</div><div>&nbsp;</div></div>
                    <div class="third" id="n3">
                        <div class="title">Savings:</div>
                        <div class="price"><%=formatcurrency(itemPriceRetail-itemPriceDeal)%></div>
                    </div>
                </div>
                <form name="frmSubmit" action="/cart/item_add.asp" method="post" style="margin:0px; padding:0px;">
                <div id="divQty">
                    Quantity <input type="text" name="qty" id="qty" value="1" maxlength="3" />
                </div>
                <div id="divAddToCart">
                    
                    <div id="divAddToCartButton" class="addCartButtonINS clickable" onclick="document.frmSubmit.submit();">Add to Shopping Cart</div>
                    
                    <div id="divLimitedQty">
                        
						<% if itemQty > 0 then%>
                        <h2 style="font-weight:bold; color:#666; font-size:12px; line-height:14px;">Limited Quantity - Order Now!</h2>
                        <div style="float:left; width:100%;">
                        	<div style="float:left; display:inline-block; padding-left:30px;">Only &nbsp;</div>
                        	<div id="varQtyRemaining" class="varQtyINS" style="float:left; display:inline-block;"><%=itemQty%></div> 
							<div style="float:left; display:inline-block;"> &nbsp;remaining at this price.</div>
						</div>
                        <% else %>
                        <h2>Next Deal Begins Soon!</h2>
                        <div id="QtyContainer"><span id="varQtyRemaining" class="varQtyOOS">0</span> remaining at this price.</div>
                        <% end if %>

                    </div>
                </div>
                <input type="hidden" name="musicSkins" value="0" />
                <input type="hidden" name="prodid" value="<%=itemID%>" />
                <input type="hidden" name="itemPrice" value="<%=itemPriceDeal%>" />
                </form>                
            </div>
			<% rs.movenext %>
            <% else %>
            <h1><%=notFound%></h1>
            <% end if %>
            <% else %>        
            <h1><%=notFound%></h1>
            <% end if %>        

        </div>
		
        <% if displayMain then %>
        
        <div id="divFeaturedItemDetailsContainer">
            <div id="divButtonShowToggle">Show Details</div>
        </div>
        
        <div id="divExpandedDetails">
            <div id="divExpandedDetailsContainer">
                
                <div id="divProductFeatures">
                    <h2>Product Features</h2>
                    <div class="hrMini"></div>
                    <ul>
                        <%if itemBullet1 <> "" then %><li><%=itemBullet1%></li><% end if %>
                        <%if itemBullet2 <> "" then %><li><%=itemBullet2%></li><% end if %>
                        <%if itemBullet3 <> "" then %><li><%=itemBullet3%></li><% end if %>
                        <%if itemBullet4 <> "" then %><li><%=itemBullet4%></li><% end if %>
                        <%if itemBullet5 <> "" then %><li><%=itemBullet5%></li><% end if %>
                        <%if itemBullet6 <> "" then %><li><%=itemBullet6%></li><% end if %>
                        <%if itemBullet7 <> "" then %><li><%=itemBullet7%></li><% end if %>
                        <%if itemBullet8 <> "" then %><li><%=itemBullet8%></li><% end if %>
                        <%if itemBullet9 <> "" then %><li><%=itemBullet9%></li><% end if %>
                        <%if itemBullet10 <> "" then %><li><%=itemBullet10%></li><% end if %>
                    </ul>
                </div>
                
                <div id="divProductDetails">
                    <h2>Product Details</h2>
                    <div class="hrMini"></div>
                    <div id="expDesc">
                        <p><%=itemLongDetail%></p>
                    </div>
                </div>
                
                <div id="divProductReviews">
                    
                    <%
					if itemID <> "" then
					
						'Star Rating / Review Single
						sql =	"select		top 3 rating, DateTimeEntd, reviewTitle, review, nickname--, * " & vbcrlf & _
								"from		we_Reviews " & vbcrlf & _
								"where		itemID = " & itemID & " " & vbcrlf & _
								"	and		approved = 1 " & vbcrlf & _
								"order by	id desc "
						session("errorSQL") = sql
	'							response.write "<pre>" & sql & "</pre>" : response.end
						set	st = oConn.execute(SQL)
						
						if not st.eof then
						do until st.eof
						%>
						<h2>Product Reviews</h2>
						<div class="hrMini"></div>
						<%
						reviewRating = st("rating")
						reviewDate = st("DateTimeEntd")
						reviewTitle = st("reviewTitle")
						review = st("review")
						reviewNickName = st("nickname")
	
						%>
						<div class="fDate"><%=MonthName(Month(reviewDate)) & " " & Day(reviewDate) & ", " & Year(reviewDate) %></div>
						<div id="divSquareStars"><% for i = 1 to 5 %><span class="squareStar <%if i <= reviewRating then%>ssFull<%else%>ssEmpty<%end if%>"></span><% next %></div>
						<div id="divReviewContainer">
							<h3><%=reviewTitle%></h3>
							<p><%=review%>
							</p>
							
							<p><span id="revBy">Reviewed By:</span> <%=reviewNickName%></p>
						</div>
						<%
						st.movenext
						loop
						end if
						
					end if
                    %>
                    
                </div>
                    
            </div>
        </div>
		
        <% end if %>
        
        <div class="mask" id="divCountdown">
        	<div id="divCountdownHeader">
            	<strong>Going Fast!</strong> Deal Ends In...
                <div id="divCountdownHeaderBlackTriangle"></div>
                <div id="divCountdownHeaderBlueTriangle"></div>
            </div>
            <div id="divCountdownHeart" align="left">
            
            	<% if displayMain then %>
				<%
				sql =	"select		count (*) as total " & vbcrlf & _
						"from		we_orderdetails " & vbcrlf & _
						"where		itemid = " & itemID & " " & vbcrlf & _
						"--	and		price = " & itemPriceDeal & " " & vbcrlf & _
						"--	and		entryDate > '" & itemReleaseDate & "' "
'				response.write "<pre>" & sql & "</pre>" : response.end
				session("errorSQL") = sql
				set mini = Server.CreateObject("ADODB.Recordset")
				mini = oConn.execute(SQL)
				varPeoplePurchased = mini("total") + 13
				%>
            	<div style="float:left; width:20px; height:18px; padding-top:5px;"><img src="/images/lightningDeal/CO-Black-Friday-Heart-Icon.png" border="0" /></div> &nbsp; <strong><span id="varPeoplePurchased"><%=varPeoplePurchased%></span> people</strong> have purchased this deal.
                <% else %>
               	<div style="float:left; width:20px; height:18px; padding-top:5px;"><img src="/images/lightningDeal/CO-Black-Friday-Heart-Icon.png" border="0" /></div> &nbsp; <strong><span></span></strong>
                <strong><span id="varPeoplePurchased" class="hide">&nbsp;</span></strong>
                <% end if %>

            	<div><div></div></div> &nbsp; <strong><span>232</span> people</strong> have purchased this deal.

            </div>
            <div id="divClockDigits"><span class="digit">00</span><span class="colon">:</span><span class="digit">00</span><span class="colon">:</span><span class="digit">00</span></div>
        </div>
        
    
    	<div class="mask" id="divCurrentUpcomingDeals">
	        <div id="HeaderEffect">
		        <div id="FiftyFourBlock" class="mask br"></div>
		        <div id="FiftyFourTriangle" class="mask br"></div>
                <div style="float:left;">Current &amp; Upcoming Deals: </div>
                <div style="float:left; width:26px; height:27px; padding-top:10px;"><img src="/images/lightningDeal/CO-Black-Friday-Upcoming-Deals-Arrow.png" border="0" /></div>
            </div>
        </div>
        
		<% if not rs.eof then %>
        
        <div id="divUpcomingItems">
                <% i = 1 %>
                <% do until rs.eof or i > 2 'Only perform iteration 2 times max %>
                <%
                    itemID = rs("itemID")
                    itemDesc = rs("itemDesc")
                    itemPic = rs("itemPic_CO")
                    itemPriceOur = rs("price_CO")
                    itemPriceRetail = rs("price_Retail")
                    itemPriceDeal = rs("price")
                    itemBullet1 = rs("bullet1")
                    itemBullet2 = rs("bullet2")
                    itemBullet3 = rs("bullet3")
                    itemBullet4 = rs("bullet4")
                    itemBullet5 = rs("bullet5")
                    itemBullet6 = rs("bullet6")
                    itemBullet7 = rs("bullet7")
                    itemBullet8 = rs("bullet8")
                    itemBullet9 = rs("bullet9")
                    itemBullet10 = rs("bullet10")
                    itemLongDetail = rs("itemLongDetail")
                    itemReleaseDate = rs("releaseDate")
                    itemExpirationDate = rs("expirationDate")
                    partNumber = rs("partNumber")
                    showAnimation = rs("showAnimation")
                    itemQty = rs("inv_qty")
                %>
                <div class="upcomingItemContainer">
                    <div class="img"><img border="0" src="/productpics/big/<%=itemPic%>" width="108" /></div>
                    <div class="desc"><%=itemDesc%></div>
                    <div class="priceDeal"><%'=formatcurrency(itemPriceDeal)%>$?</div>
                    <div class="priceRetail"><%=formatcurrency(itemPriceRetail)%></div>
                    <div class="upcomingItemHoverContainer" id="upcomingItemHoverContainer_<%=i%>"><div class="padlock"></div><h2>Sorry, This Deal is Still Locked!</h2><div>Available in: <span id="divSecondaryTimer_<%=i%>" class="divSecondaryTimer"></span></div></div>
                </div>
                <%
                if i = 2 then 'this is the second item in the upcoming list and it needs its own timer
                    dt = itemReleaseDate
                    %>
                            <div class="squiggle"></div>

                        <script type="text/javascript">
                        $(document).ready( function () {
                            var dateString = '<%=MonthName(Month(dt)) & " " & Day(dt) & ", " & Year(dt) & " " & FormatDateTime(dt,3)%>';
                            var now = new Date();
                            var SecDiff = Math.floor((Date.parse(dateString) - now.getTime())/1000);
                            CreateTimer2('divSecondaryTimer_'+'<%=i%>', SecDiff);
                        });
                        </script>
                    <%
                end if
                %>
                <% i = i + 1 %>
                <% rs.movenext %>
                <% loop %>

        </div>
        
        <div id="divExtremeDeals">
            <h2 style="padding-top:5px;"><%=projHeaderFrequency%></h2>
            <div>Check back often for maximum savings!</div>
        </div>
        <% else %>
        <div id="divUpcomingItems">
	        <h2><%=notFound%></h2>
        </div>
        <% end if %>

		
        <div id="divShareThisDeal">
        	<div style="float:left; margin-left:60px;">Share This Deal: </div>
            <a href="http://twitter.com/share/" target="_blank"><div id="divTwit" style="float:left;"></div></a>
            <a href="https://www.facebook.com/cellularoutfitter" target="_blank"><div id="divFace" style="float:left;"></div></a>
            <a href="https://pinterest.com/cellularoutfitter/" target="_blank"><div id="divPint" style="float:left;"></div></a>
        </div>


        
    </div>
    
    	<div id="divLowerPortion">

        <div class="divOtherProducts">
            <div class="divSectionTitle">
            	<div>
	                <h2 style="line-height:30px;"><span><%=projHeaderTextTerm1%></span> <%=projHeaderTextDefinition1%> <img border="0" src="/images/lightningDeal/CO-Black-Friday-Warehouse-Clearance-Arrow.png" /></h2>
                </div>
            </div>
            <div class="divOnSale">
                <%
				sql = 	"select		* " & vbcrlf &_
						"from		lightningDeal_Categories " & vbcrlf &_
						"where		projectID = " & projID & " and isActive = 1 " & vbcrlf &_
						"order by	sortOrder"
				session("errorSQL") = sql
				set	rs = oConn.execute(SQL)
				
				%>
				<% counter = 1 %>
                <% if not rs.eof then %>
                <% do while not rs.eof %>
                <%
				catID = rs("id")
				catName = rs("name")
				catPic = rs("pic")
				catBullet1 = rs("bullet1")
				catBullet2 = rs("bullet2")
				catLink = rs("link")
'				if catName = "Sherwood" then response.write "catLink:" & catLink
				if isnull(catLink) then
					catLink = "/ldc-" & catID & "-" & formatSEO(catName) & ".asp"
				end if
				'catLink = Replace(catLink, "&amp;", "n")
				'catLink = Replace(catLink, "&", "n")
				'catLink = Replace(catLink, "&", "n")
				%>
                <div class="divOnSaleSingle" style="width:19%;">
                    <div class="divPhoto clickable" onclick="window.location='<%=catLink%>';" style="background-image:url(/images/lightningDeal/<%=catPic%>);"></div>
                    <div class="divTitle clickable" onclick="window.location='<%=catLink%>';"><%=catName%></div>
                    <div class="divExclamation"><%=catBullet1%></div>
                    <div class="divStartingAt"><%=catBullet2%></div>
                    <div class="divShopNowButton clickable" onclick="window.location='<%=CatLink%>';">Shop Now</div>
                </div>
                <% rs.movenext %>
				<% if counter mod 5 = 0 and not rs.eof then %><div class="hr hrGrid"></div><% end if %>
                <% counter = counter + 1 %>
				<% loop %>
                <% end if%>
            </div>
        </div>
        
        <div class="divOtherProducts">
            <div class="divSectionTitle">
            	<div>
	            	<h2 style="line-height:30px;"><span><%=projHeaderTextTerm2%></span> <%=projHeaderTextDefinition2%> <img border="0" src="/images/lightningDeal/CO-Black-Friday-Warehouse-Clearance-Arrow.png" /></h2>
                </div>
            </div>
            <div class="divOnSale">
                <%
				sql = "select		a.itemID, a.itemDesc_CO, a.itemPic_CO, a.price_CO, a.price_Retail " & vbcrlf &_
						"from		lightningDeal_RegItems li left join we_Items a " & vbcrlf &_
						"		on	li.itemID = a.itemID " & vbcrlf &_
						"where		projectid = " & projID & " and isActive = 1 " & vbcrlf &_
						"order by	sortOrder"
				session("errorSQL") = sql
				set	rs = oConn.execute(SQL)
				%>
				<% counter = 1 %>
				<% if not rs.eof then %>
                <% do while not rs.eof %>
				<%
				regItemId = rs("itemID")
				regItemDesc = rs("itemDesc_CO")
				regItemPic = rs("itemPic_CO")
				regItemPriceOur = rs("price_CO")
				regItemPriceRetail = rs("price_Retail")
				%>
                <div class="divOnSaleSingle" style="width:19%;">
                    <div class="divPhoto clickable" onclick="window.location='/p-<%=regItemId%>-<%=formatSEO(regItemDesc)%>-.html';"><img border="0" src="/productpics/big/<%=regItemPic%>" width="146" /></div>
                    <div class="divTitle clickable" onclick="window.location='/p-<%=regItemId%>-<%=formatSEO(regItemDesc)%>-.html';"><%=regItemDesc%></div>
                    <div class="divExclamation"><%=formatcurrency(regItemPriceOur)%></div>
                    <div class="divStartingAt strike">Reg. <%=formatcurrency(regItemPriceRetail)%></div>
                    <div class="divShopNowButton clickable" onclick="window.location='/p-<%=regItemId%>-<%=formatSEO(regItemDesc)%>-.html';">View Details</div>
                </div>
                <% rs.movenext %>
				<% if counter mod 5 = 0 and not rs.eof then %><div class="hr hrGrid"></div><% end if %>
                <% counter = counter + 1 %>
				<% loop %>
				<% end if %>
            </div>
        </div>
	
        <div class="clear"></div>

    </div>
    
   <div class="hide" id="forceRefresh"><%=forceRefresh%></div>
   <div class="hide" id="divRandom"><%randomize : lowNumber = 1 : highNumber = 300 : response.write Round((highNumber - lowNumber + 1) * Rnd + LowNumber)%></div>
</div>

<!--#include virtual="/includes/template/bottom.asp"-->
