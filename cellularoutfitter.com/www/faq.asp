<%
response.buffer = true
pageTitle = "FAQ"
noLeftSide = 1

dim section
section = request.querystring("section")

dim faqid
faqid = request.querystring("faqid")

search = request.querystring("search")
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<link rel="stylesheet" type="text/css" href="/includes/css/faq.css" />
<h1 class="faq-title">Help and Customer Service</h1>
<div class="faq-left-nav">
	<div class="faq-nav-section">
		<% if (isNumeric(faqid) and faqid <> "") or (isNumeric(section) and section <> "") then %>
		<div class="back-nav"><a href="faq.html">&laquo; Back to All Topics</a></div>
		<% end if %>
		<div class="section-title">Help Topics</div>
		<ul>
			<% 
				call fOpenConn()
				sql = "SELECT * FROM XFaqCategory WHERE siteid = 2"
				set RSCat = Server.CreateObject("ADODB.Recordset")
				RSCat.open sql, oConn, 3, 3
				if not RSCat.EOF then
					do until RSCat.EOF
						category_id = RSCat("category_id")
						category_name = RSCat("category_name")
						if CInt(category_id) = CInt(section) then
			%>
							<li class="active"><a class="faq-link" href="faq.html?section=<%=category_id%>"><%=category_name%></a></li>
			<%
						else 
			%>
							<li><a class="faq-link" href="faq.html?section=<%=category_id%>"><%=category_name%></a></li>
			<%			
						end if
						RSCat.movenext
					loop
				end if
				RSCat.close
			%>
		</ul>
	</div>
	<div class="faq-nav-section">
		<div class="section-title">Contact Us</div>
		<ul>
			<li><a class="faq-link" href="javascript:doContactus();">Email Us</a></li>
			<li>
				<div>
					<script type="text/javascript">
						var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
						var lhnid = 3410;
						var lhnwindow = 6755;
						var lhnImage = "Live Chat";
						document.write(unescape("%3Cscript src='/framework/utility/liveHelp.js' type='text/javascript'%3E%3C/script%3E"));
					</script>
				</div>
			</li>
			<li><a class="faq-link" href="track-your-order.html">Check Your Order Status</a></li>
		</ul>
	</div>
</div>
<div class="faq-main-section">
	<div class="faq-search">
		<form name="frm_search" method="get" onsubmit="return checkSearchLength();">
			<input type="text" name="search" id="search" class="search-field" value="Enter a keyword or topic" onfocus="if(this.value='Enter a keyword or topic') this.value = '';" onblur="if(!this.value) this.value='Enter a keyword or topic';" />
			<input type="submit" class="search-help" value="" />
		</form>
	</div>
	<div class="faq-content">
		<% 
			if isNumeric(faqid) and faqid <> "" then 
				sqlFaq = "SELECT f.*, f2c.category_id, c.category_name FROM XFaq as f LEFT JOIN XFaqToCategory as f2c ON f2c.faq_id = f.faq_id LEFT JOIN XFaqCategory as c ON c.category_id = f2c.category_id WHERE f.faq_id = '" & faqid & "' AND f.siteid = 2"
				set RSFaq = Server.CreateObject("ADODB.Recordset")
				RSFaq.open sqlFaq, oConn, 3, 3
				if not RSFaq.EOF then
					faq_category_id = RSFaq("category_id")
					faq_category_name = RSFaq("category_name")
					faq_title = RSFaq("faq_title")
					faq_content = RSFaq("faq_content")
					faq_content = replace(faq_content, "[promo code screen shot]", "<img src=""/images/faq/CO-promo-location-desktop.jpg"" border=""0"" width=""750"" />")
		%>
					<div class="back-link"><a href="faq.html?section=<%=faq_category_id%>">&laquo; Back to <%=faq_category_name%></a></div>
					<div class="content">
						<h2><%=faq_title%></h2>
						<%=faq_content%>
					</div>
		<%		end if
			else 
				if isEmpty(search) then
					if (not isNumeric(section) or section < 1) and (not isNumeric(faqid) or faqid < 1) then
						section = 1
					end if
				
					sqlCategory = "SELECT * FROM XFaqCategory WHERE category_id = '" & section & "' AND siteid = 2"
					set RSCategory = Server.CreateObject("ADODB.Recordset")
					RSCategory.open sqlCategory, oConn, 3, 3
					if not RSCategory.EOF then
						category_name = RSCategory("category_name")
		%>
						<h2><%=category_name%></h2>
						<ul class="questions">
							<%
								sql = "SELECT f.* FROM XFaq as f LEFT JOIN XFaqToCategory as f2c ON f2c.faq_id = f.faq_id WHERE f2c.category_id = '" & section & "' AND f.siteid = 2 ORDER BY f2c.ordernum"
								set RSQuestions = Server.CreateObject("ADODB.Recordset")
								RSQuestions.open sql, oConn, 3, 3
								if not RSQuestions.EOF then
									do until RSQuestions.EOF
										faq_id = RSQuestions("faq_id")
										faq_title = RSQuestions("faq_title")
							%>
										<li><ul><li><a href="faq.html?faqid=<%=faq_id%>"><%=faq_title%></a></li></ul></li>
							<%
										RSQuestions.movenext
									loop
								end if
								RSQuestions.close
							%>
						</ul>
		<% 
					end if
				else
					arr_search = split(search)
					where = ""
					for each keyword in arr_search
						keyword = replace(keyword, "'", "&#039;")
						if where <> "" then
							where = where & " OR "
						end if
						
						where = where & " faq_title LIKE '%" & keyword & "%' OR faq_content LIKE '%" & keyword & "%'"
					next
					sqlSearch = "SELECT * FROM XFaq"
					if where <> "" then
						sqlSearch = sqlSearch & " WHERE " & where
					end if
					
					set RSSearch = Server.CreateObject("ADODB.Recordset")
					RSSearch.open sqlSearch, oConn, 3, 3
					if not RSSearch.EOF then
		%>
						<h2>Search Results for '<%=search%>'</h2>
						<ul class="questions">
		<%
						do until RSSearch.EOF
							faq_id = RSSearch("faq_id")
							faq_title = RSSearch("faq_title")
							faq_content = RSSearch("faq_content")
							for each keyword in arr_search
								faq_title = replace(faq_title, keyword, "<span class='highlight'>" & keyword & "</span>")
								stripped = strip_tags(faq_content)
								faq_content = replace(strip_tags(faq_content), keyword, "<span class='highlight'>" & keyword & "</span>")
								content_length = Len(faq_content)
								if content_length > 200 then
									faq_content = Left(faq_content, 200) & "..."
								end if
							next
		%>
							<li>
								<ul><li><a href="faq.html?faqid=<%=faq_id%>"><%=faq_title%></a></li></ul>
								<%=faq_content%>
							</li>
		<%
							RSSearch.movenext
						loop
		%>
						</ul>
		<%
					end if
				end if 
			end if 
		%>
	</div>
</div>
<script type="text/javascript">
	function sendContactUs() {
		txtContactEmail = document.frmContactus.txtContactEmail.value;
		txtMessage = document.frmContactus.txtMessage.value;

		if (!checkEmail(txtContactEmail)) {
			alert('Please enter a valid email address');
			return false;
		}

		if ((txtMessage=='')||(txtMessage=='Enter your message')) {
			alert('Please enter your message');
			return false;
		}

		url = '/ajax/getContactusPopup.asp?orderid=<%=orderid%>&txtContactEmail=' + escape(txtContactEmail) + '&txtMessage=' + escape(txtMessage) + '&submitted=Y'
		ajax(url,'popBox');

		return true;
	}
	
	function doContactus() {
		document.getElementById("popBox").innerHTML = '';
		ajax('/ajax/getContactusPopup.asp?orderid=<%=orderid%>&txtContactEmail=<%=email%>','popBox');
		document.getElementById("popCover").style.display = "";
		document.getElementById("popBox").style.display = "";
	}
	
	function closeContactUsPopup() {
		document.getElementById("popCover").style.display = "none";
		document.getElementById("popBox").style.display = "none";
	}
	
	function checkSearchLength() {
		var search = document.getElementById('search').value.trim();
		var sch_len = search.length;

		if(sch_len < 2) { 
			alert('Search term should be at least 2 characters.'); 
			return false; 
		}
	}
</script>
<!--#include virtual="/includes/template/bottom.asp"-->
