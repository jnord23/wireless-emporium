<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
dim carrierid, carrierName, carrierCode, carrierImg
carrierid = request.querystring("carrierid")
if carrierid = "" or not isNumeric(carrierid) then
	response.redirect("/")
	response.end
end if

call redirectURL("carp", carrierid, request.ServerVariables("HTTP_X_REWRITE_URL"), "")

sql = 	"select	distinct c.carriername, c.carriercode, c.carrierimg, b.brandid, b.brandname, b.brandimg, b.brandcode" & vbcrlf & _
		"from	we_items a join we_brands b " & vbcrlf & _
		"	on	a.brandid = b.brandid join we_carriers c" & vbcrlf & _
		"	on	a.carrierid = c.id left outer join we_pndetails d" & vbcrlf & _
		"	on	a.partnumber = d.partnumber" & vbcrlf & _
		"where	a.typeid = '16' " & vbcrlf & _
		"	and	a.carrierid = '" & carrierid & "'" & vbcrlf & _
		"	and a.hidelive = 0 " & vbcrlf & _
		"	and (a.inv_qty <> 0 or d.alwaysinstock = 1) " & vbcrlf & _
		"	and a.price_co > 0 	" & vbcrlf & _
		"order by b.brandname"
'response.write "<pre>" & sql & "</pre>"
'response.end
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
if RS.eof then
	call fCloseConn()
	response.redirect("/")
	response.end
else
	carrierName = RS("carrierName")
	carrierCode = RS("carrierCode")
	carrierImg = RS("carrierImg")
end if

dim SEtitle, SEdescription, SEkeywords, topText, H1color

siteId = 2
cmsData(request.ServerVariables("HTTP_X_REWRITE_URL"))
metaArray = split(session("metaTags"),"##")
SEtitle = metaArray(0)
SEdescription = metaArray(1)
SEkeywords = metaArray(2)
SeTopText = metaArray(3)
SeBottomText = metaArray(4)

select case carrierid
	case "1"
		if SEtitle = "" then SEtitle = "Cheap Alltel Cell Phone Accessories: LG, Nokia, Motorola, Samsung, Audiovox At Discounted Prices!"
		if SEdescription = "" then SEdescription = "CellularOutfitter.com offers discounted Alltel cell phone accessories for all major brands like Motorola, Audiovox, Samsung, LG, Nokia & much more at wholesale prices."
		if SEkeywords = "" then SEkeywords = "alltel cell phone accessories, alltel phones, alltel phone accessories, alltel lg accessories, alltel audiovox accessories, alltel accessories"
		if SeTopText = "" then SeTopText = "Currently based in Little Rock, AR, Alltell is the nation's 8th largest cellular carrier.  They are strictly a regional carrier, operating in just six states, mainly in rural areal.  Because of their size, accessories for Alltell phones can be hard to come by.  But not at Cellular Oufitter where we have a huge selection of accessories for every Alltell phone.  Like all of our products, our Alltell accessories are offered at wholesale prices and backed by a 100% satisfaction guarantee."
		H1color = "FFFFFF"
	case "2"
		if SEtitle = "" then SEtitle = "Cheap AT&T / Cingular Cell Phone Accessories: LG, Nokia, Motorola, iPhone At Discounted Prices!"
		if SEdescription = "" then SEdescription = "CellularOutfitter.com offers discounted AT&T / Cingular cell phone accessories for all major brands like Motorola, Audiovox, Samsung, LG, Nokia & much more at wholesale prices."
		if SEkeywords = "" then SEkeywords = "at&t cell phone accessories, at&t phones, at&t samsung accessories, at&t lg accessories, at&t accessories, cingular accessories"
		if SeTopText = "" then SeTopText = "When AT&T was given exclusivity to carry the iPhone, it helped them grow into the second largest carrier in the US.  But AT&T is much more than just Apple products.  They have a huge selection of Android and Blackberry phones, as well as dozens of feature phones.  In addition to carrying one of the largest selections of iPhone accessories online, Cellular Outfitter has accessories for every AT&T phone, past and present.  Regardless of your needs, we have the accessory you need to keep your phone looking brand new right up until the day you decide to upgrade."
		H1color = "FFFFFF"
	case "3"
		if SEtitle = "" then SEtitle = "Cheap Metro PCS Cell Phone Accessories: Nokia, Motorola, Samsung, LG At Discounted Prices!"
		if SEdescription = "" then SEdescription = "CellularOutfitter.com offers discounted Metro PCS cell phone accessories for all major brands like Motorola, LG, Samsung, Nokia & much more at wholesale prices."
		if SEkeywords = "" then SEkeywords = "metro pcs cell phone accessories, metro pcs phones, metro pcs accessories"
		if SeTopText = "" then SeTopText = "As the fifth largest carrier in the US and by far the largest no contract cell phone carrier in the US, Metro PCS has burst onto the mobile world with a massive selection of phones.  Ranging from entry level feature phones to high end smartphones on par with the major carriers, Metro PCS offers the selection that their customers crave.  Picking up where they leave off, Cellular Outfitter has the largest selection of accessories to go with those phones to help them last longer and perform better."
		H1color = "FFFFFF"
	case "4"
		if SEtitle = "" then SEtitle = "Cheap Sprint / Nextel Cell Phone Accessories: Nokia, Motorola, Pantech, LG At Discounted Prices!"
		if SEdescription = "" then SEdescription = "CellularOutfitter.com offers discounted Sprint / Nextel cell phone accessories for all major brands like Motorola, LG, Pantech, Nokia & much more at wholesale prices."
		if SEkeywords = "" then SEkeywords = "sprint cell phone accessories, nextel cell phone accessories, sprint phones, nextel phones, sprint accessories, nextel accessories"
		if SeTopText = "" then SeTopText = "What Sprint lacks in sheeer size when compared to the big 2 carriers, it more than makes up for in innovation.  They launched the first 4G phone in America, the HTC Evo 4G, released the only Samsung Galaxy S phone with a slide out keyboard and were the first CDMA carrier in the US to announce a Windows Phone 7 device.  Sprint has rewarded their customers with a variety of smartphones and feature phones across multiple operating systems, and CellularOutfitter.com has the accessories you need for all of them.  With a product line as divers as the phones on the Sprint network, there is no doubt that you will find exactly what you are looking for."
		H1color = "FFFFFF"
	case "5"
		if SEtitle = "" then SEtitle = "Cheap T-Mobile Cell Phone Accessories: Sidekick, Nokia, Motorola, Samsung At Discounted Prices!"
		if SEdescription = "" then SEdescription = "CellularOutfitter.com offers discounted T-Mobile cell phone accessories for all major brands like Sidekick, Motorola, Samsung, Nokia & much more at wholesale prices."
		if SEkeywords = "" then SEkeywords = "t-mobile cell phone accessories, t-mobile cellphone accessories, t-mobile phones, t-mobile accessories"
		if SeTopText = "" then SeTopText = "T-Mobile might be the smallest of the four national carriers, but good things come in small packages.  The first ever Android phone in the US was on their network and they haven't looked back since.  With a full compliment of 4G phones as well as an extensive line of entry level phones, T-Mobile has established itself as a carrier that can cater to any budget.  Keeping with that theme Cellular Outfitter is set up to cater to your budget as well, offering thousands of products at wholesale prices for every make and model cell phone in T-Mobiles lineup."
		H1color = "FFFFFF"
	case "6"
		if SEtitle = "" then SEtitle = "Cheap Verizon Cell Phone Accessories: Nokia, Motorola, Samsung, LG at Discounted Prices!"
		if SEdescription = "" then SEdescription = "CellularOutfitter.com offers discounted Verizon cell phone accessories for all major brands like Motorola, LG, Samsung, Nokia & much more at wholesale prices."
		if SEkeywords = "" then SEkeywords = "verizon cell phone accessories, verizon phones, verizon accessories, verizon cellphone accessories, verizon phone accessory"
		if SeTopText = "" then SeTopText = "As the nation's largest carrier, Verizon serves more than 90 million customers.  Because of their size, Verizon gets the latest and greatest phones from all the major manufacturers.  With that kind of variety and selection, it can be hard to find the specific accessory you are looking for.  Thankfully there is Cellular Outfitter, where you can find thousands of accessories for every Verizon phone on the market today all in one place.  Save time and save money by making CellularOutfitter.com your one stop for all your Verizon shopping needs."
		H1color = "FFFFFF"
	case "7"
		if SEtitle = "" then SEtitle = "Cheap Prepaid Cell Phone Accessories: Nokia, Motorola, Pantech, LG At Discounted Prices!"
		if SEdescription = "" then SEdescription = "CellularOutfitter.com offers discounted Prepaid cell phone accessories for all major brands like Motorola, LG, Pantech, Nokia & much more at wholesale prices."
		if SEkeywords = "" then SEkeywords = "prepaid cell phone accessories, prepaid phones, prepaid accessories, prepaid cellphone accessories"
		if SeTopText = "" then SeTopText = "You have reached the Prepaid Cell Phone Accessory section of CellularOutfitter.com. We offer the largest selection of Prepaid Cell Phone Accessories at wholesale prices to the public. Your Prepaid phone can now have the best of accessories, only at CellularOutfitter.com. Buy accessories for all brands, such as <a href=""/b-18-pantech-cell-phone-accessories.html"" title=""Pantech Accessories"">Pantech accessories</a>, <a href=""/b-7-nokia-cell-phone-accessories.html"" title=""Nokia Cell Phone Accessories"">Nokia cell phone accessories</a>, <a href=""/b-4-lg-cell-phone-accessories.html"" title=""LG Phone Accessories"">LG phone accessories</a>, <a href=""/b-5-motorola-cell-phone-accessories.html"" title=""Motorola Accessories"">Motorola accessories</a> and many more! For the easiest and most convenient shopping experience on-line, simply start by choosing your specific brand below:"
		H1color = "000000"
	case "8"
		if SEtitle = "" then SEtitle = "Cheap Boost Mobile / Southern LINC Cell Phone Accessories: Motorola & Nextel At Discounted Prices!"
		if SEdescription = "" then SEdescription = "CellularOutfitter.com offers discounted Boost Mobile / Southern LINC cell phone accessories for all major brands like Motorola & Nextel at wholesale prices."
		if SEkeywords = "" then SEkeywords = "boost mobile cell phone accessories, boost mobile phones, southern linc accessories, southern linc phones, southern linc cell phone accessories"
		if SeTopText = "" then SeTopText = "Boost Mobile is a prepaid cell phone company owned and operated by Sprint.  In addition to feature phones, Boost Mobile also offers a handful of Android and Blackberry devices.  While their product line is primarily dominated by Motorola phones, they do offer enough of a variet to keep customers satisfied.  In order to serve those custoemrs eclectic tastes, CellularOutfitter.c om offers the widest selection of accessories for Boost Mobile phones anywhere on the internet."
		H1color = "000000"
	case "9"
		if SEtitle = "" then SEtitle = "Cheap Cricket Cell Phone Accessories: Nokia, Motorola, Samsung, Audiovox At Discounted Prices!"
		if SEdescription = "" then SEdescription = "CellularOutfitter.com offers discounted Cricket cell phone accessories for all major brands like Motorola, Audiovox, Samsung, Nokia & much more at wholesale prices."
		if SEkeywords = "" then SEkeywords = "cricket cell phone accessories, cricket phones, cricket samsung accessories, cricket nokia accessories, cricket accessories"
		if SeTopText = "" then SeTopText = "Founded in 1999, Cricket Wireless just recently went nationwide, serving nearly 5 million customers.  Founded as a no contract wireless provider, the company has stuck to that commitment to this day.  Despite the fact that Cricket has a limited lineup of phones, you don't need to worry about having a limited lineup of accessories to choose from thanks to Cellular Outfitter.  We carry hundreds of products covering each and every phone that Cricket has to offer, all at discount prices."
		H1color = "FFFFFF"
	case "10"
		H1color = "FFFFFF"
	case "11"
		if SEtitle = "" then SEtitle = "Cheap U.S. Cellular Phone Accessories: Nokia, Motorola, Samsung, LG At Discounted Prices!"
		if SEdescription = "" then SEdescription = "CellularOutfitter.com offers discounted U.S. Cellular phone accessories for all major brands like Motorola, LG, Samsung, Nokia & much more at wholesale prices."
		if SEkeywords = "" then SEkeywords = "u.s. cellular phone accessories, u.s. cellular accessories, u.s. cellular phones"
		if SeTopText = "" then SeTopText = "Serving just over 6 million customers in 26 states, US Cellular is one of the biggest telecommunications networks in the country.  Their primary region is the Northeast, where they often times compete with the 4 major national carriers in terms of call quality and signal strength.  They also compete in terms of the phones they have to offer, with devices that compare favorably to anything on the big 4 networks.  To keep those phones functioning like the day they were bought, Cellular Outfitter has a full line of accessories for all US Cellular phones, priced well below retail prices."
		H1color = "FFFFFF"
end select



dim strBreadcrumb
strBreadcrumb = carrierName & " Cell Phones"
%>

<!--#include virtual="/includes/template/top.asp"-->
<style>
	h1 {
		color: #<%=H1color%>;
	}
</style>

<table border="0" cellspacing="0" cellpadding="0" width="775">
    <tr>
        <td class="top-sublink-gray">
            <a class="top-sublink-gray" href="/">HOME</a>&nbsp;>&nbsp;
            <span class="top-sublink-blue"><%=carrierName%> Cell Phones</span>
        </td>
    </tr>
    <tr>
        <td class="top-sublink-gray"><img src="/images/spacer.gif" width="1" height="10"></td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td width="210" valign="top" align="center"><img src="/images/carriers/CO_carrier_logo_<%=formatSEO(carrierName)%>.jpg" border="0" width="190" height="119"></td>
                    <td width="565" valign="top" class="static-content-font"><b><%=SeTopText%></b></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="top-sublink-gray"><img src="/images/spacer.gif" width="1" height="10"></td>
    </tr>
    <tr>
        <td valign="middle" width="100%" height="68" style="background-image: url('/images/carriers/CO_carrier_header_<%=formatSEO(carrierName)%>.jpg'); background-repeat: no-repeat; background-position: center bottom;">
            <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=carrierName%> Cell Phones</h1>
        </td>
    </tr>
    <tr>
        <td><img src="/images/spacer.gif" width="1" height="10" border="0"></td>
    </tr>
    <tr>
        <td align="center" width="100%" style="padding-bottom:50px;">
            <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <%
                    a = 0
                    do until RS.eof
                        %>
                        <td align="center" valign="top" width="190">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td align="center" valign="middle">
                                        <a href="/cp-<%=carrierid%>-sb-<%=rs("brandid")%>-<%=formatSEO(rs("brandname"))%>-<%=formatSEO(rs("carriername"))%>-cell-phones.html">
                                            <img src="/images/brands/CO_CATpage_buttons_<%=formatSEO(RS("brandName"))%>.jpg" width="165" height="63" border="0" alt="<%=carrierName & " " & RS("brandName")%> Cell Phones">
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="bottom">
                                        <a href="/cp-<%=carrierid%>-sb-<%=rs("brandid")%>-<%=formatSEO(rs("brandname"))%>-<%=formatSEO(rs("carriername"))%>-cell-phones.html" title="<%=RS("brandName")%> Cell Phones" class="cellphone-category-font-link">
                                            <%=RS("brandName")%> Cell Phones
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <%
                        RS.movenext
                        a = a + 1
                        if a = 4 then
                            response.write "</tr><tr><td align=""center"" valign=""top"" colspan=""4""><img src=""/images/spacer.gif"" width=""5"" height=""5""></td></tr><tr>" & vbcrlf
                            a = 0
                        end if
                    loop
                    if a = 1 then
                        response.write "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" & vbcrlf
                    elseif a = 2 then
                        response.write "<td>&nbsp;</td><td>&nbsp;</td>" & vbcrlf
                    elseif a = 3 then
                        response.write "<td>&nbsp;</td>" & vbcrlf
                    end if
                    %>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td style="padding:20px; text-align:left;">
			<%=SeBottomText%>
        </td>
    </tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<%
call fCloseConn()
%>

<!--#include virtual="/includes/template/bottom.asp"-->
