<%
'Causes Google to detect Crawl Errors
'Response.Status = "404 Not Found"
noCommentBox = true
'Soft 404
Response.Clear
Response.Status = "200 OK"

pageTitle = "File not Found - Error 404"
noLeftSide = 1
basePageName = "404error.asp"
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/template/top_index.asp"-->
<link rel="stylesheet" type="text/css" href="/includes/css/404.css<%=getCssDateParam("/includes/css/404.css")%>" />

<div class="error-banner">
	<div class="error-cellphone"></div>
	<div class="error-text">
		<h1 class="error-header">Well, this is embarrassing...</h1>
		<p class="error-text-body">We're sorry, but we can't find the page you are looking for.</p>
		<p class="error-text-body">Please check the URL for errors, or click one of the links below to get back on track.</p>
		<p class="error-text-body">If you would like some assistance, you can speak with one of our helpful representatives by calling <span class="phone">1-800-871-6926</span></p>
	</div>
</div>
<div class="links">
	<div class="link-lines"></div>
	<div class="links-title">Click one of the links below to get back on track</div>
	<div class="link-lines"></div>
	<div class="links-container">
		<a href="http://www.cellularoutfitter.com/m-1412-apple-iphone-5-cell-phone-accessories.html">Apple iPhone 5 Accessories</a> | 
		<a href="http://www.cellularoutfitter.com/m-1612-apple-iphone-5c-cell-phone-accessories.html">Apple iPhone 5C Accessories</a> | 
		<a href="http://www.cellularoutfitter.com/m-1628-apple-iphone-5s-cell-phone-accessories.html">Apple iPhone 5S Accessories</a> | 
		<a href="http://www.cellularoutfitter.com/sb-17-sm-1612-sc-3-apple-iphone-5c-covers-and-gel-skins.html">Apple iPhone 5C Cases</a> | 
		<a href="http://www.cellularoutfitter.com/sb-17-sm-1628-sc-3-apple-iphone-5s-covers-and-gel-skins.html">Apple iPhone 5S Cases</a> | 
		<a href="http://www.cellularoutfitter.com/sb-17-sm-1412-sc-3-apple-iphone-5-covers-and-gel-skins.html">Apple iPhone 5 Cases</a> | 
		<a href="http://www.cellularoutfitter.com/m-1624-samsung-galaxy-note-3-cell-phone-accessories.html">Samsung Galaxy Note 3 Accessories</a> | 
		<a href="http://www.cellularoutfitter.com/m-1523-samsung-galaxy-s4-cell-phone-accessories.html">Samsung Galaxy S4 Accessories</a> | 
		<a href="http://www.cellularoutfitter.com/sb-9-sm-1624-sc-3-samsung-galaxy-note-3-covers-and-gel-skins.html">Samsung Galaxy Note 3 Cases</a> | 
		<a href="http://www.cellularoutfitter.com/sb-9-sm-1523-sc-3-samsung-galaxy-s4-covers-and-gel-skins.html">Samsung Galaxy S4 Cases</a> |
		<a href="http://www.cellularoutfitter.com/b-20-htc-cell-phone-accessories.html"> HTC One Accessories</a> |
		<a href="http://www.cellularoutfitter.com/sb-20-sm-1512-sc-3-htc-one-covers-and-gel-skins.html"> HTC One Cases</a>
	</div>
</div>
<div id="featuredProducts" style="position:relative;width:880px;padding:0 80px;float:left;margin:0 0 100px 0;">
<%
	set fs = CreateObject("Scripting.FileSystemObject")
	myModel = prepInt(request.Cookies("myModel"))
	myBrand = prepInt(request.Cookies("myBrand"))

'	if myModel > 0 then
'		sql = 	"select top 20 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO, c.modelID from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 2 and c.modelID = " & myModel & " and c.itemid not in (select cast(configValue as int) itemid from we_config where configName = 'Free ItemID') group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO, c.modelID " &_
'				"union " &_
'				"select top 20 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO, 0 as modelID from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 2 and b.orderdatetime > '" & date-90 & "' and c.brandID = 20 and c.itemid not in (select cast(configValue as int) itemid from we_config where configName = 'Free ItemID') group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by c.modelID desc, 2 desc"
'	elseif myBrand > 0 then
'		sql = "select top 20 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 2 and b.orderdatetime > '" & date-90 & "' and c.brandID = " & myBrand & " and c.itemid not in (select cast(configValue as int) itemid from we_config where configName = 'Free ItemID') group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by 2 desc"
'	else
'		sql = "select top 20 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 2 and b.orderdatetime > '" & date-90 & "' and c.itemid not in (select cast(configValue as int) itemid from we_config where configName = 'Free ItemID') group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by 2 desc"
'	end if
	sql = "exec sp_recommendedItemsForUserCookie 2," & prepInt(myBrand) & "," & prepInt(myModel) & ",0,12"
'	response.write sql
	session("errorSQL") = sql
	set topProds = oConn.execute(sql)
	
'	if topProds.EOF then
'		myBrand = 0
'		myModel = 0
'		sql = "select top 20 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO from we_orderdetails a with (nolock) join we_orders b with (nolock) on a.orderid = b.orderid join we_Items c with (nolock) on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 2 and b.orderdatetime > '" & date-14 & "' group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by 2 desc"
'		session("errorSQL") = sql
'		set topProds = oConn.execute(sql)
'	end if
%>
	<div style="width:880px;position:relative;float:left;margin:20px auto; font-weight:bold; color:#1372c6; font-size:16px; padding:10px 0px 5px 0px;">
		<% if myBrand = 0 and myModel = 0 then %>Top Selling Products<% else %>Recommended Products<% end if %>
	</div>
	<div style="position:relative;float:left;;width:880px; margin-bottom:10px; margin:20px 0 0 0;">
		<%
		prodLap = 0
		totalProdCnt = 0
		do while not topProds.EOF
			if fs.fileExists(server.MapPath("/productpics/thumb/" & topProds("itempic"))) then
				prodLap = prodLap + 1
				totalProdCnt = totalProdCnt + 1
				prodDetails = split(topProds("itemDesc"),"##")
				itemID = prepInt(topProds("itemID"))
				itemDesc_CO = prodDetails(1)
				price_CO = prepInt(topProds("price_our"))
				itemPic_CO = topProds("itempic")
				if len(itemDesc_CO) > 50 then showName = left(itemDesc_CO,50) & "..." else showName = itemDesc_CO
		%>
		<div title="<%=itemDesc_CO%>" style="position:relative;float:left; width:135px; height:180px; padding:10px 5px 0px 5px; <% if prodLap < 6 then %>border-right:1px dotted #999; <% end if %>border-bottom:1px solid #999;">
			<div style="position:relative;float:left; width:130px; text-align:center;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html"><img src="/productpics/thumb/<%=itemPic_CO%>" border="0" /></a></div>
			<div style="position:relative;float:left; width:130px; text-align:left; height:50px; padding-top:5px;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html" style="color:#000;"><%=showName%></a></div>
			<div style="position:relative;float:left; width:130px; text-align:left; padding-top:10px;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html" style="color:#1372c6; font-weight:bold;"><%=formatCurrency(price_CO,2)%></a></div>
		</div>
		<%
				if prodLap = 6 then prodLap = 0
				if totalProdCnt = 12 then exit do
			end if
			topProds.movenext
		loop
		%>
	</div>
</div>
<!--#include virtual="/includes/template/bottom.asp"-->
