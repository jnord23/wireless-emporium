<%
	response.buffer = true
	response.expires = 70
	response.CacheControl = "public"

	pageTitle = "Brand"
	
	dim brandid : brandID = prepInt(request.querystring("brandid"))
	dim cbSort : cbSort = prepStr(request.Form("cbSort"))
	dim noLeftSide : noLeftSide = 1
	
	if brandID = 17 or brandID = 14 or brandID = 20 or brandID = 4 or brandID = 5 or brandID = 9 or brandID = 18 then
		brandAlt = 2
	else
		brandAlt = 0
	end if
	if brandID = 17 then
		abCode = "1575435796"
	elseif brandID = 14 then
		abCode = "2846309030"
	elseif brandID = 20 then
		abCode = "3439653655"
	elseif brandID = 4 then
		abCode = "2074872685"
	elseif brandID = 5 then
		abCode = "4064347951"
	elseif brandID = 9 then
		abCode = "3503307433"
	elseif brandID = 18 then
		abCode = "1213573139"
	end if
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
	siteId = 2
	cmsData(request.ServerVariables("HTTP_X_REWRITE_URL"))
	metaArray = split(session("metaTags"),"##")
	SEtitle = metaArray(0)
	SEdescription = metaArray(1)
	SEkeywords = metaArray(2)
	SeTopText = metaArray(3)
	SeBottomText = metaArray(4)
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<%
	set fso = CreateObject("Scripting.FileSystemObject")
	Set Img = Server.CreateObject("Persits.Jpeg")
	set Jpeg = Server.CreateObject("Persits.Jpeg")
	Jpeg.Quality = 60
	Jpeg.Interpolation = 1

	if brandID = 0 then PermanentRedirect("/")
	if cbSort = "" then
		if brandID = 20 then cbSort = "AZ" else cbSort = "NO"
	end if
	
	response.Cookies("myBrand") = brandID
	response.Cookies("myModel") = 0
	
	leftGoogleAd = 1
	
	'call redirectURL("b", brandid, request.ServerVariables("HTTP_X_REWRITE_URL"), "")
	
	sql = 	"	SELECT 	DISTINCT C.brandName, C.brandImg, A.topModel, A.temp, A.modelID, A.modelName, A.modelImg, releaseYear, a.releaseQuarter, a.isPhone, a.isTablet" & vbcrlf & _
			"	FROM 	we_models A join we_items B " & vbcrlf & _
			"		ON 	A.modelID = B.modelID join we_brands C " & vbcrlf & _
			"		ON 	A.brandid = C.brandid " & vbcrlf & _
			"	WHERE 	a.hideLive = 0 and C.brandid = " & brandID & " AND B.hideLive = 0  " & vbcrlf & _
			"		AND a.modelname not like 'all models%' and B.inv_qty <> 0 " & vbcrlf
	if brandid <> 17 then
		sql = sql & "		AND a.isTablet = 0" & vbcrlf
	end if			

	
	listSQL = sql & " order by a.modelname"
	session("errorSQL") = listSQL
	set listRS = oConn.execute(listSQL)
	
	select case cbSort
		case "AZ" : sql = sql & " order by a.modelname"
		case "ZA" : sql = sql & " order by a.modelname desc"
		case "NO" : sql = sql & " order by a.isPhone desc, a.isTablet, a.releaseYear desc, a.releaseQuarter desc"
		case "ON" : sql = sql & " order by a.releaseYear, a.releaseQuarter"
	end select
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	if RS.eof then response.redirect("/?ec=202")
	
	dim brandName, brandImg, lap, productList
	productList = ""
	lap = 0
	do while not RS.EOF
		lap = lap + 1
		if lap = 1 then
			brandName = RS("brandName")
			brandImg = RS("brandImg")
		end if
		productList = productList & RS("modelID") & "*" & RS("modelName") & "*" & RS("modelImg") & "#"
		RS.movenext
	loop
	
	productArray = split(productList,"#")
	itemImgPath = formatSEO(brandName) & "_" & cbSort & "_all.jpg"
	
	'### Start test of stitch date ###
	if fso.FileExists(Server.MapPath("/images/stitch/brands/" & itemImgPath)) then
		set fsTemp = fso.GetFile(Server.MapPath("/images/stitch/brands/" & itemImgPath))
		createDate = fsTemp.DateLastModified
		if datediff("h",createDate,now) > 1 then
			fso.deleteFile(Server.MapPath("/images/stitch/brands/" & itemImgPath))
'			response.Write("<!-- pop file deleted -->")
		else
'			response.Write("<!-- pop file valid -->")
		end if
	end if
	'### End test of stitch date ###
	if not fso.FileExists(Server.MapPath("/images/stitch/brands/" & itemImgPath)) then
		'create single product image
		imgWidth = 70 * (lap)
		Jpeg.New imgWidth, 112, &HFFFFFF
		imgX = 0
		imgY = 0
		for i = 0 to (ubound(productArray) - 1)
			curProductArray = split(productArray(i),"*")
			useNA = 0
			inner_itemImgPath = Server.MapPath("/modelPics/thumbs") & "\" & curProductArray(2)
			inner_itemImgPath2 = Server.MapPath("/modelPics") & "\" & curProductArray(2)
			if not fso.FileExists(inner_itemImgPath) then
				if not fso.FileExists(inner_itemImgPath2) then
					useNA = 1
				else
					session("errorSQL") = "inner_itemImgPath2:" & inner_itemImgPath2 & "<br>"
					Jpeg.Open inner_itemImgPath2
					Jpeg.Height = 112
					Jpeg.Width = 70
					Jpeg.Save inner_itemImgPath
				end if
			end if
			if useNA = 1 then
				Img.Open Server.MapPath("/modelPics/thumbs/modelna.jpg")
				Jpeg.Canvas.DrawImage X + (imgX)/2, Y + (imgY)/2, Img
				imgX = imgX + 140
			elseif not isnull(curProductArray(2)) and len(curProductArray(2)) > 0 then
				Img.Open Server.MapPath("/modelPics/thumbs/") & "\" & curProductArray(2)
				Jpeg.Canvas.DrawImage X + (imgX)/2, Y + (imgY)/2, Img
				imgX = imgX + 140
			end if
		next
		Jpeg.Save Server.MapPath("/images/stitch/brands/" & itemImgPath)
	end if
	
	dim strBreadcrumb, headerImgAltText
	strBreadcrumb = brandName & " Cell Phone Accessories"
	headerImgAltText = brandName & " Cell Phone Accessories"
	
	phoneList = ""
	if isarray(productArray) then
		for i = 0 to (ubound(productArray) - 1)
			curProductArray = split(productArray(i),"*")
			if phoneList = "" then
				phoneList = """" & curProductArray(1) & """"
			else
				phoneList = phoneList & ", """ & curProductArray(1) & """"
			end if
		next	
	end if
%>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td style="padding:0px 20px; border-bottom:1px solid #CCC;">
            <a class="top-sublink-gray" href="/">Cell Phone Accessories&nbsp;>&nbsp;</a>
            <span class="top-sublink-blue"><%=brandName%> Cell Phone Accessories</span>
        </td>
    </tr>
    <tr>
        <td width="100%" align="center" style="padding:5px 0px 5px 0px;" title="<%=headerImgAltText%>">
        	<table border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<td><img src="/images/logos/<%=brandID%>.png" border="0" width="214" height="74" /></td>
                    <td style="padding-left:10px;">
                    	<h1 style="color:#000; font-size:24px; margin:0px; padding:0px;">
                            <%
                            if brandid = "17" then
                                response.write "Apple iPhone, iPad & iPod Accessories"
                            else
                                response.write brandName & " Cell Phone Accessories"
                            end if
                            %>
                        </h1>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td style="padding-left:20px;">
        	<div style="float:left;"><img src="/images/backgrounds/gray-bar-left.png" border="0" width="22" height="64" /></div>
            <div style="float:left; height:64px; width:900px; background-image:url(/images/backgrounds/gray-bar-background.png)">
            	<div style="float:left;"><img src="/images/icons/gray-bar-bluedot2.png" border="0" width="26" height="64" /></div>
            	<div style="float:left; margin-top:27px; font-weight:bold; padding-left:10px; font-size:11px;">Select Your Cell Phone Model Below.</div>
                <div style="float:left; margin-top:25px; padding-left:20px;">
                	<form name="frmSort" method="post">
                        <select name="cbSort" onChange="this.form.submit();">
                            <option value="AZ" <%if cbSort = "AZ" then %>selected<% end if%>>Sort By A to Z</option>
                            <option value="ZA" <%if cbSort = "ZA" then %>selected<% end if%>>Sort By Z to A</option>
                            <option value="NO" <%if cbSort = "NO" then %>selected<% end if%>>Sort By Newest to Oldest</option>
                            <option value="ON" <%if cbSort = "ON" then %>selected<% end if%>>Sort By Oldest to Newest</option>
                        </select>                                                	
                    </form>
                </div>
                <div style="float:left; padding-left:10px;"><img src="/images/backgrounds/gray-bar-or.png" border="0" width="44" height="64" /></div>
                <div style="float:left; margin-top:25px; padding-left:10px;">
                	<select name="selectModel" onChange="if(this.value != ''){window.location=this.value;}" style="width:150px;">
                        <option value="">Select from this list</option>
                        <%
                        do while not listRS.EOF
                            modelID = listRS("modelID")
                            modelName = listRS("modelName")
                        %>
                        <option value="<%="/m-" & modelID & "-" & formatSEO(brandName) & "-" & formatSEO(modelName) & "-cell-phone-accessories.html"%>"><%=modelName%></option>
                        <%
							listRS.movenext
                        loop
                        %>
                    </select>
                </div>
            </div>
            <div style="float:left;"><img src="/images/backgrounds/gray-bar-right.png" border="0" width="22" height="64" /></div>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <%
                    a = 0
					row = 1
					useX = 0
                    dim altText
					'Check stitch file size
					chkFilePath = Server.MapPath("/images/stitch/brands/" & itemImgPath)
					imgWidth = ubound(productArray) * 70
					if fso.FileExists(chkFilePath) then
						Jpeg.Open chkFilePath
						if cint(imgWidth) <> cint(Jpeg.Width) and imgWidth > 0 then
							fso.DeleteFile(chkFilePath)
							response.Redirect("http://" & request.ServerVariables("SERVER_NAME") & request.ServerVariables("HTTP_X_REWRITE_URL"))
						end if
					end if
                    for i = 0 to (ubound(productArray) - 1)
						curProductArray = split(productArray(i),"*")
                        a = a + 1
						altText = brandName & " Cell Phone Accessories: " & brandName & curProductArray(1) & " Accessories"
                    %>
                    <td valign="top" style="<% if a < 6 then %>border-right:1px solid #CCC;<% end if %> border-bottom:1px solid #ccc;">
                        <div style="width:160px; margin:10px 0px 10px 0px;">
	                        <div style="margin-left:50px;"><a href="/m-<%=curProductArray(0) & "-" & formatSEO(brandName) & "-" & formatSEO(curProductArray(1))%>-cell-phone-accessories.html"><div id="PhonePic<%=a%>" style="width: 70px; height: 112px; background: url(/images/stitch/brands/<%=itemImgPath%>) <%=useX%>px 0px no-repeat; cursor:pointer;" title="<%=altText%>"></div></a></div>
    	                    <div style="text-align:center; margin:5px 5px 0px 5px; width:160px;"><a style="color:#2d5183; text-decoration:underline; font-size:12px;" href="/m-<%=curProductArray(0) & "-" & formatSEO(brandName) & "-" & formatSEO(curProductArray(1))%>-cell-phone-accessories.html" title="<%=brandName & " " & curProductArray(1)%> Cell Phone Accessories"><%=curProductArray(1)%></a></div>
                        </div>
                    </td>
                    <%
						useX = useX - 70
                        if a = 6 then
                            response.write "</tr><tr>"
                            a = 0
							row = row + 1
                        end if
                    next
                    %>
                </tr>
            </table>
        </td>
    </tr>
	<% if SeBottomText <> "" then %>
    <tr>
        <td colspan="4" align="left" class="static-content-font" style="padding-top:20px;"><div style="border:1px solid #CCC; padding:10px;"><%=SeBottomText%></div></td>
    </tr>
    <% end if %>
    <tr><td style="padding:10px 0px 50px 0px; font-size:10px;"><%=topText%></td></tr>
    <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
</table>
<script>
	function jumpto()
	{
		modelname = document.frmSearch.txtSearch.value;
		var x=document.frmSearch.selectModel;
		for (i=0;i<x.length;i++)
			if (x.options[i].text == modelname) window.location = x.options[i].value;
	}
</script>
<!--#include virtual="/includes/template/bottom_index.asp"-->