<%
response.buffer = true
response.Expires = -1
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
noLeftSide = 1
noCommentBox = true
pageTitle = "nextopia search"
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<div style="clear:both; height:10px;"></div>
<div id="sideNav" style="float:left; width:200px;"></div>
<div id="mainContent" style="float:left; width:800px; padding-left:10px;">
	<div style="width:100%; text-align:center;"><img src="//d2brc35ftammet.cloudfront.net/img/loading-grey.gif" /></div>
</div>
<div style="clear:both; height:100px;"></div>

<script>
	var nxtBtnTimer;
	
	function getNxtUrl(hrefString) {
		var ret = hrefString;
		var searchKeyword = "NULL";
		if (getQsVars("")["search"] != undefined) searchKeyword = getQsVars("")["search"];
		var itemid = hrefString.substring(hrefString.indexOf("/p-")+3);
		itemid = itemid.substring(0,itemid.indexOf("-"));
		var tracking = "IZID=NXTPIA_" + itemid + "_" + searchKeyword + "&utm_source=Nextopia&utm_medium=INT"
						
		if (hrefString.indexOf(".html?") != -1) {
			ret = hrefString.substring(0, hrefString.indexOf(".html?")+5) + "?" + tracking;
		} else {
			ret = hrefString + "?" + tracking;
		}
		
		return ret;
	}
	
	function nxtButtonSwap() {
		$('.prod-name').each(function(index) {
			try {
				var hrefString = $(this).attr("href").toString();
//				if (hrefString.indexOf("?IZID") == -1)
					$(this).attr("href",getNxtUrl(hrefString));

			} catch (e) {}
		});		

		$('.nxt_image_wrapper').each(function(index) {
			try {
				var hrefString = $(this).find('a').attr("href").toString();
//				if (hrefString.indexOf("?IZID") == -1) 
					$(this).find('a').attr("href",getNxtUrl(hrefString));
				
			} catch (e) {}
		});		
		
		clearTimeout(nxtBtnTimer);
		nxtBtnTimer = setTimeout('nxtButtonSwap()', 1000);
	}
	
	$(document).ready(function() {
		nxtButtonSwap();
	});
</script>
<!--#include virtual="/includes/template/bottom.asp"-->
