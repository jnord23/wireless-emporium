<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
curPageURL = replace(request.ServerVariables("HTTP_X_REWRITE_URL"),"-new","")
PermanentRedirect(curPageURL)
'abUser = prepInt(request.Cookies("abProductPage"))
'if abUser = 0 then
'	randomize
'	abUser = int(rnd*2)+1
'	response.Cookies("abProductPage") = abUser
'end if
'if abUser = 2 then
'	curPageURL = request.ServerVariables("HTTP_X_REWRITE_URL")
'	curPageURL = mid(curPageURL,instr(curPageURL,itemID)+len(itemID))
'	newPageURL = "/p-" & itemID & "-new" & curPageURL
'	PermanentRedirect(newPageURL)
'end if
showReviewPopup = false
utm_campaign = request("utm_campaign")
if lcase(utm_campaign) = "co14dayreview" then showReviewPopup = true

dim itemid, productPage
dim curPageName : curPageName = "Product Page"
productPage = 1
itemid = prepInt(request.querystring("itemid"))
if itemid = 0 then server.transfer("404error.asp")
dim musicSkins : musicSkins = 0
if itemid => 1000000 then musicSkins = 1

dim modelID, typeID

if musicSkins = 1 then 'hack in original pricing as constant .. currently no blow-out support for this table [knguyen/20110602]
	SQL = 	"select '' as itemDimensions, cast('True' as bit) as alwaysInStock, '' as POINT1, '' as POINT2, '' as POINT3, '' as POINT4, '' as POINT5, '' as POINT6, '' as POINT7, '' as POINT8, '' as POINT9, '' as POINT10, " &_
			"'' as COMPATIBILITY, '' as itemLongDetail_CO, 0 as HandsfreeType, null as ItemKit_NEW, 'musicSkins' as vendor, '' as UPCCode, 'new' as Condition, " &_
			"1 as NoDiscount, c.defaultImg, c.sampleImg, c.musicSkinsID as partNumber, c.brandID, 20 as typeID, c.image as itemPic, c.image as itemPic_co, " &_
			"100 as inv_qty, c.id as itemID, 'false' as hideLive, c.price_co, c.msrp as price_retail, " &_
			"c.brand + ' ' + c.model + ' ' + c.artist + ' ' + c.designName + ' Music Skins' as itemDesc_co, b.modelID, 20 as typeID, b.modelName, b.modelImg, c.brand as brandName, " &_
			"'music skins' as typeName, 'OriginalPrice' as [ActiveItemValueType], c.price_we as [OriginalPrice], c.preferredImg from we_items_musicSkins c left join we_models b " &_
			"on c.modelID = b.modelID where c.id = '" & itemID & "'"
else
	SQL = "SELECT e.alwaysInStock, (select top 1 inv_qty from we_items where partNumber = a.partNumber and master = 1) as masterQty, A.*, isnull(B.modelName, 'Universal') modelName, B.modelImg, isnull(C.brandName, 'Universal') brandName, D.typeName_CO typename FROM ((we_Items A left outer join we_models B ON A.modelID=B.modelID)"
	SQL = SQL & " left outer join we_brands C ON A.brandID=C.brandID)"
	SQL = SQL & " join we_types D ON A.typeID=D.typeID left join we_pnDetails e on a.partNumber = e.partNumber"
	SQL = SQL & " WHERE A.itemID='" & itemid & "'"
	SQL = SQL & " AND A.HideLive = 0 and A.ghost = 0"
	SQL = SQL & " AND A.price_CO > 0"
end if
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3

if not RS.eof then
	dim partnumber, brandID, itemDesc_CO, itempic_CO, price_retail, price_CO, itemLongDetail_CO, KIT, COMPATIBILITY, download_URL, download_TEXT, myPartNumber
	dim modelName, modelImg, brandName, categoryName, handsFreeType, upcCode, condition, noDiscount, defaultImg, sampleImg, inv_qty, hideLive
	dim ActiveItemValueType, OriginalPrice, preferredImg
	dim POINT(10)
	itemDimensions = RS("itemDimensions")	
	if isnull(rs("alwaysInStock")) then alwaysInStock = False else alwaysInStock = rs("alwaysInStock")
	if musicSkins = 1 then
		partnumber = "CO" & cDbl(itemid) + 250
		COMPATIBILITY = RS("COMPATIBILITY")
		itemLongDetail_CO = RS("itemLongDetail_CO")
		handsFreeType = RS("HandsfreeType")
		KIT = RS("ItemKit_NEW")
		vendor = RS("vendor")
		upcCode = RS("UPCCode")
		condition = RS("Condition")
		noDiscount = RS("NoDiscount")
		defaultImg = RS("defaultImg")
		sampleImg = RS("sampleImg")
		myPartNumber = RS("partNumber")
		brandID = RS("brandID")
		typeID = RS("typeID")
		itempic_CO = RS("itempic_CO")
		itempic = RS("itempic")
		prodQty = RS("inv_qty")
		hideLive = RS("hideLive")
		price_CO = RS("price_CO")
		price_retail = RS("price_retail")
		itemDesc_CO = RS("itemDesc_co")
		typeID = 20
		modelID = RS("modelID")
		modelName = RS("modelName")
		modelImg = RS("modelImg")
		brandName = RS("brandName")
		prodTypeName = RS("typeName")
		ActiveItemValueType = RS("ActiveItemValueType")
		OriginalPrice = RS("OriginalPrice")
		preferredImg = RS("preferredImg")
		partNumber = myPartNumber
		itemDesc_CO = insertDetails(itemDesc_CO)
		for a = 1 to 10
			POINT(a) = RS("POINT" & a)
		next
	else
		noDiscount = rs("noDiscount")
		prodQty = rs("masterQty")
		myPartNumber = RS("partnumber")
		partnumber = "CO" & cDbl(itemid) + 250
		brandID = RS("brandID")
		typeID = RS("typeID")
		itemDesc_CO = RS("itemDesc_CO")
		itempic_CO = RS("itempic_CO")
		itempic_WE = RS("itempic")
		price_retail = RS("price_retail")
		price_CO = RS("price_CO")
		itemLongDetail_CO = RS("itemLongDetail_CO")
		KIT = RS("ItemKit_NEW")
		COMPATIBILITY = RS("COMPATIBILITY")
		typeID = RS("typeID")
		modelID = RS("modelID")
		modelName = RS("modelName")
		modelImg = RS("modelImg")
		brandName = RS("brandName")
		categoryName = RS("typeName")
		vendor = rs("vendor")
		partNumber = myPartNumber
		itemDesc_CO = insertDetails(itemDesc_CO)
		for a = 1 to 10
			POINT(a) = RS("POINT" & a)
		next
	end if
else
	server.transfer("404error.asp")
end if

if isnull(session("curModelID")) or len(session("curModelID")) < 1 then session("curModelID") = modelID
if session("curModelID") <> modelID then modelID = session("curModelID")

sql = "select top 2 itemID, itemPic_co, itemDesc_CO, price_co from we_items where modelID = " & modelID & " and typeID <> " & typeID & " and typeID <> 16 and hideLive = 0 and inv_qty <> 0 and price_co is not null and price_co > 0 order by numberOfSales desc"
session("errorSQL") = sql
set testamonialsRS = oConn.execute(sql)

sql = "select top 5 itemID, itemPic_co, itemDesc_CO, price_co from we_items where modelID = " & modelID & " and typeID <> " & typeID & " and typeID <> 16 and hideLive = 0 and inv_qty <> 0 and price_co is not null and price_co > 0 order by numberOfSales desc"
session("errorSQL") = sql
set relatedRS = oConn.execute(sql)

'############# Start Recently Viewed Products ###############
'response.Cookies("recentItemList") = ""
recentItemList = prepStr(request.Cookies("recentItemList"))
if instr(recentItemList,",") > 0 then
	recentItemArray = split(recentItemList,",")
	recentItemCnt = ubound(recentItemArray)+1
elseif recentItemList <> "" then
	recentItemCnt = 1
else
	recentItemCnt = 0
end if
if recentItemList = "" then
	sql = "select * from we_brands where brandID = 9999"
else
	sql = "SELECT TOP 3 b.brandName, c.modelName, a.partNumber,a.itemPic,a.itemID,a.itemDesc_CO,a.itempic_CO,a.price_Retail,a.price_CO FROM we_Items a left join we_brands b on a.brandID = b.brandID left join we_models c on a.modelID = c.modelID WHERE ItemID in (" & recentItemList & ")"
end if
session("errorSQL") = sql
set recentRS = oConn.execute(sql)

itemID = prepInt(itemID)
if recentItemCnt > 2 then
	if prepInt(recentItemArray(0)) <> itemID and prepInt(recentItemArray(1)) <> itemID and prepInt(recentItemArray(2)) <> itemID then response.Cookies("recentItemList") = recentItemArray(1) & "," & recentItemArray(2) & "," & itemID
elseif recentItemCnt > 0 then
	if recentItemCnt = 1 then
		if prepInt(recentItemList) <> itemID then response.Cookies("recentItemList") = recentItemList & "," & itemID
	else
		if prepInt(recentItemArray(0)) <> itemID and prepInt(recentItemArray(1)) <> itemID then response.Cookies("recentItemList") = recentItemList & "," & itemID
	end if
else
	response.Cookies("recentItemList") = itemID
end if
'############# End Recently Viewed Products ###############

dim contentText, bottomText

if SEtitle = "" then SEtitle = brandName & " " & modelName & " " & singularSEO(categoryName) & ": " & itemDesc_CO
'if SEdescription = "" then SEdescription = "CellularOutfitter.com is the #1 store online offering discounted cell phone accessories like " & brandName & " " & modelName & " " & nameSEO(categoryName) & " & other " & brandName & " " & modelName & " accessories at wholesale prices."
if SEdescription = "" then SEdescription = "CellularOutfitter.com is the #1 store online offering discounted cell phone accessories like " & itemDesc_CO & " & other " & brandName & " " & modelName & " accessories at wholesale prices."
if SEkeywords = "" then SEkeywords = brandName & " " & modelName & " Accessories, " & brandName & " " & modelName & " " & nameSEO(categoryName) & ", " & brandName & " " & modelName & " " & singularSEO(categoryName) & ", " & itemDesc_CO
if contentText = "" then
	contentText = "Buy " & brandName & " " & modelName & " " & nameSEO(categoryName) & " &amp; other " & brandName & " " & modelName & " accessories at wholesale prices."
	contentText = contentText & " We offer quality products for your cell phone which give an optimal performance at unbeatable prices. We carry cutting-edge accessories for your cell phone, which you simply won't find at most retailers. We carry not only the must-have items for the most popular phones, but also the more obscure items that you normally wouldn't have thought of."
	contentText = contentText & " Get great deals on " & brandName & " " & modelName & " " & nameSEO(categoryName) & " &amp; other " & brandName & " " & modelName & " accessories. Shop around and see what we mean!"
end if

dim strBreadcrumb
strBreadcrumb = brandName & " " & modelName & " " & singularSEO(categoryName)

set fso = CreateObject("Scripting.FileSystemObject")
dim jpeg : set jpeg = Server.CreateObject("Persits.Jpeg")

itemImgPath = server.MapPath("/productpics/big/" & itempic_CO)
if musicSkins = 1 then
	if isnull(preferredImg) then																					
		if hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsLarge", itempic_CO) then
			sql = "update we_items_musicSkins set preferredImg = 1 where id = " & itemID
			session("errorSQL") = sql
			oConn.execute(sql)
			useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
		elseif hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault", defaultImg) then
			sql = "update we_items_musicSkins set preferredImg = 2 where id = " & itemID
			session("errorSQL") = sql
			oConn.execute(sql)
			useImg = "http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault/" & defaultImg
		else
			useImg = "/productPics/thumb/imagena.jpg"
		end if
	elseif preferredImg = 1 then
		useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
	elseif preferredImg = 2 then
		useImg = "/productpics/musicSkins/musicSkinsDefault/" & defaultImg
	else
		useImg = "/productPics/thumb/imagena.jpg"
	end if
elseif instr(myPartNumber,"DEC-SKN") > 0 then
	useImg = "/weImages/big/" & itempic_WE
else
	if not fso.FileExists(itemImgPath) then
		useImg = "/productPics/big/imagena.jpg"
	else
		useImg = "/productPics/big/" & itempic_CO
	end if
end if
noLeftSide = 1
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<style>
	h1 {	color: #333333;}
	.product-review 	{	font-size: 11px; font-weight: normal; text-decoration: none;	}
	.prodTab_off { float:left; border-top:1px solid #ccc; border-bottom:1px solid #ccc; height:25px; background-color:#f0f0f0; cursor:pointer; }
	.prodTab_on { float:left; border-top:1px solid #ccc; height:26px; background-color:#fff; }
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<!--#include virtual="/includes/asp/inc_breadcrumbs.asp"-->
    <tr>
    	<td style="padding-top:10px;">
            <div style="width:300px; float:left;">
            	<div style="float:left;"><img id="imgLarge" src="<%=useImg%>" border="0" width="300" height="300" /></div>
                <div style="width:300px; height:70px; padding-top:10px; float:left;">
                	<%
					curPic = mid(useImg,instrrev(useImg,"/")+1)
					if fso.fileExists(server.MapPath(useImg)) then
						if not fso.fileExists(server.MapPath("/productPics/alts/thumbs/" & curPic)) then
							jpeg.Open Server.MapPath(useImg)
							jpeg.Height = 60
							jpeg.Width = 60
							jpeg.Save Server.MapPath("/productPics/alts/thumbs/" & curPic)
						end if
					end if
					%>
					<div style="float:left; cursor:pointer; border:1px solid #CCC; margin-right:3px;" onmouseover="fnPreviewImage('<%=useImg%>')"><img src="/productPics/alts/thumbs/<%=curPic%>" width="60" height="60" /></div>
                    <%
					for i = 0 to 3
						useImg = "/altPics/" & itempic_WE
						altMainPic = replace(replace(useImg,".jpg","-" & i & ".jpg"),"/big/","/altImgs/")
						if fso.fileExists(server.MapPath(altMainPic)) then
							curPic = mid(altMainPic,instrrev(altMainPic,"/")+1)
							if not fso.fileExists(server.MapPath("/productPics/alts/thumbs/" & curPic)) then
								jpeg.Open Server.MapPath(altMainPic)
								jpeg.Height = 60
								jpeg.Width = 60
								jpeg.Save Server.MapPath("/productPics/alts/thumbs/" & curPic)
							end if
					%>
                    <div style="float:left; cursor:pointer; border:1px solid #CCC; margin-right:3px;" onmouseover="fnPreviewImage('<%=altMainPic%>')"><img src="/productPics/alts/thumbs/<%=curPic%>" width="60" height="60" /></div>
                    <%
						end if
					next
					curPic = ""
					if typeID = 17 then curPic = "gg_atl_image1.jpg"
					if typeID = 20 then curPic = "ms_altview1.jpg##ms_altview2.jpg"
					if typeID = 19 then curPic = "CO-DecalSkin2.jpg"
					if curPic <> "" then
						picArray = split(curPic,"##")
						for i = 0 to ubound(picArray)
							curPic = picArray(i)
							if not fso.fileExists(server.MapPath("/productPics/alts/thumbs/" & curPic)) then
								jpeg.Open Server.MapPath("/altPics/" & curPic)
								jpeg.Height = 60
								jpeg.Width = 60
								jpeg.Save Server.MapPath("/productPics/alts/thumbs/" & curPic)
							end if
					%>
                    <div style="float:left; cursor:pointer; border:1px solid #CCC; margin-right:3px;" onmouseover="fnPreviewImage('/altPics/<%=curPic%>')"><img src="/productPics/alts/thumbs/<%=curPic%>" width="60" height="60" /></div>
                    <%
						next
					end if
					%>
                </div>
            </div>
            <div style="float:left; padding-left:20px; width:400px;">
            	<div style="padding-bottom:10px;"><h1><%=itemDesc_CO%></h1></div>
                <div style="color:#999; font-size:12px; padding-bottom:10px;">Item No: <%=partNumber%></div>
                <div style="padding-bottom:10px; border-bottom:1px dotted #CCC;">
                	<%
					sql	=	"	select	orderid, itemid, rating, nickname, reviewTitle, review, partnumbers, approved, dateTimeEntd	" & vbcrlf & _
							"	from	co_reviews" & vbcrlf & _
							"	where	(itemid = '" & itemid & "' or partnumbers in ('" & myPartNumber& "', '" & left(myPartNumber,4) & "', '" & left(myPartNumber,8) & "', '" & left(myPartNumber,13) & "')) " & vbcrlf & _
							"		and approved = 1" & vbcrlf & _
							"	order by dateTimeEntd desc"
					session("errorSQL") = sql			
					set objRsReviews = oConn.execute(sql)
					
					avgRating = 0.0
					reviewCnt = 0
					if not objRsReviews.eof then
						do until objRsReviews.eof
							reviewCnt = reviewCnt + 1
							avgRating = cdbl(avgRating) + cdbl(objRsReviews("rating"))
							objRsReviews.movenext
						loop
						objRsReviews.movefirst
						avgRating = avgRating / reviewCnt
						response.write getRatingAvgStar(avgRating) & " &nbsp; " & formatnumber(avgRating,1) & " &nbsp; (" & formatnumber(reviewCnt,0) & " Reviews)<br>"
						response.write "<a style=""font-size:11px; text-decoration:underline;"" onclick=""showReview()"" href=""#prodTabs"">Read All Reviews</a> &nbsp;/&nbsp; <a style=""font-size:11px; text-decoration:underline;"" href=""#"" onclick=""showPopup('writeReview');return false;"">Write A Review</a>"
					else
						response.write getRatingAvgStar(avgRating) & " &nbsp; " & formatnumber(avgRating,1) & " &nbsp; (" & formatnumber(reviewCnt,0) & " Reviews)<br>"
						response.write "<a style=""font-size:11px; text-decoration:underline;"" href=""#"" onclick=""showPopup('writeReview');return false;"">Be the first to review this product!</a>"
					end if
					%>
                </div>
                <div style="font-weight:bold; padding:10px 0px 10px 0px;">Wholesale Price:</div>
                <div style="height:40px; padding-bottom:10px;">
	                <div style="float:left; font-weight:bold; color:#C00; font-size:36px;"><%=formatCurrency(price_CO,2)%></div>
                    <div style="float:left; padding:5px 0px 0px 10px;">
                    	<div style="font-size:13px; color:#C00;">Save <%=formatPercent((price_retail - price_CO) / price_retail,0)%></div>
                        <div style="font-size:13px; color:#999;">Retail Price: <span style="text-decoration:line-through;"><%=formatCurrency(price_retail,2)%></span></div>
                    </div>
                </div>
                <% if prodQty = 0 and not alwaysInStock then %>
                <div style="font-weight:bold; color:#F00; padding-bottom:10px;">Temporarily Out of Stock</div>
                <% else %>
                <form name="popSubmit" action="/cart/item_add.asp" method="post" style="margin:0px; padding:0px;">
                <div style="height:25px; padding-bottom:10px;">
	                <div style="float:left; font-size:14px; padding-top:3px;">Quantity:</div>
    	            <div style="float:left; padding-left:10px;"><input name="qty" type="text" value="1" size="2"></div>
                </div>
                <div style="padding-bottom:10px;">
                	<input type="hidden" name="prodid" value="<%=itemid%>">
                    <input type="image" src="/images/buttons/addToShoppingCart.png" border="0" width="304" height="46" />
                </div>
                </form>
                <% end if %>
                <div style="border-top:1px dotted #CCC; padding:10px 0px 10px 0px; height:50px; width:400px;">
                	<div style="float:left; width:17px;"><img src="/images/icons/checkmark.png" border="0" width="17" height="14" /></div>
                    <div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#666;">Leaves Our US Warehouse Within 24 Hours</div>
                    <div style="float:left; width:17px;"><img src="/images/icons/checkmark.png" border="0" width="17" height="14" /></div>
                    <div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#666;">Lowest Prices Online Guaranteed</div>
                    <div style="float:left; width:17px;"><img src="/images/icons/checkmark.png" border="0" width="17" height="14" /></div>
                    <div style="float:left; width:370px; padding-left:10px; font-size:14px; color:#666;">Unconditional 90 Day Return Policy</div>
                </div>
                <div style="padding-bottom:10px;"><img src="/images/icons/credit-card.png" border="0" width="254" height="26" /></div>
                <div style="position:absolute; padding-bottom:10px;">
                    <div style="float:left; margin-right:5px;">
                        <script src="http://platform.linkedin.com/in.js" type="text/javascript"></script>
                        <script type="IN/Share" data-url="<%=canonicalURL%>"></script>
                    </div>
                    <div style="float:left; margin-right:2px;" align="left">
                        <a style="vertical-align:bottom;" href="http://twitter.com/share" class="twitter-share-button" data-count="none" data-via="CellOutfitter">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
                    </div>
                    <div style="float:left; height:25px; width:47px; overflow:hidden;">
                        <fb:like href="<%=canonicalURL%>" send="false" layout="button_count" width="47" show_faces="false" font=""></fb:like>
                    </div>
                    <div style="float:left; margin:0px 2px 0px 5px;" align="left">
                        <g:plusone size="medium" count="false" href="<%=canonicalURL%>"></g:plusone>
                    </div>
                    <div style="float:left; padding:4px 5px 0px 5px; cursor:pointer;" onmouseover="plusOneBox(1)" onmouseout="plusOneBox(0)"><img src="/images/buttons/googlePlus1.gif" border="0" /></div>
                    <div style="float:left; z-index:99; position:relative; top:5px; left:7px; width:250px; border:2px solid #336699; color:#336699; background-color:#FFF; padding:5px; display:none; cursor:pointer;" id="plusOneDetails">
                        <div style="float:left; position:relative; top:-7px; left:-17px;"><img src="/images/buttons/plusOneArrow.gif" border="0" /></div>
                        <div style="text-align:left; padding:5px 5px 5px 13px; font-weight:bold; font-size:11px;">
                            The +1 button is shorthand for "this is pretty cool" or "you should check this out." Click +1 to publicly give something 
                            your stamp of approval. Your +1's can help friends, contacts, and others on the web find the best stuff when they search.
                        </div>
                    </div>
                </div>
            </div>
            <div style="float:right; width:200px; background-color:#f0f0f0; height:400px;">
            	<div style="font-weight:bold; font-size:14px; padding:10px 0px 10px 10px;">&bull; Testamonials</div>
                <%
				do while not testamonialsRS.EOF
					relatedItemID = testamonialsRS("itemID")
					relatedItemPic = testamonialsRS("itemPic_co")
					relatedItemDesc = testamonialsRS("itemDesc_CO")
					relatedPrice = testamonialsRS("price_co")
				%>
                <div style="background-color:#FFF; width:170px; margin-bottom:2px; padding:10px; margin-left:auto; margin-right:auto;">
                	<div style="width:150px; text-align:center;"><a href="/p-<%=relatedItemID%>-new-<%=formatSEO(relatedItemDesc)%>.html"><img src="/productpics/thumb/<%=relatedItemPic%>" border="0" width="100" height="100" /></a></div>
                    <div style="font-size:12px; width:150px; text-align:center;"><a href="/p-<%=relatedItemID%>-new-<%=formatSEO(relatedItemDesc)%>.html" style="color:#999;"><%=relatedItemDesc%></a></div>
                    <div style="font-size:12px; color:#C00; width:150px; text-align:center; font-weight:bold;"><%=formatCurrency(relatedPrice,2)%></div>
                </div>
                <%
					testamonialsRS.movenext
				loop
				
				sql = 	"select c.brandName, b.modelName from we_items a left join we_models b on a.modelID = b.modelID left join we_brands c on b.brandID = c.brandID where a.partNumber = '" & myPartNumber & "'" &_
						"union " &_
						"select c.brandName, b.modelName from we_relatedItems a left join we_Models b on a.modelid = b.modelID left join we_Brands c on b.brandID = c.brandID where itemID = " & itemID & " order by c.brandName, b.modelName"
				session("errorSQL") = sql
				set compRS = oConn.execute(sql)
				%>
            </div>
            <div style="float:left; width:1000px; border:1px solid #ccc; height:300px; margin-top:57px; position:relative;">
            	<div style="position:absolute; top:-27px; left:-1px;" id="prodTabs">
                	<div style="float:left;"><img id="prodDescLeft" src="/images/backgrounds/productTab_on_left.gif" border="0" width="28" height="27" /></div>
                    <div id="prodDescDiv" class="prodTab_on" onclick="showDetails()">Product Description</div>
                    <div style="float:left;"><img id="prodDescRight" src="/images/backgrounds/productTab_on_right.gif" border="0" width="28" height="27" /></div>
                    <div style="float:left;"><img id="prodCompLeft" src="/images/backgrounds/productTab_off_left.gif" border="0" width="28" height="27" /></div>
                    <div id="prodCompDiv" class="prodTab_off" onclick="showComp()">Compatibility</div>
                    <div style="float:left;"><img id="prodCompRight" src="/images/backgrounds/productTab_off_right.gif" border="0" width="28" height="27" /></div>
                    <div style="float:left;"><img id="prodReviewLeft" src="/images/backgrounds/productTab_off_left.gif" border="0" width="28" height="27" /></div>
                    <div id="prodReviewDiv" class="prodTab_off" onclick="showReview()">Reviews</div>
                    <div style="float:left;"><img id="prodReviewRight" src="/images/backgrounds/productTab_off_right.gif" border="0" width="28" height="27" /></div>
                </div>
                <div style="padding:10px; color:#333; font-size:12px; overflow:auto; height:280px;" id="prodDetails">
					<div><%=itemLongDetail_CO%></div>
                    <%
					for i = 1 to 10
						if prepStr(POINT(i)) <> "" then
					%>
                    <div style="padding-top:5px;">&bull;<span style="padding-left:10px;"><%=POINT(i)%></span></div>
                    <%
						end if
					next
					
					if not isnull(itemDimensions) or itemDimensions <> "" then
					%>
					<div style="padding-top:5px;">&bull;<span style="padding-left:10px;">Product Dimensions: </span><span style="font-size:13px; font-weight:bold;"><%=itemDimensions%></span></div>
					<%
					end if
					%>					
                    <div style="padding-top:5px;"><%=contentText%></div>
                </div>
                <div style="padding:10px; color:#333; font-size:12px; display:none; overflow:auto; height:280px;" id="prodComp">
                	<%
					compatLap = 0
					if not compRS.eof then
						do while not compRS.EOF
							compatLap = compatLap + 1
							if compatLap > 6 then compatLap = 1
							if compatLap < 4 then bgColor = "#fff" else bgColor = "#ebebeb"
					%>
					<div style="float:left; width:200px; font-size:11px; padding-right:15px; background-color:<%=bgColor%>; height:15px;"><%=compRS("brandName")%>&nbsp;<%=compRS("modelName")%></div>
					<%
							compRS.movenext
						loop
					else
					%>
					<div style="float:left; width:200px; font-size:11px; padding-right:15px; background-color:<%=bgColor%>; height:15px;"><%=brandName%>&nbsp;<%=modelName%></div>
                    <%
					end if
					%>
                </div>
                <div style="padding:10px; color:#333; font-size:12px; display:none; overflow:auto; height:280px;" id="prodReview">
                	<% if objRsReviews.EOF then %>
                    Currently no reviews available
                    <%
					else
						bgColor = "#fff"
						do while not objRsReviews.EOF
							rating = objRsReviews("rating")
							nickname = objRsReviews("nickname")
							reviewTitle = objRsReviews("reviewTitle")
							review = objRsReviews("review")
							entryDate = objRsReviews("dateTimeEntd")
							objRsReviews.movenext
					%>
                    <div>
                    	<div style="height:20px; padding:3px; background-color:#333; color:#FFF;">
	                        <div style="float:left; padding-right:5px; border-right:1px solid #999;">Posted By: <%=nickname%></div>
    	                    <div style="float:left; padding-left:5px;"><%=entryDate%></div>
                        </div>
						<div style="font-weight:bold; padding-bottom:5px; font-size:24px;"><%=reviewTitle%></div>
                        <div style="padding-bottom:5px;"><%=getRatingAvgStar(rating) & " &nbsp; " & formatnumber(rating,1)%></div>
                        <div style="padding-bottom:5px;"><%=review%></div>
                    </div>
                    <%
							if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
						loop
					end if
					%>
                </div>
            </div>
            <div style="float:left; width:1000px; padding:10px; text-align:center;"><img src="/images/icons/trust.jpg" border="0" /></div>
            <div style="float:left; width:870px; height:185px; background-color:#CCC; padding:10px; text-align:center; margin:10px 50px 20px 50px;">
            	<div style="font-weight:bold; font-size:14px; width:100%; text-align:left; padding-bottom:5px;">&bull; Related Items</div>
				<%
				rlap = 0
				do while not relatedRS.EOF
					rlap = rlap + 1
					relatedItemID = relatedRS("itemID")
					relatedItemPic = relatedRS("itemPic_co")
					relatedItemDesc = relatedRS("itemDesc_CO")
					relatedPrice = relatedRS("price_co")
				%>
                <div style="background-color:#FFF; width:170px; float:left;<% if rlap < 5 then %> margin-right:5px;<% end if %>">
                	<div style="width:150px; text-align:center;"><a href="/p-<%=relatedItemID%>-new-<%=formatSEO(relatedItemDesc)%>.html"><img src="/productpics/thumb/<%=relatedItemPic%>" border="0" width="100" height="100" /></a></div>
                    <div style="font-size:12px; width:150px; text-align:center; height:50px;"><a href="/p-<%=relatedItemID%>-new-<%=formatSEO(relatedItemDesc)%>.html" style="color:#999;"><%=relatedItemDesc%></a></div>
                    <div style="font-size:12px; color:#C00; width:150px; text-align:center; font-weight:bold;"><%=formatCurrency(relatedPrice,2)%></div>
                </div>
                <%
					relatedRS.movenext
				loop
				%>
            </div>
            <div style="float:left; width:870px; height:185px; background-color:#CCC; padding:10px; text-align:center; margin:10px 50px 20px 50px;">
            	<div style="font-weight:bold; font-size:14px; width:100%; text-align:left; padding-bottom:5px;">&bull; Recently Viewed Items</div>
				<%
				rlap = 0
				do while not recentRS.EOF
					rlap = rlap + 1
					relatedItemID = recentRS("itemID")
					relatedItemPic = recentRS("itemPic_co")
					relatedItemDesc = recentRS("itemDesc_CO")
					relatedPrice = recentRS("price_co")
				%>
                <div style="background-color:#FFF; width:170px; float:left;<% if rlap < 5 then %> margin-right:5px;<% end if %>">
                	<div style="width:150px; text-align:center;"><a href="/p-<%=relatedItemID%>-new-<%=formatSEO(relatedItemDesc)%>.html"><img src="/productpics/thumb/<%=relatedItemPic%>" border="0" width="100" height="100" /></a></div>
                    <div style="font-size:12px; width:150px; text-align:center; height:50px;"><a href="/p-<%=relatedItemID%>-new-<%=formatSEO(relatedItemDesc)%>.html" style="color:#999;"><%=relatedItemDesc%></a></div>
                    <div style="font-size:12px; color:#C00; width:150px; text-align:center; font-weight:bold;"><%=formatCurrency(relatedPrice,2)%></div>
                </div>
                <%
					recentRS.movenext
				loop
				%>
            </div>
			<!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
        </td>
    </tr>
</table>
<!--#include virtual="/includes/asp/inc_productReview.asp"-->
<%
function hasImageOnRemote(pRemoteURL, pFileName)
	dim isExists : isExists = false
	dim oXMLHTTP : set oXMLHTTP = server.CreateObject("Msxml2.ServerXMLHTTP")
	dim strTemp : strTemp = pRemoteURL & "/" & pFileName
	
	oXMLHTTP.Open "HEAD", strTemp, False	'communicating without responseBody, only looks up file header
	oXMLHTTP.Send
	
	if (oXMLHTTP.readyState = "4") and (oXMLHTTP.status = "200") and (instr(oXMLHTTP.getResponseHeader("Content-Type"), "image") > 0) then
		isExists = true
	end if

	set oXMLHTTP = Nothing
	
	hasImageOnRemote = isExists
	
end function
%>
<!--#include virtual="/includes/template/bottom.asp"-->
<script language="javascript">
	function plusOneBox(dir) {
		if (dir == 1) {
			document.getElementById('plusOneDetails').style.display = ''
		}
		else {
			document.getElementById('plusOneDetails').style.display = 'none'
		}
	}
	
	function fnPreviewImage(src) {
		var p = document.getElementById('imgLarge');
		p.src = src;
	}
	
	function showDetails() {
		document.getElementById("prodDescLeft").src = '/images/backgrounds/productTab_on_left.gif'
		document.getElementById("prodDescDiv").className = 'prodTab_on'
		document.getElementById("prodDescRight").src = '/images/backgrounds/productTab_on_right.gif'
		
		document.getElementById("prodCompLeft").src = '/images/backgrounds/productTab_off_left.gif'
		document.getElementById("prodCompDiv").className = 'prodTab_off'
		document.getElementById("prodCompRight").src = '/images/backgrounds/productTab_off_right.gif'
		
		document.getElementById("prodReviewLeft").src = '/images/backgrounds/productTab_off_left.gif'
		document.getElementById("prodReviewDiv").className = 'prodTab_off'
		document.getElementById("prodReviewRight").src = '/images/backgrounds/productTab_off_right.gif'
		
		document.getElementById("prodComp").style.display = 'none'
		document.getElementById("prodReview").style.display = 'none'
		document.getElementById("prodDetails").style.display = ''
	}
	
	function showComp() {
		document.getElementById("prodCompLeft").src = '/images/backgrounds/productTab_on_left.gif'
		document.getElementById("prodCompDiv").className = 'prodTab_on'
		document.getElementById("prodCompRight").src = '/images/backgrounds/productTab_on_right.gif'
		
		document.getElementById("prodDescLeft").src = '/images/backgrounds/productTab_off_left.gif'
		document.getElementById("prodDescDiv").className = 'prodTab_off'
		document.getElementById("prodDescRight").src = '/images/backgrounds/productTab_off_right.gif'
		
		document.getElementById("prodReviewLeft").src = '/images/backgrounds/productTab_off_left.gif'
		document.getElementById("prodReviewDiv").className = 'prodTab_off'
		document.getElementById("prodReviewRight").src = '/images/backgrounds/productTab_off_right.gif'
		
		document.getElementById("prodDetails").style.display = 'none'
		document.getElementById("prodReview").style.display = 'none'
		document.getElementById("prodComp").style.display = ''
	}
	
	function showReview() {

		document.getElementById("prodReviewLeft").src = '/images/backgrounds/productTab_on_left.gif'
		document.getElementById("prodReviewDiv").className = 'prodTab_on'
		document.getElementById("prodReviewRight").src = '/images/backgrounds/productTab_on_right.gif'
		
		document.getElementById("prodDescLeft").src = '/images/backgrounds/productTab_off_left.gif'
		document.getElementById("prodDescDiv").className = 'prodTab_off'
		document.getElementById("prodDescRight").src = '/images/backgrounds/productTab_off_right.gif'
		
		document.getElementById("prodCompLeft").src = '/images/backgrounds/productTab_off_left.gif'
		document.getElementById("prodCompDiv").className = 'prodTab_off'
		document.getElementById("prodCompRight").src = '/images/backgrounds/productTab_off_right.gif'
		
		document.getElementById("prodDetails").style.display = 'none'
		document.getElementById("prodComp").style.display = 'none'
		document.getElementById("prodReview").style.display = ''
	}
</script>
<!-- Begin Curebit product integration code -->
  <script type="text/javascript" src="http://www.curebit.com/javascripts/api/all-0.2.js"></script>
  <script type="text/javascript">

    curebit.init({site_id: 'cellular-outfitter'});

    var products = [{
          url:	'http://www.cellularoutfitter.com<%=request.ServerVariables("HTTP_X_REWRITE_URL")%>', /*REQUIRED VARIABLE */
          <% if musicSkins = 1 then %>
		  image_url: 'http://www.cellularoutfitter.com/productpics/musicSkins/musicSkinsLarge/<%=itempic_CO%>', /* REQUIRED VARIABLE */
		  <% else %>
		  image_url: 'http://www.cellularoutfitter.com/productPics/big/<%=itempic_CO%>', /* REQUIRED VARIABLE */
		  <% end if %>
          title:	'<%=itemDesc_CO%>', /* REQUIRED VARIABLE */
          product_id: '<%="CO" & (itemID+250)%>', /* REQUIRED VARIABLE */
          price:	'<%=price_CO%>' /* OPTIONAL VARIABLE */
      },
      {
          url:	'http://www.cellularoutfitter.com<%=request.ServerVariables("HTTP_X_REWRITE_URL")%>', /*REQUIRED VARIABLE */
          <% if musicSkins = 1 then %>
		  image_url: 'http://www.cellularoutfitter.com/productpics/musicSkins/musicSkinsLarge/<%=itempic_CO%>', /* REQUIRED VARIABLE */
		  <% else %>
		  image_url: 'http://www.cellularoutfitter.com/productPics/big/<%=itempic_CO%>', /* REQUIRED VARIABLE */
		  <% end if %>
          title:	'<%=itemDesc_CO%>', /* REQUIRED VARIABLE */
          product_id: '<%="CO" & (itemID+250)%>', /* REQUIRED VARIABLE */
          price:	'<%=price_CO%>' /* OPTIONAL VARIABLE */
      }];

    curebit.register_products(products);

  </script>

<!-- End Curebit product integration code -->