<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
	
	'sql = ""
	'session("errorSQL") = sql
	'set rs = oConn.execute(sql)

	SEtitle = "Apple iPad Mini Rumors | Apple iPad Mini Accessories"
	SEdescription = "Here at Cellular Outfitter, we have the most extensive coverage of the iPad Mini! Check out the full list of rumored features for the latest Apple product."
%>
<!--#include virtual="/includes/template/top.asp"-->
<style>
body {
    /*font-family: "Lucida Grande", Arial, Helvetica, sans-serif;*/
}

#divRumors h2, #divRumors h3, #divRumors dt {
    color:slategray;
}



#divRumors dt, #divRumors dd {
    margin-bottom: 1.0em;
	/*margin-left: 1.5em;*/
}

#divRumors dt {
    font-weight: bold;
	font-size:1.25em;
}



#divRumors dl dd dt {
    color: #000;
    margin-top: 1.0em;
    font-weight: inherit;
    font-style: italic;
}

.divPhotoR {
	margin-top:2.5em;
	margin-left:2.0em;
	margin-bottom:1.5em;
	min-width:1px;
	min-height:1px;
	float:right;
}

.firstPhoto {
	padding-top:0.0em;
}

.nonFirstPhoto {
	margin-top:0.5em;
}

#divRumors .italic {
    font-style: italic;
	font-size:16px;
}

#divRumors .empty {
	display:none;
}

#divRumors img {
	border:none;
}
#divBreadCrumbs {
	margin-bottom:1.0em;
}
</style>
<div id="divRumors">
<div id="divBreadCrumbs" class="top-sublink-gray">
    Tablet Accessories&nbsp;&gt;&nbsp;
    <!--<a class="top-sublink-gray" href="javascript:void();">Apple iPad Mini Accessories</a>&nbsp;&gt;&nbsp;-->
    <span class="top-sublink-blue">Apple iPad Mini Rumors</span>
</div>
<div id="divMainImg" class="divPhotoR">
	<img src="/images/ipad-mini.jpg" width="375" height="411" border="0" alt="iPad Mini" title="iPad Mini">
</div>

    <h1 class="brandModel">
        iPad Mini Rumor Roundup
    </h1>

    <div class="divPhotoR firstPhoto"></div>

    <p>Rumors have been hot on the impending launch of the iPad Mini and we're here to set the record straight.</p>
    
    <p>Here at Cellular Outfitter, we guarantee the compatibility of our accessories for the brand new iPad Mini so that you can shop with confidence.</p>
    
    <dl>
        <dt class="italic">It's set to launch in early October:</dt>
        <dd>
            <p>Initial reports may have indicated that the iPad Mini would be released with the iPhone 5 
            on September 12, but it's looking like Apple will launch both of their hot ticket items separately. 
            The iPad Mini is expected to debut in early October at a separate Apple event.</p>
        </dd>
  <dt class="italic">It will be priced under $300:</dt>
        <dd>
            <p>The iPad Mini's biggest competitiors are expected to be the Amazon Kindle Fire and the Google Nexus 7. 
            Since these products are currently priced at $199, it's a safe estimate that the iPad Mini will be priced 
            along a similar price point as well. Current projections indicate that in order to still make a profit, 
            Apple will sell the iPad Mini for around $299.</p>
      </dd>
  <dt class="italic">It will be smaller than the current iPad:</dt>
    <dd>
    	<p>Since the iPad Mini will be a downsized version of the current iPad, it will sport a smaller display. 
        The screen is expecting to be under 8-inches, but will be consistent with a 4:3 display aspect ratio.</p>
    </dd>
    <dt class="italic">The screen will be updated:</dt>
    <dd>
   	  <p>One of the updated features expected on the iPad Mini is a revamped indium gallium zinc oxide display. 
      An IGZO displays means brighter and crisper images for the iPad Mini, which may also give way to new and 
      fresh applications designed for compatibility. </p>
    </dd>
</dl>
</div>
<!--#include virtual="/includes/template/bottom.asp"-->
