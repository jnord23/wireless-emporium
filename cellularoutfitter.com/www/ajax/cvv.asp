<table cellspacing="0" cellpadding="0" width="300" border="0" style="font-size:10px;">
    <tr><td style="font-weight:bold; padding:5px 0px 5px 0px;">What is CVV2 (or Security Code)?</td></tr>
    <tr>
        <td valign="top">
            CVV2 is a security measure we require for all
            transactions. Since a CVV2 number is listed on your credit card, but is not
            stored anywhere, the only way to know the correct CVV2 number for your credit
            card is to physically have possession of the card itself. All VISA, MasterCard
            and American Express cards made in America in the past 5 years or so have a
            CVV2 number.
        </td>
    </tr>
    <tr><td style="font-weight:bold; padding:10px 0px 5px 0px;">How to find your CVV2 (or Security Code) number?</td></tr>
    <tr>
        <td>
            On a Visa or MasterCard, please turn your card
            over and look in the signature strip. You will find (either the entire 16-digit
            string of your card number, OR just the last 4 digits), followed by a space,
            followed by a 3-digit number. That 3-digit number is your CVV2 number. (See
            below) On American Express Cards, the CVV2 number is a 4-digit number that
            appears above the end of your card number. (See below)
        </td>
    </tr>
    <tr><td style="font-weight:bold; padding:10px 0px 5px 0px;">Visa &amp; MasterCard</td></tr>
    <tr>
        <td>
            This number is printed on your MasterCard &amp; Visa cards in
            the signature area of the back of the card. (it is the last 3 digits AFTER the
            credit card number in the signature area of the card). IF YOU CANNOT READ YOUR
            CVV2 NUMBER, YOU WILL HAVE TO CONTACT THE ISSUING CREDITOR.
        </td>
    </tr>
    <tr><td style="font-weight:bold; padding:10px 0px 5px 0px;">American Express</td></tr>
    <tr>
        <td>
            American Express cards show the CVV2 printed above and to the
            right of the imprinted card number on the front of the card.
        </td>
    </tr>
</table>