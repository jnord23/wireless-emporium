<!--#include virtual="/Framework/Utility/Static.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
<%
	orderid = prepStr(request.QueryString("orderid"))
	submitted = prepStr(request.QueryString("submitted"))
	if submitted = "Y" then
		cdo_to = "support@cellularoutfitter.com"
'		cdo_to = "terry@wirelessemporium.com"
		cdo_from = prepStr(request.querystring("txtContactEmail"))
		cdo_subject = "Questions on order #" & orderid
		cdo_body = cdo_body & "<p><b>Order #: </b>" & orderid & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Customer's Email: </b>" & prepStr(request.querystring("txtContactEmail")) & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p><b>Customer's Message: </b>" & prepStr(request.querystring("txtMessage")) & "</p>" & vbcrlf
		cdo_body = cdo_body & "<p>&nbsp;</p><p>" & Request.ServerVariables("REMOTE_ADDR") & "</p>" & vbcrlf

		CDOSend cdo_to,cdo_from,cdo_subject,cdo_body
	%>
    <div style="width:450px; padding:0px; text-align:left; margin-left:auto; margin-right:auto; margin-top:150px; position:relative; background-color:#fff; border:5px solid #333; border-radius:5px;">
        <div style="position:absolute; top:-25px; right:-25px; cursor:pointer;" onclick="closeContactUsPopup()"><img src="/images/orderstatus/X-Icon.png" border="0" /></div>
        <div style="float:left; width:100%; background-color:#f2f2f2;">
            <div style="float:left; padding:5px;"><img src="/images/orderstatus/Calendar-Icon.png" border="0" /></div>
            <div style="float:left; color:#333; font-size:20px; padding:15px 0px 0px 5px;">Contact us about your order:</div>
        </div>
        <div style="padding:15px;">
            <div style="float:left; width:100%; padding:20px 0px 0px 0px; color:#333; font-size:30px; font-weight:bold; text-align:left;">Order #<%=orderid%></div>
            <div style="float:left; width:100%; padding-top:10px;">
                <div style="float:left; color:#333; padding:10px; font-size:14px; font-weight:bold;">Thank you for your email, our Customer Support Team will contact you shortly.</div>
            </div>
            <div style="clear:both; height:30px;"></div>
        </div>
    </div>
    <%
	else
	%>
    <div style="width:450px; text-align:left; margin-left:auto; margin-right:auto; margin-top:150px; position:relative; background-color:#fff; border:5px solid #333; border-radius:5px;" align="left">
        <form name="frmContactus" method="post">
        <div style="position:absolute; top:-25px; right:-25px; cursor:pointer;" onclick="closeContactUsPopup()"><img src="/images/orderstatus/X-Icon.png" border="0" /></div>
        <div style="float:left; width:100%; background-color:#f2f2f2;">
            <div style="float:left; padding:5px;"><img src="/images/orderstatus/Calendar-Icon.png" border="0" /></div>
            <div style="float:left; color:#333; font-size:20px; padding:15px 0px 0px 5px;">Contact us about your order:</div>
        </div>
        <div style="padding:15px;">
			<%if orderid <> "" then%>
            <div style="float:left; width:100%; padding:20px 0px 0px 0px; color:#333; font-size:30px; font-weight:bold; text-align:left;">Order #<%=orderid%></div>
            <div style="float:left; width:100%; padding:0px 0px 20px 0px; color:#333; font-size:14px; font-weight:bold; text-align:left;">Submit a question and our customer support team will reach you within 24 hours.</div>
            <%end if%>
            <div style="float:left; width:100%; padding-top:10px;">
                <div style="float:left; width:100%; color:#333; font-size:14px; font-weight:bold;">Email Address: <span style="color:#ff6600;">*</span></div>
                <div style="float:left; width:100%; padding-top:5px;">
                	<%if request.querystring("txtContactEmail") = "" then%>
                    <input type="text" name="txtContactEmail" value="Enter your Email Address" onclick="if (this.value=='Enter your Email Address') this.value='';" class="contactEmail" />
                    <%else%>
                    <input type="text" name="txtContactEmail" value="<%=prepStr(request.querystring("txtContactEmail"))%>" onclick="if (this.value=='Enter your Email Address') this.value='';" class="contactEmail" />
                    <%end if%>
                </div>
            </div>
            <div style="float:left; width:100%; padding-top:15px;">
                <div style="float:left; width:100%; color:#333; font-size:14px; font-weight:bold;">Your Message: <span style="color:#ff6600;">*</span></div>
                <div style="float:left; width:100%; padding-top:5px;">
                    <textarea name="txtMessage" onfocus="if (this.value=='Enter your message') this.value='';" onclick="if (this.value=='Enter your message') this.value='';" class="contactContent">Enter your message</textarea>
                </div>
            </div>
            <div style="float:left; width:100%; padding:10px 0px 15px 0px; color:#666; font-size:12px;">FIELDS MARKED WITH * ARE REQUIRED.</div>
            <div style="float:left; width:100%; padding-top:15px;" align="center">
            	<div style="background-color:#145c97; width:250px; height:45px; padding-top:10px; border-radius:5px; cursor:pointer; font-size:28px; font-weight:bold; color:#fff; text-align:center; border:0px;" onClick="sendContactUs();">Submit Question</div>
            </div>
            <div style="clear:both;"></div>        
        </div>
        <input type="hidden" name="hidOrderID" value="<%=orderid%>" />
        </form>
    </div>
    <%
	end if
	
	call CloseConn(oConn)
%>
