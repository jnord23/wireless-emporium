<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/includes/asp/inc_BasePage.asp"--><%	

	dim thisUser, pageTitle, header
	
	FUNCTION CDOSend(strTo,strFrom,strSubject,strBody)
		Dim objErrMail
		Set objErrMail = Server.CreateObject("CDO.Message")
		With objErrMail
			.From = strFrom
			.To = strTo
			.Subject = strSubject
			.HTMLBody = CStr("" & strBody)
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
			.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
			.Configuration.Fields.Item("urn:schemas:httpmail:sender") = "sales@cellularoutfitter.com"
			.Configuration.Fields.Update
			.Send
		End With
		Set objErrMail = nothing
	END FUNCTION
	
	itemID = prepInt(request.QueryString("itemID"))
	email = prepStr(request.QueryString("email"))
	phone = prepStr(request.QueryString("phone"))
	comment = prepStr(request.QueryString("comment"))
	page = prepStr(request.QueryString("page"))

	remoteIPAddress = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
	
	if comment = "" then
		response.Write("No Comment Found")
	else
		session("errorSQL") = "comment:" & comment
		session("errorSQL2") = "qsVal:" & request.QueryString("comment")
		sql = ""
		fullComment = ""
		if cdbl(itemID) > 0 then
			useURL = "http://www.cellularOutfitter.com/p-" & itemID & "-commented-product.asp"
			fullComment = "New Comment on CO (" & request.ServerVariables("HTTP_USER_AGENT") & ") : <a href='" & useURL & "'>" & useURL & "</a>"
			if email <> "" then fullComment = fullComment & "<br />Email:&nbsp;" & email
			if phone <> "" then fullComment = fullComment & "<br />Phone:&nbsp;" & phone
			fullComment = fullComment & "<br />" & comment & "<br /><br />" & remoteIPAddress
			sql = "insert into we_commentCard (itemID,email,phone,comment,IP) values(" & itemID & ",'" & email & "','" & phone & "','" & comment & "','" & remoteIPAddress & "')"
		elseif page <> "" then
			useURL = replace("http://www.cellularOutfitter.com/" & page,".com//",".com/")
			fullComment = "New Comment on CO (" & request.ServerVariables("HTTP_USER_AGENT") & ") : <a href='" & useURL & "'>" & useURL & "</a>"
			if email <> "" then fullComment = fullComment & "<br />Email:&nbsp;" & email
			if phone <> "" then fullComment = fullComment & "<br />Phone:&nbsp;" & phone
			fullComment = fullComment & "<br />" & comment & "<br /><br />" & remoteIPAddress
			sql = "insert into we_commentCard (page,email,phone,comment,IP) values('CO-" & page & "','" & email & "','" & phone & "','" & comment & "','" & remoteIPAddress & "')"
		end if
		
		if sql <> "" then
			session("errorSQL") = sql
			oConn.execute(sql)
			
			if left(comment,8) = "Testing!" then
				CDOSend "jon@wirelessemporium.com;terry@wirelessemporium.com","comments@cellularOutfitter.com","CO - Comment Card",fullComment
			else
				CDOSend "tony@wirelessemporium.com;ruben@wirelessemporium.com;jon@wirelessemporium.com;eugene@wirelessemporium.com;terry@wirelessemporium.com;wemarketing@wirelessemporium.com;wemerch@wirelessemporium.com;damian@wirelessemporium.com;matt@wirelessemporium.com;muhammad@wirelessemporium.com;wepurchasing@wirelessemporium.com","comments@cellularOutfitter.com","CO - Comment Card",fullComment
			end if
			response.Write("commentGood")
		else
			response.Write("Insert command not found")
		end if
	end if
	
	call CloseConn(oConn)
%>