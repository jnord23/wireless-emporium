<%
	' Copied from PhoneSale: /ajax/accessorySelector.asp
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/includes/asp/inc_BasePage.asp"--><%
	dim carrierCode : carrierCode = prepStr(request.QueryString("carrierCode"))
	dim brandID : brandID = prepInt(request.QueryString("brandID"))
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim menu : menu = prepInt(request.QueryString("menu"))
	dim leftNav : leftNav = prepInt(request.QueryString("leftNav"))
	
	if menu = 0 then useWidth = "200px" else useWidth = "170px"
	useWidth = "140px"
	
	if modelID > 0 then
'		sql = "select distinct b.typeID, b.typeName_PS as typeName, a.brandID, c.brandName, d.modelName from we_Items a left join we_Types b on a.typeID = b.typeID left join we_brands c on a.brandID = c.brandID left join we_models d on a.modelID = d.modelID where (a.modelID = " & modelID & " and b.typeID not in (4,9,14,15,16,19,20,22,23)) or b.typeID = 5 order by b.typeName_PS"
'		sql = "select distinct b.typeID, b.typeName_PS as typeName from we_Items a left join we_Types b on a.typeID = b.typeID where a.hideLive = 0 and a.modelID = " & modelID & " and a.typeID not in (4,9,14,15,16,19,20,22,23) order by b.typeID"
		'RelatedItems implemented
		sql = 	"select	distinct c.typeID, c.typeName_PS as typeName" & vbcrlf & _
				"from 	we_models a join we_Items b " & vbcrlf & _
				"	on 	a.modelID = b.modelID join we_types c " & vbcrlf & _
				"	on 	b.typeID = c.typeID join we_brands d " & vbcrlf & _
				"	on 	a.brandID = d.brandID " & vbcrlf & _
				"where 	a.modelID = " & modelID & " and c.typeID not in (4,9,14,15,16,19,20,22,23) " & vbcrlf & _
				"	and b.hideLive = 0 and b.price_ps > 0 and b.inv_qty <> 0" & vbcrlf & _
				"union" & vbcrlf & _
				"select	distinct c.typeID, c.typeName_PS as typeName" & vbcrlf & _
				"from 	we_models a join we_relateditems b " & vbcrlf & _
				"	on 	a.modelID = b.modelID join we_types c " & vbcrlf & _
				"	on 	b.typeID = c.typeID join we_brands d " & vbcrlf & _
				"	on 	a.brandID = d.brandID join we_items e" & vbcrlf & _
				"	on	b.itemid = e.itemid" & vbcrlf & _
				"where 	a.modelID = " & modelID & " and c.typeID not in (4,9,14,15,16,19,20,22,23) " & vbcrlf & _
				"	and e.hideLive = 0 and e.price_ps > 0 and e.inv_qty <> 0" & vbcrlf & _
				"order by c.typeName_PS"		
		session("errorSQL") = sql
		set catRS = oConn.execute(sql)
%>
<select name="categoryID" style="width:<%=useWidth%>;95%" onchange="accessoryCategorySelect(this.value,<%=menu%>)">
	<option value="">Select Accessory</option>
	<%
	do while not catRS.EOF
		typeID = catRS("typeID")
		category = catRS("typeName")
	%>
	<option value="<%=typeID%>#<%=lcase(formatSEO(category))%>"><%=category%></option>
	<%
		catRS.movenext
	loop
	%>
</select>
<%
		call CloseConn(oConn)
		response.End()
	elseif brandID > 0 then
		if carrierCode <> "" then
			sql = "select b.brandName, a.modelID, a.modelName from we_models a left join we_brands b on a.brandID = b.brandID where a.brandID = " & brandID & " and a.hideLive = 0 and a.carrierCode like '%" & carrierCode & "%' and a.isPhone = 1 and a.isTablet = 0 order by a.modelName"
		else
			sql = "select b.brandName, a.modelID, a.modelName from we_models a left join we_brands b on a.brandID = b.brandID where a.brandID = " & brandID & " and a.hideLive = 0 and a.carrierCode is not null and a.isPhone = 1 and a.isTablet = 0 order by a.modelName"
		end if
		session("errorSQL") = sql
		set modelRS = oConn.execute(sql)
		
		if not modelRS.EOF then brandName = modelRS("brandName")
		
		if leftNav = 1 then
%>
<select id="modelID" name="modelID" style="width:<%=useWidth%>;95%">
	<option value="">Select Phone</option>
	<% do while not modelRS.EOF %>
	<option value="<%=formatSEO(modelRS("modelID"))%>"><%=modelRS("modelName")%></option>
	<%
			modelRS.movenext
		loop
	%>
</select>
<%
		else
%>
<select id="modelID" name="modelID" style="width:<%=useWidth%>;95%">
	<option value="">Select Phone</option>
	<% do while not modelRS.EOF %>
	<option value="<%=formatSEO(modelRS("modelID"))%>"><%=modelRS("modelName")%></option>
	<%
			modelRS.movenext
		loop
	%>
</select>
<%		
		end if
		call CloseConn(oConn)
		response.End()
	elseif carrierCode <> "" then
		if carrierCode = "none" then
			sql = "select brandID, brandName from we_brands where brandType <> 2 order by brandName"
		else
			sql = "select distinct a.brandID, a.brandName from we_brands a left join we_Models b on a.brandID = b.brandID where b.carrierCode like '%" & carrierCode & "%' and b.isPhone = 1 and b.isTablet = 0 order by a.brandName"
		end if
		session("errorSQL") = sql
		set brandRS = oConn.execute(sql)
		
		if leftNav = 1 then
%>
<select name="brandName" onchange="leftNav_accessoryBrandSelect(this.value,<%=menu%>)" style="width:<%=useWidth%>;95%">
    <option value="">Select Brand</option>
    <% do while not brandRS.EOF %>
    <option value="<%=brandRS("brandID")%>#<%=lcase(formatSEO(brandRS("brandName")))%>"><%=brandRS("brandName")%></option>
    <%
        brandRS.movenext
    loop
    %>
</select>
<%
		else
%>
<select name="brandName" onchange="accessoryBrandSelect(this.value,<%=menu%>)" style="width:<%=useWidth%>;95%">
    <option value="">Select Brand</option>
    <% do while not brandRS.EOF %>
    <option value="<%=brandRS("brandID")%>#<%=lcase(formatSEO(brandRS("brandName")))%>"><%=brandRS("brandName")%></option>
    <%
        brandRS.movenext
    loop
    %>
</select>
<%
		end if
		call CloseConn(oConn)
		response.End()
	end if
	
	sql = "select carrierCode, carrierName from we_carriers where id <> 1 and id <> 3 and id <> 8 and id <> 9 and id <> 11 and id <> 12 order by carrierName"
	session("errorSQL") = sql
	set carrierRS = oConn.execute(sql)
	
	sql = "select brandID, brandName from we_brands where brandType <> 2 order by brandName"
	session("errorSQL") = sql
	set brandRS = oConn.execute(sql)
	
	if menu = 0 then
		'##############################################
		'Main Content Option
		'##############################################
%>
<form name="accessoryForm_main" style="padding:0px; margin:0px;">
<div style="font-weight:bold; font-size:18px; color:#336799; text-align:center">Which phone do you have?</div>
<div style="padding:5px 0px 0px 20px;">
    <select name="carrierCode" onchange="newCarrier(this.value,0)" style="width:<%=useWidth%>;95%">
        <option value="none">Select Carrier</option>
        <% do while not carrierRS.EOF %>
        <option value="<%=carrierRS("carrierCode")%>"><%=carrierRS("carrierName")%></option>
        <%
            carrierRS.movenext
        loop
        %>
    </select>
</div>
<div id="brandSelect" style="padding:5px 0px 0px 20px;">
    <select name="brandName" onchange="accessoryBrandSelect(this.value,0)" style="width:<%=useWidth%>;95%">
        <option value="">Select Brand</option>
        <% do while not brandRS.EOF %>
        <option value="<%=brandRS("brandID")%>#<%=lcase(formatSEO(brandRS("brandName")))%>"><%=brandRS("brandName")%></option>
        <%
            brandRS.movenext
        loop
        %>
    </select>
</div>
<div id="modelSelect" style="padding:5px 0px 0px 20px;"></div>
<div id="accessorySelect" style="padding:5px 0px 0px 20px;"></div>
<div id="goButton" style="text-align:center; cursor:pointer;" onclick="goButton_main()"><img src="http://www.phonesale.com/images/buttons/phoneSearch.jpg" border="0" /></div>
</form>
<%
	else
		'##############################################
		'Menu Option
		'##############################################
%>
<form name="accessoryForm" style="padding:0px; margin:0px;">
<div style="padding:5px 0px 0px 20px;">
    <select name="carrierCode" onchange="newCarrier(this.value,1)" style="width:170px;95%">
        <option value="none">Select Carrier</option>
        <% do while not carrierRS.EOF %>
        <option value="<%=carrierRS("carrierCode")%>"><%=carrierRS("carrierName")%></option>
        <%
            carrierRS.movenext
        loop
        %>
    </select>
</div>
<div id="brandSelect" style="padding:5px 0px 0px 20px;">
    <select name="brandName" onchange="accessoryBrandSelect(this.value,1)" style="width:170px;95%">
        <option value="">Select Brand</option>
        <% do while not brandRS.EOF %>
        <option value="<%=brandRS("brandID")%>#<%=lcase(formatSEO(brandRS("brandName")))%>"><%=brandRS("brandName")%></option>
        <%
            brandRS.movenext
        loop
        %>
    </select>
</div>
<div id="modelSelect" style="padding:5px 0px 0px 20px;"></div>
<div id="accessorySelect" style="padding:5px 0px 0px 20px;"></div>
</form>
<%
	end if
	call CloseConn(oConn)
%>