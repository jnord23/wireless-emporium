<%
	response.buffer = true
	response.expires = -1

%>
<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%	
	dim thisUser, pageTitle, header, versionID

	dim brandid, modelid, categoryid
	brandid = ds(request.QueryString("brandid"))
	modelid = ds(request.QueryString("modelid"))
	categoryid = ds(request.QueryString("categoryid"))
	versionID = prepStr(request.QueryString("version"))
	
	if brandid <> 0 then
		sql	=	"select	modelid, modelname" & vbcrlf & _
				"from	we_models" & vbcrlf & _
				"where	brandid = '" & brandid & "'" & vbcrlf & _
				"	and	modelname not like '%all model%'" & vbcrlf & _
				"	and	hidelive = 0" & vbcrlf & _
				"order by 2" & vbcrlf
		set objRs = oConn.execute(sql)
		
		if versionID = "b" then
		%>
            <select class="finderSelectObj" name="cbModel" style="width:140px;" onchange="onFinder2('m',this);">
                <option value="">Your Phone Model</option>
                <%
                do until objRs.eof
					curModelID = prepInt(objRs("modelID"))
					curModelName = objRs("modelName")
                %>
                <option value="<%=curModelID%>"<% if prepInt(modelID) = curModelID then %> selected="selected"<% end if %>><%=curModelName%></option>
                <%
    	            objRs.movenext
	            loop
                %>
            </select>
        <%
		else
		%>
            <select name="cbModel" style="width:180px;" onchange="onFinder('m',this);">
                <option value="">2.) Your Phone Model</option>
            <%
            do until objRs.eof
            %>
                <option value="<%=objRs("modelid")%>"><%=objRs("modelname")%></option>
            <%
                objRs.movenext
            loop
            %>
            </select>
        <%
		end if
	elseif modelid <> 0 then
		sql	=	"select	distinct b.typeid, b.typename" & vbcrlf & _
				"from	we_items a join v_subtypematrix_co b" & vbcrlf & _
				"	on	a.subtypeid = b.subtypeid" & vbcrlf & _
				"where	a.modelid = '" & modelid & "'" & vbcrlf & _
				"	and	a.hidelive = 0 and a.inv_qty <> 0" & vbcrlf & _
				"union" & vbcrlf & _
				"select	typeid, typeName" & vbcrlf & _
				"from	we_typemasking" & vbcrlf & _
				"where	typeid in (5, 8)" & vbcrlf & _
				"order by 1"
		set objRs = oConn.execute(sql)
		
		if versionID = "b" then
		%>
            <select class="finderSelectObj" name="cbCat" style="width:150px;">
                <option value="">Accessory Category</option>
                <%
                do until objRs.eof
                    curCategoryID = prepInt(objRs("typeID"))
                    curCategoryName = objRs("typeName")
                %>
                <option value="<%=curCategoryID%>"<% if prepInt(categoryID) = curCategoryID or prepInt(typeID) = curCategoryID then %> selected="selected"<% end if %>><%=curCategoryName%></option>
                <%
					objRs.movenext
				loop
                %>
            </select>
        <%
		else
		%>
            <select name="cbCat" style="width:180px;">
                <option value="">3.) Accessory Category</option>
            <%
            do until objRs.eof
            %>
            
                <option value="<%=objRs("typeid")%>"><%=objRs("typename")%></option>
            <%
                objRs.movenext
            loop
            %>
            </select>
        <%
		end if
	end if
	
	call CloseConn(oConn)
%>