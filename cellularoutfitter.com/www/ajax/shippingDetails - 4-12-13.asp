<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/Framework/Utility/Static.asp"--><%	

	dim thisUser, pageTitle, header
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.Open Session("ConnectionString")
%>
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->
<%	
	sZip = prepStr(request.QueryString("zip"))
	sWeight = prepStr(request.QueryString("weight"))
	shiptype = prepStr(request.QueryString("shiptype"))
	nTotalQuantity = prepInt(request.QueryString("nTotalQuantity"))
	nSubTotal = prepInt(request.QueryString("nSubTotal"))
	
	if instr(sZip,"-") > 0 then
		zipArray = split(sZip,"-")
		useZip = zipArray(0)
	else
		useZip = sZip
	end if
	
	do while len(useZip) < 5
		useZip = "0" & useZip
	loop
	
	on error resume next
	if useZip <> "" then response.write UPScode(useZip,sWeight,shiptype,nTotalQuantity,0,0)
	on error goto 0
	
	oConn.Close
	set oConn = nothing
%>