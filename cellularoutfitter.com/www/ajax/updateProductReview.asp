<!--#include virtual="/includes/asp/inc_BasePage.asp"-->
<%
	response.buffer = true
	response.expires = -1

	dim strEmail, strTitle, strReview, strNickName, rating, itemid, partnumber
	strEmail = ds(request("email"))
	strTitle = ds(request("title"))
	strReview = ds(request("desc"))
	strNickName = ds(request("nickname"))
	itemid = request("itemid")
	partnumber = request("partnumber")
	rating = request("rating")
	emailorderid = request("emailorderid")
	verifyHuman = replace(request("verifyHuman")," ","")
	verifyHumanCode = request("verifyHumanCode")
	recaptcha_challenge_field = request("re_challenge")
	recaptcha_response_field = request("re_response")
	recaptcha_privatekey = request("re_privatekey")
	
	if left(verifyHumanCode,1) < 4 then
		vh_letter = "a"
	elseif left(verifyHumanCode,1) < 7 then
		vh_letter = "b"
	else
		vh_letter = "b"
	end if
	if right(verifyHumanCode,1) < 4 then
		vh_num = 1
	elseif left(verifyHumanCode,1) < 7 then
		vh_num = 2
	else
		vh_num = 3
	end if
	
	requiredCode = vh_letter & vh_num

	if verifyHuman <> requiredCode then
		response.write "wrong verification code"
	else
		sql	=	"insert into co_reviews(orderid,itemid, rating, nickname, reviewTitle, review, partnumbers)" & vbcrlf & _
				"values('" & emailorderid & "','" & itemid & "','" & rating & "','" & strNickName & "','" & strTitle & "','" & strReview & "','" & partnumber & "')"
				
		oConn.execute(sql)
	%>
		<div style="display:block; text-align:center; vertical-align:middle; font-size:16px;">
			<img src="/images/review/checkmark.jpg" border="0" width="19" height="15" />&nbsp;
			<b>Thank you for your response!<br><br>It will be reviewed before being posted on the site.</b>
		</div>	
	<%		
	end if

	call CloseConn(oConn)
	
	function recaptcha_confirm(recaptcha_private_key,rechallenge,reresponse)
		Dim VarString
			VarString = _
			"privatekey=" & recaptcha_private_key & _
			"&remoteip=" & Request.ServerVariables("REMOTE_ADDR") & _
			"&challenge=" & rechallenge & _
			"&response=" & reresponse
		
		Dim objXmlHttp
		Set objXmlHttp = Server.CreateObject("Msxml2.ServerXMLHTTP")
			objXmlHttp.open "POST", "http://www.google.com/recaptcha/api/verify", False
			objXmlHttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
			objXmlHttp.send VarString

		Dim ResponseString
			ResponseString = split(objXmlHttp.responseText, vblf)

		Set objXmlHttp = Nothing
		
		if ResponseString(0) = "true" then
			'They answered correctly
			recaptcha_confirm = ""
		else
			'They answered incorrectly
			recaptcha_confirm = ResponseString(1)
		end if
	end function
%>
