<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/includes/asp/inc_BasePage.asp"--><%	
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%
	
	set fso = CreateObject("Scripting.FileSystemObject")
	Set Jpeg = Server.CreateObject("Persits.Jpeg")
	dim itemID : itemID = prepInt(request.QueryString("itemID"))
	
	sql = "exec sp_quickView " & itemID
	session("errorSQL") = sql
	set rs = oConn.execute(sql)
	
	if not rs.EOF then
		itemPic = rs("itemPic_CO")
		itempic_WE = rs("itemPic")
		itemDesc = rs("itemDesc_CO")
		reviewAvg = rs("reviewAvg")
		reviewCnt = rs("reviewCnt")
		invQty = rs("inv_qty")
		partNumber = rs("partnumber")
		price = rs("price_co")
		priceRetail = rs("price_retail")
	end if
	
	function getRatingAvgStar(rating)
	dim nRating 
	dim strRatingImg
	nRating = cdbl(rating)
	strRatingImg = 	"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />"
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""	
		for i=1 to 5
			if nRating => i then
				strRatingImg = strRatingImg & "<img src=""/images/review/greenStarFull.gif"" border=""0"" width=""14"" height=""13"" /> "
			elseif nRating => ((i - 1) + .1) then
				strRatingImg = strRatingImg & "<img src=""/images/review/greenStarHalf.gif"" border=""0"" width=""8"" height=""13"" /> "				
			else
				strRatingImg = strRatingImg & "<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" /> "
			end if
		next		
	end if
	
	getRatingAvgStar = strRatingImg
end function
%>
<div class="tb quickViewBox">
	<div class="closeIcon pt" onclick="closeQuickView()"></div>
	<div class="fl imgSide">
    	<div class="tb quickViewItemPicBig"><img id="mainPic" src="/productPics/big/<%=itemPic%>" border="0" width="300" height="300" /></div>
        <div class="tb altPics mCenter">
        	<div id="altPic_0" class="fl activeAlt" onmouseup="viewAlt('/productPics/big/<%=itemPic%>',0)"><img src="/productPics/thumb/<%=itemPic%>" width="60" height="60" /></div>
			<%
			for i = 0 to 7
				useImg = "/altPics/" & itempic_WE
				altMainPic = replace(replace(useImg,".jpg","-" & i & ".jpg"),"/big/","/altImgs/")
				if fso.fileExists(server.MapPath(altMainPic)) then
					curPic = mid(altMainPic,instrrev(altMainPic,"/")+1)
					if not fso.fileExists(server.MapPath("/productPics/alts/thumbs/" & curPic)) then
						jpeg.Open Server.MapPath(altMainPic)
						jpeg.Height = 60
						jpeg.Width = 60
						jpeg.Save Server.MapPath("/productPics/alts/thumbs/" & curPic)
					end if
			%>
			<div id="altPic_<%=i+1%>" class="fl pt altPic" onmouseup="viewAlt('/altPics/<%=curPic%>',<%=i+1%>)"><img src="/productPics/alts/thumbs/<%=curPic%>" width="60" height="60" /></div>
			<%
				end if
			next
			%>
        </div>
    </div>
    <div class="fr detailSide">
    	<div class="tb quickViewItemDesc"><%=itemDesc%></div>
        <div class="tb quickViewSku">
			<div class="fl sku">SKU: <%=partnumber%></div>
            <div class="fl itemID">ItemID: <%=itemID%></div>
        </div>
        <div class="tb quickViewPrice">
			<div class="fl curPrice"><%=formatCurrency(price,2)%></div>
            <div class="fl msrp">reg. <span class="lt"><%=formatCurrency(priceRetail,2)%></span></div>
        </div>
        <% if reviewAvg > 0 then %>
        <div class="tb quickViewReviews">
			<div class="fl reviewStars"><%=getRatingAvgStar(reviewAvg)%></div>
            <div class="fl reviewCnt"><%=reviewCnt%> Reviews</div>
        </div>
		<% end if %>
        <% if invQty > 0 then %>
        <div class="tb quickViewStock">
        	<div class="fl inStockIcon"></div>
            <div class="fl inStockText">In stock and ready for delivery!</div>
        </div>
        <form name="popSubmit" action="/cart/item_add.asp" method="post" style="margin:0px; padding:0px;">
        <div class="tb quickViewQty">
        	<div class="fl"><input type="text" name="qty" value="1" class="qtyBox" /></div>
            <div class="fl qtyText">Quantity</div>
        </div>
        <div class="tb quickViewAddToCart">
        	<input type="image" name="myAction" value="Add to Cart" src="/images/icons/addToCart.gif" width="235" height="40" />
            <input type="hidden" name="prodid" value="<%=itemid%>">
        </div>
        </form>
        <% else %>
        <div class="tb quickViewOutOfStock">Sorry, this product<br />is currently out of stock</div>
        <% end if %>
        <div class="tb quickViewFullDetails pt" onclick="window.location='/p-<%=itemID%>-<%=formatSEO(itemDesc)%>.html'">
        	<div class="fl viewFullDetails">View Product Details</div>
            <div class="fl detailsArrow"></div>
        </div>
    </div>
</div>
<%
	call CloseConn(oConn)
%>