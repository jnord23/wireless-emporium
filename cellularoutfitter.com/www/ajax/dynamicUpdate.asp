<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/includes/asp/inc_BasePage.asp"--><%	

	dim thisUser, pageTitle, header
	dim myCartDetails : myCartDetails = prepInt(request.QueryString("myCartDetails"))
	dim featuredProducts : featuredProducts = prepInt(request.QueryString("featuredProducts"))
	
	if myCartDetails = 1 then
		mySession = prepInt(request.cookies("mySession"))
		
		sql = 	"select isnull(sum(a.qty), 0) totalQty, isnull(sum(b.price_co * a.qty), 0) totalPrice1, isnull(sum(c.price_we * a.qty), 0) totalPrice2 " & vbcrlf & _
				"from shoppingcart a left join we_items b on a.itemid=b.itemid left join we_items_musicSkins c on a.itemID = c.id " & vbcrlf & _
				"where a.store = 2 and a.sessionid = '" & mySession & "' and (a.purchasedorderid is null or a.purchasedorderid = 0) and (a.customCost is null or a.customCost > 0)" & vbcrlf
		session("errorSQL") = sql
		set objRsMiniCart = oConn.execute(sql)
		
		if objRsMiniCart.EOF then
			miniTotalQuantity = 0
			miniSubTotal = 0
		else
			miniTotalQuantity = formatnumber(objRsMiniCart("totalQty"), 0)
			miniSubTotal = formatcurrency(objRsMiniCart("totalPrice1") + objRsMiniCart("totalPrice2"))
		end if
%>
<%=miniTotalQuantity%> Items - <%=formatCurrency(miniSubTotal,2)%>
<%
	elseif featuredProducts = 1 then
		set fs = CreateObject("Scripting.FileSystemObject")
		myModel = prepInt(request.Cookies("myModel"))
		myBrand = prepInt(request.Cookies("myBrand"))

'		if myModel > 0 then
'			sql = 	"select top 20 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO, c.modelID from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 2 and c.modelID = " & myModel & " and c.itemid not in (select cast(configValue as int) itemid from we_config where configName = 'Free ItemID') group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO, c.modelID " &_
'					"union " &_
'					"select top 20 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO, 0 as modelID from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 2 and b.orderdatetime > '" & date-90 & "' and c.brandID = 20 and c.itemid not in (select cast(configValue as int) itemid from we_config where configName = 'Free ItemID') group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by c.modelID desc, 2 desc"
'		elseif myBrand > 0 then
'			sql = "select top 20 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 2 and b.orderdatetime > '" & date-90 & "' and c.brandID = " & myBrand & " and c.itemid not in (select cast(configValue as int) itemid from we_config where configName = 'Free ItemID') group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by 2 desc"
'		else
'			sql = "select top 20 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 2 and b.orderdatetime > '" & date-90 & "' and c.itemid not in (select cast(configValue as int) itemid from we_config where configName = 'Free ItemID') group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by 2 desc"
'		end if
		sql = "exec sp_recommendedItemsForUserCookie 2," & prepInt(myBrand) & "," & prepInt(myModel) & ",0,12"
		session("errorSQL") = sql
		set topProds = oConn.execute(sql)
		
		sql =	"select top 20 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO " &_
				"from we_orderdetails a with (nolock) " &_
					"join we_orders b with (nolock) on a.orderid = b.orderid " &_
					"join we_Items c with (nolock) on a.itemid = c.itemID " &_
					"join we_Items d with (nolock) on c.partnumber = d.partnumber and d.master = 1 " &_
				"where d.inv_qty > 0 and c.price_CO > 0 and b.store = 2 and b.orderdatetime > '" & date-14 & "' " &_
				"group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO " &_
				"order by 2 desc"
		session("errorSQL") = sql
		set topProds2 = oConn.execute(sql)
		%>
		<div style="font-weight:bold; color:#1372c6; font-size:16px; padding:10px 0px 5px 0px; float:left; width:750px;">
			<% if myBrand = 0 and myModel = 0 then %>Top Selling Products<% else %>Recommended Products<% end if %>
		</div>
		<div style="float:left; width:750px; margin-bottom:10px; margin-left:45px; margin-right:auto;">
			<%
			prodLap = 0
			totalProdCnt = 0
			do while not topProds.EOF
				if fs.fileExists(server.MapPath("/productpics/thumb/" & topProds("itempic"))) then
					prodLap = prodLap + 1
					totalProdCnt = totalProdCnt + 1
					prodDetails = split(topProds("itemDesc"),"##")
					itemID = prepInt(topProds("itemID"))
					itemDesc_CO = prodDetails(1)
					price_CO = prepInt(topProds("price_our"))
					itemPic_CO = topProds("itempic")
					if len(itemDesc_CO) > 50 then showName = left(itemDesc_CO,50) & "..." else showName = itemDesc_CO
			%>
			<div title="<%=itemDesc_CO%>" style="float:left; width:135px; height:180px; padding:10px 5px 0px 5px; <% if prodLap < 5 then %>border-right:1px dotted #999; <% end if %>border-bottom:1px solid #999;">
				<div style="float:left; width:130px; text-align:center;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html"><img src="/productpics/thumb/<%=itemPic_CO%>" border="0" /></a></div>
				<div style="float:left; width:130px; text-align:left; height:50px; padding-top:5px;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html" style="color:#000;"><%=showName%></a></div>
				<div style="float:left; width:130px; text-align:left; padding-top:10px;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html" style="color:#1372c6; font-weight:bold;"><%=formatCurrency(price_CO,2)%></a></div>
			</div>
			<%
					if prodLap = 5 then prodLap = 0
					if totalProdCnt = 10 then exit do
				end if
				topProds.movenext
			loop
			
			do while totalProdCnt < 10
				if fs.fileExists(server.MapPath("/productpics/thumb/" & topProds2("itempic"))) then
					prodLap = prodLap + 1
					totalProdCnt = totalProdCnt + 1
					prodDetails = split(topProds2("itemDesc"),"##")
					itemID = prepInt(topProds2("itemID"))
					itemDesc_CO = prodDetails(1)
					price_CO = prepInt(topProds2("price_our"))
					itemPic_CO = topProds2("itempic")
					if len(itemDesc_CO) > 50 then showName = left(itemDesc_CO,50) & "..." else showName = itemDesc_CO
			%>
			<div title="<%=itemDesc_CO%>" style="float:left; width:135px; height:180px; padding:10px 5px 0px 5px; <% if prodLap < 5 then %>border-right:1px dotted #999; <% end if %>border-bottom:1px solid #999;">
				<div style="float:left; width:130px; text-align:center;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html"><img src="/productpics/thumb/<%=itemPic_CO%>" border="0" /></a></div>
				<div style="float:left; width:130px; text-align:left; height:50px; padding-top:5px;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html" style="color:#000;"><%=showName%></a></div>
				<div style="float:left; width:130px; text-align:left; padding-top:10px;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html" style="color:#1372c6; font-weight:bold;"><%=formatCurrency(price_CO,2)%></a></div>
			</div>
			<%
					if prodLap = 5 then prodLap = 0
					if totalProdCnt = 10 then exit do
				end if
				topProds2.movenext
			loop
			%>
		</div>
<%
	elseif featuredProducts = 2 then
		myModel = prepInt(request.Cookies("myModel"))
		myBrand = prepInt(request.Cookies("myBrand"))

'		if myModel > 0 then
'			sql = 	"select top 10 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO, c.modelID from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 2 and c.modelID = " & myModel & " group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO, c.modelID " &_
'					"union " &_
'					"select top 10 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO, 0 as modelID from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 2 and b.orderdatetime > '" & date-90 & "' and c.brandID = 20 group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by c.modelID desc, 2 desc"
'		elseif myBrand > 0 then
'			sql = "select top 10 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 2 and b.orderdatetime > '" & date-90 & "' and c.brandID = " & myBrand & " group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by 2 desc"
'		else
'			sql = "select top 10 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 2 and b.orderdatetime > '" & date-90 & "' group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by 2 desc"
'		end if
		sql = "exec sp_recommendedItemsForUserCookie 2," & prepInt(myBrand) & "," & prepInt(myModel) & ",0,12"
		session("errorSQL") = sql
		set topProds = oConn.execute(sql)
		
'		if topProds.EOF then
'			myBrand = 0
'			myModel = 0
'			sql = "select top 10 a.itemid, SUM(quantity) as totalSold, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO from we_orderdetails a join we_orders b on a.orderid = b.orderid join we_Items c on a.itemid = c.itemID where (select top 1 inv_qty from we_Items where partNumber = c.partNumber and master = 1 order by inv_qty desc) > 0 and c.price_CO > 0 and b.store = 2 and b.orderdatetime > '" & date-30 & "' group by a.itemID, c.itemDesc_CO, c.price_CO, c.price_Retail, c.itemPic_CO order by 2 desc"
'			session("errorSQL") = sql
'			set topProds = oConn.execute(sql)
'		end if
		%>
        <div style="width:100%; float:left; background:url(/images/template/header-background.jpg) repeat-x; height:38px; border-top-left-radius:5px; border-top-right-radius:5px;">
            <div style="width:35px; height:38px; float:left; background:url(/images/template/header-star-icon.jpg) no-repeat;"></div>
            <div style="width:200px; float:left; font-size:18px; padding:8px 0px 0px 10px; font-style:italic; color:#222;">
            	<% if myBrand = 0 and myModel = 0 then %>Top Selling Products<% else %>Recommended Products<% end if %>
			</div>
        </div>
		<div style="width:799px; float:left; border-left:1px solid #ccc; border-right:1px solid #ccc; padding:10px;">
			<%
			prodLap = 0
			totalProdCnt = 0
			do while not topProds.EOF
				prodLap = prodLap + 1
				totalProdCnt = totalProdCnt + 1
				prodDetails = split(topProds("itemDesc"),"##")
				itemID = prepInt(topProds("itemID"))
				itemDesc_CO = prodDetails(1)
				price_CO = prepInt(topProds("price_our"))
				price_retail = prepInt(topProds("price_retail"))
				itemPic_CO = topProds("itempic")
				if len(itemDesc_CO) > 65 then showName = left(itemDesc_CO,65) & "..." else showName = itemDesc_CO
			%>
			<div title="<%=itemDesc_CO%>" style="float:left; width:185px; height:200px; padding:10px 5px 0px 5px; <% if prodLap < 4 then %>border-right:1px dotted #999; <% end if %> <%if totalProdCnt < 5 then %>border-bottom:1px solid #999;<% end if%>">
				<div style="float:left; width:180px; text-align:center;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html"><img src="/productpics/thumb/<%=itemPic_CO%>" border="0" /></a></div>
				<div style="float:left; width:180px; text-align:center; height:50px; padding-top:5px;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html" style="text-decoration:underline; color:#666;"><%=showName%></a></div>
				<div style="float:left; width:180px; text-align:center;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html" style="font-size:18px; color:#333; font-weight:bold;"><%=formatCurrency(price_CO,2)%></a></div>
				<div style="float:left; width:180px; text-align:center; padding-top:5px;"><a href="/p-<%=itemID%>-<%=formatSEO(itemDesc_CO)%>.html" style="color:#3C599B;"><%=formatPercent((price_retail - price_CO) / price_retail,0)%> OFF Retail Price: <del><%=formatcurrency(price_retail,2)%></del></a></div>
			</div>
			<%
				topProds.movenext
				if prodLap = 4 then prodLap = 0
				if totalProdCnt = 8 then exit do
			loop
			%>
		</div>
        <!-- footer -->
        <div style="width:819px; float:left; background:url(/images/template/bottom-border-background.jpg) repeat-x; height:35px; border-bottom-left-radius:5px; border-bottom-right-radius:5px; border-left:1px solid #ccc; border-right:1px solid #ccc;"></div>
        <!-- //footer -->
        
<%
	end if
	
	call CloseConn(oConn)
%>