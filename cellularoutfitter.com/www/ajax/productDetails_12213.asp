<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_CrossSells.asp"-->
<%
	
	dim modelID : modelID = prepInt(request.QueryString("modelID"))
	dim typeID : typeID = prepInt(request.QueryString("typeID"))
	dim itemID : itemID = prepInt(request.QueryString("itemID"))
	dim prodReviews : prodReviews = prepInt(request.QueryString("prodReviews"))
	dim myPartNumber : myPartNumber = prepStr(request.QueryString("myPartNumber"))
	dim latDesign : latDesign = prepInt(request.QueryString("latDesign"))
	
	set fso = CreateObject("Scripting.FileSystemObject")
	
	if prodReviews = 1 then
		function getRatingAvgStar(rating)
			dim nRating 
			dim strRatingImg
			nRating = cdbl(rating)
			strRatingImg = 	"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
							"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
							"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
							"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
							"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />"
			
			if nRating > 0 or nRating <=5 then
				strRatingImg = ""	
				for i=1 to 5
					if nRating => i then
						strRatingImg = strRatingImg & "<img src=""/images/review/greenStarFull.gif"" border=""0"" width=""14"" height=""13"" /> "
					elseif nRating => ((i - 1) + .1) then
						strRatingImg = strRatingImg & "<img src=""/images/review/greenStarHalf.gif"" border=""0"" width=""8"" height=""13"" /> "				
					else
						strRatingImg = strRatingImg & "<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" /> "
					end if
				next		
			end if
			
			getRatingAvgStar = strRatingImg
		end function

		sql	=	"	select	orderid, itemid, rating, nickname, reviewTitle, review, partnumbers, approved, dateTimeEntd	" & vbcrlf & _
				"	from	co_reviews" & vbcrlf & _
				"	where	(itemid = '" & itemid & "' or partnumbers in ('" & myPartNumber & "', '" & left(myPartNumber,4) & "', '" & left(myPartNumber,8) & "', '" & left(myPartNumber,13) & "')) " & vbcrlf & _
				"		and approved = 1" & vbcrlf & _
				"	order by dateTimeEntd desc"
		session("errorSQL") = sql
		set objRsReviews = oConn.execute(sql)
		
		if objRsReviews.EOF then
		%>
        Currently no reviews available
		<%
        else
            bgColor = "#fff"
            do while not objRsReviews.EOF
                rating = objRsReviews("rating")
                nickname = objRsReviews("nickname")
                reviewTitle = objRsReviews("reviewTitle")
                review = objRsReviews("review")
                entryDate = objRsReviews("dateTimeEntd")
                objRsReviews.movenext
        %>
        <div>
            <div style="height:20px; padding:3px; background-color:#333; color:#FFF;">
                <div style="float:left; padding-right:5px; border-right:1px solid #999;">Posted By: <%=nickname%></div>
                <div style="float:left; padding-left:5px;"><%=entryDate%></div>
            </div>
            <div style="font-weight:bold; padding-bottom:5px; font-size:24px;"><%=reviewTitle%></div>
            <div style="padding-bottom:5px;"><%=getRatingAvgStar(rating) & " &nbsp; " & formatnumber(rating,1)%></div>
            <div style="padding-bottom:5px;"><%=review%></div>
        </div>
        <%
                if bgColor = "#fff" then bgColor = "#ebebeb" else bgColor = "#fff"
            loop
        end if
		response.End()
	end if
	
'	sql = "select top 5 itemID, itemPic_co, itemDesc_CO, price_co from we_items where modelID = " & modelID & " and typeID <> " & typeID & " and typeID <> 16 and hideLive = 0 and inv_qty <> 0 and price_co is not null and price_co > 0 order by numberOfSales desc"
'	session("errorSQL") = sql
	strCrossItems = ""
	call getCrossSellItems(itemID, strCrossItems)
	
	if strCrossItems = "" then
		strCrossItems = CrossSells(itemID)
	end if
	
	if right(strCrossItems,1) = "," then
		strCrossItems = left(strCrossItems, len(strCrossItems)-1)
	end if
	
	sql = 	"	select	top 4 *" & vbcrlf & _
			"	from	(	" & vbcrlf & _
			"			select	top 8 itemid, isnull(s.subtypeName, c.typename_co) typename, a.HandsfreeType, itemdesc_co, itempic_co, price_retail, price_co, 0 musicskins, itempic defaultPic, b.rowid" & vbcrlf & _
			"			from 	we_items a with (nolock) join [dbo].[tfn_StringToColumn]('" & replace(strCrossItems, "'", "") & "', ',') b" & vbcrlf & _
			"				on	a.itemid = b.rString join we_types c" & vbcrlf & _
			"				on	a.typeid = c.typeid left outer join we_subtypes s" & vbcrlf & _
			"				on	a.subtypeid = s.subtypeid" & vbcrlf & _
			"			where	a.hidelive = 0 and a.inv_qty <> 0 and a.price_co > 0" & vbcrlf & _
			"			union" & vbcrlf & _
			"			select	top 8 id itemid, 'Music Skins' typename, 0 HandsFreeType, a.artist + ' ' + a.designName itemDesc_co, a.image itemPic_co" & vbcrlf & _
			"				,	isnull(a.msrp, 0.0) price_retail, isnull(a.price_co, 0.0) price_co, 1 musicskins, a.defaultImg defaultPic, b.rowid" & vbcrlf & _
			"			from 	we_items_musicskins a with (nolock) join [dbo].[tfn_StringToColumn]('" & replace(strCrossItems, "'", "") & "', ',') b" & vbcrlf & _
			"				on	a.id = b.rString" & vbcrlf & _
			"			where 	a.skip = 0 " & vbcrlf & _
			"				and a.deleteItem = 0 " & vbcrlf & _
			"				and (a.artist <> '' and a.artist is not null) " & vbcrlf & _
			"				and (a.designname <> '' and a.designname is not null) " & vbcrlf & _
			"				and a.deleteItem = 0 " & vbcrlf & _
			"			) a" & vbcrlf & _
			"	order by a.rowid" & vbcrlf	
	session("errorSQL") = sql
	set relatedRS = oConn.execute(sql)
	
	recentItemList = prepStr(request.Cookies("recentItemList"))
	if instr(recentItemList,",") > 0 then
		recentItemArray = split(recentItemList,",")
		recentItemCnt = ubound(recentItemArray)+1
	elseif recentItemList <> "" then
		recentItemCnt = 1
	else
		recentItemCnt = 0
	end if
	if recentItemList = "" then
		sql = "select * from we_brands where brandID = 9999"
	else
		sql = "SELECT TOP 4 b.brandName, c.modelName, a.partNumber,a.itemPic,a.itemID,a.itemDesc_CO,a.itempic_CO,a.price_Retail,a.price_CO FROM we_Items a left join we_brands b on a.brandID = b.brandID left join we_models c on a.modelID = c.modelID WHERE ItemID in (" & recentItemList & ")"
	end if
	session("errorSQL") = sql
	set recentRS = oConn.execute(sql)
	
	if recentItemCnt > 2 then
		if prepInt(recentItemArray(0)) <> itemID and prepInt(recentItemArray(1)) <> itemID and prepInt(recentItemArray(2)) <> itemID then response.Cookies("recentItemList") = recentItemArray(1) & "," & recentItemArray(2) & "," & itemID
	elseif recentItemCnt > 0 then
		if recentItemCnt = 1 then
			if prepInt(recentItemList) <> itemID then response.Cookies("recentItemList") = recentItemList & "," & itemID
		else
			if prepInt(recentItemArray(0)) <> itemID and prepInt(recentItemArray(1)) <> itemID then response.Cookies("recentItemList") = recentItemList & "," & itemID
		end if
	else
		response.Cookies("recentItemList") = itemID
	end if
	
	if latDesign = 1 then
%>
<div style="float:left; background-image:url(/images/backgrounds/subHeader_otherProds.gif); height:29px; width:750px; margin-top:10px; padding:10px 0px 0px 10px; font-weight:bold; color:#FFF;">Related Items</div>
<div style="float:left; width:750px; border-left:1px solid #666; border-right:1px solid #666; border-bottom:1px solid #666;">
<%
		prodCnt = 0
		do while not relatedRS.EOF
			prodCnt = prodCnt + 1
			relatedItemID = relatedRS("itemID")
			relatedItemPic = relatedRS("itemPic_co")
			relatedItemDesc = relatedRS("itemDesc_CO")
			relatedPrice = relatedRS("price_co")
			if len(relatedItemDesc) > 50 then relatedItemDesc = left(relatedItemDesc,50) & "..."
			if not fso.fileExists(server.MapPath("/productpics/thumb/" & relatedItemPic)) then relatedItemPic = "imagena.jpg"
%>
<div style="background-color:#FFF; width:160px; margin-bottom:2px; padding:10px; float:left;<% if prodCnt < 4 then %> border-right:1px solid #ccc;<% end if %>">
	<div style="width:140px; text-align:center;"><a href="/p-<%=relatedItemID%>-<%=formatSEO(relatedItemDesc)%>.html"><img src="/productpics/thumb/<%=relatedItemPic%>" border="0" width="100" height="100" /></a></div>
	<div style="font-size:12px; width:140px; text-align:center; height:50px;"><a href="/p-<%=relatedItemID%>-<%=formatSEO(relatedItemDesc)%>.html" style="color:#999;"><%=relatedItemDesc%></a></div>
	<div style="font-size:12px; color:#C00; width:140px; text-align:center; font-weight:bold;"><%=formatCurrency(relatedPrice,2)%></div>
</div>
<%
			relatedRS.movenext
		loop
%>
</div>
<%
		if not recentRS.EOF then
%>
<div style="float:left; background-image:url(/images/backgrounds/subHeader_otherProds.gif); height:29px; width:750px; margin-top:10px; padding:10px 0px 0px 10px; font-weight:bold; color:#FFF;">Recently Viewed Items</div>
<div style="float:left; width:750px; border-left:1px solid #666; border-right:1px solid #666; border-bottom:1px solid #666;">
<%
			prodCnt = 0
			do while not recentRS.EOF
				prodCnt = prodCnt + 1
				relatedItemID = recentRS("itemID")
				relatedItemPic = recentRS("itemPic_co")
				relatedItemDesc = recentRS("itemDesc_CO")
				relatedPrice = recentRS("price_co")
				if len(relatedItemDesc) > 50 then relatedItemDesc = left(relatedItemDesc,50) & "..."
				if not fso.fileExists(server.MapPath("/productpics/thumb/" & relatedItemPic)) then relatedItemPic = "imagena.jpg"
%>
<div style="background-color:#FFF; width:160px; margin-bottom:2px; padding:10px; float:left;<% if prodCnt < 4 then %> border-right:1px solid #ccc;<% end if %>">
	<div style="width:140px; text-align:center;"><a href="/p-<%=relatedItemID%>-<%=formatSEO(relatedItemDesc)%>.html"><img src="/productpics/thumb/<%=relatedItemPic%>" border="0" width="100" height="100" /></a></div>
	<div style="font-size:12px; width:140px; text-align:center; height:50px;"><a href="/p-<%=relatedItemID%>-<%=formatSEO(relatedItemDesc)%>.html" style="color:#999;"><%=relatedItemDesc%></a></div>
	<div style="font-size:12px; color:#C00; width:140px; text-align:center; font-weight:bold;"><%=formatCurrency(relatedPrice,2)%></div>
</div>
<%
				recentRS.movenext
			loop
%>
</div>
<%
		end if
	else
%>
<div style="font-weight:bold; font-size:14px; padding:10px 0px 10px 10px;">&bull; Related Items</div>
<%
		do while not relatedRS.EOF
			relatedItemID = relatedRS("itemID")
			relatedItemPic = relatedRS("itemPic_co")
			relatedItemDesc = relatedRS("itemDesc_CO")
			relatedPrice = relatedRS("price_co")
			if len(relatedItemDesc) > 50 then relatedItemDesc = left(relatedItemDesc,50) & "..."
			if not fso.fileExists(server.MapPath("/productpics/thumb/" & relatedItemPic)) then relatedItemPic = "imagena.jpg"
%>
<div style="background-color:#FFF; width:170px; margin-bottom:2px; padding:10px; margin-left:auto; margin-right:auto;">
	<div style="width:150px; text-align:center;"><a href="/p-<%=relatedItemID%>-<%=formatSEO(relatedItemDesc)%>.html"><img src="/productpics/thumb/<%=relatedItemPic%>" border="0" width="100" height="100" /></a></div>
	<div style="font-size:12px; width:150px; text-align:center; height:50px;"><a href="/p-<%=relatedItemID%>-<%=formatSEO(relatedItemDesc)%>.html" style="color:#999;"><%=relatedItemDesc%></a></div>
	<div style="font-size:12px; color:#C00; width:150px; text-align:center; font-weight:bold;"><%=formatCurrency(relatedPrice,2)%></div>
</div>
<%
			relatedRS.movenext
		loop
		
		if not recentRS.EOF then
%>
<div style="font-weight:bold; font-size:14px; padding:10px 0px 10px 10px;">&bull; Recently Viewed</div>
<%
			do while not recentRS.EOF
				relatedItemID = recentRS("itemID")
				relatedItemPic = recentRS("itemPic_co")
				relatedItemDesc = recentRS("itemDesc_CO")
				relatedPrice = recentRS("price_co")
				if len(relatedItemDesc) > 60 then relatedItemDesc = left(relatedItemDesc,60) & "..."
				if not fso.fileExists(server.MapPath("/productpics/thumb/" & relatedItemPic)) then relatedItemPic = "imagena.jpg"
%>
<div style="background-color:#FFF; width:170px; margin-bottom:2px; padding:10px; margin-left:auto; margin-right:auto;">
	<div style="width:150px; text-align:center;"><a href="/p-<%=relatedItemID%>-<%=formatSEO(relatedItemDesc)%>.html"><img src="/productpics/thumb/<%=relatedItemPic%>" border="0" width="100" height="100" /></a></div>
	<div style="font-size:12px; width:150px; height:50px; text-align:center;"><a href="/p-<%=relatedItemID%>-<%=formatSEO(relatedItemDesc)%>.html" style="color:#999;"><%=relatedItemDesc%></a></div>
	<div style="font-size:12px; color:#C00; width:150px; text-align:center; font-weight:bold;"><%=formatCurrency(relatedPrice,2)%></div>
</div>
<%
				recentRS.movenext
			loop
		end if
	end if
%>