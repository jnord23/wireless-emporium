<%
	response.buffer = true
	response.expires = -1
	
	itemids = "146466"
	currentTotal = 0.0
	currentTax = 0.0
	if prepStr(request.querystring("itemids")) <> "" then itemids = prepStr(request.querystring("itemids"))
	if prepInt(request.querystring("currentTotal")) > 0 then currentTotal = prepInt(request.querystring("currentTotal"))	
	if prepInt(request.querystring("currentTax")) > 0 then currentTax = prepInt(request.querystring("currentTax"))
	
	orderid = prepInt(request.querystring("orderid"))
	auth_transactionid = prepStr(request.QueryString("transid"))
	pType = prepStr(request.QueryString("pType"))
	strandsItemIDs = replace(itemids, ",", "_._")
	extOrderType = prepInt(request.querystring("extordertype"))
	
	usePromoPercent = 0
	
	if instr(request.ServerVariables("SERVER_NAME"),"staging.") > 0 then	
		if extOrderType = 0 then
			actionURL = "/framework/postProcessing.asp"
		elseif extOrderType = 1 then
			actionURL = "/framework/DoReferenceTransaction.asp"
		end if
	else
		if extOrderType = 0 then
			actionURL = "https://" & request.ServerVariables("SERVER_NAME") & "/framework/postProcessing.asp"
		elseif extOrderType = 1 then
			actionURL = "https://" & request.ServerVariables("SERVER_NAME") & "/framework/DoReferenceTransaction.asp"
		end if
	end if
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<div style="width:700px; padding:5px; text-align:left; margin-left:auto; margin-right:auto; margin-top:100px; position:relative; background-color:#fff; color:#444; border:10px solid #ccc; border-radius:5px; font-family:Arial, Helvetica, sans-serif;">
	<form name="frmPostPurchase" action="<%=actionURL%>" method="post">
    <div style="position:absolute; top:-20px; right:-20px; cursor:pointer;" onclick="pp_closePopup()"><img src="/images/icon-lightbox-close.png" border="0" /></div>
    <div style="float:left; width:100%; padding-top:10px;">
        <div style="float:left; width:100%;">
            <div style="float:left; padding:0px 20px 0px 30px;"><img src="/images/checkout/pp-checkmark.png" border="0" /></div>
            <div style="float:left; font-size:46px; color:#333; padding-top:10px;">Thanks for your purchase!</div>
        </div>
        <div style="float:left; width:680px; padding:10px;">
        	Your order is complete. You are obviously a savvy shopper since you saved up to <span style="color:#336699; font-weight:bold;">80% off MSRP</span> at CellularOutfitter.com, which has been helping smart customers save big since 2001. 
            However, we noticed that you didn't get a chance to add some of our most popular accessories.
            <%
			sql	=	"select	top 1 promoDate, promoPercent" & vbcrlf & _
					"from	postpurchasepromo" & vbcrlf & _
					"where	siteid = 2" & vbcrlf & _
					"order by 1 desc"
			set rs = oConn.execute(sql)
			if not rs.eof then usePromoPercent = rs("promoPercent")
			
			if usePromoPercent > 0 then
			%>
            <br /><br /><span style="color:#336699; font-weight:bold;">FOR ONE TIME ONLY</span>, you can easily add any of these related products to your current order and get an <span style="color:#336699; font-weight:bold; text-decoration:underline; font-style:italic;">additional <%=usePromoPercent%>% OFF</span> our already low wholesale prices.
            <br /><i>Please note:</i> <span style="color:#336699; font-weight:bold; text-decoration:underline;">You will never see this offer again</span> so be sure to take advantage now!
            <%
			end if
			%>
            <br /><br />Just click <span style="color:#336699; font-weight:bold;">ADD TO MY ORDER</span> and we'll instantly add your selected items to your cart.
            <div id="usePromoPercent" style="display:none;"><%=usePromoPercent%></div>
        </div>
        <div style="float:left; width:690px; padding:5px;">
            <div style="float:left; width:100%; border-top:1px solid #ccc; height:0px; margin:5px 0px 5px 0px;"></div>
            <%if pType = "strands" then%>
                <div style="float:left; width:100%;">
	                <!--<div style="float:left; width:100%; color:#fff; font-weight:bold; background-color:#933; text-align:center;">BY STRANDS LOGIC</div>-->
                    <div class="strandsRecs" tpl="misc_2" item="<%=strandsItemIDs%>">
                        <div align="center"><div style="width:700px; background-color:#fff; margin-left:auto; margin-right:auto; margin-top:100px;"><img src="/images/checkout/loading4.gif" border="0" /></div></div>
                    </div>
                </div>
            <%else%>
                <div style="float:left; width:100%;">
	                <!--<div style="float:left; width:100%; color:#fff; font-weight:bold; background-color:#0CF; text-align:center;">BY INTERNAL LOGIC</div>-->
                <%
                sql = "sp_ppUpsell 2, '" & itemids & "', " & usePromoPercent & ", 60"
                set rs = oConn.execute(sql)
                lap = 0
                do until rs.eof
                    lap = lap + 1
                    itemdesc = rs("itemdesc")
                    if len(itemdesc) > 65 then itemdesc = left(itemdesc, 62) & "..."
                    price_retail = rs("price_retail")
                    price_co = rs("price")
                %>
                    <div style="float:left; width:167px; padding:30px 5px 0px 0px;">
                        <div style="float:left; width:100%; text-align:center;"><img src="/productpics/thumb/<%=rs("itempic")%>" border="0" /></div>
                        <div style="float:left; width:162px; margin-top:5px; padding:5px 0px 5px 0px; background-color:#eaeff5; border-radius:5px;">
                            <div style="float:left;"><input id="id_chkItem<%=lap%>" type="checkbox" name="chkItem<%=lap%>" value="<%=rs("itemid")%>" onclick="updatePPTotal();" /></div>
                            <div style="float:left; padding:2px 0px 0px 5px; color:#444; font-weight:bold; font-size:11px;"><label style="cursor:pointer;" for="id_chkItem<%=lap%>">Add This To My Order</label></div>
                        </div>
                        <div style="float:left; width:100%; padding-top:5px; color:#333; font-weight:bold; font-size:12px;"><input id="id_qty<%=lap%>" type="text" name="chkQty<%=lap%>" maxlength="3" value="1" style="width:40px; height:25px; padding:5px; color:#333; text-align:center; border-radius:3px; border:1px solid #ccc; -moz-box-shadow: inset 0 0 1px 1px #eee; -webkit-box-shadow: inset 0 0 1px 1px #eee; box-shadow: inset 0 0 1px 1px #eee;" onkeyup="updatePPTotal();" /> &nbsp; Quantity</div>
                        <div style="float:left; width:100%; padding-top:5px; font-size:12px; color:#333; height:45px;" title="<%=rs("itemdesc")%>"><%=itemdesc%></div>
                        <div style="float:left; width:100%; padding-top:5px;"><%=getRatingAvgStar(rs("avgReview"))%></div>
                        <div style="float:left; width:100%; padding-top:5px; color:#333; font-size:11px; text-decoration:line-through;"><%=formatcurrency(price_retail)%></div>
                        <div style="float:left; width:100%; padding-top:5px; color:#336699; font-weight:bold; font-size:13px;">
                            <%=formatcurrency(price_co)%>&nbsp;
                            (Save <%=formatPercent((price_retail - price_co) / price_retail,0)%>)
                        </div>
                        <input id="id_itemPrice<%=lap%>" type="hidden" name="itemPrice<%=lap%>" value="<%=price_co%>" />
                        <input id="id_itemRetail<%=lap%>" type="hidden" name="itemRetail<%=lap%>" value="<%=price_retail%>" />
                    </div>
                <%
                    rs.movenext
                loop
                %>
                </div>
            <%end if%>
            <div style="float:left; width:100%; margin:25px 0px 0px 0px; text-align:center;">
            	<div style="display:inline; color:#333;">Please add the selected items to my order now for an additional: </div>
                <div style="display:inline; color:#336699; font-weight:bold;">$<span id="addlTotal">0.0</span>* (Save $<span id="addlSave">0.0</span>)</div>
            </div>
            <div id="pp_bottomBox" style="float:left; width:678px; border:1px solid #ccc; background-color:#f7f7f7; border-radius:5px; margin-top:15px; height:70px; padding:15px 0px 15px 10px;">
                <div style="float:left; padding:10px 15px 0px 0px;"><img src="/images/checkout/small-cta.png" border="0" onclick="pp_closePopup()" style="cursor:pointer;" /></div>
                <div style="float:left;">
                	<input type="image" src="/images/checkout/large-cta.png" onclick="return doSubmit();" />
				</div>
            </div>
            <div id="pp_bottomBoxText" style="display:none; float:left; width:678px; border:1px solid #ccc; background-color:#f7f7f7; border-radius:5px; margin-top:15px; color:#333; font-size:20px; text-align:center; height:70px; padding:15px 0px 15px 10px;">
				Please wait while we process your order...<br /><br />
                <img src="/images/progress_bar.gif" border="0" />
            </div>
            <%
			curTax = 0
			curGrandTotal = 0
			sql = 	"select	isnull(ordersubtotal, 0) ordersubtotal, isnull(ordershippingfee, 0) ordershippingfee, isnull(orderTax, 0) orderTax, isnull(ordergrandtotal, 0) ordergrandtotal, accountid" & vbcrlf & _
					"from	we_orders" & vbcrlf & _
					"where	orderid = '" & orderid & "'"
			set rs = oConn.execute(sql)
			if not rs.eof then
				curSubTotal = prepInt(rs("ordersubtotal"))
				curShippingFee = prepInt(rs("ordershippingfee"))
				curTax = prepInt(rs("orderTax"))
				curGrandTotal = prepInt(rs("ordergrandtotal"))
				accountid = prepInt(rs("accountid"))
			end if
			%>
            <div style="float:left; width:100%; margin:15px 0px 0px 0px; font-size:12px;">
                *Items will be added to your current order. You will see a 2nd charge for $<span id="addlGrandTotal">0.00</span> on your credit card. 
                Total includes $<span id="addlTaxTotal">0.00</span> in sales tax for California residents. 
                This is a CellularOutfitter.com offer.
            </div>
        </div>
    </div>
    <div style="clear:both; height:15px;"></div>
    <input type="hidden" name="addlGrandTotal" value="" />
    <input type="hidden" name="grandTotal" value="<%=curGrandTotal%>" />
    <input type="hidden" name="taxTotal" value="<%=curTax%>" />
    <input type="hidden" name="hidCount" value="<%=lap%>" />
    <input type="hidden" name="orderid" value="<%=orderid%>" />
    <input type="hidden" name="transid" value="<%=auth_transactionid%>" />
	</form>
</div>
<%
call CloseConn(oConn)

function getRatingAvgStar(rating)
	dim nRating 
	dim strRatingImg
	nRating = cdbl(rating)
	strRatingImg = 	"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />" & vbcrlf & _
					"<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" />"
	
	if nRating > 0 or nRating <=5 then
		strRatingImg = ""	
		for i=1 to 5
			if nRating => i then
				strRatingImg = strRatingImg & "<img src=""/images/review/greenStarFull.gif"" border=""0"" width=""14"" height=""13"" /> "
			elseif nRating => ((i - 1) + .1) then
				strRatingImg = strRatingImg & "<img src=""/images/review/greenStarHalf.gif"" border=""0"" width=""8"" height=""13"" /> "				
			else
				strRatingImg = strRatingImg & "<img src=""/images/review/grayStar.gif"" border=""0"" width=""14"" height=""13"" /> "
			end if
		next		
	end if
	
	getRatingAvgStar = strRatingImg
end function
%>