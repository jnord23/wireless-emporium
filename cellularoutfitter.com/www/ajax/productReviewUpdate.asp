<%
	response.buffer = true
	response.expires = -1
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!-- METADATA TYPE="typelib" NAME="ADODB Type Library" UUID="00000205-0000-0010-8000-00AA006D2EA4" -->
<%
	siteID = prepInt(request.form("siteID"))
	reviewerEmail = prepStr(request.form("reviewerEmail"))
	reviewerNickName = prepStr(request.form("reviewerNickName"))
	reviewerLocation = prepStr(request.form("reviewerLocation"))
	reviewerAge = prepInt(request.form("reviewerAge"))
	reviewerGender = prepStr(request.form("reviewerGender"))
	reviewerType = prepStr(request.form("reviewerType"))
	orderID = prepInt(request.form("orderID"))
	itemID = prepInt(request.form("itemID"))
	starRating = prepInt(request.form("starRating"))
	ownershipPeriod = prepInt(request.form("ownershipPeriod"))
	reviewTitle = prepStr(request.form("reviewTitle"))
	reviewBody = prepStr(request.form("reviewBody"))
	isRecommend = prepInt(request.form("isRecommend"))
	isProPrice = prepInt(request.form("isProPrice"))
	isProDesign = prepInt(request.form("isProDesign"))
	isProWeight = prepInt(request.form("isProWeight"))
	isProProtection = prepInt(request.form("isProProtection"))
	otherPro = prepStr(request.form("otherPro"))
	isConFit = prepInt(request.form("isConFit"))
	isConRobust = prepInt(request.form("isConRobust"))
	isConMaintenance = prepInt(request.form("isConMaintenance"))
	isConWeight = prepInt(request.form("isConWeight"))
	otherCon = prepStr(request.form("otherCon"))
	
	Dim cmd
	Set cmd = Server.CreateObject("ADODB.Command")
	Set cmd.ActiveConnection = oConn
	cmd.CommandText = "insertXReview"
	cmd.CommandType = adCmdStoredProc 

	cmd.Parameters.Append cmd.CreateParameter("siteID", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("reviewerEmail", adVarChar, adParamInput, 120)
	cmd.Parameters.Append cmd.CreateParameter("reviewerNickName", adVarChar, adParamInput, 50)
	cmd.Parameters.Append cmd.CreateParameter("reviewerLocation", adVarChar, adParamInput, 50)
	cmd.Parameters.Append cmd.CreateParameter("reviewerAge", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("reviewerGender", adChar, adParamInput, 1)
	cmd.Parameters.Append cmd.CreateParameter("reviewerType", adVarChar, adParamInput, 10)
	cmd.Parameters.Append cmd.CreateParameter("orderID", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("itemID", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("starRating", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("ownershipPeriod", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("reviewTitle", adVarChar, adParamInput, 100)
	cmd.Parameters.Append cmd.CreateParameter("reviewBody", adVarChar, adParamInput, 8000)
	cmd.Parameters.Append cmd.CreateParameter("isRecommend", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("isProPrice", adCurrency, adParamInput)	
	cmd.Parameters.Append cmd.CreateParameter("isProDesign", adCurrency, adParamInput)	
	cmd.Parameters.Append cmd.CreateParameter("isProWeight", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("isProProtection", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("otherPro", adVarChar, adParamInput, 8000)
	cmd.Parameters.Append cmd.CreateParameter("isConFit", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("isConRobust", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("isConMaintenance", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("isConWeight", adInteger, adParamInput)
	cmd.Parameters.Append cmd.CreateParameter("otherCon", adVarChar, adParamInput, 8000)

	cmd("siteID")			=	siteID
	cmd("reviewerEmail")	=	reviewerEmail
	cmd("reviewerNickName")	=	reviewerNickName
	cmd("reviewerLocation")	=	reviewerLocation
	cmd("reviewerAge")		=	reviewerAge
	cmd("reviewerGender")	=	reviewerGender
	cmd("reviewerType")		=	reviewerType
	cmd("orderID")			=	orderID	
	cmd("itemID")			=	itemID
	cmd("starRating")		=	starRating
	cmd("ownershipPeriod")	=	ownershipPeriod
	cmd("reviewTitle")		=	reviewTitle
	cmd("reviewBody")		=	reviewBody
	cmd("isRecommend")		=	isRecommend
	cmd("isProPrice")		=	isProPrice
	cmd("isProDesign")		=	isProDesign
	cmd("isProWeight")		=	isProWeight
	cmd("isProProtection")	=	isProProtection
	cmd("otherPro")			=	otherPro
	cmd("isConFit")			=	isConFit
	cmd("isConRobust")		=	isConRobust
	cmd("isConMaintenance")	=	isConMaintenance
	cmd("isConWeight")		=	isConWeight
	cmd("otherCon")			=	otherCon
	
	cmd.Execute	
	
	call CloseConn(oConn)
%>
<div style="width:800px; text-align:left; font-size:15px; margin-left:auto; margin-right:auto; margin-top:100px; position:relative; border:1px solid #ccc; background-color:#fff; color:#333; border-radius:5px; font-family:Arial, Helvetica, sans-serif; -moz-box-shadow: 0px 0px 5px 5px #333; -webkit-box-shadow: 0px 0px 5px 5px #333; box-shadow: 0px 0px 5px 5px #333;">
	<div style="position:absolute; top:-15px; right:-10px; cursor:pointer;" onclick="closeReviewPopup()"><img src="/images/icon-lightbox-close.png" border="0" /></div>
	<div style="margin:10px 0px 10px 0px; padding:10px 30px 10px 10px; width:760px; height:200px; overflow:auto;">
        <div style="padding-top:5px; font-size:20px; color:#333; text-align:center;">
			<img src="/images/review/checkmark.jpg" border="0" width="19" height="15" />&nbsp;
			<b>Thank you for your response!<br><br>It will be reviewed before being posted on the site.</b>        
        </div>    
    </div>
</div>