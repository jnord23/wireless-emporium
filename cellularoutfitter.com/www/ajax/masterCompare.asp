<%
	response.buffer = true
	response.expires = -1

%><!--#include virtual="/includes/asp/inc_BasePage.asp"--><%	
%><!--#include virtual="/includes/asp/inc_formatSEO.asp"--><%

	dim mpPageName : mpPageName = prepStr(request.QueryString("mpPageName"))
	dim compPageName : compPageName = prepStr(request.QueryString("compPageName"))
	dim forceRefresh : forceRefresh = 0
	dim replacePage : replacePage = 0
	dim serverName : serverName = "http://" & request.ServerVariables("SERVER_NAME")
	dim fso : set fso = CreateObject("Scripting.FileSystemObject")
	
	mpPageName = replace(mpPageName,"$QM","?")
	mpPageName = replace(mpPageName,"$AM","&")
	if instr(mpPageName,"<") > 0 then mpPageName = left(mpPageName,instr(mpPageName,"<")-1)
	if instr(compPageName,"<") > 0 then compPageName = left(compPageName,instr(compPageName,"<")-1)
	
	if instr(mpPageName,".asp") > 0 then
		curPage = compPageName
		testPage = replace(compPageName,".htm","_test.htm")
		
		response.Write("Searching for:" & curPage & "<br><br>")
		sql = "select lastUpdate from we_compressedPages where siteID = 2 and pageName = '" & curPage & "'"
		set lastUpdateRS = oConn.execute(sql)
		
		if lastUpdateRS.EOF then
			response.Write("Record not found: Enter new record and force refresh<br><br>")
			sql = "insert into we_compressedPages (siteID,pageName) values(2,'" & curPage & "')"
			oConn.execute(sql)
			forceRefresh = 1
		else
			response.Write("Record found!<br><br>")
			lastDate = lastUpdateRS("lastUpdate")
			if datediff("n",lastDate,now) > 60 then
				response.Write("File has expired: Force refresh<br><br>")
				sql = "update we_compressedPages set lastUpdate = '" & now & "' where pageName = '" & curPage & "' and siteID = 2"
				oConn.execute(sql)
				forceRefresh = 1
			else
				response.Write("File still good: " & lastDate & " {vs} " & now & " = " & datediff("n",lastDate,now) & " minutes old<br><br>")
			end if
		end if
	end if
	
	if forceRefresh = 1 then
		useURL = serverName & "/" & mpPageName
		set XMLHTTP = server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
		response.Write("Get MP File:" & useURL & "<br><br>")
		response.Flush()
		XMLHTTP.Open "GET", useURL, False
		session("errorMsg") = useURL
		XMLHTTP.Send
		
		allText = xmlHTTP.ResponseText
		
		response.Write("Compress HTML<br><br>")
		if instr(lcase(allText),"<script") > 0 then
			lapCnt = 0
			do while len(allText) > 0
				lapCnt = lapCnt + 1
				if instr(lcase(allText),"<script") > 0 then
					nextScript = instr(allText,"<script")
					tempHolding = left(allText,nextScript-1)
					session("errorSQL") = "nextScript:" & nextScript & "<br>" & allText
					if nextScript > 1 then allText = mid(allText,nextScript-1)
					
					tempHolding = replace(tempHolding,vbCrLf,"")
					tempHolding = replace(tempHolding,"  ","")
					showPage = showPage & replace(tempHolding,vbTab,"")
					
					endScript = instr(lcase(allText),"</script>")
					showPage = showPage & vbCrLf & left(allText,endScript+8)
					allText = mid(allText,endScript+9)
				else
					tempHolding = replace(allText,vbCrLf,"")
					tempHolding = replace(tempHolding,"  ","")
					showPage = showPage & replace(tempHolding,vbTab,"")
					allText = ""
				end if	
				if lapCnt = 100 then
					response.Write(showPage)
					response.Write("force stop<br />" & vbCrLf)
					call CloseConn(oConn)
					response.End()
				end if
			loop
		else
			showPage = replace(allText,vbCrLf,"")
			showPage = replace(showPage,"  ","")
			showPage = replace(showPage,vbTab,"")
		end if
		
		response.Write("Create file temp file:" & server.MapPath(testPage) & "<br><br>")
		set tfile = fso.createTextFile(server.MapPath(testPage),true,true)
		tfile.writeLine(showPage)
		tfile.close	
		tfile = null
		newFile = 1
		
		response.Write("curPage:" & curPage & "<br><br>")
		response.Write("testPage:" & testPage & "<br><br>")
		set curFile = fso.OpenTextFile(server.MapPath(curPage),1,0,-1)
		set testFile = fso.OpenTextFile(server.MapPath(testPage),1,0,-1)
	
		x = 0
		response.Write("Compare files!<br><br>")
		do until curFile.AtEndOfStream
			curLine = curFile.ReadLine
			if testFile.AtEndOfStream then
				if trim(curLine) <> "" then replacePage = 1
			else
				testLine = testFile.ReadLine
			end if
			if instr(curLine,"CUSTOMER TESTIMONIALS") > 0 then curLine = left(curLine,instr(curLine,"CUSTOMER TESTIMONIALS"))
			if instr(testLine,"CUSTOMER TESTIMONIALS") > 0 then testLine = left(testLine,instr(testLine,"CUSTOMER TESTIMONIALS"))
			if curLine <> testLine then
				if trim(curLine) <> "" then 
					replacePage = 1
				end if
			end if
			prevCurLine = curLine
			prevTestLine = testLine
			x = x + 1
		loop
		
		if not testFile.AtEndOfStream then replacePage = 1
		
		testFile.close
		testFile = null
		
		if replacePage = 1 then
			response.Write("New Page Found<br><br>")
			sql = "update we_compressedPages set updateWasNeeded = 1 where pageName = '" & curPage & "' and siteID = 2"
			oConn.execute(sql)
			set newCode = fso.OpenTextFile(server.MapPath(testPage),1,0,-1)
			set tfile = fso.createTextFile(server.MapPath(curPage),true,true)
			tfile.writeLine(newCode.readAll)
			tfile.close	
			tfile = null
			newCode.close	
			newCode = null
			
			'Here is where we actually make the stitch image
			if totalSrc <> "" then
				response.Write("make new image with:" & totalSrc & "<br><br>")
				totalSrc = left(totalSrc,len(totalSrc)-2)
				totalSrcArray = split(totalSrc,"##")
				
				set oImg = Server.CreateObject("Persits.Jpeg")
				set oJpeg = Server.CreateObject("Persits.Jpeg")
				oJpeg.Quality = 80
				oJpeg.Interpolation = 1
				
				oJpeg.New newCanvasWidth, newCanvasHeight, &HFFFFFF
				
				for i = 0 to ubound(totalSrcArray)
					imgDetailArray = split(totalSrcArray(i),"@@")
					imgSrc = imgDetailArray(0)
					imgLocX = imgDetailArray(1)
					
					oImg.Open server.MapPath(imgSrc)
					
					oJpeg.Canvas.DrawImage imgLocX, 0, oImg
				next
				
				oJpeg.Save server.MapPath("/productPics/stitch/" & stitchFileName)
				
				set oJpeg = nothing
				set oImg = nothing
			end if
		else
			sql = "update we_compressedPages set updateWasNeeded = 0 where pageName = '" & curPage & "' and siteID = 2"
			oConn.execute(sql)
			response.Write("file still good (1)<br><br>")
		end if
		
		'Delete the temp file
		if fso.fileExists(server.MapPath(testPage)) then
			response.Write("Deleting the temp file at:" & server.MapPath(testPage) & "<br><br>")
			response.Flush()
			fso.DeleteFile server.MapPath(testPage),true
		else
			response.Write("Can't find temp file to pop:" & testPage & "<br><br>")
		end if
	else
		response.Write("file still good (2)<br><br>")
	end if
	response.Write("Master Compare Complete<br><br>")

	call CloseConn(oConn)
%>