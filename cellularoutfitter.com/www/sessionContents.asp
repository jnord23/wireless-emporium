<%
dim item 'will be used to iterate through the contents
%>

<html>
<head>
<title>Collection Information</title>
</head>

<body>

<table border="1" width="90%">
	<tr>
		<th colspan=2>Session</th>
	</tr>
	<%
	'In case a particular item gives us trouble
	on error resume next
	for each item in Session.Contents
		if item <> "oConn" then
			%>
			<tr>
				<td><%=item%></td><!--Show the item's name-->
				<td><%=dumpitem(Session(item))%></td><!--Show the Item's contents-->
			</tr>
			<%
		end if
	next
	%>
</table>

<table border="1" width="90%">
	<tr>
		<th colspan=2>Application</th>
	</tr>
	<%
	for each item in Application.Contents
		%>
		<tr>
			<td><%=item%></td>
			<td><%=dumpItem(application(item))%></td>
		</tr>
		<%
	next
	%>
</table>

<p>&nbsp;</p>

<%
dim a(300), b, c
For Each i in Session.Contents
	a(b) = i
	b = b + 1
Next
for c = 0 to b-1
	if a(c) <> "oConn" and a(c) <> "Blocked" and a(c) <> "fname" and a(c) <> "lname" and a(c) <> "accountid" and a(c) <> "validUser" then
		response.write "Session.Contents.Remove("& a(c) & ")<br>"
		'Session.Contents.Remove(a(c))
	end if
next
%>

<p>&nbsp;</p>

<table border="1" width="90%">
	<tr>
		<th colspan=2>Session</th>
	</tr>
	<%
	'In case a particular item gives us trouble
	on error resume next
	for each item in Session.Contents
		if item <> "oConn" then
			%>
			<tr>
				<td><%=item%></td><!--Show the item's name-->
				<td><%=dumpitem(Session(item))%></td><!--Show the Item's contents-->
			</tr>
			<%
		end if
	next
	%>
</table>

<%
function dumpItem(item)
	'If it's an array we use this to iterate through it
	dim subItem
	dim result  'Will store the result
	
	'If it's an array we have to grab its contents.  
	if TypeName(item) = "Variant()" then 
		for each subItem in item
			'Separate each with a space
			result = result & " " & subItem
		next
	else
		result = item
	end if
	
	dumpItem = result
end function
%>

<p>&nbsp;</p>

</body>
</html>
