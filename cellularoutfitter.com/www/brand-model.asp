<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/asp/inc_formatSEO.asp"-->
<!--#include virtual="/includes/asp/inc_301redirect.asp"-->
<%
dim modelPage
modelPage = 1

dim modelid
modelid = request.querystring("modelid")
if modelid = "" or not isNumeric(modelid) then
	response.redirect("/")
	response.end
end if

saveModelID = modelID

response.Cookies("myBrand") = 0
response.Cookies("myModel") = modelID

dim pageTitle
pageTitle = "Brand-Model"

call fOpenConn()

sql	= "exec sp_getCatsByModel " & modelID & ", 2"
session("errorSQL") = SQL
set RS = oConn.execute( SQL)

if RS.eof then
	call fCloseConn()
	response.redirect("/")
	response.end
end if

sql = "select count(*) as itemCnt from we_Items a left join we_ItemsExtendedData b on a.partNumber = b.partNumber where b.customize = 1 and a.modelID = " & modelid
session("errorSQL") = sql
set customRS = oConn.execute(sql)

dim showCustom : showCustom = 0
if customRS("itemCnt") > 0 then showCustom = 1

dim brandName, brandID, modelName, modelImg

brandName = RS("brandName")
brandID = RS("brandID")
modelName = RS("modelName")
modelImg = RS("modelImg")

'=========================================================================================
Dim oParam : set oParam = CreateObject("Scripting.Dictionary")
	oParam.CompareMode = vbTextCompare
	oParam.Add "x_brandName", brandName
	oParam.Add "x_modelName", modelName

call redirectURL("m", modelid, request.ServerVariables("HTTP_X_REWRITE_URL"), oParam)
'=========================================================================================		

dim SEtitle, SEdescription, SEkeywords, topText, bottomText, h1, h2, altText

saveTopText = topText
if altText = "" then altText = brandName & " " & modelName

dim strBreadcrumb
strBreadcrumb = brandName & " " & modelName & " Cell Phone Accessories"

h1= brandName &" "& modelName &" Cell Phone Accessories"

dim strH1: strH1 = h1
dim strH2: strH2 = EMPTY_STRING
dim strAltText: strAltText = altText

'set input values to extract page meta-values
dim dicReplaceAttribute: set dicReplaceAttribute = CreateObject("Scripting.Dictionary")
dicReplaceAttribute( "BrandName") = brandName
dicReplaceAttribute( "ModelName") = modelName

'set known product attributes to replace out template placeholders
dim dicSeoAttributeInput: set dicSeoAttributeInput = CreateObject("Scripting.Dictionary")
dicSeoAttributeInput( "BrandId") = brandID
dicSeoAttributeInput( "ModelId") = modelID

'get content event text (initially installed to load pixels) [knguyen/20110520]
'todo: deploy to other wrappers as needed
dim dicContentEventText: set dicContentEventText = GetContentEventText( dicSeoAttributeInput)
'call LookupSeoAttributes()
%>
<!--#include virtual="/includes/template/top_index.asp"-->
<script>
window.WEDATA.pageType = 'brandModel';
window.WEDATA.pageData = {
    brand: <%= jsStr(brandName) %>,
    brandId: <%= jsStr(brandId) %>,
    model: <%= jsStr(modelName) %>,
    modelId: <%= jsStr(modelId) %>
};
</script>

    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td class="top-sublink-gray">
                <a class="top-sublink-gray" href="/">Cell Phone Accessories</a>&nbsp;>&nbsp;
                <a class="top-sublink-gray" href="/b-<%=brandID & "-" & formatSEO(brandName)%>-cell-phone-accessories.html"><%=brandName%> Cell Phone Accessories</a>&nbsp;>&nbsp;
                <span class="top-sublink-blue"><%=brandName & " " & modelName%> Cell Phone Accessories</span>
            </td>
        </tr>
        <tr>
            <td class="top-sublink-gray"><img src="/images/spacer.gif" width="1" height="10"></td>
        </tr>
        <tr>
            <td align="center" width="100%">
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td align="center" width="200" valign="top"><div style="width:200px; text-align:center; height:120px;"><img src="/modelpics/<%=modelImg%>" align="center" border="0" height="106" alt="<%=strBreadcrumb%>"></div></td>
                        <td align="center" class="static-content-font">
                            <h1 class="nevi-font-bold"><%=seH1%></h1>
                            <div style="padding-top:5px;"><%=SeTopText%></div>
                            <%' XXX=brandName : YYY = modelName %>
                            <%'#include virtual="/Framework/seoContent.asp"-->%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="middle" width="100%" style="padding:5px 0px 5px 10px; background-color:#CCC; border-top:1px solid #000; border-bottom:1px solid #000;">
                <div style="text-shadow: 1px 1px #fff; font-family: Arial; font-size: 18px; color: #000;"><%=strH1%></div>
                <!--
                <div style="width:100%; height:20px; position:relative;">
                    <div style="position:absolute; top:0px; left:0px; z-index:100; font-family: Arial; font-size: 18px; color: #000;"><%=strH1%></div>
                    <div style="position:absolute; top:1px; left:2px; z-index:50; font-family: Arial; font-size: 18px; color: #fff;"><%=strH1%></div>
                </div>
                -->
            </td>
        </tr>
        <tr>
            <td><img src="/images/spacer.gif" width="1" height="10" border="0"></td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-bottom:30px;">
                <%
                if brandID <> 11 then
                    %>
                    <div style="float:left; width:160px; margin:20px 0px 0px 80px;">
                        <div style="height:100px; width:160px; overflow:hidden;"><a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-5-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>-hands-free-kits-bluetooth-headsets.html"><img src="/images/categories/CO_cat_buttons_<%=formatSEO(replace("bluetooth & hands-free", "&", "and"))%>.jpg" border="0" alt="<%=altText%> Hands-Free Kits &amp; Bluetooth Headsets"></a></div>
                        <div style="width:160px; height:20px; padding-top:5px; color:#000; font-weight:bold; text-align:center; font-size:11px; background-color:#CCC;"><a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-5-<%=formatSEO(brandName)%>-<%=formatSEO(modelName)%>-hands-free-kits-bluetooth-headsets.html" style="color:#000; font-size:11px;">BLUETOOTH &amp; HANDS-FREE</a></div>                                                    
                    </div>
                    <%
                end if
                do until RS.eof
                    categoryID = RS("typeid")
                    if categoryID <> 5 then
                        textType = RS("typename")
                        picType = replace(textType, "&", "and")
                        textType = replace(textType,"Cell Phone ","")
                        linkType = replace(replace(textType,"&amp;","and"), "&", "and")
                        if categoryID = 19 then
                            strLink = "/sb-" & brandID & "-sm-" & modelID & "-sc-" & RS("typeid") & "-" & formatSEO(brandname) & "-" & formatSEO(modelName) & "-" & formatSEO(linkType) & "-select.html"
                        else
                            strLink = "/sb-" & brandID & "-sm-" & modelID & "-sc-" & RS("typeid") & "-" & formatSEO(brandname) & "-" & formatSEO(modelName) & "-" & formatSEO(linkType) & ".html"
                        end if
                        %>
                        <div style="float:left; width:160px; margin:20px 0px 0px 80px;" title="<%=altText & " " & textType%>">
                            <div style="height:100px; width:160px; overflow:hidden;"><a href="<%=strLink%>"><img src="/images/categories/CO_cat_buttons_<%=formatSEO(picType)%>.jpg" border="0" /></a></div>
                            <div style="width:160px; height:20px; padding-top:5px; color:#000; font-weight:bold; text-align:center; font-size:11px; background-color:#CCC;"><a href="<%=strLink%>" style="color:#000; font-size:11px;"><%=ucase(textType)%></a></div>
                        </div>
                        <%
                    end if
                    RS.movenext
                loop
                %>
                <%
                if showCustom = 1 then
                %>
                <div style="float:left; width:160px; margin:20px 0px 0px 80px;">
                    <div style="height:100px; width:160px; overflow:hidden;"><a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-24-cell-phone-custom-cases.html"><img src="/images/categories/CO_cat_buttons_customCases.jpg" border="0" alt="<%=altText%> Custom Cases"></a></div>
                    <div style="width:160px; height:20px; padding-top:5px; color:#000; font-weight:bold; text-align:center; font-size:11px; background-color:#CCC;"><a href="/sb-<%=brandID%>-sm-<%=modelID%>-sc-24-cell-phone-custom-cases.html" style="color:#000; font-size:11px;">CUSTOM CASES</a></div>
                </div>
                <%
                end if
                %>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding:10px 0px 10px 0px;">
            	<%turnToItemID = "M" & saveModelID%>
                <!--#include virtual="/includes/asp/inc_turnto.asp"-->
            </td>
		</tr>
        <%if prepStr(SeBottomText) <> "" then%>
        <tr>
            <td align="center">
                <div style="padding:10px; font-size:11px; text-align:left;"><%=SeBottomText%></div>
            </td>
        </tr>
        <%end if%>
        <!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
    </table>
<%
call fCloseConn()
%>

<!--#include virtual="/includes/template/bottom.asp"-->