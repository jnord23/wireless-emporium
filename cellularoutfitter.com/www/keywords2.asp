<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
dim SQL, RS, nPage, nLinkCount, aCount, loopCount
nPage = request.querystring("page")
if not isNumeric(nPage) then
	response.redirect("/keywords2.asp?page=1")
	response.end
end if

call fOpenConn()
SQL = "SELECT COUNT(*) AS count FROM CO_search_keywords_2"
set RS = oConn.execute(SQL)
if RS.eof then
	response.redirect("/keywords2.asp?page=1")
	response.end
else
	nLinkCount = RS("count")
end if
if nPage < 1 or (nPage - 1) * 100 > nLinkCount then
	response.redirect("/keywords2.asp?page=1")
	response.end
end if
SQL = "SELECT keywords FROM CO_search_keywords_2 ORDER BY visits DESC, keywords"
set RS = oConn.execute(SQL)
RS.move (nPage - 1) * 100

dim pageTitle
pageTitle = "Top Keywords Search Sitemap 2 - Page " & nPage & " | CellularOutfitter.com"
dim SEtitle, SEdescription, SEkeywords
SEtitle = pageTitle
SEdescription = "Sitemap for top searched keywords on Cellularoutfitter.com"
%>
<!--#include virtual="/includes/template/top.asp"-->
    <table width="775" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td class="top-sublink-gray">
                <a class="top-sublink-gray" href="http://www.CellularOutfitter.com/">Cell Phone Accessories</a>&nbsp;>&nbsp;
                <span class="top-sublink-blue"><%=pageTitle%></span>
            </td>
        </tr>
        <tr>
            <td class="top-sublink-gray"><img src="http://www.cellularoutfitter.com/images/spacer.gif" width="1" height="10"></td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <table width="100%" border="0" cellspacing="5" cellpadding="5" align="center">
                    <tr>
                        <%
                        loopCount = 0
                        do until RS.eof
                            loopCount = loopCount + 1
                            aCount = aCount + 1
                            if aCount = 1 then response.write vbtab & "</tr>" & vbcrlf & vbtab & "<tr>" & vbcrlf
                            response.write vbtab & vbtab & "<td class=""cate-pro-border""><a href=""http://www.cellularoutfitter.com/search/search.asp?keywords=" & server.URLencode(RS("keywords")) & """ title=""" & RS("keywords") & """ class=""topsale-link"">" & RS("keywords") & "</a></td>" & vbcrlf
                            if aCount = 4 then aCount = 0
                            if loopCount = 100 then exit do
                            RS.movenext
                        loop
                        %>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" bgcolor="#CCCCCC" class="topsale">
                <%
                for aCount = 1 to int(nLinkCount / 100) + 1
                    if cStr(aCount) <> nPage then response.write "<a href=""/keywords2.asp?page=" & aCount & """ class=""topsale-link"">"
                    response.write aCount
                    if cStr(aCount) <> nPage then response.write "</a>"
                    response.write "&nbsp;&nbsp;&nbsp;"
                    if aCount = 35 then response.write "<br>" & vbcrlf
                next
                %>
            </td>
        </tr>
    </table>
<%
RS.close
set RS = nothing
call fCloseConn()
%>

<!--#include virtual="/includes/template/bottom.asp"-->
