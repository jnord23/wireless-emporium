<%
response.buffer = true
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<%
dim carrierid
carrierid = request.querystring("carrierid")
if carrierid = "" or not isNumeric(carrierid) then
	response.redirect("/")
	response.end
end if

call fOpenConn()
dim SQL, RS
SQL = "SELECT * FROM we_Carriers WHERE id = '" & carrierid & "'"
'session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3
if RS.eof then
	call fCloseConn()
	response.redirect("/")
	response.end
else
	dim carrierName, carrierCode, carrierImg
	carrierName = RS("carrierName")
	carrierCode = RS("carrierCode")
	carrierImg = RS("carrierImg")
end if

dim brandid
brandid = request.querystring("brandid")
if brandid = "" or not isNumeric(brandid) then
	response.redirect("/")
	response.end
end if

SQL = "SELECT DISTINCT A.*, C.* FROM (we_models A INNER JOIN we_items B ON A.modelID = B.modelID)"
SQL = SQL & " INNER JOIN we_brands C ON A.brandid = C.brandid"
SQL = SQL & " WHERE B.brandid = '" & brandid & "'"
SQL = SQL & " AND B.hideLive = 0 AND patindex('%All %',A.modelname) = 0"
SQL = SQL & " AND A.carrierCode LIKE '%" & carrierCode & "%'"
SQL = SQL & " ORDER BY A.temp DESC"
'session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 3, 3

if RS.eof then
	call fCloseConn()
	response.redirect("/")
	response.end
end if

dim brandName, brandImg
brandName = RS("brandName")
brandImg = RS("brandImg")

dim oRsTypes

dim pageTitle
pageTitle = itemDesc_CO

select case carrierid
	case "1"
		H1color = "FFFFFF"
	case "2"
		H1color = "FFFFFF"
	case "3"
		H1color = "FFFFFF"
	case "4"
		H1color = "FFFFFF"
	case "5"
		H1color = "FFFFFF"
	case "6"
		H1color = "FFFFFF"
	case "7"
		H1color = "000000"
	case "8"
		H1color = "000000"
	case "9"
		H1color = "FFFFFF"
	case "10"
		H1color = "FFFFFF"
	case "11"
		H1color = "FFFFFF"
end select

dim strBreadcrumb
strBreadcrumb = carrierName & " " & brandName & " Cell Phone Accessories"
%>

<!--#include virtual="/includes/template/top.asp"-->
<script>
window.WEDATA.pageType = "carrierBrand";
window.WEDATA.pageData = {
	carrier: <%= jsStr(carrierName) %>,
	carrierId: <%= jsStr(carrierId) %>,
	brand: <%= jsStr(brandName) %>,
	brandId: <%= jsStr(brandId) %>
};
</script>

<style>
	h1 {
		color: #<%=H1color%>;
	}
</style>

								<td width="5">&nbsp;</td>
								<td width="775" valign="top">
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td class="top-sublink-gray">
												<a class="top-sublink-gray" href="/">Cell Phone Accessories</a>&nbsp;>&nbsp;
												<a class="top-sublink-gray" href="/car-<%=carrierid & "-" & formatSEO(carrierName)%>.html"><%=carrierName%> Cell Phone Accessories</a>&nbsp;>&nbsp;
												<span class="top-sublink-blue"><%=carrierName & " " & brandName%> Cell Phone Accessories</span>
											</td>
										</tr>
										<tr>
											<td class="top-sublink-gray"><img src="/images/spacer.gif" width="1" height="10"></td>
										</tr>
										<tr>
											<td valign="middle" width="100%" height="68" style="background-image: url('/images/carriers/CO_carrier_header_<%=formatSEO(carrierName)%>.jpg'); background-repeat: no-repeat; background-position: center bottom;">
												<h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=carrierName & " " & brandName%> Cell Phone Accessories</h1>
											</td>
										</tr>
										<tr>
											<td class="top-sublink-gray"><img src="/images/spacer.gif" width="1" height="10"></td>
										</tr>
										<tr>
											<td width="100%" align="center" valign="top">
												<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
													<tr>
														<td width="405" align="center" valign="top" style="border-style: solid; border-width: 1px; border-color: #999999;"><img src="/images/carriers/CO_carrier_logo_<%=formatSEO(carrierName)%>.jpg" border="0" width="190" height="119"><img src="/images/spacer.gif" width="10" height="1" border="0"><img src="/images/brands/CO_CATpage_buttons_<%=formatSEO(brandName)%>.jpg" align="center" border="0" alt="<%=brandName%>"></td>
														<td width="370" align="center" valign="middle" class="headerText" style="border-style: solid; border-width: 1px; border-color: #999999;">
															<img src="/images/spacer.gif" width="1" height="10">
															Which <%=Ucase(brandName)%> model do you have?
															<br>
															<form>
																<select name="selectModel" onChange="if(this.value != ''){window.location=this.value;}">
																	<option value="">Select from this list or click on an image below</option>
																	<%
																	do until RS.eof
																		response.write "<option value=""/model.asp?modelid=" & RS("modelID") & """>" & RS("brandName") & " " & RS("modelName") & "</option>" & vbCrLf
																		RS.movenext
																	loop
																	RS.movefirst
																	%>
																</select>
															</form>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="100%" align="center" valign="top">
												<table border="0" align="center" cellspacing="0" cellpadding="0" width="540">
													<tr>
														<td class="contain">
															<table cellspacing="0" cellpadding="0" border="0">
																<tr>
																	<td align="left" height="10" valign="top"></td>
																</tr>
																<tr>
																	<td>
																		<table border="0" align="center" cellspacing="0" cellpadding="0" width="746">
																			<tr>
																				<td>
																					<table border="0" align="center" cellspacing="0" cellpadding="0" width="746">
																						<tr>
																							<td>&nbsp;</td>
																						</tr>
																						<tr>
																							<td align="left" valign="top">
																								<table border="0" cellspacing="0" cellpadding="0" width="744">
																									<tr>
																										<%
																										a = 0
																										do until RS.eof
																											%>
																											<td align="center" valign="top" width="248">
																												<table border="0" cellspacing="0" cellpadding="0" width="240">
																													<tr>
																														<td align="center" valign="top" width="119">
																															<table border="0" cellspacing="0" cellpadding="0" width="119">
																																<tr>
																																	<td align="center" valign="middle" class="cellphone-top-table"><img src="/images/spacer.gif" width="1" height="6"></td>
																																</tr>
																																<tr>
																																	<td align="center" valign="middle" class="cellphone-middle-table">
																																		<table border="0" cellspacing="0" cellpadding="0" width="100">
																																			<tr>
																																				<td align="center" valign="bottom" class="cellphone-font" height="18"><a class="cellphone-font" href="/m-<%=RS("modelID")%>-<%=formatSEO(RS("brandName")) & "-" & formatSEO(RS("modelName"))%>.html"><%=RS("modelName")%></a></td>
																																			</tr>
																																			<tr>
																																				<td align="center" valign="middle" height="150"><a href="/m-<%=RS("modelID")%>-<%=formatSEO(RS("modelName"))%>.html"><img src="/modelpics/<%=RS("modelImg")%>" align="center" border="0" alt="<%=brandName & " " & RS("modelName")%>"></a></td>
																																			</tr>
																																		</table>
																																	</td>
																																</tr>
																																<tr>
																																	<td align="center" valign="middle" class="cellphone-bottom-table"><span class="cellphone-top-table"><img src="/images/spacer.gif" width="1" height="6"></span></td>
																																</tr>
																															</table>
																														</td>
																														<td align="center" valign="top">
																															<table border="0" cellspacing="0" cellpadding="0" width="110">
																																<tr>
																																	<td align="left" valign="top" class="cellphone-category-font">
																																		<%
																																		response.write "<a class=""cellphone-category-font-link"" href=""/hf-sm-" & RS("modelID") & "-" & formatSEO(RS("brandName")) & "-" & formatSEO(RS("modelName")) & "-hands-free-kits-bluetooth-headsets.html"" title=""Hands-Free Kits &amp; Bluetooth Headsets for " & brandName & " " & modelName & """>Handsfree</a><br>" & vbcrlf
																																		SQL = "SELECT DISTINCT A.typeid,B.typename FROM we_items A INNER JOIN we_types B ON A.typeid=B.typeid WHERE A.modelid=" & RS("modelid") & " AND A.hideLive = 0 AND inv_qty <> 0"
																																		set oRsTypes = Server.CreateObject("ADODB.Recordset")
																																		oRsTypes.open SQL, oConn, 3, 3
																																		do until oRsTypes.eof
																																			response.write "<a class=""cellphone-category-font-link"" href=""/sc-" & oRsTypes("typeid") & "-sb-" & brandID & "-sm-" & RS("modelID") & "-cell-phone-" & formatSEO(oRsTypes("typename")) & "-" & formatSEO(RS("modelName")) & ".html"" title=""" & nameSEO(oRsTypes("typename")) & " for " & brandName & " " & RS("modelName") & """>" & oRsTypes("typename") & "</a><br>" & vbcrlf
																																			oRsTypes.movenext
																																		loop
																																		%>
																																	</td>
																																</tr>
																															</table>
																														</td>
																													</tr>
																												</table>
																											</td>
																											<%
																											RS.movenext
																											a = a + 1
																											if a = 3 then
																												response.write "</tr><tr><td align=""center"" valign=""top"">&nbsp;</td></tr><tr>" & vbcrlf
																												a = 0
																											end if
																										loop
																										if a = 1 then
																											response.write "<td width=""248"">&nbsp;</td><td width=""248"">&nbsp;</td>" & vbcrlf
																										elseif a = 2 then
																											response.write "<td width=""248"">&nbsp;</td>" & vbcrlf
																										end if
																										%>
																									</tr>
																									<tr>
																										<td align="center" valign="top">&nbsp;</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td>&nbsp;</td>
																</tr>
																<!--#include virtual="/includes/asp/inc_GoogleAdSense.asp"-->
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>

<%
call fCloseConn()
%>

<!--#include virtual="/includes/template/bottom.asp"-->
