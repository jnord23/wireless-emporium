<%
pageTitle = "Privacy Policy"
%>
<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/includes/template/top.asp"-->
<!--#include virtual="/includes/asp/inc_CDOsend.asp"-->
    <table border="0" cellspacing="0" cellpadding="0" width="746">
        <tr>
            <td class="top-sublink-gray">
                <a class="top-sublink-gray" href="http://www.CellularOutfitter.com/">Cell Phone Accessories</a>&nbsp;>&nbsp;
                <span class="top-sublink-blue"><%=pageTitle%></span>
            </td>
        </tr>
        <tr>
            <td class="top-sublink-gray"><img src="http://www.cellularoutfitter.com/images/spacer.gif" width="1" height="10"></td>
        </tr>
        <tr>
            <td class="static-content-font">
                <table cellspacing="0" cellpadding="0" class="static-content-font">
                    <tr>
                        <td valign="top" align="left" height="25">The CellularOutfitter.com Privacy Policy (Effective 6/6/07):</td>
                    </tr>
                    <tr>
                        <td valign="top" align="left">
                            <p align="justify">
                                <img src="http://www.cellularoutfitter.com/images/infopages/privacy-policy.gif" alt="The CellularOutfitter.com Privacy Policy" width="164" height="114" border="0" align="left">
                                <a href="http://www.truste.org/ivalidate.php?url=www.CellularOutfitter.com&sealid=101" target="_blank"><img src="http://www.cellularoutfitter.com/images/infopages/truste-seal-ctv.gif" alt="TRUSTe - Click to Verify" width="68" height="95" border="0" align="right"></a>
                                This privacy statement covers the site www.CellularOutfitter.com. Because we want to demonstrate our commitment to our
                                users&rsquo; privacy, we have agreed to disclose our privacy practices. CellularOutfitter.com is a licensee of the TRUSTe
                                Privacy Program. TRUSTe is an organization whose mission is to build user&rsquo;s trust and
                                confidence in the Internet by promoting the use of fair information practices. This privacy statement covers the Web site
                                www.CellularOutfitter.com. Because this Web site wants to demonstrate its commitment to your privacy, it has agreed to
                                disclose its information practices and have its privacy practices reviewed for compliance by TRUSTe. If you have
                                questions or concerns regarding this statement, you should first contact CellularOutfitter.com at
                                <a href="mailto:info@CellularOutfitter.com">info@CellularOutfitter.com</a>.
                                If you do not receive acknowledgement of your inquiry or your inquiry has not been satisfactorily address,
                                you should contact TRUSTe at
                                <a href="http://www.truste.org/consumers/watchdog_complaint.php" target="_blank">http://www.truste.org/consumers/watchdog_complaint.php</a>
                                TRUSTe will then serve as a liaison with us to resolve your concerns.
                            </p>
                            <p align="justify">
                                If users have questions or concerns regarding this statement, you should first contact CellularOutfitter.com Privacy
                                Policy Compliance Dept. by emailing us at: <a href="mailto:info@CellularOutfitter.com">info@CellularOutfitter.com</a>.
                            </p>
                            <p align="justify">
                                At CellularOutfitter.com, we are committed to <strong>protecting your privacy online</strong>. Our Privacy Policy
                                demonstrates our firm commitment to protecting the privacy of our customers. When you shop at CellularOutfitter.com,
                                we collect transactional information, or Personally Identifiable Information, from you such as your name, telephone
                                number, e-mail address, billing address, shipping address, credit card or other payment information. Any changes,
                                corrections or updates to your transactional information can be edited during the purchase process. If your personally
                                identifiable information changes, or if you no longer desire our service, you may update your information by
                                <a href="http://www.CellularOutfitter.com/contact-us.html">contacting us</a>
                                or by contacting us by postal mail at the business address listed below. CellularOutfitter.com <strong>DOES NOT SHARE,
                                SELL or RENT</strong> your transactional information to others. Though we make every effort to preserve user privacy,
                                we may need to disclose personal information when required by law wherein we have a good-faith belief that such action
                                is necessary to comply with an appropriate law enforcement investigation, current judicial proceeding, a court order
                                or legal process served on our Web site.
                            </p>
                            <p align="justify">
                                CellularOutfitter.com uses customer information only for the purposes of processing and fulfilling orders, contacting
                                purchasers to inform them of their order status, sending opt-in promotional information (which is sent directly by
                                CellularOutfitter.com), and enhancing the user experience. We partner with other third parties to provide intermediary
                                services. For example, we use an outside shipping company (the United States Postal Service) to ship orders, and a
                                credit card processing company to bill users for goods and services. These companies do not retain, share, store or use
                                personally identifiable information for any secondary purposes. When the user purchases our products or services, we
                                share names, or other contact information including billing and shipping address necessary for the third party to
                                provide these services.
                            </p>
                            <p align="justify">
                                At any time you may opt-in to our CellularOutfitter.com Email newsletter which contains relevant promotions and
                                discounts. If you wish to subscribe to our newsletter(s), we will use your name and email address to send the newsletter
                                to you. Out of respect for your privacy, we provide you a way to unsubscribe (See Below). If you choose to use our
                                referral service to tell a friend about our site, we will ask you for your friend�s name and email address.  We will
                                automatically send your friend a one-time email inviting him or her to visit the site. CellularOutfitter.com stores this
                                information for the sole purpose of sending this one-time email and for tracking the success of our referral program.
                                Your friend may contact us at
                                <a href="mailto:info@CellularOutfitter.com">info@CellularOutfitter.com</a>
                                to request that we remove this information from our database.
                            </p>
                            <p align="justify">
                                In addition, we also offer Paypal as a payment option. If selecting Paypal as a payment option, please note that a
                                re-direct to the Paypal servers will take place along with subsequent login steps to facilitate and complete payment.
                                Payments are made to &quot;Wireless Emporium, Inc.&quot;, parent company of CellularOutfitter.com.
                            </p>
                            <p align="justify">
                                The CellularOutfitter.com shopping cart is structured around a secure Yahoo! Store checkout platform. Any information
                                provided on our checkout page is collected by Yahoo! Store on their servers from Customers who purchase merchandise from
                                our site. That information is then shared with CellularOutfitter.com. This information includes ordering information such
                                as shipping and billing names and addresses, phone numbers, email addresses and credit card information. We use this
                                information primarily to fulfill customer orders and requests. We do not share this information with any third parties,
                                other than as described within this policy.
                            </p>
                            <p align="justify">
                                You may contact us for any reason using our <a href="http://www.CellularOutfitter.com/contact-us.html">Contact Us</a> form.
                                In order to contact us, you must complete this contact us form. You are required to give us contact information (such as
                                name, and email address), however, the more information you provide the better our associates may be able to assist you.
                                We use this information to contact you about the services on our site in which you have expressed interest. 
                            </p>
                            <p align="justify">
                                We provide you the opportunity to &rsquo;opt-out&rsquo; of having your personally identifiable information used for
                                certain purposes, when we ask for this information.  For example, if you purchase a product/service but do not wish to
                                receive any additional marketing material from us, you can indicate your preference on our order form.
                            </p>
                            <p align="justify">
                                If you no longer wish to receive our newsletter and promotional communications, you may opt-out of receiving them by
                                following the instructions included in each newsletter or communication or by emailing us at
                                <a href="mailto:info@CellularOutfitter.com">info@CellularOutfitter.com</a>, or at the postal address below.
                            </p>
                            <p align="justify">
                                CellularOutfitter.com stores information that is collected through cookies to create a profile of our user. A cookie is a
                                piece of data stored on the user's computer tied to information about the user. CellularOutfitter.com uses session ID
                                cookies to create and identify user profiles. For the session ID cookie, once users close the browser, the cookie simply
                                terminates. By setting a cookie on our site, users would not have to log in a password more than once, thereby saving time
                                while shopping on our site. Only CellularOutfitter.com can access a cookie that we place. A profile is stored information
                                that we keep on individual users that is tied to the users personally identifiable information to provide offers and improve
                                the content of the site for the user. This profile is used to tailor a user&rsquo;s visit to our Web site, and to direct
                                pertinent marketing promotions to them. We DO NOT share your profile with other third parties. Some of our business partners
                                (e.g., advertisers) use cookies on our site.  We have no access to or control over these cookies. This privacy statement
                                covers the use of cookies by CellularOutfitter.com only and does not cover the use of cookies by any advertisers. This Web
                                site contains links to other sites that are not owned or controlled by CellularOutfitter.com. Please be aware that we,
                                CellularOutfitter.com, are not responsible for the privacy practices of such other sites.  
                            </p>
                            <p align="justify">
                                We encourage you to be aware when you leave our site and to read the privacy statements of each and every Web site that
                                collects personally identifiable information. This privacy statement applies only to information collected by this Web site.
                            </p>
                            <p align="justify">
                                CellularOutfitter.com Web pages contain web analytics beacons (called a &quot;single-pixel gif&quot; or &quot;Web beacon&quot;)
                                to compile anonymous, aggregated statistics about our Web site usage. Web analytics beacons are traffic and e-commerce analysis
                                services which enables us to better manage the merchandising on our site by informing us of what areas and product categories
                                are in demand by our customers. Web beacons are tiny graphics with a unique identifier, similar in function to cookies, and are
                                used to track the online movements of Web users. Similarly, the code gathers data through the use of a persistent cookie.
                                Cookies assist us in tracking which of our features you like best. And when visitors re-visit our site, cookies can enable us
                                to customize our content according to visitor preferences. However, both the Web beacons and cookies are not tied to users'
                                personally identifiable information. These cookies expire once you leave the CellularOutfitter.com site (including
                                CellularOutfitter.com third-party partner affiliate sites for cell phones and downloads).
                            </p>
                            <p align="justify">
                                The importance of security for all transactional information associated with our customers is of utmost concern to us. The
                                ordering information you provide - including credit card information - is secured using Secure Sockets Layer (SSL) encryption
                                technology via Yahoo! Store servers. Yahoo! Store uses SSL (Secure Socket Layer) encryption when transmitting certain kinds of
                                information, such as financial services information or payment information. An icon resembling a padlock is displayed on the
                                bottom of most browsers window during SSL transactions that involve credit cards and other forms of payment. Any time a Yahoo!
                                Store asks for a credit card number during checkout for payment, it will be SSL encrypted. The information you provide will be
                                stored securely on Yahoo!�s servers. This SSL technology is used to prevent such information from being intercepted and read
                                as it is transmitted over the Internet. The encrypted data goes to a secure site where your information is stored on
                                restricted-access computers located at restricted-access sites. While Yahoo! makes every effort to ensure the integrity and
                                security of its network and systems, they cannot guarantee that their security measures will prevent third-party &quot;hackers&quot;
                                from illegally obtaining this information.
                            </p>
                            <p align="justify">
                                We follow generally accepted industry standards to protect the personal information submitted to us, both during transmission
                                and once we receive it. No method of transmission over the Internet, or method of electronic storage, is 100% secure, however.
                                Therefore, while we strive to use commercially acceptable means to protect your personal information, we cannot guarantee its
                                absolute security. If you have any questions about security on our Web site, you can send email us at
                                <a href="mailto:webmaster@CellularOutfitter.com">webmaster@cellularoutfitter.com</a>. 
                            </p>
                            <p align="justify">
                                CellularOutfitter.com will remain committed to protecting your privacy online. We may change our Privacy Policy from time to
                                time. When such a change is made, we will post the revised Privacy policy on our website so please bookmark this page and check
                                back periodically. If, however, we are going to use users&rsquo; personally identifiable information in a manner materially
                                different from that stated at the time of collection we will notify users and customers via email and by posting an immediate
                                notice on our Web site for at least 30 days. Questions regarding the CellularOutfitter.com Privacy Policy should be directed to
                                <a href="mailto:info@CellularOutfitter.com">info@CellularOutfitter.com</a>.
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
<!--#include virtual="/includes/template/bottom.asp"-->
