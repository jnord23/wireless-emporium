<%
response.buffer = true
response.expires = -1
response.ExpiresAbsolute = Now() - 1
response.CacheControl = "no-cache"

'TEST CUSTOMER PROCESS IN CHECKOUT
response.Cookies("checkoutPage") = 0
response.Cookies("checkoutPage").expires = now

Session.Contents.Remove("nvpResArray")
Session.Contents.Remove("token")
Session.Contents.Remove("currencyCodeType")
Session.Contents.Remove("paymentAmount")
Session.Contents.Remove("PaymentType")
Session.Contents.Remove("PayerID")


SEtitle = "Cheap Cell Phone Accessories - Cellular Phone Accessories - CellularOutfitter.com"
SEdescription = "Buy cheap Cell Phone Accessories at Wholesale Prices to the Public.  Find the latest Discount Cellular Phone Accessories for Motorola, LG, Nokia, Samsung phones and more.  Cellular Accessories at the Lowest Prices Online Guaranteed."
SEkeywords = "cheap cell phone accessories, cell phone accessories, discount cellular phone accessories, cellular phone accessories"
pageTitle = "basket2"
%>

<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/cart/includes/inc_makeNum.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<!--#include virtual="/includes/asp/inc_getMySession.asp"-->

<%
secureURL = ""
if instr(request.ServerVariables("SERVER_NAME"), "www.cellularoutfitter.com") > 0 then secureURL = "https://www.cellularoutfitter.com"

htmBasketRows = ""
htmBasketRows2 = ""
nSubTotal = 0
EmptyBasket = false
gblShipoutDate = Date
shipoutCutoff = cdate(Date & " 03:00:00 PM")
if Now > shipoutCutoff then gblShipoutDate = gblShipoutDate+1

dim removePromo : removePromo = prepInt(request.QueryString("removePromo"))
if removePromo = 1 then session("promocode") = null

' START basket grab
dim sItemName, sRetailPrice, sItemPrice, sItemPic, nProdIdCheck, nProdQuantity, nProdIdCount, nProdTotalQuantity
dim sWeight, strItemCheck, strQty, FreeProductInCart
dim SquaretradeItemName, SquaretradeItemPrice, SquaretradeItemCondition
nProdTotalQuantity = cint(0)
sWeight = 0
strItemCheck = ""
strQty = ""
FreeProductInCart = 0
call fOpenConn()
sql = 	"SELECT e.alwaysInStock, c.brandName, d.modelName, (select top 1 inv_qty from we_items where partNumber = b.partNumber order by master desc, inv_qty desc) as inv_qty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, " & vbcrlf & _
		"B.itemDesc_CO, B.itemPic_CO, b.itemPic, B.price_CO, B.price_retail, B.Condition, B.itemWeight, B.NoDiscount, nullif(a.customcost, 0) customcost FROM ShoppingCart A INNER JOIN we_items B ON A.itemID = " & vbcrlf & _
		"B.itemID left join we_brands c on b.brandID = c.brandID left join we_models d on b.modelID = d.modelID left join we_pnDetails e on b.partNumber = e.partNumber WHERE A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)" & vbcrlf & _
		" union " & vbcrlf & _
		"SELECT cast('True' as bit) as alwaysInStock, '' as brandName, '' as modelName, 100 as inv_qty, A.itemID, A.qty, B.modelID, 3 as typeID, B.musicSkinsID, B.artist + ' ' + B.designName, B.image as itemPic_co, '' as itemPic, B.price_CO, " & vbcrlf & _
		"B.msrp as price_retail, 1 as Condition, 1 as itemWeight, 1 as NoDiscount, nullif(a.customcost, 0) customcost FROM ShoppingCart A INNER JOIN we_items_musicskins B ON A.itemID=B.id WHERE " & vbcrlf & _
		"A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
session("errorSQL") = SQL
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1
do until RS.eof
	if isnull(rs("alwaysInStock")) then alwaysInStock = False else alwaysInStock = rs("alwaysInStock")
	brandName = RS("brandName")
	modelName = RS("modelName")
	noDiscount = RS("nodiscount")
	nProdIdCheck = RS("itemID")
	nProdQuantity = RS("qty")
	nProdTotalQuantity = nProdTotalQuantity + RS("qty")
	nTypeID = prepInt(RS("typeID"))
	nProdPartNumber = RS("partNumber")
	partNumber = nProdPartNumber
	strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
	strQty = strQty & cStr(nProdQuantity)& ","  ' QTY string for each item, this is for buySafe Use
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		sItemName = insertDetails(RS("itemDesc_CO"))
		sItemPic = RS("itemPic_CO")
		baseItemPic = RS("itemPic")
		sItemPrice = RS("price_CO")
		if not isnull(rs("customCost")) then sItemPrice = rs("customCost")
		sRetailPrice = RS("price_retail")
		'Free Bluetooth
		if FreeBluetooth = 1 then
			if cStr(nProdIdCheck) = FreeBluetoothItemID then
				FreeBluetoothPrice = sItemPrice
			end if
		end if
		'Free Product With Phone Purchase
		'if nTypeID = 16 then
		'	PhonePurchased = 1
		'	SquaretradeItemName = sItemName
		'	SquaretradeItemPrice = sItemPrice
		'	SquaretradeItemCondition = RS("Condition")
		'end if
		'Category Sale
		if CategorySale = 1 then
			if nTypeID = CategorySaleTypeID and RS("NoDiscount") = 0 then
				CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
			else
				SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
				SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_CO > 0)"
				SQL = SQL & " AND (R.typeid = '" & CategorySaleTypeID & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
				SQL = SQL & " AND I.NoDiscount = 0"
				dim RSrelated
				set RSrelated = Server.CreateObject("ADODB.Recordset")
				RSrelated.open SQL, oConn, 0, 1
				if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				RSrelated.close
				set RSrelated = nothing
			end if
		end if
		
		'Free Product Offer code added 2/1/2010 by MC
		if inStr(RS("PartNumber"),"scr-") > 0 then FreeProductInCart = 1
		'session("errorSQL") = "nItemTotal = " & nItemTotal & " | nProdIdCheck = " & nProdIdCheck & " | sItemPrice = " & sItemPrice & " | nProdQuantity = " & nProdQuantity
		
		if instr(RS("PartNumber"),"PHN-") < 1 and RS("nodiscount") = 0 and ucase(left(RS("PartNumber"), 2)) <> "BT" then 
			specialAmt = specialAmt + (sItemPrice * nProdQuantity)
		end if
		
		if rs("inv_qty") > 0 or alwaysInStock then
			nSubTotal = nSubTotal + (sItemPrice * nProdQuantity)
			nItemTotal = nItemTotal + (sItemPrice * nProdQuantity)
			sWeight = sWeight + (cDbl(RS("itemWeight")) * nProdQuantity)
			if nProdIdCheck => 1000000 then
				sql = "select preferredImg, image, defaultImg from we_items_musicSkins where id = " & nProdIdCheck
				session("errorSQL") = sql
				set rs = oConn.execute(sql)
				if not rs.EOF then
					preferredImg = rs("preferredImg")
					itempic_CO = rs("image")
					defaultImg = rs("defaultImg")
					if isnull(preferredImg) then																					
						if hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsLarge", itempic_CO) then
							sql = "update we_items_musicSkins set preferredImg = 1 where id = " & itemID
							session("errorSQL") = sql
							oConn.execute(sql)
							useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
						elseif hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault", defaultImg) then
							sql = "update we_items_musicSkins set preferredImg = 2 where id = " & itemID
							session("errorSQL") = sql
							oConn.execute(sql)
							useImg = "http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault/" & defaultImg
						else
							useImg = "/productPics/thumb/imagena.jpg"
						end if
					elseif preferredImg = 1 then
						useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
					elseif preferredImg = 2 then
						useImg = "/productpics/musicSkins/musicSkinsDefault/" & defaultImg
					else
						useImg = "/productPics/thumb/imagena.jpg"
					end if
				end if
			else
				if instr(nProdPartNumber,"DEC-SKN") > 0 then
					useImg = "/productpics/decalSkins/thumb/" & baseItemPic
				else
'					useImg = "/productpics/icon/" & sItemPic
					useImg = "/productpics/thumb/" & sItemPic
				end if
			end if
'			htmBasketRows = htmBasketRows & fWriteBasketRow(nProdIdCheck,sItemName,sItemPrice,cStr(nProdQuantity),sRetailPrice,useImg)
			htmBasketRows2 = htmBasketRows2 & fWriteBasketRow2(nProdIdCheck,sItemName,sItemPrice,cStr(nProdQuantity),sRetailPrice,useImg,nTypeID)			
			
			WEhtml2 = WEhtml2 & "<input type=""hidden"" name=""itemID_" & nProdIdCount & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""itemQty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
			PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & sItemName & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & nProdQuantity & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & sItemPrice & """>" & vbcrlf
		else
			sql = "delete from ShoppingCart where itemID = " & nProdIdCheck & " and (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
			session("errorSQL") = sql
			oConn.execute SQL
			htmBasketRows = htmBasketRows & fWriteErrorBasketRow(nProdIdCheck,sItemName,sItemPrice,cStr(nProdQuantity),sRetailPrice,sItemPic)
		end if
	end if
	RS.movenext
loop
RS.close
set RS = nothing
call fCloseConn()
' END basket grab

'GET COUPON VALUES
dim sPromoCode
if request.form("promo") <> "" then
	sPromoCode = SQLquote(request.form("promo"))
else
	sPromoCode = session("promocode")
end if

'sPromoCode = ""
'session("promocode") = ""

if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	call fOpenConn()
	SQL = "SELECT * FROM CO_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc, FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		'FreeItemPartNumber = RS("FreeItemPartNumber")
	else
		sPromoCode = ""
	end if
	call fCloseConn()
end if
' END GET COUPON VALUES

if sWeight / 16 < 1 then
	sWeight = 1
else
	sWeight = sWeight / 16
end if

if strItemCheck <> "" then strItemCheck = left(strItemCheck,len(strItemCheck)-1)

if nProdIdCount = 0 then
	htmBasketRows = htmBasketRows & "<tr><td colspan=""6""><i><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Basket is currently empty.<br><br></i></td></tr>" & vbcrlf
	htmBasketRows2 = htmBasketRows2 & "<div style=""width:100%; float:left; padding:25px 0px 25px 0px; text-align:center;"">Basket is currently empty.</div>" & vbcrlf	
	EmptyBasket = true
end if
%>

<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/includes/template/top_cart.asp"-->

<td width="100%" align="center" valign="top">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
        <%
        if len(session("userMsg")) > 0 and not isnull(session("userMsg")) then
        %>
        <tr><td align="center" style="font-weight:bold; color:#C30; font-size:14px; padding:10px 0px 10px 0px;"><%=session("userMsg")%></td></tr>
        <%
            session("userMsg") = null
        end if
        %>
        <tr>
            <td width="100%" valign="top" align="center" style="padding-top:15px;">
                <img src="/images/cart/110-banner.jpg" border="0" width="980" height="78" />
            </td>
        </tr>
        <tr>
            <td width="100%" valign="top" align="center" style="border-top:1px dotted #000; padding:10px 0px 5px 0px;">
				<img src="/images/cart/step-1.jpg" />
            </td>
        </tr>
        <tr>
        	<td width="100%" valign="top" align="center" id="mvt_cartNew" style="padding-bottom:10px;">
            	<div style="float:left; width:100%; background:url(/images/cart/headerbar-blue.jpg) repeat-x; height:23px; font-size:12px; color:#fff; padding-top:5px; border-top-left-radius:5px; border-top-right-radius:5px;">
                	<div style="float:left; width:120px; text-align:center; font-weight:bold;">Item</div>
                	<div style="float:left; width:380px; text-align:left; font-weight:bold;">Description</div>
                	<div style="float:left; width:120px; text-align:center; font-weight:bold;">Retail Pricing</div>
                	<div style="float:left; width:120px; text-align:center; font-weight:bold;">Your Pricing</div>
                	<div style="float:left; width:120px; text-align:center; font-weight:bold;">Quantity</div>
                	<div style="float:left; width:120px; text-align:center; font-weight:bold;">Subtotal</div>
                </div>
                <div style="float:left; width:978px; border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc;">
				<%=htmBasketRows2%>
				<%
				if not EmptyBasket then
				    if isNumeric(nSubTotal) then
                        strItemTotal = formatCurrency(nSubTotal)
                    else
                        strItemTotal = formatCurrency(0)
                    end if
					
					if sPromoCode <> "" then
                        session("promocode") = sPromoCode
                        strItemTotal = formatCurrency(nSubTotal)					
					%>
                    <div style="width:978px; float:left; padding:5px;">
                        <div style="width:968px; float:left; border-top:1px solid #ccc; padding:20px 0px 20px 0px;">
                            <div style="float:left; width:700px; text-align:left; padding-left:20px; color:#333;">
                                <b>Promotions:&nbsp;</b><%=couponDesc%>&nbsp;<a href="/cart/basket.asp?removePromo=1" style="color:#F00; font-size:11px;">(Remove coupon)</a>
                            </div>
                            <div style="float:left; width:228px; padding-right:20px; text-align:right;">
                            	<div style="color:#666666; font-weight:bold;">Discounted Subtotal</div>
                                <div style="font-size:20px; font-weight:bold;"><%=strItemTotal%></div>
                            </div>
                        </div>
                    </div>
	                <%
					end if
                end if
				%>
                </div>
                
				<%if not EmptyBasket then%>
                <div style="float:left; width:978px; border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc; border-bottom-left-radius:5px; border-bottom-right-radius:5px; background-color:#f2f2f2;">
                	<div style="float:left; width:100%; height:0px; border-top:1px solid #fff;"></div>
                    <div style="float:left; height:30px; padding:25px 10px 10px 10px;">
						<div style="float:left; width:600px;">
                            <form action="/cart/basket.asp" method="post" style="margin:0px;">
                        	<div id="promoCodeBtn2" style="float:left; font-size:10px; cursor:pointer; color:#666; text-align:left; padding-top:6px;" onclick="showPromo2();">Enter Promo Code</div>
                        	<div id="promoCode2" style="float:left; display:none;">
                            	<div style="float:left; padding-top:6px;" class="cart-text">Enter Promotion code here (if applicable):</div>
                                <div style="float:left; padding:3px 0px 0px 5px;"><input type="text" name="promo" value="<%=sPromoCode%>"></div>
                                <div style="float:left; padding:0px 0px 0px 5px;"><input type="image" onClick="this.submit();" src="/images/cart/button_submit.jpg" width="84" height="27" border="0" alt="Submit" align="absbottom"></div>
							</div>
                            </form>
                        </div>
						<div id="mvt_shipoutDate" style="float:left; width:358px; font-size:13px; font-weight:bold; text-align:right;"><span style="font-size:14px;">Ship Out Date:&nbsp;&nbsp;</span><%=gblShipoutDate%></div>
                    </div>
                </div>
                <div style="float:left; width:100%; padding:10px 0px 10px 0px; margin-bottom:10px; border-bottom:1px solid #ccc;">
                	<div style="float:left;">
                        <div style="float:left; padding:10px 15px 0px 0px;"><a href="/privacypolicy.html" target="_blank" style="text-decoration:none;"><img src="/images/cart/truste.jpg" border="0" width="141" height="45" /></a></div>
                        <div style="float:left; padding:9px 15px 0px 0px;"><a href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=100097908&source=ctc" target="_blank" style="text-decoration:none;"><img src="/images/cart/bbb.jpg" border="0" width="114" height="46" /></a></div>
                        <div style="float:left; padding:0px 15px 0px 0px;"><img src="/images/cart/google.jpg" border="0" width="62" height="63" /></div>
                        <div style="float:left; padding:0px 15px 0px 0px;"><img src="/images/cart/5000.jpg" border="0" width="84" height="61" /></div>                    
                    </div>
                    <div style="float:right; padding:5px 10px 0px 0px;">
						<%if discountTotal > 0.0 then%>
                        <div style="font-size: 20px; font-weight: bold; color:#900; text-align:right;">Discount:&nbsp;&nbsp;<%=formatcurrency(discountTotal)%></div>
                        <div style="font-size: 22px; font-weight: bold; text-align:right;">Item Total:&nbsp;&nbsp;<%=strItemTotal%></div>
                        <%else%>
                        <div style="padding-top:15px; font-size: 22px; font-weight: bold; text-align:right;">Item Total:&nbsp;&nbsp;<%=strItemTotal%></div>
                        <%end if%>
                    </div>
                </div>
                <div style="float:left; width:100%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td valign="top" align="left">
                                <table border="0" cellpadding="5" cellspacing="0" width="100%">
                                    <tr>
                                        <td valign="top" align="left">
                                            <img src="/images/cart/happy-customer-banner.jpg" border="0" width="192" height="51" />
                                        </td>
                                        <td valign="top" align="left">
                                            <div>
                                                <div style="float:left; padding-right:5px;"><img src="/images/cart/green-check.jpg" border="0" width="16" height="16" /></div>
                                                <div style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#666;">SSL Encrypted For Safe & Secure Transactions</div>
                                            </div>
                                            <div>
                                                <div style="float:left; padding-right:5px;"><img src="/images/cart/green-check.jpg" border="0" width="16" height="16" /></div>
                                                <div style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#666;">All Orders Ship Within 1 Business Day</div>
                                            </div>
                                            <div>
                                                <div style="float:left; padding-right:5px;"><img src="/images/cart/green-check.jpg" border="0" width="16" height="16" /></div>
                                                <div style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#666;">Unconditional 1-Year Warranty</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="left" colspan="2">
                                            <a href="/"><img src="/images/cart/button-continue-shopping.jpg" border="0" width="186" height="34" alt="continue shopping" title="continue shopping" /></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" align="right">
                                <table border="0" cellpadding="5" cellspacing="0" width="100%">
                                    <tr>
                                        <td valign="top" align="right">
                                            <form name="CheckoutForm" method="post" action="<%=secureURL%>/cart/checkout.asp">
                                                <input type="hidden" name="promo" value="<%=sPromoCode%>">
                                                <input type="hidden" name="sWeight" value="<%=cStr(sWeight)%>">
                                                <input type="hidden" name="strItemCheck" value="<%=strItemCheck%>">
                                                <input type="hidden" name="shipZip" value="">
                                                <input type="hidden" name="buysafeamount" value="0">
                                                <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
                                                <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
                                                <%=WEhtml%>
                                                <!--<input type="image" name="cartContinue" onClick="return CheckSubmit('CO');" src="/images/cart/checkout-button.jpg" width="389" height="57" border="0" alt="Continue Checkout">-->
                                                <input type="image" name="cartContinue" src="/images/cart/checkout-button.jpg" width="389" height="57" border="0" alt="Continue Checkout">
                                            </form>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="right">
                                            <div style="float:right; margin-left:15px; width:110px; height:11px; font-size:1px; border-bottom:1px solid #ccc;">&nbsp;</div>
                                            <div style="float:right; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; color:#333;">Or Checkout With</div>
                                            <div style="float:right; margin-right:15px; width:110px; height:11px; font-size:1px; border-bottom:1px solid #ccc;">&nbsp;</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="right" style="padding-top:10px;">
                                            <form name="GoogleForm" method="POST" action="<%=secureURL%>/cart/process/google/CartProcessing.asp">
                                                <!--<input type="image" name="Checkout" onClick="return CheckSubmit(this.form,'GC');" alt="Google Checkout" src="https://checkout.google.com/buttons/checkout.gif?merchant_id=468324454609889&w=168&h=44&style=white&variant=text&loc=en_US" height="44" width="168">-->
												<input type="image" name="Checkout" alt="Google Checkout" src="https://checkout.google.com/buttons/checkout.gif?merchant_id=468324454609889&w=168&h=44&style=white&variant=text&loc=en_US" height="44" width="168">
                                                <input type="hidden" name="promocode" value="<%=sPromoCode%>">
                                                <input type="hidden" name="analyticsdata" value="">
                                                <input type="hidden" name="shipZip" value="">
                                                <input type="hidden" name="sWeight" value="<%=cStr(sWeight)%>">
                                                <input type="hidden" name="buysafeamount" value="0">
                                                <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
                                                <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
                                                <%=WEhtml%>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="right" style="padding-top:10px;">
                                            <form name="PaypalForm" action="<%=secureURL%>/cart/process/PaypalSOAP/ReviewOrder.asp" method="post">
                                                <% if session("stmFreeItem") = 1 then nProdIdCount = nProdIdCount + 1 %>
                                                <input type="hidden" name="promo" value="<%=sPromoCode%>">
                                                <input type="hidden" name="ItemsAndWeight" value="<%=cStr(nProdIdCount) & "|" & cStr(sWeight)%>">
                                                <input type="hidden" name="paymentAmount" value="<%=nSubTotal%>">
                                                <input type="hidden" name="shipZip" value="">
                                                <input type="hidden" name="buysafeamount" value="0">
                                                <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
                                                <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
                                                <% if session("stmFreeItem") = 1 then PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & stm_freeItem & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & stm_freeItemDesc & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""1""><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""0"">" & vbcrlf %>
                                                <%=PPhtml%>
                                                <!--<input type="image" name="cartPaypal" onClick="return CheckSubmit(this.form,'PP');" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif">-->
												<input type="image" name="cartPaypal" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif">
                                            </form>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <%end if%>
            </td>
        </tr>
    </table>
</td>
<!--#include virtual="/includes/template/bottom_cart.asp"-->
<%
function fWriteBasketRow2(nProdId,sItemName,sItemPrice,nQty,sRetailPrice,sitempic_CO,sTypeID)
	categoryDesc = ""
	select case sTypeID
		case 3 
			categoryDesc = 	"<div>" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Protects your pricey device!</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Shields your phone from scratches, dings and bumps</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Crafted to perfectly fit your phone</div>" & vbcrlf & _
							"</div>" & vbcrlf
		case 7 
			categoryDesc = 	"<div>" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Classy, timeless design</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Provides heavy duty protection</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Designed to perfectly fit your phone</div>" & vbcrlf & _
							"</div>" & vbcrlf
		case 5 
			categoryDesc = 	"<div>" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Extend the functionality of your device</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Hands-free operation for maximum convenience</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">90 Day Worry Free Return Policy</div>" & vbcrlf & _
							"</div>" & vbcrlf
		case 2 
			categoryDesc = 	"<div>" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Keep your phone powered up while on the go</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Built in over charge protection in all devices</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Save up to 80% off MSRP on our high quality items</div>" & vbcrlf & _
							"</div>" & vbcrlf
		case 1 
			categoryDesc = 	"<div>" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Guaranteed to work upon delivery</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Extend the life of your mobile device</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Long lasting & 1 Year Warranty</div>" & vbcrlf & _
							"</div>" & vbcrlf
		case 6 
			categoryDesc = 	"<div>" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Keep your phone handy at all times</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">High quality construction ensures lasting use</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">1 Year Warranty on all items</div>" & vbcrlf & _
							"</div>" & vbcrlf
		case 18 
			categoryDesc = 	"<div>" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Easy, bubble-free application</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Protects your screen from scratches and other damage</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Retains full touch screen responsiveness</div>" & vbcrlf & _
							"</div>" & vbcrlf
		case 19,20 
			categoryDesc = 	"<div>" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Cut to perfectly fit your phone</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">High quality 3M material</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Ultra-thin yet durable material</div>" & vbcrlf & _
							"</div>" & vbcrlf
		case 8
			categoryDesc = 	"<div>" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Extend the functionality of your device</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">90 Day Worry Free Return Policy</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">1 Year Warranty on all items</div>" & vbcrlf & _
							"</div>" & vbcrlf
		case 16
			categoryDesc = 	"<div>" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Save up to 80% retail prices</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Unlocked phones provide worldwide use</div>" & vbcrlf & _
							"</div>" & vbcrlf & _
							"<div style=""clear:both;"">" & vbcrlf & _
							"	<div style=""float:left; padding-right:5px;""><img src=""/images/cart/green-check.jpg"" border=""0"" width=""16"" height=""16"" /></div>" & vbcrlf & _
							"	<div style=""float:left;"">Personalized technical support included</div>" & vbcrlf & _
							"</div>" & vbcrlf
	end select
							
	strTemp = 	"<div style=""width:978px; float:left; padding:5px;"">" & vbcrlf & _
				"	<div style=""width:968px; float:left; border-top:1px solid #ccc; padding-top:10px;"">" & vbcrlf & _
				"		<div style=""float:left; width:100px; padding:0px 10px 10px 10px;"">" & vbcrlf & _
				"			<a href=""/p-" & nProdIdCheck & "-" & formatSEO(sItemName) & ".html""><img src=""" & sitempic_CO & """ border=""0"" alt=""" & sItemName & """ width=""100"" height=""100"" /></a>" & vbcrlf & _
				"		</div>" & vbcrlf & _
				"		<div style=""float:left; width:338px; padding:0px 20px 0px 10px; text-align:left;"">" & vbcrlf & _
				"			<div style=""padding-bottom:10px;""><a style=""font-size:14px; color:#333; font-weight:bold;"" href=""/p-" & nProdIdCheck & "-" & formatSEO(sItemName) & ".html"">" & sItemName & "</a></div>" & vbcrlf & _
				"			<div style=""font-size:12px; color:#999; font-weight:normal;"" class=""mvt_cartProdDetail"">" & categoryDesc & "</div>" & vbcrlf & _
				"		</div>" & vbcrlf & _
				"		<div style=""float:left; width:120px; text-align:center; font-size:12px;""><s>" & formatCurrency(cDbl(sRetailPrice)) & "</s></div>" & vbcrlf & _
				"		<div style=""float:left; width:120px; text-align:center; font-size:12px;"">" & formatCurrency(cDbl(sItemPrice)) & "</div>" & vbcrlf & _
				"		<div style=""float:left; width:120px; text-align:center;"">" & vbcrlf & _
				"			<form name=""frmEditDelete_" & nProdId & """ action=""/cart/item_edit.asp"" method=""post"" style=""margin:0px;"">" & vbcrlf & _
				"				<input type=""hidden"" name=""prodid"" value=""" & nProdId & """>" & vbcrlf & _
				"				<input type=""hidden"" name=""pagetype"" value=""edit"">" & vbcrlf & _
				"				<input type=""text"" name=""qty"" size=""1"" maxlength=""4"" value=""" & nQty & """><br />" & vbcrlf & _
				"				<a style=""font-size:12px; color:#2896d1; font-weight:normal;"" href=""javascript:EditDeleteItem('frmEditDelete_" & nProdId & "','edit');""><b>Update</b></a> | " & vbcrlf & _
				"				<a style=""font-size:12px; color:#2896d1; font-weight:normal;"" href=""javascript:EditDeleteItem('frmEditDelete_" & nProdId & "','delete');""><b>Delete</b></a>" & vbcrlf & _
				"			</form>" & vbcrlf & _
				"		</div>" & vbcrlf & _
				"		<div style=""float:left; width:120px; text-align:center; font-weight:bold;"">" & formatCurrency((cDbl(sItemPrice) * cDbl(nQty))) & "</div>" & vbcrlf & _
				"	</div>" & vbcrlf & _
				"</div>" & vbcrlf
	
	fWriteBasketRow2 = strTemp
end function

function fWriteBasketRow(nProdId,sItemName,sItemPrice,nQty,sRetailPrice,sitempic_CO)
	'this will write out the row for the basket
	dim sTemp
	sTemp	=	"<form name=""frmEditDelete" & nProdId & """ action=""/cart/item_edit.asp"" method=""post"" style=""margin:0px;"">" & vbcrlf & _
				"	<tr height=""70px"">" & vbcrlf & _
				"		<td width=""75"" align=""center"" style=""border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;""><a href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html""><img src=""" & sitempic_CO & """ width=""45"" height=""45"" border=""0""></a></td>" & vbcrlf & _
				"		<td align=""left"" valign=""middle"" style=""padding-left:10px; border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;""><a class=""product-cart"" href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html"">" & sItemName & "</a></td>" & vbcrlf & _
				"		<td align=""center"" valign=""middle"" style=""font-size:13px; font-weight:bold; border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;""><s>" & formatCurrency(cDbl(sRetailPrice)) & "</s></td>" & vbcrlf & _
				"		<td align=""center"" valign=""middle"" class=""red-price-small"" style=""border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;"">" & formatCurrency(cDbl(sItemPrice)) & "</td>" & vbcrlf & _
				"		<td align=""center"" valign=""middle"" style=""border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;"">" & vbcrlf & _
				"			<input type=""hidden"" name=""prodid"" value=""" & nProdId & """>" & vbcrlf & _
				"			<input type=""hidden"" name=""pagetype"" value=""edit"">" & vbcrlf & _
				"			<input type=""text"" name=""qty"" size=""1"" maxlength=""4"" value=""" & nQty & """><br />" & vbcrlf & _
				"			<a style=""font-size:12px;color:#000000; font-weight:normal;"" href=""javascript:EditDeleteItem('frmEditDelete" & nProdId & "','edit');""><b>Update</b></a> | " & vbcrlf & _
				"			<a style=""font-size:12px;color:#000000; font-weight:normal;"" href=""javascript:EditDeleteItem('frmEditDelete" & nProdId & "','delete');""><b>Delete</b></a>" & vbcrlf & _
				"		</td>" & vbcrlf & _
				"		<td align=""center"" valign=""middle"" style=""font-size:13px; font-weight:bold; border-bottom:1px solid #eaeaea;"">" & formatCurrency((cDbl(sItemPrice) * cDbl(nQty))) & "</td>" & vbcrlf & _
				"	</tr>" & vbcrlf & _
				"</form>" & vbcrlf
	
	fWriteBasketRow = sTemp
end function

function fWriteErrorBasketRow(nProdId,sItemName,sItemPrice,nQty,sRetailPrice,sitempic_CO)
	'this will write out the row for the basket
	dim sErrorTemp
	
	sErrorTemp	=	"<form name=""frmEditDelete" & nProdId & """ action=""/cart/item_edit.asp"" method=""post"" style=""margin:0px;"">" & vbcrlf & _
					"	<tr height=""70px"">" & vbcrlf & _
					"		<td width=""75"" align=""center""><a href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html""><img src=""/productpics/icon/" & sitempic_CO & """ width=""45"" height=""45"" border=""0""></a></td>" & vbcrlf & _
					"		<td align=""left"" valign=""middle"" style=""padding-left:10px;""><a class=""product-cart"" href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html"">" & sItemName & "</a></td>" & vbcrlf & _
					"		<td align=""center"" valign=""middle"" style=""font-size:13px; font-weight:bold;""><s>" & formatCurrency(cDbl(sRetailPrice)) & "</s></td>" & vbcrlf & _
					"		<td align=""center"" valign=""middle"" class=""red-price-small"">" & formatCurrency(cDbl(sItemPrice)) & "</td>" & vbcrlf & _
					"		<td align=""center"" valign=""middle"" style=""font-size:13px; font-weight:bold;"" colspan=""2"">This product is currently unavailable</td>" & vbcrlf & _
					"	</tr>" & vbcrlf & _
					"</form>" & vbcrlf
					
	fWriteErrorBasketRow = sErrorTemp
end function

function hasImageOnRemote(pRemoteURL, pFileName)
	dim isExists : isExists = false
	dim oXMLHTTP : set oXMLHTTP = server.CreateObject("Msxml2.ServerXMLHTTP")
	dim strTemp : strTemp = pRemoteURL & "/" & pFileName
	
	oXMLHTTP.Open "HEAD", strTemp, False	'communicating without responseBody, only looks up file header
	oXMLHTTP.Send
	
	if (oXMLHTTP.readyState = "4") and (oXMLHTTP.status = "200") and (instr(oXMLHTTP.getResponseHeader("Content-Type"), "image") > 0) then
		isExists = true
	end if

	set oXMLHTTP = Nothing
	
	hasImageOnRemote = isExists
	
end function
%>

<script language="JavaScript">
function EditDeleteItem(nForm,thisAction) {
	var frmName = eval('document.' + nForm);
	if (isNaN(frmName.qty.value)) {
		alert("Please enter a valid number for Quantity.");
	} else {
		if (thisAction == 'delete') {
			frmName.action = "/cart/item_delete.asp";
			//frmName.action = "http://staging.cellularoutfitter.com/cart/item_delete.asp";
		} else {
			frmName.action = "/cart/item_edit.asp";
			//frmName.action = "http://staging.cellularoutfitter.com/cart/item_edit.asp";
		}
		frmName.pagetype.value = thisAction;
		frmName.submit();
	}
}
</script>

<script language="javascript">
	function showPromo2() {
		document.getElementById("promoCodeBtn2").style.display = 'none'
		document.getElementById("promoCode2").style.display = ''
	}
	
	function showPromo() {
		document.getElementById("promoCodeBtn").style.display = 'none'
		document.getElementById("promoCode").style.display = ''
	}
	
function CheckSubmit(pymt) {
	/*
	bValid = true;
	CheckValidNEW(document.shippingZip.shipZip.value, "Your Shipping Zip is a required field!");
	if (bValid) {
		if (pymt == 'PP') {
			document.PaypalForm.shipZip.value = document.shippingZip.shipZip.value;
			document.PaypalForm.action = '/cart/process/PaypalSOAP/ReviewOrder.asp';
			//document.PaypalForm.action = '/cart/process/PaypalSOAP/ReviewOrder.asp';
			document.PaypalForm.submit();
		} else if (pymt == 'GC') {
			document.GoogleForm.shipZip.value = document.shippingZip.shipZip.value;
			document.GoogleForm.submit();
		} else {
			<%
'			if crossSell <> 0 then
'				if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
'					response.write "document.CheckoutForm.action = '/cart/checkout.asp';"
'				else
'					response.write "document.CheckoutForm.action = 'https://www.cellularoutfitter.com/cart/checkout.asp';"
'				end if
'			else
'				if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then
'					response.write "document.CheckoutForm.action = '/cart/checkout.asp';"
'				else
'					response.write "document.CheckoutForm.action = 'https://www.cellularoutfitter.com/cart/checkout.asp';"
'				end if
'			end if
			%>
			document.CheckoutForm.shipZip.value = document.shippingZip.shipZip.value;
			document.CheckoutForm.submit();
		}
	}
	return false;
	*/
}
</script>
<!-- <%=mySession%> -->