<%
dim useHttps : useHttps = 1
response.buffer = true
SEtitle = "Cheap Cell Phone Accessories - Cellular Phone Accessories - CellularOutfitter.com"
SEdescription = "Buy cheap Cell Phone Accessories at Wholesale Prices to the Public.  Find the latest Discount Cellular Phone Accessories for Motorola, LG, Nokia, Samsung phones and more.  Cellular Accessories at the Lowest Prices Online Guaranteed."
SEkeywords = "cheap cell phone accessories, cell phone accessories, discount cellular phone accessories, cellular phone accessories"
pageTitle = "checkout"
mvtName = "checkout"

dim szip, sPromoCode, sWeight
szip = request.form("shipZip")
'sWeight = request.form("sWeight")

session("szip") = szip
'session("sWeight") = sWeight
%>

<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/cart/includes/inc_makeNum.asp"-->
<!--#include virtual="/cart/includes/inc_getStates.asp"-->
<!--#include virtual="/includes/asp/inc_getMySession.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<%
HTTP_REFERER = lcase(request.servervariables("HTTP_REFERER"))

'=============== temporarily disabled upon Ed's request for consulting
'if instr(HTTP_REFERER,".cellularoutfitter.com/cart/basket") < 1 and instr(HTTP_REFERER,"cellularoutfitter.com/cart/checkout") < 1 and instr(HTTP_REFERER,"cellularoutfitter.com/cart/process/ebillme/") < 1 then
'	response.write "<h3>This page will only accept forms submitted from the Cellular Outfitter secure website.</h3>" & vbcrlf
'	useBody = "HTTP_REFERER:" & HTTP_REFERER & "<br />"
'	useBody = useBody & "SERVER_NAME:" & request.ServerVariables("SERVER_NAME") & "<br />"
'	useBody = useBody & "URL:" & request.ServerVariables("URL") & "<br />"
'	useBody = useBody & "HTTP_USER_AGENT:" & request.ServerVariables("HTTP_USER_AGENT") & "<br />"
'	useBody = useBody & "HTTPS:" & request.ServerVariables("HTTPS") & "<br />"
'	useBody = useBody & "QUERY_STRING:" & request.ServerVariables("QUERY_STRING") & "<br />"
'	'weEmail "jon@wirelessemporium.com,ruben@wirelessemporium.com","webmaster@cellularoutfitter.com","Checkout Error",useBody
'	response.end
'end if

dim buysafeamount, WantsBondField, ShoppingCartId
if request.form("buysafeamount") = "" then
	buysafeamount = 0
	WantsBondField = "false"
else
	buysafeamount = cDbl(request.form("buysafeamount"))
	WantsBondField = request.form("WantsBondField")
end if
ShoppingCartId = request.form("buysafecartID")

nCATax = 0
if request.form("submitted") = "submitted" then
	newEmail = request.form("newEmail")
	pword1 = request.form("pword1")
	pword2 = request.form("pword2")
	fname = request.form("fname")
	lname = request.form("lname")
	email = request.form("email")
	phone = request.form("phone")
	saddress1 = request.form("saddress1")
	saddress2 = request.form("saddress2")
	scity = request.form("scity")
	sstate = request.form("sstate")
	szip = request.form("szip")
	bAddress1 = request.form("bAddress1")
	bAddress2 = request.form("bAddress2")
	bCity = request.form("bCity")
	bState = request.form("bState")
	bZip = request.form("bZip")
	shiptype = request.form("shiptype")
	PaymentType = request.form("PaymentType")
	cc_cardType = request.form("cc_cardType")
	cc_cardOwner = request.form("cc_cardOwner")
	cardNum = request.form("cardNum")
	cc_month = request.form("cc_month")
	cc_year = request.form("cc_year")
end if

if request.form("promo") <> "" then
	sPromoCode = request.form("promo")
else
	sPromoCode = session("promocode")
end if

if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	call fOpenConn()
	SQL = "SELECT * FROM CO_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc
		'FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		'FreeItemPartNumber = RS("FreeItemPartNumber")
	else
		sPromoCode = ""
	end if
	call fCloseConn()
end if

'Start basket grab
dim htmBasketRows, WEhtml, EBtemp, EBxml
htmBasketRows = ""
WEhtml = ""
EBtemp = ""
EBxml = ""

dim sItemName, sRetailPrice, sItemPrice, sItemPic, nProdIdCheck, nProdQuantity, nProdIdCount, nTotalQuantity
dim strItemCheck, strQty, FreeProductInCart
dim SquaretradeItemName, SquaretradeItemPrice, SquaretradeItemCondition

nProdIdCount = 0
nTotalQuantity = 0
shippingQty = 0
strItemCheck = ""
strQty = ""
FreeProductInCart = 0
specialAmt = 0

call fOpenConn()
' New cart abandonment tracking added 9/20/2010 by MC
SQL = "UPDATE ShoppingCart SET DateCheckout = '" & now & "' WHERE store = 2 AND sessionID = '" & mySession & "' AND (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
oConn.execute SQL

sql = 	"SELECT c.brandName, d.modelName, (select top 1 inv_qty from we_items where partNumber = b.partNumber order by master desc, inv_qty desc) as inv_qty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, " & vbcrlf & _
		"B.itemDesc_CO, B.itemPic_CO, B.price_CO, B.price_retail, B.Condition, isnull(B.itemWeight, 0) itemWeight, B.NoDiscount, nullif(a.customcost, 0) customcost FROM ShoppingCart A INNER JOIN we_items B ON A.itemID = " & vbcrlf & _
		"B.itemID left join we_brands c on b.brandID = c.brandID left join we_models d on b.modelID = d.modelID WHERE A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)" & vbcrlf & _
		" union " & vbcrlf & _
		"SELECT '' as brandName, '' as modelName, 100 as inv_qty, A.itemID, A.qty, B.modelID, 3 as typeID, B.musicSkinsID, B.artist + ' ' + B.designName, B.image as itemPic_co, B.price_CO, " & vbcrlf & _
		"B.msrp as price_retail, 1 as Condition, 1 as itemWeight, 1 as NoDiscount, nullif(a.customcost, 0) customcost FROM ShoppingCart A INNER JOIN we_items_musicskins B ON A.itemID=B.id WHERE " & vbcrlf & _
		"A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"
'response.write "<pre>" & sql & "</pre>"
set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

do until RS.eof
	brandName = RS("brandName")
	modelName = RS("modelName")
	partNumber = RS("partNumber")
	nProdIdCheck = RS("itemID")
	nProdQuantity = RS("qty")
	strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
	strQty = strQty & cStr(nProdQuantity)& ","  ' QTY string for each item, this is for buySafe Use
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		sWeight = cdbl(sWeight) + cdbl(rs("itemWeight"))
		sItemName = insertDetails(RS("itemDesc_CO"))
		sItemPic = RS("itemPic_CO")
		sItemPrice = RS("price_CO")
		if not isnull(rs("customCost")) then sItemPrice = rs("customCost")
		sRetailPrice = RS("price_retail")
		'Free Bluetooth
		if FreeBluetooth = 1 then
			if cStr(nProdIdCheck) = FreeBluetoothItemID then
				FreeBluetoothPrice = sItemPrice
			end if
		end if
		'Free Product With Phone Purchase
		'if RS("typeID") = 16 then
		'	PhonePurchased = 1
		'	SquaretradeItemName = sItemName
		'	SquaretradeItemPrice = sItemPrice
		'	SquaretradeItemCondition = RS("Condition")
		'end if
		'Category Sale
		if CategorySale = 1 then
			if RS("typeID") = CategorySaleTypeID and RS("NoDiscount") = 0 then
				CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
			else
				SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
				SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_CO > 0)"
				SQL = SQL & " AND (R.typeid = '" & CategorySaleTypeID & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
				SQL = SQL & " AND I.NoDiscount = 0"
				dim RSrelated
				set RSrelated = Server.CreateObject("ADODB.Recordset")
				RSrelated.open SQL, oConn, 0, 1
				if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				RSrelated.close
				set RSrelated = nothing
			end if
		end if
		
		'Free Product Offer code added 2/1/2010 by MC
		if inStr(RS("PartNumber"),"scr-") > 0 then FreeProductInCart = 1
		'session("errorSQL") = "nItemTotal = " & nItemTotal & " | nProdIdCheck = " & nProdIdCheck & " | sItemPrice = " & sItemPrice & " | nProdQuantity = " & nProdQuantity
		
		if instr(RS("PartNumber"),"PHN-") < 1 and RS("nodiscount") = 0 and ucase(left(RS("PartNumber"), 2)) <> "BT" then 
			specialAmt = specialAmt + (sItemPrice * nProdQuantity)
		end if
		
		nSubTotal = nSubTotal + (sItemPrice * nProdQuantity)
		ntotalQuantity = ntotalQuantity + RS("qty")
		if left(RS("PartNumber"),4) <> "WCD-" then
			shippingQty = shippingQty + RS("qty")
		end if
		
		if nProdIdCheck => 1000000 then
			sql = "select preferredImg, image, defaultImg from we_items_musicSkins where id = " & nProdIdCheck
			session("errorSQL") = sql
			set msRS = oConn.execute(sql)
			if not msRSEOF then
				preferredImg = msRS("preferredImg")
				itempic_CO = msRS("image")
				defaultImg = msRS("defaultImg")
				if isnull(preferredImg) then																					
					if hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsLarge", itempic_CO) then
						sql = "update we_items_musicSkins set preferredImg = 1 where id = " & itemID
						session("errorSQL") = sql
						oConn.execute(sql)
						useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
					elseif hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault", defaultImg) then
						sql = "update we_items_musicSkins set preferredImg = 2 where id = " & itemID
						session("errorSQL") = sql
						oConn.execute(sql)
						useImg = "http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault/" & defaultImg
					else
						useImg = "/productPics/thumb/imagena.jpg"
					end if
				elseif preferredImg = 1 then
					useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
				elseif preferredImg = 2 then
					useImg = "/productpics/musicSkins/musicSkinsDefault/" & defaultImg
				else
					useImg = "/productPics/thumb/imagena.jpg"
				end if
			end if
			msRS = null
		else
			useImg = "/productpics/homepage65/" & sItemPic
		end if
		htmBasketRows = htmBasketRows & fWriteBasketRow(nProdIdCheck,sItemName,sItemPrice,cStr(nProdQuantity),useImg)
										
		WEhtml = WEhtml & "<input type=""hidden"" name=""itemID_" & nProdIdCount & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""itemQty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		'PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & sItemName & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & nProdQuantity & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & sItemPrice & """>" & vbcrlf
		
		'eBillme variables
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_number_" &  nProdIdCount & """ value=""" & nProdIdCheck & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_qty_" &  nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_price_" &  nProdIdCount & """ value=""" & sItemPrice & """>" & vbcrlf
		EBxml = EBxml & "			<itemdetail>" & vbcrlf
		EBxml = EBxml & "				<id>" & nProdIdCheck & "</id>" & vbcrlf
		EBxml = EBxml & "				<name>" & sItemName & "</name>" & vbcrlf
		EBxml = EBxml & "				<quantity>" & nProdQuantity & "</quantity>" & vbcrlf
		EBxml = EBxml & "				<itemcost>" & formatnumber(RS("price_CO"), 2) & "</itemcost>" & vbcrlf
		EBxml = EBxml & "			</itemdetail>"
	end if
	RS.movenext
loop
RS.close
set RS = nothing
call fCloseConn()
' END basket grab

if nProdIdCount < 1 then
'basket currently empty
	response.redirect("/sessionexpired.asp")
	response.end
end if
' END basket grab

session("sWeight") = sWeight

'#### Upfront Shipping Costs ####
dim shipcost0, shipcost2, shipcost3, shipcost4, shipcost5
'shipcost0 = formatCurrency(Application("shippingCost"))
'shipcost2 = formatCurrency(9.99)
'shipcost3 = formatCurrency(22.99)
'shipcost4 = formatCurrency(7.99)
'shipcost5 = formatCurrency(14.99)

'#### Shady Shipping Costs ####
'shipcost0 = formatCurrency(3.96 + (1.99 * nTotalQuantity))
'shipcost2 = formatCurrency(7.96 + (1.99 * nTotalQuantity))
'shipcost3 = formatCurrency(20.96 + (1.99 * nTotalQuantity))
'shipcost4 = formatCurrency(4.96 + (2.99 * nTotalQuantity))
'shipcost5 = formatCurrency(11.96 + (2.99 * nTotalQuantity))

'#### changed on 4/19/2012 5:00PM by Terry
shipcost0 = formatCurrency(4.00 + (1.99 * shippingQty))
shipcost2 = formatCurrency(8.00 + (1.99 * shippingQty))
shipcost3 = formatCurrency(21.00 + (1.99 * shippingQty))
shipcost4 = formatCurrency(5.00 + (2.99 * shippingQty))
shipcost5 = formatCurrency(12.00 + (2.99 * shippingQty))

'response.write "shiptype:" & shiptype & "<br>"
if request.form("refreshed") = "1" then
	if (len(sstate) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sstate) > 0) then
		sSCountry = "CANADA"
	else
		sSCountry = ""
	end if
	select case shiptype
		case "2"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				shiptype = "5"
				nShipRate = shipcost5
			else
				strShiptype = "0"
				nShipRate = shipcost2
			end if
		case "3"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				shiptype = "5"
				nShipRate = shipcost5
			else
				strShiptype = "0"
				nShipRate = shipcost3
			end if
		case "4"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				nShipRate = shipcost4
			else
				strShiptype = "0"
				shiptype = "0"
				nShipRate = shipcost0
			end if
		case "5"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				nShipRate = shipcost5
			else
				strShiptype = "0"
				shiptype = "2"
				nShipRate = shipcost2
			end if
		case "0"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				shiptype = "4"
				nShipRate = shipcost4
			else
				strShiptype = "0"
				nShipRate = shipcost0
			end if
		case else
			nShipRate = formatCurrency(request.form("shipcost" & shiptype))
	end select
	if sstate = "CA" then
		nCATax = round(nSubTotal * Application("taxMath"), 2)
	else
		nCATax = 0
	end if
end if

if spendThisMuch = 1 then
	if nSubTotal => stm_total then
		sql = "select b.brandName, c.modelName, a.* from we_items a left join we_brands b on a.brandID = b.brandID left join we_models c on a.modelID = c.modelID where itemID = " & stm_freeItem
		set freeItemRS = Server.CreateObject("ADODB.Recordset")
		freeItemRS.open sql, oConn, 0, 1
		
		free_brandName = prepStr(freeItemRS("brandName"))
		free_modelName = prepStr(freeItemRS("modelName"))
		free_partNumber = prepStr(freeItemRS("partNumber"))
		free_itemDesc = prepStr(freeItemRS("itemDesc"))
		free_itemID = prepInt(freeItemRS("itemID"))
		free_itemPic = freeItemRS("itemPic_CO")
		free_price = freeItemRS("price_co")
	
		htmBasketRows = htmBasketRows & fWriteBasketRow(free_itemID,free_itemDesc,0,1,free_itemPic)
	end if
end if
%>    

<!--#include virtual="/includes/template/top_cart.asp"-->
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->
<link rel="stylesheet" type="text/css" href="/includes/css/checkout.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/checkout/mvt_billing2.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/checkout/mvt_testi1.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/checkout/mvt_trust1.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/checkout/mvt_payment2.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/checkout/mvt_topNav2.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/checkout/mvt_topBanner1.css" />
<link rel="stylesheet" type="text/css" href="/includes/css/mvt/checkout/mvt_applyBtn1.css" />
<div id="account_nav" style="display:none;"></div>

<td>
    <form id="id_frmProcessOrder" action="/cart/checkout-c.asp" method="post" name="frmProcessOrder">
    <div style="width:980px; text-align:left; padding-top:10px;">
        <div id="mvt_topBanner" style="float:left; width:100%; text-align:center; padding:5px 0px 5px 0px;"><img src="/images/cart/110-banner.jpg" border="0" width="980" height="78" /></div>
        <div style="float:left; width:100%; text-align:center; border-top:1px dotted #000; border-bottom:1px dotted #000; padding:15px 0px 15px 0px;">
        	<div style="float:left; margin-left:250px; padding:0px 10px 0px 0px;"><img src="/images/checkout/lock.png" border="0" /></div>
        	<a href="/cart/basket.asp" style="font-size:26px; color:#999; font-weight:normal;"><div style="float:left; font-size:28px; color:#999; padding-top:6px;">Shopping Cart</div></a>
        	<div style="float:left; padding:0px 10px 0px 10px;"><img src="/images/checkout/dotted-div.png" border="0" /></div>
        	<div style="float:left; font-size:26px; color:#02659E; padding-top:6px;">Order</div>
        	<div style="float:left; padding:0px 10px 0px 10px;"><img src="/images/checkout/dotted-div.png" border="0" /></div>
        	<div style="float:left; font-size:26px; color:#999; padding-top:6px;">Receipt</div>
		</div>
        <div style="float:left; width:100%; padding-top:10px;">        
            <!-- item review -->
            <div style="width:950px; display:table; margin:0px auto;">
                <div style="float:left; width:100%; background:url(/images/cart/headerbar-blue.jpg) repeat-x; height:23px; font-size:12px; color:#fff; padding-top:5px; border-top-left-radius:5px; border-top-right-radius:5px;">
                    <div style="float:left; width:600px; text-align:left; font-size:14px; font-weight:bold;">&nbsp; Item</div>
                    <div style="float:left; width:110px; text-align:center; font-size:14px; font-weight:bold;">Qty.</div>
                    <div style="float:left; width:110px; text-align:center; font-size:14px; font-weight:bold;">Unit Price</div>
                    <div style="float:left; width:110px; text-align:center; font-size:14px; font-weight:bold;">Cost</div>
                </div>
                <%=htmBasketRows%>
                <%
                if sstate = "CA" then
                    nCATax = round(nSubTotal * Application("taxMath"), 2)
                else
                    nCATax = 0
                end if
                
                nAmount = nShipRate + nCATax + nSubTotal + buysafeamount
                if date > cdate("9/30/2011") then specialAmt = 0 end if
                spend2Get = 0
                
                if spend2Get = 1 then																						
                    if sPromoCode = "" and specialAmt >= 30 then
                        sPromoCode = "specialDeal"
                        nSubTotal = nSubTotal - 5
                        nAmount = nAmount - 5
                    end if
                end if	
                
                if sPromoCode <> "" then
                    session("promocode") = sPromoCode
                    if sPromoCode <> "specialDeal" then
                    %>
                    <div style="float:left; width:100%; padding-top:5px; text-align:right; font-size:16px; color:#333;">
                        <b>Promotions:</b>&nbsp;&nbsp;&nbsp;<%=couponDesc%> &nbsp;  &nbsp;  &nbsp; 
                        <input type="hidden" name="sPromoCode" value="<%=sPromoCode%>">
                        <input type="hidden" name="nPromo" value="<%=couponid%>">
                    </div>
                    <%
                    end if
                end if
                %>
                <div class="orderTotalRow">
                    <div class="orderTotals"><%=formatCurrency(nSubTotal)%></div>
                    <div class="orderTotalTitle">Subtotal:</div>
                </div>
                <% if nShipRate = "" then %>
                <div class="orderTotalRow">
                    <div id="grandTotal" class="orderTotals bold"><%=formatCurrency(nSubTotal)%></div>
                    <div class="orderTotalTitle">Total:</div>
                </div>
                <% end if %>
                <%
                if nCATax > 0 then
                    %>
                    <div class="orderTotalRow">
                        <div class="orderTotals"><%=formatCurrency(nCATax)%></div>
                        <div class="orderTotalTitle">Additional <%=Application("taxDisplay")%>% Tax for Shipments Within CA:</div>
                    </div>
                    <%
                end if
    
                if discountTotal > 0 then
                %>
                    <div class="orderTotalRow">
                        <div class="orderTotals"><%=formatCurrency(discountTotal)%></div>
                        <div class="orderTotalTitle">Discount Total:</div>
                    </div>            
                <%
                end if
    
                if nShipRate <> "" then
                %>
                <div class="orderTotalRow">
                    <div class="orderTotals"><%=formatCurrency(nShipRate)%></div>
                    <div class="orderTotalTitle">Shipping & Handling:</div>
                </div>
                <div class="orderTotalRow">
                    <div class="orderTotals bold" id="GrandTotal"><%=formatCurrency(nAmount)%></span></div>
                    <div class="orderTotalTitle">Grand Total:</div>
                </div>
                <%end if%>
                <input type="hidden" name="nCATax" value="<%=nCATax%>">
                <input type="hidden" name="subTotal" value="<%=nSubTotal%>">
                <input type="hidden" name="grandTotal" value="<%=nAmount%>">
                <input type="hidden" name="buysafeamount" value="<%=round(buysafeamount,2)%>">
                <input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
                <input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
            </div>
            <!--// item review -->
        </div>
        <div style="display:table; width:950px; margin:0px auto; padding-top:10px;">        
            <!-- Shipping Information -->
            <div style="width:350px; float:left;">
                <div style="float:left; width:100%; background:url(/images/cart/headerbar-blue.jpg) repeat-x; height:23px; font-size:12px; color:#fff; padding-top:5px; border-top-left-radius:5px; border-top-right-radius:5px;">
                    <div style="float:left; text-align:left; font-size:14px; font-weight:bold;">&nbsp; Shipping Information</div>
                </div>
                <div class="shippingSubtitle">
                	<div class="billingSubtitleText">Shipping Address:</div>
                </div>
                <div class="shippingFormEntry">
                	<div class="shippingFormTitle">*First Name:</div>
                    <div class="shippingFormField"><input type="text" name="fname" value="<%=fname%>" class="activeBillingField" /></div>
                </div>
                <div class="shippingFormEntry">
                	<div class="shippingFormTitle">*Last Name:</div>
                    <div class="shippingFormField"><input type="text" name="lname" value="<%=lname%>" class="activeBillingField" /></div>
                </div>
                <div class="shippingFormEntry">
                	<div class="shippingFormTitle">*Address 1:</div>
                    <div class="shippingFormField"><input type="text" name="sAddress1" value="<%=saddress1%>" class="activeBillingField" /></div>
                </div>
                <div class="shippingFormEntry">
                	<div class="entryExample">Stree address, PO box, company name, c/o</div>
                </div>
                <div class="shippingFormEntry">
                	<div class="shippingFormTitle">Address 2:</div>
                    <div class="shippingFormField"><input type="text" name="sAddress2" value="<%=saddress2%>" class="activeBillingField" /></div>
                </div>
                <div class="shippingFormEntry">
                	<div class="entryExample">Apartment, suite, unit, building, floor, etc.</div>
                </div>
                <div class="shippingFormEntry">
                	<div class="shippingFormTitle">*Zip/Postal:</div>
                    <div class="shippingFormField"><input type="text" name="sZip" value="<%=szip%>" class="activeBillingField" onkeyup="getNewShipping2('B')" /></div>
                </div>
                <div class="shippingFormEntry">
                	<div class="shippingFormTitle">*City:</div>
                    <div class="shippingFormField" style="position:relative;">
                    	<input type="text" name="sCity" value="<%=scity%>" class="activeBillingField" />
                        <div id="id_sCity" style="position:absolute; display:none; z-index:100; top:0px; right:-265px; width:250px; padding:5px; border-radius:5px; border:2px solid #336799; background-color:#fff;">
                            <div style="float:left; width:100%; padding-bottom:3px; border-bottom:1px solid #ccc;">
                                <div style="float:left; font-size:12px; color:#555;">City Name Suggestion</div>
                                <div style="float:right;"><a style="font-size:12px; color:#555; cursor:pointer;" onclick="closeCityPop('S');">Close</a></div>
                            </div>
                            <div id="id_return_sCity" style="float:left; text-align:center; width:100%; height:150px; overflow:auto;"></div>
                        </div>
                    </div>
                </div>
                <div class="shippingFormEntry">
                	<div class="shippingFormTitle">*State:</div>
                    <div class="shippingFormField">
                    	<select name="sState" class="activeBillingField" onchange="getNewShippingByState(this.value)">
							<%getStates(sstate)%>
                        </select>
                    </div>
                </div>
                <div class="shippingFormEntry">
                	<div class="shippingFormTitle">*Phone:</div>
                    <div class="shippingFormField"><input type="text" name="phone" value="<%=phone%>" class="activeBillingField" /></div>
                </div>
                <div class="shippingFormEntry" style="padding-bottom:20px;">
                	<div class="shippingFormTitle">*Email:</div>
                    <div class="shippingFormField"><input type="text" name="email" value="<%=email%>" class="activeBillingField" onblur="remarket(this.value,'<%=mySession%>')" /></div>
                </div>
                <div class="shippingSubtitleLower">
                	<div class="billingSubtitleText">Shipping Method:</div>
                </div>
                <div class="shippingFormEntryBottom">
                	<div id="shippingOptionsBox" class="shippingOptions">
						<%
                        if sZip <> "" then
							response.write UPScodeSelectBox(sZip,sWeight,shiptype,nTotalQuantity,0,0)
						else
						%>
                        <input type="hidden" name="shipcost0" value="<%=shipcost0%>">
                        <input type="hidden" name="shipcost2" value="<%=shipcost2%>">
                        <input type="hidden" name="shipcost3" value="<%=shipcost3%>">
                        <input type="hidden" name="shipcost4" value="<%=shipcost4%>">
                        <input type="hidden" name="shipcost5" value="<%=shipcost5%>">
                    	<select name="shiptype" class="activeBillingField">
							<% if strShiptype = "1" then %>
                            <option value="4"<%if shiptype = "4" or shiptype = "3" then%> selected="selected"<% end if %>>USPS First Class Int'l (8-12 business days)</option>
                            <option value="5"<%if shiptype = "5" then%> selected="selected"<% end if %>>USPS Priority Int'l (3-8 business days)</option>
                            <% else %>
                            <option value="0"<%if shiptype = "0" then%> selected="selected"<% end if %>>USPS First Class (4-10 business days)</option>
                            <option value="2"<%if shiptype = "2" then%> selected="selected"<% end if %>>USPS Priority Mail (2-4 business days)</option>
                            <option value="3"<%if shiptype = "3" then%> selected="selected"<% end if %>>Express Mail (1-2 business days)</option>
                            <% end if %>
                        </select>
                        <% end if %>
                    </div>
                    <div class="applyButton" onClick="fnRecalculate();"></div>
                </div>
                <div style="display:table; width:300px; padding-top:10px;">
                    <div style="float:left; width:25px; height:70px; text-align:left;"><input type="checkbox" name="chkOptMail" value="Y" checked></div>
                    <div style="float:left; width:250px; height:70px; color:#333; font-size:12px;">Please notify me via email of exclusive CellularOutfitter.com "below wholesale" private sales.</div>
                </div>
                <div style="display:table; width:300px; color:#333; font-size:12px; padding-left:25px;">
                    CellularOutfitter.com abides by the strictest Privacy Policy standards. Please review our 
                    privacy policy <a href="/privacypolicy.html" target="_blank" class="showMoreLink">here</a>.
                </div>
            </div>
            <!--// Shipping Information -->
            <!-- Billing Information -->
            <div style="width:590px; float:right;">
                <div style="float:left; width:100%; background:url(/images/cart/headerbar-blue.jpg) repeat-x; height:23px; font-size:12px; color:#fff; padding-top:5px; border-top-left-radius:5px; border-top-right-radius:5px;">
                    <div style="float:left; text-align:left; font-size:14px; font-weight:bold;">&nbsp; Billing Information</div>
                </div>
                <div class="billingSubtitle">
                	<div class="billingSubtitleText">Billing Address:</div>
                </div>
                <div class="billOption1">
                	<div class="billOptionRadio"><input type="radio" name="billingOption" value="1" checked="checked" onclick="billingFields('off')" /></div>
                    <div class="billOptionTitle">Same as shipping</div>
                </div>
                <div class="billOption2">
                	<div class="billOptionRadio"><input type="radio" name="billingOption" value="2" onclick="billingFields('on')" /></div>
                    <div class="billOptionTitle">Other</div>
                </div>
                <div class="billingFields">
                	<div id="grayOutBox" class="grayOut"></div>
                    <div class="billingFormEntry1">
                        <div class="billingFormItem">Address 1:</div>
                        <div class="billingFormItem">Address 2:</div>
                    </div>
                    <div class="billingFormEntry2">
                        <div class="billingFormItem"><input type="text" name="bAddress1" value="<%=baddress1%>" class="activeBillingField" /></div>
                        <div class="billingFormItem"><input type="text" name="bAddress2" value="<%=baddress2%>" class="activeBillingField" /></div>
                    </div>
                    <div class="billingFormEntry1">
                        <div class="billingFormItem">City:</div>
                        <div class="billingFormItem">State:</div>
                    </div>
                    <div class="billingFormEntry2">
                        <div class="billingFormItem"><input type="text" name="bCity" value="<%=bcity%>" class="activeBillingField" /></div>
                        <div class="billingFormItem">
                            <select name="bState" class="activeBillingField">
                                <%getStates(sstate)%>
                            </select>
                        </div>
                    </div>
                    <div class="billingFormEntry1">
                        <div class="billingFormItem">Zip/Postal:</div>
                    </div>
                    <div class="billingFormEntry2">
                        <div class="billingFormItem"><input type="text" name="bZip" value="<%=bzip%>" class="activeBillingField" /></div>
                    </div>
                </div>
                <div class="billingSubtitleLower">
                	<div class="billingSubtitleText">Payment Method:</div>
                </div>
                <div class="paymentSection">
                	<div class="paymentHeader">
                    	<div class="paymentLock"></div>
                        <div class="paymentHeaderText">
                            <div style="width:480px; text-align:left; font-size:28px; font-weight:bold; color:#222;">
                                Secure Credit Card Payment
                            </div>
                            <div style="width:480px; text-align:left; font-weight:bold; color:#222; font-size:14px;">
                                This is a secure 128-bit SSL encrypted payment. &nbsp; &nbsp; &nbsp; <span style="font-style:italic; font-weight:normal; font-size:14px; color:#999;">*Required Field</span>
                            </div>
                        </div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:160px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Cardholder Name:</div>
                        <div style="float:left; width:190px; text-align:center; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                            <input name="cc_cardOwner" type="text" style="width:180px; margin:5px 0px 5px 0px; border:0px;" >
                        </div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:160px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Credit Card Number:</div>
                        <div style="float:left; width:190px; text-align:center; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                            <input name="cardNum" type="text" maxlength="16" onblur="getCardType2(this.value)"  style="width:180px; margin:5px 0px 5px 0px; border:0px;">
                        </div>
                        <div style="float:left; width:140px; text-align:right; border-radius:4px; padding-left:5px;">
                            <div style="float:left; border:1px solid #f4f4f4; padding:2px; border-radius:4px;" id="cc_master"><img id="cc_master_img" src="/images/checkout/cc-master-off.png" border="0" /></div>
                            <div style="float:left; border:1px solid #f4f4f4; padding:2px; border-radius:4px;" id="cc_visa"><img id="cc_visa_img" src="/images/checkout/cc-visa-off.png" border="0" /></div>
                            <div style="float:left; border:1px solid #f4f4f4; padding:2px; border-radius:4px;" id="cc_amex"><img id="cc_amex_img" src="/images/checkout/cc-amex-off.png" border="0" /></div>
                            <div style="float:left; border:1px solid #f4f4f4; padding:2px; border-radius:4px;" id="cc_disc"><img id="cc_disc_img" src="/images/checkout/cc-discover-off.png" border="0" /></div>
                            <input type="hidden" name="cc_cardType" value="">
                        </div>
                    </div>
                    <div style="float:left; width:100%;">
                        <div style="float:left; width:160px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold;">&nbsp;</div>
                        <div style="float:left; width:190px; text-align:right; font-style:italic; color:#999; font-size:12px; text-align:left;">No spaces/dashes<br />Example: 1234123412341234</div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:160px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:5px;">*Expiration Date:</div>
                        <div style="float:left; width:80px; text-align:center; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                            <select name="cc_month" style="width:70px; margin:5px 0px 5px 0px; border:0px;">
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </div>
                        <div style="float:left; width:30px; text-align:center; padding-top:5px;">/</div>
                        <div style="float:left; width:80px; text-align:center; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                            <select name="cc_year" style="width:70px; margin:5px 0px 5px 0px; border:0px;">
                            <%
                            for countYear = 0 to 14
                                response.write "<option value=""" & right(cStr(year(date) + countYear),2) & """>" & right(cStr(year(date) + countYear),2) & "</option>" & vbcrlf
                            next
                            %>
                            </select>
                        </div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:160px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold; color:#333; padding-top:10px;">*Verification Code:</div>
                        <div style="float:left; width:80px; text-align:center;  margin:7px 15px 0px 0px; border:1px solid #ccc; border-radius:4px; background-color:#fff;">
                            <input name="secCode" type="text" id="secCode" maxlength="4"  style="width:70px; margin:5px 0px 5px 0px; border:0px;">
                        </div>
                        <div style="float:left; width:85px; text-align:left;"><img src="/images/checkout/cc-verify.png" border="0" width="62" height="42" /></div>
                    </div>
                    <div style="float:left; width:100%; padding-top:10px;">
                        <div style="float:left; width:160px; text-align:right; margin-right:15px; font-size:15px; font-weight:bold;">&nbsp;</div>
                        <div style="float:left; width:300px; text-align:right; font-style:italic; color:#333; font-size:11px; text-align:left;">
                            <span style="font-weight:bold; color:#000;">Note:</span> American Express cards show the verification code printed above and to the right of the imprinted card number on the front of the card.
                        </div>
                    </div>
                </div>
            </div>
            <!--// Billing Information -->
        </div>
        <div class="checkoutFooter">
        	<div class="shopMore"><a href="/" class="showMoreLink">&lt;&lt;&nbsp;Shop more items</a></div>
            <div class="trustAndOrder">
            	<div class="trustIcons">
                	<div style="float:left; padding:10px;">
                        <!-- START SCANALERT CODE -->
                        <a target="_blank" href="https://www.mcafeesecure.com/RatingVerify?ref=www.cellularoutfitter.com"><img width="65" height="37" border="0" src="//images.scanalert.com/meter/www.cellularoutfitter.com/63.gif" alt="McAfee Secure sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="alert('Copying Prohibited by Law - McAfee Secure is a Trademark of McAfee, Inc.'); return false;"></a>
                        <!-- END SCANALERT CODE -->                                    
                    </div>
                    <div style="float:left; padding:10px 10px 10px 5px;">
                        <img src="/images/checkout/trust-norton.png" border="0" />
                    </div>
                    <div style="float:left; padding:10px 0px 10px 5px;">
                        <a href="/privacypolicy.html" target="_blank"><img src="/images/checkout/trust-truste.png" border="0" /></a>
                    </div>
                </div>
                <div class="placeOrder" id="submitBttn"><img onClick="return CheckSubmit();" src="/images/checkout/btn-order-now.png" alt="Submit Order" style="cursor: pointer;"></div>
            </div>
        </div>
        <div style="display:none;"><input id="sameaddress" type="checkbox" name="sameaddress" value="yes" checked="checked"></div>
        <input type="hidden" name="sWeight" value="<%=sWeight%>">
        <input type="hidden" name="refreshed" value="1">
        <input type="hidden" name="nTotalQuantity" value="<%=nTotalQuantity%>">
        <input type="hidden" name="shippingTotal" value="<%=nShipRate%>">
        <input type="hidden" name="PaymentType" value="CC">
        <input type="hidden" name="submitted" value="submitted">
        <!--WE variables-->
        <%=WEhtml%>
        <!--eBillme variables-->
        <%=EBtemp%>
        <input type="hidden" name="myBasketXML" value="<%=EBxml%>">
        <input type="hidden" name="ssl_ProdIdCount" value="<%=nProdIdCount%>">
    </div>
    </form>
</td>
<!--#include virtual="/includes/template/bottom_cart.asp"-->
<%
function fWriteBasketRow(nProdId,sItemName,sItemPrice,nQty,sitempic_CO)
	prodLink = "http://www.cellularoutfitter.com/p-" & nProdId & "-" & formatSEO(sItemName) & ".html"
	sTemp	=	"<div style=""display:table; width:948px; margin:0px auto; border-left:1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc; color:#333;"">" & vbcrlf & _
				"	<div style=""float:left; width:600px; height:70px; border-right:1px solid #ccc; padding:10px 0px 10px 0px;"">" & vbcrlf & _
				"		<div style=""float:left; width:65px; margin:0px 20px 0px 5px;""><a href=""" & prodLink & """><img src=""" & useImg & """ width=""65"" height=""65"" border=""0"" /></a></div>" & vbcrlf & _
				"		<div style=""float:left; width:233px;"">" & vbcrlf & _
				"			<a href=""" & prodLink & """ style=""font-size:13px; font-weight:normal; color:#333;"">" & sItemName & " <span style=""color:#359800; font-weight:bold;"">In Stock!</span></a>" & vbcrlf & _
				"		</div>" & vbcrlf & _								
				"	</div>" & vbcrlf & _
				"	<div style=""float:left; width:76px; height:50px; text-align:center; border-right:1px solid #ccc; padding:30px 0px 10px 34px;"">" & vbcrlf & _
				"		<div style=""padding:5px; text-align:center; width:40px; overflow:hidden; color:#555; font-size:12px;"">" & nQty & vbcrlf & _
				"		</div>" & vbcrlf & _
				"	</div>" & vbcrlf

	if cDbl(sItemPrice) = 0 then
		sTemp = sTemp & "	<div style=""float:left; width:110px; text-align:center; color:#333; font-size:13px; height:50px; border-right:1px solid #ccc; padding:30px 0px 10px 0px;"">FREE</div>" & vbcrlf & _
						"	<div style=""float:left; width:110px; text-align:center; color:#333; font-size:13px; height:50px; padding:30px 0px 10px 0px;"">FREE</div>" & vbcrlf
	else
		sTemp = sTemp & "	<div style=""float:left; width:110px; text-align:center; color:#333; font-size:13px; height:50px; border-right:1px solid #ccc; padding:30px 0px 10px 0px;"">" & formatCurrency(cDbl(sItemPrice)) & vbcrlf & _
						"	</div>" & vbcrlf & _
						"	<div style=""float:left; width:110px; text-align:center; color:#333; font-size:13px; height:50px; padding:30px 0px 10px 0px;"">" & formatCurrency((cDbl(sItemPrice) * cDbl(nQty))) & vbcrlf & _
						"	</div>" & vbcrlf
	end if
	
	sTemp = sTemp & "</div>"

	fWriteBasketRow = sTemp
end function
%>
<script language="javascript">
	if (document.frmProcessOrder.sZip.value != "") {
		getNewShipping()
	}
	
	function remarket(email,mySession) {
		ajax('/ajax/remarketing.asp?email=' + email + '&mySession=' + mySession + '&siteID=2','dumpZone')
	}
	
	function getStyle(oElm, strCssRule) {
		var strValue = "";
		if(document.defaultView && document.defaultView.getComputedStyle){
			strValue = document.defaultView.getComputedStyle(oElm, "").getPropertyValue(strCssRule);
		}
		else if(oElm.currentStyle){
			strCssRule = strCssRule.replace(/\-(\w)/g, function (strMatch, p1){
				return p1.toUpperCase();
			});
			strValue = oElm.currentStyle[strCssRule];
		}
		return strValue;
	}

	function getNewShippingByState(sState) {
		strStates = "AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT";
		if (strStates.search(sState) >= 0) {
			document.getElementById('id_shippingDetails').innerHTML = '<input type="hidden" name="shipcost4" value="<%=shipcost4%>"><input type="radio" name="shiptype" value="4" checked>USPS First Class Int\'l (8-12 business days)<br>';
			document.getElementById('id_shippingDetails').innerHTML = document.getElementById('id_shippingDetails').innerHTML + '<input type="hidden" name="shipcost5" value="<%=shipcost5%>"><input type="radio" name="shiptype" value="5">USPS Priority Int\'l (3-8 business days)';
		}
	}

	function getNewShipping()
	{
		document.getElementById('dumpZone').innerHTML = "";
		zipcode = document.frmProcessOrder.sZip.value;

		updateURL = '/ajax/shippingDetails.asp?selectBox=1&zip=' + zipcode + '&weight=<%=sWeight%>&shiptype=<%=shiptype%>&nTotalQuantity=<%=nTotalQuantity%>&nSubTotal=<%=nSubTotal%>';
		if (zipcode.length >= 5) {
			ajax(updateURL,'shippingOptionsBox');
		}
	}
	
	function getNewShipping2(zipType)
	{
		document.getElementById('dumpZone').innerHTML = "";
		zipcode = document.frmProcessOrder.sZip.value;

		updateURL = '/ajax/shippingDetails.asp?selectBox=1&zip=' + zipcode + '&weight=<%=sWeight%>&shiptype=<%=shiptype%>&nTotalQuantity=<%=nTotalQuantity%>&nSubTotal=<%=nSubTotal%>';
		if (zipcode.length >= 5) {
			ajax(updateURL,'shippingOptionsBox');
			getCityByZip('S');
		}
	}

	function getCityByZip(zipType) {
		useID = ""
		if (zipType == 'B') {
			zipcode = document.frmProcessOrder.bZip.value;
			useID = "bCity"
		} else {
			zipcode = document.frmProcessOrder.sZip.value;
			useID = "sCity"
		}
	
		if (zipcode.length >= 5) {
			document.getElementById('id_return_'+useID).innerHTML = 'Loading..';
			ajax('/ajax/cityByZip.asp?zipcode=' + zipcode + '&ziptype=' + zipType,'id_return_'+useID);
			document.getElementById('id_'+useID).style.display = '';
		}
	}
	
	function populateCityState(zipType, cityname, state) {
		if (zipType == 'B') {
			document.frmProcessOrder.bCity.value = cityname;
			document.frmProcessOrder.bState.value = state;
			document.getElementById('id_bCity').style.display = 'none';
		} else {
			document.frmProcessOrder.sCity.value = cityname;
			document.frmProcessOrder.sState.value = state;
			document.getElementById('id_sCity').style.display = 'none';
		}
	}

	function closeCityPop(zipType) {
		if (zipType == 'B') {
			useID = "bCity"
		} else {
			useID = "sCity"
		}
				
		document.getElementById('id_'+useID).style.display='none';
		return false;	
	}
	

	function testValue(obj,objVal) {
		if (objVal == "Card Holder Name") {
			obj.value = ""
		}
		if (objVal == "Credit Card Number") {
			obj.value = ""
		}
		if (objVal == "Security Code") {
			obj.value = ""
		}
	}
	
	function popUp(url, winName, h, w, l, t) {
		var args = 'height=' + h + ',width=' + w + ',left=' + l + ',top=' + t + ',toolbar=1,location=1,directories=0,status=1,menubar=1,scrollbars=1,resizable=1';
		window.open(url, winName, args);
	}
	
	function getCardType(cardNum) {
		replaceValue(document.frmProcessOrder.cardNum,'Credit Card Number')
		document.getElementById("visaPic").src = "/images/cart/visa_off.gif"
		document.getElementById("amexPic").src = "/images/cart/americanExpress_off.gif"
		document.getElementById("mcPic").src = "/images/cart/masterCard_off.gif"
		document.getElementById("discoverPic").src = "/images/cart/discover_off.gif"
		var first1 = parseInt(cardNum.substring(0,1))
		var first2 = parseInt(cardNum.substring(0,2))
		if (first1 == 4) {
			document.frmProcessOrder.cc_cardType.value = "VISA"
			document.getElementById("visaPic").src = "/images/cart/visa_on.gif"
		}
		else if (first2 == 34 || first2 == 37) {
			document.frmProcessOrder.cc_cardType.value = "AMEX"
			document.getElementById("amexPic").src = "/images/cart/americanExpress_on.gif"
		}
		else if (first2 > 50 && first2 < 56) {
			document.frmProcessOrder.cc_cardType.value = "MC"
			document.getElementById("mcPic").src = "/images/cart/masterCard_on.gif"
		}
		else if (first1 == 6) {
			document.frmProcessOrder.cc_cardType.value = "DISC"
			document.getElementById("discoverPic").src = "/images/cart/discover_on.gif"
		}
	}
	
	function getCardType2(cardNum) {
		document.getElementById('cc_master_img').src = '/images/checkout/cc-master-off.png';
		document.getElementById('cc_master').style.backgroundColor = '#f4f4f4';
		document.getElementById('cc_master').style.borderColor = '#f4f4f4';
		document.getElementById('cc_visa_img').src = '/images/checkout/cc-visa-off.png';
		document.getElementById('cc_visa').style.backgroundColor = '#f4f4f4';
		document.getElementById('cc_visa').style.borderColor = '#f4f4f4';		
		document.getElementById('cc_amex_img').src = '/images/checkout/cc-amex-off.png';
		document.getElementById('cc_amex').style.backgroundColor = '#f4f4f4';
		document.getElementById('cc_amex').style.borderColor = '#f4f4f4';
		document.getElementById('cc_disc_img').src = '/images/checkout/cc-discover-off.png';
		document.getElementById('cc_disc').style.backgroundColor = '#f4f4f4';
		document.getElementById('cc_disc').style.borderColor = '#f4f4f4';
		
		var first1 = parseInt(cardNum.substring(0,1))
		var first2 = parseInt(cardNum.substring(0,2))
		if (first1 == 4) {
			document.frmProcessOrder.cc_cardType.value = "VISA"
			document.getElementById('cc_visa_img').src = '/images/checkout/cc-visa-on.png';
			document.getElementById('cc_visa').style.backgroundColor = '#fff';
			document.getElementById('cc_visa').style.borderColor = '#ccc';
		}
		else if (first2 == 34 || first2 == 37) {
			document.frmProcessOrder.cc_cardType.value = "AMEX"
			document.getElementById('cc_amex_img').src = '/images/checkout/cc-amex-on.png';
			document.getElementById('cc_amex').style.backgroundColor = '#fff';
			document.getElementById('cc_amex').style.borderColor = '#ccc';
		}
		else if (first2 > 50 && first2 < 56) {
			document.frmProcessOrder.cc_cardType.value = "MC"
			document.getElementById('cc_master_img').src = '/images/checkout/cc-master-on.png';
			document.getElementById('cc_master').style.backgroundColor = '#fff';
			document.getElementById('cc_master').style.borderColor = '#ccc';
		}
		else if (first1 == 6) {
			document.frmProcessOrder.cc_cardType.value = "DISC"
			document.getElementById('cc_disc_img').src = '/images/checkout/cc-discover-on.png';
			document.getElementById('cc_disc').style.backgroundColor = '#fff';
			document.getElementById('cc_disc').style.borderColor = '#ccc';
		}
	}
	
	function cvvHelp(dir) {
		if (document.getElementById("cvvData").innerHTML == "") {
			ajax('/ajax/cvv.asp','cvvData')
		}
		if (dir == "off") {
			document.getElementById("cvvWindow").style.display = "none"
		}
		else {
			document.getElementById("cvvWindow").style.display = ""
		}
	}
	
	function replaceValue(input,value) {
		if (input.value == "") {
			input.value = value
		}
	}
		
	function CheckSubmit() {
		var f = document.frmProcessOrder;
		bValid = true;
		
		if (f.sameaddress.checked == true) {
			f.bAddress1.value = f.sAddress1.value
			f.bAddress2.value = f.sAddress2.value
			f.bCity.value = f.sCity.value
			f.bState.selectedIndex = f.sState.selectedIndex
			f.bZip.value = f.sZip.value
		}

		CheckValidNEW(f.email.value, "Email is a required field!");
		CheckEMailNEW(f.email.value);
		
		CheckValidNEW(f.fname.value, "Your First Name is a required field!");
		CheckValidNEW(f.lname.value, "Your Last Name is a required field!");
		CheckValidNEW(f.phone.value, "Your Phone Number is a required field!");
		
		CheckValidNEW(f.sAddress1.value, "Your Address (for Shipping Address) is a required field!");
		CheckValidNEW(f.sCity.value, "Your City (for Shipping Address) is a required field!");
		sString = f.sState.options[f.sState.selectedIndex].value;
		CheckValidNEW(sString, "State (for Shipping Address) is a required field!");
		CheckValidNEW(f.sZip.value, "Your Zip Code (for Shipping Address) is a required field!");
		
		CheckValidNEW(f.bAddress1.value, "Your Address (for Billing Address) is a required field!");
		CheckValidNEW(f.bCity.value, "Your City (for Billing Address) is a required field!");
		sString = f.bState.options[f.bState.selectedIndex].value;
		CheckValidNEW(sString, "State (for Billing Address) is a required field!");
		CheckValidNEW(f.bZip.value, "Your Zip Code (for Billing Address) is a required field!");
	
		var b = "cc";
		var a = f.PaymentType.length;
		if (a != 'undefined') {
			for (var i = 0; i < a; i++) {
				if (f.PaymentType[i].checked) {
					var b = f.PaymentType[i].value;
				}
			}			
		}

		if (b != 'eBillMe' && b != 'PAY') {
			sString = f.cc_cardType.value;
			CheckValidNEW(sString, "Credit Card Type is a required field!");
			CheckValidNEW(f.cc_cardOwner.value, "Your Name (as it appears on your credit card) is a required field!");
			CheckValidNEW(f.cardNum.value, "Your Credit Card Number is a required field!");
			CheckValidNEW(f.cc_month.value, "Your Credit Card Expiration Month is a required field!");
			CheckValidNEW(f.cc_year.value, "Your Credit Card Expiration Year is a required field!");
			CheckValidNEW(f.secCode.value, "Your Credit Card Security Code is a required field!");
		}
		
		if (bValid) {
			var shiptype = f.shiptype.value;
			var shiprate = eval('document.frmProcessOrder.shipcost' + shiptype);
			f.shippingTotal.value = eval("f.shipcost" + shiptype + ".value");
			<% if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then %>
				f.action = '/framework/processing.asp';
			<% else %>
				f.action = 'https://www.cellularoutfitter.com/framework/processing.asp';
			<% end if %>
			document.getElementById("submitBttn").innerHTML = "Please wait while<br />we process your order"
			f.submit();
		}
		return false;
	}
	
	function DiffShipping() {
		var f = document.frmProcessOrder;
		
		if (f.sameaddress.checked == true)
			document.getElementById('id_ShippingInformation').style.display = '';
		else
			document.getElementById('id_ShippingInformation').style.display = 'none';
	}

	function ShipToBillPerson() {
		var f = document.frmProcessOrder;
		if (f.sameaddress.checked == false) {
			f.bAddress1.value = "";
			f.bAddress2.value = "";
			f.bCity.value = "";
			f.bZip.value = "";
			f.bState.selectedIndex = f.sState.options[0];
		}
		else {
			f.bAddress1.value = f.sAddress1.value;
			f.bAddress2.value = f.sAddress2.value;
			f.bCity.value = f.sCity.value;
			f.bZip.value = f.sZip.value;
			f.bState.selectedIndex = f.sState.selectedIndex;
		}
	}
	
	function fnRecalculate() {
		if ((document.frmProcessOrder.bZip.value != '')&&(document.frmProcessOrder.sZip.value == '')) {
			document.frmProcessOrder.sZip.value = document.frmProcessOrder.bZip.value;
		}
		
		if ((document.frmProcessOrder.bState.value != '')&&(document.frmProcessOrder.sState.value == '')) {
			document.frmProcessOrder.sState.value = document.frmProcessOrder.bState.value;
		}
				
		document.frmProcessOrder.action = '/cart/checkout-c.asp';
		document.frmProcessOrder.submit();
	}
	
	function billingFields(action) {
		if (action == "on") {
			document.getElementById("grayOutBox").style.display = "none";
			document.frmProcessOrder.sameaddress.checked = false;
		}
		if (action == "off") {
			document.getElementById("grayOutBox").style.display = "";
			document.frmProcessOrder.sameaddress.checked = true;
			ShipToBillPerson()
		}
	}
</script>