<%
response.buffer = true
SEtitle = "Cheap Cell Phone Accessories - Cellular Phone Accessories - CellularOutfitter.com"
SEdescription = "Buy cheap Cell Phone Accessories at Wholesale Prices to the Public.  Find the latest Discount Cellular Phone Accessories for Motorola, LG, Nokia, Samsung phones and more.  Cellular Accessories at the Lowest Prices Online Guaranteed."
SEkeywords = "cheap cell phone accessories, cell phone accessories, discount cellular phone accessories, cellular phone accessories"
pageTitle = "checkout"
mvtName = "checkout2"
%>

<!--#include virtual="/includes/asp/inc_dbconn.asp"-->
<!--#include virtual="/cart/includes/inc_makeNum.asp"-->
<!--#include virtual="/cart/includes/inc_getStates.asp"-->
<!--#include virtual="/includes/asp/inc_getMySession.asp"-->
<!--#include virtual="/includes/asp/inc_SQLquote.asp"-->
<!--#include virtual="/cart/includes/inc_promoVariables.asp"-->
<%
HTTP_REFERER = lcase(request.servervariables("HTTP_REFERER"))
if instr(HTTP_REFERER,".cellularoutfitter.com") < 1 then
	response.write "<h3>This page will only accept forms submitted from the Cellular Outfitter secure website.</h3>" & vbcrlf
	useBody = "HTTP_REFERER:" & HTTP_REFERER & "<br />"
	useBody = useBody & "SERVER_NAME:" & request.ServerVariables("SERVER_NAME") & "<br />"
	useBody = useBody & "URL:" & request.ServerVariables("URL") & "<br />"
	useBody = useBody & "HTTP_USER_AGENT:" & request.ServerVariables("HTTP_USER_AGENT") & "<br />"
	useBody = useBody & "HTTPS:" & request.ServerVariables("HTTPS") & "<br />"
	useBody = useBody & "QUERY_STRING:" & request.ServerVariables("QUERY_STRING") & "<br />"
	useBody = useBody & "szip:" & session("szip") & "<br />"
	useBody = useBody & "sWeight:" & session("sWeight") & "<br />"
	weEmail "jon@wirelessemporium.com,ruben@wirelessemporium.com","webmaster@cellularoutfitter.com","Checkout Error",useBody
	response.end
end if

dim buysafeamount, WantsBondField, ShoppingCartId
if request.form("buysafeamount") = "" then
	buysafeamount = 0
	WantsBondField = "false"
else
	buysafeamount = cDbl(request.form("buysafeamount"))
	WantsBondField = request.form("WantsBondField")
end if
ShoppingCartId = request.form("buysafecartID")

dim szip, sPromoCode, sWeight
szip = session("szip")
sWeight = session("sWeight")
nCATax = 0
if request.form("submitted") = "submitted" then
	newEmail = request.form("newEmail")
	pword1 = request.form("pword1")
	pword2 = request.form("pword2")
	fname = request.form("fname")
	lname = request.form("lname")
	email = request.form("email")
	phone = request.form("phone")
	saddress1 = request.form("saddress1")
	saddress2 = request.form("saddress2")
	scity = request.form("scity")
	sstate = request.form("sstate")
	szip = request.form("szip")
	bAddress1 = request.form("bAddress1")
	bAddress2 = request.form("bAddress2")
	bCity = request.form("bCity")
	bState = request.form("bState")
	bZip = request.form("bZip")
	radioAddress = request.form("radioAddress")
	shiptype = request.form("shiptype")
	PaymentType = request.form("PaymentType")
	cc_cardType = request.form("cc_cardType")
	cc_cardOwner = request.form("cc_cardOwner")
	cardNum = request.form("cardNum")
	cc_month = request.form("cc_month")
	cc_year = request.form("cc_year")
end if

if request.form("promo") <> "" then
	sPromoCode = request.form("promo")
else
	sPromoCode = session("promocode")
end if

if sPromoCode <> "" then
	'COUPON CODE SUBMITTED, CHECK IF IT EXISTS
	dim couponid
	couponid = 0
	call fOpenConn()
	SQL = "SELECT * FROM CO_coupons WHERE promoCode = '" & sPromoCode & "' AND activate = 1 AND expiration >= getdate()"
	set RS = Server.CreateObject("ADODB.Recordset")
	RS.open SQL, oConn, 0, 1
	if not RS.eof then
		dim promoMin, promoPercent, typeID, excludeBluetooth, BOGO, couponDesc
		'FreeItemPartNumber
		couponid = RS("couponid")
		promoMin = RS("promoMin")
		promoPercent = RS("promoPercent")
		typeID = RS("typeID")
		excludeBluetooth = RS("excludeBluetooth")
		BOGO = RS("BOGO")
		couponDesc = RS("couponDesc")
		'FreeItemPartNumber = RS("FreeItemPartNumber")
	else
		sPromoCode = ""
	end if
	call fCloseConn()
end if

'Start basket grab
dim htmBasketRows, WEhtml, EBtemp, EBxml
htmBasketRows = ""
WEhtml = ""
EBtemp = ""
EBxml = ""

dim sItemName, sRetailPrice, sItemPrice, sItemPic, nProdIdCheck, nProdQuantity, nProdIdCount, nTotalQuantity
dim strItemCheck, strQty, FreeProductInCart
dim SquaretradeItemName, SquaretradeItemPrice, SquaretradeItemCondition

nProdIdCount = 0
nTotalQuantity = 0
strItemCheck = ""
strQty = ""
FreeProductInCart = 0
specialAmt = 0


call fOpenConn()
' New cart abandonment tracking added 9/20/2010 by MC
SQL = "UPDATE ShoppingCart SET DateCheckout = '" & now & "' WHERE store = 2 AND sessionID = '" & mySession & "' AND (purchasedOrderID IS NULL OR purchasedOrderID = 0)"
oConn.execute SQL

sql = 	"SELECT c.brandName, d.modelName, (select top 1 inv_qty from we_items where partNumber = b.partNumber order by master desc, inv_qty desc) as inv_qty, A.itemID, A.qty, B.modelID, B.typeID, B.PartNumber, " & vbcrlf & _
		"B.itemDesc_CO, B.itemPic_CO, B.price_CO, B.price_retail, B.Condition, B.itemWeight, B.NoDiscount FROM ShoppingCart A INNER JOIN we_items B ON A.itemID = " & vbcrlf & _
		"B.itemID left join we_brands c on b.brandID = c.brandID left join we_models d on b.modelID = d.modelID WHERE A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)" & vbcrlf & _
		" union " & vbcrlf & _
		"SELECT '' as brandName, '' as modelName, 100 as inv_qty, A.itemID, A.qty, B.modelID, 3 as typeID, B.musicSkinsID, B.artist + ' ' + B.designName, B.image as itemPic_co, B.price_CO, " & vbcrlf & _
		"B.msrp as price_retail, 1 as Condition, 1 as itemWeight, 1 as NoDiscount FROM ShoppingCart A INNER JOIN we_items_musicskins B ON A.itemID=B.id WHERE " & vbcrlf & _
		"A.store = 2 AND A.sessionID = '" & mySession & "' AND (A.purchasedOrderID IS NULL OR A.purchasedOrderID = 0)"

set RS = Server.CreateObject("ADODB.Recordset")
RS.open SQL, oConn, 0, 1

do until RS.eof
	brandName = RS("brandName")
	modelName = RS("modelName")
	partNumber = RS("partNumber")
	nProdIdCheck = RS("itemID")
	nProdQuantity = RS("qty")
	strItemCheck = strItemCheck & cStr(nProdIdCheck) & ","
	strQty = strQty & cStr(nProdQuantity)& ","  ' QTY string for each item, this is for buySafe Use
	if nProdQuantity > 0 and nProdIdCheck > 0 then
		nProdIdCount = nProdIdCount + 1
		sItemName = insertDetails(RS("itemDesc_CO"))
		sItemPic = RS("itemPic_CO")
		sItemPrice = RS("price_CO")
		sRetailPrice = RS("price_retail")
		'Free Bluetooth
		if FreeBluetooth = 1 then
			if cStr(nProdIdCheck) = FreeBluetoothItemID then
				FreeBluetoothPrice = sItemPrice
			end if
		end if
		'Free Product With Phone Purchase
		'if RS("typeID") = 16 then
		'	PhonePurchased = 1
		'	SquaretradeItemName = sItemName
		'	SquaretradeItemPrice = sItemPrice
		'	SquaretradeItemCondition = RS("Condition")
		'end if
		'Category Sale
		if CategorySale = 1 then
			if RS("typeID") = CategorySaleTypeID and RS("NoDiscount") = 0 then
				CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
			else
				SQL = "SELECT R.itemid FROM we_Items AS I INNER JOIN we_relatedItems AS R ON R.ITEMID = I.itemID"
				SQL = SQL & " WHERE (I.Sports = 0) AND (I.inv_qty <> 0 OR I.ItemKit_NEW IS NOT NULL) AND (I.price_CO > 0)"
				SQL = SQL & " AND (R.typeid = '" & CategorySaleTypeID & "') AND (R.modelid = '" & RS("modelID") & "') AND (R.itemID = '" & nProdIdCheck & "')"
				SQL = SQL & " AND I.NoDiscount = 0"
				dim RSrelated
				set RSrelated = Server.CreateObject("ADODB.Recordset")
				RSrelated.open SQL, oConn, 0, 1
				if not RSrelated.eof then CategorySaleTotal = CategorySaleTotal + (nProdQuantity * sItemPrice)
				RSrelated.close
				set RSrelated = nothing
			end if
		end if
		
		'Free Product Offer code added 2/1/2010 by MC
		if inStr(RS("PartNumber"),"scr-") > 0 then FreeProductInCart = 1
		'session("errorSQL") = "nItemTotal = " & nItemTotal & " | nProdIdCheck = " & nProdIdCheck & " | sItemPrice = " & sItemPrice & " | nProdQuantity = " & nProdQuantity
		
		if instr(RS("PartNumber"),"PHN-") < 1 and RS("nodiscount") = 0 and ucase(left(RS("PartNumber"), 2)) <> "BT" then 
			specialAmt = specialAmt + (sItemPrice * nProdQuantity)
		end if
		
		nSubTotal = nSubTotal + (sItemPrice * nProdQuantity)
		ntotalQuantity = ntotalQuantity + RS("qty")
		
		if nProdIdCheck => 1000000 then
			sql = "select preferredImg, image, defaultImg from we_items_musicSkins where id = " & nProdIdCheck
			session("errorSQL") = sql
			set msRS = oConn.execute(sql)
			if not msRSEOF then
				preferredImg = msRS("preferredImg")
				itempic_CO = msRS("image")
				defaultImg = msRS("defaultImg")
				if isnull(preferredImg) then																					
					if hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsLarge", itempic_CO) then
						sql = "update we_items_musicSkins set preferredImg = 1 where id = " & itemID
						session("errorSQL") = sql
						oConn.execute(sql)
						useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
					elseif hasImageOnRemote("http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault", defaultImg) then
						sql = "update we_items_musicSkins set preferredImg = 2 where id = " & itemID
						session("errorSQL") = sql
						oConn.execute(sql)
						useImg = "http://www.wirelessemporium.com/productpics/musicSkins/musicSkinsDefault/" & defaultImg
					else
						useImg = "/productPics/thumb/imagena.jpg"
					end if
				elseif preferredImg = 1 then
					useImg = "/productpics/musicSkins/musicSkinsLarge/" & itempic_CO
				elseif preferredImg = 2 then
					useImg = "/productpics/musicSkins/musicSkinsDefault/" & defaultImg
				else
					useImg = "/productPics/thumb/imagena.jpg"
				end if
			end if
			msRS = null
		else
			useImg = "/productpics/homepage65/" & sItemPic
		end if
		htmBasketRows = htmBasketRows & fWriteBasketRow(nProdIdCheck,sItemName,sItemPrice,cStr(nProdQuantity),useImg)
										
		WEhtml = WEhtml & "<input type=""hidden"" name=""itemID_" & nProdIdCount & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""itemQty_" & nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		'PPhtml = PPhtml & "<input type=""hidden"" name=""L_NUMBER" & nProdIdCount-1 & """ value=""" & nProdIdCheck & """><input type=""hidden"" name=""L_NAME" & nProdIdCount-1 & """ value=""" & sItemName & """><input type=""hidden"" name=""L_QTY" & nProdIdCount-1 & """ value=""" & nProdQuantity & """><input type=""hidden"" name=""L_AMT" & nProdIdCount-1 & """ value=""" & sItemPrice & """>" & vbcrlf
		
		'eBillme variables
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_number_" &  nProdIdCount & """ value=""" & nProdIdCheck & """>" & vbcrlf
		EBtemp = EBtemp & "<input type=""hidden"" name=""ssl_item_qty_" &  nProdIdCount & """ value=""" & nProdQuantity & """>" & vbcrlf
		EBxml = EBxml & "<itemdetails>"
		EBxml = EBxml & "<itemid>" & nProdIdCheck & "</itemid>"
		EBxml = EBxml & "<itemquantity>" & nProdQuantity & "</itemquantity>"
		EBxml = EBxml & "<itemname>" & sItemName & "</itemname>"
		EBxml = EBxml & "<itemcost>" & rs("price_CO") & "</itemcost>"
		EBxml = EBxml & "<itemtax1>0.0</itemtax1><itemtax2>0.0</itemtax2>"
		EBxml = EBxml & "</itemdetails>"
	end if
	RS.movenext
loop
RS.close
set RS = nothing
call fCloseConn()
' END basket grab

if nProdIdCount < 1 then
'basket currently empty
	response.redirect("/sessionexpired.asp")
	response.end
end if
' END basket grab

'#### Upfront Shipping Costs ####
dim shipcost0, shipcost2, shipcost3, shipcost4, shipcost5
shipcost0 = formatCurrency(Application("shippingCost"))
shipcost2 = formatCurrency(9.99)
shipcost3 = formatCurrency(22.99)
shipcost4 = formatCurrency(7.99)
shipcost5 = formatCurrency(14.99)

'#### Shady Shipping Costs ####
'shipcost0 = formatCurrency(3.96 + (1.99 * nTotalQuantity))
'shipcost2 = formatCurrency(7.96 + (1.99 * nTotalQuantity))
'shipcost3 = formatCurrency(20.96 + (1.99 * nTotalQuantity))
'shipcost4 = formatCurrency(4.96 + (2.99 * nTotalQuantity))
'shipcost5 = formatCurrency(11.96 + (2.99 * nTotalQuantity))

'#### changed on 4/19/2012 5:00PM by Terry
shipcost0 = formatCurrency(4.00 + (1.99 * nTotalQuantity))
shipcost2 = formatCurrency(8.00 + (1.99 * nTotalQuantity))
shipcost3 = formatCurrency(21.00 + (1.99 * nTotalQuantity))
shipcost4 = formatCurrency(5.00 + (2.99 * nTotalQuantity))
shipcost5 = formatCurrency(12.00 + (2.99 * nTotalQuantity))

if request.form("refreshed") = "1" then
	if len(sstate) > 0 and inStr("AB|BC|MB|NB|NL|NS|NT|NU|ON|PE|QC|SK|YT",sstate) > 0 then
		sSCountry = "CANADA"
	else
		sSCountry = ""
	end if
	select case shiptype
		case "2"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				shiptype = "5"
				nShipRate = shipcost5
			else
				strShiptype = "0"
				nShipRate = shipcost2
			end if
		case "3"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				shiptype = "5"
				nShipRate = shipcost5
			else
				strShiptype = "0"
				nShipRate = shipcost3
			end if
		case "4"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				nShipRate = shipcost4
			else
				strShiptype = "0"
				shiptype = "0"
				nShipRate = shipcost0
			end if
		case "5"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				nShipRate = shipcost5
			else
				strShiptype = "0"
				shiptype = "2"
				nShipRate = shipcost2
			end if
		case "0"
			if sSCountry = "CANADA" then
				strShiptype = "1"
				shiptype = "4"
				nShipRate = shipcost4
			else
				strShiptype = "0"
				nShipRate = shipcost0
			end if
		case else
			nShipRate = formatCurrency(request.form("shipcost" & shiptype))
	end select
	if sstate = "CA" then
		nCATax = round(nSubTotal * .0775, 2)
	else
		nCATax = 0
	end if
end if

'if nShipRate = "" or nShipRate = 0.0 then
'	nShipRate = shipcost0
'end if
%>

<!--#include virtual="/includes/template/top_cart.asp"-->
<!--#include virtual="/cart/includes/inc_promoFunctions.asp"-->
<!--#include virtual="/cart/UPSxml.asp"-->
<script language="javascript">
<!-- Begin
function changeShippingAddress(a) {
	var b = eval('document.frmProcessOrder.sAddress1' + a);
	var c = eval('document.frmProcessOrder.sAddress2' + a);
	var d = eval('document.frmProcessOrder.sCity' + a);
	var e = eval('document.frmProcessOrder.sState' + a);
	var f = eval('document.frmProcessOrder.sZip' + a);
	document.frmProcessOrder.sAddress1.value = b.value;
	document.frmProcessOrder.sAddress2.value = c.value;
	document.frmProcessOrder.sCity.value = d.value;
	document.frmProcessOrder.sState.value = e.value;
	document.frmProcessOrder.sZip.value = f.value;
}

function CheckSubmit() {
	var f = document.frmProcessOrder;
	bValid = true;
	
	CheckValidNEW(f.email.value, "Email is a required field!");
	CheckEMailNEW(f.email.value);
	
	CheckValidNEW(f.fname.value, "Your First Name is a required field!");
	CheckValidNEW(f.lname.value, "Your Last Name is a required field!");
	CheckValidNEW(f.phone.value, "Your Phone Number is a required field!");
	
	CheckValidNEW(f.sAddress1.value, "Your Address (for Shipping Address) is a required field!");
	CheckValidNEW(f.sCity.value, "Your City (for Shipping Address) is a required field!");
	sString = f.sState.options[f.sState.selectedIndex].value;
	CheckValidNEW(sString, "State (for Shipping Address) is a required field!");
	CheckValidNEW(f.sZip.value, "Your Zip Code (for Shipping Address) is a required field!");
	
	CheckValidNEW(f.bAddress1.value, "Your Address (for Billing Address) is a required field!");
	CheckValidNEW(f.bCity.value, "Your City (for Billing Address) is a required field!");
	sString = f.bState.options[f.bState.selectedIndex].value;
	CheckValidNEW(sString, "State (for Billing Address) is a required field!");
	CheckValidNEW(f.bZip.value, "Your Zip Code (for Billing Address) is a required field!");
	
	var a = f.PaymentType.length;
	for (var i = 0; i < a; i++) {
		if (f.PaymentType[i].checked) {
			var b = f.PaymentType[i].value;
		}
	}
	if (b != 'eBillMe' && b != 'PAY') {
		sString = f.cc_cardType.value;
		CheckValidNEW(sString, "Credit Card Type is a required field!");
		CheckValidNEW(f.cc_cardOwner.value, "Your Name (as it appears on your credit card) is a required field!");
		CheckValidNEW(f.cardNum.value, "Your Credit Card Number is a required field!");
		CheckValidNEW(f.cc_month.value, "Your Credit Card Expiration Month is a required field!");
		CheckValidNEW(f.cc_year.value, "Your Credit Card Expiration Year is a required field!");
		CheckValidNEW(f.secCode.value, "Your Credit Card Security Code is a required field!");
	}

	if (bValid) {
		var aa = f.shiptype.length;
		for (var i = 0; i < aa; i++) {
			if (f.shiptype[i].checked) {
				var shiptype = f.shiptype[i].value;
			}
		}
		var shiprate = eval('document.frmProcessOrder.shipcost' + shiptype);
		f.shippingTotal.value = shiprate.value;
		<% if instr(request.ServerVariables("SERVER_NAME"),"staging") > 0 then %>
		document.frmProcessOrder.action = '/cart/process/processing.asp';
		<% else %>
		document.frmProcessOrder.action = 'https://www.cellularoutfitter.com/cart/process/processing.asp';
		<% end if %>
		//document.frmProcessOrder.action = '/cart/process/processing.asp';
		document.getElementById("submitBttn").innerHTML = "Please wait while<br />we process your order"
		document.frmProcessOrder.submit();
	}
	return false;
}

var bAddress1 = "";
var bAddress2 = "";
var bCity = "";
var bZip = "";
var bState = "";
var bStateIndex = "0";

function ShipToBillPerson() {
	var f = document.frmProcessOrder;
	if (f.sameaddress.checked == false) {
		f.bAddress1.value = "";
		f.bAddress2.value = "";
		f.bCity.value = "";
		f.bZip.value = "";
		f.bState.selectedIndex = f.sState.options[0];
	}
	else {
		f.bAddress1.value = f.sAddress1.value;
		f.bAddress2.value = f.sAddress2.value;
		f.bCity.value = f.sCity.value;
		f.bZip.value = f.sZip.value;
		f.bState.selectedIndex = f.sState.selectedIndex;
	}
}

function fnRecalculate() {
	document.frmProcessOrder.action = '/cart/checkout2.asp';
	//document.frmProcessOrder.action = '/cart/checkout2.asp';
	document.frmProcessOrder.submit();
}
//  End -->
</script>

								<td width="980" align="center" valign="top">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
										<tr>
											<td width="100%" valign="top" align="center" style="border-top:1px dotted #000; border-bottom:1px dotted #000; padding:10px 0px 5px 0px;">
                                            	<img src="/images/cart/lock.png" border="0" />&nbsp;
                                            	<img src="/images/cart/steps_cart_off.jpg" border="0" />&nbsp;
                                            	<img src="/images/cart/steps_arrow.jpg" border="0" />&nbsp;
                                            	<img src="/images/cart/steps_order_on.jpg" border="0" />&nbsp;
                                            	<img src="/images/cart/steps_arrow.jpg" border="0" />&nbsp;
                                            	<img src="/images/cart/steps_receipt.jpg" border="0" />
                                            </td>
										</tr>
										<tr>
											<td width="100%" valign="top" align="center" style="padding-top:10px;">
												<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
												<form action="/cart/checkout2.asp" method="post" name="frmProcessOrder">
													<tr>
														<td width="100%" valign="top" align="center" bgcolor="#FFFFFF">
															<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
																<tr>
																	<td width="970" valign="top" align="center">
																		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
																			<tr>
																				<td width="277" valign="top" align="center">
																					<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
																						<tr>
																							<td valign="middle" align="center" width="100%" height="25" bgcolor="#CCCCCC" class="top-link">
																								Shipping &amp; Handling Fees
																							</td>
																						</tr>
																						<tr>
																							<td valign="top" align="left" width="100%" bgcolor="#DDDDDD" class="mc-text">
																								<%
																								if strShiptype = "1" then
																									%>
																									<input type="hidden" name="shipcost4" value="<%=shipcost4%>"><input type="radio" name="shiptype" id="rad_ship1" value="4"<%if shiptype = "4" or shiptype = "3" then response.write " checked"%>>USPS First Class Int'l (8-12 business days)<br>
																									<input type="hidden" name="shipcost5" value="<%=shipcost5%>"><input type="radio" name="shiptype" id="rad_ship2" value="5"<%if shiptype = "5" then response.write " checked"%>>USPS Priority Int'l (3-8 business days)
																									<%
																								else
																									%>
																									<input type="hidden" name="shipcost0" value="<%=shipcost0%>"><input type="radio" name="shiptype" id="rad_ship1" value="0"<%if shiptype = "0" or shiptype = "" then response.write " checked"%>>USPS First Class (4-10 business days)<br />
																									<input type="hidden" name="shipcost2" value="<%=shipcost2%>"><input type="radio" name="shiptype" id="rad_ship2" value="2"<%if shiptype = "2" then response.write " checked"%>>USPS Priority Mail (2-4 business days)
																									<!--<input type="hidden" name="shipcost3" value="<%=shipcost3%>"><input type="radio" name="shiptype" id="rad_ship3" value="3"<%if shiptype = "3" then response.write " checked"%>>Express Mail (1-2 business days) - <%=shipcost3%>-->
																									<%
																									on error resume next
																									'Response.Write("<p>nTotalQuantity 2 "&nTotalQuantity)
																									if sZip <> "" then 
																										response.write UPScode(sZip,sWeight,shiptype,nTotalQuantity,0,0)
																									else
																										response.Write("no zip?")
																									end if
																									on error goto 0
																									shipcost = request.form("shipcost" & request.form("shiptype"))
																								end if
																								%>
																								<input type="hidden" name="sWeight" value="<%=sWeight%>">
																								<input type="hidden" name="refreshed" value="1">
																								<input type="hidden" name="nTotalQuantity" value="<%=nTotalQuantity%>">
																								<p align="center" style="font-size:9px;">
																								  <input type="image" name="recalculate" src="/images/cart/button_apply.jpg" width="116" height="20" border="0" alt="Apply" onClick="javascript:fnRecalculate();">
																									<br>
																									(* MUST press APPLY to update total.)
																								</p>
																								<input type="hidden" name="shippingTotal" value="<%=nShipRate%>">
																							</td>
																						</tr>
																					</table>
																				</td>
																				<td width="3" valign="top" align="center" height="100%" bgcolor="#FFFFFF"><img src="/images/spacer.gif" width="3" height="1" border="0"></td>
																				<td width="690" valign="top" align="center" height="100%">
																					<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
																						<tr>
																							<td width="100%" valign="top" align="center" height="100%" colspan="2">
																								<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
																									<%=htmBasketRows%>
                                                                                                    <%
                                                                                                    if spendThisMuch = 1 then
                                                                                                        if nSubTotal => stm_total then
                                                                                                            sql = "select b.brandName, c.modelName, a.* from we_items a left join we_brands b on a.brandID = b.brandID left join we_models c on a.modelID = c.modelID where itemID = " & stm_freeItem
																											set freeItemRS = Server.CreateObject("ADODB.Recordset")
																											freeItemRS.open sql, oConn, 0, 1
																											
																											free_brandName = prepStr(freeItemRS("brandName"))
																											free_modelName = prepStr(freeItemRS("modelName"))
																											free_partNumber = prepStr(freeItemRS("partNumber"))
																											free_itemDesc = prepStr(freeItemRS("itemDesc"))
																											free_itemID = prepInt(freeItemRS("itemID"))
																											free_itemPic = freeItemRS("itemPic_CO")
																											free_price = freeItemRS("price_co")
                                                                                                    %>
                                                                                                    <tr>
                                                                                                    	<td align="center" valign="middle" bgcolor="#EEEEEE" colspan="2">
                                                                                                        	<table width="100%" border="0" cellpadding="8" cellspacing="0" align="center">
                                                                                                            	<tr>
																													<td width="75" align="center"><a href="/p-<%=free_itemID%>-<%=formatSEO(free_itemDesc)%>.html"><img src="/productpics/icon/<%=free_itemPic%>" width="65" height="65" style="border-style: solid; border-width: 1px; border-color: #CCCCCC;"></a></td>
																													<td align="center" valign="middle" colspan="2">
                                                                                                                    	<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
																															<tr>
                                                                                                                            	<td valign="top" colspan="2"><a href="/p-<%=free_itemID%>-<%=formatSEO(free_itemDesc)%>.html" class="product-checkout"><%=free_itemDesc%></a></td>
                                                                                                                            </tr>
																															<tr>
                                                                                                                            	<td valign="top" width="80" class="cart-text">Item&nbsp;Price:</td>
                                                                                                                                <td valign="top" width="460" class="cart-text"><%=formatCurrency(free_price,2)%></td>
                                                                                                                            </tr>
																															<tr>
                                                                                                                            	<td valign="top" width="80" class="cart-text">Quantity:</td>
                                                                                                                                <td valign="top" width="460" class="cart-text">1</td>
                                                                                                                            </tr>
																														</table>
                                                                                                                    </td>
																													<td align="center" valign="middle" class="mc-text"><b>FREE</b></td>
																												</tr>
	                                                                                                        </table>
                                                                                                        </td>
                                                                                                    </tr>
																									<tr><td align="center" height="5" valign="middle" bgcolor="#FFFFFF" colspan="2"><img src="/images/spacer.gif" width="1" height="5" border="0"></td></tr>
                                                                                                    <%
                                                                                                            call fCloseConn()
                                                                                                        end if
                                                                                                    end if
																									%>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td align="center" valign="middle" height="10" colspan="2"><img src="/images/cart/hori-line-2.jpg" width="690" height="1" border="0"></td>
																						</tr>
																						<tr>
																							<td width="590" align="right" valign="top" class="mc-text2">Item&nbsp;Total:</td>
																							<td width="100" align="right" valign="top" class="mc-text2"><b><%=formatCurrency(nsubTotal)%></b></td>
																						</tr>
																						<%
																						if sstate = "CA" then
																							nCATax = round(nSubTotal * .0775, 2)
																						else
																							nCATax = 0
																						end if
																						'nAmount = nShipRate + nCATax + nsubTotal
																						nAmount = nShipRate + nCATax + nsubTotal + buysafeamount
																						if date > cdate("9/30/2011") then specialAmt = 0 end if
																						spend2Get = 0
																						if spend2Get = 1 then																						
																							if sPromoCode = "" and specialAmt >= 30 then
																								sPromoCode = "specialDeal"
																								nSubTotal = nSubTotal - 5
																								nAmount = nAmount - 5
																							end if
																						end if	
																						if sPromoCode <> "" then
																							session("promocode") = sPromoCode
																							if sPromoCode <> "specialDeal" then
																							%>
																							<tr>
																								<td align="right" valign="top" class="mc-text2" colspan="2">
																									Promotions:&nbsp;&nbsp;&nbsp;<%=couponDesc%>
																									<input type="hidden" name="sPromoCode" value="<%=sPromoCode%>">
																									<input type="hidden" name="nPromo" value="<%=couponid%>">
																								</td>
																							</tr>
                                                                                            <%
																							end if
																							%>
                                                                                            <!--
																							<tr>
																								<td align="right" valign="top" class="mc-text2">Discounted&nbsp;Subtotal:</td>
																								<td align="right" valign="top" class="mc-text2"><b><%=formatCurrency(nSubTotal)%></b></td>
																							</tr>
                                                                                            -->
																							<%
																						end if
																						if nCATax > 0 then
																							%>
																							<tr>
																								<td align="right" valign="top" class="mc-text2">Additional 7.75% Tax for Shipments Within CA:</td>
																								<td align="right" valign="top" class="mc-text2"><b><%=formatCurrency(nCATax)%></b></td>
																							</tr>
																							<%
																						end if
																						if WantsBondField = "true" and buysafeamount > 0 then
																							%>
																							<tr>
																								<td align="right" valign="top" class="mc-text2">
																									buySAFE&nbsp;Bond:
																								</td>
																								<td align="right" valign="top" class="mc-text2">
																									<input name="WantsBondField" value="<%=WantsBondField%>" type="hidden">
																									<b><%=formatCurrency(buysafeamount)%></b>
																								</td>
																							</tr>
																							<%
																						end if
																						%>
																						<tr>
																							<td align="right" valign="top" class="mc-text2">Shipping&nbsp;&amp;&nbsp;Handling *:</td>
																							<td align="right" valign="top" class="mc-text2"><b><%=nShipRate%></b></td>
																						</tr>
                                                                                        <%
																						if discountTotal > 0 then
																						%>
																						<tr>
																							<td align="right" valign="top" class="mc-text2">Discount&nbsp;Total:</td>
																							<td align="right" valign="top" class="mc-text2">
																								<b><span id="GrandTotal" style="color:#900;"><%=formatCurrency(discountTotal)%></span></b>
																							</td>
																						</tr>
                                                                                        <%
																						end if
																						%>
																						<tr>
																							<td align="right" valign="top" class="mc-text2">Grand&nbsp;Total:</td>
																							<td align="right" valign="top" class="mc-text2">
																								<input type="hidden" name="nCATax" value="<%=nCATax%>">
																								<input type="hidden" name="subTotal" value="<%=nsubTotal%>">
																								<input type="hidden" name="grandTotal" value="<%=nAmount%>">
																								<input type="hidden" name="buysafeamount" value="<%=round(buysafeamount,2)%>">
																								<input type="hidden" name="WantsBondField" value="<%=WantsBondField%>">
																								<input type="hidden" name="buysafecartID" value="<%=shoppingcartID%>">
																								<b><span id="GrandTotal"><%=formatCurrency(nAmount)%></span></b>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															
															<br><br>
															
															<img src="/images/cart/shipping_methods.jpg" border="0" alt="Shipping & Billing Information" />                                                                 
															<table width="970" border="0" cellpadding="0" cellspacing="0" align="center">
																<tr>
																	<td width="50%" valign="top">
																		<table id="tbl_step2" width="100%" border="0" cellpadding="2" cellspacing="0" align="center" class="mc-text">
																			<tr>
																				<td width="15" align="center" valign="top" class="blue-star">&nbsp;</td>
																				<td valign="top" colspan="2" align="left"><p style="font-weight:bold;">Shipping&nbsp;Address:</p></td>
																			</tr>
																			<tr>
																				<td colspan="3"><img src="/images/spacer.gif" width="1" height="20" border="0"></td>
																			</tr>
																			<tr>
																				<td valign="top" class="blue-star">*</td>
																				<td width="30" valign="top" class="checkout-text" align="left">First&nbsp;Name:</td>
																				<td align="left"><input type="text" name="fname" size="40" value="<%=fname%>"></td>
																			</tr>
																			<tr>
																				<td valign="top" class="blue-star">*</td>
																				<td valign="top" class="checkout-text" align="left">Last&nbsp;Name:</td>
																				<td align="left"><input type="text" name="lname" size="40" value="<%=lname%>"></td>
																			</tr>
																			<tr>
																				<td valign="top" class="blue-star">*</td>
																				<td valign="top" class="checkout-text" align="left">Address&nbsp;1:</td>
																				<td align="left"><input type="text" name="sAddress1" size="40" value="<%=saddress1%>"></td>
																			</tr>
																			<tr>
																				<td valign="top" class="blue-star">&nbsp;</td>
																				<td valign="top" class="checkout-text" align="left">Address&nbsp;2:</td>
																				<td align="left"><input type="text" name="sAddress2" size="40" value="<%=saddress2%>"></td>
																			</tr>
																			<tr>
																				<td valign="top" class="blue-star">*</td>
																				<td valign="top" class="checkout-text" align="left">City:</td>
																				<td align="left"><input type="text" name="sCity" size="40" value="<%=scity%>"></td>
																			</tr>
																			<tr>
																				<td valign="top" class="blue-star">*</td>
																				<td valign="top" class="checkout-text" align="left">State:</td>
																				<td align="left">
																					<select name="sState">
																						<%getStates(sstate)%>
																					</select>
																				</td>
																			</tr>
																			<tr>
																				<td valign="top" class="blue-star">*</td>
																				<td valign="top" class="checkout-text" align="left">Postal&nbsp;Code:</td>
																				<td align="left"><input type="hidden" name="sZip" value="<%=szip%>"><strong><font color="red"><%=szip%></font></strong>&nbsp;&nbsp;<a href="/cart/basket.asp">change</a></td>
																			</tr>
																			<tr>
																				<td valign="top" class="blue-star">*</td>
																				<td valign="top" class="checkout-text" align="left">Phone:</td>
																				<td align="left"><input type="text" name="phone" size="40" value="<%=phone%>"></td>
																			</tr>
																			<tr>
																				<td valign="top" class="blue-star">*</td>
																				<td valign="top" class="checkout-text" align="left">Email:</td>
																				<td align="left"><input type="text" name="email" size="40" value="<%=email%>"></td>
																			</tr>
																		</table>
																	</td>
																	<td width="50%" valign="top">
																		<table id="billing_info" width="100%" border="0" cellpadding="2" cellspacing="0" align="center" class="mc-text">
																			<tr>
																				<td width="15" valign="top" class="blue-star">&nbsp;</td>
																				<td valign="top" colspan="2" align="left"><p style="font-weight:bold;">Billing&nbsp;Address:</p></td>
																			</tr>
																			<tr>
																				<td align="left" valign="top" class="blue-star"><input id="sameaddress" type="checkbox" name="sameaddress" value="yes" onClick="javascript:ShipToBillPerson();"<%if request.form("sameaddress")="yes" then response.write " checked"%>></td>
																				<td colspan="2" class="checkout-blue" align="left">Check if Billing Address is the same as Shipping Address.</td>
																			</tr>
																			<tr>
																				<td valign="top" class="blue-star">*</td>
																				<td width="30" valign="top" class="checkout-text" align="left">Address&nbsp;1:</strong></td>
																				<td align="left"><input name="bAddress1" size="40" value="<%=bAddress1%>" type="text"></td>
																			</tr>
																			<tr>
																				<td valign="top" class="blue-star">&nbsp;</td>
																				<td valign="top" class="checkout-text" align="left">Address&nbsp;2</td>
																				<td align="left"><input name="bAddress2" size="40" value="<%=bAddress2%>" type="text"></td>
																			</tr>
																			<tr>
																				<td valign="top" class="blue-star">*</td>
																				<td valign="top" class="checkout-text" align="left">City:</strong></td>
																				<td align="left"><input name="bCity" size="40" value="<%=bCity%>" type="text"></td>
																			</tr>
																			<tr>
																				<td valign="top" class="blue-star">*</td>
																				<td valign="top" class="checkout-text" align="left">State:</strong></td>
																				<td valign="top" align="left">
																					<select name="bState">
																						<%getStates(bState)%>
																					</select>
																				</td>
																			</tr>
																			<tr>
																				<td valign="top" class="blue-star">*</td>
																				<td valign="top" class="checkout-text" align="left">Postal&nbsp;Code:</strong></td>
																				<td style="padding-bottom:5px;" align="left"><input name="bZip" size="20" value="<%=bZip%>" type="text"></td>
																			</tr>
																			<tr>
																				<td align="left" valign="top"><input type="checkbox" name="chkOptMail" value="Y" checked>&nbsp;</td>
																				<td valign="top" colspan="2" class="checkout-text" align="left">
																					<div align="justify">
																						Be alerted via email on occasional time-sensitive "below wholesale" private sales offered by CellularOutfitter.com.
																						CellularOutfitter.com abides by the strictest Privacy Policy standards.
																						<a href="/privacypolicy.html" target="_blank">Click Here</a> for Privacy Policy details.
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															
															<br><br>
															<img src="/images/cart/payment_methods.jpg" border="0" alt="Select a Payment Method" />
															<table width="970" border="0" cellpadding="0" cellspacing="0" align="center">
																<tr>
																	<td width="100%" align="center" class="mc-text">
																		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
																			<tr>
																				<td width="467" valign="top" align="left" class="mc-text">
																					<table width="100%" border="0" cellpadding="0" cellspacing="5" align="center">
																						<tr>
                                                                                        	<td><input type="radio" name="PaymentType" value="CC"></td>
																							<td width="245" colspan="2">
                                                                                            	<div style="float:left; padding-right:5px;"><img src="/images/cart/visa_off.gif" border="0" id="visaPic" /></div>
                                                                                                <div style="float:left; padding-right:5px;"><img src="/images/cart/masterCard_off.gif" border="0" id="mcPic" /></div>
                                                                                                <div style="float:left; padding-right:5px;"><img src="/images/cart/americanExpress_off.gif" border="0" id="amexPic" /></div>
                                                                                                <div style="float:left;"><img src="/images/cart/discover_off.gif" border="0" id="discoverPic" /></div>
                                                                                                <input type="hidden" name="cc_cardType" value="">
                                                                                            </td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																							<td><input name="cc_cardOwner" type="text" size="23" value="Card Holder Name" onFocus="testValue(this,this.value)" class="checkout-text"></td>
																							<td>&nbsp;</td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																							<td colspan="2">
                                                                                            	<div style="float:left; padding-right:10px;"><input name="cardNum" type="text" id="cardNum" size="23" maxlength="16" value="Credit Card Number" onFocus="testValue(this,this.value)" onblur="getCardType(this.value)" class="checkout-text"></div>
                                                                                            	<div style="float:left;"><input name="secCode" type="text" id="secCode" size="14" maxlength="4" value="Security Code" onFocus="testValue(this,this.value)" class="checkout-text"></div>
                                                                                                <div style="position:relative; float:left; padding-left:10px;" onmouseover="cvvHelp('on')">
                                                                                                	<img src="/images/cart/question.jpg" width="15" height="16" border="0">
                                                                                                    <div id="cvvWindow" style="position:absolute; top:-200px; left:30px; background-color:#FFF; display:none; width:320px; height:200px; z-index:999; border:2px solid #000; padding:3px;">
                                                                                                    	<div style="background-color:#333; height:15px; color:#FFF; font-weight:bold; font-size:11px; width:100%;">
                                                                                                        	<div style="float:left; padding-left:10px;">Credit Card CVV</div>
                                                                                                            <div style="float:right; padding-right:10px; cursor:pointer;" onclick="cvvHelp('off')">Close X</div>
                                                                                                        </div>
                                                                                                        <div id="cvvData" style="width:320px; overflow:auto; height:180px;"></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																							<td colspan="2">
																								<font size="2">Expires&nbsp;</font>
																								<select name="cc_month" class="checkout-text">
																									<option value="01">01 (January)</option>
																									<option value="02">02 (February)</option>
																									<option value="03">03 (March)</option>
																									<option value="04">04 (April)</option>
																									<option value="05">05 (May)</option>
																									<option value="06">06 (June)</option>
																									<option value="07">07 (July)</option>
																									<option value="08">08 (August)</option>
																									<option value="09">09 (September)</option>
																									<option value="10">10 (October)</option>
																									<option value="11">11 (November)</option>
																									<option value="12">12 (December)</option>
																								</select>
																								<select name="cc_year" class="checkout-text">
																									<%
																									for countYear = 0 to 14
																										response.write "<option value=""" & right(cStr(year(date) + countYear),2) & """>" & cStr(year(date) + countYear) & "</option>" & vbcrlf
																									next
																									%>
																								</select>
																							</td>
																						</tr>
																					</table>
																				</td>
																				<td width="30" valign="top" height="100%" align="left">
                                                                                	<div style="margin-top:10px;">
                                                                                    	<img src="/images/cart/or.jpg" border="0" />
                                                                                    </div>                                                                                
                                                                                	<div style="height:110px; width:30px; border-right:1px dotted #000; margin-top:15px;">
                                                                                    </div>
                                                                                </td>
																				<td width="473" valign="top" align="center" style="padding-left:10px;">
                                                                                	<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                                                                                    	<tr>
																							<td width="10px" align="center" valign="middle"><input type="radio" name="PaymentType" value="eBillMe"></td>
                                                                                            <td width="*" align="left"><img src="/images/WU/WU-Pay-wutag-small.jpg" border="0" width="227" height="44" /></td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																							<td valign="top" class="mc-text" width="100%" style="padding-top:10px;" align="left">
                                                                                                WU&reg; Pay allows you to pay cash online. No financial information required. Simply pay using your bank's online bill pay service, or at a Western Union&reg; Agent location near you. 
																								Just like paying a utility bill! <a href="http://www.westernunion.com/wupay/learn/how-to-use-wupay-video" target="_blank">Watch our demo</a>.
																							</td>
                                                                                        </tr>
																					</table>
																				</td>
																			</tr>
                                                                            <tr>
                                                                            	<td colspan="3" width="100%" align="center" style="padding:35px 0px 20px 0px; border-bottom:1px dotted #000;">
                                                                                    <!--<img onClick="popUp('https://one-time-offer.com/sg/gateway.aspx?v=0353A36353&p=92F20262D2BE77B7D7F706E72FC32333A383539F16D6060727&cl=F6E606','',screen.availHeight * .8, screen.availWidth * .9, screen.availWidth - (screen.availWidth * .91),screen.availHeight - (screen.availHeight * .87)); return CheckSubmit();" src="/images/cart/order_submit.jpg" alt="Submit Order" style="cursor: pointer;">-->
                                                                                    <div id="submitBttn" style="font-weight:bold; font-size:18px; color:#000;"><img onClick="return CheckSubmit();" src="/images/cart/order_submit.jpg" alt="Submit Order" style="cursor: pointer;"></div>
                                                                                    <input type="hidden" name="submitted" value="submitted">                                                                                
                                                                                </td>
                                                                            </tr>
																		</table>
																	</td>
																</tr>
															</table>

															<table width="970" border="0" cellpadding="0" cellspacing="0" align="center">
                                                            	<tr>
                                                                	<td widt"100%" style="padding-top:30px;">
                                                                         <div style="width:100%" >
                                                                            <div style="float:left; width:20%; padding:10px 20px 0px 100px;"><a href="/privacypolicy.html"><img src="/images/truste-seal-web.gif" border="0" /></a></div>
                                                                            <div style="float:left; width:19%; padding-top: 7px;"><a href="http://www.la.bbb.org/BusinessReport.aspx?CompanyID=100097908&source=ctc" target="_blank"><img src="/images/BBB_clickratingsm.gif" border="0" /></a></div>
                                                                            <div style="float:left; width:13%">
                                                                                <script type="text/javascript" src="/includes/js/j.js"></script>
                                                                                <script type="text/javascript">showMark(3);</script>
                                                                                <noscript><img id="googleChkOutImg" src="/images/sc.gif" border="0" width="72px" height="73px" /></noscript>
                                                                            </div>
                                                                            <div style="float:left; width:15%"><img src="/images/Inc5000_color3.jpg" border="0" /></div>
                                                                            <div style="float:left; width:15%"><a href="http://www.shopwiki.com/store-252661" target="_blank" title="CellularOutfitter.com is a ShopWiki Approved Store"><img src="/images/Shopwiki_Certified_EN.gif" border="0" /></a></div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            
														</td>
													</tr>
													<!--WE variables-->
													<%=WEhtml%>
													<!--eBillme variables-->
													<%=EBtemp%>
													<input type="hidden" name="myBasketXML" value="<%=EBxml%>">
													<input type="hidden" name="ssl_ProdIdCount" value="<%=nProdIdCount%>">
												</form>
												</table>
											</td>
										</tr>
									</table>
								</td>

<!--#include virtual="/includes/template/bottom_cart.asp"-->
<%
function fWriteBasketRow(nProdId,sItemName,sItemPrice,nQty,sitempic_CO)
	'this will write out the row for the basket
	dim sTemp
	sTemp = "<tr><td align=""center"" valign=""middle"" bgcolor=""#EEEEEE"" colspan=""2""><table width=""100%"" border=""0"" cellpadding=""8"" cellspacing=""0"" align=""center""><tr>" & vbcrlf
	sTemp = sTemp & "<td width=""75"" align=""center""><a href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html""><img src=""" & useImg & """ width=""65"" height=""65"" style=""border-style: solid; border-width: 1px; border-color: #CCCCCC;""></a></td>" & vbcrlf
	sTemp = sTemp & "<td align=""center"" valign=""middle"" colspan=""2""><table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" align=""center"">" & vbcrlf
	sTemp = sTemp & "<tr><td valign=""top"" colspan=""2"" align=""left"" style=""padding-left:15px;""><a href=""/p-" & nProdId & "-" & formatSEO(sItemName) & ".html"" class=""product-checkout"">" & sItemName & "</a></td></tr>" & vbcrlf
	sTemp = sTemp & "<tr><td valign=""top"" width=""80"" class=""cart-text"" align=""left"" style=""padding-left:15px;"">Item&nbsp;Price:</td><td valign=""top"" width=""460"" class=""cart-text"" align=""left"">" & formatCurrency(cDbl(sItemPrice)) & "</td></tr>" & vbcrlf
	sTemp = sTemp & "<tr><td valign=""top"" width=""80"" class=""cart-text"" align=""left"" style=""padding-left:15px;"">Quantity:</td><td valign=""top"" width=""460"" class=""cart-text"" align=""left"">" & nQty & "</td></tr>" & vbcrlf
	sTemp = sTemp & "</td></table>" & vbcrlf
	sTemp = sTemp & "<td align=""center"" valign=""middle"" class=""mc-text""><b>" & formatCurrency((cDbl(sItemPrice) * cDbl(nQty))) & "</b></td>" & vbcrlf
	sTemp = sTemp & "</tr></td></table></td></tr>" & vbcrlf
	sTemp = sTemp & "<tr><td align=""center"" height=""5"" valign=""middle"" bgcolor=""#FFFFFF"" colspan=""2""><img src=""/images/spacer.gif"" width=""1"" height=""5"" border=""0""></td></tr>" & vbcrlf
	fWriteBasketRow = sTemp
end function
%>
<script language="javascript">
	function testValue(obj,objVal) {
		if (objVal == "Card Holder Name") {
			obj.value = ""
		}
		if (objVal == "Credit Card Number") {
			obj.value = ""
		}
		if (objVal == "Security Code") {
			obj.value = ""
		}
	}
	
	function popUp(url, winName, h, w, l, t) {
		var args = 'height=' + h + ',width=' + w + ',left=' + l + ',top=' + t + ',toolbar=1,location=1,directories=0,status=1,menubar=1,scrollbars=1,resizable=1';
		window.open(url, winName, args);
	}
	
	function getCardType(cardNum) {
		document.getElementById("visaPic").src = "/images/cart/visa_off.gif"
		document.getElementById("amexPic").src = "/images/cart/americanExpress_off.gif"
		document.getElementById("mcPic").src = "/images/cart/masterCard_off.gif"
		document.getElementById("discoverPic").src = "/images/cart/discover_off.gif"
		var first1 = parseInt(cardNum.substring(0,1))
		var first2 = parseInt(cardNum.substring(0,2))
		if (first1 == 4) {
			document.frmProcessOrder.cc_cardType.value = "VISA"
			document.getElementById("visaPic").src = "/images/cart/visa_on.gif"
		}
		else if (first2 == 34 || first2 == 37) {
			document.frmProcessOrder.cc_cardType.value = "AMEX"
			document.getElementById("amexPic").src = "/images/cart/americanExpress_on.gif"
		}
		else if (first2 > 50 && first2 < 56) {
			document.frmProcessOrder.cc_cardType.value = "MC"
			document.getElementById("mcPic").src = "/images/cart/masterCard_on.gif"
		}
		else if (first1 == 6) {
			document.frmProcessOrder.cc_cardType.value = "DISC"
			document.getElementById("discoverPic").src = "/images/cart/discover_on.gif"
		}
	}
	
	function cvvHelp(dir) {
		if (document.getElementById("cvvData").innerHTML == "") {
			ajax('/ajax/cvv.asp','cvvData')
		}
		if (dir == "off") {
			document.getElementById("cvvWindow").style.display = "none"
		}
		else {
			document.getElementById("cvvWindow").style.display = ""
		}
	}
</script>